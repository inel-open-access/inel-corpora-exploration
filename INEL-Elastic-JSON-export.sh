#!/bin/bash
java -Xmx3g -jar software/hzsk-corpus-services-1.0-jar-with-dependencies.jar -i corpora/selkup-0.1/selkup.coma -o selkup-0.1_es-json-report-output.html -c EXB2ESJSON -p mode=inel -p mode=token -p lang=sel -f
java -Xmx3g -jar software/hzsk-corpus-services-1.0-jar-with-dependencies.jar -i corpora/dolgan-1.0/dolgan.coma -o dolgan-1.0_es-json-report-output.html -c EXB2ESJSON -p mode=inel -p mode=token -p lang=dlg -f
java -Xmx3g -jar software/hzsk-corpus-services-1.0-jar-with-dependencies.jar -i corpora/kamas-1.0/kamas.coma -o kamas-1.0_es-json-report-output.html -c EXB2ESJSON -p mode=inel -p mode=token -p lang=xas -f
