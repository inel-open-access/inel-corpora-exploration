<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2CB1AE92-D8D8-401E-4DB6-4919A8C1F3A9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Alenushka_continuation_flk.wav" />
         <referenced-file url="PKZ_196X_Alenushka_continuation_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Alenushka_continuation_flk\PKZ_196X_Alenushka_continuation_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">127</ud-information>
            <ud-information attribute-name="# HIAT:w">80</ud-information>
            <ud-information attribute-name="# e">80</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.056" type="appl" />
         <tli id="T1" time="0.844" type="appl" />
         <tli id="T2" time="1.632" type="appl" />
         <tli id="T3" time="2.42" type="appl" />
         <tli id="T4" time="3.208" type="appl" />
         <tli id="T5" time="3.996" type="appl" />
         <tli id="T6" time="4.784" type="appl" />
         <tli id="T7" time="5.85311091747305" />
         <tli id="T8" time="7.254" type="appl" />
         <tli id="T9" time="8.507" type="appl" />
         <tli id="T10" time="9.986287191770646" />
         <tli id="T11" time="11.2" type="appl" />
         <tli id="T12" time="12.302" type="appl" />
         <tli id="T13" time="13.404" type="appl" />
         <tli id="T14" time="14.505" type="appl" />
         <tli id="T15" time="15.701" type="appl" />
         <tli id="T16" time="16.532" type="appl" />
         <tli id="T17" time="17.363" type="appl" />
         <tli id="T18" time="18.194" type="appl" />
         <tli id="T19" time="19.024" type="appl" />
         <tli id="T20" time="19.855" type="appl" />
         <tli id="T21" time="20.686" type="appl" />
         <tli id="T22" time="21.457002025367228" />
         <tli id="T23" time="22.267" type="appl" />
         <tli id="T24" time="23.018" type="appl" />
         <tli id="T25" time="25.12571189972201" />
         <tli id="T26" time="25.908" type="appl" />
         <tli id="T27" time="26.645" type="appl" />
         <tli id="T28" time="27.382" type="appl" />
         <tli id="T29" time="28.33225671897547" />
         <tli id="T30" time="30.54" type="appl" />
         <tli id="T31" time="32.289" type="appl" />
         <tli id="T32" time="33.235" type="appl" />
         <tli id="T33" time="33.981" type="appl" />
         <tli id="T34" time="35.64531215914396" />
         <tli id="T35" time="36.63" type="appl" />
         <tli id="T36" time="37.389" type="appl" />
         <tli id="T37" time="38.149" type="appl" />
         <tli id="T38" time="39.175" type="appl" />
         <tli id="T39" time="40.202" type="appl" />
         <tli id="T40" time="41.228" type="appl" />
         <tli id="T41" time="42.254" type="appl" />
         <tli id="T42" time="43.281" type="appl" />
         <tli id="T43" time="45.38494205712588" />
         <tli id="T44" time="45.929" type="appl" />
         <tli id="T45" time="46.493" type="appl" />
         <tli id="T46" time="47.058" type="appl" />
         <tli id="T47" time="47.623" type="appl" />
         <tli id="T48" time="48.187" type="appl" />
         <tli id="T49" time="48.752" type="appl" />
         <tli id="T50" time="49.317" type="appl" />
         <tli id="T51" time="49.881" type="appl" />
         <tli id="T52" time="50.99806209415584" />
         <tli id="T53" time="51.77" type="appl" />
         <tli id="T54" time="52.41" type="appl" />
         <tli id="T55" time="53.051" type="appl" />
         <tli id="T56" time="54.66458943425855" />
         <tli id="T57" time="55.849" type="appl" />
         <tli id="T58" time="56.973" type="appl" />
         <tli id="T59" time="58.098" type="appl" />
         <tli id="T60" time="59.222" type="appl" />
         <tli id="T61" time="60.346" type="appl" />
         <tli id="T62" time="61.077" type="appl" />
         <tli id="T63" time="61.809" type="appl" />
         <tli id="T64" time="62.54" type="appl" />
         <tli id="T65" time="63.94066272845494" />
         <tli id="T66" time="64.602" type="appl" />
         <tli id="T67" time="65.29" type="appl" />
         <tli id="T68" time="65.978" type="appl" />
         <tli id="T69" time="66.77746248328876" />
         <tli id="T70" time="68.003" type="appl" />
         <tli id="T71" time="68.965" type="appl" />
         <tli id="T72" time="69.927" type="appl" />
         <tli id="T73" time="70.889" type="appl" />
         <tli id="T74" time="71.851" type="appl" />
         <tli id="T75" time="72.813" type="appl" />
         <tli id="T76" time="73.775" type="appl" />
         <tli id="T77" time="74.737" type="appl" />
         <tli id="T78" time="75.699" type="appl" />
         <tli id="T79" time="77.0704046889589" />
         <tli id="T80" time="78.537" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T80" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nuʔməluʔpi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">dĭ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Ivanuška</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">bünə</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">döbər</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">dʼorbi</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_27" n="HIAT:u" s="T7">
                  <nts id="Seg_28" n="HIAT:ip">"</nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">Măna</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">băʔsittə</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">moləʔjə</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_40" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">Bü</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">mĭnzəlleʔbə</ts>
                  <nts id="Seg_46" n="HIAT:ip">,</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">măna</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">mĭnzərzittə</ts>
                  <nts id="Seg_53" n="HIAT:ip">"</nts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_57" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">dĭ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">măndə:</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_67" n="HIAT:ip">"</nts>
                  <nts id="Seg_68" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Măna=</ts>
                  <nts id="Seg_71" n="HIAT:ip">)</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Măna</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">pi</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">ulundə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">iʔbölaʔbə</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_87" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">Ej</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">moliam</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">uʔbsittə</ts>
                  <nts id="Seg_96" n="HIAT:ip">"</nts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_100" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">Dĭgəttə</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">šobi</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">bazoʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">măndə:</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_114" n="HIAT:ip">"</nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">Kallam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">dʼăganə</ts>
                  <nts id="Seg_120" n="HIAT:ip">"</nts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_124" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">Dĭgəttə</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">bazoʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">nuʔməluʔpi</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_136" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">Šobi</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">bazoʔ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_145" n="HIAT:w" s="T36">dʼorlaʔbə:</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">"</nts>
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">Supsaʔ</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_152" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">mă-</ts>
                  <nts id="Seg_155" n="HIAT:ip">)</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_158" n="HIAT:w" s="T39">măna</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T41" id="Seg_162" n="HIAT:w" s="T40">băts-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T41">bătləʔjə</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_170" n="HIAT:w" s="T42">mĭnzərleʔbəʔjə</ts>
                  <nts id="Seg_171" n="HIAT:ip">!</nts>
                  <nts id="Seg_172" n="HIAT:ip">"</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_175" n="HIAT:u" s="T43">
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T44" id="Seg_178" n="HIAT:w" s="T43">Dĭg-</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_182" n="HIAT:w" s="T44">Dĭ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_185" n="HIAT:w" s="T45">măndə:</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_187" n="HIAT:ip">"</nts>
                  <ts e="T47" id="Seg_189" n="HIAT:w" s="T46">Măna</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_192" n="HIAT:w" s="T47">pi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_195" n="HIAT:w" s="T48">dʼabolaʔbə</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_199" n="HIAT:w" s="T49">ej</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_202" n="HIAT:w" s="T50">moliam</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_205" n="HIAT:w" s="T51">uʔbsittə</ts>
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_210" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_212" n="HIAT:w" s="T52">Dĭgəttə</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">nagurgöʔ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_218" n="HIAT:w" s="T54">kalla</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">dʼürbi</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_225" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_227" n="HIAT:w" s="T56">Dĭgəttə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_230" n="HIAT:w" s="T57">dĭn</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_233" n="HIAT:w" s="T58">tibi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_235" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_237" n="HIAT:w" s="T59">kam-</ts>
                  <nts id="Seg_238" n="HIAT:ip">)</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_241" n="HIAT:w" s="T60">nuʔməluʔpi</ts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_245" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_247" n="HIAT:w" s="T61">Nüniet</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_249" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_251" n="HIAT:w" s="T62">tin-</ts>
                  <nts id="Seg_252" n="HIAT:ip">)</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_255" n="HIAT:w" s="T63">dĭn</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_259" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_261" n="HIAT:w" s="T64">Dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_265" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_267" n="HIAT:w" s="T65">Dĭgəttə</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_270" n="HIAT:w" s="T66">dĭm</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_273" n="HIAT:w" s="T67">supsolaʔ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_276" n="HIAT:w" s="T68">deʔpiʔi</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_280" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_282" n="HIAT:w" s="T69">A</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_285" n="HIAT:w" s="T70">baška</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_288" n="HIAT:w" s="T71">nem</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_291" n="HIAT:w" s="T72">kutlaːmbiʔi</ts>
                  <nts id="Seg_292" n="HIAT:ip">,</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_295" n="HIAT:w" s="T73">ineʔinə</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_298" n="HIAT:w" s="T74">sarbi</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_302" n="HIAT:w" s="T75">ineʔi</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_305" n="HIAT:w" s="T76">bar</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_308" n="HIAT:w" s="T77">dĭm</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_311" n="HIAT:w" s="T78">sajnʼeʔluʔpiʔi</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_315" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_317" n="HIAT:w" s="T79">Bar</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T80" id="Seg_320" n="sc" s="T0">
               <ts e="T1" id="Seg_322" n="e" s="T0">Dĭgəttə </ts>
               <ts e="T2" id="Seg_324" n="e" s="T1">nuʔməluʔpi </ts>
               <ts e="T3" id="Seg_326" n="e" s="T2">dĭ </ts>
               <ts e="T4" id="Seg_328" n="e" s="T3">Ivanuška </ts>
               <ts e="T5" id="Seg_330" n="e" s="T4">bünə, </ts>
               <ts e="T6" id="Seg_332" n="e" s="T5">döbər </ts>
               <ts e="T7" id="Seg_334" n="e" s="T6">dʼorbi. </ts>
               <ts e="T8" id="Seg_336" n="e" s="T7">"Măna </ts>
               <ts e="T9" id="Seg_338" n="e" s="T8">băʔsittə </ts>
               <ts e="T10" id="Seg_340" n="e" s="T9">moləʔjə. </ts>
               <ts e="T11" id="Seg_342" n="e" s="T10">Bü </ts>
               <ts e="T12" id="Seg_344" n="e" s="T11">mĭnzəlleʔbə, </ts>
               <ts e="T13" id="Seg_346" n="e" s="T12">măna </ts>
               <ts e="T14" id="Seg_348" n="e" s="T13">mĭnzərzittə". </ts>
               <ts e="T15" id="Seg_350" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_352" n="e" s="T15">dĭ </ts>
               <ts e="T17" id="Seg_354" n="e" s="T16">măndə: </ts>
               <ts e="T18" id="Seg_356" n="e" s="T17">"(Măna=) </ts>
               <ts e="T19" id="Seg_358" n="e" s="T18">Măna </ts>
               <ts e="T20" id="Seg_360" n="e" s="T19">pi </ts>
               <ts e="T21" id="Seg_362" n="e" s="T20">ulundə </ts>
               <ts e="T22" id="Seg_364" n="e" s="T21">iʔbölaʔbə. </ts>
               <ts e="T23" id="Seg_366" n="e" s="T22">Ej </ts>
               <ts e="T24" id="Seg_368" n="e" s="T23">moliam </ts>
               <ts e="T25" id="Seg_370" n="e" s="T24">uʔbsittə". </ts>
               <ts e="T26" id="Seg_372" n="e" s="T25">Dĭgəttə </ts>
               <ts e="T27" id="Seg_374" n="e" s="T26">šobi </ts>
               <ts e="T28" id="Seg_376" n="e" s="T27">bazoʔ, </ts>
               <ts e="T29" id="Seg_378" n="e" s="T28">măndə: </ts>
               <ts e="T30" id="Seg_380" n="e" s="T29">"Kallam </ts>
               <ts e="T31" id="Seg_382" n="e" s="T30">dʼăganə". </ts>
               <ts e="T32" id="Seg_384" n="e" s="T31">Dĭgəttə </ts>
               <ts e="T33" id="Seg_386" n="e" s="T32">bazoʔ </ts>
               <ts e="T34" id="Seg_388" n="e" s="T33">nuʔməluʔpi. </ts>
               <ts e="T35" id="Seg_390" n="e" s="T34">Šobi, </ts>
               <ts e="T36" id="Seg_392" n="e" s="T35">bazoʔ </ts>
               <ts e="T37" id="Seg_394" n="e" s="T36">dʼorlaʔbə: </ts>
               <ts e="T38" id="Seg_396" n="e" s="T37">"Supsaʔ, </ts>
               <ts e="T39" id="Seg_398" n="e" s="T38">(mă-) </ts>
               <ts e="T40" id="Seg_400" n="e" s="T39">măna </ts>
               <ts e="T41" id="Seg_402" n="e" s="T40">(băts-) </ts>
               <ts e="T42" id="Seg_404" n="e" s="T41">bătləʔjə, </ts>
               <ts e="T43" id="Seg_406" n="e" s="T42">mĭnzərleʔbəʔjə!" </ts>
               <ts e="T44" id="Seg_408" n="e" s="T43">(Dĭg-) </ts>
               <ts e="T45" id="Seg_410" n="e" s="T44">Dĭ </ts>
               <ts e="T46" id="Seg_412" n="e" s="T45">măndə: </ts>
               <ts e="T47" id="Seg_414" n="e" s="T46">"Măna </ts>
               <ts e="T48" id="Seg_416" n="e" s="T47">pi </ts>
               <ts e="T49" id="Seg_418" n="e" s="T48">dʼabolaʔbə, </ts>
               <ts e="T50" id="Seg_420" n="e" s="T49">ej </ts>
               <ts e="T51" id="Seg_422" n="e" s="T50">moliam </ts>
               <ts e="T52" id="Seg_424" n="e" s="T51">uʔbsittə". </ts>
               <ts e="T53" id="Seg_426" n="e" s="T52">Dĭgəttə </ts>
               <ts e="T54" id="Seg_428" n="e" s="T53">nagurgöʔ </ts>
               <ts e="T55" id="Seg_430" n="e" s="T54">kalla </ts>
               <ts e="T56" id="Seg_432" n="e" s="T55">dʼürbi. </ts>
               <ts e="T57" id="Seg_434" n="e" s="T56">Dĭgəttə </ts>
               <ts e="T58" id="Seg_436" n="e" s="T57">dĭn </ts>
               <ts e="T59" id="Seg_438" n="e" s="T58">tibi </ts>
               <ts e="T60" id="Seg_440" n="e" s="T59">(kam-) </ts>
               <ts e="T61" id="Seg_442" n="e" s="T60">nuʔməluʔpi. </ts>
               <ts e="T62" id="Seg_444" n="e" s="T61">Nüniet </ts>
               <ts e="T63" id="Seg_446" n="e" s="T62">(tin-) </ts>
               <ts e="T64" id="Seg_448" n="e" s="T63">dĭn. </ts>
               <ts e="T65" id="Seg_450" n="e" s="T64">Dʼăbaktərlaʔbə. </ts>
               <ts e="T66" id="Seg_452" n="e" s="T65">Dĭgəttə </ts>
               <ts e="T67" id="Seg_454" n="e" s="T66">dĭm </ts>
               <ts e="T68" id="Seg_456" n="e" s="T67">supsolaʔ </ts>
               <ts e="T69" id="Seg_458" n="e" s="T68">deʔpiʔi. </ts>
               <ts e="T70" id="Seg_460" n="e" s="T69">A </ts>
               <ts e="T71" id="Seg_462" n="e" s="T70">baška </ts>
               <ts e="T72" id="Seg_464" n="e" s="T71">nem </ts>
               <ts e="T73" id="Seg_466" n="e" s="T72">kutlaːmbiʔi, </ts>
               <ts e="T74" id="Seg_468" n="e" s="T73">ineʔinə </ts>
               <ts e="T75" id="Seg_470" n="e" s="T74">sarbi, </ts>
               <ts e="T76" id="Seg_472" n="e" s="T75">ineʔi </ts>
               <ts e="T77" id="Seg_474" n="e" s="T76">bar </ts>
               <ts e="T78" id="Seg_476" n="e" s="T77">dĭm </ts>
               <ts e="T79" id="Seg_478" n="e" s="T78">sajnʼeʔluʔpiʔi. </ts>
               <ts e="T80" id="Seg_480" n="e" s="T79">Bar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_481" s="T0">PKZ_196X_Alenushka_continuation_flk.001 (001)</ta>
            <ta e="T10" id="Seg_482" s="T7">PKZ_196X_Alenushka_continuation_flk.002 (002)</ta>
            <ta e="T14" id="Seg_483" s="T10">PKZ_196X_Alenushka_continuation_flk.003 (003)</ta>
            <ta e="T22" id="Seg_484" s="T14">PKZ_196X_Alenushka_continuation_flk.004 (004)</ta>
            <ta e="T25" id="Seg_485" s="T22">PKZ_196X_Alenushka_continuation_flk.005 (005)</ta>
            <ta e="T31" id="Seg_486" s="T25">PKZ_196X_Alenushka_continuation_flk.006 (006)</ta>
            <ta e="T34" id="Seg_487" s="T31">PKZ_196X_Alenushka_continuation_flk.007 (008)</ta>
            <ta e="T43" id="Seg_488" s="T34">PKZ_196X_Alenushka_continuation_flk.008 (009)</ta>
            <ta e="T52" id="Seg_489" s="T43">PKZ_196X_Alenushka_continuation_flk.009 (011)</ta>
            <ta e="T56" id="Seg_490" s="T52">PKZ_196X_Alenushka_continuation_flk.010 (012)</ta>
            <ta e="T61" id="Seg_491" s="T56">PKZ_196X_Alenushka_continuation_flk.011 (013)</ta>
            <ta e="T64" id="Seg_492" s="T61">PKZ_196X_Alenushka_continuation_flk.012 (014)</ta>
            <ta e="T65" id="Seg_493" s="T64">PKZ_196X_Alenushka_continuation_flk.013 (014)</ta>
            <ta e="T69" id="Seg_494" s="T65">PKZ_196X_Alenushka_continuation_flk.014 (016)</ta>
            <ta e="T79" id="Seg_495" s="T69">PKZ_196X_Alenushka_continuation_flk.015 (017)</ta>
            <ta e="T80" id="Seg_496" s="T79">PKZ_196X_Alenushka_continuation_flk.016 (018)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_497" s="T0">Dĭgəttə nuʔməluʔpi dĭ Ivanuška bünə, döbər dʼorbi. </ta>
            <ta e="T10" id="Seg_498" s="T7">"Măna băʔsittə moləʔjə. </ta>
            <ta e="T14" id="Seg_499" s="T10">Bü mĭnzəlleʔbə, măna mĭnzərzittə". </ta>
            <ta e="T22" id="Seg_500" s="T14">A dĭ măndə: "(Măna=) Măna pi ulundə iʔbölaʔbə. </ta>
            <ta e="T25" id="Seg_501" s="T22">Ej moliam uʔbsittə". </ta>
            <ta e="T31" id="Seg_502" s="T25">Dĭgəttə šobi bazoʔ, măndə: "Kallam dʼăganə". </ta>
            <ta e="T34" id="Seg_503" s="T31">Dĭgəttə bazoʔ nuʔməluʔpi. </ta>
            <ta e="T43" id="Seg_504" s="T34">Šobi, bazoʔ dʼorlaʔbə: "Supsaʔ, (mă-) măna (băts-) bătləʔjə, mĭnzərleʔbəʔjə!" </ta>
            <ta e="T52" id="Seg_505" s="T43">(Dĭg-) Dĭ măndə:" Măna pi dʼabolaʔbə, ej moliam uʔbsittə". </ta>
            <ta e="T56" id="Seg_506" s="T52">Dĭgəttə nagurgöʔ kalla dʼürbi. </ta>
            <ta e="T61" id="Seg_507" s="T56">Dĭgəttə dĭn tibi (kam-) nuʔməluʔpi. </ta>
            <ta e="T64" id="Seg_508" s="T61">Nüniet (tin-) dĭn. </ta>
            <ta e="T65" id="Seg_509" s="T64">Dʼăbaktərlaʔbə. </ta>
            <ta e="T69" id="Seg_510" s="T65">Dĭgəttə dĭm supsolaʔ deʔpiʔi. </ta>
            <ta e="T79" id="Seg_511" s="T69">A baška nem kutlaːmbiʔi, ineʔinə sarbi, ineʔi bar dĭm sajnʼeʔluʔpiʔi. </ta>
            <ta e="T80" id="Seg_512" s="T79">Bar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_513" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_514" s="T1">nuʔmə-luʔ-pi</ta>
            <ta e="T3" id="Seg_515" s="T2">dĭ</ta>
            <ta e="T4" id="Seg_516" s="T3">Ivanuška</ta>
            <ta e="T5" id="Seg_517" s="T4">bü-nə</ta>
            <ta e="T6" id="Seg_518" s="T5">döbər</ta>
            <ta e="T7" id="Seg_519" s="T6">dʼor-bi</ta>
            <ta e="T8" id="Seg_520" s="T7">măna</ta>
            <ta e="T9" id="Seg_521" s="T8">băʔ-sittə</ta>
            <ta e="T10" id="Seg_522" s="T9">mo-lə-ʔjə</ta>
            <ta e="T11" id="Seg_523" s="T10">bü</ta>
            <ta e="T12" id="Seg_524" s="T11">mĭnzəl-leʔbə</ta>
            <ta e="T13" id="Seg_525" s="T12">măna</ta>
            <ta e="T14" id="Seg_526" s="T13">mĭnzər-zittə</ta>
            <ta e="T15" id="Seg_527" s="T14">a</ta>
            <ta e="T16" id="Seg_528" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_529" s="T16">măn-də</ta>
            <ta e="T18" id="Seg_530" s="T17">măna</ta>
            <ta e="T19" id="Seg_531" s="T18">măna</ta>
            <ta e="T20" id="Seg_532" s="T19">pi</ta>
            <ta e="T21" id="Seg_533" s="T20">ulu-ndə</ta>
            <ta e="T22" id="Seg_534" s="T21">iʔbö-laʔbə</ta>
            <ta e="T23" id="Seg_535" s="T22">ej</ta>
            <ta e="T24" id="Seg_536" s="T23">mo-lia-m</ta>
            <ta e="T25" id="Seg_537" s="T24">uʔb-sittə</ta>
            <ta e="T26" id="Seg_538" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_539" s="T26">šo-bi</ta>
            <ta e="T28" id="Seg_540" s="T27">bazoʔ</ta>
            <ta e="T29" id="Seg_541" s="T28">măn-də</ta>
            <ta e="T30" id="Seg_542" s="T29">kal-la-m</ta>
            <ta e="T31" id="Seg_543" s="T30">dʼăga-nə</ta>
            <ta e="T32" id="Seg_544" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_545" s="T32">bazoʔ</ta>
            <ta e="T34" id="Seg_546" s="T33">nuʔmə-luʔ-pi</ta>
            <ta e="T35" id="Seg_547" s="T34">šo-bi</ta>
            <ta e="T36" id="Seg_548" s="T35">bazoʔ</ta>
            <ta e="T37" id="Seg_549" s="T36">dʼor-laʔbə</ta>
            <ta e="T38" id="Seg_550" s="T37">sups-aʔ</ta>
            <ta e="T40" id="Seg_551" s="T39">măna</ta>
            <ta e="T42" id="Seg_552" s="T41">băt-lə-ʔjə</ta>
            <ta e="T43" id="Seg_553" s="T42">mĭnzər-leʔbə-ʔjə</ta>
            <ta e="T45" id="Seg_554" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_555" s="T45">măn-də</ta>
            <ta e="T47" id="Seg_556" s="T46">măna</ta>
            <ta e="T48" id="Seg_557" s="T47">pi</ta>
            <ta e="T49" id="Seg_558" s="T48">dʼabo-laʔbə</ta>
            <ta e="T50" id="Seg_559" s="T49">ej</ta>
            <ta e="T51" id="Seg_560" s="T50">mo-lia-m</ta>
            <ta e="T52" id="Seg_561" s="T51">uʔb-sittə</ta>
            <ta e="T53" id="Seg_562" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_563" s="T53">nagur-göʔ</ta>
            <ta e="T55" id="Seg_564" s="T54">kal-la</ta>
            <ta e="T56" id="Seg_565" s="T55">dʼür-bi</ta>
            <ta e="T57" id="Seg_566" s="T56">dĭgəttə</ta>
            <ta e="T58" id="Seg_567" s="T57">dĭn</ta>
            <ta e="T59" id="Seg_568" s="T58">tibi</ta>
            <ta e="T61" id="Seg_569" s="T60">nuʔmə-luʔ-pi</ta>
            <ta e="T62" id="Seg_570" s="T61">nün-ie-t</ta>
            <ta e="T64" id="Seg_571" s="T63">dĭn</ta>
            <ta e="T65" id="Seg_572" s="T64">dʼăbaktər-laʔbə</ta>
            <ta e="T66" id="Seg_573" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_574" s="T66">dĭ-m</ta>
            <ta e="T68" id="Seg_575" s="T67">supso-laʔ</ta>
            <ta e="T69" id="Seg_576" s="T68">deʔ-pi-ʔi</ta>
            <ta e="T70" id="Seg_577" s="T69">a</ta>
            <ta e="T71" id="Seg_578" s="T70">baška</ta>
            <ta e="T72" id="Seg_579" s="T71">ne-m</ta>
            <ta e="T73" id="Seg_580" s="T72">kut-laːm-bi-ʔi</ta>
            <ta e="T74" id="Seg_581" s="T73">ine-ʔi-nə</ta>
            <ta e="T75" id="Seg_582" s="T74">sar-bi</ta>
            <ta e="T76" id="Seg_583" s="T75">ine-ʔi</ta>
            <ta e="T77" id="Seg_584" s="T76">bar</ta>
            <ta e="T78" id="Seg_585" s="T77">dĭ-m</ta>
            <ta e="T79" id="Seg_586" s="T78">saj-nʼeʔ-luʔ-pi-ʔi</ta>
            <ta e="T80" id="Seg_587" s="T79">bar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_588" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_589" s="T1">nuʔmə-luʔbdə-bi</ta>
            <ta e="T3" id="Seg_590" s="T2">dĭ</ta>
            <ta e="T4" id="Seg_591" s="T3">Ivanuška</ta>
            <ta e="T5" id="Seg_592" s="T4">bü-Tə</ta>
            <ta e="T6" id="Seg_593" s="T5">döbər</ta>
            <ta e="T7" id="Seg_594" s="T6">tʼor-bi</ta>
            <ta e="T8" id="Seg_595" s="T7">măna</ta>
            <ta e="T9" id="Seg_596" s="T8">băt-zittə</ta>
            <ta e="T10" id="Seg_597" s="T9">mo-lV-jəʔ</ta>
            <ta e="T11" id="Seg_598" s="T10">bü</ta>
            <ta e="T12" id="Seg_599" s="T11">mĭnzəl-laʔbə</ta>
            <ta e="T13" id="Seg_600" s="T12">măna</ta>
            <ta e="T14" id="Seg_601" s="T13">mĭnzər-zittə</ta>
            <ta e="T15" id="Seg_602" s="T14">a</ta>
            <ta e="T16" id="Seg_603" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_604" s="T16">măn-ntə</ta>
            <ta e="T18" id="Seg_605" s="T17">măna</ta>
            <ta e="T19" id="Seg_606" s="T18">măna</ta>
            <ta e="T20" id="Seg_607" s="T19">pi</ta>
            <ta e="T21" id="Seg_608" s="T20">ulu-gəndə</ta>
            <ta e="T22" id="Seg_609" s="T21">iʔbö-laʔbə</ta>
            <ta e="T23" id="Seg_610" s="T22">ej</ta>
            <ta e="T24" id="Seg_611" s="T23">mo-liA-m</ta>
            <ta e="T25" id="Seg_612" s="T24">uʔbdə-zittə</ta>
            <ta e="T26" id="Seg_613" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_614" s="T26">šo-bi</ta>
            <ta e="T28" id="Seg_615" s="T27">bazoʔ</ta>
            <ta e="T29" id="Seg_616" s="T28">măn-ntə</ta>
            <ta e="T30" id="Seg_617" s="T29">kan-lV-m</ta>
            <ta e="T31" id="Seg_618" s="T30">tʼăga-Tə</ta>
            <ta e="T32" id="Seg_619" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_620" s="T32">bazoʔ</ta>
            <ta e="T34" id="Seg_621" s="T33">nuʔmə-luʔbdə-bi</ta>
            <ta e="T35" id="Seg_622" s="T34">šo-bi</ta>
            <ta e="T36" id="Seg_623" s="T35">bazoʔ</ta>
            <ta e="T37" id="Seg_624" s="T36">tʼor-laʔbə</ta>
            <ta e="T38" id="Seg_625" s="T37">supso-ʔ</ta>
            <ta e="T40" id="Seg_626" s="T39">măna</ta>
            <ta e="T42" id="Seg_627" s="T41">băt-lV-jəʔ</ta>
            <ta e="T43" id="Seg_628" s="T42">mĭnzər-laʔbə-jəʔ</ta>
            <ta e="T45" id="Seg_629" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_630" s="T45">măn-ntə</ta>
            <ta e="T47" id="Seg_631" s="T46">măna</ta>
            <ta e="T48" id="Seg_632" s="T47">pi</ta>
            <ta e="T49" id="Seg_633" s="T48">dʼabə-laʔbə</ta>
            <ta e="T50" id="Seg_634" s="T49">ej</ta>
            <ta e="T51" id="Seg_635" s="T50">mo-liA-m</ta>
            <ta e="T52" id="Seg_636" s="T51">uʔbdə-zittə</ta>
            <ta e="T53" id="Seg_637" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_638" s="T53">nagur-göʔ</ta>
            <ta e="T55" id="Seg_639" s="T54">kan-lAʔ</ta>
            <ta e="T56" id="Seg_640" s="T55">tʼür-bi</ta>
            <ta e="T57" id="Seg_641" s="T56">dĭgəttə</ta>
            <ta e="T58" id="Seg_642" s="T57">dĭn</ta>
            <ta e="T59" id="Seg_643" s="T58">tibi</ta>
            <ta e="T61" id="Seg_644" s="T60">nuʔmə-luʔbdə-bi</ta>
            <ta e="T62" id="Seg_645" s="T61">nünə-liA-t</ta>
            <ta e="T64" id="Seg_646" s="T63">dĭn</ta>
            <ta e="T65" id="Seg_647" s="T64">tʼăbaktər-laʔbə</ta>
            <ta e="T66" id="Seg_648" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_649" s="T66">dĭ-m</ta>
            <ta e="T68" id="Seg_650" s="T67">supso-lAʔ</ta>
            <ta e="T69" id="Seg_651" s="T68">det-bi-jəʔ</ta>
            <ta e="T70" id="Seg_652" s="T69">a</ta>
            <ta e="T71" id="Seg_653" s="T70">baška</ta>
            <ta e="T72" id="Seg_654" s="T71">ne-m</ta>
            <ta e="T73" id="Seg_655" s="T72">kut-laːm-bi-jəʔ</ta>
            <ta e="T74" id="Seg_656" s="T73">ine-jəʔ-Tə</ta>
            <ta e="T75" id="Seg_657" s="T74">sar-bi</ta>
            <ta e="T76" id="Seg_658" s="T75">ine-jəʔ</ta>
            <ta e="T77" id="Seg_659" s="T76">bar</ta>
            <ta e="T78" id="Seg_660" s="T77">dĭ-m</ta>
            <ta e="T79" id="Seg_661" s="T78">săj-nʼeʔbdə-luʔbdə-bi-jəʔ</ta>
            <ta e="T80" id="Seg_662" s="T79">bar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_663" s="T0">then</ta>
            <ta e="T2" id="Seg_664" s="T1">run-MOM-PST.[3SG]</ta>
            <ta e="T3" id="Seg_665" s="T2">this.[NOM.SG]</ta>
            <ta e="T4" id="Seg_666" s="T3">Ivanushka.[NOM.SG]</ta>
            <ta e="T5" id="Seg_667" s="T4">water-LAT</ta>
            <ta e="T6" id="Seg_668" s="T5">here</ta>
            <ta e="T7" id="Seg_669" s="T6">cry-PST.[3SG]</ta>
            <ta e="T8" id="Seg_670" s="T7">I.ACC</ta>
            <ta e="T9" id="Seg_671" s="T8">cut-INF.LAT</ta>
            <ta e="T10" id="Seg_672" s="T9">want-FUT-3PL</ta>
            <ta e="T11" id="Seg_673" s="T10">water.[NOM.SG]</ta>
            <ta e="T12" id="Seg_674" s="T11">boil-DUR.[3SG]</ta>
            <ta e="T13" id="Seg_675" s="T12">I.ACC</ta>
            <ta e="T14" id="Seg_676" s="T13">boil-INF.LAT</ta>
            <ta e="T15" id="Seg_677" s="T14">and</ta>
            <ta e="T16" id="Seg_678" s="T15">this.[NOM.SG]</ta>
            <ta e="T17" id="Seg_679" s="T16">say-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_680" s="T17">I.LAT</ta>
            <ta e="T19" id="Seg_681" s="T18">I.LAT</ta>
            <ta e="T20" id="Seg_682" s="T19">stone.[NOM.SG]</ta>
            <ta e="T21" id="Seg_683" s="T20">head-LAT/LOC.3SG</ta>
            <ta e="T22" id="Seg_684" s="T21">lie-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_685" s="T22">NEG</ta>
            <ta e="T24" id="Seg_686" s="T23">can-PRS-1SG</ta>
            <ta e="T25" id="Seg_687" s="T24">get.up-INF.LAT</ta>
            <ta e="T26" id="Seg_688" s="T25">then</ta>
            <ta e="T27" id="Seg_689" s="T26">come-PST.[3SG]</ta>
            <ta e="T28" id="Seg_690" s="T27">again</ta>
            <ta e="T29" id="Seg_691" s="T28">say-IPFVZ.[3SG]</ta>
            <ta e="T30" id="Seg_692" s="T29">go-FUT-1SG</ta>
            <ta e="T31" id="Seg_693" s="T30">river-LAT</ta>
            <ta e="T32" id="Seg_694" s="T31">then</ta>
            <ta e="T33" id="Seg_695" s="T32">again</ta>
            <ta e="T34" id="Seg_696" s="T33">run-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_697" s="T34">come-PST.[3SG]</ta>
            <ta e="T36" id="Seg_698" s="T35">again</ta>
            <ta e="T37" id="Seg_699" s="T36">cry-DUR.[3SG]</ta>
            <ta e="T38" id="Seg_700" s="T37">depart-IMP.2SG</ta>
            <ta e="T40" id="Seg_701" s="T39">I.ACC</ta>
            <ta e="T42" id="Seg_702" s="T41">cut-FUT-3PL</ta>
            <ta e="T43" id="Seg_703" s="T42">boil-DUR-3PL</ta>
            <ta e="T45" id="Seg_704" s="T44">this</ta>
            <ta e="T46" id="Seg_705" s="T45">say-IPFVZ.[3SG]</ta>
            <ta e="T47" id="Seg_706" s="T46">I.ACC</ta>
            <ta e="T48" id="Seg_707" s="T47">stone.[NOM.SG]</ta>
            <ta e="T49" id="Seg_708" s="T48">capture-DUR.[3SG]</ta>
            <ta e="T50" id="Seg_709" s="T49">NEG</ta>
            <ta e="T51" id="Seg_710" s="T50">can-PRS-1SG</ta>
            <ta e="T52" id="Seg_711" s="T51">get.up-INF.LAT</ta>
            <ta e="T53" id="Seg_712" s="T52">then</ta>
            <ta e="T54" id="Seg_713" s="T53">three-COLL</ta>
            <ta e="T55" id="Seg_714" s="T54">go-CVB</ta>
            <ta e="T56" id="Seg_715" s="T55">disappear-PST.[3SG]</ta>
            <ta e="T57" id="Seg_716" s="T56">then</ta>
            <ta e="T58" id="Seg_717" s="T57">there</ta>
            <ta e="T59" id="Seg_718" s="T58">man.[NOM.SG]</ta>
            <ta e="T61" id="Seg_719" s="T60">run-MOM-PST.[3SG]</ta>
            <ta e="T62" id="Seg_720" s="T61">hear-PRS-3SG.O</ta>
            <ta e="T64" id="Seg_721" s="T63">there</ta>
            <ta e="T65" id="Seg_722" s="T64">speak-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_723" s="T65">then</ta>
            <ta e="T67" id="Seg_724" s="T66">this-ACC</ta>
            <ta e="T68" id="Seg_725" s="T67">depart-CVB</ta>
            <ta e="T69" id="Seg_726" s="T68">bring-PST-3PL</ta>
            <ta e="T70" id="Seg_727" s="T69">and</ta>
            <ta e="T71" id="Seg_728" s="T70">another.[NOM.SG]</ta>
            <ta e="T72" id="Seg_729" s="T71">woman-ACC</ta>
            <ta e="T73" id="Seg_730" s="T72">kill-RES-PST-3PL</ta>
            <ta e="T74" id="Seg_731" s="T73">horse-3PL-LAT</ta>
            <ta e="T75" id="Seg_732" s="T74">bind-PST.[3SG]</ta>
            <ta e="T76" id="Seg_733" s="T75">horse-PL</ta>
            <ta e="T77" id="Seg_734" s="T76">PTCL</ta>
            <ta e="T78" id="Seg_735" s="T77">this-ACC</ta>
            <ta e="T79" id="Seg_736" s="T78">off-pull-MOM-PST-3PL</ta>
            <ta e="T80" id="Seg_737" s="T79">PTCL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_738" s="T0">тогда</ta>
            <ta e="T2" id="Seg_739" s="T1">бежать-MOM-PST.[3SG]</ta>
            <ta e="T3" id="Seg_740" s="T2">этот.[NOM.SG]</ta>
            <ta e="T4" id="Seg_741" s="T3">Иванушка.[NOM.SG]</ta>
            <ta e="T5" id="Seg_742" s="T4">вода-LAT</ta>
            <ta e="T6" id="Seg_743" s="T5">здесь</ta>
            <ta e="T7" id="Seg_744" s="T6">плакать-PST.[3SG]</ta>
            <ta e="T8" id="Seg_745" s="T7">я.ACC</ta>
            <ta e="T9" id="Seg_746" s="T8">резать-INF.LAT</ta>
            <ta e="T10" id="Seg_747" s="T9">хотеть-FUT-3PL</ta>
            <ta e="T11" id="Seg_748" s="T10">вода.[NOM.SG]</ta>
            <ta e="T12" id="Seg_749" s="T11">кипеть-DUR.[3SG]</ta>
            <ta e="T13" id="Seg_750" s="T12">я.ACC</ta>
            <ta e="T14" id="Seg_751" s="T13">кипятить-INF.LAT</ta>
            <ta e="T15" id="Seg_752" s="T14">а</ta>
            <ta e="T16" id="Seg_753" s="T15">этот.[NOM.SG]</ta>
            <ta e="T17" id="Seg_754" s="T16">сказать-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_755" s="T17">я.LAT</ta>
            <ta e="T19" id="Seg_756" s="T18">я.LAT</ta>
            <ta e="T20" id="Seg_757" s="T19">камень.[NOM.SG]</ta>
            <ta e="T21" id="Seg_758" s="T20">голова-LAT/LOC.3SG</ta>
            <ta e="T22" id="Seg_759" s="T21">лежать-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_760" s="T22">NEG</ta>
            <ta e="T24" id="Seg_761" s="T23">мочь-PRS-1SG</ta>
            <ta e="T25" id="Seg_762" s="T24">встать-INF.LAT</ta>
            <ta e="T26" id="Seg_763" s="T25">тогда</ta>
            <ta e="T27" id="Seg_764" s="T26">прийти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_765" s="T27">опять</ta>
            <ta e="T29" id="Seg_766" s="T28">сказать-IPFVZ.[3SG]</ta>
            <ta e="T30" id="Seg_767" s="T29">пойти-FUT-1SG</ta>
            <ta e="T31" id="Seg_768" s="T30">река-LAT</ta>
            <ta e="T32" id="Seg_769" s="T31">тогда</ta>
            <ta e="T33" id="Seg_770" s="T32">опять</ta>
            <ta e="T34" id="Seg_771" s="T33">бежать-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_772" s="T34">прийти-PST.[3SG]</ta>
            <ta e="T36" id="Seg_773" s="T35">опять</ta>
            <ta e="T37" id="Seg_774" s="T36">плакать-DUR.[3SG]</ta>
            <ta e="T38" id="Seg_775" s="T37">уйти-IMP.2SG</ta>
            <ta e="T40" id="Seg_776" s="T39">я.ACC</ta>
            <ta e="T42" id="Seg_777" s="T41">резать-FUT-3PL</ta>
            <ta e="T43" id="Seg_778" s="T42">кипятить-DUR-3PL</ta>
            <ta e="T45" id="Seg_779" s="T44">этот</ta>
            <ta e="T46" id="Seg_780" s="T45">сказать-IPFVZ.[3SG]</ta>
            <ta e="T47" id="Seg_781" s="T46">я.ACC</ta>
            <ta e="T48" id="Seg_782" s="T47">камень.[NOM.SG]</ta>
            <ta e="T49" id="Seg_783" s="T48">ловить-DUR.[3SG]</ta>
            <ta e="T50" id="Seg_784" s="T49">NEG</ta>
            <ta e="T51" id="Seg_785" s="T50">мочь-PRS-1SG</ta>
            <ta e="T52" id="Seg_786" s="T51">встать-INF.LAT</ta>
            <ta e="T53" id="Seg_787" s="T52">тогда</ta>
            <ta e="T54" id="Seg_788" s="T53">три-COLL</ta>
            <ta e="T55" id="Seg_789" s="T54">пойти-CVB</ta>
            <ta e="T56" id="Seg_790" s="T55">исчезнуть-PST.[3SG]</ta>
            <ta e="T57" id="Seg_791" s="T56">тогда</ta>
            <ta e="T58" id="Seg_792" s="T57">там</ta>
            <ta e="T59" id="Seg_793" s="T58">мужчина.[NOM.SG]</ta>
            <ta e="T61" id="Seg_794" s="T60">бежать-MOM-PST.[3SG]</ta>
            <ta e="T62" id="Seg_795" s="T61">слышать-PRS-3SG.O</ta>
            <ta e="T64" id="Seg_796" s="T63">там</ta>
            <ta e="T65" id="Seg_797" s="T64">говорить-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_798" s="T65">тогда</ta>
            <ta e="T67" id="Seg_799" s="T66">этот-ACC</ta>
            <ta e="T68" id="Seg_800" s="T67">уйти-CVB</ta>
            <ta e="T69" id="Seg_801" s="T68">принести-PST-3PL</ta>
            <ta e="T70" id="Seg_802" s="T69">а</ta>
            <ta e="T71" id="Seg_803" s="T70">другой.[NOM.SG]</ta>
            <ta e="T72" id="Seg_804" s="T71">женщина-ACC</ta>
            <ta e="T73" id="Seg_805" s="T72">убить-RES-PST-3PL</ta>
            <ta e="T74" id="Seg_806" s="T73">лошадь-3PL-LAT</ta>
            <ta e="T75" id="Seg_807" s="T74">завязать-PST.[3SG]</ta>
            <ta e="T76" id="Seg_808" s="T75">лошадь-PL</ta>
            <ta e="T77" id="Seg_809" s="T76">PTCL</ta>
            <ta e="T78" id="Seg_810" s="T77">этот-ACC</ta>
            <ta e="T79" id="Seg_811" s="T78">от-тянуть-MOM-PST-3PL</ta>
            <ta e="T80" id="Seg_812" s="T79">PTCL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_813" s="T0">adv</ta>
            <ta e="T2" id="Seg_814" s="T1">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_815" s="T2">dempro-n:case</ta>
            <ta e="T4" id="Seg_816" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_817" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_818" s="T5">adv</ta>
            <ta e="T7" id="Seg_819" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_820" s="T7">pers</ta>
            <ta e="T9" id="Seg_821" s="T8">v-v:n.fin</ta>
            <ta e="T10" id="Seg_822" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_823" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_824" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_825" s="T12">pers</ta>
            <ta e="T14" id="Seg_826" s="T13">v-v:n.fin</ta>
            <ta e="T15" id="Seg_827" s="T14">conj</ta>
            <ta e="T16" id="Seg_828" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_829" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_830" s="T17">pers</ta>
            <ta e="T19" id="Seg_831" s="T18">pers</ta>
            <ta e="T20" id="Seg_832" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_833" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_834" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_835" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_836" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_837" s="T24">v-v:n.fin</ta>
            <ta e="T26" id="Seg_838" s="T25">adv</ta>
            <ta e="T27" id="Seg_839" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_840" s="T27">adv</ta>
            <ta e="T29" id="Seg_841" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_842" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_843" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_844" s="T31">adv</ta>
            <ta e="T33" id="Seg_845" s="T32">adv</ta>
            <ta e="T34" id="Seg_846" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_847" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_848" s="T35">adv</ta>
            <ta e="T37" id="Seg_849" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_850" s="T37">v-v:mood.pn</ta>
            <ta e="T40" id="Seg_851" s="T39">pers</ta>
            <ta e="T42" id="Seg_852" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_853" s="T42">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_854" s="T44">dempro</ta>
            <ta e="T46" id="Seg_855" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_856" s="T46">pers</ta>
            <ta e="T48" id="Seg_857" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_858" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_859" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_860" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_861" s="T51">v-v:n.fin</ta>
            <ta e="T53" id="Seg_862" s="T52">adv</ta>
            <ta e="T54" id="Seg_863" s="T53">num-num&gt;num</ta>
            <ta e="T55" id="Seg_864" s="T54">v-v:n.fin</ta>
            <ta e="T56" id="Seg_865" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_866" s="T56">adv</ta>
            <ta e="T58" id="Seg_867" s="T57">adv</ta>
            <ta e="T59" id="Seg_868" s="T58">n-n:case</ta>
            <ta e="T61" id="Seg_869" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_870" s="T61">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_871" s="T63">adv</ta>
            <ta e="T65" id="Seg_872" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_873" s="T65">adv</ta>
            <ta e="T67" id="Seg_874" s="T66">dempro-n:case</ta>
            <ta e="T68" id="Seg_875" s="T67">v-v:n.fin</ta>
            <ta e="T69" id="Seg_876" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_877" s="T69">conj</ta>
            <ta e="T71" id="Seg_878" s="T70">adj-n:case</ta>
            <ta e="T72" id="Seg_879" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_880" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_881" s="T73">n-n:case.poss-n:case</ta>
            <ta e="T75" id="Seg_882" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_883" s="T75">n-n:num</ta>
            <ta e="T77" id="Seg_884" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_885" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_886" s="T78">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_887" s="T79">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_888" s="T0">adv</ta>
            <ta e="T2" id="Seg_889" s="T1">v</ta>
            <ta e="T3" id="Seg_890" s="T2">dempro</ta>
            <ta e="T4" id="Seg_891" s="T3">propr</ta>
            <ta e="T5" id="Seg_892" s="T4">n</ta>
            <ta e="T6" id="Seg_893" s="T5">adv</ta>
            <ta e="T7" id="Seg_894" s="T6">v</ta>
            <ta e="T8" id="Seg_895" s="T7">pers</ta>
            <ta e="T9" id="Seg_896" s="T8">v</ta>
            <ta e="T10" id="Seg_897" s="T9">v</ta>
            <ta e="T11" id="Seg_898" s="T10">n</ta>
            <ta e="T12" id="Seg_899" s="T11">v</ta>
            <ta e="T13" id="Seg_900" s="T12">pers</ta>
            <ta e="T14" id="Seg_901" s="T13">v</ta>
            <ta e="T15" id="Seg_902" s="T14">conj</ta>
            <ta e="T16" id="Seg_903" s="T15">dempro</ta>
            <ta e="T17" id="Seg_904" s="T16">v</ta>
            <ta e="T18" id="Seg_905" s="T17">pers</ta>
            <ta e="T19" id="Seg_906" s="T18">pers</ta>
            <ta e="T20" id="Seg_907" s="T19">n</ta>
            <ta e="T21" id="Seg_908" s="T20">n</ta>
            <ta e="T22" id="Seg_909" s="T21">v</ta>
            <ta e="T23" id="Seg_910" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_911" s="T23">v</ta>
            <ta e="T25" id="Seg_912" s="T24">v</ta>
            <ta e="T26" id="Seg_913" s="T25">adv</ta>
            <ta e="T27" id="Seg_914" s="T26">v</ta>
            <ta e="T28" id="Seg_915" s="T27">adv</ta>
            <ta e="T29" id="Seg_916" s="T28">v</ta>
            <ta e="T30" id="Seg_917" s="T29">v</ta>
            <ta e="T31" id="Seg_918" s="T30">n</ta>
            <ta e="T32" id="Seg_919" s="T31">adv</ta>
            <ta e="T33" id="Seg_920" s="T32">adv</ta>
            <ta e="T34" id="Seg_921" s="T33">v</ta>
            <ta e="T35" id="Seg_922" s="T34">v</ta>
            <ta e="T36" id="Seg_923" s="T35">adv</ta>
            <ta e="T37" id="Seg_924" s="T36">v</ta>
            <ta e="T38" id="Seg_925" s="T37">v</ta>
            <ta e="T40" id="Seg_926" s="T39">pers</ta>
            <ta e="T42" id="Seg_927" s="T41">v</ta>
            <ta e="T43" id="Seg_928" s="T42">v</ta>
            <ta e="T45" id="Seg_929" s="T44">dempro</ta>
            <ta e="T46" id="Seg_930" s="T45">v</ta>
            <ta e="T47" id="Seg_931" s="T46">pers</ta>
            <ta e="T48" id="Seg_932" s="T47">n</ta>
            <ta e="T49" id="Seg_933" s="T48">v</ta>
            <ta e="T50" id="Seg_934" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_935" s="T50">v</ta>
            <ta e="T52" id="Seg_936" s="T51">v</ta>
            <ta e="T53" id="Seg_937" s="T52">adv</ta>
            <ta e="T54" id="Seg_938" s="T53">num</ta>
            <ta e="T55" id="Seg_939" s="T54">v</ta>
            <ta e="T56" id="Seg_940" s="T55">v</ta>
            <ta e="T57" id="Seg_941" s="T56">adv</ta>
            <ta e="T58" id="Seg_942" s="T57">adv</ta>
            <ta e="T59" id="Seg_943" s="T58">n</ta>
            <ta e="T61" id="Seg_944" s="T60">v</ta>
            <ta e="T62" id="Seg_945" s="T61">v</ta>
            <ta e="T64" id="Seg_946" s="T63">adv</ta>
            <ta e="T65" id="Seg_947" s="T64">v</ta>
            <ta e="T66" id="Seg_948" s="T65">adv</ta>
            <ta e="T67" id="Seg_949" s="T66">dempro</ta>
            <ta e="T68" id="Seg_950" s="T67">v</ta>
            <ta e="T69" id="Seg_951" s="T68">v</ta>
            <ta e="T70" id="Seg_952" s="T69">conj</ta>
            <ta e="T71" id="Seg_953" s="T70">adj</ta>
            <ta e="T72" id="Seg_954" s="T71">n</ta>
            <ta e="T73" id="Seg_955" s="T72">v</ta>
            <ta e="T74" id="Seg_956" s="T73">n</ta>
            <ta e="T75" id="Seg_957" s="T74">v</ta>
            <ta e="T76" id="Seg_958" s="T75">n</ta>
            <ta e="T77" id="Seg_959" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_960" s="T77">dempro</ta>
            <ta e="T79" id="Seg_961" s="T78">v</ta>
            <ta e="T80" id="Seg_962" s="T79">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_963" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_964" s="T3">np.h:S</ta>
            <ta e="T7" id="Seg_965" s="T6">0.3.h:S v:pred</ta>
            <ta e="T8" id="Seg_966" s="T7">pro.h:O</ta>
            <ta e="T10" id="Seg_967" s="T9">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_968" s="T10">np:S</ta>
            <ta e="T12" id="Seg_969" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_970" s="T12">s:purp</ta>
            <ta e="T16" id="Seg_971" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_972" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_973" s="T19">np:S</ta>
            <ta e="T22" id="Seg_974" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_975" s="T22">ptcl.neg</ta>
            <ta e="T24" id="Seg_976" s="T23">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_977" s="T26">0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_978" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_979" s="T29">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_980" s="T33">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_981" s="T34">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_982" s="T36">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_983" s="T37">0.2.h:S v:pred</ta>
            <ta e="T40" id="Seg_984" s="T39">pro.h:O</ta>
            <ta e="T42" id="Seg_985" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_986" s="T42">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_987" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_988" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_989" s="T46">pro.h:O</ta>
            <ta e="T48" id="Seg_990" s="T47">np:S</ta>
            <ta e="T49" id="Seg_991" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_992" s="T49">ptcl.neg</ta>
            <ta e="T51" id="Seg_993" s="T50">0.1.h:S v:pred</ta>
            <ta e="T55" id="Seg_994" s="T54">conv:pred</ta>
            <ta e="T56" id="Seg_995" s="T55">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_996" s="T58">np.h:S</ta>
            <ta e="T61" id="Seg_997" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_998" s="T61">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T65" id="Seg_999" s="T64">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_1000" s="T66">pro.h:O</ta>
            <ta e="T68" id="Seg_1001" s="T67">conv:pred</ta>
            <ta e="T69" id="Seg_1002" s="T68">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_1003" s="T71">np.h:O</ta>
            <ta e="T73" id="Seg_1004" s="T72">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_1005" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_1006" s="T75">np:S</ta>
            <ta e="T78" id="Seg_1007" s="T77">pro:O</ta>
            <ta e="T79" id="Seg_1008" s="T78">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1009" s="T0">adv:Time</ta>
            <ta e="T4" id="Seg_1010" s="T3">np.h:A</ta>
            <ta e="T5" id="Seg_1011" s="T4">np:G</ta>
            <ta e="T6" id="Seg_1012" s="T5">adv:L</ta>
            <ta e="T7" id="Seg_1013" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_1014" s="T7">pro.h:P</ta>
            <ta e="T10" id="Seg_1015" s="T9">0.3.h:A</ta>
            <ta e="T11" id="Seg_1016" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_1017" s="T12">pro.h:P</ta>
            <ta e="T16" id="Seg_1018" s="T15">pro.h:A</ta>
            <ta e="T19" id="Seg_1019" s="T18">pro.h:Poss</ta>
            <ta e="T20" id="Seg_1020" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_1021" s="T20">np:L</ta>
            <ta e="T24" id="Seg_1022" s="T23">0.1.h:A</ta>
            <ta e="T26" id="Seg_1023" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_1024" s="T26">0.3.h:A</ta>
            <ta e="T29" id="Seg_1025" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_1026" s="T29">0.1.h:A</ta>
            <ta e="T31" id="Seg_1027" s="T30">np:G</ta>
            <ta e="T32" id="Seg_1028" s="T31">adv:Time</ta>
            <ta e="T34" id="Seg_1029" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_1030" s="T34">0.3.h:A</ta>
            <ta e="T37" id="Seg_1031" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_1032" s="T37">0.2.h:A</ta>
            <ta e="T40" id="Seg_1033" s="T39">pro.h:P</ta>
            <ta e="T42" id="Seg_1034" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_1035" s="T42">0.3.h:A</ta>
            <ta e="T45" id="Seg_1036" s="T44">pro.h:A</ta>
            <ta e="T47" id="Seg_1037" s="T46">pro.h:E</ta>
            <ta e="T48" id="Seg_1038" s="T47">np:Th</ta>
            <ta e="T51" id="Seg_1039" s="T50">0.1.h:A</ta>
            <ta e="T53" id="Seg_1040" s="T52">adv:Time</ta>
            <ta e="T56" id="Seg_1041" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_1042" s="T56">adv:Time</ta>
            <ta e="T59" id="Seg_1043" s="T58">np.h:A</ta>
            <ta e="T62" id="Seg_1044" s="T61">0.3.h:E</ta>
            <ta e="T64" id="Seg_1045" s="T63">adv:L</ta>
            <ta e="T65" id="Seg_1046" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_1047" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_1048" s="T66">pro.h:Th</ta>
            <ta e="T69" id="Seg_1049" s="T68">0.3.h:A</ta>
            <ta e="T72" id="Seg_1050" s="T71">np.h:P</ta>
            <ta e="T73" id="Seg_1051" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_1052" s="T73">np:L</ta>
            <ta e="T75" id="Seg_1053" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_1054" s="T75">np:A</ta>
            <ta e="T78" id="Seg_1055" s="T77">pro.h:P</ta>
            <ta e="T79" id="Seg_1056" s="T78">0.3:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T15" id="Seg_1057" s="T14">RUS:gram</ta>
            <ta e="T65" id="Seg_1058" s="T64">%TURK:core</ta>
            <ta e="T70" id="Seg_1059" s="T69">RUS:gram</ta>
            <ta e="T71" id="Seg_1060" s="T70">TURK:core</ta>
            <ta e="T77" id="Seg_1061" s="T76">TURK:disc</ta>
            <ta e="T80" id="Seg_1062" s="T79">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1063" s="T0">Потом побежал Иванушка к реке, плачет там.</ta>
            <ta e="T10" id="Seg_1064" s="T7">"Меня хотят зарезать.</ta>
            <ta e="T14" id="Seg_1065" s="T10">Вода кипит, чтобы меня сварить".</ta>
            <ta e="T22" id="Seg_1066" s="T14">А она говорит: "У меня на голове камень лежит.</ta>
            <ta e="T25" id="Seg_1067" s="T22">Я не могу подняться".</ta>
            <ta e="T29" id="Seg_1068" s="T25">Потом он снова пришёл, говорит:</ta>
            <ta e="T31" id="Seg_1069" s="T29">"Я пойду к реке".</ta>
            <ta e="T34" id="Seg_1070" s="T31">И снова убежал.</ta>
            <ta e="T37" id="Seg_1071" s="T34">Прибегает и опять плачет:</ta>
            <ta e="T43" id="Seg_1072" s="T37">"Выходи, меня хотят зарезать, сварить".</ta>
            <ta e="T52" id="Seg_1073" s="T43">Она говорит: "Меня камень держит, я не могу подняться".</ta>
            <ta e="T56" id="Seg_1074" s="T52">Потом в третий раз он убежал.</ta>
            <ta e="T61" id="Seg_1075" s="T56">Потом там пробегал (/проходил) человек.</ta>
            <ta e="T64" id="Seg_1076" s="T61">Он слышит [Иванушку?] там. </ta>
            <ta e="T65" id="Seg_1077" s="T64">Он говорит.</ta>
            <ta e="T69" id="Seg_1078" s="T65">Потом они вытащили [её].</ta>
            <ta e="T79" id="Seg_1079" s="T69">А ту женщину они убили, привязали её к лошадям, лошади её разорвали.</ta>
            <ta e="T80" id="Seg_1080" s="T79">Всё.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1081" s="T0">Then this Ivanushka ran to the river, cried there:</ta>
            <ta e="T10" id="Seg_1082" s="T7">"They want to cut (/kill) me.</ta>
            <ta e="T14" id="Seg_1083" s="T10">The water is boiling, to cook me."</ta>
            <ta e="T22" id="Seg_1084" s="T14">But she says: "A stone is lying on my head.</ta>
            <ta e="T25" id="Seg_1085" s="T22">I cannot get up."</ta>
            <ta e="T29" id="Seg_1086" s="T25">Then he came again, says:</ta>
            <ta e="T31" id="Seg_1087" s="T29">"I will go to the river."</ta>
            <ta e="T34" id="Seg_1088" s="T31">Then he ran off again.</ta>
            <ta e="T37" id="Seg_1089" s="T34">He came and cries again:</ta>
            <ta e="T43" id="Seg_1090" s="T37">"Come out, they will kill me, they are cooking [me].</ta>
            <ta e="T52" id="Seg_1091" s="T43">She says: "A stone is holding me down, I cannot get up."</ta>
            <ta e="T56" id="Seg_1092" s="T52">Then he left for the third time.</ta>
            <ta e="T61" id="Seg_1093" s="T56">Then there ran a man.</ta>
            <ta e="T64" id="Seg_1094" s="T61">He hears [Ivanushka?] there. </ta>
            <ta e="T65" id="Seg_1095" s="T64">He is talking.</ta>
            <ta e="T69" id="Seg_1096" s="T65">Then they brought [her] out.</ta>
            <ta e="T79" id="Seg_1097" s="T69">But they killed the other one, tied to horses, the horses tore her apart.</ta>
            <ta e="T80" id="Seg_1098" s="T79">[That's] all!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1099" s="T0">Dann lief dieser Ivanushka zum Fluss, rief dort:</ta>
            <ta e="T10" id="Seg_1100" s="T7">„Sie wollen mich schneiden (/töten).</ta>
            <ta e="T14" id="Seg_1101" s="T10">Das Wasser kocht, um mich zu kochen.“</ta>
            <ta e="T22" id="Seg_1102" s="T14">Aber sie sagt: „Ein Stein liegt auf meinem Kopf.</ta>
            <ta e="T25" id="Seg_1103" s="T22">Ich kann nicht aufstehen.“</ta>
            <ta e="T29" id="Seg_1104" s="T25">Dann kam er wieder, sagt:</ta>
            <ta e="T31" id="Seg_1105" s="T29">„Ich gehe zum Fluss.“</ta>
            <ta e="T34" id="Seg_1106" s="T31">Dann lief er wieder fort.</ta>
            <ta e="T37" id="Seg_1107" s="T34">Er kam und ruft wieder:</ta>
            <ta e="T43" id="Seg_1108" s="T37">„Komm heraus, sie werden mich töten, sie kochen [mich].“</ta>
            <ta e="T52" id="Seg_1109" s="T43">Sie sagt: „Ein Stein hält mich nieder, Ich kann nicht aufstehen.“</ta>
            <ta e="T56" id="Seg_1110" s="T52">Dann ging er ein drittes mal.</ta>
            <ta e="T61" id="Seg_1111" s="T56">Es lief dann ein Mann.</ta>
            <ta e="T64" id="Seg_1112" s="T61">Er hört [Ivanushka?] dort. </ta>
            <ta e="T65" id="Seg_1113" s="T64">Er spricht.</ta>
            <ta e="T69" id="Seg_1114" s="T65">Dann brachten sie [sie] heraus.</ta>
            <ta e="T79" id="Seg_1115" s="T69">Aber sie töteten den anderen, an Pferden gefesselt, die Pferde rissen ihn auseinander.</ta>
            <ta e="T80" id="Seg_1116" s="T79">[Das ist] alles!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_1117" s="T0">[GVY:] This is a continuation of the tale about Alenushka and her brother Ivanushka (see PKZ_196X_Alenushka_flk, AEDKL SU0201). </ta>
            <ta e="T43" id="Seg_1118" s="T37">[KlT:] Supsə- (previously only supso- attested).</ta>
            <ta e="T52" id="Seg_1119" s="T43">[GVY:] [mojliam]</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
