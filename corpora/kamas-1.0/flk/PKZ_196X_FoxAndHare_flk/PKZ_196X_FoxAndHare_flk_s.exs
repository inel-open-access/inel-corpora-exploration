<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID862F07E6-BD2D-36DC-A40D-C566BCAD9927">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndHare_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndHare_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndHare_flk\PKZ_196X_FoxAndHare_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">497</ud-information>
            <ud-information attribute-name="# HIAT:w">328</ud-information>
            <ud-information attribute-name="# e">325</ud-information>
            <ud-information attribute-name="# HIAT:u">72</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1031" time="0.106" type="appl" />
         <tli id="T1032" time="0.863" type="appl" />
         <tli id="T1033" time="1.62" type="appl" />
         <tli id="T1034" time="2.378" type="appl" />
         <tli id="T1035" time="3.135" type="appl" />
         <tli id="T1036" time="4.296" type="appl" />
         <tli id="T1037" time="5.431" type="appl" />
         <tli id="T1038" time="6.566" type="appl" />
         <tli id="T1039" time="7.702" type="appl" />
         <tli id="T1040" time="8.554" type="appl" />
         <tli id="T1041" time="9.406" type="appl" />
         <tli id="T1042" time="10.257" type="appl" />
         <tli id="T1043" time="11.109" type="appl" />
         <tli id="T1044" time="13.093297408596515" />
         <tli id="T1045" time="13.512" type="appl" />
         <tli id="T1046" time="14.4" type="appl" />
         <tli id="T1047" time="15.287" type="appl" />
         <tli id="T1048" time="16.078" type="appl" />
         <tli id="T1049" time="16.868" type="appl" />
         <tli id="T1050" time="17.659" type="appl" />
         <tli id="T1051" time="18.506615889136416" />
         <tli id="T1052" time="19.288" type="appl" />
         <tli id="T1053" time="20.126" type="appl" />
         <tli id="T1054" time="20.963" type="appl" />
         <tli id="T1055" time="21.801" type="appl" />
         <tli id="T1056" time="22.949" type="appl" />
         <tli id="T1057" time="24.098" type="appl" />
         <tli id="T1058" time="25.246" type="appl" />
         <tli id="T1059" time="25.964" type="appl" />
         <tli id="T1060" time="26.683" type="appl" />
         <tli id="T1061" time="27.401" type="appl" />
         <tli id="T1062" time="28.12" type="appl" />
         <tli id="T1063" time="28.838" type="appl" />
         <tli id="T1064" time="29.611" type="appl" />
         <tli id="T1065" time="30.232" type="appl" />
         <tli id="T1066" time="30.854" type="appl" />
         <tli id="T1067" time="31.475" type="appl" />
         <tli id="T1068" time="32.181" type="appl" />
         <tli id="T1069" time="32.887" type="appl" />
         <tli id="T1070" time="33.594" type="appl" />
         <tli id="T1071" time="34.3" type="appl" />
         <tli id="T1072" time="35.006" type="appl" />
         <tli id="T1073" time="36.1265675443913" />
         <tli id="T1074" time="36.951" type="appl" />
         <tli id="T1075" time="37.874" type="appl" />
         <tli id="T1076" time="38.798" type="appl" />
         <tli id="T1077" time="39.722" type="appl" />
         <tli id="T1078" time="40.645" type="appl" />
         <tli id="T1079" time="42.10655113681038" />
         <tli id="T1080" time="42.934" type="appl" />
         <tli id="T1081" time="43.551" type="appl" />
         <tli id="T1082" time="44.168" type="appl" />
         <tli id="T1083" time="44.784" type="appl" />
         <tli id="T1084" time="45.325" type="appl" />
         <tli id="T1085" time="46.099873513464814" />
         <tli id="T1086" time="46.995" type="appl" />
         <tli id="T1087" time="47.758" type="appl" />
         <tli id="T1088" time="48.52" type="appl" />
         <tli id="T1089" time="49.246" type="appl" />
         <tli id="T1090" time="50.20652891249509" />
         <tli id="T1091" time="50.955" type="appl" />
         <tli id="T1092" time="51.724" type="appl" />
         <tli id="T1093" time="52.494" type="appl" />
         <tli id="T1094" time="53.263" type="appl" />
         <tli id="T1095" time="54.032" type="appl" />
         <tli id="T1096" time="54.801" type="appl" />
         <tli id="T1097" time="55.57" type="appl" />
         <tli id="T1098" time="56.339" type="appl" />
         <tli id="T1099" time="57.109" type="appl" />
         <tli id="T1100" time="57.878" type="appl" />
         <tli id="T1101" time="58.647" type="appl" />
         <tli id="T1102" time="59.488" type="appl" />
         <tli id="T1103" time="60.328" type="appl" />
         <tli id="T1104" time="61.168" type="appl" />
         <tli id="T1105" time="62.206495987583544" />
         <tli id="T1106" time="62.84" type="appl" />
         <tli id="T1107" time="63.576" type="appl" />
         <tli id="T1108" time="64.311" type="appl" />
         <tli id="T1109" time="65.047" type="appl" />
         <tli id="T1110" time="66.53981743136549" />
         <tli id="T1111" time="66.993" type="appl" />
         <tli id="T1112" time="68.081" type="appl" />
         <tli id="T1113" time="69.17" type="appl" />
         <tli id="T1114" time="70.342" type="appl" />
         <tli id="T1115" time="71.76646975740401" />
         <tli id="T1116" time="72.336" type="appl" />
         <tli id="T1117" time="72.842" type="appl" />
         <tli id="T1118" time="73.349" type="appl" />
         <tli id="T1119" time="73.855" type="appl" />
         <tli id="T1120" time="74.361" type="appl" />
         <tli id="T1121" time="75.286" type="appl" />
         <tli id="T1122" time="76.212" type="appl" />
         <tli id="T1123" time="77.137" type="appl" />
         <tli id="T1124" time="78.062" type="appl" />
         <tli id="T1125" time="78.887" type="appl" />
         <tli id="T1126" time="79.575" type="appl" />
         <tli id="T1127" time="80.262" type="appl" />
         <tli id="T1128" time="80.95" type="appl" />
         <tli id="T1129" time="81.638" type="appl" />
         <tli id="T1130" time="82.28" type="appl" />
         <tli id="T1131" time="82.922" type="appl" />
         <tli id="T1132" time="83.564" type="appl" />
         <tli id="T1133" time="84.206" type="appl" />
         <tli id="T1134" time="84.848" type="appl" />
         <tli id="T1135" time="85.49" type="appl" />
         <tli id="T1136" time="86.132" type="appl" />
         <tli id="T1137" time="86.774" type="appl" />
         <tli id="T1138" time="87.482" type="appl" />
         <tli id="T1139" time="88.189" type="appl" />
         <tli id="T1140" time="88.896" type="appl" />
         <tli id="T1141" time="89.604" type="appl" />
         <tli id="T1142" time="91.00641696779584" />
         <tli id="T1143" time="91.885" type="appl" />
         <tli id="T1144" time="93.38641043768838" />
         <tli id="T1145" time="94.095" type="appl" />
         <tli id="T1146" time="94.632" type="appl" />
         <tli id="T1147" time="95.17" type="appl" />
         <tli id="T1148" time="95.708" type="appl" />
         <tli id="T1149" time="96.255" type="appl" />
         <tli id="T1150" time="96.801" type="appl" />
         <tli id="T1151" time="97.348" type="appl" />
         <tli id="T1152" time="97.895" type="appl" />
         <tli id="T1153" time="98.442" type="appl" />
         <tli id="T1154" time="98.988" type="appl" />
         <tli id="T1155" time="99.535" type="appl" />
         <tli id="T1156" time="100.485" type="appl" />
         <tli id="T1157" time="101.435" type="appl" />
         <tli id="T1158" time="102.386" type="appl" />
         <tli id="T1159" time="103.336" type="appl" />
         <tli id="T1160" time="104.168" type="appl" />
         <tli id="T1161" time="104.86" type="appl" />
         <tli id="T1162" time="105.553" type="appl" />
         <tli id="T1163" time="106.386" type="appl" />
         <tli id="T1164" time="107.219" type="appl" />
         <tli id="T1165" time="108.052" type="appl" />
         <tli id="T1166" time="108.885" type="appl" />
         <tli id="T1167" time="109.718" type="appl" />
         <tli id="T1168" time="110.551" type="appl" />
         <tli id="T1169" time="111.536" type="appl" />
         <tli id="T1170" time="112.394" type="appl" />
         <tli id="T1171" time="113.253" type="appl" />
         <tli id="T1172" time="114.111" type="appl" />
         <tli id="T1173" time="114.969" type="appl" />
         <tli id="T1174" time="116.21" type="appl" />
         <tli id="T1175" time="117.45" type="appl" />
         <tli id="T1176" time="118.691" type="appl" />
         <tli id="T1177" time="119.64" type="appl" />
         <tli id="T1178" time="120.546" type="appl" />
         <tli id="T1179" time="121.219" type="appl" />
         <tli id="T1180" time="121.868" type="appl" />
         <tli id="T1181" time="122.517" type="appl" />
         <tli id="T1182" time="123.166" type="appl" />
         <tli id="T1183" time="123.815" type="appl" />
         <tli id="T1184" time="124.269" type="appl" />
         <tli id="T1185" time="124.724" type="appl" />
         <tli id="T1186" time="125.178" type="appl" />
         <tli id="T1187" time="125.633" type="appl" />
         <tli id="T1188" time="126.087" type="appl" />
         <tli id="T1189" time="126.681" type="appl" />
         <tli id="T1190" time="127.196" type="appl" />
         <tli id="T1191" time="127.712" type="appl" />
         <tli id="T1192" time="128.227" type="appl" />
         <tli id="T1193" time="128.742" type="appl" />
         <tli id="T1194" time="129.258" type="appl" />
         <tli id="T1195" time="129.773" type="appl" />
         <tli id="T1196" time="130.288" type="appl" />
         <tli id="T1197" time="130.909" type="appl" />
         <tli id="T1198" time="131.53" type="appl" />
         <tli id="T1199" time="132.152" type="appl" />
         <tli id="T1200" time="132.773" type="appl" />
         <tli id="T1201" time="133.394" type="appl" />
         <tli id="T1202" time="133.929" type="appl" />
         <tli id="T1203" time="134.463" type="appl" />
         <tli id="T1204" time="135.84629393837636" />
         <tli id="T1205" time="136.803" type="appl" />
         <tli id="T1206" time="137.492" type="appl" />
         <tli id="T1207" time="138.181" type="appl" />
         <tli id="T1208" time="138.84332998609156" />
         <tli id="T1209" time="139.262" type="appl" />
         <tli id="T1210" time="139.653" type="appl" />
         <tli id="T1211" time="140.045" type="appl" />
         <tli id="T1212" time="140.445" type="appl" />
         <tli id="T1213" time="140.845" type="appl" />
         <tli id="T1214" time="141.245" type="appl" />
         <tli id="T1215" time="141.645" type="appl" />
         <tli id="T1216" time="142.271" type="appl" />
         <tli id="T1217" time="142.896" type="appl" />
         <tli id="T1218" time="144.95960226706853" />
         <tli id="T1219" time="145.693" type="appl" />
         <tli id="T1220" time="146.268" type="appl" />
         <tli id="T1221" time="146.843" type="appl" />
         <tli id="T1222" time="147.418" type="appl" />
         <tli id="T1223" time="147.993" type="appl" />
         <tli id="T1224" time="148.568" type="appl" />
         <tli id="T1225" time="149.143" type="appl" />
         <tli id="T1226" time="150.2195878349823" />
         <tli id="T1227" time="151.019" type="appl" />
         <tli id="T1228" time="151.582" type="appl" />
         <tli id="T1229" time="152.144" type="appl" />
         <tli id="T1230" time="152.706" type="appl" />
         <tli id="T1231" time="153.269" type="appl" />
         <tli id="T1232" time="153.831" type="appl" />
         <tli id="T1233" time="154.393" type="appl" />
         <tli id="T1234" time="154.955" type="appl" />
         <tli id="T1235" time="155.518" type="appl" />
         <tli id="T1236" time="156.08" type="appl" />
         <tli id="T1237" time="156.754" type="appl" />
         <tli id="T1238" time="157.428" type="appl" />
         <tli id="T1239" time="158.102" type="appl" />
         <tli id="T1240" time="158.775" type="appl" />
         <tli id="T1241" time="159.449" type="appl" />
         <tli id="T1242" time="160.3928932553073" />
         <tli id="T1243" time="161.131" type="appl" />
         <tli id="T1244" time="162.003" type="appl" />
         <tli id="T1245" time="162.875" type="appl" />
         <tli id="T1246" time="163.747" type="appl" />
         <tli id="T1247" time="164.619" type="appl" />
         <tli id="T1248" time="165.491" type="appl" />
         <tli id="T1249" time="166.728" type="appl" />
         <tli id="T1250" time="167.797" type="appl" />
         <tli id="T1251" time="168.865" type="appl" />
         <tli id="T1252" time="169.934" type="appl" />
         <tli id="T1253" time="171.26619675501243" />
         <tli id="T1254" time="172.079" type="appl" />
         <tli id="T1255" time="172.693" type="appl" />
         <tli id="T1256" time="173.46619071877868" />
         <tli id="T1257" time="173.978" type="appl" />
         <tli id="T1258" time="174.542" type="appl" />
         <tli id="T1259" time="175.106" type="appl" />
         <tli id="T1260" time="175.67" type="appl" />
         <tli id="T1261" time="176.368" type="appl" />
         <tli id="T1262" time="177.067" type="appl" />
         <tli id="T1263" time="177.765" type="appl" />
         <tli id="T1264" time="178.464" type="appl" />
         <tli id="T1265" time="179.162" type="appl" />
         <tli id="T1266" time="179.8276706603004" />
         <tli id="T1267" time="180.476" type="appl" />
         <tli id="T1268" time="181.09" type="appl" />
         <tli id="T1269" time="181.705" type="appl" />
         <tli id="T1270" time="182.319" type="appl" />
         <tli id="T1271" time="182.934" type="appl" />
         <tli id="T1272" time="183.548" type="appl" />
         <tli id="T1273" time="184.163" type="appl" />
         <tli id="T1274" time="185.149" type="appl" />
         <tli id="T1275" time="186.092" type="appl" />
         <tli id="T1276" time="187.036" type="appl" />
         <tli id="T1277" time="187.979" type="appl" />
         <tli id="T1278" time="188.604" type="appl" />
         <tli id="T1279" time="189.23" type="appl" />
         <tli id="T1280" time="189.855" type="appl" />
         <tli id="T1281" time="190.48" type="appl" />
         <tli id="T1282" time="190.945" type="appl" />
         <tli id="T1283" time="191.411" type="appl" />
         <tli id="T1284" time="191.876" type="appl" />
         <tli id="T1285" time="192.342" type="appl" />
         <tli id="T1286" time="192.808" type="appl" />
         <tli id="T1287" time="193.273" type="appl" />
         <tli id="T1288" time="193.678" type="appl" />
         <tli id="T1289" time="194.082" type="appl" />
         <tli id="T1290" time="194.487" type="appl" />
         <tli id="T1291" time="194.892" type="appl" />
         <tli id="T1292" time="196.0461287650701" />
         <tli id="T1293" time="196.896" type="appl" />
         <tli id="T1294" time="197.8394571779583" />
         <tli id="T1295" time="198.735" type="appl" />
         <tli id="T1296" time="199.746" type="appl" />
         <tli id="T1297" time="200.633" type="appl" />
         <tli id="T1298" time="201.519" type="appl" />
         <tli id="T1299" time="202.406" type="appl" />
         <tli id="T1300" time="203.293" type="appl" />
         <tli id="T1301" time="204.179" type="appl" />
         <tli id="T1302" time="205.066" type="appl" />
         <tli id="T1303" time="205.687" type="appl" />
         <tli id="T1304" time="206.308" type="appl" />
         <tli id="T1305" time="206.929" type="appl" />
         <tli id="T1306" time="207.55" type="appl" />
         <tli id="T1307" time="208.171" type="appl" />
         <tli id="T1308" time="208.792" type="appl" />
         <tli id="T1309" time="210.164" type="appl" />
         <tli id="T1310" time="211.535" type="appl" />
         <tli id="T1311" time="213.2460815726969" />
         <tli id="T1312" time="214.588" type="appl" />
         <tli id="T1313" time="216.27" type="appl" />
         <tli id="T1314" time="217.951" type="appl" />
         <tli id="T1315" time="219.633" type="appl" />
         <tli id="T1316" time="221.32605940325647" />
         <tli id="T1317" time="222.173" type="appl" />
         <tli id="T1318" time="222.982" type="appl" />
         <tli id="T1319" time="223.792" type="appl" />
         <tli id="T1320" time="224.437" type="appl" />
         <tli id="T1321" time="225.083" type="appl" />
         <tli id="T1322" time="225.728" type="appl" />
         <tli id="T1323" time="226.373" type="appl" />
         <tli id="T1324" time="227.019" type="appl" />
         <tli id="T1325" time="228.5993727804351" />
         <tli id="T1326" time="229.766" type="appl" />
         <tli id="T1327" time="230.78" type="appl" />
         <tli id="T1328" time="231.794" type="appl" />
         <tli id="T1329" time="232.99936070796753" />
         <tli id="T1330" time="233.602" type="appl" />
         <tli id="T1331" time="234.244" type="appl" />
         <tli id="T1332" time="234.886" type="appl" />
         <tli id="T1333" time="235.528" type="appl" />
         <tli id="T1334" time="236.234" type="appl" />
         <tli id="T1335" time="236.941" type="appl" />
         <tli id="T1336" time="237.647" type="appl" />
         <tli id="T1337" time="238.354" type="appl" />
         <tli id="T1338" time="239.06" type="appl" />
         <tli id="T1339" time="239.846" type="appl" />
         <tli id="T1340" time="240.632" type="appl" />
         <tli id="T1341" time="241.334" type="appl" />
         <tli id="T1342" time="242.035" type="appl" />
         <tli id="T1343" time="242.737" type="appl" />
         <tli id="T1344" time="243.439" type="appl" />
         <tli id="T1345" time="244.14" type="appl" />
         <tli id="T1346" time="244.842" type="appl" />
         <tli id="T1347" time="245.277" type="appl" />
         <tli id="T1348" time="245.691" type="appl" />
         <tli id="T1349" time="246.106" type="appl" />
         <tli id="T1350" time="247.086" type="appl" />
         <tli id="T1351" time="248.066" type="appl" />
         <tli id="T1352" time="249.046" type="appl" />
         <tli id="T1353" time="250.025" type="appl" />
         <tli id="T1354" time="251.005" type="appl" />
         <tli id="T1355" time="251.985" type="appl" />
         <tli id="T1356" time="252.965" type="appl" />
         <tli id="T1357" time="254.366" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T1145" start="T1144">
            <tli id="T1144.tx.1" />
         </timeline-fork>
         <timeline-fork end="T1304" start="T1302">
            <tli id="T1302.tx.1" />
         </timeline-fork>
         <timeline-fork end="T1322" start="T1321">
            <tli id="T1321.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1357" id="Seg_0" n="sc" s="T1031">
               <ts e="T1035" id="Seg_2" n="HIAT:u" s="T1031">
                  <ts e="T1032" id="Seg_4" n="HIAT:w" s="T1031">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_7" n="HIAT:w" s="T1032">lisa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_10" n="HIAT:w" s="T1033">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_13" n="HIAT:w" s="T1034">zajats</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1039" id="Seg_17" n="HIAT:u" s="T1035">
                  <ts e="T1036" id="Seg_19" n="HIAT:w" s="T1035">Lisan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_22" n="HIAT:w" s="T1036">ibi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_25" n="HIAT:w" s="T1037">turat</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_28" n="HIAT:w" s="T1038">sĭregəʔ</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1044" id="Seg_32" n="HIAT:u" s="T1039">
                  <ts e="T1040" id="Seg_34" n="HIAT:w" s="T1039">A</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_37" n="HIAT:w" s="T1040">zajatsən</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_40" n="HIAT:w" s="T1041">ibi</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T1043" id="Seg_44" n="HIAT:w" s="T1042">pa-</ts>
                  <nts id="Seg_45" n="HIAT:ip">)</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_48" n="HIAT:w" s="T1043">pagəʔ</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1047" id="Seg_52" n="HIAT:u" s="T1044">
                  <ts e="T1045" id="Seg_54" n="HIAT:w" s="T1044">Dĭgəttə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_57" n="HIAT:w" s="T1045">šobi</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_60" n="HIAT:w" s="T1046">ejü</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1051" id="Seg_64" n="HIAT:u" s="T1047">
                  <ts e="T1048" id="Seg_66" n="HIAT:w" s="T1047">Lisan</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_69" n="HIAT:w" s="T1048">turat</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_72" n="HIAT:w" s="T1049">bar</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_75" n="HIAT:w" s="T1050">mʼaŋŋuʔpi</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1055" id="Seg_79" n="HIAT:u" s="T1051">
                  <ts e="T1052" id="Seg_81" n="HIAT:w" s="T1051">Dĭgəttə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_84" n="HIAT:w" s="T1052">dĭ</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_87" n="HIAT:w" s="T1053">šobi</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_90" n="HIAT:w" s="T1054">zajatsənə</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1058" id="Seg_94" n="HIAT:u" s="T1055">
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <ts e="T1056" id="Seg_97" n="HIAT:w" s="T1055">Öʔləʔ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_100" n="HIAT:w" s="T1056">măna</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_103" n="HIAT:w" s="T1057">kunolzittə</ts>
                  <nts id="Seg_104" n="HIAT:ip">"</nts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1063" id="Seg_108" n="HIAT:u" s="T1058">
                  <ts e="T1059" id="Seg_110" n="HIAT:w" s="T1058">Dĭ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_113" n="HIAT:w" s="T1059">bostə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_116" n="HIAT:w" s="T1060">dĭm</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_119" n="HIAT:w" s="T1061">sürerlüʔpi</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_122" n="HIAT:w" s="T1062">turagəʔ</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1067" id="Seg_126" n="HIAT:u" s="T1063">
                  <ts e="T1064" id="Seg_128" n="HIAT:w" s="T1063">Dĭ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_131" n="HIAT:w" s="T1064">šonəga</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_134" n="HIAT:w" s="T1065">i</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_137" n="HIAT:w" s="T1066">dʼorlaʔbə</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1073" id="Seg_141" n="HIAT:u" s="T1067">
                  <nts id="Seg_142" n="HIAT:ip">(</nts>
                  <ts e="T1068" id="Seg_144" n="HIAT:w" s="T1067">Măn=</ts>
                  <nts id="Seg_145" n="HIAT:ip">)</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_148" n="HIAT:w" s="T1068">Men</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_151" n="HIAT:w" s="T1069">măndə:</ts>
                  <nts id="Seg_152" n="HIAT:ip">"</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_155" n="HIAT:w" s="T1070">Ĭmbi</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_158" n="HIAT:w" s="T1071">tăn</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_161" n="HIAT:w" s="T1072">dʼorlaʔbəl</ts>
                  <nts id="Seg_162" n="HIAT:ip">?</nts>
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1079" id="Seg_166" n="HIAT:u" s="T1073">
                  <nts id="Seg_167" n="HIAT:ip">"</nts>
                  <ts e="T1074" id="Seg_169" n="HIAT:w" s="T1073">Da</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_172" n="HIAT:w" s="T1074">măn</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_175" n="HIAT:w" s="T1075">turagəʔ</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_179" n="HIAT:w" s="T1076">măndə</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_183" n="HIAT:w" s="T1077">sürerbi</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_186" n="HIAT:w" s="T1078">lisička</ts>
                  <nts id="Seg_187" n="HIAT:ip">"</nts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1083" id="Seg_191" n="HIAT:u" s="T1079">
                  <nts id="Seg_192" n="HIAT:ip">"</nts>
                  <ts e="T1080" id="Seg_194" n="HIAT:w" s="T1079">Kanžəbəj</ts>
                  <nts id="Seg_195" n="HIAT:ip">,</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_198" n="HIAT:w" s="T1080">măn</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_201" n="HIAT:w" s="T1081">dĭm</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_204" n="HIAT:w" s="T1082">sürerlim</ts>
                  <nts id="Seg_205" n="HIAT:ip">!</nts>
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1085" id="Seg_209" n="HIAT:u" s="T1083">
                  <ts e="T1084" id="Seg_211" n="HIAT:w" s="T1083">Šobi</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_214" n="HIAT:w" s="T1084">men</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1088" id="Seg_218" n="HIAT:u" s="T1085">
                  <ts e="T1086" id="Seg_220" n="HIAT:w" s="T1085">Da</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_223" n="HIAT:w" s="T1086">üge</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_226" n="HIAT:w" s="T1087">kirgarlaʔbə</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1090" id="Seg_230" n="HIAT:u" s="T1088">
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <ts e="T1089" id="Seg_233" n="HIAT:w" s="T1088">Kanaʔ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_236" n="HIAT:w" s="T1089">döʔə</ts>
                  <nts id="Seg_237" n="HIAT:ip">!</nts>
                  <nts id="Seg_238" n="HIAT:ip">"</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1101" id="Seg_241" n="HIAT:u" s="T1090">
                  <ts e="T1091" id="Seg_243" n="HIAT:w" s="T1090">A</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_246" n="HIAT:w" s="T1091">dĭ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_249" n="HIAT:w" s="T1092">măndə:</ts>
                  <nts id="Seg_250" n="HIAT:ip">"</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_253" n="HIAT:w" s="T1093">Tüjö</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_256" n="HIAT:w" s="T1094">tăn</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_259" n="HIAT:w" s="T1095">bar</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_262" n="HIAT:w" s="T1096">tărlə</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_265" n="HIAT:w" s="T1097">nĭŋgələm</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_269" n="HIAT:w" s="T1098">i</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_272" n="HIAT:w" s="T1099">tăn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_275" n="HIAT:w" s="T1100">nuʔməluʔləl</ts>
                  <nts id="Seg_276" n="HIAT:ip">"</nts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1105" id="Seg_280" n="HIAT:u" s="T1101">
                  <ts e="T1102" id="Seg_282" n="HIAT:w" s="T1101">Dĭ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_285" n="HIAT:w" s="T1102">nereʔluʔpi</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_288" n="HIAT:w" s="T1103">i</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_291" n="HIAT:w" s="T1104">nuʔməluʔpi</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1110" id="Seg_295" n="HIAT:u" s="T1105">
                  <ts e="T1106" id="Seg_297" n="HIAT:w" s="T1105">Bazoʔ</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_300" n="HIAT:w" s="T1106">kandəga</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_303" n="HIAT:w" s="T1107">zajats</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_306" n="HIAT:w" s="T1108">i</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1110" id="Seg_309" n="HIAT:w" s="T1109">dʼorlaʔbə</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1113" id="Seg_313" n="HIAT:u" s="T1110">
                  <ts e="T1111" id="Seg_315" n="HIAT:w" s="T1110">Dĭgəttə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_318" n="HIAT:w" s="T1111">šonəga</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_321" n="HIAT:w" s="T1112">urgaːba</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1115" id="Seg_325" n="HIAT:u" s="T1113">
                  <nts id="Seg_326" n="HIAT:ip">"</nts>
                  <ts e="T1114" id="Seg_328" n="HIAT:w" s="T1113">Ĭmbi</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_331" n="HIAT:w" s="T1114">dʼorlaʔbəl</ts>
                  <nts id="Seg_332" n="HIAT:ip">?</nts>
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1120" id="Seg_336" n="HIAT:u" s="T1115">
                  <nts id="Seg_337" n="HIAT:ip">"</nts>
                  <ts e="T1116" id="Seg_339" n="HIAT:w" s="T1115">Da</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_342" n="HIAT:w" s="T1116">kak</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1118" id="Seg_345" n="HIAT:w" s="T1117">măna</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_348" n="HIAT:w" s="T1118">ej</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_351" n="HIAT:w" s="T1119">dʼorzittə</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1124" id="Seg_355" n="HIAT:u" s="T1120">
                  <ts e="T1121" id="Seg_357" n="HIAT:w" s="T1120">Măn</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_360" n="HIAT:w" s="T1121">turam</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_363" n="HIAT:w" s="T1122">ibi</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_366" n="HIAT:w" s="T1123">pagəʔ</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1129" id="Seg_370" n="HIAT:u" s="T1124">
                  <ts e="T1125" id="Seg_372" n="HIAT:w" s="T1124">A</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_375" n="HIAT:w" s="T1125">lisitsan</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_378" n="HIAT:w" s="T1126">turat</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_381" n="HIAT:w" s="T1127">bɨl</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_384" n="HIAT:w" s="T1128">sĭregəʔ</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1137" id="Seg_388" n="HIAT:u" s="T1129">
                  <ts e="T1130" id="Seg_390" n="HIAT:w" s="T1129">Tüj</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1131" id="Seg_393" n="HIAT:w" s="T1130">dĭ</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_396" n="HIAT:w" s="T1131">šobi</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_399" n="HIAT:w" s="T1132">măna</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_402" n="HIAT:w" s="T1133">šaːsʼtə</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_405" n="HIAT:w" s="T1134">i</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1136" id="Seg_408" n="HIAT:w" s="T1135">măna</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_411" n="HIAT:w" s="T1136">sürerlüʔpi</ts>
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1141" id="Seg_416" n="HIAT:u" s="T1137">
                  <nts id="Seg_417" n="HIAT:ip">"</nts>
                  <ts e="T1138" id="Seg_419" n="HIAT:w" s="T1137">Kanžəbəj</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_423" n="HIAT:w" s="T1138">măn</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_426" n="HIAT:w" s="T1139">dĭm</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_429" n="HIAT:w" s="T1140">sürerlim</ts>
                  <nts id="Seg_430" n="HIAT:ip">!</nts>
                  <nts id="Seg_431" n="HIAT:ip">"</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1142" id="Seg_434" n="HIAT:u" s="T1141">
                  <ts e="T1142" id="Seg_436" n="HIAT:w" s="T1141">Šobi</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1144" id="Seg_440" n="HIAT:u" s="T1142">
                  <nts id="Seg_441" n="HIAT:ip">"</nts>
                  <ts e="T1143" id="Seg_443" n="HIAT:w" s="T1142">Kanaʔ</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_446" n="HIAT:w" s="T1143">döʔə</ts>
                  <nts id="Seg_447" n="HIAT:ip">!</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1148" id="Seg_450" n="HIAT:u" s="T1144">
                  <ts e="T1144.tx.1" id="Seg_452" n="HIAT:w" s="T1144">A</ts>
                  <nts id="Seg_453" n="HIAT:ip">_</nts>
                  <ts e="T1145" id="Seg_455" n="HIAT:w" s="T1144.tx.1">to</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_458" n="HIAT:w" s="T1145">tüjö</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1147" id="Seg_461" n="HIAT:w" s="T1146">tănan</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_464" n="HIAT:w" s="T1147">münörlam</ts>
                  <nts id="Seg_465" n="HIAT:ip">!</nts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1155" id="Seg_469" n="HIAT:u" s="T1148">
                  <ts e="T1149" id="Seg_471" n="HIAT:w" s="T1148">A</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_474" n="HIAT:w" s="T1149">dĭ</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_477" n="HIAT:w" s="T1150">măndə:</ts>
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_481" n="HIAT:w" s="T1151">Măn</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1153" id="Seg_484" n="HIAT:w" s="T1152">kak</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1154" id="Seg_487" n="HIAT:w" s="T1153">nuʔməluʔləm</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_490" n="HIAT:w" s="T1154">da</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1159" id="Seg_494" n="HIAT:u" s="T1155">
                  <ts e="T1156" id="Seg_496" n="HIAT:w" s="T1155">Bar</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_499" n="HIAT:w" s="T1156">tărdə</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1158" id="Seg_502" n="HIAT:w" s="T1157">tăn</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_505" n="HIAT:w" s="T1158">nĭŋgəluʔləl</ts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1162" id="Seg_509" n="HIAT:u" s="T1159">
                  <ts e="T1160" id="Seg_511" n="HIAT:w" s="T1159">I</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_514" n="HIAT:w" s="T1160">tăn</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1162" id="Seg_517" n="HIAT:w" s="T1161">nuʔməluʔləl</ts>
                  <nts id="Seg_518" n="HIAT:ip">"</nts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1168" id="Seg_522" n="HIAT:u" s="T1162">
                  <ts e="T1163" id="Seg_524" n="HIAT:w" s="T1162">Dĭgəttə</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_527" n="HIAT:w" s="T1163">urgaːba</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_529" n="HIAT:ip">(</nts>
                  <ts e="T1165" id="Seg_531" n="HIAT:w" s="T1164">-aba</ts>
                  <nts id="Seg_532" n="HIAT:ip">)</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1166" id="Seg_535" n="HIAT:w" s="T1165">pimnuʔpi</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_538" n="HIAT:w" s="T1166">i</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_541" n="HIAT:w" s="T1167">nuʔməluʔpi</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1173" id="Seg_545" n="HIAT:u" s="T1168">
                  <ts e="T1169" id="Seg_547" n="HIAT:w" s="T1168">Bazoʔ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_550" n="HIAT:w" s="T1169">kandəga</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171" id="Seg_553" n="HIAT:w" s="T1170">zajčik</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_556" n="HIAT:w" s="T1171">i</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1173" id="Seg_559" n="HIAT:w" s="T1172">dʼorlaʔbə</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1176" id="Seg_563" n="HIAT:u" s="T1173">
                  <ts e="T1174" id="Seg_565" n="HIAT:w" s="T1173">Dĭgəttə</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_568" n="HIAT:w" s="T1174">buga</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_571" n="HIAT:w" s="T1175">šonəga</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1178" id="Seg_575" n="HIAT:u" s="T1176">
                  <nts id="Seg_576" n="HIAT:ip">"</nts>
                  <ts e="T1177" id="Seg_578" n="HIAT:w" s="T1176">Ĭmbi</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_581" n="HIAT:w" s="T1177">dʼorlaʔbəl</ts>
                  <nts id="Seg_582" n="HIAT:ip">?</nts>
                  <nts id="Seg_583" n="HIAT:ip">"</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1183" id="Seg_586" n="HIAT:u" s="T1178">
                  <nts id="Seg_587" n="HIAT:ip">"</nts>
                  <ts e="T1179" id="Seg_589" n="HIAT:w" s="T1178">Da</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_592" n="HIAT:w" s="T1179">măn</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_595" n="HIAT:w" s="T1180">turam</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_598" n="HIAT:w" s="T1181">lisitsa</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_601" n="HIAT:w" s="T1182">ibi</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1188" id="Seg_605" n="HIAT:u" s="T1183">
                  <ts e="T1184" id="Seg_607" n="HIAT:w" s="T1183">A</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_610" n="HIAT:w" s="T1184">dĭn</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_612" n="HIAT:ip">(</nts>
                  <ts e="T1186" id="Seg_614" n="HIAT:w" s="T1185">i-</ts>
                  <nts id="Seg_615" n="HIAT:ip">)</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1187" id="Seg_618" n="HIAT:w" s="T1186">ibi</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_621" n="HIAT:w" s="T1187">sĭregəʔ</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1196" id="Seg_625" n="HIAT:u" s="T1188">
                  <ts e="T1189" id="Seg_627" n="HIAT:w" s="T1188">I</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1190" id="Seg_630" n="HIAT:w" s="T1189">külaːmbi</ts>
                  <nts id="Seg_631" n="HIAT:ip">,</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_634" n="HIAT:w" s="T1190">a</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_637" n="HIAT:w" s="T1191">măn</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_640" n="HIAT:w" s="T1192">üge</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1194" id="Seg_643" n="HIAT:w" s="T1193">turam</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1195" id="Seg_646" n="HIAT:w" s="T1194">jakšə</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1196" id="Seg_650" n="HIAT:w" s="T1195">pagəʔ</ts>
                  <nts id="Seg_651" n="HIAT:ip">"</nts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1201" id="Seg_655" n="HIAT:u" s="T1196">
                  <ts e="T1197" id="Seg_657" n="HIAT:w" s="T1196">Dĭgəttə:</ts>
                  <nts id="Seg_658" n="HIAT:ip">"</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_661" n="HIAT:w" s="T1197">Kanžəbəj</ts>
                  <nts id="Seg_662" n="HIAT:ip">,</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_665" n="HIAT:w" s="T1198">măn</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_668" n="HIAT:w" s="T1199">dĭm</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_671" n="HIAT:w" s="T1200">sürerim</ts>
                  <nts id="Seg_672" n="HIAT:ip">"</nts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1204" id="Seg_676" n="HIAT:u" s="T1201">
                  <nts id="Seg_677" n="HIAT:ip">"</nts>
                  <ts e="T1202" id="Seg_679" n="HIAT:w" s="T1201">Dʼok</ts>
                  <nts id="Seg_680" n="HIAT:ip">,</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_683" n="HIAT:w" s="T1202">ej</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_686" n="HIAT:w" s="T1203">sürerləl</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1208" id="Seg_690" n="HIAT:u" s="T1204">
                  <ts e="T1205" id="Seg_692" n="HIAT:w" s="T1204">Urgaːba</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1206" id="Seg_695" n="HIAT:w" s="T1205">ej</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1207" id="Seg_698" n="HIAT:w" s="T1206">mobi</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1208" id="Seg_701" n="HIAT:w" s="T1207">sürerzittə</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1211" id="Seg_705" n="HIAT:u" s="T1208">
                  <ts e="T1209" id="Seg_707" n="HIAT:w" s="T1208">Men</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1210" id="Seg_710" n="HIAT:w" s="T1209">ej</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1211" id="Seg_713" n="HIAT:w" s="T1210">mobi</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1215" id="Seg_717" n="HIAT:u" s="T1211">
                  <ts e="T1212" id="Seg_719" n="HIAT:w" s="T1211">Buga</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_723" n="HIAT:w" s="T1212">a</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_726" n="HIAT:w" s="T1213">tăn</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1215" id="Seg_729" n="HIAT:w" s="T1214">vovsʼe</ts>
                  <nts id="Seg_730" n="HIAT:ip">"</nts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1218" id="Seg_734" n="HIAT:u" s="T1215">
                  <ts e="T1216" id="Seg_736" n="HIAT:w" s="T1215">Dĭgəttə</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_738" n="HIAT:ip">(</nts>
                  <ts e="T1217" id="Seg_740" n="HIAT:w" s="T1216">šo-</ts>
                  <nts id="Seg_741" n="HIAT:ip">)</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_744" n="HIAT:w" s="T1217">šobiʔi</ts>
                  <nts id="Seg_745" n="HIAT:ip">.</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1226" id="Seg_748" n="HIAT:u" s="T1218">
                  <ts e="T1219" id="Seg_750" n="HIAT:w" s="T1218">Šobiʔi</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_753" n="HIAT:ip">(</nts>
                  <ts e="T1220" id="Seg_755" n="HIAT:w" s="T1219">bu-</ts>
                  <nts id="Seg_756" n="HIAT:ip">)</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_759" n="HIAT:w" s="T1220">buga</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_762" n="HIAT:w" s="T1221">măndə:</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_764" n="HIAT:ip">"</nts>
                  <nts id="Seg_765" n="HIAT:ip">(</nts>
                  <ts e="T1223" id="Seg_767" n="HIAT:w" s="T1222">Kaga-</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_770" n="HIAT:w" s="T1223">kan-</ts>
                  <nts id="Seg_771" n="HIAT:ip">)</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_774" n="HIAT:w" s="T1224">Kanaʔ</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_777" n="HIAT:w" s="T1225">turagəʔ</ts>
                  <nts id="Seg_778" n="HIAT:ip">!</nts>
                  <nts id="Seg_779" n="HIAT:ip">"</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1236" id="Seg_782" n="HIAT:u" s="T1226">
                  <ts e="T1227" id="Seg_784" n="HIAT:w" s="T1226">A</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1228" id="Seg_787" n="HIAT:w" s="T1227">dĭ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_790" n="HIAT:w" s="T1228">măndə:</ts>
                  <nts id="Seg_791" n="HIAT:ip">"</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1230" id="Seg_794" n="HIAT:w" s="T1229">Tüjö</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1231" id="Seg_797" n="HIAT:w" s="T1230">suʔmiluʔləm</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1232" id="Seg_800" n="HIAT:w" s="T1231">dak</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1233" id="Seg_804" n="HIAT:w" s="T1232">bar</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1234" id="Seg_807" n="HIAT:w" s="T1233">tănan</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1235" id="Seg_810" n="HIAT:w" s="T1234">saj</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1236" id="Seg_813" n="HIAT:w" s="T1235">nožuʔləm</ts>
                  <nts id="Seg_814" n="HIAT:ip">"</nts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1242" id="Seg_818" n="HIAT:u" s="T1236">
                  <ts e="T1237" id="Seg_820" n="HIAT:w" s="T1236">Dĭgəttə</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ts e="T1238" id="Seg_824" n="HIAT:w" s="T1237">buga=</ts>
                  <nts id="Seg_825" n="HIAT:ip">)</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1239" id="Seg_828" n="HIAT:w" s="T1238">buga</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_831" n="HIAT:w" s="T1239">nerelüʔpi</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_834" n="HIAT:w" s="T1240">i</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_837" n="HIAT:w" s="T1241">nuʔməluʔpi</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1248" id="Seg_841" n="HIAT:u" s="T1242">
                  <ts e="T1243" id="Seg_843" n="HIAT:w" s="T1242">Dĭgəttə</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_846" n="HIAT:w" s="T1243">bazoʔ</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_849" n="HIAT:w" s="T1244">kandəga</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_852" n="HIAT:w" s="T1245">zajats</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_855" n="HIAT:w" s="T1246">i</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1248" id="Seg_858" n="HIAT:w" s="T1247">dʼorlaʔbə</ts>
                  <nts id="Seg_859" n="HIAT:ip">.</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1253" id="Seg_862" n="HIAT:u" s="T1248">
                  <ts e="T1249" id="Seg_864" n="HIAT:w" s="T1248">Dĭgəttə</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1250" id="Seg_867" n="HIAT:w" s="T1249">kurizən</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1251" id="Seg_870" n="HIAT:w" s="T1250">tibi</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1252" id="Seg_873" n="HIAT:w" s="T1251">šonəga</ts>
                  <nts id="Seg_874" n="HIAT:ip">,</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1253" id="Seg_877" n="HIAT:w" s="T1252">šapkoziʔ</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1256" id="Seg_881" n="HIAT:u" s="T1253">
                  <ts e="T1254" id="Seg_883" n="HIAT:w" s="T1253">Măndə:</ts>
                  <nts id="Seg_884" n="HIAT:ip">"</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1255" id="Seg_887" n="HIAT:w" s="T1254">Ĭmbi</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1256" id="Seg_890" n="HIAT:w" s="T1255">dʼorlaʔbəl</ts>
                  <nts id="Seg_891" n="HIAT:ip">?</nts>
                  <nts id="Seg_892" n="HIAT:ip">"</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1260" id="Seg_895" n="HIAT:u" s="T1256">
                  <nts id="Seg_896" n="HIAT:ip">"</nts>
                  <ts e="T1257" id="Seg_898" n="HIAT:w" s="T1256">Da</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1258" id="Seg_901" n="HIAT:w" s="T1257">măn</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_904" n="HIAT:w" s="T1258">turam</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_907" n="HIAT:w" s="T1259">pagəʔ</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1266" id="Seg_911" n="HIAT:u" s="T1260">
                  <ts e="T1261" id="Seg_913" n="HIAT:w" s="T1260">Lisitsa</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1262" id="Seg_916" n="HIAT:w" s="T1261">šobi</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1263" id="Seg_919" n="HIAT:w" s="T1262">kunolzittə</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_922" n="HIAT:w" s="T1263">i</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_925" n="HIAT:w" s="T1264">măna</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1266" id="Seg_928" n="HIAT:w" s="T1265">sürerbi</ts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1273" id="Seg_932" n="HIAT:u" s="T1266">
                  <ts e="T1267" id="Seg_934" n="HIAT:w" s="T1266">A</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1268" id="Seg_937" n="HIAT:w" s="T1267">dĭn</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_940" n="HIAT:w" s="T1268">sĭregəʔ</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1270" id="Seg_943" n="HIAT:w" s="T1269">ibi</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1271" id="Seg_946" n="HIAT:w" s="T1270">i</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1272" id="Seg_949" n="HIAT:w" s="T1271">tüj</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_952" n="HIAT:w" s="T1272">naga</ts>
                  <nts id="Seg_953" n="HIAT:ip">"</nts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1277" id="Seg_957" n="HIAT:u" s="T1273">
                  <ts e="T1274" id="Seg_959" n="HIAT:w" s="T1273">Dĭgəttə:</ts>
                  <nts id="Seg_960" n="HIAT:ip">"</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1275" id="Seg_963" n="HIAT:w" s="T1274">Kanžəbəj</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1276" id="Seg_967" n="HIAT:w" s="T1275">măn</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1277" id="Seg_970" n="HIAT:w" s="T1276">sürerləm</ts>
                  <nts id="Seg_971" n="HIAT:ip">"</nts>
                  <nts id="Seg_972" n="HIAT:ip">.</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1281" id="Seg_975" n="HIAT:u" s="T1277">
                  <nts id="Seg_976" n="HIAT:ip">"</nts>
                  <ts e="T1278" id="Seg_978" n="HIAT:w" s="T1277">Men</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1279" id="Seg_981" n="HIAT:w" s="T1278">ej</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1280" id="Seg_984" n="HIAT:w" s="T1279">mobi</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_987" n="HIAT:w" s="T1280">sürerzittə</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1287" id="Seg_991" n="HIAT:u" s="T1281">
                  <ts e="T1282" id="Seg_993" n="HIAT:w" s="T1281">Dĭ</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_996" n="HIAT:w" s="T1282">urgaːba</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_999" n="HIAT:w" s="T1283">i</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1285" id="Seg_1002" n="HIAT:w" s="T1284">buga</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1286" id="Seg_1005" n="HIAT:w" s="T1285">ej</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1287" id="Seg_1008" n="HIAT:w" s="T1286">mobi</ts>
                  <nts id="Seg_1009" n="HIAT:ip">.</nts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1291" id="Seg_1012" n="HIAT:u" s="T1287">
                  <ts e="T1288" id="Seg_1014" n="HIAT:w" s="T1287">A</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1289" id="Seg_1017" n="HIAT:w" s="T1288">tăn</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1290" id="Seg_1020" n="HIAT:w" s="T1289">ej</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1022" n="HIAT:ip">(</nts>
                  <ts e="T1291" id="Seg_1024" n="HIAT:w" s="T1290">sürerləl</ts>
                  <nts id="Seg_1025" n="HIAT:ip">)</nts>
                  <nts id="Seg_1026" n="HIAT:ip">"</nts>
                  <nts id="Seg_1027" n="HIAT:ip">.</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1292" id="Seg_1030" n="HIAT:u" s="T1291">
                  <nts id="Seg_1031" n="HIAT:ip">"</nts>
                  <ts e="T1292" id="Seg_1033" n="HIAT:w" s="T1291">Kanžəbəj</ts>
                  <nts id="Seg_1034" n="HIAT:ip">!</nts>
                  <nts id="Seg_1035" n="HIAT:ip">"</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1294" id="Seg_1038" n="HIAT:u" s="T1292">
                  <ts e="T1293" id="Seg_1040" n="HIAT:w" s="T1292">Dĭgəttə</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_1043" n="HIAT:w" s="T1293">šobi</ts>
                  <nts id="Seg_1044" n="HIAT:ip">.</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1302" id="Seg_1047" n="HIAT:u" s="T1294">
                  <ts e="T1295" id="Seg_1049" n="HIAT:w" s="T1294">Bar</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1296" id="Seg_1052" n="HIAT:w" s="T1295">kirgarlaʔbə:</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1054" n="HIAT:ip">"</nts>
                  <ts e="T1297" id="Seg_1056" n="HIAT:w" s="T1296">Uʔbdəʔ</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1298" id="Seg_1059" n="HIAT:w" s="T1297">pʼešgəʔ</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1061" n="HIAT:ip">(</nts>
                  <ts e="T1299" id="Seg_1063" n="HIAT:w" s="T1298">s-</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_1066" n="HIAT:w" s="T1299">š-</ts>
                  <nts id="Seg_1067" n="HIAT:ip">)</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1301" id="Seg_1070" n="HIAT:w" s="T1300">kanaʔ</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1072" n="HIAT:ip">(</nts>
                  <ts e="T1302" id="Seg_1074" n="HIAT:w" s="T1301">döʔə</ts>
                  <nts id="Seg_1075" n="HIAT:ip">)</nts>
                  <nts id="Seg_1076" n="HIAT:ip">!</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1308" id="Seg_1079" n="HIAT:u" s="T1302">
                  <ts e="T1302.tx.1" id="Seg_1081" n="HIAT:w" s="T1302">A</ts>
                  <nts id="Seg_1082" n="HIAT:ip">_</nts>
                  <ts e="T1304" id="Seg_1084" n="HIAT:w" s="T1302.tx.1">to</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1305" id="Seg_1087" n="HIAT:w" s="T1304">tüjö</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1306" id="Seg_1090" n="HIAT:w" s="T1305">šapkuziʔ</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_1093" n="HIAT:w" s="T1306">tănan</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1308" id="Seg_1096" n="HIAT:w" s="T1307">kutlam</ts>
                  <nts id="Seg_1097" n="HIAT:ip">!</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1311" id="Seg_1100" n="HIAT:u" s="T1308">
                  <ts e="T1309" id="Seg_1102" n="HIAT:w" s="T1308">Bar</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1310" id="Seg_1105" n="HIAT:w" s="T1309">dʼăgarləm</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1311" id="Seg_1108" n="HIAT:w" s="T1310">tănan</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1316" id="Seg_1112" n="HIAT:u" s="T1311">
                  <nts id="Seg_1113" n="HIAT:ip">"</nts>
                  <ts e="T1312" id="Seg_1115" n="HIAT:w" s="T1311">Tüjö</ts>
                  <nts id="Seg_1116" n="HIAT:ip">,</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1118" n="HIAT:ip">(</nts>
                  <ts e="T1313" id="Seg_1120" n="HIAT:w" s="T1312">ujum=</ts>
                  <nts id="Seg_1121" n="HIAT:ip">)</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1314" id="Seg_1124" n="HIAT:w" s="T1313">ujuʔi</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1315" id="Seg_1127" n="HIAT:w" s="T1314">šeriem</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1316" id="Seg_1130" n="HIAT:w" s="T1315">jamaʔi</ts>
                  <nts id="Seg_1131" n="HIAT:ip">"</nts>
                  <nts id="Seg_1132" n="HIAT:ip">.</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1325" id="Seg_1135" n="HIAT:u" s="T1316">
                  <ts e="T1317" id="Seg_1137" n="HIAT:w" s="T1316">Dĭgəttə</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1318" id="Seg_1140" n="HIAT:w" s="T1317">bazoʔ</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_1143" n="HIAT:w" s="T1318">kirgarlaʔbə:</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1145" n="HIAT:ip">"</nts>
                  <ts e="T1320" id="Seg_1147" n="HIAT:w" s="T1319">Kanaʔ</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_1150" n="HIAT:w" s="T1320">pʼešgəʔ</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321.tx.1" id="Seg_1154" n="HIAT:w" s="T1321">a</ts>
                  <nts id="Seg_1155" n="HIAT:ip">_</nts>
                  <ts e="T1322" id="Seg_1157" n="HIAT:w" s="T1321.tx.1">to</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_1160" n="HIAT:w" s="T1322">tüjö</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1324" id="Seg_1163" n="HIAT:w" s="T1323">tănan</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_1166" n="HIAT:w" s="T1324">kutlam</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1329" id="Seg_1170" n="HIAT:u" s="T1325">
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T1326" id="Seg_1173" n="HIAT:w" s="T1325">Tagaj=</ts>
                  <nts id="Seg_1174" n="HIAT:ip">)</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1327" id="Seg_1177" n="HIAT:w" s="T1326">Šapkuziʔ</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1328" id="Seg_1180" n="HIAT:w" s="T1327">bar</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_1183" n="HIAT:w" s="T1328">bătlim</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip">"</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1333" id="Seg_1188" n="HIAT:u" s="T1329">
                  <nts id="Seg_1189" n="HIAT:ip">"</nts>
                  <ts e="T1330" id="Seg_1191" n="HIAT:w" s="T1329">Tüjö</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1331" id="Seg_1194" n="HIAT:w" s="T1330">bar</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1332" id="Seg_1197" n="HIAT:w" s="T1331">oldʼam</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333" id="Seg_1200" n="HIAT:w" s="T1332">šeriom</ts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip">"</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1338" id="Seg_1205" n="HIAT:u" s="T1333">
                  <ts e="T1334" id="Seg_1207" n="HIAT:w" s="T1333">Dĭgəttə</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1335" id="Seg_1210" n="HIAT:w" s="T1334">dĭ</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1336" id="Seg_1213" n="HIAT:w" s="T1335">bazoʔ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_1216" n="HIAT:w" s="T1336">kirgarlaʔbə:</ts>
                  <nts id="Seg_1217" n="HIAT:ip">"</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1338" id="Seg_1220" n="HIAT:w" s="T1337">Kanaʔ</ts>
                  <nts id="Seg_1221" n="HIAT:ip">!</nts>
                  <nts id="Seg_1222" n="HIAT:ip">"</nts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1340" id="Seg_1225" n="HIAT:u" s="T1338">
                  <nts id="Seg_1226" n="HIAT:ip">"</nts>
                  <ts e="T1339" id="Seg_1228" n="HIAT:w" s="T1338">Tüjö</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1340" id="Seg_1231" n="HIAT:w" s="T1339">nuʔməleʔbəm</ts>
                  <nts id="Seg_1232" n="HIAT:ip">!</nts>
                  <nts id="Seg_1233" n="HIAT:ip">"</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1346" id="Seg_1236" n="HIAT:u" s="T1340">
                  <ts e="T1341" id="Seg_1238" n="HIAT:w" s="T1340">Dĭgəttə</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1342" id="Seg_1241" n="HIAT:w" s="T1341">dĭ</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1343" id="Seg_1244" n="HIAT:w" s="T1342">šapkuziʔ</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_1247" n="HIAT:w" s="T1343">dĭm</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1345" id="Seg_1250" n="HIAT:w" s="T1344">bar</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1346" id="Seg_1253" n="HIAT:w" s="T1345">dʼăgarluʔpi</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1349" id="Seg_1257" n="HIAT:u" s="T1346">
                  <ts e="T1347" id="Seg_1259" n="HIAT:w" s="T1346">I</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1348" id="Seg_1262" n="HIAT:w" s="T1347">dĭ</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_1265" n="HIAT:w" s="T1348">külaːmbi</ts>
                  <nts id="Seg_1266" n="HIAT:ip">.</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1356" id="Seg_1269" n="HIAT:u" s="T1349">
                  <ts e="T1350" id="Seg_1271" n="HIAT:w" s="T1349">Dĭgəttə</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_1274" n="HIAT:w" s="T1350">amnobiʔi</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1352" id="Seg_1277" n="HIAT:w" s="T1351">kurizən</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1353" id="Seg_1280" n="HIAT:w" s="T1352">tibi</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_1283" n="HIAT:w" s="T1353">i</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_1286" n="HIAT:w" s="T1354">zajats</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1356" id="Seg_1289" n="HIAT:w" s="T1355">turagən</ts>
                  <nts id="Seg_1290" n="HIAT:ip">.</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1357" id="Seg_1293" n="HIAT:u" s="T1356">
                  <ts e="T1357" id="Seg_1295" n="HIAT:w" s="T1356">Kabarləj</ts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1357" id="Seg_1298" n="sc" s="T1031">
               <ts e="T1032" id="Seg_1300" n="e" s="T1031">Amnobiʔi </ts>
               <ts e="T1033" id="Seg_1302" n="e" s="T1032">lisa </ts>
               <ts e="T1034" id="Seg_1304" n="e" s="T1033">i </ts>
               <ts e="T1035" id="Seg_1306" n="e" s="T1034">zajats. </ts>
               <ts e="T1036" id="Seg_1308" n="e" s="T1035">Lisan </ts>
               <ts e="T1037" id="Seg_1310" n="e" s="T1036">ibi </ts>
               <ts e="T1038" id="Seg_1312" n="e" s="T1037">turat </ts>
               <ts e="T1039" id="Seg_1314" n="e" s="T1038">sĭregəʔ. </ts>
               <ts e="T1040" id="Seg_1316" n="e" s="T1039">A </ts>
               <ts e="T1041" id="Seg_1318" n="e" s="T1040">zajatsən </ts>
               <ts e="T1042" id="Seg_1320" n="e" s="T1041">ibi </ts>
               <ts e="T1043" id="Seg_1322" n="e" s="T1042">(pa-) </ts>
               <ts e="T1044" id="Seg_1324" n="e" s="T1043">pagəʔ. </ts>
               <ts e="T1045" id="Seg_1326" n="e" s="T1044">Dĭgəttə </ts>
               <ts e="T1046" id="Seg_1328" n="e" s="T1045">šobi </ts>
               <ts e="T1047" id="Seg_1330" n="e" s="T1046">ejü. </ts>
               <ts e="T1048" id="Seg_1332" n="e" s="T1047">Lisan </ts>
               <ts e="T1049" id="Seg_1334" n="e" s="T1048">turat </ts>
               <ts e="T1050" id="Seg_1336" n="e" s="T1049">bar </ts>
               <ts e="T1051" id="Seg_1338" n="e" s="T1050">mʼaŋŋuʔpi. </ts>
               <ts e="T1052" id="Seg_1340" n="e" s="T1051">Dĭgəttə </ts>
               <ts e="T1053" id="Seg_1342" n="e" s="T1052">dĭ </ts>
               <ts e="T1054" id="Seg_1344" n="e" s="T1053">šobi </ts>
               <ts e="T1055" id="Seg_1346" n="e" s="T1054">zajatsənə. </ts>
               <ts e="T1056" id="Seg_1348" n="e" s="T1055">"Öʔləʔ </ts>
               <ts e="T1057" id="Seg_1350" n="e" s="T1056">măna </ts>
               <ts e="T1058" id="Seg_1352" n="e" s="T1057">kunolzittə". </ts>
               <ts e="T1059" id="Seg_1354" n="e" s="T1058">Dĭ </ts>
               <ts e="T1060" id="Seg_1356" n="e" s="T1059">bostə </ts>
               <ts e="T1061" id="Seg_1358" n="e" s="T1060">dĭm </ts>
               <ts e="T1062" id="Seg_1360" n="e" s="T1061">sürerlüʔpi </ts>
               <ts e="T1063" id="Seg_1362" n="e" s="T1062">turagəʔ. </ts>
               <ts e="T1064" id="Seg_1364" n="e" s="T1063">Dĭ </ts>
               <ts e="T1065" id="Seg_1366" n="e" s="T1064">šonəga </ts>
               <ts e="T1066" id="Seg_1368" n="e" s="T1065">i </ts>
               <ts e="T1067" id="Seg_1370" n="e" s="T1066">dʼorlaʔbə. </ts>
               <ts e="T1068" id="Seg_1372" n="e" s="T1067">(Măn=) </ts>
               <ts e="T1069" id="Seg_1374" n="e" s="T1068">Men </ts>
               <ts e="T1070" id="Seg_1376" n="e" s="T1069">măndə:" </ts>
               <ts e="T1071" id="Seg_1378" n="e" s="T1070">Ĭmbi </ts>
               <ts e="T1072" id="Seg_1380" n="e" s="T1071">tăn </ts>
               <ts e="T1073" id="Seg_1382" n="e" s="T1072">dʼorlaʔbəl?" </ts>
               <ts e="T1074" id="Seg_1384" n="e" s="T1073">"Da </ts>
               <ts e="T1075" id="Seg_1386" n="e" s="T1074">măn </ts>
               <ts e="T1076" id="Seg_1388" n="e" s="T1075">turagəʔ, </ts>
               <ts e="T1077" id="Seg_1390" n="e" s="T1076">măndə, </ts>
               <ts e="T1078" id="Seg_1392" n="e" s="T1077">sürerbi </ts>
               <ts e="T1079" id="Seg_1394" n="e" s="T1078">lisička". </ts>
               <ts e="T1080" id="Seg_1396" n="e" s="T1079">"Kanžəbəj, </ts>
               <ts e="T1081" id="Seg_1398" n="e" s="T1080">măn </ts>
               <ts e="T1082" id="Seg_1400" n="e" s="T1081">dĭm </ts>
               <ts e="T1083" id="Seg_1402" n="e" s="T1082">sürerlim!" </ts>
               <ts e="T1084" id="Seg_1404" n="e" s="T1083">Šobi </ts>
               <ts e="T1085" id="Seg_1406" n="e" s="T1084">men. </ts>
               <ts e="T1086" id="Seg_1408" n="e" s="T1085">Da </ts>
               <ts e="T1087" id="Seg_1410" n="e" s="T1086">üge </ts>
               <ts e="T1088" id="Seg_1412" n="e" s="T1087">kirgarlaʔbə. </ts>
               <ts e="T1089" id="Seg_1414" n="e" s="T1088">"Kanaʔ </ts>
               <ts e="T1090" id="Seg_1416" n="e" s="T1089">döʔə!" </ts>
               <ts e="T1091" id="Seg_1418" n="e" s="T1090">A </ts>
               <ts e="T1092" id="Seg_1420" n="e" s="T1091">dĭ </ts>
               <ts e="T1093" id="Seg_1422" n="e" s="T1092">măndə:" </ts>
               <ts e="T1094" id="Seg_1424" n="e" s="T1093">Tüjö </ts>
               <ts e="T1095" id="Seg_1426" n="e" s="T1094">tăn </ts>
               <ts e="T1096" id="Seg_1428" n="e" s="T1095">bar </ts>
               <ts e="T1097" id="Seg_1430" n="e" s="T1096">tărlə </ts>
               <ts e="T1098" id="Seg_1432" n="e" s="T1097">nĭŋgələm, </ts>
               <ts e="T1099" id="Seg_1434" n="e" s="T1098">i </ts>
               <ts e="T1100" id="Seg_1436" n="e" s="T1099">tăn </ts>
               <ts e="T1101" id="Seg_1438" n="e" s="T1100">nuʔməluʔləl". </ts>
               <ts e="T1102" id="Seg_1440" n="e" s="T1101">Dĭ </ts>
               <ts e="T1103" id="Seg_1442" n="e" s="T1102">nereʔluʔpi </ts>
               <ts e="T1104" id="Seg_1444" n="e" s="T1103">i </ts>
               <ts e="T1105" id="Seg_1446" n="e" s="T1104">nuʔməluʔpi. </ts>
               <ts e="T1106" id="Seg_1448" n="e" s="T1105">Bazoʔ </ts>
               <ts e="T1107" id="Seg_1450" n="e" s="T1106">kandəga </ts>
               <ts e="T1108" id="Seg_1452" n="e" s="T1107">zajats </ts>
               <ts e="T1109" id="Seg_1454" n="e" s="T1108">i </ts>
               <ts e="T1110" id="Seg_1456" n="e" s="T1109">dʼorlaʔbə. </ts>
               <ts e="T1111" id="Seg_1458" n="e" s="T1110">Dĭgəttə </ts>
               <ts e="T1112" id="Seg_1460" n="e" s="T1111">šonəga </ts>
               <ts e="T1113" id="Seg_1462" n="e" s="T1112">urgaːba. </ts>
               <ts e="T1114" id="Seg_1464" n="e" s="T1113">"Ĭmbi </ts>
               <ts e="T1115" id="Seg_1466" n="e" s="T1114">dʼorlaʔbəl?" </ts>
               <ts e="T1116" id="Seg_1468" n="e" s="T1115">"Da </ts>
               <ts e="T1117" id="Seg_1470" n="e" s="T1116">kak </ts>
               <ts e="T1118" id="Seg_1472" n="e" s="T1117">măna </ts>
               <ts e="T1119" id="Seg_1474" n="e" s="T1118">ej </ts>
               <ts e="T1120" id="Seg_1476" n="e" s="T1119">dʼorzittə. </ts>
               <ts e="T1121" id="Seg_1478" n="e" s="T1120">Măn </ts>
               <ts e="T1122" id="Seg_1480" n="e" s="T1121">turam </ts>
               <ts e="T1123" id="Seg_1482" n="e" s="T1122">ibi </ts>
               <ts e="T1124" id="Seg_1484" n="e" s="T1123">pagəʔ. </ts>
               <ts e="T1125" id="Seg_1486" n="e" s="T1124">A </ts>
               <ts e="T1126" id="Seg_1488" n="e" s="T1125">lisitsan </ts>
               <ts e="T1127" id="Seg_1490" n="e" s="T1126">turat </ts>
               <ts e="T1128" id="Seg_1492" n="e" s="T1127">bɨl </ts>
               <ts e="T1129" id="Seg_1494" n="e" s="T1128">sĭregəʔ. </ts>
               <ts e="T1130" id="Seg_1496" n="e" s="T1129">Tüj </ts>
               <ts e="T1131" id="Seg_1498" n="e" s="T1130">dĭ </ts>
               <ts e="T1132" id="Seg_1500" n="e" s="T1131">šobi </ts>
               <ts e="T1133" id="Seg_1502" n="e" s="T1132">măna </ts>
               <ts e="T1134" id="Seg_1504" n="e" s="T1133">šaːsʼtə </ts>
               <ts e="T1135" id="Seg_1506" n="e" s="T1134">i </ts>
               <ts e="T1136" id="Seg_1508" n="e" s="T1135">măna </ts>
               <ts e="T1137" id="Seg_1510" n="e" s="T1136">sürerlüʔpi". </ts>
               <ts e="T1138" id="Seg_1512" n="e" s="T1137">"Kanžəbəj, </ts>
               <ts e="T1139" id="Seg_1514" n="e" s="T1138">măn </ts>
               <ts e="T1140" id="Seg_1516" n="e" s="T1139">dĭm </ts>
               <ts e="T1141" id="Seg_1518" n="e" s="T1140">sürerlim!" </ts>
               <ts e="T1142" id="Seg_1520" n="e" s="T1141">Šobi. </ts>
               <ts e="T1143" id="Seg_1522" n="e" s="T1142">"Kanaʔ </ts>
               <ts e="T1144" id="Seg_1524" n="e" s="T1143">döʔə! </ts>
               <ts e="T1145" id="Seg_1526" n="e" s="T1144">A_to </ts>
               <ts e="T1146" id="Seg_1528" n="e" s="T1145">tüjö </ts>
               <ts e="T1147" id="Seg_1530" n="e" s="T1146">tănan </ts>
               <ts e="T1148" id="Seg_1532" n="e" s="T1147">münörlam!" </ts>
               <ts e="T1149" id="Seg_1534" n="e" s="T1148">A </ts>
               <ts e="T1150" id="Seg_1536" n="e" s="T1149">dĭ </ts>
               <ts e="T1151" id="Seg_1538" n="e" s="T1150">măndə:" </ts>
               <ts e="T1152" id="Seg_1540" n="e" s="T1151">Măn </ts>
               <ts e="T1153" id="Seg_1542" n="e" s="T1152">kak </ts>
               <ts e="T1154" id="Seg_1544" n="e" s="T1153">nuʔməluʔləm </ts>
               <ts e="T1155" id="Seg_1546" n="e" s="T1154">da. </ts>
               <ts e="T1156" id="Seg_1548" n="e" s="T1155">Bar </ts>
               <ts e="T1157" id="Seg_1550" n="e" s="T1156">tărdə </ts>
               <ts e="T1158" id="Seg_1552" n="e" s="T1157">tăn </ts>
               <ts e="T1159" id="Seg_1554" n="e" s="T1158">nĭŋgəluʔləl. </ts>
               <ts e="T1160" id="Seg_1556" n="e" s="T1159">I </ts>
               <ts e="T1161" id="Seg_1558" n="e" s="T1160">tăn </ts>
               <ts e="T1162" id="Seg_1560" n="e" s="T1161">nuʔməluʔləl". </ts>
               <ts e="T1163" id="Seg_1562" n="e" s="T1162">Dĭgəttə </ts>
               <ts e="T1164" id="Seg_1564" n="e" s="T1163">urgaːba </ts>
               <ts e="T1165" id="Seg_1566" n="e" s="T1164">(-aba) </ts>
               <ts e="T1166" id="Seg_1568" n="e" s="T1165">pimnuʔpi </ts>
               <ts e="T1167" id="Seg_1570" n="e" s="T1166">i </ts>
               <ts e="T1168" id="Seg_1572" n="e" s="T1167">nuʔməluʔpi. </ts>
               <ts e="T1169" id="Seg_1574" n="e" s="T1168">Bazoʔ </ts>
               <ts e="T1170" id="Seg_1576" n="e" s="T1169">kandəga </ts>
               <ts e="T1171" id="Seg_1578" n="e" s="T1170">zajčik </ts>
               <ts e="T1172" id="Seg_1580" n="e" s="T1171">i </ts>
               <ts e="T1173" id="Seg_1582" n="e" s="T1172">dʼorlaʔbə. </ts>
               <ts e="T1174" id="Seg_1584" n="e" s="T1173">Dĭgəttə </ts>
               <ts e="T1175" id="Seg_1586" n="e" s="T1174">buga </ts>
               <ts e="T1176" id="Seg_1588" n="e" s="T1175">šonəga. </ts>
               <ts e="T1177" id="Seg_1590" n="e" s="T1176">"Ĭmbi </ts>
               <ts e="T1178" id="Seg_1592" n="e" s="T1177">dʼorlaʔbəl?" </ts>
               <ts e="T1179" id="Seg_1594" n="e" s="T1178">"Da </ts>
               <ts e="T1180" id="Seg_1596" n="e" s="T1179">măn </ts>
               <ts e="T1181" id="Seg_1598" n="e" s="T1180">turam </ts>
               <ts e="T1182" id="Seg_1600" n="e" s="T1181">lisitsa </ts>
               <ts e="T1183" id="Seg_1602" n="e" s="T1182">ibi. </ts>
               <ts e="T1184" id="Seg_1604" n="e" s="T1183">A </ts>
               <ts e="T1185" id="Seg_1606" n="e" s="T1184">dĭn </ts>
               <ts e="T1186" id="Seg_1608" n="e" s="T1185">(i-) </ts>
               <ts e="T1187" id="Seg_1610" n="e" s="T1186">ibi </ts>
               <ts e="T1188" id="Seg_1612" n="e" s="T1187">sĭregəʔ. </ts>
               <ts e="T1189" id="Seg_1614" n="e" s="T1188">I </ts>
               <ts e="T1190" id="Seg_1616" n="e" s="T1189">külaːmbi, </ts>
               <ts e="T1191" id="Seg_1618" n="e" s="T1190">a </ts>
               <ts e="T1192" id="Seg_1620" n="e" s="T1191">măn </ts>
               <ts e="T1193" id="Seg_1622" n="e" s="T1192">üge </ts>
               <ts e="T1194" id="Seg_1624" n="e" s="T1193">turam </ts>
               <ts e="T1195" id="Seg_1626" n="e" s="T1194">jakšə, </ts>
               <ts e="T1196" id="Seg_1628" n="e" s="T1195">pagəʔ". </ts>
               <ts e="T1197" id="Seg_1630" n="e" s="T1196">Dĭgəttə:" </ts>
               <ts e="T1198" id="Seg_1632" n="e" s="T1197">Kanžəbəj, </ts>
               <ts e="T1199" id="Seg_1634" n="e" s="T1198">măn </ts>
               <ts e="T1200" id="Seg_1636" n="e" s="T1199">dĭm </ts>
               <ts e="T1201" id="Seg_1638" n="e" s="T1200">sürerim". </ts>
               <ts e="T1202" id="Seg_1640" n="e" s="T1201">"Dʼok, </ts>
               <ts e="T1203" id="Seg_1642" n="e" s="T1202">ej </ts>
               <ts e="T1204" id="Seg_1644" n="e" s="T1203">sürerləl. </ts>
               <ts e="T1205" id="Seg_1646" n="e" s="T1204">Urgaːba </ts>
               <ts e="T1206" id="Seg_1648" n="e" s="T1205">ej </ts>
               <ts e="T1207" id="Seg_1650" n="e" s="T1206">mobi </ts>
               <ts e="T1208" id="Seg_1652" n="e" s="T1207">sürerzittə. </ts>
               <ts e="T1209" id="Seg_1654" n="e" s="T1208">Men </ts>
               <ts e="T1210" id="Seg_1656" n="e" s="T1209">ej </ts>
               <ts e="T1211" id="Seg_1658" n="e" s="T1210">mobi. </ts>
               <ts e="T1212" id="Seg_1660" n="e" s="T1211">Buga, </ts>
               <ts e="T1213" id="Seg_1662" n="e" s="T1212">a </ts>
               <ts e="T1214" id="Seg_1664" n="e" s="T1213">tăn </ts>
               <ts e="T1215" id="Seg_1666" n="e" s="T1214">vovsʼe". </ts>
               <ts e="T1216" id="Seg_1668" n="e" s="T1215">Dĭgəttə </ts>
               <ts e="T1217" id="Seg_1670" n="e" s="T1216">(šo-) </ts>
               <ts e="T1218" id="Seg_1672" n="e" s="T1217">šobiʔi. </ts>
               <ts e="T1219" id="Seg_1674" n="e" s="T1218">Šobiʔi, </ts>
               <ts e="T1220" id="Seg_1676" n="e" s="T1219">(bu-) </ts>
               <ts e="T1221" id="Seg_1678" n="e" s="T1220">buga </ts>
               <ts e="T1222" id="Seg_1680" n="e" s="T1221">măndə: </ts>
               <ts e="T1223" id="Seg_1682" n="e" s="T1222">"(Kaga- </ts>
               <ts e="T1224" id="Seg_1684" n="e" s="T1223">kan-) </ts>
               <ts e="T1225" id="Seg_1686" n="e" s="T1224">Kanaʔ </ts>
               <ts e="T1226" id="Seg_1688" n="e" s="T1225">turagəʔ!" </ts>
               <ts e="T1227" id="Seg_1690" n="e" s="T1226">A </ts>
               <ts e="T1228" id="Seg_1692" n="e" s="T1227">dĭ </ts>
               <ts e="T1229" id="Seg_1694" n="e" s="T1228">măndə:" </ts>
               <ts e="T1230" id="Seg_1696" n="e" s="T1229">Tüjö </ts>
               <ts e="T1231" id="Seg_1698" n="e" s="T1230">suʔmiluʔləm </ts>
               <ts e="T1232" id="Seg_1700" n="e" s="T1231">dak, </ts>
               <ts e="T1233" id="Seg_1702" n="e" s="T1232">bar </ts>
               <ts e="T1234" id="Seg_1704" n="e" s="T1233">tănan </ts>
               <ts e="T1235" id="Seg_1706" n="e" s="T1234">saj </ts>
               <ts e="T1236" id="Seg_1708" n="e" s="T1235">nožuʔləm". </ts>
               <ts e="T1237" id="Seg_1710" n="e" s="T1236">Dĭgəttə </ts>
               <ts e="T1238" id="Seg_1712" n="e" s="T1237">(buga=) </ts>
               <ts e="T1239" id="Seg_1714" n="e" s="T1238">buga </ts>
               <ts e="T1240" id="Seg_1716" n="e" s="T1239">nerelüʔpi </ts>
               <ts e="T1241" id="Seg_1718" n="e" s="T1240">i </ts>
               <ts e="T1242" id="Seg_1720" n="e" s="T1241">nuʔməluʔpi. </ts>
               <ts e="T1243" id="Seg_1722" n="e" s="T1242">Dĭgəttə </ts>
               <ts e="T1244" id="Seg_1724" n="e" s="T1243">bazoʔ </ts>
               <ts e="T1245" id="Seg_1726" n="e" s="T1244">kandəga </ts>
               <ts e="T1246" id="Seg_1728" n="e" s="T1245">zajats </ts>
               <ts e="T1247" id="Seg_1730" n="e" s="T1246">i </ts>
               <ts e="T1248" id="Seg_1732" n="e" s="T1247">dʼorlaʔbə. </ts>
               <ts e="T1249" id="Seg_1734" n="e" s="T1248">Dĭgəttə </ts>
               <ts e="T1250" id="Seg_1736" n="e" s="T1249">kurizən </ts>
               <ts e="T1251" id="Seg_1738" n="e" s="T1250">tibi </ts>
               <ts e="T1252" id="Seg_1740" n="e" s="T1251">šonəga, </ts>
               <ts e="T1253" id="Seg_1742" n="e" s="T1252">šapkoziʔ. </ts>
               <ts e="T1254" id="Seg_1744" n="e" s="T1253">Măndə:" </ts>
               <ts e="T1255" id="Seg_1746" n="e" s="T1254">Ĭmbi </ts>
               <ts e="T1256" id="Seg_1748" n="e" s="T1255">dʼorlaʔbəl?" </ts>
               <ts e="T1257" id="Seg_1750" n="e" s="T1256">"Da </ts>
               <ts e="T1258" id="Seg_1752" n="e" s="T1257">măn </ts>
               <ts e="T1259" id="Seg_1754" n="e" s="T1258">turam </ts>
               <ts e="T1260" id="Seg_1756" n="e" s="T1259">pagəʔ. </ts>
               <ts e="T1261" id="Seg_1758" n="e" s="T1260">Lisitsa </ts>
               <ts e="T1262" id="Seg_1760" n="e" s="T1261">šobi </ts>
               <ts e="T1263" id="Seg_1762" n="e" s="T1262">kunolzittə </ts>
               <ts e="T1264" id="Seg_1764" n="e" s="T1263">i </ts>
               <ts e="T1265" id="Seg_1766" n="e" s="T1264">măna </ts>
               <ts e="T1266" id="Seg_1768" n="e" s="T1265">sürerbi. </ts>
               <ts e="T1267" id="Seg_1770" n="e" s="T1266">A </ts>
               <ts e="T1268" id="Seg_1772" n="e" s="T1267">dĭn </ts>
               <ts e="T1269" id="Seg_1774" n="e" s="T1268">sĭregəʔ </ts>
               <ts e="T1270" id="Seg_1776" n="e" s="T1269">ibi </ts>
               <ts e="T1271" id="Seg_1778" n="e" s="T1270">i </ts>
               <ts e="T1272" id="Seg_1780" n="e" s="T1271">tüj </ts>
               <ts e="T1273" id="Seg_1782" n="e" s="T1272">naga". </ts>
               <ts e="T1274" id="Seg_1784" n="e" s="T1273">Dĭgəttə:" </ts>
               <ts e="T1275" id="Seg_1786" n="e" s="T1274">Kanžəbəj, </ts>
               <ts e="T1276" id="Seg_1788" n="e" s="T1275">măn </ts>
               <ts e="T1277" id="Seg_1790" n="e" s="T1276">sürerləm". </ts>
               <ts e="T1278" id="Seg_1792" n="e" s="T1277">"Men </ts>
               <ts e="T1279" id="Seg_1794" n="e" s="T1278">ej </ts>
               <ts e="T1280" id="Seg_1796" n="e" s="T1279">mobi </ts>
               <ts e="T1281" id="Seg_1798" n="e" s="T1280">sürerzittə. </ts>
               <ts e="T1282" id="Seg_1800" n="e" s="T1281">Dĭ </ts>
               <ts e="T1283" id="Seg_1802" n="e" s="T1282">urgaːba </ts>
               <ts e="T1284" id="Seg_1804" n="e" s="T1283">i </ts>
               <ts e="T1285" id="Seg_1806" n="e" s="T1284">buga </ts>
               <ts e="T1286" id="Seg_1808" n="e" s="T1285">ej </ts>
               <ts e="T1287" id="Seg_1810" n="e" s="T1286">mobi. </ts>
               <ts e="T1288" id="Seg_1812" n="e" s="T1287">A </ts>
               <ts e="T1289" id="Seg_1814" n="e" s="T1288">tăn </ts>
               <ts e="T1290" id="Seg_1816" n="e" s="T1289">ej </ts>
               <ts e="T1291" id="Seg_1818" n="e" s="T1290">(sürerləl)". </ts>
               <ts e="T1292" id="Seg_1820" n="e" s="T1291">"Kanžəbəj!" </ts>
               <ts e="T1293" id="Seg_1822" n="e" s="T1292">Dĭgəttə </ts>
               <ts e="T1294" id="Seg_1824" n="e" s="T1293">šobi. </ts>
               <ts e="T1295" id="Seg_1826" n="e" s="T1294">Bar </ts>
               <ts e="T1296" id="Seg_1828" n="e" s="T1295">kirgarlaʔbə: </ts>
               <ts e="T1297" id="Seg_1830" n="e" s="T1296">"Uʔbdəʔ </ts>
               <ts e="T1298" id="Seg_1832" n="e" s="T1297">pʼešgəʔ </ts>
               <ts e="T1299" id="Seg_1834" n="e" s="T1298">(s- </ts>
               <ts e="T1300" id="Seg_1836" n="e" s="T1299">š-) </ts>
               <ts e="T1301" id="Seg_1838" n="e" s="T1300">kanaʔ </ts>
               <ts e="T1302" id="Seg_1840" n="e" s="T1301">(döʔə)! </ts>
               <ts e="T1304" id="Seg_1842" n="e" s="T1302">A_to </ts>
               <ts e="T1305" id="Seg_1844" n="e" s="T1304">tüjö </ts>
               <ts e="T1306" id="Seg_1846" n="e" s="T1305">šapkuziʔ </ts>
               <ts e="T1307" id="Seg_1848" n="e" s="T1306">tănan </ts>
               <ts e="T1308" id="Seg_1850" n="e" s="T1307">kutlam! </ts>
               <ts e="T1309" id="Seg_1852" n="e" s="T1308">Bar </ts>
               <ts e="T1310" id="Seg_1854" n="e" s="T1309">dʼăgarləm </ts>
               <ts e="T1311" id="Seg_1856" n="e" s="T1310">tănan. </ts>
               <ts e="T1312" id="Seg_1858" n="e" s="T1311">"Tüjö, </ts>
               <ts e="T1313" id="Seg_1860" n="e" s="T1312">(ujum=) </ts>
               <ts e="T1314" id="Seg_1862" n="e" s="T1313">ujuʔi </ts>
               <ts e="T1315" id="Seg_1864" n="e" s="T1314">šeriem </ts>
               <ts e="T1316" id="Seg_1866" n="e" s="T1315">jamaʔi". </ts>
               <ts e="T1317" id="Seg_1868" n="e" s="T1316">Dĭgəttə </ts>
               <ts e="T1318" id="Seg_1870" n="e" s="T1317">bazoʔ </ts>
               <ts e="T1319" id="Seg_1872" n="e" s="T1318">kirgarlaʔbə: </ts>
               <ts e="T1320" id="Seg_1874" n="e" s="T1319">"Kanaʔ </ts>
               <ts e="T1321" id="Seg_1876" n="e" s="T1320">pʼešgəʔ, </ts>
               <ts e="T1322" id="Seg_1878" n="e" s="T1321">a_to </ts>
               <ts e="T1323" id="Seg_1880" n="e" s="T1322">tüjö </ts>
               <ts e="T1324" id="Seg_1882" n="e" s="T1323">tănan </ts>
               <ts e="T1325" id="Seg_1884" n="e" s="T1324">kutlam. </ts>
               <ts e="T1326" id="Seg_1886" n="e" s="T1325">(Tagaj=) </ts>
               <ts e="T1327" id="Seg_1888" n="e" s="T1326">Šapkuziʔ </ts>
               <ts e="T1328" id="Seg_1890" n="e" s="T1327">bar </ts>
               <ts e="T1329" id="Seg_1892" n="e" s="T1328">bătlim." </ts>
               <ts e="T1330" id="Seg_1894" n="e" s="T1329">"Tüjö </ts>
               <ts e="T1331" id="Seg_1896" n="e" s="T1330">bar </ts>
               <ts e="T1332" id="Seg_1898" n="e" s="T1331">oldʼam </ts>
               <ts e="T1333" id="Seg_1900" n="e" s="T1332">šeriom." </ts>
               <ts e="T1334" id="Seg_1902" n="e" s="T1333">Dĭgəttə </ts>
               <ts e="T1335" id="Seg_1904" n="e" s="T1334">dĭ </ts>
               <ts e="T1336" id="Seg_1906" n="e" s="T1335">bazoʔ </ts>
               <ts e="T1337" id="Seg_1908" n="e" s="T1336">kirgarlaʔbə:" </ts>
               <ts e="T1338" id="Seg_1910" n="e" s="T1337">Kanaʔ!" </ts>
               <ts e="T1339" id="Seg_1912" n="e" s="T1338">"Tüjö </ts>
               <ts e="T1340" id="Seg_1914" n="e" s="T1339">nuʔməleʔbəm!" </ts>
               <ts e="T1341" id="Seg_1916" n="e" s="T1340">Dĭgəttə </ts>
               <ts e="T1342" id="Seg_1918" n="e" s="T1341">dĭ </ts>
               <ts e="T1343" id="Seg_1920" n="e" s="T1342">šapkuziʔ </ts>
               <ts e="T1344" id="Seg_1922" n="e" s="T1343">dĭm </ts>
               <ts e="T1345" id="Seg_1924" n="e" s="T1344">bar </ts>
               <ts e="T1346" id="Seg_1926" n="e" s="T1345">dʼăgarluʔpi. </ts>
               <ts e="T1347" id="Seg_1928" n="e" s="T1346">I </ts>
               <ts e="T1348" id="Seg_1930" n="e" s="T1347">dĭ </ts>
               <ts e="T1349" id="Seg_1932" n="e" s="T1348">külaːmbi. </ts>
               <ts e="T1350" id="Seg_1934" n="e" s="T1349">Dĭgəttə </ts>
               <ts e="T1351" id="Seg_1936" n="e" s="T1350">amnobiʔi </ts>
               <ts e="T1352" id="Seg_1938" n="e" s="T1351">kurizən </ts>
               <ts e="T1353" id="Seg_1940" n="e" s="T1352">tibi </ts>
               <ts e="T1354" id="Seg_1942" n="e" s="T1353">i </ts>
               <ts e="T1355" id="Seg_1944" n="e" s="T1354">zajats </ts>
               <ts e="T1356" id="Seg_1946" n="e" s="T1355">turagən. </ts>
               <ts e="T1357" id="Seg_1948" n="e" s="T1356">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1035" id="Seg_1949" s="T1031">PKZ_196X_FoxAndHare_flk.001 (001)</ta>
            <ta e="T1039" id="Seg_1950" s="T1035">PKZ_196X_FoxAndHare_flk.002 (002)</ta>
            <ta e="T1044" id="Seg_1951" s="T1039">PKZ_196X_FoxAndHare_flk.003 (003)</ta>
            <ta e="T1047" id="Seg_1952" s="T1044">PKZ_196X_FoxAndHare_flk.004 (004)</ta>
            <ta e="T1051" id="Seg_1953" s="T1047">PKZ_196X_FoxAndHare_flk.005 (005)</ta>
            <ta e="T1055" id="Seg_1954" s="T1051">PKZ_196X_FoxAndHare_flk.006 (006)</ta>
            <ta e="T1058" id="Seg_1955" s="T1055">PKZ_196X_FoxAndHare_flk.007 (007)</ta>
            <ta e="T1063" id="Seg_1956" s="T1058">PKZ_196X_FoxAndHare_flk.008 (008)</ta>
            <ta e="T1067" id="Seg_1957" s="T1063">PKZ_196X_FoxAndHare_flk.009 (009)</ta>
            <ta e="T1073" id="Seg_1958" s="T1067">PKZ_196X_FoxAndHare_flk.010 (010)</ta>
            <ta e="T1079" id="Seg_1959" s="T1073">PKZ_196X_FoxAndHare_flk.011 (011)</ta>
            <ta e="T1083" id="Seg_1960" s="T1079">PKZ_196X_FoxAndHare_flk.012 (012)</ta>
            <ta e="T1085" id="Seg_1961" s="T1083">PKZ_196X_FoxAndHare_flk.013 (013)</ta>
            <ta e="T1088" id="Seg_1962" s="T1085">PKZ_196X_FoxAndHare_flk.014 (014)</ta>
            <ta e="T1090" id="Seg_1963" s="T1088">PKZ_196X_FoxAndHare_flk.015 (015)</ta>
            <ta e="T1101" id="Seg_1964" s="T1090">PKZ_196X_FoxAndHare_flk.016 (016)</ta>
            <ta e="T1105" id="Seg_1965" s="T1101">PKZ_196X_FoxAndHare_flk.017 (017)</ta>
            <ta e="T1110" id="Seg_1966" s="T1105">PKZ_196X_FoxAndHare_flk.018 (018)</ta>
            <ta e="T1113" id="Seg_1967" s="T1110">PKZ_196X_FoxAndHare_flk.019 (019)</ta>
            <ta e="T1115" id="Seg_1968" s="T1113">PKZ_196X_FoxAndHare_flk.020 (020)</ta>
            <ta e="T1120" id="Seg_1969" s="T1115">PKZ_196X_FoxAndHare_flk.021 (021)</ta>
            <ta e="T1124" id="Seg_1970" s="T1120">PKZ_196X_FoxAndHare_flk.022 (022)</ta>
            <ta e="T1129" id="Seg_1971" s="T1124">PKZ_196X_FoxAndHare_flk.023 (023)</ta>
            <ta e="T1137" id="Seg_1972" s="T1129">PKZ_196X_FoxAndHare_flk.024 (024)</ta>
            <ta e="T1141" id="Seg_1973" s="T1137">PKZ_196X_FoxAndHare_flk.025 (025)</ta>
            <ta e="T1142" id="Seg_1974" s="T1141">PKZ_196X_FoxAndHare_flk.026 (026)</ta>
            <ta e="T1144" id="Seg_1975" s="T1142">PKZ_196X_FoxAndHare_flk.027 (027)</ta>
            <ta e="T1148" id="Seg_1976" s="T1144">PKZ_196X_FoxAndHare_flk.028 (028)</ta>
            <ta e="T1155" id="Seg_1977" s="T1148">PKZ_196X_FoxAndHare_flk.029 (029)</ta>
            <ta e="T1159" id="Seg_1978" s="T1155">PKZ_196X_FoxAndHare_flk.030 (030)</ta>
            <ta e="T1162" id="Seg_1979" s="T1159">PKZ_196X_FoxAndHare_flk.031 (031)</ta>
            <ta e="T1168" id="Seg_1980" s="T1162">PKZ_196X_FoxAndHare_flk.032 (032)</ta>
            <ta e="T1173" id="Seg_1981" s="T1168">PKZ_196X_FoxAndHare_flk.033 (033)</ta>
            <ta e="T1176" id="Seg_1982" s="T1173">PKZ_196X_FoxAndHare_flk.034 (034)</ta>
            <ta e="T1178" id="Seg_1983" s="T1176">PKZ_196X_FoxAndHare_flk.035 (035)</ta>
            <ta e="T1183" id="Seg_1984" s="T1178">PKZ_196X_FoxAndHare_flk.036 (036)</ta>
            <ta e="T1188" id="Seg_1985" s="T1183">PKZ_196X_FoxAndHare_flk.037 (037)</ta>
            <ta e="T1196" id="Seg_1986" s="T1188">PKZ_196X_FoxAndHare_flk.038 (038)</ta>
            <ta e="T1201" id="Seg_1987" s="T1196">PKZ_196X_FoxAndHare_flk.039 (039)</ta>
            <ta e="T1204" id="Seg_1988" s="T1201">PKZ_196X_FoxAndHare_flk.040 (040)</ta>
            <ta e="T1208" id="Seg_1989" s="T1204">PKZ_196X_FoxAndHare_flk.041 (041)</ta>
            <ta e="T1211" id="Seg_1990" s="T1208">PKZ_196X_FoxAndHare_flk.042 (042)</ta>
            <ta e="T1215" id="Seg_1991" s="T1211">PKZ_196X_FoxAndHare_flk.043 (043)</ta>
            <ta e="T1218" id="Seg_1992" s="T1215">PKZ_196X_FoxAndHare_flk.044 (044)</ta>
            <ta e="T1226" id="Seg_1993" s="T1218">PKZ_196X_FoxAndHare_flk.045 (045)</ta>
            <ta e="T1236" id="Seg_1994" s="T1226">PKZ_196X_FoxAndHare_flk.046 (046)</ta>
            <ta e="T1242" id="Seg_1995" s="T1236">PKZ_196X_FoxAndHare_flk.047 (047)</ta>
            <ta e="T1248" id="Seg_1996" s="T1242">PKZ_196X_FoxAndHare_flk.048 (048)</ta>
            <ta e="T1253" id="Seg_1997" s="T1248">PKZ_196X_FoxAndHare_flk.049 (049)</ta>
            <ta e="T1256" id="Seg_1998" s="T1253">PKZ_196X_FoxAndHare_flk.050 (050)</ta>
            <ta e="T1260" id="Seg_1999" s="T1256">PKZ_196X_FoxAndHare_flk.051 (051)</ta>
            <ta e="T1266" id="Seg_2000" s="T1260">PKZ_196X_FoxAndHare_flk.052 (052)</ta>
            <ta e="T1273" id="Seg_2001" s="T1266">PKZ_196X_FoxAndHare_flk.053 (053)</ta>
            <ta e="T1277" id="Seg_2002" s="T1273">PKZ_196X_FoxAndHare_flk.054 (054)</ta>
            <ta e="T1281" id="Seg_2003" s="T1277">PKZ_196X_FoxAndHare_flk.055 (055)</ta>
            <ta e="T1287" id="Seg_2004" s="T1281">PKZ_196X_FoxAndHare_flk.056 (056)</ta>
            <ta e="T1291" id="Seg_2005" s="T1287">PKZ_196X_FoxAndHare_flk.057 (057)</ta>
            <ta e="T1292" id="Seg_2006" s="T1291">PKZ_196X_FoxAndHare_flk.058 (058)</ta>
            <ta e="T1294" id="Seg_2007" s="T1292">PKZ_196X_FoxAndHare_flk.059 (059)</ta>
            <ta e="T1302" id="Seg_2008" s="T1294">PKZ_196X_FoxAndHare_flk.060 (060)</ta>
            <ta e="T1308" id="Seg_2009" s="T1302">PKZ_196X_FoxAndHare_flk.061 (062)</ta>
            <ta e="T1311" id="Seg_2010" s="T1308">PKZ_196X_FoxAndHare_flk.062 (063)</ta>
            <ta e="T1316" id="Seg_2011" s="T1311">PKZ_196X_FoxAndHare_flk.063 (064)</ta>
            <ta e="T1325" id="Seg_2012" s="T1316">PKZ_196X_FoxAndHare_flk.064 (065)</ta>
            <ta e="T1329" id="Seg_2013" s="T1325">PKZ_196X_FoxAndHare_flk.065 (067)</ta>
            <ta e="T1333" id="Seg_2014" s="T1329">PKZ_196X_FoxAndHare_flk.066 (068)</ta>
            <ta e="T1338" id="Seg_2015" s="T1333">PKZ_196X_FoxAndHare_flk.067 (069)</ta>
            <ta e="T1340" id="Seg_2016" s="T1338">PKZ_196X_FoxAndHare_flk.068 (070)</ta>
            <ta e="T1346" id="Seg_2017" s="T1340">PKZ_196X_FoxAndHare_flk.069 (071)</ta>
            <ta e="T1349" id="Seg_2018" s="T1346">PKZ_196X_FoxAndHare_flk.070 (072)</ta>
            <ta e="T1356" id="Seg_2019" s="T1349">PKZ_196X_FoxAndHare_flk.071 (073)</ta>
            <ta e="T1357" id="Seg_2020" s="T1356">PKZ_196X_FoxAndHare_flk.072 (074)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1035" id="Seg_2021" s="T1031">Amnobiʔi lisa i zajats. </ta>
            <ta e="T1039" id="Seg_2022" s="T1035">Lisan ibi turat sĭregəʔ. </ta>
            <ta e="T1044" id="Seg_2023" s="T1039">A zajatsən ibi (pa-) pagəʔ. </ta>
            <ta e="T1047" id="Seg_2024" s="T1044">Dĭgəttə šobi ejü. </ta>
            <ta e="T1051" id="Seg_2025" s="T1047">Lisan turat bar mʼaŋŋuʔpi. </ta>
            <ta e="T1055" id="Seg_2026" s="T1051">Dĭgəttə dĭ šobi zajatsənə. </ta>
            <ta e="T1058" id="Seg_2027" s="T1055">"Öʔləʔ măna kunolzittə". </ta>
            <ta e="T1063" id="Seg_2028" s="T1058">Dĭ bostə dĭm sürerlüʔpi turagəʔ. </ta>
            <ta e="T1067" id="Seg_2029" s="T1063">Dĭ šonəga i dʼorlaʔbə. </ta>
            <ta e="T1073" id="Seg_2030" s="T1067">(Măn=) Men măndə:" Ĭmbi tăn dʼorlaʔbəl?" </ta>
            <ta e="T1079" id="Seg_2031" s="T1073">"Da măn turagəʔ, măndə, sürerbi lisička". </ta>
            <ta e="T1083" id="Seg_2032" s="T1079">"Kanžəbəj, măn dĭm sürerlim!" </ta>
            <ta e="T1085" id="Seg_2033" s="T1083">Šobi men. </ta>
            <ta e="T1088" id="Seg_2034" s="T1085">Da üge kirgarlaʔbə. </ta>
            <ta e="T1090" id="Seg_2035" s="T1088">"Kanaʔ döʔə!" </ta>
            <ta e="T1101" id="Seg_2036" s="T1090">A dĭ măndə:" Tüjö tăn bar tărlə nĭŋgələm, i tăn nuʔməluʔləl". </ta>
            <ta e="T1105" id="Seg_2037" s="T1101">Dĭ nereʔluʔpi i nuʔməluʔpi. </ta>
            <ta e="T1110" id="Seg_2038" s="T1105">Bazoʔ kandəga zajats i dʼorlaʔbə. </ta>
            <ta e="T1113" id="Seg_2039" s="T1110">Dĭgəttə šonəga urgaːba. </ta>
            <ta e="T1115" id="Seg_2040" s="T1113">"Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T1120" id="Seg_2041" s="T1115">"Da kak măna ej dʼorzittə. </ta>
            <ta e="T1124" id="Seg_2042" s="T1120">Măn turam ibi pagəʔ. </ta>
            <ta e="T1129" id="Seg_2043" s="T1124">A lisitsan turat bɨl sĭregəʔ. </ta>
            <ta e="T1137" id="Seg_2044" s="T1129">Tüj dĭ šobi măna šaːsʼtə i măna sürerlüʔpi". </ta>
            <ta e="T1141" id="Seg_2045" s="T1137">"Kanžəbəj, măn dĭm sürerlim!" </ta>
            <ta e="T1142" id="Seg_2046" s="T1141">Šobi. </ta>
            <ta e="T1144" id="Seg_2047" s="T1142">"Kanaʔ döʔə! </ta>
            <ta e="T1148" id="Seg_2048" s="T1144">A to tüjö tănan münörlam!" </ta>
            <ta e="T1155" id="Seg_2049" s="T1148">A dĭ măndə:" Măn kak nuʔməluʔləm da. </ta>
            <ta e="T1159" id="Seg_2050" s="T1155">Bar tărdə tăn nĭŋgəluʔləl. </ta>
            <ta e="T1162" id="Seg_2051" s="T1159">I tăn nuʔməluʔləl". </ta>
            <ta e="T1168" id="Seg_2052" s="T1162">Dĭgəttə urgaːba (-aba) pimnuʔpi i nuʔməluʔpi. </ta>
            <ta e="T1173" id="Seg_2053" s="T1168">Bazoʔ kandəga zajčik i dʼorlaʔbə. </ta>
            <ta e="T1176" id="Seg_2054" s="T1173">Dĭgəttə buga šonəga. </ta>
            <ta e="T1178" id="Seg_2055" s="T1176">"Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T1183" id="Seg_2056" s="T1178">"Da măn turam lisitsa ibi. </ta>
            <ta e="T1188" id="Seg_2057" s="T1183">A dĭn (i-) ibi sĭregəʔ. </ta>
            <ta e="T1196" id="Seg_2058" s="T1188">I külaːmbi, a măn üge turam jakšə, pagəʔ". </ta>
            <ta e="T1201" id="Seg_2059" s="T1196">Dĭgəttə:" Kanžəbəj, măn dĭm sürerim". </ta>
            <ta e="T1204" id="Seg_2060" s="T1201">"Dʼok, ej sürerləl. </ta>
            <ta e="T1208" id="Seg_2061" s="T1204">Urgaːba ej mobi sürerzittə. </ta>
            <ta e="T1211" id="Seg_2062" s="T1208">Men ej mobi. </ta>
            <ta e="T1215" id="Seg_2063" s="T1211">Buga, a tăn vovsʼe". </ta>
            <ta e="T1218" id="Seg_2064" s="T1215">Dĭgəttə (šo-) šobiʔi. </ta>
            <ta e="T1226" id="Seg_2065" s="T1218">Šobiʔi, (bu-) buga măndə: "(Kaga- kan-) Kanaʔ turagəʔ!" </ta>
            <ta e="T1236" id="Seg_2066" s="T1226">A dĭ măndə:" Tüjö suʔmiluʔləm dak, bar tănan saj nožuʔləm". </ta>
            <ta e="T1242" id="Seg_2067" s="T1236">Dĭgəttə (buga=) buga nerelüʔpi i nuʔməluʔpi. </ta>
            <ta e="T1248" id="Seg_2068" s="T1242">Dĭgəttə bazoʔ kandəga zajats i dʼorlaʔbə. </ta>
            <ta e="T1253" id="Seg_2069" s="T1248">Dĭgəttə kurizən tibi šonəga, šapkoziʔ. </ta>
            <ta e="T1256" id="Seg_2070" s="T1253">Măndə:" Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T1260" id="Seg_2071" s="T1256">"Da măn turam pagəʔ. </ta>
            <ta e="T1266" id="Seg_2072" s="T1260">Lisitsa šobi kunolzittə i măna sürerbi. </ta>
            <ta e="T1273" id="Seg_2073" s="T1266">A dĭn sĭregəʔ ibi i tüj naga". </ta>
            <ta e="T1277" id="Seg_2074" s="T1273">Dĭgəttə:" Kanžəbəj, măn sürerləm". </ta>
            <ta e="T1281" id="Seg_2075" s="T1277">"Men ej mobi sürerzittə. </ta>
            <ta e="T1287" id="Seg_2076" s="T1281">Dĭ urgaːba i buga ej mobi. </ta>
            <ta e="T1291" id="Seg_2077" s="T1287">A tăn ej (sürerləl)". </ta>
            <ta e="T1292" id="Seg_2078" s="T1291">"Kanžəbəj!" </ta>
            <ta e="T1294" id="Seg_2079" s="T1292">Dĭgəttə šobi. </ta>
            <ta e="T1302" id="Seg_2080" s="T1294">Bar kirgarlaʔbə: "Uʔbdəʔ pʼešgəʔ (s- š-) kanaʔ (döʔə)! </ta>
            <ta e="T1308" id="Seg_2081" s="T1302">A to tüjö šapkuziʔ tănan kutlam! </ta>
            <ta e="T1311" id="Seg_2082" s="T1308">Bar dʼăgarləm tănan. </ta>
            <ta e="T1316" id="Seg_2083" s="T1311">"Tüjö, (ujum=) ujuʔi šeriem jamaʔi." </ta>
            <ta e="T1325" id="Seg_2084" s="T1316">Dĭgəttə bazoʔ kirgarlaʔbə: "Kanaʔ pʼešgəʔ, a to tüjö tănan kutlam. </ta>
            <ta e="T1329" id="Seg_2085" s="T1325">(Tagaj=) Šapkuziʔ bar bătlim." </ta>
            <ta e="T1333" id="Seg_2086" s="T1329">"Tüjö bar oldʼam šeriom." </ta>
            <ta e="T1338" id="Seg_2087" s="T1333">Dĭgəttə dĭ bazoʔ kirgarlaʔbə:" Kanaʔ!" </ta>
            <ta e="T1340" id="Seg_2088" s="T1338">"Tüjö nuʔməleʔbəm!" </ta>
            <ta e="T1346" id="Seg_2089" s="T1340">Dĭgəttə dĭ šapkuziʔ dĭm bar dʼăgarluʔpi. </ta>
            <ta e="T1349" id="Seg_2090" s="T1346">I dĭ külaːmbi. </ta>
            <ta e="T1356" id="Seg_2091" s="T1349">Dĭgəttə amnobiʔi kurizən tibi i zajats turagən. </ta>
            <ta e="T1357" id="Seg_2092" s="T1356">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1032" id="Seg_2093" s="T1031">amno-bi-ʔi</ta>
            <ta e="T1033" id="Seg_2094" s="T1032">lisa</ta>
            <ta e="T1034" id="Seg_2095" s="T1033">i</ta>
            <ta e="T1035" id="Seg_2096" s="T1034">zajats</ta>
            <ta e="T1036" id="Seg_2097" s="T1035">lisa-n</ta>
            <ta e="T1037" id="Seg_2098" s="T1036">i-bi</ta>
            <ta e="T1038" id="Seg_2099" s="T1037">tura-t</ta>
            <ta e="T1039" id="Seg_2100" s="T1038">sĭre-gəʔ</ta>
            <ta e="T1040" id="Seg_2101" s="T1039">a</ta>
            <ta e="T1041" id="Seg_2102" s="T1040">zajats-ən</ta>
            <ta e="T1042" id="Seg_2103" s="T1041">i-bi</ta>
            <ta e="T1044" id="Seg_2104" s="T1043">pa-gəʔ</ta>
            <ta e="T1045" id="Seg_2105" s="T1044">dĭgəttə</ta>
            <ta e="T1046" id="Seg_2106" s="T1045">šo-bi</ta>
            <ta e="T1047" id="Seg_2107" s="T1046">ejü</ta>
            <ta e="T1048" id="Seg_2108" s="T1047">lisa-n</ta>
            <ta e="T1049" id="Seg_2109" s="T1048">tura-t</ta>
            <ta e="T1050" id="Seg_2110" s="T1049">bar</ta>
            <ta e="T1051" id="Seg_2111" s="T1050">mʼaŋ-ŋuʔ-pi</ta>
            <ta e="T1052" id="Seg_2112" s="T1051">dĭgəttə</ta>
            <ta e="T1053" id="Seg_2113" s="T1052">dĭ</ta>
            <ta e="T1054" id="Seg_2114" s="T1053">šo-bi</ta>
            <ta e="T1055" id="Seg_2115" s="T1054">zajats-ənə</ta>
            <ta e="T1056" id="Seg_2116" s="T1055">öʔlə-ʔ</ta>
            <ta e="T1057" id="Seg_2117" s="T1056">măna</ta>
            <ta e="T1058" id="Seg_2118" s="T1057">kunol-zittə</ta>
            <ta e="T1059" id="Seg_2119" s="T1058">dĭ</ta>
            <ta e="T1060" id="Seg_2120" s="T1059">bos-tə</ta>
            <ta e="T1061" id="Seg_2121" s="T1060">dĭ-m</ta>
            <ta e="T1062" id="Seg_2122" s="T1061">sürer-lüʔ-pi</ta>
            <ta e="T1063" id="Seg_2123" s="T1062">tura-gəʔ</ta>
            <ta e="T1064" id="Seg_2124" s="T1063">dĭ</ta>
            <ta e="T1065" id="Seg_2125" s="T1064">šonə-ga</ta>
            <ta e="T1066" id="Seg_2126" s="T1065">i</ta>
            <ta e="T1067" id="Seg_2127" s="T1066">dʼor-laʔbə</ta>
            <ta e="T1068" id="Seg_2128" s="T1067">măn</ta>
            <ta e="T1069" id="Seg_2129" s="T1068">men</ta>
            <ta e="T1070" id="Seg_2130" s="T1069">măn-də</ta>
            <ta e="T1071" id="Seg_2131" s="T1070">ĭmbi</ta>
            <ta e="T1072" id="Seg_2132" s="T1071">tăn</ta>
            <ta e="T1073" id="Seg_2133" s="T1072">dʼor-laʔbə-l</ta>
            <ta e="T1074" id="Seg_2134" s="T1073">da</ta>
            <ta e="T1075" id="Seg_2135" s="T1074">măn</ta>
            <ta e="T1076" id="Seg_2136" s="T1075">tura-gəʔ</ta>
            <ta e="T1077" id="Seg_2137" s="T1076">măn-də</ta>
            <ta e="T1078" id="Seg_2138" s="T1077">sürer-bi</ta>
            <ta e="T1079" id="Seg_2139" s="T1078">lisička</ta>
            <ta e="T1080" id="Seg_2140" s="T1079">kan-žə-bəj</ta>
            <ta e="T1081" id="Seg_2141" s="T1080">măn</ta>
            <ta e="T1082" id="Seg_2142" s="T1081">dĭ-m</ta>
            <ta e="T1083" id="Seg_2143" s="T1082">sürer-li-m</ta>
            <ta e="T1084" id="Seg_2144" s="T1083">šo-bi</ta>
            <ta e="T1085" id="Seg_2145" s="T1084">men</ta>
            <ta e="T1086" id="Seg_2146" s="T1085">da</ta>
            <ta e="T1087" id="Seg_2147" s="T1086">üge</ta>
            <ta e="T1088" id="Seg_2148" s="T1087">kirgar-laʔbə</ta>
            <ta e="T1089" id="Seg_2149" s="T1088">kan-a-ʔ</ta>
            <ta e="T1090" id="Seg_2150" s="T1089">döʔə</ta>
            <ta e="T1091" id="Seg_2151" s="T1090">a</ta>
            <ta e="T1092" id="Seg_2152" s="T1091">dĭ</ta>
            <ta e="T1093" id="Seg_2153" s="T1092">măn-də</ta>
            <ta e="T1094" id="Seg_2154" s="T1093">tüjö</ta>
            <ta e="T1095" id="Seg_2155" s="T1094">tăn</ta>
            <ta e="T1096" id="Seg_2156" s="T1095">bar</ta>
            <ta e="T1097" id="Seg_2157" s="T1096">tăr-lə</ta>
            <ta e="T1098" id="Seg_2158" s="T1097">nĭŋgə-lə-m</ta>
            <ta e="T1099" id="Seg_2159" s="T1098">i</ta>
            <ta e="T1100" id="Seg_2160" s="T1099">tăn</ta>
            <ta e="T1101" id="Seg_2161" s="T1100">nuʔmə-luʔ-lə-l</ta>
            <ta e="T1102" id="Seg_2162" s="T1101">dĭ</ta>
            <ta e="T1103" id="Seg_2163" s="T1102">nereʔ-luʔ-pi</ta>
            <ta e="T1104" id="Seg_2164" s="T1103">i</ta>
            <ta e="T1105" id="Seg_2165" s="T1104">nuʔmə-luʔ-pi</ta>
            <ta e="T1106" id="Seg_2166" s="T1105">bazoʔ</ta>
            <ta e="T1107" id="Seg_2167" s="T1106">kandə-ga</ta>
            <ta e="T1108" id="Seg_2168" s="T1107">zajats</ta>
            <ta e="T1109" id="Seg_2169" s="T1108">i</ta>
            <ta e="T1110" id="Seg_2170" s="T1109">dʼor-laʔbə</ta>
            <ta e="T1111" id="Seg_2171" s="T1110">dĭgəttə</ta>
            <ta e="T1112" id="Seg_2172" s="T1111">šonə-ga</ta>
            <ta e="T1113" id="Seg_2173" s="T1112">urgaːba</ta>
            <ta e="T1114" id="Seg_2174" s="T1113">ĭmbi</ta>
            <ta e="T1115" id="Seg_2175" s="T1114">dʼor-laʔbə-l</ta>
            <ta e="T1116" id="Seg_2176" s="T1115">da</ta>
            <ta e="T1117" id="Seg_2177" s="T1116">kak</ta>
            <ta e="T1118" id="Seg_2178" s="T1117">măna</ta>
            <ta e="T1119" id="Seg_2179" s="T1118">ej</ta>
            <ta e="T1120" id="Seg_2180" s="T1119">dʼor-zittə</ta>
            <ta e="T1121" id="Seg_2181" s="T1120">măn</ta>
            <ta e="T1122" id="Seg_2182" s="T1121">tura-m</ta>
            <ta e="T1123" id="Seg_2183" s="T1122">i-bi</ta>
            <ta e="T1124" id="Seg_2184" s="T1123">pa-gəʔ</ta>
            <ta e="T1125" id="Seg_2185" s="T1124">a</ta>
            <ta e="T1126" id="Seg_2186" s="T1125">lisitsa-n</ta>
            <ta e="T1127" id="Seg_2187" s="T1126">tura-t</ta>
            <ta e="T1129" id="Seg_2188" s="T1128">sĭre-gəʔ</ta>
            <ta e="T1130" id="Seg_2189" s="T1129">tüj</ta>
            <ta e="T1131" id="Seg_2190" s="T1130">dĭ</ta>
            <ta e="T1132" id="Seg_2191" s="T1131">šo-bi</ta>
            <ta e="T1133" id="Seg_2192" s="T1132">măna</ta>
            <ta e="T1134" id="Seg_2193" s="T1133">šaː-sʼtə</ta>
            <ta e="T1135" id="Seg_2194" s="T1134">i</ta>
            <ta e="T1136" id="Seg_2195" s="T1135">măna</ta>
            <ta e="T1137" id="Seg_2196" s="T1136">sürer-lüʔ-pi</ta>
            <ta e="T1138" id="Seg_2197" s="T1137">kan-žə-bəj</ta>
            <ta e="T1139" id="Seg_2198" s="T1138">măn</ta>
            <ta e="T1140" id="Seg_2199" s="T1139">dĭ-m</ta>
            <ta e="T1141" id="Seg_2200" s="T1140">sürer-li-m</ta>
            <ta e="T1142" id="Seg_2201" s="T1141">šo-bi</ta>
            <ta e="T1143" id="Seg_2202" s="T1142">kan-a-ʔ</ta>
            <ta e="T1144" id="Seg_2203" s="T1143">döʔə</ta>
            <ta e="T1145" id="Seg_2204" s="T1144">ato</ta>
            <ta e="T1146" id="Seg_2205" s="T1145">tüjö</ta>
            <ta e="T1147" id="Seg_2206" s="T1146">tănan</ta>
            <ta e="T1148" id="Seg_2207" s="T1147">münör-la-m</ta>
            <ta e="T1149" id="Seg_2208" s="T1148">a</ta>
            <ta e="T1150" id="Seg_2209" s="T1149">dĭ</ta>
            <ta e="T1151" id="Seg_2210" s="T1150">măn-də</ta>
            <ta e="T1152" id="Seg_2211" s="T1151">măn</ta>
            <ta e="T1153" id="Seg_2212" s="T1152">kak</ta>
            <ta e="T1154" id="Seg_2213" s="T1153">nuʔmə-luʔ-lə-m</ta>
            <ta e="T1155" id="Seg_2214" s="T1154">da</ta>
            <ta e="T1156" id="Seg_2215" s="T1155">bar</ta>
            <ta e="T1157" id="Seg_2216" s="T1156">tăr-də</ta>
            <ta e="T1158" id="Seg_2217" s="T1157">tăn</ta>
            <ta e="T1159" id="Seg_2218" s="T1158">nĭŋgə-luʔ-lə-l</ta>
            <ta e="T1160" id="Seg_2219" s="T1159">i</ta>
            <ta e="T1161" id="Seg_2220" s="T1160">tăn</ta>
            <ta e="T1162" id="Seg_2221" s="T1161">nuʔmə-luʔ-lə-l</ta>
            <ta e="T1163" id="Seg_2222" s="T1162">dĭgəttə</ta>
            <ta e="T1164" id="Seg_2223" s="T1163">urgaːba</ta>
            <ta e="T1166" id="Seg_2224" s="T1165">pim-nuʔ-pi</ta>
            <ta e="T1167" id="Seg_2225" s="T1166">i</ta>
            <ta e="T1168" id="Seg_2226" s="T1167">nuʔmə-luʔ-pi</ta>
            <ta e="T1169" id="Seg_2227" s="T1168">bazoʔ</ta>
            <ta e="T1170" id="Seg_2228" s="T1169">kandə-ga</ta>
            <ta e="T1171" id="Seg_2229" s="T1170">zajčik</ta>
            <ta e="T1172" id="Seg_2230" s="T1171">i</ta>
            <ta e="T1173" id="Seg_2231" s="T1172">dʼor-laʔbə</ta>
            <ta e="T1174" id="Seg_2232" s="T1173">dĭgəttə</ta>
            <ta e="T1175" id="Seg_2233" s="T1174">buga</ta>
            <ta e="T1176" id="Seg_2234" s="T1175">šonə-ga</ta>
            <ta e="T1177" id="Seg_2235" s="T1176">ĭmbi</ta>
            <ta e="T1178" id="Seg_2236" s="T1177">dʼor-laʔbə-l</ta>
            <ta e="T1179" id="Seg_2237" s="T1178">da</ta>
            <ta e="T1180" id="Seg_2238" s="T1179">măn</ta>
            <ta e="T1181" id="Seg_2239" s="T1180">tura-m</ta>
            <ta e="T1182" id="Seg_2240" s="T1181">lisitsa</ta>
            <ta e="T1183" id="Seg_2241" s="T1182">i-bi</ta>
            <ta e="T1184" id="Seg_2242" s="T1183">a</ta>
            <ta e="T1185" id="Seg_2243" s="T1184">dĭ-n</ta>
            <ta e="T1187" id="Seg_2244" s="T1186">i-bi</ta>
            <ta e="T1188" id="Seg_2245" s="T1187">sĭre-gəʔ</ta>
            <ta e="T1189" id="Seg_2246" s="T1188">i</ta>
            <ta e="T1190" id="Seg_2247" s="T1189">kü-laːm-bi</ta>
            <ta e="T1191" id="Seg_2248" s="T1190">a</ta>
            <ta e="T1192" id="Seg_2249" s="T1191">măn</ta>
            <ta e="T1193" id="Seg_2250" s="T1192">üge</ta>
            <ta e="T1194" id="Seg_2251" s="T1193">tura-m</ta>
            <ta e="T1195" id="Seg_2252" s="T1194">jakšə</ta>
            <ta e="T1196" id="Seg_2253" s="T1195">pa-gəʔ</ta>
            <ta e="T1197" id="Seg_2254" s="T1196">dĭgəttə</ta>
            <ta e="T1198" id="Seg_2255" s="T1197">kan-žə-bəj</ta>
            <ta e="T1199" id="Seg_2256" s="T1198">măn</ta>
            <ta e="T1200" id="Seg_2257" s="T1199">dĭ-m</ta>
            <ta e="T1201" id="Seg_2258" s="T1200">sürer-i-m</ta>
            <ta e="T1202" id="Seg_2259" s="T1201">dʼok</ta>
            <ta e="T1203" id="Seg_2260" s="T1202">ej</ta>
            <ta e="T1204" id="Seg_2261" s="T1203">sürer-lə-l</ta>
            <ta e="T1205" id="Seg_2262" s="T1204">urgaːba</ta>
            <ta e="T1206" id="Seg_2263" s="T1205">ej</ta>
            <ta e="T1207" id="Seg_2264" s="T1206">mo-bi</ta>
            <ta e="T1208" id="Seg_2265" s="T1207">sürer-zittə</ta>
            <ta e="T1209" id="Seg_2266" s="T1208">men</ta>
            <ta e="T1210" id="Seg_2267" s="T1209">ej</ta>
            <ta e="T1211" id="Seg_2268" s="T1210">mo-bi</ta>
            <ta e="T1212" id="Seg_2269" s="T1211">buga</ta>
            <ta e="T1213" id="Seg_2270" s="T1212">a</ta>
            <ta e="T1214" id="Seg_2271" s="T1213">tăn</ta>
            <ta e="T1216" id="Seg_2272" s="T1215">dĭgəttə</ta>
            <ta e="T1218" id="Seg_2273" s="T1217">šo-bi-ʔi</ta>
            <ta e="T1219" id="Seg_2274" s="T1218">šo-bi-ʔi</ta>
            <ta e="T1221" id="Seg_2275" s="T1220">buga</ta>
            <ta e="T1222" id="Seg_2276" s="T1221">măn-də</ta>
            <ta e="T1225" id="Seg_2277" s="T1224">kan-a-ʔ</ta>
            <ta e="T1226" id="Seg_2278" s="T1225">tura-gəʔ</ta>
            <ta e="T1227" id="Seg_2279" s="T1226">a</ta>
            <ta e="T1228" id="Seg_2280" s="T1227">dĭ</ta>
            <ta e="T1229" id="Seg_2281" s="T1228">măn-də</ta>
            <ta e="T1230" id="Seg_2282" s="T1229">tüjö</ta>
            <ta e="T1231" id="Seg_2283" s="T1230">suʔmi-luʔ-lə-m</ta>
            <ta e="T1232" id="Seg_2284" s="T1231">dak</ta>
            <ta e="T1233" id="Seg_2285" s="T1232">bar</ta>
            <ta e="T1234" id="Seg_2286" s="T1233">tănan</ta>
            <ta e="T1235" id="Seg_2287" s="T1234">saj</ta>
            <ta e="T1236" id="Seg_2288" s="T1235">no-žuʔ-lə-m</ta>
            <ta e="T1237" id="Seg_2289" s="T1236">dĭgəttə</ta>
            <ta e="T1238" id="Seg_2290" s="T1237">buga</ta>
            <ta e="T1239" id="Seg_2291" s="T1238">buga</ta>
            <ta e="T1240" id="Seg_2292" s="T1239">nere-lüʔ-pi</ta>
            <ta e="T1241" id="Seg_2293" s="T1240">i</ta>
            <ta e="T1242" id="Seg_2294" s="T1241">nuʔmə-luʔ-pi</ta>
            <ta e="T1243" id="Seg_2295" s="T1242">dĭgəttə</ta>
            <ta e="T1244" id="Seg_2296" s="T1243">bazoʔ</ta>
            <ta e="T1245" id="Seg_2297" s="T1244">kandə-ga</ta>
            <ta e="T1246" id="Seg_2298" s="T1245">zajats</ta>
            <ta e="T1247" id="Seg_2299" s="T1246">i</ta>
            <ta e="T1248" id="Seg_2300" s="T1247">dʼor-laʔbə</ta>
            <ta e="T1249" id="Seg_2301" s="T1248">dĭgəttə</ta>
            <ta e="T1250" id="Seg_2302" s="T1249">kurizə-n</ta>
            <ta e="T1251" id="Seg_2303" s="T1250">tibi</ta>
            <ta e="T1252" id="Seg_2304" s="T1251">šonə-ga</ta>
            <ta e="T1253" id="Seg_2305" s="T1252">šapko-ziʔ</ta>
            <ta e="T1254" id="Seg_2306" s="T1253">măn-də</ta>
            <ta e="T1255" id="Seg_2307" s="T1254">ĭmbi</ta>
            <ta e="T1256" id="Seg_2308" s="T1255">dʼor-laʔbə-l</ta>
            <ta e="T1257" id="Seg_2309" s="T1256">da</ta>
            <ta e="T1258" id="Seg_2310" s="T1257">măn</ta>
            <ta e="T1259" id="Seg_2311" s="T1258">tura-m</ta>
            <ta e="T1260" id="Seg_2312" s="T1259">pa-gəʔ</ta>
            <ta e="T1261" id="Seg_2313" s="T1260">lisitsa</ta>
            <ta e="T1262" id="Seg_2314" s="T1261">šo-bi</ta>
            <ta e="T1263" id="Seg_2315" s="T1262">kunol-zittə</ta>
            <ta e="T1264" id="Seg_2316" s="T1263">i</ta>
            <ta e="T1265" id="Seg_2317" s="T1264">măna</ta>
            <ta e="T1266" id="Seg_2318" s="T1265">sürer-bi</ta>
            <ta e="T1267" id="Seg_2319" s="T1266">a</ta>
            <ta e="T1268" id="Seg_2320" s="T1267">dĭ-n</ta>
            <ta e="T1269" id="Seg_2321" s="T1268">sĭre-gəʔ</ta>
            <ta e="T1270" id="Seg_2322" s="T1269">i-bi</ta>
            <ta e="T1271" id="Seg_2323" s="T1270">i</ta>
            <ta e="T1272" id="Seg_2324" s="T1271">tüj</ta>
            <ta e="T1273" id="Seg_2325" s="T1272">naga</ta>
            <ta e="T1274" id="Seg_2326" s="T1273">dĭgəttə</ta>
            <ta e="T1275" id="Seg_2327" s="T1274">kan-žə-bəj</ta>
            <ta e="T1276" id="Seg_2328" s="T1275">măn</ta>
            <ta e="T1277" id="Seg_2329" s="T1276">sürer-lə-m</ta>
            <ta e="T1278" id="Seg_2330" s="T1277">men</ta>
            <ta e="T1279" id="Seg_2331" s="T1278">ej</ta>
            <ta e="T1280" id="Seg_2332" s="T1279">mo-bi</ta>
            <ta e="T1281" id="Seg_2333" s="T1280">sürer-zittə</ta>
            <ta e="T1282" id="Seg_2334" s="T1281">dĭ</ta>
            <ta e="T1283" id="Seg_2335" s="T1282">urgaːba</ta>
            <ta e="T1284" id="Seg_2336" s="T1283">i</ta>
            <ta e="T1285" id="Seg_2337" s="T1284">buga</ta>
            <ta e="T1286" id="Seg_2338" s="T1285">ej</ta>
            <ta e="T1287" id="Seg_2339" s="T1286">mo-bi</ta>
            <ta e="T1288" id="Seg_2340" s="T1287">a</ta>
            <ta e="T1289" id="Seg_2341" s="T1288">tăn</ta>
            <ta e="T1290" id="Seg_2342" s="T1289">ej</ta>
            <ta e="T1291" id="Seg_2343" s="T1290">sürer-lə-l</ta>
            <ta e="T1292" id="Seg_2344" s="T1291">kan-žə-bəj</ta>
            <ta e="T1293" id="Seg_2345" s="T1292">dĭgəttə</ta>
            <ta e="T1294" id="Seg_2346" s="T1293">šo-bi</ta>
            <ta e="T1295" id="Seg_2347" s="T1294">bar</ta>
            <ta e="T1296" id="Seg_2348" s="T1295">kirgar-laʔbə</ta>
            <ta e="T1297" id="Seg_2349" s="T1296">uʔbdə-ʔ</ta>
            <ta e="T1298" id="Seg_2350" s="T1297">pʼeš-gəʔ</ta>
            <ta e="T1301" id="Seg_2351" s="T1300">kan-a-ʔ</ta>
            <ta e="T1302" id="Seg_2352" s="T1301">döʔə</ta>
            <ta e="T1304" id="Seg_2353" s="T1302">ato</ta>
            <ta e="T1305" id="Seg_2354" s="T1304">tüjö</ta>
            <ta e="T1306" id="Seg_2355" s="T1305">šapku-ziʔ</ta>
            <ta e="T1307" id="Seg_2356" s="T1306">tănan</ta>
            <ta e="T1308" id="Seg_2357" s="T1307">kut-la-m</ta>
            <ta e="T1309" id="Seg_2358" s="T1308">bar</ta>
            <ta e="T1310" id="Seg_2359" s="T1309">dʼăgar-lə-m</ta>
            <ta e="T1311" id="Seg_2360" s="T1310">tănan</ta>
            <ta e="T1312" id="Seg_2361" s="T1311">tüjö</ta>
            <ta e="T1313" id="Seg_2362" s="T1312">uju-m</ta>
            <ta e="T1314" id="Seg_2363" s="T1313">uju-ʔi</ta>
            <ta e="T1315" id="Seg_2364" s="T1314">šer-ie-m</ta>
            <ta e="T1316" id="Seg_2365" s="T1315">jama-ʔi</ta>
            <ta e="T1317" id="Seg_2366" s="T1316">dĭgəttə</ta>
            <ta e="T1318" id="Seg_2367" s="T1317">bazoʔ</ta>
            <ta e="T1319" id="Seg_2368" s="T1318">kirgar-laʔbə</ta>
            <ta e="T1320" id="Seg_2369" s="T1319">kan-a-ʔ</ta>
            <ta e="T1321" id="Seg_2370" s="T1320">pʼeš-gəʔ</ta>
            <ta e="T1322" id="Seg_2371" s="T1321">ato</ta>
            <ta e="T1323" id="Seg_2372" s="T1322">tüjö</ta>
            <ta e="T1324" id="Seg_2373" s="T1323">tănan</ta>
            <ta e="T1325" id="Seg_2374" s="T1324">kut-la-m</ta>
            <ta e="T1326" id="Seg_2375" s="T1325">tagaj</ta>
            <ta e="T1327" id="Seg_2376" s="T1326">šapku-ziʔ</ta>
            <ta e="T1328" id="Seg_2377" s="T1327">bar</ta>
            <ta e="T1329" id="Seg_2378" s="T1328">băt-li-m</ta>
            <ta e="T1330" id="Seg_2379" s="T1329">tüjö</ta>
            <ta e="T1331" id="Seg_2380" s="T1330">bar</ta>
            <ta e="T1332" id="Seg_2381" s="T1331">oldʼa-m</ta>
            <ta e="T1333" id="Seg_2382" s="T1332">šer-io-m</ta>
            <ta e="T1334" id="Seg_2383" s="T1333">dĭgəttə</ta>
            <ta e="T1335" id="Seg_2384" s="T1334">dĭ</ta>
            <ta e="T1336" id="Seg_2385" s="T1335">bazoʔ</ta>
            <ta e="T1337" id="Seg_2386" s="T1336">kirgar-laʔbə</ta>
            <ta e="T1338" id="Seg_2387" s="T1337">kan-a-ʔ</ta>
            <ta e="T1339" id="Seg_2388" s="T1338">tüjö</ta>
            <ta e="T1340" id="Seg_2389" s="T1339">nuʔmə-leʔbə-m</ta>
            <ta e="T1341" id="Seg_2390" s="T1340">dĭgəttə</ta>
            <ta e="T1342" id="Seg_2391" s="T1341">dĭ</ta>
            <ta e="T1343" id="Seg_2392" s="T1342">šapku-ziʔ</ta>
            <ta e="T1344" id="Seg_2393" s="T1343">dĭ-m</ta>
            <ta e="T1345" id="Seg_2394" s="T1344">bar</ta>
            <ta e="T1346" id="Seg_2395" s="T1345">dʼăgar-luʔ-pi</ta>
            <ta e="T1347" id="Seg_2396" s="T1346">i</ta>
            <ta e="T1348" id="Seg_2397" s="T1347">dĭ</ta>
            <ta e="T1349" id="Seg_2398" s="T1348">kü-laːm-bi</ta>
            <ta e="T1350" id="Seg_2399" s="T1349">dĭgəttə</ta>
            <ta e="T1351" id="Seg_2400" s="T1350">amno-bi-ʔi</ta>
            <ta e="T1352" id="Seg_2401" s="T1351">kurizə-n</ta>
            <ta e="T1353" id="Seg_2402" s="T1352">tibi</ta>
            <ta e="T1354" id="Seg_2403" s="T1353">i</ta>
            <ta e="T1355" id="Seg_2404" s="T1354">zajats</ta>
            <ta e="T1356" id="Seg_2405" s="T1355">tura-gən</ta>
            <ta e="T1357" id="Seg_2406" s="T1356">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1032" id="Seg_2407" s="T1031">amno-bi-jəʔ</ta>
            <ta e="T1033" id="Seg_2408" s="T1032">lʼisa</ta>
            <ta e="T1034" id="Seg_2409" s="T1033">i</ta>
            <ta e="T1035" id="Seg_2410" s="T1034">zajats</ta>
            <ta e="T1036" id="Seg_2411" s="T1035">lʼisa-n</ta>
            <ta e="T1037" id="Seg_2412" s="T1036">i-bi</ta>
            <ta e="T1038" id="Seg_2413" s="T1037">tura-t</ta>
            <ta e="T1039" id="Seg_2414" s="T1038">sĭri-gəʔ</ta>
            <ta e="T1040" id="Seg_2415" s="T1039">a</ta>
            <ta e="T1041" id="Seg_2416" s="T1040">zajats-n</ta>
            <ta e="T1042" id="Seg_2417" s="T1041">i-bi</ta>
            <ta e="T1044" id="Seg_2418" s="T1043">pa-gəʔ</ta>
            <ta e="T1045" id="Seg_2419" s="T1044">dĭgəttə</ta>
            <ta e="T1046" id="Seg_2420" s="T1045">šo-bi</ta>
            <ta e="T1047" id="Seg_2421" s="T1046">ejü</ta>
            <ta e="T1048" id="Seg_2422" s="T1047">lʼisa-n</ta>
            <ta e="T1049" id="Seg_2423" s="T1048">tura-t</ta>
            <ta e="T1050" id="Seg_2424" s="T1049">bar</ta>
            <ta e="T1051" id="Seg_2425" s="T1050">mʼaŋ-luʔbdə-bi</ta>
            <ta e="T1052" id="Seg_2426" s="T1051">dĭgəttə</ta>
            <ta e="T1053" id="Seg_2427" s="T1052">dĭ</ta>
            <ta e="T1054" id="Seg_2428" s="T1053">šo-bi</ta>
            <ta e="T1055" id="Seg_2429" s="T1054">zajats-Tə</ta>
            <ta e="T1056" id="Seg_2430" s="T1055">öʔ-ʔ</ta>
            <ta e="T1057" id="Seg_2431" s="T1056">măna</ta>
            <ta e="T1058" id="Seg_2432" s="T1057">kunol-zittə</ta>
            <ta e="T1059" id="Seg_2433" s="T1058">dĭ</ta>
            <ta e="T1060" id="Seg_2434" s="T1059">bos-də</ta>
            <ta e="T1061" id="Seg_2435" s="T1060">dĭ-m</ta>
            <ta e="T1062" id="Seg_2436" s="T1061">sürer-luʔbdə-bi</ta>
            <ta e="T1063" id="Seg_2437" s="T1062">tura-gəʔ</ta>
            <ta e="T1064" id="Seg_2438" s="T1063">dĭ</ta>
            <ta e="T1065" id="Seg_2439" s="T1064">šonə-gA</ta>
            <ta e="T1066" id="Seg_2440" s="T1065">i</ta>
            <ta e="T1067" id="Seg_2441" s="T1066">tʼor-laʔbə</ta>
            <ta e="T1068" id="Seg_2442" s="T1067">măn</ta>
            <ta e="T1069" id="Seg_2443" s="T1068">men</ta>
            <ta e="T1070" id="Seg_2444" s="T1069">măn-ntə</ta>
            <ta e="T1071" id="Seg_2445" s="T1070">ĭmbi</ta>
            <ta e="T1072" id="Seg_2446" s="T1071">tăn</ta>
            <ta e="T1073" id="Seg_2447" s="T1072">tʼor-laʔbə-l</ta>
            <ta e="T1074" id="Seg_2448" s="T1073">da</ta>
            <ta e="T1075" id="Seg_2449" s="T1074">măn</ta>
            <ta e="T1076" id="Seg_2450" s="T1075">tura-gəʔ</ta>
            <ta e="T1077" id="Seg_2451" s="T1076">măn-ntə</ta>
            <ta e="T1078" id="Seg_2452" s="T1077">sürer-bi</ta>
            <ta e="T1079" id="Seg_2453" s="T1078">lisička</ta>
            <ta e="T1080" id="Seg_2454" s="T1079">kan-žə-bəj</ta>
            <ta e="T1081" id="Seg_2455" s="T1080">măn</ta>
            <ta e="T1082" id="Seg_2456" s="T1081">dĭ-m</ta>
            <ta e="T1083" id="Seg_2457" s="T1082">sürer-lV-m</ta>
            <ta e="T1084" id="Seg_2458" s="T1083">šo-bi</ta>
            <ta e="T1085" id="Seg_2459" s="T1084">men</ta>
            <ta e="T1086" id="Seg_2460" s="T1085">da</ta>
            <ta e="T1087" id="Seg_2461" s="T1086">üge</ta>
            <ta e="T1088" id="Seg_2462" s="T1087">kirgaːr-laʔbə</ta>
            <ta e="T1089" id="Seg_2463" s="T1088">kan-ə-ʔ</ta>
            <ta e="T1090" id="Seg_2464" s="T1089">döʔə</ta>
            <ta e="T1091" id="Seg_2465" s="T1090">a</ta>
            <ta e="T1092" id="Seg_2466" s="T1091">dĭ</ta>
            <ta e="T1093" id="Seg_2467" s="T1092">măn-ntə</ta>
            <ta e="T1094" id="Seg_2468" s="T1093">tüjö</ta>
            <ta e="T1095" id="Seg_2469" s="T1094">tăn</ta>
            <ta e="T1096" id="Seg_2470" s="T1095">bar</ta>
            <ta e="T1097" id="Seg_2471" s="T1096">tar-l</ta>
            <ta e="T1098" id="Seg_2472" s="T1097">nĭŋgə-lV-m</ta>
            <ta e="T1099" id="Seg_2473" s="T1098">i</ta>
            <ta e="T1100" id="Seg_2474" s="T1099">tăn</ta>
            <ta e="T1101" id="Seg_2475" s="T1100">nuʔmə-luʔbdə-lV-l</ta>
            <ta e="T1102" id="Seg_2476" s="T1101">dĭ</ta>
            <ta e="T1103" id="Seg_2477" s="T1102">nereʔ-luʔbdə-bi</ta>
            <ta e="T1104" id="Seg_2478" s="T1103">i</ta>
            <ta e="T1105" id="Seg_2479" s="T1104">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1106" id="Seg_2480" s="T1105">bazoʔ</ta>
            <ta e="T1107" id="Seg_2481" s="T1106">kandə-gA</ta>
            <ta e="T1108" id="Seg_2482" s="T1107">zajats</ta>
            <ta e="T1109" id="Seg_2483" s="T1108">i</ta>
            <ta e="T1110" id="Seg_2484" s="T1109">tʼor-laʔbə</ta>
            <ta e="T1111" id="Seg_2485" s="T1110">dĭgəttə</ta>
            <ta e="T1112" id="Seg_2486" s="T1111">šonə-gA</ta>
            <ta e="T1113" id="Seg_2487" s="T1112">urgaːba</ta>
            <ta e="T1114" id="Seg_2488" s="T1113">ĭmbi</ta>
            <ta e="T1115" id="Seg_2489" s="T1114">tʼor-laʔbə-l</ta>
            <ta e="T1116" id="Seg_2490" s="T1115">da</ta>
            <ta e="T1117" id="Seg_2491" s="T1116">kak</ta>
            <ta e="T1118" id="Seg_2492" s="T1117">măna</ta>
            <ta e="T1119" id="Seg_2493" s="T1118">ej</ta>
            <ta e="T1120" id="Seg_2494" s="T1119">tʼor-zittə</ta>
            <ta e="T1121" id="Seg_2495" s="T1120">măn</ta>
            <ta e="T1122" id="Seg_2496" s="T1121">tura-m</ta>
            <ta e="T1123" id="Seg_2497" s="T1122">i-bi</ta>
            <ta e="T1124" id="Seg_2498" s="T1123">pa-gəʔ</ta>
            <ta e="T1125" id="Seg_2499" s="T1124">a</ta>
            <ta e="T1126" id="Seg_2500" s="T1125">lʼisʼitsa-n</ta>
            <ta e="T1127" id="Seg_2501" s="T1126">tura-t</ta>
            <ta e="T1129" id="Seg_2502" s="T1128">sĭri-gəʔ</ta>
            <ta e="T1130" id="Seg_2503" s="T1129">tüj</ta>
            <ta e="T1131" id="Seg_2504" s="T1130">dĭ</ta>
            <ta e="T1132" id="Seg_2505" s="T1131">šo-bi</ta>
            <ta e="T1133" id="Seg_2506" s="T1132">măna</ta>
            <ta e="T1134" id="Seg_2507" s="T1133">šaː-zittə</ta>
            <ta e="T1135" id="Seg_2508" s="T1134">i</ta>
            <ta e="T1136" id="Seg_2509" s="T1135">măna</ta>
            <ta e="T1137" id="Seg_2510" s="T1136">sürer-luʔbdə-bi</ta>
            <ta e="T1138" id="Seg_2511" s="T1137">kan-žə-bəj</ta>
            <ta e="T1139" id="Seg_2512" s="T1138">măn</ta>
            <ta e="T1140" id="Seg_2513" s="T1139">dĭ-m</ta>
            <ta e="T1141" id="Seg_2514" s="T1140">sürer-lV-m</ta>
            <ta e="T1142" id="Seg_2515" s="T1141">šo-bi</ta>
            <ta e="T1143" id="Seg_2516" s="T1142">kan-ə-ʔ</ta>
            <ta e="T1144" id="Seg_2517" s="T1143">döʔə</ta>
            <ta e="T1145" id="Seg_2518" s="T1144">ato</ta>
            <ta e="T1146" id="Seg_2519" s="T1145">tüjö</ta>
            <ta e="T1147" id="Seg_2520" s="T1146">tănan</ta>
            <ta e="T1148" id="Seg_2521" s="T1147">münör-lV-m</ta>
            <ta e="T1149" id="Seg_2522" s="T1148">a</ta>
            <ta e="T1150" id="Seg_2523" s="T1149">dĭ</ta>
            <ta e="T1151" id="Seg_2524" s="T1150">măn-ntə</ta>
            <ta e="T1152" id="Seg_2525" s="T1151">măn</ta>
            <ta e="T1153" id="Seg_2526" s="T1152">kak</ta>
            <ta e="T1154" id="Seg_2527" s="T1153">nuʔmə-luʔbdə-lV-m</ta>
            <ta e="T1155" id="Seg_2528" s="T1154">da</ta>
            <ta e="T1156" id="Seg_2529" s="T1155">bar</ta>
            <ta e="T1157" id="Seg_2530" s="T1156">tar-də</ta>
            <ta e="T1158" id="Seg_2531" s="T1157">tăn</ta>
            <ta e="T1159" id="Seg_2532" s="T1158">nĭŋgə-luʔbdə-lV-l</ta>
            <ta e="T1160" id="Seg_2533" s="T1159">i</ta>
            <ta e="T1161" id="Seg_2534" s="T1160">tăn</ta>
            <ta e="T1162" id="Seg_2535" s="T1161">nuʔmə-luʔbdə-lV-l</ta>
            <ta e="T1163" id="Seg_2536" s="T1162">dĭgəttə</ta>
            <ta e="T1164" id="Seg_2537" s="T1163">urgaːba</ta>
            <ta e="T1166" id="Seg_2538" s="T1165">pim-luʔbdə-bi</ta>
            <ta e="T1167" id="Seg_2539" s="T1166">i</ta>
            <ta e="T1168" id="Seg_2540" s="T1167">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1169" id="Seg_2541" s="T1168">bazoʔ</ta>
            <ta e="T1170" id="Seg_2542" s="T1169">kandə-gA</ta>
            <ta e="T1171" id="Seg_2543" s="T1170">zajčik</ta>
            <ta e="T1172" id="Seg_2544" s="T1171">i</ta>
            <ta e="T1173" id="Seg_2545" s="T1172">tʼor-laʔbə</ta>
            <ta e="T1174" id="Seg_2546" s="T1173">dĭgəttə</ta>
            <ta e="T1175" id="Seg_2547" s="T1174">buga</ta>
            <ta e="T1176" id="Seg_2548" s="T1175">šonə-gA</ta>
            <ta e="T1177" id="Seg_2549" s="T1176">ĭmbi</ta>
            <ta e="T1178" id="Seg_2550" s="T1177">tʼor-laʔbə-l</ta>
            <ta e="T1179" id="Seg_2551" s="T1178">da</ta>
            <ta e="T1180" id="Seg_2552" s="T1179">măn</ta>
            <ta e="T1181" id="Seg_2553" s="T1180">tura-m</ta>
            <ta e="T1182" id="Seg_2554" s="T1181">lʼisʼitsa</ta>
            <ta e="T1183" id="Seg_2555" s="T1182">i-bi</ta>
            <ta e="T1184" id="Seg_2556" s="T1183">a</ta>
            <ta e="T1185" id="Seg_2557" s="T1184">dĭ-n</ta>
            <ta e="T1187" id="Seg_2558" s="T1186">i-bi</ta>
            <ta e="T1188" id="Seg_2559" s="T1187">sĭri-gəʔ</ta>
            <ta e="T1189" id="Seg_2560" s="T1188">i</ta>
            <ta e="T1190" id="Seg_2561" s="T1189">kü-laːm-bi</ta>
            <ta e="T1191" id="Seg_2562" s="T1190">a</ta>
            <ta e="T1192" id="Seg_2563" s="T1191">măn</ta>
            <ta e="T1193" id="Seg_2564" s="T1192">üge</ta>
            <ta e="T1194" id="Seg_2565" s="T1193">tura-m</ta>
            <ta e="T1195" id="Seg_2566" s="T1194">jakšə</ta>
            <ta e="T1196" id="Seg_2567" s="T1195">pa-gəʔ</ta>
            <ta e="T1197" id="Seg_2568" s="T1196">dĭgəttə</ta>
            <ta e="T1198" id="Seg_2569" s="T1197">kan-žə-bəj</ta>
            <ta e="T1199" id="Seg_2570" s="T1198">măn</ta>
            <ta e="T1200" id="Seg_2571" s="T1199">dĭ-m</ta>
            <ta e="T1201" id="Seg_2572" s="T1200">sürer-lV-m</ta>
            <ta e="T1202" id="Seg_2573" s="T1201">dʼok</ta>
            <ta e="T1203" id="Seg_2574" s="T1202">ej</ta>
            <ta e="T1204" id="Seg_2575" s="T1203">sürer-lV-l</ta>
            <ta e="T1205" id="Seg_2576" s="T1204">urgaːba</ta>
            <ta e="T1206" id="Seg_2577" s="T1205">ej</ta>
            <ta e="T1207" id="Seg_2578" s="T1206">mo-bi</ta>
            <ta e="T1208" id="Seg_2579" s="T1207">sürer-zittə</ta>
            <ta e="T1209" id="Seg_2580" s="T1208">men</ta>
            <ta e="T1210" id="Seg_2581" s="T1209">ej</ta>
            <ta e="T1211" id="Seg_2582" s="T1210">mo-bi</ta>
            <ta e="T1212" id="Seg_2583" s="T1211">buga</ta>
            <ta e="T1213" id="Seg_2584" s="T1212">a</ta>
            <ta e="T1214" id="Seg_2585" s="T1213">tăn</ta>
            <ta e="T1216" id="Seg_2586" s="T1215">dĭgəttə</ta>
            <ta e="T1218" id="Seg_2587" s="T1217">šo-bi-jəʔ</ta>
            <ta e="T1219" id="Seg_2588" s="T1218">šo-bi-jəʔ</ta>
            <ta e="T1221" id="Seg_2589" s="T1220">buga</ta>
            <ta e="T1222" id="Seg_2590" s="T1221">măn-ntə</ta>
            <ta e="T1225" id="Seg_2591" s="T1224">kan-ə-ʔ</ta>
            <ta e="T1226" id="Seg_2592" s="T1225">tura-gəʔ</ta>
            <ta e="T1227" id="Seg_2593" s="T1226">a</ta>
            <ta e="T1228" id="Seg_2594" s="T1227">dĭ</ta>
            <ta e="T1229" id="Seg_2595" s="T1228">măn-ntə</ta>
            <ta e="T1230" id="Seg_2596" s="T1229">tüjö</ta>
            <ta e="T1231" id="Seg_2597" s="T1230">süʔmə-luʔbdə-lV-m</ta>
            <ta e="T1232" id="Seg_2598" s="T1231">tak</ta>
            <ta e="T1233" id="Seg_2599" s="T1232">bar</ta>
            <ta e="T1234" id="Seg_2600" s="T1233">tănan</ta>
            <ta e="T1235" id="Seg_2601" s="T1234">saj</ta>
            <ta e="T1236" id="Seg_2602" s="T1235">no-luʔbdə-lV-m</ta>
            <ta e="T1237" id="Seg_2603" s="T1236">dĭgəttə</ta>
            <ta e="T1238" id="Seg_2604" s="T1237">buga</ta>
            <ta e="T1239" id="Seg_2605" s="T1238">buga</ta>
            <ta e="T1240" id="Seg_2606" s="T1239">nereʔ-luʔbdə-bi</ta>
            <ta e="T1241" id="Seg_2607" s="T1240">i</ta>
            <ta e="T1242" id="Seg_2608" s="T1241">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1243" id="Seg_2609" s="T1242">dĭgəttə</ta>
            <ta e="T1244" id="Seg_2610" s="T1243">bazoʔ</ta>
            <ta e="T1245" id="Seg_2611" s="T1244">kandə-gA</ta>
            <ta e="T1246" id="Seg_2612" s="T1245">zajats</ta>
            <ta e="T1247" id="Seg_2613" s="T1246">i</ta>
            <ta e="T1248" id="Seg_2614" s="T1247">tʼor-laʔbə</ta>
            <ta e="T1249" id="Seg_2615" s="T1248">dĭgəttə</ta>
            <ta e="T1250" id="Seg_2616" s="T1249">kuriza-n</ta>
            <ta e="T1251" id="Seg_2617" s="T1250">tibi</ta>
            <ta e="T1252" id="Seg_2618" s="T1251">šonə-gA</ta>
            <ta e="T1253" id="Seg_2619" s="T1252">šapku-ziʔ</ta>
            <ta e="T1254" id="Seg_2620" s="T1253">măn-ntə</ta>
            <ta e="T1255" id="Seg_2621" s="T1254">ĭmbi</ta>
            <ta e="T1256" id="Seg_2622" s="T1255">tʼor-laʔbə-l</ta>
            <ta e="T1257" id="Seg_2623" s="T1256">da</ta>
            <ta e="T1258" id="Seg_2624" s="T1257">măn</ta>
            <ta e="T1259" id="Seg_2625" s="T1258">tura-m</ta>
            <ta e="T1260" id="Seg_2626" s="T1259">pa-gəʔ</ta>
            <ta e="T1261" id="Seg_2627" s="T1260">lʼisʼitsa</ta>
            <ta e="T1262" id="Seg_2628" s="T1261">šo-bi</ta>
            <ta e="T1263" id="Seg_2629" s="T1262">kunol-zittə</ta>
            <ta e="T1264" id="Seg_2630" s="T1263">i</ta>
            <ta e="T1265" id="Seg_2631" s="T1264">măna</ta>
            <ta e="T1266" id="Seg_2632" s="T1265">sürer-bi</ta>
            <ta e="T1267" id="Seg_2633" s="T1266">a</ta>
            <ta e="T1268" id="Seg_2634" s="T1267">dĭ-n</ta>
            <ta e="T1269" id="Seg_2635" s="T1268">sĭri-gəʔ</ta>
            <ta e="T1270" id="Seg_2636" s="T1269">i-bi</ta>
            <ta e="T1271" id="Seg_2637" s="T1270">i</ta>
            <ta e="T1272" id="Seg_2638" s="T1271">tüj</ta>
            <ta e="T1273" id="Seg_2639" s="T1272">naga</ta>
            <ta e="T1274" id="Seg_2640" s="T1273">dĭgəttə</ta>
            <ta e="T1275" id="Seg_2641" s="T1274">kan-žə-bəj</ta>
            <ta e="T1276" id="Seg_2642" s="T1275">măn</ta>
            <ta e="T1277" id="Seg_2643" s="T1276">sürer-lV-m</ta>
            <ta e="T1278" id="Seg_2644" s="T1277">men</ta>
            <ta e="T1279" id="Seg_2645" s="T1278">ej</ta>
            <ta e="T1280" id="Seg_2646" s="T1279">mo-bi</ta>
            <ta e="T1281" id="Seg_2647" s="T1280">sürer-zittə</ta>
            <ta e="T1282" id="Seg_2648" s="T1281">dĭ</ta>
            <ta e="T1283" id="Seg_2649" s="T1282">urgaːba</ta>
            <ta e="T1284" id="Seg_2650" s="T1283">i</ta>
            <ta e="T1285" id="Seg_2651" s="T1284">buga</ta>
            <ta e="T1286" id="Seg_2652" s="T1285">ej</ta>
            <ta e="T1287" id="Seg_2653" s="T1286">mo-bi</ta>
            <ta e="T1288" id="Seg_2654" s="T1287">a</ta>
            <ta e="T1289" id="Seg_2655" s="T1288">tăn</ta>
            <ta e="T1290" id="Seg_2656" s="T1289">ej</ta>
            <ta e="T1291" id="Seg_2657" s="T1290">sürer-lV-l</ta>
            <ta e="T1292" id="Seg_2658" s="T1291">kan-žə-bəj</ta>
            <ta e="T1293" id="Seg_2659" s="T1292">dĭgəttə</ta>
            <ta e="T1294" id="Seg_2660" s="T1293">šo-bi</ta>
            <ta e="T1295" id="Seg_2661" s="T1294">bar</ta>
            <ta e="T1296" id="Seg_2662" s="T1295">kirgaːr-laʔbə</ta>
            <ta e="T1297" id="Seg_2663" s="T1296">uʔbdə-ʔ</ta>
            <ta e="T1298" id="Seg_2664" s="T1297">pʼeːš-gəʔ</ta>
            <ta e="T1301" id="Seg_2665" s="T1300">kan-ə-ʔ</ta>
            <ta e="T1302" id="Seg_2666" s="T1301">döʔə</ta>
            <ta e="T1304" id="Seg_2667" s="T1302">ato</ta>
            <ta e="T1305" id="Seg_2668" s="T1304">tüjö</ta>
            <ta e="T1306" id="Seg_2669" s="T1305">šapku-ziʔ</ta>
            <ta e="T1307" id="Seg_2670" s="T1306">tănan</ta>
            <ta e="T1308" id="Seg_2671" s="T1307">kut-lV-m</ta>
            <ta e="T1309" id="Seg_2672" s="T1308">bar</ta>
            <ta e="T1310" id="Seg_2673" s="T1309">dʼagar-lV-m</ta>
            <ta e="T1311" id="Seg_2674" s="T1310">tănan</ta>
            <ta e="T1312" id="Seg_2675" s="T1311">tüjö</ta>
            <ta e="T1313" id="Seg_2676" s="T1312">üjü-m</ta>
            <ta e="T1314" id="Seg_2677" s="T1313">üjü-jəʔ</ta>
            <ta e="T1315" id="Seg_2678" s="T1314">šer-liA-m</ta>
            <ta e="T1316" id="Seg_2679" s="T1315">jama-jəʔ</ta>
            <ta e="T1317" id="Seg_2680" s="T1316">dĭgəttə</ta>
            <ta e="T1318" id="Seg_2681" s="T1317">bazoʔ</ta>
            <ta e="T1319" id="Seg_2682" s="T1318">kirgaːr-laʔbə</ta>
            <ta e="T1320" id="Seg_2683" s="T1319">kan-ə-ʔ</ta>
            <ta e="T1321" id="Seg_2684" s="T1320">pʼeːš-gəʔ</ta>
            <ta e="T1322" id="Seg_2685" s="T1321">ato</ta>
            <ta e="T1323" id="Seg_2686" s="T1322">tüjö</ta>
            <ta e="T1324" id="Seg_2687" s="T1323">tănan</ta>
            <ta e="T1325" id="Seg_2688" s="T1324">kut-lV-m</ta>
            <ta e="T1326" id="Seg_2689" s="T1325">tagaj</ta>
            <ta e="T1327" id="Seg_2690" s="T1326">šapku-ziʔ</ta>
            <ta e="T1328" id="Seg_2691" s="T1327">bar</ta>
            <ta e="T1329" id="Seg_2692" s="T1328">băt-lV-m</ta>
            <ta e="T1330" id="Seg_2693" s="T1329">tüjö</ta>
            <ta e="T1331" id="Seg_2694" s="T1330">bar</ta>
            <ta e="T1332" id="Seg_2695" s="T1331">oldʼa-m</ta>
            <ta e="T1333" id="Seg_2696" s="T1332">šer-liA-m</ta>
            <ta e="T1334" id="Seg_2697" s="T1333">dĭgəttə</ta>
            <ta e="T1335" id="Seg_2698" s="T1334">dĭ</ta>
            <ta e="T1336" id="Seg_2699" s="T1335">bazoʔ</ta>
            <ta e="T1337" id="Seg_2700" s="T1336">kirgaːr-laʔbə</ta>
            <ta e="T1338" id="Seg_2701" s="T1337">kan-ə-ʔ</ta>
            <ta e="T1339" id="Seg_2702" s="T1338">tüjö</ta>
            <ta e="T1340" id="Seg_2703" s="T1339">nuʔmə-laʔbə-m</ta>
            <ta e="T1341" id="Seg_2704" s="T1340">dĭgəttə</ta>
            <ta e="T1342" id="Seg_2705" s="T1341">dĭ</ta>
            <ta e="T1343" id="Seg_2706" s="T1342">šapku-ziʔ</ta>
            <ta e="T1344" id="Seg_2707" s="T1343">dĭ-m</ta>
            <ta e="T1345" id="Seg_2708" s="T1344">bar</ta>
            <ta e="T1346" id="Seg_2709" s="T1345">dʼagar-luʔbdə-bi</ta>
            <ta e="T1347" id="Seg_2710" s="T1346">i</ta>
            <ta e="T1348" id="Seg_2711" s="T1347">dĭ</ta>
            <ta e="T1349" id="Seg_2712" s="T1348">kü-laːm-bi</ta>
            <ta e="T1350" id="Seg_2713" s="T1349">dĭgəttə</ta>
            <ta e="T1351" id="Seg_2714" s="T1350">amno-bi-jəʔ</ta>
            <ta e="T1352" id="Seg_2715" s="T1351">kuriza-n</ta>
            <ta e="T1353" id="Seg_2716" s="T1352">tibi</ta>
            <ta e="T1354" id="Seg_2717" s="T1353">i</ta>
            <ta e="T1355" id="Seg_2718" s="T1354">zajats</ta>
            <ta e="T1356" id="Seg_2719" s="T1355">tura-Kən</ta>
            <ta e="T1357" id="Seg_2720" s="T1356">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1032" id="Seg_2721" s="T1031">live-PST-3PL</ta>
            <ta e="T1033" id="Seg_2722" s="T1032">fox.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_2723" s="T1033">and</ta>
            <ta e="T1035" id="Seg_2724" s="T1034">hare.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_2725" s="T1035">fox-GEN</ta>
            <ta e="T1037" id="Seg_2726" s="T1036">be-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_2727" s="T1037">house-NOM/GEN.3SG</ta>
            <ta e="T1039" id="Seg_2728" s="T1038">snow-ABL</ta>
            <ta e="T1040" id="Seg_2729" s="T1039">and</ta>
            <ta e="T1041" id="Seg_2730" s="T1040">hare-GEN</ta>
            <ta e="T1042" id="Seg_2731" s="T1041">be-PST.[3SG]</ta>
            <ta e="T1044" id="Seg_2732" s="T1043">tree-ABL</ta>
            <ta e="T1045" id="Seg_2733" s="T1044">then</ta>
            <ta e="T1046" id="Seg_2734" s="T1045">come-PST.[3SG]</ta>
            <ta e="T1047" id="Seg_2735" s="T1046">warm.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_2736" s="T1047">fox-GEN</ta>
            <ta e="T1049" id="Seg_2737" s="T1048">house-NOM/GEN.3SG</ta>
            <ta e="T1050" id="Seg_2738" s="T1049">PTCL</ta>
            <ta e="T1051" id="Seg_2739" s="T1050">flow-MOM-PST.[3SG]</ta>
            <ta e="T1052" id="Seg_2740" s="T1051">then</ta>
            <ta e="T1053" id="Seg_2741" s="T1052">this.[NOM.SG]</ta>
            <ta e="T1054" id="Seg_2742" s="T1053">come-PST.[3SG]</ta>
            <ta e="T1055" id="Seg_2743" s="T1054">hare-LAT</ta>
            <ta e="T1056" id="Seg_2744" s="T1055">let-IMP.2SG</ta>
            <ta e="T1057" id="Seg_2745" s="T1056">I.ACC</ta>
            <ta e="T1058" id="Seg_2746" s="T1057">sleep-INF.LAT</ta>
            <ta e="T1059" id="Seg_2747" s="T1058">this.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_2748" s="T1059">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1061" id="Seg_2749" s="T1060">this-ACC</ta>
            <ta e="T1062" id="Seg_2750" s="T1061">drive-MOM-PST.[3SG]</ta>
            <ta e="T1063" id="Seg_2751" s="T1062">house-ABL</ta>
            <ta e="T1064" id="Seg_2752" s="T1063">this.[NOM.SG]</ta>
            <ta e="T1065" id="Seg_2753" s="T1064">come-PRS.[3SG]</ta>
            <ta e="T1066" id="Seg_2754" s="T1065">and</ta>
            <ta e="T1067" id="Seg_2755" s="T1066">cry-DUR.[3SG]</ta>
            <ta e="T1068" id="Seg_2756" s="T1067">I.NOM</ta>
            <ta e="T1069" id="Seg_2757" s="T1068">dog.[NOM.SG]</ta>
            <ta e="T1070" id="Seg_2758" s="T1069">say-IPFVZ.[3SG]</ta>
            <ta e="T1071" id="Seg_2759" s="T1070">what.[NOM.SG]</ta>
            <ta e="T1072" id="Seg_2760" s="T1071">you.NOM</ta>
            <ta e="T1073" id="Seg_2761" s="T1072">cry-DUR-2SG</ta>
            <ta e="T1074" id="Seg_2762" s="T1073">and</ta>
            <ta e="T1075" id="Seg_2763" s="T1074">I.NOM</ta>
            <ta e="T1076" id="Seg_2764" s="T1075">house-ABL</ta>
            <ta e="T1077" id="Seg_2765" s="T1076">say-IPFVZ.[3SG]</ta>
            <ta e="T1078" id="Seg_2766" s="T1077">drive-PST.[3SG]</ta>
            <ta e="T1079" id="Seg_2767" s="T1078">fox.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_2768" s="T1079">go-OPT.DU/PL-1DU</ta>
            <ta e="T1081" id="Seg_2769" s="T1080">I.NOM</ta>
            <ta e="T1082" id="Seg_2770" s="T1081">this-ACC</ta>
            <ta e="T1083" id="Seg_2771" s="T1082">drive-FUT-1SG</ta>
            <ta e="T1084" id="Seg_2772" s="T1083">come-PST.[3SG]</ta>
            <ta e="T1085" id="Seg_2773" s="T1084">dog.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_2774" s="T1085">and</ta>
            <ta e="T1087" id="Seg_2775" s="T1086">always</ta>
            <ta e="T1088" id="Seg_2776" s="T1087">shout-DUR.[3SG]</ta>
            <ta e="T1089" id="Seg_2777" s="T1088">go-EP-IMP.2SG</ta>
            <ta e="T1090" id="Seg_2778" s="T1089">hence</ta>
            <ta e="T1091" id="Seg_2779" s="T1090">and</ta>
            <ta e="T1092" id="Seg_2780" s="T1091">this.[NOM.SG]</ta>
            <ta e="T1093" id="Seg_2781" s="T1092">say-IPFVZ.[3SG]</ta>
            <ta e="T1094" id="Seg_2782" s="T1093">soon</ta>
            <ta e="T1095" id="Seg_2783" s="T1094">you.GEN</ta>
            <ta e="T1096" id="Seg_2784" s="T1095">PTCL</ta>
            <ta e="T1097" id="Seg_2785" s="T1096">hair-NOM/GEN/ACC.2SG</ta>
            <ta e="T1098" id="Seg_2786" s="T1097">tear-FUT-1SG</ta>
            <ta e="T1099" id="Seg_2787" s="T1098">and</ta>
            <ta e="T1100" id="Seg_2788" s="T1099">you.NOM</ta>
            <ta e="T1101" id="Seg_2789" s="T1100">run-MOM-FUT-2SG</ta>
            <ta e="T1102" id="Seg_2790" s="T1101">this.[NOM.SG]</ta>
            <ta e="T1103" id="Seg_2791" s="T1102">frighten-MOM-PST.[3SG]</ta>
            <ta e="T1104" id="Seg_2792" s="T1103">and</ta>
            <ta e="T1105" id="Seg_2793" s="T1104">run-MOM-PST.[3SG]</ta>
            <ta e="T1106" id="Seg_2794" s="T1105">again</ta>
            <ta e="T1107" id="Seg_2795" s="T1106">walk-PRS.[3SG]</ta>
            <ta e="T1108" id="Seg_2796" s="T1107">hare.[NOM.SG]</ta>
            <ta e="T1109" id="Seg_2797" s="T1108">and</ta>
            <ta e="T1110" id="Seg_2798" s="T1109">cry-DUR.[3SG]</ta>
            <ta e="T1111" id="Seg_2799" s="T1110">then</ta>
            <ta e="T1112" id="Seg_2800" s="T1111">come-PRS.[3SG]</ta>
            <ta e="T1113" id="Seg_2801" s="T1112">bear.[NOM.SG]</ta>
            <ta e="T1114" id="Seg_2802" s="T1113">what.[NOM.SG]</ta>
            <ta e="T1115" id="Seg_2803" s="T1114">cry-DUR-2SG</ta>
            <ta e="T1116" id="Seg_2804" s="T1115">and</ta>
            <ta e="T1117" id="Seg_2805" s="T1116">like</ta>
            <ta e="T1118" id="Seg_2806" s="T1117">I.LAT</ta>
            <ta e="T1119" id="Seg_2807" s="T1118">NEG</ta>
            <ta e="T1120" id="Seg_2808" s="T1119">cry-INF.LAT</ta>
            <ta e="T1121" id="Seg_2809" s="T1120">I.NOM</ta>
            <ta e="T1122" id="Seg_2810" s="T1121">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T1123" id="Seg_2811" s="T1122">be-PST.[3SG]</ta>
            <ta e="T1124" id="Seg_2812" s="T1123">tree-ABL</ta>
            <ta e="T1125" id="Seg_2813" s="T1124">and</ta>
            <ta e="T1126" id="Seg_2814" s="T1125">fox-GEN</ta>
            <ta e="T1127" id="Seg_2815" s="T1126">house-NOM/GEN.3SG</ta>
            <ta e="T1129" id="Seg_2816" s="T1128">snow-ABL</ta>
            <ta e="T1130" id="Seg_2817" s="T1129">now</ta>
            <ta e="T1131" id="Seg_2818" s="T1130">this.[NOM.SG]</ta>
            <ta e="T1132" id="Seg_2819" s="T1131">come-PST.[3SG]</ta>
            <ta e="T1133" id="Seg_2820" s="T1132">I.LAT</ta>
            <ta e="T1134" id="Seg_2821" s="T1133">spend.night-INF.LAT</ta>
            <ta e="T1135" id="Seg_2822" s="T1134">and</ta>
            <ta e="T1136" id="Seg_2823" s="T1135">I.ACC</ta>
            <ta e="T1137" id="Seg_2824" s="T1136">drive-MOM-PST.[3SG]</ta>
            <ta e="T1138" id="Seg_2825" s="T1137">go-OPT.DU/PL-1DU</ta>
            <ta e="T1139" id="Seg_2826" s="T1138">I.NOM</ta>
            <ta e="T1140" id="Seg_2827" s="T1139">this-ACC</ta>
            <ta e="T1141" id="Seg_2828" s="T1140">drive-FUT-1SG</ta>
            <ta e="T1142" id="Seg_2829" s="T1141">come-PST.[3SG]</ta>
            <ta e="T1143" id="Seg_2830" s="T1142">go-EP-IMP.2SG</ta>
            <ta e="T1144" id="Seg_2831" s="T1143">hence</ta>
            <ta e="T1145" id="Seg_2832" s="T1144">otherwise</ta>
            <ta e="T1146" id="Seg_2833" s="T1145">soon</ta>
            <ta e="T1147" id="Seg_2834" s="T1146">you.ACC</ta>
            <ta e="T1148" id="Seg_2835" s="T1147">beat-FUT-1SG</ta>
            <ta e="T1149" id="Seg_2836" s="T1148">and</ta>
            <ta e="T1150" id="Seg_2837" s="T1149">this.[NOM.SG]</ta>
            <ta e="T1151" id="Seg_2838" s="T1150">say-IPFVZ.[3SG]</ta>
            <ta e="T1152" id="Seg_2839" s="T1151">I.NOM</ta>
            <ta e="T1153" id="Seg_2840" s="T1152">like</ta>
            <ta e="T1154" id="Seg_2841" s="T1153">run-MOM-FUT-1SG</ta>
            <ta e="T1155" id="Seg_2842" s="T1154">and</ta>
            <ta e="T1156" id="Seg_2843" s="T1155">PTCL</ta>
            <ta e="T1157" id="Seg_2844" s="T1156">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T1158" id="Seg_2845" s="T1157">you.NOM</ta>
            <ta e="T1159" id="Seg_2846" s="T1158">tear-MOM-FUT-2SG</ta>
            <ta e="T1160" id="Seg_2847" s="T1159">and</ta>
            <ta e="T1161" id="Seg_2848" s="T1160">you.NOM</ta>
            <ta e="T1162" id="Seg_2849" s="T1161">run-MOM-FUT-2SG</ta>
            <ta e="T1163" id="Seg_2850" s="T1162">then</ta>
            <ta e="T1164" id="Seg_2851" s="T1163">bear.[NOM.SG]</ta>
            <ta e="T1166" id="Seg_2852" s="T1165">fear-MOM-PST.[3SG]</ta>
            <ta e="T1167" id="Seg_2853" s="T1166">and</ta>
            <ta e="T1168" id="Seg_2854" s="T1167">run-MOM-PST.[3SG]</ta>
            <ta e="T1169" id="Seg_2855" s="T1168">again</ta>
            <ta e="T1170" id="Seg_2856" s="T1169">walk-PRS.[3SG]</ta>
            <ta e="T1171" id="Seg_2857" s="T1170">hare.[NOM.SG]</ta>
            <ta e="T1172" id="Seg_2858" s="T1171">and</ta>
            <ta e="T1173" id="Seg_2859" s="T1172">cry-DUR.[3SG]</ta>
            <ta e="T1174" id="Seg_2860" s="T1173">then</ta>
            <ta e="T1175" id="Seg_2861" s="T1174">bull.[NOM.SG]</ta>
            <ta e="T1176" id="Seg_2862" s="T1175">come-PRS.[3SG]</ta>
            <ta e="T1177" id="Seg_2863" s="T1176">what.[NOM.SG]</ta>
            <ta e="T1178" id="Seg_2864" s="T1177">cry-DUR-2SG</ta>
            <ta e="T1179" id="Seg_2865" s="T1178">and</ta>
            <ta e="T1180" id="Seg_2866" s="T1179">I.GEN</ta>
            <ta e="T1181" id="Seg_2867" s="T1180">house-ACC</ta>
            <ta e="T1182" id="Seg_2868" s="T1181">fox.[NOM.SG]</ta>
            <ta e="T1183" id="Seg_2869" s="T1182">take-PST.[3SG]</ta>
            <ta e="T1184" id="Seg_2870" s="T1183">and</ta>
            <ta e="T1185" id="Seg_2871" s="T1184">this-GEN</ta>
            <ta e="T1187" id="Seg_2872" s="T1186">be-PST.[3SG]</ta>
            <ta e="T1188" id="Seg_2873" s="T1187">snow-ABL</ta>
            <ta e="T1189" id="Seg_2874" s="T1188">and</ta>
            <ta e="T1190" id="Seg_2875" s="T1189">die-RES-PST.[3SG]</ta>
            <ta e="T1191" id="Seg_2876" s="T1190">and</ta>
            <ta e="T1192" id="Seg_2877" s="T1191">I.GEN</ta>
            <ta e="T1193" id="Seg_2878" s="T1192">always</ta>
            <ta e="T1194" id="Seg_2879" s="T1193">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T1195" id="Seg_2880" s="T1194">good.[NOM.SG]</ta>
            <ta e="T1196" id="Seg_2881" s="T1195">tree-ABL</ta>
            <ta e="T1197" id="Seg_2882" s="T1196">then</ta>
            <ta e="T1198" id="Seg_2883" s="T1197">go-OPT.DU/PL-1DU</ta>
            <ta e="T1199" id="Seg_2884" s="T1198">I.NOM</ta>
            <ta e="T1200" id="Seg_2885" s="T1199">this-ACC</ta>
            <ta e="T1201" id="Seg_2886" s="T1200">drive-FUT-1SG</ta>
            <ta e="T1202" id="Seg_2887" s="T1201">no</ta>
            <ta e="T1203" id="Seg_2888" s="T1202">NEG</ta>
            <ta e="T1204" id="Seg_2889" s="T1203">drive-FUT-2SG</ta>
            <ta e="T1205" id="Seg_2890" s="T1204">bear.[NOM.SG]</ta>
            <ta e="T1206" id="Seg_2891" s="T1205">NEG</ta>
            <ta e="T1207" id="Seg_2892" s="T1206">can-PST.[3SG]</ta>
            <ta e="T1208" id="Seg_2893" s="T1207">drive-INF.LAT</ta>
            <ta e="T1209" id="Seg_2894" s="T1208">dog.[NOM.SG]</ta>
            <ta e="T1210" id="Seg_2895" s="T1209">NEG</ta>
            <ta e="T1211" id="Seg_2896" s="T1210">can-PST.[3SG]</ta>
            <ta e="T1212" id="Seg_2897" s="T1211">bull.[NOM.SG]</ta>
            <ta e="T1213" id="Seg_2898" s="T1212">and</ta>
            <ta e="T1214" id="Seg_2899" s="T1213">you.NOM</ta>
            <ta e="T1216" id="Seg_2900" s="T1215">then</ta>
            <ta e="T1218" id="Seg_2901" s="T1217">come-PST-3PL</ta>
            <ta e="T1219" id="Seg_2902" s="T1218">come-PST-3PL</ta>
            <ta e="T1221" id="Seg_2903" s="T1220">bull.[NOM.SG]</ta>
            <ta e="T1222" id="Seg_2904" s="T1221">say-IPFVZ.[3SG]</ta>
            <ta e="T1225" id="Seg_2905" s="T1224">go-EP-IMP.2SG</ta>
            <ta e="T1226" id="Seg_2906" s="T1225">house-ABL</ta>
            <ta e="T1227" id="Seg_2907" s="T1226">and</ta>
            <ta e="T1228" id="Seg_2908" s="T1227">this.[NOM.SG]</ta>
            <ta e="T1229" id="Seg_2909" s="T1228">say-IPFVZ.[3SG]</ta>
            <ta e="T1230" id="Seg_2910" s="T1229">soon</ta>
            <ta e="T1231" id="Seg_2911" s="T1230">jump-MOM-FUT-1SG</ta>
            <ta e="T1232" id="Seg_2912" s="T1231">so</ta>
            <ta e="T1233" id="Seg_2913" s="T1232">PTCL</ta>
            <ta e="T1234" id="Seg_2914" s="T1233">you.ACC</ta>
            <ta e="T1235" id="Seg_2915" s="T1234">off</ta>
            <ta e="T1236" id="Seg_2916" s="T1235">%tear.off-MOM-FUT-1SG</ta>
            <ta e="T1237" id="Seg_2917" s="T1236">then</ta>
            <ta e="T1238" id="Seg_2918" s="T1237">bull.[NOM.SG]</ta>
            <ta e="T1239" id="Seg_2919" s="T1238">bull.[NOM.SG]</ta>
            <ta e="T1240" id="Seg_2920" s="T1239">frighten-MOM-PST.[3SG]</ta>
            <ta e="T1241" id="Seg_2921" s="T1240">and</ta>
            <ta e="T1242" id="Seg_2922" s="T1241">run-MOM-PST.[3SG]</ta>
            <ta e="T1243" id="Seg_2923" s="T1242">then</ta>
            <ta e="T1244" id="Seg_2924" s="T1243">again</ta>
            <ta e="T1245" id="Seg_2925" s="T1244">walk-PRS.[3SG]</ta>
            <ta e="T1246" id="Seg_2926" s="T1245">hare.[NOM.SG]</ta>
            <ta e="T1247" id="Seg_2927" s="T1246">and</ta>
            <ta e="T1248" id="Seg_2928" s="T1247">cry-DUR.[3SG]</ta>
            <ta e="T1249" id="Seg_2929" s="T1248">then</ta>
            <ta e="T1250" id="Seg_2930" s="T1249">hen-GEN</ta>
            <ta e="T1251" id="Seg_2931" s="T1250">man.[NOM.SG]</ta>
            <ta e="T1252" id="Seg_2932" s="T1251">come-PRS.[3SG]</ta>
            <ta e="T1253" id="Seg_2933" s="T1252">scythe-INS</ta>
            <ta e="T1254" id="Seg_2934" s="T1253">say-IPFVZ.[3SG]</ta>
            <ta e="T1255" id="Seg_2935" s="T1254">what.[NOM.SG]</ta>
            <ta e="T1256" id="Seg_2936" s="T1255">cry-DUR-2SG</ta>
            <ta e="T1257" id="Seg_2937" s="T1256">and</ta>
            <ta e="T1258" id="Seg_2938" s="T1257">I.GEN</ta>
            <ta e="T1259" id="Seg_2939" s="T1258">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T1260" id="Seg_2940" s="T1259">tree-ABL</ta>
            <ta e="T1261" id="Seg_2941" s="T1260">fox.[NOM.SG]</ta>
            <ta e="T1262" id="Seg_2942" s="T1261">come-PST.[3SG]</ta>
            <ta e="T1263" id="Seg_2943" s="T1262">sleep-INF.LAT</ta>
            <ta e="T1264" id="Seg_2944" s="T1263">and</ta>
            <ta e="T1265" id="Seg_2945" s="T1264">I.ACC</ta>
            <ta e="T1266" id="Seg_2946" s="T1265">drive-PST.[3SG]</ta>
            <ta e="T1267" id="Seg_2947" s="T1266">and</ta>
            <ta e="T1268" id="Seg_2948" s="T1267">this-GEN</ta>
            <ta e="T1269" id="Seg_2949" s="T1268">snow-ABL</ta>
            <ta e="T1270" id="Seg_2950" s="T1269">be-PST.[3SG]</ta>
            <ta e="T1271" id="Seg_2951" s="T1270">and</ta>
            <ta e="T1272" id="Seg_2952" s="T1271">now</ta>
            <ta e="T1273" id="Seg_2953" s="T1272">NEG.EX</ta>
            <ta e="T1274" id="Seg_2954" s="T1273">then</ta>
            <ta e="T1275" id="Seg_2955" s="T1274">go-OPT.DU/PL-1DU</ta>
            <ta e="T1276" id="Seg_2956" s="T1275">I.NOM</ta>
            <ta e="T1277" id="Seg_2957" s="T1276">drive-FUT-1SG</ta>
            <ta e="T1278" id="Seg_2958" s="T1277">dog.[NOM.SG]</ta>
            <ta e="T1279" id="Seg_2959" s="T1278">NEG</ta>
            <ta e="T1280" id="Seg_2960" s="T1279">can-PST.[3SG]</ta>
            <ta e="T1281" id="Seg_2961" s="T1280">drive-INF.LAT</ta>
            <ta e="T1282" id="Seg_2962" s="T1281">this.[NOM.SG]</ta>
            <ta e="T1283" id="Seg_2963" s="T1282">bear.[NOM.SG]</ta>
            <ta e="T1284" id="Seg_2964" s="T1283">and</ta>
            <ta e="T1285" id="Seg_2965" s="T1284">bull.[NOM.SG]</ta>
            <ta e="T1286" id="Seg_2966" s="T1285">NEG</ta>
            <ta e="T1287" id="Seg_2967" s="T1286">can-PST.[3SG]</ta>
            <ta e="T1288" id="Seg_2968" s="T1287">and</ta>
            <ta e="T1289" id="Seg_2969" s="T1288">you.NOM</ta>
            <ta e="T1290" id="Seg_2970" s="T1289">NEG</ta>
            <ta e="T1291" id="Seg_2971" s="T1290">drive-FUT-2SG</ta>
            <ta e="T1292" id="Seg_2972" s="T1291">go-OPT.DU/PL-1DU</ta>
            <ta e="T1293" id="Seg_2973" s="T1292">then</ta>
            <ta e="T1294" id="Seg_2974" s="T1293">come-PST.[3SG]</ta>
            <ta e="T1295" id="Seg_2975" s="T1294">PTCL</ta>
            <ta e="T1296" id="Seg_2976" s="T1295">shout-DUR.[3SG]</ta>
            <ta e="T1297" id="Seg_2977" s="T1296">get.up-IMP.2SG</ta>
            <ta e="T1298" id="Seg_2978" s="T1297">stove-ABL</ta>
            <ta e="T1301" id="Seg_2979" s="T1300">go-EP-IMP.2SG</ta>
            <ta e="T1302" id="Seg_2980" s="T1301">hence</ta>
            <ta e="T1304" id="Seg_2981" s="T1302">otherwise</ta>
            <ta e="T1305" id="Seg_2982" s="T1304">soon</ta>
            <ta e="T1306" id="Seg_2983" s="T1305">scythe-INS</ta>
            <ta e="T1307" id="Seg_2984" s="T1306">you.ACC</ta>
            <ta e="T1308" id="Seg_2985" s="T1307">kill-FUT-1SG</ta>
            <ta e="T1309" id="Seg_2986" s="T1308">PTCL</ta>
            <ta e="T1310" id="Seg_2987" s="T1309">stab-FUT-1SG</ta>
            <ta e="T1311" id="Seg_2988" s="T1310">you.ACC</ta>
            <ta e="T1312" id="Seg_2989" s="T1311">soon</ta>
            <ta e="T1313" id="Seg_2990" s="T1312">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T1314" id="Seg_2991" s="T1313">foot-PL</ta>
            <ta e="T1315" id="Seg_2992" s="T1314">dress-PRS-1SG</ta>
            <ta e="T1316" id="Seg_2993" s="T1315">boot-PL</ta>
            <ta e="T1317" id="Seg_2994" s="T1316">then</ta>
            <ta e="T1318" id="Seg_2995" s="T1317">again</ta>
            <ta e="T1319" id="Seg_2996" s="T1318">shout-DUR.[3SG]</ta>
            <ta e="T1320" id="Seg_2997" s="T1319">go-EP-IMP.2SG</ta>
            <ta e="T1321" id="Seg_2998" s="T1320">stove-ABL</ta>
            <ta e="T1322" id="Seg_2999" s="T1321">otherwise</ta>
            <ta e="T1323" id="Seg_3000" s="T1322">soon</ta>
            <ta e="T1324" id="Seg_3001" s="T1323">you.ACC</ta>
            <ta e="T1325" id="Seg_3002" s="T1324">kill-FUT-1SG</ta>
            <ta e="T1326" id="Seg_3003" s="T1325">knife.[NOM.SG]</ta>
            <ta e="T1327" id="Seg_3004" s="T1326">scythe-INS</ta>
            <ta e="T1328" id="Seg_3005" s="T1327">PTCL</ta>
            <ta e="T1329" id="Seg_3006" s="T1328">cut-FUT-1SG</ta>
            <ta e="T1330" id="Seg_3007" s="T1329">soon</ta>
            <ta e="T1331" id="Seg_3008" s="T1330">PTCL</ta>
            <ta e="T1332" id="Seg_3009" s="T1331">clothing-ACC</ta>
            <ta e="T1333" id="Seg_3010" s="T1332">dress-PRS-1SG</ta>
            <ta e="T1334" id="Seg_3011" s="T1333">then</ta>
            <ta e="T1335" id="Seg_3012" s="T1334">this.[NOM.SG]</ta>
            <ta e="T1336" id="Seg_3013" s="T1335">again</ta>
            <ta e="T1337" id="Seg_3014" s="T1336">shout-DUR.[3SG]</ta>
            <ta e="T1338" id="Seg_3015" s="T1337">go-EP-IMP.2SG</ta>
            <ta e="T1339" id="Seg_3016" s="T1338">soon</ta>
            <ta e="T1340" id="Seg_3017" s="T1339">run-DUR-1SG</ta>
            <ta e="T1341" id="Seg_3018" s="T1340">then</ta>
            <ta e="T1342" id="Seg_3019" s="T1341">this.[NOM.SG]</ta>
            <ta e="T1343" id="Seg_3020" s="T1342">scythe-INS</ta>
            <ta e="T1344" id="Seg_3021" s="T1343">this-ACC</ta>
            <ta e="T1345" id="Seg_3022" s="T1344">PTCL</ta>
            <ta e="T1346" id="Seg_3023" s="T1345">stab-MOM-PST.[3SG]</ta>
            <ta e="T1347" id="Seg_3024" s="T1346">and</ta>
            <ta e="T1348" id="Seg_3025" s="T1347">this.[NOM.SG]</ta>
            <ta e="T1349" id="Seg_3026" s="T1348">die-RES-PST.[3SG]</ta>
            <ta e="T1350" id="Seg_3027" s="T1349">then</ta>
            <ta e="T1351" id="Seg_3028" s="T1350">live-PST-3PL</ta>
            <ta e="T1352" id="Seg_3029" s="T1351">hen-GEN</ta>
            <ta e="T1353" id="Seg_3030" s="T1352">man.[NOM.SG]</ta>
            <ta e="T1354" id="Seg_3031" s="T1353">and</ta>
            <ta e="T1355" id="Seg_3032" s="T1354">hare.[NOM.SG]</ta>
            <ta e="T1356" id="Seg_3033" s="T1355">house-LOC</ta>
            <ta e="T1357" id="Seg_3034" s="T1356">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1032" id="Seg_3035" s="T1031">жить-PST-3PL</ta>
            <ta e="T1033" id="Seg_3036" s="T1032">лиса.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_3037" s="T1033">и</ta>
            <ta e="T1035" id="Seg_3038" s="T1034">заяц.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_3039" s="T1035">лиса-GEN</ta>
            <ta e="T1037" id="Seg_3040" s="T1036">быть-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_3041" s="T1037">дом-NOM/GEN.3SG</ta>
            <ta e="T1039" id="Seg_3042" s="T1038">снег-ABL</ta>
            <ta e="T1040" id="Seg_3043" s="T1039">а</ta>
            <ta e="T1041" id="Seg_3044" s="T1040">заяц-GEN</ta>
            <ta e="T1042" id="Seg_3045" s="T1041">быть-PST.[3SG]</ta>
            <ta e="T1044" id="Seg_3046" s="T1043">дерево-ABL</ta>
            <ta e="T1045" id="Seg_3047" s="T1044">тогда</ta>
            <ta e="T1046" id="Seg_3048" s="T1045">прийти-PST.[3SG]</ta>
            <ta e="T1047" id="Seg_3049" s="T1046">теплый.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_3050" s="T1047">лиса-GEN</ta>
            <ta e="T1049" id="Seg_3051" s="T1048">дом-NOM/GEN.3SG</ta>
            <ta e="T1050" id="Seg_3052" s="T1049">PTCL</ta>
            <ta e="T1051" id="Seg_3053" s="T1050">течь-MOM-PST.[3SG]</ta>
            <ta e="T1052" id="Seg_3054" s="T1051">тогда</ta>
            <ta e="T1053" id="Seg_3055" s="T1052">этот.[NOM.SG]</ta>
            <ta e="T1054" id="Seg_3056" s="T1053">прийти-PST.[3SG]</ta>
            <ta e="T1055" id="Seg_3057" s="T1054">заяц-LAT</ta>
            <ta e="T1056" id="Seg_3058" s="T1055">пускать-IMP.2SG</ta>
            <ta e="T1057" id="Seg_3059" s="T1056">я.ACC</ta>
            <ta e="T1058" id="Seg_3060" s="T1057">спать-INF.LAT</ta>
            <ta e="T1059" id="Seg_3061" s="T1058">этот.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_3062" s="T1059">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1061" id="Seg_3063" s="T1060">этот-ACC</ta>
            <ta e="T1062" id="Seg_3064" s="T1061">гнать-MOM-PST.[3SG]</ta>
            <ta e="T1063" id="Seg_3065" s="T1062">дом-ABL</ta>
            <ta e="T1064" id="Seg_3066" s="T1063">этот.[NOM.SG]</ta>
            <ta e="T1065" id="Seg_3067" s="T1064">прийти-PRS.[3SG]</ta>
            <ta e="T1066" id="Seg_3068" s="T1065">и</ta>
            <ta e="T1067" id="Seg_3069" s="T1066">плакать-DUR.[3SG]</ta>
            <ta e="T1068" id="Seg_3070" s="T1067">я.NOM</ta>
            <ta e="T1069" id="Seg_3071" s="T1068">собака.[NOM.SG]</ta>
            <ta e="T1070" id="Seg_3072" s="T1069">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1071" id="Seg_3073" s="T1070">что.[NOM.SG]</ta>
            <ta e="T1072" id="Seg_3074" s="T1071">ты.NOM</ta>
            <ta e="T1073" id="Seg_3075" s="T1072">плакать-DUR-2SG</ta>
            <ta e="T1074" id="Seg_3076" s="T1073">и</ta>
            <ta e="T1075" id="Seg_3077" s="T1074">я.NOM</ta>
            <ta e="T1076" id="Seg_3078" s="T1075">дом-ABL</ta>
            <ta e="T1077" id="Seg_3079" s="T1076">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1078" id="Seg_3080" s="T1077">гнать-PST.[3SG]</ta>
            <ta e="T1079" id="Seg_3081" s="T1078">лисичка.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_3082" s="T1079">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1081" id="Seg_3083" s="T1080">я.NOM</ta>
            <ta e="T1082" id="Seg_3084" s="T1081">этот-ACC</ta>
            <ta e="T1083" id="Seg_3085" s="T1082">гнать-FUT-1SG</ta>
            <ta e="T1084" id="Seg_3086" s="T1083">прийти-PST.[3SG]</ta>
            <ta e="T1085" id="Seg_3087" s="T1084">собака.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_3088" s="T1085">и</ta>
            <ta e="T1087" id="Seg_3089" s="T1086">всегда</ta>
            <ta e="T1088" id="Seg_3090" s="T1087">кричать-DUR.[3SG]</ta>
            <ta e="T1089" id="Seg_3091" s="T1088">пойти-EP-IMP.2SG</ta>
            <ta e="T1090" id="Seg_3092" s="T1089">отсюда</ta>
            <ta e="T1091" id="Seg_3093" s="T1090">а</ta>
            <ta e="T1092" id="Seg_3094" s="T1091">этот.[NOM.SG]</ta>
            <ta e="T1093" id="Seg_3095" s="T1092">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1094" id="Seg_3096" s="T1093">скоро</ta>
            <ta e="T1095" id="Seg_3097" s="T1094">ты.GEN</ta>
            <ta e="T1096" id="Seg_3098" s="T1095">PTCL</ta>
            <ta e="T1097" id="Seg_3099" s="T1096">шерсть-NOM/GEN/ACC.2SG</ta>
            <ta e="T1098" id="Seg_3100" s="T1097">рвать-FUT-1SG</ta>
            <ta e="T1099" id="Seg_3101" s="T1098">и</ta>
            <ta e="T1100" id="Seg_3102" s="T1099">ты.NOM</ta>
            <ta e="T1101" id="Seg_3103" s="T1100">бежать-MOM-FUT-2SG</ta>
            <ta e="T1102" id="Seg_3104" s="T1101">этот.[NOM.SG]</ta>
            <ta e="T1103" id="Seg_3105" s="T1102">пугать-MOM-PST.[3SG]</ta>
            <ta e="T1104" id="Seg_3106" s="T1103">и</ta>
            <ta e="T1105" id="Seg_3107" s="T1104">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1106" id="Seg_3108" s="T1105">опять</ta>
            <ta e="T1107" id="Seg_3109" s="T1106">идти-PRS.[3SG]</ta>
            <ta e="T1108" id="Seg_3110" s="T1107">заяц.[NOM.SG]</ta>
            <ta e="T1109" id="Seg_3111" s="T1108">и</ta>
            <ta e="T1110" id="Seg_3112" s="T1109">плакать-DUR.[3SG]</ta>
            <ta e="T1111" id="Seg_3113" s="T1110">тогда</ta>
            <ta e="T1112" id="Seg_3114" s="T1111">прийти-PRS.[3SG]</ta>
            <ta e="T1113" id="Seg_3115" s="T1112">медведь.[NOM.SG]</ta>
            <ta e="T1114" id="Seg_3116" s="T1113">что.[NOM.SG]</ta>
            <ta e="T1115" id="Seg_3117" s="T1114">плакать-DUR-2SG</ta>
            <ta e="T1116" id="Seg_3118" s="T1115">и</ta>
            <ta e="T1117" id="Seg_3119" s="T1116">как</ta>
            <ta e="T1118" id="Seg_3120" s="T1117">я.LAT</ta>
            <ta e="T1119" id="Seg_3121" s="T1118">NEG</ta>
            <ta e="T1120" id="Seg_3122" s="T1119">плакать-INF.LAT</ta>
            <ta e="T1121" id="Seg_3123" s="T1120">я.NOM</ta>
            <ta e="T1122" id="Seg_3124" s="T1121">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T1123" id="Seg_3125" s="T1122">быть-PST.[3SG]</ta>
            <ta e="T1124" id="Seg_3126" s="T1123">дерево-ABL</ta>
            <ta e="T1125" id="Seg_3127" s="T1124">а</ta>
            <ta e="T1126" id="Seg_3128" s="T1125">лисица-GEN</ta>
            <ta e="T1127" id="Seg_3129" s="T1126">дом-NOM/GEN.3SG</ta>
            <ta e="T1129" id="Seg_3130" s="T1128">снег-ABL</ta>
            <ta e="T1130" id="Seg_3131" s="T1129">сейчас</ta>
            <ta e="T1131" id="Seg_3132" s="T1130">этот.[NOM.SG]</ta>
            <ta e="T1132" id="Seg_3133" s="T1131">прийти-PST.[3SG]</ta>
            <ta e="T1133" id="Seg_3134" s="T1132">я.LAT</ta>
            <ta e="T1134" id="Seg_3135" s="T1133">ночевать-INF.LAT</ta>
            <ta e="T1135" id="Seg_3136" s="T1134">и</ta>
            <ta e="T1136" id="Seg_3137" s="T1135">я.ACC</ta>
            <ta e="T1137" id="Seg_3138" s="T1136">гнать-MOM-PST.[3SG]</ta>
            <ta e="T1138" id="Seg_3139" s="T1137">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1139" id="Seg_3140" s="T1138">я.NOM</ta>
            <ta e="T1140" id="Seg_3141" s="T1139">этот-ACC</ta>
            <ta e="T1141" id="Seg_3142" s="T1140">гнать-FUT-1SG</ta>
            <ta e="T1142" id="Seg_3143" s="T1141">прийти-PST.[3SG]</ta>
            <ta e="T1143" id="Seg_3144" s="T1142">пойти-EP-IMP.2SG</ta>
            <ta e="T1144" id="Seg_3145" s="T1143">отсюда</ta>
            <ta e="T1145" id="Seg_3146" s="T1144">а.то</ta>
            <ta e="T1146" id="Seg_3147" s="T1145">скоро</ta>
            <ta e="T1147" id="Seg_3148" s="T1146">ты.ACC</ta>
            <ta e="T1148" id="Seg_3149" s="T1147">бить-FUT-1SG</ta>
            <ta e="T1149" id="Seg_3150" s="T1148">а</ta>
            <ta e="T1150" id="Seg_3151" s="T1149">этот.[NOM.SG]</ta>
            <ta e="T1151" id="Seg_3152" s="T1150">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1152" id="Seg_3153" s="T1151">я.NOM</ta>
            <ta e="T1153" id="Seg_3154" s="T1152">как</ta>
            <ta e="T1154" id="Seg_3155" s="T1153">бежать-MOM-FUT-1SG</ta>
            <ta e="T1155" id="Seg_3156" s="T1154">и</ta>
            <ta e="T1156" id="Seg_3157" s="T1155">PTCL</ta>
            <ta e="T1157" id="Seg_3158" s="T1156">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T1158" id="Seg_3159" s="T1157">ты.NOM</ta>
            <ta e="T1159" id="Seg_3160" s="T1158">рвать-MOM-FUT-2SG</ta>
            <ta e="T1160" id="Seg_3161" s="T1159">и</ta>
            <ta e="T1161" id="Seg_3162" s="T1160">ты.NOM</ta>
            <ta e="T1162" id="Seg_3163" s="T1161">бежать-MOM-FUT-2SG</ta>
            <ta e="T1163" id="Seg_3164" s="T1162">тогда</ta>
            <ta e="T1164" id="Seg_3165" s="T1163">медведь.[NOM.SG]</ta>
            <ta e="T1166" id="Seg_3166" s="T1165">бояться-MOM-PST.[3SG]</ta>
            <ta e="T1167" id="Seg_3167" s="T1166">и</ta>
            <ta e="T1168" id="Seg_3168" s="T1167">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1169" id="Seg_3169" s="T1168">опять</ta>
            <ta e="T1170" id="Seg_3170" s="T1169">идти-PRS.[3SG]</ta>
            <ta e="T1171" id="Seg_3171" s="T1170">зайчик.[NOM.SG]</ta>
            <ta e="T1172" id="Seg_3172" s="T1171">и</ta>
            <ta e="T1173" id="Seg_3173" s="T1172">плакать-DUR.[3SG]</ta>
            <ta e="T1174" id="Seg_3174" s="T1173">тогда</ta>
            <ta e="T1175" id="Seg_3175" s="T1174">бык.[NOM.SG]</ta>
            <ta e="T1176" id="Seg_3176" s="T1175">прийти-PRS.[3SG]</ta>
            <ta e="T1177" id="Seg_3177" s="T1176">что.[NOM.SG]</ta>
            <ta e="T1178" id="Seg_3178" s="T1177">плакать-DUR-2SG</ta>
            <ta e="T1179" id="Seg_3179" s="T1178">и</ta>
            <ta e="T1180" id="Seg_3180" s="T1179">я.GEN</ta>
            <ta e="T1181" id="Seg_3181" s="T1180">дом-ACC</ta>
            <ta e="T1182" id="Seg_3182" s="T1181">лисица.[NOM.SG]</ta>
            <ta e="T1183" id="Seg_3183" s="T1182">взять-PST.[3SG]</ta>
            <ta e="T1184" id="Seg_3184" s="T1183">а</ta>
            <ta e="T1185" id="Seg_3185" s="T1184">этот-GEN</ta>
            <ta e="T1187" id="Seg_3186" s="T1186">быть-PST.[3SG]</ta>
            <ta e="T1188" id="Seg_3187" s="T1187">снег-ABL</ta>
            <ta e="T1189" id="Seg_3188" s="T1188">и</ta>
            <ta e="T1190" id="Seg_3189" s="T1189">умереть-RES-PST.[3SG]</ta>
            <ta e="T1191" id="Seg_3190" s="T1190">а</ta>
            <ta e="T1192" id="Seg_3191" s="T1191">я.GEN</ta>
            <ta e="T1193" id="Seg_3192" s="T1192">всегда</ta>
            <ta e="T1194" id="Seg_3193" s="T1193">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T1195" id="Seg_3194" s="T1194">хороший.[NOM.SG]</ta>
            <ta e="T1196" id="Seg_3195" s="T1195">дерево-ABL</ta>
            <ta e="T1197" id="Seg_3196" s="T1196">тогда</ta>
            <ta e="T1198" id="Seg_3197" s="T1197">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1199" id="Seg_3198" s="T1198">я.NOM</ta>
            <ta e="T1200" id="Seg_3199" s="T1199">этот-ACC</ta>
            <ta e="T1201" id="Seg_3200" s="T1200">гнать-FUT-1SG</ta>
            <ta e="T1202" id="Seg_3201" s="T1201">нет</ta>
            <ta e="T1203" id="Seg_3202" s="T1202">NEG</ta>
            <ta e="T1204" id="Seg_3203" s="T1203">гнать-FUT-2SG</ta>
            <ta e="T1205" id="Seg_3204" s="T1204">медведь.[NOM.SG]</ta>
            <ta e="T1206" id="Seg_3205" s="T1205">NEG</ta>
            <ta e="T1207" id="Seg_3206" s="T1206">мочь-PST.[3SG]</ta>
            <ta e="T1208" id="Seg_3207" s="T1207">гнать-INF.LAT</ta>
            <ta e="T1209" id="Seg_3208" s="T1208">собака.[NOM.SG]</ta>
            <ta e="T1210" id="Seg_3209" s="T1209">NEG</ta>
            <ta e="T1211" id="Seg_3210" s="T1210">мочь-PST.[3SG]</ta>
            <ta e="T1212" id="Seg_3211" s="T1211">бык.[NOM.SG]</ta>
            <ta e="T1213" id="Seg_3212" s="T1212">а</ta>
            <ta e="T1214" id="Seg_3213" s="T1213">ты.NOM</ta>
            <ta e="T1216" id="Seg_3214" s="T1215">тогда</ta>
            <ta e="T1218" id="Seg_3215" s="T1217">прийти-PST-3PL</ta>
            <ta e="T1219" id="Seg_3216" s="T1218">прийти-PST-3PL</ta>
            <ta e="T1221" id="Seg_3217" s="T1220">бык.[NOM.SG]</ta>
            <ta e="T1222" id="Seg_3218" s="T1221">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1225" id="Seg_3219" s="T1224">пойти-EP-IMP.2SG</ta>
            <ta e="T1226" id="Seg_3220" s="T1225">дом-ABL</ta>
            <ta e="T1227" id="Seg_3221" s="T1226">а</ta>
            <ta e="T1228" id="Seg_3222" s="T1227">этот.[NOM.SG]</ta>
            <ta e="T1229" id="Seg_3223" s="T1228">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1230" id="Seg_3224" s="T1229">скоро</ta>
            <ta e="T1231" id="Seg_3225" s="T1230">прыгнуть-MOM-FUT-1SG</ta>
            <ta e="T1232" id="Seg_3226" s="T1231">так</ta>
            <ta e="T1233" id="Seg_3227" s="T1232">PTCL</ta>
            <ta e="T1234" id="Seg_3228" s="T1233">ты.ACC</ta>
            <ta e="T1235" id="Seg_3229" s="T1234">от</ta>
            <ta e="T1236" id="Seg_3230" s="T1235">%оторвать-MOM-FUT-1SG</ta>
            <ta e="T1237" id="Seg_3231" s="T1236">тогда</ta>
            <ta e="T1238" id="Seg_3232" s="T1237">бык.[NOM.SG]</ta>
            <ta e="T1239" id="Seg_3233" s="T1238">бык.[NOM.SG]</ta>
            <ta e="T1240" id="Seg_3234" s="T1239">пугать-MOM-PST.[3SG]</ta>
            <ta e="T1241" id="Seg_3235" s="T1240">и</ta>
            <ta e="T1242" id="Seg_3236" s="T1241">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1243" id="Seg_3237" s="T1242">тогда</ta>
            <ta e="T1244" id="Seg_3238" s="T1243">опять</ta>
            <ta e="T1245" id="Seg_3239" s="T1244">идти-PRS.[3SG]</ta>
            <ta e="T1246" id="Seg_3240" s="T1245">заяц.[NOM.SG]</ta>
            <ta e="T1247" id="Seg_3241" s="T1246">и</ta>
            <ta e="T1248" id="Seg_3242" s="T1247">плакать-DUR.[3SG]</ta>
            <ta e="T1249" id="Seg_3243" s="T1248">тогда</ta>
            <ta e="T1250" id="Seg_3244" s="T1249">курица-GEN</ta>
            <ta e="T1251" id="Seg_3245" s="T1250">мужчина.[NOM.SG]</ta>
            <ta e="T1252" id="Seg_3246" s="T1251">прийти-PRS.[3SG]</ta>
            <ta e="T1253" id="Seg_3247" s="T1252">коса-INS</ta>
            <ta e="T1254" id="Seg_3248" s="T1253">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1255" id="Seg_3249" s="T1254">что.[NOM.SG]</ta>
            <ta e="T1256" id="Seg_3250" s="T1255">плакать-DUR-2SG</ta>
            <ta e="T1257" id="Seg_3251" s="T1256">и</ta>
            <ta e="T1258" id="Seg_3252" s="T1257">я.GEN</ta>
            <ta e="T1259" id="Seg_3253" s="T1258">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T1260" id="Seg_3254" s="T1259">дерево-ABL</ta>
            <ta e="T1261" id="Seg_3255" s="T1260">лисица.[NOM.SG]</ta>
            <ta e="T1262" id="Seg_3256" s="T1261">прийти-PST.[3SG]</ta>
            <ta e="T1263" id="Seg_3257" s="T1262">спать-INF.LAT</ta>
            <ta e="T1264" id="Seg_3258" s="T1263">и</ta>
            <ta e="T1265" id="Seg_3259" s="T1264">я.ACC</ta>
            <ta e="T1266" id="Seg_3260" s="T1265">гнать-PST.[3SG]</ta>
            <ta e="T1267" id="Seg_3261" s="T1266">а</ta>
            <ta e="T1268" id="Seg_3262" s="T1267">этот-GEN</ta>
            <ta e="T1269" id="Seg_3263" s="T1268">снег-ABL</ta>
            <ta e="T1270" id="Seg_3264" s="T1269">быть-PST.[3SG]</ta>
            <ta e="T1271" id="Seg_3265" s="T1270">и</ta>
            <ta e="T1272" id="Seg_3266" s="T1271">сейчас</ta>
            <ta e="T1273" id="Seg_3267" s="T1272">NEG.EX</ta>
            <ta e="T1274" id="Seg_3268" s="T1273">тогда</ta>
            <ta e="T1275" id="Seg_3269" s="T1274">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1276" id="Seg_3270" s="T1275">я.NOM</ta>
            <ta e="T1277" id="Seg_3271" s="T1276">гнать-FUT-1SG</ta>
            <ta e="T1278" id="Seg_3272" s="T1277">собака.[NOM.SG]</ta>
            <ta e="T1279" id="Seg_3273" s="T1278">NEG</ta>
            <ta e="T1280" id="Seg_3274" s="T1279">мочь-PST.[3SG]</ta>
            <ta e="T1281" id="Seg_3275" s="T1280">гнать-INF.LAT</ta>
            <ta e="T1282" id="Seg_3276" s="T1281">этот.[NOM.SG]</ta>
            <ta e="T1283" id="Seg_3277" s="T1282">медведь.[NOM.SG]</ta>
            <ta e="T1284" id="Seg_3278" s="T1283">и</ta>
            <ta e="T1285" id="Seg_3279" s="T1284">бык.[NOM.SG]</ta>
            <ta e="T1286" id="Seg_3280" s="T1285">NEG</ta>
            <ta e="T1287" id="Seg_3281" s="T1286">мочь-PST.[3SG]</ta>
            <ta e="T1288" id="Seg_3282" s="T1287">а</ta>
            <ta e="T1289" id="Seg_3283" s="T1288">ты.NOM</ta>
            <ta e="T1290" id="Seg_3284" s="T1289">NEG</ta>
            <ta e="T1291" id="Seg_3285" s="T1290">гнать-FUT-2SG</ta>
            <ta e="T1292" id="Seg_3286" s="T1291">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1293" id="Seg_3287" s="T1292">тогда</ta>
            <ta e="T1294" id="Seg_3288" s="T1293">прийти-PST.[3SG]</ta>
            <ta e="T1295" id="Seg_3289" s="T1294">PTCL</ta>
            <ta e="T1296" id="Seg_3290" s="T1295">кричать-DUR.[3SG]</ta>
            <ta e="T1297" id="Seg_3291" s="T1296">встать-IMP.2SG</ta>
            <ta e="T1298" id="Seg_3292" s="T1297">печь-ABL</ta>
            <ta e="T1301" id="Seg_3293" s="T1300">пойти-EP-IMP.2SG</ta>
            <ta e="T1302" id="Seg_3294" s="T1301">отсюда</ta>
            <ta e="T1304" id="Seg_3295" s="T1302">а.то</ta>
            <ta e="T1305" id="Seg_3296" s="T1304">скоро</ta>
            <ta e="T1306" id="Seg_3297" s="T1305">коса-INS</ta>
            <ta e="T1307" id="Seg_3298" s="T1306">ты.ACC</ta>
            <ta e="T1308" id="Seg_3299" s="T1307">убить-FUT-1SG</ta>
            <ta e="T1309" id="Seg_3300" s="T1308">PTCL</ta>
            <ta e="T1310" id="Seg_3301" s="T1309">зарезать-FUT-1SG</ta>
            <ta e="T1311" id="Seg_3302" s="T1310">ты.ACC</ta>
            <ta e="T1312" id="Seg_3303" s="T1311">скоро</ta>
            <ta e="T1313" id="Seg_3304" s="T1312">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T1314" id="Seg_3305" s="T1313">нога-PL</ta>
            <ta e="T1315" id="Seg_3306" s="T1314">надеть-PRS-1SG</ta>
            <ta e="T1316" id="Seg_3307" s="T1315">сапог-PL</ta>
            <ta e="T1317" id="Seg_3308" s="T1316">тогда</ta>
            <ta e="T1318" id="Seg_3309" s="T1317">опять</ta>
            <ta e="T1319" id="Seg_3310" s="T1318">кричать-DUR.[3SG]</ta>
            <ta e="T1320" id="Seg_3311" s="T1319">пойти-EP-IMP.2SG</ta>
            <ta e="T1321" id="Seg_3312" s="T1320">печь-ABL</ta>
            <ta e="T1322" id="Seg_3313" s="T1321">а.то</ta>
            <ta e="T1323" id="Seg_3314" s="T1322">скоро</ta>
            <ta e="T1324" id="Seg_3315" s="T1323">ты.ACC</ta>
            <ta e="T1325" id="Seg_3316" s="T1324">убить-FUT-1SG</ta>
            <ta e="T1326" id="Seg_3317" s="T1325">нож.[NOM.SG]</ta>
            <ta e="T1327" id="Seg_3318" s="T1326">коса-INS</ta>
            <ta e="T1328" id="Seg_3319" s="T1327">PTCL</ta>
            <ta e="T1329" id="Seg_3320" s="T1328">резать-FUT-1SG</ta>
            <ta e="T1330" id="Seg_3321" s="T1329">скоро</ta>
            <ta e="T1331" id="Seg_3322" s="T1330">PTCL</ta>
            <ta e="T1332" id="Seg_3323" s="T1331">одежда-ACC</ta>
            <ta e="T1333" id="Seg_3324" s="T1332">надеть-PRS-1SG</ta>
            <ta e="T1334" id="Seg_3325" s="T1333">тогда</ta>
            <ta e="T1335" id="Seg_3326" s="T1334">этот.[NOM.SG]</ta>
            <ta e="T1336" id="Seg_3327" s="T1335">опять</ta>
            <ta e="T1337" id="Seg_3328" s="T1336">кричать-DUR.[3SG]</ta>
            <ta e="T1338" id="Seg_3329" s="T1337">пойти-EP-IMP.2SG</ta>
            <ta e="T1339" id="Seg_3330" s="T1338">скоро</ta>
            <ta e="T1340" id="Seg_3331" s="T1339">бежать-DUR-1SG</ta>
            <ta e="T1341" id="Seg_3332" s="T1340">тогда</ta>
            <ta e="T1342" id="Seg_3333" s="T1341">этот.[NOM.SG]</ta>
            <ta e="T1343" id="Seg_3334" s="T1342">коса-INS</ta>
            <ta e="T1344" id="Seg_3335" s="T1343">этот-ACC</ta>
            <ta e="T1345" id="Seg_3336" s="T1344">PTCL</ta>
            <ta e="T1346" id="Seg_3337" s="T1345">зарезать-MOM-PST.[3SG]</ta>
            <ta e="T1347" id="Seg_3338" s="T1346">и</ta>
            <ta e="T1348" id="Seg_3339" s="T1347">этот.[NOM.SG]</ta>
            <ta e="T1349" id="Seg_3340" s="T1348">умереть-RES-PST.[3SG]</ta>
            <ta e="T1350" id="Seg_3341" s="T1349">тогда</ta>
            <ta e="T1351" id="Seg_3342" s="T1350">жить-PST-3PL</ta>
            <ta e="T1352" id="Seg_3343" s="T1351">курица-GEN</ta>
            <ta e="T1353" id="Seg_3344" s="T1352">мужчина.[NOM.SG]</ta>
            <ta e="T1354" id="Seg_3345" s="T1353">и</ta>
            <ta e="T1355" id="Seg_3346" s="T1354">заяц.[NOM.SG]</ta>
            <ta e="T1356" id="Seg_3347" s="T1355">дом-LOC</ta>
            <ta e="T1357" id="Seg_3348" s="T1356">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1032" id="Seg_3349" s="T1031">v-v:tense-v:pn</ta>
            <ta e="T1033" id="Seg_3350" s="T1032">n-n:case</ta>
            <ta e="T1034" id="Seg_3351" s="T1033">conj</ta>
            <ta e="T1035" id="Seg_3352" s="T1034">n-n:case</ta>
            <ta e="T1036" id="Seg_3353" s="T1035">n-n:case</ta>
            <ta e="T1037" id="Seg_3354" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1038" id="Seg_3355" s="T1037">n-n:case.poss</ta>
            <ta e="T1039" id="Seg_3356" s="T1038">n-n:case</ta>
            <ta e="T1040" id="Seg_3357" s="T1039">conj</ta>
            <ta e="T1041" id="Seg_3358" s="T1040">n-n:case</ta>
            <ta e="T1042" id="Seg_3359" s="T1041">v-v:tense-v:pn</ta>
            <ta e="T1044" id="Seg_3360" s="T1043">n-n:case</ta>
            <ta e="T1045" id="Seg_3361" s="T1044">adv</ta>
            <ta e="T1046" id="Seg_3362" s="T1045">v-v:tense-v:pn</ta>
            <ta e="T1047" id="Seg_3363" s="T1046">adj-n:case</ta>
            <ta e="T1048" id="Seg_3364" s="T1047">n-n:case</ta>
            <ta e="T1049" id="Seg_3365" s="T1048">n-n:case.poss</ta>
            <ta e="T1050" id="Seg_3366" s="T1049">ptcl</ta>
            <ta e="T1051" id="Seg_3367" s="T1050">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1052" id="Seg_3368" s="T1051">adv</ta>
            <ta e="T1053" id="Seg_3369" s="T1052">dempro-n:case</ta>
            <ta e="T1054" id="Seg_3370" s="T1053">v-v:tense-v:pn</ta>
            <ta e="T1055" id="Seg_3371" s="T1054">n-n:case</ta>
            <ta e="T1056" id="Seg_3372" s="T1055">v-v:mood.pn</ta>
            <ta e="T1057" id="Seg_3373" s="T1056">pers</ta>
            <ta e="T1058" id="Seg_3374" s="T1057">v-v:n.fin</ta>
            <ta e="T1059" id="Seg_3375" s="T1058">dempro-n:case</ta>
            <ta e="T1060" id="Seg_3376" s="T1059">refl-n:case.poss</ta>
            <ta e="T1061" id="Seg_3377" s="T1060">dempro-n:case</ta>
            <ta e="T1062" id="Seg_3378" s="T1061">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1063" id="Seg_3379" s="T1062">n-n:case</ta>
            <ta e="T1064" id="Seg_3380" s="T1063">dempro-n:case</ta>
            <ta e="T1065" id="Seg_3381" s="T1064">v-v:tense-v:pn</ta>
            <ta e="T1066" id="Seg_3382" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_3383" s="T1066">v-v&gt;v-v:pn</ta>
            <ta e="T1068" id="Seg_3384" s="T1067">pers</ta>
            <ta e="T1069" id="Seg_3385" s="T1068">n-n:case</ta>
            <ta e="T1070" id="Seg_3386" s="T1069">v-v&gt;v-v:pn</ta>
            <ta e="T1071" id="Seg_3387" s="T1070">que-n:case</ta>
            <ta e="T1072" id="Seg_3388" s="T1071">pers</ta>
            <ta e="T1073" id="Seg_3389" s="T1072">v-v&gt;v-v:pn</ta>
            <ta e="T1074" id="Seg_3390" s="T1073">conj</ta>
            <ta e="T1075" id="Seg_3391" s="T1074">pers</ta>
            <ta e="T1076" id="Seg_3392" s="T1075">n-n:case</ta>
            <ta e="T1077" id="Seg_3393" s="T1076">v-v&gt;v-v:pn</ta>
            <ta e="T1078" id="Seg_3394" s="T1077">v-v:tense-v:pn</ta>
            <ta e="T1079" id="Seg_3395" s="T1078">n-n:case</ta>
            <ta e="T1080" id="Seg_3396" s="T1079">v-v:mood-v:pn</ta>
            <ta e="T1081" id="Seg_3397" s="T1080">pers</ta>
            <ta e="T1082" id="Seg_3398" s="T1081">dempro-n:case</ta>
            <ta e="T1083" id="Seg_3399" s="T1082">v-v:tense-v:pn</ta>
            <ta e="T1084" id="Seg_3400" s="T1083">v-v:tense-v:pn</ta>
            <ta e="T1085" id="Seg_3401" s="T1084">n-n:case</ta>
            <ta e="T1086" id="Seg_3402" s="T1085">conj</ta>
            <ta e="T1087" id="Seg_3403" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_3404" s="T1087">v-v&gt;v-v:pn</ta>
            <ta e="T1089" id="Seg_3405" s="T1088">v-v:ins-v:mood.pn</ta>
            <ta e="T1090" id="Seg_3406" s="T1089">adv</ta>
            <ta e="T1091" id="Seg_3407" s="T1090">conj</ta>
            <ta e="T1092" id="Seg_3408" s="T1091">dempro-n:case</ta>
            <ta e="T1093" id="Seg_3409" s="T1092">v-v&gt;v-v:pn</ta>
            <ta e="T1094" id="Seg_3410" s="T1093">adv</ta>
            <ta e="T1095" id="Seg_3411" s="T1094">pers</ta>
            <ta e="T1096" id="Seg_3412" s="T1095">ptcl</ta>
            <ta e="T1097" id="Seg_3413" s="T1096">n-n:case.poss</ta>
            <ta e="T1098" id="Seg_3414" s="T1097">v-v:tense-v:pn</ta>
            <ta e="T1099" id="Seg_3415" s="T1098">conj</ta>
            <ta e="T1100" id="Seg_3416" s="T1099">pers</ta>
            <ta e="T1101" id="Seg_3417" s="T1100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1102" id="Seg_3418" s="T1101">dempro-n:case</ta>
            <ta e="T1103" id="Seg_3419" s="T1102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1104" id="Seg_3420" s="T1103">conj</ta>
            <ta e="T1105" id="Seg_3421" s="T1104">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1106" id="Seg_3422" s="T1105">adv</ta>
            <ta e="T1107" id="Seg_3423" s="T1106">v-v:tense-v:pn</ta>
            <ta e="T1108" id="Seg_3424" s="T1107">n-n:case</ta>
            <ta e="T1109" id="Seg_3425" s="T1108">conj</ta>
            <ta e="T1110" id="Seg_3426" s="T1109">v-v&gt;v-v:pn</ta>
            <ta e="T1111" id="Seg_3427" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_3428" s="T1111">v-v:tense-v:pn</ta>
            <ta e="T1113" id="Seg_3429" s="T1112">n-n:case</ta>
            <ta e="T1114" id="Seg_3430" s="T1113">que-n:case</ta>
            <ta e="T1115" id="Seg_3431" s="T1114">v-v&gt;v-v:pn</ta>
            <ta e="T1116" id="Seg_3432" s="T1115">conj</ta>
            <ta e="T1117" id="Seg_3433" s="T1116">ptcl</ta>
            <ta e="T1118" id="Seg_3434" s="T1117">pers</ta>
            <ta e="T1119" id="Seg_3435" s="T1118">ptcl</ta>
            <ta e="T1120" id="Seg_3436" s="T1119">v-v:n.fin</ta>
            <ta e="T1121" id="Seg_3437" s="T1120">pers</ta>
            <ta e="T1122" id="Seg_3438" s="T1121">n-n:case.poss</ta>
            <ta e="T1123" id="Seg_3439" s="T1122">v-v:tense-v:pn</ta>
            <ta e="T1124" id="Seg_3440" s="T1123">n-n:case</ta>
            <ta e="T1125" id="Seg_3441" s="T1124">conj</ta>
            <ta e="T1126" id="Seg_3442" s="T1125">n-n:case</ta>
            <ta e="T1127" id="Seg_3443" s="T1126">n-n:case.poss</ta>
            <ta e="T1129" id="Seg_3444" s="T1128">n-n:case</ta>
            <ta e="T1130" id="Seg_3445" s="T1129">adv</ta>
            <ta e="T1131" id="Seg_3446" s="T1130">dempro-n:case</ta>
            <ta e="T1132" id="Seg_3447" s="T1131">v-v:tense-v:pn</ta>
            <ta e="T1133" id="Seg_3448" s="T1132">pers</ta>
            <ta e="T1134" id="Seg_3449" s="T1133">v-v:n.fin</ta>
            <ta e="T1135" id="Seg_3450" s="T1134">conj</ta>
            <ta e="T1136" id="Seg_3451" s="T1135">pers</ta>
            <ta e="T1137" id="Seg_3452" s="T1136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1138" id="Seg_3453" s="T1137">v-v:mood-v:pn</ta>
            <ta e="T1139" id="Seg_3454" s="T1138">pers</ta>
            <ta e="T1140" id="Seg_3455" s="T1139">dempro-n:case</ta>
            <ta e="T1141" id="Seg_3456" s="T1140">v-v:tense-v:pn</ta>
            <ta e="T1142" id="Seg_3457" s="T1141">v-v:tense-v:pn</ta>
            <ta e="T1143" id="Seg_3458" s="T1142">v-v:ins-v:mood.pn</ta>
            <ta e="T1144" id="Seg_3459" s="T1143">adv</ta>
            <ta e="T1145" id="Seg_3460" s="T1144">ptcl</ta>
            <ta e="T1146" id="Seg_3461" s="T1145">adv</ta>
            <ta e="T1147" id="Seg_3462" s="T1146">pers</ta>
            <ta e="T1148" id="Seg_3463" s="T1147">v-v:tense-v:pn</ta>
            <ta e="T1149" id="Seg_3464" s="T1148">conj</ta>
            <ta e="T1150" id="Seg_3465" s="T1149">dempro-n:case</ta>
            <ta e="T1151" id="Seg_3466" s="T1150">v-v&gt;v-v:pn</ta>
            <ta e="T1152" id="Seg_3467" s="T1151">pers</ta>
            <ta e="T1153" id="Seg_3468" s="T1152">ptcl</ta>
            <ta e="T1154" id="Seg_3469" s="T1153">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1155" id="Seg_3470" s="T1154">conj</ta>
            <ta e="T1156" id="Seg_3471" s="T1155">ptcl</ta>
            <ta e="T1157" id="Seg_3472" s="T1156">n-n:case.poss</ta>
            <ta e="T1158" id="Seg_3473" s="T1157">pers</ta>
            <ta e="T1159" id="Seg_3474" s="T1158">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1160" id="Seg_3475" s="T1159">conj</ta>
            <ta e="T1161" id="Seg_3476" s="T1160">pers</ta>
            <ta e="T1162" id="Seg_3477" s="T1161">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1163" id="Seg_3478" s="T1162">adv</ta>
            <ta e="T1164" id="Seg_3479" s="T1163">n-n:case</ta>
            <ta e="T1166" id="Seg_3480" s="T1165">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1167" id="Seg_3481" s="T1166">conj</ta>
            <ta e="T1168" id="Seg_3482" s="T1167">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1169" id="Seg_3483" s="T1168">adv</ta>
            <ta e="T1170" id="Seg_3484" s="T1169">v-v:tense-v:pn</ta>
            <ta e="T1171" id="Seg_3485" s="T1170">n.[n:case]</ta>
            <ta e="T1172" id="Seg_3486" s="T1171">conj</ta>
            <ta e="T1173" id="Seg_3487" s="T1172">v-v&gt;v-v:pn</ta>
            <ta e="T1174" id="Seg_3488" s="T1173">adv</ta>
            <ta e="T1175" id="Seg_3489" s="T1174">n-n:case</ta>
            <ta e="T1176" id="Seg_3490" s="T1175">v-v:tense-v:pn</ta>
            <ta e="T1177" id="Seg_3491" s="T1176">que-n:case</ta>
            <ta e="T1178" id="Seg_3492" s="T1177">v-v&gt;v-v:pn</ta>
            <ta e="T1179" id="Seg_3493" s="T1178">conj</ta>
            <ta e="T1180" id="Seg_3494" s="T1179">pers</ta>
            <ta e="T1181" id="Seg_3495" s="T1180">n-n:case</ta>
            <ta e="T1182" id="Seg_3496" s="T1181">n-n:case</ta>
            <ta e="T1183" id="Seg_3497" s="T1182">v-v:tense-v:pn</ta>
            <ta e="T1184" id="Seg_3498" s="T1183">conj</ta>
            <ta e="T1185" id="Seg_3499" s="T1184">dempro-n:case</ta>
            <ta e="T1187" id="Seg_3500" s="T1186">v-v:tense-v:pn</ta>
            <ta e="T1188" id="Seg_3501" s="T1187">n-n:case</ta>
            <ta e="T1189" id="Seg_3502" s="T1188">conj</ta>
            <ta e="T1190" id="Seg_3503" s="T1189">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1191" id="Seg_3504" s="T1190">conj</ta>
            <ta e="T1192" id="Seg_3505" s="T1191">pers</ta>
            <ta e="T1193" id="Seg_3506" s="T1192">adv</ta>
            <ta e="T1194" id="Seg_3507" s="T1193">n-n:case.poss</ta>
            <ta e="T1195" id="Seg_3508" s="T1194">adj-n:case</ta>
            <ta e="T1196" id="Seg_3509" s="T1195">n-n:case</ta>
            <ta e="T1197" id="Seg_3510" s="T1196">adv</ta>
            <ta e="T1198" id="Seg_3511" s="T1197">v-v:mood-v:pn</ta>
            <ta e="T1199" id="Seg_3512" s="T1198">pers</ta>
            <ta e="T1200" id="Seg_3513" s="T1199">dempro-n:case</ta>
            <ta e="T1201" id="Seg_3514" s="T1200">v-v:tense-v:pn</ta>
            <ta e="T1202" id="Seg_3515" s="T1201">ptcl</ta>
            <ta e="T1203" id="Seg_3516" s="T1202">ptcl</ta>
            <ta e="T1204" id="Seg_3517" s="T1203">v-v:tense-v:pn</ta>
            <ta e="T1205" id="Seg_3518" s="T1204">n-n:case</ta>
            <ta e="T1206" id="Seg_3519" s="T1205">ptcl</ta>
            <ta e="T1207" id="Seg_3520" s="T1206">v-v:tense-v:pn</ta>
            <ta e="T1208" id="Seg_3521" s="T1207">v-v:n.fin</ta>
            <ta e="T1209" id="Seg_3522" s="T1208">n-n:case</ta>
            <ta e="T1210" id="Seg_3523" s="T1209">ptcl</ta>
            <ta e="T1211" id="Seg_3524" s="T1210">v-v:tense-v:pn</ta>
            <ta e="T1212" id="Seg_3525" s="T1211">n-n:case</ta>
            <ta e="T1213" id="Seg_3526" s="T1212">conj</ta>
            <ta e="T1214" id="Seg_3527" s="T1213">pers</ta>
            <ta e="T1216" id="Seg_3528" s="T1215">adv</ta>
            <ta e="T1218" id="Seg_3529" s="T1217">v-v:tense-v:pn</ta>
            <ta e="T1219" id="Seg_3530" s="T1218">v-v:tense-v:pn</ta>
            <ta e="T1221" id="Seg_3531" s="T1220">n-n:case</ta>
            <ta e="T1222" id="Seg_3532" s="T1221">v-v&gt;v-v:pn</ta>
            <ta e="T1225" id="Seg_3533" s="T1224">v-v:ins-v:mood.pn</ta>
            <ta e="T1226" id="Seg_3534" s="T1225">n-n:case</ta>
            <ta e="T1227" id="Seg_3535" s="T1226">conj</ta>
            <ta e="T1228" id="Seg_3536" s="T1227">dempro-n:case</ta>
            <ta e="T1229" id="Seg_3537" s="T1228">v-v&gt;v-v:pn</ta>
            <ta e="T1230" id="Seg_3538" s="T1229">adv</ta>
            <ta e="T1231" id="Seg_3539" s="T1230">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1232" id="Seg_3540" s="T1231">ptcl</ta>
            <ta e="T1233" id="Seg_3541" s="T1232">ptcl</ta>
            <ta e="T1234" id="Seg_3542" s="T1233">pers</ta>
            <ta e="T1235" id="Seg_3543" s="T1234">adv</ta>
            <ta e="T1236" id="Seg_3544" s="T1235">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1237" id="Seg_3545" s="T1236">adv</ta>
            <ta e="T1238" id="Seg_3546" s="T1237">n-n:case</ta>
            <ta e="T1239" id="Seg_3547" s="T1238">n-n:case</ta>
            <ta e="T1240" id="Seg_3548" s="T1239">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1241" id="Seg_3549" s="T1240">conj</ta>
            <ta e="T1242" id="Seg_3550" s="T1241">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1243" id="Seg_3551" s="T1242">adv</ta>
            <ta e="T1244" id="Seg_3552" s="T1243">adv</ta>
            <ta e="T1245" id="Seg_3553" s="T1244">v-v:tense-v:pn</ta>
            <ta e="T1246" id="Seg_3554" s="T1245">n-n:case</ta>
            <ta e="T1247" id="Seg_3555" s="T1246">conj</ta>
            <ta e="T1248" id="Seg_3556" s="T1247">v-v&gt;v-v:pn</ta>
            <ta e="T1249" id="Seg_3557" s="T1248">adv</ta>
            <ta e="T1250" id="Seg_3558" s="T1249">n-n:case</ta>
            <ta e="T1251" id="Seg_3559" s="T1250">n-n:case</ta>
            <ta e="T1252" id="Seg_3560" s="T1251">v-v:tense-v:pn</ta>
            <ta e="T1253" id="Seg_3561" s="T1252">n-n:case</ta>
            <ta e="T1254" id="Seg_3562" s="T1253">v-v&gt;v-v:pn</ta>
            <ta e="T1255" id="Seg_3563" s="T1254">que-n:case</ta>
            <ta e="T1256" id="Seg_3564" s="T1255">v-v&gt;v-v:pn</ta>
            <ta e="T1257" id="Seg_3565" s="T1256">conj</ta>
            <ta e="T1258" id="Seg_3566" s="T1257">pers</ta>
            <ta e="T1259" id="Seg_3567" s="T1258">n-n:case.poss</ta>
            <ta e="T1260" id="Seg_3568" s="T1259">n-n:case</ta>
            <ta e="T1261" id="Seg_3569" s="T1260">n-n:case</ta>
            <ta e="T1262" id="Seg_3570" s="T1261">v-v:tense-v:pn</ta>
            <ta e="T1263" id="Seg_3571" s="T1262">v-v:n.fin</ta>
            <ta e="T1264" id="Seg_3572" s="T1263">conj</ta>
            <ta e="T1265" id="Seg_3573" s="T1264">pers</ta>
            <ta e="T1266" id="Seg_3574" s="T1265">v-v:tense-v:pn</ta>
            <ta e="T1267" id="Seg_3575" s="T1266">conj</ta>
            <ta e="T1268" id="Seg_3576" s="T1267">dempro-n:case</ta>
            <ta e="T1269" id="Seg_3577" s="T1268">n-n:case</ta>
            <ta e="T1270" id="Seg_3578" s="T1269">v-v:tense-v:pn</ta>
            <ta e="T1271" id="Seg_3579" s="T1270">conj</ta>
            <ta e="T1272" id="Seg_3580" s="T1271">adv</ta>
            <ta e="T1273" id="Seg_3581" s="T1272">v</ta>
            <ta e="T1274" id="Seg_3582" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_3583" s="T1274">v-v:mood-v:pn</ta>
            <ta e="T1276" id="Seg_3584" s="T1275">pers</ta>
            <ta e="T1277" id="Seg_3585" s="T1276">v-v:tense-v:pn</ta>
            <ta e="T1278" id="Seg_3586" s="T1277">n-n:case</ta>
            <ta e="T1279" id="Seg_3587" s="T1278">ptcl</ta>
            <ta e="T1280" id="Seg_3588" s="T1279">v-v:tense-v:pn</ta>
            <ta e="T1281" id="Seg_3589" s="T1280">v-v:n.fin</ta>
            <ta e="T1282" id="Seg_3590" s="T1281">dempro-n:case</ta>
            <ta e="T1283" id="Seg_3591" s="T1282">n-n:case</ta>
            <ta e="T1284" id="Seg_3592" s="T1283">conj</ta>
            <ta e="T1285" id="Seg_3593" s="T1284">n-n:case</ta>
            <ta e="T1286" id="Seg_3594" s="T1285">ptcl</ta>
            <ta e="T1287" id="Seg_3595" s="T1286">v-v:tense-v:pn</ta>
            <ta e="T1288" id="Seg_3596" s="T1287">conj</ta>
            <ta e="T1289" id="Seg_3597" s="T1288">pers</ta>
            <ta e="T1290" id="Seg_3598" s="T1289">ptcl</ta>
            <ta e="T1291" id="Seg_3599" s="T1290">v-v:tense-v:pn</ta>
            <ta e="T1292" id="Seg_3600" s="T1291">v-v:mood-v:pn</ta>
            <ta e="T1293" id="Seg_3601" s="T1292">adv</ta>
            <ta e="T1294" id="Seg_3602" s="T1293">v-v:tense-v:pn</ta>
            <ta e="T1295" id="Seg_3603" s="T1294">ptcl</ta>
            <ta e="T1296" id="Seg_3604" s="T1295">v-v&gt;v-v:pn</ta>
            <ta e="T1297" id="Seg_3605" s="T1296">v-v:mood.pn</ta>
            <ta e="T1298" id="Seg_3606" s="T1297">n-n:case</ta>
            <ta e="T1301" id="Seg_3607" s="T1300">v-v:ins-v:mood.pn</ta>
            <ta e="T1302" id="Seg_3608" s="T1301">adv</ta>
            <ta e="T1304" id="Seg_3609" s="T1302">ptcl</ta>
            <ta e="T1305" id="Seg_3610" s="T1304">adv</ta>
            <ta e="T1306" id="Seg_3611" s="T1305">n-n:case</ta>
            <ta e="T1307" id="Seg_3612" s="T1306">pers</ta>
            <ta e="T1308" id="Seg_3613" s="T1307">v-v:tense-v:pn</ta>
            <ta e="T1309" id="Seg_3614" s="T1308">ptcl</ta>
            <ta e="T1310" id="Seg_3615" s="T1309">v-v:tense-v:pn</ta>
            <ta e="T1311" id="Seg_3616" s="T1310">pers</ta>
            <ta e="T1312" id="Seg_3617" s="T1311">adv</ta>
            <ta e="T1313" id="Seg_3618" s="T1312">n-n:case.poss</ta>
            <ta e="T1314" id="Seg_3619" s="T1313">n-n:num</ta>
            <ta e="T1315" id="Seg_3620" s="T1314">v-v:tense-v:pn</ta>
            <ta e="T1316" id="Seg_3621" s="T1315">n-n:num</ta>
            <ta e="T1317" id="Seg_3622" s="T1316">adv</ta>
            <ta e="T1318" id="Seg_3623" s="T1317">adv</ta>
            <ta e="T1319" id="Seg_3624" s="T1318">v-v&gt;v-v:pn</ta>
            <ta e="T1320" id="Seg_3625" s="T1319">v-v:ins-v:mood.pn</ta>
            <ta e="T1321" id="Seg_3626" s="T1320">n-n:case</ta>
            <ta e="T1322" id="Seg_3627" s="T1321">ptcl</ta>
            <ta e="T1323" id="Seg_3628" s="T1322">adv</ta>
            <ta e="T1324" id="Seg_3629" s="T1323">pers</ta>
            <ta e="T1325" id="Seg_3630" s="T1324">v-v:tense-v:pn</ta>
            <ta e="T1326" id="Seg_3631" s="T1325">n-n:case</ta>
            <ta e="T1327" id="Seg_3632" s="T1326">n-n:case</ta>
            <ta e="T1328" id="Seg_3633" s="T1327">ptcl</ta>
            <ta e="T1329" id="Seg_3634" s="T1328">v-v:tense-v:pn</ta>
            <ta e="T1330" id="Seg_3635" s="T1329">adv</ta>
            <ta e="T1331" id="Seg_3636" s="T1330">ptcl</ta>
            <ta e="T1332" id="Seg_3637" s="T1331">n-n:case</ta>
            <ta e="T1333" id="Seg_3638" s="T1332">v-v:tense-v:pn</ta>
            <ta e="T1334" id="Seg_3639" s="T1333">adv</ta>
            <ta e="T1335" id="Seg_3640" s="T1334">dempro-n:case</ta>
            <ta e="T1336" id="Seg_3641" s="T1335">adv</ta>
            <ta e="T1337" id="Seg_3642" s="T1336">v-v&gt;v-v:pn</ta>
            <ta e="T1338" id="Seg_3643" s="T1337">v-v:ins-v:mood.pn</ta>
            <ta e="T1339" id="Seg_3644" s="T1338">adv</ta>
            <ta e="T1340" id="Seg_3645" s="T1339">v-v&gt;v-v:pn</ta>
            <ta e="T1341" id="Seg_3646" s="T1340">adv</ta>
            <ta e="T1342" id="Seg_3647" s="T1341">dempro-n:case</ta>
            <ta e="T1343" id="Seg_3648" s="T1342">n-n:case</ta>
            <ta e="T1344" id="Seg_3649" s="T1343">dempro-n:case</ta>
            <ta e="T1345" id="Seg_3650" s="T1344">ptcl</ta>
            <ta e="T1346" id="Seg_3651" s="T1345">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1347" id="Seg_3652" s="T1346">conj</ta>
            <ta e="T1348" id="Seg_3653" s="T1347">dempro-n:case</ta>
            <ta e="T1349" id="Seg_3654" s="T1348">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1350" id="Seg_3655" s="T1349">adv</ta>
            <ta e="T1351" id="Seg_3656" s="T1350">v-v:tense-v:pn</ta>
            <ta e="T1352" id="Seg_3657" s="T1351">n-n:case</ta>
            <ta e="T1353" id="Seg_3658" s="T1352">n-n:case</ta>
            <ta e="T1354" id="Seg_3659" s="T1353">conj</ta>
            <ta e="T1355" id="Seg_3660" s="T1354">n-n:case</ta>
            <ta e="T1356" id="Seg_3661" s="T1355">n-n:case</ta>
            <ta e="T1357" id="Seg_3662" s="T1356">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1032" id="Seg_3663" s="T1031">v</ta>
            <ta e="T1033" id="Seg_3664" s="T1032">n</ta>
            <ta e="T1034" id="Seg_3665" s="T1033">conj</ta>
            <ta e="T1035" id="Seg_3666" s="T1034">n</ta>
            <ta e="T1036" id="Seg_3667" s="T1035">n</ta>
            <ta e="T1037" id="Seg_3668" s="T1036">v</ta>
            <ta e="T1038" id="Seg_3669" s="T1037">n</ta>
            <ta e="T1039" id="Seg_3670" s="T1038">n</ta>
            <ta e="T1040" id="Seg_3671" s="T1039">conj</ta>
            <ta e="T1041" id="Seg_3672" s="T1040">n</ta>
            <ta e="T1042" id="Seg_3673" s="T1041">v</ta>
            <ta e="T1044" id="Seg_3674" s="T1043">n</ta>
            <ta e="T1045" id="Seg_3675" s="T1044">adv</ta>
            <ta e="T1046" id="Seg_3676" s="T1045">v</ta>
            <ta e="T1047" id="Seg_3677" s="T1046">adj</ta>
            <ta e="T1048" id="Seg_3678" s="T1047">n</ta>
            <ta e="T1049" id="Seg_3679" s="T1048">n</ta>
            <ta e="T1050" id="Seg_3680" s="T1049">ptcl</ta>
            <ta e="T1051" id="Seg_3681" s="T1050">v</ta>
            <ta e="T1052" id="Seg_3682" s="T1051">adv</ta>
            <ta e="T1053" id="Seg_3683" s="T1052">dempro</ta>
            <ta e="T1054" id="Seg_3684" s="T1053">v</ta>
            <ta e="T1055" id="Seg_3685" s="T1054">n</ta>
            <ta e="T1056" id="Seg_3686" s="T1055">v</ta>
            <ta e="T1057" id="Seg_3687" s="T1056">pers</ta>
            <ta e="T1058" id="Seg_3688" s="T1057">v</ta>
            <ta e="T1059" id="Seg_3689" s="T1058">dempro</ta>
            <ta e="T1060" id="Seg_3690" s="T1059">refl</ta>
            <ta e="T1061" id="Seg_3691" s="T1060">dempro</ta>
            <ta e="T1062" id="Seg_3692" s="T1061">v</ta>
            <ta e="T1063" id="Seg_3693" s="T1062">n</ta>
            <ta e="T1064" id="Seg_3694" s="T1063">dempro</ta>
            <ta e="T1065" id="Seg_3695" s="T1064">v</ta>
            <ta e="T1066" id="Seg_3696" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_3697" s="T1066">v</ta>
            <ta e="T1068" id="Seg_3698" s="T1067">pers</ta>
            <ta e="T1069" id="Seg_3699" s="T1068">n</ta>
            <ta e="T1070" id="Seg_3700" s="T1069">v</ta>
            <ta e="T1071" id="Seg_3701" s="T1070">que</ta>
            <ta e="T1072" id="Seg_3702" s="T1071">pers</ta>
            <ta e="T1073" id="Seg_3703" s="T1072">v</ta>
            <ta e="T1074" id="Seg_3704" s="T1073">conj</ta>
            <ta e="T1075" id="Seg_3705" s="T1074">pers</ta>
            <ta e="T1076" id="Seg_3706" s="T1075">n</ta>
            <ta e="T1077" id="Seg_3707" s="T1076">v</ta>
            <ta e="T1078" id="Seg_3708" s="T1077">v</ta>
            <ta e="T1079" id="Seg_3709" s="T1078">n</ta>
            <ta e="T1080" id="Seg_3710" s="T1079">v</ta>
            <ta e="T1081" id="Seg_3711" s="T1080">pers</ta>
            <ta e="T1082" id="Seg_3712" s="T1081">dempro</ta>
            <ta e="T1083" id="Seg_3713" s="T1082">v</ta>
            <ta e="T1084" id="Seg_3714" s="T1083">v</ta>
            <ta e="T1085" id="Seg_3715" s="T1084">n</ta>
            <ta e="T1086" id="Seg_3716" s="T1085">conj</ta>
            <ta e="T1087" id="Seg_3717" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_3718" s="T1087">v</ta>
            <ta e="T1089" id="Seg_3719" s="T1088">v</ta>
            <ta e="T1090" id="Seg_3720" s="T1089">adv</ta>
            <ta e="T1091" id="Seg_3721" s="T1090">conj</ta>
            <ta e="T1092" id="Seg_3722" s="T1091">dempro</ta>
            <ta e="T1093" id="Seg_3723" s="T1092">v</ta>
            <ta e="T1094" id="Seg_3724" s="T1093">adv</ta>
            <ta e="T1095" id="Seg_3725" s="T1094">pers</ta>
            <ta e="T1096" id="Seg_3726" s="T1095">ptcl</ta>
            <ta e="T1097" id="Seg_3727" s="T1096">n</ta>
            <ta e="T1098" id="Seg_3728" s="T1097">v</ta>
            <ta e="T1099" id="Seg_3729" s="T1098">conj</ta>
            <ta e="T1100" id="Seg_3730" s="T1099">pers</ta>
            <ta e="T1101" id="Seg_3731" s="T1100">v</ta>
            <ta e="T1102" id="Seg_3732" s="T1101">dempro</ta>
            <ta e="T1103" id="Seg_3733" s="T1102">v</ta>
            <ta e="T1104" id="Seg_3734" s="T1103">conj</ta>
            <ta e="T1105" id="Seg_3735" s="T1104">v</ta>
            <ta e="T1106" id="Seg_3736" s="T1105">adv</ta>
            <ta e="T1107" id="Seg_3737" s="T1106">v</ta>
            <ta e="T1108" id="Seg_3738" s="T1107">n</ta>
            <ta e="T1109" id="Seg_3739" s="T1108">conj</ta>
            <ta e="T1110" id="Seg_3740" s="T1109">v</ta>
            <ta e="T1111" id="Seg_3741" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_3742" s="T1111">v</ta>
            <ta e="T1113" id="Seg_3743" s="T1112">n</ta>
            <ta e="T1114" id="Seg_3744" s="T1113">que</ta>
            <ta e="T1115" id="Seg_3745" s="T1114">v</ta>
            <ta e="T1116" id="Seg_3746" s="T1115">conj</ta>
            <ta e="T1117" id="Seg_3747" s="T1116">ptcl</ta>
            <ta e="T1118" id="Seg_3748" s="T1117">pers</ta>
            <ta e="T1119" id="Seg_3749" s="T1118">ptcl</ta>
            <ta e="T1120" id="Seg_3750" s="T1119">v</ta>
            <ta e="T1121" id="Seg_3751" s="T1120">pers</ta>
            <ta e="T1122" id="Seg_3752" s="T1121">n</ta>
            <ta e="T1123" id="Seg_3753" s="T1122">v</ta>
            <ta e="T1124" id="Seg_3754" s="T1123">n</ta>
            <ta e="T1125" id="Seg_3755" s="T1124">conj</ta>
            <ta e="T1126" id="Seg_3756" s="T1125">n</ta>
            <ta e="T1127" id="Seg_3757" s="T1126">n</ta>
            <ta e="T1129" id="Seg_3758" s="T1128">n</ta>
            <ta e="T1130" id="Seg_3759" s="T1129">adv</ta>
            <ta e="T1131" id="Seg_3760" s="T1130">dempro</ta>
            <ta e="T1132" id="Seg_3761" s="T1131">v</ta>
            <ta e="T1133" id="Seg_3762" s="T1132">pers</ta>
            <ta e="T1134" id="Seg_3763" s="T1133">v</ta>
            <ta e="T1135" id="Seg_3764" s="T1134">conj</ta>
            <ta e="T1136" id="Seg_3765" s="T1135">pers</ta>
            <ta e="T1137" id="Seg_3766" s="T1136">v</ta>
            <ta e="T1138" id="Seg_3767" s="T1137">v</ta>
            <ta e="T1139" id="Seg_3768" s="T1138">pers</ta>
            <ta e="T1140" id="Seg_3769" s="T1139">dempro</ta>
            <ta e="T1141" id="Seg_3770" s="T1140">v</ta>
            <ta e="T1142" id="Seg_3771" s="T1141">v</ta>
            <ta e="T1143" id="Seg_3772" s="T1142">v</ta>
            <ta e="T1144" id="Seg_3773" s="T1143">adv</ta>
            <ta e="T1145" id="Seg_3774" s="T1144">ptcl</ta>
            <ta e="T1146" id="Seg_3775" s="T1145">adv</ta>
            <ta e="T1147" id="Seg_3776" s="T1146">pers</ta>
            <ta e="T1148" id="Seg_3777" s="T1147">v</ta>
            <ta e="T1149" id="Seg_3778" s="T1148">conj</ta>
            <ta e="T1150" id="Seg_3779" s="T1149">dempro</ta>
            <ta e="T1151" id="Seg_3780" s="T1150">v</ta>
            <ta e="T1152" id="Seg_3781" s="T1151">pers</ta>
            <ta e="T1153" id="Seg_3782" s="T1152">ptcl</ta>
            <ta e="T1154" id="Seg_3783" s="T1153">v</ta>
            <ta e="T1155" id="Seg_3784" s="T1154">conj</ta>
            <ta e="T1156" id="Seg_3785" s="T1155">ptcl</ta>
            <ta e="T1157" id="Seg_3786" s="T1156">n</ta>
            <ta e="T1158" id="Seg_3787" s="T1157">pers</ta>
            <ta e="T1159" id="Seg_3788" s="T1158">v</ta>
            <ta e="T1160" id="Seg_3789" s="T1159">conj</ta>
            <ta e="T1161" id="Seg_3790" s="T1160">pers</ta>
            <ta e="T1162" id="Seg_3791" s="T1161">v</ta>
            <ta e="T1163" id="Seg_3792" s="T1162">adv</ta>
            <ta e="T1164" id="Seg_3793" s="T1163">n</ta>
            <ta e="T1167" id="Seg_3794" s="T1166">conj</ta>
            <ta e="T1168" id="Seg_3795" s="T1167">v</ta>
            <ta e="T1169" id="Seg_3796" s="T1168">adv</ta>
            <ta e="T1170" id="Seg_3797" s="T1169">v</ta>
            <ta e="T1171" id="Seg_3798" s="T1170">n</ta>
            <ta e="T1172" id="Seg_3799" s="T1171">conj</ta>
            <ta e="T1173" id="Seg_3800" s="T1172">v</ta>
            <ta e="T1174" id="Seg_3801" s="T1173">adv</ta>
            <ta e="T1175" id="Seg_3802" s="T1174">n</ta>
            <ta e="T1176" id="Seg_3803" s="T1175">v</ta>
            <ta e="T1177" id="Seg_3804" s="T1176">que</ta>
            <ta e="T1178" id="Seg_3805" s="T1177">v</ta>
            <ta e="T1179" id="Seg_3806" s="T1178">conj</ta>
            <ta e="T1180" id="Seg_3807" s="T1179">pers</ta>
            <ta e="T1181" id="Seg_3808" s="T1180">n</ta>
            <ta e="T1182" id="Seg_3809" s="T1181">n</ta>
            <ta e="T1183" id="Seg_3810" s="T1182">v</ta>
            <ta e="T1184" id="Seg_3811" s="T1183">conj</ta>
            <ta e="T1185" id="Seg_3812" s="T1184">dempro</ta>
            <ta e="T1187" id="Seg_3813" s="T1186">v</ta>
            <ta e="T1188" id="Seg_3814" s="T1187">n</ta>
            <ta e="T1189" id="Seg_3815" s="T1188">conj</ta>
            <ta e="T1190" id="Seg_3816" s="T1189">v</ta>
            <ta e="T1191" id="Seg_3817" s="T1190">conj</ta>
            <ta e="T1192" id="Seg_3818" s="T1191">pers</ta>
            <ta e="T1193" id="Seg_3819" s="T1192">adv</ta>
            <ta e="T1194" id="Seg_3820" s="T1193">n</ta>
            <ta e="T1195" id="Seg_3821" s="T1194">adj</ta>
            <ta e="T1196" id="Seg_3822" s="T1195">n</ta>
            <ta e="T1197" id="Seg_3823" s="T1196">adv</ta>
            <ta e="T1198" id="Seg_3824" s="T1197">v</ta>
            <ta e="T1199" id="Seg_3825" s="T1198">pers</ta>
            <ta e="T1200" id="Seg_3826" s="T1199">dempro</ta>
            <ta e="T1201" id="Seg_3827" s="T1200">v</ta>
            <ta e="T1202" id="Seg_3828" s="T1201">ptcl</ta>
            <ta e="T1203" id="Seg_3829" s="T1202">ptcl</ta>
            <ta e="T1204" id="Seg_3830" s="T1203">v</ta>
            <ta e="T1205" id="Seg_3831" s="T1204">n</ta>
            <ta e="T1206" id="Seg_3832" s="T1205">ptcl</ta>
            <ta e="T1207" id="Seg_3833" s="T1206">v</ta>
            <ta e="T1208" id="Seg_3834" s="T1207">v</ta>
            <ta e="T1209" id="Seg_3835" s="T1208">n</ta>
            <ta e="T1210" id="Seg_3836" s="T1209">ptcl</ta>
            <ta e="T1211" id="Seg_3837" s="T1210">v</ta>
            <ta e="T1212" id="Seg_3838" s="T1211">n</ta>
            <ta e="T1213" id="Seg_3839" s="T1212">conj</ta>
            <ta e="T1214" id="Seg_3840" s="T1213">pers</ta>
            <ta e="T1216" id="Seg_3841" s="T1215">adv</ta>
            <ta e="T1218" id="Seg_3842" s="T1217">v</ta>
            <ta e="T1219" id="Seg_3843" s="T1218">v</ta>
            <ta e="T1221" id="Seg_3844" s="T1220">n</ta>
            <ta e="T1222" id="Seg_3845" s="T1221">v</ta>
            <ta e="T1225" id="Seg_3846" s="T1224">v</ta>
            <ta e="T1226" id="Seg_3847" s="T1225">n</ta>
            <ta e="T1227" id="Seg_3848" s="T1226">conj</ta>
            <ta e="T1228" id="Seg_3849" s="T1227">dempro</ta>
            <ta e="T1229" id="Seg_3850" s="T1228">v</ta>
            <ta e="T1230" id="Seg_3851" s="T1229">adv</ta>
            <ta e="T1231" id="Seg_3852" s="T1230">v</ta>
            <ta e="T1232" id="Seg_3853" s="T1231">ptcl</ta>
            <ta e="T1233" id="Seg_3854" s="T1232">ptcl</ta>
            <ta e="T1234" id="Seg_3855" s="T1233">pers</ta>
            <ta e="T1235" id="Seg_3856" s="T1234">adv</ta>
            <ta e="T1236" id="Seg_3857" s="T1235">v</ta>
            <ta e="T1237" id="Seg_3858" s="T1236">adv</ta>
            <ta e="T1238" id="Seg_3859" s="T1237">n</ta>
            <ta e="T1239" id="Seg_3860" s="T1238">n</ta>
            <ta e="T1240" id="Seg_3861" s="T1239">v</ta>
            <ta e="T1241" id="Seg_3862" s="T1240">conj</ta>
            <ta e="T1242" id="Seg_3863" s="T1241">v</ta>
            <ta e="T1243" id="Seg_3864" s="T1242">adv</ta>
            <ta e="T1244" id="Seg_3865" s="T1243">adv</ta>
            <ta e="T1245" id="Seg_3866" s="T1244">v</ta>
            <ta e="T1246" id="Seg_3867" s="T1245">n</ta>
            <ta e="T1247" id="Seg_3868" s="T1246">conj</ta>
            <ta e="T1248" id="Seg_3869" s="T1247">v</ta>
            <ta e="T1249" id="Seg_3870" s="T1248">adv</ta>
            <ta e="T1250" id="Seg_3871" s="T1249">n</ta>
            <ta e="T1251" id="Seg_3872" s="T1250">n</ta>
            <ta e="T1252" id="Seg_3873" s="T1251">v</ta>
            <ta e="T1253" id="Seg_3874" s="T1252">n</ta>
            <ta e="T1254" id="Seg_3875" s="T1253">v</ta>
            <ta e="T1255" id="Seg_3876" s="T1254">que</ta>
            <ta e="T1256" id="Seg_3877" s="T1255">v</ta>
            <ta e="T1257" id="Seg_3878" s="T1256">conj</ta>
            <ta e="T1258" id="Seg_3879" s="T1257">pers</ta>
            <ta e="T1259" id="Seg_3880" s="T1258">n</ta>
            <ta e="T1260" id="Seg_3881" s="T1259">n</ta>
            <ta e="T1261" id="Seg_3882" s="T1260">n</ta>
            <ta e="T1262" id="Seg_3883" s="T1261">v</ta>
            <ta e="T1263" id="Seg_3884" s="T1262">v</ta>
            <ta e="T1264" id="Seg_3885" s="T1263">conj</ta>
            <ta e="T1265" id="Seg_3886" s="T1264">pers</ta>
            <ta e="T1266" id="Seg_3887" s="T1265">v</ta>
            <ta e="T1267" id="Seg_3888" s="T1266">conj</ta>
            <ta e="T1268" id="Seg_3889" s="T1267">dempro</ta>
            <ta e="T1269" id="Seg_3890" s="T1268">n</ta>
            <ta e="T1270" id="Seg_3891" s="T1269">v</ta>
            <ta e="T1271" id="Seg_3892" s="T1270">conj</ta>
            <ta e="T1272" id="Seg_3893" s="T1271">adv</ta>
            <ta e="T1273" id="Seg_3894" s="T1272">v</ta>
            <ta e="T1274" id="Seg_3895" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_3896" s="T1274">v</ta>
            <ta e="T1276" id="Seg_3897" s="T1275">pers</ta>
            <ta e="T1277" id="Seg_3898" s="T1276">v</ta>
            <ta e="T1278" id="Seg_3899" s="T1277">n</ta>
            <ta e="T1279" id="Seg_3900" s="T1278">ptcl</ta>
            <ta e="T1280" id="Seg_3901" s="T1279">v</ta>
            <ta e="T1281" id="Seg_3902" s="T1280">v</ta>
            <ta e="T1282" id="Seg_3903" s="T1281">dempro</ta>
            <ta e="T1283" id="Seg_3904" s="T1282">n</ta>
            <ta e="T1284" id="Seg_3905" s="T1283">conj</ta>
            <ta e="T1285" id="Seg_3906" s="T1284">n</ta>
            <ta e="T1286" id="Seg_3907" s="T1285">ptcl</ta>
            <ta e="T1287" id="Seg_3908" s="T1286">v</ta>
            <ta e="T1288" id="Seg_3909" s="T1287">conj</ta>
            <ta e="T1289" id="Seg_3910" s="T1288">pers</ta>
            <ta e="T1290" id="Seg_3911" s="T1289">ptcl</ta>
            <ta e="T1291" id="Seg_3912" s="T1290">v</ta>
            <ta e="T1292" id="Seg_3913" s="T1291">v</ta>
            <ta e="T1293" id="Seg_3914" s="T1292">adv</ta>
            <ta e="T1294" id="Seg_3915" s="T1293">v</ta>
            <ta e="T1295" id="Seg_3916" s="T1294">ptcl</ta>
            <ta e="T1296" id="Seg_3917" s="T1295">v</ta>
            <ta e="T1297" id="Seg_3918" s="T1296">v</ta>
            <ta e="T1298" id="Seg_3919" s="T1297">n</ta>
            <ta e="T1301" id="Seg_3920" s="T1300">v</ta>
            <ta e="T1302" id="Seg_3921" s="T1301">adv</ta>
            <ta e="T1304" id="Seg_3922" s="T1302">ptcl</ta>
            <ta e="T1305" id="Seg_3923" s="T1304">adv</ta>
            <ta e="T1306" id="Seg_3924" s="T1305">n</ta>
            <ta e="T1307" id="Seg_3925" s="T1306">pers</ta>
            <ta e="T1308" id="Seg_3926" s="T1307">v</ta>
            <ta e="T1309" id="Seg_3927" s="T1308">ptcl</ta>
            <ta e="T1310" id="Seg_3928" s="T1309">v</ta>
            <ta e="T1311" id="Seg_3929" s="T1310">pers</ta>
            <ta e="T1312" id="Seg_3930" s="T1311">adv</ta>
            <ta e="T1313" id="Seg_3931" s="T1312">n</ta>
            <ta e="T1314" id="Seg_3932" s="T1313">n</ta>
            <ta e="T1315" id="Seg_3933" s="T1314">v</ta>
            <ta e="T1316" id="Seg_3934" s="T1315">n</ta>
            <ta e="T1317" id="Seg_3935" s="T1316">adv</ta>
            <ta e="T1318" id="Seg_3936" s="T1317">adv</ta>
            <ta e="T1319" id="Seg_3937" s="T1318">v</ta>
            <ta e="T1320" id="Seg_3938" s="T1319">v</ta>
            <ta e="T1321" id="Seg_3939" s="T1320">n</ta>
            <ta e="T1322" id="Seg_3940" s="T1321">ptcl</ta>
            <ta e="T1323" id="Seg_3941" s="T1322">adv</ta>
            <ta e="T1324" id="Seg_3942" s="T1323">pers</ta>
            <ta e="T1325" id="Seg_3943" s="T1324">v</ta>
            <ta e="T1326" id="Seg_3944" s="T1325">n</ta>
            <ta e="T1327" id="Seg_3945" s="T1326">n</ta>
            <ta e="T1328" id="Seg_3946" s="T1327">ptcl</ta>
            <ta e="T1329" id="Seg_3947" s="T1328">v</ta>
            <ta e="T1330" id="Seg_3948" s="T1329">adv</ta>
            <ta e="T1331" id="Seg_3949" s="T1330">ptcl</ta>
            <ta e="T1332" id="Seg_3950" s="T1331">n</ta>
            <ta e="T1333" id="Seg_3951" s="T1332">v</ta>
            <ta e="T1334" id="Seg_3952" s="T1333">adv</ta>
            <ta e="T1335" id="Seg_3953" s="T1334">dempro</ta>
            <ta e="T1336" id="Seg_3954" s="T1335">adv</ta>
            <ta e="T1337" id="Seg_3955" s="T1336">v</ta>
            <ta e="T1338" id="Seg_3956" s="T1337">v</ta>
            <ta e="T1339" id="Seg_3957" s="T1338">adv</ta>
            <ta e="T1340" id="Seg_3958" s="T1339">v</ta>
            <ta e="T1341" id="Seg_3959" s="T1340">adv</ta>
            <ta e="T1342" id="Seg_3960" s="T1341">dempro</ta>
            <ta e="T1343" id="Seg_3961" s="T1342">n</ta>
            <ta e="T1344" id="Seg_3962" s="T1343">dempro</ta>
            <ta e="T1345" id="Seg_3963" s="T1344">ptcl</ta>
            <ta e="T1346" id="Seg_3964" s="T1345">v</ta>
            <ta e="T1347" id="Seg_3965" s="T1346">conj</ta>
            <ta e="T1348" id="Seg_3966" s="T1347">dempro</ta>
            <ta e="T1349" id="Seg_3967" s="T1348">v</ta>
            <ta e="T1350" id="Seg_3968" s="T1349">adv</ta>
            <ta e="T1351" id="Seg_3969" s="T1350">v</ta>
            <ta e="T1352" id="Seg_3970" s="T1351">n</ta>
            <ta e="T1353" id="Seg_3971" s="T1352">n</ta>
            <ta e="T1354" id="Seg_3972" s="T1353">conj</ta>
            <ta e="T1355" id="Seg_3973" s="T1354">n</ta>
            <ta e="T1356" id="Seg_3974" s="T1355">n</ta>
            <ta e="T1357" id="Seg_3975" s="T1356">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1033" id="Seg_3976" s="T1032">np.h:E</ta>
            <ta e="T1035" id="Seg_3977" s="T1034">np.h:E</ta>
            <ta e="T1036" id="Seg_3978" s="T1035">np.h:Poss</ta>
            <ta e="T1038" id="Seg_3979" s="T1037">np:Th</ta>
            <ta e="T1041" id="Seg_3980" s="T1040">np.h:Poss np:Th</ta>
            <ta e="T1045" id="Seg_3981" s="T1044">adv:Time</ta>
            <ta e="T1048" id="Seg_3982" s="T1047">np.h:Poss</ta>
            <ta e="T1049" id="Seg_3983" s="T1048">np:Th</ta>
            <ta e="T1052" id="Seg_3984" s="T1051">adv:Time</ta>
            <ta e="T1053" id="Seg_3985" s="T1052">pro.h:A</ta>
            <ta e="T1055" id="Seg_3986" s="T1054">np:G</ta>
            <ta e="T1056" id="Seg_3987" s="T1055">0.2.h:A</ta>
            <ta e="T1057" id="Seg_3988" s="T1056">pro.h:E</ta>
            <ta e="T1059" id="Seg_3989" s="T1058">pro.h:A</ta>
            <ta e="T1061" id="Seg_3990" s="T1060">pro.h:Th</ta>
            <ta e="T1063" id="Seg_3991" s="T1062">np:So</ta>
            <ta e="T1064" id="Seg_3992" s="T1063">pro.h:A</ta>
            <ta e="T1067" id="Seg_3993" s="T1066">0.3.h:E</ta>
            <ta e="T1069" id="Seg_3994" s="T1068">np.h:A</ta>
            <ta e="T1072" id="Seg_3995" s="T1071">pro.h:E</ta>
            <ta e="T1075" id="Seg_3996" s="T1074">pro.h:Poss</ta>
            <ta e="T1076" id="Seg_3997" s="T1075">np:So</ta>
            <ta e="T1077" id="Seg_3998" s="T1076">0.3.h:A</ta>
            <ta e="T1079" id="Seg_3999" s="T1078">np.h:A</ta>
            <ta e="T1080" id="Seg_4000" s="T1079">0.1.h:A</ta>
            <ta e="T1081" id="Seg_4001" s="T1080">pro.h:A</ta>
            <ta e="T1082" id="Seg_4002" s="T1081">pro.h:Th</ta>
            <ta e="T1085" id="Seg_4003" s="T1084">np.h:A</ta>
            <ta e="T1088" id="Seg_4004" s="T1087">0.3.h:A</ta>
            <ta e="T1089" id="Seg_4005" s="T1088">0.2.h:A</ta>
            <ta e="T1090" id="Seg_4006" s="T1089">adv:So</ta>
            <ta e="T1092" id="Seg_4007" s="T1091">pro.h:A</ta>
            <ta e="T1094" id="Seg_4008" s="T1093">adv:Time</ta>
            <ta e="T1095" id="Seg_4009" s="T1094">pro.h:Poss</ta>
            <ta e="T1097" id="Seg_4010" s="T1096">np:P</ta>
            <ta e="T1098" id="Seg_4011" s="T1097">0.1.h:A</ta>
            <ta e="T1100" id="Seg_4012" s="T1099">pro.h:A</ta>
            <ta e="T1102" id="Seg_4013" s="T1101">pro.h:E</ta>
            <ta e="T1105" id="Seg_4014" s="T1104">0.3.h:A</ta>
            <ta e="T1108" id="Seg_4015" s="T1107">np.h:A</ta>
            <ta e="T1110" id="Seg_4016" s="T1109">0.3.h:E</ta>
            <ta e="T1111" id="Seg_4017" s="T1110">adv:Time</ta>
            <ta e="T1113" id="Seg_4018" s="T1112">np.h:A</ta>
            <ta e="T1115" id="Seg_4019" s="T1114">0.2.h:E</ta>
            <ta e="T1118" id="Seg_4020" s="T1117">pro.h:E</ta>
            <ta e="T1121" id="Seg_4021" s="T1120">pro.h:Poss</ta>
            <ta e="T1122" id="Seg_4022" s="T1121">np:Th</ta>
            <ta e="T1126" id="Seg_4023" s="T1125">np.h:Poss</ta>
            <ta e="T1127" id="Seg_4024" s="T1126">np:Th</ta>
            <ta e="T1130" id="Seg_4025" s="T1129">adv:Time</ta>
            <ta e="T1131" id="Seg_4026" s="T1130">pro.h:A</ta>
            <ta e="T1133" id="Seg_4027" s="T1132">pro:G</ta>
            <ta e="T1136" id="Seg_4028" s="T1135">pro.h:Th</ta>
            <ta e="T1137" id="Seg_4029" s="T1136">0.3.h:A</ta>
            <ta e="T1138" id="Seg_4030" s="T1137">0.1.h:A</ta>
            <ta e="T1139" id="Seg_4031" s="T1138">pro.h:A</ta>
            <ta e="T1140" id="Seg_4032" s="T1139">pro.h:Th</ta>
            <ta e="T1142" id="Seg_4033" s="T1141">0.3.h:A</ta>
            <ta e="T1143" id="Seg_4034" s="T1142">0.2.h:A</ta>
            <ta e="T1144" id="Seg_4035" s="T1143">adv:So</ta>
            <ta e="T1146" id="Seg_4036" s="T1145">adv:Time</ta>
            <ta e="T1147" id="Seg_4037" s="T1146">pro.h:E</ta>
            <ta e="T1148" id="Seg_4038" s="T1147">0.1.h:A</ta>
            <ta e="T1150" id="Seg_4039" s="T1149">pro.h:A</ta>
            <ta e="T1152" id="Seg_4040" s="T1151">pro.h:A</ta>
            <ta e="T1157" id="Seg_4041" s="T1156">np:P</ta>
            <ta e="T1158" id="Seg_4042" s="T1157">pro.h:Poss</ta>
            <ta e="T1159" id="Seg_4043" s="T1158">0.2.h:A</ta>
            <ta e="T1161" id="Seg_4044" s="T1160">pro.h:A</ta>
            <ta e="T1163" id="Seg_4045" s="T1162">adv:Time</ta>
            <ta e="T1164" id="Seg_4046" s="T1163">np.h:E</ta>
            <ta e="T1168" id="Seg_4047" s="T1167">0.3.h:A</ta>
            <ta e="T1171" id="Seg_4048" s="T1170">np.h:A</ta>
            <ta e="T1173" id="Seg_4049" s="T1172">0.3.h:E</ta>
            <ta e="T1174" id="Seg_4050" s="T1173">adv:Time</ta>
            <ta e="T1175" id="Seg_4051" s="T1174">np.h:A</ta>
            <ta e="T1178" id="Seg_4052" s="T1177">0.2.h:E</ta>
            <ta e="T1180" id="Seg_4053" s="T1179">pro.h:Poss</ta>
            <ta e="T1181" id="Seg_4054" s="T1180">np:Th</ta>
            <ta e="T1182" id="Seg_4055" s="T1181">np.h:A</ta>
            <ta e="T1185" id="Seg_4056" s="T1184">pro.h:Poss</ta>
            <ta e="T1190" id="Seg_4057" s="T1189">0.3:P</ta>
            <ta e="T1192" id="Seg_4058" s="T1191">pro.h:Poss</ta>
            <ta e="T1194" id="Seg_4059" s="T1193">np:Th</ta>
            <ta e="T1197" id="Seg_4060" s="T1196">adv:Time</ta>
            <ta e="T1198" id="Seg_4061" s="T1197">0.1.h:A</ta>
            <ta e="T1199" id="Seg_4062" s="T1198">pro.h:A</ta>
            <ta e="T1200" id="Seg_4063" s="T1199">pro.h:Th</ta>
            <ta e="T1204" id="Seg_4064" s="T1203">0.2.h:A</ta>
            <ta e="T1205" id="Seg_4065" s="T1204">np.h:A</ta>
            <ta e="T1209" id="Seg_4066" s="T1208">np.h:A</ta>
            <ta e="T1216" id="Seg_4067" s="T1215">adv:Time</ta>
            <ta e="T1218" id="Seg_4068" s="T1217">0.3.h:A</ta>
            <ta e="T1219" id="Seg_4069" s="T1218">0.3.h:A</ta>
            <ta e="T1221" id="Seg_4070" s="T1220">np.h:A</ta>
            <ta e="T1225" id="Seg_4071" s="T1224">0.2.h:A</ta>
            <ta e="T1226" id="Seg_4072" s="T1225">np:So</ta>
            <ta e="T1228" id="Seg_4073" s="T1227">pro.h:A</ta>
            <ta e="T1231" id="Seg_4074" s="T1230">0.1.h:A</ta>
            <ta e="T1234" id="Seg_4075" s="T1233">pro.h:P</ta>
            <ta e="T1236" id="Seg_4076" s="T1235">0.1.h:A</ta>
            <ta e="T1237" id="Seg_4077" s="T1236">adv:Time</ta>
            <ta e="T1239" id="Seg_4078" s="T1238">np.h:E</ta>
            <ta e="T1242" id="Seg_4079" s="T1241">0.3.h:A</ta>
            <ta e="T1243" id="Seg_4080" s="T1242">adv:Time</ta>
            <ta e="T1246" id="Seg_4081" s="T1245">np.h:A</ta>
            <ta e="T1248" id="Seg_4082" s="T1247">0.3.h:E</ta>
            <ta e="T1249" id="Seg_4083" s="T1248">adv:Time</ta>
            <ta e="T1250" id="Seg_4084" s="T1249">np.h:Poss</ta>
            <ta e="T1251" id="Seg_4085" s="T1250">np.h:A</ta>
            <ta e="T1253" id="Seg_4086" s="T1252">np:Ins</ta>
            <ta e="T1254" id="Seg_4087" s="T1253">0.3.h:A</ta>
            <ta e="T1256" id="Seg_4088" s="T1255">0.2.h:E</ta>
            <ta e="T1258" id="Seg_4089" s="T1257">pro.h:Poss</ta>
            <ta e="T1259" id="Seg_4090" s="T1258">np:Th</ta>
            <ta e="T1261" id="Seg_4091" s="T1260">np.h:A</ta>
            <ta e="T1265" id="Seg_4092" s="T1264">pro.h:Th</ta>
            <ta e="T1266" id="Seg_4093" s="T1265">0.3.h:A</ta>
            <ta e="T1268" id="Seg_4094" s="T1267">pro.h:Poss np:Th</ta>
            <ta e="T1274" id="Seg_4095" s="T1273">adv:Time</ta>
            <ta e="T1275" id="Seg_4096" s="T1274">0.1.h:A</ta>
            <ta e="T1276" id="Seg_4097" s="T1275">pro.h:A</ta>
            <ta e="T1278" id="Seg_4098" s="T1277">np.h:A</ta>
            <ta e="T1283" id="Seg_4099" s="T1282">np.h:A</ta>
            <ta e="T1285" id="Seg_4100" s="T1284">np.h:A</ta>
            <ta e="T1289" id="Seg_4101" s="T1288">pro.h:A</ta>
            <ta e="T1292" id="Seg_4102" s="T1291">0.1.h:A</ta>
            <ta e="T1293" id="Seg_4103" s="T1292">adv:Time</ta>
            <ta e="T1294" id="Seg_4104" s="T1293">0.3.h:A</ta>
            <ta e="T1296" id="Seg_4105" s="T1295">0.3.h:A</ta>
            <ta e="T1297" id="Seg_4106" s="T1296">0.2.h:A</ta>
            <ta e="T1298" id="Seg_4107" s="T1297">np:So</ta>
            <ta e="T1301" id="Seg_4108" s="T1300">0.2.h:A</ta>
            <ta e="T1302" id="Seg_4109" s="T1301">adv:So</ta>
            <ta e="T1305" id="Seg_4110" s="T1304">adv:Time</ta>
            <ta e="T1306" id="Seg_4111" s="T1305">np:Ins</ta>
            <ta e="T1307" id="Seg_4112" s="T1306">pro.h:P</ta>
            <ta e="T1308" id="Seg_4113" s="T1307">0.1.h:A</ta>
            <ta e="T1310" id="Seg_4114" s="T1309">0.1.h:A</ta>
            <ta e="T1311" id="Seg_4115" s="T1310">pro.h:P</ta>
            <ta e="T1312" id="Seg_4116" s="T1311">adv:Time</ta>
            <ta e="T1314" id="Seg_4117" s="T1313">np:Th</ta>
            <ta e="T1316" id="Seg_4118" s="T1315">np:Th</ta>
            <ta e="T1317" id="Seg_4119" s="T1316">adv:Time</ta>
            <ta e="T1319" id="Seg_4120" s="T1318">0.3.h:A</ta>
            <ta e="T1320" id="Seg_4121" s="T1319">0.2.h:A</ta>
            <ta e="T1321" id="Seg_4122" s="T1320">np:So</ta>
            <ta e="T1324" id="Seg_4123" s="T1323">pro.h:P</ta>
            <ta e="T1325" id="Seg_4124" s="T1324">0.1.h:A</ta>
            <ta e="T1327" id="Seg_4125" s="T1326">np:Ins</ta>
            <ta e="T1329" id="Seg_4126" s="T1328">0.1.h:A</ta>
            <ta e="T1332" id="Seg_4127" s="T1331">np:Th</ta>
            <ta e="T1333" id="Seg_4128" s="T1332">0.1.h:A</ta>
            <ta e="T1334" id="Seg_4129" s="T1333">adv:Time</ta>
            <ta e="T1335" id="Seg_4130" s="T1334">pro.h:A</ta>
            <ta e="T1338" id="Seg_4131" s="T1337">0.2.h:A</ta>
            <ta e="T1339" id="Seg_4132" s="T1338">adv:Time</ta>
            <ta e="T1340" id="Seg_4133" s="T1339">0.1.h:A</ta>
            <ta e="T1341" id="Seg_4134" s="T1340">adv:Time</ta>
            <ta e="T1342" id="Seg_4135" s="T1341">pro.h:A</ta>
            <ta e="T1343" id="Seg_4136" s="T1342">np:Ins</ta>
            <ta e="T1344" id="Seg_4137" s="T1343">pro.h:P</ta>
            <ta e="T1348" id="Seg_4138" s="T1347">pro.h:P</ta>
            <ta e="T1350" id="Seg_4139" s="T1349">adv:Time</ta>
            <ta e="T1352" id="Seg_4140" s="T1351">np.h:Poss</ta>
            <ta e="T1353" id="Seg_4141" s="T1352">np.h:E</ta>
            <ta e="T1355" id="Seg_4142" s="T1354">np.h:E</ta>
            <ta e="T1356" id="Seg_4143" s="T1355">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1032" id="Seg_4144" s="T1031">v:pred</ta>
            <ta e="T1033" id="Seg_4145" s="T1032">np.h:S</ta>
            <ta e="T1035" id="Seg_4146" s="T1034">np.h:S</ta>
            <ta e="T1037" id="Seg_4147" s="T1036">cop</ta>
            <ta e="T1038" id="Seg_4148" s="T1037">np:S</ta>
            <ta e="T1039" id="Seg_4149" s="T1038">n:pred</ta>
            <ta e="T1041" id="Seg_4150" s="T1040">np:S</ta>
            <ta e="T1042" id="Seg_4151" s="T1041">cop</ta>
            <ta e="T1044" id="Seg_4152" s="T1043">n:pred</ta>
            <ta e="T1046" id="Seg_4153" s="T1045">v:pred 0.3:S</ta>
            <ta e="T1047" id="Seg_4154" s="T1046">adj:pred</ta>
            <ta e="T1049" id="Seg_4155" s="T1048">np:S</ta>
            <ta e="T1051" id="Seg_4156" s="T1050">v:pred</ta>
            <ta e="T1053" id="Seg_4157" s="T1052">pro.h:S</ta>
            <ta e="T1054" id="Seg_4158" s="T1053">v:pred</ta>
            <ta e="T1056" id="Seg_4159" s="T1055">v:pred 0.2.h:S</ta>
            <ta e="T1057" id="Seg_4160" s="T1056">pro.h:O</ta>
            <ta e="T1059" id="Seg_4161" s="T1058">pro.h:S</ta>
            <ta e="T1061" id="Seg_4162" s="T1060">pro.h:O</ta>
            <ta e="T1062" id="Seg_4163" s="T1061">v:pred</ta>
            <ta e="T1064" id="Seg_4164" s="T1063">pro.h:S</ta>
            <ta e="T1065" id="Seg_4165" s="T1064">v:pred</ta>
            <ta e="T1067" id="Seg_4166" s="T1066">v:pred 0.3.h:S</ta>
            <ta e="T1069" id="Seg_4167" s="T1068">np.h:S</ta>
            <ta e="T1070" id="Seg_4168" s="T1069">v:pred</ta>
            <ta e="T1072" id="Seg_4169" s="T1071">pro.h:S</ta>
            <ta e="T1073" id="Seg_4170" s="T1072">v:pred</ta>
            <ta e="T1077" id="Seg_4171" s="T1076">v:pred 0.3.h:S</ta>
            <ta e="T1078" id="Seg_4172" s="T1077">v:pred</ta>
            <ta e="T1079" id="Seg_4173" s="T1078">np.h:S</ta>
            <ta e="T1080" id="Seg_4174" s="T1079">v:pred 0.1.h:S</ta>
            <ta e="T1081" id="Seg_4175" s="T1080">pro.h:S</ta>
            <ta e="T1082" id="Seg_4176" s="T1081">pro.h:O</ta>
            <ta e="T1083" id="Seg_4177" s="T1082">v:pred</ta>
            <ta e="T1084" id="Seg_4178" s="T1083">v:pred</ta>
            <ta e="T1085" id="Seg_4179" s="T1084">np.h:S</ta>
            <ta e="T1088" id="Seg_4180" s="T1087">v:pred 0.3.h:S</ta>
            <ta e="T1089" id="Seg_4181" s="T1088">v:pred 0.2.h:S</ta>
            <ta e="T1092" id="Seg_4182" s="T1091">pro.h:S</ta>
            <ta e="T1093" id="Seg_4183" s="T1092">v:pred</ta>
            <ta e="T1097" id="Seg_4184" s="T1096">np:O</ta>
            <ta e="T1098" id="Seg_4185" s="T1097">v:pred 0.1.h:S</ta>
            <ta e="T1100" id="Seg_4186" s="T1099">pro.h:S</ta>
            <ta e="T1101" id="Seg_4187" s="T1100">v:pred</ta>
            <ta e="T1102" id="Seg_4188" s="T1101">pro.h:S</ta>
            <ta e="T1103" id="Seg_4189" s="T1102">v:pred</ta>
            <ta e="T1105" id="Seg_4190" s="T1104">v:pred 0.3.h:S</ta>
            <ta e="T1107" id="Seg_4191" s="T1106">v:pred</ta>
            <ta e="T1108" id="Seg_4192" s="T1107">np.h:S</ta>
            <ta e="T1110" id="Seg_4193" s="T1109">v:pred 0.3.h:S</ta>
            <ta e="T1112" id="Seg_4194" s="T1111">v:pred</ta>
            <ta e="T1113" id="Seg_4195" s="T1112">np.h:S</ta>
            <ta e="T1115" id="Seg_4196" s="T1114">v:pred 0.2.h:S</ta>
            <ta e="T1117" id="Seg_4197" s="T1116">ptcl:pred</ta>
            <ta e="T1119" id="Seg_4198" s="T1118">ptcl.neg</ta>
            <ta e="T1122" id="Seg_4199" s="T1121">np:S</ta>
            <ta e="T1123" id="Seg_4200" s="T1122">cop</ta>
            <ta e="T1124" id="Seg_4201" s="T1123">n:pred</ta>
            <ta e="T1127" id="Seg_4202" s="T1126">np:S</ta>
            <ta e="T1129" id="Seg_4203" s="T1128">n:pred</ta>
            <ta e="T1131" id="Seg_4204" s="T1130">pro.h:S</ta>
            <ta e="T1132" id="Seg_4205" s="T1131">v:pred</ta>
            <ta e="T1136" id="Seg_4206" s="T1135">pro.h:O</ta>
            <ta e="T1137" id="Seg_4207" s="T1136">v:pred 0.3.h:S</ta>
            <ta e="T1138" id="Seg_4208" s="T1137">v:pred 0.1.h:S</ta>
            <ta e="T1139" id="Seg_4209" s="T1138">pro.h:S</ta>
            <ta e="T1140" id="Seg_4210" s="T1139">pro.h:O</ta>
            <ta e="T1141" id="Seg_4211" s="T1140">v:pred</ta>
            <ta e="T1142" id="Seg_4212" s="T1141">v:pred 0.3.h:S</ta>
            <ta e="T1143" id="Seg_4213" s="T1142">v:pred 0.2.h:S</ta>
            <ta e="T1147" id="Seg_4214" s="T1146">pro.h:O</ta>
            <ta e="T1148" id="Seg_4215" s="T1147">v:pred 0.1.h:S</ta>
            <ta e="T1150" id="Seg_4216" s="T1149">pro.h:S</ta>
            <ta e="T1151" id="Seg_4217" s="T1150">v:pred</ta>
            <ta e="T1153" id="Seg_4218" s="T1152">ptcl:pred</ta>
            <ta e="T1157" id="Seg_4219" s="T1156">np:O</ta>
            <ta e="T1159" id="Seg_4220" s="T1158">v:pred 0.2.h:S</ta>
            <ta e="T1161" id="Seg_4221" s="T1160">pro.h:S</ta>
            <ta e="T1162" id="Seg_4222" s="T1161">v:pred</ta>
            <ta e="T1164" id="Seg_4223" s="T1163">np.h:S</ta>
            <ta e="T1166" id="Seg_4224" s="T1165">v:pred</ta>
            <ta e="T1168" id="Seg_4225" s="T1167">v:pred 0.3.h:S</ta>
            <ta e="T1170" id="Seg_4226" s="T1169">v:pred</ta>
            <ta e="T1171" id="Seg_4227" s="T1170">np.h:S</ta>
            <ta e="T1173" id="Seg_4228" s="T1172">v:pred 0.3.h:S</ta>
            <ta e="T1175" id="Seg_4229" s="T1174">np.h:S</ta>
            <ta e="T1176" id="Seg_4230" s="T1175">v:pred</ta>
            <ta e="T1178" id="Seg_4231" s="T1177">v:pred 0.2.h:S</ta>
            <ta e="T1181" id="Seg_4232" s="T1180">np:O</ta>
            <ta e="T1182" id="Seg_4233" s="T1181">np.h:S</ta>
            <ta e="T1183" id="Seg_4234" s="T1182">v:pred</ta>
            <ta e="T1187" id="Seg_4235" s="T1186">cop</ta>
            <ta e="T1188" id="Seg_4236" s="T1187">n:pred</ta>
            <ta e="T1190" id="Seg_4237" s="T1189">v:pred 0.3:S</ta>
            <ta e="T1194" id="Seg_4238" s="T1193">np:S</ta>
            <ta e="T1195" id="Seg_4239" s="T1194">adj:pred</ta>
            <ta e="T1196" id="Seg_4240" s="T1195">n:pred</ta>
            <ta e="T1198" id="Seg_4241" s="T1197">v:pred 0.1.h:S</ta>
            <ta e="T1199" id="Seg_4242" s="T1198">pro.h:S</ta>
            <ta e="T1200" id="Seg_4243" s="T1199">pro.h:O</ta>
            <ta e="T1201" id="Seg_4244" s="T1200">v:pred</ta>
            <ta e="T1203" id="Seg_4245" s="T1202">ptcl.neg</ta>
            <ta e="T1204" id="Seg_4246" s="T1203">v:pred 0.2.h:S</ta>
            <ta e="T1205" id="Seg_4247" s="T1204">np.h:S</ta>
            <ta e="T1206" id="Seg_4248" s="T1205">ptcl.neg</ta>
            <ta e="T1207" id="Seg_4249" s="T1206">v:pred</ta>
            <ta e="T1209" id="Seg_4250" s="T1208">np.h:S</ta>
            <ta e="T1210" id="Seg_4251" s="T1209">ptcl.neg</ta>
            <ta e="T1211" id="Seg_4252" s="T1210">v:pred</ta>
            <ta e="T1218" id="Seg_4253" s="T1217">v:pred 0.3.h:S</ta>
            <ta e="T1219" id="Seg_4254" s="T1218">v:pred 0.3.h:S</ta>
            <ta e="T1221" id="Seg_4255" s="T1220">np.h:S</ta>
            <ta e="T1222" id="Seg_4256" s="T1221">v:pred</ta>
            <ta e="T1225" id="Seg_4257" s="T1224">v:pred 0.2.h:S</ta>
            <ta e="T1228" id="Seg_4258" s="T1227">pro.h:S</ta>
            <ta e="T1229" id="Seg_4259" s="T1228">v:pred</ta>
            <ta e="T1231" id="Seg_4260" s="T1230">v:pred 0.1.h:S</ta>
            <ta e="T1234" id="Seg_4261" s="T1233">pro.h:O</ta>
            <ta e="T1236" id="Seg_4262" s="T1235">v:pred 0.1.h:S</ta>
            <ta e="T1239" id="Seg_4263" s="T1238">np.h:S</ta>
            <ta e="T1240" id="Seg_4264" s="T1239">v:pred</ta>
            <ta e="T1242" id="Seg_4265" s="T1241">v:pred 0.3.h:S</ta>
            <ta e="T1245" id="Seg_4266" s="T1244">v:pred</ta>
            <ta e="T1246" id="Seg_4267" s="T1245">np.h:S</ta>
            <ta e="T1248" id="Seg_4268" s="T1247">v:pred 0.3.h:S</ta>
            <ta e="T1251" id="Seg_4269" s="T1250">np.h:S</ta>
            <ta e="T1252" id="Seg_4270" s="T1251">v:pred</ta>
            <ta e="T1254" id="Seg_4271" s="T1253">v:pred 0.3.h:S</ta>
            <ta e="T1256" id="Seg_4272" s="T1255">v:pred 0.2.h:S</ta>
            <ta e="T1259" id="Seg_4273" s="T1258">np:S</ta>
            <ta e="T1260" id="Seg_4274" s="T1259">n:pred</ta>
            <ta e="T1261" id="Seg_4275" s="T1260">np.h:S</ta>
            <ta e="T1262" id="Seg_4276" s="T1261">v:pred</ta>
            <ta e="T1263" id="Seg_4277" s="T1262">s:purp</ta>
            <ta e="T1265" id="Seg_4278" s="T1264">pro.h:O</ta>
            <ta e="T1266" id="Seg_4279" s="T1265">v:pred 0.3.h:S</ta>
            <ta e="T1268" id="Seg_4280" s="T1267">np:S</ta>
            <ta e="T1269" id="Seg_4281" s="T1268">n:pred</ta>
            <ta e="T1270" id="Seg_4282" s="T1269">cop</ta>
            <ta e="T1273" id="Seg_4283" s="T1272">v:pred</ta>
            <ta e="T1275" id="Seg_4284" s="T1274">v:pred 0.1.h:S</ta>
            <ta e="T1276" id="Seg_4285" s="T1275">pro.h:S</ta>
            <ta e="T1277" id="Seg_4286" s="T1276">v:pred</ta>
            <ta e="T1278" id="Seg_4287" s="T1277">np.h:S</ta>
            <ta e="T1279" id="Seg_4288" s="T1278">ptcl.neg</ta>
            <ta e="T1280" id="Seg_4289" s="T1279">v:pred</ta>
            <ta e="T1283" id="Seg_4290" s="T1282">np.h:S</ta>
            <ta e="T1285" id="Seg_4291" s="T1284">np.h:S</ta>
            <ta e="T1286" id="Seg_4292" s="T1285">ptcl.neg</ta>
            <ta e="T1287" id="Seg_4293" s="T1286">v:pred</ta>
            <ta e="T1289" id="Seg_4294" s="T1288">pro.h:S</ta>
            <ta e="T1290" id="Seg_4295" s="T1289">ptcl.neg</ta>
            <ta e="T1291" id="Seg_4296" s="T1290">v:pred</ta>
            <ta e="T1292" id="Seg_4297" s="T1291">v:pred 0.1.h:S</ta>
            <ta e="T1294" id="Seg_4298" s="T1293">v:pred 0.3.h:S</ta>
            <ta e="T1296" id="Seg_4299" s="T1295">v:pred 0.3.h:S</ta>
            <ta e="T1297" id="Seg_4300" s="T1296">v:pred 0.2.h:S</ta>
            <ta e="T1301" id="Seg_4301" s="T1300">v:pred 0.2.h:S</ta>
            <ta e="T1307" id="Seg_4302" s="T1306">pro.h:O</ta>
            <ta e="T1308" id="Seg_4303" s="T1307">v:pred 0.1.h:S</ta>
            <ta e="T1310" id="Seg_4304" s="T1309">v:pred 0.1.h:S</ta>
            <ta e="T1311" id="Seg_4305" s="T1310">pro.h:O</ta>
            <ta e="T1314" id="Seg_4306" s="T1313">np:S</ta>
            <ta e="T1315" id="Seg_4307" s="T1314">v:pred</ta>
            <ta e="T1316" id="Seg_4308" s="T1315">np:O</ta>
            <ta e="T1319" id="Seg_4309" s="T1318">v:pred 0.3.h:S</ta>
            <ta e="T1320" id="Seg_4310" s="T1319">v:pred 0.2.h:S</ta>
            <ta e="T1324" id="Seg_4311" s="T1323">pro.h:O</ta>
            <ta e="T1325" id="Seg_4312" s="T1324">v:pred 0.1.h:S</ta>
            <ta e="T1329" id="Seg_4313" s="T1328">v:pred 0.1.h:S</ta>
            <ta e="T1332" id="Seg_4314" s="T1331">np:O</ta>
            <ta e="T1333" id="Seg_4315" s="T1332">v:pred 0.1.h:S</ta>
            <ta e="T1335" id="Seg_4316" s="T1334">pro.h:S</ta>
            <ta e="T1337" id="Seg_4317" s="T1336">v:pred</ta>
            <ta e="T1338" id="Seg_4318" s="T1337">v:pred 0.2.h:S</ta>
            <ta e="T1340" id="Seg_4319" s="T1339">v:pred 0.1.h:S</ta>
            <ta e="T1342" id="Seg_4320" s="T1341">pro.h:S</ta>
            <ta e="T1344" id="Seg_4321" s="T1343">pro.h:O</ta>
            <ta e="T1346" id="Seg_4322" s="T1345">v:pred</ta>
            <ta e="T1348" id="Seg_4323" s="T1347">pro.h:S</ta>
            <ta e="T1349" id="Seg_4324" s="T1348">v:pred</ta>
            <ta e="T1351" id="Seg_4325" s="T1350">v:pred</ta>
            <ta e="T1353" id="Seg_4326" s="T1352">np.h:S</ta>
            <ta e="T1355" id="Seg_4327" s="T1354">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1033" id="Seg_4328" s="T1032">RUS:cult</ta>
            <ta e="T1034" id="Seg_4329" s="T1033">RUS:gram</ta>
            <ta e="T1035" id="Seg_4330" s="T1034">RUS:core</ta>
            <ta e="T1036" id="Seg_4331" s="T1035">RUS:cult</ta>
            <ta e="T1038" id="Seg_4332" s="T1037">TAT:cult</ta>
            <ta e="T1040" id="Seg_4333" s="T1039">RUS:gram</ta>
            <ta e="T1041" id="Seg_4334" s="T1040">RUS:core</ta>
            <ta e="T1048" id="Seg_4335" s="T1047">RUS:cult</ta>
            <ta e="T1049" id="Seg_4336" s="T1048">TAT:cult</ta>
            <ta e="T1050" id="Seg_4337" s="T1049">TURK:disc</ta>
            <ta e="T1055" id="Seg_4338" s="T1054">RUS:core</ta>
            <ta e="T1063" id="Seg_4339" s="T1062">TAT:cult</ta>
            <ta e="T1066" id="Seg_4340" s="T1065">RUS:gram</ta>
            <ta e="T1074" id="Seg_4341" s="T1073">RUS:gram</ta>
            <ta e="T1076" id="Seg_4342" s="T1075">TAT:cult</ta>
            <ta e="T1079" id="Seg_4343" s="T1078">RUS:cult</ta>
            <ta e="T1086" id="Seg_4344" s="T1085">RUS:gram</ta>
            <ta e="T1091" id="Seg_4345" s="T1090">RUS:gram</ta>
            <ta e="T1096" id="Seg_4346" s="T1095">TURK:disc</ta>
            <ta e="T1099" id="Seg_4347" s="T1098">RUS:gram</ta>
            <ta e="T1104" id="Seg_4348" s="T1103">RUS:gram</ta>
            <ta e="T1108" id="Seg_4349" s="T1107">RUS:core</ta>
            <ta e="T1109" id="Seg_4350" s="T1108">RUS:gram</ta>
            <ta e="T1116" id="Seg_4351" s="T1115">RUS:gram</ta>
            <ta e="T1117" id="Seg_4352" s="T1116">RUS:gram</ta>
            <ta e="T1122" id="Seg_4353" s="T1121">TAT:cult</ta>
            <ta e="T1125" id="Seg_4354" s="T1124">RUS:gram</ta>
            <ta e="T1126" id="Seg_4355" s="T1125">RUS:cult</ta>
            <ta e="T1127" id="Seg_4356" s="T1126">TAT:cult</ta>
            <ta e="T1135" id="Seg_4357" s="T1134">RUS:gram</ta>
            <ta e="T1145" id="Seg_4358" s="T1144">RUS:gram</ta>
            <ta e="T1149" id="Seg_4359" s="T1148">RUS:gram</ta>
            <ta e="T1153" id="Seg_4360" s="T1152">RUS:gram</ta>
            <ta e="T1155" id="Seg_4361" s="T1154">RUS:gram</ta>
            <ta e="T1156" id="Seg_4362" s="T1155">TURK:disc</ta>
            <ta e="T1160" id="Seg_4363" s="T1159">RUS:gram</ta>
            <ta e="T1167" id="Seg_4364" s="T1166">RUS:gram</ta>
            <ta e="T1171" id="Seg_4365" s="T1170">RUS:core</ta>
            <ta e="T1172" id="Seg_4366" s="T1171">RUS:gram</ta>
            <ta e="T1179" id="Seg_4367" s="T1178">RUS:gram</ta>
            <ta e="T1181" id="Seg_4368" s="T1180">TAT:cult</ta>
            <ta e="T1182" id="Seg_4369" s="T1181">RUS:cult</ta>
            <ta e="T1184" id="Seg_4370" s="T1183">RUS:gram</ta>
            <ta e="T1189" id="Seg_4371" s="T1188">RUS:gram</ta>
            <ta e="T1191" id="Seg_4372" s="T1190">RUS:gram</ta>
            <ta e="T1194" id="Seg_4373" s="T1193">TAT:cult</ta>
            <ta e="T1195" id="Seg_4374" s="T1194">TURK:core</ta>
            <ta e="T1202" id="Seg_4375" s="T1201">TURK:disc</ta>
            <ta e="T1213" id="Seg_4376" s="T1212">RUS:gram</ta>
            <ta e="T1226" id="Seg_4377" s="T1225">TAT:cult</ta>
            <ta e="T1227" id="Seg_4378" s="T1226">RUS:gram</ta>
            <ta e="T1232" id="Seg_4379" s="T1231">RUS:gram</ta>
            <ta e="T1233" id="Seg_4380" s="T1232">TURK:disc</ta>
            <ta e="T1241" id="Seg_4381" s="T1240">RUS:gram</ta>
            <ta e="T1246" id="Seg_4382" s="T1245">RUS:core</ta>
            <ta e="T1247" id="Seg_4383" s="T1246">RUS:gram</ta>
            <ta e="T1250" id="Seg_4384" s="T1249">RUS:cult</ta>
            <ta e="T1257" id="Seg_4385" s="T1256">RUS:gram</ta>
            <ta e="T1259" id="Seg_4386" s="T1258">TAT:cult</ta>
            <ta e="T1261" id="Seg_4387" s="T1260">RUS:cult</ta>
            <ta e="T1264" id="Seg_4388" s="T1263">RUS:gram</ta>
            <ta e="T1267" id="Seg_4389" s="T1266">RUS:gram</ta>
            <ta e="T1271" id="Seg_4390" s="T1270">RUS:gram</ta>
            <ta e="T1284" id="Seg_4391" s="T1283">RUS:gram</ta>
            <ta e="T1288" id="Seg_4392" s="T1287">RUS:gram</ta>
            <ta e="T1295" id="Seg_4393" s="T1294">TURK:disc</ta>
            <ta e="T1298" id="Seg_4394" s="T1297">RUS:cult</ta>
            <ta e="T1304" id="Seg_4395" s="T1302">RUS:gram</ta>
            <ta e="T1309" id="Seg_4396" s="T1308">TURK:disc</ta>
            <ta e="T1321" id="Seg_4397" s="T1320">RUS:cult</ta>
            <ta e="T1322" id="Seg_4398" s="T1321">RUS:gram</ta>
            <ta e="T1328" id="Seg_4399" s="T1327">TURK:disc</ta>
            <ta e="T1331" id="Seg_4400" s="T1330">TURK:disc</ta>
            <ta e="T1345" id="Seg_4401" s="T1344">TURK:disc</ta>
            <ta e="T1347" id="Seg_4402" s="T1346">RUS:gram</ta>
            <ta e="T1352" id="Seg_4403" s="T1351">RUS:cult</ta>
            <ta e="T1354" id="Seg_4404" s="T1353">RUS:gram</ta>
            <ta e="T1355" id="Seg_4405" s="T1354">RUS:core</ta>
            <ta e="T1356" id="Seg_4406" s="T1355">TAT:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T1071" id="Seg_4407" s="T1070">RUS:calq</ta>
            <ta e="T1120" id="Seg_4408" s="T1115">RUS:calq</ta>
            <ta e="T1128" id="Seg_4409" s="T1127">RUS:int</ta>
            <ta e="T1154" id="Seg_4410" s="T1152">RUS:calq</ta>
            <ta e="T1215" id="Seg_4411" s="T1214">RUS:int</ta>
            <ta e="T1255" id="Seg_4412" s="T1254">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1035" id="Seg_4413" s="T1031">Жили лиса и заяц.</ta>
            <ta e="T1039" id="Seg_4414" s="T1035">У лисы был дом из снега.</ta>
            <ta e="T1044" id="Seg_4415" s="T1039">А у зайца был из дерева.</ta>
            <ta e="T1047" id="Seg_4416" s="T1044">Потом пришло тепло.</ta>
            <ta e="T1051" id="Seg_4417" s="T1047">У лисы дом потёк [=растаял].</ta>
            <ta e="T1055" id="Seg_4418" s="T1051">Тогда она пришла к зайцу.</ta>
            <ta e="T1058" id="Seg_4419" s="T1055">«Пусти меня поспать».</ta>
            <ta e="T1063" id="Seg_4420" s="T1058">[Потом] она сама его из дома выгнала.</ta>
            <ta e="T1067" id="Seg_4421" s="T1063">Он идёт и плачет.</ta>
            <ta e="T1073" id="Seg_4422" s="T1067">Собака говорит: «Что ты плачешь?»</ta>
            <ta e="T1079" id="Seg_4423" s="T1073">«Да меня, говорит, лиса из дома выгнала.»</ta>
            <ta e="T1083" id="Seg_4424" s="T1079">«Пойдём, я её выгоню!»</ta>
            <ta e="T1085" id="Seg_4425" s="T1083">Пришла собака.</ta>
            <ta e="T1088" id="Seg_4426" s="T1085">И всё кричит.</ta>
            <ta e="T1090" id="Seg_4427" s="T1088">«Уходи отсюда!»</ta>
            <ta e="T1101" id="Seg_4428" s="T1090">А она говорит: «Сейчас у тебя шерсть вырву, и ты убежишь».</ta>
            <ta e="T1105" id="Seg_4429" s="T1101">[Собака] испугалась и убежала.</ta>
            <ta e="T1110" id="Seg_4430" s="T1105">Опять идёт заяц и плачет.</ta>
            <ta e="T1113" id="Seg_4431" s="T1110">Потом приходит медведь.</ta>
            <ta e="T1115" id="Seg_4432" s="T1113">«Что ты плачешь?»</ta>
            <ta e="T1120" id="Seg_4433" s="T1115">«Да как мне не плакать.</ta>
            <ta e="T1124" id="Seg_4434" s="T1120">У меня дом был из дерева.</ta>
            <ta e="T1129" id="Seg_4435" s="T1124">А у лисы дом был из снега.</ta>
            <ta e="T1137" id="Seg_4436" s="T1129">Теперь она пришла ко мне переночевать и меня выгнала».</ta>
            <ta e="T1141" id="Seg_4437" s="T1137">«Пойдём, я её выгоню!»</ta>
            <ta e="T1142" id="Seg_4438" s="T1141">Он пришёл.</ta>
            <ta e="T1144" id="Seg_4439" s="T1142">«Уходи отсюда!</ta>
            <ta e="T1148" id="Seg_4440" s="T1144">А то сейчас побью тебя!»</ta>
            <ta e="T1155" id="Seg_4441" s="T1148">А она отвечает: «Я как выбегу.</ta>
            <ta e="T1159" id="Seg_4442" s="T1155">Да шерсть твою вырву.</ta>
            <ta e="T1162" id="Seg_4443" s="T1159">И ты убежишь».</ta>
            <ta e="T1168" id="Seg_4444" s="T1162">Тогда медведь испугался и убежал.</ta>
            <ta e="T1173" id="Seg_4445" s="T1168">Опять идёт зайчик и плачет.</ta>
            <ta e="T1176" id="Seg_4446" s="T1173">Потом идёт бык.</ta>
            <ta e="T1178" id="Seg_4447" s="T1176">«Что ты плачешь?»</ta>
            <ta e="T1183" id="Seg_4448" s="T1178">«Да мой дом лиса отняла.</ta>
            <ta e="T1188" id="Seg_4449" s="T1183">А её [дом] был из снега.</ta>
            <ta e="T1196" id="Seg_4450" s="T1188">И он умер [=растаял], а мой дом хороший, деревянный».</ta>
            <ta e="T1201" id="Seg_4451" s="T1196">Потом [бык говорит]: «Пойдём, я её выгоню».</ta>
            <ta e="T1204" id="Seg_4452" s="T1201">«Нет, не выгонишь.</ta>
            <ta e="T1208" id="Seg_4453" s="T1204">Медведь не смог выгнать.</ta>
            <ta e="T1211" id="Seg_4454" s="T1208">Собака не смогла выгнать.</ta>
            <ta e="T1215" id="Seg_4455" s="T1211">А ты, бык, и вовсе [не сможешь]».</ta>
            <ta e="T1218" id="Seg_4456" s="T1215">Потом пришли.</ta>
            <ta e="T1226" id="Seg_4457" s="T1218">Пришли они, бык говорит: «Выходи из дома!»</ta>
            <ta e="T1236" id="Seg_4458" s="T1226">А она говорит: «Сейчас как выпрыгну, (разорву?) тебя на части».</ta>
            <ta e="T1242" id="Seg_4459" s="T1236">Тогда бык испугался и убежал.</ta>
            <ta e="T1248" id="Seg_4460" s="T1242">Тогда опять идёт заяц и плачет.</ta>
            <ta e="T1253" id="Seg_4461" s="T1248">Потом идёт петух с косой.</ta>
            <ta e="T1256" id="Seg_4462" s="T1253">Говорит: «Что ты плачешь?»</ta>
            <ta e="T1260" id="Seg_4463" s="T1256">«Да у меня дом из дерева.</ta>
            <ta e="T1266" id="Seg_4464" s="T1260">Лисица пришла поспать и меня выгнала.</ta>
            <ta e="T1273" id="Seg_4465" s="T1266">А у неё [дом] из снега был и теперь нету».</ta>
            <ta e="T1277" id="Seg_4466" s="T1273">Потом [петух говорит]: «Пойдём, я выгоню».</ta>
            <ta e="T1281" id="Seg_4467" s="T1277">«Собака не смогла выгнать.</ta>
            <ta e="T1287" id="Seg_4468" s="T1281">Медведь и бык не смогли.</ta>
            <ta e="T1291" id="Seg_4469" s="T1287">И ты не выгонишь».</ta>
            <ta e="T1292" id="Seg_4470" s="T1291">«Пойдём!»</ta>
            <ta e="T1294" id="Seg_4471" s="T1292">Потом они пошли.</ta>
            <ta e="T1296" id="Seg_4472" s="T1294">Он кричит:</ta>
            <ta e="T1302" id="Seg_4473" s="T1296">«Вставай с печи, уходи (отсюда?)!</ta>
            <ta e="T1308" id="Seg_4474" s="T1302">А (то?) сейчас косой тебя убью!</ta>
            <ta e="T1311" id="Seg_4475" s="T1308">Зарублю тебя».</ta>
            <ta e="T1316" id="Seg_4476" s="T1311">«Подожди, сапоги на ноги надеваю».</ta>
            <ta e="T1319" id="Seg_4477" s="T1316">Потом он опять кричит:</ta>
            <ta e="T1325" id="Seg_4478" s="T1319">«Уходи с печи, а то сейчас убью тебя.</ta>
            <ta e="T1329" id="Seg_4479" s="T1325">Косой зарублю».</ta>
            <ta e="T1333" id="Seg_4480" s="T1329">«Подожди, платье надеваю».</ta>
            <ta e="T1338" id="Seg_4481" s="T1333">Потом он опять кричит: «Уходи!»</ta>
            <ta e="T1340" id="Seg_4482" s="T1338">«Уже бегу!»</ta>
            <ta e="T1346" id="Seg_4483" s="T1340">Потом он её косой зарубил.</ta>
            <ta e="T1349" id="Seg_4484" s="T1346">И она умерла.</ta>
            <ta e="T1356" id="Seg_4485" s="T1349">Потом петух и заяц жили в [его] доме.</ta>
            <ta e="T1357" id="Seg_4486" s="T1356">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1035" id="Seg_4487" s="T1031">There lived a fox and a hare.</ta>
            <ta e="T1039" id="Seg_4488" s="T1035">The fox had a house made of snow.</ta>
            <ta e="T1044" id="Seg_4489" s="T1039">And the hare had [a house] made of wood.</ta>
            <ta e="T1047" id="Seg_4490" s="T1044">Then it became warm.</ta>
            <ta e="T1051" id="Seg_4491" s="T1047">The fox's house flowed away [=melted].</ta>
            <ta e="T1055" id="Seg_4492" s="T1051">Then he came to the hare.</ta>
            <ta e="T1058" id="Seg_4493" s="T1055">"Let me sleep."</ta>
            <ta e="T1063" id="Seg_4494" s="T1058">It [the fox] drove it [the hare] out of the house.</ta>
            <ta e="T1067" id="Seg_4495" s="T1063">It comes and is crying.</ta>
            <ta e="T1073" id="Seg_4496" s="T1067">A dog says: "Why are you crying?"</ta>
            <ta e="T1079" id="Seg_4497" s="T1073">"From my house, he says, the fox drove [me] out."</ta>
            <ta e="T1083" id="Seg_4498" s="T1079">"Let’s go, I will drive it out!"</ta>
            <ta e="T1085" id="Seg_4499" s="T1083">The dog came.</ta>
            <ta e="T1088" id="Seg_4500" s="T1085">And keeps shouting.</ta>
            <ta e="T1090" id="Seg_4501" s="T1088">"Go [out] from there!"</ta>
            <ta e="T1101" id="Seg_4502" s="T1090">But it says: "Now I will cut your hair, and you will run away."</ta>
            <ta e="T1105" id="Seg_4503" s="T1101">It got scared and ran away.</ta>
            <ta e="T1110" id="Seg_4504" s="T1105">Again the hare is going and crying.</ta>
            <ta e="T1113" id="Seg_4505" s="T1110">Then a bear comes.</ta>
            <ta e="T1115" id="Seg_4506" s="T1113">"Why are you crying?"</ta>
            <ta e="T1120" id="Seg_4507" s="T1115">"Well how should I not cry.</ta>
            <ta e="T1124" id="Seg_4508" s="T1120">I had a house made of wood.</ta>
            <ta e="T1129" id="Seg_4509" s="T1124">And the fox's house was made of snow.</ta>
            <ta e="T1137" id="Seg_4510" s="T1129">Now it came to me to spend the night and drove me out."</ta>
            <ta e="T1141" id="Seg_4511" s="T1137">"Let's go, I will drive it out!"</ta>
            <ta e="T1142" id="Seg_4512" s="T1141">It came.</ta>
            <ta e="T1144" id="Seg_4513" s="T1142">"Go [out] from there!</ta>
            <ta e="T1148" id="Seg_4514" s="T1144">Or I will beat you!"</ta>
            <ta e="T1155" id="Seg_4515" s="T1148">And it says: "I will run [out now].</ta>
            <ta e="T1159" id="Seg_4516" s="T1155">And I will cut your hair!</ta>
            <ta e="T1162" id="Seg_4517" s="T1159">And you will run away!"</ta>
            <ta e="T1168" id="Seg_4518" s="T1162">Then the bear got scared and ran away.</ta>
            <ta e="T1173" id="Seg_4519" s="T1168">Again the hare is going and crying.</ta>
            <ta e="T1176" id="Seg_4520" s="T1173">Then a bull comes.</ta>
            <ta e="T1178" id="Seg_4521" s="T1176">"Why are you crying?"</ta>
            <ta e="T1183" id="Seg_4522" s="T1178">"Well, the fox took my house.</ta>
            <ta e="T1188" id="Seg_4523" s="T1183">It had [a house] made of snow.</ta>
            <ta e="T1196" id="Seg_4524" s="T1188">And it died [= melted down], and my house was good, made of wood."</ta>
            <ta e="T1201" id="Seg_4525" s="T1196">Then: "Let's go, I will drive it out."</ta>
            <ta e="T1204" id="Seg_4526" s="T1201">"Not, you won't drive [it] out.</ta>
            <ta e="T1208" id="Seg_4527" s="T1204">The bear couldn't drive [it] out.</ta>
            <ta e="T1211" id="Seg_4528" s="T1208">The dog could not.</ta>
            <ta e="T1215" id="Seg_4529" s="T1211">And you, bull, even less."</ta>
            <ta e="T1218" id="Seg_4530" s="T1215">Then they came.</ta>
            <ta e="T1226" id="Seg_4531" s="T1218">They came, the bull says: "Go [out] from the house!"</ta>
            <ta e="T1236" id="Seg_4532" s="T1226">But it says: "Now I will jump, so I will (tear?) you apart."</ta>
            <ta e="T1242" id="Seg_4533" s="T1236">Then the bull got scared and ran away.</ta>
            <ta e="T1248" id="Seg_4534" s="T1242">Then again the hare is going and crying.</ta>
            <ta e="T1253" id="Seg_4535" s="T1248">Then a rooster comes, with a scythe.</ta>
            <ta e="T1256" id="Seg_4536" s="T1253">It says: "Why are you crying?"</ta>
            <ta e="T1260" id="Seg_4537" s="T1256">Well, my house [is] made of wood.</ta>
            <ta e="T1266" id="Seg_4538" s="T1260">The fox came to sleep and drove me out.</ta>
            <ta e="T1273" id="Seg_4539" s="T1266">And it had [a house] made of snow and now it is no [more]."</ta>
            <ta e="T1277" id="Seg_4540" s="T1273">Then: "Let's go, I will drive it out."</ta>
            <ta e="T1281" id="Seg_4541" s="T1277">"The dog could not drive [it] out.</ta>
            <ta e="T1287" id="Seg_4542" s="T1281">The bear and the bull could not.</ta>
            <ta e="T1291" id="Seg_4543" s="T1287">And you will not drive [it] out."</ta>
            <ta e="T1292" id="Seg_4544" s="T1291">"Let's go!"</ta>
            <ta e="T1294" id="Seg_4545" s="T1292">Then it came.</ta>
            <ta e="T1296" id="Seg_4546" s="T1294">It shouts:</ta>
            <ta e="T1302" id="Seg_4547" s="T1296">"Get off from the stove and go out (from there?)!</ta>
            <ta e="T1308" id="Seg_4548" s="T1302">Or I will kill you with a scythe!</ta>
            <ta e="T1311" id="Seg_4549" s="T1308">I will stab you."</ta>
            <ta e="T1316" id="Seg_4550" s="T1311">"Wait, I'm putting on [my] boots."</ta>
            <ta e="T1319" id="Seg_4551" s="T1316">Then it is shouting again:</ta>
            <ta e="T1325" id="Seg_4552" s="T1319">"Get away from the stove, or I will kill you!</ta>
            <ta e="T1329" id="Seg_4553" s="T1325">I will cut [you] with a scythe."</ta>
            <ta e="T1333" id="Seg_4554" s="T1329">"Wait, I’m putting my clothes."</ta>
            <ta e="T1338" id="Seg_4555" s="T1333">Then it is shouting again: "Go [away]!"</ta>
            <ta e="T1340" id="Seg_4556" s="T1338">"I'm already running [away]!".</ta>
            <ta e="T1346" id="Seg_4557" s="T1340">Then it stabbed it with the scythe.</ta>
            <ta e="T1349" id="Seg_4558" s="T1346">And it died.</ta>
            <ta e="T1356" id="Seg_4559" s="T1349">Then the rooster and the hare lived in the house.</ta>
            <ta e="T1357" id="Seg_4560" s="T1356">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1035" id="Seg_4561" s="T1031">Es lebte ein Fuchs und ein Hase.</ta>
            <ta e="T1039" id="Seg_4562" s="T1035">Der Fuchs hat ein Haus aus Schnee.</ta>
            <ta e="T1044" id="Seg_4563" s="T1039">Und der Hase hatte [ein Haus] aus Holz.</ta>
            <ta e="T1047" id="Seg_4564" s="T1044">Dann wurde er warm.</ta>
            <ta e="T1051" id="Seg_4565" s="T1047">Dem Fuchs sein Haus zefloss [=zerschmolz].</ta>
            <ta e="T1055" id="Seg_4566" s="T1051">Dann kam er zum Hasen.</ta>
            <ta e="T1058" id="Seg_4567" s="T1055">„Lass mich schlafen.“</ta>
            <ta e="T1063" id="Seg_4568" s="T1058">Er [der Fuchs] vertrieb ihn [den Hasen] vom Haus.</ta>
            <ta e="T1067" id="Seg_4569" s="T1063">Er kommt und weint.</ta>
            <ta e="T1073" id="Seg_4570" s="T1067">Ein Hund sagt: „Warum weinst du?“</ta>
            <ta e="T1079" id="Seg_4571" s="T1073">„Von meinem Haus, sagt er, der Fuchs hat [mich] vertrieben.“</ta>
            <ta e="T1083" id="Seg_4572" s="T1079">„Gehen wir, ich werde ihn vertreiben!“</ta>
            <ta e="T1085" id="Seg_4573" s="T1083">Der Hund kam.</ta>
            <ta e="T1088" id="Seg_4574" s="T1085">Und rief als.</ta>
            <ta e="T1090" id="Seg_4575" s="T1088">„Geh [hinaus] von da!“</ta>
            <ta e="T1101" id="Seg_4576" s="T1090">Aber er sagt: „Nun werde ich die die Haare schneiden, und du wirst weg laufen.“</ta>
            <ta e="T1105" id="Seg_4577" s="T1101">Er bekam Angst und lief weg.</ta>
            <ta e="T1110" id="Seg_4578" s="T1105">Wieder geht der Hase und weint.</ta>
            <ta e="T1113" id="Seg_4579" s="T1110">Dann kommt ein Bär.</ta>
            <ta e="T1115" id="Seg_4580" s="T1113">„Warum weinst du?“</ta>
            <ta e="T1120" id="Seg_4581" s="T1115">„Also, warum sollte ich nicht weinen.</ta>
            <ta e="T1124" id="Seg_4582" s="T1120">Ich hatte ein Haus aus Holz.</ta>
            <ta e="T1129" id="Seg_4583" s="T1124">Und dem Fuchs sein Haus war aus Schnee.</ta>
            <ta e="T1137" id="Seg_4584" s="T1129">Nun ist er zu mir gekommen um zu Übernachten und hat mich vertrieben.“</ta>
            <ta e="T1141" id="Seg_4585" s="T1137">„Gehen wir, ich werde ihn vertreiben!“</ta>
            <ta e="T1142" id="Seg_4586" s="T1141">Er kam.</ta>
            <ta e="T1144" id="Seg_4587" s="T1142">„Geh [raus] von da!</ta>
            <ta e="T1148" id="Seg_4588" s="T1144">Oder ich schlage dich!“</ta>
            <ta e="T1155" id="Seg_4589" s="T1148">Und er sagt: „Ich werde rennen [hinaus jetzt].</ta>
            <ta e="T1159" id="Seg_4590" s="T1155">Und ich werde dir die Haare schneiden!</ta>
            <ta e="T1162" id="Seg_4591" s="T1159">Und du wirst weglaufen!“</ta>
            <ta e="T1168" id="Seg_4592" s="T1162">Dann bekam der Bär Angst und lief weg.</ta>
            <ta e="T1173" id="Seg_4593" s="T1168">Wieder geht der Hase und weint.</ta>
            <ta e="T1176" id="Seg_4594" s="T1173">Dann kommt ein Bulle.</ta>
            <ta e="T1178" id="Seg_4595" s="T1176">„Warum weinst du?“</ta>
            <ta e="T1183" id="Seg_4596" s="T1178">„Also, der Fuchs hat mir mein Haus genommen.</ta>
            <ta e="T1188" id="Seg_4597" s="T1183">Er hatte [ein Haus] aus Schnee.</ta>
            <ta e="T1196" id="Seg_4598" s="T1188">Und es starb [= schmolz nieder], und mein Haus war gut, aus Holz gemacht.“</ta>
            <ta e="T1201" id="Seg_4599" s="T1196">Dann: „Gehen wir, ich werde ihn vertreiben.“</ta>
            <ta e="T1204" id="Seg_4600" s="T1201">„Nicht, du wirst [ihn] nicht vertreiben.</ta>
            <ta e="T1208" id="Seg_4601" s="T1204">Der Bär konnte [ihn] nicht vertreiben.</ta>
            <ta e="T1211" id="Seg_4602" s="T1208">Der Hund konnte nicht.</ta>
            <ta e="T1215" id="Seg_4603" s="T1211">Und du, Bulle, umso weniger.“</ta>
            <ta e="T1218" id="Seg_4604" s="T1215">Dann kamen sie.</ta>
            <ta e="T1226" id="Seg_4605" s="T1218">Sie kamen, der Bulle sagt: „Geh [raus] aus dem Haus!"</ta>
            <ta e="T1236" id="Seg_4606" s="T1226">Aber er sagt: „Nun werde ich springen, und werde dich (auseinanderreißen?).</ta>
            <ta e="T1242" id="Seg_4607" s="T1236">Dann bekam der Bulle Angst und lief weg.</ta>
            <ta e="T1248" id="Seg_4608" s="T1242">Dann geht der Hase wieder und weint.</ta>
            <ta e="T1253" id="Seg_4609" s="T1248">Dann kommt ein Hahn, mit einer Sense.</ta>
            <ta e="T1256" id="Seg_4610" s="T1253">Er sagt: „Warum weinst du?“</ta>
            <ta e="T1260" id="Seg_4611" s="T1256">Also, mein Haus [ist] aus Haus gemacht.</ta>
            <ta e="T1266" id="Seg_4612" s="T1260">Der Fuchs kam um zu schlafen und vertrieb mich.</ta>
            <ta e="T1273" id="Seg_4613" s="T1266">Und er hatte [ein Haus] aus Schnee und nun ist es nicht [mehr da].“</ta>
            <ta e="T1277" id="Seg_4614" s="T1273">Dann: „Gehen wir, ich werde ihn vertreiben.“</ta>
            <ta e="T1281" id="Seg_4615" s="T1277">„Der Hund konnte [ihn] nicht vertreiben.</ta>
            <ta e="T1287" id="Seg_4616" s="T1281">Der Bär und der Bulle konnten nicht.</ta>
            <ta e="T1291" id="Seg_4617" s="T1287">Und du wirst [ihn] nicht vertreiben.“</ta>
            <ta e="T1292" id="Seg_4618" s="T1291">“Gehen wir!”</ta>
            <ta e="T1294" id="Seg_4619" s="T1292">Dann kam er.</ta>
            <ta e="T1296" id="Seg_4620" s="T1294">Er ruft:</ta>
            <ta e="T1302" id="Seg_4621" s="T1296">„Steh auf vom Ofen und geh raus (von da?)!</ta>
            <ta e="T1308" id="Seg_4622" s="T1302">Oder ich töte dich mit einer Sense!</ta>
            <ta e="T1311" id="Seg_4623" s="T1308">Ich werde dich erstechen.“</ta>
            <ta e="T1316" id="Seg_4624" s="T1311">„Warte, ich ziehe mir [meine] Stiefel an.“</ta>
            <ta e="T1319" id="Seg_4625" s="T1316">Dann ruft er wieder:</ta>
            <ta e="T1325" id="Seg_4626" s="T1319">„Geh vom Ofen, oder ich töte dich!</ta>
            <ta e="T1329" id="Seg_4627" s="T1325">Ich werde [dich] mit einer Sense schneiden.“</ta>
            <ta e="T1333" id="Seg_4628" s="T1329">„Warte, ich ziehe mich an.“</ta>
            <ta e="T1338" id="Seg_4629" s="T1333">Dann ruft er wieder: „Geh [weg]!“</ta>
            <ta e="T1340" id="Seg_4630" s="T1338">„Ich laufe schon [weg]!“</ta>
            <ta e="T1346" id="Seg_4631" s="T1340">Dann stach er mit der Sense.</ta>
            <ta e="T1349" id="Seg_4632" s="T1346">Und er starb.</ta>
            <ta e="T1356" id="Seg_4633" s="T1349">Dann lebten der Hahn und der Hase in dem Haus.</ta>
            <ta e="T1357" id="Seg_4634" s="T1356">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1035" id="Seg_4635" s="T1031">[GVY:] http://hyaenidae.narod.ru/story1/002.html</ta>
            <ta e="T1159" id="Seg_4636" s="T1155">[GVY:] niŋgəluʔləl = niŋgəluʔləm [-1SG]?</ta>
            <ta e="T1201" id="Seg_4637" s="T1196">[GVY:] sürerim pronounced with š.</ta>
            <ta e="T1308" id="Seg_4638" s="T1302">Šapkə - ’scythe’. [AAV:] "A to"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
