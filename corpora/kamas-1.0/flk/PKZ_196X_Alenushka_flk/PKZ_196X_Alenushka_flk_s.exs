<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3A5BA04D-9279-B71D-DB4E-D9C9236CABE2">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Alenushka_flk.wav" />
         <referenced-file url="PKZ_196X_Alenushka_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Alenushka_flk\PKZ_196X_Alenushka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">196</ud-information>
            <ud-information attribute-name="# HIAT:w">134</ud-information>
            <ud-information attribute-name="# e">130</ud-information>
            <ud-information attribute-name="# HIAT:u">31</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.009" type="appl" />
         <tli id="T1" time="0.894" type="appl" />
         <tli id="T2" time="1.779" type="appl" />
         <tli id="T3" time="2.663" type="appl" />
         <tli id="T4" time="3.548" type="appl" />
         <tli id="T5" time="4.593266906150991" />
         <tli id="T6" time="5.646" type="appl" />
         <tli id="T7" time="6.564" type="appl" />
         <tli id="T8" time="7.546557529409176" />
         <tli id="T9" time="8.429" type="appl" />
         <tli id="T10" time="9.162" type="appl" />
         <tli id="T11" time="9.896" type="appl" />
         <tli id="T12" time="10.582333938936145" />
         <tli id="T13" time="11.395" type="appl" />
         <tli id="T14" time="12.161" type="appl" />
         <tli id="T15" time="12.927" type="appl" />
         <tli id="T16" time="15.23311303418725" />
         <tli id="T17" time="16.122" type="appl" />
         <tli id="T18" time="16.623" type="appl" />
         <tli id="T19" time="17.124" type="appl" />
         <tli id="T20" time="17.625" type="appl" />
         <tli id="T21" time="18.126" type="appl" />
         <tli id="T22" time="18.627" type="appl" />
         <tli id="T23" time="19.128" type="appl" />
         <tli id="T24" time="19.629" type="appl" />
         <tli id="T25" time="20.522" type="appl" />
         <tli id="T26" time="21.414" type="appl" />
         <tli id="T27" time="23.82632209373533" />
         <tli id="T28" time="24.74" type="appl" />
         <tli id="T29" time="25.488" type="appl" />
         <tli id="T30" time="26.236" type="appl" />
         <tli id="T31" time="26.984" type="appl" />
         <tli id="T32" time="28.766250653180737" />
         <tli id="T33" time="29.723" type="appl" />
         <tli id="T34" time="30.505" type="appl" />
         <tli id="T35" time="31.287" type="appl" />
         <tli id="T36" time="32.07" type="appl" />
         <tli id="T37" time="32.852" type="appl" />
         <tli id="T38" time="33.634" type="appl" />
         <tli id="T39" time="35.01282698273586" />
         <tli id="T40" time="35.651" type="appl" />
         <tli id="T41" time="36.13" type="appl" />
         <tli id="T42" time="36.609" type="appl" />
         <tli id="T43" time="37.087" type="appl" />
         <tli id="T44" time="37.566" type="appl" />
         <tli id="T45" time="38.045" type="appl" />
         <tli id="T46" time="38.524" type="appl" />
         <tli id="T47" time="39.532" type="appl" />
         <tli id="T48" time="42.61271707419033" />
         <tli id="T49" time="44.19" type="appl" />
         <tli id="T50" time="45.077" type="appl" />
         <tli id="T51" time="45.963" type="appl" />
         <tli id="T52" time="46.952" type="appl" />
         <tli id="T53" time="47.941" type="appl" />
         <tli id="T54" time="48.93" type="appl" />
         <tli id="T55" time="49.426" type="appl" />
         <tli id="T56" time="49.922" type="appl" />
         <tli id="T57" time="50.418" type="appl" />
         <tli id="T58" time="50.914" type="appl" />
         <tli id="T59" time="51.65925291112341" />
         <tli id="T60" time="52.818" type="appl" />
         <tli id="T61" time="53.762" type="appl" />
         <tli id="T62" time="54.905" type="appl" />
         <tli id="T63" time="55.686" type="appl" />
         <tli id="T64" time="57.04584167769817" />
         <tli id="T65" time="58.008" type="appl" />
         <tli id="T66" time="58.608" type="appl" />
         <tli id="T67" time="59.207" type="appl" />
         <tli id="T68" time="59.807" type="appl" />
         <tli id="T69" time="60.406" type="appl" />
         <tli id="T70" time="61.006" type="appl" />
         <tli id="T71" time="61.68577457563879" />
         <tli id="T72" time="62.292" type="appl" />
         <tli id="T73" time="62.773" type="appl" />
         <tli id="T74" time="63.254" type="appl" />
         <tli id="T75" time="63.736" type="appl" />
         <tli id="T76" time="64.218" type="appl" />
         <tli id="T77" time="64.88572829835645" />
         <tli id="T78" time="65.55" type="appl" />
         <tli id="T79" time="66.214" type="appl" />
         <tli id="T80" time="66.879" type="appl" />
         <tli id="T81" time="67.53235668985421" />
         <tli id="T82" time="68.371" type="appl" />
         <tli id="T83" time="69.06" type="appl" />
         <tli id="T84" time="69.748" type="appl" />
         <tli id="T85" time="71.5656316945296" />
         <tli id="T86" time="72.629" type="appl" />
         <tli id="T87" time="73.548" type="appl" />
         <tli id="T88" time="74.468" type="appl" />
         <tli id="T89" time="75.387" type="appl" />
         <tli id="T90" time="76.307" type="appl" />
         <tli id="T91" time="77.226" type="appl" />
         <tli id="T92" time="78.38553306607163" />
         <tli id="T93" time="79.458" type="appl" />
         <tli id="T94" time="80.476" type="appl" />
         <tli id="T95" time="81.493" type="appl" />
         <tli id="T96" time="82.51" type="appl" />
         <tli id="T97" time="83.528" type="appl" />
         <tli id="T98" time="84.65210910639375" />
         <tli id="T99" time="85.282" type="appl" />
         <tli id="T100" time="86.019" type="appl" />
         <tli id="T101" time="86.756" type="appl" />
         <tli id="T102" time="87.457" type="appl" />
         <tli id="T103" time="88.158" type="appl" />
         <tli id="T104" time="88.86" type="appl" />
         <tli id="T130" time="89.29658579435683" type="intp" />
         <tli id="T105" time="89.8787001868326" />
         <tli id="T106" time="90.83" type="appl" />
         <tli id="T107" time="91.42" type="appl" />
         <tli id="T108" time="94.02530688585425" />
         <tli id="T109" time="94.949" type="appl" />
         <tli id="T110" time="95.854" type="appl" />
         <tli id="T111" time="97.91858391516077" />
         <tli id="T112" time="98.566" type="appl" />
         <tli id="T113" time="99.174" type="appl" />
         <tli id="T114" time="100.31188263694335" />
         <tli id="T115" time="101.156" type="appl" />
         <tli id="T116" time="101.985" type="appl" />
         <tli id="T117" time="102.813" type="appl" />
         <tli id="T118" time="103.642" type="appl" />
         <tli id="T119" time="104.66515301389052" />
         <tli id="T120" time="105.365" type="appl" />
         <tli id="T121" time="106.105" type="appl" />
         <tli id="T122" time="106.844" type="appl" />
         <tli id="T123" time="107.584" type="appl" />
         <tli id="T124" time="108.324" type="appl" />
         <tli id="T125" time="109.19842078774056" />
         <tli id="T126" time="110.074" type="appl" />
         <tli id="T127" time="110.82" type="appl" />
         <tli id="T128" time="111.71171777412506" />
         <tli id="T129" time="113.625" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T30" start="T29">
            <tli id="T29.tx.1" />
         </timeline-fork>
         <timeline-fork end="T44" start="T43">
            <tli id="T43.tx.1" />
         </timeline-fork>
         <timeline-fork end="T57" start="T56">
            <tli id="T56.tx.1" />
         </timeline-fork>
         <timeline-fork end="T69" start="T68">
            <tli id="T68.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T129" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">šidegöʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Alʼonuška</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Ivanuška</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Dĭgəttə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">amnobiʔi</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">amnobiʔi</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">Dĭzeŋ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">ĭmbidə</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">naga</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">amzittə</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Dĭgəttə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">kambiʔi</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">šobiʔi</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">šobiʔi</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_65" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">Dĭ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">nʼi</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">măndə:</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_75" n="HIAT:ip">"</nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">Măn</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">ugaːndə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">bü</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">bĭssittə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">axota</ts>
                  <nts id="Seg_90" n="HIAT:ip">"</nts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_94" n="HIAT:u" s="T24">
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">Iʔ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">bĭdeʔ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">dön</ts>
                  <nts id="Seg_104" n="HIAT:ip">!</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_107" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Šobi</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">tüžöj</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29.tx.1" id="Seg_116" n="HIAT:w" s="T29">a</ts>
                  <nts id="Seg_117" n="HIAT:ip">_</nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29.tx.1">to</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">tüžöj</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">molal</ts>
                  <nts id="Seg_126" n="HIAT:ip">!</nts>
                  <nts id="Seg_127" n="HIAT:ip">"</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_130" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">Dĭgəttə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">kambiʔi</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_139" n="HIAT:w" s="T34">kambiʔi</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_143" n="HIAT:w" s="T35">dĭgəttə</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_146" n="HIAT:w" s="T36">ularən</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">üjüt</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">nulaʔbə</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_156" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_158" n="HIAT:w" s="T39">Dĭ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_161" n="HIAT:w" s="T40">măndə:</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_165" n="HIAT:w" s="T41">Iʔ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">bĭdeʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip">,</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.1" id="Seg_172" n="HIAT:w" s="T43">a</ts>
                  <nts id="Seg_173" n="HIAT:ip">_</nts>
                  <ts e="T44" id="Seg_175" n="HIAT:w" s="T43.tx.1">to</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_178" n="HIAT:w" s="T44">ular</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_181" n="HIAT:w" s="T45">molal</ts>
                  <nts id="Seg_182" n="HIAT:ip">!</nts>
                  <nts id="Seg_183" n="HIAT:ip">"</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_186" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_188" n="HIAT:w" s="T46">Dĭgəttə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_191" n="HIAT:w" s="T47">kanlaʔbəʔjə</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_195" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_197" n="HIAT:w" s="T48">Inen</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_200" n="HIAT:w" s="T49">üjüt</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_203" n="HIAT:w" s="T50">nulaʔbə</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_207" n="HIAT:u" s="T51">
                  <nts id="Seg_208" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">Ugaːndə</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">bĭssittə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_216" n="HIAT:w" s="T53">axota</ts>
                  <nts id="Seg_217" n="HIAT:ip">!</nts>
                  <nts id="Seg_218" n="HIAT:ip">"</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_221" n="HIAT:u" s="T54">
                  <nts id="Seg_222" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_224" n="HIAT:w" s="T54">Iʔ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_227" n="HIAT:w" s="T55">bĭdeʔ</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56.tx.1" id="Seg_231" n="HIAT:w" s="T56">a</ts>
                  <nts id="Seg_232" n="HIAT:ip">_</nts>
                  <ts e="T57" id="Seg_234" n="HIAT:w" s="T56.tx.1">to</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">ine</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">molal</ts>
                  <nts id="Seg_241" n="HIAT:ip">!</nts>
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_245" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_247" n="HIAT:w" s="T59">Dĭgəttə</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_250" n="HIAT:w" s="T60">šonəgaʔi</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_254" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_256" n="HIAT:w" s="T61">Poʔton</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_259" n="HIAT:w" s="T62">üjüt</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_262" n="HIAT:w" s="T63">nulaʔbə</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_266" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_268" n="HIAT:w" s="T64">Dĭ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_271" n="HIAT:w" s="T65">măndə:</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_273" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_275" n="HIAT:w" s="T66">Iʔ</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_278" n="HIAT:w" s="T67">bĭdeʔ</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68.tx.1" id="Seg_282" n="HIAT:w" s="T68">a</ts>
                  <nts id="Seg_283" n="HIAT:ip">_</nts>
                  <ts e="T69" id="Seg_285" n="HIAT:w" s="T68.tx.1">to</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_288" n="HIAT:w" s="T69">poʔtobə</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_291" n="HIAT:w" s="T70">molal</ts>
                  <nts id="Seg_292" n="HIAT:ip">"</nts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_296" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_298" n="HIAT:w" s="T71">Dĭ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_301" n="HIAT:w" s="T72">bar</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_304" n="HIAT:w" s="T73">bĭʔpi</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_307" n="HIAT:w" s="T74">i</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_310" n="HIAT:w" s="T75">poʔto</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_313" n="HIAT:w" s="T76">molaːmbi</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_317" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_319" n="HIAT:w" s="T77">Dĭgəttə</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">Alʼonuška</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">dʼorbi</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_329" n="HIAT:w" s="T80">dʼorbi</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_333" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_335" n="HIAT:w" s="T81">Kambiʔi</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_339" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_341" n="HIAT:w" s="T82">Dĭgəttə</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_344" n="HIAT:w" s="T83">šonəga</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_347" n="HIAT:w" s="T84">kuza</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_351" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_353" n="HIAT:w" s="T85">Dĭ</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_356" n="HIAT:w" s="T86">Alʼonuška</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_359" n="HIAT:w" s="T87">iluʔpi</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_362" n="HIAT:w" s="T88">tibinə</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_365" n="HIAT:w" s="T89">i</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_368" n="HIAT:w" s="T90">maːndə</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_371" n="HIAT:w" s="T91">kunlaʔpi</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_375" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_377" n="HIAT:w" s="T92">Nu</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_379" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_381" n="HIAT:w" s="T93">de-</ts>
                  <nts id="Seg_382" n="HIAT:ip">)</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_385" n="HIAT:w" s="T94">dĭ</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_388" n="HIAT:w" s="T95">Vanʼuška</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_391" n="HIAT:w" s="T96">mobi</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_394" n="HIAT:w" s="T97">poʔto</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_398" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_400" n="HIAT:w" s="T98">Dĭm</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_403" n="HIAT:w" s="T99">tože</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_406" n="HIAT:w" s="T100">bădlaʔbəʔjə</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_410" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_412" n="HIAT:w" s="T101">Dĭgəttə</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_415" n="HIAT:w" s="T102">dĭ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_418" n="HIAT:w" s="T103">saməjzittə</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_421" n="HIAT:w" s="T104">kalla</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_424" n="HIAT:w" s="T130">dʼürbi</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_428" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_430" n="HIAT:w" s="T105">Baška</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_433" n="HIAT:w" s="T106">ne</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_436" n="HIAT:w" s="T107">šobi</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_440" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_442" n="HIAT:w" s="T108">Dĭm</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_445" n="HIAT:w" s="T109">baʔluʔpi</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_448" n="HIAT:w" s="T110">bünə</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_452" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_454" n="HIAT:w" s="T111">I</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_457" n="HIAT:w" s="T112">šobi</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_460" n="HIAT:w" s="T113">tibi</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_464" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_466" n="HIAT:w" s="T114">Măndə:</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_470" n="HIAT:w" s="T115">Nada</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_473" n="HIAT:w" s="T116">kuʔsʼittə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_476" n="HIAT:w" s="T117">dĭ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_479" n="HIAT:w" s="T118">poʔto</ts>
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_484" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_486" n="HIAT:w" s="T119">Dĭgəttə</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_489" n="HIAT:w" s="T120">dĭ</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_492" n="HIAT:w" s="T121">kambi</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_495" n="HIAT:w" s="T122">bünə</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_499" n="HIAT:w" s="T123">dʼorlaʔbə</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_503" n="HIAT:w" s="T124">dʼorlaʔbə</ts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_507" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_509" n="HIAT:w" s="T125">Măna</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_512" n="HIAT:w" s="T126">băʔsittə</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_515" n="HIAT:w" s="T127">xočet</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_519" n="HIAT:u" s="T128">
                  <nts id="Seg_520" n="HIAT:ip">(</nts>
                  <ts e="T129" id="Seg_522" n="HIAT:w" s="T128">Mĭnzərzittə</ts>
                  <nts id="Seg_523" n="HIAT:ip">)</nts>
                  <nts id="Seg_524" n="HIAT:ip">"</nts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T129" id="Seg_527" n="sc" s="T0">
               <ts e="T1" id="Seg_529" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_531" n="e" s="T1">šidegöʔ </ts>
               <ts e="T3" id="Seg_533" n="e" s="T2">Alʼonuška </ts>
               <ts e="T4" id="Seg_535" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_537" n="e" s="T4">Ivanuška. </ts>
               <ts e="T6" id="Seg_539" n="e" s="T5">Dĭgəttə </ts>
               <ts e="T7" id="Seg_541" n="e" s="T6">amnobiʔi, </ts>
               <ts e="T8" id="Seg_543" n="e" s="T7">amnobiʔi. </ts>
               <ts e="T9" id="Seg_545" n="e" s="T8">Dĭzeŋ </ts>
               <ts e="T10" id="Seg_547" n="e" s="T9">ĭmbidə </ts>
               <ts e="T11" id="Seg_549" n="e" s="T10">naga </ts>
               <ts e="T12" id="Seg_551" n="e" s="T11">amzittə. </ts>
               <ts e="T13" id="Seg_553" n="e" s="T12">Dĭgəttə </ts>
               <ts e="T14" id="Seg_555" n="e" s="T13">kambiʔi, </ts>
               <ts e="T15" id="Seg_557" n="e" s="T14">šobiʔi, </ts>
               <ts e="T16" id="Seg_559" n="e" s="T15">šobiʔi. </ts>
               <ts e="T17" id="Seg_561" n="e" s="T16">Dĭ </ts>
               <ts e="T18" id="Seg_563" n="e" s="T17">nʼi </ts>
               <ts e="T19" id="Seg_565" n="e" s="T18">măndə: </ts>
               <ts e="T20" id="Seg_567" n="e" s="T19">"Măn </ts>
               <ts e="T21" id="Seg_569" n="e" s="T20">ugaːndə </ts>
               <ts e="T22" id="Seg_571" n="e" s="T21">bü </ts>
               <ts e="T23" id="Seg_573" n="e" s="T22">bĭssittə </ts>
               <ts e="T24" id="Seg_575" n="e" s="T23">axota". </ts>
               <ts e="T25" id="Seg_577" n="e" s="T24">"Iʔ </ts>
               <ts e="T26" id="Seg_579" n="e" s="T25">bĭdeʔ </ts>
               <ts e="T27" id="Seg_581" n="e" s="T26">dön! </ts>
               <ts e="T28" id="Seg_583" n="e" s="T27">Šobi </ts>
               <ts e="T29" id="Seg_585" n="e" s="T28">tüžöj, </ts>
               <ts e="T30" id="Seg_587" n="e" s="T29">a_to </ts>
               <ts e="T31" id="Seg_589" n="e" s="T30">tüžöj </ts>
               <ts e="T32" id="Seg_591" n="e" s="T31">molal!" </ts>
               <ts e="T33" id="Seg_593" n="e" s="T32">Dĭgəttə </ts>
               <ts e="T34" id="Seg_595" n="e" s="T33">kambiʔi, </ts>
               <ts e="T35" id="Seg_597" n="e" s="T34">kambiʔi, </ts>
               <ts e="T36" id="Seg_599" n="e" s="T35">dĭgəttə </ts>
               <ts e="T37" id="Seg_601" n="e" s="T36">ularən </ts>
               <ts e="T38" id="Seg_603" n="e" s="T37">üjüt </ts>
               <ts e="T39" id="Seg_605" n="e" s="T38">nulaʔbə. </ts>
               <ts e="T40" id="Seg_607" n="e" s="T39">Dĭ </ts>
               <ts e="T41" id="Seg_609" n="e" s="T40">măndə: </ts>
               <ts e="T42" id="Seg_611" n="e" s="T41">"Iʔ </ts>
               <ts e="T43" id="Seg_613" n="e" s="T42">bĭdeʔ, </ts>
               <ts e="T44" id="Seg_615" n="e" s="T43">a_to </ts>
               <ts e="T45" id="Seg_617" n="e" s="T44">ular </ts>
               <ts e="T46" id="Seg_619" n="e" s="T45">molal!" </ts>
               <ts e="T47" id="Seg_621" n="e" s="T46">Dĭgəttə </ts>
               <ts e="T48" id="Seg_623" n="e" s="T47">kanlaʔbəʔjə. </ts>
               <ts e="T49" id="Seg_625" n="e" s="T48">Inen </ts>
               <ts e="T50" id="Seg_627" n="e" s="T49">üjüt </ts>
               <ts e="T51" id="Seg_629" n="e" s="T50">nulaʔbə. </ts>
               <ts e="T52" id="Seg_631" n="e" s="T51">"Ugaːndə </ts>
               <ts e="T53" id="Seg_633" n="e" s="T52">bĭssittə </ts>
               <ts e="T54" id="Seg_635" n="e" s="T53">axota!" </ts>
               <ts e="T55" id="Seg_637" n="e" s="T54">"Iʔ </ts>
               <ts e="T56" id="Seg_639" n="e" s="T55">bĭdeʔ, </ts>
               <ts e="T57" id="Seg_641" n="e" s="T56">a_to </ts>
               <ts e="T58" id="Seg_643" n="e" s="T57">ine </ts>
               <ts e="T59" id="Seg_645" n="e" s="T58">molal!" </ts>
               <ts e="T60" id="Seg_647" n="e" s="T59">Dĭgəttə </ts>
               <ts e="T61" id="Seg_649" n="e" s="T60">šonəgaʔi. </ts>
               <ts e="T62" id="Seg_651" n="e" s="T61">Poʔton </ts>
               <ts e="T63" id="Seg_653" n="e" s="T62">üjüt </ts>
               <ts e="T64" id="Seg_655" n="e" s="T63">nulaʔbə. </ts>
               <ts e="T65" id="Seg_657" n="e" s="T64">Dĭ </ts>
               <ts e="T66" id="Seg_659" n="e" s="T65">măndə: </ts>
               <ts e="T67" id="Seg_661" n="e" s="T66">"Iʔ </ts>
               <ts e="T68" id="Seg_663" n="e" s="T67">bĭdeʔ, </ts>
               <ts e="T69" id="Seg_665" n="e" s="T68">a_to </ts>
               <ts e="T70" id="Seg_667" n="e" s="T69">poʔtobə </ts>
               <ts e="T71" id="Seg_669" n="e" s="T70">molal". </ts>
               <ts e="T72" id="Seg_671" n="e" s="T71">Dĭ </ts>
               <ts e="T73" id="Seg_673" n="e" s="T72">bar </ts>
               <ts e="T74" id="Seg_675" n="e" s="T73">bĭʔpi </ts>
               <ts e="T75" id="Seg_677" n="e" s="T74">i </ts>
               <ts e="T76" id="Seg_679" n="e" s="T75">poʔto </ts>
               <ts e="T77" id="Seg_681" n="e" s="T76">molaːmbi. </ts>
               <ts e="T78" id="Seg_683" n="e" s="T77">Dĭgəttə </ts>
               <ts e="T79" id="Seg_685" n="e" s="T78">Alʼonuška </ts>
               <ts e="T80" id="Seg_687" n="e" s="T79">dʼorbi, </ts>
               <ts e="T81" id="Seg_689" n="e" s="T80">dʼorbi. </ts>
               <ts e="T82" id="Seg_691" n="e" s="T81">Kambiʔi. </ts>
               <ts e="T83" id="Seg_693" n="e" s="T82">Dĭgəttə </ts>
               <ts e="T84" id="Seg_695" n="e" s="T83">šonəga </ts>
               <ts e="T85" id="Seg_697" n="e" s="T84">kuza. </ts>
               <ts e="T86" id="Seg_699" n="e" s="T85">Dĭ </ts>
               <ts e="T87" id="Seg_701" n="e" s="T86">Alʼonuška </ts>
               <ts e="T88" id="Seg_703" n="e" s="T87">iluʔpi </ts>
               <ts e="T89" id="Seg_705" n="e" s="T88">tibinə </ts>
               <ts e="T90" id="Seg_707" n="e" s="T89">i </ts>
               <ts e="T91" id="Seg_709" n="e" s="T90">maːndə </ts>
               <ts e="T92" id="Seg_711" n="e" s="T91">kunlaʔpi. </ts>
               <ts e="T93" id="Seg_713" n="e" s="T92">Nu </ts>
               <ts e="T94" id="Seg_715" n="e" s="T93">(de-) </ts>
               <ts e="T95" id="Seg_717" n="e" s="T94">dĭ </ts>
               <ts e="T96" id="Seg_719" n="e" s="T95">Vanʼuška </ts>
               <ts e="T97" id="Seg_721" n="e" s="T96">mobi </ts>
               <ts e="T98" id="Seg_723" n="e" s="T97">poʔto. </ts>
               <ts e="T99" id="Seg_725" n="e" s="T98">Dĭm </ts>
               <ts e="T100" id="Seg_727" n="e" s="T99">tože </ts>
               <ts e="T101" id="Seg_729" n="e" s="T100">bădlaʔbəʔjə. </ts>
               <ts e="T102" id="Seg_731" n="e" s="T101">Dĭgəttə </ts>
               <ts e="T103" id="Seg_733" n="e" s="T102">dĭ </ts>
               <ts e="T104" id="Seg_735" n="e" s="T103">saməjzittə </ts>
               <ts e="T130" id="Seg_737" n="e" s="T104">kalla </ts>
               <ts e="T105" id="Seg_739" n="e" s="T130">dʼürbi. </ts>
               <ts e="T106" id="Seg_741" n="e" s="T105">Baška </ts>
               <ts e="T107" id="Seg_743" n="e" s="T106">ne </ts>
               <ts e="T108" id="Seg_745" n="e" s="T107">šobi. </ts>
               <ts e="T109" id="Seg_747" n="e" s="T108">Dĭm </ts>
               <ts e="T110" id="Seg_749" n="e" s="T109">baʔluʔpi </ts>
               <ts e="T111" id="Seg_751" n="e" s="T110">bünə. </ts>
               <ts e="T112" id="Seg_753" n="e" s="T111">I </ts>
               <ts e="T113" id="Seg_755" n="e" s="T112">šobi </ts>
               <ts e="T114" id="Seg_757" n="e" s="T113">tibi. </ts>
               <ts e="T115" id="Seg_759" n="e" s="T114">Măndə: </ts>
               <ts e="T116" id="Seg_761" n="e" s="T115">"Nada </ts>
               <ts e="T117" id="Seg_763" n="e" s="T116">kuʔsʼittə </ts>
               <ts e="T118" id="Seg_765" n="e" s="T117">dĭ </ts>
               <ts e="T119" id="Seg_767" n="e" s="T118">poʔto". </ts>
               <ts e="T120" id="Seg_769" n="e" s="T119">Dĭgəttə </ts>
               <ts e="T121" id="Seg_771" n="e" s="T120">dĭ </ts>
               <ts e="T122" id="Seg_773" n="e" s="T121">kambi </ts>
               <ts e="T123" id="Seg_775" n="e" s="T122">bünə, </ts>
               <ts e="T124" id="Seg_777" n="e" s="T123">dʼorlaʔbə, </ts>
               <ts e="T125" id="Seg_779" n="e" s="T124">dʼorlaʔbə. </ts>
               <ts e="T126" id="Seg_781" n="e" s="T125">Măna </ts>
               <ts e="T127" id="Seg_783" n="e" s="T126">băʔsittə </ts>
               <ts e="T128" id="Seg_785" n="e" s="T127">xočet. </ts>
               <ts e="T129" id="Seg_787" n="e" s="T128">(Mĭnzərzittə)". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_788" s="T0">PKZ_196X_Alenushka_flk.001 (002)</ta>
            <ta e="T8" id="Seg_789" s="T5">PKZ_196X_Alenushka_flk.002 (003)</ta>
            <ta e="T12" id="Seg_790" s="T8">PKZ_196X_Alenushka_flk.003 (004)</ta>
            <ta e="T16" id="Seg_791" s="T12">PKZ_196X_Alenushka_flk.004 (005)</ta>
            <ta e="T24" id="Seg_792" s="T16">PKZ_196X_Alenushka_flk.005 (006)</ta>
            <ta e="T27" id="Seg_793" s="T24">PKZ_196X_Alenushka_flk.006 (007)</ta>
            <ta e="T32" id="Seg_794" s="T27">PKZ_196X_Alenushka_flk.007 (008)</ta>
            <ta e="T39" id="Seg_795" s="T32">PKZ_196X_Alenushka_flk.008 (009)</ta>
            <ta e="T46" id="Seg_796" s="T39">PKZ_196X_Alenushka_flk.009 (010)</ta>
            <ta e="T48" id="Seg_797" s="T46">PKZ_196X_Alenushka_flk.010 (011)</ta>
            <ta e="T51" id="Seg_798" s="T48">PKZ_196X_Alenushka_flk.011 (012)</ta>
            <ta e="T54" id="Seg_799" s="T51">PKZ_196X_Alenushka_flk.012 (013)</ta>
            <ta e="T59" id="Seg_800" s="T54">PKZ_196X_Alenushka_flk.013 (014)</ta>
            <ta e="T61" id="Seg_801" s="T59">PKZ_196X_Alenushka_flk.014 (015)</ta>
            <ta e="T64" id="Seg_802" s="T61">PKZ_196X_Alenushka_flk.015 (016)</ta>
            <ta e="T71" id="Seg_803" s="T64">PKZ_196X_Alenushka_flk.016 (017)</ta>
            <ta e="T77" id="Seg_804" s="T71">PKZ_196X_Alenushka_flk.017 (018)</ta>
            <ta e="T81" id="Seg_805" s="T77">PKZ_196X_Alenushka_flk.018 (019)</ta>
            <ta e="T82" id="Seg_806" s="T81">PKZ_196X_Alenushka_flk.019 (020)</ta>
            <ta e="T85" id="Seg_807" s="T82">PKZ_196X_Alenushka_flk.020 (021)</ta>
            <ta e="T92" id="Seg_808" s="T85">PKZ_196X_Alenushka_flk.021 (022)</ta>
            <ta e="T98" id="Seg_809" s="T92">PKZ_196X_Alenushka_flk.022 (023)</ta>
            <ta e="T101" id="Seg_810" s="T98">PKZ_196X_Alenushka_flk.023 (024)</ta>
            <ta e="T105" id="Seg_811" s="T101">PKZ_196X_Alenushka_flk.024 (025)</ta>
            <ta e="T108" id="Seg_812" s="T105">PKZ_196X_Alenushka_flk.025 (026)</ta>
            <ta e="T111" id="Seg_813" s="T108">PKZ_196X_Alenushka_flk.026 (027)</ta>
            <ta e="T114" id="Seg_814" s="T111">PKZ_196X_Alenushka_flk.027 (028)</ta>
            <ta e="T119" id="Seg_815" s="T114">PKZ_196X_Alenushka_flk.028 (029)</ta>
            <ta e="T125" id="Seg_816" s="T119">PKZ_196X_Alenushka_flk.029 (030)</ta>
            <ta e="T128" id="Seg_817" s="T125">PKZ_196X_Alenushka_flk.030 (031)</ta>
            <ta e="T129" id="Seg_818" s="T128">PKZ_196X_Alenushka_flk.031 (032)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_819" s="T0">Amnobiʔi šidegöʔ Alʼonuška i Ivanuška. </ta>
            <ta e="T8" id="Seg_820" s="T5">Dĭgəttə amnobiʔi, amnobiʔi. </ta>
            <ta e="T12" id="Seg_821" s="T8">Dĭzeŋ ĭmbidə naga amzittə. </ta>
            <ta e="T16" id="Seg_822" s="T12">Dĭgəttə kambiʔi, šobiʔi, šobiʔi. </ta>
            <ta e="T24" id="Seg_823" s="T16">Dĭ nʼi măndə: "Măn ugaːndə bü bĭssittə axota". </ta>
            <ta e="T27" id="Seg_824" s="T24">"Iʔ bĭdeʔ dön! </ta>
            <ta e="T32" id="Seg_825" s="T27">Šobi tüžöj, a to tüžöj molal!" </ta>
            <ta e="T39" id="Seg_826" s="T32">Dĭgəttə kambiʔi, kambiʔi, dĭgəttə ularən üjüt nulaʔbə. </ta>
            <ta e="T46" id="Seg_827" s="T39">Dĭ măndə: "Iʔ bĭdeʔ, a to ular molal!" </ta>
            <ta e="T48" id="Seg_828" s="T46">Dĭgəttə kanlaʔbəʔjə. </ta>
            <ta e="T51" id="Seg_829" s="T48">Inen üjüt nulaʔbə. </ta>
            <ta e="T54" id="Seg_830" s="T51">"Ugaːndə bĭssittə axota!" </ta>
            <ta e="T59" id="Seg_831" s="T54">"Iʔ bĭdeʔ, a to ine molal!" </ta>
            <ta e="T61" id="Seg_832" s="T59">Dĭgəttə šonəgaʔi. </ta>
            <ta e="T64" id="Seg_833" s="T61">Poʔton üjüt nulaʔbə. </ta>
            <ta e="T71" id="Seg_834" s="T64">Dĭ măndə: "Iʔ bĭdeʔ, a to poʔtobə molal". </ta>
            <ta e="T77" id="Seg_835" s="T71">Dĭ bar bĭʔpi i poʔto molaːmbi. </ta>
            <ta e="T81" id="Seg_836" s="T77">Dĭgəttə Alʼonuška dʼorbi, dʼorbi. </ta>
            <ta e="T82" id="Seg_837" s="T81">Kambiʔi. </ta>
            <ta e="T85" id="Seg_838" s="T82">Dĭgəttə šonəga kuza. </ta>
            <ta e="T92" id="Seg_839" s="T85">Dĭ Alʼonuška iluʔpi tibinə i maːndə kunlaʔpi. </ta>
            <ta e="T98" id="Seg_840" s="T92">Nu (de-) dĭ Vanʼuška mobi poʔto. </ta>
            <ta e="T101" id="Seg_841" s="T98">Dĭm tože bădlaʔbəʔjə. </ta>
            <ta e="T105" id="Seg_842" s="T101">Dĭgəttə dĭ saməjzittə kalla dʼürbi. </ta>
            <ta e="T108" id="Seg_843" s="T105">Baška ne šobi. </ta>
            <ta e="T111" id="Seg_844" s="T108">Dĭm baʔluʔpi bünə. </ta>
            <ta e="T114" id="Seg_845" s="T111">I šobi tibi. </ta>
            <ta e="T119" id="Seg_846" s="T114">Măndə: "Nada kuʔsʼittə dĭ poʔto". </ta>
            <ta e="T125" id="Seg_847" s="T119">Dĭgəttə dĭ kambi bünə, dʼorlaʔbə, dʼorlaʔbə. </ta>
            <ta e="T128" id="Seg_848" s="T125">"Măna băʔsittə xočet. </ta>
            <ta e="T129" id="Seg_849" s="T128">(Mĭnzərzittə)".</ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_850" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_851" s="T1">šide-göʔ</ta>
            <ta e="T3" id="Seg_852" s="T2">Alʼonuška</ta>
            <ta e="T4" id="Seg_853" s="T3">i</ta>
            <ta e="T5" id="Seg_854" s="T4">Ivanuška</ta>
            <ta e="T6" id="Seg_855" s="T5">dĭgəttə</ta>
            <ta e="T7" id="Seg_856" s="T6">amno-bi-ʔi</ta>
            <ta e="T8" id="Seg_857" s="T7">amno-bi-ʔi</ta>
            <ta e="T9" id="Seg_858" s="T8">dĭ-zeŋ</ta>
            <ta e="T10" id="Seg_859" s="T9">ĭmbi=də</ta>
            <ta e="T11" id="Seg_860" s="T10">naga</ta>
            <ta e="T12" id="Seg_861" s="T11">am-zittə</ta>
            <ta e="T13" id="Seg_862" s="T12">dĭgəttə</ta>
            <ta e="T14" id="Seg_863" s="T13">kam-bi-ʔi</ta>
            <ta e="T15" id="Seg_864" s="T14">šo-bi-ʔi</ta>
            <ta e="T16" id="Seg_865" s="T15">šo-bi-ʔi</ta>
            <ta e="T17" id="Seg_866" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_867" s="T17">nʼi</ta>
            <ta e="T19" id="Seg_868" s="T18">măn-də</ta>
            <ta e="T20" id="Seg_869" s="T19">măn</ta>
            <ta e="T21" id="Seg_870" s="T20">ugaːndə</ta>
            <ta e="T22" id="Seg_871" s="T21">bü</ta>
            <ta e="T23" id="Seg_872" s="T22">bĭs-sittə</ta>
            <ta e="T24" id="Seg_873" s="T23">axota</ta>
            <ta e="T25" id="Seg_874" s="T24">i-ʔ</ta>
            <ta e="T26" id="Seg_875" s="T25">bĭd-e-ʔ</ta>
            <ta e="T27" id="Seg_876" s="T26">dön</ta>
            <ta e="T28" id="Seg_877" s="T27">šo-bi</ta>
            <ta e="T29" id="Seg_878" s="T28">tüžöj</ta>
            <ta e="T30" id="Seg_879" s="T29">ato</ta>
            <ta e="T31" id="Seg_880" s="T30">tüžöj</ta>
            <ta e="T32" id="Seg_881" s="T31">mo-la-l</ta>
            <ta e="T33" id="Seg_882" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_883" s="T33">kam-bi-ʔi</ta>
            <ta e="T35" id="Seg_884" s="T34">kam-bi-ʔi</ta>
            <ta e="T36" id="Seg_885" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_886" s="T36">ular-ə-n</ta>
            <ta e="T38" id="Seg_887" s="T37">üjü-t</ta>
            <ta e="T39" id="Seg_888" s="T38">nu-laʔbə</ta>
            <ta e="T40" id="Seg_889" s="T39">dĭ</ta>
            <ta e="T41" id="Seg_890" s="T40">măn-də</ta>
            <ta e="T42" id="Seg_891" s="T41">i-ʔ</ta>
            <ta e="T43" id="Seg_892" s="T42">bĭd-e-ʔ</ta>
            <ta e="T44" id="Seg_893" s="T43">ato</ta>
            <ta e="T45" id="Seg_894" s="T44">ular</ta>
            <ta e="T46" id="Seg_895" s="T45">mo-la-l</ta>
            <ta e="T47" id="Seg_896" s="T46">dĭgəttə</ta>
            <ta e="T48" id="Seg_897" s="T47">kan-laʔbə-ʔjə</ta>
            <ta e="T49" id="Seg_898" s="T48">ine-n</ta>
            <ta e="T50" id="Seg_899" s="T49">üjü-t</ta>
            <ta e="T51" id="Seg_900" s="T50">nu-laʔbə</ta>
            <ta e="T52" id="Seg_901" s="T51">ugaːndə</ta>
            <ta e="T53" id="Seg_902" s="T52">bĭs-sittə</ta>
            <ta e="T54" id="Seg_903" s="T53">axota</ta>
            <ta e="T55" id="Seg_904" s="T54">i-ʔ</ta>
            <ta e="T56" id="Seg_905" s="T55">bĭd-e-ʔ</ta>
            <ta e="T57" id="Seg_906" s="T56">ato</ta>
            <ta e="T58" id="Seg_907" s="T57">ine</ta>
            <ta e="T59" id="Seg_908" s="T58">mo-la-l</ta>
            <ta e="T60" id="Seg_909" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_910" s="T60">šonə-ga-ʔi</ta>
            <ta e="T62" id="Seg_911" s="T61">poʔto-n</ta>
            <ta e="T63" id="Seg_912" s="T62">üjü-t</ta>
            <ta e="T64" id="Seg_913" s="T63">nu-laʔbə</ta>
            <ta e="T65" id="Seg_914" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_915" s="T65">măn-də</ta>
            <ta e="T67" id="Seg_916" s="T66">i-ʔ</ta>
            <ta e="T68" id="Seg_917" s="T67">bĭd-e-ʔ</ta>
            <ta e="T69" id="Seg_918" s="T68">ato</ta>
            <ta e="T70" id="Seg_919" s="T69">poʔto-bə</ta>
            <ta e="T71" id="Seg_920" s="T70">mo-la-l</ta>
            <ta e="T72" id="Seg_921" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_922" s="T72">bar</ta>
            <ta e="T74" id="Seg_923" s="T73">bĭʔ-pi</ta>
            <ta e="T75" id="Seg_924" s="T74">i</ta>
            <ta e="T76" id="Seg_925" s="T75">poʔto</ta>
            <ta e="T77" id="Seg_926" s="T76">mo-laːm-bi</ta>
            <ta e="T78" id="Seg_927" s="T77">dĭgəttə</ta>
            <ta e="T79" id="Seg_928" s="T78">Alʼonuška</ta>
            <ta e="T80" id="Seg_929" s="T79">dʼor-bi</ta>
            <ta e="T81" id="Seg_930" s="T80">dʼor-bi</ta>
            <ta e="T82" id="Seg_931" s="T81">kam-bi-ʔi</ta>
            <ta e="T83" id="Seg_932" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_933" s="T83">šonə-ga</ta>
            <ta e="T85" id="Seg_934" s="T84">kuza</ta>
            <ta e="T86" id="Seg_935" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_936" s="T86">Alʼonuška</ta>
            <ta e="T88" id="Seg_937" s="T87">i-luʔ-pi</ta>
            <ta e="T89" id="Seg_938" s="T88">tibi-nə</ta>
            <ta e="T90" id="Seg_939" s="T89">i</ta>
            <ta e="T91" id="Seg_940" s="T90">ma-ndə</ta>
            <ta e="T92" id="Seg_941" s="T91">kun-laʔ-pi</ta>
            <ta e="T93" id="Seg_942" s="T92">nu</ta>
            <ta e="T95" id="Seg_943" s="T94">dĭ</ta>
            <ta e="T96" id="Seg_944" s="T95">Vanʼuška</ta>
            <ta e="T97" id="Seg_945" s="T96">mo-bi</ta>
            <ta e="T98" id="Seg_946" s="T97">poʔto</ta>
            <ta e="T99" id="Seg_947" s="T98">dĭ-m</ta>
            <ta e="T100" id="Seg_948" s="T99">tože</ta>
            <ta e="T101" id="Seg_949" s="T100">băd-laʔbə-ʔjə</ta>
            <ta e="T102" id="Seg_950" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_951" s="T102">dĭ</ta>
            <ta e="T104" id="Seg_952" s="T103">saməj-zittə</ta>
            <ta e="T130" id="Seg_953" s="T104">kal-la</ta>
            <ta e="T105" id="Seg_954" s="T130">dʼür-bi</ta>
            <ta e="T106" id="Seg_955" s="T105">baška</ta>
            <ta e="T107" id="Seg_956" s="T106">ne</ta>
            <ta e="T108" id="Seg_957" s="T107">šo-bi</ta>
            <ta e="T109" id="Seg_958" s="T108">dĭ-m</ta>
            <ta e="T110" id="Seg_959" s="T109">baʔ-luʔ-pi</ta>
            <ta e="T111" id="Seg_960" s="T110">bü-nə</ta>
            <ta e="T112" id="Seg_961" s="T111">i</ta>
            <ta e="T113" id="Seg_962" s="T112">šo-bi</ta>
            <ta e="T114" id="Seg_963" s="T113">tibi</ta>
            <ta e="T115" id="Seg_964" s="T114">măn-də</ta>
            <ta e="T116" id="Seg_965" s="T115">nada</ta>
            <ta e="T117" id="Seg_966" s="T116">kuʔ-sʼittə</ta>
            <ta e="T118" id="Seg_967" s="T117">dĭ</ta>
            <ta e="T119" id="Seg_968" s="T118">poʔto</ta>
            <ta e="T120" id="Seg_969" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_970" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_971" s="T121">kam-bi</ta>
            <ta e="T123" id="Seg_972" s="T122">bü-nə</ta>
            <ta e="T124" id="Seg_973" s="T123">dʼor-laʔbə</ta>
            <ta e="T125" id="Seg_974" s="T124">dʼor-laʔbə</ta>
            <ta e="T126" id="Seg_975" s="T125">măna</ta>
            <ta e="T127" id="Seg_976" s="T126">băʔ-sittə</ta>
            <ta e="T128" id="Seg_977" s="T127">xočet</ta>
            <ta e="T129" id="Seg_978" s="T128">mĭnzər-zittə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_979" s="T0">amno-bi-jəʔ</ta>
            <ta e="T2" id="Seg_980" s="T1">šide-göʔ</ta>
            <ta e="T3" id="Seg_981" s="T2">Alʼonuška</ta>
            <ta e="T4" id="Seg_982" s="T3">i</ta>
            <ta e="T5" id="Seg_983" s="T4">Ivanuška</ta>
            <ta e="T6" id="Seg_984" s="T5">dĭgəttə</ta>
            <ta e="T7" id="Seg_985" s="T6">amno-bi-jəʔ</ta>
            <ta e="T8" id="Seg_986" s="T7">amno-bi-jəʔ</ta>
            <ta e="T9" id="Seg_987" s="T8">dĭ-zAŋ</ta>
            <ta e="T10" id="Seg_988" s="T9">ĭmbi=də</ta>
            <ta e="T11" id="Seg_989" s="T10">naga</ta>
            <ta e="T12" id="Seg_990" s="T11">am-zittə</ta>
            <ta e="T13" id="Seg_991" s="T12">dĭgəttə</ta>
            <ta e="T14" id="Seg_992" s="T13">kan-bi-jəʔ</ta>
            <ta e="T15" id="Seg_993" s="T14">šo-bi-jəʔ</ta>
            <ta e="T16" id="Seg_994" s="T15">šo-bi-jəʔ</ta>
            <ta e="T17" id="Seg_995" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_996" s="T17">nʼi</ta>
            <ta e="T19" id="Seg_997" s="T18">măn-ntə</ta>
            <ta e="T20" id="Seg_998" s="T19">măn</ta>
            <ta e="T21" id="Seg_999" s="T20">ugaːndə</ta>
            <ta e="T22" id="Seg_1000" s="T21">bü</ta>
            <ta e="T23" id="Seg_1001" s="T22">bĭs-zittə</ta>
            <ta e="T24" id="Seg_1002" s="T23">axota</ta>
            <ta e="T25" id="Seg_1003" s="T24">e-ʔ</ta>
            <ta e="T26" id="Seg_1004" s="T25">bĭs-ə-ʔ</ta>
            <ta e="T27" id="Seg_1005" s="T26">dön</ta>
            <ta e="T28" id="Seg_1006" s="T27">šo-bi</ta>
            <ta e="T29" id="Seg_1007" s="T28">tüžöj</ta>
            <ta e="T30" id="Seg_1008" s="T29">ato</ta>
            <ta e="T31" id="Seg_1009" s="T30">tüžöj</ta>
            <ta e="T32" id="Seg_1010" s="T31">mo-lV-l</ta>
            <ta e="T33" id="Seg_1011" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_1012" s="T33">kan-bi-jəʔ</ta>
            <ta e="T35" id="Seg_1013" s="T34">kan-bi-jəʔ</ta>
            <ta e="T36" id="Seg_1014" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_1015" s="T36">ular-ə-n</ta>
            <ta e="T38" id="Seg_1016" s="T37">üjü-t</ta>
            <ta e="T39" id="Seg_1017" s="T38">nu-laʔbə</ta>
            <ta e="T40" id="Seg_1018" s="T39">dĭ</ta>
            <ta e="T41" id="Seg_1019" s="T40">măn-ntə</ta>
            <ta e="T42" id="Seg_1020" s="T41">e-ʔ</ta>
            <ta e="T43" id="Seg_1021" s="T42">bĭs-ə-ʔ</ta>
            <ta e="T44" id="Seg_1022" s="T43">ato</ta>
            <ta e="T45" id="Seg_1023" s="T44">ular</ta>
            <ta e="T46" id="Seg_1024" s="T45">mo-lV-l</ta>
            <ta e="T47" id="Seg_1025" s="T46">dĭgəttə</ta>
            <ta e="T48" id="Seg_1026" s="T47">kan-laʔbə-jəʔ</ta>
            <ta e="T49" id="Seg_1027" s="T48">ine-n</ta>
            <ta e="T50" id="Seg_1028" s="T49">üjü-t</ta>
            <ta e="T51" id="Seg_1029" s="T50">nu-laʔbə</ta>
            <ta e="T52" id="Seg_1030" s="T51">ugaːndə</ta>
            <ta e="T53" id="Seg_1031" s="T52">bĭs-zittə</ta>
            <ta e="T54" id="Seg_1032" s="T53">axota</ta>
            <ta e="T55" id="Seg_1033" s="T54">e-ʔ</ta>
            <ta e="T56" id="Seg_1034" s="T55">bĭs-ə-ʔ</ta>
            <ta e="T57" id="Seg_1035" s="T56">ato</ta>
            <ta e="T58" id="Seg_1036" s="T57">ine</ta>
            <ta e="T59" id="Seg_1037" s="T58">mo-lV-l</ta>
            <ta e="T60" id="Seg_1038" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_1039" s="T60">šonə-gA-jəʔ</ta>
            <ta e="T62" id="Seg_1040" s="T61">poʔto-n</ta>
            <ta e="T63" id="Seg_1041" s="T62">üjü-t</ta>
            <ta e="T64" id="Seg_1042" s="T63">nu-laʔbə</ta>
            <ta e="T65" id="Seg_1043" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_1044" s="T65">măn-ntə</ta>
            <ta e="T67" id="Seg_1045" s="T66">e-ʔ</ta>
            <ta e="T68" id="Seg_1046" s="T67">bĭs-ə-ʔ</ta>
            <ta e="T69" id="Seg_1047" s="T68">ato</ta>
            <ta e="T70" id="Seg_1048" s="T69">poʔto-bə</ta>
            <ta e="T71" id="Seg_1049" s="T70">mo-lV-l</ta>
            <ta e="T72" id="Seg_1050" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_1051" s="T72">bar</ta>
            <ta e="T74" id="Seg_1052" s="T73">bĭs-bi</ta>
            <ta e="T75" id="Seg_1053" s="T74">i</ta>
            <ta e="T76" id="Seg_1054" s="T75">poʔto</ta>
            <ta e="T77" id="Seg_1055" s="T76">mo-laːm-bi</ta>
            <ta e="T78" id="Seg_1056" s="T77">dĭgəttə</ta>
            <ta e="T79" id="Seg_1057" s="T78">Alʼonuška</ta>
            <ta e="T80" id="Seg_1058" s="T79">tʼor-bi</ta>
            <ta e="T81" id="Seg_1059" s="T80">tʼor-bi</ta>
            <ta e="T82" id="Seg_1060" s="T81">kan-bi-jəʔ</ta>
            <ta e="T83" id="Seg_1061" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1062" s="T83">šonə-gA</ta>
            <ta e="T85" id="Seg_1063" s="T84">kuza</ta>
            <ta e="T86" id="Seg_1064" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_1065" s="T86">Alʼonuška</ta>
            <ta e="T88" id="Seg_1066" s="T87">i-luʔbdə-bi</ta>
            <ta e="T89" id="Seg_1067" s="T88">tibi-Tə</ta>
            <ta e="T90" id="Seg_1068" s="T89">i</ta>
            <ta e="T91" id="Seg_1069" s="T90">maʔ-gəndə</ta>
            <ta e="T92" id="Seg_1070" s="T91">kun-laʔbə-bi</ta>
            <ta e="T93" id="Seg_1071" s="T92">nu</ta>
            <ta e="T95" id="Seg_1072" s="T94">dĭ</ta>
            <ta e="T96" id="Seg_1073" s="T95">Vanʼuška</ta>
            <ta e="T97" id="Seg_1074" s="T96">mo-bi</ta>
            <ta e="T98" id="Seg_1075" s="T97">poʔto</ta>
            <ta e="T99" id="Seg_1076" s="T98">dĭ-m</ta>
            <ta e="T100" id="Seg_1077" s="T99">tože</ta>
            <ta e="T101" id="Seg_1078" s="T100">bădə-laʔbə-jəʔ</ta>
            <ta e="T102" id="Seg_1079" s="T101">dĭgəttə</ta>
            <ta e="T103" id="Seg_1080" s="T102">dĭ</ta>
            <ta e="T104" id="Seg_1081" s="T103">saməj-zittə</ta>
            <ta e="T130" id="Seg_1082" s="T104">kan-lAʔ</ta>
            <ta e="T105" id="Seg_1083" s="T130">tʼür-bi</ta>
            <ta e="T106" id="Seg_1084" s="T105">baška</ta>
            <ta e="T107" id="Seg_1085" s="T106">ne</ta>
            <ta e="T108" id="Seg_1086" s="T107">šo-bi</ta>
            <ta e="T109" id="Seg_1087" s="T108">dĭ-m</ta>
            <ta e="T110" id="Seg_1088" s="T109">baʔbdə-luʔbdə-bi</ta>
            <ta e="T111" id="Seg_1089" s="T110">bü-Tə</ta>
            <ta e="T112" id="Seg_1090" s="T111">i</ta>
            <ta e="T113" id="Seg_1091" s="T112">šo-bi</ta>
            <ta e="T114" id="Seg_1092" s="T113">tibi</ta>
            <ta e="T115" id="Seg_1093" s="T114">măn-ntə</ta>
            <ta e="T116" id="Seg_1094" s="T115">nadə</ta>
            <ta e="T117" id="Seg_1095" s="T116">kut-zittə</ta>
            <ta e="T118" id="Seg_1096" s="T117">dĭ</ta>
            <ta e="T119" id="Seg_1097" s="T118">poʔto</ta>
            <ta e="T120" id="Seg_1098" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_1099" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_1100" s="T121">kan-bi</ta>
            <ta e="T123" id="Seg_1101" s="T122">bü-Tə</ta>
            <ta e="T124" id="Seg_1102" s="T123">tʼor-laʔbə</ta>
            <ta e="T125" id="Seg_1103" s="T124">tʼor-laʔbə</ta>
            <ta e="T126" id="Seg_1104" s="T125">măna</ta>
            <ta e="T127" id="Seg_1105" s="T126">băt-zittə</ta>
            <ta e="T128" id="Seg_1106" s="T127">xočet</ta>
            <ta e="T129" id="Seg_1107" s="T128">mĭnzər-zittə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1108" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_1109" s="T1">two-COLL</ta>
            <ta e="T3" id="Seg_1110" s="T2">Alenushka.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1111" s="T3">and</ta>
            <ta e="T5" id="Seg_1112" s="T4">Ivanushka.[NOM.SG]</ta>
            <ta e="T6" id="Seg_1113" s="T5">then</ta>
            <ta e="T7" id="Seg_1114" s="T6">live-PST-3PL</ta>
            <ta e="T8" id="Seg_1115" s="T7">live-PST-3PL</ta>
            <ta e="T9" id="Seg_1116" s="T8">this-PL</ta>
            <ta e="T10" id="Seg_1117" s="T9">what.[NOM.SG]=INDEF</ta>
            <ta e="T11" id="Seg_1118" s="T10">NEG.EX.[3SG]</ta>
            <ta e="T12" id="Seg_1119" s="T11">eat-INF.LAT</ta>
            <ta e="T13" id="Seg_1120" s="T12">then</ta>
            <ta e="T14" id="Seg_1121" s="T13">go-PST-3PL</ta>
            <ta e="T15" id="Seg_1122" s="T14">come-PST-3PL</ta>
            <ta e="T16" id="Seg_1123" s="T15">come-PST-3PL</ta>
            <ta e="T17" id="Seg_1124" s="T16">this.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1125" s="T17">boy.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1126" s="T18">say-IPFVZ.[3SG]</ta>
            <ta e="T20" id="Seg_1127" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_1128" s="T20">very</ta>
            <ta e="T22" id="Seg_1129" s="T21">water.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1130" s="T22">drink-INF.LAT</ta>
            <ta e="T24" id="Seg_1131" s="T23">one.wants</ta>
            <ta e="T25" id="Seg_1132" s="T24">NEG.AUX-IMP.2SG</ta>
            <ta e="T26" id="Seg_1133" s="T25">drink-EP-CNG</ta>
            <ta e="T27" id="Seg_1134" s="T26">here</ta>
            <ta e="T28" id="Seg_1135" s="T27">come-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1136" s="T28">cow.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1137" s="T29">otherwise</ta>
            <ta e="T31" id="Seg_1138" s="T30">cow.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1139" s="T31">become-FUT-2SG</ta>
            <ta e="T33" id="Seg_1140" s="T32">then</ta>
            <ta e="T34" id="Seg_1141" s="T33">go-PST-3PL</ta>
            <ta e="T35" id="Seg_1142" s="T34">go-PST-3PL</ta>
            <ta e="T36" id="Seg_1143" s="T35">then</ta>
            <ta e="T37" id="Seg_1144" s="T36">sheep-EP-GEN</ta>
            <ta e="T38" id="Seg_1145" s="T37">foot-NOM/GEN.3SG</ta>
            <ta e="T39" id="Seg_1146" s="T38">stand-DUR.[3SG]</ta>
            <ta e="T40" id="Seg_1147" s="T39">this.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1148" s="T40">say-IPFVZ.[3SG]</ta>
            <ta e="T42" id="Seg_1149" s="T41">NEG.AUX-IMP.2SG</ta>
            <ta e="T43" id="Seg_1150" s="T42">drink-EP-CNG</ta>
            <ta e="T44" id="Seg_1151" s="T43">otherwise</ta>
            <ta e="T45" id="Seg_1152" s="T44">sheep.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1153" s="T45">become-FUT-2SG</ta>
            <ta e="T47" id="Seg_1154" s="T46">then</ta>
            <ta e="T48" id="Seg_1155" s="T47">go-DUR-3PL</ta>
            <ta e="T49" id="Seg_1156" s="T48">horse-GEN</ta>
            <ta e="T50" id="Seg_1157" s="T49">foot-NOM/GEN.3SG</ta>
            <ta e="T51" id="Seg_1158" s="T50">stand-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_1159" s="T51">very</ta>
            <ta e="T53" id="Seg_1160" s="T52">drink-INF.LAT</ta>
            <ta e="T54" id="Seg_1161" s="T53">one.wants</ta>
            <ta e="T55" id="Seg_1162" s="T54">NEG.AUX-IMP.2SG</ta>
            <ta e="T56" id="Seg_1163" s="T55">drink-EP-CNG</ta>
            <ta e="T57" id="Seg_1164" s="T56">otherwise</ta>
            <ta e="T58" id="Seg_1165" s="T57">horse.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1166" s="T58">become-FUT-2SG</ta>
            <ta e="T60" id="Seg_1167" s="T59">then</ta>
            <ta e="T61" id="Seg_1168" s="T60">come-PRS-3PL</ta>
            <ta e="T62" id="Seg_1169" s="T61">goat-GEN</ta>
            <ta e="T63" id="Seg_1170" s="T62">foot-NOM/GEN.3SG</ta>
            <ta e="T64" id="Seg_1171" s="T63">stand-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_1172" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1173" s="T65">say-IPFVZ.[3SG]</ta>
            <ta e="T67" id="Seg_1174" s="T66">NEG.AUX-IMP.2SG</ta>
            <ta e="T68" id="Seg_1175" s="T67">drink-EP-CNG</ta>
            <ta e="T69" id="Seg_1176" s="T68">otherwise</ta>
            <ta e="T70" id="Seg_1177" s="T69">goat-ACC.3SG</ta>
            <ta e="T71" id="Seg_1178" s="T70">become-FUT-2SG</ta>
            <ta e="T72" id="Seg_1179" s="T71">this.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1180" s="T72">PTCL</ta>
            <ta e="T74" id="Seg_1181" s="T73">drink-PST.[3SG]</ta>
            <ta e="T75" id="Seg_1182" s="T74">and</ta>
            <ta e="T76" id="Seg_1183" s="T75">goat.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1184" s="T76">become-RES-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1185" s="T77">then</ta>
            <ta e="T79" id="Seg_1186" s="T78">Alenushka.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1187" s="T79">cry-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1188" s="T80">cry-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1189" s="T81">go-PST-3PL</ta>
            <ta e="T83" id="Seg_1190" s="T82">then</ta>
            <ta e="T84" id="Seg_1191" s="T83">come-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_1192" s="T84">man.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1193" s="T85">this.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1194" s="T86">Alenushka.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1195" s="T87">take-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1196" s="T88">man-LAT</ta>
            <ta e="T90" id="Seg_1197" s="T89">and</ta>
            <ta e="T91" id="Seg_1198" s="T90">tent-LAT/LOC.3SG</ta>
            <ta e="T92" id="Seg_1199" s="T91">bring-DUR-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1200" s="T92">well</ta>
            <ta e="T95" id="Seg_1201" s="T94">this.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1202" s="T95">Vanyushka.[NOM.SG]</ta>
            <ta e="T97" id="Seg_1203" s="T96">become-PST.[3SG]</ta>
            <ta e="T98" id="Seg_1204" s="T97">goat.[NOM.SG]</ta>
            <ta e="T99" id="Seg_1205" s="T98">this-ACC</ta>
            <ta e="T100" id="Seg_1206" s="T99">also</ta>
            <ta e="T101" id="Seg_1207" s="T100">feed-DUR-3PL</ta>
            <ta e="T102" id="Seg_1208" s="T101">then</ta>
            <ta e="T103" id="Seg_1209" s="T102">this.[NOM.SG]</ta>
            <ta e="T104" id="Seg_1210" s="T103">hunt-INF.LAT</ta>
            <ta e="T130" id="Seg_1211" s="T104">go-CVB</ta>
            <ta e="T105" id="Seg_1212" s="T130">disappear-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1213" s="T105">another.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1214" s="T106">woman.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1215" s="T107">come-PST.[3SG]</ta>
            <ta e="T109" id="Seg_1216" s="T108">this-ACC</ta>
            <ta e="T110" id="Seg_1217" s="T109">throw-MOM-PST.[3SG]</ta>
            <ta e="T111" id="Seg_1218" s="T110">water-LAT</ta>
            <ta e="T112" id="Seg_1219" s="T111">and</ta>
            <ta e="T113" id="Seg_1220" s="T112">come-PST.[3SG]</ta>
            <ta e="T114" id="Seg_1221" s="T113">man.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1222" s="T114">say-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_1223" s="T115">one.should</ta>
            <ta e="T117" id="Seg_1224" s="T116">kill-INF.LAT</ta>
            <ta e="T118" id="Seg_1225" s="T117">this.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1226" s="T118">goat.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1227" s="T119">then</ta>
            <ta e="T121" id="Seg_1228" s="T120">this.[NOM.SG]</ta>
            <ta e="T122" id="Seg_1229" s="T121">go-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1230" s="T122">water-LAT</ta>
            <ta e="T124" id="Seg_1231" s="T123">cry-DUR.[3SG]</ta>
            <ta e="T125" id="Seg_1232" s="T124">cry-DUR.[3SG]</ta>
            <ta e="T126" id="Seg_1233" s="T125">I.ACC</ta>
            <ta e="T127" id="Seg_1234" s="T126">cut-INF.LAT</ta>
            <ta e="T128" id="Seg_1235" s="T127">want.3SG</ta>
            <ta e="T129" id="Seg_1236" s="T128">boil-INF.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1237" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_1238" s="T1">два-COLL</ta>
            <ta e="T3" id="Seg_1239" s="T2">Аленушка.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1240" s="T3">и</ta>
            <ta e="T5" id="Seg_1241" s="T4">Иванушка.[NOM.SG]</ta>
            <ta e="T6" id="Seg_1242" s="T5">тогда</ta>
            <ta e="T7" id="Seg_1243" s="T6">жить-PST-3PL</ta>
            <ta e="T8" id="Seg_1244" s="T7">жить-PST-3PL</ta>
            <ta e="T9" id="Seg_1245" s="T8">этот-PL</ta>
            <ta e="T10" id="Seg_1246" s="T9">что.[NOM.SG]=INDEF</ta>
            <ta e="T11" id="Seg_1247" s="T10">NEG.EX.[3SG]</ta>
            <ta e="T12" id="Seg_1248" s="T11">съесть-INF.LAT</ta>
            <ta e="T13" id="Seg_1249" s="T12">тогда</ta>
            <ta e="T14" id="Seg_1250" s="T13">пойти-PST-3PL</ta>
            <ta e="T15" id="Seg_1251" s="T14">прийти-PST-3PL</ta>
            <ta e="T16" id="Seg_1252" s="T15">прийти-PST-3PL</ta>
            <ta e="T17" id="Seg_1253" s="T16">этот.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1254" s="T17">мальчик.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1255" s="T18">сказать-IPFVZ.[3SG]</ta>
            <ta e="T20" id="Seg_1256" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_1257" s="T20">очень</ta>
            <ta e="T22" id="Seg_1258" s="T21">вода.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1259" s="T22">пить-INF.LAT</ta>
            <ta e="T24" id="Seg_1260" s="T23">хочется</ta>
            <ta e="T25" id="Seg_1261" s="T24">NEG.AUX-IMP.2SG</ta>
            <ta e="T26" id="Seg_1262" s="T25">пить-EP-CNG</ta>
            <ta e="T27" id="Seg_1263" s="T26">здесь</ta>
            <ta e="T28" id="Seg_1264" s="T27">прийти-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1265" s="T28">корова.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1266" s="T29">а.то</ta>
            <ta e="T31" id="Seg_1267" s="T30">корова.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1268" s="T31">стать-FUT-2SG</ta>
            <ta e="T33" id="Seg_1269" s="T32">тогда</ta>
            <ta e="T34" id="Seg_1270" s="T33">пойти-PST-3PL</ta>
            <ta e="T35" id="Seg_1271" s="T34">пойти-PST-3PL</ta>
            <ta e="T36" id="Seg_1272" s="T35">тогда</ta>
            <ta e="T37" id="Seg_1273" s="T36">овца-EP-GEN</ta>
            <ta e="T38" id="Seg_1274" s="T37">нога-NOM/GEN.3SG</ta>
            <ta e="T39" id="Seg_1275" s="T38">стоять-DUR.[3SG]</ta>
            <ta e="T40" id="Seg_1276" s="T39">этот.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1277" s="T40">сказать-IPFVZ.[3SG]</ta>
            <ta e="T42" id="Seg_1278" s="T41">NEG.AUX-IMP.2SG</ta>
            <ta e="T43" id="Seg_1279" s="T42">пить-EP-CNG</ta>
            <ta e="T44" id="Seg_1280" s="T43">а.то</ta>
            <ta e="T45" id="Seg_1281" s="T44">овца.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1282" s="T45">стать-FUT-2SG</ta>
            <ta e="T47" id="Seg_1283" s="T46">тогда</ta>
            <ta e="T48" id="Seg_1284" s="T47">пойти-DUR-3PL</ta>
            <ta e="T49" id="Seg_1285" s="T48">лошадь-GEN</ta>
            <ta e="T50" id="Seg_1286" s="T49">нога-NOM/GEN.3SG</ta>
            <ta e="T51" id="Seg_1287" s="T50">стоять-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_1288" s="T51">очень</ta>
            <ta e="T53" id="Seg_1289" s="T52">пить-INF.LAT</ta>
            <ta e="T54" id="Seg_1290" s="T53">хочется</ta>
            <ta e="T55" id="Seg_1291" s="T54">NEG.AUX-IMP.2SG</ta>
            <ta e="T56" id="Seg_1292" s="T55">пить-EP-CNG</ta>
            <ta e="T57" id="Seg_1293" s="T56">а.то</ta>
            <ta e="T58" id="Seg_1294" s="T57">лошадь.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1295" s="T58">стать-FUT-2SG</ta>
            <ta e="T60" id="Seg_1296" s="T59">тогда</ta>
            <ta e="T61" id="Seg_1297" s="T60">прийти-PRS-3PL</ta>
            <ta e="T62" id="Seg_1298" s="T61">коза-GEN</ta>
            <ta e="T63" id="Seg_1299" s="T62">нога-NOM/GEN.3SG</ta>
            <ta e="T64" id="Seg_1300" s="T63">стоять-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_1301" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1302" s="T65">сказать-IPFVZ.[3SG]</ta>
            <ta e="T67" id="Seg_1303" s="T66">NEG.AUX-IMP.2SG</ta>
            <ta e="T68" id="Seg_1304" s="T67">пить-EP-CNG</ta>
            <ta e="T69" id="Seg_1305" s="T68">а.то</ta>
            <ta e="T70" id="Seg_1306" s="T69">коза-ACC.3SG</ta>
            <ta e="T71" id="Seg_1307" s="T70">стать-FUT-2SG</ta>
            <ta e="T72" id="Seg_1308" s="T71">этот.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1309" s="T72">PTCL</ta>
            <ta e="T74" id="Seg_1310" s="T73">пить-PST.[3SG]</ta>
            <ta e="T75" id="Seg_1311" s="T74">и</ta>
            <ta e="T76" id="Seg_1312" s="T75">коза.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1313" s="T76">стать-RES-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1314" s="T77">тогда</ta>
            <ta e="T79" id="Seg_1315" s="T78">Аленушка.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1316" s="T79">плакать-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1317" s="T80">плакать-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1318" s="T81">пойти-PST-3PL</ta>
            <ta e="T83" id="Seg_1319" s="T82">тогда</ta>
            <ta e="T84" id="Seg_1320" s="T83">прийти-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_1321" s="T84">мужчина.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1322" s="T85">этот.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1323" s="T86">Аленушка.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1324" s="T87">взять-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1325" s="T88">мужчина-LAT</ta>
            <ta e="T90" id="Seg_1326" s="T89">и</ta>
            <ta e="T91" id="Seg_1327" s="T90">чум-LAT/LOC.3SG</ta>
            <ta e="T92" id="Seg_1328" s="T91">нести-DUR-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1329" s="T92">ну</ta>
            <ta e="T95" id="Seg_1330" s="T94">этот.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1331" s="T95">Ванюшка.[NOM.SG]</ta>
            <ta e="T97" id="Seg_1332" s="T96">стать-PST.[3SG]</ta>
            <ta e="T98" id="Seg_1333" s="T97">коза.[NOM.SG]</ta>
            <ta e="T99" id="Seg_1334" s="T98">этот-ACC</ta>
            <ta e="T100" id="Seg_1335" s="T99">тоже</ta>
            <ta e="T101" id="Seg_1336" s="T100">кормить-DUR-3PL</ta>
            <ta e="T102" id="Seg_1337" s="T101">тогда</ta>
            <ta e="T103" id="Seg_1338" s="T102">этот.[NOM.SG]</ta>
            <ta e="T104" id="Seg_1339" s="T103">охотиться-INF.LAT</ta>
            <ta e="T130" id="Seg_1340" s="T104">пойти-CVB</ta>
            <ta e="T105" id="Seg_1341" s="T130">исчезнуть-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1342" s="T105">другой.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1343" s="T106">женщина.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1344" s="T107">прийти-PST.[3SG]</ta>
            <ta e="T109" id="Seg_1345" s="T108">этот-ACC</ta>
            <ta e="T110" id="Seg_1346" s="T109">бросить-MOM-PST.[3SG]</ta>
            <ta e="T111" id="Seg_1347" s="T110">вода-LAT</ta>
            <ta e="T112" id="Seg_1348" s="T111">и</ta>
            <ta e="T113" id="Seg_1349" s="T112">прийти-PST.[3SG]</ta>
            <ta e="T114" id="Seg_1350" s="T113">мужчина.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1351" s="T114">сказать-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_1352" s="T115">надо</ta>
            <ta e="T117" id="Seg_1353" s="T116">убить-INF.LAT</ta>
            <ta e="T118" id="Seg_1354" s="T117">этот.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1355" s="T118">коза.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1356" s="T119">тогда</ta>
            <ta e="T121" id="Seg_1357" s="T120">этот.[NOM.SG]</ta>
            <ta e="T122" id="Seg_1358" s="T121">пойти-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1359" s="T122">вода-LAT</ta>
            <ta e="T124" id="Seg_1360" s="T123">плакать-DUR.[3SG]</ta>
            <ta e="T125" id="Seg_1361" s="T124">плакать-DUR.[3SG]</ta>
            <ta e="T126" id="Seg_1362" s="T125">я.ACC</ta>
            <ta e="T127" id="Seg_1363" s="T126">резать-INF.LAT</ta>
            <ta e="T128" id="Seg_1364" s="T127">хотеть.3SG</ta>
            <ta e="T129" id="Seg_1365" s="T128">кипятить-INF.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1366" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_1367" s="T1">num-num&gt;num</ta>
            <ta e="T3" id="Seg_1368" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_1369" s="T3">conj</ta>
            <ta e="T5" id="Seg_1370" s="T4">propr-n:case</ta>
            <ta e="T6" id="Seg_1371" s="T5">adv</ta>
            <ta e="T7" id="Seg_1372" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1373" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1374" s="T8">dempro-n:num</ta>
            <ta e="T10" id="Seg_1375" s="T9">que-n:case=ptcl</ta>
            <ta e="T11" id="Seg_1376" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_1377" s="T11">v-v:n.fin</ta>
            <ta e="T13" id="Seg_1378" s="T12">adv</ta>
            <ta e="T14" id="Seg_1379" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1380" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1381" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1382" s="T16">dempro-n:case</ta>
            <ta e="T18" id="Seg_1383" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1384" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_1385" s="T19">pers</ta>
            <ta e="T21" id="Seg_1386" s="T20">adv</ta>
            <ta e="T22" id="Seg_1387" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1388" s="T22">v-v:n.fin</ta>
            <ta e="T24" id="Seg_1389" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1390" s="T24">aux-v:mood.pn</ta>
            <ta e="T26" id="Seg_1391" s="T25">v-v:ins-v:mood.pn</ta>
            <ta e="T27" id="Seg_1392" s="T26">adv</ta>
            <ta e="T28" id="Seg_1393" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1394" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1395" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_1396" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1397" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1398" s="T32">adv</ta>
            <ta e="T34" id="Seg_1399" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_1400" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1401" s="T35">adv</ta>
            <ta e="T37" id="Seg_1402" s="T36">n-n:ins-n:case</ta>
            <ta e="T38" id="Seg_1403" s="T37">n-n:case.poss</ta>
            <ta e="T39" id="Seg_1404" s="T38">v-v&gt;v-v:pn</ta>
            <ta e="T40" id="Seg_1405" s="T39">dempro-n:case</ta>
            <ta e="T41" id="Seg_1406" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_1407" s="T41">aux-v:mood.pn</ta>
            <ta e="T43" id="Seg_1408" s="T42">v-v:ins-v:mood.pn</ta>
            <ta e="T44" id="Seg_1409" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1410" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1411" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1412" s="T46">adv</ta>
            <ta e="T48" id="Seg_1413" s="T47">v-v&gt;v-v:pn</ta>
            <ta e="T49" id="Seg_1414" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1415" s="T49">n-n:case.poss</ta>
            <ta e="T51" id="Seg_1416" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_1417" s="T51">adv</ta>
            <ta e="T53" id="Seg_1418" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_1419" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1420" s="T54">aux-v:mood.pn</ta>
            <ta e="T56" id="Seg_1421" s="T55">v-v:ins-v:mood.pn</ta>
            <ta e="T57" id="Seg_1422" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_1423" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1424" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_1425" s="T59">adv</ta>
            <ta e="T61" id="Seg_1426" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1427" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1428" s="T62">n-n:case.poss</ta>
            <ta e="T64" id="Seg_1429" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_1430" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_1431" s="T65">v-v&gt;v-v:pn</ta>
            <ta e="T67" id="Seg_1432" s="T66">aux-v:mood.pn</ta>
            <ta e="T68" id="Seg_1433" s="T67">v-v:ins-v:mood.pn</ta>
            <ta e="T69" id="Seg_1434" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1435" s="T69">n</ta>
            <ta e="T71" id="Seg_1436" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1437" s="T71">dempro-n:case</ta>
            <ta e="T73" id="Seg_1438" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1439" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1440" s="T74">conj</ta>
            <ta e="T76" id="Seg_1441" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1442" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1443" s="T77">adv</ta>
            <ta e="T79" id="Seg_1444" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_1445" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1446" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1447" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1448" s="T82">adv</ta>
            <ta e="T84" id="Seg_1449" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1450" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1451" s="T85">dempro-n:case</ta>
            <ta e="T87" id="Seg_1452" s="T86">propr-n:case</ta>
            <ta e="T88" id="Seg_1453" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1454" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1455" s="T89">conj</ta>
            <ta e="T91" id="Seg_1456" s="T90">n-n:case.poss</ta>
            <ta e="T92" id="Seg_1457" s="T91">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1458" s="T92">ptcl</ta>
            <ta e="T95" id="Seg_1459" s="T94">dempro-n:case</ta>
            <ta e="T96" id="Seg_1460" s="T95">propr-n:case</ta>
            <ta e="T97" id="Seg_1461" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_1462" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_1463" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_1464" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1465" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_1466" s="T101">adv</ta>
            <ta e="T103" id="Seg_1467" s="T102">dempro-n:case</ta>
            <ta e="T104" id="Seg_1468" s="T103">v-v:n.fin</ta>
            <ta e="T130" id="Seg_1469" s="T104">v-v:n-fin</ta>
            <ta e="T105" id="Seg_1470" s="T130">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1471" s="T105">adj-n:case</ta>
            <ta e="T107" id="Seg_1472" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1473" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1474" s="T108">dempro-n:case</ta>
            <ta e="T110" id="Seg_1475" s="T109">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1476" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_1477" s="T111">conj</ta>
            <ta e="T113" id="Seg_1478" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_1479" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_1480" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_1481" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1482" s="T116">v-v:n.fin</ta>
            <ta e="T118" id="Seg_1483" s="T117">dempro-n:case</ta>
            <ta e="T119" id="Seg_1484" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_1485" s="T119">adv</ta>
            <ta e="T121" id="Seg_1486" s="T120">dempro-n:case</ta>
            <ta e="T122" id="Seg_1487" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_1488" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1489" s="T123">v-v&gt;v-v:pn</ta>
            <ta e="T125" id="Seg_1490" s="T124">v-v&gt;v-v:pn</ta>
            <ta e="T126" id="Seg_1491" s="T125">pers</ta>
            <ta e="T127" id="Seg_1492" s="T126">v-v:n.fin</ta>
            <ta e="T128" id="Seg_1493" s="T127">v</ta>
            <ta e="T129" id="Seg_1494" s="T128">v-v:n.fin</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1495" s="T0">v</ta>
            <ta e="T2" id="Seg_1496" s="T1">num</ta>
            <ta e="T3" id="Seg_1497" s="T2">propr</ta>
            <ta e="T4" id="Seg_1498" s="T3">conj</ta>
            <ta e="T5" id="Seg_1499" s="T4">propr</ta>
            <ta e="T6" id="Seg_1500" s="T5">adv</ta>
            <ta e="T7" id="Seg_1501" s="T6">v</ta>
            <ta e="T8" id="Seg_1502" s="T7">v</ta>
            <ta e="T9" id="Seg_1503" s="T8">dempro</ta>
            <ta e="T10" id="Seg_1504" s="T9">que</ta>
            <ta e="T11" id="Seg_1505" s="T10">v</ta>
            <ta e="T12" id="Seg_1506" s="T11">v</ta>
            <ta e="T13" id="Seg_1507" s="T12">adv</ta>
            <ta e="T14" id="Seg_1508" s="T13">v</ta>
            <ta e="T15" id="Seg_1509" s="T14">v</ta>
            <ta e="T16" id="Seg_1510" s="T15">v</ta>
            <ta e="T17" id="Seg_1511" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1512" s="T17">n</ta>
            <ta e="T19" id="Seg_1513" s="T18">v</ta>
            <ta e="T20" id="Seg_1514" s="T19">pers</ta>
            <ta e="T21" id="Seg_1515" s="T20">adv</ta>
            <ta e="T22" id="Seg_1516" s="T21">n</ta>
            <ta e="T23" id="Seg_1517" s="T22">v</ta>
            <ta e="T24" id="Seg_1518" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1519" s="T24">aux</ta>
            <ta e="T26" id="Seg_1520" s="T25">v</ta>
            <ta e="T27" id="Seg_1521" s="T26">adv</ta>
            <ta e="T28" id="Seg_1522" s="T27">v</ta>
            <ta e="T29" id="Seg_1523" s="T28">n</ta>
            <ta e="T30" id="Seg_1524" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_1525" s="T30">n</ta>
            <ta e="T32" id="Seg_1526" s="T31">v</ta>
            <ta e="T33" id="Seg_1527" s="T32">adv</ta>
            <ta e="T34" id="Seg_1528" s="T33">v</ta>
            <ta e="T35" id="Seg_1529" s="T34">v</ta>
            <ta e="T36" id="Seg_1530" s="T35">adv</ta>
            <ta e="T37" id="Seg_1531" s="T36">n</ta>
            <ta e="T38" id="Seg_1532" s="T37">n</ta>
            <ta e="T39" id="Seg_1533" s="T38">v</ta>
            <ta e="T40" id="Seg_1534" s="T39">dempro</ta>
            <ta e="T41" id="Seg_1535" s="T40">v</ta>
            <ta e="T42" id="Seg_1536" s="T41">aux</ta>
            <ta e="T43" id="Seg_1537" s="T42">v</ta>
            <ta e="T44" id="Seg_1538" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1539" s="T44">n</ta>
            <ta e="T46" id="Seg_1540" s="T45">v</ta>
            <ta e="T47" id="Seg_1541" s="T46">adv</ta>
            <ta e="T48" id="Seg_1542" s="T47">v</ta>
            <ta e="T49" id="Seg_1543" s="T48">n</ta>
            <ta e="T50" id="Seg_1544" s="T49">n</ta>
            <ta e="T51" id="Seg_1545" s="T50">v</ta>
            <ta e="T52" id="Seg_1546" s="T51">adv</ta>
            <ta e="T53" id="Seg_1547" s="T52">v</ta>
            <ta e="T54" id="Seg_1548" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1549" s="T54">aux</ta>
            <ta e="T56" id="Seg_1550" s="T55">v</ta>
            <ta e="T57" id="Seg_1551" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_1552" s="T57">n</ta>
            <ta e="T59" id="Seg_1553" s="T58">v</ta>
            <ta e="T60" id="Seg_1554" s="T59">adv</ta>
            <ta e="T61" id="Seg_1555" s="T60">v</ta>
            <ta e="T62" id="Seg_1556" s="T61">n</ta>
            <ta e="T63" id="Seg_1557" s="T62">n</ta>
            <ta e="T64" id="Seg_1558" s="T63">v</ta>
            <ta e="T65" id="Seg_1559" s="T64">dempro</ta>
            <ta e="T66" id="Seg_1560" s="T65">v</ta>
            <ta e="T67" id="Seg_1561" s="T66">aux</ta>
            <ta e="T68" id="Seg_1562" s="T67">v</ta>
            <ta e="T69" id="Seg_1563" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1564" s="T69">n</ta>
            <ta e="T71" id="Seg_1565" s="T70">v</ta>
            <ta e="T72" id="Seg_1566" s="T71">dempro</ta>
            <ta e="T73" id="Seg_1567" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1568" s="T73">v</ta>
            <ta e="T75" id="Seg_1569" s="T74">conj</ta>
            <ta e="T76" id="Seg_1570" s="T75">n</ta>
            <ta e="T77" id="Seg_1571" s="T76">v</ta>
            <ta e="T78" id="Seg_1572" s="T77">adv</ta>
            <ta e="T79" id="Seg_1573" s="T78">propr</ta>
            <ta e="T80" id="Seg_1574" s="T79">v</ta>
            <ta e="T81" id="Seg_1575" s="T80">v</ta>
            <ta e="T82" id="Seg_1576" s="T81">v</ta>
            <ta e="T83" id="Seg_1577" s="T82">adv</ta>
            <ta e="T84" id="Seg_1578" s="T83">v</ta>
            <ta e="T85" id="Seg_1579" s="T84">n</ta>
            <ta e="T86" id="Seg_1580" s="T85">dempro</ta>
            <ta e="T87" id="Seg_1581" s="T86">propr</ta>
            <ta e="T88" id="Seg_1582" s="T87">v</ta>
            <ta e="T89" id="Seg_1583" s="T88">n</ta>
            <ta e="T90" id="Seg_1584" s="T89">conj</ta>
            <ta e="T91" id="Seg_1585" s="T90">n</ta>
            <ta e="T92" id="Seg_1586" s="T91">v</ta>
            <ta e="T93" id="Seg_1587" s="T92">ptcl</ta>
            <ta e="T95" id="Seg_1588" s="T94">dempro</ta>
            <ta e="T96" id="Seg_1589" s="T95">propr</ta>
            <ta e="T97" id="Seg_1590" s="T96">v</ta>
            <ta e="T98" id="Seg_1591" s="T97">n</ta>
            <ta e="T99" id="Seg_1592" s="T98">dempro</ta>
            <ta e="T100" id="Seg_1593" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1594" s="T100">v</ta>
            <ta e="T102" id="Seg_1595" s="T101">adv</ta>
            <ta e="T103" id="Seg_1596" s="T102">dempro</ta>
            <ta e="T104" id="Seg_1597" s="T103">v</ta>
            <ta e="T130" id="Seg_1598" s="T104">v</ta>
            <ta e="T105" id="Seg_1599" s="T130">v</ta>
            <ta e="T106" id="Seg_1600" s="T105">adj</ta>
            <ta e="T107" id="Seg_1601" s="T106">n</ta>
            <ta e="T108" id="Seg_1602" s="T107">v</ta>
            <ta e="T109" id="Seg_1603" s="T108">dempro</ta>
            <ta e="T110" id="Seg_1604" s="T109">v</ta>
            <ta e="T111" id="Seg_1605" s="T110">n</ta>
            <ta e="T112" id="Seg_1606" s="T111">conj</ta>
            <ta e="T113" id="Seg_1607" s="T112">v</ta>
            <ta e="T114" id="Seg_1608" s="T113">n</ta>
            <ta e="T115" id="Seg_1609" s="T114">v</ta>
            <ta e="T116" id="Seg_1610" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1611" s="T116">v</ta>
            <ta e="T118" id="Seg_1612" s="T117">dempro</ta>
            <ta e="T119" id="Seg_1613" s="T118">n</ta>
            <ta e="T120" id="Seg_1614" s="T119">adv</ta>
            <ta e="T121" id="Seg_1615" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1616" s="T121">v</ta>
            <ta e="T123" id="Seg_1617" s="T122">n</ta>
            <ta e="T124" id="Seg_1618" s="T123">v</ta>
            <ta e="T125" id="Seg_1619" s="T124">v</ta>
            <ta e="T126" id="Seg_1620" s="T125">pers</ta>
            <ta e="T127" id="Seg_1621" s="T126">v</ta>
            <ta e="T128" id="Seg_1622" s="T127">v</ta>
            <ta e="T129" id="Seg_1623" s="T128">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1624" s="T2">np.h:E</ta>
            <ta e="T5" id="Seg_1625" s="T4">np.h:E</ta>
            <ta e="T6" id="Seg_1626" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_1627" s="T6">0.3.h:E</ta>
            <ta e="T8" id="Seg_1628" s="T7">0.3.h:E</ta>
            <ta e="T9" id="Seg_1629" s="T8">pro.h:Th</ta>
            <ta e="T10" id="Seg_1630" s="T9">pro:Th</ta>
            <ta e="T13" id="Seg_1631" s="T12">adv:Time</ta>
            <ta e="T14" id="Seg_1632" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_1633" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_1634" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_1635" s="T17">np.h:A</ta>
            <ta e="T20" id="Seg_1636" s="T19">pro.h:E</ta>
            <ta e="T22" id="Seg_1637" s="T21">np:P</ta>
            <ta e="T25" id="Seg_1638" s="T24">0.2.h:A</ta>
            <ta e="T27" id="Seg_1639" s="T26">adv:L</ta>
            <ta e="T29" id="Seg_1640" s="T28">np:A</ta>
            <ta e="T31" id="Seg_1641" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1642" s="T31">0.2.h:P</ta>
            <ta e="T33" id="Seg_1643" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_1644" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_1645" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_1646" s="T35">adv:Time</ta>
            <ta e="T37" id="Seg_1647" s="T36">np:Poss</ta>
            <ta e="T38" id="Seg_1648" s="T37">np:Th</ta>
            <ta e="T40" id="Seg_1649" s="T39">pro.h:A</ta>
            <ta e="T42" id="Seg_1650" s="T41">0.2.h:A</ta>
            <ta e="T45" id="Seg_1651" s="T44">np:Th</ta>
            <ta e="T46" id="Seg_1652" s="T45">0.2.h:P</ta>
            <ta e="T47" id="Seg_1653" s="T46">adv:Time</ta>
            <ta e="T48" id="Seg_1654" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_1655" s="T48">np:Poss</ta>
            <ta e="T50" id="Seg_1656" s="T49">np:Th</ta>
            <ta e="T55" id="Seg_1657" s="T54">0.2.h:A</ta>
            <ta e="T58" id="Seg_1658" s="T57">np:Th</ta>
            <ta e="T59" id="Seg_1659" s="T58">0.2.h:P</ta>
            <ta e="T60" id="Seg_1660" s="T59">adv:Time</ta>
            <ta e="T61" id="Seg_1661" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_1662" s="T61">np:Poss</ta>
            <ta e="T63" id="Seg_1663" s="T62">np:Th</ta>
            <ta e="T65" id="Seg_1664" s="T64">pro.h:A</ta>
            <ta e="T66" id="Seg_1665" s="T65">0.2.h:A</ta>
            <ta e="T70" id="Seg_1666" s="T69">np:Th</ta>
            <ta e="T71" id="Seg_1667" s="T70">0.2.h:P</ta>
            <ta e="T72" id="Seg_1668" s="T71">pro.h:A</ta>
            <ta e="T76" id="Seg_1669" s="T75">np:Th</ta>
            <ta e="T77" id="Seg_1670" s="T76">0.3.h:P</ta>
            <ta e="T78" id="Seg_1671" s="T77">adv:Time</ta>
            <ta e="T79" id="Seg_1672" s="T78">np.h:E</ta>
            <ta e="T81" id="Seg_1673" s="T80">0.3.h:E</ta>
            <ta e="T82" id="Seg_1674" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1675" s="T82">adv:Time</ta>
            <ta e="T85" id="Seg_1676" s="T84">np.h:A</ta>
            <ta e="T86" id="Seg_1677" s="T85">pro.h:A</ta>
            <ta e="T87" id="Seg_1678" s="T86">np.h:Th</ta>
            <ta e="T91" id="Seg_1679" s="T90">np:G</ta>
            <ta e="T92" id="Seg_1680" s="T91">0.3.h:A</ta>
            <ta e="T96" id="Seg_1681" s="T95">np.h:P</ta>
            <ta e="T98" id="Seg_1682" s="T97">np:Th</ta>
            <ta e="T99" id="Seg_1683" s="T98">np.h:B</ta>
            <ta e="T101" id="Seg_1684" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_1685" s="T101">adv:Time</ta>
            <ta e="T103" id="Seg_1686" s="T102">pro.h:A</ta>
            <ta e="T107" id="Seg_1687" s="T106">np.h:A</ta>
            <ta e="T109" id="Seg_1688" s="T108">pro.h:Th</ta>
            <ta e="T110" id="Seg_1689" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_1690" s="T110">np:G</ta>
            <ta e="T114" id="Seg_1691" s="T113">np.h:A</ta>
            <ta e="T115" id="Seg_1692" s="T114">0.3.h:A</ta>
            <ta e="T119" id="Seg_1693" s="T118">np.h:P</ta>
            <ta e="T120" id="Seg_1694" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_1695" s="T120">pro.h:A</ta>
            <ta e="T123" id="Seg_1696" s="T122">np:G</ta>
            <ta e="T124" id="Seg_1697" s="T123">0.3.h:E</ta>
            <ta e="T125" id="Seg_1698" s="T124">0.3.h:E</ta>
            <ta e="T126" id="Seg_1699" s="T125">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1700" s="T0">v:pred</ta>
            <ta e="T3" id="Seg_1701" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_1702" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_1703" s="T6">v:pred 0.3.h:S</ta>
            <ta e="T8" id="Seg_1704" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T9" id="Seg_1705" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1706" s="T9">pro:O</ta>
            <ta e="T11" id="Seg_1707" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_1708" s="T11">s:purp</ta>
            <ta e="T14" id="Seg_1709" s="T13">v:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_1710" s="T14">v:pred 0.3.h:S</ta>
            <ta e="T16" id="Seg_1711" s="T15">v:pred 0.3.h:S</ta>
            <ta e="T18" id="Seg_1712" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_1713" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_1714" s="T21">np:O</ta>
            <ta e="T23" id="Seg_1715" s="T22">s:purp</ta>
            <ta e="T24" id="Seg_1716" s="T23">ptcl:pred</ta>
            <ta e="T25" id="Seg_1717" s="T24">v:pred 0.2.h:S</ta>
            <ta e="T28" id="Seg_1718" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_1719" s="T28">np:S</ta>
            <ta e="T31" id="Seg_1720" s="T30">n:pred</ta>
            <ta e="T32" id="Seg_1721" s="T31">cop 0.2.h:S</ta>
            <ta e="T34" id="Seg_1722" s="T33">v:pred 0.3.h:S</ta>
            <ta e="T35" id="Seg_1723" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T38" id="Seg_1724" s="T37">np:S</ta>
            <ta e="T39" id="Seg_1725" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_1726" s="T39">pro.h:S</ta>
            <ta e="T41" id="Seg_1727" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_1728" s="T41">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_1729" s="T44">n:pred</ta>
            <ta e="T46" id="Seg_1730" s="T45">cop 0.2.h:S</ta>
            <ta e="T48" id="Seg_1731" s="T47">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_1732" s="T49">np:S</ta>
            <ta e="T51" id="Seg_1733" s="T50">v:pred</ta>
            <ta e="T54" id="Seg_1734" s="T53">ptcl:pred</ta>
            <ta e="T55" id="Seg_1735" s="T54">v:pred 0.2.h:S</ta>
            <ta e="T58" id="Seg_1736" s="T57">n:pred</ta>
            <ta e="T59" id="Seg_1737" s="T58">cop 0.2.h:S</ta>
            <ta e="T61" id="Seg_1738" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_1739" s="T62">np:S</ta>
            <ta e="T64" id="Seg_1740" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_1741" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_1742" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1743" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T70" id="Seg_1744" s="T69">n:pred</ta>
            <ta e="T71" id="Seg_1745" s="T70">cop 0.2.h:S</ta>
            <ta e="T72" id="Seg_1746" s="T71">pro.h:S</ta>
            <ta e="T74" id="Seg_1747" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_1748" s="T75">n:pred</ta>
            <ta e="T77" id="Seg_1749" s="T76">cop 0.3.h:S</ta>
            <ta e="T79" id="Seg_1750" s="T78">np.h:S</ta>
            <ta e="T80" id="Seg_1751" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1752" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_1753" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_1754" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_1755" s="T84">np.h:S</ta>
            <ta e="T86" id="Seg_1756" s="T85">pro.h:S</ta>
            <ta e="T87" id="Seg_1757" s="T86">np.h:O</ta>
            <ta e="T88" id="Seg_1758" s="T87">v:pred</ta>
            <ta e="T92" id="Seg_1759" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T96" id="Seg_1760" s="T95">np.h:S</ta>
            <ta e="T97" id="Seg_1761" s="T96">cop</ta>
            <ta e="T98" id="Seg_1762" s="T97">n:pred</ta>
            <ta e="T99" id="Seg_1763" s="T98">np.h:O</ta>
            <ta e="T101" id="Seg_1764" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T103" id="Seg_1765" s="T102">pro.h:S</ta>
            <ta e="T104" id="Seg_1766" s="T103">s:purp</ta>
            <ta e="T130" id="Seg_1767" s="T104">conv:pred</ta>
            <ta e="T105" id="Seg_1768" s="T130">v:pred</ta>
            <ta e="T107" id="Seg_1769" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_1770" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_1771" s="T108">pro.h:O</ta>
            <ta e="T110" id="Seg_1772" s="T109">v:pred 0.3.h:S</ta>
            <ta e="T113" id="Seg_1773" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_1774" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_1775" s="T114">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_1776" s="T115">ptcl:pred</ta>
            <ta e="T119" id="Seg_1777" s="T118">np.h:O</ta>
            <ta e="T121" id="Seg_1778" s="T120">pro.h:S</ta>
            <ta e="T122" id="Seg_1779" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_1780" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_1781" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_1782" s="T125">pro.h:O</ta>
            <ta e="T128" id="Seg_1783" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_1784" s="T128">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1785" s="T3">RUS:gram</ta>
            <ta e="T10" id="Seg_1786" s="T9">TURK:gram(INDEF)</ta>
            <ta e="T24" id="Seg_1787" s="T23">RUS:mod</ta>
            <ta e="T29" id="Seg_1788" s="T28">TURK:cult</ta>
            <ta e="T30" id="Seg_1789" s="T29">RUS:gram</ta>
            <ta e="T31" id="Seg_1790" s="T30">TURK:cult</ta>
            <ta e="T44" id="Seg_1791" s="T43">RUS:gram</ta>
            <ta e="T54" id="Seg_1792" s="T53">RUS:mod</ta>
            <ta e="T57" id="Seg_1793" s="T56">RUS:gram</ta>
            <ta e="T69" id="Seg_1794" s="T68">RUS:gram</ta>
            <ta e="T73" id="Seg_1795" s="T72">TURK:disc</ta>
            <ta e="T75" id="Seg_1796" s="T74">RUS:gram</ta>
            <ta e="T90" id="Seg_1797" s="T89">RUS:gram</ta>
            <ta e="T100" id="Seg_1798" s="T99">RUS:mod</ta>
            <ta e="T106" id="Seg_1799" s="T105">TURK:core</ta>
            <ta e="T112" id="Seg_1800" s="T111">RUS:gram</ta>
            <ta e="T116" id="Seg_1801" s="T115">RUS:mod</ta>
            <ta e="T128" id="Seg_1802" s="T127">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T128" id="Seg_1803" s="T127">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1804" s="T0">Жили двое, Аленушка и Иванушка.</ta>
            <ta e="T8" id="Seg_1805" s="T5">Они жили, жили.</ta>
            <ta e="T12" id="Seg_1806" s="T8">Им было нечего есть.</ta>
            <ta e="T16" id="Seg_1807" s="T12">Потом они пошли, шли, шли.</ta>
            <ta e="T24" id="Seg_1808" s="T16">Мальчик говорит: "Я очень хочу пить".</ta>
            <ta e="T27" id="Seg_1809" s="T24">"Не пей отсюда!</ta>
            <ta e="T32" id="Seg_1810" s="T27">Корова прошла, а то ты станешь коровой!"</ta>
            <ta e="T39" id="Seg_1811" s="T32">Потом они шли-шли, [видят] след овцы.</ta>
            <ta e="T46" id="Seg_1812" s="T39">Она говорит: «Не пей, а то овцой станешь!»</ta>
            <ta e="T48" id="Seg_1813" s="T46">Они идут.</ta>
            <ta e="T51" id="Seg_1814" s="T48">Лошадиный след стоит [=есть].</ta>
            <ta e="T54" id="Seg_1815" s="T51">"Я очень хочу пить".</ta>
            <ta e="T59" id="Seg_1816" s="T54">"Не пей, а то станешь лошадью".</ta>
            <ta e="T61" id="Seg_1817" s="T59">Они идут.</ta>
            <ta e="T64" id="Seg_1818" s="T61">Козий след стоит [=есть].</ta>
            <ta e="T71" id="Seg_1819" s="T64">Она говорит: "Не пей, а то козлёнком станешь".</ta>
            <ta e="T77" id="Seg_1820" s="T71">Но он выпил и стал козлёнком.</ta>
            <ta e="T81" id="Seg_1821" s="T77">Тогда Аленушка плакала-плакала.</ta>
            <ta e="T82" id="Seg_1822" s="T81">Они идут.</ta>
            <ta e="T85" id="Seg_1823" s="T82">Идет человек.</ta>
            <ta e="T92" id="Seg_1824" s="T85">Он взял Аленушку замуж и привел домой.</ta>
            <ta e="T98" id="Seg_1825" s="T92">Этот Ванюшка стал козлёнком.</ta>
            <ta e="T101" id="Seg_1826" s="T98">Его тоже кормят.</ta>
            <ta e="T105" id="Seg_1827" s="T101">Потом тот [мужчина] ушел охотиться.</ta>
            <ta e="T108" id="Seg_1828" s="T105">Другая женщина пришла.</ta>
            <ta e="T111" id="Seg_1829" s="T108">И бросила её в реку.</ta>
            <ta e="T114" id="Seg_1830" s="T111">Пришел мужчина.</ta>
            <ta e="T119" id="Seg_1831" s="T114">[Она] говорит: "Надо убить этого козлёнка".</ta>
            <ta e="T125" id="Seg_1832" s="T119">Тогда он пошел к реке, плачет, плачет.</ta>
            <ta e="T128" id="Seg_1833" s="T125">"Она хочет меня зарезать.</ta>
            <ta e="T129" id="Seg_1834" s="T128">[И] сварить".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1835" s="T0">There lived two [persons], Alyonushka and Ivanushka.</ta>
            <ta e="T8" id="Seg_1836" s="T5">Then they lived, they lived.</ta>
            <ta e="T12" id="Seg_1837" s="T8">They had nothing to eat.</ta>
            <ta e="T16" id="Seg_1838" s="T12">Then they went, they came, they came.</ta>
            <ta e="T24" id="Seg_1839" s="T16">The boy says: "I want very much to drink water [= I am very thirsty]."</ta>
            <ta e="T27" id="Seg_1840" s="T24">"Don't drink from there!</ta>
            <ta e="T32" id="Seg_1841" s="T27">A cow came, or you will become a cow!"</ta>
            <ta e="T39" id="Seg_1842" s="T32">Then they went, they went, then a sheep's foot is standing [= there is a sheep's footptint].</ta>
            <ta e="T46" id="Seg_1843" s="T39">[She] says: "Do not drink, or you will become a sheep!"</ta>
            <ta e="T48" id="Seg_1844" s="T46">Then they are going.</ta>
            <ta e="T51" id="Seg_1845" s="T48">A horse's foot is standing [= there is].</ta>
            <ta e="T54" id="Seg_1846" s="T51">"I am vert thirsty."</ta>
            <ta e="T59" id="Seg_1847" s="T54">"Don't drink, or you will become a horse."</ta>
            <ta e="T61" id="Seg_1848" s="T59">Then they are coming.</ta>
            <ta e="T64" id="Seg_1849" s="T61">A goat's foot is standing.</ta>
            <ta e="T71" id="Seg_1850" s="T64">She says: "Don't drink, or you will become a goat."</ta>
            <ta e="T77" id="Seg_1851" s="T71">But he drank and became a goat.</ta>
            <ta e="T81" id="Seg_1852" s="T77">Then Alyonushka cried, cried.</ta>
            <ta e="T82" id="Seg_1853" s="T81">They went.</ta>
            <ta e="T85" id="Seg_1854" s="T82">Then a man comes.</ta>
            <ta e="T92" id="Seg_1855" s="T85">He took Alyonushka as his wife and took her home.</ta>
            <ta e="T98" id="Seg_1856" s="T92">This Vanyushka became a goat.</ta>
            <ta e="T101" id="Seg_1857" s="T98">They feed him too.</ta>
            <ta e="T105" id="Seg_1858" s="T101">Then this [man] went to hunt.</ta>
            <ta e="T108" id="Seg_1859" s="T105">Another woman came.</ta>
            <ta e="T111" id="Seg_1860" s="T108">And threw her to the river.</ta>
            <ta e="T114" id="Seg_1861" s="T111">And the man came.</ta>
            <ta e="T119" id="Seg_1862" s="T114">[She] says: "[We] should kill this goat."</ta>
            <ta e="T125" id="Seg_1863" s="T119">Then he went to the river, he is crying, he is crying.</ta>
            <ta e="T128" id="Seg_1864" s="T125">"She wants to kill me.</ta>
            <ta e="T129" id="Seg_1865" s="T128">[And] cook."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1866" s="T0">Es lebten zwei [Leute], Aljonuschka und Ivanuschka.</ta>
            <ta e="T8" id="Seg_1867" s="T5">Dann lebten sie und lebten.</ta>
            <ta e="T12" id="Seg_1868" s="T8">Sie hatten nichts zu essen.</ta>
            <ta e="T16" id="Seg_1869" s="T12">Dann gingen sie, sie kamen, sie kamen.</ta>
            <ta e="T24" id="Seg_1870" s="T16">Der Junge sagt: "Ich möchte sehr gerne Wasser trinken [= ich bin sehr durstig]."</ta>
            <ta e="T27" id="Seg_1871" s="T24">"Trinke nicht von dort!</ta>
            <ta e="T32" id="Seg_1872" s="T27">Eine Kuh ist gekommen, oder du wirst zu einer Kuh!"</ta>
            <ta e="T39" id="Seg_1873" s="T32">Dann gingen sie, sie gingen, der Fußabdruck eines Schafes steht [= ist da].</ta>
            <ta e="T46" id="Seg_1874" s="T39">[Sie] sagt: "Trinke nicht oder du wirst zu einem Schaf!"</ta>
            <ta e="T48" id="Seg_1875" s="T46">Dann gehen sie.</ta>
            <ta e="T51" id="Seg_1876" s="T48">Der Fußabdruck eines Pferdes steht [= ist da].</ta>
            <ta e="T54" id="Seg_1877" s="T51">"Ich bin sehr durstig."</ta>
            <ta e="T59" id="Seg_1878" s="T54">"Trinke nicht oder du wirst zu einem Pferd."</ta>
            <ta e="T61" id="Seg_1879" s="T59">Dann kommen sie.</ta>
            <ta e="T64" id="Seg_1880" s="T61">Der Fußabdruck einer Ziege steht [=ist da].</ta>
            <ta e="T71" id="Seg_1881" s="T64">Sie sagt: "Trinke nicht oder du wirst zu einer Ziege."</ta>
            <ta e="T77" id="Seg_1882" s="T71">Aber er trank und wurde zu einer Ziege.</ta>
            <ta e="T81" id="Seg_1883" s="T77">Dann weinte Aljonuschka und weinte.</ta>
            <ta e="T82" id="Seg_1884" s="T81">Sie gingen.</ta>
            <ta e="T85" id="Seg_1885" s="T82">Dann kam ein Mann.</ta>
            <ta e="T92" id="Seg_1886" s="T85">Er nahm Aljonuschka zur Frau und nahm sie mit nach Hause.</ta>
            <ta e="T98" id="Seg_1887" s="T92">Dieser Vanjuschka wurde zu einer Ziege.</ta>
            <ta e="T101" id="Seg_1888" s="T98">Sie fütteren ihn auch.</ta>
            <ta e="T105" id="Seg_1889" s="T101">Dann ging dieser [Mann] jagen.</ta>
            <ta e="T108" id="Seg_1890" s="T105">Eine andere Frau kam.</ta>
            <ta e="T111" id="Seg_1891" s="T108">Und werfte sie zum Fluss.</ta>
            <ta e="T114" id="Seg_1892" s="T111">Und der Mann kam.</ta>
            <ta e="T119" id="Seg_1893" s="T114">[Sie] sagt: "[Wir] müssen diese Ziege töten."</ta>
            <ta e="T125" id="Seg_1894" s="T119">Dann ging er zum Fluss, er weint und weint.</ta>
            <ta e="T128" id="Seg_1895" s="T125">"Sie möchte mich töten.</ta>
            <ta e="T129" id="Seg_1896" s="T128">[Und] kochen."</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T32" id="Seg_1897" s="T27">[GVY:] That is, don't drink from the cow's footprint.</ta>
            <ta e="T51" id="Seg_1898" s="T48">[GVY:] A horse's footprint</ta>
            <ta e="T111" id="Seg_1899" s="T108">[GVY:] This woman, a witch, let Alyonushka to the river and pushed in the water.</ta>
            <ta e="T129" id="Seg_1900" s="T128">[AAV] Continued in PKZ_196X_Alenushka_continuation_flk, AEDKL SU0197.  </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T130" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
