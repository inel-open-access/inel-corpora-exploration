<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3902DBF2-41AB-1E4E-C7C8-C7A43BCA5A25">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ChildrenAndRye_flk.wav" />
         <referenced-file url="PKZ_196X_ChildrenAndRye_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ChildrenAndRye_flk\PKZ_196X_ChildrenAndRye_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">115</ud-information>
            <ud-information attribute-name="# HIAT:w">71</ud-information>
            <ud-information attribute-name="# e">72</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T488" time="0.098" type="appl" />
         <tli id="T489" time="0.966" type="appl" />
         <tli id="T490" time="1.833" type="appl" />
         <tli id="T491" time="3.159670286501936" />
         <tli id="T492" time="4.09" type="appl" />
         <tli id="T493" time="4.965" type="appl" />
         <tli id="T494" time="5.841" type="appl" />
         <tli id="T495" time="7.366" type="appl" />
         <tli id="T496" time="9.0657206532545" />
         <tli id="T497" time="9.905" type="appl" />
         <tli id="T498" time="10.92" type="appl" />
         <tli id="T499" time="11.934" type="appl" />
         <tli id="T500" time="12.949" type="appl" />
         <tli id="T501" time="13.964" type="appl" />
         <tli id="T502" time="14.919006343741156" />
         <tli id="T503" time="15.863" type="appl" />
         <tli id="T504" time="16.746" type="appl" />
         <tli id="T505" time="17.918130232314777" />
         <tli id="T506" time="18.316" type="appl" />
         <tli id="T507" time="18.948" type="appl" />
         <tli id="T508" time="19.579" type="appl" />
         <tli id="T509" time="20.211" type="appl" />
         <tli id="T510" time="20.842" type="appl" />
         <tli id="T511" time="21.474" type="appl" />
         <tli id="T512" time="22.105" type="appl" />
         <tli id="T513" time="22.737" type="appl" />
         <tli id="T514" time="23.368" type="appl" />
         <tli id="T515" time="24.432" type="appl" />
         <tli id="T516" time="25.495" type="appl" />
         <tli id="T517" time="26.558" type="appl" />
         <tli id="T518" time="27.728655977209225" />
         <tli id="T519" time="28.691" type="appl" />
         <tli id="T520" time="29.76" type="appl" />
         <tli id="T521" time="30.83" type="appl" />
         <tli id="T522" time="31.899" type="appl" />
         <tli id="T523" time="32.968" type="appl" />
         <tli id="T524" time="34.037" type="appl" />
         <tli id="T525" time="34.764" type="appl" />
         <tli id="T526" time="35.488" type="appl" />
         <tli id="T527" time="36.212" type="appl" />
         <tli id="T528" time="37.27611021544056" />
         <tli id="T529" time="37.971" type="appl" />
         <tli id="T530" time="38.669" type="appl" />
         <tli id="T531" time="39.366" type="appl" />
         <tli id="T532" time="40.064" type="appl" />
         <tli id="T533" time="41.155705377348" />
         <tli id="T534" time="41.684" type="appl" />
         <tli id="T535" time="42.266" type="appl" />
         <tli id="T536" time="42.849" type="appl" />
         <tli id="T537" time="43.431" type="appl" />
         <tli id="T538" time="44.758" type="appl" />
         <tli id="T539" time="46.085" type="appl" />
         <tli id="T540" time="47.412" type="appl" />
         <tli id="T541" time="48.738" type="appl" />
         <tli id="T542" time="50.065" type="appl" />
         <tli id="T543" time="51.392" type="appl" />
         <tli id="T544" time="52.051" type="appl" />
         <tli id="T545" time="52.71" type="appl" />
         <tli id="T546" time="53.368" type="appl" />
         <tli id="T547" time="54.027" type="appl" />
         <tli id="T548" time="54.686" type="appl" />
         <tli id="T549" time="55.48087720370383" />
         <tli id="T550" time="56.072" type="appl" />
         <tli id="T551" time="56.8" type="appl" />
         <tli id="T552" time="58.77386691157715" />
         <tli id="T553" time="59.438" type="appl" />
         <tli id="T554" time="60.017" type="appl" />
         <tli id="T555" time="60.596" type="appl" />
         <tli id="T556" time="61.174" type="appl" />
         <tli id="T557" time="61.753" type="appl" />
         <tli id="T558" time="62.03352676410763" />
         <tli id="T0" time="62.86676338205382" type="intp" />
         <tli id="T559" time="63.7" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T559" id="Seg_0" n="sc" s="T488">
               <ts e="T491" id="Seg_2" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_4" n="HIAT:w" s="T488">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_7" n="HIAT:w" s="T489">kagaʔi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_10" n="HIAT:w" s="T490">amnobiʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_14" n="HIAT:u" s="T491">
                  <nts id="Seg_15" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_17" n="HIAT:w" s="T491">A-</ts>
                  <nts id="Seg_18" n="HIAT:ip">)</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_21" n="HIAT:w" s="T492">Aš</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_24" n="HIAT:w" s="T493">kuʔpiʔi</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_28" n="HIAT:u" s="T494">
                  <nts id="Seg_29" n="HIAT:ip">(</nts>
                  <ts e="T495" id="Seg_31" n="HIAT:w" s="T494">Tarə-</ts>
                  <nts id="Seg_32" n="HIAT:ip">)</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_35" n="HIAT:w" s="T495">Tararluʔpiʔi</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_39" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_41" n="HIAT:w" s="T496">Dĭgəttə</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <ts e="T498" id="Seg_45" n="HIAT:w" s="T497">dĭ-</ts>
                  <nts id="Seg_46" n="HIAT:ip">)</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_49" n="HIAT:w" s="T498">onʼiʔ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_52" n="HIAT:w" s="T499">esseŋdə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_56" n="HIAT:w" s="T500">iʔgö</ts>
                  <nts id="Seg_57" n="HIAT:ip">)</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_60" n="HIAT:w" s="T501">ibiʔi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_64" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_66" n="HIAT:w" s="T502">Onʼiʔ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_69" n="HIAT:w" s="T503">naga</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_72" n="HIAT:w" s="T504">esseŋdə</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_76" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_78" n="HIAT:w" s="T505">Dĭgəttə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_81" n="HIAT:w" s="T506">dĭn</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_84" n="HIAT:w" s="T507">nebə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_87" n="HIAT:w" s="T508">mălia:</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">"</nts>
                  <ts e="T510" id="Seg_91" n="HIAT:w" s="T509">Dĭn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_94" n="HIAT:w" s="T510">esseŋdə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_97" n="HIAT:w" s="T511">iʔgö</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_101" n="HIAT:w" s="T512">davaj</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_104" n="HIAT:w" s="T513">kanžəbəj</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip">"</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_109" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_111" n="HIAT:w" s="T514">Dĭgəttə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_114" n="HIAT:w" s="T515">dĭzeŋ</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_117" n="HIAT:w" s="T516">delʼitsʼa</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_120" n="HIAT:w" s="T517">stalʼi</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_124" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_126" n="HIAT:w" s="T518">Dĭgəttə</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_129" n="HIAT:w" s="T519">dĭ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_131" n="HIAT:ip">(</nts>
                  <ts e="T521" id="Seg_133" n="HIAT:w" s="T520">kalləj</ts>
                  <nts id="Seg_134" n="HIAT:ip">)</nts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_138" n="HIAT:w" s="T521">girgit</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_141" n="HIAT:w" s="T522">esseŋdə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_144" n="HIAT:w" s="T523">iʔgö</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_148" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_150" n="HIAT:w" s="T524">Dĭ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_153" n="HIAT:w" s="T525">brattə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_156" n="HIAT:w" s="T526">kămnaʔbə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_159" n="HIAT:w" s="T527">ipek</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_163" n="HIAT:u" s="T528">
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T529" id="Seg_166" n="HIAT:w" s="T528">Dĭn</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_169" n="HIAT:w" s="T529">esseŋdə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_172" n="HIAT:w" s="T530">naga</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_176" n="HIAT:w" s="T531">šində</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_179" n="HIAT:w" s="T532">mĭləj</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_183" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_185" n="HIAT:w" s="T533">A</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_188" n="HIAT:w" s="T534">măna</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_191" n="HIAT:w" s="T535">esseŋbə</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_194" n="HIAT:w" s="T536">mĭləʔi</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_199" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_201" n="HIAT:w" s="T537">Dĭgəttə</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">(</nts>
                  <ts e="T539" id="Seg_205" n="HIAT:w" s="T538">dĭ=</ts>
                  <nts id="Seg_206" n="HIAT:ip">)</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_209" n="HIAT:w" s="T539">dĭ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_212" n="HIAT:w" s="T540">dĭʔnə</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_215" n="HIAT:w" s="T541">kămlaʔbə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_218" n="HIAT:w" s="T542">aš</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_222" n="HIAT:u" s="T543">
                  <nts id="Seg_223" n="HIAT:ip">"</nts>
                  <ts e="T544" id="Seg_225" n="HIAT:w" s="T543">Dĭn</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_228" n="HIAT:w" s="T544">esseŋdə</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_231" n="HIAT:w" s="T545">iʔgö</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_235" n="HIAT:w" s="T546">pušaj</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_238" n="HIAT:w" s="T547">dĭʔnə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_241" n="HIAT:w" s="T548">moləj</ts>
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_246" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_248" n="HIAT:w" s="T549">Dăre</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_251" n="HIAT:w" s="T550">vsʼo</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_254" n="HIAT:w" s="T551">amnobiʔi</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_258" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_260" n="HIAT:w" s="T552">Dĭ</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_263" n="HIAT:w" s="T553">dĭʔnə</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_266" n="HIAT:w" s="T554">kămnaj</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_270" n="HIAT:w" s="T555">dĭ</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_273" n="HIAT:w" s="T556">dĭʔnə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_276" n="HIAT:w" s="T557">kămnaj</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_280" n="HIAT:u" s="T558">
                  <nts id="Seg_281" n="HIAT:ip">(</nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ats e="T0" id="Seg_283" n="HIAT:non-pho" s="T558">NOISE</ats>
                  <nts id="Seg_284" n="HIAT:ip">)</nts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_288" n="HIAT:w" s="T0">Kabarləj</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T559" id="Seg_291" n="sc" s="T488">
               <ts e="T489" id="Seg_293" n="e" s="T488">Šide </ts>
               <ts e="T490" id="Seg_295" n="e" s="T489">kagaʔi </ts>
               <ts e="T491" id="Seg_297" n="e" s="T490">amnobiʔi. </ts>
               <ts e="T492" id="Seg_299" n="e" s="T491">(A-) </ts>
               <ts e="T493" id="Seg_301" n="e" s="T492">Aš </ts>
               <ts e="T494" id="Seg_303" n="e" s="T493">kuʔpiʔi. </ts>
               <ts e="T495" id="Seg_305" n="e" s="T494">(Tarə-) </ts>
               <ts e="T496" id="Seg_307" n="e" s="T495">Tararluʔpiʔi. </ts>
               <ts e="T497" id="Seg_309" n="e" s="T496">Dĭgəttə </ts>
               <ts e="T498" id="Seg_311" n="e" s="T497">(dĭ-) </ts>
               <ts e="T499" id="Seg_313" n="e" s="T498">onʼiʔ </ts>
               <ts e="T500" id="Seg_315" n="e" s="T499">esseŋdə </ts>
               <ts e="T501" id="Seg_317" n="e" s="T500">(iʔgö) </ts>
               <ts e="T502" id="Seg_319" n="e" s="T501">ibiʔi. </ts>
               <ts e="T503" id="Seg_321" n="e" s="T502">Onʼiʔ </ts>
               <ts e="T504" id="Seg_323" n="e" s="T503">naga </ts>
               <ts e="T505" id="Seg_325" n="e" s="T504">esseŋdə. </ts>
               <ts e="T506" id="Seg_327" n="e" s="T505">Dĭgəttə </ts>
               <ts e="T507" id="Seg_329" n="e" s="T506">dĭn </ts>
               <ts e="T508" id="Seg_331" n="e" s="T507">nebə </ts>
               <ts e="T509" id="Seg_333" n="e" s="T508">mălia: </ts>
               <ts e="T510" id="Seg_335" n="e" s="T509">"Dĭn </ts>
               <ts e="T511" id="Seg_337" n="e" s="T510">esseŋdə </ts>
               <ts e="T512" id="Seg_339" n="e" s="T511">iʔgö, </ts>
               <ts e="T513" id="Seg_341" n="e" s="T512">davaj </ts>
               <ts e="T514" id="Seg_343" n="e" s="T513">kanžəbəj." </ts>
               <ts e="T515" id="Seg_345" n="e" s="T514">Dĭgəttə </ts>
               <ts e="T516" id="Seg_347" n="e" s="T515">dĭzeŋ </ts>
               <ts e="T517" id="Seg_349" n="e" s="T516">delʼitsʼa </ts>
               <ts e="T518" id="Seg_351" n="e" s="T517">stalʼi. </ts>
               <ts e="T519" id="Seg_353" n="e" s="T518">Dĭgəttə </ts>
               <ts e="T520" id="Seg_355" n="e" s="T519">dĭ </ts>
               <ts e="T521" id="Seg_357" n="e" s="T520">(kalləj), </ts>
               <ts e="T522" id="Seg_359" n="e" s="T521">girgit </ts>
               <ts e="T523" id="Seg_361" n="e" s="T522">esseŋdə </ts>
               <ts e="T524" id="Seg_363" n="e" s="T523">iʔgö. </ts>
               <ts e="T525" id="Seg_365" n="e" s="T524">Dĭ </ts>
               <ts e="T526" id="Seg_367" n="e" s="T525">brattə </ts>
               <ts e="T527" id="Seg_369" n="e" s="T526">kămnaʔbə </ts>
               <ts e="T528" id="Seg_371" n="e" s="T527">ipek. </ts>
               <ts e="T529" id="Seg_373" n="e" s="T528">"Dĭn </ts>
               <ts e="T530" id="Seg_375" n="e" s="T529">esseŋdə </ts>
               <ts e="T531" id="Seg_377" n="e" s="T530">naga, </ts>
               <ts e="T532" id="Seg_379" n="e" s="T531">šində </ts>
               <ts e="T533" id="Seg_381" n="e" s="T532">mĭləj. </ts>
               <ts e="T534" id="Seg_383" n="e" s="T533">A </ts>
               <ts e="T535" id="Seg_385" n="e" s="T534">măna </ts>
               <ts e="T536" id="Seg_387" n="e" s="T535">esseŋbə </ts>
               <ts e="T537" id="Seg_389" n="e" s="T536">mĭləʔi." </ts>
               <ts e="T538" id="Seg_391" n="e" s="T537">Dĭgəttə </ts>
               <ts e="T539" id="Seg_393" n="e" s="T538">(dĭ=) </ts>
               <ts e="T540" id="Seg_395" n="e" s="T539">dĭ </ts>
               <ts e="T541" id="Seg_397" n="e" s="T540">dĭʔnə </ts>
               <ts e="T542" id="Seg_399" n="e" s="T541">kămlaʔbə </ts>
               <ts e="T543" id="Seg_401" n="e" s="T542">aš. </ts>
               <ts e="T544" id="Seg_403" n="e" s="T543">"Dĭn </ts>
               <ts e="T545" id="Seg_405" n="e" s="T544">esseŋdə </ts>
               <ts e="T546" id="Seg_407" n="e" s="T545">iʔgö, </ts>
               <ts e="T547" id="Seg_409" n="e" s="T546">pušaj </ts>
               <ts e="T548" id="Seg_411" n="e" s="T547">dĭʔnə </ts>
               <ts e="T549" id="Seg_413" n="e" s="T548">moləj". </ts>
               <ts e="T550" id="Seg_415" n="e" s="T549">Dăre </ts>
               <ts e="T551" id="Seg_417" n="e" s="T550">vsʼo </ts>
               <ts e="T552" id="Seg_419" n="e" s="T551">amnobiʔi. </ts>
               <ts e="T553" id="Seg_421" n="e" s="T552">Dĭ </ts>
               <ts e="T554" id="Seg_423" n="e" s="T553">dĭʔnə </ts>
               <ts e="T555" id="Seg_425" n="e" s="T554">kămnaj, </ts>
               <ts e="T556" id="Seg_427" n="e" s="T555">dĭ </ts>
               <ts e="T557" id="Seg_429" n="e" s="T556">dĭʔnə </ts>
               <ts e="T558" id="Seg_431" n="e" s="T557">kămnaj. </ts>
               <ts e="T0" id="Seg_433" n="e" s="T558">((NOISE)) </ts>
               <ts e="T559" id="Seg_435" n="e" s="T0">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T491" id="Seg_436" s="T488">PKZ_196X_ChildrenAndRye_flk.001 (001)</ta>
            <ta e="T494" id="Seg_437" s="T491">PKZ_196X_ChildrenAndRye_flk.002 (002)</ta>
            <ta e="T496" id="Seg_438" s="T494">PKZ_196X_ChildrenAndRye_flk.003 (003)</ta>
            <ta e="T502" id="Seg_439" s="T496">PKZ_196X_ChildrenAndRye_flk.004 (004)</ta>
            <ta e="T505" id="Seg_440" s="T502">PKZ_196X_ChildrenAndRye_flk.005 (005)</ta>
            <ta e="T514" id="Seg_441" s="T505">PKZ_196X_ChildrenAndRye_flk.006 (006)</ta>
            <ta e="T518" id="Seg_442" s="T514">PKZ_196X_ChildrenAndRye_flk.007 (007)</ta>
            <ta e="T524" id="Seg_443" s="T518">PKZ_196X_ChildrenAndRye_flk.008 (008)</ta>
            <ta e="T528" id="Seg_444" s="T524">PKZ_196X_ChildrenAndRye_flk.009 (009)</ta>
            <ta e="T533" id="Seg_445" s="T528">PKZ_196X_ChildrenAndRye_flk.010 (010)</ta>
            <ta e="T537" id="Seg_446" s="T533">PKZ_196X_ChildrenAndRye_flk.011 (011)</ta>
            <ta e="T543" id="Seg_447" s="T537">PKZ_196X_ChildrenAndRye_flk.012 (012)</ta>
            <ta e="T549" id="Seg_448" s="T543">PKZ_196X_ChildrenAndRye_flk.013 (013)</ta>
            <ta e="T552" id="Seg_449" s="T549">PKZ_196X_ChildrenAndRye_flk.014 (014)</ta>
            <ta e="T558" id="Seg_450" s="T552">PKZ_196X_ChildrenAndRye_flk.015 (015)</ta>
            <ta e="T559" id="Seg_451" s="T558">PKZ_196X_ChildrenAndRye_flk.016 (016)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T491" id="Seg_452" s="T488">Šide kagaʔi amnobiʔi. </ta>
            <ta e="T494" id="Seg_453" s="T491">(A-) Aš kuʔpiʔi. </ta>
            <ta e="T496" id="Seg_454" s="T494">(Tarə-) Tararluʔpiʔi. </ta>
            <ta e="T502" id="Seg_455" s="T496">Dĭgəttə (dĭ-) onʼiʔ esseŋdə (iʔgö) ibiʔi. </ta>
            <ta e="T505" id="Seg_456" s="T502">Onʼiʔ naga esseŋdə. </ta>
            <ta e="T514" id="Seg_457" s="T505">Dĭgəttə dĭn nebə mălia: "Dĭn esseŋdə iʔgö, davaj kanžəbəj". </ta>
            <ta e="T518" id="Seg_458" s="T514">Dĭgəttə dĭzeŋ delʼitsʼa stalʼi. </ta>
            <ta e="T524" id="Seg_459" s="T518">Dĭgəttə dĭ (kalləj), girgit esseŋdə iʔgö. </ta>
            <ta e="T528" id="Seg_460" s="T524">Dĭ brattə kămnaʔbə ipek. </ta>
            <ta e="T533" id="Seg_461" s="T528">"Dĭn esseŋdə naga, šində mĭləj. </ta>
            <ta e="T537" id="Seg_462" s="T533">A măna esseŋbə mĭləʔi". </ta>
            <ta e="T543" id="Seg_463" s="T537">Dĭgəttə (dĭ=) dĭ dĭʔnə kămlaʔbə aš. </ta>
            <ta e="T549" id="Seg_464" s="T543">"Dĭn esseŋdə iʔgö, pušaj dĭʔnə moləj". </ta>
            <ta e="T552" id="Seg_465" s="T549">Dăre vsʼo amnobiʔi. </ta>
            <ta e="T558" id="Seg_466" s="T552">Dĭ dĭʔnə kămnaj, dĭ dĭʔnə kămnaj. </ta>
            <ta e="T0" id="Seg_467" s="T558">((NOISE)) </ta>
            <ta e="T559" id="Seg_468" s="T0">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T489" id="Seg_469" s="T488">šide</ta>
            <ta e="T490" id="Seg_470" s="T489">kaga-ʔi</ta>
            <ta e="T491" id="Seg_471" s="T490">amno-bi-ʔi</ta>
            <ta e="T493" id="Seg_472" s="T492">aš</ta>
            <ta e="T494" id="Seg_473" s="T493">kuʔ-pi-ʔi</ta>
            <ta e="T496" id="Seg_474" s="T495">tarar-luʔ-pi-ʔi</ta>
            <ta e="T497" id="Seg_475" s="T496">dĭgəttə</ta>
            <ta e="T499" id="Seg_476" s="T498">onʼiʔ</ta>
            <ta e="T500" id="Seg_477" s="T499">es-seŋ-də</ta>
            <ta e="T501" id="Seg_478" s="T500">iʔgö</ta>
            <ta e="T502" id="Seg_479" s="T501">i-bi-ʔi</ta>
            <ta e="T503" id="Seg_480" s="T502">onʼiʔ</ta>
            <ta e="T504" id="Seg_481" s="T503">naga</ta>
            <ta e="T505" id="Seg_482" s="T504">es-seŋ-də</ta>
            <ta e="T506" id="Seg_483" s="T505">dĭgəttə</ta>
            <ta e="T507" id="Seg_484" s="T506">dĭ-n</ta>
            <ta e="T508" id="Seg_485" s="T507">ne-bə</ta>
            <ta e="T509" id="Seg_486" s="T508">mă-lia</ta>
            <ta e="T510" id="Seg_487" s="T509">dĭ-n</ta>
            <ta e="T511" id="Seg_488" s="T510">es-seŋ-də</ta>
            <ta e="T512" id="Seg_489" s="T511">iʔgö</ta>
            <ta e="T513" id="Seg_490" s="T512">davaj</ta>
            <ta e="T514" id="Seg_491" s="T513">kan-žə-bəj</ta>
            <ta e="T515" id="Seg_492" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_493" s="T515">dĭ-zeŋ</ta>
            <ta e="T519" id="Seg_494" s="T518">dĭgəttə</ta>
            <ta e="T520" id="Seg_495" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_496" s="T520">kal-lə-j</ta>
            <ta e="T522" id="Seg_497" s="T521">girgit</ta>
            <ta e="T523" id="Seg_498" s="T522">es-seŋ-də</ta>
            <ta e="T524" id="Seg_499" s="T523">iʔgö</ta>
            <ta e="T525" id="Seg_500" s="T524">dĭ</ta>
            <ta e="T526" id="Seg_501" s="T525">brat-tə</ta>
            <ta e="T527" id="Seg_502" s="T526">kăm-naʔbə</ta>
            <ta e="T528" id="Seg_503" s="T527">ipek</ta>
            <ta e="T529" id="Seg_504" s="T528">dĭ-n</ta>
            <ta e="T530" id="Seg_505" s="T529">es-seŋ-də</ta>
            <ta e="T531" id="Seg_506" s="T530">naga</ta>
            <ta e="T532" id="Seg_507" s="T531">šində</ta>
            <ta e="T533" id="Seg_508" s="T532">mĭ-lə-j</ta>
            <ta e="T534" id="Seg_509" s="T533">a</ta>
            <ta e="T535" id="Seg_510" s="T534">măna</ta>
            <ta e="T536" id="Seg_511" s="T535">es-seŋ-bə</ta>
            <ta e="T537" id="Seg_512" s="T536">mĭ-lə-ʔi</ta>
            <ta e="T538" id="Seg_513" s="T537">dĭgəttə</ta>
            <ta e="T539" id="Seg_514" s="T538">dĭ</ta>
            <ta e="T540" id="Seg_515" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_516" s="T540">dĭʔ-nə</ta>
            <ta e="T542" id="Seg_517" s="T541">kăm-laʔbə</ta>
            <ta e="T543" id="Seg_518" s="T542">aš</ta>
            <ta e="T544" id="Seg_519" s="T543">dĭ-n</ta>
            <ta e="T545" id="Seg_520" s="T544">es-seŋ-də</ta>
            <ta e="T546" id="Seg_521" s="T545">iʔgö</ta>
            <ta e="T547" id="Seg_522" s="T546">pušaj</ta>
            <ta e="T548" id="Seg_523" s="T547">dĭʔ-nə</ta>
            <ta e="T549" id="Seg_524" s="T548">mo-lə-j</ta>
            <ta e="T550" id="Seg_525" s="T549">dăre</ta>
            <ta e="T552" id="Seg_526" s="T551">amno-bi-ʔi</ta>
            <ta e="T553" id="Seg_527" s="T552">dĭ</ta>
            <ta e="T554" id="Seg_528" s="T553">dĭʔ-nə</ta>
            <ta e="T555" id="Seg_529" s="T554">kăm-na-j</ta>
            <ta e="T556" id="Seg_530" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_531" s="T556">dĭʔ-nə</ta>
            <ta e="T558" id="Seg_532" s="T557">kăm-na-j</ta>
            <ta e="T559" id="Seg_533" s="T0">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T489" id="Seg_534" s="T488">šide</ta>
            <ta e="T490" id="Seg_535" s="T489">kaga-jəʔ</ta>
            <ta e="T491" id="Seg_536" s="T490">amno-bi-jəʔ</ta>
            <ta e="T493" id="Seg_537" s="T492">aš</ta>
            <ta e="T494" id="Seg_538" s="T493">kuʔ-bi-jəʔ</ta>
            <ta e="T496" id="Seg_539" s="T495">tarar-luʔbdə-bi-jəʔ</ta>
            <ta e="T497" id="Seg_540" s="T496">dĭgəttə</ta>
            <ta e="T499" id="Seg_541" s="T498">onʼiʔ</ta>
            <ta e="T500" id="Seg_542" s="T499">ešši-zAŋ-də</ta>
            <ta e="T501" id="Seg_543" s="T500">iʔgö</ta>
            <ta e="T502" id="Seg_544" s="T501">i-bi-jəʔ</ta>
            <ta e="T503" id="Seg_545" s="T502">onʼiʔ</ta>
            <ta e="T504" id="Seg_546" s="T503">naga</ta>
            <ta e="T505" id="Seg_547" s="T504">ešši-zAŋ-də</ta>
            <ta e="T506" id="Seg_548" s="T505">dĭgəttə</ta>
            <ta e="T507" id="Seg_549" s="T506">dĭ-n</ta>
            <ta e="T508" id="Seg_550" s="T507">ne-bə</ta>
            <ta e="T509" id="Seg_551" s="T508">măn-liA</ta>
            <ta e="T510" id="Seg_552" s="T509">dĭ-n</ta>
            <ta e="T511" id="Seg_553" s="T510">ešši-zAŋ-də</ta>
            <ta e="T512" id="Seg_554" s="T511">iʔgö</ta>
            <ta e="T513" id="Seg_555" s="T512">davaj</ta>
            <ta e="T514" id="Seg_556" s="T513">kan-žə-bəj</ta>
            <ta e="T515" id="Seg_557" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_558" s="T515">dĭ-zAŋ</ta>
            <ta e="T519" id="Seg_559" s="T518">dĭgəttə</ta>
            <ta e="T520" id="Seg_560" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_561" s="T520">kan-lV-j</ta>
            <ta e="T522" id="Seg_562" s="T521">girgit</ta>
            <ta e="T523" id="Seg_563" s="T522">ešši-zAŋ-də</ta>
            <ta e="T524" id="Seg_564" s="T523">iʔgö</ta>
            <ta e="T525" id="Seg_565" s="T524">dĭ</ta>
            <ta e="T526" id="Seg_566" s="T525">brat-də</ta>
            <ta e="T527" id="Seg_567" s="T526">kămnə-laʔbə</ta>
            <ta e="T528" id="Seg_568" s="T527">ipek</ta>
            <ta e="T529" id="Seg_569" s="T528">dĭ-n</ta>
            <ta e="T530" id="Seg_570" s="T529">ešši-zAŋ-də</ta>
            <ta e="T531" id="Seg_571" s="T530">naga</ta>
            <ta e="T532" id="Seg_572" s="T531">šində</ta>
            <ta e="T533" id="Seg_573" s="T532">mĭ-lV-j</ta>
            <ta e="T534" id="Seg_574" s="T533">a</ta>
            <ta e="T535" id="Seg_575" s="T534">măna</ta>
            <ta e="T536" id="Seg_576" s="T535">ešši-zAŋ-m</ta>
            <ta e="T537" id="Seg_577" s="T536">mĭ-lV-jəʔ</ta>
            <ta e="T538" id="Seg_578" s="T537">dĭgəttə</ta>
            <ta e="T539" id="Seg_579" s="T538">dĭ</ta>
            <ta e="T540" id="Seg_580" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_581" s="T540">dĭ-Tə</ta>
            <ta e="T542" id="Seg_582" s="T541">kămnə-laʔbə</ta>
            <ta e="T543" id="Seg_583" s="T542">aš</ta>
            <ta e="T544" id="Seg_584" s="T543">dĭ-n</ta>
            <ta e="T545" id="Seg_585" s="T544">ešši-zAŋ-də</ta>
            <ta e="T546" id="Seg_586" s="T545">iʔgö</ta>
            <ta e="T547" id="Seg_587" s="T546">pušaj</ta>
            <ta e="T548" id="Seg_588" s="T547">dĭ-Tə</ta>
            <ta e="T549" id="Seg_589" s="T548">mo-lV-j</ta>
            <ta e="T550" id="Seg_590" s="T549">dărəʔ</ta>
            <ta e="T552" id="Seg_591" s="T551">amno-bi-jəʔ</ta>
            <ta e="T553" id="Seg_592" s="T552">dĭ</ta>
            <ta e="T554" id="Seg_593" s="T553">dĭ-Tə</ta>
            <ta e="T555" id="Seg_594" s="T554">kămnə-lV-j</ta>
            <ta e="T556" id="Seg_595" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_596" s="T556">dĭ-Tə</ta>
            <ta e="T558" id="Seg_597" s="T557">kămnə-lV-j</ta>
            <ta e="T559" id="Seg_598" s="T0">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T489" id="Seg_599" s="T488">two.[NOM.SG]</ta>
            <ta e="T490" id="Seg_600" s="T489">brother-PL</ta>
            <ta e="T491" id="Seg_601" s="T490">live-PST-3PL</ta>
            <ta e="T493" id="Seg_602" s="T492">rye.[NOM.SG]</ta>
            <ta e="T494" id="Seg_603" s="T493">sow-PST-3PL</ta>
            <ta e="T496" id="Seg_604" s="T495">get.tired-MOM-PST-3PL</ta>
            <ta e="T497" id="Seg_605" s="T496">then</ta>
            <ta e="T499" id="Seg_606" s="T498">one.[NOM.SG]</ta>
            <ta e="T500" id="Seg_607" s="T499">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_608" s="T500">many</ta>
            <ta e="T502" id="Seg_609" s="T501">be-PST-3PL</ta>
            <ta e="T503" id="Seg_610" s="T502">one.[NOM.SG]</ta>
            <ta e="T504" id="Seg_611" s="T503">NEG.EX</ta>
            <ta e="T505" id="Seg_612" s="T504">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T506" id="Seg_613" s="T505">then</ta>
            <ta e="T507" id="Seg_614" s="T506">this-GEN</ta>
            <ta e="T508" id="Seg_615" s="T507">woman-ACC.3SG</ta>
            <ta e="T509" id="Seg_616" s="T508">say-PRS.[3SG]</ta>
            <ta e="T510" id="Seg_617" s="T509">this-GEN</ta>
            <ta e="T511" id="Seg_618" s="T510">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T512" id="Seg_619" s="T511">many</ta>
            <ta e="T513" id="Seg_620" s="T512">HORT</ta>
            <ta e="T514" id="Seg_621" s="T513">go-OPT.DU/PL-1DU</ta>
            <ta e="T515" id="Seg_622" s="T514">then</ta>
            <ta e="T516" id="Seg_623" s="T515">this-PL</ta>
            <ta e="T519" id="Seg_624" s="T518">then</ta>
            <ta e="T520" id="Seg_625" s="T519">this.[NOM.SG]</ta>
            <ta e="T521" id="Seg_626" s="T520">go-FUT-3SG</ta>
            <ta e="T522" id="Seg_627" s="T521">what.kind</ta>
            <ta e="T523" id="Seg_628" s="T522">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T524" id="Seg_629" s="T523">many</ta>
            <ta e="T525" id="Seg_630" s="T524">this.[NOM.SG]</ta>
            <ta e="T526" id="Seg_631" s="T525">brother-NOM/GEN/ACC.3SG</ta>
            <ta e="T527" id="Seg_632" s="T526">pour-DUR.[3SG]</ta>
            <ta e="T528" id="Seg_633" s="T527">bread.[NOM.SG]</ta>
            <ta e="T529" id="Seg_634" s="T528">this-GEN</ta>
            <ta e="T530" id="Seg_635" s="T529">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T531" id="Seg_636" s="T530">NEG.EX</ta>
            <ta e="T532" id="Seg_637" s="T531">who.[NOM.SG]</ta>
            <ta e="T533" id="Seg_638" s="T532">give-FUT-3SG</ta>
            <ta e="T534" id="Seg_639" s="T533">and</ta>
            <ta e="T535" id="Seg_640" s="T534">I.LAT</ta>
            <ta e="T536" id="Seg_641" s="T535">child-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T537" id="Seg_642" s="T536">give-FUT-3PL</ta>
            <ta e="T538" id="Seg_643" s="T537">then</ta>
            <ta e="T539" id="Seg_644" s="T538">this.[NOM.SG]</ta>
            <ta e="T540" id="Seg_645" s="T539">this.[NOM.SG]</ta>
            <ta e="T541" id="Seg_646" s="T540">this-LAT</ta>
            <ta e="T542" id="Seg_647" s="T541">pour-DUR.[3SG]</ta>
            <ta e="T543" id="Seg_648" s="T542">rye.[NOM.SG]</ta>
            <ta e="T544" id="Seg_649" s="T543">this-GEN</ta>
            <ta e="T545" id="Seg_650" s="T544">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T546" id="Seg_651" s="T545">many</ta>
            <ta e="T547" id="Seg_652" s="T546">JUSS</ta>
            <ta e="T548" id="Seg_653" s="T547">this-LAT</ta>
            <ta e="T549" id="Seg_654" s="T548">become-FUT-3SG</ta>
            <ta e="T550" id="Seg_655" s="T549">so</ta>
            <ta e="T552" id="Seg_656" s="T551">live-PST-3PL</ta>
            <ta e="T553" id="Seg_657" s="T552">this.[NOM.SG]</ta>
            <ta e="T554" id="Seg_658" s="T553">this-LAT</ta>
            <ta e="T555" id="Seg_659" s="T554">pour-FUT-3SG</ta>
            <ta e="T556" id="Seg_660" s="T555">this.[NOM.SG]</ta>
            <ta e="T557" id="Seg_661" s="T556">this-LAT</ta>
            <ta e="T558" id="Seg_662" s="T557">pour-FUT-3SG</ta>
            <ta e="T559" id="Seg_663" s="T0">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T489" id="Seg_664" s="T488">два.[NOM.SG]</ta>
            <ta e="T490" id="Seg_665" s="T489">брат-PL</ta>
            <ta e="T491" id="Seg_666" s="T490">жить-PST-3PL</ta>
            <ta e="T493" id="Seg_667" s="T492">рожь.[NOM.SG]</ta>
            <ta e="T494" id="Seg_668" s="T493">сеять-PST-3PL</ta>
            <ta e="T496" id="Seg_669" s="T495">устать-MOM-PST-3PL</ta>
            <ta e="T497" id="Seg_670" s="T496">тогда</ta>
            <ta e="T499" id="Seg_671" s="T498">один.[NOM.SG]</ta>
            <ta e="T500" id="Seg_672" s="T499">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T501" id="Seg_673" s="T500">много</ta>
            <ta e="T502" id="Seg_674" s="T501">быть-PST-3PL</ta>
            <ta e="T503" id="Seg_675" s="T502">один.[NOM.SG]</ta>
            <ta e="T504" id="Seg_676" s="T503">NEG.EX</ta>
            <ta e="T505" id="Seg_677" s="T504">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T506" id="Seg_678" s="T505">тогда</ta>
            <ta e="T507" id="Seg_679" s="T506">этот-GEN</ta>
            <ta e="T508" id="Seg_680" s="T507">женщина-ACC.3SG</ta>
            <ta e="T509" id="Seg_681" s="T508">сказать-PRS.[3SG]</ta>
            <ta e="T510" id="Seg_682" s="T509">этот-GEN</ta>
            <ta e="T511" id="Seg_683" s="T510">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T512" id="Seg_684" s="T511">много</ta>
            <ta e="T513" id="Seg_685" s="T512">HORT</ta>
            <ta e="T514" id="Seg_686" s="T513">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T515" id="Seg_687" s="T514">тогда</ta>
            <ta e="T516" id="Seg_688" s="T515">этот-PL</ta>
            <ta e="T519" id="Seg_689" s="T518">тогда</ta>
            <ta e="T520" id="Seg_690" s="T519">этот.[NOM.SG]</ta>
            <ta e="T521" id="Seg_691" s="T520">пойти-FUT-3SG</ta>
            <ta e="T522" id="Seg_692" s="T521">какой</ta>
            <ta e="T523" id="Seg_693" s="T522">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T524" id="Seg_694" s="T523">много</ta>
            <ta e="T525" id="Seg_695" s="T524">этот.[NOM.SG]</ta>
            <ta e="T526" id="Seg_696" s="T525">брат-NOM/GEN/ACC.3SG</ta>
            <ta e="T527" id="Seg_697" s="T526">лить-DUR.[3SG]</ta>
            <ta e="T528" id="Seg_698" s="T527">хлеб.[NOM.SG]</ta>
            <ta e="T529" id="Seg_699" s="T528">этот-GEN</ta>
            <ta e="T530" id="Seg_700" s="T529">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T531" id="Seg_701" s="T530">NEG.EX</ta>
            <ta e="T532" id="Seg_702" s="T531">кто.[NOM.SG]</ta>
            <ta e="T533" id="Seg_703" s="T532">дать-FUT-3SG</ta>
            <ta e="T534" id="Seg_704" s="T533">а</ta>
            <ta e="T535" id="Seg_705" s="T534">я.LAT</ta>
            <ta e="T536" id="Seg_706" s="T535">ребенок-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T537" id="Seg_707" s="T536">дать-FUT-3PL</ta>
            <ta e="T538" id="Seg_708" s="T537">тогда</ta>
            <ta e="T539" id="Seg_709" s="T538">этот.[NOM.SG]</ta>
            <ta e="T540" id="Seg_710" s="T539">этот.[NOM.SG]</ta>
            <ta e="T541" id="Seg_711" s="T540">этот-LAT</ta>
            <ta e="T542" id="Seg_712" s="T541">лить-DUR.[3SG]</ta>
            <ta e="T543" id="Seg_713" s="T542">рожь.[NOM.SG]</ta>
            <ta e="T544" id="Seg_714" s="T543">этот-GEN</ta>
            <ta e="T545" id="Seg_715" s="T544">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T546" id="Seg_716" s="T545">много</ta>
            <ta e="T547" id="Seg_717" s="T546">JUSS</ta>
            <ta e="T548" id="Seg_718" s="T547">этот-LAT</ta>
            <ta e="T549" id="Seg_719" s="T548">стать-FUT-3SG</ta>
            <ta e="T550" id="Seg_720" s="T549">так</ta>
            <ta e="T552" id="Seg_721" s="T551">жить-PST-3PL</ta>
            <ta e="T553" id="Seg_722" s="T552">этот.[NOM.SG]</ta>
            <ta e="T554" id="Seg_723" s="T553">этот-LAT</ta>
            <ta e="T555" id="Seg_724" s="T554">лить-FUT-3SG</ta>
            <ta e="T556" id="Seg_725" s="T555">этот.[NOM.SG]</ta>
            <ta e="T557" id="Seg_726" s="T556">этот-LAT</ta>
            <ta e="T558" id="Seg_727" s="T557">лить-FUT-3SG</ta>
            <ta e="T559" id="Seg_728" s="T0">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T489" id="Seg_729" s="T488">num-n:case</ta>
            <ta e="T490" id="Seg_730" s="T489">n-n:num</ta>
            <ta e="T491" id="Seg_731" s="T490">v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_732" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_733" s="T493">v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_734" s="T495">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_735" s="T496">adv</ta>
            <ta e="T499" id="Seg_736" s="T498">num-n:case</ta>
            <ta e="T500" id="Seg_737" s="T499">n-n:num-n:case.poss</ta>
            <ta e="T501" id="Seg_738" s="T500">quant</ta>
            <ta e="T502" id="Seg_739" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_740" s="T502">num-n:case</ta>
            <ta e="T504" id="Seg_741" s="T503">v</ta>
            <ta e="T505" id="Seg_742" s="T504">n-n:num-n:case.poss</ta>
            <ta e="T506" id="Seg_743" s="T505">adv</ta>
            <ta e="T507" id="Seg_744" s="T506">dempro-n:case</ta>
            <ta e="T508" id="Seg_745" s="T507">n-n:case.poss</ta>
            <ta e="T509" id="Seg_746" s="T508">v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_747" s="T509">dempro-n:case</ta>
            <ta e="T511" id="Seg_748" s="T510">n-n:num-n:case.poss</ta>
            <ta e="T512" id="Seg_749" s="T511">quant</ta>
            <ta e="T513" id="Seg_750" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_751" s="T513">v-v:mood-v:pn</ta>
            <ta e="T515" id="Seg_752" s="T514">adv</ta>
            <ta e="T516" id="Seg_753" s="T515">dempro-n:num</ta>
            <ta e="T519" id="Seg_754" s="T518">adv</ta>
            <ta e="T520" id="Seg_755" s="T519">dempro-n:case</ta>
            <ta e="T521" id="Seg_756" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_757" s="T521">que</ta>
            <ta e="T523" id="Seg_758" s="T522">n-n:num-n:case.poss</ta>
            <ta e="T524" id="Seg_759" s="T523">quant</ta>
            <ta e="T525" id="Seg_760" s="T524">dempro-n:case</ta>
            <ta e="T526" id="Seg_761" s="T525">n-n:case.poss</ta>
            <ta e="T527" id="Seg_762" s="T526">v-v&gt;v-v:pn</ta>
            <ta e="T528" id="Seg_763" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_764" s="T528">dempro-n:case</ta>
            <ta e="T530" id="Seg_765" s="T529">n-n:num-n:case.poss</ta>
            <ta e="T531" id="Seg_766" s="T530">v</ta>
            <ta e="T532" id="Seg_767" s="T531">que</ta>
            <ta e="T533" id="Seg_768" s="T532">v-v:tense-v:pn</ta>
            <ta e="T534" id="Seg_769" s="T533">conj</ta>
            <ta e="T535" id="Seg_770" s="T534">pers</ta>
            <ta e="T536" id="Seg_771" s="T535">n-n:num-n:case.poss</ta>
            <ta e="T537" id="Seg_772" s="T536">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_773" s="T537">adv</ta>
            <ta e="T539" id="Seg_774" s="T538">dempro-n:case</ta>
            <ta e="T540" id="Seg_775" s="T539">dempro-n:case</ta>
            <ta e="T541" id="Seg_776" s="T540">dempro-n:case</ta>
            <ta e="T542" id="Seg_777" s="T541">v-v&gt;v-v:pn</ta>
            <ta e="T543" id="Seg_778" s="T542">n-n:case</ta>
            <ta e="T544" id="Seg_779" s="T543">dempro-n:case</ta>
            <ta e="T545" id="Seg_780" s="T544">n-n:num-n:case.poss</ta>
            <ta e="T546" id="Seg_781" s="T545">quant</ta>
            <ta e="T547" id="Seg_782" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_783" s="T547">dempro-n:case</ta>
            <ta e="T549" id="Seg_784" s="T548">v-v:tense-v:pn</ta>
            <ta e="T550" id="Seg_785" s="T549">ptcl</ta>
            <ta e="T552" id="Seg_786" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_787" s="T552">dempro-n:case</ta>
            <ta e="T554" id="Seg_788" s="T553">dempro-n:case</ta>
            <ta e="T555" id="Seg_789" s="T554">v-v:tense-v:pn</ta>
            <ta e="T556" id="Seg_790" s="T555">dempro-n:case</ta>
            <ta e="T557" id="Seg_791" s="T556">dempro-n:case</ta>
            <ta e="T558" id="Seg_792" s="T557">v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_793" s="T0">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T489" id="Seg_794" s="T488">num</ta>
            <ta e="T490" id="Seg_795" s="T489">n</ta>
            <ta e="T491" id="Seg_796" s="T490">v</ta>
            <ta e="T493" id="Seg_797" s="T492">n</ta>
            <ta e="T494" id="Seg_798" s="T493">v</ta>
            <ta e="T496" id="Seg_799" s="T495">v</ta>
            <ta e="T497" id="Seg_800" s="T496">adv</ta>
            <ta e="T499" id="Seg_801" s="T498">num</ta>
            <ta e="T500" id="Seg_802" s="T499">n</ta>
            <ta e="T501" id="Seg_803" s="T500">quant</ta>
            <ta e="T502" id="Seg_804" s="T501">v</ta>
            <ta e="T503" id="Seg_805" s="T502">num</ta>
            <ta e="T504" id="Seg_806" s="T503">v</ta>
            <ta e="T505" id="Seg_807" s="T504">n</ta>
            <ta e="T506" id="Seg_808" s="T505">adv</ta>
            <ta e="T507" id="Seg_809" s="T506">dempro</ta>
            <ta e="T508" id="Seg_810" s="T507">n</ta>
            <ta e="T509" id="Seg_811" s="T508">v</ta>
            <ta e="T510" id="Seg_812" s="T509">dempro</ta>
            <ta e="T511" id="Seg_813" s="T510">n</ta>
            <ta e="T512" id="Seg_814" s="T511">quant</ta>
            <ta e="T513" id="Seg_815" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_816" s="T513">v</ta>
            <ta e="T515" id="Seg_817" s="T514">adv</ta>
            <ta e="T516" id="Seg_818" s="T515">dempro</ta>
            <ta e="T519" id="Seg_819" s="T518">adv</ta>
            <ta e="T520" id="Seg_820" s="T519">dempro</ta>
            <ta e="T521" id="Seg_821" s="T520">v</ta>
            <ta e="T522" id="Seg_822" s="T521">que</ta>
            <ta e="T523" id="Seg_823" s="T522">n</ta>
            <ta e="T524" id="Seg_824" s="T523">quant</ta>
            <ta e="T525" id="Seg_825" s="T524">dempro</ta>
            <ta e="T526" id="Seg_826" s="T525">n</ta>
            <ta e="T527" id="Seg_827" s="T526">v</ta>
            <ta e="T528" id="Seg_828" s="T527">n</ta>
            <ta e="T529" id="Seg_829" s="T528">dempro</ta>
            <ta e="T530" id="Seg_830" s="T529">n</ta>
            <ta e="T531" id="Seg_831" s="T530">v</ta>
            <ta e="T532" id="Seg_832" s="T531">que</ta>
            <ta e="T533" id="Seg_833" s="T532">v</ta>
            <ta e="T534" id="Seg_834" s="T533">conj</ta>
            <ta e="T535" id="Seg_835" s="T534">pers</ta>
            <ta e="T536" id="Seg_836" s="T535">n</ta>
            <ta e="T537" id="Seg_837" s="T536">v</ta>
            <ta e="T538" id="Seg_838" s="T537">adv</ta>
            <ta e="T539" id="Seg_839" s="T538">dempro</ta>
            <ta e="T540" id="Seg_840" s="T539">dempro</ta>
            <ta e="T541" id="Seg_841" s="T540">dempro</ta>
            <ta e="T542" id="Seg_842" s="T541">v</ta>
            <ta e="T543" id="Seg_843" s="T542">n</ta>
            <ta e="T544" id="Seg_844" s="T543">dempro</ta>
            <ta e="T545" id="Seg_845" s="T544">n</ta>
            <ta e="T546" id="Seg_846" s="T545">quant</ta>
            <ta e="T547" id="Seg_847" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_848" s="T547">dempro</ta>
            <ta e="T549" id="Seg_849" s="T548">v</ta>
            <ta e="T550" id="Seg_850" s="T549">ptcl</ta>
            <ta e="T552" id="Seg_851" s="T551">v</ta>
            <ta e="T553" id="Seg_852" s="T552">dempro</ta>
            <ta e="T554" id="Seg_853" s="T553">dempro</ta>
            <ta e="T555" id="Seg_854" s="T554">v</ta>
            <ta e="T556" id="Seg_855" s="T555">dempro</ta>
            <ta e="T557" id="Seg_856" s="T556">dempro</ta>
            <ta e="T558" id="Seg_857" s="T557">v</ta>
            <ta e="T559" id="Seg_858" s="T0">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T490" id="Seg_859" s="T489">np.h:E</ta>
            <ta e="T493" id="Seg_860" s="T492">np:Th</ta>
            <ta e="T494" id="Seg_861" s="T493">0.3.h:A</ta>
            <ta e="T496" id="Seg_862" s="T495">0.3.h:E</ta>
            <ta e="T497" id="Seg_863" s="T496">adv:Time</ta>
            <ta e="T499" id="Seg_864" s="T498">np.h:Poss</ta>
            <ta e="T500" id="Seg_865" s="T499">np.h:Th</ta>
            <ta e="T503" id="Seg_866" s="T502">np.h:Poss</ta>
            <ta e="T505" id="Seg_867" s="T504">np.h:Th</ta>
            <ta e="T506" id="Seg_868" s="T505">adv:Time</ta>
            <ta e="T507" id="Seg_869" s="T506">pro.h:Poss</ta>
            <ta e="T508" id="Seg_870" s="T507">np.h:R</ta>
            <ta e="T509" id="Seg_871" s="T508">0.3.h:A</ta>
            <ta e="T510" id="Seg_872" s="T509">pro.h:Poss</ta>
            <ta e="T511" id="Seg_873" s="T510">np.h:Th</ta>
            <ta e="T514" id="Seg_874" s="T513">0.1.h:A</ta>
            <ta e="T515" id="Seg_875" s="T514">adv:Time</ta>
            <ta e="T516" id="Seg_876" s="T515">pro.h:A</ta>
            <ta e="T519" id="Seg_877" s="T518">adv:Time</ta>
            <ta e="T520" id="Seg_878" s="T519">pro.h:A</ta>
            <ta e="T523" id="Seg_879" s="T522">np.h:Th 0.3.h:Poss</ta>
            <ta e="T525" id="Seg_880" s="T524">pro.h:A</ta>
            <ta e="T526" id="Seg_881" s="T525">np.h:B</ta>
            <ta e="T528" id="Seg_882" s="T527">np:Th</ta>
            <ta e="T529" id="Seg_883" s="T528">pro.h:Poss</ta>
            <ta e="T530" id="Seg_884" s="T529">np.h:Th</ta>
            <ta e="T532" id="Seg_885" s="T531">pro.h:A</ta>
            <ta e="T535" id="Seg_886" s="T534">pro.h:R</ta>
            <ta e="T536" id="Seg_887" s="T535">np.h:A</ta>
            <ta e="T538" id="Seg_888" s="T537">adv:Time</ta>
            <ta e="T540" id="Seg_889" s="T539">pro.h:A</ta>
            <ta e="T541" id="Seg_890" s="T540">pro.h:B</ta>
            <ta e="T543" id="Seg_891" s="T542">np:Th</ta>
            <ta e="T544" id="Seg_892" s="T543">pro.h:Poss</ta>
            <ta e="T545" id="Seg_893" s="T544">np.h:Th</ta>
            <ta e="T548" id="Seg_894" s="T547">pro.h:B</ta>
            <ta e="T549" id="Seg_895" s="T548">0.3:A</ta>
            <ta e="T552" id="Seg_896" s="T551">0.3.h:E</ta>
            <ta e="T553" id="Seg_897" s="T552">pro.h:A</ta>
            <ta e="T554" id="Seg_898" s="T553">pro.h:B</ta>
            <ta e="T556" id="Seg_899" s="T555">pro.h:A</ta>
            <ta e="T557" id="Seg_900" s="T556">pro.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T490" id="Seg_901" s="T489">np.h:S</ta>
            <ta e="T491" id="Seg_902" s="T490">v:pred</ta>
            <ta e="T493" id="Seg_903" s="T492">np:O</ta>
            <ta e="T494" id="Seg_904" s="T493">v:pred 0.3.h:S</ta>
            <ta e="T496" id="Seg_905" s="T495">v:pred 0.3.h:S</ta>
            <ta e="T500" id="Seg_906" s="T499">np.h:S</ta>
            <ta e="T501" id="Seg_907" s="T500">adj:pred</ta>
            <ta e="T502" id="Seg_908" s="T501">cop</ta>
            <ta e="T504" id="Seg_909" s="T503">v:pred</ta>
            <ta e="T505" id="Seg_910" s="T504">np.h:S</ta>
            <ta e="T509" id="Seg_911" s="T508">v:pred 0.3.h:S</ta>
            <ta e="T511" id="Seg_912" s="T510">np.h:S</ta>
            <ta e="T512" id="Seg_913" s="T511">adj:pred</ta>
            <ta e="T514" id="Seg_914" s="T513">v:pred 0.1.h:S</ta>
            <ta e="T516" id="Seg_915" s="T515">pro.h:S</ta>
            <ta e="T520" id="Seg_916" s="T519">pro.h:S</ta>
            <ta e="T521" id="Seg_917" s="T520">v:pred</ta>
            <ta e="T522" id="Seg_918" s="T521">s:rel</ta>
            <ta e="T523" id="Seg_919" s="T522">np.h:S</ta>
            <ta e="T524" id="Seg_920" s="T523">adj:pred</ta>
            <ta e="T525" id="Seg_921" s="T524">pro.h:S</ta>
            <ta e="T527" id="Seg_922" s="T526">v:pred</ta>
            <ta e="T528" id="Seg_923" s="T527">np:O</ta>
            <ta e="T530" id="Seg_924" s="T529">np.h:S</ta>
            <ta e="T531" id="Seg_925" s="T530">v:pred</ta>
            <ta e="T532" id="Seg_926" s="T531">pro.h:S</ta>
            <ta e="T533" id="Seg_927" s="T532">v:pred</ta>
            <ta e="T536" id="Seg_928" s="T535">np.h:S</ta>
            <ta e="T537" id="Seg_929" s="T536">v:pred</ta>
            <ta e="T540" id="Seg_930" s="T539">pro.h:S</ta>
            <ta e="T542" id="Seg_931" s="T541">v:pred</ta>
            <ta e="T543" id="Seg_932" s="T542">np:O</ta>
            <ta e="T545" id="Seg_933" s="T544">np.h:S</ta>
            <ta e="T546" id="Seg_934" s="T545">adj:pred</ta>
            <ta e="T548" id="Seg_935" s="T547">n:pred</ta>
            <ta e="T549" id="Seg_936" s="T548">cop 0.3:S</ta>
            <ta e="T552" id="Seg_937" s="T551">v:pred 0.3.h:S</ta>
            <ta e="T553" id="Seg_938" s="T552">pro.h:S</ta>
            <ta e="T555" id="Seg_939" s="T554">v:pred</ta>
            <ta e="T556" id="Seg_940" s="T555">pro.h:S</ta>
            <ta e="T558" id="Seg_941" s="T557">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T493" id="Seg_942" s="T492">TURK:cult</ta>
            <ta e="T513" id="Seg_943" s="T512">RUS:gram</ta>
            <ta e="T526" id="Seg_944" s="T525">RUS:core</ta>
            <ta e="T534" id="Seg_945" s="T533">RUS:gram</ta>
            <ta e="T543" id="Seg_946" s="T542">TURK:cult</ta>
            <ta e="T547" id="Seg_947" s="T546">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T547" id="Seg_948" s="T546">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T551" id="Seg_949" s="T550">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T491" id="Seg_950" s="T488">Жили два брата.</ta>
            <ta e="T494" id="Seg_951" s="T491">Сеяли рожь.</ta>
            <ta e="T496" id="Seg_952" s="T494">Устали они.</ta>
            <ta e="T502" id="Seg_953" s="T496">[Потом] у одного было много детей.</ta>
            <ta e="T505" id="Seg_954" s="T502">У [другого] детей не было.</ta>
            <ta e="T514" id="Seg_955" s="T505">Тогда он говорит жене: «У него много детей, давай пойдём [с ним поделимся зерном]».</ta>
            <ta e="T518" id="Seg_956" s="T514">И они стали делиться.</ta>
            <ta e="T524" id="Seg_957" s="T518">Потом пошёл тот, у которого детей много.</ta>
            <ta e="T528" id="Seg_958" s="T524">Он своему брату зерна насыпает.</ta>
            <ta e="T533" id="Seg_959" s="T528">«У него детей нет, кто ему даст [зерна]?</ta>
            <ta e="T537" id="Seg_960" s="T533">А мне мои дети дадут».</ta>
            <ta e="T543" id="Seg_961" s="T537">И насыпает ему ржи.</ta>
            <ta e="T549" id="Seg_962" s="T543">«У него детей много, пусть это для него будет».</ta>
            <ta e="T552" id="Seg_963" s="T549">Так и жили.</ta>
            <ta e="T558" id="Seg_964" s="T552">Этот тому отсыплет, тот этому отсыплет.</ta>
            <ta e="T559" id="Seg_965" s="T558">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T491" id="Seg_966" s="T488">Two brothers lived.</ta>
            <ta e="T494" id="Seg_967" s="T491">They sowed rye.</ta>
            <ta e="T496" id="Seg_968" s="T494">They got tired.</ta>
            <ta e="T502" id="Seg_969" s="T496">Then one had (many?) children.</ta>
            <ta e="T505" id="Seg_970" s="T502">One did not have children.</ta>
            <ta e="T514" id="Seg_971" s="T505">Then he tellls his wife: "He has many children, let's go [and share grain with him]."</ta>
            <ta e="T518" id="Seg_972" s="T514">Then they started to share.</ta>
            <ta e="T524" id="Seg_973" s="T518">Then he will go, the one who has many children.</ta>
            <ta e="T528" id="Seg_974" s="T524">He pours grain to his brother.</ta>
            <ta e="T533" id="Seg_975" s="T528">"He has no children, who will give [him grain]?</ta>
            <ta e="T537" id="Seg_976" s="T533">And to me, my children will give [me grain]."</ta>
            <ta e="T543" id="Seg_977" s="T537">Then he pours him rye.</ta>
            <ta e="T549" id="Seg_978" s="T543">He has many children, let it be for him.</ta>
            <ta e="T552" id="Seg_979" s="T549">So they lived on.</ta>
            <ta e="T558" id="Seg_980" s="T552">He will pour to him, he will pour to him.</ta>
            <ta e="T559" id="Seg_981" s="T558">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T491" id="Seg_982" s="T488">Es lebten zwei Brüder.</ta>
            <ta e="T494" id="Seg_983" s="T491">Sie säten Roggen.</ta>
            <ta e="T496" id="Seg_984" s="T494">Sie wurden müde.</ta>
            <ta e="T502" id="Seg_985" s="T496">Dann hatte einer (viele?) Kinder.</ta>
            <ta e="T505" id="Seg_986" s="T502">Einer hatte keine Kinder.</ta>
            <ta e="T514" id="Seg_987" s="T505">Dann sagt er seiner Frau: „Er hat viele Kinder, gehen wir [zu ihm, um ihm Korn zu teilen].“</ta>
            <ta e="T518" id="Seg_988" s="T514">Dann fingen sie an zu teilen.</ta>
            <ta e="T524" id="Seg_989" s="T518">Dann will er gehen, der, der viele Kinder hat.</ta>
            <ta e="T528" id="Seg_990" s="T524">Er schenkt seinem Bruder Korn ein.</ta>
            <ta e="T533" id="Seg_991" s="T528">„Er hat keine Kinder, wer wird [ihm Korn] geben?</ta>
            <ta e="T537" id="Seg_992" s="T533">Und mir, meine Kinder werden [mir] Korn [geben].</ta>
            <ta e="T543" id="Seg_993" s="T537">Dann schenkt er ihm Roggen ein.</ta>
            <ta e="T549" id="Seg_994" s="T543">Er hat viele Kinder, es soll für ihn sein.</ta>
            <ta e="T552" id="Seg_995" s="T549">So lebten sie immer.</ta>
            <ta e="T558" id="Seg_996" s="T552">Er wird auf ihn einschenken, er schenkt auf ihn ein.</ta>
            <ta e="T559" id="Seg_997" s="T558">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T502" id="Seg_998" s="T496">[KlT:] Says ’iʔbo’ instead of ’iʔgö’ (iʔgo)?</ta>
            <ta e="T518" id="Seg_999" s="T514">[AAV:] They put some grain in the other brother's place while he was asleep.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T0" />
            <conversion-tli id="T559" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
