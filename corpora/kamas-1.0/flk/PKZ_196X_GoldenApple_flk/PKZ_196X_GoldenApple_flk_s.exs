<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC913784C-65E0-7325-4C80-5360F3D6605F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GoldenApple_flk.wav" />
         <referenced-file url="PKZ_196X_GoldenApple_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GoldenApple_flk\PKZ_196X_GoldenApple_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">101</ud-information>
            <ud-information attribute-name="# HIAT:w">68</ud-information>
            <ud-information attribute-name="# e">68</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.028" type="appl" />
         <tli id="T1" time="0.774" type="appl" />
         <tli id="T2" time="1.52" type="appl" />
         <tli id="T3" time="2.265" type="appl" />
         <tli id="T4" time="3.011" type="appl" />
         <tli id="T5" time="3.952" type="appl" />
         <tli id="T6" time="4.894" type="appl" />
         <tli id="T7" time="5.745" type="appl" />
         <tli id="T8" time="6.49" type="appl" />
         <tli id="T9" time="7.234" type="appl" />
         <tli id="T10" time="7.979" type="appl" />
         <tli id="T11" time="8.773" type="appl" />
         <tli id="T12" time="9.567" type="appl" />
         <tli id="T13" time="10.361" type="appl" />
         <tli id="T14" time="11.155" type="appl" />
         <tli id="T15" time="11.848" type="appl" />
         <tli id="T16" time="12.506" type="appl" />
         <tli id="T17" time="13.164" type="appl" />
         <tli id="T18" time="13.823" type="appl" />
         <tli id="T19" time="14.482" type="appl" />
         <tli id="T20" time="15.14" type="appl" />
         <tli id="T21" time="15.946" type="appl" />
         <tli id="T22" time="16.751" type="appl" />
         <tli id="T23" time="17.557" type="appl" />
         <tli id="T24" time="18.135" type="appl" />
         <tli id="T25" time="18.713" type="appl" />
         <tli id="T26" time="19.739158348651962" />
         <tli id="T27" time="20.724" type="appl" />
         <tli id="T28" time="21.667" type="appl" />
         <tli id="T29" time="22.845692556849126" />
         <tli id="T30" time="23.624" type="appl" />
         <tli id="T31" time="24.23" type="appl" />
         <tli id="T32" time="24.835" type="appl" />
         <tli id="T33" time="25.528" type="appl" />
         <tli id="T34" time="26.22" type="appl" />
         <tli id="T35" time="26.913" type="appl" />
         <tli id="T36" time="27.605" type="appl" />
         <tli id="T37" time="28.298" type="appl" />
         <tli id="T38" time="28.99" type="appl" />
         <tli id="T39" time="29.683" type="appl" />
         <tli id="T40" time="30.375" type="appl" />
         <tli id="T41" time="31.068" type="appl" />
         <tli id="T42" time="31.662" type="appl" />
         <tli id="T43" time="32.256" type="appl" />
         <tli id="T44" time="32.85" type="appl" />
         <tli id="T45" time="33.619" type="appl" />
         <tli id="T46" time="34.177" type="appl" />
         <tli id="T47" time="34.735" type="appl" />
         <tli id="T48" time="35.293" type="appl" />
         <tli id="T49" time="35.851" type="appl" />
         <tli id="T50" time="36.409" type="appl" />
         <tli id="T51" time="36.967" type="appl" />
         <tli id="T52" time="37.525" type="appl" />
         <tli id="T53" time="37.992" type="appl" />
         <tli id="T54" time="38.459" type="appl" />
         <tli id="T55" time="38.926" type="appl" />
         <tli id="T56" time="39.393" type="appl" />
         <tli id="T57" time="39.86" type="appl" />
         <tli id="T58" time="40.327" type="appl" />
         <tli id="T59" time="40.794" type="appl" />
         <tli id="T60" time="41.447" type="appl" />
         <tli id="T61" time="42.05" type="appl" />
         <tli id="T62" time="42.654" type="appl" />
         <tli id="T63" time="43.257" type="appl" />
         <tli id="T64" time="43.861" type="appl" />
         <tli id="T65" time="44.464" type="appl" />
         <tli id="T68" time="44.722857142857144" type="intp" />
         <tli id="T66" time="45.068" type="appl" />
         <tli id="T67" time="45.898" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T67" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kaməndə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">urgo</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">koŋ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭgəttə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">măndə:</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Nada</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">mĭzittə</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">zălatoj</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">jablokă</ts>
                  <nts id="Seg_36" n="HIAT:ip">"</nts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_40" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">Bar</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">iləm</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_47" n="HIAT:ip">(</nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">sə-</ts>
                  <nts id="Seg_50" n="HIAT:ip">)</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">oʔbdəbi</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_57" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">Šindidə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">dĭʔnə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">jakšə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">ej</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">mobi</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">azittə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Šobi</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">onʼiʔ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">kuza</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_90" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">Dĭn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">ĭmbidə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">naga</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_102" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">Deʔpi</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">kuvšin</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">dĭʔnə</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_114" n="HIAT:u" s="T29">
                  <nts id="Seg_115" n="HIAT:ip">"</nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">Ĭmbileʔ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">tăn</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">šobial</ts>
                  <nts id="Seg_124" n="HIAT:ip">?</nts>
                  <nts id="Seg_125" n="HIAT:ip">"</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_128" n="HIAT:u" s="T32">
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">Măn</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">šobial</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">tăn</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">măna</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">aktʼa</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_147" n="HIAT:w" s="T37">iʔgö</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">dolžən</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">mĭʔ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">măna</ts>
                  <nts id="Seg_158" n="HIAT:ip">"</nts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_162" n="HIAT:u" s="T41">
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_165" n="HIAT:w" s="T41">Da</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">tăn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">šʼaːmnial</ts>
                  <nts id="Seg_172" n="HIAT:ip">!</nts>
                  <nts id="Seg_173" n="HIAT:ip">"</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_176" n="HIAT:u" s="T44">
                  <nts id="Seg_177" n="HIAT:ip">"</nts>
                  <nts id="Seg_178" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_180" n="HIAT:w" s="T44">Ak-</ts>
                  <nts id="Seg_181" n="HIAT:ip">)</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_184" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_187" n="HIAT:w" s="T46">šʼaːmniam</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_190" n="HIAT:w" s="T47">dak</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_194" n="HIAT:w" s="T48">dettə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">măna</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_200" n="HIAT:w" s="T50">zălatoj</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">jablokă</ts>
                  <nts id="Seg_204" n="HIAT:ip">"</nts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_208" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_210" n="HIAT:w" s="T52">Dĭ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_213" n="HIAT:w" s="T53">bar</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_217" n="HIAT:w" s="T54">m-</ts>
                  <nts id="Seg_218" n="HIAT:ip">)</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">dĭbər</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_224" n="HIAT:w" s="T56">i</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_227" n="HIAT:w" s="T57">döbər</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_230" n="HIAT:w" s="T58">mĭmbi</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_234" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">Dĭgəttə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_239" n="HIAT:w" s="T60">mĭbi</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_242" n="HIAT:w" s="T61">dĭʔnə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_245" n="HIAT:w" s="T62">zălatoj</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">jablokă</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_251" n="HIAT:w" s="T64">da</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T65">kalla</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_257" n="HIAT:w" s="T68">dʼürbi</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_261" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_263" n="HIAT:w" s="T66">Kabarləj</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T67" id="Seg_266" n="sc" s="T0">
               <ts e="T1" id="Seg_268" n="e" s="T0">Amnobi </ts>
               <ts e="T2" id="Seg_270" n="e" s="T1">kaməndə </ts>
               <ts e="T3" id="Seg_272" n="e" s="T2">urgo </ts>
               <ts e="T4" id="Seg_274" n="e" s="T3">koŋ. </ts>
               <ts e="T5" id="Seg_276" n="e" s="T4">Dĭgəttə </ts>
               <ts e="T6" id="Seg_278" n="e" s="T5">măndə: </ts>
               <ts e="T7" id="Seg_280" n="e" s="T6">"Nada </ts>
               <ts e="T8" id="Seg_282" n="e" s="T7">mĭzittə </ts>
               <ts e="T9" id="Seg_284" n="e" s="T8">zălatoj </ts>
               <ts e="T10" id="Seg_286" n="e" s="T9">jablokă". </ts>
               <ts e="T11" id="Seg_288" n="e" s="T10">Bar </ts>
               <ts e="T12" id="Seg_290" n="e" s="T11">iləm </ts>
               <ts e="T13" id="Seg_292" n="e" s="T12">(sə-) </ts>
               <ts e="T14" id="Seg_294" n="e" s="T13">oʔbdəbi. </ts>
               <ts e="T15" id="Seg_296" n="e" s="T14">Šindidə </ts>
               <ts e="T16" id="Seg_298" n="e" s="T15">dĭʔnə </ts>
               <ts e="T17" id="Seg_300" n="e" s="T16">jakšə </ts>
               <ts e="T18" id="Seg_302" n="e" s="T17">ej </ts>
               <ts e="T19" id="Seg_304" n="e" s="T18">mobi </ts>
               <ts e="T20" id="Seg_306" n="e" s="T19">azittə. </ts>
               <ts e="T21" id="Seg_308" n="e" s="T20">Šobi </ts>
               <ts e="T22" id="Seg_310" n="e" s="T21">onʼiʔ </ts>
               <ts e="T23" id="Seg_312" n="e" s="T22">kuza. </ts>
               <ts e="T24" id="Seg_314" n="e" s="T23">Dĭn </ts>
               <ts e="T25" id="Seg_316" n="e" s="T24">ĭmbidə </ts>
               <ts e="T26" id="Seg_318" n="e" s="T25">naga. </ts>
               <ts e="T27" id="Seg_320" n="e" s="T26">Deʔpi </ts>
               <ts e="T28" id="Seg_322" n="e" s="T27">kuvšin </ts>
               <ts e="T29" id="Seg_324" n="e" s="T28">dĭʔnə. </ts>
               <ts e="T30" id="Seg_326" n="e" s="T29">"Ĭmbileʔ </ts>
               <ts e="T31" id="Seg_328" n="e" s="T30">tăn </ts>
               <ts e="T32" id="Seg_330" n="e" s="T31">šobial?" </ts>
               <ts e="T33" id="Seg_332" n="e" s="T32">"Măn </ts>
               <ts e="T34" id="Seg_334" n="e" s="T33">šobial, </ts>
               <ts e="T35" id="Seg_336" n="e" s="T34">tăn </ts>
               <ts e="T36" id="Seg_338" n="e" s="T35">măna </ts>
               <ts e="T37" id="Seg_340" n="e" s="T36">aktʼa </ts>
               <ts e="T38" id="Seg_342" n="e" s="T37">iʔgö </ts>
               <ts e="T39" id="Seg_344" n="e" s="T38">dolžən, </ts>
               <ts e="T40" id="Seg_346" n="e" s="T39">mĭʔ </ts>
               <ts e="T41" id="Seg_348" n="e" s="T40">măna". </ts>
               <ts e="T42" id="Seg_350" n="e" s="T41">"Da </ts>
               <ts e="T43" id="Seg_352" n="e" s="T42">tăn </ts>
               <ts e="T44" id="Seg_354" n="e" s="T43">šʼaːmnial!" </ts>
               <ts e="T45" id="Seg_356" n="e" s="T44">"(Ak-) </ts>
               <ts e="T46" id="Seg_358" n="e" s="T45">A </ts>
               <ts e="T47" id="Seg_360" n="e" s="T46">šʼaːmniam </ts>
               <ts e="T48" id="Seg_362" n="e" s="T47">dak, </ts>
               <ts e="T49" id="Seg_364" n="e" s="T48">dettə </ts>
               <ts e="T50" id="Seg_366" n="e" s="T49">măna </ts>
               <ts e="T51" id="Seg_368" n="e" s="T50">zălatoj </ts>
               <ts e="T52" id="Seg_370" n="e" s="T51">jablokă". </ts>
               <ts e="T53" id="Seg_372" n="e" s="T52">Dĭ </ts>
               <ts e="T54" id="Seg_374" n="e" s="T53">bar </ts>
               <ts e="T55" id="Seg_376" n="e" s="T54">(m-) </ts>
               <ts e="T56" id="Seg_378" n="e" s="T55">dĭbər </ts>
               <ts e="T57" id="Seg_380" n="e" s="T56">i </ts>
               <ts e="T58" id="Seg_382" n="e" s="T57">döbər </ts>
               <ts e="T59" id="Seg_384" n="e" s="T58">mĭmbi. </ts>
               <ts e="T60" id="Seg_386" n="e" s="T59">Dĭgəttə </ts>
               <ts e="T61" id="Seg_388" n="e" s="T60">mĭbi </ts>
               <ts e="T62" id="Seg_390" n="e" s="T61">dĭʔnə </ts>
               <ts e="T63" id="Seg_392" n="e" s="T62">zălatoj </ts>
               <ts e="T64" id="Seg_394" n="e" s="T63">jablokă </ts>
               <ts e="T65" id="Seg_396" n="e" s="T64">da </ts>
               <ts e="T68" id="Seg_398" n="e" s="T65">kalla </ts>
               <ts e="T66" id="Seg_400" n="e" s="T68">dʼürbi. </ts>
               <ts e="T67" id="Seg_402" n="e" s="T66">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_403" s="T0">PKZ_196X_GoldenApple_flk.001 (002)</ta>
            <ta e="T10" id="Seg_404" s="T4">PKZ_196X_GoldenApple_flk.002 (003)</ta>
            <ta e="T14" id="Seg_405" s="T10">PKZ_196X_GoldenApple_flk.003 (005)</ta>
            <ta e="T20" id="Seg_406" s="T14">PKZ_196X_GoldenApple_flk.004 (006)</ta>
            <ta e="T23" id="Seg_407" s="T20">PKZ_196X_GoldenApple_flk.005 (007)</ta>
            <ta e="T26" id="Seg_408" s="T23">PKZ_196X_GoldenApple_flk.006 (008)</ta>
            <ta e="T29" id="Seg_409" s="T26">PKZ_196X_GoldenApple_flk.007 (009)</ta>
            <ta e="T32" id="Seg_410" s="T29">PKZ_196X_GoldenApple_flk.008 (010)</ta>
            <ta e="T41" id="Seg_411" s="T32">PKZ_196X_GoldenApple_flk.009 (011)</ta>
            <ta e="T44" id="Seg_412" s="T41">PKZ_196X_GoldenApple_flk.010 (012)</ta>
            <ta e="T52" id="Seg_413" s="T44">PKZ_196X_GoldenApple_flk.011 (013)</ta>
            <ta e="T59" id="Seg_414" s="T52">PKZ_196X_GoldenApple_flk.012 (014)</ta>
            <ta e="T66" id="Seg_415" s="T59">PKZ_196X_GoldenApple_flk.013 (015)</ta>
            <ta e="T67" id="Seg_416" s="T66">PKZ_196X_GoldenApple_flk.014 (016)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_417" s="T0">Amnobi kaməndə urgo koŋ. </ta>
            <ta e="T10" id="Seg_418" s="T4">Dĭgəttə măndə: "Nada mĭzittə zălatoj jablokă". </ta>
            <ta e="T14" id="Seg_419" s="T10">Bar iləm (sə-) oʔbdəbi. </ta>
            <ta e="T20" id="Seg_420" s="T14">Šindidə dĭʔnə jakšə ej mobi azittə. </ta>
            <ta e="T23" id="Seg_421" s="T20">Šobi onʼiʔ kuza. </ta>
            <ta e="T26" id="Seg_422" s="T23">Dĭn ĭmbidə naga. </ta>
            <ta e="T29" id="Seg_423" s="T26">Deʔpi kuvšin dĭʔnə. </ta>
            <ta e="T32" id="Seg_424" s="T29">"Ĭmbileʔ tăn šobial?" </ta>
            <ta e="T41" id="Seg_425" s="T32">"Măn šobial, tăn măna aktʼa iʔgö dolžən, mĭʔ măna". </ta>
            <ta e="T44" id="Seg_426" s="T41">"Da tăn šʼaːmnial!" </ta>
            <ta e="T52" id="Seg_427" s="T44">"(Ak-) A šʼaːmniam dak, dettə măna zălatoj jablokă". </ta>
            <ta e="T59" id="Seg_428" s="T52">Dĭ bar (m-) dĭbər i döbər mĭmbi. </ta>
            <ta e="T66" id="Seg_429" s="T59">Dĭgəttə mĭbi dĭʔnə zălatoj jablokă da kalla dʼürbi. </ta>
            <ta e="T67" id="Seg_430" s="T66">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_431" s="T0">amno-bi</ta>
            <ta e="T2" id="Seg_432" s="T1">kamən=də</ta>
            <ta e="T3" id="Seg_433" s="T2">urgo</ta>
            <ta e="T4" id="Seg_434" s="T3">koŋ</ta>
            <ta e="T5" id="Seg_435" s="T4">dĭgəttə</ta>
            <ta e="T6" id="Seg_436" s="T5">măn-də</ta>
            <ta e="T7" id="Seg_437" s="T6">nada</ta>
            <ta e="T8" id="Seg_438" s="T7">mĭ-zittə</ta>
            <ta e="T10" id="Seg_439" s="T9">jablokă</ta>
            <ta e="T11" id="Seg_440" s="T10">bar</ta>
            <ta e="T12" id="Seg_441" s="T11">il-ə-m</ta>
            <ta e="T14" id="Seg_442" s="T13">oʔbdə-bi</ta>
            <ta e="T15" id="Seg_443" s="T14">šindi=də</ta>
            <ta e="T16" id="Seg_444" s="T15">dĭʔ-nə</ta>
            <ta e="T17" id="Seg_445" s="T16">jakšə</ta>
            <ta e="T18" id="Seg_446" s="T17">ej</ta>
            <ta e="T19" id="Seg_447" s="T18">mo-bi</ta>
            <ta e="T20" id="Seg_448" s="T19">a-zittə</ta>
            <ta e="T21" id="Seg_449" s="T20">šo-bi</ta>
            <ta e="T22" id="Seg_450" s="T21">onʼiʔ</ta>
            <ta e="T23" id="Seg_451" s="T22">kuza</ta>
            <ta e="T24" id="Seg_452" s="T23">dĭ-n</ta>
            <ta e="T25" id="Seg_453" s="T24">ĭmbi=də</ta>
            <ta e="T26" id="Seg_454" s="T25">naga</ta>
            <ta e="T27" id="Seg_455" s="T26">deʔ-pi</ta>
            <ta e="T28" id="Seg_456" s="T27">kuvšin</ta>
            <ta e="T29" id="Seg_457" s="T28">dĭʔ-nə</ta>
            <ta e="T30" id="Seg_458" s="T29">ĭmbi-leʔ</ta>
            <ta e="T31" id="Seg_459" s="T30">tăn</ta>
            <ta e="T32" id="Seg_460" s="T31">šo-bia-l</ta>
            <ta e="T33" id="Seg_461" s="T32">măn</ta>
            <ta e="T34" id="Seg_462" s="T33">šo-bia-l</ta>
            <ta e="T35" id="Seg_463" s="T34">tăn</ta>
            <ta e="T36" id="Seg_464" s="T35">măna</ta>
            <ta e="T37" id="Seg_465" s="T36">aktʼa</ta>
            <ta e="T38" id="Seg_466" s="T37">iʔgö</ta>
            <ta e="T40" id="Seg_467" s="T39">mĭ-ʔ</ta>
            <ta e="T41" id="Seg_468" s="T40">măna</ta>
            <ta e="T42" id="Seg_469" s="T41">da</ta>
            <ta e="T43" id="Seg_470" s="T42">tăn</ta>
            <ta e="T44" id="Seg_471" s="T43">šʼaːm-nia-l</ta>
            <ta e="T46" id="Seg_472" s="T45">a</ta>
            <ta e="T47" id="Seg_473" s="T46">šʼaːm-nia-m</ta>
            <ta e="T48" id="Seg_474" s="T47">dak</ta>
            <ta e="T49" id="Seg_475" s="T48">det-tə</ta>
            <ta e="T50" id="Seg_476" s="T49">măna</ta>
            <ta e="T52" id="Seg_477" s="T51">jablokă</ta>
            <ta e="T53" id="Seg_478" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_479" s="T53">bar</ta>
            <ta e="T56" id="Seg_480" s="T55">dĭbər</ta>
            <ta e="T57" id="Seg_481" s="T56">i</ta>
            <ta e="T58" id="Seg_482" s="T57">döbər</ta>
            <ta e="T59" id="Seg_483" s="T58">mĭm-bi</ta>
            <ta e="T60" id="Seg_484" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_485" s="T60">mĭ-bi</ta>
            <ta e="T62" id="Seg_486" s="T61">dĭʔ-nə</ta>
            <ta e="T64" id="Seg_487" s="T63">jablokă</ta>
            <ta e="T65" id="Seg_488" s="T64">da</ta>
            <ta e="T68" id="Seg_489" s="T65">kal-la</ta>
            <ta e="T66" id="Seg_490" s="T68">dʼür-bi</ta>
            <ta e="T67" id="Seg_491" s="T66">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_492" s="T0">amno-bi</ta>
            <ta e="T2" id="Seg_493" s="T1">kamən=də</ta>
            <ta e="T3" id="Seg_494" s="T2">urgo</ta>
            <ta e="T4" id="Seg_495" s="T3">koŋ</ta>
            <ta e="T5" id="Seg_496" s="T4">dĭgəttə</ta>
            <ta e="T6" id="Seg_497" s="T5">măn-ntə</ta>
            <ta e="T7" id="Seg_498" s="T6">nadə</ta>
            <ta e="T8" id="Seg_499" s="T7">mĭ-zittə</ta>
            <ta e="T10" id="Seg_500" s="T9">jablokă</ta>
            <ta e="T11" id="Seg_501" s="T10">bar</ta>
            <ta e="T12" id="Seg_502" s="T11">il-ə-m</ta>
            <ta e="T14" id="Seg_503" s="T13">oʔbdə-bi</ta>
            <ta e="T15" id="Seg_504" s="T14">šində=də</ta>
            <ta e="T16" id="Seg_505" s="T15">dĭ-Tə</ta>
            <ta e="T17" id="Seg_506" s="T16">jakšə</ta>
            <ta e="T18" id="Seg_507" s="T17">ej</ta>
            <ta e="T19" id="Seg_508" s="T18">mo-bi</ta>
            <ta e="T20" id="Seg_509" s="T19">a-zittə</ta>
            <ta e="T21" id="Seg_510" s="T20">šo-bi</ta>
            <ta e="T22" id="Seg_511" s="T21">onʼiʔ</ta>
            <ta e="T23" id="Seg_512" s="T22">kuza</ta>
            <ta e="T24" id="Seg_513" s="T23">dĭ-n</ta>
            <ta e="T25" id="Seg_514" s="T24">ĭmbi=də</ta>
            <ta e="T26" id="Seg_515" s="T25">naga</ta>
            <ta e="T27" id="Seg_516" s="T26">det-bi</ta>
            <ta e="T28" id="Seg_517" s="T27">kuvšin</ta>
            <ta e="T29" id="Seg_518" s="T28">dĭ-Tə</ta>
            <ta e="T30" id="Seg_519" s="T29">ĭmbi-%%</ta>
            <ta e="T31" id="Seg_520" s="T30">tăn</ta>
            <ta e="T32" id="Seg_521" s="T31">šo-bi-l</ta>
            <ta e="T33" id="Seg_522" s="T32">măn</ta>
            <ta e="T34" id="Seg_523" s="T33">šo-bi-l</ta>
            <ta e="T35" id="Seg_524" s="T34">tăn</ta>
            <ta e="T36" id="Seg_525" s="T35">măna</ta>
            <ta e="T37" id="Seg_526" s="T36">aktʼa</ta>
            <ta e="T38" id="Seg_527" s="T37">iʔgö</ta>
            <ta e="T40" id="Seg_528" s="T39">mĭ-ʔ</ta>
            <ta e="T41" id="Seg_529" s="T40">măna</ta>
            <ta e="T42" id="Seg_530" s="T41">da</ta>
            <ta e="T43" id="Seg_531" s="T42">tăn</ta>
            <ta e="T44" id="Seg_532" s="T43">šʼaːm-liA-l</ta>
            <ta e="T46" id="Seg_533" s="T45">a</ta>
            <ta e="T47" id="Seg_534" s="T46">šʼaːm-liA-m</ta>
            <ta e="T48" id="Seg_535" s="T47">tak</ta>
            <ta e="T49" id="Seg_536" s="T48">det-t</ta>
            <ta e="T50" id="Seg_537" s="T49">măna</ta>
            <ta e="T52" id="Seg_538" s="T51">jablokă</ta>
            <ta e="T53" id="Seg_539" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_540" s="T53">bar</ta>
            <ta e="T56" id="Seg_541" s="T55">dĭbər</ta>
            <ta e="T57" id="Seg_542" s="T56">i</ta>
            <ta e="T58" id="Seg_543" s="T57">döbər</ta>
            <ta e="T59" id="Seg_544" s="T58">mĭn-bi</ta>
            <ta e="T60" id="Seg_545" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_546" s="T60">mĭ-bi</ta>
            <ta e="T62" id="Seg_547" s="T61">dĭ-Tə</ta>
            <ta e="T64" id="Seg_548" s="T63">jablokă</ta>
            <ta e="T65" id="Seg_549" s="T64">da</ta>
            <ta e="T68" id="Seg_550" s="T65">kan-lAʔ</ta>
            <ta e="T66" id="Seg_551" s="T68">tʼür-bi</ta>
            <ta e="T67" id="Seg_552" s="T66">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_553" s="T0">live-PST.[3SG]</ta>
            <ta e="T2" id="Seg_554" s="T1">when=INDEF</ta>
            <ta e="T3" id="Seg_555" s="T2">big.[NOM.SG]</ta>
            <ta e="T4" id="Seg_556" s="T3">chief.[NOM.SG]</ta>
            <ta e="T5" id="Seg_557" s="T4">then</ta>
            <ta e="T6" id="Seg_558" s="T5">say-IPFVZ.[3SG]</ta>
            <ta e="T7" id="Seg_559" s="T6">one.should</ta>
            <ta e="T8" id="Seg_560" s="T7">give-INF.LAT</ta>
            <ta e="T10" id="Seg_561" s="T9">apple.[NOM.SG]</ta>
            <ta e="T11" id="Seg_562" s="T10">all</ta>
            <ta e="T12" id="Seg_563" s="T11">people-EP-ACC</ta>
            <ta e="T14" id="Seg_564" s="T13">collect-PST.[3SG]</ta>
            <ta e="T15" id="Seg_565" s="T14">who.[NOM.SG]=INDEF</ta>
            <ta e="T16" id="Seg_566" s="T15">this-LAT</ta>
            <ta e="T17" id="Seg_567" s="T16">good.[NOM.SG]</ta>
            <ta e="T18" id="Seg_568" s="T17">NEG</ta>
            <ta e="T19" id="Seg_569" s="T18">can-PST.[3SG]</ta>
            <ta e="T20" id="Seg_570" s="T19">make-INF.LAT</ta>
            <ta e="T21" id="Seg_571" s="T20">come-PST.[3SG]</ta>
            <ta e="T22" id="Seg_572" s="T21">one.[NOM.SG]</ta>
            <ta e="T23" id="Seg_573" s="T22">man.[NOM.SG]</ta>
            <ta e="T24" id="Seg_574" s="T23">this-GEN</ta>
            <ta e="T25" id="Seg_575" s="T24">what.[NOM.SG]=INDEF</ta>
            <ta e="T26" id="Seg_576" s="T25">NEG.EX.[3SG]</ta>
            <ta e="T27" id="Seg_577" s="T26">bring-PST.[3SG]</ta>
            <ta e="T28" id="Seg_578" s="T27">jug.[NOM.SG]</ta>
            <ta e="T29" id="Seg_579" s="T28">this-LAT</ta>
            <ta e="T30" id="Seg_580" s="T29">what-%%</ta>
            <ta e="T31" id="Seg_581" s="T30">you.NOM</ta>
            <ta e="T32" id="Seg_582" s="T31">come-PST-2SG</ta>
            <ta e="T33" id="Seg_583" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_584" s="T33">come-PST-2SG</ta>
            <ta e="T35" id="Seg_585" s="T34">you.NOM</ta>
            <ta e="T36" id="Seg_586" s="T35">I.LAT</ta>
            <ta e="T37" id="Seg_587" s="T36">money.[NOM.SG]</ta>
            <ta e="T38" id="Seg_588" s="T37">many</ta>
            <ta e="T40" id="Seg_589" s="T39">give-IMP.2SG</ta>
            <ta e="T41" id="Seg_590" s="T40">I.LAT</ta>
            <ta e="T42" id="Seg_591" s="T41">and</ta>
            <ta e="T43" id="Seg_592" s="T42">you.NOM</ta>
            <ta e="T44" id="Seg_593" s="T43">lie-PRS-2SG</ta>
            <ta e="T46" id="Seg_594" s="T45">and</ta>
            <ta e="T47" id="Seg_595" s="T46">lie-PRS-1SG</ta>
            <ta e="T48" id="Seg_596" s="T47">so</ta>
            <ta e="T49" id="Seg_597" s="T48">bring-IMP.2SG.O</ta>
            <ta e="T50" id="Seg_598" s="T49">I.LAT</ta>
            <ta e="T52" id="Seg_599" s="T51">apple.[NOM.SG]</ta>
            <ta e="T53" id="Seg_600" s="T52">this.[NOM.SG]</ta>
            <ta e="T54" id="Seg_601" s="T53">PTCL</ta>
            <ta e="T56" id="Seg_602" s="T55">there</ta>
            <ta e="T57" id="Seg_603" s="T56">and</ta>
            <ta e="T58" id="Seg_604" s="T57">here</ta>
            <ta e="T59" id="Seg_605" s="T58">go-PST.[3SG]</ta>
            <ta e="T60" id="Seg_606" s="T59">then</ta>
            <ta e="T61" id="Seg_607" s="T60">give-PST.[3SG]</ta>
            <ta e="T62" id="Seg_608" s="T61">this-LAT</ta>
            <ta e="T64" id="Seg_609" s="T63">apple.[NOM.SG]</ta>
            <ta e="T65" id="Seg_610" s="T64">and</ta>
            <ta e="T68" id="Seg_611" s="T65">go-CVB</ta>
            <ta e="T66" id="Seg_612" s="T68">disappear-PST.[3SG]</ta>
            <ta e="T67" id="Seg_613" s="T66">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_614" s="T0">жить-PST.[3SG]</ta>
            <ta e="T2" id="Seg_615" s="T1">когда=INDEF</ta>
            <ta e="T3" id="Seg_616" s="T2">большой.[NOM.SG]</ta>
            <ta e="T4" id="Seg_617" s="T3">вождь.[NOM.SG]</ta>
            <ta e="T5" id="Seg_618" s="T4">тогда</ta>
            <ta e="T6" id="Seg_619" s="T5">сказать-IPFVZ.[3SG]</ta>
            <ta e="T7" id="Seg_620" s="T6">надо</ta>
            <ta e="T8" id="Seg_621" s="T7">дать-INF.LAT</ta>
            <ta e="T10" id="Seg_622" s="T9">яблоко.[NOM.SG]</ta>
            <ta e="T11" id="Seg_623" s="T10">весь</ta>
            <ta e="T12" id="Seg_624" s="T11">люди-EP-ACC</ta>
            <ta e="T14" id="Seg_625" s="T13">собирать-PST.[3SG]</ta>
            <ta e="T15" id="Seg_626" s="T14">кто.[NOM.SG]=INDEF</ta>
            <ta e="T16" id="Seg_627" s="T15">этот-LAT</ta>
            <ta e="T17" id="Seg_628" s="T16">хороший.[NOM.SG]</ta>
            <ta e="T18" id="Seg_629" s="T17">NEG</ta>
            <ta e="T19" id="Seg_630" s="T18">мочь-PST.[3SG]</ta>
            <ta e="T20" id="Seg_631" s="T19">делать-INF.LAT</ta>
            <ta e="T21" id="Seg_632" s="T20">прийти-PST.[3SG]</ta>
            <ta e="T22" id="Seg_633" s="T21">один.[NOM.SG]</ta>
            <ta e="T23" id="Seg_634" s="T22">мужчина.[NOM.SG]</ta>
            <ta e="T24" id="Seg_635" s="T23">этот-GEN</ta>
            <ta e="T25" id="Seg_636" s="T24">что.[NOM.SG]=INDEF</ta>
            <ta e="T26" id="Seg_637" s="T25">NEG.EX.[3SG]</ta>
            <ta e="T27" id="Seg_638" s="T26">принести-PST.[3SG]</ta>
            <ta e="T28" id="Seg_639" s="T27">кувшин.[NOM.SG]</ta>
            <ta e="T29" id="Seg_640" s="T28">этот-LAT</ta>
            <ta e="T30" id="Seg_641" s="T29">что-%%</ta>
            <ta e="T31" id="Seg_642" s="T30">ты.NOM</ta>
            <ta e="T32" id="Seg_643" s="T31">прийти-PST-2SG</ta>
            <ta e="T33" id="Seg_644" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_645" s="T33">прийти-PST-2SG</ta>
            <ta e="T35" id="Seg_646" s="T34">ты.NOM</ta>
            <ta e="T36" id="Seg_647" s="T35">я.LAT</ta>
            <ta e="T37" id="Seg_648" s="T36">деньги.[NOM.SG]</ta>
            <ta e="T38" id="Seg_649" s="T37">много</ta>
            <ta e="T40" id="Seg_650" s="T39">дать-IMP.2SG</ta>
            <ta e="T41" id="Seg_651" s="T40">я.LAT</ta>
            <ta e="T42" id="Seg_652" s="T41">и</ta>
            <ta e="T43" id="Seg_653" s="T42">ты.NOM</ta>
            <ta e="T44" id="Seg_654" s="T43">лгать-PRS-2SG</ta>
            <ta e="T46" id="Seg_655" s="T45">а</ta>
            <ta e="T47" id="Seg_656" s="T46">лгать-PRS-1SG</ta>
            <ta e="T48" id="Seg_657" s="T47">так</ta>
            <ta e="T49" id="Seg_658" s="T48">принести-IMP.2SG.O</ta>
            <ta e="T50" id="Seg_659" s="T49">я.LAT</ta>
            <ta e="T52" id="Seg_660" s="T51">яблоко.[NOM.SG]</ta>
            <ta e="T53" id="Seg_661" s="T52">этот.[NOM.SG]</ta>
            <ta e="T54" id="Seg_662" s="T53">PTCL</ta>
            <ta e="T56" id="Seg_663" s="T55">там</ta>
            <ta e="T57" id="Seg_664" s="T56">и</ta>
            <ta e="T58" id="Seg_665" s="T57">здесь</ta>
            <ta e="T59" id="Seg_666" s="T58">идти-PST.[3SG]</ta>
            <ta e="T60" id="Seg_667" s="T59">тогда</ta>
            <ta e="T61" id="Seg_668" s="T60">дать-PST.[3SG]</ta>
            <ta e="T62" id="Seg_669" s="T61">этот-LAT</ta>
            <ta e="T64" id="Seg_670" s="T63">яблоко.[NOM.SG]</ta>
            <ta e="T65" id="Seg_671" s="T64">и</ta>
            <ta e="T68" id="Seg_672" s="T65">пойти-CVB</ta>
            <ta e="T66" id="Seg_673" s="T68">исчезнуть-PST.[3SG]</ta>
            <ta e="T67" id="Seg_674" s="T66">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_675" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_676" s="T1">que=ptcl</ta>
            <ta e="T3" id="Seg_677" s="T2">adj-n:case</ta>
            <ta e="T4" id="Seg_678" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_679" s="T4">adv</ta>
            <ta e="T6" id="Seg_680" s="T5">v-v&gt;v-v:pn</ta>
            <ta e="T7" id="Seg_681" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_682" s="T7">v-v:n.fin</ta>
            <ta e="T10" id="Seg_683" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_684" s="T10">quant</ta>
            <ta e="T12" id="Seg_685" s="T11">n-n:ins-n:case</ta>
            <ta e="T14" id="Seg_686" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_687" s="T14">que-n:case=ptcl</ta>
            <ta e="T16" id="Seg_688" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_689" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_690" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_691" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_692" s="T19">v-v:n.fin</ta>
            <ta e="T21" id="Seg_693" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_694" s="T21">num-n:case</ta>
            <ta e="T23" id="Seg_695" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_696" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_697" s="T24">que-n:case=ptcl</ta>
            <ta e="T26" id="Seg_698" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_699" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_700" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_701" s="T28">dempro-n:case</ta>
            <ta e="T30" id="Seg_702" s="T29">que-%%</ta>
            <ta e="T31" id="Seg_703" s="T30">pers</ta>
            <ta e="T32" id="Seg_704" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_705" s="T32">pers</ta>
            <ta e="T34" id="Seg_706" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_707" s="T34">pers</ta>
            <ta e="T36" id="Seg_708" s="T35">pers</ta>
            <ta e="T37" id="Seg_709" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_710" s="T37">quant</ta>
            <ta e="T40" id="Seg_711" s="T39">v-v:mood.pn</ta>
            <ta e="T41" id="Seg_712" s="T40">pers</ta>
            <ta e="T42" id="Seg_713" s="T41">conj</ta>
            <ta e="T43" id="Seg_714" s="T42">pers</ta>
            <ta e="T44" id="Seg_715" s="T43">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_716" s="T45">conj</ta>
            <ta e="T47" id="Seg_717" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_718" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_719" s="T48">v-v:mood.pn</ta>
            <ta e="T50" id="Seg_720" s="T49">pers</ta>
            <ta e="T52" id="Seg_721" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_722" s="T52">dempro-n:case</ta>
            <ta e="T54" id="Seg_723" s="T53">ptcl</ta>
            <ta e="T56" id="Seg_724" s="T55">adv</ta>
            <ta e="T57" id="Seg_725" s="T56">conj</ta>
            <ta e="T58" id="Seg_726" s="T57">adv</ta>
            <ta e="T59" id="Seg_727" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_728" s="T59">adv</ta>
            <ta e="T61" id="Seg_729" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_730" s="T61">dempro-n:case</ta>
            <ta e="T64" id="Seg_731" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_732" s="T64">conj</ta>
            <ta e="T68" id="Seg_733" s="T65">v-v:n-fin</ta>
            <ta e="T66" id="Seg_734" s="T68">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_735" s="T66">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_736" s="T0">v</ta>
            <ta e="T2" id="Seg_737" s="T1">que</ta>
            <ta e="T3" id="Seg_738" s="T2">adj</ta>
            <ta e="T4" id="Seg_739" s="T3">n</ta>
            <ta e="T5" id="Seg_740" s="T4">adv</ta>
            <ta e="T6" id="Seg_741" s="T5">v</ta>
            <ta e="T7" id="Seg_742" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_743" s="T7">v</ta>
            <ta e="T10" id="Seg_744" s="T9">n</ta>
            <ta e="T11" id="Seg_745" s="T10">quant</ta>
            <ta e="T12" id="Seg_746" s="T11">n</ta>
            <ta e="T14" id="Seg_747" s="T13">v</ta>
            <ta e="T15" id="Seg_748" s="T14">que</ta>
            <ta e="T16" id="Seg_749" s="T15">dempro</ta>
            <ta e="T17" id="Seg_750" s="T16">adj</ta>
            <ta e="T18" id="Seg_751" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_752" s="T18">v</ta>
            <ta e="T20" id="Seg_753" s="T19">v</ta>
            <ta e="T21" id="Seg_754" s="T20">v</ta>
            <ta e="T22" id="Seg_755" s="T21">num</ta>
            <ta e="T23" id="Seg_756" s="T22">n</ta>
            <ta e="T24" id="Seg_757" s="T23">dempro</ta>
            <ta e="T25" id="Seg_758" s="T24">que</ta>
            <ta e="T26" id="Seg_759" s="T25">v</ta>
            <ta e="T27" id="Seg_760" s="T26">v</ta>
            <ta e="T28" id="Seg_761" s="T27">n</ta>
            <ta e="T29" id="Seg_762" s="T28">dempro</ta>
            <ta e="T30" id="Seg_763" s="T29">que</ta>
            <ta e="T31" id="Seg_764" s="T30">pers</ta>
            <ta e="T32" id="Seg_765" s="T31">v</ta>
            <ta e="T33" id="Seg_766" s="T32">pers</ta>
            <ta e="T34" id="Seg_767" s="T33">v</ta>
            <ta e="T35" id="Seg_768" s="T34">pers</ta>
            <ta e="T36" id="Seg_769" s="T35">pers</ta>
            <ta e="T37" id="Seg_770" s="T36">n</ta>
            <ta e="T38" id="Seg_771" s="T37">quant</ta>
            <ta e="T40" id="Seg_772" s="T39">pers</ta>
            <ta e="T41" id="Seg_773" s="T40">pers</ta>
            <ta e="T42" id="Seg_774" s="T41">conj</ta>
            <ta e="T43" id="Seg_775" s="T42">pers</ta>
            <ta e="T44" id="Seg_776" s="T43">v</ta>
            <ta e="T46" id="Seg_777" s="T45">conj</ta>
            <ta e="T47" id="Seg_778" s="T46">v</ta>
            <ta e="T48" id="Seg_779" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_780" s="T48">v</ta>
            <ta e="T50" id="Seg_781" s="T49">pers</ta>
            <ta e="T52" id="Seg_782" s="T51">n</ta>
            <ta e="T53" id="Seg_783" s="T52">dempro</ta>
            <ta e="T54" id="Seg_784" s="T53">ptcl</ta>
            <ta e="T56" id="Seg_785" s="T55">adv</ta>
            <ta e="T57" id="Seg_786" s="T56">conj</ta>
            <ta e="T58" id="Seg_787" s="T57">adv</ta>
            <ta e="T59" id="Seg_788" s="T58">v</ta>
            <ta e="T60" id="Seg_789" s="T59">adv</ta>
            <ta e="T61" id="Seg_790" s="T60">v</ta>
            <ta e="T62" id="Seg_791" s="T61">dempro</ta>
            <ta e="T64" id="Seg_792" s="T63">n</ta>
            <ta e="T65" id="Seg_793" s="T64">conj</ta>
            <ta e="T68" id="Seg_794" s="T65">v</ta>
            <ta e="T66" id="Seg_795" s="T68">v</ta>
            <ta e="T67" id="Seg_796" s="T66">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_797" s="T0">v:pred</ta>
            <ta e="T4" id="Seg_798" s="T3">np.h:S</ta>
            <ta e="T6" id="Seg_799" s="T5">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_800" s="T9">np:O</ta>
            <ta e="T12" id="Seg_801" s="T11">np.h:O</ta>
            <ta e="T14" id="Seg_802" s="T13">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_803" s="T14">pro.h:S</ta>
            <ta e="T19" id="Seg_804" s="T18">v:pred</ta>
            <ta e="T21" id="Seg_805" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_806" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_807" s="T24">pro:S</ta>
            <ta e="T26" id="Seg_808" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_809" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_810" s="T27">np:O</ta>
            <ta e="T31" id="Seg_811" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_812" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_813" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_814" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_815" s="T34">pro.h:S</ta>
            <ta e="T40" id="Seg_816" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_817" s="T42">pro.h:S</ta>
            <ta e="T44" id="Seg_818" s="T43">v:pred</ta>
            <ta e="T48" id="Seg_819" s="T44">s:cond</ta>
            <ta e="T49" id="Seg_820" s="T48">0.2.h:S v:pred</ta>
            <ta e="T52" id="Seg_821" s="T51">np:O</ta>
            <ta e="T53" id="Seg_822" s="T52">pro:S</ta>
            <ta e="T59" id="Seg_823" s="T58">v:pred</ta>
            <ta e="T61" id="Seg_824" s="T60">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_825" s="T63">np:O</ta>
            <ta e="T68" id="Seg_826" s="T65">conv:pred</ta>
            <ta e="T66" id="Seg_827" s="T68">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_828" s="T1">adv:Time</ta>
            <ta e="T4" id="Seg_829" s="T3">np.h:Th</ta>
            <ta e="T6" id="Seg_830" s="T5">0.3.h:A</ta>
            <ta e="T10" id="Seg_831" s="T9">np:Th</ta>
            <ta e="T12" id="Seg_832" s="T11">np.h:Th</ta>
            <ta e="T14" id="Seg_833" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_834" s="T14">pro.h:A</ta>
            <ta e="T16" id="Seg_835" s="T15">pro.h:B</ta>
            <ta e="T23" id="Seg_836" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_837" s="T23">pro.h:Poss</ta>
            <ta e="T25" id="Seg_838" s="T24">pro:Th</ta>
            <ta e="T27" id="Seg_839" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_840" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_841" s="T28">pro.h:R</ta>
            <ta e="T31" id="Seg_842" s="T30">pro.h:A</ta>
            <ta e="T33" id="Seg_843" s="T32">pro.h:A</ta>
            <ta e="T35" id="Seg_844" s="T34">pro.h:A</ta>
            <ta e="T36" id="Seg_845" s="T35">pro.h:R</ta>
            <ta e="T37" id="Seg_846" s="T36">np:Th</ta>
            <ta e="T41" id="Seg_847" s="T40">pro.h:R</ta>
            <ta e="T43" id="Seg_848" s="T42">pro.h:A</ta>
            <ta e="T47" id="Seg_849" s="T46">0.1.h:A</ta>
            <ta e="T49" id="Seg_850" s="T48">0.2.h:A</ta>
            <ta e="T50" id="Seg_851" s="T49">pro.h:R</ta>
            <ta e="T52" id="Seg_852" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_853" s="T52">pro:A</ta>
            <ta e="T61" id="Seg_854" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_855" s="T61">pro.h:R</ta>
            <ta e="T64" id="Seg_856" s="T63">np:Th</ta>
            <ta e="T66" id="Seg_857" s="T68">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_858" s="T1">TURK:gram(INDEF)</ta>
            <ta e="T4" id="Seg_859" s="T3">TURK:cult</ta>
            <ta e="T7" id="Seg_860" s="T6">RUS:mod</ta>
            <ta e="T9" id="Seg_861" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_862" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_863" s="T10">TURK:disc</ta>
            <ta e="T15" id="Seg_864" s="T14">TURK:gram(INDEF)</ta>
            <ta e="T17" id="Seg_865" s="T16">TURK:core</ta>
            <ta e="T25" id="Seg_866" s="T24">TURK:gram(INDEF)</ta>
            <ta e="T28" id="Seg_867" s="T27">RUS:cult</ta>
            <ta e="T37" id="Seg_868" s="T36">TAT:cult</ta>
            <ta e="T42" id="Seg_869" s="T41">RUS:gram</ta>
            <ta e="T46" id="Seg_870" s="T45">RUS:gram</ta>
            <ta e="T48" id="Seg_871" s="T47">RUS:gram</ta>
            <ta e="T51" id="Seg_872" s="T50">RUS:cult</ta>
            <ta e="T52" id="Seg_873" s="T51">RUS:cult</ta>
            <ta e="T54" id="Seg_874" s="T53">TURK:disc</ta>
            <ta e="T57" id="Seg_875" s="T56">RUS:gram</ta>
            <ta e="T63" id="Seg_876" s="T62">RUS:cult</ta>
            <ta e="T64" id="Seg_877" s="T63">RUS:cult</ta>
            <ta e="T65" id="Seg_878" s="T64">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T39" id="Seg_879" s="T38">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_880" s="T0">Жил однажды большой царь.</ta>
            <ta e="T6" id="Seg_881" s="T4">Потом он говорит:</ta>
            <ta e="T10" id="Seg_882" s="T6">"Надо дать золотое яблоко".</ta>
            <ta e="T14" id="Seg_883" s="T10">Он собрал весь народ.</ta>
            <ta e="T20" id="Seg_884" s="T14">Никто не мог ему хорошо сделать [=солгать].</ta>
            <ta e="T23" id="Seg_885" s="T20">Пришел один человек.</ta>
            <ta e="T26" id="Seg_886" s="T23">У него ничего не было.</ta>
            <ta e="T29" id="Seg_887" s="T26">Принес ему кувшин.</ta>
            <ta e="T32" id="Seg_888" s="T29">"Зачем ты пришел?"</ta>
            <ta e="T41" id="Seg_889" s="T32">"Я пришел, [потому что] ты мне много денег должен, отдай мне!"</ta>
            <ta e="T44" id="Seg_890" s="T41">"Ты врешь!"</ta>
            <ta e="T52" id="Seg_891" s="T44">"А если я вру, дай мне золотое яблоко!"</ta>
            <ta e="T59" id="Seg_892" s="T52">Он ходил туда и сюда.</ta>
            <ta e="T66" id="Seg_893" s="T59">Потом он дал ему золотое яблоко, и [тот] ушел.</ta>
            <ta e="T67" id="Seg_894" s="T66">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_895" s="T0">There lived at some point a big chief.</ta>
            <ta e="T6" id="Seg_896" s="T4">Then he says:</ta>
            <ta e="T10" id="Seg_897" s="T6">"[I] must give a golden apple."</ta>
            <ta e="T14" id="Seg_898" s="T10">He gathered all the people.</ta>
            <ta e="T20" id="Seg_899" s="T14">Nobody could make it good [=lie] for him.</ta>
            <ta e="T23" id="Seg_900" s="T20">One man came.</ta>
            <ta e="T26" id="Seg_901" s="T23">He had nothing.</ta>
            <ta e="T29" id="Seg_902" s="T26">He brought him a jug.</ta>
            <ta e="T32" id="Seg_903" s="T29">"What did you come for?"</ta>
            <ta e="T41" id="Seg_904" s="T32">"I came, [because] you owe me a lot of money, give me!"</ta>
            <ta e="T44" id="Seg_905" s="T41">"You are lying!"</ta>
            <ta e="T52" id="Seg_906" s="T44">"But [if] I am lying, then bring me a golden apple!"</ta>
            <ta e="T59" id="Seg_907" s="T52">He went there and here.</ta>
            <ta e="T66" id="Seg_908" s="T59">Then he gave him a golden apple and [that person] left.</ta>
            <ta e="T67" id="Seg_909" s="T66">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_910" s="T0">Es lebte einmal ein großer Zar.</ta>
            <ta e="T6" id="Seg_911" s="T4">Dann sagt er:</ta>
            <ta e="T10" id="Seg_912" s="T6">"[Ich] muss einen goldenen Apfel geben."</ta>
            <ta e="T14" id="Seg_913" s="T10">Er versammelte alle Leute.</ta>
            <ta e="T20" id="Seg_914" s="T14">Niemand konnte es ihm recht machen [=lügen].</ta>
            <ta e="T23" id="Seg_915" s="T20">Ein Mann kam.</ta>
            <ta e="T26" id="Seg_916" s="T23">Er hatte nichts.</ta>
            <ta e="T29" id="Seg_917" s="T26">Er brachte ihm einen Krug.</ta>
            <ta e="T32" id="Seg_918" s="T29">"Wozu bist du gekommen?"</ta>
            <ta e="T41" id="Seg_919" s="T32">"Ich bin gekommen, [weil] du mir viel Geld schuldest, gib mir!"</ta>
            <ta e="T44" id="Seg_920" s="T41">"Du lügst!"</ta>
            <ta e="T52" id="Seg_921" s="T44">"Aber [wenn] ich lüge, dann bring mir einen goldenen Apfel!"</ta>
            <ta e="T59" id="Seg_922" s="T52">Er ging hierhin und dorthin.</ta>
            <ta e="T66" id="Seg_923" s="T59">Dann gab er ihm einen goldenen Apfel und [dieser Mann] ging.</ta>
            <ta e="T67" id="Seg_924" s="T66">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_925" s="T6">[GVY:] The golden apple will be given to the person who is best in telling lies".</ta>
            <ta e="T41" id="Seg_926" s="T32">[GVY:] šobial = šobiam</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T68" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
