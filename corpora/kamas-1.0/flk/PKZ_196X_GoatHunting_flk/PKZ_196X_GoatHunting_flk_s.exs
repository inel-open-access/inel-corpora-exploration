<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF671DEA1-BCAA-4352-F787-319502C0D312">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GoatHunting_flk.wav" />
         <referenced-file url="PKZ_196X_GoatHunting_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GoatHunting_flk\PKZ_196X_GoatHunting_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">90</ud-information>
            <ud-information attribute-name="# HIAT:w">61</ud-information>
            <ud-information attribute-name="# e">61</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T520" time="0.1" type="appl" />
         <tli id="T521" time="0.8" type="appl" />
         <tli id="T522" time="1.499" type="appl" />
         <tli id="T523" time="2.198" type="appl" />
         <tli id="T524" time="2.898" type="appl" />
         <tli id="T525" time="3.813" type="appl" />
         <tli id="T526" time="4.727" type="appl" />
         <tli id="T527" time="5.98578365962463" />
         <tli id="T528" time="6.684" type="appl" />
         <tli id="T529" time="7.445" type="appl" />
         <tli id="T530" time="8.207" type="appl" />
         <tli id="T531" time="8.969" type="appl" />
         <tli id="T532" time="9.669" type="appl" />
         <tli id="T533" time="10.23" type="appl" />
         <tli id="T534" time="10.792" type="appl" />
         <tli id="T535" time="11.353" type="appl" />
         <tli id="T536" time="11.914" type="appl" />
         <tli id="T537" time="12.811443422938238" />
         <tli id="T538" time="13.252" type="appl" />
         <tli id="T539" time="13.861" type="appl" />
         <tli id="T540" time="14.47" type="appl" />
         <tli id="T541" time="15.08" type="appl" />
         <tli id="T542" time="15.689" type="appl" />
         <tli id="T543" time="16.298" type="appl" />
         <tli id="T544" time="17.530747243666788" />
         <tli id="T545" time="18.157" type="appl" />
         <tli id="T546" time="18.701" type="appl" />
         <tli id="T547" time="19.245" type="appl" />
         <tli id="T548" time="19.789" type="appl" />
         <tli id="T549" time="20.333" type="appl" />
         <tli id="T550" time="20.877" type="appl" />
         <tli id="T551" time="21.421" type="appl" />
         <tli id="T552" time="21.965" type="appl" />
         <tli id="T553" time="22.509" type="appl" />
         <tli id="T554" time="23.176581051798266" />
         <tli id="T555" time="25.242942894207655" />
         <tli id="T556" time="25.921" type="appl" />
         <tli id="T557" time="26.592" type="appl" />
         <tli id="T558" time="27.263" type="appl" />
         <tli id="T559" time="27.933" type="appl" />
         <tli id="T560" time="28.604" type="appl" />
         <tli id="T561" time="29.275" type="appl" />
         <tli id="T562" time="30.60215231774686" />
         <tli id="T563" time="31.445" type="appl" />
         <tli id="T564" time="32.144" type="appl" />
         <tli id="T565" time="32.843" type="appl" />
         <tli id="T566" time="33.542" type="appl" />
         <tli id="T567" time="34.241" type="appl" />
         <tli id="T568" time="34.94" type="appl" />
         <tli id="T569" time="35.639" type="appl" />
         <tli id="T570" time="36.338" type="appl" />
         <tli id="T571" time="37.139" type="appl" />
         <tli id="T572" time="37.94" type="appl" />
         <tli id="T573" time="38.74" type="appl" />
         <tli id="T574" time="39.541" type="appl" />
         <tli id="T575" time="40.342" type="appl" />
         <tli id="T576" time="41.045" type="appl" />
         <tli id="T577" time="42.267098202316014" />
         <tli id="T578" time="42.894" type="appl" />
         <tli id="T579" time="43.464" type="appl" />
         <tli id="T580" time="44.034" type="appl" />
         <tli id="T581" time="45.06" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T581" id="Seg_0" n="sc" s="T520">
               <ts e="T524" id="Seg_2" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_4" n="HIAT:w" s="T520">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_7" n="HIAT:w" s="T521">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_10" n="HIAT:w" s="T522">kambi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_13" n="HIAT:w" s="T523">dʼijenə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_17" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_19" n="HIAT:w" s="T524">Xatʼel</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_22" n="HIAT:w" s="T525">kuʔsittə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_25" n="HIAT:w" s="T526">poʔto</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_29" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_31" n="HIAT:w" s="T527">Mĭlleʔbi</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_35" n="HIAT:w" s="T528">mĭlleʔbi</ts>
                  <nts id="Seg_36" n="HIAT:ip">,</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_39" n="HIAT:w" s="T529">ej</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_42" n="HIAT:w" s="T530">kuʔpi</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_46" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_48" n="HIAT:w" s="T531">I</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_51" n="HIAT:w" s="T532">parluʔpi</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_54" n="HIAT:w" s="T533">maːndə</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_58" n="HIAT:w" s="T534">ĭmbidə</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_61" n="HIAT:w" s="T535">ej</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_64" n="HIAT:w" s="T536">deʔpi</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_68" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_70" n="HIAT:w" s="T537">Net</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_73" n="HIAT:w" s="T538">kudonzlaʔbə:</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_75" n="HIAT:ip">"</nts>
                  <ts e="T540" id="Seg_77" n="HIAT:w" s="T539">Ĭmbi</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_80" n="HIAT:w" s="T540">ĭmbidə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_83" n="HIAT:w" s="T541">ej</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_86" n="HIAT:w" s="T542">kuʔpial</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_89" n="HIAT:w" s="T543">da</ts>
                  <nts id="Seg_90" n="HIAT:ip">?</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_93" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_95" n="HIAT:w" s="T544">Naga</ts>
                  <nts id="Seg_96" n="HIAT:ip">,</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_99" n="HIAT:w" s="T545">ĭmbidə</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_102" n="HIAT:w" s="T546">ej</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_105" n="HIAT:w" s="T547">kuʔpiam</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_108" n="HIAT:w" s="T548">dărəʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_112" n="HIAT:w" s="T549">ĭmbidə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_115" n="HIAT:w" s="T550">ej</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_118" n="HIAT:w" s="T551">deʔpiem</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_122" n="HIAT:w" s="T552">šobiam</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_125" n="HIAT:w" s="T553">maʔnʼi</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_129" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_131" n="HIAT:w" s="T554">Kabarləj</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_135" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_137" n="HIAT:w" s="T555">Dĭgəttə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_140" n="HIAT:w" s="T556">dĭ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_142" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_144" n="HIAT:w" s="T557">net</ts>
                  <nts id="Seg_145" n="HIAT:ip">)</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_148" n="HIAT:w" s="T558">sürerbi</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_152" n="HIAT:w" s="T559">dĭ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_155" n="HIAT:w" s="T560">bazoʔ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_158" n="HIAT:w" s="T561">kambi</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_162" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_164" n="HIAT:w" s="T562">Mĭmbi</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_168" n="HIAT:w" s="T563">mĭmbi</ts>
                  <nts id="Seg_169" n="HIAT:ip">,</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_172" n="HIAT:w" s="T564">onʼiʔ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_175" n="HIAT:w" s="T565">poʔto</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_178" n="HIAT:w" s="T566">kuʔpi</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_181" n="HIAT:w" s="T567">i</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_184" n="HIAT:w" s="T568">deʔpi</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_187" n="HIAT:w" s="T569">maːndə</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_191" n="HIAT:u" s="T570">
                  <nts id="Seg_192" n="HIAT:ip">(</nts>
                  <ts e="T571" id="Seg_194" n="HIAT:w" s="T570">Măndə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_197" n="HIAT:w" s="T571">am-</ts>
                  <nts id="Seg_198" n="HIAT:ip">)</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_201" n="HIAT:w" s="T572">Măndə:</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T574" id="Seg_205" n="HIAT:w" s="T573">Uja</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_208" n="HIAT:w" s="T574">deʔpiem</ts>
                  <nts id="Seg_209" n="HIAT:ip">!</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_212" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_214" n="HIAT:w" s="T575">Amoraʔ</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_217" n="HIAT:w" s="T576">tuj</ts>
                  <nts id="Seg_218" n="HIAT:ip">!</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_221" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_223" n="HIAT:w" s="T577">Da</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_226" n="HIAT:w" s="T578">iʔ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_229" n="HIAT:w" s="T579">kudonzaʔ</ts>
                  <nts id="Seg_230" n="HIAT:ip">!</nts>
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_234" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_236" n="HIAT:w" s="T580">Kabarləj</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T581" id="Seg_239" n="sc" s="T520">
               <ts e="T521" id="Seg_241" n="e" s="T520">Onʼiʔ </ts>
               <ts e="T522" id="Seg_243" n="e" s="T521">kuza </ts>
               <ts e="T523" id="Seg_245" n="e" s="T522">kambi </ts>
               <ts e="T524" id="Seg_247" n="e" s="T523">dʼijenə. </ts>
               <ts e="T525" id="Seg_249" n="e" s="T524">Xatʼel </ts>
               <ts e="T526" id="Seg_251" n="e" s="T525">kuʔsittə </ts>
               <ts e="T527" id="Seg_253" n="e" s="T526">poʔto. </ts>
               <ts e="T528" id="Seg_255" n="e" s="T527">Mĭlleʔbi, </ts>
               <ts e="T529" id="Seg_257" n="e" s="T528">mĭlleʔbi, </ts>
               <ts e="T530" id="Seg_259" n="e" s="T529">ej </ts>
               <ts e="T531" id="Seg_261" n="e" s="T530">kuʔpi. </ts>
               <ts e="T532" id="Seg_263" n="e" s="T531">I </ts>
               <ts e="T533" id="Seg_265" n="e" s="T532">parluʔpi </ts>
               <ts e="T534" id="Seg_267" n="e" s="T533">maːndə, </ts>
               <ts e="T535" id="Seg_269" n="e" s="T534">ĭmbidə </ts>
               <ts e="T536" id="Seg_271" n="e" s="T535">ej </ts>
               <ts e="T537" id="Seg_273" n="e" s="T536">deʔpi. </ts>
               <ts e="T538" id="Seg_275" n="e" s="T537">Net </ts>
               <ts e="T539" id="Seg_277" n="e" s="T538">kudonzlaʔbə: </ts>
               <ts e="T540" id="Seg_279" n="e" s="T539">"Ĭmbi </ts>
               <ts e="T541" id="Seg_281" n="e" s="T540">ĭmbidə </ts>
               <ts e="T542" id="Seg_283" n="e" s="T541">ej </ts>
               <ts e="T543" id="Seg_285" n="e" s="T542">kuʔpial </ts>
               <ts e="T544" id="Seg_287" n="e" s="T543">da? </ts>
               <ts e="T545" id="Seg_289" n="e" s="T544">Naga, </ts>
               <ts e="T546" id="Seg_291" n="e" s="T545">ĭmbidə </ts>
               <ts e="T547" id="Seg_293" n="e" s="T546">ej </ts>
               <ts e="T548" id="Seg_295" n="e" s="T547">kuʔpiam </ts>
               <ts e="T549" id="Seg_297" n="e" s="T548">dărəʔ, </ts>
               <ts e="T550" id="Seg_299" n="e" s="T549">ĭmbidə </ts>
               <ts e="T551" id="Seg_301" n="e" s="T550">ej </ts>
               <ts e="T552" id="Seg_303" n="e" s="T551">deʔpiem, </ts>
               <ts e="T553" id="Seg_305" n="e" s="T552">šobiam </ts>
               <ts e="T554" id="Seg_307" n="e" s="T553">maʔnʼi. </ts>
               <ts e="T555" id="Seg_309" n="e" s="T554">Kabarləj. </ts>
               <ts e="T556" id="Seg_311" n="e" s="T555">Dĭgəttə </ts>
               <ts e="T557" id="Seg_313" n="e" s="T556">dĭ </ts>
               <ts e="T558" id="Seg_315" n="e" s="T557">(net) </ts>
               <ts e="T559" id="Seg_317" n="e" s="T558">sürerbi, </ts>
               <ts e="T560" id="Seg_319" n="e" s="T559">dĭ </ts>
               <ts e="T561" id="Seg_321" n="e" s="T560">bazoʔ </ts>
               <ts e="T562" id="Seg_323" n="e" s="T561">kambi. </ts>
               <ts e="T563" id="Seg_325" n="e" s="T562">Mĭmbi, </ts>
               <ts e="T564" id="Seg_327" n="e" s="T563">mĭmbi, </ts>
               <ts e="T565" id="Seg_329" n="e" s="T564">onʼiʔ </ts>
               <ts e="T566" id="Seg_331" n="e" s="T565">poʔto </ts>
               <ts e="T567" id="Seg_333" n="e" s="T566">kuʔpi </ts>
               <ts e="T568" id="Seg_335" n="e" s="T567">i </ts>
               <ts e="T569" id="Seg_337" n="e" s="T568">deʔpi </ts>
               <ts e="T570" id="Seg_339" n="e" s="T569">maːndə. </ts>
               <ts e="T571" id="Seg_341" n="e" s="T570">(Măndə </ts>
               <ts e="T572" id="Seg_343" n="e" s="T571">am-) </ts>
               <ts e="T573" id="Seg_345" n="e" s="T572">Măndə: </ts>
               <ts e="T574" id="Seg_347" n="e" s="T573">"Uja </ts>
               <ts e="T575" id="Seg_349" n="e" s="T574">deʔpiem! </ts>
               <ts e="T576" id="Seg_351" n="e" s="T575">Amoraʔ </ts>
               <ts e="T577" id="Seg_353" n="e" s="T576">tuj! </ts>
               <ts e="T578" id="Seg_355" n="e" s="T577">Da </ts>
               <ts e="T579" id="Seg_357" n="e" s="T578">iʔ </ts>
               <ts e="T580" id="Seg_359" n="e" s="T579">kudonzaʔ!" </ts>
               <ts e="T581" id="Seg_361" n="e" s="T580">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T524" id="Seg_362" s="T520">PKZ_196X_GoatHunting_flk.001 (001)</ta>
            <ta e="T527" id="Seg_363" s="T524">PKZ_196X_GoatHunting_flk.002 (002)</ta>
            <ta e="T531" id="Seg_364" s="T527">PKZ_196X_GoatHunting_flk.003 (003)</ta>
            <ta e="T537" id="Seg_365" s="T531">PKZ_196X_GoatHunting_flk.004 (004)</ta>
            <ta e="T544" id="Seg_366" s="T537">PKZ_196X_GoatHunting_flk.005 (005)</ta>
            <ta e="T554" id="Seg_367" s="T544">PKZ_196X_GoatHunting_flk.006 (006)</ta>
            <ta e="T555" id="Seg_368" s="T554">PKZ_196X_GoatHunting_flk.007 (007)</ta>
            <ta e="T562" id="Seg_369" s="T555">PKZ_196X_GoatHunting_flk.008 (008)</ta>
            <ta e="T570" id="Seg_370" s="T562">PKZ_196X_GoatHunting_flk.009 (009)</ta>
            <ta e="T575" id="Seg_371" s="T570">PKZ_196X_GoatHunting_flk.010 (010)</ta>
            <ta e="T577" id="Seg_372" s="T575">PKZ_196X_GoatHunting_flk.011 (011)</ta>
            <ta e="T580" id="Seg_373" s="T577">PKZ_196X_GoatHunting_flk.012 (012)</ta>
            <ta e="T581" id="Seg_374" s="T580">PKZ_196X_GoatHunting_flk.013 (013)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T524" id="Seg_375" s="T520">Onʼiʔ kuza kambi dʼijenə. </ta>
            <ta e="T527" id="Seg_376" s="T524">Xatʼel kuʔsittə poʔto. </ta>
            <ta e="T531" id="Seg_377" s="T527">Mĭlleʔbi, mĭlleʔbi, ej kuʔpi. </ta>
            <ta e="T537" id="Seg_378" s="T531">I parluʔpi maːndə, ĭmbidə ej deʔpi. </ta>
            <ta e="T544" id="Seg_379" s="T537">Net kudonzlaʔbə:" Ĭmbi ĭmbidə ej kuʔpial da? </ta>
            <ta e="T554" id="Seg_380" s="T544">Naga, ĭmbidə ej kuʔpiam dărəʔ, ĭmbidə ej deʔpiem, šobiam maʔnʼi. </ta>
            <ta e="T555" id="Seg_381" s="T554">Kabarləj. </ta>
            <ta e="T562" id="Seg_382" s="T555">Dĭgəttə dĭ (net) sürerbi, dĭ bazoʔ kambi. </ta>
            <ta e="T570" id="Seg_383" s="T562">Mĭmbi, mĭmbi, onʼiʔ poʔto kuʔpi i deʔpi maːndə. </ta>
            <ta e="T575" id="Seg_384" s="T570">(Măndə am-) Măndə:" Uja deʔpiem! </ta>
            <ta e="T577" id="Seg_385" s="T575">Amoraʔ tuj! </ta>
            <ta e="T580" id="Seg_386" s="T577">Da iʔ kudonzaʔ!" </ta>
            <ta e="T581" id="Seg_387" s="T580">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T521" id="Seg_388" s="T520">onʼiʔ</ta>
            <ta e="T522" id="Seg_389" s="T521">kuza</ta>
            <ta e="T523" id="Seg_390" s="T522">kam-bi</ta>
            <ta e="T524" id="Seg_391" s="T523">dʼije-nə</ta>
            <ta e="T525" id="Seg_392" s="T524">xatʼel</ta>
            <ta e="T526" id="Seg_393" s="T525">kuʔ-sittə</ta>
            <ta e="T527" id="Seg_394" s="T526">poʔto</ta>
            <ta e="T528" id="Seg_395" s="T527">mĭl-leʔ-bi</ta>
            <ta e="T529" id="Seg_396" s="T528">mĭl-leʔ-bi</ta>
            <ta e="T530" id="Seg_397" s="T529">ej</ta>
            <ta e="T531" id="Seg_398" s="T530">kuʔ-pi</ta>
            <ta e="T532" id="Seg_399" s="T531">i</ta>
            <ta e="T533" id="Seg_400" s="T532">par-luʔ-pi</ta>
            <ta e="T534" id="Seg_401" s="T533">ma-ndə</ta>
            <ta e="T535" id="Seg_402" s="T534">ĭmbi=də</ta>
            <ta e="T536" id="Seg_403" s="T535">ej</ta>
            <ta e="T537" id="Seg_404" s="T536">deʔ-pi</ta>
            <ta e="T538" id="Seg_405" s="T537">ne-t</ta>
            <ta e="T539" id="Seg_406" s="T538">kudo-nz-laʔbə</ta>
            <ta e="T540" id="Seg_407" s="T539">ĭmbi</ta>
            <ta e="T541" id="Seg_408" s="T540">ĭmbi=də</ta>
            <ta e="T542" id="Seg_409" s="T541">ej</ta>
            <ta e="T543" id="Seg_410" s="T542">kuʔ-pia-l</ta>
            <ta e="T544" id="Seg_411" s="T543">da?</ta>
            <ta e="T545" id="Seg_412" s="T544">naga</ta>
            <ta e="T546" id="Seg_413" s="T545">ĭmbi=də</ta>
            <ta e="T547" id="Seg_414" s="T546">ej</ta>
            <ta e="T548" id="Seg_415" s="T547">kuʔ-pia-m</ta>
            <ta e="T549" id="Seg_416" s="T548">dărəʔ</ta>
            <ta e="T550" id="Seg_417" s="T549">ĭmbi=də</ta>
            <ta e="T551" id="Seg_418" s="T550">ej</ta>
            <ta e="T552" id="Seg_419" s="T551">deʔ-pie-m</ta>
            <ta e="T553" id="Seg_420" s="T552">šo-bia-m</ta>
            <ta e="T554" id="Seg_421" s="T553">maʔ-nʼi</ta>
            <ta e="T555" id="Seg_422" s="T554">kabarləj</ta>
            <ta e="T556" id="Seg_423" s="T555">dĭgəttə</ta>
            <ta e="T557" id="Seg_424" s="T556">dĭ</ta>
            <ta e="T558" id="Seg_425" s="T557">ne-t</ta>
            <ta e="T559" id="Seg_426" s="T558">sürer-bi</ta>
            <ta e="T560" id="Seg_427" s="T559">dĭ</ta>
            <ta e="T561" id="Seg_428" s="T560">bazoʔ</ta>
            <ta e="T562" id="Seg_429" s="T561">kam-bi</ta>
            <ta e="T563" id="Seg_430" s="T562">mĭm-bi</ta>
            <ta e="T564" id="Seg_431" s="T563">mĭm-bi</ta>
            <ta e="T565" id="Seg_432" s="T564">onʼiʔ</ta>
            <ta e="T566" id="Seg_433" s="T565">poʔto</ta>
            <ta e="T567" id="Seg_434" s="T566">kuʔ-pi</ta>
            <ta e="T568" id="Seg_435" s="T567">i</ta>
            <ta e="T569" id="Seg_436" s="T568">deʔ-pi</ta>
            <ta e="T570" id="Seg_437" s="T569">ma-ndə</ta>
            <ta e="T571" id="Seg_438" s="T570">măn-də</ta>
            <ta e="T573" id="Seg_439" s="T572">măn-də</ta>
            <ta e="T574" id="Seg_440" s="T573">uja</ta>
            <ta e="T575" id="Seg_441" s="T574">deʔ-pie-m</ta>
            <ta e="T576" id="Seg_442" s="T575">amor-a-ʔ</ta>
            <ta e="T577" id="Seg_443" s="T576">tuj</ta>
            <ta e="T578" id="Seg_444" s="T577">da</ta>
            <ta e="T579" id="Seg_445" s="T578">i-ʔ</ta>
            <ta e="T580" id="Seg_446" s="T579">kudo-nza-ʔ</ta>
            <ta e="T581" id="Seg_447" s="T580">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T521" id="Seg_448" s="T520">onʼiʔ</ta>
            <ta e="T522" id="Seg_449" s="T521">kuza</ta>
            <ta e="T523" id="Seg_450" s="T522">kan-bi</ta>
            <ta e="T524" id="Seg_451" s="T523">dʼije-Tə</ta>
            <ta e="T525" id="Seg_452" s="T524">xatʼel</ta>
            <ta e="T526" id="Seg_453" s="T525">kut-zittə</ta>
            <ta e="T527" id="Seg_454" s="T526">poʔto</ta>
            <ta e="T528" id="Seg_455" s="T527">mĭn-laʔbə-bi</ta>
            <ta e="T529" id="Seg_456" s="T528">mĭn-laʔbə-bi</ta>
            <ta e="T530" id="Seg_457" s="T529">ej</ta>
            <ta e="T531" id="Seg_458" s="T530">kut-bi</ta>
            <ta e="T532" id="Seg_459" s="T531">i</ta>
            <ta e="T533" id="Seg_460" s="T532">par-luʔbdə-bi</ta>
            <ta e="T534" id="Seg_461" s="T533">maʔ-gəndə</ta>
            <ta e="T535" id="Seg_462" s="T534">ĭmbi=də</ta>
            <ta e="T536" id="Seg_463" s="T535">ej</ta>
            <ta e="T537" id="Seg_464" s="T536">det-bi</ta>
            <ta e="T538" id="Seg_465" s="T537">ne-t</ta>
            <ta e="T539" id="Seg_466" s="T538">kudo-nzə-laʔbə</ta>
            <ta e="T540" id="Seg_467" s="T539">ĭmbi</ta>
            <ta e="T541" id="Seg_468" s="T540">ĭmbi=də</ta>
            <ta e="T542" id="Seg_469" s="T541">ej</ta>
            <ta e="T543" id="Seg_470" s="T542">kut-bi-l</ta>
            <ta e="T544" id="Seg_471" s="T543">ta</ta>
            <ta e="T545" id="Seg_472" s="T544">naga</ta>
            <ta e="T546" id="Seg_473" s="T545">ĭmbi=də</ta>
            <ta e="T547" id="Seg_474" s="T546">ej</ta>
            <ta e="T548" id="Seg_475" s="T547">kut-bi-m</ta>
            <ta e="T549" id="Seg_476" s="T548">dărəʔ</ta>
            <ta e="T550" id="Seg_477" s="T549">ĭmbi=də</ta>
            <ta e="T551" id="Seg_478" s="T550">ej</ta>
            <ta e="T552" id="Seg_479" s="T551">det-bi-m</ta>
            <ta e="T553" id="Seg_480" s="T552">šo-bi-m</ta>
            <ta e="T554" id="Seg_481" s="T553">maʔ-gənʼi</ta>
            <ta e="T555" id="Seg_482" s="T554">kabarləj</ta>
            <ta e="T556" id="Seg_483" s="T555">dĭgəttə</ta>
            <ta e="T557" id="Seg_484" s="T556">dĭ</ta>
            <ta e="T558" id="Seg_485" s="T557">ne-t</ta>
            <ta e="T559" id="Seg_486" s="T558">sürer-bi</ta>
            <ta e="T560" id="Seg_487" s="T559">dĭ</ta>
            <ta e="T561" id="Seg_488" s="T560">bazoʔ</ta>
            <ta e="T562" id="Seg_489" s="T561">kan-bi</ta>
            <ta e="T563" id="Seg_490" s="T562">mĭn-bi</ta>
            <ta e="T564" id="Seg_491" s="T563">mĭn-bi</ta>
            <ta e="T565" id="Seg_492" s="T564">onʼiʔ</ta>
            <ta e="T566" id="Seg_493" s="T565">poʔto</ta>
            <ta e="T567" id="Seg_494" s="T566">kut-bi</ta>
            <ta e="T568" id="Seg_495" s="T567">i</ta>
            <ta e="T569" id="Seg_496" s="T568">det-bi</ta>
            <ta e="T570" id="Seg_497" s="T569">maʔ-gəndə</ta>
            <ta e="T571" id="Seg_498" s="T570">măn-ntə</ta>
            <ta e="T573" id="Seg_499" s="T572">măn-ntə</ta>
            <ta e="T574" id="Seg_500" s="T573">uja</ta>
            <ta e="T575" id="Seg_501" s="T574">det-bi-m</ta>
            <ta e="T576" id="Seg_502" s="T575">amor-ə-ʔ</ta>
            <ta e="T577" id="Seg_503" s="T576">tüj</ta>
            <ta e="T578" id="Seg_504" s="T577">da</ta>
            <ta e="T579" id="Seg_505" s="T578">e-ʔ</ta>
            <ta e="T580" id="Seg_506" s="T579">kudo-nzə-ʔ</ta>
            <ta e="T581" id="Seg_507" s="T580">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T521" id="Seg_508" s="T520">one.[NOM.SG]</ta>
            <ta e="T522" id="Seg_509" s="T521">man.[NOM.SG]</ta>
            <ta e="T523" id="Seg_510" s="T522">go-PST.[3SG]</ta>
            <ta e="T524" id="Seg_511" s="T523">forest-LAT</ta>
            <ta e="T525" id="Seg_512" s="T524">want.PST.M.SG</ta>
            <ta e="T526" id="Seg_513" s="T525">kill-INF.LAT</ta>
            <ta e="T527" id="Seg_514" s="T526">goat.[NOM.SG]</ta>
            <ta e="T528" id="Seg_515" s="T527">go-DUR-PST.[3SG]</ta>
            <ta e="T529" id="Seg_516" s="T528">go-DUR-PST.[3SG]</ta>
            <ta e="T530" id="Seg_517" s="T529">NEG</ta>
            <ta e="T531" id="Seg_518" s="T530">kill-PST.[3SG]</ta>
            <ta e="T532" id="Seg_519" s="T531">and</ta>
            <ta e="T533" id="Seg_520" s="T532">return-MOM-PST.[3SG]</ta>
            <ta e="T534" id="Seg_521" s="T533">tent-LAT/LOC.3SG</ta>
            <ta e="T535" id="Seg_522" s="T534">what.[NOM.SG]=INDEF</ta>
            <ta e="T536" id="Seg_523" s="T535">NEG</ta>
            <ta e="T537" id="Seg_524" s="T536">bring-PST.[3SG]</ta>
            <ta e="T538" id="Seg_525" s="T537">woman-NOM/GEN.3SG</ta>
            <ta e="T539" id="Seg_526" s="T538">scold-DES-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_527" s="T539">what.[NOM.SG]</ta>
            <ta e="T541" id="Seg_528" s="T540">what.[NOM.SG]=INDEF</ta>
            <ta e="T542" id="Seg_529" s="T541">NEG</ta>
            <ta e="T543" id="Seg_530" s="T542">kill-PST-2SG</ta>
            <ta e="T544" id="Seg_531" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_532" s="T544">NEG.EX</ta>
            <ta e="T546" id="Seg_533" s="T545">what.[NOM.SG]=INDEF</ta>
            <ta e="T547" id="Seg_534" s="T546">NEG</ta>
            <ta e="T548" id="Seg_535" s="T547">kill-PST-1SG</ta>
            <ta e="T549" id="Seg_536" s="T548">so</ta>
            <ta e="T550" id="Seg_537" s="T549">what.[NOM.SG]=INDEF</ta>
            <ta e="T551" id="Seg_538" s="T550">NEG</ta>
            <ta e="T552" id="Seg_539" s="T551">bring-PST-1SG</ta>
            <ta e="T553" id="Seg_540" s="T552">come-PST-1SG</ta>
            <ta e="T554" id="Seg_541" s="T553">tent-LAT/LOC.1SG</ta>
            <ta e="T555" id="Seg_542" s="T554">enough</ta>
            <ta e="T556" id="Seg_543" s="T555">then</ta>
            <ta e="T557" id="Seg_544" s="T556">this.[NOM.SG]</ta>
            <ta e="T558" id="Seg_545" s="T557">woman-NOM/GEN.3SG</ta>
            <ta e="T559" id="Seg_546" s="T558">drive-PST.[3SG]</ta>
            <ta e="T560" id="Seg_547" s="T559">this.[NOM.SG]</ta>
            <ta e="T561" id="Seg_548" s="T560">again</ta>
            <ta e="T562" id="Seg_549" s="T561">go-PST.[3SG]</ta>
            <ta e="T563" id="Seg_550" s="T562">go-PST.[3SG]</ta>
            <ta e="T564" id="Seg_551" s="T563">go-PST.[3SG]</ta>
            <ta e="T565" id="Seg_552" s="T564">single.[NOM.SG]</ta>
            <ta e="T566" id="Seg_553" s="T565">goat.[NOM.SG]</ta>
            <ta e="T567" id="Seg_554" s="T566">kill-PST.[3SG]</ta>
            <ta e="T568" id="Seg_555" s="T567">and</ta>
            <ta e="T569" id="Seg_556" s="T568">bring-PST.[3SG]</ta>
            <ta e="T570" id="Seg_557" s="T569">tent-LAT/LOC.3SG</ta>
            <ta e="T571" id="Seg_558" s="T570">say-IPFVZ.[3SG]</ta>
            <ta e="T573" id="Seg_559" s="T572">say-IPFVZ.[3SG]</ta>
            <ta e="T574" id="Seg_560" s="T573">meat.[NOM.SG]</ta>
            <ta e="T575" id="Seg_561" s="T574">bring-PST-1SG</ta>
            <ta e="T576" id="Seg_562" s="T575">eat-EP-IMP.2SG</ta>
            <ta e="T577" id="Seg_563" s="T576">now</ta>
            <ta e="T578" id="Seg_564" s="T577">and</ta>
            <ta e="T579" id="Seg_565" s="T578">NEG.AUX-IMP.2SG</ta>
            <ta e="T580" id="Seg_566" s="T579">scold-DES-CNG</ta>
            <ta e="T581" id="Seg_567" s="T580">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T521" id="Seg_568" s="T520">один.[NOM.SG]</ta>
            <ta e="T522" id="Seg_569" s="T521">мужчина.[NOM.SG]</ta>
            <ta e="T523" id="Seg_570" s="T522">пойти-PST.[3SG]</ta>
            <ta e="T524" id="Seg_571" s="T523">лес-LAT</ta>
            <ta e="T525" id="Seg_572" s="T524">хотеть.PST.M.SG</ta>
            <ta e="T526" id="Seg_573" s="T525">убить-INF.LAT</ta>
            <ta e="T527" id="Seg_574" s="T526">коза.[NOM.SG]</ta>
            <ta e="T528" id="Seg_575" s="T527">идти-DUR-PST.[3SG]</ta>
            <ta e="T529" id="Seg_576" s="T528">идти-DUR-PST.[3SG]</ta>
            <ta e="T530" id="Seg_577" s="T529">NEG</ta>
            <ta e="T531" id="Seg_578" s="T530">убить-PST.[3SG]</ta>
            <ta e="T532" id="Seg_579" s="T531">и</ta>
            <ta e="T533" id="Seg_580" s="T532">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T534" id="Seg_581" s="T533">чум-LAT/LOC.3SG</ta>
            <ta e="T535" id="Seg_582" s="T534">что.[NOM.SG]=INDEF</ta>
            <ta e="T536" id="Seg_583" s="T535">NEG</ta>
            <ta e="T537" id="Seg_584" s="T536">принести-PST.[3SG]</ta>
            <ta e="T538" id="Seg_585" s="T537">женщина-NOM/GEN.3SG</ta>
            <ta e="T539" id="Seg_586" s="T538">ругать-DES-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_587" s="T539">что.[NOM.SG]</ta>
            <ta e="T541" id="Seg_588" s="T540">что.[NOM.SG]=INDEF</ta>
            <ta e="T542" id="Seg_589" s="T541">NEG</ta>
            <ta e="T543" id="Seg_590" s="T542">убить-PST-2SG</ta>
            <ta e="T544" id="Seg_591" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_592" s="T544">NEG.EX</ta>
            <ta e="T546" id="Seg_593" s="T545">что.[NOM.SG]=INDEF</ta>
            <ta e="T547" id="Seg_594" s="T546">NEG</ta>
            <ta e="T548" id="Seg_595" s="T547">убить-PST-1SG</ta>
            <ta e="T549" id="Seg_596" s="T548">так</ta>
            <ta e="T550" id="Seg_597" s="T549">что.[NOM.SG]=INDEF</ta>
            <ta e="T551" id="Seg_598" s="T550">NEG</ta>
            <ta e="T552" id="Seg_599" s="T551">принести-PST-1SG</ta>
            <ta e="T553" id="Seg_600" s="T552">прийти-PST-1SG</ta>
            <ta e="T554" id="Seg_601" s="T553">чум-LAT/LOC.1SG</ta>
            <ta e="T555" id="Seg_602" s="T554">хватит</ta>
            <ta e="T556" id="Seg_603" s="T555">тогда</ta>
            <ta e="T557" id="Seg_604" s="T556">этот.[NOM.SG]</ta>
            <ta e="T558" id="Seg_605" s="T557">женщина-NOM/GEN.3SG</ta>
            <ta e="T559" id="Seg_606" s="T558">гнать-PST.[3SG]</ta>
            <ta e="T560" id="Seg_607" s="T559">этот.[NOM.SG]</ta>
            <ta e="T561" id="Seg_608" s="T560">опять</ta>
            <ta e="T562" id="Seg_609" s="T561">пойти-PST.[3SG]</ta>
            <ta e="T563" id="Seg_610" s="T562">идти-PST.[3SG]</ta>
            <ta e="T564" id="Seg_611" s="T563">идти-PST.[3SG]</ta>
            <ta e="T565" id="Seg_612" s="T564">один.[NOM.SG]</ta>
            <ta e="T566" id="Seg_613" s="T565">коза.[NOM.SG]</ta>
            <ta e="T567" id="Seg_614" s="T566">убить-PST.[3SG]</ta>
            <ta e="T568" id="Seg_615" s="T567">и</ta>
            <ta e="T569" id="Seg_616" s="T568">принести-PST.[3SG]</ta>
            <ta e="T570" id="Seg_617" s="T569">чум-LAT/LOC.3SG</ta>
            <ta e="T571" id="Seg_618" s="T570">сказать-IPFVZ.[3SG]</ta>
            <ta e="T573" id="Seg_619" s="T572">сказать-IPFVZ.[3SG]</ta>
            <ta e="T574" id="Seg_620" s="T573">мясо.[NOM.SG]</ta>
            <ta e="T575" id="Seg_621" s="T574">принести-PST-1SG</ta>
            <ta e="T576" id="Seg_622" s="T575">есть-EP-IMP.2SG</ta>
            <ta e="T577" id="Seg_623" s="T576">сейчас</ta>
            <ta e="T578" id="Seg_624" s="T577">и</ta>
            <ta e="T579" id="Seg_625" s="T578">NEG.AUX-IMP.2SG</ta>
            <ta e="T580" id="Seg_626" s="T579">ругать-DES-CNG</ta>
            <ta e="T581" id="Seg_627" s="T580">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T521" id="Seg_628" s="T520">num-n:case</ta>
            <ta e="T522" id="Seg_629" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_630" s="T522">v-v:tense-v:pn</ta>
            <ta e="T524" id="Seg_631" s="T523">n-n:case</ta>
            <ta e="T525" id="Seg_632" s="T524">v</ta>
            <ta e="T526" id="Seg_633" s="T525">v-v:n.fin</ta>
            <ta e="T527" id="Seg_634" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_635" s="T527">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_636" s="T528">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_637" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_638" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_639" s="T531">conj</ta>
            <ta e="T533" id="Seg_640" s="T532">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T534" id="Seg_641" s="T533">n-n:case.poss</ta>
            <ta e="T535" id="Seg_642" s="T534">que-n:case=ptcl</ta>
            <ta e="T536" id="Seg_643" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_644" s="T536">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_645" s="T537">n-n:case.poss</ta>
            <ta e="T539" id="Seg_646" s="T538">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T540" id="Seg_647" s="T539">que-n:case</ta>
            <ta e="T541" id="Seg_648" s="T540">que-n:case=ptcl</ta>
            <ta e="T542" id="Seg_649" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_650" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_651" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_652" s="T544">v</ta>
            <ta e="T546" id="Seg_653" s="T545">que-n:case=ptcl</ta>
            <ta e="T547" id="Seg_654" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_655" s="T547">v-v:tense-v:pn</ta>
            <ta e="T549" id="Seg_656" s="T548">ptcl</ta>
            <ta e="T550" id="Seg_657" s="T549">que-n:case=ptcl</ta>
            <ta e="T551" id="Seg_658" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_659" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_660" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_661" s="T553">n-n:case.poss</ta>
            <ta e="T555" id="Seg_662" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_663" s="T555">adv</ta>
            <ta e="T557" id="Seg_664" s="T556">dempro-n:case</ta>
            <ta e="T558" id="Seg_665" s="T557">n-n:case.poss</ta>
            <ta e="T559" id="Seg_666" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_667" s="T559">dempro-n:case</ta>
            <ta e="T561" id="Seg_668" s="T560">adv</ta>
            <ta e="T562" id="Seg_669" s="T561">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_670" s="T562">v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_671" s="T563">v-v:tense-v:pn</ta>
            <ta e="T565" id="Seg_672" s="T564">adj-n:case</ta>
            <ta e="T566" id="Seg_673" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_674" s="T566">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_675" s="T567">conj</ta>
            <ta e="T569" id="Seg_676" s="T568">v-v:tense-v:pn</ta>
            <ta e="T570" id="Seg_677" s="T569">n-n:case.poss</ta>
            <ta e="T571" id="Seg_678" s="T570">v-v&gt;v-v:pn</ta>
            <ta e="T573" id="Seg_679" s="T572">v-v&gt;v-v:pn</ta>
            <ta e="T574" id="Seg_680" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_681" s="T574">v-v:tense-v:pn</ta>
            <ta e="T576" id="Seg_682" s="T575">v-v:ins-v:mood.pn</ta>
            <ta e="T577" id="Seg_683" s="T576">adv</ta>
            <ta e="T578" id="Seg_684" s="T577">conj</ta>
            <ta e="T579" id="Seg_685" s="T578">aux-v:mood.pn</ta>
            <ta e="T580" id="Seg_686" s="T579">v-v&gt;v-v:n.fin</ta>
            <ta e="T581" id="Seg_687" s="T580">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T521" id="Seg_688" s="T520">num</ta>
            <ta e="T522" id="Seg_689" s="T521">n</ta>
            <ta e="T523" id="Seg_690" s="T522">v</ta>
            <ta e="T524" id="Seg_691" s="T523">n</ta>
            <ta e="T525" id="Seg_692" s="T524">v</ta>
            <ta e="T526" id="Seg_693" s="T525">v</ta>
            <ta e="T527" id="Seg_694" s="T526">n</ta>
            <ta e="T528" id="Seg_695" s="T527">v</ta>
            <ta e="T529" id="Seg_696" s="T528">v</ta>
            <ta e="T530" id="Seg_697" s="T529">ptcl</ta>
            <ta e="T531" id="Seg_698" s="T530">v</ta>
            <ta e="T532" id="Seg_699" s="T531">conj</ta>
            <ta e="T533" id="Seg_700" s="T532">v</ta>
            <ta e="T534" id="Seg_701" s="T533">n</ta>
            <ta e="T535" id="Seg_702" s="T534">que</ta>
            <ta e="T536" id="Seg_703" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_704" s="T536">v</ta>
            <ta e="T538" id="Seg_705" s="T537">n</ta>
            <ta e="T539" id="Seg_706" s="T538">v</ta>
            <ta e="T540" id="Seg_707" s="T539">que</ta>
            <ta e="T541" id="Seg_708" s="T540">que</ta>
            <ta e="T542" id="Seg_709" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_710" s="T542">v</ta>
            <ta e="T544" id="Seg_711" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_712" s="T544">v</ta>
            <ta e="T546" id="Seg_713" s="T545">que</ta>
            <ta e="T547" id="Seg_714" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_715" s="T547">v</ta>
            <ta e="T549" id="Seg_716" s="T548">ptcl</ta>
            <ta e="T550" id="Seg_717" s="T549">que</ta>
            <ta e="T551" id="Seg_718" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_719" s="T551">v</ta>
            <ta e="T553" id="Seg_720" s="T552">v</ta>
            <ta e="T554" id="Seg_721" s="T553">n</ta>
            <ta e="T555" id="Seg_722" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_723" s="T555">adv</ta>
            <ta e="T557" id="Seg_724" s="T556">dempro</ta>
            <ta e="T558" id="Seg_725" s="T557">n</ta>
            <ta e="T559" id="Seg_726" s="T558">v</ta>
            <ta e="T560" id="Seg_727" s="T559">dempro</ta>
            <ta e="T561" id="Seg_728" s="T560">adv</ta>
            <ta e="T562" id="Seg_729" s="T561">v</ta>
            <ta e="T563" id="Seg_730" s="T562">v</ta>
            <ta e="T564" id="Seg_731" s="T563">v</ta>
            <ta e="T565" id="Seg_732" s="T564">adj</ta>
            <ta e="T566" id="Seg_733" s="T565">n</ta>
            <ta e="T567" id="Seg_734" s="T566">v</ta>
            <ta e="T568" id="Seg_735" s="T567">conj</ta>
            <ta e="T569" id="Seg_736" s="T568">v</ta>
            <ta e="T570" id="Seg_737" s="T569">n</ta>
            <ta e="T571" id="Seg_738" s="T570">v</ta>
            <ta e="T573" id="Seg_739" s="T572">v</ta>
            <ta e="T574" id="Seg_740" s="T573">n</ta>
            <ta e="T575" id="Seg_741" s="T574">v</ta>
            <ta e="T576" id="Seg_742" s="T575">v</ta>
            <ta e="T577" id="Seg_743" s="T576">adv</ta>
            <ta e="T578" id="Seg_744" s="T577">conj</ta>
            <ta e="T579" id="Seg_745" s="T578">aux</ta>
            <ta e="T580" id="Seg_746" s="T579">v</ta>
            <ta e="T581" id="Seg_747" s="T580">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T522" id="Seg_748" s="T521">np.h:S</ta>
            <ta e="T523" id="Seg_749" s="T522">v:pred</ta>
            <ta e="T525" id="Seg_750" s="T524">0.3.h:S v:pred</ta>
            <ta e="T527" id="Seg_751" s="T525">s:compl</ta>
            <ta e="T528" id="Seg_752" s="T527">0.3.h:S v:pred</ta>
            <ta e="T529" id="Seg_753" s="T528">0.3.h:S v:pred</ta>
            <ta e="T531" id="Seg_754" s="T530">0.3.h:S v:pred</ta>
            <ta e="T533" id="Seg_755" s="T532">0.3.h:S v:pred</ta>
            <ta e="T535" id="Seg_756" s="T534">pro:O</ta>
            <ta e="T537" id="Seg_757" s="T536">0.3.h:S v:pred</ta>
            <ta e="T538" id="Seg_758" s="T537">np.h:S</ta>
            <ta e="T539" id="Seg_759" s="T538">v:pred</ta>
            <ta e="T541" id="Seg_760" s="T540">pro:O</ta>
            <ta e="T543" id="Seg_761" s="T542">0.3.h:S v:pred</ta>
            <ta e="T546" id="Seg_762" s="T545">pro:O</ta>
            <ta e="T548" id="Seg_763" s="T547">0.1.h:S v:pred</ta>
            <ta e="T550" id="Seg_764" s="T549">pro:O</ta>
            <ta e="T552" id="Seg_765" s="T551">0.1.h:S v:pred</ta>
            <ta e="T553" id="Seg_766" s="T552">0.1.h:S v:pred</ta>
            <ta e="T558" id="Seg_767" s="T557">np.h:S</ta>
            <ta e="T559" id="Seg_768" s="T558">v:pred</ta>
            <ta e="T560" id="Seg_769" s="T559">pro.h:S</ta>
            <ta e="T562" id="Seg_770" s="T561">v:pred</ta>
            <ta e="T563" id="Seg_771" s="T562">0.3.h:S v:pred</ta>
            <ta e="T564" id="Seg_772" s="T563">0.3.h:S v:pred</ta>
            <ta e="T566" id="Seg_773" s="T565">np:O</ta>
            <ta e="T567" id="Seg_774" s="T566">0.3.h:S v:pred</ta>
            <ta e="T569" id="Seg_775" s="T568">0.3.h:S v:pred</ta>
            <ta e="T571" id="Seg_776" s="T570">0.3.h:S v:pred</ta>
            <ta e="T573" id="Seg_777" s="T572">0.3.h:S v:pred</ta>
            <ta e="T574" id="Seg_778" s="T573">np:O</ta>
            <ta e="T575" id="Seg_779" s="T574">0.1.h:S v:pred</ta>
            <ta e="T576" id="Seg_780" s="T575">0.2.h:S v:pred</ta>
            <ta e="T579" id="Seg_781" s="T578">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T522" id="Seg_782" s="T521">np.h:A</ta>
            <ta e="T524" id="Seg_783" s="T523">np:G</ta>
            <ta e="T525" id="Seg_784" s="T524">0.3.h:E</ta>
            <ta e="T527" id="Seg_785" s="T526">np:P</ta>
            <ta e="T528" id="Seg_786" s="T527">0.3.h:A</ta>
            <ta e="T529" id="Seg_787" s="T528">0.3.h:A</ta>
            <ta e="T531" id="Seg_788" s="T530">0.3.h:A</ta>
            <ta e="T533" id="Seg_789" s="T532">0.3.h:A</ta>
            <ta e="T534" id="Seg_790" s="T533">np:G</ta>
            <ta e="T535" id="Seg_791" s="T534">pro:Th</ta>
            <ta e="T537" id="Seg_792" s="T536">0.3.h:A</ta>
            <ta e="T538" id="Seg_793" s="T537">np.h:A 0.3.h:Poss</ta>
            <ta e="T541" id="Seg_794" s="T540">pro:P</ta>
            <ta e="T543" id="Seg_795" s="T542">0.3.h:A</ta>
            <ta e="T546" id="Seg_796" s="T545">pro:P</ta>
            <ta e="T548" id="Seg_797" s="T547">0.1.h:A</ta>
            <ta e="T550" id="Seg_798" s="T549">pro:P</ta>
            <ta e="T552" id="Seg_799" s="T551">0.1.h:A</ta>
            <ta e="T553" id="Seg_800" s="T552">0.1.h:A</ta>
            <ta e="T554" id="Seg_801" s="T553">np:G</ta>
            <ta e="T558" id="Seg_802" s="T557">np.h:A</ta>
            <ta e="T560" id="Seg_803" s="T559">pro.h:A</ta>
            <ta e="T563" id="Seg_804" s="T562">0.3.h:A</ta>
            <ta e="T564" id="Seg_805" s="T563">0.3.h:A</ta>
            <ta e="T566" id="Seg_806" s="T565">np:P</ta>
            <ta e="T567" id="Seg_807" s="T566">0.3.h:A</ta>
            <ta e="T569" id="Seg_808" s="T568">0.3.h:A</ta>
            <ta e="T570" id="Seg_809" s="T569">np:G</ta>
            <ta e="T571" id="Seg_810" s="T570">0.3.h:A</ta>
            <ta e="T573" id="Seg_811" s="T572">0.3.h:A</ta>
            <ta e="T574" id="Seg_812" s="T573">np:Th</ta>
            <ta e="T575" id="Seg_813" s="T574">np:Th</ta>
            <ta e="T576" id="Seg_814" s="T575">0.2.h:A</ta>
            <ta e="T579" id="Seg_815" s="T578">0.2.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T525" id="Seg_816" s="T524">RUS:mod</ta>
            <ta e="T532" id="Seg_817" s="T531">RUS:gram</ta>
            <ta e="T535" id="Seg_818" s="T534">TURK:gram(INDEF)</ta>
            <ta e="T541" id="Seg_819" s="T540">TURK:gram(INDEF)</ta>
            <ta e="T544" id="Seg_820" s="T543">RUS:disc</ta>
            <ta e="T546" id="Seg_821" s="T545">TURK:gram(INDEF)</ta>
            <ta e="T550" id="Seg_822" s="T549">TURK:gram(INDEF)</ta>
            <ta e="T568" id="Seg_823" s="T567">RUS:gram</ta>
            <ta e="T578" id="Seg_824" s="T577">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T525" id="Seg_825" s="T524">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T524" id="Seg_826" s="T520">Один человек пошёл в тайгу.</ta>
            <ta e="T527" id="Seg_827" s="T524">Он хотел козу убить.</ta>
            <ta e="T531" id="Seg_828" s="T527">Ходил, ходил, не убил.</ta>
            <ta e="T537" id="Seg_829" s="T531">И вернулся домой, ничего не принёс.</ta>
            <ta e="T544" id="Seg_830" s="T537">Его жена ругается: «Почему ты никого не убил-то?»</ta>
            <ta e="T554" id="Seg_831" s="T544">«Нет, никого не убил, ничего не принёс, пришёл домой».</ta>
            <ta e="T555" id="Seg_832" s="T554">Хватит!</ta>
            <ta e="T562" id="Seg_833" s="T555">Потом жена [его] отправила, он опять пошёл.</ta>
            <ta e="T570" id="Seg_834" s="T562">Шёл, шёл, убил козу и принёс домой.</ta>
            <ta e="T575" id="Seg_835" s="T570">Говорит: "Я принёс мясо!</ta>
            <ta e="T577" id="Seg_836" s="T575">Ешь теперь!</ta>
            <ta e="T580" id="Seg_837" s="T577">И не ругайся!"</ta>
            <ta e="T581" id="Seg_838" s="T580">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T524" id="Seg_839" s="T520">One man went to the taiga.</ta>
            <ta e="T527" id="Seg_840" s="T524">He wanted to kill a goat.</ta>
            <ta e="T531" id="Seg_841" s="T527">He went, he went, he did not kill.</ta>
            <ta e="T537" id="Seg_842" s="T531">And he came home, he did not bring anything.</ta>
            <ta e="T544" id="Seg_843" s="T537">His wife is scolding: "Why didn't you kill anything?"</ta>
            <ta e="T554" id="Seg_844" s="T544">There is nothing, I did not kill anything, so I didn't bring anything, I came home.</ta>
            <ta e="T555" id="Seg_845" s="T554">Enough!</ta>
            <ta e="T562" id="Seg_846" s="T555">Then the woman sent [him] away, he went again.</ta>
            <ta e="T570" id="Seg_847" s="T562">He went, he went, he killed a goat and brought [it] home.</ta>
            <ta e="T575" id="Seg_848" s="T570">He says: "I brought meat!</ta>
            <ta e="T577" id="Seg_849" s="T575">Eat now!</ta>
            <ta e="T580" id="Seg_850" s="T577">And don't scold!"</ta>
            <ta e="T581" id="Seg_851" s="T580">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T524" id="Seg_852" s="T520">Ein Mann ging in die Taiga.</ta>
            <ta e="T527" id="Seg_853" s="T524">Er wollte eine Ziege töten.</ta>
            <ta e="T531" id="Seg_854" s="T527">Er ging, er ging, er tötete nicht.</ta>
            <ta e="T537" id="Seg_855" s="T531">Und er kam nach Hause, er brachte nichts.</ta>
            <ta e="T544" id="Seg_856" s="T537">Seine Frau schimpft: „Warum hast du nichts getötet?“</ta>
            <ta e="T554" id="Seg_857" s="T544">Es gibt nichts, I habe nichts getötet, also habe ich nichts gebracht, ich bin nach Hause gekommen.</ta>
            <ta e="T555" id="Seg_858" s="T554">Genug!</ta>
            <ta e="T562" id="Seg_859" s="T555">Dann schickte [ihn] die Frau wieder fort, er ging wieder.</ta>
            <ta e="T570" id="Seg_860" s="T562">Er ging, er ging, er tötete eine Ziege und brachte [sie] nach Hause.</ta>
            <ta e="T575" id="Seg_861" s="T570">Er sagt: „Ich habe Fleisch gebracht!</ta>
            <ta e="T577" id="Seg_862" s="T575">Ißt nun!</ta>
            <ta e="T580" id="Seg_863" s="T577">Und schimpf nicht!”</ta>
            <ta e="T581" id="Seg_864" s="T580">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T544" id="Seg_865" s="T537">[KlT:] Sentence-final "to" (Rus.) '~then'.</ta>
            <ta e="T562" id="Seg_866" s="T555">[GVY:] nen</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
