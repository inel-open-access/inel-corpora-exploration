<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDED625B3B-58FF-461A-6F4A-271C4AD9229D">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Ducks_flk.wav" />
         <referenced-file url="PKZ_196X_Ducks_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Ducks_flk\PKZ_196X_Ducks_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">560</ud-information>
            <ud-information attribute-name="# HIAT:w">341</ud-information>
            <ud-information attribute-name="# e">341</ud-information>
            <ud-information attribute-name="# HIAT:u">86</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T227" time="0.0" type="appl" />
         <tli id="T228" time="0.925" type="appl" />
         <tli id="T229" time="1.851" type="appl" />
         <tli id="T230" time="3.153294104604873" />
         <tli id="T231" time="3.701" type="appl" />
         <tli id="T232" time="4.133" type="appl" />
         <tli id="T233" time="4.566" type="appl" />
         <tli id="T234" time="4.998" type="appl" />
         <tli id="T235" time="5.431" type="appl" />
         <tli id="T236" time="5.863" type="appl" />
         <tli id="T237" time="6.986579749737647" />
         <tli id="T238" time="8.069" type="appl" />
         <tli id="T239" time="9.062" type="appl" />
         <tli id="T240" time="11.193194083787697" />
         <tli id="T241" time="12.002" type="appl" />
         <tli id="T242" time="14.08649142289661" />
         <tli id="T243" time="15.2" type="appl" />
         <tli id="T244" time="15.98" type="appl" />
         <tli id="T245" time="16.759" type="appl" />
         <tli id="T246" time="17.538" type="appl" />
         <tli id="T247" time="18.317" type="appl" />
         <tli id="T248" time="19.097" type="appl" />
         <tli id="T249" time="20.159749201533057" />
         <tli id="T250" time="20.967" type="appl" />
         <tli id="T251" time="21.654" type="appl" />
         <tli id="T252" time="22.34" type="appl" />
         <tli id="T253" time="23.027" type="appl" />
         <tli id="T254" time="23.89970267443651" />
         <tli id="T255" time="25.132" type="appl" />
         <tli id="T256" time="26.55" type="appl" />
         <tli id="T257" time="27.272" type="appl" />
         <tli id="T258" time="27.994" type="appl" />
         <tli id="T259" time="28.716" type="appl" />
         <tli id="T260" time="29.659631017731666" />
         <tli id="T261" time="30.502" type="appl" />
         <tli id="T262" time="31.403" type="appl" />
         <tli id="T263" time="32.304" type="appl" />
         <tli id="T264" time="32.942" type="appl" />
         <tli id="T265" time="33.579" type="appl" />
         <tli id="T266" time="34.217" type="appl" />
         <tli id="T267" time="34.854" type="appl" />
         <tli id="T268" time="35.492" type="appl" />
         <tli id="T269" time="36.129" type="appl" />
         <tli id="T270" time="37.052872375039925" />
         <tli id="T271" time="38.19" type="appl" />
         <tli id="T272" time="39.238" type="appl" />
         <tli id="T273" time="40.287" type="appl" />
         <tli id="T274" time="41.952811417079204" />
         <tli id="T275" time="42.659" type="appl" />
         <tli id="T276" time="43.241" type="appl" />
         <tli id="T277" time="43.823" type="appl" />
         <tli id="T278" time="45.34610253598805" />
         <tli id="T279" time="46.332" type="appl" />
         <tli id="T280" time="47.244" type="appl" />
         <tli id="T281" time="48.156" type="appl" />
         <tli id="T282" time="49.9593784776087" />
         <tli id="T283" time="51.476" type="appl" />
         <tli id="T284" time="52.637" type="appl" />
         <tli id="T285" time="54.65932000772802" />
         <tli id="T286" time="55.644" type="appl" />
         <tli id="T287" time="57.56595051429826" />
         <tli id="T288" time="58.686" type="appl" />
         <tli id="T289" time="59.524" type="appl" />
         <tli id="T290" time="60.361" type="appl" />
         <tli id="T291" time="61.199" type="appl" />
         <tli id="T292" time="62.036" type="appl" />
         <tli id="T293" time="62.874" type="appl" />
         <tli id="T294" time="63.711" type="appl" />
         <tli id="T295" time="64.488" type="appl" />
         <tli id="T296" time="65.264" type="appl" />
         <tli id="T297" time="66.176" type="appl" />
         <tli id="T298" time="67.005" type="appl" />
         <tli id="T299" time="67.746" type="appl" />
         <tli id="T300" time="68.488" type="appl" />
         <tli id="T301" time="68.992" type="appl" />
         <tli id="T302" time="69.492" type="appl" />
         <tli id="T303" time="69.992" type="appl" />
         <tli id="T304" time="70.492" type="appl" />
         <tli id="T305" time="70.992" type="appl" />
         <tli id="T306" time="71.492" type="appl" />
         <tli id="T307" time="71.992" type="appl" />
         <tli id="T308" time="73.67241680758657" />
         <tli id="T309" time="74.964" type="appl" />
         <tli id="T310" time="79.29901347626841" />
         <tli id="T311" time="80.106" type="appl" />
         <tli id="T312" time="80.744" type="appl" />
         <tli id="T313" time="81.382" type="appl" />
         <tli id="T314" time="82.02" type="appl" />
         <tli id="T315" time="82.658" type="appl" />
         <tli id="T316" time="83.24266884209062" />
         <tli id="T317" time="84.046" type="appl" />
         <tli id="T318" time="84.797" type="appl" />
         <tli id="T319" time="85.547" type="appl" />
         <tli id="T320" time="86.163" type="appl" />
         <tli id="T321" time="86.78" type="appl" />
         <tli id="T322" time="87.396" type="appl" />
         <tli id="T323" time="89.19222373257631" />
         <tli id="T324" time="90.379" type="appl" />
         <tli id="T325" time="91.413" type="appl" />
         <tli id="T326" time="92.447" type="appl" />
         <tli id="T327" time="94.3054934539969" />
         <tli id="T328" time="94.888" />
         <tli id="T329" time="95.75880870728201" />
         <tli id="T330" time="95.9921391378553" />
         <tli id="T331" time="96.7921291855352" />
         <tli id="T332" time="96.96546036253251" />
         <tli id="T333" time="98.01878059197769" />
         <tli id="T334" time="98.405" type="appl" />
         <tli id="T335" time="98.919" type="appl" />
         <tli id="T336" time="99.432" type="appl" />
         <tli id="T337" time="99.946" type="appl" />
         <tli id="T338" time="100.459" type="appl" />
         <tli id="T339" time="101.488" type="appl" />
         <tli id="T340" time="103.57204483962222" />
         <tli id="T341" time="105.274" type="appl" />
         <tli id="T342" time="106.689" type="appl" />
         <tli id="T343" time="108.058" type="appl" />
         <tli id="T344" time="108.837" type="appl" />
         <tli id="T345" time="109.615" type="appl" />
         <tli id="T346" time="110.394" type="appl" />
         <tli id="T347" time="112.01860642637564" />
         <tli id="T348" time="112.955" type="appl" />
         <tli id="T349" time="113.585" type="appl" />
         <tli id="T350" time="114.27399503475647" />
         <tli id="T351" time="114.774" type="appl" />
         <tli id="T352" time="115.333" type="appl" />
         <tli id="T353" time="115.893" type="appl" />
         <tli id="T354" time="116.453" type="appl" />
         <tli id="T355" time="117.012" type="appl" />
         <tli id="T356" time="117.572" type="appl" />
         <tli id="T357" time="118.132" type="appl" />
         <tli id="T358" time="118.691" type="appl" />
         <tli id="T359" time="119.251" type="appl" />
         <tli id="T360" time="119.803" type="appl" />
         <tli id="T361" time="120.202" type="appl" />
         <tli id="T362" time="120.602" type="appl" />
         <tli id="T363" time="121.001" type="appl" />
         <tli id="T364" time="121.401" type="appl" />
         <tli id="T365" time="122.502" type="appl" />
         <tli id="T366" time="123.557" type="appl" />
         <tli id="T367" time="124.611" type="appl" />
         <tli id="T368" time="125.666" type="appl" />
         <tli id="T369" time="126.876" type="appl" />
         <tli id="T370" time="128.45840190619725" />
         <tli id="T371" time="129.242" type="appl" />
         <tli id="T372" time="129.933" type="appl" />
         <tli id="T373" time="131.0517029774262" />
         <tli id="T374" time="131.742" type="appl" />
         <tli id="T375" time="132.444" type="appl" />
         <tli id="T376" time="134.40499459395105" />
         <tli id="T377" time="136.5116350528414" />
         <tli id="T378" time="137.075" type="appl" />
         <tli id="T379" time="137.464" type="appl" />
         <tli id="T380" time="137.852" type="appl" />
         <tli id="T381" time="138.36494532996647" />
         <tli id="T382" time="138.864" type="appl" />
         <tli id="T383" time="139.487" type="appl" />
         <tli id="T384" time="140.11" type="appl" />
         <tli id="T385" time="140.767337317877" />
         <tli id="T386" time="141.372" type="appl" />
         <tli id="T387" time="142.011" type="appl" />
         <tli id="T388" time="142.649" type="appl" />
         <tli id="T389" time="143.288" type="appl" />
         <tli id="T390" time="143.926" type="appl" />
         <tli id="T391" time="144.839" type="appl" />
         <tli id="T392" time="145.53566341392104" />
         <tli id="T393" time="146.706" type="appl" />
         <tli id="T394" time="147.823" type="appl" />
         <tli id="T395" time="148.94" type="appl" />
         <tli id="T396" time="150.057" type="appl" />
         <tli id="T397" time="151.015" type="appl" />
         <tli id="T398" time="151.973" type="appl" />
         <tli id="T399" time="153.8647525037642" />
         <tli id="T400" time="154.693" type="appl" />
         <tli id="T401" time="155.442" type="appl" />
         <tli id="T402" time="156.192" type="appl" />
         <tli id="T403" time="157.3247094599797" />
         <tli id="T404" time="158.303" type="appl" />
         <tli id="T405" time="159.354" type="appl" />
         <tli id="T406" time="160.404" type="appl" />
         <tli id="T407" time="161.455" type="appl" />
         <tli id="T408" time="162.662" type="appl" />
         <tli id="T409" time="163.869" type="appl" />
         <tli id="T410" time="165.075" type="appl" />
         <tli id="T411" time="166.282" type="appl" />
         <tli id="T412" time="167.489" type="appl" />
         <tli id="T413" time="168.109" type="appl" />
         <tli id="T414" time="168.73" type="appl" />
         <tli id="T415" time="169.35" type="appl" />
         <tli id="T416" time="169.97" type="appl" />
         <tli id="T417" time="170.591" type="appl" />
         <tli id="T418" time="171.211" type="appl" />
         <tli id="T419" time="172.406" type="appl" />
         <tli id="T420" time="173.6" type="appl" />
         <tli id="T421" time="174.794" type="appl" />
         <tli id="T422" time="176.70446836502487" />
         <tli id="T423" time="177.428" type="appl" />
         <tli id="T424" time="178.256" type="appl" />
         <tli id="T425" time="179.085" type="appl" />
         <tli id="T426" time="179.913" type="appl" />
         <tli id="T427" time="180.7353296772067" />
         <tli id="T428" time="181.183" type="appl" />
         <tli id="T429" time="181.625" type="appl" />
         <tli id="T430" time="182.067" type="appl" />
         <tli id="T431" time="184.25104114813843" />
         <tli id="T432" time="185.152" type="appl" />
         <tli id="T433" time="185.8" type="appl" />
         <tli id="T434" time="186.448" type="appl" />
         <tli id="T435" time="187.41766842020462" />
         <tli id="T436" time="188.36" type="appl" />
         <tli id="T437" time="189.221" type="appl" />
         <tli id="T438" time="190.082" type="appl" />
         <tli id="T439" time="190.646" type="appl" />
         <tli id="T440" time="191.21" type="appl" />
         <tli id="T441" time="191.775" type="appl" />
         <tli id="T442" time="192.339" type="appl" />
         <tli id="T443" time="192.903" type="appl" />
         <tli id="T444" time="193.493" type="appl" />
         <tli id="T445" time="194.083" type="appl" />
         <tli id="T446" time="194.7709102751289" />
         <tli id="T447" time="195.502" type="appl" />
         <tli id="T448" time="196.33" type="appl" />
         <tli id="T449" time="196.955" type="appl" />
         <tli id="T450" time="197.581" type="appl" />
         <tli id="T451" time="198.49086399684035" />
         <tli id="T452" time="199.027" type="appl" />
         <tli id="T453" time="199.673" type="appl" />
         <tli id="T454" time="200.32" type="appl" />
         <tli id="T455" time="200.966" type="appl" />
         <tli id="T456" time="201.613" type="appl" />
         <tli id="T457" time="202.26" type="appl" />
         <tli id="T458" time="202.906" type="appl" />
         <tli id="T459" time="205.2641130671967" />
         <tli id="T460" time="207.9640794781163" />
         <tli id="T461" time="208.596" type="appl" />
         <tli id="T462" time="209.088" type="appl" />
         <tli id="T463" time="209.581" type="appl" />
         <tli id="T464" time="210.074" type="appl" />
         <tli id="T465" time="210.567" type="appl" />
         <tli id="T466" time="211.059" type="appl" />
         <tli id="T467" time="211.552" type="appl" />
         <tli id="T468" time="212.045" type="appl" />
         <tli id="T469" time="212.537" type="appl" />
         <tli id="T470" time="213.39734521860885" />
         <tli id="T471" time="214.119" type="appl" />
         <tli id="T472" time="214.878" type="appl" />
         <tli id="T473" time="215.78398219418716" />
         <tli id="T474" time="216.394" type="appl" />
         <tli id="T475" time="217.152" type="appl" />
         <tli id="T476" time="217.909" type="appl" />
         <tli id="T477" time="218.667" type="appl" />
         <tli id="T478" time="219.172" type="appl" />
         <tli id="T479" time="219.677" type="appl" />
         <tli id="T480" time="220.182" type="appl" />
         <tli id="T481" time="220.687" type="appl" />
         <tli id="T482" time="221.61724295851963" />
         <tli id="T483" time="222.464" type="appl" />
         <tli id="T484" time="224.01054651782863" />
         <tli id="T485" time="224.646" type="appl" />
         <tli id="T486" time="225.176" type="appl" />
         <tli id="T487" time="225.706" type="appl" />
         <tli id="T488" time="226.235" type="appl" />
         <tli id="T489" time="226.71" type="appl" />
         <tli id="T490" time="227.185" type="appl" />
         <tli id="T491" time="227.66" type="appl" />
         <tli id="T492" time="228.135" type="appl" />
         <tli id="T493" time="228.61" type="appl" />
         <tli id="T494" time="229.085" type="appl" />
         <tli id="T495" time="229.695" type="appl" />
         <tli id="T496" time="230.305" type="appl" />
         <tli id="T497" time="230.916" type="appl" />
         <tli id="T498" time="231.526" type="appl" />
         <tli id="T499" time="232.3304430136994" />
         <tli id="T500" time="233.386" type="appl" />
         <tli id="T501" time="234.322" type="appl" />
         <tli id="T502" time="235.259" type="appl" />
         <tli id="T503" time="235.764" type="appl" />
         <tli id="T504" time="236.268" type="appl" />
         <tli id="T505" time="236.8237204481681" />
         <tli id="T506" time="237.648" type="appl" />
         <tli id="T507" time="239.84368287815965" />
         <tli id="T508" time="241.008" type="appl" />
         <tli id="T509" time="242.024" type="appl" />
         <tli id="T510" time="243.039" type="appl" />
         <tli id="T511" time="243.736" type="appl" />
         <tli id="T512" time="244.432" type="appl" />
         <tli id="T513" time="245.129" type="appl" />
         <tli id="T514" time="246.322" type="appl" />
         <tli id="T515" time="247.6235860918465" />
         <tli id="T516" time="248.17" type="appl" />
         <tli id="T517" time="248.825" type="appl" />
         <tli id="T518" time="249.481" type="appl" />
         <tli id="T519" time="250.137" type="appl" />
         <tli id="T520" time="250.793" type="appl" />
         <tli id="T521" time="251.448" type="appl" />
         <tli id="T522" time="252.83685456922709" />
         <tli id="T523" time="253.676" type="appl" />
         <tli id="T524" time="254.514" type="appl" />
         <tli id="T525" time="255.352" type="appl" />
         <tli id="T526" time="256.19" type="appl" />
         <tli id="T527" time="256.903" type="appl" />
         <tli id="T528" time="257.617" type="appl" />
         <tli id="T529" time="258.4701178216396" />
         <tli id="T530" time="259.259" type="appl" />
         <tli id="T531" time="260.1834298404207" />
         <tli id="T532" time="260.78" type="appl" />
         <tli id="T533" time="261.56" type="appl" />
         <tli id="T534" time="262.576" type="appl" />
         <tli id="T535" time="263.592" type="appl" />
         <tli id="T536" time="264.608" type="appl" />
         <tli id="T537" time="265.624" type="appl" />
         <tli id="T538" time="266.117" type="appl" />
         <tli id="T539" time="266.61" type="appl" />
         <tli id="T540" time="267.153" type="appl" />
         <tli id="T541" time="267.697" type="appl" />
         <tli id="T542" time="268.24" type="appl" />
         <tli id="T543" time="268.69" type="appl" />
         <tli id="T544" time="269.14" type="appl" />
         <tli id="T545" time="269.59" type="appl" />
         <tli id="T546" time="270.04" type="appl" />
         <tli id="T547" time="270.49" type="appl" />
         <tli id="T548" time="272.0899483833896" />
         <tli id="T549" time="272.96" type="appl" />
         <tli id="T550" time="273.766" type="appl" />
         <tli id="T551" time="274.573" type="appl" />
         <tli id="T552" time="275.379" type="appl" />
         <tli id="T553" time="276.8098896647009" />
         <tli id="T554" time="278.102" type="appl" />
         <tli id="T555" time="279.626" type="appl" />
         <tli id="T556" time="280.239" type="appl" />
         <tli id="T557" time="280.852" type="appl" />
         <tli id="T558" time="281.465" type="appl" />
         <tli id="T559" time="282.078" type="appl" />
         <tli id="T560" time="283.2431429647933" />
         <tli id="T561" time="284.626" type="appl" />
         <tli id="T562" time="285.195" type="appl" />
         <tli id="T563" time="285.764" type="appl" />
         <tli id="T564" time="286.333" type="appl" />
         <tli id="T565" time="286.902" type="appl" />
         <tli id="T566" time="288.34" type="appl" />
         <tli id="T567" time="291.00304642728815" />
         <tli id="T568" time="292.223" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T568" id="Seg_0" n="sc" s="T227">
               <ts e="T230" id="Seg_2" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_4" n="HIAT:w" s="T227">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_7" n="HIAT:w" s="T228">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_10" n="HIAT:w" s="T229">büzʼezʼiʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_14" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_16" n="HIAT:w" s="T230">Dĭzeŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_20" n="HIAT:w" s="T231">bɨl-</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_23" n="HIAT:w" s="T232">ko-</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_27" n="HIAT:w" s="T233">ibi</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_30" n="HIAT:w" s="T234">koʔbdo</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_33" n="HIAT:w" s="T235">i</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_36" n="HIAT:w" s="T236">nʼi</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_40" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_42" n="HIAT:w" s="T237">Dĭzeŋ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_45" n="HIAT:w" s="T238">togonorzittə</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_48" n="HIAT:w" s="T239">kallaʔbəʔjə</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_52" n="HIAT:u" s="T240">
                  <nts id="Seg_53" n="HIAT:ip">"</nts>
                  <ts e="T241" id="Seg_55" n="HIAT:w" s="T240">Măndot</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_58" n="HIAT:w" s="T241">nʼim</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_62" n="HIAT:u" s="T242">
                  <nts id="Seg_63" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_65" n="HIAT:w" s="T242">M-</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_68" n="HIAT:w" s="T243">mi-</ts>
                  <nts id="Seg_69" n="HIAT:ip">)</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_72" n="HIAT:w" s="T244">Măn</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_75" n="HIAT:w" s="T245">tănan</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_78" n="HIAT:w" s="T246">ilem</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_81" n="HIAT:w" s="T247">plat</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_85" n="HIAT:w" s="T248">kuvas</ts>
                  <nts id="Seg_86" n="HIAT:ip">"</nts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_90" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_92" n="HIAT:w" s="T249">Kambiʔi</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_96" n="HIAT:w" s="T250">a</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_99" n="HIAT:w" s="T251">koʔbdot</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_102" n="HIAT:w" s="T252">nʼibə</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_105" n="HIAT:w" s="T253">ibi</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_109" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_111" n="HIAT:w" s="T254">Nʼiʔnenə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_114" n="HIAT:w" s="T255">amnolbi</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_118" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_120" n="HIAT:w" s="T256">Noʔgən</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_124" n="HIAT:w" s="T257">a</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_127" n="HIAT:w" s="T258">bostə</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_130" n="HIAT:w" s="T259">nuʔməluʔpi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_134" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_136" n="HIAT:w" s="T260">Dön</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_139" n="HIAT:w" s="T261">nabəʔi</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_142" n="HIAT:w" s="T262">nanʼergöluʔpi</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_146" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_148" n="HIAT:w" s="T263">Dĭ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_151" n="HIAT:w" s="T264">nʼim</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_153" n="HIAT:ip">(</nts>
                  <ts e="T266" id="Seg_155" n="HIAT:w" s="T265">i-</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_158" n="HIAT:w" s="T266">i-</ts>
                  <nts id="Seg_159" n="HIAT:ip">)</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_162" n="HIAT:w" s="T267">iluʔpiʔi</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <ts e="T269" id="Seg_167" n="HIAT:w" s="T268">kunnaːlbiʔi</ts>
                  <nts id="Seg_168" n="HIAT:ip">)</nts>
                  <nts id="Seg_169" n="HIAT:ip">,</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_172" n="HIAT:w" s="T269">kumbiʔi</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_176" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_178" n="HIAT:w" s="T270">Koʔbdo</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_180" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_182" n="HIAT:w" s="T271">suʔmileʔ-</ts>
                  <nts id="Seg_183" n="HIAT:ip">)</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_186" n="HIAT:w" s="T272">nuʔmileʔ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_189" n="HIAT:w" s="T273">šobi</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_193" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_195" n="HIAT:w" s="T274">Kuliot:</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_198" n="HIAT:w" s="T275">naga</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_202" n="HIAT:w" s="T276">naga</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_205" n="HIAT:w" s="T277">nʼi</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_209" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_211" n="HIAT:w" s="T278">Dibər-döbər</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_214" n="HIAT:w" s="T279">nuʔməleʔbi</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_218" n="HIAT:w" s="T280">dĭgəttə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_221" n="HIAT:w" s="T281">kambi</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_225" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_227" n="HIAT:w" s="T282">Nuʔməluʔpi</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_231" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_233" n="HIAT:w" s="T283">Nuʔməleʔbə</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_237" n="HIAT:w" s="T284">nuʔməleʔbə</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_241" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_243" n="HIAT:w" s="T285">Pʼeš</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_246" n="HIAT:w" s="T286">nulaʔbə</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_250" n="HIAT:u" s="T287">
                  <nts id="Seg_251" n="HIAT:ip">"</nts>
                  <ts e="T288" id="Seg_253" n="HIAT:w" s="T287">Tăn</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_256" n="HIAT:w" s="T288">ej</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_259" n="HIAT:w" s="T289">kubiam</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_262" n="HIAT:ip">(</nts>
                  <ts e="T291" id="Seg_264" n="HIAT:w" s="T290">gije-</ts>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_268" n="HIAT:w" s="T291">gijen</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_271" n="HIAT:w" s="T292">nʼergölüʔpiʔi</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_274" n="HIAT:w" s="T293">nabəʔi</ts>
                  <nts id="Seg_275" n="HIAT:ip">?</nts>
                  <nts id="Seg_276" n="HIAT:ip">"</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_279" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_281" n="HIAT:w" s="T294">Dĭ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_284" n="HIAT:w" s="T295">măndə:</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_286" n="HIAT:ip">"</nts>
                  <ts e="T297" id="Seg_288" n="HIAT:w" s="T296">Pirog</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_291" n="HIAT:w" s="T297">amaʔ</ts>
                  <nts id="Seg_292" n="HIAT:ip">"</nts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_296" n="HIAT:u" s="T298">
                  <nts id="Seg_297" n="HIAT:ip">"</nts>
                  <ts e="T299" id="Seg_299" n="HIAT:w" s="T298">Ej</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_302" n="HIAT:w" s="T299">budəj</ts>
                  <nts id="Seg_303" n="HIAT:ip">!</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_306" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_308" n="HIAT:w" s="T300">Măn</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_311" n="HIAT:w" s="T301">abandə</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_314" n="HIAT:w" s="T302">budəj</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_317" n="HIAT:w" s="T303">ige</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_321" n="HIAT:w" s="T304">da</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_324" n="HIAT:w" s="T305">ej</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_327" n="HIAT:w" s="T306">amniam</ts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_332" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_334" n="HIAT:w" s="T307">Nuʔməluʔpi</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_338" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_340" n="HIAT:w" s="T308">Dĭgəttə</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_343" n="HIAT:w" s="T309">šonəga</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_347" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_349" n="HIAT:w" s="T310">Dĭgəttə</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_352" n="HIAT:w" s="T311">pʼeš</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T313" id="Seg_356" n="HIAT:w" s="T312">ĭmbi</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_359" n="HIAT:w" s="T313">dĭʔnə</ts>
                  <nts id="Seg_360" n="HIAT:ip">)</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_363" n="HIAT:w" s="T314">ej</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_366" n="HIAT:w" s="T315">mămbi</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_370" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_372" n="HIAT:w" s="T316">Dĭ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_375" n="HIAT:w" s="T317">nuʔməluʔpi</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_378" n="HIAT:w" s="T318">bazoʔ</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_382" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_384" n="HIAT:w" s="T319">Dĭgəttə</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_387" n="HIAT:w" s="T320">šonəga</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_391" n="HIAT:w" s="T321">bü</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_394" n="HIAT:w" s="T322">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_398" n="HIAT:u" s="T323">
                  <nts id="Seg_399" n="HIAT:ip">"</nts>
                  <ts e="T324" id="Seg_401" n="HIAT:w" s="T323">Amnaʔ</ts>
                  <nts id="Seg_402" n="HIAT:ip">,</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_405" n="HIAT:w" s="T324">kiselʼ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_408" n="HIAT:w" s="T325">măn</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_411" n="HIAT:w" s="T326">sutsiʔ</ts>
                  <nts id="Seg_412" n="HIAT:ip">!</nts>
                  <nts id="Seg_413" n="HIAT:ip">"</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_416" n="HIAT:u" s="T327">
                  <nts id="Seg_417" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_419" n="HIAT:w" s="T327">Măn</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_422" n="HIAT:w" s="T328">abandə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_425" n="HIAT:w" s="T329">ej</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_428" n="HIAT:w" s="T330">dĭrgitdə</ts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_432" n="HIAT:w" s="T331">ej</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_435" n="HIAT:w" s="T332">amniam</ts>
                  <nts id="Seg_436" n="HIAT:ip">"</nts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_440" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_442" n="HIAT:w" s="T333">Dĭ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_445" n="HIAT:w" s="T334">dĭʔnə</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_448" n="HIAT:w" s="T335">ĭmbidə</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_451" n="HIAT:w" s="T336">ej</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_454" n="HIAT:w" s="T337">mămbi</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_458" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_460" n="HIAT:w" s="T338">Dĭgəttə</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_463" n="HIAT:w" s="T339">nuʔməlaʔbə</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_467" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_469" n="HIAT:w" s="T340">Nulaʔbə</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_472" n="HIAT:w" s="T341">jablokă</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_476" n="HIAT:u" s="T342">
                  <nts id="Seg_477" n="HIAT:ip">"</nts>
                  <ts e="T343" id="Seg_479" n="HIAT:w" s="T342">Kubial</ts>
                  <nts id="Seg_480" n="HIAT:ip">,</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_483" n="HIAT:w" s="T343">gibər</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ts e="T345" id="Seg_487" n="HIAT:w" s="T344">nam-</ts>
                  <nts id="Seg_488" n="HIAT:ip">)</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_491" n="HIAT:w" s="T345">nabəʔi</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_494" n="HIAT:w" s="T346">nʼergölüʔpiʔi</ts>
                  <nts id="Seg_495" n="HIAT:ip">?</nts>
                  <nts id="Seg_496" n="HIAT:ip">"</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_499" n="HIAT:u" s="T347">
                  <nts id="Seg_500" n="HIAT:ip">"</nts>
                  <ts e="T348" id="Seg_502" n="HIAT:w" s="T347">Amnaʔ</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_505" n="HIAT:w" s="T348">măn</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_508" n="HIAT:w" s="T349">jablokă</ts>
                  <nts id="Seg_509" n="HIAT:ip">!</nts>
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_513" n="HIAT:u" s="T350">
                  <nts id="Seg_514" n="HIAT:ip">"</nts>
                  <ts e="T351" id="Seg_516" n="HIAT:w" s="T350">Măn</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_519" n="HIAT:w" s="T351">abanə</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_522" n="HIAT:w" s="T352">iššo</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_525" n="HIAT:w" s="T353">jakšə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_527" n="HIAT:ip">(</nts>
                  <ts e="T355" id="Seg_529" n="HIAT:w" s="T354">jablok</ts>
                  <nts id="Seg_530" n="HIAT:ip">)</nts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_534" n="HIAT:w" s="T355">i</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_537" n="HIAT:w" s="T356">to</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_540" n="HIAT:w" s="T357">ej</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_543" n="HIAT:w" s="T358">amniam</ts>
                  <nts id="Seg_544" n="HIAT:ip">"</nts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_548" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_550" n="HIAT:w" s="T359">I</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_553" n="HIAT:w" s="T360">dĭʔnə</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_556" n="HIAT:w" s="T361">ĭmbidə</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_559" n="HIAT:w" s="T362">ej</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_562" n="HIAT:w" s="T363">nörbəbi</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_566" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_568" n="HIAT:w" s="T364">Dĭgəttə</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_570" n="HIAT:ip">(</nts>
                  <ts e="T366" id="Seg_572" n="HIAT:w" s="T365">m-</ts>
                  <nts id="Seg_573" n="HIAT:ip">)</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_576" n="HIAT:w" s="T366">kambi</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_580" n="HIAT:w" s="T367">kambi</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_584" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_586" n="HIAT:w" s="T368">Nulaʔbə</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_589" n="HIAT:w" s="T369">tura</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_593" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_595" n="HIAT:w" s="T370">Dĭ</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_598" n="HIAT:w" s="T371">turanə</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_601" n="HIAT:w" s="T372">šübi</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_605" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_607" n="HIAT:w" s="T373">Dĭn</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_610" n="HIAT:w" s="T374">nüke</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_613" n="HIAT:w" s="T375">amnolaʔbə</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_617" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_619" n="HIAT:w" s="T376">Jererleʔbə</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_623" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_625" n="HIAT:w" s="T377">Nʼi</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_627" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_629" n="HIAT:w" s="T378">ige</ts>
                  <nts id="Seg_630" n="HIAT:ip">)</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_633" n="HIAT:w" s="T379">i</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_635" n="HIAT:ip">(</nts>
                  <ts e="T381" id="Seg_637" n="HIAT:w" s="T380">dĭn</ts>
                  <nts id="Seg_638" n="HIAT:ip">)</nts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_642" n="HIAT:u" s="T381">
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <ts e="T382" id="Seg_645" n="HIAT:w" s="T381">Jerereʔ</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_649" n="HIAT:w" s="T382">ĭmbi</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_652" n="HIAT:w" s="T383">ileʔ</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_655" n="HIAT:w" s="T384">šobial</ts>
                  <nts id="Seg_656" n="HIAT:ip">?</nts>
                  <nts id="Seg_657" n="HIAT:ip">"</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_660" n="HIAT:u" s="T385">
                  <nts id="Seg_661" n="HIAT:ip">"</nts>
                  <ts e="T386" id="Seg_663" n="HIAT:w" s="T385">Da</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_666" n="HIAT:w" s="T386">măn</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_669" n="HIAT:w" s="T387">mĭmbiem</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_672" n="HIAT:w" s="T388">da</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_675" n="HIAT:w" s="T389">kănnaːmbiam</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_679" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_681" n="HIAT:w" s="T390">Ejümzittə</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_684" n="HIAT:w" s="T391">šobiam</ts>
                  <nts id="Seg_685" n="HIAT:ip">"</nts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_689" n="HIAT:u" s="T392">
                  <nts id="Seg_690" n="HIAT:ip">"</nts>
                  <ts e="T393" id="Seg_692" n="HIAT:w" s="T392">Amnaʔ</ts>
                  <nts id="Seg_693" n="HIAT:ip">,</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_696" n="HIAT:w" s="T393">jerereʔ</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_699" n="HIAT:w" s="T394">kudʼelʼə</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_702" n="HIAT:w" s="T395">măna</ts>
                  <nts id="Seg_703" n="HIAT:ip">"</nts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_707" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_709" n="HIAT:w" s="T396">Amnəbi</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_713" n="HIAT:w" s="T397">dĭgəttə</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_716" n="HIAT:w" s="T398">jererlaʔbə</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_720" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_722" n="HIAT:w" s="T399">A</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_725" n="HIAT:w" s="T400">nʼit</ts>
                  <nts id="Seg_726" n="HIAT:ip">,</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_729" n="HIAT:w" s="T401">nʼiʔnen</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_732" n="HIAT:w" s="T402">amnolaʔbə</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_736" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_738" n="HIAT:w" s="T403">Dĭgəttə</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_741" n="HIAT:w" s="T404">tumo</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_744" n="HIAT:w" s="T405">supsobi</ts>
                  <nts id="Seg_745" n="HIAT:ip">,</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_748" n="HIAT:w" s="T406">măndə:</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <ts e="T408" id="Seg_752" n="HIAT:w" s="T407">Koʔbdo</ts>
                  <nts id="Seg_753" n="HIAT:ip">,</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_756" n="HIAT:w" s="T408">tănan</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_759" n="HIAT:w" s="T409">nüke</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_762" n="HIAT:w" s="T410">nendleʔbə</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_765" n="HIAT:w" s="T411">moltʼa</ts>
                  <nts id="Seg_766" n="HIAT:ip">.</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_769" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_771" n="HIAT:w" s="T412">I</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_774" n="HIAT:w" s="T413">dĭn</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_777" n="HIAT:w" s="T414">tănan</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_780" n="HIAT:w" s="T415">nendləj</ts>
                  <nts id="Seg_781" n="HIAT:ip">,</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_783" n="HIAT:ip">(</nts>
                  <ts e="T417" id="Seg_785" n="HIAT:w" s="T416">ibə</ts>
                  <nts id="Seg_786" n="HIAT:ip">)</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_789" n="HIAT:w" s="T417">amnəj</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_793" n="HIAT:u" s="T418">
                  <nts id="Seg_794" n="HIAT:ip">(</nts>
                  <ts e="T419" id="Seg_796" n="HIAT:w" s="T418">Elendə</ts>
                  <nts id="Seg_797" n="HIAT:ip">)</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_800" n="HIAT:w" s="T419">bar</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_802" n="HIAT:ip">(</nts>
                  <ts e="T421" id="Seg_804" n="HIAT:w" s="T420">amnolləj</ts>
                  <nts id="Seg_805" n="HIAT:ip">)</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_808" n="HIAT:w" s="T421">bostə</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_812" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_814" n="HIAT:w" s="T422">Deʔ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_817" n="HIAT:w" s="T423">măna</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_820" n="HIAT:w" s="T424">amzittə</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ts e="T426" id="Seg_824" n="HIAT:w" s="T425">m-</ts>
                  <nts id="Seg_825" n="HIAT:ip">)</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_828" n="HIAT:w" s="T426">kaška</ts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_833" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_835" n="HIAT:w" s="T427">Dĭ</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_838" n="HIAT:w" s="T428">dĭʔnə</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_840" n="HIAT:ip">(</nts>
                  <ts e="T430" id="Seg_842" n="HIAT:w" s="T429">mĭm-</ts>
                  <nts id="Seg_843" n="HIAT:ip">)</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_846" n="HIAT:w" s="T430">mĭbi</ts>
                  <nts id="Seg_847" n="HIAT:ip">.</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_850" n="HIAT:u" s="T431">
                  <nts id="Seg_851" n="HIAT:ip">"</nts>
                  <ts e="T432" id="Seg_853" n="HIAT:w" s="T431">Kanaʔ</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_857" n="HIAT:w" s="T432">a</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_860" n="HIAT:w" s="T433">măn</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_863" n="HIAT:w" s="T434">jererləm</ts>
                  <nts id="Seg_864" n="HIAT:ip">.</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_867" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_869" n="HIAT:w" s="T435">It</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_872" n="HIAT:w" s="T436">bostə</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_874" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_876" n="HIAT:w" s="T437">nʼin</ts>
                  <nts id="Seg_877" n="HIAT:ip">)</nts>
                  <nts id="Seg_878" n="HIAT:ip">"</nts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_882" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_884" n="HIAT:w" s="T438">Dĭ</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_887" n="HIAT:w" s="T439">kabarlaʔ</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_890" n="HIAT:w" s="T440">ibi</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_893" n="HIAT:w" s="T441">i</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_896" n="HIAT:w" s="T442">nuʔməluʔpi</ts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_900" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_902" n="HIAT:w" s="T443">Dĭ</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_905" n="HIAT:w" s="T444">nüke</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_908" n="HIAT:w" s="T445">šoləj</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_912" n="HIAT:u" s="T446">
                  <nts id="Seg_913" n="HIAT:ip">"</nts>
                  <ts e="T447" id="Seg_915" n="HIAT:w" s="T446">Tăn</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_918" n="HIAT:w" s="T447">jererleʔbəl</ts>
                  <nts id="Seg_919" n="HIAT:ip">?</nts>
                  <nts id="Seg_920" n="HIAT:ip">"</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_923" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_925" n="HIAT:w" s="T448">Dĭ</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_928" n="HIAT:w" s="T449">măndə:</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_930" n="HIAT:ip">"</nts>
                  <ts e="T451" id="Seg_932" n="HIAT:w" s="T450">Jererleʔbəm</ts>
                  <nts id="Seg_933" n="HIAT:ip">"</nts>
                  <nts id="Seg_934" n="HIAT:ip">.</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_937" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_939" n="HIAT:w" s="T451">Dĭgəttə</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_942" n="HIAT:w" s="T452">bazoʔ</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_945" n="HIAT:w" s="T453">kalləj</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_948" n="HIAT:w" s="T454">moltʼa</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_951" n="HIAT:w" s="T455">nendəsʼtə</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_955" n="HIAT:w" s="T456">dĭ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_958" n="HIAT:w" s="T457">koʔbdo</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_961" n="HIAT:w" s="T458">nuʔməluʔpi</ts>
                  <nts id="Seg_962" n="HIAT:ip">.</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_965" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_967" n="HIAT:w" s="T459">Šobi</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_971" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_973" n="HIAT:w" s="T460">Dĭgəttə</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_976" n="HIAT:w" s="T461">nüke</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_978" n="HIAT:ip">(</nts>
                  <ts e="T463" id="Seg_980" n="HIAT:w" s="T462">šo-</ts>
                  <nts id="Seg_981" n="HIAT:ip">)</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_984" n="HIAT:w" s="T463">turanə</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_987" n="HIAT:w" s="T464">šobi</ts>
                  <nts id="Seg_988" n="HIAT:ip">,</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_991" n="HIAT:w" s="T465">koʔbdo</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_994" n="HIAT:w" s="T466">naga</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_997" n="HIAT:w" s="T467">i</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1000" n="HIAT:w" s="T468">nʼi</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1003" n="HIAT:w" s="T469">naga</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1007" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1009" n="HIAT:w" s="T470">Dĭgəttə</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1012" n="HIAT:w" s="T471">nabəʔinə</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1015" n="HIAT:w" s="T472">măndə</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1019" n="HIAT:u" s="T473">
                  <nts id="Seg_1020" n="HIAT:ip">"</nts>
                  <ts e="T474" id="Seg_1022" n="HIAT:w" s="T473">Kangaʔ</ts>
                  <nts id="Seg_1023" n="HIAT:ip">,</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1026" n="HIAT:w" s="T474">măndərgaʔ</ts>
                  <nts id="Seg_1027" n="HIAT:ip">,</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1030" n="HIAT:w" s="T475">gijen</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1033" n="HIAT:w" s="T476">koʔbdo</ts>
                  <nts id="Seg_1034" n="HIAT:ip">!</nts>
                  <nts id="Seg_1035" n="HIAT:ip">"</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1038" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1040" n="HIAT:w" s="T477">Dĭ</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1043" n="HIAT:w" s="T478">koʔbdo</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1045" n="HIAT:ip">(</nts>
                  <ts e="T480" id="Seg_1047" n="HIAT:w" s="T479">nu-</ts>
                  <nts id="Seg_1048" n="HIAT:ip">)</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1051" n="HIAT:w" s="T480">nuʔməleʔ</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1054" n="HIAT:w" s="T481">šonəga</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1058" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1060" n="HIAT:w" s="T482">Bünə</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1063" n="HIAT:w" s="T483">šobi</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1067" n="HIAT:u" s="T484">
                  <nts id="Seg_1068" n="HIAT:ip">"</nts>
                  <ts e="T485" id="Seg_1070" n="HIAT:w" s="T484">Bü</ts>
                  <nts id="Seg_1071" n="HIAT:ip">,</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1074" n="HIAT:w" s="T485">bü</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1078" n="HIAT:w" s="T486">šaʔlaʔ</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1081" n="HIAT:w" s="T487">măna</ts>
                  <nts id="Seg_1082" n="HIAT:ip">!</nts>
                  <nts id="Seg_1083" n="HIAT:ip">"</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1086" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1088" n="HIAT:w" s="T488">Dĭ</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1091" n="HIAT:w" s="T489">măndə:</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1093" n="HIAT:ip">"</nts>
                  <nts id="Seg_1094" n="HIAT:ip">(</nts>
                  <ts e="T491" id="Seg_1096" n="HIAT:w" s="T490">Am-</ts>
                  <nts id="Seg_1097" n="HIAT:ip">)</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1100" n="HIAT:w" s="T491">Amnaʔ</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1103" n="HIAT:w" s="T492">măn</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1106" n="HIAT:w" s="T493">kiselʼbə</ts>
                  <nts id="Seg_1107" n="HIAT:ip">"</nts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1111" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1113" n="HIAT:w" s="T494">Dĭ</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1116" n="HIAT:w" s="T495">ambi</ts>
                  <nts id="Seg_1117" n="HIAT:ip">,</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1120" n="HIAT:w" s="T496">dĭ</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1123" n="HIAT:w" s="T497">dĭm</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1126" n="HIAT:w" s="T498">kajluʔpi</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_1130" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_1132" n="HIAT:w" s="T499">Dĭgəttə</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1135" n="HIAT:w" s="T500">nabəʔi</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1138" n="HIAT:w" s="T501">nʼergölüʔpiʔi</ts>
                  <nts id="Seg_1139" n="HIAT:ip">.</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1142" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_1144" n="HIAT:w" s="T502">Dĭ</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1147" n="HIAT:w" s="T503">bar</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1150" n="HIAT:w" s="T504">supsobi</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_1154" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1156" n="HIAT:w" s="T505">Bazoʔ</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1159" n="HIAT:w" s="T506">nuʔməlaʔbə</ts>
                  <nts id="Seg_1160" n="HIAT:ip">.</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1163" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1165" n="HIAT:w" s="T507">Nabəʔi</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1168" n="HIAT:w" s="T508">bazoʔ</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1171" n="HIAT:w" s="T509">nʼergölaʔbəʔjə</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1175" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1177" n="HIAT:w" s="T510">Dĭgəttə</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1180" n="HIAT:w" s="T511">jablokdə</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1183" n="HIAT:w" s="T512">măndə:</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1185" n="HIAT:ip">"</nts>
                  <ts e="T514" id="Seg_1187" n="HIAT:w" s="T513">Šaʔlaːndə</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1190" n="HIAT:w" s="T514">miʔnʼibeʔ</ts>
                  <nts id="Seg_1191" n="HIAT:ip">!</nts>
                  <nts id="Seg_1192" n="HIAT:ip">"</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1195" n="HIAT:u" s="T515">
                  <nts id="Seg_1196" n="HIAT:ip">"</nts>
                  <ts e="T516" id="Seg_1198" n="HIAT:w" s="T515">Amaʔ</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1201" n="HIAT:w" s="T516">jablokă</ts>
                  <nts id="Seg_1202" n="HIAT:ip">"</nts>
                  <nts id="Seg_1203" n="HIAT:ip">,</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1206" n="HIAT:w" s="T517">dĭ</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1209" n="HIAT:w" s="T518">ambi</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1213" n="HIAT:w" s="T519">dĭ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1216" n="HIAT:w" s="T520">dĭm</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1219" n="HIAT:w" s="T521">šaʔlaːmbi</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_1223" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1225" n="HIAT:w" s="T522">Dĭgəttə</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1228" n="HIAT:w" s="T523">nabəʔi</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1231" n="HIAT:w" s="T524">nʼergölüʔpiʔi</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1234" n="HIAT:w" s="T525">dĭ</ts>
                  <nts id="Seg_1235" n="HIAT:ip">.</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_1238" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_1240" n="HIAT:w" s="T526">Dĭzeŋ</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1243" n="HIAT:w" s="T527">bazoʔ</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1246" n="HIAT:w" s="T528">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1250" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_1252" n="HIAT:w" s="T529">Pʼešdə</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1255" n="HIAT:w" s="T530">šobi</ts>
                  <nts id="Seg_1256" n="HIAT:ip">.</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_1259" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1261" n="HIAT:w" s="T531">Pʼeš</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1264" n="HIAT:w" s="T532">măndə:</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1266" n="HIAT:ip">"</nts>
                  <ts e="T534" id="Seg_1268" n="HIAT:w" s="T533">Nabəʔi</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1270" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_1272" n="HIAT:w" s="T534">a-</ts>
                  <nts id="Seg_1273" n="HIAT:ip">)</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1276" n="HIAT:w" s="T535">bar</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1278" n="HIAT:ip">(</nts>
                  <ts e="T537" id="Seg_1280" n="HIAT:w" s="T536">kabarliaʔiʔi</ts>
                  <nts id="Seg_1281" n="HIAT:ip">)</nts>
                  <nts id="Seg_1282" n="HIAT:ip">"</nts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_1286" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_1288" n="HIAT:w" s="T537">Pʼeš</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1291" n="HIAT:w" s="T538">măndə:</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1293" n="HIAT:ip">"</nts>
                  <ts e="T540" id="Seg_1295" n="HIAT:w" s="T539">Amaʔ</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1298" n="HIAT:w" s="T540">măn</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1301" n="HIAT:w" s="T541">pirogdə</ts>
                  <nts id="Seg_1302" n="HIAT:ip">!</nts>
                  <nts id="Seg_1303" n="HIAT:ip">"</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_1306" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_1308" n="HIAT:w" s="T542">Dĭ</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1311" n="HIAT:w" s="T543">bar</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1314" n="HIAT:w" s="T544">ambi</ts>
                  <nts id="Seg_1315" n="HIAT:ip">,</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1318" n="HIAT:w" s="T545">pʼešdə</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1320" n="HIAT:ip">(</nts>
                  <ts e="T547" id="Seg_1322" n="HIAT:w" s="T546">še-</ts>
                  <nts id="Seg_1323" n="HIAT:ip">)</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1326" n="HIAT:w" s="T547">sʼaluʔpi</ts>
                  <nts id="Seg_1327" n="HIAT:ip">.</nts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_1330" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_1332" n="HIAT:w" s="T548">Nabəʔi</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1335" n="HIAT:w" s="T549">nʼergölaʔpi</ts>
                  <nts id="Seg_1336" n="HIAT:ip">,</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1339" n="HIAT:w" s="T550">nʼergölaʔpi</ts>
                  <nts id="Seg_1340" n="HIAT:ip">,</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1343" n="HIAT:w" s="T551">gijendə</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1346" n="HIAT:w" s="T552">naga</ts>
                  <nts id="Seg_1347" n="HIAT:ip">.</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_1350" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_1352" n="HIAT:w" s="T553">Parluʔpiʔi</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1355" n="HIAT:w" s="T554">nükenə</ts>
                  <nts id="Seg_1356" n="HIAT:ip">.</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_1359" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_1361" n="HIAT:w" s="T555">A</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1364" n="HIAT:w" s="T556">dĭzeŋ</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1367" n="HIAT:w" s="T557">supsolaʔ</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1370" n="HIAT:w" s="T558">šobiʔi</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1373" n="HIAT:w" s="T559">maːndən</ts>
                  <nts id="Seg_1374" n="HIAT:ip">.</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_1377" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_1379" n="HIAT:w" s="T560">Šobiʔi</ts>
                  <nts id="Seg_1380" n="HIAT:ip">.</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_1383" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1385" n="HIAT:w" s="T561">I</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1388" n="HIAT:w" s="T562">abat</ts>
                  <nts id="Seg_1389" n="HIAT:ip">,</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1392" n="HIAT:w" s="T563">ijat</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1395" n="HIAT:w" s="T564">šobiʔi</ts>
                  <nts id="Seg_1396" n="HIAT:ip">.</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_1399" n="HIAT:u" s="T565">
                  <nts id="Seg_1400" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_1402" n="HIAT:w" s="T565">Togonorzit-</ts>
                  <nts id="Seg_1403" n="HIAT:ip">)</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1406" n="HIAT:w" s="T566">Togonorgəʔ</ts>
                  <nts id="Seg_1407" n="HIAT:ip">.</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_1410" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_1412" n="HIAT:w" s="T567">Bar</ts>
                  <nts id="Seg_1413" n="HIAT:ip">.</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T568" id="Seg_1415" n="sc" s="T227">
               <ts e="T228" id="Seg_1417" n="e" s="T227">Amnobiʔi </ts>
               <ts e="T229" id="Seg_1419" n="e" s="T228">nüke </ts>
               <ts e="T230" id="Seg_1421" n="e" s="T229">büzʼezʼiʔ. </ts>
               <ts e="T231" id="Seg_1423" n="e" s="T230">Dĭzeŋ </ts>
               <ts e="T232" id="Seg_1425" n="e" s="T231">(bɨl- </ts>
               <ts e="T233" id="Seg_1427" n="e" s="T232">ko-) </ts>
               <ts e="T234" id="Seg_1429" n="e" s="T233">ibi </ts>
               <ts e="T235" id="Seg_1431" n="e" s="T234">koʔbdo </ts>
               <ts e="T236" id="Seg_1433" n="e" s="T235">i </ts>
               <ts e="T237" id="Seg_1435" n="e" s="T236">nʼi. </ts>
               <ts e="T238" id="Seg_1437" n="e" s="T237">Dĭzeŋ </ts>
               <ts e="T239" id="Seg_1439" n="e" s="T238">togonorzittə </ts>
               <ts e="T240" id="Seg_1441" n="e" s="T239">kallaʔbəʔjə. </ts>
               <ts e="T241" id="Seg_1443" n="e" s="T240">"Măndot </ts>
               <ts e="T242" id="Seg_1445" n="e" s="T241">nʼim. </ts>
               <ts e="T243" id="Seg_1447" n="e" s="T242">(M- </ts>
               <ts e="T244" id="Seg_1449" n="e" s="T243">mi-) </ts>
               <ts e="T245" id="Seg_1451" n="e" s="T244">Măn </ts>
               <ts e="T246" id="Seg_1453" n="e" s="T245">tănan </ts>
               <ts e="T247" id="Seg_1455" n="e" s="T246">ilem </ts>
               <ts e="T248" id="Seg_1457" n="e" s="T247">plat, </ts>
               <ts e="T249" id="Seg_1459" n="e" s="T248">kuvas". </ts>
               <ts e="T250" id="Seg_1461" n="e" s="T249">Kambiʔi, </ts>
               <ts e="T251" id="Seg_1463" n="e" s="T250">a </ts>
               <ts e="T252" id="Seg_1465" n="e" s="T251">koʔbdot </ts>
               <ts e="T253" id="Seg_1467" n="e" s="T252">nʼibə </ts>
               <ts e="T254" id="Seg_1469" n="e" s="T253">ibi. </ts>
               <ts e="T255" id="Seg_1471" n="e" s="T254">Nʼiʔnenə </ts>
               <ts e="T256" id="Seg_1473" n="e" s="T255">amnolbi. </ts>
               <ts e="T257" id="Seg_1475" n="e" s="T256">Noʔgən, </ts>
               <ts e="T258" id="Seg_1477" n="e" s="T257">a </ts>
               <ts e="T259" id="Seg_1479" n="e" s="T258">bostə </ts>
               <ts e="T260" id="Seg_1481" n="e" s="T259">nuʔməluʔpi. </ts>
               <ts e="T261" id="Seg_1483" n="e" s="T260">Dön </ts>
               <ts e="T262" id="Seg_1485" n="e" s="T261">nabəʔi </ts>
               <ts e="T263" id="Seg_1487" n="e" s="T262">nanʼergöluʔpi. </ts>
               <ts e="T264" id="Seg_1489" n="e" s="T263">Dĭ </ts>
               <ts e="T265" id="Seg_1491" n="e" s="T264">nʼim </ts>
               <ts e="T266" id="Seg_1493" n="e" s="T265">(i- </ts>
               <ts e="T267" id="Seg_1495" n="e" s="T266">i-) </ts>
               <ts e="T268" id="Seg_1497" n="e" s="T267">iluʔpiʔi, </ts>
               <ts e="T269" id="Seg_1499" n="e" s="T268">(kunnaːlbiʔi), </ts>
               <ts e="T270" id="Seg_1501" n="e" s="T269">kumbiʔi. </ts>
               <ts e="T271" id="Seg_1503" n="e" s="T270">Koʔbdo </ts>
               <ts e="T272" id="Seg_1505" n="e" s="T271">(suʔmileʔ-) </ts>
               <ts e="T273" id="Seg_1507" n="e" s="T272">nuʔmileʔ </ts>
               <ts e="T274" id="Seg_1509" n="e" s="T273">šobi. </ts>
               <ts e="T275" id="Seg_1511" n="e" s="T274">Kuliot: </ts>
               <ts e="T276" id="Seg_1513" n="e" s="T275">naga, </ts>
               <ts e="T277" id="Seg_1515" n="e" s="T276">naga </ts>
               <ts e="T278" id="Seg_1517" n="e" s="T277">nʼi. </ts>
               <ts e="T279" id="Seg_1519" n="e" s="T278">Dibər-döbər </ts>
               <ts e="T280" id="Seg_1521" n="e" s="T279">nuʔməleʔbi, </ts>
               <ts e="T281" id="Seg_1523" n="e" s="T280">dĭgəttə </ts>
               <ts e="T282" id="Seg_1525" n="e" s="T281">kambi. </ts>
               <ts e="T283" id="Seg_1527" n="e" s="T282">Nuʔməluʔpi. </ts>
               <ts e="T284" id="Seg_1529" n="e" s="T283">Nuʔməleʔbə, </ts>
               <ts e="T285" id="Seg_1531" n="e" s="T284">nuʔməleʔbə. </ts>
               <ts e="T286" id="Seg_1533" n="e" s="T285">Pʼeš </ts>
               <ts e="T287" id="Seg_1535" n="e" s="T286">nulaʔbə. </ts>
               <ts e="T288" id="Seg_1537" n="e" s="T287">"Tăn </ts>
               <ts e="T289" id="Seg_1539" n="e" s="T288">ej </ts>
               <ts e="T290" id="Seg_1541" n="e" s="T289">kubiam, </ts>
               <ts e="T291" id="Seg_1543" n="e" s="T290">(gije-) </ts>
               <ts e="T292" id="Seg_1545" n="e" s="T291">gijen </ts>
               <ts e="T293" id="Seg_1547" n="e" s="T292">nʼergölüʔpiʔi </ts>
               <ts e="T294" id="Seg_1549" n="e" s="T293">nabəʔi?" </ts>
               <ts e="T295" id="Seg_1551" n="e" s="T294">Dĭ </ts>
               <ts e="T296" id="Seg_1553" n="e" s="T295">măndə: </ts>
               <ts e="T297" id="Seg_1555" n="e" s="T296">"Pirog </ts>
               <ts e="T298" id="Seg_1557" n="e" s="T297">amaʔ". </ts>
               <ts e="T299" id="Seg_1559" n="e" s="T298">"Ej </ts>
               <ts e="T300" id="Seg_1561" n="e" s="T299">budəj! </ts>
               <ts e="T301" id="Seg_1563" n="e" s="T300">Măn </ts>
               <ts e="T302" id="Seg_1565" n="e" s="T301">abandə </ts>
               <ts e="T303" id="Seg_1567" n="e" s="T302">budəj </ts>
               <ts e="T304" id="Seg_1569" n="e" s="T303">ige, </ts>
               <ts e="T305" id="Seg_1571" n="e" s="T304">da </ts>
               <ts e="T306" id="Seg_1573" n="e" s="T305">ej </ts>
               <ts e="T307" id="Seg_1575" n="e" s="T306">amniam". </ts>
               <ts e="T308" id="Seg_1577" n="e" s="T307">Nuʔməluʔpi. </ts>
               <ts e="T309" id="Seg_1579" n="e" s="T308">Dĭgəttə </ts>
               <ts e="T310" id="Seg_1581" n="e" s="T309">šonəga. </ts>
               <ts e="T311" id="Seg_1583" n="e" s="T310">Dĭgəttə </ts>
               <ts e="T312" id="Seg_1585" n="e" s="T311">pʼeš </ts>
               <ts e="T313" id="Seg_1587" n="e" s="T312">(ĭmbi </ts>
               <ts e="T314" id="Seg_1589" n="e" s="T313">dĭʔnə) </ts>
               <ts e="T315" id="Seg_1591" n="e" s="T314">ej </ts>
               <ts e="T316" id="Seg_1593" n="e" s="T315">mămbi. </ts>
               <ts e="T317" id="Seg_1595" n="e" s="T316">Dĭ </ts>
               <ts e="T318" id="Seg_1597" n="e" s="T317">nuʔməluʔpi </ts>
               <ts e="T319" id="Seg_1599" n="e" s="T318">bazoʔ. </ts>
               <ts e="T320" id="Seg_1601" n="e" s="T319">Dĭgəttə </ts>
               <ts e="T321" id="Seg_1603" n="e" s="T320">šonəga, </ts>
               <ts e="T322" id="Seg_1605" n="e" s="T321">bü </ts>
               <ts e="T323" id="Seg_1607" n="e" s="T322">mʼaŋŋaʔbə. </ts>
               <ts e="T324" id="Seg_1609" n="e" s="T323">"Amnaʔ, </ts>
               <ts e="T325" id="Seg_1611" n="e" s="T324">kiselʼ </ts>
               <ts e="T326" id="Seg_1613" n="e" s="T325">măn </ts>
               <ts e="T327" id="Seg_1615" n="e" s="T326">sutsiʔ!" </ts>
               <ts e="T328" id="Seg_1617" n="e" s="T327">"Măn </ts>
               <ts e="T329" id="Seg_1619" n="e" s="T328">abandə </ts>
               <ts e="T330" id="Seg_1621" n="e" s="T329">ej </ts>
               <ts e="T331" id="Seg_1623" n="e" s="T330">dĭrgitdə, </ts>
               <ts e="T332" id="Seg_1625" n="e" s="T331">ej </ts>
               <ts e="T333" id="Seg_1627" n="e" s="T332">amniam". </ts>
               <ts e="T334" id="Seg_1629" n="e" s="T333">Dĭ </ts>
               <ts e="T335" id="Seg_1631" n="e" s="T334">dĭʔnə </ts>
               <ts e="T336" id="Seg_1633" n="e" s="T335">ĭmbidə </ts>
               <ts e="T337" id="Seg_1635" n="e" s="T336">ej </ts>
               <ts e="T338" id="Seg_1637" n="e" s="T337">mămbi. </ts>
               <ts e="T339" id="Seg_1639" n="e" s="T338">Dĭgəttə </ts>
               <ts e="T340" id="Seg_1641" n="e" s="T339">nuʔməlaʔbə. </ts>
               <ts e="T341" id="Seg_1643" n="e" s="T340">Nulaʔbə </ts>
               <ts e="T342" id="Seg_1645" n="e" s="T341">jablokă. </ts>
               <ts e="T343" id="Seg_1647" n="e" s="T342">"Kubial, </ts>
               <ts e="T344" id="Seg_1649" n="e" s="T343">gibər </ts>
               <ts e="T345" id="Seg_1651" n="e" s="T344">(nam-) </ts>
               <ts e="T346" id="Seg_1653" n="e" s="T345">nabəʔi </ts>
               <ts e="T347" id="Seg_1655" n="e" s="T346">nʼergölüʔpiʔi?" </ts>
               <ts e="T348" id="Seg_1657" n="e" s="T347">"Amnaʔ </ts>
               <ts e="T349" id="Seg_1659" n="e" s="T348">măn </ts>
               <ts e="T350" id="Seg_1661" n="e" s="T349">jablokă!" </ts>
               <ts e="T351" id="Seg_1663" n="e" s="T350">"Măn </ts>
               <ts e="T352" id="Seg_1665" n="e" s="T351">abanə </ts>
               <ts e="T353" id="Seg_1667" n="e" s="T352">iššo </ts>
               <ts e="T354" id="Seg_1669" n="e" s="T353">jakšə </ts>
               <ts e="T355" id="Seg_1671" n="e" s="T354">(jablok), </ts>
               <ts e="T356" id="Seg_1673" n="e" s="T355">i </ts>
               <ts e="T357" id="Seg_1675" n="e" s="T356">to </ts>
               <ts e="T358" id="Seg_1677" n="e" s="T357">ej </ts>
               <ts e="T359" id="Seg_1679" n="e" s="T358">amniam". </ts>
               <ts e="T360" id="Seg_1681" n="e" s="T359">I </ts>
               <ts e="T361" id="Seg_1683" n="e" s="T360">dĭʔnə </ts>
               <ts e="T362" id="Seg_1685" n="e" s="T361">ĭmbidə </ts>
               <ts e="T363" id="Seg_1687" n="e" s="T362">ej </ts>
               <ts e="T364" id="Seg_1689" n="e" s="T363">nörbəbi. </ts>
               <ts e="T365" id="Seg_1691" n="e" s="T364">Dĭgəttə </ts>
               <ts e="T366" id="Seg_1693" n="e" s="T365">(m-) </ts>
               <ts e="T367" id="Seg_1695" n="e" s="T366">kambi, </ts>
               <ts e="T368" id="Seg_1697" n="e" s="T367">kambi. </ts>
               <ts e="T369" id="Seg_1699" n="e" s="T368">Nulaʔbə </ts>
               <ts e="T370" id="Seg_1701" n="e" s="T369">tura. </ts>
               <ts e="T371" id="Seg_1703" n="e" s="T370">Dĭ </ts>
               <ts e="T372" id="Seg_1705" n="e" s="T371">turanə </ts>
               <ts e="T373" id="Seg_1707" n="e" s="T372">šübi. </ts>
               <ts e="T374" id="Seg_1709" n="e" s="T373">Dĭn </ts>
               <ts e="T375" id="Seg_1711" n="e" s="T374">nüke </ts>
               <ts e="T376" id="Seg_1713" n="e" s="T375">amnolaʔbə. </ts>
               <ts e="T377" id="Seg_1715" n="e" s="T376">Jererleʔbə. </ts>
               <ts e="T378" id="Seg_1717" n="e" s="T377">Nʼi </ts>
               <ts e="T379" id="Seg_1719" n="e" s="T378">(ige) </ts>
               <ts e="T380" id="Seg_1721" n="e" s="T379">i </ts>
               <ts e="T381" id="Seg_1723" n="e" s="T380">(dĭn). </ts>
               <ts e="T382" id="Seg_1725" n="e" s="T381">"Jerereʔ, </ts>
               <ts e="T383" id="Seg_1727" n="e" s="T382">ĭmbi </ts>
               <ts e="T384" id="Seg_1729" n="e" s="T383">ileʔ </ts>
               <ts e="T385" id="Seg_1731" n="e" s="T384">šobial?" </ts>
               <ts e="T386" id="Seg_1733" n="e" s="T385">"Da </ts>
               <ts e="T387" id="Seg_1735" n="e" s="T386">măn </ts>
               <ts e="T388" id="Seg_1737" n="e" s="T387">mĭmbiem </ts>
               <ts e="T389" id="Seg_1739" n="e" s="T388">da </ts>
               <ts e="T390" id="Seg_1741" n="e" s="T389">kănnaːmbiam. </ts>
               <ts e="T391" id="Seg_1743" n="e" s="T390">Ejümzittə </ts>
               <ts e="T392" id="Seg_1745" n="e" s="T391">šobiam". </ts>
               <ts e="T393" id="Seg_1747" n="e" s="T392">"Amnaʔ, </ts>
               <ts e="T394" id="Seg_1749" n="e" s="T393">jerereʔ </ts>
               <ts e="T395" id="Seg_1751" n="e" s="T394">kudʼelʼə </ts>
               <ts e="T396" id="Seg_1753" n="e" s="T395">măna". </ts>
               <ts e="T397" id="Seg_1755" n="e" s="T396">Amnəbi, </ts>
               <ts e="T398" id="Seg_1757" n="e" s="T397">dĭgəttə </ts>
               <ts e="T399" id="Seg_1759" n="e" s="T398">jererlaʔbə. </ts>
               <ts e="T400" id="Seg_1761" n="e" s="T399">A </ts>
               <ts e="T401" id="Seg_1763" n="e" s="T400">nʼit, </ts>
               <ts e="T402" id="Seg_1765" n="e" s="T401">nʼiʔnen </ts>
               <ts e="T403" id="Seg_1767" n="e" s="T402">amnolaʔbə. </ts>
               <ts e="T404" id="Seg_1769" n="e" s="T403">Dĭgəttə </ts>
               <ts e="T405" id="Seg_1771" n="e" s="T404">tumo </ts>
               <ts e="T406" id="Seg_1773" n="e" s="T405">supsobi, </ts>
               <ts e="T407" id="Seg_1775" n="e" s="T406">măndə: </ts>
               <ts e="T408" id="Seg_1777" n="e" s="T407">"Koʔbdo, </ts>
               <ts e="T409" id="Seg_1779" n="e" s="T408">tănan </ts>
               <ts e="T410" id="Seg_1781" n="e" s="T409">nüke </ts>
               <ts e="T411" id="Seg_1783" n="e" s="T410">nendleʔbə </ts>
               <ts e="T412" id="Seg_1785" n="e" s="T411">moltʼa. </ts>
               <ts e="T413" id="Seg_1787" n="e" s="T412">I </ts>
               <ts e="T414" id="Seg_1789" n="e" s="T413">dĭn </ts>
               <ts e="T415" id="Seg_1791" n="e" s="T414">tănan </ts>
               <ts e="T416" id="Seg_1793" n="e" s="T415">nendləj, </ts>
               <ts e="T417" id="Seg_1795" n="e" s="T416">(ibə) </ts>
               <ts e="T418" id="Seg_1797" n="e" s="T417">amnəj. </ts>
               <ts e="T419" id="Seg_1799" n="e" s="T418">(Elendə) </ts>
               <ts e="T420" id="Seg_1801" n="e" s="T419">bar </ts>
               <ts e="T421" id="Seg_1803" n="e" s="T420">(amnolləj) </ts>
               <ts e="T422" id="Seg_1805" n="e" s="T421">bostə. </ts>
               <ts e="T423" id="Seg_1807" n="e" s="T422">Deʔ </ts>
               <ts e="T424" id="Seg_1809" n="e" s="T423">măna </ts>
               <ts e="T425" id="Seg_1811" n="e" s="T424">amzittə </ts>
               <ts e="T426" id="Seg_1813" n="e" s="T425">(m-) </ts>
               <ts e="T427" id="Seg_1815" n="e" s="T426">kaška". </ts>
               <ts e="T428" id="Seg_1817" n="e" s="T427">Dĭ </ts>
               <ts e="T429" id="Seg_1819" n="e" s="T428">dĭʔnə </ts>
               <ts e="T430" id="Seg_1821" n="e" s="T429">(mĭm-) </ts>
               <ts e="T431" id="Seg_1823" n="e" s="T430">mĭbi. </ts>
               <ts e="T432" id="Seg_1825" n="e" s="T431">"Kanaʔ, </ts>
               <ts e="T433" id="Seg_1827" n="e" s="T432">a </ts>
               <ts e="T434" id="Seg_1829" n="e" s="T433">măn </ts>
               <ts e="T435" id="Seg_1831" n="e" s="T434">jererləm. </ts>
               <ts e="T436" id="Seg_1833" n="e" s="T435">It </ts>
               <ts e="T437" id="Seg_1835" n="e" s="T436">bostə </ts>
               <ts e="T438" id="Seg_1837" n="e" s="T437">(nʼin)". </ts>
               <ts e="T439" id="Seg_1839" n="e" s="T438">Dĭ </ts>
               <ts e="T440" id="Seg_1841" n="e" s="T439">kabarlaʔ </ts>
               <ts e="T441" id="Seg_1843" n="e" s="T440">ibi </ts>
               <ts e="T442" id="Seg_1845" n="e" s="T441">i </ts>
               <ts e="T443" id="Seg_1847" n="e" s="T442">nuʔməluʔpi. </ts>
               <ts e="T444" id="Seg_1849" n="e" s="T443">Dĭ </ts>
               <ts e="T445" id="Seg_1851" n="e" s="T444">nüke </ts>
               <ts e="T446" id="Seg_1853" n="e" s="T445">šoləj. </ts>
               <ts e="T447" id="Seg_1855" n="e" s="T446">"Tăn </ts>
               <ts e="T448" id="Seg_1857" n="e" s="T447">jererleʔbəl?" </ts>
               <ts e="T449" id="Seg_1859" n="e" s="T448">Dĭ </ts>
               <ts e="T450" id="Seg_1861" n="e" s="T449">măndə: </ts>
               <ts e="T451" id="Seg_1863" n="e" s="T450">"Jererleʔbəm". </ts>
               <ts e="T452" id="Seg_1865" n="e" s="T451">Dĭgəttə </ts>
               <ts e="T453" id="Seg_1867" n="e" s="T452">bazoʔ </ts>
               <ts e="T454" id="Seg_1869" n="e" s="T453">kalləj </ts>
               <ts e="T455" id="Seg_1871" n="e" s="T454">moltʼa </ts>
               <ts e="T456" id="Seg_1873" n="e" s="T455">nendəsʼtə, </ts>
               <ts e="T457" id="Seg_1875" n="e" s="T456">dĭ </ts>
               <ts e="T458" id="Seg_1877" n="e" s="T457">koʔbdo </ts>
               <ts e="T459" id="Seg_1879" n="e" s="T458">nuʔməluʔpi. </ts>
               <ts e="T460" id="Seg_1881" n="e" s="T459">Šobi. </ts>
               <ts e="T461" id="Seg_1883" n="e" s="T460">Dĭgəttə </ts>
               <ts e="T462" id="Seg_1885" n="e" s="T461">nüke </ts>
               <ts e="T463" id="Seg_1887" n="e" s="T462">(šo-) </ts>
               <ts e="T464" id="Seg_1889" n="e" s="T463">turanə </ts>
               <ts e="T465" id="Seg_1891" n="e" s="T464">šobi, </ts>
               <ts e="T466" id="Seg_1893" n="e" s="T465">koʔbdo </ts>
               <ts e="T467" id="Seg_1895" n="e" s="T466">naga </ts>
               <ts e="T468" id="Seg_1897" n="e" s="T467">i </ts>
               <ts e="T469" id="Seg_1899" n="e" s="T468">nʼi </ts>
               <ts e="T470" id="Seg_1901" n="e" s="T469">naga. </ts>
               <ts e="T471" id="Seg_1903" n="e" s="T470">Dĭgəttə </ts>
               <ts e="T472" id="Seg_1905" n="e" s="T471">nabəʔinə </ts>
               <ts e="T473" id="Seg_1907" n="e" s="T472">măndə. </ts>
               <ts e="T474" id="Seg_1909" n="e" s="T473">"Kangaʔ, </ts>
               <ts e="T475" id="Seg_1911" n="e" s="T474">măndərgaʔ, </ts>
               <ts e="T476" id="Seg_1913" n="e" s="T475">gijen </ts>
               <ts e="T477" id="Seg_1915" n="e" s="T476">koʔbdo!" </ts>
               <ts e="T478" id="Seg_1917" n="e" s="T477">Dĭ </ts>
               <ts e="T479" id="Seg_1919" n="e" s="T478">koʔbdo </ts>
               <ts e="T480" id="Seg_1921" n="e" s="T479">(nu-) </ts>
               <ts e="T481" id="Seg_1923" n="e" s="T480">nuʔməleʔ </ts>
               <ts e="T482" id="Seg_1925" n="e" s="T481">šonəga. </ts>
               <ts e="T483" id="Seg_1927" n="e" s="T482">Bünə </ts>
               <ts e="T484" id="Seg_1929" n="e" s="T483">šobi. </ts>
               <ts e="T485" id="Seg_1931" n="e" s="T484">"Bü, </ts>
               <ts e="T486" id="Seg_1933" n="e" s="T485">bü, </ts>
               <ts e="T487" id="Seg_1935" n="e" s="T486">šaʔlaʔ </ts>
               <ts e="T488" id="Seg_1937" n="e" s="T487">măna!" </ts>
               <ts e="T489" id="Seg_1939" n="e" s="T488">Dĭ </ts>
               <ts e="T490" id="Seg_1941" n="e" s="T489">măndə: </ts>
               <ts e="T491" id="Seg_1943" n="e" s="T490">"(Am-) </ts>
               <ts e="T492" id="Seg_1945" n="e" s="T491">Amnaʔ </ts>
               <ts e="T493" id="Seg_1947" n="e" s="T492">măn </ts>
               <ts e="T494" id="Seg_1949" n="e" s="T493">kiselʼbə". </ts>
               <ts e="T495" id="Seg_1951" n="e" s="T494">Dĭ </ts>
               <ts e="T496" id="Seg_1953" n="e" s="T495">ambi, </ts>
               <ts e="T497" id="Seg_1955" n="e" s="T496">dĭ </ts>
               <ts e="T498" id="Seg_1957" n="e" s="T497">dĭm </ts>
               <ts e="T499" id="Seg_1959" n="e" s="T498">kajluʔpi. </ts>
               <ts e="T500" id="Seg_1961" n="e" s="T499">Dĭgəttə </ts>
               <ts e="T501" id="Seg_1963" n="e" s="T500">nabəʔi </ts>
               <ts e="T502" id="Seg_1965" n="e" s="T501">nʼergölüʔpiʔi. </ts>
               <ts e="T503" id="Seg_1967" n="e" s="T502">Dĭ </ts>
               <ts e="T504" id="Seg_1969" n="e" s="T503">bar </ts>
               <ts e="T505" id="Seg_1971" n="e" s="T504">supsobi. </ts>
               <ts e="T506" id="Seg_1973" n="e" s="T505">Bazoʔ </ts>
               <ts e="T507" id="Seg_1975" n="e" s="T506">nuʔməlaʔbə. </ts>
               <ts e="T508" id="Seg_1977" n="e" s="T507">Nabəʔi </ts>
               <ts e="T509" id="Seg_1979" n="e" s="T508">bazoʔ </ts>
               <ts e="T510" id="Seg_1981" n="e" s="T509">nʼergölaʔbəʔjə. </ts>
               <ts e="T511" id="Seg_1983" n="e" s="T510">Dĭgəttə </ts>
               <ts e="T512" id="Seg_1985" n="e" s="T511">jablokdə </ts>
               <ts e="T513" id="Seg_1987" n="e" s="T512">măndə: </ts>
               <ts e="T514" id="Seg_1989" n="e" s="T513">"Šaʔlaːndə </ts>
               <ts e="T515" id="Seg_1991" n="e" s="T514">miʔnʼibeʔ!" </ts>
               <ts e="T516" id="Seg_1993" n="e" s="T515">"Amaʔ </ts>
               <ts e="T517" id="Seg_1995" n="e" s="T516">jablokă", </ts>
               <ts e="T518" id="Seg_1997" n="e" s="T517">dĭ </ts>
               <ts e="T519" id="Seg_1999" n="e" s="T518">ambi, </ts>
               <ts e="T520" id="Seg_2001" n="e" s="T519">dĭ </ts>
               <ts e="T521" id="Seg_2003" n="e" s="T520">dĭm </ts>
               <ts e="T522" id="Seg_2005" n="e" s="T521">šaʔlaːmbi. </ts>
               <ts e="T523" id="Seg_2007" n="e" s="T522">Dĭgəttə </ts>
               <ts e="T524" id="Seg_2009" n="e" s="T523">nabəʔi </ts>
               <ts e="T525" id="Seg_2011" n="e" s="T524">nʼergölüʔpiʔi </ts>
               <ts e="T526" id="Seg_2013" n="e" s="T525">dĭ. </ts>
               <ts e="T527" id="Seg_2015" n="e" s="T526">Dĭzeŋ </ts>
               <ts e="T528" id="Seg_2017" n="e" s="T527">bazoʔ </ts>
               <ts e="T529" id="Seg_2019" n="e" s="T528">nuʔməluʔpiʔi. </ts>
               <ts e="T530" id="Seg_2021" n="e" s="T529">Pʼešdə </ts>
               <ts e="T531" id="Seg_2023" n="e" s="T530">šobi. </ts>
               <ts e="T532" id="Seg_2025" n="e" s="T531">Pʼeš </ts>
               <ts e="T533" id="Seg_2027" n="e" s="T532">măndə: </ts>
               <ts e="T534" id="Seg_2029" n="e" s="T533">"Nabəʔi </ts>
               <ts e="T535" id="Seg_2031" n="e" s="T534">(a-) </ts>
               <ts e="T536" id="Seg_2033" n="e" s="T535">bar </ts>
               <ts e="T537" id="Seg_2035" n="e" s="T536">(kabarliaʔiʔi)". </ts>
               <ts e="T538" id="Seg_2037" n="e" s="T537">Pʼeš </ts>
               <ts e="T539" id="Seg_2039" n="e" s="T538">măndə: </ts>
               <ts e="T540" id="Seg_2041" n="e" s="T539">"Amaʔ </ts>
               <ts e="T541" id="Seg_2043" n="e" s="T540">măn </ts>
               <ts e="T542" id="Seg_2045" n="e" s="T541">pirogdə!" </ts>
               <ts e="T543" id="Seg_2047" n="e" s="T542">Dĭ </ts>
               <ts e="T544" id="Seg_2049" n="e" s="T543">bar </ts>
               <ts e="T545" id="Seg_2051" n="e" s="T544">ambi, </ts>
               <ts e="T546" id="Seg_2053" n="e" s="T545">pʼešdə </ts>
               <ts e="T547" id="Seg_2055" n="e" s="T546">(še-) </ts>
               <ts e="T548" id="Seg_2057" n="e" s="T547">sʼaluʔpi. </ts>
               <ts e="T549" id="Seg_2059" n="e" s="T548">Nabəʔi </ts>
               <ts e="T550" id="Seg_2061" n="e" s="T549">nʼergölaʔpi, </ts>
               <ts e="T551" id="Seg_2063" n="e" s="T550">nʼergölaʔpi, </ts>
               <ts e="T552" id="Seg_2065" n="e" s="T551">gijendə </ts>
               <ts e="T553" id="Seg_2067" n="e" s="T552">naga. </ts>
               <ts e="T554" id="Seg_2069" n="e" s="T553">Parluʔpiʔi </ts>
               <ts e="T555" id="Seg_2071" n="e" s="T554">nükenə. </ts>
               <ts e="T556" id="Seg_2073" n="e" s="T555">A </ts>
               <ts e="T557" id="Seg_2075" n="e" s="T556">dĭzeŋ </ts>
               <ts e="T558" id="Seg_2077" n="e" s="T557">supsolaʔ </ts>
               <ts e="T559" id="Seg_2079" n="e" s="T558">šobiʔi </ts>
               <ts e="T560" id="Seg_2081" n="e" s="T559">maːndən. </ts>
               <ts e="T561" id="Seg_2083" n="e" s="T560">Šobiʔi. </ts>
               <ts e="T562" id="Seg_2085" n="e" s="T561">I </ts>
               <ts e="T563" id="Seg_2087" n="e" s="T562">abat, </ts>
               <ts e="T564" id="Seg_2089" n="e" s="T563">ijat </ts>
               <ts e="T565" id="Seg_2091" n="e" s="T564">šobiʔi. </ts>
               <ts e="T566" id="Seg_2093" n="e" s="T565">(Togonorzit-) </ts>
               <ts e="T567" id="Seg_2095" n="e" s="T566">Togonorgəʔ. </ts>
               <ts e="T568" id="Seg_2097" n="e" s="T567">Bar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T230" id="Seg_2098" s="T227">PKZ_196X_Ducks_flk.001 (001)</ta>
            <ta e="T237" id="Seg_2099" s="T230">PKZ_196X_Ducks_flk.002 (002)</ta>
            <ta e="T240" id="Seg_2100" s="T237">PKZ_196X_Ducks_flk.003 (003)</ta>
            <ta e="T242" id="Seg_2101" s="T240">PKZ_196X_Ducks_flk.004 (004)</ta>
            <ta e="T249" id="Seg_2102" s="T242">PKZ_196X_Ducks_flk.005 (005)</ta>
            <ta e="T254" id="Seg_2103" s="T249">PKZ_196X_Ducks_flk.006 (006)</ta>
            <ta e="T256" id="Seg_2104" s="T254">PKZ_196X_Ducks_flk.007 (007)</ta>
            <ta e="T260" id="Seg_2105" s="T256">PKZ_196X_Ducks_flk.008 (008)</ta>
            <ta e="T263" id="Seg_2106" s="T260">PKZ_196X_Ducks_flk.009 (009)</ta>
            <ta e="T270" id="Seg_2107" s="T263">PKZ_196X_Ducks_flk.010 (010)</ta>
            <ta e="T274" id="Seg_2108" s="T270">PKZ_196X_Ducks_flk.011 (011)</ta>
            <ta e="T278" id="Seg_2109" s="T274">PKZ_196X_Ducks_flk.012 (012)</ta>
            <ta e="T282" id="Seg_2110" s="T278">PKZ_196X_Ducks_flk.013 (013)</ta>
            <ta e="T283" id="Seg_2111" s="T282">PKZ_196X_Ducks_flk.014 (014)</ta>
            <ta e="T285" id="Seg_2112" s="T283">PKZ_196X_Ducks_flk.015 (015)</ta>
            <ta e="T287" id="Seg_2113" s="T285">PKZ_196X_Ducks_flk.016 (016)</ta>
            <ta e="T294" id="Seg_2114" s="T287">PKZ_196X_Ducks_flk.017 (017)</ta>
            <ta e="T298" id="Seg_2115" s="T294">PKZ_196X_Ducks_flk.018 (018) </ta>
            <ta e="T300" id="Seg_2116" s="T298">PKZ_196X_Ducks_flk.019 (020)</ta>
            <ta e="T307" id="Seg_2117" s="T300">PKZ_196X_Ducks_flk.020 (021)</ta>
            <ta e="T308" id="Seg_2118" s="T307">PKZ_196X_Ducks_flk.021 (022)</ta>
            <ta e="T310" id="Seg_2119" s="T308">PKZ_196X_Ducks_flk.022 (023)</ta>
            <ta e="T316" id="Seg_2120" s="T310">PKZ_196X_Ducks_flk.023 (024)</ta>
            <ta e="T319" id="Seg_2121" s="T316">PKZ_196X_Ducks_flk.024 (025)</ta>
            <ta e="T323" id="Seg_2122" s="T319">PKZ_196X_Ducks_flk.025 (026)</ta>
            <ta e="T327" id="Seg_2123" s="T323">PKZ_196X_Ducks_flk.026 (027)</ta>
            <ta e="T333" id="Seg_2124" s="T327">PKZ_196X_Ducks_flk.027 (028)</ta>
            <ta e="T338" id="Seg_2125" s="T333">PKZ_196X_Ducks_flk.028 (029)</ta>
            <ta e="T340" id="Seg_2126" s="T338">PKZ_196X_Ducks_flk.029 (030)</ta>
            <ta e="T342" id="Seg_2127" s="T340">PKZ_196X_Ducks_flk.030 (031)</ta>
            <ta e="T347" id="Seg_2128" s="T342">PKZ_196X_Ducks_flk.031 (032)</ta>
            <ta e="T350" id="Seg_2129" s="T347">PKZ_196X_Ducks_flk.032 (033)</ta>
            <ta e="T359" id="Seg_2130" s="T350">PKZ_196X_Ducks_flk.033 (034)</ta>
            <ta e="T364" id="Seg_2131" s="T359">PKZ_196X_Ducks_flk.034 (035)</ta>
            <ta e="T368" id="Seg_2132" s="T364">PKZ_196X_Ducks_flk.035 (036)</ta>
            <ta e="T370" id="Seg_2133" s="T368">PKZ_196X_Ducks_flk.036 (037)</ta>
            <ta e="T373" id="Seg_2134" s="T370">PKZ_196X_Ducks_flk.037 (038)</ta>
            <ta e="T376" id="Seg_2135" s="T373">PKZ_196X_Ducks_flk.038 (039)</ta>
            <ta e="T377" id="Seg_2136" s="T376">PKZ_196X_Ducks_flk.039 (040)</ta>
            <ta e="T381" id="Seg_2137" s="T377">PKZ_196X_Ducks_flk.040 (041)</ta>
            <ta e="T385" id="Seg_2138" s="T381">PKZ_196X_Ducks_flk.041 (042)</ta>
            <ta e="T390" id="Seg_2139" s="T385">PKZ_196X_Ducks_flk.042 (043)</ta>
            <ta e="T392" id="Seg_2140" s="T390">PKZ_196X_Ducks_flk.043 (044)</ta>
            <ta e="T396" id="Seg_2141" s="T392">PKZ_196X_Ducks_flk.044 (045)</ta>
            <ta e="T399" id="Seg_2142" s="T396">PKZ_196X_Ducks_flk.045 (046)</ta>
            <ta e="T403" id="Seg_2143" s="T399">PKZ_196X_Ducks_flk.046 (047)</ta>
            <ta e="T412" id="Seg_2144" s="T403">PKZ_196X_Ducks_flk.047 (048)</ta>
            <ta e="T418" id="Seg_2145" s="T412">PKZ_196X_Ducks_flk.048 (050)</ta>
            <ta e="T422" id="Seg_2146" s="T418">PKZ_196X_Ducks_flk.049 (051)</ta>
            <ta e="T427" id="Seg_2147" s="T422">PKZ_196X_Ducks_flk.050 (052)</ta>
            <ta e="T431" id="Seg_2148" s="T427">PKZ_196X_Ducks_flk.051 (053)</ta>
            <ta e="T435" id="Seg_2149" s="T431">PKZ_196X_Ducks_flk.052 (054)</ta>
            <ta e="T438" id="Seg_2150" s="T435">PKZ_196X_Ducks_flk.053 (055)</ta>
            <ta e="T443" id="Seg_2151" s="T438">PKZ_196X_Ducks_flk.054 (056)</ta>
            <ta e="T446" id="Seg_2152" s="T443">PKZ_196X_Ducks_flk.055 (057)</ta>
            <ta e="T448" id="Seg_2153" s="T446">PKZ_196X_Ducks_flk.056 (058)</ta>
            <ta e="T451" id="Seg_2154" s="T448">PKZ_196X_Ducks_flk.057 (059)</ta>
            <ta e="T459" id="Seg_2155" s="T451">PKZ_196X_Ducks_flk.058 (060)</ta>
            <ta e="T460" id="Seg_2156" s="T459">PKZ_196X_Ducks_flk.059 (061)</ta>
            <ta e="T470" id="Seg_2157" s="T460">PKZ_196X_Ducks_flk.060 (062)</ta>
            <ta e="T473" id="Seg_2158" s="T470">PKZ_196X_Ducks_flk.061 (063)</ta>
            <ta e="T477" id="Seg_2159" s="T473">PKZ_196X_Ducks_flk.062 (064)</ta>
            <ta e="T482" id="Seg_2160" s="T477">PKZ_196X_Ducks_flk.063 (065)</ta>
            <ta e="T484" id="Seg_2161" s="T482">PKZ_196X_Ducks_flk.064 (066)</ta>
            <ta e="T488" id="Seg_2162" s="T484">PKZ_196X_Ducks_flk.065 (067)</ta>
            <ta e="T494" id="Seg_2163" s="T488">PKZ_196X_Ducks_flk.066 (068)</ta>
            <ta e="T499" id="Seg_2164" s="T494">PKZ_196X_Ducks_flk.067 (069)</ta>
            <ta e="T502" id="Seg_2165" s="T499">PKZ_196X_Ducks_flk.068 (070)</ta>
            <ta e="T505" id="Seg_2166" s="T502">PKZ_196X_Ducks_flk.069 (071)</ta>
            <ta e="T507" id="Seg_2167" s="T505">PKZ_196X_Ducks_flk.070 (072)</ta>
            <ta e="T510" id="Seg_2168" s="T507">PKZ_196X_Ducks_flk.071 (073)</ta>
            <ta e="T515" id="Seg_2169" s="T510">PKZ_196X_Ducks_flk.072 (074) </ta>
            <ta e="T522" id="Seg_2170" s="T515">PKZ_196X_Ducks_flk.073 (076)</ta>
            <ta e="T526" id="Seg_2171" s="T522">PKZ_196X_Ducks_flk.074 (077)</ta>
            <ta e="T529" id="Seg_2172" s="T526">PKZ_196X_Ducks_flk.075 (078)</ta>
            <ta e="T531" id="Seg_2173" s="T529">PKZ_196X_Ducks_flk.076 (079)</ta>
            <ta e="T537" id="Seg_2174" s="T531">PKZ_196X_Ducks_flk.077 (080) </ta>
            <ta e="T542" id="Seg_2175" s="T537">PKZ_196X_Ducks_flk.078 (082) </ta>
            <ta e="T548" id="Seg_2176" s="T542">PKZ_196X_Ducks_flk.079 (084)</ta>
            <ta e="T553" id="Seg_2177" s="T548">PKZ_196X_Ducks_flk.080 (085)</ta>
            <ta e="T555" id="Seg_2178" s="T553">PKZ_196X_Ducks_flk.081 (086)</ta>
            <ta e="T560" id="Seg_2179" s="T555">PKZ_196X_Ducks_flk.082 (087)</ta>
            <ta e="T561" id="Seg_2180" s="T560">PKZ_196X_Ducks_flk.083 (088)</ta>
            <ta e="T565" id="Seg_2181" s="T561">PKZ_196X_Ducks_flk.084 (089)</ta>
            <ta e="T567" id="Seg_2182" s="T565">PKZ_196X_Ducks_flk.085 (090)</ta>
            <ta e="T568" id="Seg_2183" s="T567">PKZ_196X_Ducks_flk.086 (091)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T230" id="Seg_2184" s="T227">Amnobiʔi nüke büzʼezʼiʔ. </ta>
            <ta e="T237" id="Seg_2185" s="T230">Dĭzeŋ (bɨl- ko-) ibi koʔbdo i nʼi. </ta>
            <ta e="T240" id="Seg_2186" s="T237">Dĭzeŋ togonorzittə kallaʔbəʔjə. </ta>
            <ta e="T242" id="Seg_2187" s="T240">"Măndot nʼim. </ta>
            <ta e="T249" id="Seg_2188" s="T242">(M- mi-) Măn tănan ilem plat, kuvas". </ta>
            <ta e="T254" id="Seg_2189" s="T249">Kambiʔi, a koʔbdot nʼibə ibi. </ta>
            <ta e="T256" id="Seg_2190" s="T254">Nʼiʔnenə amnolbi. </ta>
            <ta e="T260" id="Seg_2191" s="T256">Noʔgən, a bostə nuʔməluʔpi. </ta>
            <ta e="T263" id="Seg_2192" s="T260">Dön nabəʔi nanʼergöluʔpi. </ta>
            <ta e="T270" id="Seg_2193" s="T263">Dĭ nʼim (i- i-) iluʔpiʔi, (kunnaːlbiʔi), kumbiʔi. </ta>
            <ta e="T274" id="Seg_2194" s="T270">Koʔbdo (suʔmileʔ-) nuʔmileʔ šobi. </ta>
            <ta e="T278" id="Seg_2195" s="T274">Kuliot: naga, naga nʼi. </ta>
            <ta e="T282" id="Seg_2196" s="T278">Dibər-döbər nuʔməleʔbi, dĭgəttə kambi. </ta>
            <ta e="T283" id="Seg_2197" s="T282">Nuʔməluʔpi. </ta>
            <ta e="T285" id="Seg_2198" s="T283">Nuʔməleʔbə, nuʔməleʔbə. </ta>
            <ta e="T287" id="Seg_2199" s="T285">Pʼeš nulaʔbə. </ta>
            <ta e="T294" id="Seg_2200" s="T287">"Tăn ej kubiam, (gije-) gijen nʼergölüʔpiʔi nabəʔi?" </ta>
            <ta e="T298" id="Seg_2201" s="T294">Dĭ măndə: "Pirog amaʔ". </ta>
            <ta e="T300" id="Seg_2202" s="T298">"Ej budəj! </ta>
            <ta e="T307" id="Seg_2203" s="T300">Măn abandə budəj ige, da ej amniam". </ta>
            <ta e="T308" id="Seg_2204" s="T307">Nuʔməluʔpi. </ta>
            <ta e="T310" id="Seg_2205" s="T308">Dĭgəttə šonəga. </ta>
            <ta e="T316" id="Seg_2206" s="T310">Dĭgəttə pʼeš (ĭmbi dĭʔnə) ej mămbi. </ta>
            <ta e="T319" id="Seg_2207" s="T316">Dĭ nuʔməluʔpi bazoʔ. </ta>
            <ta e="T323" id="Seg_2208" s="T319">Dĭgəttə šonəga, bü mʼaŋŋaʔbə. </ta>
            <ta e="T327" id="Seg_2209" s="T323">"Amnaʔ, kiselʼ măn sutsiʔ!" </ta>
            <ta e="T333" id="Seg_2210" s="T327">"Măn abandə ej dĭrgitdə, ej amniam". </ta>
            <ta e="T338" id="Seg_2211" s="T333">Dĭ dĭʔnə ĭmbidə ej mămbi. </ta>
            <ta e="T340" id="Seg_2212" s="T338">Dĭgəttə nuʔməlaʔbə. </ta>
            <ta e="T342" id="Seg_2213" s="T340">Nulaʔbə jablokă. </ta>
            <ta e="T347" id="Seg_2214" s="T342">"Kubial, gibər (nam-) nabəʔi nʼergölüʔpiʔi?" </ta>
            <ta e="T350" id="Seg_2215" s="T347">"Amnaʔ măn jablokă!" </ta>
            <ta e="T359" id="Seg_2216" s="T350">"Măn abanə iššo jakšə (jablok), i to ej amniam". </ta>
            <ta e="T364" id="Seg_2217" s="T359">I dĭʔnə ĭmbidə ej nörbəbi. </ta>
            <ta e="T368" id="Seg_2218" s="T364">Dĭgəttə (m-) kambi, kambi. </ta>
            <ta e="T370" id="Seg_2219" s="T368">Nulaʔbə tura. </ta>
            <ta e="T373" id="Seg_2220" s="T370">Dĭ turanə šübi. </ta>
            <ta e="T376" id="Seg_2221" s="T373">Dĭn nüke amnolaʔbə. </ta>
            <ta e="T377" id="Seg_2222" s="T376">Jererleʔbə. </ta>
            <ta e="T381" id="Seg_2223" s="T377">Nʼi (ige) i (dĭn). </ta>
            <ta e="T385" id="Seg_2224" s="T381">"Jerereʔ, ĭmbi ileʔ šobial?" </ta>
            <ta e="T390" id="Seg_2225" s="T385">"Da măn mĭmbiem da kănnaːmbiam. </ta>
            <ta e="T392" id="Seg_2226" s="T390">Ejümzittə šobiam". </ta>
            <ta e="T396" id="Seg_2227" s="T392">"Amnaʔ, jerereʔ kudʼelʼə măna". </ta>
            <ta e="T399" id="Seg_2228" s="T396">Amnəbi, dĭgəttə jererlaʔbə. </ta>
            <ta e="T403" id="Seg_2229" s="T399">A nʼit, nʼiʔnen amnolaʔbə. </ta>
            <ta e="T412" id="Seg_2230" s="T403">Dĭgəttə tumo supsobi, măndə: "Koʔbdo, tănan nüke nendleʔbə moltʼa. </ta>
            <ta e="T418" id="Seg_2231" s="T412">I dĭn tănan nendləj, (ibə) amnəj. </ta>
            <ta e="T422" id="Seg_2232" s="T418">(Elendə) bar (amnolləj) bostə. </ta>
            <ta e="T427" id="Seg_2233" s="T422">Deʔ măna amzittə (m-) kaška". </ta>
            <ta e="T431" id="Seg_2234" s="T427">Dĭ dĭʔnə (mĭm-) mĭbi. </ta>
            <ta e="T435" id="Seg_2235" s="T431">"Kanaʔ, a măn jererləm. </ta>
            <ta e="T438" id="Seg_2236" s="T435">It bostə (nʼin)". </ta>
            <ta e="T443" id="Seg_2237" s="T438">Dĭ kabarlaʔ ibi i nuʔməluʔpi. </ta>
            <ta e="T446" id="Seg_2238" s="T443">Dĭ nüke šoləj. </ta>
            <ta e="T448" id="Seg_2239" s="T446">"Tăn jererleʔbəl?" </ta>
            <ta e="T451" id="Seg_2240" s="T448">Dĭ măndə: "Jererleʔbəm". </ta>
            <ta e="T459" id="Seg_2241" s="T451">Dĭgəttə bazoʔ kalləj moltʼa nendəsʼtə, dĭ koʔbdo nuʔməluʔpi. </ta>
            <ta e="T460" id="Seg_2242" s="T459">Šobi. </ta>
            <ta e="T470" id="Seg_2243" s="T460">Dĭgəttə nüke (šo-) turanə šobi, koʔbdo naga i nʼi naga. </ta>
            <ta e="T473" id="Seg_2244" s="T470">Dĭgəttə nabəʔinə măndə. </ta>
            <ta e="T477" id="Seg_2245" s="T473">"Kangaʔ, măndərgaʔ, gijen koʔbdo!" </ta>
            <ta e="T482" id="Seg_2246" s="T477">Dĭ koʔbdo (nu-) nuʔməleʔ šonəga. </ta>
            <ta e="T484" id="Seg_2247" s="T482">Bünə šobi. </ta>
            <ta e="T488" id="Seg_2248" s="T484">"Bü, bü, šaʔlaʔ măna!" </ta>
            <ta e="T494" id="Seg_2249" s="T488">Dĭ măndə: "(Am-) Amnaʔ măn kiselʼbə". </ta>
            <ta e="T499" id="Seg_2250" s="T494">Dĭ ambi, dĭ dĭm kajluʔpi. </ta>
            <ta e="T502" id="Seg_2251" s="T499">Dĭgəttə nabəʔi nʼergölüʔpiʔi. </ta>
            <ta e="T505" id="Seg_2252" s="T502">Dĭ bar supsobi. </ta>
            <ta e="T507" id="Seg_2253" s="T505">Bazoʔ nuʔməlaʔbə. </ta>
            <ta e="T510" id="Seg_2254" s="T507">Nabəʔi bazoʔ nʼergölaʔbəʔjə. </ta>
            <ta e="T515" id="Seg_2255" s="T510">Dĭgəttə jablokdə măndə: "Šaʔlaːndə miʔnʼibeʔ!" </ta>
            <ta e="T522" id="Seg_2256" s="T515">"Amaʔ jablokă", dĭ ambi, dĭ dĭm šaʔlaːmbi. </ta>
            <ta e="T526" id="Seg_2257" s="T522">Dĭgəttə nabəʔi nʼergölüʔpiʔi dĭ. </ta>
            <ta e="T529" id="Seg_2258" s="T526">Dĭzeŋ bazoʔ nuʔməluʔpiʔi. </ta>
            <ta e="T531" id="Seg_2259" s="T529">Pʼešdə šobi. </ta>
            <ta e="T537" id="Seg_2260" s="T531">Pʼeš măndə: "Nabəʔi (a-) bar (kabarliaʔiʔi)". </ta>
            <ta e="T542" id="Seg_2261" s="T537">Pʼeš măndə: "Amaʔ măn pirogdə!" </ta>
            <ta e="T548" id="Seg_2262" s="T542">Dĭ bar ambi, pʼešdə (še-) sʼaluʔpi. </ta>
            <ta e="T553" id="Seg_2263" s="T548">Nabəʔi nʼergölaʔpi, nʼergölaʔpi, gijendə naga. </ta>
            <ta e="T555" id="Seg_2264" s="T553">Parluʔpiʔi nükenə. </ta>
            <ta e="T560" id="Seg_2265" s="T555">A dĭzeŋ supsolaʔ šobiʔi maːndən. </ta>
            <ta e="T561" id="Seg_2266" s="T560">Šobiʔi. </ta>
            <ta e="T565" id="Seg_2267" s="T561">I abat, ijat šobiʔi. </ta>
            <ta e="T567" id="Seg_2268" s="T565">(Togonorzit-) Togonorgəʔ. </ta>
            <ta e="T568" id="Seg_2269" s="T567">Bar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T228" id="Seg_2270" s="T227">amno-bi-ʔi</ta>
            <ta e="T229" id="Seg_2271" s="T228">nüke</ta>
            <ta e="T230" id="Seg_2272" s="T229">büzʼe-zʼiʔ</ta>
            <ta e="T231" id="Seg_2273" s="T230">dĭ-zeŋ</ta>
            <ta e="T234" id="Seg_2274" s="T233">i-bi</ta>
            <ta e="T235" id="Seg_2275" s="T234">koʔbdo</ta>
            <ta e="T236" id="Seg_2276" s="T235">i</ta>
            <ta e="T237" id="Seg_2277" s="T236">nʼi</ta>
            <ta e="T238" id="Seg_2278" s="T237">dĭ-zeŋ</ta>
            <ta e="T239" id="Seg_2279" s="T238">togonor-zittə</ta>
            <ta e="T240" id="Seg_2280" s="T239">kal-laʔbə-ʔjə</ta>
            <ta e="T241" id="Seg_2281" s="T240">măndo-t</ta>
            <ta e="T242" id="Seg_2282" s="T241">nʼi-m</ta>
            <ta e="T245" id="Seg_2283" s="T244">măn</ta>
            <ta e="T246" id="Seg_2284" s="T245">tănan</ta>
            <ta e="T247" id="Seg_2285" s="T246">i-le-m</ta>
            <ta e="T248" id="Seg_2286" s="T247">plat</ta>
            <ta e="T249" id="Seg_2287" s="T248">kuvas</ta>
            <ta e="T250" id="Seg_2288" s="T249">kam-bi-ʔi</ta>
            <ta e="T251" id="Seg_2289" s="T250">a</ta>
            <ta e="T252" id="Seg_2290" s="T251">koʔbdo-t</ta>
            <ta e="T253" id="Seg_2291" s="T252">nʼi-bə</ta>
            <ta e="T254" id="Seg_2292" s="T253">i-bi</ta>
            <ta e="T255" id="Seg_2293" s="T254">nʼiʔnenə</ta>
            <ta e="T256" id="Seg_2294" s="T255">amnol-bi</ta>
            <ta e="T257" id="Seg_2295" s="T256">noʔ-gən</ta>
            <ta e="T258" id="Seg_2296" s="T257">a</ta>
            <ta e="T259" id="Seg_2297" s="T258">bos-tə</ta>
            <ta e="T260" id="Seg_2298" s="T259">nuʔmə-luʔ-pi</ta>
            <ta e="T261" id="Seg_2299" s="T260">dön</ta>
            <ta e="T262" id="Seg_2300" s="T261">nabə-ʔi</ta>
            <ta e="T263" id="Seg_2301" s="T262">na-nʼergö-luʔ-pi</ta>
            <ta e="T264" id="Seg_2302" s="T263">dĭ</ta>
            <ta e="T265" id="Seg_2303" s="T264">nʼi-m</ta>
            <ta e="T268" id="Seg_2304" s="T267">i-luʔ-pi-ʔi</ta>
            <ta e="T269" id="Seg_2305" s="T268">kun-naːl-bi-ʔi</ta>
            <ta e="T270" id="Seg_2306" s="T269">kum-bi-ʔi</ta>
            <ta e="T271" id="Seg_2307" s="T270">koʔbdo</ta>
            <ta e="T273" id="Seg_2308" s="T272">nuʔmi-leʔ</ta>
            <ta e="T274" id="Seg_2309" s="T273">šo-bi</ta>
            <ta e="T275" id="Seg_2310" s="T274">ku-lio-t</ta>
            <ta e="T276" id="Seg_2311" s="T275">naga</ta>
            <ta e="T277" id="Seg_2312" s="T276">naga</ta>
            <ta e="T278" id="Seg_2313" s="T277">nʼi</ta>
            <ta e="T279" id="Seg_2314" s="T278">dibər_döbər</ta>
            <ta e="T280" id="Seg_2315" s="T279">nuʔmə-leʔ-bi</ta>
            <ta e="T281" id="Seg_2316" s="T280">dĭgəttə</ta>
            <ta e="T282" id="Seg_2317" s="T281">kam-bi</ta>
            <ta e="T283" id="Seg_2318" s="T282">nuʔmə-luʔ-pi</ta>
            <ta e="T284" id="Seg_2319" s="T283">nuʔmə-leʔbə</ta>
            <ta e="T285" id="Seg_2320" s="T284">nuʔmə-leʔbə</ta>
            <ta e="T286" id="Seg_2321" s="T285">pʼeš</ta>
            <ta e="T287" id="Seg_2322" s="T286">nu-laʔbə</ta>
            <ta e="T288" id="Seg_2323" s="T287">tăn</ta>
            <ta e="T289" id="Seg_2324" s="T288">ej</ta>
            <ta e="T290" id="Seg_2325" s="T289">ku-bia-m</ta>
            <ta e="T292" id="Seg_2326" s="T291">gijen</ta>
            <ta e="T293" id="Seg_2327" s="T292">nʼergö-lüʔ-pi-ʔi</ta>
            <ta e="T294" id="Seg_2328" s="T293">nabə-ʔi</ta>
            <ta e="T295" id="Seg_2329" s="T294">dĭ</ta>
            <ta e="T296" id="Seg_2330" s="T295">măn-də</ta>
            <ta e="T297" id="Seg_2331" s="T296">pirog</ta>
            <ta e="T298" id="Seg_2332" s="T297">am-a-ʔ</ta>
            <ta e="T299" id="Seg_2333" s="T298">ej</ta>
            <ta e="T300" id="Seg_2334" s="T299">budəj</ta>
            <ta e="T301" id="Seg_2335" s="T300">măn</ta>
            <ta e="T302" id="Seg_2336" s="T301">aba-ndə</ta>
            <ta e="T303" id="Seg_2337" s="T302">budəj</ta>
            <ta e="T304" id="Seg_2338" s="T303">i-ge</ta>
            <ta e="T305" id="Seg_2339" s="T304">da</ta>
            <ta e="T306" id="Seg_2340" s="T305">ej</ta>
            <ta e="T307" id="Seg_2341" s="T306">am-nia-m</ta>
            <ta e="T308" id="Seg_2342" s="T307">nuʔmə-luʔ-pi</ta>
            <ta e="T309" id="Seg_2343" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_2344" s="T309">šonə-ga</ta>
            <ta e="T311" id="Seg_2345" s="T310">dĭgəttə</ta>
            <ta e="T312" id="Seg_2346" s="T311">pʼeš</ta>
            <ta e="T313" id="Seg_2347" s="T312">ĭmbi</ta>
            <ta e="T314" id="Seg_2348" s="T313">dĭʔ-nə</ta>
            <ta e="T315" id="Seg_2349" s="T314">ej</ta>
            <ta e="T316" id="Seg_2350" s="T315">măm-bi</ta>
            <ta e="T317" id="Seg_2351" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_2352" s="T317">nuʔmə-luʔ-pi</ta>
            <ta e="T319" id="Seg_2353" s="T318">bazoʔ</ta>
            <ta e="T320" id="Seg_2354" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2355" s="T320">šonə-ga</ta>
            <ta e="T322" id="Seg_2356" s="T321">bü</ta>
            <ta e="T323" id="Seg_2357" s="T322">mʼaŋ-ŋaʔbə</ta>
            <ta e="T324" id="Seg_2358" s="T323">amna-ʔ</ta>
            <ta e="T325" id="Seg_2359" s="T324">kiselʼ</ta>
            <ta e="T326" id="Seg_2360" s="T325">măn</ta>
            <ta e="T327" id="Seg_2361" s="T326">sut-siʔ</ta>
            <ta e="T328" id="Seg_2362" s="T327">măn</ta>
            <ta e="T329" id="Seg_2363" s="T328">aba-ndə</ta>
            <ta e="T330" id="Seg_2364" s="T329">ej</ta>
            <ta e="T331" id="Seg_2365" s="T330">dĭrgit=də</ta>
            <ta e="T332" id="Seg_2366" s="T331">ej</ta>
            <ta e="T333" id="Seg_2367" s="T332">am-nia-m</ta>
            <ta e="T334" id="Seg_2368" s="T333">dĭ</ta>
            <ta e="T335" id="Seg_2369" s="T334">dĭʔ-nə</ta>
            <ta e="T336" id="Seg_2370" s="T335">ĭmbi=də</ta>
            <ta e="T337" id="Seg_2371" s="T336">ej</ta>
            <ta e="T338" id="Seg_2372" s="T337">măm-bi</ta>
            <ta e="T339" id="Seg_2373" s="T338">dĭgəttə</ta>
            <ta e="T340" id="Seg_2374" s="T339">nuʔmə-laʔbə</ta>
            <ta e="T341" id="Seg_2375" s="T340">nu-laʔbə</ta>
            <ta e="T342" id="Seg_2376" s="T341">jablokă</ta>
            <ta e="T343" id="Seg_2377" s="T342">ku-bia-l</ta>
            <ta e="T344" id="Seg_2378" s="T343">gibər</ta>
            <ta e="T346" id="Seg_2379" s="T345">nabə-ʔi</ta>
            <ta e="T347" id="Seg_2380" s="T346">nʼergö-lüʔ-pi-ʔi</ta>
            <ta e="T348" id="Seg_2381" s="T347">amna-ʔ</ta>
            <ta e="T349" id="Seg_2382" s="T348">măn</ta>
            <ta e="T350" id="Seg_2383" s="T349">jablokă</ta>
            <ta e="T351" id="Seg_2384" s="T350">măn</ta>
            <ta e="T352" id="Seg_2385" s="T351">aba-nə</ta>
            <ta e="T353" id="Seg_2386" s="T352">iššo</ta>
            <ta e="T354" id="Seg_2387" s="T353">jakšə</ta>
            <ta e="T355" id="Seg_2388" s="T354">jablok</ta>
            <ta e="T356" id="Seg_2389" s="T355">i</ta>
            <ta e="T357" id="Seg_2390" s="T356">to</ta>
            <ta e="T358" id="Seg_2391" s="T357">ej</ta>
            <ta e="T359" id="Seg_2392" s="T358">am-nia-m</ta>
            <ta e="T360" id="Seg_2393" s="T359">i</ta>
            <ta e="T361" id="Seg_2394" s="T360">dĭʔ-nə</ta>
            <ta e="T362" id="Seg_2395" s="T361">ĭmbi=də</ta>
            <ta e="T363" id="Seg_2396" s="T362">ej</ta>
            <ta e="T364" id="Seg_2397" s="T363">nörbə-bi</ta>
            <ta e="T365" id="Seg_2398" s="T364">dĭgəttə</ta>
            <ta e="T367" id="Seg_2399" s="T366">kam-bi</ta>
            <ta e="T368" id="Seg_2400" s="T367">kam-bi</ta>
            <ta e="T369" id="Seg_2401" s="T368">nu-laʔbə</ta>
            <ta e="T370" id="Seg_2402" s="T369">tura</ta>
            <ta e="T371" id="Seg_2403" s="T370">dĭ</ta>
            <ta e="T372" id="Seg_2404" s="T371">tura-nə</ta>
            <ta e="T373" id="Seg_2405" s="T372">šü-bi</ta>
            <ta e="T374" id="Seg_2406" s="T373">dĭn</ta>
            <ta e="T375" id="Seg_2407" s="T374">nüke</ta>
            <ta e="T376" id="Seg_2408" s="T375">amno-laʔbə</ta>
            <ta e="T377" id="Seg_2409" s="T376">jerer-leʔbə</ta>
            <ta e="T378" id="Seg_2410" s="T377">nʼi</ta>
            <ta e="T379" id="Seg_2411" s="T378">i-ge</ta>
            <ta e="T380" id="Seg_2412" s="T379">i</ta>
            <ta e="T381" id="Seg_2413" s="T380">dĭn</ta>
            <ta e="T382" id="Seg_2414" s="T381">jerer-e-ʔ</ta>
            <ta e="T383" id="Seg_2415" s="T382">ĭmbi</ta>
            <ta e="T384" id="Seg_2416" s="T383">i-leʔ</ta>
            <ta e="T385" id="Seg_2417" s="T384">šo-bia-l</ta>
            <ta e="T386" id="Seg_2418" s="T385">da</ta>
            <ta e="T387" id="Seg_2419" s="T386">măn</ta>
            <ta e="T388" id="Seg_2420" s="T387">mĭm-bie-m</ta>
            <ta e="T389" id="Seg_2421" s="T388">da</ta>
            <ta e="T390" id="Seg_2422" s="T389">kăn-naːm-bia-m</ta>
            <ta e="T391" id="Seg_2423" s="T390">ejü-m-zittə</ta>
            <ta e="T392" id="Seg_2424" s="T391">šo-bia-m</ta>
            <ta e="T393" id="Seg_2425" s="T392">amna-ʔ</ta>
            <ta e="T394" id="Seg_2426" s="T393">jerer-e-ʔ</ta>
            <ta e="T395" id="Seg_2427" s="T394">kudʼelʼə</ta>
            <ta e="T396" id="Seg_2428" s="T395">măna</ta>
            <ta e="T397" id="Seg_2429" s="T396">amnə-bi</ta>
            <ta e="T398" id="Seg_2430" s="T397">dĭgəttə</ta>
            <ta e="T399" id="Seg_2431" s="T398">jerer-laʔbə</ta>
            <ta e="T400" id="Seg_2432" s="T399">a</ta>
            <ta e="T401" id="Seg_2433" s="T400">nʼi-t</ta>
            <ta e="T402" id="Seg_2434" s="T401">nʼiʔnen</ta>
            <ta e="T403" id="Seg_2435" s="T402">amno-laʔbə</ta>
            <ta e="T404" id="Seg_2436" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_2437" s="T404">tumo</ta>
            <ta e="T406" id="Seg_2438" s="T405">supso-bi</ta>
            <ta e="T407" id="Seg_2439" s="T406">măn-də</ta>
            <ta e="T408" id="Seg_2440" s="T407">koʔbdo</ta>
            <ta e="T409" id="Seg_2441" s="T408">tănan</ta>
            <ta e="T410" id="Seg_2442" s="T409">nüke</ta>
            <ta e="T411" id="Seg_2443" s="T410">nend-leʔbə</ta>
            <ta e="T412" id="Seg_2444" s="T411">moltʼa</ta>
            <ta e="T413" id="Seg_2445" s="T412">i</ta>
            <ta e="T414" id="Seg_2446" s="T413">dĭn</ta>
            <ta e="T415" id="Seg_2447" s="T414">tănan</ta>
            <ta e="T416" id="Seg_2448" s="T415">nend-lə-j</ta>
            <ta e="T417" id="Seg_2449" s="T416">ibə</ta>
            <ta e="T418" id="Seg_2450" s="T417">am-nə-j</ta>
            <ta e="T419" id="Seg_2451" s="T418">ele-ndə</ta>
            <ta e="T420" id="Seg_2452" s="T419">bar</ta>
            <ta e="T421" id="Seg_2453" s="T420">amnol-lə-j</ta>
            <ta e="T422" id="Seg_2454" s="T421">bos-tə</ta>
            <ta e="T423" id="Seg_2455" s="T422">de-ʔ</ta>
            <ta e="T424" id="Seg_2456" s="T423">măna</ta>
            <ta e="T425" id="Seg_2457" s="T424">am-zittə</ta>
            <ta e="T427" id="Seg_2458" s="T426">kaška</ta>
            <ta e="T428" id="Seg_2459" s="T427">dĭ</ta>
            <ta e="T429" id="Seg_2460" s="T428">dĭʔ-nə</ta>
            <ta e="T431" id="Seg_2461" s="T430">mĭ-bi</ta>
            <ta e="T432" id="Seg_2462" s="T431">kan-a-ʔ</ta>
            <ta e="T433" id="Seg_2463" s="T432">a</ta>
            <ta e="T434" id="Seg_2464" s="T433">măn</ta>
            <ta e="T435" id="Seg_2465" s="T434">jerer-lə-m</ta>
            <ta e="T436" id="Seg_2466" s="T435">i-t</ta>
            <ta e="T437" id="Seg_2467" s="T436">bos-tə</ta>
            <ta e="T438" id="Seg_2468" s="T437">nʼi-n</ta>
            <ta e="T439" id="Seg_2469" s="T438">dĭ</ta>
            <ta e="T440" id="Seg_2470" s="T439">kabar-laʔ</ta>
            <ta e="T441" id="Seg_2471" s="T440">i-bi</ta>
            <ta e="T442" id="Seg_2472" s="T441">i</ta>
            <ta e="T443" id="Seg_2473" s="T442">nuʔmə-luʔ-pi</ta>
            <ta e="T444" id="Seg_2474" s="T443">dĭ</ta>
            <ta e="T445" id="Seg_2475" s="T444">nüke</ta>
            <ta e="T446" id="Seg_2476" s="T445">šo-lə-j</ta>
            <ta e="T447" id="Seg_2477" s="T446">tăn</ta>
            <ta e="T448" id="Seg_2478" s="T447">jerer-leʔbə-l</ta>
            <ta e="T449" id="Seg_2479" s="T448">dĭ</ta>
            <ta e="T450" id="Seg_2480" s="T449">măn-də</ta>
            <ta e="T451" id="Seg_2481" s="T450">jerer-leʔbə-m</ta>
            <ta e="T452" id="Seg_2482" s="T451">dĭgəttə</ta>
            <ta e="T453" id="Seg_2483" s="T452">bazoʔ</ta>
            <ta e="T454" id="Seg_2484" s="T453">kal-lə-j</ta>
            <ta e="T455" id="Seg_2485" s="T454">moltʼa</ta>
            <ta e="T456" id="Seg_2486" s="T455">nendə-sʼtə</ta>
            <ta e="T457" id="Seg_2487" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_2488" s="T457">koʔbdo</ta>
            <ta e="T459" id="Seg_2489" s="T458">nuʔmə-luʔ-pi</ta>
            <ta e="T460" id="Seg_2490" s="T459">šo-bi</ta>
            <ta e="T461" id="Seg_2491" s="T460">dĭgəttə</ta>
            <ta e="T462" id="Seg_2492" s="T461">nüke</ta>
            <ta e="T464" id="Seg_2493" s="T463">tura-nə</ta>
            <ta e="T465" id="Seg_2494" s="T464">šo-bi</ta>
            <ta e="T466" id="Seg_2495" s="T465">koʔbdo</ta>
            <ta e="T467" id="Seg_2496" s="T466">naga</ta>
            <ta e="T468" id="Seg_2497" s="T467">i</ta>
            <ta e="T469" id="Seg_2498" s="T468">nʼi</ta>
            <ta e="T470" id="Seg_2499" s="T469">naga</ta>
            <ta e="T471" id="Seg_2500" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_2501" s="T471">nabə-ʔi-nə</ta>
            <ta e="T473" id="Seg_2502" s="T472">măn-də</ta>
            <ta e="T474" id="Seg_2503" s="T473">kan-gaʔ</ta>
            <ta e="T475" id="Seg_2504" s="T474">măndə-r-gaʔ</ta>
            <ta e="T476" id="Seg_2505" s="T475">gijen</ta>
            <ta e="T477" id="Seg_2506" s="T476">koʔbdo</ta>
            <ta e="T478" id="Seg_2507" s="T477">dĭ</ta>
            <ta e="T479" id="Seg_2508" s="T478">koʔbdo</ta>
            <ta e="T481" id="Seg_2509" s="T480">nuʔmə-leʔ</ta>
            <ta e="T482" id="Seg_2510" s="T481">šonə-ga</ta>
            <ta e="T483" id="Seg_2511" s="T482">bü-nə</ta>
            <ta e="T484" id="Seg_2512" s="T483">šo-bi</ta>
            <ta e="T485" id="Seg_2513" s="T484">bü</ta>
            <ta e="T486" id="Seg_2514" s="T485">bü</ta>
            <ta e="T487" id="Seg_2515" s="T486">šaʔl-a-ʔ</ta>
            <ta e="T488" id="Seg_2516" s="T487">măna</ta>
            <ta e="T489" id="Seg_2517" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_2518" s="T489">măn-də</ta>
            <ta e="T492" id="Seg_2519" s="T491">amna-ʔ</ta>
            <ta e="T493" id="Seg_2520" s="T492">măn</ta>
            <ta e="T494" id="Seg_2521" s="T493">kiselʼ-bə</ta>
            <ta e="T495" id="Seg_2522" s="T494">dĭ</ta>
            <ta e="T496" id="Seg_2523" s="T495">am-bi</ta>
            <ta e="T497" id="Seg_2524" s="T496">dĭ</ta>
            <ta e="T498" id="Seg_2525" s="T497">dĭ-m</ta>
            <ta e="T499" id="Seg_2526" s="T498">kaj-luʔ-pi</ta>
            <ta e="T500" id="Seg_2527" s="T499">dĭgəttə</ta>
            <ta e="T501" id="Seg_2528" s="T500">nabə-ʔi</ta>
            <ta e="T502" id="Seg_2529" s="T501">nʼergö-lüʔ-pi-ʔi</ta>
            <ta e="T503" id="Seg_2530" s="T502">dĭ</ta>
            <ta e="T504" id="Seg_2531" s="T503">bar</ta>
            <ta e="T505" id="Seg_2532" s="T504">supso-bi</ta>
            <ta e="T506" id="Seg_2533" s="T505">bazoʔ</ta>
            <ta e="T507" id="Seg_2534" s="T506">nuʔmə-laʔbə</ta>
            <ta e="T508" id="Seg_2535" s="T507">nabə-ʔi</ta>
            <ta e="T509" id="Seg_2536" s="T508">bazoʔ</ta>
            <ta e="T510" id="Seg_2537" s="T509">nʼergö-laʔbə-ʔjə</ta>
            <ta e="T511" id="Seg_2538" s="T510">dĭgəttə</ta>
            <ta e="T512" id="Seg_2539" s="T511">jablok-də</ta>
            <ta e="T513" id="Seg_2540" s="T512">măn-də</ta>
            <ta e="T514" id="Seg_2541" s="T513">šaʔ-laːn-də</ta>
            <ta e="T515" id="Seg_2542" s="T514">miʔnʼibeʔ</ta>
            <ta e="T516" id="Seg_2543" s="T515">am-a-ʔ</ta>
            <ta e="T517" id="Seg_2544" s="T516">jablokă</ta>
            <ta e="T518" id="Seg_2545" s="T517">dĭ</ta>
            <ta e="T519" id="Seg_2546" s="T518">am-bi</ta>
            <ta e="T520" id="Seg_2547" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_2548" s="T520">dĭ-m</ta>
            <ta e="T522" id="Seg_2549" s="T521">šaʔ-laːm-bi</ta>
            <ta e="T523" id="Seg_2550" s="T522">dĭgəttə</ta>
            <ta e="T524" id="Seg_2551" s="T523">nabə-ʔi</ta>
            <ta e="T525" id="Seg_2552" s="T524">nʼergö-lüʔ-pi-ʔi</ta>
            <ta e="T526" id="Seg_2553" s="T525">dĭ</ta>
            <ta e="T527" id="Seg_2554" s="T526">dĭ-zeŋ</ta>
            <ta e="T528" id="Seg_2555" s="T527">bazoʔ</ta>
            <ta e="T529" id="Seg_2556" s="T528">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T530" id="Seg_2557" s="T529">pʼeš-də</ta>
            <ta e="T531" id="Seg_2558" s="T530">šo-bi</ta>
            <ta e="T532" id="Seg_2559" s="T531">pʼeš</ta>
            <ta e="T533" id="Seg_2560" s="T532">măn-də</ta>
            <ta e="T534" id="Seg_2561" s="T533">nabə-ʔi</ta>
            <ta e="T536" id="Seg_2562" s="T535">bar</ta>
            <ta e="T537" id="Seg_2563" s="T536">kabar-lia-ʔi-ʔi</ta>
            <ta e="T538" id="Seg_2564" s="T537">pʼeš</ta>
            <ta e="T539" id="Seg_2565" s="T538">măn-də</ta>
            <ta e="T540" id="Seg_2566" s="T539">am-a-ʔ</ta>
            <ta e="T541" id="Seg_2567" s="T540">măn</ta>
            <ta e="T542" id="Seg_2568" s="T541">pirog-də</ta>
            <ta e="T543" id="Seg_2569" s="T542">dĭ</ta>
            <ta e="T544" id="Seg_2570" s="T543">bar</ta>
            <ta e="T545" id="Seg_2571" s="T544">am-bi</ta>
            <ta e="T546" id="Seg_2572" s="T545">pʼeš-də</ta>
            <ta e="T548" id="Seg_2573" s="T547">sʼa-luʔ-pi</ta>
            <ta e="T549" id="Seg_2574" s="T548">nabə-ʔi</ta>
            <ta e="T550" id="Seg_2575" s="T549">nʼergö-laʔ-pi</ta>
            <ta e="T551" id="Seg_2576" s="T550">nʼergö-laʔ-pi</ta>
            <ta e="T552" id="Seg_2577" s="T551">gijen=də</ta>
            <ta e="T553" id="Seg_2578" s="T552">naga</ta>
            <ta e="T554" id="Seg_2579" s="T553">par-luʔ-pi-ʔi</ta>
            <ta e="T555" id="Seg_2580" s="T554">nüke-nə</ta>
            <ta e="T556" id="Seg_2581" s="T555">a</ta>
            <ta e="T557" id="Seg_2582" s="T556">dĭ-zeŋ</ta>
            <ta e="T558" id="Seg_2583" s="T557">supso-laʔ</ta>
            <ta e="T559" id="Seg_2584" s="T558">šo-bi-ʔi</ta>
            <ta e="T560" id="Seg_2585" s="T559">ma-ndən</ta>
            <ta e="T561" id="Seg_2586" s="T560">šo-bi-ʔi</ta>
            <ta e="T562" id="Seg_2587" s="T561">i</ta>
            <ta e="T563" id="Seg_2588" s="T562">aba-t</ta>
            <ta e="T564" id="Seg_2589" s="T563">ija-t</ta>
            <ta e="T565" id="Seg_2590" s="T564">šo-bi-ʔi</ta>
            <ta e="T567" id="Seg_2591" s="T566">togonor-gəʔ</ta>
            <ta e="T568" id="Seg_2592" s="T567">bar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T228" id="Seg_2593" s="T227">amno-bi-jəʔ</ta>
            <ta e="T229" id="Seg_2594" s="T228">nüke</ta>
            <ta e="T230" id="Seg_2595" s="T229">büzʼe-ziʔ</ta>
            <ta e="T231" id="Seg_2596" s="T230">dĭ-zAŋ</ta>
            <ta e="T234" id="Seg_2597" s="T233">i-bi</ta>
            <ta e="T235" id="Seg_2598" s="T234">koʔbdo</ta>
            <ta e="T236" id="Seg_2599" s="T235">i</ta>
            <ta e="T237" id="Seg_2600" s="T236">nʼi</ta>
            <ta e="T238" id="Seg_2601" s="T237">dĭ-zAŋ</ta>
            <ta e="T239" id="Seg_2602" s="T238">togonər-zittə</ta>
            <ta e="T240" id="Seg_2603" s="T239">kan-laʔbə-jəʔ</ta>
            <ta e="T241" id="Seg_2604" s="T240">măndo-t</ta>
            <ta e="T242" id="Seg_2605" s="T241">nʼi-m</ta>
            <ta e="T245" id="Seg_2606" s="T244">măn</ta>
            <ta e="T246" id="Seg_2607" s="T245">tănan</ta>
            <ta e="T247" id="Seg_2608" s="T246">i-lV-m</ta>
            <ta e="T248" id="Seg_2609" s="T247">plat</ta>
            <ta e="T249" id="Seg_2610" s="T248">kuvas</ta>
            <ta e="T250" id="Seg_2611" s="T249">kan-bi-jəʔ</ta>
            <ta e="T251" id="Seg_2612" s="T250">a</ta>
            <ta e="T252" id="Seg_2613" s="T251">koʔbdo-t</ta>
            <ta e="T253" id="Seg_2614" s="T252">nʼi-bə</ta>
            <ta e="T254" id="Seg_2615" s="T253">i-bi</ta>
            <ta e="T255" id="Seg_2616" s="T254">nʼiʔnen</ta>
            <ta e="T256" id="Seg_2617" s="T255">amnol-bi</ta>
            <ta e="T257" id="Seg_2618" s="T256">noʔ-Kən</ta>
            <ta e="T258" id="Seg_2619" s="T257">a</ta>
            <ta e="T259" id="Seg_2620" s="T258">bos-də</ta>
            <ta e="T260" id="Seg_2621" s="T259">nuʔmə-luʔbdə-bi</ta>
            <ta e="T261" id="Seg_2622" s="T260">dön</ta>
            <ta e="T262" id="Seg_2623" s="T261">nabə-jəʔ</ta>
            <ta e="T263" id="Seg_2624" s="T262">na-nʼergö-luʔbdə-bi</ta>
            <ta e="T264" id="Seg_2625" s="T263">dĭ</ta>
            <ta e="T265" id="Seg_2626" s="T264">nʼi-m</ta>
            <ta e="T268" id="Seg_2627" s="T267">i-luʔbdə-bi-jəʔ</ta>
            <ta e="T269" id="Seg_2628" s="T268">kun-laːm-bi-jəʔ</ta>
            <ta e="T270" id="Seg_2629" s="T269">kun-bi-jəʔ</ta>
            <ta e="T271" id="Seg_2630" s="T270">koʔbdo</ta>
            <ta e="T273" id="Seg_2631" s="T272">nuʔmə-lAʔ</ta>
            <ta e="T274" id="Seg_2632" s="T273">šo-bi</ta>
            <ta e="T275" id="Seg_2633" s="T274">ku-liA-t</ta>
            <ta e="T276" id="Seg_2634" s="T275">naga</ta>
            <ta e="T277" id="Seg_2635" s="T276">naga</ta>
            <ta e="T278" id="Seg_2636" s="T277">nʼi</ta>
            <ta e="T279" id="Seg_2637" s="T278">dĭbər_döbər</ta>
            <ta e="T280" id="Seg_2638" s="T279">nuʔmə-laʔbə-bi</ta>
            <ta e="T281" id="Seg_2639" s="T280">dĭgəttə</ta>
            <ta e="T282" id="Seg_2640" s="T281">kan-bi</ta>
            <ta e="T283" id="Seg_2641" s="T282">nuʔmə-luʔbdə-bi</ta>
            <ta e="T284" id="Seg_2642" s="T283">nuʔmə-laʔbə</ta>
            <ta e="T285" id="Seg_2643" s="T284">nuʔmə-laʔbə</ta>
            <ta e="T286" id="Seg_2644" s="T285">pʼeːš</ta>
            <ta e="T287" id="Seg_2645" s="T286">nu-laʔbə</ta>
            <ta e="T288" id="Seg_2646" s="T287">tăn</ta>
            <ta e="T289" id="Seg_2647" s="T288">ej</ta>
            <ta e="T290" id="Seg_2648" s="T289">ku-bi-m</ta>
            <ta e="T292" id="Seg_2649" s="T291">gijen</ta>
            <ta e="T293" id="Seg_2650" s="T292">nʼergö-luʔbdə-bi-jəʔ</ta>
            <ta e="T294" id="Seg_2651" s="T293">nabə-jəʔ</ta>
            <ta e="T295" id="Seg_2652" s="T294">dĭ</ta>
            <ta e="T296" id="Seg_2653" s="T295">măn-ntə</ta>
            <ta e="T297" id="Seg_2654" s="T296">pirog</ta>
            <ta e="T298" id="Seg_2655" s="T297">am-ə-ʔ</ta>
            <ta e="T299" id="Seg_2656" s="T298">ej</ta>
            <ta e="T300" id="Seg_2657" s="T299">budəj</ta>
            <ta e="T301" id="Seg_2658" s="T300">măn</ta>
            <ta e="T302" id="Seg_2659" s="T301">aba-gəndə</ta>
            <ta e="T303" id="Seg_2660" s="T302">budəj</ta>
            <ta e="T304" id="Seg_2661" s="T303">i-gA</ta>
            <ta e="T305" id="Seg_2662" s="T304">da</ta>
            <ta e="T306" id="Seg_2663" s="T305">ej</ta>
            <ta e="T307" id="Seg_2664" s="T306">am-liA-m</ta>
            <ta e="T308" id="Seg_2665" s="T307">nuʔmə-luʔbdə-bi</ta>
            <ta e="T309" id="Seg_2666" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_2667" s="T309">šonə-gA</ta>
            <ta e="T311" id="Seg_2668" s="T310">dĭgəttə</ta>
            <ta e="T312" id="Seg_2669" s="T311">pʼeːš</ta>
            <ta e="T313" id="Seg_2670" s="T312">ĭmbi</ta>
            <ta e="T314" id="Seg_2671" s="T313">dĭ-Tə</ta>
            <ta e="T315" id="Seg_2672" s="T314">ej</ta>
            <ta e="T316" id="Seg_2673" s="T315">măn-bi</ta>
            <ta e="T317" id="Seg_2674" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_2675" s="T317">nuʔmə-luʔbdə-bi</ta>
            <ta e="T319" id="Seg_2676" s="T318">bazoʔ</ta>
            <ta e="T320" id="Seg_2677" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2678" s="T320">šonə-gA</ta>
            <ta e="T322" id="Seg_2679" s="T321">bü</ta>
            <ta e="T323" id="Seg_2680" s="T322">mʼaŋ-laʔbə</ta>
            <ta e="T324" id="Seg_2681" s="T323">amna-ʔ</ta>
            <ta e="T325" id="Seg_2682" s="T324">kiselʼ</ta>
            <ta e="T326" id="Seg_2683" s="T325">măn</ta>
            <ta e="T327" id="Seg_2684" s="T326">süt-ziʔ</ta>
            <ta e="T328" id="Seg_2685" s="T327">măn</ta>
            <ta e="T329" id="Seg_2686" s="T328">aba-gəndə</ta>
            <ta e="T330" id="Seg_2687" s="T329">ej</ta>
            <ta e="T331" id="Seg_2688" s="T330">dĭrgit=də</ta>
            <ta e="T332" id="Seg_2689" s="T331">ej</ta>
            <ta e="T333" id="Seg_2690" s="T332">am-liA-m</ta>
            <ta e="T334" id="Seg_2691" s="T333">dĭ</ta>
            <ta e="T335" id="Seg_2692" s="T334">dĭ-Tə</ta>
            <ta e="T336" id="Seg_2693" s="T335">ĭmbi=də</ta>
            <ta e="T337" id="Seg_2694" s="T336">ej</ta>
            <ta e="T338" id="Seg_2695" s="T337">măn-bi</ta>
            <ta e="T339" id="Seg_2696" s="T338">dĭgəttə</ta>
            <ta e="T340" id="Seg_2697" s="T339">nuʔmə-laʔbə</ta>
            <ta e="T341" id="Seg_2698" s="T340">nu-laʔbə</ta>
            <ta e="T342" id="Seg_2699" s="T341">jablokă</ta>
            <ta e="T343" id="Seg_2700" s="T342">ku-bi-l</ta>
            <ta e="T344" id="Seg_2701" s="T343">gibər</ta>
            <ta e="T346" id="Seg_2702" s="T345">nabə-jəʔ</ta>
            <ta e="T347" id="Seg_2703" s="T346">nʼergö-luʔbdə-bi-jəʔ</ta>
            <ta e="T348" id="Seg_2704" s="T347">amna-ʔ</ta>
            <ta e="T349" id="Seg_2705" s="T348">măn</ta>
            <ta e="T350" id="Seg_2706" s="T349">jablokă</ta>
            <ta e="T351" id="Seg_2707" s="T350">măn</ta>
            <ta e="T352" id="Seg_2708" s="T351">aba-Tə</ta>
            <ta e="T353" id="Seg_2709" s="T352">ĭššo</ta>
            <ta e="T354" id="Seg_2710" s="T353">jakšə</ta>
            <ta e="T355" id="Seg_2711" s="T354">jablokă</ta>
            <ta e="T356" id="Seg_2712" s="T355">i</ta>
            <ta e="T357" id="Seg_2713" s="T356">to</ta>
            <ta e="T358" id="Seg_2714" s="T357">ej</ta>
            <ta e="T359" id="Seg_2715" s="T358">am-liA-m</ta>
            <ta e="T360" id="Seg_2716" s="T359">i</ta>
            <ta e="T361" id="Seg_2717" s="T360">dĭ-Tə</ta>
            <ta e="T362" id="Seg_2718" s="T361">ĭmbi=də</ta>
            <ta e="T363" id="Seg_2719" s="T362">ej</ta>
            <ta e="T364" id="Seg_2720" s="T363">nörbə-bi</ta>
            <ta e="T365" id="Seg_2721" s="T364">dĭgəttə</ta>
            <ta e="T367" id="Seg_2722" s="T366">kan-bi</ta>
            <ta e="T368" id="Seg_2723" s="T367">kan-bi</ta>
            <ta e="T369" id="Seg_2724" s="T368">nu-laʔbə</ta>
            <ta e="T370" id="Seg_2725" s="T369">tura</ta>
            <ta e="T371" id="Seg_2726" s="T370">dĭ</ta>
            <ta e="T372" id="Seg_2727" s="T371">tura-Tə</ta>
            <ta e="T373" id="Seg_2728" s="T372">šü-bi</ta>
            <ta e="T374" id="Seg_2729" s="T373">dĭn</ta>
            <ta e="T375" id="Seg_2730" s="T374">nüke</ta>
            <ta e="T376" id="Seg_2731" s="T375">amno-laʔbə</ta>
            <ta e="T377" id="Seg_2732" s="T376">erer-laʔbə</ta>
            <ta e="T378" id="Seg_2733" s="T377">nʼi</ta>
            <ta e="T379" id="Seg_2734" s="T378">i-gA</ta>
            <ta e="T380" id="Seg_2735" s="T379">i</ta>
            <ta e="T381" id="Seg_2736" s="T380">dĭn</ta>
            <ta e="T382" id="Seg_2737" s="T381">erer-ə-ʔ</ta>
            <ta e="T383" id="Seg_2738" s="T382">ĭmbi</ta>
            <ta e="T384" id="Seg_2739" s="T383">i-lAʔ</ta>
            <ta e="T385" id="Seg_2740" s="T384">šo-bi-l</ta>
            <ta e="T386" id="Seg_2741" s="T385">da</ta>
            <ta e="T387" id="Seg_2742" s="T386">măn</ta>
            <ta e="T388" id="Seg_2743" s="T387">mĭn-bi-m</ta>
            <ta e="T389" id="Seg_2744" s="T388">da</ta>
            <ta e="T390" id="Seg_2745" s="T389">kănzə-laːm-bi-m</ta>
            <ta e="T391" id="Seg_2746" s="T390">ejü-m-zittə</ta>
            <ta e="T392" id="Seg_2747" s="T391">šo-bi-m</ta>
            <ta e="T393" id="Seg_2748" s="T392">amnə-ʔ</ta>
            <ta e="T394" id="Seg_2749" s="T393">erer-ə-ʔ</ta>
            <ta e="T395" id="Seg_2750" s="T394">kudʼelʼə</ta>
            <ta e="T396" id="Seg_2751" s="T395">măna</ta>
            <ta e="T397" id="Seg_2752" s="T396">amnə-bi</ta>
            <ta e="T398" id="Seg_2753" s="T397">dĭgəttə</ta>
            <ta e="T399" id="Seg_2754" s="T398">erer-laʔbə</ta>
            <ta e="T400" id="Seg_2755" s="T399">a</ta>
            <ta e="T401" id="Seg_2756" s="T400">nʼi-t</ta>
            <ta e="T402" id="Seg_2757" s="T401">nʼiʔnen</ta>
            <ta e="T403" id="Seg_2758" s="T402">amnə-laʔbə</ta>
            <ta e="T404" id="Seg_2759" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_2760" s="T404">tumo</ta>
            <ta e="T406" id="Seg_2761" s="T405">supso-bi</ta>
            <ta e="T407" id="Seg_2762" s="T406">măn-ntə</ta>
            <ta e="T408" id="Seg_2763" s="T407">koʔbdo</ta>
            <ta e="T409" id="Seg_2764" s="T408">tănan</ta>
            <ta e="T410" id="Seg_2765" s="T409">nüke</ta>
            <ta e="T411" id="Seg_2766" s="T410">nend-laʔbə</ta>
            <ta e="T412" id="Seg_2767" s="T411">multʼa</ta>
            <ta e="T413" id="Seg_2768" s="T412">i</ta>
            <ta e="T414" id="Seg_2769" s="T413">dĭn</ta>
            <ta e="T415" id="Seg_2770" s="T414">tănan</ta>
            <ta e="T416" id="Seg_2771" s="T415">nend-lV-j</ta>
            <ta e="T417" id="Seg_2772" s="T416">ibə</ta>
            <ta e="T418" id="Seg_2773" s="T417">am-lV-j</ta>
            <ta e="T419" id="Seg_2774" s="T418">hele-gəndə</ta>
            <ta e="T420" id="Seg_2775" s="T419">bar</ta>
            <ta e="T421" id="Seg_2776" s="T420">amnol-lV-j</ta>
            <ta e="T422" id="Seg_2777" s="T421">bos-də</ta>
            <ta e="T423" id="Seg_2778" s="T422">det-ʔ</ta>
            <ta e="T424" id="Seg_2779" s="T423">măna</ta>
            <ta e="T425" id="Seg_2780" s="T424">am-zittə</ta>
            <ta e="T427" id="Seg_2781" s="T426">kaška</ta>
            <ta e="T428" id="Seg_2782" s="T427">dĭ</ta>
            <ta e="T429" id="Seg_2783" s="T428">dĭ-Tə</ta>
            <ta e="T431" id="Seg_2784" s="T430">mĭ-bi</ta>
            <ta e="T432" id="Seg_2785" s="T431">kan-ə-ʔ</ta>
            <ta e="T433" id="Seg_2786" s="T432">a</ta>
            <ta e="T434" id="Seg_2787" s="T433">măn</ta>
            <ta e="T435" id="Seg_2788" s="T434">erer-lV-m</ta>
            <ta e="T436" id="Seg_2789" s="T435">i-t</ta>
            <ta e="T437" id="Seg_2790" s="T436">bos-də</ta>
            <ta e="T438" id="Seg_2791" s="T437">nʼi-n</ta>
            <ta e="T439" id="Seg_2792" s="T438">dĭ</ta>
            <ta e="T440" id="Seg_2793" s="T439">kabar-lAʔ</ta>
            <ta e="T441" id="Seg_2794" s="T440">i-bi</ta>
            <ta e="T442" id="Seg_2795" s="T441">i</ta>
            <ta e="T443" id="Seg_2796" s="T442">nuʔmə-luʔbdə-bi</ta>
            <ta e="T444" id="Seg_2797" s="T443">dĭ</ta>
            <ta e="T445" id="Seg_2798" s="T444">nüke</ta>
            <ta e="T446" id="Seg_2799" s="T445">šo-lV-j</ta>
            <ta e="T447" id="Seg_2800" s="T446">tăn</ta>
            <ta e="T448" id="Seg_2801" s="T447">erer-laʔbə-l</ta>
            <ta e="T449" id="Seg_2802" s="T448">dĭ</ta>
            <ta e="T450" id="Seg_2803" s="T449">măn-ntə</ta>
            <ta e="T451" id="Seg_2804" s="T450">erer-laʔbə-m</ta>
            <ta e="T452" id="Seg_2805" s="T451">dĭgəttə</ta>
            <ta e="T453" id="Seg_2806" s="T452">bazoʔ</ta>
            <ta e="T454" id="Seg_2807" s="T453">kan-lV-j</ta>
            <ta e="T455" id="Seg_2808" s="T454">multʼa</ta>
            <ta e="T456" id="Seg_2809" s="T455">nendə-zittə</ta>
            <ta e="T457" id="Seg_2810" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_2811" s="T457">koʔbdo</ta>
            <ta e="T459" id="Seg_2812" s="T458">nuʔmə-luʔbdə-bi</ta>
            <ta e="T460" id="Seg_2813" s="T459">šo-bi</ta>
            <ta e="T461" id="Seg_2814" s="T460">dĭgəttə</ta>
            <ta e="T462" id="Seg_2815" s="T461">nüke</ta>
            <ta e="T464" id="Seg_2816" s="T463">tura-Tə</ta>
            <ta e="T465" id="Seg_2817" s="T464">šo-bi</ta>
            <ta e="T466" id="Seg_2818" s="T465">koʔbdo</ta>
            <ta e="T467" id="Seg_2819" s="T466">naga</ta>
            <ta e="T468" id="Seg_2820" s="T467">i</ta>
            <ta e="T469" id="Seg_2821" s="T468">nʼi</ta>
            <ta e="T470" id="Seg_2822" s="T469">naga</ta>
            <ta e="T471" id="Seg_2823" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_2824" s="T471">nabə-jəʔ-Tə</ta>
            <ta e="T473" id="Seg_2825" s="T472">măn-ntə</ta>
            <ta e="T474" id="Seg_2826" s="T473">kan-KAʔ</ta>
            <ta e="T475" id="Seg_2827" s="T474">măndo-r-KAʔ</ta>
            <ta e="T476" id="Seg_2828" s="T475">gijen</ta>
            <ta e="T477" id="Seg_2829" s="T476">koʔbdo</ta>
            <ta e="T478" id="Seg_2830" s="T477">dĭ</ta>
            <ta e="T479" id="Seg_2831" s="T478">koʔbdo</ta>
            <ta e="T481" id="Seg_2832" s="T480">nuʔmə-lAʔ</ta>
            <ta e="T482" id="Seg_2833" s="T481">šonə-gA</ta>
            <ta e="T483" id="Seg_2834" s="T482">bü-Tə</ta>
            <ta e="T484" id="Seg_2835" s="T483">šo-bi</ta>
            <ta e="T485" id="Seg_2836" s="T484">bü</ta>
            <ta e="T486" id="Seg_2837" s="T485">bü</ta>
            <ta e="T487" id="Seg_2838" s="T486">šaʔbdəl-ə-ʔ</ta>
            <ta e="T488" id="Seg_2839" s="T487">măna</ta>
            <ta e="T489" id="Seg_2840" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_2841" s="T489">măn-ntə</ta>
            <ta e="T492" id="Seg_2842" s="T491">amna-ʔ</ta>
            <ta e="T493" id="Seg_2843" s="T492">măn</ta>
            <ta e="T494" id="Seg_2844" s="T493">kiselʼ-m</ta>
            <ta e="T495" id="Seg_2845" s="T494">dĭ</ta>
            <ta e="T496" id="Seg_2846" s="T495">am-bi</ta>
            <ta e="T497" id="Seg_2847" s="T496">dĭ</ta>
            <ta e="T498" id="Seg_2848" s="T497">dĭ-m</ta>
            <ta e="T499" id="Seg_2849" s="T498">kaj-luʔbdə-bi</ta>
            <ta e="T500" id="Seg_2850" s="T499">dĭgəttə</ta>
            <ta e="T501" id="Seg_2851" s="T500">nabə-jəʔ</ta>
            <ta e="T502" id="Seg_2852" s="T501">nʼergö-luʔbdə-bi-jəʔ</ta>
            <ta e="T503" id="Seg_2853" s="T502">dĭ</ta>
            <ta e="T504" id="Seg_2854" s="T503">bar</ta>
            <ta e="T505" id="Seg_2855" s="T504">supso-bi</ta>
            <ta e="T506" id="Seg_2856" s="T505">bazoʔ</ta>
            <ta e="T507" id="Seg_2857" s="T506">nuʔmə-laʔbə</ta>
            <ta e="T508" id="Seg_2858" s="T507">nabə-jəʔ</ta>
            <ta e="T509" id="Seg_2859" s="T508">bazoʔ</ta>
            <ta e="T510" id="Seg_2860" s="T509">nʼergö-laʔbə-jəʔ</ta>
            <ta e="T511" id="Seg_2861" s="T510">dĭgəttə</ta>
            <ta e="T512" id="Seg_2862" s="T511">jablokă-Tə</ta>
            <ta e="T513" id="Seg_2863" s="T512">măn-ntə</ta>
            <ta e="T514" id="Seg_2864" s="T513">šaʔ-laːm-t</ta>
            <ta e="T515" id="Seg_2865" s="T514">miʔnʼibeʔ</ta>
            <ta e="T516" id="Seg_2866" s="T515">am-ə-ʔ</ta>
            <ta e="T517" id="Seg_2867" s="T516">jablokă</ta>
            <ta e="T518" id="Seg_2868" s="T517">dĭ</ta>
            <ta e="T519" id="Seg_2869" s="T518">am-bi</ta>
            <ta e="T520" id="Seg_2870" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_2871" s="T520">dĭ-m</ta>
            <ta e="T522" id="Seg_2872" s="T521">šaʔ-laːm-bi</ta>
            <ta e="T523" id="Seg_2873" s="T522">dĭgəttə</ta>
            <ta e="T524" id="Seg_2874" s="T523">nabə-jəʔ</ta>
            <ta e="T525" id="Seg_2875" s="T524">nʼergö-luʔbdə-bi-jəʔ</ta>
            <ta e="T526" id="Seg_2876" s="T525">dĭ</ta>
            <ta e="T527" id="Seg_2877" s="T526">dĭ-zAŋ</ta>
            <ta e="T528" id="Seg_2878" s="T527">bazoʔ</ta>
            <ta e="T529" id="Seg_2879" s="T528">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T530" id="Seg_2880" s="T529">pʼeːš-Tə</ta>
            <ta e="T531" id="Seg_2881" s="T530">šo-bi</ta>
            <ta e="T532" id="Seg_2882" s="T531">pʼeːš</ta>
            <ta e="T533" id="Seg_2883" s="T532">măn-ntə</ta>
            <ta e="T534" id="Seg_2884" s="T533">nabə-jəʔ</ta>
            <ta e="T536" id="Seg_2885" s="T535">bar</ta>
            <ta e="T537" id="Seg_2886" s="T536">kabar-liA-jəʔ-jəʔ</ta>
            <ta e="T538" id="Seg_2887" s="T537">pʼeːš</ta>
            <ta e="T539" id="Seg_2888" s="T538">măn-ntə</ta>
            <ta e="T540" id="Seg_2889" s="T539">am-ə-ʔ</ta>
            <ta e="T541" id="Seg_2890" s="T540">măn</ta>
            <ta e="T542" id="Seg_2891" s="T541">pirog-də</ta>
            <ta e="T543" id="Seg_2892" s="T542">dĭ</ta>
            <ta e="T544" id="Seg_2893" s="T543">bar</ta>
            <ta e="T545" id="Seg_2894" s="T544">am-bi</ta>
            <ta e="T546" id="Seg_2895" s="T545">pʼeːš-Tə</ta>
            <ta e="T548" id="Seg_2896" s="T547">sʼa-luʔbdə-bi</ta>
            <ta e="T549" id="Seg_2897" s="T548">nabə-jəʔ</ta>
            <ta e="T550" id="Seg_2898" s="T549">nʼergö-laʔbə-bi</ta>
            <ta e="T551" id="Seg_2899" s="T550">nʼergö-laʔbə-bi</ta>
            <ta e="T552" id="Seg_2900" s="T551">gijen=də</ta>
            <ta e="T553" id="Seg_2901" s="T552">naga</ta>
            <ta e="T554" id="Seg_2902" s="T553">par-luʔbdə-bi-jəʔ</ta>
            <ta e="T555" id="Seg_2903" s="T554">nüke-Tə</ta>
            <ta e="T556" id="Seg_2904" s="T555">a</ta>
            <ta e="T557" id="Seg_2905" s="T556">dĭ-zAŋ</ta>
            <ta e="T558" id="Seg_2906" s="T557">supso-lAʔ</ta>
            <ta e="T559" id="Seg_2907" s="T558">šo-bi-jəʔ</ta>
            <ta e="T560" id="Seg_2908" s="T559">maʔ-gəndən</ta>
            <ta e="T561" id="Seg_2909" s="T560">šo-bi-jəʔ</ta>
            <ta e="T562" id="Seg_2910" s="T561">i</ta>
            <ta e="T563" id="Seg_2911" s="T562">aba-t</ta>
            <ta e="T564" id="Seg_2912" s="T563">ija-t</ta>
            <ta e="T565" id="Seg_2913" s="T564">šo-bi-jəʔ</ta>
            <ta e="T567" id="Seg_2914" s="T566">togonər-gəʔ</ta>
            <ta e="T568" id="Seg_2915" s="T567">bar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T228" id="Seg_2916" s="T227">live-PST-3PL</ta>
            <ta e="T229" id="Seg_2917" s="T228">woman.[NOM.SG]</ta>
            <ta e="T230" id="Seg_2918" s="T229">man-INS</ta>
            <ta e="T231" id="Seg_2919" s="T230">this-PL</ta>
            <ta e="T234" id="Seg_2920" s="T233">be-PST.[3SG]</ta>
            <ta e="T235" id="Seg_2921" s="T234">girl.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2922" s="T235">and</ta>
            <ta e="T237" id="Seg_2923" s="T236">boy.[NOM.SG]</ta>
            <ta e="T238" id="Seg_2924" s="T237">this-PL</ta>
            <ta e="T239" id="Seg_2925" s="T238">work-INF.LAT</ta>
            <ta e="T240" id="Seg_2926" s="T239">go-DUR-3PL</ta>
            <ta e="T241" id="Seg_2927" s="T240">look-IMP.2SG.O</ta>
            <ta e="T242" id="Seg_2928" s="T241">boy-ACC</ta>
            <ta e="T245" id="Seg_2929" s="T244">I.NOM</ta>
            <ta e="T246" id="Seg_2930" s="T245">you.DAT</ta>
            <ta e="T247" id="Seg_2931" s="T246">take-FUT-1SG</ta>
            <ta e="T248" id="Seg_2932" s="T247">scarf.[NOM.SG]</ta>
            <ta e="T249" id="Seg_2933" s="T248">beautiful.[NOM.SG]</ta>
            <ta e="T250" id="Seg_2934" s="T249">go-PST-3PL</ta>
            <ta e="T251" id="Seg_2935" s="T250">and</ta>
            <ta e="T252" id="Seg_2936" s="T251">daughter-NOM/GEN.3SG</ta>
            <ta e="T253" id="Seg_2937" s="T252">son-ACC.3SG</ta>
            <ta e="T254" id="Seg_2938" s="T253">take-PST.[3SG]</ta>
            <ta e="T255" id="Seg_2939" s="T254">outside</ta>
            <ta e="T256" id="Seg_2940" s="T255">seat-PST.[3SG]</ta>
            <ta e="T257" id="Seg_2941" s="T256">grass-LOC</ta>
            <ta e="T258" id="Seg_2942" s="T257">and</ta>
            <ta e="T259" id="Seg_2943" s="T258">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T260" id="Seg_2944" s="T259">run-MOM-PST.[3SG]</ta>
            <ta e="T261" id="Seg_2945" s="T260">here</ta>
            <ta e="T262" id="Seg_2946" s="T261">duck-PL</ta>
            <ta e="T263" id="Seg_2947" s="T262">PREF-fly-MOM-PST.[3SG]</ta>
            <ta e="T264" id="Seg_2948" s="T263">this.[NOM.SG]</ta>
            <ta e="T265" id="Seg_2949" s="T264">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T268" id="Seg_2950" s="T267">take-MOM-PST-3PL</ta>
            <ta e="T269" id="Seg_2951" s="T268">bring-RES-PST-3PL</ta>
            <ta e="T270" id="Seg_2952" s="T269">bring-PST-3PL</ta>
            <ta e="T271" id="Seg_2953" s="T270">girl.[NOM.SG]</ta>
            <ta e="T273" id="Seg_2954" s="T272">run-CVB</ta>
            <ta e="T274" id="Seg_2955" s="T273">come-PST.[3SG]</ta>
            <ta e="T275" id="Seg_2956" s="T274">see-PRS-3SG.O</ta>
            <ta e="T276" id="Seg_2957" s="T275">NEG.EX.[3SG]</ta>
            <ta e="T277" id="Seg_2958" s="T276">NEG.EX.[3SG]</ta>
            <ta e="T278" id="Seg_2959" s="T277">boy.[NOM.SG]</ta>
            <ta e="T279" id="Seg_2960" s="T278">there_here</ta>
            <ta e="T280" id="Seg_2961" s="T279">run-DUR-PST.[3SG]</ta>
            <ta e="T281" id="Seg_2962" s="T280">then</ta>
            <ta e="T282" id="Seg_2963" s="T281">go-PST.[3SG]</ta>
            <ta e="T283" id="Seg_2964" s="T282">run-MOM-PST.[3SG]</ta>
            <ta e="T284" id="Seg_2965" s="T283">run-DUR.[3SG]</ta>
            <ta e="T285" id="Seg_2966" s="T284">run-DUR.[3SG]</ta>
            <ta e="T286" id="Seg_2967" s="T285">stove.[NOM.SG]</ta>
            <ta e="T287" id="Seg_2968" s="T286">stand-DUR.[3SG]</ta>
            <ta e="T288" id="Seg_2969" s="T287">you.NOM</ta>
            <ta e="T289" id="Seg_2970" s="T288">NEG</ta>
            <ta e="T290" id="Seg_2971" s="T289">see-PST-1SG</ta>
            <ta e="T292" id="Seg_2972" s="T291">where</ta>
            <ta e="T293" id="Seg_2973" s="T292">fly-MOM-PST-3PL</ta>
            <ta e="T294" id="Seg_2974" s="T293">duck-PL</ta>
            <ta e="T295" id="Seg_2975" s="T294">this.[NOM.SG]</ta>
            <ta e="T296" id="Seg_2976" s="T295">say-IPFVZ.[3SG]</ta>
            <ta e="T297" id="Seg_2977" s="T296">pie.[NOM.SG]</ta>
            <ta e="T298" id="Seg_2978" s="T297">eat-EP-IMP.2SG</ta>
            <ta e="T299" id="Seg_2979" s="T298">NEG</ta>
            <ta e="T300" id="Seg_2980" s="T299">wheat.[NOM.SG]</ta>
            <ta e="T301" id="Seg_2981" s="T300">I.GEN</ta>
            <ta e="T302" id="Seg_2982" s="T301">father-LAT/LOC.3SG</ta>
            <ta e="T303" id="Seg_2983" s="T302">wheat.[NOM.SG]</ta>
            <ta e="T304" id="Seg_2984" s="T303">be-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_2985" s="T304">and</ta>
            <ta e="T306" id="Seg_2986" s="T305">NEG</ta>
            <ta e="T307" id="Seg_2987" s="T306">eat-PRS-1SG</ta>
            <ta e="T308" id="Seg_2988" s="T307">run-MOM-PST.[3SG]</ta>
            <ta e="T309" id="Seg_2989" s="T308">then</ta>
            <ta e="T310" id="Seg_2990" s="T309">come-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_2991" s="T310">then</ta>
            <ta e="T312" id="Seg_2992" s="T311">stove.[NOM.SG]</ta>
            <ta e="T313" id="Seg_2993" s="T312">what</ta>
            <ta e="T314" id="Seg_2994" s="T313">this-LAT</ta>
            <ta e="T315" id="Seg_2995" s="T314">NEG</ta>
            <ta e="T316" id="Seg_2996" s="T315">say-PST.[3SG]</ta>
            <ta e="T317" id="Seg_2997" s="T316">this.[NOM.SG]</ta>
            <ta e="T318" id="Seg_2998" s="T317">run-MOM-PST.[3SG]</ta>
            <ta e="T319" id="Seg_2999" s="T318">again</ta>
            <ta e="T320" id="Seg_3000" s="T319">then</ta>
            <ta e="T321" id="Seg_3001" s="T320">come-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3002" s="T321">water.[NOM.SG]</ta>
            <ta e="T323" id="Seg_3003" s="T322">flow-DUR.[3SG]</ta>
            <ta e="T324" id="Seg_3004" s="T323">eat-IMP.2SG</ta>
            <ta e="T325" id="Seg_3005" s="T324">kissel.[NOM.SG]</ta>
            <ta e="T326" id="Seg_3006" s="T325">I.GEN</ta>
            <ta e="T327" id="Seg_3007" s="T326">milk-INS</ta>
            <ta e="T328" id="Seg_3008" s="T327">I.GEN</ta>
            <ta e="T329" id="Seg_3009" s="T328">father-LAT/LOC.3SG</ta>
            <ta e="T330" id="Seg_3010" s="T329">NEG</ta>
            <ta e="T331" id="Seg_3011" s="T330">such=INDEF</ta>
            <ta e="T332" id="Seg_3012" s="T331">NEG</ta>
            <ta e="T333" id="Seg_3013" s="T332">eat-PRS-1SG</ta>
            <ta e="T334" id="Seg_3014" s="T333">this.[NOM.SG]</ta>
            <ta e="T335" id="Seg_3015" s="T334">this-LAT</ta>
            <ta e="T336" id="Seg_3016" s="T335">what.[NOM.SG]=INDEF</ta>
            <ta e="T337" id="Seg_3017" s="T336">NEG</ta>
            <ta e="T338" id="Seg_3018" s="T337">say-PST.[3SG]</ta>
            <ta e="T339" id="Seg_3019" s="T338">then</ta>
            <ta e="T340" id="Seg_3020" s="T339">run-DUR.[3SG]</ta>
            <ta e="T341" id="Seg_3021" s="T340">stand-DUR.[3SG]</ta>
            <ta e="T342" id="Seg_3022" s="T341">apple.tree.[NOM.SG]</ta>
            <ta e="T343" id="Seg_3023" s="T342">see-PST-2SG</ta>
            <ta e="T344" id="Seg_3024" s="T343">where.to</ta>
            <ta e="T346" id="Seg_3025" s="T345">duck-PL</ta>
            <ta e="T347" id="Seg_3026" s="T346">fly-MOM-PST-3PL</ta>
            <ta e="T348" id="Seg_3027" s="T347">eat-IMP.2SG</ta>
            <ta e="T349" id="Seg_3028" s="T348">I.GEN</ta>
            <ta e="T350" id="Seg_3029" s="T349">apple.[NOM.SG]</ta>
            <ta e="T351" id="Seg_3030" s="T350">I.GEN</ta>
            <ta e="T352" id="Seg_3031" s="T351">father-LAT</ta>
            <ta e="T353" id="Seg_3032" s="T352">more</ta>
            <ta e="T354" id="Seg_3033" s="T353">good.[NOM.SG]</ta>
            <ta e="T355" id="Seg_3034" s="T354">apple.[NOM.SG]</ta>
            <ta e="T356" id="Seg_3035" s="T355">and</ta>
            <ta e="T357" id="Seg_3036" s="T356">then</ta>
            <ta e="T358" id="Seg_3037" s="T357">NEG</ta>
            <ta e="T359" id="Seg_3038" s="T358">eat-PRS-1SG</ta>
            <ta e="T360" id="Seg_3039" s="T359">and</ta>
            <ta e="T361" id="Seg_3040" s="T360">this-LAT</ta>
            <ta e="T362" id="Seg_3041" s="T361">what.[NOM.SG]=INDEF</ta>
            <ta e="T363" id="Seg_3042" s="T362">NEG</ta>
            <ta e="T364" id="Seg_3043" s="T363">tell-PST.[3SG]</ta>
            <ta e="T365" id="Seg_3044" s="T364">then</ta>
            <ta e="T367" id="Seg_3045" s="T366">go-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3046" s="T367">go-PST.[3SG]</ta>
            <ta e="T369" id="Seg_3047" s="T368">stand-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_3048" s="T369">house.[NOM.SG]</ta>
            <ta e="T371" id="Seg_3049" s="T370">this.[NOM.SG]</ta>
            <ta e="T372" id="Seg_3050" s="T371">house-LAT</ta>
            <ta e="T373" id="Seg_3051" s="T372">enter-PST.[3SG]</ta>
            <ta e="T374" id="Seg_3052" s="T373">there</ta>
            <ta e="T375" id="Seg_3053" s="T374">woman.[NOM.SG]</ta>
            <ta e="T376" id="Seg_3054" s="T375">sit-DUR.[3SG]</ta>
            <ta e="T377" id="Seg_3055" s="T376">spin-DUR.[3SG]</ta>
            <ta e="T378" id="Seg_3056" s="T377">boy.[NOM.SG]</ta>
            <ta e="T379" id="Seg_3057" s="T378">be-PRS.[3SG]</ta>
            <ta e="T380" id="Seg_3058" s="T379">and</ta>
            <ta e="T381" id="Seg_3059" s="T380">there</ta>
            <ta e="T382" id="Seg_3060" s="T381">spin-EP-IMP.2SG</ta>
            <ta e="T383" id="Seg_3061" s="T382">what.[NOM.SG]</ta>
            <ta e="T384" id="Seg_3062" s="T383">be-CVB</ta>
            <ta e="T385" id="Seg_3063" s="T384">come-PST-2SG</ta>
            <ta e="T386" id="Seg_3064" s="T385">and</ta>
            <ta e="T387" id="Seg_3065" s="T386">I.NOM</ta>
            <ta e="T388" id="Seg_3066" s="T387">go-PST-1SG</ta>
            <ta e="T389" id="Seg_3067" s="T388">and</ta>
            <ta e="T390" id="Seg_3068" s="T389">freeze-RES-PST-1SG</ta>
            <ta e="T391" id="Seg_3069" s="T390">warm-FACT-INF.LAT</ta>
            <ta e="T392" id="Seg_3070" s="T391">come-PST-1SG</ta>
            <ta e="T393" id="Seg_3071" s="T392">sit-IMP.2SG</ta>
            <ta e="T394" id="Seg_3072" s="T393">spin-EP-IMP.2SG</ta>
            <ta e="T395" id="Seg_3073" s="T394">tow.[NOM.SG]</ta>
            <ta e="T396" id="Seg_3074" s="T395">I.LAT</ta>
            <ta e="T397" id="Seg_3075" s="T396">sit.down-PST.[3SG]</ta>
            <ta e="T398" id="Seg_3076" s="T397">then</ta>
            <ta e="T399" id="Seg_3077" s="T398">spin-DUR.[3SG]</ta>
            <ta e="T400" id="Seg_3078" s="T399">and</ta>
            <ta e="T401" id="Seg_3079" s="T400">son-NOM/GEN.3SG</ta>
            <ta e="T402" id="Seg_3080" s="T401">outside</ta>
            <ta e="T403" id="Seg_3081" s="T402">sit-DUR.[3SG]</ta>
            <ta e="T404" id="Seg_3082" s="T403">then</ta>
            <ta e="T405" id="Seg_3083" s="T404">mouse.[NOM.SG]</ta>
            <ta e="T406" id="Seg_3084" s="T405">depart-PST.[3SG]</ta>
            <ta e="T407" id="Seg_3085" s="T406">say-IPFVZ.[3SG]</ta>
            <ta e="T408" id="Seg_3086" s="T407">girl.[NOM.SG]</ta>
            <ta e="T409" id="Seg_3087" s="T408">you.DAT</ta>
            <ta e="T410" id="Seg_3088" s="T409">woman.[NOM.SG]</ta>
            <ta e="T411" id="Seg_3089" s="T410">burn-DUR.[3SG]</ta>
            <ta e="T412" id="Seg_3090" s="T411">sauna.[NOM.SG]</ta>
            <ta e="T413" id="Seg_3091" s="T412">and</ta>
            <ta e="T414" id="Seg_3092" s="T413">there</ta>
            <ta e="T415" id="Seg_3093" s="T414">you.ACC</ta>
            <ta e="T416" id="Seg_3094" s="T415">burn-FUT-3SG</ta>
            <ta e="T417" id="Seg_3095" s="T416">%%</ta>
            <ta e="T418" id="Seg_3096" s="T417">eat-FUT-3SG</ta>
            <ta e="T419" id="Seg_3097" s="T418">companion-LAT/LOC.3SG</ta>
            <ta e="T420" id="Seg_3098" s="T419">PTCL</ta>
            <ta e="T421" id="Seg_3099" s="T420">seat-FUT-3SG</ta>
            <ta e="T422" id="Seg_3100" s="T421">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T423" id="Seg_3101" s="T422">bring-IMP.2SG</ta>
            <ta e="T424" id="Seg_3102" s="T423">I.LAT</ta>
            <ta e="T425" id="Seg_3103" s="T424">eat-INF.LAT</ta>
            <ta e="T427" id="Seg_3104" s="T426">porridge.[NOM.SG]</ta>
            <ta e="T428" id="Seg_3105" s="T427">this.[NOM.SG]</ta>
            <ta e="T429" id="Seg_3106" s="T428">this-LAT</ta>
            <ta e="T431" id="Seg_3107" s="T430">give-PST.[3SG]</ta>
            <ta e="T432" id="Seg_3108" s="T431">go-EP-IMP.2SG</ta>
            <ta e="T433" id="Seg_3109" s="T432">and</ta>
            <ta e="T434" id="Seg_3110" s="T433">I.NOM</ta>
            <ta e="T435" id="Seg_3111" s="T434">spin-FUT-1SG</ta>
            <ta e="T436" id="Seg_3112" s="T435">take-IMP.2SG.O</ta>
            <ta e="T437" id="Seg_3113" s="T436">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T438" id="Seg_3114" s="T437">boy-GEN</ta>
            <ta e="T439" id="Seg_3115" s="T438">this.[NOM.SG]</ta>
            <ta e="T440" id="Seg_3116" s="T439">grab-CVB</ta>
            <ta e="T441" id="Seg_3117" s="T440">take-PST.[3SG]</ta>
            <ta e="T442" id="Seg_3118" s="T441">and</ta>
            <ta e="T443" id="Seg_3119" s="T442">run-MOM-PST.[3SG]</ta>
            <ta e="T444" id="Seg_3120" s="T443">this.[NOM.SG]</ta>
            <ta e="T445" id="Seg_3121" s="T444">woman.[NOM.SG]</ta>
            <ta e="T446" id="Seg_3122" s="T445">come-FUT-3SG</ta>
            <ta e="T447" id="Seg_3123" s="T446">you.NOM</ta>
            <ta e="T448" id="Seg_3124" s="T447">spin-DUR-2SG</ta>
            <ta e="T449" id="Seg_3125" s="T448">this.[NOM.SG]</ta>
            <ta e="T450" id="Seg_3126" s="T449">say-IPFVZ.[3SG]</ta>
            <ta e="T451" id="Seg_3127" s="T450">spin-DUR-1SG</ta>
            <ta e="T452" id="Seg_3128" s="T451">then</ta>
            <ta e="T453" id="Seg_3129" s="T452">again</ta>
            <ta e="T454" id="Seg_3130" s="T453">go-FUT-3SG</ta>
            <ta e="T455" id="Seg_3131" s="T454">sauna.[NOM.SG]</ta>
            <ta e="T456" id="Seg_3132" s="T455">light-INF.LAT</ta>
            <ta e="T457" id="Seg_3133" s="T456">this.[NOM.SG]</ta>
            <ta e="T458" id="Seg_3134" s="T457">girl.[NOM.SG]</ta>
            <ta e="T459" id="Seg_3135" s="T458">run-MOM-PST.[3SG]</ta>
            <ta e="T460" id="Seg_3136" s="T459">come-PST.[3SG]</ta>
            <ta e="T461" id="Seg_3137" s="T460">then</ta>
            <ta e="T462" id="Seg_3138" s="T461">woman.[NOM.SG]</ta>
            <ta e="T464" id="Seg_3139" s="T463">house-LAT</ta>
            <ta e="T465" id="Seg_3140" s="T464">come-PST.[3SG]</ta>
            <ta e="T466" id="Seg_3141" s="T465">girl.[NOM.SG]</ta>
            <ta e="T467" id="Seg_3142" s="T466">NEG.EX.[3SG]</ta>
            <ta e="T468" id="Seg_3143" s="T467">and</ta>
            <ta e="T469" id="Seg_3144" s="T468">boy.[NOM.SG]</ta>
            <ta e="T470" id="Seg_3145" s="T469">NEG.EX.[3SG]</ta>
            <ta e="T471" id="Seg_3146" s="T470">then</ta>
            <ta e="T472" id="Seg_3147" s="T471">duck-3PL-LAT</ta>
            <ta e="T473" id="Seg_3148" s="T472">say-IPFVZ.[3SG]</ta>
            <ta e="T474" id="Seg_3149" s="T473">go-IMP.2PL</ta>
            <ta e="T475" id="Seg_3150" s="T474">look-FRQ-IMP.2PL</ta>
            <ta e="T476" id="Seg_3151" s="T475">where</ta>
            <ta e="T477" id="Seg_3152" s="T476">girl.[NOM.SG]</ta>
            <ta e="T478" id="Seg_3153" s="T477">this.[NOM.SG]</ta>
            <ta e="T479" id="Seg_3154" s="T478">girl.[NOM.SG]</ta>
            <ta e="T481" id="Seg_3155" s="T480">run-CVB</ta>
            <ta e="T482" id="Seg_3156" s="T481">come-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_3157" s="T482">water-LAT</ta>
            <ta e="T484" id="Seg_3158" s="T483">come-PST.[3SG]</ta>
            <ta e="T485" id="Seg_3159" s="T484">water.[NOM.SG]</ta>
            <ta e="T486" id="Seg_3160" s="T485">water.[NOM.SG]</ta>
            <ta e="T487" id="Seg_3161" s="T486">hide-EP-IMP.2SG</ta>
            <ta e="T488" id="Seg_3162" s="T487">I.ACC</ta>
            <ta e="T489" id="Seg_3163" s="T488">this.[NOM.SG]</ta>
            <ta e="T490" id="Seg_3164" s="T489">say-IPFVZ.[3SG]</ta>
            <ta e="T492" id="Seg_3165" s="T491">eat-IMP.2SG</ta>
            <ta e="T493" id="Seg_3166" s="T492">I.GEN</ta>
            <ta e="T494" id="Seg_3167" s="T493">kissel-NOM/GEN/ACC.1SG</ta>
            <ta e="T495" id="Seg_3168" s="T494">this.[NOM.SG]</ta>
            <ta e="T496" id="Seg_3169" s="T495">eat-PST.[3SG]</ta>
            <ta e="T497" id="Seg_3170" s="T496">this.[NOM.SG]</ta>
            <ta e="T498" id="Seg_3171" s="T497">this-ACC</ta>
            <ta e="T499" id="Seg_3172" s="T498">close-MOM-PST.[3SG]</ta>
            <ta e="T500" id="Seg_3173" s="T499">then</ta>
            <ta e="T501" id="Seg_3174" s="T500">duck-PL</ta>
            <ta e="T502" id="Seg_3175" s="T501">fly-MOM-PST-3PL</ta>
            <ta e="T503" id="Seg_3176" s="T502">this.[NOM.SG]</ta>
            <ta e="T504" id="Seg_3177" s="T503">PTCL</ta>
            <ta e="T505" id="Seg_3178" s="T504">depart-PST.[3SG]</ta>
            <ta e="T506" id="Seg_3179" s="T505">again</ta>
            <ta e="T507" id="Seg_3180" s="T506">run-DUR.[3SG]</ta>
            <ta e="T508" id="Seg_3181" s="T507">duck-PL</ta>
            <ta e="T509" id="Seg_3182" s="T508">again</ta>
            <ta e="T510" id="Seg_3183" s="T509">fly-DUR-3PL</ta>
            <ta e="T511" id="Seg_3184" s="T510">then</ta>
            <ta e="T512" id="Seg_3185" s="T511">apple.tree-LAT</ta>
            <ta e="T513" id="Seg_3186" s="T512">say-IPFVZ.[3SG]</ta>
            <ta e="T514" id="Seg_3187" s="T513">hide-RES-IMP.2SG.O</ta>
            <ta e="T515" id="Seg_3188" s="T514">we.ACC</ta>
            <ta e="T516" id="Seg_3189" s="T515">eat-EP-IMP.2SG</ta>
            <ta e="T517" id="Seg_3190" s="T516">apple.tree.[NOM.SG]</ta>
            <ta e="T518" id="Seg_3191" s="T517">this.[NOM.SG]</ta>
            <ta e="T519" id="Seg_3192" s="T518">eat-PST.[3SG]</ta>
            <ta e="T520" id="Seg_3193" s="T519">this.[NOM.SG]</ta>
            <ta e="T521" id="Seg_3194" s="T520">this-ACC</ta>
            <ta e="T522" id="Seg_3195" s="T521">hide-RES-PST.[3SG]</ta>
            <ta e="T523" id="Seg_3196" s="T522">then</ta>
            <ta e="T524" id="Seg_3197" s="T523">duck-PL</ta>
            <ta e="T525" id="Seg_3198" s="T524">fly-MOM-PST-3PL</ta>
            <ta e="T526" id="Seg_3199" s="T525">this.[NOM.SG]</ta>
            <ta e="T527" id="Seg_3200" s="T526">this-PL</ta>
            <ta e="T528" id="Seg_3201" s="T527">again</ta>
            <ta e="T529" id="Seg_3202" s="T528">run-MOM-PST-3PL</ta>
            <ta e="T530" id="Seg_3203" s="T529">stove-LAT</ta>
            <ta e="T531" id="Seg_3204" s="T530">come-PST.[3SG]</ta>
            <ta e="T532" id="Seg_3205" s="T531">stove.[NOM.SG]</ta>
            <ta e="T533" id="Seg_3206" s="T532">say-IPFVZ.[3SG]</ta>
            <ta e="T534" id="Seg_3207" s="T533">duck-PL</ta>
            <ta e="T536" id="Seg_3208" s="T535">PTCL</ta>
            <ta e="T537" id="Seg_3209" s="T536">grab-PRS-3PL-3PL</ta>
            <ta e="T538" id="Seg_3210" s="T537">stove.[NOM.SG]</ta>
            <ta e="T539" id="Seg_3211" s="T538">say-IPFVZ.[3SG]</ta>
            <ta e="T540" id="Seg_3212" s="T539">eat-EP-IMP.2SG</ta>
            <ta e="T541" id="Seg_3213" s="T540">I.GEN</ta>
            <ta e="T542" id="Seg_3214" s="T541">pie-NOM/GEN/ACC.3SG</ta>
            <ta e="T543" id="Seg_3215" s="T542">this.[NOM.SG]</ta>
            <ta e="T544" id="Seg_3216" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_3217" s="T544">eat-PST.[3SG]</ta>
            <ta e="T546" id="Seg_3218" s="T545">stove-LAT</ta>
            <ta e="T548" id="Seg_3219" s="T547">climb-MOM-PST</ta>
            <ta e="T549" id="Seg_3220" s="T548">duck-PL</ta>
            <ta e="T550" id="Seg_3221" s="T549">fly-DUR-PST.[3SG]</ta>
            <ta e="T551" id="Seg_3222" s="T550">fly-DUR-PST.[3SG]</ta>
            <ta e="T552" id="Seg_3223" s="T551">where=INDEF</ta>
            <ta e="T553" id="Seg_3224" s="T552">NEG.EX.[3SG]</ta>
            <ta e="T554" id="Seg_3225" s="T553">return-MOM-PST-3PL</ta>
            <ta e="T555" id="Seg_3226" s="T554">woman-LAT</ta>
            <ta e="T556" id="Seg_3227" s="T555">and</ta>
            <ta e="T557" id="Seg_3228" s="T556">this-PL</ta>
            <ta e="T558" id="Seg_3229" s="T557">depart-CVB</ta>
            <ta e="T559" id="Seg_3230" s="T558">come-PST-3PL</ta>
            <ta e="T560" id="Seg_3231" s="T559">tent-LAT/LOC.3PL</ta>
            <ta e="T561" id="Seg_3232" s="T560">come-PST-3PL</ta>
            <ta e="T562" id="Seg_3233" s="T561">and</ta>
            <ta e="T563" id="Seg_3234" s="T562">father-NOM/GEN.3SG</ta>
            <ta e="T564" id="Seg_3235" s="T563">mother-NOM/GEN.3SG</ta>
            <ta e="T565" id="Seg_3236" s="T564">come-PST-3PL</ta>
            <ta e="T567" id="Seg_3237" s="T566">work-ABL</ta>
            <ta e="T568" id="Seg_3238" s="T567">all</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T228" id="Seg_3239" s="T227">жить-PST-3PL</ta>
            <ta e="T229" id="Seg_3240" s="T228">женщина.[NOM.SG]</ta>
            <ta e="T230" id="Seg_3241" s="T229">мужчина-INS</ta>
            <ta e="T231" id="Seg_3242" s="T230">этот-PL</ta>
            <ta e="T234" id="Seg_3243" s="T233">быть-PST.[3SG]</ta>
            <ta e="T235" id="Seg_3244" s="T234">девушка.[NOM.SG]</ta>
            <ta e="T236" id="Seg_3245" s="T235">и</ta>
            <ta e="T237" id="Seg_3246" s="T236">мальчик.[NOM.SG]</ta>
            <ta e="T238" id="Seg_3247" s="T237">этот-PL</ta>
            <ta e="T239" id="Seg_3248" s="T238">работать-INF.LAT</ta>
            <ta e="T240" id="Seg_3249" s="T239">пойти-DUR-3PL</ta>
            <ta e="T241" id="Seg_3250" s="T240">смотреть-IMP.2SG.O</ta>
            <ta e="T242" id="Seg_3251" s="T241">мальчик-ACC</ta>
            <ta e="T245" id="Seg_3252" s="T244">я.NOM</ta>
            <ta e="T246" id="Seg_3253" s="T245">ты.DAT</ta>
            <ta e="T247" id="Seg_3254" s="T246">взять-FUT-1SG</ta>
            <ta e="T248" id="Seg_3255" s="T247">платок.[NOM.SG]</ta>
            <ta e="T249" id="Seg_3256" s="T248">красивый.[NOM.SG]</ta>
            <ta e="T250" id="Seg_3257" s="T249">пойти-PST-3PL</ta>
            <ta e="T251" id="Seg_3258" s="T250">а</ta>
            <ta e="T252" id="Seg_3259" s="T251">дочь-NOM/GEN.3SG</ta>
            <ta e="T253" id="Seg_3260" s="T252">сын-ACC.3SG</ta>
            <ta e="T254" id="Seg_3261" s="T253">взять-PST.[3SG]</ta>
            <ta e="T255" id="Seg_3262" s="T254">снаружи</ta>
            <ta e="T256" id="Seg_3263" s="T255">сажать-PST.[3SG]</ta>
            <ta e="T257" id="Seg_3264" s="T256">трава-LOC</ta>
            <ta e="T258" id="Seg_3265" s="T257">а</ta>
            <ta e="T259" id="Seg_3266" s="T258">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T260" id="Seg_3267" s="T259">бежать-MOM-PST.[3SG]</ta>
            <ta e="T261" id="Seg_3268" s="T260">здесь</ta>
            <ta e="T262" id="Seg_3269" s="T261">утка-PL</ta>
            <ta e="T263" id="Seg_3270" s="T262">PREF-лететь-MOM-PST.[3SG]</ta>
            <ta e="T264" id="Seg_3271" s="T263">этот.[NOM.SG]</ta>
            <ta e="T265" id="Seg_3272" s="T264">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T268" id="Seg_3273" s="T267">взять-MOM-PST-3PL</ta>
            <ta e="T269" id="Seg_3274" s="T268">нести-RES-PST-3PL</ta>
            <ta e="T270" id="Seg_3275" s="T269">нести-PST-3PL</ta>
            <ta e="T271" id="Seg_3276" s="T270">девушка.[NOM.SG]</ta>
            <ta e="T273" id="Seg_3277" s="T272">бежать-CVB</ta>
            <ta e="T274" id="Seg_3278" s="T273">прийти-PST.[3SG]</ta>
            <ta e="T275" id="Seg_3279" s="T274">видеть-PRS-3SG.O</ta>
            <ta e="T276" id="Seg_3280" s="T275">NEG.EX.[3SG]</ta>
            <ta e="T277" id="Seg_3281" s="T276">NEG.EX.[3SG]</ta>
            <ta e="T278" id="Seg_3282" s="T277">мальчик.[NOM.SG]</ta>
            <ta e="T279" id="Seg_3283" s="T278">там_здесь</ta>
            <ta e="T280" id="Seg_3284" s="T279">бежать-DUR-PST.[3SG]</ta>
            <ta e="T281" id="Seg_3285" s="T280">тогда</ta>
            <ta e="T282" id="Seg_3286" s="T281">пойти-PST.[3SG]</ta>
            <ta e="T283" id="Seg_3287" s="T282">бежать-MOM-PST.[3SG]</ta>
            <ta e="T284" id="Seg_3288" s="T283">бежать-DUR.[3SG]</ta>
            <ta e="T285" id="Seg_3289" s="T284">бежать-DUR.[3SG]</ta>
            <ta e="T286" id="Seg_3290" s="T285">печь.[NOM.SG]</ta>
            <ta e="T287" id="Seg_3291" s="T286">стоять-DUR.[3SG]</ta>
            <ta e="T288" id="Seg_3292" s="T287">ты.NOM</ta>
            <ta e="T289" id="Seg_3293" s="T288">NEG</ta>
            <ta e="T290" id="Seg_3294" s="T289">видеть-PST-1SG</ta>
            <ta e="T292" id="Seg_3295" s="T291">где</ta>
            <ta e="T293" id="Seg_3296" s="T292">лететь-MOM-PST-3PL</ta>
            <ta e="T294" id="Seg_3297" s="T293">утка-PL</ta>
            <ta e="T295" id="Seg_3298" s="T294">этот.[NOM.SG]</ta>
            <ta e="T296" id="Seg_3299" s="T295">сказать-IPFVZ.[3SG]</ta>
            <ta e="T297" id="Seg_3300" s="T296">пирог.[NOM.SG]</ta>
            <ta e="T298" id="Seg_3301" s="T297">съесть-EP-IMP.2SG</ta>
            <ta e="T299" id="Seg_3302" s="T298">NEG</ta>
            <ta e="T300" id="Seg_3303" s="T299">мука.[NOM.SG]</ta>
            <ta e="T301" id="Seg_3304" s="T300">я.GEN</ta>
            <ta e="T302" id="Seg_3305" s="T301">отец-LAT/LOC.3SG</ta>
            <ta e="T303" id="Seg_3306" s="T302">мука.[NOM.SG]</ta>
            <ta e="T304" id="Seg_3307" s="T303">быть-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_3308" s="T304">и</ta>
            <ta e="T306" id="Seg_3309" s="T305">NEG</ta>
            <ta e="T307" id="Seg_3310" s="T306">съесть-PRS-1SG</ta>
            <ta e="T308" id="Seg_3311" s="T307">бежать-MOM-PST.[3SG]</ta>
            <ta e="T309" id="Seg_3312" s="T308">тогда</ta>
            <ta e="T310" id="Seg_3313" s="T309">прийти-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_3314" s="T310">тогда</ta>
            <ta e="T312" id="Seg_3315" s="T311">печь.[NOM.SG]</ta>
            <ta e="T313" id="Seg_3316" s="T312">что</ta>
            <ta e="T314" id="Seg_3317" s="T313">этот-LAT</ta>
            <ta e="T315" id="Seg_3318" s="T314">NEG</ta>
            <ta e="T316" id="Seg_3319" s="T315">сказать-PST.[3SG]</ta>
            <ta e="T317" id="Seg_3320" s="T316">этот.[NOM.SG]</ta>
            <ta e="T318" id="Seg_3321" s="T317">бежать-MOM-PST.[3SG]</ta>
            <ta e="T319" id="Seg_3322" s="T318">опять</ta>
            <ta e="T320" id="Seg_3323" s="T319">тогда</ta>
            <ta e="T321" id="Seg_3324" s="T320">прийти-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3325" s="T321">вода.[NOM.SG]</ta>
            <ta e="T323" id="Seg_3326" s="T322">течь-DUR.[3SG]</ta>
            <ta e="T324" id="Seg_3327" s="T323">есть-IMP.2SG</ta>
            <ta e="T325" id="Seg_3328" s="T324">кисель.[NOM.SG]</ta>
            <ta e="T326" id="Seg_3329" s="T325">я.GEN</ta>
            <ta e="T327" id="Seg_3330" s="T326">молоко-INS</ta>
            <ta e="T328" id="Seg_3331" s="T327">я.GEN</ta>
            <ta e="T329" id="Seg_3332" s="T328">отец-LAT/LOC.3SG</ta>
            <ta e="T330" id="Seg_3333" s="T329">NEG</ta>
            <ta e="T331" id="Seg_3334" s="T330">такой=INDEF</ta>
            <ta e="T332" id="Seg_3335" s="T331">NEG</ta>
            <ta e="T333" id="Seg_3336" s="T332">съесть-PRS-1SG</ta>
            <ta e="T334" id="Seg_3337" s="T333">этот.[NOM.SG]</ta>
            <ta e="T335" id="Seg_3338" s="T334">этот-LAT</ta>
            <ta e="T336" id="Seg_3339" s="T335">что.[NOM.SG]=INDEF</ta>
            <ta e="T337" id="Seg_3340" s="T336">NEG</ta>
            <ta e="T338" id="Seg_3341" s="T337">сказать-PST.[3SG]</ta>
            <ta e="T339" id="Seg_3342" s="T338">тогда</ta>
            <ta e="T340" id="Seg_3343" s="T339">бежать-DUR.[3SG]</ta>
            <ta e="T341" id="Seg_3344" s="T340">стоять-DUR.[3SG]</ta>
            <ta e="T342" id="Seg_3345" s="T341">яблоня.[NOM.SG]</ta>
            <ta e="T343" id="Seg_3346" s="T342">видеть-PST-2SG</ta>
            <ta e="T344" id="Seg_3347" s="T343">куда</ta>
            <ta e="T346" id="Seg_3348" s="T345">утка-PL</ta>
            <ta e="T347" id="Seg_3349" s="T346">лететь-MOM-PST-3PL</ta>
            <ta e="T348" id="Seg_3350" s="T347">есть-IMP.2SG</ta>
            <ta e="T349" id="Seg_3351" s="T348">я.GEN</ta>
            <ta e="T350" id="Seg_3352" s="T349">яблоко.[NOM.SG]</ta>
            <ta e="T351" id="Seg_3353" s="T350">я.GEN</ta>
            <ta e="T352" id="Seg_3354" s="T351">отец-LAT</ta>
            <ta e="T353" id="Seg_3355" s="T352">еще</ta>
            <ta e="T354" id="Seg_3356" s="T353">хороший.[NOM.SG]</ta>
            <ta e="T355" id="Seg_3357" s="T354">яблоко.[NOM.SG]</ta>
            <ta e="T356" id="Seg_3358" s="T355">и</ta>
            <ta e="T357" id="Seg_3359" s="T356">то</ta>
            <ta e="T358" id="Seg_3360" s="T357">NEG</ta>
            <ta e="T359" id="Seg_3361" s="T358">съесть-PRS-1SG</ta>
            <ta e="T360" id="Seg_3362" s="T359">и</ta>
            <ta e="T361" id="Seg_3363" s="T360">этот-LAT</ta>
            <ta e="T362" id="Seg_3364" s="T361">что.[NOM.SG]=INDEF</ta>
            <ta e="T363" id="Seg_3365" s="T362">NEG</ta>
            <ta e="T364" id="Seg_3366" s="T363">сказать-PST.[3SG]</ta>
            <ta e="T365" id="Seg_3367" s="T364">тогда</ta>
            <ta e="T367" id="Seg_3368" s="T366">пойти-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3369" s="T367">пойти-PST.[3SG]</ta>
            <ta e="T369" id="Seg_3370" s="T368">стоять-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_3371" s="T369">дом.[NOM.SG]</ta>
            <ta e="T371" id="Seg_3372" s="T370">этот.[NOM.SG]</ta>
            <ta e="T372" id="Seg_3373" s="T371">дом-LAT</ta>
            <ta e="T373" id="Seg_3374" s="T372">войти-PST.[3SG]</ta>
            <ta e="T374" id="Seg_3375" s="T373">там</ta>
            <ta e="T375" id="Seg_3376" s="T374">женщина.[NOM.SG]</ta>
            <ta e="T376" id="Seg_3377" s="T375">сидеть-DUR.[3SG]</ta>
            <ta e="T377" id="Seg_3378" s="T376">прясть-DUR.[3SG]</ta>
            <ta e="T378" id="Seg_3379" s="T377">мальчик.[NOM.SG]</ta>
            <ta e="T379" id="Seg_3380" s="T378">быть-PRS.[3SG]</ta>
            <ta e="T380" id="Seg_3381" s="T379">и</ta>
            <ta e="T381" id="Seg_3382" s="T380">там</ta>
            <ta e="T382" id="Seg_3383" s="T381">прясть-EP-IMP.2SG</ta>
            <ta e="T383" id="Seg_3384" s="T382">что.[NOM.SG]</ta>
            <ta e="T384" id="Seg_3385" s="T383">быть-CVB</ta>
            <ta e="T385" id="Seg_3386" s="T384">прийти-PST-2SG</ta>
            <ta e="T386" id="Seg_3387" s="T385">и</ta>
            <ta e="T387" id="Seg_3388" s="T386">я.NOM</ta>
            <ta e="T388" id="Seg_3389" s="T387">идти-PST-1SG</ta>
            <ta e="T389" id="Seg_3390" s="T388">и</ta>
            <ta e="T390" id="Seg_3391" s="T389">замерзнуть-RES-PST-1SG</ta>
            <ta e="T391" id="Seg_3392" s="T390">теплый-FACT-INF.LAT</ta>
            <ta e="T392" id="Seg_3393" s="T391">прийти-PST-1SG</ta>
            <ta e="T393" id="Seg_3394" s="T392">сидеть-IMP.2SG</ta>
            <ta e="T394" id="Seg_3395" s="T393">прясть-EP-IMP.2SG</ta>
            <ta e="T395" id="Seg_3396" s="T394">кудель.[NOM.SG]</ta>
            <ta e="T396" id="Seg_3397" s="T395">я.LAT</ta>
            <ta e="T397" id="Seg_3398" s="T396">сесть-PST.[3SG]</ta>
            <ta e="T398" id="Seg_3399" s="T397">тогда</ta>
            <ta e="T399" id="Seg_3400" s="T398">прясть-DUR.[3SG]</ta>
            <ta e="T400" id="Seg_3401" s="T399">а</ta>
            <ta e="T401" id="Seg_3402" s="T400">сын-NOM/GEN.3SG</ta>
            <ta e="T402" id="Seg_3403" s="T401">снаружи</ta>
            <ta e="T403" id="Seg_3404" s="T402">сидеть-DUR.[3SG]</ta>
            <ta e="T404" id="Seg_3405" s="T403">тогда</ta>
            <ta e="T405" id="Seg_3406" s="T404">мышь.[NOM.SG]</ta>
            <ta e="T406" id="Seg_3407" s="T405">уйти-PST.[3SG]</ta>
            <ta e="T407" id="Seg_3408" s="T406">сказать-IPFVZ.[3SG]</ta>
            <ta e="T408" id="Seg_3409" s="T407">девушка.[NOM.SG]</ta>
            <ta e="T409" id="Seg_3410" s="T408">ты.DAT</ta>
            <ta e="T410" id="Seg_3411" s="T409">женщина.[NOM.SG]</ta>
            <ta e="T411" id="Seg_3412" s="T410">гореть-DUR.[3SG]</ta>
            <ta e="T412" id="Seg_3413" s="T411">баня.[NOM.SG]</ta>
            <ta e="T413" id="Seg_3414" s="T412">и</ta>
            <ta e="T414" id="Seg_3415" s="T413">там</ta>
            <ta e="T415" id="Seg_3416" s="T414">ты.ACC</ta>
            <ta e="T416" id="Seg_3417" s="T415">гореть-FUT-3SG</ta>
            <ta e="T417" id="Seg_3418" s="T416">%%</ta>
            <ta e="T418" id="Seg_3419" s="T417">съесть-FUT-3SG</ta>
            <ta e="T419" id="Seg_3420" s="T418">товарищ-LAT/LOC.3SG</ta>
            <ta e="T420" id="Seg_3421" s="T419">PTCL</ta>
            <ta e="T421" id="Seg_3422" s="T420">сажать-FUT-3SG</ta>
            <ta e="T422" id="Seg_3423" s="T421">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T423" id="Seg_3424" s="T422">принести-IMP.2SG</ta>
            <ta e="T424" id="Seg_3425" s="T423">я.LAT</ta>
            <ta e="T425" id="Seg_3426" s="T424">съесть-INF.LAT</ta>
            <ta e="T427" id="Seg_3427" s="T426">кашка.[NOM.SG]</ta>
            <ta e="T428" id="Seg_3428" s="T427">этот.[NOM.SG]</ta>
            <ta e="T429" id="Seg_3429" s="T428">этот-LAT</ta>
            <ta e="T431" id="Seg_3430" s="T430">дать-PST.[3SG]</ta>
            <ta e="T432" id="Seg_3431" s="T431">пойти-EP-IMP.2SG</ta>
            <ta e="T433" id="Seg_3432" s="T432">а</ta>
            <ta e="T434" id="Seg_3433" s="T433">я.NOM</ta>
            <ta e="T435" id="Seg_3434" s="T434">прясть-FUT-1SG</ta>
            <ta e="T436" id="Seg_3435" s="T435">взять-IMP.2SG.O</ta>
            <ta e="T437" id="Seg_3436" s="T436">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T438" id="Seg_3437" s="T437">мальчик-GEN</ta>
            <ta e="T439" id="Seg_3438" s="T438">этот.[NOM.SG]</ta>
            <ta e="T440" id="Seg_3439" s="T439">хватать-CVB</ta>
            <ta e="T441" id="Seg_3440" s="T440">взять-PST.[3SG]</ta>
            <ta e="T442" id="Seg_3441" s="T441">и</ta>
            <ta e="T443" id="Seg_3442" s="T442">бежать-MOM-PST.[3SG]</ta>
            <ta e="T444" id="Seg_3443" s="T443">этот.[NOM.SG]</ta>
            <ta e="T445" id="Seg_3444" s="T444">женщина.[NOM.SG]</ta>
            <ta e="T446" id="Seg_3445" s="T445">прийти-FUT-3SG</ta>
            <ta e="T447" id="Seg_3446" s="T446">ты.NOM</ta>
            <ta e="T448" id="Seg_3447" s="T447">прясть-DUR-2SG</ta>
            <ta e="T449" id="Seg_3448" s="T448">этот.[NOM.SG]</ta>
            <ta e="T450" id="Seg_3449" s="T449">сказать-IPFVZ.[3SG]</ta>
            <ta e="T451" id="Seg_3450" s="T450">прясть-DUR-1SG</ta>
            <ta e="T452" id="Seg_3451" s="T451">тогда</ta>
            <ta e="T453" id="Seg_3452" s="T452">опять</ta>
            <ta e="T454" id="Seg_3453" s="T453">пойти-FUT-3SG</ta>
            <ta e="T455" id="Seg_3454" s="T454">баня.[NOM.SG]</ta>
            <ta e="T456" id="Seg_3455" s="T455">светить-INF.LAT</ta>
            <ta e="T457" id="Seg_3456" s="T456">этот.[NOM.SG]</ta>
            <ta e="T458" id="Seg_3457" s="T457">девушка.[NOM.SG]</ta>
            <ta e="T459" id="Seg_3458" s="T458">бежать-MOM-PST.[3SG]</ta>
            <ta e="T460" id="Seg_3459" s="T459">прийти-PST.[3SG]</ta>
            <ta e="T461" id="Seg_3460" s="T460">тогда</ta>
            <ta e="T462" id="Seg_3461" s="T461">женщина.[NOM.SG]</ta>
            <ta e="T464" id="Seg_3462" s="T463">дом-LAT</ta>
            <ta e="T465" id="Seg_3463" s="T464">прийти-PST.[3SG]</ta>
            <ta e="T466" id="Seg_3464" s="T465">девушка.[NOM.SG]</ta>
            <ta e="T467" id="Seg_3465" s="T466">NEG.EX.[3SG]</ta>
            <ta e="T468" id="Seg_3466" s="T467">и</ta>
            <ta e="T469" id="Seg_3467" s="T468">мальчик.[NOM.SG]</ta>
            <ta e="T470" id="Seg_3468" s="T469">NEG.EX.[3SG]</ta>
            <ta e="T471" id="Seg_3469" s="T470">тогда</ta>
            <ta e="T472" id="Seg_3470" s="T471">утка-3PL-LAT</ta>
            <ta e="T473" id="Seg_3471" s="T472">сказать-IPFVZ.[3SG]</ta>
            <ta e="T474" id="Seg_3472" s="T473">пойти-IMP.2PL</ta>
            <ta e="T475" id="Seg_3473" s="T474">смотреть-FRQ-IMP.2PL</ta>
            <ta e="T476" id="Seg_3474" s="T475">где</ta>
            <ta e="T477" id="Seg_3475" s="T476">девушка.[NOM.SG]</ta>
            <ta e="T478" id="Seg_3476" s="T477">этот.[NOM.SG]</ta>
            <ta e="T479" id="Seg_3477" s="T478">девушка.[NOM.SG]</ta>
            <ta e="T481" id="Seg_3478" s="T480">бежать-CVB</ta>
            <ta e="T482" id="Seg_3479" s="T481">прийти-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_3480" s="T482">вода-LAT</ta>
            <ta e="T484" id="Seg_3481" s="T483">прийти-PST.[3SG]</ta>
            <ta e="T485" id="Seg_3482" s="T484">вода.[NOM.SG]</ta>
            <ta e="T486" id="Seg_3483" s="T485">вода.[NOM.SG]</ta>
            <ta e="T487" id="Seg_3484" s="T486">прятать-EP-IMP.2SG</ta>
            <ta e="T488" id="Seg_3485" s="T487">я.ACC</ta>
            <ta e="T489" id="Seg_3486" s="T488">этот.[NOM.SG]</ta>
            <ta e="T490" id="Seg_3487" s="T489">сказать-IPFVZ.[3SG]</ta>
            <ta e="T492" id="Seg_3488" s="T491">есть-IMP.2SG</ta>
            <ta e="T493" id="Seg_3489" s="T492">я.GEN</ta>
            <ta e="T494" id="Seg_3490" s="T493">кисель-NOM/GEN/ACC.1SG</ta>
            <ta e="T495" id="Seg_3491" s="T494">этот.[NOM.SG]</ta>
            <ta e="T496" id="Seg_3492" s="T495">съесть-PST.[3SG]</ta>
            <ta e="T497" id="Seg_3493" s="T496">этот.[NOM.SG]</ta>
            <ta e="T498" id="Seg_3494" s="T497">этот-ACC</ta>
            <ta e="T499" id="Seg_3495" s="T498">закрыть-MOM-PST.[3SG]</ta>
            <ta e="T500" id="Seg_3496" s="T499">тогда</ta>
            <ta e="T501" id="Seg_3497" s="T500">утка-PL</ta>
            <ta e="T502" id="Seg_3498" s="T501">лететь-MOM-PST-3PL</ta>
            <ta e="T503" id="Seg_3499" s="T502">этот.[NOM.SG]</ta>
            <ta e="T504" id="Seg_3500" s="T503">PTCL</ta>
            <ta e="T505" id="Seg_3501" s="T504">уйти-PST.[3SG]</ta>
            <ta e="T506" id="Seg_3502" s="T505">опять</ta>
            <ta e="T507" id="Seg_3503" s="T506">бежать-DUR.[3SG]</ta>
            <ta e="T508" id="Seg_3504" s="T507">утка-PL</ta>
            <ta e="T509" id="Seg_3505" s="T508">опять</ta>
            <ta e="T510" id="Seg_3506" s="T509">лететь-DUR-3PL</ta>
            <ta e="T511" id="Seg_3507" s="T510">тогда</ta>
            <ta e="T512" id="Seg_3508" s="T511">яблоня-LAT</ta>
            <ta e="T513" id="Seg_3509" s="T512">сказать-IPFVZ.[3SG]</ta>
            <ta e="T514" id="Seg_3510" s="T513">спрятаться-RES-IMP.2SG.O</ta>
            <ta e="T515" id="Seg_3511" s="T514">мы.ACC</ta>
            <ta e="T516" id="Seg_3512" s="T515">съесть-EP-IMP.2SG</ta>
            <ta e="T517" id="Seg_3513" s="T516">яблоня.[NOM.SG]</ta>
            <ta e="T518" id="Seg_3514" s="T517">этот.[NOM.SG]</ta>
            <ta e="T519" id="Seg_3515" s="T518">съесть-PST.[3SG]</ta>
            <ta e="T520" id="Seg_3516" s="T519">этот.[NOM.SG]</ta>
            <ta e="T521" id="Seg_3517" s="T520">этот-ACC</ta>
            <ta e="T522" id="Seg_3518" s="T521">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T523" id="Seg_3519" s="T522">тогда</ta>
            <ta e="T524" id="Seg_3520" s="T523">утка-PL</ta>
            <ta e="T525" id="Seg_3521" s="T524">лететь-MOM-PST-3PL</ta>
            <ta e="T526" id="Seg_3522" s="T525">этот.[NOM.SG]</ta>
            <ta e="T527" id="Seg_3523" s="T526">этот-PL</ta>
            <ta e="T528" id="Seg_3524" s="T527">опять</ta>
            <ta e="T529" id="Seg_3525" s="T528">бежать-MOM-PST-3PL</ta>
            <ta e="T530" id="Seg_3526" s="T529">печь-LAT</ta>
            <ta e="T531" id="Seg_3527" s="T530">прийти-PST.[3SG]</ta>
            <ta e="T532" id="Seg_3528" s="T531">печь.[NOM.SG]</ta>
            <ta e="T533" id="Seg_3529" s="T532">сказать-IPFVZ.[3SG]</ta>
            <ta e="T534" id="Seg_3530" s="T533">утка-PL</ta>
            <ta e="T536" id="Seg_3531" s="T535">PTCL</ta>
            <ta e="T537" id="Seg_3532" s="T536">хватать-PRS-3PL-3PL</ta>
            <ta e="T538" id="Seg_3533" s="T537">печь.[NOM.SG]</ta>
            <ta e="T539" id="Seg_3534" s="T538">сказать-IPFVZ.[3SG]</ta>
            <ta e="T540" id="Seg_3535" s="T539">съесть-EP-IMP.2SG</ta>
            <ta e="T541" id="Seg_3536" s="T540">я.GEN</ta>
            <ta e="T542" id="Seg_3537" s="T541">пирог-NOM/GEN/ACC.3SG</ta>
            <ta e="T543" id="Seg_3538" s="T542">этот.[NOM.SG]</ta>
            <ta e="T544" id="Seg_3539" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_3540" s="T544">съесть-PST.[3SG]</ta>
            <ta e="T546" id="Seg_3541" s="T545">печь-LAT</ta>
            <ta e="T548" id="Seg_3542" s="T547">влезать-MOM-PST</ta>
            <ta e="T549" id="Seg_3543" s="T548">утка-PL</ta>
            <ta e="T550" id="Seg_3544" s="T549">лететь-DUR-PST.[3SG]</ta>
            <ta e="T551" id="Seg_3545" s="T550">лететь-DUR-PST.[3SG]</ta>
            <ta e="T552" id="Seg_3546" s="T551">где=INDEF</ta>
            <ta e="T553" id="Seg_3547" s="T552">NEG.EX.[3SG]</ta>
            <ta e="T554" id="Seg_3548" s="T553">вернуться-MOM-PST-3PL</ta>
            <ta e="T555" id="Seg_3549" s="T554">женщина-LAT</ta>
            <ta e="T556" id="Seg_3550" s="T555">а</ta>
            <ta e="T557" id="Seg_3551" s="T556">этот-PL</ta>
            <ta e="T558" id="Seg_3552" s="T557">уйти-CVB</ta>
            <ta e="T559" id="Seg_3553" s="T558">прийти-PST-3PL</ta>
            <ta e="T560" id="Seg_3554" s="T559">чум-LAT/LOC.3PL</ta>
            <ta e="T561" id="Seg_3555" s="T560">прийти-PST-3PL</ta>
            <ta e="T562" id="Seg_3556" s="T561">и</ta>
            <ta e="T563" id="Seg_3557" s="T562">отец-NOM/GEN.3SG</ta>
            <ta e="T564" id="Seg_3558" s="T563">мать-NOM/GEN.3SG</ta>
            <ta e="T565" id="Seg_3559" s="T564">прийти-PST-3PL</ta>
            <ta e="T567" id="Seg_3560" s="T566">работать-ABL</ta>
            <ta e="T568" id="Seg_3561" s="T567">весь</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T228" id="Seg_3562" s="T227">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_3563" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_3564" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_3565" s="T230">dempro-n:num</ta>
            <ta e="T234" id="Seg_3566" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_3567" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3568" s="T235">conj</ta>
            <ta e="T237" id="Seg_3569" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_3570" s="T237">dempro-n:num</ta>
            <ta e="T239" id="Seg_3571" s="T238">v-v:n.fin</ta>
            <ta e="T240" id="Seg_3572" s="T239">v-v&gt;v-v:pn</ta>
            <ta e="T241" id="Seg_3573" s="T240">v-v:mood.pn</ta>
            <ta e="T242" id="Seg_3574" s="T241">n-n:case</ta>
            <ta e="T245" id="Seg_3575" s="T244">pers</ta>
            <ta e="T246" id="Seg_3576" s="T245">pers</ta>
            <ta e="T247" id="Seg_3577" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_3578" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3579" s="T248">adj-n:case</ta>
            <ta e="T250" id="Seg_3580" s="T249">v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_3581" s="T250">conj</ta>
            <ta e="T252" id="Seg_3582" s="T251">n-n:case.poss</ta>
            <ta e="T253" id="Seg_3583" s="T252">n-n:case.poss</ta>
            <ta e="T254" id="Seg_3584" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_3585" s="T254">adv</ta>
            <ta e="T256" id="Seg_3586" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_3587" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_3588" s="T257">conj</ta>
            <ta e="T259" id="Seg_3589" s="T258">refl-n:case.poss</ta>
            <ta e="T260" id="Seg_3590" s="T259">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_3591" s="T260">adv</ta>
            <ta e="T262" id="Seg_3592" s="T261">n-n:num</ta>
            <ta e="T263" id="Seg_3593" s="T262">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_3594" s="T263">dempro-n:case</ta>
            <ta e="T265" id="Seg_3595" s="T264">n-n:case.poss</ta>
            <ta e="T268" id="Seg_3596" s="T267">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_3597" s="T268">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_3598" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_3599" s="T270">n-n:case</ta>
            <ta e="T273" id="Seg_3600" s="T272">v-v:n.fin</ta>
            <ta e="T274" id="Seg_3601" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_3602" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_3603" s="T275">v-v:pn</ta>
            <ta e="T277" id="Seg_3604" s="T276">v-v:pn</ta>
            <ta e="T278" id="Seg_3605" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_3606" s="T278">adv-adv</ta>
            <ta e="T280" id="Seg_3607" s="T279">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_3608" s="T280">adv</ta>
            <ta e="T282" id="Seg_3609" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_3610" s="T282">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_3611" s="T283">v-v&gt;v-v:pn</ta>
            <ta e="T285" id="Seg_3612" s="T284">v-v&gt;v-v:pn</ta>
            <ta e="T286" id="Seg_3613" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_3614" s="T286">v-v&gt;v-v:pn</ta>
            <ta e="T288" id="Seg_3615" s="T287">pers</ta>
            <ta e="T289" id="Seg_3616" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_3617" s="T289">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_3618" s="T291">que</ta>
            <ta e="T293" id="Seg_3619" s="T292">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_3620" s="T293">n-n:num</ta>
            <ta e="T295" id="Seg_3621" s="T294">dempro-n:case</ta>
            <ta e="T296" id="Seg_3622" s="T295">v-v&gt;v-v:pn</ta>
            <ta e="T297" id="Seg_3623" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_3624" s="T297">v-v:ins-v:mood.pn</ta>
            <ta e="T299" id="Seg_3625" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_3626" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_3627" s="T300">pers</ta>
            <ta e="T302" id="Seg_3628" s="T301">n-n:case.poss</ta>
            <ta e="T303" id="Seg_3629" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_3630" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_3631" s="T304">conj</ta>
            <ta e="T306" id="Seg_3632" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3633" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_3634" s="T307">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T309" id="Seg_3635" s="T308">adv</ta>
            <ta e="T310" id="Seg_3636" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_3637" s="T310">adv</ta>
            <ta e="T312" id="Seg_3638" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_3639" s="T312">que</ta>
            <ta e="T314" id="Seg_3640" s="T313">dempro-n:case</ta>
            <ta e="T315" id="Seg_3641" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_3642" s="T315">v-v:tense-v:pn</ta>
            <ta e="T317" id="Seg_3643" s="T316">dempro-n:case</ta>
            <ta e="T318" id="Seg_3644" s="T317">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_3645" s="T318">adv</ta>
            <ta e="T320" id="Seg_3646" s="T319">adv</ta>
            <ta e="T321" id="Seg_3647" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_3648" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_3649" s="T322">v-v&gt;v-v:pn</ta>
            <ta e="T324" id="Seg_3650" s="T323">v-v:mood.pn</ta>
            <ta e="T325" id="Seg_3651" s="T324">n-n:case</ta>
            <ta e="T326" id="Seg_3652" s="T325">pers</ta>
            <ta e="T327" id="Seg_3653" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_3654" s="T327">pers</ta>
            <ta e="T329" id="Seg_3655" s="T328">n-n:case.poss</ta>
            <ta e="T330" id="Seg_3656" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_3657" s="T330">adj=ptcl</ta>
            <ta e="T332" id="Seg_3658" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_3659" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_3660" s="T333">dempro-n:case</ta>
            <ta e="T335" id="Seg_3661" s="T334">dempro-n:case</ta>
            <ta e="T336" id="Seg_3662" s="T335">que-n:case=ptcl</ta>
            <ta e="T337" id="Seg_3663" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_3664" s="T337">v-v:tense-v:pn</ta>
            <ta e="T339" id="Seg_3665" s="T338">adv</ta>
            <ta e="T340" id="Seg_3666" s="T339">v-v&gt;v-v:pn</ta>
            <ta e="T341" id="Seg_3667" s="T340">v-v&gt;v-v:pn</ta>
            <ta e="T342" id="Seg_3668" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_3669" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_3670" s="T343">que</ta>
            <ta e="T346" id="Seg_3671" s="T345">n-n:num</ta>
            <ta e="T347" id="Seg_3672" s="T346">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_3673" s="T347">v-v:mood.pn</ta>
            <ta e="T349" id="Seg_3674" s="T348">pers</ta>
            <ta e="T350" id="Seg_3675" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_3676" s="T350">pers</ta>
            <ta e="T352" id="Seg_3677" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_3678" s="T352">adv</ta>
            <ta e="T354" id="Seg_3679" s="T353">adj-n:case</ta>
            <ta e="T355" id="Seg_3680" s="T354">n-n:case</ta>
            <ta e="T356" id="Seg_3681" s="T355">conj</ta>
            <ta e="T357" id="Seg_3682" s="T356">conj</ta>
            <ta e="T358" id="Seg_3683" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_3684" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_3685" s="T359">conj</ta>
            <ta e="T361" id="Seg_3686" s="T360">dempro-n:case</ta>
            <ta e="T362" id="Seg_3687" s="T361">que-n:case=ptcl</ta>
            <ta e="T363" id="Seg_3688" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_3689" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_3690" s="T364">adv</ta>
            <ta e="T367" id="Seg_3691" s="T366">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_3692" s="T367">v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_3693" s="T368">v-v&gt;v-v:pn</ta>
            <ta e="T370" id="Seg_3694" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_3695" s="T370">dempro-n:case</ta>
            <ta e="T372" id="Seg_3696" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_3697" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_3698" s="T373">adv</ta>
            <ta e="T375" id="Seg_3699" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_3700" s="T375">v-v&gt;v-v:pn</ta>
            <ta e="T377" id="Seg_3701" s="T376">v-v&gt;v-v:pn</ta>
            <ta e="T378" id="Seg_3702" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_3703" s="T378">v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_3704" s="T379">conj</ta>
            <ta e="T381" id="Seg_3705" s="T380">adv</ta>
            <ta e="T382" id="Seg_3706" s="T381">v-v:ins-v:mood.pn</ta>
            <ta e="T383" id="Seg_3707" s="T382">que-n:case</ta>
            <ta e="T384" id="Seg_3708" s="T383">v-v:n.fin</ta>
            <ta e="T385" id="Seg_3709" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_3710" s="T385">conj</ta>
            <ta e="T387" id="Seg_3711" s="T386">pers</ta>
            <ta e="T388" id="Seg_3712" s="T387">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_3713" s="T388">conj</ta>
            <ta e="T390" id="Seg_3714" s="T389">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T391" id="Seg_3715" s="T390">adj-adj&gt;v-v:n.fin</ta>
            <ta e="T392" id="Seg_3716" s="T391">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_3717" s="T392">v-v:mood.pn</ta>
            <ta e="T394" id="Seg_3718" s="T393">v-v:ins-v:mood.pn</ta>
            <ta e="T395" id="Seg_3719" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_3720" s="T395">pers</ta>
            <ta e="T397" id="Seg_3721" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_3722" s="T397">adv</ta>
            <ta e="T399" id="Seg_3723" s="T398">v-v&gt;v-v:pn</ta>
            <ta e="T400" id="Seg_3724" s="T399">conj</ta>
            <ta e="T401" id="Seg_3725" s="T400">n-n:case.poss</ta>
            <ta e="T402" id="Seg_3726" s="T401">adv</ta>
            <ta e="T403" id="Seg_3727" s="T402">v-v&gt;v-v:pn</ta>
            <ta e="T404" id="Seg_3728" s="T403">adv</ta>
            <ta e="T405" id="Seg_3729" s="T404">n-n:case</ta>
            <ta e="T406" id="Seg_3730" s="T405">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_3731" s="T406">v-v&gt;v-v:pn</ta>
            <ta e="T408" id="Seg_3732" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_3733" s="T408">pers</ta>
            <ta e="T410" id="Seg_3734" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_3735" s="T410">v-v&gt;v-v:pn</ta>
            <ta e="T412" id="Seg_3736" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_3737" s="T412">conj</ta>
            <ta e="T414" id="Seg_3738" s="T413">adv</ta>
            <ta e="T415" id="Seg_3739" s="T414">pers</ta>
            <ta e="T416" id="Seg_3740" s="T415">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_3741" s="T416">%%</ta>
            <ta e="T418" id="Seg_3742" s="T417">v-v:tense-v:pn</ta>
            <ta e="T419" id="Seg_3743" s="T418">n-n:case.poss</ta>
            <ta e="T420" id="Seg_3744" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_3745" s="T420">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_3746" s="T421">refl-n:case.poss</ta>
            <ta e="T423" id="Seg_3747" s="T422">v-v:mood.pn</ta>
            <ta e="T424" id="Seg_3748" s="T423">pers</ta>
            <ta e="T425" id="Seg_3749" s="T424">v-v:n.fin</ta>
            <ta e="T427" id="Seg_3750" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_3751" s="T427">dempro-n:case</ta>
            <ta e="T429" id="Seg_3752" s="T428">dempro-n:case</ta>
            <ta e="T431" id="Seg_3753" s="T430">v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_3754" s="T431">v-v:ins-v:mood.pn</ta>
            <ta e="T433" id="Seg_3755" s="T432">conj</ta>
            <ta e="T434" id="Seg_3756" s="T433">pers</ta>
            <ta e="T435" id="Seg_3757" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_3758" s="T435">v-v:mood.pn</ta>
            <ta e="T437" id="Seg_3759" s="T436">refl-n:case.poss</ta>
            <ta e="T438" id="Seg_3760" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_3761" s="T438">dempro-n:case</ta>
            <ta e="T440" id="Seg_3762" s="T439">v-v:n.fin</ta>
            <ta e="T441" id="Seg_3763" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_3764" s="T441">conj</ta>
            <ta e="T443" id="Seg_3765" s="T442">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_3766" s="T443">dempro-n:case</ta>
            <ta e="T445" id="Seg_3767" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_3768" s="T445">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_3769" s="T446">pers</ta>
            <ta e="T448" id="Seg_3770" s="T447">v-v&gt;v-v:pn</ta>
            <ta e="T449" id="Seg_3771" s="T448">dempro-n:case</ta>
            <ta e="T450" id="Seg_3772" s="T449">v-v&gt;v-v:pn</ta>
            <ta e="T451" id="Seg_3773" s="T450">v-v&gt;v-v:pn</ta>
            <ta e="T452" id="Seg_3774" s="T451">adv</ta>
            <ta e="T453" id="Seg_3775" s="T452">adv</ta>
            <ta e="T454" id="Seg_3776" s="T453">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_3777" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_3778" s="T455">v-v:n.fin</ta>
            <ta e="T457" id="Seg_3779" s="T456">dempro-n:case</ta>
            <ta e="T458" id="Seg_3780" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_3781" s="T458">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_3782" s="T459">v-v:tense-v:pn</ta>
            <ta e="T461" id="Seg_3783" s="T460">adv</ta>
            <ta e="T462" id="Seg_3784" s="T461">n-n:case</ta>
            <ta e="T464" id="Seg_3785" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_3786" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_3787" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_3788" s="T466">v-v:pn</ta>
            <ta e="T468" id="Seg_3789" s="T467">conj</ta>
            <ta e="T469" id="Seg_3790" s="T468">n-n:case</ta>
            <ta e="T470" id="Seg_3791" s="T469">v-v:pn</ta>
            <ta e="T471" id="Seg_3792" s="T470">adv</ta>
            <ta e="T472" id="Seg_3793" s="T471">n-n:case.poss-n:case</ta>
            <ta e="T473" id="Seg_3794" s="T472">v-v&gt;v-v:pn</ta>
            <ta e="T474" id="Seg_3795" s="T473">v-v:mood.pn</ta>
            <ta e="T475" id="Seg_3796" s="T474">v-v&gt;v-v:mood.pn</ta>
            <ta e="T476" id="Seg_3797" s="T475">que</ta>
            <ta e="T477" id="Seg_3798" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_3799" s="T477">dempro-n:case</ta>
            <ta e="T479" id="Seg_3800" s="T478">n-n:case</ta>
            <ta e="T481" id="Seg_3801" s="T480">v-v:n.fin</ta>
            <ta e="T482" id="Seg_3802" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_3803" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_3804" s="T483">v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_3805" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_3806" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_3807" s="T486">v-v:ins-v:mood.pn</ta>
            <ta e="T488" id="Seg_3808" s="T487">pers</ta>
            <ta e="T489" id="Seg_3809" s="T488">dempro-n:case</ta>
            <ta e="T490" id="Seg_3810" s="T489">v-v&gt;v-v:pn</ta>
            <ta e="T492" id="Seg_3811" s="T491">v-v:mood.pn</ta>
            <ta e="T493" id="Seg_3812" s="T492">pers</ta>
            <ta e="T494" id="Seg_3813" s="T493">n-n:case.poss</ta>
            <ta e="T495" id="Seg_3814" s="T494">dempro-n:case</ta>
            <ta e="T496" id="Seg_3815" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_3816" s="T496">dempro-n:case</ta>
            <ta e="T498" id="Seg_3817" s="T497">dempro-n:case</ta>
            <ta e="T499" id="Seg_3818" s="T498">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T500" id="Seg_3819" s="T499">adv</ta>
            <ta e="T501" id="Seg_3820" s="T500">n-n:num</ta>
            <ta e="T502" id="Seg_3821" s="T501">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_3822" s="T502">dempro-n:case</ta>
            <ta e="T504" id="Seg_3823" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_3824" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_3825" s="T505">adv</ta>
            <ta e="T507" id="Seg_3826" s="T506">v-v&gt;v-v:pn</ta>
            <ta e="T508" id="Seg_3827" s="T507">n-n:num</ta>
            <ta e="T509" id="Seg_3828" s="T508">adv</ta>
            <ta e="T510" id="Seg_3829" s="T509">v-v&gt;v-v:pn</ta>
            <ta e="T511" id="Seg_3830" s="T510">adv</ta>
            <ta e="T512" id="Seg_3831" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_3832" s="T512">v-v&gt;v-v:pn</ta>
            <ta e="T514" id="Seg_3833" s="T513">v-v&gt;v-v:mood.pn</ta>
            <ta e="T515" id="Seg_3834" s="T514">pers</ta>
            <ta e="T516" id="Seg_3835" s="T515">v-v:ins-v:mood.pn</ta>
            <ta e="T517" id="Seg_3836" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_3837" s="T517">dempro-n:case</ta>
            <ta e="T519" id="Seg_3838" s="T518">v-v:tense-v:pn</ta>
            <ta e="T520" id="Seg_3839" s="T519">dempro-n:case</ta>
            <ta e="T521" id="Seg_3840" s="T520">dempro-n:case</ta>
            <ta e="T522" id="Seg_3841" s="T521">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_3842" s="T522">adv</ta>
            <ta e="T524" id="Seg_3843" s="T523">n-n:num</ta>
            <ta e="T525" id="Seg_3844" s="T524">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T526" id="Seg_3845" s="T525">dempro-n:case</ta>
            <ta e="T527" id="Seg_3846" s="T526">dempro-n:num</ta>
            <ta e="T528" id="Seg_3847" s="T527">adv</ta>
            <ta e="T529" id="Seg_3848" s="T528">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_3849" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_3850" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_3851" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_3852" s="T532">v-v&gt;v-v:pn</ta>
            <ta e="T534" id="Seg_3853" s="T533">n-n:num</ta>
            <ta e="T536" id="Seg_3854" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_3855" s="T536">v-v:tense-v:pn-v:pn</ta>
            <ta e="T538" id="Seg_3856" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_3857" s="T538">v-v&gt;v-v:pn</ta>
            <ta e="T540" id="Seg_3858" s="T539">v-v:ins-v:mood.pn</ta>
            <ta e="T541" id="Seg_3859" s="T540">pers</ta>
            <ta e="T542" id="Seg_3860" s="T541">n-n:case.poss</ta>
            <ta e="T543" id="Seg_3861" s="T542">dempro-n:case</ta>
            <ta e="T544" id="Seg_3862" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_3863" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_3864" s="T545">n-n:case</ta>
            <ta e="T548" id="Seg_3865" s="T547">v-v&gt;v-v:tense</ta>
            <ta e="T549" id="Seg_3866" s="T548">n-n:num</ta>
            <ta e="T550" id="Seg_3867" s="T549">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_3868" s="T550">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_3869" s="T551">que=ptcl</ta>
            <ta e="T553" id="Seg_3870" s="T552">v-v:pn</ta>
            <ta e="T554" id="Seg_3871" s="T553">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_3872" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_3873" s="T555">conj</ta>
            <ta e="T557" id="Seg_3874" s="T556">dempro-n:num</ta>
            <ta e="T558" id="Seg_3875" s="T557">v-v:n.fin</ta>
            <ta e="T559" id="Seg_3876" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_3877" s="T559">n-n:case.poss</ta>
            <ta e="T561" id="Seg_3878" s="T560">v-v:tense-v:pn</ta>
            <ta e="T562" id="Seg_3879" s="T561">conj</ta>
            <ta e="T563" id="Seg_3880" s="T562">n-n:case.poss</ta>
            <ta e="T564" id="Seg_3881" s="T563">n-n:case.poss</ta>
            <ta e="T565" id="Seg_3882" s="T564">v-v:tense-v:pn</ta>
            <ta e="T567" id="Seg_3883" s="T566">v-n:case</ta>
            <ta e="T568" id="Seg_3884" s="T567">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T228" id="Seg_3885" s="T227">v</ta>
            <ta e="T229" id="Seg_3886" s="T228">n</ta>
            <ta e="T230" id="Seg_3887" s="T229">n</ta>
            <ta e="T231" id="Seg_3888" s="T230">dempro</ta>
            <ta e="T234" id="Seg_3889" s="T233">v</ta>
            <ta e="T235" id="Seg_3890" s="T234">n</ta>
            <ta e="T236" id="Seg_3891" s="T235">conj</ta>
            <ta e="T237" id="Seg_3892" s="T236">n</ta>
            <ta e="T238" id="Seg_3893" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3894" s="T238">v</ta>
            <ta e="T240" id="Seg_3895" s="T239">v</ta>
            <ta e="T241" id="Seg_3896" s="T240">v</ta>
            <ta e="T242" id="Seg_3897" s="T241">n</ta>
            <ta e="T245" id="Seg_3898" s="T244">pers</ta>
            <ta e="T246" id="Seg_3899" s="T245">pers</ta>
            <ta e="T247" id="Seg_3900" s="T246">v</ta>
            <ta e="T248" id="Seg_3901" s="T247">n</ta>
            <ta e="T249" id="Seg_3902" s="T248">adj</ta>
            <ta e="T250" id="Seg_3903" s="T249">v</ta>
            <ta e="T251" id="Seg_3904" s="T250">conj</ta>
            <ta e="T252" id="Seg_3905" s="T251">n</ta>
            <ta e="T253" id="Seg_3906" s="T252">n</ta>
            <ta e="T254" id="Seg_3907" s="T253">v</ta>
            <ta e="T255" id="Seg_3908" s="T254">adv</ta>
            <ta e="T256" id="Seg_3909" s="T255">v</ta>
            <ta e="T257" id="Seg_3910" s="T256">n</ta>
            <ta e="T258" id="Seg_3911" s="T257">conj</ta>
            <ta e="T259" id="Seg_3912" s="T258">refl</ta>
            <ta e="T260" id="Seg_3913" s="T259">v</ta>
            <ta e="T261" id="Seg_3914" s="T260">adv</ta>
            <ta e="T262" id="Seg_3915" s="T261">n</ta>
            <ta e="T263" id="Seg_3916" s="T262">v</ta>
            <ta e="T264" id="Seg_3917" s="T263">dempro</ta>
            <ta e="T265" id="Seg_3918" s="T264">n</ta>
            <ta e="T268" id="Seg_3919" s="T267">v</ta>
            <ta e="T269" id="Seg_3920" s="T268">v</ta>
            <ta e="T270" id="Seg_3921" s="T269">v</ta>
            <ta e="T271" id="Seg_3922" s="T270">n</ta>
            <ta e="T273" id="Seg_3923" s="T272">v</ta>
            <ta e="T274" id="Seg_3924" s="T273">v</ta>
            <ta e="T275" id="Seg_3925" s="T274">v</ta>
            <ta e="T276" id="Seg_3926" s="T275">v</ta>
            <ta e="T277" id="Seg_3927" s="T276">v</ta>
            <ta e="T278" id="Seg_3928" s="T277">n</ta>
            <ta e="T279" id="Seg_3929" s="T278">adv</ta>
            <ta e="T280" id="Seg_3930" s="T279">v</ta>
            <ta e="T281" id="Seg_3931" s="T280">adv</ta>
            <ta e="T282" id="Seg_3932" s="T281">v</ta>
            <ta e="T283" id="Seg_3933" s="T282">v</ta>
            <ta e="T284" id="Seg_3934" s="T283">v</ta>
            <ta e="T285" id="Seg_3935" s="T284">v</ta>
            <ta e="T286" id="Seg_3936" s="T285">n</ta>
            <ta e="T287" id="Seg_3937" s="T286">v</ta>
            <ta e="T288" id="Seg_3938" s="T287">pers</ta>
            <ta e="T289" id="Seg_3939" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_3940" s="T289">v</ta>
            <ta e="T292" id="Seg_3941" s="T291">que</ta>
            <ta e="T293" id="Seg_3942" s="T292">v</ta>
            <ta e="T294" id="Seg_3943" s="T293">n</ta>
            <ta e="T295" id="Seg_3944" s="T294">dempro</ta>
            <ta e="T296" id="Seg_3945" s="T295">v</ta>
            <ta e="T297" id="Seg_3946" s="T296">n</ta>
            <ta e="T298" id="Seg_3947" s="T297">v</ta>
            <ta e="T299" id="Seg_3948" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_3949" s="T299">n</ta>
            <ta e="T301" id="Seg_3950" s="T300">pers</ta>
            <ta e="T302" id="Seg_3951" s="T301">n</ta>
            <ta e="T303" id="Seg_3952" s="T302">n</ta>
            <ta e="T304" id="Seg_3953" s="T303">v</ta>
            <ta e="T305" id="Seg_3954" s="T304">conj</ta>
            <ta e="T306" id="Seg_3955" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3956" s="T306">v</ta>
            <ta e="T308" id="Seg_3957" s="T307">v</ta>
            <ta e="T309" id="Seg_3958" s="T308">adv</ta>
            <ta e="T310" id="Seg_3959" s="T309">v</ta>
            <ta e="T311" id="Seg_3960" s="T310">adv</ta>
            <ta e="T312" id="Seg_3961" s="T311">n</ta>
            <ta e="T313" id="Seg_3962" s="T312">que</ta>
            <ta e="T314" id="Seg_3963" s="T313">dempro</ta>
            <ta e="T315" id="Seg_3964" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_3965" s="T315">v</ta>
            <ta e="T317" id="Seg_3966" s="T316">dempro</ta>
            <ta e="T318" id="Seg_3967" s="T317">v</ta>
            <ta e="T319" id="Seg_3968" s="T318">adv</ta>
            <ta e="T320" id="Seg_3969" s="T319">adv</ta>
            <ta e="T321" id="Seg_3970" s="T320">v</ta>
            <ta e="T322" id="Seg_3971" s="T321">n</ta>
            <ta e="T323" id="Seg_3972" s="T322">v</ta>
            <ta e="T324" id="Seg_3973" s="T323">n</ta>
            <ta e="T325" id="Seg_3974" s="T324">n</ta>
            <ta e="T326" id="Seg_3975" s="T325">pers</ta>
            <ta e="T327" id="Seg_3976" s="T326">n</ta>
            <ta e="T328" id="Seg_3977" s="T327">pers</ta>
            <ta e="T329" id="Seg_3978" s="T328">n</ta>
            <ta e="T330" id="Seg_3979" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_3980" s="T330">adj</ta>
            <ta e="T332" id="Seg_3981" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_3982" s="T332">v</ta>
            <ta e="T334" id="Seg_3983" s="T333">dempro</ta>
            <ta e="T335" id="Seg_3984" s="T334">dempro</ta>
            <ta e="T336" id="Seg_3985" s="T335">que</ta>
            <ta e="T337" id="Seg_3986" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_3987" s="T337">v</ta>
            <ta e="T339" id="Seg_3988" s="T338">adv</ta>
            <ta e="T340" id="Seg_3989" s="T339">v</ta>
            <ta e="T341" id="Seg_3990" s="T340">v</ta>
            <ta e="T342" id="Seg_3991" s="T341">n</ta>
            <ta e="T343" id="Seg_3992" s="T342">v</ta>
            <ta e="T344" id="Seg_3993" s="T343">que</ta>
            <ta e="T346" id="Seg_3994" s="T345">n</ta>
            <ta e="T347" id="Seg_3995" s="T346">v</ta>
            <ta e="T348" id="Seg_3996" s="T347">v</ta>
            <ta e="T349" id="Seg_3997" s="T348">pers</ta>
            <ta e="T350" id="Seg_3998" s="T349">n</ta>
            <ta e="T351" id="Seg_3999" s="T350">pers</ta>
            <ta e="T352" id="Seg_4000" s="T351">n</ta>
            <ta e="T353" id="Seg_4001" s="T352">adv</ta>
            <ta e="T354" id="Seg_4002" s="T353">adj</ta>
            <ta e="T355" id="Seg_4003" s="T354">n</ta>
            <ta e="T356" id="Seg_4004" s="T355">conj</ta>
            <ta e="T357" id="Seg_4005" s="T356">conj</ta>
            <ta e="T358" id="Seg_4006" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_4007" s="T358">v</ta>
            <ta e="T360" id="Seg_4008" s="T359">conj</ta>
            <ta e="T361" id="Seg_4009" s="T360">dempro</ta>
            <ta e="T362" id="Seg_4010" s="T361">que</ta>
            <ta e="T363" id="Seg_4011" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_4012" s="T363">v</ta>
            <ta e="T365" id="Seg_4013" s="T364">adv</ta>
            <ta e="T367" id="Seg_4014" s="T366">v</ta>
            <ta e="T368" id="Seg_4015" s="T367">v</ta>
            <ta e="T369" id="Seg_4016" s="T368">v</ta>
            <ta e="T370" id="Seg_4017" s="T369">n</ta>
            <ta e="T371" id="Seg_4018" s="T370">dempro</ta>
            <ta e="T372" id="Seg_4019" s="T371">n</ta>
            <ta e="T373" id="Seg_4020" s="T372">v</ta>
            <ta e="T374" id="Seg_4021" s="T373">adv</ta>
            <ta e="T375" id="Seg_4022" s="T374">n</ta>
            <ta e="T376" id="Seg_4023" s="T375">v</ta>
            <ta e="T377" id="Seg_4024" s="T376">v</ta>
            <ta e="T378" id="Seg_4025" s="T377">n</ta>
            <ta e="T379" id="Seg_4026" s="T378">v</ta>
            <ta e="T380" id="Seg_4027" s="T379">conj</ta>
            <ta e="T381" id="Seg_4028" s="T380">adv</ta>
            <ta e="T382" id="Seg_4029" s="T381">v</ta>
            <ta e="T383" id="Seg_4030" s="T382">que</ta>
            <ta e="T384" id="Seg_4031" s="T383">v</ta>
            <ta e="T385" id="Seg_4032" s="T384">v</ta>
            <ta e="T386" id="Seg_4033" s="T385">conj</ta>
            <ta e="T387" id="Seg_4034" s="T386">pers</ta>
            <ta e="T388" id="Seg_4035" s="T387">v</ta>
            <ta e="T389" id="Seg_4036" s="T388">conj</ta>
            <ta e="T390" id="Seg_4037" s="T389">v</ta>
            <ta e="T391" id="Seg_4038" s="T390">v</ta>
            <ta e="T392" id="Seg_4039" s="T391">v</ta>
            <ta e="T393" id="Seg_4040" s="T392">v</ta>
            <ta e="T394" id="Seg_4041" s="T393">v</ta>
            <ta e="T395" id="Seg_4042" s="T394">n</ta>
            <ta e="T396" id="Seg_4043" s="T395">pers</ta>
            <ta e="T397" id="Seg_4044" s="T396">v</ta>
            <ta e="T398" id="Seg_4045" s="T397">adv</ta>
            <ta e="T399" id="Seg_4046" s="T398">v</ta>
            <ta e="T400" id="Seg_4047" s="T399">conj</ta>
            <ta e="T401" id="Seg_4048" s="T400">n</ta>
            <ta e="T402" id="Seg_4049" s="T401">adv</ta>
            <ta e="T403" id="Seg_4050" s="T402">v</ta>
            <ta e="T404" id="Seg_4051" s="T403">adv</ta>
            <ta e="T405" id="Seg_4052" s="T404">n</ta>
            <ta e="T406" id="Seg_4053" s="T405">v</ta>
            <ta e="T407" id="Seg_4054" s="T406">v</ta>
            <ta e="T408" id="Seg_4055" s="T407">n</ta>
            <ta e="T409" id="Seg_4056" s="T408">pers</ta>
            <ta e="T410" id="Seg_4057" s="T409">n</ta>
            <ta e="T411" id="Seg_4058" s="T410">v</ta>
            <ta e="T412" id="Seg_4059" s="T411">n</ta>
            <ta e="T413" id="Seg_4060" s="T412">conj</ta>
            <ta e="T414" id="Seg_4061" s="T413">adv</ta>
            <ta e="T415" id="Seg_4062" s="T414">pers</ta>
            <ta e="T416" id="Seg_4063" s="T415">v</ta>
            <ta e="T418" id="Seg_4064" s="T417">v</ta>
            <ta e="T419" id="Seg_4065" s="T418">n</ta>
            <ta e="T420" id="Seg_4066" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_4067" s="T420">v</ta>
            <ta e="T422" id="Seg_4068" s="T421">refl</ta>
            <ta e="T423" id="Seg_4069" s="T422">v</ta>
            <ta e="T424" id="Seg_4070" s="T423">pers</ta>
            <ta e="T425" id="Seg_4071" s="T424">v</ta>
            <ta e="T427" id="Seg_4072" s="T426">n</ta>
            <ta e="T428" id="Seg_4073" s="T427">dempro</ta>
            <ta e="T429" id="Seg_4074" s="T428">dempro</ta>
            <ta e="T431" id="Seg_4075" s="T430">v</ta>
            <ta e="T432" id="Seg_4076" s="T431">v</ta>
            <ta e="T433" id="Seg_4077" s="T432">conj</ta>
            <ta e="T434" id="Seg_4078" s="T433">pers</ta>
            <ta e="T435" id="Seg_4079" s="T434">v</ta>
            <ta e="T436" id="Seg_4080" s="T435">v</ta>
            <ta e="T437" id="Seg_4081" s="T436">refl</ta>
            <ta e="T438" id="Seg_4082" s="T437">n</ta>
            <ta e="T439" id="Seg_4083" s="T438">dempro</ta>
            <ta e="T440" id="Seg_4084" s="T439">v</ta>
            <ta e="T441" id="Seg_4085" s="T440">v</ta>
            <ta e="T442" id="Seg_4086" s="T441">conj</ta>
            <ta e="T443" id="Seg_4087" s="T442">v</ta>
            <ta e="T444" id="Seg_4088" s="T443">dempro</ta>
            <ta e="T445" id="Seg_4089" s="T444">n</ta>
            <ta e="T446" id="Seg_4090" s="T445">v</ta>
            <ta e="T447" id="Seg_4091" s="T446">pers</ta>
            <ta e="T448" id="Seg_4092" s="T447">v</ta>
            <ta e="T449" id="Seg_4093" s="T448">dempro</ta>
            <ta e="T450" id="Seg_4094" s="T449">v</ta>
            <ta e="T451" id="Seg_4095" s="T450">v</ta>
            <ta e="T452" id="Seg_4096" s="T451">adv</ta>
            <ta e="T453" id="Seg_4097" s="T452">adv</ta>
            <ta e="T454" id="Seg_4098" s="T453">v</ta>
            <ta e="T455" id="Seg_4099" s="T454">n</ta>
            <ta e="T456" id="Seg_4100" s="T455">v</ta>
            <ta e="T457" id="Seg_4101" s="T456">dempro</ta>
            <ta e="T458" id="Seg_4102" s="T457">n</ta>
            <ta e="T459" id="Seg_4103" s="T458">v</ta>
            <ta e="T460" id="Seg_4104" s="T459">v</ta>
            <ta e="T461" id="Seg_4105" s="T460">adv</ta>
            <ta e="T462" id="Seg_4106" s="T461">n</ta>
            <ta e="T464" id="Seg_4107" s="T463">n</ta>
            <ta e="T465" id="Seg_4108" s="T464">v</ta>
            <ta e="T466" id="Seg_4109" s="T465">n</ta>
            <ta e="T467" id="Seg_4110" s="T466">v</ta>
            <ta e="T468" id="Seg_4111" s="T467">conj</ta>
            <ta e="T469" id="Seg_4112" s="T468">n</ta>
            <ta e="T470" id="Seg_4113" s="T469">v</ta>
            <ta e="T471" id="Seg_4114" s="T470">adv</ta>
            <ta e="T472" id="Seg_4115" s="T471">n</ta>
            <ta e="T473" id="Seg_4116" s="T472">v</ta>
            <ta e="T474" id="Seg_4117" s="T473">v</ta>
            <ta e="T475" id="Seg_4118" s="T474">v</ta>
            <ta e="T476" id="Seg_4119" s="T475">que</ta>
            <ta e="T477" id="Seg_4120" s="T476">n</ta>
            <ta e="T478" id="Seg_4121" s="T477">dempro</ta>
            <ta e="T479" id="Seg_4122" s="T478">n</ta>
            <ta e="T481" id="Seg_4123" s="T480">v</ta>
            <ta e="T482" id="Seg_4124" s="T481">v</ta>
            <ta e="T483" id="Seg_4125" s="T482">n</ta>
            <ta e="T484" id="Seg_4126" s="T483">v</ta>
            <ta e="T485" id="Seg_4127" s="T484">n</ta>
            <ta e="T486" id="Seg_4128" s="T485">n</ta>
            <ta e="T487" id="Seg_4129" s="T486">v</ta>
            <ta e="T488" id="Seg_4130" s="T487">pers</ta>
            <ta e="T489" id="Seg_4131" s="T488">dempro</ta>
            <ta e="T490" id="Seg_4132" s="T489">v</ta>
            <ta e="T492" id="Seg_4133" s="T491">n</ta>
            <ta e="T493" id="Seg_4134" s="T492">pers</ta>
            <ta e="T494" id="Seg_4135" s="T493">n</ta>
            <ta e="T495" id="Seg_4136" s="T494">dempro</ta>
            <ta e="T496" id="Seg_4137" s="T495">v</ta>
            <ta e="T497" id="Seg_4138" s="T496">dempro</ta>
            <ta e="T498" id="Seg_4139" s="T497">dempro</ta>
            <ta e="T499" id="Seg_4140" s="T498">v</ta>
            <ta e="T500" id="Seg_4141" s="T499">adv</ta>
            <ta e="T501" id="Seg_4142" s="T500">n</ta>
            <ta e="T502" id="Seg_4143" s="T501">v</ta>
            <ta e="T503" id="Seg_4144" s="T502">dempro</ta>
            <ta e="T504" id="Seg_4145" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_4146" s="T504">v</ta>
            <ta e="T506" id="Seg_4147" s="T505">adv</ta>
            <ta e="T507" id="Seg_4148" s="T506">v</ta>
            <ta e="T508" id="Seg_4149" s="T507">n</ta>
            <ta e="T509" id="Seg_4150" s="T508">adv</ta>
            <ta e="T510" id="Seg_4151" s="T509">v</ta>
            <ta e="T511" id="Seg_4152" s="T510">adv</ta>
            <ta e="T512" id="Seg_4153" s="T511">n</ta>
            <ta e="T513" id="Seg_4154" s="T512">v</ta>
            <ta e="T514" id="Seg_4155" s="T513">v</ta>
            <ta e="T515" id="Seg_4156" s="T514">pers</ta>
            <ta e="T516" id="Seg_4157" s="T515">v</ta>
            <ta e="T517" id="Seg_4158" s="T516">n</ta>
            <ta e="T518" id="Seg_4159" s="T517">dempro</ta>
            <ta e="T519" id="Seg_4160" s="T518">v</ta>
            <ta e="T520" id="Seg_4161" s="T519">dempro</ta>
            <ta e="T521" id="Seg_4162" s="T520">dempro</ta>
            <ta e="T522" id="Seg_4163" s="T521">v</ta>
            <ta e="T523" id="Seg_4164" s="T522">adv</ta>
            <ta e="T524" id="Seg_4165" s="T523">n</ta>
            <ta e="T525" id="Seg_4166" s="T524">v</ta>
            <ta e="T526" id="Seg_4167" s="T525">dempro</ta>
            <ta e="T527" id="Seg_4168" s="T526">dempro</ta>
            <ta e="T528" id="Seg_4169" s="T527">adv</ta>
            <ta e="T529" id="Seg_4170" s="T528">v</ta>
            <ta e="T530" id="Seg_4171" s="T529">n</ta>
            <ta e="T531" id="Seg_4172" s="T530">v</ta>
            <ta e="T532" id="Seg_4173" s="T531">n</ta>
            <ta e="T533" id="Seg_4174" s="T532">v</ta>
            <ta e="T534" id="Seg_4175" s="T533">n</ta>
            <ta e="T536" id="Seg_4176" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_4177" s="T536">v</ta>
            <ta e="T538" id="Seg_4178" s="T537">n</ta>
            <ta e="T539" id="Seg_4179" s="T538">v</ta>
            <ta e="T540" id="Seg_4180" s="T539">v</ta>
            <ta e="T541" id="Seg_4181" s="T540">pers</ta>
            <ta e="T542" id="Seg_4182" s="T541">n</ta>
            <ta e="T543" id="Seg_4183" s="T542">dempro</ta>
            <ta e="T544" id="Seg_4184" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_4185" s="T544">v</ta>
            <ta e="T546" id="Seg_4186" s="T545">n</ta>
            <ta e="T548" id="Seg_4187" s="T547">v</ta>
            <ta e="T549" id="Seg_4188" s="T548">n</ta>
            <ta e="T550" id="Seg_4189" s="T549">v</ta>
            <ta e="T551" id="Seg_4190" s="T550">v</ta>
            <ta e="T552" id="Seg_4191" s="T551">que</ta>
            <ta e="T553" id="Seg_4192" s="T552">v</ta>
            <ta e="T554" id="Seg_4193" s="T553">v</ta>
            <ta e="T555" id="Seg_4194" s="T554">n</ta>
            <ta e="T556" id="Seg_4195" s="T555">conj</ta>
            <ta e="T557" id="Seg_4196" s="T556">dempro</ta>
            <ta e="T558" id="Seg_4197" s="T557">v</ta>
            <ta e="T559" id="Seg_4198" s="T558">v</ta>
            <ta e="T560" id="Seg_4199" s="T559">n</ta>
            <ta e="T561" id="Seg_4200" s="T560">v</ta>
            <ta e="T562" id="Seg_4201" s="T561">conj</ta>
            <ta e="T563" id="Seg_4202" s="T562">n</ta>
            <ta e="T564" id="Seg_4203" s="T563">n</ta>
            <ta e="T565" id="Seg_4204" s="T564">v</ta>
            <ta e="T567" id="Seg_4205" s="T566">n</ta>
            <ta e="T568" id="Seg_4206" s="T567">quant</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T229" id="Seg_4207" s="T228">np.h:E</ta>
            <ta e="T230" id="Seg_4208" s="T229">np.h:Com</ta>
            <ta e="T231" id="Seg_4209" s="T230">pro.h:Poss</ta>
            <ta e="T235" id="Seg_4210" s="T234">np.h:E</ta>
            <ta e="T237" id="Seg_4211" s="T236">np.h:E</ta>
            <ta e="T238" id="Seg_4212" s="T237">pro.h:A</ta>
            <ta e="T241" id="Seg_4213" s="T240">0.2.h:A</ta>
            <ta e="T242" id="Seg_4214" s="T241">np.h:Th</ta>
            <ta e="T245" id="Seg_4215" s="T244">pro.h:A</ta>
            <ta e="T246" id="Seg_4216" s="T245">pro.h:B</ta>
            <ta e="T248" id="Seg_4217" s="T247">np:Th</ta>
            <ta e="T250" id="Seg_4218" s="T249">0.3.h:A</ta>
            <ta e="T252" id="Seg_4219" s="T251">np.h:A</ta>
            <ta e="T253" id="Seg_4220" s="T252">np.h:Th</ta>
            <ta e="T255" id="Seg_4221" s="T254">adv:L</ta>
            <ta e="T256" id="Seg_4222" s="T255">0.3.h:A</ta>
            <ta e="T257" id="Seg_4223" s="T256">np:L</ta>
            <ta e="T259" id="Seg_4224" s="T258">pro.h:A</ta>
            <ta e="T261" id="Seg_4225" s="T260">adv:L</ta>
            <ta e="T262" id="Seg_4226" s="T261">np.h:A</ta>
            <ta e="T264" id="Seg_4227" s="T263">pro.h:A</ta>
            <ta e="T265" id="Seg_4228" s="T264">np.h:Th</ta>
            <ta e="T270" id="Seg_4229" s="T269">0.3.h:A</ta>
            <ta e="T271" id="Seg_4230" s="T270">np.h:A</ta>
            <ta e="T275" id="Seg_4231" s="T274">0.3.h:E</ta>
            <ta e="T276" id="Seg_4232" s="T275">0.3.h:E</ta>
            <ta e="T278" id="Seg_4233" s="T277">np.h:E</ta>
            <ta e="T279" id="Seg_4234" s="T278">adv:L</ta>
            <ta e="T280" id="Seg_4235" s="T279">0.3.h:A</ta>
            <ta e="T281" id="Seg_4236" s="T280">adv:Time</ta>
            <ta e="T282" id="Seg_4237" s="T281">0.3.h:A</ta>
            <ta e="T283" id="Seg_4238" s="T282">0.3.h:A</ta>
            <ta e="T284" id="Seg_4239" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_4240" s="T284">0.3.h:A</ta>
            <ta e="T286" id="Seg_4241" s="T285">np.h:E</ta>
            <ta e="T288" id="Seg_4242" s="T287">pro.h:E</ta>
            <ta e="T294" id="Seg_4243" s="T293">np.h:A</ta>
            <ta e="T295" id="Seg_4244" s="T294">pro.h:A</ta>
            <ta e="T297" id="Seg_4245" s="T296">np:P</ta>
            <ta e="T298" id="Seg_4246" s="T297">0.2.h:A</ta>
            <ta e="T300" id="Seg_4247" s="T299">np:Th</ta>
            <ta e="T301" id="Seg_4248" s="T300">pro.h:Poss</ta>
            <ta e="T302" id="Seg_4249" s="T301">np.h:Poss</ta>
            <ta e="T303" id="Seg_4250" s="T302">np:Th</ta>
            <ta e="T307" id="Seg_4251" s="T306">0.1.h:A</ta>
            <ta e="T308" id="Seg_4252" s="T307">0.3.h:A</ta>
            <ta e="T309" id="Seg_4253" s="T308">adv:Time</ta>
            <ta e="T310" id="Seg_4254" s="T309">0.3.h:A</ta>
            <ta e="T311" id="Seg_4255" s="T310">adv:Time</ta>
            <ta e="T312" id="Seg_4256" s="T311">np.h:A</ta>
            <ta e="T313" id="Seg_4257" s="T312">pro:Th</ta>
            <ta e="T314" id="Seg_4258" s="T313">pro.h:R</ta>
            <ta e="T317" id="Seg_4259" s="T316">pro.h:A</ta>
            <ta e="T320" id="Seg_4260" s="T319">adv:Time</ta>
            <ta e="T321" id="Seg_4261" s="T320">0.3.h:A</ta>
            <ta e="T322" id="Seg_4262" s="T321">np:Th</ta>
            <ta e="T324" id="Seg_4263" s="T323">0.2.h:A</ta>
            <ta e="T325" id="Seg_4264" s="T324">np:P</ta>
            <ta e="T326" id="Seg_4265" s="T325">pro.h:Poss</ta>
            <ta e="T327" id="Seg_4266" s="T326">np:Th</ta>
            <ta e="T328" id="Seg_4267" s="T327">pro.h:Poss</ta>
            <ta e="T329" id="Seg_4268" s="T328">np.h:Poss</ta>
            <ta e="T333" id="Seg_4269" s="T332">0.1.h:A</ta>
            <ta e="T334" id="Seg_4270" s="T333">pro.h:A</ta>
            <ta e="T335" id="Seg_4271" s="T334">pro.h:R</ta>
            <ta e="T336" id="Seg_4272" s="T335">pro:Th</ta>
            <ta e="T338" id="Seg_4273" s="T337">0.3.h:A</ta>
            <ta e="T339" id="Seg_4274" s="T338">adv:Time</ta>
            <ta e="T340" id="Seg_4275" s="T339">0.3.h:A</ta>
            <ta e="T342" id="Seg_4276" s="T341">np.h:E</ta>
            <ta e="T343" id="Seg_4277" s="T342">0.2.h:E</ta>
            <ta e="T346" id="Seg_4278" s="T345">np.h:A</ta>
            <ta e="T348" id="Seg_4279" s="T347">0.2.h:A</ta>
            <ta e="T349" id="Seg_4280" s="T348">pro.h:Poss</ta>
            <ta e="T350" id="Seg_4281" s="T349">np:P</ta>
            <ta e="T351" id="Seg_4282" s="T350">pro.h:Poss</ta>
            <ta e="T352" id="Seg_4283" s="T351">np.h:Poss</ta>
            <ta e="T355" id="Seg_4284" s="T354">np:Th</ta>
            <ta e="T357" id="Seg_4285" s="T356">adv:Time</ta>
            <ta e="T359" id="Seg_4286" s="T358">0.1.h:A</ta>
            <ta e="T361" id="Seg_4287" s="T360">pro.h:R</ta>
            <ta e="T362" id="Seg_4288" s="T361">pro:Th</ta>
            <ta e="T364" id="Seg_4289" s="T363">0.3.h:A</ta>
            <ta e="T365" id="Seg_4290" s="T364">adv:Time</ta>
            <ta e="T367" id="Seg_4291" s="T366">0.3.h:A</ta>
            <ta e="T368" id="Seg_4292" s="T367">0.3.h:A</ta>
            <ta e="T370" id="Seg_4293" s="T369">np:Th</ta>
            <ta e="T371" id="Seg_4294" s="T370">pro.h:A</ta>
            <ta e="T372" id="Seg_4295" s="T371">np:G</ta>
            <ta e="T374" id="Seg_4296" s="T373">adv:L</ta>
            <ta e="T375" id="Seg_4297" s="T374">np.h:A</ta>
            <ta e="T377" id="Seg_4298" s="T376">0.3.h:A</ta>
            <ta e="T378" id="Seg_4299" s="T377">np.h:E</ta>
            <ta e="T381" id="Seg_4300" s="T380">adv:L</ta>
            <ta e="T382" id="Seg_4301" s="T381">0.2.h:A</ta>
            <ta e="T385" id="Seg_4302" s="T384">0.2.h:A</ta>
            <ta e="T387" id="Seg_4303" s="T386">pro.h:A</ta>
            <ta e="T390" id="Seg_4304" s="T389">0.1.h:E</ta>
            <ta e="T392" id="Seg_4305" s="T391">0.1.h:A</ta>
            <ta e="T393" id="Seg_4306" s="T392">0.2.h:A</ta>
            <ta e="T394" id="Seg_4307" s="T393">0.2.h:A</ta>
            <ta e="T395" id="Seg_4308" s="T394">np:P</ta>
            <ta e="T396" id="Seg_4309" s="T395">pro.h:B</ta>
            <ta e="T397" id="Seg_4310" s="T396">0.3.h:A</ta>
            <ta e="T398" id="Seg_4311" s="T397">adv:Time</ta>
            <ta e="T399" id="Seg_4312" s="T398">0.3.h:A</ta>
            <ta e="T401" id="Seg_4313" s="T400">np.h:E</ta>
            <ta e="T402" id="Seg_4314" s="T401">adv:L</ta>
            <ta e="T404" id="Seg_4315" s="T403">adv:Time</ta>
            <ta e="T405" id="Seg_4316" s="T404">np.h:A</ta>
            <ta e="T407" id="Seg_4317" s="T406">0.3.h:A</ta>
            <ta e="T409" id="Seg_4318" s="T408">pro.h:B</ta>
            <ta e="T410" id="Seg_4319" s="T409">np.h:A</ta>
            <ta e="T412" id="Seg_4320" s="T411">np:P</ta>
            <ta e="T414" id="Seg_4321" s="T413">adv:L</ta>
            <ta e="T415" id="Seg_4322" s="T414">pro.h:P</ta>
            <ta e="T416" id="Seg_4323" s="T415">0.3.h:A</ta>
            <ta e="T418" id="Seg_4324" s="T417">0.3.h:A</ta>
            <ta e="T421" id="Seg_4325" s="T420">0.3.h:A</ta>
            <ta e="T422" id="Seg_4326" s="T421">pro.h:Th</ta>
            <ta e="T423" id="Seg_4327" s="T422">0.2.h:A</ta>
            <ta e="T424" id="Seg_4328" s="T423">pro.h:R</ta>
            <ta e="T427" id="Seg_4329" s="T426">np:Th</ta>
            <ta e="T428" id="Seg_4330" s="T427">pro.h:A</ta>
            <ta e="T429" id="Seg_4331" s="T428">pro.h:R</ta>
            <ta e="T432" id="Seg_4332" s="T431">0.2.h:A</ta>
            <ta e="T434" id="Seg_4333" s="T433">pro.h:A</ta>
            <ta e="T436" id="Seg_4334" s="T435">0.2.h:A</ta>
            <ta e="T437" id="Seg_4335" s="T436">pro.h:Poss</ta>
            <ta e="T438" id="Seg_4336" s="T437">np.h:Th</ta>
            <ta e="T439" id="Seg_4337" s="T438">pro.h:A</ta>
            <ta e="T443" id="Seg_4338" s="T442">0.3.h:A</ta>
            <ta e="T445" id="Seg_4339" s="T444">np.h:A</ta>
            <ta e="T447" id="Seg_4340" s="T446">pro.h:A</ta>
            <ta e="T449" id="Seg_4341" s="T448">pro.h:A</ta>
            <ta e="T451" id="Seg_4342" s="T450">0.1.h:A</ta>
            <ta e="T452" id="Seg_4343" s="T451">adv:Time</ta>
            <ta e="T454" id="Seg_4344" s="T453">0.3.h:A</ta>
            <ta e="T455" id="Seg_4345" s="T454">np:P</ta>
            <ta e="T458" id="Seg_4346" s="T457">np.h:A</ta>
            <ta e="T460" id="Seg_4347" s="T459">0.3.h:A</ta>
            <ta e="T461" id="Seg_4348" s="T460">adv:Time</ta>
            <ta e="T462" id="Seg_4349" s="T461">np.h:A</ta>
            <ta e="T464" id="Seg_4350" s="T463">np:G</ta>
            <ta e="T466" id="Seg_4351" s="T465">np.h:E</ta>
            <ta e="T469" id="Seg_4352" s="T468">np.h:E</ta>
            <ta e="T471" id="Seg_4353" s="T470">adv:Time</ta>
            <ta e="T472" id="Seg_4354" s="T471">np.h:R</ta>
            <ta e="T473" id="Seg_4355" s="T472">0.3.h:A</ta>
            <ta e="T474" id="Seg_4356" s="T473">0.2.h:A</ta>
            <ta e="T475" id="Seg_4357" s="T474">0.2.h:A</ta>
            <ta e="T477" id="Seg_4358" s="T476">np.h:Th</ta>
            <ta e="T479" id="Seg_4359" s="T478">np.h:A</ta>
            <ta e="T483" id="Seg_4360" s="T482">np:G</ta>
            <ta e="T484" id="Seg_4361" s="T483">0.3.h:A</ta>
            <ta e="T487" id="Seg_4362" s="T486">0.2.h:A</ta>
            <ta e="T488" id="Seg_4363" s="T487">pro.h:Th</ta>
            <ta e="T489" id="Seg_4364" s="T488">pro.h:A</ta>
            <ta e="T492" id="Seg_4365" s="T491">0.2.h:A</ta>
            <ta e="T493" id="Seg_4366" s="T492">pro.h:Poss</ta>
            <ta e="T494" id="Seg_4367" s="T493">np:P</ta>
            <ta e="T495" id="Seg_4368" s="T494">pro.h:A</ta>
            <ta e="T497" id="Seg_4369" s="T496">pro.h:A</ta>
            <ta e="T498" id="Seg_4370" s="T497">pro.h:Th</ta>
            <ta e="T500" id="Seg_4371" s="T499">adv:Time</ta>
            <ta e="T501" id="Seg_4372" s="T500">np.h:A</ta>
            <ta e="T503" id="Seg_4373" s="T502">pro.h:A</ta>
            <ta e="T507" id="Seg_4374" s="T506">0.3.h:A</ta>
            <ta e="T508" id="Seg_4375" s="T507">np.h:A</ta>
            <ta e="T511" id="Seg_4376" s="T510">adv:Time</ta>
            <ta e="T512" id="Seg_4377" s="T511">np.h:R</ta>
            <ta e="T513" id="Seg_4378" s="T512">0.2.h:A</ta>
            <ta e="T514" id="Seg_4379" s="T513">0.2.h:A</ta>
            <ta e="T515" id="Seg_4380" s="T514">pro.h:Th</ta>
            <ta e="T516" id="Seg_4381" s="T515">0.2.h:A</ta>
            <ta e="T517" id="Seg_4382" s="T516">np:P</ta>
            <ta e="T518" id="Seg_4383" s="T517">pro.h:A</ta>
            <ta e="T520" id="Seg_4384" s="T519">pro.h:A</ta>
            <ta e="T521" id="Seg_4385" s="T520">pro.h:Th</ta>
            <ta e="T523" id="Seg_4386" s="T522">adv:Time</ta>
            <ta e="T524" id="Seg_4387" s="T523">np.h:A</ta>
            <ta e="T527" id="Seg_4388" s="T526">pro.h:A</ta>
            <ta e="T530" id="Seg_4389" s="T529">np:G</ta>
            <ta e="T531" id="Seg_4390" s="T530">0.3.h:A</ta>
            <ta e="T532" id="Seg_4391" s="T531">np.h:A</ta>
            <ta e="T534" id="Seg_4392" s="T533">np.h:A</ta>
            <ta e="T538" id="Seg_4393" s="T537">np.h:A</ta>
            <ta e="T540" id="Seg_4394" s="T539">0.2.h:A</ta>
            <ta e="T541" id="Seg_4395" s="T540">pro.h:Poss</ta>
            <ta e="T542" id="Seg_4396" s="T541">np:P</ta>
            <ta e="T543" id="Seg_4397" s="T542">pro.h:A</ta>
            <ta e="T546" id="Seg_4398" s="T545">np:G</ta>
            <ta e="T548" id="Seg_4399" s="T547">0.3.h:A</ta>
            <ta e="T549" id="Seg_4400" s="T548">np.h:A</ta>
            <ta e="T551" id="Seg_4401" s="T550">0.3.h:A</ta>
            <ta e="T553" id="Seg_4402" s="T552">0.3.h:E</ta>
            <ta e="T554" id="Seg_4403" s="T553">0.3.h:A</ta>
            <ta e="T555" id="Seg_4404" s="T554">np:G</ta>
            <ta e="T557" id="Seg_4405" s="T556">pro.h:A</ta>
            <ta e="T560" id="Seg_4406" s="T559">np:G</ta>
            <ta e="T561" id="Seg_4407" s="T560">0.3.h:A</ta>
            <ta e="T563" id="Seg_4408" s="T562">np.h:A</ta>
            <ta e="T564" id="Seg_4409" s="T563">np.h:A</ta>
            <ta e="T567" id="Seg_4410" s="T566">np:So</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T228" id="Seg_4411" s="T227">v:pred</ta>
            <ta e="T229" id="Seg_4412" s="T228">np.h:S</ta>
            <ta e="T234" id="Seg_4413" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_4414" s="T234">np.h:S</ta>
            <ta e="T237" id="Seg_4415" s="T236">np.h:S</ta>
            <ta e="T238" id="Seg_4416" s="T237">pro.h:S</ta>
            <ta e="T239" id="Seg_4417" s="T238">s:purp</ta>
            <ta e="T240" id="Seg_4418" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_4419" s="T240">v:pred 0.2.h:S</ta>
            <ta e="T242" id="Seg_4420" s="T241">np.h:O</ta>
            <ta e="T245" id="Seg_4421" s="T244">pro.h:S</ta>
            <ta e="T247" id="Seg_4422" s="T246">v:pred</ta>
            <ta e="T248" id="Seg_4423" s="T247">np:O</ta>
            <ta e="T250" id="Seg_4424" s="T249">v:pred 0.3.h:S</ta>
            <ta e="T252" id="Seg_4425" s="T251">np.h:S</ta>
            <ta e="T253" id="Seg_4426" s="T252">np.h:O</ta>
            <ta e="T254" id="Seg_4427" s="T253">v:pred</ta>
            <ta e="T256" id="Seg_4428" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T259" id="Seg_4429" s="T258">pro.h:S</ta>
            <ta e="T260" id="Seg_4430" s="T259">v:pred</ta>
            <ta e="T262" id="Seg_4431" s="T261">np.h:S</ta>
            <ta e="T263" id="Seg_4432" s="T262">v:pred</ta>
            <ta e="T264" id="Seg_4433" s="T263">pro.h:S</ta>
            <ta e="T265" id="Seg_4434" s="T264">np.h:O</ta>
            <ta e="T268" id="Seg_4435" s="T267">v:pred</ta>
            <ta e="T270" id="Seg_4436" s="T269">v:pred 0.3.h:S</ta>
            <ta e="T271" id="Seg_4437" s="T270">np.h:S</ta>
            <ta e="T273" id="Seg_4438" s="T272">conv:pred</ta>
            <ta e="T274" id="Seg_4439" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_4440" s="T274">v:pred 0.3.h:S</ta>
            <ta e="T276" id="Seg_4441" s="T275">v:pred 0.3.h:S</ta>
            <ta e="T277" id="Seg_4442" s="T276">v:pred</ta>
            <ta e="T278" id="Seg_4443" s="T277">np.h:S</ta>
            <ta e="T280" id="Seg_4444" s="T279">v:pred 0.3.h:S</ta>
            <ta e="T282" id="Seg_4445" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_4446" s="T282">v:pred 0.3.h:S</ta>
            <ta e="T284" id="Seg_4447" s="T283">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_4448" s="T284">v:pred 0.3.h:S</ta>
            <ta e="T286" id="Seg_4449" s="T285">np.h:S</ta>
            <ta e="T287" id="Seg_4450" s="T286">v:pred</ta>
            <ta e="T288" id="Seg_4451" s="T287">pro.h:S</ta>
            <ta e="T289" id="Seg_4452" s="T288">ptcl.neg</ta>
            <ta e="T290" id="Seg_4453" s="T289">v:pred</ta>
            <ta e="T293" id="Seg_4454" s="T292">v:pred</ta>
            <ta e="T294" id="Seg_4455" s="T293">np.h:S</ta>
            <ta e="T295" id="Seg_4456" s="T294">pro.h:S</ta>
            <ta e="T296" id="Seg_4457" s="T295">v:pred</ta>
            <ta e="T297" id="Seg_4458" s="T296">np:O</ta>
            <ta e="T298" id="Seg_4459" s="T297">v:pred 0.2.h:S</ta>
            <ta e="T299" id="Seg_4460" s="T298">ptcl.neg</ta>
            <ta e="T300" id="Seg_4461" s="T299">n:pred</ta>
            <ta e="T303" id="Seg_4462" s="T302">n:pred</ta>
            <ta e="T304" id="Seg_4463" s="T303">cop</ta>
            <ta e="T306" id="Seg_4464" s="T305">ptcl.neg</ta>
            <ta e="T307" id="Seg_4465" s="T306">v:pred 0.1.h:S</ta>
            <ta e="T308" id="Seg_4466" s="T307">v:pred 0.3.h:S</ta>
            <ta e="T310" id="Seg_4467" s="T309">v:pred 0.3.h:S</ta>
            <ta e="T312" id="Seg_4468" s="T311">np.h:S</ta>
            <ta e="T313" id="Seg_4469" s="T312">pro:O</ta>
            <ta e="T315" id="Seg_4470" s="T314">ptcl:pred</ta>
            <ta e="T316" id="Seg_4471" s="T315">v:pred</ta>
            <ta e="T317" id="Seg_4472" s="T316">pro.h:S</ta>
            <ta e="T318" id="Seg_4473" s="T317">v:pred</ta>
            <ta e="T321" id="Seg_4474" s="T320">v:pred 0.3.h:S</ta>
            <ta e="T322" id="Seg_4475" s="T321">np:S</ta>
            <ta e="T323" id="Seg_4476" s="T322">v:pred</ta>
            <ta e="T324" id="Seg_4477" s="T323">v:pred 0.2.h:S</ta>
            <ta e="T325" id="Seg_4478" s="T324">np:O</ta>
            <ta e="T330" id="Seg_4479" s="T329">ptcl.neg</ta>
            <ta e="T332" id="Seg_4480" s="T331">ptcl.neg</ta>
            <ta e="T333" id="Seg_4481" s="T332">v:pred 0.1.h:S</ta>
            <ta e="T334" id="Seg_4482" s="T333">pro.h:S</ta>
            <ta e="T336" id="Seg_4483" s="T335">pro:O</ta>
            <ta e="T337" id="Seg_4484" s="T336">ptcl:pred</ta>
            <ta e="T338" id="Seg_4485" s="T337">v:pred 0.3.h:S</ta>
            <ta e="T340" id="Seg_4486" s="T339">v:pred 0.3.h:S</ta>
            <ta e="T341" id="Seg_4487" s="T340">v:pred</ta>
            <ta e="T342" id="Seg_4488" s="T341">np.h:S</ta>
            <ta e="T343" id="Seg_4489" s="T342">v:pred 0.2.h:S</ta>
            <ta e="T346" id="Seg_4490" s="T345">np.h:S</ta>
            <ta e="T347" id="Seg_4491" s="T346">v:pred</ta>
            <ta e="T348" id="Seg_4492" s="T347">v:pred 0.2.h:S</ta>
            <ta e="T350" id="Seg_4493" s="T349">np:O</ta>
            <ta e="T354" id="Seg_4494" s="T353">adj:pred</ta>
            <ta e="T355" id="Seg_4495" s="T354">np:S</ta>
            <ta e="T358" id="Seg_4496" s="T357">ptcl.neg</ta>
            <ta e="T359" id="Seg_4497" s="T358">v:pred 0.1.h:S</ta>
            <ta e="T362" id="Seg_4498" s="T361">pro:O</ta>
            <ta e="T363" id="Seg_4499" s="T362">ptcl.neg</ta>
            <ta e="T364" id="Seg_4500" s="T363">v:pred 0.3.h:S</ta>
            <ta e="T367" id="Seg_4501" s="T366">v:pred 0.3.h:S</ta>
            <ta e="T368" id="Seg_4502" s="T367">v:pred 0.3.h:S</ta>
            <ta e="T369" id="Seg_4503" s="T368">v:pred</ta>
            <ta e="T370" id="Seg_4504" s="T369">np:S</ta>
            <ta e="T371" id="Seg_4505" s="T370">pro.h:S</ta>
            <ta e="T373" id="Seg_4506" s="T372">v:pred</ta>
            <ta e="T375" id="Seg_4507" s="T374">np.h:S</ta>
            <ta e="T376" id="Seg_4508" s="T375">v:pred</ta>
            <ta e="T377" id="Seg_4509" s="T376">v:pred 0.3.h:S</ta>
            <ta e="T378" id="Seg_4510" s="T377">np.h:S</ta>
            <ta e="T379" id="Seg_4511" s="T378">v:pred</ta>
            <ta e="T382" id="Seg_4512" s="T381">v:pred 0.2.h:S</ta>
            <ta e="T384" id="Seg_4513" s="T383">conv:pred</ta>
            <ta e="T385" id="Seg_4514" s="T384">v:pred 0.2.h:S</ta>
            <ta e="T387" id="Seg_4515" s="T386">pro.h:S</ta>
            <ta e="T388" id="Seg_4516" s="T387">v:pred</ta>
            <ta e="T390" id="Seg_4517" s="T389">v:pred 0.1.h:S</ta>
            <ta e="T391" id="Seg_4518" s="T390">s:purp</ta>
            <ta e="T392" id="Seg_4519" s="T391">v:pred 0.1.h:S</ta>
            <ta e="T393" id="Seg_4520" s="T392">v:pred 0.2.h:S</ta>
            <ta e="T394" id="Seg_4521" s="T393">v:pred 0.2.h:S</ta>
            <ta e="T395" id="Seg_4522" s="T394">np:O</ta>
            <ta e="T397" id="Seg_4523" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T399" id="Seg_4524" s="T398"> v:pred 0.3.h:S</ta>
            <ta e="T401" id="Seg_4525" s="T400">np.h:S </ta>
            <ta e="T403" id="Seg_4526" s="T402">v:pred</ta>
            <ta e="T405" id="Seg_4527" s="T404">np.h:S</ta>
            <ta e="T406" id="Seg_4528" s="T405">v:pred</ta>
            <ta e="T407" id="Seg_4529" s="T406">v:pred 0.3.h:S</ta>
            <ta e="T410" id="Seg_4530" s="T409">np.h:S</ta>
            <ta e="T411" id="Seg_4531" s="T410">v:pred</ta>
            <ta e="T412" id="Seg_4532" s="T411">np:O</ta>
            <ta e="T415" id="Seg_4533" s="T414">pro.h:O</ta>
            <ta e="T416" id="Seg_4534" s="T415">v:pred 0.3.h:S</ta>
            <ta e="T418" id="Seg_4535" s="T417">v:pred 0.3.h:S</ta>
            <ta e="T421" id="Seg_4536" s="T420">v:pred 0.3.h:S</ta>
            <ta e="T422" id="Seg_4537" s="T421">pro.h:O</ta>
            <ta e="T423" id="Seg_4538" s="T422">v:pred 0.2.h:S</ta>
            <ta e="T425" id="Seg_4539" s="T424">s:purp</ta>
            <ta e="T427" id="Seg_4540" s="T426">np:O</ta>
            <ta e="T428" id="Seg_4541" s="T427">pro.h:S</ta>
            <ta e="T431" id="Seg_4542" s="T430">v:pred</ta>
            <ta e="T432" id="Seg_4543" s="T431">v:pred 0.2.h:S</ta>
            <ta e="T434" id="Seg_4544" s="T433">pro.h:S</ta>
            <ta e="T435" id="Seg_4545" s="T434">v:pred</ta>
            <ta e="T436" id="Seg_4546" s="T435">v:pred 0.2.h:S</ta>
            <ta e="T438" id="Seg_4547" s="T437">np.h:O</ta>
            <ta e="T439" id="Seg_4548" s="T438">pro.h:S</ta>
            <ta e="T440" id="Seg_4549" s="T439">conv:pred</ta>
            <ta e="T441" id="Seg_4550" s="T440">v:pred</ta>
            <ta e="T443" id="Seg_4551" s="T442">v:pred 0.3.h:S</ta>
            <ta e="T445" id="Seg_4552" s="T444">np.h:S</ta>
            <ta e="T446" id="Seg_4553" s="T445">v:pred</ta>
            <ta e="T447" id="Seg_4554" s="T446">pro.h:S</ta>
            <ta e="T448" id="Seg_4555" s="T447">v:pred</ta>
            <ta e="T449" id="Seg_4556" s="T448">pro.h:S</ta>
            <ta e="T450" id="Seg_4557" s="T449">v:pred</ta>
            <ta e="T451" id="Seg_4558" s="T450">v:pred 0.1.h:S</ta>
            <ta e="T454" id="Seg_4559" s="T453">v:pred 0.3.h:S</ta>
            <ta e="T455" id="Seg_4560" s="T454">np:O</ta>
            <ta e="T456" id="Seg_4561" s="T455">s:purp</ta>
            <ta e="T458" id="Seg_4562" s="T457">np.h:S</ta>
            <ta e="T459" id="Seg_4563" s="T458">v:pred</ta>
            <ta e="T460" id="Seg_4564" s="T459">v:pred 0.3.h:S</ta>
            <ta e="T462" id="Seg_4565" s="T461">np.h:S</ta>
            <ta e="T465" id="Seg_4566" s="T464">v:pred</ta>
            <ta e="T466" id="Seg_4567" s="T465">np.h:S</ta>
            <ta e="T467" id="Seg_4568" s="T466">v:pred</ta>
            <ta e="T469" id="Seg_4569" s="T468">np.h:S</ta>
            <ta e="T470" id="Seg_4570" s="T469">v:pred</ta>
            <ta e="T473" id="Seg_4571" s="T472">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_4572" s="T473">v:pred 0.2.h:S</ta>
            <ta e="T475" id="Seg_4573" s="T474">v:pred 0.2.h:S</ta>
            <ta e="T477" id="Seg_4574" s="T476">np.h:S</ta>
            <ta e="T479" id="Seg_4575" s="T478">np.h:S</ta>
            <ta e="T481" id="Seg_4576" s="T480">conv:pred</ta>
            <ta e="T482" id="Seg_4577" s="T481">v:pred</ta>
            <ta e="T484" id="Seg_4578" s="T483">v:pred 0.3.h:S</ta>
            <ta e="T487" id="Seg_4579" s="T486">v:pred 0.2.h:S</ta>
            <ta e="T488" id="Seg_4580" s="T487">pro.h:O</ta>
            <ta e="T489" id="Seg_4581" s="T488">pro.h:S</ta>
            <ta e="T490" id="Seg_4582" s="T489">v:pred</ta>
            <ta e="T492" id="Seg_4583" s="T491">v:pred 0.2.h:S</ta>
            <ta e="T494" id="Seg_4584" s="T493">np:O</ta>
            <ta e="T495" id="Seg_4585" s="T494">pro.h:S</ta>
            <ta e="T496" id="Seg_4586" s="T495">v:pred</ta>
            <ta e="T497" id="Seg_4587" s="T496">pro.h:S</ta>
            <ta e="T498" id="Seg_4588" s="T497">pro.h:O</ta>
            <ta e="T499" id="Seg_4589" s="T498">v:pred</ta>
            <ta e="T501" id="Seg_4590" s="T500">np.h:S</ta>
            <ta e="T502" id="Seg_4591" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_4592" s="T502">pro.h:S</ta>
            <ta e="T505" id="Seg_4593" s="T504">v:pred</ta>
            <ta e="T507" id="Seg_4594" s="T506">v:pred 0.3.h:S</ta>
            <ta e="T508" id="Seg_4595" s="T507">np.h:S</ta>
            <ta e="T510" id="Seg_4596" s="T509">v:pred</ta>
            <ta e="T513" id="Seg_4597" s="T512">v:pred 0.2.h:S</ta>
            <ta e="T514" id="Seg_4598" s="T513">v:pred 0.2.h:S</ta>
            <ta e="T515" id="Seg_4599" s="T514">pro.h:O</ta>
            <ta e="T516" id="Seg_4600" s="T515">v:pred 0.2.h:S</ta>
            <ta e="T517" id="Seg_4601" s="T516">np:O</ta>
            <ta e="T518" id="Seg_4602" s="T517">pro.h:S</ta>
            <ta e="T519" id="Seg_4603" s="T518">v:pred</ta>
            <ta e="T520" id="Seg_4604" s="T519">pro.h:S</ta>
            <ta e="T521" id="Seg_4605" s="T520">pro.h:O</ta>
            <ta e="T522" id="Seg_4606" s="T521">v:pred</ta>
            <ta e="T524" id="Seg_4607" s="T523">np.h:S</ta>
            <ta e="T525" id="Seg_4608" s="T524">v:pred</ta>
            <ta e="T527" id="Seg_4609" s="T526">pro.h:S</ta>
            <ta e="T529" id="Seg_4610" s="T528">v:pred</ta>
            <ta e="T531" id="Seg_4611" s="T530">v:pred 0.3.h:S</ta>
            <ta e="T532" id="Seg_4612" s="T531">np.h:S</ta>
            <ta e="T533" id="Seg_4613" s="T532">v:pred</ta>
            <ta e="T534" id="Seg_4614" s="T533">np.h:S</ta>
            <ta e="T537" id="Seg_4615" s="T536">v:pred</ta>
            <ta e="T538" id="Seg_4616" s="T537">np.h:S</ta>
            <ta e="T539" id="Seg_4617" s="T538">v:pred</ta>
            <ta e="T540" id="Seg_4618" s="T539">v:pred 0.2.h:S</ta>
            <ta e="T542" id="Seg_4619" s="T541">np:O</ta>
            <ta e="T543" id="Seg_4620" s="T542">pro.h:S</ta>
            <ta e="T545" id="Seg_4621" s="T544">v:pred</ta>
            <ta e="T548" id="Seg_4622" s="T547">v:pred 0.3.h:S</ta>
            <ta e="T549" id="Seg_4623" s="T548">np.h:S</ta>
            <ta e="T550" id="Seg_4624" s="T549">v:pred</ta>
            <ta e="T551" id="Seg_4625" s="T550">v:pred 0.3.h:S</ta>
            <ta e="T553" id="Seg_4626" s="T552">v:pred 0.3.h:S</ta>
            <ta e="T554" id="Seg_4627" s="T553">v:pred 0.3.h:S</ta>
            <ta e="T557" id="Seg_4628" s="T556">pro.h:S</ta>
            <ta e="T558" id="Seg_4629" s="T557">conv:pred</ta>
            <ta e="T559" id="Seg_4630" s="T558">v:pred</ta>
            <ta e="T561" id="Seg_4631" s="T560">v:pred 0.3.h:S</ta>
            <ta e="T563" id="Seg_4632" s="T562">np.h:S</ta>
            <ta e="T564" id="Seg_4633" s="T563">np.h:S</ta>
            <ta e="T565" id="Seg_4634" s="T564">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T236" id="Seg_4635" s="T235">RUS:gram</ta>
            <ta e="T248" id="Seg_4636" s="T247">RUS:cult</ta>
            <ta e="T251" id="Seg_4637" s="T250">RUS:gram</ta>
            <ta e="T258" id="Seg_4638" s="T257">RUS:gram</ta>
            <ta e="T263" id="Seg_4639" s="T262">RUS:gram</ta>
            <ta e="T286" id="Seg_4640" s="T285">RUS:cult</ta>
            <ta e="T297" id="Seg_4641" s="T296">RUS:cult</ta>
            <ta e="T305" id="Seg_4642" s="T304">RUS:gram</ta>
            <ta e="T312" id="Seg_4643" s="T311">RUS:cult</ta>
            <ta e="T325" id="Seg_4644" s="T324">RUS:cult</ta>
            <ta e="T327" id="Seg_4645" s="T326">TURK:cult</ta>
            <ta e="T331" id="Seg_4646" s="T330">TURK:gram(INDEF)</ta>
            <ta e="T336" id="Seg_4647" s="T335">TURK:gram(INDEF)</ta>
            <ta e="T342" id="Seg_4648" s="T341">RUS:core</ta>
            <ta e="T350" id="Seg_4649" s="T349">RUS:core</ta>
            <ta e="T353" id="Seg_4650" s="T352">RUS:mod</ta>
            <ta e="T354" id="Seg_4651" s="T353">TURK:core</ta>
            <ta e="T355" id="Seg_4652" s="T354">RUS:core</ta>
            <ta e="T356" id="Seg_4653" s="T355">RUS:gram</ta>
            <ta e="T357" id="Seg_4654" s="T356">RUS:gram</ta>
            <ta e="T360" id="Seg_4655" s="T359">RUS:gram</ta>
            <ta e="T362" id="Seg_4656" s="T361">TURK:gram(INDEF)</ta>
            <ta e="T370" id="Seg_4657" s="T369">TAT:cult</ta>
            <ta e="T372" id="Seg_4658" s="T371">TAT:cult</ta>
            <ta e="T380" id="Seg_4659" s="T379">RUS:gram</ta>
            <ta e="T386" id="Seg_4660" s="T385">RUS:gram</ta>
            <ta e="T389" id="Seg_4661" s="T388">RUS:gram</ta>
            <ta e="T395" id="Seg_4662" s="T394">RUS:cult</ta>
            <ta e="T400" id="Seg_4663" s="T399">RUS:gram</ta>
            <ta e="T413" id="Seg_4664" s="T412">RUS:gram</ta>
            <ta e="T420" id="Seg_4665" s="T419">TURK:disc</ta>
            <ta e="T427" id="Seg_4666" s="T426">RUS:cult</ta>
            <ta e="T433" id="Seg_4667" s="T432">RUS:gram</ta>
            <ta e="T442" id="Seg_4668" s="T441">RUS:gram</ta>
            <ta e="T464" id="Seg_4669" s="T463">TAT:cult</ta>
            <ta e="T468" id="Seg_4670" s="T467">RUS:gram</ta>
            <ta e="T494" id="Seg_4671" s="T493">RUS:cult</ta>
            <ta e="T504" id="Seg_4672" s="T503">TURK:disc</ta>
            <ta e="T512" id="Seg_4673" s="T511">RUS:core</ta>
            <ta e="T517" id="Seg_4674" s="T516">RUS:core</ta>
            <ta e="T530" id="Seg_4675" s="T529">RUS:cult</ta>
            <ta e="T532" id="Seg_4676" s="T531">RUS:cult</ta>
            <ta e="T536" id="Seg_4677" s="T535">TURK:disc</ta>
            <ta e="T538" id="Seg_4678" s="T537">RUS:cult</ta>
            <ta e="T542" id="Seg_4679" s="T541">RUS:cult</ta>
            <ta e="T544" id="Seg_4680" s="T543">TURK:disc</ta>
            <ta e="T546" id="Seg_4681" s="T545">RUS:cult</ta>
            <ta e="T552" id="Seg_4682" s="T551">TURK:gram(INDEF)</ta>
            <ta e="T556" id="Seg_4683" s="T555">RUS:gram</ta>
            <ta e="T562" id="Seg_4684" s="T561">RUS:gram</ta>
            <ta e="T567" id="Seg_4685" s="T566">TURK:core</ta>
            <ta e="T568" id="Seg_4686" s="T567">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T230" id="Seg_4687" s="T227">Жили были женщина и мужчина.</ta>
            <ta e="T237" id="Seg_4688" s="T230">У них были дочь и сын.</ta>
            <ta e="T240" id="Seg_4689" s="T237">Они ходят работать.</ta>
            <ta e="T242" id="Seg_4690" s="T240">"Смотри за мальчиком!</ta>
            <ta e="T249" id="Seg_4691" s="T242">Я тебе куплю платок, красивый".</ta>
            <ta e="T254" id="Seg_4692" s="T249">Ушли, а дочка взяла мальчика.</ta>
            <ta e="T256" id="Seg_4693" s="T254">Посадила на улице.</ta>
            <ta e="T260" id="Seg_4694" s="T256">На траву, а сама убежала.</ta>
            <ta e="T263" id="Seg_4695" s="T260">Тут утки прилетели.</ta>
            <ta e="T270" id="Seg_4696" s="T263">Взяли мальчика и унесли с собой.</ta>
            <ta e="T274" id="Seg_4697" s="T270">Девушка прибежала.</ta>
            <ta e="T278" id="Seg_4698" s="T274">Смотрит: нет мальчика.</ta>
            <ta e="T282" id="Seg_4699" s="T278">Туда-сюда побежала, потом пошла.</ta>
            <ta e="T283" id="Seg_4700" s="T282">Побежала.</ta>
            <ta e="T285" id="Seg_4701" s="T283">Бежит-бежит.</ta>
            <ta e="T287" id="Seg_4702" s="T285">[Видит:] стоит печь.</ta>
            <ta e="T294" id="Seg_4703" s="T287">"Ты не видела, куда полетели утки?"</ta>
            <ta e="T296" id="Seg_4704" s="T294">Та говорит:</ta>
            <ta e="T298" id="Seg_4705" s="T296">"Съешь пирожок!"</ta>
            <ta e="T300" id="Seg_4706" s="T298">"Это не из муки пирожок!</ta>
            <ta e="T307" id="Seg_4707" s="T300">У моего отца из муки [пирожки], я и то их не ем!"</ta>
            <ta e="T308" id="Seg_4708" s="T307">Побежала [дальше].</ta>
            <ta e="T310" id="Seg_4709" s="T308">Идет.</ta>
            <ta e="T316" id="Seg_4710" s="T310">Печь ничего ей не сказала.</ta>
            <ta e="T319" id="Seg_4711" s="T316">Она опять побежала дальше.</ta>
            <ta e="T323" id="Seg_4712" s="T319">Идет, [видит:] речка течет.</ta>
            <ta e="T327" id="Seg_4713" s="T323">"Съешь мой кисель с молоком!"</ta>
            <ta e="T333" id="Seg_4714" s="T327">"У моего отца и не такой [кисель], [и то] я не ем".</ta>
            <ta e="T338" id="Seg_4715" s="T333">[Речка] не сказала ей ничего.</ta>
            <ta e="T340" id="Seg_4716" s="T338">[Дальше] бежит.</ta>
            <ta e="T342" id="Seg_4717" s="T340">Стоит яблоня.</ta>
            <ta e="T347" id="Seg_4718" s="T342">"Ты не видела, куда утки полетели?"</ta>
            <ta e="T350" id="Seg_4719" s="T347">"Съешь мое яблоко!"</ta>
            <ta e="T359" id="Seg_4720" s="T350">"У моего отца еще лучше яблоки, [и то] я их не ем".</ta>
            <ta e="T364" id="Seg_4721" s="T359">И [яблоня] ничего ей не сказала.</ta>
            <ta e="T368" id="Seg_4722" s="T364">Она идет, идет.</ta>
            <ta e="T370" id="Seg_4723" s="T368">Стоит дом.</ta>
            <ta e="T373" id="Seg_4724" s="T370">Она в дом вошла.</ta>
            <ta e="T376" id="Seg_4725" s="T373">Там женщина сидит.</ta>
            <ta e="T377" id="Seg_4726" s="T376">Прядет.</ta>
            <ta e="T381" id="Seg_4727" s="T377">Мальчик [тоже] там.</ta>
            <ta e="T385" id="Seg_4728" s="T381">"Пряди(?), зачем ты пришла?"</ta>
            <ta e="T390" id="Seg_4729" s="T385">"Я шла и замерзла.</ta>
            <ta e="T392" id="Seg_4730" s="T390">Я пришла погреться".</ta>
            <ta e="T396" id="Seg_4731" s="T392">"Садись, спряди мне кудель".</ta>
            <ta e="T399" id="Seg_4732" s="T396">Она села, прядет.</ta>
            <ta e="T403" id="Seg_4733" s="T399">А мальчик сидит на улице.</ta>
            <ta e="T407" id="Seg_4734" s="T403">Потом мышка выбежала, говорит:</ta>
            <ta e="T412" id="Seg_4735" s="T407">"Девочка, [эта] женщина топит тебе баню.</ta>
            <ta e="T418" id="Seg_4736" s="T412">Она зажарит тебя и съест.</ta>
            <ta e="T422" id="Seg_4737" s="T418">И друга твоего посадит на них. [?]</ta>
            <ta e="T427" id="Seg_4738" s="T422">Дай мне кашки поесть".</ta>
            <ta e="T431" id="Seg_4739" s="T427">Она дала ей.</ta>
            <ta e="T435" id="Seg_4740" s="T431">"Иди, а я буду прясть.</ta>
            <ta e="T438" id="Seg_4741" s="T435">Возьми своего [братца]".</ta>
            <ta e="T443" id="Seg_4742" s="T438">Она схватила [его] и убежала.</ta>
            <ta e="T446" id="Seg_4743" s="T443">Женщина придет.</ta>
            <ta e="T448" id="Seg_4744" s="T446">"Ты прядешь?"</ta>
            <ta e="T451" id="Seg_4745" s="T448">Та [мышь] говорит: "Я пряду".</ta>
            <ta e="T459" id="Seg_4746" s="T451">Та опять уйдет топить баню; а девочка убежала.</ta>
            <ta e="T460" id="Seg_4747" s="T459">Она пришла.</ta>
            <ta e="T470" id="Seg_4748" s="T460">Женщина пришла в дом: девочки нет и мальчика нет.</ta>
            <ta e="T473" id="Seg_4749" s="T470">Она уткам говорит:</ta>
            <ta e="T477" id="Seg_4750" s="T473">"Летите, посмотрите, где девочка!"</ta>
            <ta e="T482" id="Seg_4751" s="T477">Девочка бежит.</ta>
            <ta e="T484" id="Seg_4752" s="T482">Прибежала к речке.</ta>
            <ta e="T488" id="Seg_4753" s="T484">"Речка, речка, спрячь меня!"</ta>
            <ta e="T494" id="Seg_4754" s="T488">Та говорит: "Поешь моего киселя!"</ta>
            <ta e="T499" id="Seg_4755" s="T494">Она поела, [речка] спрятала ее.</ta>
            <ta e="T502" id="Seg_4756" s="T499">Утки пролетели.</ta>
            <ta e="T505" id="Seg_4757" s="T502">Она вышла.</ta>
            <ta e="T507" id="Seg_4758" s="T505">Снова бежит.</ta>
            <ta e="T510" id="Seg_4759" s="T507">Утки снова летят.</ta>
            <ta e="T513" id="Seg_4760" s="T510">Она говорит яблоне:</ta>
            <ta e="T515" id="Seg_4761" s="T513">"Спрячь нас".</ta>
            <ta e="T522" id="Seg_4762" s="T515">"Поешь моих яблочек", та поела, [яблоня] спрятала ее [= их].</ta>
            <ta e="T526" id="Seg_4763" s="T522">Утки пролетели.</ta>
            <ta e="T529" id="Seg_4764" s="T526">Они дальше побежали.</ta>
            <ta e="T531" id="Seg_4765" s="T529">Прибежали к печке.</ta>
            <ta e="T533" id="Seg_4766" s="T531">Печка говорит:</ta>
            <ta e="T537" id="Seg_4767" s="T533">"Утки нас догоняют".</ta>
            <ta e="T539" id="Seg_4768" s="T537">Печка говорит:</ta>
            <ta e="T542" id="Seg_4769" s="T539">"Поешь моих пирожков!"</ta>
            <ta e="T548" id="Seg_4770" s="T542">Она поела, залезла в печь.</ta>
            <ta e="T553" id="Seg_4771" s="T548">Утки летали, летали, их нигде нет.</ta>
            <ta e="T555" id="Seg_4772" s="T553">Вернулись к женщине.</ta>
            <ta e="T560" id="Seg_4773" s="T555">А они [= девочка с мальчиком] вылезли и пришли домой.</ta>
            <ta e="T561" id="Seg_4774" s="T560">Пришли.</ta>
            <ta e="T565" id="Seg_4775" s="T561">И отец с матерью пришли.</ta>
            <ta e="T567" id="Seg_4776" s="T565">(На работ-) С работы.</ta>
            <ta e="T568" id="Seg_4777" s="T567">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T230" id="Seg_4778" s="T227">There lived a woman with a man.</ta>
            <ta e="T237" id="Seg_4779" s="T230">They had a daughter and a son.</ta>
            <ta e="T240" id="Seg_4780" s="T237">They are going to work.</ta>
            <ta e="T242" id="Seg_4781" s="T240">"Look after the boy!</ta>
            <ta e="T249" id="Seg_4782" s="T242">I will buy you a kerchief, beautiful."</ta>
            <ta e="T254" id="Seg_4783" s="T249">They went, but the daughter took the boy.</ta>
            <ta e="T256" id="Seg_4784" s="T254">She seated him outside.</ta>
            <ta e="T260" id="Seg_4785" s="T256">In the grass, and herself ran away.</ta>
            <ta e="T263" id="Seg_4786" s="T260">Ducks came flying there.</ta>
            <ta e="T270" id="Seg_4787" s="T263">(And?) they took the boy, took him away.</ta>
            <ta e="T274" id="Seg_4788" s="T270">The girl came running.</ta>
            <ta e="T278" id="Seg_4789" s="T274">She sees: the boy is not there.</ta>
            <ta e="T282" id="Seg_4790" s="T278">She ran here-there, then went.</ta>
            <ta e="T283" id="Seg_4791" s="T282">She ran.</ta>
            <ta e="T285" id="Seg_4792" s="T283">She is running, she is running.</ta>
            <ta e="T287" id="Seg_4793" s="T285">[She sees:] there is an oven.</ta>
            <ta e="T294" id="Seg_4794" s="T287">"Didn't you see where the ducks flew?"</ta>
            <ta e="T296" id="Seg_4795" s="T294">It says:</ta>
            <ta e="T298" id="Seg_4796" s="T296">"Eat a pie!"</ta>
            <ta e="T300" id="Seg_4797" s="T298">"[It’s] not wheat!</ta>
            <ta e="T307" id="Seg_4798" s="T300">My father’s [pies] are [made of] wheat, and I do not eat [them either]."</ta>
            <ta e="T308" id="Seg_4799" s="T307">She ran away.</ta>
            <ta e="T310" id="Seg_4800" s="T308">She goes.</ta>
            <ta e="T316" id="Seg_4801" s="T310">Then the oven did not tell her anything.</ta>
            <ta e="T319" id="Seg_4802" s="T316">She ran away again.</ta>
            <ta e="T323" id="Seg_4803" s="T319">Then she comes, river is flowing.</ta>
            <ta e="T327" id="Seg_4804" s="T323">"Eat my kissel with milk!"</ta>
            <ta e="T333" id="Seg_4805" s="T327">"My father’s [kissel] is not like that, I do not eat [it either]."</ta>
            <ta e="T338" id="Seg_4806" s="T333">It did not tell her anything.</ta>
            <ta e="T340" id="Seg_4807" s="T338">Then she is running.</ta>
            <ta e="T342" id="Seg_4808" s="T340">An apple [tree] stands.</ta>
            <ta e="T347" id="Seg_4809" s="T342">"Did you see where the ducks flew?"</ta>
            <ta e="T350" id="Seg_4810" s="T347">"Eat my apple!"</ta>
            <ta e="T359" id="Seg_4811" s="T350">"My father has even better apples, and I don't eat [them either]."</ta>
            <ta e="T364" id="Seg_4812" s="T359">And it did not tell her anything.</ta>
            <ta e="T368" id="Seg_4813" s="T364">Then she went, she went.</ta>
            <ta e="T370" id="Seg_4814" s="T368">A house is standing.</ta>
            <ta e="T373" id="Seg_4815" s="T370">She entered the house.</ta>
            <ta e="T376" id="Seg_4816" s="T373">There is a woman sitting.</ta>
            <ta e="T377" id="Seg_4817" s="T376">She is spinning.</ta>
            <ta e="T381" id="Seg_4818" s="T377">The boy is also there.</ta>
            <ta e="T385" id="Seg_4819" s="T381">"Spin(?), what did you come for?"</ta>
            <ta e="T390" id="Seg_4820" s="T385">"Well, I was going and freezing.</ta>
            <ta e="T392" id="Seg_4821" s="T390">I came to warm up."</ta>
            <ta e="T396" id="Seg_4822" s="T392">"Sit down, spin me a tow."</ta>
            <ta e="T399" id="Seg_4823" s="T396">She sat, then she is spinning.</ta>
            <ta e="T403" id="Seg_4824" s="T399">And her boy [= her brother] is sitting outside.</ta>
            <ta e="T407" id="Seg_4825" s="T403">Then a mouse came out, says:</ta>
            <ta e="T412" id="Seg_4826" s="T407">"Girl, the woman is heating the sauna for you.</ta>
            <ta e="T418" id="Seg_4827" s="T412">And she will burn [= fry] you in order to eat you.</ta>
            <ta e="T422" id="Seg_4828" s="T418">And she will seat your friend. [?]</ta>
            <ta e="T427" id="Seg_4829" s="T422">Give me some porridge to eat."</ta>
            <ta e="T431" id="Seg_4830" s="T427">She gave it to it.</ta>
            <ta e="T435" id="Seg_4831" s="T431">"Go, and I will spin!</ta>
            <ta e="T438" id="Seg_4832" s="T435">Take your boy."</ta>
            <ta e="T443" id="Seg_4833" s="T438">She grabed [him] and ran [away].</ta>
            <ta e="T446" id="Seg_4834" s="T443">The woman comes.</ta>
            <ta e="T448" id="Seg_4835" s="T446">"Are you spinning?"</ta>
            <ta e="T451" id="Seg_4836" s="T448">It [the mouse] says: "I am spinning."</ta>
            <ta e="T459" id="Seg_4837" s="T451">Then again she will go to heat the sauna, the girl ran away.</ta>
            <ta e="T460" id="Seg_4838" s="T459">She came.</ta>
            <ta e="T470" id="Seg_4839" s="T460">Then the woman came to the house, there is no girl, there is no boy.</ta>
            <ta e="T473" id="Seg_4840" s="T470">Then she says to the ducks.</ta>
            <ta e="T477" id="Seg_4841" s="T473">"Go, look, where is the girl!"</ta>
            <ta e="T482" id="Seg_4842" s="T477">The girl is running.</ta>
            <ta e="T484" id="Seg_4843" s="T482">She came to the river.</ta>
            <ta e="T488" id="Seg_4844" s="T484">"River, river, hide me!"</ta>
            <ta e="T494" id="Seg_4845" s="T488">It says: "Eat my kissel!"</ta>
            <ta e="T499" id="Seg_4846" s="T494">She ate, it covered her.</ta>
            <ta e="T502" id="Seg_4847" s="T499">Then the ducks flew [by].</ta>
            <ta e="T505" id="Seg_4848" s="T502">She came out.</ta>
            <ta e="T507" id="Seg_4849" s="T505">Again she is running.</ta>
            <ta e="T510" id="Seg_4850" s="T507">The ducks are flying again.</ta>
            <ta e="T513" id="Seg_4851" s="T510">Then she says to the apple tree:</ta>
            <ta e="T515" id="Seg_4852" s="T513">"Hide us!"</ta>
            <ta e="T522" id="Seg_4853" s="T515">"Eat the apple", she ate, it hid her.</ta>
            <ta e="T526" id="Seg_4854" s="T522">Then the ducks flew [by].</ta>
            <ta e="T529" id="Seg_4855" s="T526">They ran again.</ta>
            <ta e="T531" id="Seg_4856" s="T529">She came to the oven.</ta>
            <ta e="T533" id="Seg_4857" s="T531">The oven says:</ta>
            <ta e="T537" id="Seg_4858" s="T533">"The ducks are catching us."</ta>
            <ta e="T539" id="Seg_4859" s="T537">The oven says:</ta>
            <ta e="T542" id="Seg_4860" s="T539">"Eat my pie!"</ta>
            <ta e="T548" id="Seg_4861" s="T542">She ate it all, climbed into the oven.</ta>
            <ta e="T553" id="Seg_4862" s="T548">The ducks were flying, were flying, [they] aren't anywhere.</ta>
            <ta e="T555" id="Seg_4863" s="T553">They returned to the woman.</ta>
            <ta e="T560" id="Seg_4864" s="T555">And they [= the girl and the boy] came out and returned home.</ta>
            <ta e="T561" id="Seg_4865" s="T560">They came.</ta>
            <ta e="T565" id="Seg_4866" s="T561">And her father, mother came.</ta>
            <ta e="T567" id="Seg_4867" s="T565">(To work-) From work.</ta>
            <ta e="T568" id="Seg_4868" s="T567">[That's] all!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T230" id="Seg_4869" s="T227">Es lebte eine Frau und ein Mann.</ta>
            <ta e="T237" id="Seg_4870" s="T230">Sie hatten eine Tochter und einen Sohn.</ta>
            <ta e="T240" id="Seg_4871" s="T237">Sie gehen arbeiten.</ta>
            <ta e="T242" id="Seg_4872" s="T240">„Schau nach dem Jungen!</ta>
            <ta e="T249" id="Seg_4873" s="T242">Ich kauf dir ein Halstuch, Schönes.“</ta>
            <ta e="T254" id="Seg_4874" s="T249">Sie gingen, aber die Tochter nahm den Jungen.</ta>
            <ta e="T256" id="Seg_4875" s="T254">Sie setzte ihn draußen.</ta>
            <ta e="T260" id="Seg_4876" s="T256">Im Gras, und selber lief fort.</ta>
            <ta e="T263" id="Seg_4877" s="T260">Enten kamen dort angeflogen.</ta>
            <ta e="T270" id="Seg_4878" s="T263">(Und?) sie nahmen den Jungen, nahmen ihn weg.</ta>
            <ta e="T274" id="Seg_4879" s="T270">Die Tochter kam angerannt.</ta>
            <ta e="T278" id="Seg_4880" s="T274">Sie sah: es ist nichts, es ist kein Junge da.</ta>
            <ta e="T282" id="Seg_4881" s="T278">Sie lief hier, dort, dann ging sie.</ta>
            <ta e="T283" id="Seg_4882" s="T282">Sie lief.</ta>
            <ta e="T285" id="Seg_4883" s="T283">Sie läuft, sie läuft.</ta>
            <ta e="T287" id="Seg_4884" s="T285">Ein Ofen steht.</ta>
            <ta e="T294" id="Seg_4885" s="T287">„Hast du nicht gesehen, wohin die Enten flogen?“</ta>
            <ta e="T296" id="Seg_4886" s="T294">Er sagt:</ta>
            <ta e="T298" id="Seg_4887" s="T296">Iss eine Pastete!</ta>
            <ta e="T300" id="Seg_4888" s="T298">„[Es ist] nicht Weizen!</ta>
            <ta e="T307" id="Seg_4889" s="T300">Meinem Vater seine [Pasteten] sind aus Weizen [gemacht], und ich esse [sie auch] nicht.“ </ta>
            <ta e="T308" id="Seg_4890" s="T307">Sie lief weg.</ta>
            <ta e="T310" id="Seg_4891" s="T308">Dann kommt sie.</ta>
            <ta e="T316" id="Seg_4892" s="T310">Dann erzählte ihr der Ofen nichts.</ta>
            <ta e="T319" id="Seg_4893" s="T316">Sie lief wieder weg.</ta>
            <ta e="T323" id="Seg_4894" s="T319">Dann kommt sie, Fluss fließt.</ta>
            <ta e="T327" id="Seg_4895" s="T323">„Iss mein Kissel (?) mit Milch!“</ta>
            <ta e="T333" id="Seg_4896" s="T327">„Mein Vater sein [Kissel] ist nicht so, ich esse [es auch] nicht.“</ta>
            <ta e="T338" id="Seg_4897" s="T333">Es erzählte ihr nichts.</ta>
            <ta e="T340" id="Seg_4898" s="T338">Dann läuft sie.</ta>
            <ta e="T342" id="Seg_4899" s="T340">Ein Apfel[baum] steht.</ta>
            <ta e="T347" id="Seg_4900" s="T342">„Hast du gesehen, wohin die Enten flogen?“</ta>
            <ta e="T350" id="Seg_4901" s="T347">„Iss meinen Apfel!“</ta>
            <ta e="T359" id="Seg_4902" s="T350">„Mein Vater hat noch bessere Äpfel, und ich esse [sie auch] nicht.“</ta>
            <ta e="T364" id="Seg_4903" s="T359">Und er erzählte ihr nichts.</ta>
            <ta e="T368" id="Seg_4904" s="T364">Dann ging sie, sie ging.</ta>
            <ta e="T370" id="Seg_4905" s="T368">Ein Haus steht.</ta>
            <ta e="T373" id="Seg_4906" s="T370">Sie trat in das Haus ein.</ta>
            <ta e="T376" id="Seg_4907" s="T373">Es sitzt eine Frau.</ta>
            <ta e="T377" id="Seg_4908" s="T376">Sie spinnt.</ta>
            <ta e="T381" id="Seg_4909" s="T377">Der Junge ist auch da.</ta>
            <ta e="T385" id="Seg_4910" s="T381">Spin, wofür bist du gekommen?</ta>
            <ta e="T390" id="Seg_4911" s="T385">„Also, ich ging und fror.</ta>
            <ta e="T392" id="Seg_4912" s="T390">Ich kam, um mich aufzuwärmen.“</ta>
            <ta e="T396" id="Seg_4913" s="T392">„Sitz, spinne mir einen Tau.“</ta>
            <ta e="T399" id="Seg_4914" s="T396">Sie saß, dann spinnt sie.</ta>
            <ta e="T403" id="Seg_4915" s="T399">Und ihr Junge [= ihr Bruder] sitzt draußen.</ta>
            <ta e="T407" id="Seg_4916" s="T403">Dann kam eine Maus heraus, sagte:</ta>
            <ta e="T412" id="Seg_4917" s="T407">„Mädchen, die Frau erhitzt die Sauna für dich.</ta>
            <ta e="T418" id="Seg_4918" s="T412">Und sie wird dich brennen [= braten] um dich zu essen.</ta>
            <ta e="T422" id="Seg_4919" s="T418">Und sie wird auf (?) sitzen.</ta>
            <ta e="T427" id="Seg_4920" s="T422">Gib mir etwas Brei zu essen.“</ta>
            <ta e="T431" id="Seg_4921" s="T427">Sie gab es ihr.</ta>
            <ta e="T435" id="Seg_4922" s="T431">„Geh, und ich werde spinnen!</ta>
            <ta e="T438" id="Seg_4923" s="T435">Nimm dein Junge.“</ta>
            <ta e="T443" id="Seg_4924" s="T438">Sie schnappte [ihn] und lief [weg].</ta>
            <ta e="T446" id="Seg_4925" s="T443">Die Frau wird kommen.</ta>
            <ta e="T448" id="Seg_4926" s="T446">„Spinnst du?“</ta>
            <ta e="T451" id="Seg_4927" s="T448">Sie [die Maus] sagt: „Ich spinne.“</ta>
            <ta e="T459" id="Seg_4928" s="T451">Dann will sie wieder gehen, die Sauna zu erhitzen, das Mädchen lief weg.</ta>
            <ta e="T460" id="Seg_4929" s="T459">Sie kam.</ta>
            <ta e="T470" id="Seg_4930" s="T460">Dann kam die Frau zum Haus, es ist kein Mädchen, es ist kein Junge.</ta>
            <ta e="T473" id="Seg_4931" s="T470">Dann sagt sie zu den Enten.</ta>
            <ta e="T477" id="Seg_4932" s="T473">“Geht, schaut, wo das Mädchen ist!“</ta>
            <ta e="T482" id="Seg_4933" s="T477">Das Mädchen läuft.</ta>
            <ta e="T484" id="Seg_4934" s="T482">Sie kam zum Fluss.</ta>
            <ta e="T488" id="Seg_4935" s="T484">“Fluss, Fluss, versteck mich!“</ta>
            <ta e="T494" id="Seg_4936" s="T488">Er sagt: “Iss mein Kissel!“</ta>
            <ta e="T499" id="Seg_4937" s="T494">Sie aß, er bedeckte sie.</ta>
            <ta e="T502" id="Seg_4938" s="T499">Dann flogen die Enten [vorbei].</ta>
            <ta e="T505" id="Seg_4939" s="T502">Sie kam heraus.</ta>
            <ta e="T507" id="Seg_4940" s="T505">Wieder läuft sie.</ta>
            <ta e="T510" id="Seg_4941" s="T507">Die Enten fliegen wieder.</ta>
            <ta e="T513" id="Seg_4942" s="T510">Dann sagt sie zum Apfelbaum:</ta>
            <ta e="T515" id="Seg_4943" s="T513">“Versteck uns!”</ta>
            <ta e="T522" id="Seg_4944" s="T515">“Iss meinen Apfel“, sie aß, er versteckte sie.</ta>
            <ta e="T526" id="Seg_4945" s="T522">Dann flogen die Enten [vorbei].</ta>
            <ta e="T529" id="Seg_4946" s="T526">Sie (lief) – sie liefen wieder.</ta>
            <ta e="T531" id="Seg_4947" s="T529">Sie kam zum Ofen.</ta>
            <ta e="T533" id="Seg_4948" s="T531">Der Ofen sagt:</ta>
            <ta e="T537" id="Seg_4949" s="T533">„Die Enten holen uns ein.“</ta>
            <ta e="T539" id="Seg_4950" s="T537">Der Ofen sagt:</ta>
            <ta e="T542" id="Seg_4951" s="T539">„Iss meine Pastete!“</ta>
            <ta e="T548" id="Seg_4952" s="T542">Sie aß es alles auf, kletterte in den Ofen.</ta>
            <ta e="T553" id="Seg_4953" s="T548">Die Enten flogen, flogen, [sie] sind nirgendwo.</ta>
            <ta e="T555" id="Seg_4954" s="T553">Sie kamen zur Frau zurück.</ta>
            <ta e="T560" id="Seg_4955" s="T555">Aber sie kommen heraus, kamen zu ihrem Zuhause.</ta>
            <ta e="T561" id="Seg_4956" s="T560">Sie kamen.</ta>
            <ta e="T565" id="Seg_4957" s="T561">Und ihr Vater, Mutter kamen.</ta>
            <ta e="T567" id="Seg_4958" s="T565">(Zu Arbeit-) Von der Arbeit.</ta>
            <ta e="T568" id="Seg_4959" s="T567">[Das ist] alles!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T230" id="Seg_4960" s="T227">[GVY:] The original tale is called "Geese and Swans" ("Гуси-лебеди").</ta>
            <ta e="T263" id="Seg_4961" s="T260">[KlT:] Russian prefix attached to a Kamas verb? first instance!; verb in SG instead of PL.</ta>
            <ta e="T270" id="Seg_4962" s="T263">[GVY:] Glossing of kunnaːlbiʔi is problematic</ta>
            <ta e="T294" id="Seg_4963" s="T287">[KlT:] 1SG instead of 2SG.</ta>
            <ta e="T316" id="Seg_4964" s="T310">[GVY:] ĭmbidə would be expected</ta>
            <ta e="T333" id="Seg_4965" s="T327">[GVY:] Glossing of dĭrgitdə is problematic</ta>
            <ta e="T396" id="Seg_4966" s="T392">[GVY:] Russian кудель 'tow'.</ta>
            <ta e="T418" id="Seg_4967" s="T412">[KlT:] Ru. ибо 'in order to'. [GVY:] Maybe this is only "i" 'and'</ta>
            <ta e="T422" id="Seg_4968" s="T418">[GVY:] In the standard Russian version Baba-Yaga means to roll on her victim's bones. Legənan 'on your bones' would be in place here.</ta>
            <ta e="T438" id="Seg_4969" s="T435">[KlT:] GEN instead of ACC?</ta>
            <ta e="T488" id="Seg_4970" s="T484">[KlT:] Šaʔ(bdə)l ’to hide’ (tr.).</ta>
            <ta e="T515" id="Seg_4971" s="T513">[KlT:] Res. intr. form (not expected). [GVY:] Glossing of šaʔlaːndə is problematic.</ta>
            <ta e="T522" id="Seg_4972" s="T515">[KlT:] Intr. form.</ta>
            <ta e="T533" id="Seg_4973" s="T531">[GVY:] They say to the oven.</ta>
            <ta e="T537" id="Seg_4974" s="T533">[KlT:] Reduplication of the PL marker?</ta>
            <ta e="T542" id="Seg_4975" s="T539">[KlT:] POSS.3SG instead of POSS.1SG?</ta>
            <ta e="T567" id="Seg_4976" s="T565">[KlT:] Nominal inflection with a verbal stem.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
