<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1F11263C-21AF-CEC0-DA0B-A698561010C4">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Corpse_flk\AA_1914_Corpse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">586</ud-information>
            <ud-information attribute-name="# HIAT:w">380</ud-information>
            <ud-information attribute-name="# e">372</ud-information>
            <ud-information attribute-name="# HIAT:u">106</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <timeline-fork end="T36" start="T35">
            <tli id="T35.tx.1" />
         </timeline-fork>
         <timeline-fork end="T100" start="T99">
            <tli id="T99.tx.1" />
         </timeline-fork>
         <timeline-fork end="T107" start="T106">
            <tli id="T106.tx.1" />
         </timeline-fork>
         <timeline-fork end="T183" start="T182">
            <tli id="T182.tx.1" />
         </timeline-fork>
         <timeline-fork end="T222" start="T221">
            <tli id="T221.tx.1" />
         </timeline-fork>
         <timeline-fork end="T276" start="T275">
            <tli id="T275.tx.1" />
         </timeline-fork>
         <timeline-fork end="T336" start="T335">
            <tli id="T335.tx.1" />
         </timeline-fork>
         <timeline-fork end="T337" start="T336">
            <tli id="T336.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T372" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nüke</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">amnobi</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">oʔb</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">nʼit</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">ibi</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_21" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Nʼit</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">ijaandə</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">mălia:</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_31" n="HIAT:ip">"</nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">Măn</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">nejləm</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Ijat</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">mălia:</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Naga</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">manʼəkkən</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">ne</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">naga</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip">"</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_65" n="HIAT:u" s="T16">
                  <nts id="Seg_66" n="HIAT:ip">"</nts>
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">Măn</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">kallam</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">ne</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">pileʔ</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip">"</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_82" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">Kallaʔ</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_87" n="HIAT:w" s="T21">tʼürbi</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">ijabə</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">baʔlaːmbi</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_98" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">Kandəga</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">kandəga</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_108" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_110" n="HIAT:w" s="T26">Tʼăgan</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">toʔgəndə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">maʔ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">nuga</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_123" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">Šübi</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">dĭ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">maʔdə</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">ne</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">amna</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_142" n="HIAT:u" s="T35">
                  <ts e="T35.tx.1" id="Seg_144" n="HIAT:w" s="T35">Numan</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_147" n="HIAT:w" s="T35.tx.1">üzəbi</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_151" n="HIAT:w" s="T36">udabə</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_154" n="HIAT:w" s="T37">mĭbi</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_158" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_160" n="HIAT:w" s="T38">Amnəbi</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_163" n="HIAT:w" s="T39">šün</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_166" n="HIAT:w" s="T40">toʔgəndə</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_170" n="HIAT:u" s="T41">
                  <nts id="Seg_171" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_173" n="HIAT:w" s="T41">Adʼam</ts>
                  <nts id="Seg_174" n="HIAT:ip">!</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_177" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_179" n="HIAT:w" s="T42">Aspaʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_182" n="HIAT:w" s="T43">edəʔ</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_186" n="HIAT:w" s="T44">uja</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_189" n="HIAT:w" s="T45">padaʔ</ts>
                  <nts id="Seg_190" n="HIAT:ip">!</nts>
                  <nts id="Seg_191" n="HIAT:ip">"</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_194" n="HIAT:u" s="T46">
                  <nts id="Seg_195" n="HIAT:ip">"</nts>
                  <ts e="T47" id="Seg_197" n="HIAT:w" s="T46">Ujam</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_200" n="HIAT:w" s="T47">naga</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip">"</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_205" n="HIAT:u" s="T48">
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <ts e="T49" id="Seg_208" n="HIAT:w" s="T48">Măn</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_211" n="HIAT:w" s="T49">tetliem</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip">"</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_216" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_218" n="HIAT:w" s="T50">Nʼiʔdə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_221" n="HIAT:w" s="T51">uʔbdəbi</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_225" n="HIAT:w" s="T52">tʼalaš</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_228" n="HIAT:w" s="T53">šüškü</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_231" n="HIAT:w" s="T54">ibi</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_235" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_237" n="HIAT:w" s="T55">Püjebə</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_240" n="HIAT:w" s="T56">taləj</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_243" n="HIAT:w" s="T57">toʔbdəbi</ts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_247" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_249" n="HIAT:w" s="T58">Kemdə</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_252" n="HIAT:w" s="T59">mʼaŋnuʔbi</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_256" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_258" n="HIAT:w" s="T60">Dĭ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_261" n="HIAT:w" s="T61">kemzʼəʔ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_264" n="HIAT:w" s="T62">šüšküm</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_267" n="HIAT:w" s="T63">bar</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_270" n="HIAT:w" s="T64">tʼoʔbdəbi</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_274" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_276" n="HIAT:w" s="T65">Šübi</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_279" n="HIAT:w" s="T66">maʔdə</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_283" n="HIAT:w" s="T67">dĭ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_286" n="HIAT:w" s="T68">šüšküm</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_289" n="HIAT:w" s="T69">aspaʔdə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_292" n="HIAT:w" s="T70">paʔdluʔbi</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_296" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_298" n="HIAT:w" s="T71">Amnaiʔ</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_302" n="HIAT:w" s="T72">tʼăbaktərlaʔbəjəʔ</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_306" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_308" n="HIAT:w" s="T73">Mălia:</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_310" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_312" n="HIAT:w" s="T74">Adʼaŋ</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_316" n="HIAT:w" s="T75">amoržəbəj</ts>
                  <nts id="Seg_317" n="HIAT:ip">!</nts>
                  <nts id="Seg_318" n="HIAT:ip">"</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_321" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_323" n="HIAT:w" s="T76">Dĭ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_326" n="HIAT:w" s="T77">ne</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_329" n="HIAT:w" s="T78">aspaʔdəbə</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_332" n="HIAT:w" s="T79">ibi</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_336" n="HIAT:w" s="T80">šündə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_339" n="HIAT:w" s="T81">toʔgəndə</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_342" n="HIAT:w" s="T82">nuldəbi</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_346" n="HIAT:w" s="T83">kiʔgobə</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_349" n="HIAT:w" s="T84">ibi</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_353" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_355" n="HIAT:w" s="T85">Ujabə</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_358" n="HIAT:w" s="T86">suʔlaʔbə</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_362" n="HIAT:w" s="T87">kur</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_365" n="HIAT:w" s="T88">le</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_368" n="HIAT:w" s="T89">aspaʔgən</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_371" n="HIAT:w" s="T90">koldərlaʔbə</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_375" n="HIAT:u" s="T91">
                  <nts id="Seg_376" n="HIAT:ip">"</nts>
                  <ts e="T92" id="Seg_378" n="HIAT:w" s="T91">Gijən</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_381" n="HIAT:w" s="T92">tăn</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_384" n="HIAT:w" s="T93">ujal</ts>
                  <nts id="Seg_385" n="HIAT:ip">?</nts>
                  <nts id="Seg_386" n="HIAT:ip">"</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_389" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_391" n="HIAT:w" s="T94">Onʼiʔ</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_394" n="HIAT:w" s="T95">tʼalaš</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_397" n="HIAT:w" s="T96">le</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_400" n="HIAT:w" s="T97">tĭrlöleʔ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_403" n="HIAT:w" s="T98">mĭŋge</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_407" n="HIAT:u" s="T99">
                  <nts id="Seg_408" n="HIAT:ip">"</nts>
                  <ts e="T99.tx.1" id="Seg_410" n="HIAT:w" s="T99">Kötʼsʼün</ts>
                  <nts id="Seg_411" n="HIAT:ip">_</nts>
                  <ts e="T100" id="Seg_413" n="HIAT:w" s="T99.tx.1">Güdʼər</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_417" n="HIAT:w" s="T100">gijənde</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_420" n="HIAT:w" s="T101">ujal</ts>
                  <nts id="Seg_421" n="HIAT:ip">?</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_424" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_426" n="HIAT:w" s="T102">Oʔnʼiʔ</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_429" n="HIAT:w" s="T103">le</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_432" n="HIAT:w" s="T104">tĭrlöleʔ</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_435" n="HIAT:w" s="T105">mĭŋge</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip">"</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_440" n="HIAT:u" s="T106">
                  <ts e="T106.tx.1" id="Seg_442" n="HIAT:w" s="T106">Kötʼsʼün</ts>
                  <nts id="Seg_443" n="HIAT:ip">_</nts>
                  <ts e="T107" id="Seg_445" n="HIAT:w" s="T106.tx.1">Güdʼər</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_448" n="HIAT:w" s="T107">tʼorluʔbi</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_452" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_454" n="HIAT:w" s="T108">Šünə</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_457" n="HIAT:w" s="T109">tĭrlöleʔbi</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_461" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_463" n="HIAT:w" s="T110">Ne</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_466" n="HIAT:w" s="T111">săbəjʔleʔbi</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_469" n="HIAT:w" s="T112">ködʼi</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_473" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_475" n="HIAT:w" s="T113">Nʼeʔleʔbliet</ts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_479" n="HIAT:u" s="T114">
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <ts e="T115" id="Seg_482" n="HIAT:w" s="T114">Iʔ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_485" n="HIAT:w" s="T115">tʼoraʔ</ts>
                  <nts id="Seg_486" n="HIAT:ip">!</nts>
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_490" n="HIAT:u" s="T116">
                  <nts id="Seg_491" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_493" n="HIAT:w" s="T116">Tăn</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_496" n="HIAT:w" s="T117">aspaʔlə</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_499" n="HIAT:w" s="T118">koːlaːmbi</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_503" n="HIAT:w" s="T119">măn</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_506" n="HIAT:w" s="T120">ujam</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_509" n="HIAT:w" s="T121">amnuʔbi</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip">"</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_514" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_516" n="HIAT:w" s="T122">Ne:</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_518" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_520" n="HIAT:w" s="T123">Iʔ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_523" n="HIAT:w" s="T124">tʼoraʔ</ts>
                  <nts id="Seg_524" n="HIAT:ip">!</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_527" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_529" n="HIAT:w" s="T125">Mĭliem</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_533" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_535" n="HIAT:w" s="T126">Onʼiʔ</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_538" n="HIAT:w" s="T127">ularbə</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_541" n="HIAT:w" s="T128">ige</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_545" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_547" n="HIAT:w" s="T129">Dĭm</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_550" n="HIAT:w" s="T130">mĭlim</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip">"</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_555" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_557" n="HIAT:w" s="T131">Dĭgəttə</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_560" n="HIAT:w" s="T132">piʔdöbi</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_564" n="HIAT:w" s="T133">ulardə</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_567" n="HIAT:w" s="T134">bokullaʔ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_570" n="HIAT:w" s="T135">ibi</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_574" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_576" n="HIAT:w" s="T136">Kandəga</ts>
                  <nts id="Seg_577" n="HIAT:ip">.</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_580" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_582" n="HIAT:w" s="T137">Maʔ</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_585" n="HIAT:w" s="T138">nuga</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_589" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_591" n="HIAT:w" s="T139">Šübi</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_594" n="HIAT:w" s="T140">dĭ</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_597" n="HIAT:w" s="T141">maʔdə</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_601" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_603" n="HIAT:w" s="T142">Nüke</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_606" n="HIAT:w" s="T143">büzʼe</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_609" n="HIAT:w" s="T144">amnaiʔ</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_613" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_615" n="HIAT:w" s="T145">Udabə</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_618" n="HIAT:w" s="T146">mĭbi</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_622" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_624" n="HIAT:w" s="T147">Mălia:</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_626" n="HIAT:ip">"</nts>
                  <ts e="T149" id="Seg_628" n="HIAT:w" s="T148">Gibər</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_631" n="HIAT:w" s="T149">kandəgal</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_634" n="HIAT:w" s="T150">tăn</ts>
                  <nts id="Seg_635" n="HIAT:ip">?</nts>
                  <nts id="Seg_636" n="HIAT:ip">"</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_639" n="HIAT:u" s="T151">
                  <nts id="Seg_640" n="HIAT:ip">"</nts>
                  <ts e="T152" id="Seg_642" n="HIAT:w" s="T151">Ĭmbənʼim</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_646" n="HIAT:w" s="T152">ĭmbənʼiinʼi</ts>
                  <nts id="Seg_647" n="HIAT:ip">)</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_650" n="HIAT:w" s="T153">jadajlaʔ</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_653" n="HIAT:w" s="T154">mĭmbiem</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_657" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_659" n="HIAT:w" s="T155">Ĭmbənʼim</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_662" n="HIAT:w" s="T156">onʼiʔ</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_665" n="HIAT:w" s="T157">ular</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_668" n="HIAT:w" s="T158">mĭbi</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip">"</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_673" n="HIAT:u" s="T159">
                  <nts id="Seg_674" n="HIAT:ip">"</nts>
                  <ts e="T160" id="Seg_676" n="HIAT:w" s="T159">Öʔle</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_679" n="HIAT:w" s="T160">it</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_682" n="HIAT:w" s="T161">mĭʔ</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_685" n="HIAT:w" s="T162">ularzaŋgənʼibeʔ</ts>
                  <nts id="Seg_686" n="HIAT:ip">!</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_689" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_691" n="HIAT:w" s="T163">Öʔle</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_694" n="HIAT:w" s="T164">it</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_697" n="HIAT:w" s="T165">ularlə</ts>
                  <nts id="Seg_698" n="HIAT:ip">!</nts>
                  <nts id="Seg_699" n="HIAT:ip">"</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_702" n="HIAT:u" s="T166">
                  <nts id="Seg_703" n="HIAT:ip">"</nts>
                  <ts e="T167" id="Seg_705" n="HIAT:w" s="T166">Šĭʔ</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_708" n="HIAT:w" s="T167">ularzaŋnaʔ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_711" n="HIAT:w" s="T168">măn</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_714" n="HIAT:w" s="T169">ularbə</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_717" n="HIAT:w" s="T170">amnuʔlədən</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip">"</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_722" n="HIAT:u" s="T171">
                  <nts id="Seg_723" n="HIAT:ip">"</nts>
                  <ts e="T172" id="Seg_725" n="HIAT:w" s="T171">Ĭmbijleʔ</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_728" n="HIAT:w" s="T172">amnədən</ts>
                  <nts id="Seg_729" n="HIAT:ip">?</nts>
                  <nts id="Seg_730" n="HIAT:ip">"</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_733" n="HIAT:u" s="T173">
                  <nts id="Seg_734" n="HIAT:ip">"</nts>
                  <ts e="T174" id="Seg_736" n="HIAT:w" s="T173">No</ts>
                  <nts id="Seg_737" n="HIAT:ip">,</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_740" n="HIAT:w" s="T174">öʔle</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_743" n="HIAT:w" s="T175">it</ts>
                  <nts id="Seg_744" n="HIAT:ip">!</nts>
                  <nts id="Seg_745" n="HIAT:ip">"</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_748" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_750" n="HIAT:w" s="T176">Büzʼe</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_753" n="HIAT:w" s="T177">kunnaʔ</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_756" n="HIAT:w" s="T178">öʔleʔ</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_759" n="HIAT:w" s="T179">ibi</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_763" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_765" n="HIAT:w" s="T180">Pĭn</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_768" n="HIAT:w" s="T181">uʔbdəbi</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182.tx.1" id="Seg_771" n="HIAT:w" s="T182">Kötʼsʼün</ts>
                  <nts id="Seg_772" n="HIAT:ip">_</nts>
                  <ts e="T183" id="Seg_774" n="HIAT:w" s="T182.tx.1">Güdʼər</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_778" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_780" n="HIAT:w" s="T183">Ulardəbə</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_783" n="HIAT:w" s="T184">bostubə</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_786" n="HIAT:w" s="T185">taləj</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_789" n="HIAT:w" s="T186">nʼeʔbdəbi</ts>
                  <nts id="Seg_790" n="HIAT:ip">,</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_793" n="HIAT:w" s="T187">taləj</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_796" n="HIAT:w" s="T188">nüzəbi</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_800" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_802" n="HIAT:w" s="T189">Ibi</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_805" n="HIAT:w" s="T190">pʼeli</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_808" n="HIAT:w" s="T191">ularin</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_811" n="HIAT:w" s="T192">püjezeŋdə</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_814" n="HIAT:w" s="T193">kemzʼəʔ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_817" n="HIAT:w" s="T194">tʼuʔlababi</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_821" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_823" n="HIAT:w" s="T195">Bostə</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_826" n="HIAT:w" s="T196">kunolsʼtə</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_829" n="HIAT:w" s="T197">iʔbəbi</ts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_833" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_835" n="HIAT:w" s="T198">Ertən</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_838" n="HIAT:w" s="T199">büzʼe</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_841" n="HIAT:w" s="T200">nüketsʼəʔ</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_844" n="HIAT:w" s="T201">uʔbdəbi</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_848" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_850" n="HIAT:w" s="T202">Büzʼe</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_853" n="HIAT:w" s="T203">kambi</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_856" n="HIAT:w" s="T204">šedendə</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_860" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_862" n="HIAT:w" s="T205">Kubində</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_865" n="HIAT:w" s="T206">ular</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_868" n="HIAT:w" s="T207">taləj</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_871" n="HIAT:w" s="T208">nʼeʔbdöleʔ</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_875" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_877" n="HIAT:w" s="T209">Nükeendə</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_880" n="HIAT:w" s="T210">šobiza</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_883" n="HIAT:w" s="T211">nörbəbi:</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_885" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_887" n="HIAT:w" s="T212">Šăn</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_890" n="HIAT:w" s="T213">ĭmbijleʔ</ts>
                  <nts id="Seg_891" n="HIAT:ip">,</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_894" n="HIAT:w" s="T214">dĭ</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_897" n="HIAT:w" s="T215">nʼin</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_900" n="HIAT:w" s="T216">ulardə</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_903" n="HIAT:w" s="T217">mĭʔ</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_906" n="HIAT:w" s="T218">ularzaŋbaʔ</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_909" n="HIAT:w" s="T219">taləj</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_912" n="HIAT:w" s="T220">nüzəbi</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip">"</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_917" n="HIAT:u" s="T221">
                  <ts e="T221.tx.1" id="Seg_919" n="HIAT:w" s="T221">Kötʼsʼün</ts>
                  <nts id="Seg_920" n="HIAT:ip">_</nts>
                  <ts e="T222" id="Seg_922" n="HIAT:w" s="T221.tx.1">Güdʼər</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_925" n="HIAT:w" s="T222">nünneʔ</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_928" n="HIAT:w" s="T223">baʔbi</ts>
                  <nts id="Seg_929" n="HIAT:ip">,</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_932" n="HIAT:w" s="T224">uʔla</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_935" n="HIAT:w" s="T225">saʔməbi</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_939" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_941" n="HIAT:w" s="T226">Mălia:</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_943" n="HIAT:ip">"</nts>
                  <ts e="T228" id="Seg_945" n="HIAT:w" s="T227">Măn</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_948" n="HIAT:w" s="T228">nörbəbiöm</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_951" n="HIAT:w" s="T229">šĭʔnʼidə</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_955" n="HIAT:w" s="T230">ularbə</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_958" n="HIAT:w" s="T231">amnədən</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_962" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_964" n="HIAT:w" s="T232">Šăn</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_967" n="HIAT:w" s="T233">ĭmbijleʔ</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_970" n="HIAT:w" s="T234">amnaʔ</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_973" n="HIAT:w" s="T235">kumbiiʔ</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip">"</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_978" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_980" n="HIAT:w" s="T236">Bazoʔ</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_983" n="HIAT:w" s="T237">tʼorlaʔ</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_986" n="HIAT:w" s="T238">tĭrlöleʔ</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_989" n="HIAT:w" s="T239">kujobi</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_993" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_995" n="HIAT:w" s="T240">Büzʼe</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_998" n="HIAT:w" s="T241">šügəʔ</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_1001" n="HIAT:w" s="T242">săbəjʔleʔbddə</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_1005" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_1007" n="HIAT:w" s="T243">Dĭ</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1010" n="HIAT:w" s="T244">bazoʔ</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1013" n="HIAT:w" s="T245">šünə</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1016" n="HIAT:w" s="T246">tĭrlölie</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_1020" n="HIAT:u" s="T247">
                  <nts id="Seg_1021" n="HIAT:ip">"</nts>
                  <ts e="T248" id="Seg_1023" n="HIAT:w" s="T247">Iʔ</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1026" n="HIAT:w" s="T248">tʼoraʔ</ts>
                  <nts id="Seg_1027" n="HIAT:ip">!</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_1030" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_1032" n="HIAT:w" s="T249">Măn</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1035" n="HIAT:w" s="T250">ularəm</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1038" n="HIAT:w" s="T251">mĭlem</ts>
                  <nts id="Seg_1039" n="HIAT:ip">.</nts>
                  <nts id="Seg_1040" n="HIAT:ip">"</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_1043" n="HIAT:u" s="T252">
                  <nts id="Seg_1044" n="HIAT:ip">"</nts>
                  <ts e="T253" id="Seg_1046" n="HIAT:w" s="T252">Măna</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1049" n="HIAT:w" s="T253">ej</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1052" n="HIAT:w" s="T254">kereʔ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1055" n="HIAT:w" s="T255">tăn</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1058" n="HIAT:w" s="T256">ularlaʔ</ts>
                  <nts id="Seg_1059" n="HIAT:ip">.</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_1062" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_1064" n="HIAT:w" s="T257">Măn</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1067" n="HIAT:w" s="T258">ĭmbinʼim</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1070" n="HIAT:w" s="T259">măna</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1073" n="HIAT:w" s="T260">mĭbi</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip">"</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1078" n="HIAT:u" s="T261">
                  <nts id="Seg_1079" n="HIAT:ip">"</nts>
                  <ts e="T262" id="Seg_1081" n="HIAT:w" s="T261">No</ts>
                  <nts id="Seg_1082" n="HIAT:ip">,</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1085" n="HIAT:w" s="T262">iʔ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1088" n="HIAT:w" s="T263">tʼoraʔ</ts>
                  <nts id="Seg_1089" n="HIAT:ip">!</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_1092" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1094" n="HIAT:w" s="T264">Bar</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1097" n="HIAT:w" s="T265">mĭlim</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1100" n="HIAT:w" s="T266">sumnam</ts>
                  <nts id="Seg_1101" n="HIAT:ip">.</nts>
                  <nts id="Seg_1102" n="HIAT:ip">"</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1105" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1107" n="HIAT:w" s="T267">Pʼüʔtöbi</ts>
                  <nts id="Seg_1108" n="HIAT:ip">,</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1111" n="HIAT:w" s="T268">ularzaŋdə</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1114" n="HIAT:w" s="T269">bokulleibi</ts>
                  <nts id="Seg_1115" n="HIAT:ip">.</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1118" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1120" n="HIAT:w" s="T270">Kandəga</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1124" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1126" n="HIAT:w" s="T271">Küres</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1129" n="HIAT:w" s="T272">pa</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1132" n="HIAT:w" s="T273">nuga</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1136" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1138" n="HIAT:w" s="T274">Dĭm</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275.tx.1" id="Seg_1141" n="HIAT:w" s="T275">Köčün</ts>
                  <nts id="Seg_1142" n="HIAT:ip">_</nts>
                  <ts e="T276" id="Seg_1144" n="HIAT:w" s="T275.tx.1">Güdʼər</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1147" n="HIAT:w" s="T276">tĭllüʔbi</ts>
                  <nts id="Seg_1148" n="HIAT:ip">,</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1151" n="HIAT:w" s="T277">ne</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1154" n="HIAT:w" s="T278">tʼaktə</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1157" n="HIAT:w" s="T279">săbəjʔleibi</ts>
                  <nts id="Seg_1158" n="HIAT:ip">.</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1161" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1163" n="HIAT:w" s="T280">Manžəleibi</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1167" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1169" n="HIAT:w" s="T281">Bazoʔ</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T283" id="Seg_1173" n="HIAT:w" s="T282">mĭllaːndəbi</ts>
                  <nts id="Seg_1174" n="HIAT:ip">)</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1177" n="HIAT:w" s="T283">tʼütʼe</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1180" n="HIAT:w" s="T284">kandəbi</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1184" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1186" n="HIAT:w" s="T285">Maʔ</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1189" n="HIAT:w" s="T286">nuga</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1193" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1195" n="HIAT:w" s="T287">Amnolbi</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1198" n="HIAT:w" s="T288">küne</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1201" n="HIAT:w" s="T289">nem</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1204" n="HIAT:w" s="T290">pan</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1207" n="HIAT:w" s="T291">tabəndə</ts>
                  <nts id="Seg_1208" n="HIAT:ip">,</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1211" n="HIAT:w" s="T292">ularim</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1214" n="HIAT:w" s="T293">udaandə</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1217" n="HIAT:w" s="T294">sarbi</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1221" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1223" n="HIAT:w" s="T295">Tagajdə</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1226" n="HIAT:w" s="T296">sĭjgəndə</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1229" n="HIAT:w" s="T297">müʔbdəbi</ts>
                  <nts id="Seg_1230" n="HIAT:ip">.</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1233" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1235" n="HIAT:w" s="T298">Bostə</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1238" n="HIAT:w" s="T299">kambi</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1241" n="HIAT:w" s="T300">maʔdə</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1245" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1247" n="HIAT:w" s="T301">Büzʼe</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1250" n="HIAT:w" s="T302">amna</ts>
                  <nts id="Seg_1251" n="HIAT:ip">,</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1254" n="HIAT:w" s="T303">šide</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1257" n="HIAT:w" s="T304">koʔbdot</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1260" n="HIAT:w" s="T305">amna</ts>
                  <nts id="Seg_1261" n="HIAT:ip">.</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1264" n="HIAT:u" s="T306">
                  <nts id="Seg_1265" n="HIAT:ip">"</nts>
                  <ts e="T307" id="Seg_1267" n="HIAT:w" s="T306">Amoržəbaʔ</ts>
                  <nts id="Seg_1268" n="HIAT:ip">!</nts>
                  <nts id="Seg_1269" n="HIAT:ip">"</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1272" n="HIAT:u" s="T307">
                  <nts id="Seg_1273" n="HIAT:ip">"</nts>
                  <ts e="T308" id="Seg_1275" n="HIAT:w" s="T307">Măn</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1278" n="HIAT:w" s="T308">nükem</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1281" n="HIAT:w" s="T309">ige</ts>
                  <nts id="Seg_1282" n="HIAT:ip">.</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1285" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1287" n="HIAT:w" s="T310">Pan</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1290" n="HIAT:w" s="T311">tabəndə</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1293" n="HIAT:w" s="T312">ularzaŋdə</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1296" n="HIAT:w" s="T313">tʼabolamnət</ts>
                  <nts id="Seg_1297" n="HIAT:ip">.</nts>
                  <nts id="Seg_1298" n="HIAT:ip">"</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1301" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1303" n="HIAT:w" s="T314">Koʔpsaŋ:</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1305" n="HIAT:ip">"</nts>
                  <ts e="T316" id="Seg_1307" n="HIAT:w" s="T315">Kallaʔ</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1310" n="HIAT:w" s="T316">kăstəlbaʔ</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip">"</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1315" n="HIAT:u" s="T317">
                  <nts id="Seg_1316" n="HIAT:ip">"</nts>
                  <ts e="T318" id="Seg_1318" n="HIAT:w" s="T317">Ige</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1321" n="HIAT:w" s="T318">kaŋga</ts>
                  <nts id="Seg_1322" n="HIAT:ip">!</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1325" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1327" n="HIAT:w" s="T319">Nereʔləleʔ</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1331" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1333" n="HIAT:w" s="T320">Dĭ</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1336" n="HIAT:w" s="T321">neröbində</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1339" n="HIAT:w" s="T322">bospostəbə</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1342" n="HIAT:w" s="T323">tʼăgarəldə</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip">"</nts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1347" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1349" n="HIAT:w" s="T324">Koʔpsaŋ</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1352" n="HIAT:w" s="T325">üʔməllüʔbiiʔ</ts>
                  <nts id="Seg_1353" n="HIAT:ip">.</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1356" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1358" n="HIAT:w" s="T326">Ulariʔ</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1361" n="HIAT:w" s="T327">neröbiiʔ</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1365" n="HIAT:w" s="T328">tunoloʔbdəbi</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1369" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1371" n="HIAT:w" s="T329">Kubindən</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1374" n="HIAT:w" s="T330">tagaj</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1377" n="HIAT:w" s="T331">sijgəndə</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1380" n="HIAT:w" s="T332">müʔbdöleʔbə</ts>
                  <nts id="Seg_1381" n="HIAT:ip">.</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1384" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1386" n="HIAT:w" s="T333">Üʔməleʔ</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1389" n="HIAT:w" s="T334">šobijəʔ</ts>
                  <nts id="Seg_1390" n="HIAT:ip">.</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1393" n="HIAT:u" s="T335">
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <ts e="T335.tx.1" id="Seg_1396" n="HIAT:w" s="T335">Kötʼsʼün</ts>
                  <nts id="Seg_1397" n="HIAT:ip">_</nts>
                  <ts e="T336" id="Seg_1399" n="HIAT:w" s="T335.tx.1">Güdʼər</ts>
                  <nts id="Seg_1400" n="HIAT:ip">,</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336.tx.1" id="Seg_1403" n="HIAT:w" s="T336">Kötʼsʼün</ts>
                  <nts id="Seg_1404" n="HIAT:ip">_</nts>
                  <ts e="T337" id="Seg_1406" n="HIAT:w" s="T336.tx.1">Güdʼər</ts>
                  <nts id="Seg_1407" n="HIAT:ip">,</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1410" n="HIAT:w" s="T337">nükel</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1413" n="HIAT:w" s="T338">tʼăgarobi</ts>
                  <nts id="Seg_1414" n="HIAT:ip">.</nts>
                  <nts id="Seg_1415" n="HIAT:ip">"</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1418" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1420" n="HIAT:w" s="T339">Bazoʔ</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1423" n="HIAT:w" s="T340">tĭrlöleʔ</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1426" n="HIAT:w" s="T341">tʼorlaʔbə</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1430" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1432" n="HIAT:w" s="T342">Šügəʔ</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1435" n="HIAT:w" s="T343">săbəj</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1438" n="HIAT:w" s="T344">iliet</ts>
                  <nts id="Seg_1439" n="HIAT:ip">.</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1442" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1444" n="HIAT:w" s="T345">Dĭ</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1447" n="HIAT:w" s="T346">bazoʔ</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1450" n="HIAT:w" s="T347">šünə</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1453" n="HIAT:w" s="T348">păʔlaʔbəlie</ts>
                  <nts id="Seg_1454" n="HIAT:ip">.</nts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1457" n="HIAT:u" s="T349">
                  <nts id="Seg_1458" n="HIAT:ip">"</nts>
                  <ts e="T350" id="Seg_1460" n="HIAT:w" s="T349">Iʔ</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1463" n="HIAT:w" s="T350">tʼoraʔ</ts>
                  <nts id="Seg_1464" n="HIAT:ip">!</nts>
                  <nts id="Seg_1465" n="HIAT:ip">"</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1468" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1470" n="HIAT:w" s="T351">Măndə</ts>
                  <nts id="Seg_1471" n="HIAT:ip">.</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1474" n="HIAT:u" s="T352">
                  <nts id="Seg_1475" n="HIAT:ip">"</nts>
                  <ts e="T353" id="Seg_1477" n="HIAT:w" s="T352">Koʔbdom</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1480" n="HIAT:w" s="T353">mĭlim</ts>
                  <nts id="Seg_1481" n="HIAT:ip">.</nts>
                  <nts id="Seg_1482" n="HIAT:ip">"</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1485" n="HIAT:u" s="T354">
                  <nts id="Seg_1486" n="HIAT:ip">"</nts>
                  <ts e="T355" id="Seg_1488" n="HIAT:w" s="T354">Măna</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1491" n="HIAT:w" s="T355">ej</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1494" n="HIAT:w" s="T356">kereʔ</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1497" n="HIAT:w" s="T357">tăn</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1500" n="HIAT:w" s="T358">koʔbdol</ts>
                  <nts id="Seg_1501" n="HIAT:ip">.</nts>
                  <nts id="Seg_1502" n="HIAT:ip">"</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1505" n="HIAT:u" s="T359">
                  <nts id="Seg_1506" n="HIAT:ip">"</nts>
                  <ts e="T360" id="Seg_1508" n="HIAT:w" s="T359">Iʔ</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1511" n="HIAT:w" s="T360">tʼoraʔ</ts>
                  <nts id="Seg_1512" n="HIAT:ip">!</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1515" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1517" n="HIAT:w" s="T361">Šide</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1520" n="HIAT:w" s="T362">šöbə</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1523" n="HIAT:w" s="T363">mĭlim</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip">"</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1528" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1530" n="HIAT:w" s="T364">Pʼüʔtöbi</ts>
                  <nts id="Seg_1531" n="HIAT:ip">,</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1534" n="HIAT:w" s="T365">nükezeŋdə</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1537" n="HIAT:w" s="T366">bokulluʔbi</ts>
                  <nts id="Seg_1538" n="HIAT:ip">,</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1541" n="HIAT:w" s="T367">kallaʔ</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1544" n="HIAT:w" s="T368">tʼürbi</ts>
                  <nts id="Seg_1545" n="HIAT:ip">.</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1548" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1550" n="HIAT:w" s="T369">Büzʼe</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1553" n="HIAT:w" s="T370">unʼə</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1556" n="HIAT:w" s="T371">maʔbi</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T372" id="Seg_1559" n="sc" s="T0">
               <ts e="T1" id="Seg_1561" n="e" s="T0">Nüke </ts>
               <ts e="T2" id="Seg_1563" n="e" s="T1">amnobi, </ts>
               <ts e="T3" id="Seg_1565" n="e" s="T2">oʔb </ts>
               <ts e="T4" id="Seg_1567" n="e" s="T3">nʼit </ts>
               <ts e="T5" id="Seg_1569" n="e" s="T4">ibi. </ts>
               <ts e="T6" id="Seg_1571" n="e" s="T5">Nʼit </ts>
               <ts e="T7" id="Seg_1573" n="e" s="T6">ijaandə </ts>
               <ts e="T8" id="Seg_1575" n="e" s="T7">mălia: </ts>
               <ts e="T9" id="Seg_1577" n="e" s="T8">"Măn </ts>
               <ts e="T10" id="Seg_1579" n="e" s="T9">nejləm." </ts>
               <ts e="T11" id="Seg_1581" n="e" s="T10">Ijat </ts>
               <ts e="T12" id="Seg_1583" n="e" s="T11">mălia: </ts>
               <ts e="T13" id="Seg_1585" n="e" s="T12">"Naga, </ts>
               <ts e="T14" id="Seg_1587" n="e" s="T13">manʼəkkən </ts>
               <ts e="T15" id="Seg_1589" n="e" s="T14">ne </ts>
               <ts e="T16" id="Seg_1591" n="e" s="T15">naga." </ts>
               <ts e="T17" id="Seg_1593" n="e" s="T16">"Măn </ts>
               <ts e="T18" id="Seg_1595" n="e" s="T17">kallam </ts>
               <ts e="T19" id="Seg_1597" n="e" s="T18">ne </ts>
               <ts e="T20" id="Seg_1599" n="e" s="T19">pileʔ." </ts>
               <ts e="T21" id="Seg_1601" n="e" s="T20">Kallaʔ </ts>
               <ts e="T22" id="Seg_1603" n="e" s="T21">tʼürbi, </ts>
               <ts e="T23" id="Seg_1605" n="e" s="T22">ijabə </ts>
               <ts e="T24" id="Seg_1607" n="e" s="T23">baʔlaːmbi. </ts>
               <ts e="T25" id="Seg_1609" n="e" s="T24">Kandəga, </ts>
               <ts e="T26" id="Seg_1611" n="e" s="T25">kandəga. </ts>
               <ts e="T27" id="Seg_1613" n="e" s="T26">Tʼăgan </ts>
               <ts e="T28" id="Seg_1615" n="e" s="T27">toʔgəndə </ts>
               <ts e="T29" id="Seg_1617" n="e" s="T28">maʔ </ts>
               <ts e="T30" id="Seg_1619" n="e" s="T29">nuga. </ts>
               <ts e="T31" id="Seg_1621" n="e" s="T30">Šübi </ts>
               <ts e="T32" id="Seg_1623" n="e" s="T31">dĭ </ts>
               <ts e="T33" id="Seg_1625" n="e" s="T32">maʔdə, </ts>
               <ts e="T34" id="Seg_1627" n="e" s="T33">ne </ts>
               <ts e="T35" id="Seg_1629" n="e" s="T34">amna. </ts>
               <ts e="T36" id="Seg_1631" n="e" s="T35">Numan üzəbi, </ts>
               <ts e="T37" id="Seg_1633" n="e" s="T36">udabə </ts>
               <ts e="T38" id="Seg_1635" n="e" s="T37">mĭbi. </ts>
               <ts e="T39" id="Seg_1637" n="e" s="T38">Amnəbi </ts>
               <ts e="T40" id="Seg_1639" n="e" s="T39">šün </ts>
               <ts e="T41" id="Seg_1641" n="e" s="T40">toʔgəndə. </ts>
               <ts e="T42" id="Seg_1643" n="e" s="T41">"Adʼam! </ts>
               <ts e="T43" id="Seg_1645" n="e" s="T42">Aspaʔ </ts>
               <ts e="T44" id="Seg_1647" n="e" s="T43">edəʔ, </ts>
               <ts e="T45" id="Seg_1649" n="e" s="T44">uja </ts>
               <ts e="T46" id="Seg_1651" n="e" s="T45">padaʔ!" </ts>
               <ts e="T47" id="Seg_1653" n="e" s="T46">"Ujam </ts>
               <ts e="T48" id="Seg_1655" n="e" s="T47">naga." </ts>
               <ts e="T49" id="Seg_1657" n="e" s="T48">"Măn </ts>
               <ts e="T50" id="Seg_1659" n="e" s="T49">tetliem." </ts>
               <ts e="T51" id="Seg_1661" n="e" s="T50">Nʼiʔdə </ts>
               <ts e="T52" id="Seg_1663" n="e" s="T51">uʔbdəbi, </ts>
               <ts e="T53" id="Seg_1665" n="e" s="T52">tʼalaš </ts>
               <ts e="T54" id="Seg_1667" n="e" s="T53">šüškü </ts>
               <ts e="T55" id="Seg_1669" n="e" s="T54">ibi. </ts>
               <ts e="T56" id="Seg_1671" n="e" s="T55">Püjebə </ts>
               <ts e="T57" id="Seg_1673" n="e" s="T56">taləj </ts>
               <ts e="T58" id="Seg_1675" n="e" s="T57">toʔbdəbi. </ts>
               <ts e="T59" id="Seg_1677" n="e" s="T58">Kemdə </ts>
               <ts e="T60" id="Seg_1679" n="e" s="T59">mʼaŋnuʔbi. </ts>
               <ts e="T61" id="Seg_1681" n="e" s="T60">Dĭ </ts>
               <ts e="T62" id="Seg_1683" n="e" s="T61">kemzʼəʔ </ts>
               <ts e="T63" id="Seg_1685" n="e" s="T62">šüšküm </ts>
               <ts e="T64" id="Seg_1687" n="e" s="T63">bar </ts>
               <ts e="T65" id="Seg_1689" n="e" s="T64">tʼoʔbdəbi. </ts>
               <ts e="T66" id="Seg_1691" n="e" s="T65">Šübi </ts>
               <ts e="T67" id="Seg_1693" n="e" s="T66">maʔdə, </ts>
               <ts e="T68" id="Seg_1695" n="e" s="T67">dĭ </ts>
               <ts e="T69" id="Seg_1697" n="e" s="T68">šüšküm </ts>
               <ts e="T70" id="Seg_1699" n="e" s="T69">aspaʔdə </ts>
               <ts e="T71" id="Seg_1701" n="e" s="T70">paʔdluʔbi. </ts>
               <ts e="T72" id="Seg_1703" n="e" s="T71">Amnaiʔ, </ts>
               <ts e="T73" id="Seg_1705" n="e" s="T72">tʼăbaktərlaʔbəjəʔ. </ts>
               <ts e="T74" id="Seg_1707" n="e" s="T73">Mălia: </ts>
               <ts e="T75" id="Seg_1709" n="e" s="T74">"Adʼaŋ, </ts>
               <ts e="T76" id="Seg_1711" n="e" s="T75">amoržəbəj!" </ts>
               <ts e="T77" id="Seg_1713" n="e" s="T76">Dĭ </ts>
               <ts e="T78" id="Seg_1715" n="e" s="T77">ne </ts>
               <ts e="T79" id="Seg_1717" n="e" s="T78">aspaʔdəbə </ts>
               <ts e="T80" id="Seg_1719" n="e" s="T79">ibi, </ts>
               <ts e="T81" id="Seg_1721" n="e" s="T80">šündə </ts>
               <ts e="T82" id="Seg_1723" n="e" s="T81">toʔgəndə </ts>
               <ts e="T83" id="Seg_1725" n="e" s="T82">nuldəbi, </ts>
               <ts e="T84" id="Seg_1727" n="e" s="T83">kiʔgobə </ts>
               <ts e="T85" id="Seg_1729" n="e" s="T84">ibi. </ts>
               <ts e="T86" id="Seg_1731" n="e" s="T85">Ujabə </ts>
               <ts e="T87" id="Seg_1733" n="e" s="T86">suʔlaʔbə, </ts>
               <ts e="T88" id="Seg_1735" n="e" s="T87">kur </ts>
               <ts e="T89" id="Seg_1737" n="e" s="T88">le </ts>
               <ts e="T90" id="Seg_1739" n="e" s="T89">aspaʔgən </ts>
               <ts e="T91" id="Seg_1741" n="e" s="T90">koldərlaʔbə. </ts>
               <ts e="T92" id="Seg_1743" n="e" s="T91">"Gijən </ts>
               <ts e="T93" id="Seg_1745" n="e" s="T92">tăn </ts>
               <ts e="T94" id="Seg_1747" n="e" s="T93">ujal?" </ts>
               <ts e="T95" id="Seg_1749" n="e" s="T94">Onʼiʔ </ts>
               <ts e="T96" id="Seg_1751" n="e" s="T95">tʼalaš </ts>
               <ts e="T97" id="Seg_1753" n="e" s="T96">le </ts>
               <ts e="T98" id="Seg_1755" n="e" s="T97">tĭrlöleʔ </ts>
               <ts e="T99" id="Seg_1757" n="e" s="T98">mĭŋge. </ts>
               <ts e="T100" id="Seg_1759" n="e" s="T99">"Kötʼsʼün_Güdʼər, </ts>
               <ts e="T101" id="Seg_1761" n="e" s="T100">gijənde </ts>
               <ts e="T102" id="Seg_1763" n="e" s="T101">ujal? </ts>
               <ts e="T103" id="Seg_1765" n="e" s="T102">Oʔnʼiʔ </ts>
               <ts e="T104" id="Seg_1767" n="e" s="T103">le </ts>
               <ts e="T105" id="Seg_1769" n="e" s="T104">tĭrlöleʔ </ts>
               <ts e="T106" id="Seg_1771" n="e" s="T105">mĭŋge." </ts>
               <ts e="T107" id="Seg_1773" n="e" s="T106">Kötʼsʼün_Güdʼər </ts>
               <ts e="T108" id="Seg_1775" n="e" s="T107">tʼorluʔbi. </ts>
               <ts e="T109" id="Seg_1777" n="e" s="T108">Šünə </ts>
               <ts e="T110" id="Seg_1779" n="e" s="T109">tĭrlöleʔbi. </ts>
               <ts e="T111" id="Seg_1781" n="e" s="T110">Ne </ts>
               <ts e="T112" id="Seg_1783" n="e" s="T111">săbəjʔleʔbi </ts>
               <ts e="T113" id="Seg_1785" n="e" s="T112">ködʼi. </ts>
               <ts e="T114" id="Seg_1787" n="e" s="T113">Nʼeʔleʔbliet. </ts>
               <ts e="T115" id="Seg_1789" n="e" s="T114">"Iʔ </ts>
               <ts e="T116" id="Seg_1791" n="e" s="T115">tʼoraʔ!" </ts>
               <ts e="T117" id="Seg_1793" n="e" s="T116">"Tăn </ts>
               <ts e="T118" id="Seg_1795" n="e" s="T117">aspaʔlə </ts>
               <ts e="T119" id="Seg_1797" n="e" s="T118">koːlaːmbi, </ts>
               <ts e="T120" id="Seg_1799" n="e" s="T119">măn </ts>
               <ts e="T121" id="Seg_1801" n="e" s="T120">ujam </ts>
               <ts e="T122" id="Seg_1803" n="e" s="T121">amnuʔbi." </ts>
               <ts e="T123" id="Seg_1805" n="e" s="T122">Ne: </ts>
               <ts e="T124" id="Seg_1807" n="e" s="T123">"Iʔ </ts>
               <ts e="T125" id="Seg_1809" n="e" s="T124">tʼoraʔ! </ts>
               <ts e="T126" id="Seg_1811" n="e" s="T125">Mĭliem. </ts>
               <ts e="T127" id="Seg_1813" n="e" s="T126">Onʼiʔ </ts>
               <ts e="T128" id="Seg_1815" n="e" s="T127">ularbə </ts>
               <ts e="T129" id="Seg_1817" n="e" s="T128">ige. </ts>
               <ts e="T130" id="Seg_1819" n="e" s="T129">Dĭm </ts>
               <ts e="T131" id="Seg_1821" n="e" s="T130">mĭlim." </ts>
               <ts e="T132" id="Seg_1823" n="e" s="T131">Dĭgəttə </ts>
               <ts e="T133" id="Seg_1825" n="e" s="T132">piʔdöbi, </ts>
               <ts e="T134" id="Seg_1827" n="e" s="T133">ulardə </ts>
               <ts e="T135" id="Seg_1829" n="e" s="T134">bokullaʔ </ts>
               <ts e="T136" id="Seg_1831" n="e" s="T135">ibi. </ts>
               <ts e="T137" id="Seg_1833" n="e" s="T136">Kandəga. </ts>
               <ts e="T138" id="Seg_1835" n="e" s="T137">Maʔ </ts>
               <ts e="T139" id="Seg_1837" n="e" s="T138">nuga. </ts>
               <ts e="T140" id="Seg_1839" n="e" s="T139">Šübi </ts>
               <ts e="T141" id="Seg_1841" n="e" s="T140">dĭ </ts>
               <ts e="T142" id="Seg_1843" n="e" s="T141">maʔdə. </ts>
               <ts e="T143" id="Seg_1845" n="e" s="T142">Nüke </ts>
               <ts e="T144" id="Seg_1847" n="e" s="T143">büzʼe </ts>
               <ts e="T145" id="Seg_1849" n="e" s="T144">amnaiʔ. </ts>
               <ts e="T146" id="Seg_1851" n="e" s="T145">Udabə </ts>
               <ts e="T147" id="Seg_1853" n="e" s="T146">mĭbi. </ts>
               <ts e="T148" id="Seg_1855" n="e" s="T147">Mălia: </ts>
               <ts e="T149" id="Seg_1857" n="e" s="T148">"Gibər </ts>
               <ts e="T150" id="Seg_1859" n="e" s="T149">kandəgal </ts>
               <ts e="T151" id="Seg_1861" n="e" s="T150">tăn?" </ts>
               <ts e="T152" id="Seg_1863" n="e" s="T151">"Ĭmbənʼim </ts>
               <ts e="T153" id="Seg_1865" n="e" s="T152">(ĭmbənʼiinʼi) </ts>
               <ts e="T154" id="Seg_1867" n="e" s="T153">jadajlaʔ </ts>
               <ts e="T155" id="Seg_1869" n="e" s="T154">mĭmbiem. </ts>
               <ts e="T156" id="Seg_1871" n="e" s="T155">Ĭmbənʼim </ts>
               <ts e="T157" id="Seg_1873" n="e" s="T156">onʼiʔ </ts>
               <ts e="T158" id="Seg_1875" n="e" s="T157">ular </ts>
               <ts e="T159" id="Seg_1877" n="e" s="T158">mĭbi." </ts>
               <ts e="T160" id="Seg_1879" n="e" s="T159">"Öʔle </ts>
               <ts e="T161" id="Seg_1881" n="e" s="T160">it </ts>
               <ts e="T162" id="Seg_1883" n="e" s="T161">mĭʔ </ts>
               <ts e="T163" id="Seg_1885" n="e" s="T162">ularzaŋgənʼibeʔ! </ts>
               <ts e="T164" id="Seg_1887" n="e" s="T163">Öʔle </ts>
               <ts e="T165" id="Seg_1889" n="e" s="T164">it </ts>
               <ts e="T166" id="Seg_1891" n="e" s="T165">ularlə!" </ts>
               <ts e="T167" id="Seg_1893" n="e" s="T166">"Šĭʔ </ts>
               <ts e="T168" id="Seg_1895" n="e" s="T167">ularzaŋnaʔ </ts>
               <ts e="T169" id="Seg_1897" n="e" s="T168">măn </ts>
               <ts e="T170" id="Seg_1899" n="e" s="T169">ularbə </ts>
               <ts e="T171" id="Seg_1901" n="e" s="T170">amnuʔlədən." </ts>
               <ts e="T172" id="Seg_1903" n="e" s="T171">"Ĭmbijleʔ </ts>
               <ts e="T173" id="Seg_1905" n="e" s="T172">amnədən?" </ts>
               <ts e="T174" id="Seg_1907" n="e" s="T173">"No, </ts>
               <ts e="T175" id="Seg_1909" n="e" s="T174">öʔle </ts>
               <ts e="T176" id="Seg_1911" n="e" s="T175">it!" </ts>
               <ts e="T177" id="Seg_1913" n="e" s="T176">Büzʼe </ts>
               <ts e="T178" id="Seg_1915" n="e" s="T177">kunnaʔ </ts>
               <ts e="T179" id="Seg_1917" n="e" s="T178">öʔleʔ </ts>
               <ts e="T180" id="Seg_1919" n="e" s="T179">ibi. </ts>
               <ts e="T181" id="Seg_1921" n="e" s="T180">Pĭn </ts>
               <ts e="T182" id="Seg_1923" n="e" s="T181">uʔbdəbi </ts>
               <ts e="T183" id="Seg_1925" n="e" s="T182">Kötʼsʼün_Güdʼər. </ts>
               <ts e="T184" id="Seg_1927" n="e" s="T183">Ulardəbə </ts>
               <ts e="T185" id="Seg_1929" n="e" s="T184">bostubə </ts>
               <ts e="T186" id="Seg_1931" n="e" s="T185">taləj </ts>
               <ts e="T187" id="Seg_1933" n="e" s="T186">nʼeʔbdəbi, </ts>
               <ts e="T188" id="Seg_1935" n="e" s="T187">taləj </ts>
               <ts e="T189" id="Seg_1937" n="e" s="T188">nüzəbi. </ts>
               <ts e="T190" id="Seg_1939" n="e" s="T189">Ibi </ts>
               <ts e="T191" id="Seg_1941" n="e" s="T190">pʼeli </ts>
               <ts e="T192" id="Seg_1943" n="e" s="T191">ularin </ts>
               <ts e="T193" id="Seg_1945" n="e" s="T192">püjezeŋdə </ts>
               <ts e="T194" id="Seg_1947" n="e" s="T193">kemzʼəʔ </ts>
               <ts e="T195" id="Seg_1949" n="e" s="T194">tʼuʔlababi. </ts>
               <ts e="T196" id="Seg_1951" n="e" s="T195">Bostə </ts>
               <ts e="T197" id="Seg_1953" n="e" s="T196">kunolsʼtə </ts>
               <ts e="T198" id="Seg_1955" n="e" s="T197">iʔbəbi. </ts>
               <ts e="T199" id="Seg_1957" n="e" s="T198">Ertən </ts>
               <ts e="T200" id="Seg_1959" n="e" s="T199">büzʼe </ts>
               <ts e="T201" id="Seg_1961" n="e" s="T200">nüketsʼəʔ </ts>
               <ts e="T202" id="Seg_1963" n="e" s="T201">uʔbdəbi. </ts>
               <ts e="T203" id="Seg_1965" n="e" s="T202">Büzʼe </ts>
               <ts e="T204" id="Seg_1967" n="e" s="T203">kambi </ts>
               <ts e="T205" id="Seg_1969" n="e" s="T204">šedendə. </ts>
               <ts e="T206" id="Seg_1971" n="e" s="T205">Kubində </ts>
               <ts e="T207" id="Seg_1973" n="e" s="T206">ular </ts>
               <ts e="T208" id="Seg_1975" n="e" s="T207">taləj </ts>
               <ts e="T209" id="Seg_1977" n="e" s="T208">nʼeʔbdöleʔ. </ts>
               <ts e="T210" id="Seg_1979" n="e" s="T209">Nükeendə </ts>
               <ts e="T211" id="Seg_1981" n="e" s="T210">šobiza </ts>
               <ts e="T212" id="Seg_1983" n="e" s="T211">nörbəbi: </ts>
               <ts e="T213" id="Seg_1985" n="e" s="T212">"Šăn </ts>
               <ts e="T214" id="Seg_1987" n="e" s="T213">ĭmbijleʔ, </ts>
               <ts e="T215" id="Seg_1989" n="e" s="T214">dĭ </ts>
               <ts e="T216" id="Seg_1991" n="e" s="T215">nʼin </ts>
               <ts e="T217" id="Seg_1993" n="e" s="T216">ulardə </ts>
               <ts e="T218" id="Seg_1995" n="e" s="T217">mĭʔ </ts>
               <ts e="T219" id="Seg_1997" n="e" s="T218">ularzaŋbaʔ </ts>
               <ts e="T220" id="Seg_1999" n="e" s="T219">taləj </ts>
               <ts e="T221" id="Seg_2001" n="e" s="T220">nüzəbi." </ts>
               <ts e="T222" id="Seg_2003" n="e" s="T221">Kötʼsʼün_Güdʼər </ts>
               <ts e="T223" id="Seg_2005" n="e" s="T222">nünneʔ </ts>
               <ts e="T224" id="Seg_2007" n="e" s="T223">baʔbi, </ts>
               <ts e="T225" id="Seg_2009" n="e" s="T224">uʔla </ts>
               <ts e="T226" id="Seg_2011" n="e" s="T225">saʔməbi. </ts>
               <ts e="T227" id="Seg_2013" n="e" s="T226">Mălia: </ts>
               <ts e="T228" id="Seg_2015" n="e" s="T227">"Măn </ts>
               <ts e="T229" id="Seg_2017" n="e" s="T228">nörbəbiöm </ts>
               <ts e="T230" id="Seg_2019" n="e" s="T229">šĭʔnʼidə, </ts>
               <ts e="T231" id="Seg_2021" n="e" s="T230">ularbə </ts>
               <ts e="T232" id="Seg_2023" n="e" s="T231">amnədən. </ts>
               <ts e="T233" id="Seg_2025" n="e" s="T232">Šăn </ts>
               <ts e="T234" id="Seg_2027" n="e" s="T233">ĭmbijleʔ </ts>
               <ts e="T235" id="Seg_2029" n="e" s="T234">amnaʔ </ts>
               <ts e="T236" id="Seg_2031" n="e" s="T235">kumbiiʔ." </ts>
               <ts e="T237" id="Seg_2033" n="e" s="T236">Bazoʔ </ts>
               <ts e="T238" id="Seg_2035" n="e" s="T237">tʼorlaʔ </ts>
               <ts e="T239" id="Seg_2037" n="e" s="T238">tĭrlöleʔ </ts>
               <ts e="T240" id="Seg_2039" n="e" s="T239">kujobi. </ts>
               <ts e="T241" id="Seg_2041" n="e" s="T240">Büzʼe </ts>
               <ts e="T242" id="Seg_2043" n="e" s="T241">šügəʔ </ts>
               <ts e="T243" id="Seg_2045" n="e" s="T242">săbəjʔleʔbddə. </ts>
               <ts e="T244" id="Seg_2047" n="e" s="T243">Dĭ </ts>
               <ts e="T245" id="Seg_2049" n="e" s="T244">bazoʔ </ts>
               <ts e="T246" id="Seg_2051" n="e" s="T245">šünə </ts>
               <ts e="T247" id="Seg_2053" n="e" s="T246">tĭrlölie. </ts>
               <ts e="T248" id="Seg_2055" n="e" s="T247">"Iʔ </ts>
               <ts e="T249" id="Seg_2057" n="e" s="T248">tʼoraʔ! </ts>
               <ts e="T250" id="Seg_2059" n="e" s="T249">Măn </ts>
               <ts e="T251" id="Seg_2061" n="e" s="T250">ularəm </ts>
               <ts e="T252" id="Seg_2063" n="e" s="T251">mĭlem." </ts>
               <ts e="T253" id="Seg_2065" n="e" s="T252">"Măna </ts>
               <ts e="T254" id="Seg_2067" n="e" s="T253">ej </ts>
               <ts e="T255" id="Seg_2069" n="e" s="T254">kereʔ </ts>
               <ts e="T256" id="Seg_2071" n="e" s="T255">tăn </ts>
               <ts e="T257" id="Seg_2073" n="e" s="T256">ularlaʔ. </ts>
               <ts e="T258" id="Seg_2075" n="e" s="T257">Măn </ts>
               <ts e="T259" id="Seg_2077" n="e" s="T258">ĭmbinʼim </ts>
               <ts e="T260" id="Seg_2079" n="e" s="T259">măna </ts>
               <ts e="T261" id="Seg_2081" n="e" s="T260">mĭbi." </ts>
               <ts e="T262" id="Seg_2083" n="e" s="T261">"No, </ts>
               <ts e="T263" id="Seg_2085" n="e" s="T262">iʔ </ts>
               <ts e="T264" id="Seg_2087" n="e" s="T263">tʼoraʔ! </ts>
               <ts e="T265" id="Seg_2089" n="e" s="T264">Bar </ts>
               <ts e="T266" id="Seg_2091" n="e" s="T265">mĭlim </ts>
               <ts e="T267" id="Seg_2093" n="e" s="T266">sumnam." </ts>
               <ts e="T268" id="Seg_2095" n="e" s="T267">Pʼüʔtöbi, </ts>
               <ts e="T269" id="Seg_2097" n="e" s="T268">ularzaŋdə </ts>
               <ts e="T270" id="Seg_2099" n="e" s="T269">bokulleibi. </ts>
               <ts e="T271" id="Seg_2101" n="e" s="T270">Kandəga. </ts>
               <ts e="T272" id="Seg_2103" n="e" s="T271">Küres </ts>
               <ts e="T273" id="Seg_2105" n="e" s="T272">pa </ts>
               <ts e="T274" id="Seg_2107" n="e" s="T273">nuga. </ts>
               <ts e="T275" id="Seg_2109" n="e" s="T274">Dĭm </ts>
               <ts e="T276" id="Seg_2111" n="e" s="T275">Köčün_Güdʼər </ts>
               <ts e="T277" id="Seg_2113" n="e" s="T276">tĭllüʔbi, </ts>
               <ts e="T278" id="Seg_2115" n="e" s="T277">ne </ts>
               <ts e="T279" id="Seg_2117" n="e" s="T278">tʼaktə </ts>
               <ts e="T280" id="Seg_2119" n="e" s="T279">săbəjʔleibi. </ts>
               <ts e="T281" id="Seg_2121" n="e" s="T280">Manžəleibi. </ts>
               <ts e="T282" id="Seg_2123" n="e" s="T281">Bazoʔ </ts>
               <ts e="T283" id="Seg_2125" n="e" s="T282">(mĭllaːndəbi) </ts>
               <ts e="T284" id="Seg_2127" n="e" s="T283">tʼütʼe </ts>
               <ts e="T285" id="Seg_2129" n="e" s="T284">kandəbi. </ts>
               <ts e="T286" id="Seg_2131" n="e" s="T285">Maʔ </ts>
               <ts e="T287" id="Seg_2133" n="e" s="T286">nuga. </ts>
               <ts e="T288" id="Seg_2135" n="e" s="T287">Amnolbi </ts>
               <ts e="T289" id="Seg_2137" n="e" s="T288">küne </ts>
               <ts e="T290" id="Seg_2139" n="e" s="T289">nem </ts>
               <ts e="T291" id="Seg_2141" n="e" s="T290">pan </ts>
               <ts e="T292" id="Seg_2143" n="e" s="T291">tabəndə, </ts>
               <ts e="T293" id="Seg_2145" n="e" s="T292">ularim </ts>
               <ts e="T294" id="Seg_2147" n="e" s="T293">udaandə </ts>
               <ts e="T295" id="Seg_2149" n="e" s="T294">sarbi. </ts>
               <ts e="T296" id="Seg_2151" n="e" s="T295">Tagajdə </ts>
               <ts e="T297" id="Seg_2153" n="e" s="T296">sĭjgəndə </ts>
               <ts e="T298" id="Seg_2155" n="e" s="T297">müʔbdəbi. </ts>
               <ts e="T299" id="Seg_2157" n="e" s="T298">Bostə </ts>
               <ts e="T300" id="Seg_2159" n="e" s="T299">kambi </ts>
               <ts e="T301" id="Seg_2161" n="e" s="T300">maʔdə. </ts>
               <ts e="T302" id="Seg_2163" n="e" s="T301">Büzʼe </ts>
               <ts e="T303" id="Seg_2165" n="e" s="T302">amna, </ts>
               <ts e="T304" id="Seg_2167" n="e" s="T303">šide </ts>
               <ts e="T305" id="Seg_2169" n="e" s="T304">koʔbdot </ts>
               <ts e="T306" id="Seg_2171" n="e" s="T305">amna. </ts>
               <ts e="T307" id="Seg_2173" n="e" s="T306">"Amoržəbaʔ!" </ts>
               <ts e="T308" id="Seg_2175" n="e" s="T307">"Măn </ts>
               <ts e="T309" id="Seg_2177" n="e" s="T308">nükem </ts>
               <ts e="T310" id="Seg_2179" n="e" s="T309">ige. </ts>
               <ts e="T311" id="Seg_2181" n="e" s="T310">Pan </ts>
               <ts e="T312" id="Seg_2183" n="e" s="T311">tabəndə </ts>
               <ts e="T313" id="Seg_2185" n="e" s="T312">ularzaŋdə </ts>
               <ts e="T314" id="Seg_2187" n="e" s="T313">tʼabolamnət." </ts>
               <ts e="T315" id="Seg_2189" n="e" s="T314">Koʔpsaŋ: </ts>
               <ts e="T316" id="Seg_2191" n="e" s="T315">"Kallaʔ </ts>
               <ts e="T317" id="Seg_2193" n="e" s="T316">kăstəlbaʔ." </ts>
               <ts e="T318" id="Seg_2195" n="e" s="T317">"Ige </ts>
               <ts e="T319" id="Seg_2197" n="e" s="T318">kaŋga! </ts>
               <ts e="T320" id="Seg_2199" n="e" s="T319">Nereʔləleʔ. </ts>
               <ts e="T321" id="Seg_2201" n="e" s="T320">Dĭ </ts>
               <ts e="T322" id="Seg_2203" n="e" s="T321">neröbində </ts>
               <ts e="T323" id="Seg_2205" n="e" s="T322">bospostəbə </ts>
               <ts e="T324" id="Seg_2207" n="e" s="T323">tʼăgarəldə." </ts>
               <ts e="T325" id="Seg_2209" n="e" s="T324">Koʔpsaŋ </ts>
               <ts e="T326" id="Seg_2211" n="e" s="T325">üʔməllüʔbiiʔ. </ts>
               <ts e="T327" id="Seg_2213" n="e" s="T326">Ulariʔ </ts>
               <ts e="T328" id="Seg_2215" n="e" s="T327">neröbiiʔ, </ts>
               <ts e="T329" id="Seg_2217" n="e" s="T328">tunoloʔbdəbi. </ts>
               <ts e="T330" id="Seg_2219" n="e" s="T329">Kubindən </ts>
               <ts e="T331" id="Seg_2221" n="e" s="T330">tagaj </ts>
               <ts e="T332" id="Seg_2223" n="e" s="T331">sijgəndə </ts>
               <ts e="T333" id="Seg_2225" n="e" s="T332">müʔbdöleʔbə. </ts>
               <ts e="T334" id="Seg_2227" n="e" s="T333">Üʔməleʔ </ts>
               <ts e="T335" id="Seg_2229" n="e" s="T334">šobijəʔ. </ts>
               <ts e="T336" id="Seg_2231" n="e" s="T335">"Kötʼsʼün_Güdʼər, </ts>
               <ts e="T337" id="Seg_2233" n="e" s="T336">Kötʼsʼün_Güdʼər, </ts>
               <ts e="T338" id="Seg_2235" n="e" s="T337">nükel </ts>
               <ts e="T339" id="Seg_2237" n="e" s="T338">tʼăgarobi." </ts>
               <ts e="T340" id="Seg_2239" n="e" s="T339">Bazoʔ </ts>
               <ts e="T341" id="Seg_2241" n="e" s="T340">tĭrlöleʔ </ts>
               <ts e="T342" id="Seg_2243" n="e" s="T341">tʼorlaʔbə. </ts>
               <ts e="T343" id="Seg_2245" n="e" s="T342">Šügəʔ </ts>
               <ts e="T344" id="Seg_2247" n="e" s="T343">săbəj </ts>
               <ts e="T345" id="Seg_2249" n="e" s="T344">iliet. </ts>
               <ts e="T346" id="Seg_2251" n="e" s="T345">Dĭ </ts>
               <ts e="T347" id="Seg_2253" n="e" s="T346">bazoʔ </ts>
               <ts e="T348" id="Seg_2255" n="e" s="T347">šünə </ts>
               <ts e="T349" id="Seg_2257" n="e" s="T348">păʔlaʔbəlie. </ts>
               <ts e="T350" id="Seg_2259" n="e" s="T349">"Iʔ </ts>
               <ts e="T351" id="Seg_2261" n="e" s="T350">tʼoraʔ!" </ts>
               <ts e="T352" id="Seg_2263" n="e" s="T351">Măndə. </ts>
               <ts e="T353" id="Seg_2265" n="e" s="T352">"Koʔbdom </ts>
               <ts e="T354" id="Seg_2267" n="e" s="T353">mĭlim." </ts>
               <ts e="T355" id="Seg_2269" n="e" s="T354">"Măna </ts>
               <ts e="T356" id="Seg_2271" n="e" s="T355">ej </ts>
               <ts e="T357" id="Seg_2273" n="e" s="T356">kereʔ </ts>
               <ts e="T358" id="Seg_2275" n="e" s="T357">tăn </ts>
               <ts e="T359" id="Seg_2277" n="e" s="T358">koʔbdol." </ts>
               <ts e="T360" id="Seg_2279" n="e" s="T359">"Iʔ </ts>
               <ts e="T361" id="Seg_2281" n="e" s="T360">tʼoraʔ! </ts>
               <ts e="T362" id="Seg_2283" n="e" s="T361">Šide </ts>
               <ts e="T363" id="Seg_2285" n="e" s="T362">šöbə </ts>
               <ts e="T364" id="Seg_2287" n="e" s="T363">mĭlim." </ts>
               <ts e="T365" id="Seg_2289" n="e" s="T364">Pʼüʔtöbi, </ts>
               <ts e="T366" id="Seg_2291" n="e" s="T365">nükezeŋdə </ts>
               <ts e="T367" id="Seg_2293" n="e" s="T366">bokulluʔbi, </ts>
               <ts e="T368" id="Seg_2295" n="e" s="T367">kallaʔ </ts>
               <ts e="T369" id="Seg_2297" n="e" s="T368">tʼürbi. </ts>
               <ts e="T370" id="Seg_2299" n="e" s="T369">Büzʼe </ts>
               <ts e="T371" id="Seg_2301" n="e" s="T370">unʼə </ts>
               <ts e="T372" id="Seg_2303" n="e" s="T371">maʔbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_2304" s="T0">AA_1914_Corpse_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_2305" s="T5">AA_1914_Corpse_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_2306" s="T10">AA_1914_Corpse_flk.003 (001.004))</ta>
            <ta e="T20" id="Seg_2307" s="T16">AA_1914_Corpse_flk.004 (001.006)</ta>
            <ta e="T24" id="Seg_2308" s="T20">AA_1914_Corpse_flk.005 (001.007)</ta>
            <ta e="T26" id="Seg_2309" s="T24">AA_1914_Corpse_flk.006 (002.001)</ta>
            <ta e="T30" id="Seg_2310" s="T26">AA_1914_Corpse_flk.007 (002.002)</ta>
            <ta e="T35" id="Seg_2311" s="T30">AA_1914_Corpse_flk.008 (002.003)</ta>
            <ta e="T38" id="Seg_2312" s="T35">AA_1914_Corpse_flk.009 (002.004)</ta>
            <ta e="T41" id="Seg_2313" s="T38">AA_1914_Corpse_flk.010 (002.005)</ta>
            <ta e="T42" id="Seg_2314" s="T41">AA_1914_Corpse_flk.011 (002.006)</ta>
            <ta e="T46" id="Seg_2315" s="T42">AA_1914_Corpse_flk.012 (002.007)</ta>
            <ta e="T48" id="Seg_2316" s="T46">AA_1914_Corpse_flk.013 (002.008)</ta>
            <ta e="T50" id="Seg_2317" s="T48">AA_1914_Corpse_flk.014 (002.009)</ta>
            <ta e="T55" id="Seg_2318" s="T50">AA_1914_Corpse_flk.015 (002.010)</ta>
            <ta e="T58" id="Seg_2319" s="T55">AA_1914_Corpse_flk.016 (002.011)</ta>
            <ta e="T60" id="Seg_2320" s="T58">AA_1914_Corpse_flk.017 (002.012)</ta>
            <ta e="T65" id="Seg_2321" s="T60">AA_1914_Corpse_flk.018 (002.013)</ta>
            <ta e="T71" id="Seg_2322" s="T65">AA_1914_Corpse_flk.019 (002.014)</ta>
            <ta e="T73" id="Seg_2323" s="T71">AA_1914_Corpse_flk.020 (002.015)</ta>
            <ta e="T76" id="Seg_2324" s="T73">AA_1914_Corpse_flk.021 (002.016)</ta>
            <ta e="T85" id="Seg_2325" s="T76">AA_1914_Corpse_flk.022 (002.018)</ta>
            <ta e="T91" id="Seg_2326" s="T85">AA_1914_Corpse_flk.023 (002.019)</ta>
            <ta e="T94" id="Seg_2327" s="T91">AA_1914_Corpse_flk.024 (002.020)</ta>
            <ta e="T99" id="Seg_2328" s="T94">AA_1914_Corpse_flk.025 (002.021)</ta>
            <ta e="T102" id="Seg_2329" s="T99">AA_1914_Corpse_flk.026 (002.022)</ta>
            <ta e="T106" id="Seg_2330" s="T102">AA_1914_Corpse_flk.027 (002.023)</ta>
            <ta e="T108" id="Seg_2331" s="T106">AA_1914_Corpse_flk.028 (002.024)</ta>
            <ta e="T110" id="Seg_2332" s="T108">AA_1914_Corpse_flk.029 (002.025)</ta>
            <ta e="T113" id="Seg_2333" s="T110">AA_1914_Corpse_flk.030 (002.026)</ta>
            <ta e="T114" id="Seg_2334" s="T113">AA_1914_Corpse_flk.031 (002.027)</ta>
            <ta e="T116" id="Seg_2335" s="T114">AA_1914_Corpse_flk.032 (002.028)</ta>
            <ta e="T122" id="Seg_2336" s="T116">AA_1914_Corpse_flk.033 (002.029)</ta>
            <ta e="T125" id="Seg_2337" s="T122">AA_1914_Corpse_flk.034 (002.030)</ta>
            <ta e="T126" id="Seg_2338" s="T125">AA_1914_Corpse_flk.035 (002.032)</ta>
            <ta e="T129" id="Seg_2339" s="T126">AA_1914_Corpse_flk.036 (002.033)</ta>
            <ta e="T131" id="Seg_2340" s="T129">AA_1914_Corpse_flk.037 (002.034)</ta>
            <ta e="T136" id="Seg_2341" s="T131">AA_1914_Corpse_flk.038 (002.035)</ta>
            <ta e="T137" id="Seg_2342" s="T136">AA_1914_Corpse_flk.039 (003.001)</ta>
            <ta e="T139" id="Seg_2343" s="T137">AA_1914_Corpse_flk.040 (003.002)</ta>
            <ta e="T142" id="Seg_2344" s="T139">AA_1914_Corpse_flk.041 (003.003)</ta>
            <ta e="T145" id="Seg_2345" s="T142">AA_1914_Corpse_flk.042 (003.004)</ta>
            <ta e="T147" id="Seg_2346" s="T145">AA_1914_Corpse_flk.043 (003.005)</ta>
            <ta e="T151" id="Seg_2347" s="T147">AA_1914_Corpse_flk.044 (003.006)</ta>
            <ta e="T155" id="Seg_2348" s="T151">AA_1914_Corpse_flk.045 (003.008)</ta>
            <ta e="T159" id="Seg_2349" s="T155">AA_1914_Corpse_flk.046 (003.009)</ta>
            <ta e="T163" id="Seg_2350" s="T159">AA_1914_Corpse_flk.047 (003.010)</ta>
            <ta e="T166" id="Seg_2351" s="T163">AA_1914_Corpse_flk.048 (003.011)</ta>
            <ta e="T171" id="Seg_2352" s="T166">AA_1914_Corpse_flk.049 (003.012)</ta>
            <ta e="T173" id="Seg_2353" s="T171">AA_1914_Corpse_flk.050 (003.013)</ta>
            <ta e="T176" id="Seg_2354" s="T173">AA_1914_Corpse_flk.051 (003.014)</ta>
            <ta e="T180" id="Seg_2355" s="T176">AA_1914_Corpse_flk.052 (003.015)</ta>
            <ta e="T183" id="Seg_2356" s="T180">AA_1914_Corpse_flk.053 (003.016)</ta>
            <ta e="T189" id="Seg_2357" s="T183">AA_1914_Corpse_flk.054 (003.017)</ta>
            <ta e="T195" id="Seg_2358" s="T189">AA_1914_Corpse_flk.055 (003.018)</ta>
            <ta e="T198" id="Seg_2359" s="T195">AA_1914_Corpse_flk.056 (003.019)</ta>
            <ta e="T202" id="Seg_2360" s="T198">AA_1914_Corpse_flk.057 (003.020)</ta>
            <ta e="T205" id="Seg_2361" s="T202">AA_1914_Corpse_flk.058 (003.021)</ta>
            <ta e="T209" id="Seg_2362" s="T205">AA_1914_Corpse_flk.059 (003.022)</ta>
            <ta e="T221" id="Seg_2363" s="T209">AA_1914_Corpse_flk.060 (003.023) </ta>
            <ta e="T226" id="Seg_2364" s="T221">AA_1914_Corpse_flk.061 (003.025)</ta>
            <ta e="T232" id="Seg_2365" s="T226">AA_1914_Corpse_flk.062 (003.026)</ta>
            <ta e="T236" id="Seg_2366" s="T232">AA_1914_Corpse_flk.063 (003.028)</ta>
            <ta e="T240" id="Seg_2367" s="T236">AA_1914_Corpse_flk.064 (003.029)</ta>
            <ta e="T243" id="Seg_2368" s="T240">AA_1914_Corpse_flk.065 (003.030)</ta>
            <ta e="T247" id="Seg_2369" s="T243">AA_1914_Corpse_flk.066 (003.031)</ta>
            <ta e="T249" id="Seg_2370" s="T247">AA_1914_Corpse_flk.067 (003.032)</ta>
            <ta e="T252" id="Seg_2371" s="T249">AA_1914_Corpse_flk.068 (003.033)</ta>
            <ta e="T257" id="Seg_2372" s="T252">AA_1914_Corpse_flk.069 (003.034)</ta>
            <ta e="T261" id="Seg_2373" s="T257">AA_1914_Corpse_flk.070 (003.035)</ta>
            <ta e="T264" id="Seg_2374" s="T261">AA_1914_Corpse_flk.071 (003.036)</ta>
            <ta e="T267" id="Seg_2375" s="T264">AA_1914_Corpse_flk.072 (003.037)</ta>
            <ta e="T270" id="Seg_2376" s="T267">AA_1914_Corpse_flk.073 (003.038)</ta>
            <ta e="T271" id="Seg_2377" s="T270">AA_1914_Corpse_flk.074 (004.001)</ta>
            <ta e="T274" id="Seg_2378" s="T271">AA_1914_Corpse_flk.075 (004.002)</ta>
            <ta e="T280" id="Seg_2379" s="T274">AA_1914_Corpse_flk.076 (004.003)</ta>
            <ta e="T281" id="Seg_2380" s="T280">AA_1914_Corpse_flk.077 (004.004)</ta>
            <ta e="T285" id="Seg_2381" s="T281">AA_1914_Corpse_flk.078 (004.005)</ta>
            <ta e="T287" id="Seg_2382" s="T285">AA_1914_Corpse_flk.079 (004.006)</ta>
            <ta e="T295" id="Seg_2383" s="T287">AA_1914_Corpse_flk.080 (004.007)</ta>
            <ta e="T298" id="Seg_2384" s="T295">AA_1914_Corpse_flk.081 (004.008)</ta>
            <ta e="T301" id="Seg_2385" s="T298">AA_1914_Corpse_flk.082 (004.009)</ta>
            <ta e="T306" id="Seg_2386" s="T301">AA_1914_Corpse_flk.083 (005.001)</ta>
            <ta e="T307" id="Seg_2387" s="T306">AA_1914_Corpse_flk.084 (005.002)</ta>
            <ta e="T310" id="Seg_2388" s="T307">AA_1914_Corpse_flk.085 (005.003)</ta>
            <ta e="T314" id="Seg_2389" s="T310">AA_1914_Corpse_flk.086 (005.004)</ta>
            <ta e="T317" id="Seg_2390" s="T314">AA_1914_Corpse_flk.087 (005.005)</ta>
            <ta e="T319" id="Seg_2391" s="T317">AA_1914_Corpse_flk.088 (005.007)</ta>
            <ta e="T320" id="Seg_2392" s="T319">AA_1914_Corpse_flk.089 (005.008)</ta>
            <ta e="T324" id="Seg_2393" s="T320">AA_1914_Corpse_flk.090 (005.009)</ta>
            <ta e="T326" id="Seg_2394" s="T324">AA_1914_Corpse_flk.091 (005.010)</ta>
            <ta e="T329" id="Seg_2395" s="T326">AA_1914_Corpse_flk.092 (005.011)</ta>
            <ta e="T333" id="Seg_2396" s="T329">AA_1914_Corpse_flk.093 (005.012)</ta>
            <ta e="T335" id="Seg_2397" s="T333">AA_1914_Corpse_flk.094 (005.013)</ta>
            <ta e="T339" id="Seg_2398" s="T335">AA_1914_Corpse_flk.095 (005.014)</ta>
            <ta e="T342" id="Seg_2399" s="T339">AA_1914_Corpse_flk.096 (005.015)</ta>
            <ta e="T345" id="Seg_2400" s="T342">AA_1914_Corpse_flk.097 (005.016)</ta>
            <ta e="T349" id="Seg_2401" s="T345">AA_1914_Corpse_flk.098 (005.017)</ta>
            <ta e="T351" id="Seg_2402" s="T349">AA_1914_Corpse_flk.099 (005.018)</ta>
            <ta e="T352" id="Seg_2403" s="T351">AA_1914_Corpse_flk.100 (005.019)</ta>
            <ta e="T354" id="Seg_2404" s="T352">AA_1914_Corpse_flk.101 (005.020)</ta>
            <ta e="T359" id="Seg_2405" s="T354">AA_1914_Corpse_flk.102 (005.021)</ta>
            <ta e="T361" id="Seg_2406" s="T359">AA_1914_Corpse_flk.103 (005.022)</ta>
            <ta e="T364" id="Seg_2407" s="T361">AA_1914_Corpse_flk.104 (005.023)</ta>
            <ta e="T369" id="Seg_2408" s="T364">AA_1914_Corpse_flk.105 (005.024)</ta>
            <ta e="T372" id="Seg_2409" s="T369">AA_1914_Corpse_flk.106 (005.025)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_2410" s="T0">Nüke amnobi, oʔb nʼit ibi. </ta>
            <ta e="T10" id="Seg_2411" s="T5">Nʼit ijaandə mălia: "Măn nejləm." </ta>
            <ta e="T16" id="Seg_2412" s="T10">Ijat mălia: "Naga, manʼəkkən ne naga." </ta>
            <ta e="T20" id="Seg_2413" s="T16">"Măn kallam ne pileʔ." </ta>
            <ta e="T24" id="Seg_2414" s="T20">Kallaʔ tʼürbi, ijabə baʔlaːmbi. </ta>
            <ta e="T26" id="Seg_2415" s="T24">Kandəga, kandəga. </ta>
            <ta e="T30" id="Seg_2416" s="T26">Tʼăgan toʔgəndə maʔ nuga. </ta>
            <ta e="T35" id="Seg_2417" s="T30">Šübi dĭ maʔdə, ne amna. </ta>
            <ta e="T38" id="Seg_2418" s="T35">Numan üzəbi, udabə mĭbi. </ta>
            <ta e="T41" id="Seg_2419" s="T38">Amnəbi šün toʔgəndə. </ta>
            <ta e="T42" id="Seg_2420" s="T41">"Adʼam! </ta>
            <ta e="T46" id="Seg_2421" s="T42">Aspaʔ edəʔ, uja padaʔ!" </ta>
            <ta e="T48" id="Seg_2422" s="T46">"Ujam naga." </ta>
            <ta e="T50" id="Seg_2423" s="T48">"Măn tetliem." </ta>
            <ta e="T55" id="Seg_2424" s="T50">Nʼiʔdə uʔbdəbi, tʼalaš šüškü ibi. </ta>
            <ta e="T58" id="Seg_2425" s="T55">Püjebə taləj toʔbdəbi. </ta>
            <ta e="T60" id="Seg_2426" s="T58">Kemdə mʼaŋnuʔbi. </ta>
            <ta e="T65" id="Seg_2427" s="T60">Dĭ kemzʼəʔ šüšküm bar tʼoʔbdəbi. </ta>
            <ta e="T71" id="Seg_2428" s="T65">Šübi maʔdə, dĭ šüšküm aspaʔdə paʔdluʔbi. </ta>
            <ta e="T73" id="Seg_2429" s="T71">Amnaiʔ, tʼăbaktərlaʔbəjəʔ. </ta>
            <ta e="T76" id="Seg_2430" s="T73">Mălia: "Adʼaŋ, amoržəbəj!" </ta>
            <ta e="T85" id="Seg_2431" s="T76">Dĭ ne aspaʔdəbə ibi, šündə toʔgəndə nuldəbi, kiʔgobə ibi. </ta>
            <ta e="T91" id="Seg_2432" s="T85">Ujabə suʔlaʔbə, kur le aspaʔgən koldərlaʔbə. </ta>
            <ta e="T94" id="Seg_2433" s="T91">"Gijən tăn ujal?" </ta>
            <ta e="T99" id="Seg_2434" s="T94">Onʼiʔ tʼalaš le tĭrlöleʔ mĭŋge. </ta>
            <ta e="T102" id="Seg_2435" s="T99">"Kötʼsʼün_Güdʼər, gijənde ujal? </ta>
            <ta e="T106" id="Seg_2436" s="T102">Oʔnʼiʔ le tĭrlöleʔ mĭŋge." </ta>
            <ta e="T108" id="Seg_2437" s="T106">Kötʼsʼün_Güdʼər tʼorluʔbi. </ta>
            <ta e="T110" id="Seg_2438" s="T108">Šünə tĭrlöleʔbi. </ta>
            <ta e="T113" id="Seg_2439" s="T110">Ne săbəjʔleʔbi ködʼi. </ta>
            <ta e="T114" id="Seg_2440" s="T113">Nʼeʔleʔbliet. </ta>
            <ta e="T116" id="Seg_2441" s="T114">"Iʔ tʼoraʔ!" </ta>
            <ta e="T122" id="Seg_2442" s="T116">"Tăn aspaʔlə koːlaːmbi, măn ujam amnuʔbi." </ta>
            <ta e="T125" id="Seg_2443" s="T122">Ne: "Iʔ tʼoraʔ! </ta>
            <ta e="T126" id="Seg_2444" s="T125">Mĭliem. </ta>
            <ta e="T129" id="Seg_2445" s="T126">Onʼiʔ ularbə ige. </ta>
            <ta e="T131" id="Seg_2446" s="T129">Dĭm mĭlim." </ta>
            <ta e="T136" id="Seg_2447" s="T131">Dĭgəttə piʔdöbi, ulardə bokullaʔ ibi. </ta>
            <ta e="T137" id="Seg_2448" s="T136">Kandəga. </ta>
            <ta e="T139" id="Seg_2449" s="T137">Maʔ nuga. </ta>
            <ta e="T142" id="Seg_2450" s="T139">Šübi dĭ maʔdə. </ta>
            <ta e="T145" id="Seg_2451" s="T142">Nüke büzʼe amnaiʔ. </ta>
            <ta e="T147" id="Seg_2452" s="T145">Udabə mĭbi. </ta>
            <ta e="T151" id="Seg_2453" s="T147">Mălia: "Gibər kandəgal tăn?" </ta>
            <ta e="T155" id="Seg_2454" s="T151">"Ĭmbənʼim (ĭmbənʼiinʼi) jadajlaʔ mĭmbiem. </ta>
            <ta e="T159" id="Seg_2455" s="T155">Ĭmbənʼim onʼiʔ ular mĭbi." </ta>
            <ta e="T163" id="Seg_2456" s="T159">"Öʔle it mĭʔ ularzaŋgənʼibeʔ! </ta>
            <ta e="T166" id="Seg_2457" s="T163">Öʔle it ularlə!" </ta>
            <ta e="T171" id="Seg_2458" s="T166">"Šĭʔ ularzaŋnaʔ măn ularbə amnuʔlədən." </ta>
            <ta e="T173" id="Seg_2459" s="T171">"Ĭmbijleʔ amnədən?." </ta>
            <ta e="T176" id="Seg_2460" s="T173">"No, öʔle it!" </ta>
            <ta e="T180" id="Seg_2461" s="T176">Büzʼe kunnaʔ öʔleʔ ibi. </ta>
            <ta e="T183" id="Seg_2462" s="T180">Pĭn uʔbdəbi Kötʼsʼün_Güdʼər. </ta>
            <ta e="T189" id="Seg_2463" s="T183">Ulardəbə bostubə taləj nʼeʔbdəbi, taləj nüzəbi. </ta>
            <ta e="T195" id="Seg_2464" s="T189">Ibi pʼeli ularin püjezeŋdə kemzʼəʔ tʼuʔlababi. </ta>
            <ta e="T198" id="Seg_2465" s="T195">Bostə kunolsʼtə iʔbəbi. </ta>
            <ta e="T202" id="Seg_2466" s="T198">Ertən büzʼe nüketsʼəʔ uʔbdəbi. </ta>
            <ta e="T205" id="Seg_2467" s="T202">Büzʼe kambi šedendə. </ta>
            <ta e="T209" id="Seg_2468" s="T205">Kubində ular taləj nʼeʔbdöleʔ. </ta>
            <ta e="T221" id="Seg_2469" s="T209">Nükeendə šobiza nörbəbi: "Šăn ĭmbijleʔ, dĭ nʼin ulardə mĭʔ ularzaŋbaʔ taləj nüzəbi." </ta>
            <ta e="T226" id="Seg_2470" s="T221">Kötʼsʼün_Güdʼər nünneʔ baʔbi, uʔla saʔməbi.</ta>
            <ta e="T232" id="Seg_2471" s="T226">Mălia: "Măn nörbəbiöm šĭʔnʼidə, ularbə amnədən. </ta>
            <ta e="T236" id="Seg_2472" s="T232">Šăn ĭmbijleʔ amnaʔ kumbiiʔ." </ta>
            <ta e="T240" id="Seg_2473" s="T236">Bazoʔ tʼorlaʔ tĭrlöleʔ kujobi. </ta>
            <ta e="T243" id="Seg_2474" s="T240">Büzʼe šügəʔ săbəjʔleʔbddə. </ta>
            <ta e="T247" id="Seg_2475" s="T243">Dĭ bazoʔ šünə tĭrlölie. </ta>
            <ta e="T249" id="Seg_2476" s="T247">"Iʔ tʼoraʔ! </ta>
            <ta e="T252" id="Seg_2477" s="T249">Măn ularəm mĭlem." </ta>
            <ta e="T257" id="Seg_2478" s="T252">"Măna ej kereʔ tăn ularlaʔ. </ta>
            <ta e="T261" id="Seg_2479" s="T257">Măn ĭmbinʼim măna mĭbi." </ta>
            <ta e="T264" id="Seg_2480" s="T261">"No, iʔ tʼoraʔ! </ta>
            <ta e="T267" id="Seg_2481" s="T264">Bar mĭlim sumnam." </ta>
            <ta e="T270" id="Seg_2482" s="T267">Pʼüʔtöbi, ularzaŋdə bokulleibi. </ta>
            <ta e="T271" id="Seg_2483" s="T270">Kandəga. </ta>
            <ta e="T274" id="Seg_2484" s="T271">Küres pa nuga. </ta>
            <ta e="T280" id="Seg_2485" s="T274">Dĭm Köčün Güdʼər tĭllüʔbi, ne tʼaktə săbəjʔleibi. </ta>
            <ta e="T281" id="Seg_2486" s="T280">Manžəleibi. </ta>
            <ta e="T285" id="Seg_2487" s="T281">Bazoʔ (mĭllaːndəbi) tʼütʼe kandəbi. </ta>
            <ta e="T287" id="Seg_2488" s="T285">Maʔ nuga. </ta>
            <ta e="T295" id="Seg_2489" s="T287">Amnolbi küne nem pan tabəndə, ularim udaandə sarbi. </ta>
            <ta e="T298" id="Seg_2490" s="T295">Tagajdə sĭjgəndə müʔbdəbi. </ta>
            <ta e="T301" id="Seg_2491" s="T298">Bostə kambi maʔdə. </ta>
            <ta e="T306" id="Seg_2492" s="T301">Büzʼe amna, šide koʔbdot amna. </ta>
            <ta e="T307" id="Seg_2493" s="T306">"Amoržəbaʔ!" </ta>
            <ta e="T310" id="Seg_2494" s="T307">"Măn nükem ige. </ta>
            <ta e="T314" id="Seg_2495" s="T310">Pan tabəndə ularzaŋdə tʼabolamnət." </ta>
            <ta e="T317" id="Seg_2496" s="T314">Koʔpsaŋ: "Kallaʔ kăstəlbaʔ." </ta>
            <ta e="T319" id="Seg_2497" s="T317">"Ige kaŋga! </ta>
            <ta e="T320" id="Seg_2498" s="T319">Nereʔləleʔ. </ta>
            <ta e="T324" id="Seg_2499" s="T320">Dĭ neröbində bospostəbə tʼăgarəldə." </ta>
            <ta e="T326" id="Seg_2500" s="T324">Koʔpsaŋ üʔməllüʔbiiʔ. </ta>
            <ta e="T329" id="Seg_2501" s="T326">Ulariʔ neröbiiʔ, tunoloʔbdəbi. </ta>
            <ta e="T333" id="Seg_2502" s="T329">Kubindən tagaj sijgəndə müʔbdöleʔbə. </ta>
            <ta e="T335" id="Seg_2503" s="T333">Üʔməleʔ šobijəʔ. </ta>
            <ta e="T339" id="Seg_2504" s="T335">"Kötʼsʼün_Güdʼər, Kötʼsʼün_Güdʼər, nükel tʼăgarobi." </ta>
            <ta e="T342" id="Seg_2505" s="T339">Bazoʔ tĭrlöleʔ tʼorlaʔbə. </ta>
            <ta e="T345" id="Seg_2506" s="T342">Šügəʔ săbəj iliet. </ta>
            <ta e="T349" id="Seg_2507" s="T345">Dĭ bazoʔ šünə păʔlaʔbəlie. </ta>
            <ta e="T351" id="Seg_2508" s="T349">"Iʔ tʼoraʔ!" </ta>
            <ta e="T352" id="Seg_2509" s="T351">Măndə. </ta>
            <ta e="T354" id="Seg_2510" s="T352">"Koʔbdom mĭlim." </ta>
            <ta e="T359" id="Seg_2511" s="T354">"Măna ej kereʔ tăn koʔbdol." </ta>
            <ta e="T361" id="Seg_2512" s="T359">"Iʔ tʼoraʔ! </ta>
            <ta e="T364" id="Seg_2513" s="T361">Šide šöbə mĭlim." </ta>
            <ta e="T369" id="Seg_2514" s="T364">Pʼüʔtöbi, nükezeŋdə bokulluʔbi, kallaʔ tʼürbi. </ta>
            <ta e="T372" id="Seg_2515" s="T369">Büzʼe unʼə maʔbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2516" s="T0">nüke</ta>
            <ta e="T2" id="Seg_2517" s="T1">amno-bi</ta>
            <ta e="T3" id="Seg_2518" s="T2">oʔb</ta>
            <ta e="T4" id="Seg_2519" s="T3">nʼi-t</ta>
            <ta e="T5" id="Seg_2520" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_2521" s="T5">nʼi-t</ta>
            <ta e="T7" id="Seg_2522" s="T6">ija-andə</ta>
            <ta e="T8" id="Seg_2523" s="T7">mă-lia</ta>
            <ta e="T9" id="Seg_2524" s="T8">măn</ta>
            <ta e="T10" id="Seg_2525" s="T9">ne-j-lə-m</ta>
            <ta e="T11" id="Seg_2526" s="T10">ija-t</ta>
            <ta e="T12" id="Seg_2527" s="T11">mă-lia</ta>
            <ta e="T13" id="Seg_2528" s="T12">naga</ta>
            <ta e="T14" id="Seg_2529" s="T13">manʼək-kən</ta>
            <ta e="T15" id="Seg_2530" s="T14">ne</ta>
            <ta e="T16" id="Seg_2531" s="T15">naga</ta>
            <ta e="T17" id="Seg_2532" s="T16">măn</ta>
            <ta e="T18" id="Seg_2533" s="T17">kal-la-m</ta>
            <ta e="T19" id="Seg_2534" s="T18">ne</ta>
            <ta e="T20" id="Seg_2535" s="T19">pi-leʔ</ta>
            <ta e="T21" id="Seg_2536" s="T20">kal-laʔ</ta>
            <ta e="T22" id="Seg_2537" s="T21">tʼür-bi</ta>
            <ta e="T23" id="Seg_2538" s="T22">ija-bə</ta>
            <ta e="T24" id="Seg_2539" s="T23">baʔ-laːm-bi</ta>
            <ta e="T25" id="Seg_2540" s="T24">kandə-ga</ta>
            <ta e="T26" id="Seg_2541" s="T25">kandə-ga</ta>
            <ta e="T27" id="Seg_2542" s="T26">tʼăga-n</ta>
            <ta e="T28" id="Seg_2543" s="T27">toʔ-gəndə</ta>
            <ta e="T29" id="Seg_2544" s="T28">maʔ</ta>
            <ta e="T30" id="Seg_2545" s="T29">nu-ga</ta>
            <ta e="T31" id="Seg_2546" s="T30">šü-bi</ta>
            <ta e="T32" id="Seg_2547" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_2548" s="T32">maʔ-də</ta>
            <ta e="T34" id="Seg_2549" s="T33">ne</ta>
            <ta e="T35" id="Seg_2550" s="T34">amna</ta>
            <ta e="T36" id="Seg_2551" s="T35">numan üzə-bi</ta>
            <ta e="T37" id="Seg_2552" s="T36">uda-bə</ta>
            <ta e="T38" id="Seg_2553" s="T37">mĭ-bi</ta>
            <ta e="T39" id="Seg_2554" s="T38">amnə-bi</ta>
            <ta e="T40" id="Seg_2555" s="T39">šü-n</ta>
            <ta e="T41" id="Seg_2556" s="T40">toʔ-gəndə</ta>
            <ta e="T42" id="Seg_2557" s="T41">adʼa-m</ta>
            <ta e="T43" id="Seg_2558" s="T42">aspaʔ</ta>
            <ta e="T44" id="Seg_2559" s="T43">edə-ʔ</ta>
            <ta e="T45" id="Seg_2560" s="T44">uja</ta>
            <ta e="T46" id="Seg_2561" s="T45">pada-ʔ</ta>
            <ta e="T47" id="Seg_2562" s="T46">uja-m</ta>
            <ta e="T48" id="Seg_2563" s="T47">naga</ta>
            <ta e="T49" id="Seg_2564" s="T48">măn</ta>
            <ta e="T50" id="Seg_2565" s="T49">tet-lie-m</ta>
            <ta e="T51" id="Seg_2566" s="T50">nʼiʔdə</ta>
            <ta e="T52" id="Seg_2567" s="T51">uʔbdə-bi</ta>
            <ta e="T53" id="Seg_2568" s="T52">tʼalaš</ta>
            <ta e="T54" id="Seg_2569" s="T53">šüškü</ta>
            <ta e="T55" id="Seg_2570" s="T54">i-bi</ta>
            <ta e="T56" id="Seg_2571" s="T55">püje-bə</ta>
            <ta e="T57" id="Seg_2572" s="T56">talə-j</ta>
            <ta e="T58" id="Seg_2573" s="T57">toʔbdə-bi</ta>
            <ta e="T59" id="Seg_2574" s="T58">kem-də</ta>
            <ta e="T60" id="Seg_2575" s="T59">mʼaŋ-nuʔ-bi</ta>
            <ta e="T61" id="Seg_2576" s="T60">dĭ</ta>
            <ta e="T62" id="Seg_2577" s="T61">kem-zʼəʔ</ta>
            <ta e="T63" id="Seg_2578" s="T62">šüškü-m</ta>
            <ta e="T64" id="Seg_2579" s="T63">bar</ta>
            <ta e="T65" id="Seg_2580" s="T64">tʼoʔbdə-bi</ta>
            <ta e="T66" id="Seg_2581" s="T65">šü-bi</ta>
            <ta e="T67" id="Seg_2582" s="T66">maʔ-də</ta>
            <ta e="T68" id="Seg_2583" s="T67">dĭ</ta>
            <ta e="T69" id="Seg_2584" s="T68">šüškü-m</ta>
            <ta e="T70" id="Seg_2585" s="T69">aspaʔ-də</ta>
            <ta e="T71" id="Seg_2586" s="T70">paʔd-luʔ-bi</ta>
            <ta e="T72" id="Seg_2587" s="T71">amna-iʔ</ta>
            <ta e="T73" id="Seg_2588" s="T72">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T74" id="Seg_2589" s="T73">mă-lia</ta>
            <ta e="T75" id="Seg_2590" s="T74">adʼa-ŋ</ta>
            <ta e="T76" id="Seg_2591" s="T75">amor-žə-bəj</ta>
            <ta e="T77" id="Seg_2592" s="T76">dĭ</ta>
            <ta e="T78" id="Seg_2593" s="T77">ne</ta>
            <ta e="T79" id="Seg_2594" s="T78">aspaʔ-də-bə</ta>
            <ta e="T80" id="Seg_2595" s="T79">i-bi</ta>
            <ta e="T81" id="Seg_2596" s="T80">šü-ndə</ta>
            <ta e="T82" id="Seg_2597" s="T81">toʔ-gəndə</ta>
            <ta e="T83" id="Seg_2598" s="T82">nuldə-bi</ta>
            <ta e="T84" id="Seg_2599" s="T83">kiʔgo-bə</ta>
            <ta e="T85" id="Seg_2600" s="T84">i-bi</ta>
            <ta e="T86" id="Seg_2601" s="T85">uja-bə</ta>
            <ta e="T87" id="Seg_2602" s="T86">suʔ-laʔbə</ta>
            <ta e="T88" id="Seg_2603" s="T87">kur</ta>
            <ta e="T89" id="Seg_2604" s="T88">le</ta>
            <ta e="T90" id="Seg_2605" s="T89">aspaʔ-gən</ta>
            <ta e="T91" id="Seg_2606" s="T90">koldər-laʔbə</ta>
            <ta e="T92" id="Seg_2607" s="T91">gijən</ta>
            <ta e="T93" id="Seg_2608" s="T92">tăn</ta>
            <ta e="T94" id="Seg_2609" s="T93">uja-l</ta>
            <ta e="T95" id="Seg_2610" s="T94">onʼiʔ</ta>
            <ta e="T96" id="Seg_2611" s="T95">tʼalaš</ta>
            <ta e="T97" id="Seg_2612" s="T96">le</ta>
            <ta e="T98" id="Seg_2613" s="T97">tĭrlö-leʔ</ta>
            <ta e="T99" id="Seg_2614" s="T98">mĭŋ-ge</ta>
            <ta e="T100" id="Seg_2615" s="T99">Kötʼsʼün_Güdʼər</ta>
            <ta e="T101" id="Seg_2616" s="T100">gijən=de</ta>
            <ta e="T102" id="Seg_2617" s="T101">uja-l</ta>
            <ta e="T103" id="Seg_2618" s="T102">oʔnʼiʔ</ta>
            <ta e="T104" id="Seg_2619" s="T103">le</ta>
            <ta e="T105" id="Seg_2620" s="T104">tĭrlö-leʔ</ta>
            <ta e="T106" id="Seg_2621" s="T105">mĭŋ-ge</ta>
            <ta e="T107" id="Seg_2622" s="T106">Kötʼsʼün_Güdʼər</ta>
            <ta e="T108" id="Seg_2623" s="T107">tʼor-luʔ-bi</ta>
            <ta e="T109" id="Seg_2624" s="T108">šü-nə</ta>
            <ta e="T110" id="Seg_2625" s="T109">tĭrlö-leʔ-bi</ta>
            <ta e="T111" id="Seg_2626" s="T110">ne</ta>
            <ta e="T112" id="Seg_2627" s="T111">săbəjʔ-leʔ-bi</ta>
            <ta e="T113" id="Seg_2628" s="T112">ködʼi</ta>
            <ta e="T114" id="Seg_2629" s="T113">nʼeʔ-leʔb-lie-t</ta>
            <ta e="T115" id="Seg_2630" s="T114">i-ʔ</ta>
            <ta e="T116" id="Seg_2631" s="T115">tʼor-a-ʔ</ta>
            <ta e="T117" id="Seg_2632" s="T116">tăn</ta>
            <ta e="T118" id="Seg_2633" s="T117">aspaʔ-lə</ta>
            <ta e="T119" id="Seg_2634" s="T118">koː-laːm-bi</ta>
            <ta e="T120" id="Seg_2635" s="T119">măn</ta>
            <ta e="T121" id="Seg_2636" s="T120">uja-m</ta>
            <ta e="T122" id="Seg_2637" s="T121">am-nuʔ-bi</ta>
            <ta e="T123" id="Seg_2638" s="T122">ne</ta>
            <ta e="T124" id="Seg_2639" s="T123">i-ʔ</ta>
            <ta e="T125" id="Seg_2640" s="T124">tʼor-a-ʔ</ta>
            <ta e="T126" id="Seg_2641" s="T125">mĭ-lie-m</ta>
            <ta e="T127" id="Seg_2642" s="T126">onʼiʔ</ta>
            <ta e="T128" id="Seg_2643" s="T127">ular-bə</ta>
            <ta e="T129" id="Seg_2644" s="T128">i-ge</ta>
            <ta e="T130" id="Seg_2645" s="T129">dĭ-m</ta>
            <ta e="T131" id="Seg_2646" s="T130">mĭ-li-m</ta>
            <ta e="T132" id="Seg_2647" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_2648" s="T132">piʔdö-bi</ta>
            <ta e="T134" id="Seg_2649" s="T133">ular-də</ta>
            <ta e="T135" id="Seg_2650" s="T134">bokul-laʔ</ta>
            <ta e="T136" id="Seg_2651" s="T135">i-bi</ta>
            <ta e="T137" id="Seg_2652" s="T136">kandə-ga</ta>
            <ta e="T138" id="Seg_2653" s="T137">maʔ</ta>
            <ta e="T139" id="Seg_2654" s="T138">nu-ga</ta>
            <ta e="T140" id="Seg_2655" s="T139">šü-bi</ta>
            <ta e="T141" id="Seg_2656" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_2657" s="T141">maʔ-də</ta>
            <ta e="T143" id="Seg_2658" s="T142">nüke</ta>
            <ta e="T144" id="Seg_2659" s="T143">büzʼe</ta>
            <ta e="T145" id="Seg_2660" s="T144">amna-iʔ</ta>
            <ta e="T146" id="Seg_2661" s="T145">uda-bə</ta>
            <ta e="T147" id="Seg_2662" s="T146">mĭ-bi</ta>
            <ta e="T148" id="Seg_2663" s="T147">mă-lia</ta>
            <ta e="T149" id="Seg_2664" s="T148">gibər</ta>
            <ta e="T150" id="Seg_2665" s="T149">kandə-ga-l</ta>
            <ta e="T151" id="Seg_2666" s="T150">tăn</ta>
            <ta e="T152" id="Seg_2667" s="T151">ĭmbənʼi-m</ta>
            <ta e="T153" id="Seg_2668" s="T152">ĭmbənʼi-inʼi</ta>
            <ta e="T154" id="Seg_2669" s="T153">jada-j-laʔ</ta>
            <ta e="T155" id="Seg_2670" s="T154">mĭm-bie-m</ta>
            <ta e="T156" id="Seg_2671" s="T155">ĭmbənʼi-m</ta>
            <ta e="T157" id="Seg_2672" s="T156">onʼiʔ</ta>
            <ta e="T158" id="Seg_2673" s="T157">ular</ta>
            <ta e="T159" id="Seg_2674" s="T158">mĭ-bi</ta>
            <ta e="T160" id="Seg_2675" s="T159">öʔ-le</ta>
            <ta e="T161" id="Seg_2676" s="T160">i-t</ta>
            <ta e="T162" id="Seg_2677" s="T161">mĭʔ</ta>
            <ta e="T163" id="Seg_2678" s="T162">ular-zaŋ-gənʼibeʔ</ta>
            <ta e="T164" id="Seg_2679" s="T163">öʔ-le</ta>
            <ta e="T165" id="Seg_2680" s="T164">i-t</ta>
            <ta e="T166" id="Seg_2681" s="T165">ular-lə</ta>
            <ta e="T167" id="Seg_2682" s="T166">šĭʔ</ta>
            <ta e="T168" id="Seg_2683" s="T167">ular-zaŋ-naʔ</ta>
            <ta e="T169" id="Seg_2684" s="T168">măn</ta>
            <ta e="T170" id="Seg_2685" s="T169">ular-bə</ta>
            <ta e="T171" id="Seg_2686" s="T170">am-nuʔ-lə-dən</ta>
            <ta e="T172" id="Seg_2687" s="T171">ĭmbi-j-leʔ</ta>
            <ta e="T173" id="Seg_2688" s="T172">am-nə-dən</ta>
            <ta e="T174" id="Seg_2689" s="T173">no</ta>
            <ta e="T175" id="Seg_2690" s="T174">öʔ-le</ta>
            <ta e="T176" id="Seg_2691" s="T175">i-t</ta>
            <ta e="T177" id="Seg_2692" s="T176">büzʼe</ta>
            <ta e="T178" id="Seg_2693" s="T177">kun-naʔ</ta>
            <ta e="T179" id="Seg_2694" s="T178">öʔ-leʔ</ta>
            <ta e="T180" id="Seg_2695" s="T179">i-bi</ta>
            <ta e="T181" id="Seg_2696" s="T180">pĭ-n</ta>
            <ta e="T182" id="Seg_2697" s="T181">uʔbdə-bi</ta>
            <ta e="T183" id="Seg_2698" s="T182">Kötʼsʼün_Güdʼər</ta>
            <ta e="T184" id="Seg_2699" s="T183">ular-də-bə</ta>
            <ta e="T185" id="Seg_2700" s="T184">bostu-bə</ta>
            <ta e="T186" id="Seg_2701" s="T185">talə-j</ta>
            <ta e="T187" id="Seg_2702" s="T186">nʼeʔbdə-bi</ta>
            <ta e="T188" id="Seg_2703" s="T187">talə-j</ta>
            <ta e="T189" id="Seg_2704" s="T188">nüzə-bi</ta>
            <ta e="T190" id="Seg_2705" s="T189">i-bi</ta>
            <ta e="T191" id="Seg_2706" s="T190">pʼel-i</ta>
            <ta e="T192" id="Seg_2707" s="T191">ular-i-n</ta>
            <ta e="T193" id="Seg_2708" s="T192">püje-zeŋ-də</ta>
            <ta e="T194" id="Seg_2709" s="T193">kem-zʼəʔ</ta>
            <ta e="T195" id="Seg_2710" s="T194">tʼuʔ-laba-bi</ta>
            <ta e="T196" id="Seg_2711" s="T195">bos-tə</ta>
            <ta e="T197" id="Seg_2712" s="T196">kunol-sʼtə</ta>
            <ta e="T198" id="Seg_2713" s="T197">iʔbə-bi</ta>
            <ta e="T199" id="Seg_2714" s="T198">ertə-n</ta>
            <ta e="T200" id="Seg_2715" s="T199">büzʼe</ta>
            <ta e="T201" id="Seg_2716" s="T200">nüke-t-sʼəʔ</ta>
            <ta e="T202" id="Seg_2717" s="T201">uʔbdə-bi</ta>
            <ta e="T203" id="Seg_2718" s="T202">büzʼe</ta>
            <ta e="T204" id="Seg_2719" s="T203">kam-bi</ta>
            <ta e="T205" id="Seg_2720" s="T204">šeden-də</ta>
            <ta e="T206" id="Seg_2721" s="T205">ku-bi-ndə</ta>
            <ta e="T207" id="Seg_2722" s="T206">ular</ta>
            <ta e="T208" id="Seg_2723" s="T207">talə-j</ta>
            <ta e="T209" id="Seg_2724" s="T208">nʼeʔbd-ö-leʔ</ta>
            <ta e="T210" id="Seg_2725" s="T209">nüke-endə</ta>
            <ta e="T211" id="Seg_2726" s="T210">šo-biza</ta>
            <ta e="T212" id="Seg_2727" s="T211">nörbə-bi</ta>
            <ta e="T213" id="Seg_2728" s="T212">šen</ta>
            <ta e="T214" id="Seg_2729" s="T213">ĭmbi-leʔ</ta>
            <ta e="T215" id="Seg_2730" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_2731" s="T215">nʼi-n</ta>
            <ta e="T217" id="Seg_2732" s="T216">ular-də</ta>
            <ta e="T218" id="Seg_2733" s="T217">mĭʔ</ta>
            <ta e="T219" id="Seg_2734" s="T218">ular-zaŋ-bAʔ</ta>
            <ta e="T220" id="Seg_2735" s="T219">talə-j</ta>
            <ta e="T221" id="Seg_2736" s="T220">nüzə-bi</ta>
            <ta e="T222" id="Seg_2737" s="T221">Kötʼsʼün_Güdʼər</ta>
            <ta e="T223" id="Seg_2738" s="T222">nün-neʔ</ta>
            <ta e="T224" id="Seg_2739" s="T223">baʔ-bi</ta>
            <ta e="T225" id="Seg_2740" s="T224">uʔ-la</ta>
            <ta e="T226" id="Seg_2741" s="T225">saʔmə-bi</ta>
            <ta e="T227" id="Seg_2742" s="T226">mă-lia</ta>
            <ta e="T228" id="Seg_2743" s="T227">măn</ta>
            <ta e="T229" id="Seg_2744" s="T228">nörbə-biö-m</ta>
            <ta e="T230" id="Seg_2745" s="T229">šĭʔnʼidə</ta>
            <ta e="T231" id="Seg_2746" s="T230">ular-bə</ta>
            <ta e="T232" id="Seg_2747" s="T231">am-nə-dən</ta>
            <ta e="T233" id="Seg_2748" s="T232">šen</ta>
            <ta e="T234" id="Seg_2749" s="T233">ĭmbi-leʔ</ta>
            <ta e="T235" id="Seg_2750" s="T234">am-naʔ</ta>
            <ta e="T236" id="Seg_2751" s="T235">kum-bi-iʔ</ta>
            <ta e="T237" id="Seg_2752" s="T236">bazoʔ</ta>
            <ta e="T238" id="Seg_2753" s="T237">tʼor-laʔ</ta>
            <ta e="T239" id="Seg_2754" s="T238">tĭrlö-leʔ</ta>
            <ta e="T240" id="Seg_2755" s="T239">kujo-bi</ta>
            <ta e="T241" id="Seg_2756" s="T240">büzʼe</ta>
            <ta e="T242" id="Seg_2757" s="T241">šü-gəʔ</ta>
            <ta e="T243" id="Seg_2758" s="T242">săbəjʔ-leʔbd-də</ta>
            <ta e="T244" id="Seg_2759" s="T243">dĭ</ta>
            <ta e="T245" id="Seg_2760" s="T244">bazoʔ</ta>
            <ta e="T246" id="Seg_2761" s="T245">šü-nə</ta>
            <ta e="T247" id="Seg_2762" s="T246">tĭrlö-lie</ta>
            <ta e="T248" id="Seg_2763" s="T247">i-ʔ</ta>
            <ta e="T249" id="Seg_2764" s="T248">tʼor-a-ʔ</ta>
            <ta e="T250" id="Seg_2765" s="T249">măn</ta>
            <ta e="T251" id="Seg_2766" s="T250">ular-əm</ta>
            <ta e="T252" id="Seg_2767" s="T251">mĭ-le-m</ta>
            <ta e="T253" id="Seg_2768" s="T252">măna</ta>
            <ta e="T254" id="Seg_2769" s="T253">ej</ta>
            <ta e="T255" id="Seg_2770" s="T254">kereʔ</ta>
            <ta e="T256" id="Seg_2771" s="T255">tăn</ta>
            <ta e="T257" id="Seg_2772" s="T256">ular-laʔ</ta>
            <ta e="T258" id="Seg_2773" s="T257">măn</ta>
            <ta e="T259" id="Seg_2774" s="T258">ĭmbinʼi-m</ta>
            <ta e="T260" id="Seg_2775" s="T259">măna</ta>
            <ta e="T261" id="Seg_2776" s="T260">mĭ-bi</ta>
            <ta e="T262" id="Seg_2777" s="T261">no</ta>
            <ta e="T263" id="Seg_2778" s="T262">i-ʔ</ta>
            <ta e="T264" id="Seg_2779" s="T263">tʼor-a-ʔ</ta>
            <ta e="T265" id="Seg_2780" s="T264">bar</ta>
            <ta e="T266" id="Seg_2781" s="T265">mĭ-li-m</ta>
            <ta e="T267" id="Seg_2782" s="T266">sumna-m</ta>
            <ta e="T268" id="Seg_2783" s="T267">pʼüʔtö-bi</ta>
            <ta e="T269" id="Seg_2784" s="T268">ular-zaŋ-də</ta>
            <ta e="T270" id="Seg_2785" s="T269">bokul-lei-bi</ta>
            <ta e="T271" id="Seg_2786" s="T270">kandə-ga</ta>
            <ta e="T272" id="Seg_2787" s="T271">küres</ta>
            <ta e="T273" id="Seg_2788" s="T272">pa</ta>
            <ta e="T274" id="Seg_2789" s="T273">nu-ga</ta>
            <ta e="T275" id="Seg_2790" s="T274">dĭ-m</ta>
            <ta e="T276" id="Seg_2791" s="T275">Köčün_Güdʼər</ta>
            <ta e="T277" id="Seg_2792" s="T276">tĭl-lüʔ-bi</ta>
            <ta e="T278" id="Seg_2793" s="T277">ne</ta>
            <ta e="T279" id="Seg_2794" s="T278">tʼaktə</ta>
            <ta e="T280" id="Seg_2795" s="T279">săbəjʔ-lei-bi</ta>
            <ta e="T281" id="Seg_2796" s="T280">manžə-lei-bi</ta>
            <ta e="T282" id="Seg_2797" s="T281">bazoʔ</ta>
            <ta e="T283" id="Seg_2798" s="T282">mĭl-laːndə-bi</ta>
            <ta e="T284" id="Seg_2799" s="T283">tʼütʼe</ta>
            <ta e="T285" id="Seg_2800" s="T284">kandə-bi</ta>
            <ta e="T286" id="Seg_2801" s="T285">maʔ</ta>
            <ta e="T287" id="Seg_2802" s="T286">nu-ga</ta>
            <ta e="T288" id="Seg_2803" s="T287">amnol-bi</ta>
            <ta e="T289" id="Seg_2804" s="T288">kü-ne</ta>
            <ta e="T290" id="Seg_2805" s="T289">ne-m</ta>
            <ta e="T291" id="Seg_2806" s="T290">pa-n</ta>
            <ta e="T292" id="Seg_2807" s="T291">tabə-ndə</ta>
            <ta e="T293" id="Seg_2808" s="T292">ular-i-m</ta>
            <ta e="T294" id="Seg_2809" s="T293">uda-andə</ta>
            <ta e="T295" id="Seg_2810" s="T294">sar-bi</ta>
            <ta e="T296" id="Seg_2811" s="T295">tagaj-də</ta>
            <ta e="T297" id="Seg_2812" s="T296">sĭj-gəndə</ta>
            <ta e="T298" id="Seg_2813" s="T297">müʔbdə-bi</ta>
            <ta e="T299" id="Seg_2814" s="T298">bos-tə</ta>
            <ta e="T300" id="Seg_2815" s="T299">kam-bi</ta>
            <ta e="T301" id="Seg_2816" s="T300">maʔ-də</ta>
            <ta e="T302" id="Seg_2817" s="T301">büzʼe</ta>
            <ta e="T303" id="Seg_2818" s="T302">amna</ta>
            <ta e="T304" id="Seg_2819" s="T303">šide</ta>
            <ta e="T305" id="Seg_2820" s="T304">koʔbdo-t</ta>
            <ta e="T306" id="Seg_2821" s="T305">amna</ta>
            <ta e="T307" id="Seg_2822" s="T306">amor-žə-baʔ</ta>
            <ta e="T308" id="Seg_2823" s="T307">măn</ta>
            <ta e="T309" id="Seg_2824" s="T308">nüke-m</ta>
            <ta e="T310" id="Seg_2825" s="T309">i-ge</ta>
            <ta e="T311" id="Seg_2826" s="T310">pa-n</ta>
            <ta e="T312" id="Seg_2827" s="T311">tabə-ndə</ta>
            <ta e="T313" id="Seg_2828" s="T312">ular-zaŋ-də</ta>
            <ta e="T314" id="Seg_2829" s="T313">tʼabo-lamnə-t</ta>
            <ta e="T315" id="Seg_2830" s="T314">koʔp-saŋ</ta>
            <ta e="T316" id="Seg_2831" s="T315">kal-laʔ</ta>
            <ta e="T317" id="Seg_2832" s="T316">kăstə-l-baʔ</ta>
            <ta e="T318" id="Seg_2833" s="T317">i-ge</ta>
            <ta e="T319" id="Seg_2834" s="T318">kaŋ-ga</ta>
            <ta e="T320" id="Seg_2835" s="T319">nereʔ-lə-leʔ</ta>
            <ta e="T321" id="Seg_2836" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_2837" s="T321">nerö-bi-ndə</ta>
            <ta e="T323" id="Seg_2838" s="T322">bospostə-bə</ta>
            <ta e="T324" id="Seg_2839" s="T323">tʼăgar-əl-də</ta>
            <ta e="T325" id="Seg_2840" s="T324">koʔp-saŋ</ta>
            <ta e="T326" id="Seg_2841" s="T325">üʔməl-lüʔ-bi-iʔ</ta>
            <ta e="T327" id="Seg_2842" s="T326">ular-iʔ</ta>
            <ta e="T328" id="Seg_2843" s="T327">nerö-bi-iʔ</ta>
            <ta e="T329" id="Seg_2844" s="T328">tuno-loʔbdə-bi</ta>
            <ta e="T330" id="Seg_2845" s="T329">ku-bi-ndən</ta>
            <ta e="T331" id="Seg_2846" s="T330">tagaj</ta>
            <ta e="T332" id="Seg_2847" s="T331">sij-gəndə</ta>
            <ta e="T333" id="Seg_2848" s="T332">müʔbd-ö-leʔbə</ta>
            <ta e="T334" id="Seg_2849" s="T333">üʔmə-leʔ</ta>
            <ta e="T335" id="Seg_2850" s="T334">šo-bi-jəʔ</ta>
            <ta e="T336" id="Seg_2851" s="T335">Kötʼsʼün_Güdʼər</ta>
            <ta e="T337" id="Seg_2852" s="T336">Kötʼsʼün_Güdʼər</ta>
            <ta e="T338" id="Seg_2853" s="T337">nüke-l</ta>
            <ta e="T339" id="Seg_2854" s="T338">tʼăgar-o-bi</ta>
            <ta e="T340" id="Seg_2855" s="T339">bazoʔ</ta>
            <ta e="T341" id="Seg_2856" s="T340">tĭrlö-leʔ</ta>
            <ta e="T342" id="Seg_2857" s="T341">tʼor-laʔbə</ta>
            <ta e="T343" id="Seg_2858" s="T342">šü-gəʔ</ta>
            <ta e="T344" id="Seg_2859" s="T343">săbəj</ta>
            <ta e="T345" id="Seg_2860" s="T344">i-lie-t</ta>
            <ta e="T346" id="Seg_2861" s="T345">dĭ</ta>
            <ta e="T347" id="Seg_2862" s="T346">bazoʔ</ta>
            <ta e="T348" id="Seg_2863" s="T347">šü-nə</ta>
            <ta e="T349" id="Seg_2864" s="T348">păʔ-laʔbə-lie</ta>
            <ta e="T350" id="Seg_2865" s="T349">i-ʔ</ta>
            <ta e="T351" id="Seg_2866" s="T350">tʼor-a-ʔ</ta>
            <ta e="T352" id="Seg_2867" s="T351">măn-də</ta>
            <ta e="T353" id="Seg_2868" s="T352">koʔbdo-m</ta>
            <ta e="T354" id="Seg_2869" s="T353">mĭ-li-m</ta>
            <ta e="T355" id="Seg_2870" s="T354">măna</ta>
            <ta e="T356" id="Seg_2871" s="T355">ej</ta>
            <ta e="T357" id="Seg_2872" s="T356">kereʔ</ta>
            <ta e="T358" id="Seg_2873" s="T357">tăn</ta>
            <ta e="T359" id="Seg_2874" s="T358">koʔbdo-l</ta>
            <ta e="T360" id="Seg_2875" s="T359">i-ʔ</ta>
            <ta e="T361" id="Seg_2876" s="T360">tʼor-a-ʔ</ta>
            <ta e="T362" id="Seg_2877" s="T361">šide</ta>
            <ta e="T363" id="Seg_2878" s="T362">šö-bə</ta>
            <ta e="T364" id="Seg_2879" s="T363">mĭ-li-m</ta>
            <ta e="T365" id="Seg_2880" s="T364">pʼüʔtö-bi</ta>
            <ta e="T366" id="Seg_2881" s="T365">nüke-zeŋ-də</ta>
            <ta e="T367" id="Seg_2882" s="T366">bokul-luʔ-bi</ta>
            <ta e="T368" id="Seg_2883" s="T367">kal-laʔ</ta>
            <ta e="T369" id="Seg_2884" s="T368">tʼür-bi</ta>
            <ta e="T370" id="Seg_2885" s="T369">büzʼe</ta>
            <ta e="T371" id="Seg_2886" s="T370">unʼə</ta>
            <ta e="T372" id="Seg_2887" s="T371">maʔ-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2888" s="T0">nüke</ta>
            <ta e="T2" id="Seg_2889" s="T1">amno-bi</ta>
            <ta e="T3" id="Seg_2890" s="T2">oʔb</ta>
            <ta e="T4" id="Seg_2891" s="T3">nʼi-t</ta>
            <ta e="T5" id="Seg_2892" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_2893" s="T5">nʼi-t</ta>
            <ta e="T7" id="Seg_2894" s="T6">ija-gəndə</ta>
            <ta e="T8" id="Seg_2895" s="T7">măn-liA</ta>
            <ta e="T9" id="Seg_2896" s="T8">măn</ta>
            <ta e="T10" id="Seg_2897" s="T9">ne-j-lV-m</ta>
            <ta e="T11" id="Seg_2898" s="T10">ija-t</ta>
            <ta e="T12" id="Seg_2899" s="T11">măn-liA</ta>
            <ta e="T13" id="Seg_2900" s="T12">naga</ta>
            <ta e="T14" id="Seg_2901" s="T13">manʼək-Kən</ta>
            <ta e="T15" id="Seg_2902" s="T14">ne</ta>
            <ta e="T16" id="Seg_2903" s="T15">naga</ta>
            <ta e="T17" id="Seg_2904" s="T16">măn</ta>
            <ta e="T18" id="Seg_2905" s="T17">kan-lV-m</ta>
            <ta e="T19" id="Seg_2906" s="T18">ne</ta>
            <ta e="T20" id="Seg_2907" s="T19">pi-lAʔ</ta>
            <ta e="T21" id="Seg_2908" s="T20">kan-lAʔ</ta>
            <ta e="T22" id="Seg_2909" s="T21">tʼür-bi</ta>
            <ta e="T23" id="Seg_2910" s="T22">ija-bə</ta>
            <ta e="T24" id="Seg_2911" s="T23">baʔ-laːm-bi</ta>
            <ta e="T25" id="Seg_2912" s="T24">kandə-gA</ta>
            <ta e="T26" id="Seg_2913" s="T25">kandə-gA</ta>
            <ta e="T27" id="Seg_2914" s="T26">tʼăga-n</ta>
            <ta e="T28" id="Seg_2915" s="T27">toʔ-gəndə</ta>
            <ta e="T29" id="Seg_2916" s="T28">maʔ</ta>
            <ta e="T30" id="Seg_2917" s="T29">nu-gA</ta>
            <ta e="T31" id="Seg_2918" s="T30">šü-bi</ta>
            <ta e="T32" id="Seg_2919" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_2920" s="T32">maʔ-Tə</ta>
            <ta e="T34" id="Seg_2921" s="T33">ne</ta>
            <ta e="T35" id="Seg_2922" s="T34">amnə</ta>
            <ta e="T36" id="Seg_2923" s="T35">numan üzə-bi</ta>
            <ta e="T37" id="Seg_2924" s="T36">uda-bə</ta>
            <ta e="T38" id="Seg_2925" s="T37">mĭ-bi</ta>
            <ta e="T39" id="Seg_2926" s="T38">amnə-bi</ta>
            <ta e="T40" id="Seg_2927" s="T39">šü-n</ta>
            <ta e="T41" id="Seg_2928" s="T40">toʔ-gəndə</ta>
            <ta e="T42" id="Seg_2929" s="T41">adʼa-m</ta>
            <ta e="T43" id="Seg_2930" s="T42">aspaʔ</ta>
            <ta e="T44" id="Seg_2931" s="T43">edə-ʔ</ta>
            <ta e="T45" id="Seg_2932" s="T44">uja</ta>
            <ta e="T46" id="Seg_2933" s="T45">pada-ʔ</ta>
            <ta e="T47" id="Seg_2934" s="T46">uja-m</ta>
            <ta e="T48" id="Seg_2935" s="T47">naga</ta>
            <ta e="T49" id="Seg_2936" s="T48">măn</ta>
            <ta e="T50" id="Seg_2937" s="T49">det-liA-m</ta>
            <ta e="T51" id="Seg_2938" s="T50">nʼiʔdə</ta>
            <ta e="T52" id="Seg_2939" s="T51">uʔbdə-bi</ta>
            <ta e="T53" id="Seg_2940" s="T52">tʼalaš</ta>
            <ta e="T54" id="Seg_2941" s="T53">šüškə</ta>
            <ta e="T55" id="Seg_2942" s="T54">i-bi</ta>
            <ta e="T56" id="Seg_2943" s="T55">püje-bə</ta>
            <ta e="T57" id="Seg_2944" s="T56">talə-j</ta>
            <ta e="T58" id="Seg_2945" s="T57">toʔbdə-bi</ta>
            <ta e="T59" id="Seg_2946" s="T58">kem-də</ta>
            <ta e="T60" id="Seg_2947" s="T59">mʼaŋ-luʔbdə-bi</ta>
            <ta e="T61" id="Seg_2948" s="T60">dĭ</ta>
            <ta e="T62" id="Seg_2949" s="T61">kem-ziʔ</ta>
            <ta e="T63" id="Seg_2950" s="T62">šüškə-m</ta>
            <ta e="T64" id="Seg_2951" s="T63">bar</ta>
            <ta e="T65" id="Seg_2952" s="T64">tʼuʔbdə-bi</ta>
            <ta e="T66" id="Seg_2953" s="T65">šü-bi</ta>
            <ta e="T67" id="Seg_2954" s="T66">maʔ-Tə</ta>
            <ta e="T68" id="Seg_2955" s="T67">dĭ</ta>
            <ta e="T69" id="Seg_2956" s="T68">šüškə-m</ta>
            <ta e="T70" id="Seg_2957" s="T69">aspaʔ-Tə</ta>
            <ta e="T71" id="Seg_2958" s="T70">padə-luʔbdə-bi</ta>
            <ta e="T72" id="Seg_2959" s="T71">amnə-jəʔ</ta>
            <ta e="T73" id="Seg_2960" s="T72">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T74" id="Seg_2961" s="T73">măn-liA</ta>
            <ta e="T75" id="Seg_2962" s="T74">adʼa-ŋ</ta>
            <ta e="T76" id="Seg_2963" s="T75">amor-žə-bəj</ta>
            <ta e="T77" id="Seg_2964" s="T76">dĭ</ta>
            <ta e="T78" id="Seg_2965" s="T77">ne</ta>
            <ta e="T79" id="Seg_2966" s="T78">aspaʔ-də-bə</ta>
            <ta e="T80" id="Seg_2967" s="T79">i-bi</ta>
            <ta e="T81" id="Seg_2968" s="T80">šü-ndə</ta>
            <ta e="T82" id="Seg_2969" s="T81">toʔ-gəndə</ta>
            <ta e="T83" id="Seg_2970" s="T82">nuldə-bi</ta>
            <ta e="T84" id="Seg_2971" s="T83">kiʔgo-bə</ta>
            <ta e="T85" id="Seg_2972" s="T84">i-bi</ta>
            <ta e="T86" id="Seg_2973" s="T85">uja-bə</ta>
            <ta e="T87" id="Seg_2974" s="T86">suʔbdə-laʔbə</ta>
            <ta e="T88" id="Seg_2975" s="T87">kur</ta>
            <ta e="T89" id="Seg_2976" s="T88">le</ta>
            <ta e="T90" id="Seg_2977" s="T89">aspaʔ-Kən</ta>
            <ta e="T91" id="Seg_2978" s="T90">koldər-laʔbə</ta>
            <ta e="T92" id="Seg_2979" s="T91">gijen</ta>
            <ta e="T93" id="Seg_2980" s="T92">tăn</ta>
            <ta e="T94" id="Seg_2981" s="T93">uja-l</ta>
            <ta e="T95" id="Seg_2982" s="T94">onʼiʔ</ta>
            <ta e="T96" id="Seg_2983" s="T95">tʼalaš</ta>
            <ta e="T97" id="Seg_2984" s="T96">le</ta>
            <ta e="T98" id="Seg_2985" s="T97">tĭrlö-lAʔ</ta>
            <ta e="T99" id="Seg_2986" s="T98">mĭn-gA</ta>
            <ta e="T100" id="Seg_2987" s="T99">Kötʼsʼün_Güdʼər</ta>
            <ta e="T101" id="Seg_2988" s="T100">gijen=de</ta>
            <ta e="T102" id="Seg_2989" s="T101">uja-l</ta>
            <ta e="T103" id="Seg_2990" s="T102">onʼiʔ</ta>
            <ta e="T104" id="Seg_2991" s="T103">le</ta>
            <ta e="T105" id="Seg_2992" s="T104">tĭrlö-lAʔ</ta>
            <ta e="T106" id="Seg_2993" s="T105">mĭn-gA</ta>
            <ta e="T107" id="Seg_2994" s="T106">Kötʼsʼün_Güdʼər</ta>
            <ta e="T108" id="Seg_2995" s="T107">tʼor-luʔbdə-bi</ta>
            <ta e="T109" id="Seg_2996" s="T108">šü-Tə</ta>
            <ta e="T110" id="Seg_2997" s="T109">tĭrlö-laʔbə-bi</ta>
            <ta e="T111" id="Seg_2998" s="T110">ne</ta>
            <ta e="T112" id="Seg_2999" s="T111">săbəj-laʔbə-bi</ta>
            <ta e="T113" id="Seg_3000" s="T112">kedʼi</ta>
            <ta e="T114" id="Seg_3001" s="T113">nʼeʔbdə-laʔbə-liA-t</ta>
            <ta e="T115" id="Seg_3002" s="T114">e-ʔ</ta>
            <ta e="T116" id="Seg_3003" s="T115">tʼor-ə-ʔ</ta>
            <ta e="T117" id="Seg_3004" s="T116">tăn</ta>
            <ta e="T118" id="Seg_3005" s="T117">aspaʔ-l</ta>
            <ta e="T119" id="Seg_3006" s="T118">koː-laːm-bi</ta>
            <ta e="T120" id="Seg_3007" s="T119">măn</ta>
            <ta e="T121" id="Seg_3008" s="T120">uja-m</ta>
            <ta e="T122" id="Seg_3009" s="T121">am-luʔbdə-bi</ta>
            <ta e="T123" id="Seg_3010" s="T122">ne</ta>
            <ta e="T124" id="Seg_3011" s="T123">e-ʔ</ta>
            <ta e="T125" id="Seg_3012" s="T124">tʼor-ə-ʔ</ta>
            <ta e="T126" id="Seg_3013" s="T125">mĭ-liA-m</ta>
            <ta e="T127" id="Seg_3014" s="T126">onʼiʔ</ta>
            <ta e="T128" id="Seg_3015" s="T127">ular-m</ta>
            <ta e="T129" id="Seg_3016" s="T128">i-gA</ta>
            <ta e="T130" id="Seg_3017" s="T129">dĭ-m</ta>
            <ta e="T131" id="Seg_3018" s="T130">mĭ-lV-m</ta>
            <ta e="T132" id="Seg_3019" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_3020" s="T132">piʔdö-bi</ta>
            <ta e="T134" id="Seg_3021" s="T133">ular-də</ta>
            <ta e="T135" id="Seg_3022" s="T134">bokuʔ-lAʔ</ta>
            <ta e="T136" id="Seg_3023" s="T135">i-bi</ta>
            <ta e="T137" id="Seg_3024" s="T136">kandə-gA</ta>
            <ta e="T138" id="Seg_3025" s="T137">maʔ</ta>
            <ta e="T139" id="Seg_3026" s="T138">nu-gA</ta>
            <ta e="T140" id="Seg_3027" s="T139">šü-bi</ta>
            <ta e="T141" id="Seg_3028" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_3029" s="T141">maʔ-Tə</ta>
            <ta e="T143" id="Seg_3030" s="T142">nüke</ta>
            <ta e="T144" id="Seg_3031" s="T143">büzʼe</ta>
            <ta e="T145" id="Seg_3032" s="T144">amnə-jəʔ</ta>
            <ta e="T146" id="Seg_3033" s="T145">uda-bə</ta>
            <ta e="T147" id="Seg_3034" s="T146">mĭ-bi</ta>
            <ta e="T148" id="Seg_3035" s="T147">măn-liA</ta>
            <ta e="T149" id="Seg_3036" s="T148">gibər</ta>
            <ta e="T150" id="Seg_3037" s="T149">kandə-gA-l</ta>
            <ta e="T151" id="Seg_3038" s="T150">tăn</ta>
            <ta e="T152" id="Seg_3039" s="T151">ĭmbənʼi-m</ta>
            <ta e="T153" id="Seg_3040" s="T152">ĭmbənʼi-gənʼi</ta>
            <ta e="T154" id="Seg_3041" s="T153">jada-j-lAʔ</ta>
            <ta e="T155" id="Seg_3042" s="T154">mĭn-bi-m</ta>
            <ta e="T156" id="Seg_3043" s="T155">ĭmbənʼi-m</ta>
            <ta e="T157" id="Seg_3044" s="T156">onʼiʔ</ta>
            <ta e="T158" id="Seg_3045" s="T157">ular</ta>
            <ta e="T159" id="Seg_3046" s="T158">mĭ-bi</ta>
            <ta e="T160" id="Seg_3047" s="T159">öʔ-lAʔ</ta>
            <ta e="T161" id="Seg_3048" s="T160">i-t</ta>
            <ta e="T162" id="Seg_3049" s="T161">miʔ</ta>
            <ta e="T163" id="Seg_3050" s="T162">ular-zAŋ-gənʼibAʔ</ta>
            <ta e="T164" id="Seg_3051" s="T163">öʔ-lAʔ</ta>
            <ta e="T165" id="Seg_3052" s="T164">i-t</ta>
            <ta e="T166" id="Seg_3053" s="T165">ular-l</ta>
            <ta e="T167" id="Seg_3054" s="T166">šiʔ</ta>
            <ta e="T168" id="Seg_3055" s="T167">ular-zAŋ-lAʔ</ta>
            <ta e="T169" id="Seg_3056" s="T168">măn</ta>
            <ta e="T170" id="Seg_3057" s="T169">ular-m</ta>
            <ta e="T171" id="Seg_3058" s="T170">am-luʔbdə-lV-dən</ta>
            <ta e="T172" id="Seg_3059" s="T171">ĭmbi-j-lAʔ</ta>
            <ta e="T173" id="Seg_3060" s="T172">am-lV-dən</ta>
            <ta e="T174" id="Seg_3061" s="T173">no</ta>
            <ta e="T175" id="Seg_3062" s="T174">öʔ-lAʔ</ta>
            <ta e="T176" id="Seg_3063" s="T175">i-t</ta>
            <ta e="T177" id="Seg_3064" s="T176">büzʼe</ta>
            <ta e="T178" id="Seg_3065" s="T177">kun-lAʔ</ta>
            <ta e="T179" id="Seg_3066" s="T178">öʔ-lAʔ</ta>
            <ta e="T180" id="Seg_3067" s="T179">i-bi</ta>
            <ta e="T181" id="Seg_3068" s="T180">pi-n</ta>
            <ta e="T182" id="Seg_3069" s="T181">uʔbdə-bi</ta>
            <ta e="T183" id="Seg_3070" s="T182">Kötʼsʼün_Güdʼər</ta>
            <ta e="T184" id="Seg_3071" s="T183">ular-də-bə</ta>
            <ta e="T185" id="Seg_3072" s="T184">bostə-bə</ta>
            <ta e="T186" id="Seg_3073" s="T185">talə-j</ta>
            <ta e="T187" id="Seg_3074" s="T186">nʼeʔbdə-bi</ta>
            <ta e="T188" id="Seg_3075" s="T187">talə-j</ta>
            <ta e="T189" id="Seg_3076" s="T188">nüzə-bi</ta>
            <ta e="T190" id="Seg_3077" s="T189">i-bi</ta>
            <ta e="T191" id="Seg_3078" s="T190">pʼel-j</ta>
            <ta e="T192" id="Seg_3079" s="T191">ular-jəʔ-n</ta>
            <ta e="T193" id="Seg_3080" s="T192">püje-zAŋ-də</ta>
            <ta e="T194" id="Seg_3081" s="T193">kem-ziʔ</ta>
            <ta e="T195" id="Seg_3082" s="T194">tʼuʔbdə-labaʔ-bi</ta>
            <ta e="T196" id="Seg_3083" s="T195">bos-də</ta>
            <ta e="T197" id="Seg_3084" s="T196">kunol-zittə</ta>
            <ta e="T198" id="Seg_3085" s="T197">iʔbə-bi</ta>
            <ta e="T199" id="Seg_3086" s="T198">ertə-n</ta>
            <ta e="T200" id="Seg_3087" s="T199">büzʼe</ta>
            <ta e="T201" id="Seg_3088" s="T200">nüke-t-ziʔ</ta>
            <ta e="T202" id="Seg_3089" s="T201">uʔbdə-bi</ta>
            <ta e="T203" id="Seg_3090" s="T202">büzʼe</ta>
            <ta e="T204" id="Seg_3091" s="T203">kan-bi</ta>
            <ta e="T205" id="Seg_3092" s="T204">šeden-də</ta>
            <ta e="T206" id="Seg_3093" s="T205">ku-bi-gəndə</ta>
            <ta e="T207" id="Seg_3094" s="T206">ular</ta>
            <ta e="T208" id="Seg_3095" s="T207">talə-j</ta>
            <ta e="T209" id="Seg_3096" s="T208">nʼeʔbdə-o-lAʔ</ta>
            <ta e="T210" id="Seg_3097" s="T209">nüke-gəndə</ta>
            <ta e="T211" id="Seg_3098" s="T210">šo-bizA</ta>
            <ta e="T212" id="Seg_3099" s="T211">nörbə-bi</ta>
            <ta e="T213" id="Seg_3100" s="T212">šen</ta>
            <ta e="T214" id="Seg_3101" s="T213">ĭmbi-lAʔ</ta>
            <ta e="T215" id="Seg_3102" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_3103" s="T215">nʼi-n</ta>
            <ta e="T217" id="Seg_3104" s="T216">ular-də</ta>
            <ta e="T218" id="Seg_3105" s="T217">miʔ</ta>
            <ta e="T219" id="Seg_3106" s="T218">ular-zAŋ-bAʔ</ta>
            <ta e="T220" id="Seg_3107" s="T219">talə-j</ta>
            <ta e="T221" id="Seg_3108" s="T220">nüzə-bi</ta>
            <ta e="T222" id="Seg_3109" s="T221">Kötʼsʼün_Güdʼər</ta>
            <ta e="T223" id="Seg_3110" s="T222">nünə-lAʔ</ta>
            <ta e="T224" id="Seg_3111" s="T223">baʔbdə-bi</ta>
            <ta e="T225" id="Seg_3112" s="T224">uʔbdə-lAʔ</ta>
            <ta e="T226" id="Seg_3113" s="T225">saʔmə-bi</ta>
            <ta e="T227" id="Seg_3114" s="T226">măn-liA</ta>
            <ta e="T228" id="Seg_3115" s="T227">măn</ta>
            <ta e="T229" id="Seg_3116" s="T228">nörbə-bi-m</ta>
            <ta e="T230" id="Seg_3117" s="T229">šĭʔnʼidə</ta>
            <ta e="T231" id="Seg_3118" s="T230">ular-m</ta>
            <ta e="T232" id="Seg_3119" s="T231">am-lV-dən</ta>
            <ta e="T233" id="Seg_3120" s="T232">šen</ta>
            <ta e="T234" id="Seg_3121" s="T233">ĭmbi-lAʔ</ta>
            <ta e="T235" id="Seg_3122" s="T234">am-lAʔ</ta>
            <ta e="T236" id="Seg_3123" s="T235">kun-bi-jəʔ</ta>
            <ta e="T237" id="Seg_3124" s="T236">bazoʔ</ta>
            <ta e="T238" id="Seg_3125" s="T237">tʼor-lAʔ</ta>
            <ta e="T239" id="Seg_3126" s="T238">tĭrlö-lAʔ</ta>
            <ta e="T240" id="Seg_3127" s="T239">kojo-bi</ta>
            <ta e="T241" id="Seg_3128" s="T240">büzʼe</ta>
            <ta e="T242" id="Seg_3129" s="T241">šü-gəʔ</ta>
            <ta e="T243" id="Seg_3130" s="T242">săbəj-laʔbdə-t</ta>
            <ta e="T244" id="Seg_3131" s="T243">dĭ</ta>
            <ta e="T245" id="Seg_3132" s="T244">bazoʔ</ta>
            <ta e="T246" id="Seg_3133" s="T245">šü-Tə</ta>
            <ta e="T247" id="Seg_3134" s="T246">tĭrlö-liA</ta>
            <ta e="T248" id="Seg_3135" s="T247">e-ʔ</ta>
            <ta e="T249" id="Seg_3136" s="T248">tʼor-ə-ʔ</ta>
            <ta e="T250" id="Seg_3137" s="T249">măn</ta>
            <ta e="T251" id="Seg_3138" s="T250">ular-m</ta>
            <ta e="T252" id="Seg_3139" s="T251">mĭ-lV-m</ta>
            <ta e="T253" id="Seg_3140" s="T252">măna</ta>
            <ta e="T254" id="Seg_3141" s="T253">ej</ta>
            <ta e="T255" id="Seg_3142" s="T254">kereʔ</ta>
            <ta e="T256" id="Seg_3143" s="T255">tăn</ta>
            <ta e="T257" id="Seg_3144" s="T256">ular-lAʔ</ta>
            <ta e="T258" id="Seg_3145" s="T257">măn</ta>
            <ta e="T259" id="Seg_3146" s="T258">ĭmbənʼi-m</ta>
            <ta e="T260" id="Seg_3147" s="T259">măna</ta>
            <ta e="T261" id="Seg_3148" s="T260">mĭ-bi</ta>
            <ta e="T262" id="Seg_3149" s="T261">no</ta>
            <ta e="T263" id="Seg_3150" s="T262">e-ʔ</ta>
            <ta e="T264" id="Seg_3151" s="T263">tʼor-ə-ʔ</ta>
            <ta e="T265" id="Seg_3152" s="T264">bar</ta>
            <ta e="T266" id="Seg_3153" s="T265">mĭ-lV-m</ta>
            <ta e="T267" id="Seg_3154" s="T266">sumna-m</ta>
            <ta e="T268" id="Seg_3155" s="T267">piʔdö-bi</ta>
            <ta e="T269" id="Seg_3156" s="T268">ular-zAŋ-də</ta>
            <ta e="T270" id="Seg_3157" s="T269">bokuʔ-lei-bi</ta>
            <ta e="T271" id="Seg_3158" s="T270">kandə-gA</ta>
            <ta e="T272" id="Seg_3159" s="T271">küres</ta>
            <ta e="T273" id="Seg_3160" s="T272">pa</ta>
            <ta e="T274" id="Seg_3161" s="T273">nu-gA</ta>
            <ta e="T275" id="Seg_3162" s="T274">dĭ-m</ta>
            <ta e="T276" id="Seg_3163" s="T275">Kötʼsʼün_Güdʼər</ta>
            <ta e="T277" id="Seg_3164" s="T276">tĭl-luʔbdə-bi</ta>
            <ta e="T278" id="Seg_3165" s="T277">ne</ta>
            <ta e="T279" id="Seg_3166" s="T278">tʼaktə</ta>
            <ta e="T280" id="Seg_3167" s="T279">săbəj-lei-bi</ta>
            <ta e="T281" id="Seg_3168" s="T280">manžə-lei-bi</ta>
            <ta e="T282" id="Seg_3169" s="T281">bazoʔ</ta>
            <ta e="T283" id="Seg_3170" s="T282">mĭn-laːndə-bi</ta>
            <ta e="T284" id="Seg_3171" s="T283">tʼütʼe</ta>
            <ta e="T285" id="Seg_3172" s="T284">kandə-bi</ta>
            <ta e="T286" id="Seg_3173" s="T285">maʔ</ta>
            <ta e="T287" id="Seg_3174" s="T286">nu-gA</ta>
            <ta e="T288" id="Seg_3175" s="T287">amnol-bi</ta>
            <ta e="T289" id="Seg_3176" s="T288">kü-NTA</ta>
            <ta e="T290" id="Seg_3177" s="T289">ne-m</ta>
            <ta e="T291" id="Seg_3178" s="T290">pa-n</ta>
            <ta e="T292" id="Seg_3179" s="T291">tabə-gəndə</ta>
            <ta e="T293" id="Seg_3180" s="T292">ular-jəʔ-m</ta>
            <ta e="T294" id="Seg_3181" s="T293">uda-gəndə</ta>
            <ta e="T295" id="Seg_3182" s="T294">sar-bi</ta>
            <ta e="T296" id="Seg_3183" s="T295">tagaj-də</ta>
            <ta e="T297" id="Seg_3184" s="T296">sĭj-gəndə</ta>
            <ta e="T298" id="Seg_3185" s="T297">müʔbdə-bi</ta>
            <ta e="T299" id="Seg_3186" s="T298">bos-də</ta>
            <ta e="T300" id="Seg_3187" s="T299">kan-bi</ta>
            <ta e="T301" id="Seg_3188" s="T300">maʔ-Tə</ta>
            <ta e="T302" id="Seg_3189" s="T301">büzʼe</ta>
            <ta e="T303" id="Seg_3190" s="T302">amnə</ta>
            <ta e="T304" id="Seg_3191" s="T303">šide</ta>
            <ta e="T305" id="Seg_3192" s="T304">koʔbdo-t</ta>
            <ta e="T306" id="Seg_3193" s="T305">amnə</ta>
            <ta e="T307" id="Seg_3194" s="T306">amor-žə-bAʔ</ta>
            <ta e="T308" id="Seg_3195" s="T307">măn</ta>
            <ta e="T309" id="Seg_3196" s="T308">nüke-m</ta>
            <ta e="T310" id="Seg_3197" s="T309">i-gA</ta>
            <ta e="T311" id="Seg_3198" s="T310">pa-n</ta>
            <ta e="T312" id="Seg_3199" s="T311">tabə-gəndə</ta>
            <ta e="T313" id="Seg_3200" s="T312">ular-zAŋ-də</ta>
            <ta e="T314" id="Seg_3201" s="T313">tʼabo-lamnə-t</ta>
            <ta e="T315" id="Seg_3202" s="T314">koʔbdo-zAŋ</ta>
            <ta e="T316" id="Seg_3203" s="T315">kan-lAʔ</ta>
            <ta e="T317" id="Seg_3204" s="T316">kăštə-l-bAʔ</ta>
            <ta e="T318" id="Seg_3205" s="T317">e-KAʔ</ta>
            <ta e="T319" id="Seg_3206" s="T318">kan-KAʔ</ta>
            <ta e="T320" id="Seg_3207" s="T319">nereʔ-lV-lAʔ</ta>
            <ta e="T321" id="Seg_3208" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_3209" s="T321">nerö-bi-gəndə</ta>
            <ta e="T323" id="Seg_3210" s="T322">bospostə-bə</ta>
            <ta e="T324" id="Seg_3211" s="T323">tʼăgar-l-t</ta>
            <ta e="T325" id="Seg_3212" s="T324">koʔbdo-zAŋ</ta>
            <ta e="T326" id="Seg_3213" s="T325">üʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T327" id="Seg_3214" s="T326">ular-jəʔ</ta>
            <ta e="T328" id="Seg_3215" s="T327">nerö-bi-jəʔ</ta>
            <ta e="T329" id="Seg_3216" s="T328">tuno-loʔbdə-bi</ta>
            <ta e="T330" id="Seg_3217" s="T329">ku-bi-gəndən</ta>
            <ta e="T331" id="Seg_3218" s="T330">tagaj</ta>
            <ta e="T332" id="Seg_3219" s="T331">sĭj-gəndə</ta>
            <ta e="T333" id="Seg_3220" s="T332">müʔbdə-o-laʔbə</ta>
            <ta e="T334" id="Seg_3221" s="T333">üʔmə-lAʔ</ta>
            <ta e="T335" id="Seg_3222" s="T334">šo-bi-jəʔ</ta>
            <ta e="T336" id="Seg_3223" s="T335">Kötʼsʼün_Güdʼər</ta>
            <ta e="T337" id="Seg_3224" s="T336">Kötʼsʼün_Güdʼər</ta>
            <ta e="T338" id="Seg_3225" s="T337">nüke-l</ta>
            <ta e="T339" id="Seg_3226" s="T338">tʼăgar-o-bi</ta>
            <ta e="T340" id="Seg_3227" s="T339">bazoʔ</ta>
            <ta e="T341" id="Seg_3228" s="T340">tĭrlö-lAʔ</ta>
            <ta e="T342" id="Seg_3229" s="T341">tʼor-laʔbə</ta>
            <ta e="T343" id="Seg_3230" s="T342">šü-gəʔ</ta>
            <ta e="T344" id="Seg_3231" s="T343">săbəj</ta>
            <ta e="T345" id="Seg_3232" s="T344">i-liA-t</ta>
            <ta e="T346" id="Seg_3233" s="T345">dĭ</ta>
            <ta e="T347" id="Seg_3234" s="T346">bazoʔ</ta>
            <ta e="T348" id="Seg_3235" s="T347">šü-Tə</ta>
            <ta e="T349" id="Seg_3236" s="T348">păda-laʔbə-liA</ta>
            <ta e="T350" id="Seg_3237" s="T349">e-ʔ</ta>
            <ta e="T351" id="Seg_3238" s="T350">tʼor-ə-ʔ</ta>
            <ta e="T352" id="Seg_3239" s="T351">măn-ntə</ta>
            <ta e="T353" id="Seg_3240" s="T352">koʔbdo-m</ta>
            <ta e="T354" id="Seg_3241" s="T353">mĭ-lV-m</ta>
            <ta e="T355" id="Seg_3242" s="T354">măna</ta>
            <ta e="T356" id="Seg_3243" s="T355">ej</ta>
            <ta e="T357" id="Seg_3244" s="T356">kereʔ</ta>
            <ta e="T358" id="Seg_3245" s="T357">tăn</ta>
            <ta e="T359" id="Seg_3246" s="T358">koʔbdo-l</ta>
            <ta e="T360" id="Seg_3247" s="T359">e-ʔ</ta>
            <ta e="T361" id="Seg_3248" s="T360">tʼor-ə-ʔ</ta>
            <ta e="T362" id="Seg_3249" s="T361">šide</ta>
            <ta e="T363" id="Seg_3250" s="T362">šö-m</ta>
            <ta e="T364" id="Seg_3251" s="T363">mĭ-lV-m</ta>
            <ta e="T365" id="Seg_3252" s="T364">piʔdö-bi</ta>
            <ta e="T366" id="Seg_3253" s="T365">nüke-zAŋ-də</ta>
            <ta e="T367" id="Seg_3254" s="T366">bokuʔ-luʔbdə-bi</ta>
            <ta e="T368" id="Seg_3255" s="T367">kan-lAʔ</ta>
            <ta e="T369" id="Seg_3256" s="T368">tʼür-bi</ta>
            <ta e="T370" id="Seg_3257" s="T369">büzʼe</ta>
            <ta e="T371" id="Seg_3258" s="T370">unʼə</ta>
            <ta e="T372" id="Seg_3259" s="T371">ma-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3260" s="T0">woman.[NOM.SG]</ta>
            <ta e="T2" id="Seg_3261" s="T1">live-PST.[3SG]</ta>
            <ta e="T3" id="Seg_3262" s="T2">one.[NOM.SG]</ta>
            <ta e="T4" id="Seg_3263" s="T3">son-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_3264" s="T4">be-PST.[3SG]</ta>
            <ta e="T6" id="Seg_3265" s="T5">son-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_3266" s="T6">mother-LAT/LOC.3SG</ta>
            <ta e="T8" id="Seg_3267" s="T7">say-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_3268" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_3269" s="T9">woman-VBLZ-FUT-1SG</ta>
            <ta e="T11" id="Seg_3270" s="T10">mother-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_3271" s="T11">say-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_3272" s="T12">NEG.EX</ta>
            <ta e="T14" id="Seg_3273" s="T13">vicinity-LOC</ta>
            <ta e="T15" id="Seg_3274" s="T14">woman.[NOM.SG]</ta>
            <ta e="T16" id="Seg_3275" s="T15">NEG.EX.[3SG]</ta>
            <ta e="T17" id="Seg_3276" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_3277" s="T17">go-FUT-1SG</ta>
            <ta e="T19" id="Seg_3278" s="T18">woman.[NOM.SG]</ta>
            <ta e="T20" id="Seg_3279" s="T19">look.for-CVB</ta>
            <ta e="T21" id="Seg_3280" s="T20">go-CVB</ta>
            <ta e="T22" id="Seg_3281" s="T21">disappear-PST.[3SG]</ta>
            <ta e="T23" id="Seg_3282" s="T22">mother-ACC.3SG</ta>
            <ta e="T24" id="Seg_3283" s="T23">leave-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_3284" s="T24">walk-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_3285" s="T25">walk-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_3286" s="T26">river-GEN</ta>
            <ta e="T28" id="Seg_3287" s="T27">edge-LAT/LOC.3SG</ta>
            <ta e="T29" id="Seg_3288" s="T28">tent.[NOM.SG]</ta>
            <ta e="T30" id="Seg_3289" s="T29">stand-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_3290" s="T30">enter-PST.[3SG]</ta>
            <ta e="T32" id="Seg_3291" s="T31">this.[NOM.SG]</ta>
            <ta e="T33" id="Seg_3292" s="T32">tent-LAT</ta>
            <ta e="T34" id="Seg_3293" s="T33">woman.[NOM.SG]</ta>
            <ta e="T35" id="Seg_3294" s="T34">live.[3SG]</ta>
            <ta e="T36" id="Seg_3295" s="T35">bow-PST.[3SG]</ta>
            <ta e="T37" id="Seg_3296" s="T36">hand-ACC.3SG</ta>
            <ta e="T38" id="Seg_3297" s="T37">give-PST.[3SG]</ta>
            <ta e="T39" id="Seg_3298" s="T38">sit.down-PST.[3SG]</ta>
            <ta e="T40" id="Seg_3299" s="T39">fire-GEN</ta>
            <ta e="T41" id="Seg_3300" s="T40">edge-LAT/LOC.3SG</ta>
            <ta e="T42" id="Seg_3301" s="T41">aunt-NOM/GEN/ACC.1SG</ta>
            <ta e="T43" id="Seg_3302" s="T42">cauldron.[NOM.SG]</ta>
            <ta e="T44" id="Seg_3303" s="T43">hang.up-IMP.2SG</ta>
            <ta e="T45" id="Seg_3304" s="T44">meat.[NOM.SG]</ta>
            <ta e="T46" id="Seg_3305" s="T45">cook-IMP.2SG</ta>
            <ta e="T47" id="Seg_3306" s="T46">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T48" id="Seg_3307" s="T47">NEG.EX.[3SG]</ta>
            <ta e="T49" id="Seg_3308" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_3309" s="T49">bring-PRS-1SG</ta>
            <ta e="T51" id="Seg_3310" s="T50">outwards</ta>
            <ta e="T52" id="Seg_3311" s="T51">get.up-PST.[3SG]</ta>
            <ta e="T53" id="Seg_3312" s="T52">bare.[NOM.SG]</ta>
            <ta e="T54" id="Seg_3313" s="T53">blade.bone.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3314" s="T54">take-PST.[3SG]</ta>
            <ta e="T56" id="Seg_3315" s="T55">nose-ACC.3SG</ta>
            <ta e="T57" id="Seg_3316" s="T56">tear.apart-CVB</ta>
            <ta e="T58" id="Seg_3317" s="T57">hit-PST.[3SG]</ta>
            <ta e="T59" id="Seg_3318" s="T58">blood-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_3319" s="T59">flow-MOM-PST.[3SG]</ta>
            <ta e="T61" id="Seg_3320" s="T60">this.[NOM.SG]</ta>
            <ta e="T62" id="Seg_3321" s="T61">blood-INS</ta>
            <ta e="T63" id="Seg_3322" s="T62">blade.bone-ACC</ta>
            <ta e="T64" id="Seg_3323" s="T63">all</ta>
            <ta e="T65" id="Seg_3324" s="T64">smear-PST.[3SG]</ta>
            <ta e="T66" id="Seg_3325" s="T65">enter-PST.[3SG]</ta>
            <ta e="T67" id="Seg_3326" s="T66">tent-LAT</ta>
            <ta e="T68" id="Seg_3327" s="T67">this.[NOM.SG]</ta>
            <ta e="T69" id="Seg_3328" s="T68">blade.bone-ACC</ta>
            <ta e="T70" id="Seg_3329" s="T69">cauldron-LAT</ta>
            <ta e="T71" id="Seg_3330" s="T70">put-MOM-PST.[3SG]</ta>
            <ta e="T72" id="Seg_3331" s="T71">sit-3PL</ta>
            <ta e="T73" id="Seg_3332" s="T72">speak-DUR-3PL</ta>
            <ta e="T74" id="Seg_3333" s="T73">say-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_3334" s="T74">aunt-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_3335" s="T75">eat-OPT.DU/PL-1DU</ta>
            <ta e="T77" id="Seg_3336" s="T76">this.[NOM.SG]</ta>
            <ta e="T78" id="Seg_3337" s="T77">woman.[NOM.SG]</ta>
            <ta e="T79" id="Seg_3338" s="T78">cauldron-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T80" id="Seg_3339" s="T79">take-PST.[3SG]</ta>
            <ta e="T81" id="Seg_3340" s="T80">fire-GEN.3SG</ta>
            <ta e="T82" id="Seg_3341" s="T81">edge-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_3342" s="T82">place-PST.[3SG]</ta>
            <ta e="T84" id="Seg_3343" s="T83">ladle-ACC.3SG</ta>
            <ta e="T85" id="Seg_3344" s="T84">take-PST.[3SG]</ta>
            <ta e="T86" id="Seg_3345" s="T85">meat-ACC.3SG</ta>
            <ta e="T87" id="Seg_3346" s="T86">scoop-DUR.[3SG]</ta>
            <ta e="T88" id="Seg_3347" s="T87">just</ta>
            <ta e="T89" id="Seg_3348" s="T88">bone.[NOM.SG]</ta>
            <ta e="T90" id="Seg_3349" s="T89">cauldron-LOC</ta>
            <ta e="T91" id="Seg_3350" s="T90">clatter-DUR.[3SG]</ta>
            <ta e="T92" id="Seg_3351" s="T91">where</ta>
            <ta e="T93" id="Seg_3352" s="T92">you.GEN</ta>
            <ta e="T94" id="Seg_3353" s="T93">meat-NOM/GEN/ACC.2SG</ta>
            <ta e="T95" id="Seg_3354" s="T94">single.[NOM.SG]</ta>
            <ta e="T96" id="Seg_3355" s="T95">bare.[NOM.SG]</ta>
            <ta e="T97" id="Seg_3356" s="T96">bone.[NOM.SG]</ta>
            <ta e="T98" id="Seg_3357" s="T97">roll-CVB</ta>
            <ta e="T99" id="Seg_3358" s="T98">go-PRS</ta>
            <ta e="T100" id="Seg_3359" s="T99">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T101" id="Seg_3360" s="T100">where=PTCL</ta>
            <ta e="T102" id="Seg_3361" s="T101">meat-NOM/GEN/ACC.2SG</ta>
            <ta e="T103" id="Seg_3362" s="T102">single</ta>
            <ta e="T104" id="Seg_3363" s="T103">bone.[NOM.SG]</ta>
            <ta e="T105" id="Seg_3364" s="T104">roll-CVB</ta>
            <ta e="T106" id="Seg_3365" s="T105">go-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_3366" s="T106">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T108" id="Seg_3367" s="T107">cry-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_3368" s="T108">fire-LAT</ta>
            <ta e="T110" id="Seg_3369" s="T109">roll-DUR-PST.[3SG]</ta>
            <ta e="T111" id="Seg_3370" s="T110">woman.[NOM.SG]</ta>
            <ta e="T112" id="Seg_3371" s="T111">take.out-DUR-PST.[3SG]</ta>
            <ta e="T113" id="Seg_3372" s="T112">away</ta>
            <ta e="T114" id="Seg_3373" s="T113">pull-DUR-PRS-3SG.O</ta>
            <ta e="T115" id="Seg_3374" s="T114">NEG.AUX-IMP.2SG</ta>
            <ta e="T116" id="Seg_3375" s="T115">cry-EP-CNG</ta>
            <ta e="T117" id="Seg_3376" s="T116">you.GEN</ta>
            <ta e="T118" id="Seg_3377" s="T117">cauldron-NOM/GEN/ACC.2SG</ta>
            <ta e="T119" id="Seg_3378" s="T118">become.dry-RES-PST.[3SG]</ta>
            <ta e="T120" id="Seg_3379" s="T119">I.GEN</ta>
            <ta e="T121" id="Seg_3380" s="T120">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T122" id="Seg_3381" s="T121">eat-MOM-PST.[3SG]</ta>
            <ta e="T123" id="Seg_3382" s="T122">woman.[NOM.SG]</ta>
            <ta e="T124" id="Seg_3383" s="T123">NEG.AUX-IMP.2SG</ta>
            <ta e="T125" id="Seg_3384" s="T124">cry-EP-CNG</ta>
            <ta e="T126" id="Seg_3385" s="T125">give-PRS-1SG</ta>
            <ta e="T127" id="Seg_3386" s="T126">single.[NOM.SG]</ta>
            <ta e="T128" id="Seg_3387" s="T127">sheep-NOM/GEN/ACC.1SG</ta>
            <ta e="T129" id="Seg_3388" s="T128">be-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_3389" s="T129">this-ACC</ta>
            <ta e="T131" id="Seg_3390" s="T130">give-FUT-1SG</ta>
            <ta e="T132" id="Seg_3391" s="T131">then</ta>
            <ta e="T133" id="Seg_3392" s="T132">wash.oneself-PST.[3SG]</ta>
            <ta e="T134" id="Seg_3393" s="T133">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T135" id="Seg_3394" s="T134">tie.up-CVB</ta>
            <ta e="T136" id="Seg_3395" s="T135">take-PST.[3SG]</ta>
            <ta e="T137" id="Seg_3396" s="T136">walk-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_3397" s="T137">tent.[NOM.SG]</ta>
            <ta e="T139" id="Seg_3398" s="T138">stand-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3399" s="T139">enter-PST.[3SG]</ta>
            <ta e="T141" id="Seg_3400" s="T140">this.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3401" s="T141">tent-LAT</ta>
            <ta e="T143" id="Seg_3402" s="T142">woman.[NOM.SG]</ta>
            <ta e="T144" id="Seg_3403" s="T143">man.[NOM.SG]</ta>
            <ta e="T145" id="Seg_3404" s="T144">sit-3PL</ta>
            <ta e="T146" id="Seg_3405" s="T145">hand-ACC.3SG</ta>
            <ta e="T147" id="Seg_3406" s="T146">give-PST.[3SG]</ta>
            <ta e="T148" id="Seg_3407" s="T147">say-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_3408" s="T148">where.to</ta>
            <ta e="T150" id="Seg_3409" s="T149">walk-PRS-2SG</ta>
            <ta e="T151" id="Seg_3410" s="T150">you.NOM</ta>
            <ta e="T152" id="Seg_3411" s="T151">mother.in.law-NOM/GEN/ACC.1SG</ta>
            <ta e="T153" id="Seg_3412" s="T152">mother.in.law-LAT/LOC.1SG</ta>
            <ta e="T154" id="Seg_3413" s="T153">village-VBLZ-CVB</ta>
            <ta e="T155" id="Seg_3414" s="T154">go-PST-1SG</ta>
            <ta e="T156" id="Seg_3415" s="T155">mother.in.law-NOM/GEN/ACC.1SG</ta>
            <ta e="T157" id="Seg_3416" s="T156">single.[NOM.SG]</ta>
            <ta e="T158" id="Seg_3417" s="T157">sheep.[NOM.SG]</ta>
            <ta e="T159" id="Seg_3418" s="T158">give-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3419" s="T159">let-CVB</ta>
            <ta e="T161" id="Seg_3420" s="T160">take-IMP.2SG.O</ta>
            <ta e="T162" id="Seg_3421" s="T161">we.GEN</ta>
            <ta e="T163" id="Seg_3422" s="T162">sheep-PL-LAT/LOC.1PL</ta>
            <ta e="T164" id="Seg_3423" s="T163">let-CVB</ta>
            <ta e="T165" id="Seg_3424" s="T164">take-IMP.2SG.O</ta>
            <ta e="T166" id="Seg_3425" s="T165">sheep-NOM/GEN/ACC.2SG</ta>
            <ta e="T167" id="Seg_3426" s="T166">you.PL.GEN</ta>
            <ta e="T168" id="Seg_3427" s="T167">sheep-PL-NOM/GEN/ACC.2PL</ta>
            <ta e="T169" id="Seg_3428" s="T168">I.GEN</ta>
            <ta e="T170" id="Seg_3429" s="T169">sheep-NOM/GEN/ACC.1SG</ta>
            <ta e="T171" id="Seg_3430" s="T170">eat-MOM-FUT-3PL.O</ta>
            <ta e="T172" id="Seg_3431" s="T171">what-VBLZ-CVB</ta>
            <ta e="T173" id="Seg_3432" s="T172">eat-FUT-3PL.O</ta>
            <ta e="T174" id="Seg_3433" s="T173">well</ta>
            <ta e="T175" id="Seg_3434" s="T174">let-CVB</ta>
            <ta e="T176" id="Seg_3435" s="T175">take-IMP.2SG.O</ta>
            <ta e="T177" id="Seg_3436" s="T176">man.[NOM.SG]</ta>
            <ta e="T178" id="Seg_3437" s="T177">bring-CVB</ta>
            <ta e="T179" id="Seg_3438" s="T178">let-CVB</ta>
            <ta e="T180" id="Seg_3439" s="T179">take-PST.[3SG]</ta>
            <ta e="T181" id="Seg_3440" s="T180">night-LOC.ADV</ta>
            <ta e="T182" id="Seg_3441" s="T181">get.up-PST.[3SG]</ta>
            <ta e="T183" id="Seg_3442" s="T182">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T184" id="Seg_3443" s="T183">sheep-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T185" id="Seg_3444" s="T184">self-ACC.3SG</ta>
            <ta e="T186" id="Seg_3445" s="T185">tear.apart-CVB</ta>
            <ta e="T187" id="Seg_3446" s="T186">pull-PST.[3SG]</ta>
            <ta e="T188" id="Seg_3447" s="T187">tear.apart-CVB</ta>
            <ta e="T189" id="Seg_3448" s="T188">tear-PST.[3SG]</ta>
            <ta e="T190" id="Seg_3449" s="T189">take-PST.[3SG]</ta>
            <ta e="T191" id="Seg_3450" s="T190">half-ADJZ</ta>
            <ta e="T192" id="Seg_3451" s="T191">sheep-PL-GEN</ta>
            <ta e="T193" id="Seg_3452" s="T192">nose-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T194" id="Seg_3453" s="T193">blood-INS</ta>
            <ta e="T195" id="Seg_3454" s="T194">smear-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_3455" s="T195">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T197" id="Seg_3456" s="T196">sleep-INF.LAT</ta>
            <ta e="T198" id="Seg_3457" s="T197">lie.down-PST.[3SG]</ta>
            <ta e="T199" id="Seg_3458" s="T198">morning-LOC.ADV</ta>
            <ta e="T200" id="Seg_3459" s="T199">man.[NOM.SG]</ta>
            <ta e="T201" id="Seg_3460" s="T200">woman-3SG-INS</ta>
            <ta e="T202" id="Seg_3461" s="T201">get.up-PST.[3SG]</ta>
            <ta e="T203" id="Seg_3462" s="T202">man.[NOM.SG]</ta>
            <ta e="T204" id="Seg_3463" s="T203">go-PST.[3SG]</ta>
            <ta e="T205" id="Seg_3464" s="T204">corral-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_3465" s="T205">see-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T207" id="Seg_3466" s="T206">sheep.[NOM.SG]</ta>
            <ta e="T208" id="Seg_3467" s="T207">tear.apart-CVB</ta>
            <ta e="T209" id="Seg_3468" s="T208">pull-DETR-CVB</ta>
            <ta e="T210" id="Seg_3469" s="T209">woman-LAT/LOC.3SG</ta>
            <ta e="T211" id="Seg_3470" s="T210">come-CVB.ANT</ta>
            <ta e="T212" id="Seg_3471" s="T211">tell-PST.[3SG]</ta>
            <ta e="T213" id="Seg_3472" s="T212">really</ta>
            <ta e="T214" id="Seg_3473" s="T213">what-NOM/GEN/ACC.2PL</ta>
            <ta e="T215" id="Seg_3474" s="T214">this.[NOM.SG]</ta>
            <ta e="T216" id="Seg_3475" s="T215">boy-GEN</ta>
            <ta e="T217" id="Seg_3476" s="T216">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T218" id="Seg_3477" s="T217">we.GEN</ta>
            <ta e="T219" id="Seg_3478" s="T218">sheep-PL-NOM/GEN/ACC.1PL</ta>
            <ta e="T220" id="Seg_3479" s="T219">tear.apart-CVB</ta>
            <ta e="T221" id="Seg_3480" s="T220">tear-PST.[3SG]</ta>
            <ta e="T222" id="Seg_3481" s="T221">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T223" id="Seg_3482" s="T222">hear-CVB</ta>
            <ta e="T224" id="Seg_3483" s="T223">throw-PST.[3SG]</ta>
            <ta e="T225" id="Seg_3484" s="T224">get.up-CVB</ta>
            <ta e="T226" id="Seg_3485" s="T225">collapse-PST.[3SG]</ta>
            <ta e="T227" id="Seg_3486" s="T226">say-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_3487" s="T227">I.NOM</ta>
            <ta e="T229" id="Seg_3488" s="T228">tell-PST-1SG</ta>
            <ta e="T230" id="Seg_3489" s="T229">you.PL.LAT</ta>
            <ta e="T231" id="Seg_3490" s="T230">sheep-NOM/GEN/ACC.1SG</ta>
            <ta e="T232" id="Seg_3491" s="T231">eat-FUT-3PL.O</ta>
            <ta e="T233" id="Seg_3492" s="T232">really</ta>
            <ta e="T234" id="Seg_3493" s="T233">what-NOM/GEN/ACC.2PL</ta>
            <ta e="T235" id="Seg_3494" s="T234">eat-CVB</ta>
            <ta e="T236" id="Seg_3495" s="T235">bring-PST-3PL</ta>
            <ta e="T237" id="Seg_3496" s="T236">again</ta>
            <ta e="T238" id="Seg_3497" s="T237">cry-CVB</ta>
            <ta e="T239" id="Seg_3498" s="T238">roll-CVB</ta>
            <ta e="T240" id="Seg_3499" s="T239">stay-PST.[3SG]</ta>
            <ta e="T241" id="Seg_3500" s="T240">man.[NOM.SG]</ta>
            <ta e="T242" id="Seg_3501" s="T241">fire-ABL</ta>
            <ta e="T243" id="Seg_3502" s="T242">take.out-TR.RES-3SG.O</ta>
            <ta e="T244" id="Seg_3503" s="T243">this.[NOM.SG]</ta>
            <ta e="T245" id="Seg_3504" s="T244">again</ta>
            <ta e="T246" id="Seg_3505" s="T245">fire-LAT</ta>
            <ta e="T247" id="Seg_3506" s="T246">roll-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_3507" s="T247">NEG.AUX-IMP.2SG</ta>
            <ta e="T249" id="Seg_3508" s="T248">cry-EP-CNG</ta>
            <ta e="T250" id="Seg_3509" s="T249">I.NOM</ta>
            <ta e="T251" id="Seg_3510" s="T250">sheep-ACC</ta>
            <ta e="T252" id="Seg_3511" s="T251">give-FUT-1SG</ta>
            <ta e="T253" id="Seg_3512" s="T252">I.LAT</ta>
            <ta e="T254" id="Seg_3513" s="T253">NEG</ta>
            <ta e="T255" id="Seg_3514" s="T254">one.needs</ta>
            <ta e="T256" id="Seg_3515" s="T255">you.GEN</ta>
            <ta e="T257" id="Seg_3516" s="T256">sheep-NOM/GEN/ACC.2PL</ta>
            <ta e="T258" id="Seg_3517" s="T257">I.GEN</ta>
            <ta e="T259" id="Seg_3518" s="T258">mother.in.law-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_3519" s="T259">I.LAT</ta>
            <ta e="T261" id="Seg_3520" s="T260">give-PST.[3SG]</ta>
            <ta e="T262" id="Seg_3521" s="T261">well</ta>
            <ta e="T263" id="Seg_3522" s="T262">NEG.AUX-IMP.2SG</ta>
            <ta e="T264" id="Seg_3523" s="T263">cry-EP-CNG</ta>
            <ta e="T265" id="Seg_3524" s="T264">all</ta>
            <ta e="T266" id="Seg_3525" s="T265">give-FUT-1SG</ta>
            <ta e="T267" id="Seg_3526" s="T266">five-ACC</ta>
            <ta e="T268" id="Seg_3527" s="T267">wash.oneself-PST.[3SG]</ta>
            <ta e="T269" id="Seg_3528" s="T268">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T270" id="Seg_3529" s="T269">tie.up-DIR-PST.[3SG]</ta>
            <ta e="T271" id="Seg_3530" s="T270">walk-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_3531" s="T271">cross</ta>
            <ta e="T273" id="Seg_3532" s="T272">tree.[NOM.SG]</ta>
            <ta e="T274" id="Seg_3533" s="T273">stand-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_3534" s="T274">this-ACC</ta>
            <ta e="T276" id="Seg_3535" s="T275">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T277" id="Seg_3536" s="T276">dig-MOM-PST.[3SG]</ta>
            <ta e="T278" id="Seg_3537" s="T277">woman.[NOM.SG]</ta>
            <ta e="T279" id="Seg_3538" s="T278">old.[NOM.SG]</ta>
            <ta e="T280" id="Seg_3539" s="T279">take.out-DIR-PST.[3SG]</ta>
            <ta e="T281" id="Seg_3540" s="T280">pick.up-DIR-PST.[3SG]</ta>
            <ta e="T282" id="Seg_3541" s="T281">again</ta>
            <ta e="T283" id="Seg_3542" s="T282">go-DUR-PST.[3SG]</ta>
            <ta e="T284" id="Seg_3543" s="T283">a.bit</ta>
            <ta e="T285" id="Seg_3544" s="T284">walk-PST.[3SG]</ta>
            <ta e="T286" id="Seg_3545" s="T285">tent.[NOM.SG]</ta>
            <ta e="T287" id="Seg_3546" s="T286">stand-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_3547" s="T287">seat-PST.[3SG]</ta>
            <ta e="T289" id="Seg_3548" s="T288">die-PTCP</ta>
            <ta e="T290" id="Seg_3549" s="T289">woman-ACC</ta>
            <ta e="T291" id="Seg_3550" s="T290">tree-GEN</ta>
            <ta e="T292" id="Seg_3551" s="T291">trunk-LAT/LOC.3SG</ta>
            <ta e="T293" id="Seg_3552" s="T292">sheep-PL-ACC</ta>
            <ta e="T294" id="Seg_3553" s="T293">hand-LAT/LOC.3SG</ta>
            <ta e="T295" id="Seg_3554" s="T294">bind-PST.[3SG]</ta>
            <ta e="T296" id="Seg_3555" s="T295">knife-NOM/GEN/ACC.3SG</ta>
            <ta e="T297" id="Seg_3556" s="T296">heart-LAT/LOC.3SG</ta>
            <ta e="T298" id="Seg_3557" s="T297">push-PST.[3SG]</ta>
            <ta e="T299" id="Seg_3558" s="T298">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T300" id="Seg_3559" s="T299">go-PST.[3SG]</ta>
            <ta e="T301" id="Seg_3560" s="T300">tent-LAT</ta>
            <ta e="T302" id="Seg_3561" s="T301">man.[NOM.SG]</ta>
            <ta e="T303" id="Seg_3562" s="T302">sit.[3SG]</ta>
            <ta e="T304" id="Seg_3563" s="T303">two.[NOM.SG]</ta>
            <ta e="T305" id="Seg_3564" s="T304">daughter-NOM/GEN.3SG</ta>
            <ta e="T306" id="Seg_3565" s="T305">sit.[3SG]</ta>
            <ta e="T307" id="Seg_3566" s="T306">eat-OPT.DU/PL-1PL</ta>
            <ta e="T308" id="Seg_3567" s="T307">I.GEN</ta>
            <ta e="T309" id="Seg_3568" s="T308">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T310" id="Seg_3569" s="T309">be-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_3570" s="T310">tree-GEN</ta>
            <ta e="T312" id="Seg_3571" s="T311">base-LAT/LOC.3SG</ta>
            <ta e="T313" id="Seg_3572" s="T312">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T314" id="Seg_3573" s="T313">hold-DUR-3SG.O</ta>
            <ta e="T315" id="Seg_3574" s="T314">daughter-PL</ta>
            <ta e="T316" id="Seg_3575" s="T315">go-CVB</ta>
            <ta e="T317" id="Seg_3576" s="T316">call-FUT-1PL</ta>
            <ta e="T318" id="Seg_3577" s="T317">NEG.AUX-IMP.2PL</ta>
            <ta e="T319" id="Seg_3578" s="T318">go-IMP.2PL</ta>
            <ta e="T320" id="Seg_3579" s="T319">frighten-FUT-CVB</ta>
            <ta e="T321" id="Seg_3580" s="T320">this.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3581" s="T321">get.frightened-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T323" id="Seg_3582" s="T322">self-ACC.3SG</ta>
            <ta e="T324" id="Seg_3583" s="T323">stab-FUT-3SG.O</ta>
            <ta e="T325" id="Seg_3584" s="T324">daughter-PL</ta>
            <ta e="T326" id="Seg_3585" s="T325">run-MOM-PST-3PL</ta>
            <ta e="T327" id="Seg_3586" s="T326">sheep-PL</ta>
            <ta e="T328" id="Seg_3587" s="T327">get.frightened-PST-3PL</ta>
            <ta e="T329" id="Seg_3588" s="T328">gallop-INCH-PST.[3SG]</ta>
            <ta e="T330" id="Seg_3589" s="T329">see-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T331" id="Seg_3590" s="T330">knife.[NOM.SG]</ta>
            <ta e="T332" id="Seg_3591" s="T331">heart-LAT/LOC.3SG</ta>
            <ta e="T333" id="Seg_3592" s="T332">push-DETR-DUR.[3SG]</ta>
            <ta e="T334" id="Seg_3593" s="T333">run-CVB</ta>
            <ta e="T335" id="Seg_3594" s="T334">come-PST-3PL</ta>
            <ta e="T336" id="Seg_3595" s="T335">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T337" id="Seg_3596" s="T336">Kochun_Gudur.[NOM.SG]</ta>
            <ta e="T338" id="Seg_3597" s="T337">woman-NOM/GEN/ACC.2SG</ta>
            <ta e="T339" id="Seg_3598" s="T338">stab-RFL-PST.[3SG]</ta>
            <ta e="T340" id="Seg_3599" s="T339">again</ta>
            <ta e="T341" id="Seg_3600" s="T340">roll-CVB</ta>
            <ta e="T342" id="Seg_3601" s="T341">cry-DUR.[3SG]</ta>
            <ta e="T343" id="Seg_3602" s="T342">fire-ABL</ta>
            <ta e="T344" id="Seg_3603" s="T343">out</ta>
            <ta e="T345" id="Seg_3604" s="T344">take-PRS-3SG.O</ta>
            <ta e="T346" id="Seg_3605" s="T345">this.[NOM.SG]</ta>
            <ta e="T347" id="Seg_3606" s="T346">again</ta>
            <ta e="T348" id="Seg_3607" s="T347">fire-LAT</ta>
            <ta e="T349" id="Seg_3608" s="T348">creep.into-DUR-PRS.[3SG]</ta>
            <ta e="T350" id="Seg_3609" s="T349">NEG.AUX-IMP.2SG</ta>
            <ta e="T351" id="Seg_3610" s="T350">cry-EP-CNG</ta>
            <ta e="T352" id="Seg_3611" s="T351">say-IPFVZ.[3SG]</ta>
            <ta e="T353" id="Seg_3612" s="T352">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T354" id="Seg_3613" s="T353">give-FUT-1SG</ta>
            <ta e="T355" id="Seg_3614" s="T354">I.LAT</ta>
            <ta e="T356" id="Seg_3615" s="T355">NEG</ta>
            <ta e="T357" id="Seg_3616" s="T356">one.needs</ta>
            <ta e="T358" id="Seg_3617" s="T357">you.GEN</ta>
            <ta e="T359" id="Seg_3618" s="T358">daughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T360" id="Seg_3619" s="T359">NEG.AUX-IMP.2SG</ta>
            <ta e="T361" id="Seg_3620" s="T360">cry-EP-CNG</ta>
            <ta e="T362" id="Seg_3621" s="T361">two.[NOM.SG]</ta>
            <ta e="T363" id="Seg_3622" s="T362">that-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_3623" s="T363">give-FUT-1SG</ta>
            <ta e="T365" id="Seg_3624" s="T364">wash.oneself-PST.[3SG]</ta>
            <ta e="T366" id="Seg_3625" s="T365">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T367" id="Seg_3626" s="T366">tie.up-MOM-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3627" s="T367">go-CVB</ta>
            <ta e="T369" id="Seg_3628" s="T368">disappear-PST.[3SG]</ta>
            <ta e="T370" id="Seg_3629" s="T369">man.[NOM.SG]</ta>
            <ta e="T371" id="Seg_3630" s="T370">alone</ta>
            <ta e="T372" id="Seg_3631" s="T371">remain-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3632" s="T0">женщина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_3633" s="T1">жить-PST.[3SG]</ta>
            <ta e="T3" id="Seg_3634" s="T2">один.[NOM.SG]</ta>
            <ta e="T4" id="Seg_3635" s="T3">сын-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_3636" s="T4">быть-PST.[3SG]</ta>
            <ta e="T6" id="Seg_3637" s="T5">сын-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_3638" s="T6">мать-LAT/LOC.3SG</ta>
            <ta e="T8" id="Seg_3639" s="T7">сказать-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_3640" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_3641" s="T9">женщина-VBLZ-FUT-1SG</ta>
            <ta e="T11" id="Seg_3642" s="T10">мать-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_3643" s="T11">сказать-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_3644" s="T12">NEG.EX</ta>
            <ta e="T14" id="Seg_3645" s="T13">соседство-LOC</ta>
            <ta e="T15" id="Seg_3646" s="T14">женщина.[NOM.SG]</ta>
            <ta e="T16" id="Seg_3647" s="T15">NEG.EX.[3SG]</ta>
            <ta e="T17" id="Seg_3648" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_3649" s="T17">пойти-FUT-1SG</ta>
            <ta e="T19" id="Seg_3650" s="T18">женщина.[NOM.SG]</ta>
            <ta e="T20" id="Seg_3651" s="T19">искать-CVB</ta>
            <ta e="T21" id="Seg_3652" s="T20">пойти-CVB</ta>
            <ta e="T22" id="Seg_3653" s="T21">исчезнуть-PST.[3SG]</ta>
            <ta e="T23" id="Seg_3654" s="T22">мать-ACC.3SG</ta>
            <ta e="T24" id="Seg_3655" s="T23">покинуть-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_3656" s="T24">идти-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_3657" s="T25">идти-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_3658" s="T26">река-GEN</ta>
            <ta e="T28" id="Seg_3659" s="T27">край-LAT/LOC.3SG</ta>
            <ta e="T29" id="Seg_3660" s="T28">чум.[NOM.SG]</ta>
            <ta e="T30" id="Seg_3661" s="T29">стоять-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_3662" s="T30">войти-PST.[3SG]</ta>
            <ta e="T32" id="Seg_3663" s="T31">этот.[NOM.SG]</ta>
            <ta e="T33" id="Seg_3664" s="T32">чум-LAT</ta>
            <ta e="T34" id="Seg_3665" s="T33">женщина.[NOM.SG]</ta>
            <ta e="T35" id="Seg_3666" s="T34">жить.[3SG]</ta>
            <ta e="T36" id="Seg_3667" s="T35">кланяться-PST.[3SG]</ta>
            <ta e="T37" id="Seg_3668" s="T36">рука-ACC.3SG</ta>
            <ta e="T38" id="Seg_3669" s="T37">дать-PST.[3SG]</ta>
            <ta e="T39" id="Seg_3670" s="T38">сесть-PST.[3SG]</ta>
            <ta e="T40" id="Seg_3671" s="T39">огонь-GEN</ta>
            <ta e="T41" id="Seg_3672" s="T40">край-LAT/LOC.3SG</ta>
            <ta e="T42" id="Seg_3673" s="T41">тетя-NOM/GEN/ACC.1SG</ta>
            <ta e="T43" id="Seg_3674" s="T42">котел.[NOM.SG]</ta>
            <ta e="T44" id="Seg_3675" s="T43">вешать-IMP.2SG</ta>
            <ta e="T45" id="Seg_3676" s="T44">мясо.[NOM.SG]</ta>
            <ta e="T46" id="Seg_3677" s="T45">варить-IMP.2SG</ta>
            <ta e="T47" id="Seg_3678" s="T46">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T48" id="Seg_3679" s="T47">NEG.EX.[3SG]</ta>
            <ta e="T49" id="Seg_3680" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_3681" s="T49">принести-PRS-1SG</ta>
            <ta e="T51" id="Seg_3682" s="T50">наружу</ta>
            <ta e="T52" id="Seg_3683" s="T51">встать-PST.[3SG]</ta>
            <ta e="T53" id="Seg_3684" s="T52">голый.[NOM.SG]</ta>
            <ta e="T54" id="Seg_3685" s="T53">лопатка.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3686" s="T54">взять-PST.[3SG]</ta>
            <ta e="T56" id="Seg_3687" s="T55">нос-ACC.3SG</ta>
            <ta e="T57" id="Seg_3688" s="T56">оторвать-CVB</ta>
            <ta e="T58" id="Seg_3689" s="T57">ударить-PST.[3SG]</ta>
            <ta e="T59" id="Seg_3690" s="T58">кровь-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_3691" s="T59">течь-MOM-PST.[3SG]</ta>
            <ta e="T61" id="Seg_3692" s="T60">этот.[NOM.SG]</ta>
            <ta e="T62" id="Seg_3693" s="T61">кровь-INS</ta>
            <ta e="T63" id="Seg_3694" s="T62">лопатка-ACC</ta>
            <ta e="T64" id="Seg_3695" s="T63">весь</ta>
            <ta e="T65" id="Seg_3696" s="T64">мазать-PST.[3SG]</ta>
            <ta e="T66" id="Seg_3697" s="T65">войти-PST.[3SG]</ta>
            <ta e="T67" id="Seg_3698" s="T66">чум-LAT</ta>
            <ta e="T68" id="Seg_3699" s="T67">этот.[NOM.SG]</ta>
            <ta e="T69" id="Seg_3700" s="T68">лопатка-ACC</ta>
            <ta e="T70" id="Seg_3701" s="T69">котел-LAT</ta>
            <ta e="T71" id="Seg_3702" s="T70">вставлять-MOM-PST.[3SG]</ta>
            <ta e="T72" id="Seg_3703" s="T71">сидеть-3PL</ta>
            <ta e="T73" id="Seg_3704" s="T72">говорить-DUR-3PL</ta>
            <ta e="T74" id="Seg_3705" s="T73">сказать-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_3706" s="T74">тетя-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_3707" s="T75">есть-OPT.DU/PL-1DU</ta>
            <ta e="T77" id="Seg_3708" s="T76">этот.[NOM.SG]</ta>
            <ta e="T78" id="Seg_3709" s="T77">женщина.[NOM.SG]</ta>
            <ta e="T79" id="Seg_3710" s="T78">котел-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T80" id="Seg_3711" s="T79">взять-PST.[3SG]</ta>
            <ta e="T81" id="Seg_3712" s="T80">огонь-GEN.3SG</ta>
            <ta e="T82" id="Seg_3713" s="T81">край-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_3714" s="T82">поставить-PST.[3SG]</ta>
            <ta e="T84" id="Seg_3715" s="T83">ковш-ACC.3SG</ta>
            <ta e="T85" id="Seg_3716" s="T84">взять-PST.[3SG]</ta>
            <ta e="T86" id="Seg_3717" s="T85">мясо-ACC.3SG</ta>
            <ta e="T87" id="Seg_3718" s="T86">черпать-DUR.[3SG]</ta>
            <ta e="T88" id="Seg_3719" s="T87">только</ta>
            <ta e="T89" id="Seg_3720" s="T88">кость.[NOM.SG]</ta>
            <ta e="T90" id="Seg_3721" s="T89">котел-LOC</ta>
            <ta e="T91" id="Seg_3722" s="T90">греметь-DUR.[3SG]</ta>
            <ta e="T92" id="Seg_3723" s="T91">где</ta>
            <ta e="T93" id="Seg_3724" s="T92">ты.GEN</ta>
            <ta e="T94" id="Seg_3725" s="T93">мясо-NOM/GEN/ACC.2SG</ta>
            <ta e="T95" id="Seg_3726" s="T94">один.[NOM.SG]</ta>
            <ta e="T96" id="Seg_3727" s="T95">голый.[NOM.SG]</ta>
            <ta e="T97" id="Seg_3728" s="T96">кость.[NOM.SG]</ta>
            <ta e="T98" id="Seg_3729" s="T97">катиться-CVB</ta>
            <ta e="T99" id="Seg_3730" s="T98">идти-PRS</ta>
            <ta e="T100" id="Seg_3731" s="T99">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T101" id="Seg_3732" s="T100">где=PTCL</ta>
            <ta e="T102" id="Seg_3733" s="T101">мясо-NOM/GEN/ACC.2SG</ta>
            <ta e="T103" id="Seg_3734" s="T102">один</ta>
            <ta e="T104" id="Seg_3735" s="T103">кость.[NOM.SG]</ta>
            <ta e="T105" id="Seg_3736" s="T104">катиться-CVB</ta>
            <ta e="T106" id="Seg_3737" s="T105">идти-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_3738" s="T106">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T108" id="Seg_3739" s="T107">плакать-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_3740" s="T108">огонь-LAT</ta>
            <ta e="T110" id="Seg_3741" s="T109">катиться-DUR-PST.[3SG]</ta>
            <ta e="T111" id="Seg_3742" s="T110">женщина.[NOM.SG]</ta>
            <ta e="T112" id="Seg_3743" s="T111">вынимать-DUR-PST.[3SG]</ta>
            <ta e="T113" id="Seg_3744" s="T112">прочь</ta>
            <ta e="T114" id="Seg_3745" s="T113">тянуть-DUR-PRS-3SG.O</ta>
            <ta e="T115" id="Seg_3746" s="T114">NEG.AUX-IMP.2SG</ta>
            <ta e="T116" id="Seg_3747" s="T115">плакать-EP-CNG</ta>
            <ta e="T117" id="Seg_3748" s="T116">ты.GEN</ta>
            <ta e="T118" id="Seg_3749" s="T117">котел-NOM/GEN/ACC.2SG</ta>
            <ta e="T119" id="Seg_3750" s="T118">высохнуть-RES-PST.[3SG]</ta>
            <ta e="T120" id="Seg_3751" s="T119">я.GEN</ta>
            <ta e="T121" id="Seg_3752" s="T120">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T122" id="Seg_3753" s="T121">съесть-MOM-PST.[3SG]</ta>
            <ta e="T123" id="Seg_3754" s="T122">женщина.[NOM.SG]</ta>
            <ta e="T124" id="Seg_3755" s="T123">NEG.AUX-IMP.2SG</ta>
            <ta e="T125" id="Seg_3756" s="T124">плакать-EP-CNG</ta>
            <ta e="T126" id="Seg_3757" s="T125">дать-PRS-1SG</ta>
            <ta e="T127" id="Seg_3758" s="T126">один.[NOM.SG]</ta>
            <ta e="T128" id="Seg_3759" s="T127">овца-NOM/GEN/ACC.1SG</ta>
            <ta e="T129" id="Seg_3760" s="T128">быть-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_3761" s="T129">этот-ACC</ta>
            <ta e="T131" id="Seg_3762" s="T130">дать-FUT-1SG</ta>
            <ta e="T132" id="Seg_3763" s="T131">тогда</ta>
            <ta e="T133" id="Seg_3764" s="T132">мыться-PST.[3SG]</ta>
            <ta e="T134" id="Seg_3765" s="T133">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T135" id="Seg_3766" s="T134">привязывать-CVB</ta>
            <ta e="T136" id="Seg_3767" s="T135">взять-PST.[3SG]</ta>
            <ta e="T137" id="Seg_3768" s="T136">идти-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_3769" s="T137">чум.[NOM.SG]</ta>
            <ta e="T139" id="Seg_3770" s="T138">стоять-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_3771" s="T139">войти-PST.[3SG]</ta>
            <ta e="T141" id="Seg_3772" s="T140">этот.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3773" s="T141">чум-LAT</ta>
            <ta e="T143" id="Seg_3774" s="T142">женщина.[NOM.SG]</ta>
            <ta e="T144" id="Seg_3775" s="T143">мужчина.[NOM.SG]</ta>
            <ta e="T145" id="Seg_3776" s="T144">сидеть-3PL</ta>
            <ta e="T146" id="Seg_3777" s="T145">рука-ACC.3SG</ta>
            <ta e="T147" id="Seg_3778" s="T146">дать-PST.[3SG]</ta>
            <ta e="T148" id="Seg_3779" s="T147">сказать-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_3780" s="T148">куда</ta>
            <ta e="T150" id="Seg_3781" s="T149">идти-PRS-2SG</ta>
            <ta e="T151" id="Seg_3782" s="T150">ты.NOM</ta>
            <ta e="T152" id="Seg_3783" s="T151">теща-NOM/GEN/ACC.1SG</ta>
            <ta e="T153" id="Seg_3784" s="T152">теща-LAT/LOC.1SG</ta>
            <ta e="T154" id="Seg_3785" s="T153">деревня-VBLZ-CVB</ta>
            <ta e="T155" id="Seg_3786" s="T154">идти-PST-1SG</ta>
            <ta e="T156" id="Seg_3787" s="T155">теща-NOM/GEN/ACC.1SG</ta>
            <ta e="T157" id="Seg_3788" s="T156">один.[NOM.SG]</ta>
            <ta e="T158" id="Seg_3789" s="T157">овца.[NOM.SG]</ta>
            <ta e="T159" id="Seg_3790" s="T158">дать-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3791" s="T159">пускать-CVB</ta>
            <ta e="T161" id="Seg_3792" s="T160">взять-IMP.2SG.O</ta>
            <ta e="T162" id="Seg_3793" s="T161">мы.GEN</ta>
            <ta e="T163" id="Seg_3794" s="T162">овца-PL-LAT/LOC.1PL</ta>
            <ta e="T164" id="Seg_3795" s="T163">пускать-CVB</ta>
            <ta e="T165" id="Seg_3796" s="T164">взять-IMP.2SG.O</ta>
            <ta e="T166" id="Seg_3797" s="T165">овца-NOM/GEN/ACC.2SG</ta>
            <ta e="T167" id="Seg_3798" s="T166">вы.GEN</ta>
            <ta e="T168" id="Seg_3799" s="T167">овца-PL-NOM/GEN/ACC.2PL</ta>
            <ta e="T169" id="Seg_3800" s="T168">я.GEN</ta>
            <ta e="T170" id="Seg_3801" s="T169">овца-NOM/GEN/ACC.1SG</ta>
            <ta e="T171" id="Seg_3802" s="T170">съесть-MOM-FUT-3PL.O</ta>
            <ta e="T172" id="Seg_3803" s="T171">что-VBLZ-CVB</ta>
            <ta e="T173" id="Seg_3804" s="T172">съесть-FUT-3PL.O</ta>
            <ta e="T174" id="Seg_3805" s="T173">ну</ta>
            <ta e="T175" id="Seg_3806" s="T174">пускать-CVB</ta>
            <ta e="T176" id="Seg_3807" s="T175">взять-IMP.2SG.O</ta>
            <ta e="T177" id="Seg_3808" s="T176">мужчина.[NOM.SG]</ta>
            <ta e="T178" id="Seg_3809" s="T177">нести-CVB</ta>
            <ta e="T179" id="Seg_3810" s="T178">пускать-CVB</ta>
            <ta e="T180" id="Seg_3811" s="T179">взять-PST.[3SG]</ta>
            <ta e="T181" id="Seg_3812" s="T180">ночь-LOC.ADV</ta>
            <ta e="T182" id="Seg_3813" s="T181">встать-PST.[3SG]</ta>
            <ta e="T183" id="Seg_3814" s="T182">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T184" id="Seg_3815" s="T183">овца-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T185" id="Seg_3816" s="T184">сам-ACC.3SG</ta>
            <ta e="T186" id="Seg_3817" s="T185">оторвать-CVB</ta>
            <ta e="T187" id="Seg_3818" s="T186">тянуть-PST.[3SG]</ta>
            <ta e="T188" id="Seg_3819" s="T187">оторвать-CVB</ta>
            <ta e="T189" id="Seg_3820" s="T188">рвать-PST.[3SG]</ta>
            <ta e="T190" id="Seg_3821" s="T189">взять-PST.[3SG]</ta>
            <ta e="T191" id="Seg_3822" s="T190">половина-ADJZ</ta>
            <ta e="T192" id="Seg_3823" s="T191">овца-PL-GEN</ta>
            <ta e="T193" id="Seg_3824" s="T192">нос-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T194" id="Seg_3825" s="T193">кровь-INS</ta>
            <ta e="T195" id="Seg_3826" s="T194">мазать-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_3827" s="T195">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T197" id="Seg_3828" s="T196">спать-INF.LAT</ta>
            <ta e="T198" id="Seg_3829" s="T197">ложиться-PST.[3SG]</ta>
            <ta e="T199" id="Seg_3830" s="T198">утро-LOC.ADV</ta>
            <ta e="T200" id="Seg_3831" s="T199">мужчина.[NOM.SG]</ta>
            <ta e="T201" id="Seg_3832" s="T200">женщина-3SG-INS</ta>
            <ta e="T202" id="Seg_3833" s="T201">встать-PST.[3SG]</ta>
            <ta e="T203" id="Seg_3834" s="T202">мужчина.[NOM.SG]</ta>
            <ta e="T204" id="Seg_3835" s="T203">пойти-PST.[3SG]</ta>
            <ta e="T205" id="Seg_3836" s="T204">кораль-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_3837" s="T205">видеть-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T207" id="Seg_3838" s="T206">овца.[NOM.SG]</ta>
            <ta e="T208" id="Seg_3839" s="T207">оторвать-CVB</ta>
            <ta e="T209" id="Seg_3840" s="T208">тянуть-DETR-CVB</ta>
            <ta e="T210" id="Seg_3841" s="T209">женщина-LAT/LOC.3SG</ta>
            <ta e="T211" id="Seg_3842" s="T210">прийти-CVB.ANT</ta>
            <ta e="T212" id="Seg_3843" s="T211">сказать-PST.[3SG]</ta>
            <ta e="T213" id="Seg_3844" s="T212">действительно</ta>
            <ta e="T214" id="Seg_3845" s="T213">что-NOM/GEN/ACC.2PL</ta>
            <ta e="T215" id="Seg_3846" s="T214">этот.[NOM.SG]</ta>
            <ta e="T216" id="Seg_3847" s="T215">мальчик-GEN</ta>
            <ta e="T217" id="Seg_3848" s="T216">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T218" id="Seg_3849" s="T217">мы.GEN</ta>
            <ta e="T219" id="Seg_3850" s="T218">овца-PL-NOM/GEN/ACC.1PL</ta>
            <ta e="T220" id="Seg_3851" s="T219">оторвать-CVB</ta>
            <ta e="T221" id="Seg_3852" s="T220">рвать-PST.[3SG]</ta>
            <ta e="T222" id="Seg_3853" s="T221">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T223" id="Seg_3854" s="T222">слышать-CVB</ta>
            <ta e="T224" id="Seg_3855" s="T223">бросить-PST.[3SG]</ta>
            <ta e="T225" id="Seg_3856" s="T224">встать-CVB</ta>
            <ta e="T226" id="Seg_3857" s="T225">обрушиться-PST.[3SG]</ta>
            <ta e="T227" id="Seg_3858" s="T226">сказать-PRS.[3SG]</ta>
            <ta e="T228" id="Seg_3859" s="T227">я.NOM</ta>
            <ta e="T229" id="Seg_3860" s="T228">сказать-PST-1SG</ta>
            <ta e="T230" id="Seg_3861" s="T229">вы.LAT</ta>
            <ta e="T231" id="Seg_3862" s="T230">овца-NOM/GEN/ACC.1SG</ta>
            <ta e="T232" id="Seg_3863" s="T231">съесть-FUT-3PL.O</ta>
            <ta e="T233" id="Seg_3864" s="T232">действительно</ta>
            <ta e="T234" id="Seg_3865" s="T233">что-NOM/GEN/ACC.2PL</ta>
            <ta e="T235" id="Seg_3866" s="T234">съесть-CVB</ta>
            <ta e="T236" id="Seg_3867" s="T235">нести-PST-3PL</ta>
            <ta e="T237" id="Seg_3868" s="T236">опять</ta>
            <ta e="T238" id="Seg_3869" s="T237">плакать-CVB</ta>
            <ta e="T239" id="Seg_3870" s="T238">катиться-CVB</ta>
            <ta e="T240" id="Seg_3871" s="T239">остаться-PST.[3SG]</ta>
            <ta e="T241" id="Seg_3872" s="T240">мужчина.[NOM.SG]</ta>
            <ta e="T242" id="Seg_3873" s="T241">огонь-ABL</ta>
            <ta e="T243" id="Seg_3874" s="T242">вынимать-TR.RES-3SG.O</ta>
            <ta e="T244" id="Seg_3875" s="T243">этот.[NOM.SG]</ta>
            <ta e="T245" id="Seg_3876" s="T244">опять</ta>
            <ta e="T246" id="Seg_3877" s="T245">огонь-LAT</ta>
            <ta e="T247" id="Seg_3878" s="T246">катиться-PRS.[3SG]</ta>
            <ta e="T248" id="Seg_3879" s="T247">NEG.AUX-IMP.2SG</ta>
            <ta e="T249" id="Seg_3880" s="T248">плакать-EP-CNG</ta>
            <ta e="T250" id="Seg_3881" s="T249">я.NOM</ta>
            <ta e="T251" id="Seg_3882" s="T250">овца-ACC</ta>
            <ta e="T252" id="Seg_3883" s="T251">дать-FUT-1SG</ta>
            <ta e="T253" id="Seg_3884" s="T252">я.LAT</ta>
            <ta e="T254" id="Seg_3885" s="T253">NEG</ta>
            <ta e="T255" id="Seg_3886" s="T254">нужно</ta>
            <ta e="T256" id="Seg_3887" s="T255">ты.GEN</ta>
            <ta e="T257" id="Seg_3888" s="T256">овца-NOM/GEN/ACC.2PL</ta>
            <ta e="T258" id="Seg_3889" s="T257">я.GEN</ta>
            <ta e="T259" id="Seg_3890" s="T258">теща-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_3891" s="T259">я.LAT</ta>
            <ta e="T261" id="Seg_3892" s="T260">дать-PST.[3SG]</ta>
            <ta e="T262" id="Seg_3893" s="T261">ну</ta>
            <ta e="T263" id="Seg_3894" s="T262">NEG.AUX-IMP.2SG</ta>
            <ta e="T264" id="Seg_3895" s="T263">плакать-EP-CNG</ta>
            <ta e="T265" id="Seg_3896" s="T264">весь</ta>
            <ta e="T266" id="Seg_3897" s="T265">дать-FUT-1SG</ta>
            <ta e="T267" id="Seg_3898" s="T266">пять-ACC</ta>
            <ta e="T268" id="Seg_3899" s="T267">мыться-PST.[3SG]</ta>
            <ta e="T269" id="Seg_3900" s="T268">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T270" id="Seg_3901" s="T269">привязывать-DIR-PST.[3SG]</ta>
            <ta e="T271" id="Seg_3902" s="T270">идти-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_3903" s="T271">крест</ta>
            <ta e="T273" id="Seg_3904" s="T272">дерево.[NOM.SG]</ta>
            <ta e="T274" id="Seg_3905" s="T273">стоять-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_3906" s="T274">этот-ACC</ta>
            <ta e="T276" id="Seg_3907" s="T275">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T277" id="Seg_3908" s="T276">копать-MOM-PST.[3SG]</ta>
            <ta e="T278" id="Seg_3909" s="T277">женщина.[NOM.SG]</ta>
            <ta e="T279" id="Seg_3910" s="T278">старый.[NOM.SG]</ta>
            <ta e="T280" id="Seg_3911" s="T279">вынимать-DIR-PST.[3SG]</ta>
            <ta e="T281" id="Seg_3912" s="T280">поднимать-DIR-PST.[3SG]</ta>
            <ta e="T282" id="Seg_3913" s="T281">опять</ta>
            <ta e="T283" id="Seg_3914" s="T282">идти-DUR-PST.[3SG]</ta>
            <ta e="T284" id="Seg_3915" s="T283">немного</ta>
            <ta e="T285" id="Seg_3916" s="T284">идти-PST.[3SG]</ta>
            <ta e="T286" id="Seg_3917" s="T285">чум.[NOM.SG]</ta>
            <ta e="T287" id="Seg_3918" s="T286">стоять-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_3919" s="T287">сажать-PST.[3SG]</ta>
            <ta e="T289" id="Seg_3920" s="T288">умереть-PTCP</ta>
            <ta e="T290" id="Seg_3921" s="T289">женщина-ACC</ta>
            <ta e="T291" id="Seg_3922" s="T290">дерево-GEN</ta>
            <ta e="T292" id="Seg_3923" s="T291">ствол-LAT/LOC.3SG</ta>
            <ta e="T293" id="Seg_3924" s="T292">овца-PL-ACC</ta>
            <ta e="T294" id="Seg_3925" s="T293">рука-LAT/LOC.3SG</ta>
            <ta e="T295" id="Seg_3926" s="T294">завязать-PST.[3SG]</ta>
            <ta e="T296" id="Seg_3927" s="T295">нож-NOM/GEN/ACC.3SG</ta>
            <ta e="T297" id="Seg_3928" s="T296">сердце-LAT/LOC.3SG</ta>
            <ta e="T298" id="Seg_3929" s="T297">вставить-PST.[3SG]</ta>
            <ta e="T299" id="Seg_3930" s="T298">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T300" id="Seg_3931" s="T299">пойти-PST.[3SG]</ta>
            <ta e="T301" id="Seg_3932" s="T300">чум-LAT</ta>
            <ta e="T302" id="Seg_3933" s="T301">мужчина.[NOM.SG]</ta>
            <ta e="T303" id="Seg_3934" s="T302">сидеть.[3SG]</ta>
            <ta e="T304" id="Seg_3935" s="T303">два.[NOM.SG]</ta>
            <ta e="T305" id="Seg_3936" s="T304">дочь-NOM/GEN.3SG</ta>
            <ta e="T306" id="Seg_3937" s="T305">сидеть.[3SG]</ta>
            <ta e="T307" id="Seg_3938" s="T306">есть-OPT.DU/PL-1PL</ta>
            <ta e="T308" id="Seg_3939" s="T307">я.GEN</ta>
            <ta e="T309" id="Seg_3940" s="T308">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T310" id="Seg_3941" s="T309">быть-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_3942" s="T310">дерево-GEN</ta>
            <ta e="T312" id="Seg_3943" s="T311">основа-LAT/LOC.3SG</ta>
            <ta e="T313" id="Seg_3944" s="T312">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T314" id="Seg_3945" s="T313">держать-DUR-3SG.O</ta>
            <ta e="T315" id="Seg_3946" s="T314">дочь-PL</ta>
            <ta e="T316" id="Seg_3947" s="T315">пойти-CVB</ta>
            <ta e="T317" id="Seg_3948" s="T316">позвать-FUT-1PL</ta>
            <ta e="T318" id="Seg_3949" s="T317">NEG.AUX-IMP.2PL</ta>
            <ta e="T319" id="Seg_3950" s="T318">пойти-IMP.2PL</ta>
            <ta e="T320" id="Seg_3951" s="T319">пугать-FUT-CVB</ta>
            <ta e="T321" id="Seg_3952" s="T320">этот.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3953" s="T321">пугаться-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T323" id="Seg_3954" s="T322">сам-ACC.3SG</ta>
            <ta e="T324" id="Seg_3955" s="T323">колоть-FUT-3SG.O</ta>
            <ta e="T325" id="Seg_3956" s="T324">дочь-PL</ta>
            <ta e="T326" id="Seg_3957" s="T325">бежать-MOM-PST-3PL</ta>
            <ta e="T327" id="Seg_3958" s="T326">овца-PL</ta>
            <ta e="T328" id="Seg_3959" s="T327">пугаться-PST-3PL</ta>
            <ta e="T329" id="Seg_3960" s="T328">скакать-INCH-PST.[3SG]</ta>
            <ta e="T330" id="Seg_3961" s="T329">видеть-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T331" id="Seg_3962" s="T330">нож.[NOM.SG]</ta>
            <ta e="T332" id="Seg_3963" s="T331">сердце-LAT/LOC.3SG</ta>
            <ta e="T333" id="Seg_3964" s="T332">вставить-DETR-DUR.[3SG]</ta>
            <ta e="T334" id="Seg_3965" s="T333">бежать-CVB</ta>
            <ta e="T335" id="Seg_3966" s="T334">прийти-PST-3PL</ta>
            <ta e="T336" id="Seg_3967" s="T335">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T337" id="Seg_3968" s="T336">Кёчун_Гюдюр.[NOM.SG]</ta>
            <ta e="T338" id="Seg_3969" s="T337">женщина-NOM/GEN/ACC.2SG</ta>
            <ta e="T339" id="Seg_3970" s="T338">колоть-RFL-PST.[3SG]</ta>
            <ta e="T340" id="Seg_3971" s="T339">опять</ta>
            <ta e="T341" id="Seg_3972" s="T340">катиться-CVB</ta>
            <ta e="T342" id="Seg_3973" s="T341">плакать-DUR.[3SG]</ta>
            <ta e="T343" id="Seg_3974" s="T342">огонь-ABL</ta>
            <ta e="T344" id="Seg_3975" s="T343">из</ta>
            <ta e="T345" id="Seg_3976" s="T344">взять-PRS-3SG.O</ta>
            <ta e="T346" id="Seg_3977" s="T345">этот.[NOM.SG]</ta>
            <ta e="T347" id="Seg_3978" s="T346">опять</ta>
            <ta e="T348" id="Seg_3979" s="T347">огонь-LAT</ta>
            <ta e="T349" id="Seg_3980" s="T348">ползти-DUR-PRS.[3SG]</ta>
            <ta e="T350" id="Seg_3981" s="T349">NEG.AUX-IMP.2SG</ta>
            <ta e="T351" id="Seg_3982" s="T350">плакать-EP-CNG</ta>
            <ta e="T352" id="Seg_3983" s="T351">сказать-IPFVZ.[3SG]</ta>
            <ta e="T353" id="Seg_3984" s="T352">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T354" id="Seg_3985" s="T353">дать-FUT-1SG</ta>
            <ta e="T355" id="Seg_3986" s="T354">я.LAT</ta>
            <ta e="T356" id="Seg_3987" s="T355">NEG</ta>
            <ta e="T357" id="Seg_3988" s="T356">нужно</ta>
            <ta e="T358" id="Seg_3989" s="T357">ты.GEN</ta>
            <ta e="T359" id="Seg_3990" s="T358">дочь-NOM/GEN/ACC.2SG</ta>
            <ta e="T360" id="Seg_3991" s="T359">NEG.AUX-IMP.2SG</ta>
            <ta e="T361" id="Seg_3992" s="T360">плакать-EP-CNG</ta>
            <ta e="T362" id="Seg_3993" s="T361">два.[NOM.SG]</ta>
            <ta e="T363" id="Seg_3994" s="T362">тот-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_3995" s="T363">дать-FUT-1SG</ta>
            <ta e="T365" id="Seg_3996" s="T364">мыться-PST.[3SG]</ta>
            <ta e="T366" id="Seg_3997" s="T365">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T367" id="Seg_3998" s="T366">привязывать-MOM-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3999" s="T367">пойти-CVB</ta>
            <ta e="T369" id="Seg_4000" s="T368">исчезнуть-PST.[3SG]</ta>
            <ta e="T370" id="Seg_4001" s="T369">мужчина.[NOM.SG]</ta>
            <ta e="T371" id="Seg_4002" s="T370">один</ta>
            <ta e="T372" id="Seg_4003" s="T371">остаться-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4004" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_4005" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_4006" s="T2">num-n:case</ta>
            <ta e="T4" id="Seg_4007" s="T3">n-n:case.poss</ta>
            <ta e="T5" id="Seg_4008" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_4009" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_4010" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_4011" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_4012" s="T8">pers</ta>
            <ta e="T10" id="Seg_4013" s="T9">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_4014" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_4015" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_4016" s="T12">v</ta>
            <ta e="T14" id="Seg_4017" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_4018" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_4019" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_4020" s="T16">pers</ta>
            <ta e="T18" id="Seg_4021" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_4022" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_4023" s="T19">v-v:n.fin</ta>
            <ta e="T21" id="Seg_4024" s="T20">v-v:n.fin</ta>
            <ta e="T22" id="Seg_4025" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_4026" s="T22">n-n:case.poss</ta>
            <ta e="T24" id="Seg_4027" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_4028" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_4029" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_4030" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_4031" s="T27">n-n:case.poss</ta>
            <ta e="T29" id="Seg_4032" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_4033" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_4034" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_4035" s="T31">dempro-n:case</ta>
            <ta e="T33" id="Seg_4036" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_4037" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_4038" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_4039" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_4040" s="T36">n-n:case.poss</ta>
            <ta e="T38" id="Seg_4041" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_4042" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_4043" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_4044" s="T40">n-n:case.poss</ta>
            <ta e="T42" id="Seg_4045" s="T41">n-n:case.poss</ta>
            <ta e="T43" id="Seg_4046" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_4047" s="T43">v-v:mood.pn</ta>
            <ta e="T45" id="Seg_4048" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_4049" s="T45">v-v:mood.pn</ta>
            <ta e="T47" id="Seg_4050" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_4051" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_4052" s="T48">pers</ta>
            <ta e="T50" id="Seg_4053" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_4054" s="T50">adv</ta>
            <ta e="T52" id="Seg_4055" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_4056" s="T52">adj-n:case</ta>
            <ta e="T54" id="Seg_4057" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_4058" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_4059" s="T55">n-n:case.poss</ta>
            <ta e="T57" id="Seg_4060" s="T56">v-v:n.fin</ta>
            <ta e="T58" id="Seg_4061" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_4062" s="T58">n-n:case.poss</ta>
            <ta e="T60" id="Seg_4063" s="T59">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_4064" s="T60">dempro-n:case</ta>
            <ta e="T62" id="Seg_4065" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_4066" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_4067" s="T63">quant</ta>
            <ta e="T65" id="Seg_4068" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_4069" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_4070" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_4071" s="T67">dempro-n:case</ta>
            <ta e="T69" id="Seg_4072" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_4073" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_4074" s="T70">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_4075" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_4076" s="T72">v-v&gt;v-v:pn</ta>
            <ta e="T74" id="Seg_4077" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_4078" s="T74">n-n:case.poss</ta>
            <ta e="T76" id="Seg_4079" s="T75">v-v:mood-v:pn</ta>
            <ta e="T77" id="Seg_4080" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_4081" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_4082" s="T78">n-n:case.poss-n:case.poss</ta>
            <ta e="T80" id="Seg_4083" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_4084" s="T80">n-n:case.poss</ta>
            <ta e="T82" id="Seg_4085" s="T81">n-n:case.poss</ta>
            <ta e="T83" id="Seg_4086" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_4087" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_4088" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_4089" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_4090" s="T86">v-v&gt;v-v:pn</ta>
            <ta e="T88" id="Seg_4091" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4092" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_4093" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_4094" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_4095" s="T91">que</ta>
            <ta e="T93" id="Seg_4096" s="T92">pers</ta>
            <ta e="T94" id="Seg_4097" s="T93">n-n:case.poss</ta>
            <ta e="T95" id="Seg_4098" s="T94">adj-n:case</ta>
            <ta e="T96" id="Seg_4099" s="T95">adj-n:case</ta>
            <ta e="T97" id="Seg_4100" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_4101" s="T97">v-v:n.fin</ta>
            <ta e="T99" id="Seg_4102" s="T98">v-v:tense</ta>
            <ta e="T100" id="Seg_4103" s="T99">propr-n:case</ta>
            <ta e="T101" id="Seg_4104" s="T100">que=ptcl</ta>
            <ta e="T102" id="Seg_4105" s="T101">n-n:case.poss</ta>
            <ta e="T103" id="Seg_4106" s="T102">adj</ta>
            <ta e="T104" id="Seg_4107" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_4108" s="T104">v-v:n.fin</ta>
            <ta e="T106" id="Seg_4109" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_4110" s="T106">propr-n:case</ta>
            <ta e="T108" id="Seg_4111" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_4112" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_4113" s="T109">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_4114" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_4115" s="T111">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_4116" s="T112">adv</ta>
            <ta e="T114" id="Seg_4117" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_4118" s="T114">aux-v:mood.pn</ta>
            <ta e="T116" id="Seg_4119" s="T115">v-v:ins-v:n.fin</ta>
            <ta e="T117" id="Seg_4120" s="T116">pers</ta>
            <ta e="T118" id="Seg_4121" s="T117">n-n:case.poss</ta>
            <ta e="T119" id="Seg_4122" s="T118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_4123" s="T119">pers</ta>
            <ta e="T121" id="Seg_4124" s="T120">n-n:case.poss</ta>
            <ta e="T122" id="Seg_4125" s="T121">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_4126" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_4127" s="T123">aux-v:mood.pn</ta>
            <ta e="T125" id="Seg_4128" s="T124">v-v:ins-v:n.fin</ta>
            <ta e="T126" id="Seg_4129" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_4130" s="T126">adj-n:case</ta>
            <ta e="T128" id="Seg_4131" s="T127">n-n:case.poss</ta>
            <ta e="T129" id="Seg_4132" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_4133" s="T129">dempro-n:case</ta>
            <ta e="T131" id="Seg_4134" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_4135" s="T131">adv</ta>
            <ta e="T133" id="Seg_4136" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_4137" s="T133">n-n:case.poss</ta>
            <ta e="T135" id="Seg_4138" s="T134">v-v:n.fin</ta>
            <ta e="T136" id="Seg_4139" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_4140" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_4141" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_4142" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_4143" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_4144" s="T140">dempro-n:case</ta>
            <ta e="T142" id="Seg_4145" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_4146" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_4147" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_4148" s="T144">v-v:pn</ta>
            <ta e="T146" id="Seg_4149" s="T145">n-n:case.poss</ta>
            <ta e="T147" id="Seg_4150" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_4151" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_4152" s="T148">que</ta>
            <ta e="T150" id="Seg_4153" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_4154" s="T150">pers</ta>
            <ta e="T152" id="Seg_4155" s="T151">n-n:case.poss</ta>
            <ta e="T153" id="Seg_4156" s="T152">n-n:case.poss</ta>
            <ta e="T154" id="Seg_4157" s="T153">n-n&gt;v-v:n.fin</ta>
            <ta e="T155" id="Seg_4158" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_4159" s="T155">n-n:case.poss</ta>
            <ta e="T157" id="Seg_4160" s="T156">adj-n:case</ta>
            <ta e="T158" id="Seg_4161" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_4162" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_4163" s="T159">v-v:n.fin</ta>
            <ta e="T161" id="Seg_4164" s="T160">v-v:mood.pn</ta>
            <ta e="T162" id="Seg_4165" s="T161">pers</ta>
            <ta e="T163" id="Seg_4166" s="T162">n-n:num-n:case.poss</ta>
            <ta e="T164" id="Seg_4167" s="T163">v-v:n.fin</ta>
            <ta e="T165" id="Seg_4168" s="T164">v-v:mood.pn</ta>
            <ta e="T166" id="Seg_4169" s="T165">n-n:case.poss</ta>
            <ta e="T167" id="Seg_4170" s="T166">pers</ta>
            <ta e="T168" id="Seg_4171" s="T167">n-n:num-n:case.poss</ta>
            <ta e="T169" id="Seg_4172" s="T168">pers</ta>
            <ta e="T170" id="Seg_4173" s="T169">n-n:case.poss</ta>
            <ta e="T171" id="Seg_4174" s="T170">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_4175" s="T171">que-n&gt;v-v:n.fin</ta>
            <ta e="T173" id="Seg_4176" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_4177" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_4178" s="T174">v-v:n.fin</ta>
            <ta e="T176" id="Seg_4179" s="T175">v-v:mood.pn</ta>
            <ta e="T177" id="Seg_4180" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_4181" s="T177">v-v:n.fin</ta>
            <ta e="T179" id="Seg_4182" s="T178">v-v:n.fin</ta>
            <ta e="T180" id="Seg_4183" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_4184" s="T180">n-adv:case</ta>
            <ta e="T182" id="Seg_4185" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_4186" s="T182">propr-n:case</ta>
            <ta e="T184" id="Seg_4187" s="T183">n-n:case.poss-n:case.poss</ta>
            <ta e="T185" id="Seg_4188" s="T184">refl-n:case.poss</ta>
            <ta e="T186" id="Seg_4189" s="T185">v-v:n.fin</ta>
            <ta e="T187" id="Seg_4190" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_4191" s="T187">v-v:n.fin</ta>
            <ta e="T189" id="Seg_4192" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_4193" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_4194" s="T190">n-n&gt;adj</ta>
            <ta e="T192" id="Seg_4195" s="T191">n-n:num-n:case</ta>
            <ta e="T193" id="Seg_4196" s="T192">n-n:num-n:case.poss</ta>
            <ta e="T194" id="Seg_4197" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_4198" s="T194">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_4199" s="T195">refl-n:case.poss</ta>
            <ta e="T197" id="Seg_4200" s="T196">v-v:n.fin</ta>
            <ta e="T198" id="Seg_4201" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_4202" s="T198">n-adv:case</ta>
            <ta e="T200" id="Seg_4203" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_4204" s="T200">n-n:case.poss-n:case</ta>
            <ta e="T202" id="Seg_4205" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_4206" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_4207" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_4208" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_4209" s="T205">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T207" id="Seg_4210" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_4211" s="T207">v-v:n.fin</ta>
            <ta e="T209" id="Seg_4212" s="T208">v-v&gt;v-v:n.fin</ta>
            <ta e="T210" id="Seg_4213" s="T209">n-n:case.poss</ta>
            <ta e="T211" id="Seg_4214" s="T210">v-v:n.fin</ta>
            <ta e="T212" id="Seg_4215" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_4216" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_4217" s="T213">que-n:case.poss</ta>
            <ta e="T215" id="Seg_4218" s="T214">dempro-n:case</ta>
            <ta e="T216" id="Seg_4219" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_4220" s="T216">n-n:case.poss</ta>
            <ta e="T218" id="Seg_4221" s="T217">pers</ta>
            <ta e="T219" id="Seg_4222" s="T218">n-n:num-n:case.poss</ta>
            <ta e="T220" id="Seg_4223" s="T219">v-v:n.fin</ta>
            <ta e="T221" id="Seg_4224" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_4225" s="T221">propr-n:case</ta>
            <ta e="T223" id="Seg_4226" s="T222">v-v:n.fin</ta>
            <ta e="T224" id="Seg_4227" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_4228" s="T224">v-v:n.fin</ta>
            <ta e="T226" id="Seg_4229" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_4230" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_4231" s="T227">pers</ta>
            <ta e="T229" id="Seg_4232" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_4233" s="T229">pers</ta>
            <ta e="T231" id="Seg_4234" s="T230">n-n:case.poss</ta>
            <ta e="T232" id="Seg_4235" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_4236" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_4237" s="T233">que-n:case.poss</ta>
            <ta e="T235" id="Seg_4238" s="T234">v-v:n.fin</ta>
            <ta e="T236" id="Seg_4239" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_4240" s="T236">adv</ta>
            <ta e="T238" id="Seg_4241" s="T237">v-v:n.fin</ta>
            <ta e="T239" id="Seg_4242" s="T238">v-v:n.fin</ta>
            <ta e="T240" id="Seg_4243" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_4244" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_4245" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_4246" s="T242">v-v&gt;v-v:pn</ta>
            <ta e="T244" id="Seg_4247" s="T243">dempro-n:case</ta>
            <ta e="T245" id="Seg_4248" s="T244">adv</ta>
            <ta e="T246" id="Seg_4249" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_4250" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_4251" s="T247">aux-v:mood.pn</ta>
            <ta e="T249" id="Seg_4252" s="T248">v-v:ins-v:n.fin</ta>
            <ta e="T250" id="Seg_4253" s="T249">pers</ta>
            <ta e="T251" id="Seg_4254" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_4255" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_4256" s="T252">pers</ta>
            <ta e="T254" id="Seg_4257" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_4258" s="T254">adv</ta>
            <ta e="T256" id="Seg_4259" s="T255">pers</ta>
            <ta e="T257" id="Seg_4260" s="T256">n-n:case.poss</ta>
            <ta e="T258" id="Seg_4261" s="T257">pers</ta>
            <ta e="T259" id="Seg_4262" s="T258">n-n:case.poss</ta>
            <ta e="T260" id="Seg_4263" s="T259">pers</ta>
            <ta e="T261" id="Seg_4264" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_4265" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4266" s="T262">aux-v:mood.pn</ta>
            <ta e="T264" id="Seg_4267" s="T263">v-v:ins-v:n.fin</ta>
            <ta e="T265" id="Seg_4268" s="T264">quant</ta>
            <ta e="T266" id="Seg_4269" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_4270" s="T266">num-n:case</ta>
            <ta e="T268" id="Seg_4271" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_4272" s="T268">n-n:num-n:case.poss</ta>
            <ta e="T270" id="Seg_4273" s="T269">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_4274" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_4275" s="T271">n</ta>
            <ta e="T273" id="Seg_4276" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_4277" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_4278" s="T274">dempro-n:case</ta>
            <ta e="T276" id="Seg_4279" s="T275">propr-n:case</ta>
            <ta e="T277" id="Seg_4280" s="T276">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_4281" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_4282" s="T278">adj-n:case</ta>
            <ta e="T280" id="Seg_4283" s="T279">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_4284" s="T280">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_4285" s="T281">adv</ta>
            <ta e="T283" id="Seg_4286" s="T282">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_4287" s="T283">adv</ta>
            <ta e="T285" id="Seg_4288" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_4289" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_4290" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_4291" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_4292" s="T288">v-v:n.fin</ta>
            <ta e="T290" id="Seg_4293" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_4294" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_4295" s="T291">n-n:case.poss</ta>
            <ta e="T293" id="Seg_4296" s="T292">n-n:num-n:case</ta>
            <ta e="T294" id="Seg_4297" s="T293">n-n:case.poss</ta>
            <ta e="T295" id="Seg_4298" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_4299" s="T295">n-n:case.poss</ta>
            <ta e="T297" id="Seg_4300" s="T296">n-n:case.poss</ta>
            <ta e="T298" id="Seg_4301" s="T297">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_4302" s="T298">refl-n:case.poss</ta>
            <ta e="T300" id="Seg_4303" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_4304" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_4305" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4306" s="T302">v-v:pn</ta>
            <ta e="T304" id="Seg_4307" s="T303">num-n:case</ta>
            <ta e="T305" id="Seg_4308" s="T304">n-n:case.poss</ta>
            <ta e="T306" id="Seg_4309" s="T305">v-v:pn</ta>
            <ta e="T307" id="Seg_4310" s="T306">v-v:mood-v:pn</ta>
            <ta e="T308" id="Seg_4311" s="T307">pers</ta>
            <ta e="T309" id="Seg_4312" s="T308">n-n:case.poss</ta>
            <ta e="T310" id="Seg_4313" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_4314" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_4315" s="T311">n-n:case.poss</ta>
            <ta e="T313" id="Seg_4316" s="T312">n-n:num-n:case.poss</ta>
            <ta e="T314" id="Seg_4317" s="T313">v-v&gt;v-v:pn</ta>
            <ta e="T315" id="Seg_4318" s="T314">n-n:num</ta>
            <ta e="T316" id="Seg_4319" s="T315">v-v:n.fin</ta>
            <ta e="T317" id="Seg_4320" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_4321" s="T317">aux-v:mood.pn</ta>
            <ta e="T319" id="Seg_4322" s="T318">v-v:mood.pn</ta>
            <ta e="T320" id="Seg_4323" s="T319">v-v:tense-v:n.fin</ta>
            <ta e="T321" id="Seg_4324" s="T320">dempro-n:case</ta>
            <ta e="T322" id="Seg_4325" s="T321">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T323" id="Seg_4326" s="T322">refl-n:case.poss</ta>
            <ta e="T324" id="Seg_4327" s="T323">v-v:tense-v:pn</ta>
            <ta e="T325" id="Seg_4328" s="T324">n-n:num</ta>
            <ta e="T326" id="Seg_4329" s="T325">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_4330" s="T326">n-n:num</ta>
            <ta e="T328" id="Seg_4331" s="T327">v-v:tense-v:pn</ta>
            <ta e="T329" id="Seg_4332" s="T328">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_4333" s="T329">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T331" id="Seg_4334" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_4335" s="T331">n-n:case.poss</ta>
            <ta e="T333" id="Seg_4336" s="T332">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T334" id="Seg_4337" s="T333">v-v:n.fin</ta>
            <ta e="T335" id="Seg_4338" s="T334">v-v:tense-v:pn</ta>
            <ta e="T336" id="Seg_4339" s="T335">propr-n:case</ta>
            <ta e="T337" id="Seg_4340" s="T336">propr-n:case</ta>
            <ta e="T338" id="Seg_4341" s="T337">n-n:case.poss</ta>
            <ta e="T339" id="Seg_4342" s="T338">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_4343" s="T339">adv</ta>
            <ta e="T341" id="Seg_4344" s="T340">v-v:n.fin</ta>
            <ta e="T342" id="Seg_4345" s="T341">v-v&gt;v-v:pn</ta>
            <ta e="T343" id="Seg_4346" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_4347" s="T343">adv</ta>
            <ta e="T345" id="Seg_4348" s="T344">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_4349" s="T345">dempro-n:case</ta>
            <ta e="T347" id="Seg_4350" s="T346">adv</ta>
            <ta e="T348" id="Seg_4351" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_4352" s="T348">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_4353" s="T349">aux-v:mood.pn</ta>
            <ta e="T351" id="Seg_4354" s="T350">v-v:ins-v:n.fin</ta>
            <ta e="T352" id="Seg_4355" s="T351">v-v&gt;v-v:pn</ta>
            <ta e="T353" id="Seg_4356" s="T352">n-n:case.poss</ta>
            <ta e="T354" id="Seg_4357" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_4358" s="T354">pers</ta>
            <ta e="T356" id="Seg_4359" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_4360" s="T356">adv</ta>
            <ta e="T358" id="Seg_4361" s="T357">pers</ta>
            <ta e="T359" id="Seg_4362" s="T358">n-n:case.poss</ta>
            <ta e="T360" id="Seg_4363" s="T359">aux-v:mood.pn</ta>
            <ta e="T361" id="Seg_4364" s="T360">v-v:ins-v:n.fin</ta>
            <ta e="T362" id="Seg_4365" s="T361">num-n:case</ta>
            <ta e="T363" id="Seg_4366" s="T362">dempro-n:case.poss</ta>
            <ta e="T364" id="Seg_4367" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_4368" s="T364">v-v:tense-v:pn</ta>
            <ta e="T366" id="Seg_4369" s="T365">n-n:num-n:case.poss</ta>
            <ta e="T367" id="Seg_4370" s="T366">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_4371" s="T367">v-v:n.fin</ta>
            <ta e="T369" id="Seg_4372" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_4373" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_4374" s="T370">adv</ta>
            <ta e="T372" id="Seg_4375" s="T371">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4376" s="T0">n</ta>
            <ta e="T2" id="Seg_4377" s="T1">v</ta>
            <ta e="T3" id="Seg_4378" s="T2">num</ta>
            <ta e="T4" id="Seg_4379" s="T3">n</ta>
            <ta e="T5" id="Seg_4380" s="T4">v</ta>
            <ta e="T6" id="Seg_4381" s="T5">n</ta>
            <ta e="T7" id="Seg_4382" s="T6">n</ta>
            <ta e="T8" id="Seg_4383" s="T7">v</ta>
            <ta e="T9" id="Seg_4384" s="T8">pers</ta>
            <ta e="T10" id="Seg_4385" s="T9">v</ta>
            <ta e="T11" id="Seg_4386" s="T10">n</ta>
            <ta e="T12" id="Seg_4387" s="T11">v</ta>
            <ta e="T13" id="Seg_4388" s="T12">v</ta>
            <ta e="T14" id="Seg_4389" s="T13">n</ta>
            <ta e="T15" id="Seg_4390" s="T14">n</ta>
            <ta e="T16" id="Seg_4391" s="T15">v</ta>
            <ta e="T17" id="Seg_4392" s="T16">pers</ta>
            <ta e="T18" id="Seg_4393" s="T17">v</ta>
            <ta e="T19" id="Seg_4394" s="T18">n</ta>
            <ta e="T20" id="Seg_4395" s="T19">v</ta>
            <ta e="T21" id="Seg_4396" s="T20">v</ta>
            <ta e="T22" id="Seg_4397" s="T21">v</ta>
            <ta e="T23" id="Seg_4398" s="T22">n</ta>
            <ta e="T24" id="Seg_4399" s="T23">v</ta>
            <ta e="T25" id="Seg_4400" s="T24">v</ta>
            <ta e="T26" id="Seg_4401" s="T25">v</ta>
            <ta e="T27" id="Seg_4402" s="T26">n</ta>
            <ta e="T28" id="Seg_4403" s="T27">n</ta>
            <ta e="T29" id="Seg_4404" s="T28">n</ta>
            <ta e="T30" id="Seg_4405" s="T29">v</ta>
            <ta e="T31" id="Seg_4406" s="T30">v</ta>
            <ta e="T32" id="Seg_4407" s="T31">dempro</ta>
            <ta e="T33" id="Seg_4408" s="T32">n</ta>
            <ta e="T34" id="Seg_4409" s="T33">n</ta>
            <ta e="T35" id="Seg_4410" s="T34">v</ta>
            <ta e="T36" id="Seg_4411" s="T35">v</ta>
            <ta e="T37" id="Seg_4412" s="T36">n</ta>
            <ta e="T38" id="Seg_4413" s="T37">v</ta>
            <ta e="T39" id="Seg_4414" s="T38">v</ta>
            <ta e="T40" id="Seg_4415" s="T39">n</ta>
            <ta e="T41" id="Seg_4416" s="T40">n</ta>
            <ta e="T42" id="Seg_4417" s="T41">n</ta>
            <ta e="T43" id="Seg_4418" s="T42">n</ta>
            <ta e="T44" id="Seg_4419" s="T43">v</ta>
            <ta e="T45" id="Seg_4420" s="T44">n</ta>
            <ta e="T46" id="Seg_4421" s="T45">v</ta>
            <ta e="T47" id="Seg_4422" s="T46">n</ta>
            <ta e="T48" id="Seg_4423" s="T47">v</ta>
            <ta e="T49" id="Seg_4424" s="T48">pers</ta>
            <ta e="T50" id="Seg_4425" s="T49">v</ta>
            <ta e="T51" id="Seg_4426" s="T50">adv</ta>
            <ta e="T52" id="Seg_4427" s="T51">v</ta>
            <ta e="T53" id="Seg_4428" s="T52">adj</ta>
            <ta e="T54" id="Seg_4429" s="T53">n</ta>
            <ta e="T55" id="Seg_4430" s="T54">v</ta>
            <ta e="T56" id="Seg_4431" s="T55">n</ta>
            <ta e="T57" id="Seg_4432" s="T56">v</ta>
            <ta e="T58" id="Seg_4433" s="T57">v</ta>
            <ta e="T59" id="Seg_4434" s="T58">n</ta>
            <ta e="T60" id="Seg_4435" s="T59">v</ta>
            <ta e="T61" id="Seg_4436" s="T60">dempro</ta>
            <ta e="T62" id="Seg_4437" s="T61">n</ta>
            <ta e="T63" id="Seg_4438" s="T62">n</ta>
            <ta e="T64" id="Seg_4439" s="T63">quant</ta>
            <ta e="T65" id="Seg_4440" s="T64">v</ta>
            <ta e="T66" id="Seg_4441" s="T65">v</ta>
            <ta e="T67" id="Seg_4442" s="T66">n</ta>
            <ta e="T68" id="Seg_4443" s="T67">dempro</ta>
            <ta e="T69" id="Seg_4444" s="T68">n</ta>
            <ta e="T70" id="Seg_4445" s="T69">n</ta>
            <ta e="T71" id="Seg_4446" s="T70">v</ta>
            <ta e="T72" id="Seg_4447" s="T71">v</ta>
            <ta e="T73" id="Seg_4448" s="T72">v</ta>
            <ta e="T74" id="Seg_4449" s="T73">v</ta>
            <ta e="T75" id="Seg_4450" s="T74">n</ta>
            <ta e="T76" id="Seg_4451" s="T75">v</ta>
            <ta e="T77" id="Seg_4452" s="T76">dempro</ta>
            <ta e="T78" id="Seg_4453" s="T77">n</ta>
            <ta e="T79" id="Seg_4454" s="T78">n</ta>
            <ta e="T80" id="Seg_4455" s="T79">v</ta>
            <ta e="T81" id="Seg_4456" s="T80">n</ta>
            <ta e="T82" id="Seg_4457" s="T81">n</ta>
            <ta e="T83" id="Seg_4458" s="T82">n</ta>
            <ta e="T84" id="Seg_4459" s="T83">n</ta>
            <ta e="T85" id="Seg_4460" s="T84">v</ta>
            <ta e="T86" id="Seg_4461" s="T85">n</ta>
            <ta e="T87" id="Seg_4462" s="T86">v</ta>
            <ta e="T88" id="Seg_4463" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4464" s="T88">n</ta>
            <ta e="T90" id="Seg_4465" s="T89">n</ta>
            <ta e="T91" id="Seg_4466" s="T90">v</ta>
            <ta e="T92" id="Seg_4467" s="T91">que</ta>
            <ta e="T93" id="Seg_4468" s="T92">pers</ta>
            <ta e="T94" id="Seg_4469" s="T93">n</ta>
            <ta e="T95" id="Seg_4470" s="T94">adj</ta>
            <ta e="T96" id="Seg_4471" s="T95">adj</ta>
            <ta e="T97" id="Seg_4472" s="T96">n</ta>
            <ta e="T98" id="Seg_4473" s="T97">v</ta>
            <ta e="T99" id="Seg_4474" s="T98">v</ta>
            <ta e="T100" id="Seg_4475" s="T99">propr</ta>
            <ta e="T101" id="Seg_4476" s="T100">que</ta>
            <ta e="T102" id="Seg_4477" s="T101">n</ta>
            <ta e="T103" id="Seg_4478" s="T102">adj</ta>
            <ta e="T104" id="Seg_4479" s="T103">n</ta>
            <ta e="T105" id="Seg_4480" s="T104">v</ta>
            <ta e="T106" id="Seg_4481" s="T105">v</ta>
            <ta e="T107" id="Seg_4482" s="T106">propr</ta>
            <ta e="T108" id="Seg_4483" s="T107">v</ta>
            <ta e="T109" id="Seg_4484" s="T108">n</ta>
            <ta e="T110" id="Seg_4485" s="T109">v</ta>
            <ta e="T111" id="Seg_4486" s="T110">n</ta>
            <ta e="T112" id="Seg_4487" s="T111">v</ta>
            <ta e="T113" id="Seg_4488" s="T112">adv</ta>
            <ta e="T114" id="Seg_4489" s="T113">v</ta>
            <ta e="T115" id="Seg_4490" s="T114">aux</ta>
            <ta e="T116" id="Seg_4491" s="T115">v</ta>
            <ta e="T117" id="Seg_4492" s="T116">pers</ta>
            <ta e="T118" id="Seg_4493" s="T117">n</ta>
            <ta e="T119" id="Seg_4494" s="T118">v</ta>
            <ta e="T120" id="Seg_4495" s="T119">pers</ta>
            <ta e="T121" id="Seg_4496" s="T120">n</ta>
            <ta e="T122" id="Seg_4497" s="T121">v</ta>
            <ta e="T123" id="Seg_4498" s="T122">n</ta>
            <ta e="T124" id="Seg_4499" s="T123">aux</ta>
            <ta e="T125" id="Seg_4500" s="T124">v</ta>
            <ta e="T126" id="Seg_4501" s="T125">v</ta>
            <ta e="T127" id="Seg_4502" s="T126">adj</ta>
            <ta e="T128" id="Seg_4503" s="T127">n</ta>
            <ta e="T129" id="Seg_4504" s="T128">v</ta>
            <ta e="T130" id="Seg_4505" s="T129">dempro</ta>
            <ta e="T131" id="Seg_4506" s="T130">v</ta>
            <ta e="T132" id="Seg_4507" s="T131">adv</ta>
            <ta e="T133" id="Seg_4508" s="T132">v</ta>
            <ta e="T134" id="Seg_4509" s="T133">n</ta>
            <ta e="T135" id="Seg_4510" s="T134">v</ta>
            <ta e="T136" id="Seg_4511" s="T135">v</ta>
            <ta e="T137" id="Seg_4512" s="T136">v</ta>
            <ta e="T138" id="Seg_4513" s="T137">n</ta>
            <ta e="T139" id="Seg_4514" s="T138">v</ta>
            <ta e="T140" id="Seg_4515" s="T139">v</ta>
            <ta e="T141" id="Seg_4516" s="T140">dempro</ta>
            <ta e="T142" id="Seg_4517" s="T141">n</ta>
            <ta e="T143" id="Seg_4518" s="T142">n</ta>
            <ta e="T144" id="Seg_4519" s="T143">n</ta>
            <ta e="T145" id="Seg_4520" s="T144">v</ta>
            <ta e="T146" id="Seg_4521" s="T145">n</ta>
            <ta e="T147" id="Seg_4522" s="T146">v</ta>
            <ta e="T148" id="Seg_4523" s="T147">v</ta>
            <ta e="T149" id="Seg_4524" s="T148">que</ta>
            <ta e="T150" id="Seg_4525" s="T149">v</ta>
            <ta e="T151" id="Seg_4526" s="T150">pers</ta>
            <ta e="T152" id="Seg_4527" s="T151">n</ta>
            <ta e="T153" id="Seg_4528" s="T152">n</ta>
            <ta e="T154" id="Seg_4529" s="T153">v</ta>
            <ta e="T155" id="Seg_4530" s="T154">v</ta>
            <ta e="T156" id="Seg_4531" s="T155">n</ta>
            <ta e="T157" id="Seg_4532" s="T156">adj</ta>
            <ta e="T158" id="Seg_4533" s="T157">n</ta>
            <ta e="T159" id="Seg_4534" s="T158">v</ta>
            <ta e="T160" id="Seg_4535" s="T159">v</ta>
            <ta e="T161" id="Seg_4536" s="T160">v</ta>
            <ta e="T162" id="Seg_4537" s="T161">pers</ta>
            <ta e="T163" id="Seg_4538" s="T162">n</ta>
            <ta e="T164" id="Seg_4539" s="T163">v</ta>
            <ta e="T165" id="Seg_4540" s="T164">v</ta>
            <ta e="T166" id="Seg_4541" s="T165">n</ta>
            <ta e="T167" id="Seg_4542" s="T166">pers</ta>
            <ta e="T168" id="Seg_4543" s="T167">n</ta>
            <ta e="T169" id="Seg_4544" s="T168">pers</ta>
            <ta e="T170" id="Seg_4545" s="T169">n</ta>
            <ta e="T171" id="Seg_4546" s="T170">v</ta>
            <ta e="T172" id="Seg_4547" s="T171">v</ta>
            <ta e="T173" id="Seg_4548" s="T172">v</ta>
            <ta e="T174" id="Seg_4549" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_4550" s="T174">v</ta>
            <ta e="T176" id="Seg_4551" s="T175">v</ta>
            <ta e="T177" id="Seg_4552" s="T176">n</ta>
            <ta e="T178" id="Seg_4553" s="T177">v</ta>
            <ta e="T179" id="Seg_4554" s="T178">v</ta>
            <ta e="T180" id="Seg_4555" s="T179">v</ta>
            <ta e="T181" id="Seg_4556" s="T180">adv</ta>
            <ta e="T182" id="Seg_4557" s="T181">v</ta>
            <ta e="T183" id="Seg_4558" s="T182">propr</ta>
            <ta e="T184" id="Seg_4559" s="T183">n</ta>
            <ta e="T185" id="Seg_4560" s="T184">refl</ta>
            <ta e="T186" id="Seg_4561" s="T185">v</ta>
            <ta e="T187" id="Seg_4562" s="T186">v</ta>
            <ta e="T188" id="Seg_4563" s="T187">v</ta>
            <ta e="T189" id="Seg_4564" s="T188">v</ta>
            <ta e="T190" id="Seg_4565" s="T189">v</ta>
            <ta e="T191" id="Seg_4566" s="T190">adj</ta>
            <ta e="T192" id="Seg_4567" s="T191">n</ta>
            <ta e="T193" id="Seg_4568" s="T192">n</ta>
            <ta e="T194" id="Seg_4569" s="T193">n</ta>
            <ta e="T195" id="Seg_4570" s="T194">v</ta>
            <ta e="T196" id="Seg_4571" s="T195">refl</ta>
            <ta e="T197" id="Seg_4572" s="T196">v</ta>
            <ta e="T198" id="Seg_4573" s="T197">v</ta>
            <ta e="T199" id="Seg_4574" s="T198">adv</ta>
            <ta e="T200" id="Seg_4575" s="T199">n</ta>
            <ta e="T201" id="Seg_4576" s="T200">n</ta>
            <ta e="T202" id="Seg_4577" s="T201">v</ta>
            <ta e="T203" id="Seg_4578" s="T202">n</ta>
            <ta e="T204" id="Seg_4579" s="T203">v</ta>
            <ta e="T205" id="Seg_4580" s="T204">n</ta>
            <ta e="T206" id="Seg_4581" s="T205">v</ta>
            <ta e="T207" id="Seg_4582" s="T206">n</ta>
            <ta e="T208" id="Seg_4583" s="T207">v</ta>
            <ta e="T209" id="Seg_4584" s="T208">v</ta>
            <ta e="T210" id="Seg_4585" s="T209">n</ta>
            <ta e="T211" id="Seg_4586" s="T210">v</ta>
            <ta e="T212" id="Seg_4587" s="T211">v</ta>
            <ta e="T213" id="Seg_4588" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_4589" s="T213">que</ta>
            <ta e="T215" id="Seg_4590" s="T214">dempro</ta>
            <ta e="T216" id="Seg_4591" s="T215">n</ta>
            <ta e="T217" id="Seg_4592" s="T216">n</ta>
            <ta e="T218" id="Seg_4593" s="T217">pers</ta>
            <ta e="T219" id="Seg_4594" s="T218">n</ta>
            <ta e="T220" id="Seg_4595" s="T219">v</ta>
            <ta e="T221" id="Seg_4596" s="T220">v</ta>
            <ta e="T222" id="Seg_4597" s="T221">propr</ta>
            <ta e="T223" id="Seg_4598" s="T222">v</ta>
            <ta e="T224" id="Seg_4599" s="T223">v</ta>
            <ta e="T225" id="Seg_4600" s="T224">v</ta>
            <ta e="T226" id="Seg_4601" s="T225">v</ta>
            <ta e="T227" id="Seg_4602" s="T226">v</ta>
            <ta e="T228" id="Seg_4603" s="T227">pers</ta>
            <ta e="T229" id="Seg_4604" s="T228">v</ta>
            <ta e="T230" id="Seg_4605" s="T229">pers</ta>
            <ta e="T231" id="Seg_4606" s="T230">n</ta>
            <ta e="T232" id="Seg_4607" s="T231">v</ta>
            <ta e="T233" id="Seg_4608" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_4609" s="T233">que</ta>
            <ta e="T235" id="Seg_4610" s="T234">v</ta>
            <ta e="T236" id="Seg_4611" s="T235">v</ta>
            <ta e="T237" id="Seg_4612" s="T236">adv</ta>
            <ta e="T238" id="Seg_4613" s="T237">v</ta>
            <ta e="T239" id="Seg_4614" s="T238">v</ta>
            <ta e="T240" id="Seg_4615" s="T239">v</ta>
            <ta e="T241" id="Seg_4616" s="T240">n</ta>
            <ta e="T242" id="Seg_4617" s="T241">n</ta>
            <ta e="T243" id="Seg_4618" s="T242">v</ta>
            <ta e="T244" id="Seg_4619" s="T243">dempro</ta>
            <ta e="T245" id="Seg_4620" s="T244">adv</ta>
            <ta e="T246" id="Seg_4621" s="T245">n</ta>
            <ta e="T247" id="Seg_4622" s="T246">v</ta>
            <ta e="T248" id="Seg_4623" s="T247">aux</ta>
            <ta e="T249" id="Seg_4624" s="T248">v</ta>
            <ta e="T250" id="Seg_4625" s="T249">pers</ta>
            <ta e="T251" id="Seg_4626" s="T250">n</ta>
            <ta e="T252" id="Seg_4627" s="T251">v</ta>
            <ta e="T253" id="Seg_4628" s="T252">pers</ta>
            <ta e="T254" id="Seg_4629" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_4630" s="T254">adv</ta>
            <ta e="T256" id="Seg_4631" s="T255">pers</ta>
            <ta e="T257" id="Seg_4632" s="T256">n</ta>
            <ta e="T258" id="Seg_4633" s="T257">pers</ta>
            <ta e="T259" id="Seg_4634" s="T258">n</ta>
            <ta e="T260" id="Seg_4635" s="T259">pers</ta>
            <ta e="T261" id="Seg_4636" s="T260">v</ta>
            <ta e="T262" id="Seg_4637" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4638" s="T262">aux</ta>
            <ta e="T264" id="Seg_4639" s="T263">v</ta>
            <ta e="T265" id="Seg_4640" s="T264">quant</ta>
            <ta e="T266" id="Seg_4641" s="T265">v</ta>
            <ta e="T267" id="Seg_4642" s="T266">num</ta>
            <ta e="T268" id="Seg_4643" s="T267">v</ta>
            <ta e="T269" id="Seg_4644" s="T268">n</ta>
            <ta e="T270" id="Seg_4645" s="T269">v</ta>
            <ta e="T271" id="Seg_4646" s="T270">v</ta>
            <ta e="T272" id="Seg_4647" s="T271">n</ta>
            <ta e="T273" id="Seg_4648" s="T272">n</ta>
            <ta e="T274" id="Seg_4649" s="T273">v</ta>
            <ta e="T275" id="Seg_4650" s="T274">dempro</ta>
            <ta e="T276" id="Seg_4651" s="T275">propr</ta>
            <ta e="T277" id="Seg_4652" s="T276">v</ta>
            <ta e="T278" id="Seg_4653" s="T277">n</ta>
            <ta e="T279" id="Seg_4654" s="T278">adj</ta>
            <ta e="T280" id="Seg_4655" s="T279">v</ta>
            <ta e="T281" id="Seg_4656" s="T280">v</ta>
            <ta e="T282" id="Seg_4657" s="T281">adv</ta>
            <ta e="T283" id="Seg_4658" s="T282">v</ta>
            <ta e="T284" id="Seg_4659" s="T283">adv</ta>
            <ta e="T285" id="Seg_4660" s="T284">v</ta>
            <ta e="T286" id="Seg_4661" s="T285">n</ta>
            <ta e="T287" id="Seg_4662" s="T286">v</ta>
            <ta e="T288" id="Seg_4663" s="T287">v</ta>
            <ta e="T289" id="Seg_4664" s="T288">adj</ta>
            <ta e="T290" id="Seg_4665" s="T289">n</ta>
            <ta e="T291" id="Seg_4666" s="T290">n</ta>
            <ta e="T292" id="Seg_4667" s="T291">n</ta>
            <ta e="T293" id="Seg_4668" s="T292">n</ta>
            <ta e="T294" id="Seg_4669" s="T293">n</ta>
            <ta e="T295" id="Seg_4670" s="T294">v</ta>
            <ta e="T296" id="Seg_4671" s="T295">n</ta>
            <ta e="T297" id="Seg_4672" s="T296">n</ta>
            <ta e="T299" id="Seg_4673" s="T298">refl</ta>
            <ta e="T300" id="Seg_4674" s="T299">v</ta>
            <ta e="T301" id="Seg_4675" s="T300">n</ta>
            <ta e="T302" id="Seg_4676" s="T301">n</ta>
            <ta e="T303" id="Seg_4677" s="T302">v</ta>
            <ta e="T304" id="Seg_4678" s="T303">num</ta>
            <ta e="T305" id="Seg_4679" s="T304">n</ta>
            <ta e="T306" id="Seg_4680" s="T305">v</ta>
            <ta e="T307" id="Seg_4681" s="T306">v</ta>
            <ta e="T308" id="Seg_4682" s="T307">pers</ta>
            <ta e="T309" id="Seg_4683" s="T308">n</ta>
            <ta e="T310" id="Seg_4684" s="T309">v</ta>
            <ta e="T311" id="Seg_4685" s="T310">n</ta>
            <ta e="T312" id="Seg_4686" s="T311">n</ta>
            <ta e="T313" id="Seg_4687" s="T312">n</ta>
            <ta e="T314" id="Seg_4688" s="T313">v</ta>
            <ta e="T315" id="Seg_4689" s="T314">n</ta>
            <ta e="T316" id="Seg_4690" s="T315">v</ta>
            <ta e="T317" id="Seg_4691" s="T316">v</ta>
            <ta e="T318" id="Seg_4692" s="T317">aux</ta>
            <ta e="T319" id="Seg_4693" s="T318">v</ta>
            <ta e="T320" id="Seg_4694" s="T319">v</ta>
            <ta e="T321" id="Seg_4695" s="T320">dempro</ta>
            <ta e="T322" id="Seg_4696" s="T321">v</ta>
            <ta e="T323" id="Seg_4697" s="T322">refl</ta>
            <ta e="T324" id="Seg_4698" s="T323">v</ta>
            <ta e="T325" id="Seg_4699" s="T324">n</ta>
            <ta e="T326" id="Seg_4700" s="T325">v</ta>
            <ta e="T327" id="Seg_4701" s="T326">n</ta>
            <ta e="T328" id="Seg_4702" s="T327">v</ta>
            <ta e="T329" id="Seg_4703" s="T328">v</ta>
            <ta e="T330" id="Seg_4704" s="T329">v</ta>
            <ta e="T331" id="Seg_4705" s="T330">n</ta>
            <ta e="T332" id="Seg_4706" s="T331">n</ta>
            <ta e="T333" id="Seg_4707" s="T332">v</ta>
            <ta e="T334" id="Seg_4708" s="T333">v</ta>
            <ta e="T335" id="Seg_4709" s="T334">v</ta>
            <ta e="T336" id="Seg_4710" s="T335">propr</ta>
            <ta e="T337" id="Seg_4711" s="T336">propr</ta>
            <ta e="T338" id="Seg_4712" s="T337">n</ta>
            <ta e="T339" id="Seg_4713" s="T338">v</ta>
            <ta e="T340" id="Seg_4714" s="T339">adv</ta>
            <ta e="T341" id="Seg_4715" s="T340">v</ta>
            <ta e="T342" id="Seg_4716" s="T341">v</ta>
            <ta e="T343" id="Seg_4717" s="T342">n</ta>
            <ta e="T344" id="Seg_4718" s="T343">adv</ta>
            <ta e="T345" id="Seg_4719" s="T344">v</ta>
            <ta e="T346" id="Seg_4720" s="T345">dempro</ta>
            <ta e="T347" id="Seg_4721" s="T346">adv</ta>
            <ta e="T348" id="Seg_4722" s="T347">n</ta>
            <ta e="T349" id="Seg_4723" s="T348">v</ta>
            <ta e="T350" id="Seg_4724" s="T349">aux</ta>
            <ta e="T351" id="Seg_4725" s="T350">v</ta>
            <ta e="T352" id="Seg_4726" s="T351">v</ta>
            <ta e="T353" id="Seg_4727" s="T352">n</ta>
            <ta e="T354" id="Seg_4728" s="T353">v</ta>
            <ta e="T355" id="Seg_4729" s="T354">pers</ta>
            <ta e="T356" id="Seg_4730" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_4731" s="T356">adv</ta>
            <ta e="T358" id="Seg_4732" s="T357">pers</ta>
            <ta e="T359" id="Seg_4733" s="T358">n</ta>
            <ta e="T360" id="Seg_4734" s="T359">aux</ta>
            <ta e="T361" id="Seg_4735" s="T360">v</ta>
            <ta e="T362" id="Seg_4736" s="T361">num</ta>
            <ta e="T363" id="Seg_4737" s="T362">dempro</ta>
            <ta e="T364" id="Seg_4738" s="T363">v</ta>
            <ta e="T365" id="Seg_4739" s="T364">v</ta>
            <ta e="T366" id="Seg_4740" s="T365">n</ta>
            <ta e="T367" id="Seg_4741" s="T366">v</ta>
            <ta e="T368" id="Seg_4742" s="T367">v</ta>
            <ta e="T369" id="Seg_4743" s="T368">v</ta>
            <ta e="T370" id="Seg_4744" s="T369">n</ta>
            <ta e="T371" id="Seg_4745" s="T370">adv</ta>
            <ta e="T372" id="Seg_4746" s="T371">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_4747" s="T0">np.h:Th</ta>
            <ta e="T4" id="Seg_4748" s="T3">np.h:Th 0.3.h:Poss</ta>
            <ta e="T6" id="Seg_4749" s="T5">np.h:A 0.3.h:Poss</ta>
            <ta e="T7" id="Seg_4750" s="T6">np.h:R 0.3.h:Poss</ta>
            <ta e="T9" id="Seg_4751" s="T8">pro.h:A</ta>
            <ta e="T11" id="Seg_4752" s="T10">np.h:A 0.3.h:Poss</ta>
            <ta e="T14" id="Seg_4753" s="T13">np:L</ta>
            <ta e="T15" id="Seg_4754" s="T14">np.h:Th</ta>
            <ta e="T17" id="Seg_4755" s="T16">pro.h:A</ta>
            <ta e="T19" id="Seg_4756" s="T18">np.h:Th</ta>
            <ta e="T22" id="Seg_4757" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_4758" s="T22">np.h:E 0.3.h:Poss</ta>
            <ta e="T25" id="Seg_4759" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_4760" s="T25">0.3.h:A</ta>
            <ta e="T28" id="Seg_4761" s="T26">np:L</ta>
            <ta e="T29" id="Seg_4762" s="T28">np:Th</ta>
            <ta e="T31" id="Seg_4763" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_4764" s="T32">np:G</ta>
            <ta e="T34" id="Seg_4765" s="T33">np.h:Th</ta>
            <ta e="T36" id="Seg_4766" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_4767" s="T36">np.h:Th</ta>
            <ta e="T38" id="Seg_4768" s="T37">0.3.h:A</ta>
            <ta e="T41" id="Seg_4769" s="T39">np:G</ta>
            <ta e="T42" id="Seg_4770" s="T41">0.1.h:Poss</ta>
            <ta e="T47" id="Seg_4771" s="T46">np:Th 0.1.h:Poss</ta>
            <ta e="T49" id="Seg_4772" s="T48">pro.h:A</ta>
            <ta e="T51" id="Seg_4773" s="T50">adv:G</ta>
            <ta e="T52" id="Seg_4774" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_4775" s="T53">np:Th</ta>
            <ta e="T56" id="Seg_4776" s="T55">np:P 0.3.h:Poss</ta>
            <ta e="T58" id="Seg_4777" s="T57">0.3.h:A</ta>
            <ta e="T59" id="Seg_4778" s="T58">np:Th 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_4779" s="T61">np:Ins</ta>
            <ta e="T63" id="Seg_4780" s="T62">np:P</ta>
            <ta e="T65" id="Seg_4781" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_4782" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_4783" s="T66">np:G</ta>
            <ta e="T69" id="Seg_4784" s="T68">np:Th</ta>
            <ta e="T70" id="Seg_4785" s="T69">np:G</ta>
            <ta e="T71" id="Seg_4786" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_4787" s="T71">0.3.h:Th</ta>
            <ta e="T73" id="Seg_4788" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_4789" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_4790" s="T74">0.1.h:Poss</ta>
            <ta e="T76" id="Seg_4791" s="T75">0.1.h:A</ta>
            <ta e="T78" id="Seg_4792" s="T77">np.h:A</ta>
            <ta e="T79" id="Seg_4793" s="T78">np:Th 0.3.h:Poss</ta>
            <ta e="T82" id="Seg_4794" s="T80">np:G</ta>
            <ta e="T83" id="Seg_4795" s="T82">0.3.h:A</ta>
            <ta e="T84" id="Seg_4796" s="T83">np:Th 0.3.h:Poss</ta>
            <ta e="T85" id="Seg_4797" s="T84">0.3.h:A</ta>
            <ta e="T86" id="Seg_4798" s="T85">np:Th 0.3.h:Poss</ta>
            <ta e="T87" id="Seg_4799" s="T86">0.3.h:A</ta>
            <ta e="T89" id="Seg_4800" s="T88">np:Th</ta>
            <ta e="T90" id="Seg_4801" s="T89">np:L</ta>
            <ta e="T93" id="Seg_4802" s="T92">pro.h:Poss</ta>
            <ta e="T94" id="Seg_4803" s="T93">np:Th</ta>
            <ta e="T97" id="Seg_4804" s="T96">np:Th</ta>
            <ta e="T102" id="Seg_4805" s="T101">np:Th 0.2.h:Poss</ta>
            <ta e="T104" id="Seg_4806" s="T103">np:Th</ta>
            <ta e="T107" id="Seg_4807" s="T106">np.h:A</ta>
            <ta e="T109" id="Seg_4808" s="T108">np:G</ta>
            <ta e="T110" id="Seg_4809" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_4810" s="T110">np.h:A</ta>
            <ta e="T114" id="Seg_4811" s="T113">0.3.h:A 0.3.h:P</ta>
            <ta e="T117" id="Seg_4812" s="T116">pro.h:Poss</ta>
            <ta e="T118" id="Seg_4813" s="T117">np:Th</ta>
            <ta e="T120" id="Seg_4814" s="T119">pro.h:Poss</ta>
            <ta e="T121" id="Seg_4815" s="T120">np:P</ta>
            <ta e="T122" id="Seg_4816" s="T121">0.3:A</ta>
            <ta e="T123" id="Seg_4817" s="T122">np.h:Th</ta>
            <ta e="T126" id="Seg_4818" s="T125">0.1.h:A</ta>
            <ta e="T128" id="Seg_4819" s="T127">np:Th 0.1.h:Poss</ta>
            <ta e="T130" id="Seg_4820" s="T129">np:Th</ta>
            <ta e="T131" id="Seg_4821" s="T130">0.1.h:A</ta>
            <ta e="T133" id="Seg_4822" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_4823" s="T133">np:P 0.3.h:Poss</ta>
            <ta e="T136" id="Seg_4824" s="T135">0.3.h:A</ta>
            <ta e="T137" id="Seg_4825" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_4826" s="T137">np:Th</ta>
            <ta e="T140" id="Seg_4827" s="T139">0.3.h:A</ta>
            <ta e="T142" id="Seg_4828" s="T141">np:G</ta>
            <ta e="T143" id="Seg_4829" s="T142">np.h:Th</ta>
            <ta e="T144" id="Seg_4830" s="T143">np.h:Th</ta>
            <ta e="T146" id="Seg_4831" s="T145">np:Th 0.3.h:Poss</ta>
            <ta e="T147" id="Seg_4832" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_4833" s="T147">0.3.h:A</ta>
            <ta e="T151" id="Seg_4834" s="T150">pro.h:A</ta>
            <ta e="T152" id="Seg_4835" s="T151">np.h:Th 0.1.h:Poss</ta>
            <ta e="T153" id="Seg_4836" s="T152">np.h:Th 0.1.h:Poss</ta>
            <ta e="T155" id="Seg_4837" s="T154">0.1.h:A</ta>
            <ta e="T156" id="Seg_4838" s="T155">np.h:A 0.1.h:Poss</ta>
            <ta e="T158" id="Seg_4839" s="T157">np:Th</ta>
            <ta e="T161" id="Seg_4840" s="T160">0.2.h:A 0.3:Th</ta>
            <ta e="T162" id="Seg_4841" s="T161">pro.h:Poss</ta>
            <ta e="T163" id="Seg_4842" s="T162">np:G</ta>
            <ta e="T166" id="Seg_4843" s="T165">np.h:Th 0.2.h:Poss</ta>
            <ta e="T167" id="Seg_4844" s="T166">pro.h:Poss</ta>
            <ta e="T168" id="Seg_4845" s="T167">np:A</ta>
            <ta e="T169" id="Seg_4846" s="T168">pro.h:Poss</ta>
            <ta e="T170" id="Seg_4847" s="T169">np:P</ta>
            <ta e="T173" id="Seg_4848" s="T172">0.3:A</ta>
            <ta e="T176" id="Seg_4849" s="T175">0.2.h:A 0.3:P</ta>
            <ta e="T177" id="Seg_4850" s="T176">np.h:A</ta>
            <ta e="T181" id="Seg_4851" s="T180">adv:Time</ta>
            <ta e="T183" id="Seg_4852" s="T182">np.h:A</ta>
            <ta e="T184" id="Seg_4853" s="T183">np:P 0.3.h:Poss</ta>
            <ta e="T189" id="Seg_4854" s="T188">0.3.h:A</ta>
            <ta e="T190" id="Seg_4855" s="T189">0.3.h:A</ta>
            <ta e="T192" id="Seg_4856" s="T191">np:Poss</ta>
            <ta e="T193" id="Seg_4857" s="T192">np:Th</ta>
            <ta e="T194" id="Seg_4858" s="T193">np:Ins</ta>
            <ta e="T195" id="Seg_4859" s="T194">0.3.h:A</ta>
            <ta e="T196" id="Seg_4860" s="T195">pro.h:A</ta>
            <ta e="T199" id="Seg_4861" s="T198">adv:Time</ta>
            <ta e="T200" id="Seg_4862" s="T199">np.h:A</ta>
            <ta e="T201" id="Seg_4863" s="T200">np.h:Com 0.3.h:Poss</ta>
            <ta e="T203" id="Seg_4864" s="T202">np.h:A</ta>
            <ta e="T205" id="Seg_4865" s="T204">np:G</ta>
            <ta e="T206" id="Seg_4866" s="T205">0.3.h:E</ta>
            <ta e="T207" id="Seg_4867" s="T206">np:Th</ta>
            <ta e="T210" id="Seg_4868" s="T209">np:R 0.3.h:Poss</ta>
            <ta e="T211" id="Seg_4869" s="T210">0.3.h:A</ta>
            <ta e="T216" id="Seg_4870" s="T215">np.h:Poss</ta>
            <ta e="T217" id="Seg_4871" s="T216">np:P</ta>
            <ta e="T218" id="Seg_4872" s="T217">pro.h:Poss</ta>
            <ta e="T219" id="Seg_4873" s="T218">np:A</ta>
            <ta e="T222" id="Seg_4874" s="T221">np.h:A</ta>
            <ta e="T226" id="Seg_4875" s="T225">0.3.h:A</ta>
            <ta e="T227" id="Seg_4876" s="T226">0.3.h:A</ta>
            <ta e="T228" id="Seg_4877" s="T227">pro.h:A</ta>
            <ta e="T230" id="Seg_4878" s="T229">pro.h:R</ta>
            <ta e="T231" id="Seg_4879" s="T230">np:P</ta>
            <ta e="T232" id="Seg_4880" s="T231">0.3:A</ta>
            <ta e="T236" id="Seg_4881" s="T235">0.3:A 0.3:P</ta>
            <ta e="T237" id="Seg_4882" s="T236">adv:Time</ta>
            <ta e="T240" id="Seg_4883" s="T239">0.3.h:A</ta>
            <ta e="T241" id="Seg_4884" s="T240">np.h:A</ta>
            <ta e="T242" id="Seg_4885" s="T241">np:So</ta>
            <ta e="T243" id="Seg_4886" s="T242">0.3.h:P</ta>
            <ta e="T244" id="Seg_4887" s="T243">pro.h:A</ta>
            <ta e="T245" id="Seg_4888" s="T244">adv:Time</ta>
            <ta e="T246" id="Seg_4889" s="T245">np:G</ta>
            <ta e="T248" id="Seg_4890" s="T247">0.2.h:A</ta>
            <ta e="T250" id="Seg_4891" s="T249">pro.h:A</ta>
            <ta e="T251" id="Seg_4892" s="T250">np:Th 0.1.h:Poss</ta>
            <ta e="T253" id="Seg_4893" s="T252">pro.h:Th</ta>
            <ta e="T256" id="Seg_4894" s="T255">pro.h:Poss</ta>
            <ta e="T257" id="Seg_4895" s="T256">np:Th</ta>
            <ta e="T258" id="Seg_4896" s="T257">pro.h:Poss</ta>
            <ta e="T259" id="Seg_4897" s="T258">np.h:A</ta>
            <ta e="T260" id="Seg_4898" s="T259">pro.h:R</ta>
            <ta e="T263" id="Seg_4899" s="T262">0.2.h:A</ta>
            <ta e="T266" id="Seg_4900" s="T265">0.1.h:A</ta>
            <ta e="T267" id="Seg_4901" s="T266">np:Th</ta>
            <ta e="T268" id="Seg_4902" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_4903" s="T268">np:P 0.3.h:Poss</ta>
            <ta e="T270" id="Seg_4904" s="T269">0.3.h:A</ta>
            <ta e="T271" id="Seg_4905" s="T270">0.3.h:A</ta>
            <ta e="T273" id="Seg_4906" s="T272">np:Th</ta>
            <ta e="T275" id="Seg_4907" s="T274">np:P</ta>
            <ta e="T276" id="Seg_4908" s="T275">np.h:A</ta>
            <ta e="T278" id="Seg_4909" s="T277">np:Th</ta>
            <ta e="T280" id="Seg_4910" s="T279">0.3.h:A</ta>
            <ta e="T281" id="Seg_4911" s="T280">0.3.h:A</ta>
            <ta e="T282" id="Seg_4912" s="T281">adv:Time</ta>
            <ta e="T283" id="Seg_4913" s="T282">0.3.h:A</ta>
            <ta e="T285" id="Seg_4914" s="T284">0.3.h:A</ta>
            <ta e="T286" id="Seg_4915" s="T285">np:Th</ta>
            <ta e="T288" id="Seg_4916" s="T287">0.3.h:A</ta>
            <ta e="T290" id="Seg_4917" s="T289">np:Th</ta>
            <ta e="T291" id="Seg_4918" s="T290">np:Poss</ta>
            <ta e="T292" id="Seg_4919" s="T291">np:G</ta>
            <ta e="T293" id="Seg_4920" s="T292">np:P</ta>
            <ta e="T294" id="Seg_4921" s="T293">np:G 0.3:Poss</ta>
            <ta e="T295" id="Seg_4922" s="T294">0.3.h:A</ta>
            <ta e="T296" id="Seg_4923" s="T295">np:Ins 0.3.h:Poss</ta>
            <ta e="T297" id="Seg_4924" s="T296">np:G 0.3:Poss</ta>
            <ta e="T298" id="Seg_4925" s="T297">0.3.h:A</ta>
            <ta e="T299" id="Seg_4926" s="T298">pro.h:A</ta>
            <ta e="T301" id="Seg_4927" s="T300">np:G</ta>
            <ta e="T302" id="Seg_4928" s="T301">np.h:Th</ta>
            <ta e="T305" id="Seg_4929" s="T304">np.h:Th 0.3.h:Poss</ta>
            <ta e="T307" id="Seg_4930" s="T306">0.1.h:A</ta>
            <ta e="T308" id="Seg_4931" s="T307">pro.h:Poss</ta>
            <ta e="T309" id="Seg_4932" s="T308">np.h:Th</ta>
            <ta e="T311" id="Seg_4933" s="T310">np:Poss</ta>
            <ta e="T312" id="Seg_4934" s="T311">np:L</ta>
            <ta e="T313" id="Seg_4935" s="T312">np:P 0.3:Poss</ta>
            <ta e="T315" id="Seg_4936" s="T314">np.h:Th</ta>
            <ta e="T317" id="Seg_4937" s="T316">0.1.h:A</ta>
            <ta e="T318" id="Seg_4938" s="T317">0.2.h:A</ta>
            <ta e="T320" id="Seg_4939" s="T319">0.2.h:A</ta>
            <ta e="T321" id="Seg_4940" s="T320">pro.h:A</ta>
            <ta e="T322" id="Seg_4941" s="T321">0.3.h:E</ta>
            <ta e="T323" id="Seg_4942" s="T322">pro.h:P</ta>
            <ta e="T325" id="Seg_4943" s="T324">0.3.h:A</ta>
            <ta e="T327" id="Seg_4944" s="T326">np:E</ta>
            <ta e="T329" id="Seg_4945" s="T328">0.3:A</ta>
            <ta e="T330" id="Seg_4946" s="T329">0.3.h:E</ta>
            <ta e="T331" id="Seg_4947" s="T330">np:Th</ta>
            <ta e="T332" id="Seg_4948" s="T331">np:L 0.3:Poss</ta>
            <ta e="T335" id="Seg_4949" s="T334">0.3.h:A</ta>
            <ta e="T336" id="Seg_4950" s="T335">np.h:Th</ta>
            <ta e="T337" id="Seg_4951" s="T336">np.h:Th</ta>
            <ta e="T338" id="Seg_4952" s="T337">np.h:A 0.2.h:Poss</ta>
            <ta e="T340" id="Seg_4953" s="T339">adv:Time</ta>
            <ta e="T342" id="Seg_4954" s="T341">0.3.h:A</ta>
            <ta e="T343" id="Seg_4955" s="T342">np:So</ta>
            <ta e="T345" id="Seg_4956" s="T344">0.3.h:A 0.3.h:P</ta>
            <ta e="T346" id="Seg_4957" s="T345">pro.h:A</ta>
            <ta e="T347" id="Seg_4958" s="T346">adv:Time</ta>
            <ta e="T348" id="Seg_4959" s="T347">np:G</ta>
            <ta e="T350" id="Seg_4960" s="T349">0.2.h:A</ta>
            <ta e="T352" id="Seg_4961" s="T351">0.3.h:A</ta>
            <ta e="T353" id="Seg_4962" s="T352">np.h:Th</ta>
            <ta e="T354" id="Seg_4963" s="T353">0.3.h:A</ta>
            <ta e="T355" id="Seg_4964" s="T354">pro.h:Th</ta>
            <ta e="T358" id="Seg_4965" s="T357">pro.h:Poss</ta>
            <ta e="T359" id="Seg_4966" s="T358">np.h:Th</ta>
            <ta e="T360" id="Seg_4967" s="T359">0.2.h:A</ta>
            <ta e="T363" id="Seg_4968" s="T362">pro.h:Th 0.1.h:Poss</ta>
            <ta e="T364" id="Seg_4969" s="T363">0.1.h:A</ta>
            <ta e="T365" id="Seg_4970" s="T364">0.3.h:A</ta>
            <ta e="T366" id="Seg_4971" s="T365">np.h:P 0.3.h:Poss</ta>
            <ta e="T369" id="Seg_4972" s="T368">0.3.h:Poss</ta>
            <ta e="T370" id="Seg_4973" s="T369">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_4974" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_4975" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_4976" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_4977" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_4978" s="T5">np:S</ta>
            <ta e="T8" id="Seg_4979" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_4980" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_4981" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_4982" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_4983" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_4984" s="T12">ptcl:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_4985" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_4986" s="T15">ptcl:pred</ta>
            <ta e="T17" id="Seg_4987" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_4988" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_4989" s="T18">s:purp</ta>
            <ta e="T22" id="Seg_4990" s="T21">v:pred 0.3.h:S</ta>
            <ta e="T23" id="Seg_4991" s="T22">np.h:S</ta>
            <ta e="T24" id="Seg_4992" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_4993" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_4994" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T29" id="Seg_4995" s="T28">np:S</ta>
            <ta e="T30" id="Seg_4996" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_4997" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_4998" s="T33">np:S</ta>
            <ta e="T35" id="Seg_4999" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_5000" s="T35">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_5001" s="T36">np:O</ta>
            <ta e="T38" id="Seg_5002" s="T37">v:pred 0.3.h:S</ta>
            <ta e="T39" id="Seg_5003" s="T38">v:pred 0.3.h:S</ta>
            <ta e="T43" id="Seg_5004" s="T42">np:O</ta>
            <ta e="T44" id="Seg_5005" s="T43">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_5006" s="T44">np:O</ta>
            <ta e="T46" id="Seg_5007" s="T45">v:pred 0.2.h:S</ta>
            <ta e="T47" id="Seg_5008" s="T46">np:S</ta>
            <ta e="T48" id="Seg_5009" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_5010" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_5011" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_5012" s="T51">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_5013" s="T53">np:O</ta>
            <ta e="T55" id="Seg_5014" s="T54">v:pred 0.3.h:S</ta>
            <ta e="T56" id="Seg_5015" s="T55">np:O</ta>
            <ta e="T58" id="Seg_5016" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_5017" s="T58">np:S</ta>
            <ta e="T60" id="Seg_5018" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_5019" s="T62">np:O</ta>
            <ta e="T65" id="Seg_5020" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T66" id="Seg_5021" s="T65">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_5022" s="T68">np:O</ta>
            <ta e="T71" id="Seg_5023" s="T70">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_5024" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T73" id="Seg_5025" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_5026" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_5027" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T78" id="Seg_5028" s="T77">np:S</ta>
            <ta e="T79" id="Seg_5029" s="T78">np:O</ta>
            <ta e="T80" id="Seg_5030" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_5031" s="T82">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_5032" s="T83">np:O</ta>
            <ta e="T85" id="Seg_5033" s="T84">v:pred 0.3.h:S</ta>
            <ta e="T86" id="Seg_5034" s="T85">np:O</ta>
            <ta e="T87" id="Seg_5035" s="T86">v:pred 0.3.h:S</ta>
            <ta e="T89" id="Seg_5036" s="T88">np:S</ta>
            <ta e="T91" id="Seg_5037" s="T90">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_5038" s="T91">n:pred</ta>
            <ta e="T94" id="Seg_5039" s="T93">np:S</ta>
            <ta e="T97" id="Seg_5040" s="T96">np:S</ta>
            <ta e="T99" id="Seg_5041" s="T98">v:pred</ta>
            <ta e="T101" id="Seg_5042" s="T100">n:pred</ta>
            <ta e="T102" id="Seg_5043" s="T101">np:S</ta>
            <ta e="T104" id="Seg_5044" s="T103">np:S</ta>
            <ta e="T106" id="Seg_5045" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_5046" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_5047" s="T107">v:pred</ta>
            <ta e="T110" id="Seg_5048" s="T109">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_5049" s="T110">np:S</ta>
            <ta e="T112" id="Seg_5050" s="T111">v:pred</ta>
            <ta e="T114" id="Seg_5051" s="T113">v:pred 0.3.h:S 0.3.h:O</ta>
            <ta e="T115" id="Seg_5052" s="T114">v:pred 0.2.h:S</ta>
            <ta e="T118" id="Seg_5053" s="T117">np:S</ta>
            <ta e="T119" id="Seg_5054" s="T118">v:pred</ta>
            <ta e="T121" id="Seg_5055" s="T120">np:O</ta>
            <ta e="T122" id="Seg_5056" s="T121">v:pred 0.3:S</ta>
            <ta e="T123" id="Seg_5057" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_5058" s="T123">v:pred 0.2.h:S</ta>
            <ta e="T126" id="Seg_5059" s="T125">v:pred 0.1.h:S</ta>
            <ta e="T128" id="Seg_5060" s="T127">np:S</ta>
            <ta e="T129" id="Seg_5061" s="T128">v:pred</ta>
            <ta e="T130" id="Seg_5062" s="T129">pro:O</ta>
            <ta e="T131" id="Seg_5063" s="T130">v:pred 0.1.h:S</ta>
            <ta e="T133" id="Seg_5064" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_5065" s="T133">np:O</ta>
            <ta e="T136" id="Seg_5066" s="T135">v:pred 0.3.h:S</ta>
            <ta e="T137" id="Seg_5067" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_5068" s="T137">np:S</ta>
            <ta e="T139" id="Seg_5069" s="T138">v:pred</ta>
            <ta e="T140" id="Seg_5070" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T143" id="Seg_5071" s="T142">np:S</ta>
            <ta e="T144" id="Seg_5072" s="T143">np:S</ta>
            <ta e="T145" id="Seg_5073" s="T144">v:pred</ta>
            <ta e="T146" id="Seg_5074" s="T145">np:O</ta>
            <ta e="T147" id="Seg_5075" s="T146">v:pred 0.3.h:S</ta>
            <ta e="T148" id="Seg_5076" s="T147">v:pred 0.3.h:S</ta>
            <ta e="T150" id="Seg_5077" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_5078" s="T150">pro.h:S</ta>
            <ta e="T154" id="Seg_5079" s="T153">s:purp</ta>
            <ta e="T155" id="Seg_5080" s="T154">v:pred 0.1.h:S</ta>
            <ta e="T156" id="Seg_5081" s="T155">np.h:S</ta>
            <ta e="T158" id="Seg_5082" s="T157">np:O</ta>
            <ta e="T159" id="Seg_5083" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_5084" s="T160">v:pred 0.2.h:S 0.3:O</ta>
            <ta e="T165" id="Seg_5085" s="T164">v:pred 0.2.h:S</ta>
            <ta e="T166" id="Seg_5086" s="T165">np:O</ta>
            <ta e="T168" id="Seg_5087" s="T167">np:S</ta>
            <ta e="T170" id="Seg_5088" s="T169">np:O</ta>
            <ta e="T171" id="Seg_5089" s="T170">v:pred</ta>
            <ta e="T173" id="Seg_5090" s="T172">v:pred 0.3:S 0.3:O</ta>
            <ta e="T176" id="Seg_5091" s="T175">v:pred 0.2.h:S</ta>
            <ta e="T177" id="Seg_5092" s="T176">np:S</ta>
            <ta e="T178" id="Seg_5093" s="T177">s:adv</ta>
            <ta e="T180" id="Seg_5094" s="T179">v:pred</ta>
            <ta e="T182" id="Seg_5095" s="T181">v:pred</ta>
            <ta e="T183" id="Seg_5096" s="T182">np:S</ta>
            <ta e="T184" id="Seg_5097" s="T183">np:O</ta>
            <ta e="T187" id="Seg_5098" s="T186">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_5099" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_5100" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T193" id="Seg_5101" s="T192">np:O</ta>
            <ta e="T195" id="Seg_5102" s="T194">v:pred 0.3.h:S 0.3:O</ta>
            <ta e="T196" id="Seg_5103" s="T195">pro.h:S</ta>
            <ta e="T197" id="Seg_5104" s="T196">s:purp</ta>
            <ta e="T198" id="Seg_5105" s="T197">v:pred</ta>
            <ta e="T200" id="Seg_5106" s="T199">np:S</ta>
            <ta e="T202" id="Seg_5107" s="T201">v:pred</ta>
            <ta e="T206" id="Seg_5108" s="T205">s:temp</ta>
            <ta e="T207" id="Seg_5109" s="T206">np:S</ta>
            <ta e="T209" id="Seg_5110" s="T208">conv:pred</ta>
            <ta e="T211" id="Seg_5111" s="T210">s:temp</ta>
            <ta e="T212" id="Seg_5112" s="T211">v:pred 0.3.h:S</ta>
            <ta e="T217" id="Seg_5113" s="T216">np:O</ta>
            <ta e="T219" id="Seg_5114" s="T218">np:S</ta>
            <ta e="T221" id="Seg_5115" s="T220">v:pred</ta>
            <ta e="T222" id="Seg_5116" s="T221">np:S</ta>
            <ta e="T224" id="Seg_5117" s="T223">v:pred</ta>
            <ta e="T226" id="Seg_5118" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T227" id="Seg_5119" s="T226">v:pred 0.3.h:S</ta>
            <ta e="T228" id="Seg_5120" s="T227">pro.h:S</ta>
            <ta e="T229" id="Seg_5121" s="T228">v:pred</ta>
            <ta e="T231" id="Seg_5122" s="T230">np:O</ta>
            <ta e="T232" id="Seg_5123" s="T231">v:pred 0.3:O</ta>
            <ta e="T236" id="Seg_5124" s="T235">v:pred 0.3:S 0.3:O</ta>
            <ta e="T238" id="Seg_5125" s="T237">s:adv</ta>
            <ta e="T240" id="Seg_5126" s="T239">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_5127" s="T240">np.h:S</ta>
            <ta e="T243" id="Seg_5128" s="T242">v:pred</ta>
            <ta e="T244" id="Seg_5129" s="T243">pro.h:S</ta>
            <ta e="T247" id="Seg_5130" s="T246">v:pred</ta>
            <ta e="T248" id="Seg_5131" s="T247">v:pred 0.2.h:S</ta>
            <ta e="T250" id="Seg_5132" s="T249">pro.h:S</ta>
            <ta e="T251" id="Seg_5133" s="T250">np:O</ta>
            <ta e="T252" id="Seg_5134" s="T251">v:pred</ta>
            <ta e="T254" id="Seg_5135" s="T253">ptcl.neg</ta>
            <ta e="T255" id="Seg_5136" s="T254">ptcl:pred</ta>
            <ta e="T257" id="Seg_5137" s="T256">np:S</ta>
            <ta e="T259" id="Seg_5138" s="T258">np:S</ta>
            <ta e="T261" id="Seg_5139" s="T260">v:pred</ta>
            <ta e="T263" id="Seg_5140" s="T262">v:pred 0.2.h:S</ta>
            <ta e="T266" id="Seg_5141" s="T265">v:pred 0.1.h:S</ta>
            <ta e="T267" id="Seg_5142" s="T266">np:O</ta>
            <ta e="T268" id="Seg_5143" s="T267">v:pred 0.3.h:S</ta>
            <ta e="T269" id="Seg_5144" s="T268">np:O</ta>
            <ta e="T270" id="Seg_5145" s="T269">v:pred 0.3.h:S</ta>
            <ta e="T271" id="Seg_5146" s="T270">v:pred 0.3.h:S</ta>
            <ta e="T273" id="Seg_5147" s="T272">np:S</ta>
            <ta e="T274" id="Seg_5148" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_5149" s="T274">pro:O</ta>
            <ta e="T276" id="Seg_5150" s="T275">np:S</ta>
            <ta e="T277" id="Seg_5151" s="T276">v:pred</ta>
            <ta e="T278" id="Seg_5152" s="T277">np:O</ta>
            <ta e="T280" id="Seg_5153" s="T279">v:pred 0.3.h:S</ta>
            <ta e="T281" id="Seg_5154" s="T280">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_5155" s="T282">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_5156" s="T284">v:pred 0.3.h:S</ta>
            <ta e="T286" id="Seg_5157" s="T285">np:S</ta>
            <ta e="T287" id="Seg_5158" s="T286">v:pred</ta>
            <ta e="T288" id="Seg_5159" s="T287">v:pred 0.3.h:S</ta>
            <ta e="T290" id="Seg_5160" s="T289">np:O</ta>
            <ta e="T293" id="Seg_5161" s="T292">np:O</ta>
            <ta e="T295" id="Seg_5162" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T296" id="Seg_5163" s="T295">np:O</ta>
            <ta e="T298" id="Seg_5164" s="T297">v:pred 0.3.h:S</ta>
            <ta e="T299" id="Seg_5165" s="T298">pro.h:S</ta>
            <ta e="T300" id="Seg_5166" s="T299">v:pred</ta>
            <ta e="T302" id="Seg_5167" s="T301">np.h:S</ta>
            <ta e="T303" id="Seg_5168" s="T302">v:pred</ta>
            <ta e="T305" id="Seg_5169" s="T304">np.h:S</ta>
            <ta e="T306" id="Seg_5170" s="T305">v:pred</ta>
            <ta e="T307" id="Seg_5171" s="T306">v:pred 0.1.h:S</ta>
            <ta e="T309" id="Seg_5172" s="T308">np:S</ta>
            <ta e="T310" id="Seg_5173" s="T309">v:pred</ta>
            <ta e="T313" id="Seg_5174" s="T312">np:O</ta>
            <ta e="T314" id="Seg_5175" s="T313">v:pred 0.3.h:S</ta>
            <ta e="T315" id="Seg_5176" s="T314">np.h:S</ta>
            <ta e="T317" id="Seg_5177" s="T316">v:pred 0.1.h:S</ta>
            <ta e="T318" id="Seg_5178" s="T317">v:pred 0.2.h:S</ta>
            <ta e="T320" id="Seg_5179" s="T319">v:pred 0.2.h:S</ta>
            <ta e="T321" id="Seg_5180" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_5181" s="T321">s:cond</ta>
            <ta e="T323" id="Seg_5182" s="T322">pro.h:O</ta>
            <ta e="T324" id="Seg_5183" s="T323">v:pred</ta>
            <ta e="T325" id="Seg_5184" s="T324">np.h:S</ta>
            <ta e="T326" id="Seg_5185" s="T325">v:pred</ta>
            <ta e="T327" id="Seg_5186" s="T326">np:S</ta>
            <ta e="T328" id="Seg_5187" s="T327">v:pred</ta>
            <ta e="T329" id="Seg_5188" s="T328">v:pred 0.3:S</ta>
            <ta e="T330" id="Seg_5189" s="T329">s:temp</ta>
            <ta e="T331" id="Seg_5190" s="T330">np:S</ta>
            <ta e="T333" id="Seg_5191" s="T332">v:pred</ta>
            <ta e="T334" id="Seg_5192" s="T333">s:adv</ta>
            <ta e="T335" id="Seg_5193" s="T334">v:pred 0.3.h:S</ta>
            <ta e="T338" id="Seg_5194" s="T337">np.h:S</ta>
            <ta e="T339" id="Seg_5195" s="T338">v:pred</ta>
            <ta e="T341" id="Seg_5196" s="T340">s:adv</ta>
            <ta e="T342" id="Seg_5197" s="T341">v:pred 0.3.h:S</ta>
            <ta e="T345" id="Seg_5198" s="T344">v:pred 0.3.h:S 0.3.h:O</ta>
            <ta e="T346" id="Seg_5199" s="T345">pro.h:S</ta>
            <ta e="T349" id="Seg_5200" s="T348">v:pred</ta>
            <ta e="T351" id="Seg_5201" s="T350">v:pred 0.2.h:S</ta>
            <ta e="T352" id="Seg_5202" s="T351">v:pred 0.3.h:S</ta>
            <ta e="T353" id="Seg_5203" s="T352">np.h:O</ta>
            <ta e="T354" id="Seg_5204" s="T353">v:pred 0.1.h:S</ta>
            <ta e="T356" id="Seg_5205" s="T355">ptcl.neg</ta>
            <ta e="T357" id="Seg_5206" s="T356">ptcl:pred</ta>
            <ta e="T359" id="Seg_5207" s="T358">np:S</ta>
            <ta e="T361" id="Seg_5208" s="T360">v:pred 0.2.h:S</ta>
            <ta e="T363" id="Seg_5209" s="T362">pro.h:O</ta>
            <ta e="T364" id="Seg_5210" s="T363">v:pred 0.1.h:S</ta>
            <ta e="T365" id="Seg_5211" s="T364">v:pred 0.3.h:S</ta>
            <ta e="T366" id="Seg_5212" s="T365">np:O</ta>
            <ta e="T367" id="Seg_5213" s="T366">v:pred 0.3.h:S</ta>
            <ta e="T369" id="Seg_5214" s="T368">v:pred 0.3.h:S</ta>
            <ta e="T370" id="Seg_5215" s="T369">np.h:S</ta>
            <ta e="T372" id="Seg_5216" s="T371">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_5217" s="T0">new</ta>
            <ta e="T4" id="Seg_5218" s="T3">new</ta>
            <ta e="T6" id="Seg_5219" s="T5">giv-active</ta>
            <ta e="T7" id="Seg_5220" s="T6">giv-active</ta>
            <ta e="T8" id="Seg_5221" s="T7">quot-sp</ta>
            <ta e="T9" id="Seg_5222" s="T8">giv-active-Q</ta>
            <ta e="T10" id="Seg_5223" s="T9">new-Q</ta>
            <ta e="T11" id="Seg_5224" s="T10">giv-inactive</ta>
            <ta e="T12" id="Seg_5225" s="T11">quot-sp</ta>
            <ta e="T14" id="Seg_5226" s="T13">accs-gen-Q</ta>
            <ta e="T15" id="Seg_5227" s="T14">accs-sit-Q</ta>
            <ta e="T17" id="Seg_5228" s="T16">giv-inactive-Q</ta>
            <ta e="T19" id="Seg_5229" s="T18">accs-sit-Q</ta>
            <ta e="T22" id="Seg_5230" s="T21">0.giv-active</ta>
            <ta e="T23" id="Seg_5231" s="T22">giv-inactive</ta>
            <ta e="T24" id="Seg_5232" s="T23">0.giv-active</ta>
            <ta e="T25" id="Seg_5233" s="T24">0.giv-active</ta>
            <ta e="T26" id="Seg_5234" s="T25">0.giv-active</ta>
            <ta e="T28" id="Seg_5235" s="T27">accs-gen</ta>
            <ta e="T29" id="Seg_5236" s="T28">new</ta>
            <ta e="T31" id="Seg_5237" s="T30">0.giv-active</ta>
            <ta e="T33" id="Seg_5238" s="T32">giv-active</ta>
            <ta e="T34" id="Seg_5239" s="T33">new</ta>
            <ta e="T36" id="Seg_5240" s="T35">0.giv-active</ta>
            <ta e="T37" id="Seg_5241" s="T36">accs-inf</ta>
            <ta e="T38" id="Seg_5242" s="T37">0.giv-active</ta>
            <ta e="T39" id="Seg_5243" s="T38">0.giv-active</ta>
            <ta e="T41" id="Seg_5244" s="T40">accs-sit</ta>
            <ta e="T42" id="Seg_5245" s="T41">giv-inactive-Q</ta>
            <ta e="T43" id="Seg_5246" s="T42">accs-sit-Q</ta>
            <ta e="T45" id="Seg_5247" s="T44">new-Q</ta>
            <ta e="T47" id="Seg_5248" s="T46">giv-active-Q</ta>
            <ta e="T49" id="Seg_5249" s="T48">giv-inactive-Q</ta>
            <ta e="T51" id="Seg_5250" s="T50">accs-gen</ta>
            <ta e="T52" id="Seg_5251" s="T51">0.giv-inactive</ta>
            <ta e="T54" id="Seg_5252" s="T53">new</ta>
            <ta e="T55" id="Seg_5253" s="T54">0.giv-inactive</ta>
            <ta e="T56" id="Seg_5254" s="T55">accs-inf</ta>
            <ta e="T58" id="Seg_5255" s="T57">0.giv-active</ta>
            <ta e="T59" id="Seg_5256" s="T58">accs-inf</ta>
            <ta e="T62" id="Seg_5257" s="T61">giv-active</ta>
            <ta e="T63" id="Seg_5258" s="T62">giv-inactive</ta>
            <ta e="T65" id="Seg_5259" s="T64">0.giv-inactive</ta>
            <ta e="T66" id="Seg_5260" s="T65">0.giv-active</ta>
            <ta e="T67" id="Seg_5261" s="T66">giv-inactive</ta>
            <ta e="T69" id="Seg_5262" s="T68">giv-active</ta>
            <ta e="T70" id="Seg_5263" s="T69">giv-inactive</ta>
            <ta e="T71" id="Seg_5264" s="T70">0.giv-active</ta>
            <ta e="T72" id="Seg_5265" s="T71">accs-aggr</ta>
            <ta e="T73" id="Seg_5266" s="T72">giv-active</ta>
            <ta e="T74" id="Seg_5267" s="T73">quot-sp</ta>
            <ta e="T75" id="Seg_5268" s="T74">giv-inactive-Q</ta>
            <ta e="T76" id="Seg_5269" s="T75">giv-inactive-Q</ta>
            <ta e="T78" id="Seg_5270" s="T77">giv-active</ta>
            <ta e="T79" id="Seg_5271" s="T78">giv-inactive</ta>
            <ta e="T82" id="Seg_5272" s="T81">giv-inactive</ta>
            <ta e="T83" id="Seg_5273" s="T82">0.giv-active</ta>
            <ta e="T84" id="Seg_5274" s="T83">accs-sit</ta>
            <ta e="T85" id="Seg_5275" s="T84">0.giv-active</ta>
            <ta e="T86" id="Seg_5276" s="T85">giv-inactive</ta>
            <ta e="T87" id="Seg_5277" s="T86">0.giv-active</ta>
            <ta e="T89" id="Seg_5278" s="T88">giv-inactive</ta>
            <ta e="T90" id="Seg_5279" s="T89">giv-active</ta>
            <ta e="T94" id="Seg_5280" s="T93">giv-active-Q</ta>
            <ta e="T97" id="Seg_5281" s="T96">giv-inactive-Q</ta>
            <ta e="T100" id="Seg_5282" s="T99">giv-inactive-Q</ta>
            <ta e="T102" id="Seg_5283" s="T101">giv-inactive-Q</ta>
            <ta e="T104" id="Seg_5284" s="T103">giv-inactive-Q</ta>
            <ta e="T107" id="Seg_5285" s="T106">giv-inactive</ta>
            <ta e="T109" id="Seg_5286" s="T108">giv-inactive</ta>
            <ta e="T110" id="Seg_5287" s="T109">0.giv-active</ta>
            <ta e="T111" id="Seg_5288" s="T110">giv-inactive</ta>
            <ta e="T114" id="Seg_5289" s="T113">0.giv-active 0.giv-active</ta>
            <ta e="T115" id="Seg_5290" s="T114">0.giv-active-Q</ta>
            <ta e="T118" id="Seg_5291" s="T117">giv-inactive-Q</ta>
            <ta e="T121" id="Seg_5292" s="T120">giv-inactive-Q</ta>
            <ta e="T123" id="Seg_5293" s="T122">giv-inactive</ta>
            <ta e="T124" id="Seg_5294" s="T123">0.giv-active-Q</ta>
            <ta e="T126" id="Seg_5295" s="T125">0.giv-inactive-Q</ta>
            <ta e="T128" id="Seg_5296" s="T127">new-Q</ta>
            <ta e="T130" id="Seg_5297" s="T129">giv-active-Q</ta>
            <ta e="T131" id="Seg_5298" s="T130">0.giv-inactive-Q</ta>
            <ta e="T133" id="Seg_5299" s="T132">0.giv-inactive</ta>
            <ta e="T134" id="Seg_5300" s="T133">giv-active</ta>
            <ta e="T136" id="Seg_5301" s="T135">0.giv-active</ta>
            <ta e="T137" id="Seg_5302" s="T136">0.giv-active</ta>
            <ta e="T138" id="Seg_5303" s="T137">new</ta>
            <ta e="T140" id="Seg_5304" s="T139">0.giv-active</ta>
            <ta e="T142" id="Seg_5305" s="T141">giv-active</ta>
            <ta e="T143" id="Seg_5306" s="T142">new</ta>
            <ta e="T144" id="Seg_5307" s="T143">new</ta>
            <ta e="T146" id="Seg_5308" s="T145">accs-inf</ta>
            <ta e="T147" id="Seg_5309" s="T146">0.giv-inactive</ta>
            <ta e="T148" id="Seg_5310" s="T147">0.giv-inactive quot-sp</ta>
            <ta e="T151" id="Seg_5311" s="T150">giv-inactive-Q</ta>
            <ta e="T152" id="Seg_5312" s="T151">giv-inactive-Q</ta>
            <ta e="T153" id="Seg_5313" s="T152">giv-active-Q</ta>
            <ta e="T155" id="Seg_5314" s="T154">0.giv-inactive-Q</ta>
            <ta e="T156" id="Seg_5315" s="T155">giv-active-Q</ta>
            <ta e="T158" id="Seg_5316" s="T157">giv-inactive-Q</ta>
            <ta e="T161" id="Seg_5317" s="T160">0.giv-active-Q</ta>
            <ta e="T163" id="Seg_5318" s="T162">new-Q</ta>
            <ta e="T165" id="Seg_5319" s="T164">0.giv-active-Q</ta>
            <ta e="T166" id="Seg_5320" s="T165">giv-inactive-Q</ta>
            <ta e="T168" id="Seg_5321" s="T167">giv-inactive-Q</ta>
            <ta e="T170" id="Seg_5322" s="T169">giv-active-Q</ta>
            <ta e="T173" id="Seg_5323" s="T172">0.giv-active-Q</ta>
            <ta e="T176" id="Seg_5324" s="T175">0.giv-inactive-Q</ta>
            <ta e="T177" id="Seg_5325" s="T176">giv-inactive</ta>
            <ta e="T183" id="Seg_5326" s="T182">giv-inactive</ta>
            <ta e="T184" id="Seg_5327" s="T183">giv-inactive</ta>
            <ta e="T187" id="Seg_5328" s="T186">0.giv-active</ta>
            <ta e="T189" id="Seg_5329" s="T188">0.giv-active</ta>
            <ta e="T190" id="Seg_5330" s="T189">0.giv-active</ta>
            <ta e="T193" id="Seg_5331" s="T192">new</ta>
            <ta e="T194" id="Seg_5332" s="T193">accs-sit</ta>
            <ta e="T195" id="Seg_5333" s="T194">0.giv-active</ta>
            <ta e="T196" id="Seg_5334" s="T195">giv-active</ta>
            <ta e="T199" id="Seg_5335" s="T198">accs-gen</ta>
            <ta e="T200" id="Seg_5336" s="T199">giv-inactive</ta>
            <ta e="T201" id="Seg_5337" s="T200">giv-inactive</ta>
            <ta e="T203" id="Seg_5338" s="T202">giv-active</ta>
            <ta e="T205" id="Seg_5339" s="T204">new</ta>
            <ta e="T206" id="Seg_5340" s="T205">0.giv-active</ta>
            <ta e="T207" id="Seg_5341" s="T206">giv-inactive</ta>
            <ta e="T210" id="Seg_5342" s="T209">giv-inactive</ta>
            <ta e="T212" id="Seg_5343" s="T211">0.giv-active</ta>
            <ta e="T217" id="Seg_5344" s="T216">giv-inactive-Q</ta>
            <ta e="T219" id="Seg_5345" s="T218">giv-inactive-Q</ta>
            <ta e="T222" id="Seg_5346" s="T221">giv-inactive</ta>
            <ta e="T226" id="Seg_5347" s="T225">0.giv-active</ta>
            <ta e="T227" id="Seg_5348" s="T226">0.giv-active quot-sp</ta>
            <ta e="T228" id="Seg_5349" s="T227">giv-active-Q</ta>
            <ta e="T230" id="Seg_5350" s="T229">giv-inactive-Q</ta>
            <ta e="T231" id="Seg_5351" s="T230">giv-inactive-Q</ta>
            <ta e="T232" id="Seg_5352" s="T231">0.giv-inactive-Q</ta>
            <ta e="T236" id="Seg_5353" s="T235">giv-active-Q</ta>
            <ta e="T240" id="Seg_5354" s="T239">giv-inactive</ta>
            <ta e="T241" id="Seg_5355" s="T240">giv-active</ta>
            <ta e="T242" id="Seg_5356" s="T241">accs-sit</ta>
            <ta e="T244" id="Seg_5357" s="T243">giv-active</ta>
            <ta e="T246" id="Seg_5358" s="T245">giv-active</ta>
            <ta e="T248" id="Seg_5359" s="T247">0.giv-active-Q</ta>
            <ta e="T250" id="Seg_5360" s="T249">giv-inactive-Q</ta>
            <ta e="T251" id="Seg_5361" s="T250">new-Q</ta>
            <ta e="T253" id="Seg_5362" s="T252">giv-inactive-Q</ta>
            <ta e="T257" id="Seg_5363" s="T256">giv-active-Q</ta>
            <ta e="T259" id="Seg_5364" s="T258">giv-inactive-Q</ta>
            <ta e="T260" id="Seg_5365" s="T259">giv-active-Q</ta>
            <ta e="T263" id="Seg_5366" s="T262">0.giv-active-Q</ta>
            <ta e="T266" id="Seg_5367" s="T265">0.giv-active-Q</ta>
            <ta e="T267" id="Seg_5368" s="T266">accs-sit-Q</ta>
            <ta e="T268" id="Seg_5369" s="T267">0.giv-active</ta>
            <ta e="T269" id="Seg_5370" s="T268">giv-active</ta>
            <ta e="T270" id="Seg_5371" s="T269">0.giv-active</ta>
            <ta e="T271" id="Seg_5372" s="T270">0.giv-active</ta>
            <ta e="T273" id="Seg_5373" s="T271">new</ta>
            <ta e="T275" id="Seg_5374" s="T274">giv-active</ta>
            <ta e="T276" id="Seg_5375" s="T275">giv-inactive</ta>
            <ta e="T278" id="Seg_5376" s="T277">new</ta>
            <ta e="T279" id="Seg_5377" s="T278">new</ta>
            <ta e="T280" id="Seg_5378" s="T279">0.giv-active</ta>
            <ta e="T281" id="Seg_5379" s="T280">0.giv-active</ta>
            <ta e="T283" id="Seg_5380" s="T282">0.giv-active</ta>
            <ta e="T285" id="Seg_5381" s="T284">0.giv-active</ta>
            <ta e="T286" id="Seg_5382" s="T285">new</ta>
            <ta e="T288" id="Seg_5383" s="T287">0.giv-inactive</ta>
            <ta e="T290" id="Seg_5384" s="T289">giv-inactive</ta>
            <ta e="T292" id="Seg_5385" s="T291">new</ta>
            <ta e="T293" id="Seg_5386" s="T292">giv-inactive</ta>
            <ta e="T294" id="Seg_5387" s="T293">accs-inf</ta>
            <ta e="T295" id="Seg_5388" s="T294">0.giv-active</ta>
            <ta e="T296" id="Seg_5389" s="T295">new</ta>
            <ta e="T297" id="Seg_5390" s="T296">accs-inf</ta>
            <ta e="T298" id="Seg_5391" s="T297">0.giv-active</ta>
            <ta e="T299" id="Seg_5392" s="T298">giv-active</ta>
            <ta e="T301" id="Seg_5393" s="T300">giv-inactive</ta>
            <ta e="T302" id="Seg_5394" s="T301">new</ta>
            <ta e="T305" id="Seg_5395" s="T304">new</ta>
            <ta e="T307" id="Seg_5396" s="T306">accs-aggr-Q</ta>
            <ta e="T309" id="Seg_5397" s="T308">new-Q</ta>
            <ta e="T312" id="Seg_5398" s="T311">giv-inactive-Q</ta>
            <ta e="T313" id="Seg_5399" s="T312">giv-inactive-Q</ta>
            <ta e="T314" id="Seg_5400" s="T313">0.giv-active-Q</ta>
            <ta e="T315" id="Seg_5401" s="T314">giv-inactive</ta>
            <ta e="T317" id="Seg_5402" s="T316">0.giv-active-Q</ta>
            <ta e="T318" id="Seg_5403" s="T317">0.giv-active-Q</ta>
            <ta e="T319" id="Seg_5404" s="T318">0.giv-active-Q</ta>
            <ta e="T320" id="Seg_5405" s="T319">0.giv-active-Q</ta>
            <ta e="T321" id="Seg_5406" s="T320">giv-inactive-Q</ta>
            <ta e="T323" id="Seg_5407" s="T322">giv-active-Q</ta>
            <ta e="T325" id="Seg_5408" s="T324">giv-inactive</ta>
            <ta e="T327" id="Seg_5409" s="T326">giv-inactive</ta>
            <ta e="T329" id="Seg_5410" s="T328">giv-active</ta>
            <ta e="T330" id="Seg_5411" s="T329">0.giv-inactive</ta>
            <ta e="T331" id="Seg_5412" s="T330">accs-sit</ta>
            <ta e="T332" id="Seg_5413" s="T331">accs-inf</ta>
            <ta e="T335" id="Seg_5414" s="T334">0.giv-inactive</ta>
            <ta e="T336" id="Seg_5415" s="T335">giv-inactive-Q</ta>
            <ta e="T337" id="Seg_5416" s="T336">giv-inactive-Q</ta>
            <ta e="T338" id="Seg_5417" s="T337">giv-inactive-Q</ta>
            <ta e="T342" id="Seg_5418" s="T341">0.giv-active</ta>
            <ta e="T343" id="Seg_5419" s="T342">accs-sit</ta>
            <ta e="T345" id="Seg_5420" s="T344">0.giv-inactive</ta>
            <ta e="T346" id="Seg_5421" s="T345">giv-active</ta>
            <ta e="T348" id="Seg_5422" s="T347">giv-active</ta>
            <ta e="T350" id="Seg_5423" s="T349">0.giv-active-Q</ta>
            <ta e="T351" id="Seg_5424" s="T350">0.giv-active-Q</ta>
            <ta e="T352" id="Seg_5425" s="T351">0.giv-inactive quot-sp</ta>
            <ta e="T353" id="Seg_5426" s="T352">giv-inactive-Q</ta>
            <ta e="T354" id="Seg_5427" s="T353">0.giv-active-Q</ta>
            <ta e="T355" id="Seg_5428" s="T354">giv-inactive-Q</ta>
            <ta e="T359" id="Seg_5429" s="T358">giv-active-Q</ta>
            <ta e="T360" id="Seg_5430" s="T359">0.giv-active-Q</ta>
            <ta e="T361" id="Seg_5431" s="T360">0.giv-active-Q</ta>
            <ta e="T363" id="Seg_5432" s="T362">giv-inactive-Q</ta>
            <ta e="T364" id="Seg_5433" s="T363">0.giv-active-Q</ta>
            <ta e="T365" id="Seg_5434" s="T364">0.giv-inactive</ta>
            <ta e="T366" id="Seg_5435" s="T365">giv-active</ta>
            <ta e="T367" id="Seg_5436" s="T366">0.giv-active</ta>
            <ta e="T369" id="Seg_5437" s="T368">0.giv-active</ta>
            <ta e="T370" id="Seg_5438" s="T369">giv-inactive</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T64" id="Seg_5439" s="T63">TURK:disc</ta>
            <ta e="T73" id="Seg_5440" s="T72">%TURK:core</ta>
            <ta e="T101" id="Seg_5441" s="T100">TURK:mod(PTCL)</ta>
            <ta e="T174" id="Seg_5442" s="T173">RUS:disc</ta>
            <ta e="T262" id="Seg_5443" s="T261">RUS:disc</ta>
            <ta e="T265" id="Seg_5444" s="T264">TURK:disc</ta>
            <ta e="T284" id="Seg_5445" s="T283">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_5446" s="T0">Жила женщина, был у нее один сын.</ta>
            <ta e="T8" id="Seg_5447" s="T5">Сын говорит матери: </ta>
            <ta e="T10" id="Seg_5448" s="T8">"Я женюсь."</ta>
            <ta e="T12" id="Seg_5449" s="T10">Его мать говорит:</ta>
            <ta e="T16" id="Seg_5450" s="T12">"Нету, в округе ни одной женщины нет."</ta>
            <ta e="T20" id="Seg_5451" s="T16">"Пойду себе женщину искать."</ta>
            <ta e="T24" id="Seg_5452" s="T20">Он ушёл, мать осталась.</ta>
            <ta e="T26" id="Seg_5453" s="T24">Идёт он, идёт.</ta>
            <ta e="T30" id="Seg_5454" s="T26">На берегу реки чум стоит.</ta>
            <ta e="T35" id="Seg_5455" s="T30">Вошёл он в этот чум, в нём женщина живет.</ta>
            <ta e="T38" id="Seg_5456" s="T35">Поклонился, руку ей подал. </ta>
            <ta e="T41" id="Seg_5457" s="T38">Сел у огня.</ta>
            <ta e="T42" id="Seg_5458" s="T41">"Тётушка!</ta>
            <ta e="T46" id="Seg_5459" s="T42">Повесь котёл на огонь, мяса свари!"</ta>
            <ta e="T48" id="Seg_5460" s="T46">"Нет у меня мяса."</ta>
            <ta e="T50" id="Seg_5461" s="T48">"Я принесу."</ta>
            <ta e="T55" id="Seg_5462" s="T50">Вышел он, взял лопатку без мяса.</ta>
            <ta e="T58" id="Seg_5463" s="T55">Разбил себе нос.</ta>
            <ta e="T60" id="Seg_5464" s="T58">Потекла у него кровь.</ta>
            <ta e="T65" id="Seg_5465" s="T60">Кровью своей всю эту кость обмазал.</ta>
            <ta e="T71" id="Seg_5466" s="T65">Вошёл в чум, кость в котёл засунул.</ta>
            <ta e="T73" id="Seg_5467" s="T71">Сидят, разговаривают.</ta>
            <ta e="T74" id="Seg_5468" s="T73">Он говорит: </ta>
            <ta e="T76" id="Seg_5469" s="T74">"Тётушка, давай поедим!"</ta>
            <ta e="T85" id="Seg_5470" s="T76">Женщина котел взяла, у огня поставила, половник взяла.</ta>
            <ta e="T91" id="Seg_5471" s="T85">Мясо мешает, а в котле только лопатка болтается.</ta>
            <ta e="T94" id="Seg_5472" s="T91">"Где твоё мясо?"</ta>
            <ta e="T99" id="Seg_5473" s="T94">Кость одна болтается.</ta>
            <ta e="T102" id="Seg_5474" s="T99">"Кёчун-Гюдюр, где твоё мясо?</ta>
            <ta e="T106" id="Seg_5475" s="T102">Кость одна болтается."</ta>
            <ta e="T108" id="Seg_5476" s="T106">Кёчун-Гюдюр заплакал.</ta>
            <ta e="T110" id="Seg_5477" s="T108">В огонь бросается.</ta>
            <ta e="T113" id="Seg_5478" s="T110">Женщина его обратно тащит.</ta>
            <ta e="T114" id="Seg_5479" s="T113">Вытащить пытается.</ta>
            <ta e="T116" id="Seg_5480" s="T114">"Не плачь!"</ta>
            <ta e="T122" id="Seg_5481" s="T116">"Котел твой выкипел и мясо моё съел."</ta>
            <ta e="T123" id="Seg_5482" s="T122">Женщина:</ta>
            <ta e="T125" id="Seg_5483" s="T123">"Не плачь!</ta>
            <ta e="T126" id="Seg_5484" s="T125">Дам я [тебе кое-что].</ta>
            <ta e="T129" id="Seg_5485" s="T126">Есть у меня одна овца.</ta>
            <ta e="T131" id="Seg_5486" s="T129">Отдам её [тебе]."</ta>
            <ta e="T136" id="Seg_5487" s="T131">Тогда он помылся, овцу взял на привязь.</ta>
            <ta e="T137" id="Seg_5488" s="T136">Идёт.</ta>
            <ta e="T139" id="Seg_5489" s="T137">Стоит чум.</ta>
            <ta e="T142" id="Seg_5490" s="T139">Зашёл в этот чум.</ta>
            <ta e="T145" id="Seg_5491" s="T142">В нём жена с мужем живут.</ta>
            <ta e="T147" id="Seg_5492" s="T145">Руку им подал. </ta>
            <ta e="T148" id="Seg_5493" s="T147">[Муж] говорит:</ta>
            <ta e="T151" id="Seg_5494" s="T148">"Куда ты идёшь?"</ta>
            <ta e="T155" id="Seg_5495" s="T151">"К тёще в гости ходил.</ta>
            <ta e="T159" id="Seg_5496" s="T155">Тёща мне единственную овцу отдала."</ta>
            <ta e="T163" id="Seg_5497" s="T159">"Оставь её с нашими овцами.</ta>
            <ta e="T166" id="Seg_5498" s="T163">Развяжи свою овцу!"</ta>
            <ta e="T171" id="Seg_5499" s="T166">"Ваши овцы мою сожрут."</ta>
            <ta e="T173" id="Seg_5500" s="T171">"Зачем им её есть?"</ta>
            <ta e="T176" id="Seg_5501" s="T173">"Нy, тогда пусти её!"</ta>
            <ta e="T180" id="Seg_5502" s="T176">Муж пустил её [к другим].</ta>
            <ta e="T183" id="Seg_5503" s="T180">Ночью Кёчун-Гюдюр встал.</ta>
            <ta e="T189" id="Seg_5504" s="T183">Разорвал на части свою собственную овцу.</ta>
            <ta e="T195" id="Seg_5505" s="T189">Морды половине овец кровью измазал.</ta>
            <ta e="T198" id="Seg_5506" s="T195">Сам лёг спать.</ta>
            <ta e="T202" id="Seg_5507" s="T198">Наутро встали муж с женой.</ta>
            <ta e="T205" id="Seg_5508" s="T202">Пошёл муж в загон.</ta>
            <ta e="T209" id="Seg_5509" s="T205">Увидел разорванную овцу.</ta>
            <ta e="T212" id="Seg_5510" s="T209">Прибежал к жене и говорит:</ta>
            <ta e="T221" id="Seg_5511" s="T212">"Надо же, наши овцы овцу этого парня разорвали."</ta>
            <ta e="T226" id="Seg_5512" s="T221">Кёчун-Гюдюр услышал, вскочил.</ta>
            <ta e="T227" id="Seg_5513" s="T226">Говорит:</ta>
            <ta e="T232" id="Seg_5514" s="T227">"Говорил я вам, сожрут они мою овцу.</ta>
            <ta e="T236" id="Seg_5515" s="T232">И вправду сожрали."</ta>
            <ta e="T240" id="Seg_5516" s="T236">Вновь, рыдая, [в огонь] бросается.</ta>
            <ta e="T243" id="Seg_5517" s="T240">Муж его из огня вытаскивает.</ta>
            <ta e="T247" id="Seg_5518" s="T243">А тот снова в огонь бросается.</ta>
            <ta e="T249" id="Seg_5519" s="T247">"Не плачь!</ta>
            <ta e="T252" id="Seg_5520" s="T249">Дам я тебе овцу."</ta>
            <ta e="T257" id="Seg_5521" s="T252">"Не надо мне твоей овцы.</ta>
            <ta e="T261" id="Seg_5522" s="T257">Моя тёща мне дала одну."</ta>
            <ta e="T264" id="Seg_5523" s="T261">"Ну же, не плачь!</ta>
            <ta e="T267" id="Seg_5524" s="T264">Отдам тебе все пять."</ta>
            <ta e="T270" id="Seg_5525" s="T267">Помылся тот, привязал своих овец.</ta>
            <ta e="T271" id="Seg_5526" s="T270">Идёт.</ta>
            <ta e="T274" id="Seg_5527" s="T271">Крест деревянный стоит.</ta>
            <ta e="T280" id="Seg_5528" s="T274">Выкопал его Кёчун-Гюдюр, достал [труп] старухи.</ta>
            <ta e="T281" id="Seg_5529" s="T280">Погрузил её.</ta>
            <ta e="T285" id="Seg_5530" s="T281">Снова немного прошёл. </ta>
            <ta e="T287" id="Seg_5531" s="T285">Стоит чум.</ta>
            <ta e="T295" id="Seg_5532" s="T287">Усадил мёртвую старуху под дерево, овец к её руке привязал.</ta>
            <ta e="T298" id="Seg_5533" s="T295">В сердце ей свой нож воткнул.</ta>
            <ta e="T301" id="Seg_5534" s="T298">Сам в чум вошёл.</ta>
            <ta e="T306" id="Seg_5535" s="T301">В нём человек живёт с двумя дочерьми.</ta>
            <ta e="T307" id="Seg_5536" s="T306">"Поешь с нами!"</ta>
            <ta e="T310" id="Seg_5537" s="T307">"У меня жена есть.</ta>
            <ta e="T314" id="Seg_5538" s="T310">Она под деревом овец пасёт."</ta>
            <ta e="T315" id="Seg_5539" s="T314">Дочери: </ta>
            <ta e="T317" id="Seg_5540" s="T315">"Мы её позовем."</ta>
            <ta e="T319" id="Seg_5541" s="T317">"Не ходите!</ta>
            <ta e="T320" id="Seg_5542" s="T319">Напугаете её,</ta>
            <ta e="T324" id="Seg_5543" s="T320">она сама себя от испуга заколет."</ta>
            <ta e="T326" id="Seg_5544" s="T324">Выбежали дочери.</ta>
            <ta e="T329" id="Seg_5545" s="T326">Овцы испугались, побежали.</ta>
            <ta e="T333" id="Seg_5546" s="T329">[Дочери] увидели, что у неё в груди нож торчит.</ta>
            <ta e="T335" id="Seg_5547" s="T333">Прибежали:</ta>
            <ta e="T339" id="Seg_5548" s="T335">"Кёчун-Гюдюр, Кёчун-Гюдюр, твоя жена сама себя заколола!"</ta>
            <ta e="T342" id="Seg_5549" s="T339">Бросился он снова [в огонь], рыдая.</ta>
            <ta e="T345" id="Seg_5550" s="T342">[Старик] его из огня вытаскивает.</ta>
            <ta e="T349" id="Seg_5551" s="T345">А тот снова в огонь бросается. </ta>
            <ta e="T351" id="Seg_5552" s="T349">"Не плачь!", </ta>
            <ta e="T352" id="Seg_5553" s="T351">говорит он ему.</ta>
            <ta e="T354" id="Seg_5554" s="T352">"Дочь свою тебе отдам."</ta>
            <ta e="T359" id="Seg_5555" s="T354">"Не нужна мне твоя дочь."</ta>
            <ta e="T361" id="Seg_5556" s="T359">"Не плачь!</ta>
            <ta e="T364" id="Seg_5557" s="T361">Обеих тебе отдам."</ta>
            <ta e="T369" id="Seg_5558" s="T364">Помылся тот, привязал жён своих и ушёл.</ta>
            <ta e="T372" id="Seg_5559" s="T369">Старик один остался.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_5560" s="T0">There lived a woman, she had one son.</ta>
            <ta e="T8" id="Seg_5561" s="T5">The son says to his mother:</ta>
            <ta e="T10" id="Seg_5562" s="T8">"I will take a wife."</ta>
            <ta e="T12" id="Seg_5563" s="T10">His mother says:</ta>
            <ta e="T16" id="Seg_5564" s="T12">"There is none, there is no woman around."</ta>
            <ta e="T20" id="Seg_5565" s="T16">"I will go find a woman."</ta>
            <ta e="T24" id="Seg_5566" s="T20">He went off, left his mother.</ta>
            <ta e="T26" id="Seg_5567" s="T24">He walks and walks.</ta>
            <ta e="T30" id="Seg_5568" s="T26">There’s a tent standing on the shore of the river.</ta>
            <ta e="T35" id="Seg_5569" s="T30">He entered this tent, a woman lives there.</ta>
            <ta e="T38" id="Seg_5570" s="T35">He bowed, shook hands.</ta>
            <ta e="T41" id="Seg_5571" s="T38">He sat down at the fire.</ta>
            <ta e="T42" id="Seg_5572" s="T41">"My aunt!</ta>
            <ta e="T46" id="Seg_5573" s="T42">Hang up the cauldron, cook meat!"</ta>
            <ta e="T48" id="Seg_5574" s="T46">"I have no meat."</ta>
            <ta e="T50" id="Seg_5575" s="T48">"I will bring some."</ta>
            <ta e="T55" id="Seg_5576" s="T50">He got outside, took a bare blade bone.</ta>
            <ta e="T58" id="Seg_5577" s="T55">He smashed his own nose.</ta>
            <ta e="T60" id="Seg_5578" s="T58">His blood started running.</ta>
            <ta e="T65" id="Seg_5579" s="T60">With this blood he coated all of the blade bone.</ta>
            <ta e="T71" id="Seg_5580" s="T65">He entered the tent and [instead of meat] put this bladebone into the cauldron.</ta>
            <ta e="T73" id="Seg_5581" s="T71">They sit and talk.</ta>
            <ta e="T74" id="Seg_5582" s="T73">He says:</ta>
            <ta e="T76" id="Seg_5583" s="T74">"Aunt, let’s eat!"</ta>
            <ta e="T85" id="Seg_5584" s="T76">The woman took her cauldron, placed it at the side of her fire, and took her scoop.</ta>
            <ta e="T91" id="Seg_5585" s="T85">She’s scooping her meat, but only a bone is clattering in the cauldron.</ta>
            <ta e="T94" id="Seg_5586" s="T91">"Where is your meat?</ta>
            <ta e="T99" id="Seg_5587" s="T94">A single bare bone is rolling around.</ta>
            <ta e="T102" id="Seg_5588" s="T99">Kochun Gudur, where is your meat?</ta>
            <ta e="T106" id="Seg_5589" s="T102">A single bare bone is rolling around."</ta>
            <ta e="T108" id="Seg_5590" s="T106">Kochun Gudur started crying.</ta>
            <ta e="T110" id="Seg_5591" s="T108">He kept rolling into the fire.</ta>
            <ta e="T113" id="Seg_5592" s="T110">The woman kept taking him out.</ta>
            <ta e="T114" id="Seg_5593" s="T113">She is pulling him out.</ta>
            <ta e="T116" id="Seg_5594" s="T114">"Don’t cry!"</ta>
            <ta e="T122" id="Seg_5595" s="T116">"Your cauldron dried up and ate my meat."</ta>
            <ta e="T123" id="Seg_5596" s="T122">The woman:</ta>
            <ta e="T125" id="Seg_5597" s="T123">"Don't cry!</ta>
            <ta e="T126" id="Seg_5598" s="T125">I give [you something].</ta>
            <ta e="T129" id="Seg_5599" s="T126">I have a single sheep.</ta>
            <ta e="T131" id="Seg_5600" s="T129">This one I will give [to you]."</ta>
            <ta e="T136" id="Seg_5601" s="T131">Then he washed himself and tethered his sheep.</ta>
            <ta e="T137" id="Seg_5602" s="T136">He walks.</ta>
            <ta e="T139" id="Seg_5603" s="T137">A tent is standing (there).</ta>
            <ta e="T142" id="Seg_5604" s="T139">He entered this tent.</ta>
            <ta e="T145" id="Seg_5605" s="T142">A woman and a man live there.</ta>
            <ta e="T147" id="Seg_5606" s="T145">He shook hands.</ta>
            <ta e="T148" id="Seg_5607" s="T147">He [the man?] says:</ta>
            <ta e="T151" id="Seg_5608" s="T148">"Where are you going?"</ta>
            <ta e="T155" id="Seg_5609" s="T151">"I visited my mother-in-law.</ta>
            <ta e="T159" id="Seg_5610" s="T155">My mother-in-law gave me this single sheep."</ta>
            <ta e="T163" id="Seg_5611" s="T159">"Let it among our sheep.</ta>
            <ta e="T166" id="Seg_5612" s="T163">Let your sheep go."</ta>
            <ta e="T171" id="Seg_5613" s="T166">‎‎"Your sheep will eat up my sheep."</ta>
            <ta e="T173" id="Seg_5614" s="T171">"Why would they eat it?"</ta>
            <ta e="T176" id="Seg_5615" s="T173">‎‎"Well then, let it go!"</ta>
            <ta e="T180" id="Seg_5616" s="T176">The man led it away and let it go.</ta>
            <ta e="T183" id="Seg_5617" s="T180">At night Kochun Gudur got up.</ta>
            <ta e="T189" id="Seg_5618" s="T183">He tore his own sheep in pieces.</ta>
            <ta e="T195" id="Seg_5619" s="T189">He [took] half of the sheep’ noses and smeared them with blood.</ta>
            <ta e="T198" id="Seg_5620" s="T195">He himself lay down to sleep.</ta>
            <ta e="T202" id="Seg_5621" s="T198">In the morning the man and his wife got up.</ta>
            <ta e="T205" id="Seg_5622" s="T202">The man went to the corral.</ta>
            <ta e="T209" id="Seg_5623" s="T205">[In his] seeing a sheep is torn into pieces.</ta>
            <ta e="T212" id="Seg_5624" s="T209">Coming back he told his wife:</ta>
            <ta e="T221" id="Seg_5625" s="T212">"Really, why, our sheep have torn this boy’s sheep into pieces!"</ta>
            <ta e="T226" id="Seg_5626" s="T221">Kochun Gudur heard it and jumped up.</ta>
            <ta e="T227" id="Seg_5627" s="T226">He says:</ta>
            <ta e="T232" id="Seg_5628" s="T227">‎‎"I told you they will eat my sheep.</ta>
            <ta e="T236" id="Seg_5629" s="T232">Really, why, they have eaten it up."</ta>
            <ta e="T240" id="Seg_5630" s="T236">Again crying he starts rolling [into the fire].</ta>
            <ta e="T243" id="Seg_5631" s="T240">The man keeps taking him out of the fire.</ta>
            <ta e="T247" id="Seg_5632" s="T243">​He rolls again into the fire.</ta>
            <ta e="T249" id="Seg_5633" s="T247">"Don’t cry!</ta>
            <ta e="T252" id="Seg_5634" s="T249">I’ll give (you) a sheep."</ta>
            <ta e="T257" id="Seg_5635" s="T252">"I don’t need your sheep.</ta>
            <ta e="T261" id="Seg_5636" s="T257">My mother-in-law gave me one."</ta>
            <ta e="T264" id="Seg_5637" s="T261">"Come on, don’t cry!</ta>
            <ta e="T267" id="Seg_5638" s="T264">I will give you all five."</ta>
            <ta e="T270" id="Seg_5639" s="T267">He scrubbed himself and tethered his sheep.</ta>
            <ta e="T271" id="Seg_5640" s="T270">He walks.</ta>
            <ta e="T274" id="Seg_5641" s="T271">There stands a wooden cross.</ta>
            <ta e="T280" id="Seg_5642" s="T274">Kochun Gudur dug it out, took out an old woman[’s dead body].</ta>
            <ta e="T281" id="Seg_5643" s="T280">Loaded it up.</ta>
            <ta e="T285" id="Seg_5644" s="T281">Again he walked a bit.</ta>
            <ta e="T287" id="Seg_5645" s="T285">A tent stands.</ta>
            <ta e="T295" id="Seg_5646" s="T287">He seated the dead woman at the trunk of a tree and tied the sheep to her hand.</ta>
            <ta e="T298" id="Seg_5647" s="T295">He pushed his knife into her heart.</ta>
            <ta e="T301" id="Seg_5648" s="T298">He himself went to the tent.</ta>
            <ta e="T306" id="Seg_5649" s="T301">A man and his two daughters live there.</ta>
            <ta e="T307" id="Seg_5650" s="T306">"Let’s eat!"</ta>
            <ta e="T310" id="Seg_5651" s="T307">"I have a wife.</ta>
            <ta e="T314" id="Seg_5652" s="T310">She’s holding her sheep at the trunk of the tree."</ta>
            <ta e="T315" id="Seg_5653" s="T314">The daughters:</ta>
            <ta e="T317" id="Seg_5654" s="T315">"We’ll go and invite her."</ta>
            <ta e="T319" id="Seg_5655" s="T317">"Don’t go!</ta>
            <ta e="T320" id="Seg_5656" s="T319">You will frighten her.</ta>
            <ta e="T324" id="Seg_5657" s="T320">In being terrified she’ll stab herself."</ta>
            <ta e="T326" id="Seg_5658" s="T324">The girls ran off.</ta>
            <ta e="T329" id="Seg_5659" s="T326">The sheep got frightened and started galloping.</ta>
            <ta e="T333" id="Seg_5660" s="T329">As they see, a knife is being stuck in her heart.</ta>
            <ta e="T335" id="Seg_5661" s="T333">They came running.</ta>
            <ta e="T339" id="Seg_5662" s="T335">"Kochun Gudur, Kochun Gudur, your wife has stabbed herself."</ta>
            <ta e="T342" id="Seg_5663" s="T339">Again he rolls [into the fire] and cries.</ta>
            <ta e="T345" id="Seg_5664" s="T342">[The man] takes him out of the fire.</ta>
            <ta e="T349" id="Seg_5665" s="T345">He dives again into the fire.</ta>
            <ta e="T351" id="Seg_5666" s="T349">"Don’t cry!</ta>
            <ta e="T352" id="Seg_5667" s="T351">He says.</ta>
            <ta e="T354" id="Seg_5668" s="T352">"I’ll give you my daughter.‎‎"</ta>
            <ta e="T359" id="Seg_5669" s="T354">"I don’t need your daughter.‎‎"</ta>
            <ta e="T361" id="Seg_5670" s="T359">"Don’t cry!</ta>
            <ta e="T364" id="Seg_5671" s="T361">I’ll give you both."</ta>
            <ta e="T369" id="Seg_5672" s="T364">He scrubbed himself, tethered his wives, and went off.</ta>
            <ta e="T372" id="Seg_5673" s="T369">The man stayed alone.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_5674" s="T0">Es lebte eine alte Frau, die hatte einen Sohn.</ta>
            <ta e="T8" id="Seg_5675" s="T5">Der Sohn sagt zu seiner Mutter: </ta>
            <ta e="T10" id="Seg_5676" s="T8">„Ich werde mir eine Frau nehmen.“</ta>
            <ta e="T12" id="Seg_5677" s="T10">Seine Mutter sagt: </ta>
            <ta e="T16" id="Seg_5678" s="T12">„Es gibt in der Gegend keine Frau, es gibt keine.“</ta>
            <ta e="T20" id="Seg_5679" s="T16">„Ich werde mir eine Frau suchen gehen.“</ta>
            <ta e="T24" id="Seg_5680" s="T20">Er ging davon und ließ seine Mutter zurück.</ta>
            <ta e="T26" id="Seg_5681" s="T24">Er wandert und wandert.</ta>
            <ta e="T30" id="Seg_5682" s="T26">Am Flussufer steht ein Zelt.</ta>
            <ta e="T35" id="Seg_5683" s="T30">Er betrat das Zelt, eine Frau wohnt darin.</ta>
            <ta e="T38" id="Seg_5684" s="T35">Er verbeugte sich und gab ihr die Hand.</ta>
            <ta e="T41" id="Seg_5685" s="T38">Er setzte sich an das Feuer.</ta>
            <ta e="T42" id="Seg_5686" s="T41">„Tante!</ta>
            <ta e="T46" id="Seg_5687" s="T42">Häng einen Kessel auf und koche Fleisch.“</ta>
            <ta e="T48" id="Seg_5688" s="T46">„Ich habe kein Fleisch.“</ta>
            <ta e="T50" id="Seg_5689" s="T48">„Ich besorge welches.“</ta>
            <ta e="T55" id="Seg_5690" s="T50">Er ging hinaus und nahm einen blanken Schulterknochen.</ta>
            <ta e="T58" id="Seg_5691" s="T55">Er zerschmetterte seine Nase.</ta>
            <ta e="T60" id="Seg_5692" s="T58">Sein Blut fing an zu fließen.</ta>
            <ta e="T65" id="Seg_5693" s="T60">Mit diesem Blut beschmierte er den gesamten Schulterknochen.</ta>
            <ta e="T71" id="Seg_5694" s="T65">Er ging in das Zelt und warf den Schulterknochen in den Kessel.</ta>
            <ta e="T73" id="Seg_5695" s="T71">Sie sitzen und unterhalten sich.</ta>
            <ta e="T74" id="Seg_5696" s="T73">Er sagt:</ta>
            <ta e="T76" id="Seg_5697" s="T74">„Tante, lass uns essen!“</ta>
            <ta e="T85" id="Seg_5698" s="T76">Die Frau nahm ihren Kessel, stellte ihn neben das Feuer und nahm eine Kelle.</ta>
            <ta e="T91" id="Seg_5699" s="T85">Sie schöpft nach dem Fleisch, im Kessel klappert nur ein Knochen.</ta>
            <ta e="T94" id="Seg_5700" s="T91">„Wo ist dein Fleisch?</ta>
            <ta e="T99" id="Seg_5701" s="T94">Im Kessel dreht sich nur ein einziger blanker Knochen.</ta>
            <ta e="T102" id="Seg_5702" s="T99">Kötschün Güdür, wo ist dein Fleisch?</ta>
            <ta e="T106" id="Seg_5703" s="T102">Im Kessel dreht sich nur ein einziger Knochen.“</ta>
            <ta e="T108" id="Seg_5704" s="T106">Kötschün Güdür fing an zu weinen.</ta>
            <ta e="T110" id="Seg_5705" s="T108">Er wälzte sich in das Feuer.</ta>
            <ta e="T113" id="Seg_5706" s="T110">Die Frau holte ihn heraus.</ta>
            <ta e="T114" id="Seg_5707" s="T113">Sie zog ihn weg.</ta>
            <ta e="T116" id="Seg_5708" s="T114">„Weine nicht!“</ta>
            <ta e="T122" id="Seg_5709" s="T116">„Dein Kessel ist trocken geworden und fraß mein Fleisch auf.“</ta>
            <ta e="T123" id="Seg_5710" s="T122">Die Frau:</ta>
            <ta e="T125" id="Seg_5711" s="T123">„Weine nicht!</ta>
            <ta e="T126" id="Seg_5712" s="T125">Ich gebe [dir etwas].</ta>
            <ta e="T129" id="Seg_5713" s="T126">Ein Schaf habe ich.</ta>
            <ta e="T131" id="Seg_5714" s="T129">Das werde ich dir geben.“</ta>
            <ta e="T136" id="Seg_5715" s="T131">Dann wusch er sich und leinte sein Schaf an.</ta>
            <ta e="T137" id="Seg_5716" s="T136">Er wandert.</ta>
            <ta e="T139" id="Seg_5717" s="T137">Da steht ein Zelt.</ta>
            <ta e="T142" id="Seg_5718" s="T139">Er betrat das Zelt.</ta>
            <ta e="T145" id="Seg_5719" s="T142">Eine Frau und ein Mann wohnen darin.</ta>
            <ta e="T147" id="Seg_5720" s="T145">Er gab ihnen die Hand.</ta>
            <ta e="T148" id="Seg_5721" s="T147">Er/sie sagt:</ta>
            <ta e="T151" id="Seg_5722" s="T148">„Wohin gehst du?“</ta>
            <ta e="T155" id="Seg_5723" s="T151">„Ich habe meine Schwiegermutter besucht.</ta>
            <ta e="T159" id="Seg_5724" s="T155">Meine Schwiegermutter gab mir dieses eine Schaf.“</ta>
            <ta e="T163" id="Seg_5725" s="T159">„Lass es bei unseren Schafen.</ta>
            <ta e="T166" id="Seg_5726" s="T163">Mach dein Schaf los!.“</ta>
            <ta e="T171" id="Seg_5727" s="T166">„Eure Schafe werden mein Schaf auffressen.“</ta>
            <ta e="T173" id="Seg_5728" s="T171">„Warum sollten sie es auffressen?</ta>
            <ta e="T176" id="Seg_5729" s="T173">Na los, mach es los!“</ta>
            <ta e="T180" id="Seg_5730" s="T176">Der Mann führte es davon und machte es los.</ta>
            <ta e="T183" id="Seg_5731" s="T180">In der Nacht stand Kötschün Güdür auf.</ta>
            <ta e="T189" id="Seg_5732" s="T183">Er riss sein eigenes Schaf in Stücke.</ta>
            <ta e="T195" id="Seg_5733" s="T189">Er nahm die Nasen von der Hälfte der anderen Schafe und beschmierte sie mit Blut.</ta>
            <ta e="T198" id="Seg_5734" s="T195">Er selbst legte sich schlafen.</ta>
            <ta e="T202" id="Seg_5735" s="T198"> Am Morgen stand der Mann mit seiner Frau auf.</ta>
            <ta e="T205" id="Seg_5736" s="T202">Der Mann ging zum Pferch.</ta>
            <ta e="T209" id="Seg_5737" s="T205">Als er sich umsah, war ein Schaf in Stücke gerissen.</ta>
            <ta e="T212" id="Seg_5738" s="T209">Nachdem er zu seiner Frau zurückgekommen war, erzählte er:</ta>
            <ta e="T221" id="Seg_5739" s="T212">„Tatsache! Unsere Schafe haben das Schaf dieses Jungen wirklich in Stücke gerissen!</ta>
            <ta e="T226" id="Seg_5740" s="T221">Als Kötschün Güdür das hörte, sprang er auf.</ta>
            <ta e="T227" id="Seg_5741" s="T226">Er sagt: </ta>
            <ta e="T232" id="Seg_5742" s="T227">„Ich habe euch gesagt, sie würden mein Schaf auffressen.</ta>
            <ta e="T236" id="Seg_5743" s="T232">Und tatsächlich! Sie haben es wirklich aufgefressen.“</ta>
            <ta e="T240" id="Seg_5744" s="T236">Wieder weinte er und wälzte sich [ins Feuer].</ta>
            <ta e="T243" id="Seg_5745" s="T240">Der Mann zieht ihn aus dem Feuer heraus.</ta>
            <ta e="T247" id="Seg_5746" s="T243">Er wälzt sich abermals in das Feuer.</ta>
            <ta e="T249" id="Seg_5747" s="T247">„Weine nicht!</ta>
            <ta e="T252" id="Seg_5748" s="T249">Ich werde dir ein Schaf geben.“</ta>
            <ta e="T257" id="Seg_5749" s="T252">„Ich brauche euer Schaf nicht.</ta>
            <ta e="T261" id="Seg_5750" s="T257">Meine Schwiegermutter hat mir eins gegeben.“</ta>
            <ta e="T264" id="Seg_5751" s="T261">„Na, weine nicht!</ta>
            <ta e="T267" id="Seg_5752" s="T264">Ich werde dir alle fünf geben.“</ta>
            <ta e="T270" id="Seg_5753" s="T267">Er schrubbte sich und leinte seine Schafe an.</ta>
            <ta e="T271" id="Seg_5754" s="T270">Er wandert.</ta>
            <ta e="T274" id="Seg_5755" s="T271">Da steht ein hölzernes Kreuz.</ta>
            <ta e="T280" id="Seg_5756" s="T274">Dies grub Kötschün Güdür aus, er holte eine alte Frau heraus. </ta>
            <ta e="T281" id="Seg_5757" s="T280">Er lud sie sich auf.</ta>
            <ta e="T285" id="Seg_5758" s="T281">Wieder wanderte er ein Stück.</ta>
            <ta e="T287" id="Seg_5759" s="T285">Da steht ein Zelt.</ta>
            <ta e="T295" id="Seg_5760" s="T287">Die tote Frau setzte er an einen Baumstamm und band die Schafe an ihre Hand.</ta>
            <ta e="T298" id="Seg_5761" s="T295">Er stach sein Messer in ihr Herz.</ta>
            <ta e="T301" id="Seg_5762" s="T298">Er selbst ging zu dem Zelt.</ta>
            <ta e="T306" id="Seg_5763" s="T301">Ein Mann und seine zwei Töchter wohnen darin.</ta>
            <ta e="T307" id="Seg_5764" s="T306">„Essen wir!“</ta>
            <ta e="T310" id="Seg_5765" s="T307">„Ich habe eine Frau.</ta>
            <ta e="T314" id="Seg_5766" s="T310">Sie hält ihre Schafe am Baumstamm.“</ta>
            <ta e="T315" id="Seg_5767" s="T314">Die Mädchen:</ta>
            <ta e="T317" id="Seg_5768" s="T315">„Wir werden sie rufen gehen.“</ta>
            <ta e="T319" id="Seg_5769" s="T317">„Geht nicht!</ta>
            <ta e="T320" id="Seg_5770" s="T319">Ihr werdet sie erschrecken.</ta>
            <ta e="T324" id="Seg_5771" s="T320">Wenn sie erschrickt, wird sie sich erstechen.“</ta>
            <ta e="T326" id="Seg_5772" s="T324">Die Mädchen liefen los.</ta>
            <ta e="T329" id="Seg_5773" s="T326">Die Schafe erschraken und galoppierten davon.</ta>
            <ta e="T333" id="Seg_5774" s="T329">Als [die Mädchen] hinsahen, steckte ein Messer in ihrem Herz.</ta>
            <ta e="T335" id="Seg_5775" s="T333">Sie kamen zurückgelaufen:</ta>
            <ta e="T339" id="Seg_5776" s="T335">„Kötschün Güdür, Kötschün Güdür, deine Frau hat sich erstochen!“</ta>
            <ta e="T342" id="Seg_5777" s="T339">Wieder weinte er und wälzte sich.</ta>
            <ta e="T345" id="Seg_5778" s="T342">Der Mann holt ihn aus dem Feuer. </ta>
            <ta e="T349" id="Seg_5779" s="T345">Er wirft sich erneut in das Feuer.</ta>
            <ta e="T351" id="Seg_5780" s="T349">„Weine nicht!“,</ta>
            <ta e="T352" id="Seg_5781" s="T351">sagt er,</ta>
            <ta e="T354" id="Seg_5782" s="T352">„Ich werde dir meine Tochter geben.“</ta>
            <ta e="T359" id="Seg_5783" s="T354">„Ich brauche deine Tochter nicht.“</ta>
            <ta e="T361" id="Seg_5784" s="T359">„Weine nicht!“</ta>
            <ta e="T364" id="Seg_5785" s="T361">Ich werde dir beide geben.“</ta>
            <ta e="T369" id="Seg_5786" s="T364">Er schrubbte sich, leinte seine Weiber an und ging fort.</ta>
            <ta e="T372" id="Seg_5787" s="T369">Der Mann blieb allein zurück.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T5" id="Seg_5788" s="T0">Eine Alte lebte, einen ⌈ihren⌉ Knaben hatte sie.</ta>
            <ta e="T8" id="Seg_5789" s="T5">Ihr Sohn sagt zu seiner Mutter: »Ich werde heiraten.»</ta>
            <ta e="T10" id="Seg_5790" s="T8">»Ich werde heiraten.»</ta>
            <ta e="T12" id="Seg_5791" s="T10">Seine Mutter sagt:</ta>
            <ta e="T16" id="Seg_5792" s="T12">»Es gibt in der Nähe kein Weib, es gibt [es] nicht.»</ta>
            <ta e="T20" id="Seg_5793" s="T16">»Ich gehe ein Weib suchen.»</ta>
            <ta e="T24" id="Seg_5794" s="T20">Er ging, liess die Mutter zurück.</ta>
            <ta e="T26" id="Seg_5795" s="T24">Er geht, geht,</ta>
            <ta e="T30" id="Seg_5796" s="T26">am Ufer des Flusses ein Zelt steht.</ta>
            <ta e="T35" id="Seg_5797" s="T30">Er trat hinein in dieses Zelt. Ein Weib sitzt [dort].</ta>
            <ta e="T38" id="Seg_5798" s="T35">Betete zu Gott, gab die Hand,</ta>
            <ta e="T41" id="Seg_5799" s="T38">setzte sich an das Feuer.</ta>
            <ta e="T42" id="Seg_5800" s="T41">»Meine Tante!</ta>
            <ta e="T46" id="Seg_5801" s="T42">Einen Kessel hänge (über das Feuer), Fleisch stecke (hinein)!»</ta>
            <ta e="T48" id="Seg_5802" s="T46">»Ich habe nicht Fleisch.»</ta>
            <ta e="T50" id="Seg_5803" s="T48">»Ich bringe [es]».</ta>
            <ta e="T55" id="Seg_5804" s="T50">Er kam heraus, nahm einen kahlen Schulterknochen.</ta>
            <ta e="T58" id="Seg_5805" s="T55">Schlug sich aber die Nase,</ta>
            <ta e="T60" id="Seg_5806" s="T58">sein Blut floss,</ta>
            <ta e="T65" id="Seg_5807" s="T60">mit diesem Blute bestrich er gänzlich den Schulterknochen.</ta>
            <ta e="T71" id="Seg_5808" s="T65">Trat in das Zelt hinein, steckte den Schulterknochen in den Kessel.</ta>
            <ta e="T73" id="Seg_5809" s="T71">Sitzend sprechen sie.</ta>
            <ta e="T76" id="Seg_5810" s="T73">»Meine Tante, lasst uns essen!»</ta>
            <ta e="T85" id="Seg_5811" s="T76">Der Kessel des Weibes wurde gar, neben das Feuer stellte sie [ihn], nahm die Schöpfkelle,</ta>
            <ta e="T91" id="Seg_5812" s="T85">schöpfte Fleisch. Nur der Knochen im Kessel poltert.</ta>
            <ta e="T94" id="Seg_5813" s="T91">»Wo ist dein Fleisch?</ta>
            <ta e="T99" id="Seg_5814" s="T94">Allein dieser leere Knochen sich drehend geht.</ta>
            <ta e="T102" id="Seg_5815" s="T99">Ke̮t́š́ün-gɯd́ɯr, wo ist dein Fleisch?</ta>
            <ta e="T106" id="Seg_5816" s="T102">Allein der Knochen, der Schulterblattknochen sich drehend geht.</ta>
            <ta e="T108" id="Seg_5817" s="T106">Ke̮t́š́ün-gɯd́ɯr begann zu weinen,</ta>
            <ta e="T110" id="Seg_5818" s="T108">warf sich in das Feuer.</ta>
            <ta e="T113" id="Seg_5819" s="T110">Das Weib zog,</ta>
            <ta e="T114" id="Seg_5820" s="T113">zieht ihn weg.</ta>
            <ta e="T116" id="Seg_5821" s="T114">​»Weine nicht!»</ta>
            <ta e="T122" id="Seg_5822" s="T116">»Dein Kessel wurde trocken, er ass mein Fleisch.»</ta>
            <ta e="T123" id="Seg_5823" s="T122">Das Weib:</ta>
            <ta e="T125" id="Seg_5824" s="T123">»Weine nicht!</ta>
            <ta e="T126" id="Seg_5825" s="T125">Ich gebe.</ta>
            <ta e="T129" id="Seg_5826" s="T126">Ein Schaf habe ich,</ta>
            <ta e="T131" id="Seg_5827" s="T129">das gebe ich.»</ta>
            <ta e="T136" id="Seg_5828" s="T131">Dann wusch er sich, machte sein Schaf an,</ta>
            <ta e="T137" id="Seg_5829" s="T136">geht.</ta>
            <ta e="T139" id="Seg_5830" s="T137">Eine Zelt steht,</ta>
            <ta e="T142" id="Seg_5831" s="T139">er ging hinein in das Zelt.</ta>
            <ta e="T145" id="Seg_5832" s="T142">Ein Weib und ein Mann wohnen [dort].</ta>
            <ta e="T147" id="Seg_5833" s="T145">Gab &lt;gibt&gt; die Hand.</ta>
            <ta e="T151" id="Seg_5834" s="T147">»Wohin wanderst du?»</ta>
            <ta e="T155" id="Seg_5835" s="T151">»Meine Schwiegermutter ging zu Besuch.</ta>
            <ta e="T159" id="Seg_5836" s="T155">Meine Schwiegermutter gab dieses einzige Schaf.»</ta>
            <ta e="T163" id="Seg_5837" s="T159">»Lass es unter unsere Schafe,</ta>
            <ta e="T166" id="Seg_5838" s="T163">lass dein Schaf frei!»</ta>
            <ta e="T171" id="Seg_5839" s="T166">»Deine Schafe werden mein Schaf fressen.»</ta>
            <ta e="T173" id="Seg_5840" s="T171">»Warum werden sie [es] fressen?</ta>
            <ta e="T176" id="Seg_5841" s="T173">Na, lass es frei!»</ta>
            <ta e="T180" id="Seg_5842" s="T176">Der Alte brachte [es] und liess [es] los.</ta>
            <ta e="T183" id="Seg_5843" s="T180">In der Nacht stand Ke̮t́š́ün-gɯd́ɯr auf,</ta>
            <ta e="T189" id="Seg_5844" s="T183">zerriss aber sein Schaf, sein eigenes zerriss er.</ta>
            <ta e="T195" id="Seg_5845" s="T189">Nahm die Mäuler der Schafe, bestrich [sie] mit Blut.</ta>
            <ta e="T198" id="Seg_5846" s="T195">Selbst legte er sich schlafen.</ta>
            <ta e="T202" id="Seg_5847" s="T198">Am Morgen stand der Alte mit der Alten auf.</ta>
            <ta e="T205" id="Seg_5848" s="T202">Der Alte ging auf den Hof.</ta>
            <ta e="T209" id="Seg_5849" s="T205">Als er das Schaf zerrissen sah</ta>
            <ta e="T212" id="Seg_5850" s="T209">und nachdem er zu seinem Weibe gekommen war40 sagte er (laufend kommend):</ta>
            <ta e="T221" id="Seg_5851" s="T212">»Das Schaf dieses Burschen unsere Schafe zerrissen.»</ta>
            <ta e="T226" id="Seg_5852" s="T221">Ke̮t́š́ün-gɯd́ɯr hörte es, aufstehend fiel er nieder,</ta>
            <ta e="T227" id="Seg_5853" s="T226">sagt:</ta>
            <ta e="T232" id="Seg_5854" s="T227">»Ich sagte euch: 'Mein Schaf sie fressen'.</ta>
            <ta e="T236" id="Seg_5855" s="T232">Sehr schlimm, sie frassen [es].»</ta>
            <ta e="T240" id="Seg_5856" s="T236">Wieder weinend sich werfend stürzte er sich.</ta>
            <ta e="T243" id="Seg_5857" s="T240">Der Alte zieht ihn aus dem Feuer weg,</ta>
            <ta e="T247" id="Seg_5858" s="T243">er wirft sich abermals in das Feuer.</ta>
            <ta e="T249" id="Seg_5859" s="T247">»Weine nicht!</ta>
            <ta e="T252" id="Seg_5860" s="T249">Ich gebe ein Schaf.»</ta>
            <ta e="T257" id="Seg_5861" s="T252">»Ich brauche dein Schaf nicht.</ta>
            <ta e="T261" id="Seg_5862" s="T257">Meine Schwiegermutter gab mir.»</ta>
            <ta e="T264" id="Seg_5863" s="T261">»Na, weine nicht!</ta>
            <ta e="T267" id="Seg_5864" s="T264">Alle fünf gebe ich.»</ta>
            <ta e="T270" id="Seg_5865" s="T267">Er wusch sich, machte seine Schafe an,</ta>
            <ta e="T271" id="Seg_5866" s="T270">wandert.</ta>
            <ta e="T274" id="Seg_5867" s="T271">Ein Totenkreuz steht.</ta>
            <ta e="T280" id="Seg_5868" s="T274">Dort grub Ke̮t́š́ün-gɯd́ɯr, ein altes Weib zog er herauf,</ta>
            <ta e="T281" id="Seg_5869" s="T280">hob [es] hervor.</ta>
            <ta e="T285" id="Seg_5870" s="T281">Abermals ein wenig ging er.</ta>
            <ta e="T287" id="Seg_5871" s="T285">Ein Zelt steht, war da.</ta>
            <ta e="T295" id="Seg_5872" s="T287">Das tote Weib an den Stamm des Baumes, die Schafe an ihre Hand band er,</ta>
            <ta e="T298" id="Seg_5873" s="T295">sein Messer stach er in ihr Herz;</ta>
            <ta e="T301" id="Seg_5874" s="T298">selbst ging er in das Zelt.</ta>
            <ta e="T306" id="Seg_5875" s="T301">Ein Alter sitzt, seine zwei Töchter sitzen.</ta>
            <ta e="T307" id="Seg_5876" s="T306">»Lasst uns essen!»</ta>
            <ta e="T310" id="Seg_5877" s="T307">»Mein Weib ist am Stamme des Baumes, </ta>
            <ta e="T314" id="Seg_5878" s="T310">ihre Schafe, festhaltend sie sitzt.»</ta>
            <ta e="T315" id="Seg_5879" s="T314">Die Mädchen:</ta>
            <ta e="T317" id="Seg_5880" s="T315">»Gehend wir rufen.»</ta>
            <ta e="T319" id="Seg_5881" s="T317">»Gehet nicht,</ta>
            <ta e="T320" id="Seg_5882" s="T319">sie erschrickt,</ta>
            <ta e="T324" id="Seg_5883" s="T320">bei dem Erschrecken schneidet sie sich selbst.»</ta>
            <ta e="T326" id="Seg_5884" s="T324">Die Mädchen liefen,</ta>
            <ta e="T329" id="Seg_5885" s="T326">die Schafe erschraken, liefen.</ta>
            <ta e="T333" id="Seg_5886" s="T329">Als sie sahen: ein Messer sie in ihr Herz sticht,</ta>
            <ta e="T335" id="Seg_5887" s="T333">laufend kamen sie:</ta>
            <ta e="T339" id="Seg_5888" s="T335">»Ke̮t́š́ün-gɯd́ɯr, dein Weib schnitt sich selbst!»</ta>
            <ta e="T342" id="Seg_5889" s="T339">Wieder sich werfend weint er.</ta>
            <ta e="T345" id="Seg_5890" s="T342">Aus dem Feuer zieht er ihn.</ta>
            <ta e="T349" id="Seg_5891" s="T345">Er wirft sich wieder in das Feuer.</ta>
            <ta e="T351" id="Seg_5892" s="T349">»Weine nicht!» </ta>
            <ta e="T352" id="Seg_5893" s="T351">sagt er,</ta>
            <ta e="T354" id="Seg_5894" s="T352">»meine Tochter gebe ich.»</ta>
            <ta e="T359" id="Seg_5895" s="T354">»Ich benötige nicht deine Tochter.»</ta>
            <ta e="T361" id="Seg_5896" s="T359">»Weine nicht!</ta>
            <ta e="T364" id="Seg_5897" s="T361">Jene zwei gebe ich.»</ta>
            <ta e="T369" id="Seg_5898" s="T364">Er wusch sich, seine Weiber band er, ging.</ta>
            <ta e="T372" id="Seg_5899" s="T369">Der Alte blieb allein.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
