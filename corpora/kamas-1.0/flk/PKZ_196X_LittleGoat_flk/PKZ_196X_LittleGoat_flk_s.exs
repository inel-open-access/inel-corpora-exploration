<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF26AB1AC-81C0-EFC7-BEBD-DC74A84608D3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_LittleGoat_flk.wav" />
         <referenced-file url="PKZ_196X_LittleGoat_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_LittleGoat_flk\PKZ_196X_LittleGoat_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">121</ud-information>
            <ud-information attribute-name="# HIAT:w">83</ud-information>
            <ud-information attribute-name="# e">83</ud-information>
            <ud-information attribute-name="# HIAT:u">24</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T60" time="0.062" type="appl" />
         <tli id="T61" time="1.316" type="appl" />
         <tli id="T62" time="5.37956775893433" />
         <tli id="T63" time="6.277" type="appl" />
         <tli id="T64" time="7.163" type="appl" />
         <tli id="T65" time="8.959280133838588" />
         <tli id="T66" time="9.547" type="appl" />
         <tli id="T67" time="10.235" type="appl" />
         <tli id="T68" time="11.105774332570748" />
         <tli id="T69" time="12.016" type="appl" />
         <tli id="T70" time="14.312183368565066" />
         <tli id="T71" time="15.129" type="appl" />
         <tli id="T72" time="15.952" type="appl" />
         <tli id="T73" time="16.774" type="appl" />
         <tli id="T74" time="18.178539378703743" />
         <tli id="T75" time="18.978" type="appl" />
         <tli id="T76" time="19.622" type="appl" />
         <tli id="T77" time="21.218295138398975" />
         <tli id="T78" time="22.383" type="appl" />
         <tli id="T79" time="23.657" type="appl" />
         <tli id="T80" time="25.111315672745505" />
         <tli id="T81" time="26.056" type="appl" />
         <tli id="T82" time="28.244397267168228" />
         <tli id="T83" time="28.769" type="appl" />
         <tli id="T84" time="29.344" type="appl" />
         <tli id="T85" time="30.11091396171793" />
         <tli id="T86" time="31.13" type="appl" />
         <tli id="T87" time="32.196" type="appl" />
         <tli id="T88" time="32.675" type="appl" />
         <tli id="T89" time="33.153" type="appl" />
         <tli id="T90" time="34.03726515132428" />
         <tli id="T91" time="35.17" type="appl" />
         <tli id="T92" time="36.406" type="appl" />
         <tli id="T93" time="38.183598665645405" />
         <tli id="T94" time="38.625" type="appl" />
         <tli id="T95" time="39.241" type="appl" />
         <tli id="T96" time="39.856" type="appl" />
         <tli id="T97" time="40.673" type="appl" />
         <tli id="T98" time="41.489" type="appl" />
         <tli id="T99" time="42.306" type="appl" />
         <tli id="T100" time="43.123" type="appl" />
         <tli id="T101" time="43.94" type="appl" />
         <tli id="T102" time="44.756" type="appl" />
         <tli id="T103" time="45.573" type="appl" />
         <tli id="T104" time="46.65625123269068" />
         <tli id="T105" time="47.24" type="appl" />
         <tli id="T106" time="47.854" type="appl" />
         <tli id="T107" time="48.469" type="appl" />
         <tli id="T108" time="49.7160053855418" />
         <tli id="T109" time="50.743" type="appl" />
         <tli id="T110" time="51.79" type="appl" />
         <tli id="T111" time="53.98232925879828" />
         <tli id="T112" time="54.897" type="appl" />
         <tli id="T113" time="55.814" type="appl" />
         <tli id="T114" time="56.732" type="appl" />
         <tli id="T115" time="58.48863384992543" />
         <tli id="T116" time="59.441" type="appl" />
         <tli id="T117" time="60.407" type="appl" />
         <tli id="T118" time="61.374" type="appl" />
         <tli id="T119" time="62.341" type="appl" />
         <tli id="T120" time="63.308" type="appl" />
         <tli id="T121" time="64.274" type="appl" />
         <tli id="T122" time="66.09468938021547" />
         <tli id="T123" time="66.75" type="appl" />
         <tli id="T124" time="67.37" type="appl" />
         <tli id="T125" time="67.989" type="appl" />
         <tli id="T126" time="68.609" type="appl" />
         <tli id="T127" time="69.38775811988532" />
         <tli id="T128" time="70.291" type="appl" />
         <tli id="T129" time="71.211" type="appl" />
         <tli id="T130" time="72.131" type="appl" />
         <tli id="T131" time="73.051" type="appl" />
         <tli id="T132" time="73.971" type="appl" />
         <tli id="T133" time="74.711" type="appl" />
         <tli id="T134" time="75.45" type="appl" />
         <tli id="T135" time="76.19" type="appl" />
         <tli id="T136" time="76.929" type="appl" />
         <tli id="T137" time="77.669" type="appl" />
         <tli id="T138" time="78.409" type="appl" />
         <tli id="T139" time="79.148" type="appl" />
         <tli id="T140" time="79.888" type="appl" />
         <tli id="T141" time="80.627" type="appl" />
         <tli id="T142" time="81.56011342077018" />
         <tli id="T143" time="82.68" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T143" id="Seg_0" n="sc" s="T60">
               <ts e="T62" id="Seg_2" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_4" n="HIAT:w" s="T60">Arəmdə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_7" n="HIAT:w" s="T61">tĭmezeŋdə</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_11" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_13" n="HIAT:w" s="T62">Amnobiʔi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_16" n="HIAT:w" s="T63">nagur</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_19" n="HIAT:w" s="T64">poʔto</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_23" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_25" n="HIAT:w" s="T65">Urgo</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_28" n="HIAT:w" s="T66">poʔto</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_31" n="HIAT:w" s="T67">kambi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_35" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_37" n="HIAT:w" s="T68">Šonəga</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_41" n="HIAT:w" s="T69">šonəga</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_45" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_47" n="HIAT:w" s="T70">Volkdə</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_51" n="HIAT:w" s="T71">măndə:</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_54" n="HIAT:w" s="T72">Gibər</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_57" n="HIAT:w" s="T73">kandəgal</ts>
                  <nts id="Seg_58" n="HIAT:ip">?</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_61" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_63" n="HIAT:w" s="T74">Ĭmbi</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_66" n="HIAT:w" s="T75">dʼügən</ts>
                  <nts id="Seg_67" n="HIAT:ip">?</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_70" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_72" n="HIAT:w" s="T76">Ujuʔi</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_76" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_78" n="HIAT:w" s="T77">A</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_81" n="HIAT:w" s="T78">ĭmbi</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_84" n="HIAT:w" s="T79">ulugən</ts>
                  <nts id="Seg_85" n="HIAT:ip">?</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_88" n="HIAT:u" s="T80">
                  <nts id="Seg_89" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_91" n="HIAT:w" s="T80">Am-</ts>
                  <nts id="Seg_92" n="HIAT:ip">)</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_95" n="HIAT:w" s="T81">Amnuʔi</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_99" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_101" n="HIAT:w" s="T82">A</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_104" n="HIAT:w" s="T83">ĭmbi</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_107" n="HIAT:w" s="T84">šüjögən</ts>
                  <nts id="Seg_108" n="HIAT:ip">?</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_111" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_113" n="HIAT:w" s="T85">Sĭjdə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_116" n="HIAT:w" s="T86">pimnie</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_120" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_122" n="HIAT:w" s="T87">Dĭ</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_125" n="HIAT:w" s="T88">dĭm</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_128" n="HIAT:w" s="T89">amnuʔpi</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_132" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_134" n="HIAT:w" s="T90">Dĭgəttə</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_137" n="HIAT:w" s="T91">baška</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_140" n="HIAT:w" s="T92">šobi</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_144" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_146" n="HIAT:w" s="T93">I</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_149" n="HIAT:w" s="T94">dĭm</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_152" n="HIAT:w" s="T95">amnuʔpi</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_156" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_158" n="HIAT:w" s="T96">Dĭgəttə</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_161" n="HIAT:w" s="T97">üdʼüge</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_164" n="HIAT:w" s="T98">kandəga:</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_167" n="HIAT:w" s="T99">Gibər</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_170" n="HIAT:w" s="T100">măn</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_173" n="HIAT:w" s="T101">kagazaŋbə</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_176" n="HIAT:w" s="T102">kaŋluʔpiʔi</ts>
                  <nts id="Seg_177" n="HIAT:ip">?</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_180" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_182" n="HIAT:w" s="T103">Šonəga</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_186" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_188" n="HIAT:w" s="T104">Dĭgəttə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_191" n="HIAT:w" s="T105">bazoʔ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_194" n="HIAT:w" s="T106">volk</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_197" n="HIAT:w" s="T107">šonəga</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_201" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_203" n="HIAT:w" s="T108">Ĭmbi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_206" n="HIAT:w" s="T109">tăn</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_209" n="HIAT:w" s="T110">dĭn</ts>
                  <nts id="Seg_210" n="HIAT:ip">?</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_213" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_215" n="HIAT:w" s="T111">Măn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_218" n="HIAT:w" s="T112">üjüzeŋbə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_220" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_222" n="HIAT:w" s="T113">bazaj-</ts>
                  <nts id="Seg_223" n="HIAT:ip">)</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_226" n="HIAT:w" s="T114">bazajʔi</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_230" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_232" n="HIAT:w" s="T115">A</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_235" n="HIAT:w" s="T116">amnuʔi</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_238" n="HIAT:w" s="T117">tože</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_241" n="HIAT:w" s="T118">bazajʔi</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_245" n="HIAT:w" s="T119">tüjo</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_248" n="HIAT:w" s="T120">tănan</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_251" n="HIAT:w" s="T121">muʔlim</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_255" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_257" n="HIAT:w" s="T122">Dĭgəttə</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_261" n="HIAT:w" s="T123">muʔl-</ts>
                  <nts id="Seg_262" n="HIAT:ip">)</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_265" n="HIAT:w" s="T124">muʔluʔpi</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_268" n="HIAT:w" s="T125">dĭ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_271" n="HIAT:w" s="T126">volkə</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_275" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_277" n="HIAT:w" s="T127">Šide</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_280" n="HIAT:w" s="T128">kagazaŋdə</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_284" n="HIAT:w" s="T129">suʔməluʔ-</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_287" n="HIAT:w" s="T130">su-</ts>
                  <nts id="Seg_288" n="HIAT:ip">)</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_291" n="HIAT:w" s="T131">supsoluʔpiʔi</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_295" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_297" n="HIAT:w" s="T132">Dĭgəttə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_300" n="HIAT:w" s="T133">dĭ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_303" n="HIAT:w" s="T134">kandəga</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_306" n="HIAT:w" s="T135">maʔnʼi</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_310" n="HIAT:w" s="T136">a</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_313" n="HIAT:w" s="T137">dĭzeŋ</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_315" n="HIAT:ip">(</nts>
                  <ts e="T139" id="Seg_317" n="HIAT:w" s="T138">d-</ts>
                  <nts id="Seg_318" n="HIAT:ip">)</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_321" n="HIAT:w" s="T139">šide</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_324" n="HIAT:w" s="T140">brattə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_327" n="HIAT:w" s="T141">šonəgaʔi</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_331" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_333" n="HIAT:w" s="T142">Kabarləj</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T143" id="Seg_336" n="sc" s="T60">
               <ts e="T61" id="Seg_338" n="e" s="T60">Arəmdə </ts>
               <ts e="T62" id="Seg_340" n="e" s="T61">tĭmezeŋdə. </ts>
               <ts e="T63" id="Seg_342" n="e" s="T62">Amnobiʔi </ts>
               <ts e="T64" id="Seg_344" n="e" s="T63">nagur </ts>
               <ts e="T65" id="Seg_346" n="e" s="T64">poʔto. </ts>
               <ts e="T66" id="Seg_348" n="e" s="T65">Urgo </ts>
               <ts e="T67" id="Seg_350" n="e" s="T66">poʔto </ts>
               <ts e="T68" id="Seg_352" n="e" s="T67">kambi. </ts>
               <ts e="T69" id="Seg_354" n="e" s="T68">Šonəga, </ts>
               <ts e="T70" id="Seg_356" n="e" s="T69">šonəga. </ts>
               <ts e="T71" id="Seg_358" n="e" s="T70">Volkdə, </ts>
               <ts e="T72" id="Seg_360" n="e" s="T71">măndə: </ts>
               <ts e="T73" id="Seg_362" n="e" s="T72">Gibər </ts>
               <ts e="T74" id="Seg_364" n="e" s="T73">kandəgal? </ts>
               <ts e="T75" id="Seg_366" n="e" s="T74">Ĭmbi </ts>
               <ts e="T76" id="Seg_368" n="e" s="T75">dʼügən? </ts>
               <ts e="T77" id="Seg_370" n="e" s="T76">Ujuʔi. </ts>
               <ts e="T78" id="Seg_372" n="e" s="T77">A </ts>
               <ts e="T79" id="Seg_374" n="e" s="T78">ĭmbi </ts>
               <ts e="T80" id="Seg_376" n="e" s="T79">ulugən? </ts>
               <ts e="T81" id="Seg_378" n="e" s="T80">(Am-) </ts>
               <ts e="T82" id="Seg_380" n="e" s="T81">Amnuʔi. </ts>
               <ts e="T83" id="Seg_382" n="e" s="T82">A </ts>
               <ts e="T84" id="Seg_384" n="e" s="T83">ĭmbi </ts>
               <ts e="T85" id="Seg_386" n="e" s="T84">šüjögən? </ts>
               <ts e="T86" id="Seg_388" n="e" s="T85">Sĭjdə </ts>
               <ts e="T87" id="Seg_390" n="e" s="T86">pimnie. </ts>
               <ts e="T88" id="Seg_392" n="e" s="T87">Dĭ </ts>
               <ts e="T89" id="Seg_394" n="e" s="T88">dĭm </ts>
               <ts e="T90" id="Seg_396" n="e" s="T89">amnuʔpi. </ts>
               <ts e="T91" id="Seg_398" n="e" s="T90">Dĭgəttə </ts>
               <ts e="T92" id="Seg_400" n="e" s="T91">baška </ts>
               <ts e="T93" id="Seg_402" n="e" s="T92">šobi. </ts>
               <ts e="T94" id="Seg_404" n="e" s="T93">I </ts>
               <ts e="T95" id="Seg_406" n="e" s="T94">dĭm </ts>
               <ts e="T96" id="Seg_408" n="e" s="T95">amnuʔpi. </ts>
               <ts e="T97" id="Seg_410" n="e" s="T96">Dĭgəttə </ts>
               <ts e="T98" id="Seg_412" n="e" s="T97">üdʼüge </ts>
               <ts e="T99" id="Seg_414" n="e" s="T98">kandəga: </ts>
               <ts e="T100" id="Seg_416" n="e" s="T99">Gibər </ts>
               <ts e="T101" id="Seg_418" n="e" s="T100">măn </ts>
               <ts e="T102" id="Seg_420" n="e" s="T101">kagazaŋbə </ts>
               <ts e="T103" id="Seg_422" n="e" s="T102">kaŋluʔpiʔi? </ts>
               <ts e="T104" id="Seg_424" n="e" s="T103">Šonəga. </ts>
               <ts e="T105" id="Seg_426" n="e" s="T104">Dĭgəttə </ts>
               <ts e="T106" id="Seg_428" n="e" s="T105">bazoʔ </ts>
               <ts e="T107" id="Seg_430" n="e" s="T106">volk </ts>
               <ts e="T108" id="Seg_432" n="e" s="T107">šonəga. </ts>
               <ts e="T109" id="Seg_434" n="e" s="T108">Ĭmbi </ts>
               <ts e="T110" id="Seg_436" n="e" s="T109">tăn </ts>
               <ts e="T111" id="Seg_438" n="e" s="T110">dĭn? </ts>
               <ts e="T112" id="Seg_440" n="e" s="T111">Măn </ts>
               <ts e="T113" id="Seg_442" n="e" s="T112">üjüzeŋbə </ts>
               <ts e="T114" id="Seg_444" n="e" s="T113">(bazaj-) </ts>
               <ts e="T115" id="Seg_446" n="e" s="T114">bazajʔi. </ts>
               <ts e="T116" id="Seg_448" n="e" s="T115">A </ts>
               <ts e="T117" id="Seg_450" n="e" s="T116">amnuʔi </ts>
               <ts e="T118" id="Seg_452" n="e" s="T117">tože </ts>
               <ts e="T119" id="Seg_454" n="e" s="T118">bazajʔi, </ts>
               <ts e="T120" id="Seg_456" n="e" s="T119">tüjo </ts>
               <ts e="T121" id="Seg_458" n="e" s="T120">tănan </ts>
               <ts e="T122" id="Seg_460" n="e" s="T121">muʔlim. </ts>
               <ts e="T123" id="Seg_462" n="e" s="T122">Dĭgəttə </ts>
               <ts e="T124" id="Seg_464" n="e" s="T123">(muʔl-) </ts>
               <ts e="T125" id="Seg_466" n="e" s="T124">muʔluʔpi </ts>
               <ts e="T126" id="Seg_468" n="e" s="T125">dĭ </ts>
               <ts e="T127" id="Seg_470" n="e" s="T126">volkə. </ts>
               <ts e="T128" id="Seg_472" n="e" s="T127">Šide </ts>
               <ts e="T129" id="Seg_474" n="e" s="T128">kagazaŋdə </ts>
               <ts e="T130" id="Seg_476" n="e" s="T129">(suʔməluʔ- </ts>
               <ts e="T131" id="Seg_478" n="e" s="T130">su-) </ts>
               <ts e="T132" id="Seg_480" n="e" s="T131">supsoluʔpiʔi. </ts>
               <ts e="T133" id="Seg_482" n="e" s="T132">Dĭgəttə </ts>
               <ts e="T134" id="Seg_484" n="e" s="T133">dĭ </ts>
               <ts e="T135" id="Seg_486" n="e" s="T134">kandəga </ts>
               <ts e="T136" id="Seg_488" n="e" s="T135">maʔnʼi, </ts>
               <ts e="T137" id="Seg_490" n="e" s="T136">a </ts>
               <ts e="T138" id="Seg_492" n="e" s="T137">dĭzeŋ </ts>
               <ts e="T139" id="Seg_494" n="e" s="T138">(d-) </ts>
               <ts e="T140" id="Seg_496" n="e" s="T139">šide </ts>
               <ts e="T141" id="Seg_498" n="e" s="T140">brattə </ts>
               <ts e="T142" id="Seg_500" n="e" s="T141">šonəgaʔi. </ts>
               <ts e="T143" id="Seg_502" n="e" s="T142">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T62" id="Seg_503" s="T60">PKZ_196X_LittleGoat_flk.001 (001)</ta>
            <ta e="T65" id="Seg_504" s="T62">PKZ_196X_LittleGoat_flk.002 (002)</ta>
            <ta e="T68" id="Seg_505" s="T65">PKZ_196X_LittleGoat_flk.003 (003)</ta>
            <ta e="T70" id="Seg_506" s="T68">PKZ_196X_LittleGoat_flk.004 (004)</ta>
            <ta e="T74" id="Seg_507" s="T70">PKZ_196X_LittleGoat_flk.005 (005)</ta>
            <ta e="T76" id="Seg_508" s="T74">PKZ_196X_LittleGoat_flk.006 (006)</ta>
            <ta e="T77" id="Seg_509" s="T76">PKZ_196X_LittleGoat_flk.007 (007)</ta>
            <ta e="T80" id="Seg_510" s="T77">PKZ_196X_LittleGoat_flk.008 (008)</ta>
            <ta e="T82" id="Seg_511" s="T80">PKZ_196X_LittleGoat_flk.009 (009)</ta>
            <ta e="T85" id="Seg_512" s="T82">PKZ_196X_LittleGoat_flk.010 (010)</ta>
            <ta e="T87" id="Seg_513" s="T85">PKZ_196X_LittleGoat_flk.011 (011)</ta>
            <ta e="T90" id="Seg_514" s="T87">PKZ_196X_LittleGoat_flk.012 (012)</ta>
            <ta e="T93" id="Seg_515" s="T90">PKZ_196X_LittleGoat_flk.013 (013)</ta>
            <ta e="T96" id="Seg_516" s="T93">PKZ_196X_LittleGoat_flk.014 (014)</ta>
            <ta e="T103" id="Seg_517" s="T96">PKZ_196X_LittleGoat_flk.015 (015)</ta>
            <ta e="T104" id="Seg_518" s="T103">PKZ_196X_LittleGoat_flk.016 (016)</ta>
            <ta e="T108" id="Seg_519" s="T104">PKZ_196X_LittleGoat_flk.017 (017)</ta>
            <ta e="T111" id="Seg_520" s="T108">PKZ_196X_LittleGoat_flk.018 (018)</ta>
            <ta e="T115" id="Seg_521" s="T111">PKZ_196X_LittleGoat_flk.019 (019)</ta>
            <ta e="T122" id="Seg_522" s="T115">PKZ_196X_LittleGoat_flk.020 (020)</ta>
            <ta e="T127" id="Seg_523" s="T122">PKZ_196X_LittleGoat_flk.021 (021)</ta>
            <ta e="T132" id="Seg_524" s="T127">PKZ_196X_LittleGoat_flk.022 (022)</ta>
            <ta e="T142" id="Seg_525" s="T132">PKZ_196X_LittleGoat_flk.023 (023)</ta>
            <ta e="T143" id="Seg_526" s="T142">PKZ_196X_LittleGoat_flk.024 (024)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T62" id="Seg_527" s="T60">Arəmdə tĭmezeŋdə. </ta>
            <ta e="T65" id="Seg_528" s="T62">Amnobiʔi nagur poʔto. </ta>
            <ta e="T68" id="Seg_529" s="T65">Urgo poʔto kambi. </ta>
            <ta e="T70" id="Seg_530" s="T68">Šonəga, šonəga. </ta>
            <ta e="T74" id="Seg_531" s="T70">Volkdə, măndə: "Gibər kandəgal? </ta>
            <ta e="T76" id="Seg_532" s="T74">Ĭmbi dʼügən?" </ta>
            <ta e="T77" id="Seg_533" s="T76">"Ujuʔi". </ta>
            <ta e="T80" id="Seg_534" s="T77">"A ĭmbi ulugən?" </ta>
            <ta e="T82" id="Seg_535" s="T80">"(Am-) Amnuʔi". </ta>
            <ta e="T85" id="Seg_536" s="T82">"A ĭmbi šüjögən?" </ta>
            <ta e="T87" id="Seg_537" s="T85">Sĭjdə pimnie. </ta>
            <ta e="T90" id="Seg_538" s="T87">Dĭ dĭm amnuʔpi. </ta>
            <ta e="T93" id="Seg_539" s="T90">Dĭgəttə baška šobi. </ta>
            <ta e="T96" id="Seg_540" s="T93">I dĭm amnuʔpi. </ta>
            <ta e="T103" id="Seg_541" s="T96">Dĭgəttə üdʼüge kandəga:" Gibər măn kagazaŋbə kaŋluʔpiʔi?" </ta>
            <ta e="T104" id="Seg_542" s="T103">Šonəga. </ta>
            <ta e="T108" id="Seg_543" s="T104">Dĭgəttə bazoʔ volk šonəga. </ta>
            <ta e="T111" id="Seg_544" s="T108">"Ĭmbi tăn dĭn?" </ta>
            <ta e="T115" id="Seg_545" s="T111">"Măn üjüzeŋbə (bazaj-) bazajʔi. </ta>
            <ta e="T122" id="Seg_546" s="T115">A amnuʔi tože bazajʔi, tüjo tănan muʔlim". </ta>
            <ta e="T127" id="Seg_547" s="T122">Dĭgəttə (muʔl-) muʔluʔpi dĭ volkə. </ta>
            <ta e="T132" id="Seg_548" s="T127">Šide kagazaŋdə (suʔməluʔ- su-) supsoluʔpiʔi. </ta>
            <ta e="T142" id="Seg_549" s="T132">Dĭgəttə dĭ kandəga maʔnʼi, a dĭzeŋ (d-) šide brattə šonəgaʔi. </ta>
            <ta e="T143" id="Seg_550" s="T142">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T61" id="Seg_551" s="T60">arəm-də</ta>
            <ta e="T62" id="Seg_552" s="T61">tĭme-zeŋ-də</ta>
            <ta e="T63" id="Seg_553" s="T62">amno-bi-ʔi</ta>
            <ta e="T64" id="Seg_554" s="T63">nagur</ta>
            <ta e="T65" id="Seg_555" s="T64">poʔto</ta>
            <ta e="T66" id="Seg_556" s="T65">urgo</ta>
            <ta e="T67" id="Seg_557" s="T66">poʔto</ta>
            <ta e="T68" id="Seg_558" s="T67">kam-bi</ta>
            <ta e="T69" id="Seg_559" s="T68">šonə-ga</ta>
            <ta e="T70" id="Seg_560" s="T69">šonə-ga</ta>
            <ta e="T71" id="Seg_561" s="T70">volk-də</ta>
            <ta e="T72" id="Seg_562" s="T71">măn-də</ta>
            <ta e="T73" id="Seg_563" s="T72">gibər</ta>
            <ta e="T74" id="Seg_564" s="T73">kandə-ga-l</ta>
            <ta e="T75" id="Seg_565" s="T74">ĭmbi</ta>
            <ta e="T76" id="Seg_566" s="T75">dʼü-gən</ta>
            <ta e="T77" id="Seg_567" s="T76">uju-ʔi</ta>
            <ta e="T78" id="Seg_568" s="T77">a</ta>
            <ta e="T79" id="Seg_569" s="T78">ĭmbi</ta>
            <ta e="T80" id="Seg_570" s="T79">ulu-gən</ta>
            <ta e="T82" id="Seg_571" s="T81">amnu-ʔi</ta>
            <ta e="T83" id="Seg_572" s="T82">a</ta>
            <ta e="T84" id="Seg_573" s="T83">ĭmbi</ta>
            <ta e="T85" id="Seg_574" s="T84">šüjö-gən</ta>
            <ta e="T86" id="Seg_575" s="T85">sĭj-də</ta>
            <ta e="T87" id="Seg_576" s="T86">pim-nie</ta>
            <ta e="T88" id="Seg_577" s="T87">dĭ</ta>
            <ta e="T89" id="Seg_578" s="T88">dĭ-m</ta>
            <ta e="T90" id="Seg_579" s="T89">am-nuʔ-pi</ta>
            <ta e="T91" id="Seg_580" s="T90">dĭgəttə</ta>
            <ta e="T92" id="Seg_581" s="T91">baška</ta>
            <ta e="T93" id="Seg_582" s="T92">šo-bi</ta>
            <ta e="T94" id="Seg_583" s="T93">i</ta>
            <ta e="T95" id="Seg_584" s="T94">dĭ-m</ta>
            <ta e="T96" id="Seg_585" s="T95">am-nuʔ-pi</ta>
            <ta e="T97" id="Seg_586" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_587" s="T97">üdʼüge</ta>
            <ta e="T99" id="Seg_588" s="T98">kandə-ga</ta>
            <ta e="T100" id="Seg_589" s="T99">gibər</ta>
            <ta e="T101" id="Seg_590" s="T100">măn</ta>
            <ta e="T102" id="Seg_591" s="T101">kaga-zaŋ-bə</ta>
            <ta e="T103" id="Seg_592" s="T102">kaŋ-luʔ-pi-ʔi</ta>
            <ta e="T104" id="Seg_593" s="T103">šonə-ga</ta>
            <ta e="T105" id="Seg_594" s="T104">dĭgəttə</ta>
            <ta e="T106" id="Seg_595" s="T105">bazoʔ</ta>
            <ta e="T107" id="Seg_596" s="T106">volk</ta>
            <ta e="T108" id="Seg_597" s="T107">šonə-ga</ta>
            <ta e="T109" id="Seg_598" s="T108">ĭmbi</ta>
            <ta e="T110" id="Seg_599" s="T109">tăn</ta>
            <ta e="T111" id="Seg_600" s="T110">dĭn</ta>
            <ta e="T112" id="Seg_601" s="T111">măn</ta>
            <ta e="T113" id="Seg_602" s="T112">üjü-zeŋ-bə</ta>
            <ta e="T115" id="Seg_603" s="T114">baza-j-ʔi</ta>
            <ta e="T116" id="Seg_604" s="T115">a</ta>
            <ta e="T117" id="Seg_605" s="T116">amnu-ʔi</ta>
            <ta e="T118" id="Seg_606" s="T117">tože</ta>
            <ta e="T119" id="Seg_607" s="T118">baza-j-ʔi</ta>
            <ta e="T120" id="Seg_608" s="T119">tüjo</ta>
            <ta e="T121" id="Seg_609" s="T120">tănan</ta>
            <ta e="T122" id="Seg_610" s="T121">muʔ-li-m</ta>
            <ta e="T123" id="Seg_611" s="T122">dĭgəttə</ta>
            <ta e="T125" id="Seg_612" s="T124">muʔ-luʔ-pi</ta>
            <ta e="T126" id="Seg_613" s="T125">dĭ</ta>
            <ta e="T127" id="Seg_614" s="T126">volkə</ta>
            <ta e="T128" id="Seg_615" s="T127">šide</ta>
            <ta e="T129" id="Seg_616" s="T128">kaga-zaŋ-də</ta>
            <ta e="T132" id="Seg_617" s="T131">supso-luʔ-pi-ʔi</ta>
            <ta e="T133" id="Seg_618" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_619" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_620" s="T134">kandə-ga</ta>
            <ta e="T136" id="Seg_621" s="T135">maʔ-nʼi</ta>
            <ta e="T137" id="Seg_622" s="T136">a</ta>
            <ta e="T138" id="Seg_623" s="T137">dĭ-zeŋ</ta>
            <ta e="T140" id="Seg_624" s="T139">šide</ta>
            <ta e="T141" id="Seg_625" s="T140">brat-tə</ta>
            <ta e="T142" id="Seg_626" s="T141">šonə-ga-ʔi</ta>
            <ta e="T143" id="Seg_627" s="T142">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T61" id="Seg_628" s="T60">arəm-t</ta>
            <ta e="T62" id="Seg_629" s="T61">tĭme-zAŋ-də</ta>
            <ta e="T63" id="Seg_630" s="T62">amno-bi-jəʔ</ta>
            <ta e="T64" id="Seg_631" s="T63">nagur</ta>
            <ta e="T65" id="Seg_632" s="T64">poʔto</ta>
            <ta e="T66" id="Seg_633" s="T65">urgo</ta>
            <ta e="T67" id="Seg_634" s="T66">poʔto</ta>
            <ta e="T68" id="Seg_635" s="T67">kan-bi</ta>
            <ta e="T69" id="Seg_636" s="T68">šonə-gA</ta>
            <ta e="T70" id="Seg_637" s="T69">šonə-gA</ta>
            <ta e="T71" id="Seg_638" s="T70">volk-Tə</ta>
            <ta e="T72" id="Seg_639" s="T71">măn-ntə</ta>
            <ta e="T73" id="Seg_640" s="T72">gibər</ta>
            <ta e="T74" id="Seg_641" s="T73">kandə-gA-l</ta>
            <ta e="T75" id="Seg_642" s="T74">ĭmbi</ta>
            <ta e="T76" id="Seg_643" s="T75">tʼo-Kən</ta>
            <ta e="T77" id="Seg_644" s="T76">üjü-jəʔ</ta>
            <ta e="T78" id="Seg_645" s="T77">a</ta>
            <ta e="T79" id="Seg_646" s="T78">ĭmbi</ta>
            <ta e="T80" id="Seg_647" s="T79">ulu-Kən</ta>
            <ta e="T82" id="Seg_648" s="T81">amnu-jəʔ</ta>
            <ta e="T83" id="Seg_649" s="T82">a</ta>
            <ta e="T84" id="Seg_650" s="T83">ĭmbi</ta>
            <ta e="T85" id="Seg_651" s="T84">šüjə-Kən</ta>
            <ta e="T86" id="Seg_652" s="T85">sĭj-də</ta>
            <ta e="T87" id="Seg_653" s="T86">pim-liA</ta>
            <ta e="T88" id="Seg_654" s="T87">dĭ</ta>
            <ta e="T89" id="Seg_655" s="T88">dĭ-m</ta>
            <ta e="T90" id="Seg_656" s="T89">am-luʔbdə-bi</ta>
            <ta e="T91" id="Seg_657" s="T90">dĭgəttə</ta>
            <ta e="T92" id="Seg_658" s="T91">baška</ta>
            <ta e="T93" id="Seg_659" s="T92">šo-bi</ta>
            <ta e="T94" id="Seg_660" s="T93">i</ta>
            <ta e="T95" id="Seg_661" s="T94">dĭ-m</ta>
            <ta e="T96" id="Seg_662" s="T95">am-luʔbdə-bi</ta>
            <ta e="T97" id="Seg_663" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_664" s="T97">üdʼüge</ta>
            <ta e="T99" id="Seg_665" s="T98">kandə-gA</ta>
            <ta e="T100" id="Seg_666" s="T99">gibər</ta>
            <ta e="T101" id="Seg_667" s="T100">măn</ta>
            <ta e="T102" id="Seg_668" s="T101">kaga-zAŋ-m</ta>
            <ta e="T103" id="Seg_669" s="T102">kan-luʔbdə-bi-jəʔ</ta>
            <ta e="T104" id="Seg_670" s="T103">šonə-gA</ta>
            <ta e="T105" id="Seg_671" s="T104">dĭgəttə</ta>
            <ta e="T106" id="Seg_672" s="T105">bazoʔ</ta>
            <ta e="T107" id="Seg_673" s="T106">volk</ta>
            <ta e="T108" id="Seg_674" s="T107">šonə-gA</ta>
            <ta e="T109" id="Seg_675" s="T108">ĭmbi</ta>
            <ta e="T110" id="Seg_676" s="T109">tăn</ta>
            <ta e="T111" id="Seg_677" s="T110">dĭn</ta>
            <ta e="T112" id="Seg_678" s="T111">măn</ta>
            <ta e="T113" id="Seg_679" s="T112">üjü-zAŋ-m</ta>
            <ta e="T115" id="Seg_680" s="T114">baza-j-jəʔ</ta>
            <ta e="T116" id="Seg_681" s="T115">a</ta>
            <ta e="T117" id="Seg_682" s="T116">amnu-jəʔ</ta>
            <ta e="T118" id="Seg_683" s="T117">tože</ta>
            <ta e="T119" id="Seg_684" s="T118">baza-j-jəʔ</ta>
            <ta e="T120" id="Seg_685" s="T119">tüjö</ta>
            <ta e="T121" id="Seg_686" s="T120">tănan</ta>
            <ta e="T122" id="Seg_687" s="T121">müʔbdə-lV-m</ta>
            <ta e="T123" id="Seg_688" s="T122">dĭgəttə</ta>
            <ta e="T125" id="Seg_689" s="T124">müʔbdə-luʔbdə-bi</ta>
            <ta e="T126" id="Seg_690" s="T125">dĭ</ta>
            <ta e="T127" id="Seg_691" s="T126">volk</ta>
            <ta e="T128" id="Seg_692" s="T127">šide</ta>
            <ta e="T129" id="Seg_693" s="T128">kaga-zAŋ-də</ta>
            <ta e="T132" id="Seg_694" s="T131">supso-luʔbdə-bi-jəʔ</ta>
            <ta e="T133" id="Seg_695" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_696" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_697" s="T134">kandə-gA</ta>
            <ta e="T136" id="Seg_698" s="T135">maʔ-gənʼi</ta>
            <ta e="T137" id="Seg_699" s="T136">a</ta>
            <ta e="T138" id="Seg_700" s="T137">dĭ-zAŋ</ta>
            <ta e="T140" id="Seg_701" s="T139">šide</ta>
            <ta e="T141" id="Seg_702" s="T140">brat-də</ta>
            <ta e="T142" id="Seg_703" s="T141">šonə-gA-jəʔ</ta>
            <ta e="T143" id="Seg_704" s="T142">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T61" id="Seg_705" s="T60">clean-IMP.2SG.O</ta>
            <ta e="T62" id="Seg_706" s="T61">tooth-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_707" s="T62">live-PST-3PL</ta>
            <ta e="T64" id="Seg_708" s="T63">three.[NOM.SG]</ta>
            <ta e="T65" id="Seg_709" s="T64">goat.[NOM.SG]</ta>
            <ta e="T66" id="Seg_710" s="T65">big.[NOM.SG]</ta>
            <ta e="T67" id="Seg_711" s="T66">goat.[NOM.SG]</ta>
            <ta e="T68" id="Seg_712" s="T67">go-PST.[3SG]</ta>
            <ta e="T69" id="Seg_713" s="T68">come-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_714" s="T69">come-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_715" s="T70">wolf-LAT</ta>
            <ta e="T72" id="Seg_716" s="T71">say-IPFVZ.[3SG]</ta>
            <ta e="T73" id="Seg_717" s="T72">where.to</ta>
            <ta e="T74" id="Seg_718" s="T73">walk-PRS-2SG</ta>
            <ta e="T75" id="Seg_719" s="T74">what.[NOM.SG]</ta>
            <ta e="T76" id="Seg_720" s="T75">earth-LOC</ta>
            <ta e="T77" id="Seg_721" s="T76">foot-PL</ta>
            <ta e="T78" id="Seg_722" s="T77">and</ta>
            <ta e="T79" id="Seg_723" s="T78">what.[NOM.SG]</ta>
            <ta e="T80" id="Seg_724" s="T79">head-LOC</ta>
            <ta e="T82" id="Seg_725" s="T81">horn-PL</ta>
            <ta e="T83" id="Seg_726" s="T82">and</ta>
            <ta e="T84" id="Seg_727" s="T83">what.[NOM.SG]</ta>
            <ta e="T85" id="Seg_728" s="T84">inside-LOC</ta>
            <ta e="T86" id="Seg_729" s="T85">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T87" id="Seg_730" s="T86">fear-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_731" s="T87">this.[NOM.SG]</ta>
            <ta e="T89" id="Seg_732" s="T88">this-ACC</ta>
            <ta e="T90" id="Seg_733" s="T89">eat-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_734" s="T90">then</ta>
            <ta e="T92" id="Seg_735" s="T91">another.[NOM.SG]</ta>
            <ta e="T93" id="Seg_736" s="T92">come-PST.[3SG]</ta>
            <ta e="T94" id="Seg_737" s="T93">and</ta>
            <ta e="T95" id="Seg_738" s="T94">this-ACC</ta>
            <ta e="T96" id="Seg_739" s="T95">eat-MOM-PST.[3SG]</ta>
            <ta e="T97" id="Seg_740" s="T96">then</ta>
            <ta e="T98" id="Seg_741" s="T97">small.[NOM.SG]</ta>
            <ta e="T99" id="Seg_742" s="T98">walk-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_743" s="T99">where.to</ta>
            <ta e="T101" id="Seg_744" s="T100">I.GEN</ta>
            <ta e="T102" id="Seg_745" s="T101">brother-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T103" id="Seg_746" s="T102">go-MOM-PST-3PL</ta>
            <ta e="T104" id="Seg_747" s="T103">come-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_748" s="T104">then</ta>
            <ta e="T106" id="Seg_749" s="T105">again</ta>
            <ta e="T107" id="Seg_750" s="T106">wolf.[NOM.SG]</ta>
            <ta e="T108" id="Seg_751" s="T107">come-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_752" s="T108">what.[NOM.SG]</ta>
            <ta e="T110" id="Seg_753" s="T109">you.GEN</ta>
            <ta e="T111" id="Seg_754" s="T110">there</ta>
            <ta e="T112" id="Seg_755" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_756" s="T112">foot-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T115" id="Seg_757" s="T114">iron-ADJZ-PL</ta>
            <ta e="T116" id="Seg_758" s="T115">and</ta>
            <ta e="T117" id="Seg_759" s="T116">horn-PL</ta>
            <ta e="T118" id="Seg_760" s="T117">also</ta>
            <ta e="T119" id="Seg_761" s="T118">iron-ADJZ-PL</ta>
            <ta e="T120" id="Seg_762" s="T119">soon</ta>
            <ta e="T121" id="Seg_763" s="T120">you.ACC</ta>
            <ta e="T122" id="Seg_764" s="T121">punch-FUT-1SG</ta>
            <ta e="T123" id="Seg_765" s="T122">then</ta>
            <ta e="T125" id="Seg_766" s="T124">push-MOM-PST.[3SG]</ta>
            <ta e="T126" id="Seg_767" s="T125">this.[NOM.SG]</ta>
            <ta e="T127" id="Seg_768" s="T126">wolf.ACC</ta>
            <ta e="T128" id="Seg_769" s="T127">two.[NOM.SG]</ta>
            <ta e="T129" id="Seg_770" s="T128">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T132" id="Seg_771" s="T131">depart-MOM-PST-3PL</ta>
            <ta e="T133" id="Seg_772" s="T132">then</ta>
            <ta e="T134" id="Seg_773" s="T133">this.[NOM.SG]</ta>
            <ta e="T135" id="Seg_774" s="T134">walk-PRS.[3SG]</ta>
            <ta e="T136" id="Seg_775" s="T135">tent-LAT/LOC.1SG</ta>
            <ta e="T137" id="Seg_776" s="T136">and</ta>
            <ta e="T138" id="Seg_777" s="T137">this-PL</ta>
            <ta e="T140" id="Seg_778" s="T139">two.[NOM.SG]</ta>
            <ta e="T141" id="Seg_779" s="T140">brother-NOM/GEN/ACC.3SG</ta>
            <ta e="T142" id="Seg_780" s="T141">come-PRS-3PL</ta>
            <ta e="T143" id="Seg_781" s="T142">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T61" id="Seg_782" s="T60">чистить-IMP.2SG.O</ta>
            <ta e="T62" id="Seg_783" s="T61">зуб-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_784" s="T62">жить-PST-3PL</ta>
            <ta e="T64" id="Seg_785" s="T63">три.[NOM.SG]</ta>
            <ta e="T65" id="Seg_786" s="T64">коза.[NOM.SG]</ta>
            <ta e="T66" id="Seg_787" s="T65">большой.[NOM.SG]</ta>
            <ta e="T67" id="Seg_788" s="T66">коза.[NOM.SG]</ta>
            <ta e="T68" id="Seg_789" s="T67">пойти-PST.[3SG]</ta>
            <ta e="T69" id="Seg_790" s="T68">прийти-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_791" s="T69">прийти-PRS.[3SG]</ta>
            <ta e="T71" id="Seg_792" s="T70">волк-LAT</ta>
            <ta e="T72" id="Seg_793" s="T71">сказать-IPFVZ.[3SG]</ta>
            <ta e="T73" id="Seg_794" s="T72">куда</ta>
            <ta e="T74" id="Seg_795" s="T73">идти-PRS-2SG</ta>
            <ta e="T75" id="Seg_796" s="T74">что.[NOM.SG]</ta>
            <ta e="T76" id="Seg_797" s="T75">земля-LOC</ta>
            <ta e="T77" id="Seg_798" s="T76">нога-PL</ta>
            <ta e="T78" id="Seg_799" s="T77">а</ta>
            <ta e="T79" id="Seg_800" s="T78">что.[NOM.SG]</ta>
            <ta e="T80" id="Seg_801" s="T79">голова-LOC</ta>
            <ta e="T82" id="Seg_802" s="T81">рог-PL</ta>
            <ta e="T83" id="Seg_803" s="T82">а</ta>
            <ta e="T84" id="Seg_804" s="T83">что.[NOM.SG]</ta>
            <ta e="T85" id="Seg_805" s="T84">внутри-LOC</ta>
            <ta e="T86" id="Seg_806" s="T85">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T87" id="Seg_807" s="T86">бояться-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_808" s="T87">этот.[NOM.SG]</ta>
            <ta e="T89" id="Seg_809" s="T88">этот-ACC</ta>
            <ta e="T90" id="Seg_810" s="T89">съесть-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_811" s="T90">тогда</ta>
            <ta e="T92" id="Seg_812" s="T91">другой.[NOM.SG]</ta>
            <ta e="T93" id="Seg_813" s="T92">прийти-PST.[3SG]</ta>
            <ta e="T94" id="Seg_814" s="T93">и</ta>
            <ta e="T95" id="Seg_815" s="T94">этот-ACC</ta>
            <ta e="T96" id="Seg_816" s="T95">съесть-MOM-PST.[3SG]</ta>
            <ta e="T97" id="Seg_817" s="T96">тогда</ta>
            <ta e="T98" id="Seg_818" s="T97">маленький.[NOM.SG]</ta>
            <ta e="T99" id="Seg_819" s="T98">идти-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_820" s="T99">куда</ta>
            <ta e="T101" id="Seg_821" s="T100">я.GEN</ta>
            <ta e="T102" id="Seg_822" s="T101">брат-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T103" id="Seg_823" s="T102">пойти-MOM-PST-3PL</ta>
            <ta e="T104" id="Seg_824" s="T103">прийти-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_825" s="T104">тогда</ta>
            <ta e="T106" id="Seg_826" s="T105">опять</ta>
            <ta e="T107" id="Seg_827" s="T106">волк.[NOM.SG]</ta>
            <ta e="T108" id="Seg_828" s="T107">прийти-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_829" s="T108">что.[NOM.SG]</ta>
            <ta e="T110" id="Seg_830" s="T109">ты.GEN</ta>
            <ta e="T111" id="Seg_831" s="T110">там</ta>
            <ta e="T112" id="Seg_832" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_833" s="T112">нога-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T115" id="Seg_834" s="T114">железо-ADJZ-PL</ta>
            <ta e="T116" id="Seg_835" s="T115">а</ta>
            <ta e="T117" id="Seg_836" s="T116">рог-PL</ta>
            <ta e="T118" id="Seg_837" s="T117">тоже</ta>
            <ta e="T119" id="Seg_838" s="T118">железо-ADJZ-PL</ta>
            <ta e="T120" id="Seg_839" s="T119">скоро</ta>
            <ta e="T121" id="Seg_840" s="T120">ты.ACC</ta>
            <ta e="T122" id="Seg_841" s="T121">уколоть-FUT-1SG</ta>
            <ta e="T123" id="Seg_842" s="T122">тогда</ta>
            <ta e="T125" id="Seg_843" s="T124">вставить-MOM-PST.[3SG]</ta>
            <ta e="T126" id="Seg_844" s="T125">этот.[NOM.SG]</ta>
            <ta e="T127" id="Seg_845" s="T126">волк.ACC</ta>
            <ta e="T128" id="Seg_846" s="T127">два.[NOM.SG]</ta>
            <ta e="T129" id="Seg_847" s="T128">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T132" id="Seg_848" s="T131">уйти-MOM-PST-3PL</ta>
            <ta e="T133" id="Seg_849" s="T132">тогда</ta>
            <ta e="T134" id="Seg_850" s="T133">этот.[NOM.SG]</ta>
            <ta e="T135" id="Seg_851" s="T134">идти-PRS.[3SG]</ta>
            <ta e="T136" id="Seg_852" s="T135">чум-LAT/LOC.1SG</ta>
            <ta e="T137" id="Seg_853" s="T136">а</ta>
            <ta e="T138" id="Seg_854" s="T137">этот-PL</ta>
            <ta e="T140" id="Seg_855" s="T139">два.[NOM.SG]</ta>
            <ta e="T141" id="Seg_856" s="T140">брат-NOM/GEN/ACC.3SG</ta>
            <ta e="T142" id="Seg_857" s="T141">прийти-PRS-3PL</ta>
            <ta e="T143" id="Seg_858" s="T142">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T61" id="Seg_859" s="T60">v-v:mood.pn</ta>
            <ta e="T62" id="Seg_860" s="T61">n-n:num-n:case.poss</ta>
            <ta e="T63" id="Seg_861" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_862" s="T63">num-n:case</ta>
            <ta e="T65" id="Seg_863" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_864" s="T65">adj-n:case</ta>
            <ta e="T67" id="Seg_865" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_866" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_867" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_868" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_869" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_870" s="T71">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_871" s="T72">que</ta>
            <ta e="T74" id="Seg_872" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_873" s="T74">que-n:case</ta>
            <ta e="T76" id="Seg_874" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_875" s="T76">n-n:num</ta>
            <ta e="T78" id="Seg_876" s="T77">conj</ta>
            <ta e="T79" id="Seg_877" s="T78">que-n:case</ta>
            <ta e="T80" id="Seg_878" s="T79">n-n:case</ta>
            <ta e="T82" id="Seg_879" s="T81">n-n:num</ta>
            <ta e="T83" id="Seg_880" s="T82">conj</ta>
            <ta e="T84" id="Seg_881" s="T83">que-n:case</ta>
            <ta e="T85" id="Seg_882" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_883" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_884" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_885" s="T87">dempro-n:case</ta>
            <ta e="T89" id="Seg_886" s="T88">dempro-n:case</ta>
            <ta e="T90" id="Seg_887" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_888" s="T90">adv</ta>
            <ta e="T92" id="Seg_889" s="T91">adj-n:case</ta>
            <ta e="T93" id="Seg_890" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_891" s="T93">conj</ta>
            <ta e="T95" id="Seg_892" s="T94">dempro-n:case</ta>
            <ta e="T96" id="Seg_893" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_894" s="T96">adv</ta>
            <ta e="T98" id="Seg_895" s="T97">adj-n:case</ta>
            <ta e="T99" id="Seg_896" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_897" s="T99">que</ta>
            <ta e="T101" id="Seg_898" s="T100">pers</ta>
            <ta e="T102" id="Seg_899" s="T101">n-n:num-n:case.poss</ta>
            <ta e="T103" id="Seg_900" s="T102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_901" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_902" s="T104">adv</ta>
            <ta e="T106" id="Seg_903" s="T105">adv</ta>
            <ta e="T107" id="Seg_904" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_905" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_906" s="T108">que-n:case</ta>
            <ta e="T110" id="Seg_907" s="T109">pers</ta>
            <ta e="T111" id="Seg_908" s="T110">adv</ta>
            <ta e="T112" id="Seg_909" s="T111">pers</ta>
            <ta e="T113" id="Seg_910" s="T112">n-n:num-n:case.poss</ta>
            <ta e="T115" id="Seg_911" s="T114">n-n&gt;adj-n:num</ta>
            <ta e="T116" id="Seg_912" s="T115">conj</ta>
            <ta e="T117" id="Seg_913" s="T116">n-n:num</ta>
            <ta e="T118" id="Seg_914" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_915" s="T118">n-n&gt;adj-n:num</ta>
            <ta e="T120" id="Seg_916" s="T119">adv</ta>
            <ta e="T121" id="Seg_917" s="T120">pers</ta>
            <ta e="T122" id="Seg_918" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_919" s="T122">adv</ta>
            <ta e="T125" id="Seg_920" s="T124">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_921" s="T125">dempro-n:case</ta>
            <ta e="T127" id="Seg_922" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_923" s="T127">num-n:case</ta>
            <ta e="T129" id="Seg_924" s="T128">n-n:num-n:case.poss</ta>
            <ta e="T132" id="Seg_925" s="T131">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_926" s="T132">adv</ta>
            <ta e="T134" id="Seg_927" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_928" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_929" s="T135">n-n:case.poss</ta>
            <ta e="T137" id="Seg_930" s="T136">conj</ta>
            <ta e="T138" id="Seg_931" s="T137">dempro-n:num</ta>
            <ta e="T140" id="Seg_932" s="T139">num-n:case</ta>
            <ta e="T141" id="Seg_933" s="T140">n-n:case.poss</ta>
            <ta e="T142" id="Seg_934" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_935" s="T142">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T61" id="Seg_936" s="T60">v</ta>
            <ta e="T62" id="Seg_937" s="T61">n</ta>
            <ta e="T63" id="Seg_938" s="T62">v</ta>
            <ta e="T64" id="Seg_939" s="T63">num</ta>
            <ta e="T65" id="Seg_940" s="T64">n</ta>
            <ta e="T66" id="Seg_941" s="T65">adj</ta>
            <ta e="T67" id="Seg_942" s="T66">n</ta>
            <ta e="T68" id="Seg_943" s="T67">v</ta>
            <ta e="T69" id="Seg_944" s="T68">v</ta>
            <ta e="T70" id="Seg_945" s="T69">v</ta>
            <ta e="T71" id="Seg_946" s="T70">n</ta>
            <ta e="T72" id="Seg_947" s="T71">v</ta>
            <ta e="T73" id="Seg_948" s="T72">que</ta>
            <ta e="T74" id="Seg_949" s="T73">v</ta>
            <ta e="T75" id="Seg_950" s="T74">que</ta>
            <ta e="T76" id="Seg_951" s="T75">n</ta>
            <ta e="T77" id="Seg_952" s="T76">n</ta>
            <ta e="T78" id="Seg_953" s="T77">conj</ta>
            <ta e="T79" id="Seg_954" s="T78">que</ta>
            <ta e="T80" id="Seg_955" s="T79">n</ta>
            <ta e="T82" id="Seg_956" s="T81">n</ta>
            <ta e="T83" id="Seg_957" s="T82">conj</ta>
            <ta e="T84" id="Seg_958" s="T83">que</ta>
            <ta e="T85" id="Seg_959" s="T84">n</ta>
            <ta e="T86" id="Seg_960" s="T85">n</ta>
            <ta e="T87" id="Seg_961" s="T86">v</ta>
            <ta e="T88" id="Seg_962" s="T87">dempro</ta>
            <ta e="T89" id="Seg_963" s="T88">dempro</ta>
            <ta e="T90" id="Seg_964" s="T89">v</ta>
            <ta e="T91" id="Seg_965" s="T90">adv</ta>
            <ta e="T92" id="Seg_966" s="T91">adj</ta>
            <ta e="T93" id="Seg_967" s="T92">v</ta>
            <ta e="T94" id="Seg_968" s="T93">conj</ta>
            <ta e="T95" id="Seg_969" s="T94">dempro</ta>
            <ta e="T96" id="Seg_970" s="T95">v</ta>
            <ta e="T97" id="Seg_971" s="T96">adv</ta>
            <ta e="T98" id="Seg_972" s="T97">adj</ta>
            <ta e="T99" id="Seg_973" s="T98">v</ta>
            <ta e="T100" id="Seg_974" s="T99">que</ta>
            <ta e="T101" id="Seg_975" s="T100">pers</ta>
            <ta e="T102" id="Seg_976" s="T101">n</ta>
            <ta e="T103" id="Seg_977" s="T102">v</ta>
            <ta e="T104" id="Seg_978" s="T103">v</ta>
            <ta e="T105" id="Seg_979" s="T104">adv</ta>
            <ta e="T106" id="Seg_980" s="T105">adv</ta>
            <ta e="T107" id="Seg_981" s="T106">n</ta>
            <ta e="T108" id="Seg_982" s="T107">v</ta>
            <ta e="T109" id="Seg_983" s="T108">que</ta>
            <ta e="T110" id="Seg_984" s="T109">pers</ta>
            <ta e="T111" id="Seg_985" s="T110">adv</ta>
            <ta e="T112" id="Seg_986" s="T111">pers</ta>
            <ta e="T113" id="Seg_987" s="T112">n</ta>
            <ta e="T115" id="Seg_988" s="T114">adj</ta>
            <ta e="T116" id="Seg_989" s="T115">conj</ta>
            <ta e="T117" id="Seg_990" s="T116">n</ta>
            <ta e="T118" id="Seg_991" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_992" s="T118">adj</ta>
            <ta e="T120" id="Seg_993" s="T119">adv</ta>
            <ta e="T121" id="Seg_994" s="T120">pers</ta>
            <ta e="T122" id="Seg_995" s="T121">v</ta>
            <ta e="T123" id="Seg_996" s="T122">adv</ta>
            <ta e="T125" id="Seg_997" s="T124">v</ta>
            <ta e="T126" id="Seg_998" s="T125">dempro</ta>
            <ta e="T127" id="Seg_999" s="T126">n</ta>
            <ta e="T128" id="Seg_1000" s="T127">num</ta>
            <ta e="T129" id="Seg_1001" s="T128">n</ta>
            <ta e="T132" id="Seg_1002" s="T131">v</ta>
            <ta e="T133" id="Seg_1003" s="T132">adv</ta>
            <ta e="T134" id="Seg_1004" s="T133">dempro</ta>
            <ta e="T135" id="Seg_1005" s="T134">v</ta>
            <ta e="T136" id="Seg_1006" s="T135">n</ta>
            <ta e="T137" id="Seg_1007" s="T136">conj</ta>
            <ta e="T138" id="Seg_1008" s="T137">dempro</ta>
            <ta e="T140" id="Seg_1009" s="T139">num</ta>
            <ta e="T141" id="Seg_1010" s="T140">n</ta>
            <ta e="T142" id="Seg_1011" s="T141">v</ta>
            <ta e="T143" id="Seg_1012" s="T142">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T61" id="Seg_1013" s="T60">0.2.h:A</ta>
            <ta e="T62" id="Seg_1014" s="T61">np:Th</ta>
            <ta e="T65" id="Seg_1015" s="T64">np.h:E</ta>
            <ta e="T67" id="Seg_1016" s="T66">np.h:A</ta>
            <ta e="T69" id="Seg_1017" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_1018" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_1019" s="T70">np.h:R</ta>
            <ta e="T72" id="Seg_1020" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_1021" s="T72">pro:G</ta>
            <ta e="T74" id="Seg_1022" s="T73">0.2.h:A</ta>
            <ta e="T75" id="Seg_1023" s="T74">pro:Th</ta>
            <ta e="T76" id="Seg_1024" s="T75">np:Th</ta>
            <ta e="T79" id="Seg_1025" s="T78">pro:Th</ta>
            <ta e="T80" id="Seg_1026" s="T79">np:Th</ta>
            <ta e="T84" id="Seg_1027" s="T83">pro:Th</ta>
            <ta e="T85" id="Seg_1028" s="T84">np:Th</ta>
            <ta e="T86" id="Seg_1029" s="T85">np:E</ta>
            <ta e="T88" id="Seg_1030" s="T87">pro.h:A</ta>
            <ta e="T89" id="Seg_1031" s="T88">pro.h:P</ta>
            <ta e="T91" id="Seg_1032" s="T90">adv:Time</ta>
            <ta e="T92" id="Seg_1033" s="T91">pro.h:A</ta>
            <ta e="T95" id="Seg_1034" s="T94">pro.h:P</ta>
            <ta e="T96" id="Seg_1035" s="T95">0.3.h:A</ta>
            <ta e="T97" id="Seg_1036" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_1037" s="T97">np.h:A</ta>
            <ta e="T100" id="Seg_1038" s="T99">pro:L</ta>
            <ta e="T101" id="Seg_1039" s="T100">pro.h:Poss</ta>
            <ta e="T102" id="Seg_1040" s="T101">np.h:A</ta>
            <ta e="T104" id="Seg_1041" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_1042" s="T104">adv:Time</ta>
            <ta e="T107" id="Seg_1043" s="T106">np.h:A</ta>
            <ta e="T109" id="Seg_1044" s="T108">pro:Th</ta>
            <ta e="T110" id="Seg_1045" s="T109">pro.h:Poss</ta>
            <ta e="T111" id="Seg_1046" s="T110">adv:L</ta>
            <ta e="T112" id="Seg_1047" s="T111">pro.h:Poss</ta>
            <ta e="T113" id="Seg_1048" s="T112">np:Th</ta>
            <ta e="T117" id="Seg_1049" s="T116">np:Th</ta>
            <ta e="T120" id="Seg_1050" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_1051" s="T120">pro.h:E</ta>
            <ta e="T122" id="Seg_1052" s="T121">0.1.h:A</ta>
            <ta e="T123" id="Seg_1053" s="T122">adv:Time</ta>
            <ta e="T126" id="Seg_1054" s="T125">pro.h:A</ta>
            <ta e="T127" id="Seg_1055" s="T126">np.h:E</ta>
            <ta e="T129" id="Seg_1056" s="T128">np.h:A</ta>
            <ta e="T133" id="Seg_1057" s="T132">adv:Time</ta>
            <ta e="T134" id="Seg_1058" s="T133">pro.h:A</ta>
            <ta e="T136" id="Seg_1059" s="T135">np:G</ta>
            <ta e="T138" id="Seg_1060" s="T137">pro.h:A</ta>
            <ta e="T141" id="Seg_1061" s="T140">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T61" id="Seg_1062" s="T60">v:pred 0.2.h:S</ta>
            <ta e="T62" id="Seg_1063" s="T61">np:O</ta>
            <ta e="T63" id="Seg_1064" s="T62">v:pred</ta>
            <ta e="T65" id="Seg_1065" s="T64">np.h:S</ta>
            <ta e="T67" id="Seg_1066" s="T66">np.h:S</ta>
            <ta e="T68" id="Seg_1067" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_1068" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T70" id="Seg_1069" s="T69">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_1070" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_1071" s="T73">v:pred 0.2.h:S</ta>
            <ta e="T75" id="Seg_1072" s="T74">pro:S</ta>
            <ta e="T76" id="Seg_1073" s="T75">n:pred</ta>
            <ta e="T79" id="Seg_1074" s="T78">pro:S</ta>
            <ta e="T80" id="Seg_1075" s="T79">n:pred</ta>
            <ta e="T84" id="Seg_1076" s="T83">pro:S</ta>
            <ta e="T85" id="Seg_1077" s="T84">n:pred</ta>
            <ta e="T86" id="Seg_1078" s="T85">np:S</ta>
            <ta e="T87" id="Seg_1079" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_1080" s="T87">pro.h:S</ta>
            <ta e="T89" id="Seg_1081" s="T88">pro.h:O</ta>
            <ta e="T90" id="Seg_1082" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_1083" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_1084" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_1085" s="T94">pro.h:O</ta>
            <ta e="T96" id="Seg_1086" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_1087" s="T97">np.h:S</ta>
            <ta e="T99" id="Seg_1088" s="T98">v:pred</ta>
            <ta e="T102" id="Seg_1089" s="T101">np.h:S</ta>
            <ta e="T103" id="Seg_1090" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1091" s="T103">v:pred 0.3.h:S</ta>
            <ta e="T107" id="Seg_1092" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_1093" s="T107">v:pred</ta>
            <ta e="T113" id="Seg_1094" s="T112">np:S</ta>
            <ta e="T115" id="Seg_1095" s="T114">adj:pred</ta>
            <ta e="T117" id="Seg_1096" s="T116">np:S</ta>
            <ta e="T119" id="Seg_1097" s="T118">adj:pred</ta>
            <ta e="T121" id="Seg_1098" s="T120">pro.h:O</ta>
            <ta e="T122" id="Seg_1099" s="T121">v:pred 0.1.h:S</ta>
            <ta e="T125" id="Seg_1100" s="T124">v:pred</ta>
            <ta e="T126" id="Seg_1101" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_1102" s="T126">np.h:O</ta>
            <ta e="T129" id="Seg_1103" s="T128">np.h:S</ta>
            <ta e="T132" id="Seg_1104" s="T131">v:pred</ta>
            <ta e="T134" id="Seg_1105" s="T133">pro.h:S</ta>
            <ta e="T135" id="Seg_1106" s="T134">v:pred</ta>
            <ta e="T138" id="Seg_1107" s="T137">pro.h:S</ta>
            <ta e="T141" id="Seg_1108" s="T140">np.h:S</ta>
            <ta e="T142" id="Seg_1109" s="T141">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T71" id="Seg_1110" s="T70">RUS:cult</ta>
            <ta e="T78" id="Seg_1111" s="T77">RUS:gram</ta>
            <ta e="T83" id="Seg_1112" s="T82">RUS:gram</ta>
            <ta e="T92" id="Seg_1113" s="T91">TURK:core</ta>
            <ta e="T94" id="Seg_1114" s="T93">RUS:gram</ta>
            <ta e="T107" id="Seg_1115" s="T106">RUS:cult</ta>
            <ta e="T116" id="Seg_1116" s="T115">RUS:gram</ta>
            <ta e="T118" id="Seg_1117" s="T117">RUS:mod</ta>
            <ta e="T127" id="Seg_1118" s="T126">RUS:cult</ta>
            <ta e="T137" id="Seg_1119" s="T136">RUS:gram</ta>
            <ta e="T141" id="Seg_1120" s="T140">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T71" id="Seg_1121" s="T70">dir:infl</ta>
            <ta e="T107" id="Seg_1122" s="T106">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T62" id="Seg_1123" s="T60">Почисти зубы!</ta>
            <ta e="T65" id="Seg_1124" s="T62">Жили три козла.</ta>
            <ta e="T68" id="Seg_1125" s="T65">Шёл большой козёл.</ta>
            <ta e="T70" id="Seg_1126" s="T68">Идёт, идёт.</ta>
            <ta e="T74" id="Seg_1127" s="T70">Волку(?) говорит: "Куда идёшь?</ta>
            <ta e="T76" id="Seg_1128" s="T74">Что на земле?"</ta>
            <ta e="T77" id="Seg_1129" s="T76">"Ноги".</ta>
            <ta e="T80" id="Seg_1130" s="T77">"А что на голове?"</ta>
            <ta e="T82" id="Seg_1131" s="T80">"Рога".</ta>
            <ta e="T85" id="Seg_1132" s="T82">"А что внутри?"</ta>
            <ta e="T87" id="Seg_1133" s="T85">У него сердце замерло.</ta>
            <ta e="T90" id="Seg_1134" s="T87">Он его съел.</ta>
            <ta e="T93" id="Seg_1135" s="T90">Потом другой [козёл] пошёл.</ta>
            <ta e="T96" id="Seg_1136" s="T93">И того он съел.</ta>
            <ta e="T103" id="Seg_1137" s="T96">Потом маленький [козлик] пошёл: "Куда мои братья делись?"</ta>
            <ta e="T104" id="Seg_1138" s="T103">Идёт.</ta>
            <ta e="T108" id="Seg_1139" s="T104">Потом опять волк идёт.</ta>
            <ta e="T111" id="Seg_1140" s="T108">"Что у тебя там?"</ta>
            <ta e="T115" id="Seg_1141" s="T111">"У меня копыта железные.</ta>
            <ta e="T122" id="Seg_1142" s="T115">И рога тоже железные, сейчас я тебя забодаю".</ta>
            <ta e="T127" id="Seg_1143" s="T122">И забодал волка.</ta>
            <ta e="T132" id="Seg_1144" s="T127">Два брата его наружу вышли.</ta>
            <ta e="T142" id="Seg_1145" s="T132">Потом он идёт домой, и два брата его идут.</ta>
            <ta e="T143" id="Seg_1146" s="T142">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T62" id="Seg_1147" s="T60">Brush his (your?) teeth!</ta>
            <ta e="T65" id="Seg_1148" s="T62">Three goats lived.</ta>
            <ta e="T68" id="Seg_1149" s="T65">The big goat went.</ta>
            <ta e="T70" id="Seg_1150" s="T68">It comes, it comes.</ta>
            <ta e="T74" id="Seg_1151" s="T70">To a wolf (?) it says: "Where are you going?</ta>
            <ta e="T76" id="Seg_1152" s="T74">What is on the ground?"</ta>
            <ta e="T77" id="Seg_1153" s="T76">"Feet."</ta>
            <ta e="T80" id="Seg_1154" s="T77">"And what is on the head?"</ta>
            <ta e="T82" id="Seg_1155" s="T80">"The horns."</ta>
            <ta e="T85" id="Seg_1156" s="T82">"And what is inside?"</ta>
            <ta e="T87" id="Seg_1157" s="T85">Its heart got frightened.</ta>
            <ta e="T90" id="Seg_1158" s="T87">It ate it.</ta>
            <ta e="T93" id="Seg_1159" s="T90">Then another [the second goat] came.</ta>
            <ta e="T96" id="Seg_1160" s="T93">And this one, it also ate it.</ta>
            <ta e="T103" id="Seg_1161" s="T96">Then the small [goat] comes: "Where did my brothers go?"</ta>
            <ta e="T104" id="Seg_1162" s="T103">It comes.</ta>
            <ta e="T108" id="Seg_1163" s="T104">Then the wolf comes again.</ta>
            <ta e="T111" id="Seg_1164" s="T108">"What do you [have] here?"</ta>
            <ta e="T115" id="Seg_1165" s="T111">"My feet are made of iron.</ta>
            <ta e="T122" id="Seg_1166" s="T115">And the horns are also made of iron, now I will punch you.</ta>
            <ta e="T127" id="Seg_1167" s="T122">Then he punched the wolf.</ta>
            <ta e="T132" id="Seg_1168" s="T127">Two of his brothers came out.</ta>
            <ta e="T142" id="Seg_1169" s="T132">Then he goes home and these two brothers of his are coming.</ta>
            <ta e="T143" id="Seg_1170" s="T142">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T62" id="Seg_1171" s="T60">Putz ihm (dir?) die Zähne!</ta>
            <ta e="T65" id="Seg_1172" s="T62">Es lebten drei Ziegenböcke.</ta>
            <ta e="T68" id="Seg_1173" s="T65">Der große Ziegenbock ging.</ta>
            <ta e="T70" id="Seg_1174" s="T68">Er kommt, Er kommt.</ta>
            <ta e="T74" id="Seg_1175" s="T70">Zu einem Wolf (?) sagt er: „Wohin gehst du?</ta>
            <ta e="T76" id="Seg_1176" s="T74">Was ist da auf dem Boden?“</ta>
            <ta e="T77" id="Seg_1177" s="T76">„Füße.“</ta>
            <ta e="T80" id="Seg_1178" s="T77">„Und was ist auf dem Kopf?“</ta>
            <ta e="T82" id="Seg_1179" s="T80">„Die Hörner.“</ta>
            <ta e="T85" id="Seg_1180" s="T82">„Und was ist drinnen?“</ta>
            <ta e="T87" id="Seg_1181" s="T85">Sein Herz bekam Angst.</ta>
            <ta e="T90" id="Seg_1182" s="T87">Er fraß ihn auf.</ta>
            <ta e="T93" id="Seg_1183" s="T90">Dann kam ein anderer [der zweite Ziegenbock].</ta>
            <ta e="T96" id="Seg_1184" s="T93">Und diesen fraß er auch auf.</ta>
            <ta e="T103" id="Seg_1185" s="T96">Dann kommt der kleine [Ziegenbock]: „Wo sind meine Brüder hingegangen?“</ta>
            <ta e="T104" id="Seg_1186" s="T103">Er kommt.</ta>
            <ta e="T108" id="Seg_1187" s="T104">Dann kommt wieder der Wolf.</ta>
            <ta e="T111" id="Seg_1188" s="T108">„Was [hast] du hier?“</ta>
            <ta e="T115" id="Seg_1189" s="T111">„Meine Füße sind aus Eisen.</ta>
            <ta e="T122" id="Seg_1190" s="T115">Und die Hörner sind aus Eisen, nun werde ich dich stoßen.</ta>
            <ta e="T127" id="Seg_1191" s="T122">Dann stieß er den Wolf.</ta>
            <ta e="T132" id="Seg_1192" s="T127">Zwei seine Brüder kamen heraus.</ta>
            <ta e="T142" id="Seg_1193" s="T132">Dann geht er nach Hause, und diese zwei Brüder von ihm kommen mit.</ta>
            <ta e="T143" id="Seg_1194" s="T142">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T62" id="Seg_1195" s="T60">[GVY:] This sentence may not be part of the text.</ta>
            <ta e="T103" id="Seg_1196" s="T96">[GVY:] kanluʔpiʔi?</ta>
            <ta e="T127" id="Seg_1197" s="T122">[KlT:] goal expression using Russian dative(?).</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
