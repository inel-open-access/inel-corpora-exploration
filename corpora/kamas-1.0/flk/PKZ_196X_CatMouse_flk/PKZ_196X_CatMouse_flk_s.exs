<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID35040EB0-CCE6-16BD-0E52-5821F6D9E00C">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_CatMouse_flk.wav" />
         <referenced-file url="PKZ_196X_CatMouse_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_CatMouse_flk\PKZ_196X_CatMouse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">49</ud-information>
            <ud-information attribute-name="# HIAT:w">29</ud-information>
            <ud-information attribute-name="# e">30</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T458" time="0.0" type="appl" />
         <tli id="T459" time="0.664" type="appl" />
         <tli id="T460" time="1.328" type="appl" />
         <tli id="T461" time="1.992" type="appl" />
         <tli id="T462" time="3.168" type="appl" />
         <tli id="T463" time="4.343" type="appl" />
         <tli id="T464" time="5.519" type="appl" />
         <tli id="T465" time="6.274" type="appl" />
         <tli id="T466" time="7.03" type="appl" />
         <tli id="T467" time="7.785" type="appl" />
         <tli id="T468" time="8.658729574287888" />
         <tli id="T469" time="10.098518325670632" />
         <tli id="T470" time="10.908" type="appl" />
         <tli id="T471" time="11.444" type="appl" />
         <tli id="T472" time="12.104890613477139" />
         <tli id="T473" time="12.822" type="appl" />
         <tli id="T474" time="13.509" type="appl" />
         <tli id="T475" time="14.196" type="appl" />
         <tli id="T476" time="14.883" type="appl" />
         <tli id="T477" time="15.57" type="appl" />
         <tli id="T478" time="16.257" type="appl" />
         <tli id="T479" time="18.090679033577615" />
         <tli id="T480" time="18.93" type="appl" />
         <tli id="T481" time="19.823758086167956" />
         <tli id="T482" time="22.110089297854444" />
         <tli id="T483" time="23.15" type="appl" />
         <tli id="T484" time="23.868" type="appl" />
         <tli id="T485" time="24.585" type="appl" />
         <tli id="T486" time="25.303" type="appl" />
         <tli id="T487" time="26.021" type="appl" />
         <tli id="T488" time="27.116" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T488" id="Seg_0" n="sc" s="T458">
               <ts e="T461" id="Seg_2" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_4" n="HIAT:w" s="T458">Tospak</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_7" n="HIAT:w" s="T459">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_10" n="HIAT:w" s="T460">tumo</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_14" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_16" n="HIAT:w" s="T461">Tospak</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_19" n="HIAT:w" s="T462">badəbi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_22" n="HIAT:w" s="T463">tumo</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_26" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_28" n="HIAT:w" s="T464">Dĭ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_31" n="HIAT:w" s="T465">nuʔməluʔpi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_34" n="HIAT:w" s="T466">norkanə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <nts id="Seg_37" n="HIAT:ip">(</nts>
                  <ats e="T468" id="Seg_38" n="HIAT:non-pho" s="T467">…</ats>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip">)</nts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_44" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_46" n="HIAT:w" s="T468">Paʔlaːmbi</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_50" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_52" n="HIAT:w" s="T469">Măndə:</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_55" n="HIAT:w" s="T470">Šoʔ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_58" n="HIAT:w" s="T471">döbər</ts>
                  <nts id="Seg_59" n="HIAT:ip">!</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_62" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_64" n="HIAT:w" s="T472">Nuʔməʔ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_67" n="HIAT:w" s="T473">iššo</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_71" n="HIAT:w" s="T474">măn</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_74" n="HIAT:w" s="T475">tănan</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_77" n="HIAT:w" s="T476">budəj</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_80" n="HIAT:w" s="T477">ipek</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_83" n="HIAT:w" s="T478">mĭlem</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_87" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_89" n="HIAT:w" s="T479">Celɨj</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_92" n="HIAT:w" s="T480">băra</ts>
                  <nts id="Seg_93" n="HIAT:ip">"</nts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_97" n="HIAT:u" s="T481">
                  <nts id="Seg_98" n="HIAT:ip">"</nts>
                  <ts e="T482" id="Seg_100" n="HIAT:w" s="T481">Dʼok</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_104" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_106" n="HIAT:w" s="T482">Amga</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_109" n="HIAT:w" s="T483">nuʔməsʼtə</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_113" n="HIAT:w" s="T484">a</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_116" n="HIAT:w" s="T485">mĭliel</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_119" n="HIAT:w" s="T486">iʔgö</ts>
                  <nts id="Seg_120" n="HIAT:ip">"</nts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_124" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_126" n="HIAT:w" s="T487">Kabarləj</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T488" id="Seg_129" n="sc" s="T458">
               <ts e="T459" id="Seg_131" n="e" s="T458">Tospak </ts>
               <ts e="T460" id="Seg_133" n="e" s="T459">i </ts>
               <ts e="T461" id="Seg_135" n="e" s="T460">tumo. </ts>
               <ts e="T462" id="Seg_137" n="e" s="T461">Tospak </ts>
               <ts e="T463" id="Seg_139" n="e" s="T462">badəbi </ts>
               <ts e="T464" id="Seg_141" n="e" s="T463">tumo. </ts>
               <ts e="T465" id="Seg_143" n="e" s="T464">Dĭ </ts>
               <ts e="T466" id="Seg_145" n="e" s="T465">nuʔməluʔpi </ts>
               <ts e="T467" id="Seg_147" n="e" s="T466">norkanə </ts>
               <ts e="T468" id="Seg_149" n="e" s="T467">((…)). </ts>
               <ts e="T469" id="Seg_151" n="e" s="T468">Paʔlaːmbi. </ts>
               <ts e="T470" id="Seg_153" n="e" s="T469">Măndə: </ts>
               <ts e="T471" id="Seg_155" n="e" s="T470">Šoʔ </ts>
               <ts e="T472" id="Seg_157" n="e" s="T471">döbər! </ts>
               <ts e="T473" id="Seg_159" n="e" s="T472">Nuʔməʔ </ts>
               <ts e="T474" id="Seg_161" n="e" s="T473">iššo, </ts>
               <ts e="T475" id="Seg_163" n="e" s="T474">măn </ts>
               <ts e="T476" id="Seg_165" n="e" s="T475">tănan </ts>
               <ts e="T477" id="Seg_167" n="e" s="T476">budəj </ts>
               <ts e="T478" id="Seg_169" n="e" s="T477">ipek </ts>
               <ts e="T479" id="Seg_171" n="e" s="T478">mĭlem. </ts>
               <ts e="T480" id="Seg_173" n="e" s="T479">Celɨj </ts>
               <ts e="T481" id="Seg_175" n="e" s="T480">băra". </ts>
               <ts e="T482" id="Seg_177" n="e" s="T481">"Dʼok. </ts>
               <ts e="T483" id="Seg_179" n="e" s="T482">Amga </ts>
               <ts e="T484" id="Seg_181" n="e" s="T483">nuʔməsʼtə, </ts>
               <ts e="T485" id="Seg_183" n="e" s="T484">a </ts>
               <ts e="T486" id="Seg_185" n="e" s="T485">mĭliel </ts>
               <ts e="T487" id="Seg_187" n="e" s="T486">iʔgö". </ts>
               <ts e="T488" id="Seg_189" n="e" s="T487">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T461" id="Seg_190" s="T458">PKZ_196X_CatMouse_flk.001 (001)</ta>
            <ta e="T464" id="Seg_191" s="T461">PKZ_196X_CatMouse_flk.002 (002)</ta>
            <ta e="T468" id="Seg_192" s="T464">PKZ_196X_CatMouse_flk.003 (003)</ta>
            <ta e="T469" id="Seg_193" s="T468">PKZ_196X_CatMouse_flk.004 (004)</ta>
            <ta e="T472" id="Seg_194" s="T469">PKZ_196X_CatMouse_flk.005 (005)</ta>
            <ta e="T479" id="Seg_195" s="T472">PKZ_196X_CatMouse_flk.006 (006)</ta>
            <ta e="T481" id="Seg_196" s="T479">PKZ_196X_CatMouse_flk.007 (007)</ta>
            <ta e="T482" id="Seg_197" s="T481">PKZ_196X_CatMouse_flk.008 (008)</ta>
            <ta e="T487" id="Seg_198" s="T482">PKZ_196X_CatMouse_flk.009 (009)</ta>
            <ta e="T488" id="Seg_199" s="T487">PKZ_196X_CatMouse_flk.010 (010)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T461" id="Seg_200" s="T458">Tospak i tumo. </ta>
            <ta e="T464" id="Seg_201" s="T461">Tospak badəbi tumo. </ta>
            <ta e="T468" id="Seg_202" s="T464">Dĭ nuʔməluʔpi norkanə ((…)). </ta>
            <ta e="T469" id="Seg_203" s="T468">Paʔlaːmbi. </ta>
            <ta e="T472" id="Seg_204" s="T469">Măndə:" Šoʔ döbər! </ta>
            <ta e="T479" id="Seg_205" s="T472">Nuʔməʔ iššo, măn tănan budəj ipek mĭlem. </ta>
            <ta e="T481" id="Seg_206" s="T479">Celɨj băra". </ta>
            <ta e="T482" id="Seg_207" s="T481">"Dʼok. </ta>
            <ta e="T487" id="Seg_208" s="T482">Amga nuʔməsʼtə, a mĭliel iʔgö". </ta>
            <ta e="T488" id="Seg_209" s="T487">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T459" id="Seg_210" s="T458">tospak</ta>
            <ta e="T460" id="Seg_211" s="T459">i</ta>
            <ta e="T461" id="Seg_212" s="T460">tumo</ta>
            <ta e="T462" id="Seg_213" s="T461">tospak</ta>
            <ta e="T463" id="Seg_214" s="T462">bădə-bi</ta>
            <ta e="T464" id="Seg_215" s="T463">tumo</ta>
            <ta e="T465" id="Seg_216" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_217" s="T465">nuʔmə-luʔ-pi</ta>
            <ta e="T467" id="Seg_218" s="T466">norka-nə</ta>
            <ta e="T469" id="Seg_219" s="T468">paʔ-laːm-bi</ta>
            <ta e="T470" id="Seg_220" s="T469">măn-də</ta>
            <ta e="T471" id="Seg_221" s="T470">šo-ʔ</ta>
            <ta e="T472" id="Seg_222" s="T471">döbər</ta>
            <ta e="T473" id="Seg_223" s="T472">nuʔmə-ʔ</ta>
            <ta e="T474" id="Seg_224" s="T473">iššo</ta>
            <ta e="T475" id="Seg_225" s="T474">măn</ta>
            <ta e="T476" id="Seg_226" s="T475">tănan</ta>
            <ta e="T477" id="Seg_227" s="T476">budəj</ta>
            <ta e="T478" id="Seg_228" s="T477">ipek</ta>
            <ta e="T479" id="Seg_229" s="T478">mĭ-le-m</ta>
            <ta e="T480" id="Seg_230" s="T479">celɨj</ta>
            <ta e="T481" id="Seg_231" s="T480">băra</ta>
            <ta e="T482" id="Seg_232" s="T481">dʼok</ta>
            <ta e="T483" id="Seg_233" s="T482">amga</ta>
            <ta e="T484" id="Seg_234" s="T483">nuʔmə-sʼtə</ta>
            <ta e="T485" id="Seg_235" s="T484">a</ta>
            <ta e="T486" id="Seg_236" s="T485">mĭ-lie-l</ta>
            <ta e="T487" id="Seg_237" s="T486">iʔgö</ta>
            <ta e="T488" id="Seg_238" s="T487">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T459" id="Seg_239" s="T458">tospak</ta>
            <ta e="T460" id="Seg_240" s="T459">i</ta>
            <ta e="T461" id="Seg_241" s="T460">tumo</ta>
            <ta e="T462" id="Seg_242" s="T461">tospak</ta>
            <ta e="T463" id="Seg_243" s="T462">bădə-bi</ta>
            <ta e="T464" id="Seg_244" s="T463">tumo</ta>
            <ta e="T465" id="Seg_245" s="T464">dĭ</ta>
            <ta e="T466" id="Seg_246" s="T465">nuʔmə-luʔbdə-bi</ta>
            <ta e="T467" id="Seg_247" s="T466">norka-Tə</ta>
            <ta e="T469" id="Seg_248" s="T468">paʔ-laːm-bi</ta>
            <ta e="T470" id="Seg_249" s="T469">măn-ntə</ta>
            <ta e="T471" id="Seg_250" s="T470">šo-ʔ</ta>
            <ta e="T472" id="Seg_251" s="T471">döbər</ta>
            <ta e="T473" id="Seg_252" s="T472">nuʔmə-ʔ</ta>
            <ta e="T474" id="Seg_253" s="T473">ĭššo</ta>
            <ta e="T475" id="Seg_254" s="T474">măn</ta>
            <ta e="T476" id="Seg_255" s="T475">tănan</ta>
            <ta e="T477" id="Seg_256" s="T476">budəj</ta>
            <ta e="T478" id="Seg_257" s="T477">ipek</ta>
            <ta e="T479" id="Seg_258" s="T478">mĭ-lV-m</ta>
            <ta e="T480" id="Seg_259" s="T479">celɨj</ta>
            <ta e="T481" id="Seg_260" s="T480">băra</ta>
            <ta e="T482" id="Seg_261" s="T481">dʼok</ta>
            <ta e="T483" id="Seg_262" s="T482">amka</ta>
            <ta e="T484" id="Seg_263" s="T483">nuʔmə-zittə</ta>
            <ta e="T485" id="Seg_264" s="T484">a</ta>
            <ta e="T486" id="Seg_265" s="T485">mĭ-liA-l</ta>
            <ta e="T487" id="Seg_266" s="T486">iʔgö</ta>
            <ta e="T488" id="Seg_267" s="T487">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T459" id="Seg_268" s="T458">cat.[NOM.SG]</ta>
            <ta e="T460" id="Seg_269" s="T459">and</ta>
            <ta e="T461" id="Seg_270" s="T460">mouse.[NOM.SG]</ta>
            <ta e="T462" id="Seg_271" s="T461">cat.[NOM.SG]</ta>
            <ta e="T463" id="Seg_272" s="T462">feed-PST.[3SG]</ta>
            <ta e="T464" id="Seg_273" s="T463">mouse.[NOM.SG]</ta>
            <ta e="T465" id="Seg_274" s="T464">this.[NOM.SG]</ta>
            <ta e="T466" id="Seg_275" s="T465">run-MOM-PST.[3SG]</ta>
            <ta e="T467" id="Seg_276" s="T466">burrow-LAT</ta>
            <ta e="T469" id="Seg_277" s="T468">climb-RES-PST.[3SG]</ta>
            <ta e="T470" id="Seg_278" s="T469">say-IPFVZ.[3SG]</ta>
            <ta e="T471" id="Seg_279" s="T470">come-IMP.2SG</ta>
            <ta e="T472" id="Seg_280" s="T471">here</ta>
            <ta e="T473" id="Seg_281" s="T472">run-IMP.2SG</ta>
            <ta e="T474" id="Seg_282" s="T473">more</ta>
            <ta e="T475" id="Seg_283" s="T474">I.NOM</ta>
            <ta e="T476" id="Seg_284" s="T475">you.DAT</ta>
            <ta e="T477" id="Seg_285" s="T476">wheat.[NOM.SG]</ta>
            <ta e="T478" id="Seg_286" s="T477">bread.[NOM.SG]</ta>
            <ta e="T479" id="Seg_287" s="T478">give-FUT-1SG</ta>
            <ta e="T480" id="Seg_288" s="T479">whole.[NOM.SG]</ta>
            <ta e="T481" id="Seg_289" s="T480">sack.[NOM.SG]</ta>
            <ta e="T482" id="Seg_290" s="T481">no</ta>
            <ta e="T483" id="Seg_291" s="T482">few</ta>
            <ta e="T484" id="Seg_292" s="T483">run-INF.LAT</ta>
            <ta e="T485" id="Seg_293" s="T484">and</ta>
            <ta e="T486" id="Seg_294" s="T485">give-PRS-2SG</ta>
            <ta e="T487" id="Seg_295" s="T486">many</ta>
            <ta e="T488" id="Seg_296" s="T487">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T459" id="Seg_297" s="T458">кот.[NOM.SG]</ta>
            <ta e="T460" id="Seg_298" s="T459">и</ta>
            <ta e="T461" id="Seg_299" s="T460">мышь.[NOM.SG]</ta>
            <ta e="T462" id="Seg_300" s="T461">кот.[NOM.SG]</ta>
            <ta e="T463" id="Seg_301" s="T462">кормить-PST.[3SG]</ta>
            <ta e="T464" id="Seg_302" s="T463">мышь.[NOM.SG]</ta>
            <ta e="T465" id="Seg_303" s="T464">этот.[NOM.SG]</ta>
            <ta e="T466" id="Seg_304" s="T465">бежать-MOM-PST.[3SG]</ta>
            <ta e="T467" id="Seg_305" s="T466">норка-LAT</ta>
            <ta e="T469" id="Seg_306" s="T468">лезть-RES-PST.[3SG]</ta>
            <ta e="T470" id="Seg_307" s="T469">сказать-IPFVZ.[3SG]</ta>
            <ta e="T471" id="Seg_308" s="T470">прийти-IMP.2SG</ta>
            <ta e="T472" id="Seg_309" s="T471">здесь</ta>
            <ta e="T473" id="Seg_310" s="T472">бежать-IMP.2SG</ta>
            <ta e="T474" id="Seg_311" s="T473">еще</ta>
            <ta e="T475" id="Seg_312" s="T474">я.NOM</ta>
            <ta e="T476" id="Seg_313" s="T475">ты.DAT</ta>
            <ta e="T477" id="Seg_314" s="T476">мука.[NOM.SG]</ta>
            <ta e="T478" id="Seg_315" s="T477">хлеб.[NOM.SG]</ta>
            <ta e="T479" id="Seg_316" s="T478">дать-FUT-1SG</ta>
            <ta e="T480" id="Seg_317" s="T479">целый.[NOM.SG]</ta>
            <ta e="T481" id="Seg_318" s="T480">мешок.[NOM.SG]</ta>
            <ta e="T482" id="Seg_319" s="T481">нет</ta>
            <ta e="T483" id="Seg_320" s="T482">мало</ta>
            <ta e="T484" id="Seg_321" s="T483">бежать-INF.LAT</ta>
            <ta e="T485" id="Seg_322" s="T484">а</ta>
            <ta e="T486" id="Seg_323" s="T485">дать-PRS-2SG</ta>
            <ta e="T487" id="Seg_324" s="T486">много</ta>
            <ta e="T488" id="Seg_325" s="T487">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T459" id="Seg_326" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_327" s="T459">conj</ta>
            <ta e="T461" id="Seg_328" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_329" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_330" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_331" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_332" s="T464">dempro-n:case</ta>
            <ta e="T466" id="Seg_333" s="T465">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T467" id="Seg_334" s="T466">n-n:case</ta>
            <ta e="T469" id="Seg_335" s="T468">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_336" s="T469">v-v&gt;v-v:pn</ta>
            <ta e="T471" id="Seg_337" s="T470">v-v:mood.pn</ta>
            <ta e="T472" id="Seg_338" s="T471">adv</ta>
            <ta e="T473" id="Seg_339" s="T472">v-v:mood.pn</ta>
            <ta e="T474" id="Seg_340" s="T473">adv</ta>
            <ta e="T475" id="Seg_341" s="T474">pers</ta>
            <ta e="T476" id="Seg_342" s="T475">pers</ta>
            <ta e="T477" id="Seg_343" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_344" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_345" s="T478">v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_346" s="T479">adj-n:case</ta>
            <ta e="T481" id="Seg_347" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_348" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_349" s="T482">adv</ta>
            <ta e="T484" id="Seg_350" s="T483">v-v:n.fin</ta>
            <ta e="T485" id="Seg_351" s="T484">conj</ta>
            <ta e="T486" id="Seg_352" s="T485">v-v:tense-v:pn</ta>
            <ta e="T487" id="Seg_353" s="T486">quant</ta>
            <ta e="T488" id="Seg_354" s="T487">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T459" id="Seg_355" s="T458">n</ta>
            <ta e="T460" id="Seg_356" s="T459">conj</ta>
            <ta e="T461" id="Seg_357" s="T460">n</ta>
            <ta e="T462" id="Seg_358" s="T461">n</ta>
            <ta e="T463" id="Seg_359" s="T462">v</ta>
            <ta e="T464" id="Seg_360" s="T463">n</ta>
            <ta e="T465" id="Seg_361" s="T464">dempro</ta>
            <ta e="T466" id="Seg_362" s="T465">v</ta>
            <ta e="T467" id="Seg_363" s="T466">n</ta>
            <ta e="T469" id="Seg_364" s="T468">v</ta>
            <ta e="T470" id="Seg_365" s="T469">v</ta>
            <ta e="T471" id="Seg_366" s="T470">v</ta>
            <ta e="T472" id="Seg_367" s="T471">adv</ta>
            <ta e="T473" id="Seg_368" s="T472">v</ta>
            <ta e="T474" id="Seg_369" s="T473">adv</ta>
            <ta e="T475" id="Seg_370" s="T474">pers</ta>
            <ta e="T476" id="Seg_371" s="T475">pers</ta>
            <ta e="T477" id="Seg_372" s="T476">n</ta>
            <ta e="T478" id="Seg_373" s="T477">n</ta>
            <ta e="T479" id="Seg_374" s="T478">v</ta>
            <ta e="T480" id="Seg_375" s="T479">adj</ta>
            <ta e="T481" id="Seg_376" s="T480">n</ta>
            <ta e="T482" id="Seg_377" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_378" s="T482">adv</ta>
            <ta e="T484" id="Seg_379" s="T483">v</ta>
            <ta e="T485" id="Seg_380" s="T484">conj</ta>
            <ta e="T486" id="Seg_381" s="T485">v</ta>
            <ta e="T487" id="Seg_382" s="T486">quant</ta>
            <ta e="T488" id="Seg_383" s="T487">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T462" id="Seg_384" s="T461">np:S</ta>
            <ta e="T463" id="Seg_385" s="T462">v:pred</ta>
            <ta e="T464" id="Seg_386" s="T463">np:O</ta>
            <ta e="T465" id="Seg_387" s="T464">pro:S</ta>
            <ta e="T466" id="Seg_388" s="T465">v:pred</ta>
            <ta e="T469" id="Seg_389" s="T468">0.3:S v:pred</ta>
            <ta e="T470" id="Seg_390" s="T469">0.3:S v:pred</ta>
            <ta e="T471" id="Seg_391" s="T470">0.2:S v:pred</ta>
            <ta e="T473" id="Seg_392" s="T472">0.2:S v:pred</ta>
            <ta e="T475" id="Seg_393" s="T474">pro.h:S</ta>
            <ta e="T478" id="Seg_394" s="T477">np:O</ta>
            <ta e="T479" id="Seg_395" s="T478">v:pred</ta>
            <ta e="T486" id="Seg_396" s="T485">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T462" id="Seg_397" s="T461">np:A</ta>
            <ta e="T464" id="Seg_398" s="T463">np:Th</ta>
            <ta e="T465" id="Seg_399" s="T464">pro:A</ta>
            <ta e="T467" id="Seg_400" s="T466">np:G</ta>
            <ta e="T469" id="Seg_401" s="T468">0.3:A</ta>
            <ta e="T470" id="Seg_402" s="T469">0.3:A</ta>
            <ta e="T471" id="Seg_403" s="T470">0.2:A</ta>
            <ta e="T473" id="Seg_404" s="T472">0.2:A</ta>
            <ta e="T475" id="Seg_405" s="T474">pro.h:A</ta>
            <ta e="T476" id="Seg_406" s="T475">pro.h:R</ta>
            <ta e="T478" id="Seg_407" s="T477">np:Th</ta>
            <ta e="T486" id="Seg_408" s="T485">0.2.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T460" id="Seg_409" s="T459">RUS:gram</ta>
            <ta e="T467" id="Seg_410" s="T466">RUS:cult</ta>
            <ta e="T474" id="Seg_411" s="T473">RUS:mod</ta>
            <ta e="T480" id="Seg_412" s="T479">RUS:core</ta>
            <ta e="T482" id="Seg_413" s="T481">TURK:disc</ta>
            <ta e="T485" id="Seg_414" s="T484">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T461" id="Seg_415" s="T458">Кот и мышь.</ta>
            <ta e="T464" id="Seg_416" s="T461">Кошка [захотела] покормить мышь.</ta>
            <ta e="T468" id="Seg_417" s="T464">[Мышь] забежала в норку.</ta>
            <ta e="T469" id="Seg_418" s="T468">Залезла.</ta>
            <ta e="T472" id="Seg_419" s="T469">Говорит: «Иди сюда!</ta>
            <ta e="T479" id="Seg_420" s="T472">Пробеги ещё [немного], я тебе пшеничного хлеба дам.</ta>
            <ta e="T481" id="Seg_421" s="T479">Целый мешок».</ta>
            <ta e="T482" id="Seg_422" s="T481">«Нет.</ta>
            <ta e="T487" id="Seg_423" s="T482">Бежать немного, а ты дашь много».</ta>
            <ta e="T488" id="Seg_424" s="T487">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T461" id="Seg_425" s="T458">Cat and mouse.</ta>
            <ta e="T464" id="Seg_426" s="T461">A cat [wanted to] give food to a mouse.</ta>
            <ta e="T468" id="Seg_427" s="T464">[The mouse] ran into a burrow.</ta>
            <ta e="T469" id="Seg_428" s="T468">It climbed in.</ta>
            <ta e="T472" id="Seg_429" s="T469">It says: "Come here!</ta>
            <ta e="T479" id="Seg_430" s="T472">Run some more, I will give you wheat bread.</ta>
            <ta e="T481" id="Seg_431" s="T479">A full sack."</ta>
            <ta e="T482" id="Seg_432" s="T481">"No.</ta>
            <ta e="T487" id="Seg_433" s="T482">A little bit to run, and you will give a lot."</ta>
            <ta e="T488" id="Seg_434" s="T487">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T461" id="Seg_435" s="T458">Katze und Maus.</ta>
            <ta e="T464" id="Seg_436" s="T461">Eine Katze [wollte] einer Maus etwas zum Essen geben. </ta>
            <ta e="T468" id="Seg_437" s="T464">[Die Maus] lief in ein Loch.</ta>
            <ta e="T469" id="Seg_438" s="T468">Sie kletterte hinein.</ta>
            <ta e="T472" id="Seg_439" s="T469">Sie sagt: „Komm her!</ta>
            <ta e="T479" id="Seg_440" s="T472">Renn etwas weiter, ich gebe dir Weizenbrot.</ta>
            <ta e="T481" id="Seg_441" s="T479">Ein voller Sack.“</ta>
            <ta e="T482" id="Seg_442" s="T481">„Nein.</ta>
            <ta e="T487" id="Seg_443" s="T482">Ein bisschen rennen, und du gibst viel.“</ta>
            <ta e="T488" id="Seg_444" s="T487">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T468" id="Seg_445" s="T464">[KlT:] норка Ru. 'small burrow'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
