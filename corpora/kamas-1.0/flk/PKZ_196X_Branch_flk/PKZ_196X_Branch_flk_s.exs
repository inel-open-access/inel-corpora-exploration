<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID85125040-ABA9-381B-C224-294C7B4B1539">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Branch_flk.wav" />
         <referenced-file url="PKZ_196X_Branch_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Branch_flk\PKZ_196X_Branch_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">68</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">46</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.652" type="appl" />
         <tli id="T2" time="1.304" type="appl" />
         <tli id="T3" time="1.955" type="appl" />
         <tli id="T4" time="2.607" type="appl" />
         <tli id="T5" time="3.045" type="appl" />
         <tli id="T6" time="3.484" type="appl" />
         <tli id="T7" time="3.922" type="appl" />
         <tli id="T8" time="4.361" type="appl" />
         <tli id="T9" time="4.799" type="appl" />
         <tli id="T10" time="5.375" type="appl" />
         <tli id="T11" time="5.933" type="appl" />
         <tli id="T12" time="7.472525371922607" />
         <tli id="T13" time="9.124" type="appl" />
         <tli id="T14" time="9.773" type="appl" />
         <tli id="T15" time="10.385" type="appl" />
         <tli id="T16" time="10.996" type="appl" />
         <tli id="T17" time="11.608" type="appl" />
         <tli id="T18" time="12.358663728407235" />
         <tli id="T19" time="13.11" type="appl" />
         <tli id="T20" time="14.018484261510473" />
         <tli id="T21" time="14.413" type="appl" />
         <tli id="T22" time="14.938" type="appl" />
         <tli id="T23" time="15.463" type="appl" />
         <tli id="T24" time="15.988" type="appl" />
         <tli id="T25" time="16.68" type="appl" />
         <tli id="T26" time="17.373" type="appl" />
         <tli id="T27" time="18.278023701883843" />
         <tli id="T28" time="18.887" type="appl" />
         <tli id="T29" time="19.371" type="appl" />
         <tli id="T30" time="19.856" type="appl" />
         <tli id="T31" time="20.34" type="appl" />
         <tli id="T32" time="20.825" type="appl" />
         <tli id="T33" time="21.507" type="appl" />
         <tli id="T34" time="22.19" type="appl" />
         <tli id="T35" time="22.872" type="appl" />
         <tli id="T36" time="23.555" type="appl" />
         <tli id="T37" time="24.544012862996464" />
         <tli id="T38" time="25.268" type="appl" />
         <tli id="T39" time="25.894" type="appl" />
         <tli id="T40" time="26.521" type="appl" />
         <tli id="T41" time="27.147" type="appl" />
         <tli id="T42" time="27.773" type="appl" />
         <tli id="T43" time="28.4" type="appl" />
         <tli id="T44" time="29.026" type="appl" />
         <tli id="T45" time="29.652" type="appl" />
         <tli id="T46" time="30.63" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T46" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">mu</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">jaʔlaʔbia</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">A</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">onʼ-</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">onʼiʔ</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">kuza</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">šonəga</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_37" n="HIAT:u" s="T9">
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Ĭmbi</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">tăn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">alaʔbəl</ts>
                  <nts id="Seg_47" n="HIAT:ip">?</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Jaʔlaʔbəl</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_56" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_58" n="HIAT:w" s="T13">Dĭ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">saʔməluʔləj</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">tăn</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">saʔməluʔləl</ts>
                  <nts id="Seg_71" n="HIAT:ip">"</nts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_75" n="HIAT:u" s="T18">
                  <nts id="Seg_76" n="HIAT:ip">"</nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">Kanaʔ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">dĭʔə</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_85" n="HIAT:u" s="T20">
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_88" n="HIAT:w" s="T20">Tăn</ts>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_92" n="HIAT:w" s="T21">ĭmbidə</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">ej</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">tĭmniel</ts>
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_103" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_105" n="HIAT:w" s="T24">Dĭgəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_108" n="HIAT:w" s="T25">jaʔpi</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_112" n="HIAT:w" s="T26">jaʔpi</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_116" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_118" n="HIAT:w" s="T27">Mu</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_121" n="HIAT:w" s="T28">saʔməluʔpi</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_124" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">dĭ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_130" n="HIAT:w" s="T31">saʔməluʔpi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_134" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_136" n="HIAT:w" s="T32">Dĭgəttə:</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_139" n="HIAT:w" s="T33">davaj</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_142" n="HIAT:w" s="T34">bădəsʼtə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">dĭ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">kuzam</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_152" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_154" n="HIAT:w" s="T37">Dĭ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_157" n="HIAT:w" s="T38">nörbələj</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_161" n="HIAT:w" s="T39">kumən</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_164" n="HIAT:w" s="T40">măn</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_167" n="HIAT:w" s="T41">amnolam</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_170" n="HIAT:w" s="T42">alʼi</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_173" n="HIAT:w" s="T43">büžü</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">küləm</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_180" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_182" n="HIAT:w" s="T45">Kabarləj</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T46" id="Seg_185" n="sc" s="T0">
               <ts e="T1" id="Seg_187" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_189" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_191" n="e" s="T2">mu </ts>
               <ts e="T4" id="Seg_193" n="e" s="T3">jaʔlaʔbia. </ts>
               <ts e="T5" id="Seg_195" n="e" s="T4">A </ts>
               <ts e="T6" id="Seg_197" n="e" s="T5">(onʼ-) </ts>
               <ts e="T7" id="Seg_199" n="e" s="T6">onʼiʔ </ts>
               <ts e="T8" id="Seg_201" n="e" s="T7">kuza </ts>
               <ts e="T9" id="Seg_203" n="e" s="T8">šonəga. </ts>
               <ts e="T10" id="Seg_205" n="e" s="T9">"Ĭmbi </ts>
               <ts e="T11" id="Seg_207" n="e" s="T10">tăn </ts>
               <ts e="T12" id="Seg_209" n="e" s="T11">alaʔbəl? </ts>
               <ts e="T13" id="Seg_211" n="e" s="T12">Jaʔlaʔbəl. </ts>
               <ts e="T14" id="Seg_213" n="e" s="T13">Dĭ </ts>
               <ts e="T15" id="Seg_215" n="e" s="T14">saʔməluʔləj </ts>
               <ts e="T16" id="Seg_217" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_219" n="e" s="T16">tăn </ts>
               <ts e="T18" id="Seg_221" n="e" s="T17">saʔməluʔləl". </ts>
               <ts e="T19" id="Seg_223" n="e" s="T18">"Kanaʔ </ts>
               <ts e="T20" id="Seg_225" n="e" s="T19">dĭʔə. </ts>
               <ts e="T21" id="Seg_227" n="e" s="T20">(Tăn) </ts>
               <ts e="T22" id="Seg_229" n="e" s="T21">ĭmbidə </ts>
               <ts e="T23" id="Seg_231" n="e" s="T22">ej </ts>
               <ts e="T24" id="Seg_233" n="e" s="T23">tĭmniel". </ts>
               <ts e="T25" id="Seg_235" n="e" s="T24">Dĭgəttə </ts>
               <ts e="T26" id="Seg_237" n="e" s="T25">jaʔpi, </ts>
               <ts e="T27" id="Seg_239" n="e" s="T26">jaʔpi. </ts>
               <ts e="T28" id="Seg_241" n="e" s="T27">Mu </ts>
               <ts e="T29" id="Seg_243" n="e" s="T28">saʔməluʔpi </ts>
               <ts e="T30" id="Seg_245" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_247" n="e" s="T30">dĭ </ts>
               <ts e="T32" id="Seg_249" n="e" s="T31">saʔməluʔpi. </ts>
               <ts e="T33" id="Seg_251" n="e" s="T32">Dĭgəttə: </ts>
               <ts e="T34" id="Seg_253" n="e" s="T33">davaj </ts>
               <ts e="T35" id="Seg_255" n="e" s="T34">bădəsʼtə </ts>
               <ts e="T36" id="Seg_257" n="e" s="T35">dĭ </ts>
               <ts e="T37" id="Seg_259" n="e" s="T36">kuzam. </ts>
               <ts e="T38" id="Seg_261" n="e" s="T37">Dĭ </ts>
               <ts e="T39" id="Seg_263" n="e" s="T38">nörbələj, </ts>
               <ts e="T40" id="Seg_265" n="e" s="T39">kumən </ts>
               <ts e="T41" id="Seg_267" n="e" s="T40">măn </ts>
               <ts e="T42" id="Seg_269" n="e" s="T41">amnolam </ts>
               <ts e="T43" id="Seg_271" n="e" s="T42">alʼi </ts>
               <ts e="T44" id="Seg_273" n="e" s="T43">büžü </ts>
               <ts e="T45" id="Seg_275" n="e" s="T44">küləm. </ts>
               <ts e="T46" id="Seg_277" n="e" s="T45">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_278" s="T0">PKZ_196X_Branch_flk.001 (001)</ta>
            <ta e="T9" id="Seg_279" s="T4">PKZ_196X_Branch_flk.002 (002)</ta>
            <ta e="T12" id="Seg_280" s="T9">PKZ_196X_Branch_flk.003 (003)</ta>
            <ta e="T13" id="Seg_281" s="T12">PKZ_196X_Branch_flk.004 (004)</ta>
            <ta e="T18" id="Seg_282" s="T13">PKZ_196X_Branch_flk.005 (005)</ta>
            <ta e="T20" id="Seg_283" s="T18">PKZ_196X_Branch_flk.006 (006)</ta>
            <ta e="T24" id="Seg_284" s="T20">PKZ_196X_Branch_flk.007 (007)</ta>
            <ta e="T27" id="Seg_285" s="T24">PKZ_196X_Branch_flk.008 (008)</ta>
            <ta e="T32" id="Seg_286" s="T27">PKZ_196X_Branch_flk.009 (009)</ta>
            <ta e="T37" id="Seg_287" s="T32">PKZ_196X_Branch_flk.010 (010)</ta>
            <ta e="T45" id="Seg_288" s="T37">PKZ_196X_Branch_flk.011 (011)</ta>
            <ta e="T46" id="Seg_289" s="T45">PKZ_196X_Branch_flk.012 (012)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_290" s="T0">Onʼiʔ kuza mu jaʔlaʔbia. </ta>
            <ta e="T9" id="Seg_291" s="T4">A (onʼ-) onʼiʔ kuza šonəga. </ta>
            <ta e="T12" id="Seg_292" s="T9">"Ĭmbi tăn alaʔbəl? </ta>
            <ta e="T13" id="Seg_293" s="T12">Jaʔlaʔbəl. </ta>
            <ta e="T18" id="Seg_294" s="T13">Dĭ saʔməluʔləj i tăn saʔməluʔləl". </ta>
            <ta e="T20" id="Seg_295" s="T18">"Kanaʔ dĭʔə. </ta>
            <ta e="T24" id="Seg_296" s="T20">(Tăn) ĭmbidə ej tĭmniel". </ta>
            <ta e="T27" id="Seg_297" s="T24">Dĭgəttə jaʔpi, jaʔpi. </ta>
            <ta e="T32" id="Seg_298" s="T27">Mu saʔməluʔpi i dĭ saʔməluʔpi. </ta>
            <ta e="T37" id="Seg_299" s="T32">Dĭgəttə: davaj bădəsʼtə dĭ kuzam. </ta>
            <ta e="T45" id="Seg_300" s="T37">Dĭ nörbələj, kumən măn amnolam alʼi büžü küləm. </ta>
            <ta e="T46" id="Seg_301" s="T45">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_302" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_303" s="T1">kuza</ta>
            <ta e="T3" id="Seg_304" s="T2">mu</ta>
            <ta e="T4" id="Seg_305" s="T3">jaʔ-laʔ-bia</ta>
            <ta e="T5" id="Seg_306" s="T4">a</ta>
            <ta e="T7" id="Seg_307" s="T6">onʼiʔ</ta>
            <ta e="T8" id="Seg_308" s="T7">kuza</ta>
            <ta e="T9" id="Seg_309" s="T8">šonə-ga</ta>
            <ta e="T10" id="Seg_310" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_311" s="T10">tăn</ta>
            <ta e="T12" id="Seg_312" s="T11">a-laʔbə-l</ta>
            <ta e="T13" id="Seg_313" s="T12">jaʔ-laʔbə-l</ta>
            <ta e="T14" id="Seg_314" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_315" s="T14">saʔmə-luʔ-lə-j</ta>
            <ta e="T16" id="Seg_316" s="T15">i</ta>
            <ta e="T17" id="Seg_317" s="T16">tăn</ta>
            <ta e="T18" id="Seg_318" s="T17">saʔmə-luʔ-lə-l</ta>
            <ta e="T19" id="Seg_319" s="T18">kan-a-ʔ</ta>
            <ta e="T20" id="Seg_320" s="T19">dĭʔə</ta>
            <ta e="T21" id="Seg_321" s="T20">tăn</ta>
            <ta e="T22" id="Seg_322" s="T21">ĭmbi=də</ta>
            <ta e="T23" id="Seg_323" s="T22">ej</ta>
            <ta e="T24" id="Seg_324" s="T23">tĭm-nie-l</ta>
            <ta e="T25" id="Seg_325" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_326" s="T25">jaʔ-pi</ta>
            <ta e="T27" id="Seg_327" s="T26">jaʔ-pi</ta>
            <ta e="T28" id="Seg_328" s="T27">mu</ta>
            <ta e="T29" id="Seg_329" s="T28">saʔmə-luʔ-pi</ta>
            <ta e="T30" id="Seg_330" s="T29">i</ta>
            <ta e="T31" id="Seg_331" s="T30">dĭ</ta>
            <ta e="T32" id="Seg_332" s="T31">saʔmə-luʔ-pi</ta>
            <ta e="T33" id="Seg_333" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_334" s="T33">davaj</ta>
            <ta e="T35" id="Seg_335" s="T34">bădə-sʼtə</ta>
            <ta e="T36" id="Seg_336" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_337" s="T36">kuza-m</ta>
            <ta e="T38" id="Seg_338" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_339" s="T38">nörbə-lə-j</ta>
            <ta e="T40" id="Seg_340" s="T39">kumən</ta>
            <ta e="T41" id="Seg_341" s="T40">măn</ta>
            <ta e="T42" id="Seg_342" s="T41">amno-la-m</ta>
            <ta e="T43" id="Seg_343" s="T42">alʼi</ta>
            <ta e="T44" id="Seg_344" s="T43">büžü</ta>
            <ta e="T45" id="Seg_345" s="T44">kü-lə-m</ta>
            <ta e="T46" id="Seg_346" s="T45">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_347" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_348" s="T1">kuza</ta>
            <ta e="T3" id="Seg_349" s="T2">mu</ta>
            <ta e="T4" id="Seg_350" s="T3">hʼaʔ-laʔbə-bi</ta>
            <ta e="T5" id="Seg_351" s="T4">a</ta>
            <ta e="T7" id="Seg_352" s="T6">onʼiʔ</ta>
            <ta e="T8" id="Seg_353" s="T7">kuza</ta>
            <ta e="T9" id="Seg_354" s="T8">šonə-gA</ta>
            <ta e="T10" id="Seg_355" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_356" s="T10">tăn</ta>
            <ta e="T12" id="Seg_357" s="T11">a-laʔbə-l</ta>
            <ta e="T13" id="Seg_358" s="T12">hʼaʔ-laʔbə-l</ta>
            <ta e="T14" id="Seg_359" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_360" s="T14">saʔmə-luʔbdə-lV-j</ta>
            <ta e="T16" id="Seg_361" s="T15">i</ta>
            <ta e="T17" id="Seg_362" s="T16">tăn</ta>
            <ta e="T18" id="Seg_363" s="T17">saʔmə-luʔbdə-lV-l</ta>
            <ta e="T19" id="Seg_364" s="T18">kan-ə-ʔ</ta>
            <ta e="T20" id="Seg_365" s="T19">dĭʔə</ta>
            <ta e="T21" id="Seg_366" s="T20">tăn</ta>
            <ta e="T22" id="Seg_367" s="T21">ĭmbi=də</ta>
            <ta e="T23" id="Seg_368" s="T22">ej</ta>
            <ta e="T24" id="Seg_369" s="T23">tĭm-liA-l</ta>
            <ta e="T25" id="Seg_370" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_371" s="T25">hʼaʔ-bi</ta>
            <ta e="T27" id="Seg_372" s="T26">hʼaʔ-bi</ta>
            <ta e="T28" id="Seg_373" s="T27">mu</ta>
            <ta e="T29" id="Seg_374" s="T28">saʔmə-luʔbdə-bi</ta>
            <ta e="T30" id="Seg_375" s="T29">i</ta>
            <ta e="T31" id="Seg_376" s="T30">dĭ</ta>
            <ta e="T32" id="Seg_377" s="T31">saʔmə-luʔbdə-bi</ta>
            <ta e="T33" id="Seg_378" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_379" s="T33">davaj</ta>
            <ta e="T35" id="Seg_380" s="T34">bădə-zittə</ta>
            <ta e="T36" id="Seg_381" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_382" s="T36">kuza-m</ta>
            <ta e="T38" id="Seg_383" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_384" s="T38">nörbə-lV-j</ta>
            <ta e="T40" id="Seg_385" s="T39">kumən</ta>
            <ta e="T41" id="Seg_386" s="T40">măn</ta>
            <ta e="T42" id="Seg_387" s="T41">amno-lV-m</ta>
            <ta e="T43" id="Seg_388" s="T42">aľi</ta>
            <ta e="T44" id="Seg_389" s="T43">büžü</ta>
            <ta e="T45" id="Seg_390" s="T44">kü-lV-m</ta>
            <ta e="T46" id="Seg_391" s="T45">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_392" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_393" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_394" s="T2">branch.[NOM.SG]</ta>
            <ta e="T4" id="Seg_395" s="T3">cut-DUR-PST.[3SG]</ta>
            <ta e="T5" id="Seg_396" s="T4">and</ta>
            <ta e="T7" id="Seg_397" s="T6">one.[NOM.SG]</ta>
            <ta e="T8" id="Seg_398" s="T7">man.[NOM.SG]</ta>
            <ta e="T9" id="Seg_399" s="T8">come-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_400" s="T9">what.[NOM.SG]</ta>
            <ta e="T11" id="Seg_401" s="T10">you.GEN</ta>
            <ta e="T12" id="Seg_402" s="T11">make-DUR-2SG</ta>
            <ta e="T13" id="Seg_403" s="T12">cut-DUR-2SG</ta>
            <ta e="T14" id="Seg_404" s="T13">this.[NOM.SG]</ta>
            <ta e="T15" id="Seg_405" s="T14">fall-MOM-FUT-3SG</ta>
            <ta e="T16" id="Seg_406" s="T15">and</ta>
            <ta e="T17" id="Seg_407" s="T16">you.GEN</ta>
            <ta e="T18" id="Seg_408" s="T17">fall-MOM-FUT-2SG</ta>
            <ta e="T19" id="Seg_409" s="T18">go-EP-IMP.2SG</ta>
            <ta e="T20" id="Seg_410" s="T19">from.there</ta>
            <ta e="T21" id="Seg_411" s="T20">you.GEN</ta>
            <ta e="T22" id="Seg_412" s="T21">what.[NOM.SG]=INDEF</ta>
            <ta e="T23" id="Seg_413" s="T22">NEG</ta>
            <ta e="T24" id="Seg_414" s="T23">know-PRS-2SG</ta>
            <ta e="T25" id="Seg_415" s="T24">then</ta>
            <ta e="T26" id="Seg_416" s="T25">cut-PST.[3SG]</ta>
            <ta e="T27" id="Seg_417" s="T26">cut-PST.[3SG]</ta>
            <ta e="T28" id="Seg_418" s="T27">branch.[NOM.SG]</ta>
            <ta e="T29" id="Seg_419" s="T28">fall-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_420" s="T29">and</ta>
            <ta e="T31" id="Seg_421" s="T30">this.[NOM.SG]</ta>
            <ta e="T32" id="Seg_422" s="T31">fall-MOM-PST.[3SG]</ta>
            <ta e="T33" id="Seg_423" s="T32">then</ta>
            <ta e="T34" id="Seg_424" s="T33">HORT</ta>
            <ta e="T35" id="Seg_425" s="T34">feed-INF.LAT</ta>
            <ta e="T36" id="Seg_426" s="T35">this.[NOM.SG]</ta>
            <ta e="T37" id="Seg_427" s="T36">man-ACC</ta>
            <ta e="T38" id="Seg_428" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_429" s="T38">tell-FUT-3SG</ta>
            <ta e="T40" id="Seg_430" s="T39">how.much</ta>
            <ta e="T41" id="Seg_431" s="T40">I.NOM</ta>
            <ta e="T42" id="Seg_432" s="T41">live-FUT-1SG</ta>
            <ta e="T43" id="Seg_433" s="T42">or</ta>
            <ta e="T44" id="Seg_434" s="T43">soon</ta>
            <ta e="T45" id="Seg_435" s="T44">die-FUT-1SG</ta>
            <ta e="T46" id="Seg_436" s="T45">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_437" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_438" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_439" s="T2">ветка.[NOM.SG]</ta>
            <ta e="T4" id="Seg_440" s="T3">резать-DUR-PST.[3SG]</ta>
            <ta e="T5" id="Seg_441" s="T4">а</ta>
            <ta e="T7" id="Seg_442" s="T6">один.[NOM.SG]</ta>
            <ta e="T8" id="Seg_443" s="T7">мужчина.[NOM.SG]</ta>
            <ta e="T9" id="Seg_444" s="T8">прийти-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_445" s="T9">что.[NOM.SG]</ta>
            <ta e="T11" id="Seg_446" s="T10">ты.GEN</ta>
            <ta e="T12" id="Seg_447" s="T11">делать-DUR-2SG</ta>
            <ta e="T13" id="Seg_448" s="T12">резать-DUR-2SG</ta>
            <ta e="T14" id="Seg_449" s="T13">этот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_450" s="T14">упасть-MOM-FUT-3SG</ta>
            <ta e="T16" id="Seg_451" s="T15">и</ta>
            <ta e="T17" id="Seg_452" s="T16">ты.GEN</ta>
            <ta e="T18" id="Seg_453" s="T17">упасть-MOM-FUT-2SG</ta>
            <ta e="T19" id="Seg_454" s="T18">пойти-EP-IMP.2SG</ta>
            <ta e="T20" id="Seg_455" s="T19">оттуда</ta>
            <ta e="T21" id="Seg_456" s="T20">ты.GEN</ta>
            <ta e="T22" id="Seg_457" s="T21">что.[NOM.SG]=INDEF</ta>
            <ta e="T23" id="Seg_458" s="T22">NEG</ta>
            <ta e="T24" id="Seg_459" s="T23">знать-PRS-2SG</ta>
            <ta e="T25" id="Seg_460" s="T24">тогда</ta>
            <ta e="T26" id="Seg_461" s="T25">резать-PST.[3SG]</ta>
            <ta e="T27" id="Seg_462" s="T26">резать-PST.[3SG]</ta>
            <ta e="T28" id="Seg_463" s="T27">ветка.[NOM.SG]</ta>
            <ta e="T29" id="Seg_464" s="T28">упасть-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_465" s="T29">и</ta>
            <ta e="T31" id="Seg_466" s="T30">этот.[NOM.SG]</ta>
            <ta e="T32" id="Seg_467" s="T31">упасть-MOM-PST.[3SG]</ta>
            <ta e="T33" id="Seg_468" s="T32">тогда</ta>
            <ta e="T34" id="Seg_469" s="T33">HORT</ta>
            <ta e="T35" id="Seg_470" s="T34">кормить-INF.LAT</ta>
            <ta e="T36" id="Seg_471" s="T35">этот.[NOM.SG]</ta>
            <ta e="T37" id="Seg_472" s="T36">мужчина-ACC</ta>
            <ta e="T38" id="Seg_473" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_474" s="T38">сказать-FUT-3SG</ta>
            <ta e="T40" id="Seg_475" s="T39">сколько</ta>
            <ta e="T41" id="Seg_476" s="T40">я.NOM</ta>
            <ta e="T42" id="Seg_477" s="T41">жить-FUT-1SG</ta>
            <ta e="T43" id="Seg_478" s="T42">или</ta>
            <ta e="T44" id="Seg_479" s="T43">скоро</ta>
            <ta e="T45" id="Seg_480" s="T44">умереть-FUT-1SG</ta>
            <ta e="T46" id="Seg_481" s="T45">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_482" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_483" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_484" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_485" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_486" s="T4">conj</ta>
            <ta e="T7" id="Seg_487" s="T6">num-n:case</ta>
            <ta e="T8" id="Seg_488" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_489" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_490" s="T9">que-n:case</ta>
            <ta e="T11" id="Seg_491" s="T10">pers</ta>
            <ta e="T12" id="Seg_492" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_493" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_494" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_495" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_496" s="T15">conj</ta>
            <ta e="T17" id="Seg_497" s="T16">pers</ta>
            <ta e="T18" id="Seg_498" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_499" s="T18">v-v:ins-v:mood.pn</ta>
            <ta e="T20" id="Seg_500" s="T19">adv</ta>
            <ta e="T21" id="Seg_501" s="T20">pers</ta>
            <ta e="T22" id="Seg_502" s="T21">que-n:case=ptcl</ta>
            <ta e="T23" id="Seg_503" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_504" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_505" s="T24">adv</ta>
            <ta e="T26" id="Seg_506" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_507" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_508" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_509" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_510" s="T29">conj</ta>
            <ta e="T31" id="Seg_511" s="T30">dempro-n:case</ta>
            <ta e="T32" id="Seg_512" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_513" s="T32">adv</ta>
            <ta e="T34" id="Seg_514" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_515" s="T34">v-v:n.fin</ta>
            <ta e="T36" id="Seg_516" s="T35">dempro-n:case</ta>
            <ta e="T37" id="Seg_517" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_518" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_519" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_520" s="T39">adv</ta>
            <ta e="T41" id="Seg_521" s="T40">pers</ta>
            <ta e="T42" id="Seg_522" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_523" s="T42">conj</ta>
            <ta e="T44" id="Seg_524" s="T43">adv</ta>
            <ta e="T45" id="Seg_525" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_526" s="T45">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_527" s="T0">num</ta>
            <ta e="T2" id="Seg_528" s="T1">n</ta>
            <ta e="T3" id="Seg_529" s="T2">n</ta>
            <ta e="T4" id="Seg_530" s="T3">v</ta>
            <ta e="T5" id="Seg_531" s="T4">conj</ta>
            <ta e="T7" id="Seg_532" s="T6">num</ta>
            <ta e="T8" id="Seg_533" s="T7">n</ta>
            <ta e="T9" id="Seg_534" s="T8">v</ta>
            <ta e="T10" id="Seg_535" s="T9">que</ta>
            <ta e="T11" id="Seg_536" s="T10">pers</ta>
            <ta e="T12" id="Seg_537" s="T11">v</ta>
            <ta e="T13" id="Seg_538" s="T12">v</ta>
            <ta e="T14" id="Seg_539" s="T13">dempro</ta>
            <ta e="T15" id="Seg_540" s="T14">v</ta>
            <ta e="T16" id="Seg_541" s="T15">conj</ta>
            <ta e="T17" id="Seg_542" s="T16">pers</ta>
            <ta e="T18" id="Seg_543" s="T17">v</ta>
            <ta e="T19" id="Seg_544" s="T18">v</ta>
            <ta e="T20" id="Seg_545" s="T19">adv</ta>
            <ta e="T21" id="Seg_546" s="T20">pers</ta>
            <ta e="T22" id="Seg_547" s="T21">que</ta>
            <ta e="T23" id="Seg_548" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_549" s="T23">v</ta>
            <ta e="T25" id="Seg_550" s="T24">adv</ta>
            <ta e="T26" id="Seg_551" s="T25">v</ta>
            <ta e="T27" id="Seg_552" s="T26">v</ta>
            <ta e="T28" id="Seg_553" s="T27">n</ta>
            <ta e="T29" id="Seg_554" s="T28">v</ta>
            <ta e="T30" id="Seg_555" s="T29">conj</ta>
            <ta e="T31" id="Seg_556" s="T30">dempro</ta>
            <ta e="T32" id="Seg_557" s="T31">v</ta>
            <ta e="T33" id="Seg_558" s="T32">adv</ta>
            <ta e="T34" id="Seg_559" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_560" s="T34">v</ta>
            <ta e="T36" id="Seg_561" s="T35">dempro</ta>
            <ta e="T37" id="Seg_562" s="T36">n</ta>
            <ta e="T38" id="Seg_563" s="T37">dempro</ta>
            <ta e="T39" id="Seg_564" s="T38">v</ta>
            <ta e="T40" id="Seg_565" s="T39">adv</ta>
            <ta e="T41" id="Seg_566" s="T40">pers</ta>
            <ta e="T42" id="Seg_567" s="T41">v</ta>
            <ta e="T43" id="Seg_568" s="T42">conj</ta>
            <ta e="T44" id="Seg_569" s="T43">adv</ta>
            <ta e="T45" id="Seg_570" s="T44">v</ta>
            <ta e="T46" id="Seg_571" s="T45">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_572" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_573" s="T2">np:P</ta>
            <ta e="T8" id="Seg_574" s="T7">np.h:A</ta>
            <ta e="T11" id="Seg_575" s="T10">pro.h:A</ta>
            <ta e="T13" id="Seg_576" s="T12">0.2.h:A</ta>
            <ta e="T14" id="Seg_577" s="T13">pro:Th</ta>
            <ta e="T17" id="Seg_578" s="T16">pro.h:E</ta>
            <ta e="T19" id="Seg_579" s="T18">0.2.h:A</ta>
            <ta e="T21" id="Seg_580" s="T20">pro.h:E</ta>
            <ta e="T22" id="Seg_581" s="T21">pro:Th</ta>
            <ta e="T25" id="Seg_582" s="T24">adv:Time</ta>
            <ta e="T26" id="Seg_583" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_584" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_585" s="T27">np:Th</ta>
            <ta e="T31" id="Seg_586" s="T30">pro.h:E</ta>
            <ta e="T33" id="Seg_587" s="T32">adv:Time</ta>
            <ta e="T37" id="Seg_588" s="T36">np.h:B</ta>
            <ta e="T38" id="Seg_589" s="T37">pro.h:A</ta>
            <ta e="T41" id="Seg_590" s="T40">pro.h:E</ta>
            <ta e="T45" id="Seg_591" s="T44">0.1.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_592" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_593" s="T2">np:O</ta>
            <ta e="T4" id="Seg_594" s="T3">v:pred</ta>
            <ta e="T8" id="Seg_595" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_596" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_597" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_598" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_599" s="T12">v:pred 0.2.h:S</ta>
            <ta e="T14" id="Seg_600" s="T13">pro:S</ta>
            <ta e="T15" id="Seg_601" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_602" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_603" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_604" s="T18">v:pred 0.2.h:S</ta>
            <ta e="T21" id="Seg_605" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_606" s="T21">pro:O</ta>
            <ta e="T23" id="Seg_607" s="T22">ptcl.neg</ta>
            <ta e="T24" id="Seg_608" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_609" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T27" id="Seg_610" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_611" s="T27">np:S</ta>
            <ta e="T29" id="Seg_612" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_613" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_614" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_615" s="T33">ptcl:pred</ta>
            <ta e="T37" id="Seg_616" s="T36">np.h:O</ta>
            <ta e="T38" id="Seg_617" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_618" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_619" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_620" s="T41">v:pred</ta>
            <ta e="T45" id="Seg_621" s="T44">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_622" s="T4">RUS:gram</ta>
            <ta e="T16" id="Seg_623" s="T15">RUS:gram</ta>
            <ta e="T22" id="Seg_624" s="T21">TURK:gram(INDEF)</ta>
            <ta e="T30" id="Seg_625" s="T29">RUS:gram</ta>
            <ta e="T34" id="Seg_626" s="T33">RUS:gram</ta>
            <ta e="T43" id="Seg_627" s="T42">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_628" s="T0">Один человек пилил ветку.</ta>
            <ta e="T9" id="Seg_629" s="T4">И приходит [другой] человек.</ta>
            <ta e="T12" id="Seg_630" s="T9">«Что ты делаешь?</ta>
            <ta e="T13" id="Seg_631" s="T12">Ты пилишь [ветку].</ta>
            <ta e="T18" id="Seg_632" s="T13">Она упадёт и ты упадёшь».</ta>
            <ta e="T20" id="Seg_633" s="T18">«Уходи отсюда!</ta>
            <ta e="T24" id="Seg_634" s="T20">Ты ничего не знаешь».</ta>
            <ta e="T27" id="Seg_635" s="T24">Потом он пилил, пилил.</ta>
            <ta e="T32" id="Seg_636" s="T27">Ветка упала и он упал.</ta>
            <ta e="T37" id="Seg_637" s="T32">Потом [говорит]: «Давай кормить этого человека.</ta>
            <ta e="T45" id="Seg_638" s="T37">Он скажет, сколько я проживу или скоро умру».</ta>
            <ta e="T46" id="Seg_639" s="T45">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_640" s="T0">One man was cutting a branch.</ta>
            <ta e="T9" id="Seg_641" s="T4">And one [another] man comes.</ta>
            <ta e="T12" id="Seg_642" s="T9">"What are you doing?</ta>
            <ta e="T13" id="Seg_643" s="T12">You are cutting [a branch].</ta>
            <ta e="T18" id="Seg_644" s="T13">It will fall down and you will fall down."</ta>
            <ta e="T20" id="Seg_645" s="T18">"Go away from there!</ta>
            <ta e="T24" id="Seg_646" s="T20">You do not know anything."</ta>
            <ta e="T27" id="Seg_647" s="T24">Then he cut, he cut.</ta>
            <ta e="T32" id="Seg_648" s="T27">The branch fell down and he fell down.</ta>
            <ta e="T37" id="Seg_649" s="T32">Then [he said]: “[Let's] feed this man!</ta>
            <ta e="T45" id="Seg_650" s="T37">He will tell how long I will live or if I will die soon.”</ta>
            <ta e="T46" id="Seg_651" s="T45">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_652" s="T0">Ein Mann sägte an einem Ast.</ta>
            <ta e="T9" id="Seg_653" s="T4">Und ein [anderer] Mann kommt.</ta>
            <ta e="T12" id="Seg_654" s="T9">“Was machst du?</ta>
            <ta e="T13" id="Seg_655" s="T12">Du sägst [an einem Ast].</ta>
            <ta e="T18" id="Seg_656" s="T13">Er wird herunterfallen, und du wirst herunterfallen.“ </ta>
            <ta e="T20" id="Seg_657" s="T18">„Geh weg von dort!</ta>
            <ta e="T24" id="Seg_658" s="T20">Du weißt gar nichts.“</ta>
            <ta e="T27" id="Seg_659" s="T24">Dann sägte er, er sägte</ta>
            <ta e="T32" id="Seg_660" s="T27">Der Ast fiel herunter und er fiel herunter.</ta>
            <ta e="T37" id="Seg_661" s="T32">Dann [sagte er]: [lass uns] nähren diesen Mann!</ta>
            <ta e="T45" id="Seg_662" s="T37">Er wird mir sagen, wie lange ich leben werde, oder ob ich bald sterbe.</ta>
            <ta e="T46" id="Seg_663" s="T45">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T37" id="Seg_664" s="T32">[KlT:] Bădə ’feed’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
