<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6EDDFE89-A27B-410B-56D7-E0EA5D48BF19">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\NN_1914_Riddles_flk\NN_1914_Riddles_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">338</ud-information>
            <ud-information attribute-name="# HIAT:w">243</ud-information>
            <ud-information attribute-name="# e">242</ud-information>
            <ud-information attribute-name="# HIAT:u">31</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN">
            <abbreviation>NN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN"
                      type="t">
         <timeline-fork end="T69" start="T68">
            <tli id="T68.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T242" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Măjagən</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">aspaʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">mĭnzəlleʔbə</ts>
                  <nts id="Seg_11" n="HIAT:ip">—</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">ködəmge</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_18" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">Măjagən</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">süjö</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">sĭktölaːmbi</ts>
                  <nts id="Seg_27" n="HIAT:ip">—</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">marga</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_34" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">Boštə</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">šü</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">tʼogən</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">kugojdə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">dü</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">tʼogən</ts>
                  <nts id="Seg_53" n="HIAT:ip">—</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">tugul</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_60" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">Šün</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">toʔgən</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">šide</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">boːs</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">ne</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">iʔbəjəʔ</ts>
                  <nts id="Seg_78" n="HIAT:ip">—</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">măjʔka</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_85" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">Üjüt</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">naga</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">udat</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">naga</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">măjanə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">sʼalaːndəga</ts>
                  <nts id="Seg_105" n="HIAT:ip">—</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">neni</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">šü</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_115" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">Süjö</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">iʔgö</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">uluzaŋdə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">oʔb</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">tʼogən</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">kötendə</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">băzəj</ts>
                  <nts id="Seg_138" n="HIAT:ip">—</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">maʔ</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_145" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">Orogən</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">tʼüpi</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">büzəj</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">iʔbe</ts>
                  <nts id="Seg_157" n="HIAT:ip">—</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">šĭke</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_164" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">Nuna</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">lăbəlaʔ</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">kojobi</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">tʼalaš</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">nʼi</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">nulaʔ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">šolaʔ</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">üzəbi</ts>
                  <nts id="Seg_189" n="HIAT:ip">—</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">kʼama</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_196" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">Tagajbə</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">seləbiem</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">seləne</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">pim</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">bünə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">üštəbiem</ts>
                  <nts id="Seg_215" n="HIAT:ip">—</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">tʼaʔ</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_222" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">Iʔbəbində</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">koškagadə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">boʔbdə</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">uʔbdəbində</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">inegedə</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">püržə</ts>
                  <nts id="Seg_241" n="HIAT:ip">—</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">duga</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_248" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">Ujužət</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_254" n="HIAT:w" s="T67">udažət</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68.tx.1" id="Seg_257" n="HIAT:w" s="T68">numan</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68.tx.1">üzəlie</ts>
                  <nts id="Seg_261" n="HIAT:ip">—</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_264" n="HIAT:w" s="T69">baltu</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_268" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_270" n="HIAT:w" s="T70">Šadǝra</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">koʔbdə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">amna</ts>
                  <nts id="Seg_277" n="HIAT:ip">—</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_280" n="HIAT:w" s="T73">ĭntak</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_284" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_286" n="HIAT:w" s="T74">Uruʔbə</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_289" n="HIAT:w" s="T75">oʔpəliom</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_293" n="HIAT:w" s="T76">oʔpəleʔ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_296" n="HIAT:w" s="T77">ej</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_299" n="HIAT:w" s="T78">moliom</ts>
                  <nts id="Seg_300" n="HIAT:ip">—</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_303" n="HIAT:w" s="T79">aʔtʼə</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_307" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">Kamdʼzʼu</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">kubiaŋ</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">tʼildəleʔ</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">ej</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_322" n="HIAT:w" s="T84">moliam</ts>
                  <nts id="Seg_323" n="HIAT:ip">—</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_326" n="HIAT:w" s="T85">nanzə</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_330" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_332" n="HIAT:w" s="T86">Teʔdə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_335" n="HIAT:w" s="T87">tibi</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_338" n="HIAT:w" s="T88">oʔb</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_341" n="HIAT:w" s="T89">üžün</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">šerleʔ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_347" n="HIAT:w" s="T91">nogaiʔ</ts>
                  <nts id="Seg_348" n="HIAT:ip">—</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">stol</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_355" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_357" n="HIAT:w" s="T93">Šö</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_360" n="HIAT:w" s="T94">tʼogən</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_363" n="HIAT:w" s="T95">pa</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_366" n="HIAT:w" s="T96">pajliajəʔ</ts>
                  <nts id="Seg_367" n="HIAT:ip">,</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_370" n="HIAT:w" s="T97">taptə</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">döbər</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_376" n="HIAT:w" s="T99">saʔməlaʔbə</ts>
                  <nts id="Seg_377" n="HIAT:ip">—</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_380" n="HIAT:w" s="T100">pʼaŋdona</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_383" n="HIAT:w" s="T101">sazə</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_387" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_389" n="HIAT:w" s="T102">Tuluŋzəbi</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_392" n="HIAT:w" s="T103">koʔbdə</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">jadan</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_398" n="HIAT:w" s="T105">kunzə</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_401" n="HIAT:w" s="T106">mĭŋge</ts>
                  <nts id="Seg_402" n="HIAT:ip">—</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_405" n="HIAT:w" s="T107">šaːškən</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_409" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_411" n="HIAT:w" s="T108">Ige</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">dĭrgit</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">tʼerəm</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">gibər</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_423" n="HIAT:w" s="T112">pa</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_426" n="HIAT:w" s="T113">mĭnzʼət</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_429" n="HIAT:w" s="T114">tagajnə</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_432" n="HIAT:w" s="T115">šăn</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_436" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_438" n="HIAT:w" s="T116">Kujan</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">jilgəndə</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_444" n="HIAT:w" s="T118">nulam</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_448" n="HIAT:w" s="T119">altənəj</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_451" n="HIAT:w" s="T120">bargam</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_454" n="HIAT:w" s="T121">nʼeʔlim</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_458" n="HIAT:w" s="T122">kin</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_461" n="HIAT:w" s="T123">jilgəndə</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_464" n="HIAT:w" s="T124">nulam</ts>
                  <nts id="Seg_465" n="HIAT:ip">,</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_468" n="HIAT:w" s="T125">kümüžəj</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_471" n="HIAT:w" s="T126">bargam</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_474" n="HIAT:w" s="T127">nʼeʔlim</ts>
                  <nts id="Seg_475" n="HIAT:ip">—</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_478" n="HIAT:w" s="T128">tažə</ts>
                  <nts id="Seg_479" n="HIAT:ip">,</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_482" n="HIAT:w" s="T129">kuruʔjo</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_486" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_488" n="HIAT:w" s="T130">Nemnəjzəbi</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_491" n="HIAT:w" s="T131">büzʼe</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_494" n="HIAT:w" s="T132">amna</ts>
                  <nts id="Seg_495" n="HIAT:ip">,</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_498" n="HIAT:w" s="T133">dĭ</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">büzʼem</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_504" n="HIAT:w" s="T135">ambinan</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_507" n="HIAT:w" s="T136">tʼorlal</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_510" n="HIAT:w" s="T137">köbergən</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_514" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_516" n="HIAT:w" s="T138">Šide</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_519" n="HIAT:w" s="T139">marat</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_522" n="HIAT:w" s="T140">sʼukku</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_526" n="HIAT:w" s="T141">šide</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_529" n="HIAT:w" s="T142">marat</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_532" n="HIAT:w" s="T143">tuluka</ts>
                  <nts id="Seg_533" n="HIAT:ip">,</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_536" n="HIAT:w" s="T144">orta</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_539" n="HIAT:w" s="T145">tʼergəndə</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_542" n="HIAT:w" s="T146">marka</ts>
                  <nts id="Seg_543" n="HIAT:ip">—</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_546" n="HIAT:w" s="T147">kaptə</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_550" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_552" n="HIAT:w" s="T148">Šide</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_555" n="HIAT:w" s="T149">kuza</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_558" n="HIAT:w" s="T150">iʔbə</ts>
                  <nts id="Seg_559" n="HIAT:ip">,</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_562" n="HIAT:w" s="T151">šide</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_565" n="HIAT:w" s="T152">kuza</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_568" n="HIAT:w" s="T153">nuga</ts>
                  <nts id="Seg_569" n="HIAT:ip">,</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_572" n="HIAT:w" s="T154">sumnaŋgit</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_575" n="HIAT:w" s="T155">kuza</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_578" n="HIAT:w" s="T156">bokulaʔ</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_581" n="HIAT:w" s="T157">mĭŋge</ts>
                  <nts id="Seg_582" n="HIAT:ip">—</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_585" n="HIAT:w" s="T158">kăloda</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_589" n="HIAT:w" s="T159">ajə</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_593" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_595" n="HIAT:w" s="T160">Amniom</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_599" n="HIAT:w" s="T161">amniom</ts>
                  <nts id="Seg_600" n="HIAT:ip">,</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_603" n="HIAT:w" s="T162">tüleʔ</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_606" n="HIAT:w" s="T163">ej</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_609" n="HIAT:w" s="T164">moliam</ts>
                  <nts id="Seg_610" n="HIAT:ip">—</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_613" n="HIAT:w" s="T165">băra</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_617" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_619" n="HIAT:w" s="T166">Körispə</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">amna</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">šiden</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">nĭgəndə</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_632" n="HIAT:w" s="T170">tʼimat</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_635" n="HIAT:w" s="T171">tʼugən</ts>
                  <nts id="Seg_636" n="HIAT:ip">,</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">kürüt</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">kudaigən</ts>
                  <nts id="Seg_643" n="HIAT:ip">—</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_646" n="HIAT:w" s="T174">koŋgoro</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_650" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_652" n="HIAT:w" s="T175">Nünörim</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">da</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_658" n="HIAT:w" s="T177">sadərim</ts>
                  <nts id="Seg_659" n="HIAT:ip">—</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_662" n="HIAT:w" s="T178">săbərgu</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_665" n="HIAT:w" s="T179">pʼeːš</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_668" n="HIAT:w" s="T180">săbərəjʔdna</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_672" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_674" n="HIAT:w" s="T181">Nʼi</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_677" n="HIAT:w" s="T182">noga</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_681" n="HIAT:w" s="T183">orta</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_684" n="HIAT:w" s="T184">tʼerdə</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_687" n="HIAT:w" s="T185">amolaʔbə</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_690" n="HIAT:w" s="T186">a</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">marat</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">kĭnzəleʔbə</ts>
                  <nts id="Seg_697" n="HIAT:ip">—</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_700" n="HIAT:w" s="T189">samovar</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_704" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_706" n="HIAT:w" s="T190">Šide</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_709" n="HIAT:w" s="T191">tibi</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_712" n="HIAT:w" s="T192">koža</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_715" n="HIAT:w" s="T193">tʼargo</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_718" n="HIAT:w" s="T194">šürle</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_721" n="HIAT:w" s="T195">kandəgaiʔ</ts>
                  <nts id="Seg_722" n="HIAT:ip">—</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_725" n="HIAT:w" s="T196">tejme</ts>
                  <nts id="Seg_726" n="HIAT:ip">.</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_729" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_731" n="HIAT:w" s="T197">Üjütsʼəʔ</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_734" n="HIAT:w" s="T198">pʼaŋdəliat</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_738" n="HIAT:w" s="T199">nanətsʼəʔ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_741" n="HIAT:w" s="T200">kĭškəliet</ts>
                  <nts id="Seg_742" n="HIAT:ip">,</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_745" n="HIAT:w" s="T201">aŋdə</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_748" n="HIAT:w" s="T202">šiʔdəliet</ts>
                  <nts id="Seg_749" n="HIAT:ip">,</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_752" n="HIAT:w" s="T203">tibər</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_755" n="HIAT:w" s="T204">păʔdəliat</ts>
                  <nts id="Seg_756" n="HIAT:ip">—</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_759" n="HIAT:w" s="T205">sogarzʼət</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_763" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_765" n="HIAT:w" s="T206">Toʔbdit</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_768" n="HIAT:w" s="T207">măna</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_772" n="HIAT:w" s="T208">münördə</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_775" n="HIAT:w" s="T209">măna</ts>
                  <nts id="Seg_776" n="HIAT:ip">,</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_779" n="HIAT:w" s="T210">ige</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_782" n="HIAT:w" s="T211">măna</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_785" n="HIAT:w" s="T212">tarzəbi</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_788" n="HIAT:w" s="T213">tʼer</ts>
                  <nts id="Seg_789" n="HIAT:ip">,</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_792" n="HIAT:w" s="T214">tarzəbi</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_795" n="HIAT:w" s="T215">tʼerən</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_798" n="HIAT:w" s="T216">šüjündə</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_801" n="HIAT:w" s="T217">kümə</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_804" n="HIAT:w" s="T218">tʼer</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_807" n="HIAT:w" s="T219">ige</ts>
                  <nts id="Seg_808" n="HIAT:ip">,</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_811" n="HIAT:w" s="T220">kümə</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_814" n="HIAT:w" s="T221">tʼerən</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_817" n="HIAT:w" s="T222">šüjündə</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_820" n="HIAT:w" s="T223">nʼamga</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_823" n="HIAT:w" s="T224">tʼer</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_826" n="HIAT:w" s="T225">ige</ts>
                  <nts id="Seg_827" n="HIAT:ip">—</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_830" n="HIAT:w" s="T226">sanə</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_834" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_836" n="HIAT:w" s="T227">Pam</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_839" n="HIAT:w" s="T228">paroldi</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_842" n="HIAT:w" s="T229">kük</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_845" n="HIAT:w" s="T230">noʔ</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_848" n="HIAT:w" s="T231">özerbi</ts>
                  <nts id="Seg_849" n="HIAT:ip">—</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_852" n="HIAT:w" s="T232">tʼüstək</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_856" n="HIAT:w" s="T233">ki</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_860" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_862" n="HIAT:w" s="T234">Kömə</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_865" n="HIAT:w" s="T235">poʔdə</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_868" n="HIAT:w" s="T236">iʔböne</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_871" n="HIAT:w" s="T237">tʼogən</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_874" n="HIAT:w" s="T238">noʔ</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_877" n="HIAT:w" s="T239">ej</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_880" n="HIAT:w" s="T240">özerie</ts>
                  <nts id="Seg_881" n="HIAT:ip">—</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_884" n="HIAT:w" s="T241">šü</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T242" id="Seg_887" n="sc" s="T0">
               <ts e="T1" id="Seg_889" n="e" s="T0">Măjagən </ts>
               <ts e="T2" id="Seg_891" n="e" s="T1">aspaʔ </ts>
               <ts e="T3" id="Seg_893" n="e" s="T2">mĭnzəlleʔbə— </ts>
               <ts e="T4" id="Seg_895" n="e" s="T3">ködəmge. </ts>
               <ts e="T5" id="Seg_897" n="e" s="T4">Măjagən </ts>
               <ts e="T6" id="Seg_899" n="e" s="T5">süjö </ts>
               <ts e="T7" id="Seg_901" n="e" s="T6">sĭktölaːmbi— </ts>
               <ts e="T8" id="Seg_903" n="e" s="T7">marga. </ts>
               <ts e="T9" id="Seg_905" n="e" s="T8">Boštə </ts>
               <ts e="T10" id="Seg_907" n="e" s="T9">šü </ts>
               <ts e="T11" id="Seg_909" n="e" s="T10">tʼogən, </ts>
               <ts e="T12" id="Seg_911" n="e" s="T11">kugojdə </ts>
               <ts e="T13" id="Seg_913" n="e" s="T12">dü </ts>
               <ts e="T14" id="Seg_915" n="e" s="T13">tʼogən— </ts>
               <ts e="T15" id="Seg_917" n="e" s="T14">tugul. </ts>
               <ts e="T16" id="Seg_919" n="e" s="T15">Šün </ts>
               <ts e="T17" id="Seg_921" n="e" s="T16">toʔgən </ts>
               <ts e="T18" id="Seg_923" n="e" s="T17">šide </ts>
               <ts e="T19" id="Seg_925" n="e" s="T18">boːs </ts>
               <ts e="T20" id="Seg_927" n="e" s="T19">ne </ts>
               <ts e="T21" id="Seg_929" n="e" s="T20">iʔbəjəʔ— </ts>
               <ts e="T22" id="Seg_931" n="e" s="T21">măjʔka. </ts>
               <ts e="T23" id="Seg_933" n="e" s="T22">Üjüt </ts>
               <ts e="T24" id="Seg_935" n="e" s="T23">naga, </ts>
               <ts e="T25" id="Seg_937" n="e" s="T24">udat </ts>
               <ts e="T26" id="Seg_939" n="e" s="T25">naga, </ts>
               <ts e="T27" id="Seg_941" n="e" s="T26">măjanə </ts>
               <ts e="T28" id="Seg_943" n="e" s="T27">sʼalaːndəga— </ts>
               <ts e="T29" id="Seg_945" n="e" s="T28">neni </ts>
               <ts e="T30" id="Seg_947" n="e" s="T29">šü. </ts>
               <ts e="T31" id="Seg_949" n="e" s="T30">Süjö </ts>
               <ts e="T32" id="Seg_951" n="e" s="T31">iʔgö, </ts>
               <ts e="T33" id="Seg_953" n="e" s="T32">uluzaŋdə </ts>
               <ts e="T34" id="Seg_955" n="e" s="T33">oʔb </ts>
               <ts e="T35" id="Seg_957" n="e" s="T34">tʼogən, </ts>
               <ts e="T36" id="Seg_959" n="e" s="T35">kötendə </ts>
               <ts e="T37" id="Seg_961" n="e" s="T36">băzəj— </ts>
               <ts e="T38" id="Seg_963" n="e" s="T37">maʔ. </ts>
               <ts e="T39" id="Seg_965" n="e" s="T38">Orogən </ts>
               <ts e="T40" id="Seg_967" n="e" s="T39">tʼüpi </ts>
               <ts e="T41" id="Seg_969" n="e" s="T40">büzəj </ts>
               <ts e="T42" id="Seg_971" n="e" s="T41">iʔbe— </ts>
               <ts e="T43" id="Seg_973" n="e" s="T42">šĭke. </ts>
               <ts e="T44" id="Seg_975" n="e" s="T43">Nuna </ts>
               <ts e="T45" id="Seg_977" n="e" s="T44">lăbəlaʔ </ts>
               <ts e="T46" id="Seg_979" n="e" s="T45">kojobi, </ts>
               <ts e="T47" id="Seg_981" n="e" s="T46">tʼalaš </ts>
               <ts e="T48" id="Seg_983" n="e" s="T47">nʼi </ts>
               <ts e="T49" id="Seg_985" n="e" s="T48">nulaʔ </ts>
               <ts e="T50" id="Seg_987" n="e" s="T49">šolaʔ </ts>
               <ts e="T51" id="Seg_989" n="e" s="T50">üzəbi— </ts>
               <ts e="T52" id="Seg_991" n="e" s="T51">kʼama. </ts>
               <ts e="T53" id="Seg_993" n="e" s="T52">Tagajbə </ts>
               <ts e="T54" id="Seg_995" n="e" s="T53">seləbiem, </ts>
               <ts e="T55" id="Seg_997" n="e" s="T54">seləne </ts>
               <ts e="T56" id="Seg_999" n="e" s="T55">pim </ts>
               <ts e="T57" id="Seg_1001" n="e" s="T56">bünə </ts>
               <ts e="T58" id="Seg_1003" n="e" s="T57">üštəbiem— </ts>
               <ts e="T59" id="Seg_1005" n="e" s="T58">tʼaʔ. </ts>
               <ts e="T60" id="Seg_1007" n="e" s="T59">Iʔbəbində </ts>
               <ts e="T61" id="Seg_1009" n="e" s="T60">koškagadə </ts>
               <ts e="T62" id="Seg_1011" n="e" s="T61">boʔbdə, </ts>
               <ts e="T63" id="Seg_1013" n="e" s="T62">uʔbdəbində </ts>
               <ts e="T64" id="Seg_1015" n="e" s="T63">inegedə </ts>
               <ts e="T65" id="Seg_1017" n="e" s="T64">püržə— </ts>
               <ts e="T66" id="Seg_1019" n="e" s="T65">duga. </ts>
               <ts e="T67" id="Seg_1021" n="e" s="T66">Ujužət, </ts>
               <ts e="T68" id="Seg_1023" n="e" s="T67">udažət </ts>
               <ts e="T69" id="Seg_1025" n="e" s="T68">numan üzəlie— </ts>
               <ts e="T70" id="Seg_1027" n="e" s="T69">baltu. </ts>
               <ts e="T71" id="Seg_1029" n="e" s="T70">Šadǝra </ts>
               <ts e="T72" id="Seg_1031" n="e" s="T71">koʔbdə </ts>
               <ts e="T73" id="Seg_1033" n="e" s="T72">amna— </ts>
               <ts e="T74" id="Seg_1035" n="e" s="T73">ĭntak. </ts>
               <ts e="T75" id="Seg_1037" n="e" s="T74">Uruʔbə </ts>
               <ts e="T76" id="Seg_1039" n="e" s="T75">oʔpəliom, </ts>
               <ts e="T77" id="Seg_1041" n="e" s="T76">oʔpəleʔ </ts>
               <ts e="T78" id="Seg_1043" n="e" s="T77">ej </ts>
               <ts e="T79" id="Seg_1045" n="e" s="T78">moliom— </ts>
               <ts e="T80" id="Seg_1047" n="e" s="T79">aʔtʼə. </ts>
               <ts e="T81" id="Seg_1049" n="e" s="T80">Kamdʼzʼu </ts>
               <ts e="T82" id="Seg_1051" n="e" s="T81">kubiaŋ, </ts>
               <ts e="T83" id="Seg_1053" n="e" s="T82">tʼildəleʔ </ts>
               <ts e="T84" id="Seg_1055" n="e" s="T83">ej </ts>
               <ts e="T85" id="Seg_1057" n="e" s="T84">moliam— </ts>
               <ts e="T86" id="Seg_1059" n="e" s="T85">nanzə. </ts>
               <ts e="T87" id="Seg_1061" n="e" s="T86">Teʔdə </ts>
               <ts e="T88" id="Seg_1063" n="e" s="T87">tibi </ts>
               <ts e="T89" id="Seg_1065" n="e" s="T88">oʔb </ts>
               <ts e="T90" id="Seg_1067" n="e" s="T89">üžün </ts>
               <ts e="T91" id="Seg_1069" n="e" s="T90">šerleʔ </ts>
               <ts e="T92" id="Seg_1071" n="e" s="T91">nogaiʔ— </ts>
               <ts e="T93" id="Seg_1073" n="e" s="T92">stol. </ts>
               <ts e="T94" id="Seg_1075" n="e" s="T93">Šö </ts>
               <ts e="T95" id="Seg_1077" n="e" s="T94">tʼogən </ts>
               <ts e="T96" id="Seg_1079" n="e" s="T95">pa </ts>
               <ts e="T97" id="Seg_1081" n="e" s="T96">pajliajəʔ, </ts>
               <ts e="T98" id="Seg_1083" n="e" s="T97">taptə </ts>
               <ts e="T99" id="Seg_1085" n="e" s="T98">döbər </ts>
               <ts e="T100" id="Seg_1087" n="e" s="T99">saʔməlaʔbə— </ts>
               <ts e="T101" id="Seg_1089" n="e" s="T100">pʼaŋdona </ts>
               <ts e="T102" id="Seg_1091" n="e" s="T101">sazə. </ts>
               <ts e="T103" id="Seg_1093" n="e" s="T102">Tuluŋzəbi </ts>
               <ts e="T104" id="Seg_1095" n="e" s="T103">koʔbdə </ts>
               <ts e="T105" id="Seg_1097" n="e" s="T104">jadan </ts>
               <ts e="T106" id="Seg_1099" n="e" s="T105">kunzə </ts>
               <ts e="T107" id="Seg_1101" n="e" s="T106">mĭŋge— </ts>
               <ts e="T108" id="Seg_1103" n="e" s="T107">šaːškən. </ts>
               <ts e="T109" id="Seg_1105" n="e" s="T108">Ige </ts>
               <ts e="T110" id="Seg_1107" n="e" s="T109">dĭrgit </ts>
               <ts e="T111" id="Seg_1109" n="e" s="T110">tʼerəm </ts>
               <ts e="T112" id="Seg_1111" n="e" s="T111">gibər </ts>
               <ts e="T113" id="Seg_1113" n="e" s="T112">pa </ts>
               <ts e="T114" id="Seg_1115" n="e" s="T113">mĭnzʼət </ts>
               <ts e="T115" id="Seg_1117" n="e" s="T114">tagajnə </ts>
               <ts e="T116" id="Seg_1119" n="e" s="T115">šăn. </ts>
               <ts e="T117" id="Seg_1121" n="e" s="T116">Kujan </ts>
               <ts e="T118" id="Seg_1123" n="e" s="T117">jilgəndə </ts>
               <ts e="T119" id="Seg_1125" n="e" s="T118">nulam, </ts>
               <ts e="T120" id="Seg_1127" n="e" s="T119">altənəj </ts>
               <ts e="T121" id="Seg_1129" n="e" s="T120">bargam </ts>
               <ts e="T122" id="Seg_1131" n="e" s="T121">nʼeʔlim, </ts>
               <ts e="T123" id="Seg_1133" n="e" s="T122">kin </ts>
               <ts e="T124" id="Seg_1135" n="e" s="T123">jilgəndə </ts>
               <ts e="T125" id="Seg_1137" n="e" s="T124">nulam, </ts>
               <ts e="T126" id="Seg_1139" n="e" s="T125">kümüžəj </ts>
               <ts e="T127" id="Seg_1141" n="e" s="T126">bargam </ts>
               <ts e="T128" id="Seg_1143" n="e" s="T127">nʼeʔlim— </ts>
               <ts e="T129" id="Seg_1145" n="e" s="T128">tažə, </ts>
               <ts e="T130" id="Seg_1147" n="e" s="T129">kuruʔjo. </ts>
               <ts e="T131" id="Seg_1149" n="e" s="T130">Nemnəjzəbi </ts>
               <ts e="T132" id="Seg_1151" n="e" s="T131">büzʼe </ts>
               <ts e="T133" id="Seg_1153" n="e" s="T132">amna, </ts>
               <ts e="T134" id="Seg_1155" n="e" s="T133">dĭ </ts>
               <ts e="T135" id="Seg_1157" n="e" s="T134">büzʼem </ts>
               <ts e="T136" id="Seg_1159" n="e" s="T135">ambinan </ts>
               <ts e="T137" id="Seg_1161" n="e" s="T136">tʼorlal </ts>
               <ts e="T138" id="Seg_1163" n="e" s="T137">köbergən. </ts>
               <ts e="T139" id="Seg_1165" n="e" s="T138">Šide </ts>
               <ts e="T140" id="Seg_1167" n="e" s="T139">marat </ts>
               <ts e="T141" id="Seg_1169" n="e" s="T140">sʼukku, </ts>
               <ts e="T142" id="Seg_1171" n="e" s="T141">šide </ts>
               <ts e="T143" id="Seg_1173" n="e" s="T142">marat </ts>
               <ts e="T144" id="Seg_1175" n="e" s="T143">tuluka, </ts>
               <ts e="T145" id="Seg_1177" n="e" s="T144">orta </ts>
               <ts e="T146" id="Seg_1179" n="e" s="T145">tʼergəndə </ts>
               <ts e="T147" id="Seg_1181" n="e" s="T146">marka— </ts>
               <ts e="T148" id="Seg_1183" n="e" s="T147">kaptə. </ts>
               <ts e="T149" id="Seg_1185" n="e" s="T148">Šide </ts>
               <ts e="T150" id="Seg_1187" n="e" s="T149">kuza </ts>
               <ts e="T151" id="Seg_1189" n="e" s="T150">iʔbə, </ts>
               <ts e="T152" id="Seg_1191" n="e" s="T151">šide </ts>
               <ts e="T153" id="Seg_1193" n="e" s="T152">kuza </ts>
               <ts e="T154" id="Seg_1195" n="e" s="T153">nuga, </ts>
               <ts e="T155" id="Seg_1197" n="e" s="T154">sumnaŋgit </ts>
               <ts e="T156" id="Seg_1199" n="e" s="T155">kuza </ts>
               <ts e="T157" id="Seg_1201" n="e" s="T156">bokulaʔ </ts>
               <ts e="T158" id="Seg_1203" n="e" s="T157">mĭŋge— </ts>
               <ts e="T159" id="Seg_1205" n="e" s="T158">kăloda, </ts>
               <ts e="T160" id="Seg_1207" n="e" s="T159">ajə. </ts>
               <ts e="T161" id="Seg_1209" n="e" s="T160">Amniom, </ts>
               <ts e="T162" id="Seg_1211" n="e" s="T161">amniom, </ts>
               <ts e="T163" id="Seg_1213" n="e" s="T162">tüleʔ </ts>
               <ts e="T164" id="Seg_1215" n="e" s="T163">ej </ts>
               <ts e="T165" id="Seg_1217" n="e" s="T164">moliam— </ts>
               <ts e="T166" id="Seg_1219" n="e" s="T165">băra. </ts>
               <ts e="T167" id="Seg_1221" n="e" s="T166">Körispə </ts>
               <ts e="T168" id="Seg_1223" n="e" s="T167">amna </ts>
               <ts e="T169" id="Seg_1225" n="e" s="T168">šiden </ts>
               <ts e="T170" id="Seg_1227" n="e" s="T169">nĭgəndə, </ts>
               <ts e="T171" id="Seg_1229" n="e" s="T170">tʼimat </ts>
               <ts e="T172" id="Seg_1231" n="e" s="T171">tʼugən, </ts>
               <ts e="T173" id="Seg_1233" n="e" s="T172">kürüt </ts>
               <ts e="T174" id="Seg_1235" n="e" s="T173">kudaigən— </ts>
               <ts e="T175" id="Seg_1237" n="e" s="T174">koŋgoro. </ts>
               <ts e="T176" id="Seg_1239" n="e" s="T175">Nünörim </ts>
               <ts e="T177" id="Seg_1241" n="e" s="T176">da </ts>
               <ts e="T178" id="Seg_1243" n="e" s="T177">sadərim— </ts>
               <ts e="T179" id="Seg_1245" n="e" s="T178">săbərgu </ts>
               <ts e="T180" id="Seg_1247" n="e" s="T179">pʼeːš </ts>
               <ts e="T181" id="Seg_1249" n="e" s="T180">săbərəjʔdna. </ts>
               <ts e="T182" id="Seg_1251" n="e" s="T181">Nʼi </ts>
               <ts e="T183" id="Seg_1253" n="e" s="T182">noga, </ts>
               <ts e="T184" id="Seg_1255" n="e" s="T183">orta </ts>
               <ts e="T185" id="Seg_1257" n="e" s="T184">tʼerdə </ts>
               <ts e="T186" id="Seg_1259" n="e" s="T185">amolaʔbə </ts>
               <ts e="T187" id="Seg_1261" n="e" s="T186">a </ts>
               <ts e="T188" id="Seg_1263" n="e" s="T187">marat </ts>
               <ts e="T189" id="Seg_1265" n="e" s="T188">kĭnzəleʔbə— </ts>
               <ts e="T190" id="Seg_1267" n="e" s="T189">samovar. </ts>
               <ts e="T191" id="Seg_1269" n="e" s="T190">Šide </ts>
               <ts e="T192" id="Seg_1271" n="e" s="T191">tibi </ts>
               <ts e="T193" id="Seg_1273" n="e" s="T192">koža </ts>
               <ts e="T194" id="Seg_1275" n="e" s="T193">tʼargo </ts>
               <ts e="T195" id="Seg_1277" n="e" s="T194">šürle </ts>
               <ts e="T196" id="Seg_1279" n="e" s="T195">kandəgaiʔ— </ts>
               <ts e="T197" id="Seg_1281" n="e" s="T196">tejme. </ts>
               <ts e="T198" id="Seg_1283" n="e" s="T197">Üjütsʼəʔ </ts>
               <ts e="T199" id="Seg_1285" n="e" s="T198">pʼaŋdəliat, </ts>
               <ts e="T200" id="Seg_1287" n="e" s="T199">nanətsʼəʔ </ts>
               <ts e="T201" id="Seg_1289" n="e" s="T200">kĭškəliet, </ts>
               <ts e="T202" id="Seg_1291" n="e" s="T201">aŋdə </ts>
               <ts e="T203" id="Seg_1293" n="e" s="T202">šiʔdəliet, </ts>
               <ts e="T204" id="Seg_1295" n="e" s="T203">tibər </ts>
               <ts e="T205" id="Seg_1297" n="e" s="T204">păʔdəliat— </ts>
               <ts e="T206" id="Seg_1299" n="e" s="T205">sogarzʼət. </ts>
               <ts e="T207" id="Seg_1301" n="e" s="T206">Toʔbdit </ts>
               <ts e="T208" id="Seg_1303" n="e" s="T207">măna, </ts>
               <ts e="T209" id="Seg_1305" n="e" s="T208">münördə </ts>
               <ts e="T210" id="Seg_1307" n="e" s="T209">măna, </ts>
               <ts e="T211" id="Seg_1309" n="e" s="T210">ige </ts>
               <ts e="T212" id="Seg_1311" n="e" s="T211">măna </ts>
               <ts e="T213" id="Seg_1313" n="e" s="T212">tarzəbi </ts>
               <ts e="T214" id="Seg_1315" n="e" s="T213">tʼer, </ts>
               <ts e="T215" id="Seg_1317" n="e" s="T214">tarzəbi </ts>
               <ts e="T216" id="Seg_1319" n="e" s="T215">tʼerən </ts>
               <ts e="T217" id="Seg_1321" n="e" s="T216">šüjündə </ts>
               <ts e="T218" id="Seg_1323" n="e" s="T217">kümə </ts>
               <ts e="T219" id="Seg_1325" n="e" s="T218">tʼer </ts>
               <ts e="T220" id="Seg_1327" n="e" s="T219">ige, </ts>
               <ts e="T221" id="Seg_1329" n="e" s="T220">kümə </ts>
               <ts e="T222" id="Seg_1331" n="e" s="T221">tʼerən </ts>
               <ts e="T223" id="Seg_1333" n="e" s="T222">šüjündə </ts>
               <ts e="T224" id="Seg_1335" n="e" s="T223">nʼamga </ts>
               <ts e="T225" id="Seg_1337" n="e" s="T224">tʼer </ts>
               <ts e="T226" id="Seg_1339" n="e" s="T225">ige— </ts>
               <ts e="T227" id="Seg_1341" n="e" s="T226">sanə. </ts>
               <ts e="T228" id="Seg_1343" n="e" s="T227">Pam </ts>
               <ts e="T229" id="Seg_1345" n="e" s="T228">paroldi </ts>
               <ts e="T230" id="Seg_1347" n="e" s="T229">kük </ts>
               <ts e="T231" id="Seg_1349" n="e" s="T230">noʔ </ts>
               <ts e="T232" id="Seg_1351" n="e" s="T231">özerbi— </ts>
               <ts e="T233" id="Seg_1353" n="e" s="T232">tʼüstək, </ts>
               <ts e="T234" id="Seg_1355" n="e" s="T233">ki. </ts>
               <ts e="T235" id="Seg_1357" n="e" s="T234">Kömə </ts>
               <ts e="T236" id="Seg_1359" n="e" s="T235">poʔdə </ts>
               <ts e="T237" id="Seg_1361" n="e" s="T236">iʔböne </ts>
               <ts e="T238" id="Seg_1363" n="e" s="T237">tʼogən </ts>
               <ts e="T239" id="Seg_1365" n="e" s="T238">noʔ </ts>
               <ts e="T240" id="Seg_1367" n="e" s="T239">ej </ts>
               <ts e="T241" id="Seg_1369" n="e" s="T240">özerie— </ts>
               <ts e="T242" id="Seg_1371" n="e" s="T241">šü. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1372" s="T0">NN_1914_Riddles_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_1373" s="T4">NN_1914_Riddles_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_1374" s="T8">NN_1914_Riddles_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_1375" s="T15">NN_1914_Riddles_flk.004 (001.004)</ta>
            <ta e="T30" id="Seg_1376" s="T22">NN_1914_Riddles_flk.005 (001.005)</ta>
            <ta e="T38" id="Seg_1377" s="T30">NN_1914_Riddles_flk.006 (001.006)</ta>
            <ta e="T43" id="Seg_1378" s="T38">NN_1914_Riddles_flk.007 (001.007)</ta>
            <ta e="T52" id="Seg_1379" s="T43">NN_1914_Riddles_flk.008 (001.008)</ta>
            <ta e="T59" id="Seg_1380" s="T52">NN_1914_Riddles_flk.009 (001.009)</ta>
            <ta e="T66" id="Seg_1381" s="T59">NN_1914_Riddles_flk.010 (001.010)</ta>
            <ta e="T70" id="Seg_1382" s="T66">NN_1914_Riddles_flk.011 (001.011)</ta>
            <ta e="T74" id="Seg_1383" s="T70">NN_1914_Riddles_flk.012 (001.012)</ta>
            <ta e="T80" id="Seg_1384" s="T74">NN_1914_Riddles_flk.013 (001.013)</ta>
            <ta e="T86" id="Seg_1385" s="T80">NN_1914_Riddles_flk.014 (001.014)</ta>
            <ta e="T93" id="Seg_1386" s="T86">NN_1914_Riddles_flk.015 (001.015)</ta>
            <ta e="T102" id="Seg_1387" s="T93">NN_1914_Riddles_flk.016 (001.016)</ta>
            <ta e="T108" id="Seg_1388" s="T102">NN_1914_Riddles_flk.017 (001.017)</ta>
            <ta e="T116" id="Seg_1389" s="T108">NN_1914_Riddles_flk.018 (001.018)</ta>
            <ta e="T130" id="Seg_1390" s="T116">NN_1914_Riddles_flk.019 (001.019)</ta>
            <ta e="T138" id="Seg_1391" s="T130">NN_1914_Riddles_flk.020 (001.020)</ta>
            <ta e="T148" id="Seg_1392" s="T138">NN_1914_Riddles_flk.021 (001.021)</ta>
            <ta e="T160" id="Seg_1393" s="T148">NN_1914_Riddles_flk.022 (001.022)</ta>
            <ta e="T166" id="Seg_1394" s="T160">NN_1914_Riddles_flk.023 (001.023)</ta>
            <ta e="T175" id="Seg_1395" s="T166">NN_1914_Riddles_flk.024 (001.024)</ta>
            <ta e="T181" id="Seg_1396" s="T175">NN_1914_Riddles_flk.025 (001.025)</ta>
            <ta e="T190" id="Seg_1397" s="T181">NN_1914_Riddles_flk.026 (001.026)</ta>
            <ta e="T197" id="Seg_1398" s="T190">NN_1914_Riddles_flk.027 (001.027)</ta>
            <ta e="T206" id="Seg_1399" s="T197">NN_1914_Riddles_flk.028 (001.028)</ta>
            <ta e="T227" id="Seg_1400" s="T206">NN_1914_Riddles_flk.029 (001.029)</ta>
            <ta e="T234" id="Seg_1401" s="T227">NN_1914_Riddles_flk.030 (001.030)</ta>
            <ta e="T242" id="Seg_1402" s="T234">NN_1914_Riddles_flk.031 (001.031)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1403" s="T0">Măjagən aspaʔ mĭnzəlleʔbə— ködəmge. </ta>
            <ta e="T8" id="Seg_1404" s="T4">Măjagən süjö sĭktölaːmbi— marga. </ta>
            <ta e="T15" id="Seg_1405" s="T8">Boštə šü tʼogən, kugojdə dü tʼogən— tugul. </ta>
            <ta e="T22" id="Seg_1406" s="T15">Šün toʔgən šide boːs ne iʔbəjəʔ— măjʔka. </ta>
            <ta e="T30" id="Seg_1407" s="T22">Üjüt naga, udat naga, măjanə sʼalaːndəga— neni šü. </ta>
            <ta e="T38" id="Seg_1408" s="T30">Süjö iʔgö, uluzaŋdə oʔb tʼogən, kötendə băzəj— maʔ. </ta>
            <ta e="T43" id="Seg_1409" s="T38">Orogən tʼüpi büzəj iʔbe— šĭke. </ta>
            <ta e="T52" id="Seg_1410" s="T43">Nuna lăbəlaʔ kojobi, tʼalaš nʼi nulaʔ šolaʔ üzəbi— kʼama. </ta>
            <ta e="T59" id="Seg_1411" s="T52">Tagajbə seləbiem, seləne pim bünə üštəbiem— tʼaʔ. </ta>
            <ta e="T66" id="Seg_1412" s="T59">Iʔbəbində koškagadə boʔbdə, uʔbdəbində inegedə püržə— duga. </ta>
            <ta e="T70" id="Seg_1413" s="T66">Ujužət, udažət numan üzəlie— baltu. </ta>
            <ta e="T74" id="Seg_1414" s="T70">Šadǝra koʔbdə amna— ĭntak. </ta>
            <ta e="T80" id="Seg_1415" s="T74">Uruʔbə oʔpəliom, oʔpəleʔ ej moliom— aʔtʼə. </ta>
            <ta e="T86" id="Seg_1416" s="T80">Kamdʼzʼu kubiaŋ, tʼildəleʔ ej moliam— nanzə. </ta>
            <ta e="T93" id="Seg_1417" s="T86">Teʔdə tibi oʔb üžün šerleʔ nogaiʔ— stol. </ta>
            <ta e="T102" id="Seg_1418" s="T93">Šö tʼogən pa pajliajəʔ, taptə döbər saʔməlaʔbə— pʼaŋdona sazə. </ta>
            <ta e="T108" id="Seg_1419" s="T102">Tuluŋzəbi koʔbdə jadan kunzə mĭŋge— šaːškən. </ta>
            <ta e="T116" id="Seg_1420" s="T108">Ige dĭrgit tʼerəm gibər pa mĭnzʼət— tagajnə šăn. </ta>
            <ta e="T130" id="Seg_1421" s="T116">Kujan jilgəndə nulam, altənəj bargam nʼeʔlim, kin jilgəndə nulam, kümüžəj bargam nʼeʔlim— tažə, kuruʔjo. </ta>
            <ta e="T138" id="Seg_1422" s="T130">Nemnəjzəbi büzʼe amna, dĭ büzʼem ambinan tʼorlal— köbergən. </ta>
            <ta e="T148" id="Seg_1423" s="T138">Šide marat sʼukku, šide marat tuluka, orta tʼergəndə marka— kaptə. </ta>
            <ta e="T160" id="Seg_1424" s="T148">Šide kuza iʔbə, šide kuza nuga, sumnaŋgit kuza bokulaʔ mĭŋge— kăloda, ajə. </ta>
            <ta e="T166" id="Seg_1425" s="T160">Amniom, amniom, tüleʔ ej moliam— băra. </ta>
            <ta e="T175" id="Seg_1426" s="T166">Körispə amna šiden nĭgəndə, tʼimat tʼugən, kürüt kudaigən— koŋgoro. </ta>
            <ta e="T181" id="Seg_1427" s="T175">Nünörim da sadərim— săbərgu pʼeːš săbərəjʔdna. </ta>
            <ta e="T190" id="Seg_1428" s="T181">Nʼi noga, orta tʼerdə amolaʔbə a marat kĭnzəleʔbə— samovar. </ta>
            <ta e="T197" id="Seg_1429" s="T190">Šide tibi koža tʼargo šürle kandəgaiʔ— tejme. </ta>
            <ta e="T206" id="Seg_1430" s="T197">Üjütsʼəʔ pʼaŋdəliat, nanətsʼəʔ kĭškəliet, aŋdə šiʔdəliet, tibər păʔdəliat— sogarzʼət. </ta>
            <ta e="T227" id="Seg_1431" s="T206">Toʔbdit măna, münördə măna, ige măna tarzəbi tʼer, tarzəbi tʼerən šüjündə kümə tʼer ige, kümə tʼerən šüjündə nʼamga tʼer ige— sanə. </ta>
            <ta e="T234" id="Seg_1432" s="T227">Pam paroldi kük noʔ özerbi— tʼüstək, ki. </ta>
            <ta e="T242" id="Seg_1433" s="T234">Kömə poʔdə iʔböne tʼogən noʔ ej özerie— šü. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1434" s="T0">măja-gən</ta>
            <ta e="T2" id="Seg_1435" s="T1">aspaʔ</ta>
            <ta e="T3" id="Seg_1436" s="T2">mĭnzəl-leʔbə</ta>
            <ta e="T4" id="Seg_1437" s="T3">ködəmge</ta>
            <ta e="T5" id="Seg_1438" s="T4">măja-gən</ta>
            <ta e="T6" id="Seg_1439" s="T5">süjö</ta>
            <ta e="T7" id="Seg_1440" s="T6">sĭkt-ö-laːm-bi</ta>
            <ta e="T8" id="Seg_1441" s="T7">marga</ta>
            <ta e="T9" id="Seg_1442" s="T8">boš-tə</ta>
            <ta e="T10" id="Seg_1443" s="T9">šü</ta>
            <ta e="T11" id="Seg_1444" s="T10">tʼo-gən</ta>
            <ta e="T12" id="Seg_1445" s="T11">kugoj-də</ta>
            <ta e="T13" id="Seg_1446" s="T12">dü</ta>
            <ta e="T14" id="Seg_1447" s="T13">tʼo-gən</ta>
            <ta e="T15" id="Seg_1448" s="T14">tugul</ta>
            <ta e="T16" id="Seg_1449" s="T15">šü-n</ta>
            <ta e="T17" id="Seg_1450" s="T16">toʔ-gən</ta>
            <ta e="T18" id="Seg_1451" s="T17">šide</ta>
            <ta e="T19" id="Seg_1452" s="T18">boːs</ta>
            <ta e="T20" id="Seg_1453" s="T19">ne</ta>
            <ta e="T21" id="Seg_1454" s="T20">iʔbə-jəʔ</ta>
            <ta e="T22" id="Seg_1455" s="T21">măjʔka</ta>
            <ta e="T23" id="Seg_1456" s="T22">üjü-t</ta>
            <ta e="T24" id="Seg_1457" s="T23">naga</ta>
            <ta e="T25" id="Seg_1458" s="T24">uda-t</ta>
            <ta e="T26" id="Seg_1459" s="T25">naga</ta>
            <ta e="T27" id="Seg_1460" s="T26">măja-nə</ta>
            <ta e="T28" id="Seg_1461" s="T27">sʼa-laːndə-ga</ta>
            <ta e="T29" id="Seg_1462" s="T28">nen-i</ta>
            <ta e="T30" id="Seg_1463" s="T29">šü</ta>
            <ta e="T31" id="Seg_1464" s="T30">süjö</ta>
            <ta e="T32" id="Seg_1465" s="T31">iʔgö</ta>
            <ta e="T33" id="Seg_1466" s="T32">ulu-zaŋ-də</ta>
            <ta e="T34" id="Seg_1467" s="T33">oʔb</ta>
            <ta e="T35" id="Seg_1468" s="T34">tʼo-gən</ta>
            <ta e="T36" id="Seg_1469" s="T35">köten-də</ta>
            <ta e="T37" id="Seg_1470" s="T36">băzəj</ta>
            <ta e="T38" id="Seg_1471" s="T37">maʔ</ta>
            <ta e="T39" id="Seg_1472" s="T38">oro-gən</ta>
            <ta e="T40" id="Seg_1473" s="T39">tʼüpi</ta>
            <ta e="T41" id="Seg_1474" s="T40">büzəj</ta>
            <ta e="T42" id="Seg_1475" s="T41">iʔbe</ta>
            <ta e="T43" id="Seg_1476" s="T42">šĭke</ta>
            <ta e="T44" id="Seg_1477" s="T43">nuna</ta>
            <ta e="T45" id="Seg_1478" s="T44">lăbə-laʔ</ta>
            <ta e="T46" id="Seg_1479" s="T45">kojo-bi</ta>
            <ta e="T47" id="Seg_1480" s="T46">tʼalaš</ta>
            <ta e="T48" id="Seg_1481" s="T47">nʼi</ta>
            <ta e="T49" id="Seg_1482" s="T48">nu-laʔ</ta>
            <ta e="T50" id="Seg_1483" s="T49">šo-laʔ</ta>
            <ta e="T51" id="Seg_1484" s="T50">üzə-bi</ta>
            <ta e="T52" id="Seg_1485" s="T51">kʼama</ta>
            <ta e="T53" id="Seg_1486" s="T52">tagaj-bə</ta>
            <ta e="T54" id="Seg_1487" s="T53">selə-bie-m</ta>
            <ta e="T55" id="Seg_1488" s="T54">selə-ne</ta>
            <ta e="T56" id="Seg_1489" s="T55">pi-m</ta>
            <ta e="T57" id="Seg_1490" s="T56">bü-nə</ta>
            <ta e="T58" id="Seg_1491" s="T57">üštə-bie-m</ta>
            <ta e="T59" id="Seg_1492" s="T58">tʼaʔ</ta>
            <ta e="T60" id="Seg_1493" s="T59">iʔbə-bi-ndə</ta>
            <ta e="T61" id="Seg_1494" s="T60">koška-ga=də</ta>
            <ta e="T62" id="Seg_1495" s="T61">boʔbdə</ta>
            <ta e="T63" id="Seg_1496" s="T62">uʔbdə-bi-ndə</ta>
            <ta e="T64" id="Seg_1497" s="T63">ine-ge=də</ta>
            <ta e="T65" id="Seg_1498" s="T64">püržə</ta>
            <ta e="T66" id="Seg_1499" s="T65">duga</ta>
            <ta e="T67" id="Seg_1500" s="T66">uju-žət</ta>
            <ta e="T68" id="Seg_1501" s="T67">uda-žət</ta>
            <ta e="T69" id="Seg_1502" s="T68">numan üzə-lie</ta>
            <ta e="T70" id="Seg_1503" s="T69">baltu</ta>
            <ta e="T71" id="Seg_1504" s="T70">šadəra</ta>
            <ta e="T72" id="Seg_1505" s="T71">koʔbdə</ta>
            <ta e="T73" id="Seg_1506" s="T72">amna</ta>
            <ta e="T74" id="Seg_1507" s="T73">ĭntak</ta>
            <ta e="T75" id="Seg_1508" s="T74">uruʔ-bə</ta>
            <ta e="T76" id="Seg_1509" s="T75">oʔpə-lio-m</ta>
            <ta e="T77" id="Seg_1510" s="T76">oʔpə-leʔ</ta>
            <ta e="T78" id="Seg_1511" s="T77">ej</ta>
            <ta e="T79" id="Seg_1512" s="T78">mo-lio-m</ta>
            <ta e="T80" id="Seg_1513" s="T79">aʔtʼə</ta>
            <ta e="T81" id="Seg_1514" s="T80">kamdʼzʼu</ta>
            <ta e="T82" id="Seg_1515" s="T81">ku-bia-̆ŋ</ta>
            <ta e="T83" id="Seg_1516" s="T82">tʼildə-leʔ</ta>
            <ta e="T84" id="Seg_1517" s="T83">ej</ta>
            <ta e="T85" id="Seg_1518" s="T84">mo-lia-m</ta>
            <ta e="T86" id="Seg_1519" s="T85">nanzə</ta>
            <ta e="T87" id="Seg_1520" s="T86">teʔdə</ta>
            <ta e="T88" id="Seg_1521" s="T87">tibi</ta>
            <ta e="T89" id="Seg_1522" s="T88">oʔb</ta>
            <ta e="T90" id="Seg_1523" s="T89">üžü-n</ta>
            <ta e="T91" id="Seg_1524" s="T90">šer-leʔ</ta>
            <ta e="T92" id="Seg_1525" s="T91">no-ga-iʔ</ta>
            <ta e="T93" id="Seg_1526" s="T92">stol</ta>
            <ta e="T94" id="Seg_1527" s="T93">šö</ta>
            <ta e="T95" id="Seg_1528" s="T94">tʼo-gən</ta>
            <ta e="T96" id="Seg_1529" s="T95">pa</ta>
            <ta e="T97" id="Seg_1530" s="T96">pa-j-lia-jəʔ</ta>
            <ta e="T98" id="Seg_1531" s="T97">taptə</ta>
            <ta e="T99" id="Seg_1532" s="T98">döbər</ta>
            <ta e="T100" id="Seg_1533" s="T99">saʔmə-laʔbə</ta>
            <ta e="T101" id="Seg_1534" s="T100">pʼaŋd-o-na</ta>
            <ta e="T102" id="Seg_1535" s="T101">sazə</ta>
            <ta e="T103" id="Seg_1536" s="T102">tuluŋ-zəbi</ta>
            <ta e="T104" id="Seg_1537" s="T103">koʔbdə</ta>
            <ta e="T105" id="Seg_1538" s="T104">jada-n</ta>
            <ta e="T106" id="Seg_1539" s="T105">kunzə</ta>
            <ta e="T107" id="Seg_1540" s="T106">mĭŋ-ge</ta>
            <ta e="T108" id="Seg_1541" s="T107">šaːškən</ta>
            <ta e="T109" id="Seg_1542" s="T108">i-ge</ta>
            <ta e="T110" id="Seg_1543" s="T109">dĭrgit</ta>
            <ta e="T111" id="Seg_1544" s="T110">tʼerə-m</ta>
            <ta e="T112" id="Seg_1545" s="T111">gibər</ta>
            <ta e="T113" id="Seg_1546" s="T112">pa</ta>
            <ta e="T114" id="Seg_1547" s="T113">mĭn-zʼət</ta>
            <ta e="T115" id="Seg_1548" s="T114">tagaj-nə</ta>
            <ta e="T116" id="Seg_1549" s="T115">šăn</ta>
            <ta e="T117" id="Seg_1550" s="T116">kuja-n</ta>
            <ta e="T118" id="Seg_1551" s="T117">jil-gəndə</ta>
            <ta e="T119" id="Seg_1552" s="T118">nu-la-m</ta>
            <ta e="T120" id="Seg_1553" s="T119">altən-əj</ta>
            <ta e="T121" id="Seg_1554" s="T120">barga-m</ta>
            <ta e="T122" id="Seg_1555" s="T121">nʼeʔ-li-m</ta>
            <ta e="T123" id="Seg_1556" s="T122">ki-n</ta>
            <ta e="T124" id="Seg_1557" s="T123">jil-gəndə</ta>
            <ta e="T125" id="Seg_1558" s="T124">nu-la-m</ta>
            <ta e="T126" id="Seg_1559" s="T125">kümüž-əj</ta>
            <ta e="T127" id="Seg_1560" s="T126">barga-m</ta>
            <ta e="T128" id="Seg_1561" s="T127">nʼeʔ-li-m</ta>
            <ta e="T129" id="Seg_1562" s="T128">tažə</ta>
            <ta e="T130" id="Seg_1563" s="T129">kuruʔjo</ta>
            <ta e="T131" id="Seg_1564" s="T130">nemnəj-zəbi</ta>
            <ta e="T132" id="Seg_1565" s="T131">büzʼe</ta>
            <ta e="T133" id="Seg_1566" s="T132">amna</ta>
            <ta e="T134" id="Seg_1567" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_1568" s="T134">büzʼe-m</ta>
            <ta e="T136" id="Seg_1569" s="T135">am-bi-nan</ta>
            <ta e="T137" id="Seg_1570" s="T136">tʼor-la-l</ta>
            <ta e="T138" id="Seg_1571" s="T137">köbergən</ta>
            <ta e="T139" id="Seg_1572" s="T138">šide</ta>
            <ta e="T140" id="Seg_1573" s="T139">mara-t</ta>
            <ta e="T141" id="Seg_1574" s="T140">sʼukku</ta>
            <ta e="T142" id="Seg_1575" s="T141">šide</ta>
            <ta e="T143" id="Seg_1576" s="T142">mara-t</ta>
            <ta e="T144" id="Seg_1577" s="T143">tuluka</ta>
            <ta e="T145" id="Seg_1578" s="T144">orta</ta>
            <ta e="T146" id="Seg_1579" s="T145">tʼer-gəndə</ta>
            <ta e="T147" id="Seg_1580" s="T146">marka</ta>
            <ta e="T148" id="Seg_1581" s="T147">kaptə</ta>
            <ta e="T149" id="Seg_1582" s="T148">šide</ta>
            <ta e="T150" id="Seg_1583" s="T149">kuza</ta>
            <ta e="T151" id="Seg_1584" s="T150">iʔbə</ta>
            <ta e="T152" id="Seg_1585" s="T151">šide</ta>
            <ta e="T153" id="Seg_1586" s="T152">kuza</ta>
            <ta e="T154" id="Seg_1587" s="T153">nu-ga</ta>
            <ta e="T155" id="Seg_1588" s="T154">sumnaŋ-git</ta>
            <ta e="T156" id="Seg_1589" s="T155">kuza</ta>
            <ta e="T157" id="Seg_1590" s="T156">boku-laʔ</ta>
            <ta e="T158" id="Seg_1591" s="T157">mĭŋ-ge</ta>
            <ta e="T159" id="Seg_1592" s="T158">kăloda</ta>
            <ta e="T160" id="Seg_1593" s="T159">ajə</ta>
            <ta e="T161" id="Seg_1594" s="T160">am-nio-m</ta>
            <ta e="T162" id="Seg_1595" s="T161">am-nio-m</ta>
            <ta e="T163" id="Seg_1596" s="T162">tü-leʔ</ta>
            <ta e="T164" id="Seg_1597" s="T163">ej</ta>
            <ta e="T165" id="Seg_1598" s="T164">mo-lia-m</ta>
            <ta e="T166" id="Seg_1599" s="T165">băra</ta>
            <ta e="T167" id="Seg_1600" s="T166">körispə</ta>
            <ta e="T168" id="Seg_1601" s="T167">amna</ta>
            <ta e="T169" id="Seg_1602" s="T168">šiden</ta>
            <ta e="T170" id="Seg_1603" s="T169">nĭ-gəndə</ta>
            <ta e="T171" id="Seg_1604" s="T170">tʼima-t</ta>
            <ta e="T172" id="Seg_1605" s="T171">tʼu-gən</ta>
            <ta e="T173" id="Seg_1606" s="T172">kürü-t</ta>
            <ta e="T174" id="Seg_1607" s="T173">kudai-gən</ta>
            <ta e="T175" id="Seg_1608" s="T174">koŋgoro</ta>
            <ta e="T176" id="Seg_1609" s="T175">nünör-i-m</ta>
            <ta e="T177" id="Seg_1610" s="T176">da</ta>
            <ta e="T178" id="Seg_1611" s="T177">sadər-i-m</ta>
            <ta e="T179" id="Seg_1612" s="T178">săbərgu</ta>
            <ta e="T180" id="Seg_1613" s="T179">pʼeːš</ta>
            <ta e="T181" id="Seg_1614" s="T180">săbərəjʔd-na</ta>
            <ta e="T182" id="Seg_1615" s="T181">nʼi</ta>
            <ta e="T183" id="Seg_1616" s="T182">no-ga</ta>
            <ta e="T184" id="Seg_1617" s="T183">orta</ta>
            <ta e="T185" id="Seg_1618" s="T184">tʼer-də</ta>
            <ta e="T186" id="Seg_1619" s="T185">amo-laʔbə</ta>
            <ta e="T187" id="Seg_1620" s="T186">a</ta>
            <ta e="T188" id="Seg_1621" s="T187">mara-t</ta>
            <ta e="T189" id="Seg_1622" s="T188">kĭnzə-leʔbə</ta>
            <ta e="T190" id="Seg_1623" s="T189">samovar</ta>
            <ta e="T191" id="Seg_1624" s="T190">šide</ta>
            <ta e="T192" id="Seg_1625" s="T191">tibi</ta>
            <ta e="T193" id="Seg_1626" s="T192">koža</ta>
            <ta e="T194" id="Seg_1627" s="T193">tʼargo</ta>
            <ta e="T195" id="Seg_1628" s="T194">šür-le</ta>
            <ta e="T196" id="Seg_1629" s="T195">kandə-ga-iʔ</ta>
            <ta e="T197" id="Seg_1630" s="T196">tejme</ta>
            <ta e="T198" id="Seg_1631" s="T197">üjü-t-sʼəʔ</ta>
            <ta e="T199" id="Seg_1632" s="T198">pʼaŋdə-lia-t</ta>
            <ta e="T200" id="Seg_1633" s="T199">nanə-t-sʼəʔ</ta>
            <ta e="T201" id="Seg_1634" s="T200">kĭškə-lie-t</ta>
            <ta e="T202" id="Seg_1635" s="T201">aŋ-də</ta>
            <ta e="T203" id="Seg_1636" s="T202">šiʔdə-lie-t</ta>
            <ta e="T204" id="Seg_1637" s="T203">tibər</ta>
            <ta e="T205" id="Seg_1638" s="T204">păʔ-də-lia-t</ta>
            <ta e="T206" id="Seg_1639" s="T205">sogar-zʼət</ta>
            <ta e="T207" id="Seg_1640" s="T206">toʔbdi-t</ta>
            <ta e="T208" id="Seg_1641" s="T207">măna</ta>
            <ta e="T209" id="Seg_1642" s="T208">münör-də</ta>
            <ta e="T210" id="Seg_1643" s="T209">măna</ta>
            <ta e="T211" id="Seg_1644" s="T210">i-ge</ta>
            <ta e="T212" id="Seg_1645" s="T211">măna</ta>
            <ta e="T213" id="Seg_1646" s="T212">tar-zəbi</ta>
            <ta e="T214" id="Seg_1647" s="T213">tʼer</ta>
            <ta e="T215" id="Seg_1648" s="T214">tar-zəbi</ta>
            <ta e="T216" id="Seg_1649" s="T215">tʼer-ə-n</ta>
            <ta e="T217" id="Seg_1650" s="T216">šüjü-ndə</ta>
            <ta e="T218" id="Seg_1651" s="T217">kümə</ta>
            <ta e="T219" id="Seg_1652" s="T218">tʼer</ta>
            <ta e="T220" id="Seg_1653" s="T219">i-ge</ta>
            <ta e="T221" id="Seg_1654" s="T220">kümə</ta>
            <ta e="T222" id="Seg_1655" s="T221">tʼer-ə-n</ta>
            <ta e="T223" id="Seg_1656" s="T222">šüjü-ndə</ta>
            <ta e="T224" id="Seg_1657" s="T223">nʼamga</ta>
            <ta e="T225" id="Seg_1658" s="T224">tʼer</ta>
            <ta e="T226" id="Seg_1659" s="T225">i-ge</ta>
            <ta e="T227" id="Seg_1660" s="T226">sanə</ta>
            <ta e="T228" id="Seg_1661" s="T227">pa-m</ta>
            <ta e="T229" id="Seg_1662" s="T228">par-old-i</ta>
            <ta e="T230" id="Seg_1663" s="T229">kük</ta>
            <ta e="T231" id="Seg_1664" s="T230">noʔ</ta>
            <ta e="T232" id="Seg_1665" s="T231">özer-bi</ta>
            <ta e="T233" id="Seg_1666" s="T232">tʼüstək</ta>
            <ta e="T234" id="Seg_1667" s="T233">ki</ta>
            <ta e="T235" id="Seg_1668" s="T234">kömə</ta>
            <ta e="T236" id="Seg_1669" s="T235">poʔdə</ta>
            <ta e="T237" id="Seg_1670" s="T236">iʔb-ö-ne</ta>
            <ta e="T238" id="Seg_1671" s="T237">tʼo-gən</ta>
            <ta e="T239" id="Seg_1672" s="T238">noʔ</ta>
            <ta e="T240" id="Seg_1673" s="T239">ej</ta>
            <ta e="T241" id="Seg_1674" s="T240">özer-ie</ta>
            <ta e="T242" id="Seg_1675" s="T241">šü</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1676" s="T0">măja-Kən</ta>
            <ta e="T2" id="Seg_1677" s="T1">aspaʔ</ta>
            <ta e="T3" id="Seg_1678" s="T2">mĭnzəl-laʔbə</ta>
            <ta e="T4" id="Seg_1679" s="T3">ködəmge</ta>
            <ta e="T5" id="Seg_1680" s="T4">măja-Kən</ta>
            <ta e="T6" id="Seg_1681" s="T5">süjö</ta>
            <ta e="T7" id="Seg_1682" s="T6">sĭkt-o-laːm-bi</ta>
            <ta e="T8" id="Seg_1683" s="T7">marga</ta>
            <ta e="T9" id="Seg_1684" s="T8">bos-də</ta>
            <ta e="T10" id="Seg_1685" s="T9">šö</ta>
            <ta e="T11" id="Seg_1686" s="T10">tʼo-Kən</ta>
            <ta e="T12" id="Seg_1687" s="T11">kugoj-də</ta>
            <ta e="T13" id="Seg_1688" s="T12">dö</ta>
            <ta e="T14" id="Seg_1689" s="T13">tʼo-Kən</ta>
            <ta e="T15" id="Seg_1690" s="T14">tugul</ta>
            <ta e="T16" id="Seg_1691" s="T15">šü-n</ta>
            <ta e="T17" id="Seg_1692" s="T16">toʔ-Kən</ta>
            <ta e="T18" id="Seg_1693" s="T17">šide</ta>
            <ta e="T19" id="Seg_1694" s="T18">boːs</ta>
            <ta e="T20" id="Seg_1695" s="T19">ne</ta>
            <ta e="T21" id="Seg_1696" s="T20">iʔbə-jəʔ</ta>
            <ta e="T22" id="Seg_1697" s="T21">măjʔka</ta>
            <ta e="T23" id="Seg_1698" s="T22">üjü-t</ta>
            <ta e="T24" id="Seg_1699" s="T23">naga</ta>
            <ta e="T25" id="Seg_1700" s="T24">uda-t</ta>
            <ta e="T26" id="Seg_1701" s="T25">naga</ta>
            <ta e="T27" id="Seg_1702" s="T26">măja-Tə</ta>
            <ta e="T28" id="Seg_1703" s="T27">sʼa-laːndə-gA</ta>
            <ta e="T29" id="Seg_1704" s="T28">nen-j</ta>
            <ta e="T30" id="Seg_1705" s="T29">šü</ta>
            <ta e="T31" id="Seg_1706" s="T30">süjö</ta>
            <ta e="T32" id="Seg_1707" s="T31">iʔgö</ta>
            <ta e="T33" id="Seg_1708" s="T32">ulu-zAŋ-də</ta>
            <ta e="T34" id="Seg_1709" s="T33">oʔb</ta>
            <ta e="T35" id="Seg_1710" s="T34">tʼo-Kən</ta>
            <ta e="T36" id="Seg_1711" s="T35">köten-də</ta>
            <ta e="T37" id="Seg_1712" s="T36">băzəj</ta>
            <ta e="T38" id="Seg_1713" s="T37">maʔ</ta>
            <ta e="T39" id="Seg_1714" s="T38">oro-Kən</ta>
            <ta e="T40" id="Seg_1715" s="T39">tʼüpi</ta>
            <ta e="T41" id="Seg_1716" s="T40">büzəj</ta>
            <ta e="T42" id="Seg_1717" s="T41">iʔbə</ta>
            <ta e="T43" id="Seg_1718" s="T42">šĭkə</ta>
            <ta e="T44" id="Seg_1719" s="T43">nuna</ta>
            <ta e="T45" id="Seg_1720" s="T44">lăbə-lAʔ</ta>
            <ta e="T46" id="Seg_1721" s="T45">kojo-bi</ta>
            <ta e="T47" id="Seg_1722" s="T46">tʼalaš</ta>
            <ta e="T48" id="Seg_1723" s="T47">nʼi</ta>
            <ta e="T49" id="Seg_1724" s="T48">nu-lAʔ</ta>
            <ta e="T50" id="Seg_1725" s="T49">šo-lAʔ</ta>
            <ta e="T51" id="Seg_1726" s="T50">üzə-bi</ta>
            <ta e="T52" id="Seg_1727" s="T51">kʼama</ta>
            <ta e="T53" id="Seg_1728" s="T52">tagaj-m</ta>
            <ta e="T54" id="Seg_1729" s="T53">selə-bi-m</ta>
            <ta e="T55" id="Seg_1730" s="T54">selə-NTA</ta>
            <ta e="T56" id="Seg_1731" s="T55">pi-m</ta>
            <ta e="T57" id="Seg_1732" s="T56">bü-Tə</ta>
            <ta e="T58" id="Seg_1733" s="T57">üštə-bi-m</ta>
            <ta e="T59" id="Seg_1734" s="T58">tʼaʔ</ta>
            <ta e="T60" id="Seg_1735" s="T59">iʔbə-bi-gəndə</ta>
            <ta e="T61" id="Seg_1736" s="T60">koška-gəʔ=da</ta>
            <ta e="T62" id="Seg_1737" s="T61">boʔbdə</ta>
            <ta e="T63" id="Seg_1738" s="T62">uʔbdə-bi-gəndə</ta>
            <ta e="T64" id="Seg_1739" s="T63">ine-gəʔ=da</ta>
            <ta e="T65" id="Seg_1740" s="T64">püržə</ta>
            <ta e="T66" id="Seg_1741" s="T65">duga</ta>
            <ta e="T67" id="Seg_1742" s="T66">üjü-žət</ta>
            <ta e="T68" id="Seg_1743" s="T67">uda-žət</ta>
            <ta e="T69" id="Seg_1744" s="T68">numan üzə-liA</ta>
            <ta e="T70" id="Seg_1745" s="T69">baltu</ta>
            <ta e="T71" id="Seg_1746" s="T70">šadəra</ta>
            <ta e="T72" id="Seg_1747" s="T71">koʔbdo</ta>
            <ta e="T73" id="Seg_1748" s="T72">amnə</ta>
            <ta e="T74" id="Seg_1749" s="T73">ĭntak</ta>
            <ta e="T75" id="Seg_1750" s="T74">uruʔ-m</ta>
            <ta e="T76" id="Seg_1751" s="T75">oʔbdə-liA-m</ta>
            <ta e="T77" id="Seg_1752" s="T76">oʔbdə-lAʔ</ta>
            <ta e="T78" id="Seg_1753" s="T77">ej</ta>
            <ta e="T79" id="Seg_1754" s="T78">mo-liA-m</ta>
            <ta e="T80" id="Seg_1755" s="T79">aʔtʼi</ta>
            <ta e="T81" id="Seg_1756" s="T80">kamdʼžʼu</ta>
            <ta e="T82" id="Seg_1757" s="T81">ku-bi-̆ŋ</ta>
            <ta e="T83" id="Seg_1758" s="T82">tʼildə-lAʔ</ta>
            <ta e="T84" id="Seg_1759" s="T83">ej</ta>
            <ta e="T85" id="Seg_1760" s="T84">mo-liA-m</ta>
            <ta e="T86" id="Seg_1761" s="T85">nanzə</ta>
            <ta e="T87" id="Seg_1762" s="T86">teʔdə</ta>
            <ta e="T88" id="Seg_1763" s="T87">tibi</ta>
            <ta e="T89" id="Seg_1764" s="T88">oʔb</ta>
            <ta e="T90" id="Seg_1765" s="T89">üžü-n</ta>
            <ta e="T91" id="Seg_1766" s="T90">šer-lAʔ</ta>
            <ta e="T92" id="Seg_1767" s="T91">nu-gA-jəʔ</ta>
            <ta e="T93" id="Seg_1768" s="T92">stol</ta>
            <ta e="T94" id="Seg_1769" s="T93">šö</ta>
            <ta e="T95" id="Seg_1770" s="T94">tʼo-Kən</ta>
            <ta e="T96" id="Seg_1771" s="T95">pa</ta>
            <ta e="T97" id="Seg_1772" s="T96">pa-j-liA-jəʔ</ta>
            <ta e="T98" id="Seg_1773" s="T97">taptə</ta>
            <ta e="T99" id="Seg_1774" s="T98">döbər</ta>
            <ta e="T100" id="Seg_1775" s="T99">saʔmə-laʔbə</ta>
            <ta e="T101" id="Seg_1776" s="T100">pʼaŋdə-o-NTA</ta>
            <ta e="T102" id="Seg_1777" s="T101">sazən</ta>
            <ta e="T103" id="Seg_1778" s="T102">tuluŋ-zəbi</ta>
            <ta e="T104" id="Seg_1779" s="T103">koʔbdo</ta>
            <ta e="T105" id="Seg_1780" s="T104">jada-n</ta>
            <ta e="T106" id="Seg_1781" s="T105">kunzə</ta>
            <ta e="T107" id="Seg_1782" s="T106">mĭn-gA</ta>
            <ta e="T108" id="Seg_1783" s="T107">šaːškən</ta>
            <ta e="T109" id="Seg_1784" s="T108">i-gA</ta>
            <ta e="T110" id="Seg_1785" s="T109">dĭrgit</ta>
            <ta e="T111" id="Seg_1786" s="T110">tʼerə-m</ta>
            <ta e="T112" id="Seg_1787" s="T111">gibər</ta>
            <ta e="T113" id="Seg_1788" s="T112">pa</ta>
            <ta e="T114" id="Seg_1789" s="T113">mĭn-zit</ta>
            <ta e="T115" id="Seg_1790" s="T114">tagaj-nə</ta>
            <ta e="T116" id="Seg_1791" s="T115">šăn</ta>
            <ta e="T117" id="Seg_1792" s="T116">kuja-n</ta>
            <ta e="T118" id="Seg_1793" s="T117">il-gəndə</ta>
            <ta e="T119" id="Seg_1794" s="T118">nu-lV-m</ta>
            <ta e="T120" id="Seg_1795" s="T119">altən-j</ta>
            <ta e="T121" id="Seg_1796" s="T120">barga-m</ta>
            <ta e="T122" id="Seg_1797" s="T121">nʼeʔbdə-lV-m</ta>
            <ta e="T123" id="Seg_1798" s="T122">ki-n</ta>
            <ta e="T124" id="Seg_1799" s="T123">il-gəndə</ta>
            <ta e="T125" id="Seg_1800" s="T124">nu-lV-m</ta>
            <ta e="T126" id="Seg_1801" s="T125">kümüž-j</ta>
            <ta e="T127" id="Seg_1802" s="T126">barga-m</ta>
            <ta e="T128" id="Seg_1803" s="T127">nʼeʔbdə-lV-m</ta>
            <ta e="T129" id="Seg_1804" s="T128">tažə</ta>
            <ta e="T130" id="Seg_1805" s="T129">kuruʔjo</ta>
            <ta e="T131" id="Seg_1806" s="T130">nemnəj-zəbi</ta>
            <ta e="T132" id="Seg_1807" s="T131">büzʼe</ta>
            <ta e="T133" id="Seg_1808" s="T132">amnə</ta>
            <ta e="T134" id="Seg_1809" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_1810" s="T134">büzʼe-m</ta>
            <ta e="T136" id="Seg_1811" s="T135">am-bi-gənAn</ta>
            <ta e="T137" id="Seg_1812" s="T136">tʼor-lV-l</ta>
            <ta e="T138" id="Seg_1813" s="T137">köbergən</ta>
            <ta e="T139" id="Seg_1814" s="T138">šide</ta>
            <ta e="T140" id="Seg_1815" s="T139">mara-t</ta>
            <ta e="T141" id="Seg_1816" s="T140">sʼukku</ta>
            <ta e="T142" id="Seg_1817" s="T141">šide</ta>
            <ta e="T143" id="Seg_1818" s="T142">mara-t</ta>
            <ta e="T144" id="Seg_1819" s="T143">tuluka</ta>
            <ta e="T145" id="Seg_1820" s="T144">orta</ta>
            <ta e="T146" id="Seg_1821" s="T145">tʼer-gəndə</ta>
            <ta e="T147" id="Seg_1822" s="T146">marka</ta>
            <ta e="T148" id="Seg_1823" s="T147">kaptə</ta>
            <ta e="T149" id="Seg_1824" s="T148">šide</ta>
            <ta e="T150" id="Seg_1825" s="T149">kuza</ta>
            <ta e="T151" id="Seg_1826" s="T150">iʔbə</ta>
            <ta e="T152" id="Seg_1827" s="T151">šide</ta>
            <ta e="T153" id="Seg_1828" s="T152">kuza</ta>
            <ta e="T154" id="Seg_1829" s="T153">nu-gA</ta>
            <ta e="T155" id="Seg_1830" s="T154">sumna-git</ta>
            <ta e="T156" id="Seg_1831" s="T155">kuza</ta>
            <ta e="T157" id="Seg_1832" s="T156">bokuʔ-lAʔ</ta>
            <ta e="T158" id="Seg_1833" s="T157">mĭn-gA</ta>
            <ta e="T159" id="Seg_1834" s="T158">kăloda</ta>
            <ta e="T160" id="Seg_1835" s="T159">ajə</ta>
            <ta e="T161" id="Seg_1836" s="T160">am-liA-m</ta>
            <ta e="T162" id="Seg_1837" s="T161">am-liA-m</ta>
            <ta e="T163" id="Seg_1838" s="T162">tüʔ-lAʔ</ta>
            <ta e="T164" id="Seg_1839" s="T163">ej</ta>
            <ta e="T165" id="Seg_1840" s="T164">mo-liA-m</ta>
            <ta e="T166" id="Seg_1841" s="T165">băra</ta>
            <ta e="T167" id="Seg_1842" s="T166">körispə</ta>
            <ta e="T168" id="Seg_1843" s="T167">amnə</ta>
            <ta e="T169" id="Seg_1844" s="T168">šeden</ta>
            <ta e="T170" id="Seg_1845" s="T169">nĭ-gəndə</ta>
            <ta e="T171" id="Seg_1846" s="T170">tʼima-t</ta>
            <ta e="T172" id="Seg_1847" s="T171">tʼü-Kən</ta>
            <ta e="T173" id="Seg_1848" s="T172">kürü-t</ta>
            <ta e="T174" id="Seg_1849" s="T173">kudaj-Kən</ta>
            <ta e="T175" id="Seg_1850" s="T174">koŋgoro</ta>
            <ta e="T176" id="Seg_1851" s="T175">nünör-lV-m</ta>
            <ta e="T177" id="Seg_1852" s="T176">da</ta>
            <ta e="T178" id="Seg_1853" s="T177">sădər-lV-m</ta>
            <ta e="T179" id="Seg_1854" s="T178">săbərgu</ta>
            <ta e="T180" id="Seg_1855" s="T179">pʼeːš</ta>
            <ta e="T181" id="Seg_1856" s="T180">săbərəjʔdə-NTA</ta>
            <ta e="T182" id="Seg_1857" s="T181">nʼi</ta>
            <ta e="T183" id="Seg_1858" s="T182">nu-gA</ta>
            <ta e="T184" id="Seg_1859" s="T183">orta</ta>
            <ta e="T185" id="Seg_1860" s="T184">tʼer-də</ta>
            <ta e="T186" id="Seg_1861" s="T185">amo-laʔbə</ta>
            <ta e="T187" id="Seg_1862" s="T186">a</ta>
            <ta e="T188" id="Seg_1863" s="T187">mara-t</ta>
            <ta e="T189" id="Seg_1864" s="T188">kĭnzə-laʔbə</ta>
            <ta e="T190" id="Seg_1865" s="T189">samovar</ta>
            <ta e="T191" id="Seg_1866" s="T190">šide</ta>
            <ta e="T192" id="Seg_1867" s="T191">tibi</ta>
            <ta e="T193" id="Seg_1868" s="T192">koža</ta>
            <ta e="T194" id="Seg_1869" s="T193">tʼargo</ta>
            <ta e="T195" id="Seg_1870" s="T194">šür-lAʔ</ta>
            <ta e="T196" id="Seg_1871" s="T195">kandə-gA-jəʔ</ta>
            <ta e="T197" id="Seg_1872" s="T196">tejme</ta>
            <ta e="T198" id="Seg_1873" s="T197">üjü-t-ziʔ</ta>
            <ta e="T199" id="Seg_1874" s="T198">pʼaŋdə-liA-t</ta>
            <ta e="T200" id="Seg_1875" s="T199">nanə-t-ziʔ</ta>
            <ta e="T201" id="Seg_1876" s="T200">kĭškə-liA-t</ta>
            <ta e="T202" id="Seg_1877" s="T201">aŋ-də</ta>
            <ta e="T203" id="Seg_1878" s="T202">šiʔdə-liA-t</ta>
            <ta e="T204" id="Seg_1879" s="T203">dĭbər</ta>
            <ta e="T205" id="Seg_1880" s="T204">păda-də-liA-t</ta>
            <ta e="T206" id="Seg_1881" s="T205">sogar-zit</ta>
            <ta e="T207" id="Seg_1882" s="T206">toʔbdə-t</ta>
            <ta e="T208" id="Seg_1883" s="T207">măna</ta>
            <ta e="T209" id="Seg_1884" s="T208">münör-t</ta>
            <ta e="T210" id="Seg_1885" s="T209">măna</ta>
            <ta e="T211" id="Seg_1886" s="T210">i-gA</ta>
            <ta e="T212" id="Seg_1887" s="T211">măna</ta>
            <ta e="T213" id="Seg_1888" s="T212">tar-zəbi</ta>
            <ta e="T214" id="Seg_1889" s="T213">tʼer</ta>
            <ta e="T215" id="Seg_1890" s="T214">tar-zəbi</ta>
            <ta e="T216" id="Seg_1891" s="T215">tʼer-ə-n</ta>
            <ta e="T217" id="Seg_1892" s="T216">šüjə-gəndə</ta>
            <ta e="T218" id="Seg_1893" s="T217">kömə</ta>
            <ta e="T219" id="Seg_1894" s="T218">tʼer</ta>
            <ta e="T220" id="Seg_1895" s="T219">i-gA</ta>
            <ta e="T221" id="Seg_1896" s="T220">kömə</ta>
            <ta e="T222" id="Seg_1897" s="T221">tʼer-ə-n</ta>
            <ta e="T223" id="Seg_1898" s="T222">šüjə-gəndə</ta>
            <ta e="T224" id="Seg_1899" s="T223">nʼamga</ta>
            <ta e="T225" id="Seg_1900" s="T224">tʼer</ta>
            <ta e="T226" id="Seg_1901" s="T225">i-gA</ta>
            <ta e="T227" id="Seg_1902" s="T226">sanə</ta>
            <ta e="T228" id="Seg_1903" s="T227">pa-m</ta>
            <ta e="T229" id="Seg_1904" s="T228">par-old-j</ta>
            <ta e="T230" id="Seg_1905" s="T229">kük</ta>
            <ta e="T231" id="Seg_1906" s="T230">noʔ</ta>
            <ta e="T232" id="Seg_1907" s="T231">özer-bi</ta>
            <ta e="T233" id="Seg_1908" s="T232">tʼüstək</ta>
            <ta e="T234" id="Seg_1909" s="T233">ki</ta>
            <ta e="T235" id="Seg_1910" s="T234">kömə</ta>
            <ta e="T236" id="Seg_1911" s="T235">poʔto</ta>
            <ta e="T237" id="Seg_1912" s="T236">iʔbə-ö-NTA</ta>
            <ta e="T238" id="Seg_1913" s="T237">tʼo-Kən</ta>
            <ta e="T239" id="Seg_1914" s="T238">noʔ</ta>
            <ta e="T240" id="Seg_1915" s="T239">ej</ta>
            <ta e="T241" id="Seg_1916" s="T240">özer-liA</ta>
            <ta e="T242" id="Seg_1917" s="T241">šü</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1918" s="T0">mountain-LOC</ta>
            <ta e="T2" id="Seg_1919" s="T1">cauldron.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1920" s="T2">boil-DUR.[3SG]</ta>
            <ta e="T4" id="Seg_1921" s="T3">ant.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1922" s="T4">mountain-LOC</ta>
            <ta e="T6" id="Seg_1923" s="T5">bird.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1924" s="T6">choke-DETR-RES-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1925" s="T7">nipple.[NOM.SG]</ta>
            <ta e="T9" id="Seg_1926" s="T8">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T10" id="Seg_1927" s="T9">that.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1928" s="T10">place-LOC</ta>
            <ta e="T12" id="Seg_1929" s="T11">earring-NOM/GEN/ACC.3SG</ta>
            <ta e="T13" id="Seg_1930" s="T12">that.[NOM.SG]</ta>
            <ta e="T14" id="Seg_1931" s="T13">place-LOC</ta>
            <ta e="T15" id="Seg_1932" s="T14">martagon.lily</ta>
            <ta e="T16" id="Seg_1933" s="T15">fire-GEN</ta>
            <ta e="T17" id="Seg_1934" s="T16">edge-LOC</ta>
            <ta e="T18" id="Seg_1935" s="T17">two.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1936" s="T18">pregnant.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1937" s="T19">woman.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1938" s="T20">lie.down-3PL</ta>
            <ta e="T22" id="Seg_1939" s="T21">calf.of.a.leg.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1940" s="T22">foot-NOM/GEN.3SG</ta>
            <ta e="T24" id="Seg_1941" s="T23">NEG.EX.[3SG]</ta>
            <ta e="T25" id="Seg_1942" s="T24">hand-NOM/GEN.3SG</ta>
            <ta e="T26" id="Seg_1943" s="T25">NEG.EX.[3SG]</ta>
            <ta e="T27" id="Seg_1944" s="T26">mountain-LAT</ta>
            <ta e="T28" id="Seg_1945" s="T27">climb-DUR-PRS.[3SG]</ta>
            <ta e="T29" id="Seg_1946" s="T28">burn-ADJZ</ta>
            <ta e="T30" id="Seg_1947" s="T29">fire.[NOM.SG]</ta>
            <ta e="T31" id="Seg_1948" s="T30">bird.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1949" s="T31">many</ta>
            <ta e="T33" id="Seg_1950" s="T32">head-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T34" id="Seg_1951" s="T33">one.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1952" s="T34">place-LOC</ta>
            <ta e="T36" id="Seg_1953" s="T35">ass-NOM/GEN/ACC.3SG</ta>
            <ta e="T37" id="Seg_1954" s="T36">apart</ta>
            <ta e="T38" id="Seg_1955" s="T37">tent.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1956" s="T38">den-LOC</ta>
            <ta e="T40" id="Seg_1957" s="T39">moist.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1958" s="T40">calf.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1959" s="T41">lie.down.[3SG]</ta>
            <ta e="T43" id="Seg_1960" s="T42">language.[NOM.SG]</ta>
            <ta e="T44" id="Seg_1961" s="T43">cliff.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1962" s="T44">slide-CVB</ta>
            <ta e="T46" id="Seg_1963" s="T45">stay-PST.[3SG]</ta>
            <ta e="T47" id="Seg_1964" s="T46">naked.[NOM.SG]</ta>
            <ta e="T48" id="Seg_1965" s="T47">boy.[NOM.SG]</ta>
            <ta e="T49" id="Seg_1966" s="T48">stand-CVB</ta>
            <ta e="T50" id="Seg_1967" s="T49">come-CVB</ta>
            <ta e="T51" id="Seg_1968" s="T50">descend-PST.[3SG]</ta>
            <ta e="T52" id="Seg_1969" s="T51">marrow.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1970" s="T52">knife-NOM/GEN/ACC.1SG</ta>
            <ta e="T54" id="Seg_1971" s="T53">sharpen-PST-1SG</ta>
            <ta e="T55" id="Seg_1972" s="T54">sharpen-PTCP</ta>
            <ta e="T56" id="Seg_1973" s="T55">stone-ACC</ta>
            <ta e="T57" id="Seg_1974" s="T56">water-LAT</ta>
            <ta e="T58" id="Seg_1975" s="T57">drop-PST-1SG</ta>
            <ta e="T59" id="Seg_1976" s="T58">otter.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1977" s="T59">lie.down-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T61" id="Seg_1978" s="T60">cat-ABL=PTCL</ta>
            <ta e="T62" id="Seg_1979" s="T61">low.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1980" s="T62">get.up-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T64" id="Seg_1981" s="T63">horse-ABL=PTCL</ta>
            <ta e="T65" id="Seg_1982" s="T64">tall.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1983" s="T65">saddle.[NOM.SG]</ta>
            <ta e="T67" id="Seg_1984" s="T66">foot-CAR.ADJ</ta>
            <ta e="T68" id="Seg_1985" s="T67">hand-CAR.ADJ</ta>
            <ta e="T69" id="Seg_1986" s="T68">pray-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_1987" s="T69">axe.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1988" s="T70">pockmarked.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1989" s="T71">girl.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1990" s="T72">sit.[3SG]</ta>
            <ta e="T74" id="Seg_1991" s="T73">thimble.[NOM.SG]</ta>
            <ta e="T75" id="Seg_1992" s="T74">lasso-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_1993" s="T75">collect-PRS-1SG</ta>
            <ta e="T77" id="Seg_1994" s="T76">collect-CVB</ta>
            <ta e="T78" id="Seg_1995" s="T77">NEG</ta>
            <ta e="T79" id="Seg_1996" s="T78">can-PRS-1SG</ta>
            <ta e="T80" id="Seg_1997" s="T79">road.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1998" s="T80">whip.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1999" s="T81">find-PST-1SG</ta>
            <ta e="T83" id="Seg_2000" s="T82">pick.up-CVB</ta>
            <ta e="T84" id="Seg_2001" s="T83">NEG</ta>
            <ta e="T85" id="Seg_2002" s="T84">can-PRS-1SG</ta>
            <ta e="T86" id="Seg_2003" s="T85">snake.[NOM.SG]</ta>
            <ta e="T87" id="Seg_2004" s="T86">four.[NOM.SG]</ta>
            <ta e="T88" id="Seg_2005" s="T87">man.[NOM.SG]</ta>
            <ta e="T89" id="Seg_2006" s="T88">one.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2007" s="T89">cap-GEN</ta>
            <ta e="T91" id="Seg_2008" s="T90">wear-CVB</ta>
            <ta e="T92" id="Seg_2009" s="T91">stand-PRS-3PL</ta>
            <ta e="T93" id="Seg_2010" s="T92">table.[NOM.SG]</ta>
            <ta e="T94" id="Seg_2011" s="T93">that.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2012" s="T94">place-LOC</ta>
            <ta e="T96" id="Seg_2013" s="T95">tree.[NOM.SG]</ta>
            <ta e="T97" id="Seg_2014" s="T96">tree-VBLZ-PRS-3PL</ta>
            <ta e="T98" id="Seg_2015" s="T97">splinter.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2016" s="T98">here</ta>
            <ta e="T100" id="Seg_2017" s="T99">fall-DUR.[3SG]</ta>
            <ta e="T101" id="Seg_2018" s="T100">write-DETR-PTCP</ta>
            <ta e="T102" id="Seg_2019" s="T101">paper.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2020" s="T102">braid-ADJZ.[NOM.SG]</ta>
            <ta e="T104" id="Seg_2021" s="T103">girl.[NOM.SG]</ta>
            <ta e="T105" id="Seg_2022" s="T104">village-GEN</ta>
            <ta e="T106" id="Seg_2023" s="T105">along</ta>
            <ta e="T107" id="Seg_2024" s="T106">go-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_2025" s="T107">magpie.[NOM.SG]</ta>
            <ta e="T109" id="Seg_2026" s="T108">be-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_2027" s="T109">such.[NOM.SG]</ta>
            <ta e="T111" id="Seg_2028" s="T110">item-NOM/GEN/ACC.1SG</ta>
            <ta e="T112" id="Seg_2029" s="T111">where.to</ta>
            <ta e="T113" id="Seg_2030" s="T112">tree.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2031" s="T113">go-INF</ta>
            <ta e="T115" id="Seg_2032" s="T114">knife-GEN.1SG</ta>
            <ta e="T116" id="Seg_2033" s="T115">sheath.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2034" s="T116">sun-GEN</ta>
            <ta e="T118" id="Seg_2035" s="T117">underpart-LAT/LOC.3SG</ta>
            <ta e="T119" id="Seg_2036" s="T118">stand-FUT-1SG</ta>
            <ta e="T120" id="Seg_2037" s="T119">gold-ADJZ.[NOM.SG]</ta>
            <ta e="T121" id="Seg_2038" s="T120">horn-NOM/GEN/ACC.1SG</ta>
            <ta e="T122" id="Seg_2039" s="T121">blow-FUT-1SG</ta>
            <ta e="T123" id="Seg_2040" s="T122">moon-GEN</ta>
            <ta e="T124" id="Seg_2041" s="T123">underpart-LAT/LOC.3SG</ta>
            <ta e="T125" id="Seg_2042" s="T124">stand-FUT-1SG</ta>
            <ta e="T126" id="Seg_2043" s="T125">silver-ADJZ.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2044" s="T126">horn-NOM/GEN/ACC.1SG</ta>
            <ta e="T128" id="Seg_2045" s="T127">blow-FUT-1SG</ta>
            <ta e="T129" id="Seg_2046" s="T128">goose.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2047" s="T129">crane.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2048" s="T130">rag-ADJZ.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2049" s="T131">man.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2050" s="T132">sit.[3SG]</ta>
            <ta e="T134" id="Seg_2051" s="T133">this.[NOM.SG]</ta>
            <ta e="T135" id="Seg_2052" s="T134">man-ACC</ta>
            <ta e="T136" id="Seg_2053" s="T135">eat-CVB.TEMP-LAT/LOC.2SG</ta>
            <ta e="T137" id="Seg_2054" s="T136">cry-FUT-2SG</ta>
            <ta e="T138" id="Seg_2055" s="T137">onion.[NOM.SG]</ta>
            <ta e="T139" id="Seg_2056" s="T138">two.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2057" s="T139">end-NOM/GEN.3SG</ta>
            <ta e="T141" id="Seg_2058" s="T140">sharp.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2059" s="T141">two.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2060" s="T142">end-NOM/GEN.3SG</ta>
            <ta e="T144" id="Seg_2061" s="T143">round.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2062" s="T144">directly</ta>
            <ta e="T146" id="Seg_2063" s="T145">center-LAT/LOC.3SG</ta>
            <ta e="T147" id="Seg_2064" s="T146">button.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2065" s="T147">scissors.[NOM.SG]</ta>
            <ta e="T149" id="Seg_2066" s="T148">two.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2067" s="T149">man.[NOM.SG]</ta>
            <ta e="T151" id="Seg_2068" s="T150">lie.down.[3SG]</ta>
            <ta e="T152" id="Seg_2069" s="T151">two.[NOM.SG]</ta>
            <ta e="T153" id="Seg_2070" s="T152">man.[NOM.SG]</ta>
            <ta e="T154" id="Seg_2071" s="T153">stand-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_2072" s="T154">five-ORD.[NOM.SG]</ta>
            <ta e="T156" id="Seg_2073" s="T155">man.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2074" s="T156">tie.up-CVB</ta>
            <ta e="T158" id="Seg_2075" s="T157">go-PRS.[3SG]</ta>
            <ta e="T159" id="Seg_2076" s="T158">threshold.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2077" s="T159">door.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2078" s="T160">eat-PRS-1SG</ta>
            <ta e="T162" id="Seg_2079" s="T161">eat-PRS-1SG</ta>
            <ta e="T163" id="Seg_2080" s="T162">shit-CVB</ta>
            <ta e="T164" id="Seg_2081" s="T163">NEG</ta>
            <ta e="T165" id="Seg_2082" s="T164">can-PRS-1SG</ta>
            <ta e="T166" id="Seg_2083" s="T165">sack.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2084" s="T166">rooster.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2085" s="T167">sit.[3SG]</ta>
            <ta e="T169" id="Seg_2086" s="T168">corral</ta>
            <ta e="T170" id="Seg_2087" s="T169">top-LAT/LOC.3SG</ta>
            <ta e="T171" id="Seg_2088" s="T170">tail-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_2089" s="T171">land-LOC</ta>
            <ta e="T173" id="Seg_2090" s="T172">voice-NOM/GEN.3SG</ta>
            <ta e="T174" id="Seg_2091" s="T173">God-LOC</ta>
            <ta e="T175" id="Seg_2092" s="T174">bell.[NOM.SG]</ta>
            <ta e="T176" id="Seg_2093" s="T175">wet-FUT-1SG</ta>
            <ta e="T177" id="Seg_2094" s="T176">and</ta>
            <ta e="T178" id="Seg_2095" s="T177">shake-FUT-1SG</ta>
            <ta e="T179" id="Seg_2096" s="T178">broom.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2097" s="T179">stove.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2098" s="T180">sweep-PTCP</ta>
            <ta e="T182" id="Seg_2099" s="T181">boy.[NOM.SG]</ta>
            <ta e="T183" id="Seg_2100" s="T182">stand-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_2101" s="T183">directly</ta>
            <ta e="T185" id="Seg_2102" s="T184">center-NOM/GEN/ACC.3SG</ta>
            <ta e="T186" id="Seg_2103" s="T185">burn-DUR.[3SG]</ta>
            <ta e="T187" id="Seg_2104" s="T186">and</ta>
            <ta e="T188" id="Seg_2105" s="T187">end-NOM/GEN.3SG</ta>
            <ta e="T189" id="Seg_2106" s="T188">piss-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_2107" s="T189">samovar.[NOM.SG]</ta>
            <ta e="T191" id="Seg_2108" s="T190">two.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2109" s="T191">man.[NOM.SG]</ta>
            <ta e="T193" id="Seg_2110" s="T192">side.by.side</ta>
            <ta e="T194" id="Seg_2111" s="T193">for.contest</ta>
            <ta e="T195" id="Seg_2112" s="T194">run-CVB</ta>
            <ta e="T196" id="Seg_2113" s="T195">walk-PRS-3PL</ta>
            <ta e="T197" id="Seg_2114" s="T196">ski.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2115" s="T197">foot-3SG-INS</ta>
            <ta e="T199" id="Seg_2116" s="T198">press-PRS-3SG.O</ta>
            <ta e="T200" id="Seg_2117" s="T199">belly-3SG-INS</ta>
            <ta e="T201" id="Seg_2118" s="T200">rub-PRS-3SG.O</ta>
            <ta e="T202" id="Seg_2119" s="T201">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T203" id="Seg_2120" s="T202">open-PRS-3SG.O</ta>
            <ta e="T204" id="Seg_2121" s="T203">there</ta>
            <ta e="T205" id="Seg_2122" s="T204">creep.into-TR-PRS-3SG.O</ta>
            <ta e="T206" id="Seg_2123" s="T205">weave-INF</ta>
            <ta e="T207" id="Seg_2124" s="T206">hit-IMP.2SG.O</ta>
            <ta e="T208" id="Seg_2125" s="T207">I.ACC</ta>
            <ta e="T209" id="Seg_2126" s="T208">beat-IMP.2SG.O</ta>
            <ta e="T210" id="Seg_2127" s="T209">I.ACC</ta>
            <ta e="T211" id="Seg_2128" s="T210">be-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_2129" s="T211">I.LAT</ta>
            <ta e="T213" id="Seg_2130" s="T212">hair-ADJZ.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2131" s="T213">center.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2132" s="T214">hair-ADJZ.[NOM.SG]</ta>
            <ta e="T216" id="Seg_2133" s="T215">center-EP-GEN</ta>
            <ta e="T217" id="Seg_2134" s="T216">inside-LAT/LOC.3SG</ta>
            <ta e="T218" id="Seg_2135" s="T217">red.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2136" s="T218">center.[NOM.SG]</ta>
            <ta e="T220" id="Seg_2137" s="T219">be-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_2138" s="T220">red.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2139" s="T221">center-EP-GEN</ta>
            <ta e="T223" id="Seg_2140" s="T222">inside-LAT/LOC.3SG</ta>
            <ta e="T224" id="Seg_2141" s="T223">sweet.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2142" s="T224">center.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2143" s="T225">be-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_2144" s="T226">pine.nut.[NOM.SG]</ta>
            <ta e="T228" id="Seg_2145" s="T227">tree-ACC</ta>
            <ta e="T229" id="Seg_2146" s="T228">return-TR-ADJZ</ta>
            <ta e="T230" id="Seg_2147" s="T229">green.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2148" s="T230">grass.[NOM.SG]</ta>
            <ta e="T232" id="Seg_2149" s="T231">grow-PST.[3SG]</ta>
            <ta e="T233" id="Seg_2150" s="T232">ring.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2151" s="T233">penis.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2152" s="T234">red.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2153" s="T235">goat.[NOM.SG]</ta>
            <ta e="T237" id="Seg_2154" s="T236">lie.down-DUR-PTCP</ta>
            <ta e="T238" id="Seg_2155" s="T237">place-LOC</ta>
            <ta e="T239" id="Seg_2156" s="T238">grass.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2157" s="T239">NEG</ta>
            <ta e="T241" id="Seg_2158" s="T240">grow-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_2159" s="T241">fire.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2160" s="T0">гора-LOC</ta>
            <ta e="T2" id="Seg_2161" s="T1">котел.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2162" s="T2">кипеть-DUR.[3SG]</ta>
            <ta e="T4" id="Seg_2163" s="T3">муравей.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2164" s="T4">гора-LOC</ta>
            <ta e="T6" id="Seg_2165" s="T5">птица.[NOM.SG]</ta>
            <ta e="T7" id="Seg_2166" s="T6">задушить-DETR-RES-PST.[3SG]</ta>
            <ta e="T8" id="Seg_2167" s="T7">сосок.[NOM.SG]</ta>
            <ta e="T9" id="Seg_2168" s="T8">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T10" id="Seg_2169" s="T9">тот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2170" s="T10">место-LOC</ta>
            <ta e="T12" id="Seg_2171" s="T11">серьга-NOM/GEN/ACC.3SG</ta>
            <ta e="T13" id="Seg_2172" s="T12">тот.[NOM.SG]</ta>
            <ta e="T14" id="Seg_2173" s="T13">место-LOC</ta>
            <ta e="T15" id="Seg_2174" s="T14">лилия.саранка</ta>
            <ta e="T16" id="Seg_2175" s="T15">огонь-GEN</ta>
            <ta e="T17" id="Seg_2176" s="T16">край-LOC</ta>
            <ta e="T18" id="Seg_2177" s="T17">два.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2178" s="T18">беременный.[NOM.SG]</ta>
            <ta e="T20" id="Seg_2179" s="T19">женщина.[NOM.SG]</ta>
            <ta e="T21" id="Seg_2180" s="T20">ложиться-3PL</ta>
            <ta e="T22" id="Seg_2181" s="T21">икра.ноги.[NOM.SG]</ta>
            <ta e="T23" id="Seg_2182" s="T22">нога-NOM/GEN.3SG</ta>
            <ta e="T24" id="Seg_2183" s="T23">NEG.EX.[3SG]</ta>
            <ta e="T25" id="Seg_2184" s="T24">рука-NOM/GEN.3SG</ta>
            <ta e="T26" id="Seg_2185" s="T25">NEG.EX.[3SG]</ta>
            <ta e="T27" id="Seg_2186" s="T26">гора-LAT</ta>
            <ta e="T28" id="Seg_2187" s="T27">влезать-DUR-PRS.[3SG]</ta>
            <ta e="T29" id="Seg_2188" s="T28">гореть-ADJZ</ta>
            <ta e="T30" id="Seg_2189" s="T29">огонь.[NOM.SG]</ta>
            <ta e="T31" id="Seg_2190" s="T30">птица.[NOM.SG]</ta>
            <ta e="T32" id="Seg_2191" s="T31">много</ta>
            <ta e="T33" id="Seg_2192" s="T32">голова-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T34" id="Seg_2193" s="T33">один.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2194" s="T34">место-LOC</ta>
            <ta e="T36" id="Seg_2195" s="T35">зад-NOM/GEN/ACC.3SG</ta>
            <ta e="T37" id="Seg_2196" s="T36">отдельно</ta>
            <ta e="T38" id="Seg_2197" s="T37">чум.[NOM.SG]</ta>
            <ta e="T39" id="Seg_2198" s="T38">яма-LOC</ta>
            <ta e="T40" id="Seg_2199" s="T39">мокрый.[NOM.SG]</ta>
            <ta e="T41" id="Seg_2200" s="T40">теленок.[NOM.SG]</ta>
            <ta e="T42" id="Seg_2201" s="T41">ложиться.[3SG]</ta>
            <ta e="T43" id="Seg_2202" s="T42">язык.[NOM.SG]</ta>
            <ta e="T44" id="Seg_2203" s="T43">утес.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2204" s="T44">скользить-CVB</ta>
            <ta e="T46" id="Seg_2205" s="T45">остаться-PST.[3SG]</ta>
            <ta e="T47" id="Seg_2206" s="T46">голый.[NOM.SG]</ta>
            <ta e="T48" id="Seg_2207" s="T47">мальчик.[NOM.SG]</ta>
            <ta e="T49" id="Seg_2208" s="T48">стоять-CVB</ta>
            <ta e="T50" id="Seg_2209" s="T49">прийти-CVB</ta>
            <ta e="T51" id="Seg_2210" s="T50">спуститься-PST.[3SG]</ta>
            <ta e="T52" id="Seg_2211" s="T51">костный.мозг.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2212" s="T52">нож-NOM/GEN/ACC.1SG</ta>
            <ta e="T54" id="Seg_2213" s="T53">точить-PST-1SG</ta>
            <ta e="T55" id="Seg_2214" s="T54">точить-PTCP</ta>
            <ta e="T56" id="Seg_2215" s="T55">камень-ACC</ta>
            <ta e="T57" id="Seg_2216" s="T56">вода-LAT</ta>
            <ta e="T58" id="Seg_2217" s="T57">уронить-PST-1SG</ta>
            <ta e="T59" id="Seg_2218" s="T58">выдра.[NOM.SG]</ta>
            <ta e="T60" id="Seg_2219" s="T59">ложиться-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T61" id="Seg_2220" s="T60">кошка-ABL=PTCL</ta>
            <ta e="T62" id="Seg_2221" s="T61">низкий.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2222" s="T62">встать-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T64" id="Seg_2223" s="T63">лошадь-ABL=PTCL</ta>
            <ta e="T65" id="Seg_2224" s="T64">высокий.[NOM.SG]</ta>
            <ta e="T66" id="Seg_2225" s="T65">седло.[NOM.SG]</ta>
            <ta e="T67" id="Seg_2226" s="T66">нога-CAR.ADJ</ta>
            <ta e="T68" id="Seg_2227" s="T67">рука-CAR.ADJ</ta>
            <ta e="T69" id="Seg_2228" s="T68">молиться-PRS.[3SG]</ta>
            <ta e="T70" id="Seg_2229" s="T69">топор.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2230" s="T70">рябой.[NOM.SG]</ta>
            <ta e="T72" id="Seg_2231" s="T71">девушка.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2232" s="T72">сидеть.[3SG]</ta>
            <ta e="T74" id="Seg_2233" s="T73">наперсток.[NOM.SG]</ta>
            <ta e="T75" id="Seg_2234" s="T74">аркан-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_2235" s="T75">собирать-PRS-1SG</ta>
            <ta e="T77" id="Seg_2236" s="T76">собирать-CVB</ta>
            <ta e="T78" id="Seg_2237" s="T77">NEG</ta>
            <ta e="T79" id="Seg_2238" s="T78">мочь-PRS-1SG</ta>
            <ta e="T80" id="Seg_2239" s="T79">дорога.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2240" s="T80">кнут.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2241" s="T81">найти-PST-1SG</ta>
            <ta e="T83" id="Seg_2242" s="T82">поднять-CVB</ta>
            <ta e="T84" id="Seg_2243" s="T83">NEG</ta>
            <ta e="T85" id="Seg_2244" s="T84">мочь-PRS-1SG</ta>
            <ta e="T86" id="Seg_2245" s="T85">змея.[NOM.SG]</ta>
            <ta e="T87" id="Seg_2246" s="T86">четыре.[NOM.SG]</ta>
            <ta e="T88" id="Seg_2247" s="T87">мужчина.[NOM.SG]</ta>
            <ta e="T89" id="Seg_2248" s="T88">один.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2249" s="T89">шапка-GEN</ta>
            <ta e="T91" id="Seg_2250" s="T90">носить-CVB</ta>
            <ta e="T92" id="Seg_2251" s="T91">стоять-PRS-3PL</ta>
            <ta e="T93" id="Seg_2252" s="T92">стол.[NOM.SG]</ta>
            <ta e="T94" id="Seg_2253" s="T93">тот.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2254" s="T94">место-LOC</ta>
            <ta e="T96" id="Seg_2255" s="T95">дерево.[NOM.SG]</ta>
            <ta e="T97" id="Seg_2256" s="T96">дерево-VBLZ-PRS-3PL</ta>
            <ta e="T98" id="Seg_2257" s="T97">щепка.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2258" s="T98">здесь</ta>
            <ta e="T100" id="Seg_2259" s="T99">упасть-DUR.[3SG]</ta>
            <ta e="T101" id="Seg_2260" s="T100">писать-DETR-PTCP</ta>
            <ta e="T102" id="Seg_2261" s="T101">бумага.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2262" s="T102">коса-ADJZ.[NOM.SG]</ta>
            <ta e="T104" id="Seg_2263" s="T103">девушка.[NOM.SG]</ta>
            <ta e="T105" id="Seg_2264" s="T104">деревня-GEN</ta>
            <ta e="T106" id="Seg_2265" s="T105">вдоль</ta>
            <ta e="T107" id="Seg_2266" s="T106">идти-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_2267" s="T107">сорока.[NOM.SG]</ta>
            <ta e="T109" id="Seg_2268" s="T108">быть-PRS.[3SG]</ta>
            <ta e="T110" id="Seg_2269" s="T109">такой.[NOM.SG]</ta>
            <ta e="T111" id="Seg_2270" s="T110">вещь-NOM/GEN/ACC.1SG</ta>
            <ta e="T112" id="Seg_2271" s="T111">куда</ta>
            <ta e="T113" id="Seg_2272" s="T112">дерево.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2273" s="T113">идти-INF</ta>
            <ta e="T115" id="Seg_2274" s="T114">нож-GEN.1SG</ta>
            <ta e="T116" id="Seg_2275" s="T115">ножницы.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2276" s="T116">солнце-GEN</ta>
            <ta e="T118" id="Seg_2277" s="T117">низ-LAT/LOC.3SG</ta>
            <ta e="T119" id="Seg_2278" s="T118">стоять-FUT-1SG</ta>
            <ta e="T120" id="Seg_2279" s="T119">золото-ADJZ.[NOM.SG]</ta>
            <ta e="T121" id="Seg_2280" s="T120">рог-NOM/GEN/ACC.1SG</ta>
            <ta e="T122" id="Seg_2281" s="T121">дуть-FUT-1SG</ta>
            <ta e="T123" id="Seg_2282" s="T122">луна-GEN</ta>
            <ta e="T124" id="Seg_2283" s="T123">низ-LAT/LOC.3SG</ta>
            <ta e="T125" id="Seg_2284" s="T124">стоять-FUT-1SG</ta>
            <ta e="T126" id="Seg_2285" s="T125">серебро-ADJZ.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2286" s="T126">рог-NOM/GEN/ACC.1SG</ta>
            <ta e="T128" id="Seg_2287" s="T127">дуть-FUT-1SG</ta>
            <ta e="T129" id="Seg_2288" s="T128">гусь.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2289" s="T129">журавль.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2290" s="T130">лохмотья-ADJZ.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2291" s="T131">мужчина.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2292" s="T132">сидеть.[3SG]</ta>
            <ta e="T134" id="Seg_2293" s="T133">этот.[NOM.SG]</ta>
            <ta e="T135" id="Seg_2294" s="T134">мужчина-ACC</ta>
            <ta e="T136" id="Seg_2295" s="T135">съесть-CVB.TEMP-LAT/LOC.2SG</ta>
            <ta e="T137" id="Seg_2296" s="T136">плакать-FUT-2SG</ta>
            <ta e="T138" id="Seg_2297" s="T137">лук.[NOM.SG]</ta>
            <ta e="T139" id="Seg_2298" s="T138">два.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2299" s="T139">конец-NOM/GEN.3SG</ta>
            <ta e="T141" id="Seg_2300" s="T140">острый.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2301" s="T141">два.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2302" s="T142">конец-NOM/GEN.3SG</ta>
            <ta e="T144" id="Seg_2303" s="T143">круглый.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2304" s="T144">прямо</ta>
            <ta e="T146" id="Seg_2305" s="T145">центр-LAT/LOC.3SG</ta>
            <ta e="T147" id="Seg_2306" s="T146">пуговица.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2307" s="T147">ножницы.[NOM.SG]</ta>
            <ta e="T149" id="Seg_2308" s="T148">два.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2309" s="T149">мужчина.[NOM.SG]</ta>
            <ta e="T151" id="Seg_2310" s="T150">ложиться.[3SG]</ta>
            <ta e="T152" id="Seg_2311" s="T151">два.[NOM.SG]</ta>
            <ta e="T153" id="Seg_2312" s="T152">мужчина.[NOM.SG]</ta>
            <ta e="T154" id="Seg_2313" s="T153">стоять-PRS.[3SG]</ta>
            <ta e="T155" id="Seg_2314" s="T154">пять-ORD.[NOM.SG]</ta>
            <ta e="T156" id="Seg_2315" s="T155">мужчина.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2316" s="T156">привязывать-CVB</ta>
            <ta e="T158" id="Seg_2317" s="T157">идти-PRS.[3SG]</ta>
            <ta e="T159" id="Seg_2318" s="T158">порог.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2319" s="T159">дверь.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2320" s="T160">съесть-PRS-1SG</ta>
            <ta e="T162" id="Seg_2321" s="T161">съесть-PRS-1SG</ta>
            <ta e="T163" id="Seg_2322" s="T162">испражняться-CVB</ta>
            <ta e="T164" id="Seg_2323" s="T163">NEG</ta>
            <ta e="T165" id="Seg_2324" s="T164">мочь-PRS-1SG</ta>
            <ta e="T166" id="Seg_2325" s="T165">мешок.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2326" s="T166">петух.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2327" s="T167">сидеть.[3SG]</ta>
            <ta e="T169" id="Seg_2328" s="T168">кораль</ta>
            <ta e="T170" id="Seg_2329" s="T169">верх-LAT/LOC.3SG</ta>
            <ta e="T171" id="Seg_2330" s="T170">хвост-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_2331" s="T171">земля-LOC</ta>
            <ta e="T173" id="Seg_2332" s="T172">голос-NOM/GEN.3SG</ta>
            <ta e="T174" id="Seg_2333" s="T173">бог-LOC</ta>
            <ta e="T175" id="Seg_2334" s="T174">колокол.[NOM.SG]</ta>
            <ta e="T176" id="Seg_2335" s="T175">мочить-FUT-1SG</ta>
            <ta e="T177" id="Seg_2336" s="T176">и</ta>
            <ta e="T178" id="Seg_2337" s="T177">трясти-FUT-1SG</ta>
            <ta e="T179" id="Seg_2338" s="T178">метла.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2339" s="T179">печь.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2340" s="T180">мести-PTCP</ta>
            <ta e="T182" id="Seg_2341" s="T181">мальчик.[NOM.SG]</ta>
            <ta e="T183" id="Seg_2342" s="T182">стоять-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_2343" s="T183">прямо</ta>
            <ta e="T185" id="Seg_2344" s="T184">центр-NOM/GEN/ACC.3SG</ta>
            <ta e="T186" id="Seg_2345" s="T185">гореть-DUR.[3SG]</ta>
            <ta e="T187" id="Seg_2346" s="T186">а</ta>
            <ta e="T188" id="Seg_2347" s="T187">конец-NOM/GEN.3SG</ta>
            <ta e="T189" id="Seg_2348" s="T188">мочиться-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_2349" s="T189">самовар.[NOM.SG]</ta>
            <ta e="T191" id="Seg_2350" s="T190">два.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2351" s="T191">мужчина.[NOM.SG]</ta>
            <ta e="T193" id="Seg_2352" s="T192">рядом</ta>
            <ta e="T194" id="Seg_2353" s="T193">наперегонки</ta>
            <ta e="T195" id="Seg_2354" s="T194">бежать-CVB</ta>
            <ta e="T196" id="Seg_2355" s="T195">идти-PRS-3PL</ta>
            <ta e="T197" id="Seg_2356" s="T196">лыжи.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2357" s="T197">нога-3SG-INS</ta>
            <ta e="T199" id="Seg_2358" s="T198">нажимать-PRS-3SG.O</ta>
            <ta e="T200" id="Seg_2359" s="T199">живот-3SG-INS</ta>
            <ta e="T201" id="Seg_2360" s="T200">тереть-PRS-3SG.O</ta>
            <ta e="T202" id="Seg_2361" s="T201">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T203" id="Seg_2362" s="T202">открыть-PRS-3SG.O</ta>
            <ta e="T204" id="Seg_2363" s="T203">там</ta>
            <ta e="T205" id="Seg_2364" s="T204">ползти-TR-PRS-3SG.O</ta>
            <ta e="T206" id="Seg_2365" s="T205">ткать-INF</ta>
            <ta e="T207" id="Seg_2366" s="T206">ударить-IMP.2SG.O</ta>
            <ta e="T208" id="Seg_2367" s="T207">я.ACC</ta>
            <ta e="T209" id="Seg_2368" s="T208">бить-IMP.2SG.O</ta>
            <ta e="T210" id="Seg_2369" s="T209">я.ACC</ta>
            <ta e="T211" id="Seg_2370" s="T210">быть-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_2371" s="T211">я.LAT</ta>
            <ta e="T213" id="Seg_2372" s="T212">шерсть-ADJZ.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2373" s="T213">центр.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2374" s="T214">шерсть-ADJZ.[NOM.SG]</ta>
            <ta e="T216" id="Seg_2375" s="T215">центр-EP-GEN</ta>
            <ta e="T217" id="Seg_2376" s="T216">внутри-LAT/LOC.3SG</ta>
            <ta e="T218" id="Seg_2377" s="T217">красный.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2378" s="T218">центр.[NOM.SG]</ta>
            <ta e="T220" id="Seg_2379" s="T219">быть-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_2380" s="T220">красный.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2381" s="T221">центр-EP-GEN</ta>
            <ta e="T223" id="Seg_2382" s="T222">внутри-LAT/LOC.3SG</ta>
            <ta e="T224" id="Seg_2383" s="T223">сладкий.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2384" s="T224">центр.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2385" s="T225">быть-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_2386" s="T226">кедровый.орех.[NOM.SG]</ta>
            <ta e="T228" id="Seg_2387" s="T227">дерево-ACC</ta>
            <ta e="T229" id="Seg_2388" s="T228">вернуться-TR-ADJZ</ta>
            <ta e="T230" id="Seg_2389" s="T229">зеленый.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2390" s="T230">трава.[NOM.SG]</ta>
            <ta e="T232" id="Seg_2391" s="T231">расти-PST.[3SG]</ta>
            <ta e="T233" id="Seg_2392" s="T232">кольцо.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2393" s="T233">пенис.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2394" s="T234">красный.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2395" s="T235">коза.[NOM.SG]</ta>
            <ta e="T237" id="Seg_2396" s="T236">ложиться-DUR-PTCP</ta>
            <ta e="T238" id="Seg_2397" s="T237">место-LOC</ta>
            <ta e="T239" id="Seg_2398" s="T238">трава.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2399" s="T239">NEG</ta>
            <ta e="T241" id="Seg_2400" s="T240">расти-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_2401" s="T241">огонь.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2402" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2403" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2404" s="T2">v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_2405" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_2406" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_2407" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2408" s="T6">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_2409" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2410" s="T8">refl-n:case.poss</ta>
            <ta e="T10" id="Seg_2411" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_2412" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2413" s="T11">n-n:case.poss</ta>
            <ta e="T13" id="Seg_2414" s="T12">dempro-n:case</ta>
            <ta e="T14" id="Seg_2415" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_2416" s="T14">n</ta>
            <ta e="T16" id="Seg_2417" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2418" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_2419" s="T17">num-n:case</ta>
            <ta e="T19" id="Seg_2420" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_2421" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_2422" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_2423" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_2424" s="T22">n-n:case.poss</ta>
            <ta e="T24" id="Seg_2425" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_2426" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_2427" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_2428" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_2429" s="T27">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_2430" s="T28">v-v&gt;adj</ta>
            <ta e="T30" id="Seg_2431" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_2432" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_2433" s="T31">quant</ta>
            <ta e="T33" id="Seg_2434" s="T32">n-n:num-n:case.poss</ta>
            <ta e="T34" id="Seg_2435" s="T33">num-n:case</ta>
            <ta e="T35" id="Seg_2436" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_2437" s="T35">n-n:case.poss</ta>
            <ta e="T37" id="Seg_2438" s="T36">adv</ta>
            <ta e="T38" id="Seg_2439" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2440" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_2441" s="T39">adj-n:case</ta>
            <ta e="T41" id="Seg_2442" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_2443" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_2444" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_2445" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_2446" s="T44">v-v:n.fin</ta>
            <ta e="T46" id="Seg_2447" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_2448" s="T46">adj-n:case</ta>
            <ta e="T48" id="Seg_2449" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_2450" s="T48">v-v:n.fin</ta>
            <ta e="T50" id="Seg_2451" s="T49">v-v:n.fin</ta>
            <ta e="T51" id="Seg_2452" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_2453" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2454" s="T52">n-n:case.poss</ta>
            <ta e="T54" id="Seg_2455" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2456" s="T54">v-v:n.fin</ta>
            <ta e="T56" id="Seg_2457" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_2458" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_2459" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_2460" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_2461" s="T59">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T61" id="Seg_2462" s="T60">n-n:case=ptcl</ta>
            <ta e="T62" id="Seg_2463" s="T61">adj-n:case</ta>
            <ta e="T63" id="Seg_2464" s="T62">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T64" id="Seg_2465" s="T63">n-n:case=ptcl</ta>
            <ta e="T65" id="Seg_2466" s="T64">adj-n:case</ta>
            <ta e="T66" id="Seg_2467" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_2468" s="T66">n-n&gt;adj</ta>
            <ta e="T68" id="Seg_2469" s="T67">n-n&gt;adj</ta>
            <ta e="T69" id="Seg_2470" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_2471" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_2472" s="T70">adj-n:case</ta>
            <ta e="T72" id="Seg_2473" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2474" s="T72">v-v:pn</ta>
            <ta e="T74" id="Seg_2475" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2476" s="T74">n-n:case.poss</ta>
            <ta e="T76" id="Seg_2477" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2478" s="T76">v-v:n.fin</ta>
            <ta e="T78" id="Seg_2479" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2480" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_2481" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2482" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2483" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_2484" s="T82">v-v:n.fin</ta>
            <ta e="T84" id="Seg_2485" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2486" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_2487" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_2488" s="T86">num-n:case</ta>
            <ta e="T88" id="Seg_2489" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_2490" s="T88">num-n:case</ta>
            <ta e="T90" id="Seg_2491" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2492" s="T90">v-v:n.fin</ta>
            <ta e="T92" id="Seg_2493" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_2494" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_2495" s="T93">dempro-n:case</ta>
            <ta e="T95" id="Seg_2496" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2497" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2498" s="T96">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_2499" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_2500" s="T98">adv</ta>
            <ta e="T100" id="Seg_2501" s="T99">v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_2502" s="T100">v-v&gt;v-v:n.fin</ta>
            <ta e="T102" id="Seg_2503" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2504" s="T102">n-n&gt;adj-n:case</ta>
            <ta e="T104" id="Seg_2505" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2506" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_2507" s="T105">post</ta>
            <ta e="T107" id="Seg_2508" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_2509" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_2510" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_2511" s="T109">adj-n:case</ta>
            <ta e="T111" id="Seg_2512" s="T110">n-n:case.poss</ta>
            <ta e="T112" id="Seg_2513" s="T111">que</ta>
            <ta e="T113" id="Seg_2514" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2515" s="T113">v-v:n.fin</ta>
            <ta e="T115" id="Seg_2516" s="T114">n-n:case.poss</ta>
            <ta e="T116" id="Seg_2517" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_2518" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2519" s="T117">n-n:case.poss</ta>
            <ta e="T119" id="Seg_2520" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_2521" s="T119">n-n&gt;adj-n:case</ta>
            <ta e="T121" id="Seg_2522" s="T120">n-n:case.poss</ta>
            <ta e="T122" id="Seg_2523" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2524" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2525" s="T123">n-n:case.poss</ta>
            <ta e="T125" id="Seg_2526" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_2527" s="T125">n-n&gt;adj-n:case</ta>
            <ta e="T127" id="Seg_2528" s="T126">n-n:case.poss</ta>
            <ta e="T128" id="Seg_2529" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_2530" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_2531" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_2532" s="T130">n-n&gt;adj-n:case</ta>
            <ta e="T132" id="Seg_2533" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_2534" s="T132">v-v:pn</ta>
            <ta e="T134" id="Seg_2535" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_2536" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_2537" s="T135">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T137" id="Seg_2538" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2539" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2540" s="T138">num-n:case</ta>
            <ta e="T140" id="Seg_2541" s="T139">n-n:case.poss</ta>
            <ta e="T141" id="Seg_2542" s="T140">adj-n:case</ta>
            <ta e="T142" id="Seg_2543" s="T141">num-n:case</ta>
            <ta e="T143" id="Seg_2544" s="T142">n-n:case.poss</ta>
            <ta e="T144" id="Seg_2545" s="T143">adj-n:case</ta>
            <ta e="T145" id="Seg_2546" s="T144">adv</ta>
            <ta e="T146" id="Seg_2547" s="T145">n-n:case.poss</ta>
            <ta e="T147" id="Seg_2548" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2549" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2550" s="T148">num-n:case</ta>
            <ta e="T150" id="Seg_2551" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2552" s="T150">v-v:pn</ta>
            <ta e="T152" id="Seg_2553" s="T151">num-n:case</ta>
            <ta e="T153" id="Seg_2554" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2555" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_2556" s="T154">num-num&gt;num-n:case</ta>
            <ta e="T156" id="Seg_2557" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_2558" s="T156">v-v:n.fin</ta>
            <ta e="T158" id="Seg_2559" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_2560" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2561" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2562" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2563" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_2564" s="T162">v-v:n.fin</ta>
            <ta e="T164" id="Seg_2565" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_2566" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_2567" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_2568" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_2569" s="T167">v-v:pn</ta>
            <ta e="T169" id="Seg_2570" s="T168">n</ta>
            <ta e="T170" id="Seg_2571" s="T169">n-n:case.poss</ta>
            <ta e="T171" id="Seg_2572" s="T170">n-n:case.poss</ta>
            <ta e="T172" id="Seg_2573" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_2574" s="T172">n-n:case.poss</ta>
            <ta e="T174" id="Seg_2575" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2576" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_2577" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_2578" s="T176">conj</ta>
            <ta e="T178" id="Seg_2579" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_2580" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_2581" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2582" s="T180">v-v:n.fin</ta>
            <ta e="T182" id="Seg_2583" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_2584" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_2585" s="T183">adv</ta>
            <ta e="T185" id="Seg_2586" s="T184">n-n:case.poss</ta>
            <ta e="T186" id="Seg_2587" s="T185">v-v&gt;v-v:pn</ta>
            <ta e="T187" id="Seg_2588" s="T186">conj</ta>
            <ta e="T188" id="Seg_2589" s="T187">n-n:case.poss</ta>
            <ta e="T189" id="Seg_2590" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_2591" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_2592" s="T190">num-n:case</ta>
            <ta e="T192" id="Seg_2593" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_2594" s="T192">adv</ta>
            <ta e="T194" id="Seg_2595" s="T193">adv</ta>
            <ta e="T195" id="Seg_2596" s="T194">v-v:n.fin</ta>
            <ta e="T196" id="Seg_2597" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2598" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_2599" s="T197">n-n:case.poss-n:case</ta>
            <ta e="T199" id="Seg_2600" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_2601" s="T199">n-n:case.poss-n:case</ta>
            <ta e="T201" id="Seg_2602" s="T200">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_2603" s="T201">n-n:case.poss</ta>
            <ta e="T203" id="Seg_2604" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2605" s="T203">adv</ta>
            <ta e="T205" id="Seg_2606" s="T204">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_2607" s="T205">v-v:n.fin</ta>
            <ta e="T207" id="Seg_2608" s="T206">v-v:mood.pn</ta>
            <ta e="T208" id="Seg_2609" s="T207">pers</ta>
            <ta e="T209" id="Seg_2610" s="T208">v-v:mood.pn</ta>
            <ta e="T210" id="Seg_2611" s="T209">pers</ta>
            <ta e="T211" id="Seg_2612" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_2613" s="T211">pers</ta>
            <ta e="T213" id="Seg_2614" s="T212">n-n&gt;adj-n:case</ta>
            <ta e="T214" id="Seg_2615" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_2616" s="T214">n-n&gt;adj-n:case</ta>
            <ta e="T216" id="Seg_2617" s="T215">n-n:ins-n:case</ta>
            <ta e="T217" id="Seg_2618" s="T216">n-n:case.poss</ta>
            <ta e="T218" id="Seg_2619" s="T217">adj-n:case</ta>
            <ta e="T219" id="Seg_2620" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_2621" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_2622" s="T220">adj-n:case</ta>
            <ta e="T222" id="Seg_2623" s="T221">n-n:ins-n:case</ta>
            <ta e="T223" id="Seg_2624" s="T222">n-n:case.poss</ta>
            <ta e="T224" id="Seg_2625" s="T223">adj-n:case</ta>
            <ta e="T225" id="Seg_2626" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_2627" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_2628" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_2629" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_2630" s="T228">v-v&gt;v-v&gt;adj</ta>
            <ta e="T230" id="Seg_2631" s="T229">adj-n:case</ta>
            <ta e="T231" id="Seg_2632" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_2633" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_2634" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_2635" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_2636" s="T234">adj-n:case</ta>
            <ta e="T236" id="Seg_2637" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_2638" s="T236">v-v&gt;v-v:n.fin</ta>
            <ta e="T238" id="Seg_2639" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_2640" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_2641" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_2642" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_2643" s="T241">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2644" s="T0">n</ta>
            <ta e="T2" id="Seg_2645" s="T1">n</ta>
            <ta e="T3" id="Seg_2646" s="T2">v</ta>
            <ta e="T4" id="Seg_2647" s="T3">n</ta>
            <ta e="T5" id="Seg_2648" s="T4">n</ta>
            <ta e="T6" id="Seg_2649" s="T5">n</ta>
            <ta e="T7" id="Seg_2650" s="T6">v</ta>
            <ta e="T8" id="Seg_2651" s="T7">n</ta>
            <ta e="T9" id="Seg_2652" s="T8">refl</ta>
            <ta e="T10" id="Seg_2653" s="T9">dempro</ta>
            <ta e="T11" id="Seg_2654" s="T10">n</ta>
            <ta e="T12" id="Seg_2655" s="T11">n</ta>
            <ta e="T13" id="Seg_2656" s="T12">dempro</ta>
            <ta e="T14" id="Seg_2657" s="T13">n</ta>
            <ta e="T15" id="Seg_2658" s="T14">n</ta>
            <ta e="T16" id="Seg_2659" s="T15">n</ta>
            <ta e="T17" id="Seg_2660" s="T16">n</ta>
            <ta e="T18" id="Seg_2661" s="T17">num</ta>
            <ta e="T19" id="Seg_2662" s="T18">adj</ta>
            <ta e="T20" id="Seg_2663" s="T19">n</ta>
            <ta e="T21" id="Seg_2664" s="T20">v</ta>
            <ta e="T22" id="Seg_2665" s="T21">n</ta>
            <ta e="T23" id="Seg_2666" s="T22">n</ta>
            <ta e="T24" id="Seg_2667" s="T23">v</ta>
            <ta e="T25" id="Seg_2668" s="T24">n</ta>
            <ta e="T26" id="Seg_2669" s="T25">v</ta>
            <ta e="T27" id="Seg_2670" s="T26">n</ta>
            <ta e="T28" id="Seg_2671" s="T27">v</ta>
            <ta e="T29" id="Seg_2672" s="T28">v</ta>
            <ta e="T30" id="Seg_2673" s="T29">n</ta>
            <ta e="T31" id="Seg_2674" s="T30">n</ta>
            <ta e="T32" id="Seg_2675" s="T31">quant</ta>
            <ta e="T33" id="Seg_2676" s="T32">n</ta>
            <ta e="T34" id="Seg_2677" s="T33">num</ta>
            <ta e="T35" id="Seg_2678" s="T34">n</ta>
            <ta e="T36" id="Seg_2679" s="T35">n</ta>
            <ta e="T37" id="Seg_2680" s="T36">adv</ta>
            <ta e="T38" id="Seg_2681" s="T37">n</ta>
            <ta e="T39" id="Seg_2682" s="T38">n</ta>
            <ta e="T40" id="Seg_2683" s="T39">adj</ta>
            <ta e="T41" id="Seg_2684" s="T40">n</ta>
            <ta e="T42" id="Seg_2685" s="T41">v</ta>
            <ta e="T43" id="Seg_2686" s="T42">n</ta>
            <ta e="T44" id="Seg_2687" s="T43">n</ta>
            <ta e="T45" id="Seg_2688" s="T44">v</ta>
            <ta e="T46" id="Seg_2689" s="T45">v</ta>
            <ta e="T47" id="Seg_2690" s="T46">adj</ta>
            <ta e="T48" id="Seg_2691" s="T47">n</ta>
            <ta e="T49" id="Seg_2692" s="T48">v</ta>
            <ta e="T50" id="Seg_2693" s="T49">v</ta>
            <ta e="T51" id="Seg_2694" s="T50">v</ta>
            <ta e="T52" id="Seg_2695" s="T51">n</ta>
            <ta e="T53" id="Seg_2696" s="T52">n</ta>
            <ta e="T54" id="Seg_2697" s="T53">v</ta>
            <ta e="T55" id="Seg_2698" s="T54">adj</ta>
            <ta e="T56" id="Seg_2699" s="T55">n</ta>
            <ta e="T57" id="Seg_2700" s="T56">n</ta>
            <ta e="T58" id="Seg_2701" s="T57">v</ta>
            <ta e="T59" id="Seg_2702" s="T58">n</ta>
            <ta e="T60" id="Seg_2703" s="T59">v</ta>
            <ta e="T61" id="Seg_2704" s="T60">n</ta>
            <ta e="T62" id="Seg_2705" s="T61">adj</ta>
            <ta e="T63" id="Seg_2706" s="T62">v</ta>
            <ta e="T64" id="Seg_2707" s="T63">n</ta>
            <ta e="T65" id="Seg_2708" s="T64">adj</ta>
            <ta e="T66" id="Seg_2709" s="T65">n</ta>
            <ta e="T67" id="Seg_2710" s="T66">adj</ta>
            <ta e="T68" id="Seg_2711" s="T67">adj</ta>
            <ta e="T69" id="Seg_2712" s="T68">v</ta>
            <ta e="T70" id="Seg_2713" s="T69">n</ta>
            <ta e="T71" id="Seg_2714" s="T70">adj</ta>
            <ta e="T72" id="Seg_2715" s="T71">n</ta>
            <ta e="T73" id="Seg_2716" s="T72">v</ta>
            <ta e="T74" id="Seg_2717" s="T73">n</ta>
            <ta e="T75" id="Seg_2718" s="T74">n</ta>
            <ta e="T76" id="Seg_2719" s="T75">v</ta>
            <ta e="T77" id="Seg_2720" s="T76">v</ta>
            <ta e="T78" id="Seg_2721" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2722" s="T78">v</ta>
            <ta e="T80" id="Seg_2723" s="T79">n</ta>
            <ta e="T81" id="Seg_2724" s="T80">n</ta>
            <ta e="T82" id="Seg_2725" s="T81">v</ta>
            <ta e="T83" id="Seg_2726" s="T82">v</ta>
            <ta e="T84" id="Seg_2727" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2728" s="T84">v</ta>
            <ta e="T86" id="Seg_2729" s="T85">n</ta>
            <ta e="T87" id="Seg_2730" s="T86">num</ta>
            <ta e="T88" id="Seg_2731" s="T87">n</ta>
            <ta e="T89" id="Seg_2732" s="T88">num</ta>
            <ta e="T90" id="Seg_2733" s="T89">n</ta>
            <ta e="T91" id="Seg_2734" s="T90">v</ta>
            <ta e="T92" id="Seg_2735" s="T91">v</ta>
            <ta e="T93" id="Seg_2736" s="T92">n</ta>
            <ta e="T94" id="Seg_2737" s="T93">dempro</ta>
            <ta e="T95" id="Seg_2738" s="T94">n</ta>
            <ta e="T96" id="Seg_2739" s="T95">n</ta>
            <ta e="T97" id="Seg_2740" s="T96">v</ta>
            <ta e="T98" id="Seg_2741" s="T97">n</ta>
            <ta e="T99" id="Seg_2742" s="T98">adv</ta>
            <ta e="T100" id="Seg_2743" s="T99">v</ta>
            <ta e="T101" id="Seg_2744" s="T100">adj</ta>
            <ta e="T102" id="Seg_2745" s="T101">n</ta>
            <ta e="T103" id="Seg_2746" s="T102">adj</ta>
            <ta e="T104" id="Seg_2747" s="T103">n</ta>
            <ta e="T105" id="Seg_2748" s="T104">n</ta>
            <ta e="T106" id="Seg_2749" s="T105">post</ta>
            <ta e="T107" id="Seg_2750" s="T106">v</ta>
            <ta e="T108" id="Seg_2751" s="T107">n</ta>
            <ta e="T109" id="Seg_2752" s="T108">v</ta>
            <ta e="T110" id="Seg_2753" s="T109">adj</ta>
            <ta e="T111" id="Seg_2754" s="T110">n</ta>
            <ta e="T112" id="Seg_2755" s="T111">que</ta>
            <ta e="T113" id="Seg_2756" s="T112">n</ta>
            <ta e="T114" id="Seg_2757" s="T113">v</ta>
            <ta e="T115" id="Seg_2758" s="T114">n</ta>
            <ta e="T116" id="Seg_2759" s="T115">n</ta>
            <ta e="T117" id="Seg_2760" s="T116">n</ta>
            <ta e="T118" id="Seg_2761" s="T117">n</ta>
            <ta e="T119" id="Seg_2762" s="T118">v</ta>
            <ta e="T120" id="Seg_2763" s="T119">adj</ta>
            <ta e="T121" id="Seg_2764" s="T120">n</ta>
            <ta e="T122" id="Seg_2765" s="T121">v</ta>
            <ta e="T123" id="Seg_2766" s="T122">n</ta>
            <ta e="T124" id="Seg_2767" s="T123">n</ta>
            <ta e="T125" id="Seg_2768" s="T124">v</ta>
            <ta e="T126" id="Seg_2769" s="T125">adj</ta>
            <ta e="T127" id="Seg_2770" s="T126">n</ta>
            <ta e="T128" id="Seg_2771" s="T127">v</ta>
            <ta e="T129" id="Seg_2772" s="T128">n</ta>
            <ta e="T130" id="Seg_2773" s="T129">n</ta>
            <ta e="T131" id="Seg_2774" s="T130">adj</ta>
            <ta e="T132" id="Seg_2775" s="T131">n</ta>
            <ta e="T133" id="Seg_2776" s="T132">v</ta>
            <ta e="T134" id="Seg_2777" s="T133">dempro</ta>
            <ta e="T135" id="Seg_2778" s="T134">n</ta>
            <ta e="T136" id="Seg_2779" s="T135">v</ta>
            <ta e="T137" id="Seg_2780" s="T136">v</ta>
            <ta e="T138" id="Seg_2781" s="T137">n</ta>
            <ta e="T139" id="Seg_2782" s="T138">num</ta>
            <ta e="T140" id="Seg_2783" s="T139">n</ta>
            <ta e="T141" id="Seg_2784" s="T140">adj</ta>
            <ta e="T142" id="Seg_2785" s="T141">num</ta>
            <ta e="T143" id="Seg_2786" s="T142">n</ta>
            <ta e="T144" id="Seg_2787" s="T143">adj</ta>
            <ta e="T145" id="Seg_2788" s="T144">adv</ta>
            <ta e="T146" id="Seg_2789" s="T145">n</ta>
            <ta e="T147" id="Seg_2790" s="T146">n</ta>
            <ta e="T148" id="Seg_2791" s="T147">n</ta>
            <ta e="T149" id="Seg_2792" s="T148">num</ta>
            <ta e="T150" id="Seg_2793" s="T149">n</ta>
            <ta e="T151" id="Seg_2794" s="T150">v</ta>
            <ta e="T152" id="Seg_2795" s="T151">num</ta>
            <ta e="T153" id="Seg_2796" s="T152">n</ta>
            <ta e="T154" id="Seg_2797" s="T153">v</ta>
            <ta e="T155" id="Seg_2798" s="T154">num</ta>
            <ta e="T156" id="Seg_2799" s="T155">n</ta>
            <ta e="T157" id="Seg_2800" s="T156">v</ta>
            <ta e="T158" id="Seg_2801" s="T157">v</ta>
            <ta e="T159" id="Seg_2802" s="T158">n</ta>
            <ta e="T160" id="Seg_2803" s="T159">n</ta>
            <ta e="T161" id="Seg_2804" s="T160">v</ta>
            <ta e="T162" id="Seg_2805" s="T161">v</ta>
            <ta e="T163" id="Seg_2806" s="T162">v</ta>
            <ta e="T164" id="Seg_2807" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_2808" s="T164">v</ta>
            <ta e="T166" id="Seg_2809" s="T165">n</ta>
            <ta e="T167" id="Seg_2810" s="T166">n</ta>
            <ta e="T168" id="Seg_2811" s="T167">v</ta>
            <ta e="T169" id="Seg_2812" s="T168">n</ta>
            <ta e="T170" id="Seg_2813" s="T169">n</ta>
            <ta e="T171" id="Seg_2814" s="T170">n</ta>
            <ta e="T172" id="Seg_2815" s="T171">n</ta>
            <ta e="T173" id="Seg_2816" s="T172">n</ta>
            <ta e="T174" id="Seg_2817" s="T173">n</ta>
            <ta e="T175" id="Seg_2818" s="T174">n</ta>
            <ta e="T176" id="Seg_2819" s="T175">v</ta>
            <ta e="T177" id="Seg_2820" s="T176">conj</ta>
            <ta e="T178" id="Seg_2821" s="T177">v</ta>
            <ta e="T179" id="Seg_2822" s="T178">n</ta>
            <ta e="T180" id="Seg_2823" s="T179">n</ta>
            <ta e="T181" id="Seg_2824" s="T180">adj</ta>
            <ta e="T182" id="Seg_2825" s="T181">n</ta>
            <ta e="T183" id="Seg_2826" s="T182">v</ta>
            <ta e="T184" id="Seg_2827" s="T183">adv</ta>
            <ta e="T185" id="Seg_2828" s="T184">n</ta>
            <ta e="T186" id="Seg_2829" s="T185">v</ta>
            <ta e="T187" id="Seg_2830" s="T186">conj</ta>
            <ta e="T188" id="Seg_2831" s="T187">n</ta>
            <ta e="T189" id="Seg_2832" s="T188">v</ta>
            <ta e="T190" id="Seg_2833" s="T189">n</ta>
            <ta e="T191" id="Seg_2834" s="T190">num</ta>
            <ta e="T192" id="Seg_2835" s="T191">n</ta>
            <ta e="T193" id="Seg_2836" s="T192">adv</ta>
            <ta e="T194" id="Seg_2837" s="T193">adv</ta>
            <ta e="T195" id="Seg_2838" s="T194">v</ta>
            <ta e="T196" id="Seg_2839" s="T195">v</ta>
            <ta e="T197" id="Seg_2840" s="T196">n</ta>
            <ta e="T198" id="Seg_2841" s="T197">n</ta>
            <ta e="T199" id="Seg_2842" s="T198">v</ta>
            <ta e="T200" id="Seg_2843" s="T199">n</ta>
            <ta e="T201" id="Seg_2844" s="T200">v</ta>
            <ta e="T202" id="Seg_2845" s="T201">n</ta>
            <ta e="T203" id="Seg_2846" s="T202">v</ta>
            <ta e="T204" id="Seg_2847" s="T203">adv</ta>
            <ta e="T205" id="Seg_2848" s="T204">v</ta>
            <ta e="T206" id="Seg_2849" s="T205">v</ta>
            <ta e="T207" id="Seg_2850" s="T206">v</ta>
            <ta e="T208" id="Seg_2851" s="T207">pers</ta>
            <ta e="T209" id="Seg_2852" s="T208">v</ta>
            <ta e="T210" id="Seg_2853" s="T209">pers</ta>
            <ta e="T211" id="Seg_2854" s="T210">v</ta>
            <ta e="T212" id="Seg_2855" s="T211">pers</ta>
            <ta e="T213" id="Seg_2856" s="T212">adj</ta>
            <ta e="T214" id="Seg_2857" s="T213">n</ta>
            <ta e="T215" id="Seg_2858" s="T214">adj</ta>
            <ta e="T216" id="Seg_2859" s="T215">n</ta>
            <ta e="T217" id="Seg_2860" s="T216">n</ta>
            <ta e="T218" id="Seg_2861" s="T217">adj</ta>
            <ta e="T219" id="Seg_2862" s="T218">n</ta>
            <ta e="T220" id="Seg_2863" s="T219">v</ta>
            <ta e="T221" id="Seg_2864" s="T220">adj</ta>
            <ta e="T222" id="Seg_2865" s="T221">n</ta>
            <ta e="T223" id="Seg_2866" s="T222">n</ta>
            <ta e="T224" id="Seg_2867" s="T223">adj</ta>
            <ta e="T225" id="Seg_2868" s="T224">n</ta>
            <ta e="T226" id="Seg_2869" s="T225">v</ta>
            <ta e="T227" id="Seg_2870" s="T226">n</ta>
            <ta e="T228" id="Seg_2871" s="T227">n</ta>
            <ta e="T229" id="Seg_2872" s="T228">v</ta>
            <ta e="T230" id="Seg_2873" s="T229">adj</ta>
            <ta e="T231" id="Seg_2874" s="T230">n</ta>
            <ta e="T232" id="Seg_2875" s="T231">v</ta>
            <ta e="T233" id="Seg_2876" s="T232">n</ta>
            <ta e="T234" id="Seg_2877" s="T233">n</ta>
            <ta e="T235" id="Seg_2878" s="T234">adj</ta>
            <ta e="T236" id="Seg_2879" s="T235">n</ta>
            <ta e="T237" id="Seg_2880" s="T236">adj</ta>
            <ta e="T238" id="Seg_2881" s="T237">n</ta>
            <ta e="T239" id="Seg_2882" s="T238">n</ta>
            <ta e="T240" id="Seg_2883" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_2884" s="T240">v</ta>
            <ta e="T242" id="Seg_2885" s="T241">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2886" s="T0">np:L</ta>
            <ta e="T2" id="Seg_2887" s="T1">np:Th</ta>
            <ta e="T5" id="Seg_2888" s="T4">np:L</ta>
            <ta e="T6" id="Seg_2889" s="T5">np:A</ta>
            <ta e="T9" id="Seg_2890" s="T8">np:Th 0.3.h:Poss</ta>
            <ta e="T11" id="Seg_2891" s="T10">np:L</ta>
            <ta e="T12" id="Seg_2892" s="T11">np:Th 0.3.h:Poss</ta>
            <ta e="T14" id="Seg_2893" s="T13">np:L</ta>
            <ta e="T16" id="Seg_2894" s="T15">np:Poss</ta>
            <ta e="T17" id="Seg_2895" s="T16">np:L</ta>
            <ta e="T20" id="Seg_2896" s="T19">np.h:Th</ta>
            <ta e="T23" id="Seg_2897" s="T22">np:Th 0.3.h:Poss</ta>
            <ta e="T25" id="Seg_2898" s="T24">np:Th 0.3.h:Poss</ta>
            <ta e="T27" id="Seg_2899" s="T26">np:G</ta>
            <ta e="T28" id="Seg_2900" s="T27">0.3.h:A</ta>
            <ta e="T31" id="Seg_2901" s="T30">np:Th</ta>
            <ta e="T33" id="Seg_2902" s="T32">np:Th 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_2903" s="T34">np:L</ta>
            <ta e="T36" id="Seg_2904" s="T35">np:Th 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_2905" s="T38">np:L</ta>
            <ta e="T41" id="Seg_2906" s="T40">np:Th</ta>
            <ta e="T44" id="Seg_2907" s="T43">np:Th</ta>
            <ta e="T48" id="Seg_2908" s="T47">np.h:A</ta>
            <ta e="T53" id="Seg_2909" s="T52">np:P 0.1.h:Poss</ta>
            <ta e="T56" id="Seg_2910" s="T54">np:Th</ta>
            <ta e="T57" id="Seg_2911" s="T56">np:G</ta>
            <ta e="T58" id="Seg_2912" s="T57">0.1.h:A</ta>
            <ta e="T60" id="Seg_2913" s="T59">0.3:Th</ta>
            <ta e="T63" id="Seg_2914" s="T62">0.3:Th</ta>
            <ta e="T69" id="Seg_2915" s="T68">0.3.h:A</ta>
            <ta e="T72" id="Seg_2916" s="T71">np.h:Th</ta>
            <ta e="T75" id="Seg_2917" s="T74">np:P 0.1.h:Poss</ta>
            <ta e="T76" id="Seg_2918" s="T75">0.1.h:A</ta>
            <ta e="T79" id="Seg_2919" s="T78">0.1.h:A</ta>
            <ta e="T81" id="Seg_2920" s="T80">np:Th</ta>
            <ta e="T82" id="Seg_2921" s="T81">0.1.h:E</ta>
            <ta e="T85" id="Seg_2922" s="T84">0.1.h:E</ta>
            <ta e="T88" id="Seg_2923" s="T87">np.h:A</ta>
            <ta e="T90" id="Seg_2924" s="T89">np:Th</ta>
            <ta e="T95" id="Seg_2925" s="T94">np:L</ta>
            <ta e="T97" id="Seg_2926" s="T96">0.3.h:A</ta>
            <ta e="T98" id="Seg_2927" s="T97">np:Th</ta>
            <ta e="T99" id="Seg_2928" s="T98">adv:L</ta>
            <ta e="T104" id="Seg_2929" s="T103">np.h:A</ta>
            <ta e="T106" id="Seg_2930" s="T104">pp:Pth</ta>
            <ta e="T111" id="Seg_2931" s="T110">np:Th 0.1.h:Poss</ta>
            <ta e="T113" id="Seg_2932" s="T112">np:A</ta>
            <ta e="T115" id="Seg_2933" s="T114">np:Poss</ta>
            <ta e="T118" id="Seg_2934" s="T116">pp:L</ta>
            <ta e="T119" id="Seg_2935" s="T118">0.1.h:A</ta>
            <ta e="T121" id="Seg_2936" s="T120">np:P 0.1.h:Poss</ta>
            <ta e="T122" id="Seg_2937" s="T121">0.1.h:A</ta>
            <ta e="T124" id="Seg_2938" s="T122">pp:L</ta>
            <ta e="T125" id="Seg_2939" s="T124">0.1.h:A</ta>
            <ta e="T127" id="Seg_2940" s="T126">np:P 0.1.h:Poss</ta>
            <ta e="T128" id="Seg_2941" s="T127">0.1.h:A</ta>
            <ta e="T132" id="Seg_2942" s="T131">np.h:Th</ta>
            <ta e="T135" id="Seg_2943" s="T134">np.h:P</ta>
            <ta e="T136" id="Seg_2944" s="T135">0.2.h:A</ta>
            <ta e="T137" id="Seg_2945" s="T136">0.2.h:E</ta>
            <ta e="T140" id="Seg_2946" s="T139">np:Th 0.3.h:Poss</ta>
            <ta e="T143" id="Seg_2947" s="T142">np:Th 0.3.h:Poss</ta>
            <ta e="T146" id="Seg_2948" s="T145">np:L 0.3.h:Poss</ta>
            <ta e="T150" id="Seg_2949" s="T149">np.h:Th</ta>
            <ta e="T153" id="Seg_2950" s="T152">np.h:A</ta>
            <ta e="T156" id="Seg_2951" s="T155">np.h:A</ta>
            <ta e="T161" id="Seg_2952" s="T160">0.1.h:A</ta>
            <ta e="T162" id="Seg_2953" s="T161">0.1.h:A</ta>
            <ta e="T165" id="Seg_2954" s="T164">0.1.h:A</ta>
            <ta e="T167" id="Seg_2955" s="T166">np:Th</ta>
            <ta e="T170" id="Seg_2956" s="T168">np:L</ta>
            <ta e="T171" id="Seg_2957" s="T170">np:Th 0.3.h:Poss</ta>
            <ta e="T172" id="Seg_2958" s="T171">np:L</ta>
            <ta e="T173" id="Seg_2959" s="T172">np:Th 0.3.h:Poss</ta>
            <ta e="T174" id="Seg_2960" s="T173">np:L</ta>
            <ta e="T176" id="Seg_2961" s="T175">0.1.h:A</ta>
            <ta e="T178" id="Seg_2962" s="T177">0.1.h:A</ta>
            <ta e="T182" id="Seg_2963" s="T181">np.h:A</ta>
            <ta e="T185" id="Seg_2964" s="T184">np:E 0.3.h:Poss</ta>
            <ta e="T188" id="Seg_2965" s="T187">np:A 0.3.h:Poss</ta>
            <ta e="T192" id="Seg_2966" s="T191">np.h:A</ta>
            <ta e="T198" id="Seg_2967" s="T197">np:Ins 0.3.h:Poss</ta>
            <ta e="T199" id="Seg_2968" s="T198">0.3:P 0.3.h:A</ta>
            <ta e="T200" id="Seg_2969" s="T199">np:Ins 0.3.h:Poss</ta>
            <ta e="T201" id="Seg_2970" s="T200">0.3:P 0.3.h:A</ta>
            <ta e="T202" id="Seg_2971" s="T201">np:P 0.3.h:Poss</ta>
            <ta e="T203" id="Seg_2972" s="T202">0.3.h:A</ta>
            <ta e="T204" id="Seg_2973" s="T203">adv:G</ta>
            <ta e="T205" id="Seg_2974" s="T204">0.3.h:A 0.3:P</ta>
            <ta e="T207" id="Seg_2975" s="T206">0.2.h:A</ta>
            <ta e="T208" id="Seg_2976" s="T207">pro.h:E</ta>
            <ta e="T209" id="Seg_2977" s="T208">0.2.h:A</ta>
            <ta e="T210" id="Seg_2978" s="T209">pro.h:P</ta>
            <ta e="T212" id="Seg_2979" s="T211">pro.h:Poss</ta>
            <ta e="T214" id="Seg_2980" s="T213">np:Th</ta>
            <ta e="T217" id="Seg_2981" s="T215">pp:L</ta>
            <ta e="T219" id="Seg_2982" s="T218">np:Th</ta>
            <ta e="T223" id="Seg_2983" s="T221">pp:L</ta>
            <ta e="T225" id="Seg_2984" s="T224">np:Th</ta>
            <ta e="T228" id="Seg_2985" s="T227">np:Th</ta>
            <ta e="T231" id="Seg_2986" s="T230">np:Th</ta>
            <ta e="T236" id="Seg_2987" s="T235">np:Th</ta>
            <ta e="T238" id="Seg_2988" s="T237">np:L</ta>
            <ta e="T239" id="Seg_2989" s="T238">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2990" s="T1">np:S</ta>
            <ta e="T3" id="Seg_2991" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_2992" s="T5">np:S</ta>
            <ta e="T7" id="Seg_2993" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_2994" s="T8">np:S</ta>
            <ta e="T12" id="Seg_2995" s="T11">np:S</ta>
            <ta e="T20" id="Seg_2996" s="T19">np.h:S</ta>
            <ta e="T21" id="Seg_2997" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_2998" s="T22">np:S</ta>
            <ta e="T24" id="Seg_2999" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_3000" s="T24">np:S</ta>
            <ta e="T26" id="Seg_3001" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_3002" s="T27">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_3003" s="T30">np:S</ta>
            <ta e="T32" id="Seg_3004" s="T31">adj:pred</ta>
            <ta e="T33" id="Seg_3005" s="T32">np:S</ta>
            <ta e="T36" id="Seg_3006" s="T35">np:S</ta>
            <ta e="T37" id="Seg_3007" s="T36">adj:pred</ta>
            <ta e="T41" id="Seg_3008" s="T40">np:S</ta>
            <ta e="T42" id="Seg_3009" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_3010" s="T43">np:S</ta>
            <ta e="T46" id="Seg_3011" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_3012" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_3013" s="T48">s:adv</ta>
            <ta e="T51" id="Seg_3014" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_3015" s="T52">np:O</ta>
            <ta e="T54" id="Seg_3016" s="T53">v:pred 0.1.h:S</ta>
            <ta e="T56" id="Seg_3017" s="T55">np:O</ta>
            <ta e="T58" id="Seg_3018" s="T57">v:pred 0.1.h:S</ta>
            <ta e="T60" id="Seg_3019" s="T59">s:temp</ta>
            <ta e="T62" id="Seg_3020" s="T61">adj:pred 0.3:S</ta>
            <ta e="T63" id="Seg_3021" s="T62">s:temp</ta>
            <ta e="T65" id="Seg_3022" s="T64">adj:pred 0.3:S</ta>
            <ta e="T69" id="Seg_3023" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_3024" s="T71">np.h:S</ta>
            <ta e="T73" id="Seg_3025" s="T72">v:pred</ta>
            <ta e="T75" id="Seg_3026" s="T74">np:O</ta>
            <ta e="T76" id="Seg_3027" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T77" id="Seg_3028" s="T76">s:purp</ta>
            <ta e="T78" id="Seg_3029" s="T77">ptcl.neg</ta>
            <ta e="T79" id="Seg_3030" s="T78">v:pred 0.1.h:S</ta>
            <ta e="T81" id="Seg_3031" s="T80">np:O</ta>
            <ta e="T82" id="Seg_3032" s="T81">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_3033" s="T82">s:purp</ta>
            <ta e="T84" id="Seg_3034" s="T83">ptcl.neg</ta>
            <ta e="T85" id="Seg_3035" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T88" id="Seg_3036" s="T87">np.h:S</ta>
            <ta e="T91" id="Seg_3037" s="T88">s:adv</ta>
            <ta e="T92" id="Seg_3038" s="T91">v:pred</ta>
            <ta e="T96" id="Seg_3039" s="T95">np:O</ta>
            <ta e="T97" id="Seg_3040" s="T96">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_3041" s="T97">np:S</ta>
            <ta e="T100" id="Seg_3042" s="T99">v:pred</ta>
            <ta e="T104" id="Seg_3043" s="T103">np.h:S</ta>
            <ta e="T107" id="Seg_3044" s="T106">v:pred</ta>
            <ta e="T109" id="Seg_3045" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_3046" s="T110">np:S</ta>
            <ta e="T114" id="Seg_3047" s="T111">s:purp</ta>
            <ta e="T119" id="Seg_3048" s="T118">v:pred 0.1.h:S</ta>
            <ta e="T121" id="Seg_3049" s="T120">np:O</ta>
            <ta e="T122" id="Seg_3050" s="T121">v:pred 0.1.h:S</ta>
            <ta e="T125" id="Seg_3051" s="T124">v:pred 0.1.h:S</ta>
            <ta e="T127" id="Seg_3052" s="T126">np:O</ta>
            <ta e="T128" id="Seg_3053" s="T127">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_3054" s="T131">np.h:S</ta>
            <ta e="T133" id="Seg_3055" s="T132">v:pred</ta>
            <ta e="T136" id="Seg_3056" s="T133">s:cond</ta>
            <ta e="T137" id="Seg_3057" s="T136">v:pred 0.2.h:S</ta>
            <ta e="T140" id="Seg_3058" s="T139">np:S</ta>
            <ta e="T141" id="Seg_3059" s="T140">adj:pred</ta>
            <ta e="T143" id="Seg_3060" s="T142">np:S</ta>
            <ta e="T144" id="Seg_3061" s="T143">adj:pred</ta>
            <ta e="T147" id="Seg_3062" s="T146">np:S</ta>
            <ta e="T150" id="Seg_3063" s="T149">np.h:S</ta>
            <ta e="T151" id="Seg_3064" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_3065" s="T152">np.h:S</ta>
            <ta e="T154" id="Seg_3066" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_3067" s="T155">np.h:S</ta>
            <ta e="T158" id="Seg_3068" s="T157">v:pred</ta>
            <ta e="T161" id="Seg_3069" s="T160">v:pred 0.1.h:S</ta>
            <ta e="T162" id="Seg_3070" s="T161">v:pred 0.1.h:S</ta>
            <ta e="T163" id="Seg_3071" s="T162">s:purp</ta>
            <ta e="T164" id="Seg_3072" s="T163">ptcl.neg</ta>
            <ta e="T165" id="Seg_3073" s="T164">v:pred 0.1.h:S</ta>
            <ta e="T167" id="Seg_3074" s="T166">np:S</ta>
            <ta e="T168" id="Seg_3075" s="T167">v:pred</ta>
            <ta e="T171" id="Seg_3076" s="T170">np:S</ta>
            <ta e="T173" id="Seg_3077" s="T172">np:S</ta>
            <ta e="T176" id="Seg_3078" s="T175">v:pred 0.1.h:S</ta>
            <ta e="T178" id="Seg_3079" s="T177">v:pred 0.1.h:S</ta>
            <ta e="T181" id="Seg_3080" s="T179">s:adv</ta>
            <ta e="T182" id="Seg_3081" s="T181">np.h:S</ta>
            <ta e="T183" id="Seg_3082" s="T182">v:pred</ta>
            <ta e="T185" id="Seg_3083" s="T184">np:S</ta>
            <ta e="T186" id="Seg_3084" s="T185">v:pred</ta>
            <ta e="T188" id="Seg_3085" s="T187">np:S</ta>
            <ta e="T189" id="Seg_3086" s="T188">v:pred</ta>
            <ta e="T192" id="Seg_3087" s="T191">np.h:S</ta>
            <ta e="T196" id="Seg_3088" s="T195">v:pred</ta>
            <ta e="T199" id="Seg_3089" s="T198">v:pred 0.3.h:S 0.3:O</ta>
            <ta e="T201" id="Seg_3090" s="T200">v:pred 0.3.h:S 0.3:O</ta>
            <ta e="T202" id="Seg_3091" s="T201">np:O</ta>
            <ta e="T203" id="Seg_3092" s="T202">v:pred 0.3.h:S</ta>
            <ta e="T205" id="Seg_3093" s="T204">v:pred 0.3.h:S 0.3:O</ta>
            <ta e="T207" id="Seg_3094" s="T206">v:pred 0.2.h:S</ta>
            <ta e="T208" id="Seg_3095" s="T207">pro.h:O</ta>
            <ta e="T209" id="Seg_3096" s="T208">v:pred 0.2.h:S</ta>
            <ta e="T210" id="Seg_3097" s="T209">pro.h:O</ta>
            <ta e="T211" id="Seg_3098" s="T210">v:pred</ta>
            <ta e="T214" id="Seg_3099" s="T213">np:S</ta>
            <ta e="T219" id="Seg_3100" s="T218">np:S</ta>
            <ta e="T220" id="Seg_3101" s="T219">v:pred</ta>
            <ta e="T225" id="Seg_3102" s="T224">np:S</ta>
            <ta e="T226" id="Seg_3103" s="T225">v:pred</ta>
            <ta e="T229" id="Seg_3104" s="T227">s:adv</ta>
            <ta e="T231" id="Seg_3105" s="T230">np:S</ta>
            <ta e="T232" id="Seg_3106" s="T231">v:pred</ta>
            <ta e="T237" id="Seg_3107" s="T234">s:rel</ta>
            <ta e="T239" id="Seg_3108" s="T238">np:S</ta>
            <ta e="T240" id="Seg_3109" s="T239">ptcl.neg</ta>
            <ta e="T241" id="Seg_3110" s="T240">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_3111" s="T0">new </ta>
            <ta e="T2" id="Seg_3112" s="T1">new </ta>
            <ta e="T4" id="Seg_3113" s="T3">accs-inf </ta>
            <ta e="T5" id="Seg_3114" s="T4">new </ta>
            <ta e="T6" id="Seg_3115" s="T5">new </ta>
            <ta e="T8" id="Seg_3116" s="T7">accs-inf </ta>
            <ta e="T9" id="Seg_3117" s="T8">new </ta>
            <ta e="T11" id="Seg_3118" s="T10">new </ta>
            <ta e="T12" id="Seg_3119" s="T11">new </ta>
            <ta e="T14" id="Seg_3120" s="T13">new </ta>
            <ta e="T15" id="Seg_3121" s="T14">accs-inf </ta>
            <ta e="T17" id="Seg_3122" s="T16">new </ta>
            <ta e="T20" id="Seg_3123" s="T19">new </ta>
            <ta e="T22" id="Seg_3124" s="T21">accs-inf </ta>
            <ta e="T23" id="Seg_3125" s="T22">new</ta>
            <ta e="T25" id="Seg_3126" s="T24">new </ta>
            <ta e="T27" id="Seg_3127" s="T26">new </ta>
            <ta e="T30" id="Seg_3128" s="T29">accs-inf </ta>
            <ta e="T31" id="Seg_3129" s="T30">new </ta>
            <ta e="T33" id="Seg_3130" s="T32">accs-inf </ta>
            <ta e="T35" id="Seg_3131" s="T34">new </ta>
            <ta e="T36" id="Seg_3132" s="T35">accs-inf </ta>
            <ta e="T38" id="Seg_3133" s="T37">accs-inf </ta>
            <ta e="T39" id="Seg_3134" s="T38">new </ta>
            <ta e="T41" id="Seg_3135" s="T40">new </ta>
            <ta e="T43" id="Seg_3136" s="T42">accs-inf </ta>
            <ta e="T44" id="Seg_3137" s="T43">new </ta>
            <ta e="T48" id="Seg_3138" s="T47">new </ta>
            <ta e="T52" id="Seg_3139" s="T51">accs-inf</ta>
            <ta e="T53" id="Seg_3140" s="T52">new </ta>
            <ta e="T54" id="Seg_3141" s="T53">0.new</ta>
            <ta e="T56" id="Seg_3142" s="T55">new </ta>
            <ta e="T57" id="Seg_3143" s="T56">new </ta>
            <ta e="T58" id="Seg_3144" s="T57">0.giv-active</ta>
            <ta e="T59" id="Seg_3145" s="T58">accs-inf </ta>
            <ta e="T60" id="Seg_3146" s="T59">0.new</ta>
            <ta e="T61" id="Seg_3147" s="T60">new</ta>
            <ta e="T63" id="Seg_3148" s="T62">0.giv-active</ta>
            <ta e="T64" id="Seg_3149" s="T63">new</ta>
            <ta e="T66" id="Seg_3150" s="T65">accs-inf </ta>
            <ta e="T67" id="Seg_3151" s="T66">new </ta>
            <ta e="T68" id="Seg_3152" s="T67">new </ta>
            <ta e="T69" id="Seg_3153" s="T68">0.new</ta>
            <ta e="T70" id="Seg_3154" s="T69">accs-inf </ta>
            <ta e="T72" id="Seg_3155" s="T71">new </ta>
            <ta e="T74" id="Seg_3156" s="T73">accs-inf </ta>
            <ta e="T75" id="Seg_3157" s="T74">new </ta>
            <ta e="T76" id="Seg_3158" s="T75">0.new</ta>
            <ta e="T79" id="Seg_3159" s="T78">0.giv-active</ta>
            <ta e="T80" id="Seg_3160" s="T79">accs-inf </ta>
            <ta e="T81" id="Seg_3161" s="T80">new </ta>
            <ta e="T82" id="Seg_3162" s="T81">0.new</ta>
            <ta e="T85" id="Seg_3163" s="T84">0.giv-active</ta>
            <ta e="T86" id="Seg_3164" s="T85">accs-inf </ta>
            <ta e="T88" id="Seg_3165" s="T87">new </ta>
            <ta e="T90" id="Seg_3166" s="T89">new </ta>
            <ta e="T93" id="Seg_3167" s="T92">accs-inf </ta>
            <ta e="T95" id="Seg_3168" s="T94">new </ta>
            <ta e="T96" id="Seg_3169" s="T95">new </ta>
            <ta e="T97" id="Seg_3170" s="T96">0.new</ta>
            <ta e="T98" id="Seg_3171" s="T97">new </ta>
            <ta e="T102" id="Seg_3172" s="T101">accs-inf </ta>
            <ta e="T104" id="Seg_3173" s="T103">new </ta>
            <ta e="T105" id="Seg_3174" s="T104">new </ta>
            <ta e="T108" id="Seg_3175" s="T107">accs-inf </ta>
            <ta e="T111" id="Seg_3176" s="T110">new </ta>
            <ta e="T113" id="Seg_3177" s="T112">new </ta>
            <ta e="T116" id="Seg_3178" s="T115">accs-inf </ta>
            <ta e="T117" id="Seg_3179" s="T116">accs-gen </ta>
            <ta e="T119" id="Seg_3180" s="T118">0.new</ta>
            <ta e="T121" id="Seg_3181" s="T120">new </ta>
            <ta e="T122" id="Seg_3182" s="T121">0.giv-active</ta>
            <ta e="T123" id="Seg_3183" s="T122">accs-gen </ta>
            <ta e="T125" id="Seg_3184" s="T124">0.giv-active</ta>
            <ta e="T127" id="Seg_3185" s="T126">new </ta>
            <ta e="T128" id="Seg_3186" s="T127">0.giv-active</ta>
            <ta e="T129" id="Seg_3187" s="T128">accs-inf </ta>
            <ta e="T130" id="Seg_3188" s="T129">accs-inf </ta>
            <ta e="T132" id="Seg_3189" s="T131">new </ta>
            <ta e="T135" id="Seg_3190" s="T134">giv-active </ta>
            <ta e="T136" id="Seg_3191" s="T135">0.new</ta>
            <ta e="T138" id="Seg_3192" s="T137">accs-inf </ta>
            <ta e="T140" id="Seg_3193" s="T139">new </ta>
            <ta e="T143" id="Seg_3194" s="T142">new </ta>
            <ta e="T146" id="Seg_3195" s="T145">new </ta>
            <ta e="T147" id="Seg_3196" s="T146">new </ta>
            <ta e="T148" id="Seg_3197" s="T147">accs-inf </ta>
            <ta e="T150" id="Seg_3198" s="T149">new </ta>
            <ta e="T153" id="Seg_3199" s="T152">new </ta>
            <ta e="T156" id="Seg_3200" s="T155">new </ta>
            <ta e="T159" id="Seg_3201" s="T158">accs-inf </ta>
            <ta e="T160" id="Seg_3202" s="T159">accs-inf </ta>
            <ta e="T161" id="Seg_3203" s="T160">0.new</ta>
            <ta e="T162" id="Seg_3204" s="T161">0.giv-active</ta>
            <ta e="T165" id="Seg_3205" s="T164">0.giv-active0.giv-active</ta>
            <ta e="T166" id="Seg_3206" s="T165">accs-inf </ta>
            <ta e="T167" id="Seg_3207" s="T166">new </ta>
            <ta e="T170" id="Seg_3208" s="T169">new </ta>
            <ta e="T171" id="Seg_3209" s="T170">accs-inf </ta>
            <ta e="T172" id="Seg_3210" s="T171">new </ta>
            <ta e="T173" id="Seg_3211" s="T172">accs-inf </ta>
            <ta e="T174" id="Seg_3212" s="T173">accs-gen </ta>
            <ta e="T175" id="Seg_3213" s="T174">accs-inf </ta>
            <ta e="T176" id="Seg_3214" s="T175">0.new</ta>
            <ta e="T178" id="Seg_3215" s="T177">0.giv-active</ta>
            <ta e="T179" id="Seg_3216" s="T178">accs-inf </ta>
            <ta e="T180" id="Seg_3217" s="T179">accs-inf</ta>
            <ta e="T182" id="Seg_3218" s="T181">new </ta>
            <ta e="T185" id="Seg_3219" s="T184">new </ta>
            <ta e="T188" id="Seg_3220" s="T187">new </ta>
            <ta e="T190" id="Seg_3221" s="T189">accs-inf </ta>
            <ta e="T192" id="Seg_3222" s="T191">new </ta>
            <ta e="T197" id="Seg_3223" s="T196">accs-inf </ta>
            <ta e="T198" id="Seg_3224" s="T197">new </ta>
            <ta e="T199" id="Seg_3225" s="T198">0.new</ta>
            <ta e="T200" id="Seg_3226" s="T199">new </ta>
            <ta e="T201" id="Seg_3227" s="T200">0.giv-active</ta>
            <ta e="T202" id="Seg_3228" s="T201">new </ta>
            <ta e="T203" id="Seg_3229" s="T202">0.giv-active</ta>
            <ta e="T205" id="Seg_3230" s="T204">0.giv-active</ta>
            <ta e="T207" id="Seg_3231" s="T206">0.new</ta>
            <ta e="T208" id="Seg_3232" s="T207">new </ta>
            <ta e="T209" id="Seg_3233" s="T208">0.giv-active</ta>
            <ta e="T210" id="Seg_3234" s="T209">giv-active </ta>
            <ta e="T212" id="Seg_3235" s="T211">giv-active </ta>
            <ta e="T214" id="Seg_3236" s="T213">new </ta>
            <ta e="T217" id="Seg_3237" s="T216">accs-inf </ta>
            <ta e="T219" id="Seg_3238" s="T218">new </ta>
            <ta e="T223" id="Seg_3239" s="T222">accs-inf </ta>
            <ta e="T225" id="Seg_3240" s="T224">new </ta>
            <ta e="T227" id="Seg_3241" s="T226">accs-inf </ta>
            <ta e="T228" id="Seg_3242" s="T227">new </ta>
            <ta e="T231" id="Seg_3243" s="T230">new </ta>
            <ta e="T233" id="Seg_3244" s="T232">new </ta>
            <ta e="T234" id="Seg_3245" s="T233">new </ta>
            <ta e="T236" id="Seg_3246" s="T235">new </ta>
            <ta e="T238" id="Seg_3247" s="T237">accs-sit </ta>
            <ta e="T239" id="Seg_3248" s="T238">new </ta>
            <ta e="T242" id="Seg_3249" s="T241">accs-inf </ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T61" id="Seg_3250" s="T60">RUS:cult=TURK:mod(PTCL)</ta>
            <ta e="T64" id="Seg_3251" s="T63">TURK:mod(PTCL)</ta>
            <ta e="T93" id="Seg_3252" s="T92">RUS:cult</ta>
            <ta e="T108" id="Seg_3253" s="T107">TURK:cult</ta>
            <ta e="T177" id="Seg_3254" s="T176">RUS:gram</ta>
            <ta e="T180" id="Seg_3255" s="T179">RUS:cult</ta>
            <ta e="T187" id="Seg_3256" s="T186">RUS:gram</ta>
            <ta e="T190" id="Seg_3257" s="T189">RUS:cult</ta>
            <ta e="T230" id="Seg_3258" s="T229">TAT:core</ta>
            <ta e="T233" id="Seg_3259" s="T232">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_3260" s="T0">На горе котёл кипит. — Муравейник.</ta>
            <ta e="T8" id="Seg_3261" s="T4">На горе птичка повесилась. — Пуговица. Сосок.</ta>
            <ta e="T15" id="Seg_3262" s="T8">Сама на том свете, а сережка её на этом. — Лилия [=сарана].</ta>
            <ta e="T22" id="Seg_3263" s="T15">У огня две беременные лежат. — Икры.</ta>
            <ta e="T30" id="Seg_3264" s="T22">Без рук без ног, по горе поднимается. — Пожар.</ta>
            <ta e="T38" id="Seg_3265" s="T30">Много птиц, головы вместе, хвосты врозь. — Чум.</ta>
            <ta e="T43" id="Seg_3266" s="T38">В берлоге мокрый теленок лежит. — Язык.</ta>
            <ta e="T52" id="Seg_3267" s="T43">Утёс упал, голый парень показался. — Костный мозг.</ta>
            <ta e="T59" id="Seg_3268" s="T52">Нож я точил, точило в воду уронил. — Выдра.</ta>
            <ta e="T66" id="Seg_3269" s="T59">Когда лежит, ниже кошки, а если поднимется, выше лошади будет. — Седло.</ta>
            <ta e="T70" id="Seg_3270" s="T66">Без ног, без рук и богу молится. — Топор.</ta>
            <ta e="T74" id="Seg_3271" s="T70">Конопатая девочка сидит. — Напёрсток.</ta>
            <ta e="T80" id="Seg_3272" s="T74">Аркан свой собираю, никак не соберу. — Дорога.</ta>
            <ta e="T86" id="Seg_3273" s="T80">Нашёл я хлыст, а поднять не могу. — Змея.</ta>
            <ta e="T93" id="Seg_3274" s="T86">Четверо одну шапку носят. — Стол.</ta>
            <ta e="T102" id="Seg_3275" s="T93">На том свете дерево рубят, щепки сюда падают. — Письмо.</ta>
            <ta e="T108" id="Seg_3276" s="T102">Девочка с косичкой по деревне гуляет. — Сорока.</ta>
            <ta e="T116" id="Seg_3277" s="T108">Есть у меня такая вещь, куда дерево может войти. — Ножны для ножа.</ta>
            <ta e="T130" id="Seg_3278" s="T116">Под солнцем встану — в золотой рог дуну, под луной встану — в серебряный рог дуну. — Гусь и журавль.</ta>
            <ta e="T138" id="Seg_3279" s="T130">Сидит оборванец, съешь его — заплачешь. — Лук.</ta>
            <ta e="T148" id="Seg_3280" s="T138">Два конца острых, два круглых, посередине пуговица. — Ножницы.</ta>
            <ta e="T160" id="Seg_3281" s="T148">Двое лежат, двое стоят, пятый закрывает. — Порог и дверь.</ta>
            <ta e="T166" id="Seg_3282" s="T160">Ем, ем, а облегчиться не могу. — Мешок.</ta>
            <ta e="T175" id="Seg_3283" s="T166">Сидит петух на заборе, его хвост на земле, голос у Бога. — Колокол [церковный].</ta>
            <ta e="T181" id="Seg_3284" s="T175">Я намочу и потрясу. — Веник для печки.</ta>
            <ta e="T190" id="Seg_3285" s="T181">Парень стоит, в середине горит, внизу писает. — Самовар.</ta>
            <ta e="T197" id="Seg_3286" s="T190">Двое рядом наперегонки бегут. — Лыжи.</ta>
            <ta e="T206" id="Seg_3287" s="T197">Ногой нажмёт, животом потрёт, откроет рот, туда возьмёт. — Ткачество.</ta>
            <ta e="T227" id="Seg_3288" s="T206">Сорви меня [с дерева], расколи, внутри сердце мохнатое, в мохнатом сердце красное сердце, в красном сердце сладкое сердце. — Орех кедровый.</ta>
            <ta e="T234" id="Seg_3289" s="T227">Вокруг дерева трава зеленая росла. — Кольцо, пенис. [?]</ta>
            <ta e="T242" id="Seg_3290" s="T234">Там, где красная коза лежала, трава не растёт. — Огонь.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3291" s="T0">On a mountain a cauldron is boiling. — An ant[hill].</ta>
            <ta e="T8" id="Seg_3292" s="T4">On a mountain a bird has hanged itself. — A button, a nipple.</ta>
            <ta e="T15" id="Seg_3293" s="T8">Her body is in the other world, her earring is in this world. — The lily.</ta>
            <ta e="T22" id="Seg_3294" s="T15">By the fire two pregnant women are lying. — The calves.</ta>
            <ta e="T30" id="Seg_3295" s="T22">It has no feet, no hands, [but] climbs up a mountain. — Wildfire.</ta>
            <ta e="T38" id="Seg_3296" s="T30">Many birds, their heads together, their behinds apart. — The tent.</ta>
            <ta e="T43" id="Seg_3297" s="T38">In a den a wet calf is lying. — The tongue.</ta>
            <ta e="T52" id="Seg_3298" s="T43">A cliff slided down, and a naked boy appeared to fall down. — Bone marrow.</ta>
            <ta e="T59" id="Seg_3299" s="T52">I sharpened my knife, dropped the sharpening stone into the water. — The otter.</ta>
            <ta e="T66" id="Seg_3300" s="T59">When it is lying it is smaller than a cat, when it got up it is taller than a horse. — The saddle.</ta>
            <ta e="T70" id="Seg_3301" s="T66">Without feet and hands he prays. — The axe.</ta>
            <ta e="T74" id="Seg_3302" s="T70">A pockmarked girl sits. — A thimble.</ta>
            <ta e="T80" id="Seg_3303" s="T74">I am collecting my lasso, cannot collect it. — The road.</ta>
            <ta e="T86" id="Seg_3304" s="T80">I found a whip, cannot pick it up. — A snake.</ta>
            <ta e="T93" id="Seg_3305" s="T86">Four men are wearing one cap. — A table.</ta>
            <ta e="T102" id="Seg_3306" s="T93">They are cutting wood in the other world, a splinter is falling here. — A letter.</ta>
            <ta e="T108" id="Seg_3307" s="T102">A girl with a braid walks down the village. — A magpie.</ta>
            <ta e="T116" id="Seg_3308" s="T108">I have such a thing in which wood may go. — My knife's sheath.</ta>
            <ta e="T130" id="Seg_3309" s="T116">I will stand under the sun and blow my golden horn, I will stand under the moon and blow my silver horn. — Goose and crane.</ta>
            <ta e="T138" id="Seg_3310" s="T130">A raggy man is sitting, if you eat this man, you will cry. — An onion.</ta>
            <ta e="T148" id="Seg_3311" s="T138">Two of its ends are pointed, two of its ends are round, right in the center is a button. — Scissors.</ta>
            <ta e="T160" id="Seg_3312" s="T148">Two men are lying, two men are standing, the fifth man goes shutting. — Threshold and door.</ta>
            <ta e="T166" id="Seg_3313" s="T160">I eat and eat, but I cannot shit. — A sack.</ta>
            <ta e="T175" id="Seg_3314" s="T166">A rooster sits atop a fence, its tail is on the ground, his voice is with God. — The [church]bell.</ta>
            <ta e="T181" id="Seg_3315" s="T175">I will wet and shake. — A broom sweeping the stove.</ta>
            <ta e="T190" id="Seg_3316" s="T181">A boy is standing, his midpart is burning, but his (lower?) edge is pissing. — A samovar.</ta>
            <ta e="T197" id="Seg_3317" s="T190">Two men are racing side by side. — The ski[s].</ta>
            <ta e="T206" id="Seg_3318" s="T197">One presses with its foot, rubs with its belly, opens its mouth, and puts it in there. — Weaving.</ta>
            <ta e="T227" id="Seg_3319" s="T206">Knock me [down from the tree], crash me [open], I have a hairy center, inside the hairy center is a red center, inside the red center is sweet center. — The pine nut.</ta>
            <ta e="T234" id="Seg_3320" s="T227">Green grass grew around a tree. — The ring, the penis. [?]</ta>
            <ta e="T242" id="Seg_3321" s="T234">Where the red goat had been lying grass does not grow. — The fire.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3322" s="T0">Auf einem Berg kocht ein Kessel. – Der Ameisen[haufen].</ta>
            <ta e="T8" id="Seg_3323" s="T4">Auf einem Berg erhängte sich ein Vogel. – Der Knopf, die Brustwarze.</ta>
            <ta e="T15" id="Seg_3324" s="T8">Ihr Körper ist in jener Welt, ihr Ohrring ist in dieser Welt. – Die Lilie.</ta>
            <ta e="T22" id="Seg_3325" s="T15">Am Feuer liegen zwei schwangere Frauen. – Die Waden.</ta>
            <ta e="T30" id="Seg_3326" s="T22">Es hat weder Fuß noch Hand und klettert [doch] den Berg hinauf. – Das Lauffeuer.</ta>
            <ta e="T38" id="Seg_3327" s="T30">Viele Vögel, ihre Köpfe [sind] zusammen, ihre Schwänze getrennt. – Das Zelt.</ta>
            <ta e="T43" id="Seg_3328" s="T38">In einer Höhle liegt ein feuchtes Kalb. – Die Zunge.</ta>
            <ta e="T52" id="Seg_3329" s="T43">Eine Felswand rutschte herab und ein nackter Junge stürzte herunter. – Das Knochenmark.</ta>
            <ta e="T59" id="Seg_3330" s="T52">Ich schliff das Messer und ließ den Schleifstein ins Wasser fallen. – Der Fischotter</ta>
            <ta e="T66" id="Seg_3331" s="T59">Wenn er liegt, ist er niedriger als eine Katze, wenn er aufgestanden ist, höher als ein Pferd. – Der Sattel.</ta>
            <ta e="T70" id="Seg_3332" s="T66">Ohne Hände, ohne Füße, betet sie. — Die Axt.</ta>
            <ta e="T74" id="Seg_3333" s="T70">[Es] sitzt ein pockennarbiges Mädchen. – Der Fingerhut.</ta>
            <ta e="T80" id="Seg_3334" s="T74">Ich wickle mein Seil auf, kann es nicht aufwickeln. – Die Straße.</ta>
            <ta e="T86" id="Seg_3335" s="T80">Ich fand eine Peitsche, [doch] kann sie nicht aufheben. – Die Schlange.</ta>
            <ta e="T93" id="Seg_3336" s="T86">Vier Männer tragen eine [einzige] Mütze. – Der Tisch.</ta>
            <ta e="T102" id="Seg_3337" s="T93">In jener Welt hackt man Holz, ein Splitter fällt hierher. – Der Brief.</ta>
            <ta e="T108" id="Seg_3338" s="T102">Ein Mädchen mit Zopf geht vorüber. – Die Elster.</ta>
            <ta e="T116" id="Seg_3339" s="T108">Ich habe einen Gegenstand, in den das Holz [hinein]geht. – Die Messerscheide.</ta>
            <ta e="T130" id="Seg_3340" s="T116">Ich werde unter der Sonne stehen und ein goldenes Horn blasen; ich werde unter dem Mond stehen und ein silbernes Horn blasen. – Die Wildgans, der Kranich.</ta>
            <ta e="T138" id="Seg_3341" s="T130">[Es] lebt ein zerlumpter Mann. Isst du diesen Mann, wirst du weinen. – Die Zwiebel.</ta>
            <ta e="T148" id="Seg_3342" s="T138">Zwei ihrer Enden sind scharf, zwei ihrer Enden sind rund, mittendrin ein Knopf. – Die Schere.</ta>
            <ta e="T160" id="Seg_3343" s="T148">Zwei Männer liegen, zwei Männer stehen, der fünfte Mann macht zu. — Die Türpfosten und die Tür.</ta>
            <ta e="T166" id="Seg_3344" s="T160">Ich esse und esse, [doch] scheißen kann ich nicht. — Der Sack.</ta>
            <ta e="T175" id="Seg_3345" s="T166">Ein Hahn sitzt auf dem Zaun, sein Schwanz auf der Erde, seine Stimme bei Gott. – Die [Kirchen]glocke.</ta>
            <ta e="T181" id="Seg_3346" s="T175">Ich werde nass machen nass und mich schütteln. – Der Besen, der den Ofen kehrt.</ta>
            <ta e="T190" id="Seg_3347" s="T181">Ein Junge steht herum, sein mittlerer Teil brennt, doch sein [unteres] Ende pisst. – Der Samowar.</ta>
            <ta e="T197" id="Seg_3348" s="T190">Zwei Männer laufen nebeneinander um die Wette. – Die Skier.</ta>
            <ta e="T206" id="Seg_3349" s="T197">Man drückt es mit dem Fuß, man reibt mit dem Bauch, es öffnet den Mund, dort steckt man etwas hinein. – Weben.</ta>
            <ta e="T227" id="Seg_3350" s="T206">Schlage mich [vom Baum herunter], knacke mich, ich habe ein haariges Herz, darin ist ein rotes Herz, in dem roten Herz ist ein süßes Herz. – Der Pinienkern.</ta>
            <ta e="T234" id="Seg_3351" s="T227">Grünes Gras wuchs um den Baum herum. – Der Ring, der Penis. [?]</ta>
            <ta e="T242" id="Seg_3352" s="T234">Wo die rote Ziege lag, wächst kein Gras. – Das Feuer.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T4" id="Seg_3353" s="T0">Auf dem Berge kocht ein Kessel. – Der Ameisenhaufen.</ta>
            <ta e="T8" id="Seg_3354" s="T4">Auf dem Berge hing ein Vogel. – Ein Knopf. Die Brustwarze.</ta>
            <ta e="T15" id="Seg_3355" s="T8">Sie selbst in jener anderen Welt, ihr Ohrring in dieser Welt. – Die Lilienzwiebel (сарана).</ta>
            <ta e="T22" id="Seg_3356" s="T15">Am Feuer liegen zwei schwangere Frauen. – Die Waden.</ta>
            <ta e="T30" id="Seg_3357" s="T22">⌈Seinen⌉ Fuss gibt es nicht, ⌈seine Hand⌉ gibt es nicht, kriecht auf den Berg. – Das Lauffeuer.</ta>
            <ta e="T38" id="Seg_3358" s="T30">Viele Vögel, ihre Köpfe [sind] zusammen, ihre Schwänze getrennt. – Das Zelt.</ta>
            <ta e="T43" id="Seg_3359" s="T38">In der Grube liegt ein nasses Kalb. – Die Zunge.</ta>
            <ta e="T52" id="Seg_3360" s="T43">Eine steile Felsenwand stürzend fiel herab, ein nackter Knabe stehend kommend fiel. – Das Knochenmark.</ta>
            <ta e="T59" id="Seg_3361" s="T52">Das Messer schliff ich, den Schleifstein liess ich ins Wasser fallen. – Der Fischotter (wenn er ins Wasser geht).</ta>
            <ta e="T66" id="Seg_3362" s="T59">Wenn es liegt, niedriger als die Katze, wenn es aufsteht, höher als das Pferd. – Das Krummholz.</ta>
            <ta e="T70" id="Seg_3363" s="T66">Ohne Füsse, ohne Hände, betet zu Gott. - Die Axt.</ta>
            <ta e="T74" id="Seg_3364" s="T70">Ein blatternarbiges Mädchen sitzt. – Der Fingerhut.</ta>
            <ta e="T80" id="Seg_3365" s="T74">Den Faden sammle ich auf, sammle ich auf, kann [ihn] nicht aufsammeln. – Der Weg.</ta>
            <ta e="T86" id="Seg_3366" s="T80">Eine Peitsche fand ich, aufheben kann ich sie nicht. – Die Schlange.</ta>
            <ta e="T93" id="Seg_3367" s="T86">Vier Männer e i n e Mütze sich aufsetzend stehen. – Der Tisch.</ta>
            <ta e="T102" id="Seg_3368" s="T93">Im Himmel hackt man Holz, der Span ⌈sein Span⌉ fällt hierher. – Der Brief.</ta>
            <ta e="T108" id="Seg_3369" s="T102">Ein Mädchen mit Zopf geht die Strasse entlang. – Die Elster.</ta>
            <ta e="T116" id="Seg_3370" s="T108">Es gibt ein solches Ding, wo das Holz (hinein)gehen kann. – Die Messerscheide.</ta>
            <ta e="T130" id="Seg_3371" s="T116">Unter der Sonne stehe ich, blase in ein goldenes Horn; unter dem Monde stehe ich, blase in ein silbernes Horn. – Die Wildgans, der Kranich.</ta>
            <ta e="T138" id="Seg_3372" s="T130">Ein zerlumpter Alter sitzt, wenn du diesen Alten isst, wirst du weinen. – Die Zwiebel.</ta>
            <ta e="T148" id="Seg_3373" s="T138">Zwei von seinen Enden sind scharf, zwei von seinen Enden sind rund, in der Mitte ⌈in seiner Mitte⌉ ein Knopf. – Die Schere.</ta>
            <ta e="T160" id="Seg_3374" s="T148">Zwei Menschen liegen, zwei Menschen stehen, der fünfte Mensch die Tür zumachend geht. - Die Türpfosten, die Tür.</ta>
            <ta e="T166" id="Seg_3375" s="T160">Ich esse, ich esse, scheissen kann ich nicht. - Der Sack.</ta>
            <ta e="T175" id="Seg_3376" s="T166">Ein Hahn sitzt auf dem Zaun, sein Schwanz auf der Erde, seine Stimme bei Gott. – Die Kirchenglocke.</ta>
            <ta e="T181" id="Seg_3377" s="T175">Ich mache nass und zittere. – Der Besen, der den Ofen kehrt.</ta>
            <ta e="T190" id="Seg_3378" s="T181">Ein Knabe steht, sein mittlerer Teil brennt, aber sein [unteres] Ende pisst. – Der Samowar.</ta>
            <ta e="T197" id="Seg_3379" s="T190">Zwei Männer nebeneinander um die Wette laufend gehen. – Die Skier.</ta>
            <ta e="T206" id="Seg_3380" s="T197">[Es] drückt herab mit seinem Fusse, [es] drückt mit seinem Bauche, öffnet seinen Mund, dorthin steckt [es]. – Das Weben.</ta>
            <ta e="T227" id="Seg_3381" s="T206">Schlage mich, hacke mich, ich habe ein Haariges, mitten drinnen ist eine rote Mitte, in der roten Mitte ist eine süsse Mitte. – Die Nuss (von Pinus cembra).</ta>
            <ta e="T234" id="Seg_3382" s="T227">Den Baum umgebendes goldenes Gras wuchs. – Der Ring. Der Penis.</ta>
            <ta e="T242" id="Seg_3383" s="T234">Auf der Stelle, wo die rote Ziege lag, wächst kein Gras. – Das Feuer.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
