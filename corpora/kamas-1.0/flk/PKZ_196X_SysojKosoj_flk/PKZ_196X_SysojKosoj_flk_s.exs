<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID7EB1A650-2CBF-F3A5-16D4-D8157D8DA853">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SysojKosoj_flk.wav" />
         <referenced-file url="PKZ_196X_SysojKosoj_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SysojKosoj_flk\PKZ_196X_SysojKosoj_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">392</ud-information>
            <ud-information attribute-name="# HIAT:w">242</ud-information>
            <ud-information attribute-name="# e">241</ud-information>
            <ud-information attribute-name="# HIAT:u">53</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2406" time="0.005" type="appl" />
         <tli id="T2407" time="0.639" type="appl" />
         <tli id="T2408" time="1.272" type="appl" />
         <tli id="T2409" time="1.906" type="appl" />
         <tli id="T2410" time="2.54" type="appl" />
         <tli id="T2411" time="3.352" type="appl" />
         <tli id="T2412" time="4.164" type="appl" />
         <tli id="T2413" time="4.976" type="appl" />
         <tli id="T2414" time="5.789" type="appl" />
         <tli id="T2415" time="6.601" type="appl" />
         <tli id="T2416" time="7.413" type="appl" />
         <tli id="T2417" time="8.53995456823174" />
         <tli id="T2418" time="9.036" type="appl" />
         <tli id="T2419" time="9.46" type="appl" />
         <tli id="T2420" time="9.885" type="appl" />
         <tli id="T2421" time="10.309" type="appl" />
         <tli id="T2422" time="10.734" type="appl" />
         <tli id="T2423" time="11.62" type="appl" />
         <tli id="T2424" time="12.507" type="appl" />
         <tli id="T2425" time="13.289" type="appl" />
         <tli id="T2426" time="13.745" type="appl" />
         <tli id="T2427" time="14.202" type="appl" />
         <tli id="T2428" time="14.658" type="appl" />
         <tli id="T2429" time="15.114" type="appl" />
         <tli id="T2430" time="15.628" type="appl" />
         <tli id="T2431" time="16.143" type="appl" />
         <tli id="T2432" time="16.658" type="appl" />
         <tli id="T2433" time="17.172" type="appl" />
         <tli id="T2434" time="17.8" type="appl" />
         <tli id="T2435" time="18.99989892229544" />
         <tli id="T2436" time="19.95" type="appl" />
         <tli id="T2437" time="21.18655395615962" />
         <tli id="T2438" time="21.706" type="appl" />
         <tli id="T2439" time="22.302" type="appl" />
         <tli id="T2440" time="22.897" type="appl" />
         <tli id="T2441" time="23.492" type="appl" />
         <tli id="T2442" time="24.088" type="appl" />
         <tli id="T2443" time="24.683" type="appl" />
         <tli id="T2444" time="25.511" type="appl" />
         <tli id="T2445" time="26.061" type="appl" />
         <tli id="T2446" time="26.61" type="appl" />
         <tli id="T2447" time="27.16" type="appl" />
         <tli id="T2448" time="27.709" type="appl" />
         <tli id="T2449" time="28.259" type="appl" />
         <tli id="T2450" time="28.808" type="appl" />
         <tli id="T2451" time="29.358" type="appl" />
         <tli id="T2452" time="29.907" type="appl" />
         <tli id="T2453" time="30.312" type="appl" />
         <tli id="T2454" time="30.718" type="appl" />
         <tli id="T2455" time="31.123" type="appl" />
         <tli id="T2456" time="31.906496927054732" />
         <tli id="T2457" time="32.639" type="appl" />
         <tli id="T2458" time="33.319" type="appl" />
         <tli id="T2459" time="33.999" type="appl" />
         <tli id="T2460" time="34.461" type="appl" />
         <tli id="T2461" time="34.923" type="appl" />
         <tli id="T2462" time="35.388" type="appl" />
         <tli id="T2463" time="35.853" type="appl" />
         <tli id="T2464" time="36.318" type="appl" />
         <tli id="T2465" time="37.694" type="appl" />
         <tli id="T2466" time="39.046" type="appl" />
         <tli id="T2467" time="40.398" type="appl" />
         <tli id="T2468" time="41.75" type="appl" />
         <tli id="T2469" time="42.317" type="appl" />
         <tli id="T2470" time="42.883" type="appl" />
         <tli id="T2471" time="43.45" type="appl" />
         <tli id="T2472" time="44.017" type="appl" />
         <tli id="T2473" time="44.583" type="appl" />
         <tli id="T2474" time="45.33309216547685" />
         <tli id="T2475" time="45.816" type="appl" />
         <tli id="T2476" time="46.432" type="appl" />
         <tli id="T2477" time="47.048" type="appl" />
         <tli id="T2478" time="49.75973528281164" />
         <tli id="T2479" time="50.491" type="appl" />
         <tli id="T2480" time="51.264" type="appl" />
         <tli id="T2481" time="52.036" type="appl" />
         <tli id="T2482" time="52.809" type="appl" />
         <tli id="T2483" time="53.292" type="appl" />
         <tli id="T2484" time="53.775" type="appl" />
         <tli id="T2485" time="54.385" type="appl" />
         <tli id="T2486" time="54.994" type="appl" />
         <tli id="T2487" time="55.604" type="appl" />
         <tli id="T2488" time="56.249" type="appl" />
         <tli id="T2489" time="56.895" type="appl" />
         <tli id="T2490" time="57.54" type="appl" />
         <tli id="T2491" time="57.934" type="appl" />
         <tli id="T2492" time="58.328" type="appl" />
         <tli id="T2493" time="58.723" type="appl" />
         <tli id="T2494" time="60.15301332486729" />
         <tli id="T2495" time="60.906" type="appl" />
         <tli id="T2496" time="61.645" type="appl" />
         <tli id="T2497" time="62.384" type="appl" />
         <tli id="T2498" time="63.70632775489657" />
         <tli id="T2499" time="64.334" type="appl" />
         <tli id="T2500" time="65.097" type="appl" />
         <tli id="T2501" time="65.861" type="appl" />
         <tli id="T2502" time="66.624" type="appl" />
         <tli id="T2503" time="67.388" type="appl" />
         <tli id="T2504" time="68.16" type="appl" />
         <tli id="T2505" time="68.932" type="appl" />
         <tli id="T2506" time="69.704" type="appl" />
         <tli id="T2507" time="70.476" type="appl" />
         <tli id="T2508" time="71.248" type="appl" />
         <tli id="T2509" time="72.019" type="appl" />
         <tli id="T2510" time="72.791" type="appl" />
         <tli id="T2511" time="73.563" type="appl" />
         <tli id="T2512" time="74.335" type="appl" />
         <tli id="T2513" time="75.107" type="appl" />
         <tli id="T2514" time="76.71292522766795" />
         <tli id="T2515" time="77.773" type="appl" />
         <tli id="T2516" time="78.635" type="appl" />
         <tli id="T2517" time="79.497" type="appl" />
         <tli id="T2518" time="80.359" type="appl" />
         <tli id="T2519" time="81.221" type="appl" />
         <tli id="T2520" time="82.083" type="appl" />
         <tli id="T2521" time="82.945" type="appl" />
         <tli id="T2522" time="83.807" type="appl" />
         <tli id="T2523" time="84.535" type="appl" />
         <tli id="T2524" time="85.263" type="appl" />
         <tli id="T2525" time="85.991" type="appl" />
         <tli id="T2526" time="86.719" type="appl" />
         <tli id="T2527" time="87.447" type="appl" />
         <tli id="T2528" time="89.43952419000549" />
         <tli id="T2529" time="90.24" type="appl" />
         <tli id="T2530" time="91.19951482701812" />
         <tli id="T2531" time="91.846" type="appl" />
         <tli id="T2532" time="92.378" type="appl" />
         <tli id="T2533" time="92.911" type="appl" />
         <tli id="T2534" time="93.444" type="appl" />
         <tli id="T2535" time="93.976" type="appl" />
         <tli id="T2536" time="94.509" type="appl" />
         <tli id="T2537" time="95.464" type="appl" />
         <tli id="T2538" time="96.203" type="appl" />
         <tli id="T2539" time="96.943" type="appl" />
         <tli id="T2540" time="97.497" type="appl" />
         <tli id="T2541" time="98.051" type="appl" />
         <tli id="T2542" time="98.604" type="appl" />
         <tli id="T2543" time="99.158" type="appl" />
         <tli id="T2544" time="99.89946854406918" />
         <tli id="T2545" time="100.852" type="appl" />
         <tli id="T2546" time="101.685" type="appl" />
         <tli id="T2547" time="102.519" type="appl" />
         <tli id="T2548" time="103.352" type="appl" />
         <tli id="T2549" time="104.185" type="appl" />
         <tli id="T2550" time="105.018" type="appl" />
         <tli id="T2551" time="105.852" type="appl" />
         <tli id="T2552" time="106.685" type="appl" />
         <tli id="T2553" time="108.026091977851" />
         <tli id="T2554" time="108.836" type="appl" />
         <tli id="T2555" time="109.4" type="appl" />
         <tli id="T2556" time="110.12" type="appl" />
         <tli id="T2557" time="110.84" type="appl" />
         <tli id="T2558" time="111.561" type="appl" />
         <tli id="T2559" time="112.85273296723413" />
         <tli id="T2560" time="113.476" type="appl" />
         <tli id="T2561" time="114.077" type="appl" />
         <tli id="T2562" time="114.677" type="appl" />
         <tli id="T2563" time="117.54604133260113" />
         <tli id="T2564" time="118.951" type="appl" />
         <tli id="T2565" time="119.928" type="appl" />
         <tli id="T2566" time="120.694" type="appl" />
         <tli id="T2567" time="121.273" type="appl" />
         <tli id="T2568" time="121.853" type="appl" />
         <tli id="T2569" time="122.432" type="appl" />
         <tli id="T2570" time="123.22601111568736" />
         <tli id="T2571" time="123.995" type="appl" />
         <tli id="T2572" time="125.38599962474831" />
         <tli id="T2573" time="126.392" type="appl" />
         <tli id="T0" time="127.04346153846154" type="intp" />
         <tli id="T2574" time="127.333" type="appl" />
         <tli id="T2575" time="129.34597855802673" />
         <tli id="T2576" time="129.911" type="appl" />
         <tli id="T2577" time="130.517" type="appl" />
         <tli id="T2578" time="131.122" type="appl" />
         <tli id="T2579" time="131.727" type="appl" />
         <tli id="T2580" time="132.332" type="appl" />
         <tli id="T2581" time="132.938" type="appl" />
         <tli id="T2582" time="133.543" type="appl" />
         <tli id="T2583" time="134.148" type="appl" />
         <tli id="T2584" time="134.754" type="appl" />
         <tli id="T2585" time="135.359" type="appl" />
         <tli id="T2586" time="136.144" type="appl" />
         <tli id="T2587" time="136.832" type="appl" />
         <tli id="T2588" time="137.521" type="appl" />
         <tli id="T2589" time="138.209" type="appl" />
         <tli id="T2590" time="138.897" type="appl" />
         <tli id="T2591" time="139.586" type="appl" />
         <tli id="T2592" time="140.274" type="appl" />
         <tli id="T2593" time="140.962" type="appl" />
         <tli id="T2594" time="141.691" type="appl" />
         <tli id="T2595" time="142.421" type="appl" />
         <tli id="T2596" time="143.15" type="appl" />
         <tli id="T2597" time="143.951" type="appl" />
         <tli id="T2598" time="144.752" type="appl" />
         <tli id="T2599" time="145.553" type="appl" />
         <tli id="T2600" time="146.354" type="appl" />
         <tli id="T2601" time="147.155" type="appl" />
         <tli id="T2602" time="147.956" type="appl" />
         <tli id="T2603" time="148.757" type="appl" />
         <tli id="T2604" time="149.225" type="appl" />
         <tli id="T2605" time="149.692" type="appl" />
         <tli id="T2606" time="150.16" type="appl" />
         <tli id="T2607" time="150.628" type="appl" />
         <tli id="T2608" time="151.782" type="appl" />
         <tli id="T2609" time="152.893" type="appl" />
         <tli id="T2610" time="154.004" type="appl" />
         <tli id="T2611" time="155.115" type="appl" />
         <tli id="T2612" time="155.702" type="appl" />
         <tli id="T2613" time="156.271" type="appl" />
         <tli id="T2614" time="156.841" type="appl" />
         <tli id="T2615" time="157.411" type="appl" />
         <tli id="T2616" time="157.981" type="appl" />
         <tli id="T2617" time="158.55" type="appl" />
         <tli id="T2618" time="163.2591314765239" />
         <tli id="T2619" time="164.028" type="appl" />
         <tli id="T2620" time="164.817" type="appl" />
         <tli id="T2621" time="165.95911711285007" />
         <tli id="T2622" time="166.471" type="appl" />
         <tli id="T2623" time="167.071" type="appl" />
         <tli id="T2624" time="167.487" type="appl" />
         <tli id="T2625" time="167.904" type="appl" />
         <tli id="T2626" time="168.32" type="appl" />
         <tli id="T2627" time="169.4390985996705" />
         <tli id="T2628" time="170.54" type="appl" />
         <tli id="T2629" time="171.412" type="appl" />
         <tli id="T2630" time="172.284" type="appl" />
         <tli id="T2631" time="173.157" type="appl" />
         <tli id="T2632" time="173.802" type="appl" />
         <tli id="T2633" time="174.447" type="appl" />
         <tli id="T2634" time="175.092" type="appl" />
         <tli id="T2635" time="175.736" type="appl" />
         <tli id="T2636" time="176.381" type="appl" />
         <tli id="T2637" time="177.026" type="appl" />
         <tli id="T2638" time="177.671" type="appl" />
         <tli id="T2639" time="178.058" type="appl" />
         <tli id="T2640" time="178.446" type="appl" />
         <tli id="T2641" time="178.833" type="appl" />
         <tli id="T2642" time="179.22" type="appl" />
         <tli id="T2643" time="179.608" type="appl" />
         <tli id="T2644" time="179.995" type="appl" />
         <tli id="T2645" time="180.382" type="appl" />
         <tli id="T2646" time="180.77" type="appl" />
         <tli id="T2647" time="181.157" type="appl" />
         <tli id="T2648" time="182.099" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T2424" start="T2423">
            <tli id="T2423.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2648" id="Seg_0" n="sc" s="T2406">
               <ts e="T2410" id="Seg_2" n="HIAT:u" s="T2406">
                  <ts e="T2407" id="Seg_4" n="HIAT:w" s="T2406">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2408" id="Seg_7" n="HIAT:w" s="T2407">onʼiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T2409" id="Seg_11" n="HIAT:w" s="T2408">kuz-</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2410" id="Seg_15" n="HIAT:w" s="T2409">kuza</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2417" id="Seg_19" n="HIAT:u" s="T2410">
                  <ts e="T2411" id="Seg_21" n="HIAT:w" s="T2410">Dĭʔnə</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2412" id="Seg_24" n="HIAT:w" s="T2411">šobi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2413" id="Seg_27" n="HIAT:w" s="T2412">kuza</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_29" n="HIAT:ip">(</nts>
                  <ts e="T2414" id="Seg_31" n="HIAT:w" s="T2413">m-</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2415" id="Seg_34" n="HIAT:w" s="T2414">məj-</ts>
                  <nts id="Seg_35" n="HIAT:ip">)</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2416" id="Seg_38" n="HIAT:w" s="T2415">najmɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2417" id="Seg_41" n="HIAT:w" s="T2416">izittə</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2422" id="Seg_45" n="HIAT:u" s="T2417">
                  <nts id="Seg_46" n="HIAT:ip">(</nts>
                  <ts e="T2418" id="Seg_48" n="HIAT:w" s="T2417">Kăde-</ts>
                  <nts id="Seg_49" n="HIAT:ip">)</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_51" n="HIAT:ip">(</nts>
                  <ts e="T2419" id="Seg_53" n="HIAT:w" s="T2418">Kădəʔ</ts>
                  <nts id="Seg_54" n="HIAT:ip">)</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2420" id="Seg_57" n="HIAT:w" s="T2419">tănan</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_59" n="HIAT:ip">(</nts>
                  <ts e="T2421" id="Seg_61" n="HIAT:w" s="T2420">ka-</ts>
                  <nts id="Seg_62" n="HIAT:ip">)</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2422" id="Seg_65" n="HIAT:w" s="T2421">kăštəliaʔi</ts>
                  <nts id="Seg_66" n="HIAT:ip">?</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2424" id="Seg_69" n="HIAT:u" s="T2422">
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <ts e="T2423" id="Seg_72" n="HIAT:w" s="T2422">Si-</ts>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2423.tx.1" id="Seg_76" n="HIAT:w" s="T2423">Sɨsoj</ts>
                  <nts id="Seg_77" n="HIAT:ip">_</nts>
                  <ts e="T2424" id="Seg_79" n="HIAT:w" s="T2423.tx.1">Kasoj</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2429" id="Seg_83" n="HIAT:u" s="T2424">
                  <ts e="T2425" id="Seg_85" n="HIAT:w" s="T2424">Dĭ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2426" id="Seg_88" n="HIAT:w" s="T2425">măndə:</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">"</nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ts e="T2427" id="Seg_93" n="HIAT:w" s="T2426">m-</ts>
                  <nts id="Seg_94" n="HIAT:ip">)</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2428" id="Seg_97" n="HIAT:w" s="T2427">No</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2429" id="Seg_101" n="HIAT:w" s="T2428">amnoʔ</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2433" id="Seg_105" n="HIAT:u" s="T2429">
                  <ts e="T2430" id="Seg_107" n="HIAT:w" s="T2429">Mĭnzəreʔ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2431" id="Seg_110" n="HIAT:w" s="T2430">măna</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2432" id="Seg_113" n="HIAT:w" s="T2431">šide</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2433" id="Seg_116" n="HIAT:w" s="T2432">munəj</ts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2435" id="Seg_121" n="HIAT:u" s="T2433">
                  <ts e="T2434" id="Seg_123" n="HIAT:w" s="T2433">Dĭ</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2435" id="Seg_126" n="HIAT:w" s="T2434">mĭnzərbi</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2437" id="Seg_130" n="HIAT:u" s="T2435">
                  <ts e="T2436" id="Seg_132" n="HIAT:w" s="T2435">Mĭnzərbi</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2437" id="Seg_136" n="HIAT:w" s="T2436">mĭnzərbi</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2443" id="Seg_140" n="HIAT:u" s="T2437">
                  <ts e="T2438" id="Seg_142" n="HIAT:w" s="T2437">Ibi</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2439" id="Seg_145" n="HIAT:w" s="T2438">da</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2440" id="Seg_148" n="HIAT:w" s="T2439">onʼiʔ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2441" id="Seg_151" n="HIAT:w" s="T2440">amnuʔpi</ts>
                  <nts id="Seg_152" n="HIAT:ip">,</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2442" id="Seg_155" n="HIAT:w" s="T2441">onʼiʔ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2443" id="Seg_158" n="HIAT:w" s="T2442">detleʔbə</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2452" id="Seg_162" n="HIAT:u" s="T2443">
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <ts e="T2444" id="Seg_165" n="HIAT:w" s="T2443">Ĭmbi</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2445" id="Seg_168" n="HIAT:w" s="T2444">tăn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2446" id="Seg_171" n="HIAT:w" s="T2445">onʼiʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2447" id="Seg_174" n="HIAT:w" s="T2446">deʔpiel</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2448" id="Seg_178" n="HIAT:w" s="T2447">măn</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2449" id="Seg_181" n="HIAT:w" s="T2448">šide</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <ts e="T2450" id="Seg_185" n="HIAT:w" s="T2449">mă-</ts>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2451" id="Seg_189" n="HIAT:w" s="T2450">mămbiam</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2452" id="Seg_192" n="HIAT:w" s="T2451">mĭnzereʔ</ts>
                  <nts id="Seg_193" n="HIAT:ip">!</nts>
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2456" id="Seg_197" n="HIAT:u" s="T2452">
                  <nts id="Seg_198" n="HIAT:ip">"</nts>
                  <ts e="T2453" id="Seg_200" n="HIAT:w" s="T2452">A</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2454" id="Seg_203" n="HIAT:w" s="T2453">măn</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2455" id="Seg_206" n="HIAT:w" s="T2454">onʼiʔ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2456" id="Seg_209" n="HIAT:w" s="T2455">ambiam</ts>
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2459" id="Seg_214" n="HIAT:u" s="T2456">
                  <ts e="T2457" id="Seg_216" n="HIAT:w" s="T2456">Dĭgəttə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2458" id="Seg_219" n="HIAT:w" s="T2457">dĭ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2459" id="Seg_222" n="HIAT:w" s="T2458">kudonzlubi</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2461" id="Seg_226" n="HIAT:u" s="T2459">
                  <nts id="Seg_227" n="HIAT:ip">"</nts>
                  <ts e="T2460" id="Seg_229" n="HIAT:w" s="T2459">Kanaʔ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2461" id="Seg_232" n="HIAT:w" s="T2460">döʔə</ts>
                  <nts id="Seg_233" n="HIAT:ip">!</nts>
                  <nts id="Seg_234" n="HIAT:ip">"</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2464" id="Seg_237" n="HIAT:u" s="T2461">
                  <ts e="T2462" id="Seg_239" n="HIAT:w" s="T2461">Dĭ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_241" n="HIAT:ip">(</nts>
                  <ts e="T2463" id="Seg_243" n="HIAT:w" s="T2462">k-</ts>
                  <nts id="Seg_244" n="HIAT:ip">)</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2464" id="Seg_247" n="HIAT:w" s="T2463">kambi</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2468" id="Seg_251" n="HIAT:u" s="T2464">
                  <ts e="T2465" id="Seg_253" n="HIAT:w" s="T2464">I</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2466" id="Seg_256" n="HIAT:w" s="T2465">kanzittə</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2467" id="Seg_259" n="HIAT:w" s="T2466">xatʼel</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2468" id="Seg_262" n="HIAT:w" s="T2467">dĭʔə</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2474" id="Seg_266" n="HIAT:u" s="T2468">
                  <nts id="Seg_267" n="HIAT:ip">(</nts>
                  <ts e="T2469" id="Seg_269" n="HIAT:w" s="T2468">Dĭ-</ts>
                  <nts id="Seg_270" n="HIAT:ip">)</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2470" id="Seg_273" n="HIAT:w" s="T2469">Dĭgəttə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2471" id="Seg_276" n="HIAT:w" s="T2470">dĭ</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2472" id="Seg_279" n="HIAT:w" s="T2471">kirgarlaʔbə:</ts>
                  <nts id="Seg_280" n="HIAT:ip">"</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2473" id="Seg_283" n="HIAT:w" s="T2472">Šoʔ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2474" id="Seg_286" n="HIAT:w" s="T2473">döbər</ts>
                  <nts id="Seg_287" n="HIAT:ip">!</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2478" id="Seg_290" n="HIAT:u" s="T2474">
                  <ts e="T2475" id="Seg_292" n="HIAT:w" s="T2474">Kanaʔ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2476" id="Seg_295" n="HIAT:w" s="T2475">măna</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2477" id="Seg_298" n="HIAT:w" s="T2476">užu</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2478" id="Seg_301" n="HIAT:w" s="T2477">iʔ</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2482" id="Seg_305" n="HIAT:u" s="T2478">
                  <ts e="T2479" id="Seg_307" n="HIAT:w" s="T2478">Kunə</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T2480" id="Seg_311" n="HIAT:w" s="T2479">kunə</ts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2481" id="Seg_316" n="HIAT:w" s="T2480">kunə</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2482" id="Seg_319" n="HIAT:w" s="T2481">kunə</ts>
                  <nts id="Seg_320" n="HIAT:ip">"</nts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2484" id="Seg_324" n="HIAT:u" s="T2482">
                  <ts e="T2483" id="Seg_326" n="HIAT:w" s="T2482">Dĭ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2484" id="Seg_329" n="HIAT:w" s="T2483">kambi</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2487" id="Seg_333" n="HIAT:u" s="T2484">
                  <ts e="T2485" id="Seg_335" n="HIAT:w" s="T2484">Ibi</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_337" n="HIAT:ip">(</nts>
                  <ts e="T2486" id="Seg_339" n="HIAT:w" s="T2485">uzu-</ts>
                  <nts id="Seg_340" n="HIAT:ip">)</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2487" id="Seg_343" n="HIAT:w" s="T2486">užu</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2490" id="Seg_347" n="HIAT:u" s="T2487">
                  <ts e="T2488" id="Seg_349" n="HIAT:w" s="T2487">Šonəga</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2489" id="Seg_352" n="HIAT:w" s="T2488">i</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2490" id="Seg_355" n="HIAT:w" s="T2489">sădărlaʔbə</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2494" id="Seg_359" n="HIAT:u" s="T2490">
                  <ts e="T2491" id="Seg_361" n="HIAT:w" s="T2490">To</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2492" id="Seg_364" n="HIAT:w" s="T2491">döbər</ts>
                  <nts id="Seg_365" n="HIAT:ip">,</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2493" id="Seg_368" n="HIAT:w" s="T2492">to</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2494" id="Seg_371" n="HIAT:w" s="T2493">döbər</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2498" id="Seg_375" n="HIAT:u" s="T2494">
                  <ts e="T2495" id="Seg_377" n="HIAT:w" s="T2494">To</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2496" id="Seg_380" n="HIAT:w" s="T2495">bögəlgəndə</ts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2497" id="Seg_384" n="HIAT:w" s="T2496">to</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2498" id="Seg_387" n="HIAT:w" s="T2497">kadəlgəndə</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2503" id="Seg_391" n="HIAT:u" s="T2498">
                  <ts e="T2499" id="Seg_393" n="HIAT:w" s="T2498">Dĭgəttə</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2500" id="Seg_396" n="HIAT:w" s="T2499">dĭ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2501" id="Seg_399" n="HIAT:w" s="T2500">kulaʔbə</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2502" id="Seg_402" n="HIAT:w" s="T2501">i</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2503" id="Seg_405" n="HIAT:w" s="T2502">kudonzlaʔbə</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2514" id="Seg_409" n="HIAT:u" s="T2503">
                  <ts e="T2504" id="Seg_411" n="HIAT:w" s="T2503">Măn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2505" id="Seg_414" n="HIAT:w" s="T2504">mămbiam</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2506" id="Seg_417" n="HIAT:w" s="T2505">kunə</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2507" id="Seg_421" n="HIAT:w" s="T2506">kunə</ts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2508" id="Seg_425" n="HIAT:w" s="T2507">a</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2509" id="Seg_428" n="HIAT:w" s="T2508">tăn</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2510" id="Seg_431" n="HIAT:w" s="T2509">dö</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2511" id="Seg_434" n="HIAT:w" s="T2510">kadəlgəndə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2512" id="Seg_437" n="HIAT:w" s="T2511">i</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2513" id="Seg_440" n="HIAT:w" s="T2512">bögəlgəndə</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2514" id="Seg_443" n="HIAT:w" s="T2513">ibiel</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2522" id="Seg_447" n="HIAT:u" s="T2514">
                  <ts e="T2515" id="Seg_449" n="HIAT:w" s="T2514">Aktʼa</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2516" id="Seg_452" n="HIAT:w" s="T2515">amga</ts>
                  <nts id="Seg_453" n="HIAT:ip">,</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2517" id="Seg_456" n="HIAT:w" s="T2516">nada</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_458" n="HIAT:ip">(</nts>
                  <ts e="T2518" id="Seg_460" n="HIAT:w" s="T2517">iššo=</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2519" id="Seg_463" n="HIAT:w" s="T2518">dil-</ts>
                  <nts id="Seg_464" n="HIAT:ip">)</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2520" id="Seg_467" n="HIAT:w" s="T2519">iššo</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2521" id="Seg_470" n="HIAT:w" s="T2520">dĭldʼi</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2522" id="Seg_473" n="HIAT:w" s="T2521">sumna</ts>
                  <nts id="Seg_474" n="HIAT:ip">"</nts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2528" id="Seg_478" n="HIAT:u" s="T2522">
                  <ts e="T2523" id="Seg_480" n="HIAT:w" s="T2522">Dĭgəttə</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2524" id="Seg_483" n="HIAT:w" s="T2523">dĭ</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2525" id="Seg_486" n="HIAT:w" s="T2524">mĭbi</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_488" n="HIAT:ip">(</nts>
                  <ts e="T2526" id="Seg_490" n="HIAT:w" s="T2525">jemu=</ts>
                  <nts id="Seg_491" n="HIAT:ip">)</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2527" id="Seg_494" n="HIAT:w" s="T2526">dĭʔnə</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2528" id="Seg_497" n="HIAT:w" s="T2527">bieʔ</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2530" id="Seg_501" n="HIAT:u" s="T2528">
                  <ts e="T2529" id="Seg_503" n="HIAT:w" s="T2528">Dĭ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2530" id="Seg_506" n="HIAT:w" s="T2529">kambi</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2536" id="Seg_510" n="HIAT:u" s="T2530">
                  <ts e="T2531" id="Seg_512" n="HIAT:w" s="T2530">Dĭ</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_514" n="HIAT:ip">(</nts>
                  <ts e="T2532" id="Seg_516" n="HIAT:w" s="T2531">užum=</ts>
                  <nts id="Seg_517" n="HIAT:ip">)</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2533" id="Seg_520" n="HIAT:w" s="T2532">dĭ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2534" id="Seg_523" n="HIAT:w" s="T2533">že</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2535" id="Seg_526" n="HIAT:w" s="T2534">užum</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2536" id="Seg_529" n="HIAT:w" s="T2535">deʔpi</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2539" id="Seg_533" n="HIAT:u" s="T2536">
                  <ts e="T2537" id="Seg_535" n="HIAT:w" s="T2536">Kunə</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2538" id="Seg_539" n="HIAT:w" s="T2537">kunə</ts>
                  <nts id="Seg_540" n="HIAT:ip">,</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_542" n="HIAT:ip">(</nts>
                  <ts e="T2539" id="Seg_544" n="HIAT:w" s="T2538">barəʔlaʔbə</ts>
                  <nts id="Seg_545" n="HIAT:ip">)</nts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2544" id="Seg_549" n="HIAT:u" s="T2539">
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <ts e="T2540" id="Seg_552" n="HIAT:w" s="T2539">No</ts>
                  <nts id="Seg_553" n="HIAT:ip">,</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2541" id="Seg_556" n="HIAT:w" s="T2540">ugaːndə</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2542" id="Seg_559" n="HIAT:w" s="T2541">jakšə</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_561" n="HIAT:ip">(</nts>
                  <ts e="T2543" id="Seg_563" n="HIAT:w" s="T2542">uz-</ts>
                  <nts id="Seg_564" n="HIAT:ip">)</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2544" id="Seg_567" n="HIAT:w" s="T2543">užu</ts>
                  <nts id="Seg_568" n="HIAT:ip">"</nts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2553" id="Seg_572" n="HIAT:u" s="T2544">
                  <ts e="T2545" id="Seg_574" n="HIAT:w" s="T2544">Dĭgəttə</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2546" id="Seg_577" n="HIAT:w" s="T2545">šerbi</ts>
                  <nts id="Seg_578" n="HIAT:ip">,</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2547" id="Seg_581" n="HIAT:w" s="T2546">dĭ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2548" id="Seg_584" n="HIAT:w" s="T2547">măndə:</ts>
                  <nts id="Seg_585" n="HIAT:ip">"</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2549" id="Seg_588" n="HIAT:w" s="T2548">Mĭnzəreʔ</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2550" id="Seg_591" n="HIAT:w" s="T2549">kaməndə</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2551" id="Seg_594" n="HIAT:w" s="T2550">măn</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2552" id="Seg_597" n="HIAT:w" s="T2551">ej</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2553" id="Seg_600" n="HIAT:w" s="T2552">ambiam</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2555" id="Seg_604" n="HIAT:u" s="T2553">
                  <nts id="Seg_605" n="HIAT:ip">"</nts>
                  <ts e="T2554" id="Seg_607" n="HIAT:w" s="T2553">Dărə</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2555" id="Seg_610" n="HIAT:w" s="T2554">mĭnzəreʔ</ts>
                  <nts id="Seg_611" n="HIAT:ip">"</nts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2559" id="Seg_615" n="HIAT:u" s="T2555">
                  <ts e="T2556" id="Seg_617" n="HIAT:w" s="T2555">Dĭ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2557" id="Seg_620" n="HIAT:w" s="T2556">măndə:</ts>
                  <nts id="Seg_621" n="HIAT:ip">"</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2558" id="Seg_624" n="HIAT:w" s="T2557">ĭmbi</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2559" id="Seg_627" n="HIAT:w" s="T2558">mĭnzərzittə</ts>
                  <nts id="Seg_628" n="HIAT:ip">?</nts>
                  <nts id="Seg_629" n="HIAT:ip">"</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2563" id="Seg_632" n="HIAT:u" s="T2559">
                  <nts id="Seg_633" n="HIAT:ip">(</nts>
                  <ts e="T2560" id="Seg_635" n="HIAT:w" s="T2559">Dĭʔnə=</ts>
                  <nts id="Seg_636" n="HIAT:ip">)</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2561" id="Seg_639" n="HIAT:w" s="T2560">Dĭn</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2562" id="Seg_642" n="HIAT:w" s="T2561">jamat</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2563" id="Seg_645" n="HIAT:w" s="T2562">ibi</ts>
                  <nts id="Seg_646" n="HIAT:ip">.</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2565" id="Seg_649" n="HIAT:u" s="T2563">
                  <nts id="Seg_650" n="HIAT:ip">(</nts>
                  <ts e="T2564" id="Seg_652" n="HIAT:w" s="T2563">Aʔ-</ts>
                  <nts id="Seg_653" n="HIAT:ip">)</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2565" id="Seg_656" n="HIAT:w" s="T2564">Băʔpi</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2570" id="Seg_660" n="HIAT:u" s="T2565">
                  <ts e="T2566" id="Seg_662" n="HIAT:w" s="T2565">I</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2567" id="Seg_665" n="HIAT:w" s="T2566">mĭnzerbi</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2568" id="Seg_669" n="HIAT:w" s="T2567">döbər</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2569" id="Seg_672" n="HIAT:w" s="T2568">munəj</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2570" id="Seg_675" n="HIAT:w" s="T2569">embi</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2572" id="Seg_679" n="HIAT:u" s="T2570">
                  <ts e="T2571" id="Seg_681" n="HIAT:w" s="T2570">Kajaʔ</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2572" id="Seg_684" n="HIAT:w" s="T2571">embi</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2575" id="Seg_688" n="HIAT:u" s="T2572">
                  <nts id="Seg_689" n="HIAT:ip">(</nts>
                  <ts e="T2573" id="Seg_691" n="HIAT:w" s="T2572">Lavlo-</ts>
                  <nts id="Seg_692" n="HIAT:ip">)</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_695" n="HIAT:w" s="T2573">Lavrovɨj</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2574" id="Seg_698" n="HIAT:w" s="T0">lis</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2575" id="Seg_701" n="HIAT:w" s="T2574">embi</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2585" id="Seg_705" n="HIAT:u" s="T2575">
                  <ts e="T2576" id="Seg_707" n="HIAT:w" s="T2575">Dĭgəttə</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2577" id="Seg_710" n="HIAT:w" s="T2576">mĭnzərbi</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2578" id="Seg_713" n="HIAT:w" s="T2577">dĭ</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_715" n="HIAT:ip">(</nts>
                  <ts e="T2579" id="Seg_717" n="HIAT:w" s="T2578">amdə-</ts>
                  <nts id="Seg_718" n="HIAT:ip">)</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2580" id="Seg_721" n="HIAT:w" s="T2579">amnaʔbə</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2581" id="Seg_724" n="HIAT:w" s="T2580">da</ts>
                  <nts id="Seg_725" n="HIAT:ip">"</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2582" id="Seg_728" n="HIAT:w" s="T2581">Kaməndə</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2583" id="Seg_731" n="HIAT:w" s="T2582">dĭrgit</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2584" id="Seg_734" n="HIAT:w" s="T2583">ej</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2585" id="Seg_737" n="HIAT:w" s="T2584">ambiam</ts>
                  <nts id="Seg_738" n="HIAT:ip">"</nts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2593" id="Seg_742" n="HIAT:u" s="T2585">
                  <ts e="T2586" id="Seg_744" n="HIAT:w" s="T2585">Dĭgəttə</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2587" id="Seg_747" n="HIAT:w" s="T2586">stal</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2588" id="Seg_750" n="HIAT:w" s="T2587">šerzittə</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2589" id="Seg_753" n="HIAT:w" s="T2588">jamaʔtə</ts>
                  <nts id="Seg_754" n="HIAT:ip">,</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2590" id="Seg_757" n="HIAT:w" s="T2589">a</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2591" id="Seg_760" n="HIAT:w" s="T2590">dĭn</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2592" id="Seg_763" n="HIAT:w" s="T2591">šiʔi</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2593" id="Seg_766" n="HIAT:w" s="T2592">molaːmbiʔi</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2596" id="Seg_770" n="HIAT:u" s="T2593">
                  <ts e="T2594" id="Seg_772" n="HIAT:w" s="T2593">Ujut</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2595" id="Seg_775" n="HIAT:w" s="T2594">döbər</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2596" id="Seg_778" n="HIAT:w" s="T2595">kambi</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2603" id="Seg_782" n="HIAT:u" s="T2596">
                  <nts id="Seg_783" n="HIAT:ip">"</nts>
                  <ts e="T2597" id="Seg_785" n="HIAT:w" s="T2596">A</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2598" id="Seg_788" n="HIAT:w" s="T2597">tăn</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2599" id="Seg_791" n="HIAT:w" s="T2598">tuj</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2600" id="Seg_794" n="HIAT:w" s="T2599">măndoʔ</ts>
                  <nts id="Seg_795" n="HIAT:ip">,</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2601" id="Seg_798" n="HIAT:w" s="T2600">măn</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2602" id="Seg_801" n="HIAT:w" s="T2601">girgit</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2603" id="Seg_804" n="HIAT:w" s="T2602">jamaʔi</ts>
                  <nts id="Seg_805" n="HIAT:ip">"</nts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2607" id="Seg_809" n="HIAT:u" s="T2603">
                  <nts id="Seg_810" n="HIAT:ip">"</nts>
                  <ts e="T2604" id="Seg_812" n="HIAT:w" s="T2603">A</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2605" id="Seg_815" n="HIAT:w" s="T2604">tăn</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2606" id="Seg_818" n="HIAT:w" s="T2605">tujo</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2607" id="Seg_821" n="HIAT:w" s="T2606">ambial</ts>
                  <nts id="Seg_822" n="HIAT:ip">"</nts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2611" id="Seg_826" n="HIAT:u" s="T2607">
                  <ts e="T2608" id="Seg_828" n="HIAT:w" s="T2607">Dĭgəttə</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2609" id="Seg_831" n="HIAT:w" s="T2608">kambiʔi</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2610" id="Seg_834" n="HIAT:w" s="T2609">jadajlaʔ</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2611" id="Seg_837" n="HIAT:w" s="T2610">ineziʔ</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2618" id="Seg_841" n="HIAT:u" s="T2611">
                  <ts e="T2612" id="Seg_843" n="HIAT:w" s="T2611">Dĭgəttə</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2613" id="Seg_846" n="HIAT:w" s="T2612">dĭn</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2614" id="Seg_849" n="HIAT:w" s="T2613">bü</ts>
                  <nts id="Seg_850" n="HIAT:ip">,</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2615" id="Seg_853" n="HIAT:w" s="T2614">bügən</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2616" id="Seg_856" n="HIAT:w" s="T2615">kandəgaʔi</ts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2618" id="Seg_860" n="HIAT:w" s="T2616">dĭ</ts>
                  <nts id="Seg_861" n="HIAT:ip">…</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2621" id="Seg_864" n="HIAT:u" s="T2618">
                  <ts e="T2620" id="Seg_866" n="HIAT:w" s="T2618">baza</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2621" id="Seg_869" n="HIAT:w" s="T2620">iluʔpi</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2623" id="Seg_873" n="HIAT:u" s="T2621">
                  <ts e="T2622" id="Seg_875" n="HIAT:w" s="T2621">Dĭ</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2623" id="Seg_878" n="HIAT:w" s="T2622">maːluʔpi</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2627" id="Seg_882" n="HIAT:u" s="T2623">
                  <nts id="Seg_883" n="HIAT:ip">"</nts>
                  <ts e="T2624" id="Seg_885" n="HIAT:w" s="T2623">Tăn</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2625" id="Seg_888" n="HIAT:w" s="T2624">măna</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2626" id="Seg_891" n="HIAT:w" s="T2625">iʔ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2627" id="Seg_894" n="HIAT:w" s="T2626">mandəʔ</ts>
                  <nts id="Seg_895" n="HIAT:ip">!</nts>
                  <nts id="Seg_896" n="HIAT:ip">"</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2631" id="Seg_899" n="HIAT:u" s="T2627">
                  <ts e="T2628" id="Seg_901" n="HIAT:w" s="T2627">I</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2629" id="Seg_904" n="HIAT:w" s="T2628">kirgarlaʔbə:</ts>
                  <nts id="Seg_905" n="HIAT:ip">"</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2630" id="Seg_908" n="HIAT:w" s="T2629">Măn</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2631" id="Seg_911" n="HIAT:w" s="T2630">maluʔpiam</ts>
                  <nts id="Seg_912" n="HIAT:ip">!</nts>
                  <nts id="Seg_913" n="HIAT:ip">"</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2638" id="Seg_916" n="HIAT:u" s="T2631">
                  <nts id="Seg_917" n="HIAT:ip">"</nts>
                  <ts e="T2632" id="Seg_919" n="HIAT:w" s="T2631">A</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2633" id="Seg_922" n="HIAT:w" s="T2632">tăn</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2634" id="Seg_925" n="HIAT:w" s="T2633">măndərzittə</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2635" id="Seg_928" n="HIAT:w" s="T2634">măna</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2636" id="Seg_931" n="HIAT:w" s="T2635">nʼe</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2637" id="Seg_934" n="HIAT:w" s="T2636">velʼel</ts>
                  <nts id="Seg_935" n="HIAT:ip">,</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_937" n="HIAT:ip">(</nts>
                  <ts e="T2638" id="Seg_939" n="HIAT:w" s="T2637">amnolaʔbəʔ</ts>
                  <nts id="Seg_940" n="HIAT:ip">)</nts>
                  <nts id="Seg_941" n="HIAT:ip">"</nts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2647" id="Seg_945" n="HIAT:u" s="T2638">
                  <ts e="T2639" id="Seg_947" n="HIAT:w" s="T2638">Bostə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2640" id="Seg_950" n="HIAT:w" s="T2639">kalla</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2641" id="Seg_953" n="HIAT:w" s="T2640">dʼürbi</ts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2642" id="Seg_957" n="HIAT:w" s="T2641">dĭ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_959" n="HIAT:ip">(</nts>
                  <ts e="T2643" id="Seg_961" n="HIAT:w" s="T2642">dĭʔ-</ts>
                  <nts id="Seg_962" n="HIAT:ip">)</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2644" id="Seg_965" n="HIAT:w" s="T2643">tujo</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2645" id="Seg_968" n="HIAT:w" s="T2644">da</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2646" id="Seg_971" n="HIAT:w" s="T2645">dĭn</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2647" id="Seg_974" n="HIAT:w" s="T2646">amnolaʔbə</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2648" id="Seg_978" n="HIAT:u" s="T2647">
                  <ts e="T2648" id="Seg_980" n="HIAT:w" s="T2647">Kabarləj</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2648" id="Seg_983" n="sc" s="T2406">
               <ts e="T2407" id="Seg_985" n="e" s="T2406">Amnobi </ts>
               <ts e="T2408" id="Seg_987" n="e" s="T2407">onʼiʔ </ts>
               <ts e="T2409" id="Seg_989" n="e" s="T2408">(kuz-) </ts>
               <ts e="T2410" id="Seg_991" n="e" s="T2409">kuza. </ts>
               <ts e="T2411" id="Seg_993" n="e" s="T2410">Dĭʔnə </ts>
               <ts e="T2412" id="Seg_995" n="e" s="T2411">šobi </ts>
               <ts e="T2413" id="Seg_997" n="e" s="T2412">kuza </ts>
               <ts e="T2414" id="Seg_999" n="e" s="T2413">(m- </ts>
               <ts e="T2415" id="Seg_1001" n="e" s="T2414">məj-) </ts>
               <ts e="T2416" id="Seg_1003" n="e" s="T2415">najmɨ </ts>
               <ts e="T2417" id="Seg_1005" n="e" s="T2416">izittə. </ts>
               <ts e="T2418" id="Seg_1007" n="e" s="T2417">(Kăde-) </ts>
               <ts e="T2419" id="Seg_1009" n="e" s="T2418">(Kădəʔ) </ts>
               <ts e="T2420" id="Seg_1011" n="e" s="T2419">tănan </ts>
               <ts e="T2421" id="Seg_1013" n="e" s="T2420">(ka-) </ts>
               <ts e="T2422" id="Seg_1015" n="e" s="T2421">kăštəliaʔi? </ts>
               <ts e="T2423" id="Seg_1017" n="e" s="T2422">(Si-) </ts>
               <ts e="T2424" id="Seg_1019" n="e" s="T2423">Sɨsoj_Kasoj. </ts>
               <ts e="T2425" id="Seg_1021" n="e" s="T2424">Dĭ </ts>
               <ts e="T2426" id="Seg_1023" n="e" s="T2425">măndə: </ts>
               <ts e="T2427" id="Seg_1025" n="e" s="T2426">"(m-) </ts>
               <ts e="T2428" id="Seg_1027" n="e" s="T2427">No, </ts>
               <ts e="T2429" id="Seg_1029" n="e" s="T2428">amnoʔ. </ts>
               <ts e="T2430" id="Seg_1031" n="e" s="T2429">Mĭnzəreʔ </ts>
               <ts e="T2431" id="Seg_1033" n="e" s="T2430">măna </ts>
               <ts e="T2432" id="Seg_1035" n="e" s="T2431">šide </ts>
               <ts e="T2433" id="Seg_1037" n="e" s="T2432">munəj". </ts>
               <ts e="T2434" id="Seg_1039" n="e" s="T2433">Dĭ </ts>
               <ts e="T2435" id="Seg_1041" n="e" s="T2434">mĭnzərbi. </ts>
               <ts e="T2436" id="Seg_1043" n="e" s="T2435">Mĭnzərbi, </ts>
               <ts e="T2437" id="Seg_1045" n="e" s="T2436">mĭnzərbi. </ts>
               <ts e="T2438" id="Seg_1047" n="e" s="T2437">Ibi </ts>
               <ts e="T2439" id="Seg_1049" n="e" s="T2438">da </ts>
               <ts e="T2440" id="Seg_1051" n="e" s="T2439">onʼiʔ </ts>
               <ts e="T2441" id="Seg_1053" n="e" s="T2440">amnuʔpi, </ts>
               <ts e="T2442" id="Seg_1055" n="e" s="T2441">onʼiʔ </ts>
               <ts e="T2443" id="Seg_1057" n="e" s="T2442">detleʔbə. </ts>
               <ts e="T2444" id="Seg_1059" n="e" s="T2443">"Ĭmbi </ts>
               <ts e="T2445" id="Seg_1061" n="e" s="T2444">tăn </ts>
               <ts e="T2446" id="Seg_1063" n="e" s="T2445">onʼiʔ </ts>
               <ts e="T2447" id="Seg_1065" n="e" s="T2446">deʔpiel, </ts>
               <ts e="T2448" id="Seg_1067" n="e" s="T2447">măn </ts>
               <ts e="T2449" id="Seg_1069" n="e" s="T2448">šide </ts>
               <ts e="T2450" id="Seg_1071" n="e" s="T2449">(mă-) </ts>
               <ts e="T2451" id="Seg_1073" n="e" s="T2450">mămbiam </ts>
               <ts e="T2452" id="Seg_1075" n="e" s="T2451">mĭnzereʔ!" </ts>
               <ts e="T2453" id="Seg_1077" n="e" s="T2452">"A </ts>
               <ts e="T2454" id="Seg_1079" n="e" s="T2453">măn </ts>
               <ts e="T2455" id="Seg_1081" n="e" s="T2454">onʼiʔ </ts>
               <ts e="T2456" id="Seg_1083" n="e" s="T2455">ambiam". </ts>
               <ts e="T2457" id="Seg_1085" n="e" s="T2456">Dĭgəttə </ts>
               <ts e="T2458" id="Seg_1087" n="e" s="T2457">dĭ </ts>
               <ts e="T2459" id="Seg_1089" n="e" s="T2458">kudonzlubi. </ts>
               <ts e="T2460" id="Seg_1091" n="e" s="T2459">"Kanaʔ </ts>
               <ts e="T2461" id="Seg_1093" n="e" s="T2460">döʔə!" </ts>
               <ts e="T2462" id="Seg_1095" n="e" s="T2461">Dĭ </ts>
               <ts e="T2463" id="Seg_1097" n="e" s="T2462">(k-) </ts>
               <ts e="T2464" id="Seg_1099" n="e" s="T2463">kambi. </ts>
               <ts e="T2465" id="Seg_1101" n="e" s="T2464">I </ts>
               <ts e="T2466" id="Seg_1103" n="e" s="T2465">kanzittə </ts>
               <ts e="T2467" id="Seg_1105" n="e" s="T2466">xatʼel </ts>
               <ts e="T2468" id="Seg_1107" n="e" s="T2467">dĭʔə. </ts>
               <ts e="T2469" id="Seg_1109" n="e" s="T2468">(Dĭ-) </ts>
               <ts e="T2470" id="Seg_1111" n="e" s="T2469">Dĭgəttə </ts>
               <ts e="T2471" id="Seg_1113" n="e" s="T2470">dĭ </ts>
               <ts e="T2472" id="Seg_1115" n="e" s="T2471">kirgarlaʔbə:" </ts>
               <ts e="T2473" id="Seg_1117" n="e" s="T2472">Šoʔ </ts>
               <ts e="T2474" id="Seg_1119" n="e" s="T2473">döbər! </ts>
               <ts e="T2475" id="Seg_1121" n="e" s="T2474">Kanaʔ </ts>
               <ts e="T2476" id="Seg_1123" n="e" s="T2475">măna </ts>
               <ts e="T2477" id="Seg_1125" n="e" s="T2476">užu </ts>
               <ts e="T2478" id="Seg_1127" n="e" s="T2477">iʔ. </ts>
               <ts e="T2479" id="Seg_1129" n="e" s="T2478">Kunə </ts>
               <ts e="T2480" id="Seg_1131" n="e" s="T2479">(kunə), </ts>
               <ts e="T2481" id="Seg_1133" n="e" s="T2480">kunə </ts>
               <ts e="T2482" id="Seg_1135" n="e" s="T2481">kunə". </ts>
               <ts e="T2483" id="Seg_1137" n="e" s="T2482">Dĭ </ts>
               <ts e="T2484" id="Seg_1139" n="e" s="T2483">kambi. </ts>
               <ts e="T2485" id="Seg_1141" n="e" s="T2484">Ibi </ts>
               <ts e="T2486" id="Seg_1143" n="e" s="T2485">(uzu-) </ts>
               <ts e="T2487" id="Seg_1145" n="e" s="T2486">užu. </ts>
               <ts e="T2488" id="Seg_1147" n="e" s="T2487">Šonəga </ts>
               <ts e="T2489" id="Seg_1149" n="e" s="T2488">i </ts>
               <ts e="T2490" id="Seg_1151" n="e" s="T2489">sădărlaʔbə. </ts>
               <ts e="T2491" id="Seg_1153" n="e" s="T2490">To </ts>
               <ts e="T2492" id="Seg_1155" n="e" s="T2491">döbər, </ts>
               <ts e="T2493" id="Seg_1157" n="e" s="T2492">to </ts>
               <ts e="T2494" id="Seg_1159" n="e" s="T2493">döbər. </ts>
               <ts e="T2495" id="Seg_1161" n="e" s="T2494">To </ts>
               <ts e="T2496" id="Seg_1163" n="e" s="T2495">bögəlgəndə, </ts>
               <ts e="T2497" id="Seg_1165" n="e" s="T2496">to </ts>
               <ts e="T2498" id="Seg_1167" n="e" s="T2497">kadəlgəndə. </ts>
               <ts e="T2499" id="Seg_1169" n="e" s="T2498">Dĭgəttə </ts>
               <ts e="T2500" id="Seg_1171" n="e" s="T2499">dĭ </ts>
               <ts e="T2501" id="Seg_1173" n="e" s="T2500">kulaʔbə </ts>
               <ts e="T2502" id="Seg_1175" n="e" s="T2501">i </ts>
               <ts e="T2503" id="Seg_1177" n="e" s="T2502">kudonzlaʔbə. </ts>
               <ts e="T2504" id="Seg_1179" n="e" s="T2503">Măn </ts>
               <ts e="T2505" id="Seg_1181" n="e" s="T2504">mămbiam </ts>
               <ts e="T2506" id="Seg_1183" n="e" s="T2505">kunə, </ts>
               <ts e="T2507" id="Seg_1185" n="e" s="T2506">kunə, </ts>
               <ts e="T2508" id="Seg_1187" n="e" s="T2507">a </ts>
               <ts e="T2509" id="Seg_1189" n="e" s="T2508">tăn </ts>
               <ts e="T2510" id="Seg_1191" n="e" s="T2509">dö </ts>
               <ts e="T2511" id="Seg_1193" n="e" s="T2510">kadəlgəndə </ts>
               <ts e="T2512" id="Seg_1195" n="e" s="T2511">i </ts>
               <ts e="T2513" id="Seg_1197" n="e" s="T2512">bögəlgəndə </ts>
               <ts e="T2514" id="Seg_1199" n="e" s="T2513">ibiel. </ts>
               <ts e="T2515" id="Seg_1201" n="e" s="T2514">Aktʼa </ts>
               <ts e="T2516" id="Seg_1203" n="e" s="T2515">amga, </ts>
               <ts e="T2517" id="Seg_1205" n="e" s="T2516">nada </ts>
               <ts e="T2518" id="Seg_1207" n="e" s="T2517">(iššo= </ts>
               <ts e="T2519" id="Seg_1209" n="e" s="T2518">dil-) </ts>
               <ts e="T2520" id="Seg_1211" n="e" s="T2519">iššo </ts>
               <ts e="T2521" id="Seg_1213" n="e" s="T2520">dĭldʼi </ts>
               <ts e="T2522" id="Seg_1215" n="e" s="T2521">sumna". </ts>
               <ts e="T2523" id="Seg_1217" n="e" s="T2522">Dĭgəttə </ts>
               <ts e="T2524" id="Seg_1219" n="e" s="T2523">dĭ </ts>
               <ts e="T2525" id="Seg_1221" n="e" s="T2524">mĭbi </ts>
               <ts e="T2526" id="Seg_1223" n="e" s="T2525">(jemu=) </ts>
               <ts e="T2527" id="Seg_1225" n="e" s="T2526">dĭʔnə </ts>
               <ts e="T2528" id="Seg_1227" n="e" s="T2527">bieʔ. </ts>
               <ts e="T2529" id="Seg_1229" n="e" s="T2528">Dĭ </ts>
               <ts e="T2530" id="Seg_1231" n="e" s="T2529">kambi. </ts>
               <ts e="T2531" id="Seg_1233" n="e" s="T2530">Dĭ </ts>
               <ts e="T2532" id="Seg_1235" n="e" s="T2531">(užum=) </ts>
               <ts e="T2533" id="Seg_1237" n="e" s="T2532">dĭ </ts>
               <ts e="T2534" id="Seg_1239" n="e" s="T2533">že </ts>
               <ts e="T2535" id="Seg_1241" n="e" s="T2534">užum </ts>
               <ts e="T2536" id="Seg_1243" n="e" s="T2535">deʔpi. </ts>
               <ts e="T2537" id="Seg_1245" n="e" s="T2536">Kunə, </ts>
               <ts e="T2538" id="Seg_1247" n="e" s="T2537">kunə, </ts>
               <ts e="T2539" id="Seg_1249" n="e" s="T2538">(barəʔlaʔbə). </ts>
               <ts e="T2540" id="Seg_1251" n="e" s="T2539">"No, </ts>
               <ts e="T2541" id="Seg_1253" n="e" s="T2540">ugaːndə </ts>
               <ts e="T2542" id="Seg_1255" n="e" s="T2541">jakšə </ts>
               <ts e="T2543" id="Seg_1257" n="e" s="T2542">(uz-) </ts>
               <ts e="T2544" id="Seg_1259" n="e" s="T2543">užu". </ts>
               <ts e="T2545" id="Seg_1261" n="e" s="T2544">Dĭgəttə </ts>
               <ts e="T2546" id="Seg_1263" n="e" s="T2545">šerbi, </ts>
               <ts e="T2547" id="Seg_1265" n="e" s="T2546">dĭ </ts>
               <ts e="T2548" id="Seg_1267" n="e" s="T2547">măndə:" </ts>
               <ts e="T2549" id="Seg_1269" n="e" s="T2548">Mĭnzəreʔ </ts>
               <ts e="T2550" id="Seg_1271" n="e" s="T2549">kaməndə </ts>
               <ts e="T2551" id="Seg_1273" n="e" s="T2550">măn </ts>
               <ts e="T2552" id="Seg_1275" n="e" s="T2551">ej </ts>
               <ts e="T2553" id="Seg_1277" n="e" s="T2552">ambiam. </ts>
               <ts e="T2554" id="Seg_1279" n="e" s="T2553">"Dărə </ts>
               <ts e="T2555" id="Seg_1281" n="e" s="T2554">mĭnzəreʔ". </ts>
               <ts e="T2556" id="Seg_1283" n="e" s="T2555">Dĭ </ts>
               <ts e="T2557" id="Seg_1285" n="e" s="T2556">măndə:" </ts>
               <ts e="T2558" id="Seg_1287" n="e" s="T2557">ĭmbi </ts>
               <ts e="T2559" id="Seg_1289" n="e" s="T2558">mĭnzərzittə?" </ts>
               <ts e="T2560" id="Seg_1291" n="e" s="T2559">(Dĭʔnə=) </ts>
               <ts e="T2561" id="Seg_1293" n="e" s="T2560">Dĭn </ts>
               <ts e="T2562" id="Seg_1295" n="e" s="T2561">jamat </ts>
               <ts e="T2563" id="Seg_1297" n="e" s="T2562">ibi. </ts>
               <ts e="T2564" id="Seg_1299" n="e" s="T2563">(Aʔ-) </ts>
               <ts e="T2565" id="Seg_1301" n="e" s="T2564">Băʔpi. </ts>
               <ts e="T2566" id="Seg_1303" n="e" s="T2565">I </ts>
               <ts e="T2567" id="Seg_1305" n="e" s="T2566">mĭnzerbi, </ts>
               <ts e="T2568" id="Seg_1307" n="e" s="T2567">döbər </ts>
               <ts e="T2569" id="Seg_1309" n="e" s="T2568">munəj </ts>
               <ts e="T2570" id="Seg_1311" n="e" s="T2569">embi. </ts>
               <ts e="T2571" id="Seg_1313" n="e" s="T2570">Kajaʔ </ts>
               <ts e="T2572" id="Seg_1315" n="e" s="T2571">embi. </ts>
               <ts e="T2573" id="Seg_1317" n="e" s="T2572">(Lavlo-) </ts>
               <ts e="T0" id="Seg_1319" n="e" s="T2573">Lavrovɨj </ts>
               <ts e="T2574" id="Seg_1321" n="e" s="T0">lis </ts>
               <ts e="T2575" id="Seg_1323" n="e" s="T2574">embi. </ts>
               <ts e="T2576" id="Seg_1325" n="e" s="T2575">Dĭgəttə </ts>
               <ts e="T2577" id="Seg_1327" n="e" s="T2576">mĭnzərbi </ts>
               <ts e="T2578" id="Seg_1329" n="e" s="T2577">dĭ </ts>
               <ts e="T2579" id="Seg_1331" n="e" s="T2578">(amdə-) </ts>
               <ts e="T2580" id="Seg_1333" n="e" s="T2579">amnaʔbə </ts>
               <ts e="T2581" id="Seg_1335" n="e" s="T2580">da" </ts>
               <ts e="T2582" id="Seg_1337" n="e" s="T2581">Kaməndə </ts>
               <ts e="T2583" id="Seg_1339" n="e" s="T2582">dĭrgit </ts>
               <ts e="T2584" id="Seg_1341" n="e" s="T2583">ej </ts>
               <ts e="T2585" id="Seg_1343" n="e" s="T2584">ambiam". </ts>
               <ts e="T2586" id="Seg_1345" n="e" s="T2585">Dĭgəttə </ts>
               <ts e="T2587" id="Seg_1347" n="e" s="T2586">stal </ts>
               <ts e="T2588" id="Seg_1349" n="e" s="T2587">šerzittə </ts>
               <ts e="T2589" id="Seg_1351" n="e" s="T2588">jamaʔtə, </ts>
               <ts e="T2590" id="Seg_1353" n="e" s="T2589">a </ts>
               <ts e="T2591" id="Seg_1355" n="e" s="T2590">dĭn </ts>
               <ts e="T2592" id="Seg_1357" n="e" s="T2591">šiʔi </ts>
               <ts e="T2593" id="Seg_1359" n="e" s="T2592">molaːmbiʔi. </ts>
               <ts e="T2594" id="Seg_1361" n="e" s="T2593">Ujut </ts>
               <ts e="T2595" id="Seg_1363" n="e" s="T2594">döbər </ts>
               <ts e="T2596" id="Seg_1365" n="e" s="T2595">kambi. </ts>
               <ts e="T2597" id="Seg_1367" n="e" s="T2596">"A </ts>
               <ts e="T2598" id="Seg_1369" n="e" s="T2597">tăn </ts>
               <ts e="T2599" id="Seg_1371" n="e" s="T2598">tuj </ts>
               <ts e="T2600" id="Seg_1373" n="e" s="T2599">măndoʔ, </ts>
               <ts e="T2601" id="Seg_1375" n="e" s="T2600">măn </ts>
               <ts e="T2602" id="Seg_1377" n="e" s="T2601">girgit </ts>
               <ts e="T2603" id="Seg_1379" n="e" s="T2602">jamaʔi". </ts>
               <ts e="T2604" id="Seg_1381" n="e" s="T2603">"A </ts>
               <ts e="T2605" id="Seg_1383" n="e" s="T2604">tăn </ts>
               <ts e="T2606" id="Seg_1385" n="e" s="T2605">tujo </ts>
               <ts e="T2607" id="Seg_1387" n="e" s="T2606">ambial". </ts>
               <ts e="T2608" id="Seg_1389" n="e" s="T2607">Dĭgəttə </ts>
               <ts e="T2609" id="Seg_1391" n="e" s="T2608">kambiʔi </ts>
               <ts e="T2610" id="Seg_1393" n="e" s="T2609">jadajlaʔ </ts>
               <ts e="T2611" id="Seg_1395" n="e" s="T2610">ineziʔ. </ts>
               <ts e="T2612" id="Seg_1397" n="e" s="T2611">Dĭgəttə </ts>
               <ts e="T2613" id="Seg_1399" n="e" s="T2612">dĭn </ts>
               <ts e="T2614" id="Seg_1401" n="e" s="T2613">bü, </ts>
               <ts e="T2615" id="Seg_1403" n="e" s="T2614">bügən </ts>
               <ts e="T2616" id="Seg_1405" n="e" s="T2615">kandəgaʔi, </ts>
               <ts e="T2618" id="Seg_1407" n="e" s="T2616">dĭ… </ts>
               <ts e="T2620" id="Seg_1409" n="e" s="T2618">baza </ts>
               <ts e="T2621" id="Seg_1411" n="e" s="T2620">iluʔpi. </ts>
               <ts e="T2622" id="Seg_1413" n="e" s="T2621">Dĭ </ts>
               <ts e="T2623" id="Seg_1415" n="e" s="T2622">maːluʔpi. </ts>
               <ts e="T2624" id="Seg_1417" n="e" s="T2623">"Tăn </ts>
               <ts e="T2625" id="Seg_1419" n="e" s="T2624">măna </ts>
               <ts e="T2626" id="Seg_1421" n="e" s="T2625">iʔ </ts>
               <ts e="T2627" id="Seg_1423" n="e" s="T2626">mandəʔ!" </ts>
               <ts e="T2628" id="Seg_1425" n="e" s="T2627">I </ts>
               <ts e="T2629" id="Seg_1427" n="e" s="T2628">kirgarlaʔbə:" </ts>
               <ts e="T2630" id="Seg_1429" n="e" s="T2629">Măn </ts>
               <ts e="T2631" id="Seg_1431" n="e" s="T2630">maluʔpiam!" </ts>
               <ts e="T2632" id="Seg_1433" n="e" s="T2631">"A </ts>
               <ts e="T2633" id="Seg_1435" n="e" s="T2632">tăn </ts>
               <ts e="T2634" id="Seg_1437" n="e" s="T2633">măndərzittə </ts>
               <ts e="T2635" id="Seg_1439" n="e" s="T2634">măna </ts>
               <ts e="T2636" id="Seg_1441" n="e" s="T2635">nʼe </ts>
               <ts e="T2637" id="Seg_1443" n="e" s="T2636">velʼel, </ts>
               <ts e="T2638" id="Seg_1445" n="e" s="T2637">(amnolaʔbəʔ)". </ts>
               <ts e="T2639" id="Seg_1447" n="e" s="T2638">Bostə </ts>
               <ts e="T2640" id="Seg_1449" n="e" s="T2639">kalla </ts>
               <ts e="T2641" id="Seg_1451" n="e" s="T2640">dʼürbi, </ts>
               <ts e="T2642" id="Seg_1453" n="e" s="T2641">dĭ </ts>
               <ts e="T2643" id="Seg_1455" n="e" s="T2642">(dĭʔ-) </ts>
               <ts e="T2644" id="Seg_1457" n="e" s="T2643">tujo </ts>
               <ts e="T2645" id="Seg_1459" n="e" s="T2644">da </ts>
               <ts e="T2646" id="Seg_1461" n="e" s="T2645">dĭn </ts>
               <ts e="T2647" id="Seg_1463" n="e" s="T2646">amnolaʔbə. </ts>
               <ts e="T2648" id="Seg_1465" n="e" s="T2647">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2410" id="Seg_1466" s="T2406">PKZ_196X_SysojKosoj_flk.001 (001)</ta>
            <ta e="T2417" id="Seg_1467" s="T2410">PKZ_196X_SysojKosoj_flk.002 (002)</ta>
            <ta e="T2422" id="Seg_1468" s="T2417">PKZ_196X_SysojKosoj_flk.003 (003)</ta>
            <ta e="T2424" id="Seg_1469" s="T2422">PKZ_196X_SysojKosoj_flk.004 (004)</ta>
            <ta e="T2429" id="Seg_1470" s="T2424">PKZ_196X_SysojKosoj_flk.005 (005)</ta>
            <ta e="T2433" id="Seg_1471" s="T2429">PKZ_196X_SysojKosoj_flk.006 (006)</ta>
            <ta e="T2435" id="Seg_1472" s="T2433">PKZ_196X_SysojKosoj_flk.007 (007)</ta>
            <ta e="T2437" id="Seg_1473" s="T2435">PKZ_196X_SysojKosoj_flk.008 (008)</ta>
            <ta e="T2443" id="Seg_1474" s="T2437">PKZ_196X_SysojKosoj_flk.009 (009)</ta>
            <ta e="T2452" id="Seg_1475" s="T2443">PKZ_196X_SysojKosoj_flk.010 (010)</ta>
            <ta e="T2456" id="Seg_1476" s="T2452">PKZ_196X_SysojKosoj_flk.011 (011)</ta>
            <ta e="T2459" id="Seg_1477" s="T2456">PKZ_196X_SysojKosoj_flk.012 (012)</ta>
            <ta e="T2461" id="Seg_1478" s="T2459">PKZ_196X_SysojKosoj_flk.013 (013)</ta>
            <ta e="T2464" id="Seg_1479" s="T2461">PKZ_196X_SysojKosoj_flk.014 (014)</ta>
            <ta e="T2468" id="Seg_1480" s="T2464">PKZ_196X_SysojKosoj_flk.015 (015)</ta>
            <ta e="T2474" id="Seg_1481" s="T2468">PKZ_196X_SysojKosoj_flk.016 (016)</ta>
            <ta e="T2478" id="Seg_1482" s="T2474">PKZ_196X_SysojKosoj_flk.017 (017)</ta>
            <ta e="T2482" id="Seg_1483" s="T2478">PKZ_196X_SysojKosoj_flk.018 (018)</ta>
            <ta e="T2484" id="Seg_1484" s="T2482">PKZ_196X_SysojKosoj_flk.019 (019)</ta>
            <ta e="T2487" id="Seg_1485" s="T2484">PKZ_196X_SysojKosoj_flk.020 (020)</ta>
            <ta e="T2490" id="Seg_1486" s="T2487">PKZ_196X_SysojKosoj_flk.021 (021)</ta>
            <ta e="T2494" id="Seg_1487" s="T2490">PKZ_196X_SysojKosoj_flk.022 (022)</ta>
            <ta e="T2498" id="Seg_1488" s="T2494">PKZ_196X_SysojKosoj_flk.023 (023)</ta>
            <ta e="T2503" id="Seg_1489" s="T2498">PKZ_196X_SysojKosoj_flk.024 (024)</ta>
            <ta e="T2514" id="Seg_1490" s="T2503">PKZ_196X_SysojKosoj_flk.025 (025)</ta>
            <ta e="T2522" id="Seg_1491" s="T2514">PKZ_196X_SysojKosoj_flk.026 (026)</ta>
            <ta e="T2528" id="Seg_1492" s="T2522">PKZ_196X_SysojKosoj_flk.027 (027)</ta>
            <ta e="T2530" id="Seg_1493" s="T2528">PKZ_196X_SysojKosoj_flk.028 (028)</ta>
            <ta e="T2536" id="Seg_1494" s="T2530">PKZ_196X_SysojKosoj_flk.029 (029)</ta>
            <ta e="T2539" id="Seg_1495" s="T2536">PKZ_196X_SysojKosoj_flk.030 (030)</ta>
            <ta e="T2544" id="Seg_1496" s="T2539">PKZ_196X_SysojKosoj_flk.031 (031)</ta>
            <ta e="T2553" id="Seg_1497" s="T2544">PKZ_196X_SysojKosoj_flk.032 (032)</ta>
            <ta e="T2555" id="Seg_1498" s="T2553">PKZ_196X_SysojKosoj_flk.033 (033)</ta>
            <ta e="T2559" id="Seg_1499" s="T2555">PKZ_196X_SysojKosoj_flk.034 (034)</ta>
            <ta e="T2563" id="Seg_1500" s="T2559">PKZ_196X_SysojKosoj_flk.035 (035)</ta>
            <ta e="T2565" id="Seg_1501" s="T2563">PKZ_196X_SysojKosoj_flk.036 (036)</ta>
            <ta e="T2570" id="Seg_1502" s="T2565">PKZ_196X_SysojKosoj_flk.037 (037)</ta>
            <ta e="T2572" id="Seg_1503" s="T2570">PKZ_196X_SysojKosoj_flk.038 (038)</ta>
            <ta e="T2575" id="Seg_1504" s="T2572">PKZ_196X_SysojKosoj_flk.039 (039)</ta>
            <ta e="T2585" id="Seg_1505" s="T2575">PKZ_196X_SysojKosoj_flk.040 (040)</ta>
            <ta e="T2593" id="Seg_1506" s="T2585">PKZ_196X_SysojKosoj_flk.041 (041)</ta>
            <ta e="T2596" id="Seg_1507" s="T2593">PKZ_196X_SysojKosoj_flk.042 (042)</ta>
            <ta e="T2603" id="Seg_1508" s="T2596">PKZ_196X_SysojKosoj_flk.043 (043)</ta>
            <ta e="T2607" id="Seg_1509" s="T2603">PKZ_196X_SysojKosoj_flk.044 (044)</ta>
            <ta e="T2611" id="Seg_1510" s="T2607">PKZ_196X_SysojKosoj_flk.045 (045)</ta>
            <ta e="T2618" id="Seg_1511" s="T2611">PKZ_196X_SysojKosoj_flk.046 (046)</ta>
            <ta e="T2621" id="Seg_1512" s="T2618">PKZ_196X_SysojKosoj_flk.047 (047)</ta>
            <ta e="T2623" id="Seg_1513" s="T2621">PKZ_196X_SysojKosoj_flk.048 (048)</ta>
            <ta e="T2627" id="Seg_1514" s="T2623">PKZ_196X_SysojKosoj_flk.049 (049)</ta>
            <ta e="T2631" id="Seg_1515" s="T2627">PKZ_196X_SysojKosoj_flk.050 (050)</ta>
            <ta e="T2638" id="Seg_1516" s="T2631">PKZ_196X_SysojKosoj_flk.051 (051)</ta>
            <ta e="T2647" id="Seg_1517" s="T2638">PKZ_196X_SysojKosoj_flk.052 (052)</ta>
            <ta e="T2648" id="Seg_1518" s="T2647">PKZ_196X_SysojKosoj_flk.053 (053)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2410" id="Seg_1519" s="T2406">Amnobi onʼiʔ (kuz-) kuza. </ta>
            <ta e="T2417" id="Seg_1520" s="T2410">Dĭʔnə šobi kuza (m- məj-) najmɨ izittə. </ta>
            <ta e="T2422" id="Seg_1521" s="T2417">(Kăde-) (Kădəʔ) tănan (ka-) kăštəliaʔi? </ta>
            <ta e="T2424" id="Seg_1522" s="T2422">(Si-) Sɨsoj Kasoj. </ta>
            <ta e="T2429" id="Seg_1523" s="T2424">Dĭ măndə: "(m-) No, amnoʔ. </ta>
            <ta e="T2433" id="Seg_1524" s="T2429">Mĭnzəreʔ măna šide munəj". </ta>
            <ta e="T2435" id="Seg_1525" s="T2433">Dĭ mĭnzərbi. </ta>
            <ta e="T2437" id="Seg_1526" s="T2435">Mĭnzərbi, mĭnzərbi. </ta>
            <ta e="T2443" id="Seg_1527" s="T2437">Ibi da onʼiʔ amnuʔpi, onʼiʔ detleʔbə. </ta>
            <ta e="T2452" id="Seg_1528" s="T2443">"Ĭmbi tăn onʼiʔ deʔpiel, măn šide (mă-) mămbiam mĭnzereʔ!" </ta>
            <ta e="T2456" id="Seg_1529" s="T2452">"A măn onʼiʔ ambiam". </ta>
            <ta e="T2459" id="Seg_1530" s="T2456">Dĭgəttə dĭ kudonzlubi. </ta>
            <ta e="T2461" id="Seg_1531" s="T2459">"Kanaʔ döʔə!" </ta>
            <ta e="T2464" id="Seg_1532" s="T2461">Dĭ (k-) kambi. </ta>
            <ta e="T2468" id="Seg_1533" s="T2464">I kanzittə xatʼel dĭʔə. </ta>
            <ta e="T2474" id="Seg_1534" s="T2468">(Dĭ-) Dĭgəttə dĭ kirgarlaʔbə:" Šoʔ döbər! </ta>
            <ta e="T2478" id="Seg_1535" s="T2474">Kanaʔ măna užu iʔ. </ta>
            <ta e="T2482" id="Seg_1536" s="T2478">Kunə (kunə), kunə kunə". </ta>
            <ta e="T2484" id="Seg_1537" s="T2482">Dĭ kambi. </ta>
            <ta e="T2487" id="Seg_1538" s="T2484">Ibi (uzu-) užu. </ta>
            <ta e="T2490" id="Seg_1539" s="T2487">Šonəga i sădărlaʔbə. </ta>
            <ta e="T2494" id="Seg_1540" s="T2490">To döbər, to döbər. </ta>
            <ta e="T2498" id="Seg_1541" s="T2494">((NOISE)) To bögəlgəndə, to kadəlgəndə. </ta>
            <ta e="T2503" id="Seg_1542" s="T2498">Dĭgəttə dĭ kulaʔbə i kudonzlaʔbə. </ta>
            <ta e="T2514" id="Seg_1543" s="T2503">Măn mămbiam kunə, kunə, a tăn dö kadəlgəndə i bögəlgəndə ibiel. </ta>
            <ta e="T2522" id="Seg_1544" s="T2514">Aktʼa amga, nada (iššo= dil-) iššo dĭldʼi sumna". </ta>
            <ta e="T2528" id="Seg_1545" s="T2522">Dĭgəttə dĭ mĭbi (jemu=) dĭʔnə bieʔ. </ta>
            <ta e="T2530" id="Seg_1546" s="T2528">Dĭ kambi. </ta>
            <ta e="T2536" id="Seg_1547" s="T2530">Dĭ (užum=) dĭ že užum deʔpi. </ta>
            <ta e="T2539" id="Seg_1548" s="T2536">Kunə, kunə, (barəʔlaʔbə). </ta>
            <ta e="T2544" id="Seg_1549" s="T2539">"No, ugaːndə jakšə (uz-) užu". </ta>
            <ta e="T2553" id="Seg_1550" s="T2544">Dĭgəttə šerbi, dĭ măndə:" Mĭnzəreʔ kaməndə măn ej ambiam. </ta>
            <ta e="T2555" id="Seg_1551" s="T2553">"Dărə mĭnzəreʔ". </ta>
            <ta e="T2559" id="Seg_1552" s="T2555">Dĭ măndə:" ĭmbi mĭnzərzittə?" </ta>
            <ta e="T2563" id="Seg_1553" s="T2559">(Dĭʔnə=) Dĭn jamat ibi. </ta>
            <ta e="T2565" id="Seg_1554" s="T2563">(Aʔ-) Băʔpi. </ta>
            <ta e="T2570" id="Seg_1555" s="T2565">I mĭnzerbi, döbər munəj embi. </ta>
            <ta e="T2572" id="Seg_1556" s="T2570">Kajaʔ embi. </ta>
            <ta e="T2575" id="Seg_1557" s="T2572">(Lavlo-) Lavrovɨj lis embi. </ta>
            <ta e="T2585" id="Seg_1558" s="T2575">Dĭgəttə mĭnzərbi dĭ (amdə-) amnaʔbə da" Kaməndə dĭrgit ej ambiam". </ta>
            <ta e="T2593" id="Seg_1559" s="T2585">Dĭgəttə stal šerzittə jamaʔtə, a dĭn šiʔi molaːmbiʔi. </ta>
            <ta e="T2596" id="Seg_1560" s="T2593">Ujut döbər kambi. </ta>
            <ta e="T2603" id="Seg_1561" s="T2596">"A tăn tuj măndoʔ, măn girgit jamaʔi". </ta>
            <ta e="T2607" id="Seg_1562" s="T2603">"A tăn tujo ambial". </ta>
            <ta e="T2611" id="Seg_1563" s="T2607">Dĭgəttə kambiʔi jadajlaʔ ineziʔ. </ta>
            <ta e="T2618" id="Seg_1564" s="T2611">Dĭgəttə dĭn bü, bügən kandəgaʔi, dĭ… </ta>
            <ta e="T2621" id="Seg_1565" s="T2618">baza iluʔpi. </ta>
            <ta e="T2623" id="Seg_1566" s="T2621">Dĭ maːluʔpi. </ta>
            <ta e="T2627" id="Seg_1567" s="T2623">"Tăn măna iʔ mandəʔ!" </ta>
            <ta e="T2631" id="Seg_1568" s="T2627">I kirgarlaʔbə:" Măn maluʔpiam!" </ta>
            <ta e="T2638" id="Seg_1569" s="T2631">"A tăn măndərzittə măna nʼe velʼel, (amnolaʔbəʔ)". </ta>
            <ta e="T2647" id="Seg_1570" s="T2638">Bostə kalla dʼürbi, dĭ (dĭʔ-) tujo da dĭn amnolaʔbə. </ta>
            <ta e="T2648" id="Seg_1571" s="T2647">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2407" id="Seg_1572" s="T2406">amno-bi</ta>
            <ta e="T2408" id="Seg_1573" s="T2407">onʼiʔ</ta>
            <ta e="T2410" id="Seg_1574" s="T2409">kuza</ta>
            <ta e="T2411" id="Seg_1575" s="T2410">dĭʔ-nə</ta>
            <ta e="T2412" id="Seg_1576" s="T2411">šo-bi</ta>
            <ta e="T2413" id="Seg_1577" s="T2412">kuza</ta>
            <ta e="T2416" id="Seg_1578" s="T2415">najmɨ</ta>
            <ta e="T2417" id="Seg_1579" s="T2416">i-zittə</ta>
            <ta e="T2419" id="Seg_1580" s="T2418">kădəʔ</ta>
            <ta e="T2420" id="Seg_1581" s="T2419">tănan</ta>
            <ta e="T2422" id="Seg_1582" s="T2421">kăštə-lia-ʔi</ta>
            <ta e="T2424" id="Seg_1583" s="T2423">Sɨsoj_Kasoj</ta>
            <ta e="T2425" id="Seg_1584" s="T2424">dĭ</ta>
            <ta e="T2426" id="Seg_1585" s="T2425">măn-də</ta>
            <ta e="T2428" id="Seg_1586" s="T2427">no</ta>
            <ta e="T2429" id="Seg_1587" s="T2428">amno-ʔ</ta>
            <ta e="T2430" id="Seg_1588" s="T2429">mĭnzər-e-ʔ</ta>
            <ta e="T2431" id="Seg_1589" s="T2430">măna</ta>
            <ta e="T2432" id="Seg_1590" s="T2431">šide</ta>
            <ta e="T2433" id="Seg_1591" s="T2432">munəj</ta>
            <ta e="T2434" id="Seg_1592" s="T2433">dĭ</ta>
            <ta e="T2435" id="Seg_1593" s="T2434">mĭnzər-bi</ta>
            <ta e="T2436" id="Seg_1594" s="T2435">mĭnzər-bi</ta>
            <ta e="T2437" id="Seg_1595" s="T2436">mĭnzər-bi</ta>
            <ta e="T2438" id="Seg_1596" s="T2437">i-bi</ta>
            <ta e="T2439" id="Seg_1597" s="T2438">da</ta>
            <ta e="T2440" id="Seg_1598" s="T2439">onʼiʔ</ta>
            <ta e="T2441" id="Seg_1599" s="T2440">am-nuʔ-pi</ta>
            <ta e="T2442" id="Seg_1600" s="T2441">onʼiʔ</ta>
            <ta e="T2443" id="Seg_1601" s="T2442">det-leʔbə</ta>
            <ta e="T2444" id="Seg_1602" s="T2443">ĭmbi</ta>
            <ta e="T2445" id="Seg_1603" s="T2444">tăn</ta>
            <ta e="T2446" id="Seg_1604" s="T2445">onʼiʔ</ta>
            <ta e="T2447" id="Seg_1605" s="T2446">deʔ-pie-l</ta>
            <ta e="T2448" id="Seg_1606" s="T2447">măn</ta>
            <ta e="T2449" id="Seg_1607" s="T2448">šide</ta>
            <ta e="T2451" id="Seg_1608" s="T2450">măm-bia-m</ta>
            <ta e="T2452" id="Seg_1609" s="T2451">mĭnzer-e-ʔ</ta>
            <ta e="T2453" id="Seg_1610" s="T2452">a</ta>
            <ta e="T2454" id="Seg_1611" s="T2453">măn</ta>
            <ta e="T2455" id="Seg_1612" s="T2454">onʼiʔ</ta>
            <ta e="T2456" id="Seg_1613" s="T2455">am-bia-m</ta>
            <ta e="T2457" id="Seg_1614" s="T2456">dĭgəttə</ta>
            <ta e="T2458" id="Seg_1615" s="T2457">dĭ</ta>
            <ta e="T2459" id="Seg_1616" s="T2458">kudo-nzlu-bi</ta>
            <ta e="T2460" id="Seg_1617" s="T2459">kan-a-ʔ</ta>
            <ta e="T2461" id="Seg_1618" s="T2460">döʔə</ta>
            <ta e="T2462" id="Seg_1619" s="T2461">dĭ</ta>
            <ta e="T2464" id="Seg_1620" s="T2463">kam-bi</ta>
            <ta e="T2465" id="Seg_1621" s="T2464">i</ta>
            <ta e="T2466" id="Seg_1622" s="T2465">kan-zittə</ta>
            <ta e="T2467" id="Seg_1623" s="T2466">xatʼel</ta>
            <ta e="T2468" id="Seg_1624" s="T2467">dĭʔə</ta>
            <ta e="T2470" id="Seg_1625" s="T2469">dĭgəttə</ta>
            <ta e="T2471" id="Seg_1626" s="T2470">dĭ</ta>
            <ta e="T2472" id="Seg_1627" s="T2471">kirgar-laʔbə</ta>
            <ta e="T2473" id="Seg_1628" s="T2472">šo-ʔ</ta>
            <ta e="T2474" id="Seg_1629" s="T2473">döbər</ta>
            <ta e="T2475" id="Seg_1630" s="T2474">kan-a-ʔ</ta>
            <ta e="T2476" id="Seg_1631" s="T2475">măna</ta>
            <ta e="T2477" id="Seg_1632" s="T2476">užu</ta>
            <ta e="T2478" id="Seg_1633" s="T2477">i-ʔ</ta>
            <ta e="T2479" id="Seg_1634" s="T2478">ku-nə</ta>
            <ta e="T2480" id="Seg_1635" s="T2479">ku-nə</ta>
            <ta e="T2481" id="Seg_1636" s="T2480">ku-nə</ta>
            <ta e="T2482" id="Seg_1637" s="T2481">ku-nə</ta>
            <ta e="T2483" id="Seg_1638" s="T2482">dĭ</ta>
            <ta e="T2484" id="Seg_1639" s="T2483">kam-bi</ta>
            <ta e="T2485" id="Seg_1640" s="T2484">i-bi</ta>
            <ta e="T2487" id="Seg_1641" s="T2486">užu</ta>
            <ta e="T2488" id="Seg_1642" s="T2487">šonə-ga</ta>
            <ta e="T2489" id="Seg_1643" s="T2488">i</ta>
            <ta e="T2490" id="Seg_1644" s="T2489">sădăr-laʔbə</ta>
            <ta e="T2491" id="Seg_1645" s="T2490">to</ta>
            <ta e="T2492" id="Seg_1646" s="T2491">döbər</ta>
            <ta e="T2493" id="Seg_1647" s="T2492">to</ta>
            <ta e="T2494" id="Seg_1648" s="T2493">döbər</ta>
            <ta e="T2495" id="Seg_1649" s="T2494">to</ta>
            <ta e="T2496" id="Seg_1650" s="T2495">bögəl-gəndə</ta>
            <ta e="T2497" id="Seg_1651" s="T2496">to</ta>
            <ta e="T2498" id="Seg_1652" s="T2497">kadəl-gəndə</ta>
            <ta e="T2499" id="Seg_1653" s="T2498">dĭgəttə</ta>
            <ta e="T2500" id="Seg_1654" s="T2499">dĭ</ta>
            <ta e="T2501" id="Seg_1655" s="T2500">ku-laʔbə</ta>
            <ta e="T2502" id="Seg_1656" s="T2501">i</ta>
            <ta e="T2503" id="Seg_1657" s="T2502">kudo-nz-laʔbə</ta>
            <ta e="T2504" id="Seg_1658" s="T2503">măn</ta>
            <ta e="T2505" id="Seg_1659" s="T2504">măm-bia-m</ta>
            <ta e="T2506" id="Seg_1660" s="T2505">ku-nə</ta>
            <ta e="T2507" id="Seg_1661" s="T2506">ku-nə</ta>
            <ta e="T2508" id="Seg_1662" s="T2507">a</ta>
            <ta e="T2509" id="Seg_1663" s="T2508">tăn</ta>
            <ta e="T2510" id="Seg_1664" s="T2509">dö</ta>
            <ta e="T2511" id="Seg_1665" s="T2510">kadəl-gəndə</ta>
            <ta e="T2512" id="Seg_1666" s="T2511">i</ta>
            <ta e="T2513" id="Seg_1667" s="T2512">bögəl-gəndə</ta>
            <ta e="T2514" id="Seg_1668" s="T2513">i-bie-l</ta>
            <ta e="T2515" id="Seg_1669" s="T2514">aktʼa</ta>
            <ta e="T2516" id="Seg_1670" s="T2515">amga</ta>
            <ta e="T2517" id="Seg_1671" s="T2516">nada</ta>
            <ta e="T2518" id="Seg_1672" s="T2517">iššo</ta>
            <ta e="T2520" id="Seg_1673" s="T2519">iššo</ta>
            <ta e="T2521" id="Seg_1674" s="T2520">dĭldʼi</ta>
            <ta e="T2522" id="Seg_1675" s="T2521">sumna</ta>
            <ta e="T2523" id="Seg_1676" s="T2522">dĭgəttə</ta>
            <ta e="T2524" id="Seg_1677" s="T2523">dĭ</ta>
            <ta e="T2525" id="Seg_1678" s="T2524">mĭ-bi</ta>
            <ta e="T2527" id="Seg_1679" s="T2526">dĭʔ-nə</ta>
            <ta e="T2528" id="Seg_1680" s="T2527">bieʔ</ta>
            <ta e="T2529" id="Seg_1681" s="T2528">dĭ</ta>
            <ta e="T2530" id="Seg_1682" s="T2529">kam-bi</ta>
            <ta e="T2531" id="Seg_1683" s="T2530">dĭ</ta>
            <ta e="T2532" id="Seg_1684" s="T2531">užu-m</ta>
            <ta e="T2533" id="Seg_1685" s="T2532">dĭ</ta>
            <ta e="T2534" id="Seg_1686" s="T2533">že</ta>
            <ta e="T2535" id="Seg_1687" s="T2534">užu-m</ta>
            <ta e="T2536" id="Seg_1688" s="T2535">deʔ-pi</ta>
            <ta e="T2537" id="Seg_1689" s="T2536">ku-nə</ta>
            <ta e="T2538" id="Seg_1690" s="T2537">ku-nə</ta>
            <ta e="T2539" id="Seg_1691" s="T2538">barəʔ-laʔbə</ta>
            <ta e="T2540" id="Seg_1692" s="T2539">no</ta>
            <ta e="T2541" id="Seg_1693" s="T2540">ugaːndə</ta>
            <ta e="T2542" id="Seg_1694" s="T2541">jakšə</ta>
            <ta e="T2544" id="Seg_1695" s="T2543">užu</ta>
            <ta e="T2545" id="Seg_1696" s="T2544">dĭgəttə</ta>
            <ta e="T2546" id="Seg_1697" s="T2545">šer-bi</ta>
            <ta e="T2547" id="Seg_1698" s="T2546">dĭ</ta>
            <ta e="T2548" id="Seg_1699" s="T2547">măn-də</ta>
            <ta e="T2549" id="Seg_1700" s="T2548">mĭnzər-e-ʔ</ta>
            <ta e="T2550" id="Seg_1701" s="T2549">kamən=də</ta>
            <ta e="T2551" id="Seg_1702" s="T2550">măn</ta>
            <ta e="T2552" id="Seg_1703" s="T2551">ej</ta>
            <ta e="T2553" id="Seg_1704" s="T2552">am-bia-m</ta>
            <ta e="T2554" id="Seg_1705" s="T2553">dărə</ta>
            <ta e="T2555" id="Seg_1706" s="T2554">mĭnzər-e-ʔ</ta>
            <ta e="T2556" id="Seg_1707" s="T2555">dĭ</ta>
            <ta e="T2557" id="Seg_1708" s="T2556">măn-də</ta>
            <ta e="T2558" id="Seg_1709" s="T2557">ĭmbi</ta>
            <ta e="T2559" id="Seg_1710" s="T2558">mĭnzər-zittə</ta>
            <ta e="T2560" id="Seg_1711" s="T2559">dĭʔ-nə</ta>
            <ta e="T2561" id="Seg_1712" s="T2560">dĭn</ta>
            <ta e="T2562" id="Seg_1713" s="T2561">jama-t</ta>
            <ta e="T2563" id="Seg_1714" s="T2562">i-bi</ta>
            <ta e="T2565" id="Seg_1715" s="T2564">băʔ-pi</ta>
            <ta e="T2566" id="Seg_1716" s="T2565">i</ta>
            <ta e="T2567" id="Seg_1717" s="T2566">mĭnzer-bi</ta>
            <ta e="T2568" id="Seg_1718" s="T2567">döbər</ta>
            <ta e="T2569" id="Seg_1719" s="T2568">munəj</ta>
            <ta e="T2570" id="Seg_1720" s="T2569">em-bi</ta>
            <ta e="T2571" id="Seg_1721" s="T2570">kajaʔ</ta>
            <ta e="T2572" id="Seg_1722" s="T2571">em-bi</ta>
            <ta e="T0" id="Seg_1723" s="T2573">lavrovɨj</ta>
            <ta e="T2574" id="Seg_1724" s="T0">lis</ta>
            <ta e="T2575" id="Seg_1725" s="T2574">em-bi</ta>
            <ta e="T2576" id="Seg_1726" s="T2575">dĭgəttə</ta>
            <ta e="T2577" id="Seg_1727" s="T2576">mĭnzər-bi</ta>
            <ta e="T2578" id="Seg_1728" s="T2577">dĭ</ta>
            <ta e="T2580" id="Seg_1729" s="T2579">am-naʔbə</ta>
            <ta e="T2581" id="Seg_1730" s="T2580">da</ta>
            <ta e="T2582" id="Seg_1731" s="T2581">kamən=də</ta>
            <ta e="T2583" id="Seg_1732" s="T2582">dĭrgit</ta>
            <ta e="T2584" id="Seg_1733" s="T2583">ej</ta>
            <ta e="T2585" id="Seg_1734" s="T2584">am-bia-m</ta>
            <ta e="T2586" id="Seg_1735" s="T2585">dĭgəttə</ta>
            <ta e="T2588" id="Seg_1736" s="T2587">šer-zittə</ta>
            <ta e="T2589" id="Seg_1737" s="T2588">jama-ʔ-tə</ta>
            <ta e="T2590" id="Seg_1738" s="T2589">a</ta>
            <ta e="T2591" id="Seg_1739" s="T2590">dĭn</ta>
            <ta e="T2592" id="Seg_1740" s="T2591">ši-ʔi</ta>
            <ta e="T2593" id="Seg_1741" s="T2592">mo-laːm-bi-ʔi</ta>
            <ta e="T2594" id="Seg_1742" s="T2593">uju-t</ta>
            <ta e="T2595" id="Seg_1743" s="T2594">döbər</ta>
            <ta e="T2596" id="Seg_1744" s="T2595">kam-bi</ta>
            <ta e="T2597" id="Seg_1745" s="T2596">a</ta>
            <ta e="T2598" id="Seg_1746" s="T2597">tăn</ta>
            <ta e="T2599" id="Seg_1747" s="T2598">tuj</ta>
            <ta e="T2600" id="Seg_1748" s="T2599">măndo-ʔ</ta>
            <ta e="T2601" id="Seg_1749" s="T2600">măn</ta>
            <ta e="T2602" id="Seg_1750" s="T2601">girgit</ta>
            <ta e="T2603" id="Seg_1751" s="T2602">jama-ʔi</ta>
            <ta e="T2604" id="Seg_1752" s="T2603">a</ta>
            <ta e="T2605" id="Seg_1753" s="T2604">tăn</ta>
            <ta e="T2606" id="Seg_1754" s="T2605">tujo</ta>
            <ta e="T2607" id="Seg_1755" s="T2606">am-bia-l</ta>
            <ta e="T2608" id="Seg_1756" s="T2607">dĭgəttə</ta>
            <ta e="T2609" id="Seg_1757" s="T2608">kam-bi-ʔi</ta>
            <ta e="T2610" id="Seg_1758" s="T2609">jada-j-laʔ</ta>
            <ta e="T2611" id="Seg_1759" s="T2610">ine-ziʔ</ta>
            <ta e="T2612" id="Seg_1760" s="T2611">dĭgəttə</ta>
            <ta e="T2613" id="Seg_1761" s="T2612">dĭn</ta>
            <ta e="T2614" id="Seg_1762" s="T2613">bü</ta>
            <ta e="T2615" id="Seg_1763" s="T2614">bü-gən</ta>
            <ta e="T2616" id="Seg_1764" s="T2615">kan-də-ga-ʔi</ta>
            <ta e="T2618" id="Seg_1765" s="T2616">dĭ</ta>
            <ta e="T2620" id="Seg_1766" s="T2618">baza</ta>
            <ta e="T2621" id="Seg_1767" s="T2620">i-luʔ-pi</ta>
            <ta e="T2622" id="Seg_1768" s="T2621">dĭ</ta>
            <ta e="T2623" id="Seg_1769" s="T2622">ma-luʔ-pi</ta>
            <ta e="T2624" id="Seg_1770" s="T2623">tăn</ta>
            <ta e="T2625" id="Seg_1771" s="T2624">măna</ta>
            <ta e="T2626" id="Seg_1772" s="T2625">i-ʔ</ta>
            <ta e="T2627" id="Seg_1773" s="T2626">mandə-ʔ</ta>
            <ta e="T2628" id="Seg_1774" s="T2627">i</ta>
            <ta e="T2629" id="Seg_1775" s="T2628">kirgar-laʔbə</ta>
            <ta e="T2630" id="Seg_1776" s="T2629">măn</ta>
            <ta e="T2631" id="Seg_1777" s="T2630">ma-luʔ-pia-m</ta>
            <ta e="T2632" id="Seg_1778" s="T2631">a</ta>
            <ta e="T2633" id="Seg_1779" s="T2632">tăn</ta>
            <ta e="T2634" id="Seg_1780" s="T2633">măndə-r-zittə</ta>
            <ta e="T2635" id="Seg_1781" s="T2634">măna</ta>
            <ta e="T2636" id="Seg_1782" s="T2635">nʼe</ta>
            <ta e="T2638" id="Seg_1783" s="T2637">amno-laʔbə-ʔ</ta>
            <ta e="T2639" id="Seg_1784" s="T2638">bos-tə</ta>
            <ta e="T2640" id="Seg_1785" s="T2639">kal-la</ta>
            <ta e="T2641" id="Seg_1786" s="T2640">dʼür-bi</ta>
            <ta e="T2642" id="Seg_1787" s="T2641">dĭ</ta>
            <ta e="T2644" id="Seg_1788" s="T2643">tujo</ta>
            <ta e="T2645" id="Seg_1789" s="T2644">da</ta>
            <ta e="T2646" id="Seg_1790" s="T2645">dĭn</ta>
            <ta e="T2647" id="Seg_1791" s="T2646">amno-laʔbə</ta>
            <ta e="T2648" id="Seg_1792" s="T2647">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2407" id="Seg_1793" s="T2406">amno-bi</ta>
            <ta e="T2408" id="Seg_1794" s="T2407">onʼiʔ</ta>
            <ta e="T2410" id="Seg_1795" s="T2409">kuza</ta>
            <ta e="T2411" id="Seg_1796" s="T2410">dĭ-Tə</ta>
            <ta e="T2412" id="Seg_1797" s="T2411">šo-bi</ta>
            <ta e="T2413" id="Seg_1798" s="T2412">kuza</ta>
            <ta e="T2416" id="Seg_1799" s="T2415">najmɨ</ta>
            <ta e="T2417" id="Seg_1800" s="T2416">i-zittə</ta>
            <ta e="T2419" id="Seg_1801" s="T2418">kădaʔ</ta>
            <ta e="T2420" id="Seg_1802" s="T2419">tănan</ta>
            <ta e="T2422" id="Seg_1803" s="T2421">kăštə-liA-jəʔ</ta>
            <ta e="T2424" id="Seg_1804" s="T2423">Sɨsoj_Kasoj</ta>
            <ta e="T2425" id="Seg_1805" s="T2424">dĭ</ta>
            <ta e="T2426" id="Seg_1806" s="T2425">măn-ntə</ta>
            <ta e="T2428" id="Seg_1807" s="T2427">no</ta>
            <ta e="T2429" id="Seg_1808" s="T2428">amno-ʔ</ta>
            <ta e="T2430" id="Seg_1809" s="T2429">mĭnzər-ə-ʔ</ta>
            <ta e="T2431" id="Seg_1810" s="T2430">măna</ta>
            <ta e="T2432" id="Seg_1811" s="T2431">šide</ta>
            <ta e="T2433" id="Seg_1812" s="T2432">munəj</ta>
            <ta e="T2434" id="Seg_1813" s="T2433">dĭ</ta>
            <ta e="T2435" id="Seg_1814" s="T2434">mĭnzər-bi</ta>
            <ta e="T2436" id="Seg_1815" s="T2435">mĭnzər-bi</ta>
            <ta e="T2437" id="Seg_1816" s="T2436">mĭnzər-bi</ta>
            <ta e="T2438" id="Seg_1817" s="T2437">i-bi</ta>
            <ta e="T2439" id="Seg_1818" s="T2438">da</ta>
            <ta e="T2440" id="Seg_1819" s="T2439">onʼiʔ</ta>
            <ta e="T2441" id="Seg_1820" s="T2440">am-luʔbdə-bi</ta>
            <ta e="T2442" id="Seg_1821" s="T2441">onʼiʔ</ta>
            <ta e="T2443" id="Seg_1822" s="T2442">det-laʔbə</ta>
            <ta e="T2444" id="Seg_1823" s="T2443">ĭmbi</ta>
            <ta e="T2445" id="Seg_1824" s="T2444">tăn</ta>
            <ta e="T2446" id="Seg_1825" s="T2445">onʼiʔ</ta>
            <ta e="T2447" id="Seg_1826" s="T2446">det-bi-l</ta>
            <ta e="T2448" id="Seg_1827" s="T2447">măn</ta>
            <ta e="T2449" id="Seg_1828" s="T2448">šide</ta>
            <ta e="T2451" id="Seg_1829" s="T2450">măn-bi-m</ta>
            <ta e="T2452" id="Seg_1830" s="T2451">mĭnzər-ə-ʔ</ta>
            <ta e="T2453" id="Seg_1831" s="T2452">a</ta>
            <ta e="T2454" id="Seg_1832" s="T2453">măn</ta>
            <ta e="T2455" id="Seg_1833" s="T2454">onʼiʔ</ta>
            <ta e="T2456" id="Seg_1834" s="T2455">am-bi-m</ta>
            <ta e="T2457" id="Seg_1835" s="T2456">dĭgəttə</ta>
            <ta e="T2458" id="Seg_1836" s="T2457">dĭ</ta>
            <ta e="T2459" id="Seg_1837" s="T2458">kudo-nzə-bi</ta>
            <ta e="T2460" id="Seg_1838" s="T2459">kan-ə-ʔ</ta>
            <ta e="T2461" id="Seg_1839" s="T2460">döʔə</ta>
            <ta e="T2462" id="Seg_1840" s="T2461">dĭ</ta>
            <ta e="T2464" id="Seg_1841" s="T2463">kan-bi</ta>
            <ta e="T2465" id="Seg_1842" s="T2464">i</ta>
            <ta e="T2466" id="Seg_1843" s="T2465">kan-zittə</ta>
            <ta e="T2467" id="Seg_1844" s="T2466">xatʼel</ta>
            <ta e="T2468" id="Seg_1845" s="T2467">dĭʔə</ta>
            <ta e="T2470" id="Seg_1846" s="T2469">dĭgəttə</ta>
            <ta e="T2471" id="Seg_1847" s="T2470">dĭ</ta>
            <ta e="T2472" id="Seg_1848" s="T2471">kirgaːr-laʔbə</ta>
            <ta e="T2473" id="Seg_1849" s="T2472">šo-ʔ</ta>
            <ta e="T2474" id="Seg_1850" s="T2473">döbər</ta>
            <ta e="T2475" id="Seg_1851" s="T2474">kan-ə-ʔ</ta>
            <ta e="T2476" id="Seg_1852" s="T2475">măna</ta>
            <ta e="T2477" id="Seg_1853" s="T2476">üžü</ta>
            <ta e="T2478" id="Seg_1854" s="T2477">i-ʔ</ta>
            <ta e="T2479" id="Seg_1855" s="T2478">ku-Tə</ta>
            <ta e="T2480" id="Seg_1856" s="T2479">ku-Tə</ta>
            <ta e="T2481" id="Seg_1857" s="T2480">ku-Tə</ta>
            <ta e="T2482" id="Seg_1858" s="T2481">ku-Tə</ta>
            <ta e="T2483" id="Seg_1859" s="T2482">dĭ</ta>
            <ta e="T2484" id="Seg_1860" s="T2483">kan-bi</ta>
            <ta e="T2485" id="Seg_1861" s="T2484">i-bi</ta>
            <ta e="T2487" id="Seg_1862" s="T2486">üžü</ta>
            <ta e="T2488" id="Seg_1863" s="T2487">šonə-gA</ta>
            <ta e="T2489" id="Seg_1864" s="T2488">i</ta>
            <ta e="T2490" id="Seg_1865" s="T2489">sădăr-laʔbə</ta>
            <ta e="T2491" id="Seg_1866" s="T2490">to</ta>
            <ta e="T2492" id="Seg_1867" s="T2491">döbər</ta>
            <ta e="T2493" id="Seg_1868" s="T2492">to</ta>
            <ta e="T2494" id="Seg_1869" s="T2493">döbər</ta>
            <ta e="T2495" id="Seg_1870" s="T2494">to</ta>
            <ta e="T2496" id="Seg_1871" s="T2495">bögəl-gəndə</ta>
            <ta e="T2497" id="Seg_1872" s="T2496">to</ta>
            <ta e="T2498" id="Seg_1873" s="T2497">kadul-gəndə</ta>
            <ta e="T2499" id="Seg_1874" s="T2498">dĭgəttə</ta>
            <ta e="T2500" id="Seg_1875" s="T2499">dĭ</ta>
            <ta e="T2501" id="Seg_1876" s="T2500">ku-laʔbə</ta>
            <ta e="T2502" id="Seg_1877" s="T2501">i</ta>
            <ta e="T2503" id="Seg_1878" s="T2502">kudo-nzə-laʔbə</ta>
            <ta e="T2504" id="Seg_1879" s="T2503">măn</ta>
            <ta e="T2505" id="Seg_1880" s="T2504">măn-bi-m</ta>
            <ta e="T2506" id="Seg_1881" s="T2505">ku-Tə</ta>
            <ta e="T2507" id="Seg_1882" s="T2506">ku-Tə</ta>
            <ta e="T2508" id="Seg_1883" s="T2507">a</ta>
            <ta e="T2509" id="Seg_1884" s="T2508">tăn</ta>
            <ta e="T2510" id="Seg_1885" s="T2509">dö</ta>
            <ta e="T2511" id="Seg_1886" s="T2510">kadul-gəndə</ta>
            <ta e="T2512" id="Seg_1887" s="T2511">i</ta>
            <ta e="T2513" id="Seg_1888" s="T2512">bögəl-gəndə</ta>
            <ta e="T2514" id="Seg_1889" s="T2513">i-bi-l</ta>
            <ta e="T2515" id="Seg_1890" s="T2514">aktʼa</ta>
            <ta e="T2516" id="Seg_1891" s="T2515">amka</ta>
            <ta e="T2517" id="Seg_1892" s="T2516">nadə</ta>
            <ta e="T2518" id="Seg_1893" s="T2517">ĭššo</ta>
            <ta e="T2520" id="Seg_1894" s="T2519">ĭššo</ta>
            <ta e="T2521" id="Seg_1895" s="T2520">dĭldʼi</ta>
            <ta e="T2522" id="Seg_1896" s="T2521">sumna</ta>
            <ta e="T2523" id="Seg_1897" s="T2522">dĭgəttə</ta>
            <ta e="T2524" id="Seg_1898" s="T2523">dĭ</ta>
            <ta e="T2525" id="Seg_1899" s="T2524">mĭ-bi</ta>
            <ta e="T2527" id="Seg_1900" s="T2526">dĭ-Tə</ta>
            <ta e="T2528" id="Seg_1901" s="T2527">biəʔ</ta>
            <ta e="T2529" id="Seg_1902" s="T2528">dĭ</ta>
            <ta e="T2530" id="Seg_1903" s="T2529">kan-bi</ta>
            <ta e="T2531" id="Seg_1904" s="T2530">dĭ</ta>
            <ta e="T2532" id="Seg_1905" s="T2531">üžü-m</ta>
            <ta e="T2533" id="Seg_1906" s="T2532">dĭ</ta>
            <ta e="T2534" id="Seg_1907" s="T2533">že</ta>
            <ta e="T2535" id="Seg_1908" s="T2534">üžü-m</ta>
            <ta e="T2536" id="Seg_1909" s="T2535">det-bi</ta>
            <ta e="T2537" id="Seg_1910" s="T2536">ku-Tə</ta>
            <ta e="T2538" id="Seg_1911" s="T2537">ku-Tə</ta>
            <ta e="T2539" id="Seg_1912" s="T2538">barəʔ-laʔbə</ta>
            <ta e="T2540" id="Seg_1913" s="T2539">no</ta>
            <ta e="T2541" id="Seg_1914" s="T2540">ugaːndə</ta>
            <ta e="T2542" id="Seg_1915" s="T2541">jakšə</ta>
            <ta e="T2544" id="Seg_1916" s="T2543">üžü</ta>
            <ta e="T2545" id="Seg_1917" s="T2544">dĭgəttə</ta>
            <ta e="T2546" id="Seg_1918" s="T2545">šer-bi</ta>
            <ta e="T2547" id="Seg_1919" s="T2546">dĭ</ta>
            <ta e="T2548" id="Seg_1920" s="T2547">măn-ntə</ta>
            <ta e="T2549" id="Seg_1921" s="T2548">mĭnzər-ə-ʔ</ta>
            <ta e="T2550" id="Seg_1922" s="T2549">kamən=də</ta>
            <ta e="T2551" id="Seg_1923" s="T2550">măn</ta>
            <ta e="T2552" id="Seg_1924" s="T2551">ej</ta>
            <ta e="T2553" id="Seg_1925" s="T2552">am-bi-m</ta>
            <ta e="T2554" id="Seg_1926" s="T2553">dărəʔ</ta>
            <ta e="T2555" id="Seg_1927" s="T2554">mĭnzər-ə-ʔ</ta>
            <ta e="T2556" id="Seg_1928" s="T2555">dĭ</ta>
            <ta e="T2557" id="Seg_1929" s="T2556">măn-ntə</ta>
            <ta e="T2558" id="Seg_1930" s="T2557">ĭmbi</ta>
            <ta e="T2559" id="Seg_1931" s="T2558">mĭnzər-zittə</ta>
            <ta e="T2560" id="Seg_1932" s="T2559">dĭ-Tə</ta>
            <ta e="T2561" id="Seg_1933" s="T2560">dĭn</ta>
            <ta e="T2562" id="Seg_1934" s="T2561">jama-t</ta>
            <ta e="T2563" id="Seg_1935" s="T2562">i-bi</ta>
            <ta e="T2565" id="Seg_1936" s="T2564">băd-bi</ta>
            <ta e="T2566" id="Seg_1937" s="T2565">i</ta>
            <ta e="T2567" id="Seg_1938" s="T2566">mĭnzər-bi</ta>
            <ta e="T2568" id="Seg_1939" s="T2567">döbər</ta>
            <ta e="T2569" id="Seg_1940" s="T2568">munəj</ta>
            <ta e="T2570" id="Seg_1941" s="T2569">hen-bi</ta>
            <ta e="T2571" id="Seg_1942" s="T2570">kajaʔ</ta>
            <ta e="T2572" id="Seg_1943" s="T2571">hen-bi</ta>
            <ta e="T0" id="Seg_1944" s="T2573">lavrovɨj</ta>
            <ta e="T2574" id="Seg_1945" s="T0">lis</ta>
            <ta e="T2575" id="Seg_1946" s="T2574">hen-bi</ta>
            <ta e="T2576" id="Seg_1947" s="T2575">dĭgəttə</ta>
            <ta e="T2577" id="Seg_1948" s="T2576">mĭnzər-bi</ta>
            <ta e="T2578" id="Seg_1949" s="T2577">dĭ</ta>
            <ta e="T2580" id="Seg_1950" s="T2579">am-laʔbə</ta>
            <ta e="T2581" id="Seg_1951" s="T2580">da</ta>
            <ta e="T2582" id="Seg_1952" s="T2581">kamən=də</ta>
            <ta e="T2583" id="Seg_1953" s="T2582">dĭrgit</ta>
            <ta e="T2584" id="Seg_1954" s="T2583">ej</ta>
            <ta e="T2585" id="Seg_1955" s="T2584">am-bi-m</ta>
            <ta e="T2586" id="Seg_1956" s="T2585">dĭgəttə</ta>
            <ta e="T2588" id="Seg_1957" s="T2587">šer-zittə</ta>
            <ta e="T2589" id="Seg_1958" s="T2588">jama-jəʔ-də</ta>
            <ta e="T2590" id="Seg_1959" s="T2589">a</ta>
            <ta e="T2591" id="Seg_1960" s="T2590">dĭn</ta>
            <ta e="T2592" id="Seg_1961" s="T2591">ši-jəʔ</ta>
            <ta e="T2593" id="Seg_1962" s="T2592">mo-laːm-bi-jəʔ</ta>
            <ta e="T2594" id="Seg_1963" s="T2593">üjü-t</ta>
            <ta e="T2595" id="Seg_1964" s="T2594">döbər</ta>
            <ta e="T2596" id="Seg_1965" s="T2595">kan-bi</ta>
            <ta e="T2597" id="Seg_1966" s="T2596">a</ta>
            <ta e="T2598" id="Seg_1967" s="T2597">tăn</ta>
            <ta e="T2599" id="Seg_1968" s="T2598">tüj</ta>
            <ta e="T2600" id="Seg_1969" s="T2599">măndo-ʔ</ta>
            <ta e="T2601" id="Seg_1970" s="T2600">măn</ta>
            <ta e="T2602" id="Seg_1971" s="T2601">girgit</ta>
            <ta e="T2603" id="Seg_1972" s="T2602">jama-jəʔ</ta>
            <ta e="T2604" id="Seg_1973" s="T2603">a</ta>
            <ta e="T2605" id="Seg_1974" s="T2604">tăn</ta>
            <ta e="T2606" id="Seg_1975" s="T2605">tüjö</ta>
            <ta e="T2607" id="Seg_1976" s="T2606">am-bi-l</ta>
            <ta e="T2608" id="Seg_1977" s="T2607">dĭgəttə</ta>
            <ta e="T2609" id="Seg_1978" s="T2608">kan-bi-jəʔ</ta>
            <ta e="T2610" id="Seg_1979" s="T2609">jada-j-lAʔ</ta>
            <ta e="T2611" id="Seg_1980" s="T2610">ine-ziʔ</ta>
            <ta e="T2612" id="Seg_1981" s="T2611">dĭgəttə</ta>
            <ta e="T2613" id="Seg_1982" s="T2612">dĭn</ta>
            <ta e="T2614" id="Seg_1983" s="T2613">bü</ta>
            <ta e="T2615" id="Seg_1984" s="T2614">bü-Kən</ta>
            <ta e="T2616" id="Seg_1985" s="T2615">kan-ntə-gA-jəʔ</ta>
            <ta e="T2618" id="Seg_1986" s="T2616">dĭ</ta>
            <ta e="T2620" id="Seg_1987" s="T2618">baza</ta>
            <ta e="T2621" id="Seg_1988" s="T2620">i-luʔbdə-bi</ta>
            <ta e="T2622" id="Seg_1989" s="T2621">dĭ</ta>
            <ta e="T2623" id="Seg_1990" s="T2622">ma-luʔbdə-bi</ta>
            <ta e="T2624" id="Seg_1991" s="T2623">tăn</ta>
            <ta e="T2625" id="Seg_1992" s="T2624">măna</ta>
            <ta e="T2626" id="Seg_1993" s="T2625">e-ʔ</ta>
            <ta e="T2627" id="Seg_1994" s="T2626">măndo-ʔ</ta>
            <ta e="T2628" id="Seg_1995" s="T2627">i</ta>
            <ta e="T2629" id="Seg_1996" s="T2628">kirgaːr-laʔbə</ta>
            <ta e="T2630" id="Seg_1997" s="T2629">măn</ta>
            <ta e="T2631" id="Seg_1998" s="T2630">ma-luʔbdə-bi-m</ta>
            <ta e="T2632" id="Seg_1999" s="T2631">a</ta>
            <ta e="T2633" id="Seg_2000" s="T2632">tăn</ta>
            <ta e="T2634" id="Seg_2001" s="T2633">măndo-r-zittə</ta>
            <ta e="T2635" id="Seg_2002" s="T2634">măna</ta>
            <ta e="T2636" id="Seg_2003" s="T2635">nʼe</ta>
            <ta e="T2638" id="Seg_2004" s="T2637">amno-laʔbə-ʔ</ta>
            <ta e="T2639" id="Seg_2005" s="T2638">bos-də</ta>
            <ta e="T2640" id="Seg_2006" s="T2639">kan-lAʔ</ta>
            <ta e="T2641" id="Seg_2007" s="T2640">tʼür-bi</ta>
            <ta e="T2642" id="Seg_2008" s="T2641">dĭ</ta>
            <ta e="T2644" id="Seg_2009" s="T2643">tüjö</ta>
            <ta e="T2645" id="Seg_2010" s="T2644">da</ta>
            <ta e="T2646" id="Seg_2011" s="T2645">dĭn</ta>
            <ta e="T2647" id="Seg_2012" s="T2646">amno-laʔbə</ta>
            <ta e="T2648" id="Seg_2013" s="T2647">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2407" id="Seg_2014" s="T2406">live-PST.[3SG]</ta>
            <ta e="T2408" id="Seg_2015" s="T2407">one.[NOM.SG]</ta>
            <ta e="T2410" id="Seg_2016" s="T2409">man.[NOM.SG]</ta>
            <ta e="T2411" id="Seg_2017" s="T2410">this-LAT</ta>
            <ta e="T2412" id="Seg_2018" s="T2411">come-PST.[3SG]</ta>
            <ta e="T2413" id="Seg_2019" s="T2412">man.[NOM.SG]</ta>
            <ta e="T2416" id="Seg_2020" s="T2415">hired</ta>
            <ta e="T2417" id="Seg_2021" s="T2416">take-INF.LAT</ta>
            <ta e="T2419" id="Seg_2022" s="T2418">how</ta>
            <ta e="T2420" id="Seg_2023" s="T2419">you.ACC</ta>
            <ta e="T2422" id="Seg_2024" s="T2421">call-PRS-3PL</ta>
            <ta e="T2424" id="Seg_2025" s="T2423">Sysoy_Kosoy.[NOM.SG]</ta>
            <ta e="T2425" id="Seg_2026" s="T2424">this.[NOM.SG]</ta>
            <ta e="T2426" id="Seg_2027" s="T2425">say-IPFVZ.[3SG]</ta>
            <ta e="T2428" id="Seg_2028" s="T2427">well</ta>
            <ta e="T2429" id="Seg_2029" s="T2428">live-IMP.2SG</ta>
            <ta e="T2430" id="Seg_2030" s="T2429">boil-EP-IMP.2SG</ta>
            <ta e="T2431" id="Seg_2031" s="T2430">I.LAT</ta>
            <ta e="T2432" id="Seg_2032" s="T2431">two.[NOM.SG]</ta>
            <ta e="T2433" id="Seg_2033" s="T2432">egg.[NOM.SG]</ta>
            <ta e="T2434" id="Seg_2034" s="T2433">this.[NOM.SG]</ta>
            <ta e="T2435" id="Seg_2035" s="T2434">boil-PST.[3SG]</ta>
            <ta e="T2436" id="Seg_2036" s="T2435">boil-PST.[3SG]</ta>
            <ta e="T2437" id="Seg_2037" s="T2436">boil-PST.[3SG]</ta>
            <ta e="T2438" id="Seg_2038" s="T2437">take-PST.[3SG]</ta>
            <ta e="T2439" id="Seg_2039" s="T2438">and</ta>
            <ta e="T2440" id="Seg_2040" s="T2439">one.[NOM.SG]</ta>
            <ta e="T2441" id="Seg_2041" s="T2440">eat-MOM-PST.[3SG]</ta>
            <ta e="T2442" id="Seg_2042" s="T2441">one.[NOM.SG]</ta>
            <ta e="T2443" id="Seg_2043" s="T2442">bring-DUR.[3SG]</ta>
            <ta e="T2444" id="Seg_2044" s="T2443">what.[NOM.SG]</ta>
            <ta e="T2445" id="Seg_2045" s="T2444">you.NOM</ta>
            <ta e="T2446" id="Seg_2046" s="T2445">one.[NOM.SG]</ta>
            <ta e="T2447" id="Seg_2047" s="T2446">bring-PST-2SG</ta>
            <ta e="T2448" id="Seg_2048" s="T2447">I.NOM</ta>
            <ta e="T2449" id="Seg_2049" s="T2448">two.[NOM.SG]</ta>
            <ta e="T2451" id="Seg_2050" s="T2450">say-PST-1SG</ta>
            <ta e="T2452" id="Seg_2051" s="T2451">boil-EP-IMP.2SG</ta>
            <ta e="T2453" id="Seg_2052" s="T2452">and</ta>
            <ta e="T2454" id="Seg_2053" s="T2453">I.NOM</ta>
            <ta e="T2455" id="Seg_2054" s="T2454">one.[NOM.SG]</ta>
            <ta e="T2456" id="Seg_2055" s="T2455">eat-PST-1SG</ta>
            <ta e="T2457" id="Seg_2056" s="T2456">then</ta>
            <ta e="T2458" id="Seg_2057" s="T2457">this.[NOM.SG]</ta>
            <ta e="T2459" id="Seg_2058" s="T2458">scold-DES-PST.[3SG]</ta>
            <ta e="T2460" id="Seg_2059" s="T2459">go-EP-IMP.2SG</ta>
            <ta e="T2461" id="Seg_2060" s="T2460">hence</ta>
            <ta e="T2462" id="Seg_2061" s="T2461">this.[NOM.SG]</ta>
            <ta e="T2464" id="Seg_2062" s="T2463">go-PST.[3SG]</ta>
            <ta e="T2465" id="Seg_2063" s="T2464">and</ta>
            <ta e="T2466" id="Seg_2064" s="T2465">go-INF.LAT</ta>
            <ta e="T2467" id="Seg_2065" s="T2466">want.PST.M.SG</ta>
            <ta e="T2468" id="Seg_2066" s="T2467">from.there</ta>
            <ta e="T2470" id="Seg_2067" s="T2469">then</ta>
            <ta e="T2471" id="Seg_2068" s="T2470">this.[NOM.SG]</ta>
            <ta e="T2472" id="Seg_2069" s="T2471">shout-DUR.[3SG]</ta>
            <ta e="T2473" id="Seg_2070" s="T2472">come-IMP.2SG</ta>
            <ta e="T2474" id="Seg_2071" s="T2473">here</ta>
            <ta e="T2475" id="Seg_2072" s="T2474">go-EP-IMP.2SG</ta>
            <ta e="T2476" id="Seg_2073" s="T2475">I.LAT</ta>
            <ta e="T2477" id="Seg_2074" s="T2476">cap.[NOM.SG]</ta>
            <ta e="T2478" id="Seg_2075" s="T2477">take-IMP.2SG</ta>
            <ta e="T2479" id="Seg_2076" s="T2478">ear-LAT</ta>
            <ta e="T2480" id="Seg_2077" s="T2479">ear-LAT</ta>
            <ta e="T2481" id="Seg_2078" s="T2480">ear-LAT</ta>
            <ta e="T2482" id="Seg_2079" s="T2481">ear-LAT</ta>
            <ta e="T2483" id="Seg_2080" s="T2482">this.[NOM.SG]</ta>
            <ta e="T2484" id="Seg_2081" s="T2483">go-PST.[3SG]</ta>
            <ta e="T2485" id="Seg_2082" s="T2484">take-PST.[3SG]</ta>
            <ta e="T2487" id="Seg_2083" s="T2486">cap.[NOM.SG]</ta>
            <ta e="T2488" id="Seg_2084" s="T2487">come-PRS.[3SG]</ta>
            <ta e="T2489" id="Seg_2085" s="T2488">and</ta>
            <ta e="T2490" id="Seg_2086" s="T2489">shake-DUR.[3SG]</ta>
            <ta e="T2491" id="Seg_2087" s="T2490">then</ta>
            <ta e="T2492" id="Seg_2088" s="T2491">here</ta>
            <ta e="T2493" id="Seg_2089" s="T2492">then</ta>
            <ta e="T2494" id="Seg_2090" s="T2493">here</ta>
            <ta e="T2495" id="Seg_2091" s="T2494">then</ta>
            <ta e="T2496" id="Seg_2092" s="T2495">back-LAT/LOC.3SG</ta>
            <ta e="T2497" id="Seg_2093" s="T2496">then</ta>
            <ta e="T2498" id="Seg_2094" s="T2497">face-LAT/LOC.3SG</ta>
            <ta e="T2499" id="Seg_2095" s="T2498">then</ta>
            <ta e="T2500" id="Seg_2096" s="T2499">this.[NOM.SG]</ta>
            <ta e="T2501" id="Seg_2097" s="T2500">see-DUR.[3SG]</ta>
            <ta e="T2502" id="Seg_2098" s="T2501">and</ta>
            <ta e="T2503" id="Seg_2099" s="T2502">scold-DES-DUR.[3SG]</ta>
            <ta e="T2504" id="Seg_2100" s="T2503">I.NOM</ta>
            <ta e="T2505" id="Seg_2101" s="T2504">say-PST-1SG</ta>
            <ta e="T2506" id="Seg_2102" s="T2505">ear-LAT</ta>
            <ta e="T2507" id="Seg_2103" s="T2506">ear-LAT</ta>
            <ta e="T2508" id="Seg_2104" s="T2507">and</ta>
            <ta e="T2509" id="Seg_2105" s="T2508">you.NOM</ta>
            <ta e="T2510" id="Seg_2106" s="T2509">that.[NOM.SG]</ta>
            <ta e="T2511" id="Seg_2107" s="T2510">face-LAT/LOC.3SG</ta>
            <ta e="T2512" id="Seg_2108" s="T2511">and</ta>
            <ta e="T2513" id="Seg_2109" s="T2512">back-LAT/LOC.3SG</ta>
            <ta e="T2514" id="Seg_2110" s="T2513">take-PST-2SG</ta>
            <ta e="T2515" id="Seg_2111" s="T2514">money.[NOM.SG]</ta>
            <ta e="T2516" id="Seg_2112" s="T2515">few</ta>
            <ta e="T2517" id="Seg_2113" s="T2516">one.should</ta>
            <ta e="T2518" id="Seg_2114" s="T2517">more</ta>
            <ta e="T2520" id="Seg_2115" s="T2519">more</ta>
            <ta e="T2521" id="Seg_2116" s="T2520">so.many</ta>
            <ta e="T2522" id="Seg_2117" s="T2521">five.[NOM.SG]</ta>
            <ta e="T2523" id="Seg_2118" s="T2522">then</ta>
            <ta e="T2524" id="Seg_2119" s="T2523">this.[NOM.SG]</ta>
            <ta e="T2525" id="Seg_2120" s="T2524">give-PST.[3SG]</ta>
            <ta e="T2527" id="Seg_2121" s="T2526">this-LAT</ta>
            <ta e="T2528" id="Seg_2122" s="T2527">ten.[NOM.SG]</ta>
            <ta e="T2529" id="Seg_2123" s="T2528">this.[NOM.SG]</ta>
            <ta e="T2530" id="Seg_2124" s="T2529">go-PST.[3SG]</ta>
            <ta e="T2531" id="Seg_2125" s="T2530">this.[NOM.SG]</ta>
            <ta e="T2532" id="Seg_2126" s="T2531">cap-ACC</ta>
            <ta e="T2533" id="Seg_2127" s="T2532">this.[NOM.SG]</ta>
            <ta e="T2534" id="Seg_2128" s="T2533">PTCL</ta>
            <ta e="T2535" id="Seg_2129" s="T2534">cap-ACC</ta>
            <ta e="T2536" id="Seg_2130" s="T2535">bring-PST.[3SG]</ta>
            <ta e="T2537" id="Seg_2131" s="T2536">ear-LAT</ta>
            <ta e="T2538" id="Seg_2132" s="T2537">ear-LAT</ta>
            <ta e="T2539" id="Seg_2133" s="T2538">throw.away-DUR.[3SG]</ta>
            <ta e="T2540" id="Seg_2134" s="T2539">well</ta>
            <ta e="T2541" id="Seg_2135" s="T2540">very</ta>
            <ta e="T2542" id="Seg_2136" s="T2541">good</ta>
            <ta e="T2544" id="Seg_2137" s="T2543">cap.[NOM.SG]</ta>
            <ta e="T2545" id="Seg_2138" s="T2544">then</ta>
            <ta e="T2546" id="Seg_2139" s="T2545">dress-PST.[3SG]</ta>
            <ta e="T2547" id="Seg_2140" s="T2546">this.[NOM.SG]</ta>
            <ta e="T2548" id="Seg_2141" s="T2547">say-IPFVZ.[3SG]</ta>
            <ta e="T2549" id="Seg_2142" s="T2548">boil-EP-IMP.2SG</ta>
            <ta e="T2550" id="Seg_2143" s="T2549">when=INDEF</ta>
            <ta e="T2551" id="Seg_2144" s="T2550">I.NOM</ta>
            <ta e="T2552" id="Seg_2145" s="T2551">NEG</ta>
            <ta e="T2553" id="Seg_2146" s="T2552">eat-PST-1SG</ta>
            <ta e="T2554" id="Seg_2147" s="T2553">so</ta>
            <ta e="T2555" id="Seg_2148" s="T2554">boil-EP-IMP.2SG</ta>
            <ta e="T2556" id="Seg_2149" s="T2555">this.[NOM.SG]</ta>
            <ta e="T2557" id="Seg_2150" s="T2556">say-IPFVZ.[3SG]</ta>
            <ta e="T2558" id="Seg_2151" s="T2557">what.[NOM.SG]</ta>
            <ta e="T2559" id="Seg_2152" s="T2558">boil-INF.LAT</ta>
            <ta e="T2560" id="Seg_2153" s="T2559">this-LAT</ta>
            <ta e="T2561" id="Seg_2154" s="T2560">there</ta>
            <ta e="T2562" id="Seg_2155" s="T2561">boot-NOM/GEN.3SG</ta>
            <ta e="T2563" id="Seg_2156" s="T2562">take-PST.[3SG]</ta>
            <ta e="T2565" id="Seg_2157" s="T2564">%%-PST.[3SG]</ta>
            <ta e="T2566" id="Seg_2158" s="T2565">and</ta>
            <ta e="T2567" id="Seg_2159" s="T2566">boil-PST.[3SG]</ta>
            <ta e="T2568" id="Seg_2160" s="T2567">here</ta>
            <ta e="T2569" id="Seg_2161" s="T2568">egg.[NOM.SG]</ta>
            <ta e="T2570" id="Seg_2162" s="T2569">put-PST.[3SG]</ta>
            <ta e="T2571" id="Seg_2163" s="T2570">butter.[NOM.SG]</ta>
            <ta e="T2572" id="Seg_2164" s="T2571">put-PST.[3SG]</ta>
            <ta e="T0" id="Seg_2165" s="T2573">laurel.ADJ</ta>
            <ta e="T2574" id="Seg_2166" s="T0">leaf.[NOM.SG]</ta>
            <ta e="T2575" id="Seg_2167" s="T2574">put-PST.[3SG]</ta>
            <ta e="T2576" id="Seg_2168" s="T2575">then</ta>
            <ta e="T2577" id="Seg_2169" s="T2576">boil-PST.[3SG]</ta>
            <ta e="T2578" id="Seg_2170" s="T2577">this.[NOM.SG]</ta>
            <ta e="T2580" id="Seg_2171" s="T2579">eat-DUR.[3SG]</ta>
            <ta e="T2581" id="Seg_2172" s="T2580">and</ta>
            <ta e="T2582" id="Seg_2173" s="T2581">when=INDEF</ta>
            <ta e="T2583" id="Seg_2174" s="T2582">such.[NOM.SG]</ta>
            <ta e="T2584" id="Seg_2175" s="T2583">NEG</ta>
            <ta e="T2585" id="Seg_2176" s="T2584">eat-PST-1SG</ta>
            <ta e="T2586" id="Seg_2177" s="T2585">then</ta>
            <ta e="T2588" id="Seg_2178" s="T2587">dress-INF.LAT</ta>
            <ta e="T2589" id="Seg_2179" s="T2588">boot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2590" id="Seg_2180" s="T2589">and</ta>
            <ta e="T2591" id="Seg_2181" s="T2590">there</ta>
            <ta e="T2592" id="Seg_2182" s="T2591">hole-NOM/GEN/ACC.3PL</ta>
            <ta e="T2593" id="Seg_2183" s="T2592">become-RES-PST-3PL</ta>
            <ta e="T2594" id="Seg_2184" s="T2593">foot-NOM/GEN.3SG</ta>
            <ta e="T2595" id="Seg_2185" s="T2594">here</ta>
            <ta e="T2596" id="Seg_2186" s="T2595">go-PST.[3SG]</ta>
            <ta e="T2597" id="Seg_2187" s="T2596">and</ta>
            <ta e="T2598" id="Seg_2188" s="T2597">you.NOM</ta>
            <ta e="T2599" id="Seg_2189" s="T2598">now</ta>
            <ta e="T2600" id="Seg_2190" s="T2599">look-IMP.2SG</ta>
            <ta e="T2601" id="Seg_2191" s="T2600">I.NOM</ta>
            <ta e="T2602" id="Seg_2192" s="T2601">what.kind</ta>
            <ta e="T2603" id="Seg_2193" s="T2602">boot-PL</ta>
            <ta e="T2604" id="Seg_2194" s="T2603">and</ta>
            <ta e="T2605" id="Seg_2195" s="T2604">you.NOM</ta>
            <ta e="T2606" id="Seg_2196" s="T2605">soon</ta>
            <ta e="T2607" id="Seg_2197" s="T2606">eat-PST-2SG</ta>
            <ta e="T2608" id="Seg_2198" s="T2607">then</ta>
            <ta e="T2609" id="Seg_2199" s="T2608">go-PST-3PL</ta>
            <ta e="T2610" id="Seg_2200" s="T2609">village-VBLZ-CVB</ta>
            <ta e="T2611" id="Seg_2201" s="T2610">horse-INS</ta>
            <ta e="T2612" id="Seg_2202" s="T2611">then</ta>
            <ta e="T2613" id="Seg_2203" s="T2612">there</ta>
            <ta e="T2614" id="Seg_2204" s="T2613">water.[NOM.SG]</ta>
            <ta e="T2615" id="Seg_2205" s="T2614">water-LOC</ta>
            <ta e="T2616" id="Seg_2206" s="T2615">go-IPFVZ-PRS-3PL</ta>
            <ta e="T2618" id="Seg_2207" s="T2616">this.[NOM.SG]</ta>
            <ta e="T2620" id="Seg_2208" s="T2618">iron.[NOM.SG]</ta>
            <ta e="T2621" id="Seg_2209" s="T2620">take-MOM-PST.[3SG]</ta>
            <ta e="T2622" id="Seg_2210" s="T2621">this.[NOM.SG]</ta>
            <ta e="T2623" id="Seg_2211" s="T2622">leave-MOM-PST.[3SG]</ta>
            <ta e="T2624" id="Seg_2212" s="T2623">you.NOM</ta>
            <ta e="T2625" id="Seg_2213" s="T2624">I.LAT</ta>
            <ta e="T2626" id="Seg_2214" s="T2625">NEG.AUX-IMP.2SG</ta>
            <ta e="T2627" id="Seg_2215" s="T2626">look-CNG</ta>
            <ta e="T2628" id="Seg_2216" s="T2627">and</ta>
            <ta e="T2629" id="Seg_2217" s="T2628">shout-DUR.[3SG]</ta>
            <ta e="T2630" id="Seg_2218" s="T2629">I.NOM</ta>
            <ta e="T2631" id="Seg_2219" s="T2630">remain-MOM-PST-1SG</ta>
            <ta e="T2632" id="Seg_2220" s="T2631">and</ta>
            <ta e="T2633" id="Seg_2221" s="T2632">you.NOM</ta>
            <ta e="T2634" id="Seg_2222" s="T2633">look-FRQ-INF.LAT</ta>
            <ta e="T2635" id="Seg_2223" s="T2634">I.LAT</ta>
            <ta e="T2636" id="Seg_2224" s="T2635">not</ta>
            <ta e="T2638" id="Seg_2225" s="T2637">sit-DUR-IMP.2SG</ta>
            <ta e="T2639" id="Seg_2226" s="T2638">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T2640" id="Seg_2227" s="T2639">go-CVB</ta>
            <ta e="T2641" id="Seg_2228" s="T2640">disappear-PST.[3SG]</ta>
            <ta e="T2642" id="Seg_2229" s="T2641">this.[NOM.SG]</ta>
            <ta e="T2644" id="Seg_2230" s="T2643">soon</ta>
            <ta e="T2645" id="Seg_2231" s="T2644">and</ta>
            <ta e="T2646" id="Seg_2232" s="T2645">there</ta>
            <ta e="T2647" id="Seg_2233" s="T2646">sit-DUR.[3SG]</ta>
            <ta e="T2648" id="Seg_2234" s="T2647">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2407" id="Seg_2235" s="T2406">жить-PST.[3SG]</ta>
            <ta e="T2408" id="Seg_2236" s="T2407">один.[NOM.SG]</ta>
            <ta e="T2410" id="Seg_2237" s="T2409">мужчина.[NOM.SG]</ta>
            <ta e="T2411" id="Seg_2238" s="T2410">этот-LAT</ta>
            <ta e="T2412" id="Seg_2239" s="T2411">прийти-PST.[3SG]</ta>
            <ta e="T2413" id="Seg_2240" s="T2412">мужчина.[NOM.SG]</ta>
            <ta e="T2416" id="Seg_2241" s="T2415">наемный</ta>
            <ta e="T2417" id="Seg_2242" s="T2416">взять-INF.LAT</ta>
            <ta e="T2419" id="Seg_2243" s="T2418">как</ta>
            <ta e="T2420" id="Seg_2244" s="T2419">ты.ACC</ta>
            <ta e="T2422" id="Seg_2245" s="T2421">позвать-PRS-3PL</ta>
            <ta e="T2424" id="Seg_2246" s="T2423">Сысой_Косой.[NOM.SG]</ta>
            <ta e="T2425" id="Seg_2247" s="T2424">этот.[NOM.SG]</ta>
            <ta e="T2426" id="Seg_2248" s="T2425">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2428" id="Seg_2249" s="T2427">ну</ta>
            <ta e="T2429" id="Seg_2250" s="T2428">жить-IMP.2SG</ta>
            <ta e="T2430" id="Seg_2251" s="T2429">кипятить-EP-IMP.2SG</ta>
            <ta e="T2431" id="Seg_2252" s="T2430">я.LAT</ta>
            <ta e="T2432" id="Seg_2253" s="T2431">два.[NOM.SG]</ta>
            <ta e="T2433" id="Seg_2254" s="T2432">яйцо.[NOM.SG]</ta>
            <ta e="T2434" id="Seg_2255" s="T2433">этот.[NOM.SG]</ta>
            <ta e="T2435" id="Seg_2256" s="T2434">кипятить-PST.[3SG]</ta>
            <ta e="T2436" id="Seg_2257" s="T2435">кипятить-PST.[3SG]</ta>
            <ta e="T2437" id="Seg_2258" s="T2436">кипятить-PST.[3SG]</ta>
            <ta e="T2438" id="Seg_2259" s="T2437">взять-PST.[3SG]</ta>
            <ta e="T2439" id="Seg_2260" s="T2438">и</ta>
            <ta e="T2440" id="Seg_2261" s="T2439">один.[NOM.SG]</ta>
            <ta e="T2441" id="Seg_2262" s="T2440">съесть-MOM-PST.[3SG]</ta>
            <ta e="T2442" id="Seg_2263" s="T2441">один.[NOM.SG]</ta>
            <ta e="T2443" id="Seg_2264" s="T2442">принести-DUR.[3SG]</ta>
            <ta e="T2444" id="Seg_2265" s="T2443">что.[NOM.SG]</ta>
            <ta e="T2445" id="Seg_2266" s="T2444">ты.NOM</ta>
            <ta e="T2446" id="Seg_2267" s="T2445">один.[NOM.SG]</ta>
            <ta e="T2447" id="Seg_2268" s="T2446">принести-PST-2SG</ta>
            <ta e="T2448" id="Seg_2269" s="T2447">я.NOM</ta>
            <ta e="T2449" id="Seg_2270" s="T2448">два.[NOM.SG]</ta>
            <ta e="T2451" id="Seg_2271" s="T2450">сказать-PST-1SG</ta>
            <ta e="T2452" id="Seg_2272" s="T2451">кипятить-EP-IMP.2SG</ta>
            <ta e="T2453" id="Seg_2273" s="T2452">а</ta>
            <ta e="T2454" id="Seg_2274" s="T2453">я.NOM</ta>
            <ta e="T2455" id="Seg_2275" s="T2454">один.[NOM.SG]</ta>
            <ta e="T2456" id="Seg_2276" s="T2455">съесть-PST-1SG</ta>
            <ta e="T2457" id="Seg_2277" s="T2456">тогда</ta>
            <ta e="T2458" id="Seg_2278" s="T2457">этот.[NOM.SG]</ta>
            <ta e="T2459" id="Seg_2279" s="T2458">ругать-DES-PST.[3SG]</ta>
            <ta e="T2460" id="Seg_2280" s="T2459">пойти-EP-IMP.2SG</ta>
            <ta e="T2461" id="Seg_2281" s="T2460">отсюда</ta>
            <ta e="T2462" id="Seg_2282" s="T2461">этот.[NOM.SG]</ta>
            <ta e="T2464" id="Seg_2283" s="T2463">пойти-PST.[3SG]</ta>
            <ta e="T2465" id="Seg_2284" s="T2464">и</ta>
            <ta e="T2466" id="Seg_2285" s="T2465">пойти-INF.LAT</ta>
            <ta e="T2467" id="Seg_2286" s="T2466">хотеть.PST.M.SG</ta>
            <ta e="T2468" id="Seg_2287" s="T2467">оттуда</ta>
            <ta e="T2470" id="Seg_2288" s="T2469">тогда</ta>
            <ta e="T2471" id="Seg_2289" s="T2470">этот.[NOM.SG]</ta>
            <ta e="T2472" id="Seg_2290" s="T2471">кричать-DUR.[3SG]</ta>
            <ta e="T2473" id="Seg_2291" s="T2472">прийти-IMP.2SG</ta>
            <ta e="T2474" id="Seg_2292" s="T2473">здесь</ta>
            <ta e="T2475" id="Seg_2293" s="T2474">пойти-EP-IMP.2SG</ta>
            <ta e="T2476" id="Seg_2294" s="T2475">я.LAT</ta>
            <ta e="T2477" id="Seg_2295" s="T2476">шапка.[NOM.SG]</ta>
            <ta e="T2478" id="Seg_2296" s="T2477">взять-IMP.2SG</ta>
            <ta e="T2479" id="Seg_2297" s="T2478">ухо-LAT</ta>
            <ta e="T2480" id="Seg_2298" s="T2479">ухо-LAT</ta>
            <ta e="T2481" id="Seg_2299" s="T2480">ухо-LAT</ta>
            <ta e="T2482" id="Seg_2300" s="T2481">ухо-LAT</ta>
            <ta e="T2483" id="Seg_2301" s="T2482">этот.[NOM.SG]</ta>
            <ta e="T2484" id="Seg_2302" s="T2483">пойти-PST.[3SG]</ta>
            <ta e="T2485" id="Seg_2303" s="T2484">взять-PST.[3SG]</ta>
            <ta e="T2487" id="Seg_2304" s="T2486">шапка.[NOM.SG]</ta>
            <ta e="T2488" id="Seg_2305" s="T2487">прийти-PRS.[3SG]</ta>
            <ta e="T2489" id="Seg_2306" s="T2488">и</ta>
            <ta e="T2490" id="Seg_2307" s="T2489">трясти-DUR.[3SG]</ta>
            <ta e="T2491" id="Seg_2308" s="T2490">то</ta>
            <ta e="T2492" id="Seg_2309" s="T2491">здесь</ta>
            <ta e="T2493" id="Seg_2310" s="T2492">то</ta>
            <ta e="T2494" id="Seg_2311" s="T2493">здесь</ta>
            <ta e="T2495" id="Seg_2312" s="T2494">то</ta>
            <ta e="T2496" id="Seg_2313" s="T2495">спина-LAT/LOC.3SG</ta>
            <ta e="T2497" id="Seg_2314" s="T2496">то</ta>
            <ta e="T2498" id="Seg_2315" s="T2497">лицо-LAT/LOC.3SG</ta>
            <ta e="T2499" id="Seg_2316" s="T2498">тогда</ta>
            <ta e="T2500" id="Seg_2317" s="T2499">этот.[NOM.SG]</ta>
            <ta e="T2501" id="Seg_2318" s="T2500">видеть-DUR.[3SG]</ta>
            <ta e="T2502" id="Seg_2319" s="T2501">и</ta>
            <ta e="T2503" id="Seg_2320" s="T2502">ругать-DES-DUR.[3SG]</ta>
            <ta e="T2504" id="Seg_2321" s="T2503">я.NOM</ta>
            <ta e="T2505" id="Seg_2322" s="T2504">сказать-PST-1SG</ta>
            <ta e="T2506" id="Seg_2323" s="T2505">ухо-LAT</ta>
            <ta e="T2507" id="Seg_2324" s="T2506">ухо-LAT</ta>
            <ta e="T2508" id="Seg_2325" s="T2507">а</ta>
            <ta e="T2509" id="Seg_2326" s="T2508">ты.NOM</ta>
            <ta e="T2510" id="Seg_2327" s="T2509">тот.[NOM.SG]</ta>
            <ta e="T2511" id="Seg_2328" s="T2510">лицо-LAT/LOC.3SG</ta>
            <ta e="T2512" id="Seg_2329" s="T2511">и</ta>
            <ta e="T2513" id="Seg_2330" s="T2512">спина-LAT/LOC.3SG</ta>
            <ta e="T2514" id="Seg_2331" s="T2513">взять-PST-2SG</ta>
            <ta e="T2515" id="Seg_2332" s="T2514">деньги.[NOM.SG]</ta>
            <ta e="T2516" id="Seg_2333" s="T2515">мало</ta>
            <ta e="T2517" id="Seg_2334" s="T2516">надо</ta>
            <ta e="T2518" id="Seg_2335" s="T2517">еще</ta>
            <ta e="T2520" id="Seg_2336" s="T2519">еще</ta>
            <ta e="T2521" id="Seg_2337" s="T2520">столько</ta>
            <ta e="T2522" id="Seg_2338" s="T2521">пять.[NOM.SG]</ta>
            <ta e="T2523" id="Seg_2339" s="T2522">тогда</ta>
            <ta e="T2524" id="Seg_2340" s="T2523">этот.[NOM.SG]</ta>
            <ta e="T2525" id="Seg_2341" s="T2524">дать-PST.[3SG]</ta>
            <ta e="T2527" id="Seg_2342" s="T2526">этот-LAT</ta>
            <ta e="T2528" id="Seg_2343" s="T2527">десять.[NOM.SG]</ta>
            <ta e="T2529" id="Seg_2344" s="T2528">этот.[NOM.SG]</ta>
            <ta e="T2530" id="Seg_2345" s="T2529">пойти-PST.[3SG]</ta>
            <ta e="T2531" id="Seg_2346" s="T2530">этот.[NOM.SG]</ta>
            <ta e="T2532" id="Seg_2347" s="T2531">шапка-ACC</ta>
            <ta e="T2533" id="Seg_2348" s="T2532">этот.[NOM.SG]</ta>
            <ta e="T2534" id="Seg_2349" s="T2533">же</ta>
            <ta e="T2535" id="Seg_2350" s="T2534">шапка-ACC</ta>
            <ta e="T2536" id="Seg_2351" s="T2535">принести-PST.[3SG]</ta>
            <ta e="T2537" id="Seg_2352" s="T2536">ухо-LAT</ta>
            <ta e="T2538" id="Seg_2353" s="T2537">ухо-LAT</ta>
            <ta e="T2539" id="Seg_2354" s="T2538">выбросить-DUR.[3SG]</ta>
            <ta e="T2540" id="Seg_2355" s="T2539">ну</ta>
            <ta e="T2541" id="Seg_2356" s="T2540">очень</ta>
            <ta e="T2542" id="Seg_2357" s="T2541">хороший</ta>
            <ta e="T2544" id="Seg_2358" s="T2543">шапка.[NOM.SG]</ta>
            <ta e="T2545" id="Seg_2359" s="T2544">тогда</ta>
            <ta e="T2546" id="Seg_2360" s="T2545">надеть-PST.[3SG]</ta>
            <ta e="T2547" id="Seg_2361" s="T2546">этот.[NOM.SG]</ta>
            <ta e="T2548" id="Seg_2362" s="T2547">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2549" id="Seg_2363" s="T2548">кипятить-EP-IMP.2SG</ta>
            <ta e="T2550" id="Seg_2364" s="T2549">когда=INDEF</ta>
            <ta e="T2551" id="Seg_2365" s="T2550">я.NOM</ta>
            <ta e="T2552" id="Seg_2366" s="T2551">NEG</ta>
            <ta e="T2553" id="Seg_2367" s="T2552">съесть-PST-1SG</ta>
            <ta e="T2554" id="Seg_2368" s="T2553">так</ta>
            <ta e="T2555" id="Seg_2369" s="T2554">кипятить-EP-IMP.2SG</ta>
            <ta e="T2556" id="Seg_2370" s="T2555">этот.[NOM.SG]</ta>
            <ta e="T2557" id="Seg_2371" s="T2556">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2558" id="Seg_2372" s="T2557">что.[NOM.SG]</ta>
            <ta e="T2559" id="Seg_2373" s="T2558">кипятить-INF.LAT</ta>
            <ta e="T2560" id="Seg_2374" s="T2559">этот-LAT</ta>
            <ta e="T2561" id="Seg_2375" s="T2560">там</ta>
            <ta e="T2562" id="Seg_2376" s="T2561">сапог-NOM/GEN.3SG</ta>
            <ta e="T2563" id="Seg_2377" s="T2562">взять-PST.[3SG]</ta>
            <ta e="T2565" id="Seg_2378" s="T2564">%%-PST.[3SG]</ta>
            <ta e="T2566" id="Seg_2379" s="T2565">и</ta>
            <ta e="T2567" id="Seg_2380" s="T2566">кипятить-PST.[3SG]</ta>
            <ta e="T2568" id="Seg_2381" s="T2567">здесь</ta>
            <ta e="T2569" id="Seg_2382" s="T2568">яйцо.[NOM.SG]</ta>
            <ta e="T2570" id="Seg_2383" s="T2569">класть-PST.[3SG]</ta>
            <ta e="T2571" id="Seg_2384" s="T2570">масло.[NOM.SG]</ta>
            <ta e="T2572" id="Seg_2385" s="T2571">класть-PST.[3SG]</ta>
            <ta e="T0" id="Seg_2386" s="T2573">лавровый</ta>
            <ta e="T2574" id="Seg_2387" s="T0">лист.[NOM.SG]</ta>
            <ta e="T2575" id="Seg_2388" s="T2574">класть-PST.[3SG]</ta>
            <ta e="T2576" id="Seg_2389" s="T2575">тогда</ta>
            <ta e="T2577" id="Seg_2390" s="T2576">кипятить-PST.[3SG]</ta>
            <ta e="T2578" id="Seg_2391" s="T2577">этот.[NOM.SG]</ta>
            <ta e="T2580" id="Seg_2392" s="T2579">съесть-DUR.[3SG]</ta>
            <ta e="T2581" id="Seg_2393" s="T2580">и</ta>
            <ta e="T2582" id="Seg_2394" s="T2581">когда=INDEF</ta>
            <ta e="T2583" id="Seg_2395" s="T2582">такой.[NOM.SG]</ta>
            <ta e="T2584" id="Seg_2396" s="T2583">NEG</ta>
            <ta e="T2585" id="Seg_2397" s="T2584">съесть-PST-1SG</ta>
            <ta e="T2586" id="Seg_2398" s="T2585">тогда</ta>
            <ta e="T2588" id="Seg_2399" s="T2587">надеть-INF.LAT</ta>
            <ta e="T2589" id="Seg_2400" s="T2588">сапог-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2590" id="Seg_2401" s="T2589">а</ta>
            <ta e="T2591" id="Seg_2402" s="T2590">там</ta>
            <ta e="T2592" id="Seg_2403" s="T2591">отверстие-NOM/GEN/ACC.3PL</ta>
            <ta e="T2593" id="Seg_2404" s="T2592">стать-RES-PST-3PL</ta>
            <ta e="T2594" id="Seg_2405" s="T2593">нога-NOM/GEN.3SG</ta>
            <ta e="T2595" id="Seg_2406" s="T2594">здесь</ta>
            <ta e="T2596" id="Seg_2407" s="T2595">пойти-PST.[3SG]</ta>
            <ta e="T2597" id="Seg_2408" s="T2596">а</ta>
            <ta e="T2598" id="Seg_2409" s="T2597">ты.NOM</ta>
            <ta e="T2599" id="Seg_2410" s="T2598">сейчас</ta>
            <ta e="T2600" id="Seg_2411" s="T2599">смотреть-IMP.2SG</ta>
            <ta e="T2601" id="Seg_2412" s="T2600">я.NOM</ta>
            <ta e="T2602" id="Seg_2413" s="T2601">какой</ta>
            <ta e="T2603" id="Seg_2414" s="T2602">сапог-PL</ta>
            <ta e="T2604" id="Seg_2415" s="T2603">а</ta>
            <ta e="T2605" id="Seg_2416" s="T2604">ты.NOM</ta>
            <ta e="T2606" id="Seg_2417" s="T2605">скоро</ta>
            <ta e="T2607" id="Seg_2418" s="T2606">съесть-PST-2SG</ta>
            <ta e="T2608" id="Seg_2419" s="T2607">тогда</ta>
            <ta e="T2609" id="Seg_2420" s="T2608">пойти-PST-3PL</ta>
            <ta e="T2610" id="Seg_2421" s="T2609">деревня-VBLZ-CVB</ta>
            <ta e="T2611" id="Seg_2422" s="T2610">лошадь-INS</ta>
            <ta e="T2612" id="Seg_2423" s="T2611">тогда</ta>
            <ta e="T2613" id="Seg_2424" s="T2612">там</ta>
            <ta e="T2614" id="Seg_2425" s="T2613">вода.[NOM.SG]</ta>
            <ta e="T2615" id="Seg_2426" s="T2614">вода-LOC</ta>
            <ta e="T2616" id="Seg_2427" s="T2615">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T2618" id="Seg_2428" s="T2616">этот.[NOM.SG]</ta>
            <ta e="T2620" id="Seg_2429" s="T2618">железо.[NOM.SG]</ta>
            <ta e="T2621" id="Seg_2430" s="T2620">взять-MOM-PST.[3SG]</ta>
            <ta e="T2622" id="Seg_2431" s="T2621">этот.[NOM.SG]</ta>
            <ta e="T2623" id="Seg_2432" s="T2622">оставить-MOM-PST.[3SG]</ta>
            <ta e="T2624" id="Seg_2433" s="T2623">ты.NOM</ta>
            <ta e="T2625" id="Seg_2434" s="T2624">я.LAT</ta>
            <ta e="T2626" id="Seg_2435" s="T2625">NEG.AUX-IMP.2SG</ta>
            <ta e="T2627" id="Seg_2436" s="T2626">смотреть-CNG</ta>
            <ta e="T2628" id="Seg_2437" s="T2627">и</ta>
            <ta e="T2629" id="Seg_2438" s="T2628">кричать-DUR.[3SG]</ta>
            <ta e="T2630" id="Seg_2439" s="T2629">я.NOM</ta>
            <ta e="T2631" id="Seg_2440" s="T2630">остаться-MOM-PST-1SG</ta>
            <ta e="T2632" id="Seg_2441" s="T2631">а</ta>
            <ta e="T2633" id="Seg_2442" s="T2632">ты.NOM</ta>
            <ta e="T2634" id="Seg_2443" s="T2633">смотреть-FRQ-INF.LAT</ta>
            <ta e="T2635" id="Seg_2444" s="T2634">я.LAT</ta>
            <ta e="T2636" id="Seg_2445" s="T2635">не</ta>
            <ta e="T2638" id="Seg_2446" s="T2637">сидеть-DUR-IMP.2SG</ta>
            <ta e="T2639" id="Seg_2447" s="T2638">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T2640" id="Seg_2448" s="T2639">пойти-CVB</ta>
            <ta e="T2641" id="Seg_2449" s="T2640">исчезнуть-PST.[3SG]</ta>
            <ta e="T2642" id="Seg_2450" s="T2641">этот.[NOM.SG]</ta>
            <ta e="T2644" id="Seg_2451" s="T2643">скоро</ta>
            <ta e="T2645" id="Seg_2452" s="T2644">и</ta>
            <ta e="T2646" id="Seg_2453" s="T2645">там</ta>
            <ta e="T2647" id="Seg_2454" s="T2646">сидеть-DUR.[3SG]</ta>
            <ta e="T2648" id="Seg_2455" s="T2647">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2407" id="Seg_2456" s="T2406">v-v:tense-v:pn</ta>
            <ta e="T2408" id="Seg_2457" s="T2407">num-n:case</ta>
            <ta e="T2410" id="Seg_2458" s="T2409">n-n:case</ta>
            <ta e="T2411" id="Seg_2459" s="T2410">dempro-n:case</ta>
            <ta e="T2412" id="Seg_2460" s="T2411">v-v:tense-v:pn</ta>
            <ta e="T2413" id="Seg_2461" s="T2412">n-n:case</ta>
            <ta e="T2416" id="Seg_2462" s="T2415">n</ta>
            <ta e="T2417" id="Seg_2463" s="T2416">v-v:n.fin</ta>
            <ta e="T2419" id="Seg_2464" s="T2418">que</ta>
            <ta e="T2420" id="Seg_2465" s="T2419">pers</ta>
            <ta e="T2422" id="Seg_2466" s="T2421">v-v:tense-v:pn</ta>
            <ta e="T2424" id="Seg_2467" s="T2423">propr-n:case</ta>
            <ta e="T2425" id="Seg_2468" s="T2424">dempro-n:case</ta>
            <ta e="T2426" id="Seg_2469" s="T2425">v-v&gt;v-v:pn</ta>
            <ta e="T2428" id="Seg_2470" s="T2427">ptcl</ta>
            <ta e="T2429" id="Seg_2471" s="T2428">v-v:mood.pn</ta>
            <ta e="T2430" id="Seg_2472" s="T2429">v-v:ins-v:mood.pn</ta>
            <ta e="T2431" id="Seg_2473" s="T2430">pers</ta>
            <ta e="T2432" id="Seg_2474" s="T2431">num-n:case</ta>
            <ta e="T2433" id="Seg_2475" s="T2432">n-n:case</ta>
            <ta e="T2434" id="Seg_2476" s="T2433">dempro-n:case</ta>
            <ta e="T2435" id="Seg_2477" s="T2434">v-v:tense-v:pn</ta>
            <ta e="T2436" id="Seg_2478" s="T2435">v-v:tense-v:pn</ta>
            <ta e="T2437" id="Seg_2479" s="T2436">v-v:tense-v:pn</ta>
            <ta e="T2438" id="Seg_2480" s="T2437">v-v:tense-v:pn</ta>
            <ta e="T2439" id="Seg_2481" s="T2438">conj</ta>
            <ta e="T2440" id="Seg_2482" s="T2439">num-n:case</ta>
            <ta e="T2441" id="Seg_2483" s="T2440">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2442" id="Seg_2484" s="T2441">num-n:case</ta>
            <ta e="T2443" id="Seg_2485" s="T2442">v-v&gt;v-v:pn</ta>
            <ta e="T2444" id="Seg_2486" s="T2443">que-n:case</ta>
            <ta e="T2445" id="Seg_2487" s="T2444">pers</ta>
            <ta e="T2446" id="Seg_2488" s="T2445">num-n:case</ta>
            <ta e="T2447" id="Seg_2489" s="T2446">v-v:tense-v:pn</ta>
            <ta e="T2448" id="Seg_2490" s="T2447">pers</ta>
            <ta e="T2449" id="Seg_2491" s="T2448">num-n:case</ta>
            <ta e="T2451" id="Seg_2492" s="T2450">v-v:tense-v:pn</ta>
            <ta e="T2452" id="Seg_2493" s="T2451">v-v:ins-v:mood.pn</ta>
            <ta e="T2453" id="Seg_2494" s="T2452">conj</ta>
            <ta e="T2454" id="Seg_2495" s="T2453">pers</ta>
            <ta e="T2455" id="Seg_2496" s="T2454">num-n:case</ta>
            <ta e="T2456" id="Seg_2497" s="T2455">v-v:tense-v:pn</ta>
            <ta e="T2457" id="Seg_2498" s="T2456">adv</ta>
            <ta e="T2458" id="Seg_2499" s="T2457">dempro-n:case</ta>
            <ta e="T2459" id="Seg_2500" s="T2458">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2460" id="Seg_2501" s="T2459">v-v:ins-v:mood.pn</ta>
            <ta e="T2461" id="Seg_2502" s="T2460">adv</ta>
            <ta e="T2462" id="Seg_2503" s="T2461">dempro-n:case</ta>
            <ta e="T2464" id="Seg_2504" s="T2463">v-v:tense-v:pn</ta>
            <ta e="T2465" id="Seg_2505" s="T2464">conj</ta>
            <ta e="T2466" id="Seg_2506" s="T2465">v-v:n.fin</ta>
            <ta e="T2467" id="Seg_2507" s="T2466">v</ta>
            <ta e="T2468" id="Seg_2508" s="T2467">adv</ta>
            <ta e="T2470" id="Seg_2509" s="T2469">adv</ta>
            <ta e="T2471" id="Seg_2510" s="T2470">dempro-n:case</ta>
            <ta e="T2472" id="Seg_2511" s="T2471">v-v&gt;v-v:pn</ta>
            <ta e="T2473" id="Seg_2512" s="T2472">v-v:mood.pn</ta>
            <ta e="T2474" id="Seg_2513" s="T2473">adv</ta>
            <ta e="T2475" id="Seg_2514" s="T2474">v-v:ins-v:mood.pn</ta>
            <ta e="T2476" id="Seg_2515" s="T2475">pers</ta>
            <ta e="T2477" id="Seg_2516" s="T2476">n-n:case</ta>
            <ta e="T2478" id="Seg_2517" s="T2477">v-v:mood.pn</ta>
            <ta e="T2479" id="Seg_2518" s="T2478">n-n:case</ta>
            <ta e="T2480" id="Seg_2519" s="T2479">n-n:case</ta>
            <ta e="T2481" id="Seg_2520" s="T2480">n-n:case</ta>
            <ta e="T2482" id="Seg_2521" s="T2481">n-n:case</ta>
            <ta e="T2483" id="Seg_2522" s="T2482">dempro-n:case</ta>
            <ta e="T2484" id="Seg_2523" s="T2483">v-v:tense-v:pn</ta>
            <ta e="T2485" id="Seg_2524" s="T2484">v-v:tense-v:pn</ta>
            <ta e="T2487" id="Seg_2525" s="T2486">n-n:case</ta>
            <ta e="T2488" id="Seg_2526" s="T2487">v-v:tense-v:pn</ta>
            <ta e="T2489" id="Seg_2527" s="T2488">conj</ta>
            <ta e="T2490" id="Seg_2528" s="T2489">v-v&gt;v-v:pn</ta>
            <ta e="T2491" id="Seg_2529" s="T2490">conj</ta>
            <ta e="T2492" id="Seg_2530" s="T2491">adv</ta>
            <ta e="T2493" id="Seg_2531" s="T2492">conj</ta>
            <ta e="T2494" id="Seg_2532" s="T2493">adv</ta>
            <ta e="T2495" id="Seg_2533" s="T2494">conj</ta>
            <ta e="T2496" id="Seg_2534" s="T2495">n-n:case.poss</ta>
            <ta e="T2497" id="Seg_2535" s="T2496">conj</ta>
            <ta e="T2498" id="Seg_2536" s="T2497">n-n:case.poss</ta>
            <ta e="T2499" id="Seg_2537" s="T2498">adv</ta>
            <ta e="T2500" id="Seg_2538" s="T2499">dempro-n:case</ta>
            <ta e="T2501" id="Seg_2539" s="T2500">v-v&gt;v-v:pn</ta>
            <ta e="T2502" id="Seg_2540" s="T2501">conj</ta>
            <ta e="T2503" id="Seg_2541" s="T2502">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T2504" id="Seg_2542" s="T2503">pers</ta>
            <ta e="T2505" id="Seg_2543" s="T2504">v-v:tense-v:pn</ta>
            <ta e="T2506" id="Seg_2544" s="T2505">n-n:case</ta>
            <ta e="T2507" id="Seg_2545" s="T2506">n-n:case</ta>
            <ta e="T2508" id="Seg_2546" s="T2507">conj</ta>
            <ta e="T2509" id="Seg_2547" s="T2508">pers</ta>
            <ta e="T2510" id="Seg_2548" s="T2509">dempro-n:case</ta>
            <ta e="T2511" id="Seg_2549" s="T2510">n-n:case.poss</ta>
            <ta e="T2512" id="Seg_2550" s="T2511">conj</ta>
            <ta e="T2513" id="Seg_2551" s="T2512">n-n:case.poss</ta>
            <ta e="T2514" id="Seg_2552" s="T2513">v-v:tense-v:pn</ta>
            <ta e="T2515" id="Seg_2553" s="T2514">n-n:case</ta>
            <ta e="T2516" id="Seg_2554" s="T2515">adv</ta>
            <ta e="T2517" id="Seg_2555" s="T2516">ptcl</ta>
            <ta e="T2518" id="Seg_2556" s="T2517">adv</ta>
            <ta e="T2520" id="Seg_2557" s="T2519">adv</ta>
            <ta e="T2521" id="Seg_2558" s="T2520">adv</ta>
            <ta e="T2522" id="Seg_2559" s="T2521">num-n:case</ta>
            <ta e="T2523" id="Seg_2560" s="T2522">adv</ta>
            <ta e="T2524" id="Seg_2561" s="T2523">dempro-n:case</ta>
            <ta e="T2525" id="Seg_2562" s="T2524">v-v:tense-v:pn</ta>
            <ta e="T2527" id="Seg_2563" s="T2526">dempro-n:case</ta>
            <ta e="T2528" id="Seg_2564" s="T2527">num-n:case</ta>
            <ta e="T2529" id="Seg_2565" s="T2528">dempro-n:case</ta>
            <ta e="T2530" id="Seg_2566" s="T2529">v-v:tense-v:pn</ta>
            <ta e="T2531" id="Seg_2567" s="T2530">dempro-n:case</ta>
            <ta e="T2532" id="Seg_2568" s="T2531">n-n:case</ta>
            <ta e="T2533" id="Seg_2569" s="T2532">dempro-n:case</ta>
            <ta e="T2534" id="Seg_2570" s="T2533">ptcl</ta>
            <ta e="T2535" id="Seg_2571" s="T2534">n-n:case</ta>
            <ta e="T2536" id="Seg_2572" s="T2535">v-v:tense-v:pn</ta>
            <ta e="T2537" id="Seg_2573" s="T2536">n-n:case</ta>
            <ta e="T2538" id="Seg_2574" s="T2537">n-n:case</ta>
            <ta e="T2539" id="Seg_2575" s="T2538">v-v&gt;v-v:pn</ta>
            <ta e="T2540" id="Seg_2576" s="T2539">ptcl</ta>
            <ta e="T2541" id="Seg_2577" s="T2540">adv</ta>
            <ta e="T2542" id="Seg_2578" s="T2541">adj</ta>
            <ta e="T2544" id="Seg_2579" s="T2543">n-n:case</ta>
            <ta e="T2545" id="Seg_2580" s="T2544">adv</ta>
            <ta e="T2546" id="Seg_2581" s="T2545">v-v:tense-v:pn</ta>
            <ta e="T2547" id="Seg_2582" s="T2546">dempro-n:case</ta>
            <ta e="T2548" id="Seg_2583" s="T2547">v-v&gt;v-v:pn</ta>
            <ta e="T2549" id="Seg_2584" s="T2548">v-v:ins-v:mood.pn</ta>
            <ta e="T2550" id="Seg_2585" s="T2549">que=ptcl</ta>
            <ta e="T2551" id="Seg_2586" s="T2550">pers</ta>
            <ta e="T2552" id="Seg_2587" s="T2551">ptcl</ta>
            <ta e="T2553" id="Seg_2588" s="T2552">v-v:tense-v:pn</ta>
            <ta e="T2554" id="Seg_2589" s="T2553">ptcl</ta>
            <ta e="T2555" id="Seg_2590" s="T2554">v-v:ins-v:mood.pn</ta>
            <ta e="T2556" id="Seg_2591" s="T2555">dempro-n:case</ta>
            <ta e="T2557" id="Seg_2592" s="T2556">v-v&gt;v-v:pn</ta>
            <ta e="T2558" id="Seg_2593" s="T2557">que-n:case</ta>
            <ta e="T2559" id="Seg_2594" s="T2558">v-v:n.fin</ta>
            <ta e="T2560" id="Seg_2595" s="T2559">dempro-n:case</ta>
            <ta e="T2561" id="Seg_2596" s="T2560">adv</ta>
            <ta e="T2562" id="Seg_2597" s="T2561">n-n:case.poss</ta>
            <ta e="T2563" id="Seg_2598" s="T2562">v-v:tense-v:pn</ta>
            <ta e="T2565" id="Seg_2599" s="T2564">v-v:tense-v:pn</ta>
            <ta e="T2566" id="Seg_2600" s="T2565">conj</ta>
            <ta e="T2567" id="Seg_2601" s="T2566">v-v:tense-v:pn</ta>
            <ta e="T2568" id="Seg_2602" s="T2567">adv</ta>
            <ta e="T2569" id="Seg_2603" s="T2568">n-n:case</ta>
            <ta e="T2570" id="Seg_2604" s="T2569">v-v:tense-v:pn</ta>
            <ta e="T2571" id="Seg_2605" s="T2570">n-n:case</ta>
            <ta e="T2572" id="Seg_2606" s="T2571">v-v:tense-v:pn</ta>
            <ta e="T0" id="Seg_2607" s="T2573">adj</ta>
            <ta e="T2574" id="Seg_2608" s="T0">n-n:case</ta>
            <ta e="T2575" id="Seg_2609" s="T2574">v-v:tense-v:pn</ta>
            <ta e="T2576" id="Seg_2610" s="T2575">adv</ta>
            <ta e="T2577" id="Seg_2611" s="T2576">v-v:tense-v:pn</ta>
            <ta e="T2578" id="Seg_2612" s="T2577">dempro-n:case</ta>
            <ta e="T2580" id="Seg_2613" s="T2579">v-v&gt;v-v:pn</ta>
            <ta e="T2581" id="Seg_2614" s="T2580">conj</ta>
            <ta e="T2582" id="Seg_2615" s="T2581">que=ptcl</ta>
            <ta e="T2583" id="Seg_2616" s="T2582">adj-n:case</ta>
            <ta e="T2584" id="Seg_2617" s="T2583">ptcl</ta>
            <ta e="T2585" id="Seg_2618" s="T2584">v-v:tense-v:pn</ta>
            <ta e="T2586" id="Seg_2619" s="T2585">adv</ta>
            <ta e="T2588" id="Seg_2620" s="T2587">v-v:n.fin</ta>
            <ta e="T2589" id="Seg_2621" s="T2588">n-n:num-n:case.poss</ta>
            <ta e="T2590" id="Seg_2622" s="T2589">conj</ta>
            <ta e="T2591" id="Seg_2623" s="T2590">adv</ta>
            <ta e="T2592" id="Seg_2624" s="T2591">n-n:case.poss</ta>
            <ta e="T2593" id="Seg_2625" s="T2592">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2594" id="Seg_2626" s="T2593">n-n:case.poss</ta>
            <ta e="T2595" id="Seg_2627" s="T2594">adv</ta>
            <ta e="T2596" id="Seg_2628" s="T2595">v-v:tense-v:pn</ta>
            <ta e="T2597" id="Seg_2629" s="T2596">conj</ta>
            <ta e="T2598" id="Seg_2630" s="T2597">pers</ta>
            <ta e="T2599" id="Seg_2631" s="T2598">adv</ta>
            <ta e="T2600" id="Seg_2632" s="T2599">v-v:mood.pn</ta>
            <ta e="T2601" id="Seg_2633" s="T2600">pers</ta>
            <ta e="T2602" id="Seg_2634" s="T2601">que</ta>
            <ta e="T2603" id="Seg_2635" s="T2602">n-n:num</ta>
            <ta e="T2604" id="Seg_2636" s="T2603">conj</ta>
            <ta e="T2605" id="Seg_2637" s="T2604">pers</ta>
            <ta e="T2606" id="Seg_2638" s="T2605">adv</ta>
            <ta e="T2607" id="Seg_2639" s="T2606">v-v:tense-v:pn</ta>
            <ta e="T2608" id="Seg_2640" s="T2607">adv</ta>
            <ta e="T2609" id="Seg_2641" s="T2608">v-v:tense-v:pn</ta>
            <ta e="T2610" id="Seg_2642" s="T2609">n-n&gt;v-v:n.fin</ta>
            <ta e="T2611" id="Seg_2643" s="T2610">n-n:case</ta>
            <ta e="T2612" id="Seg_2644" s="T2611">adv</ta>
            <ta e="T2613" id="Seg_2645" s="T2612">adv</ta>
            <ta e="T2614" id="Seg_2646" s="T2613">n-n:case</ta>
            <ta e="T2615" id="Seg_2647" s="T2614">n-n:case</ta>
            <ta e="T2616" id="Seg_2648" s="T2615">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2618" id="Seg_2649" s="T2616">dempro-n:case</ta>
            <ta e="T2620" id="Seg_2650" s="T2618">n-n:case</ta>
            <ta e="T2621" id="Seg_2651" s="T2620">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2622" id="Seg_2652" s="T2621">dempro-n:case</ta>
            <ta e="T2623" id="Seg_2653" s="T2622">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2624" id="Seg_2654" s="T2623">pers</ta>
            <ta e="T2625" id="Seg_2655" s="T2624">pers</ta>
            <ta e="T2626" id="Seg_2656" s="T2625">aux-v:mood.pn</ta>
            <ta e="T2627" id="Seg_2657" s="T2626">v-v:n.fin</ta>
            <ta e="T2628" id="Seg_2658" s="T2627">conj</ta>
            <ta e="T2629" id="Seg_2659" s="T2628">v-v&gt;v-v:pn</ta>
            <ta e="T2630" id="Seg_2660" s="T2629">pers</ta>
            <ta e="T2631" id="Seg_2661" s="T2630">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2632" id="Seg_2662" s="T2631">conj</ta>
            <ta e="T2633" id="Seg_2663" s="T2632">pers</ta>
            <ta e="T2634" id="Seg_2664" s="T2633">v-v&gt;v-v:n.fin</ta>
            <ta e="T2635" id="Seg_2665" s="T2634">pers</ta>
            <ta e="T2636" id="Seg_2666" s="T2635">ptcl</ta>
            <ta e="T2638" id="Seg_2667" s="T2637">v-v&gt;v-v:mood.pn</ta>
            <ta e="T2639" id="Seg_2668" s="T2638">refl-n:case.poss</ta>
            <ta e="T2640" id="Seg_2669" s="T2639">v-v:n.fin</ta>
            <ta e="T2641" id="Seg_2670" s="T2640">v-v:tense-v:pn</ta>
            <ta e="T2642" id="Seg_2671" s="T2641">dempro-n:case</ta>
            <ta e="T2644" id="Seg_2672" s="T2643">adv</ta>
            <ta e="T2645" id="Seg_2673" s="T2644">conj</ta>
            <ta e="T2646" id="Seg_2674" s="T2645">adv</ta>
            <ta e="T2647" id="Seg_2675" s="T2646">v-v&gt;v-v:pn</ta>
            <ta e="T2648" id="Seg_2676" s="T2647">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2407" id="Seg_2677" s="T2406">v</ta>
            <ta e="T2408" id="Seg_2678" s="T2407">num</ta>
            <ta e="T2410" id="Seg_2679" s="T2409">n</ta>
            <ta e="T2411" id="Seg_2680" s="T2410">dempro</ta>
            <ta e="T2412" id="Seg_2681" s="T2411">v</ta>
            <ta e="T2413" id="Seg_2682" s="T2412">n</ta>
            <ta e="T2416" id="Seg_2683" s="T2415">n</ta>
            <ta e="T2417" id="Seg_2684" s="T2416">v</ta>
            <ta e="T2419" id="Seg_2685" s="T2418">que</ta>
            <ta e="T2420" id="Seg_2686" s="T2419">pers</ta>
            <ta e="T2422" id="Seg_2687" s="T2421">v</ta>
            <ta e="T2424" id="Seg_2688" s="T2423">propr</ta>
            <ta e="T2425" id="Seg_2689" s="T2424">dempro</ta>
            <ta e="T2426" id="Seg_2690" s="T2425">v</ta>
            <ta e="T2428" id="Seg_2691" s="T2427">ptcl</ta>
            <ta e="T2429" id="Seg_2692" s="T2428">v</ta>
            <ta e="T2430" id="Seg_2693" s="T2429">v</ta>
            <ta e="T2431" id="Seg_2694" s="T2430">pers</ta>
            <ta e="T2432" id="Seg_2695" s="T2431">num</ta>
            <ta e="T2433" id="Seg_2696" s="T2432">n</ta>
            <ta e="T2434" id="Seg_2697" s="T2433">dempro</ta>
            <ta e="T2435" id="Seg_2698" s="T2434">v</ta>
            <ta e="T2436" id="Seg_2699" s="T2435">v</ta>
            <ta e="T2437" id="Seg_2700" s="T2436">v</ta>
            <ta e="T2438" id="Seg_2701" s="T2437">v</ta>
            <ta e="T2439" id="Seg_2702" s="T2438">conj</ta>
            <ta e="T2440" id="Seg_2703" s="T2439">num</ta>
            <ta e="T2441" id="Seg_2704" s="T2440">v</ta>
            <ta e="T2442" id="Seg_2705" s="T2441">num</ta>
            <ta e="T2443" id="Seg_2706" s="T2442">v</ta>
            <ta e="T2444" id="Seg_2707" s="T2443">que</ta>
            <ta e="T2445" id="Seg_2708" s="T2444">pers</ta>
            <ta e="T2446" id="Seg_2709" s="T2445">num</ta>
            <ta e="T2447" id="Seg_2710" s="T2446">v</ta>
            <ta e="T2448" id="Seg_2711" s="T2447">pers</ta>
            <ta e="T2449" id="Seg_2712" s="T2448">num</ta>
            <ta e="T2451" id="Seg_2713" s="T2450">v</ta>
            <ta e="T2452" id="Seg_2714" s="T2451">v</ta>
            <ta e="T2453" id="Seg_2715" s="T2452">conj</ta>
            <ta e="T2454" id="Seg_2716" s="T2453">pers</ta>
            <ta e="T2455" id="Seg_2717" s="T2454">num</ta>
            <ta e="T2456" id="Seg_2718" s="T2455">v</ta>
            <ta e="T2457" id="Seg_2719" s="T2456">adv</ta>
            <ta e="T2458" id="Seg_2720" s="T2457">dempro</ta>
            <ta e="T2459" id="Seg_2721" s="T2458">v</ta>
            <ta e="T2460" id="Seg_2722" s="T2459">v</ta>
            <ta e="T2461" id="Seg_2723" s="T2460">adv</ta>
            <ta e="T2462" id="Seg_2724" s="T2461">dempro</ta>
            <ta e="T2464" id="Seg_2725" s="T2463">v</ta>
            <ta e="T2465" id="Seg_2726" s="T2464">conj</ta>
            <ta e="T2466" id="Seg_2727" s="T2465">v</ta>
            <ta e="T2467" id="Seg_2728" s="T2466">v</ta>
            <ta e="T2468" id="Seg_2729" s="T2467">adv</ta>
            <ta e="T2470" id="Seg_2730" s="T2469">adv</ta>
            <ta e="T2471" id="Seg_2731" s="T2470">dempro</ta>
            <ta e="T2472" id="Seg_2732" s="T2471">v</ta>
            <ta e="T2473" id="Seg_2733" s="T2472">v</ta>
            <ta e="T2474" id="Seg_2734" s="T2473">adv</ta>
            <ta e="T2475" id="Seg_2735" s="T2474">v</ta>
            <ta e="T2476" id="Seg_2736" s="T2475">pers</ta>
            <ta e="T2477" id="Seg_2737" s="T2476">n</ta>
            <ta e="T2478" id="Seg_2738" s="T2477">v</ta>
            <ta e="T2479" id="Seg_2739" s="T2478">n</ta>
            <ta e="T2480" id="Seg_2740" s="T2479">n</ta>
            <ta e="T2481" id="Seg_2741" s="T2480">n</ta>
            <ta e="T2482" id="Seg_2742" s="T2481">n</ta>
            <ta e="T2483" id="Seg_2743" s="T2482">dempro</ta>
            <ta e="T2484" id="Seg_2744" s="T2483">v</ta>
            <ta e="T2485" id="Seg_2745" s="T2484">v</ta>
            <ta e="T2487" id="Seg_2746" s="T2486">n</ta>
            <ta e="T2488" id="Seg_2747" s="T2487">v</ta>
            <ta e="T2489" id="Seg_2748" s="T2488">conj</ta>
            <ta e="T2490" id="Seg_2749" s="T2489">v</ta>
            <ta e="T2491" id="Seg_2750" s="T2490">conj</ta>
            <ta e="T2492" id="Seg_2751" s="T2491">adv</ta>
            <ta e="T2493" id="Seg_2752" s="T2492">conj</ta>
            <ta e="T2494" id="Seg_2753" s="T2493">adv</ta>
            <ta e="T2495" id="Seg_2754" s="T2494">conj</ta>
            <ta e="T2496" id="Seg_2755" s="T2495">n</ta>
            <ta e="T2497" id="Seg_2756" s="T2496">conj</ta>
            <ta e="T2498" id="Seg_2757" s="T2497">n</ta>
            <ta e="T2499" id="Seg_2758" s="T2498">adv</ta>
            <ta e="T2500" id="Seg_2759" s="T2499">dempro</ta>
            <ta e="T2501" id="Seg_2760" s="T2500">v</ta>
            <ta e="T2502" id="Seg_2761" s="T2501">conj</ta>
            <ta e="T2503" id="Seg_2762" s="T2502">v</ta>
            <ta e="T2504" id="Seg_2763" s="T2503">pers</ta>
            <ta e="T2505" id="Seg_2764" s="T2504">v</ta>
            <ta e="T2506" id="Seg_2765" s="T2505">n</ta>
            <ta e="T2507" id="Seg_2766" s="T2506">n</ta>
            <ta e="T2508" id="Seg_2767" s="T2507">conj</ta>
            <ta e="T2509" id="Seg_2768" s="T2508">pers</ta>
            <ta e="T2510" id="Seg_2769" s="T2509">dempro</ta>
            <ta e="T2511" id="Seg_2770" s="T2510">n</ta>
            <ta e="T2512" id="Seg_2771" s="T2511">conj</ta>
            <ta e="T2513" id="Seg_2772" s="T2512">n</ta>
            <ta e="T2514" id="Seg_2773" s="T2513">v</ta>
            <ta e="T2515" id="Seg_2774" s="T2514">n</ta>
            <ta e="T2516" id="Seg_2775" s="T2515">adv</ta>
            <ta e="T2517" id="Seg_2776" s="T2516">ptcl</ta>
            <ta e="T2518" id="Seg_2777" s="T2517">adv</ta>
            <ta e="T2520" id="Seg_2778" s="T2519">adv</ta>
            <ta e="T2521" id="Seg_2779" s="T2520">adv</ta>
            <ta e="T2522" id="Seg_2780" s="T2521">num</ta>
            <ta e="T2523" id="Seg_2781" s="T2522">adv</ta>
            <ta e="T2524" id="Seg_2782" s="T2523">dempro</ta>
            <ta e="T2525" id="Seg_2783" s="T2524">v</ta>
            <ta e="T2527" id="Seg_2784" s="T2526">dempro</ta>
            <ta e="T2528" id="Seg_2785" s="T2527">num</ta>
            <ta e="T2529" id="Seg_2786" s="T2528">dempro</ta>
            <ta e="T2530" id="Seg_2787" s="T2529">v</ta>
            <ta e="T2531" id="Seg_2788" s="T2530">dempro</ta>
            <ta e="T2532" id="Seg_2789" s="T2531">n</ta>
            <ta e="T2533" id="Seg_2790" s="T2532">dempro</ta>
            <ta e="T2534" id="Seg_2791" s="T2533">ptcl</ta>
            <ta e="T2535" id="Seg_2792" s="T2534">n</ta>
            <ta e="T2536" id="Seg_2793" s="T2535">v</ta>
            <ta e="T2537" id="Seg_2794" s="T2536">n</ta>
            <ta e="T2538" id="Seg_2795" s="T2537">n</ta>
            <ta e="T2539" id="Seg_2796" s="T2538">v</ta>
            <ta e="T2540" id="Seg_2797" s="T2539">ptcl</ta>
            <ta e="T2541" id="Seg_2798" s="T2540">adv</ta>
            <ta e="T2542" id="Seg_2799" s="T2541">adj</ta>
            <ta e="T2544" id="Seg_2800" s="T2543">n</ta>
            <ta e="T2545" id="Seg_2801" s="T2544">adv</ta>
            <ta e="T2546" id="Seg_2802" s="T2545">v</ta>
            <ta e="T2547" id="Seg_2803" s="T2546">dempro</ta>
            <ta e="T2548" id="Seg_2804" s="T2547">v</ta>
            <ta e="T2549" id="Seg_2805" s="T2548">v</ta>
            <ta e="T2550" id="Seg_2806" s="T2549">que</ta>
            <ta e="T2551" id="Seg_2807" s="T2550">pers</ta>
            <ta e="T2552" id="Seg_2808" s="T2551">ptcl</ta>
            <ta e="T2553" id="Seg_2809" s="T2552">v</ta>
            <ta e="T2554" id="Seg_2810" s="T2553">ptcl</ta>
            <ta e="T2555" id="Seg_2811" s="T2554">v</ta>
            <ta e="T2556" id="Seg_2812" s="T2555">dempro</ta>
            <ta e="T2557" id="Seg_2813" s="T2556">v</ta>
            <ta e="T2558" id="Seg_2814" s="T2557">que</ta>
            <ta e="T2559" id="Seg_2815" s="T2558">v</ta>
            <ta e="T2560" id="Seg_2816" s="T2559">dempro</ta>
            <ta e="T2561" id="Seg_2817" s="T2560">adv</ta>
            <ta e="T2562" id="Seg_2818" s="T2561">n</ta>
            <ta e="T2563" id="Seg_2819" s="T2562">v</ta>
            <ta e="T2565" id="Seg_2820" s="T2564">v</ta>
            <ta e="T2566" id="Seg_2821" s="T2565">conj</ta>
            <ta e="T2567" id="Seg_2822" s="T2566">v</ta>
            <ta e="T2568" id="Seg_2823" s="T2567">adv</ta>
            <ta e="T2569" id="Seg_2824" s="T2568">n</ta>
            <ta e="T2570" id="Seg_2825" s="T2569">v</ta>
            <ta e="T2571" id="Seg_2826" s="T2570">n</ta>
            <ta e="T2572" id="Seg_2827" s="T2571">v</ta>
            <ta e="T0" id="Seg_2828" s="T2573">adj</ta>
            <ta e="T2574" id="Seg_2829" s="T0">n</ta>
            <ta e="T2575" id="Seg_2830" s="T2574">v</ta>
            <ta e="T2576" id="Seg_2831" s="T2575">adv</ta>
            <ta e="T2577" id="Seg_2832" s="T2576">v</ta>
            <ta e="T2578" id="Seg_2833" s="T2577">dempro</ta>
            <ta e="T2580" id="Seg_2834" s="T2579">v</ta>
            <ta e="T2581" id="Seg_2835" s="T2580">conj</ta>
            <ta e="T2582" id="Seg_2836" s="T2581">que</ta>
            <ta e="T2583" id="Seg_2837" s="T2582">adj</ta>
            <ta e="T2584" id="Seg_2838" s="T2583">ptcl</ta>
            <ta e="T2585" id="Seg_2839" s="T2584">v</ta>
            <ta e="T2586" id="Seg_2840" s="T2585">adv</ta>
            <ta e="T2588" id="Seg_2841" s="T2587">v</ta>
            <ta e="T2589" id="Seg_2842" s="T2588">n</ta>
            <ta e="T2590" id="Seg_2843" s="T2589">conj</ta>
            <ta e="T2591" id="Seg_2844" s="T2590">adv</ta>
            <ta e="T2592" id="Seg_2845" s="T2591">n</ta>
            <ta e="T2593" id="Seg_2846" s="T2592">v</ta>
            <ta e="T2594" id="Seg_2847" s="T2593">n</ta>
            <ta e="T2595" id="Seg_2848" s="T2594">adv</ta>
            <ta e="T2596" id="Seg_2849" s="T2595">v</ta>
            <ta e="T2597" id="Seg_2850" s="T2596">conj</ta>
            <ta e="T2598" id="Seg_2851" s="T2597">pers</ta>
            <ta e="T2599" id="Seg_2852" s="T2598">adv</ta>
            <ta e="T2600" id="Seg_2853" s="T2599">v</ta>
            <ta e="T2601" id="Seg_2854" s="T2600">pers</ta>
            <ta e="T2602" id="Seg_2855" s="T2601">que</ta>
            <ta e="T2603" id="Seg_2856" s="T2602">n</ta>
            <ta e="T2604" id="Seg_2857" s="T2603">conj</ta>
            <ta e="T2605" id="Seg_2858" s="T2604">pers</ta>
            <ta e="T2606" id="Seg_2859" s="T2605">adv</ta>
            <ta e="T2607" id="Seg_2860" s="T2606">v</ta>
            <ta e="T2608" id="Seg_2861" s="T2607">adv</ta>
            <ta e="T2609" id="Seg_2862" s="T2608">v</ta>
            <ta e="T2610" id="Seg_2863" s="T2609">v</ta>
            <ta e="T2611" id="Seg_2864" s="T2610">n</ta>
            <ta e="T2612" id="Seg_2865" s="T2611">adv</ta>
            <ta e="T2613" id="Seg_2866" s="T2612">adv</ta>
            <ta e="T2614" id="Seg_2867" s="T2613">n</ta>
            <ta e="T2615" id="Seg_2868" s="T2614">n</ta>
            <ta e="T2616" id="Seg_2869" s="T2615">v</ta>
            <ta e="T2618" id="Seg_2870" s="T2616">dempro</ta>
            <ta e="T2620" id="Seg_2871" s="T2618">n</ta>
            <ta e="T2621" id="Seg_2872" s="T2620">v</ta>
            <ta e="T2622" id="Seg_2873" s="T2621">dempro</ta>
            <ta e="T2623" id="Seg_2874" s="T2622">v</ta>
            <ta e="T2624" id="Seg_2875" s="T2623">pers</ta>
            <ta e="T2625" id="Seg_2876" s="T2624">pers</ta>
            <ta e="T2626" id="Seg_2877" s="T2625">aux</ta>
            <ta e="T2627" id="Seg_2878" s="T2626">v</ta>
            <ta e="T2628" id="Seg_2879" s="T2627">conj</ta>
            <ta e="T2629" id="Seg_2880" s="T2628">v</ta>
            <ta e="T2630" id="Seg_2881" s="T2629">pers</ta>
            <ta e="T2631" id="Seg_2882" s="T2630">v</ta>
            <ta e="T2632" id="Seg_2883" s="T2631">conj</ta>
            <ta e="T2633" id="Seg_2884" s="T2632">pers</ta>
            <ta e="T2634" id="Seg_2885" s="T2633">v</ta>
            <ta e="T2635" id="Seg_2886" s="T2634">pers</ta>
            <ta e="T2636" id="Seg_2887" s="T2635">ptcl</ta>
            <ta e="T2638" id="Seg_2888" s="T2637">v</ta>
            <ta e="T2639" id="Seg_2889" s="T2638">refl</ta>
            <ta e="T2640" id="Seg_2890" s="T2639">v</ta>
            <ta e="T2641" id="Seg_2891" s="T2640">v</ta>
            <ta e="T2642" id="Seg_2892" s="T2641">dempro</ta>
            <ta e="T2644" id="Seg_2893" s="T2643">adv</ta>
            <ta e="T2645" id="Seg_2894" s="T2644">conj</ta>
            <ta e="T2646" id="Seg_2895" s="T2645">adv</ta>
            <ta e="T2647" id="Seg_2896" s="T2646">v</ta>
            <ta e="T2648" id="Seg_2897" s="T2647">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2410" id="Seg_2898" s="T2409">np.h:E</ta>
            <ta e="T2411" id="Seg_2899" s="T2410">pro:G</ta>
            <ta e="T2413" id="Seg_2900" s="T2412">np.h:A</ta>
            <ta e="T2420" id="Seg_2901" s="T2419">pro.h:Th</ta>
            <ta e="T2422" id="Seg_2902" s="T2421">0.3.h:A</ta>
            <ta e="T2425" id="Seg_2903" s="T2424">pro.h:A</ta>
            <ta e="T2429" id="Seg_2904" s="T2428">0.2.h:E</ta>
            <ta e="T2430" id="Seg_2905" s="T2429">0.2.h:A</ta>
            <ta e="T2431" id="Seg_2906" s="T2430">pro.h:B</ta>
            <ta e="T2433" id="Seg_2907" s="T2432">np:P</ta>
            <ta e="T2434" id="Seg_2908" s="T2433">pro.h:A</ta>
            <ta e="T2436" id="Seg_2909" s="T2435">0.3.h:A</ta>
            <ta e="T2437" id="Seg_2910" s="T2436">0.3.h:A</ta>
            <ta e="T2438" id="Seg_2911" s="T2437">0.3.h:A</ta>
            <ta e="T2440" id="Seg_2912" s="T2439">np:P</ta>
            <ta e="T2441" id="Seg_2913" s="T2440">0.3.h:A</ta>
            <ta e="T2442" id="Seg_2914" s="T2441">np:Th</ta>
            <ta e="T2443" id="Seg_2915" s="T2442">0.3.h:A</ta>
            <ta e="T2445" id="Seg_2916" s="T2444">pro.h:A</ta>
            <ta e="T2446" id="Seg_2917" s="T2445">np:Th</ta>
            <ta e="T2448" id="Seg_2918" s="T2447">pro.h:A</ta>
            <ta e="T2449" id="Seg_2919" s="T2448">np:P</ta>
            <ta e="T2452" id="Seg_2920" s="T2451">0.2.h:A</ta>
            <ta e="T2454" id="Seg_2921" s="T2453">pro.h:A</ta>
            <ta e="T2455" id="Seg_2922" s="T2454">np:P</ta>
            <ta e="T2457" id="Seg_2923" s="T2456">adv:Time</ta>
            <ta e="T2458" id="Seg_2924" s="T2457">pro.h:A</ta>
            <ta e="T2460" id="Seg_2925" s="T2459">0.2.h:A</ta>
            <ta e="T2461" id="Seg_2926" s="T2460">adv:G</ta>
            <ta e="T2462" id="Seg_2927" s="T2461">pro.h:A</ta>
            <ta e="T2468" id="Seg_2928" s="T2467">adv:So</ta>
            <ta e="T2470" id="Seg_2929" s="T2469">adv:Time</ta>
            <ta e="T2471" id="Seg_2930" s="T2470">pro.h:A</ta>
            <ta e="T2473" id="Seg_2931" s="T2472">0.2.h:A</ta>
            <ta e="T2474" id="Seg_2932" s="T2473">adv:G</ta>
            <ta e="T2475" id="Seg_2933" s="T2474">0.2.h:A</ta>
            <ta e="T2476" id="Seg_2934" s="T2475">pro.h:B</ta>
            <ta e="T2477" id="Seg_2935" s="T2476">np:Th</ta>
            <ta e="T2478" id="Seg_2936" s="T2477">0.2.h:A</ta>
            <ta e="T2479" id="Seg_2937" s="T2478">np:So</ta>
            <ta e="T2480" id="Seg_2938" s="T2479">np:G</ta>
            <ta e="T2481" id="Seg_2939" s="T2480">np:So</ta>
            <ta e="T2482" id="Seg_2940" s="T2481">np:G</ta>
            <ta e="T2483" id="Seg_2941" s="T2482">pro.h:A</ta>
            <ta e="T2485" id="Seg_2942" s="T2484">0.3.h:A</ta>
            <ta e="T2487" id="Seg_2943" s="T2486">np:Th</ta>
            <ta e="T2488" id="Seg_2944" s="T2487">0.3.h:A</ta>
            <ta e="T2490" id="Seg_2945" s="T2489">0.3.h:A</ta>
            <ta e="T2491" id="Seg_2946" s="T2490">adv:Time</ta>
            <ta e="T2492" id="Seg_2947" s="T2491">adv:L</ta>
            <ta e="T2493" id="Seg_2948" s="T2492">adv:Time</ta>
            <ta e="T2494" id="Seg_2949" s="T2493">adv:L</ta>
            <ta e="T2495" id="Seg_2950" s="T2494">adv:Time</ta>
            <ta e="T2496" id="Seg_2951" s="T2495">np:L 0.3.h:Poss</ta>
            <ta e="T2497" id="Seg_2952" s="T2496">adv:Time</ta>
            <ta e="T2498" id="Seg_2953" s="T2497">np:L 0.3.h:Poss</ta>
            <ta e="T2499" id="Seg_2954" s="T2498">adv:Time</ta>
            <ta e="T2500" id="Seg_2955" s="T2499">pro.h:A</ta>
            <ta e="T2503" id="Seg_2956" s="T2502">0.3.h:A</ta>
            <ta e="T2504" id="Seg_2957" s="T2503">pro.h:A</ta>
            <ta e="T2506" id="Seg_2958" s="T2505">np:So</ta>
            <ta e="T2507" id="Seg_2959" s="T2506">np:G</ta>
            <ta e="T2509" id="Seg_2960" s="T2508">pro.h:A</ta>
            <ta e="T2510" id="Seg_2961" s="T2509">pro:Th</ta>
            <ta e="T2511" id="Seg_2962" s="T2510">np:L</ta>
            <ta e="T2513" id="Seg_2963" s="T2512">np:L</ta>
            <ta e="T2515" id="Seg_2964" s="T2514">np:Th</ta>
            <ta e="T2522" id="Seg_2965" s="T2521">np:Th</ta>
            <ta e="T2523" id="Seg_2966" s="T2522">adv:Time</ta>
            <ta e="T2524" id="Seg_2967" s="T2523">pro.h:A</ta>
            <ta e="T2527" id="Seg_2968" s="T2526">pro.h:R</ta>
            <ta e="T2528" id="Seg_2969" s="T2527">np:Th</ta>
            <ta e="T2529" id="Seg_2970" s="T2528">pro.h:A</ta>
            <ta e="T2533" id="Seg_2971" s="T2532">pro.h:A</ta>
            <ta e="T2535" id="Seg_2972" s="T2534">np:Th</ta>
            <ta e="T2537" id="Seg_2973" s="T2536">np:So</ta>
            <ta e="T2538" id="Seg_2974" s="T2537">np:G</ta>
            <ta e="T2539" id="Seg_2975" s="T2538">0.3:Th</ta>
            <ta e="T2545" id="Seg_2976" s="T2544">adv:Time</ta>
            <ta e="T2546" id="Seg_2977" s="T2545">0.3.h:A</ta>
            <ta e="T2547" id="Seg_2978" s="T2546">pro.h:A</ta>
            <ta e="T2549" id="Seg_2979" s="T2548">0.2.h:A</ta>
            <ta e="T2550" id="Seg_2980" s="T2549">pro:P</ta>
            <ta e="T2551" id="Seg_2981" s="T2550">pro.h:A</ta>
            <ta e="T2555" id="Seg_2982" s="T2554">0.2.h:A</ta>
            <ta e="T2556" id="Seg_2983" s="T2555">pro.h:A</ta>
            <ta e="T2558" id="Seg_2984" s="T2557">pro:P</ta>
            <ta e="T2561" id="Seg_2985" s="T2560">adv:L</ta>
            <ta e="T2562" id="Seg_2986" s="T2561">np:Th 0.3.h:Poss</ta>
            <ta e="T2563" id="Seg_2987" s="T2562">0.3.h:A</ta>
            <ta e="T2565" id="Seg_2988" s="T2564">0.3.h:A</ta>
            <ta e="T2567" id="Seg_2989" s="T2566">0.2.h:A</ta>
            <ta e="T2568" id="Seg_2990" s="T2567">adv:L</ta>
            <ta e="T2569" id="Seg_2991" s="T2568">np:Th</ta>
            <ta e="T2570" id="Seg_2992" s="T2569">0.3.h:A</ta>
            <ta e="T2571" id="Seg_2993" s="T2570">np:Th</ta>
            <ta e="T2572" id="Seg_2994" s="T2571">0.3.h:A</ta>
            <ta e="T2574" id="Seg_2995" s="T2573">np:Th</ta>
            <ta e="T2575" id="Seg_2996" s="T2574">0.3.h:A</ta>
            <ta e="T2576" id="Seg_2997" s="T2575">adv:Time</ta>
            <ta e="T2577" id="Seg_2998" s="T2576">0.3.h:A</ta>
            <ta e="T2578" id="Seg_2999" s="T2577">pro.h:A</ta>
            <ta e="T2583" id="Seg_3000" s="T2582">np:P</ta>
            <ta e="T2585" id="Seg_3001" s="T2584">0.1.h:A</ta>
            <ta e="T2586" id="Seg_3002" s="T2585">adv:Time</ta>
            <ta e="T2589" id="Seg_3003" s="T2588">np:Th</ta>
            <ta e="T2591" id="Seg_3004" s="T2590">adv:L</ta>
            <ta e="T2594" id="Seg_3005" s="T2593">np:Th 0.3.h:Poss</ta>
            <ta e="T2595" id="Seg_3006" s="T2594">adv:L</ta>
            <ta e="T2598" id="Seg_3007" s="T2597">pro.h:A</ta>
            <ta e="T2599" id="Seg_3008" s="T2598">adv:Time</ta>
            <ta e="T2601" id="Seg_3009" s="T2600">pro.h:Poss</ta>
            <ta e="T2603" id="Seg_3010" s="T2602">np:Th</ta>
            <ta e="T2605" id="Seg_3011" s="T2604">pro.h:A</ta>
            <ta e="T2608" id="Seg_3012" s="T2607">adv:Time</ta>
            <ta e="T2609" id="Seg_3013" s="T2608">0.3.h:A</ta>
            <ta e="T2611" id="Seg_3014" s="T2610">np:Ins</ta>
            <ta e="T2612" id="Seg_3015" s="T2611">adv:Time</ta>
            <ta e="T2613" id="Seg_3016" s="T2612">adv:L</ta>
            <ta e="T2614" id="Seg_3017" s="T2613">np:Th</ta>
            <ta e="T2615" id="Seg_3018" s="T2614">np:Pth</ta>
            <ta e="T2616" id="Seg_3019" s="T2615">0.3.h:A</ta>
            <ta e="T2618" id="Seg_3020" s="T2616">pro.h:A</ta>
            <ta e="T2620" id="Seg_3021" s="T2618">np:Th</ta>
            <ta e="T2621" id="Seg_3022" s="T2620">0.3.h:A</ta>
            <ta e="T2622" id="Seg_3023" s="T2621">pro.h:Th</ta>
            <ta e="T2624" id="Seg_3024" s="T2623">pro.h:A</ta>
            <ta e="T2625" id="Seg_3025" s="T2624">pro.h:Th</ta>
            <ta e="T2629" id="Seg_3026" s="T2628">0.3.h:A</ta>
            <ta e="T2630" id="Seg_3027" s="T2629">pro.h:Th</ta>
            <ta e="T2633" id="Seg_3028" s="T2632">pro.h:A</ta>
            <ta e="T2635" id="Seg_3029" s="T2634">pro.h:B</ta>
            <ta e="T2638" id="Seg_3030" s="T2637">0.2.h:E</ta>
            <ta e="T2642" id="Seg_3031" s="T2641">pro.h:A</ta>
            <ta e="T2644" id="Seg_3032" s="T2643">adv:Time</ta>
            <ta e="T2646" id="Seg_3033" s="T2645">adv:L</ta>
            <ta e="T2647" id="Seg_3034" s="T2646">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2407" id="Seg_3035" s="T2406">v:pred</ta>
            <ta e="T2410" id="Seg_3036" s="T2409">np.h:S</ta>
            <ta e="T2412" id="Seg_3037" s="T2411">v:pred</ta>
            <ta e="T2413" id="Seg_3038" s="T2412">np.h:S</ta>
            <ta e="T2417" id="Seg_3039" s="T2416">s:purp</ta>
            <ta e="T2420" id="Seg_3040" s="T2419">pro.h:O</ta>
            <ta e="T2422" id="Seg_3041" s="T2421">v:pred 0.3.h:S</ta>
            <ta e="T2425" id="Seg_3042" s="T2424">pro.h:S</ta>
            <ta e="T2426" id="Seg_3043" s="T2425">v:pred</ta>
            <ta e="T2429" id="Seg_3044" s="T2428">v:pred 0.2.h:S</ta>
            <ta e="T2430" id="Seg_3045" s="T2429">v:pred 0.2.h:S</ta>
            <ta e="T2433" id="Seg_3046" s="T2432">np:O</ta>
            <ta e="T2434" id="Seg_3047" s="T2433">pro.h:S</ta>
            <ta e="T2435" id="Seg_3048" s="T2434">v:pred</ta>
            <ta e="T2436" id="Seg_3049" s="T2435">v:pred 0.3.h:S</ta>
            <ta e="T2437" id="Seg_3050" s="T2436">v:pred 0.3.h:S</ta>
            <ta e="T2438" id="Seg_3051" s="T2437">v:pred 0.3.h:S</ta>
            <ta e="T2440" id="Seg_3052" s="T2439">np:O</ta>
            <ta e="T2441" id="Seg_3053" s="T2440">v:pred 0.3.h:S</ta>
            <ta e="T2442" id="Seg_3054" s="T2441">np:O</ta>
            <ta e="T2443" id="Seg_3055" s="T2442">v:pred 0.3.h:S</ta>
            <ta e="T2445" id="Seg_3056" s="T2444">pro.h:S</ta>
            <ta e="T2446" id="Seg_3057" s="T2445">np:O</ta>
            <ta e="T2447" id="Seg_3058" s="T2446">v:pred</ta>
            <ta e="T2448" id="Seg_3059" s="T2447">pro.h:S</ta>
            <ta e="T2449" id="Seg_3060" s="T2448">np:O</ta>
            <ta e="T2451" id="Seg_3061" s="T2450">v:pred</ta>
            <ta e="T2452" id="Seg_3062" s="T2451">v:pred 0.2.h:S</ta>
            <ta e="T2454" id="Seg_3063" s="T2453">pro.h:S</ta>
            <ta e="T2455" id="Seg_3064" s="T2454">np:O</ta>
            <ta e="T2456" id="Seg_3065" s="T2455">v:pred</ta>
            <ta e="T2458" id="Seg_3066" s="T2457">pro.h:S</ta>
            <ta e="T2459" id="Seg_3067" s="T2458">v:pred</ta>
            <ta e="T2460" id="Seg_3068" s="T2459">v:pred 0.2.h:S</ta>
            <ta e="T2462" id="Seg_3069" s="T2461">pro.h:S</ta>
            <ta e="T2464" id="Seg_3070" s="T2463">v:pred</ta>
            <ta e="T2467" id="Seg_3071" s="T2466">ptcl:pred</ta>
            <ta e="T2471" id="Seg_3072" s="T2470">pro.h:S</ta>
            <ta e="T2472" id="Seg_3073" s="T2471">v:pred</ta>
            <ta e="T2473" id="Seg_3074" s="T2472">v:pred 0.2.h:S</ta>
            <ta e="T2475" id="Seg_3075" s="T2474">v:pred 0.2.h:S</ta>
            <ta e="T2477" id="Seg_3076" s="T2476">np:O</ta>
            <ta e="T2478" id="Seg_3077" s="T2477">v:pred 0.2.h:S</ta>
            <ta e="T2483" id="Seg_3078" s="T2482">pro.h:S</ta>
            <ta e="T2484" id="Seg_3079" s="T2483">v:pred</ta>
            <ta e="T2485" id="Seg_3080" s="T2484">v:pred 0.3.h:S</ta>
            <ta e="T2487" id="Seg_3081" s="T2486">np:O</ta>
            <ta e="T2488" id="Seg_3082" s="T2487">v:pred 0.3.h:S</ta>
            <ta e="T2490" id="Seg_3083" s="T2489">v:pred 0.3.h:S</ta>
            <ta e="T2500" id="Seg_3084" s="T2499">pro.h:S</ta>
            <ta e="T2501" id="Seg_3085" s="T2500">v:pred</ta>
            <ta e="T2503" id="Seg_3086" s="T2502">v:pred 0.3.h:S</ta>
            <ta e="T2504" id="Seg_3087" s="T2503">pro.h:S</ta>
            <ta e="T2505" id="Seg_3088" s="T2504">v:pred</ta>
            <ta e="T2509" id="Seg_3089" s="T2508">pro.h:S</ta>
            <ta e="T2510" id="Seg_3090" s="T2509">pro:O</ta>
            <ta e="T2514" id="Seg_3091" s="T2513">v:pred</ta>
            <ta e="T2515" id="Seg_3092" s="T2514">np:O</ta>
            <ta e="T2516" id="Seg_3093" s="T2515">adj:pred</ta>
            <ta e="T2517" id="Seg_3094" s="T2516">ptcl:pred</ta>
            <ta e="T2522" id="Seg_3095" s="T2521">np:O</ta>
            <ta e="T2524" id="Seg_3096" s="T2523">pro.h:S</ta>
            <ta e="T2525" id="Seg_3097" s="T2524">v:pred</ta>
            <ta e="T2528" id="Seg_3098" s="T2527">np:O</ta>
            <ta e="T2529" id="Seg_3099" s="T2528">pro.h:S</ta>
            <ta e="T2530" id="Seg_3100" s="T2529">v:pred</ta>
            <ta e="T2533" id="Seg_3101" s="T2532">pro.h:S</ta>
            <ta e="T2535" id="Seg_3102" s="T2534">np:O</ta>
            <ta e="T2536" id="Seg_3103" s="T2535">v:pred</ta>
            <ta e="T2539" id="Seg_3104" s="T2538">v:pred 0.3:S</ta>
            <ta e="T2546" id="Seg_3105" s="T2545">0.3.h:S</ta>
            <ta e="T2547" id="Seg_3106" s="T2546">pro.h:S</ta>
            <ta e="T2548" id="Seg_3107" s="T2547">v:pred</ta>
            <ta e="T2549" id="Seg_3108" s="T2548">v:pred 0.2.h:S</ta>
            <ta e="T2550" id="Seg_3109" s="T2549">pro:O</ta>
            <ta e="T2551" id="Seg_3110" s="T2550">pro.h:S</ta>
            <ta e="T2552" id="Seg_3111" s="T2551">ptcl.neg</ta>
            <ta e="T2553" id="Seg_3112" s="T2552">v:pred</ta>
            <ta e="T2555" id="Seg_3113" s="T2554">v:pred 0.2.h:S</ta>
            <ta e="T2556" id="Seg_3114" s="T2555">pro.h:S</ta>
            <ta e="T2557" id="Seg_3115" s="T2556">v:pred</ta>
            <ta e="T2558" id="Seg_3116" s="T2557">pro:O</ta>
            <ta e="T2559" id="Seg_3117" s="T2558">v:pred</ta>
            <ta e="T2562" id="Seg_3118" s="T2561">np:O</ta>
            <ta e="T2563" id="Seg_3119" s="T2562">v:pred 0.3.h:S</ta>
            <ta e="T2565" id="Seg_3120" s="T2564">v:pred 0.3.h:S</ta>
            <ta e="T2567" id="Seg_3121" s="T2566">v:pred 0.2.h:S</ta>
            <ta e="T2569" id="Seg_3122" s="T2568">np:O</ta>
            <ta e="T2570" id="Seg_3123" s="T2569">v:pred 0.3.h:S</ta>
            <ta e="T2571" id="Seg_3124" s="T2570">np:O</ta>
            <ta e="T2572" id="Seg_3125" s="T2571">v:pred 0.3.h:S</ta>
            <ta e="T2574" id="Seg_3126" s="T2573">np:O</ta>
            <ta e="T2575" id="Seg_3127" s="T2574">v:pred 0.3.h:S</ta>
            <ta e="T2577" id="Seg_3128" s="T2576">v:pred 0.3.h:S</ta>
            <ta e="T2578" id="Seg_3129" s="T2577">pro.h:S</ta>
            <ta e="T2580" id="Seg_3130" s="T2579">v:pred</ta>
            <ta e="T2583" id="Seg_3131" s="T2582">np:O</ta>
            <ta e="T2584" id="Seg_3132" s="T2583">ptcl.neg</ta>
            <ta e="T2585" id="Seg_3133" s="T2584">v:pred 0.1.h:S</ta>
            <ta e="T2589" id="Seg_3134" s="T2588">np:O</ta>
            <ta e="T2592" id="Seg_3135" s="T2591">n:pred</ta>
            <ta e="T2593" id="Seg_3136" s="T2592">cop</ta>
            <ta e="T2594" id="Seg_3137" s="T2593">np:S</ta>
            <ta e="T2596" id="Seg_3138" s="T2595">v:pred</ta>
            <ta e="T2598" id="Seg_3139" s="T2597">pro.h:S</ta>
            <ta e="T2600" id="Seg_3140" s="T2599">v:pred</ta>
            <ta e="T2601" id="Seg_3141" s="T2600">pro.h:S</ta>
            <ta e="T2603" id="Seg_3142" s="T2602">np:O</ta>
            <ta e="T2605" id="Seg_3143" s="T2604">pro.h:S</ta>
            <ta e="T2607" id="Seg_3144" s="T2606">v:pred</ta>
            <ta e="T2609" id="Seg_3145" s="T2608">v:pred 0.3.h:S</ta>
            <ta e="T2610" id="Seg_3146" s="T2609">conv:pred</ta>
            <ta e="T2614" id="Seg_3147" s="T2613">n:pred</ta>
            <ta e="T2616" id="Seg_3148" s="T2615">v:pred 0.3.h:S</ta>
            <ta e="T2618" id="Seg_3149" s="T2616">pro.h:S</ta>
            <ta e="T2620" id="Seg_3150" s="T2618">np:O</ta>
            <ta e="T2621" id="Seg_3151" s="T2620">v:pred 0.3.h:S</ta>
            <ta e="T2622" id="Seg_3152" s="T2621">pro.h:S</ta>
            <ta e="T2623" id="Seg_3153" s="T2622">v:pred</ta>
            <ta e="T2624" id="Seg_3154" s="T2623">pro.h:S</ta>
            <ta e="T2626" id="Seg_3155" s="T2625">v:pred</ta>
            <ta e="T2629" id="Seg_3156" s="T2628">v:pred 0.3.h:S</ta>
            <ta e="T2630" id="Seg_3157" s="T2629">pro.h:S</ta>
            <ta e="T2631" id="Seg_3158" s="T2630">v:pred</ta>
            <ta e="T2633" id="Seg_3159" s="T2632">pro.h:S</ta>
            <ta e="T2634" id="Seg_3160" s="T2633">v:pred</ta>
            <ta e="T2636" id="Seg_3161" s="T2635">ptcl.neg</ta>
            <ta e="T2638" id="Seg_3162" s="T2637">v:pred 0.2.h:S</ta>
            <ta e="T2640" id="Seg_3163" s="T2639">conv:pred</ta>
            <ta e="T2641" id="Seg_3164" s="T2640">v:pred</ta>
            <ta e="T2642" id="Seg_3165" s="T2641">pro.h:S</ta>
            <ta e="T2647" id="Seg_3166" s="T2646">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2416" id="Seg_3167" s="T2415">RUS:cult</ta>
            <ta e="T2428" id="Seg_3168" s="T2427">RUS:disc</ta>
            <ta e="T2439" id="Seg_3169" s="T2438">RUS:gram</ta>
            <ta e="T2453" id="Seg_3170" s="T2452">RUS:gram</ta>
            <ta e="T2465" id="Seg_3171" s="T2464">RUS:gram</ta>
            <ta e="T2467" id="Seg_3172" s="T2466">RUS:mod</ta>
            <ta e="T2489" id="Seg_3173" s="T2488">RUS:gram</ta>
            <ta e="T2491" id="Seg_3174" s="T2490">RUS:gram</ta>
            <ta e="T2493" id="Seg_3175" s="T2492">RUS:gram</ta>
            <ta e="T2495" id="Seg_3176" s="T2494">RUS:gram</ta>
            <ta e="T2497" id="Seg_3177" s="T2496">RUS:gram</ta>
            <ta e="T2502" id="Seg_3178" s="T2501">RUS:gram</ta>
            <ta e="T2508" id="Seg_3179" s="T2507">RUS:gram</ta>
            <ta e="T2512" id="Seg_3180" s="T2511">RUS:gram</ta>
            <ta e="T2517" id="Seg_3181" s="T2516">RUS:mod</ta>
            <ta e="T2518" id="Seg_3182" s="T2517">RUS:mod</ta>
            <ta e="T2520" id="Seg_3183" s="T2519">RUS:mod</ta>
            <ta e="T2534" id="Seg_3184" s="T2533">RUS:disc</ta>
            <ta e="T2540" id="Seg_3185" s="T2539">RUS:disc</ta>
            <ta e="T2542" id="Seg_3186" s="T2541">TURK:core</ta>
            <ta e="T2550" id="Seg_3187" s="T2549">TURK:gram(INDEF)</ta>
            <ta e="T2566" id="Seg_3188" s="T2565">RUS:gram</ta>
            <ta e="T0" id="Seg_3189" s="T2573">RUS:cult</ta>
            <ta e="T2574" id="Seg_3190" s="T0">RUS:cult</ta>
            <ta e="T2581" id="Seg_3191" s="T2580">RUS:gram</ta>
            <ta e="T2582" id="Seg_3192" s="T2581">TURK:gram(INDEF)</ta>
            <ta e="T2590" id="Seg_3193" s="T2589">RUS:gram</ta>
            <ta e="T2597" id="Seg_3194" s="T2596">RUS:gram</ta>
            <ta e="T2604" id="Seg_3195" s="T2603">RUS:gram</ta>
            <ta e="T2628" id="Seg_3196" s="T2627">RUS:gram</ta>
            <ta e="T2632" id="Seg_3197" s="T2631">RUS:gram</ta>
            <ta e="T2636" id="Seg_3198" s="T2635">RUS:gram</ta>
            <ta e="T2645" id="Seg_3199" s="T2644">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T2467" id="Seg_3200" s="T2466">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2410" id="Seg_3201" s="T2406">Жил один человек [барин].</ta>
            <ta e="T2417" id="Seg_3202" s="T2410">Пришёл к нему человек наниматься.</ta>
            <ta e="T2422" id="Seg_3203" s="T2417">«Как твоё имя?»</ta>
            <ta e="T2424" id="Seg_3204" s="T2422">«Сысой-Косой».</ta>
            <ta e="T2429" id="Seg_3205" s="T2424">Барин говорит: «Ну, оставайся.</ta>
            <ta e="T2433" id="Seg_3206" s="T2429">Свари мне два яйца».</ta>
            <ta e="T2435" id="Seg_3207" s="T2433">Он сварил.</ta>
            <ta e="T2437" id="Seg_3208" s="T2435">Варил, варил.</ta>
            <ta e="T2443" id="Seg_3209" s="T2437">Взял и одно съел, одно [барину] принёс.</ta>
            <ta e="T2452" id="Seg_3210" s="T2443">«Ты почему одно принёс, я сказал свари два!»</ta>
            <ta e="T2456" id="Seg_3211" s="T2452">«А я одно съел».</ta>
            <ta e="T2459" id="Seg_3212" s="T2456">Тогда тот стал ругаться.</ta>
            <ta e="T2461" id="Seg_3213" s="T2459">«Иди отсюда!»</ta>
            <ta e="T2464" id="Seg_3214" s="T2461">Он ушёл.</ta>
            <ta e="T2468" id="Seg_3215" s="T2464">И хотел уйти оттуда.</ta>
            <ta e="T2474" id="Seg_3216" s="T2468">Потом [барин] кричит: «Иди сюда!</ta>
            <ta e="T2478" id="Seg_3217" s="T2474">Пойди купи мне шапку.</ta>
            <ta e="T2482" id="Seg_3218" s="T2478">От уха до уха».</ta>
            <ta e="T2484" id="Seg_3219" s="T2482">Он пошёл.</ta>
            <ta e="T2487" id="Seg_3220" s="T2484">Купил шляпу.</ta>
            <ta e="T2490" id="Seg_3221" s="T2487">Идёт и трясёт [головой].</ta>
            <ta e="T2494" id="Seg_3222" s="T2490">То туда, то сюда.</ta>
            <ta e="T2498" id="Seg_3223" s="T2494">То на спину, то на лицо.</ta>
            <ta e="T2503" id="Seg_3224" s="T2498">Потом [барин] увидел и стал ругаться.</ta>
            <ta e="T2514" id="Seg_3225" s="T2503">«Я сказал от уха до уха, а ты купил со лба на затылок».</ta>
            <ta e="T2522" id="Seg_3226" s="T2514">«Денег мало, надо ещё пять [рублей]».</ta>
            <ta e="T2528" id="Seg_3227" s="T2522">Тогда он ему дал десять.</ta>
            <ta e="T2530" id="Seg_3228" s="T2528">Он пошёл.</ta>
            <ta e="T2536" id="Seg_3229" s="T2530">Принёс ту же самую шапку.</ta>
            <ta e="T2539" id="Seg_3230" s="T2536">[Перекидывает её] от уха до уха.</ta>
            <ta e="T2544" id="Seg_3231" s="T2539">«Ну, очень хорошая шапка».</ta>
            <ta e="T2553" id="Seg_3232" s="T2544">Тогда [барин] надел её и говорит: «Приготовь что-нибудь, что я никогда не ел.</ta>
            <ta e="T2555" id="Seg_3233" s="T2553">Так приготовь».</ta>
            <ta e="T2559" id="Seg_3234" s="T2555">Он думает: «Что мне приготовить?»</ta>
            <ta e="T2563" id="Seg_3235" s="T2559">Взял его [барина] сапоги.</ta>
            <ta e="T2565" id="Seg_3236" s="T2563">Вырезал [подошвы].</ta>
            <ta e="T2570" id="Seg_3237" s="T2565">Сварил, добавил яйцо.</ta>
            <ta e="T2572" id="Seg_3238" s="T2570">Масло положил.</ta>
            <ta e="T2575" id="Seg_3239" s="T2572">Лавровый лист положил.</ta>
            <ta e="T2585" id="Seg_3240" s="T2575">Потом сварил, тот ест и [говорит]: «Никогда такого не ел».</ta>
            <ta e="T2593" id="Seg_3241" s="T2585">Потом стал сапоги надевать, а там дырки.</ta>
            <ta e="T2596" id="Seg_3242" s="T2593">Ноги насквозь прошли.</ta>
            <ta e="T2603" id="Seg_3243" s="T2596">«Ты посмотри, что у меня с сапогами».</ta>
            <ta e="T2607" id="Seg_3244" s="T2603">«А ты сейчас съел».</ta>
            <ta e="T2611" id="Seg_3245" s="T2607">Потом поехали в гости на лошади.</ta>
            <ta e="T2618" id="Seg_3246" s="T2611">Там была река, через реку переезжали, он…</ta>
            <ta e="T2621" id="Seg_3247" s="T2618">…вытащил стержень.</ta>
            <ta e="T2623" id="Seg_3248" s="T2621">Он застрял.</ta>
            <ta e="T2627" id="Seg_3249" s="T2623">«Не гляди на меня!»</ta>
            <ta e="T2631" id="Seg_3250" s="T2627">И кричит: «Я застрял!»</ta>
            <ta e="T2638" id="Seg_3251" s="T2631">«А ты велел на тебя не смотреть, теперь сиди».</ta>
            <ta e="T2647" id="Seg_3252" s="T2638">Сам ушёл, а тот [барин] и сейчас там сидит.</ta>
            <ta e="T2648" id="Seg_3253" s="T2647">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2410" id="Seg_3254" s="T2406">There lived a man [a landlord].</ta>
            <ta e="T2417" id="Seg_3255" s="T2410">A man came to him to offer his service.</ta>
            <ta e="T2422" id="Seg_3256" s="T2417">"What is your name?"</ta>
            <ta e="T2424" id="Seg_3257" s="T2422">"Sisoy-Kosoy [=Sisoy the Cross-eyed]."</ta>
            <ta e="T2429" id="Seg_3258" s="T2424">The landlord says: "Well, live [here].</ta>
            <ta e="T2433" id="Seg_3259" s="T2429">Cook me two eggs."</ta>
            <ta e="T2435" id="Seg_3260" s="T2433">He cooked.</ta>
            <ta e="T2437" id="Seg_3261" s="T2435">He cooked, he cooked.</ta>
            <ta e="T2443" id="Seg_3262" s="T2437">[He] took and ate one, and brings another [to the landlord].</ta>
            <ta e="T2452" id="Seg_3263" s="T2443">"Why did you bring one, I said you: cook two!"</ta>
            <ta e="T2456" id="Seg_3264" s="T2452">"Because I ate one [of them]".</ta>
            <ta e="T2459" id="Seg_3265" s="T2456">Then he scolded.</ta>
            <ta e="T2461" id="Seg_3266" s="T2459">"Go away!"</ta>
            <ta e="T2464" id="Seg_3267" s="T2461">He went away.</ta>
            <ta e="T2468" id="Seg_3268" s="T2464">And he wanted to go away from there.</ta>
            <ta e="T2474" id="Seg_3269" s="T2468">Then [the landlord] shouts: "Come here!</ta>
            <ta e="T2478" id="Seg_3270" s="T2474">Go buy me a hat.</ta>
            <ta e="T2482" id="Seg_3271" s="T2478">From ear to ear."</ta>
            <ta e="T2484" id="Seg_3272" s="T2482">He went.</ta>
            <ta e="T2487" id="Seg_3273" s="T2484">He bought the hat.</ta>
            <ta e="T2490" id="Seg_3274" s="T2487">He goes and shakes [his head].</ta>
            <ta e="T2494" id="Seg_3275" s="T2490">Here and there.</ta>
            <ta e="T2498" id="Seg_3276" s="T2494">That on his back, that on his face.</ta>
            <ta e="T2503" id="Seg_3277" s="T2498">Then [the landlord] sees [it] and scolds.</ta>
            <ta e="T2514" id="Seg_3278" s="T2503">"I said [from] ear to ear, and you bought it from the face to the back."</ta>
            <ta e="T2522" id="Seg_3279" s="T2514">"There is too little money, [you] should add so many… five [roubles]."</ta>
            <ta e="T2528" id="Seg_3280" s="T2522">Then he gave him ten.</ta>
            <ta e="T2530" id="Seg_3281" s="T2528">He went.</ta>
            <ta e="T2536" id="Seg_3282" s="T2530">He brought the same hat.</ta>
            <ta e="T2539" id="Seg_3283" s="T2536">[It swings from] ear to ear.</ta>
            <ta e="T2544" id="Seg_3284" s="T2539">"Well, very good hat."</ta>
            <ta e="T2553" id="Seg_3285" s="T2544">Then [the landlord] put it on and says: "Cook something I never ate.</ta>
            <ta e="T2555" id="Seg_3286" s="T2553">"Cook so."</ta>
            <ta e="T2559" id="Seg_3287" s="T2555">He thinks: "What should I cook?"</ta>
            <ta e="T2563" id="Seg_3288" s="T2559">He took his [landlord’s] boots.</ta>
            <ta e="T2565" id="Seg_3289" s="T2563">He cut [out the soles].</ta>
            <ta e="T2570" id="Seg_3290" s="T2565">And cooked [them], he put eggs there.</ta>
            <ta e="T2572" id="Seg_3291" s="T2570">He put butter.</ta>
            <ta e="T2575" id="Seg_3292" s="T2572">He put a laurel leaf.</ta>
            <ta e="T2585" id="Seg_3293" s="T2575">Then he cooked, the other eats and [says:] "I've never eaten anything like this."</ta>
            <ta e="T2593" id="Seg_3294" s="T2585">Then he started to put on his boots, and there are holes.</ta>
            <ta e="T2596" id="Seg_3295" s="T2593">His foot went out [through the holes].</ta>
            <ta e="T2603" id="Seg_3296" s="T2596">"But you look now, what kind of boots I have got."</ta>
            <ta e="T2607" id="Seg_3297" s="T2603">"But you ate it just now."</ta>
            <ta e="T2611" id="Seg_3298" s="T2607">Then they went to a visit on a horse.</ta>
            <ta e="T2618" id="Seg_3299" s="T2611">Then there was a river, they go across the river, he…</ta>
            <ta e="T2621" id="Seg_3300" s="T2618">…took [out] the iron.</ta>
            <ta e="T2623" id="Seg_3301" s="T2621">He got stuck.</ta>
            <ta e="T2627" id="Seg_3302" s="T2623">"Don't look at me!"</ta>
            <ta e="T2631" id="Seg_3303" s="T2627">And he is shouting: "I got stuck!"</ta>
            <ta e="T2638" id="Seg_3304" s="T2631">"You didn't allow me to look [at you], [so] sit [here]."</ta>
            <ta e="T2647" id="Seg_3305" s="T2638">He himself went away, he is still sitting there now.</ta>
            <ta e="T2648" id="Seg_3306" s="T2647">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2410" id="Seg_3307" s="T2406">Es lebte einmal ein Mann [ein Gutsherr].</ta>
            <ta e="T2417" id="Seg_3308" s="T2410">Ein Mann kam zu ihm, um ihm seinen Dienst zu bieten.</ta>
            <ta e="T2422" id="Seg_3309" s="T2417">"Wie ist dein Name?"</ta>
            <ta e="T2424" id="Seg_3310" s="T2422">"Sysoj-Kosoj [=Sysoj der schielende]."</ta>
            <ta e="T2429" id="Seg_3311" s="T2424">Der Gutsherr sagte: „Also, lebe [hier].</ta>
            <ta e="T2433" id="Seg_3312" s="T2429">Koche mir zwei Eier.“</ta>
            <ta e="T2435" id="Seg_3313" s="T2433">Er kochte.</ta>
            <ta e="T2437" id="Seg_3314" s="T2435">Er kochte, er kochte.</ta>
            <ta e="T2443" id="Seg_3315" s="T2437">[Er] nahm sich und aß eins, und bringt [dem Gutsherrn] ein anderes.</ta>
            <ta e="T2452" id="Seg_3316" s="T2443">„Warum hast du mir eins gebracht, ich sagte dir: koche zwei!“</ta>
            <ta e="T2456" id="Seg_3317" s="T2452">„Weil ich eins [davon] aß.“</ta>
            <ta e="T2459" id="Seg_3318" s="T2456">Dann schimpfte er.</ta>
            <ta e="T2461" id="Seg_3319" s="T2459">„Geh weg!“</ta>
            <ta e="T2464" id="Seg_3320" s="T2461">Er ging weg.</ta>
            <ta e="T2468" id="Seg_3321" s="T2464">Und er wollte von dort weggehen.</ta>
            <ta e="T2474" id="Seg_3322" s="T2468">Dann rief [der Gutsherr]: „Komm her!</ta>
            <ta e="T2478" id="Seg_3323" s="T2474">Gehe mir einen Hut kaufen.</ta>
            <ta e="T2482" id="Seg_3324" s="T2478">Von Ohr zu Ohr.“</ta>
            <ta e="T2484" id="Seg_3325" s="T2482">Er ging.</ta>
            <ta e="T2487" id="Seg_3326" s="T2484">Er kaufte den Hut.</ta>
            <ta e="T2490" id="Seg_3327" s="T2487">Er geht und schüttelt [den Kopf].</ta>
            <ta e="T2494" id="Seg_3328" s="T2490">Hier und dort.</ta>
            <ta e="T2498" id="Seg_3329" s="T2494">Das am Rücken, das im Gesicht.</ta>
            <ta e="T2503" id="Seg_3330" s="T2498">Dann sieht [es] [der Gutsherr] und schimpft.</ta>
            <ta e="T2514" id="Seg_3331" s="T2503">"Ich sagte [von] Ohr zu Ohr, und du hast es vom Gesicht am Rücken gekauft."</ta>
            <ta e="T2522" id="Seg_3332" s="T2514">"Es war zu wenig Geld, [Sie] sollten so viele … fünf [Rubel] dazulegen.“ </ta>
            <ta e="T2528" id="Seg_3333" s="T2522">Dann gab er ihm zehn.</ta>
            <ta e="T2530" id="Seg_3334" s="T2528">Er ging.</ta>
            <ta e="T2536" id="Seg_3335" s="T2530">Er kaufte den gleichen Hut.</ta>
            <ta e="T2539" id="Seg_3336" s="T2536">[Er schwingt von] Ohr zu Ohr.</ta>
            <ta e="T2544" id="Seg_3337" s="T2539">„Na also, sehr guter Hut.“</ta>
            <ta e="T2553" id="Seg_3338" s="T2544">Dann setzt [der Gutsherr] ihn auf und sagt: „Koche mir etwas, dass ich noch nie gegessen habe.“</ta>
            <ta e="T2555" id="Seg_3339" s="T2553">„Koche so.“</ta>
            <ta e="T2559" id="Seg_3340" s="T2555">Er überlegt: „Was soll ich kochen?“</ta>
            <ta e="T2563" id="Seg_3341" s="T2559">Er nahm seine [Gutsherren] Stiefel.</ta>
            <ta e="T2565" id="Seg_3342" s="T2563">Er schnitt [die Sohlen heraus].</ta>
            <ta e="T2570" id="Seg_3343" s="T2565">Und kochte [sie], er setzte Eier dort.</ta>
            <ta e="T2572" id="Seg_3344" s="T2570">Er setzte Butter.</ta>
            <ta e="T2575" id="Seg_3345" s="T2572">Er setzte ein Lorbeerblatt.</ta>
            <ta e="T2585" id="Seg_3346" s="T2575">Dann kochte er, der andere isst und [sagt]: „Sowas habe ich noch nie gegessen.“</ta>
            <ta e="T2593" id="Seg_3347" s="T2585">Dann fing er an, seine Stiefel anzuziehen, und es sind Löcher.</ta>
            <ta e="T2596" id="Seg_3348" s="T2593">Sein Fuß stieß hinaus [durch die Löcher].</ta>
            <ta e="T2603" id="Seg_3349" s="T2596">„Aber schauen Sie nun, was für Stiefel ich habe.“</ta>
            <ta e="T2607" id="Seg_3350" s="T2603">„Aber Sie haben es eben gegessen.“</ta>
            <ta e="T2611" id="Seg_3351" s="T2607">Dann gingen sie auf einem Pferd zu Besuch.</ta>
            <ta e="T2618" id="Seg_3352" s="T2611">Dann gab es einen Fluss, sie überqueren den Fluss, er…</ta>
            <ta e="T2621" id="Seg_3353" s="T2618">…nahm das Eisen [heraus].</ta>
            <ta e="T2623" id="Seg_3354" s="T2621">Er blieb stecken.</ta>
            <ta e="T2627" id="Seg_3355" s="T2623">„Schau mich nicht an!“</ta>
            <ta e="T2631" id="Seg_3356" s="T2627">Und er ruft: „ich bin stecken geblieben!“</ta>
            <ta e="T2638" id="Seg_3357" s="T2631">„Sie haben es mir nicht erlaubt, [Sie] anzusehen, [also] sitze [hier].</ta>
            <ta e="T2647" id="Seg_3358" s="T2638">Er selber ging weg, und er sitzt bis heute immer noch da.</ta>
            <ta e="T2648" id="Seg_3359" s="T2647">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2410" id="Seg_3360" s="T2406">[GVY:] A tale about a ladlord and a farm labourer ("Барин и работник"), see https://www.booksite.ru/fulltext/ust/noy/ena/rod/noe/3.htm.</ta>
            <ta e="T2417" id="Seg_3361" s="T2410">[KlT:] [intended munəjʔi] ?? Найми izittə; Ru. нанять ’hire’.</ta>
            <ta e="T2424" id="Seg_3362" s="T2422">[KlT:] RUS Сысой Косой.</ta>
            <ta e="T2459" id="Seg_3363" s="T2456">[GVY:] kudonzlubi should be kudonzʔlubi?</ta>
            <ta e="T2482" id="Seg_3364" s="T2478">[KlT:] Probably she intends ’from ear to ear’ kugəʔ kunə. [GVY:] "From ear to ear" in the Russian original (whatever it means).</ta>
            <ta e="T2498" id="Seg_3365" s="T2494">The hat is put on the other way around. [AAV:] Rooster shouts.</ta>
            <ta e="T2514" id="Seg_3366" s="T2503">[KlT:] Bögəl/begəl ’back’?</ta>
            <ta e="T2522" id="Seg_3367" s="T2514">[GVY:] dĭldʼiʔ?</ta>
            <ta e="T2621" id="Seg_3368" s="T2618">[KlT:] he took out a bolt form the wheel of the carriage.</ta>
            <ta e="T2627" id="Seg_3369" s="T2623">[GVY:] The landlord said this to S-K earlier, because he was very angry with him for the incident with the boots.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2406" />
            <conversion-tli id="T2407" />
            <conversion-tli id="T2408" />
            <conversion-tli id="T2409" />
            <conversion-tli id="T2410" />
            <conversion-tli id="T2411" />
            <conversion-tli id="T2412" />
            <conversion-tli id="T2413" />
            <conversion-tli id="T2414" />
            <conversion-tli id="T2415" />
            <conversion-tli id="T2416" />
            <conversion-tli id="T2417" />
            <conversion-tli id="T2418" />
            <conversion-tli id="T2419" />
            <conversion-tli id="T2420" />
            <conversion-tli id="T2421" />
            <conversion-tli id="T2422" />
            <conversion-tli id="T2423" />
            <conversion-tli id="T2424" />
            <conversion-tli id="T2425" />
            <conversion-tli id="T2426" />
            <conversion-tli id="T2427" />
            <conversion-tli id="T2428" />
            <conversion-tli id="T2429" />
            <conversion-tli id="T2430" />
            <conversion-tli id="T2431" />
            <conversion-tli id="T2432" />
            <conversion-tli id="T2433" />
            <conversion-tli id="T2434" />
            <conversion-tli id="T2435" />
            <conversion-tli id="T2436" />
            <conversion-tli id="T2437" />
            <conversion-tli id="T2438" />
            <conversion-tli id="T2439" />
            <conversion-tli id="T2440" />
            <conversion-tli id="T2441" />
            <conversion-tli id="T2442" />
            <conversion-tli id="T2443" />
            <conversion-tli id="T2444" />
            <conversion-tli id="T2445" />
            <conversion-tli id="T2446" />
            <conversion-tli id="T2447" />
            <conversion-tli id="T2448" />
            <conversion-tli id="T2449" />
            <conversion-tli id="T2450" />
            <conversion-tli id="T2451" />
            <conversion-tli id="T2452" />
            <conversion-tli id="T2453" />
            <conversion-tli id="T2454" />
            <conversion-tli id="T2455" />
            <conversion-tli id="T2456" />
            <conversion-tli id="T2457" />
            <conversion-tli id="T2458" />
            <conversion-tli id="T2459" />
            <conversion-tli id="T2460" />
            <conversion-tli id="T2461" />
            <conversion-tli id="T2462" />
            <conversion-tli id="T2463" />
            <conversion-tli id="T2464" />
            <conversion-tli id="T2465" />
            <conversion-tli id="T2466" />
            <conversion-tli id="T2467" />
            <conversion-tli id="T2468" />
            <conversion-tli id="T2469" />
            <conversion-tli id="T2470" />
            <conversion-tli id="T2471" />
            <conversion-tli id="T2472" />
            <conversion-tli id="T2473" />
            <conversion-tli id="T2474" />
            <conversion-tli id="T2475" />
            <conversion-tli id="T2476" />
            <conversion-tli id="T2477" />
            <conversion-tli id="T2478" />
            <conversion-tli id="T2479" />
            <conversion-tli id="T2480" />
            <conversion-tli id="T2481" />
            <conversion-tli id="T2482" />
            <conversion-tli id="T2483" />
            <conversion-tli id="T2484" />
            <conversion-tli id="T2485" />
            <conversion-tli id="T2486" />
            <conversion-tli id="T2487" />
            <conversion-tli id="T2488" />
            <conversion-tli id="T2489" />
            <conversion-tli id="T2490" />
            <conversion-tli id="T2491" />
            <conversion-tli id="T2492" />
            <conversion-tli id="T2493" />
            <conversion-tli id="T2494" />
            <conversion-tli id="T2495" />
            <conversion-tli id="T2496" />
            <conversion-tli id="T2497" />
            <conversion-tli id="T2498" />
            <conversion-tli id="T2499" />
            <conversion-tli id="T2500" />
            <conversion-tli id="T2501" />
            <conversion-tli id="T2502" />
            <conversion-tli id="T2503" />
            <conversion-tli id="T2504" />
            <conversion-tli id="T2505" />
            <conversion-tli id="T2506" />
            <conversion-tli id="T2507" />
            <conversion-tli id="T2508" />
            <conversion-tli id="T2509" />
            <conversion-tli id="T2510" />
            <conversion-tli id="T2511" />
            <conversion-tli id="T2512" />
            <conversion-tli id="T2513" />
            <conversion-tli id="T2514" />
            <conversion-tli id="T2515" />
            <conversion-tli id="T2516" />
            <conversion-tli id="T2517" />
            <conversion-tli id="T2518" />
            <conversion-tli id="T2519" />
            <conversion-tli id="T2520" />
            <conversion-tli id="T2521" />
            <conversion-tli id="T2522" />
            <conversion-tli id="T2523" />
            <conversion-tli id="T2524" />
            <conversion-tli id="T2525" />
            <conversion-tli id="T2526" />
            <conversion-tli id="T2527" />
            <conversion-tli id="T2528" />
            <conversion-tli id="T2529" />
            <conversion-tli id="T2530" />
            <conversion-tli id="T2531" />
            <conversion-tli id="T2532" />
            <conversion-tli id="T2533" />
            <conversion-tli id="T2534" />
            <conversion-tli id="T2535" />
            <conversion-tli id="T2536" />
            <conversion-tli id="T2537" />
            <conversion-tli id="T2538" />
            <conversion-tli id="T2539" />
            <conversion-tli id="T2540" />
            <conversion-tli id="T2541" />
            <conversion-tli id="T2542" />
            <conversion-tli id="T2543" />
            <conversion-tli id="T2544" />
            <conversion-tli id="T2545" />
            <conversion-tli id="T2546" />
            <conversion-tli id="T2547" />
            <conversion-tli id="T2548" />
            <conversion-tli id="T2549" />
            <conversion-tli id="T2550" />
            <conversion-tli id="T2551" />
            <conversion-tli id="T2552" />
            <conversion-tli id="T2553" />
            <conversion-tli id="T2554" />
            <conversion-tli id="T2555" />
            <conversion-tli id="T2556" />
            <conversion-tli id="T2557" />
            <conversion-tli id="T2558" />
            <conversion-tli id="T2559" />
            <conversion-tli id="T2560" />
            <conversion-tli id="T2561" />
            <conversion-tli id="T2562" />
            <conversion-tli id="T2563" />
            <conversion-tli id="T2564" />
            <conversion-tli id="T2565" />
            <conversion-tli id="T2566" />
            <conversion-tli id="T2567" />
            <conversion-tli id="T2568" />
            <conversion-tli id="T2569" />
            <conversion-tli id="T2570" />
            <conversion-tli id="T2571" />
            <conversion-tli id="T2572" />
            <conversion-tli id="T2573" />
            <conversion-tli id="T0" />
            <conversion-tli id="T2574" />
            <conversion-tli id="T2575" />
            <conversion-tli id="T2576" />
            <conversion-tli id="T2577" />
            <conversion-tli id="T2578" />
            <conversion-tli id="T2579" />
            <conversion-tli id="T2580" />
            <conversion-tli id="T2581" />
            <conversion-tli id="T2582" />
            <conversion-tli id="T2583" />
            <conversion-tli id="T2584" />
            <conversion-tli id="T2585" />
            <conversion-tli id="T2586" />
            <conversion-tli id="T2587" />
            <conversion-tli id="T2588" />
            <conversion-tli id="T2589" />
            <conversion-tli id="T2590" />
            <conversion-tli id="T2591" />
            <conversion-tli id="T2592" />
            <conversion-tli id="T2593" />
            <conversion-tli id="T2594" />
            <conversion-tli id="T2595" />
            <conversion-tli id="T2596" />
            <conversion-tli id="T2597" />
            <conversion-tli id="T2598" />
            <conversion-tli id="T2599" />
            <conversion-tli id="T2600" />
            <conversion-tli id="T2601" />
            <conversion-tli id="T2602" />
            <conversion-tli id="T2603" />
            <conversion-tli id="T2604" />
            <conversion-tli id="T2605" />
            <conversion-tli id="T2606" />
            <conversion-tli id="T2607" />
            <conversion-tli id="T2608" />
            <conversion-tli id="T2609" />
            <conversion-tli id="T2610" />
            <conversion-tli id="T2611" />
            <conversion-tli id="T2612" />
            <conversion-tli id="T2613" />
            <conversion-tli id="T2614" />
            <conversion-tli id="T2615" />
            <conversion-tli id="T2616" />
            <conversion-tli id="T2617" />
            <conversion-tli id="T2618" />
            <conversion-tli id="T2619" />
            <conversion-tli id="T2620" />
            <conversion-tli id="T2621" />
            <conversion-tli id="T2622" />
            <conversion-tli id="T2623" />
            <conversion-tli id="T2624" />
            <conversion-tli id="T2625" />
            <conversion-tli id="T2626" />
            <conversion-tli id="T2627" />
            <conversion-tli id="T2628" />
            <conversion-tli id="T2629" />
            <conversion-tli id="T2630" />
            <conversion-tli id="T2631" />
            <conversion-tli id="T2632" />
            <conversion-tli id="T2633" />
            <conversion-tli id="T2634" />
            <conversion-tli id="T2635" />
            <conversion-tli id="T2636" />
            <conversion-tli id="T2637" />
            <conversion-tli id="T2638" />
            <conversion-tli id="T2639" />
            <conversion-tli id="T2640" />
            <conversion-tli id="T2641" />
            <conversion-tli id="T2642" />
            <conversion-tli id="T2643" />
            <conversion-tli id="T2644" />
            <conversion-tli id="T2645" />
            <conversion-tli id="T2646" />
            <conversion-tli id="T2647" />
            <conversion-tli id="T2648" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
