<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID94A1FA7D-9114-FA81-153F-D9B03285AA65">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ManSowsGold_flk.wav" />
         <referenced-file url="PKZ_196X_ManSowsGold_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ManSowsGold_flk\PKZ_196X_ManSowsGold_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">114</ud-information>
            <ud-information attribute-name="# HIAT:w">80</ud-information>
            <ud-information attribute-name="# e">80</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1601" time="0.002" type="appl" />
         <tli id="T1602" time="0.78" type="appl" />
         <tli id="T1603" time="1.558" type="appl" />
         <tli id="T1604" time="2.336" type="appl" />
         <tli id="T1605" time="2.855" type="appl" />
         <tli id="T1606" time="3.374" type="appl" />
         <tli id="T1607" time="3.892" type="appl" />
         <tli id="T1608" time="4.6333126449334765" />
         <tli id="T1609" time="5.892" type="appl" />
         <tli id="T1610" time="7.031" type="appl" />
         <tli id="T1611" time="8.32662948708189" />
         <tli id="T1612" time="9.458" type="appl" />
         <tli id="T1613" time="10.379" type="appl" />
         <tli id="T1614" time="11.719947668766983" />
         <tli id="T1615" time="13.873271387203692" />
         <tli id="T1616" time="14.613" type="appl" />
         <tli id="T1617" time="15.215" type="appl" />
         <tli id="T1618" time="15.816" type="appl" />
         <tli id="T1619" time="17.664" type="appl" />
         <tli id="T1620" time="19.513" type="appl" />
         <tli id="T1621" time="21.361" type="appl" />
         <tli id="T1622" time="23.209" type="appl" />
         <tli id="T1623" time="25.058" type="appl" />
         <tli id="T1624" time="27.08654572138808" />
         <tli id="T1625" time="28.0" type="appl" />
         <tli id="T1626" time="28.952" type="appl" />
         <tli id="T1627" time="29.905" type="appl" />
         <tli id="T1628" time="30.705" type="appl" />
         <tli id="T1629" time="31.498" type="appl" />
         <tli id="T1630" time="32.292" type="appl" />
         <tli id="T1631" time="33.085" type="appl" />
         <tli id="T1632" time="34.299" type="appl" />
         <tli id="T1633" time="35.512" type="appl" />
         <tli id="T1634" time="36.726" type="appl" />
         <tli id="T1635" time="37.939" type="appl" />
         <tli id="T1636" time="38.637" type="appl" />
         <tli id="T1637" time="39.335" type="appl" />
         <tli id="T1638" time="40.033" type="appl" />
         <tli id="T1639" time="40.73" type="appl" />
         <tli id="T1640" time="41.428" type="appl" />
         <tli id="T1641" time="42.126" type="appl" />
         <tli id="T1642" time="42.824" type="appl" />
         <tli id="T1643" time="43.62" type="appl" />
         <tli id="T1644" time="44.417" type="appl" />
         <tli id="T1645" time="45.60646302732362" />
         <tli id="T1646" time="46.207" type="appl" />
         <tli id="T1647" time="46.934" type="appl" />
         <tli id="T1648" time="47.662" type="appl" />
         <tli id="T1649" time="48.392" type="appl" />
         <tli id="T1650" time="49.0531143042022" />
         <tli id="T1651" time="50.228" type="appl" />
         <tli id="T1652" time="51.366" type="appl" />
         <tli id="T1653" time="52.504" type="appl" />
         <tli id="T1654" time="53.642" type="appl" />
         <tli id="T1655" time="54.183" type="appl" />
         <tli id="T1656" time="54.723" type="appl" />
         <tli id="T1657" time="55.264" type="appl" />
         <tli id="T1658" time="55.664" type="appl" />
         <tli id="T1659" time="56.064" type="appl" />
         <tli id="T1660" time="56.464" type="appl" />
         <tli id="T1661" time="57.019745398728105" />
         <tli id="T1662" time="57.811" type="appl" />
         <tli id="T1663" time="58.309" type="appl" />
         <tli id="T1664" time="58.808" type="appl" />
         <tli id="T1665" time="59.306" type="appl" />
         <tli id="T1666" time="59.805" type="appl" />
         <tli id="T1667" time="60.303" type="appl" />
         <tli id="T1668" time="61.36639265699663" />
         <tli id="T1669" time="62.17" type="appl" />
         <tli id="T1670" time="62.999" type="appl" />
         <tli id="T1671" time="63.795" type="appl" />
         <tli id="T1672" time="64.591" type="appl" />
         <tli id="T1673" time="65.387" type="appl" />
         <tli id="T1674" time="66.183" type="appl" />
         <tli id="T1675" time="66.979" type="appl" />
         <tli id="T1676" time="67.87969690750025" />
         <tli id="T1677" time="68.48" type="appl" />
         <tli id="T1678" time="69.052" type="appl" />
         <tli id="T1679" time="69.624" type="appl" />
         <tli id="T1680" time="70.196" type="appl" />
         <tli id="T1681" time="71.153" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1681" id="Seg_0" n="sc" s="T1601">
               <ts e="T1604" id="Seg_2" n="HIAT:u" s="T1601">
                  <ts e="T1602" id="Seg_4" n="HIAT:w" s="T1601">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1603" id="Seg_7" n="HIAT:w" s="T1602">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1604" id="Seg_10" n="HIAT:w" s="T1603">dʼaʔpiʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1608" id="Seg_14" n="HIAT:u" s="T1604">
                  <ts e="T1605" id="Seg_16" n="HIAT:w" s="T1604">Dĭ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1606" id="Seg_19" n="HIAT:w" s="T1605">ĭmbidə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1607" id="Seg_22" n="HIAT:w" s="T1606">ej</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1608" id="Seg_25" n="HIAT:w" s="T1607">tojirbi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1611" id="Seg_29" n="HIAT:u" s="T1608">
                  <ts e="T1609" id="Seg_31" n="HIAT:w" s="T1608">Deʔpiʔi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1610" id="Seg_34" n="HIAT:w" s="T1609">dĭm</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1611" id="Seg_37" n="HIAT:w" s="T1610">koŋdə</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1614" id="Seg_41" n="HIAT:u" s="T1611">
                  <ts e="T1612" id="Seg_43" n="HIAT:w" s="T1611">Dĭm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1613" id="Seg_46" n="HIAT:w" s="T1612">kuʔsʼittə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1614" id="Seg_49" n="HIAT:w" s="T1613">nada</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1615" id="Seg_53" n="HIAT:u" s="T1614">
                  <ts e="T1615" id="Seg_55" n="HIAT:w" s="T1614">Edəsʼtə</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1624" id="Seg_59" n="HIAT:u" s="T1615">
                  <ts e="T1616" id="Seg_61" n="HIAT:w" s="T1615">Dĭgəttə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1617" id="Seg_64" n="HIAT:w" s="T1616">dĭ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1618" id="Seg_67" n="HIAT:w" s="T1617">măndə:</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1619" id="Seg_70" n="HIAT:w" s="T1618">Măn</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1620" id="Seg_73" n="HIAT:w" s="T1619">umeju</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1621" id="Seg_76" n="HIAT:w" s="T1620">zolota</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_78" n="HIAT:ip">(</nts>
                  <ts e="T1622" id="Seg_80" n="HIAT:w" s="T1621">barə-</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1623" id="Seg_83" n="HIAT:w" s="T1622">barəʔsittə=</ts>
                  <nts id="Seg_84" n="HIAT:ip">)</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1624" id="Seg_87" n="HIAT:w" s="T1623">kuʔsittə</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1627" id="Seg_91" n="HIAT:u" s="T1624">
                  <ts e="T1625" id="Seg_93" n="HIAT:w" s="T1624">Nada</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1626" id="Seg_96" n="HIAT:w" s="T1625">dʼü</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1627" id="Seg_99" n="HIAT:w" s="T1626">tarirzittə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1635" id="Seg_103" n="HIAT:u" s="T1627">
                  <ts e="T1628" id="Seg_105" n="HIAT:w" s="T1627">Dĭgəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1629" id="Seg_108" n="HIAT:w" s="T1628">šobiʔi</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1630" id="Seg_112" n="HIAT:w" s="T1629">koŋdə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1631" id="Seg_115" n="HIAT:w" s="T1630">nörbiʔi:</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <ts e="T1632" id="Seg_119" n="HIAT:w" s="T1631">Dĭ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1633" id="Seg_122" n="HIAT:w" s="T1632">kuʔləj</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1634" id="Seg_125" n="HIAT:w" s="T1633">zolota</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1635" id="Seg_128" n="HIAT:w" s="T1634">tănan</ts>
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1642" id="Seg_133" n="HIAT:u" s="T1635">
                  <ts e="T1636" id="Seg_135" n="HIAT:w" s="T1635">Dĭgəttə</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1637" id="Seg_138" n="HIAT:w" s="T1636">dĭ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1638" id="Seg_141" n="HIAT:w" s="T1637">šobi:</ts>
                  <nts id="Seg_142" n="HIAT:ip">"</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1639" id="Seg_145" n="HIAT:w" s="T1638">Tăn</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1640" id="Seg_149" n="HIAT:w" s="T1639">mămbim</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1641" id="Seg_153" n="HIAT:w" s="T1640">kuʔləl</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1642" id="Seg_156" n="HIAT:w" s="T1641">zolota</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1645" id="Seg_161" n="HIAT:u" s="T1642">
                  <ts e="T1643" id="Seg_163" n="HIAT:w" s="T1642">Dĭ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1644" id="Seg_166" n="HIAT:w" s="T1643">tarirbi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1645" id="Seg_169" n="HIAT:w" s="T1644">dʼü</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1648" id="Seg_173" n="HIAT:u" s="T1645">
                  <ts e="T1646" id="Seg_175" n="HIAT:w" s="T1645">Dĭgəttə:</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_177" n="HIAT:ip">"</nts>
                  <ts e="T1647" id="Seg_179" n="HIAT:w" s="T1646">Dettə</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1648" id="Seg_182" n="HIAT:w" s="T1647">zolota</ts>
                  <nts id="Seg_183" n="HIAT:ip">!</nts>
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1650" id="Seg_187" n="HIAT:u" s="T1648">
                  <ts e="T1649" id="Seg_189" n="HIAT:w" s="T1648">Dĭ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1650" id="Seg_192" n="HIAT:w" s="T1649">deʔpi</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1654" id="Seg_196" n="HIAT:u" s="T1650">
                  <ts e="T1651" id="Seg_198" n="HIAT:w" s="T1650">Šindidə</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1652" id="Seg_202" n="HIAT:w" s="T1651">abəs</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1653" id="Seg_205" n="HIAT:w" s="T1652">ej</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1654" id="Seg_208" n="HIAT:w" s="T1653">mĭbi</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1657" id="Seg_212" n="HIAT:u" s="T1654">
                  <ts e="T1655" id="Seg_214" n="HIAT:w" s="T1654">Sudʼja</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1656" id="Seg_217" n="HIAT:w" s="T1655">ej</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1657" id="Seg_220" n="HIAT:w" s="T1656">mĭbi</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1661" id="Seg_224" n="HIAT:u" s="T1657">
                  <ts e="T1658" id="Seg_226" n="HIAT:w" s="T1657">Iʔgö</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1659" id="Seg_229" n="HIAT:w" s="T1658">il</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1660" id="Seg_232" n="HIAT:w" s="T1659">ej</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1661" id="Seg_235" n="HIAT:w" s="T1660">mĭbiʔi</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1668" id="Seg_239" n="HIAT:u" s="T1661">
                  <ts e="T1662" id="Seg_241" n="HIAT:w" s="T1661">Dĭgəttə</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1663" id="Seg_244" n="HIAT:w" s="T1662">dĭ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1664" id="Seg_247" n="HIAT:w" s="T1663">măndə:</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1665" id="Seg_250" n="HIAT:w" s="T1664">măn</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1666" id="Seg_253" n="HIAT:w" s="T1665">kaməndə</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1667" id="Seg_256" n="HIAT:w" s="T1666">ej</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1668" id="Seg_259" n="HIAT:w" s="T1667">tojirbiam</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1670" id="Seg_263" n="HIAT:u" s="T1668">
                  <ts e="T1669" id="Seg_265" n="HIAT:w" s="T1668">Ej</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1670" id="Seg_268" n="HIAT:w" s="T1669">tojirlam</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1676" id="Seg_272" n="HIAT:u" s="T1670">
                  <ts e="T1671" id="Seg_274" n="HIAT:w" s="T1670">A</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672" id="Seg_277" n="HIAT:w" s="T1671">tăn</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1673" id="Seg_280" n="HIAT:w" s="T1672">măna</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ts e="T1674" id="Seg_284" n="HIAT:w" s="T1673">kuʔsit-</ts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1675" id="Seg_288" n="HIAT:w" s="T1674">kuʔsittə</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1676" id="Seg_291" n="HIAT:w" s="T1675">xatʼel</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1680" id="Seg_295" n="HIAT:u" s="T1676">
                  <ts e="T1677" id="Seg_297" n="HIAT:w" s="T1676">Dĭgəttə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1678" id="Seg_300" n="HIAT:w" s="T1677">maluʔpi</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1679" id="Seg_303" n="HIAT:w" s="T1678">dĭ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1680" id="Seg_306" n="HIAT:w" s="T1679">kuza</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1681" id="Seg_310" n="HIAT:u" s="T1680">
                  <ts e="T1681" id="Seg_312" n="HIAT:w" s="T1680">Kabarləj</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1681" id="Seg_315" n="sc" s="T1601">
               <ts e="T1602" id="Seg_317" n="e" s="T1601">Onʼiʔ </ts>
               <ts e="T1603" id="Seg_319" n="e" s="T1602">kuza </ts>
               <ts e="T1604" id="Seg_321" n="e" s="T1603">dʼaʔpiʔi. </ts>
               <ts e="T1605" id="Seg_323" n="e" s="T1604">Dĭ </ts>
               <ts e="T1606" id="Seg_325" n="e" s="T1605">ĭmbidə </ts>
               <ts e="T1607" id="Seg_327" n="e" s="T1606">ej </ts>
               <ts e="T1608" id="Seg_329" n="e" s="T1607">tojirbi. </ts>
               <ts e="T1609" id="Seg_331" n="e" s="T1608">Deʔpiʔi </ts>
               <ts e="T1610" id="Seg_333" n="e" s="T1609">dĭm </ts>
               <ts e="T1611" id="Seg_335" n="e" s="T1610">koŋdə. </ts>
               <ts e="T1612" id="Seg_337" n="e" s="T1611">Dĭm </ts>
               <ts e="T1613" id="Seg_339" n="e" s="T1612">kuʔsʼittə </ts>
               <ts e="T1614" id="Seg_341" n="e" s="T1613">nada. </ts>
               <ts e="T1615" id="Seg_343" n="e" s="T1614">Edəsʼtə. </ts>
               <ts e="T1616" id="Seg_345" n="e" s="T1615">Dĭgəttə </ts>
               <ts e="T1617" id="Seg_347" n="e" s="T1616">dĭ </ts>
               <ts e="T1618" id="Seg_349" n="e" s="T1617">măndə: </ts>
               <ts e="T1619" id="Seg_351" n="e" s="T1618">Măn </ts>
               <ts e="T1620" id="Seg_353" n="e" s="T1619">umeju </ts>
               <ts e="T1621" id="Seg_355" n="e" s="T1620">zolota </ts>
               <ts e="T1622" id="Seg_357" n="e" s="T1621">(barə- </ts>
               <ts e="T1623" id="Seg_359" n="e" s="T1622">barəʔsittə=) </ts>
               <ts e="T1624" id="Seg_361" n="e" s="T1623">kuʔsittə. </ts>
               <ts e="T1625" id="Seg_363" n="e" s="T1624">Nada </ts>
               <ts e="T1626" id="Seg_365" n="e" s="T1625">dʼü </ts>
               <ts e="T1627" id="Seg_367" n="e" s="T1626">tarirzittə. </ts>
               <ts e="T1628" id="Seg_369" n="e" s="T1627">Dĭgəttə </ts>
               <ts e="T1629" id="Seg_371" n="e" s="T1628">šobiʔi, </ts>
               <ts e="T1630" id="Seg_373" n="e" s="T1629">koŋdə </ts>
               <ts e="T1631" id="Seg_375" n="e" s="T1630">nörbiʔi: </ts>
               <ts e="T1632" id="Seg_377" n="e" s="T1631">"Dĭ </ts>
               <ts e="T1633" id="Seg_379" n="e" s="T1632">kuʔləj </ts>
               <ts e="T1634" id="Seg_381" n="e" s="T1633">zolota </ts>
               <ts e="T1635" id="Seg_383" n="e" s="T1634">tănan". </ts>
               <ts e="T1636" id="Seg_385" n="e" s="T1635">Dĭgəttə </ts>
               <ts e="T1637" id="Seg_387" n="e" s="T1636">dĭ </ts>
               <ts e="T1638" id="Seg_389" n="e" s="T1637">šobi:" </ts>
               <ts e="T1639" id="Seg_391" n="e" s="T1638">Tăn, </ts>
               <ts e="T1640" id="Seg_393" n="e" s="T1639">mămbim, </ts>
               <ts e="T1641" id="Seg_395" n="e" s="T1640">kuʔləl </ts>
               <ts e="T1642" id="Seg_397" n="e" s="T1641">zolota". </ts>
               <ts e="T1643" id="Seg_399" n="e" s="T1642">Dĭ </ts>
               <ts e="T1644" id="Seg_401" n="e" s="T1643">tarirbi </ts>
               <ts e="T1645" id="Seg_403" n="e" s="T1644">dʼü. </ts>
               <ts e="T1646" id="Seg_405" n="e" s="T1645">Dĭgəttə: </ts>
               <ts e="T1647" id="Seg_407" n="e" s="T1646">"Dettə </ts>
               <ts e="T1648" id="Seg_409" n="e" s="T1647">zolota!" </ts>
               <ts e="T1649" id="Seg_411" n="e" s="T1648">Dĭ </ts>
               <ts e="T1650" id="Seg_413" n="e" s="T1649">deʔpi. </ts>
               <ts e="T1651" id="Seg_415" n="e" s="T1650">Šindidə, </ts>
               <ts e="T1652" id="Seg_417" n="e" s="T1651">abəs </ts>
               <ts e="T1653" id="Seg_419" n="e" s="T1652">ej </ts>
               <ts e="T1654" id="Seg_421" n="e" s="T1653">mĭbi. </ts>
               <ts e="T1655" id="Seg_423" n="e" s="T1654">Sudʼja </ts>
               <ts e="T1656" id="Seg_425" n="e" s="T1655">ej </ts>
               <ts e="T1657" id="Seg_427" n="e" s="T1656">mĭbi. </ts>
               <ts e="T1658" id="Seg_429" n="e" s="T1657">Iʔgö </ts>
               <ts e="T1659" id="Seg_431" n="e" s="T1658">il </ts>
               <ts e="T1660" id="Seg_433" n="e" s="T1659">ej </ts>
               <ts e="T1661" id="Seg_435" n="e" s="T1660">mĭbiʔi. </ts>
               <ts e="T1662" id="Seg_437" n="e" s="T1661">Dĭgəttə </ts>
               <ts e="T1663" id="Seg_439" n="e" s="T1662">dĭ </ts>
               <ts e="T1664" id="Seg_441" n="e" s="T1663">măndə: </ts>
               <ts e="T1665" id="Seg_443" n="e" s="T1664">măn </ts>
               <ts e="T1666" id="Seg_445" n="e" s="T1665">kaməndə </ts>
               <ts e="T1667" id="Seg_447" n="e" s="T1666">ej </ts>
               <ts e="T1668" id="Seg_449" n="e" s="T1667">tojirbiam. </ts>
               <ts e="T1669" id="Seg_451" n="e" s="T1668">Ej </ts>
               <ts e="T1670" id="Seg_453" n="e" s="T1669">tojirlam. </ts>
               <ts e="T1671" id="Seg_455" n="e" s="T1670">A </ts>
               <ts e="T1672" id="Seg_457" n="e" s="T1671">tăn </ts>
               <ts e="T1673" id="Seg_459" n="e" s="T1672">măna </ts>
               <ts e="T1674" id="Seg_461" n="e" s="T1673">(kuʔsit-) </ts>
               <ts e="T1675" id="Seg_463" n="e" s="T1674">kuʔsittə </ts>
               <ts e="T1676" id="Seg_465" n="e" s="T1675">xatʼel. </ts>
               <ts e="T1677" id="Seg_467" n="e" s="T1676">Dĭgəttə </ts>
               <ts e="T1678" id="Seg_469" n="e" s="T1677">maluʔpi </ts>
               <ts e="T1679" id="Seg_471" n="e" s="T1678">dĭ </ts>
               <ts e="T1680" id="Seg_473" n="e" s="T1679">kuza. </ts>
               <ts e="T1681" id="Seg_475" n="e" s="T1680">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1604" id="Seg_476" s="T1601">PKZ_196X_ManSowsGold_flk.001 (001)</ta>
            <ta e="T1608" id="Seg_477" s="T1604">PKZ_196X_ManSowsGold_flk.002 (002)</ta>
            <ta e="T1611" id="Seg_478" s="T1608">PKZ_196X_ManSowsGold_flk.003 (003)</ta>
            <ta e="T1614" id="Seg_479" s="T1611">PKZ_196X_ManSowsGold_flk.004 (004)</ta>
            <ta e="T1615" id="Seg_480" s="T1614">PKZ_196X_ManSowsGold_flk.005 (005)</ta>
            <ta e="T1624" id="Seg_481" s="T1615">PKZ_196X_ManSowsGold_flk.006 (006) </ta>
            <ta e="T1627" id="Seg_482" s="T1624">PKZ_196X_ManSowsGold_flk.007 (008)</ta>
            <ta e="T1635" id="Seg_483" s="T1627">PKZ_196X_ManSowsGold_flk.008 (009) </ta>
            <ta e="T1642" id="Seg_484" s="T1635">PKZ_196X_ManSowsGold_flk.009 (011)</ta>
            <ta e="T1645" id="Seg_485" s="T1642">PKZ_196X_ManSowsGold_flk.010 (012)</ta>
            <ta e="T1648" id="Seg_486" s="T1645">PKZ_196X_ManSowsGold_flk.011 (013)</ta>
            <ta e="T1650" id="Seg_487" s="T1648">PKZ_196X_ManSowsGold_flk.012 (014)</ta>
            <ta e="T1654" id="Seg_488" s="T1650">PKZ_196X_ManSowsGold_flk.013 (015)</ta>
            <ta e="T1657" id="Seg_489" s="T1654">PKZ_196X_ManSowsGold_flk.014 (016)</ta>
            <ta e="T1661" id="Seg_490" s="T1657">PKZ_196X_ManSowsGold_flk.015 (017)</ta>
            <ta e="T1668" id="Seg_491" s="T1661">PKZ_196X_ManSowsGold_flk.016 (018)</ta>
            <ta e="T1670" id="Seg_492" s="T1668">PKZ_196X_ManSowsGold_flk.017 (019)</ta>
            <ta e="T1676" id="Seg_493" s="T1670">PKZ_196X_ManSowsGold_flk.018 (020)</ta>
            <ta e="T1680" id="Seg_494" s="T1676">PKZ_196X_ManSowsGold_flk.019 (021)</ta>
            <ta e="T1681" id="Seg_495" s="T1680">PKZ_196X_ManSowsGold_flk.020 (022)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1604" id="Seg_496" s="T1601">Onʼiʔ kuza dʼaʔpiʔi. </ta>
            <ta e="T1608" id="Seg_497" s="T1604">Dĭ ĭmbidə ej tojirbi. </ta>
            <ta e="T1611" id="Seg_498" s="T1608">Deʔpiʔi dĭm koŋdə. </ta>
            <ta e="T1614" id="Seg_499" s="T1611">Dĭm kuʔsʼittə nada. </ta>
            <ta e="T1615" id="Seg_500" s="T1614">Edəsʼtə. </ta>
            <ta e="T1624" id="Seg_501" s="T1615">Dĭgəttə dĭ măndə: Măn umeju zolota (barə- barəʔsittə=) kuʔsittə. </ta>
            <ta e="T1627" id="Seg_502" s="T1624">Nada dʼü tarirzittə. </ta>
            <ta e="T1635" id="Seg_503" s="T1627">Dĭgəttə šobiʔi, koŋdə nörbiʔi: "Dĭ kuʔləj zolota tănan". </ta>
            <ta e="T1642" id="Seg_504" s="T1635">Dĭgəttə dĭ šobi:" Tăn, mămbim, kuʔləl zolota". </ta>
            <ta e="T1645" id="Seg_505" s="T1642">Dĭ tarirbi dʼü. </ta>
            <ta e="T1648" id="Seg_506" s="T1645">Dĭgəttə: "Dettə zolota!" </ta>
            <ta e="T1650" id="Seg_507" s="T1648">Dĭ deʔpi. </ta>
            <ta e="T1654" id="Seg_508" s="T1650">Šindidə, abəs ej mĭbi. </ta>
            <ta e="T1657" id="Seg_509" s="T1654">Sudʼja ej mĭbi. </ta>
            <ta e="T1661" id="Seg_510" s="T1657">Iʔgö il ej mĭbiʔi. </ta>
            <ta e="T1668" id="Seg_511" s="T1661">Dĭgəttə dĭ măndə: măn kaməndə ej tojirbiam. </ta>
            <ta e="T1670" id="Seg_512" s="T1668">Ej tojirlam. </ta>
            <ta e="T1676" id="Seg_513" s="T1670">A tăn măna (kuʔsit-) kuʔsittə xatʼel. </ta>
            <ta e="T1680" id="Seg_514" s="T1676">Dĭgəttə maluʔpi dĭ kuza. </ta>
            <ta e="T1681" id="Seg_515" s="T1680">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1602" id="Seg_516" s="T1601">onʼiʔ</ta>
            <ta e="T1603" id="Seg_517" s="T1602">kuza</ta>
            <ta e="T1604" id="Seg_518" s="T1603">dʼaʔ-pi-ʔi</ta>
            <ta e="T1605" id="Seg_519" s="T1604">dĭ</ta>
            <ta e="T1606" id="Seg_520" s="T1605">ĭmbi=də</ta>
            <ta e="T1607" id="Seg_521" s="T1606">ej</ta>
            <ta e="T1608" id="Seg_522" s="T1607">tojir-bi</ta>
            <ta e="T1609" id="Seg_523" s="T1608">deʔ-pi-ʔi</ta>
            <ta e="T1610" id="Seg_524" s="T1609">dĭ-m</ta>
            <ta e="T1611" id="Seg_525" s="T1610">koŋ-də</ta>
            <ta e="T1612" id="Seg_526" s="T1611">dĭ-m</ta>
            <ta e="T1613" id="Seg_527" s="T1612">kuʔ-sʼittə</ta>
            <ta e="T1614" id="Seg_528" s="T1613">nada</ta>
            <ta e="T1615" id="Seg_529" s="T1614">edə-sʼtə</ta>
            <ta e="T1616" id="Seg_530" s="T1615">dĭgəttə</ta>
            <ta e="T1617" id="Seg_531" s="T1616">dĭ</ta>
            <ta e="T1618" id="Seg_532" s="T1617">măn-də</ta>
            <ta e="T1619" id="Seg_533" s="T1618">măn</ta>
            <ta e="T1621" id="Seg_534" s="T1620">zolota</ta>
            <ta e="T1623" id="Seg_535" s="T1622">barəʔ-sittə</ta>
            <ta e="T1624" id="Seg_536" s="T1623">kuʔ-sittə</ta>
            <ta e="T1625" id="Seg_537" s="T1624">nada</ta>
            <ta e="T1626" id="Seg_538" s="T1625">dʼü</ta>
            <ta e="T1627" id="Seg_539" s="T1626">tarir-zittə</ta>
            <ta e="T1628" id="Seg_540" s="T1627">dĭgəttə</ta>
            <ta e="T1629" id="Seg_541" s="T1628">šo-bi-ʔi</ta>
            <ta e="T1630" id="Seg_542" s="T1629">koŋ-də</ta>
            <ta e="T1631" id="Seg_543" s="T1630">nör-bi-ʔi</ta>
            <ta e="T1632" id="Seg_544" s="T1631">dĭ</ta>
            <ta e="T1633" id="Seg_545" s="T1632">kuʔ-lə-j</ta>
            <ta e="T1634" id="Seg_546" s="T1633">zolota</ta>
            <ta e="T1635" id="Seg_547" s="T1634">tănan</ta>
            <ta e="T1636" id="Seg_548" s="T1635">dĭgəttə</ta>
            <ta e="T1637" id="Seg_549" s="T1636">dĭ</ta>
            <ta e="T1638" id="Seg_550" s="T1637">šo-bi</ta>
            <ta e="T1639" id="Seg_551" s="T1638">tăn</ta>
            <ta e="T1640" id="Seg_552" s="T1639">măm-bi-m</ta>
            <ta e="T1641" id="Seg_553" s="T1640">kuʔ-lə-l</ta>
            <ta e="T1642" id="Seg_554" s="T1641">zolota</ta>
            <ta e="T1643" id="Seg_555" s="T1642">dĭ</ta>
            <ta e="T1644" id="Seg_556" s="T1643">tarir-bi</ta>
            <ta e="T1645" id="Seg_557" s="T1644">dʼü</ta>
            <ta e="T1646" id="Seg_558" s="T1645">dĭgəttə</ta>
            <ta e="T1647" id="Seg_559" s="T1646">det-tə</ta>
            <ta e="T1648" id="Seg_560" s="T1647">zolota</ta>
            <ta e="T1649" id="Seg_561" s="T1648">dĭ</ta>
            <ta e="T1650" id="Seg_562" s="T1649">deʔ-pi</ta>
            <ta e="T1651" id="Seg_563" s="T1650">šindi=də</ta>
            <ta e="T1652" id="Seg_564" s="T1651">abəs</ta>
            <ta e="T1653" id="Seg_565" s="T1652">ej</ta>
            <ta e="T1654" id="Seg_566" s="T1653">mĭ-bi</ta>
            <ta e="T1655" id="Seg_567" s="T1654">sudʼja</ta>
            <ta e="T1656" id="Seg_568" s="T1655">ej</ta>
            <ta e="T1657" id="Seg_569" s="T1656">mĭ-bi</ta>
            <ta e="T1658" id="Seg_570" s="T1657">iʔgö</ta>
            <ta e="T1659" id="Seg_571" s="T1658">il</ta>
            <ta e="T1660" id="Seg_572" s="T1659">ej</ta>
            <ta e="T1661" id="Seg_573" s="T1660">mĭ-bi-ʔi</ta>
            <ta e="T1662" id="Seg_574" s="T1661">dĭgəttə</ta>
            <ta e="T1663" id="Seg_575" s="T1662">dĭ</ta>
            <ta e="T1664" id="Seg_576" s="T1663">măn-də</ta>
            <ta e="T1665" id="Seg_577" s="T1664">măn</ta>
            <ta e="T1666" id="Seg_578" s="T1665">kamən=də</ta>
            <ta e="T1667" id="Seg_579" s="T1666">ej</ta>
            <ta e="T1668" id="Seg_580" s="T1667">tojir-bia-m</ta>
            <ta e="T1669" id="Seg_581" s="T1668">ej</ta>
            <ta e="T1670" id="Seg_582" s="T1669">tojir-la-m</ta>
            <ta e="T1671" id="Seg_583" s="T1670">a</ta>
            <ta e="T1672" id="Seg_584" s="T1671">tăn</ta>
            <ta e="T1673" id="Seg_585" s="T1672">măna</ta>
            <ta e="T1675" id="Seg_586" s="T1674">kuʔ-sittə</ta>
            <ta e="T1676" id="Seg_587" s="T1675">xatʼel</ta>
            <ta e="T1677" id="Seg_588" s="T1676">dĭgəttə</ta>
            <ta e="T1678" id="Seg_589" s="T1677">ma-luʔ-pi</ta>
            <ta e="T1679" id="Seg_590" s="T1678">dĭ</ta>
            <ta e="T1680" id="Seg_591" s="T1679">kuza</ta>
            <ta e="T1681" id="Seg_592" s="T1680">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1602" id="Seg_593" s="T1601">onʼiʔ</ta>
            <ta e="T1603" id="Seg_594" s="T1602">kuza</ta>
            <ta e="T1604" id="Seg_595" s="T1603">dʼabə-bi-jəʔ</ta>
            <ta e="T1605" id="Seg_596" s="T1604">dĭ</ta>
            <ta e="T1606" id="Seg_597" s="T1605">ĭmbi=də</ta>
            <ta e="T1607" id="Seg_598" s="T1606">ej</ta>
            <ta e="T1608" id="Seg_599" s="T1607">tojar-bi</ta>
            <ta e="T1609" id="Seg_600" s="T1608">det-bi-jəʔ</ta>
            <ta e="T1610" id="Seg_601" s="T1609">dĭ-m</ta>
            <ta e="T1611" id="Seg_602" s="T1610">koŋ-Tə</ta>
            <ta e="T1612" id="Seg_603" s="T1611">dĭ-m</ta>
            <ta e="T1613" id="Seg_604" s="T1612">kut-zittə</ta>
            <ta e="T1614" id="Seg_605" s="T1613">nadə</ta>
            <ta e="T1615" id="Seg_606" s="T1614">edə-zittə</ta>
            <ta e="T1616" id="Seg_607" s="T1615">dĭgəttə</ta>
            <ta e="T1617" id="Seg_608" s="T1616">dĭ</ta>
            <ta e="T1618" id="Seg_609" s="T1617">măn-ntə</ta>
            <ta e="T1619" id="Seg_610" s="T1618">măn</ta>
            <ta e="T1621" id="Seg_611" s="T1620">zolota</ta>
            <ta e="T1623" id="Seg_612" s="T1622">barəʔ-zittə</ta>
            <ta e="T1624" id="Seg_613" s="T1623">kuʔ-zittə</ta>
            <ta e="T1625" id="Seg_614" s="T1624">nadə</ta>
            <ta e="T1626" id="Seg_615" s="T1625">tʼo</ta>
            <ta e="T1627" id="Seg_616" s="T1626">tajər-zittə</ta>
            <ta e="T1628" id="Seg_617" s="T1627">dĭgəttə</ta>
            <ta e="T1629" id="Seg_618" s="T1628">šo-bi-jəʔ</ta>
            <ta e="T1630" id="Seg_619" s="T1629">koŋ-Tə</ta>
            <ta e="T1631" id="Seg_620" s="T1630">nörbə-bi-jəʔ</ta>
            <ta e="T1632" id="Seg_621" s="T1631">dĭ</ta>
            <ta e="T1633" id="Seg_622" s="T1632">kuʔ-lV-j</ta>
            <ta e="T1634" id="Seg_623" s="T1633">zolota</ta>
            <ta e="T1635" id="Seg_624" s="T1634">tănan</ta>
            <ta e="T1636" id="Seg_625" s="T1635">dĭgəttə</ta>
            <ta e="T1637" id="Seg_626" s="T1636">dĭ</ta>
            <ta e="T1638" id="Seg_627" s="T1637">šo-bi</ta>
            <ta e="T1639" id="Seg_628" s="T1638">tăn</ta>
            <ta e="T1640" id="Seg_629" s="T1639">măn-bi-m</ta>
            <ta e="T1641" id="Seg_630" s="T1640">kuʔ-lV-l</ta>
            <ta e="T1642" id="Seg_631" s="T1641">zolota</ta>
            <ta e="T1643" id="Seg_632" s="T1642">dĭ</ta>
            <ta e="T1644" id="Seg_633" s="T1643">tajər-bi</ta>
            <ta e="T1645" id="Seg_634" s="T1644">tʼo</ta>
            <ta e="T1646" id="Seg_635" s="T1645">dĭgəttə</ta>
            <ta e="T1647" id="Seg_636" s="T1646">det-t</ta>
            <ta e="T1648" id="Seg_637" s="T1647">zolota</ta>
            <ta e="T1649" id="Seg_638" s="T1648">dĭ</ta>
            <ta e="T1650" id="Seg_639" s="T1649">det-bi</ta>
            <ta e="T1651" id="Seg_640" s="T1650">šində=də</ta>
            <ta e="T1652" id="Seg_641" s="T1651">abəs</ta>
            <ta e="T1653" id="Seg_642" s="T1652">ej</ta>
            <ta e="T1654" id="Seg_643" s="T1653">mĭ-bi</ta>
            <ta e="T1655" id="Seg_644" s="T1654">sudʼja</ta>
            <ta e="T1656" id="Seg_645" s="T1655">ej</ta>
            <ta e="T1657" id="Seg_646" s="T1656">mĭ-bi</ta>
            <ta e="T1658" id="Seg_647" s="T1657">iʔgö</ta>
            <ta e="T1659" id="Seg_648" s="T1658">il</ta>
            <ta e="T1660" id="Seg_649" s="T1659">ej</ta>
            <ta e="T1661" id="Seg_650" s="T1660">mĭ-bi-jəʔ</ta>
            <ta e="T1662" id="Seg_651" s="T1661">dĭgəttə</ta>
            <ta e="T1663" id="Seg_652" s="T1662">dĭ</ta>
            <ta e="T1664" id="Seg_653" s="T1663">măn-ntə</ta>
            <ta e="T1665" id="Seg_654" s="T1664">măn</ta>
            <ta e="T1666" id="Seg_655" s="T1665">kamən=də</ta>
            <ta e="T1667" id="Seg_656" s="T1666">ej</ta>
            <ta e="T1668" id="Seg_657" s="T1667">tojar-bi-m</ta>
            <ta e="T1669" id="Seg_658" s="T1668">ej</ta>
            <ta e="T1670" id="Seg_659" s="T1669">tojar-lV-m</ta>
            <ta e="T1671" id="Seg_660" s="T1670">a</ta>
            <ta e="T1672" id="Seg_661" s="T1671">tăn</ta>
            <ta e="T1673" id="Seg_662" s="T1672">măna</ta>
            <ta e="T1675" id="Seg_663" s="T1674">kut-zittə</ta>
            <ta e="T1676" id="Seg_664" s="T1675">xatʼel</ta>
            <ta e="T1677" id="Seg_665" s="T1676">dĭgəttə</ta>
            <ta e="T1678" id="Seg_666" s="T1677">ma-luʔbdə-bi</ta>
            <ta e="T1679" id="Seg_667" s="T1678">dĭ</ta>
            <ta e="T1680" id="Seg_668" s="T1679">kuza</ta>
            <ta e="T1681" id="Seg_669" s="T1680">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1602" id="Seg_670" s="T1601">one.[NOM.SG]</ta>
            <ta e="T1603" id="Seg_671" s="T1602">man.[NOM.SG]</ta>
            <ta e="T1604" id="Seg_672" s="T1603">capture-PST-3PL</ta>
            <ta e="T1605" id="Seg_673" s="T1604">this.[NOM.SG]</ta>
            <ta e="T1606" id="Seg_674" s="T1605">what.[NOM.SG]=INDEF</ta>
            <ta e="T1607" id="Seg_675" s="T1606">NEG</ta>
            <ta e="T1608" id="Seg_676" s="T1607">steal-PST.[3SG]</ta>
            <ta e="T1609" id="Seg_677" s="T1608">bring-PST-3PL</ta>
            <ta e="T1610" id="Seg_678" s="T1609">this-ACC</ta>
            <ta e="T1611" id="Seg_679" s="T1610">chief-LAT</ta>
            <ta e="T1612" id="Seg_680" s="T1611">this-ACC</ta>
            <ta e="T1613" id="Seg_681" s="T1612">kill-INF.LAT</ta>
            <ta e="T1614" id="Seg_682" s="T1613">one.should</ta>
            <ta e="T1615" id="Seg_683" s="T1614">hang.up-INF.LAT</ta>
            <ta e="T1616" id="Seg_684" s="T1615">then</ta>
            <ta e="T1617" id="Seg_685" s="T1616">this.[NOM.SG]</ta>
            <ta e="T1618" id="Seg_686" s="T1617">say-IPFVZ.[3SG]</ta>
            <ta e="T1619" id="Seg_687" s="T1618">I.NOM</ta>
            <ta e="T1621" id="Seg_688" s="T1620">gold.[NOM.SG]</ta>
            <ta e="T1623" id="Seg_689" s="T1622">throw.away-INF.LAT</ta>
            <ta e="T1624" id="Seg_690" s="T1623">sow-INF.LAT</ta>
            <ta e="T1625" id="Seg_691" s="T1624">one.should</ta>
            <ta e="T1626" id="Seg_692" s="T1625">earth.[NOM.SG]</ta>
            <ta e="T1627" id="Seg_693" s="T1626">plough-INF.LAT</ta>
            <ta e="T1628" id="Seg_694" s="T1627">then</ta>
            <ta e="T1629" id="Seg_695" s="T1628">come-PST-3PL</ta>
            <ta e="T1630" id="Seg_696" s="T1629">chief-LAT</ta>
            <ta e="T1631" id="Seg_697" s="T1630">tell-PST-3PL</ta>
            <ta e="T1632" id="Seg_698" s="T1631">this</ta>
            <ta e="T1633" id="Seg_699" s="T1632">sow-FUT-ADJZ</ta>
            <ta e="T1634" id="Seg_700" s="T1633">gold.[NOM.SG]</ta>
            <ta e="T1635" id="Seg_701" s="T1634">you.DAT</ta>
            <ta e="T1636" id="Seg_702" s="T1635">then</ta>
            <ta e="T1637" id="Seg_703" s="T1636">this.[NOM.SG]</ta>
            <ta e="T1638" id="Seg_704" s="T1637">come-PST.[3SG]</ta>
            <ta e="T1639" id="Seg_705" s="T1638">you.NOM</ta>
            <ta e="T1640" id="Seg_706" s="T1639">say-PST-1SG</ta>
            <ta e="T1641" id="Seg_707" s="T1640">sprinkle-FUT-2SG</ta>
            <ta e="T1642" id="Seg_708" s="T1641">gold.[NOM.SG]</ta>
            <ta e="T1643" id="Seg_709" s="T1642">this.[NOM.SG]</ta>
            <ta e="T1644" id="Seg_710" s="T1643">plough-PST.[3SG]</ta>
            <ta e="T1645" id="Seg_711" s="T1644">earth.[NOM.SG]</ta>
            <ta e="T1646" id="Seg_712" s="T1645">then</ta>
            <ta e="T1647" id="Seg_713" s="T1646">bring-IMP.2SG.O</ta>
            <ta e="T1648" id="Seg_714" s="T1647">gold.[NOM.SG]</ta>
            <ta e="T1649" id="Seg_715" s="T1648">this.[NOM.SG]</ta>
            <ta e="T1650" id="Seg_716" s="T1649">bring-PST.[3SG]</ta>
            <ta e="T1651" id="Seg_717" s="T1650">who.[NOM.SG]=INDEF</ta>
            <ta e="T1652" id="Seg_718" s="T1651">priest.[NOM.SG]</ta>
            <ta e="T1653" id="Seg_719" s="T1652">NEG</ta>
            <ta e="T1654" id="Seg_720" s="T1653">give-PST.[3SG]</ta>
            <ta e="T1655" id="Seg_721" s="T1654">judge</ta>
            <ta e="T1656" id="Seg_722" s="T1655">NEG</ta>
            <ta e="T1657" id="Seg_723" s="T1656">give-PST.[3SG]</ta>
            <ta e="T1658" id="Seg_724" s="T1657">many</ta>
            <ta e="T1659" id="Seg_725" s="T1658">people.[NOM.SG]</ta>
            <ta e="T1660" id="Seg_726" s="T1659">NEG</ta>
            <ta e="T1661" id="Seg_727" s="T1660">give-PST-3PL</ta>
            <ta e="T1662" id="Seg_728" s="T1661">then</ta>
            <ta e="T1663" id="Seg_729" s="T1662">this.[NOM.SG]</ta>
            <ta e="T1664" id="Seg_730" s="T1663">say-IPFVZ.[3SG]</ta>
            <ta e="T1665" id="Seg_731" s="T1664">I.NOM</ta>
            <ta e="T1666" id="Seg_732" s="T1665">when=INDEF</ta>
            <ta e="T1667" id="Seg_733" s="T1666">NEG</ta>
            <ta e="T1668" id="Seg_734" s="T1667">steal-PST-1SG</ta>
            <ta e="T1669" id="Seg_735" s="T1668">NEG</ta>
            <ta e="T1670" id="Seg_736" s="T1669">steal-FUT-1SG</ta>
            <ta e="T1671" id="Seg_737" s="T1670">and</ta>
            <ta e="T1672" id="Seg_738" s="T1671">you.NOM</ta>
            <ta e="T1673" id="Seg_739" s="T1672">I.ACC</ta>
            <ta e="T1675" id="Seg_740" s="T1674">kill-INF.LAT</ta>
            <ta e="T1676" id="Seg_741" s="T1675">want.PST.M.SG</ta>
            <ta e="T1677" id="Seg_742" s="T1676">then</ta>
            <ta e="T1678" id="Seg_743" s="T1677">remain-MOM-PST.[3SG]</ta>
            <ta e="T1679" id="Seg_744" s="T1678">this.[NOM.SG]</ta>
            <ta e="T1680" id="Seg_745" s="T1679">man.[NOM.SG]</ta>
            <ta e="T1681" id="Seg_746" s="T1680">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1602" id="Seg_747" s="T1601">один.[NOM.SG]</ta>
            <ta e="T1603" id="Seg_748" s="T1602">мужчина.[NOM.SG]</ta>
            <ta e="T1604" id="Seg_749" s="T1603">ловить-PST-3PL</ta>
            <ta e="T1605" id="Seg_750" s="T1604">этот.[NOM.SG]</ta>
            <ta e="T1606" id="Seg_751" s="T1605">что.[NOM.SG]=INDEF</ta>
            <ta e="T1607" id="Seg_752" s="T1606">NEG</ta>
            <ta e="T1608" id="Seg_753" s="T1607">украсть-PST.[3SG]</ta>
            <ta e="T1609" id="Seg_754" s="T1608">принести-PST-3PL</ta>
            <ta e="T1610" id="Seg_755" s="T1609">этот-ACC</ta>
            <ta e="T1611" id="Seg_756" s="T1610">вождь-LAT</ta>
            <ta e="T1612" id="Seg_757" s="T1611">этот-ACC</ta>
            <ta e="T1613" id="Seg_758" s="T1612">убить-INF.LAT</ta>
            <ta e="T1614" id="Seg_759" s="T1613">надо</ta>
            <ta e="T1615" id="Seg_760" s="T1614">вешать-INF.LAT</ta>
            <ta e="T1616" id="Seg_761" s="T1615">тогда</ta>
            <ta e="T1617" id="Seg_762" s="T1616">этот.[NOM.SG]</ta>
            <ta e="T1618" id="Seg_763" s="T1617">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1619" id="Seg_764" s="T1618">я.NOM</ta>
            <ta e="T1621" id="Seg_765" s="T1620">золото.[NOM.SG]</ta>
            <ta e="T1623" id="Seg_766" s="T1622">выбросить-INF.LAT</ta>
            <ta e="T1624" id="Seg_767" s="T1623">сеять-INF.LAT</ta>
            <ta e="T1625" id="Seg_768" s="T1624">надо</ta>
            <ta e="T1626" id="Seg_769" s="T1625">земля.[NOM.SG]</ta>
            <ta e="T1627" id="Seg_770" s="T1626">пахать-INF.LAT</ta>
            <ta e="T1628" id="Seg_771" s="T1627">тогда</ta>
            <ta e="T1629" id="Seg_772" s="T1628">прийти-PST-3PL</ta>
            <ta e="T1630" id="Seg_773" s="T1629">вождь-LAT</ta>
            <ta e="T1631" id="Seg_774" s="T1630">сказать-PST-3PL</ta>
            <ta e="T1632" id="Seg_775" s="T1631">этот</ta>
            <ta e="T1633" id="Seg_776" s="T1632">сеять-FUT-ADJZ</ta>
            <ta e="T1634" id="Seg_777" s="T1633">золото.[NOM.SG]</ta>
            <ta e="T1635" id="Seg_778" s="T1634">ты.DAT</ta>
            <ta e="T1636" id="Seg_779" s="T1635">тогда</ta>
            <ta e="T1637" id="Seg_780" s="T1636">этот.[NOM.SG]</ta>
            <ta e="T1638" id="Seg_781" s="T1637">прийти-PST.[3SG]</ta>
            <ta e="T1639" id="Seg_782" s="T1638">ты.NOM</ta>
            <ta e="T1640" id="Seg_783" s="T1639">сказать-PST-1SG</ta>
            <ta e="T1641" id="Seg_784" s="T1640">брызгать-FUT-2SG</ta>
            <ta e="T1642" id="Seg_785" s="T1641">золото.[NOM.SG]</ta>
            <ta e="T1643" id="Seg_786" s="T1642">этот.[NOM.SG]</ta>
            <ta e="T1644" id="Seg_787" s="T1643">пахать-PST.[3SG]</ta>
            <ta e="T1645" id="Seg_788" s="T1644">земля.[NOM.SG]</ta>
            <ta e="T1646" id="Seg_789" s="T1645">тогда</ta>
            <ta e="T1647" id="Seg_790" s="T1646">принести-IMP.2SG.O</ta>
            <ta e="T1648" id="Seg_791" s="T1647">золото.[NOM.SG]</ta>
            <ta e="T1649" id="Seg_792" s="T1648">этот.[NOM.SG]</ta>
            <ta e="T1650" id="Seg_793" s="T1649">принести-PST.[3SG]</ta>
            <ta e="T1651" id="Seg_794" s="T1650">кто.[NOM.SG]=INDEF</ta>
            <ta e="T1652" id="Seg_795" s="T1651">священник.[NOM.SG]</ta>
            <ta e="T1653" id="Seg_796" s="T1652">NEG</ta>
            <ta e="T1654" id="Seg_797" s="T1653">дать-PST.[3SG]</ta>
            <ta e="T1655" id="Seg_798" s="T1654">судья</ta>
            <ta e="T1656" id="Seg_799" s="T1655">NEG</ta>
            <ta e="T1657" id="Seg_800" s="T1656">дать-PST.[3SG]</ta>
            <ta e="T1658" id="Seg_801" s="T1657">много</ta>
            <ta e="T1659" id="Seg_802" s="T1658">люди.[NOM.SG]</ta>
            <ta e="T1660" id="Seg_803" s="T1659">NEG</ta>
            <ta e="T1661" id="Seg_804" s="T1660">дать-PST-3PL</ta>
            <ta e="T1662" id="Seg_805" s="T1661">тогда</ta>
            <ta e="T1663" id="Seg_806" s="T1662">этот.[NOM.SG]</ta>
            <ta e="T1664" id="Seg_807" s="T1663">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1665" id="Seg_808" s="T1664">я.NOM</ta>
            <ta e="T1666" id="Seg_809" s="T1665">когда=INDEF</ta>
            <ta e="T1667" id="Seg_810" s="T1666">NEG</ta>
            <ta e="T1668" id="Seg_811" s="T1667">украсть-PST-1SG</ta>
            <ta e="T1669" id="Seg_812" s="T1668">NEG</ta>
            <ta e="T1670" id="Seg_813" s="T1669">украсть-FUT-1SG</ta>
            <ta e="T1671" id="Seg_814" s="T1670">а</ta>
            <ta e="T1672" id="Seg_815" s="T1671">ты.NOM</ta>
            <ta e="T1673" id="Seg_816" s="T1672">я.ACC</ta>
            <ta e="T1675" id="Seg_817" s="T1674">убить-INF.LAT</ta>
            <ta e="T1676" id="Seg_818" s="T1675">хотеть.PST.M.SG</ta>
            <ta e="T1677" id="Seg_819" s="T1676">тогда</ta>
            <ta e="T1678" id="Seg_820" s="T1677">остаться-MOM-PST.[3SG]</ta>
            <ta e="T1679" id="Seg_821" s="T1678">этот.[NOM.SG]</ta>
            <ta e="T1680" id="Seg_822" s="T1679">мужчина.[NOM.SG]</ta>
            <ta e="T1681" id="Seg_823" s="T1680">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1602" id="Seg_824" s="T1601">num-n:case</ta>
            <ta e="T1603" id="Seg_825" s="T1602">n-n:case</ta>
            <ta e="T1604" id="Seg_826" s="T1603">v-v:tense-v:pn</ta>
            <ta e="T1605" id="Seg_827" s="T1604">dempro-n:case</ta>
            <ta e="T1606" id="Seg_828" s="T1605">que-n:case=ptcl</ta>
            <ta e="T1607" id="Seg_829" s="T1606">ptcl</ta>
            <ta e="T1608" id="Seg_830" s="T1607">v-v:tense-v:pn</ta>
            <ta e="T1609" id="Seg_831" s="T1608">v-v:tense-v:pn</ta>
            <ta e="T1610" id="Seg_832" s="T1609">dempro-n:case</ta>
            <ta e="T1611" id="Seg_833" s="T1610">n-n:case</ta>
            <ta e="T1612" id="Seg_834" s="T1611">dempro-n:case</ta>
            <ta e="T1613" id="Seg_835" s="T1612">v-v:n.fin</ta>
            <ta e="T1614" id="Seg_836" s="T1613">ptcl</ta>
            <ta e="T1615" id="Seg_837" s="T1614">v-v:n.fin</ta>
            <ta e="T1616" id="Seg_838" s="T1615">adv</ta>
            <ta e="T1617" id="Seg_839" s="T1616">dempro-n:case</ta>
            <ta e="T1618" id="Seg_840" s="T1617">v-v&gt;v-v:pn</ta>
            <ta e="T1619" id="Seg_841" s="T1618">pers</ta>
            <ta e="T1621" id="Seg_842" s="T1620">n-n:case</ta>
            <ta e="T1623" id="Seg_843" s="T1622">v-v:n.fin</ta>
            <ta e="T1624" id="Seg_844" s="T1623">v-v:n.fin</ta>
            <ta e="T1625" id="Seg_845" s="T1624">ptcl</ta>
            <ta e="T1626" id="Seg_846" s="T1625">n-n:case</ta>
            <ta e="T1627" id="Seg_847" s="T1626">v-v:n.fin</ta>
            <ta e="T1628" id="Seg_848" s="T1627">adv</ta>
            <ta e="T1629" id="Seg_849" s="T1628">v-v:tense-v:pn</ta>
            <ta e="T1630" id="Seg_850" s="T1629">n-n:case</ta>
            <ta e="T1631" id="Seg_851" s="T1630">v-v:tense-v:pn</ta>
            <ta e="T1632" id="Seg_852" s="T1631">dempro</ta>
            <ta e="T1633" id="Seg_853" s="T1632">v-v:tense-v&gt;adj</ta>
            <ta e="T1634" id="Seg_854" s="T1633">n-n:case</ta>
            <ta e="T1635" id="Seg_855" s="T1634">pers</ta>
            <ta e="T1636" id="Seg_856" s="T1635">adv</ta>
            <ta e="T1637" id="Seg_857" s="T1636">dempro-n:case</ta>
            <ta e="T1638" id="Seg_858" s="T1637">v-v:tense-v:pn</ta>
            <ta e="T1639" id="Seg_859" s="T1638">pers</ta>
            <ta e="T1640" id="Seg_860" s="T1639">v-v:tense-v:pn</ta>
            <ta e="T1641" id="Seg_861" s="T1640">v-v:tense-v:pn</ta>
            <ta e="T1642" id="Seg_862" s="T1641">n-n:case</ta>
            <ta e="T1643" id="Seg_863" s="T1642">dempro-n:case</ta>
            <ta e="T1644" id="Seg_864" s="T1643">v-v:tense-v:pn</ta>
            <ta e="T1645" id="Seg_865" s="T1644">n-n:case</ta>
            <ta e="T1646" id="Seg_866" s="T1645">adv</ta>
            <ta e="T1647" id="Seg_867" s="T1646">v-v:mood.pn</ta>
            <ta e="T1648" id="Seg_868" s="T1647">n-n:case</ta>
            <ta e="T1649" id="Seg_869" s="T1648">dempro-n:case</ta>
            <ta e="T1650" id="Seg_870" s="T1649">v-v:tense-v:pn</ta>
            <ta e="T1651" id="Seg_871" s="T1650">que-n:case=ptcl</ta>
            <ta e="T1652" id="Seg_872" s="T1651">n-n:case</ta>
            <ta e="T1653" id="Seg_873" s="T1652">ptcl</ta>
            <ta e="T1654" id="Seg_874" s="T1653">v-v:tense-v:pn</ta>
            <ta e="T1655" id="Seg_875" s="T1654">n</ta>
            <ta e="T1656" id="Seg_876" s="T1655">ptcl</ta>
            <ta e="T1657" id="Seg_877" s="T1656">v-v:tense-v:pn</ta>
            <ta e="T1658" id="Seg_878" s="T1657">quant</ta>
            <ta e="T1659" id="Seg_879" s="T1658">n-n:case</ta>
            <ta e="T1660" id="Seg_880" s="T1659">ptcl</ta>
            <ta e="T1661" id="Seg_881" s="T1660">v-v:tense-v:pn</ta>
            <ta e="T1662" id="Seg_882" s="T1661">adv</ta>
            <ta e="T1663" id="Seg_883" s="T1662">dempro-n:case</ta>
            <ta e="T1664" id="Seg_884" s="T1663">v-v&gt;v-v:pn</ta>
            <ta e="T1665" id="Seg_885" s="T1664">pers</ta>
            <ta e="T1666" id="Seg_886" s="T1665">que=ptcl</ta>
            <ta e="T1667" id="Seg_887" s="T1666">ptcl</ta>
            <ta e="T1668" id="Seg_888" s="T1667">v-v:tense-v:pn</ta>
            <ta e="T1669" id="Seg_889" s="T1668">ptcl</ta>
            <ta e="T1670" id="Seg_890" s="T1669">v-v:tense-v:pn</ta>
            <ta e="T1671" id="Seg_891" s="T1670">conj</ta>
            <ta e="T1672" id="Seg_892" s="T1671">pers</ta>
            <ta e="T1673" id="Seg_893" s="T1672">pers</ta>
            <ta e="T1675" id="Seg_894" s="T1674">v-v:n.fin</ta>
            <ta e="T1676" id="Seg_895" s="T1675">v</ta>
            <ta e="T1677" id="Seg_896" s="T1676">adv</ta>
            <ta e="T1678" id="Seg_897" s="T1677">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1679" id="Seg_898" s="T1678">dempro-n:case</ta>
            <ta e="T1680" id="Seg_899" s="T1679">n-n:case</ta>
            <ta e="T1681" id="Seg_900" s="T1680">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1602" id="Seg_901" s="T1601">num</ta>
            <ta e="T1603" id="Seg_902" s="T1602">n</ta>
            <ta e="T1604" id="Seg_903" s="T1603">v</ta>
            <ta e="T1605" id="Seg_904" s="T1604">dempro</ta>
            <ta e="T1606" id="Seg_905" s="T1605">que</ta>
            <ta e="T1607" id="Seg_906" s="T1606">ptcl</ta>
            <ta e="T1608" id="Seg_907" s="T1607">v</ta>
            <ta e="T1609" id="Seg_908" s="T1608">v</ta>
            <ta e="T1610" id="Seg_909" s="T1609">dempro</ta>
            <ta e="T1611" id="Seg_910" s="T1610">n</ta>
            <ta e="T1612" id="Seg_911" s="T1611">dempro</ta>
            <ta e="T1613" id="Seg_912" s="T1612">v</ta>
            <ta e="T1614" id="Seg_913" s="T1613">ptcl</ta>
            <ta e="T1615" id="Seg_914" s="T1614">v</ta>
            <ta e="T1616" id="Seg_915" s="T1615">adv</ta>
            <ta e="T1617" id="Seg_916" s="T1616">dempro</ta>
            <ta e="T1618" id="Seg_917" s="T1617">v</ta>
            <ta e="T1619" id="Seg_918" s="T1618">pers</ta>
            <ta e="T1621" id="Seg_919" s="T1620">n</ta>
            <ta e="T1623" id="Seg_920" s="T1622">v</ta>
            <ta e="T1624" id="Seg_921" s="T1623">v</ta>
            <ta e="T1625" id="Seg_922" s="T1624">ptcl</ta>
            <ta e="T1626" id="Seg_923" s="T1625">n</ta>
            <ta e="T1627" id="Seg_924" s="T1626">v</ta>
            <ta e="T1628" id="Seg_925" s="T1627">adv</ta>
            <ta e="T1629" id="Seg_926" s="T1628">v</ta>
            <ta e="T1630" id="Seg_927" s="T1629">n</ta>
            <ta e="T1631" id="Seg_928" s="T1630">v</ta>
            <ta e="T1632" id="Seg_929" s="T1631">dempro</ta>
            <ta e="T1633" id="Seg_930" s="T1632">adj</ta>
            <ta e="T1634" id="Seg_931" s="T1633">n</ta>
            <ta e="T1635" id="Seg_932" s="T1634">pers</ta>
            <ta e="T1636" id="Seg_933" s="T1635">adv</ta>
            <ta e="T1637" id="Seg_934" s="T1636">dempro</ta>
            <ta e="T1638" id="Seg_935" s="T1637">v</ta>
            <ta e="T1639" id="Seg_936" s="T1638">pers</ta>
            <ta e="T1640" id="Seg_937" s="T1639">v</ta>
            <ta e="T1641" id="Seg_938" s="T1640">v</ta>
            <ta e="T1642" id="Seg_939" s="T1641">n</ta>
            <ta e="T1643" id="Seg_940" s="T1642">dempro</ta>
            <ta e="T1644" id="Seg_941" s="T1643">v</ta>
            <ta e="T1645" id="Seg_942" s="T1644">n</ta>
            <ta e="T1646" id="Seg_943" s="T1645">adv</ta>
            <ta e="T1647" id="Seg_944" s="T1646">v</ta>
            <ta e="T1648" id="Seg_945" s="T1647">n</ta>
            <ta e="T1649" id="Seg_946" s="T1648">dempro</ta>
            <ta e="T1650" id="Seg_947" s="T1649">v</ta>
            <ta e="T1651" id="Seg_948" s="T1650">que</ta>
            <ta e="T1652" id="Seg_949" s="T1651">n</ta>
            <ta e="T1653" id="Seg_950" s="T1652">ptcl</ta>
            <ta e="T1654" id="Seg_951" s="T1653">v</ta>
            <ta e="T1655" id="Seg_952" s="T1654">n</ta>
            <ta e="T1656" id="Seg_953" s="T1655">ptcl</ta>
            <ta e="T1657" id="Seg_954" s="T1656">v</ta>
            <ta e="T1658" id="Seg_955" s="T1657">quant</ta>
            <ta e="T1659" id="Seg_956" s="T1658">n</ta>
            <ta e="T1660" id="Seg_957" s="T1659">ptcl</ta>
            <ta e="T1661" id="Seg_958" s="T1660">v</ta>
            <ta e="T1662" id="Seg_959" s="T1661">adv</ta>
            <ta e="T1663" id="Seg_960" s="T1662">dempro</ta>
            <ta e="T1664" id="Seg_961" s="T1663">v</ta>
            <ta e="T1665" id="Seg_962" s="T1664">pers</ta>
            <ta e="T1666" id="Seg_963" s="T1665">que</ta>
            <ta e="T1667" id="Seg_964" s="T1666">ptcl</ta>
            <ta e="T1668" id="Seg_965" s="T1667">v</ta>
            <ta e="T1669" id="Seg_966" s="T1668">ptcl</ta>
            <ta e="T1670" id="Seg_967" s="T1669">v</ta>
            <ta e="T1671" id="Seg_968" s="T1670">conj</ta>
            <ta e="T1672" id="Seg_969" s="T1671">pers</ta>
            <ta e="T1673" id="Seg_970" s="T1672">pers</ta>
            <ta e="T1675" id="Seg_971" s="T1674">v</ta>
            <ta e="T1676" id="Seg_972" s="T1675">v</ta>
            <ta e="T1677" id="Seg_973" s="T1676">adv</ta>
            <ta e="T1678" id="Seg_974" s="T1677">v</ta>
            <ta e="T1679" id="Seg_975" s="T1678">dempro</ta>
            <ta e="T1680" id="Seg_976" s="T1679">n</ta>
            <ta e="T1681" id="Seg_977" s="T1680">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1603" id="Seg_978" s="T1602">np.h:Th</ta>
            <ta e="T1604" id="Seg_979" s="T1603">0.3.h:A</ta>
            <ta e="T1605" id="Seg_980" s="T1604">pro.h:A</ta>
            <ta e="T1606" id="Seg_981" s="T1605">pro:Th</ta>
            <ta e="T1609" id="Seg_982" s="T1608">0.3.h:A</ta>
            <ta e="T1610" id="Seg_983" s="T1609">pro.h:Th</ta>
            <ta e="T1611" id="Seg_984" s="T1610">np:G</ta>
            <ta e="T1612" id="Seg_985" s="T1611">pro.h:P</ta>
            <ta e="T1616" id="Seg_986" s="T1615">adv:Time</ta>
            <ta e="T1617" id="Seg_987" s="T1616">pro.h:A</ta>
            <ta e="T1619" id="Seg_988" s="T1618">pro.h:A</ta>
            <ta e="T1621" id="Seg_989" s="T1620">np:Th</ta>
            <ta e="T1626" id="Seg_990" s="T1625">np:Th</ta>
            <ta e="T1628" id="Seg_991" s="T1627">adv:Time</ta>
            <ta e="T1629" id="Seg_992" s="T1628">0.3.h:A</ta>
            <ta e="T1630" id="Seg_993" s="T1629">np.h:R</ta>
            <ta e="T1631" id="Seg_994" s="T1630">0.3.h:A</ta>
            <ta e="T1634" id="Seg_995" s="T1631">np:Th</ta>
            <ta e="T1635" id="Seg_996" s="T1634">pro.h:B</ta>
            <ta e="T1636" id="Seg_997" s="T1635">adv:Time</ta>
            <ta e="T1637" id="Seg_998" s="T1636">pro.h:A</ta>
            <ta e="T1640" id="Seg_999" s="T1639">0.1.h:A</ta>
            <ta e="T1641" id="Seg_1000" s="T1640">0.2.h:A</ta>
            <ta e="T1642" id="Seg_1001" s="T1641">np:Th</ta>
            <ta e="T1643" id="Seg_1002" s="T1642">pro.h:A</ta>
            <ta e="T1645" id="Seg_1003" s="T1644">np:Th</ta>
            <ta e="T1646" id="Seg_1004" s="T1645">adv:Time</ta>
            <ta e="T1647" id="Seg_1005" s="T1646">0.2.h:A</ta>
            <ta e="T1648" id="Seg_1006" s="T1647">np:Th</ta>
            <ta e="T1649" id="Seg_1007" s="T1648">pro.h:A</ta>
            <ta e="T1652" id="Seg_1008" s="T1651">np.h:A</ta>
            <ta e="T1655" id="Seg_1009" s="T1654">np.h:A</ta>
            <ta e="T1659" id="Seg_1010" s="T1658">np.h:A</ta>
            <ta e="T1662" id="Seg_1011" s="T1661">adv:Time</ta>
            <ta e="T1663" id="Seg_1012" s="T1662">pro.h:A</ta>
            <ta e="T1665" id="Seg_1013" s="T1664">pro.h:A</ta>
            <ta e="T1666" id="Seg_1014" s="T1665">pro:Th</ta>
            <ta e="T1670" id="Seg_1015" s="T1669">0.1.h:A</ta>
            <ta e="T1672" id="Seg_1016" s="T1671">pro.h:A</ta>
            <ta e="T1673" id="Seg_1017" s="T1672">pro.h:P</ta>
            <ta e="T1677" id="Seg_1018" s="T1676">adv:Time</ta>
            <ta e="T1680" id="Seg_1019" s="T1679">np.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1603" id="Seg_1020" s="T1602">np.h:O</ta>
            <ta e="T1604" id="Seg_1021" s="T1603">v:pred 0.3.h:S</ta>
            <ta e="T1605" id="Seg_1022" s="T1604">pro.h:S</ta>
            <ta e="T1606" id="Seg_1023" s="T1605">pro:O</ta>
            <ta e="T1607" id="Seg_1024" s="T1606">ptcl.neg</ta>
            <ta e="T1608" id="Seg_1025" s="T1607">v:pred</ta>
            <ta e="T1609" id="Seg_1026" s="T1608">v:pred 0.3.h:S</ta>
            <ta e="T1610" id="Seg_1027" s="T1609">pro.h:O</ta>
            <ta e="T1612" id="Seg_1028" s="T1611">pro.h:O</ta>
            <ta e="T1614" id="Seg_1029" s="T1613">ptcl:pred</ta>
            <ta e="T1615" id="Seg_1030" s="T1614">v:pred</ta>
            <ta e="T1617" id="Seg_1031" s="T1616">pro.h:S</ta>
            <ta e="T1618" id="Seg_1032" s="T1617">v:pred</ta>
            <ta e="T1619" id="Seg_1033" s="T1618">pro.h:S</ta>
            <ta e="T1621" id="Seg_1034" s="T1620">np:O</ta>
            <ta e="T1624" id="Seg_1035" s="T1623">v:pred</ta>
            <ta e="T1625" id="Seg_1036" s="T1624">ptcl:pred</ta>
            <ta e="T1626" id="Seg_1037" s="T1625">np:O</ta>
            <ta e="T1627" id="Seg_1038" s="T1626">v:pred</ta>
            <ta e="T1629" id="Seg_1039" s="T1628">v:pred 0.3.h:S</ta>
            <ta e="T1631" id="Seg_1040" s="T1630">v:pred 0.3.h:S</ta>
            <ta e="T1634" id="Seg_1041" s="T1631">np:O</ta>
            <ta e="T1637" id="Seg_1042" s="T1636">pro.h:S</ta>
            <ta e="T1638" id="Seg_1043" s="T1637">v:pred</ta>
            <ta e="T1640" id="Seg_1044" s="T1639">v:pred 0.1.h:S</ta>
            <ta e="T1641" id="Seg_1045" s="T1640">v:pred 0.2.h:S</ta>
            <ta e="T1642" id="Seg_1046" s="T1641">np:O</ta>
            <ta e="T1643" id="Seg_1047" s="T1642">pro.h:S</ta>
            <ta e="T1644" id="Seg_1048" s="T1643">v:pred</ta>
            <ta e="T1645" id="Seg_1049" s="T1644">np:O</ta>
            <ta e="T1647" id="Seg_1050" s="T1646">v:pred 0.2.h:S</ta>
            <ta e="T1648" id="Seg_1051" s="T1647">np:O</ta>
            <ta e="T1649" id="Seg_1052" s="T1648">pro.h:S</ta>
            <ta e="T1650" id="Seg_1053" s="T1649">v:pred</ta>
            <ta e="T1652" id="Seg_1054" s="T1651">np.h:S</ta>
            <ta e="T1653" id="Seg_1055" s="T1652">ptcl.neg</ta>
            <ta e="T1654" id="Seg_1056" s="T1653">v:pred</ta>
            <ta e="T1655" id="Seg_1057" s="T1654">np.h:S</ta>
            <ta e="T1656" id="Seg_1058" s="T1655">ptcl.neg</ta>
            <ta e="T1657" id="Seg_1059" s="T1656">v:pred</ta>
            <ta e="T1659" id="Seg_1060" s="T1658">np.h:S</ta>
            <ta e="T1660" id="Seg_1061" s="T1659">ptcl.neg</ta>
            <ta e="T1661" id="Seg_1062" s="T1660">v:pred</ta>
            <ta e="T1663" id="Seg_1063" s="T1662">pro.h:S</ta>
            <ta e="T1664" id="Seg_1064" s="T1663">v:pred</ta>
            <ta e="T1665" id="Seg_1065" s="T1664">pro.h:S</ta>
            <ta e="T1666" id="Seg_1066" s="T1665">pro:O</ta>
            <ta e="T1667" id="Seg_1067" s="T1666">ptcl.neg</ta>
            <ta e="T1668" id="Seg_1068" s="T1667">v:pred</ta>
            <ta e="T1669" id="Seg_1069" s="T1668">ptcl.neg</ta>
            <ta e="T1670" id="Seg_1070" s="T1669">v:pred 0.1.h:S</ta>
            <ta e="T1672" id="Seg_1071" s="T1671">pro.h:S</ta>
            <ta e="T1673" id="Seg_1072" s="T1672">pro.h:O</ta>
            <ta e="T1676" id="Seg_1073" s="T1675">v:pred</ta>
            <ta e="T1678" id="Seg_1074" s="T1677">v:pred</ta>
            <ta e="T1680" id="Seg_1075" s="T1679">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1606" id="Seg_1076" s="T1605">TURK:gram(INDEF)</ta>
            <ta e="T1611" id="Seg_1077" s="T1610">TURK:cult</ta>
            <ta e="T1614" id="Seg_1078" s="T1613">RUS:mod</ta>
            <ta e="T1621" id="Seg_1079" s="T1620">RUS:cult</ta>
            <ta e="T1625" id="Seg_1080" s="T1624">RUS:mod</ta>
            <ta e="T1630" id="Seg_1081" s="T1629">TURK:cult</ta>
            <ta e="T1634" id="Seg_1082" s="T1633">RUS:cult</ta>
            <ta e="T1642" id="Seg_1083" s="T1641">RUS:cult</ta>
            <ta e="T1648" id="Seg_1084" s="T1647">RUS:cult</ta>
            <ta e="T1651" id="Seg_1085" s="T1650">TURK:gram(INDEF)</ta>
            <ta e="T1652" id="Seg_1086" s="T1651">TURK:cult</ta>
            <ta e="T1655" id="Seg_1087" s="T1654">RUS:cult</ta>
            <ta e="T1666" id="Seg_1088" s="T1665">TURK:gram(INDEF)</ta>
            <ta e="T1671" id="Seg_1089" s="T1670">RUS:gram</ta>
            <ta e="T1676" id="Seg_1090" s="T1675">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1604" id="Seg_1091" s="T1601">Одного человека поймали.</ta>
            <ta e="T1608" id="Seg_1092" s="T1604">Он ничего не украл.</ta>
            <ta e="T1611" id="Seg_1093" s="T1608">Его привели к царю.</ta>
            <ta e="T1614" id="Seg_1094" s="T1611">"Надо его убить.</ta>
            <ta e="T1615" id="Seg_1095" s="T1614">Повесить".</ta>
            <ta e="T1618" id="Seg_1096" s="T1615">Потом он говорит: </ta>
            <ta e="T1624" id="Seg_1097" s="T1618">"Я могу золото сеять.</ta>
            <ta e="T1627" id="Seg_1098" s="T1624">Надо землю вспахать".</ta>
            <ta e="T1631" id="Seg_1099" s="T1627">Потом [люди] пришли и сказали царю:</ta>
            <ta e="T1635" id="Seg_1100" s="T1631">"Он для тебя золото посеет".</ta>
            <ta e="T1642" id="Seg_1101" s="T1635">Потом он приходит: "Ты, говорю я, будешь золото сеять". </ta>
            <ta e="T1645" id="Seg_1102" s="T1642">Он вспахал землю.</ta>
            <ta e="T1648" id="Seg_1103" s="T1645">Потом [он говорит]: "Принесите золото!"</ta>
            <ta e="T1650" id="Seg_1104" s="T1648">Он принёс.</ta>
            <ta e="T1654" id="Seg_1105" s="T1650">Никто [не дал золота], священник не дал.</ta>
            <ta e="T1657" id="Seg_1106" s="T1654">Судья не дал.</ta>
            <ta e="T1661" id="Seg_1107" s="T1657">Многие люди не дали.</ta>
            <ta e="T1668" id="Seg_1108" s="T1661">Потом он сказал: "Я никогда не крал.</ta>
            <ta e="T1670" id="Seg_1109" s="T1668">Я не буду красть.</ta>
            <ta e="T1676" id="Seg_1110" s="T1670">А ты меня убить хотел".</ta>
            <ta e="T1680" id="Seg_1111" s="T1676">Потом этот человек остался [в живых].</ta>
            <ta e="T1681" id="Seg_1112" s="T1680">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1604" id="Seg_1113" s="T1601">One man was caught.</ta>
            <ta e="T1608" id="Seg_1114" s="T1604">He did not steal anything.</ta>
            <ta e="T1611" id="Seg_1115" s="T1608">They brought him to the chief.</ta>
            <ta e="T1614" id="Seg_1116" s="T1611">"[We] should kill him.</ta>
            <ta e="T1615" id="Seg_1117" s="T1614">Hang [him]."</ta>
            <ta e="T1618" id="Seg_1118" s="T1615">Then he says:</ta>
            <ta e="T1624" id="Seg_1119" s="T1618">"I can sow gold.</ta>
            <ta e="T1627" id="Seg_1120" s="T1624">[One] must plough the land."</ta>
            <ta e="T1631" id="Seg_1121" s="T1627">Then they came and told to the chief:</ta>
            <ta e="T1635" id="Seg_1122" s="T1631">"He will sow gold for you."</ta>
            <ta e="T1642" id="Seg_1123" s="T1635">Then he came: "You, I said [you] will sow gold."</ta>
            <ta e="T1645" id="Seg_1124" s="T1642">He ploughed ground.</ta>
            <ta e="T1648" id="Seg_1125" s="T1645">Then [he says]: "Bring the gold!"</ta>
            <ta e="T1650" id="Seg_1126" s="T1648">He brought.</ta>
            <ta e="T1654" id="Seg_1127" s="T1650">Nobody [gave the gold], priest did not give.</ta>
            <ta e="T1657" id="Seg_1128" s="T1654">The judge did not give.</ta>
            <ta e="T1661" id="Seg_1129" s="T1657">Many people did not give.</ta>
            <ta e="T1668" id="Seg_1130" s="T1661">Then he said: "I never stole.</ta>
            <ta e="T1670" id="Seg_1131" s="T1668">I will not steal.</ta>
            <ta e="T1676" id="Seg_1132" s="T1670">But you wanted to kill me.</ta>
            <ta e="T1680" id="Seg_1133" s="T1676">Then this man stayed [alive].</ta>
            <ta e="T1681" id="Seg_1134" s="T1680">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1604" id="Seg_1135" s="T1601">Ein Mann wurde erwischt.</ta>
            <ta e="T1608" id="Seg_1136" s="T1604">Er hatte nichts gestohlen.</ta>
            <ta e="T1611" id="Seg_1137" s="T1608">Sie brachte ihn zum Anführer.</ta>
            <ta e="T1614" id="Seg_1138" s="T1611">"[Wir] sollten ihn töten.</ta>
            <ta e="T1615" id="Seg_1139" s="T1614">Aufhängen."</ta>
            <ta e="T1618" id="Seg_1140" s="T1615">Dann sagt er:</ta>
            <ta e="T1624" id="Seg_1141" s="T1618">"Ich kann Gold sähen.</ta>
            <ta e="T1627" id="Seg_1142" s="T1624">[Man] muss des Land pflügen."</ta>
            <ta e="T1631" id="Seg_1143" s="T1627">Dann kamen sie und erzählten dem Anführer:</ta>
            <ta e="T1635" id="Seg_1144" s="T1631">„Er wird dir Gold sähen.“</ta>
            <ta e="T1642" id="Seg_1145" s="T1635">Dann kam er: „Du, ich sagte [du] werdest Gold sähen.“</ta>
            <ta e="T1645" id="Seg_1146" s="T1642">Er pflügte Boden.</ta>
            <ta e="T1648" id="Seg_1147" s="T1645">Dann [sagt er]: „Bring das Gold!“</ta>
            <ta e="T1650" id="Seg_1148" s="T1648">Er brachte.</ta>
            <ta e="T1654" id="Seg_1149" s="T1650">Niemand [gab das Gold], Priester gab nicht.</ta>
            <ta e="T1657" id="Seg_1150" s="T1654">Der Richter gab nicht.</ta>
            <ta e="T1661" id="Seg_1151" s="T1657">Viele Leute gaben nicht.</ta>
            <ta e="T1668" id="Seg_1152" s="T1661">Dann sagte er: "Ich habe niemals gestohlen.</ta>
            <ta e="T1670" id="Seg_1153" s="T1668">Ich werde nicht stehlen.</ta>
            <ta e="T1676" id="Seg_1154" s="T1670">Aber ihr wollten mich töten.</ta>
            <ta e="T1680" id="Seg_1155" s="T1676">Dann blieb dieser Mann [am Leben].</ta>
            <ta e="T1681" id="Seg_1156" s="T1680">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1604" id="Seg_1157" s="T1601">[GVY:] Lit. "[they] caught one man" = Russian "Одного человека поймали".</ta>
            <ta e="T1615" id="Seg_1158" s="T1614">[KlT:] edə- ’to hang’.</ta>
            <ta e="T1627" id="Seg_1159" s="T1624">[GVY:] Donner tajer- (67a), but Plotnikova always pronounces this verb as tarir-.</ta>
            <ta e="T1642" id="Seg_1160" s="T1635">[GVY:] Either the prisoner was brought to the chief or the chief came to him.</ta>
            <ta e="T1645" id="Seg_1161" s="T1642">[KlT:] possible contamination producing tarer- instead of tajir-. </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1601" />
            <conversion-tli id="T1602" />
            <conversion-tli id="T1603" />
            <conversion-tli id="T1604" />
            <conversion-tli id="T1605" />
            <conversion-tli id="T1606" />
            <conversion-tli id="T1607" />
            <conversion-tli id="T1608" />
            <conversion-tli id="T1609" />
            <conversion-tli id="T1610" />
            <conversion-tli id="T1611" />
            <conversion-tli id="T1612" />
            <conversion-tli id="T1613" />
            <conversion-tli id="T1614" />
            <conversion-tli id="T1615" />
            <conversion-tli id="T1616" />
            <conversion-tli id="T1617" />
            <conversion-tli id="T1618" />
            <conversion-tli id="T1619" />
            <conversion-tli id="T1620" />
            <conversion-tli id="T1621" />
            <conversion-tli id="T1622" />
            <conversion-tli id="T1623" />
            <conversion-tli id="T1624" />
            <conversion-tli id="T1625" />
            <conversion-tli id="T1626" />
            <conversion-tli id="T1627" />
            <conversion-tli id="T1628" />
            <conversion-tli id="T1629" />
            <conversion-tli id="T1630" />
            <conversion-tli id="T1631" />
            <conversion-tli id="T1632" />
            <conversion-tli id="T1633" />
            <conversion-tli id="T1634" />
            <conversion-tli id="T1635" />
            <conversion-tli id="T1636" />
            <conversion-tli id="T1637" />
            <conversion-tli id="T1638" />
            <conversion-tli id="T1639" />
            <conversion-tli id="T1640" />
            <conversion-tli id="T1641" />
            <conversion-tli id="T1642" />
            <conversion-tli id="T1643" />
            <conversion-tli id="T1644" />
            <conversion-tli id="T1645" />
            <conversion-tli id="T1646" />
            <conversion-tli id="T1647" />
            <conversion-tli id="T1648" />
            <conversion-tli id="T1649" />
            <conversion-tli id="T1650" />
            <conversion-tli id="T1651" />
            <conversion-tli id="T1652" />
            <conversion-tli id="T1653" />
            <conversion-tli id="T1654" />
            <conversion-tli id="T1655" />
            <conversion-tli id="T1656" />
            <conversion-tli id="T1657" />
            <conversion-tli id="T1658" />
            <conversion-tli id="T1659" />
            <conversion-tli id="T1660" />
            <conversion-tli id="T1661" />
            <conversion-tli id="T1662" />
            <conversion-tli id="T1663" />
            <conversion-tli id="T1664" />
            <conversion-tli id="T1665" />
            <conversion-tli id="T1666" />
            <conversion-tli id="T1667" />
            <conversion-tli id="T1668" />
            <conversion-tli id="T1669" />
            <conversion-tli id="T1670" />
            <conversion-tli id="T1671" />
            <conversion-tli id="T1672" />
            <conversion-tli id="T1673" />
            <conversion-tli id="T1674" />
            <conversion-tli id="T1675" />
            <conversion-tli id="T1676" />
            <conversion-tli id="T1677" />
            <conversion-tli id="T1678" />
            <conversion-tli id="T1679" />
            <conversion-tli id="T1680" />
            <conversion-tli id="T1681" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
