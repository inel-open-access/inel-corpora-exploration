<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA28178E9-0603-30B7-82C1-722CD9BE1BA7">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_StupidMan_flk.wav" />
         <referenced-file url="PKZ_196X_StupidMan_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_StupidMan_flk\PKZ_196X_StupidMan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">130</ud-information>
            <ud-information attribute-name="# HIAT:w">89</ud-information>
            <ud-information attribute-name="# e">89</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T48" time="0.0" type="appl" />
         <tli id="T49" time="0.718" type="appl" />
         <tli id="T50" time="1.437" type="appl" />
         <tli id="T51" time="2.155" type="appl" />
         <tli id="T52" time="2.786" type="appl" />
         <tli id="T53" time="3.416" type="appl" />
         <tli id="T54" time="4.046" type="appl" />
         <tli id="T55" time="4.677" type="appl" />
         <tli id="T56" time="5.391" type="appl" />
         <tli id="T57" time="5.97" type="appl" />
         <tli id="T58" time="6.55" type="appl" />
         <tli id="T59" time="7.13" type="appl" />
         <tli id="T60" time="8.044" type="appl" />
         <tli id="T61" time="8.583" type="appl" />
         <tli id="T62" time="9.123" type="appl" />
         <tli id="T63" time="9.662" type="appl" />
         <tli id="T64" time="10.202" type="appl" />
         <tli id="T65" time="10.741" type="appl" />
         <tli id="T66" time="11.281" type="appl" />
         <tli id="T67" time="12.221" type="appl" />
         <tli id="T68" time="13.052" type="appl" />
         <tli id="T69" time="13.882" type="appl" />
         <tli id="T70" time="14.713" type="appl" />
         <tli id="T71" time="15.295" type="appl" />
         <tli id="T72" time="15.763" type="appl" />
         <tli id="T73" time="16.23" type="appl" />
         <tli id="T74" time="16.698" type="appl" />
         <tli id="T75" time="17.165" type="appl" />
         <tli id="T76" time="17.867" type="appl" />
         <tli id="T77" time="18.541" type="appl" />
         <tli id="T78" time="19.215" type="appl" />
         <tli id="T79" time="19.889" type="appl" />
         <tli id="T80" time="20.563" type="appl" />
         <tli id="T81" time="21.237" type="appl" />
         <tli id="T82" time="21.911" type="appl" />
         <tli id="T83" time="22.585" type="appl" />
         <tli id="T84" time="23.259" type="appl" />
         <tli id="T85" time="23.933" type="appl" />
         <tli id="T86" time="24.607" type="appl" />
         <tli id="T87" time="25.949" type="appl" />
         <tli id="T88" time="27.493" type="appl" />
         <tli id="T89" time="28.266" type="appl" />
         <tli id="T90" time="29.04" type="appl" />
         <tli id="T91" time="29.813" type="appl" />
         <tli id="T92" time="30.477" type="appl" />
         <tli id="T93" time="31.142" type="appl" />
         <tli id="T94" time="31.806" type="appl" />
         <tli id="T95" time="32.47" type="appl" />
         <tli id="T96" time="35.018" type="appl" />
         <tli id="T97" time="35.734" type="appl" />
         <tli id="T98" time="36.408" type="appl" />
         <tli id="T99" time="37.083" type="appl" />
         <tli id="T100" time="37.757" type="appl" />
         <tli id="T101" time="38.432" type="appl" />
         <tli id="T102" time="39.106" type="appl" />
         <tli id="T103" time="39.78" type="appl" />
         <tli id="T104" time="40.455" type="appl" />
         <tli id="T105" time="41.129" type="appl" />
         <tli id="T106" time="41.569" type="appl" />
         <tli id="T107" time="42.01" type="appl" />
         <tli id="T108" time="42.45" type="appl" />
         <tli id="T109" time="42.97" type="appl" />
         <tli id="T110" time="43.49" type="appl" />
         <tli id="T111" time="44.01" type="appl" />
         <tli id="T112" time="44.529" type="appl" />
         <tli id="T113" time="45.049" type="appl" />
         <tli id="T114" time="45.569" type="appl" />
         <tli id="T115" time="46.628" type="appl" />
         <tli id="T116" time="47.688" type="appl" />
         <tli id="T117" time="48.747" type="appl" />
         <tli id="T118" time="49.3" type="appl" />
         <tli id="T119" time="49.716" type="appl" />
         <tli id="T120" time="50.133" type="appl" />
         <tli id="T121" time="51.026" type="appl" />
         <tli id="T122" time="51.629" type="appl" />
         <tli id="T123" time="52.232" type="appl" />
         <tli id="T124" time="52.835" type="appl" />
         <tli id="T125" time="53.438" type="appl" />
         <tli id="T126" time="54.234" type="appl" />
         <tli id="T127" time="54.757" type="appl" />
         <tli id="T128" time="55.281" type="appl" />
         <tli id="T129" time="56.827" type="appl" />
         <tli id="T130" time="57.397" type="appl" />
         <tli id="T131" time="57.968" type="appl" />
         <tli id="T132" time="58.538" type="appl" />
         <tli id="T133" time="59.109" type="appl" />
         <tli id="T134" time="59.68" type="appl" />
         <tli id="T135" time="60.25" type="appl" />
         <tli id="T136" time="60.821" type="appl" />
         <tli id="T137" time="62.06" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T137" id="Seg_0" n="sc" s="T48">
               <ts e="T51" id="Seg_2" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_4" n="HIAT:w" s="T48">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_7" n="HIAT:w" s="T49">onʼiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_10" n="HIAT:w" s="T50">büzʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_14" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_16" n="HIAT:w" s="T51">Dĭn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_19" n="HIAT:w" s="T52">sagəštə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_22" n="HIAT:w" s="T53">iʔgö</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_25" n="HIAT:w" s="T54">ibi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_29" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_31" n="HIAT:w" s="T55">Il</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_34" n="HIAT:w" s="T56">dĭ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_37" n="HIAT:w" s="T57">bar</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_40" n="HIAT:w" s="T58">kabažərbi</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_44" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_46" n="HIAT:w" s="T59">Ĭmbi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_49" n="HIAT:w" s="T60">mălləj</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_52" n="HIAT:w" s="T61">dak</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_55" n="HIAT:w" s="T62">uge</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_58" n="HIAT:w" s="T63">jakšə</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_60" n="HIAT:ip">(</nts>
                  <ts e="T65" id="Seg_62" n="HIAT:w" s="T64">mo-</ts>
                  <nts id="Seg_63" n="HIAT:ip">)</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_66" n="HIAT:w" s="T65">mogaʔ</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_70" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_72" n="HIAT:w" s="T66">Dĭgəttə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_75" n="HIAT:w" s="T67">dĭn</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_78" n="HIAT:w" s="T68">nʼit</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_81" n="HIAT:w" s="T69">ibi</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_85" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_87" n="HIAT:w" s="T70">Dĭ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_90" n="HIAT:w" s="T71">dĭm</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_93" n="HIAT:w" s="T72">ugaːndə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_96" n="HIAT:w" s="T73">tăŋ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_99" n="HIAT:w" s="T74">ajirbi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_103" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_105" n="HIAT:w" s="T75">Il</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_109" n="HIAT:w" s="T76">mĭŋgə-</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_113" n="HIAT:w" s="T77">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_116" n="HIAT:w" s="T78">da</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_119" n="HIAT:w" s="T79">măliaʔi</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_122" n="HIAT:w" s="T80">a</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_125" n="HIAT:w" s="T81">tăn</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_128" n="HIAT:w" s="T82">nʼil</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_131" n="HIAT:w" s="T83">ej</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_134" n="HIAT:w" s="T84">jakšə</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_137" n="HIAT:w" s="T85">alia</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_141" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_143" n="HIAT:w" s="T86">Kudonzlia</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_147" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_149" n="HIAT:w" s="T87">Dĭgəttə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_152" n="HIAT:w" s="T88">onʼiʔ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_155" n="HIAT:w" s="T89">nüke</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_158" n="HIAT:w" s="T90">šobi:</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">"</nts>
                  <ts e="T92" id="Seg_162" n="HIAT:w" s="T91">Tăn</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_165" n="HIAT:w" s="T92">nʼil</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_168" n="HIAT:w" s="T93">măna</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_171" n="HIAT:w" s="T94">kudolbi</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_175" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_177" n="HIAT:w" s="T95">Kudolaʔ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_180" n="HIAT:w" s="T96">dĭm</ts>
                  <nts id="Seg_181" n="HIAT:ip">"</nts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_185" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_187" n="HIAT:w" s="T97">Dĭgəttə</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_190" n="HIAT:w" s="T98">dĭ</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_193" n="HIAT:w" s="T99">edəʔpi</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_197" n="HIAT:w" s="T100">edəʔpi</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_201" n="HIAT:w" s="T101">nudʼin</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_204" n="HIAT:w" s="T102">šobi</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_207" n="HIAT:w" s="T103">dĭn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_210" n="HIAT:w" s="T104">nʼit</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_214" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_216" n="HIAT:w" s="T105">Davaj</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_219" n="HIAT:w" s="T106">dĭ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_222" n="HIAT:w" s="T107">dĭʔnə:</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_224" n="HIAT:ip">"</nts>
                  <ts e="T109" id="Seg_226" n="HIAT:w" s="T108">Tăn</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_229" n="HIAT:w" s="T109">iʔ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_232" n="HIAT:w" s="T110">kirgaraʔ</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_235" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_237" n="HIAT:w" s="T111">ikš-</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_240" n="HIAT:w" s="T112">i-</ts>
                  <nts id="Seg_241" n="HIAT:ip">,</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_244" n="HIAT:w" s="T113">ir-</ts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_249" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_251" n="HIAT:w" s="T114">Ilzʼiʔ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_254" n="HIAT:w" s="T115">jakšə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_257" n="HIAT:w" s="T116">dʼăbaktəraʔ</ts>
                  <nts id="Seg_258" n="HIAT:ip">!</nts>
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_262" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_264" n="HIAT:w" s="T117">A</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_267" n="HIAT:w" s="T118">dĭ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_270" n="HIAT:w" s="T119">măndə:</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_272" n="HIAT:ip">"</nts>
                  <ts e="T121" id="Seg_274" n="HIAT:w" s="T120">Kirgarzittə</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_277" n="HIAT:w" s="T121">dăk</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_280" n="HIAT:w" s="T122">tolʼko</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_283" n="HIAT:w" s="T123">jakšə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_286" n="HIAT:w" s="T124">moləj</ts>
                  <nts id="Seg_287" n="HIAT:ip">"</nts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_291" n="HIAT:u" s="T125">
                  <nts id="Seg_292" n="HIAT:ip">"</nts>
                  <ts e="T126" id="Seg_294" n="HIAT:w" s="T125">Dʼok</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_298" n="HIAT:w" s="T126">ej</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_301" n="HIAT:w" s="T127">jakšə</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_305" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_307" n="HIAT:w" s="T128">Ine</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_310" n="HIAT:w" s="T129">ej</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_313" n="HIAT:w" s="T130">kirgaria</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_316" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_318" n="HIAT:w" s="T131">kuləl</ts>
                  <nts id="Seg_319" n="HIAT:ip">)</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_322" n="HIAT:w" s="T132">kădə</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_325" n="HIAT:w" s="T133">jakšə</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_327" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_329" n="HIAT:w" s="T134">i-</ts>
                  <nts id="Seg_330" n="HIAT:ip">)</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_333" n="HIAT:w" s="T135">molaʔbə</ts>
                  <nts id="Seg_334" n="HIAT:ip">"</nts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_338" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_340" n="HIAT:w" s="T136">Kabarləj</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T137" id="Seg_343" n="sc" s="T48">
               <ts e="T49" id="Seg_345" n="e" s="T48">Amnobi </ts>
               <ts e="T50" id="Seg_347" n="e" s="T49">onʼiʔ </ts>
               <ts e="T51" id="Seg_349" n="e" s="T50">büzʼe. </ts>
               <ts e="T52" id="Seg_351" n="e" s="T51">Dĭn </ts>
               <ts e="T53" id="Seg_353" n="e" s="T52">sagəštə </ts>
               <ts e="T54" id="Seg_355" n="e" s="T53">iʔgö </ts>
               <ts e="T55" id="Seg_357" n="e" s="T54">ibi. </ts>
               <ts e="T56" id="Seg_359" n="e" s="T55">Il </ts>
               <ts e="T57" id="Seg_361" n="e" s="T56">dĭ </ts>
               <ts e="T58" id="Seg_363" n="e" s="T57">bar </ts>
               <ts e="T59" id="Seg_365" n="e" s="T58">kabažərbi. </ts>
               <ts e="T60" id="Seg_367" n="e" s="T59">Ĭmbi </ts>
               <ts e="T61" id="Seg_369" n="e" s="T60">mălləj </ts>
               <ts e="T62" id="Seg_371" n="e" s="T61">dak </ts>
               <ts e="T63" id="Seg_373" n="e" s="T62">uge </ts>
               <ts e="T64" id="Seg_375" n="e" s="T63">jakšə </ts>
               <ts e="T65" id="Seg_377" n="e" s="T64">(mo-) </ts>
               <ts e="T66" id="Seg_379" n="e" s="T65">mogaʔ. </ts>
               <ts e="T67" id="Seg_381" n="e" s="T66">Dĭgəttə </ts>
               <ts e="T68" id="Seg_383" n="e" s="T67">dĭn </ts>
               <ts e="T69" id="Seg_385" n="e" s="T68">nʼit </ts>
               <ts e="T70" id="Seg_387" n="e" s="T69">ibi. </ts>
               <ts e="T71" id="Seg_389" n="e" s="T70">Dĭ </ts>
               <ts e="T72" id="Seg_391" n="e" s="T71">dĭm </ts>
               <ts e="T73" id="Seg_393" n="e" s="T72">ugaːndə </ts>
               <ts e="T74" id="Seg_395" n="e" s="T73">tăŋ </ts>
               <ts e="T75" id="Seg_397" n="e" s="T74">ajirbi. </ts>
               <ts e="T76" id="Seg_399" n="e" s="T75">Il </ts>
               <ts e="T77" id="Seg_401" n="e" s="T76">(mĭŋgə-) </ts>
               <ts e="T78" id="Seg_403" n="e" s="T77">mĭlleʔbəʔjə </ts>
               <ts e="T79" id="Seg_405" n="e" s="T78">da </ts>
               <ts e="T80" id="Seg_407" n="e" s="T79">măliaʔi </ts>
               <ts e="T81" id="Seg_409" n="e" s="T80">a </ts>
               <ts e="T82" id="Seg_411" n="e" s="T81">tăn </ts>
               <ts e="T83" id="Seg_413" n="e" s="T82">nʼil </ts>
               <ts e="T84" id="Seg_415" n="e" s="T83">ej </ts>
               <ts e="T85" id="Seg_417" n="e" s="T84">jakšə </ts>
               <ts e="T86" id="Seg_419" n="e" s="T85">alia. </ts>
               <ts e="T87" id="Seg_421" n="e" s="T86">Kudonzlia. </ts>
               <ts e="T88" id="Seg_423" n="e" s="T87">Dĭgəttə </ts>
               <ts e="T89" id="Seg_425" n="e" s="T88">onʼiʔ </ts>
               <ts e="T90" id="Seg_427" n="e" s="T89">nüke </ts>
               <ts e="T91" id="Seg_429" n="e" s="T90">šobi: </ts>
               <ts e="T92" id="Seg_431" n="e" s="T91">"Tăn </ts>
               <ts e="T93" id="Seg_433" n="e" s="T92">nʼil </ts>
               <ts e="T94" id="Seg_435" n="e" s="T93">măna </ts>
               <ts e="T95" id="Seg_437" n="e" s="T94">kudolbi. </ts>
               <ts e="T96" id="Seg_439" n="e" s="T95">Kudolaʔ </ts>
               <ts e="T97" id="Seg_441" n="e" s="T96">dĭm". </ts>
               <ts e="T98" id="Seg_443" n="e" s="T97">Dĭgəttə </ts>
               <ts e="T99" id="Seg_445" n="e" s="T98">dĭ </ts>
               <ts e="T100" id="Seg_447" n="e" s="T99">edəʔpi, </ts>
               <ts e="T101" id="Seg_449" n="e" s="T100">edəʔpi, </ts>
               <ts e="T102" id="Seg_451" n="e" s="T101">nudʼin </ts>
               <ts e="T103" id="Seg_453" n="e" s="T102">šobi </ts>
               <ts e="T104" id="Seg_455" n="e" s="T103">dĭn </ts>
               <ts e="T105" id="Seg_457" n="e" s="T104">nʼit. </ts>
               <ts e="T106" id="Seg_459" n="e" s="T105">Davaj </ts>
               <ts e="T107" id="Seg_461" n="e" s="T106">dĭ </ts>
               <ts e="T108" id="Seg_463" n="e" s="T107">dĭʔnə: </ts>
               <ts e="T109" id="Seg_465" n="e" s="T108">"Tăn </ts>
               <ts e="T110" id="Seg_467" n="e" s="T109">iʔ </ts>
               <ts e="T111" id="Seg_469" n="e" s="T110">kirgaraʔ, </ts>
               <ts e="T112" id="Seg_471" n="e" s="T111">(ikš- </ts>
               <ts e="T113" id="Seg_473" n="e" s="T112">i-, </ts>
               <ts e="T114" id="Seg_475" n="e" s="T113">ir-). </ts>
               <ts e="T115" id="Seg_477" n="e" s="T114">Ilzʼiʔ </ts>
               <ts e="T116" id="Seg_479" n="e" s="T115">jakšə </ts>
               <ts e="T117" id="Seg_481" n="e" s="T116">dʼăbaktəraʔ!" </ts>
               <ts e="T118" id="Seg_483" n="e" s="T117">A </ts>
               <ts e="T119" id="Seg_485" n="e" s="T118">dĭ </ts>
               <ts e="T120" id="Seg_487" n="e" s="T119">măndə: </ts>
               <ts e="T121" id="Seg_489" n="e" s="T120">"Kirgarzittə </ts>
               <ts e="T122" id="Seg_491" n="e" s="T121">dăk </ts>
               <ts e="T123" id="Seg_493" n="e" s="T122">tolʼko </ts>
               <ts e="T124" id="Seg_495" n="e" s="T123">jakšə </ts>
               <ts e="T125" id="Seg_497" n="e" s="T124">moləj". </ts>
               <ts e="T126" id="Seg_499" n="e" s="T125">"Dʼok, </ts>
               <ts e="T127" id="Seg_501" n="e" s="T126">ej </ts>
               <ts e="T128" id="Seg_503" n="e" s="T127">jakšə. </ts>
               <ts e="T129" id="Seg_505" n="e" s="T128">Ine </ts>
               <ts e="T130" id="Seg_507" n="e" s="T129">ej </ts>
               <ts e="T131" id="Seg_509" n="e" s="T130">kirgaria, </ts>
               <ts e="T132" id="Seg_511" n="e" s="T131">(kuləl) </ts>
               <ts e="T133" id="Seg_513" n="e" s="T132">kădə </ts>
               <ts e="T134" id="Seg_515" n="e" s="T133">jakšə </ts>
               <ts e="T135" id="Seg_517" n="e" s="T134">(i-) </ts>
               <ts e="T136" id="Seg_519" n="e" s="T135">molaʔbə". </ts>
               <ts e="T137" id="Seg_521" n="e" s="T136">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T51" id="Seg_522" s="T48">PKZ_196X_StupidMan_flk.001 (001)</ta>
            <ta e="T55" id="Seg_523" s="T51">PKZ_196X_StupidMan_flk.002 (002)</ta>
            <ta e="T59" id="Seg_524" s="T55">PKZ_196X_StupidMan_flk.003 (003)</ta>
            <ta e="T66" id="Seg_525" s="T59">PKZ_196X_StupidMan_flk.004 (004)</ta>
            <ta e="T70" id="Seg_526" s="T66">PKZ_196X_StupidMan_flk.005 (005)</ta>
            <ta e="T75" id="Seg_527" s="T70">PKZ_196X_StupidMan_flk.006 (006)</ta>
            <ta e="T86" id="Seg_528" s="T75">PKZ_196X_StupidMan_flk.007 (007)</ta>
            <ta e="T87" id="Seg_529" s="T86">PKZ_196X_StupidMan_flk.008 (008)</ta>
            <ta e="T95" id="Seg_530" s="T87">PKZ_196X_StupidMan_flk.009 (009) </ta>
            <ta e="T97" id="Seg_531" s="T95">PKZ_196X_StupidMan_flk.010 (011)</ta>
            <ta e="T105" id="Seg_532" s="T97">PKZ_196X_StupidMan_flk.011 (012)</ta>
            <ta e="T114" id="Seg_533" s="T105">PKZ_196X_StupidMan_flk.012 (013) </ta>
            <ta e="T117" id="Seg_534" s="T114">PKZ_196X_StupidMan_flk.013 (015)</ta>
            <ta e="T125" id="Seg_535" s="T117">PKZ_196X_StupidMan_flk.014 (016) </ta>
            <ta e="T128" id="Seg_536" s="T125">PKZ_196X_StupidMan_flk.015 (018)</ta>
            <ta e="T136" id="Seg_537" s="T128">PKZ_196X_StupidMan_flk.016 (019)</ta>
            <ta e="T137" id="Seg_538" s="T136">PKZ_196X_StupidMan_flk.017 (020)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T51" id="Seg_539" s="T48">Amnobi onʼiʔ büzʼe. </ta>
            <ta e="T55" id="Seg_540" s="T51">Dĭn sagəštə iʔgö ibi. </ta>
            <ta e="T59" id="Seg_541" s="T55">Il dĭ bar kabažərbi. </ta>
            <ta e="T66" id="Seg_542" s="T59">Ĭmbi mălləj dak uge jakšə (mo-) mogaʔ. </ta>
            <ta e="T70" id="Seg_543" s="T66">Dĭgəttə dĭn nʼit ibi. </ta>
            <ta e="T75" id="Seg_544" s="T70">Dĭ dĭm ugaːndə tăŋ ajirbi. </ta>
            <ta e="T86" id="Seg_545" s="T75">Il (mĭŋgə-) mĭlleʔbəʔjə da măliaʔi a tăn nʼil ej jakšə alia. </ta>
            <ta e="T87" id="Seg_546" s="T86">Kudonzlia. </ta>
            <ta e="T95" id="Seg_547" s="T87">Dĭgəttə onʼiʔ nüke šobi: "Tăn nʼil măna kudolbi. </ta>
            <ta e="T97" id="Seg_548" s="T95">Kudolaʔ dĭm". </ta>
            <ta e="T105" id="Seg_549" s="T97">Dĭgəttə dĭ edəʔpi, edəʔpi, nudʼin šobi dĭn nʼit. </ta>
            <ta e="T114" id="Seg_550" s="T105">Davaj dĭ dĭʔnə: "Tăn iʔ kirgaraʔ, (ikš- i-, ir-). </ta>
            <ta e="T117" id="Seg_551" s="T114">Ilzʼiʔ jakšə dʼăbaktəraʔ!" </ta>
            <ta e="T125" id="Seg_552" s="T117">A dĭ măndə: "Kirgarzittə dăk tolʼko jakšə moləj". </ta>
            <ta e="T128" id="Seg_553" s="T125">"Dʼok, ej jakšə. </ta>
            <ta e="T136" id="Seg_554" s="T128">Ine ej kirgaria, (kuləl) kădə jakšə (i-) molaʔbə". </ta>
            <ta e="T137" id="Seg_555" s="T136">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T49" id="Seg_556" s="T48">amno-bi</ta>
            <ta e="T50" id="Seg_557" s="T49">onʼiʔ</ta>
            <ta e="T51" id="Seg_558" s="T50">büzʼe</ta>
            <ta e="T52" id="Seg_559" s="T51">dĭ-n</ta>
            <ta e="T53" id="Seg_560" s="T52">sagəš-tə</ta>
            <ta e="T54" id="Seg_561" s="T53">iʔgö</ta>
            <ta e="T55" id="Seg_562" s="T54">i-bi</ta>
            <ta e="T56" id="Seg_563" s="T55">il</ta>
            <ta e="T57" id="Seg_564" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_565" s="T57">bar</ta>
            <ta e="T59" id="Seg_566" s="T58">kabažər-bi</ta>
            <ta e="T60" id="Seg_567" s="T59">ĭmbi</ta>
            <ta e="T61" id="Seg_568" s="T60">măl-lə-j</ta>
            <ta e="T62" id="Seg_569" s="T61">dak</ta>
            <ta e="T63" id="Seg_570" s="T62">uge</ta>
            <ta e="T64" id="Seg_571" s="T63">jakšə</ta>
            <ta e="T66" id="Seg_572" s="T65">mo-gaʔ</ta>
            <ta e="T67" id="Seg_573" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_574" s="T67">dĭ-n</ta>
            <ta e="T69" id="Seg_575" s="T68">nʼi-t</ta>
            <ta e="T70" id="Seg_576" s="T69">i-bi</ta>
            <ta e="T71" id="Seg_577" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_578" s="T71">dĭ-m</ta>
            <ta e="T73" id="Seg_579" s="T72">ugaːndə</ta>
            <ta e="T74" id="Seg_580" s="T73">tăŋ</ta>
            <ta e="T75" id="Seg_581" s="T74">ajir-bi</ta>
            <ta e="T76" id="Seg_582" s="T75">il</ta>
            <ta e="T78" id="Seg_583" s="T77">mĭl-leʔbə-ʔjə</ta>
            <ta e="T79" id="Seg_584" s="T78">da</ta>
            <ta e="T80" id="Seg_585" s="T79">mă-lia-ʔi</ta>
            <ta e="T81" id="Seg_586" s="T80">a</ta>
            <ta e="T82" id="Seg_587" s="T81">tăn</ta>
            <ta e="T83" id="Seg_588" s="T82">nʼi-l</ta>
            <ta e="T84" id="Seg_589" s="T83">ej</ta>
            <ta e="T85" id="Seg_590" s="T84">jakšə</ta>
            <ta e="T86" id="Seg_591" s="T85">a-lia</ta>
            <ta e="T87" id="Seg_592" s="T86">kudo-nz-lia</ta>
            <ta e="T88" id="Seg_593" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_594" s="T88">onʼiʔ</ta>
            <ta e="T90" id="Seg_595" s="T89">nüke</ta>
            <ta e="T91" id="Seg_596" s="T90">šo-bi</ta>
            <ta e="T92" id="Seg_597" s="T91">tăn</ta>
            <ta e="T93" id="Seg_598" s="T92">nʼi-l</ta>
            <ta e="T94" id="Seg_599" s="T93">măna</ta>
            <ta e="T95" id="Seg_600" s="T94">kudo-l-bi</ta>
            <ta e="T96" id="Seg_601" s="T95">kudo-l-aʔ</ta>
            <ta e="T97" id="Seg_602" s="T96">dĭ-m</ta>
            <ta e="T98" id="Seg_603" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_604" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_605" s="T99">edəʔ-pi</ta>
            <ta e="T101" id="Seg_606" s="T100">edəʔ-pi</ta>
            <ta e="T102" id="Seg_607" s="T101">nudʼi-n</ta>
            <ta e="T103" id="Seg_608" s="T102">šo-bi</ta>
            <ta e="T104" id="Seg_609" s="T103">dĭ-n</ta>
            <ta e="T105" id="Seg_610" s="T104">nʼi-t</ta>
            <ta e="T106" id="Seg_611" s="T105">davaj</ta>
            <ta e="T107" id="Seg_612" s="T106">dĭ</ta>
            <ta e="T108" id="Seg_613" s="T107">dĭʔ-nə</ta>
            <ta e="T109" id="Seg_614" s="T108">tăn</ta>
            <ta e="T110" id="Seg_615" s="T109">i-ʔ</ta>
            <ta e="T111" id="Seg_616" s="T110">kirgar-a-ʔ</ta>
            <ta e="T115" id="Seg_617" s="T114">il-zʼiʔ</ta>
            <ta e="T116" id="Seg_618" s="T115">jakšə</ta>
            <ta e="T117" id="Seg_619" s="T116">dʼăbaktər-a-ʔ</ta>
            <ta e="T118" id="Seg_620" s="T117">a</ta>
            <ta e="T119" id="Seg_621" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_622" s="T119">măn-də</ta>
            <ta e="T121" id="Seg_623" s="T120">kirgar-zittə</ta>
            <ta e="T122" id="Seg_624" s="T121">dăk</ta>
            <ta e="T123" id="Seg_625" s="T122">tolʼko</ta>
            <ta e="T124" id="Seg_626" s="T123">jakšə</ta>
            <ta e="T125" id="Seg_627" s="T124">mo-lə-j</ta>
            <ta e="T126" id="Seg_628" s="T125">dʼok</ta>
            <ta e="T127" id="Seg_629" s="T126">ej</ta>
            <ta e="T128" id="Seg_630" s="T127">jakšə</ta>
            <ta e="T129" id="Seg_631" s="T128">ine</ta>
            <ta e="T130" id="Seg_632" s="T129">ej</ta>
            <ta e="T131" id="Seg_633" s="T130">kirgar-ia</ta>
            <ta e="T132" id="Seg_634" s="T131">ku-lə-l</ta>
            <ta e="T133" id="Seg_635" s="T132">kădə</ta>
            <ta e="T134" id="Seg_636" s="T133">jakšə</ta>
            <ta e="T136" id="Seg_637" s="T135">mo-laʔbə</ta>
            <ta e="T137" id="Seg_638" s="T136">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T49" id="Seg_639" s="T48">amno-bi</ta>
            <ta e="T50" id="Seg_640" s="T49">onʼiʔ</ta>
            <ta e="T51" id="Seg_641" s="T50">büzʼe</ta>
            <ta e="T52" id="Seg_642" s="T51">dĭ-n</ta>
            <ta e="T53" id="Seg_643" s="T52">sagəš-də</ta>
            <ta e="T54" id="Seg_644" s="T53">iʔgö</ta>
            <ta e="T55" id="Seg_645" s="T54">i-bi</ta>
            <ta e="T56" id="Seg_646" s="T55">il</ta>
            <ta e="T57" id="Seg_647" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_648" s="T57">bar</ta>
            <ta e="T59" id="Seg_649" s="T58">kabažar-bi</ta>
            <ta e="T60" id="Seg_650" s="T59">ĭmbi</ta>
            <ta e="T61" id="Seg_651" s="T60">măn-lV-j</ta>
            <ta e="T62" id="Seg_652" s="T61">tak</ta>
            <ta e="T63" id="Seg_653" s="T62">üge</ta>
            <ta e="T64" id="Seg_654" s="T63">jakšə</ta>
            <ta e="T66" id="Seg_655" s="T65">mo-KAʔ</ta>
            <ta e="T67" id="Seg_656" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_657" s="T67">dĭ-n</ta>
            <ta e="T69" id="Seg_658" s="T68">nʼi-t</ta>
            <ta e="T70" id="Seg_659" s="T69">i-bi</ta>
            <ta e="T71" id="Seg_660" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_661" s="T71">dĭ-m</ta>
            <ta e="T73" id="Seg_662" s="T72">ugaːndə</ta>
            <ta e="T74" id="Seg_663" s="T73">tăŋ</ta>
            <ta e="T75" id="Seg_664" s="T74">ajir-bi</ta>
            <ta e="T76" id="Seg_665" s="T75">il</ta>
            <ta e="T78" id="Seg_666" s="T77">mĭn-laʔbə-jəʔ</ta>
            <ta e="T79" id="Seg_667" s="T78">da</ta>
            <ta e="T80" id="Seg_668" s="T79">măn-liA-jəʔ</ta>
            <ta e="T81" id="Seg_669" s="T80">a</ta>
            <ta e="T82" id="Seg_670" s="T81">tăn</ta>
            <ta e="T83" id="Seg_671" s="T82">nʼi-l</ta>
            <ta e="T84" id="Seg_672" s="T83">ej</ta>
            <ta e="T85" id="Seg_673" s="T84">jakšə</ta>
            <ta e="T86" id="Seg_674" s="T85">a-liA</ta>
            <ta e="T87" id="Seg_675" s="T86">kudo-nzə-liA</ta>
            <ta e="T88" id="Seg_676" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_677" s="T88">onʼiʔ</ta>
            <ta e="T90" id="Seg_678" s="T89">nüke</ta>
            <ta e="T91" id="Seg_679" s="T90">šo-bi</ta>
            <ta e="T92" id="Seg_680" s="T91">tăn</ta>
            <ta e="T93" id="Seg_681" s="T92">nʼi-l</ta>
            <ta e="T94" id="Seg_682" s="T93">măna</ta>
            <ta e="T95" id="Seg_683" s="T94">kudo-l-bi</ta>
            <ta e="T96" id="Seg_684" s="T95">kudo-lə-ʔ</ta>
            <ta e="T97" id="Seg_685" s="T96">dĭ-m</ta>
            <ta e="T98" id="Seg_686" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_687" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_688" s="T99">edəʔ-bi</ta>
            <ta e="T101" id="Seg_689" s="T100">edəʔ-bi</ta>
            <ta e="T102" id="Seg_690" s="T101">nüdʼi-n</ta>
            <ta e="T103" id="Seg_691" s="T102">šo-bi</ta>
            <ta e="T104" id="Seg_692" s="T103">dĭ-n</ta>
            <ta e="T105" id="Seg_693" s="T104">nʼi-t</ta>
            <ta e="T106" id="Seg_694" s="T105">davaj</ta>
            <ta e="T107" id="Seg_695" s="T106">dĭ</ta>
            <ta e="T108" id="Seg_696" s="T107">dĭ-Tə</ta>
            <ta e="T109" id="Seg_697" s="T108">tăn</ta>
            <ta e="T110" id="Seg_698" s="T109">e-ʔ</ta>
            <ta e="T111" id="Seg_699" s="T110">kirgaːr-ə-ʔ</ta>
            <ta e="T115" id="Seg_700" s="T114">il-ziʔ</ta>
            <ta e="T116" id="Seg_701" s="T115">jakšə</ta>
            <ta e="T117" id="Seg_702" s="T116">tʼăbaktər-ə-ʔ</ta>
            <ta e="T118" id="Seg_703" s="T117">a</ta>
            <ta e="T119" id="Seg_704" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_705" s="T119">măn-ntə</ta>
            <ta e="T121" id="Seg_706" s="T120">kirgaːr-zittə</ta>
            <ta e="T122" id="Seg_707" s="T121">tak</ta>
            <ta e="T123" id="Seg_708" s="T122">tolʼko</ta>
            <ta e="T124" id="Seg_709" s="T123">jakšə</ta>
            <ta e="T125" id="Seg_710" s="T124">mo-lV-j</ta>
            <ta e="T126" id="Seg_711" s="T125">dʼok</ta>
            <ta e="T127" id="Seg_712" s="T126">ej</ta>
            <ta e="T128" id="Seg_713" s="T127">jakšə</ta>
            <ta e="T129" id="Seg_714" s="T128">ine</ta>
            <ta e="T130" id="Seg_715" s="T129">ej</ta>
            <ta e="T131" id="Seg_716" s="T130">kirgaːr-liA</ta>
            <ta e="T132" id="Seg_717" s="T131">ku-lV-l</ta>
            <ta e="T133" id="Seg_718" s="T132">kădaʔ</ta>
            <ta e="T134" id="Seg_719" s="T133">jakšə</ta>
            <ta e="T136" id="Seg_720" s="T135">mo-laʔbə</ta>
            <ta e="T137" id="Seg_721" s="T136">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T49" id="Seg_722" s="T48">live-PST.[3SG]</ta>
            <ta e="T50" id="Seg_723" s="T49">one.[NOM.SG]</ta>
            <ta e="T51" id="Seg_724" s="T50">man.[NOM.SG]</ta>
            <ta e="T52" id="Seg_725" s="T51">this-GEN</ta>
            <ta e="T53" id="Seg_726" s="T52">mind-NOM/GEN/ACC.3SG</ta>
            <ta e="T54" id="Seg_727" s="T53">many</ta>
            <ta e="T55" id="Seg_728" s="T54">be-PST.[3SG]</ta>
            <ta e="T56" id="Seg_729" s="T55">people.[NOM.SG]</ta>
            <ta e="T57" id="Seg_730" s="T56">this.[NOM.SG]</ta>
            <ta e="T58" id="Seg_731" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_732" s="T58">help-PST.[3SG]</ta>
            <ta e="T60" id="Seg_733" s="T59">what.[NOM.SG]</ta>
            <ta e="T61" id="Seg_734" s="T60">say-FUT-3SG</ta>
            <ta e="T62" id="Seg_735" s="T61">so</ta>
            <ta e="T63" id="Seg_736" s="T62">always</ta>
            <ta e="T64" id="Seg_737" s="T63">good.[NOM.SG]</ta>
            <ta e="T66" id="Seg_738" s="T65">become-IMP.2PL</ta>
            <ta e="T67" id="Seg_739" s="T66">then</ta>
            <ta e="T68" id="Seg_740" s="T67">this-GEN</ta>
            <ta e="T69" id="Seg_741" s="T68">son-NOM/GEN.3SG</ta>
            <ta e="T70" id="Seg_742" s="T69">be-PST.[3SG]</ta>
            <ta e="T71" id="Seg_743" s="T70">this.[NOM.SG]</ta>
            <ta e="T72" id="Seg_744" s="T71">this-ACC</ta>
            <ta e="T73" id="Seg_745" s="T72">very</ta>
            <ta e="T74" id="Seg_746" s="T73">strongly</ta>
            <ta e="T75" id="Seg_747" s="T74">pity-PST.[3SG]</ta>
            <ta e="T76" id="Seg_748" s="T75">people.[NOM.SG]</ta>
            <ta e="T78" id="Seg_749" s="T77">go-DUR-3PL</ta>
            <ta e="T79" id="Seg_750" s="T78">and</ta>
            <ta e="T80" id="Seg_751" s="T79">say-PRS-3PL</ta>
            <ta e="T81" id="Seg_752" s="T80">and</ta>
            <ta e="T82" id="Seg_753" s="T81">you.GEN</ta>
            <ta e="T83" id="Seg_754" s="T82">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T84" id="Seg_755" s="T83">NEG</ta>
            <ta e="T85" id="Seg_756" s="T84">good.[NOM.SG]</ta>
            <ta e="T86" id="Seg_757" s="T85">make-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_758" s="T86">scold-DES-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_759" s="T87">then</ta>
            <ta e="T89" id="Seg_760" s="T88">one.[NOM.SG]</ta>
            <ta e="T90" id="Seg_761" s="T89">woman.[NOM.SG]</ta>
            <ta e="T91" id="Seg_762" s="T90">come-PST.[3SG]</ta>
            <ta e="T92" id="Seg_763" s="T91">you.GEN</ta>
            <ta e="T93" id="Seg_764" s="T92">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T94" id="Seg_765" s="T93">I.ACC</ta>
            <ta e="T95" id="Seg_766" s="T94">scold-FRQ-PST.[3SG]</ta>
            <ta e="T96" id="Seg_767" s="T95">scold-TR-IMP.2SG</ta>
            <ta e="T97" id="Seg_768" s="T96">this-ACC</ta>
            <ta e="T98" id="Seg_769" s="T97">then</ta>
            <ta e="T99" id="Seg_770" s="T98">this.[NOM.SG]</ta>
            <ta e="T100" id="Seg_771" s="T99">wait-PST.[3SG]</ta>
            <ta e="T101" id="Seg_772" s="T100">wait-PST.[3SG]</ta>
            <ta e="T102" id="Seg_773" s="T101">evening-LOC.ADV</ta>
            <ta e="T103" id="Seg_774" s="T102">come-PST.[3SG]</ta>
            <ta e="T104" id="Seg_775" s="T103">this-GEN</ta>
            <ta e="T105" id="Seg_776" s="T104">son-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_777" s="T105">INCH</ta>
            <ta e="T107" id="Seg_778" s="T106">this.[NOM.SG]</ta>
            <ta e="T108" id="Seg_779" s="T107">this-LAT</ta>
            <ta e="T109" id="Seg_780" s="T108">you.NOM</ta>
            <ta e="T110" id="Seg_781" s="T109">NEG.AUX-IMP.2SG</ta>
            <ta e="T111" id="Seg_782" s="T110">shout-EP-CNG</ta>
            <ta e="T115" id="Seg_783" s="T114">people-INS</ta>
            <ta e="T116" id="Seg_784" s="T115">good.[NOM.SG]</ta>
            <ta e="T117" id="Seg_785" s="T116">speak-EP-IMP.2SG</ta>
            <ta e="T118" id="Seg_786" s="T117">and</ta>
            <ta e="T119" id="Seg_787" s="T118">this.[NOM.SG]</ta>
            <ta e="T120" id="Seg_788" s="T119">say-IPFVZ.[3SG]</ta>
            <ta e="T121" id="Seg_789" s="T120">shout-INF.LAT</ta>
            <ta e="T122" id="Seg_790" s="T121">so</ta>
            <ta e="T123" id="Seg_791" s="T122">only</ta>
            <ta e="T124" id="Seg_792" s="T123">good.[NOM.SG]</ta>
            <ta e="T125" id="Seg_793" s="T124">become-FUT-3SG</ta>
            <ta e="T126" id="Seg_794" s="T125">no</ta>
            <ta e="T127" id="Seg_795" s="T126">NEG</ta>
            <ta e="T128" id="Seg_796" s="T127">good.[NOM.SG]</ta>
            <ta e="T129" id="Seg_797" s="T128">horse.[NOM.SG]</ta>
            <ta e="T130" id="Seg_798" s="T129">NEG</ta>
            <ta e="T131" id="Seg_799" s="T130">shout-PRS.[3SG]</ta>
            <ta e="T132" id="Seg_800" s="T131">see-FUT-2SG</ta>
            <ta e="T133" id="Seg_801" s="T132">how</ta>
            <ta e="T134" id="Seg_802" s="T133">good.[NOM.SG]</ta>
            <ta e="T136" id="Seg_803" s="T135">become-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_804" s="T136">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T49" id="Seg_805" s="T48">жить-PST.[3SG]</ta>
            <ta e="T50" id="Seg_806" s="T49">один.[NOM.SG]</ta>
            <ta e="T51" id="Seg_807" s="T50">мужчина.[NOM.SG]</ta>
            <ta e="T52" id="Seg_808" s="T51">этот-GEN</ta>
            <ta e="T53" id="Seg_809" s="T52">ум-NOM/GEN/ACC.3SG</ta>
            <ta e="T54" id="Seg_810" s="T53">много</ta>
            <ta e="T55" id="Seg_811" s="T54">быть-PST.[3SG]</ta>
            <ta e="T56" id="Seg_812" s="T55">люди.[NOM.SG]</ta>
            <ta e="T57" id="Seg_813" s="T56">этот.[NOM.SG]</ta>
            <ta e="T58" id="Seg_814" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_815" s="T58">помогать-PST.[3SG]</ta>
            <ta e="T60" id="Seg_816" s="T59">что.[NOM.SG]</ta>
            <ta e="T61" id="Seg_817" s="T60">сказать-FUT-3SG</ta>
            <ta e="T62" id="Seg_818" s="T61">так</ta>
            <ta e="T63" id="Seg_819" s="T62">всегда</ta>
            <ta e="T64" id="Seg_820" s="T63">хороший.[NOM.SG]</ta>
            <ta e="T66" id="Seg_821" s="T65">стать-IMP.2PL</ta>
            <ta e="T67" id="Seg_822" s="T66">тогда</ta>
            <ta e="T68" id="Seg_823" s="T67">этот-GEN</ta>
            <ta e="T69" id="Seg_824" s="T68">сын-NOM/GEN.3SG</ta>
            <ta e="T70" id="Seg_825" s="T69">быть-PST.[3SG]</ta>
            <ta e="T71" id="Seg_826" s="T70">этот.[NOM.SG]</ta>
            <ta e="T72" id="Seg_827" s="T71">этот-ACC</ta>
            <ta e="T73" id="Seg_828" s="T72">очень</ta>
            <ta e="T74" id="Seg_829" s="T73">сильно</ta>
            <ta e="T75" id="Seg_830" s="T74">жалеть-PST.[3SG]</ta>
            <ta e="T76" id="Seg_831" s="T75">люди.[NOM.SG]</ta>
            <ta e="T78" id="Seg_832" s="T77">идти-DUR-3PL</ta>
            <ta e="T79" id="Seg_833" s="T78">и</ta>
            <ta e="T80" id="Seg_834" s="T79">сказать-PRS-3PL</ta>
            <ta e="T81" id="Seg_835" s="T80">а</ta>
            <ta e="T82" id="Seg_836" s="T81">ты.GEN</ta>
            <ta e="T83" id="Seg_837" s="T82">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T84" id="Seg_838" s="T83">NEG</ta>
            <ta e="T85" id="Seg_839" s="T84">хороший.[NOM.SG]</ta>
            <ta e="T86" id="Seg_840" s="T85">делать-PRS.[3SG]</ta>
            <ta e="T87" id="Seg_841" s="T86">ругать-DES-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_842" s="T87">тогда</ta>
            <ta e="T89" id="Seg_843" s="T88">один.[NOM.SG]</ta>
            <ta e="T90" id="Seg_844" s="T89">женщина.[NOM.SG]</ta>
            <ta e="T91" id="Seg_845" s="T90">прийти-PST.[3SG]</ta>
            <ta e="T92" id="Seg_846" s="T91">ты.GEN</ta>
            <ta e="T93" id="Seg_847" s="T92">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T94" id="Seg_848" s="T93">я.ACC</ta>
            <ta e="T95" id="Seg_849" s="T94">ругать-FRQ-PST.[3SG]</ta>
            <ta e="T96" id="Seg_850" s="T95">ругать-TR-IMP.2SG</ta>
            <ta e="T97" id="Seg_851" s="T96">этот-ACC</ta>
            <ta e="T98" id="Seg_852" s="T97">тогда</ta>
            <ta e="T99" id="Seg_853" s="T98">этот.[NOM.SG]</ta>
            <ta e="T100" id="Seg_854" s="T99">ждать-PST.[3SG]</ta>
            <ta e="T101" id="Seg_855" s="T100">ждать-PST.[3SG]</ta>
            <ta e="T102" id="Seg_856" s="T101">вечер-LOC.ADV</ta>
            <ta e="T103" id="Seg_857" s="T102">прийти-PST.[3SG]</ta>
            <ta e="T104" id="Seg_858" s="T103">этот-GEN</ta>
            <ta e="T105" id="Seg_859" s="T104">сын-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_860" s="T105">INCH</ta>
            <ta e="T107" id="Seg_861" s="T106">этот.[NOM.SG]</ta>
            <ta e="T108" id="Seg_862" s="T107">этот-LAT</ta>
            <ta e="T109" id="Seg_863" s="T108">ты.NOM</ta>
            <ta e="T110" id="Seg_864" s="T109">NEG.AUX-IMP.2SG</ta>
            <ta e="T111" id="Seg_865" s="T110">кричать-EP-CNG</ta>
            <ta e="T115" id="Seg_866" s="T114">люди-INS</ta>
            <ta e="T116" id="Seg_867" s="T115">хороший.[NOM.SG]</ta>
            <ta e="T117" id="Seg_868" s="T116">говорить-EP-IMP.2SG</ta>
            <ta e="T118" id="Seg_869" s="T117">а</ta>
            <ta e="T119" id="Seg_870" s="T118">этот.[NOM.SG]</ta>
            <ta e="T120" id="Seg_871" s="T119">сказать-IPFVZ.[3SG]</ta>
            <ta e="T121" id="Seg_872" s="T120">кричать-INF.LAT</ta>
            <ta e="T122" id="Seg_873" s="T121">так</ta>
            <ta e="T123" id="Seg_874" s="T122">только</ta>
            <ta e="T124" id="Seg_875" s="T123">хороший.[NOM.SG]</ta>
            <ta e="T125" id="Seg_876" s="T124">стать-FUT-3SG</ta>
            <ta e="T126" id="Seg_877" s="T125">нет</ta>
            <ta e="T127" id="Seg_878" s="T126">NEG</ta>
            <ta e="T128" id="Seg_879" s="T127">хороший.[NOM.SG]</ta>
            <ta e="T129" id="Seg_880" s="T128">лошадь.[NOM.SG]</ta>
            <ta e="T130" id="Seg_881" s="T129">NEG</ta>
            <ta e="T131" id="Seg_882" s="T130">кричать-PRS.[3SG]</ta>
            <ta e="T132" id="Seg_883" s="T131">видеть-FUT-2SG</ta>
            <ta e="T133" id="Seg_884" s="T132">как</ta>
            <ta e="T134" id="Seg_885" s="T133">хороший.[NOM.SG]</ta>
            <ta e="T136" id="Seg_886" s="T135">стать-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_887" s="T136">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T49" id="Seg_888" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_889" s="T49">num-n:case</ta>
            <ta e="T51" id="Seg_890" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_891" s="T51">dempro-n:case</ta>
            <ta e="T53" id="Seg_892" s="T52">n-n:case.poss</ta>
            <ta e="T54" id="Seg_893" s="T53">quant</ta>
            <ta e="T55" id="Seg_894" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_895" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_896" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_897" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_898" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_899" s="T59">que-n:case</ta>
            <ta e="T61" id="Seg_900" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_901" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_902" s="T62">adv</ta>
            <ta e="T64" id="Seg_903" s="T63">adj-n:case</ta>
            <ta e="T66" id="Seg_904" s="T65">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_905" s="T66">adv</ta>
            <ta e="T68" id="Seg_906" s="T67">dempro-n:case</ta>
            <ta e="T69" id="Seg_907" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_908" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_909" s="T70">dempro-n:case</ta>
            <ta e="T72" id="Seg_910" s="T71">dempro-n:case</ta>
            <ta e="T73" id="Seg_911" s="T72">adv</ta>
            <ta e="T74" id="Seg_912" s="T73">adv</ta>
            <ta e="T75" id="Seg_913" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_914" s="T75">n-n:case</ta>
            <ta e="T78" id="Seg_915" s="T77">v-v&gt;v-v:pn</ta>
            <ta e="T79" id="Seg_916" s="T78">conj</ta>
            <ta e="T80" id="Seg_917" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_918" s="T80">conj</ta>
            <ta e="T82" id="Seg_919" s="T81">pers</ta>
            <ta e="T83" id="Seg_920" s="T82">n-n:case.poss</ta>
            <ta e="T84" id="Seg_921" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_922" s="T84">adj-n:case</ta>
            <ta e="T86" id="Seg_923" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_924" s="T86">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_925" s="T87">adv</ta>
            <ta e="T89" id="Seg_926" s="T88">num-n:case</ta>
            <ta e="T90" id="Seg_927" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_928" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_929" s="T91">pers</ta>
            <ta e="T93" id="Seg_930" s="T92">n-n:case.poss</ta>
            <ta e="T94" id="Seg_931" s="T93">pers</ta>
            <ta e="T95" id="Seg_932" s="T94">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_933" s="T95">v-v&gt;v-v:mood.pn</ta>
            <ta e="T97" id="Seg_934" s="T96">dempro-n:case</ta>
            <ta e="T98" id="Seg_935" s="T97">adv</ta>
            <ta e="T99" id="Seg_936" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_937" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_938" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_939" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_940" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_941" s="T103">dempro-n:case</ta>
            <ta e="T105" id="Seg_942" s="T104">n-n:case.poss</ta>
            <ta e="T106" id="Seg_943" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_944" s="T106">dempro-n:case</ta>
            <ta e="T108" id="Seg_945" s="T107">dempro-n:case</ta>
            <ta e="T109" id="Seg_946" s="T108">pers</ta>
            <ta e="T110" id="Seg_947" s="T109">aux-v:mood.pn</ta>
            <ta e="T111" id="Seg_948" s="T110">v-v:ins-v:n.fin</ta>
            <ta e="T115" id="Seg_949" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_950" s="T115">adj-n:case</ta>
            <ta e="T117" id="Seg_951" s="T116">v-v:ins-v:mood.pn</ta>
            <ta e="T118" id="Seg_952" s="T117">conj</ta>
            <ta e="T119" id="Seg_953" s="T118">dempro-n:case</ta>
            <ta e="T120" id="Seg_954" s="T119">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_955" s="T120">v-v:n.fin</ta>
            <ta e="T122" id="Seg_956" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_957" s="T122">adv</ta>
            <ta e="T124" id="Seg_958" s="T123">adj-n:case</ta>
            <ta e="T125" id="Seg_959" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_960" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_961" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_962" s="T127">adj-n:case</ta>
            <ta e="T129" id="Seg_963" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_964" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_965" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_966" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_967" s="T132">que</ta>
            <ta e="T134" id="Seg_968" s="T133">adj-n:case</ta>
            <ta e="T136" id="Seg_969" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_970" s="T136">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T49" id="Seg_971" s="T48">v</ta>
            <ta e="T50" id="Seg_972" s="T49">num</ta>
            <ta e="T51" id="Seg_973" s="T50">n</ta>
            <ta e="T52" id="Seg_974" s="T51">dempro</ta>
            <ta e="T53" id="Seg_975" s="T52">n</ta>
            <ta e="T54" id="Seg_976" s="T53">quant</ta>
            <ta e="T55" id="Seg_977" s="T54">v</ta>
            <ta e="T56" id="Seg_978" s="T55">n</ta>
            <ta e="T57" id="Seg_979" s="T56">dempro</ta>
            <ta e="T58" id="Seg_980" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_981" s="T58">v</ta>
            <ta e="T60" id="Seg_982" s="T59">que</ta>
            <ta e="T61" id="Seg_983" s="T60">v</ta>
            <ta e="T62" id="Seg_984" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_985" s="T62">adv</ta>
            <ta e="T64" id="Seg_986" s="T63">adj</ta>
            <ta e="T66" id="Seg_987" s="T65">v</ta>
            <ta e="T67" id="Seg_988" s="T66">adv</ta>
            <ta e="T68" id="Seg_989" s="T67">dempro</ta>
            <ta e="T69" id="Seg_990" s="T68">n</ta>
            <ta e="T70" id="Seg_991" s="T69">v</ta>
            <ta e="T71" id="Seg_992" s="T70">dempro</ta>
            <ta e="T72" id="Seg_993" s="T71">dempro</ta>
            <ta e="T73" id="Seg_994" s="T72">adv</ta>
            <ta e="T74" id="Seg_995" s="T73">adv</ta>
            <ta e="T75" id="Seg_996" s="T74">v</ta>
            <ta e="T76" id="Seg_997" s="T75">n</ta>
            <ta e="T78" id="Seg_998" s="T77">v</ta>
            <ta e="T79" id="Seg_999" s="T78">conj</ta>
            <ta e="T80" id="Seg_1000" s="T79">v</ta>
            <ta e="T81" id="Seg_1001" s="T80">conj</ta>
            <ta e="T82" id="Seg_1002" s="T81">pers</ta>
            <ta e="T83" id="Seg_1003" s="T82">n</ta>
            <ta e="T84" id="Seg_1004" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_1005" s="T84">adj</ta>
            <ta e="T86" id="Seg_1006" s="T85">v</ta>
            <ta e="T87" id="Seg_1007" s="T86">v</ta>
            <ta e="T88" id="Seg_1008" s="T87">adv</ta>
            <ta e="T89" id="Seg_1009" s="T88">num</ta>
            <ta e="T90" id="Seg_1010" s="T89">n</ta>
            <ta e="T91" id="Seg_1011" s="T90">v</ta>
            <ta e="T92" id="Seg_1012" s="T91">pers</ta>
            <ta e="T93" id="Seg_1013" s="T92">n</ta>
            <ta e="T94" id="Seg_1014" s="T93">pers</ta>
            <ta e="T95" id="Seg_1015" s="T94">v</ta>
            <ta e="T96" id="Seg_1016" s="T95">v</ta>
            <ta e="T97" id="Seg_1017" s="T96">dempro</ta>
            <ta e="T98" id="Seg_1018" s="T97">adv</ta>
            <ta e="T99" id="Seg_1019" s="T98">dempro</ta>
            <ta e="T100" id="Seg_1020" s="T99">v</ta>
            <ta e="T101" id="Seg_1021" s="T100">v</ta>
            <ta e="T102" id="Seg_1022" s="T101">n</ta>
            <ta e="T103" id="Seg_1023" s="T102">v</ta>
            <ta e="T104" id="Seg_1024" s="T103">dempro</ta>
            <ta e="T105" id="Seg_1025" s="T104">n</ta>
            <ta e="T106" id="Seg_1026" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1027" s="T106">dempro</ta>
            <ta e="T108" id="Seg_1028" s="T107">dempro</ta>
            <ta e="T109" id="Seg_1029" s="T108">pers</ta>
            <ta e="T110" id="Seg_1030" s="T109">aux</ta>
            <ta e="T111" id="Seg_1031" s="T110">v</ta>
            <ta e="T115" id="Seg_1032" s="T114">n</ta>
            <ta e="T116" id="Seg_1033" s="T115">adj</ta>
            <ta e="T117" id="Seg_1034" s="T116">v</ta>
            <ta e="T118" id="Seg_1035" s="T117">conj</ta>
            <ta e="T119" id="Seg_1036" s="T118">dempro</ta>
            <ta e="T120" id="Seg_1037" s="T119">v</ta>
            <ta e="T121" id="Seg_1038" s="T120">v</ta>
            <ta e="T122" id="Seg_1039" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1040" s="T122">adv</ta>
            <ta e="T124" id="Seg_1041" s="T123">adj</ta>
            <ta e="T125" id="Seg_1042" s="T124">v</ta>
            <ta e="T126" id="Seg_1043" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_1044" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1045" s="T127">adj</ta>
            <ta e="T129" id="Seg_1046" s="T128">n</ta>
            <ta e="T130" id="Seg_1047" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_1048" s="T130">v</ta>
            <ta e="T132" id="Seg_1049" s="T131">v</ta>
            <ta e="T133" id="Seg_1050" s="T132">que</ta>
            <ta e="T134" id="Seg_1051" s="T133">adj</ta>
            <ta e="T136" id="Seg_1052" s="T135">v</ta>
            <ta e="T137" id="Seg_1053" s="T136">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T51" id="Seg_1054" s="T50">np.h:E</ta>
            <ta e="T52" id="Seg_1055" s="T51">pro.h:Poss</ta>
            <ta e="T53" id="Seg_1056" s="T52">np:Th</ta>
            <ta e="T56" id="Seg_1057" s="T55">np.h:A</ta>
            <ta e="T57" id="Seg_1058" s="T56">pro.h:B</ta>
            <ta e="T60" id="Seg_1059" s="T59">pro:Th</ta>
            <ta e="T61" id="Seg_1060" s="T60">0.3.h:A</ta>
            <ta e="T63" id="Seg_1061" s="T62">adv:Time</ta>
            <ta e="T66" id="Seg_1062" s="T65">0.2.h:Th</ta>
            <ta e="T67" id="Seg_1063" s="T66">adv:Time</ta>
            <ta e="T68" id="Seg_1064" s="T67">pro.h:Poss</ta>
            <ta e="T69" id="Seg_1065" s="T68">np.h:Th</ta>
            <ta e="T71" id="Seg_1066" s="T70">pro.h:E</ta>
            <ta e="T72" id="Seg_1067" s="T71">pro.h:B</ta>
            <ta e="T76" id="Seg_1068" s="T75">np.h:A</ta>
            <ta e="T80" id="Seg_1069" s="T79">0.3.h:A</ta>
            <ta e="T82" id="Seg_1070" s="T81">pro.h:Poss</ta>
            <ta e="T83" id="Seg_1071" s="T82">np.h:Th</ta>
            <ta e="T87" id="Seg_1072" s="T86">0.3.h:A</ta>
            <ta e="T88" id="Seg_1073" s="T87">adv:Time</ta>
            <ta e="T90" id="Seg_1074" s="T89">np.h:A</ta>
            <ta e="T92" id="Seg_1075" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_1076" s="T92">np.h:A</ta>
            <ta e="T94" id="Seg_1077" s="T93">pro.h:R</ta>
            <ta e="T96" id="Seg_1078" s="T95">0.2.h:A</ta>
            <ta e="T97" id="Seg_1079" s="T96">pro.h:R</ta>
            <ta e="T98" id="Seg_1080" s="T97">adv:Time</ta>
            <ta e="T99" id="Seg_1081" s="T98">pro.h:A</ta>
            <ta e="T101" id="Seg_1082" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_1083" s="T101">n:Time</ta>
            <ta e="T104" id="Seg_1084" s="T103">pro.h:Poss</ta>
            <ta e="T105" id="Seg_1085" s="T104">np.h:A</ta>
            <ta e="T107" id="Seg_1086" s="T106">pro.h:A</ta>
            <ta e="T108" id="Seg_1087" s="T107">pro.h:R</ta>
            <ta e="T109" id="Seg_1088" s="T108">pro.h:A</ta>
            <ta e="T115" id="Seg_1089" s="T114">np.h:R</ta>
            <ta e="T117" id="Seg_1090" s="T116">0.2.h:A</ta>
            <ta e="T119" id="Seg_1091" s="T118">pro.h:A</ta>
            <ta e="T132" id="Seg_1092" s="T131">0.2.h:E</ta>
            <ta e="T136" id="Seg_1093" s="T135">0.3:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T49" id="Seg_1094" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_1095" s="T50">np.h:S</ta>
            <ta e="T53" id="Seg_1096" s="T52">np:S</ta>
            <ta e="T54" id="Seg_1097" s="T53">adj:pred</ta>
            <ta e="T55" id="Seg_1098" s="T54">cop</ta>
            <ta e="T56" id="Seg_1099" s="T55">np.h:S</ta>
            <ta e="T57" id="Seg_1100" s="T56">pro.h:O</ta>
            <ta e="T59" id="Seg_1101" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_1102" s="T59">pro:O</ta>
            <ta e="T61" id="Seg_1103" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_1104" s="T63">adj:pred</ta>
            <ta e="T66" id="Seg_1105" s="T65">cop 0.2.h:S</ta>
            <ta e="T69" id="Seg_1106" s="T68">np.h:S</ta>
            <ta e="T70" id="Seg_1107" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_1108" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_1109" s="T71">pro.h:O</ta>
            <ta e="T75" id="Seg_1110" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_1111" s="T75">np.h:S</ta>
            <ta e="T78" id="Seg_1112" s="T77">v:pred</ta>
            <ta e="T80" id="Seg_1113" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_1114" s="T82">np.h:S</ta>
            <ta e="T84" id="Seg_1115" s="T83">ptcl.neg</ta>
            <ta e="T86" id="Seg_1116" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_1117" s="T86">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_1118" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_1119" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_1120" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_1121" s="T93">pro.h:O</ta>
            <ta e="T95" id="Seg_1122" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_1123" s="T95">v:pred 0.2.h:S</ta>
            <ta e="T97" id="Seg_1124" s="T96">pro.h:O</ta>
            <ta e="T99" id="Seg_1125" s="T98">pro.h:S</ta>
            <ta e="T100" id="Seg_1126" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_1127" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T103" id="Seg_1128" s="T102">v:pred</ta>
            <ta e="T105" id="Seg_1129" s="T104">np.h:S</ta>
            <ta e="T106" id="Seg_1130" s="T105">ptcl:pred</ta>
            <ta e="T109" id="Seg_1131" s="T108">pro.h:S</ta>
            <ta e="T110" id="Seg_1132" s="T109">v:pred</ta>
            <ta e="T117" id="Seg_1133" s="T116">v:pred 0.2.h:S</ta>
            <ta e="T119" id="Seg_1134" s="T118">pro.h:S</ta>
            <ta e="T120" id="Seg_1135" s="T119">v:pred</ta>
            <ta e="T125" id="Seg_1136" s="T124">v:pred 0.3:S</ta>
            <ta e="T127" id="Seg_1137" s="T126">ptcl.neg</ta>
            <ta e="T132" id="Seg_1138" s="T131">v:pred 0.2.h:S</ta>
            <ta e="T134" id="Seg_1139" s="T133">adj:pred</ta>
            <ta e="T136" id="Seg_1140" s="T135">cop 0.3:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T58" id="Seg_1141" s="T57">TURK:disc</ta>
            <ta e="T62" id="Seg_1142" s="T61">RUS:gram</ta>
            <ta e="T64" id="Seg_1143" s="T63">TURK:core</ta>
            <ta e="T79" id="Seg_1144" s="T78">RUS:gram</ta>
            <ta e="T81" id="Seg_1145" s="T80">RUS:gram</ta>
            <ta e="T85" id="Seg_1146" s="T84">TURK:core</ta>
            <ta e="T106" id="Seg_1147" s="T105">RUS:gram</ta>
            <ta e="T116" id="Seg_1148" s="T115">TURK:core</ta>
            <ta e="T117" id="Seg_1149" s="T116">%TURK:core</ta>
            <ta e="T118" id="Seg_1150" s="T117">RUS:gram</ta>
            <ta e="T122" id="Seg_1151" s="T121">RUS:gram</ta>
            <ta e="T123" id="Seg_1152" s="T122">RUS:mod</ta>
            <ta e="T124" id="Seg_1153" s="T123">TURK:core</ta>
            <ta e="T126" id="Seg_1154" s="T125">TURK:disc</ta>
            <ta e="T128" id="Seg_1155" s="T127">TURK:core</ta>
            <ta e="T134" id="Seg_1156" s="T133">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T125" id="Seg_1157" s="T120">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T51" id="Seg_1158" s="T48">Жил один человек.</ta>
            <ta e="T55" id="Seg_1159" s="T51">Он был очень (глупым?).</ta>
            <ta e="T59" id="Seg_1160" s="T55">Люди ему помогали.</ta>
            <ta e="T66" id="Seg_1161" s="T59">Что он скажет, [они] всегда [отвечают]: «Будь хорошим.» [?]</ta>
            <ta e="T70" id="Seg_1162" s="T66">Потом у него был сын.</ta>
            <ta e="T75" id="Seg_1163" s="T70">Он его очень любил.</ta>
            <ta e="T86" id="Seg_1164" s="T75">Люди приходят и говорят: «Твой сын нехорошо поступает.</ta>
            <ta e="T87" id="Seg_1165" s="T86">Он ругается. [?]</ta>
            <ta e="T91" id="Seg_1166" s="T87">Потом пришла одна женщина:</ta>
            <ta e="T95" id="Seg_1167" s="T91">«Твой сын меня обругал.</ta>
            <ta e="T97" id="Seg_1168" s="T95">Выругай его!»</ta>
            <ta e="T105" id="Seg_1169" s="T97">Потом он ждал, ждал, вечером приходит его сын.</ta>
            <ta e="T108" id="Seg_1170" s="T105">Он стал ему [говорить]:</ta>
            <ta e="T114" id="Seg_1171" s="T108">«Не кричи!</ta>
            <ta e="T117" id="Seg_1172" s="T114">Говори с людьми по-хорошему!»</ta>
            <ta e="T120" id="Seg_1173" s="T117">А он говорит:</ta>
            <ta e="T125" id="Seg_1174" s="T120">«[Если] кричать, так только хорошо будет».</ta>
            <ta e="T128" id="Seg_1175" s="T125">«Нет, не хорошо.</ta>
            <ta e="T136" id="Seg_1176" s="T128">Если не будешь кричать на лошадь, увидишь, какой она хорошей станет». [?]</ta>
            <ta e="T137" id="Seg_1177" s="T136">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T51" id="Seg_1178" s="T48">One man lived.</ta>
            <ta e="T55" id="Seg_1179" s="T51">He was very (stupid?).</ta>
            <ta e="T59" id="Seg_1180" s="T55">People took care of him.</ta>
            <ta e="T66" id="Seg_1181" s="T59">Whatever he says, [they] always [answer]: "Be good." [?]</ta>
            <ta e="T70" id="Seg_1182" s="T66">Then he had a son.</ta>
            <ta e="T75" id="Seg_1183" s="T70">He cared for him very much.</ta>
            <ta e="T86" id="Seg_1184" s="T75">People are going and saying: "Your son does not do good.</ta>
            <ta e="T87" id="Seg_1185" s="T86">He swears. [?]</ta>
            <ta e="T91" id="Seg_1186" s="T87">Then one woman came:</ta>
            <ta e="T95" id="Seg_1187" s="T91">“Your son cursed me.</ta>
            <ta e="T97" id="Seg_1188" s="T95">Scold him!”</ta>
            <ta e="T105" id="Seg_1189" s="T97">Then he waited, he waited, in the evening his son came.</ta>
            <ta e="T108" id="Seg_1190" s="T105">He started [to tell] him:</ta>
            <ta e="T114" id="Seg_1191" s="T108">"Don't shout!</ta>
            <ta e="T117" id="Seg_1192" s="T114">Speak nicely with people!"</ta>
            <ta e="T120" id="Seg_1193" s="T117">But he says:</ta>
            <ta e="T125" id="Seg_1194" s="T120">"It is only good to shout."</ta>
            <ta e="T128" id="Seg_1195" s="T125">"No, not good.</ta>
            <ta e="T136" id="Seg_1196" s="T128">If you don't shout at the horse, you see how good it is." [?]</ta>
            <ta e="T137" id="Seg_1197" s="T136">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T51" id="Seg_1198" s="T48">Es lebte ein Mann.</ta>
            <ta e="T55" id="Seg_1199" s="T51">Er war sehr (dumm?).</ta>
            <ta e="T59" id="Seg_1200" s="T55">Leute schauten nach ihm.</ta>
            <ta e="T66" id="Seg_1201" s="T59">Was auch immer er sagt [antworten sie] immer: „Sei brav.“ [?]</ta>
            <ta e="T70" id="Seg_1202" s="T66">Dann bekam er einen Sohn.</ta>
            <ta e="T75" id="Seg_1203" s="T70">Er sorgte sich sehr um ihn.</ta>
            <ta e="T86" id="Seg_1204" s="T75">Leute gehen umher und sagen: „Dein Sohn schlägt sich nicht gut.</ta>
            <ta e="T87" id="Seg_1205" s="T86">Er flucht. [?]</ta>
            <ta e="T91" id="Seg_1206" s="T87">Dann kam eine Frau:</ta>
            <ta e="T95" id="Seg_1207" s="T91"> „Dein Sohn hat mich angeflucht.</ta>
            <ta e="T97" id="Seg_1208" s="T95">Weise ihn zurecht!”</ta>
            <ta e="T105" id="Seg_1209" s="T97">Dann wartete er, er wartete, am Abend kam sein Sohn.</ta>
            <ta e="T108" id="Seg_1210" s="T105">Er fing an, ihm [zu erzählen]:</ta>
            <ta e="T114" id="Seg_1211" s="T108"> „Brüll nicht!</ta>
            <ta e="T117" id="Seg_1212" s="T114">Sprich nett mit den Leuten!“</ta>
            <ta e="T120" id="Seg_1213" s="T117">Aber er sagt:</ta>
            <ta e="T125" id="Seg_1214" s="T120">„Es nutzt nur was, zu brüllen.“</ta>
            <ta e="T128" id="Seg_1215" s="T125">„Nein, nicht gut.</ta>
            <ta e="T136" id="Seg_1216" s="T128">Wenn du nicht das Pferd anbrüllst, siehst du, wieviel es nutzt.“ [?]</ta>
            <ta e="T137" id="Seg_1217" s="T136">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T55" id="Seg_1218" s="T51">[GVY:] stupid or smart? (Cf. the "wizzards" in "Six fools")</ta>
            <ta e="T66" id="Seg_1219" s="T59">[KlT:] 2PL instead of 3SG. [GVY:] Unclear</ta>
            <ta e="T95" id="Seg_1220" s="T91">[GVY:] Treating -l- in kudol- as a transitive suffix is a temporary solution.</ta>
            <ta e="T136" id="Seg_1221" s="T128">[GVY:] kuiol? [GVY:] Generally unclear. Might also mean "If the horse does not neigh"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
