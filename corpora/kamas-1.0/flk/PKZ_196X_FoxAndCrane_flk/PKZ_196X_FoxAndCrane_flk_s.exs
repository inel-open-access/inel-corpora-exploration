<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8F84B907-F4B7-34D2-2FF2-44A497935B7D">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndCrane_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndCrane_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndCrane_flk\PKZ_196X_FoxAndCrane_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">126</ud-information>
            <ud-information attribute-name="# HIAT:w">83</ud-information>
            <ud-information attribute-name="# e">84</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T831" time="0.0" type="appl" />
         <tli id="T832" time="0.693" type="appl" />
         <tli id="T833" time="1.385" type="appl" />
         <tli id="T834" time="2.078" type="appl" />
         <tli id="T835" time="2.77" type="appl" />
         <tli id="T836" time="3.9733131948557125" />
         <tli id="T837" time="4.632" type="appl" />
         <tli id="T838" time="5.409" type="appl" />
         <tli id="T839" time="6.186" type="appl" />
         <tli id="T840" time="6.963" type="appl" />
         <tli id="T841" time="7.739" type="appl" />
         <tli id="T842" time="8.516" type="appl" />
         <tli id="T843" time="9.293" type="appl" />
         <tli id="T844" time="10.07" type="appl" />
         <tli id="T845" time="12.446603581871837" />
         <tli id="T846" time="13.333" type="appl" />
         <tli id="T847" time="14.08" type="appl" />
         <tli id="T848" time="14.791" type="appl" />
         <tli id="T849" time="15.502" type="appl" />
         <tli id="T850" time="16.213" type="appl" />
         <tli id="T851" time="17.233245987755062" />
         <tli id="T852" time="17.88" type="appl" />
         <tli id="T853" time="18.514" type="appl" />
         <tli id="T854" time="19.148" type="appl" />
         <tli id="T855" time="19.781" type="appl" />
         <tli id="T856" time="20.414" type="appl" />
         <tli id="T857" time="21.5932238894927" />
         <tli id="T858" time="21.972" type="appl" />
         <tli id="T859" time="22.411" type="appl" />
         <tli id="T860" time="22.851" type="appl" />
         <tli id="T861" time="23.29" type="appl" />
         <tli id="T862" time="23.73" type="appl" />
         <tli id="T863" time="24.169" type="appl" />
         <tli id="T864" time="24.609" type="appl" />
         <tli id="T865" time="25.554" type="appl" />
         <tli id="T866" time="26.67986477485329" />
         <tli id="T867" time="27.433" type="appl" />
         <tli id="T868" time="28.135" type="appl" />
         <tli id="T869" time="28.837" type="appl" />
         <tli id="T870" time="29.539" type="appl" />
         <tli id="T871" time="30.219" type="appl" />
         <tli id="T872" time="30.787" type="appl" />
         <tli id="T873" time="31.43317401635014" />
         <tli id="T874" time="32.199" type="appl" />
         <tli id="T875" time="33.042" type="appl" />
         <tli id="T876" time="33.886" type="appl" />
         <tli id="T877" time="34.729" type="appl" />
         <tli id="T878" time="35.572" type="appl" />
         <tli id="T879" time="36.255" type="appl" />
         <tli id="T880" time="36.938" type="appl" />
         <tli id="T881" time="37.621" type="appl" />
         <tli id="T882" time="38.304" type="appl" />
         <tli id="T883" time="39.064" type="appl" />
         <tli id="T884" time="39.823" type="appl" />
         <tli id="T885" time="40.583" type="appl" />
         <tli id="T886" time="41.495" type="appl" />
         <tli id="T887" time="42.392" type="appl" />
         <tli id="T888" time="43.299" type="appl" />
         <tli id="T889" time="44.40644159552667" />
         <tli id="T890" time="44.854" type="appl" />
         <tli id="T891" time="45.504" type="appl" />
         <tli id="T892" time="46.153" type="appl" />
         <tli id="T893" time="46.802" type="appl" />
         <tli id="T894" time="47.451" type="appl" />
         <tli id="T895" time="48.1" type="appl" />
         <tli id="T896" time="48.75" type="appl" />
         <tli id="T897" time="49.399" type="appl" />
         <tli id="T898" time="50.047" type="appl" />
         <tli id="T899" time="50.695" type="appl" />
         <tli id="T900" time="51.342" type="appl" />
         <tli id="T901" time="51.99" type="appl" />
         <tli id="T902" time="52.589" type="appl" />
         <tli id="T903" time="53.187" type="appl" />
         <tli id="T904" time="53.786" type="appl" />
         <tli id="T905" time="54.385" type="appl" />
         <tli id="T906" time="54.983" type="appl" />
         <tli id="T907" time="55.582" type="appl" />
         <tli id="T908" time="56.18" type="appl" />
         <tli id="T909" time="56.779" type="appl" />
         <tli id="T910" time="57.378" type="appl" />
         <tli id="T911" time="57.976" type="appl" />
         <tli id="T912" time="58.575" type="appl" />
         <tli id="T913" time="59.421" type="appl" />
         <tli id="T914" time="60.21" type="appl" />
         <tli id="T915" time="61.863" type="appl" />
         <tli id="T0" time="61.913" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T915" id="Seg_0" n="sc" s="T831">
               <ts e="T836" id="Seg_2" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_4" n="HIAT:w" s="T831">Lisa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_7" n="HIAT:w" s="T832">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_10" n="HIAT:w" s="T833">žuravlʼ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_13" n="HIAT:w" s="T834">amnobiʔi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_16" n="HIAT:w" s="T835">šidegöʔ</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_20" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_22" n="HIAT:w" s="T836">Lisa</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">(</nts>
                  <ts e="T838" id="Seg_26" n="HIAT:w" s="T837">žuravlʼənə</ts>
                  <nts id="Seg_27" n="HIAT:ip">)</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_30" n="HIAT:w" s="T838">măndə:</ts>
                  <nts id="Seg_31" n="HIAT:ip">"</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_34" n="HIAT:w" s="T839">Šoʔ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_37" n="HIAT:w" s="T840">măna</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_40" n="HIAT:w" s="T841">jadajlaʔ</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_44" n="HIAT:w" s="T842">măn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_47" n="HIAT:w" s="T843">tănan</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_50" n="HIAT:w" s="T844">mĭnzərləm</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_54" n="HIAT:u" s="T845">
                  <ts e="T846" id="Seg_56" n="HIAT:w" s="T845">Kaša</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_59" n="HIAT:w" s="T846">amnal</ts>
                  <nts id="Seg_60" n="HIAT:ip">"</nts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T851" id="Seg_64" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_66" n="HIAT:w" s="T847">Dĭgəttə</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_69" n="HIAT:w" s="T848">dĭ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_72" n="HIAT:w" s="T849">kămnəbi</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_75" n="HIAT:w" s="T850">tarʼelkanə</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T857" id="Seg_79" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_81" n="HIAT:w" s="T851">Bostə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_84" n="HIAT:w" s="T852">amnaʔbə</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_88" n="HIAT:w" s="T853">a</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_91" n="HIAT:w" s="T854">dĭʔnə</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_94" n="HIAT:w" s="T855">nʼelʼzʼa</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_97" n="HIAT:w" s="T856">amzittə</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_101" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_103" n="HIAT:w" s="T857">Dĭn</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_106" n="HIAT:w" s="T858">püjet</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_108" n="HIAT:ip">(</nts>
                  <ts e="T860" id="Seg_110" n="HIAT:w" s="T859">no-</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_113" n="HIAT:w" s="T860">u-</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_117" n="HIAT:w" s="T861">numo</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_121" n="HIAT:w" s="T862">ej</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_124" n="HIAT:w" s="T863">mobi</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_128" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_130" n="HIAT:w" s="T864">Dĭgəttə</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_133" n="HIAT:w" s="T865">măndə:</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <ts e="T867" id="Seg_137" n="HIAT:w" s="T866">Jakšə</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_141" n="HIAT:w" s="T867">măn</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_144" n="HIAT:w" s="T868">kallam</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_147" n="HIAT:w" s="T869">maʔnən</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T873" id="Seg_151" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_153" n="HIAT:w" s="T870">Šoʔ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_156" n="HIAT:w" s="T871">măna</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_159" n="HIAT:w" s="T872">jadajlaʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip">!</nts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_164" n="HIAT:u" s="T873">
                  <ts e="T874" id="Seg_166" n="HIAT:w" s="T873">Dĭgəttə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_169" n="HIAT:w" s="T874">karəldʼan</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_172" n="HIAT:w" s="T875">lisitsa</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_175" n="HIAT:w" s="T876">dĭʔnə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_178" n="HIAT:w" s="T877">kambi</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_182" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_184" n="HIAT:w" s="T878">A</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_186" n="HIAT:ip">(</nts>
                  <ts e="T880" id="Seg_188" n="HIAT:w" s="T879">dĭ</ts>
                  <nts id="Seg_189" n="HIAT:ip">)</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_192" n="HIAT:w" s="T880">mĭnzərbi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_195" n="HIAT:w" s="T881">mĭj</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T885" id="Seg_199" n="HIAT:u" s="T882">
                  <ts e="T883" id="Seg_201" n="HIAT:w" s="T882">I</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_204" n="HIAT:w" s="T883">kukšində</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_207" n="HIAT:w" s="T884">kămnəbi</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T887" id="Seg_211" n="HIAT:u" s="T885">
                  <ts e="T886" id="Seg_213" n="HIAT:w" s="T885">Bostə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_216" n="HIAT:w" s="T886">amnaʔbə</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T889" id="Seg_220" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_222" n="HIAT:w" s="T887">Urgo</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_225" n="HIAT:w" s="T888">püjetsiʔ</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_229" n="HIAT:u" s="T889">
                  <ts e="T890" id="Seg_231" n="HIAT:w" s="T889">A</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_234" n="HIAT:w" s="T890">dĭ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_237" n="HIAT:w" s="T891">mĭnge</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_241" n="HIAT:w" s="T892">mĭnge</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_244" n="HIAT:w" s="T893">vʼezʼdʼe</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_248" n="HIAT:w" s="T894">ej</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_251" n="HIAT:w" s="T895">molia</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_254" n="HIAT:w" s="T896">amzittə</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T901" id="Seg_258" n="HIAT:u" s="T897">
                  <ts e="T898" id="Seg_260" n="HIAT:w" s="T897">Dĭgəttə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_263" n="HIAT:w" s="T898">kalla</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_266" n="HIAT:w" s="T899">dʼürbi</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_269" n="HIAT:w" s="T900">maːndə</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_273" n="HIAT:u" s="T901">
                  <ts e="T902" id="Seg_275" n="HIAT:w" s="T901">I</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">(</nts>
                  <ts e="T903" id="Seg_279" n="HIAT:w" s="T902">suj</ts>
                  <nts id="Seg_280" n="HIAT:ip">)</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_283" n="HIAT:w" s="T903">šindin</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_287" n="HIAT:w" s="T904">dĭ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_290" n="HIAT:w" s="T905">dĭʔnə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_293" n="HIAT:w" s="T906">ej</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_296" n="HIAT:w" s="T907">mĭnlie</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_300" n="HIAT:w" s="T908">dĭ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_303" n="HIAT:w" s="T909">dĭʔnə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_306" n="HIAT:w" s="T910">ej</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_309" n="HIAT:w" s="T911">mĭnlie</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_313" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_315" n="HIAT:w" s="T912">I</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_318" n="HIAT:w" s="T913">kabarləj</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T915" id="Seg_322" n="HIAT:u" s="T914">
                  <nts id="Seg_323" n="HIAT:ip">(</nts>
                  <nts id="Seg_324" n="HIAT:ip">(</nts>
                  <ats e="T915" id="Seg_325" n="HIAT:non-pho" s="T914">DMG</ats>
                  <nts id="Seg_326" n="HIAT:ip">)</nts>
                  <nts id="Seg_327" n="HIAT:ip">)</nts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T915" id="Seg_330" n="sc" s="T831">
               <ts e="T832" id="Seg_332" n="e" s="T831">Lisa </ts>
               <ts e="T833" id="Seg_334" n="e" s="T832">i </ts>
               <ts e="T834" id="Seg_336" n="e" s="T833">žuravlʼ </ts>
               <ts e="T835" id="Seg_338" n="e" s="T834">amnobiʔi </ts>
               <ts e="T836" id="Seg_340" n="e" s="T835">šidegöʔ. </ts>
               <ts e="T837" id="Seg_342" n="e" s="T836">Lisa </ts>
               <ts e="T838" id="Seg_344" n="e" s="T837">(žuravlʼənə) </ts>
               <ts e="T839" id="Seg_346" n="e" s="T838">măndə:" </ts>
               <ts e="T840" id="Seg_348" n="e" s="T839">Šoʔ </ts>
               <ts e="T841" id="Seg_350" n="e" s="T840">măna </ts>
               <ts e="T842" id="Seg_352" n="e" s="T841">jadajlaʔ, </ts>
               <ts e="T843" id="Seg_354" n="e" s="T842">măn </ts>
               <ts e="T844" id="Seg_356" n="e" s="T843">tănan </ts>
               <ts e="T845" id="Seg_358" n="e" s="T844">mĭnzərləm. </ts>
               <ts e="T846" id="Seg_360" n="e" s="T845">Kaša </ts>
               <ts e="T847" id="Seg_362" n="e" s="T846">amnal". </ts>
               <ts e="T848" id="Seg_364" n="e" s="T847">Dĭgəttə </ts>
               <ts e="T849" id="Seg_366" n="e" s="T848">dĭ </ts>
               <ts e="T850" id="Seg_368" n="e" s="T849">kămnəbi </ts>
               <ts e="T851" id="Seg_370" n="e" s="T850">tarʼelkanə. </ts>
               <ts e="T852" id="Seg_372" n="e" s="T851">Bostə </ts>
               <ts e="T853" id="Seg_374" n="e" s="T852">amnaʔbə, </ts>
               <ts e="T854" id="Seg_376" n="e" s="T853">a </ts>
               <ts e="T855" id="Seg_378" n="e" s="T854">dĭʔnə </ts>
               <ts e="T856" id="Seg_380" n="e" s="T855">nʼelʼzʼa </ts>
               <ts e="T857" id="Seg_382" n="e" s="T856">amzittə. </ts>
               <ts e="T858" id="Seg_384" n="e" s="T857">Dĭn </ts>
               <ts e="T859" id="Seg_386" n="e" s="T858">püjet </ts>
               <ts e="T860" id="Seg_388" n="e" s="T859">(no- </ts>
               <ts e="T861" id="Seg_390" n="e" s="T860">u-) </ts>
               <ts e="T862" id="Seg_392" n="e" s="T861">numo, </ts>
               <ts e="T863" id="Seg_394" n="e" s="T862">ej </ts>
               <ts e="T864" id="Seg_396" n="e" s="T863">mobi. </ts>
               <ts e="T865" id="Seg_398" n="e" s="T864">Dĭgəttə </ts>
               <ts e="T866" id="Seg_400" n="e" s="T865">măndə: </ts>
               <ts e="T867" id="Seg_402" n="e" s="T866">"Jakšə, </ts>
               <ts e="T868" id="Seg_404" n="e" s="T867">măn </ts>
               <ts e="T869" id="Seg_406" n="e" s="T868">kallam </ts>
               <ts e="T870" id="Seg_408" n="e" s="T869">maʔnən. </ts>
               <ts e="T871" id="Seg_410" n="e" s="T870">Šoʔ </ts>
               <ts e="T872" id="Seg_412" n="e" s="T871">măna </ts>
               <ts e="T873" id="Seg_414" n="e" s="T872">jadajlaʔ!" </ts>
               <ts e="T874" id="Seg_416" n="e" s="T873">Dĭgəttə </ts>
               <ts e="T875" id="Seg_418" n="e" s="T874">karəldʼan </ts>
               <ts e="T876" id="Seg_420" n="e" s="T875">lisitsa </ts>
               <ts e="T877" id="Seg_422" n="e" s="T876">dĭʔnə </ts>
               <ts e="T878" id="Seg_424" n="e" s="T877">kambi. </ts>
               <ts e="T879" id="Seg_426" n="e" s="T878">A </ts>
               <ts e="T880" id="Seg_428" n="e" s="T879">(dĭ) </ts>
               <ts e="T881" id="Seg_430" n="e" s="T880">mĭnzərbi </ts>
               <ts e="T882" id="Seg_432" n="e" s="T881">mĭj. </ts>
               <ts e="T883" id="Seg_434" n="e" s="T882">I </ts>
               <ts e="T884" id="Seg_436" n="e" s="T883">kukšində </ts>
               <ts e="T885" id="Seg_438" n="e" s="T884">kămnəbi. </ts>
               <ts e="T886" id="Seg_440" n="e" s="T885">Bostə </ts>
               <ts e="T887" id="Seg_442" n="e" s="T886">amnaʔbə. </ts>
               <ts e="T888" id="Seg_444" n="e" s="T887">Urgo </ts>
               <ts e="T889" id="Seg_446" n="e" s="T888">püjetsiʔ. </ts>
               <ts e="T890" id="Seg_448" n="e" s="T889">A </ts>
               <ts e="T891" id="Seg_450" n="e" s="T890">dĭ </ts>
               <ts e="T892" id="Seg_452" n="e" s="T891">mĭnge, </ts>
               <ts e="T893" id="Seg_454" n="e" s="T892">mĭnge </ts>
               <ts e="T894" id="Seg_456" n="e" s="T893">vʼezʼdʼe, </ts>
               <ts e="T895" id="Seg_458" n="e" s="T894">ej </ts>
               <ts e="T896" id="Seg_460" n="e" s="T895">molia </ts>
               <ts e="T897" id="Seg_462" n="e" s="T896">amzittə. </ts>
               <ts e="T898" id="Seg_464" n="e" s="T897">Dĭgəttə </ts>
               <ts e="T899" id="Seg_466" n="e" s="T898">kalla </ts>
               <ts e="T900" id="Seg_468" n="e" s="T899">dʼürbi </ts>
               <ts e="T901" id="Seg_470" n="e" s="T900">maːndə. </ts>
               <ts e="T902" id="Seg_472" n="e" s="T901">I </ts>
               <ts e="T903" id="Seg_474" n="e" s="T902">(suj) </ts>
               <ts e="T904" id="Seg_476" n="e" s="T903">šindin, </ts>
               <ts e="T905" id="Seg_478" n="e" s="T904">dĭ </ts>
               <ts e="T906" id="Seg_480" n="e" s="T905">dĭʔnə </ts>
               <ts e="T907" id="Seg_482" n="e" s="T906">ej </ts>
               <ts e="T908" id="Seg_484" n="e" s="T907">mĭnlie, </ts>
               <ts e="T909" id="Seg_486" n="e" s="T908">dĭ </ts>
               <ts e="T910" id="Seg_488" n="e" s="T909">dĭʔnə </ts>
               <ts e="T911" id="Seg_490" n="e" s="T910">ej </ts>
               <ts e="T912" id="Seg_492" n="e" s="T911">mĭnlie. </ts>
               <ts e="T913" id="Seg_494" n="e" s="T912">I </ts>
               <ts e="T914" id="Seg_496" n="e" s="T913">kabarləj. </ts>
               <ts e="T915" id="Seg_498" n="e" s="T914">((DMG)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T836" id="Seg_499" s="T831">PKZ_196X_FoxAndCrane_flk.001 (001)</ta>
            <ta e="T845" id="Seg_500" s="T836">PKZ_196X_FoxAndCrane_flk.002 (002)</ta>
            <ta e="T847" id="Seg_501" s="T845">PKZ_196X_FoxAndCrane_flk.003 (003)</ta>
            <ta e="T851" id="Seg_502" s="T847">PKZ_196X_FoxAndCrane_flk.004 (004)</ta>
            <ta e="T857" id="Seg_503" s="T851">PKZ_196X_FoxAndCrane_flk.005 (005)</ta>
            <ta e="T864" id="Seg_504" s="T857">PKZ_196X_FoxAndCrane_flk.006 (006)</ta>
            <ta e="T870" id="Seg_505" s="T864">PKZ_196X_FoxAndCrane_flk.007 (007)</ta>
            <ta e="T873" id="Seg_506" s="T870">PKZ_196X_FoxAndCrane_flk.008 (009)</ta>
            <ta e="T878" id="Seg_507" s="T873">PKZ_196X_FoxAndCrane_flk.009 (010)</ta>
            <ta e="T882" id="Seg_508" s="T878">PKZ_196X_FoxAndCrane_flk.010 (011)</ta>
            <ta e="T885" id="Seg_509" s="T882">PKZ_196X_FoxAndCrane_flk.011 (012)</ta>
            <ta e="T887" id="Seg_510" s="T885">PKZ_196X_FoxAndCrane_flk.012 (013)</ta>
            <ta e="T889" id="Seg_511" s="T887">PKZ_196X_FoxAndCrane_flk.013 (014)</ta>
            <ta e="T897" id="Seg_512" s="T889">PKZ_196X_FoxAndCrane_flk.014 (015)</ta>
            <ta e="T901" id="Seg_513" s="T897">PKZ_196X_FoxAndCrane_flk.015 (016)</ta>
            <ta e="T912" id="Seg_514" s="T901">PKZ_196X_FoxAndCrane_flk.016 (017)</ta>
            <ta e="T914" id="Seg_515" s="T912">PKZ_196X_FoxAndCrane_flk.017 (018)</ta>
            <ta e="T915" id="Seg_516" s="T914">PKZ_196X_FoxAndCrane_flk.018 (019)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T836" id="Seg_517" s="T831">Lisa i žuravlʼ amnobiʔi šidegöʔ. </ta>
            <ta e="T845" id="Seg_518" s="T836">Lisa (žuravlʼənə) măndə:" Šoʔ măna jadajlaʔ, măn tănan mĭnzərləm. </ta>
            <ta e="T847" id="Seg_519" s="T845">Kaša amnal". </ta>
            <ta e="T851" id="Seg_520" s="T847">Dĭgəttə dĭ kămnəbi tarʼelkanə. </ta>
            <ta e="T857" id="Seg_521" s="T851">Bostə amnaʔbə, a dĭʔnə nʼelʼzʼa amzittə. </ta>
            <ta e="T864" id="Seg_522" s="T857">Dĭn püjet (no- u-) numo, ej mobi. </ta>
            <ta e="T870" id="Seg_523" s="T864">Dĭgəttə măndə: "Jakšə, măn kallam maʔnən. </ta>
            <ta e="T873" id="Seg_524" s="T870">Šoʔ măna jadajlaʔ!" </ta>
            <ta e="T878" id="Seg_525" s="T873">Dĭgəttə karəldʼan lisitsa dĭʔnə kambi. </ta>
            <ta e="T882" id="Seg_526" s="T878">A (dĭ) mĭnzərbi mĭj. </ta>
            <ta e="T885" id="Seg_527" s="T882">I kukšində kămnəbi. </ta>
            <ta e="T887" id="Seg_528" s="T885">Bostə amnaʔbə. </ta>
            <ta e="T889" id="Seg_529" s="T887">Urgo püjetsiʔ. </ta>
            <ta e="T897" id="Seg_530" s="T889">A dĭ mĭnge, mĭnge vʼezʼdʼe, ej molia amzittə. </ta>
            <ta e="T901" id="Seg_531" s="T897">Dĭgəttə kalla dʼürbi maːndə. </ta>
            <ta e="T912" id="Seg_532" s="T901">I (suj) šindin, dĭ dĭʔnə ej mĭnlie, dĭ dĭʔnə ej mĭnlie. </ta>
            <ta e="T914" id="Seg_533" s="T912">I kabarləj. </ta>
            <ta e="T915" id="Seg_534" s="T914">((DMG)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T832" id="Seg_535" s="T831">lisa</ta>
            <ta e="T833" id="Seg_536" s="T832">i</ta>
            <ta e="T834" id="Seg_537" s="T833">žuravlʼ</ta>
            <ta e="T835" id="Seg_538" s="T834">amno-bi-ʔi</ta>
            <ta e="T836" id="Seg_539" s="T835">šide-göʔ</ta>
            <ta e="T837" id="Seg_540" s="T836">lisa</ta>
            <ta e="T838" id="Seg_541" s="T837">žuravlʼə-nə</ta>
            <ta e="T839" id="Seg_542" s="T838">măn-də</ta>
            <ta e="T840" id="Seg_543" s="T839">šo-ʔ</ta>
            <ta e="T841" id="Seg_544" s="T840">măna</ta>
            <ta e="T842" id="Seg_545" s="T841">jada-j-laʔ</ta>
            <ta e="T843" id="Seg_546" s="T842">măn</ta>
            <ta e="T844" id="Seg_547" s="T843">tănan</ta>
            <ta e="T845" id="Seg_548" s="T844">mĭnzər-lə-m</ta>
            <ta e="T846" id="Seg_549" s="T845">kaša</ta>
            <ta e="T847" id="Seg_550" s="T846">am-na-l</ta>
            <ta e="T848" id="Seg_551" s="T847">dĭgəttə</ta>
            <ta e="T849" id="Seg_552" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_553" s="T849">kămnə-bi</ta>
            <ta e="T851" id="Seg_554" s="T850">tarʼelka-nə</ta>
            <ta e="T852" id="Seg_555" s="T851">bos-tə</ta>
            <ta e="T853" id="Seg_556" s="T852">am-naʔbə</ta>
            <ta e="T854" id="Seg_557" s="T853">a</ta>
            <ta e="T855" id="Seg_558" s="T854">dĭʔ-nə</ta>
            <ta e="T856" id="Seg_559" s="T855">nʼelʼzʼa</ta>
            <ta e="T857" id="Seg_560" s="T856">am-zittə</ta>
            <ta e="T858" id="Seg_561" s="T857">dĭ-n</ta>
            <ta e="T859" id="Seg_562" s="T858">püje-t</ta>
            <ta e="T862" id="Seg_563" s="T861">numo</ta>
            <ta e="T863" id="Seg_564" s="T862">ej</ta>
            <ta e="T864" id="Seg_565" s="T863">mo-bi</ta>
            <ta e="T865" id="Seg_566" s="T864">dĭgəttə</ta>
            <ta e="T866" id="Seg_567" s="T865">măn-də</ta>
            <ta e="T867" id="Seg_568" s="T866">jakšə</ta>
            <ta e="T868" id="Seg_569" s="T867">măn</ta>
            <ta e="T869" id="Seg_570" s="T868">kal-la-m</ta>
            <ta e="T870" id="Seg_571" s="T869">maʔ-nə-n</ta>
            <ta e="T871" id="Seg_572" s="T870">šo-ʔ</ta>
            <ta e="T872" id="Seg_573" s="T871">măna</ta>
            <ta e="T873" id="Seg_574" s="T872">jada-j-laʔ</ta>
            <ta e="T874" id="Seg_575" s="T873">dĭgəttə</ta>
            <ta e="T875" id="Seg_576" s="T874">karəldʼan</ta>
            <ta e="T876" id="Seg_577" s="T875">lisitsa</ta>
            <ta e="T877" id="Seg_578" s="T876">dĭʔ-nə</ta>
            <ta e="T878" id="Seg_579" s="T877">kam-bi</ta>
            <ta e="T879" id="Seg_580" s="T878">a</ta>
            <ta e="T880" id="Seg_581" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_582" s="T880">mĭnzər-bi</ta>
            <ta e="T882" id="Seg_583" s="T881">mĭj</ta>
            <ta e="T883" id="Seg_584" s="T882">i</ta>
            <ta e="T884" id="Seg_585" s="T883">kukšin-də</ta>
            <ta e="T885" id="Seg_586" s="T884">kămnə-bi</ta>
            <ta e="T886" id="Seg_587" s="T885">bos-tə</ta>
            <ta e="T887" id="Seg_588" s="T886">am-naʔbə</ta>
            <ta e="T888" id="Seg_589" s="T887">urgo</ta>
            <ta e="T889" id="Seg_590" s="T888">püje-t-siʔ</ta>
            <ta e="T890" id="Seg_591" s="T889">a</ta>
            <ta e="T891" id="Seg_592" s="T890">dĭ</ta>
            <ta e="T892" id="Seg_593" s="T891">mĭn-ge</ta>
            <ta e="T893" id="Seg_594" s="T892">mĭn-ge</ta>
            <ta e="T894" id="Seg_595" s="T893">vʼezʼdʼe</ta>
            <ta e="T895" id="Seg_596" s="T894">ej</ta>
            <ta e="T896" id="Seg_597" s="T895">mo-lia</ta>
            <ta e="T897" id="Seg_598" s="T896">am-zittə</ta>
            <ta e="T898" id="Seg_599" s="T897">dĭgəttə</ta>
            <ta e="T899" id="Seg_600" s="T898">kal-la</ta>
            <ta e="T900" id="Seg_601" s="T899">dʼür-bi</ta>
            <ta e="T901" id="Seg_602" s="T900">ma-ndə</ta>
            <ta e="T902" id="Seg_603" s="T901">i</ta>
            <ta e="T903" id="Seg_604" s="T902">suj</ta>
            <ta e="T904" id="Seg_605" s="T903">šindi-n</ta>
            <ta e="T905" id="Seg_606" s="T904">dĭ</ta>
            <ta e="T906" id="Seg_607" s="T905">dĭʔ-nə</ta>
            <ta e="T907" id="Seg_608" s="T906">ej</ta>
            <ta e="T908" id="Seg_609" s="T907">mĭn-lie</ta>
            <ta e="T909" id="Seg_610" s="T908">dĭ</ta>
            <ta e="T910" id="Seg_611" s="T909">dĭʔ-nə</ta>
            <ta e="T911" id="Seg_612" s="T910">ej</ta>
            <ta e="T912" id="Seg_613" s="T911">mĭn-lie</ta>
            <ta e="T913" id="Seg_614" s="T912">i</ta>
            <ta e="T914" id="Seg_615" s="T913">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T832" id="Seg_616" s="T831">lʼisa</ta>
            <ta e="T833" id="Seg_617" s="T832">i</ta>
            <ta e="T834" id="Seg_618" s="T833">žuravlʼ</ta>
            <ta e="T835" id="Seg_619" s="T834">amno-bi-jəʔ</ta>
            <ta e="T836" id="Seg_620" s="T835">šide-göʔ</ta>
            <ta e="T837" id="Seg_621" s="T836">lʼisa</ta>
            <ta e="T838" id="Seg_622" s="T837">žuravlʼ-Tə</ta>
            <ta e="T839" id="Seg_623" s="T838">măn-ntə</ta>
            <ta e="T840" id="Seg_624" s="T839">šo-ʔ</ta>
            <ta e="T841" id="Seg_625" s="T840">măna</ta>
            <ta e="T842" id="Seg_626" s="T841">jada-j-lAʔ</ta>
            <ta e="T843" id="Seg_627" s="T842">măn</ta>
            <ta e="T844" id="Seg_628" s="T843">tănan</ta>
            <ta e="T845" id="Seg_629" s="T844">mĭnzər-lV-m</ta>
            <ta e="T846" id="Seg_630" s="T845">kaša</ta>
            <ta e="T847" id="Seg_631" s="T846">am-lV-l</ta>
            <ta e="T848" id="Seg_632" s="T847">dĭgəttə</ta>
            <ta e="T849" id="Seg_633" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_634" s="T849">kămnə-bi</ta>
            <ta e="T851" id="Seg_635" s="T850">tarʼelka-Tə</ta>
            <ta e="T852" id="Seg_636" s="T851">bos-də</ta>
            <ta e="T853" id="Seg_637" s="T852">am-laʔbə</ta>
            <ta e="T854" id="Seg_638" s="T853">a</ta>
            <ta e="T855" id="Seg_639" s="T854">dĭ-Tə</ta>
            <ta e="T856" id="Seg_640" s="T855">nʼelʼzʼa</ta>
            <ta e="T857" id="Seg_641" s="T856">am-zittə</ta>
            <ta e="T858" id="Seg_642" s="T857">dĭ-n</ta>
            <ta e="T859" id="Seg_643" s="T858">püje-t</ta>
            <ta e="T862" id="Seg_644" s="T861">numo</ta>
            <ta e="T863" id="Seg_645" s="T862">ej</ta>
            <ta e="T864" id="Seg_646" s="T863">mo-bi</ta>
            <ta e="T865" id="Seg_647" s="T864">dĭgəttə</ta>
            <ta e="T866" id="Seg_648" s="T865">măn-ntə</ta>
            <ta e="T867" id="Seg_649" s="T866">jakšə</ta>
            <ta e="T868" id="Seg_650" s="T867">măn</ta>
            <ta e="T869" id="Seg_651" s="T868">kan-lV-m</ta>
            <ta e="T870" id="Seg_652" s="T869">maʔ-Tə-n</ta>
            <ta e="T871" id="Seg_653" s="T870">šo-ʔ</ta>
            <ta e="T872" id="Seg_654" s="T871">măna</ta>
            <ta e="T873" id="Seg_655" s="T872">jada-j-lAʔ</ta>
            <ta e="T874" id="Seg_656" s="T873">dĭgəttə</ta>
            <ta e="T875" id="Seg_657" s="T874">karəldʼaːn</ta>
            <ta e="T876" id="Seg_658" s="T875">lʼisʼitsa</ta>
            <ta e="T877" id="Seg_659" s="T876">dĭ-Tə</ta>
            <ta e="T878" id="Seg_660" s="T877">kan-bi</ta>
            <ta e="T879" id="Seg_661" s="T878">a</ta>
            <ta e="T880" id="Seg_662" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_663" s="T880">mĭnzər-bi</ta>
            <ta e="T882" id="Seg_664" s="T881">mĭje</ta>
            <ta e="T883" id="Seg_665" s="T882">i</ta>
            <ta e="T884" id="Seg_666" s="T883">kuvšin-Tə</ta>
            <ta e="T885" id="Seg_667" s="T884">kămnə-bi</ta>
            <ta e="T886" id="Seg_668" s="T885">bos-də</ta>
            <ta e="T887" id="Seg_669" s="T886">am-laʔbə</ta>
            <ta e="T888" id="Seg_670" s="T887">urgo</ta>
            <ta e="T889" id="Seg_671" s="T888">püje-t-ziʔ</ta>
            <ta e="T890" id="Seg_672" s="T889">a</ta>
            <ta e="T891" id="Seg_673" s="T890">dĭ</ta>
            <ta e="T892" id="Seg_674" s="T891">mĭn-gA</ta>
            <ta e="T893" id="Seg_675" s="T892">mĭn-gA</ta>
            <ta e="T894" id="Seg_676" s="T893">vʼezʼdʼe</ta>
            <ta e="T895" id="Seg_677" s="T894">ej</ta>
            <ta e="T896" id="Seg_678" s="T895">mo-liA</ta>
            <ta e="T897" id="Seg_679" s="T896">am-zittə</ta>
            <ta e="T898" id="Seg_680" s="T897">dĭgəttə</ta>
            <ta e="T899" id="Seg_681" s="T898">kan-lAʔ</ta>
            <ta e="T900" id="Seg_682" s="T899">tʼür-bi</ta>
            <ta e="T901" id="Seg_683" s="T900">maʔ-gəndə</ta>
            <ta e="T902" id="Seg_684" s="T901">i</ta>
            <ta e="T903" id="Seg_685" s="T902">suj</ta>
            <ta e="T904" id="Seg_686" s="T903">šində-n</ta>
            <ta e="T905" id="Seg_687" s="T904">dĭ</ta>
            <ta e="T906" id="Seg_688" s="T905">dĭ-Tə</ta>
            <ta e="T907" id="Seg_689" s="T906">ej</ta>
            <ta e="T908" id="Seg_690" s="T907">mĭn-liA</ta>
            <ta e="T909" id="Seg_691" s="T908">dĭ</ta>
            <ta e="T910" id="Seg_692" s="T909">dĭ-Tə</ta>
            <ta e="T911" id="Seg_693" s="T910">ej</ta>
            <ta e="T912" id="Seg_694" s="T911">mĭn-liA</ta>
            <ta e="T913" id="Seg_695" s="T912">i</ta>
            <ta e="T914" id="Seg_696" s="T913">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T832" id="Seg_697" s="T831">fox.[NOM.SG]</ta>
            <ta e="T833" id="Seg_698" s="T832">and</ta>
            <ta e="T834" id="Seg_699" s="T833">crane.[NOM.SG]</ta>
            <ta e="T835" id="Seg_700" s="T834">live-PST-3PL</ta>
            <ta e="T836" id="Seg_701" s="T835">two-COLL</ta>
            <ta e="T837" id="Seg_702" s="T836">fox.[NOM.SG]</ta>
            <ta e="T838" id="Seg_703" s="T837">crane-LAT</ta>
            <ta e="T839" id="Seg_704" s="T838">say-IPFVZ.[3SG]</ta>
            <ta e="T840" id="Seg_705" s="T839">come-IMP.2SG</ta>
            <ta e="T841" id="Seg_706" s="T840">I.LAT</ta>
            <ta e="T842" id="Seg_707" s="T841">village-VBLZ-CVB</ta>
            <ta e="T843" id="Seg_708" s="T842">I.NOM</ta>
            <ta e="T844" id="Seg_709" s="T843">you.DAT</ta>
            <ta e="T845" id="Seg_710" s="T844">boil-FUT-1SG</ta>
            <ta e="T846" id="Seg_711" s="T845">porridge.[NOM.SG]</ta>
            <ta e="T847" id="Seg_712" s="T846">eat-FUT-2SG</ta>
            <ta e="T848" id="Seg_713" s="T847">then</ta>
            <ta e="T849" id="Seg_714" s="T848">this.[NOM.SG]</ta>
            <ta e="T850" id="Seg_715" s="T849">pour-PST.[3SG]</ta>
            <ta e="T851" id="Seg_716" s="T850">plate-LAT</ta>
            <ta e="T852" id="Seg_717" s="T851">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T853" id="Seg_718" s="T852">eat-DUR.[3SG]</ta>
            <ta e="T854" id="Seg_719" s="T853">and</ta>
            <ta e="T855" id="Seg_720" s="T854">this-LAT</ta>
            <ta e="T856" id="Seg_721" s="T855">one.cannot</ta>
            <ta e="T857" id="Seg_722" s="T856">eat-INF.LAT</ta>
            <ta e="T858" id="Seg_723" s="T857">this-GEN</ta>
            <ta e="T859" id="Seg_724" s="T858">nose-NOM/GEN.3SG</ta>
            <ta e="T862" id="Seg_725" s="T861">long.[NOM.SG]</ta>
            <ta e="T863" id="Seg_726" s="T862">NEG</ta>
            <ta e="T864" id="Seg_727" s="T863">can-PST.[3SG]</ta>
            <ta e="T865" id="Seg_728" s="T864">then</ta>
            <ta e="T866" id="Seg_729" s="T865">say-IPFVZ.[3SG]</ta>
            <ta e="T867" id="Seg_730" s="T866">good.[NOM.SG]</ta>
            <ta e="T868" id="Seg_731" s="T867">I.NOM</ta>
            <ta e="T869" id="Seg_732" s="T868">go-FUT-1SG</ta>
            <ta e="T870" id="Seg_733" s="T869">tent-LAT-%%</ta>
            <ta e="T871" id="Seg_734" s="T870">come-IMP.2SG</ta>
            <ta e="T872" id="Seg_735" s="T871">I.LAT</ta>
            <ta e="T873" id="Seg_736" s="T872">village-VBLZ-CVB</ta>
            <ta e="T874" id="Seg_737" s="T873">then</ta>
            <ta e="T875" id="Seg_738" s="T874">tomorrow</ta>
            <ta e="T876" id="Seg_739" s="T875">fox.[NOM.SG]</ta>
            <ta e="T877" id="Seg_740" s="T876">this-LAT</ta>
            <ta e="T878" id="Seg_741" s="T877">go-PST.[3SG]</ta>
            <ta e="T879" id="Seg_742" s="T878">and</ta>
            <ta e="T880" id="Seg_743" s="T879">this.[NOM.SG]</ta>
            <ta e="T881" id="Seg_744" s="T880">boil-PST.[3SG]</ta>
            <ta e="T882" id="Seg_745" s="T881">soup.[NOM.SG]</ta>
            <ta e="T883" id="Seg_746" s="T882">and</ta>
            <ta e="T884" id="Seg_747" s="T883">jug-LAT</ta>
            <ta e="T885" id="Seg_748" s="T884">pour-PST.[3SG]</ta>
            <ta e="T886" id="Seg_749" s="T885">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T887" id="Seg_750" s="T886">eat-DUR.[3SG]</ta>
            <ta e="T888" id="Seg_751" s="T887">big.[NOM.SG]</ta>
            <ta e="T889" id="Seg_752" s="T888">nose-3SG-INS</ta>
            <ta e="T890" id="Seg_753" s="T889">and</ta>
            <ta e="T891" id="Seg_754" s="T890">this.[NOM.SG]</ta>
            <ta e="T892" id="Seg_755" s="T891">go-PRS.[3SG]</ta>
            <ta e="T893" id="Seg_756" s="T892">go-PRS.[3SG]</ta>
            <ta e="T894" id="Seg_757" s="T893">everywhere</ta>
            <ta e="T895" id="Seg_758" s="T894">NEG</ta>
            <ta e="T896" id="Seg_759" s="T895">can-PRS.[3SG]</ta>
            <ta e="T897" id="Seg_760" s="T896">eat-INF.LAT</ta>
            <ta e="T898" id="Seg_761" s="T897">then</ta>
            <ta e="T899" id="Seg_762" s="T898">go-CVB</ta>
            <ta e="T900" id="Seg_763" s="T899">disappear-PST.[3SG]</ta>
            <ta e="T901" id="Seg_764" s="T900">tent-LAT/LOC.3SG</ta>
            <ta e="T902" id="Seg_765" s="T901">and</ta>
            <ta e="T903" id="Seg_766" s="T902">%%</ta>
            <ta e="T904" id="Seg_767" s="T903">who-GEN</ta>
            <ta e="T905" id="Seg_768" s="T904">this.[NOM.SG]</ta>
            <ta e="T906" id="Seg_769" s="T905">this-LAT</ta>
            <ta e="T907" id="Seg_770" s="T906">NEG</ta>
            <ta e="T908" id="Seg_771" s="T907">go-PRS.[3SG]</ta>
            <ta e="T909" id="Seg_772" s="T908">this.[NOM.SG]</ta>
            <ta e="T910" id="Seg_773" s="T909">this-LAT</ta>
            <ta e="T911" id="Seg_774" s="T910">NEG</ta>
            <ta e="T912" id="Seg_775" s="T911">go-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_776" s="T912">and</ta>
            <ta e="T914" id="Seg_777" s="T913">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T832" id="Seg_778" s="T831">лиса.[NOM.SG]</ta>
            <ta e="T833" id="Seg_779" s="T832">и</ta>
            <ta e="T834" id="Seg_780" s="T833">журавль.[NOM.SG]</ta>
            <ta e="T835" id="Seg_781" s="T834">жить-PST-3PL</ta>
            <ta e="T836" id="Seg_782" s="T835">два-COLL</ta>
            <ta e="T837" id="Seg_783" s="T836">лиса.[NOM.SG]</ta>
            <ta e="T838" id="Seg_784" s="T837">журавль-LAT</ta>
            <ta e="T839" id="Seg_785" s="T838">сказать-IPFVZ.[3SG]</ta>
            <ta e="T840" id="Seg_786" s="T839">прийти-IMP.2SG</ta>
            <ta e="T841" id="Seg_787" s="T840">я.LAT</ta>
            <ta e="T842" id="Seg_788" s="T841">деревня-VBLZ-CVB</ta>
            <ta e="T843" id="Seg_789" s="T842">я.NOM</ta>
            <ta e="T844" id="Seg_790" s="T843">ты.DAT</ta>
            <ta e="T845" id="Seg_791" s="T844">кипятить-FUT-1SG</ta>
            <ta e="T846" id="Seg_792" s="T845">каша.[NOM.SG]</ta>
            <ta e="T847" id="Seg_793" s="T846">съесть-FUT-2SG</ta>
            <ta e="T848" id="Seg_794" s="T847">тогда</ta>
            <ta e="T849" id="Seg_795" s="T848">этот.[NOM.SG]</ta>
            <ta e="T850" id="Seg_796" s="T849">лить-PST.[3SG]</ta>
            <ta e="T851" id="Seg_797" s="T850">тарелка-LAT</ta>
            <ta e="T852" id="Seg_798" s="T851">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T853" id="Seg_799" s="T852">съесть-DUR.[3SG]</ta>
            <ta e="T854" id="Seg_800" s="T853">а</ta>
            <ta e="T855" id="Seg_801" s="T854">этот-LAT</ta>
            <ta e="T856" id="Seg_802" s="T855">нельзя</ta>
            <ta e="T857" id="Seg_803" s="T856">съесть-INF.LAT</ta>
            <ta e="T858" id="Seg_804" s="T857">этот-GEN</ta>
            <ta e="T859" id="Seg_805" s="T858">нос-NOM/GEN.3SG</ta>
            <ta e="T862" id="Seg_806" s="T861">длинный.[NOM.SG]</ta>
            <ta e="T863" id="Seg_807" s="T862">NEG</ta>
            <ta e="T864" id="Seg_808" s="T863">мочь-PST.[3SG]</ta>
            <ta e="T865" id="Seg_809" s="T864">тогда</ta>
            <ta e="T866" id="Seg_810" s="T865">сказать-IPFVZ.[3SG]</ta>
            <ta e="T867" id="Seg_811" s="T866">хороший.[NOM.SG]</ta>
            <ta e="T868" id="Seg_812" s="T867">я.NOM</ta>
            <ta e="T869" id="Seg_813" s="T868">пойти-FUT-1SG</ta>
            <ta e="T870" id="Seg_814" s="T869">чум-LAT-%%</ta>
            <ta e="T871" id="Seg_815" s="T870">прийти-IMP.2SG</ta>
            <ta e="T872" id="Seg_816" s="T871">я.LAT</ta>
            <ta e="T873" id="Seg_817" s="T872">деревня-VBLZ-CVB</ta>
            <ta e="T874" id="Seg_818" s="T873">тогда</ta>
            <ta e="T875" id="Seg_819" s="T874">завтра</ta>
            <ta e="T876" id="Seg_820" s="T875">лисица.[NOM.SG]</ta>
            <ta e="T877" id="Seg_821" s="T876">этот-LAT</ta>
            <ta e="T878" id="Seg_822" s="T877">пойти-PST.[3SG]</ta>
            <ta e="T879" id="Seg_823" s="T878">а</ta>
            <ta e="T880" id="Seg_824" s="T879">этот.[NOM.SG]</ta>
            <ta e="T881" id="Seg_825" s="T880">кипятить-PST.[3SG]</ta>
            <ta e="T882" id="Seg_826" s="T881">суп.[NOM.SG]</ta>
            <ta e="T883" id="Seg_827" s="T882">и</ta>
            <ta e="T884" id="Seg_828" s="T883">кувшин-LAT</ta>
            <ta e="T885" id="Seg_829" s="T884">лить-PST.[3SG]</ta>
            <ta e="T886" id="Seg_830" s="T885">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T887" id="Seg_831" s="T886">съесть-DUR.[3SG]</ta>
            <ta e="T888" id="Seg_832" s="T887">большой.[NOM.SG]</ta>
            <ta e="T889" id="Seg_833" s="T888">нос-3SG-INS</ta>
            <ta e="T890" id="Seg_834" s="T889">а</ta>
            <ta e="T891" id="Seg_835" s="T890">этот.[NOM.SG]</ta>
            <ta e="T892" id="Seg_836" s="T891">идти-PRS.[3SG]</ta>
            <ta e="T893" id="Seg_837" s="T892">идти-PRS.[3SG]</ta>
            <ta e="T894" id="Seg_838" s="T893">везде</ta>
            <ta e="T895" id="Seg_839" s="T894">NEG</ta>
            <ta e="T896" id="Seg_840" s="T895">мочь-PRS.[3SG]</ta>
            <ta e="T897" id="Seg_841" s="T896">съесть-INF.LAT</ta>
            <ta e="T898" id="Seg_842" s="T897">тогда</ta>
            <ta e="T899" id="Seg_843" s="T898">пойти-CVB</ta>
            <ta e="T900" id="Seg_844" s="T899">исчезнуть-PST.[3SG]</ta>
            <ta e="T901" id="Seg_845" s="T900">чум-LAT/LOC.3SG</ta>
            <ta e="T902" id="Seg_846" s="T901">и</ta>
            <ta e="T903" id="Seg_847" s="T902">%%</ta>
            <ta e="T904" id="Seg_848" s="T903">кто-GEN</ta>
            <ta e="T905" id="Seg_849" s="T904">этот.[NOM.SG]</ta>
            <ta e="T906" id="Seg_850" s="T905">этот-LAT</ta>
            <ta e="T907" id="Seg_851" s="T906">NEG</ta>
            <ta e="T908" id="Seg_852" s="T907">идти-PRS.[3SG]</ta>
            <ta e="T909" id="Seg_853" s="T908">этот.[NOM.SG]</ta>
            <ta e="T910" id="Seg_854" s="T909">этот-LAT</ta>
            <ta e="T911" id="Seg_855" s="T910">NEG</ta>
            <ta e="T912" id="Seg_856" s="T911">идти-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_857" s="T912">и</ta>
            <ta e="T914" id="Seg_858" s="T913">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T832" id="Seg_859" s="T831">n-n:case</ta>
            <ta e="T833" id="Seg_860" s="T832">conj</ta>
            <ta e="T834" id="Seg_861" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_862" s="T834">v-v:tense-v:pn</ta>
            <ta e="T836" id="Seg_863" s="T835">num-num&gt;num</ta>
            <ta e="T837" id="Seg_864" s="T836">n-n:case</ta>
            <ta e="T838" id="Seg_865" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_866" s="T838">v-v&gt;v-v:pn</ta>
            <ta e="T840" id="Seg_867" s="T839">v-v:mood.pn</ta>
            <ta e="T841" id="Seg_868" s="T840">pers</ta>
            <ta e="T842" id="Seg_869" s="T841">n-n&gt;v-v:n.fin</ta>
            <ta e="T843" id="Seg_870" s="T842">pers</ta>
            <ta e="T844" id="Seg_871" s="T843">pers</ta>
            <ta e="T845" id="Seg_872" s="T844">v-v:tense-v:pn</ta>
            <ta e="T846" id="Seg_873" s="T845">n-n:case</ta>
            <ta e="T847" id="Seg_874" s="T846">v-v:tense-v:pn</ta>
            <ta e="T848" id="Seg_875" s="T847">adv</ta>
            <ta e="T849" id="Seg_876" s="T848">dempro-n:case</ta>
            <ta e="T850" id="Seg_877" s="T849">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_878" s="T850">n-n:case</ta>
            <ta e="T852" id="Seg_879" s="T851">refl-n:case.poss</ta>
            <ta e="T853" id="Seg_880" s="T852">v-v&gt;v-v:pn</ta>
            <ta e="T854" id="Seg_881" s="T853">conj</ta>
            <ta e="T855" id="Seg_882" s="T854">dempro-n:case</ta>
            <ta e="T856" id="Seg_883" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_884" s="T856">v-v:n.fin</ta>
            <ta e="T858" id="Seg_885" s="T857">dempro-n:case</ta>
            <ta e="T859" id="Seg_886" s="T858">n-n:case.poss</ta>
            <ta e="T862" id="Seg_887" s="T861">adj-n:case</ta>
            <ta e="T863" id="Seg_888" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_889" s="T863">v-v:tense-v:pn</ta>
            <ta e="T865" id="Seg_890" s="T864">adv</ta>
            <ta e="T866" id="Seg_891" s="T865">v-v&gt;v-v:pn</ta>
            <ta e="T867" id="Seg_892" s="T866">adj-n:case</ta>
            <ta e="T868" id="Seg_893" s="T867">pers</ta>
            <ta e="T869" id="Seg_894" s="T868">v-v:tense-v:pn</ta>
            <ta e="T870" id="Seg_895" s="T869">n-n:case-%%</ta>
            <ta e="T871" id="Seg_896" s="T870">v-v:mood.pn</ta>
            <ta e="T872" id="Seg_897" s="T871">pers</ta>
            <ta e="T873" id="Seg_898" s="T872">n-n&gt;v-v:n.fin</ta>
            <ta e="T874" id="Seg_899" s="T873">adv</ta>
            <ta e="T875" id="Seg_900" s="T874">adv</ta>
            <ta e="T876" id="Seg_901" s="T875">n-n:case</ta>
            <ta e="T877" id="Seg_902" s="T876">dempro-n:case</ta>
            <ta e="T878" id="Seg_903" s="T877">v-v:tense-v:pn</ta>
            <ta e="T879" id="Seg_904" s="T878">conj</ta>
            <ta e="T880" id="Seg_905" s="T879">dempro-n:case</ta>
            <ta e="T881" id="Seg_906" s="T880">v-v:tense-v:pn</ta>
            <ta e="T882" id="Seg_907" s="T881">n-n:case</ta>
            <ta e="T883" id="Seg_908" s="T882">conj</ta>
            <ta e="T884" id="Seg_909" s="T883">n-n:case</ta>
            <ta e="T885" id="Seg_910" s="T884">v-v:tense-v:pn</ta>
            <ta e="T886" id="Seg_911" s="T885">refl-n:case.poss</ta>
            <ta e="T887" id="Seg_912" s="T886">v-v&gt;v-v:pn</ta>
            <ta e="T888" id="Seg_913" s="T887">adj-n:case</ta>
            <ta e="T889" id="Seg_914" s="T888">n-n:case.poss-n:case</ta>
            <ta e="T890" id="Seg_915" s="T889">conj</ta>
            <ta e="T891" id="Seg_916" s="T890">dempro-n:case</ta>
            <ta e="T892" id="Seg_917" s="T891">v-v:tense-v:pn</ta>
            <ta e="T893" id="Seg_918" s="T892">v-v:tense-v:pn</ta>
            <ta e="T894" id="Seg_919" s="T893">adv</ta>
            <ta e="T895" id="Seg_920" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_921" s="T895">v-v:tense-v:pn</ta>
            <ta e="T897" id="Seg_922" s="T896">v-v:n.fin</ta>
            <ta e="T898" id="Seg_923" s="T897">adv</ta>
            <ta e="T899" id="Seg_924" s="T898">v-v:n.fin</ta>
            <ta e="T900" id="Seg_925" s="T899">v-v:tense-v:pn</ta>
            <ta e="T901" id="Seg_926" s="T900">n-n:case.poss</ta>
            <ta e="T902" id="Seg_927" s="T901">conj</ta>
            <ta e="T903" id="Seg_928" s="T902">%%</ta>
            <ta e="T904" id="Seg_929" s="T903">que-n:case</ta>
            <ta e="T905" id="Seg_930" s="T904">dempro-n:case</ta>
            <ta e="T906" id="Seg_931" s="T905">dempro-n:case</ta>
            <ta e="T907" id="Seg_932" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_933" s="T907">v-v:tense-v:pn</ta>
            <ta e="T909" id="Seg_934" s="T908">dempro-n:case</ta>
            <ta e="T910" id="Seg_935" s="T909">dempro-n:case</ta>
            <ta e="T911" id="Seg_936" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_937" s="T911">v-v:tense-v:pn</ta>
            <ta e="T913" id="Seg_938" s="T912">conj</ta>
            <ta e="T914" id="Seg_939" s="T913">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T832" id="Seg_940" s="T831">n</ta>
            <ta e="T833" id="Seg_941" s="T832">conj</ta>
            <ta e="T834" id="Seg_942" s="T833">n</ta>
            <ta e="T835" id="Seg_943" s="T834">v</ta>
            <ta e="T836" id="Seg_944" s="T835">num</ta>
            <ta e="T837" id="Seg_945" s="T836">n</ta>
            <ta e="T838" id="Seg_946" s="T837">n</ta>
            <ta e="T839" id="Seg_947" s="T838">v</ta>
            <ta e="T840" id="Seg_948" s="T839">v</ta>
            <ta e="T841" id="Seg_949" s="T840">pers</ta>
            <ta e="T842" id="Seg_950" s="T841">v</ta>
            <ta e="T843" id="Seg_951" s="T842">pers</ta>
            <ta e="T844" id="Seg_952" s="T843">pers</ta>
            <ta e="T845" id="Seg_953" s="T844">v</ta>
            <ta e="T846" id="Seg_954" s="T845">n</ta>
            <ta e="T847" id="Seg_955" s="T846">v</ta>
            <ta e="T848" id="Seg_956" s="T847">adv</ta>
            <ta e="T849" id="Seg_957" s="T848">dempro</ta>
            <ta e="T850" id="Seg_958" s="T849">v</ta>
            <ta e="T851" id="Seg_959" s="T850">n</ta>
            <ta e="T852" id="Seg_960" s="T851">refl</ta>
            <ta e="T853" id="Seg_961" s="T852">v</ta>
            <ta e="T854" id="Seg_962" s="T853">conj</ta>
            <ta e="T855" id="Seg_963" s="T854">dempro</ta>
            <ta e="T856" id="Seg_964" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_965" s="T856">v</ta>
            <ta e="T858" id="Seg_966" s="T857">dempro</ta>
            <ta e="T859" id="Seg_967" s="T858">n</ta>
            <ta e="T862" id="Seg_968" s="T861">adj</ta>
            <ta e="T863" id="Seg_969" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_970" s="T863">v</ta>
            <ta e="T865" id="Seg_971" s="T864">adv</ta>
            <ta e="T866" id="Seg_972" s="T865">v</ta>
            <ta e="T867" id="Seg_973" s="T866">adj</ta>
            <ta e="T868" id="Seg_974" s="T867">pers</ta>
            <ta e="T869" id="Seg_975" s="T868">v</ta>
            <ta e="T870" id="Seg_976" s="T869">n</ta>
            <ta e="T871" id="Seg_977" s="T870">v</ta>
            <ta e="T872" id="Seg_978" s="T871">pers</ta>
            <ta e="T873" id="Seg_979" s="T872">v</ta>
            <ta e="T874" id="Seg_980" s="T873">adv</ta>
            <ta e="T875" id="Seg_981" s="T874">adv</ta>
            <ta e="T876" id="Seg_982" s="T875">n</ta>
            <ta e="T877" id="Seg_983" s="T876">dempro</ta>
            <ta e="T878" id="Seg_984" s="T877">v</ta>
            <ta e="T879" id="Seg_985" s="T878">conj</ta>
            <ta e="T880" id="Seg_986" s="T879">dempro</ta>
            <ta e="T881" id="Seg_987" s="T880">v</ta>
            <ta e="T882" id="Seg_988" s="T881">n</ta>
            <ta e="T883" id="Seg_989" s="T882">conj</ta>
            <ta e="T884" id="Seg_990" s="T883">n</ta>
            <ta e="T885" id="Seg_991" s="T884">v</ta>
            <ta e="T886" id="Seg_992" s="T885">refl</ta>
            <ta e="T887" id="Seg_993" s="T886">v</ta>
            <ta e="T888" id="Seg_994" s="T887">adj</ta>
            <ta e="T889" id="Seg_995" s="T888">n</ta>
            <ta e="T890" id="Seg_996" s="T889">conj</ta>
            <ta e="T891" id="Seg_997" s="T890">dempro</ta>
            <ta e="T892" id="Seg_998" s="T891">v</ta>
            <ta e="T893" id="Seg_999" s="T892">v</ta>
            <ta e="T894" id="Seg_1000" s="T893">adv</ta>
            <ta e="T895" id="Seg_1001" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_1002" s="T895">v</ta>
            <ta e="T897" id="Seg_1003" s="T896">v</ta>
            <ta e="T898" id="Seg_1004" s="T897">adv</ta>
            <ta e="T899" id="Seg_1005" s="T898">v</ta>
            <ta e="T900" id="Seg_1006" s="T899">v</ta>
            <ta e="T901" id="Seg_1007" s="T900">n</ta>
            <ta e="T902" id="Seg_1008" s="T901">conj</ta>
            <ta e="T904" id="Seg_1009" s="T903">que</ta>
            <ta e="T905" id="Seg_1010" s="T904">dempro</ta>
            <ta e="T906" id="Seg_1011" s="T905">dempro</ta>
            <ta e="T907" id="Seg_1012" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_1013" s="T907">v</ta>
            <ta e="T909" id="Seg_1014" s="T908">dempro</ta>
            <ta e="T910" id="Seg_1015" s="T909">dempro</ta>
            <ta e="T911" id="Seg_1016" s="T910">ptcl</ta>
            <ta e="T912" id="Seg_1017" s="T911">v</ta>
            <ta e="T913" id="Seg_1018" s="T912">conj</ta>
            <ta e="T914" id="Seg_1019" s="T913">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T832" id="Seg_1020" s="T831">np.h:E</ta>
            <ta e="T834" id="Seg_1021" s="T833">np.h:E</ta>
            <ta e="T837" id="Seg_1022" s="T836">np.h:A</ta>
            <ta e="T838" id="Seg_1023" s="T837">np.h:R</ta>
            <ta e="T840" id="Seg_1024" s="T839">0.2.h:A</ta>
            <ta e="T841" id="Seg_1025" s="T840">pro:G</ta>
            <ta e="T843" id="Seg_1026" s="T842">pro.h:A</ta>
            <ta e="T844" id="Seg_1027" s="T843">pro.h:B</ta>
            <ta e="T845" id="Seg_1028" s="T844">np:P</ta>
            <ta e="T846" id="Seg_1029" s="T845">np:P</ta>
            <ta e="T847" id="Seg_1030" s="T846">0.2.h:A</ta>
            <ta e="T848" id="Seg_1031" s="T847">adv:Time</ta>
            <ta e="T849" id="Seg_1032" s="T848">pro.h:A</ta>
            <ta e="T851" id="Seg_1033" s="T850">np:G</ta>
            <ta e="T852" id="Seg_1034" s="T851">pro.h:A</ta>
            <ta e="T858" id="Seg_1035" s="T857">pro.h:Poss</ta>
            <ta e="T859" id="Seg_1036" s="T858">np:Th</ta>
            <ta e="T864" id="Seg_1037" s="T863">0.3.h:A</ta>
            <ta e="T865" id="Seg_1038" s="T864">adv:Time</ta>
            <ta e="T866" id="Seg_1039" s="T865">0.3.h:A</ta>
            <ta e="T868" id="Seg_1040" s="T867">pro.h:A</ta>
            <ta e="T870" id="Seg_1041" s="T869">np:G</ta>
            <ta e="T871" id="Seg_1042" s="T870">0.2.h:A</ta>
            <ta e="T872" id="Seg_1043" s="T871">pro:G</ta>
            <ta e="T874" id="Seg_1044" s="T873">adv:Time</ta>
            <ta e="T875" id="Seg_1045" s="T874">adv:Time</ta>
            <ta e="T876" id="Seg_1046" s="T875">np.h:A</ta>
            <ta e="T877" id="Seg_1047" s="T876">pro:G</ta>
            <ta e="T880" id="Seg_1048" s="T879">pro.h:A</ta>
            <ta e="T882" id="Seg_1049" s="T881">np:P</ta>
            <ta e="T884" id="Seg_1050" s="T883">np:G</ta>
            <ta e="T885" id="Seg_1051" s="T884">0.3.h:A</ta>
            <ta e="T886" id="Seg_1052" s="T885">pro.h:A</ta>
            <ta e="T889" id="Seg_1053" s="T888">np:Ins</ta>
            <ta e="T891" id="Seg_1054" s="T890">pro.h:A</ta>
            <ta e="T893" id="Seg_1055" s="T892">0.3.h:A</ta>
            <ta e="T894" id="Seg_1056" s="T893">adv:L</ta>
            <ta e="T896" id="Seg_1057" s="T895">0.3.h:A</ta>
            <ta e="T898" id="Seg_1058" s="T897">adv:Time</ta>
            <ta e="T900" id="Seg_1059" s="T899">0.3.h:A</ta>
            <ta e="T901" id="Seg_1060" s="T900">np:G</ta>
            <ta e="T904" id="Seg_1061" s="T903">pro.h:Poss</ta>
            <ta e="T905" id="Seg_1062" s="T904">pro.h:A</ta>
            <ta e="T906" id="Seg_1063" s="T905">pro:G</ta>
            <ta e="T909" id="Seg_1064" s="T908">pro.h:A</ta>
            <ta e="T910" id="Seg_1065" s="T909">pro:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T832" id="Seg_1066" s="T831">np.h:S</ta>
            <ta e="T834" id="Seg_1067" s="T833">np.h:S</ta>
            <ta e="T835" id="Seg_1068" s="T834">v:pred</ta>
            <ta e="T837" id="Seg_1069" s="T836">np.h:S</ta>
            <ta e="T839" id="Seg_1070" s="T838">v:pred</ta>
            <ta e="T840" id="Seg_1071" s="T839">v:pred 0.2.h:S</ta>
            <ta e="T843" id="Seg_1072" s="T842">pro.h:S</ta>
            <ta e="T845" id="Seg_1073" s="T844">v:pred</ta>
            <ta e="T846" id="Seg_1074" s="T845">np:O</ta>
            <ta e="T847" id="Seg_1075" s="T846">v:pred 0.2.h:S</ta>
            <ta e="T849" id="Seg_1076" s="T848">pro.h:S</ta>
            <ta e="T850" id="Seg_1077" s="T849">v:pred</ta>
            <ta e="T852" id="Seg_1078" s="T851">pro.h:S</ta>
            <ta e="T853" id="Seg_1079" s="T852">v:pred</ta>
            <ta e="T856" id="Seg_1080" s="T855">ptcl:pred</ta>
            <ta e="T859" id="Seg_1081" s="T858">np:S</ta>
            <ta e="T862" id="Seg_1082" s="T861">adj:pred</ta>
            <ta e="T863" id="Seg_1083" s="T862">ptcl.neg</ta>
            <ta e="T864" id="Seg_1084" s="T863">v:pred 0.3.h:S</ta>
            <ta e="T866" id="Seg_1085" s="T865">v:pred 0.3.h:S</ta>
            <ta e="T868" id="Seg_1086" s="T867">pro.h:S</ta>
            <ta e="T869" id="Seg_1087" s="T868">v:pred</ta>
            <ta e="T871" id="Seg_1088" s="T870">v:pred 0.2.h:S</ta>
            <ta e="T876" id="Seg_1089" s="T875">np.h:S</ta>
            <ta e="T878" id="Seg_1090" s="T877">v:pred</ta>
            <ta e="T880" id="Seg_1091" s="T879">pro.h:S</ta>
            <ta e="T881" id="Seg_1092" s="T880">v:pred</ta>
            <ta e="T882" id="Seg_1093" s="T881">np:O</ta>
            <ta e="T885" id="Seg_1094" s="T884">v:pred 0.3.h:S</ta>
            <ta e="T886" id="Seg_1095" s="T885">pro.h:S</ta>
            <ta e="T887" id="Seg_1096" s="T886">v:pred</ta>
            <ta e="T891" id="Seg_1097" s="T890">pro.h:S</ta>
            <ta e="T892" id="Seg_1098" s="T891">v:pred</ta>
            <ta e="T893" id="Seg_1099" s="T892">v:pred 0.3.h:S</ta>
            <ta e="T895" id="Seg_1100" s="T894">ptcl.neg</ta>
            <ta e="T896" id="Seg_1101" s="T895">v:pred 0.3.h:S</ta>
            <ta e="T899" id="Seg_1102" s="T898">conv:pred</ta>
            <ta e="T900" id="Seg_1103" s="T899">v:pred 0.3.h:S</ta>
            <ta e="T905" id="Seg_1104" s="T904">pro.h:S</ta>
            <ta e="T907" id="Seg_1105" s="T906">ptcl.neg</ta>
            <ta e="T908" id="Seg_1106" s="T907">v:pred</ta>
            <ta e="T909" id="Seg_1107" s="T908">pro.h:S</ta>
            <ta e="T911" id="Seg_1108" s="T910">ptcl.neg</ta>
            <ta e="T912" id="Seg_1109" s="T911">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T832" id="Seg_1110" s="T831">RUS:cult</ta>
            <ta e="T833" id="Seg_1111" s="T832">RUS:gram</ta>
            <ta e="T834" id="Seg_1112" s="T833">RUS:core</ta>
            <ta e="T837" id="Seg_1113" s="T836">RUS:cult</ta>
            <ta e="T838" id="Seg_1114" s="T837">RUS:core</ta>
            <ta e="T846" id="Seg_1115" s="T845">RUS:cult</ta>
            <ta e="T851" id="Seg_1116" s="T850">RUS:cult</ta>
            <ta e="T854" id="Seg_1117" s="T853">RUS:gram</ta>
            <ta e="T856" id="Seg_1118" s="T855">RUS:mod</ta>
            <ta e="T867" id="Seg_1119" s="T866">TURK:core</ta>
            <ta e="T876" id="Seg_1120" s="T875">RUS:cult</ta>
            <ta e="T879" id="Seg_1121" s="T878">RUS:gram</ta>
            <ta e="T883" id="Seg_1122" s="T882">RUS:gram</ta>
            <ta e="T884" id="Seg_1123" s="T883">RUS:cult</ta>
            <ta e="T890" id="Seg_1124" s="T889">RUS:gram</ta>
            <ta e="T894" id="Seg_1125" s="T893">RUS:core</ta>
            <ta e="T902" id="Seg_1126" s="T901">RUS:gram</ta>
            <ta e="T913" id="Seg_1127" s="T912">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T884" id="Seg_1128" s="T883">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T832" id="Seg_1129" s="T831">dir:bare</ta>
            <ta e="T834" id="Seg_1130" s="T833">dir:bare</ta>
            <ta e="T837" id="Seg_1131" s="T836">dir:bare</ta>
            <ta e="T846" id="Seg_1132" s="T845">dir:bare</ta>
            <ta e="T851" id="Seg_1133" s="T850">dir:infl</ta>
            <ta e="T876" id="Seg_1134" s="T875">dir:bare</ta>
            <ta e="T884" id="Seg_1135" s="T883">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T836" id="Seg_1136" s="T831">Жили вдвоём лиса и журавль.</ta>
            <ta e="T845" id="Seg_1137" s="T836">Лиса говорит журавлю: «Приходи ко мне в гости, я тебе приготовлю [еды].</ta>
            <ta e="T847" id="Seg_1138" s="T845">Каши поешь!»</ta>
            <ta e="T851" id="Seg_1139" s="T847">И налила в тарелку.</ta>
            <ta e="T857" id="Seg_1140" s="T851">Сама ест, а [журавль] не может есть.</ta>
            <ta e="T864" id="Seg_1141" s="T857">У него клюв длинный, он не может.</ta>
            <ta e="T866" id="Seg_1142" s="T864">Потом он говорит: </ta>
            <ta e="T870" id="Seg_1143" s="T866">«Хорошо, я пойду домой.</ta>
            <ta e="T873" id="Seg_1144" s="T870">Приходи ко мне в гости!»</ta>
            <ta e="T878" id="Seg_1145" s="T873">Потом на другой день лисица к нему приходит.</ta>
            <ta e="T882" id="Seg_1146" s="T878">А он сварил суп.</ta>
            <ta e="T885" id="Seg_1147" s="T882">И налил в кувшин.</ta>
            <ta e="T887" id="Seg_1148" s="T885">Сам ест.</ta>
            <ta e="T889" id="Seg_1149" s="T887">Своим большим носом [=клювом].</ta>
            <ta e="T897" id="Seg_1150" s="T889">А [лиса] ходит, ходит везде [со всех сторон], не может есть.</ta>
            <ta e="T901" id="Seg_1151" s="T897">Потом пошла домой.</ta>
            <ta e="T912" id="Seg_1152" s="T901">И чей (?), она к нему не ходит, он к ней не ходит.</ta>
            <ta e="T914" id="Seg_1153" s="T912">И хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T836" id="Seg_1154" s="T831">A fox and a crane were living together.</ta>
            <ta e="T845" id="Seg_1155" s="T836">The fox told the stork: "Come to visit me, I will cook for you!</ta>
            <ta e="T847" id="Seg_1156" s="T845">You will eat porridge!"</ta>
            <ta e="T851" id="Seg_1157" s="T847">Then he poured [it] into a plate.</ta>
            <ta e="T857" id="Seg_1158" s="T851">He himself is eating but [the crane] cannot eat.</ta>
            <ta e="T864" id="Seg_1159" s="T857">His nose [=beak] is long, he could not.</ta>
            <ta e="T866" id="Seg_1160" s="T864">Then he says:</ta>
            <ta e="T870" id="Seg_1161" s="T866">"Well, I will go home.</ta>
            <ta e="T873" id="Seg_1162" s="T870">Come to visit me!"</ta>
            <ta e="T878" id="Seg_1163" s="T873">Then the next day the fox went to him.</ta>
            <ta e="T882" id="Seg_1164" s="T878">And he cooked soup.</ta>
            <ta e="T885" id="Seg_1165" s="T882">And poured it into a jug.</ta>
            <ta e="T887" id="Seg_1166" s="T885">He himself is eating.</ta>
            <ta e="T889" id="Seg_1167" s="T887">With its big nose [=beak].</ta>
            <ta e="T897" id="Seg_1168" s="T889">But [the fox] comes, comes everywhere [from every side], he cannot eat.</ta>
            <ta e="T901" id="Seg_1169" s="T897">Then he went home.</ta>
            <ta e="T912" id="Seg_1170" s="T901">And whose (?), he [the fox] does not go to visit him [the crane], [and] he [the crane] does not go to visit him [the fox].</ta>
            <ta e="T914" id="Seg_1171" s="T912">And that's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T836" id="Seg_1172" s="T831">Ein Fuchs und ein Kranich lebten zusammen.</ta>
            <ta e="T845" id="Seg_1173" s="T836">Der Fuchs sagte dem Storch: „Komm mich besuchen, ich koche für dich!</ta>
            <ta e="T847" id="Seg_1174" s="T845">Du wirst Brei essen!“</ta>
            <ta e="T851" id="Seg_1175" s="T847">Dann schüttete er es auf einen Teller.</ta>
            <ta e="T857" id="Seg_1176" s="T851">Er selbst isst aber er [der Kranich] kann nicht essen.</ta>
            <ta e="T864" id="Seg_1177" s="T857">Seine Nase [=Schabel] ist lang, er konnte nicht.</ta>
            <ta e="T866" id="Seg_1178" s="T864">Dann sagt er:</ta>
            <ta e="T870" id="Seg_1179" s="T866">„Also, ich gehe nach Hause.</ta>
            <ta e="T873" id="Seg_1180" s="T870">Komm mich besuchen!“</ta>
            <ta e="T878" id="Seg_1181" s="T873">Dann am nächsten Tag ging der Fuchs zu ihm.</ta>
            <ta e="T882" id="Seg_1182" s="T878">Und er kochte Suppe.</ta>
            <ta e="T885" id="Seg_1183" s="T882">Und schüttete sie in eine Kanne.</ta>
            <ta e="T887" id="Seg_1184" s="T885">Selber isst er.</ta>
            <ta e="T889" id="Seg_1185" s="T887">Mit seiner großen Nase [=Schnabel].</ta>
            <ta e="T897" id="Seg_1186" s="T889">Aber [der Fuchs] kommt, kommt überall [von jeder Seite], er kann nicht essen.</ta>
            <ta e="T901" id="Seg_1187" s="T897">Dann ging er nach Hause.</ta>
            <ta e="T912" id="Seg_1188" s="T901">Und wessen (?), er [der Fuchs] geht nicht zu ihm [der Kranich], [und] er [der Kranich] geht nicht zu ihm [der Fuchs].</ta>
            <ta e="T914" id="Seg_1189" s="T912">Und das ist alles.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T836" id="Seg_1190" s="T831">[GVY:] http://narodstory.net/russkie-skazki.php?id=37</ta>
            <ta e="T857" id="Seg_1191" s="T851">[GVY:] Note that Russian нельзя is only used deontically, meaning "it is not allowed". Here, it is used as a root - at least internal - (im)possibility.</ta>
            <ta e="T885" id="Seg_1192" s="T882">[GVY:] kukšin = Ru. kuvšin 'jug'; k instead of v is probably occasional.</ta>
            <ta e="T912" id="Seg_1193" s="T901">[GVY:] suj = tüj?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
