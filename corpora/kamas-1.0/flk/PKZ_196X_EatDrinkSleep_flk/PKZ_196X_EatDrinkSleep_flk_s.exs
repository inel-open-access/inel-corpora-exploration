<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID44BC7F18-9CCF-A5BC-5E63-79780EE36AEC">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_EatDrinkSleep_flk.wav" />
         <referenced-file url="PKZ_196X_EatDrinkSleep_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_EatDrinkSleep_flk\PKZ_196X_EatDrinkSleep_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">50</ud-information>
            <ud-information attribute-name="# HIAT:w">34</ud-information>
            <ud-information attribute-name="# e">34</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.062" type="appl" />
         <tli id="T1" time="1.101" type="appl" />
         <tli id="T2" time="2.14" type="appl" />
         <tli id="T3" time="3.179" type="appl" />
         <tli id="T4" time="4.218" type="appl" />
         <tli id="T5" time="5.257" type="appl" />
         <tli id="T6" time="5.783" type="appl" />
         <tli id="T7" time="6.31" type="appl" />
         <tli id="T8" time="6.836" type="appl" />
         <tli id="T9" time="7.362" type="appl" />
         <tli id="T10" time="7.888" type="appl" />
         <tli id="T11" time="8.414" type="appl" />
         <tli id="T12" time="8.941" type="appl" />
         <tli id="T13" time="9.467" type="appl" />
         <tli id="T14" time="10.03" type="appl" />
         <tli id="T15" time="10.593" type="appl" />
         <tli id="T16" time="11.156" type="appl" />
         <tli id="T17" time="11.718" type="appl" />
         <tli id="T18" time="12.281" type="appl" />
         <tli id="T19" time="13.3529800528166" />
         <tli id="T20" time="14.276" type="appl" />
         <tli id="T21" time="15.17" type="appl" />
         <tli id="T22" time="16.064" type="appl" />
         <tli id="T23" time="16.959" type="appl" />
         <tli id="T24" time="17.853" type="appl" />
         <tli id="T25" time="18.747" type="appl" />
         <tli id="T26" time="19.261" type="appl" />
         <tli id="T27" time="19.776" type="appl" />
         <tli id="T28" time="20.29" type="appl" />
         <tli id="T29" time="20.804" type="appl" />
         <tli id="T30" time="21.432" type="appl" />
         <tli id="T31" time="22.06" type="appl" />
         <tli id="T32" time="22.687" type="appl" />
         <tli id="T33" time="23.36604846985631" />
         <tli id="T34" time="24.386" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T34" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">mămbi:</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ĭmbim</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ajirzittə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_20" n="HIAT:u" s="T5">
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Dĭ</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">ĭmbi</ts>
                  <nts id="Seg_27" n="HIAT:ip">)</nts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">amzittə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">naga</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">dak</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">tože</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">ej</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">jakšə</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_51" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">Bü</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">bĭssittə</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">tože</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">naga</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">ej</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">jakšə</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">kunolzittə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">iššo</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">iʔgö</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">nada</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">kunolzittə</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_95" n="HIAT:u" s="T25">
                  <nts id="Seg_96" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Ku-</ts>
                  <nts id="Seg_99" n="HIAT:ip">)</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Kumen</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">ej</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">kunolaʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_112" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_114" n="HIAT:w" s="T29">A</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">kunollal</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">sʼo</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">ravno</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_128" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">Kabarləj</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T34" id="Seg_133" n="sc" s="T0">
               <ts e="T1" id="Seg_135" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_137" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_139" n="e" s="T2">mămbi: </ts>
               <ts e="T4" id="Seg_141" n="e" s="T3">ĭmbim </ts>
               <ts e="T5" id="Seg_143" n="e" s="T4">ajirzittə. </ts>
               <ts e="T6" id="Seg_145" n="e" s="T5">(Dĭ </ts>
               <ts e="T7" id="Seg_147" n="e" s="T6">ĭmbi), </ts>
               <ts e="T8" id="Seg_149" n="e" s="T7">amzittə </ts>
               <ts e="T9" id="Seg_151" n="e" s="T8">naga </ts>
               <ts e="T10" id="Seg_153" n="e" s="T9">dak, </ts>
               <ts e="T11" id="Seg_155" n="e" s="T10">tože </ts>
               <ts e="T12" id="Seg_157" n="e" s="T11">ej </ts>
               <ts e="T13" id="Seg_159" n="e" s="T12">jakšə. </ts>
               <ts e="T14" id="Seg_161" n="e" s="T13">Bü </ts>
               <ts e="T15" id="Seg_163" n="e" s="T14">bĭssittə </ts>
               <ts e="T16" id="Seg_165" n="e" s="T15">tože </ts>
               <ts e="T17" id="Seg_167" n="e" s="T16">naga, </ts>
               <ts e="T18" id="Seg_169" n="e" s="T17">ej </ts>
               <ts e="T19" id="Seg_171" n="e" s="T18">jakšə. </ts>
               <ts e="T20" id="Seg_173" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_175" n="e" s="T20">kunolzittə </ts>
               <ts e="T22" id="Seg_177" n="e" s="T21">iššo, </ts>
               <ts e="T23" id="Seg_179" n="e" s="T22">iʔgö </ts>
               <ts e="T24" id="Seg_181" n="e" s="T23">nada </ts>
               <ts e="T25" id="Seg_183" n="e" s="T24">kunolzittə. </ts>
               <ts e="T26" id="Seg_185" n="e" s="T25">(Ku-) </ts>
               <ts e="T27" id="Seg_187" n="e" s="T26">Kumen </ts>
               <ts e="T28" id="Seg_189" n="e" s="T27">ej </ts>
               <ts e="T29" id="Seg_191" n="e" s="T28">kunolaʔ. </ts>
               <ts e="T30" id="Seg_193" n="e" s="T29">A </ts>
               <ts e="T31" id="Seg_195" n="e" s="T30">kunollal, </ts>
               <ts e="T32" id="Seg_197" n="e" s="T31">sʼo </ts>
               <ts e="T33" id="Seg_199" n="e" s="T32">ravno. </ts>
               <ts e="T34" id="Seg_201" n="e" s="T33">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_202" s="T0">PKZ_196X_EatDrinkSleep_flk.001 (002)</ta>
            <ta e="T13" id="Seg_203" s="T5">PKZ_196X_EatDrinkSleep_flk.002 (003)</ta>
            <ta e="T19" id="Seg_204" s="T13">PKZ_196X_EatDrinkSleep_flk.003 (004)</ta>
            <ta e="T25" id="Seg_205" s="T19">PKZ_196X_EatDrinkSleep_flk.004 (005)</ta>
            <ta e="T29" id="Seg_206" s="T25">PKZ_196X_EatDrinkSleep_flk.005 (006)</ta>
            <ta e="T33" id="Seg_207" s="T29">PKZ_196X_EatDrinkSleep_flk.006 (007)</ta>
            <ta e="T34" id="Seg_208" s="T33">PKZ_196X_EatDrinkSleep_flk.007 (008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_209" s="T0">Onʼiʔ kuza mămbi: ĭmbim ajirzittə. </ta>
            <ta e="T13" id="Seg_210" s="T5">(Dĭ ĭmbi), amzittə naga dak, tože ej jakšə. </ta>
            <ta e="T19" id="Seg_211" s="T13">Bü bĭssittə tože naga, ej jakšə. </ta>
            <ta e="T25" id="Seg_212" s="T19">A kunolzittə iššo, iʔgö nada kunolzittə. </ta>
            <ta e="T29" id="Seg_213" s="T25">(Ku-) Kumen ej kunolaʔ. </ta>
            <ta e="T33" id="Seg_214" s="T29">A kunollal, sʼo ravno. </ta>
            <ta e="T34" id="Seg_215" s="T33">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_216" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_217" s="T1">kuza</ta>
            <ta e="T3" id="Seg_218" s="T2">măm-bi</ta>
            <ta e="T4" id="Seg_219" s="T3">ĭmbi-m</ta>
            <ta e="T5" id="Seg_220" s="T4">ajir-zittə</ta>
            <ta e="T6" id="Seg_221" s="T5">dĭ</ta>
            <ta e="T7" id="Seg_222" s="T6">ĭmbi</ta>
            <ta e="T8" id="Seg_223" s="T7">am-zittə</ta>
            <ta e="T9" id="Seg_224" s="T8">naga</ta>
            <ta e="T10" id="Seg_225" s="T9">dak</ta>
            <ta e="T11" id="Seg_226" s="T10">tože</ta>
            <ta e="T12" id="Seg_227" s="T11">ej</ta>
            <ta e="T13" id="Seg_228" s="T12">jakšə</ta>
            <ta e="T14" id="Seg_229" s="T13">bü</ta>
            <ta e="T15" id="Seg_230" s="T14">bĭs-sittə</ta>
            <ta e="T16" id="Seg_231" s="T15">tože</ta>
            <ta e="T17" id="Seg_232" s="T16">naga</ta>
            <ta e="T18" id="Seg_233" s="T17">ej</ta>
            <ta e="T19" id="Seg_234" s="T18">jakšə</ta>
            <ta e="T20" id="Seg_235" s="T19">a</ta>
            <ta e="T21" id="Seg_236" s="T20">kunol-zittə</ta>
            <ta e="T22" id="Seg_237" s="T21">iššo</ta>
            <ta e="T23" id="Seg_238" s="T22">iʔgö</ta>
            <ta e="T24" id="Seg_239" s="T23">nada</ta>
            <ta e="T25" id="Seg_240" s="T24">kunol-zittə</ta>
            <ta e="T27" id="Seg_241" s="T26">kumen</ta>
            <ta e="T28" id="Seg_242" s="T27">ej</ta>
            <ta e="T29" id="Seg_243" s="T28">kunol-a-ʔ</ta>
            <ta e="T30" id="Seg_244" s="T29">a</ta>
            <ta e="T31" id="Seg_245" s="T30">kunol-la-l</ta>
            <ta e="T34" id="Seg_246" s="T33">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_247" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_248" s="T1">kuza</ta>
            <ta e="T3" id="Seg_249" s="T2">măn-bi</ta>
            <ta e="T4" id="Seg_250" s="T3">ĭmbi-m</ta>
            <ta e="T5" id="Seg_251" s="T4">ajir-zittə</ta>
            <ta e="T6" id="Seg_252" s="T5">dĭ</ta>
            <ta e="T7" id="Seg_253" s="T6">ĭmbi</ta>
            <ta e="T8" id="Seg_254" s="T7">am-zittə</ta>
            <ta e="T9" id="Seg_255" s="T8">naga</ta>
            <ta e="T10" id="Seg_256" s="T9">tak</ta>
            <ta e="T11" id="Seg_257" s="T10">tože</ta>
            <ta e="T12" id="Seg_258" s="T11">ej</ta>
            <ta e="T13" id="Seg_259" s="T12">jakšə</ta>
            <ta e="T14" id="Seg_260" s="T13">bü</ta>
            <ta e="T15" id="Seg_261" s="T14">bĭs-zittə</ta>
            <ta e="T16" id="Seg_262" s="T15">tože</ta>
            <ta e="T17" id="Seg_263" s="T16">naga</ta>
            <ta e="T18" id="Seg_264" s="T17">ej</ta>
            <ta e="T19" id="Seg_265" s="T18">jakšə</ta>
            <ta e="T20" id="Seg_266" s="T19">a</ta>
            <ta e="T21" id="Seg_267" s="T20">kunol-zittə</ta>
            <ta e="T22" id="Seg_268" s="T21">ĭššo</ta>
            <ta e="T23" id="Seg_269" s="T22">iʔgö</ta>
            <ta e="T24" id="Seg_270" s="T23">nadə</ta>
            <ta e="T25" id="Seg_271" s="T24">kunol-zittə</ta>
            <ta e="T27" id="Seg_272" s="T26">kumən</ta>
            <ta e="T28" id="Seg_273" s="T27">ej</ta>
            <ta e="T29" id="Seg_274" s="T28">kunol-ə-ʔ</ta>
            <ta e="T30" id="Seg_275" s="T29">a</ta>
            <ta e="T31" id="Seg_276" s="T30">kunol-lV-l</ta>
            <ta e="T34" id="Seg_277" s="T33">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_278" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_279" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_280" s="T2">say-PST.[3SG]</ta>
            <ta e="T4" id="Seg_281" s="T3">what-ACC</ta>
            <ta e="T5" id="Seg_282" s="T4">pity-INF.LAT</ta>
            <ta e="T6" id="Seg_283" s="T5">this.[NOM.SG]</ta>
            <ta e="T7" id="Seg_284" s="T6">what.[NOM.SG]</ta>
            <ta e="T8" id="Seg_285" s="T7">eat-INF.LAT</ta>
            <ta e="T9" id="Seg_286" s="T8">NEG.EX.[3SG]</ta>
            <ta e="T10" id="Seg_287" s="T9">so</ta>
            <ta e="T11" id="Seg_288" s="T10">also</ta>
            <ta e="T12" id="Seg_289" s="T11">NEG</ta>
            <ta e="T13" id="Seg_290" s="T12">good.[NOM.SG]</ta>
            <ta e="T14" id="Seg_291" s="T13">water.[NOM.SG]</ta>
            <ta e="T15" id="Seg_292" s="T14">drink-INF.LAT</ta>
            <ta e="T16" id="Seg_293" s="T15">also</ta>
            <ta e="T17" id="Seg_294" s="T16">NEG.EX.[3SG]</ta>
            <ta e="T18" id="Seg_295" s="T17">NEG</ta>
            <ta e="T19" id="Seg_296" s="T18">good.[NOM.SG]</ta>
            <ta e="T20" id="Seg_297" s="T19">and</ta>
            <ta e="T21" id="Seg_298" s="T20">sleep-INF.LAT</ta>
            <ta e="T22" id="Seg_299" s="T21">more</ta>
            <ta e="T23" id="Seg_300" s="T22">many</ta>
            <ta e="T24" id="Seg_301" s="T23">one.should</ta>
            <ta e="T25" id="Seg_302" s="T24">sleep-INF.LAT</ta>
            <ta e="T27" id="Seg_303" s="T26">how.much</ta>
            <ta e="T28" id="Seg_304" s="T27">NEG</ta>
            <ta e="T29" id="Seg_305" s="T28">sleep-EP-IMP.2SG</ta>
            <ta e="T30" id="Seg_306" s="T29">and</ta>
            <ta e="T31" id="Seg_307" s="T30">sleep-FUT-2SG</ta>
            <ta e="T34" id="Seg_308" s="T33">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_309" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_310" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_311" s="T2">сказать-PST.[3SG]</ta>
            <ta e="T4" id="Seg_312" s="T3">что-ACC</ta>
            <ta e="T5" id="Seg_313" s="T4">жалеть-INF.LAT</ta>
            <ta e="T6" id="Seg_314" s="T5">этот.[NOM.SG]</ta>
            <ta e="T7" id="Seg_315" s="T6">что.[NOM.SG]</ta>
            <ta e="T8" id="Seg_316" s="T7">съесть-INF.LAT</ta>
            <ta e="T9" id="Seg_317" s="T8">NEG.EX.[3SG]</ta>
            <ta e="T10" id="Seg_318" s="T9">так</ta>
            <ta e="T11" id="Seg_319" s="T10">тоже</ta>
            <ta e="T12" id="Seg_320" s="T11">NEG</ta>
            <ta e="T13" id="Seg_321" s="T12">хороший.[NOM.SG]</ta>
            <ta e="T14" id="Seg_322" s="T13">вода.[NOM.SG]</ta>
            <ta e="T15" id="Seg_323" s="T14">пить-INF.LAT</ta>
            <ta e="T16" id="Seg_324" s="T15">тоже</ta>
            <ta e="T17" id="Seg_325" s="T16">NEG.EX.[3SG]</ta>
            <ta e="T18" id="Seg_326" s="T17">NEG</ta>
            <ta e="T19" id="Seg_327" s="T18">хороший.[NOM.SG]</ta>
            <ta e="T20" id="Seg_328" s="T19">а</ta>
            <ta e="T21" id="Seg_329" s="T20">спать-INF.LAT</ta>
            <ta e="T22" id="Seg_330" s="T21">еще</ta>
            <ta e="T23" id="Seg_331" s="T22">много</ta>
            <ta e="T24" id="Seg_332" s="T23">надо</ta>
            <ta e="T25" id="Seg_333" s="T24">спать-INF.LAT</ta>
            <ta e="T27" id="Seg_334" s="T26">сколько</ta>
            <ta e="T28" id="Seg_335" s="T27">NEG</ta>
            <ta e="T29" id="Seg_336" s="T28">спать-EP-IMP.2SG</ta>
            <ta e="T30" id="Seg_337" s="T29">а</ta>
            <ta e="T31" id="Seg_338" s="T30">спать-FUT-2SG</ta>
            <ta e="T34" id="Seg_339" s="T33">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_340" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_341" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_342" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_343" s="T3">que-n:case</ta>
            <ta e="T5" id="Seg_344" s="T4">v-v:n.fin</ta>
            <ta e="T6" id="Seg_345" s="T5">dempro-n:case</ta>
            <ta e="T7" id="Seg_346" s="T6">que-n:case</ta>
            <ta e="T8" id="Seg_347" s="T7">v-v:n.fin</ta>
            <ta e="T9" id="Seg_348" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_349" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_350" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_351" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_352" s="T12">adj-n:case</ta>
            <ta e="T14" id="Seg_353" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_354" s="T14">v-v:n.fin</ta>
            <ta e="T16" id="Seg_355" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_356" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_357" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_358" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_359" s="T19">conj</ta>
            <ta e="T21" id="Seg_360" s="T20">v-v:n.fin</ta>
            <ta e="T22" id="Seg_361" s="T21">adv</ta>
            <ta e="T23" id="Seg_362" s="T22">quant</ta>
            <ta e="T24" id="Seg_363" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_364" s="T24">v-v:n.fin</ta>
            <ta e="T27" id="Seg_365" s="T26">adv</ta>
            <ta e="T28" id="Seg_366" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_367" s="T28">v-v:ins-v:mood.pn</ta>
            <ta e="T30" id="Seg_368" s="T29">conj</ta>
            <ta e="T31" id="Seg_369" s="T30">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_370" s="T33">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_371" s="T0">num</ta>
            <ta e="T2" id="Seg_372" s="T1">n</ta>
            <ta e="T3" id="Seg_373" s="T2">v</ta>
            <ta e="T4" id="Seg_374" s="T3">que</ta>
            <ta e="T5" id="Seg_375" s="T4">v</ta>
            <ta e="T6" id="Seg_376" s="T5">dempro</ta>
            <ta e="T7" id="Seg_377" s="T6">que</ta>
            <ta e="T8" id="Seg_378" s="T7">v</ta>
            <ta e="T9" id="Seg_379" s="T8">v</ta>
            <ta e="T10" id="Seg_380" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_381" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_382" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_383" s="T12">adj</ta>
            <ta e="T14" id="Seg_384" s="T13">n</ta>
            <ta e="T15" id="Seg_385" s="T14">v</ta>
            <ta e="T16" id="Seg_386" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_387" s="T16">v</ta>
            <ta e="T18" id="Seg_388" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_389" s="T18">adj</ta>
            <ta e="T20" id="Seg_390" s="T19">conj</ta>
            <ta e="T21" id="Seg_391" s="T20">v</ta>
            <ta e="T22" id="Seg_392" s="T21">adv</ta>
            <ta e="T23" id="Seg_393" s="T22">quant</ta>
            <ta e="T24" id="Seg_394" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_395" s="T24">v</ta>
            <ta e="T27" id="Seg_396" s="T26">adv</ta>
            <ta e="T28" id="Seg_397" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_398" s="T28">v</ta>
            <ta e="T30" id="Seg_399" s="T29">conj</ta>
            <ta e="T31" id="Seg_400" s="T30">v</ta>
            <ta e="T34" id="Seg_401" s="T33">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_402" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_403" s="T3">pro:B</ta>
            <ta e="T9" id="Seg_404" s="T8">0.3:Th</ta>
            <ta e="T14" id="Seg_405" s="T13">np:Th</ta>
            <ta e="T29" id="Seg_406" s="T28">0.2.h:E</ta>
            <ta e="T31" id="Seg_407" s="T30">0.2.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_408" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_409" s="T3">pro:O</ta>
            <ta e="T5" id="Seg_410" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_411" s="T7">s:purp</ta>
            <ta e="T9" id="Seg_412" s="T8">v:pred 0.3:S</ta>
            <ta e="T12" id="Seg_413" s="T11">ptcl.neg</ta>
            <ta e="T13" id="Seg_414" s="T12">adj:pred</ta>
            <ta e="T14" id="Seg_415" s="T13">np:O</ta>
            <ta e="T15" id="Seg_416" s="T14">s:purp</ta>
            <ta e="T17" id="Seg_417" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_418" s="T17">ptcl.neg</ta>
            <ta e="T19" id="Seg_419" s="T18">adj:pred</ta>
            <ta e="T24" id="Seg_420" s="T23">ptcl:pred</ta>
            <ta e="T28" id="Seg_421" s="T27">ptcl.neg</ta>
            <ta e="T29" id="Seg_422" s="T28">v:pred 0.2.h:S</ta>
            <ta e="T31" id="Seg_423" s="T30">v:pred 0.2.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_424" s="T9">RUS:gram</ta>
            <ta e="T11" id="Seg_425" s="T10">RUS:mod</ta>
            <ta e="T13" id="Seg_426" s="T12">TURK:core</ta>
            <ta e="T16" id="Seg_427" s="T15">RUS:mod</ta>
            <ta e="T19" id="Seg_428" s="T18">TURK:core</ta>
            <ta e="T20" id="Seg_429" s="T19">RUS:gram</ta>
            <ta e="T22" id="Seg_430" s="T21">RUS:mod</ta>
            <ta e="T24" id="Seg_431" s="T23">RUS:mod</ta>
            <ta e="T30" id="Seg_432" s="T29">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T33" id="Seg_433" s="T31">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_434" s="T0">Один человек сказал: что я должен любить?</ta>
            <ta e="T13" id="Seg_435" s="T5">Если кому нечего есть, это тоже плохо.</ta>
            <ta e="T19" id="Seg_436" s="T13">[Если] нечего пить, это тоже плохо.</ta>
            <ta e="T25" id="Seg_437" s="T19">И спать, тоже, спать надо много.</ta>
            <ta e="T29" id="Seg_438" s="T25">Хоть сколько будешь не спать.</ta>
            <ta e="T33" id="Seg_439" s="T29">А всё равно заснешь.</ta>
            <ta e="T34" id="Seg_440" s="T33">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_441" s="T0">One man said: what should I like?</ta>
            <ta e="T13" id="Seg_442" s="T5">If someone has nothing to eat, this is not good, too.</ta>
            <ta e="T19" id="Seg_443" s="T13">[If] there is no water to drink, it is not good, too.</ta>
            <ta e="T25" id="Seg_444" s="T19">And to sleep, too, it is necessary to sleep a lot.</ta>
            <ta e="T29" id="Seg_445" s="T25">No matter how long you don't sleep.</ta>
            <ta e="T33" id="Seg_446" s="T29">You'll fall asleep anyway.</ta>
            <ta e="T34" id="Seg_447" s="T33">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_448" s="T0">Ein Mann sagte: was soll mir gefallen?</ta>
            <ta e="T13" id="Seg_449" s="T5">Wenn einer nichts zu essen hat, das ist auch nicht gut.</ta>
            <ta e="T19" id="Seg_450" s="T13">[Wenn] es kein Wasser zu trinken gibt, ist es auch nicht gut.</ta>
            <ta e="T25" id="Seg_451" s="T19">Und auch zu schlafen, man muss auch viel schlafen.</ta>
            <ta e="T29" id="Seg_452" s="T25">Egal, wie viel Zeit du nicht schläfst.</ta>
            <ta e="T33" id="Seg_453" s="T29">Aber du wirst trotzdem einschlafen.</ta>
            <ta e="T34" id="Seg_454" s="T33">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_455" s="T0">[GVY:] or "who to feel sorry for"? The text is probably about what is more important: eating, drinking or sleeping; however, the text is obscure, and its source could not be found.</ta>
            <ta e="T29" id="Seg_456" s="T25">[GVY:] Unclear.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
