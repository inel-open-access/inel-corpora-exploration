<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID87665D6E-A1A6-3154-7347-CDECB0200E8B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ManAndBear_flk.wav" />
         <referenced-file url="PKZ_196X_ManAndBear_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ManAndBear_flk\PKZ_196X_ManAndBear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">203</ud-information>
            <ud-information attribute-name="# HIAT:w">142</ud-information>
            <ud-information attribute-name="# e">142</ud-information>
            <ud-information attribute-name="# HIAT:u">38</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1412" time="0.009" type="appl" />
         <tli id="T1413" time="0.531" type="appl" />
         <tli id="T1414" time="1.053" type="appl" />
         <tli id="T1415" time="1.699902965778583" />
         <tli id="T1416" time="2.752" type="appl" />
         <tli id="T1417" time="3.93" type="appl" />
         <tli id="T1418" time="5.108" type="appl" />
         <tli id="T1419" time="6.47963012837954" />
         <tli id="T1420" time="7.323" type="appl" />
         <tli id="T1421" time="8.256" type="appl" />
         <tli id="T1422" time="9.979430352041328" />
         <tli id="T1423" time="10.811" type="appl" />
         <tli id="T1424" time="11.879" type="appl" />
         <tli id="T1425" time="12.948" type="appl" />
         <tli id="T1426" time="13.705" type="appl" />
         <tli id="T1427" time="14.462" type="appl" />
         <tli id="T1428" time="15.219" type="appl" />
         <tli id="T1429" time="15.976" type="appl" />
         <tli id="T1430" time="16.733" type="appl" />
         <tli id="T1431" time="17.49" type="appl" />
         <tli id="T1432" time="18.38" type="appl" />
         <tli id="T1433" time="20.105518999169433" />
         <tli id="T1434" time="21.258" type="appl" />
         <tli id="T1435" time="22.35" type="appl" />
         <tli id="T1436" time="23.443" type="appl" />
         <tli id="T1437" time="24.631" type="appl" />
         <tli id="T1438" time="25.819" type="appl" />
         <tli id="T1439" time="27.007" type="appl" />
         <tli id="T1440" time="28.45" type="appl" />
         <tli id="T1441" time="29.669" type="appl" />
         <tli id="T1442" time="30.399" type="appl" />
         <tli id="T1443" time="30.916" type="appl" />
         <tli id="T1444" time="31.433" type="appl" />
         <tli id="T1445" time="32.197" type="appl" />
         <tli id="T1446" time="32.961" type="appl" />
         <tli id="T1447" time="33.725" type="appl" />
         <tli id="T1448" time="34.488" type="appl" />
         <tli id="T1449" time="35.252" type="appl" />
         <tli id="T1450" time="36.016" type="appl" />
         <tli id="T1451" time="36.78" type="appl" />
         <tli id="T1452" time="37.544" type="appl" />
         <tli id="T1453" time="38.307" type="appl" />
         <tli id="T1454" time="39.07" type="appl" />
         <tli id="T1455" time="39.834" type="appl" />
         <tli id="T1456" time="40.768" type="appl" />
         <tli id="T1" time="41.61945449995167" type="intp" />
         <tli id="T1457" time="42.47090899990334" />
         <tli id="T1458" time="42.585454499951666" type="intp" />
         <tli id="T1459" time="42.7" type="appl" />
         <tli id="T1460" time="43.271" type="appl" />
         <tli id="T1461" time="43.841" type="appl" />
         <tli id="T1462" time="44.297471402347774" />
         <tli id="T1463" time="45.162" type="appl" />
         <tli id="T1464" time="45.911" type="appl" />
         <tli id="T1465" time="46.66" type="appl" />
         <tli id="T1466" time="47.83726934285142" />
         <tli id="T1467" time="48.799" type="appl" />
         <tli id="T1468" time="49.681" type="appl" />
         <tli id="T1469" time="50.44" type="appl" />
         <tli id="T1470" time="51.198" type="appl" />
         <tli id="T1471" time="51.957" type="appl" />
         <tli id="T1472" time="52.716" type="appl" />
         <tli id="T1473" time="54.658" type="appl" />
         <tli id="T1474" time="55.939" type="appl" />
         <tli id="T1475" time="57.221" type="appl" />
         <tli id="T1476" time="58.502" type="appl" />
         <tli id="T1477" time="59.447" type="appl" />
         <tli id="T1478" time="60.267" type="appl" />
         <tli id="T1479" time="61.088" type="appl" />
         <tli id="T1480" time="61.964" type="appl" />
         <tli id="T1481" time="62.84" type="appl" />
         <tli id="T1482" time="63.716" type="appl" />
         <tli id="T1483" time="65.06295272940771" />
         <tli id="T1484" time="65.728" type="appl" />
         <tli id="T1485" time="66.299" type="appl" />
         <tli id="T1486" time="66.87" type="appl" />
         <tli id="T1487" time="67.441" type="appl" />
         <tli id="T1488" time="68.008" type="appl" />
         <tli id="T1489" time="68.576" type="appl" />
         <tli id="T1490" time="69.143" type="appl" />
         <tli id="T1491" time="69.71" type="appl" />
         <tli id="T1492" time="70.797" type="appl" />
         <tli id="T1493" time="71.884" type="appl" />
         <tli id="T1494" time="72.971" type="appl" />
         <tli id="T1495" time="73.585" type="appl" />
         <tli id="T1496" time="74.198" type="appl" />
         <tli id="T1497" time="74.812" type="appl" />
         <tli id="T1498" time="75.425" type="appl" />
         <tli id="T1499" time="76.039" type="appl" />
         <tli id="T1500" time="76.652" type="appl" />
         <tli id="T1501" time="77.266" type="appl" />
         <tli id="T1502" time="77.879" type="appl" />
         <tli id="T1503" time="78.493" type="appl" />
         <tli id="T1504" time="79.264" type="appl" />
         <tli id="T1505" time="79.827" type="appl" />
         <tli id="T1506" time="80.39" type="appl" />
         <tli id="T1507" time="81.56867721281074" />
         <tli id="T1508" time="82.55" type="appl" />
         <tli id="T1509" time="83.595" type="appl" />
         <tli id="T1510" time="84.411" type="appl" />
         <tli id="T1511" time="85.227" type="appl" />
         <tli id="T1512" time="86.043" type="appl" />
         <tli id="T1513" time="86.833" type="appl" />
         <tli id="T1514" time="87.395" type="appl" />
         <tli id="T1515" time="87.956" type="appl" />
         <tli id="T1516" time="88.518" type="appl" />
         <tli id="T1517" time="89.08" type="appl" />
         <tli id="T1518" time="89.642" type="appl" />
         <tli id="T1519" time="90.204" type="appl" />
         <tli id="T1520" time="90.765" type="appl" />
         <tli id="T1521" time="91.327" type="appl" />
         <tli id="T1522" time="91.889" type="appl" />
         <tli id="T1523" time="92.523" type="appl" />
         <tli id="T1524" time="93.145" type="appl" />
         <tli id="T1525" time="93.768" type="appl" />
         <tli id="T1526" time="94.641" type="appl" />
         <tli id="T1527" time="95.514" type="appl" />
         <tli id="T1528" time="96.387" type="appl" />
         <tli id="T1529" time="97.14" type="appl" />
         <tli id="T1530" time="97.892" type="appl" />
         <tli id="T1531" time="98.645" type="appl" />
         <tli id="T1532" time="99.397" type="appl" />
         <tli id="T1533" time="100.15" type="appl" />
         <tli id="T1534" time="100.977" type="appl" />
         <tli id="T1535" time="101.718" type="appl" />
         <tli id="T1536" time="102.46" type="appl" />
         <tli id="T1537" time="103.201" type="appl" />
         <tli id="T1538" time="104.167" type="appl" />
         <tli id="T1539" time="105.133" type="appl" />
         <tli id="T1540" time="105.634" type="appl" />
         <tli id="T1541" time="106.134" type="appl" />
         <tli id="T1542" time="106.635" type="appl" />
         <tli id="T1543" time="107.324" type="appl" />
         <tli id="T1544" time="108.014" type="appl" />
         <tli id="T1545" time="108.703" type="appl" />
         <tli id="T1546" time="109.362" type="appl" />
         <tli id="T1547" time="109.965" type="appl" />
         <tli id="T1548" time="110.568" type="appl" />
         <tli id="T1549" time="111.17" type="appl" />
         <tli id="T1550" time="111.773" type="appl" />
         <tli id="T1551" time="112.376" type="appl" />
         <tli id="T1552" time="113.60018211699149" />
         <tli id="T1553" time="114.296" type="appl" />
         <tli id="T1554" time="114.87" type="appl" />
         <tli id="T1555" time="115.445" type="appl" />
         <tli id="T1556" time="116.351" type="appl" />
         <tli id="T0" time="116.38" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1556" id="Seg_0" n="sc" s="T1412">
               <ts e="T1415" id="Seg_2" n="HIAT:u" s="T1412">
                  <ts e="T1413" id="Seg_4" n="HIAT:w" s="T1412">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1414" id="Seg_7" n="HIAT:w" s="T1413">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1415" id="Seg_10" n="HIAT:w" s="T1414">ibi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1419" id="Seg_14" n="HIAT:u" s="T1415">
                  <ts e="T1416" id="Seg_16" n="HIAT:w" s="T1415">Ugaːndə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_19" n="HIAT:w" s="T1416">maktanarzittə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_22" n="HIAT:w" s="T1417">jakšə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1419" id="Seg_25" n="HIAT:w" s="T1418">mobi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1422" id="Seg_29" n="HIAT:u" s="T1419">
                  <ts e="T1420" id="Seg_31" n="HIAT:w" s="T1419">Dĭgəttə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_34" n="HIAT:w" s="T1420">kudonzəbi</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1422" id="Seg_37" n="HIAT:w" s="T1421">ilziʔ</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1425" id="Seg_41" n="HIAT:u" s="T1422">
                  <ts e="T1423" id="Seg_43" n="HIAT:w" s="T1422">Dĭgəttə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_46" n="HIAT:w" s="T1423">mĭmbi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_49" n="HIAT:w" s="T1424">dʼijenə</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1431" id="Seg_53" n="HIAT:u" s="T1425">
                  <ts e="T1426" id="Seg_55" n="HIAT:w" s="T1425">Măndə:</ts>
                  <nts id="Seg_56" n="HIAT:ip">"</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1427" id="Seg_59" n="HIAT:w" s="T1426">Măn</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1428" id="Seg_62" n="HIAT:w" s="T1427">dĭn</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_65" n="HIAT:w" s="T1428">urgaːban</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1430" id="Seg_68" n="HIAT:w" s="T1429">ujut</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1431" id="Seg_71" n="HIAT:w" s="T1430">kubiom</ts>
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1433" id="Seg_76" n="HIAT:u" s="T1431">
                  <ts e="T1432" id="Seg_78" n="HIAT:w" s="T1431">Dĭgəttə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1433" id="Seg_81" n="HIAT:w" s="T1432">kambiʔi</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1436" id="Seg_85" n="HIAT:u" s="T1433">
                  <ts e="T1434" id="Seg_87" n="HIAT:w" s="T1433">Dĭn</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1435" id="Seg_90" n="HIAT:w" s="T1434">slʼedəʔi</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_93" n="HIAT:w" s="T1435">măndərbiʔi</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1439" id="Seg_97" n="HIAT:u" s="T1436">
                  <ts e="T1437" id="Seg_99" n="HIAT:w" s="T1436">Šobiʔi</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_102" n="HIAT:w" s="T1437">urgo</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_105" n="HIAT:w" s="T1438">măjanə</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1441" id="Seg_109" n="HIAT:u" s="T1439">
                  <ts e="T1440" id="Seg_111" n="HIAT:w" s="T1439">Berlogtə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_114" n="HIAT:w" s="T1440">urgaːbanə</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1451" id="Seg_118" n="HIAT:u" s="T1441">
                  <ts e="T1442" id="Seg_120" n="HIAT:w" s="T1441">Dĭgəttə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_123" n="HIAT:w" s="T1442">dĭ</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1444" id="Seg_126" n="HIAT:w" s="T1443">măndə:</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_128" n="HIAT:ip">"</nts>
                  <ts e="T1445" id="Seg_130" n="HIAT:w" s="T1444">Sargaʔ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_133" n="HIAT:w" s="T1445">rʼemnʼi</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1448" id="Seg_136" n="HIAT:w" s="T1447">i</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1449" id="Seg_139" n="HIAT:w" s="T1448">măna</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1450" id="Seg_142" n="HIAT:w" s="T1449">öʔlüʔgeʔ</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1451" id="Seg_146" n="HIAT:w" s="T1450">dibər</ts>
                  <nts id="Seg_147" n="HIAT:ip">"</nts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1455" id="Seg_151" n="HIAT:u" s="T1451">
                  <ts e="T1452" id="Seg_153" n="HIAT:w" s="T1451">Dĭzeŋ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1453" id="Seg_156" n="HIAT:w" s="T1452">dĭm</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1454" id="Seg_159" n="HIAT:w" s="T1453">dibər</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1455" id="Seg_162" n="HIAT:w" s="T1454">öʔlüʔbiʔi</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1457" id="Seg_166" n="HIAT:u" s="T1455">
                  <ts e="T1456" id="Seg_168" n="HIAT:w" s="T1455">I</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1" id="Seg_171" n="HIAT:w" s="T1456">baruʔluʔpi</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_174" n="HIAT:w" s="T1">rʼemnʼiʔi</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1462" id="Seg_178" n="HIAT:u" s="T1457">
                  <ts e="T1460" id="Seg_180" n="HIAT:w" s="T1457">Dĭ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1461" id="Seg_183" n="HIAT:w" s="T1460">dĭn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1462" id="Seg_186" n="HIAT:w" s="T1461">amnolaʔbə</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1466" id="Seg_190" n="HIAT:u" s="T1462">
                  <ts e="T1463" id="Seg_192" n="HIAT:w" s="T1462">Dʼala</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1464" id="Seg_196" n="HIAT:w" s="T1463">nagur</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1465" id="Seg_199" n="HIAT:w" s="T1464">dʼala</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1466" id="Seg_202" n="HIAT:w" s="T1465">amnoluʔpi</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1468" id="Seg_206" n="HIAT:u" s="T1466">
                  <ts e="T1467" id="Seg_208" n="HIAT:w" s="T1466">Amzittə</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1468" id="Seg_211" n="HIAT:w" s="T1467">axota</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1472" id="Seg_215" n="HIAT:u" s="T1468">
                  <ts e="T1469" id="Seg_217" n="HIAT:w" s="T1468">Dĭgəttə</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1470" id="Seg_220" n="HIAT:w" s="T1469">bar</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471" id="Seg_223" n="HIAT:w" s="T1470">remnʼiʔi</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1472" id="Seg_226" n="HIAT:w" s="T1471">ambi</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1473" id="Seg_230" n="HIAT:u" s="T1472">
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <ts e="T1473" id="Seg_233" n="HIAT:w" s="T1472">Kunoluʔbdə</ts>
                  <nts id="Seg_234" n="HIAT:ip">)</nts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1476" id="Seg_238" n="HIAT:u" s="T1473">
                  <ts e="T1474" id="Seg_240" n="HIAT:w" s="T1473">Păʔpi</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1475" id="Seg_243" n="HIAT:w" s="T1474">urgaːbanə</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1476" id="Seg_247" n="HIAT:w" s="T1475">berloktə</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1479" id="Seg_251" n="HIAT:u" s="T1476">
                  <ts e="T1477" id="Seg_253" n="HIAT:w" s="T1476">Dĭn</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1478" id="Seg_256" n="HIAT:w" s="T1477">ejü</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1479" id="Seg_260" n="HIAT:w" s="T1478">kunolluʔpi</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1483" id="Seg_264" n="HIAT:u" s="T1479">
                  <ts e="T1480" id="Seg_266" n="HIAT:w" s="T1479">Dĭgəttə</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1481" id="Seg_269" n="HIAT:w" s="T1480">urgaːba</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1482" id="Seg_272" n="HIAT:w" s="T1481">ujut</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1483" id="Seg_275" n="HIAT:w" s="T1482">mĭbi</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1487" id="Seg_279" n="HIAT:u" s="T1483">
                  <ts e="T1484" id="Seg_281" n="HIAT:w" s="T1483">Dĭ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1485" id="Seg_284" n="HIAT:w" s="T1484">dĭm</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1486" id="Seg_287" n="HIAT:w" s="T1485">ambi</ts>
                  <nts id="Seg_288" n="HIAT:ip">,</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1487" id="Seg_291" n="HIAT:w" s="T1486">ambi</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1491" id="Seg_295" n="HIAT:u" s="T1487">
                  <ts e="T1488" id="Seg_297" n="HIAT:w" s="T1487">Dĭgəttə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1489" id="Seg_300" n="HIAT:w" s="T1488">amzittə</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1490" id="Seg_303" n="HIAT:w" s="T1489">nʼe</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_306" n="HIAT:w" s="T1490">axota</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1494" id="Seg_310" n="HIAT:u" s="T1491">
                  <ts e="T1492" id="Seg_312" n="HIAT:w" s="T1491">Dĭgəttə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1493" id="Seg_315" n="HIAT:w" s="T1492">ejü</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1494" id="Seg_318" n="HIAT:w" s="T1493">mobi</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1503" id="Seg_322" n="HIAT:u" s="T1494">
                  <ts e="T1495" id="Seg_324" n="HIAT:w" s="T1494">Urgaːba</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1496" id="Seg_327" n="HIAT:w" s="T1495">măndə:</ts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1497" id="Seg_331" n="HIAT:w" s="T1496">Amnaʔ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1498" id="Seg_334" n="HIAT:w" s="T1497">măna</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1499" id="Seg_338" n="HIAT:w" s="T1498">măn</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1500" id="Seg_341" n="HIAT:w" s="T1499">tăn</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1501" id="Seg_344" n="HIAT:w" s="T1500">kunnallam</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1502" id="Seg_347" n="HIAT:w" s="T1501">maːndə</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_349" n="HIAT:ip">(</nts>
                  <ts e="T1503" id="Seg_351" n="HIAT:w" s="T1502">dĭg-</ts>
                  <nts id="Seg_352" n="HIAT:ip">)</nts>
                  <nts id="Seg_353" n="HIAT:ip">"</nts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1507" id="Seg_357" n="HIAT:u" s="T1503">
                  <ts e="T1504" id="Seg_359" n="HIAT:w" s="T1503">Dĭgəttə</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_362" n="HIAT:w" s="T1504">dĭ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1506" id="Seg_365" n="HIAT:w" s="T1505">kambi</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1507" id="Seg_368" n="HIAT:w" s="T1506">maːndə</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1509" id="Seg_372" n="HIAT:u" s="T1507">
                  <ts e="T1508" id="Seg_374" n="HIAT:w" s="T1507">Amnəbi</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1509" id="Seg_377" n="HIAT:w" s="T1508">urgaːbanə</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1512" id="Seg_381" n="HIAT:u" s="T1509">
                  <ts e="T1510" id="Seg_383" n="HIAT:w" s="T1509">Urgaːba</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1511" id="Seg_386" n="HIAT:w" s="T1510">kumbi</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1512" id="Seg_389" n="HIAT:w" s="T1511">maːʔndə</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1522" id="Seg_393" n="HIAT:u" s="T1512">
                  <ts e="T1513" id="Seg_395" n="HIAT:w" s="T1512">A</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1514" id="Seg_398" n="HIAT:w" s="T1513">nagur</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1515" id="Seg_401" n="HIAT:w" s="T1514">menzeŋ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1516" id="Seg_404" n="HIAT:w" s="T1515">dettə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1517" id="Seg_407" n="HIAT:w" s="T1516">döbər</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1518" id="Seg_410" n="HIAT:w" s="T1517">da</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_412" n="HIAT:ip">(</nts>
                  <ts e="T1519" id="Seg_414" n="HIAT:w" s="T1518">saraʔdə</ts>
                  <nts id="Seg_415" n="HIAT:ip">)</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1520" id="Seg_418" n="HIAT:w" s="T1519">măna</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1521" id="Seg_421" n="HIAT:w" s="T1520">oʔb</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1522" id="Seg_424" n="HIAT:w" s="T1521">nʼit</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1525" id="Seg_428" n="HIAT:u" s="T1522">
                  <ts e="T1523" id="Seg_430" n="HIAT:w" s="T1522">Dĭgəttə</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_433" n="HIAT:w" s="T1523">dĭ</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1525" id="Seg_436" n="HIAT:w" s="T1524">šobi</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1528" id="Seg_440" n="HIAT:u" s="T1525">
                  <ts e="T1526" id="Seg_442" n="HIAT:w" s="T1525">Il</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1527" id="Seg_445" n="HIAT:w" s="T1526">šobiʔi</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1528" id="Seg_449" n="HIAT:w" s="T1527">surarlaʔbəʔjə</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1533" id="Seg_453" n="HIAT:u" s="T1528">
                  <ts e="T1529" id="Seg_455" n="HIAT:w" s="T1528">Dĭ</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1530" id="Seg_458" n="HIAT:w" s="T1529">ĭmbidə</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1531" id="Seg_461" n="HIAT:w" s="T1530">ej</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1532" id="Seg_464" n="HIAT:w" s="T1531">dʼăbaktərbi</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1533" id="Seg_467" n="HIAT:w" s="T1532">ilziʔ</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1537" id="Seg_471" n="HIAT:u" s="T1533">
                  <ts e="T1534" id="Seg_473" n="HIAT:w" s="T1533">Dĭgəttə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1535" id="Seg_476" n="HIAT:w" s="T1534">nagur</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1536" id="Seg_479" n="HIAT:w" s="T1535">men</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1537" id="Seg_482" n="HIAT:w" s="T1536">ibi</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1539" id="Seg_486" n="HIAT:u" s="T1537">
                  <ts e="T1538" id="Seg_488" n="HIAT:w" s="T1537">Urgaːbanə</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1539" id="Seg_491" n="HIAT:w" s="T1538">kumbi</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1542" id="Seg_495" n="HIAT:u" s="T1539">
                  <ts e="T1540" id="Seg_497" n="HIAT:w" s="T1539">I</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1541" id="Seg_500" n="HIAT:w" s="T1540">dibər</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_502" n="HIAT:ip">(</nts>
                  <ts e="T1542" id="Seg_504" n="HIAT:w" s="T1541">öʔlubi</ts>
                  <nts id="Seg_505" n="HIAT:ip">)</nts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1545" id="Seg_509" n="HIAT:u" s="T1542">
                  <ts e="T1543" id="Seg_511" n="HIAT:w" s="T1542">I</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1544" id="Seg_514" n="HIAT:w" s="T1543">parbi</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1545" id="Seg_517" n="HIAT:w" s="T1544">maːʔndə</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1552" id="Seg_521" n="HIAT:u" s="T1545">
                  <ts e="T1546" id="Seg_523" n="HIAT:w" s="T1545">I</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1547" id="Seg_526" n="HIAT:w" s="T1546">kaməndə</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_528" n="HIAT:ip">(</nts>
                  <ts e="T1548" id="Seg_530" n="HIAT:w" s="T1547">ej</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1549" id="Seg_533" n="HIAT:w" s="T1548">kuno-</ts>
                  <nts id="Seg_534" n="HIAT:ip">)</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1550" id="Seg_537" n="HIAT:w" s="T1549">ej</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1551" id="Seg_540" n="HIAT:w" s="T1550">kudonzəbi</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1552" id="Seg_543" n="HIAT:w" s="T1551">ilziʔ</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1555" id="Seg_547" n="HIAT:u" s="T1552">
                  <ts e="T1553" id="Seg_549" n="HIAT:w" s="T1552">Dal</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1554" id="Seg_552" n="HIAT:w" s="T1553">jakšə</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1555" id="Seg_555" n="HIAT:w" s="T1554">amnosʼtə</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1556" id="Seg_559" n="HIAT:u" s="T1555">
                  <ts e="T1556" id="Seg_561" n="HIAT:w" s="T1555">Kabarləj</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1556" id="Seg_564" n="sc" s="T1412">
               <ts e="T1413" id="Seg_566" n="e" s="T1412">Onʼiʔ </ts>
               <ts e="T1414" id="Seg_568" n="e" s="T1413">kuza </ts>
               <ts e="T1415" id="Seg_570" n="e" s="T1414">ibi. </ts>
               <ts e="T1416" id="Seg_572" n="e" s="T1415">Ugaːndə </ts>
               <ts e="T1417" id="Seg_574" n="e" s="T1416">maktanarzittə </ts>
               <ts e="T1418" id="Seg_576" n="e" s="T1417">jakšə </ts>
               <ts e="T1419" id="Seg_578" n="e" s="T1418">mobi. </ts>
               <ts e="T1420" id="Seg_580" n="e" s="T1419">Dĭgəttə </ts>
               <ts e="T1421" id="Seg_582" n="e" s="T1420">kudonzəbi </ts>
               <ts e="T1422" id="Seg_584" n="e" s="T1421">ilziʔ. </ts>
               <ts e="T1423" id="Seg_586" n="e" s="T1422">Dĭgəttə </ts>
               <ts e="T1424" id="Seg_588" n="e" s="T1423">mĭmbi </ts>
               <ts e="T1425" id="Seg_590" n="e" s="T1424">dʼijenə. </ts>
               <ts e="T1426" id="Seg_592" n="e" s="T1425">Măndə:" </ts>
               <ts e="T1427" id="Seg_594" n="e" s="T1426">Măn </ts>
               <ts e="T1428" id="Seg_596" n="e" s="T1427">dĭn </ts>
               <ts e="T1429" id="Seg_598" n="e" s="T1428">urgaːban </ts>
               <ts e="T1430" id="Seg_600" n="e" s="T1429">ujut </ts>
               <ts e="T1431" id="Seg_602" n="e" s="T1430">kubiom". </ts>
               <ts e="T1432" id="Seg_604" n="e" s="T1431">Dĭgəttə </ts>
               <ts e="T1433" id="Seg_606" n="e" s="T1432">kambiʔi. </ts>
               <ts e="T1434" id="Seg_608" n="e" s="T1433">Dĭn </ts>
               <ts e="T1435" id="Seg_610" n="e" s="T1434">slʼedəʔi </ts>
               <ts e="T1436" id="Seg_612" n="e" s="T1435">măndərbiʔi. </ts>
               <ts e="T1437" id="Seg_614" n="e" s="T1436">Šobiʔi </ts>
               <ts e="T1438" id="Seg_616" n="e" s="T1437">urgo </ts>
               <ts e="T1439" id="Seg_618" n="e" s="T1438">măjanə. </ts>
               <ts e="T1440" id="Seg_620" n="e" s="T1439">Berlogtə </ts>
               <ts e="T1441" id="Seg_622" n="e" s="T1440">urgaːbanə. </ts>
               <ts e="T1442" id="Seg_624" n="e" s="T1441">Dĭgəttə </ts>
               <ts e="T1443" id="Seg_626" n="e" s="T1442">dĭ </ts>
               <ts e="T1444" id="Seg_628" n="e" s="T1443">măndə: </ts>
               <ts e="T1445" id="Seg_630" n="e" s="T1444">"Sargaʔ </ts>
               <ts e="T1447" id="Seg_632" n="e" s="T1445">rʼemnʼi </ts>
               <ts e="T1448" id="Seg_634" n="e" s="T1447">i </ts>
               <ts e="T1449" id="Seg_636" n="e" s="T1448">măna </ts>
               <ts e="T1450" id="Seg_638" n="e" s="T1449">öʔlüʔgeʔ, </ts>
               <ts e="T1451" id="Seg_640" n="e" s="T1450">dibər". </ts>
               <ts e="T1452" id="Seg_642" n="e" s="T1451">Dĭzeŋ </ts>
               <ts e="T1453" id="Seg_644" n="e" s="T1452">dĭm </ts>
               <ts e="T1454" id="Seg_646" n="e" s="T1453">dibər </ts>
               <ts e="T1455" id="Seg_648" n="e" s="T1454">öʔlüʔbiʔi. </ts>
               <ts e="T1456" id="Seg_650" n="e" s="T1455">I </ts>
               <ts e="T1" id="Seg_652" n="e" s="T1456">baruʔluʔpi </ts>
               <ts e="T1457" id="Seg_654" n="e" s="T1">rʼemnʼiʔi. </ts>
               <ts e="T1460" id="Seg_656" n="e" s="T1457">Dĭ </ts>
               <ts e="T1461" id="Seg_658" n="e" s="T1460">dĭn </ts>
               <ts e="T1462" id="Seg_660" n="e" s="T1461">amnolaʔbə. </ts>
               <ts e="T1463" id="Seg_662" n="e" s="T1462">Dʼala, </ts>
               <ts e="T1464" id="Seg_664" n="e" s="T1463">nagur </ts>
               <ts e="T1465" id="Seg_666" n="e" s="T1464">dʼala </ts>
               <ts e="T1466" id="Seg_668" n="e" s="T1465">amnoluʔpi. </ts>
               <ts e="T1467" id="Seg_670" n="e" s="T1466">Amzittə </ts>
               <ts e="T1468" id="Seg_672" n="e" s="T1467">axota. </ts>
               <ts e="T1469" id="Seg_674" n="e" s="T1468">Dĭgəttə </ts>
               <ts e="T1470" id="Seg_676" n="e" s="T1469">bar </ts>
               <ts e="T1471" id="Seg_678" n="e" s="T1470">remnʼiʔi </ts>
               <ts e="T1472" id="Seg_680" n="e" s="T1471">ambi. </ts>
               <ts e="T1473" id="Seg_682" n="e" s="T1472">(Kunoluʔbdə). </ts>
               <ts e="T1474" id="Seg_684" n="e" s="T1473">Păʔpi </ts>
               <ts e="T1475" id="Seg_686" n="e" s="T1474">urgaːbanə, </ts>
               <ts e="T1476" id="Seg_688" n="e" s="T1475">berloktə. </ts>
               <ts e="T1477" id="Seg_690" n="e" s="T1476">Dĭn </ts>
               <ts e="T1478" id="Seg_692" n="e" s="T1477">ejü, </ts>
               <ts e="T1479" id="Seg_694" n="e" s="T1478">kunolluʔpi. </ts>
               <ts e="T1480" id="Seg_696" n="e" s="T1479">Dĭgəttə </ts>
               <ts e="T1481" id="Seg_698" n="e" s="T1480">urgaːba </ts>
               <ts e="T1482" id="Seg_700" n="e" s="T1481">ujut </ts>
               <ts e="T1483" id="Seg_702" n="e" s="T1482">mĭbi. </ts>
               <ts e="T1484" id="Seg_704" n="e" s="T1483">Dĭ </ts>
               <ts e="T1485" id="Seg_706" n="e" s="T1484">dĭm </ts>
               <ts e="T1486" id="Seg_708" n="e" s="T1485">ambi, </ts>
               <ts e="T1487" id="Seg_710" n="e" s="T1486">ambi. </ts>
               <ts e="T1488" id="Seg_712" n="e" s="T1487">Dĭgəttə </ts>
               <ts e="T1489" id="Seg_714" n="e" s="T1488">amzittə </ts>
               <ts e="T1490" id="Seg_716" n="e" s="T1489">nʼe </ts>
               <ts e="T1491" id="Seg_718" n="e" s="T1490">axota. </ts>
               <ts e="T1492" id="Seg_720" n="e" s="T1491">Dĭgəttə </ts>
               <ts e="T1493" id="Seg_722" n="e" s="T1492">ejü </ts>
               <ts e="T1494" id="Seg_724" n="e" s="T1493">mobi. </ts>
               <ts e="T1495" id="Seg_726" n="e" s="T1494">Urgaːba </ts>
               <ts e="T1496" id="Seg_728" n="e" s="T1495">măndə:" </ts>
               <ts e="T1497" id="Seg_730" n="e" s="T1496">Amnaʔ </ts>
               <ts e="T1498" id="Seg_732" n="e" s="T1497">măna, </ts>
               <ts e="T1499" id="Seg_734" n="e" s="T1498">măn </ts>
               <ts e="T1500" id="Seg_736" n="e" s="T1499">tăn </ts>
               <ts e="T1501" id="Seg_738" n="e" s="T1500">kunnallam </ts>
               <ts e="T1502" id="Seg_740" n="e" s="T1501">maːndə </ts>
               <ts e="T1503" id="Seg_742" n="e" s="T1502">(dĭg-)". </ts>
               <ts e="T1504" id="Seg_744" n="e" s="T1503">Dĭgəttə </ts>
               <ts e="T1505" id="Seg_746" n="e" s="T1504">dĭ </ts>
               <ts e="T1506" id="Seg_748" n="e" s="T1505">kambi </ts>
               <ts e="T1507" id="Seg_750" n="e" s="T1506">maːndə. </ts>
               <ts e="T1508" id="Seg_752" n="e" s="T1507">Amnəbi </ts>
               <ts e="T1509" id="Seg_754" n="e" s="T1508">urgaːbanə. </ts>
               <ts e="T1510" id="Seg_756" n="e" s="T1509">Urgaːba </ts>
               <ts e="T1511" id="Seg_758" n="e" s="T1510">kumbi </ts>
               <ts e="T1512" id="Seg_760" n="e" s="T1511">maːʔndə. </ts>
               <ts e="T1513" id="Seg_762" n="e" s="T1512">A </ts>
               <ts e="T1514" id="Seg_764" n="e" s="T1513">nagur </ts>
               <ts e="T1515" id="Seg_766" n="e" s="T1514">menzeŋ </ts>
               <ts e="T1516" id="Seg_768" n="e" s="T1515">dettə </ts>
               <ts e="T1517" id="Seg_770" n="e" s="T1516">döbər </ts>
               <ts e="T1518" id="Seg_772" n="e" s="T1517">da </ts>
               <ts e="T1519" id="Seg_774" n="e" s="T1518">(saraʔdə) </ts>
               <ts e="T1520" id="Seg_776" n="e" s="T1519">măna </ts>
               <ts e="T1521" id="Seg_778" n="e" s="T1520">oʔb </ts>
               <ts e="T1522" id="Seg_780" n="e" s="T1521">nʼit. </ts>
               <ts e="T1523" id="Seg_782" n="e" s="T1522">Dĭgəttə </ts>
               <ts e="T1524" id="Seg_784" n="e" s="T1523">dĭ </ts>
               <ts e="T1525" id="Seg_786" n="e" s="T1524">šobi. </ts>
               <ts e="T1526" id="Seg_788" n="e" s="T1525">Il </ts>
               <ts e="T1527" id="Seg_790" n="e" s="T1526">šobiʔi, </ts>
               <ts e="T1528" id="Seg_792" n="e" s="T1527">surarlaʔbəʔjə. </ts>
               <ts e="T1529" id="Seg_794" n="e" s="T1528">Dĭ </ts>
               <ts e="T1530" id="Seg_796" n="e" s="T1529">ĭmbidə </ts>
               <ts e="T1531" id="Seg_798" n="e" s="T1530">ej </ts>
               <ts e="T1532" id="Seg_800" n="e" s="T1531">dʼăbaktərbi </ts>
               <ts e="T1533" id="Seg_802" n="e" s="T1532">ilziʔ. </ts>
               <ts e="T1534" id="Seg_804" n="e" s="T1533">Dĭgəttə </ts>
               <ts e="T1535" id="Seg_806" n="e" s="T1534">nagur </ts>
               <ts e="T1536" id="Seg_808" n="e" s="T1535">men </ts>
               <ts e="T1537" id="Seg_810" n="e" s="T1536">ibi. </ts>
               <ts e="T1538" id="Seg_812" n="e" s="T1537">Urgaːbanə </ts>
               <ts e="T1539" id="Seg_814" n="e" s="T1538">kumbi. </ts>
               <ts e="T1540" id="Seg_816" n="e" s="T1539">I </ts>
               <ts e="T1541" id="Seg_818" n="e" s="T1540">dibər </ts>
               <ts e="T1542" id="Seg_820" n="e" s="T1541">(öʔlubi). </ts>
               <ts e="T1543" id="Seg_822" n="e" s="T1542">I </ts>
               <ts e="T1544" id="Seg_824" n="e" s="T1543">parbi </ts>
               <ts e="T1545" id="Seg_826" n="e" s="T1544">maːʔndə. </ts>
               <ts e="T1546" id="Seg_828" n="e" s="T1545">I </ts>
               <ts e="T1547" id="Seg_830" n="e" s="T1546">kaməndə </ts>
               <ts e="T1548" id="Seg_832" n="e" s="T1547">(ej </ts>
               <ts e="T1549" id="Seg_834" n="e" s="T1548">kuno-) </ts>
               <ts e="T1550" id="Seg_836" n="e" s="T1549">ej </ts>
               <ts e="T1551" id="Seg_838" n="e" s="T1550">kudonzəbi </ts>
               <ts e="T1552" id="Seg_840" n="e" s="T1551">ilziʔ. </ts>
               <ts e="T1553" id="Seg_842" n="e" s="T1552">Dal </ts>
               <ts e="T1554" id="Seg_844" n="e" s="T1553">jakšə </ts>
               <ts e="T1555" id="Seg_846" n="e" s="T1554">amnosʼtə. </ts>
               <ts e="T1556" id="Seg_848" n="e" s="T1555">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1415" id="Seg_849" s="T1412">PKZ_196X_ManAndBear_flk.001 (001)</ta>
            <ta e="T1419" id="Seg_850" s="T1415">PKZ_196X_ManAndBear_flk.002 (002)</ta>
            <ta e="T1422" id="Seg_851" s="T1419">PKZ_196X_ManAndBear_flk.003 (003)</ta>
            <ta e="T1425" id="Seg_852" s="T1422">PKZ_196X_ManAndBear_flk.004 (004)</ta>
            <ta e="T1431" id="Seg_853" s="T1425">PKZ_196X_ManAndBear_flk.005 (005)</ta>
            <ta e="T1433" id="Seg_854" s="T1431">PKZ_196X_ManAndBear_flk.006 (006)</ta>
            <ta e="T1436" id="Seg_855" s="T1433">PKZ_196X_ManAndBear_flk.007 (007)</ta>
            <ta e="T1439" id="Seg_856" s="T1436">PKZ_196X_ManAndBear_flk.008 (008)</ta>
            <ta e="T1441" id="Seg_857" s="T1439">PKZ_196X_ManAndBear_flk.009 (009)</ta>
            <ta e="T1451" id="Seg_858" s="T1441">PKZ_196X_ManAndBear_flk.010 (010) </ta>
            <ta e="T1455" id="Seg_859" s="T1451">PKZ_196X_ManAndBear_flk.011 (012)</ta>
            <ta e="T1457" id="Seg_860" s="T1455">PKZ_196X_ManAndBear_flk.012 (013)</ta>
            <ta e="T1462" id="Seg_861" s="T1457">PKZ_196X_ManAndBear_flk.013 (014)</ta>
            <ta e="T1466" id="Seg_862" s="T1462">PKZ_196X_ManAndBear_flk.014 (015)</ta>
            <ta e="T1468" id="Seg_863" s="T1466">PKZ_196X_ManAndBear_flk.015 (016)</ta>
            <ta e="T1472" id="Seg_864" s="T1468">PKZ_196X_ManAndBear_flk.016 (017)</ta>
            <ta e="T1473" id="Seg_865" s="T1472">PKZ_196X_ManAndBear_flk.017 (018)</ta>
            <ta e="T1476" id="Seg_866" s="T1473">PKZ_196X_ManAndBear_flk.018 (019)</ta>
            <ta e="T1479" id="Seg_867" s="T1476">PKZ_196X_ManAndBear_flk.019 (020)</ta>
            <ta e="T1483" id="Seg_868" s="T1479">PKZ_196X_ManAndBear_flk.020 (021)</ta>
            <ta e="T1487" id="Seg_869" s="T1483">PKZ_196X_ManAndBear_flk.021 (022)</ta>
            <ta e="T1491" id="Seg_870" s="T1487">PKZ_196X_ManAndBear_flk.022 (023)</ta>
            <ta e="T1494" id="Seg_871" s="T1491">PKZ_196X_ManAndBear_flk.023 (024)</ta>
            <ta e="T1503" id="Seg_872" s="T1494">PKZ_196X_ManAndBear_flk.024 (025)</ta>
            <ta e="T1507" id="Seg_873" s="T1503">PKZ_196X_ManAndBear_flk.025 (026)</ta>
            <ta e="T1509" id="Seg_874" s="T1507">PKZ_196X_ManAndBear_flk.026 (027)</ta>
            <ta e="T1512" id="Seg_875" s="T1509">PKZ_196X_ManAndBear_flk.027 (028)</ta>
            <ta e="T1522" id="Seg_876" s="T1512">PKZ_196X_ManAndBear_flk.028 (029)</ta>
            <ta e="T1525" id="Seg_877" s="T1522">PKZ_196X_ManAndBear_flk.029 (030)</ta>
            <ta e="T1528" id="Seg_878" s="T1525">PKZ_196X_ManAndBear_flk.030 (031)</ta>
            <ta e="T1533" id="Seg_879" s="T1528">PKZ_196X_ManAndBear_flk.031 (032)</ta>
            <ta e="T1537" id="Seg_880" s="T1533">PKZ_196X_ManAndBear_flk.032 (033)</ta>
            <ta e="T1539" id="Seg_881" s="T1537">PKZ_196X_ManAndBear_flk.033 (034)</ta>
            <ta e="T1542" id="Seg_882" s="T1539">PKZ_196X_ManAndBear_flk.034 (035)</ta>
            <ta e="T1545" id="Seg_883" s="T1542">PKZ_196X_ManAndBear_flk.035 (036)</ta>
            <ta e="T1552" id="Seg_884" s="T1545">PKZ_196X_ManAndBear_flk.036 (037)</ta>
            <ta e="T1555" id="Seg_885" s="T1552">PKZ_196X_ManAndBear_flk.037 (038)</ta>
            <ta e="T1556" id="Seg_886" s="T1555">PKZ_196X_ManAndBear_flk.038 (039)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1415" id="Seg_887" s="T1412">Onʼiʔ kuza ibi. </ta>
            <ta e="T1419" id="Seg_888" s="T1415">Ugaːndə maktanarzittə jakšə mobi. </ta>
            <ta e="T1422" id="Seg_889" s="T1419">Dĭgəttə kudonzəbi ilziʔ. </ta>
            <ta e="T1425" id="Seg_890" s="T1422">Dĭgəttə mĭmbi dʼijenə. </ta>
            <ta e="T1431" id="Seg_891" s="T1425">Măndə:" Măn dĭn urgaːban ujut kubiom". </ta>
            <ta e="T1433" id="Seg_892" s="T1431">Dĭgəttə kambiʔi. </ta>
            <ta e="T1436" id="Seg_893" s="T1433">Dĭn slʼedəʔi măndərbiʔi. </ta>
            <ta e="T1439" id="Seg_894" s="T1436">Šobiʔi urgo măjanə. </ta>
            <ta e="T1441" id="Seg_895" s="T1439">Berlogtə urgaːbanə. </ta>
            <ta e="T1451" id="Seg_896" s="T1441">Dĭgəttə dĭ măndə: "Sargaʔ rʼemnʼi i măna öʔlüʔgeʔ, dibər". </ta>
            <ta e="T1455" id="Seg_897" s="T1451">Dĭzeŋ dĭm dibər öʔlüʔbiʔi. </ta>
            <ta e="T1457" id="Seg_898" s="T1455">I baruʔluʔpi rʼemnʼiʔi. </ta>
            <ta e="T1462" id="Seg_899" s="T1457">Dĭ dĭn amnolaʔbə. </ta>
            <ta e="T1466" id="Seg_900" s="T1462">Dʼala, nagur dʼala amnoluʔpi. </ta>
            <ta e="T1468" id="Seg_901" s="T1466">Amzittə axota. </ta>
            <ta e="T1472" id="Seg_902" s="T1468">Dĭgəttə bar remnʼiʔi ambi. </ta>
            <ta e="T1473" id="Seg_903" s="T1472">(Kunoluʔbdə). </ta>
            <ta e="T1476" id="Seg_904" s="T1473">Păʔpi urgaːbanə, berloktə. </ta>
            <ta e="T1479" id="Seg_905" s="T1476">Dĭn ejü, kunolluʔpi. </ta>
            <ta e="T1483" id="Seg_906" s="T1479">Dĭgəttə urgaːba ujut mĭbi. </ta>
            <ta e="T1487" id="Seg_907" s="T1483">Dĭ dĭm ambi, ambi. </ta>
            <ta e="T1491" id="Seg_908" s="T1487">Dĭgəttə amzittə nʼe axota. </ta>
            <ta e="T1494" id="Seg_909" s="T1491">Dĭgəttə ejü mobi. </ta>
            <ta e="T1503" id="Seg_910" s="T1494">Urgaːba măndə:" Amnaʔ măna, măn tăn kunnallam maːndə (dĭg-)". </ta>
            <ta e="T1507" id="Seg_911" s="T1503">Dĭgəttə dĭ kambi maːndə. </ta>
            <ta e="T1509" id="Seg_912" s="T1507">Amnəbi urgaːbanə. </ta>
            <ta e="T1512" id="Seg_913" s="T1509">Urgaːba kumbi maːʔndə. </ta>
            <ta e="T1522" id="Seg_914" s="T1512">A nagur menzeŋ dettə döbər da (saraʔdə) măna oʔb nʼit. </ta>
            <ta e="T1525" id="Seg_915" s="T1522">Dĭgəttə dĭ šobi. </ta>
            <ta e="T1528" id="Seg_916" s="T1525">Il šobiʔi, surarlaʔbəʔjə. </ta>
            <ta e="T1533" id="Seg_917" s="T1528">Dĭ ĭmbidə ej dʼăbaktərbi ilziʔ. </ta>
            <ta e="T1537" id="Seg_918" s="T1533">Dĭgəttə nagur men ibi. </ta>
            <ta e="T1539" id="Seg_919" s="T1537">Urgaːbanə kumbi. </ta>
            <ta e="T1542" id="Seg_920" s="T1539">I dibər (öʔlubi). </ta>
            <ta e="T1545" id="Seg_921" s="T1542">I parbi maːʔndə. </ta>
            <ta e="T1552" id="Seg_922" s="T1545">I kaməndə (ej kuno-) ej kudonzəbi ilziʔ. </ta>
            <ta e="T1555" id="Seg_923" s="T1552">Dal jakšə amnosʼtə. </ta>
            <ta e="T1556" id="Seg_924" s="T1555">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1413" id="Seg_925" s="T1412">onʼiʔ</ta>
            <ta e="T1414" id="Seg_926" s="T1413">kuza</ta>
            <ta e="T1415" id="Seg_927" s="T1414">i-bi</ta>
            <ta e="T1416" id="Seg_928" s="T1415">ugaːndə</ta>
            <ta e="T1417" id="Seg_929" s="T1416">makta-nar-zittə</ta>
            <ta e="T1418" id="Seg_930" s="T1417">jakšə</ta>
            <ta e="T1419" id="Seg_931" s="T1418">mo-bi</ta>
            <ta e="T1420" id="Seg_932" s="T1419">dĭgəttə</ta>
            <ta e="T1421" id="Seg_933" s="T1420">kudo-nzə-bi</ta>
            <ta e="T1422" id="Seg_934" s="T1421">il-ziʔ</ta>
            <ta e="T1423" id="Seg_935" s="T1422">dĭgəttə</ta>
            <ta e="T1424" id="Seg_936" s="T1423">mĭm-bi</ta>
            <ta e="T1425" id="Seg_937" s="T1424">dʼije-nə</ta>
            <ta e="T1426" id="Seg_938" s="T1425">măn-də</ta>
            <ta e="T1427" id="Seg_939" s="T1426">măn</ta>
            <ta e="T1428" id="Seg_940" s="T1427">dĭn</ta>
            <ta e="T1429" id="Seg_941" s="T1428">urgaːba-n</ta>
            <ta e="T1430" id="Seg_942" s="T1429">uju-t</ta>
            <ta e="T1431" id="Seg_943" s="T1430">ku-bio-m</ta>
            <ta e="T1432" id="Seg_944" s="T1431">dĭgəttə</ta>
            <ta e="T1433" id="Seg_945" s="T1432">kam-bi-ʔi</ta>
            <ta e="T1434" id="Seg_946" s="T1433">dĭn</ta>
            <ta e="T1435" id="Seg_947" s="T1434">slʼedə-ʔi</ta>
            <ta e="T1436" id="Seg_948" s="T1435">măndə-r-bi-ʔi</ta>
            <ta e="T1437" id="Seg_949" s="T1436">šo-bi-ʔi</ta>
            <ta e="T1438" id="Seg_950" s="T1437">urgo</ta>
            <ta e="T1439" id="Seg_951" s="T1438">măja-nə</ta>
            <ta e="T1440" id="Seg_952" s="T1439">berlog-tə</ta>
            <ta e="T1441" id="Seg_953" s="T1440">urgaːba-nə</ta>
            <ta e="T1442" id="Seg_954" s="T1441">dĭgəttə</ta>
            <ta e="T1443" id="Seg_955" s="T1442">dĭ</ta>
            <ta e="T1444" id="Seg_956" s="T1443">măn-də</ta>
            <ta e="T1445" id="Seg_957" s="T1444">sar-gaʔ</ta>
            <ta e="T1447" id="Seg_958" s="T1445">rʼemnʼi</ta>
            <ta e="T1448" id="Seg_959" s="T1447">i</ta>
            <ta e="T1449" id="Seg_960" s="T1448">măna</ta>
            <ta e="T1450" id="Seg_961" s="T1449">öʔ-lüʔ-geʔ</ta>
            <ta e="T1451" id="Seg_962" s="T1450">dibər</ta>
            <ta e="T1452" id="Seg_963" s="T1451">dĭ-zeŋ</ta>
            <ta e="T1453" id="Seg_964" s="T1452">dĭ-m</ta>
            <ta e="T1454" id="Seg_965" s="T1453">dibər</ta>
            <ta e="T1455" id="Seg_966" s="T1454">öʔ-lüʔ-bi-ʔi</ta>
            <ta e="T1456" id="Seg_967" s="T1455">i</ta>
            <ta e="T1" id="Seg_968" s="T1456">baruʔ-luʔ-pi</ta>
            <ta e="T1457" id="Seg_969" s="T1">rʼemnʼi-ʔi</ta>
            <ta e="T1460" id="Seg_970" s="T1457">dĭ</ta>
            <ta e="T1461" id="Seg_971" s="T1460">dĭn</ta>
            <ta e="T1462" id="Seg_972" s="T1461">amno-laʔbə</ta>
            <ta e="T1463" id="Seg_973" s="T1462">dʼala</ta>
            <ta e="T1464" id="Seg_974" s="T1463">nagur</ta>
            <ta e="T1465" id="Seg_975" s="T1464">dʼala</ta>
            <ta e="T1466" id="Seg_976" s="T1465">amno-luʔ-pi</ta>
            <ta e="T1467" id="Seg_977" s="T1466">am-zittə</ta>
            <ta e="T1468" id="Seg_978" s="T1467">axota</ta>
            <ta e="T1469" id="Seg_979" s="T1468">dĭgəttə</ta>
            <ta e="T1470" id="Seg_980" s="T1469">bar</ta>
            <ta e="T1471" id="Seg_981" s="T1470">remnʼi-ʔi</ta>
            <ta e="T1472" id="Seg_982" s="T1471">am-bi</ta>
            <ta e="T1473" id="Seg_983" s="T1472">kuno-luʔbdə</ta>
            <ta e="T1474" id="Seg_984" s="T1473">păʔ-pi</ta>
            <ta e="T1475" id="Seg_985" s="T1474">urgaːba-nə</ta>
            <ta e="T1476" id="Seg_986" s="T1475">berlok-tə</ta>
            <ta e="T1477" id="Seg_987" s="T1476">dĭn</ta>
            <ta e="T1478" id="Seg_988" s="T1477">ejü</ta>
            <ta e="T1479" id="Seg_989" s="T1478">kunol-luʔ-pi</ta>
            <ta e="T1480" id="Seg_990" s="T1479">dĭgəttə</ta>
            <ta e="T1481" id="Seg_991" s="T1480">urgaːba</ta>
            <ta e="T1482" id="Seg_992" s="T1481">uju-t</ta>
            <ta e="T1483" id="Seg_993" s="T1482">mĭ-bi</ta>
            <ta e="T1484" id="Seg_994" s="T1483">dĭ</ta>
            <ta e="T1485" id="Seg_995" s="T1484">dĭ-m</ta>
            <ta e="T1486" id="Seg_996" s="T1485">am-bi</ta>
            <ta e="T1487" id="Seg_997" s="T1486">am-bi</ta>
            <ta e="T1488" id="Seg_998" s="T1487">dĭgəttə</ta>
            <ta e="T1489" id="Seg_999" s="T1488">am-zittə</ta>
            <ta e="T1490" id="Seg_1000" s="T1489">nʼe</ta>
            <ta e="T1491" id="Seg_1001" s="T1490">axota</ta>
            <ta e="T1492" id="Seg_1002" s="T1491">dĭgəttə</ta>
            <ta e="T1493" id="Seg_1003" s="T1492">ejü</ta>
            <ta e="T1494" id="Seg_1004" s="T1493">mo-bi</ta>
            <ta e="T1495" id="Seg_1005" s="T1494">urgaːba</ta>
            <ta e="T1496" id="Seg_1006" s="T1495">măn-də</ta>
            <ta e="T1497" id="Seg_1007" s="T1496">amna-ʔ</ta>
            <ta e="T1498" id="Seg_1008" s="T1497">măna</ta>
            <ta e="T1499" id="Seg_1009" s="T1498">măn</ta>
            <ta e="T1500" id="Seg_1010" s="T1499">tăn</ta>
            <ta e="T1501" id="Seg_1011" s="T1500">kun-nal-la-m</ta>
            <ta e="T1502" id="Seg_1012" s="T1501">ma-ndə</ta>
            <ta e="T1504" id="Seg_1013" s="T1503">dĭgəttə</ta>
            <ta e="T1505" id="Seg_1014" s="T1504">dĭ</ta>
            <ta e="T1506" id="Seg_1015" s="T1505">kam-bi</ta>
            <ta e="T1507" id="Seg_1016" s="T1506">ma-ndə</ta>
            <ta e="T1508" id="Seg_1017" s="T1507">amnə-bi</ta>
            <ta e="T1509" id="Seg_1018" s="T1508">urgaːba-nə</ta>
            <ta e="T1510" id="Seg_1019" s="T1509">urgaːba</ta>
            <ta e="T1511" id="Seg_1020" s="T1510">kum-bi</ta>
            <ta e="T1512" id="Seg_1021" s="T1511">maːʔ-ndə</ta>
            <ta e="T1513" id="Seg_1022" s="T1512">a</ta>
            <ta e="T1514" id="Seg_1023" s="T1513">nagur</ta>
            <ta e="T1515" id="Seg_1024" s="T1514">men-zeŋ</ta>
            <ta e="T1516" id="Seg_1025" s="T1515">det-tə</ta>
            <ta e="T1517" id="Seg_1026" s="T1516">döbər</ta>
            <ta e="T1518" id="Seg_1027" s="T1517">da</ta>
            <ta e="T1519" id="Seg_1028" s="T1518">saraʔ-də</ta>
            <ta e="T1520" id="Seg_1029" s="T1519">măna</ta>
            <ta e="T1521" id="Seg_1030" s="T1520">oʔb</ta>
            <ta e="T1522" id="Seg_1031" s="T1521">nʼi-t</ta>
            <ta e="T1523" id="Seg_1032" s="T1522">dĭgəttə</ta>
            <ta e="T1524" id="Seg_1033" s="T1523">dĭ</ta>
            <ta e="T1525" id="Seg_1034" s="T1524">šo-bi</ta>
            <ta e="T1526" id="Seg_1035" s="T1525">il</ta>
            <ta e="T1527" id="Seg_1036" s="T1526">šo-bi-ʔi</ta>
            <ta e="T1528" id="Seg_1037" s="T1527">surar-laʔbə-ʔjə</ta>
            <ta e="T1529" id="Seg_1038" s="T1528">dĭ</ta>
            <ta e="T1530" id="Seg_1039" s="T1529">ĭmbi=də</ta>
            <ta e="T1531" id="Seg_1040" s="T1530">ej</ta>
            <ta e="T1532" id="Seg_1041" s="T1531">dʼăbaktər-bi</ta>
            <ta e="T1533" id="Seg_1042" s="T1532">il-ziʔ</ta>
            <ta e="T1534" id="Seg_1043" s="T1533">dĭgəttə</ta>
            <ta e="T1535" id="Seg_1044" s="T1534">nagur</ta>
            <ta e="T1536" id="Seg_1045" s="T1535">men</ta>
            <ta e="T1537" id="Seg_1046" s="T1536">i-bi</ta>
            <ta e="T1538" id="Seg_1047" s="T1537">urgaːba-nə</ta>
            <ta e="T1539" id="Seg_1048" s="T1538">kum-bi</ta>
            <ta e="T1540" id="Seg_1049" s="T1539">i</ta>
            <ta e="T1541" id="Seg_1050" s="T1540">dibər</ta>
            <ta e="T1542" id="Seg_1051" s="T1541">öʔlu-bi</ta>
            <ta e="T1543" id="Seg_1052" s="T1542">i</ta>
            <ta e="T1544" id="Seg_1053" s="T1543">par-bi</ta>
            <ta e="T1545" id="Seg_1054" s="T1544">maːʔ-ndə</ta>
            <ta e="T1546" id="Seg_1055" s="T1545">i</ta>
            <ta e="T1547" id="Seg_1056" s="T1546">kamən=də</ta>
            <ta e="T1548" id="Seg_1057" s="T1547">ej</ta>
            <ta e="T1550" id="Seg_1058" s="T1549">ej</ta>
            <ta e="T1551" id="Seg_1059" s="T1550">kudo-nzə-bi</ta>
            <ta e="T1552" id="Seg_1060" s="T1551">il-ziʔ</ta>
            <ta e="T1554" id="Seg_1061" s="T1553">jakšə</ta>
            <ta e="T1555" id="Seg_1062" s="T1554">amno-sʼtə</ta>
            <ta e="T1556" id="Seg_1063" s="T1555">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1413" id="Seg_1064" s="T1412">onʼiʔ</ta>
            <ta e="T1414" id="Seg_1065" s="T1413">kuza</ta>
            <ta e="T1415" id="Seg_1066" s="T1414">i-bi</ta>
            <ta e="T1416" id="Seg_1067" s="T1415">ugaːndə</ta>
            <ta e="T1417" id="Seg_1068" s="T1416">makta-nar-zittə</ta>
            <ta e="T1418" id="Seg_1069" s="T1417">jakšə</ta>
            <ta e="T1419" id="Seg_1070" s="T1418">mo-bi</ta>
            <ta e="T1420" id="Seg_1071" s="T1419">dĭgəttə</ta>
            <ta e="T1421" id="Seg_1072" s="T1420">kudo-nzə-bi</ta>
            <ta e="T1422" id="Seg_1073" s="T1421">il-ziʔ</ta>
            <ta e="T1423" id="Seg_1074" s="T1422">dĭgəttə</ta>
            <ta e="T1424" id="Seg_1075" s="T1423">mĭn-bi</ta>
            <ta e="T1425" id="Seg_1076" s="T1424">dʼije-Tə</ta>
            <ta e="T1426" id="Seg_1077" s="T1425">măn-ntə</ta>
            <ta e="T1427" id="Seg_1078" s="T1426">măn</ta>
            <ta e="T1428" id="Seg_1079" s="T1427">dĭn</ta>
            <ta e="T1429" id="Seg_1080" s="T1428">urgaːba-n</ta>
            <ta e="T1430" id="Seg_1081" s="T1429">üjü-t</ta>
            <ta e="T1431" id="Seg_1082" s="T1430">ku-bi-m</ta>
            <ta e="T1432" id="Seg_1083" s="T1431">dĭgəttə</ta>
            <ta e="T1433" id="Seg_1084" s="T1432">kan-bi-jəʔ</ta>
            <ta e="T1434" id="Seg_1085" s="T1433">dĭn</ta>
            <ta e="T1435" id="Seg_1086" s="T1434">slʼedə-jəʔ</ta>
            <ta e="T1436" id="Seg_1087" s="T1435">măndo-r-bi-jəʔ</ta>
            <ta e="T1437" id="Seg_1088" s="T1436">šo-bi-jəʔ</ta>
            <ta e="T1438" id="Seg_1089" s="T1437">urgo</ta>
            <ta e="T1439" id="Seg_1090" s="T1438">măja-Tə</ta>
            <ta e="T1440" id="Seg_1091" s="T1439">berlogə-Tə</ta>
            <ta e="T1441" id="Seg_1092" s="T1440">urgaːba-Tə</ta>
            <ta e="T1442" id="Seg_1093" s="T1441">dĭgəttə</ta>
            <ta e="T1443" id="Seg_1094" s="T1442">dĭ</ta>
            <ta e="T1444" id="Seg_1095" s="T1443">măn-ntə</ta>
            <ta e="T1445" id="Seg_1096" s="T1444">sar-KAʔ</ta>
            <ta e="T1447" id="Seg_1097" s="T1445">rʼemnʼi</ta>
            <ta e="T1448" id="Seg_1098" s="T1447">i</ta>
            <ta e="T1449" id="Seg_1099" s="T1448">măna</ta>
            <ta e="T1450" id="Seg_1100" s="T1449">öʔ-luʔbdə-KAʔ</ta>
            <ta e="T1451" id="Seg_1101" s="T1450">dĭbər</ta>
            <ta e="T1452" id="Seg_1102" s="T1451">dĭ-zAŋ</ta>
            <ta e="T1453" id="Seg_1103" s="T1452">dĭ-m</ta>
            <ta e="T1454" id="Seg_1104" s="T1453">dĭbər</ta>
            <ta e="T1455" id="Seg_1105" s="T1454">öʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T1456" id="Seg_1106" s="T1455">i</ta>
            <ta e="T1" id="Seg_1107" s="T1456">barəʔ-luʔbdə-bi</ta>
            <ta e="T1457" id="Seg_1108" s="T1">rʼemnʼi-jəʔ</ta>
            <ta e="T1460" id="Seg_1109" s="T1457">dĭ</ta>
            <ta e="T1461" id="Seg_1110" s="T1460">dĭn</ta>
            <ta e="T1462" id="Seg_1111" s="T1461">amnə-laʔbə</ta>
            <ta e="T1463" id="Seg_1112" s="T1462">tʼala</ta>
            <ta e="T1464" id="Seg_1113" s="T1463">nagur</ta>
            <ta e="T1465" id="Seg_1114" s="T1464">tʼala</ta>
            <ta e="T1466" id="Seg_1115" s="T1465">amnə-luʔbdə-bi</ta>
            <ta e="T1467" id="Seg_1116" s="T1466">am-zittə</ta>
            <ta e="T1468" id="Seg_1117" s="T1467">axota</ta>
            <ta e="T1469" id="Seg_1118" s="T1468">dĭgəttə</ta>
            <ta e="T1470" id="Seg_1119" s="T1469">bar</ta>
            <ta e="T1471" id="Seg_1120" s="T1470">remnʼi-jəʔ</ta>
            <ta e="T1472" id="Seg_1121" s="T1471">am-bi</ta>
            <ta e="T1473" id="Seg_1122" s="T1472">kunol-luʔbdə</ta>
            <ta e="T1474" id="Seg_1123" s="T1473">păda-bi</ta>
            <ta e="T1475" id="Seg_1124" s="T1474">urgaːba-Tə</ta>
            <ta e="T1476" id="Seg_1125" s="T1475">berlogə-də</ta>
            <ta e="T1477" id="Seg_1126" s="T1476">dĭn</ta>
            <ta e="T1478" id="Seg_1127" s="T1477">ejü</ta>
            <ta e="T1479" id="Seg_1128" s="T1478">kunol-luʔbdə-bi</ta>
            <ta e="T1480" id="Seg_1129" s="T1479">dĭgəttə</ta>
            <ta e="T1481" id="Seg_1130" s="T1480">urgaːba</ta>
            <ta e="T1482" id="Seg_1131" s="T1481">üjü-t</ta>
            <ta e="T1483" id="Seg_1132" s="T1482">mĭ-bi</ta>
            <ta e="T1484" id="Seg_1133" s="T1483">dĭ</ta>
            <ta e="T1485" id="Seg_1134" s="T1484">dĭ-m</ta>
            <ta e="T1486" id="Seg_1135" s="T1485">am-bi</ta>
            <ta e="T1487" id="Seg_1136" s="T1486">am-bi</ta>
            <ta e="T1488" id="Seg_1137" s="T1487">dĭgəttə</ta>
            <ta e="T1489" id="Seg_1138" s="T1488">am-zittə</ta>
            <ta e="T1490" id="Seg_1139" s="T1489">nʼe</ta>
            <ta e="T1491" id="Seg_1140" s="T1490">axota</ta>
            <ta e="T1492" id="Seg_1141" s="T1491">dĭgəttə</ta>
            <ta e="T1493" id="Seg_1142" s="T1492">ejü</ta>
            <ta e="T1494" id="Seg_1143" s="T1493">mo-bi</ta>
            <ta e="T1495" id="Seg_1144" s="T1494">urgaːba</ta>
            <ta e="T1496" id="Seg_1145" s="T1495">măn-ntə</ta>
            <ta e="T1497" id="Seg_1146" s="T1496">amnə-ʔ</ta>
            <ta e="T1498" id="Seg_1147" s="T1497">măna</ta>
            <ta e="T1499" id="Seg_1148" s="T1498">măn</ta>
            <ta e="T1500" id="Seg_1149" s="T1499">tăn</ta>
            <ta e="T1501" id="Seg_1150" s="T1500">kun-laːm-lV-m</ta>
            <ta e="T1502" id="Seg_1151" s="T1501">maʔ-gəndə</ta>
            <ta e="T1504" id="Seg_1152" s="T1503">dĭgəttə</ta>
            <ta e="T1505" id="Seg_1153" s="T1504">dĭ</ta>
            <ta e="T1506" id="Seg_1154" s="T1505">kan-bi</ta>
            <ta e="T1507" id="Seg_1155" s="T1506">maʔ-gəndə</ta>
            <ta e="T1508" id="Seg_1156" s="T1507">amnə-bi</ta>
            <ta e="T1509" id="Seg_1157" s="T1508">urgaːba-Tə</ta>
            <ta e="T1510" id="Seg_1158" s="T1509">urgaːba</ta>
            <ta e="T1511" id="Seg_1159" s="T1510">kun-bi</ta>
            <ta e="T1512" id="Seg_1160" s="T1511">maʔ-gəndə</ta>
            <ta e="T1513" id="Seg_1161" s="T1512">a</ta>
            <ta e="T1514" id="Seg_1162" s="T1513">nagur</ta>
            <ta e="T1515" id="Seg_1163" s="T1514">men-zAŋ</ta>
            <ta e="T1516" id="Seg_1164" s="T1515">det-t</ta>
            <ta e="T1517" id="Seg_1165" s="T1516">döbər</ta>
            <ta e="T1518" id="Seg_1166" s="T1517">da</ta>
            <ta e="T1519" id="Seg_1167" s="T1518">sar-t</ta>
            <ta e="T1520" id="Seg_1168" s="T1519">măna</ta>
            <ta e="T1521" id="Seg_1169" s="T1520">oʔb</ta>
            <ta e="T1522" id="Seg_1170" s="T1521">nʼi-t</ta>
            <ta e="T1523" id="Seg_1171" s="T1522">dĭgəttə</ta>
            <ta e="T1524" id="Seg_1172" s="T1523">dĭ</ta>
            <ta e="T1525" id="Seg_1173" s="T1524">šo-bi</ta>
            <ta e="T1526" id="Seg_1174" s="T1525">il</ta>
            <ta e="T1527" id="Seg_1175" s="T1526">šo-bi-jəʔ</ta>
            <ta e="T1528" id="Seg_1176" s="T1527">surar-laʔbə-jəʔ</ta>
            <ta e="T1529" id="Seg_1177" s="T1528">dĭ</ta>
            <ta e="T1530" id="Seg_1178" s="T1529">ĭmbi=də</ta>
            <ta e="T1531" id="Seg_1179" s="T1530">ej</ta>
            <ta e="T1532" id="Seg_1180" s="T1531">tʼăbaktər-bi</ta>
            <ta e="T1533" id="Seg_1181" s="T1532">il-ziʔ</ta>
            <ta e="T1534" id="Seg_1182" s="T1533">dĭgəttə</ta>
            <ta e="T1535" id="Seg_1183" s="T1534">nagur</ta>
            <ta e="T1536" id="Seg_1184" s="T1535">men</ta>
            <ta e="T1537" id="Seg_1185" s="T1536">i-bi</ta>
            <ta e="T1538" id="Seg_1186" s="T1537">urgaːba-Tə</ta>
            <ta e="T1539" id="Seg_1187" s="T1538">kun-bi</ta>
            <ta e="T1540" id="Seg_1188" s="T1539">i</ta>
            <ta e="T1541" id="Seg_1189" s="T1540">dĭbər</ta>
            <ta e="T1542" id="Seg_1190" s="T1541">öʔlu-bi</ta>
            <ta e="T1543" id="Seg_1191" s="T1542">i</ta>
            <ta e="T1544" id="Seg_1192" s="T1543">par-bi</ta>
            <ta e="T1545" id="Seg_1193" s="T1544">maʔ-gəndə</ta>
            <ta e="T1546" id="Seg_1194" s="T1545">i</ta>
            <ta e="T1547" id="Seg_1195" s="T1546">kamən=də</ta>
            <ta e="T1548" id="Seg_1196" s="T1547">ej</ta>
            <ta e="T1550" id="Seg_1197" s="T1549">ej</ta>
            <ta e="T1551" id="Seg_1198" s="T1550">kudo-nzə-bi</ta>
            <ta e="T1552" id="Seg_1199" s="T1551">il-ziʔ</ta>
            <ta e="T1554" id="Seg_1200" s="T1553">jakšə</ta>
            <ta e="T1555" id="Seg_1201" s="T1554">amno-zittə</ta>
            <ta e="T1556" id="Seg_1202" s="T1555">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1413" id="Seg_1203" s="T1412">one.[NOM.SG]</ta>
            <ta e="T1414" id="Seg_1204" s="T1413">man.[NOM.SG]</ta>
            <ta e="T1415" id="Seg_1205" s="T1414">be-PST.[3SG]</ta>
            <ta e="T1416" id="Seg_1206" s="T1415">very</ta>
            <ta e="T1417" id="Seg_1207" s="T1416">boast-MULT-INF.LAT</ta>
            <ta e="T1418" id="Seg_1208" s="T1417">good.[NOM.SG]</ta>
            <ta e="T1419" id="Seg_1209" s="T1418">become-PST.[3SG]</ta>
            <ta e="T1420" id="Seg_1210" s="T1419">then</ta>
            <ta e="T1421" id="Seg_1211" s="T1420">scold-DES-PST.[3SG]</ta>
            <ta e="T1422" id="Seg_1212" s="T1421">people-INS</ta>
            <ta e="T1423" id="Seg_1213" s="T1422">then</ta>
            <ta e="T1424" id="Seg_1214" s="T1423">go-PST.[3SG]</ta>
            <ta e="T1425" id="Seg_1215" s="T1424">forest-LAT</ta>
            <ta e="T1426" id="Seg_1216" s="T1425">say-IPFVZ.[3SG]</ta>
            <ta e="T1427" id="Seg_1217" s="T1426">I.NOM</ta>
            <ta e="T1428" id="Seg_1218" s="T1427">there</ta>
            <ta e="T1429" id="Seg_1219" s="T1428">bear-GEN</ta>
            <ta e="T1430" id="Seg_1220" s="T1429">foot-NOM/GEN.3SG</ta>
            <ta e="T1431" id="Seg_1221" s="T1430">find-PST-1SG</ta>
            <ta e="T1432" id="Seg_1222" s="T1431">then</ta>
            <ta e="T1433" id="Seg_1223" s="T1432">go-PST-3PL</ta>
            <ta e="T1434" id="Seg_1224" s="T1433">there</ta>
            <ta e="T1435" id="Seg_1225" s="T1434">trace-PL</ta>
            <ta e="T1436" id="Seg_1226" s="T1435">look-FRQ-PST-3PL</ta>
            <ta e="T1437" id="Seg_1227" s="T1436">come-PST-3PL</ta>
            <ta e="T1438" id="Seg_1228" s="T1437">big</ta>
            <ta e="T1439" id="Seg_1229" s="T1438">mountain-LAT</ta>
            <ta e="T1440" id="Seg_1230" s="T1439">lair-LAT</ta>
            <ta e="T1441" id="Seg_1231" s="T1440">bear-LAT</ta>
            <ta e="T1442" id="Seg_1232" s="T1441">then</ta>
            <ta e="T1443" id="Seg_1233" s="T1442">this.[NOM.SG]</ta>
            <ta e="T1444" id="Seg_1234" s="T1443">say-IPFVZ.[3SG]</ta>
            <ta e="T1445" id="Seg_1235" s="T1444">bind-IMP.2PL</ta>
            <ta e="T1447" id="Seg_1236" s="T1445">belt</ta>
            <ta e="T1448" id="Seg_1237" s="T1447">and</ta>
            <ta e="T1449" id="Seg_1238" s="T1448">I.ACC</ta>
            <ta e="T1450" id="Seg_1239" s="T1449">send-MOM-IMP.2PL</ta>
            <ta e="T1451" id="Seg_1240" s="T1450">there</ta>
            <ta e="T1452" id="Seg_1241" s="T1451">this-PL</ta>
            <ta e="T1453" id="Seg_1242" s="T1452">this-ACC</ta>
            <ta e="T1454" id="Seg_1243" s="T1453">there</ta>
            <ta e="T1455" id="Seg_1244" s="T1454">send-MOM-PST-3PL</ta>
            <ta e="T1456" id="Seg_1245" s="T1455">and</ta>
            <ta e="T1" id="Seg_1246" s="T1456">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T1457" id="Seg_1247" s="T1">belt-PL</ta>
            <ta e="T1460" id="Seg_1248" s="T1457">this.[NOM.SG]</ta>
            <ta e="T1461" id="Seg_1249" s="T1460">there</ta>
            <ta e="T1462" id="Seg_1250" s="T1461">sit-DUR.[3SG]</ta>
            <ta e="T1463" id="Seg_1251" s="T1462">day.[NOM.SG]</ta>
            <ta e="T1464" id="Seg_1252" s="T1463">three.[NOM.SG]</ta>
            <ta e="T1465" id="Seg_1253" s="T1464">day.[NOM.SG]</ta>
            <ta e="T1466" id="Seg_1254" s="T1465">sit-MOM-PST.[3SG]</ta>
            <ta e="T1467" id="Seg_1255" s="T1466">eat-INF.LAT</ta>
            <ta e="T1468" id="Seg_1256" s="T1467">one.wants</ta>
            <ta e="T1469" id="Seg_1257" s="T1468">then</ta>
            <ta e="T1470" id="Seg_1258" s="T1469">PTCL</ta>
            <ta e="T1471" id="Seg_1259" s="T1470">belt-PL</ta>
            <ta e="T1472" id="Seg_1260" s="T1471">eat-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_1261" s="T1472">sleep-MOM.[3SG]</ta>
            <ta e="T1474" id="Seg_1262" s="T1473">creep.into-PST.[3SG]</ta>
            <ta e="T1475" id="Seg_1263" s="T1474">bear-LAT</ta>
            <ta e="T1476" id="Seg_1264" s="T1475">lair-NOM/GEN/ACC.3SG</ta>
            <ta e="T1477" id="Seg_1265" s="T1476">there</ta>
            <ta e="T1478" id="Seg_1266" s="T1477">warm.[NOM.SG]</ta>
            <ta e="T1479" id="Seg_1267" s="T1478">sleep-MOM-PST.[3SG]</ta>
            <ta e="T1480" id="Seg_1268" s="T1479">then</ta>
            <ta e="T1481" id="Seg_1269" s="T1480">bear.[NOM.SG]</ta>
            <ta e="T1482" id="Seg_1270" s="T1481">foot-NOM/GEN.3SG</ta>
            <ta e="T1483" id="Seg_1271" s="T1482">give-PST.[3SG]</ta>
            <ta e="T1484" id="Seg_1272" s="T1483">this.[NOM.SG]</ta>
            <ta e="T1485" id="Seg_1273" s="T1484">this-ACC</ta>
            <ta e="T1486" id="Seg_1274" s="T1485">eat-PST.[3SG]</ta>
            <ta e="T1487" id="Seg_1275" s="T1486">eat-PST.[3SG]</ta>
            <ta e="T1488" id="Seg_1276" s="T1487">then</ta>
            <ta e="T1489" id="Seg_1277" s="T1488">eat-INF.LAT</ta>
            <ta e="T1490" id="Seg_1278" s="T1489">not</ta>
            <ta e="T1491" id="Seg_1279" s="T1490">one.wants</ta>
            <ta e="T1492" id="Seg_1280" s="T1491">then</ta>
            <ta e="T1493" id="Seg_1281" s="T1492">warm.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_1282" s="T1493">become-PST.[3SG]</ta>
            <ta e="T1495" id="Seg_1283" s="T1494">bear.[NOM.SG]</ta>
            <ta e="T1496" id="Seg_1284" s="T1495">say-IPFVZ.[3SG]</ta>
            <ta e="T1497" id="Seg_1285" s="T1496">sit-IMP.2SG</ta>
            <ta e="T1498" id="Seg_1286" s="T1497">I.LAT</ta>
            <ta e="T1499" id="Seg_1287" s="T1498">I.NOM</ta>
            <ta e="T1500" id="Seg_1288" s="T1499">you.NOM</ta>
            <ta e="T1501" id="Seg_1289" s="T1500">bring-RES-FUT-1SG</ta>
            <ta e="T1502" id="Seg_1290" s="T1501">tent-LAT/LOC.3SG</ta>
            <ta e="T1504" id="Seg_1291" s="T1503">then</ta>
            <ta e="T1505" id="Seg_1292" s="T1504">this.[NOM.SG]</ta>
            <ta e="T1506" id="Seg_1293" s="T1505">go-PST.[3SG]</ta>
            <ta e="T1507" id="Seg_1294" s="T1506">tent-LAT/LOC.3SG</ta>
            <ta e="T1508" id="Seg_1295" s="T1507">sit.down-PST.[3SG]</ta>
            <ta e="T1509" id="Seg_1296" s="T1508">bear-LAT</ta>
            <ta e="T1510" id="Seg_1297" s="T1509">bear.[NOM.SG]</ta>
            <ta e="T1511" id="Seg_1298" s="T1510">bring-PST.[3SG]</ta>
            <ta e="T1512" id="Seg_1299" s="T1511">tent-LAT/LOC.3SG</ta>
            <ta e="T1513" id="Seg_1300" s="T1512">and</ta>
            <ta e="T1514" id="Seg_1301" s="T1513">three.[NOM.SG]</ta>
            <ta e="T1515" id="Seg_1302" s="T1514">dog-PL</ta>
            <ta e="T1516" id="Seg_1303" s="T1515">bring-IMP.2SG.O</ta>
            <ta e="T1517" id="Seg_1304" s="T1516">here</ta>
            <ta e="T1518" id="Seg_1305" s="T1517">and</ta>
            <ta e="T1519" id="Seg_1306" s="T1518">bind-IMP.2SG.O</ta>
            <ta e="T1520" id="Seg_1307" s="T1519">I.LAT</ta>
            <ta e="T1521" id="Seg_1308" s="T1520">one.[NOM.SG]</ta>
            <ta e="T1522" id="Seg_1309" s="T1521">son-NOM/GEN.3SG</ta>
            <ta e="T1523" id="Seg_1310" s="T1522">then</ta>
            <ta e="T1524" id="Seg_1311" s="T1523">this.[NOM.SG]</ta>
            <ta e="T1525" id="Seg_1312" s="T1524">come-PST.[3SG]</ta>
            <ta e="T1526" id="Seg_1313" s="T1525">people.[NOM.SG]</ta>
            <ta e="T1527" id="Seg_1314" s="T1526">come-PST-3PL</ta>
            <ta e="T1528" id="Seg_1315" s="T1527">ask-DUR-3PL</ta>
            <ta e="T1529" id="Seg_1316" s="T1528">this.[NOM.SG]</ta>
            <ta e="T1530" id="Seg_1317" s="T1529">what.[NOM.SG]=INDEF</ta>
            <ta e="T1531" id="Seg_1318" s="T1530">NEG</ta>
            <ta e="T1532" id="Seg_1319" s="T1531">speak-PST.[3SG]</ta>
            <ta e="T1533" id="Seg_1320" s="T1532">people-INS</ta>
            <ta e="T1534" id="Seg_1321" s="T1533">then</ta>
            <ta e="T1535" id="Seg_1322" s="T1534">three.[NOM.SG]</ta>
            <ta e="T1536" id="Seg_1323" s="T1535">dog.[NOM.SG]</ta>
            <ta e="T1537" id="Seg_1324" s="T1536">take-PST.[3SG]</ta>
            <ta e="T1538" id="Seg_1325" s="T1537">bear-LAT</ta>
            <ta e="T1539" id="Seg_1326" s="T1538">bring-PST.[3SG]</ta>
            <ta e="T1540" id="Seg_1327" s="T1539">and</ta>
            <ta e="T1541" id="Seg_1328" s="T1540">there</ta>
            <ta e="T1542" id="Seg_1329" s="T1541">send-PST.[3SG]</ta>
            <ta e="T1543" id="Seg_1330" s="T1542">and</ta>
            <ta e="T1544" id="Seg_1331" s="T1543">return-PST.[3SG]</ta>
            <ta e="T1545" id="Seg_1332" s="T1544">tent-LAT/LOC.3SG</ta>
            <ta e="T1546" id="Seg_1333" s="T1545">and</ta>
            <ta e="T1547" id="Seg_1334" s="T1546">when=INDEF</ta>
            <ta e="T1548" id="Seg_1335" s="T1547">NEG</ta>
            <ta e="T1550" id="Seg_1336" s="T1549">NEG</ta>
            <ta e="T1551" id="Seg_1337" s="T1550">scold-DES-PST.[3SG]</ta>
            <ta e="T1552" id="Seg_1338" s="T1551">people-INS</ta>
            <ta e="T1554" id="Seg_1339" s="T1553">good</ta>
            <ta e="T1555" id="Seg_1340" s="T1554">live-INF.LAT</ta>
            <ta e="T1556" id="Seg_1341" s="T1555">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1413" id="Seg_1342" s="T1412">один.[NOM.SG]</ta>
            <ta e="T1414" id="Seg_1343" s="T1413">мужчина.[NOM.SG]</ta>
            <ta e="T1415" id="Seg_1344" s="T1414">быть-PST.[3SG]</ta>
            <ta e="T1416" id="Seg_1345" s="T1415">очень</ta>
            <ta e="T1417" id="Seg_1346" s="T1416">хвастаться-MULT-INF.LAT</ta>
            <ta e="T1418" id="Seg_1347" s="T1417">хороший.[NOM.SG]</ta>
            <ta e="T1419" id="Seg_1348" s="T1418">стать-PST.[3SG]</ta>
            <ta e="T1420" id="Seg_1349" s="T1419">тогда</ta>
            <ta e="T1421" id="Seg_1350" s="T1420">ругать-DES-PST.[3SG]</ta>
            <ta e="T1422" id="Seg_1351" s="T1421">люди-INS</ta>
            <ta e="T1423" id="Seg_1352" s="T1422">тогда</ta>
            <ta e="T1424" id="Seg_1353" s="T1423">идти-PST.[3SG]</ta>
            <ta e="T1425" id="Seg_1354" s="T1424">лес-LAT</ta>
            <ta e="T1426" id="Seg_1355" s="T1425">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1427" id="Seg_1356" s="T1426">я.NOM</ta>
            <ta e="T1428" id="Seg_1357" s="T1427">там</ta>
            <ta e="T1429" id="Seg_1358" s="T1428">медведь-GEN</ta>
            <ta e="T1430" id="Seg_1359" s="T1429">нога-NOM/GEN.3SG</ta>
            <ta e="T1431" id="Seg_1360" s="T1430">видеть-PST-1SG</ta>
            <ta e="T1432" id="Seg_1361" s="T1431">тогда</ta>
            <ta e="T1433" id="Seg_1362" s="T1432">пойти-PST-3PL</ta>
            <ta e="T1434" id="Seg_1363" s="T1433">там</ta>
            <ta e="T1435" id="Seg_1364" s="T1434">след-PL</ta>
            <ta e="T1436" id="Seg_1365" s="T1435">смотреть-FRQ-PST-3PL</ta>
            <ta e="T1437" id="Seg_1366" s="T1436">прийти-PST-3PL</ta>
            <ta e="T1438" id="Seg_1367" s="T1437">большой</ta>
            <ta e="T1439" id="Seg_1368" s="T1438">гора-LAT</ta>
            <ta e="T1440" id="Seg_1369" s="T1439">берлога-LAT</ta>
            <ta e="T1441" id="Seg_1370" s="T1440">медведь-LAT</ta>
            <ta e="T1442" id="Seg_1371" s="T1441">тогда</ta>
            <ta e="T1443" id="Seg_1372" s="T1442">этот.[NOM.SG]</ta>
            <ta e="T1444" id="Seg_1373" s="T1443">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1445" id="Seg_1374" s="T1444">завязать-IMP.2PL</ta>
            <ta e="T1447" id="Seg_1375" s="T1445">ремень</ta>
            <ta e="T1448" id="Seg_1376" s="T1447">и</ta>
            <ta e="T1449" id="Seg_1377" s="T1448">я.ACC</ta>
            <ta e="T1450" id="Seg_1378" s="T1449">послать-MOM-IMP.2PL</ta>
            <ta e="T1451" id="Seg_1379" s="T1450">там</ta>
            <ta e="T1452" id="Seg_1380" s="T1451">этот-PL</ta>
            <ta e="T1453" id="Seg_1381" s="T1452">этот-ACC</ta>
            <ta e="T1454" id="Seg_1382" s="T1453">там</ta>
            <ta e="T1455" id="Seg_1383" s="T1454">послать-MOM-PST-3PL</ta>
            <ta e="T1456" id="Seg_1384" s="T1455">и</ta>
            <ta e="T1" id="Seg_1385" s="T1456">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T1457" id="Seg_1386" s="T1">ремень-PL</ta>
            <ta e="T1460" id="Seg_1387" s="T1457">этот.[NOM.SG]</ta>
            <ta e="T1461" id="Seg_1388" s="T1460">там</ta>
            <ta e="T1462" id="Seg_1389" s="T1461">сидеть-DUR.[3SG]</ta>
            <ta e="T1463" id="Seg_1390" s="T1462">день.[NOM.SG]</ta>
            <ta e="T1464" id="Seg_1391" s="T1463">три.[NOM.SG]</ta>
            <ta e="T1465" id="Seg_1392" s="T1464">день.[NOM.SG]</ta>
            <ta e="T1466" id="Seg_1393" s="T1465">сидеть-MOM-PST.[3SG]</ta>
            <ta e="T1467" id="Seg_1394" s="T1466">съесть-INF.LAT</ta>
            <ta e="T1468" id="Seg_1395" s="T1467">хочется</ta>
            <ta e="T1469" id="Seg_1396" s="T1468">тогда</ta>
            <ta e="T1470" id="Seg_1397" s="T1469">PTCL</ta>
            <ta e="T1471" id="Seg_1398" s="T1470">ремень-PL</ta>
            <ta e="T1472" id="Seg_1399" s="T1471">съесть-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_1400" s="T1472">спать-MOM.[3SG]</ta>
            <ta e="T1474" id="Seg_1401" s="T1473">ползти-PST.[3SG]</ta>
            <ta e="T1475" id="Seg_1402" s="T1474">медведь-LAT</ta>
            <ta e="T1476" id="Seg_1403" s="T1475">берлога-NOM/GEN/ACC.3SG</ta>
            <ta e="T1477" id="Seg_1404" s="T1476">там</ta>
            <ta e="T1478" id="Seg_1405" s="T1477">теплый.[NOM.SG]</ta>
            <ta e="T1479" id="Seg_1406" s="T1478">спать-MOM-PST.[3SG]</ta>
            <ta e="T1480" id="Seg_1407" s="T1479">тогда</ta>
            <ta e="T1481" id="Seg_1408" s="T1480">медведь.[NOM.SG]</ta>
            <ta e="T1482" id="Seg_1409" s="T1481">нога-NOM/GEN.3SG</ta>
            <ta e="T1483" id="Seg_1410" s="T1482">дать-PST.[3SG]</ta>
            <ta e="T1484" id="Seg_1411" s="T1483">этот.[NOM.SG]</ta>
            <ta e="T1485" id="Seg_1412" s="T1484">этот-ACC</ta>
            <ta e="T1486" id="Seg_1413" s="T1485">съесть-PST.[3SG]</ta>
            <ta e="T1487" id="Seg_1414" s="T1486">съесть-PST.[3SG]</ta>
            <ta e="T1488" id="Seg_1415" s="T1487">тогда</ta>
            <ta e="T1489" id="Seg_1416" s="T1488">съесть-INF.LAT</ta>
            <ta e="T1490" id="Seg_1417" s="T1489">не</ta>
            <ta e="T1491" id="Seg_1418" s="T1490">хочется</ta>
            <ta e="T1492" id="Seg_1419" s="T1491">тогда</ta>
            <ta e="T1493" id="Seg_1420" s="T1492">теплый.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_1421" s="T1493">стать-PST.[3SG]</ta>
            <ta e="T1495" id="Seg_1422" s="T1494">медведь.[NOM.SG]</ta>
            <ta e="T1496" id="Seg_1423" s="T1495">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1497" id="Seg_1424" s="T1496">сидеть-IMP.2SG</ta>
            <ta e="T1498" id="Seg_1425" s="T1497">я.LAT</ta>
            <ta e="T1499" id="Seg_1426" s="T1498">я.NOM</ta>
            <ta e="T1500" id="Seg_1427" s="T1499">ты.NOM</ta>
            <ta e="T1501" id="Seg_1428" s="T1500">нести-RES-FUT-1SG</ta>
            <ta e="T1502" id="Seg_1429" s="T1501">чум-LAT/LOC.3SG</ta>
            <ta e="T1504" id="Seg_1430" s="T1503">тогда</ta>
            <ta e="T1505" id="Seg_1431" s="T1504">этот.[NOM.SG]</ta>
            <ta e="T1506" id="Seg_1432" s="T1505">пойти-PST.[3SG]</ta>
            <ta e="T1507" id="Seg_1433" s="T1506">чум-LAT/LOC.3SG</ta>
            <ta e="T1508" id="Seg_1434" s="T1507">сесть-PST.[3SG]</ta>
            <ta e="T1509" id="Seg_1435" s="T1508">медведь-LAT</ta>
            <ta e="T1510" id="Seg_1436" s="T1509">медведь.[NOM.SG]</ta>
            <ta e="T1511" id="Seg_1437" s="T1510">нести-PST.[3SG]</ta>
            <ta e="T1512" id="Seg_1438" s="T1511">чум-LAT/LOC.3SG</ta>
            <ta e="T1513" id="Seg_1439" s="T1512">а</ta>
            <ta e="T1514" id="Seg_1440" s="T1513">три.[NOM.SG]</ta>
            <ta e="T1515" id="Seg_1441" s="T1514">собака-PL</ta>
            <ta e="T1516" id="Seg_1442" s="T1515">принести-IMP.2SG.O</ta>
            <ta e="T1517" id="Seg_1443" s="T1516">здесь</ta>
            <ta e="T1518" id="Seg_1444" s="T1517">и</ta>
            <ta e="T1519" id="Seg_1445" s="T1518">завязать-IMP.2SG.O</ta>
            <ta e="T1520" id="Seg_1446" s="T1519">я.LAT</ta>
            <ta e="T1521" id="Seg_1447" s="T1520">один.[NOM.SG]</ta>
            <ta e="T1522" id="Seg_1448" s="T1521">сын-NOM/GEN.3SG</ta>
            <ta e="T1523" id="Seg_1449" s="T1522">тогда</ta>
            <ta e="T1524" id="Seg_1450" s="T1523">этот.[NOM.SG]</ta>
            <ta e="T1525" id="Seg_1451" s="T1524">прийти-PST.[3SG]</ta>
            <ta e="T1526" id="Seg_1452" s="T1525">люди.[NOM.SG]</ta>
            <ta e="T1527" id="Seg_1453" s="T1526">прийти-PST-3PL</ta>
            <ta e="T1528" id="Seg_1454" s="T1527">спросить-DUR-3PL</ta>
            <ta e="T1529" id="Seg_1455" s="T1528">этот.[NOM.SG]</ta>
            <ta e="T1530" id="Seg_1456" s="T1529">что.[NOM.SG]=INDEF</ta>
            <ta e="T1531" id="Seg_1457" s="T1530">NEG</ta>
            <ta e="T1532" id="Seg_1458" s="T1531">говорить-PST.[3SG]</ta>
            <ta e="T1533" id="Seg_1459" s="T1532">люди-INS</ta>
            <ta e="T1534" id="Seg_1460" s="T1533">тогда</ta>
            <ta e="T1535" id="Seg_1461" s="T1534">три.[NOM.SG]</ta>
            <ta e="T1536" id="Seg_1462" s="T1535">собака.[NOM.SG]</ta>
            <ta e="T1537" id="Seg_1463" s="T1536">взять-PST.[3SG]</ta>
            <ta e="T1538" id="Seg_1464" s="T1537">медведь-LAT</ta>
            <ta e="T1539" id="Seg_1465" s="T1538">нести-PST.[3SG]</ta>
            <ta e="T1540" id="Seg_1466" s="T1539">и</ta>
            <ta e="T1541" id="Seg_1467" s="T1540">там</ta>
            <ta e="T1542" id="Seg_1468" s="T1541">посылать-PST.[3SG]</ta>
            <ta e="T1543" id="Seg_1469" s="T1542">и</ta>
            <ta e="T1544" id="Seg_1470" s="T1543">вернуться-PST.[3SG]</ta>
            <ta e="T1545" id="Seg_1471" s="T1544">чум-LAT/LOC.3SG</ta>
            <ta e="T1546" id="Seg_1472" s="T1545">и</ta>
            <ta e="T1547" id="Seg_1473" s="T1546">когда=INDEF</ta>
            <ta e="T1548" id="Seg_1474" s="T1547">NEG</ta>
            <ta e="T1550" id="Seg_1475" s="T1549">NEG</ta>
            <ta e="T1551" id="Seg_1476" s="T1550">ругать-DES-PST.[3SG]</ta>
            <ta e="T1552" id="Seg_1477" s="T1551">люди-INS</ta>
            <ta e="T1554" id="Seg_1478" s="T1553">хороший</ta>
            <ta e="T1555" id="Seg_1479" s="T1554">жить-INF.LAT</ta>
            <ta e="T1556" id="Seg_1480" s="T1555">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1413" id="Seg_1481" s="T1412">num-n:case</ta>
            <ta e="T1414" id="Seg_1482" s="T1413">n-n:case</ta>
            <ta e="T1415" id="Seg_1483" s="T1414">v-v:tense-v:pn</ta>
            <ta e="T1416" id="Seg_1484" s="T1415">adv</ta>
            <ta e="T1417" id="Seg_1485" s="T1416">v-v&gt;v-v:n.fin</ta>
            <ta e="T1418" id="Seg_1486" s="T1417">adj-n:case</ta>
            <ta e="T1419" id="Seg_1487" s="T1418">v-v:tense-v:pn</ta>
            <ta e="T1420" id="Seg_1488" s="T1419">adv</ta>
            <ta e="T1421" id="Seg_1489" s="T1420">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1422" id="Seg_1490" s="T1421">n-n:case</ta>
            <ta e="T1423" id="Seg_1491" s="T1422">adv</ta>
            <ta e="T1424" id="Seg_1492" s="T1423">v-v:tense-v:pn</ta>
            <ta e="T1425" id="Seg_1493" s="T1424">n-n:case</ta>
            <ta e="T1426" id="Seg_1494" s="T1425">v-v&gt;v-v:pn</ta>
            <ta e="T1427" id="Seg_1495" s="T1426">pers</ta>
            <ta e="T1428" id="Seg_1496" s="T1427">adv</ta>
            <ta e="T1429" id="Seg_1497" s="T1428">n-n:case</ta>
            <ta e="T1430" id="Seg_1498" s="T1429">n-n:case.poss</ta>
            <ta e="T1431" id="Seg_1499" s="T1430">v-v:tense-v:pn</ta>
            <ta e="T1432" id="Seg_1500" s="T1431">adv</ta>
            <ta e="T1433" id="Seg_1501" s="T1432">v-v:tense-v:pn</ta>
            <ta e="T1434" id="Seg_1502" s="T1433">adv</ta>
            <ta e="T1435" id="Seg_1503" s="T1434">n-n:num</ta>
            <ta e="T1436" id="Seg_1504" s="T1435">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1437" id="Seg_1505" s="T1436">v-v:tense-v:pn</ta>
            <ta e="T1438" id="Seg_1506" s="T1437">adj</ta>
            <ta e="T1439" id="Seg_1507" s="T1438">n-n:case</ta>
            <ta e="T1440" id="Seg_1508" s="T1439">n-n:case</ta>
            <ta e="T1441" id="Seg_1509" s="T1440">n-n:case</ta>
            <ta e="T1442" id="Seg_1510" s="T1441">adv</ta>
            <ta e="T1443" id="Seg_1511" s="T1442">dempro-n:case</ta>
            <ta e="T1444" id="Seg_1512" s="T1443">v-v&gt;v-v:pn</ta>
            <ta e="T1445" id="Seg_1513" s="T1444">v-v:mood.pn</ta>
            <ta e="T1447" id="Seg_1514" s="T1445">n</ta>
            <ta e="T1448" id="Seg_1515" s="T1447">conj</ta>
            <ta e="T1449" id="Seg_1516" s="T1448">pers</ta>
            <ta e="T1450" id="Seg_1517" s="T1449">v-v&gt;v-v:mood.pn</ta>
            <ta e="T1451" id="Seg_1518" s="T1450">adv</ta>
            <ta e="T1452" id="Seg_1519" s="T1451">dempro-n:num</ta>
            <ta e="T1453" id="Seg_1520" s="T1452">dempro-n:case</ta>
            <ta e="T1454" id="Seg_1521" s="T1453">adv</ta>
            <ta e="T1455" id="Seg_1522" s="T1454">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1456" id="Seg_1523" s="T1455">conj</ta>
            <ta e="T1" id="Seg_1524" s="T1456">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1457" id="Seg_1525" s="T1">n-n:num</ta>
            <ta e="T1460" id="Seg_1526" s="T1457">dempro-n:case</ta>
            <ta e="T1461" id="Seg_1527" s="T1460">adv</ta>
            <ta e="T1462" id="Seg_1528" s="T1461">v-v&gt;v-v:pn</ta>
            <ta e="T1463" id="Seg_1529" s="T1462">n-n:case</ta>
            <ta e="T1464" id="Seg_1530" s="T1463">num-n:case</ta>
            <ta e="T1465" id="Seg_1531" s="T1464">n-n:case</ta>
            <ta e="T1466" id="Seg_1532" s="T1465">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1467" id="Seg_1533" s="T1466">v-v:n.fin</ta>
            <ta e="T1468" id="Seg_1534" s="T1467">ptcl</ta>
            <ta e="T1469" id="Seg_1535" s="T1468">adv</ta>
            <ta e="T1470" id="Seg_1536" s="T1469">ptcl</ta>
            <ta e="T1471" id="Seg_1537" s="T1470">n-n:num</ta>
            <ta e="T1472" id="Seg_1538" s="T1471">v-v:tense-v:pn</ta>
            <ta e="T1473" id="Seg_1539" s="T1472">v-v&gt;v-v:pn</ta>
            <ta e="T1474" id="Seg_1540" s="T1473">v-v:tense-v:pn</ta>
            <ta e="T1475" id="Seg_1541" s="T1474">n-n:case</ta>
            <ta e="T1476" id="Seg_1542" s="T1475">n-n:case.poss</ta>
            <ta e="T1477" id="Seg_1543" s="T1476">adv</ta>
            <ta e="T1478" id="Seg_1544" s="T1477">adj-n:case</ta>
            <ta e="T1479" id="Seg_1545" s="T1478">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1480" id="Seg_1546" s="T1479">adv</ta>
            <ta e="T1481" id="Seg_1547" s="T1480">n-n:case</ta>
            <ta e="T1482" id="Seg_1548" s="T1481">n-n:case.poss</ta>
            <ta e="T1483" id="Seg_1549" s="T1482">v-v:tense-v:pn</ta>
            <ta e="T1484" id="Seg_1550" s="T1483">dempro-n:case</ta>
            <ta e="T1485" id="Seg_1551" s="T1484">dempro-n:case</ta>
            <ta e="T1486" id="Seg_1552" s="T1485">v-v:tense-v:pn</ta>
            <ta e="T1487" id="Seg_1553" s="T1486">v-v:tense-v:pn</ta>
            <ta e="T1488" id="Seg_1554" s="T1487">adv</ta>
            <ta e="T1489" id="Seg_1555" s="T1488">v-v:n.fin</ta>
            <ta e="T1490" id="Seg_1556" s="T1489">ptcl</ta>
            <ta e="T1491" id="Seg_1557" s="T1490">ptcl</ta>
            <ta e="T1492" id="Seg_1558" s="T1491">adv</ta>
            <ta e="T1493" id="Seg_1559" s="T1492">adj-n:case</ta>
            <ta e="T1494" id="Seg_1560" s="T1493">v-v:tense-v:pn</ta>
            <ta e="T1495" id="Seg_1561" s="T1494">n-n:case</ta>
            <ta e="T1496" id="Seg_1562" s="T1495">v-v&gt;v-v:pn</ta>
            <ta e="T1497" id="Seg_1563" s="T1496">v-v:mood.pn</ta>
            <ta e="T1498" id="Seg_1564" s="T1497">pers</ta>
            <ta e="T1499" id="Seg_1565" s="T1498">pers</ta>
            <ta e="T1500" id="Seg_1566" s="T1499">pers</ta>
            <ta e="T1501" id="Seg_1567" s="T1500">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1502" id="Seg_1568" s="T1501">n-n:case.poss</ta>
            <ta e="T1504" id="Seg_1569" s="T1503">adv</ta>
            <ta e="T1505" id="Seg_1570" s="T1504">dempro-n:case</ta>
            <ta e="T1506" id="Seg_1571" s="T1505">v-v:tense-v:pn</ta>
            <ta e="T1507" id="Seg_1572" s="T1506">n-n:case.poss</ta>
            <ta e="T1508" id="Seg_1573" s="T1507">v-v:tense-v:pn</ta>
            <ta e="T1509" id="Seg_1574" s="T1508">n-n:case</ta>
            <ta e="T1510" id="Seg_1575" s="T1509">n-n:case</ta>
            <ta e="T1511" id="Seg_1576" s="T1510">v-v:tense-v:pn</ta>
            <ta e="T1512" id="Seg_1577" s="T1511">n-n:case.poss</ta>
            <ta e="T1513" id="Seg_1578" s="T1512">conj</ta>
            <ta e="T1514" id="Seg_1579" s="T1513">num-n:case</ta>
            <ta e="T1515" id="Seg_1580" s="T1514">n-n:num</ta>
            <ta e="T1516" id="Seg_1581" s="T1515">v-v:mood.pn</ta>
            <ta e="T1517" id="Seg_1582" s="T1516">adv</ta>
            <ta e="T1518" id="Seg_1583" s="T1517">conj</ta>
            <ta e="T1519" id="Seg_1584" s="T1518">v-v:mood.pn</ta>
            <ta e="T1520" id="Seg_1585" s="T1519">pers</ta>
            <ta e="T1521" id="Seg_1586" s="T1520">num-n:case</ta>
            <ta e="T1522" id="Seg_1587" s="T1521">n-n:case.poss</ta>
            <ta e="T1523" id="Seg_1588" s="T1522">adv</ta>
            <ta e="T1524" id="Seg_1589" s="T1523">dempro-n:case</ta>
            <ta e="T1525" id="Seg_1590" s="T1524">v-v:tense-v:pn</ta>
            <ta e="T1526" id="Seg_1591" s="T1525">n-n:case</ta>
            <ta e="T1527" id="Seg_1592" s="T1526">v-v:tense-v:pn</ta>
            <ta e="T1528" id="Seg_1593" s="T1527">v-v&gt;v-v:pn</ta>
            <ta e="T1529" id="Seg_1594" s="T1528">dempro-n:case</ta>
            <ta e="T1530" id="Seg_1595" s="T1529">que-n:case=ptcl</ta>
            <ta e="T1531" id="Seg_1596" s="T1530">ptcl</ta>
            <ta e="T1532" id="Seg_1597" s="T1531">v-v:tense-v:pn</ta>
            <ta e="T1533" id="Seg_1598" s="T1532">n-n:case</ta>
            <ta e="T1534" id="Seg_1599" s="T1533">adv</ta>
            <ta e="T1535" id="Seg_1600" s="T1534">num-n:case</ta>
            <ta e="T1536" id="Seg_1601" s="T1535">n-n:case</ta>
            <ta e="T1537" id="Seg_1602" s="T1536">v-v:tense-v:pn</ta>
            <ta e="T1538" id="Seg_1603" s="T1537">n-n:case</ta>
            <ta e="T1539" id="Seg_1604" s="T1538">v-v:tense-v:pn</ta>
            <ta e="T1540" id="Seg_1605" s="T1539">conj</ta>
            <ta e="T1541" id="Seg_1606" s="T1540">adv</ta>
            <ta e="T1542" id="Seg_1607" s="T1541">v-v:tense-v:pn</ta>
            <ta e="T1543" id="Seg_1608" s="T1542">conj</ta>
            <ta e="T1544" id="Seg_1609" s="T1543">v-v:tense-v:pn</ta>
            <ta e="T1545" id="Seg_1610" s="T1544">n-n:case.poss</ta>
            <ta e="T1546" id="Seg_1611" s="T1545">conj</ta>
            <ta e="T1547" id="Seg_1612" s="T1546">que=ptcl</ta>
            <ta e="T1548" id="Seg_1613" s="T1547">ptcl</ta>
            <ta e="T1550" id="Seg_1614" s="T1549">ptcl</ta>
            <ta e="T1551" id="Seg_1615" s="T1550">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1552" id="Seg_1616" s="T1551">n-n:case</ta>
            <ta e="T1554" id="Seg_1617" s="T1553">adj</ta>
            <ta e="T1555" id="Seg_1618" s="T1554">v-v:n.fin</ta>
            <ta e="T1556" id="Seg_1619" s="T1555">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1413" id="Seg_1620" s="T1412">num</ta>
            <ta e="T1414" id="Seg_1621" s="T1413">n</ta>
            <ta e="T1415" id="Seg_1622" s="T1414">v</ta>
            <ta e="T1416" id="Seg_1623" s="T1415">adv</ta>
            <ta e="T1417" id="Seg_1624" s="T1416">v</ta>
            <ta e="T1418" id="Seg_1625" s="T1417">adj</ta>
            <ta e="T1419" id="Seg_1626" s="T1418">v</ta>
            <ta e="T1420" id="Seg_1627" s="T1419">adv</ta>
            <ta e="T1421" id="Seg_1628" s="T1420">v</ta>
            <ta e="T1422" id="Seg_1629" s="T1421">n</ta>
            <ta e="T1423" id="Seg_1630" s="T1422">adv</ta>
            <ta e="T1424" id="Seg_1631" s="T1423">v</ta>
            <ta e="T1425" id="Seg_1632" s="T1424">n</ta>
            <ta e="T1426" id="Seg_1633" s="T1425">v</ta>
            <ta e="T1427" id="Seg_1634" s="T1426">pers</ta>
            <ta e="T1428" id="Seg_1635" s="T1427">adv</ta>
            <ta e="T1429" id="Seg_1636" s="T1428">n</ta>
            <ta e="T1430" id="Seg_1637" s="T1429">n</ta>
            <ta e="T1431" id="Seg_1638" s="T1430">v</ta>
            <ta e="T1432" id="Seg_1639" s="T1431">adv</ta>
            <ta e="T1433" id="Seg_1640" s="T1432">v</ta>
            <ta e="T1434" id="Seg_1641" s="T1433">adv</ta>
            <ta e="T1435" id="Seg_1642" s="T1434">n</ta>
            <ta e="T1436" id="Seg_1643" s="T1435">v</ta>
            <ta e="T1437" id="Seg_1644" s="T1436">v</ta>
            <ta e="T1438" id="Seg_1645" s="T1437">adj</ta>
            <ta e="T1439" id="Seg_1646" s="T1438">n</ta>
            <ta e="T1440" id="Seg_1647" s="T1439">n</ta>
            <ta e="T1441" id="Seg_1648" s="T1440">n</ta>
            <ta e="T1442" id="Seg_1649" s="T1441">adv</ta>
            <ta e="T1443" id="Seg_1650" s="T1442">dempro</ta>
            <ta e="T1444" id="Seg_1651" s="T1443">v</ta>
            <ta e="T1445" id="Seg_1652" s="T1444">v</ta>
            <ta e="T1447" id="Seg_1653" s="T1445">n</ta>
            <ta e="T1448" id="Seg_1654" s="T1447">conj</ta>
            <ta e="T1449" id="Seg_1655" s="T1448">pers</ta>
            <ta e="T1450" id="Seg_1656" s="T1449">v</ta>
            <ta e="T1451" id="Seg_1657" s="T1450">adv</ta>
            <ta e="T1452" id="Seg_1658" s="T1451">dempro</ta>
            <ta e="T1453" id="Seg_1659" s="T1452">dempro</ta>
            <ta e="T1454" id="Seg_1660" s="T1453">adv</ta>
            <ta e="T1455" id="Seg_1661" s="T1454">v</ta>
            <ta e="T1456" id="Seg_1662" s="T1455">conj</ta>
            <ta e="T1" id="Seg_1663" s="T1456">v</ta>
            <ta e="T1457" id="Seg_1664" s="T1">n</ta>
            <ta e="T1460" id="Seg_1665" s="T1457">dempro</ta>
            <ta e="T1461" id="Seg_1666" s="T1460">adv</ta>
            <ta e="T1462" id="Seg_1667" s="T1461">v</ta>
            <ta e="T1463" id="Seg_1668" s="T1462">n</ta>
            <ta e="T1464" id="Seg_1669" s="T1463">num</ta>
            <ta e="T1465" id="Seg_1670" s="T1464">n</ta>
            <ta e="T1466" id="Seg_1671" s="T1465">v</ta>
            <ta e="T1467" id="Seg_1672" s="T1466">v</ta>
            <ta e="T1468" id="Seg_1673" s="T1467">ptcl</ta>
            <ta e="T1469" id="Seg_1674" s="T1468">adv</ta>
            <ta e="T1470" id="Seg_1675" s="T1469">ptcl</ta>
            <ta e="T1471" id="Seg_1676" s="T1470">n</ta>
            <ta e="T1472" id="Seg_1677" s="T1471">v</ta>
            <ta e="T1473" id="Seg_1678" s="T1472">v</ta>
            <ta e="T1474" id="Seg_1679" s="T1473">v</ta>
            <ta e="T1475" id="Seg_1680" s="T1474">n</ta>
            <ta e="T1476" id="Seg_1681" s="T1475">n</ta>
            <ta e="T1477" id="Seg_1682" s="T1476">adv</ta>
            <ta e="T1478" id="Seg_1683" s="T1477">adj</ta>
            <ta e="T1479" id="Seg_1684" s="T1478">v</ta>
            <ta e="T1480" id="Seg_1685" s="T1479">adv</ta>
            <ta e="T1481" id="Seg_1686" s="T1480">n</ta>
            <ta e="T1482" id="Seg_1687" s="T1481">n</ta>
            <ta e="T1483" id="Seg_1688" s="T1482">v</ta>
            <ta e="T1484" id="Seg_1689" s="T1483">dempro</ta>
            <ta e="T1485" id="Seg_1690" s="T1484">dempro</ta>
            <ta e="T1486" id="Seg_1691" s="T1485">v</ta>
            <ta e="T1487" id="Seg_1692" s="T1486">v</ta>
            <ta e="T1488" id="Seg_1693" s="T1487">adv</ta>
            <ta e="T1489" id="Seg_1694" s="T1488">v</ta>
            <ta e="T1490" id="Seg_1695" s="T1489">ptcl</ta>
            <ta e="T1491" id="Seg_1696" s="T1490">ptcl</ta>
            <ta e="T1492" id="Seg_1697" s="T1491">adv</ta>
            <ta e="T1493" id="Seg_1698" s="T1492">adj</ta>
            <ta e="T1494" id="Seg_1699" s="T1493">v</ta>
            <ta e="T1495" id="Seg_1700" s="T1494">n</ta>
            <ta e="T1496" id="Seg_1701" s="T1495">v</ta>
            <ta e="T1497" id="Seg_1702" s="T1496">v</ta>
            <ta e="T1498" id="Seg_1703" s="T1497">pers</ta>
            <ta e="T1499" id="Seg_1704" s="T1498">pers</ta>
            <ta e="T1500" id="Seg_1705" s="T1499">pers</ta>
            <ta e="T1501" id="Seg_1706" s="T1500">v</ta>
            <ta e="T1502" id="Seg_1707" s="T1501">n</ta>
            <ta e="T1504" id="Seg_1708" s="T1503">adv</ta>
            <ta e="T1505" id="Seg_1709" s="T1504">dempro</ta>
            <ta e="T1506" id="Seg_1710" s="T1505">v</ta>
            <ta e="T1507" id="Seg_1711" s="T1506">n</ta>
            <ta e="T1508" id="Seg_1712" s="T1507">v</ta>
            <ta e="T1509" id="Seg_1713" s="T1508">n</ta>
            <ta e="T1510" id="Seg_1714" s="T1509">n</ta>
            <ta e="T1511" id="Seg_1715" s="T1510">v</ta>
            <ta e="T1512" id="Seg_1716" s="T1511">n</ta>
            <ta e="T1513" id="Seg_1717" s="T1512">conj</ta>
            <ta e="T1514" id="Seg_1718" s="T1513">num</ta>
            <ta e="T1515" id="Seg_1719" s="T1514">n</ta>
            <ta e="T1516" id="Seg_1720" s="T1515">v</ta>
            <ta e="T1517" id="Seg_1721" s="T1516">adv</ta>
            <ta e="T1518" id="Seg_1722" s="T1517">conj</ta>
            <ta e="T1519" id="Seg_1723" s="T1518">v</ta>
            <ta e="T1520" id="Seg_1724" s="T1519">pers</ta>
            <ta e="T1521" id="Seg_1725" s="T1520">num</ta>
            <ta e="T1522" id="Seg_1726" s="T1521">n</ta>
            <ta e="T1523" id="Seg_1727" s="T1522">adv</ta>
            <ta e="T1524" id="Seg_1728" s="T1523">dempro</ta>
            <ta e="T1525" id="Seg_1729" s="T1524">v</ta>
            <ta e="T1526" id="Seg_1730" s="T1525">n</ta>
            <ta e="T1527" id="Seg_1731" s="T1526">v</ta>
            <ta e="T1528" id="Seg_1732" s="T1527">v</ta>
            <ta e="T1529" id="Seg_1733" s="T1528">dempro</ta>
            <ta e="T1530" id="Seg_1734" s="T1529">que</ta>
            <ta e="T1531" id="Seg_1735" s="T1530">ptcl</ta>
            <ta e="T1532" id="Seg_1736" s="T1531">v</ta>
            <ta e="T1533" id="Seg_1737" s="T1532">n</ta>
            <ta e="T1534" id="Seg_1738" s="T1533">adv</ta>
            <ta e="T1535" id="Seg_1739" s="T1534">num</ta>
            <ta e="T1536" id="Seg_1740" s="T1535">n</ta>
            <ta e="T1537" id="Seg_1741" s="T1536">v</ta>
            <ta e="T1538" id="Seg_1742" s="T1537">n</ta>
            <ta e="T1539" id="Seg_1743" s="T1538">v</ta>
            <ta e="T1540" id="Seg_1744" s="T1539">conj</ta>
            <ta e="T1541" id="Seg_1745" s="T1540">adv</ta>
            <ta e="T1542" id="Seg_1746" s="T1541">v</ta>
            <ta e="T1543" id="Seg_1747" s="T1542">conj</ta>
            <ta e="T1544" id="Seg_1748" s="T1543">v</ta>
            <ta e="T1545" id="Seg_1749" s="T1544">n</ta>
            <ta e="T1546" id="Seg_1750" s="T1545">conj</ta>
            <ta e="T1547" id="Seg_1751" s="T1546">que</ta>
            <ta e="T1548" id="Seg_1752" s="T1547">ptcl</ta>
            <ta e="T1550" id="Seg_1753" s="T1549">ptcl</ta>
            <ta e="T1551" id="Seg_1754" s="T1550">v</ta>
            <ta e="T1552" id="Seg_1755" s="T1551">n</ta>
            <ta e="T1554" id="Seg_1756" s="T1553">adj</ta>
            <ta e="T1555" id="Seg_1757" s="T1554">v</ta>
            <ta e="T1556" id="Seg_1758" s="T1555">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1414" id="Seg_1759" s="T1413">np.h:E</ta>
            <ta e="T1419" id="Seg_1760" s="T1418">0.3.h:A</ta>
            <ta e="T1420" id="Seg_1761" s="T1419">adv:Time</ta>
            <ta e="T1421" id="Seg_1762" s="T1420">0.3.h:A</ta>
            <ta e="T1422" id="Seg_1763" s="T1421">np.h:Com</ta>
            <ta e="T1423" id="Seg_1764" s="T1422">adv:Time</ta>
            <ta e="T1424" id="Seg_1765" s="T1423">0.3.h:A</ta>
            <ta e="T1425" id="Seg_1766" s="T1424">np:G</ta>
            <ta e="T1426" id="Seg_1767" s="T1425">0.3.h:A</ta>
            <ta e="T1427" id="Seg_1768" s="T1426">pro.h:E</ta>
            <ta e="T1428" id="Seg_1769" s="T1427">adv:L</ta>
            <ta e="T1429" id="Seg_1770" s="T1428">np.h:Poss</ta>
            <ta e="T1430" id="Seg_1771" s="T1429">np:Th</ta>
            <ta e="T1432" id="Seg_1772" s="T1431">adv:Time</ta>
            <ta e="T1433" id="Seg_1773" s="T1432">0.3.h:A</ta>
            <ta e="T1434" id="Seg_1774" s="T1433">adv:L</ta>
            <ta e="T1435" id="Seg_1775" s="T1434">np:Th</ta>
            <ta e="T1436" id="Seg_1776" s="T1435">0.3.h:A</ta>
            <ta e="T1437" id="Seg_1777" s="T1436">0.3.h:A</ta>
            <ta e="T1439" id="Seg_1778" s="T1438">np:G</ta>
            <ta e="T1440" id="Seg_1779" s="T1439">np:G</ta>
            <ta e="T1441" id="Seg_1780" s="T1440">np:G</ta>
            <ta e="T1442" id="Seg_1781" s="T1441">adv:Time</ta>
            <ta e="T1443" id="Seg_1782" s="T1442">pro.h:A</ta>
            <ta e="T1445" id="Seg_1783" s="T1444">0.2.h:A</ta>
            <ta e="T1447" id="Seg_1784" s="T1445">np:P</ta>
            <ta e="T1449" id="Seg_1785" s="T1448">pro.h:Th</ta>
            <ta e="T1450" id="Seg_1786" s="T1449">0.2.h:A</ta>
            <ta e="T1451" id="Seg_1787" s="T1450">adv:L</ta>
            <ta e="T1452" id="Seg_1788" s="T1451">pro.h:A</ta>
            <ta e="T1453" id="Seg_1789" s="T1452">pro.h:Th</ta>
            <ta e="T1454" id="Seg_1790" s="T1453">adv:L</ta>
            <ta e="T1" id="Seg_1791" s="T1456">0.3.h:A</ta>
            <ta e="T1457" id="Seg_1792" s="T1">np:Th</ta>
            <ta e="T1460" id="Seg_1793" s="T1457">pro.h:E</ta>
            <ta e="T1461" id="Seg_1794" s="T1460">adv:L</ta>
            <ta e="T1463" id="Seg_1795" s="T1462">n:Time</ta>
            <ta e="T1465" id="Seg_1796" s="T1464">n:Time</ta>
            <ta e="T1466" id="Seg_1797" s="T1465">0.3.h:E</ta>
            <ta e="T1469" id="Seg_1798" s="T1468">adv:Time</ta>
            <ta e="T1471" id="Seg_1799" s="T1470">np:P</ta>
            <ta e="T1472" id="Seg_1800" s="T1471">0.3.h:A</ta>
            <ta e="T1473" id="Seg_1801" s="T1472">0.3.h:E</ta>
            <ta e="T1474" id="Seg_1802" s="T1473">0.3.h:A</ta>
            <ta e="T1475" id="Seg_1803" s="T1474">np.h:Poss</ta>
            <ta e="T1476" id="Seg_1804" s="T1475">np:G</ta>
            <ta e="T1477" id="Seg_1805" s="T1476">adv:L</ta>
            <ta e="T1479" id="Seg_1806" s="T1478">0.3.h:E</ta>
            <ta e="T1480" id="Seg_1807" s="T1479">adv:Time</ta>
            <ta e="T1481" id="Seg_1808" s="T1480">np.h:A</ta>
            <ta e="T1482" id="Seg_1809" s="T1481">np:P</ta>
            <ta e="T1484" id="Seg_1810" s="T1483">pro.h:A</ta>
            <ta e="T1485" id="Seg_1811" s="T1484">pro:Th</ta>
            <ta e="T1487" id="Seg_1812" s="T1486">0.3.h:A</ta>
            <ta e="T1488" id="Seg_1813" s="T1487">adv:Time</ta>
            <ta e="T1492" id="Seg_1814" s="T1491">adv:Time</ta>
            <ta e="T1495" id="Seg_1815" s="T1494">np.h:A</ta>
            <ta e="T1497" id="Seg_1816" s="T1496">0.2.h:A</ta>
            <ta e="T1498" id="Seg_1817" s="T1497">pro:L</ta>
            <ta e="T1499" id="Seg_1818" s="T1498">pro.h:A</ta>
            <ta e="T1500" id="Seg_1819" s="T1499">pro.h:Th</ta>
            <ta e="T1502" id="Seg_1820" s="T1501">np:G</ta>
            <ta e="T1504" id="Seg_1821" s="T1503">adv:Time</ta>
            <ta e="T1505" id="Seg_1822" s="T1504">pro.h:A</ta>
            <ta e="T1507" id="Seg_1823" s="T1506">np:G</ta>
            <ta e="T1508" id="Seg_1824" s="T1507">0.3.h:A</ta>
            <ta e="T1509" id="Seg_1825" s="T1508">np:G</ta>
            <ta e="T1510" id="Seg_1826" s="T1509">np.h:A</ta>
            <ta e="T1512" id="Seg_1827" s="T1511">np:G</ta>
            <ta e="T1515" id="Seg_1828" s="T1514">np:Th</ta>
            <ta e="T1516" id="Seg_1829" s="T1515">0.2.h:A</ta>
            <ta e="T1517" id="Seg_1830" s="T1516">adv:L</ta>
            <ta e="T1519" id="Seg_1831" s="T1518">0.2.h:A</ta>
            <ta e="T1520" id="Seg_1832" s="T1519">pro.h:B</ta>
            <ta e="T1522" id="Seg_1833" s="T1521">np:Th</ta>
            <ta e="T1523" id="Seg_1834" s="T1522">adv:Time</ta>
            <ta e="T1524" id="Seg_1835" s="T1523">pro.h:A</ta>
            <ta e="T1526" id="Seg_1836" s="T1525">np.h:A</ta>
            <ta e="T1528" id="Seg_1837" s="T1527">0.3.h:A</ta>
            <ta e="T1529" id="Seg_1838" s="T1528">pro.h:A</ta>
            <ta e="T1530" id="Seg_1839" s="T1529">pro:Th</ta>
            <ta e="T1533" id="Seg_1840" s="T1532">np.h:R</ta>
            <ta e="T1534" id="Seg_1841" s="T1533">adv:Time</ta>
            <ta e="T1536" id="Seg_1842" s="T1535">np:Th</ta>
            <ta e="T1537" id="Seg_1843" s="T1536">0.3.h:A</ta>
            <ta e="T1538" id="Seg_1844" s="T1537">np:G</ta>
            <ta e="T1539" id="Seg_1845" s="T1538">0.3.h:A</ta>
            <ta e="T1541" id="Seg_1846" s="T1540">adv:L</ta>
            <ta e="T1542" id="Seg_1847" s="T1541">0.3.h:A</ta>
            <ta e="T1544" id="Seg_1848" s="T1543">0.3.h:A</ta>
            <ta e="T1545" id="Seg_1849" s="T1544">np:G</ta>
            <ta e="T1551" id="Seg_1850" s="T1550">0.3.h:A</ta>
            <ta e="T1552" id="Seg_1851" s="T1551">np.h:Com</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1414" id="Seg_1852" s="T1413">np.h:S</ta>
            <ta e="T1415" id="Seg_1853" s="T1414">v:pred</ta>
            <ta e="T1418" id="Seg_1854" s="T1417">adj:pred</ta>
            <ta e="T1419" id="Seg_1855" s="T1418">cop 0.3.h:S</ta>
            <ta e="T1421" id="Seg_1856" s="T1420">v:pred 0.3.h:S</ta>
            <ta e="T1424" id="Seg_1857" s="T1423">v:pred 0.3.h:S</ta>
            <ta e="T1426" id="Seg_1858" s="T1425">v:pred 0.3.h:S</ta>
            <ta e="T1427" id="Seg_1859" s="T1426">pro.h:S</ta>
            <ta e="T1430" id="Seg_1860" s="T1429">np:O</ta>
            <ta e="T1431" id="Seg_1861" s="T1430">v:pred</ta>
            <ta e="T1433" id="Seg_1862" s="T1432">v:pred 0.3.h:S</ta>
            <ta e="T1435" id="Seg_1863" s="T1434">np:O</ta>
            <ta e="T1436" id="Seg_1864" s="T1435">v:pred 0.3.h:S</ta>
            <ta e="T1437" id="Seg_1865" s="T1436">v:pred 0.3.h:S</ta>
            <ta e="T1443" id="Seg_1866" s="T1442">pro.h:S</ta>
            <ta e="T1444" id="Seg_1867" s="T1443">v:pred</ta>
            <ta e="T1445" id="Seg_1868" s="T1444">v:pred 0.2.h:S</ta>
            <ta e="T1447" id="Seg_1869" s="T1445">np:O</ta>
            <ta e="T1449" id="Seg_1870" s="T1448">pro.h:O</ta>
            <ta e="T1450" id="Seg_1871" s="T1449">v:pred 0.2.h:S</ta>
            <ta e="T1452" id="Seg_1872" s="T1451">pro.h:S</ta>
            <ta e="T1453" id="Seg_1873" s="T1452">pro.h:O</ta>
            <ta e="T1455" id="Seg_1874" s="T1454">v:pred</ta>
            <ta e="T1" id="Seg_1875" s="T1456">v:pred 0.3.h:S</ta>
            <ta e="T1457" id="Seg_1876" s="T1">np:O</ta>
            <ta e="T1460" id="Seg_1877" s="T1457">pro.h:S</ta>
            <ta e="T1462" id="Seg_1878" s="T1461">v:pred</ta>
            <ta e="T1466" id="Seg_1879" s="T1465">v:pred 0.3.h:S</ta>
            <ta e="T1468" id="Seg_1880" s="T1467">ptcl:pred</ta>
            <ta e="T1471" id="Seg_1881" s="T1470">np:O</ta>
            <ta e="T1472" id="Seg_1882" s="T1471">v:pred 0.3.h:S</ta>
            <ta e="T1473" id="Seg_1883" s="T1472">v:pred 0.3.h:S</ta>
            <ta e="T1474" id="Seg_1884" s="T1473">v:pred 0.3.h:S</ta>
            <ta e="T1478" id="Seg_1885" s="T1477">adj:pred</ta>
            <ta e="T1479" id="Seg_1886" s="T1478">v:pred 0.3.h:S</ta>
            <ta e="T1481" id="Seg_1887" s="T1480">np.h:S</ta>
            <ta e="T1482" id="Seg_1888" s="T1481">np:O</ta>
            <ta e="T1483" id="Seg_1889" s="T1482">v:pred</ta>
            <ta e="T1484" id="Seg_1890" s="T1483">pro.h:S</ta>
            <ta e="T1485" id="Seg_1891" s="T1484">pro:O</ta>
            <ta e="T1486" id="Seg_1892" s="T1485">v:pred</ta>
            <ta e="T1487" id="Seg_1893" s="T1486">v:pred 0.3.h:S</ta>
            <ta e="T1490" id="Seg_1894" s="T1489">ptcl.neg</ta>
            <ta e="T1491" id="Seg_1895" s="T1490">ptcl:pred</ta>
            <ta e="T1493" id="Seg_1896" s="T1492">adj:pred</ta>
            <ta e="T1494" id="Seg_1897" s="T1493">v:pred 0.3:S</ta>
            <ta e="T1495" id="Seg_1898" s="T1494">np.h:S</ta>
            <ta e="T1496" id="Seg_1899" s="T1495">v:pred</ta>
            <ta e="T1497" id="Seg_1900" s="T1496">v:pred 0.2.h:S</ta>
            <ta e="T1499" id="Seg_1901" s="T1498">pro.h:S</ta>
            <ta e="T1500" id="Seg_1902" s="T1499">pro.h:O</ta>
            <ta e="T1501" id="Seg_1903" s="T1500">v:pred</ta>
            <ta e="T1505" id="Seg_1904" s="T1504">pro.h:S</ta>
            <ta e="T1506" id="Seg_1905" s="T1505">v:pred</ta>
            <ta e="T1508" id="Seg_1906" s="T1507">v:pred 0.3.h:S</ta>
            <ta e="T1510" id="Seg_1907" s="T1509">np.h:S</ta>
            <ta e="T1511" id="Seg_1908" s="T1510">v:pred</ta>
            <ta e="T1515" id="Seg_1909" s="T1514">np:O</ta>
            <ta e="T1516" id="Seg_1910" s="T1515">v:pred 0.2.h:S</ta>
            <ta e="T1519" id="Seg_1911" s="T1518">v:pred 0.2.h:S</ta>
            <ta e="T1522" id="Seg_1912" s="T1521">np:S</ta>
            <ta e="T1524" id="Seg_1913" s="T1523">pro.h:S</ta>
            <ta e="T1525" id="Seg_1914" s="T1524">v:pred</ta>
            <ta e="T1526" id="Seg_1915" s="T1525">np.h:S</ta>
            <ta e="T1527" id="Seg_1916" s="T1526">v:pred</ta>
            <ta e="T1528" id="Seg_1917" s="T1527">v:pred 0.3.h:S</ta>
            <ta e="T1529" id="Seg_1918" s="T1528">pro.h:S</ta>
            <ta e="T1530" id="Seg_1919" s="T1529">pro:O</ta>
            <ta e="T1531" id="Seg_1920" s="T1530">ptcl.neg</ta>
            <ta e="T1532" id="Seg_1921" s="T1531">v:pred</ta>
            <ta e="T1536" id="Seg_1922" s="T1535">np:O</ta>
            <ta e="T1537" id="Seg_1923" s="T1536">v:pred 0.3.h:S</ta>
            <ta e="T1539" id="Seg_1924" s="T1538">v:pred 0.3.h:S</ta>
            <ta e="T1542" id="Seg_1925" s="T1541">v:pred 0.3.h:S</ta>
            <ta e="T1544" id="Seg_1926" s="T1543">v:pred 0.3.h:S</ta>
            <ta e="T1550" id="Seg_1927" s="T1549">ptcl.neg</ta>
            <ta e="T1551" id="Seg_1928" s="T1550">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1418" id="Seg_1929" s="T1417">TURK:core</ta>
            <ta e="T1435" id="Seg_1930" s="T1434">RUS:core</ta>
            <ta e="T1440" id="Seg_1931" s="T1439">RUS:cult</ta>
            <ta e="T1447" id="Seg_1932" s="T1445">RUS:cult</ta>
            <ta e="T1448" id="Seg_1933" s="T1447">RUS:gram</ta>
            <ta e="T1468" id="Seg_1934" s="T1467">RUS:mod</ta>
            <ta e="T1470" id="Seg_1935" s="T1469">TURK:disc</ta>
            <ta e="T1471" id="Seg_1936" s="T1470">RUS:cult</ta>
            <ta e="T1476" id="Seg_1937" s="T1475">RUS:cult</ta>
            <ta e="T1490" id="Seg_1938" s="T1489">RUS:gram</ta>
            <ta e="T1491" id="Seg_1939" s="T1490">RUS:mod</ta>
            <ta e="T1513" id="Seg_1940" s="T1512">RUS:gram</ta>
            <ta e="T1518" id="Seg_1941" s="T1517">RUS:gram</ta>
            <ta e="T1530" id="Seg_1942" s="T1529">TURK:gram(INDEF)</ta>
            <ta e="T1532" id="Seg_1943" s="T1531">%TURK:core</ta>
            <ta e="T1540" id="Seg_1944" s="T1539">RUS:gram</ta>
            <ta e="T1543" id="Seg_1945" s="T1542">RUS:gram</ta>
            <ta e="T1546" id="Seg_1946" s="T1545">RUS:gram</ta>
            <ta e="T1547" id="Seg_1947" s="T1546">TURK:gram(INDEF)</ta>
            <ta e="T1554" id="Seg_1948" s="T1553">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T1553" id="Seg_1949" s="T1552">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1415" id="Seg_1950" s="T1412">Жил один человек.</ta>
            <ta e="T1419" id="Seg_1951" s="T1415">Он очень хорошо умел хвастаться.</ta>
            <ta e="T1422" id="Seg_1952" s="T1419">Потом он поссорился с людьми.</ta>
            <ta e="T1425" id="Seg_1953" s="T1422">Тогда он ушёл в тайгу.</ta>
            <ta e="T1431" id="Seg_1954" s="T1425">Он говорит: «Я нашёл медвежий след».</ta>
            <ta e="T1433" id="Seg_1955" s="T1431">Потом они пошли.</ta>
            <ta e="T1436" id="Seg_1956" s="T1433">Они шли по следам.</ta>
            <ta e="T1439" id="Seg_1957" s="T1436">Пришли к большой горе.</ta>
            <ta e="T1441" id="Seg_1958" s="T1439">В берлогу к медведю.</ta>
            <ta e="T1444" id="Seg_1959" s="T1441">Тогда он говорит:</ta>
            <ta e="T1451" id="Seg_1960" s="T1444">«Свяжите ремни и опустите меня туда».</ta>
            <ta e="T1455" id="Seg_1961" s="T1451">Они опустили его туда.</ta>
            <ta e="T1457" id="Seg_1962" s="T1455">И бросили [ремни].</ta>
            <ta e="T1462" id="Seg_1963" s="T1457">Он там сидит.</ta>
            <ta e="T1466" id="Seg_1964" s="T1462">День, три дня просидел.</ta>
            <ta e="T1468" id="Seg_1965" s="T1466">Есть хочется.</ta>
            <ta e="T1472" id="Seg_1966" s="T1468">Он тогда ремни съел.</ta>
            <ta e="T1473" id="Seg_1967" s="T1472">Заснул.</ta>
            <ta e="T1476" id="Seg_1968" s="T1473">Залез к медведю в берлогу.</ta>
            <ta e="T1479" id="Seg_1969" s="T1476">Там [было] тепло, он заснул.</ta>
            <ta e="T1483" id="Seg_1970" s="T1479">Потом медведь свою лапу дал.</ta>
            <ta e="T1487" id="Seg_1971" s="T1483">Он ел, ел.</ta>
            <ta e="T1491" id="Seg_1972" s="T1487">Потом [ему] есть [уже] не хотелось.</ta>
            <ta e="T1494" id="Seg_1973" s="T1491">Потом тепло стало.</ta>
            <ta e="T1503" id="Seg_1974" s="T1494">Медведь говорит: «Садись на меня, я отвезу тебя домой».</ta>
            <ta e="T1507" id="Seg_1975" s="T1503">Тогда он пошёл домой.</ta>
            <ta e="T1509" id="Seg_1976" s="T1507">Сел на медведя.</ta>
            <ta e="T1512" id="Seg_1977" s="T1509">Медведь отвёл его домой.</ta>
            <ta e="T1522" id="Seg_1978" s="T1512">«А принеси мне трёх собак и привяжи (ко мне?) одного (щенка?)». [?]</ta>
            <ta e="T1525" id="Seg_1979" s="T1522">Потом он пришёл.</ta>
            <ta e="T1528" id="Seg_1980" s="T1525">Люди пришли, спрашивают.</ta>
            <ta e="T1533" id="Seg_1981" s="T1528">Он ничего людям не сказал.</ta>
            <ta e="T1537" id="Seg_1982" s="T1533">Потом взял трёх собак.</ta>
            <ta e="T1539" id="Seg_1983" s="T1537">Отвёл [их] к медведю.</ta>
            <ta e="T1542" id="Seg_1984" s="T1539">И пустил [их?] туда.</ta>
            <ta e="T1545" id="Seg_1985" s="T1542">И вернулся домой.</ta>
            <ta e="T1552" id="Seg_1986" s="T1545">И никогда не ссорился с людьми.</ta>
            <ta e="T1555" id="Seg_1987" s="T1552">Дал им хорошо жить.</ta>
            <ta e="T1556" id="Seg_1988" s="T1555">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1415" id="Seg_1989" s="T1412">[There] was a man.</ta>
            <ta e="T1419" id="Seg_1990" s="T1415">He was very good in boasting.</ta>
            <ta e="T1422" id="Seg_1991" s="T1419">Then he quarrelled with people.</ta>
            <ta e="T1425" id="Seg_1992" s="T1422">Then he went to the taiga.</ta>
            <ta e="T1431" id="Seg_1993" s="T1425">He says: “I found a bear's foot (/footprint) there.”</ta>
            <ta e="T1433" id="Seg_1994" s="T1431">Then they went.</ta>
            <ta e="T1436" id="Seg_1995" s="T1433">They were following the tracks.</ta>
            <ta e="T1439" id="Seg_1996" s="T1436">They came to a big mountain.</ta>
            <ta e="T1441" id="Seg_1997" s="T1439">To the bear, in a lair.</ta>
            <ta e="T1444" id="Seg_1998" s="T1441">Then he says:</ta>
            <ta e="T1451" id="Seg_1999" s="T1444">“Tie up belts and let me down there.”</ta>
            <ta e="T1455" id="Seg_2000" s="T1451">They let him down there.</ta>
            <ta e="T1457" id="Seg_2001" s="T1455">And threw [the belts].</ta>
            <ta e="T1462" id="Seg_2002" s="T1457">He is sitting there.</ta>
            <ta e="T1466" id="Seg_2003" s="T1462">One day, three days he sat [there].</ta>
            <ta e="T1468" id="Seg_2004" s="T1466">He wants to eat.</ta>
            <ta e="T1472" id="Seg_2005" s="T1468">Then he ate belts.</ta>
            <ta e="T1473" id="Seg_2006" s="T1472">He fell asleep.</ta>
            <ta e="T1476" id="Seg_2007" s="T1473">He crept into the bear’s cave.</ta>
            <ta e="T1479" id="Seg_2008" s="T1476">It was warm there, he fell asleep.</ta>
            <ta e="T1483" id="Seg_2009" s="T1479">Then the bear gave its foot.</ta>
            <ta e="T1487" id="Seg_2010" s="T1483">He ate it, ate.</ta>
            <ta e="T1491" id="Seg_2011" s="T1487">Then he did not want to eat [anymore].</ta>
            <ta e="T1494" id="Seg_2012" s="T1491">Then it became warm.</ta>
            <ta e="T1503" id="Seg_2013" s="T1494">The bear says: "Sit on me, I will take you home."</ta>
            <ta e="T1507" id="Seg_2014" s="T1503">Then he went home.</ta>
            <ta e="T1509" id="Seg_2015" s="T1507">He sat on the bear.</ta>
            <ta e="T1512" id="Seg_2016" s="T1509">The bear took [him] home.</ta>
            <ta e="T1522" id="Seg_2017" s="T1512">“But bring three dogs here and tie one of [their?] sons to me. [?]</ta>
            <ta e="T1525" id="Seg_2018" s="T1522">Then he came.</ta>
            <ta e="T1528" id="Seg_2019" s="T1525">People came, they are asking.</ta>
            <ta e="T1533" id="Seg_2020" s="T1528">He did not tell anything to the people.</ta>
            <ta e="T1537" id="Seg_2021" s="T1533">Then he took three dogs.</ta>
            <ta e="T1539" id="Seg_2022" s="T1537">He took [them] to the bear.</ta>
            <ta e="T1542" id="Seg_2023" s="T1539">And sent [them?] there.</ta>
            <ta e="T1545" id="Seg_2024" s="T1542">And returned home.</ta>
            <ta e="T1552" id="Seg_2025" s="T1545">And he never quarrelled with people.</ta>
            <ta e="T1555" id="Seg_2026" s="T1552">He let them live well.</ta>
            <ta e="T1556" id="Seg_2027" s="T1555">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1415" id="Seg_2028" s="T1412">[Es] war ein Mann.</ta>
            <ta e="T1419" id="Seg_2029" s="T1415">Er war sehr gut im Angeben.</ta>
            <ta e="T1422" id="Seg_2030" s="T1419">Dann stritt er sich mit Leuten.</ta>
            <ta e="T1425" id="Seg_2031" s="T1422">Dann ging er auf die Taiga.</ta>
            <ta e="T1431" id="Seg_2032" s="T1425">Er sagt: „Ich fand dort einen Bärenfuß (/-abdruck).“</ta>
            <ta e="T1433" id="Seg_2033" s="T1431">Dann gingen sie.</ta>
            <ta e="T1436" id="Seg_2034" s="T1433">Sie folgten die Spuren.</ta>
            <ta e="T1439" id="Seg_2035" s="T1436">Sie kamen zu einem großen Berg.</ta>
            <ta e="T1441" id="Seg_2036" s="T1439">Zum Bären, in einer Höhle.</ta>
            <ta e="T1444" id="Seg_2037" s="T1441">Dann sagt er:</ta>
            <ta e="T1451" id="Seg_2038" s="T1444">„Verbindet die Riemen und lasst mich da runter.“</ta>
            <ta e="T1455" id="Seg_2039" s="T1451">Sie haben ihn dort hingestellt.</ta>
            <ta e="T1457" id="Seg_2040" s="T1455">Und warfen [die Riemen].</ta>
            <ta e="T1462" id="Seg_2041" s="T1457">Er sitzt dort.</ta>
            <ta e="T1466" id="Seg_2042" s="T1462">Einen Tag, drei Tage saß er [dort].</ta>
            <ta e="T1468" id="Seg_2043" s="T1466">Er wollte essen.</ta>
            <ta e="T1472" id="Seg_2044" s="T1468">Dann aß er Riemen.</ta>
            <ta e="T1473" id="Seg_2045" s="T1472">Er schlief ein.</ta>
            <ta e="T1476" id="Seg_2046" s="T1473">Er schlich sich in die Höhle des Bären.</ta>
            <ta e="T1479" id="Seg_2047" s="T1476">Dort war es warm, er schlief ein.</ta>
            <ta e="T1483" id="Seg_2048" s="T1479">Dann gab der Bär seinen Fuß.</ta>
            <ta e="T1487" id="Seg_2049" s="T1483">Er aß es, aß.</ta>
            <ta e="T1491" id="Seg_2050" s="T1487">Dann wollte er nicht [mehr] essen.</ta>
            <ta e="T1494" id="Seg_2051" s="T1491">Dann wurde es warm.</ta>
            <ta e="T1503" id="Seg_2052" s="T1494">Der Bär sagt: „Setz dich auf mich, ich nehme dich nach Hause.“</ta>
            <ta e="T1507" id="Seg_2053" s="T1503">Dann ging er nach Hause.</ta>
            <ta e="T1509" id="Seg_2054" s="T1507">Er saß auf dem Bären.</ta>
            <ta e="T1512" id="Seg_2055" s="T1509">Der Bär nahm ihn nach Hause.</ta>
            <ta e="T1522" id="Seg_2056" s="T1512">Aber bringe drei Hunde dort und binde einen [ihrer?] Söhne an mich. [?]</ta>
            <ta e="T1525" id="Seg_2057" s="T1522">Dann kam er.</ta>
            <ta e="T1528" id="Seg_2058" s="T1525">Leute kamen, sie fragen.</ta>
            <ta e="T1533" id="Seg_2059" s="T1528">Er hat den Leuten nichts erzählt.</ta>
            <ta e="T1537" id="Seg_2060" s="T1533">Dann nahm er drei Hunde</ta>
            <ta e="T1539" id="Seg_2061" s="T1537">Er nahm [sie] zum Bären.</ta>
            <ta e="T1542" id="Seg_2062" s="T1539">Und schickte [sie?] dort.</ta>
            <ta e="T1545" id="Seg_2063" s="T1542">Und ging wieder nach Hause.</ta>
            <ta e="T1552" id="Seg_2064" s="T1545">Und er stritt sich nie wieder mit Leuten.</ta>
            <ta e="T1555" id="Seg_2065" s="T1552">Er ließ sie wohl leben.</ta>
            <ta e="T1556" id="Seg_2066" s="T1555">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1466" id="Seg_2067" s="T1462">[GVY:] rather amnoluʔpi, than amnolaʔpi.</ta>
            <ta e="T1472" id="Seg_2068" s="T1468">Ru. ремень ’belt’ (??).</ta>
            <ta e="T1473" id="Seg_2069" s="T1472">[GVY:] Rather konoluʔbdə, but Donner has this stem with ko- as well (32a).</ta>
            <ta e="T1476" id="Seg_2070" s="T1473">Берлога 'bear cave'.</ta>
            <ta e="T1533" id="Seg_2071" s="T1528">tʼăbaktərzi- nʼi- baktərbi</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1534" />
            <conversion-tli id="T1535" />
            <conversion-tli id="T1536" />
            <conversion-tli id="T1537" />
            <conversion-tli id="T1538" />
            <conversion-tli id="T1539" />
            <conversion-tli id="T1540" />
            <conversion-tli id="T1541" />
            <conversion-tli id="T1542" />
            <conversion-tli id="T1543" />
            <conversion-tli id="T1544" />
            <conversion-tli id="T1545" />
            <conversion-tli id="T1546" />
            <conversion-tli id="T1547" />
            <conversion-tli id="T1548" />
            <conversion-tli id="T1549" />
            <conversion-tli id="T1550" />
            <conversion-tli id="T1551" />
            <conversion-tli id="T1552" />
            <conversion-tli id="T1553" />
            <conversion-tli id="T1554" />
            <conversion-tli id="T1555" />
            <conversion-tli id="T1556" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
