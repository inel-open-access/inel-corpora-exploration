<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF0304189-892D-8992-0133-3A3635B95B56">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_MiceBuryCat_flk.wav" />
         <referenced-file url="PKZ_196X_MiceBuryCat_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_MiceBuryCat_flk\PKZ_196X_MiceBuryCat_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">52</ud-information>
            <ud-information attribute-name="# e">52</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.014" type="appl" />
         <tli id="T1" time="1.199" type="appl" />
         <tli id="T2" time="2.383" type="appl" />
         <tli id="T3" time="3.568" type="appl" />
         <tli id="T4" time="4.753" type="appl" />
         <tli id="T5" time="5.596" type="appl" />
         <tli id="T6" time="6.439" type="appl" />
         <tli id="T7" time="7.282" type="appl" />
         <tli id="T8" time="7.774" type="appl" />
         <tli id="T9" time="8.265" type="appl" />
         <tli id="T10" time="8.757" type="appl" />
         <tli id="T11" time="9.248" type="appl" />
         <tli id="T12" time="9.74" type="appl" />
         <tli id="T13" time="10.718" type="appl" />
         <tli id="T14" time="11.552" type="appl" />
         <tli id="T15" time="12.387" type="appl" />
         <tli id="T16" time="13.083" type="appl" />
         <tli id="T17" time="13.779" type="appl" />
         <tli id="T18" time="14.628" type="appl" />
         <tli id="T19" time="15.341" type="appl" />
         <tli id="T20" time="16.053" type="appl" />
         <tli id="T21" time="17.118" type="appl" />
         <tli id="T22" time="18.17" type="appl" />
         <tli id="T23" time="19.221" type="appl" />
         <tli id="T24" time="20.272" type="appl" />
         <tli id="T25" time="21.324" type="appl" />
         <tli id="T26" time="22.375" type="appl" />
         <tli id="T27" time="23.427" type="appl" />
         <tli id="T28" time="25.17162158002736" />
         <tli id="T29" time="25.844" type="appl" />
         <tli id="T30" time="26.495" type="appl" />
         <tli id="T31" time="27.147" type="appl" />
         <tli id="T32" time="27.798" type="appl" />
         <tli id="T33" time="28.45" type="appl" />
         <tli id="T34" time="29.83130470620297" />
         <tli id="T35" time="30.598" type="appl" />
         <tli id="T36" time="31.442" type="appl" />
         <tli id="T37" time="33.00442225707507" />
         <tli id="T38" time="34.72430529935448" />
         <tli id="T39" time="35.563" type="appl" />
         <tli id="T40" time="36.439" type="appl" />
         <tli id="T41" time="37.395" type="appl" />
         <tli id="T42" time="38.352" type="appl" />
         <tli id="T43" time="39.66396938590116" />
         <tli id="T44" time="40.614" type="appl" />
         <tli id="T45" time="41.545" type="appl" />
         <tli id="T46" time="42.67043160321906" />
         <tli id="T47" time="44.81695229939723" />
         <tli id="T48" time="45.583" type="appl" />
         <tli id="T49" time="46.139" type="appl" />
         <tli id="T50" time="46.695" type="appl" />
         <tli id="T51" time="47.41010928950069" />
         <tli id="T52" time="48.73" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T52" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tumoʔim</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tospak</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">üge</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">sürerluʔpi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭzeŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">bar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">tʼorlaʔpiʔi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Dĭgəttə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">dĭ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">ibi</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">da</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">külaːmbi</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Dĭzeŋ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">bar</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">suʔmiluʔpiʔi</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">I</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">šobiʔi</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">Dĭm</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">emnə-</ts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">embiʔi</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_82" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">I</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">kun-</ts>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">davaj</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">kunzittə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">krospaʔinə</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">dĭn</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">dʼünə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">tĭldzittə</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_112" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">I</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">dĭn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">nükebə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">tospak</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">amnolbiʔi</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_130" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">Kunlaʔbəʔjə</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_136" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">Iʔgö</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">tumoʔi</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">šobiʔi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_148" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">Kumbiʔi</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_154" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_156" n="HIAT:w" s="T38">Dʼünə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_159" n="HIAT:w" s="T39">embiʔi</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_163" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_165" n="HIAT:w" s="T40">I</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_168" n="HIAT:w" s="T41">dʼüzʼiʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_171" n="HIAT:w" s="T42">kămnəbiʔi</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_175" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_177" n="HIAT:w" s="T43">I</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_180" n="HIAT:w" s="T44">parluʔbiʔi</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_183" n="HIAT:w" s="T45">maʔndə</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_187" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_189" n="HIAT:w" s="T46">Suʔmileʔbəʔjə</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_193" n="HIAT:u" s="T47">
                  <nts id="Seg_194" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_196" n="HIAT:w" s="T47">To</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_200" n="HIAT:w" s="T48">dĭ</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">külaːmbi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_206" n="HIAT:w" s="T50">tospak</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_210" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_212" n="HIAT:w" s="T51">Kabarləj</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T52" id="Seg_215" n="sc" s="T0">
               <ts e="T1" id="Seg_217" n="e" s="T0">Tumoʔim </ts>
               <ts e="T2" id="Seg_219" n="e" s="T1">tospak </ts>
               <ts e="T3" id="Seg_221" n="e" s="T2">üge </ts>
               <ts e="T4" id="Seg_223" n="e" s="T3">sürerluʔpi. </ts>
               <ts e="T5" id="Seg_225" n="e" s="T4">Dĭzeŋ </ts>
               <ts e="T6" id="Seg_227" n="e" s="T5">bar </ts>
               <ts e="T7" id="Seg_229" n="e" s="T6">tʼorlaʔpiʔi. </ts>
               <ts e="T8" id="Seg_231" n="e" s="T7">Dĭgəttə </ts>
               <ts e="T9" id="Seg_233" n="e" s="T8">dĭ </ts>
               <ts e="T10" id="Seg_235" n="e" s="T9">ibi </ts>
               <ts e="T11" id="Seg_237" n="e" s="T10">da </ts>
               <ts e="T12" id="Seg_239" n="e" s="T11">külaːmbi. </ts>
               <ts e="T13" id="Seg_241" n="e" s="T12">Dĭzeŋ </ts>
               <ts e="T14" id="Seg_243" n="e" s="T13">bar </ts>
               <ts e="T15" id="Seg_245" n="e" s="T14">suʔmiluʔpiʔi. </ts>
               <ts e="T16" id="Seg_247" n="e" s="T15">I </ts>
               <ts e="T17" id="Seg_249" n="e" s="T16">šobiʔi. </ts>
               <ts e="T18" id="Seg_251" n="e" s="T17">Dĭm </ts>
               <ts e="T19" id="Seg_253" n="e" s="T18">(emnə-) </ts>
               <ts e="T20" id="Seg_255" n="e" s="T19">embiʔi. </ts>
               <ts e="T21" id="Seg_257" n="e" s="T20">I </ts>
               <ts e="T22" id="Seg_259" n="e" s="T21">(kun-) </ts>
               <ts e="T23" id="Seg_261" n="e" s="T22">davaj </ts>
               <ts e="T24" id="Seg_263" n="e" s="T23">kunzittə </ts>
               <ts e="T25" id="Seg_265" n="e" s="T24">krospaʔinə, </ts>
               <ts e="T26" id="Seg_267" n="e" s="T25">dĭn </ts>
               <ts e="T27" id="Seg_269" n="e" s="T26">dʼünə </ts>
               <ts e="T28" id="Seg_271" n="e" s="T27">tĭldzittə. </ts>
               <ts e="T29" id="Seg_273" n="e" s="T28">I </ts>
               <ts e="T30" id="Seg_275" n="e" s="T29">dĭn </ts>
               <ts e="T31" id="Seg_277" n="e" s="T30">nükebə </ts>
               <ts e="T32" id="Seg_279" n="e" s="T31">tospak </ts>
               <ts e="T33" id="Seg_281" n="e" s="T32">amnolbiʔi. </ts>
               <ts e="T34" id="Seg_283" n="e" s="T33">Kunlaʔbəʔjə. </ts>
               <ts e="T35" id="Seg_285" n="e" s="T34">Iʔgö </ts>
               <ts e="T36" id="Seg_287" n="e" s="T35">tumoʔi </ts>
               <ts e="T37" id="Seg_289" n="e" s="T36">šobiʔi. </ts>
               <ts e="T38" id="Seg_291" n="e" s="T37">Kumbiʔi. </ts>
               <ts e="T39" id="Seg_293" n="e" s="T38">Dʼünə </ts>
               <ts e="T40" id="Seg_295" n="e" s="T39">embiʔi. </ts>
               <ts e="T41" id="Seg_297" n="e" s="T40">I </ts>
               <ts e="T42" id="Seg_299" n="e" s="T41">dʼüzʼiʔ </ts>
               <ts e="T43" id="Seg_301" n="e" s="T42">kămnəbiʔi. </ts>
               <ts e="T44" id="Seg_303" n="e" s="T43">I </ts>
               <ts e="T45" id="Seg_305" n="e" s="T44">parluʔbiʔi </ts>
               <ts e="T46" id="Seg_307" n="e" s="T45">maʔndə. </ts>
               <ts e="T47" id="Seg_309" n="e" s="T46">Suʔmileʔbəʔjə. </ts>
               <ts e="T48" id="Seg_311" n="e" s="T47">(To) </ts>
               <ts e="T49" id="Seg_313" n="e" s="T48">dĭ </ts>
               <ts e="T50" id="Seg_315" n="e" s="T49">külaːmbi </ts>
               <ts e="T51" id="Seg_317" n="e" s="T50">tospak. </ts>
               <ts e="T52" id="Seg_319" n="e" s="T51">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_320" s="T0">PKZ_196X_MiceBuryCat_flk.001 (001)</ta>
            <ta e="T7" id="Seg_321" s="T4">PKZ_196X_MiceBuryCat_flk.002 (002)</ta>
            <ta e="T12" id="Seg_322" s="T7">PKZ_196X_MiceBuryCat_flk.003 (003)</ta>
            <ta e="T15" id="Seg_323" s="T12">PKZ_196X_MiceBuryCat_flk.004 (004)</ta>
            <ta e="T17" id="Seg_324" s="T15">PKZ_196X_MiceBuryCat_flk.005 (005)</ta>
            <ta e="T20" id="Seg_325" s="T17">PKZ_196X_MiceBuryCat_flk.006 (006)</ta>
            <ta e="T28" id="Seg_326" s="T20">PKZ_196X_MiceBuryCat_flk.007 (007)</ta>
            <ta e="T33" id="Seg_327" s="T28">PKZ_196X_MiceBuryCat_flk.008 (008)</ta>
            <ta e="T34" id="Seg_328" s="T33">PKZ_196X_MiceBuryCat_flk.009 (009)</ta>
            <ta e="T37" id="Seg_329" s="T34">PKZ_196X_MiceBuryCat_flk.010 (010)</ta>
            <ta e="T38" id="Seg_330" s="T37">PKZ_196X_MiceBuryCat_flk.011 (011)</ta>
            <ta e="T40" id="Seg_331" s="T38">PKZ_196X_MiceBuryCat_flk.012 (012)</ta>
            <ta e="T43" id="Seg_332" s="T40">PKZ_196X_MiceBuryCat_flk.013 (013)</ta>
            <ta e="T46" id="Seg_333" s="T43">PKZ_196X_MiceBuryCat_flk.014 (014)</ta>
            <ta e="T47" id="Seg_334" s="T46">PKZ_196X_MiceBuryCat_flk.015 (015)</ta>
            <ta e="T51" id="Seg_335" s="T47">PKZ_196X_MiceBuryCat_flk.016 (016)</ta>
            <ta e="T52" id="Seg_336" s="T51">PKZ_196X_MiceBuryCat_flk.017 (017)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_337" s="T0">Tumoʔim tospak üge sürerluʔpi. </ta>
            <ta e="T7" id="Seg_338" s="T4">Dĭzeŋ bar tʼorlaʔpiʔi. </ta>
            <ta e="T12" id="Seg_339" s="T7">Dĭgəttə dĭ ibi da külaːmbi. </ta>
            <ta e="T15" id="Seg_340" s="T12">Dĭzeŋ bar suʔmiluʔpiʔi. </ta>
            <ta e="T17" id="Seg_341" s="T15">I šobiʔi. </ta>
            <ta e="T20" id="Seg_342" s="T17">Dĭm (emnə-) embiʔi. </ta>
            <ta e="T28" id="Seg_343" s="T20">I (kun-) davaj kunzittə krospaʔinə, dĭn dʼünə tĭldzittə. </ta>
            <ta e="T33" id="Seg_344" s="T28">I dĭn nükebə tospak amnolbiʔi. </ta>
            <ta e="T34" id="Seg_345" s="T33">Kunlaʔbəʔjə. </ta>
            <ta e="T37" id="Seg_346" s="T34">Iʔgö tumoʔi šobiʔi. </ta>
            <ta e="T38" id="Seg_347" s="T37">Kumbiʔi. </ta>
            <ta e="T40" id="Seg_348" s="T38">Dʼünə embiʔi. </ta>
            <ta e="T43" id="Seg_349" s="T40">I dʼüzʼiʔ kămnəbiʔi. </ta>
            <ta e="T46" id="Seg_350" s="T43">I parluʔbiʔi maʔndə. </ta>
            <ta e="T47" id="Seg_351" s="T46">Suʔmileʔbəʔjə. </ta>
            <ta e="T51" id="Seg_352" s="T47">(To) dĭ külaːmbi tospak. </ta>
            <ta e="T52" id="Seg_353" s="T51">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_354" s="T0">tumo-ʔi-m</ta>
            <ta e="T2" id="Seg_355" s="T1">tospak</ta>
            <ta e="T3" id="Seg_356" s="T2">üge</ta>
            <ta e="T4" id="Seg_357" s="T3">sürer-luʔ-pi</ta>
            <ta e="T5" id="Seg_358" s="T4">dĭ-zeŋ</ta>
            <ta e="T6" id="Seg_359" s="T5">bar</ta>
            <ta e="T7" id="Seg_360" s="T6">tʼor-laʔpi-ʔi</ta>
            <ta e="T8" id="Seg_361" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_362" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_363" s="T9">i-bi</ta>
            <ta e="T11" id="Seg_364" s="T10">da</ta>
            <ta e="T12" id="Seg_365" s="T11">kü-laːm-bi</ta>
            <ta e="T13" id="Seg_366" s="T12">dĭ-zeŋ</ta>
            <ta e="T14" id="Seg_367" s="T13">bar</ta>
            <ta e="T15" id="Seg_368" s="T14">suʔmi-luʔ-pi-ʔi</ta>
            <ta e="T16" id="Seg_369" s="T15">i</ta>
            <ta e="T17" id="Seg_370" s="T16">šo-bi-ʔi</ta>
            <ta e="T18" id="Seg_371" s="T17">dĭ-m</ta>
            <ta e="T20" id="Seg_372" s="T19">em-bi-ʔi</ta>
            <ta e="T21" id="Seg_373" s="T20">i</ta>
            <ta e="T23" id="Seg_374" s="T22">davaj</ta>
            <ta e="T24" id="Seg_375" s="T23">kun-zittə</ta>
            <ta e="T25" id="Seg_376" s="T24">krospa-ʔi-nə</ta>
            <ta e="T26" id="Seg_377" s="T25">dĭn</ta>
            <ta e="T27" id="Seg_378" s="T26">dʼü-nə</ta>
            <ta e="T28" id="Seg_379" s="T27">tĭld-zittə</ta>
            <ta e="T29" id="Seg_380" s="T28">i</ta>
            <ta e="T30" id="Seg_381" s="T29">dĭn</ta>
            <ta e="T31" id="Seg_382" s="T30">nüke-bə</ta>
            <ta e="T32" id="Seg_383" s="T31">tospak</ta>
            <ta e="T33" id="Seg_384" s="T32">amnol-bi-ʔi</ta>
            <ta e="T34" id="Seg_385" s="T33">kun-laʔbə-ʔjə</ta>
            <ta e="T35" id="Seg_386" s="T34">iʔgö</ta>
            <ta e="T36" id="Seg_387" s="T35">tumo-ʔi</ta>
            <ta e="T37" id="Seg_388" s="T36">šo-bi-ʔi</ta>
            <ta e="T38" id="Seg_389" s="T37">kum-bi-ʔi</ta>
            <ta e="T39" id="Seg_390" s="T38">dʼü-nə</ta>
            <ta e="T40" id="Seg_391" s="T39">em-bi-ʔi</ta>
            <ta e="T41" id="Seg_392" s="T40">i</ta>
            <ta e="T42" id="Seg_393" s="T41">dʼü-zʼiʔ</ta>
            <ta e="T43" id="Seg_394" s="T42">kămnə-bi-ʔi</ta>
            <ta e="T44" id="Seg_395" s="T43">i</ta>
            <ta e="T45" id="Seg_396" s="T44">par-luʔ-bi-ʔi</ta>
            <ta e="T46" id="Seg_397" s="T45">maʔ-ndə</ta>
            <ta e="T47" id="Seg_398" s="T46">suʔmi-leʔbə-ʔjə</ta>
            <ta e="T48" id="Seg_399" s="T47">to</ta>
            <ta e="T49" id="Seg_400" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_401" s="T49">kü-laːm-bi</ta>
            <ta e="T51" id="Seg_402" s="T50">tospak</ta>
            <ta e="T52" id="Seg_403" s="T51">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_404" s="T0">tumo-jəʔ-m</ta>
            <ta e="T2" id="Seg_405" s="T1">tospak</ta>
            <ta e="T3" id="Seg_406" s="T2">üge</ta>
            <ta e="T4" id="Seg_407" s="T3">sürer-luʔbdə-bi</ta>
            <ta e="T5" id="Seg_408" s="T4">dĭ-zAŋ</ta>
            <ta e="T6" id="Seg_409" s="T5">bar</ta>
            <ta e="T7" id="Seg_410" s="T6">tʼor-laʔpi-jəʔ</ta>
            <ta e="T8" id="Seg_411" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_412" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_413" s="T9">i-bi</ta>
            <ta e="T11" id="Seg_414" s="T10">da</ta>
            <ta e="T12" id="Seg_415" s="T11">kü-laːm-bi</ta>
            <ta e="T13" id="Seg_416" s="T12">dĭ-zAŋ</ta>
            <ta e="T14" id="Seg_417" s="T13">bar</ta>
            <ta e="T15" id="Seg_418" s="T14">süʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T16" id="Seg_419" s="T15">i</ta>
            <ta e="T17" id="Seg_420" s="T16">šo-bi-jəʔ</ta>
            <ta e="T18" id="Seg_421" s="T17">dĭ-m</ta>
            <ta e="T20" id="Seg_422" s="T19">hen-bi-jəʔ</ta>
            <ta e="T21" id="Seg_423" s="T20">i</ta>
            <ta e="T23" id="Seg_424" s="T22">davaj</ta>
            <ta e="T24" id="Seg_425" s="T23">kun-zittə</ta>
            <ta e="T25" id="Seg_426" s="T24">krospa-jəʔ-Tə</ta>
            <ta e="T26" id="Seg_427" s="T25">dĭn</ta>
            <ta e="T27" id="Seg_428" s="T26">tʼo-Tə</ta>
            <ta e="T28" id="Seg_429" s="T27">tĭldə-zittə</ta>
            <ta e="T29" id="Seg_430" s="T28">i</ta>
            <ta e="T30" id="Seg_431" s="T29">dĭn</ta>
            <ta e="T31" id="Seg_432" s="T30">nüke-bə</ta>
            <ta e="T32" id="Seg_433" s="T31">tospak</ta>
            <ta e="T33" id="Seg_434" s="T32">amnol-bi-jəʔ</ta>
            <ta e="T34" id="Seg_435" s="T33">kun-laʔbə-jəʔ</ta>
            <ta e="T35" id="Seg_436" s="T34">iʔgö</ta>
            <ta e="T36" id="Seg_437" s="T35">tumo-jəʔ</ta>
            <ta e="T37" id="Seg_438" s="T36">šo-bi-jəʔ</ta>
            <ta e="T38" id="Seg_439" s="T37">kun-bi-jəʔ</ta>
            <ta e="T39" id="Seg_440" s="T38">tʼo-Tə</ta>
            <ta e="T40" id="Seg_441" s="T39">hen-bi-jəʔ</ta>
            <ta e="T41" id="Seg_442" s="T40">i</ta>
            <ta e="T42" id="Seg_443" s="T41">tʼo-ziʔ</ta>
            <ta e="T43" id="Seg_444" s="T42">kămnə-bi-jəʔ</ta>
            <ta e="T44" id="Seg_445" s="T43">i</ta>
            <ta e="T45" id="Seg_446" s="T44">par-luʔbdə-bi-jəʔ</ta>
            <ta e="T46" id="Seg_447" s="T45">maʔ-gəndə</ta>
            <ta e="T47" id="Seg_448" s="T46">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T49" id="Seg_449" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_450" s="T49">kü-laːm-bi</ta>
            <ta e="T51" id="Seg_451" s="T50">tospak</ta>
            <ta e="T52" id="Seg_452" s="T51">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_453" s="T0">mouse-3PL-ACC</ta>
            <ta e="T2" id="Seg_454" s="T1">cat.[NOM.SG]</ta>
            <ta e="T3" id="Seg_455" s="T2">always</ta>
            <ta e="T4" id="Seg_456" s="T3">drive-MOM-PST.[3SG]</ta>
            <ta e="T5" id="Seg_457" s="T4">this-PL</ta>
            <ta e="T6" id="Seg_458" s="T5">all</ta>
            <ta e="T7" id="Seg_459" s="T6">cry-DUR.PST-3PL</ta>
            <ta e="T8" id="Seg_460" s="T7">then</ta>
            <ta e="T9" id="Seg_461" s="T8">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_462" s="T9">take-PST.[3SG]</ta>
            <ta e="T11" id="Seg_463" s="T10">and</ta>
            <ta e="T12" id="Seg_464" s="T11">die-RES-PST.[3SG]</ta>
            <ta e="T13" id="Seg_465" s="T12">this-PL</ta>
            <ta e="T14" id="Seg_466" s="T13">all</ta>
            <ta e="T15" id="Seg_467" s="T14">jump-MOM-PST-3PL</ta>
            <ta e="T16" id="Seg_468" s="T15">and</ta>
            <ta e="T17" id="Seg_469" s="T16">come-PST-3PL</ta>
            <ta e="T18" id="Seg_470" s="T17">this-ACC</ta>
            <ta e="T20" id="Seg_471" s="T19">put-PST-3PL</ta>
            <ta e="T21" id="Seg_472" s="T20">and</ta>
            <ta e="T23" id="Seg_473" s="T22">INCH</ta>
            <ta e="T24" id="Seg_474" s="T23">bring-INF.LAT</ta>
            <ta e="T25" id="Seg_475" s="T24">cross-PL-LAT</ta>
            <ta e="T26" id="Seg_476" s="T25">there</ta>
            <ta e="T27" id="Seg_477" s="T26">place-LAT</ta>
            <ta e="T28" id="Seg_478" s="T27">bury-INF.LAT</ta>
            <ta e="T29" id="Seg_479" s="T28">and</ta>
            <ta e="T30" id="Seg_480" s="T29">there</ta>
            <ta e="T31" id="Seg_481" s="T30">woman-ACC.3SG</ta>
            <ta e="T32" id="Seg_482" s="T31">cat.[NOM.SG]</ta>
            <ta e="T33" id="Seg_483" s="T32">seat-PST-3PL</ta>
            <ta e="T34" id="Seg_484" s="T33">bring-DUR-3PL</ta>
            <ta e="T35" id="Seg_485" s="T34">many</ta>
            <ta e="T36" id="Seg_486" s="T35">mouse-PL</ta>
            <ta e="T37" id="Seg_487" s="T36">come-PST-3PL</ta>
            <ta e="T38" id="Seg_488" s="T37">bring-PST-3PL</ta>
            <ta e="T39" id="Seg_489" s="T38">place-LAT</ta>
            <ta e="T40" id="Seg_490" s="T39">put-PST-3PL</ta>
            <ta e="T41" id="Seg_491" s="T40">and</ta>
            <ta e="T42" id="Seg_492" s="T41">earth-INS</ta>
            <ta e="T43" id="Seg_493" s="T42">pour-PST-3PL</ta>
            <ta e="T44" id="Seg_494" s="T43">and</ta>
            <ta e="T45" id="Seg_495" s="T44">return-MOM-PST-3PL</ta>
            <ta e="T46" id="Seg_496" s="T45">tent-LAT/LOC.3SG</ta>
            <ta e="T47" id="Seg_497" s="T46">jump-DUR-3PL</ta>
            <ta e="T49" id="Seg_498" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_499" s="T49">die-RES-PST.[3SG]</ta>
            <ta e="T51" id="Seg_500" s="T50">cat.[NOM.SG]</ta>
            <ta e="T52" id="Seg_501" s="T51">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_502" s="T0">мышь-3PL-ACC</ta>
            <ta e="T2" id="Seg_503" s="T1">кот.[NOM.SG]</ta>
            <ta e="T3" id="Seg_504" s="T2">всегда</ta>
            <ta e="T4" id="Seg_505" s="T3">гнать-MOM-PST.[3SG]</ta>
            <ta e="T5" id="Seg_506" s="T4">этот-PL</ta>
            <ta e="T6" id="Seg_507" s="T5">весь</ta>
            <ta e="T7" id="Seg_508" s="T6">плакать-DUR.PST-3PL</ta>
            <ta e="T8" id="Seg_509" s="T7">тогда</ta>
            <ta e="T9" id="Seg_510" s="T8">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_511" s="T9">взять-PST.[3SG]</ta>
            <ta e="T11" id="Seg_512" s="T10">и</ta>
            <ta e="T12" id="Seg_513" s="T11">умереть-RES-PST.[3SG]</ta>
            <ta e="T13" id="Seg_514" s="T12">этот-PL</ta>
            <ta e="T14" id="Seg_515" s="T13">весь</ta>
            <ta e="T15" id="Seg_516" s="T14">прыгнуть-MOM-PST-3PL</ta>
            <ta e="T16" id="Seg_517" s="T15">и</ta>
            <ta e="T17" id="Seg_518" s="T16">прийти-PST-3PL</ta>
            <ta e="T18" id="Seg_519" s="T17">этот-ACC</ta>
            <ta e="T20" id="Seg_520" s="T19">класть-PST-3PL</ta>
            <ta e="T21" id="Seg_521" s="T20">и</ta>
            <ta e="T23" id="Seg_522" s="T22">INCH</ta>
            <ta e="T24" id="Seg_523" s="T23">нести-INF.LAT</ta>
            <ta e="T25" id="Seg_524" s="T24">крест-PL-LAT</ta>
            <ta e="T26" id="Seg_525" s="T25">там</ta>
            <ta e="T27" id="Seg_526" s="T26">место-LAT</ta>
            <ta e="T28" id="Seg_527" s="T27">хоронить-INF.LAT</ta>
            <ta e="T29" id="Seg_528" s="T28">и</ta>
            <ta e="T30" id="Seg_529" s="T29">там</ta>
            <ta e="T31" id="Seg_530" s="T30">женщина-ACC.3SG</ta>
            <ta e="T32" id="Seg_531" s="T31">кот.[NOM.SG]</ta>
            <ta e="T33" id="Seg_532" s="T32">сажать-PST-3PL</ta>
            <ta e="T34" id="Seg_533" s="T33">нести-DUR-3PL</ta>
            <ta e="T35" id="Seg_534" s="T34">много</ta>
            <ta e="T36" id="Seg_535" s="T35">мышь-PL</ta>
            <ta e="T37" id="Seg_536" s="T36">прийти-PST-3PL</ta>
            <ta e="T38" id="Seg_537" s="T37">нести-PST-3PL</ta>
            <ta e="T39" id="Seg_538" s="T38">место-LAT</ta>
            <ta e="T40" id="Seg_539" s="T39">класть-PST-3PL</ta>
            <ta e="T41" id="Seg_540" s="T40">и</ta>
            <ta e="T42" id="Seg_541" s="T41">земля-INS</ta>
            <ta e="T43" id="Seg_542" s="T42">лить-PST-3PL</ta>
            <ta e="T44" id="Seg_543" s="T43">и</ta>
            <ta e="T45" id="Seg_544" s="T44">вернуться-MOM-PST-3PL</ta>
            <ta e="T46" id="Seg_545" s="T45">чум-LAT/LOC.3SG</ta>
            <ta e="T47" id="Seg_546" s="T46">прыгнуть-DUR-3PL</ta>
            <ta e="T49" id="Seg_547" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_548" s="T49">умереть-RES-PST.[3SG]</ta>
            <ta e="T51" id="Seg_549" s="T50">кот.[NOM.SG]</ta>
            <ta e="T52" id="Seg_550" s="T51">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_551" s="T0">n-n:case.poss-n:case</ta>
            <ta e="T2" id="Seg_552" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_553" s="T2">adv</ta>
            <ta e="T4" id="Seg_554" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_555" s="T4">dempro-n:num</ta>
            <ta e="T6" id="Seg_556" s="T5">quant</ta>
            <ta e="T7" id="Seg_557" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_558" s="T7">adv</ta>
            <ta e="T9" id="Seg_559" s="T8">dempro-n:case</ta>
            <ta e="T10" id="Seg_560" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_561" s="T10">conj</ta>
            <ta e="T12" id="Seg_562" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_563" s="T12">dempro-n:num</ta>
            <ta e="T14" id="Seg_564" s="T13">quant</ta>
            <ta e="T15" id="Seg_565" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_566" s="T15">conj</ta>
            <ta e="T17" id="Seg_567" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_568" s="T17">dempro-n:case</ta>
            <ta e="T20" id="Seg_569" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_570" s="T20">conj</ta>
            <ta e="T23" id="Seg_571" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_572" s="T23">v-v:n.fin</ta>
            <ta e="T25" id="Seg_573" s="T24">n-n:num-n:case</ta>
            <ta e="T26" id="Seg_574" s="T25">adv</ta>
            <ta e="T27" id="Seg_575" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_576" s="T27">v-v:n.fin</ta>
            <ta e="T29" id="Seg_577" s="T28">conj</ta>
            <ta e="T30" id="Seg_578" s="T29">adv</ta>
            <ta e="T31" id="Seg_579" s="T30">n-n:case.poss</ta>
            <ta e="T32" id="Seg_580" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_581" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_582" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_583" s="T34">quant</ta>
            <ta e="T36" id="Seg_584" s="T35">n-n:num</ta>
            <ta e="T37" id="Seg_585" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_586" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_587" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_588" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_589" s="T40">conj</ta>
            <ta e="T42" id="Seg_590" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_591" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_592" s="T43">conj</ta>
            <ta e="T45" id="Seg_593" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_594" s="T45">n-n:case.poss</ta>
            <ta e="T47" id="Seg_595" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T49" id="Seg_596" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_597" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_598" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_599" s="T51">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_600" s="T0">n</ta>
            <ta e="T2" id="Seg_601" s="T1">n</ta>
            <ta e="T3" id="Seg_602" s="T2">adv</ta>
            <ta e="T4" id="Seg_603" s="T3">v</ta>
            <ta e="T5" id="Seg_604" s="T4">dempro</ta>
            <ta e="T6" id="Seg_605" s="T5">quant</ta>
            <ta e="T7" id="Seg_606" s="T6">v</ta>
            <ta e="T8" id="Seg_607" s="T7">adv</ta>
            <ta e="T9" id="Seg_608" s="T8">dempro</ta>
            <ta e="T10" id="Seg_609" s="T9">v</ta>
            <ta e="T11" id="Seg_610" s="T10">conj</ta>
            <ta e="T12" id="Seg_611" s="T11">v</ta>
            <ta e="T13" id="Seg_612" s="T12">dempro</ta>
            <ta e="T14" id="Seg_613" s="T13">quant</ta>
            <ta e="T15" id="Seg_614" s="T14">v</ta>
            <ta e="T16" id="Seg_615" s="T15">conj</ta>
            <ta e="T17" id="Seg_616" s="T16">v</ta>
            <ta e="T18" id="Seg_617" s="T17">dempro</ta>
            <ta e="T20" id="Seg_618" s="T19">v</ta>
            <ta e="T21" id="Seg_619" s="T20">conj</ta>
            <ta e="T23" id="Seg_620" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_621" s="T23">v</ta>
            <ta e="T25" id="Seg_622" s="T24">n</ta>
            <ta e="T26" id="Seg_623" s="T25">adv</ta>
            <ta e="T27" id="Seg_624" s="T26">n</ta>
            <ta e="T28" id="Seg_625" s="T27">v</ta>
            <ta e="T29" id="Seg_626" s="T28">conj</ta>
            <ta e="T30" id="Seg_627" s="T29">adv</ta>
            <ta e="T31" id="Seg_628" s="T30">n</ta>
            <ta e="T32" id="Seg_629" s="T31">n</ta>
            <ta e="T33" id="Seg_630" s="T32">v</ta>
            <ta e="T34" id="Seg_631" s="T33">v</ta>
            <ta e="T35" id="Seg_632" s="T34">quant</ta>
            <ta e="T36" id="Seg_633" s="T35">n</ta>
            <ta e="T37" id="Seg_634" s="T36">v</ta>
            <ta e="T38" id="Seg_635" s="T37">v</ta>
            <ta e="T39" id="Seg_636" s="T38">n</ta>
            <ta e="T40" id="Seg_637" s="T39">v</ta>
            <ta e="T41" id="Seg_638" s="T40">conj</ta>
            <ta e="T42" id="Seg_639" s="T41">n</ta>
            <ta e="T43" id="Seg_640" s="T42">v</ta>
            <ta e="T44" id="Seg_641" s="T43">conj</ta>
            <ta e="T45" id="Seg_642" s="T44">v</ta>
            <ta e="T46" id="Seg_643" s="T45">n</ta>
            <ta e="T47" id="Seg_644" s="T46">v</ta>
            <ta e="T48" id="Seg_645" s="T47">v</ta>
            <ta e="T49" id="Seg_646" s="T48">dempro</ta>
            <ta e="T50" id="Seg_647" s="T49">v</ta>
            <ta e="T51" id="Seg_648" s="T50">n</ta>
            <ta e="T52" id="Seg_649" s="T51">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_650" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_651" s="T1">np:A</ta>
            <ta e="T5" id="Seg_652" s="T4">pro:A</ta>
            <ta e="T8" id="Seg_653" s="T7">adv:Time</ta>
            <ta e="T9" id="Seg_654" s="T8">pro:Th</ta>
            <ta e="T12" id="Seg_655" s="T11">0.3:P</ta>
            <ta e="T13" id="Seg_656" s="T12">pro:A</ta>
            <ta e="T17" id="Seg_657" s="T16">0.3:A</ta>
            <ta e="T18" id="Seg_658" s="T17">pro:Th</ta>
            <ta e="T20" id="Seg_659" s="T19">0.3:A</ta>
            <ta e="T25" id="Seg_660" s="T24">np:G</ta>
            <ta e="T26" id="Seg_661" s="T25">adv:L</ta>
            <ta e="T27" id="Seg_662" s="T26">np:L</ta>
            <ta e="T30" id="Seg_663" s="T29">adv:L</ta>
            <ta e="T31" id="Seg_664" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_665" s="T31">np:Poss</ta>
            <ta e="T33" id="Seg_666" s="T32">0.3:A</ta>
            <ta e="T34" id="Seg_667" s="T33">0.3:A</ta>
            <ta e="T36" id="Seg_668" s="T35">np:A</ta>
            <ta e="T38" id="Seg_669" s="T37">0.3:A</ta>
            <ta e="T39" id="Seg_670" s="T38">np:G</ta>
            <ta e="T40" id="Seg_671" s="T39">0.3:A</ta>
            <ta e="T42" id="Seg_672" s="T41">np:Ins</ta>
            <ta e="T43" id="Seg_673" s="T42">0.3:A</ta>
            <ta e="T45" id="Seg_674" s="T44">0.3:A</ta>
            <ta e="T46" id="Seg_675" s="T45">np:G</ta>
            <ta e="T47" id="Seg_676" s="T46">0.3:A</ta>
            <ta e="T49" id="Seg_677" s="T48">pro:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_678" s="T0">np:O</ta>
            <ta e="T2" id="Seg_679" s="T1">np:S</ta>
            <ta e="T4" id="Seg_680" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_681" s="T4">pro:S</ta>
            <ta e="T7" id="Seg_682" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_683" s="T8">pro:S</ta>
            <ta e="T10" id="Seg_684" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_685" s="T11">v:pred 0.3:S</ta>
            <ta e="T13" id="Seg_686" s="T12">pro:S</ta>
            <ta e="T15" id="Seg_687" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_688" s="T16">v:pred 0.3:S</ta>
            <ta e="T18" id="Seg_689" s="T17">pro:O</ta>
            <ta e="T20" id="Seg_690" s="T19">v:pred 0.3:S</ta>
            <ta e="T23" id="Seg_691" s="T22">ptcl:pred</ta>
            <ta e="T31" id="Seg_692" s="T30">np:O</ta>
            <ta e="T33" id="Seg_693" s="T32">v:pred 0.3:S</ta>
            <ta e="T34" id="Seg_694" s="T33">v:pred 0.3:S</ta>
            <ta e="T36" id="Seg_695" s="T35">np:S</ta>
            <ta e="T37" id="Seg_696" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_697" s="T37">v:pred 0.3:S</ta>
            <ta e="T40" id="Seg_698" s="T39">v:pred 0.3:S</ta>
            <ta e="T42" id="Seg_699" s="T41">np:O</ta>
            <ta e="T43" id="Seg_700" s="T42">v:pred 0.3:S</ta>
            <ta e="T45" id="Seg_701" s="T44">v:pred 0.3:S</ta>
            <ta e="T47" id="Seg_702" s="T46">v:pred 0.3:S</ta>
            <ta e="T49" id="Seg_703" s="T48">pro:S</ta>
            <ta e="T50" id="Seg_704" s="T49">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_705" s="T5">TURK:disc</ta>
            <ta e="T11" id="Seg_706" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_707" s="T13">TURK:disc</ta>
            <ta e="T16" id="Seg_708" s="T15">RUS:gram</ta>
            <ta e="T21" id="Seg_709" s="T20">RUS:gram</ta>
            <ta e="T23" id="Seg_710" s="T22">RUS:gram</ta>
            <ta e="T29" id="Seg_711" s="T28">RUS:gram</ta>
            <ta e="T41" id="Seg_712" s="T40">RUS:gram</ta>
            <ta e="T44" id="Seg_713" s="T43">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T12" id="Seg_714" s="T9">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_715" s="T0">Кот всё время за мышами гонялся.</ta>
            <ta e="T7" id="Seg_716" s="T4">Они плакали.</ta>
            <ta e="T12" id="Seg_717" s="T7">Потом он взял да и умер.</ta>
            <ta e="T15" id="Seg_718" s="T12">Они все заплясали.</ta>
            <ta e="T17" id="Seg_719" s="T15">И пришли.</ta>
            <ta e="T20" id="Seg_720" s="T17">Положили его [в гроб?].</ta>
            <ta e="T28" id="Seg_721" s="T20">И понесли его на кладбище, чтобы там похоронить.</ta>
            <ta e="T33" id="Seg_722" s="T28">И там посадили его жену кошку. [?]</ta>
            <ta e="T34" id="Seg_723" s="T33">Несут его.</ta>
            <ta e="T37" id="Seg_724" s="T34">Много мышей пришло.</ta>
            <ta e="T38" id="Seg_725" s="T37">Принесли.</ta>
            <ta e="T40" id="Seg_726" s="T38">Положили в землю.</ta>
            <ta e="T43" id="Seg_727" s="T40">И землёй засыпали.</ta>
            <ta e="T46" id="Seg_728" s="T43">И домой вернулись.</ta>
            <ta e="T47" id="Seg_729" s="T46">Пляшут.</ta>
            <ta e="T51" id="Seg_730" s="T47">[Потому что] кот умер.</ta>
            <ta e="T52" id="Seg_731" s="T51">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_732" s="T0">The cat was always chasing mice.</ta>
            <ta e="T7" id="Seg_733" s="T4">They peeped.</ta>
            <ta e="T12" id="Seg_734" s="T7">Then it suddenly died.</ta>
            <ta e="T15" id="Seg_735" s="T12">They started to dance.</ta>
            <ta e="T17" id="Seg_736" s="T15">And they came.</ta>
            <ta e="T20" id="Seg_737" s="T17">They put it [in a coffin?].</ta>
            <ta e="T28" id="Seg_738" s="T20">Started to carry it to the crosses [=cemetery], to bury it.</ta>
            <ta e="T33" id="Seg_739" s="T28">And they seated its wife the cat. [?]</ta>
            <ta e="T34" id="Seg_740" s="T33">They are carrying it.</ta>
            <ta e="T37" id="Seg_741" s="T34">Many mice came.</ta>
            <ta e="T38" id="Seg_742" s="T37">They carried [it].</ta>
            <ta e="T40" id="Seg_743" s="T38">They put [it] into the ground.</ta>
            <ta e="T43" id="Seg_744" s="T40">And poured [it] with ground.</ta>
            <ta e="T46" id="Seg_745" s="T43">And returned home.</ta>
            <ta e="T47" id="Seg_746" s="T46">They are dancing.</ta>
            <ta e="T51" id="Seg_747" s="T47">[Because] the cat died.</ta>
            <ta e="T52" id="Seg_748" s="T51">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_749" s="T0">Die Katze jagte immer Mäuse.</ta>
            <ta e="T7" id="Seg_750" s="T4">Sie piepsen.</ta>
            <ta e="T12" id="Seg_751" s="T7">Dann starb sie auf einmal.</ta>
            <ta e="T15" id="Seg_752" s="T12">Sie fingen an zu tanzen.</ta>
            <ta e="T17" id="Seg_753" s="T15">Und sie kamen.</ta>
            <ta e="T20" id="Seg_754" s="T17">Sie legten sie [in einen Sarg?].</ta>
            <ta e="T28" id="Seg_755" s="T20">Sie trugen sie zu den Kräuzen [=Friedhof], um sie zu begraben.</ta>
            <ta e="T33" id="Seg_756" s="T28">Und sie setzten seine Frau mit der Katze. [?]</ta>
            <ta e="T34" id="Seg_757" s="T33">Sie trugen sie.</ta>
            <ta e="T37" id="Seg_758" s="T34">Viele Mäuse kamen.</ta>
            <ta e="T38" id="Seg_759" s="T37">Sie trugen [sie].</ta>
            <ta e="T40" id="Seg_760" s="T38">Sie legten [sie] ins Grab.</ta>
            <ta e="T43" id="Seg_761" s="T40">Und bedeckten [sie] mit Erde.</ta>
            <ta e="T46" id="Seg_762" s="T43">Und gingen wieder nach Hause.</ta>
            <ta e="T47" id="Seg_763" s="T46">Sie tanzen.</ta>
            <ta e="T51" id="Seg_764" s="T47">[Weil] die Katze tot ist.</ta>
            <ta e="T52" id="Seg_765" s="T51">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_766" s="T0">[GVY:] tospak here pronounced with š [AAV]: Tape SU0192</ta>
            <ta e="T12" id="Seg_767" s="T7">[GVY:] a calque of the Russian "взял и…" 'suddenly…'.</ta>
            <ta e="T15" id="Seg_768" s="T12">[GVY:] suʔməluʔpiʔi pronounced with a š- </ta>
            <ta e="T33" id="Seg_769" s="T28">[KlT:] Unclear</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
