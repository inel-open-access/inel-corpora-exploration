<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3D2BA197-FC02-5734-3F10-BFDF1AD04EBC">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SheepAndPriest_flk.wav" />
         <referenced-file url="PKZ_196X_SheepAndPriest_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SheepAndPriest_flk\PKZ_196X_SheepAndPriest_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">123</ud-information>
            <ud-information attribute-name="# HIAT:w">80</ud-information>
            <ud-information attribute-name="# e">78</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T137" time="0.033" type="appl" />
         <tli id="T138" time="0.661" type="appl" />
         <tli id="T139" time="1.288" type="appl" />
         <tli id="T140" time="1.916" type="appl" />
         <tli id="T141" time="3.253332168286273" />
         <tli id="T142" time="3.546" type="appl" />
         <tli id="T143" time="4.139" type="appl" />
         <tli id="T144" time="4.732" type="appl" />
         <tli id="T145" time="5.325" type="appl" />
         <tli id="T146" time="5.918" type="appl" />
         <tli id="T147" time="6.511" type="appl" />
         <tli id="T148" time="7.104" type="appl" />
         <tli id="T149" time="7.863" type="appl" />
         <tli id="T150" time="8.504" type="appl" />
         <tli id="T151" time="9.144" type="appl" />
         <tli id="T152" time="10.979996067966171" />
         <tli id="T153" time="11.624" type="appl" />
         <tli id="T154" time="12.425" type="appl" />
         <tli id="T155" time="14.753328050035908" />
         <tli id="T156" time="15.44" type="appl" />
         <tli id="T157" time="16.43" type="appl" />
         <tli id="T158" time="17.421" type="appl" />
         <tli id="T159" time="18.093" type="appl" />
         <tli id="T160" time="18.762" type="appl" />
         <tli id="T161" time="19.43" type="appl" />
         <tli id="T162" time="20.098" type="appl" />
         <tli id="T163" time="20.766" type="appl" />
         <tli id="T164" time="21.435" type="appl" />
         <tli id="T165" time="22.839991820796662" />
         <tli id="T166" time="23.503" type="appl" />
         <tli id="T167" time="24.141" type="appl" />
         <tli id="T168" time="24.966657725885437" />
         <tli id="T169" time="25.668" type="appl" />
         <tli id="T170" time="26.6933237742177" />
         <tli id="T171" time="27.55" type="appl" />
         <tli id="T172" time="28.253" type="appl" />
         <tli id="T173" time="28.899" type="appl" />
         <tli id="T174" time="29.544" type="appl" />
         <tli id="T175" time="30.19" type="appl" />
         <tli id="T176" time="30.713" type="appl" />
         <tli id="T177" time="31.236" type="appl" />
         <tli id="T178" time="31.76" type="appl" />
         <tli id="T179" time="32.406655061556506" />
         <tli id="T180" time="33.04" type="appl" />
         <tli id="T181" time="35.42665398006814" />
         <tli id="T182" time="36.284" type="appl" />
         <tli id="T183" time="37.53331989231909" />
         <tli id="T184" time="37.957" type="appl" />
         <tli id="T185" time="38.362" type="appl" />
         <tli id="T186" time="38.768" type="appl" />
         <tli id="T187" time="39.173" type="appl" />
         <tli id="T188" time="39.717" type="appl" />
         <tli id="T189" time="40.261" type="appl" />
         <tli id="T190" time="40.805" type="appl" />
         <tli id="T191" time="41.349" type="appl" />
         <tli id="T192" time="41.893" type="appl" />
         <tli id="T193" time="42.437" type="appl" />
         <tli id="T194" time="42.981" type="appl" />
         <tli id="T195" time="43.525" type="appl" />
         <tli id="T196" time="44.069" type="appl" />
         <tli id="T197" time="44.613" type="appl" />
         <tli id="T198" time="45.157" type="appl" />
         <tli id="T199" time="45.772" type="appl" />
         <tli id="T200" time="46.072" type="appl" />
         <tli id="T201" time="46.66664995492605" />
         <tli id="T202" time="47.396" type="appl" />
         <tli id="T203" time="48.04" type="appl" />
         <tli id="T204" time="48.683" type="appl" />
         <tli id="T205" time="49.327" type="appl" />
         <tli id="T206" time="49.834" type="appl" />
         <tli id="T207" time="50.34" type="appl" />
         <tli id="T208" time="50.846" type="appl" />
         <tli id="T209" time="51.353" type="appl" />
         <tli id="T210" time="51.884" type="appl" />
         <tli id="T211" time="52.415" type="appl" />
         <tli id="T212" time="52.946" type="appl" />
         <tli id="T213" time="53.478" type="appl" />
         <tli id="T214" time="54.009" type="appl" />
         <tli id="T215" time="54.54" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T165" start="T164">
            <tli id="T164.tx.1" />
         </timeline-fork>
         <timeline-fork end="T195" start="T194">
            <tli id="T194.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T215" id="Seg_0" n="sc" s="T137">
               <ts e="T141" id="Seg_2" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_4" n="HIAT:w" s="T137">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_7" n="HIAT:w" s="T138">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_10" n="HIAT:w" s="T139">külükzəbi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_13" n="HIAT:w" s="T140">ibi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_17" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_19" n="HIAT:w" s="T141">Dĭn</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_22" n="HIAT:w" s="T142">ulardə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_25" n="HIAT:w" s="T143">iʔgö</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_28" n="HIAT:w" s="T144">ibi</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_32" n="HIAT:w" s="T145">a</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_35" n="HIAT:w" s="T146">noʔ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_38" n="HIAT:w" s="T147">naga</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_42" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_44" n="HIAT:w" s="T148">Dĭgəttə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_47" n="HIAT:w" s="T149">kambi</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_50" n="HIAT:w" s="T150">abəstə</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_54" n="HIAT:w" s="T151">măndə:</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">"</nts>
                  <ts e="T153" id="Seg_58" n="HIAT:w" s="T152">It</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_61" n="HIAT:w" s="T153">măn</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_64" n="HIAT:w" s="T154">ularzaŋdə</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_68" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_70" n="HIAT:w" s="T155">Puskaj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_73" n="HIAT:w" s="T156">tăn</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_76" n="HIAT:w" s="T157">mogəjəʔ</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_80" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_82" n="HIAT:w" s="T158">A</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_85" n="HIAT:w" s="T159">măn</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_88" n="HIAT:w" s="T160">külalləm</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_91" n="HIAT:w" s="T161">dak</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_95" n="HIAT:w" s="T162">tăn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_98" n="HIAT:w" s="T163">dĭgəttə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164.tx.1" id="Seg_101" n="HIAT:w" s="T164">numan</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_104" n="HIAT:w" s="T164.tx.1">üzələl</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_108" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_110" n="HIAT:w" s="T165">Măna</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_113" n="HIAT:w" s="T166">dʼünə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_116" n="HIAT:w" s="T167">elləl</ts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_121" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_123" n="HIAT:w" s="T168">Dĭgəttə</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_126" n="HIAT:w" s="T169">šobi</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_130" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_132" n="HIAT:w" s="T170">Ejü</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_135" n="HIAT:w" s="T171">mobi</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_139" n="HIAT:u" s="T172">
                  <nts id="Seg_140" n="HIAT:ip">(</nts>
                  <ts e="T173" id="Seg_142" n="HIAT:w" s="T172">Sĭr-</ts>
                  <nts id="Seg_143" n="HIAT:ip">)</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_146" n="HIAT:w" s="T173">Sĭre</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_149" n="HIAT:w" s="T174">nagobi</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_153" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_155" n="HIAT:w" s="T175">Dĭ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_158" n="HIAT:w" s="T176">šobi:</ts>
                  <nts id="Seg_159" n="HIAT:ip">"</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_162" n="HIAT:w" s="T177">Kanžəbəj</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_165" n="HIAT:w" s="T178">mănziʔ</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_169" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_171" n="HIAT:w" s="T179">Măn</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_174" n="HIAT:w" s="T180">kallam</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_178" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_180" n="HIAT:w" s="T181">Kondʼo</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_183" n="HIAT:w" s="T182">kallam</ts>
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_188" n="HIAT:u" s="T183">
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <nts id="Seg_190" n="HIAT:ip">(</nts>
                  <ts e="T184" id="Seg_192" n="HIAT:w" s="T183">A</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_195" n="HIAT:w" s="T184">ĭmbi</ts>
                  <nts id="Seg_196" n="HIAT:ip">)</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_199" n="HIAT:w" s="T185">măn</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_202" n="HIAT:w" s="T186">kallam</ts>
                  <nts id="Seg_203" n="HIAT:ip">?</nts>
                  <nts id="Seg_204" n="HIAT:ip">"</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_207" n="HIAT:u" s="T187">
                  <nts id="Seg_208" n="HIAT:ip">"</nts>
                  <ts e="T188" id="Seg_210" n="HIAT:w" s="T187">A</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_213" n="HIAT:w" s="T188">tăn</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <ts e="T190" id="Seg_217" n="HIAT:w" s="T189">m-</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_220" n="HIAT:w" s="T190">m-</ts>
                  <nts id="Seg_221" n="HIAT:ip">)</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_224" n="HIAT:w" s="T191">măn</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_226" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_228" n="HIAT:w" s="T192">ardə</ts>
                  <nts id="Seg_229" n="HIAT:ip">)</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_232" n="HIAT:w" s="T193">kudajdə</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194.tx.1" id="Seg_235" n="HIAT:w" s="T194">numan</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_238" n="HIAT:w" s="T194.tx.1">üzələl</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_242" n="HIAT:w" s="T195">možet</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_245" n="HIAT:w" s="T196">măn</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_248" n="HIAT:w" s="T197">külalləm</ts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_253" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_255" n="HIAT:w" s="T198">A</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_258" n="HIAT:w" s="T199">dĭ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_261" n="HIAT:w" s="T200">măndə:</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_263" n="HIAT:ip">"</nts>
                  <ts e="T202" id="Seg_265" n="HIAT:w" s="T201">It</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_267" n="HIAT:ip">(</nts>
                  <ts e="T203" id="Seg_269" n="HIAT:w" s="T202">bə-</ts>
                  <nts id="Seg_270" n="HIAT:ip">)</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_273" n="HIAT:w" s="T203">bostə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_276" n="HIAT:w" s="T204">ulardə</ts>
                  <nts id="Seg_277" n="HIAT:ip">!</nts>
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_281" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_283" n="HIAT:w" s="T205">A</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_286" n="HIAT:w" s="T206">dĭʔnə</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_289" n="HIAT:w" s="T207">ĭmbi</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_292" n="HIAT:w" s="T208">kereʔ</ts>
                  <nts id="Seg_293" n="HIAT:ip">?</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_296" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_298" n="HIAT:w" s="T209">Ibi</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_301" n="HIAT:w" s="T210">ulardə</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_304" n="HIAT:w" s="T211">da</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_306" n="HIAT:ip">(</nts>
                  <ts e="T213" id="Seg_308" n="HIAT:w" s="T212">ka-</ts>
                  <nts id="Seg_309" n="HIAT:ip">)</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_312" n="HIAT:w" s="T213">kaluʔpi</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_315" n="HIAT:w" s="T214">maːʔndə</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T215" id="Seg_318" n="sc" s="T137">
               <ts e="T138" id="Seg_320" n="e" s="T137">Onʼiʔ </ts>
               <ts e="T139" id="Seg_322" n="e" s="T138">kuza </ts>
               <ts e="T140" id="Seg_324" n="e" s="T139">külükzəbi </ts>
               <ts e="T141" id="Seg_326" n="e" s="T140">ibi. </ts>
               <ts e="T142" id="Seg_328" n="e" s="T141">Dĭn </ts>
               <ts e="T143" id="Seg_330" n="e" s="T142">ulardə </ts>
               <ts e="T144" id="Seg_332" n="e" s="T143">iʔgö </ts>
               <ts e="T145" id="Seg_334" n="e" s="T144">ibi, </ts>
               <ts e="T146" id="Seg_336" n="e" s="T145">a </ts>
               <ts e="T147" id="Seg_338" n="e" s="T146">noʔ </ts>
               <ts e="T148" id="Seg_340" n="e" s="T147">naga. </ts>
               <ts e="T149" id="Seg_342" n="e" s="T148">Dĭgəttə </ts>
               <ts e="T150" id="Seg_344" n="e" s="T149">kambi </ts>
               <ts e="T151" id="Seg_346" n="e" s="T150">abəstə, </ts>
               <ts e="T152" id="Seg_348" n="e" s="T151">măndə: </ts>
               <ts e="T153" id="Seg_350" n="e" s="T152">"It </ts>
               <ts e="T154" id="Seg_352" n="e" s="T153">măn </ts>
               <ts e="T155" id="Seg_354" n="e" s="T154">ularzaŋdə. </ts>
               <ts e="T156" id="Seg_356" n="e" s="T155">Puskaj </ts>
               <ts e="T157" id="Seg_358" n="e" s="T156">tăn </ts>
               <ts e="T158" id="Seg_360" n="e" s="T157">mogəjəʔ. </ts>
               <ts e="T159" id="Seg_362" n="e" s="T158">A </ts>
               <ts e="T160" id="Seg_364" n="e" s="T159">măn </ts>
               <ts e="T161" id="Seg_366" n="e" s="T160">külalləm </ts>
               <ts e="T162" id="Seg_368" n="e" s="T161">dak, </ts>
               <ts e="T163" id="Seg_370" n="e" s="T162">tăn </ts>
               <ts e="T164" id="Seg_372" n="e" s="T163">dĭgəttə </ts>
               <ts e="T165" id="Seg_374" n="e" s="T164">numan üzələl. </ts>
               <ts e="T166" id="Seg_376" n="e" s="T165">Măna </ts>
               <ts e="T167" id="Seg_378" n="e" s="T166">dʼünə </ts>
               <ts e="T168" id="Seg_380" n="e" s="T167">elləl". </ts>
               <ts e="T169" id="Seg_382" n="e" s="T168">Dĭgəttə </ts>
               <ts e="T170" id="Seg_384" n="e" s="T169">šobi. </ts>
               <ts e="T171" id="Seg_386" n="e" s="T170">Ejü </ts>
               <ts e="T172" id="Seg_388" n="e" s="T171">mobi. </ts>
               <ts e="T173" id="Seg_390" n="e" s="T172">(Sĭr-) </ts>
               <ts e="T174" id="Seg_392" n="e" s="T173">Sĭre </ts>
               <ts e="T175" id="Seg_394" n="e" s="T174">nagobi. </ts>
               <ts e="T176" id="Seg_396" n="e" s="T175">Dĭ </ts>
               <ts e="T177" id="Seg_398" n="e" s="T176">šobi:" </ts>
               <ts e="T178" id="Seg_400" n="e" s="T177">Kanžəbəj </ts>
               <ts e="T179" id="Seg_402" n="e" s="T178">mănziʔ. </ts>
               <ts e="T180" id="Seg_404" n="e" s="T179">Măn </ts>
               <ts e="T181" id="Seg_406" n="e" s="T180">kallam. </ts>
               <ts e="T182" id="Seg_408" n="e" s="T181">Kondʼo </ts>
               <ts e="T183" id="Seg_410" n="e" s="T182">kallam". </ts>
               <ts e="T184" id="Seg_412" n="e" s="T183">"(A </ts>
               <ts e="T185" id="Seg_414" n="e" s="T184">ĭmbi) </ts>
               <ts e="T186" id="Seg_416" n="e" s="T185">măn </ts>
               <ts e="T187" id="Seg_418" n="e" s="T186">kallam?" </ts>
               <ts e="T188" id="Seg_420" n="e" s="T187">"A </ts>
               <ts e="T189" id="Seg_422" n="e" s="T188">tăn </ts>
               <ts e="T190" id="Seg_424" n="e" s="T189">(m- </ts>
               <ts e="T191" id="Seg_426" n="e" s="T190">m-) </ts>
               <ts e="T192" id="Seg_428" n="e" s="T191">măn </ts>
               <ts e="T193" id="Seg_430" n="e" s="T192">(ardə) </ts>
               <ts e="T194" id="Seg_432" n="e" s="T193">kudajdə </ts>
               <ts e="T195" id="Seg_434" n="e" s="T194">numan üzələl, </ts>
               <ts e="T196" id="Seg_436" n="e" s="T195">možet </ts>
               <ts e="T197" id="Seg_438" n="e" s="T196">măn </ts>
               <ts e="T198" id="Seg_440" n="e" s="T197">külalləm". </ts>
               <ts e="T199" id="Seg_442" n="e" s="T198">A </ts>
               <ts e="T200" id="Seg_444" n="e" s="T199">dĭ </ts>
               <ts e="T201" id="Seg_446" n="e" s="T200">măndə: </ts>
               <ts e="T202" id="Seg_448" n="e" s="T201">"It </ts>
               <ts e="T203" id="Seg_450" n="e" s="T202">(bə-) </ts>
               <ts e="T204" id="Seg_452" n="e" s="T203">bostə </ts>
               <ts e="T205" id="Seg_454" n="e" s="T204">ulardə!" </ts>
               <ts e="T206" id="Seg_456" n="e" s="T205">A </ts>
               <ts e="T207" id="Seg_458" n="e" s="T206">dĭʔnə </ts>
               <ts e="T208" id="Seg_460" n="e" s="T207">ĭmbi </ts>
               <ts e="T209" id="Seg_462" n="e" s="T208">kereʔ? </ts>
               <ts e="T210" id="Seg_464" n="e" s="T209">Ibi </ts>
               <ts e="T211" id="Seg_466" n="e" s="T210">ulardə </ts>
               <ts e="T212" id="Seg_468" n="e" s="T211">da </ts>
               <ts e="T213" id="Seg_470" n="e" s="T212">(ka-) </ts>
               <ts e="T214" id="Seg_472" n="e" s="T213">kaluʔpi </ts>
               <ts e="T215" id="Seg_474" n="e" s="T214">maːʔndə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T141" id="Seg_475" s="T137">PKZ_196X_SheepAndPriest_flk.001 (001)</ta>
            <ta e="T148" id="Seg_476" s="T141">PKZ_196X_SheepAndPriest_flk.002 (002)</ta>
            <ta e="T155" id="Seg_477" s="T148">PKZ_196X_SheepAndPriest_flk.003 (003) </ta>
            <ta e="T158" id="Seg_478" s="T155">PKZ_196X_SheepAndPriest_flk.004 (005)</ta>
            <ta e="T165" id="Seg_479" s="T158">PKZ_196X_SheepAndPriest_flk.005 (006)</ta>
            <ta e="T168" id="Seg_480" s="T165">PKZ_196X_SheepAndPriest_flk.006 (007)</ta>
            <ta e="T170" id="Seg_481" s="T168">PKZ_196X_SheepAndPriest_flk.007 (008)</ta>
            <ta e="T172" id="Seg_482" s="T170">PKZ_196X_SheepAndPriest_flk.008 (009)</ta>
            <ta e="T175" id="Seg_483" s="T172">PKZ_196X_SheepAndPriest_flk.009 (010)</ta>
            <ta e="T179" id="Seg_484" s="T175">PKZ_196X_SheepAndPriest_flk.010 (011)</ta>
            <ta e="T181" id="Seg_485" s="T179">PKZ_196X_SheepAndPriest_flk.011 (012)</ta>
            <ta e="T183" id="Seg_486" s="T181">PKZ_196X_SheepAndPriest_flk.012 (013)</ta>
            <ta e="T187" id="Seg_487" s="T183">PKZ_196X_SheepAndPriest_flk.013 (014)</ta>
            <ta e="T198" id="Seg_488" s="T187">PKZ_196X_SheepAndPriest_flk.014 (015)</ta>
            <ta e="T205" id="Seg_489" s="T198">PKZ_196X_SheepAndPriest_flk.015 (016) </ta>
            <ta e="T209" id="Seg_490" s="T205">PKZ_196X_SheepAndPriest_flk.016 (018)</ta>
            <ta e="T215" id="Seg_491" s="T209">PKZ_196X_SheepAndPriest_flk.017 (019)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T141" id="Seg_492" s="T137">Onʼiʔ kuza külükzəbi ibi. </ta>
            <ta e="T148" id="Seg_493" s="T141">Dĭn ulardə iʔgö ibi, a noʔ naga. </ta>
            <ta e="T155" id="Seg_494" s="T148">Dĭgəttə kambi abəstə, măndə: "It măn ularzaŋdə. </ta>
            <ta e="T158" id="Seg_495" s="T155">Puskaj tăn mogəjəʔ. </ta>
            <ta e="T165" id="Seg_496" s="T158">A măn külalləm dak, tăn dĭgəttə numan üzələl. </ta>
            <ta e="T168" id="Seg_497" s="T165">Măna dʼünə elləl". </ta>
            <ta e="T170" id="Seg_498" s="T168">Dĭgəttə šobi. </ta>
            <ta e="T172" id="Seg_499" s="T170">Ejü mobi. </ta>
            <ta e="T175" id="Seg_500" s="T172">(Sĭr-) Sĭre nagobi. </ta>
            <ta e="T179" id="Seg_501" s="T175">Dĭ šobi:" Kanžəbəj mănziʔ. </ta>
            <ta e="T181" id="Seg_502" s="T179">Măn kallam. </ta>
            <ta e="T183" id="Seg_503" s="T181">Kondʼo kallam". </ta>
            <ta e="T187" id="Seg_504" s="T183">"(A ĭmbi) măn kallam?" </ta>
            <ta e="T198" id="Seg_505" s="T187">"A tăn (m- m-) măn (ardə) kudajdə numan üzələl, možet măn külalləm". </ta>
            <ta e="T205" id="Seg_506" s="T198">A dĭ măndə: "It (bə-) bostə ulardə!" </ta>
            <ta e="T209" id="Seg_507" s="T205">A dĭʔnə ĭmbi kereʔ? </ta>
            <ta e="T215" id="Seg_508" s="T209">Ibi ulardə da (ka-) kaluʔpi maːʔndə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T138" id="Seg_509" s="T137">onʼiʔ</ta>
            <ta e="T139" id="Seg_510" s="T138">kuza</ta>
            <ta e="T140" id="Seg_511" s="T139">külük-zəbi</ta>
            <ta e="T141" id="Seg_512" s="T140">i-bi</ta>
            <ta e="T142" id="Seg_513" s="T141">dĭ-n</ta>
            <ta e="T143" id="Seg_514" s="T142">ular-də</ta>
            <ta e="T144" id="Seg_515" s="T143">iʔgö</ta>
            <ta e="T145" id="Seg_516" s="T144">i-bi</ta>
            <ta e="T146" id="Seg_517" s="T145">a</ta>
            <ta e="T147" id="Seg_518" s="T146">noʔ</ta>
            <ta e="T148" id="Seg_519" s="T147">naga</ta>
            <ta e="T149" id="Seg_520" s="T148">dĭgəttə</ta>
            <ta e="T150" id="Seg_521" s="T149">kam-bi</ta>
            <ta e="T151" id="Seg_522" s="T150">abəs-tə</ta>
            <ta e="T152" id="Seg_523" s="T151">măn-də</ta>
            <ta e="T153" id="Seg_524" s="T152">i-t</ta>
            <ta e="T154" id="Seg_525" s="T153">măn</ta>
            <ta e="T155" id="Seg_526" s="T154">ular-zaŋ-də</ta>
            <ta e="T156" id="Seg_527" s="T155">puskaj</ta>
            <ta e="T157" id="Seg_528" s="T156">tăn</ta>
            <ta e="T158" id="Seg_529" s="T157">mo-gə-jəʔ</ta>
            <ta e="T159" id="Seg_530" s="T158">a</ta>
            <ta e="T160" id="Seg_531" s="T159">măn</ta>
            <ta e="T161" id="Seg_532" s="T160">kü-lal-lə-m</ta>
            <ta e="T162" id="Seg_533" s="T161">dak</ta>
            <ta e="T163" id="Seg_534" s="T162">tăn</ta>
            <ta e="T164" id="Seg_535" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_536" s="T164">numan üzə-lə-l</ta>
            <ta e="T166" id="Seg_537" s="T165">măna</ta>
            <ta e="T167" id="Seg_538" s="T166">dʼü-nə</ta>
            <ta e="T168" id="Seg_539" s="T167">el-lə-l</ta>
            <ta e="T169" id="Seg_540" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_541" s="T169">šo-bi</ta>
            <ta e="T171" id="Seg_542" s="T170">ejü</ta>
            <ta e="T172" id="Seg_543" s="T171">mo-bi</ta>
            <ta e="T174" id="Seg_544" s="T173">sĭre</ta>
            <ta e="T175" id="Seg_545" s="T174">nago-bi</ta>
            <ta e="T176" id="Seg_546" s="T175">dĭ</ta>
            <ta e="T177" id="Seg_547" s="T176">šo-bi</ta>
            <ta e="T178" id="Seg_548" s="T177">kan-žə-bəj</ta>
            <ta e="T179" id="Seg_549" s="T178">măn-ziʔ</ta>
            <ta e="T180" id="Seg_550" s="T179">măn</ta>
            <ta e="T181" id="Seg_551" s="T180">kal-la-m</ta>
            <ta e="T182" id="Seg_552" s="T181">kondʼo</ta>
            <ta e="T183" id="Seg_553" s="T182">kal-la-m</ta>
            <ta e="T184" id="Seg_554" s="T183">a</ta>
            <ta e="T185" id="Seg_555" s="T184">ĭmbi</ta>
            <ta e="T186" id="Seg_556" s="T185">măn</ta>
            <ta e="T187" id="Seg_557" s="T186">kal-la-m</ta>
            <ta e="T188" id="Seg_558" s="T187">a</ta>
            <ta e="T189" id="Seg_559" s="T188">tăn</ta>
            <ta e="T192" id="Seg_560" s="T191">măn</ta>
            <ta e="T193" id="Seg_561" s="T192">ardə</ta>
            <ta e="T194" id="Seg_562" s="T193">kudaj-də</ta>
            <ta e="T195" id="Seg_563" s="T194">numan üzə-lə-l</ta>
            <ta e="T196" id="Seg_564" s="T195">možet</ta>
            <ta e="T197" id="Seg_565" s="T196">măn</ta>
            <ta e="T198" id="Seg_566" s="T197">kü-lal-lə-m</ta>
            <ta e="T199" id="Seg_567" s="T198">a</ta>
            <ta e="T200" id="Seg_568" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_569" s="T200">măn-də</ta>
            <ta e="T202" id="Seg_570" s="T201">i-t</ta>
            <ta e="T204" id="Seg_571" s="T203">bos-tə</ta>
            <ta e="T205" id="Seg_572" s="T204">ular-də</ta>
            <ta e="T206" id="Seg_573" s="T205">a</ta>
            <ta e="T207" id="Seg_574" s="T206">dĭʔ-nə</ta>
            <ta e="T208" id="Seg_575" s="T207">ĭmbi</ta>
            <ta e="T209" id="Seg_576" s="T208">kereʔ</ta>
            <ta e="T210" id="Seg_577" s="T209">i-bi</ta>
            <ta e="T211" id="Seg_578" s="T210">ular-də</ta>
            <ta e="T212" id="Seg_579" s="T211">da</ta>
            <ta e="T214" id="Seg_580" s="T213">ka-luʔ-pi</ta>
            <ta e="T215" id="Seg_581" s="T214">maːʔ-ndə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T138" id="Seg_582" s="T137">onʼiʔ</ta>
            <ta e="T139" id="Seg_583" s="T138">kuza</ta>
            <ta e="T140" id="Seg_584" s="T139">külük-zəbi</ta>
            <ta e="T141" id="Seg_585" s="T140">i-bi</ta>
            <ta e="T142" id="Seg_586" s="T141">dĭ-n</ta>
            <ta e="T143" id="Seg_587" s="T142">ular-də</ta>
            <ta e="T144" id="Seg_588" s="T143">iʔgö</ta>
            <ta e="T145" id="Seg_589" s="T144">i-bi</ta>
            <ta e="T146" id="Seg_590" s="T145">a</ta>
            <ta e="T147" id="Seg_591" s="T146">noʔ</ta>
            <ta e="T148" id="Seg_592" s="T147">naga</ta>
            <ta e="T149" id="Seg_593" s="T148">dĭgəttə</ta>
            <ta e="T150" id="Seg_594" s="T149">kan-bi</ta>
            <ta e="T151" id="Seg_595" s="T150">abəs-Tə</ta>
            <ta e="T152" id="Seg_596" s="T151">măn-ntə</ta>
            <ta e="T153" id="Seg_597" s="T152">i-t</ta>
            <ta e="T154" id="Seg_598" s="T153">măn</ta>
            <ta e="T155" id="Seg_599" s="T154">ular-zAŋ-də</ta>
            <ta e="T156" id="Seg_600" s="T155">puskaj</ta>
            <ta e="T157" id="Seg_601" s="T156">tăn</ta>
            <ta e="T158" id="Seg_602" s="T157">mo-KV-jəʔ</ta>
            <ta e="T159" id="Seg_603" s="T158">a</ta>
            <ta e="T160" id="Seg_604" s="T159">măn</ta>
            <ta e="T161" id="Seg_605" s="T160">kü-laːm-lV-m</ta>
            <ta e="T162" id="Seg_606" s="T161">tak</ta>
            <ta e="T163" id="Seg_607" s="T162">tăn</ta>
            <ta e="T164" id="Seg_608" s="T163">dĭgəttə</ta>
            <ta e="T165" id="Seg_609" s="T164">numan üzə-lV-l</ta>
            <ta e="T166" id="Seg_610" s="T165">măna</ta>
            <ta e="T167" id="Seg_611" s="T166">tʼo-Tə</ta>
            <ta e="T168" id="Seg_612" s="T167">hen-lV-l</ta>
            <ta e="T169" id="Seg_613" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_614" s="T169">šo-bi</ta>
            <ta e="T171" id="Seg_615" s="T170">ejü</ta>
            <ta e="T172" id="Seg_616" s="T171">mo-bi</ta>
            <ta e="T174" id="Seg_617" s="T173">sĭri</ta>
            <ta e="T175" id="Seg_618" s="T174">naga-bi</ta>
            <ta e="T176" id="Seg_619" s="T175">dĭ</ta>
            <ta e="T177" id="Seg_620" s="T176">šo-bi</ta>
            <ta e="T178" id="Seg_621" s="T177">kan-žə-bəj</ta>
            <ta e="T179" id="Seg_622" s="T178">măn-ziʔ</ta>
            <ta e="T180" id="Seg_623" s="T179">măn</ta>
            <ta e="T181" id="Seg_624" s="T180">kan-lV-m</ta>
            <ta e="T182" id="Seg_625" s="T181">kondʼo</ta>
            <ta e="T183" id="Seg_626" s="T182">kan-lV-m</ta>
            <ta e="T184" id="Seg_627" s="T183">a</ta>
            <ta e="T185" id="Seg_628" s="T184">ĭmbi</ta>
            <ta e="T186" id="Seg_629" s="T185">măn</ta>
            <ta e="T187" id="Seg_630" s="T186">kan-lV-m</ta>
            <ta e="T188" id="Seg_631" s="T187">a</ta>
            <ta e="T189" id="Seg_632" s="T188">tăn</ta>
            <ta e="T192" id="Seg_633" s="T191">măn</ta>
            <ta e="T193" id="Seg_634" s="T192">ardə</ta>
            <ta e="T194" id="Seg_635" s="T193">kudaj-Tə</ta>
            <ta e="T195" id="Seg_636" s="T194">numan üzə-lV-l</ta>
            <ta e="T196" id="Seg_637" s="T195">možet</ta>
            <ta e="T197" id="Seg_638" s="T196">măn</ta>
            <ta e="T198" id="Seg_639" s="T197">kü-laːm-lV-m</ta>
            <ta e="T199" id="Seg_640" s="T198">a</ta>
            <ta e="T200" id="Seg_641" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_642" s="T200">măn-ntə</ta>
            <ta e="T202" id="Seg_643" s="T201">i-t</ta>
            <ta e="T204" id="Seg_644" s="T203">bos-də</ta>
            <ta e="T205" id="Seg_645" s="T204">ular-də</ta>
            <ta e="T206" id="Seg_646" s="T205">a</ta>
            <ta e="T207" id="Seg_647" s="T206">dĭ-Tə</ta>
            <ta e="T208" id="Seg_648" s="T207">ĭmbi</ta>
            <ta e="T209" id="Seg_649" s="T208">kereʔ</ta>
            <ta e="T210" id="Seg_650" s="T209">i-bi</ta>
            <ta e="T211" id="Seg_651" s="T210">ular-də</ta>
            <ta e="T212" id="Seg_652" s="T211">da</ta>
            <ta e="T214" id="Seg_653" s="T213">kan-luʔbdə-bi</ta>
            <ta e="T215" id="Seg_654" s="T214">maʔ-gəndə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T138" id="Seg_655" s="T137">one.[NOM.SG]</ta>
            <ta e="T139" id="Seg_656" s="T138">man.[NOM.SG]</ta>
            <ta e="T140" id="Seg_657" s="T139">cunning-ADJZ.[NOM.SG]</ta>
            <ta e="T141" id="Seg_658" s="T140">be-PST.[3SG]</ta>
            <ta e="T142" id="Seg_659" s="T141">this-GEN</ta>
            <ta e="T143" id="Seg_660" s="T142">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T144" id="Seg_661" s="T143">many</ta>
            <ta e="T145" id="Seg_662" s="T144">be-PST.[3SG]</ta>
            <ta e="T146" id="Seg_663" s="T145">and</ta>
            <ta e="T147" id="Seg_664" s="T146">grass.[NOM.SG]</ta>
            <ta e="T148" id="Seg_665" s="T147">NEG.EX</ta>
            <ta e="T149" id="Seg_666" s="T148">then</ta>
            <ta e="T150" id="Seg_667" s="T149">go-PST.[3SG]</ta>
            <ta e="T151" id="Seg_668" s="T150">priest-LAT</ta>
            <ta e="T152" id="Seg_669" s="T151">say-IPFVZ.[3SG]</ta>
            <ta e="T153" id="Seg_670" s="T152">take-IMP.2SG.O</ta>
            <ta e="T154" id="Seg_671" s="T153">I.NOM</ta>
            <ta e="T155" id="Seg_672" s="T154">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T156" id="Seg_673" s="T155">JUSS</ta>
            <ta e="T157" id="Seg_674" s="T156">you.GEN</ta>
            <ta e="T158" id="Seg_675" s="T157">become-IMP-3PL</ta>
            <ta e="T159" id="Seg_676" s="T158">and</ta>
            <ta e="T160" id="Seg_677" s="T159">I.NOM</ta>
            <ta e="T161" id="Seg_678" s="T160">die-RES-FUT-1SG</ta>
            <ta e="T162" id="Seg_679" s="T161">so</ta>
            <ta e="T163" id="Seg_680" s="T162">you.NOM</ta>
            <ta e="T164" id="Seg_681" s="T163">then</ta>
            <ta e="T165" id="Seg_682" s="T164">pray-FUT-2SG</ta>
            <ta e="T166" id="Seg_683" s="T165">I.ACC</ta>
            <ta e="T167" id="Seg_684" s="T166">place-LAT</ta>
            <ta e="T168" id="Seg_685" s="T167">put-FUT-2SG</ta>
            <ta e="T169" id="Seg_686" s="T168">then</ta>
            <ta e="T170" id="Seg_687" s="T169">come-PST.[3SG]</ta>
            <ta e="T171" id="Seg_688" s="T170">warm.[NOM.SG]</ta>
            <ta e="T172" id="Seg_689" s="T171">become-PST.[3SG]</ta>
            <ta e="T174" id="Seg_690" s="T173">snow.[NOM.SG]</ta>
            <ta e="T175" id="Seg_691" s="T174">NEG.EX-PST.[3SG]</ta>
            <ta e="T176" id="Seg_692" s="T175">this.[NOM.SG]</ta>
            <ta e="T177" id="Seg_693" s="T176">come-PST.[3SG]</ta>
            <ta e="T178" id="Seg_694" s="T177">go-OPT.DU/PL-1DU</ta>
            <ta e="T179" id="Seg_695" s="T178">I-INS</ta>
            <ta e="T180" id="Seg_696" s="T179">I.NOM</ta>
            <ta e="T181" id="Seg_697" s="T180">go-FUT-1SG</ta>
            <ta e="T182" id="Seg_698" s="T181">long.time</ta>
            <ta e="T183" id="Seg_699" s="T182">go-FUT-1SG</ta>
            <ta e="T184" id="Seg_700" s="T183">and</ta>
            <ta e="T185" id="Seg_701" s="T184">what.[NOM.SG]</ta>
            <ta e="T186" id="Seg_702" s="T185">I.NOM</ta>
            <ta e="T187" id="Seg_703" s="T186">go-FUT-1SG</ta>
            <ta e="T188" id="Seg_704" s="T187">and</ta>
            <ta e="T189" id="Seg_705" s="T188">you.NOM</ta>
            <ta e="T192" id="Seg_706" s="T191">I.NOM</ta>
            <ta e="T193" id="Seg_707" s="T192">really</ta>
            <ta e="T194" id="Seg_708" s="T193">God-LAT</ta>
            <ta e="T195" id="Seg_709" s="T194">pray-FUT-2SG</ta>
            <ta e="T196" id="Seg_710" s="T195">maybe</ta>
            <ta e="T197" id="Seg_711" s="T196">I.NOM</ta>
            <ta e="T198" id="Seg_712" s="T197">die-RES-FUT-1SG</ta>
            <ta e="T199" id="Seg_713" s="T198">and</ta>
            <ta e="T200" id="Seg_714" s="T199">this.[NOM.SG]</ta>
            <ta e="T201" id="Seg_715" s="T200">say-IPFVZ.[3SG]</ta>
            <ta e="T202" id="Seg_716" s="T201">take-IMP.2SG.O</ta>
            <ta e="T204" id="Seg_717" s="T203">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T205" id="Seg_718" s="T204">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_719" s="T205">and</ta>
            <ta e="T207" id="Seg_720" s="T206">this-LAT</ta>
            <ta e="T208" id="Seg_721" s="T207">what.[NOM.SG]</ta>
            <ta e="T209" id="Seg_722" s="T208">one.needs</ta>
            <ta e="T210" id="Seg_723" s="T209">take-PST.[3SG]</ta>
            <ta e="T211" id="Seg_724" s="T210">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T212" id="Seg_725" s="T211">and</ta>
            <ta e="T214" id="Seg_726" s="T213">go-MOM-PST.[3SG]</ta>
            <ta e="T215" id="Seg_727" s="T214">tent-LAT/LOC.3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T138" id="Seg_728" s="T137">один.[NOM.SG]</ta>
            <ta e="T139" id="Seg_729" s="T138">мужчина.[NOM.SG]</ta>
            <ta e="T140" id="Seg_730" s="T139">хитрый-ADJZ.[NOM.SG]</ta>
            <ta e="T141" id="Seg_731" s="T140">быть-PST.[3SG]</ta>
            <ta e="T142" id="Seg_732" s="T141">этот-GEN</ta>
            <ta e="T143" id="Seg_733" s="T142">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T144" id="Seg_734" s="T143">много</ta>
            <ta e="T145" id="Seg_735" s="T144">быть-PST.[3SG]</ta>
            <ta e="T146" id="Seg_736" s="T145">а</ta>
            <ta e="T147" id="Seg_737" s="T146">трава.[NOM.SG]</ta>
            <ta e="T148" id="Seg_738" s="T147">NEG.EX</ta>
            <ta e="T149" id="Seg_739" s="T148">тогда</ta>
            <ta e="T150" id="Seg_740" s="T149">пойти-PST.[3SG]</ta>
            <ta e="T151" id="Seg_741" s="T150">священник-LAT</ta>
            <ta e="T152" id="Seg_742" s="T151">сказать-IPFVZ.[3SG]</ta>
            <ta e="T153" id="Seg_743" s="T152">взять-IMP.2SG.O</ta>
            <ta e="T154" id="Seg_744" s="T153">я.NOM</ta>
            <ta e="T155" id="Seg_745" s="T154">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T156" id="Seg_746" s="T155">JUSS</ta>
            <ta e="T157" id="Seg_747" s="T156">ты.GEN</ta>
            <ta e="T158" id="Seg_748" s="T157">стать-IMP-3PL</ta>
            <ta e="T159" id="Seg_749" s="T158">а</ta>
            <ta e="T160" id="Seg_750" s="T159">я.NOM</ta>
            <ta e="T161" id="Seg_751" s="T160">умереть-RES-FUT-1SG</ta>
            <ta e="T162" id="Seg_752" s="T161">так</ta>
            <ta e="T163" id="Seg_753" s="T162">ты.NOM</ta>
            <ta e="T164" id="Seg_754" s="T163">тогда</ta>
            <ta e="T165" id="Seg_755" s="T164">молиться-FUT-2SG</ta>
            <ta e="T166" id="Seg_756" s="T165">я.ACC</ta>
            <ta e="T167" id="Seg_757" s="T166">место-LAT</ta>
            <ta e="T168" id="Seg_758" s="T167">класть-FUT-2SG</ta>
            <ta e="T169" id="Seg_759" s="T168">тогда</ta>
            <ta e="T170" id="Seg_760" s="T169">прийти-PST.[3SG]</ta>
            <ta e="T171" id="Seg_761" s="T170">теплый.[NOM.SG]</ta>
            <ta e="T172" id="Seg_762" s="T171">стать-PST.[3SG]</ta>
            <ta e="T174" id="Seg_763" s="T173">снег.[NOM.SG]</ta>
            <ta e="T175" id="Seg_764" s="T174">NEG.EX-PST.[3SG]</ta>
            <ta e="T176" id="Seg_765" s="T175">этот.[NOM.SG]</ta>
            <ta e="T177" id="Seg_766" s="T176">прийти-PST.[3SG]</ta>
            <ta e="T178" id="Seg_767" s="T177">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T179" id="Seg_768" s="T178">я-INS</ta>
            <ta e="T180" id="Seg_769" s="T179">я.NOM</ta>
            <ta e="T181" id="Seg_770" s="T180">пойти-FUT-1SG</ta>
            <ta e="T182" id="Seg_771" s="T181">долго</ta>
            <ta e="T183" id="Seg_772" s="T182">пойти-FUT-1SG</ta>
            <ta e="T184" id="Seg_773" s="T183">а</ta>
            <ta e="T185" id="Seg_774" s="T184">что.[NOM.SG]</ta>
            <ta e="T186" id="Seg_775" s="T185">я.NOM</ta>
            <ta e="T187" id="Seg_776" s="T186">пойти-FUT-1SG</ta>
            <ta e="T188" id="Seg_777" s="T187">а</ta>
            <ta e="T189" id="Seg_778" s="T188">ты.NOM</ta>
            <ta e="T192" id="Seg_779" s="T191">я.NOM</ta>
            <ta e="T193" id="Seg_780" s="T192">действительно</ta>
            <ta e="T194" id="Seg_781" s="T193">Бог-LAT</ta>
            <ta e="T195" id="Seg_782" s="T194">молиться-FUT-2SG</ta>
            <ta e="T196" id="Seg_783" s="T195">может.быть</ta>
            <ta e="T197" id="Seg_784" s="T196">я.NOM</ta>
            <ta e="T198" id="Seg_785" s="T197">умереть-RES-FUT-1SG</ta>
            <ta e="T199" id="Seg_786" s="T198">а</ta>
            <ta e="T200" id="Seg_787" s="T199">этот.[NOM.SG]</ta>
            <ta e="T201" id="Seg_788" s="T200">сказать-IPFVZ.[3SG]</ta>
            <ta e="T202" id="Seg_789" s="T201">взять-IMP.2SG.O</ta>
            <ta e="T204" id="Seg_790" s="T203">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T205" id="Seg_791" s="T204">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T206" id="Seg_792" s="T205">а</ta>
            <ta e="T207" id="Seg_793" s="T206">этот-LAT</ta>
            <ta e="T208" id="Seg_794" s="T207">что.[NOM.SG]</ta>
            <ta e="T209" id="Seg_795" s="T208">нужно</ta>
            <ta e="T210" id="Seg_796" s="T209">взять-PST.[3SG]</ta>
            <ta e="T211" id="Seg_797" s="T210">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T212" id="Seg_798" s="T211">и</ta>
            <ta e="T214" id="Seg_799" s="T213">пойти-MOM-PST.[3SG]</ta>
            <ta e="T215" id="Seg_800" s="T214">чум-LAT/LOC.3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T138" id="Seg_801" s="T137">num-n:case</ta>
            <ta e="T139" id="Seg_802" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_803" s="T139">adj-n&gt;adj-n:case</ta>
            <ta e="T141" id="Seg_804" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_805" s="T141">dempro-n:case</ta>
            <ta e="T143" id="Seg_806" s="T142">n-n:case.poss</ta>
            <ta e="T144" id="Seg_807" s="T143">quant</ta>
            <ta e="T145" id="Seg_808" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_809" s="T145">conj</ta>
            <ta e="T147" id="Seg_810" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_811" s="T147">v</ta>
            <ta e="T149" id="Seg_812" s="T148">adv</ta>
            <ta e="T150" id="Seg_813" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_814" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_815" s="T151">v-v&gt;v-v:pn</ta>
            <ta e="T153" id="Seg_816" s="T152">v-v:mood.pn</ta>
            <ta e="T154" id="Seg_817" s="T153">pers</ta>
            <ta e="T155" id="Seg_818" s="T154">n-n:num-n:case.poss</ta>
            <ta e="T156" id="Seg_819" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_820" s="T156">pers</ta>
            <ta e="T158" id="Seg_821" s="T157">v-v:mood-v:pn</ta>
            <ta e="T159" id="Seg_822" s="T158">conj</ta>
            <ta e="T160" id="Seg_823" s="T159">pers</ta>
            <ta e="T161" id="Seg_824" s="T160">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_825" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_826" s="T162">pers</ta>
            <ta e="T164" id="Seg_827" s="T163">adv</ta>
            <ta e="T165" id="Seg_828" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_829" s="T165">pers</ta>
            <ta e="T167" id="Seg_830" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_831" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_832" s="T168">adv</ta>
            <ta e="T170" id="Seg_833" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_834" s="T170">adj-n:case</ta>
            <ta e="T172" id="Seg_835" s="T171">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_836" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_837" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_838" s="T175">dempro-n:case</ta>
            <ta e="T177" id="Seg_839" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_840" s="T177">v-v:mood-v:pn</ta>
            <ta e="T179" id="Seg_841" s="T178">pers-n:case</ta>
            <ta e="T180" id="Seg_842" s="T179">pers</ta>
            <ta e="T181" id="Seg_843" s="T180">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_844" s="T181">adv</ta>
            <ta e="T183" id="Seg_845" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_846" s="T183">conj</ta>
            <ta e="T185" id="Seg_847" s="T184">que-n:case</ta>
            <ta e="T186" id="Seg_848" s="T185">pers</ta>
            <ta e="T187" id="Seg_849" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_850" s="T187">conj</ta>
            <ta e="T189" id="Seg_851" s="T188">pers</ta>
            <ta e="T192" id="Seg_852" s="T191">pers</ta>
            <ta e="T193" id="Seg_853" s="T192">adv</ta>
            <ta e="T194" id="Seg_854" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_855" s="T194">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_856" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_857" s="T196">pers</ta>
            <ta e="T198" id="Seg_858" s="T197">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_859" s="T198">conj</ta>
            <ta e="T200" id="Seg_860" s="T199">dempro-n:case</ta>
            <ta e="T201" id="Seg_861" s="T200">v-v&gt;v-v:pn</ta>
            <ta e="T202" id="Seg_862" s="T201">v-v:mood.pn</ta>
            <ta e="T204" id="Seg_863" s="T203">refl-n:case.poss</ta>
            <ta e="T205" id="Seg_864" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_865" s="T205">conj</ta>
            <ta e="T207" id="Seg_866" s="T206">dempro-n:case</ta>
            <ta e="T208" id="Seg_867" s="T207">que-n:case</ta>
            <ta e="T209" id="Seg_868" s="T208">adv</ta>
            <ta e="T210" id="Seg_869" s="T209">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_870" s="T210">n-n:case.poss</ta>
            <ta e="T212" id="Seg_871" s="T211">conj</ta>
            <ta e="T214" id="Seg_872" s="T213">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_873" s="T214">n-n:case.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T138" id="Seg_874" s="T137">num</ta>
            <ta e="T139" id="Seg_875" s="T138">n</ta>
            <ta e="T140" id="Seg_876" s="T139">adj</ta>
            <ta e="T141" id="Seg_877" s="T140">v</ta>
            <ta e="T142" id="Seg_878" s="T141">dempro</ta>
            <ta e="T143" id="Seg_879" s="T142">n</ta>
            <ta e="T144" id="Seg_880" s="T143">quant</ta>
            <ta e="T145" id="Seg_881" s="T144">v</ta>
            <ta e="T146" id="Seg_882" s="T145">conj</ta>
            <ta e="T147" id="Seg_883" s="T146">n</ta>
            <ta e="T148" id="Seg_884" s="T147">v</ta>
            <ta e="T149" id="Seg_885" s="T148">adv</ta>
            <ta e="T150" id="Seg_886" s="T149">v</ta>
            <ta e="T151" id="Seg_887" s="T150">n</ta>
            <ta e="T152" id="Seg_888" s="T151">v</ta>
            <ta e="T153" id="Seg_889" s="T152">v</ta>
            <ta e="T154" id="Seg_890" s="T153">pers</ta>
            <ta e="T155" id="Seg_891" s="T154">n</ta>
            <ta e="T156" id="Seg_892" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_893" s="T156">pers</ta>
            <ta e="T158" id="Seg_894" s="T157">v</ta>
            <ta e="T159" id="Seg_895" s="T158">conj</ta>
            <ta e="T160" id="Seg_896" s="T159">pers</ta>
            <ta e="T161" id="Seg_897" s="T160">v</ta>
            <ta e="T162" id="Seg_898" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_899" s="T162">pers</ta>
            <ta e="T164" id="Seg_900" s="T163">adv</ta>
            <ta e="T165" id="Seg_901" s="T164">v</ta>
            <ta e="T166" id="Seg_902" s="T165">pers</ta>
            <ta e="T167" id="Seg_903" s="T166">n</ta>
            <ta e="T168" id="Seg_904" s="T167">v</ta>
            <ta e="T169" id="Seg_905" s="T168">adv</ta>
            <ta e="T170" id="Seg_906" s="T169">v</ta>
            <ta e="T171" id="Seg_907" s="T170">adj</ta>
            <ta e="T172" id="Seg_908" s="T171">v</ta>
            <ta e="T174" id="Seg_909" s="T173">n</ta>
            <ta e="T175" id="Seg_910" s="T174">v</ta>
            <ta e="T176" id="Seg_911" s="T175">dempro</ta>
            <ta e="T177" id="Seg_912" s="T176">v</ta>
            <ta e="T178" id="Seg_913" s="T177">v</ta>
            <ta e="T179" id="Seg_914" s="T178">pers</ta>
            <ta e="T180" id="Seg_915" s="T179">pers</ta>
            <ta e="T181" id="Seg_916" s="T180">v</ta>
            <ta e="T182" id="Seg_917" s="T181">adv</ta>
            <ta e="T183" id="Seg_918" s="T182">v</ta>
            <ta e="T184" id="Seg_919" s="T183">conj</ta>
            <ta e="T185" id="Seg_920" s="T184">que</ta>
            <ta e="T186" id="Seg_921" s="T185">pers</ta>
            <ta e="T187" id="Seg_922" s="T186">v</ta>
            <ta e="T188" id="Seg_923" s="T187">conj</ta>
            <ta e="T189" id="Seg_924" s="T188">pers</ta>
            <ta e="T192" id="Seg_925" s="T191">pers</ta>
            <ta e="T193" id="Seg_926" s="T192">adv</ta>
            <ta e="T194" id="Seg_927" s="T193">n</ta>
            <ta e="T195" id="Seg_928" s="T194">v</ta>
            <ta e="T196" id="Seg_929" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_930" s="T196">pers</ta>
            <ta e="T198" id="Seg_931" s="T197">v</ta>
            <ta e="T199" id="Seg_932" s="T198">conj</ta>
            <ta e="T200" id="Seg_933" s="T199">dempro</ta>
            <ta e="T201" id="Seg_934" s="T200">v</ta>
            <ta e="T202" id="Seg_935" s="T201">v</ta>
            <ta e="T204" id="Seg_936" s="T203">refl</ta>
            <ta e="T205" id="Seg_937" s="T204">n</ta>
            <ta e="T206" id="Seg_938" s="T205">conj</ta>
            <ta e="T207" id="Seg_939" s="T206">dempro</ta>
            <ta e="T208" id="Seg_940" s="T207">que</ta>
            <ta e="T209" id="Seg_941" s="T208">adv</ta>
            <ta e="T210" id="Seg_942" s="T209">v</ta>
            <ta e="T211" id="Seg_943" s="T210">n</ta>
            <ta e="T212" id="Seg_944" s="T211">conj</ta>
            <ta e="T214" id="Seg_945" s="T213">v</ta>
            <ta e="T215" id="Seg_946" s="T214">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T139" id="Seg_947" s="T138">np.h:Th</ta>
            <ta e="T142" id="Seg_948" s="T141">pro.h:Poss</ta>
            <ta e="T143" id="Seg_949" s="T142">np:Th</ta>
            <ta e="T147" id="Seg_950" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_951" s="T148">adv:Time</ta>
            <ta e="T150" id="Seg_952" s="T149">0.3.h:A</ta>
            <ta e="T151" id="Seg_953" s="T150">np:G</ta>
            <ta e="T152" id="Seg_954" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_955" s="T152">0.2.h:A</ta>
            <ta e="T154" id="Seg_956" s="T153">pro.h:Poss</ta>
            <ta e="T155" id="Seg_957" s="T154">np:Th</ta>
            <ta e="T157" id="Seg_958" s="T156">pro.h:Poss</ta>
            <ta e="T158" id="Seg_959" s="T157">0.3:Th</ta>
            <ta e="T160" id="Seg_960" s="T159">pro.h:P</ta>
            <ta e="T163" id="Seg_961" s="T162">pro.h:A</ta>
            <ta e="T164" id="Seg_962" s="T163">adv:Time</ta>
            <ta e="T166" id="Seg_963" s="T165">pro.h:Th</ta>
            <ta e="T167" id="Seg_964" s="T166">np:G</ta>
            <ta e="T168" id="Seg_965" s="T167">0.2.h:A</ta>
            <ta e="T169" id="Seg_966" s="T168">adv:Time</ta>
            <ta e="T170" id="Seg_967" s="T169">0.3.h:A</ta>
            <ta e="T174" id="Seg_968" s="T173">np:Th</ta>
            <ta e="T176" id="Seg_969" s="T175">pro.h:A</ta>
            <ta e="T178" id="Seg_970" s="T177">0.1.h:A</ta>
            <ta e="T179" id="Seg_971" s="T178">pro.h:Com</ta>
            <ta e="T180" id="Seg_972" s="T179">pro.h:A</ta>
            <ta e="T183" id="Seg_973" s="T182">0.1.h:A</ta>
            <ta e="T186" id="Seg_974" s="T185">pro.h:A</ta>
            <ta e="T189" id="Seg_975" s="T188">pro.h:A</ta>
            <ta e="T192" id="Seg_976" s="T191">pro.h:B</ta>
            <ta e="T194" id="Seg_977" s="T193">np:R</ta>
            <ta e="T197" id="Seg_978" s="T196">pro.h:P</ta>
            <ta e="T200" id="Seg_979" s="T199">pro.h:A</ta>
            <ta e="T202" id="Seg_980" s="T201">0.2.h:A</ta>
            <ta e="T204" id="Seg_981" s="T203">pro.h:Poss</ta>
            <ta e="T205" id="Seg_982" s="T204">np:Th</ta>
            <ta e="T207" id="Seg_983" s="T206">pro.h:B</ta>
            <ta e="T208" id="Seg_984" s="T207">pro:Th</ta>
            <ta e="T210" id="Seg_985" s="T209">0.3.h:A</ta>
            <ta e="T211" id="Seg_986" s="T210">np:Th</ta>
            <ta e="T214" id="Seg_987" s="T213">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T139" id="Seg_988" s="T138">np.h:S</ta>
            <ta e="T140" id="Seg_989" s="T139">adj:pred</ta>
            <ta e="T141" id="Seg_990" s="T140">cop</ta>
            <ta e="T143" id="Seg_991" s="T142">np:S</ta>
            <ta e="T145" id="Seg_992" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_993" s="T146">np:S</ta>
            <ta e="T148" id="Seg_994" s="T147">v:pred</ta>
            <ta e="T150" id="Seg_995" s="T149">v:pred 0.3.h:S</ta>
            <ta e="T152" id="Seg_996" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T153" id="Seg_997" s="T152">v:pred 0.2.h:S</ta>
            <ta e="T155" id="Seg_998" s="T154">np:O</ta>
            <ta e="T156" id="Seg_999" s="T155">ptcl:pred</ta>
            <ta e="T160" id="Seg_1000" s="T159">pro.h:S</ta>
            <ta e="T161" id="Seg_1001" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_1002" s="T162">pro.h:S</ta>
            <ta e="T165" id="Seg_1003" s="T164">v:pred </ta>
            <ta e="T166" id="Seg_1004" s="T165">pro.h:O</ta>
            <ta e="T168" id="Seg_1005" s="T167">v:pred 0.2.h:S</ta>
            <ta e="T170" id="Seg_1006" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T171" id="Seg_1007" s="T170">adj:pred</ta>
            <ta e="T172" id="Seg_1008" s="T171">cop 0.3:S</ta>
            <ta e="T174" id="Seg_1009" s="T173">np:S</ta>
            <ta e="T175" id="Seg_1010" s="T174">v:pred</ta>
            <ta e="T176" id="Seg_1011" s="T175">pro.h:S</ta>
            <ta e="T177" id="Seg_1012" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_1013" s="T177">v:pred 0.1.h:S</ta>
            <ta e="T180" id="Seg_1014" s="T179">pro.h:S</ta>
            <ta e="T181" id="Seg_1015" s="T180">v:pred</ta>
            <ta e="T183" id="Seg_1016" s="T182">v:pred 0.1.h:S</ta>
            <ta e="T186" id="Seg_1017" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_1018" s="T186">v:pred</ta>
            <ta e="T189" id="Seg_1019" s="T188">pro.h:S</ta>
            <ta e="T195" id="Seg_1020" s="T194">v:pred</ta>
            <ta e="T197" id="Seg_1021" s="T196">pro.h:S</ta>
            <ta e="T198" id="Seg_1022" s="T197">v:pred</ta>
            <ta e="T200" id="Seg_1023" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_1024" s="T200">v:pred</ta>
            <ta e="T202" id="Seg_1025" s="T201">v:pred 0.2.h:S</ta>
            <ta e="T205" id="Seg_1026" s="T204">np:O</ta>
            <ta e="T208" id="Seg_1027" s="T207">pro:S</ta>
            <ta e="T209" id="Seg_1028" s="T208">adj:pred</ta>
            <ta e="T210" id="Seg_1029" s="T209">v:pred 0.3.h:S</ta>
            <ta e="T211" id="Seg_1030" s="T210">np:O</ta>
            <ta e="T214" id="Seg_1031" s="T213">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T140" id="Seg_1032" s="T139">TURK:core</ta>
            <ta e="T146" id="Seg_1033" s="T145">RUS:gram</ta>
            <ta e="T151" id="Seg_1034" s="T150">TURK:cult</ta>
            <ta e="T156" id="Seg_1035" s="T155">RUS:mod</ta>
            <ta e="T159" id="Seg_1036" s="T158">RUS:gram</ta>
            <ta e="T162" id="Seg_1037" s="T161">RUS:gram</ta>
            <ta e="T184" id="Seg_1038" s="T183">RUS:gram</ta>
            <ta e="T188" id="Seg_1039" s="T187">RUS:gram</ta>
            <ta e="T194" id="Seg_1040" s="T193">TURK:cult</ta>
            <ta e="T196" id="Seg_1041" s="T195">RUS:mod</ta>
            <ta e="T199" id="Seg_1042" s="T198">RUS:gram</ta>
            <ta e="T206" id="Seg_1043" s="T205">RUS:gram</ta>
            <ta e="T212" id="Seg_1044" s="T211">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T141" id="Seg_1045" s="T137">Один человек был (хитрый?).</ta>
            <ta e="T148" id="Seg_1046" s="T141">У него было много овец, а травы не было.</ta>
            <ta e="T152" id="Seg_1047" s="T148">Потом он пошёл к священнику, сказал:</ta>
            <ta e="T155" id="Seg_1048" s="T152">«Возьми моих овец!</ta>
            <ta e="T158" id="Seg_1049" s="T155">Пусть они станут твоими.</ta>
            <ta e="T165" id="Seg_1050" s="T158">А если я умру, ты тогда помолишься.</ta>
            <ta e="T168" id="Seg_1051" s="T165">Похоронишь меня».</ta>
            <ta e="T170" id="Seg_1052" s="T168">Потом он пришёл.</ta>
            <ta e="T172" id="Seg_1053" s="T170">Стало тепло.</ta>
            <ta e="T175" id="Seg_1054" s="T172">Снега не было.</ta>
            <ta e="T179" id="Seg_1055" s="T175">Он пришёл: «Пойдём со мной.</ta>
            <ta e="T181" id="Seg_1056" s="T179">Я пойду.</ta>
            <ta e="T183" id="Seg_1057" s="T181">Далеко пойду.»</ta>
            <ta e="T187" id="Seg_1058" s="T183">«А зачем мне идти?»</ta>
            <ta e="T198" id="Seg_1059" s="T187">«А ты [за] меня (правда?) Богу помолишься, может я умру».</ta>
            <ta e="T201" id="Seg_1060" s="T198">А он говорит:</ta>
            <ta e="T205" id="Seg_1061" s="T201">«Возьми своих овец!»</ta>
            <ta e="T209" id="Seg_1062" s="T205">А что ему [ещё] нужно?</ta>
            <ta e="T215" id="Seg_1063" s="T209">Он взял овец и пошёл домой.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T141" id="Seg_1064" s="T137">One man was (diligent?).</ta>
            <ta e="T148" id="Seg_1065" s="T141">He had a lot of sheep but no grass.</ta>
            <ta e="T152" id="Seg_1066" s="T148">Then he went to the priest, he says:.</ta>
            <ta e="T155" id="Seg_1067" s="T152">"Take my sheep!</ta>
            <ta e="T158" id="Seg_1068" s="T155">Let them become yours.</ta>
            <ta e="T165" id="Seg_1069" s="T158">And if I will die, then you will pray.</ta>
            <ta e="T168" id="Seg_1070" s="T165">You will bury me."</ta>
            <ta e="T170" id="Seg_1071" s="T168">Then he came.</ta>
            <ta e="T172" id="Seg_1072" s="T170">It became warm.</ta>
            <ta e="T175" id="Seg_1073" s="T172">There was no snow.</ta>
            <ta e="T179" id="Seg_1074" s="T175">He came: "Let's go with me.</ta>
            <ta e="T181" id="Seg_1075" s="T179">I will go.</ta>
            <ta e="T183" id="Seg_1076" s="T181">I will go far."</ta>
            <ta e="T187" id="Seg_1077" s="T183">"But why should I go?"</ta>
            <ta e="T198" id="Seg_1078" s="T187">"But you will (really?) pray to god [for] me, maybe I will die."</ta>
            <ta e="T201" id="Seg_1079" s="T198">And he says:</ta>
            <ta e="T205" id="Seg_1080" s="T201">"Take your sheep!"</ta>
            <ta e="T209" id="Seg_1081" s="T205">But what [else] did he need?</ta>
            <ta e="T215" id="Seg_1082" s="T209">He took the sheep and went home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T141" id="Seg_1083" s="T137">Ein Mann war (fleißig?).</ta>
            <ta e="T148" id="Seg_1084" s="T141">Er hatte viele Schafe aber kein Gras.</ta>
            <ta e="T152" id="Seg_1085" s="T148">Dann ging er zum Priester, er sagte:</ta>
            <ta e="T155" id="Seg_1086" s="T152">„Nehmen Sie meine Schafe!</ta>
            <ta e="T158" id="Seg_1087" s="T155">Lass sie die Ihre werden.</ta>
            <ta e="T165" id="Seg_1088" s="T158">Und wenn ich sterbe, dann werden Sie beten.</ta>
            <ta e="T168" id="Seg_1089" s="T165">Sie werden mich begraben.“</ta>
            <ta e="T170" id="Seg_1090" s="T168">Dann kam er.</ta>
            <ta e="T172" id="Seg_1091" s="T170">Es wurde warm.</ta>
            <ta e="T175" id="Seg_1092" s="T172">Es gab kein Schnee.</ta>
            <ta e="T179" id="Seg_1093" s="T175">Er kam: „Lass uns mit mir gehen.</ta>
            <ta e="T181" id="Seg_1094" s="T179">Ich werde gehen.</ta>
            <ta e="T183" id="Seg_1095" s="T181">Ich werde weit gehen.“</ta>
            <ta e="T187" id="Seg_1096" s="T183">„Aber warum sollte ich gehen?“</ta>
            <ta e="T198" id="Seg_1097" s="T187">„Aber Sie werden (wirklich?) an Gott [für] mich beten, vielleicht werde ich sterben.“</ta>
            <ta e="T201" id="Seg_1098" s="T198">Und er sagt:</ta>
            <ta e="T205" id="Seg_1099" s="T201">„Nimm deine Schafe!“</ta>
            <ta e="T209" id="Seg_1100" s="T205">Aber was [sonst] brauchte er?</ta>
            <ta e="T215" id="Seg_1101" s="T209">Er nahm die Schafe und ging nach Hause.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T141" id="Seg_1102" s="T137">[KlT:] Külük + zəbi (?) 'clever'?</ta>
            <ta e="T155" id="Seg_1103" s="T152">[KlT:] Wrong possessive form.</ta>
            <ta e="T158" id="Seg_1104" s="T155">[GVY:] mogəʔjəʔ</ta>
            <ta e="T198" id="Seg_1105" s="T187">[KlT:] Ardə ’right, correct’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
