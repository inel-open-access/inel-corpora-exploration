<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID11F4E63D-DEF6-BF0D-7158-8E7E69685888">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_CowAndCauldron_flk.wav" />
         <referenced-file url="PKZ_196X_CowAndCauldron_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_CowAndCauldron_flk\PKZ_196X_CowAndCauldron_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">104</ud-information>
            <ud-information attribute-name="# HIAT:w">69</ud-information>
            <ud-information attribute-name="# e">69</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.009" type="appl" />
         <tli id="T1" time="0.639" type="appl" />
         <tli id="T2" time="1.268" type="appl" />
         <tli id="T3" time="1.898" type="appl" />
         <tli id="T4" time="2.6997038005509384" />
         <tli id="T5" time="3.046" type="appl" />
         <tli id="T6" time="3.566" type="appl" />
         <tli id="T7" time="4.538" type="appl" />
         <tli id="T8" time="5.51" type="appl" />
         <tli id="T9" time="6.468668134916758" />
         <tli id="T10" time="7.018" type="appl" />
         <tli id="T11" time="7.553" type="appl" />
         <tli id="T12" time="8.089" type="appl" />
         <tli id="T13" time="9.263" type="appl" />
         <tli id="T14" time="10.437" type="appl" />
         <tli id="T15" time="11.61" type="appl" />
         <tli id="T16" time="12.784" type="appl" />
         <tli id="T17" time="13.958" type="appl" />
         <tli id="T18" time="15.132" type="appl" />
         <tli id="T19" time="16.129" type="appl" />
         <tli id="T20" time="17.125" type="appl" />
         <tli id="T21" time="18.122" type="appl" />
         <tli id="T22" time="18.942" type="appl" />
         <tli id="T23" time="19.748" type="appl" />
         <tli id="T24" time="20.554" type="appl" />
         <tli id="T25" time="21.359" type="appl" />
         <tli id="T26" time="22.164" type="appl" />
         <tli id="T27" time="22.97" type="appl" />
         <tli id="T28" time="23.786" type="appl" />
         <tli id="T29" time="24.617299099838558" />
         <tli id="T30" time="25.535" type="appl" />
         <tli id="T31" time="26.47" type="appl" />
         <tli id="T32" time="27.404" type="appl" />
         <tli id="T33" time="28.338" type="appl" />
         <tli id="T34" time="29.191" type="appl" />
         <tli id="T35" time="30.044" type="appl" />
         <tli id="T36" time="30.897" type="appl" />
         <tli id="T37" time="31.751" type="appl" />
         <tli id="T38" time="32.604" type="appl" />
         <tli id="T39" time="33.457" type="appl" />
         <tli id="T40" time="34.31" type="appl" />
         <tli id="T41" time="34.923" type="appl" />
         <tli id="T42" time="35.536" type="appl" />
         <tli id="T43" time="36.148" type="appl" />
         <tli id="T44" time="36.761" type="appl" />
         <tli id="T45" time="37.49588611876303" />
         <tli id="T46" time="38.426" type="appl" />
         <tli id="T47" time="39.287" type="appl" />
         <tli id="T48" time="40.147" type="appl" />
         <tli id="T49" time="41.007" type="appl" />
         <tli id="T50" time="41.652" type="appl" />
         <tli id="T51" time="42.296" type="appl" />
         <tli id="T52" time="42.947" type="appl" />
         <tli id="T53" time="43.53" type="appl" />
         <tli id="T54" time="44.113" type="appl" />
         <tli id="T55" time="44.696" type="appl" />
         <tli id="T56" time="46.06161299211601" />
         <tli id="T57" time="46.795" type="appl" />
         <tli id="T58" time="47.346" type="appl" />
         <tli id="T59" time="47.896" type="appl" />
         <tli id="T60" time="48.687" type="appl" />
         <tli id="T61" time="49.478" type="appl" />
         <tli id="T62" time="50.269" type="appl" />
         <tli id="T63" time="51.88764045305803" />
         <tli id="T64" time="52.817" type="appl" />
         <tli id="T65" time="53.597" type="appl" />
         <tli id="T66" time="54.378" type="appl" />
         <tli id="T67" time="55.158" type="appl" />
         <tli id="T68" time="56.433808334479615" />
         <tli id="T69" time="57.547" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T69" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tüžöjdə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ibi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">šobi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Aspaʔkən</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">toltanoʔ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">nulaʔpi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">davaj</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">amzittə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Ambi</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">ambi</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">amnutsiʔ</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">aspak</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">šerbi</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_73" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">Dĭgəttə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">nuʔməleʔbə</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">kirgarlaʔbə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">A</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">kuza</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">kubi</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">măndə:</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">"</nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">Oj</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">aspakdə</ts>
                  <nts id="Seg_107" n="HIAT:ip">!</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_110" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">Külalləj</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_114" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">tüj</ts>
                  <nts id="Seg_117" n="HIAT:ip">)</nts>
                  <nts id="Seg_118" n="HIAT:ip">!</nts>
                  <nts id="Seg_119" n="HIAT:ip">"</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_122" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_124" n="HIAT:w" s="T29">Dĭgəttə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">nuʔməbi</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_130" n="HIAT:w" s="T31">baška</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_133" n="HIAT:w" s="T32">kuzanə</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_137" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_139" n="HIAT:w" s="T33">Dĭ</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_142" n="HIAT:w" s="T34">măndə:</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">Ulut</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">sajjaʔlil</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_152" n="HIAT:w" s="T37">dĭgəttə</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_155" n="HIAT:w" s="T38">aspaʔ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_158" n="HIAT:w" s="T39">moləj</ts>
                  <nts id="Seg_159" n="HIAT:ip">"</nts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_163" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_165" n="HIAT:w" s="T40">Dĭ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_168" n="HIAT:w" s="T41">ulut</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_171" n="HIAT:w" s="T42">sajjaʔpi</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_175" n="HIAT:w" s="T43">aspaʔ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_178" n="HIAT:w" s="T44">nuldəbi</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_182" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_184" n="HIAT:w" s="T45">Net</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_187" n="HIAT:w" s="T46">šobi</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_190" n="HIAT:w" s="T47">tüžöj</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_193" n="HIAT:w" s="T48">surdəsʼtə</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_197" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_199" n="HIAT:w" s="T49">Dĭ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_202" n="HIAT:w" s="T50">iʔbolaʔbə</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_206" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_208" n="HIAT:w" s="T51">Dĭgəttə</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_211" n="HIAT:w" s="T52">măndə:</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_213" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">Gijen</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_218" n="HIAT:w" s="T54">tüžöjən</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">ulut</ts>
                  <nts id="Seg_222" n="HIAT:ip">?</nts>
                  <nts id="Seg_223" n="HIAT:ip">"</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_226" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_228" n="HIAT:w" s="T56">Dĭ</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_231" n="HIAT:w" s="T57">măndə:</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_234" n="HIAT:w" s="T58">Surdit</ts>
                  <nts id="Seg_235" n="HIAT:ip">!</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_238" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_240" n="HIAT:w" s="T59">Ulut</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_243" n="HIAT:w" s="T60">dön</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_246" n="HIAT:w" s="T61">aspaktən</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_249" n="HIAT:w" s="T62">iʔbolaʔbə</ts>
                  <nts id="Seg_250" n="HIAT:ip">"</nts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_254" n="HIAT:u" s="T63">
                  <nts id="Seg_255" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_257" n="HIAT:w" s="T63">Dĭgə-</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_260" n="HIAT:w" s="T64">dĭ=</ts>
                  <nts id="Seg_261" n="HIAT:ip">)</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_264" n="HIAT:w" s="T65">Dĭgəttə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_267" n="HIAT:w" s="T66">dĭ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_270" n="HIAT:w" s="T67">surdəbi</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_274" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_276" n="HIAT:w" s="T68">Kabarləj</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T69" id="Seg_279" n="sc" s="T0">
               <ts e="T1" id="Seg_281" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_283" n="e" s="T1">kuzan </ts>
               <ts e="T3" id="Seg_285" n="e" s="T2">tüžöjdə </ts>
               <ts e="T4" id="Seg_287" n="e" s="T3">ibi. </ts>
               <ts e="T5" id="Seg_289" n="e" s="T4">Dĭ </ts>
               <ts e="T6" id="Seg_291" n="e" s="T5">šobi. </ts>
               <ts e="T7" id="Seg_293" n="e" s="T6">Aspaʔkən </ts>
               <ts e="T8" id="Seg_295" n="e" s="T7">toltanoʔ </ts>
               <ts e="T9" id="Seg_297" n="e" s="T8">nulaʔpi. </ts>
               <ts e="T10" id="Seg_299" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_301" n="e" s="T10">davaj </ts>
               <ts e="T12" id="Seg_303" n="e" s="T11">amzittə. </ts>
               <ts e="T13" id="Seg_305" n="e" s="T12">Ambi, </ts>
               <ts e="T14" id="Seg_307" n="e" s="T13">ambi, </ts>
               <ts e="T15" id="Seg_309" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_311" n="e" s="T15">amnutsiʔ </ts>
               <ts e="T17" id="Seg_313" n="e" s="T16">aspak </ts>
               <ts e="T18" id="Seg_315" n="e" s="T17">šerbi. </ts>
               <ts e="T19" id="Seg_317" n="e" s="T18">Dĭgəttə </ts>
               <ts e="T20" id="Seg_319" n="e" s="T19">nuʔməleʔbə, </ts>
               <ts e="T21" id="Seg_321" n="e" s="T20">kirgarlaʔbə. </ts>
               <ts e="T22" id="Seg_323" n="e" s="T21">A </ts>
               <ts e="T23" id="Seg_325" n="e" s="T22">kuza </ts>
               <ts e="T24" id="Seg_327" n="e" s="T23">kubi, </ts>
               <ts e="T25" id="Seg_329" n="e" s="T24">măndə: </ts>
               <ts e="T26" id="Seg_331" n="e" s="T25">"Oj, </ts>
               <ts e="T27" id="Seg_333" n="e" s="T26">aspakdə! </ts>
               <ts e="T28" id="Seg_335" n="e" s="T27">Külalləj </ts>
               <ts e="T29" id="Seg_337" n="e" s="T28">(tüj)!" </ts>
               <ts e="T30" id="Seg_339" n="e" s="T29">Dĭgəttə </ts>
               <ts e="T31" id="Seg_341" n="e" s="T30">nuʔməbi </ts>
               <ts e="T32" id="Seg_343" n="e" s="T31">baška </ts>
               <ts e="T33" id="Seg_345" n="e" s="T32">kuzanə. </ts>
               <ts e="T34" id="Seg_347" n="e" s="T33">Dĭ </ts>
               <ts e="T35" id="Seg_349" n="e" s="T34">măndə: </ts>
               <ts e="T36" id="Seg_351" n="e" s="T35">Ulut </ts>
               <ts e="T37" id="Seg_353" n="e" s="T36">sajjaʔlil, </ts>
               <ts e="T38" id="Seg_355" n="e" s="T37">dĭgəttə </ts>
               <ts e="T39" id="Seg_357" n="e" s="T38">aspaʔ </ts>
               <ts e="T40" id="Seg_359" n="e" s="T39">moləj". </ts>
               <ts e="T41" id="Seg_361" n="e" s="T40">Dĭ </ts>
               <ts e="T42" id="Seg_363" n="e" s="T41">ulut </ts>
               <ts e="T43" id="Seg_365" n="e" s="T42">sajjaʔpi, </ts>
               <ts e="T44" id="Seg_367" n="e" s="T43">aspaʔ </ts>
               <ts e="T45" id="Seg_369" n="e" s="T44">nuldəbi. </ts>
               <ts e="T46" id="Seg_371" n="e" s="T45">Net </ts>
               <ts e="T47" id="Seg_373" n="e" s="T46">šobi </ts>
               <ts e="T48" id="Seg_375" n="e" s="T47">tüžöj </ts>
               <ts e="T49" id="Seg_377" n="e" s="T48">surdəsʼtə. </ts>
               <ts e="T50" id="Seg_379" n="e" s="T49">Dĭ </ts>
               <ts e="T51" id="Seg_381" n="e" s="T50">iʔbolaʔbə. </ts>
               <ts e="T52" id="Seg_383" n="e" s="T51">Dĭgəttə </ts>
               <ts e="T53" id="Seg_385" n="e" s="T52">măndə: </ts>
               <ts e="T54" id="Seg_387" n="e" s="T53">"Gijen </ts>
               <ts e="T55" id="Seg_389" n="e" s="T54">tüžöjən </ts>
               <ts e="T56" id="Seg_391" n="e" s="T55">ulut?" </ts>
               <ts e="T57" id="Seg_393" n="e" s="T56">Dĭ </ts>
               <ts e="T58" id="Seg_395" n="e" s="T57">măndə: </ts>
               <ts e="T59" id="Seg_397" n="e" s="T58">Surdit! </ts>
               <ts e="T60" id="Seg_399" n="e" s="T59">Ulut </ts>
               <ts e="T61" id="Seg_401" n="e" s="T60">dön </ts>
               <ts e="T62" id="Seg_403" n="e" s="T61">aspaktən </ts>
               <ts e="T63" id="Seg_405" n="e" s="T62">iʔbolaʔbə". </ts>
               <ts e="T64" id="Seg_407" n="e" s="T63">(Dĭgə- </ts>
               <ts e="T65" id="Seg_409" n="e" s="T64">dĭ=) </ts>
               <ts e="T66" id="Seg_411" n="e" s="T65">Dĭgəttə </ts>
               <ts e="T67" id="Seg_413" n="e" s="T66">dĭ </ts>
               <ts e="T68" id="Seg_415" n="e" s="T67">surdəbi. </ts>
               <ts e="T69" id="Seg_417" n="e" s="T68">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_418" s="T0">PKZ_196X_CowAndCauldron_flk.001 (002)</ta>
            <ta e="T6" id="Seg_419" s="T4">PKZ_196X_CowAndCauldron_flk.002 (003)</ta>
            <ta e="T9" id="Seg_420" s="T6">PKZ_196X_CowAndCauldron_flk.003 (004)</ta>
            <ta e="T12" id="Seg_421" s="T9">PKZ_196X_CowAndCauldron_flk.004 (005)</ta>
            <ta e="T18" id="Seg_422" s="T12">PKZ_196X_CowAndCauldron_flk.005 (006)</ta>
            <ta e="T21" id="Seg_423" s="T18">PKZ_196X_CowAndCauldron_flk.006 (007)</ta>
            <ta e="T27" id="Seg_424" s="T21">PKZ_196X_CowAndCauldron_flk.007 (008)</ta>
            <ta e="T29" id="Seg_425" s="T27">PKZ_196X_CowAndCauldron_flk.008 (009)</ta>
            <ta e="T33" id="Seg_426" s="T29">PKZ_196X_CowAndCauldron_flk.009 (010)</ta>
            <ta e="T40" id="Seg_427" s="T33">PKZ_196X_CowAndCauldron_flk.010 (011)</ta>
            <ta e="T45" id="Seg_428" s="T40">PKZ_196X_CowAndCauldron_flk.011 (012)</ta>
            <ta e="T49" id="Seg_429" s="T45">PKZ_196X_CowAndCauldron_flk.012 (013)</ta>
            <ta e="T51" id="Seg_430" s="T49">PKZ_196X_CowAndCauldron_flk.013 (014)</ta>
            <ta e="T56" id="Seg_431" s="T51">PKZ_196X_CowAndCauldron_flk.014 (015)</ta>
            <ta e="T59" id="Seg_432" s="T56">PKZ_196X_CowAndCauldron_flk.015 (016)</ta>
            <ta e="T63" id="Seg_433" s="T59">PKZ_196X_CowAndCauldron_flk.016 (017)</ta>
            <ta e="T68" id="Seg_434" s="T63">PKZ_196X_CowAndCauldron_flk.017 (018)</ta>
            <ta e="T69" id="Seg_435" s="T68">PKZ_196X_CowAndCauldron_flk.018 (019)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_436" s="T0">Onʼiʔ kuzan tüžöjdə ibi. </ta>
            <ta e="T6" id="Seg_437" s="T4">Dĭ šobi. </ta>
            <ta e="T9" id="Seg_438" s="T6">Aspaʔkən toltanoʔ nulaʔpi. </ta>
            <ta e="T12" id="Seg_439" s="T9">Dĭ davaj amzittə. </ta>
            <ta e="T18" id="Seg_440" s="T12">Ambi, ambi, i amnutsiʔ aspak šerbi. </ta>
            <ta e="T21" id="Seg_441" s="T18">Dĭgəttə nuʔməleʔbə, kirgarlaʔbə. </ta>
            <ta e="T27" id="Seg_442" s="T21">A kuza kubi, măndə: "Oj, aspakdə!" </ta>
            <ta e="T29" id="Seg_443" s="T27">Külalləj (tüj)!" </ta>
            <ta e="T33" id="Seg_444" s="T29">Dĭgəttə nuʔməbi baška kuzanə. </ta>
            <ta e="T40" id="Seg_445" s="T33">Dĭ măndə:" Ulut sajjaʔlil, dĭgəttə aspaʔ moləj". </ta>
            <ta e="T45" id="Seg_446" s="T40">Dĭ ulut sajjaʔpi, aspaʔ nuldəbi. </ta>
            <ta e="T49" id="Seg_447" s="T45">Net šobi tüžöj surdəsʼtə. </ta>
            <ta e="T51" id="Seg_448" s="T49">Dĭ iʔbolaʔbə. </ta>
            <ta e="T56" id="Seg_449" s="T51">Dĭgəttə măndə: "Gijen tüžöjən ulut?" </ta>
            <ta e="T59" id="Seg_450" s="T56">Dĭ măndə: "Surdit! </ta>
            <ta e="T63" id="Seg_451" s="T59">Ulut dön aspaktən iʔbolaʔbə". </ta>
            <ta e="T68" id="Seg_452" s="T63">(Dĭgə- dĭ=) Dĭgəttə dĭ surdəbi. </ta>
            <ta e="T69" id="Seg_453" s="T68">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_454" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_455" s="T1">kuza-n</ta>
            <ta e="T3" id="Seg_456" s="T2">tüžöj-də</ta>
            <ta e="T4" id="Seg_457" s="T3">i-bi</ta>
            <ta e="T5" id="Seg_458" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_459" s="T5">šo-bi</ta>
            <ta e="T7" id="Seg_460" s="T6">aspaʔ-kən</ta>
            <ta e="T8" id="Seg_461" s="T7">toltanoʔ</ta>
            <ta e="T9" id="Seg_462" s="T8">nu-laʔ-pi</ta>
            <ta e="T10" id="Seg_463" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_464" s="T10">davaj</ta>
            <ta e="T12" id="Seg_465" s="T11">am-zittə</ta>
            <ta e="T13" id="Seg_466" s="T12">am-bi</ta>
            <ta e="T14" id="Seg_467" s="T13">am-bi</ta>
            <ta e="T15" id="Seg_468" s="T14">i</ta>
            <ta e="T16" id="Seg_469" s="T15">amnu-t-siʔ</ta>
            <ta e="T17" id="Seg_470" s="T16">aspak</ta>
            <ta e="T18" id="Seg_471" s="T17">šer-bi</ta>
            <ta e="T19" id="Seg_472" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_473" s="T19">nuʔmə-leʔbə</ta>
            <ta e="T21" id="Seg_474" s="T20">kirgar-laʔbə</ta>
            <ta e="T22" id="Seg_475" s="T21">a</ta>
            <ta e="T23" id="Seg_476" s="T22">kuza</ta>
            <ta e="T24" id="Seg_477" s="T23">ku-bi</ta>
            <ta e="T25" id="Seg_478" s="T24">măn-də</ta>
            <ta e="T26" id="Seg_479" s="T25">oj</ta>
            <ta e="T27" id="Seg_480" s="T26">aspak-də</ta>
            <ta e="T28" id="Seg_481" s="T27">kü-lal-lə-j</ta>
            <ta e="T29" id="Seg_482" s="T28">tüj</ta>
            <ta e="T30" id="Seg_483" s="T29">dĭgəttə</ta>
            <ta e="T31" id="Seg_484" s="T30">nuʔmə-bi</ta>
            <ta e="T32" id="Seg_485" s="T31">baška</ta>
            <ta e="T33" id="Seg_486" s="T32">kuza-nə</ta>
            <ta e="T34" id="Seg_487" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_488" s="T34">măn-də</ta>
            <ta e="T36" id="Seg_489" s="T35">ulu-t</ta>
            <ta e="T37" id="Seg_490" s="T36">saj-jaʔ-li-l</ta>
            <ta e="T38" id="Seg_491" s="T37">dĭgəttə</ta>
            <ta e="T39" id="Seg_492" s="T38">aspaʔ</ta>
            <ta e="T40" id="Seg_493" s="T39">mo-lə-j</ta>
            <ta e="T41" id="Seg_494" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_495" s="T41">ulu-t</ta>
            <ta e="T43" id="Seg_496" s="T42">saj-jaʔ-pi</ta>
            <ta e="T44" id="Seg_497" s="T43">aspaʔ</ta>
            <ta e="T45" id="Seg_498" s="T44">nuldə-bi</ta>
            <ta e="T46" id="Seg_499" s="T45">ne-t</ta>
            <ta e="T47" id="Seg_500" s="T46">šo-bi</ta>
            <ta e="T48" id="Seg_501" s="T47">tüžöj</ta>
            <ta e="T49" id="Seg_502" s="T48">surdə-sʼtə</ta>
            <ta e="T50" id="Seg_503" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_504" s="T50">iʔbo-laʔbə</ta>
            <ta e="T52" id="Seg_505" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_506" s="T52">măn-də</ta>
            <ta e="T54" id="Seg_507" s="T53">gijen</ta>
            <ta e="T55" id="Seg_508" s="T54">tüžöj-ə-n</ta>
            <ta e="T56" id="Seg_509" s="T55">ulu-t</ta>
            <ta e="T57" id="Seg_510" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_511" s="T57">măn-də</ta>
            <ta e="T59" id="Seg_512" s="T58">surd-it</ta>
            <ta e="T60" id="Seg_513" s="T59">ulu-t</ta>
            <ta e="T61" id="Seg_514" s="T60">dön</ta>
            <ta e="T62" id="Seg_515" s="T61">aspak-tən</ta>
            <ta e="T63" id="Seg_516" s="T62">iʔbo-laʔbə</ta>
            <ta e="T65" id="Seg_517" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_518" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_519" s="T66">dĭ</ta>
            <ta e="T68" id="Seg_520" s="T67">surdə-bi</ta>
            <ta e="T69" id="Seg_521" s="T68">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_522" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_523" s="T1">kuza-n</ta>
            <ta e="T3" id="Seg_524" s="T2">tüžöj-də</ta>
            <ta e="T4" id="Seg_525" s="T3">i-bi</ta>
            <ta e="T5" id="Seg_526" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_527" s="T5">šo-bi</ta>
            <ta e="T7" id="Seg_528" s="T6">aspaʔ-Kən</ta>
            <ta e="T8" id="Seg_529" s="T7">toltano</ta>
            <ta e="T9" id="Seg_530" s="T8">nu-lAʔ-bi</ta>
            <ta e="T10" id="Seg_531" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_532" s="T10">davaj</ta>
            <ta e="T12" id="Seg_533" s="T11">am-zittə</ta>
            <ta e="T13" id="Seg_534" s="T12">am-bi</ta>
            <ta e="T14" id="Seg_535" s="T13">am-bi</ta>
            <ta e="T15" id="Seg_536" s="T14">i</ta>
            <ta e="T16" id="Seg_537" s="T15">amnu-t-ziʔ</ta>
            <ta e="T17" id="Seg_538" s="T16">aspaʔ</ta>
            <ta e="T18" id="Seg_539" s="T17">šer-bi</ta>
            <ta e="T19" id="Seg_540" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_541" s="T19">nuʔmə-laʔbə</ta>
            <ta e="T21" id="Seg_542" s="T20">kirgaːr-laʔbə</ta>
            <ta e="T22" id="Seg_543" s="T21">a</ta>
            <ta e="T23" id="Seg_544" s="T22">kuza</ta>
            <ta e="T24" id="Seg_545" s="T23">ku-bi</ta>
            <ta e="T25" id="Seg_546" s="T24">măn-ntə</ta>
            <ta e="T26" id="Seg_547" s="T25">oi</ta>
            <ta e="T27" id="Seg_548" s="T26">aspaʔ-də</ta>
            <ta e="T28" id="Seg_549" s="T27">kü-laːm-lV-j</ta>
            <ta e="T29" id="Seg_550" s="T28">tüj</ta>
            <ta e="T30" id="Seg_551" s="T29">dĭgəttə</ta>
            <ta e="T31" id="Seg_552" s="T30">nuʔmə-bi</ta>
            <ta e="T32" id="Seg_553" s="T31">baška</ta>
            <ta e="T33" id="Seg_554" s="T32">kuza-Tə</ta>
            <ta e="T34" id="Seg_555" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_556" s="T34">măn-ntə</ta>
            <ta e="T36" id="Seg_557" s="T35">ulu-t</ta>
            <ta e="T37" id="Seg_558" s="T36">săj-hʼaʔ-lV-l</ta>
            <ta e="T38" id="Seg_559" s="T37">dĭgəttə</ta>
            <ta e="T39" id="Seg_560" s="T38">aspaʔ</ta>
            <ta e="T40" id="Seg_561" s="T39">mo-lV-j</ta>
            <ta e="T41" id="Seg_562" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_563" s="T41">ulu-t</ta>
            <ta e="T43" id="Seg_564" s="T42">săj-hʼaʔ-bi</ta>
            <ta e="T44" id="Seg_565" s="T43">aspaʔ</ta>
            <ta e="T45" id="Seg_566" s="T44">nuldə-bi</ta>
            <ta e="T46" id="Seg_567" s="T45">ne-t</ta>
            <ta e="T47" id="Seg_568" s="T46">šo-bi</ta>
            <ta e="T48" id="Seg_569" s="T47">tüžöj</ta>
            <ta e="T49" id="Seg_570" s="T48">surdo-zittə</ta>
            <ta e="T50" id="Seg_571" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_572" s="T50">iʔbö-laʔbə</ta>
            <ta e="T52" id="Seg_573" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_574" s="T52">măn-ntə</ta>
            <ta e="T54" id="Seg_575" s="T53">gijen</ta>
            <ta e="T55" id="Seg_576" s="T54">tüžöj-ə-n</ta>
            <ta e="T56" id="Seg_577" s="T55">ulu-t</ta>
            <ta e="T57" id="Seg_578" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_579" s="T57">măn-ntə</ta>
            <ta e="T59" id="Seg_580" s="T58">surdo-t</ta>
            <ta e="T60" id="Seg_581" s="T59">ulu-t</ta>
            <ta e="T61" id="Seg_582" s="T60">dön</ta>
            <ta e="T62" id="Seg_583" s="T61">aspaʔ-dən</ta>
            <ta e="T63" id="Seg_584" s="T62">iʔbö-laʔbə</ta>
            <ta e="T65" id="Seg_585" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_586" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_587" s="T66">dĭ</ta>
            <ta e="T68" id="Seg_588" s="T67">surdo-bi</ta>
            <ta e="T69" id="Seg_589" s="T68">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_590" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_591" s="T1">man-GEN</ta>
            <ta e="T3" id="Seg_592" s="T2">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T4" id="Seg_593" s="T3">be-PST.[3SG]</ta>
            <ta e="T5" id="Seg_594" s="T4">this.[NOM.SG]</ta>
            <ta e="T6" id="Seg_595" s="T5">come-PST.[3SG]</ta>
            <ta e="T7" id="Seg_596" s="T6">cauldron-LOC</ta>
            <ta e="T8" id="Seg_597" s="T7">potato.[NOM.SG]</ta>
            <ta e="T9" id="Seg_598" s="T8">stand-CVB-PST.[3SG]</ta>
            <ta e="T10" id="Seg_599" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_600" s="T10">INCH</ta>
            <ta e="T12" id="Seg_601" s="T11">eat-INF.LAT</ta>
            <ta e="T13" id="Seg_602" s="T12">eat-PST.[3SG]</ta>
            <ta e="T14" id="Seg_603" s="T13">eat-PST.[3SG]</ta>
            <ta e="T15" id="Seg_604" s="T14">and</ta>
            <ta e="T16" id="Seg_605" s="T15">horn-3SG-INS</ta>
            <ta e="T17" id="Seg_606" s="T16">cauldron.[NOM.SG]</ta>
            <ta e="T18" id="Seg_607" s="T17">dress-PST.[3SG]</ta>
            <ta e="T19" id="Seg_608" s="T18">then</ta>
            <ta e="T20" id="Seg_609" s="T19">run-DUR.[3SG]</ta>
            <ta e="T21" id="Seg_610" s="T20">shout-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_611" s="T21">and</ta>
            <ta e="T23" id="Seg_612" s="T22">man.[NOM.SG]</ta>
            <ta e="T24" id="Seg_613" s="T23">see-PST.[3SG]</ta>
            <ta e="T25" id="Seg_614" s="T24">say-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_615" s="T25">oh</ta>
            <ta e="T27" id="Seg_616" s="T26">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T28" id="Seg_617" s="T27">die-RES-FUT-3SG</ta>
            <ta e="T29" id="Seg_618" s="T28">now</ta>
            <ta e="T30" id="Seg_619" s="T29">then</ta>
            <ta e="T31" id="Seg_620" s="T30">run-PST.[3SG]</ta>
            <ta e="T32" id="Seg_621" s="T31">another.[NOM.SG]</ta>
            <ta e="T33" id="Seg_622" s="T32">man-LAT</ta>
            <ta e="T34" id="Seg_623" s="T33">this.[NOM.SG]</ta>
            <ta e="T35" id="Seg_624" s="T34">say-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_625" s="T35">head-NOM/GEN.3SG</ta>
            <ta e="T37" id="Seg_626" s="T36">off-cut-FUT-2SG</ta>
            <ta e="T38" id="Seg_627" s="T37">then</ta>
            <ta e="T39" id="Seg_628" s="T38">cauldron.[NOM.SG]</ta>
            <ta e="T40" id="Seg_629" s="T39">become-FUT-3SG</ta>
            <ta e="T41" id="Seg_630" s="T40">this.[NOM.SG]</ta>
            <ta e="T42" id="Seg_631" s="T41">head-NOM/GEN.3SG</ta>
            <ta e="T43" id="Seg_632" s="T42">off-cut-PST.[3SG]</ta>
            <ta e="T44" id="Seg_633" s="T43">cauldron.[NOM.SG]</ta>
            <ta e="T45" id="Seg_634" s="T44">place-PST.[3SG]</ta>
            <ta e="T46" id="Seg_635" s="T45">woman-NOM/GEN.3SG</ta>
            <ta e="T47" id="Seg_636" s="T46">come-PST.[3SG]</ta>
            <ta e="T48" id="Seg_637" s="T47">cow.[NOM.SG]</ta>
            <ta e="T49" id="Seg_638" s="T48">milk-INF.LAT</ta>
            <ta e="T50" id="Seg_639" s="T49">this.[NOM.SG]</ta>
            <ta e="T51" id="Seg_640" s="T50">lie-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_641" s="T51">then</ta>
            <ta e="T53" id="Seg_642" s="T52">say-IPFVZ.[3SG]</ta>
            <ta e="T54" id="Seg_643" s="T53">where</ta>
            <ta e="T55" id="Seg_644" s="T54">cow-EP-GEN</ta>
            <ta e="T56" id="Seg_645" s="T55">head-NOM/GEN.3SG</ta>
            <ta e="T57" id="Seg_646" s="T56">this.[NOM.SG]</ta>
            <ta e="T58" id="Seg_647" s="T57">say-IPFVZ.[3SG]</ta>
            <ta e="T59" id="Seg_648" s="T58">milk-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_649" s="T59">head-NOM/GEN.3SG</ta>
            <ta e="T61" id="Seg_650" s="T60">here</ta>
            <ta e="T62" id="Seg_651" s="T61">cauldron-NOM/GEN/ACC.3PL</ta>
            <ta e="T63" id="Seg_652" s="T62">lie-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_653" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_654" s="T65">then</ta>
            <ta e="T67" id="Seg_655" s="T66">this.[NOM.SG]</ta>
            <ta e="T68" id="Seg_656" s="T67">milk-PST.[3SG]</ta>
            <ta e="T69" id="Seg_657" s="T68">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_658" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_659" s="T1">мужчина-GEN</ta>
            <ta e="T3" id="Seg_660" s="T2">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T4" id="Seg_661" s="T3">быть-PST.[3SG]</ta>
            <ta e="T5" id="Seg_662" s="T4">этот.[NOM.SG]</ta>
            <ta e="T6" id="Seg_663" s="T5">прийти-PST.[3SG]</ta>
            <ta e="T7" id="Seg_664" s="T6">котел-LOC</ta>
            <ta e="T8" id="Seg_665" s="T7">картофель.[NOM.SG]</ta>
            <ta e="T9" id="Seg_666" s="T8">стоять-CVB-PST.[3SG]</ta>
            <ta e="T10" id="Seg_667" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_668" s="T10">INCH</ta>
            <ta e="T12" id="Seg_669" s="T11">съесть-INF.LAT</ta>
            <ta e="T13" id="Seg_670" s="T12">съесть-PST.[3SG]</ta>
            <ta e="T14" id="Seg_671" s="T13">съесть-PST.[3SG]</ta>
            <ta e="T15" id="Seg_672" s="T14">и</ta>
            <ta e="T16" id="Seg_673" s="T15">рог-3SG-INS</ta>
            <ta e="T17" id="Seg_674" s="T16">котел.[NOM.SG]</ta>
            <ta e="T18" id="Seg_675" s="T17">надеть-PST.[3SG]</ta>
            <ta e="T19" id="Seg_676" s="T18">тогда</ta>
            <ta e="T20" id="Seg_677" s="T19">бежать-DUR.[3SG]</ta>
            <ta e="T21" id="Seg_678" s="T20">кричать-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_679" s="T21">а</ta>
            <ta e="T23" id="Seg_680" s="T22">мужчина.[NOM.SG]</ta>
            <ta e="T24" id="Seg_681" s="T23">видеть-PST.[3SG]</ta>
            <ta e="T25" id="Seg_682" s="T24">сказать-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_683" s="T25">о</ta>
            <ta e="T27" id="Seg_684" s="T26">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T28" id="Seg_685" s="T27">умереть-RES-FUT-3SG</ta>
            <ta e="T29" id="Seg_686" s="T28">сейчас</ta>
            <ta e="T30" id="Seg_687" s="T29">тогда</ta>
            <ta e="T31" id="Seg_688" s="T30">бежать-PST.[3SG]</ta>
            <ta e="T32" id="Seg_689" s="T31">другой.[NOM.SG]</ta>
            <ta e="T33" id="Seg_690" s="T32">мужчина-LAT</ta>
            <ta e="T34" id="Seg_691" s="T33">этот.[NOM.SG]</ta>
            <ta e="T35" id="Seg_692" s="T34">сказать-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_693" s="T35">голова-NOM/GEN.3SG</ta>
            <ta e="T37" id="Seg_694" s="T36">от-резать-FUT-2SG</ta>
            <ta e="T38" id="Seg_695" s="T37">тогда</ta>
            <ta e="T39" id="Seg_696" s="T38">котел.[NOM.SG]</ta>
            <ta e="T40" id="Seg_697" s="T39">стать-FUT-3SG</ta>
            <ta e="T41" id="Seg_698" s="T40">этот.[NOM.SG]</ta>
            <ta e="T42" id="Seg_699" s="T41">голова-NOM/GEN.3SG</ta>
            <ta e="T43" id="Seg_700" s="T42">от-резать-PST.[3SG]</ta>
            <ta e="T44" id="Seg_701" s="T43">котел.[NOM.SG]</ta>
            <ta e="T45" id="Seg_702" s="T44">поставить-PST.[3SG]</ta>
            <ta e="T46" id="Seg_703" s="T45">женщина-NOM/GEN.3SG</ta>
            <ta e="T47" id="Seg_704" s="T46">прийти-PST.[3SG]</ta>
            <ta e="T48" id="Seg_705" s="T47">корова.[NOM.SG]</ta>
            <ta e="T49" id="Seg_706" s="T48">доить-INF.LAT</ta>
            <ta e="T50" id="Seg_707" s="T49">этот.[NOM.SG]</ta>
            <ta e="T51" id="Seg_708" s="T50">лежать-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_709" s="T51">тогда</ta>
            <ta e="T53" id="Seg_710" s="T52">сказать-IPFVZ.[3SG]</ta>
            <ta e="T54" id="Seg_711" s="T53">где</ta>
            <ta e="T55" id="Seg_712" s="T54">корова-EP-GEN</ta>
            <ta e="T56" id="Seg_713" s="T55">голова-NOM/GEN.3SG</ta>
            <ta e="T57" id="Seg_714" s="T56">этот.[NOM.SG]</ta>
            <ta e="T58" id="Seg_715" s="T57">сказать-IPFVZ.[3SG]</ta>
            <ta e="T59" id="Seg_716" s="T58">доить-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_717" s="T59">голова-NOM/GEN.3SG</ta>
            <ta e="T61" id="Seg_718" s="T60">здесь</ta>
            <ta e="T62" id="Seg_719" s="T61">котел-NOM/GEN/ACC.3PL</ta>
            <ta e="T63" id="Seg_720" s="T62">лежать-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_721" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_722" s="T65">тогда</ta>
            <ta e="T67" id="Seg_723" s="T66">этот.[NOM.SG]</ta>
            <ta e="T68" id="Seg_724" s="T67">доить-PST.[3SG]</ta>
            <ta e="T69" id="Seg_725" s="T68">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_726" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_727" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_728" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_729" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_730" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_731" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_732" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_733" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_734" s="T8">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_735" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_736" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_737" s="T11">v-v:n.fin</ta>
            <ta e="T13" id="Seg_738" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_739" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_740" s="T14">conj</ta>
            <ta e="T16" id="Seg_741" s="T15">n-n:case.poss-n:case</ta>
            <ta e="T17" id="Seg_742" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_743" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_744" s="T18">adv</ta>
            <ta e="T20" id="Seg_745" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_746" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_747" s="T21">conj</ta>
            <ta e="T23" id="Seg_748" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_749" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_750" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_751" s="T25">interj</ta>
            <ta e="T27" id="Seg_752" s="T26">n-n:case.poss</ta>
            <ta e="T28" id="Seg_753" s="T27">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_754" s="T28">adv</ta>
            <ta e="T30" id="Seg_755" s="T29">adv</ta>
            <ta e="T31" id="Seg_756" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_757" s="T31">adj-n:case</ta>
            <ta e="T33" id="Seg_758" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_759" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_760" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_761" s="T35">n-n:case.poss</ta>
            <ta e="T37" id="Seg_762" s="T36">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_763" s="T37">adv</ta>
            <ta e="T39" id="Seg_764" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_765" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_766" s="T40">dempro-n:case</ta>
            <ta e="T42" id="Seg_767" s="T41">n-n:case.poss</ta>
            <ta e="T43" id="Seg_768" s="T42">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_769" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_770" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_771" s="T45">n-n:case.poss</ta>
            <ta e="T47" id="Seg_772" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_773" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_774" s="T48">v-v:n.fin</ta>
            <ta e="T50" id="Seg_775" s="T49">dempro-n:case</ta>
            <ta e="T51" id="Seg_776" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_777" s="T51">adv</ta>
            <ta e="T53" id="Seg_778" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_779" s="T53">que</ta>
            <ta e="T55" id="Seg_780" s="T54">n-n:ins-n:case</ta>
            <ta e="T56" id="Seg_781" s="T55">n-n:case.poss</ta>
            <ta e="T57" id="Seg_782" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_783" s="T57">v-v&gt;v-v:pn</ta>
            <ta e="T59" id="Seg_784" s="T58">v-v:mood.pn</ta>
            <ta e="T60" id="Seg_785" s="T59">n-n:case.poss</ta>
            <ta e="T61" id="Seg_786" s="T60">adv</ta>
            <ta e="T62" id="Seg_787" s="T61">n-n:case.poss</ta>
            <ta e="T63" id="Seg_788" s="T62">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_789" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_790" s="T65">adv</ta>
            <ta e="T67" id="Seg_791" s="T66">dempro-n:case</ta>
            <ta e="T68" id="Seg_792" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_793" s="T68">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_794" s="T0">num</ta>
            <ta e="T2" id="Seg_795" s="T1">n</ta>
            <ta e="T3" id="Seg_796" s="T2">n</ta>
            <ta e="T4" id="Seg_797" s="T3">v</ta>
            <ta e="T5" id="Seg_798" s="T4">dempro</ta>
            <ta e="T6" id="Seg_799" s="T5">v</ta>
            <ta e="T7" id="Seg_800" s="T6">n</ta>
            <ta e="T8" id="Seg_801" s="T7">n</ta>
            <ta e="T9" id="Seg_802" s="T8">v</ta>
            <ta e="T10" id="Seg_803" s="T9">dempro</ta>
            <ta e="T11" id="Seg_804" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_805" s="T11">v</ta>
            <ta e="T13" id="Seg_806" s="T12">v</ta>
            <ta e="T14" id="Seg_807" s="T13">v</ta>
            <ta e="T15" id="Seg_808" s="T14">conj</ta>
            <ta e="T16" id="Seg_809" s="T15">n</ta>
            <ta e="T17" id="Seg_810" s="T16">n</ta>
            <ta e="T18" id="Seg_811" s="T17">v</ta>
            <ta e="T19" id="Seg_812" s="T18">adv</ta>
            <ta e="T20" id="Seg_813" s="T19">v</ta>
            <ta e="T21" id="Seg_814" s="T20">v</ta>
            <ta e="T22" id="Seg_815" s="T21">conj</ta>
            <ta e="T23" id="Seg_816" s="T22">n</ta>
            <ta e="T24" id="Seg_817" s="T23">v</ta>
            <ta e="T25" id="Seg_818" s="T24">v</ta>
            <ta e="T26" id="Seg_819" s="T25">interj</ta>
            <ta e="T27" id="Seg_820" s="T26">n</ta>
            <ta e="T28" id="Seg_821" s="T27">v</ta>
            <ta e="T29" id="Seg_822" s="T28">adv</ta>
            <ta e="T30" id="Seg_823" s="T29">adv</ta>
            <ta e="T31" id="Seg_824" s="T30">v</ta>
            <ta e="T32" id="Seg_825" s="T31">adj</ta>
            <ta e="T33" id="Seg_826" s="T32">n</ta>
            <ta e="T34" id="Seg_827" s="T33">dempro</ta>
            <ta e="T35" id="Seg_828" s="T34">v</ta>
            <ta e="T36" id="Seg_829" s="T35">n</ta>
            <ta e="T37" id="Seg_830" s="T36">v</ta>
            <ta e="T38" id="Seg_831" s="T37">adv</ta>
            <ta e="T39" id="Seg_832" s="T38">n</ta>
            <ta e="T40" id="Seg_833" s="T39">v</ta>
            <ta e="T41" id="Seg_834" s="T40">dempro</ta>
            <ta e="T42" id="Seg_835" s="T41">n</ta>
            <ta e="T43" id="Seg_836" s="T42">v</ta>
            <ta e="T44" id="Seg_837" s="T43">n</ta>
            <ta e="T45" id="Seg_838" s="T44">n</ta>
            <ta e="T46" id="Seg_839" s="T45">n</ta>
            <ta e="T47" id="Seg_840" s="T46">v</ta>
            <ta e="T48" id="Seg_841" s="T47">n</ta>
            <ta e="T49" id="Seg_842" s="T48">v</ta>
            <ta e="T50" id="Seg_843" s="T49">dempro</ta>
            <ta e="T51" id="Seg_844" s="T50">v</ta>
            <ta e="T52" id="Seg_845" s="T51">adv</ta>
            <ta e="T53" id="Seg_846" s="T52">v</ta>
            <ta e="T54" id="Seg_847" s="T53">que</ta>
            <ta e="T55" id="Seg_848" s="T54">n</ta>
            <ta e="T56" id="Seg_849" s="T55">n</ta>
            <ta e="T57" id="Seg_850" s="T56">dempro</ta>
            <ta e="T58" id="Seg_851" s="T57">v</ta>
            <ta e="T59" id="Seg_852" s="T58">v</ta>
            <ta e="T60" id="Seg_853" s="T59">n</ta>
            <ta e="T61" id="Seg_854" s="T60">adv</ta>
            <ta e="T62" id="Seg_855" s="T61">n</ta>
            <ta e="T63" id="Seg_856" s="T62">v</ta>
            <ta e="T65" id="Seg_857" s="T64">dempro</ta>
            <ta e="T66" id="Seg_858" s="T65">adv</ta>
            <ta e="T67" id="Seg_859" s="T66">dempro</ta>
            <ta e="T68" id="Seg_860" s="T67">v</ta>
            <ta e="T69" id="Seg_861" s="T68">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_862" s="T1">np.h:Poss</ta>
            <ta e="T3" id="Seg_863" s="T2">np:Th</ta>
            <ta e="T5" id="Seg_864" s="T4">pro:A</ta>
            <ta e="T7" id="Seg_865" s="T6">np:L</ta>
            <ta e="T8" id="Seg_866" s="T7">np:Th</ta>
            <ta e="T10" id="Seg_867" s="T9">pro:A</ta>
            <ta e="T13" id="Seg_868" s="T12">0.3:A</ta>
            <ta e="T14" id="Seg_869" s="T13">0.3:A</ta>
            <ta e="T16" id="Seg_870" s="T15">np:Ins</ta>
            <ta e="T17" id="Seg_871" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_872" s="T18">adv:Time</ta>
            <ta e="T20" id="Seg_873" s="T19">0.3:A</ta>
            <ta e="T21" id="Seg_874" s="T20">0.3:A</ta>
            <ta e="T23" id="Seg_875" s="T22">np.h:E</ta>
            <ta e="T25" id="Seg_876" s="T24">0.3.h:A</ta>
            <ta e="T28" id="Seg_877" s="T27">0.3:P</ta>
            <ta e="T29" id="Seg_878" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_879" s="T29">adv:Time</ta>
            <ta e="T31" id="Seg_880" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_881" s="T32">np:G</ta>
            <ta e="T34" id="Seg_882" s="T33">pro.h:A</ta>
            <ta e="T36" id="Seg_883" s="T35">np:P</ta>
            <ta e="T37" id="Seg_884" s="T36">0.2.h:A</ta>
            <ta e="T38" id="Seg_885" s="T37">adv:Time</ta>
            <ta e="T39" id="Seg_886" s="T38">np:Th</ta>
            <ta e="T41" id="Seg_887" s="T40">pro.h:A</ta>
            <ta e="T42" id="Seg_888" s="T41">np:P</ta>
            <ta e="T44" id="Seg_889" s="T43">np:Th</ta>
            <ta e="T45" id="Seg_890" s="T44">0.3.h:A</ta>
            <ta e="T46" id="Seg_891" s="T45">np.h:A 0.3.h:Poss</ta>
            <ta e="T48" id="Seg_892" s="T47">np:P</ta>
            <ta e="T50" id="Seg_893" s="T49">pro:Th</ta>
            <ta e="T52" id="Seg_894" s="T51">adv:Time</ta>
            <ta e="T53" id="Seg_895" s="T52">0.3.h:A</ta>
            <ta e="T55" id="Seg_896" s="T54">np.h:Poss</ta>
            <ta e="T56" id="Seg_897" s="T55">np:Th</ta>
            <ta e="T57" id="Seg_898" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_899" s="T58">0.2.h:A</ta>
            <ta e="T60" id="Seg_900" s="T59">np:Th</ta>
            <ta e="T61" id="Seg_901" s="T60">adv:L</ta>
            <ta e="T62" id="Seg_902" s="T61">np:L</ta>
            <ta e="T66" id="Seg_903" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_904" s="T66">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_905" s="T2">np:S</ta>
            <ta e="T4" id="Seg_906" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_907" s="T4">pro:S</ta>
            <ta e="T6" id="Seg_908" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_909" s="T7">np:S</ta>
            <ta e="T9" id="Seg_910" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_911" s="T10">ptcl:pred</ta>
            <ta e="T13" id="Seg_912" s="T12">v:pred 0.3:S</ta>
            <ta e="T14" id="Seg_913" s="T13">v:pred 0.3:S</ta>
            <ta e="T17" id="Seg_914" s="T16">np:S</ta>
            <ta e="T18" id="Seg_915" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_916" s="T19">v:pred 0.3:S</ta>
            <ta e="T21" id="Seg_917" s="T20">v:pred 0.3:S</ta>
            <ta e="T23" id="Seg_918" s="T22">np.h:S</ta>
            <ta e="T24" id="Seg_919" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_920" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_921" s="T27">v:pred 0.3:S</ta>
            <ta e="T31" id="Seg_922" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_923" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_924" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_925" s="T35">np:O</ta>
            <ta e="T37" id="Seg_926" s="T36">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_927" s="T38">np:S</ta>
            <ta e="T40" id="Seg_928" s="T39">v:pred </ta>
            <ta e="T41" id="Seg_929" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_930" s="T41">np:O</ta>
            <ta e="T43" id="Seg_931" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_932" s="T43">np:O</ta>
            <ta e="T45" id="Seg_933" s="T44">v:pred 0.3.h:S</ta>
            <ta e="T46" id="Seg_934" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_935" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_936" s="T47">np:O</ta>
            <ta e="T49" id="Seg_937" s="T48">s:purp</ta>
            <ta e="T50" id="Seg_938" s="T49">pro:S</ta>
            <ta e="T51" id="Seg_939" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_940" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T56" id="Seg_941" s="T55">np:S</ta>
            <ta e="T57" id="Seg_942" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_943" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_944" s="T58">v:pred 0.2.h:S</ta>
            <ta e="T60" id="Seg_945" s="T59">np:S</ta>
            <ta e="T63" id="Seg_946" s="T62">v:pred</ta>
            <ta e="T67" id="Seg_947" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_948" s="T67">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_949" s="T2">TURK:cult</ta>
            <ta e="T11" id="Seg_950" s="T10">RUS:gram</ta>
            <ta e="T15" id="Seg_951" s="T14">RUS:gram</ta>
            <ta e="T22" id="Seg_952" s="T21">RUS:gram</ta>
            <ta e="T32" id="Seg_953" s="T31">TURK:core</ta>
            <ta e="T48" id="Seg_954" s="T47">TURK:cult</ta>
            <ta e="T55" id="Seg_955" s="T54">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_956" s="T0">У одного человека была корова.</ta>
            <ta e="T6" id="Seg_957" s="T4">Она пришла.</ta>
            <ta e="T9" id="Seg_958" s="T6">В котле лежала картошка.</ta>
            <ta e="T12" id="Seg_959" s="T9">Она стала ее есть.</ta>
            <ta e="T18" id="Seg_960" s="T12">Ела-ела и надела котел себе на рога.</ta>
            <ta e="T21" id="Seg_961" s="T18">Она бегает, мычит.</ta>
            <ta e="T27" id="Seg_962" s="T21">А человек увидел и говорит: "Ой, котел!</ta>
            <ta e="T29" id="Seg_963" s="T27">Он сейчас умрет!"</ta>
            <ta e="T33" id="Seg_964" s="T29">Тогда он побежал к другому человеку.</ta>
            <ta e="T40" id="Seg_965" s="T33">Тот говорит: "Отрежешь голову и тогда (получишь?) котел".</ta>
            <ta e="T45" id="Seg_966" s="T40">Он отрезал ей голову, поставил котел.</ta>
            <ta e="T49" id="Seg_967" s="T45">Его жена пришла доить корову.</ta>
            <ta e="T51" id="Seg_968" s="T49">Та лежит.</ta>
            <ta e="T56" id="Seg_969" s="T51">Тогда она говорит: "Где голова коровы?"</ta>
            <ta e="T59" id="Seg_970" s="T56">Тот говорит: "Дои ее!</ta>
            <ta e="T63" id="Seg_971" s="T59">Голова вот в котле лежит</ta>
            <ta e="T68" id="Seg_972" s="T63">Тогда она подоила [корову].</ta>
            <ta e="T69" id="Seg_973" s="T68">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_974" s="T0">One man had a cow.</ta>
            <ta e="T6" id="Seg_975" s="T4">It came.</ta>
            <ta e="T9" id="Seg_976" s="T6">There were potatoes in the cauldron.</ta>
            <ta e="T12" id="Seg_977" s="T9">It started eating them.</ta>
            <ta e="T18" id="Seg_978" s="T12">It ate, it ate, and put the cauldron on its horn.</ta>
            <ta e="T21" id="Seg_979" s="T18">Then it is running, bellowing.</ta>
            <ta e="T27" id="Seg_980" s="T21">But the man saw, said: "Oh, this cauldron!</ta>
            <ta e="T29" id="Seg_981" s="T27">It will die!"</ta>
            <ta e="T33" id="Seg_982" s="T29">Then he run to another man.</ta>
            <ta e="T40" id="Seg_983" s="T33">[That man] says: "You will cut off its head, then you will (have?) the cauldron."</ta>
            <ta e="T45" id="Seg_984" s="T40">He cut off its head, set the cauldron.</ta>
            <ta e="T49" id="Seg_985" s="T45">His wife came to milk the cow.</ta>
            <ta e="T51" id="Seg_986" s="T49">It is lying.</ta>
            <ta e="T56" id="Seg_987" s="T51">Then she says: "Where is the cow's head?"</ta>
            <ta e="T59" id="Seg_988" s="T56">He says: "Milk it!</ta>
            <ta e="T63" id="Seg_989" s="T59">Its head is lying there in the cauldron."</ta>
            <ta e="T68" id="Seg_990" s="T63">Then she milked [the cow].</ta>
            <ta e="T69" id="Seg_991" s="T68">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_992" s="T0">Ein Mann hatte eine Kuh.</ta>
            <ta e="T6" id="Seg_993" s="T4">Sie kam.</ta>
            <ta e="T9" id="Seg_994" s="T6">In einem Kessel lagen Kartoffeln. </ta>
            <ta e="T12" id="Seg_995" s="T9">Sie fing an zu fressen.</ta>
            <ta e="T18" id="Seg_996" s="T12">Sie fraß und fraß und trug den Kessel auf ihrem Horn.</ta>
            <ta e="T21" id="Seg_997" s="T18">Dann rennt sie und brüllt.</ta>
            <ta e="T27" id="Seg_998" s="T21">Aber der Mann sah es und sagte: "Oh, der Kessel!</ta>
            <ta e="T29" id="Seg_999" s="T27">Er wird sterben!"</ta>
            <ta e="T33" id="Seg_1000" s="T29">Dann rannte er zu einem anderen Mann.</ta>
            <ta e="T40" id="Seg_1001" s="T33">[Jener Mann] sagt: "Du schneidest den Kopf ab, dann wirst du den Kessel (haben?)."</ta>
            <ta e="T45" id="Seg_1002" s="T40">Er schnitt ihren Kopf ab und setzte den Kessel.</ta>
            <ta e="T49" id="Seg_1003" s="T45">Seine Frau kam um die Kuh zu melken.</ta>
            <ta e="T51" id="Seg_1004" s="T49">Sie liegt da.</ta>
            <ta e="T56" id="Seg_1005" s="T51">Dann sagt sie: "Wo ist der Kopf der Kuh?"</ta>
            <ta e="T59" id="Seg_1006" s="T56">Er sagt: "Melk sie!</ta>
            <ta e="T63" id="Seg_1007" s="T59">Ihr Kopf liegt dort im Kessel."</ta>
            <ta e="T68" id="Seg_1008" s="T63">Dann molk sie [die Kuh].</ta>
            <ta e="T69" id="Seg_1009" s="T68">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T63" id="Seg_1010" s="T59">[GVY:] aspaktən = aspakkən?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
