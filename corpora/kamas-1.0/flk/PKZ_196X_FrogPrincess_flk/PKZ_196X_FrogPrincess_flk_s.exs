<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4A796B6C-ABB7-0314-4FE6-A2FBA4834447">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_FrogPrincess_flk</transcription-name>
         <referenced-file url="PKZ_196X_FrogPrincess_flk.wav" />
         <referenced-file url="PKZ_196X_FrogPrincess_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FrogPrincess_flk\PKZ_196X_FrogPrincess_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">543</ud-information>
            <ud-information attribute-name="# HIAT:w">360</ud-information>
            <ud-information attribute-name="# e">364</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">90</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.715" type="appl" />
         <tli id="T2" time="1.431" type="appl" />
         <tli id="T3" time="2.146" type="appl" />
         <tli id="T4" time="2.807" type="appl" />
         <tli id="T5" time="3.468" type="appl" />
         <tli id="T6" time="4.129" type="appl" />
         <tli id="T7" time="5.066634581058379" />
         <tli id="T8" time="5.989" type="appl" />
         <tli id="T9" time="6.801" type="appl" />
         <tli id="T10" time="7.613" type="appl" />
         <tli id="T11" time="8.719944778979421" />
         <tli id="T12" time="9.564" type="appl" />
         <tli id="T13" time="10.36" type="appl" />
         <tli id="T14" time="11.155" type="appl" />
         <tli id="T15" time="11.951" type="appl" />
         <tli id="T16" time="12.556" type="appl" />
         <tli id="T17" time="13.145" type="appl" />
         <tli id="T18" time="13.734" type="appl" />
         <tli id="T19" time="14.322" type="appl" />
         <tli id="T20" time="14.911" type="appl" />
         <tli id="T21" time="16.59322825296619" />
         <tli id="T22" time="17.539" type="appl" />
         <tli id="T23" time="18.384" type="appl" />
         <tli id="T24" time="19.229" type="appl" />
         <tli id="T25" time="19.755" type="appl" />
         <tli id="T26" time="20.282" type="appl" />
         <tli id="T27" time="20.808" type="appl" />
         <tli id="T28" time="21.335" type="appl" />
         <tli id="T29" time="22.45319114342713" />
         <tli id="T30" time="23.513" type="appl" />
         <tli id="T31" time="24.535" type="appl" />
         <tli id="T32" time="25.557" type="appl" />
         <tli id="T33" time="27.519825724485507" />
         <tli id="T34" time="29.324" type="appl" />
         <tli id="T35" time="31.066469931226376" />
         <tli id="T36" time="31.809" type="appl" />
         <tli id="T37" time="32.575" type="appl" />
         <tli id="T38" time="33.341" type="appl" />
         <tli id="T39" time="34.108" type="appl" />
         <tli id="T40" time="35.604" type="appl" />
         <tli id="T41" time="37.54642889542209" />
         <tli id="T42" time="38.54" type="appl" />
         <tli id="T43" time="39.482" type="appl" />
         <tli id="T44" time="40.425" type="appl" />
         <tli id="T45" time="41.368" type="appl" />
         <tli id="T46" time="42.142" type="appl" />
         <tli id="T47" time="43.65305689048719" />
         <tli id="T48" time="46.037" type="appl" />
         <tli id="T49" time="48.282" type="appl" />
         <tli id="T363" time="49.01282244300947" type="intp" />
         <tli id="T50" time="49.92635049677131" />
         <tli id="T364" time="50.646345937237506" type="intp" />
         <tli id="T51" time="51.446340871088836" />
         <tli id="T52" time="52.77966576084104" />
         <tli id="T53" time="53.63" type="appl" />
         <tli id="T54" time="54.487" type="appl" />
         <tli id="T55" time="56.40630946096703" />
         <tli id="T56" time="57.555" type="appl" />
         <tli id="T359" time="57.967285714285715" type="intp" />
         <tli id="T57" time="58.517" type="appl" />
         <tli id="T58" time="59.078" type="appl" />
         <tli id="T59" time="59.639" type="appl" />
         <tli id="T60" time="60.199" type="appl" />
         <tli id="T61" time="60.76" type="appl" />
         <tli id="T62" time="61.321" type="appl" />
         <tli id="T63" time="62.338" type="appl" />
         <tli id="T64" time="63.356" type="appl" />
         <tli id="T65" time="64.90625563313733" />
         <tli id="T66" time="65.543" type="appl" />
         <tli id="T67" time="66.223" type="appl" />
         <tli id="T68" time="66.902" type="appl" />
         <tli id="T69" time="67.582" type="appl" />
         <tli id="T70" time="68.83956405790634" />
         <tli id="T71" time="69.585" type="appl" />
         <tli id="T72" time="70.222" type="appl" />
         <tli id="T73" time="70.86" type="appl" />
         <tli id="T74" time="71.497" type="appl" />
         <tli id="T75" time="72.135" type="appl" />
         <tli id="T76" time="72.521" type="appl" />
         <tli id="T77" time="72.906" type="appl" />
         <tli id="T78" time="73.292" type="appl" />
         <tli id="T79" time="73.91286526341348" />
         <tli id="T80" time="74.827" type="appl" />
         <tli id="T81" time="75.617" type="appl" />
         <tli id="T82" time="76.406" type="appl" />
         <tli id="T83" time="76.842" type="appl" />
         <tli id="T84" time="77.279" type="appl" />
         <tli id="T85" time="77.715" type="appl" />
         <tli id="T86" time="78.152" type="appl" />
         <tli id="T87" time="79.20616507572973" />
         <tli id="T88" time="79.953" type="appl" />
         <tli id="T89" time="80.492" type="appl" />
         <tli id="T90" time="81.031" type="appl" />
         <tli id="T91" time="81.559" type="appl" />
         <tli id="T92" time="82.088" type="appl" />
         <tli id="T93" time="82.616" type="appl" />
         <tli id="T94" time="83.144" type="appl" />
         <tli id="T95" time="83.672" type="appl" />
         <tli id="T96" time="84.201" type="appl" />
         <tli id="T97" time="84.729" type="appl" />
         <tli id="T98" time="85.178" type="appl" />
         <tli id="T99" time="85.628" type="appl" />
         <tli id="T100" time="86.077" type="appl" />
         <tli id="T101" time="86.934" type="appl" />
         <tli id="T102" time="87.792" type="appl" />
         <tli id="T103" time="89.184" type="appl" />
         <tli id="T104" time="90.576" type="appl" />
         <tli id="T105" time="91.968" type="appl" />
         <tli id="T106" time="93.36" type="appl" />
         <tli id="T107" time="94.92606552590823" />
         <tli id="T108" time="96.119" type="appl" />
         <tli id="T109" time="97.207" type="appl" />
         <tli id="T110" time="98.295" type="appl" />
         <tli id="T111" time="99.73936837791368" />
         <tli id="T112" time="100.537" type="appl" />
         <tli id="T113" time="101.364" type="appl" />
         <tli id="T114" time="102.191" type="appl" />
         <tli id="T115" time="103.018" type="appl" />
         <tli id="T116" time="105.7059972595548" />
         <tli id="T117" time="106.881" type="appl" />
         <tli id="T118" time="107.846" type="appl" />
         <tli id="T119" time="108.81" type="appl" />
         <tli id="T120" time="109.774" type="appl" />
         <tli id="T121" time="110.356" type="appl" />
         <tli id="T122" time="110.938" type="appl" />
         <tli id="T123" time="111.519" type="appl" />
         <tli id="T124" time="112.101" type="appl" />
         <tli id="T125" time="113.5926139824391" />
         <tli id="T126" time="114.333" type="appl" />
         <tli id="T127" time="114.904" type="appl" />
         <tli id="T128" time="115.475" type="appl" />
         <tli id="T129" time="116.046" type="appl" />
         <tli id="T130" time="116.617" type="appl" />
         <tli id="T131" time="117.228" type="appl" />
         <tli id="T132" time="117.839" type="appl" />
         <tli id="T133" time="118.45" type="appl" />
         <tli id="T134" time="119.062" type="appl" />
         <tli id="T135" time="119.673" type="appl" />
         <tli id="T136" time="120.284" type="appl" />
         <tli id="T137" time="121.41923108528454" />
         <tli id="T138" time="122.318" type="appl" />
         <tli id="T139" time="123.012" type="appl" />
         <tli id="T140" time="123.66499811345741" />
         <tli id="T141" time="124.331" type="appl" />
         <tli id="T142" time="124.956" type="appl" />
         <tli id="T143" time="125.582" type="appl" />
         <tli id="T360" time="125.8502857142857" type="intp" />
         <tli id="T144" time="126.208" type="appl" />
         <tli id="T145" time="127.188" type="appl" />
         <tli id="T146" time="128.6458519877415" />
         <tli id="T147" time="129.038" type="appl" />
         <tli id="T148" time="130.075" type="appl" />
         <tli id="T149" time="131.112" type="appl" />
         <tli id="T150" time="133.16582336400145" />
         <tli id="T151" time="134.288" type="appl" />
         <tli id="T152" time="135.21" type="appl" />
         <tli id="T153" time="136.131" type="appl" />
         <tli id="T154" time="137.51246250459366" />
         <tli id="T155" time="138.432" type="appl" />
         <tli id="T156" time="139.182" type="appl" />
         <tli id="T157" time="139.931" type="appl" />
         <tli id="T158" time="140.681" type="appl" />
         <tli id="T159" time="141.307" type="appl" />
         <tli id="T160" time="141.933" type="appl" />
         <tli id="T161" time="142.91242830809009" />
         <tli id="T162" time="143.952" type="appl" />
         <tli id="T163" time="145.127" type="appl" />
         <tli id="T164" time="146.78573711282024" />
         <tli id="T165" time="147.67" type="appl" />
         <tli id="T166" time="148.525" type="appl" />
         <tli id="T167" time="149.38" type="appl" />
         <tli id="T168" time="151.20570912234882" />
         <tli id="T169" time="151.973" type="appl" />
         <tli id="T170" time="152.476" type="appl" />
         <tli id="T171" time="152.979" type="appl" />
         <tli id="T172" time="153.82" type="appl" />
         <tli id="T173" time="154.662" type="appl" />
         <tli id="T174" time="155.584" type="appl" />
         <tli id="T175" time="156.504" type="appl" />
         <tli id="T176" time="157.423" type="appl" />
         <tli id="T177" time="158.343" type="appl" />
         <tli id="T178" time="159.263" type="appl" />
         <tli id="T179" time="160.309" type="appl" />
         <tli id="T180" time="161.11" type="appl" />
         <tli id="T181" time="161.91" type="appl" />
         <tli id="T182" time="162.711" type="appl" />
         <tli id="T183" time="163.512" type="appl" />
         <tli id="T184" time="164.312" type="appl" />
         <tli id="T185" time="165.112" type="appl" />
         <tli id="T186" time="165.913" type="appl" />
         <tli id="T187" time="166.52" type="appl" />
         <tli id="T188" time="167.128" type="appl" />
         <tli id="T189" time="167.735" type="appl" />
         <tli id="T190" time="168.343" type="appl" />
         <tli id="T191" time="168.95" type="appl" />
         <tli id="T192" time="169.558" type="appl" />
         <tli id="T193" time="170.165" type="appl" />
         <tli id="T194" time="170.773" type="appl" />
         <tli id="T195" time="171.38" type="appl" />
         <tli id="T196" time="171.784" type="appl" />
         <tli id="T197" time="172.188" type="appl" />
         <tli id="T198" time="172.591" type="appl" />
         <tli id="T199" time="172.995" type="appl" />
         <tli id="T200" time="174.403" type="appl" />
         <tli id="T201" time="175.81" type="appl" />
         <tli id="T202" time="177.218" type="appl" />
         <tli id="T203" time="178.626" type="appl" />
         <tli id="T204" time="180.034" type="appl" />
         <tli id="T205" time="181.442" type="appl" />
         <tli id="T206" time="182.849" type="appl" />
         <tli id="T207" time="185.3988259200441" />
         <tli id="T208" time="187.089" type="appl" />
         <tli id="T209" time="188.394" type="appl" />
         <tli id="T210" time="189.475" type="appl" />
         <tli id="T211" time="190.556" type="appl" />
         <tli id="T212" time="191.637" type="appl" />
         <tli id="T213" time="192.718" type="appl" />
         <tli id="T214" time="193.799" type="appl" />
         <tli id="T215" time="194.721" type="appl" />
         <tli id="T216" time="195.619" type="appl" />
         <tli id="T217" time="196.411" type="appl" />
         <tli id="T218" time="197.202" type="appl" />
         <tli id="T219" time="197.994" type="appl" />
         <tli id="T220" time="199.27873802236454" />
         <tli id="T221" time="200.248" type="appl" />
         <tli id="T222" time="200.996" type="appl" />
         <tli id="T223" time="201.744" type="appl" />
         <tli id="T224" time="203.22537969603107" />
         <tli id="T225" time="204.399" type="appl" />
         <tli id="T226" time="205.33869964628832" />
         <tli id="T227" time="206.248" type="appl" />
         <tli id="T228" time="207.06" type="appl" />
         <tli id="T229" time="207.872" type="appl" />
         <tli id="T230" time="208.904" type="appl" />
         <tli id="T231" time="209.936" type="appl" />
         <tli id="T232" time="210.968" type="appl" />
         <tli id="T233" time="212.001" type="appl" />
         <tli id="T234" time="213.033" type="appl" />
         <tli id="T235" time="214.065" type="appl" />
         <tli id="T236" time="215.097" type="appl" />
         <tli id="T237" time="215.745" type="appl" />
         <tli id="T238" time="216.394" type="appl" />
         <tli id="T239" time="217.042" type="appl" />
         <tli id="T240" time="217.691" type="appl" />
         <tli id="T241" time="218.339" type="appl" />
         <tli id="T242" time="218.988" type="appl" />
         <tli id="T243" time="219.636" type="appl" />
         <tli id="T244" time="220.285" type="appl" />
         <tli id="T245" time="220.933" type="appl" />
         <tli id="T246" time="221.443" type="appl" />
         <tli id="T247" time="221.953" type="appl" />
         <tli id="T248" time="222.464" type="appl" />
         <tli id="T249" time="222.974" type="appl" />
         <tli id="T250" time="223.63858375813734" />
         <tli id="T251" time="224.917" type="appl" />
         <tli id="T252" time="226.05" type="appl" />
         <tli id="T253" time="227.33189370275096" />
         <tli id="T254" time="227.8" type="appl" />
         <tli id="T255" time="228.417" type="appl" />
         <tli id="T256" time="229.38521403296934" />
         <tli id="T257" time="230.403" type="appl" />
         <tli id="T258" time="231.45" type="appl" />
         <tli id="T259" time="232.498" type="appl" />
         <tli id="T260" time="233.545" type="appl" />
         <tli id="T261" time="234.445" type="appl" />
         <tli id="T262" time="235.145" type="appl" />
         <tli id="T263" time="235.846" type="appl" />
         <tli id="T264" time="236.37" type="appl" />
         <tli id="T265" time="236.894" type="appl" />
         <tli id="T266" time="237.62516185163796" />
         <tli id="T267" time="238.692" type="appl" />
         <tli id="T268" time="240.22514538665476" />
         <tli id="T269" time="241.294" type="appl" />
         <tli id="T270" time="242.42513145474592" />
         <tli id="T271" time="243.801" type="appl" />
         <tli id="T272" time="245.4851120767272" />
         <tli id="T273" time="246.211" type="appl" />
         <tli id="T274" time="246.902" type="appl" />
         <tli id="T275" time="248.09842886064152" />
         <tli id="T276" time="248.546" type="appl" />
         <tli id="T277" time="249.307" type="appl" />
         <tli id="T278" time="250.788" type="appl" />
         <tli id="T279" time="252.27" type="appl" />
         <tli id="T280" time="253.752" type="appl" />
         <tli id="T281" time="255.233" type="appl" />
         <tli id="T282" time="256.715" type="appl" />
         <tli id="T283" time="258.196" type="appl" />
         <tli id="T284" time="258.879" type="appl" />
         <tli id="T285" time="259.562" type="appl" />
         <tli id="T286" time="260.157" type="appl" />
         <tli id="T287" time="260.752" type="appl" />
         <tli id="T288" time="261.52501050044623" />
         <tli id="T289" time="262.482" type="appl" />
         <tli id="T290" time="263.315" type="appl" />
         <tli id="T291" time="264.147" type="appl" />
         <tli id="T292" time="265.009" type="appl" />
         <tli id="T293" time="265.87" type="appl" />
         <tli id="T294" time="267.5249725043312" />
         <tli id="T295" time="268.416" type="appl" />
         <tli id="T296" time="268.999" type="appl" />
         <tli id="T297" time="269.583" type="appl" />
         <tli id="T298" time="270.869" type="appl" />
         <tli id="T299" time="272.156" type="appl" />
         <tli id="T300" time="273.442" type="appl" />
         <tli id="T301" time="274.728" type="appl" />
         <tli id="T302" time="276.015" type="appl" />
         <tli id="T303" time="277.301" type="appl" />
         <tli id="T304" time="277.9696177302079" type="intp" />
         <tli id="T305" time="278.6382354604158" />
         <tli id="T306" time="278.8911177302079" type="intp" />
         <tli id="T307" time="279.144" type="appl" />
         <tli id="T308" time="280.066" type="appl" />
         <tli id="T309" time="280.987" type="appl" />
         <tli id="T310" time="281.908" type="appl" />
         <tli id="T311" time="283.0515408454956" />
         <tli id="T312" time="283.949" type="appl" />
         <tli id="T313" time="284.737" type="appl" />
         <tli id="T314" time="285.526" type="appl" />
         <tli id="T315" time="286.315" type="appl" />
         <tli id="T316" time="287.104" type="appl" />
         <tli id="T317" time="287.892" type="appl" />
         <tli id="T318" time="288.681" type="appl" />
         <tli id="T319" time="291.7781522489238" />
         <tli id="T320" time="292.536" type="appl" />
         <tli id="T321" time="293.212" type="appl" />
         <tli id="T322" time="293.888" type="appl" />
         <tli id="T323" time="295.03813160436795" />
         <tli id="T324" time="296.106" type="appl" />
         <tli id="T325" time="297.0" type="appl" />
         <tli id="T326" time="297.894" type="appl" />
         <tli id="T327" time="298.788" type="appl" />
         <tli id="T328" time="299.682" type="appl" />
         <tli id="T329" time="300.576" type="appl" />
         <tli id="T330" time="301.47" type="appl" />
         <tli id="T331" time="302.132" type="appl" />
         <tli id="T332" time="302.795" type="appl" />
         <tli id="T333" time="303.457" type="appl" />
         <tli id="T334" time="304.051" type="appl" />
         <tli id="T335" time="304.644" type="appl" />
         <tli id="T336" time="305.238" type="appl" />
         <tli id="T337" time="305.832" type="appl" />
         <tli id="T338" time="306.727" type="appl" />
         <tli id="T339" time="307.299" type="appl" />
         <tli id="T340" time="307.87" type="appl" />
         <tli id="T341" time="308.442" type="appl" />
         <tli id="T342" time="309.013" type="appl" />
         <tli id="T343" time="309.641" type="appl" />
         <tli id="T344" time="310.269" type="appl" />
         <tli id="T361" time="310.6032786103528" type="intp" />
         <tli id="T345" time="311.104696525882" />
         <tli id="T346" time="311.69" type="appl" />
         <tli id="T347" time="312.226" type="appl" />
         <tli id="T348" time="312.761" type="appl" />
         <tli id="T349" time="313.3513489651144" />
         <tli id="T350" time="314.097" type="appl" />
         <tli id="T362" time="314.3721428571428" type="intp" />
         <tli id="T351" time="314.739" type="appl" />
         <tli id="T352" time="315.128" type="appl" />
         <tli id="T353" time="315.518" type="appl" />
         <tli id="T354" time="315.907" type="appl" />
         <tli id="T355" time="316.296" type="appl" />
         <tli id="T356" time="316.685" type="appl" />
         <tli id="T357" time="317.075" type="appl" />
         <tli id="T358" time="317.464" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T358" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">koŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">nagur</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nʼizeŋdə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ibiʔi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Dĭgəttə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">dĭ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">măndə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">nʼizeŋdə:</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Nada</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">šiʔnʼileʔ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">monoʔkosʼtə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">nezeŋ</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Štobɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">măna</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">kuzittə</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_66" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">š-</ts>
                  <nts id="Seg_69" n="HIAT:ip">)</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">šiʔnʼileʔ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">esseŋ</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_79" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">No</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">dĭzeŋ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">măndəʔi:</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">Nu</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">monoʔkoʔ</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">šindi</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">tănan</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">kereʔ</ts>
                  <nts id="Seg_105" n="HIAT:ip">?</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_108" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">Igeʔ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">multukʔi</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">i</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">kangaʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip">!</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_123" n="HIAT:u" s="T33">
                  <nts id="Seg_124" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">Dʼiʔtəgeʔ</ts>
                  <nts id="Seg_127" n="HIAT:ip">)</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">strʼelaʔi</ts>
                  <nts id="Seg_131" n="HIAT:ip">!</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_134" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">Dĭgəttə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">diʔnə=</ts>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">dĭzeŋ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">kambiʔi</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_151" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">Onʼiʔ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">dʼiʔtəbi</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_160" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">Saʔməluʔpi</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">kup-</ts>
                  <nts id="Seg_167" n="HIAT:ip">)</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">kupʼestə</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_174" n="HIAT:w" s="T44">turanə</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_178" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">Onʼiʔ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">dʼiʔtəbi</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_187" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_189" n="HIAT:w" s="T47">Saʔməluʔpi</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_191" n="HIAT:ip">(</nts>
                  <nts id="Seg_192" n="HIAT:ip">(</nts>
                  <ats e="T49" id="Seg_193" n="HIAT:non-pho" s="T48">PAUSE</ats>
                  <nts id="Seg_194" n="HIAT:ip">)</nts>
                  <nts id="Seg_195" n="HIAT:ip">)</nts>
                  <nts id="Seg_196" n="HIAT:ip">…</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_199" n="HIAT:u" s="T49">
                  <nts id="Seg_200" n="HIAT:ip">(</nts>
                  <nts id="Seg_201" n="HIAT:ip">(</nts>
                  <ats e="T363" id="Seg_202" n="HIAT:non-pho" s="T49">KA:</ats>
                  <nts id="Seg_203" n="HIAT:ip">)</nts>
                  <nts id="Seg_204" n="HIAT:ip">)</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_207" n="HIAT:w" s="T363">Боярский</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_211" n="HIAT:u" s="T50">
                  <nts id="Seg_212" n="HIAT:ip">(</nts>
                  <nts id="Seg_213" n="HIAT:ip">(</nts>
                  <ats e="T364" id="Seg_214" n="HIAT:non-pho" s="T50">PKZ:</ats>
                  <nts id="Seg_215" n="HIAT:ip">)</nts>
                  <nts id="Seg_216" n="HIAT:ip">)</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_219" n="HIAT:w" s="T364">băjarskəj</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_222" n="HIAT:w" s="T51">dvărestə</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_226" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_228" n="HIAT:w" s="T52">A</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_231" n="HIAT:w" s="T53">onʼiʔ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_234" n="HIAT:w" s="T54">dʼiʔpi</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_238" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_240" n="HIAT:w" s="T55">Kuŋgeŋ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_243" n="HIAT:w" s="T56">kalla</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_246" n="HIAT:w" s="T359">dʼürbi</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_250" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_252" n="HIAT:w" s="T57">Dĭgəttə</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_255" n="HIAT:w" s="T58">dĭ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_258" n="HIAT:w" s="T59">kambi</ts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_262" n="HIAT:w" s="T60">kambi</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_266" n="HIAT:w" s="T61">šonəga</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_270" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_272" n="HIAT:w" s="T62">Bălotăgən</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_275" n="HIAT:w" s="T63">amnolaʔbə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_278" n="HIAT:w" s="T64">lʼaguškadə</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_282" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_284" n="HIAT:w" s="T65">I</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_287" n="HIAT:w" s="T66">strʼela</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_289" n="HIAT:ip">(</nts>
                  <ts e="T68" id="Seg_291" n="HIAT:w" s="T67">dĭʔnə=</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_294" n="HIAT:w" s="T68">u-</ts>
                  <nts id="Seg_295" n="HIAT:ip">)</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_298" n="HIAT:w" s="T69">dĭʔən</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_302" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_304" n="HIAT:w" s="T70">Dĭ</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_307" n="HIAT:w" s="T71">măndə:</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_310" n="HIAT:w" s="T72">Deʔ</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_313" n="HIAT:w" s="T73">măna</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_316" n="HIAT:w" s="T74">strʼela</ts>
                  <nts id="Seg_317" n="HIAT:ip">!</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_320" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_322" n="HIAT:w" s="T75">A</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_325" n="HIAT:w" s="T76">tăn</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip">—</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_330" n="HIAT:w" s="T77">dĭ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_333" n="HIAT:w" s="T78">măndə</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_337" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_339" n="HIAT:w" s="T79">Iʔ</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_342" n="HIAT:w" s="T80">măna</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_345" n="HIAT:w" s="T81">tibinə</ts>
                  <nts id="Seg_346" n="HIAT:ip">!</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_349" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_351" n="HIAT:w" s="T82">Da</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_354" n="HIAT:w" s="T83">kăde</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_357" n="HIAT:w" s="T84">măn</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_360" n="HIAT:w" s="T85">tănan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_363" n="HIAT:w" s="T86">ilem</ts>
                  <nts id="Seg_364" n="HIAT:ip">?</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_367" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_369" n="HIAT:w" s="T87">Tăn</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_372" n="HIAT:w" s="T88">vedʼ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_375" n="HIAT:w" s="T89">lʼaguška</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_379" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_381" n="HIAT:w" s="T90">Nu</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_384" n="HIAT:w" s="T91">dăk</ts>
                  <nts id="Seg_385" n="HIAT:ip">,</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_387" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_389" n="HIAT:w" s="T92">tăn=</ts>
                  <nts id="Seg_390" n="HIAT:ip">)</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_393" n="HIAT:w" s="T93">tăn</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_396" n="HIAT:w" s="T94">sudʼbal</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_400" n="HIAT:w" s="T95">it</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_403" n="HIAT:w" s="T96">măna</ts>
                  <nts id="Seg_404" n="HIAT:ip">!</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_407" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_409" n="HIAT:w" s="T97">Dĭgəttə</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_412" n="HIAT:w" s="T98">dĭ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_415" n="HIAT:w" s="T99">ibi</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_419" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_421" n="HIAT:w" s="T100">Maːndə</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_424" n="HIAT:w" s="T101">deʔpi</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_428" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_430" n="HIAT:w" s="T102">Dĭgəttə</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_432" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_434" n="HIAT:w" s="T103">abat-</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_437" n="HIAT:w" s="T104">al-</ts>
                  <nts id="Seg_438" n="HIAT:ip">)</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_441" n="HIAT:w" s="T105">abi</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_444" n="HIAT:w" s="T106">svadʼbaʔi</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_448" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_450" n="HIAT:w" s="T107">Dĭgəttə</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_453" n="HIAT:w" s="T108">mămbi:</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_456" n="HIAT:w" s="T109">šödörjoʔ</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_459" n="HIAT:w" s="T110">kujnegəʔi</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_463" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_465" n="HIAT:w" s="T111">Dĭgəttə</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_467" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_469" n="HIAT:w" s="T112">šonə-</ts>
                  <nts id="Seg_470" n="HIAT:ip">)</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">šobi</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_476" n="HIAT:w" s="T114">Vanʼuška</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_479" n="HIAT:w" s="T115">maːndə</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_483" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_485" n="HIAT:w" s="T116">I</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_488" n="HIAT:w" s="T117">ĭmbidə</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_491" n="HIAT:w" s="T118">ej</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_494" n="HIAT:w" s="T119">dʼăbaktəriam</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_498" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_500" n="HIAT:w" s="T120">Ĭmbi</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_503" n="HIAT:w" s="T121">tăn</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_506" n="HIAT:w" s="T122">ĭmbidə</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_509" n="HIAT:w" s="T123">ej</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_512" n="HIAT:w" s="T124">dʼăbaktəria</ts>
                  <nts id="Seg_513" n="HIAT:ip">?</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_516" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_518" n="HIAT:w" s="T125">Da</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_521" n="HIAT:w" s="T126">abam</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_524" n="HIAT:w" s="T127">mămbi</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_527" n="HIAT:w" s="T128">kujnek</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_530" n="HIAT:w" s="T129">šöʔsittə</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_534" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_536" n="HIAT:w" s="T130">Iʔbeʔ</ts>
                  <nts id="Seg_537" n="HIAT:ip">,</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_541" n="HIAT:w" s="T131">kunoluʔ-</ts>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_545" n="HIAT:w" s="T132">kunolaʔ</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_549" n="HIAT:w" s="T133">a</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_551" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_553" n="HIAT:w" s="T134">e-</ts>
                  <nts id="Seg_554" n="HIAT:ip">)</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_557" n="HIAT:w" s="T135">kujnek</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_560" n="HIAT:w" s="T136">moləj</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_564" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_566" n="HIAT:w" s="T137">Dĭgəttə</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_569" n="HIAT:w" s="T138">dĭ</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_572" n="HIAT:w" s="T139">kunolluʔpi</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_576" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_578" n="HIAT:w" s="T140">A</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_581" n="HIAT:w" s="T141">dĭ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_584" n="HIAT:w" s="T142">nʼiʔdə</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_587" n="HIAT:w" s="T143">kalla</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_590" n="HIAT:w" s="T360">dʼürbi</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_594" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_596" n="HIAT:w" s="T144">Kubat</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_599" n="HIAT:w" s="T145">kürbi</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_603" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_605" n="HIAT:w" s="T146">Davaj</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_608" n="HIAT:w" s="T147">kirgarzittə:</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_611" n="HIAT:w" s="T148">Šöʔkeʔ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_614" n="HIAT:w" s="T149">măna</ts>
                  <nts id="Seg_615" n="HIAT:ip">!</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_618" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_620" n="HIAT:w" s="T150">Kujnek</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_623" n="HIAT:w" s="T151">abanə</ts>
                  <nts id="Seg_624" n="HIAT:ip">,</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_627" n="HIAT:w" s="T152">kuvas</ts>
                  <nts id="Seg_628" n="HIAT:ip">,</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_631" n="HIAT:w" s="T153">jakšə</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_635" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_637" n="HIAT:w" s="T154">Dĭgəttə</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_640" n="HIAT:w" s="T155">bazoʔ</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_643" n="HIAT:w" s="T156">kubabə</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_646" n="HIAT:w" s="T157">šerbi</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_650" n="HIAT:u" s="T158">
                  <nts id="Seg_651" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_653" n="HIAT:w" s="T158">Tʼuna-</ts>
                  <nts id="Seg_654" n="HIAT:ip">)</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_657" n="HIAT:w" s="T159">Turanə</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_660" n="HIAT:w" s="T160">šobi</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_664" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_666" n="HIAT:w" s="T161">Kujnektə</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_669" n="HIAT:w" s="T162">embi</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_672" n="HIAT:w" s="T163">stolbə</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_676" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_678" n="HIAT:w" s="T164">Stolbə</ts>
                  <nts id="Seg_679" n="HIAT:ip">,</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_682" n="HIAT:w" s="T165">dĭgəttə</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_685" n="HIAT:w" s="T166">uʔbdəbi</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_688" n="HIAT:w" s="T167">tibit</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_692" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_694" n="HIAT:w" s="T168">No</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_698" n="HIAT:w" s="T169">it</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_701" n="HIAT:w" s="T170">kujnek</ts>
                  <nts id="Seg_702" n="HIAT:ip">!</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_705" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_707" n="HIAT:w" s="T171">Kundə</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_710" n="HIAT:w" s="T172">abandə</ts>
                  <nts id="Seg_711" n="HIAT:ip">!</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_714" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_716" n="HIAT:w" s="T173">Dĭ</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_718" n="HIAT:ip">(</nts>
                  <ts e="T175" id="Seg_720" n="HIAT:w" s="T174">kun-</ts>
                  <nts id="Seg_721" n="HIAT:ip">)</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_724" n="HIAT:w" s="T175">kambi</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_728" n="HIAT:w" s="T176">abat</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_731" n="HIAT:w" s="T177">ibi</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_735" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_737" n="HIAT:w" s="T178">Staršij</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_740" n="HIAT:w" s="T179">nʼində</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_743" n="HIAT:w" s="T180">kujnek</ts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_747" n="HIAT:w" s="T181">dĭ</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_750" n="HIAT:w" s="T182">tolʼkă</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_753" n="HIAT:w" s="T183">togonorzittə</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_756" n="HIAT:w" s="T184">dĭ</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_759" n="HIAT:w" s="T185">kujnektən</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_763" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_765" n="HIAT:w" s="T186">A</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_768" n="HIAT:w" s="T187">baška</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_771" n="HIAT:w" s="T188">nʼin</ts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_775" n="HIAT:w" s="T189">a</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_778" n="HIAT:w" s="T190">dö</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_781" n="HIAT:w" s="T191">kujnek</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_784" n="HIAT:w" s="T192">tolʼkă</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_787" n="HIAT:w" s="T193">multʼanə</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_790" n="HIAT:w" s="T194">mĭzittə</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_794" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_796" n="HIAT:w" s="T195">A</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_799" n="HIAT:w" s="T196">dĭ</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_802" n="HIAT:w" s="T197">nʼi</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_805" n="HIAT:w" s="T198">mĭbi</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_809" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_811" n="HIAT:w" s="T199">A</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_813" n="HIAT:ip">(</nts>
                  <ts e="T201" id="Seg_815" n="HIAT:w" s="T200">dö=</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_818" n="HIAT:w" s="T201">dö=</ts>
                  <nts id="Seg_819" n="HIAT:ip">)</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_822" n="HIAT:w" s="T202">dĭ</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_825" n="HIAT:w" s="T203">kujnek</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_828" n="HIAT:w" s="T204">tolʼkă</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_830" n="HIAT:ip">(</nts>
                  <ts e="T206" id="Seg_832" n="HIAT:w" s="T205">sarskən</ts>
                  <nts id="Seg_833" n="HIAT:ip">)</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_836" n="HIAT:w" s="T206">amnozittə</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_840" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_842" n="HIAT:w" s="T207">Kabarləj</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_846" n="HIAT:u" s="T208">
                  <nts id="Seg_847" n="HIAT:ip">(</nts>
                  <nts id="Seg_848" n="HIAT:ip">(</nts>
                  <ats e="T209" id="Seg_849" n="HIAT:non-pho" s="T208">DMG</ats>
                  <nts id="Seg_850" n="HIAT:ip">)</nts>
                  <nts id="Seg_851" n="HIAT:ip">)</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_854" n="HIAT:w" s="T209">Dĭn</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_857" n="HIAT:w" s="T210">abat</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_860" n="HIAT:w" s="T211">ildə</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_863" n="HIAT:w" s="T212">iʔgö</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_866" n="HIAT:w" s="T213">oʔbdəbi</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_870" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_872" n="HIAT:w" s="T214">Stoldə</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_875" n="HIAT:w" s="T215">uʔbdəbi</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_879" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_881" n="HIAT:w" s="T216">Kăštəbi</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_884" n="HIAT:w" s="T217">nʼizeŋ</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_888" n="HIAT:w" s="T218">i</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_891" n="HIAT:w" s="T219">nezeŋ</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_895" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_897" n="HIAT:w" s="T220">Dĭgəttə</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_900" n="HIAT:w" s="T221">šobiʔi</ts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_904" n="HIAT:w" s="T222">amnobiʔi</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_907" n="HIAT:w" s="T223">stoldə</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_911" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_913" n="HIAT:w" s="T224">Uja</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_916" n="HIAT:w" s="T225">ambiʔi</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_920" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_922" n="HIAT:w" s="T226">I</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_925" n="HIAT:w" s="T227">leʔi</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_927" n="HIAT:ip">(</nts>
                  <ts e="T229" id="Seg_929" n="HIAT:w" s="T228">nda</ts>
                  <nts id="Seg_930" n="HIAT:ip">)</nts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_934" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_936" n="HIAT:w" s="T229">Dĭgəttə</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_939" n="HIAT:w" s="T230">šobiʔi</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_942" n="HIAT:w" s="T231">dĭn</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_945" n="HIAT:w" s="T232">kagazaŋdə</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_949" n="HIAT:w" s="T233">mălliaʔi:</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_952" n="HIAT:w" s="T234">Šindidə</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_955" n="HIAT:w" s="T235">šonəga</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_959" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_961" n="HIAT:w" s="T236">A</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_963" n="HIAT:ip">(</nts>
                  <ts e="T238" id="Seg_965" n="HIAT:w" s="T237">dĭ=</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_968" n="HIAT:w" s="T238">m-</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_971" n="HIAT:w" s="T239">dĭm=</ts>
                  <nts id="Seg_972" n="HIAT:ip">)</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_975" n="HIAT:w" s="T240">dĭ</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_978" n="HIAT:w" s="T241">măndə:</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_981" n="HIAT:w" s="T242">Măn</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_984" n="HIAT:w" s="T243">lʼaguškam</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_987" n="HIAT:w" s="T244">šonəga</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_991" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_993" n="HIAT:w" s="T245">Dĭgəttə</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_996" n="HIAT:w" s="T246">dĭ</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_999" n="HIAT:w" s="T247">šobi</ts>
                  <nts id="Seg_1000" n="HIAT:ip">,</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1003" n="HIAT:w" s="T248">kuvas</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1006" n="HIAT:w" s="T249">bar</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_1010" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_1012" n="HIAT:w" s="T250">Dĭgəttə</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1015" n="HIAT:w" s="T251">dĭzeŋ</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1018" n="HIAT:w" s="T252">amnəlbiʔi</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1022" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_1024" n="HIAT:w" s="T253">Dĭ</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1027" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1030" n="HIAT:w" s="T255">amnaʔbə</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1034" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1036" n="HIAT:w" s="T256">I</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1039" n="HIAT:w" s="T257">leʔi</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1042" n="HIAT:w" s="T258">rukaftə</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1045" n="HIAT:w" s="T259">elleʔbə</ts>
                  <nts id="Seg_1046" n="HIAT:ip">.</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1049" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1051" n="HIAT:w" s="T260">Ara</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1054" n="HIAT:w" s="T261">rukaftə</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1057" n="HIAT:w" s="T262">kămlia</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1061" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1063" n="HIAT:w" s="T263">Čaj</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1066" n="HIAT:w" s="T264">dĭbər</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1069" n="HIAT:w" s="T265">kămlia</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1073" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1075" n="HIAT:w" s="T266">Dĭgəttə</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1078" n="HIAT:w" s="T267">ambiʔi</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1082" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1084" n="HIAT:w" s="T268">Kambiʔi</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1087" n="HIAT:w" s="T269">suʔmisʼtə</ts>
                  <nts id="Seg_1088" n="HIAT:ip">.</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1091" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1093" n="HIAT:w" s="T270">Sʼarzittə</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1096" n="HIAT:w" s="T271">garmonnʼaziʔ</ts>
                  <nts id="Seg_1097" n="HIAT:ip">.</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1100" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1102" n="HIAT:w" s="T272">Dĭgəttə</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1105" n="HIAT:w" s="T273">dĭ</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1108" n="HIAT:w" s="T274">suʔmiluʔpi</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1112" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1114" n="HIAT:w" s="T275">Bü</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1117" n="HIAT:w" s="T276">molaːmbi</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1121" n="HIAT:u" s="T277">
                  <nts id="Seg_1122" n="HIAT:ip">(</nts>
                  <ts e="T278" id="Seg_1124" n="HIAT:w" s="T277">Dĭgət-</ts>
                  <nts id="Seg_1125" n="HIAT:ip">)</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1128" n="HIAT:w" s="T278">Dĭgəttə</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1131" n="HIAT:w" s="T279">onʼiʔ</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1134" n="HIAT:w" s="T280">udandə</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1136" n="HIAT:ip">(</nts>
                  <ts e="T282" id="Seg_1138" n="HIAT:w" s="T281">măxnula</ts>
                  <nts id="Seg_1139" n="HIAT:ip">)</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1142" n="HIAT:w" s="T282">măxnula</ts>
                  <nts id="Seg_1143" n="HIAT:ip">.</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1146" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1148" n="HIAT:w" s="T283">Bü</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1151" n="HIAT:w" s="T284">molaːmbi</ts>
                  <nts id="Seg_1152" n="HIAT:ip">.</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1155" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1157" n="HIAT:w" s="T285">Tsar</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1160" n="HIAT:w" s="T286">bar</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1163" n="HIAT:w" s="T287">nereʔluʔpi</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1167" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1169" n="HIAT:w" s="T288">Baška</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1172" n="HIAT:w" s="T289">udandə</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1175" n="HIAT:w" s="T290">măxnula</ts>
                  <nts id="Seg_1176" n="HIAT:ip">.</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1179" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1181" n="HIAT:w" s="T291">Dĭgəttə</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1184" n="HIAT:w" s="T292">nabəʔi</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1187" n="HIAT:w" s="T293">šoluʔpiʔi</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1191" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1193" n="HIAT:w" s="T294">Dĭgəttə</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1196" n="HIAT:w" s="T295">bar</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1199" n="HIAT:w" s="T296">dʼürleʔpi</ts>
                  <nts id="Seg_1200" n="HIAT:ip">.</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1203" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1205" n="HIAT:w" s="T297">Dĭgəttə</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1208" n="HIAT:w" s="T298">dĭzeŋ</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1210" n="HIAT:ip">(</nts>
                  <ts e="T300" id="Seg_1212" n="HIAT:w" s="T299">bareʔ</ts>
                  <nts id="Seg_1213" n="HIAT:ip">)</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1216" n="HIAT:w" s="T300">nezeŋ</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1219" n="HIAT:w" s="T301">udatsiʔ</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1222" n="HIAT:w" s="T302">barəʔluʔpi</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1226" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1228" n="HIAT:w" s="T303">Bar</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1231" n="HIAT:w" s="T304">ildə</ts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1235" n="HIAT:u" s="T305">
                  <nts id="Seg_1236" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1238" n="HIAT:w" s="T305">i</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1241" n="HIAT:w" s="T306">leziʔ</ts>
                  <nts id="Seg_1242" n="HIAT:ip">)</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1245" n="HIAT:w" s="T307">kadəldə</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1248" n="HIAT:w" s="T308">bar</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1251" n="HIAT:w" s="T309">toʔnarbi</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1253" n="HIAT:ip">(</nts>
                  <ts e="T311" id="Seg_1255" n="HIAT:w" s="T310">bil-</ts>
                  <nts id="Seg_1256" n="HIAT:ip">)</nts>
                  <nts id="Seg_1257" n="HIAT:ip">.</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1260" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1262" n="HIAT:w" s="T311">Büziʔ</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1265" n="HIAT:w" s="T312">bar</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1268" n="HIAT:w" s="T313">sʼimattə</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1271" n="HIAT:w" s="T314">bar</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1274" n="HIAT:w" s="T315">bü</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1276" n="HIAT:ip">(</nts>
                  <ts e="T317" id="Seg_1278" n="HIAT:w" s="T316">kăn-</ts>
                  <nts id="Seg_1279" n="HIAT:ip">)</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1282" n="HIAT:w" s="T317">kămnabiʔi</ts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1286" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1288" n="HIAT:w" s="T318">Kabarləj</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1292" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1294" n="HIAT:w" s="T319">Dĭgəttə</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1297" n="HIAT:w" s="T320">dĭ</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1300" n="HIAT:w" s="T321">nuʔməluʔpi</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1303" n="HIAT:w" s="T322">maːʔndə</ts>
                  <nts id="Seg_1304" n="HIAT:ip">.</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1307" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1309" n="HIAT:w" s="T323">Kubabə</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1312" n="HIAT:w" s="T324">kubi</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1315" n="HIAT:w" s="T325">da</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1318" n="HIAT:w" s="T326">ibi</ts>
                  <nts id="Seg_1319" n="HIAT:ip">,</nts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1322" n="HIAT:w" s="T327">šünə</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1324" n="HIAT:ip">(</nts>
                  <ts e="T329" id="Seg_1326" n="HIAT:w" s="T328">emn-</ts>
                  <nts id="Seg_1327" n="HIAT:ip">)</nts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1330" n="HIAT:w" s="T329">embi</ts>
                  <nts id="Seg_1331" n="HIAT:ip">.</nts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1334" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1336" n="HIAT:w" s="T330">Dĭ</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1339" n="HIAT:w" s="T331">bar</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1342" n="HIAT:w" s="T332">neniluʔpi</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1346" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1348" n="HIAT:w" s="T333">Dĭgəttə</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1351" n="HIAT:w" s="T334">dĭ</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1354" n="HIAT:w" s="T335">šobi</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1357" n="HIAT:w" s="T336">maːndə</ts>
                  <nts id="Seg_1358" n="HIAT:ip">.</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1361" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1363" n="HIAT:w" s="T337">Ugaːndə</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1366" n="HIAT:w" s="T338">tăn</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1369" n="HIAT:w" s="T339">ej</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1372" n="HIAT:w" s="T340">jakšə</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1375" n="HIAT:w" s="T341">abial</ts>
                  <nts id="Seg_1376" n="HIAT:ip">!</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1379" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1381" n="HIAT:w" s="T342">Tüj</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1384" n="HIAT:w" s="T343">măn</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1387" n="HIAT:w" s="T344">kalla</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1390" n="HIAT:w" s="T361">dʼürləm</ts>
                  <nts id="Seg_1391" n="HIAT:ip">.</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1394" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1396" n="HIAT:w" s="T345">A</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1399" n="HIAT:w" s="T346">tăn</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1402" n="HIAT:w" s="T347">măna</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1405" n="HIAT:w" s="T348">măndərlal</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1409" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1411" n="HIAT:w" s="T349">Dĭ</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1414" n="HIAT:w" s="T350">kalla</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1417" n="HIAT:w" s="T362">dʼürbi</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1421" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1423" n="HIAT:w" s="T351">A</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1426" n="HIAT:w" s="T352">dĭ</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1429" n="HIAT:w" s="T353">bar</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1432" n="HIAT:w" s="T354">dʼorbi</ts>
                  <nts id="Seg_1433" n="HIAT:ip">,</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1436" n="HIAT:w" s="T355">dʼorbi</ts>
                  <nts id="Seg_1437" n="HIAT:ip">,</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1440" n="HIAT:w" s="T356">dĭgəttə</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1442" n="HIAT:ip">(</nts>
                  <ts e="T358" id="Seg_1444" n="HIAT:w" s="T357">kambi</ts>
                  <nts id="Seg_1445" n="HIAT:ip">)</nts>
                  <nts id="Seg_1446" n="HIAT:ip">.</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T358" id="Seg_1448" n="sc" s="T0">
               <ts e="T1" id="Seg_1450" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_1452" n="e" s="T1">koŋ </ts>
               <ts e="T3" id="Seg_1454" n="e" s="T2">amnobi. </ts>
               <ts e="T4" id="Seg_1456" n="e" s="T3">Dĭn </ts>
               <ts e="T5" id="Seg_1458" n="e" s="T4">nagur </ts>
               <ts e="T6" id="Seg_1460" n="e" s="T5">nʼizeŋdə </ts>
               <ts e="T7" id="Seg_1462" n="e" s="T6">ibiʔi. </ts>
               <ts e="T8" id="Seg_1464" n="e" s="T7">Dĭgəttə </ts>
               <ts e="T9" id="Seg_1466" n="e" s="T8">dĭ </ts>
               <ts e="T10" id="Seg_1468" n="e" s="T9">măndə </ts>
               <ts e="T11" id="Seg_1470" n="e" s="T10">nʼizeŋdə: </ts>
               <ts e="T12" id="Seg_1472" n="e" s="T11">Nada </ts>
               <ts e="T13" id="Seg_1474" n="e" s="T12">šiʔnʼileʔ </ts>
               <ts e="T14" id="Seg_1476" n="e" s="T13">monoʔkosʼtə </ts>
               <ts e="T15" id="Seg_1478" n="e" s="T14">nezeŋ. </ts>
               <ts e="T16" id="Seg_1480" n="e" s="T15">Štobɨ </ts>
               <ts e="T17" id="Seg_1482" n="e" s="T16">măna </ts>
               <ts e="T18" id="Seg_1484" n="e" s="T17">kuzittə </ts>
               <ts e="T19" id="Seg_1486" n="e" s="T18">(š-) </ts>
               <ts e="T20" id="Seg_1488" n="e" s="T19">šiʔnʼileʔ </ts>
               <ts e="T21" id="Seg_1490" n="e" s="T20">esseŋ. </ts>
               <ts e="T22" id="Seg_1492" n="e" s="T21">No, </ts>
               <ts e="T23" id="Seg_1494" n="e" s="T22">dĭzeŋ </ts>
               <ts e="T24" id="Seg_1496" n="e" s="T23">măndəʔi: </ts>
               <ts e="T25" id="Seg_1498" n="e" s="T24">Nu </ts>
               <ts e="T26" id="Seg_1500" n="e" s="T25">monoʔkoʔ, </ts>
               <ts e="T27" id="Seg_1502" n="e" s="T26">šindi </ts>
               <ts e="T28" id="Seg_1504" n="e" s="T27">tănan </ts>
               <ts e="T29" id="Seg_1506" n="e" s="T28">kereʔ? </ts>
               <ts e="T30" id="Seg_1508" n="e" s="T29">Igeʔ </ts>
               <ts e="T31" id="Seg_1510" n="e" s="T30">multukʔi </ts>
               <ts e="T32" id="Seg_1512" n="e" s="T31">i </ts>
               <ts e="T33" id="Seg_1514" n="e" s="T32">kangaʔ! </ts>
               <ts e="T34" id="Seg_1516" n="e" s="T33">(Dʼiʔtəgeʔ) </ts>
               <ts e="T35" id="Seg_1518" n="e" s="T34">strʼelaʔi! </ts>
               <ts e="T36" id="Seg_1520" n="e" s="T35">Dĭgəttə </ts>
               <ts e="T37" id="Seg_1522" n="e" s="T36">(diʔnə=) </ts>
               <ts e="T38" id="Seg_1524" n="e" s="T37">dĭzeŋ </ts>
               <ts e="T39" id="Seg_1526" n="e" s="T38">kambiʔi. </ts>
               <ts e="T40" id="Seg_1528" n="e" s="T39">Onʼiʔ </ts>
               <ts e="T41" id="Seg_1530" n="e" s="T40">dʼiʔtəbi. </ts>
               <ts e="T42" id="Seg_1532" n="e" s="T41">Saʔməluʔpi </ts>
               <ts e="T43" id="Seg_1534" n="e" s="T42">(kup-) </ts>
               <ts e="T44" id="Seg_1536" n="e" s="T43">kupʼestə, </ts>
               <ts e="T45" id="Seg_1538" n="e" s="T44">turanə. </ts>
               <ts e="T46" id="Seg_1540" n="e" s="T45">Onʼiʔ </ts>
               <ts e="T47" id="Seg_1542" n="e" s="T46">dʼiʔtəbi. </ts>
               <ts e="T48" id="Seg_1544" n="e" s="T47">Saʔməluʔpi </ts>
               <ts e="T49" id="Seg_1546" n="e" s="T48">((PAUSE))… </ts>
               <ts e="T363" id="Seg_1548" n="e" s="T49">((KA:)) </ts>
               <ts e="T50" id="Seg_1550" n="e" s="T363">Боярский. </ts>
               <ts e="T364" id="Seg_1552" n="e" s="T50">((PKZ:)) </ts>
               <ts e="T51" id="Seg_1554" n="e" s="T364">băjarskəj </ts>
               <ts e="T52" id="Seg_1556" n="e" s="T51">dvărestə. </ts>
               <ts e="T53" id="Seg_1558" n="e" s="T52">A </ts>
               <ts e="T54" id="Seg_1560" n="e" s="T53">onʼiʔ </ts>
               <ts e="T55" id="Seg_1562" n="e" s="T54">dʼiʔpi. </ts>
               <ts e="T56" id="Seg_1564" n="e" s="T55">Kuŋgeŋ </ts>
               <ts e="T359" id="Seg_1566" n="e" s="T56">kalla </ts>
               <ts e="T57" id="Seg_1568" n="e" s="T359">dʼürbi. </ts>
               <ts e="T58" id="Seg_1570" n="e" s="T57">Dĭgəttə </ts>
               <ts e="T59" id="Seg_1572" n="e" s="T58">dĭ </ts>
               <ts e="T60" id="Seg_1574" n="e" s="T59">kambi, </ts>
               <ts e="T61" id="Seg_1576" n="e" s="T60">kambi, </ts>
               <ts e="T62" id="Seg_1578" n="e" s="T61">šonəga. </ts>
               <ts e="T63" id="Seg_1580" n="e" s="T62">Bălotăgən </ts>
               <ts e="T64" id="Seg_1582" n="e" s="T63">amnolaʔbə </ts>
               <ts e="T65" id="Seg_1584" n="e" s="T64">lʼaguškadə. </ts>
               <ts e="T66" id="Seg_1586" n="e" s="T65">I </ts>
               <ts e="T67" id="Seg_1588" n="e" s="T66">strʼela </ts>
               <ts e="T68" id="Seg_1590" n="e" s="T67">(dĭʔnə= </ts>
               <ts e="T69" id="Seg_1592" n="e" s="T68">u-) </ts>
               <ts e="T70" id="Seg_1594" n="e" s="T69">dĭʔən. </ts>
               <ts e="T71" id="Seg_1596" n="e" s="T70">Dĭ </ts>
               <ts e="T72" id="Seg_1598" n="e" s="T71">măndə: </ts>
               <ts e="T73" id="Seg_1600" n="e" s="T72">Deʔ </ts>
               <ts e="T74" id="Seg_1602" n="e" s="T73">măna </ts>
               <ts e="T75" id="Seg_1604" n="e" s="T74">strʼela! </ts>
               <ts e="T76" id="Seg_1606" n="e" s="T75">A </ts>
               <ts e="T77" id="Seg_1608" n="e" s="T76">tăn,— </ts>
               <ts e="T78" id="Seg_1610" n="e" s="T77">dĭ </ts>
               <ts e="T79" id="Seg_1612" n="e" s="T78">măndə. </ts>
               <ts e="T80" id="Seg_1614" n="e" s="T79">Iʔ </ts>
               <ts e="T81" id="Seg_1616" n="e" s="T80">măna </ts>
               <ts e="T82" id="Seg_1618" n="e" s="T81">tibinə! </ts>
               <ts e="T83" id="Seg_1620" n="e" s="T82">Da </ts>
               <ts e="T84" id="Seg_1622" n="e" s="T83">kăde </ts>
               <ts e="T85" id="Seg_1624" n="e" s="T84">măn </ts>
               <ts e="T86" id="Seg_1626" n="e" s="T85">tănan </ts>
               <ts e="T87" id="Seg_1628" n="e" s="T86">ilem? </ts>
               <ts e="T88" id="Seg_1630" n="e" s="T87">Tăn </ts>
               <ts e="T89" id="Seg_1632" n="e" s="T88">vedʼ </ts>
               <ts e="T90" id="Seg_1634" n="e" s="T89">lʼaguška. </ts>
               <ts e="T91" id="Seg_1636" n="e" s="T90">Nu </ts>
               <ts e="T92" id="Seg_1638" n="e" s="T91">dăk, </ts>
               <ts e="T93" id="Seg_1640" n="e" s="T92">(tăn=) </ts>
               <ts e="T94" id="Seg_1642" n="e" s="T93">tăn </ts>
               <ts e="T95" id="Seg_1644" n="e" s="T94">sudʼbal, </ts>
               <ts e="T96" id="Seg_1646" n="e" s="T95">it </ts>
               <ts e="T97" id="Seg_1648" n="e" s="T96">măna! </ts>
               <ts e="T98" id="Seg_1650" n="e" s="T97">Dĭgəttə </ts>
               <ts e="T99" id="Seg_1652" n="e" s="T98">dĭ </ts>
               <ts e="T100" id="Seg_1654" n="e" s="T99">ibi. </ts>
               <ts e="T101" id="Seg_1656" n="e" s="T100">Maːndə </ts>
               <ts e="T102" id="Seg_1658" n="e" s="T101">deʔpi. </ts>
               <ts e="T103" id="Seg_1660" n="e" s="T102">Dĭgəttə </ts>
               <ts e="T104" id="Seg_1662" n="e" s="T103">(abat- </ts>
               <ts e="T105" id="Seg_1664" n="e" s="T104">al-) </ts>
               <ts e="T106" id="Seg_1666" n="e" s="T105">abi </ts>
               <ts e="T107" id="Seg_1668" n="e" s="T106">svadʼbaʔi. </ts>
               <ts e="T108" id="Seg_1670" n="e" s="T107">Dĭgəttə </ts>
               <ts e="T109" id="Seg_1672" n="e" s="T108">mămbi: </ts>
               <ts e="T110" id="Seg_1674" n="e" s="T109">šödörjoʔ </ts>
               <ts e="T111" id="Seg_1676" n="e" s="T110">kujnegəʔi. </ts>
               <ts e="T112" id="Seg_1678" n="e" s="T111">Dĭgəttə </ts>
               <ts e="T113" id="Seg_1680" n="e" s="T112">(šonə-) </ts>
               <ts e="T114" id="Seg_1682" n="e" s="T113">šobi </ts>
               <ts e="T115" id="Seg_1684" n="e" s="T114">Vanʼuška </ts>
               <ts e="T116" id="Seg_1686" n="e" s="T115">maːndə. </ts>
               <ts e="T117" id="Seg_1688" n="e" s="T116">I </ts>
               <ts e="T118" id="Seg_1690" n="e" s="T117">ĭmbidə </ts>
               <ts e="T119" id="Seg_1692" n="e" s="T118">ej </ts>
               <ts e="T120" id="Seg_1694" n="e" s="T119">dʼăbaktəriam. </ts>
               <ts e="T121" id="Seg_1696" n="e" s="T120">Ĭmbi </ts>
               <ts e="T122" id="Seg_1698" n="e" s="T121">tăn </ts>
               <ts e="T123" id="Seg_1700" n="e" s="T122">ĭmbidə </ts>
               <ts e="T124" id="Seg_1702" n="e" s="T123">ej </ts>
               <ts e="T125" id="Seg_1704" n="e" s="T124">dʼăbaktəria? </ts>
               <ts e="T126" id="Seg_1706" n="e" s="T125">Da </ts>
               <ts e="T127" id="Seg_1708" n="e" s="T126">abam </ts>
               <ts e="T128" id="Seg_1710" n="e" s="T127">mămbi </ts>
               <ts e="T129" id="Seg_1712" n="e" s="T128">kujnek </ts>
               <ts e="T130" id="Seg_1714" n="e" s="T129">šöʔsittə. </ts>
               <ts e="T131" id="Seg_1716" n="e" s="T130">Iʔbeʔ, </ts>
               <ts e="T132" id="Seg_1718" n="e" s="T131">(kunoluʔ-) </ts>
               <ts e="T133" id="Seg_1720" n="e" s="T132">kunolaʔ, </ts>
               <ts e="T134" id="Seg_1722" n="e" s="T133">a </ts>
               <ts e="T135" id="Seg_1724" n="e" s="T134">(e-) </ts>
               <ts e="T136" id="Seg_1726" n="e" s="T135">kujnek </ts>
               <ts e="T137" id="Seg_1728" n="e" s="T136">moləj. </ts>
               <ts e="T138" id="Seg_1730" n="e" s="T137">Dĭgəttə </ts>
               <ts e="T139" id="Seg_1732" n="e" s="T138">dĭ </ts>
               <ts e="T140" id="Seg_1734" n="e" s="T139">kunolluʔpi. </ts>
               <ts e="T141" id="Seg_1736" n="e" s="T140">A </ts>
               <ts e="T142" id="Seg_1738" n="e" s="T141">dĭ </ts>
               <ts e="T143" id="Seg_1740" n="e" s="T142">nʼiʔdə </ts>
               <ts e="T360" id="Seg_1742" n="e" s="T143">kalla </ts>
               <ts e="T144" id="Seg_1744" n="e" s="T360">dʼürbi. </ts>
               <ts e="T145" id="Seg_1746" n="e" s="T144">Kubat </ts>
               <ts e="T146" id="Seg_1748" n="e" s="T145">kürbi. </ts>
               <ts e="T147" id="Seg_1750" n="e" s="T146">Davaj </ts>
               <ts e="T148" id="Seg_1752" n="e" s="T147">kirgarzittə: </ts>
               <ts e="T149" id="Seg_1754" n="e" s="T148">Šöʔkeʔ </ts>
               <ts e="T150" id="Seg_1756" n="e" s="T149">măna! </ts>
               <ts e="T151" id="Seg_1758" n="e" s="T150">Kujnek </ts>
               <ts e="T152" id="Seg_1760" n="e" s="T151">abanə, </ts>
               <ts e="T153" id="Seg_1762" n="e" s="T152">kuvas, </ts>
               <ts e="T154" id="Seg_1764" n="e" s="T153">jakšə. </ts>
               <ts e="T155" id="Seg_1766" n="e" s="T154">Dĭgəttə </ts>
               <ts e="T156" id="Seg_1768" n="e" s="T155">bazoʔ </ts>
               <ts e="T157" id="Seg_1770" n="e" s="T156">kubabə </ts>
               <ts e="T158" id="Seg_1772" n="e" s="T157">šerbi. </ts>
               <ts e="T159" id="Seg_1774" n="e" s="T158">(Tʼuna-) </ts>
               <ts e="T160" id="Seg_1776" n="e" s="T159">Turanə </ts>
               <ts e="T161" id="Seg_1778" n="e" s="T160">šobi. </ts>
               <ts e="T162" id="Seg_1780" n="e" s="T161">Kujnektə </ts>
               <ts e="T163" id="Seg_1782" n="e" s="T162">embi </ts>
               <ts e="T164" id="Seg_1784" n="e" s="T163">stolbə. </ts>
               <ts e="T165" id="Seg_1786" n="e" s="T164">Stolbə, </ts>
               <ts e="T166" id="Seg_1788" n="e" s="T165">dĭgəttə </ts>
               <ts e="T167" id="Seg_1790" n="e" s="T166">uʔbdəbi </ts>
               <ts e="T168" id="Seg_1792" n="e" s="T167">tibit. </ts>
               <ts e="T169" id="Seg_1794" n="e" s="T168">No, </ts>
               <ts e="T170" id="Seg_1796" n="e" s="T169">it </ts>
               <ts e="T171" id="Seg_1798" n="e" s="T170">kujnek! </ts>
               <ts e="T172" id="Seg_1800" n="e" s="T171">Kundə </ts>
               <ts e="T173" id="Seg_1802" n="e" s="T172">abandə! </ts>
               <ts e="T174" id="Seg_1804" n="e" s="T173">Dĭ </ts>
               <ts e="T175" id="Seg_1806" n="e" s="T174">(kun-) </ts>
               <ts e="T176" id="Seg_1808" n="e" s="T175">kambi, </ts>
               <ts e="T177" id="Seg_1810" n="e" s="T176">abat </ts>
               <ts e="T178" id="Seg_1812" n="e" s="T177">ibi. </ts>
               <ts e="T179" id="Seg_1814" n="e" s="T178">Staršij </ts>
               <ts e="T180" id="Seg_1816" n="e" s="T179">nʼində </ts>
               <ts e="T181" id="Seg_1818" n="e" s="T180">kujnek, </ts>
               <ts e="T182" id="Seg_1820" n="e" s="T181">dĭ </ts>
               <ts e="T183" id="Seg_1822" n="e" s="T182">tolʼkă </ts>
               <ts e="T184" id="Seg_1824" n="e" s="T183">togonorzittə </ts>
               <ts e="T185" id="Seg_1826" n="e" s="T184">dĭ </ts>
               <ts e="T186" id="Seg_1828" n="e" s="T185">kujnektən. </ts>
               <ts e="T187" id="Seg_1830" n="e" s="T186">A </ts>
               <ts e="T188" id="Seg_1832" n="e" s="T187">baška </ts>
               <ts e="T189" id="Seg_1834" n="e" s="T188">nʼin, </ts>
               <ts e="T190" id="Seg_1836" n="e" s="T189">a </ts>
               <ts e="T191" id="Seg_1838" n="e" s="T190">dö </ts>
               <ts e="T192" id="Seg_1840" n="e" s="T191">kujnek </ts>
               <ts e="T193" id="Seg_1842" n="e" s="T192">tolʼkă </ts>
               <ts e="T194" id="Seg_1844" n="e" s="T193">multʼanə </ts>
               <ts e="T195" id="Seg_1846" n="e" s="T194">mĭzittə. </ts>
               <ts e="T196" id="Seg_1848" n="e" s="T195">A </ts>
               <ts e="T197" id="Seg_1850" n="e" s="T196">dĭ </ts>
               <ts e="T198" id="Seg_1852" n="e" s="T197">nʼi </ts>
               <ts e="T199" id="Seg_1854" n="e" s="T198">mĭbi. </ts>
               <ts e="T200" id="Seg_1856" n="e" s="T199">A </ts>
               <ts e="T201" id="Seg_1858" n="e" s="T200">(dö= </ts>
               <ts e="T202" id="Seg_1860" n="e" s="T201">dö=) </ts>
               <ts e="T203" id="Seg_1862" n="e" s="T202">dĭ </ts>
               <ts e="T204" id="Seg_1864" n="e" s="T203">kujnek </ts>
               <ts e="T205" id="Seg_1866" n="e" s="T204">tolʼkă </ts>
               <ts e="T206" id="Seg_1868" n="e" s="T205">(sarskən) </ts>
               <ts e="T207" id="Seg_1870" n="e" s="T206">amnozittə. </ts>
               <ts e="T208" id="Seg_1872" n="e" s="T207">Kabarləj. </ts>
               <ts e="T209" id="Seg_1874" n="e" s="T208">((DMG)) </ts>
               <ts e="T210" id="Seg_1876" n="e" s="T209">Dĭn </ts>
               <ts e="T211" id="Seg_1878" n="e" s="T210">abat </ts>
               <ts e="T212" id="Seg_1880" n="e" s="T211">ildə </ts>
               <ts e="T213" id="Seg_1882" n="e" s="T212">iʔgö </ts>
               <ts e="T214" id="Seg_1884" n="e" s="T213">oʔbdəbi. </ts>
               <ts e="T215" id="Seg_1886" n="e" s="T214">Stoldə </ts>
               <ts e="T216" id="Seg_1888" n="e" s="T215">uʔbdəbi. </ts>
               <ts e="T217" id="Seg_1890" n="e" s="T216">Kăštəbi </ts>
               <ts e="T218" id="Seg_1892" n="e" s="T217">nʼizeŋ, </ts>
               <ts e="T219" id="Seg_1894" n="e" s="T218">i </ts>
               <ts e="T220" id="Seg_1896" n="e" s="T219">nezeŋ. </ts>
               <ts e="T221" id="Seg_1898" n="e" s="T220">Dĭgəttə </ts>
               <ts e="T222" id="Seg_1900" n="e" s="T221">šobiʔi, </ts>
               <ts e="T223" id="Seg_1902" n="e" s="T222">amnobiʔi </ts>
               <ts e="T224" id="Seg_1904" n="e" s="T223">stoldə. </ts>
               <ts e="T225" id="Seg_1906" n="e" s="T224">Uja </ts>
               <ts e="T226" id="Seg_1908" n="e" s="T225">ambiʔi. </ts>
               <ts e="T227" id="Seg_1910" n="e" s="T226">I </ts>
               <ts e="T228" id="Seg_1912" n="e" s="T227">leʔi </ts>
               <ts e="T229" id="Seg_1914" n="e" s="T228">(nda). </ts>
               <ts e="T230" id="Seg_1916" n="e" s="T229">Dĭgəttə </ts>
               <ts e="T231" id="Seg_1918" n="e" s="T230">šobiʔi </ts>
               <ts e="T232" id="Seg_1920" n="e" s="T231">dĭn </ts>
               <ts e="T233" id="Seg_1922" n="e" s="T232">kagazaŋdə, </ts>
               <ts e="T234" id="Seg_1924" n="e" s="T233">mălliaʔi: </ts>
               <ts e="T235" id="Seg_1926" n="e" s="T234">Šindidə </ts>
               <ts e="T236" id="Seg_1928" n="e" s="T235">šonəga. </ts>
               <ts e="T237" id="Seg_1930" n="e" s="T236">A </ts>
               <ts e="T238" id="Seg_1932" n="e" s="T237">(dĭ= </ts>
               <ts e="T239" id="Seg_1934" n="e" s="T238">m- </ts>
               <ts e="T240" id="Seg_1936" n="e" s="T239">dĭm=) </ts>
               <ts e="T241" id="Seg_1938" n="e" s="T240">dĭ </ts>
               <ts e="T242" id="Seg_1940" n="e" s="T241">măndə: </ts>
               <ts e="T243" id="Seg_1942" n="e" s="T242">Măn </ts>
               <ts e="T244" id="Seg_1944" n="e" s="T243">lʼaguškam </ts>
               <ts e="T245" id="Seg_1946" n="e" s="T244">šonəga. </ts>
               <ts e="T246" id="Seg_1948" n="e" s="T245">Dĭgəttə </ts>
               <ts e="T247" id="Seg_1950" n="e" s="T246">dĭ </ts>
               <ts e="T248" id="Seg_1952" n="e" s="T247">šobi, </ts>
               <ts e="T249" id="Seg_1954" n="e" s="T248">kuvas </ts>
               <ts e="T250" id="Seg_1956" n="e" s="T249">bar. </ts>
               <ts e="T251" id="Seg_1958" n="e" s="T250">Dĭgəttə </ts>
               <ts e="T252" id="Seg_1960" n="e" s="T251">dĭzeŋ </ts>
               <ts e="T253" id="Seg_1962" n="e" s="T252">amnəlbiʔi. </ts>
               <ts e="T254" id="Seg_1964" n="e" s="T253">Dĭ </ts>
               <ts e="T255" id="Seg_1966" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_1968" n="e" s="T255">amnaʔbə. </ts>
               <ts e="T257" id="Seg_1970" n="e" s="T256">I </ts>
               <ts e="T258" id="Seg_1972" n="e" s="T257">leʔi </ts>
               <ts e="T259" id="Seg_1974" n="e" s="T258">rukaftə </ts>
               <ts e="T260" id="Seg_1976" n="e" s="T259">elleʔbə. </ts>
               <ts e="T261" id="Seg_1978" n="e" s="T260">Ara </ts>
               <ts e="T262" id="Seg_1980" n="e" s="T261">rukaftə </ts>
               <ts e="T263" id="Seg_1982" n="e" s="T262">kămlia. </ts>
               <ts e="T264" id="Seg_1984" n="e" s="T263">Čaj </ts>
               <ts e="T265" id="Seg_1986" n="e" s="T264">dĭbər </ts>
               <ts e="T266" id="Seg_1988" n="e" s="T265">kămlia. </ts>
               <ts e="T267" id="Seg_1990" n="e" s="T266">Dĭgəttə </ts>
               <ts e="T268" id="Seg_1992" n="e" s="T267">ambiʔi. </ts>
               <ts e="T269" id="Seg_1994" n="e" s="T268">Kambiʔi </ts>
               <ts e="T270" id="Seg_1996" n="e" s="T269">suʔmisʼtə. </ts>
               <ts e="T271" id="Seg_1998" n="e" s="T270">Sʼarzittə </ts>
               <ts e="T272" id="Seg_2000" n="e" s="T271">garmonnʼaziʔ. </ts>
               <ts e="T273" id="Seg_2002" n="e" s="T272">Dĭgəttə </ts>
               <ts e="T274" id="Seg_2004" n="e" s="T273">dĭ </ts>
               <ts e="T275" id="Seg_2006" n="e" s="T274">suʔmiluʔpi. </ts>
               <ts e="T276" id="Seg_2008" n="e" s="T275">Bü </ts>
               <ts e="T277" id="Seg_2010" n="e" s="T276">molaːmbi. </ts>
               <ts e="T278" id="Seg_2012" n="e" s="T277">(Dĭgət-) </ts>
               <ts e="T279" id="Seg_2014" n="e" s="T278">Dĭgəttə </ts>
               <ts e="T280" id="Seg_2016" n="e" s="T279">onʼiʔ </ts>
               <ts e="T281" id="Seg_2018" n="e" s="T280">udandə </ts>
               <ts e="T282" id="Seg_2020" n="e" s="T281">(măxnula) </ts>
               <ts e="T283" id="Seg_2022" n="e" s="T282">măxnula. </ts>
               <ts e="T284" id="Seg_2024" n="e" s="T283">Bü </ts>
               <ts e="T285" id="Seg_2026" n="e" s="T284">molaːmbi. </ts>
               <ts e="T286" id="Seg_2028" n="e" s="T285">Tsar </ts>
               <ts e="T287" id="Seg_2030" n="e" s="T286">bar </ts>
               <ts e="T288" id="Seg_2032" n="e" s="T287">nereʔluʔpi. </ts>
               <ts e="T289" id="Seg_2034" n="e" s="T288">Baška </ts>
               <ts e="T290" id="Seg_2036" n="e" s="T289">udandə </ts>
               <ts e="T291" id="Seg_2038" n="e" s="T290">măxnula. </ts>
               <ts e="T292" id="Seg_2040" n="e" s="T291">Dĭgəttə </ts>
               <ts e="T293" id="Seg_2042" n="e" s="T292">nabəʔi </ts>
               <ts e="T294" id="Seg_2044" n="e" s="T293">šoluʔpiʔi. </ts>
               <ts e="T295" id="Seg_2046" n="e" s="T294">Dĭgəttə </ts>
               <ts e="T296" id="Seg_2048" n="e" s="T295">bar </ts>
               <ts e="T297" id="Seg_2050" n="e" s="T296">dʼürleʔpi. </ts>
               <ts e="T298" id="Seg_2052" n="e" s="T297">Dĭgəttə </ts>
               <ts e="T299" id="Seg_2054" n="e" s="T298">dĭzeŋ </ts>
               <ts e="T300" id="Seg_2056" n="e" s="T299">(bareʔ) </ts>
               <ts e="T301" id="Seg_2058" n="e" s="T300">nezeŋ </ts>
               <ts e="T302" id="Seg_2060" n="e" s="T301">udatsiʔ </ts>
               <ts e="T303" id="Seg_2062" n="e" s="T302">barəʔluʔpi. </ts>
               <ts e="T304" id="Seg_2064" n="e" s="T303">Bar </ts>
               <ts e="T305" id="Seg_2066" n="e" s="T304">ildə. </ts>
               <ts e="T306" id="Seg_2068" n="e" s="T305">(i </ts>
               <ts e="T307" id="Seg_2070" n="e" s="T306">leziʔ) </ts>
               <ts e="T308" id="Seg_2072" n="e" s="T307">kadəldə </ts>
               <ts e="T309" id="Seg_2074" n="e" s="T308">bar </ts>
               <ts e="T310" id="Seg_2076" n="e" s="T309">toʔnarbi </ts>
               <ts e="T311" id="Seg_2078" n="e" s="T310">(bil-). </ts>
               <ts e="T312" id="Seg_2080" n="e" s="T311">Büziʔ </ts>
               <ts e="T313" id="Seg_2082" n="e" s="T312">bar </ts>
               <ts e="T314" id="Seg_2084" n="e" s="T313">sʼimattə </ts>
               <ts e="T315" id="Seg_2086" n="e" s="T314">bar </ts>
               <ts e="T316" id="Seg_2088" n="e" s="T315">bü </ts>
               <ts e="T317" id="Seg_2090" n="e" s="T316">(kăn-) </ts>
               <ts e="T318" id="Seg_2092" n="e" s="T317">kămnabiʔi. </ts>
               <ts e="T319" id="Seg_2094" n="e" s="T318">Kabarləj. </ts>
               <ts e="T320" id="Seg_2096" n="e" s="T319">Dĭgəttə </ts>
               <ts e="T321" id="Seg_2098" n="e" s="T320">dĭ </ts>
               <ts e="T322" id="Seg_2100" n="e" s="T321">nuʔməluʔpi </ts>
               <ts e="T323" id="Seg_2102" n="e" s="T322">maːʔndə. </ts>
               <ts e="T324" id="Seg_2104" n="e" s="T323">Kubabə </ts>
               <ts e="T325" id="Seg_2106" n="e" s="T324">kubi </ts>
               <ts e="T326" id="Seg_2108" n="e" s="T325">da </ts>
               <ts e="T327" id="Seg_2110" n="e" s="T326">ibi, </ts>
               <ts e="T328" id="Seg_2112" n="e" s="T327">šünə </ts>
               <ts e="T329" id="Seg_2114" n="e" s="T328">(emn-) </ts>
               <ts e="T330" id="Seg_2116" n="e" s="T329">embi. </ts>
               <ts e="T331" id="Seg_2118" n="e" s="T330">Dĭ </ts>
               <ts e="T332" id="Seg_2120" n="e" s="T331">bar </ts>
               <ts e="T333" id="Seg_2122" n="e" s="T332">neniluʔpi. </ts>
               <ts e="T334" id="Seg_2124" n="e" s="T333">Dĭgəttə </ts>
               <ts e="T335" id="Seg_2126" n="e" s="T334">dĭ </ts>
               <ts e="T336" id="Seg_2128" n="e" s="T335">šobi </ts>
               <ts e="T337" id="Seg_2130" n="e" s="T336">maːndə. </ts>
               <ts e="T338" id="Seg_2132" n="e" s="T337">Ugaːndə </ts>
               <ts e="T339" id="Seg_2134" n="e" s="T338">tăn </ts>
               <ts e="T340" id="Seg_2136" n="e" s="T339">ej </ts>
               <ts e="T341" id="Seg_2138" n="e" s="T340">jakšə </ts>
               <ts e="T342" id="Seg_2140" n="e" s="T341">abial! </ts>
               <ts e="T343" id="Seg_2142" n="e" s="T342">Tüj </ts>
               <ts e="T344" id="Seg_2144" n="e" s="T343">măn </ts>
               <ts e="T361" id="Seg_2146" n="e" s="T344">kalla </ts>
               <ts e="T345" id="Seg_2148" n="e" s="T361">dʼürləm. </ts>
               <ts e="T346" id="Seg_2150" n="e" s="T345">A </ts>
               <ts e="T347" id="Seg_2152" n="e" s="T346">tăn </ts>
               <ts e="T348" id="Seg_2154" n="e" s="T347">măna </ts>
               <ts e="T349" id="Seg_2156" n="e" s="T348">măndərlal. </ts>
               <ts e="T350" id="Seg_2158" n="e" s="T349">Dĭ </ts>
               <ts e="T362" id="Seg_2160" n="e" s="T350">kalla </ts>
               <ts e="T351" id="Seg_2162" n="e" s="T362">dʼürbi. </ts>
               <ts e="T352" id="Seg_2164" n="e" s="T351">A </ts>
               <ts e="T353" id="Seg_2166" n="e" s="T352">dĭ </ts>
               <ts e="T354" id="Seg_2168" n="e" s="T353">bar </ts>
               <ts e="T355" id="Seg_2170" n="e" s="T354">dʼorbi, </ts>
               <ts e="T356" id="Seg_2172" n="e" s="T355">dʼorbi, </ts>
               <ts e="T357" id="Seg_2174" n="e" s="T356">dĭgəttə </ts>
               <ts e="T358" id="Seg_2176" n="e" s="T357">(kambi). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2177" s="T0">PKZ_196X_FrogPrincess_flk.001 (001)</ta>
            <ta e="T7" id="Seg_2178" s="T3">PKZ_196X_FrogPrincess_flk.002 (002)</ta>
            <ta e="T15" id="Seg_2179" s="T7">PKZ_196X_FrogPrincess_flk.003 (003)</ta>
            <ta e="T21" id="Seg_2180" s="T15">PKZ_196X_FrogPrincess_flk.004 (005)</ta>
            <ta e="T29" id="Seg_2181" s="T21">PKZ_196X_FrogPrincess_flk.005 (006) </ta>
            <ta e="T33" id="Seg_2182" s="T29">PKZ_196X_FrogPrincess_flk.006 (008)</ta>
            <ta e="T35" id="Seg_2183" s="T33">PKZ_196X_FrogPrincess_flk.007 (009)</ta>
            <ta e="T39" id="Seg_2184" s="T35">PKZ_196X_FrogPrincess_flk.008 (010)</ta>
            <ta e="T41" id="Seg_2185" s="T39">PKZ_196X_FrogPrincess_flk.009 (011)</ta>
            <ta e="T45" id="Seg_2186" s="T41">PKZ_196X_FrogPrincess_flk.010 (012)</ta>
            <ta e="T47" id="Seg_2187" s="T45">PKZ_196X_FrogPrincess_flk.011 (013)</ta>
            <ta e="T49" id="Seg_2188" s="T47">PKZ_196X_FrogPrincess_flk.012 (014)</ta>
            <ta e="T50" id="Seg_2189" s="T49">PKZ_196X_FrogPrincess_flk.013 (014)</ta>
            <ta e="T52" id="Seg_2190" s="T50">PKZ_196X_FrogPrincess_flk.014 (014)</ta>
            <ta e="T55" id="Seg_2191" s="T52">PKZ_196X_FrogPrincess_flk.015 (016)</ta>
            <ta e="T57" id="Seg_2192" s="T55">PKZ_196X_FrogPrincess_flk.016 (017)</ta>
            <ta e="T62" id="Seg_2193" s="T57">PKZ_196X_FrogPrincess_flk.017 (018)</ta>
            <ta e="T65" id="Seg_2194" s="T62">PKZ_196X_FrogPrincess_flk.018 (019)</ta>
            <ta e="T70" id="Seg_2195" s="T65">PKZ_196X_FrogPrincess_flk.019 (020)</ta>
            <ta e="T75" id="Seg_2196" s="T70">PKZ_196X_FrogPrincess_flk.020 (021)</ta>
            <ta e="T79" id="Seg_2197" s="T75">PKZ_196X_FrogPrincess_flk.021 (022)</ta>
            <ta e="T82" id="Seg_2198" s="T79">PKZ_196X_FrogPrincess_flk.022 (023)</ta>
            <ta e="T87" id="Seg_2199" s="T82">PKZ_196X_FrogPrincess_flk.023 (024)</ta>
            <ta e="T90" id="Seg_2200" s="T87">PKZ_196X_FrogPrincess_flk.024 (025)</ta>
            <ta e="T97" id="Seg_2201" s="T90">PKZ_196X_FrogPrincess_flk.025 (026)</ta>
            <ta e="T100" id="Seg_2202" s="T97">PKZ_196X_FrogPrincess_flk.026 (027)</ta>
            <ta e="T102" id="Seg_2203" s="T100">PKZ_196X_FrogPrincess_flk.027 (028)</ta>
            <ta e="T107" id="Seg_2204" s="T102">PKZ_196X_FrogPrincess_flk.028 (029)</ta>
            <ta e="T111" id="Seg_2205" s="T107">PKZ_196X_FrogPrincess_flk.029 (030)</ta>
            <ta e="T116" id="Seg_2206" s="T111">PKZ_196X_FrogPrincess_flk.030 (031)</ta>
            <ta e="T120" id="Seg_2207" s="T116">PKZ_196X_FrogPrincess_flk.031 (032)</ta>
            <ta e="T125" id="Seg_2208" s="T120">PKZ_196X_FrogPrincess_flk.032 (033)</ta>
            <ta e="T130" id="Seg_2209" s="T125">PKZ_196X_FrogPrincess_flk.033 (034)</ta>
            <ta e="T137" id="Seg_2210" s="T130">PKZ_196X_FrogPrincess_flk.034 (035)</ta>
            <ta e="T140" id="Seg_2211" s="T137">PKZ_196X_FrogPrincess_flk.035 (036)</ta>
            <ta e="T144" id="Seg_2212" s="T140">PKZ_196X_FrogPrincess_flk.036 (037)</ta>
            <ta e="T146" id="Seg_2213" s="T144">PKZ_196X_FrogPrincess_flk.037 (038)</ta>
            <ta e="T150" id="Seg_2214" s="T146">PKZ_196X_FrogPrincess_flk.038 (039)</ta>
            <ta e="T154" id="Seg_2215" s="T150">PKZ_196X_FrogPrincess_flk.039 (040)</ta>
            <ta e="T158" id="Seg_2216" s="T154">PKZ_196X_FrogPrincess_flk.040 (041)</ta>
            <ta e="T161" id="Seg_2217" s="T158">PKZ_196X_FrogPrincess_flk.041 (042)</ta>
            <ta e="T164" id="Seg_2218" s="T161">PKZ_196X_FrogPrincess_flk.042 (043)</ta>
            <ta e="T168" id="Seg_2219" s="T164">PKZ_196X_FrogPrincess_flk.043 (044)</ta>
            <ta e="T171" id="Seg_2220" s="T168">PKZ_196X_FrogPrincess_flk.044 (045)</ta>
            <ta e="T173" id="Seg_2221" s="T171">PKZ_196X_FrogPrincess_flk.045 (046)</ta>
            <ta e="T178" id="Seg_2222" s="T173">PKZ_196X_FrogPrincess_flk.046 (047)</ta>
            <ta e="T186" id="Seg_2223" s="T178">PKZ_196X_FrogPrincess_flk.047 (048)</ta>
            <ta e="T195" id="Seg_2224" s="T186">PKZ_196X_FrogPrincess_flk.048 (049)</ta>
            <ta e="T199" id="Seg_2225" s="T195">PKZ_196X_FrogPrincess_flk.049 (050)</ta>
            <ta e="T207" id="Seg_2226" s="T199">PKZ_196X_FrogPrincess_flk.050 (051)</ta>
            <ta e="T208" id="Seg_2227" s="T207">PKZ_196X_FrogPrincess_flk.051 (052)</ta>
            <ta e="T214" id="Seg_2228" s="T208">PKZ_196X_FrogPrincess_flk.052 (053)</ta>
            <ta e="T216" id="Seg_2229" s="T214">PKZ_196X_FrogPrincess_flk.053 (054)</ta>
            <ta e="T220" id="Seg_2230" s="T216">PKZ_196X_FrogPrincess_flk.054 (055)</ta>
            <ta e="T224" id="Seg_2231" s="T220">PKZ_196X_FrogPrincess_flk.055 (056)</ta>
            <ta e="T226" id="Seg_2232" s="T224">PKZ_196X_FrogPrincess_flk.056 (057)</ta>
            <ta e="T229" id="Seg_2233" s="T226">PKZ_196X_FrogPrincess_flk.057 (058)</ta>
            <ta e="T236" id="Seg_2234" s="T229">PKZ_196X_FrogPrincess_flk.058 (059)</ta>
            <ta e="T245" id="Seg_2235" s="T236">PKZ_196X_FrogPrincess_flk.059 (060)</ta>
            <ta e="T250" id="Seg_2236" s="T245">PKZ_196X_FrogPrincess_flk.060 (061)</ta>
            <ta e="T253" id="Seg_2237" s="T250">PKZ_196X_FrogPrincess_flk.061 (062)</ta>
            <ta e="T256" id="Seg_2238" s="T253">PKZ_196X_FrogPrincess_flk.062 (063)</ta>
            <ta e="T260" id="Seg_2239" s="T256">PKZ_196X_FrogPrincess_flk.063 (064)</ta>
            <ta e="T263" id="Seg_2240" s="T260">PKZ_196X_FrogPrincess_flk.064 (065)</ta>
            <ta e="T266" id="Seg_2241" s="T263">PKZ_196X_FrogPrincess_flk.065 (066)</ta>
            <ta e="T268" id="Seg_2242" s="T266">PKZ_196X_FrogPrincess_flk.066 (067)</ta>
            <ta e="T270" id="Seg_2243" s="T268">PKZ_196X_FrogPrincess_flk.067 (068)</ta>
            <ta e="T272" id="Seg_2244" s="T270">PKZ_196X_FrogPrincess_flk.068 (069)</ta>
            <ta e="T275" id="Seg_2245" s="T272">PKZ_196X_FrogPrincess_flk.069 (070)</ta>
            <ta e="T277" id="Seg_2246" s="T275">PKZ_196X_FrogPrincess_flk.070 (071)</ta>
            <ta e="T283" id="Seg_2247" s="T277">PKZ_196X_FrogPrincess_flk.071 (072)</ta>
            <ta e="T285" id="Seg_2248" s="T283">PKZ_196X_FrogPrincess_flk.072 (073)</ta>
            <ta e="T288" id="Seg_2249" s="T285">PKZ_196X_FrogPrincess_flk.073 (074)</ta>
            <ta e="T291" id="Seg_2250" s="T288">PKZ_196X_FrogPrincess_flk.074 (075)</ta>
            <ta e="T294" id="Seg_2251" s="T291">PKZ_196X_FrogPrincess_flk.075 (076)</ta>
            <ta e="T297" id="Seg_2252" s="T294">PKZ_196X_FrogPrincess_flk.076 (077)</ta>
            <ta e="T303" id="Seg_2253" s="T297">PKZ_196X_FrogPrincess_flk.077 (078)</ta>
            <ta e="T305" id="Seg_2254" s="T303">PKZ_196X_FrogPrincess_flk.078 (079.001)</ta>
            <ta e="T311" id="Seg_2255" s="T305">PKZ_196X_FrogPrincess_flk.079 (079.002)</ta>
            <ta e="T318" id="Seg_2256" s="T311">PKZ_196X_FrogPrincess_flk.080 (080)</ta>
            <ta e="T319" id="Seg_2257" s="T318">PKZ_196X_FrogPrincess_flk.081 (081)</ta>
            <ta e="T323" id="Seg_2258" s="T319">PKZ_196X_FrogPrincess_flk.082 (082)</ta>
            <ta e="T330" id="Seg_2259" s="T323">PKZ_196X_FrogPrincess_flk.083 (083)</ta>
            <ta e="T333" id="Seg_2260" s="T330">PKZ_196X_FrogPrincess_flk.084 (084)</ta>
            <ta e="T337" id="Seg_2261" s="T333">PKZ_196X_FrogPrincess_flk.085 (085)</ta>
            <ta e="T342" id="Seg_2262" s="T337">PKZ_196X_FrogPrincess_flk.086 (086)</ta>
            <ta e="T345" id="Seg_2263" s="T342">PKZ_196X_FrogPrincess_flk.087 (087)</ta>
            <ta e="T349" id="Seg_2264" s="T345">PKZ_196X_FrogPrincess_flk.088 (088)</ta>
            <ta e="T351" id="Seg_2265" s="T349">PKZ_196X_FrogPrincess_flk.089 (089)</ta>
            <ta e="T358" id="Seg_2266" s="T351">PKZ_196X_FrogPrincess_flk.090 (090)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2267" s="T0">Onʼiʔ koŋ amnobi. </ta>
            <ta e="T7" id="Seg_2268" s="T3">Dĭn nagur nʼizeŋdə ibiʔi. </ta>
            <ta e="T15" id="Seg_2269" s="T7">Dĭgəttə dĭ măndə nʼizeŋdə: "Nada šiʔnʼileʔ monoʔkosʼtə nezeŋ. </ta>
            <ta e="T21" id="Seg_2270" s="T15">Štobɨ măna kuzittə (š-) šiʔnʼileʔ esseŋ". </ta>
            <ta e="T29" id="Seg_2271" s="T21">No, dĭzeŋ măndəʔi: "Nu monoʔkoʔ, šindi tănan kereʔ?" </ta>
            <ta e="T33" id="Seg_2272" s="T29">"Igeʔ multukʔi i kangaʔ! </ta>
            <ta e="T35" id="Seg_2273" s="T33">(Dʼiʔtəgeʔ) strʼelaʔi!" </ta>
            <ta e="T39" id="Seg_2274" s="T35">Dĭgəttə (diʔnə=) dĭzeŋ kambiʔi. </ta>
            <ta e="T41" id="Seg_2275" s="T39">Onʼiʔ dʼiʔtəbi. </ta>
            <ta e="T45" id="Seg_2276" s="T41">Saʔməluʔpi (kup-) kupʼestə, turanə. </ta>
            <ta e="T47" id="Seg_2277" s="T45">Onʼiʔ dʼiʔtəbi. </ta>
            <ta e="T49" id="Seg_2278" s="T47">Saʔməluʔpi ((PAUSE))… </ta>
            <ta e="T50" id="Seg_2279" s="T49">[KA:] Боярский. </ta>
            <ta e="T52" id="Seg_2280" s="T50">[PKZ:] băjarskəj dvărestə. </ta>
            <ta e="T55" id="Seg_2281" s="T52">A onʼiʔ dʼiʔpi. </ta>
            <ta e="T57" id="Seg_2282" s="T55">Kuŋgeŋ kalla dʼürbi. </ta>
            <ta e="T62" id="Seg_2283" s="T57">Dĭgəttə dĭ kambi, kambi, šonəga. </ta>
            <ta e="T65" id="Seg_2284" s="T62">Bălotăgən amnolaʔbə lʼaguškadə. </ta>
            <ta e="T70" id="Seg_2285" s="T65">I strʼela (dĭʔnə= u-) dĭʔən. </ta>
            <ta e="T75" id="Seg_2286" s="T70">Dĭ măndə:" Deʔ măna strʼela!" </ta>
            <ta e="T79" id="Seg_2287" s="T75">"A tăn,— dĭ măndə. </ta>
            <ta e="T82" id="Seg_2288" s="T79">Iʔ măna tibinə!" </ta>
            <ta e="T87" id="Seg_2289" s="T82">"Da kăde măn tănan ilem? </ta>
            <ta e="T90" id="Seg_2290" s="T87">Tăn vedʼ lʼaguška". </ta>
            <ta e="T97" id="Seg_2291" s="T90">"Nu dăk, (tăn=) tăn sudʼbal, it măna!" </ta>
            <ta e="T100" id="Seg_2292" s="T97">Dĭgəttə dĭ ibi. </ta>
            <ta e="T102" id="Seg_2293" s="T100">Maːndə deʔpi. </ta>
            <ta e="T107" id="Seg_2294" s="T102">Dĭgəttə (abat- al-) abi svadʼbaʔi. </ta>
            <ta e="T111" id="Seg_2295" s="T107">Dĭgəttə mămbi: šödörjoʔ kujnegəʔi. </ta>
            <ta e="T116" id="Seg_2296" s="T111">Dĭgəttə (šonə-) šobi Vanʼuška maːndə. </ta>
            <ta e="T120" id="Seg_2297" s="T116">I ĭmbidə ej dʼăbaktəriam. </ta>
            <ta e="T125" id="Seg_2298" s="T120">"Ĭmbi tăn ĭmbidə ej dʼăbaktəria?" </ta>
            <ta e="T130" id="Seg_2299" s="T125">"Da abam mămbi kujnek šöʔsittə". </ta>
            <ta e="T137" id="Seg_2300" s="T130">"Iʔbeʔ, (kunoluʔ-) kunolaʔ, a (e-) kujnek moləj". </ta>
            <ta e="T140" id="Seg_2301" s="T137">Dĭgəttə dĭ kunolluʔpi. </ta>
            <ta e="T144" id="Seg_2302" s="T140">A dĭ nʼiʔdə kalla dʼürbi. </ta>
            <ta e="T146" id="Seg_2303" s="T144">Kubat kürbi. </ta>
            <ta e="T150" id="Seg_2304" s="T146">Davaj kirgarzittə:" Šöʔkeʔ măna! </ta>
            <ta e="T154" id="Seg_2305" s="T150">Kujnek abanə, kuvas, jakšə". </ta>
            <ta e="T158" id="Seg_2306" s="T154">Dĭgəttə bazoʔ kubabə šerbi. </ta>
            <ta e="T161" id="Seg_2307" s="T158">(Tʼuna-) Turanə šobi. </ta>
            <ta e="T164" id="Seg_2308" s="T161">Kujnektə embi stolbə. </ta>
            <ta e="T168" id="Seg_2309" s="T164">Stolbə, dĭgəttə uʔbdəbi tibit. </ta>
            <ta e="T171" id="Seg_2310" s="T168">"No, it kujnek! </ta>
            <ta e="T173" id="Seg_2311" s="T171">Kundə abandə!" </ta>
            <ta e="T178" id="Seg_2312" s="T173">Dĭ (kun-) kambi, abat ibi. </ta>
            <ta e="T186" id="Seg_2313" s="T178">"Staršij nʼində kujnek, dĭ tolʼkă togonorzittə dĭ kujnektən. </ta>
            <ta e="T195" id="Seg_2314" s="T186">A baška nʼin, a dö kujnek tolʼkă multʼanə mĭzittə". </ta>
            <ta e="T199" id="Seg_2315" s="T195">A dĭ nʼi mĭbi. </ta>
            <ta e="T207" id="Seg_2316" s="T199">"A (dö= dö=) dĭ kujnek tolʼkă (sarskən) amnozittə". </ta>
            <ta e="T208" id="Seg_2317" s="T207">Kabarləj. </ta>
            <ta e="T214" id="Seg_2318" s="T208">((DMG)) Dĭn abat ildə iʔgö oʔbdəbi. </ta>
            <ta e="T216" id="Seg_2319" s="T214">Stoldə uʔbdəbi. </ta>
            <ta e="T220" id="Seg_2320" s="T216">Kăštəbi nʼizeŋ, i nezeŋ. </ta>
            <ta e="T224" id="Seg_2321" s="T220">Dĭgəttə šobiʔi, amnobiʔi stoldə. </ta>
            <ta e="T226" id="Seg_2322" s="T224">Uja ambiʔi. </ta>
            <ta e="T229" id="Seg_2323" s="T226">I leʔi (nda). </ta>
            <ta e="T236" id="Seg_2324" s="T229">Dĭgəttə šobiʔi dĭn kagazaŋdə, mălliaʔi:" Šindidə šonəga". </ta>
            <ta e="T245" id="Seg_2325" s="T236">A (dĭ= m- dĭm=) dĭ măndə:" Măn lʼaguškam šonəga". </ta>
            <ta e="T250" id="Seg_2326" s="T245">Dĭgəttə dĭ šobi, kuvas bar. </ta>
            <ta e="T253" id="Seg_2327" s="T250">Dĭgəttə dĭzeŋ amnolbiʔi. </ta>
            <ta e="T256" id="Seg_2328" s="T253">Dĭ bar amnaʔbə. </ta>
            <ta e="T260" id="Seg_2329" s="T256">I leʔi rukaftə elleʔbə. </ta>
            <ta e="T263" id="Seg_2330" s="T260">Ara rukaftə kămlia. </ta>
            <ta e="T266" id="Seg_2331" s="T263">Čaj dĭbər kămlia. </ta>
            <ta e="T268" id="Seg_2332" s="T266">Dĭgəttə ambiʔi. </ta>
            <ta e="T270" id="Seg_2333" s="T268">Kambiʔi suʔmisʼtə. </ta>
            <ta e="T272" id="Seg_2334" s="T270">Sʼarzittə garmonnʼaziʔ. </ta>
            <ta e="T275" id="Seg_2335" s="T272">Dĭgəttə dĭ suʔmiluʔpi. </ta>
            <ta e="T277" id="Seg_2336" s="T275">Bü molaːmbi. </ta>
            <ta e="T283" id="Seg_2337" s="T277">(Dĭgət-) Dĭgəttə onʼiʔ udandə ((NOISE)) (măxnula) măxnula. </ta>
            <ta e="T285" id="Seg_2338" s="T283">Bü molaːmbi. </ta>
            <ta e="T288" id="Seg_2339" s="T285">Tsar bar nereʔluʔpi. </ta>
            <ta e="T291" id="Seg_2340" s="T288">Baška udandə măxnula. </ta>
            <ta e="T294" id="Seg_2341" s="T291">Dĭgəttə nabəʔi šoluʔpiʔi. </ta>
            <ta e="T297" id="Seg_2342" s="T294">Dĭgəttə bar dʼürleʔpi. </ta>
            <ta e="T303" id="Seg_2343" s="T297">Dĭgəttə dĭzeŋ (bareʔ) nezeŋ udatsiʔ barəʔluʔpi. </ta>
            <ta e="T305" id="Seg_2344" s="T303">Bar ildə. </ta>
            <ta e="T311" id="Seg_2345" s="T305">(i leziʔ) kadəldə bar toʔnarbi (bil-). </ta>
            <ta e="T318" id="Seg_2346" s="T311">Büziʔ bar sʼimattə bar bü (kăn-) kămnabiʔi. </ta>
            <ta e="T319" id="Seg_2347" s="T318">Kabarləj. </ta>
            <ta e="T323" id="Seg_2348" s="T319">Dĭgəttə dĭ nuʔməluʔpi maːʔndə. </ta>
            <ta e="T330" id="Seg_2349" s="T323">Kubabə kubi da ibi, šünə (emn-) embi. </ta>
            <ta e="T333" id="Seg_2350" s="T330">Dĭ bar neniluʔpi. </ta>
            <ta e="T337" id="Seg_2351" s="T333">Dĭgəttə dĭ šobi maːndə. </ta>
            <ta e="T342" id="Seg_2352" s="T337">"Ugaːndə tăn ej jakšə abial! </ta>
            <ta e="T345" id="Seg_2353" s="T342">Tüj măn kalla dʼürləm. </ta>
            <ta e="T349" id="Seg_2354" s="T345">A tăn măna măndərlal. </ta>
            <ta e="T351" id="Seg_2355" s="T349">Dĭ kalla dʼürbi. </ta>
            <ta e="T358" id="Seg_2356" s="T351">A dĭ bar dʼorbi, dʼorbi, dĭgəttə (kambi). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2357" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_2358" s="T1">koŋ</ta>
            <ta e="T3" id="Seg_2359" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_2360" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_2361" s="T4">nagur</ta>
            <ta e="T6" id="Seg_2362" s="T5">nʼi-zeŋ-də</ta>
            <ta e="T7" id="Seg_2363" s="T6">i-bi-ʔi</ta>
            <ta e="T8" id="Seg_2364" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_2365" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_2366" s="T9">măn-də</ta>
            <ta e="T11" id="Seg_2367" s="T10">nʼi-zeŋ-də</ta>
            <ta e="T12" id="Seg_2368" s="T11">nada</ta>
            <ta e="T13" id="Seg_2369" s="T12">šiʔnʼileʔ</ta>
            <ta e="T14" id="Seg_2370" s="T13">monoʔko-sʼtə</ta>
            <ta e="T15" id="Seg_2371" s="T14">ne-zeŋ</ta>
            <ta e="T16" id="Seg_2372" s="T15">štobɨ</ta>
            <ta e="T17" id="Seg_2373" s="T16">măna</ta>
            <ta e="T18" id="Seg_2374" s="T17">ku-zittə</ta>
            <ta e="T20" id="Seg_2375" s="T19">šiʔnʼileʔ</ta>
            <ta e="T21" id="Seg_2376" s="T20">es-seŋ</ta>
            <ta e="T22" id="Seg_2377" s="T21">no</ta>
            <ta e="T23" id="Seg_2378" s="T22">dĭ-zeŋ</ta>
            <ta e="T24" id="Seg_2379" s="T23">măn-də-ʔi</ta>
            <ta e="T25" id="Seg_2380" s="T24">nu</ta>
            <ta e="T26" id="Seg_2381" s="T25">monoʔko-ʔ</ta>
            <ta e="T27" id="Seg_2382" s="T26">šindi</ta>
            <ta e="T28" id="Seg_2383" s="T27">tănan</ta>
            <ta e="T29" id="Seg_2384" s="T28">kereʔ</ta>
            <ta e="T30" id="Seg_2385" s="T29">i-geʔ</ta>
            <ta e="T31" id="Seg_2386" s="T30">multuk-ʔi</ta>
            <ta e="T32" id="Seg_2387" s="T31">i</ta>
            <ta e="T33" id="Seg_2388" s="T32">kan-gaʔ</ta>
            <ta e="T34" id="Seg_2389" s="T33">dʼiʔtə-geʔ</ta>
            <ta e="T35" id="Seg_2390" s="T34">strʼela-ʔi</ta>
            <ta e="T36" id="Seg_2391" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_2392" s="T36">diʔ-nə</ta>
            <ta e="T38" id="Seg_2393" s="T37">dĭ-zeŋ</ta>
            <ta e="T39" id="Seg_2394" s="T38">kam-bi-ʔi</ta>
            <ta e="T40" id="Seg_2395" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_2396" s="T40">dʼiʔtə-bi</ta>
            <ta e="T42" id="Seg_2397" s="T41">saʔmə-luʔ-pi</ta>
            <ta e="T44" id="Seg_2398" s="T43">kupʼes-tə</ta>
            <ta e="T45" id="Seg_2399" s="T44">tura-nə</ta>
            <ta e="T46" id="Seg_2400" s="T45">onʼiʔ</ta>
            <ta e="T47" id="Seg_2401" s="T46">dʼiʔtə-bi</ta>
            <ta e="T48" id="Seg_2402" s="T47">saʔmə-luʔ-pi</ta>
            <ta e="T52" id="Seg_2403" s="T51">dvăres-tə</ta>
            <ta e="T53" id="Seg_2404" s="T52">a</ta>
            <ta e="T54" id="Seg_2405" s="T53">onʼiʔ</ta>
            <ta e="T55" id="Seg_2406" s="T54">dʼiʔ-pi</ta>
            <ta e="T56" id="Seg_2407" s="T55">kuŋge-ŋ</ta>
            <ta e="T359" id="Seg_2408" s="T56">kal-la</ta>
            <ta e="T57" id="Seg_2409" s="T359">dʼür-bi</ta>
            <ta e="T58" id="Seg_2410" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_2411" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_2412" s="T59">kam-bi</ta>
            <ta e="T61" id="Seg_2413" s="T60">kam-bi</ta>
            <ta e="T62" id="Seg_2414" s="T61">šonə-ga</ta>
            <ta e="T63" id="Seg_2415" s="T62">bălotă-gən</ta>
            <ta e="T64" id="Seg_2416" s="T63">amno-laʔbə</ta>
            <ta e="T65" id="Seg_2417" s="T64">lʼaguška-də</ta>
            <ta e="T66" id="Seg_2418" s="T65">i</ta>
            <ta e="T67" id="Seg_2419" s="T66">strʼela</ta>
            <ta e="T68" id="Seg_2420" s="T67">dĭʔ-nə</ta>
            <ta e="T70" id="Seg_2421" s="T69">dĭʔən</ta>
            <ta e="T71" id="Seg_2422" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_2423" s="T71">măn-də</ta>
            <ta e="T73" id="Seg_2424" s="T72">de-ʔ</ta>
            <ta e="T74" id="Seg_2425" s="T73">măna</ta>
            <ta e="T75" id="Seg_2426" s="T74">strʼela</ta>
            <ta e="T76" id="Seg_2427" s="T75">a</ta>
            <ta e="T77" id="Seg_2428" s="T76">tăn</ta>
            <ta e="T78" id="Seg_2429" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_2430" s="T78">măn-də</ta>
            <ta e="T80" id="Seg_2431" s="T79">i-ʔ</ta>
            <ta e="T81" id="Seg_2432" s="T80">măna</ta>
            <ta e="T82" id="Seg_2433" s="T81">tibi-nə</ta>
            <ta e="T83" id="Seg_2434" s="T82">da</ta>
            <ta e="T84" id="Seg_2435" s="T83">kăde</ta>
            <ta e="T85" id="Seg_2436" s="T84">măn</ta>
            <ta e="T86" id="Seg_2437" s="T85">tănan</ta>
            <ta e="T87" id="Seg_2438" s="T86">i-le-m</ta>
            <ta e="T88" id="Seg_2439" s="T87">tăn</ta>
            <ta e="T89" id="Seg_2440" s="T88">vedʼ</ta>
            <ta e="T90" id="Seg_2441" s="T89">lʼaguška</ta>
            <ta e="T91" id="Seg_2442" s="T90">nu</ta>
            <ta e="T92" id="Seg_2443" s="T91">dăk</ta>
            <ta e="T93" id="Seg_2444" s="T92">tăn</ta>
            <ta e="T94" id="Seg_2445" s="T93">tăn</ta>
            <ta e="T95" id="Seg_2446" s="T94">sudʼba-l</ta>
            <ta e="T96" id="Seg_2447" s="T95">i-t</ta>
            <ta e="T97" id="Seg_2448" s="T96">măna</ta>
            <ta e="T98" id="Seg_2449" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_2450" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_2451" s="T99">i-bi</ta>
            <ta e="T101" id="Seg_2452" s="T100">ma-ndə</ta>
            <ta e="T102" id="Seg_2453" s="T101">deʔ-pi</ta>
            <ta e="T103" id="Seg_2454" s="T102">dĭgəttə</ta>
            <ta e="T106" id="Seg_2455" s="T105">a-bi</ta>
            <ta e="T107" id="Seg_2456" s="T106">svadʼba-ʔi</ta>
            <ta e="T108" id="Seg_2457" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_2458" s="T108">măm-bi</ta>
            <ta e="T110" id="Seg_2459" s="T109">šödör-joʔ</ta>
            <ta e="T111" id="Seg_2460" s="T110">kujneg-əʔi</ta>
            <ta e="T112" id="Seg_2461" s="T111">dĭgəttə</ta>
            <ta e="T114" id="Seg_2462" s="T113">šo-bi</ta>
            <ta e="T115" id="Seg_2463" s="T114">Vanʼuška</ta>
            <ta e="T116" id="Seg_2464" s="T115">ma-ndə</ta>
            <ta e="T117" id="Seg_2465" s="T116">i</ta>
            <ta e="T118" id="Seg_2466" s="T117">ĭmbi=də</ta>
            <ta e="T119" id="Seg_2467" s="T118">ej</ta>
            <ta e="T120" id="Seg_2468" s="T119">dʼăbaktər-ia-m</ta>
            <ta e="T121" id="Seg_2469" s="T120">ĭmbi</ta>
            <ta e="T122" id="Seg_2470" s="T121">tăn</ta>
            <ta e="T123" id="Seg_2471" s="T122">ĭmbi=də</ta>
            <ta e="T124" id="Seg_2472" s="T123">ej</ta>
            <ta e="T125" id="Seg_2473" s="T124">dʼăbaktər-ia</ta>
            <ta e="T126" id="Seg_2474" s="T125">da</ta>
            <ta e="T127" id="Seg_2475" s="T126">aba-m</ta>
            <ta e="T128" id="Seg_2476" s="T127">măm-bi</ta>
            <ta e="T129" id="Seg_2477" s="T128">kujnek</ta>
            <ta e="T130" id="Seg_2478" s="T129">šöʔ-sittə</ta>
            <ta e="T131" id="Seg_2479" s="T130">iʔb-eʔ</ta>
            <ta e="T133" id="Seg_2480" s="T132">kunol-a-ʔ</ta>
            <ta e="T134" id="Seg_2481" s="T133">a</ta>
            <ta e="T136" id="Seg_2482" s="T135">kujnek</ta>
            <ta e="T137" id="Seg_2483" s="T136">mo-lə-j</ta>
            <ta e="T138" id="Seg_2484" s="T137">dĭgəttə</ta>
            <ta e="T139" id="Seg_2485" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_2486" s="T139">kunol-luʔ-pi</ta>
            <ta e="T141" id="Seg_2487" s="T140">a</ta>
            <ta e="T142" id="Seg_2488" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_2489" s="T142">nʼiʔdə</ta>
            <ta e="T360" id="Seg_2490" s="T143">kal-la</ta>
            <ta e="T144" id="Seg_2491" s="T360">dʼür-bi</ta>
            <ta e="T145" id="Seg_2492" s="T144">kuba-t</ta>
            <ta e="T146" id="Seg_2493" s="T145">kür-bi</ta>
            <ta e="T147" id="Seg_2494" s="T146">davaj</ta>
            <ta e="T148" id="Seg_2495" s="T147">kirgar-zittə</ta>
            <ta e="T149" id="Seg_2496" s="T148">šöʔ-keʔ</ta>
            <ta e="T150" id="Seg_2497" s="T149">măna</ta>
            <ta e="T151" id="Seg_2498" s="T150">kujnek</ta>
            <ta e="T152" id="Seg_2499" s="T151">aba-nə</ta>
            <ta e="T153" id="Seg_2500" s="T152">kuvas</ta>
            <ta e="T154" id="Seg_2501" s="T153">jakšə</ta>
            <ta e="T155" id="Seg_2502" s="T154">dĭgəttə</ta>
            <ta e="T156" id="Seg_2503" s="T155">bazoʔ</ta>
            <ta e="T157" id="Seg_2504" s="T156">kuba-bə</ta>
            <ta e="T158" id="Seg_2505" s="T157">šer-bi</ta>
            <ta e="T160" id="Seg_2506" s="T159">tura-nə</ta>
            <ta e="T161" id="Seg_2507" s="T160">šo-bi</ta>
            <ta e="T162" id="Seg_2508" s="T161">kujnek-tə</ta>
            <ta e="T163" id="Seg_2509" s="T162">em-bi</ta>
            <ta e="T164" id="Seg_2510" s="T163">stol-bə</ta>
            <ta e="T165" id="Seg_2511" s="T164">stol-bə</ta>
            <ta e="T166" id="Seg_2512" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_2513" s="T166">uʔbdə-bi</ta>
            <ta e="T168" id="Seg_2514" s="T167">tibi-t</ta>
            <ta e="T169" id="Seg_2515" s="T168">no</ta>
            <ta e="T170" id="Seg_2516" s="T169">i-t</ta>
            <ta e="T171" id="Seg_2517" s="T170">kujnek</ta>
            <ta e="T172" id="Seg_2518" s="T171">kun-də</ta>
            <ta e="T173" id="Seg_2519" s="T172">aba-ndə</ta>
            <ta e="T174" id="Seg_2520" s="T173">dĭ</ta>
            <ta e="T176" id="Seg_2521" s="T175">kam-bi</ta>
            <ta e="T177" id="Seg_2522" s="T176">aba-t</ta>
            <ta e="T178" id="Seg_2523" s="T177">i-bi</ta>
            <ta e="T179" id="Seg_2524" s="T178">staršij</ta>
            <ta e="T180" id="Seg_2525" s="T179">nʼi-ndə</ta>
            <ta e="T181" id="Seg_2526" s="T180">kujnek</ta>
            <ta e="T182" id="Seg_2527" s="T181">dĭ</ta>
            <ta e="T183" id="Seg_2528" s="T182">tolʼkă</ta>
            <ta e="T184" id="Seg_2529" s="T183">togonor-zittə</ta>
            <ta e="T185" id="Seg_2530" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_2531" s="T185">kujnek-tən</ta>
            <ta e="T187" id="Seg_2532" s="T186">a</ta>
            <ta e="T188" id="Seg_2533" s="T187">baška</ta>
            <ta e="T189" id="Seg_2534" s="T188">nʼi-n</ta>
            <ta e="T190" id="Seg_2535" s="T189">a</ta>
            <ta e="T191" id="Seg_2536" s="T190">dö</ta>
            <ta e="T192" id="Seg_2537" s="T191">kujnek</ta>
            <ta e="T193" id="Seg_2538" s="T192">tolʼkă</ta>
            <ta e="T194" id="Seg_2539" s="T193">multʼa-nə</ta>
            <ta e="T195" id="Seg_2540" s="T194">mĭ-zittə</ta>
            <ta e="T196" id="Seg_2541" s="T195">a</ta>
            <ta e="T197" id="Seg_2542" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_2543" s="T197">nʼi</ta>
            <ta e="T199" id="Seg_2544" s="T198">mĭ-bi</ta>
            <ta e="T200" id="Seg_2545" s="T199">a</ta>
            <ta e="T201" id="Seg_2546" s="T200">dö</ta>
            <ta e="T202" id="Seg_2547" s="T201">dö</ta>
            <ta e="T203" id="Seg_2548" s="T202">dĭ</ta>
            <ta e="T204" id="Seg_2549" s="T203">kujnek</ta>
            <ta e="T205" id="Seg_2550" s="T204">tolʼkă</ta>
            <ta e="T206" id="Seg_2551" s="T205">sarskən</ta>
            <ta e="T207" id="Seg_2552" s="T206">amno-zittə</ta>
            <ta e="T208" id="Seg_2553" s="T207">kabarləj</ta>
            <ta e="T210" id="Seg_2554" s="T209">dĭ-n</ta>
            <ta e="T211" id="Seg_2555" s="T210">aba-t</ta>
            <ta e="T212" id="Seg_2556" s="T211">il-də</ta>
            <ta e="T213" id="Seg_2557" s="T212">iʔgö</ta>
            <ta e="T214" id="Seg_2558" s="T213">oʔbdə-bi</ta>
            <ta e="T215" id="Seg_2559" s="T214">stol-də</ta>
            <ta e="T216" id="Seg_2560" s="T215">uʔbdə-bi</ta>
            <ta e="T217" id="Seg_2561" s="T216">kăštə-bi</ta>
            <ta e="T218" id="Seg_2562" s="T217">nʼi-zeŋ</ta>
            <ta e="T219" id="Seg_2563" s="T218">i</ta>
            <ta e="T220" id="Seg_2564" s="T219">ne-zeŋ</ta>
            <ta e="T221" id="Seg_2565" s="T220">dĭgəttə</ta>
            <ta e="T222" id="Seg_2566" s="T221">šo-bi-ʔi</ta>
            <ta e="T223" id="Seg_2567" s="T222">amno-bi-ʔi</ta>
            <ta e="T224" id="Seg_2568" s="T223">stol-də</ta>
            <ta e="T225" id="Seg_2569" s="T224">uja</ta>
            <ta e="T226" id="Seg_2570" s="T225">am-bi-ʔi</ta>
            <ta e="T227" id="Seg_2571" s="T226">i</ta>
            <ta e="T228" id="Seg_2572" s="T227">le-ʔi</ta>
            <ta e="T229" id="Seg_2573" s="T228">nda</ta>
            <ta e="T230" id="Seg_2574" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_2575" s="T230">šo-bi-ʔi</ta>
            <ta e="T232" id="Seg_2576" s="T231">dĭ-n</ta>
            <ta e="T233" id="Seg_2577" s="T232">kaga-zaŋ-də</ta>
            <ta e="T234" id="Seg_2578" s="T233">măl-lia-ʔi</ta>
            <ta e="T235" id="Seg_2579" s="T234">šindi=də</ta>
            <ta e="T236" id="Seg_2580" s="T235">šonə-ga</ta>
            <ta e="T237" id="Seg_2581" s="T236">a</ta>
            <ta e="T238" id="Seg_2582" s="T237">dĭ</ta>
            <ta e="T240" id="Seg_2583" s="T239">dĭ-m</ta>
            <ta e="T241" id="Seg_2584" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_2585" s="T241">măn-də</ta>
            <ta e="T243" id="Seg_2586" s="T242">măn</ta>
            <ta e="T244" id="Seg_2587" s="T243">lʼaguška-m</ta>
            <ta e="T245" id="Seg_2588" s="T244">šonə-ga</ta>
            <ta e="T246" id="Seg_2589" s="T245">dĭgəttə</ta>
            <ta e="T247" id="Seg_2590" s="T246">dĭ</ta>
            <ta e="T248" id="Seg_2591" s="T247">šo-bi</ta>
            <ta e="T249" id="Seg_2592" s="T248">kuvas</ta>
            <ta e="T250" id="Seg_2593" s="T249">bar</ta>
            <ta e="T251" id="Seg_2594" s="T250">dĭgəttə</ta>
            <ta e="T252" id="Seg_2595" s="T251">dĭ-zeŋ</ta>
            <ta e="T253" id="Seg_2596" s="T252">amnəl-bi-ʔi</ta>
            <ta e="T254" id="Seg_2597" s="T253">dĭ</ta>
            <ta e="T255" id="Seg_2598" s="T254">bar</ta>
            <ta e="T256" id="Seg_2599" s="T255">am-naʔbə</ta>
            <ta e="T257" id="Seg_2600" s="T256">i</ta>
            <ta e="T258" id="Seg_2601" s="T257">le-ʔi</ta>
            <ta e="T259" id="Seg_2602" s="T258">rukaf-tə</ta>
            <ta e="T260" id="Seg_2603" s="T259">el-leʔbə</ta>
            <ta e="T261" id="Seg_2604" s="T260">ara</ta>
            <ta e="T262" id="Seg_2605" s="T261">rukaf-tə</ta>
            <ta e="T263" id="Seg_2606" s="T262">kăm-lia</ta>
            <ta e="T264" id="Seg_2607" s="T263">čaj</ta>
            <ta e="T265" id="Seg_2608" s="T264">dĭbər</ta>
            <ta e="T266" id="Seg_2609" s="T265">kăm-lia</ta>
            <ta e="T267" id="Seg_2610" s="T266">dĭgəttə</ta>
            <ta e="T268" id="Seg_2611" s="T267">am-bi-ʔi</ta>
            <ta e="T269" id="Seg_2612" s="T268">kam-bi-ʔi</ta>
            <ta e="T270" id="Seg_2613" s="T269">suʔmi-sʼtə</ta>
            <ta e="T271" id="Seg_2614" s="T270">sʼar-zittə</ta>
            <ta e="T272" id="Seg_2615" s="T271">garmonnʼa-ziʔ</ta>
            <ta e="T273" id="Seg_2616" s="T272">dĭgəttə</ta>
            <ta e="T274" id="Seg_2617" s="T273">dĭ</ta>
            <ta e="T275" id="Seg_2618" s="T274">suʔmi-luʔ-pi</ta>
            <ta e="T276" id="Seg_2619" s="T275">bü</ta>
            <ta e="T277" id="Seg_2620" s="T276">mo-laːm-bi</ta>
            <ta e="T279" id="Seg_2621" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_2622" s="T279">onʼiʔ</ta>
            <ta e="T281" id="Seg_2623" s="T280">uda-ndə</ta>
            <ta e="T284" id="Seg_2624" s="T283">bü</ta>
            <ta e="T285" id="Seg_2625" s="T284">mo-laːm-bi</ta>
            <ta e="T286" id="Seg_2626" s="T285">tsar</ta>
            <ta e="T287" id="Seg_2627" s="T286">bar</ta>
            <ta e="T288" id="Seg_2628" s="T287">nereʔ-luʔ-pi</ta>
            <ta e="T289" id="Seg_2629" s="T288">baška</ta>
            <ta e="T290" id="Seg_2630" s="T289">uda-ndə</ta>
            <ta e="T292" id="Seg_2631" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_2632" s="T292">nabə-ʔi</ta>
            <ta e="T294" id="Seg_2633" s="T293">šo-luʔ-pi-ʔi</ta>
            <ta e="T295" id="Seg_2634" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_2635" s="T295">bar</ta>
            <ta e="T297" id="Seg_2636" s="T296">dʼür-leʔ-pi</ta>
            <ta e="T298" id="Seg_2637" s="T297">dĭgəttə</ta>
            <ta e="T299" id="Seg_2638" s="T298">dĭ-zeŋ</ta>
            <ta e="T300" id="Seg_2639" s="T299">bareʔ</ta>
            <ta e="T301" id="Seg_2640" s="T300">ne-zeŋ</ta>
            <ta e="T302" id="Seg_2641" s="T301">uda-t-siʔ</ta>
            <ta e="T303" id="Seg_2642" s="T302">barəʔ-luʔ-pi</ta>
            <ta e="T304" id="Seg_2643" s="T303">bar</ta>
            <ta e="T305" id="Seg_2644" s="T304">il-də</ta>
            <ta e="T306" id="Seg_2645" s="T305">i</ta>
            <ta e="T307" id="Seg_2646" s="T306">le-ziʔ</ta>
            <ta e="T308" id="Seg_2647" s="T307">kadəl-də</ta>
            <ta e="T309" id="Seg_2648" s="T308">bar</ta>
            <ta e="T310" id="Seg_2649" s="T309">toʔ-nar-bi</ta>
            <ta e="T312" id="Seg_2650" s="T311">bü-ziʔ</ta>
            <ta e="T313" id="Seg_2651" s="T312">bar</ta>
            <ta e="T314" id="Seg_2652" s="T313">sʼima-t-tə</ta>
            <ta e="T315" id="Seg_2653" s="T314">bar</ta>
            <ta e="T316" id="Seg_2654" s="T315">bü</ta>
            <ta e="T318" id="Seg_2655" s="T317">kămna-bi-ʔi</ta>
            <ta e="T319" id="Seg_2656" s="T318">kabarləj</ta>
            <ta e="T320" id="Seg_2657" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2658" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_2659" s="T321">nuʔmə-luʔ-pi</ta>
            <ta e="T323" id="Seg_2660" s="T322">maːʔ-ndə</ta>
            <ta e="T324" id="Seg_2661" s="T323">kuba-bə</ta>
            <ta e="T325" id="Seg_2662" s="T324">ku-bi</ta>
            <ta e="T326" id="Seg_2663" s="T325">da</ta>
            <ta e="T327" id="Seg_2664" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_2665" s="T327">šü-nə</ta>
            <ta e="T330" id="Seg_2666" s="T329">em-bi</ta>
            <ta e="T331" id="Seg_2667" s="T330">dĭ</ta>
            <ta e="T332" id="Seg_2668" s="T331">bar</ta>
            <ta e="T333" id="Seg_2669" s="T332">neni-luʔ-pi</ta>
            <ta e="T334" id="Seg_2670" s="T333">dĭgəttə</ta>
            <ta e="T335" id="Seg_2671" s="T334">dĭ</ta>
            <ta e="T336" id="Seg_2672" s="T335">šo-bi</ta>
            <ta e="T337" id="Seg_2673" s="T336">ma-ndə</ta>
            <ta e="T338" id="Seg_2674" s="T337">ugaːndə</ta>
            <ta e="T339" id="Seg_2675" s="T338">tăn</ta>
            <ta e="T340" id="Seg_2676" s="T339">ej</ta>
            <ta e="T341" id="Seg_2677" s="T340">jakšə</ta>
            <ta e="T342" id="Seg_2678" s="T341">a-bia-l</ta>
            <ta e="T343" id="Seg_2679" s="T342">tüj</ta>
            <ta e="T344" id="Seg_2680" s="T343">măn</ta>
            <ta e="T361" id="Seg_2681" s="T344">kal-la</ta>
            <ta e="T345" id="Seg_2682" s="T361">dʼür-lə-m</ta>
            <ta e="T346" id="Seg_2683" s="T345">a</ta>
            <ta e="T347" id="Seg_2684" s="T346">tăn</ta>
            <ta e="T348" id="Seg_2685" s="T347">măna</ta>
            <ta e="T349" id="Seg_2686" s="T348">măndə-r-la-l</ta>
            <ta e="T350" id="Seg_2687" s="T349">dĭ</ta>
            <ta e="T362" id="Seg_2688" s="T350">kal-la</ta>
            <ta e="T351" id="Seg_2689" s="T362">dʼür-bi</ta>
            <ta e="T352" id="Seg_2690" s="T351">a</ta>
            <ta e="T353" id="Seg_2691" s="T352">dĭ</ta>
            <ta e="T354" id="Seg_2692" s="T353">bar</ta>
            <ta e="T355" id="Seg_2693" s="T354">dʼor-bi</ta>
            <ta e="T356" id="Seg_2694" s="T355">dʼor-bi</ta>
            <ta e="T357" id="Seg_2695" s="T356">dĭgəttə</ta>
            <ta e="T358" id="Seg_2696" s="T357">kam-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2697" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_2698" s="T1">koŋ</ta>
            <ta e="T3" id="Seg_2699" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_2700" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_2701" s="T4">nagur</ta>
            <ta e="T6" id="Seg_2702" s="T5">nʼi-zAŋ-də</ta>
            <ta e="T7" id="Seg_2703" s="T6">i-bi-jəʔ</ta>
            <ta e="T8" id="Seg_2704" s="T7">dĭgəttə</ta>
            <ta e="T9" id="Seg_2705" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_2706" s="T9">măn-ntə</ta>
            <ta e="T11" id="Seg_2707" s="T10">nʼi-zAŋ-Tə</ta>
            <ta e="T12" id="Seg_2708" s="T11">nadə</ta>
            <ta e="T13" id="Seg_2709" s="T12">šiʔnʼileʔ</ta>
            <ta e="T14" id="Seg_2710" s="T13">monoʔko-zittə</ta>
            <ta e="T15" id="Seg_2711" s="T14">ne-zAŋ</ta>
            <ta e="T16" id="Seg_2712" s="T15">štobɨ</ta>
            <ta e="T17" id="Seg_2713" s="T16">măna</ta>
            <ta e="T18" id="Seg_2714" s="T17">ku-zittə</ta>
            <ta e="T20" id="Seg_2715" s="T19">šiʔnʼileʔ</ta>
            <ta e="T21" id="Seg_2716" s="T20">ešši-zAŋ</ta>
            <ta e="T22" id="Seg_2717" s="T21">no</ta>
            <ta e="T23" id="Seg_2718" s="T22">dĭ-zAŋ</ta>
            <ta e="T24" id="Seg_2719" s="T23">măn-ntə-jəʔ</ta>
            <ta e="T25" id="Seg_2720" s="T24">nu</ta>
            <ta e="T26" id="Seg_2721" s="T25">monoʔko-ʔ</ta>
            <ta e="T27" id="Seg_2722" s="T26">šində</ta>
            <ta e="T28" id="Seg_2723" s="T27">tănan</ta>
            <ta e="T29" id="Seg_2724" s="T28">kereʔ</ta>
            <ta e="T30" id="Seg_2725" s="T29">i-KAʔ</ta>
            <ta e="T31" id="Seg_2726" s="T30">multuk-jəʔ</ta>
            <ta e="T32" id="Seg_2727" s="T31">i</ta>
            <ta e="T33" id="Seg_2728" s="T32">kan-KAʔ</ta>
            <ta e="T34" id="Seg_2729" s="T33">tʼit-KAʔ</ta>
            <ta e="T35" id="Seg_2730" s="T34">strela-jəʔ</ta>
            <ta e="T36" id="Seg_2731" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_2732" s="T36">dĭ-Tə</ta>
            <ta e="T38" id="Seg_2733" s="T37">dĭ-zAŋ</ta>
            <ta e="T39" id="Seg_2734" s="T38">kan-bi-jəʔ</ta>
            <ta e="T40" id="Seg_2735" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_2736" s="T40">tʼit-bi</ta>
            <ta e="T42" id="Seg_2737" s="T41">saʔmə-luʔbdə-bi</ta>
            <ta e="T44" id="Seg_2738" s="T43">kupʼes-Tə</ta>
            <ta e="T45" id="Seg_2739" s="T44">tura-Tə</ta>
            <ta e="T46" id="Seg_2740" s="T45">onʼiʔ</ta>
            <ta e="T47" id="Seg_2741" s="T46">tʼit-bi</ta>
            <ta e="T48" id="Seg_2742" s="T47">saʔmə-luʔbdə-bi</ta>
            <ta e="T52" id="Seg_2743" s="T51">dvăres-Tə</ta>
            <ta e="T53" id="Seg_2744" s="T52">a</ta>
            <ta e="T54" id="Seg_2745" s="T53">onʼiʔ</ta>
            <ta e="T55" id="Seg_2746" s="T54">tʼit-bi</ta>
            <ta e="T56" id="Seg_2747" s="T55">kuŋgə-ŋ</ta>
            <ta e="T359" id="Seg_2748" s="T56">kan-lAʔ</ta>
            <ta e="T57" id="Seg_2749" s="T359">tʼür-bi</ta>
            <ta e="T58" id="Seg_2750" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_2751" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_2752" s="T59">kan-bi</ta>
            <ta e="T61" id="Seg_2753" s="T60">kan-bi</ta>
            <ta e="T62" id="Seg_2754" s="T61">šonə-gA</ta>
            <ta e="T63" id="Seg_2755" s="T62">bălotă-Kən</ta>
            <ta e="T64" id="Seg_2756" s="T63">amno-laʔbə</ta>
            <ta e="T65" id="Seg_2757" s="T64">lʼaguška-də</ta>
            <ta e="T66" id="Seg_2758" s="T65">i</ta>
            <ta e="T67" id="Seg_2759" s="T66">strela</ta>
            <ta e="T68" id="Seg_2760" s="T67">dĭ-Tə</ta>
            <ta e="T70" id="Seg_2761" s="T69">dĭʔən</ta>
            <ta e="T71" id="Seg_2762" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_2763" s="T71">măn-ntə</ta>
            <ta e="T73" id="Seg_2764" s="T72">det-ʔ</ta>
            <ta e="T74" id="Seg_2765" s="T73">măna</ta>
            <ta e="T75" id="Seg_2766" s="T74">strela</ta>
            <ta e="T76" id="Seg_2767" s="T75">a</ta>
            <ta e="T77" id="Seg_2768" s="T76">tăn</ta>
            <ta e="T78" id="Seg_2769" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_2770" s="T78">măn-ntə</ta>
            <ta e="T80" id="Seg_2771" s="T79">i-ʔ</ta>
            <ta e="T81" id="Seg_2772" s="T80">măna</ta>
            <ta e="T82" id="Seg_2773" s="T81">tibi-Tə</ta>
            <ta e="T83" id="Seg_2774" s="T82">da</ta>
            <ta e="T84" id="Seg_2775" s="T83">kădaʔ</ta>
            <ta e="T85" id="Seg_2776" s="T84">măn</ta>
            <ta e="T86" id="Seg_2777" s="T85">tănan</ta>
            <ta e="T87" id="Seg_2778" s="T86">i-lV-m</ta>
            <ta e="T88" id="Seg_2779" s="T87">tăn</ta>
            <ta e="T89" id="Seg_2780" s="T88">vedʼ</ta>
            <ta e="T90" id="Seg_2781" s="T89">lʼaguška</ta>
            <ta e="T91" id="Seg_2782" s="T90">nu</ta>
            <ta e="T92" id="Seg_2783" s="T91">tak</ta>
            <ta e="T93" id="Seg_2784" s="T92">tăn</ta>
            <ta e="T94" id="Seg_2785" s="T93">tăn</ta>
            <ta e="T95" id="Seg_2786" s="T94">sudʼba-l</ta>
            <ta e="T96" id="Seg_2787" s="T95">i-t</ta>
            <ta e="T97" id="Seg_2788" s="T96">măna</ta>
            <ta e="T98" id="Seg_2789" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_2790" s="T98">dĭ</ta>
            <ta e="T100" id="Seg_2791" s="T99">i-bi</ta>
            <ta e="T101" id="Seg_2792" s="T100">maʔ-gəndə</ta>
            <ta e="T102" id="Seg_2793" s="T101">det-bi</ta>
            <ta e="T103" id="Seg_2794" s="T102">dĭgəttə</ta>
            <ta e="T106" id="Seg_2795" s="T105">a-bi</ta>
            <ta e="T107" id="Seg_2796" s="T106">-jəʔ</ta>
            <ta e="T108" id="Seg_2797" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_2798" s="T108">măn-bi</ta>
            <ta e="T110" id="Seg_2799" s="T109">šödör-t</ta>
            <ta e="T111" id="Seg_2800" s="T110">kujnek-jəʔ</ta>
            <ta e="T112" id="Seg_2801" s="T111">dĭgəttə</ta>
            <ta e="T114" id="Seg_2802" s="T113">šo-bi</ta>
            <ta e="T115" id="Seg_2803" s="T114">Vanʼuška</ta>
            <ta e="T116" id="Seg_2804" s="T115">maʔ-gəndə</ta>
            <ta e="T117" id="Seg_2805" s="T116">i</ta>
            <ta e="T118" id="Seg_2806" s="T117">ĭmbi=də</ta>
            <ta e="T119" id="Seg_2807" s="T118">ej</ta>
            <ta e="T120" id="Seg_2808" s="T119">tʼăbaktər-liA-m</ta>
            <ta e="T121" id="Seg_2809" s="T120">ĭmbi</ta>
            <ta e="T122" id="Seg_2810" s="T121">tăn</ta>
            <ta e="T123" id="Seg_2811" s="T122">ĭmbi=də</ta>
            <ta e="T124" id="Seg_2812" s="T123">ej</ta>
            <ta e="T125" id="Seg_2813" s="T124">tʼăbaktər-liA</ta>
            <ta e="T126" id="Seg_2814" s="T125">da</ta>
            <ta e="T127" id="Seg_2815" s="T126">aba-m</ta>
            <ta e="T128" id="Seg_2816" s="T127">măn-bi</ta>
            <ta e="T129" id="Seg_2817" s="T128">kujnek</ta>
            <ta e="T130" id="Seg_2818" s="T129">šöʔ-zittə</ta>
            <ta e="T131" id="Seg_2819" s="T130">iʔbə-ʔ</ta>
            <ta e="T133" id="Seg_2820" s="T132">kunol-ə-ʔ</ta>
            <ta e="T134" id="Seg_2821" s="T133">a</ta>
            <ta e="T136" id="Seg_2822" s="T135">kujnek</ta>
            <ta e="T137" id="Seg_2823" s="T136">mo-lV-j</ta>
            <ta e="T138" id="Seg_2824" s="T137">dĭgəttə</ta>
            <ta e="T139" id="Seg_2825" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_2826" s="T139">kunol-luʔbdə-bi</ta>
            <ta e="T141" id="Seg_2827" s="T140">a</ta>
            <ta e="T142" id="Seg_2828" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_2829" s="T142">nʼiʔdə</ta>
            <ta e="T360" id="Seg_2830" s="T143">kan-lAʔ</ta>
            <ta e="T144" id="Seg_2831" s="T360">tʼür-bi</ta>
            <ta e="T145" id="Seg_2832" s="T144">kuba-t</ta>
            <ta e="T146" id="Seg_2833" s="T145">kür-bi</ta>
            <ta e="T147" id="Seg_2834" s="T146">davaj</ta>
            <ta e="T148" id="Seg_2835" s="T147">kirgaːr-zittə</ta>
            <ta e="T149" id="Seg_2836" s="T148">šöʔ-KAʔ</ta>
            <ta e="T150" id="Seg_2837" s="T149">măna</ta>
            <ta e="T151" id="Seg_2838" s="T150">kujnek</ta>
            <ta e="T152" id="Seg_2839" s="T151">aba-Tə</ta>
            <ta e="T153" id="Seg_2840" s="T152">kuvas</ta>
            <ta e="T154" id="Seg_2841" s="T153">jakšə</ta>
            <ta e="T155" id="Seg_2842" s="T154">dĭgəttə</ta>
            <ta e="T156" id="Seg_2843" s="T155">bazoʔ</ta>
            <ta e="T157" id="Seg_2844" s="T156">kuba-bə</ta>
            <ta e="T158" id="Seg_2845" s="T157">šer-bi</ta>
            <ta e="T160" id="Seg_2846" s="T159">tura-Tə</ta>
            <ta e="T161" id="Seg_2847" s="T160">šo-bi</ta>
            <ta e="T162" id="Seg_2848" s="T161">kujnek-də</ta>
            <ta e="T163" id="Seg_2849" s="T162">hen-bi</ta>
            <ta e="T164" id="Seg_2850" s="T163">stol-bə</ta>
            <ta e="T165" id="Seg_2851" s="T164">stol-bə</ta>
            <ta e="T166" id="Seg_2852" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_2853" s="T166">uʔbdə-bi</ta>
            <ta e="T168" id="Seg_2854" s="T167">tibi-t</ta>
            <ta e="T169" id="Seg_2855" s="T168">no</ta>
            <ta e="T170" id="Seg_2856" s="T169">i-t</ta>
            <ta e="T171" id="Seg_2857" s="T170">kujnek</ta>
            <ta e="T172" id="Seg_2858" s="T171">kun-t</ta>
            <ta e="T173" id="Seg_2859" s="T172">aba-gəndə</ta>
            <ta e="T174" id="Seg_2860" s="T173">dĭ</ta>
            <ta e="T176" id="Seg_2861" s="T175">kan-bi</ta>
            <ta e="T177" id="Seg_2862" s="T176">aba-t</ta>
            <ta e="T178" id="Seg_2863" s="T177">i-bi</ta>
            <ta e="T179" id="Seg_2864" s="T178">staršij</ta>
            <ta e="T180" id="Seg_2865" s="T179">nʼi-gəndə</ta>
            <ta e="T181" id="Seg_2866" s="T180">kujnek</ta>
            <ta e="T182" id="Seg_2867" s="T181">dĭ</ta>
            <ta e="T183" id="Seg_2868" s="T182">tolʼko</ta>
            <ta e="T184" id="Seg_2869" s="T183">togonər-zittə</ta>
            <ta e="T185" id="Seg_2870" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_2871" s="T185">kujnek-dən</ta>
            <ta e="T187" id="Seg_2872" s="T186">a</ta>
            <ta e="T188" id="Seg_2873" s="T187">baška</ta>
            <ta e="T189" id="Seg_2874" s="T188">nʼi-n</ta>
            <ta e="T190" id="Seg_2875" s="T189">a</ta>
            <ta e="T191" id="Seg_2876" s="T190">dö</ta>
            <ta e="T192" id="Seg_2877" s="T191">kujnek</ta>
            <ta e="T193" id="Seg_2878" s="T192">tolʼko</ta>
            <ta e="T194" id="Seg_2879" s="T193">multʼa-Tə</ta>
            <ta e="T195" id="Seg_2880" s="T194">mĭ-zittə</ta>
            <ta e="T196" id="Seg_2881" s="T195">a</ta>
            <ta e="T197" id="Seg_2882" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_2883" s="T197">nʼi</ta>
            <ta e="T199" id="Seg_2884" s="T198">mĭ-bi</ta>
            <ta e="T200" id="Seg_2885" s="T199">a</ta>
            <ta e="T201" id="Seg_2886" s="T200">dö</ta>
            <ta e="T202" id="Seg_2887" s="T201">dö</ta>
            <ta e="T203" id="Seg_2888" s="T202">dĭ</ta>
            <ta e="T204" id="Seg_2889" s="T203">kujnek</ta>
            <ta e="T205" id="Seg_2890" s="T204">tolʼko</ta>
            <ta e="T206" id="Seg_2891" s="T205">sarskən</ta>
            <ta e="T207" id="Seg_2892" s="T206">amno-zittə</ta>
            <ta e="T208" id="Seg_2893" s="T207">kabarləj</ta>
            <ta e="T210" id="Seg_2894" s="T209">dĭ-n</ta>
            <ta e="T211" id="Seg_2895" s="T210">aba-t</ta>
            <ta e="T212" id="Seg_2896" s="T211">il-də</ta>
            <ta e="T213" id="Seg_2897" s="T212">iʔgö</ta>
            <ta e="T214" id="Seg_2898" s="T213">oʔbdə-bi</ta>
            <ta e="T215" id="Seg_2899" s="T214">stol-də</ta>
            <ta e="T216" id="Seg_2900" s="T215">uʔbdə-bi</ta>
            <ta e="T217" id="Seg_2901" s="T216">kăštə-bi</ta>
            <ta e="T218" id="Seg_2902" s="T217">nʼi-zAŋ</ta>
            <ta e="T219" id="Seg_2903" s="T218">i</ta>
            <ta e="T220" id="Seg_2904" s="T219">ne-zAŋ</ta>
            <ta e="T221" id="Seg_2905" s="T220">dĭgəttə</ta>
            <ta e="T222" id="Seg_2906" s="T221">šo-bi-jəʔ</ta>
            <ta e="T223" id="Seg_2907" s="T222">amnə-bi-jəʔ</ta>
            <ta e="T224" id="Seg_2908" s="T223">stol-də</ta>
            <ta e="T225" id="Seg_2909" s="T224">uja</ta>
            <ta e="T226" id="Seg_2910" s="T225">am-bi-jəʔ</ta>
            <ta e="T227" id="Seg_2911" s="T226">i</ta>
            <ta e="T228" id="Seg_2912" s="T227">le-jəʔ</ta>
            <ta e="T229" id="Seg_2913" s="T228">nda</ta>
            <ta e="T230" id="Seg_2914" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_2915" s="T230">šo-bi-jəʔ</ta>
            <ta e="T232" id="Seg_2916" s="T231">dĭ-n</ta>
            <ta e="T233" id="Seg_2917" s="T232">kaga-zAŋ-də</ta>
            <ta e="T234" id="Seg_2918" s="T233">măn-liA-jəʔ</ta>
            <ta e="T235" id="Seg_2919" s="T234">šində=də</ta>
            <ta e="T236" id="Seg_2920" s="T235">šonə-gA</ta>
            <ta e="T237" id="Seg_2921" s="T236">a</ta>
            <ta e="T238" id="Seg_2922" s="T237">dĭ</ta>
            <ta e="T240" id="Seg_2923" s="T239">dĭ-m</ta>
            <ta e="T241" id="Seg_2924" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_2925" s="T241">măn-ntə</ta>
            <ta e="T243" id="Seg_2926" s="T242">măn</ta>
            <ta e="T244" id="Seg_2927" s="T243">lʼaguška-m</ta>
            <ta e="T245" id="Seg_2928" s="T244">šonə-gA</ta>
            <ta e="T246" id="Seg_2929" s="T245">dĭgəttə</ta>
            <ta e="T247" id="Seg_2930" s="T246">dĭ</ta>
            <ta e="T248" id="Seg_2931" s="T247">šo-bi</ta>
            <ta e="T249" id="Seg_2932" s="T248">kuvas</ta>
            <ta e="T250" id="Seg_2933" s="T249">bar</ta>
            <ta e="T251" id="Seg_2934" s="T250">dĭgəttə</ta>
            <ta e="T252" id="Seg_2935" s="T251">dĭ-zAŋ</ta>
            <ta e="T253" id="Seg_2936" s="T252">amnəl-bi-jəʔ</ta>
            <ta e="T254" id="Seg_2937" s="T253">dĭ</ta>
            <ta e="T255" id="Seg_2938" s="T254">bar</ta>
            <ta e="T256" id="Seg_2939" s="T255">am-laʔbə</ta>
            <ta e="T257" id="Seg_2940" s="T256">i</ta>
            <ta e="T258" id="Seg_2941" s="T257">le-jəʔ</ta>
            <ta e="T259" id="Seg_2942" s="T258">rukaf-Tə</ta>
            <ta e="T260" id="Seg_2943" s="T259">hen-laʔbə</ta>
            <ta e="T261" id="Seg_2944" s="T260">ara</ta>
            <ta e="T262" id="Seg_2945" s="T261">rukaf-Tə</ta>
            <ta e="T263" id="Seg_2946" s="T262">kămnə-liA</ta>
            <ta e="T264" id="Seg_2947" s="T263">čaj</ta>
            <ta e="T265" id="Seg_2948" s="T264">dĭbər</ta>
            <ta e="T266" id="Seg_2949" s="T265">kămnə-liA</ta>
            <ta e="T267" id="Seg_2950" s="T266">dĭgəttə</ta>
            <ta e="T268" id="Seg_2951" s="T267">am-bi-jəʔ</ta>
            <ta e="T269" id="Seg_2952" s="T268">kan-bi-jəʔ</ta>
            <ta e="T270" id="Seg_2953" s="T269">süʔmə-zittə</ta>
            <ta e="T271" id="Seg_2954" s="T270">sʼar-zittə</ta>
            <ta e="T272" id="Seg_2955" s="T271">garmonʼ-ziʔ</ta>
            <ta e="T273" id="Seg_2956" s="T272">dĭgəttə</ta>
            <ta e="T274" id="Seg_2957" s="T273">dĭ</ta>
            <ta e="T275" id="Seg_2958" s="T274">süʔmə-luʔbdə-bi</ta>
            <ta e="T276" id="Seg_2959" s="T275">bü</ta>
            <ta e="T277" id="Seg_2960" s="T276">mo-laːm-bi</ta>
            <ta e="T279" id="Seg_2961" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_2962" s="T279">onʼiʔ</ta>
            <ta e="T281" id="Seg_2963" s="T280">uda-gəndə</ta>
            <ta e="T284" id="Seg_2964" s="T283">bü</ta>
            <ta e="T285" id="Seg_2965" s="T284">mo-laːm-bi</ta>
            <ta e="T286" id="Seg_2966" s="T285">tsar</ta>
            <ta e="T287" id="Seg_2967" s="T286">bar</ta>
            <ta e="T288" id="Seg_2968" s="T287">nereʔ-luʔbdə-bi</ta>
            <ta e="T289" id="Seg_2969" s="T288">baška</ta>
            <ta e="T290" id="Seg_2970" s="T289">uda-gəndə</ta>
            <ta e="T292" id="Seg_2971" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_2972" s="T292">nabə-jəʔ</ta>
            <ta e="T294" id="Seg_2973" s="T293">šo-luʔbdə-bi-jəʔ</ta>
            <ta e="T295" id="Seg_2974" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_2975" s="T295">bar</ta>
            <ta e="T297" id="Seg_2976" s="T296">tʼür-laʔbə-bi</ta>
            <ta e="T298" id="Seg_2977" s="T297">dĭgəttə</ta>
            <ta e="T299" id="Seg_2978" s="T298">dĭ-zAŋ</ta>
            <ta e="T300" id="Seg_2979" s="T299">bareʔ</ta>
            <ta e="T301" id="Seg_2980" s="T300">ne-zAŋ</ta>
            <ta e="T302" id="Seg_2981" s="T301">uda-t-ziʔ</ta>
            <ta e="T303" id="Seg_2982" s="T302">barəʔ-luʔbdə-bi</ta>
            <ta e="T304" id="Seg_2983" s="T303">bar</ta>
            <ta e="T305" id="Seg_2984" s="T304">il-Tə</ta>
            <ta e="T306" id="Seg_2985" s="T305">i</ta>
            <ta e="T307" id="Seg_2986" s="T306">le-ziʔ</ta>
            <ta e="T308" id="Seg_2987" s="T307">kadul-Tə</ta>
            <ta e="T309" id="Seg_2988" s="T308">bar</ta>
            <ta e="T310" id="Seg_2989" s="T309">toʔbdə-nar-bi</ta>
            <ta e="T312" id="Seg_2990" s="T311">bü-ziʔ</ta>
            <ta e="T313" id="Seg_2991" s="T312">bar</ta>
            <ta e="T314" id="Seg_2992" s="T313">sima-t-Tə</ta>
            <ta e="T315" id="Seg_2993" s="T314">bar</ta>
            <ta e="T316" id="Seg_2994" s="T315">bü</ta>
            <ta e="T318" id="Seg_2995" s="T317">kămnə-bi-jəʔ</ta>
            <ta e="T319" id="Seg_2996" s="T318">kabarləj</ta>
            <ta e="T320" id="Seg_2997" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2998" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_2999" s="T321">nuʔmə-luʔbdə-bi</ta>
            <ta e="T323" id="Seg_3000" s="T322">maʔ-gəndə</ta>
            <ta e="T324" id="Seg_3001" s="T323">kuba-bə</ta>
            <ta e="T325" id="Seg_3002" s="T324">ku-bi</ta>
            <ta e="T326" id="Seg_3003" s="T325">da</ta>
            <ta e="T327" id="Seg_3004" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_3005" s="T327">šü-Tə</ta>
            <ta e="T330" id="Seg_3006" s="T329">hen-bi</ta>
            <ta e="T331" id="Seg_3007" s="T330">dĭ</ta>
            <ta e="T332" id="Seg_3008" s="T331">bar</ta>
            <ta e="T333" id="Seg_3009" s="T332">neni-luʔbdə-bi</ta>
            <ta e="T334" id="Seg_3010" s="T333">dĭgəttə</ta>
            <ta e="T335" id="Seg_3011" s="T334">dĭ</ta>
            <ta e="T336" id="Seg_3012" s="T335">šo-bi</ta>
            <ta e="T337" id="Seg_3013" s="T336">maʔ-gəndə</ta>
            <ta e="T338" id="Seg_3014" s="T337">ugaːndə</ta>
            <ta e="T339" id="Seg_3015" s="T338">tăn</ta>
            <ta e="T340" id="Seg_3016" s="T339">ej</ta>
            <ta e="T341" id="Seg_3017" s="T340">jakšə</ta>
            <ta e="T342" id="Seg_3018" s="T341">a-bi-l</ta>
            <ta e="T343" id="Seg_3019" s="T342">tüj</ta>
            <ta e="T344" id="Seg_3020" s="T343">măn</ta>
            <ta e="T361" id="Seg_3021" s="T344">kan-lAʔ</ta>
            <ta e="T345" id="Seg_3022" s="T361">tʼür-lV-m</ta>
            <ta e="T346" id="Seg_3023" s="T345">a</ta>
            <ta e="T347" id="Seg_3024" s="T346">tăn</ta>
            <ta e="T348" id="Seg_3025" s="T347">măna</ta>
            <ta e="T349" id="Seg_3026" s="T348">măndo-r-lV-l</ta>
            <ta e="T350" id="Seg_3027" s="T349">dĭ</ta>
            <ta e="T362" id="Seg_3028" s="T350">kan-lAʔ</ta>
            <ta e="T351" id="Seg_3029" s="T362">tʼür-bi</ta>
            <ta e="T352" id="Seg_3030" s="T351">a</ta>
            <ta e="T353" id="Seg_3031" s="T352">dĭ</ta>
            <ta e="T354" id="Seg_3032" s="T353">bar</ta>
            <ta e="T355" id="Seg_3033" s="T354">tʼor-bi</ta>
            <ta e="T356" id="Seg_3034" s="T355">tʼor-bi</ta>
            <ta e="T357" id="Seg_3035" s="T356">dĭgəttə</ta>
            <ta e="T358" id="Seg_3036" s="T357">kan-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3037" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_3038" s="T1">chief.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3039" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_3040" s="T3">this-GEN</ta>
            <ta e="T5" id="Seg_3041" s="T4">three.[NOM.SG]</ta>
            <ta e="T6" id="Seg_3042" s="T5">boy-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T7" id="Seg_3043" s="T6">be-PST-3PL</ta>
            <ta e="T8" id="Seg_3044" s="T7">then</ta>
            <ta e="T9" id="Seg_3045" s="T8">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_3046" s="T9">say-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_3047" s="T10">boy-PL-LAT</ta>
            <ta e="T12" id="Seg_3048" s="T11">one.should</ta>
            <ta e="T13" id="Seg_3049" s="T12">you.PL.LAT</ta>
            <ta e="T14" id="Seg_3050" s="T13">marry-INF.LAT</ta>
            <ta e="T15" id="Seg_3051" s="T14">woman-PL</ta>
            <ta e="T16" id="Seg_3052" s="T15">so.that</ta>
            <ta e="T17" id="Seg_3053" s="T16">I.LAT</ta>
            <ta e="T18" id="Seg_3054" s="T17">see-INF.LAT</ta>
            <ta e="T20" id="Seg_3055" s="T19">you.PL.LAT</ta>
            <ta e="T21" id="Seg_3056" s="T20">child-PL</ta>
            <ta e="T22" id="Seg_3057" s="T21">well</ta>
            <ta e="T23" id="Seg_3058" s="T22">this-PL</ta>
            <ta e="T24" id="Seg_3059" s="T23">say-IPFVZ-3PL</ta>
            <ta e="T25" id="Seg_3060" s="T24">well</ta>
            <ta e="T26" id="Seg_3061" s="T25">marry-IMP.2SG</ta>
            <ta e="T27" id="Seg_3062" s="T26">who.[NOM.SG]</ta>
            <ta e="T28" id="Seg_3063" s="T27">you.DAT</ta>
            <ta e="T29" id="Seg_3064" s="T28">one.needs</ta>
            <ta e="T30" id="Seg_3065" s="T29">take-IMP.2PL</ta>
            <ta e="T31" id="Seg_3066" s="T30">gun-PL</ta>
            <ta e="T32" id="Seg_3067" s="T31">and</ta>
            <ta e="T33" id="Seg_3068" s="T32">go-IMP.2PL</ta>
            <ta e="T34" id="Seg_3069" s="T33">shoot-IMP.2PL</ta>
            <ta e="T35" id="Seg_3070" s="T34">arrow-PL</ta>
            <ta e="T36" id="Seg_3071" s="T35">then</ta>
            <ta e="T37" id="Seg_3072" s="T36">this-LAT</ta>
            <ta e="T38" id="Seg_3073" s="T37">this-PL</ta>
            <ta e="T39" id="Seg_3074" s="T38">go-PST-3PL</ta>
            <ta e="T40" id="Seg_3075" s="T39">one.[NOM.SG]</ta>
            <ta e="T41" id="Seg_3076" s="T40">shoot-PST.[3SG]</ta>
            <ta e="T42" id="Seg_3077" s="T41">fall-MOM-PST.[3SG]</ta>
            <ta e="T44" id="Seg_3078" s="T43">merchant-LAT</ta>
            <ta e="T45" id="Seg_3079" s="T44">house-LAT</ta>
            <ta e="T46" id="Seg_3080" s="T45">one.[NOM.SG]</ta>
            <ta e="T47" id="Seg_3081" s="T46">shoot-PST.[3SG]</ta>
            <ta e="T48" id="Seg_3082" s="T47">fall-MOM-PST.[3SG]</ta>
            <ta e="T52" id="Seg_3083" s="T51">palace-LAT</ta>
            <ta e="T53" id="Seg_3084" s="T52">and</ta>
            <ta e="T54" id="Seg_3085" s="T53">one.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3086" s="T54">shoot-PST.[3SG]</ta>
            <ta e="T56" id="Seg_3087" s="T55">far-LAT.ADV</ta>
            <ta e="T359" id="Seg_3088" s="T56">go-CVB</ta>
            <ta e="T57" id="Seg_3089" s="T359">disappear-PST.[3SG]</ta>
            <ta e="T58" id="Seg_3090" s="T57">then</ta>
            <ta e="T59" id="Seg_3091" s="T58">this.[NOM.SG]</ta>
            <ta e="T60" id="Seg_3092" s="T59">go-PST.[3SG]</ta>
            <ta e="T61" id="Seg_3093" s="T60">go-PST.[3SG]</ta>
            <ta e="T62" id="Seg_3094" s="T61">come-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_3095" s="T62">swamp-LOC</ta>
            <ta e="T64" id="Seg_3096" s="T63">sit-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_3097" s="T64">frog-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_3098" s="T65">and</ta>
            <ta e="T67" id="Seg_3099" s="T66">arrow.[NOM.SG]</ta>
            <ta e="T68" id="Seg_3100" s="T67">this-LAT</ta>
            <ta e="T70" id="Seg_3101" s="T69">this.ABL</ta>
            <ta e="T71" id="Seg_3102" s="T70">this.[NOM.SG]</ta>
            <ta e="T72" id="Seg_3103" s="T71">say-IPFVZ.[3SG]</ta>
            <ta e="T73" id="Seg_3104" s="T72">bring-IMP.2SG</ta>
            <ta e="T74" id="Seg_3105" s="T73">I.LAT</ta>
            <ta e="T75" id="Seg_3106" s="T74">arrow.[NOM.SG]</ta>
            <ta e="T76" id="Seg_3107" s="T75">and</ta>
            <ta e="T77" id="Seg_3108" s="T76">you.NOM</ta>
            <ta e="T78" id="Seg_3109" s="T77">this.[NOM.SG]</ta>
            <ta e="T79" id="Seg_3110" s="T78">say-IPFVZ.[3SG]</ta>
            <ta e="T80" id="Seg_3111" s="T79">take-IMP.2SG</ta>
            <ta e="T81" id="Seg_3112" s="T80">I.ACC</ta>
            <ta e="T82" id="Seg_3113" s="T81">man-LAT</ta>
            <ta e="T83" id="Seg_3114" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_3115" s="T83">how</ta>
            <ta e="T85" id="Seg_3116" s="T84">I.NOM</ta>
            <ta e="T86" id="Seg_3117" s="T85">you.ACC</ta>
            <ta e="T87" id="Seg_3118" s="T86">take-FUT-1SG</ta>
            <ta e="T88" id="Seg_3119" s="T87">you.NOM</ta>
            <ta e="T89" id="Seg_3120" s="T88">you.know</ta>
            <ta e="T90" id="Seg_3121" s="T89">frog.[NOM.SG]</ta>
            <ta e="T91" id="Seg_3122" s="T90">well</ta>
            <ta e="T92" id="Seg_3123" s="T91">so</ta>
            <ta e="T93" id="Seg_3124" s="T92">you.NOM</ta>
            <ta e="T94" id="Seg_3125" s="T93">you.NOM</ta>
            <ta e="T95" id="Seg_3126" s="T94">fate-NOM/GEN/ACC.2SG</ta>
            <ta e="T96" id="Seg_3127" s="T95">take-IMP.2SG.O</ta>
            <ta e="T97" id="Seg_3128" s="T96">I.ACC</ta>
            <ta e="T98" id="Seg_3129" s="T97">then</ta>
            <ta e="T99" id="Seg_3130" s="T98">this.[NOM.SG]</ta>
            <ta e="T100" id="Seg_3131" s="T99">take-PST.[3SG]</ta>
            <ta e="T101" id="Seg_3132" s="T100">tent-LAT/LOC.3SG</ta>
            <ta e="T102" id="Seg_3133" s="T101">bring-PST.[3SG]</ta>
            <ta e="T103" id="Seg_3134" s="T102">then</ta>
            <ta e="T106" id="Seg_3135" s="T105">make-PST.[3SG]</ta>
            <ta e="T107" id="Seg_3136" s="T106">-PL</ta>
            <ta e="T108" id="Seg_3137" s="T107">then</ta>
            <ta e="T109" id="Seg_3138" s="T108">say-PST.[3SG]</ta>
            <ta e="T110" id="Seg_3139" s="T109">sew-IMP.2SG.O</ta>
            <ta e="T111" id="Seg_3140" s="T110">shirt-PL</ta>
            <ta e="T112" id="Seg_3141" s="T111">then</ta>
            <ta e="T114" id="Seg_3142" s="T113">come-PST.[3SG]</ta>
            <ta e="T115" id="Seg_3143" s="T114">Vanyushka.[NOM.SG]</ta>
            <ta e="T116" id="Seg_3144" s="T115">tent-LAT/LOC.3SG</ta>
            <ta e="T117" id="Seg_3145" s="T116">and</ta>
            <ta e="T118" id="Seg_3146" s="T117">what.[NOM.SG]=INDEF</ta>
            <ta e="T119" id="Seg_3147" s="T118">NEG</ta>
            <ta e="T120" id="Seg_3148" s="T119">speak-PRS-1SG</ta>
            <ta e="T121" id="Seg_3149" s="T120">what.[NOM.SG]</ta>
            <ta e="T122" id="Seg_3150" s="T121">you.NOM</ta>
            <ta e="T123" id="Seg_3151" s="T122">what.[NOM.SG]=INDEF</ta>
            <ta e="T124" id="Seg_3152" s="T123">NEG</ta>
            <ta e="T125" id="Seg_3153" s="T124">speak-PRS.[3SG]</ta>
            <ta e="T126" id="Seg_3154" s="T125">and</ta>
            <ta e="T127" id="Seg_3155" s="T126">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T128" id="Seg_3156" s="T127">say-PST.[3SG]</ta>
            <ta e="T129" id="Seg_3157" s="T128">shirt.[NOM.SG]</ta>
            <ta e="T130" id="Seg_3158" s="T129">sew-INF.LAT</ta>
            <ta e="T131" id="Seg_3159" s="T130">lie.down-IMP.2SG</ta>
            <ta e="T133" id="Seg_3160" s="T132">sleep-EP-IMP.2SG</ta>
            <ta e="T134" id="Seg_3161" s="T133">and</ta>
            <ta e="T136" id="Seg_3162" s="T135">shirt.[NOM.SG]</ta>
            <ta e="T137" id="Seg_3163" s="T136">become-FUT-3SG</ta>
            <ta e="T138" id="Seg_3164" s="T137">then</ta>
            <ta e="T139" id="Seg_3165" s="T138">this.[NOM.SG]</ta>
            <ta e="T140" id="Seg_3166" s="T139">sleep-MOM-PST.[3SG]</ta>
            <ta e="T141" id="Seg_3167" s="T140">and</ta>
            <ta e="T142" id="Seg_3168" s="T141">this.[NOM.SG]</ta>
            <ta e="T143" id="Seg_3169" s="T142">outwards</ta>
            <ta e="T360" id="Seg_3170" s="T143">go-CVB</ta>
            <ta e="T144" id="Seg_3171" s="T360">disappear-PST.[3SG]</ta>
            <ta e="T145" id="Seg_3172" s="T144">skin-NOM/GEN.3SG</ta>
            <ta e="T146" id="Seg_3173" s="T145">peel.bark-PST.[3SG]</ta>
            <ta e="T147" id="Seg_3174" s="T146">INCH</ta>
            <ta e="T148" id="Seg_3175" s="T147">shout-INF.LAT</ta>
            <ta e="T149" id="Seg_3176" s="T148">sew-IMP.2PL</ta>
            <ta e="T150" id="Seg_3177" s="T149">I.LAT</ta>
            <ta e="T151" id="Seg_3178" s="T150">shirt.[NOM.SG]</ta>
            <ta e="T152" id="Seg_3179" s="T151">father-LAT</ta>
            <ta e="T153" id="Seg_3180" s="T152">beautiful.[NOM.SG]</ta>
            <ta e="T154" id="Seg_3181" s="T153">good.[NOM.SG]</ta>
            <ta e="T155" id="Seg_3182" s="T154">then</ta>
            <ta e="T156" id="Seg_3183" s="T155">again</ta>
            <ta e="T157" id="Seg_3184" s="T156">skin-ACC.3SG</ta>
            <ta e="T158" id="Seg_3185" s="T157">dress-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3186" s="T159">house-LAT</ta>
            <ta e="T161" id="Seg_3187" s="T160">come-PST.[3SG]</ta>
            <ta e="T162" id="Seg_3188" s="T161">shirt-NOM/GEN/ACC.3SG</ta>
            <ta e="T163" id="Seg_3189" s="T162">put-PST.[3SG]</ta>
            <ta e="T164" id="Seg_3190" s="T163">table-ACC.3SG</ta>
            <ta e="T165" id="Seg_3191" s="T164">table-ACC.3SG</ta>
            <ta e="T166" id="Seg_3192" s="T165">then</ta>
            <ta e="T167" id="Seg_3193" s="T166">get.up-PST.[3SG]</ta>
            <ta e="T168" id="Seg_3194" s="T167">man-NOM/GEN.3SG</ta>
            <ta e="T169" id="Seg_3195" s="T168">well</ta>
            <ta e="T170" id="Seg_3196" s="T169">take-IMP.2SG.O</ta>
            <ta e="T171" id="Seg_3197" s="T170">shirt.[NOM.SG]</ta>
            <ta e="T172" id="Seg_3198" s="T171">bring-IMP.2SG.O</ta>
            <ta e="T173" id="Seg_3199" s="T172">father-LAT/LOC.3SG</ta>
            <ta e="T174" id="Seg_3200" s="T173">this.[NOM.SG]</ta>
            <ta e="T176" id="Seg_3201" s="T175">go-PST.[3SG]</ta>
            <ta e="T177" id="Seg_3202" s="T176">father-NOM/GEN.3SG</ta>
            <ta e="T178" id="Seg_3203" s="T177">take-PST.[3SG]</ta>
            <ta e="T179" id="Seg_3204" s="T178">elder.[NOM.SG]</ta>
            <ta e="T180" id="Seg_3205" s="T179">son-LAT/LOC.3SG</ta>
            <ta e="T181" id="Seg_3206" s="T180">shirt.[NOM.SG]</ta>
            <ta e="T182" id="Seg_3207" s="T181">this.[NOM.SG]</ta>
            <ta e="T183" id="Seg_3208" s="T182">only</ta>
            <ta e="T184" id="Seg_3209" s="T183">work-INF.LAT</ta>
            <ta e="T185" id="Seg_3210" s="T184">this.[NOM.SG]</ta>
            <ta e="T186" id="Seg_3211" s="T185">shirt-NOM/GEN/ACC.3PL</ta>
            <ta e="T187" id="Seg_3212" s="T186">and</ta>
            <ta e="T188" id="Seg_3213" s="T187">another.[NOM.SG]</ta>
            <ta e="T189" id="Seg_3214" s="T188">boy-GEN</ta>
            <ta e="T190" id="Seg_3215" s="T189">and</ta>
            <ta e="T191" id="Seg_3216" s="T190">that.[NOM.SG]</ta>
            <ta e="T192" id="Seg_3217" s="T191">shirt.[NOM.SG]</ta>
            <ta e="T193" id="Seg_3218" s="T192">only</ta>
            <ta e="T194" id="Seg_3219" s="T193">sauna-LAT</ta>
            <ta e="T195" id="Seg_3220" s="T194">give-INF.LAT</ta>
            <ta e="T196" id="Seg_3221" s="T195">and</ta>
            <ta e="T197" id="Seg_3222" s="T196">this.[NOM.SG]</ta>
            <ta e="T198" id="Seg_3223" s="T197">boy.[NOM.SG]</ta>
            <ta e="T199" id="Seg_3224" s="T198">give-PST.[3SG]</ta>
            <ta e="T200" id="Seg_3225" s="T199">and</ta>
            <ta e="T201" id="Seg_3226" s="T200">that.[NOM.SG]</ta>
            <ta e="T202" id="Seg_3227" s="T201">that.[NOM.SG]</ta>
            <ta e="T203" id="Seg_3228" s="T202">this.[NOM.SG]</ta>
            <ta e="T204" id="Seg_3229" s="T203">shirt.[NOM.SG]</ta>
            <ta e="T205" id="Seg_3230" s="T204">only</ta>
            <ta e="T206" id="Seg_3231" s="T205">%%</ta>
            <ta e="T207" id="Seg_3232" s="T206">sit-INF.LAT</ta>
            <ta e="T208" id="Seg_3233" s="T207">enough</ta>
            <ta e="T210" id="Seg_3234" s="T209">this-GEN</ta>
            <ta e="T211" id="Seg_3235" s="T210">father-NOM/GEN.3SG</ta>
            <ta e="T212" id="Seg_3236" s="T211">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T213" id="Seg_3237" s="T212">many</ta>
            <ta e="T214" id="Seg_3238" s="T213">collect-PST.[3SG]</ta>
            <ta e="T215" id="Seg_3239" s="T214">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T216" id="Seg_3240" s="T215">get.up-PST.[3SG]</ta>
            <ta e="T217" id="Seg_3241" s="T216">call-PST.[3SG]</ta>
            <ta e="T218" id="Seg_3242" s="T217">boy-PL</ta>
            <ta e="T219" id="Seg_3243" s="T218">and</ta>
            <ta e="T220" id="Seg_3244" s="T219">woman-PL</ta>
            <ta e="T221" id="Seg_3245" s="T220">then</ta>
            <ta e="T222" id="Seg_3246" s="T221">come-PST-3PL</ta>
            <ta e="T223" id="Seg_3247" s="T222">sit.down-PST-3PL</ta>
            <ta e="T224" id="Seg_3248" s="T223">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T225" id="Seg_3249" s="T224">meat.[NOM.SG]</ta>
            <ta e="T226" id="Seg_3250" s="T225">eat-PST-3PL</ta>
            <ta e="T227" id="Seg_3251" s="T226">and</ta>
            <ta e="T228" id="Seg_3252" s="T227">bone-NOM/GEN/ACC.3PL</ta>
            <ta e="T229" id="Seg_3253" s="T228">%%</ta>
            <ta e="T230" id="Seg_3254" s="T229">then</ta>
            <ta e="T231" id="Seg_3255" s="T230">come-PST-3PL</ta>
            <ta e="T232" id="Seg_3256" s="T231">this-GEN</ta>
            <ta e="T233" id="Seg_3257" s="T232">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T234" id="Seg_3258" s="T233">say-PRS-3PL</ta>
            <ta e="T235" id="Seg_3259" s="T234">who.[NOM.SG]=INDEF</ta>
            <ta e="T236" id="Seg_3260" s="T235">come-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_3261" s="T236">and</ta>
            <ta e="T238" id="Seg_3262" s="T237">this.[NOM.SG]</ta>
            <ta e="T240" id="Seg_3263" s="T239">this-ACC</ta>
            <ta e="T241" id="Seg_3264" s="T240">this.[NOM.SG]</ta>
            <ta e="T242" id="Seg_3265" s="T241">say-IPFVZ.[3SG]</ta>
            <ta e="T243" id="Seg_3266" s="T242">I.NOM</ta>
            <ta e="T244" id="Seg_3267" s="T243">frog-NOM/GEN/ACC.1SG</ta>
            <ta e="T245" id="Seg_3268" s="T244">come-PRS.[3SG]</ta>
            <ta e="T246" id="Seg_3269" s="T245">then</ta>
            <ta e="T247" id="Seg_3270" s="T246">this.[NOM.SG]</ta>
            <ta e="T248" id="Seg_3271" s="T247">come-PST.[3SG]</ta>
            <ta e="T249" id="Seg_3272" s="T248">beautiful.[NOM.SG]</ta>
            <ta e="T250" id="Seg_3273" s="T249">PTCL</ta>
            <ta e="T251" id="Seg_3274" s="T250">then</ta>
            <ta e="T252" id="Seg_3275" s="T251">this-PL</ta>
            <ta e="T253" id="Seg_3276" s="T252">sit.down-PST-3PL</ta>
            <ta e="T254" id="Seg_3277" s="T253">this.[NOM.SG]</ta>
            <ta e="T255" id="Seg_3278" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_3279" s="T255">eat-DUR.[3SG]</ta>
            <ta e="T257" id="Seg_3280" s="T256">and</ta>
            <ta e="T258" id="Seg_3281" s="T257">bone-NOM/GEN/ACC.3PL</ta>
            <ta e="T259" id="Seg_3282" s="T258">sleeve-LAT</ta>
            <ta e="T260" id="Seg_3283" s="T259">put-DUR.[3SG]</ta>
            <ta e="T261" id="Seg_3284" s="T260">vodka.[NOM.SG]</ta>
            <ta e="T262" id="Seg_3285" s="T261">sleeve-LAT</ta>
            <ta e="T263" id="Seg_3286" s="T262">pour-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_3287" s="T263">tea.[NOM.SG]</ta>
            <ta e="T265" id="Seg_3288" s="T264">there</ta>
            <ta e="T266" id="Seg_3289" s="T265">pour-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_3290" s="T266">then</ta>
            <ta e="T268" id="Seg_3291" s="T267">eat-PST-3PL</ta>
            <ta e="T269" id="Seg_3292" s="T268">go-PST-3PL</ta>
            <ta e="T270" id="Seg_3293" s="T269">jump-INF.LAT</ta>
            <ta e="T271" id="Seg_3294" s="T270">play-INF.LAT</ta>
            <ta e="T272" id="Seg_3295" s="T271">accordion-INS</ta>
            <ta e="T273" id="Seg_3296" s="T272">then</ta>
            <ta e="T274" id="Seg_3297" s="T273">this.[NOM.SG]</ta>
            <ta e="T275" id="Seg_3298" s="T274">jump-MOM-PST.[3SG]</ta>
            <ta e="T276" id="Seg_3299" s="T275">water.[NOM.SG]</ta>
            <ta e="T277" id="Seg_3300" s="T276">become-RES-PST.[3SG]</ta>
            <ta e="T279" id="Seg_3301" s="T278">then</ta>
            <ta e="T280" id="Seg_3302" s="T279">one.[NOM.SG]</ta>
            <ta e="T281" id="Seg_3303" s="T280">hand-LAT/LOC.3SG</ta>
            <ta e="T284" id="Seg_3304" s="T283">water.[NOM.SG]</ta>
            <ta e="T285" id="Seg_3305" s="T284">become-RES-PST.[3SG]</ta>
            <ta e="T286" id="Seg_3306" s="T285">tsar.[NOM.SG]</ta>
            <ta e="T287" id="Seg_3307" s="T286">PTCL</ta>
            <ta e="T288" id="Seg_3308" s="T287">frighten-MOM-PST.[3SG]</ta>
            <ta e="T289" id="Seg_3309" s="T288">another.[NOM.SG]</ta>
            <ta e="T290" id="Seg_3310" s="T289">hand-LAT/LOC.3SG</ta>
            <ta e="T292" id="Seg_3311" s="T291">then</ta>
            <ta e="T293" id="Seg_3312" s="T292">duck-PL</ta>
            <ta e="T294" id="Seg_3313" s="T293">come-MOM-PST-3PL</ta>
            <ta e="T295" id="Seg_3314" s="T294">then</ta>
            <ta e="T296" id="Seg_3315" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_3316" s="T296">disappear-DUR-PST.[3SG]</ta>
            <ta e="T298" id="Seg_3317" s="T297">then</ta>
            <ta e="T299" id="Seg_3318" s="T298">this-PL</ta>
            <ta e="T300" id="Seg_3319" s="T299">%%</ta>
            <ta e="T301" id="Seg_3320" s="T300">woman-PL</ta>
            <ta e="T302" id="Seg_3321" s="T301">hand-3SG-INS</ta>
            <ta e="T303" id="Seg_3322" s="T302">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T304" id="Seg_3323" s="T303">PTCL</ta>
            <ta e="T305" id="Seg_3324" s="T304">people-LAT</ta>
            <ta e="T306" id="Seg_3325" s="T305">and</ta>
            <ta e="T307" id="Seg_3326" s="T306">bone-INS</ta>
            <ta e="T308" id="Seg_3327" s="T307">face-LAT</ta>
            <ta e="T309" id="Seg_3328" s="T308">PTCL</ta>
            <ta e="T310" id="Seg_3329" s="T309">hit-MULT-PST.[3SG]</ta>
            <ta e="T312" id="Seg_3330" s="T311">water-INS</ta>
            <ta e="T313" id="Seg_3331" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_3332" s="T313">eye-3SG-LAT</ta>
            <ta e="T315" id="Seg_3333" s="T314">PTCL</ta>
            <ta e="T316" id="Seg_3334" s="T315">water.[NOM.SG]</ta>
            <ta e="T318" id="Seg_3335" s="T317">pour-PST-3PL</ta>
            <ta e="T319" id="Seg_3336" s="T318">enough</ta>
            <ta e="T320" id="Seg_3337" s="T319">then</ta>
            <ta e="T321" id="Seg_3338" s="T320">this.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3339" s="T321">run-MOM-PST.[3SG]</ta>
            <ta e="T323" id="Seg_3340" s="T322">tent-LAT/LOC.3SG</ta>
            <ta e="T324" id="Seg_3341" s="T323">skin-ACC.3SG</ta>
            <ta e="T325" id="Seg_3342" s="T324">find-PST.[3SG]</ta>
            <ta e="T326" id="Seg_3343" s="T325">and</ta>
            <ta e="T327" id="Seg_3344" s="T326">take-PST.[3SG]</ta>
            <ta e="T328" id="Seg_3345" s="T327">fire-LAT</ta>
            <ta e="T330" id="Seg_3346" s="T329">put-PST.[3SG]</ta>
            <ta e="T331" id="Seg_3347" s="T330">this.[NOM.SG]</ta>
            <ta e="T332" id="Seg_3348" s="T331">PTCL</ta>
            <ta e="T333" id="Seg_3349" s="T332">%burn-MOM-PST.[3SG]</ta>
            <ta e="T334" id="Seg_3350" s="T333">then</ta>
            <ta e="T335" id="Seg_3351" s="T334">this.[NOM.SG]</ta>
            <ta e="T336" id="Seg_3352" s="T335">come-PST.[3SG]</ta>
            <ta e="T337" id="Seg_3353" s="T336">tent-LAT/LOC.3SG</ta>
            <ta e="T338" id="Seg_3354" s="T337">very</ta>
            <ta e="T339" id="Seg_3355" s="T338">you.NOM</ta>
            <ta e="T340" id="Seg_3356" s="T339">NEG</ta>
            <ta e="T341" id="Seg_3357" s="T340">good.[NOM.SG]</ta>
            <ta e="T342" id="Seg_3358" s="T341">make-PST-2SG</ta>
            <ta e="T343" id="Seg_3359" s="T342">now</ta>
            <ta e="T344" id="Seg_3360" s="T343">I.NOM</ta>
            <ta e="T361" id="Seg_3361" s="T344">go-CVB</ta>
            <ta e="T345" id="Seg_3362" s="T361">disappear-FUT-1SG</ta>
            <ta e="T346" id="Seg_3363" s="T345">and</ta>
            <ta e="T347" id="Seg_3364" s="T346">you.NOM</ta>
            <ta e="T348" id="Seg_3365" s="T347">I.ACC</ta>
            <ta e="T349" id="Seg_3366" s="T348">look-FRQ-FUT-2SG</ta>
            <ta e="T350" id="Seg_3367" s="T349">this.[NOM.SG]</ta>
            <ta e="T362" id="Seg_3368" s="T350">go-CVB</ta>
            <ta e="T351" id="Seg_3369" s="T362">disappear-PST.[3SG]</ta>
            <ta e="T352" id="Seg_3370" s="T351">and</ta>
            <ta e="T353" id="Seg_3371" s="T352">this.[NOM.SG]</ta>
            <ta e="T354" id="Seg_3372" s="T353">PTCL</ta>
            <ta e="T355" id="Seg_3373" s="T354">cry-PST.[3SG]</ta>
            <ta e="T356" id="Seg_3374" s="T355">cry-PST.[3SG]</ta>
            <ta e="T357" id="Seg_3375" s="T356">then</ta>
            <ta e="T358" id="Seg_3376" s="T357">go-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3377" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_3378" s="T1">вождь.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3379" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_3380" s="T3">этот-GEN</ta>
            <ta e="T5" id="Seg_3381" s="T4">три.[NOM.SG]</ta>
            <ta e="T6" id="Seg_3382" s="T5">мальчик-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T7" id="Seg_3383" s="T6">быть-PST-3PL</ta>
            <ta e="T8" id="Seg_3384" s="T7">тогда</ta>
            <ta e="T9" id="Seg_3385" s="T8">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_3386" s="T9">сказать-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_3387" s="T10">мальчик-PL-LAT</ta>
            <ta e="T12" id="Seg_3388" s="T11">надо</ta>
            <ta e="T13" id="Seg_3389" s="T12">вы.LAT</ta>
            <ta e="T14" id="Seg_3390" s="T13">жениться-INF.LAT</ta>
            <ta e="T15" id="Seg_3391" s="T14">женщина-PL</ta>
            <ta e="T16" id="Seg_3392" s="T15">чтобы</ta>
            <ta e="T17" id="Seg_3393" s="T16">я.LAT</ta>
            <ta e="T18" id="Seg_3394" s="T17">видеть-INF.LAT</ta>
            <ta e="T20" id="Seg_3395" s="T19">вы.LAT</ta>
            <ta e="T21" id="Seg_3396" s="T20">ребенок-PL</ta>
            <ta e="T22" id="Seg_3397" s="T21">ну</ta>
            <ta e="T23" id="Seg_3398" s="T22">этот-PL</ta>
            <ta e="T24" id="Seg_3399" s="T23">сказать-IPFVZ-3PL</ta>
            <ta e="T25" id="Seg_3400" s="T24">ну</ta>
            <ta e="T26" id="Seg_3401" s="T25">жениться-IMP.2SG</ta>
            <ta e="T27" id="Seg_3402" s="T26">кто.[NOM.SG]</ta>
            <ta e="T28" id="Seg_3403" s="T27">ты.DAT</ta>
            <ta e="T29" id="Seg_3404" s="T28">нужно</ta>
            <ta e="T30" id="Seg_3405" s="T29">взять-IMP.2PL</ta>
            <ta e="T31" id="Seg_3406" s="T30">ружье-PL</ta>
            <ta e="T32" id="Seg_3407" s="T31">и</ta>
            <ta e="T33" id="Seg_3408" s="T32">пойти-IMP.2PL</ta>
            <ta e="T34" id="Seg_3409" s="T33">стрелять-IMP.2PL</ta>
            <ta e="T35" id="Seg_3410" s="T34">стрела-PL</ta>
            <ta e="T36" id="Seg_3411" s="T35">тогда</ta>
            <ta e="T37" id="Seg_3412" s="T36">этот-LAT</ta>
            <ta e="T38" id="Seg_3413" s="T37">этот-PL</ta>
            <ta e="T39" id="Seg_3414" s="T38">пойти-PST-3PL</ta>
            <ta e="T40" id="Seg_3415" s="T39">один.[NOM.SG]</ta>
            <ta e="T41" id="Seg_3416" s="T40">стрелять-PST.[3SG]</ta>
            <ta e="T42" id="Seg_3417" s="T41">упасть-MOM-PST.[3SG]</ta>
            <ta e="T44" id="Seg_3418" s="T43">купец-LAT</ta>
            <ta e="T45" id="Seg_3419" s="T44">дом-LAT</ta>
            <ta e="T46" id="Seg_3420" s="T45">один.[NOM.SG]</ta>
            <ta e="T47" id="Seg_3421" s="T46">стрелять-PST.[3SG]</ta>
            <ta e="T48" id="Seg_3422" s="T47">упасть-MOM-PST.[3SG] </ta>
            <ta e="T52" id="Seg_3423" s="T51">дворец-LAT</ta>
            <ta e="T53" id="Seg_3424" s="T52">а</ta>
            <ta e="T54" id="Seg_3425" s="T53">один.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3426" s="T54">стрелять-PST.[3SG]</ta>
            <ta e="T56" id="Seg_3427" s="T55">далеко-LAT.ADV</ta>
            <ta e="T359" id="Seg_3428" s="T56">пойти-CVB</ta>
            <ta e="T57" id="Seg_3429" s="T359">исчезнуть-PST.[3SG]</ta>
            <ta e="T58" id="Seg_3430" s="T57">тогда</ta>
            <ta e="T59" id="Seg_3431" s="T58">этот.[NOM.SG]</ta>
            <ta e="T60" id="Seg_3432" s="T59">пойти-PST.[3SG]</ta>
            <ta e="T61" id="Seg_3433" s="T60">пойти-PST.[3SG]</ta>
            <ta e="T62" id="Seg_3434" s="T61">прийти-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_3435" s="T62">болото-LOC</ta>
            <ta e="T64" id="Seg_3436" s="T63">сидеть-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_3437" s="T64">лягушка-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_3438" s="T65">и</ta>
            <ta e="T67" id="Seg_3439" s="T66">стрела.[NOM.SG]</ta>
            <ta e="T68" id="Seg_3440" s="T67">этот-LAT</ta>
            <ta e="T70" id="Seg_3441" s="T69">тот.ABL</ta>
            <ta e="T71" id="Seg_3442" s="T70">этот.[NOM.SG]</ta>
            <ta e="T72" id="Seg_3443" s="T71">сказать-IPFVZ.[3SG]</ta>
            <ta e="T73" id="Seg_3444" s="T72">принести-IMP.2SG</ta>
            <ta e="T74" id="Seg_3445" s="T73">я.LAT</ta>
            <ta e="T75" id="Seg_3446" s="T74">стрела.[NOM.SG]</ta>
            <ta e="T76" id="Seg_3447" s="T75">а</ta>
            <ta e="T77" id="Seg_3448" s="T76">ты.NOM</ta>
            <ta e="T78" id="Seg_3449" s="T77">этот.[NOM.SG]</ta>
            <ta e="T79" id="Seg_3450" s="T78">сказать-IPFVZ.[3SG]</ta>
            <ta e="T80" id="Seg_3451" s="T79">взять-IMP.2SG</ta>
            <ta e="T81" id="Seg_3452" s="T80">я.ACC</ta>
            <ta e="T82" id="Seg_3453" s="T81">мужчина-LAT</ta>
            <ta e="T83" id="Seg_3454" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_3455" s="T83">как</ta>
            <ta e="T85" id="Seg_3456" s="T84">я.NOM</ta>
            <ta e="T86" id="Seg_3457" s="T85">ты.ACC</ta>
            <ta e="T87" id="Seg_3458" s="T86">взять-FUT-1SG</ta>
            <ta e="T88" id="Seg_3459" s="T87">ты.NOM</ta>
            <ta e="T89" id="Seg_3460" s="T88">ведь</ta>
            <ta e="T90" id="Seg_3461" s="T89">лягушка.[NOM.SG]</ta>
            <ta e="T91" id="Seg_3462" s="T90">ну</ta>
            <ta e="T92" id="Seg_3463" s="T91">так</ta>
            <ta e="T93" id="Seg_3464" s="T92">ты.NOM</ta>
            <ta e="T94" id="Seg_3465" s="T93">ты.NOM</ta>
            <ta e="T95" id="Seg_3466" s="T94">судьба-NOM/GEN/ACC.2SG</ta>
            <ta e="T96" id="Seg_3467" s="T95">взять-IMP.2SG.O</ta>
            <ta e="T97" id="Seg_3468" s="T96">я.ACC</ta>
            <ta e="T98" id="Seg_3469" s="T97">тогда</ta>
            <ta e="T99" id="Seg_3470" s="T98">этот.[NOM.SG]</ta>
            <ta e="T100" id="Seg_3471" s="T99">взять-PST.[3SG]</ta>
            <ta e="T101" id="Seg_3472" s="T100">чум-LAT/LOC.3SG</ta>
            <ta e="T102" id="Seg_3473" s="T101">принести-PST.[3SG]</ta>
            <ta e="T103" id="Seg_3474" s="T102">тогда</ta>
            <ta e="T106" id="Seg_3475" s="T105">делать-PST.[3SG]</ta>
            <ta e="T107" id="Seg_3476" s="T106">-PL</ta>
            <ta e="T108" id="Seg_3477" s="T107">тогда</ta>
            <ta e="T109" id="Seg_3478" s="T108">сказать-PST.[3SG]</ta>
            <ta e="T110" id="Seg_3479" s="T109">шить-IMP.2SG.O</ta>
            <ta e="T111" id="Seg_3480" s="T110">рубашка-PL</ta>
            <ta e="T112" id="Seg_3481" s="T111">тогда</ta>
            <ta e="T114" id="Seg_3482" s="T113">прийти-PST.[3SG]</ta>
            <ta e="T115" id="Seg_3483" s="T114">Ванюшка.[NOM.SG]</ta>
            <ta e="T116" id="Seg_3484" s="T115">чум-LAT/LOC.3SG</ta>
            <ta e="T117" id="Seg_3485" s="T116">и</ta>
            <ta e="T118" id="Seg_3486" s="T117">что.[NOM.SG]=INDEF</ta>
            <ta e="T119" id="Seg_3487" s="T118">NEG</ta>
            <ta e="T120" id="Seg_3488" s="T119">говорить-PRS-1SG</ta>
            <ta e="T121" id="Seg_3489" s="T120">что.[NOM.SG]</ta>
            <ta e="T122" id="Seg_3490" s="T121">ты.NOM</ta>
            <ta e="T123" id="Seg_3491" s="T122">что.[NOM.SG]=INDEF</ta>
            <ta e="T124" id="Seg_3492" s="T123">NEG</ta>
            <ta e="T125" id="Seg_3493" s="T124">говорить-PRS.[3SG]</ta>
            <ta e="T126" id="Seg_3494" s="T125">и</ta>
            <ta e="T127" id="Seg_3495" s="T126">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T128" id="Seg_3496" s="T127">сказать-PST.[3SG]</ta>
            <ta e="T129" id="Seg_3497" s="T128">рубашка.[NOM.SG]</ta>
            <ta e="T130" id="Seg_3498" s="T129">шить-INF.LAT</ta>
            <ta e="T131" id="Seg_3499" s="T130">ложиться-IMP.2SG</ta>
            <ta e="T133" id="Seg_3500" s="T132">спать-EP-IMP.2SG</ta>
            <ta e="T134" id="Seg_3501" s="T133">а</ta>
            <ta e="T136" id="Seg_3502" s="T135">рубашка.[NOM.SG]</ta>
            <ta e="T137" id="Seg_3503" s="T136">стать-FUT-3SG</ta>
            <ta e="T138" id="Seg_3504" s="T137">тогда</ta>
            <ta e="T139" id="Seg_3505" s="T138">этот.[NOM.SG]</ta>
            <ta e="T140" id="Seg_3506" s="T139">спать-MOM-PST.[3SG]</ta>
            <ta e="T141" id="Seg_3507" s="T140">а</ta>
            <ta e="T142" id="Seg_3508" s="T141">этот.[NOM.SG]</ta>
            <ta e="T143" id="Seg_3509" s="T142">наружу</ta>
            <ta e="T360" id="Seg_3510" s="T143">пойти-CVB</ta>
            <ta e="T144" id="Seg_3511" s="T360">исчезнуть-PST.[3SG]</ta>
            <ta e="T145" id="Seg_3512" s="T144">кожа-NOM/GEN.3SG</ta>
            <ta e="T146" id="Seg_3513" s="T145">снимать.кору-PST.[3SG]</ta>
            <ta e="T147" id="Seg_3514" s="T146">INCH</ta>
            <ta e="T148" id="Seg_3515" s="T147">кричать-INF.LAT</ta>
            <ta e="T149" id="Seg_3516" s="T148">шить-IMP.2PL</ta>
            <ta e="T150" id="Seg_3517" s="T149">я.LAT</ta>
            <ta e="T151" id="Seg_3518" s="T150">рубашка.[NOM.SG]</ta>
            <ta e="T152" id="Seg_3519" s="T151">отец-LAT</ta>
            <ta e="T153" id="Seg_3520" s="T152">красивый.[NOM.SG]</ta>
            <ta e="T154" id="Seg_3521" s="T153">хороший.[NOM.SG]</ta>
            <ta e="T155" id="Seg_3522" s="T154">тогда</ta>
            <ta e="T156" id="Seg_3523" s="T155">опять</ta>
            <ta e="T157" id="Seg_3524" s="T156">кожа-ACC.3SG</ta>
            <ta e="T158" id="Seg_3525" s="T157">надеть-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3526" s="T159">дом-LAT</ta>
            <ta e="T161" id="Seg_3527" s="T160">прийти-PST.[3SG]</ta>
            <ta e="T162" id="Seg_3528" s="T161">рубашка-NOM/GEN/ACC.3SG</ta>
            <ta e="T163" id="Seg_3529" s="T162">класть-PST.[3SG]</ta>
            <ta e="T164" id="Seg_3530" s="T163">стол-ACC.3SG</ta>
            <ta e="T165" id="Seg_3531" s="T164">стол-ACC.3SG</ta>
            <ta e="T166" id="Seg_3532" s="T165">тогда</ta>
            <ta e="T167" id="Seg_3533" s="T166">встать-PST.[3SG]</ta>
            <ta e="T168" id="Seg_3534" s="T167">мужчина-NOM/GEN.3SG</ta>
            <ta e="T169" id="Seg_3535" s="T168">ну</ta>
            <ta e="T170" id="Seg_3536" s="T169">взять-IMP.2SG.O</ta>
            <ta e="T171" id="Seg_3537" s="T170">рубашка.[NOM.SG]</ta>
            <ta e="T172" id="Seg_3538" s="T171">нести-IMP.2SG.O</ta>
            <ta e="T173" id="Seg_3539" s="T172">отец-LAT/LOC.3SG</ta>
            <ta e="T174" id="Seg_3540" s="T173">этот.[NOM.SG]</ta>
            <ta e="T176" id="Seg_3541" s="T175">пойти-PST.[3SG]</ta>
            <ta e="T177" id="Seg_3542" s="T176">отец-NOM/GEN.3SG</ta>
            <ta e="T178" id="Seg_3543" s="T177">взять-PST.[3SG]</ta>
            <ta e="T179" id="Seg_3544" s="T178">старший.[NOM.SG]</ta>
            <ta e="T180" id="Seg_3545" s="T179">сын-LAT/LOC.3SG</ta>
            <ta e="T181" id="Seg_3546" s="T180">рубашка.[NOM.SG]</ta>
            <ta e="T182" id="Seg_3547" s="T181">этот.[NOM.SG]</ta>
            <ta e="T183" id="Seg_3548" s="T182">только</ta>
            <ta e="T184" id="Seg_3549" s="T183">работать-INF.LAT</ta>
            <ta e="T185" id="Seg_3550" s="T184">этот.[NOM.SG]</ta>
            <ta e="T186" id="Seg_3551" s="T185">рубашка-NOM/GEN/ACC.3PL</ta>
            <ta e="T187" id="Seg_3552" s="T186">а</ta>
            <ta e="T188" id="Seg_3553" s="T187">другой.[NOM.SG]</ta>
            <ta e="T189" id="Seg_3554" s="T188">мальчик-GEN</ta>
            <ta e="T190" id="Seg_3555" s="T189">а</ta>
            <ta e="T191" id="Seg_3556" s="T190">тот.[NOM.SG]</ta>
            <ta e="T192" id="Seg_3557" s="T191">рубашка.[NOM.SG]</ta>
            <ta e="T193" id="Seg_3558" s="T192">только</ta>
            <ta e="T194" id="Seg_3559" s="T193">баня-LAT</ta>
            <ta e="T195" id="Seg_3560" s="T194">дать-INF.LAT</ta>
            <ta e="T196" id="Seg_3561" s="T195">а</ta>
            <ta e="T197" id="Seg_3562" s="T196">этот.[NOM.SG]</ta>
            <ta e="T198" id="Seg_3563" s="T197">мальчик.[NOM.SG]</ta>
            <ta e="T199" id="Seg_3564" s="T198">дать-PST.[3SG]</ta>
            <ta e="T200" id="Seg_3565" s="T199">а</ta>
            <ta e="T201" id="Seg_3566" s="T200">тот.[NOM.SG]</ta>
            <ta e="T202" id="Seg_3567" s="T201">тот.[NOM.SG]</ta>
            <ta e="T203" id="Seg_3568" s="T202">этот.[NOM.SG]</ta>
            <ta e="T204" id="Seg_3569" s="T203">рубашка.[NOM.SG]</ta>
            <ta e="T205" id="Seg_3570" s="T204">только</ta>
            <ta e="T206" id="Seg_3571" s="T205">%%</ta>
            <ta e="T207" id="Seg_3572" s="T206">сидеть-INF.LAT</ta>
            <ta e="T208" id="Seg_3573" s="T207">хватит</ta>
            <ta e="T210" id="Seg_3574" s="T209">этот-GEN</ta>
            <ta e="T211" id="Seg_3575" s="T210">отец-NOM/GEN.3SG</ta>
            <ta e="T212" id="Seg_3576" s="T211">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T213" id="Seg_3577" s="T212">много</ta>
            <ta e="T214" id="Seg_3578" s="T213">собирать-PST.[3SG]</ta>
            <ta e="T215" id="Seg_3579" s="T214">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T216" id="Seg_3580" s="T215">встать-PST.[3SG]</ta>
            <ta e="T217" id="Seg_3581" s="T216">позвать-PST.[3SG]</ta>
            <ta e="T218" id="Seg_3582" s="T217">мальчик-PL</ta>
            <ta e="T219" id="Seg_3583" s="T218">и</ta>
            <ta e="T220" id="Seg_3584" s="T219">женщина-PL</ta>
            <ta e="T221" id="Seg_3585" s="T220">тогда</ta>
            <ta e="T222" id="Seg_3586" s="T221">прийти-PST-3PL</ta>
            <ta e="T223" id="Seg_3587" s="T222">сесть-PST-3PL</ta>
            <ta e="T224" id="Seg_3588" s="T223">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T225" id="Seg_3589" s="T224">мясо.[NOM.SG]</ta>
            <ta e="T226" id="Seg_3590" s="T225">съесть-PST-3PL</ta>
            <ta e="T227" id="Seg_3591" s="T226">и</ta>
            <ta e="T228" id="Seg_3592" s="T227">кость-NOM/GEN/ACC.3PL</ta>
            <ta e="T229" id="Seg_3593" s="T228">%%</ta>
            <ta e="T230" id="Seg_3594" s="T229">тогда</ta>
            <ta e="T231" id="Seg_3595" s="T230">прийти-PST-3PL</ta>
            <ta e="T232" id="Seg_3596" s="T231">этот-GEN</ta>
            <ta e="T233" id="Seg_3597" s="T232">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T234" id="Seg_3598" s="T233">сказать-PRS-3PL</ta>
            <ta e="T235" id="Seg_3599" s="T234">кто.[NOM.SG]=INDEF</ta>
            <ta e="T236" id="Seg_3600" s="T235">прийти-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_3601" s="T236">а</ta>
            <ta e="T238" id="Seg_3602" s="T237">этот.[NOM.SG]</ta>
            <ta e="T240" id="Seg_3603" s="T239">этот-ACC</ta>
            <ta e="T241" id="Seg_3604" s="T240">этот.[NOM.SG]</ta>
            <ta e="T242" id="Seg_3605" s="T241">сказать-IPFVZ.[3SG]</ta>
            <ta e="T243" id="Seg_3606" s="T242">я.NOM</ta>
            <ta e="T244" id="Seg_3607" s="T243">лягушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T245" id="Seg_3608" s="T244">прийти-PRS.[3SG]</ta>
            <ta e="T246" id="Seg_3609" s="T245">тогда</ta>
            <ta e="T247" id="Seg_3610" s="T246">этот.[NOM.SG]</ta>
            <ta e="T248" id="Seg_3611" s="T247">прийти-PST.[3SG]</ta>
            <ta e="T249" id="Seg_3612" s="T248">красивый.[NOM.SG]</ta>
            <ta e="T250" id="Seg_3613" s="T249">PTCL</ta>
            <ta e="T251" id="Seg_3614" s="T250">тогда</ta>
            <ta e="T252" id="Seg_3615" s="T251">этот-PL</ta>
            <ta e="T253" id="Seg_3616" s="T252">сесть-PST-3PL</ta>
            <ta e="T254" id="Seg_3617" s="T253">этот.[NOM.SG]</ta>
            <ta e="T255" id="Seg_3618" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_3619" s="T255">съесть-DUR.[3SG]</ta>
            <ta e="T257" id="Seg_3620" s="T256">и</ta>
            <ta e="T258" id="Seg_3621" s="T257">кость-NOM/GEN/ACC.3PL</ta>
            <ta e="T259" id="Seg_3622" s="T258">рукав-LAT</ta>
            <ta e="T260" id="Seg_3623" s="T259">класть-DUR.[3SG]</ta>
            <ta e="T261" id="Seg_3624" s="T260">водка.[NOM.SG]</ta>
            <ta e="T262" id="Seg_3625" s="T261">рукав-LAT</ta>
            <ta e="T263" id="Seg_3626" s="T262">лить-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_3627" s="T263">чай.[NOM.SG]</ta>
            <ta e="T265" id="Seg_3628" s="T264">там</ta>
            <ta e="T266" id="Seg_3629" s="T265">лить-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_3630" s="T266">тогда</ta>
            <ta e="T268" id="Seg_3631" s="T267">съесть-PST-3PL</ta>
            <ta e="T269" id="Seg_3632" s="T268">пойти-PST-3PL</ta>
            <ta e="T270" id="Seg_3633" s="T269">прыгнуть-INF.LAT</ta>
            <ta e="T271" id="Seg_3634" s="T270">играть-INF.LAT</ta>
            <ta e="T272" id="Seg_3635" s="T271">гармонь-INS</ta>
            <ta e="T273" id="Seg_3636" s="T272">тогда</ta>
            <ta e="T274" id="Seg_3637" s="T273">этот.[NOM.SG]</ta>
            <ta e="T275" id="Seg_3638" s="T274">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T276" id="Seg_3639" s="T275">вода.[NOM.SG]</ta>
            <ta e="T277" id="Seg_3640" s="T276">стать-RES-PST.[3SG]</ta>
            <ta e="T279" id="Seg_3641" s="T278">тогда</ta>
            <ta e="T280" id="Seg_3642" s="T279">один.[NOM.SG]</ta>
            <ta e="T281" id="Seg_3643" s="T280">рука-LAT/LOC.3SG</ta>
            <ta e="T284" id="Seg_3644" s="T283">вода.[NOM.SG]</ta>
            <ta e="T285" id="Seg_3645" s="T284">стать-RES-PST.[3SG]</ta>
            <ta e="T286" id="Seg_3646" s="T285">царь.[NOM.SG]</ta>
            <ta e="T287" id="Seg_3647" s="T286">PTCL</ta>
            <ta e="T288" id="Seg_3648" s="T287">пугать-MOM-PST.[3SG]</ta>
            <ta e="T289" id="Seg_3649" s="T288">другой.[NOM.SG]</ta>
            <ta e="T290" id="Seg_3650" s="T289">рука-LAT/LOC.3SG</ta>
            <ta e="T292" id="Seg_3651" s="T291">тогда</ta>
            <ta e="T293" id="Seg_3652" s="T292">утка-PL</ta>
            <ta e="T294" id="Seg_3653" s="T293">прийти-MOM-PST-3PL</ta>
            <ta e="T295" id="Seg_3654" s="T294">тогда</ta>
            <ta e="T296" id="Seg_3655" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_3656" s="T296">исчезнуть-DUR-PST.[3SG]</ta>
            <ta e="T298" id="Seg_3657" s="T297">тогда</ta>
            <ta e="T299" id="Seg_3658" s="T298">этот-PL</ta>
            <ta e="T300" id="Seg_3659" s="T299">%%</ta>
            <ta e="T301" id="Seg_3660" s="T300">женщина-PL</ta>
            <ta e="T302" id="Seg_3661" s="T301">рука-3SG-INS</ta>
            <ta e="T303" id="Seg_3662" s="T302">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T304" id="Seg_3663" s="T303">PTCL</ta>
            <ta e="T305" id="Seg_3664" s="T304">люди-LAT</ta>
            <ta e="T306" id="Seg_3665" s="T305">и</ta>
            <ta e="T307" id="Seg_3666" s="T306">кость-INS</ta>
            <ta e="T308" id="Seg_3667" s="T307">лицо-LAT</ta>
            <ta e="T309" id="Seg_3668" s="T308">PTCL</ta>
            <ta e="T310" id="Seg_3669" s="T309">ударить-MULT-PST.[3SG]</ta>
            <ta e="T312" id="Seg_3670" s="T311">вода-INS</ta>
            <ta e="T313" id="Seg_3671" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_3672" s="T313">глаз-3SG-LAT</ta>
            <ta e="T315" id="Seg_3673" s="T314">PTCL</ta>
            <ta e="T316" id="Seg_3674" s="T315">вода.[NOM.SG]</ta>
            <ta e="T318" id="Seg_3675" s="T317">лить-PST-3PL</ta>
            <ta e="T319" id="Seg_3676" s="T318">хватит</ta>
            <ta e="T320" id="Seg_3677" s="T319">тогда</ta>
            <ta e="T321" id="Seg_3678" s="T320">этот.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3679" s="T321">бежать-MOM-PST.[3SG]</ta>
            <ta e="T323" id="Seg_3680" s="T322">чум-LAT/LOC.3SG</ta>
            <ta e="T324" id="Seg_3681" s="T323">кожа-ACC.3SG</ta>
            <ta e="T325" id="Seg_3682" s="T324">найти-PST.[3SG]</ta>
            <ta e="T326" id="Seg_3683" s="T325">и</ta>
            <ta e="T327" id="Seg_3684" s="T326">взять-PST.[3SG]</ta>
            <ta e="T328" id="Seg_3685" s="T327">огонь-LAT</ta>
            <ta e="T330" id="Seg_3686" s="T329">класть-PST.[3SG]</ta>
            <ta e="T331" id="Seg_3687" s="T330">этот.[NOM.SG]</ta>
            <ta e="T332" id="Seg_3688" s="T331">PTCL</ta>
            <ta e="T333" id="Seg_3689" s="T332">%гореть-MOM-PST.[3SG]</ta>
            <ta e="T334" id="Seg_3690" s="T333">тогда</ta>
            <ta e="T335" id="Seg_3691" s="T334">этот.[NOM.SG]</ta>
            <ta e="T336" id="Seg_3692" s="T335">прийти-PST.[3SG]</ta>
            <ta e="T337" id="Seg_3693" s="T336">чум-LAT/LOC.3SG</ta>
            <ta e="T338" id="Seg_3694" s="T337">очень</ta>
            <ta e="T339" id="Seg_3695" s="T338">ты.NOM</ta>
            <ta e="T340" id="Seg_3696" s="T339">NEG</ta>
            <ta e="T341" id="Seg_3697" s="T340">хороший.[NOM.SG]</ta>
            <ta e="T342" id="Seg_3698" s="T341">делать-PST-2SG</ta>
            <ta e="T343" id="Seg_3699" s="T342">сейчас</ta>
            <ta e="T344" id="Seg_3700" s="T343">я.NOM</ta>
            <ta e="T361" id="Seg_3701" s="T344">пойти-CVB</ta>
            <ta e="T345" id="Seg_3702" s="T361">исчезнуть-FUT-1SG</ta>
            <ta e="T346" id="Seg_3703" s="T345">а</ta>
            <ta e="T347" id="Seg_3704" s="T346">ты.NOM</ta>
            <ta e="T348" id="Seg_3705" s="T347">я.ACC</ta>
            <ta e="T349" id="Seg_3706" s="T348">смотреть-FRQ-FUT-2SG</ta>
            <ta e="T350" id="Seg_3707" s="T349">этот.[NOM.SG]</ta>
            <ta e="T362" id="Seg_3708" s="T350">пойти-CVB</ta>
            <ta e="T351" id="Seg_3709" s="T362">исчезнуть-PST.[3SG]</ta>
            <ta e="T352" id="Seg_3710" s="T351">а</ta>
            <ta e="T353" id="Seg_3711" s="T352">этот.[NOM.SG]</ta>
            <ta e="T354" id="Seg_3712" s="T353">PTCL</ta>
            <ta e="T355" id="Seg_3713" s="T354">плакать-PST.[3SG]</ta>
            <ta e="T356" id="Seg_3714" s="T355">плакать-PST.[3SG]</ta>
            <ta e="T357" id="Seg_3715" s="T356">тогда</ta>
            <ta e="T358" id="Seg_3716" s="T357">пойти-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3717" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_3718" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_3719" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_3720" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_3721" s="T4">num-n:case</ta>
            <ta e="T6" id="Seg_3722" s="T5">n-n:num-n:case.poss</ta>
            <ta e="T7" id="Seg_3723" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_3724" s="T7">adv</ta>
            <ta e="T9" id="Seg_3725" s="T8">dempro-n:case</ta>
            <ta e="T10" id="Seg_3726" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_3727" s="T10">n-n:num-n:case</ta>
            <ta e="T12" id="Seg_3728" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_3729" s="T12">pers</ta>
            <ta e="T14" id="Seg_3730" s="T13">v-v:n.fin</ta>
            <ta e="T15" id="Seg_3731" s="T14">n-n:num</ta>
            <ta e="T16" id="Seg_3732" s="T15">conj</ta>
            <ta e="T17" id="Seg_3733" s="T16">pers</ta>
            <ta e="T18" id="Seg_3734" s="T17">v-v:n.fin</ta>
            <ta e="T20" id="Seg_3735" s="T19">pers</ta>
            <ta e="T21" id="Seg_3736" s="T20">n-n:num</ta>
            <ta e="T22" id="Seg_3737" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_3738" s="T22">dempro-n:num</ta>
            <ta e="T24" id="Seg_3739" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_3740" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3741" s="T25">v-v:mood.pn</ta>
            <ta e="T27" id="Seg_3742" s="T26">que-n:case</ta>
            <ta e="T28" id="Seg_3743" s="T27">pers</ta>
            <ta e="T29" id="Seg_3744" s="T28">adv</ta>
            <ta e="T30" id="Seg_3745" s="T29">v-v:mood.pn</ta>
            <ta e="T31" id="Seg_3746" s="T30">n-n:num</ta>
            <ta e="T32" id="Seg_3747" s="T31">conj</ta>
            <ta e="T33" id="Seg_3748" s="T32">v-v:mood.pn</ta>
            <ta e="T34" id="Seg_3749" s="T33">v-v:mood.pn</ta>
            <ta e="T35" id="Seg_3750" s="T34">n-n:num</ta>
            <ta e="T36" id="Seg_3751" s="T35">adv</ta>
            <ta e="T37" id="Seg_3752" s="T36">dempro-n:case</ta>
            <ta e="T38" id="Seg_3753" s="T37">dempro-n:num</ta>
            <ta e="T39" id="Seg_3754" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_3755" s="T39">num-n:case</ta>
            <ta e="T41" id="Seg_3756" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_3757" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_3758" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_3759" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_3760" s="T45">num-n:case</ta>
            <ta e="T47" id="Seg_3761" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_3762" s="T47">v-v&gt;v-v:tense-v:pn </ta>
            <ta e="T52" id="Seg_3763" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_3764" s="T52">conj</ta>
            <ta e="T54" id="Seg_3765" s="T53">num-n:case</ta>
            <ta e="T55" id="Seg_3766" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_3767" s="T55">adv-adv&gt;adv</ta>
            <ta e="T359" id="Seg_3768" s="T56">v-v:n-fin</ta>
            <ta e="T57" id="Seg_3769" s="T359">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_3770" s="T57">adv</ta>
            <ta e="T59" id="Seg_3771" s="T58">dempro-n:case</ta>
            <ta e="T60" id="Seg_3772" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_3773" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_3774" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_3775" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_3776" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_3777" s="T64">n-n:case.poss</ta>
            <ta e="T66" id="Seg_3778" s="T65">conj</ta>
            <ta e="T67" id="Seg_3779" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_3780" s="T67">dempro-n:case</ta>
            <ta e="T70" id="Seg_3781" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3782" s="T70">dempro-n:case</ta>
            <ta e="T72" id="Seg_3783" s="T71">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_3784" s="T72">v-v:mood.pn</ta>
            <ta e="T74" id="Seg_3785" s="T73">pers</ta>
            <ta e="T75" id="Seg_3786" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_3787" s="T75">conj</ta>
            <ta e="T77" id="Seg_3788" s="T76">pers</ta>
            <ta e="T78" id="Seg_3789" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_3790" s="T78">v-v&gt;v-v:pn</ta>
            <ta e="T80" id="Seg_3791" s="T79">v-v:mood.pn</ta>
            <ta e="T81" id="Seg_3792" s="T80">pers</ta>
            <ta e="T82" id="Seg_3793" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_3794" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_3795" s="T83">que</ta>
            <ta e="T85" id="Seg_3796" s="T84">pers</ta>
            <ta e="T86" id="Seg_3797" s="T85">pers</ta>
            <ta e="T87" id="Seg_3798" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_3799" s="T87">pers</ta>
            <ta e="T89" id="Seg_3800" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_3801" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_3802" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_3803" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_3804" s="T92">pers</ta>
            <ta e="T94" id="Seg_3805" s="T93">pers</ta>
            <ta e="T95" id="Seg_3806" s="T94">n-n:case.poss</ta>
            <ta e="T96" id="Seg_3807" s="T95">v-v:mood.pn</ta>
            <ta e="T97" id="Seg_3808" s="T96">pers</ta>
            <ta e="T98" id="Seg_3809" s="T97">adv</ta>
            <ta e="T99" id="Seg_3810" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_3811" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_3812" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_3813" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_3814" s="T102">adv</ta>
            <ta e="T106" id="Seg_3815" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_3816" s="T106">n:num</ta>
            <ta e="T108" id="Seg_3817" s="T107">adv</ta>
            <ta e="T109" id="Seg_3818" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_3819" s="T109">v-v:mood.pn</ta>
            <ta e="T111" id="Seg_3820" s="T110">n-n:num</ta>
            <ta e="T112" id="Seg_3821" s="T111">adv</ta>
            <ta e="T114" id="Seg_3822" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_3823" s="T114">propr-n:case</ta>
            <ta e="T116" id="Seg_3824" s="T115">n-n:case.poss</ta>
            <ta e="T117" id="Seg_3825" s="T116">conj</ta>
            <ta e="T118" id="Seg_3826" s="T117">que-n:case=ptcl</ta>
            <ta e="T119" id="Seg_3827" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_3828" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_3829" s="T120">que-n:case</ta>
            <ta e="T122" id="Seg_3830" s="T121">pers</ta>
            <ta e="T123" id="Seg_3831" s="T122">que-n:case=ptcl</ta>
            <ta e="T124" id="Seg_3832" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_3833" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_3834" s="T125">conj</ta>
            <ta e="T127" id="Seg_3835" s="T126">n-n:case.poss</ta>
            <ta e="T128" id="Seg_3836" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_3837" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_3838" s="T129">v-v:n.fin</ta>
            <ta e="T131" id="Seg_3839" s="T130">v-v:mood.pn</ta>
            <ta e="T133" id="Seg_3840" s="T132">v-v:ins-v:mood.pn</ta>
            <ta e="T134" id="Seg_3841" s="T133">conj</ta>
            <ta e="T136" id="Seg_3842" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_3843" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_3844" s="T137">adv</ta>
            <ta e="T139" id="Seg_3845" s="T138">dempro-n:case</ta>
            <ta e="T140" id="Seg_3846" s="T139">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_3847" s="T140">conj</ta>
            <ta e="T142" id="Seg_3848" s="T141">dempro-n:case</ta>
            <ta e="T143" id="Seg_3849" s="T142">adv</ta>
            <ta e="T360" id="Seg_3850" s="T143">v-v:n-fin</ta>
            <ta e="T144" id="Seg_3851" s="T360">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_3852" s="T144">n-n:case.poss</ta>
            <ta e="T146" id="Seg_3853" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_3854" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_3855" s="T147">v-v:n.fin</ta>
            <ta e="T149" id="Seg_3856" s="T148">v-v:mood.pn</ta>
            <ta e="T150" id="Seg_3857" s="T149">pers</ta>
            <ta e="T151" id="Seg_3858" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3859" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_3860" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_3861" s="T153">adj-n:case</ta>
            <ta e="T155" id="Seg_3862" s="T154">adv</ta>
            <ta e="T156" id="Seg_3863" s="T155">adv</ta>
            <ta e="T157" id="Seg_3864" s="T156">n-n:case.poss</ta>
            <ta e="T158" id="Seg_3865" s="T157">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_3866" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_3867" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_3868" s="T161">n-n:case.poss</ta>
            <ta e="T163" id="Seg_3869" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_3870" s="T163">n-n:case.poss</ta>
            <ta e="T165" id="Seg_3871" s="T164">n-n:case.poss</ta>
            <ta e="T166" id="Seg_3872" s="T165">adv</ta>
            <ta e="T167" id="Seg_3873" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_3874" s="T167">n-n:case.poss</ta>
            <ta e="T169" id="Seg_3875" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_3876" s="T169">v-v:mood.pn</ta>
            <ta e="T171" id="Seg_3877" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_3878" s="T171">v-v:mood.pn</ta>
            <ta e="T173" id="Seg_3879" s="T172">n-n:case.poss</ta>
            <ta e="T174" id="Seg_3880" s="T173">dempro-n:case</ta>
            <ta e="T176" id="Seg_3881" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_3882" s="T176">n-n:case.poss</ta>
            <ta e="T178" id="Seg_3883" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_3884" s="T178">adj-n:case</ta>
            <ta e="T180" id="Seg_3885" s="T179">n-n:case.poss</ta>
            <ta e="T181" id="Seg_3886" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_3887" s="T181">dempro-n:case</ta>
            <ta e="T183" id="Seg_3888" s="T182">adv</ta>
            <ta e="T184" id="Seg_3889" s="T183">v-v:n.fin</ta>
            <ta e="T185" id="Seg_3890" s="T184">dempro-n:case</ta>
            <ta e="T186" id="Seg_3891" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_3892" s="T186">conj</ta>
            <ta e="T188" id="Seg_3893" s="T187">adj-n:case</ta>
            <ta e="T189" id="Seg_3894" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_3895" s="T189">conj</ta>
            <ta e="T191" id="Seg_3896" s="T190">dempro-n:case</ta>
            <ta e="T192" id="Seg_3897" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_3898" s="T192">adv</ta>
            <ta e="T194" id="Seg_3899" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_3900" s="T194">v-v:n.fin</ta>
            <ta e="T196" id="Seg_3901" s="T195">conj</ta>
            <ta e="T197" id="Seg_3902" s="T196">dempro-n:case</ta>
            <ta e="T198" id="Seg_3903" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3904" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_3905" s="T199">conj</ta>
            <ta e="T201" id="Seg_3906" s="T200">dempro-n:case</ta>
            <ta e="T202" id="Seg_3907" s="T201">dempro-n:case</ta>
            <ta e="T203" id="Seg_3908" s="T202">dempro-n:case</ta>
            <ta e="T204" id="Seg_3909" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_3910" s="T204">adv</ta>
            <ta e="T206" id="Seg_3911" s="T205">%%</ta>
            <ta e="T207" id="Seg_3912" s="T206">v-v:n.fin</ta>
            <ta e="T208" id="Seg_3913" s="T207">ptcl</ta>
            <ta e="T210" id="Seg_3914" s="T209">dempro-n:case</ta>
            <ta e="T211" id="Seg_3915" s="T210">n-n:case.poss</ta>
            <ta e="T212" id="Seg_3916" s="T211">n-n:case.poss</ta>
            <ta e="T213" id="Seg_3917" s="T212">quant</ta>
            <ta e="T214" id="Seg_3918" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_3919" s="T214">n-n:case.poss</ta>
            <ta e="T216" id="Seg_3920" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_3921" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_3922" s="T217">n-n:num</ta>
            <ta e="T219" id="Seg_3923" s="T218">conj</ta>
            <ta e="T220" id="Seg_3924" s="T219">n-n:num</ta>
            <ta e="T221" id="Seg_3925" s="T220">adv</ta>
            <ta e="T222" id="Seg_3926" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_3927" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_3928" s="T223">n-n:case.poss</ta>
            <ta e="T225" id="Seg_3929" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_3930" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_3931" s="T226">conj</ta>
            <ta e="T228" id="Seg_3932" s="T227">n-n:case.poss</ta>
            <ta e="T229" id="Seg_3933" s="T228">%%</ta>
            <ta e="T230" id="Seg_3934" s="T229">adv</ta>
            <ta e="T231" id="Seg_3935" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_3936" s="T231">dempro-n:case</ta>
            <ta e="T233" id="Seg_3937" s="T232">n-n:num-n:case.poss</ta>
            <ta e="T234" id="Seg_3938" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_3939" s="T234">que-n:case=ptcl</ta>
            <ta e="T236" id="Seg_3940" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_3941" s="T236">conj</ta>
            <ta e="T238" id="Seg_3942" s="T237">dempro-n:case</ta>
            <ta e="T240" id="Seg_3943" s="T239">dempro-n:case</ta>
            <ta e="T241" id="Seg_3944" s="T240">dempro-n:case</ta>
            <ta e="T242" id="Seg_3945" s="T241">v-v&gt;v-v:pn</ta>
            <ta e="T243" id="Seg_3946" s="T242">pers</ta>
            <ta e="T244" id="Seg_3947" s="T243">n-n:case.poss</ta>
            <ta e="T245" id="Seg_3948" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_3949" s="T245">adv</ta>
            <ta e="T247" id="Seg_3950" s="T246">dempro-n:case</ta>
            <ta e="T248" id="Seg_3951" s="T247">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_3952" s="T248">adj-n:case</ta>
            <ta e="T250" id="Seg_3953" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3954" s="T250">adv</ta>
            <ta e="T252" id="Seg_3955" s="T251">dempro-n:num</ta>
            <ta e="T253" id="Seg_3956" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_3957" s="T253">dempro-n:case</ta>
            <ta e="T255" id="Seg_3958" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_3959" s="T255">v-v&gt;v-v:pn</ta>
            <ta e="T257" id="Seg_3960" s="T256">conj</ta>
            <ta e="T258" id="Seg_3961" s="T257">n-n:case.poss</ta>
            <ta e="T259" id="Seg_3962" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_3963" s="T259">v-v&gt;v-v:pn</ta>
            <ta e="T261" id="Seg_3964" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_3965" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_3966" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_3967" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_3968" s="T264">adv</ta>
            <ta e="T266" id="Seg_3969" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_3970" s="T266">adv</ta>
            <ta e="T268" id="Seg_3971" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_3972" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_3973" s="T269">v-v:n.fin</ta>
            <ta e="T271" id="Seg_3974" s="T270">v-v:n.fin</ta>
            <ta e="T272" id="Seg_3975" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_3976" s="T272">adv</ta>
            <ta e="T274" id="Seg_3977" s="T273">dempro-n:case</ta>
            <ta e="T275" id="Seg_3978" s="T274">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_3979" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_3980" s="T276">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_3981" s="T278">adv</ta>
            <ta e="T280" id="Seg_3982" s="T279">num-n:case</ta>
            <ta e="T281" id="Seg_3983" s="T280">n-n:case.poss</ta>
            <ta e="T284" id="Seg_3984" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_3985" s="T284">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_3986" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_3987" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_3988" s="T287">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_3989" s="T288">adj-n:case</ta>
            <ta e="T290" id="Seg_3990" s="T289">n-n:case.poss</ta>
            <ta e="T292" id="Seg_3991" s="T291">adv</ta>
            <ta e="T293" id="Seg_3992" s="T292">n-n:num</ta>
            <ta e="T294" id="Seg_3993" s="T293">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_3994" s="T294">adv</ta>
            <ta e="T296" id="Seg_3995" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3996" s="T296">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_3997" s="T297">adv</ta>
            <ta e="T299" id="Seg_3998" s="T298">dempro-n:num</ta>
            <ta e="T300" id="Seg_3999" s="T299">%%</ta>
            <ta e="T301" id="Seg_4000" s="T300">n-n:num</ta>
            <ta e="T302" id="Seg_4001" s="T301">n-n:case.poss-n:case</ta>
            <ta e="T303" id="Seg_4002" s="T302">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_4003" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_4004" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_4005" s="T305">conj</ta>
            <ta e="T307" id="Seg_4006" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_4007" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_4008" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4009" s="T309">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_4010" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_4011" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_4012" s="T313">n-n:case.poss-n:case</ta>
            <ta e="T315" id="Seg_4013" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_4014" s="T315">n-n:case</ta>
            <ta e="T318" id="Seg_4015" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_4016" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4017" s="T319">adv</ta>
            <ta e="T321" id="Seg_4018" s="T320">dempro-n:case</ta>
            <ta e="T322" id="Seg_4019" s="T321">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_4020" s="T322">n-n:case.poss</ta>
            <ta e="T324" id="Seg_4021" s="T323">n-n:case.poss</ta>
            <ta e="T325" id="Seg_4022" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_4023" s="T325">conj</ta>
            <ta e="T327" id="Seg_4024" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_4025" s="T327">n-n:case</ta>
            <ta e="T330" id="Seg_4026" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_4027" s="T330">dempro-n:case</ta>
            <ta e="T332" id="Seg_4028" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_4029" s="T332">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_4030" s="T333">adv</ta>
            <ta e="T335" id="Seg_4031" s="T334">dempro-n:case</ta>
            <ta e="T336" id="Seg_4032" s="T335">v-v:tense-v:pn</ta>
            <ta e="T337" id="Seg_4033" s="T336">n-n:case.poss</ta>
            <ta e="T338" id="Seg_4034" s="T337">adv</ta>
            <ta e="T339" id="Seg_4035" s="T338">pers</ta>
            <ta e="T340" id="Seg_4036" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_4037" s="T340">adj-n:case</ta>
            <ta e="T342" id="Seg_4038" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_4039" s="T342">adv</ta>
            <ta e="T344" id="Seg_4040" s="T343">pers</ta>
            <ta e="T361" id="Seg_4041" s="T344">v-v:n-fin</ta>
            <ta e="T345" id="Seg_4042" s="T361">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_4043" s="T345">conj</ta>
            <ta e="T347" id="Seg_4044" s="T346">pers</ta>
            <ta e="T348" id="Seg_4045" s="T347">pers</ta>
            <ta e="T349" id="Seg_4046" s="T348">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_4047" s="T349">dempro-n:case</ta>
            <ta e="T362" id="Seg_4048" s="T350">v-v:n-fin</ta>
            <ta e="T351" id="Seg_4049" s="T362">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_4050" s="T351">conj</ta>
            <ta e="T353" id="Seg_4051" s="T352">dempro-n:case</ta>
            <ta e="T354" id="Seg_4052" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_4053" s="T354">v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_4054" s="T355">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_4055" s="T356">adv</ta>
            <ta e="T358" id="Seg_4056" s="T357">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4057" s="T0">num</ta>
            <ta e="T2" id="Seg_4058" s="T1">n</ta>
            <ta e="T3" id="Seg_4059" s="T2">v</ta>
            <ta e="T4" id="Seg_4060" s="T3">dempro</ta>
            <ta e="T5" id="Seg_4061" s="T4">num</ta>
            <ta e="T6" id="Seg_4062" s="T5">n</ta>
            <ta e="T7" id="Seg_4063" s="T6">v</ta>
            <ta e="T8" id="Seg_4064" s="T7">adv</ta>
            <ta e="T9" id="Seg_4065" s="T8">dempro</ta>
            <ta e="T10" id="Seg_4066" s="T9">v</ta>
            <ta e="T11" id="Seg_4067" s="T10">n</ta>
            <ta e="T12" id="Seg_4068" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_4069" s="T12">pers</ta>
            <ta e="T14" id="Seg_4070" s="T13">v</ta>
            <ta e="T15" id="Seg_4071" s="T14">n</ta>
            <ta e="T16" id="Seg_4072" s="T15">conj</ta>
            <ta e="T17" id="Seg_4073" s="T16">pers</ta>
            <ta e="T18" id="Seg_4074" s="T17">v</ta>
            <ta e="T20" id="Seg_4075" s="T19">pers</ta>
            <ta e="T21" id="Seg_4076" s="T20">n</ta>
            <ta e="T22" id="Seg_4077" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_4078" s="T22">dempro</ta>
            <ta e="T24" id="Seg_4079" s="T23">v</ta>
            <ta e="T25" id="Seg_4080" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_4081" s="T25">v</ta>
            <ta e="T27" id="Seg_4082" s="T26">que</ta>
            <ta e="T28" id="Seg_4083" s="T27">pers</ta>
            <ta e="T29" id="Seg_4084" s="T28">adv</ta>
            <ta e="T30" id="Seg_4085" s="T29">v</ta>
            <ta e="T31" id="Seg_4086" s="T30">n</ta>
            <ta e="T32" id="Seg_4087" s="T31">conj</ta>
            <ta e="T33" id="Seg_4088" s="T32">v</ta>
            <ta e="T34" id="Seg_4089" s="T33">v</ta>
            <ta e="T35" id="Seg_4090" s="T34">n</ta>
            <ta e="T36" id="Seg_4091" s="T35">adv</ta>
            <ta e="T37" id="Seg_4092" s="T36">dempro</ta>
            <ta e="T38" id="Seg_4093" s="T37">dempro</ta>
            <ta e="T39" id="Seg_4094" s="T38">v</ta>
            <ta e="T40" id="Seg_4095" s="T39">num</ta>
            <ta e="T41" id="Seg_4096" s="T40">v</ta>
            <ta e="T42" id="Seg_4097" s="T41">v</ta>
            <ta e="T44" id="Seg_4098" s="T43">n</ta>
            <ta e="T45" id="Seg_4099" s="T44">n</ta>
            <ta e="T46" id="Seg_4100" s="T45">num</ta>
            <ta e="T47" id="Seg_4101" s="T46">v</ta>
            <ta e="T48" id="Seg_4102" s="T47">v</ta>
            <ta e="T52" id="Seg_4103" s="T51">n</ta>
            <ta e="T53" id="Seg_4104" s="T52">conj</ta>
            <ta e="T54" id="Seg_4105" s="T53">num</ta>
            <ta e="T55" id="Seg_4106" s="T54">v</ta>
            <ta e="T56" id="Seg_4107" s="T55">adv</ta>
            <ta e="T359" id="Seg_4108" s="T56">v</ta>
            <ta e="T57" id="Seg_4109" s="T359">v</ta>
            <ta e="T58" id="Seg_4110" s="T57">adv</ta>
            <ta e="T59" id="Seg_4111" s="T58">dempro</ta>
            <ta e="T60" id="Seg_4112" s="T59">v</ta>
            <ta e="T61" id="Seg_4113" s="T60">v</ta>
            <ta e="T62" id="Seg_4114" s="T61">v</ta>
            <ta e="T63" id="Seg_4115" s="T62">n</ta>
            <ta e="T64" id="Seg_4116" s="T63">v</ta>
            <ta e="T65" id="Seg_4117" s="T64">n</ta>
            <ta e="T66" id="Seg_4118" s="T65">conj</ta>
            <ta e="T67" id="Seg_4119" s="T66">n</ta>
            <ta e="T68" id="Seg_4120" s="T67">dempro</ta>
            <ta e="T70" id="Seg_4121" s="T69">dempro</ta>
            <ta e="T71" id="Seg_4122" s="T70">dempro</ta>
            <ta e="T72" id="Seg_4123" s="T71">v</ta>
            <ta e="T73" id="Seg_4124" s="T72">v</ta>
            <ta e="T74" id="Seg_4125" s="T73">pers</ta>
            <ta e="T75" id="Seg_4126" s="T74">n</ta>
            <ta e="T76" id="Seg_4127" s="T75">conj</ta>
            <ta e="T77" id="Seg_4128" s="T76">pers</ta>
            <ta e="T78" id="Seg_4129" s="T77">dempro</ta>
            <ta e="T79" id="Seg_4130" s="T78">v</ta>
            <ta e="T80" id="Seg_4131" s="T79">v</ta>
            <ta e="T81" id="Seg_4132" s="T80">pers</ta>
            <ta e="T82" id="Seg_4133" s="T81">n</ta>
            <ta e="T83" id="Seg_4134" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_4135" s="T83">que</ta>
            <ta e="T85" id="Seg_4136" s="T84">pers</ta>
            <ta e="T86" id="Seg_4137" s="T85">pers</ta>
            <ta e="T87" id="Seg_4138" s="T86">v</ta>
            <ta e="T88" id="Seg_4139" s="T87">pers</ta>
            <ta e="T89" id="Seg_4140" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_4141" s="T89">n</ta>
            <ta e="T91" id="Seg_4142" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_4143" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_4144" s="T92">pers</ta>
            <ta e="T94" id="Seg_4145" s="T93">pers</ta>
            <ta e="T95" id="Seg_4146" s="T94">n</ta>
            <ta e="T96" id="Seg_4147" s="T95">v</ta>
            <ta e="T97" id="Seg_4148" s="T96">pers</ta>
            <ta e="T98" id="Seg_4149" s="T97">adv</ta>
            <ta e="T99" id="Seg_4150" s="T98">dempro</ta>
            <ta e="T100" id="Seg_4151" s="T99">v</ta>
            <ta e="T101" id="Seg_4152" s="T100">n</ta>
            <ta e="T102" id="Seg_4153" s="T101">v</ta>
            <ta e="T103" id="Seg_4154" s="T102">adv</ta>
            <ta e="T106" id="Seg_4155" s="T105">v</ta>
            <ta e="T108" id="Seg_4156" s="T107">adv</ta>
            <ta e="T109" id="Seg_4157" s="T108">v</ta>
            <ta e="T110" id="Seg_4158" s="T109">v</ta>
            <ta e="T111" id="Seg_4159" s="T110">n</ta>
            <ta e="T112" id="Seg_4160" s="T111">adv</ta>
            <ta e="T114" id="Seg_4161" s="T113">v</ta>
            <ta e="T115" id="Seg_4162" s="T114">propr</ta>
            <ta e="T116" id="Seg_4163" s="T115">n</ta>
            <ta e="T117" id="Seg_4164" s="T116">conj</ta>
            <ta e="T118" id="Seg_4165" s="T117">que</ta>
            <ta e="T119" id="Seg_4166" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_4167" s="T119">v</ta>
            <ta e="T121" id="Seg_4168" s="T120">que</ta>
            <ta e="T122" id="Seg_4169" s="T121">pers</ta>
            <ta e="T123" id="Seg_4170" s="T122">que</ta>
            <ta e="T124" id="Seg_4171" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_4172" s="T124">v</ta>
            <ta e="T126" id="Seg_4173" s="T125">conj</ta>
            <ta e="T127" id="Seg_4174" s="T126">n</ta>
            <ta e="T128" id="Seg_4175" s="T127">v</ta>
            <ta e="T129" id="Seg_4176" s="T128">n</ta>
            <ta e="T130" id="Seg_4177" s="T129">v</ta>
            <ta e="T131" id="Seg_4178" s="T130">v</ta>
            <ta e="T133" id="Seg_4179" s="T132">v</ta>
            <ta e="T134" id="Seg_4180" s="T133">conj</ta>
            <ta e="T136" id="Seg_4181" s="T135">n</ta>
            <ta e="T137" id="Seg_4182" s="T136">v</ta>
            <ta e="T138" id="Seg_4183" s="T137">adv</ta>
            <ta e="T139" id="Seg_4184" s="T138">dempro</ta>
            <ta e="T140" id="Seg_4185" s="T139">v</ta>
            <ta e="T141" id="Seg_4186" s="T140">conj</ta>
            <ta e="T142" id="Seg_4187" s="T141">dempro</ta>
            <ta e="T143" id="Seg_4188" s="T142">adv</ta>
            <ta e="T360" id="Seg_4189" s="T143">v</ta>
            <ta e="T144" id="Seg_4190" s="T360">v</ta>
            <ta e="T145" id="Seg_4191" s="T144">n</ta>
            <ta e="T146" id="Seg_4192" s="T145">v</ta>
            <ta e="T147" id="Seg_4193" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_4194" s="T147">v</ta>
            <ta e="T149" id="Seg_4195" s="T148">v</ta>
            <ta e="T150" id="Seg_4196" s="T149">pers</ta>
            <ta e="T151" id="Seg_4197" s="T150">n</ta>
            <ta e="T152" id="Seg_4198" s="T151">n</ta>
            <ta e="T153" id="Seg_4199" s="T152">adj</ta>
            <ta e="T154" id="Seg_4200" s="T153">adj</ta>
            <ta e="T155" id="Seg_4201" s="T154">adv</ta>
            <ta e="T156" id="Seg_4202" s="T155">adv</ta>
            <ta e="T157" id="Seg_4203" s="T156">n</ta>
            <ta e="T158" id="Seg_4204" s="T157">v</ta>
            <ta e="T160" id="Seg_4205" s="T159">n</ta>
            <ta e="T161" id="Seg_4206" s="T160">v</ta>
            <ta e="T162" id="Seg_4207" s="T161">n</ta>
            <ta e="T163" id="Seg_4208" s="T162">v</ta>
            <ta e="T164" id="Seg_4209" s="T163">n</ta>
            <ta e="T165" id="Seg_4210" s="T164">n</ta>
            <ta e="T166" id="Seg_4211" s="T165">adv</ta>
            <ta e="T167" id="Seg_4212" s="T166">v</ta>
            <ta e="T168" id="Seg_4213" s="T167">n</ta>
            <ta e="T169" id="Seg_4214" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_4215" s="T169">v</ta>
            <ta e="T171" id="Seg_4216" s="T170">n</ta>
            <ta e="T172" id="Seg_4217" s="T171">v</ta>
            <ta e="T173" id="Seg_4218" s="T172">n</ta>
            <ta e="T174" id="Seg_4219" s="T173">dempro</ta>
            <ta e="T176" id="Seg_4220" s="T175">v</ta>
            <ta e="T177" id="Seg_4221" s="T176">n</ta>
            <ta e="T178" id="Seg_4222" s="T177">v</ta>
            <ta e="T179" id="Seg_4223" s="T178">adj</ta>
            <ta e="T180" id="Seg_4224" s="T179">n</ta>
            <ta e="T181" id="Seg_4225" s="T180">n</ta>
            <ta e="T182" id="Seg_4226" s="T181">dempro</ta>
            <ta e="T183" id="Seg_4227" s="T182">adv</ta>
            <ta e="T184" id="Seg_4228" s="T183">v</ta>
            <ta e="T185" id="Seg_4229" s="T184">dempro</ta>
            <ta e="T186" id="Seg_4230" s="T185">n</ta>
            <ta e="T187" id="Seg_4231" s="T186">conj</ta>
            <ta e="T188" id="Seg_4232" s="T187">adj</ta>
            <ta e="T189" id="Seg_4233" s="T188">n</ta>
            <ta e="T190" id="Seg_4234" s="T189">conj</ta>
            <ta e="T191" id="Seg_4235" s="T190">dempro</ta>
            <ta e="T192" id="Seg_4236" s="T191">n</ta>
            <ta e="T193" id="Seg_4237" s="T192">adv</ta>
            <ta e="T194" id="Seg_4238" s="T193">n</ta>
            <ta e="T195" id="Seg_4239" s="T194">v</ta>
            <ta e="T196" id="Seg_4240" s="T195">conj</ta>
            <ta e="T197" id="Seg_4241" s="T196">dempro</ta>
            <ta e="T198" id="Seg_4242" s="T197">n</ta>
            <ta e="T199" id="Seg_4243" s="T198">v</ta>
            <ta e="T200" id="Seg_4244" s="T199">conj</ta>
            <ta e="T201" id="Seg_4245" s="T200">dempro</ta>
            <ta e="T202" id="Seg_4246" s="T201">dempro</ta>
            <ta e="T203" id="Seg_4247" s="T202">dempro</ta>
            <ta e="T204" id="Seg_4248" s="T203">n</ta>
            <ta e="T205" id="Seg_4249" s="T204">adv</ta>
            <ta e="T207" id="Seg_4250" s="T206">v</ta>
            <ta e="T208" id="Seg_4251" s="T207">ptcl</ta>
            <ta e="T210" id="Seg_4252" s="T209">dempro</ta>
            <ta e="T211" id="Seg_4253" s="T210">n</ta>
            <ta e="T212" id="Seg_4254" s="T211">n</ta>
            <ta e="T213" id="Seg_4255" s="T212">quant</ta>
            <ta e="T214" id="Seg_4256" s="T213">v</ta>
            <ta e="T215" id="Seg_4257" s="T214">n</ta>
            <ta e="T216" id="Seg_4258" s="T215">v</ta>
            <ta e="T217" id="Seg_4259" s="T216">v</ta>
            <ta e="T218" id="Seg_4260" s="T217">n</ta>
            <ta e="T219" id="Seg_4261" s="T218">conj</ta>
            <ta e="T220" id="Seg_4262" s="T219">n</ta>
            <ta e="T221" id="Seg_4263" s="T220">adv</ta>
            <ta e="T222" id="Seg_4264" s="T221">v</ta>
            <ta e="T223" id="Seg_4265" s="T222">v</ta>
            <ta e="T224" id="Seg_4266" s="T223">n</ta>
            <ta e="T225" id="Seg_4267" s="T224">n</ta>
            <ta e="T226" id="Seg_4268" s="T225">v</ta>
            <ta e="T227" id="Seg_4269" s="T226">conj</ta>
            <ta e="T228" id="Seg_4270" s="T227">n</ta>
            <ta e="T230" id="Seg_4271" s="T229">adv</ta>
            <ta e="T231" id="Seg_4272" s="T230">v</ta>
            <ta e="T232" id="Seg_4273" s="T231">dempro</ta>
            <ta e="T233" id="Seg_4274" s="T232">n</ta>
            <ta e="T234" id="Seg_4275" s="T233">v</ta>
            <ta e="T235" id="Seg_4276" s="T234">que</ta>
            <ta e="T236" id="Seg_4277" s="T235">v</ta>
            <ta e="T237" id="Seg_4278" s="T236">conj</ta>
            <ta e="T238" id="Seg_4279" s="T237">dempro</ta>
            <ta e="T240" id="Seg_4280" s="T239">dempro</ta>
            <ta e="T241" id="Seg_4281" s="T240">dempro</ta>
            <ta e="T242" id="Seg_4282" s="T241">v</ta>
            <ta e="T243" id="Seg_4283" s="T242">pers</ta>
            <ta e="T244" id="Seg_4284" s="T243">n</ta>
            <ta e="T245" id="Seg_4285" s="T244">v</ta>
            <ta e="T246" id="Seg_4286" s="T245">adv</ta>
            <ta e="T247" id="Seg_4287" s="T246">dempro</ta>
            <ta e="T248" id="Seg_4288" s="T247">v</ta>
            <ta e="T249" id="Seg_4289" s="T248">adj</ta>
            <ta e="T250" id="Seg_4290" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_4291" s="T250">adv</ta>
            <ta e="T252" id="Seg_4292" s="T251">dempro</ta>
            <ta e="T253" id="Seg_4293" s="T252">v</ta>
            <ta e="T254" id="Seg_4294" s="T253">dempro</ta>
            <ta e="T255" id="Seg_4295" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_4296" s="T255">v</ta>
            <ta e="T257" id="Seg_4297" s="T256">conj</ta>
            <ta e="T258" id="Seg_4298" s="T257">n</ta>
            <ta e="T259" id="Seg_4299" s="T258">n</ta>
            <ta e="T260" id="Seg_4300" s="T259">v</ta>
            <ta e="T261" id="Seg_4301" s="T260">n</ta>
            <ta e="T262" id="Seg_4302" s="T261">n</ta>
            <ta e="T263" id="Seg_4303" s="T262">v</ta>
            <ta e="T264" id="Seg_4304" s="T263">n</ta>
            <ta e="T265" id="Seg_4305" s="T264">adv</ta>
            <ta e="T266" id="Seg_4306" s="T265">v</ta>
            <ta e="T267" id="Seg_4307" s="T266">adv</ta>
            <ta e="T268" id="Seg_4308" s="T267">v</ta>
            <ta e="T269" id="Seg_4309" s="T268">v</ta>
            <ta e="T270" id="Seg_4310" s="T269">v</ta>
            <ta e="T271" id="Seg_4311" s="T270">v</ta>
            <ta e="T272" id="Seg_4312" s="T271">n</ta>
            <ta e="T273" id="Seg_4313" s="T272">adv</ta>
            <ta e="T274" id="Seg_4314" s="T273">dempro</ta>
            <ta e="T275" id="Seg_4315" s="T274">v</ta>
            <ta e="T276" id="Seg_4316" s="T275">n</ta>
            <ta e="T277" id="Seg_4317" s="T276">v</ta>
            <ta e="T279" id="Seg_4318" s="T278">adv</ta>
            <ta e="T280" id="Seg_4319" s="T279">num</ta>
            <ta e="T281" id="Seg_4320" s="T280">n</ta>
            <ta e="T284" id="Seg_4321" s="T283">n</ta>
            <ta e="T285" id="Seg_4322" s="T284">v</ta>
            <ta e="T286" id="Seg_4323" s="T285">n</ta>
            <ta e="T287" id="Seg_4324" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_4325" s="T287">v</ta>
            <ta e="T289" id="Seg_4326" s="T288">adj</ta>
            <ta e="T290" id="Seg_4327" s="T289">n</ta>
            <ta e="T292" id="Seg_4328" s="T291">adv</ta>
            <ta e="T293" id="Seg_4329" s="T292">n</ta>
            <ta e="T294" id="Seg_4330" s="T293">v</ta>
            <ta e="T295" id="Seg_4331" s="T294">adv</ta>
            <ta e="T296" id="Seg_4332" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4333" s="T296">v</ta>
            <ta e="T298" id="Seg_4334" s="T297">adv</ta>
            <ta e="T299" id="Seg_4335" s="T298">dempro</ta>
            <ta e="T301" id="Seg_4336" s="T300">n</ta>
            <ta e="T303" id="Seg_4337" s="T302">v</ta>
            <ta e="T304" id="Seg_4338" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_4339" s="T304">n</ta>
            <ta e="T306" id="Seg_4340" s="T305">conj</ta>
            <ta e="T307" id="Seg_4341" s="T306">n</ta>
            <ta e="T308" id="Seg_4342" s="T307">n</ta>
            <ta e="T309" id="Seg_4343" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4344" s="T309">v</ta>
            <ta e="T312" id="Seg_4345" s="T311">n</ta>
            <ta e="T313" id="Seg_4346" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_4347" s="T313">n</ta>
            <ta e="T315" id="Seg_4348" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_4349" s="T315">n</ta>
            <ta e="T318" id="Seg_4350" s="T317">v</ta>
            <ta e="T319" id="Seg_4351" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4352" s="T319">adv</ta>
            <ta e="T321" id="Seg_4353" s="T320">dempro</ta>
            <ta e="T322" id="Seg_4354" s="T321">v</ta>
            <ta e="T323" id="Seg_4355" s="T322">n</ta>
            <ta e="T324" id="Seg_4356" s="T323">n</ta>
            <ta e="T325" id="Seg_4357" s="T324">v</ta>
            <ta e="T326" id="Seg_4358" s="T325">conj</ta>
            <ta e="T327" id="Seg_4359" s="T326">v</ta>
            <ta e="T328" id="Seg_4360" s="T327">n</ta>
            <ta e="T330" id="Seg_4361" s="T329">v</ta>
            <ta e="T331" id="Seg_4362" s="T330">dempro</ta>
            <ta e="T332" id="Seg_4363" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_4364" s="T332">v</ta>
            <ta e="T334" id="Seg_4365" s="T333">adv</ta>
            <ta e="T335" id="Seg_4366" s="T334">dempro</ta>
            <ta e="T336" id="Seg_4367" s="T335">v</ta>
            <ta e="T337" id="Seg_4368" s="T336">n</ta>
            <ta e="T338" id="Seg_4369" s="T337">adv</ta>
            <ta e="T339" id="Seg_4370" s="T338">pers</ta>
            <ta e="T340" id="Seg_4371" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_4372" s="T340">adj</ta>
            <ta e="T342" id="Seg_4373" s="T341">v</ta>
            <ta e="T343" id="Seg_4374" s="T342">adv</ta>
            <ta e="T344" id="Seg_4375" s="T343">pers</ta>
            <ta e="T361" id="Seg_4376" s="T344">v</ta>
            <ta e="T345" id="Seg_4377" s="T361">v</ta>
            <ta e="T346" id="Seg_4378" s="T345">conj</ta>
            <ta e="T347" id="Seg_4379" s="T346">pers</ta>
            <ta e="T348" id="Seg_4380" s="T347">pers</ta>
            <ta e="T349" id="Seg_4381" s="T348">v</ta>
            <ta e="T350" id="Seg_4382" s="T349">dempro</ta>
            <ta e="T362" id="Seg_4383" s="T350">v</ta>
            <ta e="T351" id="Seg_4384" s="T362">v</ta>
            <ta e="T352" id="Seg_4385" s="T351">conj</ta>
            <ta e="T353" id="Seg_4386" s="T352">dempro</ta>
            <ta e="T354" id="Seg_4387" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_4388" s="T354">v</ta>
            <ta e="T356" id="Seg_4389" s="T355">v</ta>
            <ta e="T357" id="Seg_4390" s="T356">adv</ta>
            <ta e="T358" id="Seg_4391" s="T357">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_4392" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_4393" s="T3">pro.h:Poss</ta>
            <ta e="T6" id="Seg_4394" s="T5">np.h:Th</ta>
            <ta e="T8" id="Seg_4395" s="T7">adv:Time</ta>
            <ta e="T9" id="Seg_4396" s="T8">pro.h:A</ta>
            <ta e="T11" id="Seg_4397" s="T10">np.h:R</ta>
            <ta e="T13" id="Seg_4398" s="T12">pro.h:B</ta>
            <ta e="T15" id="Seg_4399" s="T14">np.h:Th</ta>
            <ta e="T17" id="Seg_4400" s="T16">pro.h:E</ta>
            <ta e="T20" id="Seg_4401" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_4402" s="T20">np.h:Th</ta>
            <ta e="T23" id="Seg_4403" s="T22">pro.h:A</ta>
            <ta e="T26" id="Seg_4404" s="T25">0.2.h:A</ta>
            <ta e="T27" id="Seg_4405" s="T26">pro.h:Th</ta>
            <ta e="T28" id="Seg_4406" s="T27">pro.h:B</ta>
            <ta e="T30" id="Seg_4407" s="T29">0.2.h:A</ta>
            <ta e="T31" id="Seg_4408" s="T30">np:Th</ta>
            <ta e="T33" id="Seg_4409" s="T32">0.2.h:A</ta>
            <ta e="T34" id="Seg_4410" s="T33">0.2.h:A</ta>
            <ta e="T35" id="Seg_4411" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_4412" s="T35">adv:Time</ta>
            <ta e="T38" id="Seg_4413" s="T37">pro.h:A</ta>
            <ta e="T40" id="Seg_4414" s="T39">np.h:A</ta>
            <ta e="T42" id="Seg_4415" s="T41">0.3:Th</ta>
            <ta e="T44" id="Seg_4416" s="T43">np.h:Poss</ta>
            <ta e="T45" id="Seg_4417" s="T44">np:G</ta>
            <ta e="T46" id="Seg_4418" s="T45">np.h:A</ta>
            <ta e="T48" id="Seg_4419" s="T47">0.3:Th </ta>
            <ta e="T52" id="Seg_4420" s="T51">0.3:Th</ta>
            <ta e="T54" id="Seg_4421" s="T53">np.h:A</ta>
            <ta e="T57" id="Seg_4422" s="T359">0.3:Th</ta>
            <ta e="T58" id="Seg_4423" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_4424" s="T58">pro.h:A</ta>
            <ta e="T61" id="Seg_4425" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_4426" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_4427" s="T62">np:L</ta>
            <ta e="T65" id="Seg_4428" s="T64">np.h:E</ta>
            <ta e="T71" id="Seg_4429" s="T70">pro.h:A</ta>
            <ta e="T73" id="Seg_4430" s="T72">0.2.h:A</ta>
            <ta e="T74" id="Seg_4431" s="T73">pro.h:B</ta>
            <ta e="T75" id="Seg_4432" s="T74">np:Th</ta>
            <ta e="T78" id="Seg_4433" s="T77">pro.h:A</ta>
            <ta e="T80" id="Seg_4434" s="T79">0.2.h:A</ta>
            <ta e="T81" id="Seg_4435" s="T80">pro.h:Th</ta>
            <ta e="T85" id="Seg_4436" s="T84">pro.h:A</ta>
            <ta e="T86" id="Seg_4437" s="T85">pro.h:Th</ta>
            <ta e="T88" id="Seg_4438" s="T87">pro.h:Th</ta>
            <ta e="T90" id="Seg_4439" s="T89">np.h:Th</ta>
            <ta e="T94" id="Seg_4440" s="T93">pro.h:Poss</ta>
            <ta e="T96" id="Seg_4441" s="T95">0.2.h:A</ta>
            <ta e="T97" id="Seg_4442" s="T96">pro.h:Th</ta>
            <ta e="T98" id="Seg_4443" s="T97">adv:Time</ta>
            <ta e="T99" id="Seg_4444" s="T98">pro.h:A</ta>
            <ta e="T101" id="Seg_4445" s="T100">np:G</ta>
            <ta e="T102" id="Seg_4446" s="T101">0.3.h:A</ta>
            <ta e="T103" id="Seg_4447" s="T102">adv:Time</ta>
            <ta e="T106" id="Seg_4448" s="T105">0.3.h:A</ta>
            <ta e="T108" id="Seg_4449" s="T107">adv:Time</ta>
            <ta e="T109" id="Seg_4450" s="T108">0.3.h:A</ta>
            <ta e="T110" id="Seg_4451" s="T109">0.2.h:A</ta>
            <ta e="T111" id="Seg_4452" s="T110">np:P</ta>
            <ta e="T112" id="Seg_4453" s="T111">adv:Time</ta>
            <ta e="T115" id="Seg_4454" s="T114">np.h:A</ta>
            <ta e="T116" id="Seg_4455" s="T115">np:G</ta>
            <ta e="T118" id="Seg_4456" s="T117">pro:Th</ta>
            <ta e="T120" id="Seg_4457" s="T119">0.3.h:A</ta>
            <ta e="T122" id="Seg_4458" s="T121">pro.h:A</ta>
            <ta e="T123" id="Seg_4459" s="T122">pro:Th</ta>
            <ta e="T127" id="Seg_4460" s="T126">np.h:A</ta>
            <ta e="T129" id="Seg_4461" s="T128">np:P</ta>
            <ta e="T131" id="Seg_4462" s="T130">0.2.h:A</ta>
            <ta e="T133" id="Seg_4463" s="T132">0.2.h:E</ta>
            <ta e="T136" id="Seg_4464" s="T135">np:P</ta>
            <ta e="T138" id="Seg_4465" s="T137">adv:Time</ta>
            <ta e="T139" id="Seg_4466" s="T138">pro.h:E</ta>
            <ta e="T142" id="Seg_4467" s="T141">pro.h:A</ta>
            <ta e="T143" id="Seg_4468" s="T142">adv:G</ta>
            <ta e="T145" id="Seg_4469" s="T144">np:P</ta>
            <ta e="T146" id="Seg_4470" s="T145">0.3.h:A</ta>
            <ta e="T149" id="Seg_4471" s="T148">0.2.h:A</ta>
            <ta e="T150" id="Seg_4472" s="T149">pro.h:B</ta>
            <ta e="T151" id="Seg_4473" s="T150">np:P</ta>
            <ta e="T152" id="Seg_4474" s="T151">np.h:B</ta>
            <ta e="T155" id="Seg_4475" s="T154">adv:Time</ta>
            <ta e="T157" id="Seg_4476" s="T156">np:P</ta>
            <ta e="T158" id="Seg_4477" s="T157">0.3.h:A</ta>
            <ta e="T160" id="Seg_4478" s="T159">np:G</ta>
            <ta e="T161" id="Seg_4479" s="T160">0.3.h:A</ta>
            <ta e="T162" id="Seg_4480" s="T161">np:Th</ta>
            <ta e="T163" id="Seg_4481" s="T162">0.3.h:A</ta>
            <ta e="T164" id="Seg_4482" s="T163">np:G</ta>
            <ta e="T165" id="Seg_4483" s="T164">np:G</ta>
            <ta e="T166" id="Seg_4484" s="T165">adv:Time</ta>
            <ta e="T167" id="Seg_4485" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_4486" s="T167">np.h:P</ta>
            <ta e="T170" id="Seg_4487" s="T169">0.2.h:A</ta>
            <ta e="T171" id="Seg_4488" s="T170">np:Th</ta>
            <ta e="T172" id="Seg_4489" s="T171">0.2.h:A</ta>
            <ta e="T173" id="Seg_4490" s="T172">np:G</ta>
            <ta e="T174" id="Seg_4491" s="T173">pro.h:A</ta>
            <ta e="T177" id="Seg_4492" s="T176">np.h:A</ta>
            <ta e="T180" id="Seg_4493" s="T179">np.h:Poss</ta>
            <ta e="T181" id="Seg_4494" s="T180">np:Th</ta>
            <ta e="T182" id="Seg_4495" s="T181">pro:Th</ta>
            <ta e="T186" id="Seg_4496" s="T185">np:Th</ta>
            <ta e="T189" id="Seg_4497" s="T188">np.h:Poss</ta>
            <ta e="T192" id="Seg_4498" s="T191">np:Th</ta>
            <ta e="T194" id="Seg_4499" s="T193">np:G</ta>
            <ta e="T198" id="Seg_4500" s="T197">np.h:A</ta>
            <ta e="T204" id="Seg_4501" s="T203">np:Th</ta>
            <ta e="T210" id="Seg_4502" s="T209">pro.h:Poss</ta>
            <ta e="T211" id="Seg_4503" s="T210">np.h:A</ta>
            <ta e="T212" id="Seg_4504" s="T211">np.h:Th</ta>
            <ta e="T215" id="Seg_4505" s="T214">np:P</ta>
            <ta e="T216" id="Seg_4506" s="T215">0.3.h:A</ta>
            <ta e="T217" id="Seg_4507" s="T216">0.3.h:A</ta>
            <ta e="T218" id="Seg_4508" s="T217">np.h:R</ta>
            <ta e="T220" id="Seg_4509" s="T219">np.h:R</ta>
            <ta e="T221" id="Seg_4510" s="T220">adv:Time</ta>
            <ta e="T222" id="Seg_4511" s="T221">0.3.h:A</ta>
            <ta e="T223" id="Seg_4512" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_4513" s="T223">np:L</ta>
            <ta e="T225" id="Seg_4514" s="T224">np:P</ta>
            <ta e="T226" id="Seg_4515" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_4516" s="T227">np:P</ta>
            <ta e="T230" id="Seg_4517" s="T229">adv:Time</ta>
            <ta e="T231" id="Seg_4518" s="T230">0.3.h:A</ta>
            <ta e="T232" id="Seg_4519" s="T231">pro.h:Poss</ta>
            <ta e="T233" id="Seg_4520" s="T232">np.h:A</ta>
            <ta e="T235" id="Seg_4521" s="T234">pro.h:A</ta>
            <ta e="T241" id="Seg_4522" s="T240">pro.h:A</ta>
            <ta e="T243" id="Seg_4523" s="T242">pro.h:Poss</ta>
            <ta e="T244" id="Seg_4524" s="T243">np.h:A</ta>
            <ta e="T246" id="Seg_4525" s="T245">adv:Time</ta>
            <ta e="T247" id="Seg_4526" s="T246">pro.h:A</ta>
            <ta e="T251" id="Seg_4527" s="T250">adv:Time</ta>
            <ta e="T252" id="Seg_4528" s="T251">pro.h:A</ta>
            <ta e="T254" id="Seg_4529" s="T253">pro.h:A</ta>
            <ta e="T258" id="Seg_4530" s="T257">np:Th</ta>
            <ta e="T259" id="Seg_4531" s="T258">np:G</ta>
            <ta e="T260" id="Seg_4532" s="T259">0.3.h:A</ta>
            <ta e="T261" id="Seg_4533" s="T260">np:Th</ta>
            <ta e="T262" id="Seg_4534" s="T261">np:G</ta>
            <ta e="T263" id="Seg_4535" s="T262">0.3.h:A</ta>
            <ta e="T264" id="Seg_4536" s="T263">np:Th</ta>
            <ta e="T265" id="Seg_4537" s="T264">adv:L</ta>
            <ta e="T266" id="Seg_4538" s="T265">0.3.h:A</ta>
            <ta e="T267" id="Seg_4539" s="T266">adv:Time</ta>
            <ta e="T268" id="Seg_4540" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_4541" s="T268">0.3.h:A</ta>
            <ta e="T272" id="Seg_4542" s="T271">np:Ins</ta>
            <ta e="T273" id="Seg_4543" s="T272">adv:Time</ta>
            <ta e="T274" id="Seg_4544" s="T273">pro.h:A</ta>
            <ta e="T276" id="Seg_4545" s="T275">np:P</ta>
            <ta e="T279" id="Seg_4546" s="T278">adv:Time</ta>
            <ta e="T284" id="Seg_4547" s="T283">np:P</ta>
            <ta e="T286" id="Seg_4548" s="T285">np.h:E</ta>
            <ta e="T292" id="Seg_4549" s="T291">adv:Time</ta>
            <ta e="T293" id="Seg_4550" s="T292">np:A</ta>
            <ta e="T295" id="Seg_4551" s="T294">adv:Time</ta>
            <ta e="T297" id="Seg_4552" s="T296">0.3:P</ta>
            <ta e="T298" id="Seg_4553" s="T297">adv:Time</ta>
            <ta e="T299" id="Seg_4554" s="T298">pro.h:A</ta>
            <ta e="T301" id="Seg_4555" s="T300">np.h:A</ta>
            <ta e="T302" id="Seg_4556" s="T301">np:Ins</ta>
            <ta e="T305" id="Seg_4557" s="T304">np.h:R</ta>
            <ta e="T307" id="Seg_4558" s="T306">np:Ins</ta>
            <ta e="T308" id="Seg_4559" s="T307">np:G</ta>
            <ta e="T310" id="Seg_4560" s="T309">0.3.h:A</ta>
            <ta e="T312" id="Seg_4561" s="T311">np:Ins</ta>
            <ta e="T314" id="Seg_4562" s="T313">np:G</ta>
            <ta e="T316" id="Seg_4563" s="T315">np:Th</ta>
            <ta e="T318" id="Seg_4564" s="T317">0.3.h:A</ta>
            <ta e="T320" id="Seg_4565" s="T319">adv:Time</ta>
            <ta e="T321" id="Seg_4566" s="T320">pro.h:A</ta>
            <ta e="T323" id="Seg_4567" s="T322">np:G</ta>
            <ta e="T324" id="Seg_4568" s="T323">np:Th</ta>
            <ta e="T325" id="Seg_4569" s="T324">0.3.h:A</ta>
            <ta e="T327" id="Seg_4570" s="T326">0.3.h:A</ta>
            <ta e="T328" id="Seg_4571" s="T327">np:G</ta>
            <ta e="T330" id="Seg_4572" s="T329">0.3.h:A</ta>
            <ta e="T331" id="Seg_4573" s="T330">pro:P</ta>
            <ta e="T334" id="Seg_4574" s="T333">adv:Time</ta>
            <ta e="T335" id="Seg_4575" s="T334">pro.h:A</ta>
            <ta e="T337" id="Seg_4576" s="T336">np:G</ta>
            <ta e="T339" id="Seg_4577" s="T338">pro.h:A</ta>
            <ta e="T341" id="Seg_4578" s="T340">np:Th</ta>
            <ta e="T343" id="Seg_4579" s="T342">adv:Time</ta>
            <ta e="T344" id="Seg_4580" s="T343">pro.h:A</ta>
            <ta e="T347" id="Seg_4581" s="T346">pro.h:A</ta>
            <ta e="T348" id="Seg_4582" s="T347">pro.h:Th</ta>
            <ta e="T350" id="Seg_4583" s="T349">pro.h:A</ta>
            <ta e="T353" id="Seg_4584" s="T352">pro.h:E</ta>
            <ta e="T356" id="Seg_4585" s="T355">0.3.h:E</ta>
            <ta e="T357" id="Seg_4586" s="T356">adv:Time</ta>
            <ta e="T358" id="Seg_4587" s="T357">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_4588" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_4589" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_4590" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_4591" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_4592" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_4593" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_4594" s="T11">ptcl:pred</ta>
            <ta e="T15" id="Seg_4595" s="T14">np.h:O</ta>
            <ta e="T17" id="Seg_4596" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_4597" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_4598" s="T20">np.h:O</ta>
            <ta e="T23" id="Seg_4599" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_4600" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_4601" s="T25">v:pred 0.2.h:S</ta>
            <ta e="T27" id="Seg_4602" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_4603" s="T28">adj:pred</ta>
            <ta e="T30" id="Seg_4604" s="T29">v:pred 0.2.h:S</ta>
            <ta e="T31" id="Seg_4605" s="T30">np:O</ta>
            <ta e="T33" id="Seg_4606" s="T32">v:pred 0.2.h:S</ta>
            <ta e="T34" id="Seg_4607" s="T33">v:pred 0.2.h:S</ta>
            <ta e="T35" id="Seg_4608" s="T34">np:O</ta>
            <ta e="T38" id="Seg_4609" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_4610" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_4611" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_4612" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_4613" s="T41">v:pred 0.3:S</ta>
            <ta e="T46" id="Seg_4614" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_4615" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_4616" s="T47">v:pred 0.3:S </ta>
            <ta e="T54" id="Seg_4617" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_4618" s="T54">v:pred</ta>
            <ta e="T359" id="Seg_4619" s="T56">conv:pred</ta>
            <ta e="T57" id="Seg_4620" s="T359">v:pred 0.3:S</ta>
            <ta e="T59" id="Seg_4621" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_4622" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_4623" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T62" id="Seg_4624" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_4625" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_4626" s="T64">np.h:S</ta>
            <ta e="T71" id="Seg_4627" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_4628" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_4629" s="T72">v:pred 0.2.h:S</ta>
            <ta e="T75" id="Seg_4630" s="T74">np:O</ta>
            <ta e="T78" id="Seg_4631" s="T77">pro.h:S</ta>
            <ta e="T79" id="Seg_4632" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_4633" s="T79">v:pred 0.2.h:S</ta>
            <ta e="T81" id="Seg_4634" s="T80">pro.h:O</ta>
            <ta e="T85" id="Seg_4635" s="T84">pro.h:S</ta>
            <ta e="T86" id="Seg_4636" s="T85">pro.h:O</ta>
            <ta e="T87" id="Seg_4637" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_4638" s="T87">pro.h:S</ta>
            <ta e="T89" id="Seg_4639" s="T88">ptcl:pred</ta>
            <ta e="T90" id="Seg_4640" s="T89">n:pred</ta>
            <ta e="T96" id="Seg_4641" s="T95">v:pred 0.2.h:S</ta>
            <ta e="T97" id="Seg_4642" s="T96">pro.h:O</ta>
            <ta e="T99" id="Seg_4643" s="T98">pro.h:S</ta>
            <ta e="T100" id="Seg_4644" s="T99">v:pred</ta>
            <ta e="T102" id="Seg_4645" s="T101">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_4646" s="T105">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_4647" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_4648" s="T109">v:pred 0.2.h:S</ta>
            <ta e="T111" id="Seg_4649" s="T110">np:O</ta>
            <ta e="T114" id="Seg_4650" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_4651" s="T114">np.h:S</ta>
            <ta e="T118" id="Seg_4652" s="T117">pro:O</ta>
            <ta e="T119" id="Seg_4653" s="T118">ptcl.neg</ta>
            <ta e="T120" id="Seg_4654" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T122" id="Seg_4655" s="T121">pro.h:S</ta>
            <ta e="T123" id="Seg_4656" s="T122">pro:O</ta>
            <ta e="T124" id="Seg_4657" s="T123">ptcl.neg</ta>
            <ta e="T125" id="Seg_4658" s="T124">v:pred</ta>
            <ta e="T127" id="Seg_4659" s="T126">np.h:S</ta>
            <ta e="T128" id="Seg_4660" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_4661" s="T128">np:O</ta>
            <ta e="T130" id="Seg_4662" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_4663" s="T130">v:pred 0.2.h:S</ta>
            <ta e="T133" id="Seg_4664" s="T132">v:pred 0.2.h:S</ta>
            <ta e="T136" id="Seg_4665" s="T135">np:S</ta>
            <ta e="T137" id="Seg_4666" s="T136">v:pred</ta>
            <ta e="T139" id="Seg_4667" s="T138">pro.h:S</ta>
            <ta e="T140" id="Seg_4668" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_4669" s="T141">pro.h:S</ta>
            <ta e="T360" id="Seg_4670" s="T143">conv:pred</ta>
            <ta e="T144" id="Seg_4671" s="T360">v:pred</ta>
            <ta e="T145" id="Seg_4672" s="T144">np:O</ta>
            <ta e="T146" id="Seg_4673" s="T145">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_4674" s="T146">ptcl:pred</ta>
            <ta e="T149" id="Seg_4675" s="T148">v:pred 0.2.h:S</ta>
            <ta e="T157" id="Seg_4676" s="T156">np:O</ta>
            <ta e="T158" id="Seg_4677" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_4678" s="T160">v:pred 0.3.h:S</ta>
            <ta e="T162" id="Seg_4679" s="T161">np:O</ta>
            <ta e="T163" id="Seg_4680" s="T162">v:pred 0.3.h:S</ta>
            <ta e="T167" id="Seg_4681" s="T166">v:pred 0.3.h:S</ta>
            <ta e="T168" id="Seg_4682" s="T167">np.h:O</ta>
            <ta e="T170" id="Seg_4683" s="T169">v:pred 0.2.h:S</ta>
            <ta e="T171" id="Seg_4684" s="T170">np:O</ta>
            <ta e="T172" id="Seg_4685" s="T171">v:pred 0.2.h:S</ta>
            <ta e="T174" id="Seg_4686" s="T173">pro.h:S</ta>
            <ta e="T176" id="Seg_4687" s="T175">v:pred</ta>
            <ta e="T177" id="Seg_4688" s="T176">np.h:S</ta>
            <ta e="T178" id="Seg_4689" s="T177">v:pred</ta>
            <ta e="T182" id="Seg_4690" s="T181">pro:S</ta>
            <ta e="T184" id="Seg_4691" s="T183">v:pred</ta>
            <ta e="T186" id="Seg_4692" s="T185">np:S</ta>
            <ta e="T192" id="Seg_4693" s="T191">np:S</ta>
            <ta e="T195" id="Seg_4694" s="T194">v:pred</ta>
            <ta e="T198" id="Seg_4695" s="T197">np.h:S</ta>
            <ta e="T199" id="Seg_4696" s="T198">v:pred</ta>
            <ta e="T204" id="Seg_4697" s="T203">np:S</ta>
            <ta e="T207" id="Seg_4698" s="T206">v:pred</ta>
            <ta e="T211" id="Seg_4699" s="T210">np.h:S</ta>
            <ta e="T212" id="Seg_4700" s="T211">np.h:O</ta>
            <ta e="T214" id="Seg_4701" s="T213">v:pred</ta>
            <ta e="T215" id="Seg_4702" s="T214">np:O</ta>
            <ta e="T216" id="Seg_4703" s="T215">v:pred 0.3.h:S</ta>
            <ta e="T217" id="Seg_4704" s="T216">v:pred 0.3.h:S</ta>
            <ta e="T218" id="Seg_4705" s="T217">np.h:O</ta>
            <ta e="T220" id="Seg_4706" s="T219">np.h:O</ta>
            <ta e="T222" id="Seg_4707" s="T221">v:pred 0.3.h:S</ta>
            <ta e="T223" id="Seg_4708" s="T222">v:pred 0.3.h:S</ta>
            <ta e="T225" id="Seg_4709" s="T224">np:O</ta>
            <ta e="T226" id="Seg_4710" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T228" id="Seg_4711" s="T227">np:O</ta>
            <ta e="T231" id="Seg_4712" s="T230">v:pred 0.3.h:S</ta>
            <ta e="T233" id="Seg_4713" s="T232">np.h:S</ta>
            <ta e="T234" id="Seg_4714" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_4715" s="T234">pro.h:S</ta>
            <ta e="T236" id="Seg_4716" s="T235">v:pred</ta>
            <ta e="T241" id="Seg_4717" s="T240">pro.h:S</ta>
            <ta e="T242" id="Seg_4718" s="T241">v:pred</ta>
            <ta e="T244" id="Seg_4719" s="T243">np.h:S</ta>
            <ta e="T245" id="Seg_4720" s="T244">v:pred</ta>
            <ta e="T247" id="Seg_4721" s="T246">pro.h:S</ta>
            <ta e="T248" id="Seg_4722" s="T247">v:pred</ta>
            <ta e="T249" id="Seg_4723" s="T248">adj:pred</ta>
            <ta e="T252" id="Seg_4724" s="T251">pro.h:S</ta>
            <ta e="T253" id="Seg_4725" s="T252">v:pred</ta>
            <ta e="T254" id="Seg_4726" s="T253">pro.h:S</ta>
            <ta e="T256" id="Seg_4727" s="T255">v:pred</ta>
            <ta e="T258" id="Seg_4728" s="T257">np:O</ta>
            <ta e="T260" id="Seg_4729" s="T259">v:pred 0.3.h:S</ta>
            <ta e="T261" id="Seg_4730" s="T260">np:O</ta>
            <ta e="T263" id="Seg_4731" s="T262">v:pred 0.3.h:S</ta>
            <ta e="T264" id="Seg_4732" s="T263">np:O</ta>
            <ta e="T266" id="Seg_4733" s="T265">v:pred 0.3.h:S</ta>
            <ta e="T268" id="Seg_4734" s="T267">v:pred 0.3.h:S</ta>
            <ta e="T269" id="Seg_4735" s="T268">v:pred 0.3.h:S</ta>
            <ta e="T270" id="Seg_4736" s="T269">s:purp</ta>
            <ta e="T271" id="Seg_4737" s="T270">s:purp</ta>
            <ta e="T274" id="Seg_4738" s="T273">pro.h:S</ta>
            <ta e="T275" id="Seg_4739" s="T274">v:pred</ta>
            <ta e="T276" id="Seg_4740" s="T275">np:S</ta>
            <ta e="T277" id="Seg_4741" s="T276">v:pred</ta>
            <ta e="T284" id="Seg_4742" s="T283">np:S</ta>
            <ta e="T285" id="Seg_4743" s="T284">v:pred</ta>
            <ta e="T286" id="Seg_4744" s="T285">np.h:S</ta>
            <ta e="T288" id="Seg_4745" s="T287">v:pred</ta>
            <ta e="T293" id="Seg_4746" s="T292">np:S</ta>
            <ta e="T294" id="Seg_4747" s="T293">v:pred</ta>
            <ta e="T297" id="Seg_4748" s="T296">v:pred 0.3:S</ta>
            <ta e="T299" id="Seg_4749" s="T298">pro.h:S</ta>
            <ta e="T301" id="Seg_4750" s="T300">np.h:S</ta>
            <ta e="T303" id="Seg_4751" s="T302">v:pred</ta>
            <ta e="T310" id="Seg_4752" s="T309">v:pred 0.3.h:S</ta>
            <ta e="T316" id="Seg_4753" s="T315">np:O</ta>
            <ta e="T318" id="Seg_4754" s="T317">v:pred 0.3.h:S</ta>
            <ta e="T321" id="Seg_4755" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_4756" s="T321">v:pred</ta>
            <ta e="T324" id="Seg_4757" s="T323">np:O</ta>
            <ta e="T325" id="Seg_4758" s="T324">v:pred 0.3.h:S</ta>
            <ta e="T327" id="Seg_4759" s="T326">v:pred 0.3.h:S</ta>
            <ta e="T330" id="Seg_4760" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T331" id="Seg_4761" s="T330">pro:S</ta>
            <ta e="T333" id="Seg_4762" s="T332">v:pred</ta>
            <ta e="T335" id="Seg_4763" s="T334">pro.h:S</ta>
            <ta e="T336" id="Seg_4764" s="T335">v:pred</ta>
            <ta e="T339" id="Seg_4765" s="T338">pro.h:S</ta>
            <ta e="T340" id="Seg_4766" s="T339">ptcl.neg</ta>
            <ta e="T341" id="Seg_4767" s="T340">np:O</ta>
            <ta e="T342" id="Seg_4768" s="T341">v:pred</ta>
            <ta e="T344" id="Seg_4769" s="T343">pro.h:S</ta>
            <ta e="T361" id="Seg_4770" s="T344">conv:pred</ta>
            <ta e="T345" id="Seg_4771" s="T361">v:pred</ta>
            <ta e="T347" id="Seg_4772" s="T346">pro.h:S</ta>
            <ta e="T348" id="Seg_4773" s="T347">pro.h:O</ta>
            <ta e="T349" id="Seg_4774" s="T348">v:pred</ta>
            <ta e="T350" id="Seg_4775" s="T349">pro.h:S</ta>
            <ta e="T362" id="Seg_4776" s="T350">conv:pred</ta>
            <ta e="T351" id="Seg_4777" s="T362">v:pred</ta>
            <ta e="T353" id="Seg_4778" s="T352">pro.h:S</ta>
            <ta e="T355" id="Seg_4779" s="T354">v:pred</ta>
            <ta e="T356" id="Seg_4780" s="T355">v:pred 0.3.h:S</ta>
            <ta e="T358" id="Seg_4781" s="T357">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_4782" s="T1">TURK:cult</ta>
            <ta e="T12" id="Seg_4783" s="T11">RUS:mod</ta>
            <ta e="T16" id="Seg_4784" s="T15">RUS:gram</ta>
            <ta e="T22" id="Seg_4785" s="T21">RUS:disc</ta>
            <ta e="T25" id="Seg_4786" s="T24">RUS:disc</ta>
            <ta e="T31" id="Seg_4787" s="T30">TURK:cult</ta>
            <ta e="T32" id="Seg_4788" s="T31">RUS:gram</ta>
            <ta e="T35" id="Seg_4789" s="T34">RUS:core</ta>
            <ta e="T44" id="Seg_4790" s="T43">RUS:cult</ta>
            <ta e="T45" id="Seg_4791" s="T44">TAT:cult</ta>
            <ta e="T51" id="Seg_4792" s="T364">RUS:cult</ta>
            <ta e="T52" id="Seg_4793" s="T51">RUS:cult</ta>
            <ta e="T53" id="Seg_4794" s="T52">RUS:gram</ta>
            <ta e="T63" id="Seg_4795" s="T62">RUS:core</ta>
            <ta e="T65" id="Seg_4796" s="T64">RUS:core</ta>
            <ta e="T66" id="Seg_4797" s="T65">RUS:gram</ta>
            <ta e="T67" id="Seg_4798" s="T66">RUS:core</ta>
            <ta e="T75" id="Seg_4799" s="T74">RUS:core</ta>
            <ta e="T76" id="Seg_4800" s="T75">RUS:gram</ta>
            <ta e="T83" id="Seg_4801" s="T82">RUS:disc</ta>
            <ta e="T89" id="Seg_4802" s="T88">RUS:mod</ta>
            <ta e="T90" id="Seg_4803" s="T89">RUS:core</ta>
            <ta e="T91" id="Seg_4804" s="T90">RUS:disc</ta>
            <ta e="T92" id="Seg_4805" s="T91">RUS:gram</ta>
            <ta e="T95" id="Seg_4806" s="T94">RUS:cult</ta>
            <ta e="T117" id="Seg_4807" s="T116">RUS:gram</ta>
            <ta e="T118" id="Seg_4808" s="T117">TURK:gram(INDEF)</ta>
            <ta e="T120" id="Seg_4809" s="T119">%TURK:core</ta>
            <ta e="T123" id="Seg_4810" s="T122">TURK:gram(INDEF)</ta>
            <ta e="T125" id="Seg_4811" s="T124">%TURK:core</ta>
            <ta e="T126" id="Seg_4812" s="T125">RUS:gram</ta>
            <ta e="T134" id="Seg_4813" s="T133">RUS:gram</ta>
            <ta e="T141" id="Seg_4814" s="T140">RUS:gram</ta>
            <ta e="T147" id="Seg_4815" s="T146">RUS:gram</ta>
            <ta e="T154" id="Seg_4816" s="T153">TURK:core</ta>
            <ta e="T156" id="Seg_4817" s="T155">TURK:core</ta>
            <ta e="T160" id="Seg_4818" s="T159">TAT:cult</ta>
            <ta e="T164" id="Seg_4819" s="T163">RUS:cult</ta>
            <ta e="T165" id="Seg_4820" s="T164">RUS:cult</ta>
            <ta e="T169" id="Seg_4821" s="T168">RUS:disc</ta>
            <ta e="T179" id="Seg_4822" s="T178">RUS:core</ta>
            <ta e="T183" id="Seg_4823" s="T182">RUS:mod</ta>
            <ta e="T187" id="Seg_4824" s="T186">RUS:gram</ta>
            <ta e="T188" id="Seg_4825" s="T187">TURK:core</ta>
            <ta e="T190" id="Seg_4826" s="T189">RUS:gram</ta>
            <ta e="T193" id="Seg_4827" s="T192">RUS:mod</ta>
            <ta e="T196" id="Seg_4828" s="T195">RUS:gram</ta>
            <ta e="T200" id="Seg_4829" s="T199">RUS:gram</ta>
            <ta e="T205" id="Seg_4830" s="T204">RUS:mod</ta>
            <ta e="T215" id="Seg_4831" s="T214">RUS:cult</ta>
            <ta e="T219" id="Seg_4832" s="T218">RUS:gram</ta>
            <ta e="T224" id="Seg_4833" s="T223">RUS:cult</ta>
            <ta e="T227" id="Seg_4834" s="T226">RUS:gram</ta>
            <ta e="T235" id="Seg_4835" s="T234">TURK:gram(INDEF)</ta>
            <ta e="T237" id="Seg_4836" s="T236">RUS:gram</ta>
            <ta e="T244" id="Seg_4837" s="T243">RUS:core</ta>
            <ta e="T250" id="Seg_4838" s="T249">TURK:disc</ta>
            <ta e="T255" id="Seg_4839" s="T254">TURK:disc</ta>
            <ta e="T257" id="Seg_4840" s="T256">RUS:gram</ta>
            <ta e="T259" id="Seg_4841" s="T258">RUS:cult</ta>
            <ta e="T261" id="Seg_4842" s="T260">TURK:cult</ta>
            <ta e="T262" id="Seg_4843" s="T261">RUS:cult</ta>
            <ta e="T264" id="Seg_4844" s="T263">RUS:cult</ta>
            <ta e="T272" id="Seg_4845" s="T271">RUS:cult</ta>
            <ta e="T286" id="Seg_4846" s="T285">RUS:cult</ta>
            <ta e="T287" id="Seg_4847" s="T286">TURK:disc</ta>
            <ta e="T289" id="Seg_4848" s="T288">TURK:core</ta>
            <ta e="T296" id="Seg_4849" s="T295">TURK:disc</ta>
            <ta e="T304" id="Seg_4850" s="T303">TURK:disc</ta>
            <ta e="T306" id="Seg_4851" s="T305">RUS:gram</ta>
            <ta e="T309" id="Seg_4852" s="T308">TURK:disc</ta>
            <ta e="T313" id="Seg_4853" s="T312">TURK:disc</ta>
            <ta e="T315" id="Seg_4854" s="T314">TURK:disc</ta>
            <ta e="T326" id="Seg_4855" s="T325">RUS:gram</ta>
            <ta e="T332" id="Seg_4856" s="T331">TURK:disc</ta>
            <ta e="T341" id="Seg_4857" s="T340">TURK:core</ta>
            <ta e="T346" id="Seg_4858" s="T345">RUS:gram</ta>
            <ta e="T352" id="Seg_4859" s="T351">RUS:gram</ta>
            <ta e="T354" id="Seg_4860" s="T353">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T50" id="Seg_4861" s="T363">RUS:int</ta>
            <ta e="T283" id="Seg_4862" s="T281">RUS:int</ta>
            <ta e="T291" id="Seg_4863" s="T290">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_4864" s="T0">Жил царь.</ta>
            <ta e="T7" id="Seg_4865" s="T3">У него было три сына.</ta>
            <ta e="T11" id="Seg_4866" s="T7">Он говорит сыновьям:</ta>
            <ta e="T15" id="Seg_4867" s="T11">"Надо искать вам жен.</ta>
            <ta e="T21" id="Seg_4868" s="T15">Чтобы мне увидеть ваших детей".</ta>
            <ta e="T24" id="Seg_4869" s="T21">Ну, они говорят:</ta>
            <ta e="T29" id="Seg_4870" s="T24">"Ну, ищи, кто тебе нужен?"</ta>
            <ta e="T33" id="Seg_4871" s="T29">"Возьмите ружья [=луки] и идите!</ta>
            <ta e="T35" id="Seg_4872" s="T33">Выпустите стрелы".</ta>
            <ta e="T39" id="Seg_4873" s="T35">Они пошли.</ta>
            <ta e="T41" id="Seg_4874" s="T39">Один выстрелил.</ta>
            <ta e="T45" id="Seg_4875" s="T41">Она упала около дома купца.</ta>
            <ta e="T47" id="Seg_4876" s="T45">Другой выстрелил.</ta>
            <ta e="T49" id="Seg_4877" s="T47">Она упала… </ta>
            <ta e="T50" id="Seg_4878" s="T49">[KA:] Боярский [дворец]. </ta>
            <ta e="T52" id="Seg_4879" s="T50">[PKZ:] …у боярского дворца.</ta>
            <ta e="T55" id="Seg_4880" s="T52">А один [=третий] выстрелил,</ta>
            <ta e="T57" id="Seg_4881" s="T55">[стрела] далеко улетела.</ta>
            <ta e="T62" id="Seg_4882" s="T57">Он шел, шел, приходит.</ta>
            <ta e="T65" id="Seg_4883" s="T62">На болоте сидит лягушка.</ta>
            <ta e="T70" id="Seg_4884" s="T65">И стрелу [держит].</ta>
            <ta e="T75" id="Seg_4885" s="T70">Он говорит: "Дай мне стрелу!"</ta>
            <ta e="T79" id="Seg_4886" s="T75">"А ты, — говорит она, — </ta>
            <ta e="T82" id="Seg_4887" s="T79">возьми меня замуж!"</ta>
            <ta e="T87" id="Seg_4888" s="T82">"Да как я тебя возьму?</ta>
            <ta e="T90" id="Seg_4889" s="T87">Ты ведь лягушка".</ta>
            <ta e="T97" id="Seg_4890" s="T90">"Ну так это твоя судьба, возьми меня".</ta>
            <ta e="T100" id="Seg_4891" s="T97">Он взял [ее].</ta>
            <ta e="T102" id="Seg_4892" s="T100">Принес домой.</ta>
            <ta e="T107" id="Seg_4893" s="T102">Потом [они] устроили свадьбы.</ta>
            <ta e="T111" id="Seg_4894" s="T107">Потом [царь] говорит: "Сшейте рубашки!"</ta>
            <ta e="T116" id="Seg_4895" s="T111">Потом Ванюшка пришел домой.</ta>
            <ta e="T120" id="Seg_4896" s="T116">И ничего не говорит.</ta>
            <ta e="T125" id="Seg_4897" s="T120">"Почему ты ничего не говоришь?"</ta>
            <ta e="T130" id="Seg_4898" s="T125">"Да мой отец сказал, что надо сшить рубашку".</ta>
            <ta e="T137" id="Seg_4899" s="T130">"Ложись, спи, а рубашка будет".</ta>
            <ta e="T140" id="Seg_4900" s="T137">Тогда он заснул.</ta>
            <ta e="T144" id="Seg_4901" s="T140">А она вышла наружу.</ta>
            <ta e="T146" id="Seg_4902" s="T144">Сняла шкуру.</ta>
            <ta e="T150" id="Seg_4903" s="T146">И стала кричать: "Сшейте мне!</ta>
            <ta e="T154" id="Seg_4904" s="T150">Рубашку для отца, красивую, хорошую".</ta>
            <ta e="T158" id="Seg_4905" s="T154">Потом она снова надела шкуру.</ta>
            <ta e="T161" id="Seg_4906" s="T158">Вошла в дом.</ta>
            <ta e="T164" id="Seg_4907" s="T161">Рубашку положила на стол.</ta>
            <ta e="T168" id="Seg_4908" s="T164">На стол, потом разбудила мужа.</ta>
            <ta e="T171" id="Seg_4909" s="T168">"Ну, бери рубашку!</ta>
            <ta e="T173" id="Seg_4910" s="T171">Отнеси ее отцу!"</ta>
            <ta e="T178" id="Seg_4911" s="T173">Он пошел, отец взял [рубашки].</ta>
            <ta e="T186" id="Seg_4912" s="T178">"Рубашка старшего сына, эта их рубашка [годится] только для работы.</ta>
            <ta e="T195" id="Seg_4913" s="T186">А другого сына рубашка - только в баню ходить".</ta>
            <ta e="T199" id="Seg_4914" s="T195">А этот сын дал [ему рубашку].</ta>
            <ta e="T207" id="Seg_4915" s="T199">"А в этой рубашке только на (троне?) сидеть".</ta>
            <ta e="T208" id="Seg_4916" s="T207">Хватит!</ta>
            <ta e="T214" id="Seg_4917" s="T208">Его отец собрал много народу.</ta>
            <ta e="T216" id="Seg_4918" s="T214">Поставил стол.</ta>
            <ta e="T220" id="Seg_4919" s="T216">Позвал сыновей и [их] жен.</ta>
            <ta e="T224" id="Seg_4920" s="T220">Они пришли, сели за стол.</ta>
            <ta e="T226" id="Seg_4921" s="T224">Ели мясо.</ta>
            <ta e="T229" id="Seg_4922" s="T226">И кости (?).</ta>
            <ta e="T236" id="Seg_4923" s="T229">Потом пришли его братья, говорят: "Кто-то идет".</ta>
            <ta e="T245" id="Seg_4924" s="T236">А он говорит: "Моя лягушка идет".</ta>
            <ta e="T250" id="Seg_4925" s="T245">Она пришла, очень красивая.</ta>
            <ta e="T253" id="Seg_4926" s="T250">Они сели.</ta>
            <ta e="T256" id="Seg_4927" s="T253">Она ест.</ta>
            <ta e="T260" id="Seg_4928" s="T256">И кости в рукав кладёт.</ta>
            <ta e="T263" id="Seg_4929" s="T260">Льет водку в рукав.</ta>
            <ta e="T266" id="Seg_4930" s="T263">Льет туда чай.</ta>
            <ta e="T268" id="Seg_4931" s="T266">Они поели.</ta>
            <ta e="T270" id="Seg_4932" s="T268">Пошли танцевать.</ta>
            <ta e="T272" id="Seg_4933" s="T270">Играть на гармони.</ta>
            <ta e="T275" id="Seg_4934" s="T272">Она стала танцевать.</ta>
            <ta e="T277" id="Seg_4935" s="T275">Вода появилась.</ta>
            <ta e="T283" id="Seg_4936" s="T277">Она рукой махнула.</ta>
            <ta e="T285" id="Seg_4937" s="T283">Вода появилась.</ta>
            <ta e="T288" id="Seg_4938" s="T285">Царь испугался.</ta>
            <ta e="T291" id="Seg_4939" s="T288">Другой рукой махнула.</ta>
            <ta e="T294" id="Seg_4940" s="T291">Утки появились.</ta>
            <ta e="T297" id="Seg_4941" s="T294">Потом все исчезло.</ta>
            <ta e="T303" id="Seg_4942" s="T297">Потом те женщины взмахнули руками.</ta>
            <ta e="T305" id="Seg_4943" s="T303">Все люди…</ta>
            <ta e="T311" id="Seg_4944" s="T305">И всем людям в лицо кости полетели.</ta>
            <ta e="T318" id="Seg_4945" s="T311">И водой им в глаза брызнули.</ta>
            <ta e="T319" id="Seg_4946" s="T318">Хватит!</ta>
            <ta e="T323" id="Seg_4947" s="T319">Потом он побежал домой.</ta>
            <ta e="T330" id="Seg_4948" s="T323">Шкуру нашел, взял и положил и положил в огонь.</ta>
            <ta e="T333" id="Seg_4949" s="T330">Она загорелась.</ta>
            <ta e="T337" id="Seg_4950" s="T333">Потом она пришла домой.</ta>
            <ta e="T342" id="Seg_4951" s="T337">"Ты очень плохо сделал!</ta>
            <ta e="T345" id="Seg_4952" s="T342">Теперь я уйду!</ta>
            <ta e="T349" id="Seg_4953" s="T345">А ты будешь меня искать!"</ta>
            <ta e="T351" id="Seg_4954" s="T349">Она ушла.</ta>
            <ta e="T358" id="Seg_4955" s="T351">А он плакал-плакал, потом ушел.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_4956" s="T0">One tsar lived.</ta>
            <ta e="T7" id="Seg_4957" s="T3">He had three sons.</ta>
            <ta e="T11" id="Seg_4958" s="T7">Then he tells his sons:</ta>
            <ta e="T15" id="Seg_4959" s="T11">"It is time to search wives for you.</ta>
            <ta e="T21" id="Seg_4960" s="T15">So that I'll see your children."</ta>
            <ta e="T24" id="Seg_4961" s="T21">Well, they say:</ta>
            <ta e="T29" id="Seg_4962" s="T24">"Well, search, whom do you need?"</ta>
            <ta e="T33" id="Seg_4963" s="T29">"Take guns [= bows] and go!</ta>
            <ta e="T35" id="Seg_4964" s="T33">Shoot arrows."</ta>
            <ta e="T39" id="Seg_4965" s="T35">Then they went.</ta>
            <ta e="T41" id="Seg_4966" s="T39">One [of them] shot.</ta>
            <ta e="T45" id="Seg_4967" s="T41">It fell down near a merchant's house.</ta>
            <ta e="T47" id="Seg_4968" s="T45">One [=another] shot.</ta>
            <ta e="T49" id="Seg_4969" s="T47">It fell down… </ta>
            <ta e="T50" id="Seg_4970" s="T49">[KA:] A boyar's [palace]. </ta>
            <ta e="T52" id="Seg_4971" s="T50">[PKZ:] …near a boyar's palace.</ta>
            <ta e="T55" id="Seg_4972" s="T52">And one [=the third one] shot.</ta>
            <ta e="T57" id="Seg_4973" s="T55">It [the arrow] went far.</ta>
            <ta e="T62" id="Seg_4974" s="T57">Then he went, he went, he comes.</ta>
            <ta e="T65" id="Seg_4975" s="T62">A frog is sitting in the bog.</ta>
            <ta e="T70" id="Seg_4976" s="T65">And [it holds] the arrow.</ta>
            <ta e="T75" id="Seg_4977" s="T70">He says: "Give me the arrow!"</ta>
            <ta e="T79" id="Seg_4978" s="T75">"And you," it says,</ta>
            <ta e="T82" id="Seg_4979" s="T79">"take me as your wife!"</ta>
            <ta e="T87" id="Seg_4980" s="T82">"Well, how will I take you?</ta>
            <ta e="T90" id="Seg_4981" s="T87">You are a frog."</ta>
            <ta e="T97" id="Seg_4982" s="T90">"Well, it is your fate, take me!"</ta>
            <ta e="T100" id="Seg_4983" s="T97">Then he took [it].</ta>
            <ta e="T102" id="Seg_4984" s="T100">He brought [it] home.</ta>
            <ta e="T107" id="Seg_4985" s="T102">Then [they] made the weddings.</ta>
            <ta e="T111" id="Seg_4986" s="T107">Then [the tsar] said: ”Sew shirts!”.</ta>
            <ta e="T116" id="Seg_4987" s="T111">Then Vayushka came home.</ta>
            <ta e="T120" id="Seg_4988" s="T116">And does not say anything.</ta>
            <ta e="T125" id="Seg_4989" s="T120">"Why don't you say anything?".</ta>
            <ta e="T130" id="Seg_4990" s="T125">"My father told, that [we] must sew a shirt."</ta>
            <ta e="T137" id="Seg_4991" s="T130">"Lie down, sleep, and the shirt will come."</ta>
            <ta e="T140" id="Seg_4992" s="T137">Then he slept.</ta>
            <ta e="T144" id="Seg_4993" s="T140">And it went outside.</ta>
            <ta e="T146" id="Seg_4994" s="T144">Took off its skin.</ta>
            <ta e="T150" id="Seg_4995" s="T146">It started to yell: "Sew me!.</ta>
            <ta e="T154" id="Seg_4996" s="T150">A shirt for the father, beautiful, good."</ta>
            <ta e="T158" id="Seg_4997" s="T154">Then it put on the skin again.</ta>
            <ta e="T161" id="Seg_4998" s="T158">It came into the house.</ta>
            <ta e="T164" id="Seg_4999" s="T161">It put its shirt on the table.</ta>
            <ta e="T168" id="Seg_5000" s="T164">On the table, then woke up its husband.</ta>
            <ta e="T171" id="Seg_5001" s="T168">"Well, take the shirt!</ta>
            <ta e="T173" id="Seg_5002" s="T171">Take it to his (your) father!"</ta>
            <ta e="T178" id="Seg_5003" s="T173">He went, his father took [the shirts].</ta>
            <ta e="T186" id="Seg_5004" s="T178">"The shirt of the older son, their shirt is [good] only for work. </ta>
            <ta e="T195" id="Seg_5005" s="T186">And the other son's shirt is only [good] for the bath."</ta>
            <ta e="T199" id="Seg_5006" s="T195">And this son gave [him a shirt].</ta>
            <ta e="T207" id="Seg_5007" s="T199">"And this shirt is only good do sit on the (throne?)."</ta>
            <ta e="T208" id="Seg_5008" s="T207">Enough!</ta>
            <ta e="T214" id="Seg_5009" s="T208">His father gathered a lot of people.</ta>
            <ta e="T216" id="Seg_5010" s="T214">He set a table.</ta>
            <ta e="T220" id="Seg_5011" s="T216">He called the sons and [their] wives.</ta>
            <ta e="T224" id="Seg_5012" s="T220">Then they came, they sat at the table.</ta>
            <ta e="T226" id="Seg_5013" s="T224">They ate meat.</ta>
            <ta e="T229" id="Seg_5014" s="T226">And the bones (?).</ta>
            <ta e="T236" id="Seg_5015" s="T229">Then his brothers came, they say: "Someone is coming."</ta>
            <ta e="T245" id="Seg_5016" s="T236">And he says: "My frog is coming."</ta>
            <ta e="T250" id="Seg_5017" s="T245">Then it came, very beautiful.</ta>
            <ta e="T253" id="Seg_5018" s="T250">Then they sat down.</ta>
            <ta e="T256" id="Seg_5019" s="T253">It is eating.</ta>
            <ta e="T260" id="Seg_5020" s="T256">And puts the bones into its sleeve.</ta>
            <ta e="T263" id="Seg_5021" s="T260">Pours vodka into its sleeve.</ta>
            <ta e="T266" id="Seg_5022" s="T263">Its pours tea there.</ta>
            <ta e="T268" id="Seg_5023" s="T266">Then they [finished to] eat.</ta>
            <ta e="T270" id="Seg_5024" s="T268">They went to dance.</ta>
            <ta e="T272" id="Seg_5025" s="T270">To play the accordeon.</ta>
            <ta e="T275" id="Seg_5026" s="T272">Then it [= the frog] danced.</ta>
            <ta e="T277" id="Seg_5027" s="T275">There appeared water.</ta>
            <ta e="T283" id="Seg_5028" s="T277">Then it [=the frog] waved her one hand.</ta>
            <ta e="T285" id="Seg_5029" s="T283">There appeared water.</ta>
            <ta e="T288" id="Seg_5030" s="T285">The tsar got scared.</ta>
            <ta e="T291" id="Seg_5031" s="T288">It waved her other hand.</ta>
            <ta e="T294" id="Seg_5032" s="T291">Then ducks came.</ta>
            <ta e="T297" id="Seg_5033" s="T294">Then everything disappeared.</ta>
            <ta e="T303" id="Seg_5034" s="T297">Then they [the other two] women waved with their hands.</ta>
            <ta e="T305" id="Seg_5035" s="T303">All the people…</ta>
            <ta e="T311" id="Seg_5036" s="T305">And hit all the people's faces with the bones.</ta>
            <ta e="T318" id="Seg_5037" s="T311">With the water, they splashed in their eyes.</ta>
            <ta e="T319" id="Seg_5038" s="T318">Enough!</ta>
            <ta e="T323" id="Seg_5039" s="T319">Then he ran home.</ta>
            <ta e="T330" id="Seg_5040" s="T323">He found her skin and took [it and] put it in the fire.</ta>
            <ta e="T333" id="Seg_5041" s="T330">It burned up.</ta>
            <ta e="T337" id="Seg_5042" s="T333">Then she came home.</ta>
            <ta e="T342" id="Seg_5043" s="T337">"You did something very bad!</ta>
            <ta e="T345" id="Seg_5044" s="T342">Now I will leave!</ta>
            <ta e="T349" id="Seg_5045" s="T345">And you will look for me!".</ta>
            <ta e="T351" id="Seg_5046" s="T349">And she left.</ta>
            <ta e="T358" id="Seg_5047" s="T351">And he cried, cried, then went [away].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_5048" s="T0">Es lebte ein Zar.</ta>
            <ta e="T7" id="Seg_5049" s="T3">Er hatte drei Söhne.</ta>
            <ta e="T11" id="Seg_5050" s="T7">Dann sagt er seinen Söhnen:</ta>
            <ta e="T15" id="Seg_5051" s="T11">"Es ist Zeit, euch Frauen zu suchen.</ta>
            <ta e="T21" id="Seg_5052" s="T15">Damit ich eure Kinder sehe."</ta>
            <ta e="T24" id="Seg_5053" s="T21">Nun, sie sagen:</ta>
            <ta e="T29" id="Seg_5054" s="T24">"Gut, suche, wen brauchst du?"</ta>
            <ta e="T33" id="Seg_5055" s="T29">"Nehmt Pistolen [= Bögen] und geht!</ta>
            <ta e="T35" id="Seg_5056" s="T33">Schießt Pfeile."</ta>
            <ta e="T39" id="Seg_5057" s="T35">Dann gingen sie.</ta>
            <ta e="T41" id="Seg_5058" s="T39">Einer schoss.</ta>
            <ta e="T45" id="Seg_5059" s="T41">Er schlug bei einem Kaufmannshaus ein.</ta>
            <ta e="T47" id="Seg_5060" s="T45">Einer [=der andere] schoss.</ta>
            <ta e="T49" id="Seg_5061" s="T47">Er schlug… </ta>
            <ta e="T50" id="Seg_5062" s="T49"> [KA:] Bojaren[palast].</ta>
            <ta e="T52" id="Seg_5063" s="T50">[PKZ:] …bei einem Bojarenpalast ein.</ta>
            <ta e="T55" id="Seg_5064" s="T52">Und einer [=der dritte] schoss.</ta>
            <ta e="T57" id="Seg_5065" s="T55">Er [der Pfeil] ging weit.</ta>
            <ta e="T62" id="Seg_5066" s="T57">Dann ging und ging er, er kommt.</ta>
            <ta e="T65" id="Seg_5067" s="T62">Ein Frosch sitzt im Sumpf.</ta>
            <ta e="T70" id="Seg_5068" s="T65">Und [er hält] den Pfeil.</ta>
            <ta e="T75" id="Seg_5069" s="T70">Er sagt: "Gib mir den Pfeil!"</ta>
            <ta e="T79" id="Seg_5070" s="T75">"Und du", sagt er, </ta>
            <ta e="T82" id="Seg_5071" s="T79">"nimm mich zur Frau!"</ta>
            <ta e="T87" id="Seg_5072" s="T82">"Nun, wie soll ich dich nehmen?</ta>
            <ta e="T90" id="Seg_5073" s="T87">Du bist ein Frosch."</ta>
            <ta e="T97" id="Seg_5074" s="T90">"Nun, das ist dein Schicksal, nimm mich!"</ta>
            <ta e="T100" id="Seg_5075" s="T97">Dann nahm er [ihn].</ta>
            <ta e="T102" id="Seg_5076" s="T100">Er brachte [ihn] nach Hause.</ta>
            <ta e="T107" id="Seg_5077" s="T102">Dann feierten ]sie] die Hochzeiten.</ta>
            <ta e="T111" id="Seg_5078" s="T107">Dann sagte [der Zar]: "Näht Hemden!"</ta>
            <ta e="T116" id="Seg_5079" s="T111">Dann kam Vanjuschka nach Hause.</ta>
            <ta e="T120" id="Seg_5080" s="T116">Und sagte nichts.</ta>
            <ta e="T125" id="Seg_5081" s="T120">"Warum sagst du nichts?"</ta>
            <ta e="T130" id="Seg_5082" s="T125">"Mein Vater sagte, dass [wir] ein Hemd nähen müssen."</ta>
            <ta e="T137" id="Seg_5083" s="T130">"Leg dich hin, schlafe, und das Hemd wird kommen."</ta>
            <ta e="T140" id="Seg_5084" s="T137">Dann schlief er.</ta>
            <ta e="T144" id="Seg_5085" s="T140">Und er [=der Frosch] ging hinaus.</ta>
            <ta e="T146" id="Seg_5086" s="T144">Zog seine Haut ab.</ta>
            <ta e="T150" id="Seg_5087" s="T146">Er begann zu rufen: "Näht mir!</ta>
            <ta e="T154" id="Seg_5088" s="T150">Ein Hemd für den Vater, ein schönes, ein gutes."</ta>
            <ta e="T158" id="Seg_5089" s="T154">Dann zog er die Haut wieder an.</ta>
            <ta e="T161" id="Seg_5090" s="T158">Er kam ins Haus.</ta>
            <ta e="T164" id="Seg_5091" s="T161">Er legte sein Hemd auf den Tisch.</ta>
            <ta e="T168" id="Seg_5092" s="T164">Auf den Tisch, weckte dann seinen Mann auf.</ta>
            <ta e="T171" id="Seg_5093" s="T168">"Nun, nimm das Hemd!</ta>
            <ta e="T173" id="Seg_5094" s="T171">Bring das zu seinem (deinem) Vater!"</ta>
            <ta e="T178" id="Seg_5095" s="T173">Er ging, sein Vater nahm [die Hemden].</ta>
            <ta e="T186" id="Seg_5096" s="T178">"Das Hemd des ältesten Sohns, ihr Hemd ist nur [gut] zum arbeiten.</ta>
            <ta e="T195" id="Seg_5097" s="T186">Und das Hemd des anderen Sohns ist nur [gut] zum baden."</ta>
            <ta e="T199" id="Seg_5098" s="T195">Und dieser Sohn gab [ihm sein Hemd].</ta>
            <ta e="T207" id="Seg_5099" s="T199">"Und dieses Hemd ist nur gut um auf dem (Thron?) zu sitzen."</ta>
            <ta e="T208" id="Seg_5100" s="T207">Genug!</ta>
            <ta e="T214" id="Seg_5101" s="T208">Sein Vater versammelte viele Leute.</ta>
            <ta e="T216" id="Seg_5102" s="T214">Er deckte einen Tisch.</ta>
            <ta e="T220" id="Seg_5103" s="T216">Er rief die Söhne und [ihre] Frauen.</ta>
            <ta e="T224" id="Seg_5104" s="T220">Dann kamen sie, sie setzten sich an den Tisch.</ta>
            <ta e="T226" id="Seg_5105" s="T224">Sie aßen Fleisch.</ta>
            <ta e="T229" id="Seg_5106" s="T226">Und die Knochen (?).</ta>
            <ta e="T236" id="Seg_5107" s="T229">Dann kamen seine Brüder, sie sagen: "Es kommt jemand."</ta>
            <ta e="T245" id="Seg_5108" s="T236">Und er sagt: "Mein Frosch kommt."</ta>
            <ta e="T250" id="Seg_5109" s="T245">Dann kam er, wunderschön.</ta>
            <ta e="T253" id="Seg_5110" s="T250">Dann setzten sie sich.</ta>
            <ta e="T256" id="Seg_5111" s="T253">Er [=der Frosch] isst.</ta>
            <ta e="T260" id="Seg_5112" s="T256">Und steckt die Knochen in seinen Ärmel.</ta>
            <ta e="T263" id="Seg_5113" s="T260">Gießt Wodka in seinen Ärmel.</ta>
            <ta e="T266" id="Seg_5114" s="T263">Er gießt Tee dorthin.</ta>
            <ta e="T268" id="Seg_5115" s="T266">Dann [hörten sie auf] zu essen.</ta>
            <ta e="T270" id="Seg_5116" s="T268">Sie gingen tanzen.</ta>
            <ta e="T272" id="Seg_5117" s="T270">Akkordeon spielen.</ta>
            <ta e="T275" id="Seg_5118" s="T272">Dann tanzte er [=der Frosch].</ta>
            <ta e="T277" id="Seg_5119" s="T275">Es erschien Wasser.</ta>
            <ta e="T283" id="Seg_5120" s="T277">Dann winkte er [=der Frosch] mit einer Hand.</ta>
            <ta e="T285" id="Seg_5121" s="T283">Es erschien Wasser.</ta>
            <ta e="T288" id="Seg_5122" s="T285">Der Zar bekam Angst.</ta>
            <ta e="T291" id="Seg_5123" s="T288">Er [=der Frosch] winkte mit der anderen Hand.</ta>
            <ta e="T294" id="Seg_5124" s="T291">Dann kamen Enten.</ta>
            <ta e="T297" id="Seg_5125" s="T294">Dann verschwand alles.</ta>
            <ta e="T303" id="Seg_5126" s="T297">Dann winkten [die anderen beiden] Frauen mit ihren Händen.</ta>
            <ta e="T305" id="Seg_5127" s="T303">Allen Leuten…</ta>
            <ta e="T311" id="Seg_5128" s="T305">Und schlugen allen Leuten mit den Knochen ins Gesicht.</ta>
            <ta e="T318" id="Seg_5129" s="T311">Mit dem Wasser spritzten sie ihnen in die Augen.</ta>
            <ta e="T319" id="Seg_5130" s="T318">Genug!</ta>
            <ta e="T323" id="Seg_5131" s="T319">Dann rannte er nach Hause.</ta>
            <ta e="T330" id="Seg_5132" s="T323">Er fand ihre Haut und nahm sie und legte sie ins Feuer.</ta>
            <ta e="T333" id="Seg_5133" s="T330">Sie verbrannte.</ta>
            <ta e="T337" id="Seg_5134" s="T333">Dann kam er [=der Frosch] nach Hause.</ta>
            <ta e="T342" id="Seg_5135" s="T337">"Du hast etwas sehr Schlechtes getan!</ta>
            <ta e="T345" id="Seg_5136" s="T342">Jetzt werde ich gehen!</ta>
            <ta e="T349" id="Seg_5137" s="T345">Und du wirst mich suchen!"</ta>
            <ta e="T351" id="Seg_5138" s="T349">Und sie ging.</ta>
            <ta e="T358" id="Seg_5139" s="T351">Und er weinte, weinte und ging dann [weg].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T65" id="Seg_5140" s="T62">[GVY:] lʼaguška, dĭ…?</ta>
            <ta e="T70" id="Seg_5141" s="T65">[GVY:] Syntax unclear.</ta>
            <ta e="T107" id="Seg_5142" s="T102"> || Свадьба Ru. ’wedding’.</ta>
            <ta e="T164" id="Seg_5143" s="T161">[GVY:] stolbə = stoldə [stol-Lat]?</ta>
            <ta e="T186" id="Seg_5144" s="T178">[GVY:] Or dĭ kujnek tĭn 'this shirt of his'? </ta>
            <ta e="T207" id="Seg_5145" s="T199">[GVY:] In the standard version of the tale, the czar says that this shirt is good for a feast. Or sarskən amnozittə means "live as a czar"?</ta>
            <ta e="T277" id="Seg_5146" s="T275">[GVY:] See the following sentences</ta>
            <ta e="T318" id="Seg_5147" s="T311">[GVY:] in the tale, in the czar's eye.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T363" />
            <conversion-tli id="T50" />
            <conversion-tli id="T364" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T359" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T360" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T361" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T362" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
