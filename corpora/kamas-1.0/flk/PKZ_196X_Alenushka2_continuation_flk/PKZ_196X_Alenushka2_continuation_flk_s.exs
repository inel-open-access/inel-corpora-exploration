<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD1B701FE-459E-0049-C2CC-93EA85B1D416">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Alenushka2_continuation_flk.wav" />
         <referenced-file url="PKZ_196X_Alenushka2_continuation_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Alenushka2_continuation_flk\PKZ_196X_Alenushka2_continuation_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">261</ud-information>
            <ud-information attribute-name="# HIAT:w">162</ud-information>
            <ud-information attribute-name="# e">162</ud-information>
            <ud-information attribute-name="# HIAT:u">32</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.848" type="appl" />
         <tli id="T2" time="1.695" type="appl" />
         <tli id="T3" time="2.939922552151774" />
         <tli id="T4" time="3.579" type="appl" />
         <tli id="T5" time="4.289" type="appl" />
         <tli id="T6" time="4.917" type="appl" />
         <tli id="T7" time="5.545" type="appl" />
         <tli id="T8" time="6.173" type="appl" />
         <tli id="T9" time="6.801" type="appl" />
         <tli id="T10" time="7.429" type="appl" />
         <tli id="T11" time="7.918" type="appl" />
         <tli id="T12" time="8.408" type="appl" />
         <tli id="T13" time="8.897" type="appl" />
         <tli id="T14" time="9.386" type="appl" />
         <tli id="T15" time="9.876" type="appl" />
         <tli id="T16" time="10.99971022913929" />
         <tli id="T17" time="11.493" type="appl" />
         <tli id="T18" time="11.95" type="appl" />
         <tli id="T19" time="12.407" type="appl" />
         <tli id="T20" time="12.864" type="appl" />
         <tli id="T21" time="13.321" type="appl" />
         <tli id="T22" time="13.778" type="appl" />
         <tli id="T23" time="14.235" type="appl" />
         <tli id="T24" time="14.692" type="appl" />
         <tli id="T25" time="15.149" type="appl" />
         <tli id="T26" time="15.873" type="appl" />
         <tli id="T27" time="16.572" type="appl" />
         <tli id="T28" time="17.27" type="appl" />
         <tli id="T29" time="17.969" type="appl" />
         <tli id="T30" time="18.667" type="appl" />
         <tli id="T31" time="19.366" type="appl" />
         <tli id="T32" time="20.064" type="appl" />
         <tli id="T33" time="20.712" type="appl" />
         <tli id="T34" time="21.124" type="appl" />
         <tli id="T35" time="21.537" type="appl" />
         <tli id="T36" time="21.95" type="appl" />
         <tli id="T37" time="22.362" type="appl" />
         <tli id="T38" time="22.775" type="appl" />
         <tli id="T39" time="23.187" type="appl" />
         <tli id="T40" time="23.6" type="appl" />
         <tli id="T41" time="24.25" type="appl" />
         <tli id="T42" time="24.79" type="appl" />
         <tli id="T43" time="25.33" type="appl" />
         <tli id="T44" time="25.87" type="appl" />
         <tli id="T45" time="26.456664692513243" />
         <tli id="T46" time="27.292" type="appl" />
         <tli id="T47" time="28.132" type="appl" />
         <tli id="T48" time="28.972" type="appl" />
         <tli id="T49" time="30.812521623685942" />
         <tli id="T50" time="31.728" type="appl" />
         <tli id="T51" time="32.5" type="appl" />
         <tli id="T52" time="33.273" type="appl" />
         <tli id="T53" time="34.21243205814718" />
         <tli id="T54" time="35.044" type="appl" />
         <tli id="T55" time="35.725" type="appl" />
         <tli id="T56" time="36.406" type="appl" />
         <tli id="T57" time="37.088" type="appl" />
         <tli id="T58" time="37.769" type="appl" />
         <tli id="T59" time="38.51231878408344" />
         <tli id="T60" time="39.319" type="appl" />
         <tli id="T61" time="39.988" type="appl" />
         <tli id="T62" time="40.656" type="appl" />
         <tli id="T63" time="41.286" type="appl" />
         <tli id="T64" time="42.13222342312746" />
         <tli id="T65" time="43.125" type="appl" />
         <tli id="T66" time="43.85" type="appl" />
         <tli id="T67" time="44.574" type="appl" />
         <tli id="T68" time="46.10545208771354" />
         <tli id="T69" time="47.646" type="appl" />
         <tli id="T70" time="48.93" type="appl" />
         <tli id="T71" time="50.214" type="appl" />
         <tli id="T72" time="51.498" type="appl" />
         <tli id="T73" time="52.782" type="appl" />
         <tli id="T74" time="55.125214475607756" />
         <tli id="T75" time="56.563" type="appl" />
         <tli id="T76" time="57.779" type="appl" />
         <tli id="T77" time="58.958" type="appl" />
         <tli id="T78" time="60.37840942140276" />
         <tli id="T79" time="61.571" type="appl" />
         <tli id="T80" time="62.43" type="appl" />
         <tli id="T81" time="63.29" type="appl" />
         <tli id="T82" time="64.149" type="appl" />
         <tli id="T83" time="65.654" type="appl" />
         <tli id="T84" time="66.929" type="appl" />
         <tli id="T85" time="68.205" type="appl" />
         <tli id="T86" time="71.77144262237188" />
         <tli id="T87" time="72.768" type="appl" />
         <tli id="T88" time="73.484" type="appl" />
         <tli id="T89" time="74.40470658631735" />
         <tli id="T90" time="75.412" type="appl" />
         <tli id="T91" time="76.119" type="appl" />
         <tli id="T92" time="76.826" type="appl" />
         <tli id="T93" time="77.56" type="appl" />
         <tli id="T94" time="78.293" type="appl" />
         <tli id="T95" time="79.027" type="appl" />
         <tli id="T96" time="79.761" type="appl" />
         <tli id="T97" time="80.495" type="appl" />
         <tli id="T98" time="81.228" type="appl" />
         <tli id="T99" time="82.43116180806504" />
         <tli id="T100" time="82.961" type="appl" />
         <tli id="T101" time="83.519" type="appl" />
         <tli id="T102" time="84.078" type="appl" />
         <tli id="T103" time="84.636" type="appl" />
         <tli id="T104" time="85.195" type="appl" />
         <tli id="T105" time="85.754" type="appl" />
         <tli id="T106" time="86.312" type="appl" />
         <tli id="T107" time="87.33769921936597" />
         <tli id="T108" time="88.049" type="appl" />
         <tli id="T109" time="88.537" type="appl" />
         <tli id="T110" time="89.025" type="appl" />
         <tli id="T111" time="89.513" type="appl" />
         <tli id="T112" time="90.001" type="appl" />
         <tli id="T113" time="90.488" type="appl" />
         <tli id="T114" time="90.976" type="appl" />
         <tli id="T115" time="91.464" type="appl" />
         <tli id="T116" time="91.952" type="appl" />
         <tli id="T117" time="92.44" type="appl" />
         <tli id="T118" time="92.928" type="appl" />
         <tli id="T119" time="93.828" type="appl" />
         <tli id="T120" time="94.394" type="appl" />
         <tli id="T121" time="94.96" type="appl" />
         <tli id="T122" time="95.527" type="appl" />
         <tli id="T123" time="96.093" type="appl" />
         <tli id="T124" time="96.659" type="appl" />
         <tli id="T125" time="97.225" type="appl" />
         <tli id="T126" time="97.791" type="appl" />
         <tli id="T127" time="98.358" type="appl" />
         <tli id="T128" time="98.924" type="appl" />
         <tli id="T129" time="99.49" type="appl" />
         <tli id="T130" time="100.75067920786795" />
         <tli id="T131" time="101.637" type="appl" />
         <tli id="T132" time="102.294" type="appl" />
         <tli id="T133" time="102.95" type="appl" />
         <tli id="T134" time="103.607" type="appl" />
         <tli id="T135" time="104.263" type="appl" />
         <tli id="T136" time="105.046" type="appl" />
         <tli id="T137" time="105.828" type="appl" />
         <tli id="T138" time="106.611" type="appl" />
         <tli id="T139" time="107.81049322766097" />
         <tli id="T140" time="109.316" type="appl" />
         <tli id="T141" time="111.04374138592313" />
         <tli id="T142" time="111.893" type="appl" />
         <tli id="T143" time="112.482" type="appl" />
         <tli id="T144" time="113.071" type="appl" />
         <tli id="T145" time="113.66" type="appl" />
         <tli id="T146" time="114.249" type="appl" />
         <tli id="T147" time="114.839" type="appl" />
         <tli id="T148" time="115.428" type="appl" />
         <tli id="T149" time="116.017" type="appl" />
         <tli id="T150" time="116.606" type="appl" />
         <tli id="T151" time="117.195" type="appl" />
         <tli id="T152" time="118.13688786095598" />
         <tli id="T153" time="119.039" type="appl" />
         <tli id="T154" time="119.821" type="appl" />
         <tli id="T155" time="120.604" type="appl" />
         <tli id="T156" time="121.387" type="appl" />
         <tli id="T157" time="122.169" type="appl" />
         <tli id="T158" time="123.19008807531209" />
         <tli id="T159" time="124.164" type="appl" />
         <tli id="T160" time="125.008" type="appl" />
         <tli id="T161" time="125.853" type="appl" />
         <tli id="T162" time="126.83" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T162" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">šobi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kăldunʼnʼa</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭm</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ibi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Pibə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">sarbi</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">da</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">bünə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">baʔluʔpi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">A</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">bostə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">dĭn</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">oldʼabə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">s-</ts>
                  <nts id="Seg_57" n="HIAT:ip">)</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">šerbi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_64" n="HIAT:u" s="T16">
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">Dĭ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">kuza</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">š-</ts>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">Dĭ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">tibi</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">šobi</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">dĭm</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">ej</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">tĭmnebi</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_97" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">Dĭ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">măndə:</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_104" n="HIAT:ip">"</nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">Davaj</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">bătleʔim</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_111" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">dĭ=</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">dĭ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">poʔto</ts>
                  <nts id="Seg_121" n="HIAT:ip">"</nts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_125" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">A</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">dĭ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">măndə:</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">Ĭmbi</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_139" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">dĭ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">dăre</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">ajirbi</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">dĭn</ts>
                  <nts id="Seg_151" n="HIAT:ip">)</nts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_155" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">A</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_160" n="HIAT:w" s="T41">tüj</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">băʔsittə</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">dĭm</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_170" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_172" n="HIAT:w" s="T44">xočet</ts>
                  <nts id="Seg_173" n="HIAT:ip">)</nts>
                  <nts id="Seg_174" n="HIAT:ip">"</nts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_178" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">Dĭgəttə</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">bar</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">aspaʔi</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">mĭnzərləʔbəʔjə</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_193" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">I</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_198" n="HIAT:w" s="T50">dagaʔi</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_200" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">püʔmileʔbəʔjə</ts>
                  <nts id="Seg_203" n="HIAT:ip">)</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_206" n="HIAT:w" s="T52">bar</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_210" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_212" n="HIAT:w" s="T53">A</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">dĭ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">măndə:</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_220" n="HIAT:ip">"</nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">Măn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">kalam</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_228" n="HIAT:w" s="T58">bünə</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_232" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_234" n="HIAT:w" s="T59">Bĭtlem</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_236" n="HIAT:ip">(</nts>
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">šalaʔjə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">da</ts>
                  <nts id="Seg_242" n="HIAT:ip">)</nts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_246" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_248" n="HIAT:w" s="T62">Noʔ</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_250" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_252" n="HIAT:w" s="T63">amorlam</ts>
                  <nts id="Seg_253" n="HIAT:ip">)</nts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_258" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_260" n="HIAT:w" s="T64">Kambi</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_263" n="HIAT:w" s="T65">bar</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_267" n="HIAT:w" s="T66">bügən</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_270" n="HIAT:w" s="T67">dʼorlaʔbə</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_274" n="HIAT:u" s="T68">
                  <nts id="Seg_275" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_277" n="HIAT:w" s="T68">A-</ts>
                  <nts id="Seg_278" n="HIAT:ip">)</nts>
                  <nts id="Seg_279" n="HIAT:ip">"</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_282" n="HIAT:w" s="T69">Sʼestram</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_286" n="HIAT:w" s="T70">sʼestram</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_290" n="HIAT:w" s="T71">măna</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_293" n="HIAT:w" s="T72">dʼagarzittə</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_296" n="HIAT:w" s="T73">xočet</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_300" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_302" n="HIAT:w" s="T74">Aspaʔi</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_305" n="HIAT:w" s="T75">mĭnzəlleʔbəʔjə</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_309" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_311" n="HIAT:w" s="T76">Dagaʔi</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_314" n="HIAT:w" s="T77">püʔmileʔbəʔjə</ts>
                  <nts id="Seg_315" n="HIAT:ip">"</nts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_319" n="HIAT:u" s="T78">
                  <nts id="Seg_320" n="HIAT:ip">"</nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">Măna</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">pi</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_328" n="HIAT:w" s="T80">pʼaŋdlaʔbə</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_331" n="HIAT:w" s="T81">dʼünə</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_335" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">Noʔ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">üjüm</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_343" n="HIAT:w" s="T84">bar</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_346" n="HIAT:w" s="T85">zaputalʼi</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_350" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_352" n="HIAT:w" s="T86">Üjüm</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_355" n="HIAT:w" s="T87">kürbiʔi</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_358" n="HIAT:w" s="T88">măn</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_362" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_364" n="HIAT:w" s="T89">Ej</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_367" n="HIAT:w" s="T90">moliam</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_370" n="HIAT:w" s="T91">oʔbdəsʼtə</ts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_375" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_377" n="HIAT:w" s="T92">Dĭgəttə</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_380" n="HIAT:w" s="T93">šobi</ts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_384" n="HIAT:w" s="T94">bazoʔ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_387" n="HIAT:w" s="T95">mănlia:</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_389" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_391" n="HIAT:w" s="T96">Măn</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_394" n="HIAT:w" s="T97">kalam</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_397" n="HIAT:w" s="T98">bünə</ts>
                  <nts id="Seg_398" n="HIAT:ip">"</nts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_402" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_404" n="HIAT:w" s="T99">Dĭgəttə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_407" n="HIAT:w" s="T100">kambi</ts>
                  <nts id="Seg_408" n="HIAT:ip">,</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_411" n="HIAT:w" s="T101">bazoʔ</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_414" n="HIAT:w" s="T102">dʼorbi</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_418" n="HIAT:w" s="T103">bazoʔ</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_420" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_422" n="HIAT:w" s="T104">dăre</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_425" n="HIAT:w" s="T105">že</ts>
                  <nts id="Seg_426" n="HIAT:ip">)</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_429" n="HIAT:w" s="T106">nörbəbi</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_433" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_435" n="HIAT:w" s="T107">Dĭgəttə</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_438" n="HIAT:w" s="T108">dĭ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_441" n="HIAT:w" s="T109">tibi</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_444" n="HIAT:w" s="T110">măndə:</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_446" n="HIAT:ip">"</nts>
                  <nts id="Seg_447" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_449" n="HIAT:w" s="T111">Ĭm-</ts>
                  <nts id="Seg_450" n="HIAT:ip">)</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_453" n="HIAT:w" s="T112">Ĭmbi</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_456" n="HIAT:w" s="T113">üge</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_459" n="HIAT:w" s="T114">dĭ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_462" n="HIAT:w" s="T115">dĭbər</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_464" n="HIAT:ip">(</nts>
                  <ts e="T117" id="Seg_466" n="HIAT:w" s="T116">nu-</ts>
                  <nts id="Seg_467" n="HIAT:ip">)</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_470" n="HIAT:w" s="T117">nuʔməleʔbə</ts>
                  <nts id="Seg_471" n="HIAT:ip">?</nts>
                  <nts id="Seg_472" n="HIAT:ip">"</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_475" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_477" n="HIAT:w" s="T118">Dĭ</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_480" n="HIAT:w" s="T119">bazoʔ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_483" n="HIAT:w" s="T120">pʼeluʔpi</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_488" n="HIAT:w" s="T121">dĭ-</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_492" n="HIAT:w" s="T122">kambi</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_494" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_496" n="HIAT:w" s="T123">dĭ=</ts>
                  <nts id="Seg_497" n="HIAT:ip">)</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_500" n="HIAT:w" s="T124">dĭ</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_503" n="HIAT:w" s="T125">tože</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_505" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_507" n="HIAT:w" s="T126">dĭ-</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_510" n="HIAT:w" s="T127">dĭ-</ts>
                  <nts id="Seg_511" n="HIAT:ip">)</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_514" n="HIAT:w" s="T128">dĭʔnə</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_517" n="HIAT:w" s="T129">kambi</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_521" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_523" n="HIAT:w" s="T130">Nünəbi</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_526" n="HIAT:w" s="T131">bar</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_528" n="HIAT:ip">(</nts>
                  <ts e="T133" id="Seg_530" n="HIAT:w" s="T132">dĭ-</ts>
                  <nts id="Seg_531" n="HIAT:ip">)</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_534" n="HIAT:w" s="T133">dĭm</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_536" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_538" n="HIAT:w" s="T134">kăštlaʔbə</ts>
                  <nts id="Seg_539" n="HIAT:ip">)</nts>
                  <nts id="Seg_540" n="HIAT:ip">.</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_543" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_545" n="HIAT:w" s="T135">Dĭgəttə</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_547" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_549" n="HIAT:w" s="T136">š-</ts>
                  <nts id="Seg_550" n="HIAT:ip">)</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_553" n="HIAT:w" s="T137">iləm</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_556" n="HIAT:w" s="T138">oʔbdəbi</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_560" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_562" n="HIAT:w" s="T139">Dĭm</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_565" n="HIAT:w" s="T140">dʼaʔpiʔi</ts>
                  <nts id="Seg_566" n="HIAT:ip">.</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_569" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_571" n="HIAT:w" s="T141">I</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_574" n="HIAT:w" s="T142">dĭgəttə</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_577" n="HIAT:w" s="T143">dĭ</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_580" n="HIAT:w" s="T144">vedʼmam</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_582" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_584" n="HIAT:w" s="T145">ku-</ts>
                  <nts id="Seg_585" n="HIAT:ip">)</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_588" n="HIAT:w" s="T146">kuʔpiʔi</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_592" n="HIAT:w" s="T147">a</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_595" n="HIAT:w" s="T148">bostən</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_598" n="HIAT:w" s="T149">bazoʔ</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_601" n="HIAT:w" s="T150">davaj</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_604" n="HIAT:w" s="T151">amnosʼtə</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_608" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_610" n="HIAT:w" s="T152">I</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_613" n="HIAT:w" s="T153">dĭ</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_616" n="HIAT:w" s="T154">poʔto</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_618" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_620" n="HIAT:w" s="T155">kuzab-</ts>
                  <nts id="Seg_621" n="HIAT:ip">)</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_624" n="HIAT:w" s="T156">kuzazi</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_627" n="HIAT:w" s="T157">mobi</ts>
                  <nts id="Seg_628" n="HIAT:ip">.</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_631" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_633" n="HIAT:w" s="T158">I</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_636" n="HIAT:w" s="T159">nagurgöʔ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_639" n="HIAT:w" s="T160">amnolaʔbəʔjə</ts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_643" n="HIAT:u" s="T161">
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T162" id="Seg_646" n="HIAT:w" s="T161">Kabarləj</ts>
                  <nts id="Seg_647" n="HIAT:ip">)</nts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T162" id="Seg_650" n="sc" s="T0">
               <ts e="T1" id="Seg_652" n="e" s="T0">Dĭgəttə </ts>
               <ts e="T2" id="Seg_654" n="e" s="T1">šobi </ts>
               <ts e="T3" id="Seg_656" n="e" s="T2">kăldunʼnʼa. </ts>
               <ts e="T4" id="Seg_658" n="e" s="T3">Dĭm </ts>
               <ts e="T5" id="Seg_660" n="e" s="T4">ibi. </ts>
               <ts e="T6" id="Seg_662" n="e" s="T5">Pibə </ts>
               <ts e="T7" id="Seg_664" n="e" s="T6">sarbi </ts>
               <ts e="T8" id="Seg_666" n="e" s="T7">da </ts>
               <ts e="T9" id="Seg_668" n="e" s="T8">bünə </ts>
               <ts e="T10" id="Seg_670" n="e" s="T9">baʔluʔpi. </ts>
               <ts e="T11" id="Seg_672" n="e" s="T10">A </ts>
               <ts e="T12" id="Seg_674" n="e" s="T11">bostə </ts>
               <ts e="T13" id="Seg_676" n="e" s="T12">dĭn </ts>
               <ts e="T14" id="Seg_678" n="e" s="T13">oldʼabə </ts>
               <ts e="T15" id="Seg_680" n="e" s="T14">(s-) </ts>
               <ts e="T16" id="Seg_682" n="e" s="T15">šerbi. </ts>
               <ts e="T17" id="Seg_684" n="e" s="T16">(Dĭ </ts>
               <ts e="T18" id="Seg_686" n="e" s="T17">kuza </ts>
               <ts e="T19" id="Seg_688" n="e" s="T18">š-) </ts>
               <ts e="T20" id="Seg_690" n="e" s="T19">Dĭ </ts>
               <ts e="T21" id="Seg_692" n="e" s="T20">tibi </ts>
               <ts e="T22" id="Seg_694" n="e" s="T21">šobi, </ts>
               <ts e="T23" id="Seg_696" n="e" s="T22">dĭm </ts>
               <ts e="T24" id="Seg_698" n="e" s="T23">ej </ts>
               <ts e="T25" id="Seg_700" n="e" s="T24">tĭmnebi. </ts>
               <ts e="T26" id="Seg_702" n="e" s="T25">Dĭ </ts>
               <ts e="T27" id="Seg_704" n="e" s="T26">măndə: </ts>
               <ts e="T28" id="Seg_706" n="e" s="T27">"Davaj </ts>
               <ts e="T29" id="Seg_708" n="e" s="T28">bătleʔim </ts>
               <ts e="T30" id="Seg_710" n="e" s="T29">(dĭ=) </ts>
               <ts e="T31" id="Seg_712" n="e" s="T30">dĭ </ts>
               <ts e="T32" id="Seg_714" n="e" s="T31">poʔto". </ts>
               <ts e="T33" id="Seg_716" n="e" s="T32">A </ts>
               <ts e="T34" id="Seg_718" n="e" s="T33">dĭ </ts>
               <ts e="T35" id="Seg_720" n="e" s="T34">măndə: </ts>
               <ts e="T36" id="Seg_722" n="e" s="T35">"Ĭmbi </ts>
               <ts e="T37" id="Seg_724" n="e" s="T36">(dĭ </ts>
               <ts e="T38" id="Seg_726" n="e" s="T37">dăre </ts>
               <ts e="T39" id="Seg_728" n="e" s="T38">ajirbi </ts>
               <ts e="T40" id="Seg_730" n="e" s="T39">dĭn). </ts>
               <ts e="T41" id="Seg_732" n="e" s="T40">A </ts>
               <ts e="T42" id="Seg_734" n="e" s="T41">tüj </ts>
               <ts e="T43" id="Seg_736" n="e" s="T42">(băʔsittə) </ts>
               <ts e="T44" id="Seg_738" n="e" s="T43">dĭm </ts>
               <ts e="T45" id="Seg_740" n="e" s="T44">(xočet)". </ts>
               <ts e="T46" id="Seg_742" n="e" s="T45">Dĭgəttə </ts>
               <ts e="T47" id="Seg_744" n="e" s="T46">bar </ts>
               <ts e="T48" id="Seg_746" n="e" s="T47">aspaʔi </ts>
               <ts e="T49" id="Seg_748" n="e" s="T48">mĭnzərləʔbəʔjə. </ts>
               <ts e="T50" id="Seg_750" n="e" s="T49">I </ts>
               <ts e="T51" id="Seg_752" n="e" s="T50">dagaʔi </ts>
               <ts e="T52" id="Seg_754" n="e" s="T51">(püʔmileʔbəʔjə) </ts>
               <ts e="T53" id="Seg_756" n="e" s="T52">bar. </ts>
               <ts e="T54" id="Seg_758" n="e" s="T53">A </ts>
               <ts e="T55" id="Seg_760" n="e" s="T54">dĭ </ts>
               <ts e="T56" id="Seg_762" n="e" s="T55">măndə: </ts>
               <ts e="T57" id="Seg_764" n="e" s="T56">"Măn </ts>
               <ts e="T58" id="Seg_766" n="e" s="T57">kalam </ts>
               <ts e="T59" id="Seg_768" n="e" s="T58">bünə. </ts>
               <ts e="T60" id="Seg_770" n="e" s="T59">Bĭtlem </ts>
               <ts e="T61" id="Seg_772" n="e" s="T60">(šalaʔjə </ts>
               <ts e="T62" id="Seg_774" n="e" s="T61">da). </ts>
               <ts e="T63" id="Seg_776" n="e" s="T62">Noʔ </ts>
               <ts e="T64" id="Seg_778" n="e" s="T63">(amorlam)". </ts>
               <ts e="T65" id="Seg_780" n="e" s="T64">Kambi </ts>
               <ts e="T66" id="Seg_782" n="e" s="T65">bar, </ts>
               <ts e="T67" id="Seg_784" n="e" s="T66">bügən </ts>
               <ts e="T68" id="Seg_786" n="e" s="T67">dʼorlaʔbə. </ts>
               <ts e="T69" id="Seg_788" n="e" s="T68">(A-)" </ts>
               <ts e="T70" id="Seg_790" n="e" s="T69">Sʼestram, </ts>
               <ts e="T71" id="Seg_792" n="e" s="T70">sʼestram, </ts>
               <ts e="T72" id="Seg_794" n="e" s="T71">măna </ts>
               <ts e="T73" id="Seg_796" n="e" s="T72">dʼagarzittə </ts>
               <ts e="T74" id="Seg_798" n="e" s="T73">xočet. </ts>
               <ts e="T75" id="Seg_800" n="e" s="T74">Aspaʔi </ts>
               <ts e="T76" id="Seg_802" n="e" s="T75">mĭnzəlleʔbəʔjə. </ts>
               <ts e="T77" id="Seg_804" n="e" s="T76">Dagaʔi </ts>
               <ts e="T78" id="Seg_806" n="e" s="T77">püʔmileʔbəʔjə". </ts>
               <ts e="T79" id="Seg_808" n="e" s="T78">"Măna </ts>
               <ts e="T80" id="Seg_810" n="e" s="T79">pi </ts>
               <ts e="T81" id="Seg_812" n="e" s="T80">pʼaŋdlaʔbə </ts>
               <ts e="T82" id="Seg_814" n="e" s="T81">dʼünə. </ts>
               <ts e="T83" id="Seg_816" n="e" s="T82">Noʔ </ts>
               <ts e="T84" id="Seg_818" n="e" s="T83">üjüm </ts>
               <ts e="T85" id="Seg_820" n="e" s="T84">bar </ts>
               <ts e="T86" id="Seg_822" n="e" s="T85">zaputalʼi. </ts>
               <ts e="T87" id="Seg_824" n="e" s="T86">Üjüm </ts>
               <ts e="T88" id="Seg_826" n="e" s="T87">kürbiʔi </ts>
               <ts e="T89" id="Seg_828" n="e" s="T88">măn. </ts>
               <ts e="T90" id="Seg_830" n="e" s="T89">Ej </ts>
               <ts e="T91" id="Seg_832" n="e" s="T90">moliam </ts>
               <ts e="T92" id="Seg_834" n="e" s="T91">oʔbdəsʼtə". </ts>
               <ts e="T93" id="Seg_836" n="e" s="T92">Dĭgəttə </ts>
               <ts e="T94" id="Seg_838" n="e" s="T93">šobi, </ts>
               <ts e="T95" id="Seg_840" n="e" s="T94">bazoʔ </ts>
               <ts e="T96" id="Seg_842" n="e" s="T95">mănlia: </ts>
               <ts e="T97" id="Seg_844" n="e" s="T96">"Măn </ts>
               <ts e="T98" id="Seg_846" n="e" s="T97">kalam </ts>
               <ts e="T99" id="Seg_848" n="e" s="T98">bünə". </ts>
               <ts e="T100" id="Seg_850" n="e" s="T99">Dĭgəttə </ts>
               <ts e="T101" id="Seg_852" n="e" s="T100">kambi, </ts>
               <ts e="T102" id="Seg_854" n="e" s="T101">bazoʔ </ts>
               <ts e="T103" id="Seg_856" n="e" s="T102">dʼorbi, </ts>
               <ts e="T104" id="Seg_858" n="e" s="T103">bazoʔ </ts>
               <ts e="T105" id="Seg_860" n="e" s="T104">(dăre </ts>
               <ts e="T106" id="Seg_862" n="e" s="T105">že) </ts>
               <ts e="T107" id="Seg_864" n="e" s="T106">nörbəbi. </ts>
               <ts e="T108" id="Seg_866" n="e" s="T107">Dĭgəttə </ts>
               <ts e="T109" id="Seg_868" n="e" s="T108">dĭ </ts>
               <ts e="T110" id="Seg_870" n="e" s="T109">tibi </ts>
               <ts e="T111" id="Seg_872" n="e" s="T110">măndə: </ts>
               <ts e="T112" id="Seg_874" n="e" s="T111">"(Ĭm-) </ts>
               <ts e="T113" id="Seg_876" n="e" s="T112">Ĭmbi </ts>
               <ts e="T114" id="Seg_878" n="e" s="T113">üge </ts>
               <ts e="T115" id="Seg_880" n="e" s="T114">dĭ </ts>
               <ts e="T116" id="Seg_882" n="e" s="T115">dĭbər </ts>
               <ts e="T117" id="Seg_884" n="e" s="T116">(nu-) </ts>
               <ts e="T118" id="Seg_886" n="e" s="T117">nuʔməleʔbə?" </ts>
               <ts e="T119" id="Seg_888" n="e" s="T118">Dĭ </ts>
               <ts e="T120" id="Seg_890" n="e" s="T119">bazoʔ </ts>
               <ts e="T121" id="Seg_892" n="e" s="T120">pʼeluʔpi, </ts>
               <ts e="T122" id="Seg_894" n="e" s="T121">(dĭ-) </ts>
               <ts e="T123" id="Seg_896" n="e" s="T122">kambi </ts>
               <ts e="T124" id="Seg_898" n="e" s="T123">(dĭ=) </ts>
               <ts e="T125" id="Seg_900" n="e" s="T124">dĭ </ts>
               <ts e="T126" id="Seg_902" n="e" s="T125">tože </ts>
               <ts e="T127" id="Seg_904" n="e" s="T126">(dĭ- </ts>
               <ts e="T128" id="Seg_906" n="e" s="T127">dĭ-) </ts>
               <ts e="T129" id="Seg_908" n="e" s="T128">dĭʔnə </ts>
               <ts e="T130" id="Seg_910" n="e" s="T129">kambi. </ts>
               <ts e="T131" id="Seg_912" n="e" s="T130">Nünəbi </ts>
               <ts e="T132" id="Seg_914" n="e" s="T131">bar </ts>
               <ts e="T133" id="Seg_916" n="e" s="T132">(dĭ-) </ts>
               <ts e="T134" id="Seg_918" n="e" s="T133">dĭm </ts>
               <ts e="T135" id="Seg_920" n="e" s="T134">(kăštlaʔbə). </ts>
               <ts e="T136" id="Seg_922" n="e" s="T135">Dĭgəttə </ts>
               <ts e="T137" id="Seg_924" n="e" s="T136">(š-) </ts>
               <ts e="T138" id="Seg_926" n="e" s="T137">iləm </ts>
               <ts e="T139" id="Seg_928" n="e" s="T138">oʔbdəbi. </ts>
               <ts e="T140" id="Seg_930" n="e" s="T139">Dĭm </ts>
               <ts e="T141" id="Seg_932" n="e" s="T140">dʼaʔpiʔi. </ts>
               <ts e="T142" id="Seg_934" n="e" s="T141">I </ts>
               <ts e="T143" id="Seg_936" n="e" s="T142">dĭgəttə </ts>
               <ts e="T144" id="Seg_938" n="e" s="T143">dĭ </ts>
               <ts e="T145" id="Seg_940" n="e" s="T144">vedʼmam </ts>
               <ts e="T146" id="Seg_942" n="e" s="T145">(ku-) </ts>
               <ts e="T147" id="Seg_944" n="e" s="T146">kuʔpiʔi, </ts>
               <ts e="T148" id="Seg_946" n="e" s="T147">a </ts>
               <ts e="T149" id="Seg_948" n="e" s="T148">bostən </ts>
               <ts e="T150" id="Seg_950" n="e" s="T149">bazoʔ </ts>
               <ts e="T151" id="Seg_952" n="e" s="T150">davaj </ts>
               <ts e="T152" id="Seg_954" n="e" s="T151">amnosʼtə. </ts>
               <ts e="T153" id="Seg_956" n="e" s="T152">I </ts>
               <ts e="T154" id="Seg_958" n="e" s="T153">dĭ </ts>
               <ts e="T155" id="Seg_960" n="e" s="T154">poʔto </ts>
               <ts e="T156" id="Seg_962" n="e" s="T155">(kuzab-) </ts>
               <ts e="T157" id="Seg_964" n="e" s="T156">kuzazi </ts>
               <ts e="T158" id="Seg_966" n="e" s="T157">mobi. </ts>
               <ts e="T159" id="Seg_968" n="e" s="T158">I </ts>
               <ts e="T160" id="Seg_970" n="e" s="T159">nagurgöʔ </ts>
               <ts e="T161" id="Seg_972" n="e" s="T160">amnolaʔbəʔjə. </ts>
               <ts e="T162" id="Seg_974" n="e" s="T161">(Kabarləj). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_975" s="T0">PKZ_196X_Alenushka2_continuation_flk.001 (001)</ta>
            <ta e="T5" id="Seg_976" s="T3">PKZ_196X_Alenushka2_continuation_flk.002 (002)</ta>
            <ta e="T10" id="Seg_977" s="T5">PKZ_196X_Alenushka2_continuation_flk.003 (003)</ta>
            <ta e="T16" id="Seg_978" s="T10">PKZ_196X_Alenushka2_continuation_flk.004 (004)</ta>
            <ta e="T25" id="Seg_979" s="T16">PKZ_196X_Alenushka2_continuation_flk.005 (005)</ta>
            <ta e="T32" id="Seg_980" s="T25">PKZ_196X_Alenushka2_continuation_flk.006 (006)</ta>
            <ta e="T40" id="Seg_981" s="T32">PKZ_196X_Alenushka2_continuation_flk.007 (007)</ta>
            <ta e="T45" id="Seg_982" s="T40">PKZ_196X_Alenushka2_continuation_flk.008 (008)</ta>
            <ta e="T49" id="Seg_983" s="T45">PKZ_196X_Alenushka2_continuation_flk.009 (009)</ta>
            <ta e="T53" id="Seg_984" s="T49">PKZ_196X_Alenushka2_continuation_flk.010 (010)</ta>
            <ta e="T59" id="Seg_985" s="T53">PKZ_196X_Alenushka2_continuation_flk.011 (011)</ta>
            <ta e="T62" id="Seg_986" s="T59">PKZ_196X_Alenushka2_continuation_flk.012 (012)</ta>
            <ta e="T64" id="Seg_987" s="T62">PKZ_196X_Alenushka2_continuation_flk.013 (013)</ta>
            <ta e="T68" id="Seg_988" s="T64">PKZ_196X_Alenushka2_continuation_flk.014 (014)</ta>
            <ta e="T74" id="Seg_989" s="T68">PKZ_196X_Alenushka2_continuation_flk.015 (015)</ta>
            <ta e="T76" id="Seg_990" s="T74">PKZ_196X_Alenushka2_continuation_flk.016 (016)</ta>
            <ta e="T78" id="Seg_991" s="T76">PKZ_196X_Alenushka2_continuation_flk.017 (017)</ta>
            <ta e="T82" id="Seg_992" s="T78">PKZ_196X_Alenushka2_continuation_flk.018 (018)</ta>
            <ta e="T86" id="Seg_993" s="T82">PKZ_196X_Alenushka2_continuation_flk.019 (019)</ta>
            <ta e="T89" id="Seg_994" s="T86">PKZ_196X_Alenushka2_continuation_flk.020 (020)</ta>
            <ta e="T92" id="Seg_995" s="T89">PKZ_196X_Alenushka2_continuation_flk.021 (021)</ta>
            <ta e="T99" id="Seg_996" s="T92">PKZ_196X_Alenushka2_continuation_flk.022 (022)</ta>
            <ta e="T107" id="Seg_997" s="T99">PKZ_196X_Alenushka2_continuation_flk.023 (023)</ta>
            <ta e="T118" id="Seg_998" s="T107">PKZ_196X_Alenushka2_continuation_flk.024 (024)</ta>
            <ta e="T130" id="Seg_999" s="T118">PKZ_196X_Alenushka2_continuation_flk.025 (025)</ta>
            <ta e="T135" id="Seg_1000" s="T130">PKZ_196X_Alenushka2_continuation_flk.026 (026)</ta>
            <ta e="T139" id="Seg_1001" s="T135">PKZ_196X_Alenushka2_continuation_flk.027 (027)</ta>
            <ta e="T141" id="Seg_1002" s="T139">PKZ_196X_Alenushka2_continuation_flk.028 (028)</ta>
            <ta e="T152" id="Seg_1003" s="T141">PKZ_196X_Alenushka2_continuation_flk.029 (029)</ta>
            <ta e="T158" id="Seg_1004" s="T152">PKZ_196X_Alenushka2_continuation_flk.030 (030)</ta>
            <ta e="T161" id="Seg_1005" s="T158">PKZ_196X_Alenushka2_continuation_flk.031 (031)</ta>
            <ta e="T162" id="Seg_1006" s="T161">PKZ_196X_Alenushka2_continuation_flk.032 (032)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1007" s="T0">Dĭgəttə šobi kăldunʼnʼa. </ta>
            <ta e="T5" id="Seg_1008" s="T3">Dĭm ibi. </ta>
            <ta e="T10" id="Seg_1009" s="T5">Pibə sarbi da bünə baʔluʔpi. </ta>
            <ta e="T16" id="Seg_1010" s="T10">A bostə dĭn oldʼabə (s-) šerbi. </ta>
            <ta e="T25" id="Seg_1011" s="T16">(Dĭ kuza š-) Dĭ tibi šobi, dĭm ej tĭmnebi. </ta>
            <ta e="T32" id="Seg_1012" s="T25">Dĭ măndə:" Davaj bătleʔim (dĭ=) dĭ poʔto". </ta>
            <ta e="T40" id="Seg_1013" s="T32">A dĭ măndə: "Ĭmbi (dĭ dăre ajirbi dĭn). </ta>
            <ta e="T45" id="Seg_1014" s="T40">A tüj (băʔsittə) dĭm (xočet)". </ta>
            <ta e="T49" id="Seg_1015" s="T45">Dĭgəttə bar aspaʔi mĭnzərləʔbəʔjə. </ta>
            <ta e="T53" id="Seg_1016" s="T49">I dagaʔi (püʔmileʔbəʔjə) bar. </ta>
            <ta e="T59" id="Seg_1017" s="T53">A dĭ măndə:" Măn kalam bünə. </ta>
            <ta e="T62" id="Seg_1018" s="T59">Bĭtlem (šalaʔjə da). </ta>
            <ta e="T64" id="Seg_1019" s="T62">Noʔ (amorlam)". </ta>
            <ta e="T68" id="Seg_1020" s="T64">Kambi bar, bügən dʼorlaʔbə. </ta>
            <ta e="T74" id="Seg_1021" s="T68">(A-)" Sʼestram, sʼestram, măna dʼagarzittə xočet. </ta>
            <ta e="T76" id="Seg_1022" s="T74">Aspaʔi mĭnzəlleʔbəʔjə. </ta>
            <ta e="T78" id="Seg_1023" s="T76">Dagaʔi püʔmileʔbəʔjə". </ta>
            <ta e="T82" id="Seg_1024" s="T78">"Măna pi pʼaŋdlaʔbə dʼünə. </ta>
            <ta e="T86" id="Seg_1025" s="T82">Noʔ üjüm bar zaputalʼi. </ta>
            <ta e="T89" id="Seg_1026" s="T86">Üjüm kürbiʔi măn. </ta>
            <ta e="T92" id="Seg_1027" s="T89">Ej moliam oʔbdəsʼtə". </ta>
            <ta e="T99" id="Seg_1028" s="T92">Dĭgəttə šobi, bazoʔ mănlia: "Măn kalam bünə". </ta>
            <ta e="T107" id="Seg_1029" s="T99">Dĭgəttə kambi, bazoʔ dʼorbi, bazoʔ (dăre že) nörbəbi. </ta>
            <ta e="T118" id="Seg_1030" s="T107">Dĭgəttə dĭ tibi măndə: "(Ĭm-) Ĭmbi üge dĭ dĭbər (nu-) nuʔməleʔbə?" </ta>
            <ta e="T130" id="Seg_1031" s="T118">Dĭ bazoʔ pʼeluʔpi, (dĭ-) kambi (dĭ=) dĭ tože (dĭ- dĭ-) dĭʔnə kambi. </ta>
            <ta e="T135" id="Seg_1032" s="T130">Nünəbi bar (dĭ-) dĭm (kăštlaʔbə). </ta>
            <ta e="T139" id="Seg_1033" s="T135">Dĭgəttə (š-) iləm oʔbdəbi. </ta>
            <ta e="T141" id="Seg_1034" s="T139">Dĭm dʼaʔpiʔi. </ta>
            <ta e="T152" id="Seg_1035" s="T141">I dĭgəttə dĭ vedʼmam (ku-) kuʔpiʔi, a bostən bazoʔ davaj amnosʼtə. </ta>
            <ta e="T158" id="Seg_1036" s="T152">I dĭ poʔto (kuzab-) kuzazi mobi. </ta>
            <ta e="T161" id="Seg_1037" s="T158">I nagurgöʔ amnolaʔbəʔjə. </ta>
            <ta e="T162" id="Seg_1038" s="T161">(Kabarləj.) </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1039" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_1040" s="T1">šo-bi</ta>
            <ta e="T3" id="Seg_1041" s="T2">kăldunʼnʼa</ta>
            <ta e="T4" id="Seg_1042" s="T3">dĭ-m</ta>
            <ta e="T5" id="Seg_1043" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_1044" s="T5">pi-bə</ta>
            <ta e="T7" id="Seg_1045" s="T6">sar-bi</ta>
            <ta e="T8" id="Seg_1046" s="T7">da</ta>
            <ta e="T9" id="Seg_1047" s="T8">bü-nə</ta>
            <ta e="T10" id="Seg_1048" s="T9">baʔ-luʔ-pi</ta>
            <ta e="T11" id="Seg_1049" s="T10">a</ta>
            <ta e="T12" id="Seg_1050" s="T11">bos-tə</ta>
            <ta e="T13" id="Seg_1051" s="T12">dĭ-n</ta>
            <ta e="T14" id="Seg_1052" s="T13">oldʼa-bə</ta>
            <ta e="T16" id="Seg_1053" s="T15">šer-bi</ta>
            <ta e="T17" id="Seg_1054" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_1055" s="T17">kuza</ta>
            <ta e="T20" id="Seg_1056" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_1057" s="T20">tibi</ta>
            <ta e="T22" id="Seg_1058" s="T21">šo-bi</ta>
            <ta e="T23" id="Seg_1059" s="T22">dĭ-m</ta>
            <ta e="T24" id="Seg_1060" s="T23">ej</ta>
            <ta e="T25" id="Seg_1061" s="T24">tĭmne-bi</ta>
            <ta e="T26" id="Seg_1062" s="T25">dĭ</ta>
            <ta e="T27" id="Seg_1063" s="T26">măn-də</ta>
            <ta e="T28" id="Seg_1064" s="T27">davaj</ta>
            <ta e="T29" id="Seg_1065" s="T28">băt-le-ʔim</ta>
            <ta e="T30" id="Seg_1066" s="T29">dĭ</ta>
            <ta e="T31" id="Seg_1067" s="T30">dĭ</ta>
            <ta e="T32" id="Seg_1068" s="T31">poʔto</ta>
            <ta e="T33" id="Seg_1069" s="T32">a</ta>
            <ta e="T34" id="Seg_1070" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_1071" s="T34">măn-də</ta>
            <ta e="T36" id="Seg_1072" s="T35">ĭmbi</ta>
            <ta e="T37" id="Seg_1073" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_1074" s="T37">dăre</ta>
            <ta e="T39" id="Seg_1075" s="T38">ajir-bi</ta>
            <ta e="T40" id="Seg_1076" s="T39">dĭ-n</ta>
            <ta e="T41" id="Seg_1077" s="T40">a</ta>
            <ta e="T42" id="Seg_1078" s="T41">tüj</ta>
            <ta e="T43" id="Seg_1079" s="T42">băʔ-sittə</ta>
            <ta e="T44" id="Seg_1080" s="T43">dĭ-m</ta>
            <ta e="T45" id="Seg_1081" s="T44">xočet</ta>
            <ta e="T46" id="Seg_1082" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_1083" s="T46">bar</ta>
            <ta e="T48" id="Seg_1084" s="T47">aspa-ʔi</ta>
            <ta e="T49" id="Seg_1085" s="T48">mĭnzər-ləʔbə-ʔjə</ta>
            <ta e="T50" id="Seg_1086" s="T49">i</ta>
            <ta e="T51" id="Seg_1087" s="T50">daga-ʔi</ta>
            <ta e="T52" id="Seg_1088" s="T51">püʔmi-leʔbə-ʔjə</ta>
            <ta e="T53" id="Seg_1089" s="T52">bar</ta>
            <ta e="T54" id="Seg_1090" s="T53">a</ta>
            <ta e="T55" id="Seg_1091" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_1092" s="T55">măn-də</ta>
            <ta e="T57" id="Seg_1093" s="T56">măn</ta>
            <ta e="T58" id="Seg_1094" s="T57">ka-la-m</ta>
            <ta e="T59" id="Seg_1095" s="T58">bü-nə</ta>
            <ta e="T60" id="Seg_1096" s="T59">bĭt-le-m</ta>
            <ta e="T61" id="Seg_1097" s="T60">šalaʔjə</ta>
            <ta e="T62" id="Seg_1098" s="T61">da</ta>
            <ta e="T63" id="Seg_1099" s="T62">noʔ</ta>
            <ta e="T64" id="Seg_1100" s="T63">amor-la-m</ta>
            <ta e="T65" id="Seg_1101" s="T64">kam-bi</ta>
            <ta e="T66" id="Seg_1102" s="T65">bar</ta>
            <ta e="T67" id="Seg_1103" s="T66">bü-gən</ta>
            <ta e="T68" id="Seg_1104" s="T67">dʼor-laʔbə</ta>
            <ta e="T70" id="Seg_1105" s="T69">sʼestra-m</ta>
            <ta e="T71" id="Seg_1106" s="T70">sʼestra-m</ta>
            <ta e="T72" id="Seg_1107" s="T71">măna</ta>
            <ta e="T73" id="Seg_1108" s="T72">dʼagar-zittə</ta>
            <ta e="T74" id="Seg_1109" s="T73">xočet</ta>
            <ta e="T75" id="Seg_1110" s="T74">aspa-ʔi</ta>
            <ta e="T76" id="Seg_1111" s="T75">mĭnzəl-leʔbə-ʔjə</ta>
            <ta e="T77" id="Seg_1112" s="T76">daga-ʔi</ta>
            <ta e="T78" id="Seg_1113" s="T77">püʔmi-leʔbə-ʔjə</ta>
            <ta e="T79" id="Seg_1114" s="T78">măna</ta>
            <ta e="T80" id="Seg_1115" s="T79">pi</ta>
            <ta e="T81" id="Seg_1116" s="T80">pʼaŋd-laʔbə</ta>
            <ta e="T82" id="Seg_1117" s="T81">dʼü-nə</ta>
            <ta e="T83" id="Seg_1118" s="T82">noʔ</ta>
            <ta e="T84" id="Seg_1119" s="T83">üjü-m</ta>
            <ta e="T85" id="Seg_1120" s="T84">bar</ta>
            <ta e="T87" id="Seg_1121" s="T86">üjü-m</ta>
            <ta e="T88" id="Seg_1122" s="T87">kür-bi-ʔi</ta>
            <ta e="T89" id="Seg_1123" s="T88">măn</ta>
            <ta e="T90" id="Seg_1124" s="T89">ej</ta>
            <ta e="T91" id="Seg_1125" s="T90">mo-lia-m</ta>
            <ta e="T92" id="Seg_1126" s="T91">oʔbdə-sʼtə</ta>
            <ta e="T93" id="Seg_1127" s="T92">dĭgəttə</ta>
            <ta e="T94" id="Seg_1128" s="T93">šo-bi</ta>
            <ta e="T95" id="Seg_1129" s="T94">bazoʔ</ta>
            <ta e="T96" id="Seg_1130" s="T95">măn-lia</ta>
            <ta e="T97" id="Seg_1131" s="T96">măn</ta>
            <ta e="T98" id="Seg_1132" s="T97">ka-la-m</ta>
            <ta e="T99" id="Seg_1133" s="T98">bü-nə</ta>
            <ta e="T100" id="Seg_1134" s="T99">dĭgəttə</ta>
            <ta e="T101" id="Seg_1135" s="T100">kam-bi</ta>
            <ta e="T102" id="Seg_1136" s="T101">bazoʔ</ta>
            <ta e="T103" id="Seg_1137" s="T102">dʼor-bi</ta>
            <ta e="T104" id="Seg_1138" s="T103">bazoʔ</ta>
            <ta e="T105" id="Seg_1139" s="T104">dăre</ta>
            <ta e="T106" id="Seg_1140" s="T105">že</ta>
            <ta e="T107" id="Seg_1141" s="T106">nörbə-bi</ta>
            <ta e="T108" id="Seg_1142" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_1143" s="T108">dĭ</ta>
            <ta e="T110" id="Seg_1144" s="T109">tibi</ta>
            <ta e="T111" id="Seg_1145" s="T110">măn-də</ta>
            <ta e="T113" id="Seg_1146" s="T112">ĭmbi</ta>
            <ta e="T114" id="Seg_1147" s="T113">üge</ta>
            <ta e="T115" id="Seg_1148" s="T114">dĭ</ta>
            <ta e="T116" id="Seg_1149" s="T115">dĭbər</ta>
            <ta e="T118" id="Seg_1150" s="T117">nuʔmə-leʔbə</ta>
            <ta e="T119" id="Seg_1151" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_1152" s="T119">bazoʔ</ta>
            <ta e="T121" id="Seg_1153" s="T120">pʼe-luʔ-pi</ta>
            <ta e="T123" id="Seg_1154" s="T122">kam-bi</ta>
            <ta e="T124" id="Seg_1155" s="T123">dĭ</ta>
            <ta e="T125" id="Seg_1156" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_1157" s="T125">tože</ta>
            <ta e="T129" id="Seg_1158" s="T128">dĭʔ-nə</ta>
            <ta e="T130" id="Seg_1159" s="T129">kam-bi</ta>
            <ta e="T131" id="Seg_1160" s="T130">nünə-bi</ta>
            <ta e="T132" id="Seg_1161" s="T131">bar</ta>
            <ta e="T134" id="Seg_1162" s="T133">dĭ-m</ta>
            <ta e="T135" id="Seg_1163" s="T134">kăšt-laʔbə</ta>
            <ta e="T136" id="Seg_1164" s="T135">dĭgəttə</ta>
            <ta e="T138" id="Seg_1165" s="T137">il-ə-m</ta>
            <ta e="T139" id="Seg_1166" s="T138">oʔbdə-bi</ta>
            <ta e="T140" id="Seg_1167" s="T139">dĭ-m</ta>
            <ta e="T141" id="Seg_1168" s="T140">dʼaʔ-pi-ʔi</ta>
            <ta e="T142" id="Seg_1169" s="T141">i</ta>
            <ta e="T143" id="Seg_1170" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1171" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1172" s="T144">vedʼma-m</ta>
            <ta e="T147" id="Seg_1173" s="T146">kuʔ-pi-ʔi</ta>
            <ta e="T148" id="Seg_1174" s="T147">a</ta>
            <ta e="T149" id="Seg_1175" s="T148">bos-tən</ta>
            <ta e="T150" id="Seg_1176" s="T149">bazoʔ</ta>
            <ta e="T151" id="Seg_1177" s="T150">davaj</ta>
            <ta e="T152" id="Seg_1178" s="T151">amno-sʼtə</ta>
            <ta e="T153" id="Seg_1179" s="T152">i</ta>
            <ta e="T154" id="Seg_1180" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1181" s="T154">poʔto</ta>
            <ta e="T157" id="Seg_1182" s="T156">kuza-zi</ta>
            <ta e="T158" id="Seg_1183" s="T157">mo-bi</ta>
            <ta e="T159" id="Seg_1184" s="T158">i</ta>
            <ta e="T160" id="Seg_1185" s="T159">nagur-göʔ</ta>
            <ta e="T161" id="Seg_1186" s="T160">amno-laʔbə-ʔjə</ta>
            <ta e="T162" id="Seg_1187" s="T161">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1188" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_1189" s="T1">šo-bi</ta>
            <ta e="T3" id="Seg_1190" s="T2">kăldunʼnʼa</ta>
            <ta e="T4" id="Seg_1191" s="T3">dĭ-m</ta>
            <ta e="T5" id="Seg_1192" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_1193" s="T5">pi-bə</ta>
            <ta e="T7" id="Seg_1194" s="T6">sar-bi</ta>
            <ta e="T8" id="Seg_1195" s="T7">da</ta>
            <ta e="T9" id="Seg_1196" s="T8">bü-Tə</ta>
            <ta e="T10" id="Seg_1197" s="T9">baʔbdə-luʔbdə-bi</ta>
            <ta e="T11" id="Seg_1198" s="T10">a</ta>
            <ta e="T12" id="Seg_1199" s="T11">bos-də</ta>
            <ta e="T13" id="Seg_1200" s="T12">dĭ-n</ta>
            <ta e="T14" id="Seg_1201" s="T13">oldʼa-bə</ta>
            <ta e="T16" id="Seg_1202" s="T15">šer-bi</ta>
            <ta e="T17" id="Seg_1203" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_1204" s="T17">kuza</ta>
            <ta e="T20" id="Seg_1205" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_1206" s="T20">tibi</ta>
            <ta e="T22" id="Seg_1207" s="T21">šo-bi</ta>
            <ta e="T23" id="Seg_1208" s="T22">dĭ-m</ta>
            <ta e="T24" id="Seg_1209" s="T23">ej</ta>
            <ta e="T25" id="Seg_1210" s="T24">tĭmne-bi</ta>
            <ta e="T26" id="Seg_1211" s="T25">dĭ</ta>
            <ta e="T27" id="Seg_1212" s="T26">măn-ntə</ta>
            <ta e="T28" id="Seg_1213" s="T27">davaj</ta>
            <ta e="T29" id="Seg_1214" s="T28">băt-lV-ʔim</ta>
            <ta e="T30" id="Seg_1215" s="T29">dĭ</ta>
            <ta e="T31" id="Seg_1216" s="T30">dĭ</ta>
            <ta e="T32" id="Seg_1217" s="T31">poʔto</ta>
            <ta e="T33" id="Seg_1218" s="T32">a</ta>
            <ta e="T34" id="Seg_1219" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_1220" s="T34">măn-ntə</ta>
            <ta e="T36" id="Seg_1221" s="T35">ĭmbi</ta>
            <ta e="T37" id="Seg_1222" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_1223" s="T37">dărəʔ</ta>
            <ta e="T39" id="Seg_1224" s="T38">ajir-bi</ta>
            <ta e="T40" id="Seg_1225" s="T39">dĭ-n</ta>
            <ta e="T41" id="Seg_1226" s="T40">a</ta>
            <ta e="T42" id="Seg_1227" s="T41">tüj</ta>
            <ta e="T43" id="Seg_1228" s="T42">băt-zittə</ta>
            <ta e="T44" id="Seg_1229" s="T43">dĭ-m</ta>
            <ta e="T45" id="Seg_1230" s="T44">xočet</ta>
            <ta e="T46" id="Seg_1231" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_1232" s="T46">bar</ta>
            <ta e="T48" id="Seg_1233" s="T47">aspaʔ-jəʔ</ta>
            <ta e="T49" id="Seg_1234" s="T48">mĭnzər-laʔbə-jəʔ</ta>
            <ta e="T50" id="Seg_1235" s="T49">i</ta>
            <ta e="T51" id="Seg_1236" s="T50">tagaj-jəʔ</ta>
            <ta e="T52" id="Seg_1237" s="T51">puʔmə-laʔbə-jəʔ</ta>
            <ta e="T53" id="Seg_1238" s="T52">bar</ta>
            <ta e="T54" id="Seg_1239" s="T53">a</ta>
            <ta e="T55" id="Seg_1240" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_1241" s="T55">măn-ntə</ta>
            <ta e="T57" id="Seg_1242" s="T56">măn</ta>
            <ta e="T58" id="Seg_1243" s="T57">kan-lV-m</ta>
            <ta e="T59" id="Seg_1244" s="T58">bü-Tə</ta>
            <ta e="T60" id="Seg_1245" s="T59">bĭs-lV-m</ta>
            <ta e="T61" id="Seg_1246" s="T60">šala</ta>
            <ta e="T62" id="Seg_1247" s="T61">da</ta>
            <ta e="T63" id="Seg_1248" s="T62">noʔ</ta>
            <ta e="T64" id="Seg_1249" s="T63">amor-lV-m</ta>
            <ta e="T65" id="Seg_1250" s="T64">kan-bi</ta>
            <ta e="T66" id="Seg_1251" s="T65">bar</ta>
            <ta e="T67" id="Seg_1252" s="T66">bü-Kən</ta>
            <ta e="T68" id="Seg_1253" s="T67">tʼor-laʔbə</ta>
            <ta e="T70" id="Seg_1254" s="T69">sʼestra-m</ta>
            <ta e="T71" id="Seg_1255" s="T70">sʼestra-m</ta>
            <ta e="T72" id="Seg_1256" s="T71">măna</ta>
            <ta e="T73" id="Seg_1257" s="T72">dʼagar-zittə</ta>
            <ta e="T74" id="Seg_1258" s="T73">xočet</ta>
            <ta e="T75" id="Seg_1259" s="T74">aspaʔ-jəʔ</ta>
            <ta e="T76" id="Seg_1260" s="T75">mĭnzəl-laʔbə-jəʔ</ta>
            <ta e="T77" id="Seg_1261" s="T76">tagaj-jəʔ</ta>
            <ta e="T78" id="Seg_1262" s="T77">puʔmə-laʔbə-jəʔ</ta>
            <ta e="T79" id="Seg_1263" s="T78">măna</ta>
            <ta e="T80" id="Seg_1264" s="T79">pi</ta>
            <ta e="T81" id="Seg_1265" s="T80">pʼaŋdə-laʔbə</ta>
            <ta e="T82" id="Seg_1266" s="T81">tʼo-Tə</ta>
            <ta e="T83" id="Seg_1267" s="T82">noʔ</ta>
            <ta e="T84" id="Seg_1268" s="T83">üjü-m</ta>
            <ta e="T85" id="Seg_1269" s="T84">bar</ta>
            <ta e="T87" id="Seg_1270" s="T86">üjü-m</ta>
            <ta e="T88" id="Seg_1271" s="T87">kür-bi-jəʔ</ta>
            <ta e="T89" id="Seg_1272" s="T88">măn</ta>
            <ta e="T90" id="Seg_1273" s="T89">ej</ta>
            <ta e="T91" id="Seg_1274" s="T90">mo-liA-m</ta>
            <ta e="T92" id="Seg_1275" s="T91">oʔbdə-zittə</ta>
            <ta e="T93" id="Seg_1276" s="T92">dĭgəttə</ta>
            <ta e="T94" id="Seg_1277" s="T93">šo-bi</ta>
            <ta e="T95" id="Seg_1278" s="T94">bazoʔ</ta>
            <ta e="T96" id="Seg_1279" s="T95">măn-liA</ta>
            <ta e="T97" id="Seg_1280" s="T96">măn</ta>
            <ta e="T98" id="Seg_1281" s="T97">kan-lV-m</ta>
            <ta e="T99" id="Seg_1282" s="T98">bü-Tə</ta>
            <ta e="T100" id="Seg_1283" s="T99">dĭgəttə</ta>
            <ta e="T101" id="Seg_1284" s="T100">kan-bi</ta>
            <ta e="T102" id="Seg_1285" s="T101">bazoʔ</ta>
            <ta e="T103" id="Seg_1286" s="T102">tʼor-bi</ta>
            <ta e="T104" id="Seg_1287" s="T103">bazoʔ</ta>
            <ta e="T105" id="Seg_1288" s="T104">dărəʔ</ta>
            <ta e="T106" id="Seg_1289" s="T105">že</ta>
            <ta e="T107" id="Seg_1290" s="T106">nörbə-bi</ta>
            <ta e="T108" id="Seg_1291" s="T107">dĭgəttə</ta>
            <ta e="T109" id="Seg_1292" s="T108">dĭ</ta>
            <ta e="T110" id="Seg_1293" s="T109">tibi</ta>
            <ta e="T111" id="Seg_1294" s="T110">măn-ntə</ta>
            <ta e="T113" id="Seg_1295" s="T112">ĭmbi</ta>
            <ta e="T114" id="Seg_1296" s="T113">üge</ta>
            <ta e="T115" id="Seg_1297" s="T114">dĭ</ta>
            <ta e="T116" id="Seg_1298" s="T115">dĭbər</ta>
            <ta e="T118" id="Seg_1299" s="T117">nuʔmə-laʔbə</ta>
            <ta e="T119" id="Seg_1300" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_1301" s="T119">bazoʔ</ta>
            <ta e="T121" id="Seg_1302" s="T120">pi-luʔbdə-bi</ta>
            <ta e="T123" id="Seg_1303" s="T122">kan-bi</ta>
            <ta e="T124" id="Seg_1304" s="T123">dĭ</ta>
            <ta e="T125" id="Seg_1305" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_1306" s="T125">tože</ta>
            <ta e="T129" id="Seg_1307" s="T128">dĭ-Tə</ta>
            <ta e="T130" id="Seg_1308" s="T129">kan-bi</ta>
            <ta e="T131" id="Seg_1309" s="T130">nünə-bi</ta>
            <ta e="T132" id="Seg_1310" s="T131">bar</ta>
            <ta e="T134" id="Seg_1311" s="T133">dĭ-m</ta>
            <ta e="T135" id="Seg_1312" s="T134">kăštə-laʔbə</ta>
            <ta e="T136" id="Seg_1313" s="T135">dĭgəttə</ta>
            <ta e="T138" id="Seg_1314" s="T137">il-ə-m</ta>
            <ta e="T139" id="Seg_1315" s="T138">oʔbdə-bi</ta>
            <ta e="T140" id="Seg_1316" s="T139">dĭ-m</ta>
            <ta e="T141" id="Seg_1317" s="T140">dʼabə-bi-jəʔ</ta>
            <ta e="T142" id="Seg_1318" s="T141">i</ta>
            <ta e="T143" id="Seg_1319" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1320" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1321" s="T144">vedʼma-m</ta>
            <ta e="T147" id="Seg_1322" s="T146">kut-bi-jəʔ</ta>
            <ta e="T148" id="Seg_1323" s="T147">a</ta>
            <ta e="T149" id="Seg_1324" s="T148">bos-tən</ta>
            <ta e="T150" id="Seg_1325" s="T149">bazoʔ</ta>
            <ta e="T151" id="Seg_1326" s="T150">davaj</ta>
            <ta e="T152" id="Seg_1327" s="T151">amno-zittə</ta>
            <ta e="T153" id="Seg_1328" s="T152">i</ta>
            <ta e="T154" id="Seg_1329" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1330" s="T154">poʔto</ta>
            <ta e="T157" id="Seg_1331" s="T156">kuza-ziʔ</ta>
            <ta e="T158" id="Seg_1332" s="T157">mo-bi</ta>
            <ta e="T159" id="Seg_1333" s="T158">i</ta>
            <ta e="T160" id="Seg_1334" s="T159">nagur-göʔ</ta>
            <ta e="T161" id="Seg_1335" s="T160">amno-laʔbə-jəʔ</ta>
            <ta e="T162" id="Seg_1336" s="T161">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1337" s="T0">then</ta>
            <ta e="T2" id="Seg_1338" s="T1">come-PST.[3SG]</ta>
            <ta e="T3" id="Seg_1339" s="T2">witch.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1340" s="T3">this-ACC</ta>
            <ta e="T5" id="Seg_1341" s="T4">take-PST.[3SG]</ta>
            <ta e="T6" id="Seg_1342" s="T5">stone-ACC.3SG</ta>
            <ta e="T7" id="Seg_1343" s="T6">bind-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1344" s="T7">and</ta>
            <ta e="T9" id="Seg_1345" s="T8">water-LAT</ta>
            <ta e="T10" id="Seg_1346" s="T9">throw-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1347" s="T10">and</ta>
            <ta e="T12" id="Seg_1348" s="T11">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T13" id="Seg_1349" s="T12">this-GEN</ta>
            <ta e="T14" id="Seg_1350" s="T13">clothing-ACC.3SG</ta>
            <ta e="T16" id="Seg_1351" s="T15">dress-PST.[3SG]</ta>
            <ta e="T17" id="Seg_1352" s="T16">this</ta>
            <ta e="T18" id="Seg_1353" s="T17">man.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1354" s="T19">this</ta>
            <ta e="T21" id="Seg_1355" s="T20">man.[NOM.SG]</ta>
            <ta e="T22" id="Seg_1356" s="T21">come-PST.[3SG]</ta>
            <ta e="T23" id="Seg_1357" s="T22">this-ACC</ta>
            <ta e="T24" id="Seg_1358" s="T23">NEG</ta>
            <ta e="T25" id="Seg_1359" s="T24">know-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1360" s="T25">this.[3SG]</ta>
            <ta e="T27" id="Seg_1361" s="T26">say-IPFVZ.[3SG]</ta>
            <ta e="T28" id="Seg_1362" s="T27">HORT</ta>
            <ta e="T29" id="Seg_1363" s="T28">cut-FUT-%IMP.1PL</ta>
            <ta e="T30" id="Seg_1364" s="T29">this.[NOM.SG]</ta>
            <ta e="T31" id="Seg_1365" s="T30">this.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1366" s="T31">goat.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1367" s="T32">and</ta>
            <ta e="T34" id="Seg_1368" s="T33">this.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1369" s="T34">say-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_1370" s="T35">what.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1371" s="T36">this.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1372" s="T37">so</ta>
            <ta e="T39" id="Seg_1373" s="T38">pity-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1374" s="T39">this-GEN</ta>
            <ta e="T41" id="Seg_1375" s="T40">and</ta>
            <ta e="T42" id="Seg_1376" s="T41">now</ta>
            <ta e="T43" id="Seg_1377" s="T42">cut-INF.LAT</ta>
            <ta e="T44" id="Seg_1378" s="T43">this-ACC</ta>
            <ta e="T45" id="Seg_1379" s="T44">want.3SG</ta>
            <ta e="T46" id="Seg_1380" s="T45">then</ta>
            <ta e="T47" id="Seg_1381" s="T46">PTCL</ta>
            <ta e="T48" id="Seg_1382" s="T47">cauldron-PL</ta>
            <ta e="T49" id="Seg_1383" s="T48">boil-DUR-3PL</ta>
            <ta e="T50" id="Seg_1384" s="T49">and</ta>
            <ta e="T51" id="Seg_1385" s="T50">knife-PL</ta>
            <ta e="T52" id="Seg_1386" s="T51">sharpen-DUR-3PL</ta>
            <ta e="T53" id="Seg_1387" s="T52">PTCL</ta>
            <ta e="T54" id="Seg_1388" s="T53">and</ta>
            <ta e="T55" id="Seg_1389" s="T54">this.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1390" s="T55">say-IPFVZ.[3SG]</ta>
            <ta e="T57" id="Seg_1391" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_1392" s="T57">go-FUT-1SG</ta>
            <ta e="T59" id="Seg_1393" s="T58">water-LAT</ta>
            <ta e="T60" id="Seg_1394" s="T59">drink-FUT-1SG</ta>
            <ta e="T61" id="Seg_1395" s="T60">%%</ta>
            <ta e="T62" id="Seg_1396" s="T61">and</ta>
            <ta e="T63" id="Seg_1397" s="T62">grass.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1398" s="T63">eat-FUT-1SG</ta>
            <ta e="T65" id="Seg_1399" s="T64">go-PST.[3SG]</ta>
            <ta e="T66" id="Seg_1400" s="T65">PTCL</ta>
            <ta e="T67" id="Seg_1401" s="T66">water-LOC</ta>
            <ta e="T68" id="Seg_1402" s="T67">cry-DUR.[3SG]</ta>
            <ta e="T70" id="Seg_1403" s="T69">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T71" id="Seg_1404" s="T70">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T72" id="Seg_1405" s="T71">I.ACC</ta>
            <ta e="T73" id="Seg_1406" s="T72">stab-INF.LAT</ta>
            <ta e="T74" id="Seg_1407" s="T73">want.3SG</ta>
            <ta e="T75" id="Seg_1408" s="T74">cauldron-PL</ta>
            <ta e="T76" id="Seg_1409" s="T75">boil-DUR-3PL</ta>
            <ta e="T77" id="Seg_1410" s="T76">knife-PL</ta>
            <ta e="T78" id="Seg_1411" s="T77">sharpen-DUR-3PL</ta>
            <ta e="T79" id="Seg_1412" s="T78">I.LAT</ta>
            <ta e="T80" id="Seg_1413" s="T79">stone.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1414" s="T80">press-DUR.[3SG]</ta>
            <ta e="T82" id="Seg_1415" s="T81">earth-LAT</ta>
            <ta e="T83" id="Seg_1416" s="T82">grass.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1417" s="T83">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T85" id="Seg_1418" s="T84">PTCL</ta>
            <ta e="T87" id="Seg_1419" s="T86">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T88" id="Seg_1420" s="T87">plait-PST-3PL</ta>
            <ta e="T89" id="Seg_1421" s="T88">I.NOM</ta>
            <ta e="T90" id="Seg_1422" s="T89">NEG</ta>
            <ta e="T91" id="Seg_1423" s="T90">can-PRS-1SG</ta>
            <ta e="T92" id="Seg_1424" s="T91">collect-INF.LAT</ta>
            <ta e="T93" id="Seg_1425" s="T92">then</ta>
            <ta e="T94" id="Seg_1426" s="T93">come-PST.[3SG]</ta>
            <ta e="T95" id="Seg_1427" s="T94">again</ta>
            <ta e="T96" id="Seg_1428" s="T95">say-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1429" s="T96">I.NOM</ta>
            <ta e="T98" id="Seg_1430" s="T97">go-FUT-1SG</ta>
            <ta e="T99" id="Seg_1431" s="T98">water-LAT</ta>
            <ta e="T100" id="Seg_1432" s="T99">then</ta>
            <ta e="T101" id="Seg_1433" s="T100">go-PST.[3SG]</ta>
            <ta e="T102" id="Seg_1434" s="T101">again</ta>
            <ta e="T103" id="Seg_1435" s="T102">cry-PST.[3SG]</ta>
            <ta e="T104" id="Seg_1436" s="T103">again</ta>
            <ta e="T105" id="Seg_1437" s="T104">so</ta>
            <ta e="T106" id="Seg_1438" s="T105">PTCL</ta>
            <ta e="T107" id="Seg_1439" s="T106">tell-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1440" s="T107">then</ta>
            <ta e="T109" id="Seg_1441" s="T108">this.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1442" s="T109">man.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1443" s="T110">say-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_1444" s="T112">what.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1445" s="T113">always</ta>
            <ta e="T115" id="Seg_1446" s="T114">this.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1447" s="T115">there</ta>
            <ta e="T118" id="Seg_1448" s="T117">run-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1449" s="T118">this</ta>
            <ta e="T120" id="Seg_1450" s="T119">again</ta>
            <ta e="T121" id="Seg_1451" s="T120">ask-MOM-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1452" s="T122">go-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1453" s="T123">this.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1454" s="T124">this.[NOM.SG]</ta>
            <ta e="T126" id="Seg_1455" s="T125">also</ta>
            <ta e="T129" id="Seg_1456" s="T128">this-LAT</ta>
            <ta e="T130" id="Seg_1457" s="T129">go-PST.[3SG]</ta>
            <ta e="T131" id="Seg_1458" s="T130">hear-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1459" s="T131">PTCL</ta>
            <ta e="T134" id="Seg_1460" s="T133">this-ACC</ta>
            <ta e="T135" id="Seg_1461" s="T134">call-DUR.[3SG]</ta>
            <ta e="T136" id="Seg_1462" s="T135">then</ta>
            <ta e="T138" id="Seg_1463" s="T137">people-EP-ACC</ta>
            <ta e="T139" id="Seg_1464" s="T138">collect-PST.[3SG]</ta>
            <ta e="T140" id="Seg_1465" s="T139">this-ACC</ta>
            <ta e="T141" id="Seg_1466" s="T140">capture-PST-3PL</ta>
            <ta e="T142" id="Seg_1467" s="T141">and</ta>
            <ta e="T143" id="Seg_1468" s="T142">then</ta>
            <ta e="T144" id="Seg_1469" s="T143">this.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1470" s="T144">witch-ACC</ta>
            <ta e="T147" id="Seg_1471" s="T146">kill-PST-3PL</ta>
            <ta e="T148" id="Seg_1472" s="T147">and</ta>
            <ta e="T149" id="Seg_1473" s="T148">self-NOM/GEN/ACC.3PL</ta>
            <ta e="T150" id="Seg_1474" s="T149">again</ta>
            <ta e="T151" id="Seg_1475" s="T150">INCH</ta>
            <ta e="T152" id="Seg_1476" s="T151">live-INF.LAT</ta>
            <ta e="T153" id="Seg_1477" s="T152">and</ta>
            <ta e="T154" id="Seg_1478" s="T153">this.[NOM.SG]</ta>
            <ta e="T155" id="Seg_1479" s="T154">goat.[NOM.SG]</ta>
            <ta e="T157" id="Seg_1480" s="T156">human-INS</ta>
            <ta e="T158" id="Seg_1481" s="T157">become-PST.[3SG]</ta>
            <ta e="T159" id="Seg_1482" s="T158">and</ta>
            <ta e="T160" id="Seg_1483" s="T159">three-COLL</ta>
            <ta e="T161" id="Seg_1484" s="T160">live-DUR-3PL</ta>
            <ta e="T162" id="Seg_1485" s="T161">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1486" s="T0">тогда</ta>
            <ta e="T2" id="Seg_1487" s="T1">прийти-PST.[3SG]</ta>
            <ta e="T3" id="Seg_1488" s="T2">колдунья.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1489" s="T3">этот-ACC</ta>
            <ta e="T5" id="Seg_1490" s="T4">взять-PST.[3SG]</ta>
            <ta e="T6" id="Seg_1491" s="T5">камень-ACC.3SG</ta>
            <ta e="T7" id="Seg_1492" s="T6">завязать-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1493" s="T7">и</ta>
            <ta e="T9" id="Seg_1494" s="T8">вода-LAT</ta>
            <ta e="T10" id="Seg_1495" s="T9">бросить-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1496" s="T10">а</ta>
            <ta e="T12" id="Seg_1497" s="T11">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T13" id="Seg_1498" s="T12">этот-GEN</ta>
            <ta e="T14" id="Seg_1499" s="T13">одежда-ACC.3SG</ta>
            <ta e="T16" id="Seg_1500" s="T15">надеть-PST.[3SG]</ta>
            <ta e="T17" id="Seg_1501" s="T16">этот</ta>
            <ta e="T18" id="Seg_1502" s="T17">мужчина.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1503" s="T19">этот</ta>
            <ta e="T21" id="Seg_1504" s="T20">мужчина.[NOM.SG]</ta>
            <ta e="T22" id="Seg_1505" s="T21">прийти-PST.[3SG]</ta>
            <ta e="T23" id="Seg_1506" s="T22">этот-ACC</ta>
            <ta e="T24" id="Seg_1507" s="T23">NEG</ta>
            <ta e="T25" id="Seg_1508" s="T24">знать-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1509" s="T25">этот.[3SG]</ta>
            <ta e="T27" id="Seg_1510" s="T26">сказать-IPFVZ.[3SG]</ta>
            <ta e="T28" id="Seg_1511" s="T27">HORT</ta>
            <ta e="T29" id="Seg_1512" s="T28">резать-FUT-%IMP.1PL</ta>
            <ta e="T30" id="Seg_1513" s="T29">этот.[NOM.SG]</ta>
            <ta e="T31" id="Seg_1514" s="T30">этот.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1515" s="T31">коза.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1516" s="T32">а</ta>
            <ta e="T34" id="Seg_1517" s="T33">этот.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1518" s="T34">сказать-IPFVZ.[3SG]</ta>
            <ta e="T36" id="Seg_1519" s="T35">что.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1520" s="T36">этот.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1521" s="T37">так</ta>
            <ta e="T39" id="Seg_1522" s="T38">жалеть-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1523" s="T39">этот-GEN</ta>
            <ta e="T41" id="Seg_1524" s="T40">а</ta>
            <ta e="T42" id="Seg_1525" s="T41">сейчас</ta>
            <ta e="T43" id="Seg_1526" s="T42">резать-INF.LAT</ta>
            <ta e="T44" id="Seg_1527" s="T43">этот-ACC</ta>
            <ta e="T45" id="Seg_1528" s="T44">хотеть.3SG</ta>
            <ta e="T46" id="Seg_1529" s="T45">тогда</ta>
            <ta e="T47" id="Seg_1530" s="T46">PTCL</ta>
            <ta e="T48" id="Seg_1531" s="T47">котел-PL</ta>
            <ta e="T49" id="Seg_1532" s="T48">кипятить-DUR-3PL</ta>
            <ta e="T50" id="Seg_1533" s="T49">и</ta>
            <ta e="T51" id="Seg_1534" s="T50">нож-PL</ta>
            <ta e="T52" id="Seg_1535" s="T51">точить-DUR-3PL</ta>
            <ta e="T53" id="Seg_1536" s="T52">PTCL</ta>
            <ta e="T54" id="Seg_1537" s="T53">а</ta>
            <ta e="T55" id="Seg_1538" s="T54">этот.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1539" s="T55">сказать-IPFVZ.[3SG]</ta>
            <ta e="T57" id="Seg_1540" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_1541" s="T57">пойти-FUT-1SG</ta>
            <ta e="T59" id="Seg_1542" s="T58">вода-LAT</ta>
            <ta e="T60" id="Seg_1543" s="T59">пить-FUT-1SG</ta>
            <ta e="T61" id="Seg_1544" s="T60">%%</ta>
            <ta e="T62" id="Seg_1545" s="T61">и</ta>
            <ta e="T63" id="Seg_1546" s="T62">трава.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1547" s="T63">есть-FUT-1SG</ta>
            <ta e="T65" id="Seg_1548" s="T64">пойти-PST.[3SG]</ta>
            <ta e="T66" id="Seg_1549" s="T65">PTCL</ta>
            <ta e="T67" id="Seg_1550" s="T66">вода-LOC</ta>
            <ta e="T68" id="Seg_1551" s="T67">плакать-DUR.[3SG]</ta>
            <ta e="T70" id="Seg_1552" s="T69">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T71" id="Seg_1553" s="T70">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T72" id="Seg_1554" s="T71">я.ACC</ta>
            <ta e="T73" id="Seg_1555" s="T72">зарезать-INF.LAT</ta>
            <ta e="T74" id="Seg_1556" s="T73">хотеть.3SG</ta>
            <ta e="T75" id="Seg_1557" s="T74">котел-PL</ta>
            <ta e="T76" id="Seg_1558" s="T75">кипеть-DUR-3PL</ta>
            <ta e="T77" id="Seg_1559" s="T76">нож-PL</ta>
            <ta e="T78" id="Seg_1560" s="T77">точить-DUR-3PL</ta>
            <ta e="T79" id="Seg_1561" s="T78">я.LAT</ta>
            <ta e="T80" id="Seg_1562" s="T79">камень.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1563" s="T80">нажимать-DUR.[3SG]</ta>
            <ta e="T82" id="Seg_1564" s="T81">земля-LAT</ta>
            <ta e="T83" id="Seg_1565" s="T82">трава.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1566" s="T83">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T85" id="Seg_1567" s="T84">PTCL</ta>
            <ta e="T87" id="Seg_1568" s="T86">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T88" id="Seg_1569" s="T87">плести-PST-3PL</ta>
            <ta e="T89" id="Seg_1570" s="T88">я.NOM</ta>
            <ta e="T90" id="Seg_1571" s="T89">NEG</ta>
            <ta e="T91" id="Seg_1572" s="T90">мочь-PRS-1SG</ta>
            <ta e="T92" id="Seg_1573" s="T91">собирать-INF.LAT</ta>
            <ta e="T93" id="Seg_1574" s="T92">тогда</ta>
            <ta e="T94" id="Seg_1575" s="T93">прийти-PST.[3SG]</ta>
            <ta e="T95" id="Seg_1576" s="T94">опять</ta>
            <ta e="T96" id="Seg_1577" s="T95">сказать-PRS.[3SG]</ta>
            <ta e="T97" id="Seg_1578" s="T96">я.NOM</ta>
            <ta e="T98" id="Seg_1579" s="T97">пойти-FUT-1SG</ta>
            <ta e="T99" id="Seg_1580" s="T98">вода-LAT</ta>
            <ta e="T100" id="Seg_1581" s="T99">тогда</ta>
            <ta e="T101" id="Seg_1582" s="T100">пойти-PST.[3SG]</ta>
            <ta e="T102" id="Seg_1583" s="T101">опять</ta>
            <ta e="T103" id="Seg_1584" s="T102">плакать-PST.[3SG]</ta>
            <ta e="T104" id="Seg_1585" s="T103">опять</ta>
            <ta e="T105" id="Seg_1586" s="T104">так</ta>
            <ta e="T106" id="Seg_1587" s="T105">же</ta>
            <ta e="T107" id="Seg_1588" s="T106">сказать-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1589" s="T107">тогда</ta>
            <ta e="T109" id="Seg_1590" s="T108">этот.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1591" s="T109">мужчина.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1592" s="T110">сказать-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_1593" s="T112">что.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1594" s="T113">всегда</ta>
            <ta e="T115" id="Seg_1595" s="T114">этот.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1596" s="T115">там</ta>
            <ta e="T118" id="Seg_1597" s="T117">бежать-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1598" s="T118">этот</ta>
            <ta e="T120" id="Seg_1599" s="T119">опять</ta>
            <ta e="T121" id="Seg_1600" s="T120">спросить-MOM-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1601" s="T122">пойти-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1602" s="T123">этот.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1603" s="T124">этот.[NOM.SG]</ta>
            <ta e="T126" id="Seg_1604" s="T125">тоже</ta>
            <ta e="T129" id="Seg_1605" s="T128">этот-LAT</ta>
            <ta e="T130" id="Seg_1606" s="T129">пойти-PST.[3SG]</ta>
            <ta e="T131" id="Seg_1607" s="T130">слышать-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1608" s="T131">PTCL</ta>
            <ta e="T134" id="Seg_1609" s="T133">этот-ACC</ta>
            <ta e="T135" id="Seg_1610" s="T134">позвать-DUR.[3SG]</ta>
            <ta e="T136" id="Seg_1611" s="T135">тогда</ta>
            <ta e="T138" id="Seg_1612" s="T137">люди-EP-ACC</ta>
            <ta e="T139" id="Seg_1613" s="T138">собирать-PST.[3SG]</ta>
            <ta e="T140" id="Seg_1614" s="T139">этот-ACC</ta>
            <ta e="T141" id="Seg_1615" s="T140">ловить-PST-3PL</ta>
            <ta e="T142" id="Seg_1616" s="T141">и</ta>
            <ta e="T143" id="Seg_1617" s="T142">тогда</ta>
            <ta e="T144" id="Seg_1618" s="T143">этот.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1619" s="T144">ведьма-ACC</ta>
            <ta e="T147" id="Seg_1620" s="T146">убить-PST-3PL</ta>
            <ta e="T148" id="Seg_1621" s="T147">а</ta>
            <ta e="T149" id="Seg_1622" s="T148">сам-NOM/GEN/ACC.3PL</ta>
            <ta e="T150" id="Seg_1623" s="T149">опять</ta>
            <ta e="T151" id="Seg_1624" s="T150">INCH</ta>
            <ta e="T152" id="Seg_1625" s="T151">жить-INF.LAT</ta>
            <ta e="T153" id="Seg_1626" s="T152">и</ta>
            <ta e="T154" id="Seg_1627" s="T153">этот.[NOM.SG]</ta>
            <ta e="T155" id="Seg_1628" s="T154">коза.[NOM.SG]</ta>
            <ta e="T157" id="Seg_1629" s="T156">человек-INS</ta>
            <ta e="T158" id="Seg_1630" s="T157">стать-PST.[3SG]</ta>
            <ta e="T159" id="Seg_1631" s="T158">и</ta>
            <ta e="T160" id="Seg_1632" s="T159">три-COLL</ta>
            <ta e="T161" id="Seg_1633" s="T160">жить-DUR-3PL</ta>
            <ta e="T162" id="Seg_1634" s="T161">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1635" s="T0">adv</ta>
            <ta e="T2" id="Seg_1636" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1637" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1638" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_1639" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_1640" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_1641" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1642" s="T7">conj</ta>
            <ta e="T9" id="Seg_1643" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1644" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1645" s="T10">conj</ta>
            <ta e="T12" id="Seg_1646" s="T11">refl-n:case.poss</ta>
            <ta e="T13" id="Seg_1647" s="T12">dempro-n:case</ta>
            <ta e="T14" id="Seg_1648" s="T13">n-n:case.poss</ta>
            <ta e="T16" id="Seg_1649" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1650" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1651" s="T17">n-n:case</ta>
            <ta e="T20" id="Seg_1652" s="T19">dempro</ta>
            <ta e="T21" id="Seg_1653" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1654" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1655" s="T22">dempro-n:case</ta>
            <ta e="T24" id="Seg_1656" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1657" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1658" s="T25">dempro-v:pn</ta>
            <ta e="T27" id="Seg_1659" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_1660" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1661" s="T28">v-v:tense-v:mood.pn</ta>
            <ta e="T30" id="Seg_1662" s="T29">dempro-n:case</ta>
            <ta e="T31" id="Seg_1663" s="T30">dempro-n:case</ta>
            <ta e="T32" id="Seg_1664" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1665" s="T32">conj</ta>
            <ta e="T34" id="Seg_1666" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_1667" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1668" s="T35">que-n:case</ta>
            <ta e="T37" id="Seg_1669" s="T36">dempro-n:case</ta>
            <ta e="T38" id="Seg_1670" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1671" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1672" s="T39">dempro-n:case</ta>
            <ta e="T41" id="Seg_1673" s="T40">conj</ta>
            <ta e="T42" id="Seg_1674" s="T41">adv</ta>
            <ta e="T43" id="Seg_1675" s="T42">v-v:n.fin</ta>
            <ta e="T44" id="Seg_1676" s="T43">dempro-n:case</ta>
            <ta e="T45" id="Seg_1677" s="T44">v</ta>
            <ta e="T46" id="Seg_1678" s="T45">adv</ta>
            <ta e="T47" id="Seg_1679" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1680" s="T47">n-n:num</ta>
            <ta e="T49" id="Seg_1681" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_1682" s="T49">conj</ta>
            <ta e="T51" id="Seg_1683" s="T50">n-n:num</ta>
            <ta e="T52" id="Seg_1684" s="T51">v-v&gt;v-v:pn</ta>
            <ta e="T53" id="Seg_1685" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1686" s="T53">conj</ta>
            <ta e="T55" id="Seg_1687" s="T54">dempro-n:case</ta>
            <ta e="T56" id="Seg_1688" s="T55">v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_1689" s="T56">pers</ta>
            <ta e="T58" id="Seg_1690" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1691" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1692" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1693" s="T60">%%</ta>
            <ta e="T62" id="Seg_1694" s="T61">conj</ta>
            <ta e="T63" id="Seg_1695" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1696" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1697" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1698" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1699" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1700" s="T67">v-v&gt;v-v:pn</ta>
            <ta e="T70" id="Seg_1701" s="T69">n-n:case.poss</ta>
            <ta e="T71" id="Seg_1702" s="T70">n-n:case.poss</ta>
            <ta e="T72" id="Seg_1703" s="T71">pers</ta>
            <ta e="T73" id="Seg_1704" s="T72">v-v:n.fin</ta>
            <ta e="T74" id="Seg_1705" s="T73">v</ta>
            <ta e="T75" id="Seg_1706" s="T74">n-n:num</ta>
            <ta e="T76" id="Seg_1707" s="T75">v-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_1708" s="T76">n-n:num</ta>
            <ta e="T78" id="Seg_1709" s="T77">v-v&gt;v-v:pn</ta>
            <ta e="T79" id="Seg_1710" s="T78">pers</ta>
            <ta e="T80" id="Seg_1711" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1712" s="T80">v-v&gt;v-v:pn</ta>
            <ta e="T82" id="Seg_1713" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1714" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1715" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_1716" s="T84">ptcl</ta>
            <ta e="T87" id="Seg_1717" s="T86">n-n:case.poss</ta>
            <ta e="T88" id="Seg_1718" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1719" s="T88">pers</ta>
            <ta e="T90" id="Seg_1720" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_1721" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1722" s="T91">v-v:n.fin</ta>
            <ta e="T93" id="Seg_1723" s="T92">adv</ta>
            <ta e="T94" id="Seg_1724" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_1725" s="T94">adv</ta>
            <ta e="T96" id="Seg_1726" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_1727" s="T96">pers</ta>
            <ta e="T98" id="Seg_1728" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1729" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_1730" s="T99">adv</ta>
            <ta e="T101" id="Seg_1731" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1732" s="T101">adv</ta>
            <ta e="T103" id="Seg_1733" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1734" s="T103">adv</ta>
            <ta e="T105" id="Seg_1735" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_1736" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1737" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1738" s="T107">adv</ta>
            <ta e="T109" id="Seg_1739" s="T108">dempro-n:case</ta>
            <ta e="T110" id="Seg_1740" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1741" s="T110">v-v&gt;v-v:pn</ta>
            <ta e="T113" id="Seg_1742" s="T112">que-n:case</ta>
            <ta e="T114" id="Seg_1743" s="T113">adv</ta>
            <ta e="T115" id="Seg_1744" s="T114">dempro-n:case</ta>
            <ta e="T116" id="Seg_1745" s="T115">adv</ta>
            <ta e="T118" id="Seg_1746" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_1747" s="T118">dempro</ta>
            <ta e="T120" id="Seg_1748" s="T119">adv</ta>
            <ta e="T121" id="Seg_1749" s="T120">v-v&gt;v-v:tense</ta>
            <ta e="T123" id="Seg_1750" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_1751" s="T123">dempro-n:case</ta>
            <ta e="T125" id="Seg_1752" s="T124">dempro-n:case</ta>
            <ta e="T126" id="Seg_1753" s="T125">ptcl</ta>
            <ta e="T129" id="Seg_1754" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_1755" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_1756" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1757" s="T131">ptcl</ta>
            <ta e="T134" id="Seg_1758" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_1759" s="T134">v-v&gt;v-v:pn</ta>
            <ta e="T136" id="Seg_1760" s="T135">adv</ta>
            <ta e="T138" id="Seg_1761" s="T137">n-n:ins-n:case</ta>
            <ta e="T139" id="Seg_1762" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_1763" s="T139">dempro-n:case</ta>
            <ta e="T141" id="Seg_1764" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_1765" s="T141">conj</ta>
            <ta e="T143" id="Seg_1766" s="T142">adv</ta>
            <ta e="T144" id="Seg_1767" s="T143">dempro-n:case</ta>
            <ta e="T145" id="Seg_1768" s="T144">n-n:case</ta>
            <ta e="T147" id="Seg_1769" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_1770" s="T147">conj</ta>
            <ta e="T149" id="Seg_1771" s="T148">refl-n:case:poss</ta>
            <ta e="T150" id="Seg_1772" s="T149">adv</ta>
            <ta e="T151" id="Seg_1773" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_1774" s="T151">v-v:n.fin</ta>
            <ta e="T153" id="Seg_1775" s="T152">conj</ta>
            <ta e="T154" id="Seg_1776" s="T153">dempro-n:case</ta>
            <ta e="T155" id="Seg_1777" s="T154">n-n:case</ta>
            <ta e="T157" id="Seg_1778" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_1779" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_1780" s="T158">conj</ta>
            <ta e="T160" id="Seg_1781" s="T159">num-num&gt;num</ta>
            <ta e="T161" id="Seg_1782" s="T160">v-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_1783" s="T161">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1784" s="T0">adv</ta>
            <ta e="T2" id="Seg_1785" s="T1">v</ta>
            <ta e="T3" id="Seg_1786" s="T2">n</ta>
            <ta e="T4" id="Seg_1787" s="T3">dempro</ta>
            <ta e="T5" id="Seg_1788" s="T4">v</ta>
            <ta e="T6" id="Seg_1789" s="T5">n</ta>
            <ta e="T7" id="Seg_1790" s="T6">v</ta>
            <ta e="T8" id="Seg_1791" s="T7">conj</ta>
            <ta e="T9" id="Seg_1792" s="T8">n</ta>
            <ta e="T10" id="Seg_1793" s="T9">v</ta>
            <ta e="T11" id="Seg_1794" s="T10">conj</ta>
            <ta e="T12" id="Seg_1795" s="T11">refl</ta>
            <ta e="T13" id="Seg_1796" s="T12">dempro</ta>
            <ta e="T14" id="Seg_1797" s="T13">n</ta>
            <ta e="T16" id="Seg_1798" s="T15">v</ta>
            <ta e="T17" id="Seg_1799" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1800" s="T17">n</ta>
            <ta e="T20" id="Seg_1801" s="T19">dempro</ta>
            <ta e="T21" id="Seg_1802" s="T20">n</ta>
            <ta e="T22" id="Seg_1803" s="T21">v</ta>
            <ta e="T23" id="Seg_1804" s="T22">dempro</ta>
            <ta e="T24" id="Seg_1805" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_1806" s="T24">v</ta>
            <ta e="T26" id="Seg_1807" s="T25">dempro</ta>
            <ta e="T27" id="Seg_1808" s="T26">v</ta>
            <ta e="T28" id="Seg_1809" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1810" s="T28">v</ta>
            <ta e="T30" id="Seg_1811" s="T29">dempro</ta>
            <ta e="T31" id="Seg_1812" s="T30">dempro</ta>
            <ta e="T32" id="Seg_1813" s="T31">n</ta>
            <ta e="T33" id="Seg_1814" s="T32">conj</ta>
            <ta e="T34" id="Seg_1815" s="T33">dempro</ta>
            <ta e="T35" id="Seg_1816" s="T34">v</ta>
            <ta e="T36" id="Seg_1817" s="T35">que</ta>
            <ta e="T37" id="Seg_1818" s="T36">dempro</ta>
            <ta e="T38" id="Seg_1819" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1820" s="T38">v</ta>
            <ta e="T40" id="Seg_1821" s="T39">dempro</ta>
            <ta e="T41" id="Seg_1822" s="T40">conj</ta>
            <ta e="T42" id="Seg_1823" s="T41">adv</ta>
            <ta e="T43" id="Seg_1824" s="T42">v</ta>
            <ta e="T44" id="Seg_1825" s="T43">dempro</ta>
            <ta e="T45" id="Seg_1826" s="T44">v</ta>
            <ta e="T46" id="Seg_1827" s="T45">adv</ta>
            <ta e="T47" id="Seg_1828" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1829" s="T47">n</ta>
            <ta e="T49" id="Seg_1830" s="T48">v</ta>
            <ta e="T50" id="Seg_1831" s="T49">conj</ta>
            <ta e="T51" id="Seg_1832" s="T50">n</ta>
            <ta e="T52" id="Seg_1833" s="T51">v</ta>
            <ta e="T53" id="Seg_1834" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1835" s="T53">conj</ta>
            <ta e="T55" id="Seg_1836" s="T54">dempro</ta>
            <ta e="T56" id="Seg_1837" s="T55">v</ta>
            <ta e="T57" id="Seg_1838" s="T56">pers</ta>
            <ta e="T58" id="Seg_1839" s="T57">v</ta>
            <ta e="T59" id="Seg_1840" s="T58">n</ta>
            <ta e="T60" id="Seg_1841" s="T59">v</ta>
            <ta e="T61" id="Seg_1842" s="T60">v</ta>
            <ta e="T62" id="Seg_1843" s="T61">conj</ta>
            <ta e="T63" id="Seg_1844" s="T62">n</ta>
            <ta e="T64" id="Seg_1845" s="T63">v</ta>
            <ta e="T65" id="Seg_1846" s="T64">v</ta>
            <ta e="T66" id="Seg_1847" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1848" s="T66">n</ta>
            <ta e="T68" id="Seg_1849" s="T67">v</ta>
            <ta e="T70" id="Seg_1850" s="T69">n</ta>
            <ta e="T71" id="Seg_1851" s="T70">n</ta>
            <ta e="T72" id="Seg_1852" s="T71">pers</ta>
            <ta e="T73" id="Seg_1853" s="T72">v</ta>
            <ta e="T74" id="Seg_1854" s="T73">v</ta>
            <ta e="T75" id="Seg_1855" s="T74">n</ta>
            <ta e="T76" id="Seg_1856" s="T75">v</ta>
            <ta e="T77" id="Seg_1857" s="T76">n</ta>
            <ta e="T78" id="Seg_1858" s="T77">v</ta>
            <ta e="T79" id="Seg_1859" s="T78">pers</ta>
            <ta e="T80" id="Seg_1860" s="T79">n</ta>
            <ta e="T81" id="Seg_1861" s="T80">v</ta>
            <ta e="T82" id="Seg_1862" s="T81">n</ta>
            <ta e="T83" id="Seg_1863" s="T82">n</ta>
            <ta e="T84" id="Seg_1864" s="T83">n</ta>
            <ta e="T85" id="Seg_1865" s="T84">ptcl</ta>
            <ta e="T87" id="Seg_1866" s="T86">n</ta>
            <ta e="T88" id="Seg_1867" s="T87">v</ta>
            <ta e="T89" id="Seg_1868" s="T88">pers</ta>
            <ta e="T90" id="Seg_1869" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_1870" s="T90">v</ta>
            <ta e="T92" id="Seg_1871" s="T91">v</ta>
            <ta e="T93" id="Seg_1872" s="T92">adv</ta>
            <ta e="T94" id="Seg_1873" s="T93">v</ta>
            <ta e="T95" id="Seg_1874" s="T94">adv</ta>
            <ta e="T96" id="Seg_1875" s="T95">v</ta>
            <ta e="T97" id="Seg_1876" s="T96">pers</ta>
            <ta e="T98" id="Seg_1877" s="T97">v</ta>
            <ta e="T99" id="Seg_1878" s="T98">n</ta>
            <ta e="T100" id="Seg_1879" s="T99">adv</ta>
            <ta e="T101" id="Seg_1880" s="T100">v</ta>
            <ta e="T102" id="Seg_1881" s="T101">adv</ta>
            <ta e="T103" id="Seg_1882" s="T102">v</ta>
            <ta e="T104" id="Seg_1883" s="T103">adv</ta>
            <ta e="T105" id="Seg_1884" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_1885" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1886" s="T106">v</ta>
            <ta e="T108" id="Seg_1887" s="T107">adv</ta>
            <ta e="T109" id="Seg_1888" s="T108">dempro</ta>
            <ta e="T110" id="Seg_1889" s="T109">n</ta>
            <ta e="T111" id="Seg_1890" s="T110">v</ta>
            <ta e="T113" id="Seg_1891" s="T112">que</ta>
            <ta e="T114" id="Seg_1892" s="T113">adv</ta>
            <ta e="T115" id="Seg_1893" s="T114">dempro</ta>
            <ta e="T116" id="Seg_1894" s="T115">adv</ta>
            <ta e="T118" id="Seg_1895" s="T117">v</ta>
            <ta e="T119" id="Seg_1896" s="T118">dempro</ta>
            <ta e="T120" id="Seg_1897" s="T119">adv</ta>
            <ta e="T121" id="Seg_1898" s="T120">v</ta>
            <ta e="T123" id="Seg_1899" s="T122">v</ta>
            <ta e="T124" id="Seg_1900" s="T123">dempro</ta>
            <ta e="T125" id="Seg_1901" s="T124">dempro</ta>
            <ta e="T126" id="Seg_1902" s="T125">ptcl</ta>
            <ta e="T129" id="Seg_1903" s="T128">dempro</ta>
            <ta e="T130" id="Seg_1904" s="T129">v</ta>
            <ta e="T131" id="Seg_1905" s="T130">v</ta>
            <ta e="T132" id="Seg_1906" s="T131">ptcl</ta>
            <ta e="T134" id="Seg_1907" s="T133">dempro</ta>
            <ta e="T135" id="Seg_1908" s="T134">v</ta>
            <ta e="T136" id="Seg_1909" s="T135">adv</ta>
            <ta e="T138" id="Seg_1910" s="T137">n</ta>
            <ta e="T139" id="Seg_1911" s="T138">v</ta>
            <ta e="T140" id="Seg_1912" s="T139">dempro</ta>
            <ta e="T141" id="Seg_1913" s="T140">v</ta>
            <ta e="T142" id="Seg_1914" s="T141">conj</ta>
            <ta e="T143" id="Seg_1915" s="T142">adv</ta>
            <ta e="T144" id="Seg_1916" s="T143">dempro</ta>
            <ta e="T145" id="Seg_1917" s="T144">n</ta>
            <ta e="T147" id="Seg_1918" s="T146">v</ta>
            <ta e="T148" id="Seg_1919" s="T147">conj</ta>
            <ta e="T149" id="Seg_1920" s="T148">refl</ta>
            <ta e="T150" id="Seg_1921" s="T149">adv</ta>
            <ta e="T151" id="Seg_1922" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_1923" s="T151">v</ta>
            <ta e="T153" id="Seg_1924" s="T152">conj</ta>
            <ta e="T154" id="Seg_1925" s="T153">dempro</ta>
            <ta e="T155" id="Seg_1926" s="T154">n</ta>
            <ta e="T157" id="Seg_1927" s="T156">n</ta>
            <ta e="T158" id="Seg_1928" s="T157">v</ta>
            <ta e="T159" id="Seg_1929" s="T158">conj</ta>
            <ta e="T160" id="Seg_1930" s="T159">num</ta>
            <ta e="T161" id="Seg_1931" s="T160">v</ta>
            <ta e="T162" id="Seg_1932" s="T161">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1933" s="T0">adv:Time</ta>
            <ta e="T3" id="Seg_1934" s="T2">np.h:A</ta>
            <ta e="T4" id="Seg_1935" s="T3">pro.h:Th</ta>
            <ta e="T5" id="Seg_1936" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_1937" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_1938" s="T6">0.3.h:A</ta>
            <ta e="T9" id="Seg_1939" s="T8">np:G</ta>
            <ta e="T10" id="Seg_1940" s="T9">0.3.h:A</ta>
            <ta e="T13" id="Seg_1941" s="T12">pro.h:Poss</ta>
            <ta e="T14" id="Seg_1942" s="T13">np:Th</ta>
            <ta e="T16" id="Seg_1943" s="T15">0.3.h:A</ta>
            <ta e="T21" id="Seg_1944" s="T20">np.h:A</ta>
            <ta e="T23" id="Seg_1945" s="T22">pro.h:Th</ta>
            <ta e="T25" id="Seg_1946" s="T24">0.3.h:E</ta>
            <ta e="T26" id="Seg_1947" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_1948" s="T28">0.1.h:A</ta>
            <ta e="T32" id="Seg_1949" s="T31">np.h:P</ta>
            <ta e="T34" id="Seg_1950" s="T33">pro.h:E</ta>
            <ta e="T37" id="Seg_1951" s="T36">pro.h:E</ta>
            <ta e="T40" id="Seg_1952" s="T39">pro.h:B</ta>
            <ta e="T42" id="Seg_1953" s="T41">adv:Time</ta>
            <ta e="T44" id="Seg_1954" s="T43">pro.h:P</ta>
            <ta e="T46" id="Seg_1955" s="T45">adv:Time</ta>
            <ta e="T48" id="Seg_1956" s="T47">np:P</ta>
            <ta e="T49" id="Seg_1957" s="T48">0.3.h:A</ta>
            <ta e="T51" id="Seg_1958" s="T50">np:P</ta>
            <ta e="T52" id="Seg_1959" s="T51">0.3.h:A</ta>
            <ta e="T55" id="Seg_1960" s="T54">pro.h:A</ta>
            <ta e="T57" id="Seg_1961" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_1962" s="T58">np:G</ta>
            <ta e="T60" id="Seg_1963" s="T59">0.1.h:A</ta>
            <ta e="T63" id="Seg_1964" s="T62">np:P</ta>
            <ta e="T64" id="Seg_1965" s="T63">0.1.h:A</ta>
            <ta e="T65" id="Seg_1966" s="T64">0.3.h:A</ta>
            <ta e="T67" id="Seg_1967" s="T66">np:L</ta>
            <ta e="T68" id="Seg_1968" s="T67">0.3.h:E</ta>
            <ta e="T72" id="Seg_1969" s="T71">pro.h:P</ta>
            <ta e="T75" id="Seg_1970" s="T74">np:P</ta>
            <ta e="T76" id="Seg_1971" s="T75">0.3.h:A</ta>
            <ta e="T77" id="Seg_1972" s="T76">np:P</ta>
            <ta e="T78" id="Seg_1973" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_1974" s="T78">pro.h:Th</ta>
            <ta e="T80" id="Seg_1975" s="T79">np:Cau</ta>
            <ta e="T82" id="Seg_1976" s="T81">np:G</ta>
            <ta e="T83" id="Seg_1977" s="T82">np:Th</ta>
            <ta e="T84" id="Seg_1978" s="T83">np:Th</ta>
            <ta e="T87" id="Seg_1979" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_1980" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_1981" s="T88">pro.h:Poss</ta>
            <ta e="T91" id="Seg_1982" s="T90">0.1.h:A</ta>
            <ta e="T93" id="Seg_1983" s="T92">adv:Time</ta>
            <ta e="T94" id="Seg_1984" s="T93">0.3.h:A</ta>
            <ta e="T96" id="Seg_1985" s="T95">0.3.h:A</ta>
            <ta e="T97" id="Seg_1986" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_1987" s="T98">np:G</ta>
            <ta e="T100" id="Seg_1988" s="T99">adv:Time</ta>
            <ta e="T101" id="Seg_1989" s="T100">0.3.h:A</ta>
            <ta e="T103" id="Seg_1990" s="T102">0.3.h:A</ta>
            <ta e="T107" id="Seg_1991" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1992" s="T107">adv:Time</ta>
            <ta e="T110" id="Seg_1993" s="T109">np.h:A</ta>
            <ta e="T115" id="Seg_1994" s="T114">pro.h:A</ta>
            <ta e="T116" id="Seg_1995" s="T115">adv:L</ta>
            <ta e="T119" id="Seg_1996" s="T118">pro.h:A</ta>
            <ta e="T123" id="Seg_1997" s="T122">0.3.h:A</ta>
            <ta e="T125" id="Seg_1998" s="T124">pro.h:A</ta>
            <ta e="T129" id="Seg_1999" s="T128">pro:G</ta>
            <ta e="T131" id="Seg_2000" s="T130">0.3.h:E</ta>
            <ta e="T134" id="Seg_2001" s="T133">pro.h:Th</ta>
            <ta e="T136" id="Seg_2002" s="T135">adv:Time</ta>
            <ta e="T138" id="Seg_2003" s="T137">np.h:Th</ta>
            <ta e="T139" id="Seg_2004" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_2005" s="T139">pro.h:Th</ta>
            <ta e="T141" id="Seg_2006" s="T140">0.3.h:A</ta>
            <ta e="T143" id="Seg_2007" s="T142">adv:Time</ta>
            <ta e="T145" id="Seg_2008" s="T144">np.h:P</ta>
            <ta e="T147" id="Seg_2009" s="T146">0.3.h:A</ta>
            <ta e="T155" id="Seg_2010" s="T154">np.h:Th</ta>
            <ta e="T157" id="Seg_2011" s="T156">np.h:Th</ta>
            <ta e="T160" id="Seg_2012" s="T159">np.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2013" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_2014" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_2015" s="T3">pro.h:O</ta>
            <ta e="T5" id="Seg_2016" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T6" id="Seg_2017" s="T5">np:O</ta>
            <ta e="T7" id="Seg_2018" s="T6">v:pred 0.3.h:S</ta>
            <ta e="T10" id="Seg_2019" s="T9">v:pred 0.3.h:S</ta>
            <ta e="T14" id="Seg_2020" s="T13">np:O</ta>
            <ta e="T16" id="Seg_2021" s="T15">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_2022" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_2023" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_2024" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_2025" s="T23">ptcl.neg</ta>
            <ta e="T25" id="Seg_2026" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_2027" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_2028" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_2029" s="T28">v:pred 0.1.h:S</ta>
            <ta e="T32" id="Seg_2030" s="T31">np.h:O</ta>
            <ta e="T34" id="Seg_2031" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_2032" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_2033" s="T36">pro.h:S</ta>
            <ta e="T39" id="Seg_2034" s="T38">v:pred</ta>
            <ta e="T44" id="Seg_2035" s="T43">pro.h:O</ta>
            <ta e="T45" id="Seg_2036" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_2037" s="T47">np:O</ta>
            <ta e="T49" id="Seg_2038" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_2039" s="T50">np:O</ta>
            <ta e="T52" id="Seg_2040" s="T51">v:pred 0.3.h:S</ta>
            <ta e="T55" id="Seg_2041" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_2042" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2043" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_2044" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_2045" s="T59">v:pred 0.1.h:S</ta>
            <ta e="T63" id="Seg_2046" s="T62">np:O</ta>
            <ta e="T64" id="Seg_2047" s="T63">v:pred 0.1.h:S</ta>
            <ta e="T65" id="Seg_2048" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_2049" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_2050" s="T71">pro.h:O</ta>
            <ta e="T74" id="Seg_2051" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_2052" s="T74">np:O</ta>
            <ta e="T76" id="Seg_2053" s="T75">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_2054" s="T76">np:O</ta>
            <ta e="T78" id="Seg_2055" s="T77">v:pred 0.3.h:S</ta>
            <ta e="T80" id="Seg_2056" s="T79">np:S</ta>
            <ta e="T81" id="Seg_2057" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_2058" s="T82">np:S</ta>
            <ta e="T84" id="Seg_2059" s="T83">np:O</ta>
            <ta e="T87" id="Seg_2060" s="T86">np:O</ta>
            <ta e="T88" id="Seg_2061" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_2062" s="T89">ptcl.neg</ta>
            <ta e="T91" id="Seg_2063" s="T90">v:pred 0.1.h:S</ta>
            <ta e="T94" id="Seg_2064" s="T93">v:pred 0.3.h:S</ta>
            <ta e="T96" id="Seg_2065" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_2066" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_2067" s="T97">v:pred</ta>
            <ta e="T101" id="Seg_2068" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T103" id="Seg_2069" s="T102">v:pred 0.3.h:S</ta>
            <ta e="T107" id="Seg_2070" s="T106">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_2071" s="T109">np.h:S</ta>
            <ta e="T111" id="Seg_2072" s="T110">v:pred</ta>
            <ta e="T115" id="Seg_2073" s="T114">pro.h:S</ta>
            <ta e="T118" id="Seg_2074" s="T117">v:pred</ta>
            <ta e="T119" id="Seg_2075" s="T118">pro.h:S</ta>
            <ta e="T121" id="Seg_2076" s="T120">v:pred</ta>
            <ta e="T123" id="Seg_2077" s="T122">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_2078" s="T124">pro.h:S</ta>
            <ta e="T130" id="Seg_2079" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_2080" s="T130">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_2081" s="T133">pro.h:O</ta>
            <ta e="T135" id="Seg_2082" s="T134">v:pred</ta>
            <ta e="T138" id="Seg_2083" s="T137">np.h:O</ta>
            <ta e="T139" id="Seg_2084" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_2085" s="T139">pro.h:O</ta>
            <ta e="T141" id="Seg_2086" s="T140">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_2087" s="T144">np.h:O</ta>
            <ta e="T147" id="Seg_2088" s="T146">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_2089" s="T150">ptcl:pred</ta>
            <ta e="T155" id="Seg_2090" s="T154">np.h:S</ta>
            <ta e="T157" id="Seg_2091" s="T156">n:pred</ta>
            <ta e="T158" id="Seg_2092" s="T157">cop </ta>
            <ta e="T160" id="Seg_2093" s="T159">np.h:S</ta>
            <ta e="T161" id="Seg_2094" s="T160">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_2095" s="T2">RUS:cult</ta>
            <ta e="T8" id="Seg_2096" s="T7">RUS:gram</ta>
            <ta e="T11" id="Seg_2097" s="T10">RUS:gram</ta>
            <ta e="T28" id="Seg_2098" s="T27">RUS:gram</ta>
            <ta e="T29" id="Seg_2099" s="T28">%RUS:gram</ta>
            <ta e="T33" id="Seg_2100" s="T32">RUS:gram</ta>
            <ta e="T41" id="Seg_2101" s="T40">RUS:gram</ta>
            <ta e="T45" id="Seg_2102" s="T44">RUS:mod</ta>
            <ta e="T47" id="Seg_2103" s="T46">TURK:disc</ta>
            <ta e="T50" id="Seg_2104" s="T49">RUS:gram</ta>
            <ta e="T53" id="Seg_2105" s="T52">TURK:disc</ta>
            <ta e="T54" id="Seg_2106" s="T53">RUS:gram</ta>
            <ta e="T62" id="Seg_2107" s="T61">RUS:gram</ta>
            <ta e="T66" id="Seg_2108" s="T65">TURK:disc</ta>
            <ta e="T70" id="Seg_2109" s="T69">RUS:core</ta>
            <ta e="T71" id="Seg_2110" s="T70">RUS:core</ta>
            <ta e="T74" id="Seg_2111" s="T73">RUS:mod</ta>
            <ta e="T85" id="Seg_2112" s="T84">TURK:disc</ta>
            <ta e="T106" id="Seg_2113" s="T105">RUS:disc</ta>
            <ta e="T126" id="Seg_2114" s="T125">RUS:mod</ta>
            <ta e="T132" id="Seg_2115" s="T131">TURK:disc</ta>
            <ta e="T142" id="Seg_2116" s="T141">RUS:gram</ta>
            <ta e="T145" id="Seg_2117" s="T144">RUS:cult</ta>
            <ta e="T148" id="Seg_2118" s="T147">RUS:gram</ta>
            <ta e="T151" id="Seg_2119" s="T150">RUS:gram</ta>
            <ta e="T153" id="Seg_2120" s="T152">RUS:gram</ta>
            <ta e="T159" id="Seg_2121" s="T158">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T45" id="Seg_2122" s="T44">RUS:int</ta>
            <ta e="T74" id="Seg_2123" s="T73">RUS:int</ta>
            <ta e="T86" id="Seg_2124" s="T85">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2125" s="T0">Потом пришла колдунья.</ta>
            <ta e="T5" id="Seg_2126" s="T3">Взяла ее.</ta>
            <ta e="T10" id="Seg_2127" s="T5">Привязала камень и бросила в воду.</ta>
            <ta e="T16" id="Seg_2128" s="T10">А сама надела ее одежду.</ta>
            <ta e="T25" id="Seg_2129" s="T16">Этот человек пришел, ее не узнал.</ta>
            <ta e="T32" id="Seg_2130" s="T25">Она говорит: "Давай зарежем этого козленка".</ta>
            <ta e="T40" id="Seg_2131" s="T32">Он думает: "Почему она так сильно его любила.</ta>
            <ta e="T45" id="Seg_2132" s="T40">А теперь хочет его зарезать".</ta>
            <ta e="T49" id="Seg_2133" s="T45">Они кипятят котлы.</ta>
            <ta e="T53" id="Seg_2134" s="T49">И точат ножи.</ta>
            <ta e="T59" id="Seg_2135" s="T53">А он [=козленок] говорит: "Я пойду к реке.</ta>
            <ta e="T62" id="Seg_2136" s="T59">Попью (?).</ta>
            <ta e="T64" id="Seg_2137" s="T62">Травы поем".</ta>
            <ta e="T68" id="Seg_2138" s="T64">Пошел, плачет у реки.</ta>
            <ta e="T74" id="Seg_2139" s="T68">"Сестра, сестра, она хочет меня зарезать.</ta>
            <ta e="T76" id="Seg_2140" s="T74">Котлы кипятят.</ta>
            <ta e="T78" id="Seg_2141" s="T76">Ножи точат".</ta>
            <ta e="T82" id="Seg_2142" s="T78">"Меня камень прижимает к земле.</ta>
            <ta e="T86" id="Seg_2143" s="T82">Трава ноги запутала.</ta>
            <ta e="T89" id="Seg_2144" s="T86">Вокруг ног заплелась у меня.</ta>
            <ta e="T92" id="Seg_2145" s="T89">Не могу (освободиться?)".</ta>
            <ta e="T99" id="Seg_2146" s="T92">Потом он пришел, опять говорит: "Я пойду к реке".</ta>
            <ta e="T107" id="Seg_2147" s="T99">Пошел, опять плачет, все так же сказал.</ta>
            <ta e="T118" id="Seg_2148" s="T107">Тогда этот мужчина думает: "Почему он все время туда ходит?</ta>
            <ta e="T130" id="Seg_2149" s="T118">Он опять попросил… тот тоже пошел [за] ним.</ta>
            <ta e="T135" id="Seg_2150" s="T130">Услышал, как он зовет [сестру]. [?]</ta>
            <ta e="T139" id="Seg_2151" s="T135">Тогда он собрал людей.</ta>
            <ta e="T141" id="Seg_2152" s="T139">Они поймали ее [= колдунью]</ta>
            <ta e="T152" id="Seg_2153" s="T141">Они убили эту ведьму, а сами стали снова жить.</ta>
            <ta e="T158" id="Seg_2154" s="T152">Этот козленок стал человеком.</ta>
            <ta e="T161" id="Seg_2155" s="T158">И они живут втроем.</ta>
            <ta e="T162" id="Seg_2156" s="T161">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2157" s="T0">Then came the witch.</ta>
            <ta e="T5" id="Seg_2158" s="T3">Took her.</ta>
            <ta e="T10" id="Seg_2159" s="T5">Attached a stone and threw her in the water.</ta>
            <ta e="T16" id="Seg_2160" s="T10">And herself put on her clothes.</ta>
            <ta e="T25" id="Seg_2161" s="T16">That man came, he didn't recognize her.</ta>
            <ta e="T32" id="Seg_2162" s="T25">She says: "Let's slaughter this goat."</ta>
            <ta e="T40" id="Seg_2163" s="T32">He thinks: "Why did she love it so much. [?]</ta>
            <ta e="T45" id="Seg_2164" s="T40">And now she wants to slaughter it."</ta>
            <ta e="T49" id="Seg_2165" s="T45">Then they are heating cauldrons.</ta>
            <ta e="T53" id="Seg_2166" s="T49">And they are sharpening the knives.</ta>
            <ta e="T59" id="Seg_2167" s="T53">And it [= the goat] says: "I'll go to the river.</ta>
            <ta e="T62" id="Seg_2168" s="T59">I'll drink (?).</ta>
            <ta e="T64" id="Seg_2169" s="T62">I'll eat grass."</ta>
            <ta e="T68" id="Seg_2170" s="T64">He went and cries [near] the river.</ta>
            <ta e="T74" id="Seg_2171" s="T68">"My sister, she wants to slaughter me.</ta>
            <ta e="T76" id="Seg_2172" s="T74">They are heating cauldrons.</ta>
            <ta e="T78" id="Seg_2173" s="T76">They are sharpening knives."</ta>
            <ta e="T82" id="Seg_2174" s="T78">"A stone is pressing me down to the ground.</ta>
            <ta e="T86" id="Seg_2175" s="T82">The grass [= weed] tied around my feet.</ta>
            <ta e="T89" id="Seg_2176" s="T86">They tied around my feet.</ta>
            <ta e="T92" id="Seg_2177" s="T89">I can't release (myself?)."</ta>
            <ta e="T99" id="Seg_2178" s="T92">Then he came, [then] he says again: "I'll go to the river."</ta>
            <ta e="T107" id="Seg_2179" s="T99">Then he went, cried again, told the same again.</ta>
            <ta e="T118" id="Seg_2180" s="T107">Then that man says: "Why does he always go there?</ta>
            <ta e="T130" id="Seg_2181" s="T118">He asked again, went… this [man] went [after] him, too.</ta>
            <ta e="T135" id="Seg_2182" s="T130">He heard him call [his sister]. [?]</ta>
            <ta e="T139" id="Seg_2183" s="T135">Then he gathered people.</ta>
            <ta e="T141" id="Seg_2184" s="T139">They caught her [= the witch].</ta>
            <ta e="T152" id="Seg_2185" s="T141">And then they killed this witch, and themselves began to live again.</ta>
            <ta e="T158" id="Seg_2186" s="T152">This goat became man.</ta>
            <ta e="T161" id="Seg_2187" s="T158">And they [now] live all they three.</ta>
            <ta e="T162" id="Seg_2188" s="T161">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2189" s="T0">Dann kam die Hexe.</ta>
            <ta e="T5" id="Seg_2190" s="T3">Nahm sie.</ta>
            <ta e="T10" id="Seg_2191" s="T5">Band einen Stein an sie und warf sie ins Wasser.</ta>
            <ta e="T16" id="Seg_2192" s="T10">Und sie selbst zog ihre Kleidung an.</ta>
            <ta e="T25" id="Seg_2193" s="T16">Jener Mann kam, er erkannte sie nicht.</ta>
            <ta e="T32" id="Seg_2194" s="T25">Sie sagt: "Lass uns diese Ziege schlachten."</ta>
            <ta e="T40" id="Seg_2195" s="T32">Er denkt: "Warum hat sie sie so sehr geliebt. [?]</ta>
            <ta e="T45" id="Seg_2196" s="T40">Und jetzt möchte sie sie schlachten."</ta>
            <ta e="T49" id="Seg_2197" s="T45">Dann heizen sie die Kessel.</ta>
            <ta e="T53" id="Seg_2198" s="T49">Und sie schärfen die Messer.</ta>
            <ta e="T59" id="Seg_2199" s="T53">Und sie [= die Ziege] sagt: "Ich gehe zum Fluss.</ta>
            <ta e="T62" id="Seg_2200" s="T59">Ich trinke (?).</ta>
            <ta e="T64" id="Seg_2201" s="T62">Ich fresse Gras."</ta>
            <ta e="T68" id="Seg_2202" s="T64">Sie ging und weint [in der Nähe] des Flusses.</ta>
            <ta e="T74" id="Seg_2203" s="T68">"Meine Schwester, sie möchte mich schlachten.</ta>
            <ta e="T76" id="Seg_2204" s="T74">Sie heizen die Kessel.</ta>
            <ta e="T78" id="Seg_2205" s="T76">Sie schärfen die Messer."</ta>
            <ta e="T82" id="Seg_2206" s="T78">"Ein Stein zieht mich hinunter zum Boden.</ta>
            <ta e="T86" id="Seg_2207" s="T82">Das Gras [= Unkraut] banden sie um meine Füße.</ta>
            <ta e="T89" id="Seg_2208" s="T86">Sie banden es um meine Füße.</ta>
            <ta e="T92" id="Seg_2209" s="T89">Ich kann mich nicht (befreien?)."</ta>
            <ta e="T99" id="Seg_2210" s="T92">Dann kam sie, [dann] sagt sie wieder: "Ich gehe zum Fluss."</ta>
            <ta e="T107" id="Seg_2211" s="T99">Dann ging sie, weinte wieder und erzählte wieder das gleiche.</ta>
            <ta e="T118" id="Seg_2212" s="T107">Dann sagte jener Mann: "Warum geht sie immer dorthin?</ta>
            <ta e="T130" id="Seg_2213" s="T118">Er bat wieder [], ging… und dieser [Mann] ging auch ihm [nach].</ta>
            <ta e="T135" id="Seg_2214" s="T130">Er hörte, dass er [seine Schwester] ruft.</ta>
            <ta e="T139" id="Seg_2215" s="T135">Dann sammelte er Leute.</ta>
            <ta e="T141" id="Seg_2216" s="T139">Sie fingen sie [= die Hexe].</ta>
            <ta e="T152" id="Seg_2217" s="T141">Und dann töteten sie diese Hexe und sie selbst fingen wieder an zu leben.</ta>
            <ta e="T158" id="Seg_2218" s="T152">Diese Ziege wurde zu einem Mann.</ta>
            <ta e="T161" id="Seg_2219" s="T158">Und sie leben [jetzt] alle drei.</ta>
            <ta e="T162" id="Seg_2220" s="T161">Das ist alles.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_2221" s="T0">[GVY:] The second part of the tale "Alenushka and her brother Ivanushka" (2nd version, AEDKL SU0226).</ta>
            <ta e="T32" id="Seg_2222" s="T25">[GVY:] băt- 'slaughter', here is pronounces close to bĭt- 'drink'. The ending is unclear but is most reministent of the Russian 1PL.</ta>
            <ta e="T40" id="Seg_2223" s="T32">[GVY:] dĭn = dĭm</ta>
            <ta e="T45" id="Seg_2224" s="T40">[GVY:] bĭʔ- instead of băʔ-, see above.</ta>
            <ta e="T62" id="Seg_2225" s="T59">[GVY:] da or det(?)</ta>
            <ta e="T64" id="Seg_2226" s="T62">[GVY:] amɨrlan?</ta>
            <ta e="T92" id="Seg_2227" s="T89">[GVY:] oʔbdəsʼtə = uʔbdəsʼtə 'rise (here: to the surface)'?</ta>
            <ta e="T130" id="Seg_2228" s="T118">[GVY:] unclear</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
