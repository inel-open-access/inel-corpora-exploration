<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE2101FA6-F847-D53C-46BD-22F16D64E50A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GirlsWent_flk.wav" />
         <referenced-file url="PKZ_196X_GirlsWent_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GirlsWent_flk\PKZ_196X_GirlsWent_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">12</ud-information>
            <ud-information attribute-name="# e">12</ud-information>
            <ud-information attribute-name="# HIAT:u">2</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T508" time="0.003" type="appl" />
         <tli id="T509" time="1.399" type="appl" />
         <tli id="T510" time="2.796" type="appl" />
         <tli id="T511" time="4.192" type="appl" />
         <tli id="T512" time="6.55951864640884" />
         <tli id="T513" time="7.793" type="appl" />
         <tli id="T514" time="8.748" type="appl" />
         <tli id="T515" time="9.703" type="appl" />
         <tli id="T516" time="10.658" type="appl" />
         <tli id="T517" time="11.614" type="appl" />
         <tli id="T518" time="12.569" type="appl" />
         <tli id="T519" time="13.524" type="appl" />
         <tli id="T520" time="14.479" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T520" id="Seg_0" n="sc" s="T508">
               <ts e="T512" id="Seg_2" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_4" n="HIAT:w" s="T508">Koʔbsaŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_7" n="HIAT:w" s="T509">kambiʔi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_10" n="HIAT:w" s="T510">toltano</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_13" n="HIAT:w" s="T511">jaʔsʼittə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_17" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_19" n="HIAT:w" s="T512">Noʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_22" n="HIAT:w" s="T513">bar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_25" n="HIAT:w" s="T514">soʔsittə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_28" n="HIAT:w" s="T515">i</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T517" id="Seg_32" n="HIAT:w" s="T516">dʼü=</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_36" n="HIAT:w" s="T517">dʼüziʔ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_39" n="HIAT:w" s="T518">kămnasʼtə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_42" n="HIAT:w" s="T519">toltano</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T520" id="Seg_45" n="sc" s="T508">
               <ts e="T509" id="Seg_47" n="e" s="T508">Koʔbsaŋ </ts>
               <ts e="T510" id="Seg_49" n="e" s="T509">kambiʔi </ts>
               <ts e="T511" id="Seg_51" n="e" s="T510">toltano </ts>
               <ts e="T512" id="Seg_53" n="e" s="T511">jaʔsʼittə. </ts>
               <ts e="T513" id="Seg_55" n="e" s="T512">Noʔ </ts>
               <ts e="T514" id="Seg_57" n="e" s="T513">bar </ts>
               <ts e="T515" id="Seg_59" n="e" s="T514">soʔsittə </ts>
               <ts e="T516" id="Seg_61" n="e" s="T515">i </ts>
               <ts e="T517" id="Seg_63" n="e" s="T516">(dʼü=) </ts>
               <ts e="T518" id="Seg_65" n="e" s="T517">dʼüziʔ </ts>
               <ts e="T519" id="Seg_67" n="e" s="T518">kămnasʼtə </ts>
               <ts e="T520" id="Seg_69" n="e" s="T519">toltano. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T512" id="Seg_70" s="T508">PKZ_196X_GirlsWent_flk.001 (001)</ta>
            <ta e="T520" id="Seg_71" s="T512">PKZ_196X_GirlsWent_flk.002 (002)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T512" id="Seg_72" s="T508">Koʔbsaŋ kambiʔi toltano jaʔsʼittə. </ta>
            <ta e="T520" id="Seg_73" s="T512">Noʔ bar soʔsittə i (dʼü=) dʼüziʔ kămnasʼtə toltano. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T509" id="Seg_74" s="T508">koʔb-saŋ</ta>
            <ta e="T510" id="Seg_75" s="T509">kam-bi-ʔi</ta>
            <ta e="T511" id="Seg_76" s="T510">toltano</ta>
            <ta e="T512" id="Seg_77" s="T511">jaʔ-sʼittə</ta>
            <ta e="T513" id="Seg_78" s="T512">noʔ</ta>
            <ta e="T514" id="Seg_79" s="T513">bar</ta>
            <ta e="T515" id="Seg_80" s="T514">soʔ-sittə</ta>
            <ta e="T516" id="Seg_81" s="T515">i</ta>
            <ta e="T517" id="Seg_82" s="T516">dʼü</ta>
            <ta e="T518" id="Seg_83" s="T517">dʼü-ziʔ</ta>
            <ta e="T519" id="Seg_84" s="T518">kămna-sʼtə</ta>
            <ta e="T520" id="Seg_85" s="T519">toltano</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T509" id="Seg_86" s="T508">koʔbdo-zAŋ</ta>
            <ta e="T510" id="Seg_87" s="T509">kan-bi-jəʔ</ta>
            <ta e="T511" id="Seg_88" s="T510">toltano</ta>
            <ta e="T512" id="Seg_89" s="T511">hʼaʔ-zittə</ta>
            <ta e="T513" id="Seg_90" s="T512">noʔ</ta>
            <ta e="T514" id="Seg_91" s="T513">bar</ta>
            <ta e="T515" id="Seg_92" s="T514">soʔ-zittə</ta>
            <ta e="T516" id="Seg_93" s="T515">i</ta>
            <ta e="T517" id="Seg_94" s="T516">tʼo</ta>
            <ta e="T518" id="Seg_95" s="T517">tʼo-ziʔ</ta>
            <ta e="T519" id="Seg_96" s="T518">kămnə-zittə</ta>
            <ta e="T520" id="Seg_97" s="T519">toltano</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T509" id="Seg_98" s="T508">daughter-PL</ta>
            <ta e="T510" id="Seg_99" s="T509">go-PST-3PL</ta>
            <ta e="T511" id="Seg_100" s="T510">potato.[NOM.SG]</ta>
            <ta e="T512" id="Seg_101" s="T511">cut-INF.LAT</ta>
            <ta e="T513" id="Seg_102" s="T512">grass.[NOM.SG]</ta>
            <ta e="T514" id="Seg_103" s="T513">all</ta>
            <ta e="T515" id="Seg_104" s="T514">gather-INF.LAT</ta>
            <ta e="T516" id="Seg_105" s="T515">and</ta>
            <ta e="T517" id="Seg_106" s="T516">earth.[NOM.SG]</ta>
            <ta e="T518" id="Seg_107" s="T517">earth-INS</ta>
            <ta e="T519" id="Seg_108" s="T518">pour-INF.LAT</ta>
            <ta e="T520" id="Seg_109" s="T519">potato.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T509" id="Seg_110" s="T508">дочь-PL</ta>
            <ta e="T510" id="Seg_111" s="T509">пойти-PST-3PL</ta>
            <ta e="T511" id="Seg_112" s="T510">картофель.[NOM.SG]</ta>
            <ta e="T512" id="Seg_113" s="T511">резать-INF.LAT</ta>
            <ta e="T513" id="Seg_114" s="T512">трава.[NOM.SG]</ta>
            <ta e="T514" id="Seg_115" s="T513">весь</ta>
            <ta e="T515" id="Seg_116" s="T514">собирать-INF.LAT</ta>
            <ta e="T516" id="Seg_117" s="T515">и</ta>
            <ta e="T517" id="Seg_118" s="T516">земля.[NOM.SG]</ta>
            <ta e="T518" id="Seg_119" s="T517">земля-INS</ta>
            <ta e="T519" id="Seg_120" s="T518">лить-INF.LAT</ta>
            <ta e="T520" id="Seg_121" s="T519">картофель.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T509" id="Seg_122" s="T508">n-n:num</ta>
            <ta e="T510" id="Seg_123" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_124" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_125" s="T511">v-v:n.fin</ta>
            <ta e="T513" id="Seg_126" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_127" s="T513">quant</ta>
            <ta e="T515" id="Seg_128" s="T514">v-v:n.fin</ta>
            <ta e="T516" id="Seg_129" s="T515">conj</ta>
            <ta e="T517" id="Seg_130" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_131" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_132" s="T518">v-v:n.fin</ta>
            <ta e="T520" id="Seg_133" s="T519">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T509" id="Seg_134" s="T508">n</ta>
            <ta e="T510" id="Seg_135" s="T509">v</ta>
            <ta e="T511" id="Seg_136" s="T510">n</ta>
            <ta e="T512" id="Seg_137" s="T511">v</ta>
            <ta e="T513" id="Seg_138" s="T512">n</ta>
            <ta e="T514" id="Seg_139" s="T513">quant</ta>
            <ta e="T515" id="Seg_140" s="T514">v</ta>
            <ta e="T516" id="Seg_141" s="T515">conj</ta>
            <ta e="T517" id="Seg_142" s="T516">n</ta>
            <ta e="T518" id="Seg_143" s="T517">n</ta>
            <ta e="T519" id="Seg_144" s="T518">v</ta>
            <ta e="T520" id="Seg_145" s="T519">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T509" id="Seg_146" s="T508">np.h:A</ta>
            <ta e="T511" id="Seg_147" s="T510">np:Th</ta>
            <ta e="T513" id="Seg_148" s="T512">np:Th</ta>
            <ta e="T518" id="Seg_149" s="T517">np:Ins</ta>
            <ta e="T520" id="Seg_150" s="T519">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T509" id="Seg_151" s="T508">np.h:S</ta>
            <ta e="T510" id="Seg_152" s="T509">v:pred</ta>
            <ta e="T512" id="Seg_153" s="T510">s:purp</ta>
            <ta e="T515" id="Seg_154" s="T512">s:purp</ta>
            <ta e="T520" id="Seg_155" s="T517">s:purp</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T514" id="Seg_156" s="T513">TURK:disc</ta>
            <ta e="T516" id="Seg_157" s="T515">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T512" id="Seg_158" s="T508">Девушки пошли картошку копать.</ta>
            <ta e="T520" id="Seg_159" s="T512">Собрать всю траву и землёй засыпать картошку.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T512" id="Seg_160" s="T508">Girls went to dig potatoes.</ta>
            <ta e="T520" id="Seg_161" s="T512">To cut all the grass and to cover potato with ground.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T512" id="Seg_162" s="T508">Mädchen gingen um Kartoffeln zu buddeln.</ta>
            <ta e="T520" id="Seg_163" s="T512">Um das ganze Gras zu mähen und um die Kartoffel mit Erde zu bedecken.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T512" id="Seg_164" s="T508">[GVY:] This text was at the end of a recordings and evidently is unfinished. Its continuation has not yet been found.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
