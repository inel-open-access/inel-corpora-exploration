<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF37E3758-D309-9F27-AC63-D8846F12837F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndCauldron_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndCauldron_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndCauldron_flk\PKZ_196X_FoxAndCauldron_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">106</ud-information>
            <ud-information attribute-name="# HIAT:w">70</ud-information>
            <ud-information attribute-name="# e">70</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T761" time="0.051" type="appl" />
         <tli id="T762" time="0.923" type="appl" />
         <tli id="T763" time="1.795" type="appl" />
         <tli id="T764" time="2.667" type="appl" />
         <tli id="T765" time="3.539" type="appl" />
         <tli id="T766" time="4.411" type="appl" />
         <tli id="T767" time="5.283" type="appl" />
         <tli id="T768" time="6.276" type="appl" />
         <tli id="T769" time="7.221" type="appl" />
         <tli id="T770" time="8.166" type="appl" />
         <tli id="T771" time="9.11" type="appl" />
         <tli id="T772" time="9.845" type="appl" />
         <tli id="T773" time="10.579" type="appl" />
         <tli id="T774" time="11.692920207269632" />
         <tli id="T775" time="12.278" type="appl" />
         <tli id="T776" time="12.78" type="appl" />
         <tli id="T777" time="13.281" type="appl" />
         <tli id="T778" time="13.782" type="appl" />
         <tli id="T779" time="14.284" type="appl" />
         <tli id="T780" time="15.359457330415754" />
         <tli id="T781" time="16.058" type="appl" />
         <tli id="T782" time="16.603" type="appl" />
         <tli id="T783" time="17.148" type="appl" />
         <tli id="T784" time="17.694" type="appl" />
         <tli id="T785" time="18.239" type="appl" />
         <tli id="T786" time="18.784" type="appl" />
         <tli id="T787" time="19.71263686026015" />
         <tli id="T788" time="20.583" type="appl" />
         <tli id="T789" time="21.493" type="appl" />
         <tli id="T790" time="22.403" type="appl" />
         <tli id="T791" time="23.313" type="appl" />
         <tli id="T792" time="24.605797311876977" />
         <tli id="T793" time="25.589" type="appl" />
         <tli id="T794" time="26.639" type="appl" />
         <tli id="T795" time="27.69" type="appl" />
         <tli id="T796" time="29.19896836250912" />
         <tli id="T797" time="30.45" type="appl" />
         <tli id="T798" time="31.573" type="appl" />
         <tli id="T799" time="32.695" type="appl" />
         <tli id="T800" time="33.818" type="appl" />
         <tli id="T801" time="34.842" type="appl" />
         <tli id="T802" time="35.866" type="appl" />
         <tli id="T803" time="36.889" type="appl" />
         <tli id="T804" time="37.913" type="appl" />
         <tli id="T805" time="38.392" type="appl" />
         <tli id="T806" time="38.87" type="appl" />
         <tli id="T807" time="39.349" type="appl" />
         <tli id="T808" time="39.828" type="appl" />
         <tli id="T809" time="40.306" type="appl" />
         <tli id="T810" time="40.785" type="appl" />
         <tli id="T811" time="41.263" type="appl" />
         <tli id="T812" time="41.742" type="appl" />
         <tli id="T813" time="42.334" type="appl" />
         <tli id="T814" time="42.925" type="appl" />
         <tli id="T815" time="43.516" type="appl" />
         <tli id="T816" time="44.108" type="appl" />
         <tli id="T817" time="44.843" type="appl" />
         <tli id="T818" time="45.57" type="appl" />
         <tli id="T819" time="46.296" type="appl" />
         <tli id="T820" time="47.023" type="appl" />
         <tli id="T821" time="47.749" type="appl" />
         <tli id="T822" time="48.476" type="appl" />
         <tli id="T823" time="49.202" type="appl" />
         <tli id="T824" time="49.929" type="appl" />
         <tli id="T825" time="50.655" type="appl" />
         <tli id="T826" time="51.382" type="appl" />
         <tli id="T827" time="52.108" type="appl" />
         <tli id="T828" time="52.626" type="appl" />
         <tli id="T829" time="53.145" type="appl" />
         <tli id="T830" time="53.663" type="appl" />
         <tli id="T831" time="54.838" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T831" id="Seg_0" n="sc" s="T761">
               <ts e="T767" id="Seg_2" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_4" n="HIAT:w" s="T761">Lisa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_7" n="HIAT:w" s="T762">onʼiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_10" n="HIAT:w" s="T763">kuzandə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_13" n="HIAT:w" s="T764">mĭmbi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_16" n="HIAT:w" s="T765">kurizəʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_19" n="HIAT:w" s="T766">tojirzittə</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_23" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_25" n="HIAT:w" s="T767">Dĭgəttə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_28" n="HIAT:w" s="T768">dĭ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_31" n="HIAT:w" s="T769">edəbi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_34" n="HIAT:w" s="T770">aspak</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_38" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_40" n="HIAT:w" s="T771">Aspak</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_43" n="HIAT:w" s="T772">üge</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_46" n="HIAT:w" s="T773">kuzerleʔbə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_50" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_52" n="HIAT:w" s="T774">A</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_55" n="HIAT:w" s="T775">dĭ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_58" n="HIAT:w" s="T776">măndə:</ts>
                  <nts id="Seg_59" n="HIAT:ip">"</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_62" n="HIAT:w" s="T777">Šindi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_65" n="HIAT:w" s="T778">dĭn</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_68" n="HIAT:w" s="T779">kuzerleʔbə</ts>
                  <nts id="Seg_69" n="HIAT:ip">?</nts>
                  <nts id="Seg_70" n="HIAT:ip">"</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_73" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_75" n="HIAT:w" s="T780">Šobi</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_79" n="HIAT:w" s="T781">dĭ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_81" n="HIAT:ip">(</nts>
                  <ts e="T783" id="Seg_83" n="HIAT:w" s="T782">aspaʔ=</ts>
                  <nts id="Seg_84" n="HIAT:ip">)</nts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_88" n="HIAT:w" s="T783">dĭ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_91" n="HIAT:w" s="T784">dĭm</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_94" n="HIAT:w" s="T785">aspaʔdə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_97" n="HIAT:w" s="T786">ibi</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_101" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_103" n="HIAT:w" s="T787">Edəbi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_105" n="HIAT:ip">(</nts>
                  <ts e="T789" id="Seg_107" n="HIAT:w" s="T788">bostə=</ts>
                  <nts id="Seg_108" n="HIAT:ip">)</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_111" n="HIAT:w" s="T789">boskəndə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_114" n="HIAT:w" s="T790">ulundə</ts>
                  <nts id="Seg_115" n="HIAT:ip">,</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_118" n="HIAT:w" s="T791">kambi</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_122" n="HIAT:u" s="T792">
                  <nts id="Seg_123" n="HIAT:ip">(</nts>
                  <ts e="T793" id="Seg_125" n="HIAT:w" s="T792">Kunuʔ-</ts>
                  <nts id="Seg_126" n="HIAT:ip">)</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_129" n="HIAT:w" s="T793">Kunnaːmbi</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_133" n="HIAT:w" s="T794">prorubdə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_136" n="HIAT:w" s="T795">öʔlüʔbi</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_140" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_142" n="HIAT:w" s="T796">Aspakdə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_145" n="HIAT:w" s="T797">iʔgö</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_148" n="HIAT:w" s="T798">bü</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_151" n="HIAT:w" s="T799">mʼaŋbi</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_155" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_157" n="HIAT:w" s="T800">Dĭgəttə</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_160" n="HIAT:w" s="T801">aspaʔ</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_163" n="HIAT:w" s="T802">dĭbər</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_166" n="HIAT:w" s="T803">amnaʔbə</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_170" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_172" n="HIAT:w" s="T804">A</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_175" n="HIAT:w" s="T805">dĭ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_178" n="HIAT:w" s="T806">măndə:</ts>
                  <nts id="Seg_179" n="HIAT:ip">"</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_182" n="HIAT:w" s="T807">Măna</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_184" n="HIAT:ip">(</nts>
                  <ts e="T809" id="Seg_186" n="HIAT:w" s="T808">i-</ts>
                  <nts id="Seg_187" n="HIAT:ip">)</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_190" n="HIAT:w" s="T809">dibər</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_193" n="HIAT:w" s="T810">iʔ</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_196" n="HIAT:w" s="T811">kunaʔ</ts>
                  <nts id="Seg_197" n="HIAT:ip">!</nts>
                  <nts id="Seg_198" n="HIAT:ip">"</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_201" n="HIAT:u" s="T812">
                  <nts id="Seg_202" n="HIAT:ip">"</nts>
                  <ts e="T813" id="Seg_204" n="HIAT:w" s="T812">Măn</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_206" n="HIAT:ip">(</nts>
                  <ts e="T814" id="Seg_208" n="HIAT:w" s="T813">tʼora</ts>
                  <nts id="Seg_209" n="HIAT:ip">)</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_212" n="HIAT:w" s="T814">dăra</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_215" n="HIAT:w" s="T815">abiam</ts>
                  <nts id="Seg_216" n="HIAT:ip">"</nts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_220" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_222" n="HIAT:w" s="T816">Dĭgəttə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_225" n="HIAT:w" s="T817">aspaʔ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_228" n="HIAT:w" s="T818">kambi</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_231" n="HIAT:w" s="T819">bünə</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_235" n="HIAT:w" s="T820">i</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_238" n="HIAT:w" s="T821">lisa</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_242" n="HIAT:w" s="T822">šidegöʔ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_245" n="HIAT:w" s="T823">bar</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_248" n="HIAT:w" s="T824">külaːmbiʔi</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_251" n="HIAT:w" s="T825">dĭ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_254" n="HIAT:w" s="T826">bügən</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_258" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_260" n="HIAT:w" s="T827">I</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_263" n="HIAT:w" s="T828">daška</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_266" n="HIAT:w" s="T829">naga</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T831" id="Seg_270" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_272" n="HIAT:w" s="T830">Kabarləj</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T831" id="Seg_275" n="sc" s="T761">
               <ts e="T762" id="Seg_277" n="e" s="T761">Lisa </ts>
               <ts e="T763" id="Seg_279" n="e" s="T762">onʼiʔ </ts>
               <ts e="T764" id="Seg_281" n="e" s="T763">kuzandə </ts>
               <ts e="T765" id="Seg_283" n="e" s="T764">mĭmbi </ts>
               <ts e="T766" id="Seg_285" n="e" s="T765">kurizəʔi </ts>
               <ts e="T767" id="Seg_287" n="e" s="T766">tojirzittə. </ts>
               <ts e="T768" id="Seg_289" n="e" s="T767">Dĭgəttə </ts>
               <ts e="T769" id="Seg_291" n="e" s="T768">dĭ </ts>
               <ts e="T770" id="Seg_293" n="e" s="T769">edəbi </ts>
               <ts e="T771" id="Seg_295" n="e" s="T770">aspak. </ts>
               <ts e="T772" id="Seg_297" n="e" s="T771">Aspak </ts>
               <ts e="T773" id="Seg_299" n="e" s="T772">üge </ts>
               <ts e="T774" id="Seg_301" n="e" s="T773">kuzerleʔbə. </ts>
               <ts e="T775" id="Seg_303" n="e" s="T774">A </ts>
               <ts e="T776" id="Seg_305" n="e" s="T775">dĭ </ts>
               <ts e="T777" id="Seg_307" n="e" s="T776">măndə:" </ts>
               <ts e="T778" id="Seg_309" n="e" s="T777">Šindi </ts>
               <ts e="T779" id="Seg_311" n="e" s="T778">dĭn </ts>
               <ts e="T780" id="Seg_313" n="e" s="T779">kuzerleʔbə?" </ts>
               <ts e="T781" id="Seg_315" n="e" s="T780">Šobi, </ts>
               <ts e="T782" id="Seg_317" n="e" s="T781">dĭ </ts>
               <ts e="T783" id="Seg_319" n="e" s="T782">(aspaʔ=), </ts>
               <ts e="T784" id="Seg_321" n="e" s="T783">dĭ </ts>
               <ts e="T785" id="Seg_323" n="e" s="T784">dĭm </ts>
               <ts e="T786" id="Seg_325" n="e" s="T785">aspaʔdə </ts>
               <ts e="T787" id="Seg_327" n="e" s="T786">ibi. </ts>
               <ts e="T788" id="Seg_329" n="e" s="T787">Edəbi </ts>
               <ts e="T789" id="Seg_331" n="e" s="T788">(bostə=) </ts>
               <ts e="T790" id="Seg_333" n="e" s="T789">boskəndə </ts>
               <ts e="T791" id="Seg_335" n="e" s="T790">ulundə, </ts>
               <ts e="T792" id="Seg_337" n="e" s="T791">kambi. </ts>
               <ts e="T793" id="Seg_339" n="e" s="T792">(Kunuʔ-) </ts>
               <ts e="T794" id="Seg_341" n="e" s="T793">Kunnaːmbi, </ts>
               <ts e="T795" id="Seg_343" n="e" s="T794">prorubdə </ts>
               <ts e="T796" id="Seg_345" n="e" s="T795">öʔlüʔbi. </ts>
               <ts e="T797" id="Seg_347" n="e" s="T796">Aspakdə </ts>
               <ts e="T798" id="Seg_349" n="e" s="T797">iʔgö </ts>
               <ts e="T799" id="Seg_351" n="e" s="T798">bü </ts>
               <ts e="T800" id="Seg_353" n="e" s="T799">mʼaŋbi. </ts>
               <ts e="T801" id="Seg_355" n="e" s="T800">Dĭgəttə </ts>
               <ts e="T802" id="Seg_357" n="e" s="T801">aspaʔ </ts>
               <ts e="T803" id="Seg_359" n="e" s="T802">dĭbər </ts>
               <ts e="T804" id="Seg_361" n="e" s="T803">amnaʔbə. </ts>
               <ts e="T805" id="Seg_363" n="e" s="T804">A </ts>
               <ts e="T806" id="Seg_365" n="e" s="T805">dĭ </ts>
               <ts e="T807" id="Seg_367" n="e" s="T806">măndə:" </ts>
               <ts e="T808" id="Seg_369" n="e" s="T807">Măna </ts>
               <ts e="T809" id="Seg_371" n="e" s="T808">(i-) </ts>
               <ts e="T810" id="Seg_373" n="e" s="T809">dibər </ts>
               <ts e="T811" id="Seg_375" n="e" s="T810">iʔ </ts>
               <ts e="T812" id="Seg_377" n="e" s="T811">kunaʔ!" </ts>
               <ts e="T813" id="Seg_379" n="e" s="T812">"Măn </ts>
               <ts e="T814" id="Seg_381" n="e" s="T813">(tʼora) </ts>
               <ts e="T815" id="Seg_383" n="e" s="T814">dăra </ts>
               <ts e="T816" id="Seg_385" n="e" s="T815">abiam". </ts>
               <ts e="T817" id="Seg_387" n="e" s="T816">Dĭgəttə </ts>
               <ts e="T818" id="Seg_389" n="e" s="T817">aspaʔ </ts>
               <ts e="T819" id="Seg_391" n="e" s="T818">kambi </ts>
               <ts e="T820" id="Seg_393" n="e" s="T819">bünə, </ts>
               <ts e="T821" id="Seg_395" n="e" s="T820">i </ts>
               <ts e="T822" id="Seg_397" n="e" s="T821">lisa, </ts>
               <ts e="T823" id="Seg_399" n="e" s="T822">šidegöʔ </ts>
               <ts e="T824" id="Seg_401" n="e" s="T823">bar </ts>
               <ts e="T825" id="Seg_403" n="e" s="T824">külaːmbiʔi </ts>
               <ts e="T826" id="Seg_405" n="e" s="T825">dĭ </ts>
               <ts e="T827" id="Seg_407" n="e" s="T826">bügən. </ts>
               <ts e="T828" id="Seg_409" n="e" s="T827">I </ts>
               <ts e="T829" id="Seg_411" n="e" s="T828">daška </ts>
               <ts e="T830" id="Seg_413" n="e" s="T829">naga. </ts>
               <ts e="T831" id="Seg_415" n="e" s="T830">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T767" id="Seg_416" s="T761">PKZ_196X_FoxAndCauldron_flk.001 (001)</ta>
            <ta e="T771" id="Seg_417" s="T767">PKZ_196X_FoxAndCauldron_flk.002 (002)</ta>
            <ta e="T774" id="Seg_418" s="T771">PKZ_196X_FoxAndCauldron_flk.003 (003)</ta>
            <ta e="T780" id="Seg_419" s="T774">PKZ_196X_FoxAndCauldron_flk.004 (004)</ta>
            <ta e="T787" id="Seg_420" s="T780">PKZ_196X_FoxAndCauldron_flk.005 (005)</ta>
            <ta e="T792" id="Seg_421" s="T787">PKZ_196X_FoxAndCauldron_flk.006 (006)</ta>
            <ta e="T796" id="Seg_422" s="T792">PKZ_196X_FoxAndCauldron_flk.007 (007)</ta>
            <ta e="T800" id="Seg_423" s="T796">PKZ_196X_FoxAndCauldron_flk.008 (008)</ta>
            <ta e="T804" id="Seg_424" s="T800">PKZ_196X_FoxAndCauldron_flk.009 (009)</ta>
            <ta e="T812" id="Seg_425" s="T804">PKZ_196X_FoxAndCauldron_flk.010 (010)</ta>
            <ta e="T816" id="Seg_426" s="T812">PKZ_196X_FoxAndCauldron_flk.011 (011)</ta>
            <ta e="T827" id="Seg_427" s="T816">PKZ_196X_FoxAndCauldron_flk.012 (012)</ta>
            <ta e="T830" id="Seg_428" s="T827">PKZ_196X_FoxAndCauldron_flk.013 (013)</ta>
            <ta e="T831" id="Seg_429" s="T830">PKZ_196X_FoxAndCauldron_flk.014 (014)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T767" id="Seg_430" s="T761">Lisa onʼiʔ kuzandə mĭmbi kurizəʔi tojirzittə. </ta>
            <ta e="T771" id="Seg_431" s="T767">Dĭgəttə dĭ edəbi aspak. </ta>
            <ta e="T774" id="Seg_432" s="T771">Aspak üge kuzerleʔbə. </ta>
            <ta e="T780" id="Seg_433" s="T774">A dĭ măndə:" Šindi dĭn kuzerleʔbə?" </ta>
            <ta e="T787" id="Seg_434" s="T780">Šobi, dĭ (aspaʔ=), dĭ dĭm aspaʔdə ibi. </ta>
            <ta e="T792" id="Seg_435" s="T787">Edəbi (bostə=) boskəndə ulundə, kambi. </ta>
            <ta e="T796" id="Seg_436" s="T792">(Kunuʔ-) Kunnaːmbi, prorubdə öʔlüʔbi. </ta>
            <ta e="T800" id="Seg_437" s="T796">Aspakdə iʔgö bü mʼaŋbi. </ta>
            <ta e="T804" id="Seg_438" s="T800">Dĭgəttə aspaʔ dĭbər amnaʔbə. </ta>
            <ta e="T812" id="Seg_439" s="T804">A dĭ măndə:" Măna (i-) dibər iʔ kunaʔ!" </ta>
            <ta e="T816" id="Seg_440" s="T812">"Măn (tʼora) dăra abiam". </ta>
            <ta e="T827" id="Seg_441" s="T816">Dĭgəttə aspaʔ kambi bünə, i lisa, šidegöʔ bar külaːmbiʔi dĭ bügən. </ta>
            <ta e="T830" id="Seg_442" s="T827">I daška naga. </ta>
            <ta e="T831" id="Seg_443" s="T830">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T762" id="Seg_444" s="T761">lisa</ta>
            <ta e="T763" id="Seg_445" s="T762">onʼiʔ</ta>
            <ta e="T764" id="Seg_446" s="T763">kuza-ndə</ta>
            <ta e="T765" id="Seg_447" s="T764">mĭm-bi</ta>
            <ta e="T766" id="Seg_448" s="T765">kurizə-ʔi</ta>
            <ta e="T767" id="Seg_449" s="T766">tojir-zittə</ta>
            <ta e="T768" id="Seg_450" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_451" s="T768">dĭ</ta>
            <ta e="T770" id="Seg_452" s="T769">edə-bi</ta>
            <ta e="T771" id="Seg_453" s="T770">aspak</ta>
            <ta e="T772" id="Seg_454" s="T771">aspak</ta>
            <ta e="T773" id="Seg_455" s="T772">üge</ta>
            <ta e="T774" id="Seg_456" s="T773">kuzer-leʔbə</ta>
            <ta e="T775" id="Seg_457" s="T774">a</ta>
            <ta e="T776" id="Seg_458" s="T775">dĭ</ta>
            <ta e="T777" id="Seg_459" s="T776">măn-də</ta>
            <ta e="T778" id="Seg_460" s="T777">šindi</ta>
            <ta e="T779" id="Seg_461" s="T778">dĭn</ta>
            <ta e="T780" id="Seg_462" s="T779">kuzer-leʔbə</ta>
            <ta e="T781" id="Seg_463" s="T780">šo-bi</ta>
            <ta e="T782" id="Seg_464" s="T781">dĭ</ta>
            <ta e="T783" id="Seg_465" s="T782">aspaʔ</ta>
            <ta e="T784" id="Seg_466" s="T783">dĭ</ta>
            <ta e="T785" id="Seg_467" s="T784">dĭ-m</ta>
            <ta e="T786" id="Seg_468" s="T785">aspaʔ-də</ta>
            <ta e="T787" id="Seg_469" s="T786">i-bi</ta>
            <ta e="T788" id="Seg_470" s="T787">edə-bi</ta>
            <ta e="T789" id="Seg_471" s="T788">bos-tə</ta>
            <ta e="T790" id="Seg_472" s="T789">bos-kəndə</ta>
            <ta e="T791" id="Seg_473" s="T790">ulu-ndə</ta>
            <ta e="T792" id="Seg_474" s="T791">kam-bi</ta>
            <ta e="T794" id="Seg_475" s="T793">kun-naːm-bi</ta>
            <ta e="T795" id="Seg_476" s="T794">prorub-də</ta>
            <ta e="T796" id="Seg_477" s="T795">öʔ-lüʔ-bi</ta>
            <ta e="T797" id="Seg_478" s="T796">aspak-də</ta>
            <ta e="T798" id="Seg_479" s="T797">iʔgö</ta>
            <ta e="T799" id="Seg_480" s="T798">bü</ta>
            <ta e="T800" id="Seg_481" s="T799">mʼaŋ-bi</ta>
            <ta e="T801" id="Seg_482" s="T800">dĭgəttə</ta>
            <ta e="T802" id="Seg_483" s="T801">aspaʔ</ta>
            <ta e="T803" id="Seg_484" s="T802">dĭbər</ta>
            <ta e="T804" id="Seg_485" s="T803">am-naʔbə</ta>
            <ta e="T805" id="Seg_486" s="T804">a</ta>
            <ta e="T806" id="Seg_487" s="T805">dĭ</ta>
            <ta e="T807" id="Seg_488" s="T806">măn-də</ta>
            <ta e="T808" id="Seg_489" s="T807">măna</ta>
            <ta e="T810" id="Seg_490" s="T809">dibər</ta>
            <ta e="T811" id="Seg_491" s="T810">i-ʔ</ta>
            <ta e="T812" id="Seg_492" s="T811">kun-a-ʔ</ta>
            <ta e="T813" id="Seg_493" s="T812">măn</ta>
            <ta e="T814" id="Seg_494" s="T813">tʼora</ta>
            <ta e="T815" id="Seg_495" s="T814">dăra</ta>
            <ta e="T816" id="Seg_496" s="T815">a-bia-m</ta>
            <ta e="T817" id="Seg_497" s="T816">dĭgəttə</ta>
            <ta e="T818" id="Seg_498" s="T817">aspaʔ</ta>
            <ta e="T819" id="Seg_499" s="T818">kam-bi</ta>
            <ta e="T820" id="Seg_500" s="T819">bü-nə</ta>
            <ta e="T821" id="Seg_501" s="T820">i</ta>
            <ta e="T822" id="Seg_502" s="T821">lisa</ta>
            <ta e="T823" id="Seg_503" s="T822">šide-göʔ</ta>
            <ta e="T824" id="Seg_504" s="T823">bar</ta>
            <ta e="T825" id="Seg_505" s="T824">kü-laːm-bi-ʔi</ta>
            <ta e="T826" id="Seg_506" s="T825">dĭ</ta>
            <ta e="T827" id="Seg_507" s="T826">bü-gən</ta>
            <ta e="T828" id="Seg_508" s="T827">i</ta>
            <ta e="T829" id="Seg_509" s="T828">daška</ta>
            <ta e="T830" id="Seg_510" s="T829">naga</ta>
            <ta e="T831" id="Seg_511" s="T830">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T762" id="Seg_512" s="T761">lʼisa</ta>
            <ta e="T763" id="Seg_513" s="T762">onʼiʔ</ta>
            <ta e="T764" id="Seg_514" s="T763">kuza-gəndə</ta>
            <ta e="T765" id="Seg_515" s="T764">mĭn-bi</ta>
            <ta e="T766" id="Seg_516" s="T765">kuriza-jəʔ</ta>
            <ta e="T767" id="Seg_517" s="T766">tojar-zittə</ta>
            <ta e="T768" id="Seg_518" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_519" s="T768">dĭ</ta>
            <ta e="T770" id="Seg_520" s="T769">edə-bi</ta>
            <ta e="T771" id="Seg_521" s="T770">aspaʔ</ta>
            <ta e="T772" id="Seg_522" s="T771">aspaʔ</ta>
            <ta e="T773" id="Seg_523" s="T772">üge</ta>
            <ta e="T774" id="Seg_524" s="T773">%%-laʔbə</ta>
            <ta e="T775" id="Seg_525" s="T774">a</ta>
            <ta e="T776" id="Seg_526" s="T775">dĭ</ta>
            <ta e="T777" id="Seg_527" s="T776">măn-ntə</ta>
            <ta e="T778" id="Seg_528" s="T777">šində</ta>
            <ta e="T779" id="Seg_529" s="T778">dĭn</ta>
            <ta e="T780" id="Seg_530" s="T779">%%-laʔbə</ta>
            <ta e="T781" id="Seg_531" s="T780">šo-bi</ta>
            <ta e="T782" id="Seg_532" s="T781">dĭ</ta>
            <ta e="T783" id="Seg_533" s="T782">aspaʔ</ta>
            <ta e="T784" id="Seg_534" s="T783">dĭ</ta>
            <ta e="T785" id="Seg_535" s="T784">dĭ-m</ta>
            <ta e="T786" id="Seg_536" s="T785">aspaʔ-də</ta>
            <ta e="T787" id="Seg_537" s="T786">i-bi</ta>
            <ta e="T788" id="Seg_538" s="T787">edə-bi</ta>
            <ta e="T789" id="Seg_539" s="T788">bos-də</ta>
            <ta e="T790" id="Seg_540" s="T789">bos-gəndə</ta>
            <ta e="T791" id="Seg_541" s="T790">ulu-gəndə</ta>
            <ta e="T792" id="Seg_542" s="T791">kan-bi</ta>
            <ta e="T794" id="Seg_543" s="T793">kun-laːm-bi</ta>
            <ta e="T795" id="Seg_544" s="T794">prorubʼ-Tə</ta>
            <ta e="T796" id="Seg_545" s="T795">öʔ-luʔbdə-bi</ta>
            <ta e="T797" id="Seg_546" s="T796">aspaʔ-Tə</ta>
            <ta e="T798" id="Seg_547" s="T797">iʔgö</ta>
            <ta e="T799" id="Seg_548" s="T798">bü</ta>
            <ta e="T800" id="Seg_549" s="T799">mʼaŋ-bi</ta>
            <ta e="T801" id="Seg_550" s="T800">dĭgəttə</ta>
            <ta e="T802" id="Seg_551" s="T801">aspaʔ</ta>
            <ta e="T803" id="Seg_552" s="T802">dĭbər</ta>
            <ta e="T804" id="Seg_553" s="T803">am-laʔbə</ta>
            <ta e="T805" id="Seg_554" s="T804">a</ta>
            <ta e="T806" id="Seg_555" s="T805">dĭ</ta>
            <ta e="T807" id="Seg_556" s="T806">măn-ntə</ta>
            <ta e="T808" id="Seg_557" s="T807">măna</ta>
            <ta e="T810" id="Seg_558" s="T809">dĭbər</ta>
            <ta e="T811" id="Seg_559" s="T810">e-ʔ</ta>
            <ta e="T812" id="Seg_560" s="T811">kun-ə-ʔ</ta>
            <ta e="T813" id="Seg_561" s="T812">măn</ta>
            <ta e="T814" id="Seg_562" s="T813">tʼora</ta>
            <ta e="T815" id="Seg_563" s="T814">dăra</ta>
            <ta e="T816" id="Seg_564" s="T815">a-bi-m</ta>
            <ta e="T817" id="Seg_565" s="T816">dĭgəttə</ta>
            <ta e="T818" id="Seg_566" s="T817">aspaʔ</ta>
            <ta e="T819" id="Seg_567" s="T818">kan-bi</ta>
            <ta e="T820" id="Seg_568" s="T819">bü-Tə</ta>
            <ta e="T821" id="Seg_569" s="T820">i</ta>
            <ta e="T822" id="Seg_570" s="T821">lʼisa</ta>
            <ta e="T823" id="Seg_571" s="T822">šide-göʔ</ta>
            <ta e="T824" id="Seg_572" s="T823">bar</ta>
            <ta e="T825" id="Seg_573" s="T824">kü-laːm-bi-jəʔ</ta>
            <ta e="T826" id="Seg_574" s="T825">dĭ</ta>
            <ta e="T827" id="Seg_575" s="T826">bü-Kən</ta>
            <ta e="T828" id="Seg_576" s="T827">i</ta>
            <ta e="T829" id="Seg_577" s="T828">daška</ta>
            <ta e="T830" id="Seg_578" s="T829">naga</ta>
            <ta e="T831" id="Seg_579" s="T830">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T762" id="Seg_580" s="T761">fox.[NOM.SG]</ta>
            <ta e="T763" id="Seg_581" s="T762">one.[NOM.SG]</ta>
            <ta e="T764" id="Seg_582" s="T763">man-LAT/LOC.3SG</ta>
            <ta e="T765" id="Seg_583" s="T764">go-PST.[3SG]</ta>
            <ta e="T766" id="Seg_584" s="T765">hen-PL</ta>
            <ta e="T767" id="Seg_585" s="T766">steal-INF.LAT</ta>
            <ta e="T768" id="Seg_586" s="T767">then</ta>
            <ta e="T769" id="Seg_587" s="T768">this.[NOM.SG]</ta>
            <ta e="T770" id="Seg_588" s="T769">hang.up-PST.[3SG]</ta>
            <ta e="T771" id="Seg_589" s="T770">cauldron.[NOM.SG]</ta>
            <ta e="T772" id="Seg_590" s="T771">cauldron.[NOM.SG]</ta>
            <ta e="T773" id="Seg_591" s="T772">always</ta>
            <ta e="T774" id="Seg_592" s="T773">%%-DUR</ta>
            <ta e="T775" id="Seg_593" s="T774">and</ta>
            <ta e="T776" id="Seg_594" s="T775">this.[NOM.SG]</ta>
            <ta e="T777" id="Seg_595" s="T776">say-IPFVZ.[3SG]</ta>
            <ta e="T778" id="Seg_596" s="T777">who.[NOM.SG]</ta>
            <ta e="T779" id="Seg_597" s="T778">there</ta>
            <ta e="T780" id="Seg_598" s="T779">%%-DUR</ta>
            <ta e="T781" id="Seg_599" s="T780">come-PST.[3SG]</ta>
            <ta e="T782" id="Seg_600" s="T781">this.[NOM.SG]</ta>
            <ta e="T783" id="Seg_601" s="T782">cauldron.[NOM.SG]</ta>
            <ta e="T784" id="Seg_602" s="T783">this.[NOM.SG]</ta>
            <ta e="T785" id="Seg_603" s="T784">this-ACC</ta>
            <ta e="T786" id="Seg_604" s="T785">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T787" id="Seg_605" s="T786">take-PST.[3SG]</ta>
            <ta e="T788" id="Seg_606" s="T787">hang.up-PST.[3SG]</ta>
            <ta e="T789" id="Seg_607" s="T788">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T790" id="Seg_608" s="T789">self-LAT/LOC.3SG</ta>
            <ta e="T791" id="Seg_609" s="T790">head-LAT/LOC.3SG</ta>
            <ta e="T792" id="Seg_610" s="T791">go-PST.[3SG]</ta>
            <ta e="T794" id="Seg_611" s="T793">bring-RES-PST.[3SG]</ta>
            <ta e="T795" id="Seg_612" s="T794">ice_hole-LAT</ta>
            <ta e="T796" id="Seg_613" s="T795">send-MOM-PST.[3SG]</ta>
            <ta e="T797" id="Seg_614" s="T796">cauldron-LAT</ta>
            <ta e="T798" id="Seg_615" s="T797">many</ta>
            <ta e="T799" id="Seg_616" s="T798">water.[NOM.SG]</ta>
            <ta e="T800" id="Seg_617" s="T799">flow-PST.[3SG]</ta>
            <ta e="T801" id="Seg_618" s="T800">then</ta>
            <ta e="T802" id="Seg_619" s="T801">cauldron.[NOM.SG]</ta>
            <ta e="T803" id="Seg_620" s="T802">there</ta>
            <ta e="T804" id="Seg_621" s="T803">sit-DUR.[3SG]</ta>
            <ta e="T805" id="Seg_622" s="T804">and</ta>
            <ta e="T806" id="Seg_623" s="T805">this.[NOM.SG]</ta>
            <ta e="T807" id="Seg_624" s="T806">say-IPFVZ.[3SG]</ta>
            <ta e="T808" id="Seg_625" s="T807">I.ACC</ta>
            <ta e="T810" id="Seg_626" s="T809">there</ta>
            <ta e="T811" id="Seg_627" s="T810">NEG.AUX-IMP.2SG</ta>
            <ta e="T812" id="Seg_628" s="T811">bring-EP-CNG</ta>
            <ta e="T813" id="Seg_629" s="T812">I.NOM</ta>
            <ta e="T814" id="Seg_630" s="T813">%%</ta>
            <ta e="T815" id="Seg_631" s="T814">along</ta>
            <ta e="T816" id="Seg_632" s="T815">make-PST-1SG</ta>
            <ta e="T817" id="Seg_633" s="T816">then</ta>
            <ta e="T818" id="Seg_634" s="T817">cauldron.[NOM.SG]</ta>
            <ta e="T819" id="Seg_635" s="T818">go-PST.[3SG]</ta>
            <ta e="T820" id="Seg_636" s="T819">water-LAT</ta>
            <ta e="T821" id="Seg_637" s="T820">and</ta>
            <ta e="T822" id="Seg_638" s="T821">fox.[NOM.SG]</ta>
            <ta e="T823" id="Seg_639" s="T822">two-COLL</ta>
            <ta e="T824" id="Seg_640" s="T823">PTCL</ta>
            <ta e="T825" id="Seg_641" s="T824">die-RES-PST-3PL</ta>
            <ta e="T826" id="Seg_642" s="T825">this.[NOM.SG]</ta>
            <ta e="T827" id="Seg_643" s="T826">water-LOC</ta>
            <ta e="T828" id="Seg_644" s="T827">and</ta>
            <ta e="T829" id="Seg_645" s="T828">else</ta>
            <ta e="T830" id="Seg_646" s="T829">NEG.EX</ta>
            <ta e="T831" id="Seg_647" s="T830">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T762" id="Seg_648" s="T761">лиса.[NOM.SG]</ta>
            <ta e="T763" id="Seg_649" s="T762">один.[NOM.SG]</ta>
            <ta e="T764" id="Seg_650" s="T763">мужчина-LAT/LOC.3SG</ta>
            <ta e="T765" id="Seg_651" s="T764">идти-PST.[3SG]</ta>
            <ta e="T766" id="Seg_652" s="T765">курица-PL</ta>
            <ta e="T767" id="Seg_653" s="T766">украсть-INF.LAT</ta>
            <ta e="T768" id="Seg_654" s="T767">тогда</ta>
            <ta e="T769" id="Seg_655" s="T768">этот.[NOM.SG]</ta>
            <ta e="T770" id="Seg_656" s="T769">вешать-PST.[3SG]</ta>
            <ta e="T771" id="Seg_657" s="T770">котел.[NOM.SG]</ta>
            <ta e="T772" id="Seg_658" s="T771">котел.[NOM.SG]</ta>
            <ta e="T773" id="Seg_659" s="T772">всегда</ta>
            <ta e="T774" id="Seg_660" s="T773">%%-DUR</ta>
            <ta e="T775" id="Seg_661" s="T774">а</ta>
            <ta e="T776" id="Seg_662" s="T775">этот.[NOM.SG]</ta>
            <ta e="T777" id="Seg_663" s="T776">сказать-IPFVZ.[3SG]</ta>
            <ta e="T778" id="Seg_664" s="T777">кто.[NOM.SG]</ta>
            <ta e="T779" id="Seg_665" s="T778">там</ta>
            <ta e="T780" id="Seg_666" s="T779">%%-DUR</ta>
            <ta e="T781" id="Seg_667" s="T780">прийти-PST.[3SG]</ta>
            <ta e="T782" id="Seg_668" s="T781">этот.[NOM.SG]</ta>
            <ta e="T783" id="Seg_669" s="T782">котел.[NOM.SG]</ta>
            <ta e="T784" id="Seg_670" s="T783">этот.[NOM.SG]</ta>
            <ta e="T785" id="Seg_671" s="T784">этот-ACC</ta>
            <ta e="T786" id="Seg_672" s="T785">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T787" id="Seg_673" s="T786">взять-PST.[3SG]</ta>
            <ta e="T788" id="Seg_674" s="T787">вешать-PST.[3SG]</ta>
            <ta e="T789" id="Seg_675" s="T788">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T790" id="Seg_676" s="T789">сам-LAT/LOC.3SG</ta>
            <ta e="T791" id="Seg_677" s="T790">голова-LAT/LOC.3SG</ta>
            <ta e="T792" id="Seg_678" s="T791">пойти-PST.[3SG]</ta>
            <ta e="T794" id="Seg_679" s="T793">нести-RES-PST.[3SG]</ta>
            <ta e="T795" id="Seg_680" s="T794">прорубь-LAT</ta>
            <ta e="T796" id="Seg_681" s="T795">послать-MOM-PST.[3SG]</ta>
            <ta e="T797" id="Seg_682" s="T796">котел-LAT</ta>
            <ta e="T798" id="Seg_683" s="T797">много</ta>
            <ta e="T799" id="Seg_684" s="T798">вода.[NOM.SG]</ta>
            <ta e="T800" id="Seg_685" s="T799">течь-PST.[3SG]</ta>
            <ta e="T801" id="Seg_686" s="T800">тогда</ta>
            <ta e="T802" id="Seg_687" s="T801">котел.[NOM.SG]</ta>
            <ta e="T803" id="Seg_688" s="T802">там</ta>
            <ta e="T804" id="Seg_689" s="T803">сидеть-DUR.[3SG]</ta>
            <ta e="T805" id="Seg_690" s="T804">а</ta>
            <ta e="T806" id="Seg_691" s="T805">этот.[NOM.SG]</ta>
            <ta e="T807" id="Seg_692" s="T806">сказать-IPFVZ.[3SG]</ta>
            <ta e="T808" id="Seg_693" s="T807">я.ACC</ta>
            <ta e="T810" id="Seg_694" s="T809">там</ta>
            <ta e="T811" id="Seg_695" s="T810">NEG.AUX-IMP.2SG</ta>
            <ta e="T812" id="Seg_696" s="T811">нести-EP-CNG</ta>
            <ta e="T813" id="Seg_697" s="T812">я.NOM</ta>
            <ta e="T814" id="Seg_698" s="T813">%%</ta>
            <ta e="T815" id="Seg_699" s="T814">вдоль</ta>
            <ta e="T816" id="Seg_700" s="T815">делать-PST-1SG</ta>
            <ta e="T817" id="Seg_701" s="T816">тогда</ta>
            <ta e="T818" id="Seg_702" s="T817">котел.[NOM.SG]</ta>
            <ta e="T819" id="Seg_703" s="T818">пойти-PST.[3SG]</ta>
            <ta e="T820" id="Seg_704" s="T819">вода-LAT</ta>
            <ta e="T821" id="Seg_705" s="T820">и</ta>
            <ta e="T822" id="Seg_706" s="T821">лиса.[NOM.SG]</ta>
            <ta e="T823" id="Seg_707" s="T822">два-COLL</ta>
            <ta e="T824" id="Seg_708" s="T823">PTCL</ta>
            <ta e="T825" id="Seg_709" s="T824">умереть-RES-PST-3PL</ta>
            <ta e="T826" id="Seg_710" s="T825">этот.[NOM.SG]</ta>
            <ta e="T827" id="Seg_711" s="T826">вода-LOC</ta>
            <ta e="T828" id="Seg_712" s="T827">и</ta>
            <ta e="T829" id="Seg_713" s="T828">еще</ta>
            <ta e="T830" id="Seg_714" s="T829">NEG.EX</ta>
            <ta e="T831" id="Seg_715" s="T830">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T762" id="Seg_716" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_717" s="T762">num-n:case</ta>
            <ta e="T764" id="Seg_718" s="T763">n-n:case.poss</ta>
            <ta e="T765" id="Seg_719" s="T764">v-v:tense-v:pn</ta>
            <ta e="T766" id="Seg_720" s="T765">n-n:num</ta>
            <ta e="T767" id="Seg_721" s="T766">v-v:n.fin</ta>
            <ta e="T768" id="Seg_722" s="T767">adv</ta>
            <ta e="T769" id="Seg_723" s="T768">dempro-n:case</ta>
            <ta e="T770" id="Seg_724" s="T769">v-v:tense-v:pn</ta>
            <ta e="T771" id="Seg_725" s="T770">n-n:case</ta>
            <ta e="T772" id="Seg_726" s="T771">n-n:case</ta>
            <ta e="T773" id="Seg_727" s="T772">adv</ta>
            <ta e="T774" id="Seg_728" s="T773">v-v&gt;v</ta>
            <ta e="T775" id="Seg_729" s="T774">conj</ta>
            <ta e="T776" id="Seg_730" s="T775">dempro-n:case</ta>
            <ta e="T777" id="Seg_731" s="T776">v-v&gt;v-v:pn</ta>
            <ta e="T778" id="Seg_732" s="T777">que-n:case</ta>
            <ta e="T779" id="Seg_733" s="T778">adv</ta>
            <ta e="T780" id="Seg_734" s="T779">v-v&gt;v</ta>
            <ta e="T781" id="Seg_735" s="T780">v-v:tense-v:pn</ta>
            <ta e="T782" id="Seg_736" s="T781">dempro-n:case</ta>
            <ta e="T783" id="Seg_737" s="T782">n-n:case</ta>
            <ta e="T784" id="Seg_738" s="T783">dempro-n:case</ta>
            <ta e="T785" id="Seg_739" s="T784">dempro-n:case</ta>
            <ta e="T786" id="Seg_740" s="T785">n-n:case.poss</ta>
            <ta e="T787" id="Seg_741" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_742" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_743" s="T788">refl-n:case.poss</ta>
            <ta e="T790" id="Seg_744" s="T789">refl-n:case.poss</ta>
            <ta e="T791" id="Seg_745" s="T790">n-n:case.poss</ta>
            <ta e="T792" id="Seg_746" s="T791">v-v:tense-v:pn</ta>
            <ta e="T794" id="Seg_747" s="T793">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T795" id="Seg_748" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_749" s="T795">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_750" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_751" s="T797">quant</ta>
            <ta e="T799" id="Seg_752" s="T798">n-n:case</ta>
            <ta e="T800" id="Seg_753" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_754" s="T800">adv</ta>
            <ta e="T802" id="Seg_755" s="T801">n-n:case</ta>
            <ta e="T803" id="Seg_756" s="T802">adv</ta>
            <ta e="T804" id="Seg_757" s="T803">v-v&gt;v-v:pn</ta>
            <ta e="T805" id="Seg_758" s="T804">conj</ta>
            <ta e="T806" id="Seg_759" s="T805">dempro-n:case</ta>
            <ta e="T807" id="Seg_760" s="T806">v-v&gt;v-v:pn</ta>
            <ta e="T808" id="Seg_761" s="T807">pers</ta>
            <ta e="T810" id="Seg_762" s="T809">adv</ta>
            <ta e="T811" id="Seg_763" s="T810">aux-v:mood.pn</ta>
            <ta e="T812" id="Seg_764" s="T811">v-v:ins-v:n.fin</ta>
            <ta e="T813" id="Seg_765" s="T812">pers</ta>
            <ta e="T814" id="Seg_766" s="T813">%%</ta>
            <ta e="T815" id="Seg_767" s="T814">post</ta>
            <ta e="T816" id="Seg_768" s="T815">v-v:tense-v:pn</ta>
            <ta e="T817" id="Seg_769" s="T816">adv</ta>
            <ta e="T818" id="Seg_770" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_771" s="T818">v-v:tense-v:pn</ta>
            <ta e="T820" id="Seg_772" s="T819">n-n:case</ta>
            <ta e="T821" id="Seg_773" s="T820">conj</ta>
            <ta e="T822" id="Seg_774" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_775" s="T822">num-num&gt;num</ta>
            <ta e="T824" id="Seg_776" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_777" s="T824">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T826" id="Seg_778" s="T825">dempro-n:case</ta>
            <ta e="T827" id="Seg_779" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_780" s="T827">conj</ta>
            <ta e="T829" id="Seg_781" s="T828">adv</ta>
            <ta e="T830" id="Seg_782" s="T829">v</ta>
            <ta e="T831" id="Seg_783" s="T830">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T762" id="Seg_784" s="T761">n</ta>
            <ta e="T763" id="Seg_785" s="T762">num</ta>
            <ta e="T764" id="Seg_786" s="T763">n</ta>
            <ta e="T765" id="Seg_787" s="T764">v</ta>
            <ta e="T766" id="Seg_788" s="T765">n</ta>
            <ta e="T767" id="Seg_789" s="T766">v</ta>
            <ta e="T768" id="Seg_790" s="T767">adv</ta>
            <ta e="T769" id="Seg_791" s="T768">dempro</ta>
            <ta e="T770" id="Seg_792" s="T769">v</ta>
            <ta e="T771" id="Seg_793" s="T770">n</ta>
            <ta e="T772" id="Seg_794" s="T771">n</ta>
            <ta e="T773" id="Seg_795" s="T772">adv</ta>
            <ta e="T774" id="Seg_796" s="T773">v</ta>
            <ta e="T775" id="Seg_797" s="T774">conj</ta>
            <ta e="T776" id="Seg_798" s="T775">dempro</ta>
            <ta e="T777" id="Seg_799" s="T776">v</ta>
            <ta e="T778" id="Seg_800" s="T777">que</ta>
            <ta e="T779" id="Seg_801" s="T778">adv</ta>
            <ta e="T780" id="Seg_802" s="T779">v</ta>
            <ta e="T781" id="Seg_803" s="T780">v</ta>
            <ta e="T782" id="Seg_804" s="T781">dempro</ta>
            <ta e="T783" id="Seg_805" s="T782">n</ta>
            <ta e="T784" id="Seg_806" s="T783">dempro</ta>
            <ta e="T785" id="Seg_807" s="T784">dempro</ta>
            <ta e="T786" id="Seg_808" s="T785">n</ta>
            <ta e="T787" id="Seg_809" s="T786">v</ta>
            <ta e="T788" id="Seg_810" s="T787">v</ta>
            <ta e="T789" id="Seg_811" s="T788">refl</ta>
            <ta e="T790" id="Seg_812" s="T789">refl</ta>
            <ta e="T791" id="Seg_813" s="T790">n</ta>
            <ta e="T792" id="Seg_814" s="T791">v</ta>
            <ta e="T794" id="Seg_815" s="T793">v</ta>
            <ta e="T795" id="Seg_816" s="T794">n</ta>
            <ta e="T796" id="Seg_817" s="T795">v</ta>
            <ta e="T797" id="Seg_818" s="T796">n</ta>
            <ta e="T798" id="Seg_819" s="T797">quant</ta>
            <ta e="T799" id="Seg_820" s="T798">n</ta>
            <ta e="T800" id="Seg_821" s="T799">v</ta>
            <ta e="T801" id="Seg_822" s="T800">adv</ta>
            <ta e="T802" id="Seg_823" s="T801">n</ta>
            <ta e="T803" id="Seg_824" s="T802">adv</ta>
            <ta e="T804" id="Seg_825" s="T803">v</ta>
            <ta e="T805" id="Seg_826" s="T804">conj</ta>
            <ta e="T806" id="Seg_827" s="T805">dempro</ta>
            <ta e="T807" id="Seg_828" s="T806">v</ta>
            <ta e="T808" id="Seg_829" s="T807">pers</ta>
            <ta e="T810" id="Seg_830" s="T809">adv</ta>
            <ta e="T811" id="Seg_831" s="T810">aux</ta>
            <ta e="T812" id="Seg_832" s="T811">v</ta>
            <ta e="T813" id="Seg_833" s="T812">pers</ta>
            <ta e="T815" id="Seg_834" s="T814">post</ta>
            <ta e="T816" id="Seg_835" s="T815">v</ta>
            <ta e="T817" id="Seg_836" s="T816">adv</ta>
            <ta e="T818" id="Seg_837" s="T817">n</ta>
            <ta e="T819" id="Seg_838" s="T818">v</ta>
            <ta e="T820" id="Seg_839" s="T819">n</ta>
            <ta e="T821" id="Seg_840" s="T820">conj</ta>
            <ta e="T822" id="Seg_841" s="T821">n</ta>
            <ta e="T823" id="Seg_842" s="T822">num</ta>
            <ta e="T824" id="Seg_843" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_844" s="T824">v</ta>
            <ta e="T826" id="Seg_845" s="T825">dempro</ta>
            <ta e="T827" id="Seg_846" s="T826">n</ta>
            <ta e="T828" id="Seg_847" s="T827">conj</ta>
            <ta e="T829" id="Seg_848" s="T828">adv</ta>
            <ta e="T830" id="Seg_849" s="T829">v</ta>
            <ta e="T831" id="Seg_850" s="T830">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T762" id="Seg_851" s="T761">np.h:A</ta>
            <ta e="T764" id="Seg_852" s="T763">np:G</ta>
            <ta e="T766" id="Seg_853" s="T765">np:Th</ta>
            <ta e="T768" id="Seg_854" s="T767">adv:Time</ta>
            <ta e="T769" id="Seg_855" s="T768">pro.h:A</ta>
            <ta e="T771" id="Seg_856" s="T770">np.h:Th</ta>
            <ta e="T772" id="Seg_857" s="T771">pro:A</ta>
            <ta e="T776" id="Seg_858" s="T775">pro.h:A</ta>
            <ta e="T778" id="Seg_859" s="T777">pro.h:A</ta>
            <ta e="T782" id="Seg_860" s="T781">pro.h:A</ta>
            <ta e="T784" id="Seg_861" s="T783">pro.h:A</ta>
            <ta e="T785" id="Seg_862" s="T784">pro.h:Poss</ta>
            <ta e="T786" id="Seg_863" s="T785">np.h:Th</ta>
            <ta e="T788" id="Seg_864" s="T787">0.3.h:A</ta>
            <ta e="T790" id="Seg_865" s="T789">pro.h:Poss</ta>
            <ta e="T791" id="Seg_866" s="T790">np:Th</ta>
            <ta e="T792" id="Seg_867" s="T791">0.3.h:A</ta>
            <ta e="T794" id="Seg_868" s="T793">0.3.h:A</ta>
            <ta e="T795" id="Seg_869" s="T794">np:G</ta>
            <ta e="T796" id="Seg_870" s="T795">0.3.h:A</ta>
            <ta e="T797" id="Seg_871" s="T796">np:G</ta>
            <ta e="T799" id="Seg_872" s="T798">np:Th</ta>
            <ta e="T801" id="Seg_873" s="T800">adv:Time</ta>
            <ta e="T802" id="Seg_874" s="T801">np.h:E</ta>
            <ta e="T803" id="Seg_875" s="T802">adv:L</ta>
            <ta e="T806" id="Seg_876" s="T805">pro.h:A</ta>
            <ta e="T808" id="Seg_877" s="T807">pro.h:Th</ta>
            <ta e="T810" id="Seg_878" s="T809">adv:G</ta>
            <ta e="T811" id="Seg_879" s="T810">0.2.h:A</ta>
            <ta e="T813" id="Seg_880" s="T812">pro.h:A</ta>
            <ta e="T817" id="Seg_881" s="T816">adv:Time</ta>
            <ta e="T818" id="Seg_882" s="T817">np.h:A</ta>
            <ta e="T820" id="Seg_883" s="T819">np:G</ta>
            <ta e="T825" id="Seg_884" s="T824">0.3.h:P</ta>
            <ta e="T827" id="Seg_885" s="T826">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T762" id="Seg_886" s="T761">np.h:S</ta>
            <ta e="T765" id="Seg_887" s="T764">v:pred</ta>
            <ta e="T766" id="Seg_888" s="T765">np:O</ta>
            <ta e="T767" id="Seg_889" s="T766">s:purp</ta>
            <ta e="T769" id="Seg_890" s="T768">pro.h:S</ta>
            <ta e="T770" id="Seg_891" s="T769">v:pred</ta>
            <ta e="T771" id="Seg_892" s="T770">np.h:O</ta>
            <ta e="T772" id="Seg_893" s="T771">np.h:S</ta>
            <ta e="T776" id="Seg_894" s="T775">pro.h:S</ta>
            <ta e="T777" id="Seg_895" s="T776">v:pred</ta>
            <ta e="T780" id="Seg_896" s="T779">v:pred</ta>
            <ta e="T781" id="Seg_897" s="T780">v:pred</ta>
            <ta e="T782" id="Seg_898" s="T781">pro.h:S</ta>
            <ta e="T784" id="Seg_899" s="T783">pro.h:S</ta>
            <ta e="T786" id="Seg_900" s="T785">np.h:O</ta>
            <ta e="T787" id="Seg_901" s="T786">v:pred</ta>
            <ta e="T788" id="Seg_902" s="T787">v:pred 0.3.h:S</ta>
            <ta e="T792" id="Seg_903" s="T791">v:pred 0.3.h:S</ta>
            <ta e="T794" id="Seg_904" s="T793">v:pred 0.3.h:S</ta>
            <ta e="T796" id="Seg_905" s="T795">v:pred 0.3.h:S</ta>
            <ta e="T799" id="Seg_906" s="T798">np:S</ta>
            <ta e="T800" id="Seg_907" s="T799">v:pred</ta>
            <ta e="T802" id="Seg_908" s="T801">np.h:S</ta>
            <ta e="T804" id="Seg_909" s="T803">v:pred</ta>
            <ta e="T806" id="Seg_910" s="T805">pro.h:S</ta>
            <ta e="T807" id="Seg_911" s="T806">v:pred</ta>
            <ta e="T808" id="Seg_912" s="T807">pro.h:O</ta>
            <ta e="T811" id="Seg_913" s="T810">v:pred 0.2.h:S</ta>
            <ta e="T813" id="Seg_914" s="T812">pro.h:S</ta>
            <ta e="T816" id="Seg_915" s="T815">v:pred</ta>
            <ta e="T818" id="Seg_916" s="T817">np.h:S</ta>
            <ta e="T819" id="Seg_917" s="T818">v:pred</ta>
            <ta e="T825" id="Seg_918" s="T824">v:pred 0.3.h:S</ta>
            <ta e="T830" id="Seg_919" s="T829">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T762" id="Seg_920" s="T761">RUS:cult</ta>
            <ta e="T766" id="Seg_921" s="T765">RUS:cult</ta>
            <ta e="T775" id="Seg_922" s="T774">RUS:gram</ta>
            <ta e="T795" id="Seg_923" s="T794">RUS:core</ta>
            <ta e="T805" id="Seg_924" s="T804">RUS:gram</ta>
            <ta e="T821" id="Seg_925" s="T820">RUS:gram</ta>
            <ta e="T822" id="Seg_926" s="T821">RUS:cult</ta>
            <ta e="T824" id="Seg_927" s="T823">TURK:disc</ta>
            <ta e="T828" id="Seg_928" s="T827">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T767" id="Seg_929" s="T761">Лиса пришла к одному мужику кур красть.</ta>
            <ta e="T771" id="Seg_930" s="T767">[Потом] он подвесил котёл.</ta>
            <ta e="T774" id="Seg_931" s="T771">Котёл всё время (гудит?).</ta>
            <ta e="T780" id="Seg_932" s="T774">А она говорит: «Кто там (гудит?)?»</ta>
            <ta e="T787" id="Seg_933" s="T780">Пришла, взяла этот котёл.</ta>
            <ta e="T792" id="Seg_934" s="T787">Повесила себе на голову, пошла.</ta>
            <ta e="T796" id="Seg_935" s="T792">Принесла к проруби, опустила.</ta>
            <ta e="T800" id="Seg_936" s="T796">В котёл много воды натекло.</ta>
            <ta e="T804" id="Seg_937" s="T800">Потом котёл там сидит.</ta>
            <ta e="T812" id="Seg_938" s="T804">А она говорит: «Не забирай меня туда!</ta>
            <ta e="T816" id="Seg_939" s="T812">Это я просто так сделала».</ta>
            <ta e="T827" id="Seg_940" s="T816">Потом котёл в воду ушёл, и лиса, вдвоём они умерли в этой реке.</ta>
            <ta e="T830" id="Seg_941" s="T827">И больше ничего.</ta>
            <ta e="T831" id="Seg_942" s="T830">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T767" id="Seg_943" s="T761">The fox went to a man to steal chickens.</ta>
            <ta e="T771" id="Seg_944" s="T767">Then he hung a cauldron.</ta>
            <ta e="T774" id="Seg_945" s="T771">The cauldron is always (rumbling?).</ta>
            <ta e="T780" id="Seg_946" s="T774">And he says: "Who is rumbling here?"</ta>
            <ta e="T787" id="Seg_947" s="T780">He came, he took [the man’s] cauldron.</ta>
            <ta e="T792" id="Seg_948" s="T787">Hung [it] to his own head, went.</ta>
            <ta e="T796" id="Seg_949" s="T792">It took it to the ice-hole and put it [there].</ta>
            <ta e="T800" id="Seg_950" s="T796">A lot of water flowed in the kettle.</ta>
            <ta e="T804" id="Seg_951" s="T800">Then the cauldron is sitting there.</ta>
            <ta e="T812" id="Seg_952" s="T804">But he says: "Do not take me there!</ta>
            <ta e="T816" id="Seg_953" s="T812">I made i just so." [?]</ta>
            <ta e="T827" id="Seg_954" s="T816">Then the cauldron went to the river and the fox, two of them died in this river.</ta>
            <ta e="T830" id="Seg_955" s="T827">And there is nothing else.</ta>
            <ta e="T831" id="Seg_956" s="T830">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T767" id="Seg_957" s="T761">Der Fuchs ging zu einem Mann um Hühner zu stehlen.</ta>
            <ta e="T771" id="Seg_958" s="T767">Dann hing er ein Kessel.</ta>
            <ta e="T774" id="Seg_959" s="T771">Der Kessel (brummt?) immer.</ta>
            <ta e="T780" id="Seg_960" s="T774">Und er sagt: „Wer brummt hier?“</ta>
            <ta e="T787" id="Seg_961" s="T780">Er kam, er nahm sein Kessel. </ta>
            <ta e="T792" id="Seg_962" s="T787">Hing [ihn] über den eigenen Kopf, ging.</ta>
            <ta e="T796" id="Seg_963" s="T792">Er nahm ihn zu dem Eisloch, und setzte ihn [dort] ab.</ta>
            <ta e="T800" id="Seg_964" s="T796">Viel Wasser floss in den Kessel.</ta>
            <ta e="T804" id="Seg_965" s="T800">Dann sitzt der Kessel da.</ta>
            <ta e="T812" id="Seg_966" s="T804">Aber er sagt: „Nimm mich nicht dahin!</ta>
            <ta e="T816" id="Seg_967" s="T812">Ich habe es gerade so geschafft.“ [?]</ta>
            <ta e="T827" id="Seg_968" s="T816">Dann ging der Kessel in den Fluss, und der Fuchs, die zwei kamen in diesem Fluss um.</ta>
            <ta e="T830" id="Seg_969" s="T827">Und es ist nichts weiteres.</ta>
            <ta e="T831" id="Seg_970" s="T830">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T767" id="Seg_971" s="T761">[GVY:] This is one version of the tale "A fox and a jug", see this version here: http://narodstory.net/russkie-skazki.php?id=39</ta>
            <ta e="T816" id="Seg_972" s="T812">[GVY:] Or '"to a river", if tʼora is t'axa 'river'?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
