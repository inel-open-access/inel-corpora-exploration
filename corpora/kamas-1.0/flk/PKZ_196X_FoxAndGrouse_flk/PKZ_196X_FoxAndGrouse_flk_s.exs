<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1A7CBD24-95FC-9532-998D-1BB64CBE1C9A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndGrouse_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndGrouse_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndGrouse_flk\PKZ_196X_FoxAndGrouse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">126</ud-information>
            <ud-information attribute-name="# HIAT:w">84</ud-information>
            <ud-information attribute-name="# e">83</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T948" time="0.014" type="appl" />
         <tli id="T949" time="0.666" type="appl" />
         <tli id="T950" time="1.317" type="appl" />
         <tli id="T951" time="1.969" type="appl" />
         <tli id="T952" time="2.62" type="appl" />
         <tli id="T953" time="3.7530205933612533" />
         <tli id="T954" time="4.362" type="appl" />
         <tli id="T955" time="5.036" type="appl" />
         <tli id="T956" time="5.71" type="appl" />
         <tli id="T957" time="6.384" type="appl" />
         <tli id="T958" time="7.057" type="appl" />
         <tli id="T959" time="7.731" type="appl" />
         <tli id="T960" time="8.405" type="appl" />
         <tli id="T961" time="9.079" type="appl" />
         <tli id="T962" time="9.702" type="appl" />
         <tli id="T963" time="10.324" type="appl" />
         <tli id="T964" time="10.946" type="appl" />
         <tli id="T965" time="11.569" type="appl" />
         <tli id="T966" time="12.192" type="appl" />
         <tli id="T967" time="13.02558124232307" />
         <tli id="T968" time="13.575" type="appl" />
         <tli id="T969" time="14.337" type="appl" />
         <tli id="T970" time="15.878676826619014" />
         <tli id="T971" time="16.793" type="appl" />
         <tli id="T972" time="17.737" type="appl" />
         <tli id="T973" time="18.68" type="appl" />
         <tli id="T974" time="19.624" type="appl" />
         <tli id="T975" time="20.567" type="appl" />
         <tli id="T976" time="21.511" type="appl" />
         <tli id="T977" time="22.454" type="appl" />
         <tli id="T978" time="23.398" type="appl" />
         <tli id="T979" time="24.341" type="appl" />
         <tli id="T980" time="24.93" type="appl" />
         <tli id="T981" time="25.402" type="appl" />
         <tli id="T982" time="25.874" type="appl" />
         <tli id="T983" time="26.346" type="appl" />
         <tli id="T984" time="26.818" type="appl" />
         <tli id="T985" time="27.834" type="appl" />
         <tli id="T986" time="28.849" type="appl" />
         <tli id="T987" time="29.864" type="appl" />
         <tli id="T988" time="30.88" type="appl" />
         <tli id="T989" time="31.886" type="appl" />
         <tli id="T990" time="33.54387144901212" />
         <tli id="T991" time="34.201" type="appl" />
         <tli id="T992" time="34.758" type="appl" />
         <tli id="T993" time="35.316" type="appl" />
         <tli id="T994" time="35.873" type="appl" />
         <tli id="T995" time="36.43" type="appl" />
         <tli id="T996" time="37.27022759766034" />
         <tli id="T997" time="38.205" type="appl" />
         <tli id="T998" time="39.266" type="appl" />
         <tli id="T999" time="40.326" type="appl" />
         <tli id="T1000" time="41.386" type="appl" />
         <tli id="T1001" time="41.967" type="appl" />
         <tli id="T1002" time="42.549" type="appl" />
         <tli id="T1003" time="43.13" type="appl" />
         <tli id="T1004" time="44.71627378377849" />
         <tli id="T1005" time="45.274" type="appl" />
         <tli id="T1006" time="45.823" type="appl" />
         <tli id="T1007" time="46.373" type="appl" />
         <tli id="T1008" time="46.922" type="appl" />
         <tli id="T1009" time="47.65" type="appl" />
         <tli id="T1010" time="48.378" type="appl" />
         <tli id="T1011" time="49.105" type="appl" />
         <tli id="T1012" time="50.64911273243127" />
         <tli id="T1013" time="51.393" type="appl" />
         <tli id="T1014" time="51.976" type="appl" />
         <tli id="T1015" time="52.559" type="appl" />
         <tli id="T1016" time="53.142" type="appl" />
         <tli id="T1017" time="53.725" type="appl" />
         <tli id="T1018" time="54.308" type="appl" />
         <tli id="T1019" time="54.892" type="appl" />
         <tli id="T1020" time="55.475" type="appl" />
         <tli id="T1021" time="56.058" type="appl" />
         <tli id="T1022" time="56.641" type="appl" />
         <tli id="T1023" time="57.224" type="appl" />
         <tli id="T1024" time="57.807" type="appl" />
         <tli id="T1025" time="58.933" type="appl" />
         <tli id="T1026" time="60.42829783094096" />
         <tli id="T1027" time="61.104" type="appl" />
         <tli id="T1028" time="61.613" type="appl" />
         <tli id="T1029" time="62.122" type="appl" />
         <tli id="T1030" time="62.631" type="appl" />
         <tli id="T1031" time="63.768" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T980" start="T979">
            <tli id="T979.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1031" id="Seg_0" n="sc" s="T948">
               <ts e="T953" id="Seg_2" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_4" n="HIAT:w" s="T948">Šidegöʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T950" id="Seg_8" n="HIAT:w" s="T949">i</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_12" n="HIAT:w" s="T950">lisitsa</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_15" n="HIAT:w" s="T951">i</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_18" n="HIAT:w" s="T952">tʼetʼerʼev</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T961" id="Seg_22" n="HIAT:u" s="T953">
                  <ts e="T954" id="Seg_24" n="HIAT:w" s="T953">Lisitsa</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_27" n="HIAT:w" s="T954">dĭʔnə</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_30" n="HIAT:w" s="T955">măndə:</ts>
                  <nts id="Seg_31" n="HIAT:ip">"</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_34" n="HIAT:w" s="T956">Šoʔ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <ts e="T958" id="Seg_38" n="HIAT:w" s="T957">dunə-</ts>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_42" n="HIAT:w" s="T958">dʼünə</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_46" n="HIAT:w" s="T959">nʼergöʔ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_49" n="HIAT:w" s="T960">döbər</ts>
                  <nts id="Seg_50" n="HIAT:ip">!</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T967" id="Seg_53" n="HIAT:u" s="T961">
                  <ts e="T962" id="Seg_55" n="HIAT:w" s="T961">Tănziʔ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_58" n="HIAT:w" s="T962">dʼăbaktərlubaʔ</ts>
                  <nts id="Seg_59" n="HIAT:ip">,</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">(</nts>
                  <ts e="T964" id="Seg_63" n="HIAT:w" s="T963">măna</ts>
                  <nts id="Seg_64" n="HIAT:ip">)</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_67" n="HIAT:w" s="T964">tüj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_70" n="HIAT:w" s="T965">ej</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_73" n="HIAT:w" s="T966">nünniem</ts>
                  <nts id="Seg_74" n="HIAT:ip">!</nts>
                  <nts id="Seg_75" n="HIAT:ip">"</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T970" id="Seg_78" n="HIAT:u" s="T967">
                  <nts id="Seg_79" n="HIAT:ip">"</nts>
                  <ts e="T968" id="Seg_81" n="HIAT:w" s="T967">Dʼok</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_85" n="HIAT:w" s="T968">măn</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_88" n="HIAT:w" s="T969">pimniem</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T979" id="Seg_92" n="HIAT:u" s="T970">
                  <ts e="T971" id="Seg_94" n="HIAT:w" s="T970">Dʼügən</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_97" n="HIAT:w" s="T971">bar</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_100" n="HIAT:w" s="T972">mĭngeʔi</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_103" n="HIAT:w" s="T973">menʔi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_106" n="HIAT:w" s="T974">da</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_109" n="HIAT:w" s="T975">vsʼakɨj</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_112" n="HIAT:w" s="T976">zveriʔi</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_115" n="HIAT:w" s="T977">măna</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_118" n="HIAT:w" s="T978">amnuʔbələʔjə</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T984" id="Seg_122" n="HIAT:u" s="T979">
                  <ts e="T979.tx.1" id="Seg_124" n="HIAT:w" s="T979">A</ts>
                  <nts id="Seg_125" n="HIAT:ip">_</nts>
                  <ts e="T980" id="Seg_127" n="HIAT:w" s="T979.tx.1">to</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_130" n="HIAT:w" s="T980">tăn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_133" n="HIAT:w" s="T981">iššo</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_136" n="HIAT:w" s="T982">măna</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_139" n="HIAT:w" s="T983">amnal</ts>
                  <nts id="Seg_140" n="HIAT:ip">"</nts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T988" id="Seg_144" n="HIAT:u" s="T984">
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <ts e="T985" id="Seg_147" n="HIAT:w" s="T984">Dʼok</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_151" n="HIAT:w" s="T985">tüj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_154" n="HIAT:w" s="T986">ej</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_157" n="HIAT:w" s="T987">dʼabərolaʔbəʔjə</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T990" id="Seg_161" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_163" n="HIAT:w" s="T988">Jakšə</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_166" n="HIAT:w" s="T989">amnolaʔbəʔjə</ts>
                  <nts id="Seg_167" n="HIAT:ip">"</nts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T996" id="Seg_171" n="HIAT:u" s="T990">
                  <ts e="T991" id="Seg_173" n="HIAT:w" s="T990">A</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_176" n="HIAT:w" s="T991">dĭ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_179" n="HIAT:w" s="T992">măndə:</ts>
                  <nts id="Seg_180" n="HIAT:ip">"</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_183" n="HIAT:w" s="T993">Von</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_186" n="HIAT:w" s="T994">menzeŋ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_189" n="HIAT:w" s="T995">nuʔməlaʔbəʔjə</ts>
                  <nts id="Seg_190" n="HIAT:ip">"</nts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1000" id="Seg_194" n="HIAT:u" s="T996">
                  <ts e="T997" id="Seg_196" n="HIAT:w" s="T996">A</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_199" n="HIAT:w" s="T997">dĭ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_202" n="HIAT:w" s="T998">nuʔməsʼtə</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_205" n="HIAT:w" s="T999">xatʼel</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1004" id="Seg_209" n="HIAT:u" s="T1000">
                  <ts e="T1001" id="Seg_211" n="HIAT:w" s="T1000">Dĭ</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_214" n="HIAT:w" s="T1001">măndə:</ts>
                  <nts id="Seg_215" n="HIAT:ip">"</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_218" n="HIAT:w" s="T1002">Ĭmbi</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_221" n="HIAT:w" s="T1003">nuʔməlaʔbəl</ts>
                  <nts id="Seg_222" n="HIAT:ip">?</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1008" id="Seg_225" n="HIAT:u" s="T1004">
                  <ts e="T1005" id="Seg_227" n="HIAT:w" s="T1004">Tüj</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_230" n="HIAT:w" s="T1005">jakšə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_233" n="HIAT:w" s="T1006">bar</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_236" n="HIAT:w" s="T1007">amnolaʔbəʔjə</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1012" id="Seg_240" n="HIAT:u" s="T1008">
                  <nts id="Seg_241" n="HIAT:ip">(</nts>
                  <ts e="T1009" id="Seg_243" n="HIAT:w" s="T1008">Šindidə</ts>
                  <nts id="Seg_244" n="HIAT:ip">)</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_247" n="HIAT:w" s="T1009">šindim</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_250" n="HIAT:w" s="T1010">ej</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_253" n="HIAT:w" s="T1011">amnia</ts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1024" id="Seg_258" n="HIAT:u" s="T1012">
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <ts e="T1013" id="Seg_261" n="HIAT:w" s="T1012">A</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_264" n="HIAT:w" s="T1013">šindi</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_267" n="HIAT:w" s="T1014">tĭmniet</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_271" n="HIAT:w" s="T1015">možet</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_274" n="HIAT:w" s="T1016">dĭzeŋ</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_277" n="HIAT:w" s="T1017">ej</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_280" n="HIAT:w" s="T1018">tĭmneʔi</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_283" n="HIAT:w" s="T1019">što</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_286" n="HIAT:w" s="T1020">jakšə</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_289" n="HIAT:w" s="T1021">amnosʼtə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_292" n="HIAT:w" s="T1022">da</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_295" n="HIAT:w" s="T1023">kabarluʔjəʔ</ts>
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1026" id="Seg_300" n="HIAT:u" s="T1024">
                  <ts e="T1025" id="Seg_302" n="HIAT:w" s="T1024">Bostə</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_305" n="HIAT:w" s="T1025">nuʔməluʔpi</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1030" id="Seg_309" n="HIAT:u" s="T1026">
                  <ts e="T1027" id="Seg_311" n="HIAT:w" s="T1026">I</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_314" n="HIAT:w" s="T1027">dĭ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_317" n="HIAT:w" s="T1028">maːluʔpi</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_320" n="HIAT:w" s="T1029">unnʼa</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1031" id="Seg_324" n="HIAT:u" s="T1030">
                  <ts e="T1031" id="Seg_326" n="HIAT:w" s="T1030">Kabarləj</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1031" id="Seg_329" n="sc" s="T948">
               <ts e="T949" id="Seg_331" n="e" s="T948">Šidegöʔ </ts>
               <ts e="T950" id="Seg_333" n="e" s="T949">(i) </ts>
               <ts e="T951" id="Seg_335" n="e" s="T950">lisitsa </ts>
               <ts e="T952" id="Seg_337" n="e" s="T951">i </ts>
               <ts e="T953" id="Seg_339" n="e" s="T952">tʼetʼerʼev. </ts>
               <ts e="T954" id="Seg_341" n="e" s="T953">Lisitsa </ts>
               <ts e="T955" id="Seg_343" n="e" s="T954">dĭʔnə </ts>
               <ts e="T956" id="Seg_345" n="e" s="T955">măndə:" </ts>
               <ts e="T957" id="Seg_347" n="e" s="T956">Šoʔ </ts>
               <ts e="T958" id="Seg_349" n="e" s="T957">(dunə-) </ts>
               <ts e="T959" id="Seg_351" n="e" s="T958">dʼünə, </ts>
               <ts e="T960" id="Seg_353" n="e" s="T959">nʼergöʔ </ts>
               <ts e="T961" id="Seg_355" n="e" s="T960">döbər! </ts>
               <ts e="T962" id="Seg_357" n="e" s="T961">Tănziʔ </ts>
               <ts e="T963" id="Seg_359" n="e" s="T962">dʼăbaktərlubaʔ, </ts>
               <ts e="T964" id="Seg_361" n="e" s="T963">(măna) </ts>
               <ts e="T965" id="Seg_363" n="e" s="T964">tüj </ts>
               <ts e="T966" id="Seg_365" n="e" s="T965">ej </ts>
               <ts e="T967" id="Seg_367" n="e" s="T966">nünniem!" </ts>
               <ts e="T968" id="Seg_369" n="e" s="T967">"Dʼok, </ts>
               <ts e="T969" id="Seg_371" n="e" s="T968">măn </ts>
               <ts e="T970" id="Seg_373" n="e" s="T969">pimniem. </ts>
               <ts e="T971" id="Seg_375" n="e" s="T970">Dʼügən </ts>
               <ts e="T972" id="Seg_377" n="e" s="T971">bar </ts>
               <ts e="T973" id="Seg_379" n="e" s="T972">mĭngeʔi </ts>
               <ts e="T974" id="Seg_381" n="e" s="T973">menʔi </ts>
               <ts e="T975" id="Seg_383" n="e" s="T974">da </ts>
               <ts e="T976" id="Seg_385" n="e" s="T975">vsʼakɨj </ts>
               <ts e="T977" id="Seg_387" n="e" s="T976">zveriʔi </ts>
               <ts e="T978" id="Seg_389" n="e" s="T977">măna </ts>
               <ts e="T979" id="Seg_391" n="e" s="T978">amnuʔbələʔjə. </ts>
               <ts e="T980" id="Seg_393" n="e" s="T979">A_to </ts>
               <ts e="T981" id="Seg_395" n="e" s="T980">tăn </ts>
               <ts e="T982" id="Seg_397" n="e" s="T981">iššo </ts>
               <ts e="T983" id="Seg_399" n="e" s="T982">măna </ts>
               <ts e="T984" id="Seg_401" n="e" s="T983">amnal". </ts>
               <ts e="T985" id="Seg_403" n="e" s="T984">"Dʼok, </ts>
               <ts e="T986" id="Seg_405" n="e" s="T985">tüj </ts>
               <ts e="T987" id="Seg_407" n="e" s="T986">ej </ts>
               <ts e="T988" id="Seg_409" n="e" s="T987">dʼabərolaʔbəʔjə. </ts>
               <ts e="T989" id="Seg_411" n="e" s="T988">Jakšə </ts>
               <ts e="T990" id="Seg_413" n="e" s="T989">amnolaʔbəʔjə". </ts>
               <ts e="T991" id="Seg_415" n="e" s="T990">A </ts>
               <ts e="T992" id="Seg_417" n="e" s="T991">dĭ </ts>
               <ts e="T993" id="Seg_419" n="e" s="T992">măndə:" </ts>
               <ts e="T994" id="Seg_421" n="e" s="T993">Von </ts>
               <ts e="T995" id="Seg_423" n="e" s="T994">menzeŋ </ts>
               <ts e="T996" id="Seg_425" n="e" s="T995">nuʔməlaʔbəʔjə". </ts>
               <ts e="T997" id="Seg_427" n="e" s="T996">A </ts>
               <ts e="T998" id="Seg_429" n="e" s="T997">dĭ </ts>
               <ts e="T999" id="Seg_431" n="e" s="T998">nuʔməsʼtə </ts>
               <ts e="T1000" id="Seg_433" n="e" s="T999">xatʼel. </ts>
               <ts e="T1001" id="Seg_435" n="e" s="T1000">Dĭ </ts>
               <ts e="T1002" id="Seg_437" n="e" s="T1001">măndə:" </ts>
               <ts e="T1003" id="Seg_439" n="e" s="T1002">Ĭmbi </ts>
               <ts e="T1004" id="Seg_441" n="e" s="T1003">nuʔməlaʔbəl? </ts>
               <ts e="T1005" id="Seg_443" n="e" s="T1004">Tüj </ts>
               <ts e="T1006" id="Seg_445" n="e" s="T1005">jakšə </ts>
               <ts e="T1007" id="Seg_447" n="e" s="T1006">bar </ts>
               <ts e="T1008" id="Seg_449" n="e" s="T1007">amnolaʔbəʔjə. </ts>
               <ts e="T1009" id="Seg_451" n="e" s="T1008">(Šindidə) </ts>
               <ts e="T1010" id="Seg_453" n="e" s="T1009">šindim </ts>
               <ts e="T1011" id="Seg_455" n="e" s="T1010">ej </ts>
               <ts e="T1012" id="Seg_457" n="e" s="T1011">amnia". </ts>
               <ts e="T1013" id="Seg_459" n="e" s="T1012">"A </ts>
               <ts e="T1014" id="Seg_461" n="e" s="T1013">šindi </ts>
               <ts e="T1015" id="Seg_463" n="e" s="T1014">tĭmniet, </ts>
               <ts e="T1016" id="Seg_465" n="e" s="T1015">možet </ts>
               <ts e="T1017" id="Seg_467" n="e" s="T1016">dĭzeŋ </ts>
               <ts e="T1018" id="Seg_469" n="e" s="T1017">ej </ts>
               <ts e="T1019" id="Seg_471" n="e" s="T1018">tĭmneʔi </ts>
               <ts e="T1020" id="Seg_473" n="e" s="T1019">što </ts>
               <ts e="T1021" id="Seg_475" n="e" s="T1020">jakšə </ts>
               <ts e="T1022" id="Seg_477" n="e" s="T1021">amnosʼtə </ts>
               <ts e="T1023" id="Seg_479" n="e" s="T1022">da </ts>
               <ts e="T1024" id="Seg_481" n="e" s="T1023">kabarluʔjəʔ". </ts>
               <ts e="T1025" id="Seg_483" n="e" s="T1024">Bostə </ts>
               <ts e="T1026" id="Seg_485" n="e" s="T1025">nuʔməluʔpi. </ts>
               <ts e="T1027" id="Seg_487" n="e" s="T1026">I </ts>
               <ts e="T1028" id="Seg_489" n="e" s="T1027">dĭ </ts>
               <ts e="T1029" id="Seg_491" n="e" s="T1028">maːluʔpi </ts>
               <ts e="T1030" id="Seg_493" n="e" s="T1029">unnʼa. </ts>
               <ts e="T1031" id="Seg_495" n="e" s="T1030">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T953" id="Seg_496" s="T948">PKZ_196X_FoxAndGrouse_flk.001 (001)</ta>
            <ta e="T961" id="Seg_497" s="T953">PKZ_196X_FoxAndGrouse_flk.002 (002)</ta>
            <ta e="T967" id="Seg_498" s="T961">PKZ_196X_FoxAndGrouse_flk.003 (003)</ta>
            <ta e="T970" id="Seg_499" s="T967">PKZ_196X_FoxAndGrouse_flk.004 (004)</ta>
            <ta e="T979" id="Seg_500" s="T970">PKZ_196X_FoxAndGrouse_flk.005 (005)</ta>
            <ta e="T984" id="Seg_501" s="T979">PKZ_196X_FoxAndGrouse_flk.006 (006)</ta>
            <ta e="T988" id="Seg_502" s="T984">PKZ_196X_FoxAndGrouse_flk.007 (007)</ta>
            <ta e="T990" id="Seg_503" s="T988">PKZ_196X_FoxAndGrouse_flk.008 (008)</ta>
            <ta e="T996" id="Seg_504" s="T990">PKZ_196X_FoxAndGrouse_flk.009 (009)</ta>
            <ta e="T1000" id="Seg_505" s="T996">PKZ_196X_FoxAndGrouse_flk.010 (010)</ta>
            <ta e="T1004" id="Seg_506" s="T1000">PKZ_196X_FoxAndGrouse_flk.011 (011)</ta>
            <ta e="T1008" id="Seg_507" s="T1004">PKZ_196X_FoxAndGrouse_flk.012 (012)</ta>
            <ta e="T1012" id="Seg_508" s="T1008">PKZ_196X_FoxAndGrouse_flk.013 (013)</ta>
            <ta e="T1024" id="Seg_509" s="T1012">PKZ_196X_FoxAndGrouse_flk.014 (014)</ta>
            <ta e="T1026" id="Seg_510" s="T1024">PKZ_196X_FoxAndGrouse_flk.015 (015)</ta>
            <ta e="T1030" id="Seg_511" s="T1026">PKZ_196X_FoxAndGrouse_flk.016 (016)</ta>
            <ta e="T1031" id="Seg_512" s="T1030">PKZ_196X_FoxAndGrouse_flk.017 (017)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T953" id="Seg_513" s="T948">Šidegöʔ (i) lisitsa i tʼetʼerʼev. </ta>
            <ta e="T961" id="Seg_514" s="T953">Lisitsa dĭʔnə măndə:" Šoʔ (dunə-) dʼünə, nʼergöʔ döbər! </ta>
            <ta e="T967" id="Seg_515" s="T961">Tănziʔ dʼăbaktərlubaʔ, (măna) tüj ej nünniem!" </ta>
            <ta e="T970" id="Seg_516" s="T967">"Dʼok, măn pimniem. </ta>
            <ta e="T979" id="Seg_517" s="T970">Dʼügən bar mĭngeʔi menʔi da vsʼakɨj zveriʔi măna amnuʔbələʔjə. </ta>
            <ta e="T984" id="Seg_518" s="T979">A to tăn iššo măna amnal". </ta>
            <ta e="T988" id="Seg_519" s="T984">"Dʼok, tüj ej dʼabərolaʔbəʔjə. </ta>
            <ta e="T990" id="Seg_520" s="T988">Jakšə amnolaʔbəʔjə". </ta>
            <ta e="T996" id="Seg_521" s="T990">A dĭ măndə:" Von menzeŋ nuʔməlaʔbəʔjə". </ta>
            <ta e="T1000" id="Seg_522" s="T996">A dĭ nuʔməsʼtə xatʼel. </ta>
            <ta e="T1004" id="Seg_523" s="T1000">Dĭ măndə:" Ĭmbi nuʔməlaʔbəl? </ta>
            <ta e="T1008" id="Seg_524" s="T1004">Tüj jakšə bar amnolaʔbəʔjə. </ta>
            <ta e="T1012" id="Seg_525" s="T1008">(Šindidə) šindim ej amnia". </ta>
            <ta e="T1024" id="Seg_526" s="T1012">"A šindi tĭmniet, možet dĭzeŋ ej tĭmneʔi što jakšə amnosʼtə da kabarluʔjəʔ". </ta>
            <ta e="T1026" id="Seg_527" s="T1024">Bostə nuʔməluʔpi. </ta>
            <ta e="T1030" id="Seg_528" s="T1026">I dĭ maːluʔpi unnʼa. </ta>
            <ta e="T1031" id="Seg_529" s="T1030">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T949" id="Seg_530" s="T948">šide-göʔ</ta>
            <ta e="T950" id="Seg_531" s="T949">i</ta>
            <ta e="T951" id="Seg_532" s="T950">lisitsa</ta>
            <ta e="T952" id="Seg_533" s="T951">i</ta>
            <ta e="T953" id="Seg_534" s="T952">tʼetʼerʼev</ta>
            <ta e="T954" id="Seg_535" s="T953">lisitsa</ta>
            <ta e="T955" id="Seg_536" s="T954">dĭʔ-nə</ta>
            <ta e="T956" id="Seg_537" s="T955">măn-də</ta>
            <ta e="T957" id="Seg_538" s="T956">šo-ʔ</ta>
            <ta e="T959" id="Seg_539" s="T958">dʼü-nə</ta>
            <ta e="T960" id="Seg_540" s="T959">nʼergö-ʔ</ta>
            <ta e="T961" id="Seg_541" s="T960">döbər</ta>
            <ta e="T962" id="Seg_542" s="T961">tăn-ziʔ</ta>
            <ta e="T963" id="Seg_543" s="T962">dʼăbaktər-lu-baʔ</ta>
            <ta e="T964" id="Seg_544" s="T963">măna</ta>
            <ta e="T965" id="Seg_545" s="T964">tüj</ta>
            <ta e="T966" id="Seg_546" s="T965">ej</ta>
            <ta e="T967" id="Seg_547" s="T966">nün-nie-m</ta>
            <ta e="T968" id="Seg_548" s="T967">dʼok</ta>
            <ta e="T969" id="Seg_549" s="T968">măn</ta>
            <ta e="T970" id="Seg_550" s="T969">pim-nie-m</ta>
            <ta e="T971" id="Seg_551" s="T970">dʼü-gən</ta>
            <ta e="T972" id="Seg_552" s="T971">bar</ta>
            <ta e="T973" id="Seg_553" s="T972">mĭn-ge-ʔi</ta>
            <ta e="T974" id="Seg_554" s="T973">men-ʔi</ta>
            <ta e="T975" id="Seg_555" s="T974">da</ta>
            <ta e="T976" id="Seg_556" s="T975">vsʼakɨj</ta>
            <ta e="T977" id="Seg_557" s="T976">zveri-ʔi</ta>
            <ta e="T978" id="Seg_558" s="T977">măna</ta>
            <ta e="T979" id="Seg_559" s="T978">am-nuʔbə-lə-ʔjə</ta>
            <ta e="T980" id="Seg_560" s="T979">ato</ta>
            <ta e="T981" id="Seg_561" s="T980">tăn</ta>
            <ta e="T982" id="Seg_562" s="T981">iššo</ta>
            <ta e="T983" id="Seg_563" s="T982">măna</ta>
            <ta e="T984" id="Seg_564" s="T983">am-na-l</ta>
            <ta e="T985" id="Seg_565" s="T984">dʼok</ta>
            <ta e="T986" id="Seg_566" s="T985">tüj</ta>
            <ta e="T987" id="Seg_567" s="T986">ej</ta>
            <ta e="T988" id="Seg_568" s="T987">dʼabəro-laʔbə-ʔjə</ta>
            <ta e="T989" id="Seg_569" s="T988">jakšə</ta>
            <ta e="T990" id="Seg_570" s="T989">amno-laʔbə-ʔjə</ta>
            <ta e="T991" id="Seg_571" s="T990">a</ta>
            <ta e="T992" id="Seg_572" s="T991">dĭ</ta>
            <ta e="T993" id="Seg_573" s="T992">măn-də</ta>
            <ta e="T994" id="Seg_574" s="T993">von</ta>
            <ta e="T995" id="Seg_575" s="T994">men-zeŋ</ta>
            <ta e="T996" id="Seg_576" s="T995">nuʔmə-laʔbə-ʔjə</ta>
            <ta e="T997" id="Seg_577" s="T996">a</ta>
            <ta e="T998" id="Seg_578" s="T997">dĭ</ta>
            <ta e="T999" id="Seg_579" s="T998">nuʔmə-sʼtə</ta>
            <ta e="T1000" id="Seg_580" s="T999">xatʼel</ta>
            <ta e="T1001" id="Seg_581" s="T1000">dĭ</ta>
            <ta e="T1002" id="Seg_582" s="T1001">măn-də</ta>
            <ta e="T1003" id="Seg_583" s="T1002">ĭmbi</ta>
            <ta e="T1004" id="Seg_584" s="T1003">nuʔmə-laʔbə-l</ta>
            <ta e="T1005" id="Seg_585" s="T1004">tüj</ta>
            <ta e="T1006" id="Seg_586" s="T1005">jakšə</ta>
            <ta e="T1007" id="Seg_587" s="T1006">bar</ta>
            <ta e="T1008" id="Seg_588" s="T1007">amno-laʔbə-ʔjə</ta>
            <ta e="T1009" id="Seg_589" s="T1008">šindi=də</ta>
            <ta e="T1010" id="Seg_590" s="T1009">šindi-m</ta>
            <ta e="T1011" id="Seg_591" s="T1010">ej</ta>
            <ta e="T1012" id="Seg_592" s="T1011">am-nia</ta>
            <ta e="T1013" id="Seg_593" s="T1012">a</ta>
            <ta e="T1014" id="Seg_594" s="T1013">šindi</ta>
            <ta e="T1015" id="Seg_595" s="T1014">tĭm-nie-t</ta>
            <ta e="T1016" id="Seg_596" s="T1015">možet</ta>
            <ta e="T1017" id="Seg_597" s="T1016">dĭ-zeŋ</ta>
            <ta e="T1018" id="Seg_598" s="T1017">ej</ta>
            <ta e="T1019" id="Seg_599" s="T1018">tĭmne-ʔi</ta>
            <ta e="T1020" id="Seg_600" s="T1019">što</ta>
            <ta e="T1021" id="Seg_601" s="T1020">jakšə</ta>
            <ta e="T1022" id="Seg_602" s="T1021">amno-sʼtə</ta>
            <ta e="T1023" id="Seg_603" s="T1022">da</ta>
            <ta e="T1024" id="Seg_604" s="T1023">kabar-luʔ-jəʔ</ta>
            <ta e="T1025" id="Seg_605" s="T1024">bos-tə</ta>
            <ta e="T1026" id="Seg_606" s="T1025">nuʔmə-luʔ-pi</ta>
            <ta e="T1027" id="Seg_607" s="T1026">i</ta>
            <ta e="T1028" id="Seg_608" s="T1027">dĭ</ta>
            <ta e="T1029" id="Seg_609" s="T1028">ma-luʔ-pi</ta>
            <ta e="T1030" id="Seg_610" s="T1029">unnʼa</ta>
            <ta e="T1031" id="Seg_611" s="T1030">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T949" id="Seg_612" s="T948">šide-göʔ</ta>
            <ta e="T950" id="Seg_613" s="T949">i</ta>
            <ta e="T951" id="Seg_614" s="T950">lʼisʼitsa</ta>
            <ta e="T952" id="Seg_615" s="T951">i</ta>
            <ta e="T953" id="Seg_616" s="T952">tʼetʼer</ta>
            <ta e="T954" id="Seg_617" s="T953">lʼisʼitsa</ta>
            <ta e="T955" id="Seg_618" s="T954">dĭ-Tə</ta>
            <ta e="T956" id="Seg_619" s="T955">măn-ntə</ta>
            <ta e="T957" id="Seg_620" s="T956">šo-ʔ</ta>
            <ta e="T959" id="Seg_621" s="T958">tʼo-Tə</ta>
            <ta e="T960" id="Seg_622" s="T959">nʼergö-ʔ</ta>
            <ta e="T961" id="Seg_623" s="T960">döbər</ta>
            <ta e="T962" id="Seg_624" s="T961">tăn-ziʔ</ta>
            <ta e="T963" id="Seg_625" s="T962">tʼăbaktər-lV-bAʔ</ta>
            <ta e="T964" id="Seg_626" s="T963">măna</ta>
            <ta e="T965" id="Seg_627" s="T964">tüj</ta>
            <ta e="T966" id="Seg_628" s="T965">ej</ta>
            <ta e="T967" id="Seg_629" s="T966">nünə-liA-m</ta>
            <ta e="T968" id="Seg_630" s="T967">dʼok</ta>
            <ta e="T969" id="Seg_631" s="T968">măn</ta>
            <ta e="T970" id="Seg_632" s="T969">pim-liA-m</ta>
            <ta e="T971" id="Seg_633" s="T970">tʼo-Kən</ta>
            <ta e="T972" id="Seg_634" s="T971">bar</ta>
            <ta e="T973" id="Seg_635" s="T972">mĭn-gA-jəʔ</ta>
            <ta e="T974" id="Seg_636" s="T973">men-jəʔ</ta>
            <ta e="T975" id="Seg_637" s="T974">da</ta>
            <ta e="T976" id="Seg_638" s="T975">vsʼaka</ta>
            <ta e="T977" id="Seg_639" s="T976">zveri-jəʔ</ta>
            <ta e="T978" id="Seg_640" s="T977">măna</ta>
            <ta e="T979" id="Seg_641" s="T978">am-luʔbdə-lV-jəʔ</ta>
            <ta e="T980" id="Seg_642" s="T979">ato</ta>
            <ta e="T981" id="Seg_643" s="T980">tăn</ta>
            <ta e="T982" id="Seg_644" s="T981">ĭššo</ta>
            <ta e="T983" id="Seg_645" s="T982">măna</ta>
            <ta e="T984" id="Seg_646" s="T983">am-lV-l</ta>
            <ta e="T985" id="Seg_647" s="T984">dʼok</ta>
            <ta e="T986" id="Seg_648" s="T985">tüj</ta>
            <ta e="T987" id="Seg_649" s="T986">ej</ta>
            <ta e="T988" id="Seg_650" s="T987">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T989" id="Seg_651" s="T988">jakšə</ta>
            <ta e="T990" id="Seg_652" s="T989">amno-laʔbə-jəʔ</ta>
            <ta e="T991" id="Seg_653" s="T990">a</ta>
            <ta e="T992" id="Seg_654" s="T991">dĭ</ta>
            <ta e="T993" id="Seg_655" s="T992">măn-ntə</ta>
            <ta e="T994" id="Seg_656" s="T993">von</ta>
            <ta e="T995" id="Seg_657" s="T994">men-zAŋ</ta>
            <ta e="T996" id="Seg_658" s="T995">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T997" id="Seg_659" s="T996">a</ta>
            <ta e="T998" id="Seg_660" s="T997">dĭ</ta>
            <ta e="T999" id="Seg_661" s="T998">nuʔmə-zittə</ta>
            <ta e="T1000" id="Seg_662" s="T999">xatʼel</ta>
            <ta e="T1001" id="Seg_663" s="T1000">dĭ</ta>
            <ta e="T1002" id="Seg_664" s="T1001">măn-ntə</ta>
            <ta e="T1003" id="Seg_665" s="T1002">ĭmbi</ta>
            <ta e="T1004" id="Seg_666" s="T1003">nuʔmə-laʔbə-l</ta>
            <ta e="T1005" id="Seg_667" s="T1004">tüj</ta>
            <ta e="T1006" id="Seg_668" s="T1005">jakšə</ta>
            <ta e="T1007" id="Seg_669" s="T1006">bar</ta>
            <ta e="T1008" id="Seg_670" s="T1007">amno-laʔbə-jəʔ</ta>
            <ta e="T1009" id="Seg_671" s="T1008">šində=də</ta>
            <ta e="T1010" id="Seg_672" s="T1009">šində-m</ta>
            <ta e="T1011" id="Seg_673" s="T1010">ej</ta>
            <ta e="T1012" id="Seg_674" s="T1011">am-liA</ta>
            <ta e="T1013" id="Seg_675" s="T1012">a</ta>
            <ta e="T1014" id="Seg_676" s="T1013">šində</ta>
            <ta e="T1015" id="Seg_677" s="T1014">tĭm-liA-t</ta>
            <ta e="T1016" id="Seg_678" s="T1015">možet</ta>
            <ta e="T1017" id="Seg_679" s="T1016">dĭ-zAŋ</ta>
            <ta e="T1018" id="Seg_680" s="T1017">ej</ta>
            <ta e="T1019" id="Seg_681" s="T1018">tĭmne-jəʔ</ta>
            <ta e="T1020" id="Seg_682" s="T1019">što</ta>
            <ta e="T1021" id="Seg_683" s="T1020">jakšə</ta>
            <ta e="T1022" id="Seg_684" s="T1021">amno-zittə</ta>
            <ta e="T1023" id="Seg_685" s="T1022">da</ta>
            <ta e="T1024" id="Seg_686" s="T1023">kabar-luʔbdə-jəʔ</ta>
            <ta e="T1025" id="Seg_687" s="T1024">bos-də</ta>
            <ta e="T1026" id="Seg_688" s="T1025">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1027" id="Seg_689" s="T1026">i</ta>
            <ta e="T1028" id="Seg_690" s="T1027">dĭ</ta>
            <ta e="T1029" id="Seg_691" s="T1028">ma-luʔbdə-bi</ta>
            <ta e="T1030" id="Seg_692" s="T1029">unʼə</ta>
            <ta e="T1031" id="Seg_693" s="T1030">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T949" id="Seg_694" s="T948">two-COLL</ta>
            <ta e="T950" id="Seg_695" s="T949">and</ta>
            <ta e="T951" id="Seg_696" s="T950">fox.[NOM.SG]</ta>
            <ta e="T952" id="Seg_697" s="T951">and</ta>
            <ta e="T953" id="Seg_698" s="T952">grouse.[NOM.SG]</ta>
            <ta e="T954" id="Seg_699" s="T953">fox.[NOM.SG]</ta>
            <ta e="T955" id="Seg_700" s="T954">this-LAT</ta>
            <ta e="T956" id="Seg_701" s="T955">say-IPFVZ.[3SG]</ta>
            <ta e="T957" id="Seg_702" s="T956">come-IMP.2SG</ta>
            <ta e="T959" id="Seg_703" s="T958">place-LAT</ta>
            <ta e="T960" id="Seg_704" s="T959">fly-IMP.2SG</ta>
            <ta e="T961" id="Seg_705" s="T960">here</ta>
            <ta e="T962" id="Seg_706" s="T961">you.NOM-INS</ta>
            <ta e="T963" id="Seg_707" s="T962">speak-FUT-1PL</ta>
            <ta e="T964" id="Seg_708" s="T963">I.LAT</ta>
            <ta e="T965" id="Seg_709" s="T964">now</ta>
            <ta e="T966" id="Seg_710" s="T965">NEG</ta>
            <ta e="T967" id="Seg_711" s="T966">hear-PRS-1SG</ta>
            <ta e="T968" id="Seg_712" s="T967">no</ta>
            <ta e="T969" id="Seg_713" s="T968">I.NOM</ta>
            <ta e="T970" id="Seg_714" s="T969">fear-PRS-1SG</ta>
            <ta e="T971" id="Seg_715" s="T970">earth-LOC</ta>
            <ta e="T972" id="Seg_716" s="T971">PTCL</ta>
            <ta e="T973" id="Seg_717" s="T972">go-PRS-3PL</ta>
            <ta e="T974" id="Seg_718" s="T973">dog-PL</ta>
            <ta e="T975" id="Seg_719" s="T974">and</ta>
            <ta e="T976" id="Seg_720" s="T975">sundry</ta>
            <ta e="T977" id="Seg_721" s="T976">beast-PL</ta>
            <ta e="T978" id="Seg_722" s="T977">I.ACC</ta>
            <ta e="T979" id="Seg_723" s="T978">eat-MOM-FUT-3PL</ta>
            <ta e="T980" id="Seg_724" s="T979">otherwise</ta>
            <ta e="T981" id="Seg_725" s="T980">you.NOM</ta>
            <ta e="T982" id="Seg_726" s="T981">more</ta>
            <ta e="T983" id="Seg_727" s="T982">I.ACC</ta>
            <ta e="T984" id="Seg_728" s="T983">eat-FUT-2SG</ta>
            <ta e="T985" id="Seg_729" s="T984">no</ta>
            <ta e="T986" id="Seg_730" s="T985">now</ta>
            <ta e="T987" id="Seg_731" s="T986">NEG</ta>
            <ta e="T988" id="Seg_732" s="T987">fight-DUR-3PL</ta>
            <ta e="T989" id="Seg_733" s="T988">good</ta>
            <ta e="T990" id="Seg_734" s="T989">live-DUR-3PL</ta>
            <ta e="T991" id="Seg_735" s="T990">and</ta>
            <ta e="T992" id="Seg_736" s="T991">this.[NOM.SG]</ta>
            <ta e="T993" id="Seg_737" s="T992">say-IPFVZ.[3SG]</ta>
            <ta e="T994" id="Seg_738" s="T993">there</ta>
            <ta e="T995" id="Seg_739" s="T994">dog-PL</ta>
            <ta e="T996" id="Seg_740" s="T995">run-DUR-3PL</ta>
            <ta e="T997" id="Seg_741" s="T996">and</ta>
            <ta e="T998" id="Seg_742" s="T997">this.[NOM.SG]</ta>
            <ta e="T999" id="Seg_743" s="T998">run-INF.LAT</ta>
            <ta e="T1000" id="Seg_744" s="T999">want.PST.M.SG</ta>
            <ta e="T1001" id="Seg_745" s="T1000">this.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_746" s="T1001">say-IPFVZ.[3SG]</ta>
            <ta e="T1003" id="Seg_747" s="T1002">what.[NOM.SG]</ta>
            <ta e="T1004" id="Seg_748" s="T1003">run-DUR-2SG</ta>
            <ta e="T1005" id="Seg_749" s="T1004">now</ta>
            <ta e="T1006" id="Seg_750" s="T1005">good</ta>
            <ta e="T1007" id="Seg_751" s="T1006">PTCL</ta>
            <ta e="T1008" id="Seg_752" s="T1007">live-DUR-3PL</ta>
            <ta e="T1009" id="Seg_753" s="T1008">who.[NOM.SG]=INDEF</ta>
            <ta e="T1010" id="Seg_754" s="T1009">who-ACC</ta>
            <ta e="T1011" id="Seg_755" s="T1010">NEG</ta>
            <ta e="T1012" id="Seg_756" s="T1011">eat-PRS.[3SG]</ta>
            <ta e="T1013" id="Seg_757" s="T1012">and</ta>
            <ta e="T1014" id="Seg_758" s="T1013">who.[NOM.SG]</ta>
            <ta e="T1015" id="Seg_759" s="T1014">know-PRS-3SG.O</ta>
            <ta e="T1016" id="Seg_760" s="T1015">maybe</ta>
            <ta e="T1017" id="Seg_761" s="T1016">this-PL</ta>
            <ta e="T1018" id="Seg_762" s="T1017">NEG</ta>
            <ta e="T1019" id="Seg_763" s="T1018">know-3PL</ta>
            <ta e="T1020" id="Seg_764" s="T1019">that</ta>
            <ta e="T1021" id="Seg_765" s="T1020">good</ta>
            <ta e="T1022" id="Seg_766" s="T1021">live-INF.LAT</ta>
            <ta e="T1023" id="Seg_767" s="T1022">and</ta>
            <ta e="T1024" id="Seg_768" s="T1023">grab-MOM-3PL</ta>
            <ta e="T1025" id="Seg_769" s="T1024">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1026" id="Seg_770" s="T1025">run-MOM-PST.[3SG]</ta>
            <ta e="T1027" id="Seg_771" s="T1026">and</ta>
            <ta e="T1028" id="Seg_772" s="T1027">this.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_773" s="T1028">leave-MOM-PST.[3SG]</ta>
            <ta e="T1030" id="Seg_774" s="T1029">alone</ta>
            <ta e="T1031" id="Seg_775" s="T1030">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T949" id="Seg_776" s="T948">два-COLL</ta>
            <ta e="T950" id="Seg_777" s="T949">и</ta>
            <ta e="T951" id="Seg_778" s="T950">лисица.[NOM.SG]</ta>
            <ta e="T952" id="Seg_779" s="T951">и</ta>
            <ta e="T953" id="Seg_780" s="T952">тетерев.[NOM.SG]</ta>
            <ta e="T954" id="Seg_781" s="T953">лисица.[NOM.SG]</ta>
            <ta e="T955" id="Seg_782" s="T954">этот-LAT</ta>
            <ta e="T956" id="Seg_783" s="T955">сказать-IPFVZ.[3SG]</ta>
            <ta e="T957" id="Seg_784" s="T956">прийти-IMP.2SG</ta>
            <ta e="T959" id="Seg_785" s="T958">место-LAT</ta>
            <ta e="T960" id="Seg_786" s="T959">лететь-IMP.2SG</ta>
            <ta e="T961" id="Seg_787" s="T960">здесь</ta>
            <ta e="T962" id="Seg_788" s="T961">ты.NOM-INS</ta>
            <ta e="T963" id="Seg_789" s="T962">говорить-FUT-1PL</ta>
            <ta e="T964" id="Seg_790" s="T963">я.LAT</ta>
            <ta e="T965" id="Seg_791" s="T964">сейчас</ta>
            <ta e="T966" id="Seg_792" s="T965">NEG</ta>
            <ta e="T967" id="Seg_793" s="T966">слышать-PRS-1SG</ta>
            <ta e="T968" id="Seg_794" s="T967">нет</ta>
            <ta e="T969" id="Seg_795" s="T968">я.NOM</ta>
            <ta e="T970" id="Seg_796" s="T969">бояться-PRS-1SG</ta>
            <ta e="T971" id="Seg_797" s="T970">земля-LOC</ta>
            <ta e="T972" id="Seg_798" s="T971">PTCL</ta>
            <ta e="T973" id="Seg_799" s="T972">идти-PRS-3PL</ta>
            <ta e="T974" id="Seg_800" s="T973">собака-PL</ta>
            <ta e="T975" id="Seg_801" s="T974">и</ta>
            <ta e="T976" id="Seg_802" s="T975">всякий</ta>
            <ta e="T977" id="Seg_803" s="T976">зверь-PL</ta>
            <ta e="T978" id="Seg_804" s="T977">я.ACC</ta>
            <ta e="T979" id="Seg_805" s="T978">съесть-MOM-FUT-3PL</ta>
            <ta e="T980" id="Seg_806" s="T979">а.то</ta>
            <ta e="T981" id="Seg_807" s="T980">ты.NOM</ta>
            <ta e="T982" id="Seg_808" s="T981">еще</ta>
            <ta e="T983" id="Seg_809" s="T982">я.ACC</ta>
            <ta e="T984" id="Seg_810" s="T983">съесть-FUT-2SG</ta>
            <ta e="T985" id="Seg_811" s="T984">нет</ta>
            <ta e="T986" id="Seg_812" s="T985">сейчас</ta>
            <ta e="T987" id="Seg_813" s="T986">NEG</ta>
            <ta e="T988" id="Seg_814" s="T987">бороться-DUR-3PL</ta>
            <ta e="T989" id="Seg_815" s="T988">хороший</ta>
            <ta e="T990" id="Seg_816" s="T989">жить-DUR-3PL</ta>
            <ta e="T991" id="Seg_817" s="T990">а</ta>
            <ta e="T992" id="Seg_818" s="T991">этот.[NOM.SG]</ta>
            <ta e="T993" id="Seg_819" s="T992">сказать-IPFVZ.[3SG]</ta>
            <ta e="T994" id="Seg_820" s="T993">вон</ta>
            <ta e="T995" id="Seg_821" s="T994">собака-PL</ta>
            <ta e="T996" id="Seg_822" s="T995">бежать-DUR-3PL</ta>
            <ta e="T997" id="Seg_823" s="T996">а</ta>
            <ta e="T998" id="Seg_824" s="T997">этот.[NOM.SG]</ta>
            <ta e="T999" id="Seg_825" s="T998">бежать-INF.LAT</ta>
            <ta e="T1000" id="Seg_826" s="T999">хотеть.PST.M.SG</ta>
            <ta e="T1001" id="Seg_827" s="T1000">этот.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_828" s="T1001">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1003" id="Seg_829" s="T1002">что.[NOM.SG]</ta>
            <ta e="T1004" id="Seg_830" s="T1003">бежать-DUR-2SG</ta>
            <ta e="T1005" id="Seg_831" s="T1004">сейчас</ta>
            <ta e="T1006" id="Seg_832" s="T1005">хороший</ta>
            <ta e="T1007" id="Seg_833" s="T1006">PTCL</ta>
            <ta e="T1008" id="Seg_834" s="T1007">жить-DUR-3PL</ta>
            <ta e="T1009" id="Seg_835" s="T1008">кто.[NOM.SG]=INDEF</ta>
            <ta e="T1010" id="Seg_836" s="T1009">кто-ACC</ta>
            <ta e="T1011" id="Seg_837" s="T1010">NEG</ta>
            <ta e="T1012" id="Seg_838" s="T1011">съесть-PRS.[3SG]</ta>
            <ta e="T1013" id="Seg_839" s="T1012">а</ta>
            <ta e="T1014" id="Seg_840" s="T1013">кто.[NOM.SG]</ta>
            <ta e="T1015" id="Seg_841" s="T1014">знать-PRS-3SG.O</ta>
            <ta e="T1016" id="Seg_842" s="T1015">может.быть</ta>
            <ta e="T1017" id="Seg_843" s="T1016">этот-PL</ta>
            <ta e="T1018" id="Seg_844" s="T1017">NEG</ta>
            <ta e="T1019" id="Seg_845" s="T1018">знать-3PL</ta>
            <ta e="T1020" id="Seg_846" s="T1019">что</ta>
            <ta e="T1021" id="Seg_847" s="T1020">хороший</ta>
            <ta e="T1022" id="Seg_848" s="T1021">жить-INF.LAT</ta>
            <ta e="T1023" id="Seg_849" s="T1022">и</ta>
            <ta e="T1024" id="Seg_850" s="T1023">хватать-MOM-3PL</ta>
            <ta e="T1025" id="Seg_851" s="T1024">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1026" id="Seg_852" s="T1025">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1027" id="Seg_853" s="T1026">и</ta>
            <ta e="T1028" id="Seg_854" s="T1027">этот.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_855" s="T1028">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1030" id="Seg_856" s="T1029">один</ta>
            <ta e="T1031" id="Seg_857" s="T1030">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T949" id="Seg_858" s="T948">num-num&gt;num</ta>
            <ta e="T950" id="Seg_859" s="T949">conj</ta>
            <ta e="T951" id="Seg_860" s="T950">n-n:case</ta>
            <ta e="T952" id="Seg_861" s="T951">conj</ta>
            <ta e="T953" id="Seg_862" s="T952">n-n:case</ta>
            <ta e="T954" id="Seg_863" s="T953">n-n:case</ta>
            <ta e="T955" id="Seg_864" s="T954">dempro-n:case</ta>
            <ta e="T956" id="Seg_865" s="T955">v-v&gt;v-v:pn</ta>
            <ta e="T957" id="Seg_866" s="T956">v-v:mood.pn</ta>
            <ta e="T959" id="Seg_867" s="T958">n-n:case</ta>
            <ta e="T960" id="Seg_868" s="T959">v-v:mood.pn</ta>
            <ta e="T961" id="Seg_869" s="T960">adv</ta>
            <ta e="T962" id="Seg_870" s="T961">pers-n:case</ta>
            <ta e="T963" id="Seg_871" s="T962">v-v:tense-v:pn</ta>
            <ta e="T964" id="Seg_872" s="T963">pers</ta>
            <ta e="T965" id="Seg_873" s="T964">adv</ta>
            <ta e="T966" id="Seg_874" s="T965">ptcl</ta>
            <ta e="T967" id="Seg_875" s="T966">v-v:tense-v:pn</ta>
            <ta e="T968" id="Seg_876" s="T967">ptcl</ta>
            <ta e="T969" id="Seg_877" s="T968">pers</ta>
            <ta e="T970" id="Seg_878" s="T969">v-v:tense-v:pn</ta>
            <ta e="T971" id="Seg_879" s="T970">n-n:case</ta>
            <ta e="T972" id="Seg_880" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_881" s="T972">v-v:tense-v:pn</ta>
            <ta e="T974" id="Seg_882" s="T973">n-n:num</ta>
            <ta e="T975" id="Seg_883" s="T974">conj</ta>
            <ta e="T976" id="Seg_884" s="T975">adj</ta>
            <ta e="T977" id="Seg_885" s="T976">n-n:num</ta>
            <ta e="T978" id="Seg_886" s="T977">pers</ta>
            <ta e="T979" id="Seg_887" s="T978">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T980" id="Seg_888" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_889" s="T980">pers</ta>
            <ta e="T982" id="Seg_890" s="T981">adv</ta>
            <ta e="T983" id="Seg_891" s="T982">pers</ta>
            <ta e="T984" id="Seg_892" s="T983">v-v:tense-v:pn</ta>
            <ta e="T985" id="Seg_893" s="T984">ptcl</ta>
            <ta e="T986" id="Seg_894" s="T985">adv</ta>
            <ta e="T987" id="Seg_895" s="T986">ptcl</ta>
            <ta e="T988" id="Seg_896" s="T987">v-v&gt;v-v:pn</ta>
            <ta e="T989" id="Seg_897" s="T988">adj</ta>
            <ta e="T990" id="Seg_898" s="T989">v-v&gt;v-v:pn</ta>
            <ta e="T991" id="Seg_899" s="T990">conj</ta>
            <ta e="T992" id="Seg_900" s="T991">dempro-n:case</ta>
            <ta e="T993" id="Seg_901" s="T992">v-v&gt;v-v:pn</ta>
            <ta e="T994" id="Seg_902" s="T993">adv</ta>
            <ta e="T995" id="Seg_903" s="T994">n-n:num</ta>
            <ta e="T996" id="Seg_904" s="T995">v-v&gt;v-v:pn</ta>
            <ta e="T997" id="Seg_905" s="T996">conj</ta>
            <ta e="T998" id="Seg_906" s="T997">dempro-n:case</ta>
            <ta e="T999" id="Seg_907" s="T998">v-v:n.fin</ta>
            <ta e="T1000" id="Seg_908" s="T999">v</ta>
            <ta e="T1001" id="Seg_909" s="T1000">dempro-n:case</ta>
            <ta e="T1002" id="Seg_910" s="T1001">v-v&gt;v-v:pn</ta>
            <ta e="T1003" id="Seg_911" s="T1002">que-n:case</ta>
            <ta e="T1004" id="Seg_912" s="T1003">v-v&gt;v-v:pn</ta>
            <ta e="T1005" id="Seg_913" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_914" s="T1005">adj</ta>
            <ta e="T1007" id="Seg_915" s="T1006">ptcl</ta>
            <ta e="T1008" id="Seg_916" s="T1007">v-v&gt;v-v:pn</ta>
            <ta e="T1009" id="Seg_917" s="T1008">que-n:case=ptcl</ta>
            <ta e="T1010" id="Seg_918" s="T1009">que-n:case</ta>
            <ta e="T1011" id="Seg_919" s="T1010">ptcl</ta>
            <ta e="T1012" id="Seg_920" s="T1011">v-v:tense-v:pn</ta>
            <ta e="T1013" id="Seg_921" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_922" s="T1013">que-n:case</ta>
            <ta e="T1015" id="Seg_923" s="T1014">v-v:tense-v:pn</ta>
            <ta e="T1016" id="Seg_924" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_925" s="T1016">dempro-n:num</ta>
            <ta e="T1018" id="Seg_926" s="T1017">ptcl</ta>
            <ta e="T1019" id="Seg_927" s="T1018">v-v:pn</ta>
            <ta e="T1020" id="Seg_928" s="T1019">conj</ta>
            <ta e="T1021" id="Seg_929" s="T1020">adj</ta>
            <ta e="T1022" id="Seg_930" s="T1021">v-v:n.fin</ta>
            <ta e="T1023" id="Seg_931" s="T1022">conj</ta>
            <ta e="T1024" id="Seg_932" s="T1023">v-v&gt;v-v:pn</ta>
            <ta e="T1025" id="Seg_933" s="T1024">refl-n:case.poss</ta>
            <ta e="T1026" id="Seg_934" s="T1025">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1027" id="Seg_935" s="T1026">conj</ta>
            <ta e="T1028" id="Seg_936" s="T1027">dempro-n:case</ta>
            <ta e="T1029" id="Seg_937" s="T1028">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1030" id="Seg_938" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_939" s="T1030">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T949" id="Seg_940" s="T948">num</ta>
            <ta e="T950" id="Seg_941" s="T949">conj</ta>
            <ta e="T951" id="Seg_942" s="T950">n</ta>
            <ta e="T952" id="Seg_943" s="T951">conj</ta>
            <ta e="T953" id="Seg_944" s="T952">n</ta>
            <ta e="T954" id="Seg_945" s="T953">n</ta>
            <ta e="T955" id="Seg_946" s="T954">dempro</ta>
            <ta e="T956" id="Seg_947" s="T955">v</ta>
            <ta e="T957" id="Seg_948" s="T956">v</ta>
            <ta e="T959" id="Seg_949" s="T958">n</ta>
            <ta e="T960" id="Seg_950" s="T959">v</ta>
            <ta e="T961" id="Seg_951" s="T960">adv</ta>
            <ta e="T962" id="Seg_952" s="T961">pers</ta>
            <ta e="T963" id="Seg_953" s="T962">v</ta>
            <ta e="T964" id="Seg_954" s="T963">pers</ta>
            <ta e="T965" id="Seg_955" s="T964">adv</ta>
            <ta e="T966" id="Seg_956" s="T965">ptcl</ta>
            <ta e="T967" id="Seg_957" s="T966">v</ta>
            <ta e="T968" id="Seg_958" s="T967">ptcl</ta>
            <ta e="T969" id="Seg_959" s="T968">pers</ta>
            <ta e="T970" id="Seg_960" s="T969">v</ta>
            <ta e="T971" id="Seg_961" s="T970">n</ta>
            <ta e="T972" id="Seg_962" s="T971">ptcl</ta>
            <ta e="T973" id="Seg_963" s="T972">v</ta>
            <ta e="T974" id="Seg_964" s="T973">n</ta>
            <ta e="T975" id="Seg_965" s="T974">conj</ta>
            <ta e="T976" id="Seg_966" s="T975">adj</ta>
            <ta e="T977" id="Seg_967" s="T976">n</ta>
            <ta e="T978" id="Seg_968" s="T977">pers</ta>
            <ta e="T979" id="Seg_969" s="T978">v</ta>
            <ta e="T980" id="Seg_970" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_971" s="T980">pers</ta>
            <ta e="T982" id="Seg_972" s="T981">adv</ta>
            <ta e="T983" id="Seg_973" s="T982">pers</ta>
            <ta e="T984" id="Seg_974" s="T983">v</ta>
            <ta e="T985" id="Seg_975" s="T984">ptcl</ta>
            <ta e="T986" id="Seg_976" s="T985">adv</ta>
            <ta e="T987" id="Seg_977" s="T986">ptcl</ta>
            <ta e="T988" id="Seg_978" s="T987">v</ta>
            <ta e="T989" id="Seg_979" s="T988">adj</ta>
            <ta e="T990" id="Seg_980" s="T989">v</ta>
            <ta e="T991" id="Seg_981" s="T990">conj</ta>
            <ta e="T992" id="Seg_982" s="T991">dempro</ta>
            <ta e="T993" id="Seg_983" s="T992">v</ta>
            <ta e="T994" id="Seg_984" s="T993">adv</ta>
            <ta e="T995" id="Seg_985" s="T994">n</ta>
            <ta e="T996" id="Seg_986" s="T995">v</ta>
            <ta e="T997" id="Seg_987" s="T996">conj</ta>
            <ta e="T998" id="Seg_988" s="T997">dempro</ta>
            <ta e="T999" id="Seg_989" s="T998">v</ta>
            <ta e="T1000" id="Seg_990" s="T999">v</ta>
            <ta e="T1001" id="Seg_991" s="T1000">dempro</ta>
            <ta e="T1002" id="Seg_992" s="T1001">v</ta>
            <ta e="T1003" id="Seg_993" s="T1002">que</ta>
            <ta e="T1004" id="Seg_994" s="T1003">v</ta>
            <ta e="T1005" id="Seg_995" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_996" s="T1005">adj</ta>
            <ta e="T1007" id="Seg_997" s="T1006">ptcl</ta>
            <ta e="T1008" id="Seg_998" s="T1007">v</ta>
            <ta e="T1009" id="Seg_999" s="T1008">que</ta>
            <ta e="T1010" id="Seg_1000" s="T1009">que</ta>
            <ta e="T1011" id="Seg_1001" s="T1010">ptcl</ta>
            <ta e="T1012" id="Seg_1002" s="T1011">v</ta>
            <ta e="T1013" id="Seg_1003" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_1004" s="T1013">que</ta>
            <ta e="T1015" id="Seg_1005" s="T1014">v</ta>
            <ta e="T1016" id="Seg_1006" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_1007" s="T1016">dempro</ta>
            <ta e="T1018" id="Seg_1008" s="T1017">ptcl</ta>
            <ta e="T1019" id="Seg_1009" s="T1018">v</ta>
            <ta e="T1020" id="Seg_1010" s="T1019">conj</ta>
            <ta e="T1021" id="Seg_1011" s="T1020">adj</ta>
            <ta e="T1022" id="Seg_1012" s="T1021">v</ta>
            <ta e="T1023" id="Seg_1013" s="T1022">conj</ta>
            <ta e="T1024" id="Seg_1014" s="T1023">v</ta>
            <ta e="T1025" id="Seg_1015" s="T1024">refl</ta>
            <ta e="T1026" id="Seg_1016" s="T1025">v</ta>
            <ta e="T1027" id="Seg_1017" s="T1026">conj</ta>
            <ta e="T1028" id="Seg_1018" s="T1027">dempro</ta>
            <ta e="T1029" id="Seg_1019" s="T1028">v</ta>
            <ta e="T1030" id="Seg_1020" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_1021" s="T1030">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T954" id="Seg_1022" s="T953">np.h:A</ta>
            <ta e="T955" id="Seg_1023" s="T954">pro.h:R</ta>
            <ta e="T957" id="Seg_1024" s="T956">0.2.h:A</ta>
            <ta e="T959" id="Seg_1025" s="T958">np:G</ta>
            <ta e="T960" id="Seg_1026" s="T959">0.2.h:A</ta>
            <ta e="T961" id="Seg_1027" s="T960">adv:L</ta>
            <ta e="T962" id="Seg_1028" s="T961">pro.h:Com</ta>
            <ta e="T963" id="Seg_1029" s="T962">0.1.h:A</ta>
            <ta e="T965" id="Seg_1030" s="T964">adv:Time</ta>
            <ta e="T967" id="Seg_1031" s="T966">0.1.h:E</ta>
            <ta e="T969" id="Seg_1032" s="T968">pro.h:E</ta>
            <ta e="T971" id="Seg_1033" s="T970">np:L</ta>
            <ta e="T974" id="Seg_1034" s="T973">np:A</ta>
            <ta e="T977" id="Seg_1035" s="T976">np:A</ta>
            <ta e="T978" id="Seg_1036" s="T977">pro.h:P</ta>
            <ta e="T981" id="Seg_1037" s="T980">pro.h:A</ta>
            <ta e="T983" id="Seg_1038" s="T982">pro.h:P</ta>
            <ta e="T986" id="Seg_1039" s="T985">adv:Time</ta>
            <ta e="T988" id="Seg_1040" s="T987">0.3:A</ta>
            <ta e="T990" id="Seg_1041" s="T989">0.3:A</ta>
            <ta e="T992" id="Seg_1042" s="T991">pro.h:A</ta>
            <ta e="T994" id="Seg_1043" s="T993">adv:L</ta>
            <ta e="T995" id="Seg_1044" s="T994">np:A</ta>
            <ta e="T998" id="Seg_1045" s="T997">pro.h:A</ta>
            <ta e="T1001" id="Seg_1046" s="T1000">pro.h:A</ta>
            <ta e="T1004" id="Seg_1047" s="T1003">0.2.h:A</ta>
            <ta e="T1005" id="Seg_1048" s="T1004">adv:Time</ta>
            <ta e="T1008" id="Seg_1049" s="T1007">0.3.h:A</ta>
            <ta e="T1009" id="Seg_1050" s="T1008">pro:A</ta>
            <ta e="T1010" id="Seg_1051" s="T1009">pro:P</ta>
            <ta e="T1014" id="Seg_1052" s="T1013">pro.h:E</ta>
            <ta e="T1017" id="Seg_1053" s="T1016">np:E</ta>
            <ta e="T1024" id="Seg_1054" s="T1023">0.3:A</ta>
            <ta e="T1025" id="Seg_1055" s="T1024">pro.h:A</ta>
            <ta e="T1028" id="Seg_1056" s="T1027">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T954" id="Seg_1057" s="T953">np.h:S</ta>
            <ta e="T956" id="Seg_1058" s="T955">v:pred</ta>
            <ta e="T957" id="Seg_1059" s="T956">v:pred 0.2.h:S</ta>
            <ta e="T960" id="Seg_1060" s="T959">v:pred 0.2.h:S</ta>
            <ta e="T963" id="Seg_1061" s="T962">v:pred 0.1.h:S</ta>
            <ta e="T966" id="Seg_1062" s="T965">ptcl.neg</ta>
            <ta e="T967" id="Seg_1063" s="T966">v:pred 0.1.h:S</ta>
            <ta e="T969" id="Seg_1064" s="T968">pro.h:S</ta>
            <ta e="T970" id="Seg_1065" s="T969">v:pred</ta>
            <ta e="T973" id="Seg_1066" s="T972">v:pred</ta>
            <ta e="T974" id="Seg_1067" s="T973">np:S</ta>
            <ta e="T977" id="Seg_1068" s="T976">np:S</ta>
            <ta e="T978" id="Seg_1069" s="T977">pro.h:O</ta>
            <ta e="T979" id="Seg_1070" s="T978">v:pred</ta>
            <ta e="T981" id="Seg_1071" s="T980">pro.h:S</ta>
            <ta e="T983" id="Seg_1072" s="T982">pro.h:O</ta>
            <ta e="T984" id="Seg_1073" s="T983">v:pred</ta>
            <ta e="T987" id="Seg_1074" s="T986">ptcl.neg</ta>
            <ta e="T988" id="Seg_1075" s="T987">v:pred 0.3:S</ta>
            <ta e="T990" id="Seg_1076" s="T989">v:pred 0.3:S</ta>
            <ta e="T992" id="Seg_1077" s="T991">pro.h:S</ta>
            <ta e="T993" id="Seg_1078" s="T992">v:pred</ta>
            <ta e="T995" id="Seg_1079" s="T994">np:S</ta>
            <ta e="T996" id="Seg_1080" s="T995">v:pred</ta>
            <ta e="T1000" id="Seg_1081" s="T999">ptcl:pred</ta>
            <ta e="T1001" id="Seg_1082" s="T1000">pro.h:S</ta>
            <ta e="T1002" id="Seg_1083" s="T1001">v:pred</ta>
            <ta e="T1004" id="Seg_1084" s="T1003">v:pred 0.2.h:S</ta>
            <ta e="T1008" id="Seg_1085" s="T1007">v:pred 0.3.h:S</ta>
            <ta e="T1009" id="Seg_1086" s="T1008">pro:S</ta>
            <ta e="T1010" id="Seg_1087" s="T1009">pro:O</ta>
            <ta e="T1011" id="Seg_1088" s="T1010">ptcl:pred</ta>
            <ta e="T1012" id="Seg_1089" s="T1011">v:pred</ta>
            <ta e="T1014" id="Seg_1090" s="T1013">pro.h:S</ta>
            <ta e="T1015" id="Seg_1091" s="T1014">v:pred</ta>
            <ta e="T1017" id="Seg_1092" s="T1016">np:S</ta>
            <ta e="T1018" id="Seg_1093" s="T1017">ptcl.neg</ta>
            <ta e="T1019" id="Seg_1094" s="T1018">v:pred</ta>
            <ta e="T1024" id="Seg_1095" s="T1023">v:pred 0.3.S</ta>
            <ta e="T1025" id="Seg_1096" s="T1024">pro.h:S</ta>
            <ta e="T1026" id="Seg_1097" s="T1025">v:pred</ta>
            <ta e="T1028" id="Seg_1098" s="T1027">pro.h:S</ta>
            <ta e="T1029" id="Seg_1099" s="T1028">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T950" id="Seg_1100" s="T949">RUS:gram</ta>
            <ta e="T951" id="Seg_1101" s="T950">RUS:cult</ta>
            <ta e="T952" id="Seg_1102" s="T951">RUS:gram</ta>
            <ta e="T953" id="Seg_1103" s="T952">RUS:cult</ta>
            <ta e="T954" id="Seg_1104" s="T953">RUS:cult</ta>
            <ta e="T963" id="Seg_1105" s="T962">%TURK:core</ta>
            <ta e="T968" id="Seg_1106" s="T967">TURK:disc</ta>
            <ta e="T972" id="Seg_1107" s="T971">TURK:disc</ta>
            <ta e="T975" id="Seg_1108" s="T974">RUS:gram</ta>
            <ta e="T976" id="Seg_1109" s="T975">RUS:core</ta>
            <ta e="T977" id="Seg_1110" s="T976">RUS:core</ta>
            <ta e="T980" id="Seg_1111" s="T979">RUS:gram</ta>
            <ta e="T982" id="Seg_1112" s="T981">RUS:mod</ta>
            <ta e="T985" id="Seg_1113" s="T984">TURK:disc</ta>
            <ta e="T989" id="Seg_1114" s="T988">TURK:core</ta>
            <ta e="T991" id="Seg_1115" s="T990">RUS:gram</ta>
            <ta e="T994" id="Seg_1116" s="T993">RUS:core</ta>
            <ta e="T997" id="Seg_1117" s="T996">RUS:gram</ta>
            <ta e="T1000" id="Seg_1118" s="T999">RUS:mod</ta>
            <ta e="T1006" id="Seg_1119" s="T1005">TURK:core</ta>
            <ta e="T1007" id="Seg_1120" s="T1006">TURK:disc</ta>
            <ta e="T1009" id="Seg_1121" s="T1008">TURK:gram(INDEF)</ta>
            <ta e="T1013" id="Seg_1122" s="T1012">RUS:gram</ta>
            <ta e="T1016" id="Seg_1123" s="T1015">RUS:mod</ta>
            <ta e="T1020" id="Seg_1124" s="T1019">RUS:gram</ta>
            <ta e="T1021" id="Seg_1125" s="T1020">TURK:core</ta>
            <ta e="T1023" id="Seg_1126" s="T1022">RUS:gram</ta>
            <ta e="T1027" id="Seg_1127" s="T1026">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T977" id="Seg_1128" s="T975">RUS:int.ins</ta>
            <ta e="T1000" id="Seg_1129" s="T999">RUS:int.alt</ta>
            <ta e="T1003" id="Seg_1130" s="T1002">RUS:calq</ta>
            <ta e="T1015" id="Seg_1131" s="T1013">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T953" id="Seg_1132" s="T948">[Были] двое, лисица и тетерев.</ta>
            <ta e="T961" id="Seg_1133" s="T953">Лисица ему говорит: «Спускайся на землю, лети сюда!</ta>
            <ta e="T967" id="Seg_1134" s="T961">Мы с тобой поговорим, [а то] сейчас я тебя не слышу!»</ta>
            <ta e="T970" id="Seg_1135" s="T967">«Нет, я боюсь.</ta>
            <ta e="T979" id="Seg_1136" s="T970">Кругом собаки ходят и всякие звери, они меня съедят.</ta>
            <ta e="T984" id="Seg_1137" s="T979">И ты ещё можешь меня съесть».</ta>
            <ta e="T988" id="Seg_1138" s="T984">«Нет, теперь [звери] не дерутся.</ta>
            <ta e="T990" id="Seg_1139" s="T988">[Все] хорошо [=мирно] живут».</ta>
            <ta e="T996" id="Seg_1140" s="T990">А он говорит: «Смотри, собаки бегут!»</ta>
            <ta e="T1000" id="Seg_1141" s="T996">А она хотела убежать.</ta>
            <ta e="T1004" id="Seg_1142" s="T1000">[Тетерев] говорит: «Что ты убегаешь? </ta>
            <ta e="T1008" id="Seg_1143" s="T1004">Теперь же [все] хорошо живут. </ta>
            <ta e="T1012" id="Seg_1144" s="T1008">Никто никого не ест».</ta>
            <ta e="T1024" id="Seg_1145" s="T1012">«А кто знает, может они не знают, что [надо] хорошо жить, и поймают [меня]».</ta>
            <ta e="T1026" id="Seg_1146" s="T1024">Сама убежала.</ta>
            <ta e="T1030" id="Seg_1147" s="T1026">И тетерева оставила одного.</ta>
            <ta e="T1031" id="Seg_1148" s="T1030">Хваит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T953" id="Seg_1149" s="T948">[There are] two of them, a fox and a black grouse.</ta>
            <ta e="T961" id="Seg_1150" s="T953">The fox tells it: "Come to the ground, fly here!</ta>
            <ta e="T967" id="Seg_1151" s="T961">We will talk with you, I cannot hear [you] now!"</ta>
            <ta e="T970" id="Seg_1152" s="T967">"No, I am scared.</ta>
            <ta e="T979" id="Seg_1153" s="T970">There are dogs wandering on the ground and all kinds of beasts, they will eat me.</ta>
            <ta e="T984" id="Seg_1154" s="T979">And you may eat me, too."</ta>
            <ta e="T988" id="Seg_1155" s="T984">"No, [the animals] do not fight anymore.</ta>
            <ta e="T990" id="Seg_1156" s="T988">[All] live well [=in peace]."</ta>
            <ta e="T996" id="Seg_1157" s="T990">And [the grouse] says: "Look, there are dogs running!"</ta>
            <ta e="T1000" id="Seg_1158" s="T996">And [the fox] wanted to run [away].</ta>
            <ta e="T1004" id="Seg_1159" s="T1000">[The grouse] says: "Why are you running away?</ta>
            <ta e="T1008" id="Seg_1160" s="T1004">Now [all] live well.</ta>
            <ta e="T1012" id="Seg_1161" s="T1008">Nobody eats anyone."</ta>
            <ta e="T1024" id="Seg_1162" s="T1012">"But who knows, maybe they do not know that [we should] live well, and will catch [me]."</ta>
            <ta e="T1026" id="Seg_1163" s="T1024">[The fox] ran away.</ta>
            <ta e="T1030" id="Seg_1164" s="T1026">And left [the grouse] alone.</ta>
            <ta e="T1031" id="Seg_1165" s="T1030">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T953" id="Seg_1166" s="T948">[Es sind] zwei von ihnen, ein Fuchs und ein Birkhuhn.</ta>
            <ta e="T961" id="Seg_1167" s="T953">Der Fuchs erzählt ihm: „Komm zum Boden, flieg her!</ta>
            <ta e="T967" id="Seg_1168" s="T961">Wir werden mit dir reden, jetzt kann ich [dich] nicht hören!“ </ta>
            <ta e="T970" id="Seg_1169" s="T967">„Nein, ich habe Angst.</ta>
            <ta e="T979" id="Seg_1170" s="T970">Es sind Hunde, die auf dem Boden streunen und alle Art von Biestern, sie werden mich fressen.</ta>
            <ta e="T984" id="Seg_1171" s="T979">Und du frisst mich auch vielleicht.“</ta>
            <ta e="T988" id="Seg_1172" s="T984">„Nein, jetzt kämpfen [die Tiere] nicht.</ta>
            <ta e="T990" id="Seg_1173" s="T988">Wir leben gut [=in Frieden].“</ta>
            <ta e="T996" id="Seg_1174" s="T990">Aber es sagt: „Schau, es sind laufende Hund!“</ta>
            <ta e="T1000" id="Seg_1175" s="T996">Und er [der Fuchs] wollte [weg]rennen.</ta>
            <ta e="T1004" id="Seg_1176" s="T1000">[Das Birkhuhn] sagt: „Warum rennst du?</ta>
            <ta e="T1008" id="Seg_1177" s="T1004">Jetzt lebt jeder gut.</ta>
            <ta e="T1012" id="Seg_1178" s="T1008">Keine frisst niemanden.“</ta>
            <ta e="T1024" id="Seg_1179" s="T1012">„Aber wer weiß, vielleicht wissen sie nicht, dass [wir] gut leben [sollen] und werden [mich] fangen.“</ta>
            <ta e="T1026" id="Seg_1180" s="T1024">Er [der Fuchs] lief weg.</ta>
            <ta e="T1030" id="Seg_1181" s="T1026">Und es [das Birkhuhn] in Ruhe gelassen.</ta>
            <ta e="T1031" id="Seg_1182" s="T1030">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T953" id="Seg_1183" s="T948">[GVY:] https://deti-online.com/skazki/russkie-narodnye-skazki/lisa-i-teterev/</ta>
            <ta e="T967" id="Seg_1184" s="T961">[GVY:] dʼăbaktərlubaʔ - an unusual variant of the Future? Măna - should be măn?</ta>
            <ta e="T988" id="Seg_1185" s="T984">[GVY] dʼabərolaʔbəʔjə = dʼabərolaʔbəbaʔ? Or is this a general statement 'beasts aren't fighting now'? The same in the next sentence.</ta>
            <ta e="T1012" id="Seg_1186" s="T1008">[GVY:] Šidindədə - probably a slip of the tongue.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
