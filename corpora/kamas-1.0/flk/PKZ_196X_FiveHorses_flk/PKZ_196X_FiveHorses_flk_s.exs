<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID41F2D44F-3C91-C1E0-3178-8C68BBE4A6FF">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FiveHorses_flk.wav" />
         <referenced-file url="PKZ_196X_FiveHorses_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FiveHorses_flk\PKZ_196X_FiveHorses_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">63</ud-information>
            <ud-information attribute-name="# HIAT:w">45</ud-information>
            <ud-information attribute-name="# e">45</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T463" time="0.0" type="appl" />
         <tli id="T464" time="0.681" type="appl" />
         <tli id="T465" time="1.362" type="appl" />
         <tli id="T466" time="2.042" type="appl" />
         <tli id="T467" time="2.723" type="appl" />
         <tli id="T468" time="3.404" type="appl" />
         <tli id="T469" time="4.353" type="appl" />
         <tli id="T470" time="5.3" type="appl" />
         <tli id="T471" time="6.246" type="appl" />
         <tli id="T472" time="7.193" type="appl" />
         <tli id="T473" time="8.139" type="appl" />
         <tli id="T474" time="9.086" type="appl" />
         <tli id="T475" time="10.032" type="appl" />
         <tli id="T476" time="11.047" type="appl" />
         <tli id="T477" time="11.916" type="appl" />
         <tli id="T478" time="12.786" type="appl" />
         <tli id="T479" time="13.655" type="appl" />
         <tli id="T480" time="14.525" type="appl" />
         <tli id="T481" time="15.415" type="appl" />
         <tli id="T482" time="16.169" type="appl" />
         <tli id="T483" time="16.923" type="appl" />
         <tli id="T484" time="17.678" type="appl" />
         <tli id="T485" time="18.432" type="appl" />
         <tli id="T486" time="19.186" type="appl" />
         <tli id="T487" time="21.224172436640714" />
         <tli id="T488" time="21.936" type="appl" />
         <tli id="T489" time="22.489" type="appl" />
         <tli id="T490" time="23.042" type="appl" />
         <tli id="T491" time="23.596" type="appl" />
         <tli id="T492" time="24.149" type="appl" />
         <tli id="T493" time="26.183589614046713" />
         <tli id="T494" time="26.791" type="appl" />
         <tli id="T495" time="27.279" type="appl" />
         <tli id="T496" time="27.767" type="appl" />
         <tli id="T497" time="28.255" type="appl" />
         <tli id="T498" time="29.629851281679638" />
         <tli id="T499" time="31.61628450596323" />
         <tli id="T500" time="32.549" type="appl" />
         <tli id="T501" time="33.378" type="appl" />
         <tli id="T502" time="34.206" type="appl" />
         <tli id="T503" time="35.035" type="appl" />
         <tli id="T504" time="35.864" type="appl" />
         <tli id="T505" time="36.692" type="appl" />
         <tli id="T506" time="37.521" type="appl" />
         <tli id="T507" time="38.90876083940699" />
         <tli id="T508" time="40.242" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T508" id="Seg_0" n="sc" s="T463">
               <ts e="T468" id="Seg_2" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_4" n="HIAT:w" s="T463">Külüksəbi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_7" n="HIAT:w" s="T464">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_10" n="HIAT:w" s="T465">šobi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_13" n="HIAT:w" s="T466">koŋdə</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_17" n="HIAT:w" s="T467">amnolaʔbə</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_21" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_23" n="HIAT:w" s="T468">Onʼiʔ</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_26" n="HIAT:w" s="T469">kuza</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_29" n="HIAT:w" s="T470">šobi</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_32" n="HIAT:w" s="T471">i</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_35" n="HIAT:w" s="T472">deʔpi</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_38" n="HIAT:w" s="T473">svitok</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_41" n="HIAT:w" s="T474">sazən</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_45" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_47" n="HIAT:w" s="T475">Dĭ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_50" n="HIAT:w" s="T476">măndə:</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_52" n="HIAT:ip">"</nts>
                  <ts e="T478" id="Seg_54" n="HIAT:w" s="T477">Kanaʔ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_57" n="HIAT:w" s="T478">ineʔi</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_61" n="HIAT:w" s="T479">măndot</ts>
                  <nts id="Seg_62" n="HIAT:ip">"</nts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_66" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_68" n="HIAT:w" s="T480">A</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_71" n="HIAT:w" s="T481">dĭ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_74" n="HIAT:w" s="T482">mĭmbi</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_78" n="HIAT:w" s="T483">mĭmbi</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_81" n="HIAT:w" s="T484">i</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_84" n="HIAT:w" s="T485">amnəbi</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_87" n="HIAT:w" s="T486">koŋdə</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_91" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_93" n="HIAT:w" s="T487">Dĭgəttə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_96" n="HIAT:w" s="T488">dĭ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_99" n="HIAT:w" s="T489">măndə:</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_101" n="HIAT:ip">"</nts>
                  <ts e="T491" id="Seg_103" n="HIAT:w" s="T490">ĭmbi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_106" n="HIAT:w" s="T491">döbər</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_109" n="HIAT:w" s="T492">amndolaʔbəl</ts>
                  <nts id="Seg_110" n="HIAT:ip">?</nts>
                  <nts id="Seg_111" n="HIAT:ip">"</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_114" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_116" n="HIAT:w" s="T493">A</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_119" n="HIAT:w" s="T494">dĭ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_122" n="HIAT:w" s="T495">măndə:</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_124" n="HIAT:ip">"</nts>
                  <ts e="T497" id="Seg_126" n="HIAT:w" s="T496">tăn</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_129" n="HIAT:w" s="T497">kanaʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip">!</nts>
                  <nts id="Seg_131" n="HIAT:ip">"</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_134" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_136" n="HIAT:w" s="T498">Ineʔi</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_140" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_142" n="HIAT:w" s="T499">Dĭgəttə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_145" n="HIAT:w" s="T500">dibər</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_148" n="HIAT:w" s="T501">-</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_151" n="HIAT:w" s="T502">davaj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_154" n="HIAT:w" s="T503">meritʼ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_157" n="HIAT:w" s="T504">-</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_160" n="HIAT:w" s="T505">sumna</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_163" n="HIAT:w" s="T506">abi</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_167" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_169" n="HIAT:w" s="T507">Kabarləj</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T508" id="Seg_172" n="sc" s="T463">
               <ts e="T464" id="Seg_174" n="e" s="T463">Külüksəbi </ts>
               <ts e="T465" id="Seg_176" n="e" s="T464">kuza </ts>
               <ts e="T466" id="Seg_178" n="e" s="T465">šobi </ts>
               <ts e="T467" id="Seg_180" n="e" s="T466">koŋdə, </ts>
               <ts e="T468" id="Seg_182" n="e" s="T467">amnolaʔbə. </ts>
               <ts e="T469" id="Seg_184" n="e" s="T468">Onʼiʔ </ts>
               <ts e="T470" id="Seg_186" n="e" s="T469">kuza </ts>
               <ts e="T471" id="Seg_188" n="e" s="T470">šobi </ts>
               <ts e="T472" id="Seg_190" n="e" s="T471">i </ts>
               <ts e="T473" id="Seg_192" n="e" s="T472">deʔpi </ts>
               <ts e="T474" id="Seg_194" n="e" s="T473">svitok </ts>
               <ts e="T475" id="Seg_196" n="e" s="T474">sazən. </ts>
               <ts e="T476" id="Seg_198" n="e" s="T475">Dĭ </ts>
               <ts e="T477" id="Seg_200" n="e" s="T476">măndə: </ts>
               <ts e="T478" id="Seg_202" n="e" s="T477">"Kanaʔ </ts>
               <ts e="T479" id="Seg_204" n="e" s="T478">ineʔi, </ts>
               <ts e="T480" id="Seg_206" n="e" s="T479">măndot". </ts>
               <ts e="T481" id="Seg_208" n="e" s="T480">A </ts>
               <ts e="T482" id="Seg_210" n="e" s="T481">dĭ </ts>
               <ts e="T483" id="Seg_212" n="e" s="T482">mĭmbi, </ts>
               <ts e="T484" id="Seg_214" n="e" s="T483">mĭmbi </ts>
               <ts e="T485" id="Seg_216" n="e" s="T484">i </ts>
               <ts e="T486" id="Seg_218" n="e" s="T485">amnəbi </ts>
               <ts e="T487" id="Seg_220" n="e" s="T486">koŋdə. </ts>
               <ts e="T488" id="Seg_222" n="e" s="T487">Dĭgəttə </ts>
               <ts e="T489" id="Seg_224" n="e" s="T488">dĭ </ts>
               <ts e="T490" id="Seg_226" n="e" s="T489">măndə: </ts>
               <ts e="T491" id="Seg_228" n="e" s="T490">"ĭmbi </ts>
               <ts e="T492" id="Seg_230" n="e" s="T491">döbər </ts>
               <ts e="T493" id="Seg_232" n="e" s="T492">amndolaʔbəl?" </ts>
               <ts e="T494" id="Seg_234" n="e" s="T493">A </ts>
               <ts e="T495" id="Seg_236" n="e" s="T494">dĭ </ts>
               <ts e="T496" id="Seg_238" n="e" s="T495">măndə: </ts>
               <ts e="T497" id="Seg_240" n="e" s="T496">"tăn </ts>
               <ts e="T498" id="Seg_242" n="e" s="T497">kanaʔ!" </ts>
               <ts e="T499" id="Seg_244" n="e" s="T498">Ineʔi. </ts>
               <ts e="T500" id="Seg_246" n="e" s="T499">Dĭgəttə </ts>
               <ts e="T501" id="Seg_248" n="e" s="T500">dibər </ts>
               <ts e="T502" id="Seg_250" n="e" s="T501">- </ts>
               <ts e="T503" id="Seg_252" n="e" s="T502">davaj </ts>
               <ts e="T504" id="Seg_254" n="e" s="T503">meritʼ </ts>
               <ts e="T505" id="Seg_256" n="e" s="T504">- </ts>
               <ts e="T506" id="Seg_258" n="e" s="T505">sumna </ts>
               <ts e="T507" id="Seg_260" n="e" s="T506">abi. </ts>
               <ts e="T508" id="Seg_262" n="e" s="T507">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T468" id="Seg_263" s="T463">PKZ_196X_FiveHorses_flk.001 (001)</ta>
            <ta e="T475" id="Seg_264" s="T468">PKZ_196X_FiveHorses_flk.002 (002)</ta>
            <ta e="T480" id="Seg_265" s="T475">PKZ_196X_FiveHorses_flk.003 (003)</ta>
            <ta e="T487" id="Seg_266" s="T480">PKZ_196X_FiveHorses_flk.004 (004)</ta>
            <ta e="T493" id="Seg_267" s="T487">PKZ_196X_FiveHorses_flk.005 (005)</ta>
            <ta e="T498" id="Seg_268" s="T493">PKZ_196X_FiveHorses_flk.006 (006)</ta>
            <ta e="T499" id="Seg_269" s="T498">PKZ_196X_FiveHorses_flk.007 (007)</ta>
            <ta e="T507" id="Seg_270" s="T499">PKZ_196X_FiveHorses_flk.008 (008)</ta>
            <ta e="T508" id="Seg_271" s="T507">PKZ_196X_FiveHorses_flk.009 (009)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T468" id="Seg_272" s="T463">Külüksəbi kuza šobi koŋdə, amnolaʔbə. </ta>
            <ta e="T475" id="Seg_273" s="T468">Onʼiʔ kuza šobi i deʔpi svitok sazən. </ta>
            <ta e="T480" id="Seg_274" s="T475">Dĭ măndə: "Kanaʔ ineʔi, măndot". </ta>
            <ta e="T487" id="Seg_275" s="T480">A dĭ mĭmbi, mĭmbi i amnəbi koŋdə. </ta>
            <ta e="T493" id="Seg_276" s="T487">Dĭgəttə dĭ măndə: "ĭmbi döbər amndolaʔbəl?" </ta>
            <ta e="T498" id="Seg_277" s="T493">A dĭ măndə: "tăn kanaʔ!" </ta>
            <ta e="T499" id="Seg_278" s="T498">Ineʔi. </ta>
            <ta e="T507" id="Seg_279" s="T499">Dĭgəttə dibər - davaj meritʼ - sumna abi. </ta>
            <ta e="T508" id="Seg_280" s="T507">Kabarlə j. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T464" id="Seg_281" s="T463">külük-səbi</ta>
            <ta e="T465" id="Seg_282" s="T464">kuza</ta>
            <ta e="T466" id="Seg_283" s="T465">šo-bi</ta>
            <ta e="T467" id="Seg_284" s="T466">koŋ-də</ta>
            <ta e="T468" id="Seg_285" s="T467">amno-laʔbə</ta>
            <ta e="T469" id="Seg_286" s="T468">onʼiʔ</ta>
            <ta e="T470" id="Seg_287" s="T469">kuza</ta>
            <ta e="T471" id="Seg_288" s="T470">šo-bi</ta>
            <ta e="T472" id="Seg_289" s="T471">i</ta>
            <ta e="T473" id="Seg_290" s="T472">deʔ-pi</ta>
            <ta e="T474" id="Seg_291" s="T473">svitok</ta>
            <ta e="T475" id="Seg_292" s="T474">sazən</ta>
            <ta e="T476" id="Seg_293" s="T475">dĭ</ta>
            <ta e="T477" id="Seg_294" s="T476">măn-də</ta>
            <ta e="T478" id="Seg_295" s="T477">kan-a-ʔ</ta>
            <ta e="T479" id="Seg_296" s="T478">ine-ʔi</ta>
            <ta e="T480" id="Seg_297" s="T479">măndo-t</ta>
            <ta e="T481" id="Seg_298" s="T480">a</ta>
            <ta e="T482" id="Seg_299" s="T481">dĭ</ta>
            <ta e="T483" id="Seg_300" s="T482">mĭm-bi</ta>
            <ta e="T484" id="Seg_301" s="T483">mĭm-bi</ta>
            <ta e="T485" id="Seg_302" s="T484">i</ta>
            <ta e="T486" id="Seg_303" s="T485">amnə-bi</ta>
            <ta e="T487" id="Seg_304" s="T486">koŋ-də</ta>
            <ta e="T488" id="Seg_305" s="T487">dĭgəttə</ta>
            <ta e="T489" id="Seg_306" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_307" s="T489">măn-də</ta>
            <ta e="T491" id="Seg_308" s="T490">ĭmbi</ta>
            <ta e="T492" id="Seg_309" s="T491">döbər</ta>
            <ta e="T493" id="Seg_310" s="T492">amno-laʔbə-l</ta>
            <ta e="T494" id="Seg_311" s="T493">a</ta>
            <ta e="T495" id="Seg_312" s="T494">dĭ</ta>
            <ta e="T496" id="Seg_313" s="T495">măn-də</ta>
            <ta e="T497" id="Seg_314" s="T496">tăn</ta>
            <ta e="T498" id="Seg_315" s="T497">kan-a-ʔ</ta>
            <ta e="T499" id="Seg_316" s="T498">ine-ʔi</ta>
            <ta e="T500" id="Seg_317" s="T499">dĭgəttə</ta>
            <ta e="T501" id="Seg_318" s="T500">dibər</ta>
            <ta e="T506" id="Seg_319" s="T505">sumna</ta>
            <ta e="T507" id="Seg_320" s="T506">a-bi</ta>
            <ta e="T508" id="Seg_321" s="T507">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T464" id="Seg_322" s="T463">külük-zəbi</ta>
            <ta e="T465" id="Seg_323" s="T464">kuza</ta>
            <ta e="T466" id="Seg_324" s="T465">šo-bi</ta>
            <ta e="T467" id="Seg_325" s="T466">koŋ-Tə</ta>
            <ta e="T468" id="Seg_326" s="T467">amnə-laʔbə</ta>
            <ta e="T469" id="Seg_327" s="T468">onʼiʔ</ta>
            <ta e="T470" id="Seg_328" s="T469">kuza</ta>
            <ta e="T471" id="Seg_329" s="T470">šo-bi</ta>
            <ta e="T472" id="Seg_330" s="T471">i</ta>
            <ta e="T473" id="Seg_331" s="T472">det-bi</ta>
            <ta e="T474" id="Seg_332" s="T473">svitok</ta>
            <ta e="T475" id="Seg_333" s="T474">sazən</ta>
            <ta e="T476" id="Seg_334" s="T475">dĭ</ta>
            <ta e="T477" id="Seg_335" s="T476">măn-ntə</ta>
            <ta e="T478" id="Seg_336" s="T477">kan-ə-ʔ</ta>
            <ta e="T479" id="Seg_337" s="T478">ine-jəʔ</ta>
            <ta e="T480" id="Seg_338" s="T479">măndo-t</ta>
            <ta e="T481" id="Seg_339" s="T480">a</ta>
            <ta e="T482" id="Seg_340" s="T481">dĭ</ta>
            <ta e="T483" id="Seg_341" s="T482">mĭn-bi</ta>
            <ta e="T484" id="Seg_342" s="T483">mĭn-bi</ta>
            <ta e="T485" id="Seg_343" s="T484">i</ta>
            <ta e="T486" id="Seg_344" s="T485">amnə-bi</ta>
            <ta e="T487" id="Seg_345" s="T486">koŋ-Tə</ta>
            <ta e="T488" id="Seg_346" s="T487">dĭgəttə</ta>
            <ta e="T489" id="Seg_347" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_348" s="T489">măn-ntə</ta>
            <ta e="T491" id="Seg_349" s="T490">ĭmbi</ta>
            <ta e="T492" id="Seg_350" s="T491">döbər</ta>
            <ta e="T493" id="Seg_351" s="T492">amno-laʔbə-l</ta>
            <ta e="T494" id="Seg_352" s="T493">a</ta>
            <ta e="T495" id="Seg_353" s="T494">dĭ</ta>
            <ta e="T496" id="Seg_354" s="T495">măn-ntə</ta>
            <ta e="T497" id="Seg_355" s="T496">tăn</ta>
            <ta e="T498" id="Seg_356" s="T497">kan-ə-ʔ</ta>
            <ta e="T499" id="Seg_357" s="T498">ine-jəʔ</ta>
            <ta e="T500" id="Seg_358" s="T499">dĭgəttə</ta>
            <ta e="T501" id="Seg_359" s="T500">dĭbər</ta>
            <ta e="T506" id="Seg_360" s="T505">sumna</ta>
            <ta e="T507" id="Seg_361" s="T506">a-bi</ta>
            <ta e="T508" id="Seg_362" s="T507">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T464" id="Seg_363" s="T463">cunning-ADJZ.[NOM.SG]</ta>
            <ta e="T465" id="Seg_364" s="T464">man.[NOM.SG]</ta>
            <ta e="T466" id="Seg_365" s="T465">come-PST.[3SG]</ta>
            <ta e="T467" id="Seg_366" s="T466">chief-LAT</ta>
            <ta e="T468" id="Seg_367" s="T467">sit-DUR.[3SG]</ta>
            <ta e="T469" id="Seg_368" s="T468">one.[NOM.SG]</ta>
            <ta e="T470" id="Seg_369" s="T469">man.[NOM.SG]</ta>
            <ta e="T471" id="Seg_370" s="T470">come-PST.[3SG]</ta>
            <ta e="T472" id="Seg_371" s="T471">and</ta>
            <ta e="T473" id="Seg_372" s="T472">bring-PST.[3SG]</ta>
            <ta e="T474" id="Seg_373" s="T473">scroll.[NOM.SG]</ta>
            <ta e="T475" id="Seg_374" s="T474">paper.[NOM.SG]</ta>
            <ta e="T476" id="Seg_375" s="T475">this.[NOM.SG]</ta>
            <ta e="T477" id="Seg_376" s="T476">say-IPFVZ.[3SG]</ta>
            <ta e="T478" id="Seg_377" s="T477">go-EP-IMP.2SG</ta>
            <ta e="T479" id="Seg_378" s="T478">horse-PL</ta>
            <ta e="T480" id="Seg_379" s="T479">look-IMP.2SG.O</ta>
            <ta e="T481" id="Seg_380" s="T480">and</ta>
            <ta e="T482" id="Seg_381" s="T481">this.[NOM.SG]</ta>
            <ta e="T483" id="Seg_382" s="T482">go-PST.[3SG]</ta>
            <ta e="T484" id="Seg_383" s="T483">go-PST.[3SG]</ta>
            <ta e="T485" id="Seg_384" s="T484">and</ta>
            <ta e="T486" id="Seg_385" s="T485">sit.down-PST.[3SG]</ta>
            <ta e="T487" id="Seg_386" s="T486">chief-LAT</ta>
            <ta e="T488" id="Seg_387" s="T487">then</ta>
            <ta e="T489" id="Seg_388" s="T488">this.[NOM.SG]</ta>
            <ta e="T490" id="Seg_389" s="T489">say-IPFVZ.[3SG]</ta>
            <ta e="T491" id="Seg_390" s="T490">what.[NOM.SG]</ta>
            <ta e="T492" id="Seg_391" s="T491">here</ta>
            <ta e="T493" id="Seg_392" s="T492">sit-DUR-2SG</ta>
            <ta e="T494" id="Seg_393" s="T493">and</ta>
            <ta e="T495" id="Seg_394" s="T494">this.[NOM.SG]</ta>
            <ta e="T496" id="Seg_395" s="T495">say-IPFVZ.[3SG]</ta>
            <ta e="T497" id="Seg_396" s="T496">you.GEN</ta>
            <ta e="T498" id="Seg_397" s="T497">go-EP-IMP.2SG</ta>
            <ta e="T499" id="Seg_398" s="T498">horse-PL</ta>
            <ta e="T500" id="Seg_399" s="T499">then</ta>
            <ta e="T501" id="Seg_400" s="T500">there</ta>
            <ta e="T506" id="Seg_401" s="T505">five.[NOM.SG]</ta>
            <ta e="T507" id="Seg_402" s="T506">make-PST.[3SG]</ta>
            <ta e="T508" id="Seg_403" s="T507">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T464" id="Seg_404" s="T463">хитрый-ADJZ.[NOM.SG]</ta>
            <ta e="T465" id="Seg_405" s="T464">мужчина.[NOM.SG]</ta>
            <ta e="T466" id="Seg_406" s="T465">прийти-PST.[3SG]</ta>
            <ta e="T467" id="Seg_407" s="T466">вождь-LAT</ta>
            <ta e="T468" id="Seg_408" s="T467">сидеть-DUR.[3SG]</ta>
            <ta e="T469" id="Seg_409" s="T468">один.[NOM.SG]</ta>
            <ta e="T470" id="Seg_410" s="T469">мужчина.[NOM.SG]</ta>
            <ta e="T471" id="Seg_411" s="T470">прийти-PST.[3SG]</ta>
            <ta e="T472" id="Seg_412" s="T471">и</ta>
            <ta e="T473" id="Seg_413" s="T472">принести-PST.[3SG]</ta>
            <ta e="T474" id="Seg_414" s="T473">свиток.[NOM.SG]</ta>
            <ta e="T475" id="Seg_415" s="T474">бумага.[NOM.SG]</ta>
            <ta e="T476" id="Seg_416" s="T475">этот.[NOM.SG]</ta>
            <ta e="T477" id="Seg_417" s="T476">сказать-IPFVZ.[3SG]</ta>
            <ta e="T478" id="Seg_418" s="T477">пойти-EP-IMP.2SG</ta>
            <ta e="T479" id="Seg_419" s="T478">лошадь-PL</ta>
            <ta e="T480" id="Seg_420" s="T479">смотреть-IMP.2SG.O</ta>
            <ta e="T481" id="Seg_421" s="T480">а</ta>
            <ta e="T482" id="Seg_422" s="T481">этот.[NOM.SG]</ta>
            <ta e="T483" id="Seg_423" s="T482">идти-PST.[3SG]</ta>
            <ta e="T484" id="Seg_424" s="T483">идти-PST.[3SG]</ta>
            <ta e="T485" id="Seg_425" s="T484">и</ta>
            <ta e="T486" id="Seg_426" s="T485">сесть-PST.[3SG]</ta>
            <ta e="T487" id="Seg_427" s="T486">вождь-LAT</ta>
            <ta e="T488" id="Seg_428" s="T487">тогда</ta>
            <ta e="T489" id="Seg_429" s="T488">этот.[NOM.SG]</ta>
            <ta e="T490" id="Seg_430" s="T489">сказать-IPFVZ.[3SG]</ta>
            <ta e="T491" id="Seg_431" s="T490">что.[NOM.SG]</ta>
            <ta e="T492" id="Seg_432" s="T491">здесь</ta>
            <ta e="T493" id="Seg_433" s="T492">сидеть-DUR-2SG</ta>
            <ta e="T494" id="Seg_434" s="T493">а</ta>
            <ta e="T495" id="Seg_435" s="T494">этот.[NOM.SG]</ta>
            <ta e="T496" id="Seg_436" s="T495">сказать-IPFVZ.[3SG]</ta>
            <ta e="T497" id="Seg_437" s="T496">ты.GEN</ta>
            <ta e="T498" id="Seg_438" s="T497">пойти-EP-IMP.2SG</ta>
            <ta e="T499" id="Seg_439" s="T498">лошадь-PL</ta>
            <ta e="T500" id="Seg_440" s="T499">тогда</ta>
            <ta e="T501" id="Seg_441" s="T500">там</ta>
            <ta e="T506" id="Seg_442" s="T505">пять.[NOM.SG]</ta>
            <ta e="T507" id="Seg_443" s="T506">делать-PST.[3SG]</ta>
            <ta e="T508" id="Seg_444" s="T507">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T464" id="Seg_445" s="T463">adj-n&gt;adj-n:case</ta>
            <ta e="T465" id="Seg_446" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_447" s="T465">v-v:tense-v:pn</ta>
            <ta e="T467" id="Seg_448" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_449" s="T467">v-v&gt;v-v:pn</ta>
            <ta e="T469" id="Seg_450" s="T468">num-n:case</ta>
            <ta e="T470" id="Seg_451" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_452" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_453" s="T471">conj</ta>
            <ta e="T473" id="Seg_454" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_455" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_456" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_457" s="T475">dempro-n:case</ta>
            <ta e="T477" id="Seg_458" s="T476">v-v&gt;v-v:pn</ta>
            <ta e="T478" id="Seg_459" s="T477">v-v:ins-v:mood.pn</ta>
            <ta e="T479" id="Seg_460" s="T478">n-n:num</ta>
            <ta e="T480" id="Seg_461" s="T479">v-v:mood.pn</ta>
            <ta e="T481" id="Seg_462" s="T480">conj</ta>
            <ta e="T482" id="Seg_463" s="T481">dempro-n:case</ta>
            <ta e="T483" id="Seg_464" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_465" s="T483">v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_466" s="T484">conj</ta>
            <ta e="T486" id="Seg_467" s="T485">v-v:tense-v:pn</ta>
            <ta e="T487" id="Seg_468" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_469" s="T487">adv</ta>
            <ta e="T489" id="Seg_470" s="T488">dempro-n:case</ta>
            <ta e="T490" id="Seg_471" s="T489">v-v&gt;v-v:pn</ta>
            <ta e="T491" id="Seg_472" s="T490">que-n:case</ta>
            <ta e="T492" id="Seg_473" s="T491">adv</ta>
            <ta e="T493" id="Seg_474" s="T492">v-v&gt;v-v:pn</ta>
            <ta e="T494" id="Seg_475" s="T493">conj</ta>
            <ta e="T495" id="Seg_476" s="T494">dempro-n:case</ta>
            <ta e="T496" id="Seg_477" s="T495">v-v&gt;v-v:pn</ta>
            <ta e="T497" id="Seg_478" s="T496">pers</ta>
            <ta e="T498" id="Seg_479" s="T497">v-v:ins-v:mood.pn</ta>
            <ta e="T499" id="Seg_480" s="T498">n-n:num</ta>
            <ta e="T500" id="Seg_481" s="T499">adv</ta>
            <ta e="T501" id="Seg_482" s="T500">adv</ta>
            <ta e="T506" id="Seg_483" s="T505">num-n:case</ta>
            <ta e="T507" id="Seg_484" s="T506">v-v:tense-v:pn</ta>
            <ta e="T508" id="Seg_485" s="T507">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T464" id="Seg_486" s="T463">adj</ta>
            <ta e="T465" id="Seg_487" s="T464">n</ta>
            <ta e="T466" id="Seg_488" s="T465">v</ta>
            <ta e="T467" id="Seg_489" s="T466">n</ta>
            <ta e="T468" id="Seg_490" s="T467">v</ta>
            <ta e="T469" id="Seg_491" s="T468">num</ta>
            <ta e="T470" id="Seg_492" s="T469">n</ta>
            <ta e="T471" id="Seg_493" s="T470">v</ta>
            <ta e="T472" id="Seg_494" s="T471">conj</ta>
            <ta e="T473" id="Seg_495" s="T472">v</ta>
            <ta e="T474" id="Seg_496" s="T473">n</ta>
            <ta e="T475" id="Seg_497" s="T474">n</ta>
            <ta e="T476" id="Seg_498" s="T475">dempro</ta>
            <ta e="T477" id="Seg_499" s="T476">v</ta>
            <ta e="T478" id="Seg_500" s="T477">v</ta>
            <ta e="T479" id="Seg_501" s="T478">n</ta>
            <ta e="T480" id="Seg_502" s="T479">v</ta>
            <ta e="T481" id="Seg_503" s="T480">conj</ta>
            <ta e="T482" id="Seg_504" s="T481">dempro</ta>
            <ta e="T483" id="Seg_505" s="T482">v</ta>
            <ta e="T484" id="Seg_506" s="T483">v</ta>
            <ta e="T485" id="Seg_507" s="T484">conj</ta>
            <ta e="T486" id="Seg_508" s="T485">v</ta>
            <ta e="T487" id="Seg_509" s="T486">n</ta>
            <ta e="T488" id="Seg_510" s="T487">adv</ta>
            <ta e="T489" id="Seg_511" s="T488">dempro</ta>
            <ta e="T490" id="Seg_512" s="T489">v</ta>
            <ta e="T491" id="Seg_513" s="T490">que</ta>
            <ta e="T492" id="Seg_514" s="T491">adv</ta>
            <ta e="T493" id="Seg_515" s="T492">v</ta>
            <ta e="T494" id="Seg_516" s="T493">conj</ta>
            <ta e="T495" id="Seg_517" s="T494">dempro</ta>
            <ta e="T496" id="Seg_518" s="T495">v</ta>
            <ta e="T497" id="Seg_519" s="T496">pers</ta>
            <ta e="T498" id="Seg_520" s="T497">v</ta>
            <ta e="T499" id="Seg_521" s="T498">n</ta>
            <ta e="T500" id="Seg_522" s="T499">adv</ta>
            <ta e="T501" id="Seg_523" s="T500">adv</ta>
            <ta e="T506" id="Seg_524" s="T505">num</ta>
            <ta e="T507" id="Seg_525" s="T506">v</ta>
            <ta e="T508" id="Seg_526" s="T507">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T465" id="Seg_527" s="T464">np.h:A</ta>
            <ta e="T467" id="Seg_528" s="T466">np:G</ta>
            <ta e="T468" id="Seg_529" s="T467">0.3.h:A</ta>
            <ta e="T470" id="Seg_530" s="T469">np.h:A</ta>
            <ta e="T473" id="Seg_531" s="T472">0.3.h:A</ta>
            <ta e="T474" id="Seg_532" s="T473">np:Th</ta>
            <ta e="T476" id="Seg_533" s="T475">pro.h:A</ta>
            <ta e="T478" id="Seg_534" s="T477">0.2.h:A</ta>
            <ta e="T479" id="Seg_535" s="T478">np:Th</ta>
            <ta e="T480" id="Seg_536" s="T479">0.2.h:A</ta>
            <ta e="T482" id="Seg_537" s="T481">pro.h:A</ta>
            <ta e="T484" id="Seg_538" s="T483">0.3.h:A</ta>
            <ta e="T486" id="Seg_539" s="T485">0.3.h:E</ta>
            <ta e="T487" id="Seg_540" s="T486">np:G</ta>
            <ta e="T488" id="Seg_541" s="T487">adv:Time</ta>
            <ta e="T489" id="Seg_542" s="T488">pro.h:A</ta>
            <ta e="T492" id="Seg_543" s="T491">adv:L</ta>
            <ta e="T493" id="Seg_544" s="T492">0.2.h:E</ta>
            <ta e="T495" id="Seg_545" s="T494">pro.h:A</ta>
            <ta e="T497" id="Seg_546" s="T496">pro.h:A</ta>
            <ta e="T500" id="Seg_547" s="T499">adv:Time</ta>
            <ta e="T501" id="Seg_548" s="T500">adv:L</ta>
            <ta e="T506" id="Seg_549" s="T505">np:P</ta>
            <ta e="T507" id="Seg_550" s="T506">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T465" id="Seg_551" s="T464">np.h:S</ta>
            <ta e="T466" id="Seg_552" s="T465">v:pred</ta>
            <ta e="T468" id="Seg_553" s="T467">v:pred 0.3.h:S</ta>
            <ta e="T470" id="Seg_554" s="T469">np.h:S</ta>
            <ta e="T471" id="Seg_555" s="T470">v:pred</ta>
            <ta e="T473" id="Seg_556" s="T472">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_557" s="T473">np:O</ta>
            <ta e="T476" id="Seg_558" s="T475">pro.h:S</ta>
            <ta e="T477" id="Seg_559" s="T476">v:pred</ta>
            <ta e="T478" id="Seg_560" s="T477">v:pred 0.2.h:S</ta>
            <ta e="T479" id="Seg_561" s="T478">np:O</ta>
            <ta e="T480" id="Seg_562" s="T479">v:pred 0.2.h:S</ta>
            <ta e="T482" id="Seg_563" s="T481">pro.h:S</ta>
            <ta e="T483" id="Seg_564" s="T482">v:pred</ta>
            <ta e="T484" id="Seg_565" s="T483">v:pred 0.3.h:S</ta>
            <ta e="T486" id="Seg_566" s="T485">v:pred 0.3.h:S</ta>
            <ta e="T489" id="Seg_567" s="T488">pro.h:S</ta>
            <ta e="T490" id="Seg_568" s="T489">v:pred</ta>
            <ta e="T493" id="Seg_569" s="T492">v:pred 0.2.h:S</ta>
            <ta e="T495" id="Seg_570" s="T494">pro.h:S</ta>
            <ta e="T496" id="Seg_571" s="T495">v:pred</ta>
            <ta e="T497" id="Seg_572" s="T496">pro.h:S</ta>
            <ta e="T498" id="Seg_573" s="T497">v:pred</ta>
            <ta e="T506" id="Seg_574" s="T505">np:O</ta>
            <ta e="T507" id="Seg_575" s="T506">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T464" id="Seg_576" s="T463">TURK:core</ta>
            <ta e="T467" id="Seg_577" s="T466">TURK:cult</ta>
            <ta e="T472" id="Seg_578" s="T471">RUS:gram</ta>
            <ta e="T474" id="Seg_579" s="T473">RUS:cult</ta>
            <ta e="T481" id="Seg_580" s="T480">RUS:gram</ta>
            <ta e="T485" id="Seg_581" s="T484">RUS:gram</ta>
            <ta e="T487" id="Seg_582" s="T486">TURK:cult</ta>
            <ta e="T494" id="Seg_583" s="T493">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T491" id="Seg_584" s="T490">RUS:calq</ta>
            <ta e="T504" id="Seg_585" s="T502">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T468" id="Seg_586" s="T463">Хитрый человек пришёл к царю, он сидит.</ta>
            <ta e="T475" id="Seg_587" s="T468">Один человек пришёл и принёс свиток бумаги.</ta>
            <ta e="T480" id="Seg_588" s="T475">Он говорит: «Иди посмотри на лошадей».</ta>
            <ta e="T487" id="Seg_589" s="T480">А тот пошёл, пошёл и сел к царю.</ta>
            <ta e="T493" id="Seg_590" s="T487">Потом он говорит: «Ты что здесь сидишь?»</ta>
            <ta e="T498" id="Seg_591" s="T493">А тот говорит: «Ты иди!»</ta>
            <ta e="T499" id="Seg_592" s="T498">Лошади!</ta>
            <ta e="T507" id="Seg_593" s="T499">Потом там, давай мерить, сделал пять. [?]</ta>
            <ta e="T508" id="Seg_594" s="T507">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T468" id="Seg_595" s="T463">A cunning man came to the chief, he is sitting.</ta>
            <ta e="T475" id="Seg_596" s="T468">One man came and brought a scroll of paper.</ta>
            <ta e="T480" id="Seg_597" s="T475">He says: "Go look at the horses." (?)</ta>
            <ta e="T487" id="Seg_598" s="T480">But he went, went, and sat down at the chief's.</ta>
            <ta e="T493" id="Seg_599" s="T487">Then he said: “Why are you sitting here?”</ta>
            <ta e="T498" id="Seg_600" s="T493">But he says: “You go!”</ta>
            <ta e="T499" id="Seg_601" s="T498">Horses!</ta>
            <ta e="T507" id="Seg_602" s="T499">Then there, let me measure, made five. [?]</ta>
            <ta e="T508" id="Seg_603" s="T507">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T468" id="Seg_604" s="T463">Ein listiger Mann kam zum Oberhaupt, er sitzt.</ta>
            <ta e="T475" id="Seg_605" s="T468">Ein Mann kam und brachte eine Papier-Rolle.</ta>
            <ta e="T480" id="Seg_606" s="T475">Er sagt: „Geh schau dir die Pferde an.“ (?) </ta>
            <ta e="T487" id="Seg_607" s="T480">Aber er ging, ging, und setzte sich beim Oberhaupt.</ta>
            <ta e="T493" id="Seg_608" s="T487">Dann fragte er: „Warum sitzt du hier?“</ta>
            <ta e="T498" id="Seg_609" s="T493">Aber er sagt: „Du gehe!“</ta>
            <ta e="T499" id="Seg_610" s="T498">Pferde!</ta>
            <ta e="T507" id="Seg_611" s="T499">Dann dort, lass mich messen, machte fünf. [?]</ta>
            <ta e="T508" id="Seg_612" s="T507">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T475" id="Seg_613" s="T468">Свиток Ru. 'scroll'.</ta>
            <ta e="T480" id="Seg_614" s="T475">[GVY:] Or inəʔi 'bows'?</ta>
            <ta e="T499" id="Seg_615" s="T498">[KlT:] The whole story is uninterpretable.</ta>
            <ta e="T507" id="Seg_616" s="T499">[KlT:] Code switch ’дай мерить' - ’let me measure’. [GVY:] Or давай мерить.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
