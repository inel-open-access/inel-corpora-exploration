<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB0C8122B-6BA8-BF8E-8009-0E4D13EB21C0">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_TopsAndRoots_flk.wav" />
         <referenced-file url="PKZ_196X_TopsAndRoots_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_TopsAndRoots_flk\PKZ_196X_TopsAndRoots_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">179</ud-information>
            <ud-information attribute-name="# HIAT:w">111</ud-information>
            <ud-information attribute-name="# e">111</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2178" time="0.0" type="appl" />
         <tli id="T2179" time="0.717" type="appl" />
         <tli id="T2180" time="1.434" type="appl" />
         <tli id="T2181" time="2.151" type="appl" />
         <tli id="T2182" time="3.806623851453001" />
         <tli id="T2183" time="4.601" type="appl" />
         <tli id="T2184" time="5.183" type="appl" />
         <tli id="T2185" time="5.766" type="appl" />
         <tli id="T2186" time="6.43" type="appl" />
         <tli id="T2187" time="7.239918568612889" />
         <tli id="T2188" time="8.24" type="appl" />
         <tli id="T2189" time="9.506559741106793" />
         <tli id="T2190" time="10.068" type="appl" />
         <tli id="T2191" time="10.557" type="appl" />
         <tli id="T2192" time="11.046" type="appl" />
         <tli id="T2193" time="11.535" type="appl" />
         <tli id="T2194" time="12.024" type="appl" />
         <tli id="T2195" time="12.513" type="appl" />
         <tli id="T2196" time="13.002" type="appl" />
         <tli id="T2197" time="14.126507777984079" />
         <tli id="T2198" time="14.832" type="appl" />
         <tli id="T2199" time="15.451" type="appl" />
         <tli id="T2200" time="16.071" type="appl" />
         <tli id="T2201" time="16.69" type="appl" />
         <tli id="T2202" time="17.31" type="appl" />
         <tli id="T2203" time="17.929" type="appl" />
         <tli id="T2204" time="18.549" type="appl" />
         <tli id="T2205" time="19.168" type="appl" />
         <tli id="T2206" time="19.788" type="appl" />
         <tli id="T2207" time="20.407" type="appl" />
         <tli id="T2208" time="21.439758855118832" />
         <tli id="T2209" time="22.075" type="appl" />
         <tli id="T2210" time="22.714" type="appl" />
         <tli id="T2211" time="23.354" type="appl" />
         <tli id="T2212" time="24.313059870839048" />
         <tli id="T2213" time="25.474" type="appl" />
         <tli id="T2214" time="27.27302657844874" />
         <tli id="T2215" time="28.162" type="appl" />
         <tli id="T2216" time="29.626333442832113" />
         <tli id="T2217" time="30.648" type="appl" />
         <tli id="T2218" time="31.519" type="appl" />
         <tli id="T2219" time="32.389" type="appl" />
         <tli id="T2220" time="33.26" type="appl" />
         <tli id="T2221" time="34.63961038905393" />
         <tli id="T2222" time="35.699" type="appl" />
         <tli id="T2223" time="36.504" type="appl" />
         <tli id="T2224" time="37.308" type="appl" />
         <tli id="T2225" time="38.113" type="appl" />
         <tli id="T2226" time="38.917" type="appl" />
         <tli id="T2227" time="39.721" type="appl" />
         <tli id="T2228" time="40.526" type="appl" />
         <tli id="T2229" time="41.33" type="appl" />
         <tli id="T2230" time="42.135" type="appl" />
         <tli id="T2231" time="43.366178903155465" />
         <tli id="T2232" time="44.213" type="appl" />
         <tli id="T2233" time="44.816" type="appl" />
         <tli id="T2234" time="45.42" type="appl" />
         <tli id="T2235" time="45.894" type="appl" />
         <tli id="T2236" time="46.369" type="appl" />
         <tli id="T2237" time="46.844" type="appl" />
         <tli id="T2238" time="47.318" type="appl" />
         <tli id="T2239" time="47.758" type="appl" />
         <tli id="T2240" time="48.198" type="appl" />
         <tli id="T2241" time="48.84" type="appl" />
         <tli id="T2242" time="49.483" type="appl" />
         <tli id="T2243" time="50.95942683101005" />
         <tli id="T2244" time="51.86" type="appl" />
         <tli id="T2245" time="52.998" type="appl" />
         <tli id="T2246" time="54.135" type="appl" />
         <tli id="T2247" time="55.273" type="appl" />
         <tli id="T2248" time="56.41" type="appl" />
         <tli id="T2249" time="57.143" type="appl" />
         <tli id="T2250" time="57.876" type="appl" />
         <tli id="T2251" time="58.608" type="appl" />
         <tli id="T2252" time="59.341" type="appl" />
         <tli id="T2253" time="60.074" type="appl" />
         <tli id="T2254" time="60.807" type="appl" />
         <tli id="T2255" time="61.64" type="appl" />
         <tli id="T2256" time="62.337" type="appl" />
         <tli id="T2257" time="63.033" type="appl" />
         <tli id="T2258" time="63.965" type="appl" />
         <tli id="T2259" time="64.762" type="appl" />
         <tli id="T2260" time="65.558" type="appl" />
         <tli id="T2261" time="66.354" type="appl" />
         <tli id="T2262" time="66.823" type="appl" />
         <tli id="T2263" time="67.293" type="appl" />
         <tli id="T2264" time="67.762" type="appl" />
         <tli id="T2265" time="68.231" type="appl" />
         <tli id="T2266" time="68.7" type="appl" />
         <tli id="T2267" time="69.17" type="appl" />
         <tli id="T2268" time="69.639" type="appl" />
         <tli id="T2269" time="70.58" type="appl" />
         <tli id="T2270" time="71.52" type="appl" />
         <tli id="T2271" time="72.461" type="appl" />
         <tli id="T2272" time="73.471" type="appl" />
         <tli id="T2273" time="74.218" type="appl" />
         <tli id="T2274" time="74.67" type="appl" />
         <tli id="T2275" time="75.117" type="appl" />
         <tli id="T2276" time="75.564" type="appl" />
         <tli id="T2277" time="76.01" type="appl" />
         <tli id="T2278" time="76.456" type="appl" />
         <tli id="T2279" time="76.903" type="appl" />
         <tli id="T2280" time="77.925" type="appl" />
         <tli id="T2281" time="78.947" type="appl" />
         <tli id="T2282" time="79.969" type="appl" />
         <tli id="T2283" time="80.991" type="appl" />
         <tli id="T2284" time="82.012" type="appl" />
         <tli id="T2285" time="83.034" type="appl" />
         <tli id="T2286" time="84.056" type="appl" />
         <tli id="T2287" time="85.078" type="appl" />
         <tli id="T2288" time="86.50569368906154" />
         <tli id="T2289" time="87.519" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2289" id="Seg_0" n="sc" s="T2178">
               <ts e="T2182" id="Seg_2" n="HIAT:u" s="T2178">
                  <ts e="T2179" id="Seg_4" n="HIAT:w" s="T2178">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2180" id="Seg_7" n="HIAT:w" s="T2179">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2181" id="Seg_10" n="HIAT:w" s="T2180">kuʔpi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2182" id="Seg_13" n="HIAT:w" s="T2181">rʼepa</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2185" id="Seg_17" n="HIAT:u" s="T2182">
                  <ts e="T2183" id="Seg_19" n="HIAT:w" s="T2182">A</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2184" id="Seg_22" n="HIAT:w" s="T2183">urgaːba</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2185" id="Seg_25" n="HIAT:w" s="T2184">šobi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2187" id="Seg_29" n="HIAT:u" s="T2185">
                  <nts id="Seg_30" n="HIAT:ip">"</nts>
                  <ts e="T2186" id="Seg_32" n="HIAT:w" s="T2185">Ĭmbi</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2187" id="Seg_35" n="HIAT:w" s="T2186">alaʔbəl</ts>
                  <nts id="Seg_36" n="HIAT:ip">?</nts>
                  <nts id="Seg_37" n="HIAT:ip">"</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2189" id="Seg_40" n="HIAT:u" s="T2187">
                  <nts id="Seg_41" n="HIAT:ip">"</nts>
                  <ts e="T2188" id="Seg_43" n="HIAT:w" s="T2187">Kuʔlaʔbəm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2189" id="Seg_46" n="HIAT:w" s="T2188">rʼepa</ts>
                  <nts id="Seg_47" n="HIAT:ip">"</nts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2197" id="Seg_51" n="HIAT:u" s="T2189">
                  <nts id="Seg_52" n="HIAT:ip">"</nts>
                  <ts e="T2190" id="Seg_54" n="HIAT:w" s="T2189">Nu</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2191" id="Seg_57" n="HIAT:w" s="T2190">dak</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2192" id="Seg_60" n="HIAT:w" s="T2191">a</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2193" id="Seg_63" n="HIAT:w" s="T2192">măna</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T2194" id="Seg_67" n="HIAT:w" s="T2193">i-</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2195" id="Seg_71" n="HIAT:w" s="T2194">iʔ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_73" n="HIAT:ip">(</nts>
                  <ts e="T2196" id="Seg_75" n="HIAT:w" s="T2195">ele-</ts>
                  <nts id="Seg_76" n="HIAT:ip">)</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2197" id="Seg_79" n="HIAT:w" s="T2196">eleziʔ</ts>
                  <nts id="Seg_80" n="HIAT:ip">"</nts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2208" id="Seg_84" n="HIAT:u" s="T2197">
                  <nts id="Seg_85" n="HIAT:ip">"</nts>
                  <ts e="T2198" id="Seg_87" n="HIAT:w" s="T2197">Nu</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">(</nts>
                  <ts e="T2199" id="Seg_92" n="HIAT:w" s="T2198">m-</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2200" id="Seg_95" n="HIAT:w" s="T2199">m-</ts>
                  <nts id="Seg_96" n="HIAT:ip">)</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2201" id="Seg_99" n="HIAT:w" s="T2200">măn</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2202" id="Seg_102" n="HIAT:w" s="T2201">ilem</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2203" id="Seg_105" n="HIAT:w" s="T2202">kareški</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2204" id="Seg_109" n="HIAT:w" s="T2203">a</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_111" n="HIAT:ip">(</nts>
                  <ts e="T2205" id="Seg_113" n="HIAT:w" s="T2204">tăn=</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2206" id="Seg_117" n="HIAT:w" s="T2205">tănan</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2207" id="Seg_120" n="HIAT:w" s="T2206">molam</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2208" id="Seg_123" n="HIAT:w" s="T2207">verški</ts>
                  <nts id="Seg_124" n="HIAT:ip">"</nts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2212" id="Seg_128" n="HIAT:u" s="T2208">
                  <ts e="T2209" id="Seg_130" n="HIAT:w" s="T2208">Dĭgəttə</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2210" id="Seg_133" n="HIAT:w" s="T2209">urgaːba</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2211" id="Seg_136" n="HIAT:w" s="T2210">kalla</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2212" id="Seg_139" n="HIAT:w" s="T2211">dʼürbi</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2214" id="Seg_143" n="HIAT:u" s="T2212">
                  <ts e="T2213" id="Seg_145" n="HIAT:w" s="T2212">Rʼepat</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2214" id="Seg_148" n="HIAT:w" s="T2213">özerbi</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2216" id="Seg_152" n="HIAT:u" s="T2214">
                  <ts e="T2215" id="Seg_154" n="HIAT:w" s="T2214">Tibi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2216" id="Seg_157" n="HIAT:w" s="T2215">šobi</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2221" id="Seg_161" n="HIAT:u" s="T2216">
                  <ts e="T2217" id="Seg_163" n="HIAT:w" s="T2216">Rʼepabə</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2218" id="Seg_166" n="HIAT:w" s="T2217">nĭŋgəbi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2219" id="Seg_169" n="HIAT:w" s="T2218">i</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2220" id="Seg_172" n="HIAT:w" s="T2219">dĭʔnə</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2221" id="Seg_175" n="HIAT:w" s="T2220">mĭluʔpi</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2231" id="Seg_179" n="HIAT:u" s="T2221">
                  <ts e="T2222" id="Seg_181" n="HIAT:w" s="T2221">Dĭ</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <ts e="T2223" id="Seg_185" n="HIAT:w" s="T2222">e-</ts>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2224" id="Seg_189" n="HIAT:w" s="T2223">no</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2225" id="Seg_193" n="HIAT:w" s="T2224">a</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2226" id="Seg_196" n="HIAT:w" s="T2225">bostəkəndə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2227" id="Seg_199" n="HIAT:w" s="T2226">rʼepa</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2228" id="Seg_202" n="HIAT:w" s="T2227">ibi</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T2229" id="Seg_207" n="HIAT:w" s="T2228">ka-</ts>
                  <nts id="Seg_208" n="HIAT:ip">)</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2230" id="Seg_211" n="HIAT:w" s="T2229">kambi</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2231" id="Seg_214" n="HIAT:w" s="T2230">sadarzittə</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2234" id="Seg_218" n="HIAT:u" s="T2231">
                  <ts e="T2232" id="Seg_220" n="HIAT:w" s="T2231">A</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2233" id="Seg_223" n="HIAT:w" s="T2232">urgaːba</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2234" id="Seg_226" n="HIAT:w" s="T2233">šonəga</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2238" id="Seg_230" n="HIAT:u" s="T2234">
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <ts e="T2235" id="Seg_233" n="HIAT:w" s="T2234">Deʔ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2236" id="Seg_236" n="HIAT:w" s="T2235">măna</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2237" id="Seg_239" n="HIAT:w" s="T2236">onʼiʔ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2238" id="Seg_242" n="HIAT:w" s="T2237">amzittə</ts>
                  <nts id="Seg_243" n="HIAT:ip">!</nts>
                  <nts id="Seg_244" n="HIAT:ip">"</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2240" id="Seg_247" n="HIAT:u" s="T2238">
                  <ts e="T2239" id="Seg_249" n="HIAT:w" s="T2238">Dĭ</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2240" id="Seg_252" n="HIAT:w" s="T2239">mĭbi</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2243" id="Seg_256" n="HIAT:u" s="T2240">
                  <nts id="Seg_257" n="HIAT:ip">"</nts>
                  <ts e="T2241" id="Seg_259" n="HIAT:w" s="T2240">O</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2242" id="Seg_263" n="HIAT:w" s="T2241">girgit</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2243" id="Seg_266" n="HIAT:w" s="T2242">nʼamga</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2248" id="Seg_270" n="HIAT:u" s="T2243">
                  <ts e="T2244" id="Seg_272" n="HIAT:w" s="T2243">Măn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2245" id="Seg_275" n="HIAT:w" s="T2244">tănan</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2246" id="Seg_278" n="HIAT:w" s="T2245">tüj</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2247" id="Seg_281" n="HIAT:w" s="T2246">bar</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2248" id="Seg_284" n="HIAT:w" s="T2247">amluʔpim</ts>
                  <nts id="Seg_285" n="HIAT:ip">!</nts>
                  <nts id="Seg_286" n="HIAT:ip">"</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2254" id="Seg_289" n="HIAT:u" s="T2248">
                  <ts e="T2249" id="Seg_291" n="HIAT:w" s="T2248">Dĭgəttə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2250" id="Seg_294" n="HIAT:w" s="T2249">dĭ</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ts e="T2251" id="Seg_298" n="HIAT:w" s="T2250">bar=</ts>
                  <nts id="Seg_299" n="HIAT:ip">)</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2252" id="Seg_302" n="HIAT:w" s="T2251">davaj</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2253" id="Seg_305" n="HIAT:w" s="T2252">aš</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2254" id="Seg_308" n="HIAT:w" s="T2253">kuʔsittə</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2257" id="Seg_312" n="HIAT:u" s="T2254">
                  <ts e="T2255" id="Seg_314" n="HIAT:w" s="T2254">Bazoʔ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2256" id="Seg_317" n="HIAT:w" s="T2255">urgaːba</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2257" id="Seg_320" n="HIAT:w" s="T2256">šobi</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2261" id="Seg_324" n="HIAT:u" s="T2257">
                  <nts id="Seg_325" n="HIAT:ip">"</nts>
                  <ts e="T2258" id="Seg_327" n="HIAT:w" s="T2257">No</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2259" id="Seg_331" n="HIAT:w" s="T2258">mĭlel</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2260" id="Seg_334" n="HIAT:w" s="T2259">măna</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2261" id="Seg_337" n="HIAT:w" s="T2260">ĭmbi</ts>
                  <nts id="Seg_338" n="HIAT:ip">?</nts>
                  <nts id="Seg_339" n="HIAT:ip">"</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2268" id="Seg_342" n="HIAT:u" s="T2261">
                  <nts id="Seg_343" n="HIAT:ip">"</nts>
                  <ts e="T2262" id="Seg_345" n="HIAT:w" s="T2261">No</ts>
                  <nts id="Seg_346" n="HIAT:ip">,</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2263" id="Seg_349" n="HIAT:w" s="T2262">iʔ</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2264" id="Seg_352" n="HIAT:w" s="T2263">kareški</ts>
                  <nts id="Seg_353" n="HIAT:ip">,</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2265" id="Seg_356" n="HIAT:w" s="T2264">a</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2266" id="Seg_359" n="HIAT:w" s="T2265">măn</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2267" id="Seg_362" n="HIAT:w" s="T2266">ilem</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2268" id="Seg_365" n="HIAT:w" s="T2267">verški</ts>
                  <nts id="Seg_366" n="HIAT:ip">"</nts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2271" id="Seg_370" n="HIAT:u" s="T2268">
                  <ts e="T2269" id="Seg_372" n="HIAT:w" s="T2268">Dĭgəttə</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2270" id="Seg_375" n="HIAT:w" s="T2269">dĭ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2271" id="Seg_378" n="HIAT:w" s="T2270">iluʔpi</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2273" id="Seg_382" n="HIAT:u" s="T2271">
                  <ts e="T2272" id="Seg_384" n="HIAT:w" s="T2271">Aš</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2273" id="Seg_387" n="HIAT:w" s="T2272">konluʔpi</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2279" id="Seg_391" n="HIAT:u" s="T2273">
                  <ts e="T2274" id="Seg_393" n="HIAT:w" s="T2273">A</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2275" id="Seg_396" n="HIAT:w" s="T2274">dĭ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2276" id="Seg_399" n="HIAT:w" s="T2275">ĭmbidə</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2277" id="Seg_402" n="HIAT:w" s="T2276">ej</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2278" id="Seg_405" n="HIAT:w" s="T2277">mobi</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2279" id="Seg_408" n="HIAT:w" s="T2278">azittə</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2288" id="Seg_412" n="HIAT:u" s="T2279">
                  <ts e="T2280" id="Seg_414" n="HIAT:w" s="T2279">Dĭgəttə</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_416" n="HIAT:ip">(</nts>
                  <ts e="T2281" id="Seg_418" n="HIAT:w" s="T2280">dĭzeŋ=</ts>
                  <nts id="Seg_419" n="HIAT:ip">)</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2282" id="Seg_422" n="HIAT:w" s="T2281">külaːmbi</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_424" n="HIAT:ip">(</nts>
                  <ts e="T2283" id="Seg_426" n="HIAT:w" s="T2282">dĭn=</ts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2284" id="Seg_430" n="HIAT:w" s="T2283">dĭzeŋ</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2285" id="Seg_433" n="HIAT:w" s="T2284">družbat</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2286" id="Seg_437" n="HIAT:w" s="T2285">urgaːba</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2287" id="Seg_440" n="HIAT:w" s="T2286">kuroluʔpi</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2288" id="Seg_443" n="HIAT:w" s="T2287">tibinə</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2289" id="Seg_447" n="HIAT:u" s="T2288">
                  <ts e="T2289" id="Seg_449" n="HIAT:w" s="T2288">Kabarləj</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2289" id="Seg_452" n="sc" s="T2178">
               <ts e="T2179" id="Seg_454" n="e" s="T2178">Onʼiʔ </ts>
               <ts e="T2180" id="Seg_456" n="e" s="T2179">kuza </ts>
               <ts e="T2181" id="Seg_458" n="e" s="T2180">kuʔpi </ts>
               <ts e="T2182" id="Seg_460" n="e" s="T2181">rʼepa. </ts>
               <ts e="T2183" id="Seg_462" n="e" s="T2182">A </ts>
               <ts e="T2184" id="Seg_464" n="e" s="T2183">urgaːba </ts>
               <ts e="T2185" id="Seg_466" n="e" s="T2184">šobi. </ts>
               <ts e="T2186" id="Seg_468" n="e" s="T2185">"Ĭmbi </ts>
               <ts e="T2187" id="Seg_470" n="e" s="T2186">alaʔbəl?" </ts>
               <ts e="T2188" id="Seg_472" n="e" s="T2187">"Kuʔlaʔbəm </ts>
               <ts e="T2189" id="Seg_474" n="e" s="T2188">rʼepa". </ts>
               <ts e="T2190" id="Seg_476" n="e" s="T2189">"Nu </ts>
               <ts e="T2191" id="Seg_478" n="e" s="T2190">dak </ts>
               <ts e="T2192" id="Seg_480" n="e" s="T2191">a </ts>
               <ts e="T2193" id="Seg_482" n="e" s="T2192">măna </ts>
               <ts e="T2194" id="Seg_484" n="e" s="T2193">(i-) </ts>
               <ts e="T2195" id="Seg_486" n="e" s="T2194">iʔ </ts>
               <ts e="T2196" id="Seg_488" n="e" s="T2195">(ele-) </ts>
               <ts e="T2197" id="Seg_490" n="e" s="T2196">eleziʔ". </ts>
               <ts e="T2198" id="Seg_492" n="e" s="T2197">"Nu, </ts>
               <ts e="T2199" id="Seg_494" n="e" s="T2198">(m- </ts>
               <ts e="T2200" id="Seg_496" n="e" s="T2199">m-) </ts>
               <ts e="T2201" id="Seg_498" n="e" s="T2200">măn </ts>
               <ts e="T2202" id="Seg_500" n="e" s="T2201">ilem </ts>
               <ts e="T2203" id="Seg_502" n="e" s="T2202">kareški, </ts>
               <ts e="T2204" id="Seg_504" n="e" s="T2203">a </ts>
               <ts e="T2205" id="Seg_506" n="e" s="T2204">(tăn=) </ts>
               <ts e="T2206" id="Seg_508" n="e" s="T2205">tănan </ts>
               <ts e="T2207" id="Seg_510" n="e" s="T2206">molam </ts>
               <ts e="T2208" id="Seg_512" n="e" s="T2207">verški". </ts>
               <ts e="T2209" id="Seg_514" n="e" s="T2208">Dĭgəttə </ts>
               <ts e="T2210" id="Seg_516" n="e" s="T2209">urgaːba </ts>
               <ts e="T2211" id="Seg_518" n="e" s="T2210">kalla </ts>
               <ts e="T2212" id="Seg_520" n="e" s="T2211">dʼürbi. </ts>
               <ts e="T2213" id="Seg_522" n="e" s="T2212">Rʼepat </ts>
               <ts e="T2214" id="Seg_524" n="e" s="T2213">özerbi. </ts>
               <ts e="T2215" id="Seg_526" n="e" s="T2214">Tibi </ts>
               <ts e="T2216" id="Seg_528" n="e" s="T2215">šobi. </ts>
               <ts e="T2217" id="Seg_530" n="e" s="T2216">Rʼepabə </ts>
               <ts e="T2218" id="Seg_532" n="e" s="T2217">nĭŋgəbi </ts>
               <ts e="T2219" id="Seg_534" n="e" s="T2218">i </ts>
               <ts e="T2220" id="Seg_536" n="e" s="T2219">dĭʔnə </ts>
               <ts e="T2221" id="Seg_538" n="e" s="T2220">mĭluʔpi. </ts>
               <ts e="T2222" id="Seg_540" n="e" s="T2221">Dĭ </ts>
               <ts e="T2223" id="Seg_542" n="e" s="T2222">(e-) </ts>
               <ts e="T2224" id="Seg_544" n="e" s="T2223">no, </ts>
               <ts e="T2225" id="Seg_546" n="e" s="T2224">a </ts>
               <ts e="T2226" id="Seg_548" n="e" s="T2225">bostəkəndə </ts>
               <ts e="T2227" id="Seg_550" n="e" s="T2226">rʼepa </ts>
               <ts e="T2228" id="Seg_552" n="e" s="T2227">ibi, </ts>
               <ts e="T2229" id="Seg_554" n="e" s="T2228">(ka-) </ts>
               <ts e="T2230" id="Seg_556" n="e" s="T2229">kambi </ts>
               <ts e="T2231" id="Seg_558" n="e" s="T2230">sadarzittə. </ts>
               <ts e="T2232" id="Seg_560" n="e" s="T2231">A </ts>
               <ts e="T2233" id="Seg_562" n="e" s="T2232">urgaːba </ts>
               <ts e="T2234" id="Seg_564" n="e" s="T2233">šonəga. </ts>
               <ts e="T2235" id="Seg_566" n="e" s="T2234">"Deʔ </ts>
               <ts e="T2236" id="Seg_568" n="e" s="T2235">măna </ts>
               <ts e="T2237" id="Seg_570" n="e" s="T2236">onʼiʔ </ts>
               <ts e="T2238" id="Seg_572" n="e" s="T2237">amzittə!" </ts>
               <ts e="T2239" id="Seg_574" n="e" s="T2238">Dĭ </ts>
               <ts e="T2240" id="Seg_576" n="e" s="T2239">mĭbi. </ts>
               <ts e="T2241" id="Seg_578" n="e" s="T2240">"O, </ts>
               <ts e="T2242" id="Seg_580" n="e" s="T2241">girgit </ts>
               <ts e="T2243" id="Seg_582" n="e" s="T2242">nʼamga. </ts>
               <ts e="T2244" id="Seg_584" n="e" s="T2243">Măn </ts>
               <ts e="T2245" id="Seg_586" n="e" s="T2244">tănan </ts>
               <ts e="T2246" id="Seg_588" n="e" s="T2245">tüj </ts>
               <ts e="T2247" id="Seg_590" n="e" s="T2246">bar </ts>
               <ts e="T2248" id="Seg_592" n="e" s="T2247">amluʔpim!" </ts>
               <ts e="T2249" id="Seg_594" n="e" s="T2248">Dĭgəttə </ts>
               <ts e="T2250" id="Seg_596" n="e" s="T2249">dĭ </ts>
               <ts e="T2251" id="Seg_598" n="e" s="T2250">(bar=) </ts>
               <ts e="T2252" id="Seg_600" n="e" s="T2251">davaj </ts>
               <ts e="T2253" id="Seg_602" n="e" s="T2252">aš </ts>
               <ts e="T2254" id="Seg_604" n="e" s="T2253">kuʔsittə. </ts>
               <ts e="T2255" id="Seg_606" n="e" s="T2254">Bazoʔ </ts>
               <ts e="T2256" id="Seg_608" n="e" s="T2255">urgaːba </ts>
               <ts e="T2257" id="Seg_610" n="e" s="T2256">šobi. </ts>
               <ts e="T2258" id="Seg_612" n="e" s="T2257">"No, </ts>
               <ts e="T2259" id="Seg_614" n="e" s="T2258">mĭlel </ts>
               <ts e="T2260" id="Seg_616" n="e" s="T2259">măna </ts>
               <ts e="T2261" id="Seg_618" n="e" s="T2260">ĭmbi?" </ts>
               <ts e="T2262" id="Seg_620" n="e" s="T2261">"No, </ts>
               <ts e="T2263" id="Seg_622" n="e" s="T2262">iʔ </ts>
               <ts e="T2264" id="Seg_624" n="e" s="T2263">kareški, </ts>
               <ts e="T2265" id="Seg_626" n="e" s="T2264">a </ts>
               <ts e="T2266" id="Seg_628" n="e" s="T2265">măn </ts>
               <ts e="T2267" id="Seg_630" n="e" s="T2266">ilem </ts>
               <ts e="T2268" id="Seg_632" n="e" s="T2267">verški". </ts>
               <ts e="T2269" id="Seg_634" n="e" s="T2268">Dĭgəttə </ts>
               <ts e="T2270" id="Seg_636" n="e" s="T2269">dĭ </ts>
               <ts e="T2271" id="Seg_638" n="e" s="T2270">iluʔpi. </ts>
               <ts e="T2272" id="Seg_640" n="e" s="T2271">Aš </ts>
               <ts e="T2273" id="Seg_642" n="e" s="T2272">konluʔpi. </ts>
               <ts e="T2274" id="Seg_644" n="e" s="T2273">A </ts>
               <ts e="T2275" id="Seg_646" n="e" s="T2274">dĭ </ts>
               <ts e="T2276" id="Seg_648" n="e" s="T2275">ĭmbidə </ts>
               <ts e="T2277" id="Seg_650" n="e" s="T2276">ej </ts>
               <ts e="T2278" id="Seg_652" n="e" s="T2277">mobi </ts>
               <ts e="T2279" id="Seg_654" n="e" s="T2278">azittə. </ts>
               <ts e="T2280" id="Seg_656" n="e" s="T2279">Dĭgəttə </ts>
               <ts e="T2281" id="Seg_658" n="e" s="T2280">(dĭzeŋ=) </ts>
               <ts e="T2282" id="Seg_660" n="e" s="T2281">külaːmbi </ts>
               <ts e="T2283" id="Seg_662" n="e" s="T2282">(dĭn=) </ts>
               <ts e="T2284" id="Seg_664" n="e" s="T2283">dĭzeŋ </ts>
               <ts e="T2285" id="Seg_666" n="e" s="T2284">družbat, </ts>
               <ts e="T2286" id="Seg_668" n="e" s="T2285">urgaːba </ts>
               <ts e="T2287" id="Seg_670" n="e" s="T2286">kuroluʔpi </ts>
               <ts e="T2288" id="Seg_672" n="e" s="T2287">tibinə. </ts>
               <ts e="T2289" id="Seg_674" n="e" s="T2288">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2182" id="Seg_675" s="T2178">PKZ_196X_TopsAndRoots_flk.001 (001)</ta>
            <ta e="T2185" id="Seg_676" s="T2182">PKZ_196X_TopsAndRoots_flk.002 (002)</ta>
            <ta e="T2187" id="Seg_677" s="T2185">PKZ_196X_TopsAndRoots_flk.003 (003)</ta>
            <ta e="T2189" id="Seg_678" s="T2187">PKZ_196X_TopsAndRoots_flk.004 (004)</ta>
            <ta e="T2197" id="Seg_679" s="T2189">PKZ_196X_TopsAndRoots_flk.005 (005)</ta>
            <ta e="T2208" id="Seg_680" s="T2197">PKZ_196X_TopsAndRoots_flk.006 (006)</ta>
            <ta e="T2212" id="Seg_681" s="T2208">PKZ_196X_TopsAndRoots_flk.007 (007)</ta>
            <ta e="T2214" id="Seg_682" s="T2212">PKZ_196X_TopsAndRoots_flk.008 (008)</ta>
            <ta e="T2216" id="Seg_683" s="T2214">PKZ_196X_TopsAndRoots_flk.009 (009)</ta>
            <ta e="T2221" id="Seg_684" s="T2216">PKZ_196X_TopsAndRoots_flk.010 (010)</ta>
            <ta e="T2231" id="Seg_685" s="T2221">PKZ_196X_TopsAndRoots_flk.011 (011)</ta>
            <ta e="T2234" id="Seg_686" s="T2231">PKZ_196X_TopsAndRoots_flk.012 (012)</ta>
            <ta e="T2238" id="Seg_687" s="T2234">PKZ_196X_TopsAndRoots_flk.013 (013)</ta>
            <ta e="T2240" id="Seg_688" s="T2238">PKZ_196X_TopsAndRoots_flk.014 (014)</ta>
            <ta e="T2243" id="Seg_689" s="T2240">PKZ_196X_TopsAndRoots_flk.015 (015)</ta>
            <ta e="T2248" id="Seg_690" s="T2243">PKZ_196X_TopsAndRoots_flk.016 (016)</ta>
            <ta e="T2254" id="Seg_691" s="T2248">PKZ_196X_TopsAndRoots_flk.017 (017)</ta>
            <ta e="T2257" id="Seg_692" s="T2254">PKZ_196X_TopsAndRoots_flk.018 (018)</ta>
            <ta e="T2261" id="Seg_693" s="T2257">PKZ_196X_TopsAndRoots_flk.019 (019)</ta>
            <ta e="T2268" id="Seg_694" s="T2261">PKZ_196X_TopsAndRoots_flk.020 (020)</ta>
            <ta e="T2271" id="Seg_695" s="T2268">PKZ_196X_TopsAndRoots_flk.021 (021)</ta>
            <ta e="T2273" id="Seg_696" s="T2271">PKZ_196X_TopsAndRoots_flk.022 (022)</ta>
            <ta e="T2279" id="Seg_697" s="T2273">PKZ_196X_TopsAndRoots_flk.023 (023)</ta>
            <ta e="T2288" id="Seg_698" s="T2279">PKZ_196X_TopsAndRoots_flk.024 (024)</ta>
            <ta e="T2289" id="Seg_699" s="T2288">PKZ_196X_TopsAndRoots_flk.025 (025)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2182" id="Seg_700" s="T2178">Onʼiʔ kuza kuʔpi rʼepa. </ta>
            <ta e="T2185" id="Seg_701" s="T2182">A urgaːba šobi. </ta>
            <ta e="T2187" id="Seg_702" s="T2185">"Ĭmbi alaʔbəl?" </ta>
            <ta e="T2189" id="Seg_703" s="T2187">"Kuʔlaʔbəm rʼepa". </ta>
            <ta e="T2197" id="Seg_704" s="T2189">"Nu dak a măna (i-) iʔ (ele-) eleziʔ". </ta>
            <ta e="T2208" id="Seg_705" s="T2197">"Nu, (m- m-) măn ilem kareški, a (tăn=) tănan molam verški". </ta>
            <ta e="T2212" id="Seg_706" s="T2208">Dĭgəttə urgaːba kalla dʼürbi. </ta>
            <ta e="T2214" id="Seg_707" s="T2212">Rʼepat özerbi. </ta>
            <ta e="T2216" id="Seg_708" s="T2214">Tibi šobi. </ta>
            <ta e="T2221" id="Seg_709" s="T2216">Rʼepabə nĭŋgəbi i dĭʔnə mĭluʔpi. </ta>
            <ta e="T2231" id="Seg_710" s="T2221">Dĭ (e-) no, a bostəkəndə rʼepa ibi, (ka-) kambi sadarzittə. </ta>
            <ta e="T2234" id="Seg_711" s="T2231">A urgaːba šonəga. </ta>
            <ta e="T2238" id="Seg_712" s="T2234">"Deʔ măna onʼiʔ amzittə!" </ta>
            <ta e="T2240" id="Seg_713" s="T2238">Dĭ mĭbi. </ta>
            <ta e="T2243" id="Seg_714" s="T2240">"O, girgit nʼamga. </ta>
            <ta e="T2248" id="Seg_715" s="T2243">Măn tănan tüj bar amluʔpim!" </ta>
            <ta e="T2254" id="Seg_716" s="T2248">Dĭgəttə dĭ (bar=) davaj aš kuʔsittə. </ta>
            <ta e="T2257" id="Seg_717" s="T2254">Bazoʔ urgaːba šobi. </ta>
            <ta e="T2261" id="Seg_718" s="T2257">"No, mĭlel măna ĭmbi?" </ta>
            <ta e="T2268" id="Seg_719" s="T2261">"No, iʔ kareški, a măn ilem verški". </ta>
            <ta e="T2271" id="Seg_720" s="T2268">Dĭgəttə dĭ iluʔpi. </ta>
            <ta e="T2273" id="Seg_721" s="T2271">Aš konluʔpi. </ta>
            <ta e="T2279" id="Seg_722" s="T2273">A dĭ ĭmbidə ej mobi azittə. </ta>
            <ta e="T2288" id="Seg_723" s="T2279">Dĭgəttə (dĭzeŋ=) külaːmbi (dĭn=) dĭzeŋ družbat, urgaːba kuroluʔpi tibinə. </ta>
            <ta e="T2289" id="Seg_724" s="T2288">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2179" id="Seg_725" s="T2178">onʼiʔ</ta>
            <ta e="T2180" id="Seg_726" s="T2179">kuza</ta>
            <ta e="T2181" id="Seg_727" s="T2180">kuʔ-pi</ta>
            <ta e="T2182" id="Seg_728" s="T2181">rʼepa</ta>
            <ta e="T2183" id="Seg_729" s="T2182">a</ta>
            <ta e="T2184" id="Seg_730" s="T2183">urgaːba</ta>
            <ta e="T2185" id="Seg_731" s="T2184">šo-bi</ta>
            <ta e="T2186" id="Seg_732" s="T2185">ĭmbi</ta>
            <ta e="T2187" id="Seg_733" s="T2186">a-laʔbə-l</ta>
            <ta e="T2188" id="Seg_734" s="T2187">kuʔ-laʔbə-m</ta>
            <ta e="T2189" id="Seg_735" s="T2188">rʼepa</ta>
            <ta e="T2190" id="Seg_736" s="T2189">nu</ta>
            <ta e="T2191" id="Seg_737" s="T2190">dak</ta>
            <ta e="T2192" id="Seg_738" s="T2191">a</ta>
            <ta e="T2193" id="Seg_739" s="T2192">măna</ta>
            <ta e="T2195" id="Seg_740" s="T2194">i-ʔ</ta>
            <ta e="T2197" id="Seg_741" s="T2196">ele-ziʔ</ta>
            <ta e="T2198" id="Seg_742" s="T2197">nu</ta>
            <ta e="T2201" id="Seg_743" s="T2200">măn</ta>
            <ta e="T2202" id="Seg_744" s="T2201">i-le-m</ta>
            <ta e="T2203" id="Seg_745" s="T2202">kareški</ta>
            <ta e="T2204" id="Seg_746" s="T2203">a</ta>
            <ta e="T2205" id="Seg_747" s="T2204">tăn</ta>
            <ta e="T2206" id="Seg_748" s="T2205">tănan</ta>
            <ta e="T2207" id="Seg_749" s="T2206">mo-la-m</ta>
            <ta e="T2208" id="Seg_750" s="T2207">verški</ta>
            <ta e="T2209" id="Seg_751" s="T2208">dĭgəttə</ta>
            <ta e="T2210" id="Seg_752" s="T2209">urgaːba</ta>
            <ta e="T2211" id="Seg_753" s="T2210">kal-la</ta>
            <ta e="T2212" id="Seg_754" s="T2211">dʼür-bi</ta>
            <ta e="T2213" id="Seg_755" s="T2212">rʼepa-t</ta>
            <ta e="T2214" id="Seg_756" s="T2213">özer-bi</ta>
            <ta e="T2215" id="Seg_757" s="T2214">tibi</ta>
            <ta e="T2216" id="Seg_758" s="T2215">šo-bi</ta>
            <ta e="T2217" id="Seg_759" s="T2216">rʼepa-bə</ta>
            <ta e="T2218" id="Seg_760" s="T2217">nĭŋgə-bi</ta>
            <ta e="T2219" id="Seg_761" s="T2218">i</ta>
            <ta e="T2220" id="Seg_762" s="T2219">dĭʔ-nə</ta>
            <ta e="T2221" id="Seg_763" s="T2220">mĭ-luʔ-pi</ta>
            <ta e="T2222" id="Seg_764" s="T2221">dĭ</ta>
            <ta e="T2224" id="Seg_765" s="T2223">no</ta>
            <ta e="T2225" id="Seg_766" s="T2224">a</ta>
            <ta e="T2226" id="Seg_767" s="T2225">bostə-kəndə</ta>
            <ta e="T2227" id="Seg_768" s="T2226">rʼepa</ta>
            <ta e="T2228" id="Seg_769" s="T2227">i-bi</ta>
            <ta e="T2230" id="Seg_770" s="T2229">kam-bi</ta>
            <ta e="T2231" id="Seg_771" s="T2230">sadar-zittə</ta>
            <ta e="T2232" id="Seg_772" s="T2231">a</ta>
            <ta e="T2233" id="Seg_773" s="T2232">urgaːba</ta>
            <ta e="T2234" id="Seg_774" s="T2233">šonə-ga</ta>
            <ta e="T2235" id="Seg_775" s="T2234">de-ʔ</ta>
            <ta e="T2236" id="Seg_776" s="T2235">măna</ta>
            <ta e="T2237" id="Seg_777" s="T2236">onʼiʔ</ta>
            <ta e="T2238" id="Seg_778" s="T2237">am-zittə</ta>
            <ta e="T2239" id="Seg_779" s="T2238">dĭ</ta>
            <ta e="T2240" id="Seg_780" s="T2239">mĭ-bi</ta>
            <ta e="T2241" id="Seg_781" s="T2240">o</ta>
            <ta e="T2242" id="Seg_782" s="T2241">girgit</ta>
            <ta e="T2243" id="Seg_783" s="T2242">nʼamga</ta>
            <ta e="T2244" id="Seg_784" s="T2243">măn</ta>
            <ta e="T2245" id="Seg_785" s="T2244">tănan</ta>
            <ta e="T2246" id="Seg_786" s="T2245">tüj</ta>
            <ta e="T2247" id="Seg_787" s="T2246">bar</ta>
            <ta e="T2248" id="Seg_788" s="T2247">am-luʔ-pi-m</ta>
            <ta e="T2249" id="Seg_789" s="T2248">dĭgəttə</ta>
            <ta e="T2250" id="Seg_790" s="T2249">dĭ</ta>
            <ta e="T2251" id="Seg_791" s="T2250">bar</ta>
            <ta e="T2252" id="Seg_792" s="T2251">davaj</ta>
            <ta e="T2253" id="Seg_793" s="T2252">aš</ta>
            <ta e="T2254" id="Seg_794" s="T2253">kuʔ-sittə</ta>
            <ta e="T2255" id="Seg_795" s="T2254">bazoʔ</ta>
            <ta e="T2256" id="Seg_796" s="T2255">urgaːba</ta>
            <ta e="T2257" id="Seg_797" s="T2256">šo-bi</ta>
            <ta e="T2258" id="Seg_798" s="T2257">no</ta>
            <ta e="T2259" id="Seg_799" s="T2258">mĭ-le-l</ta>
            <ta e="T2260" id="Seg_800" s="T2259">măna</ta>
            <ta e="T2261" id="Seg_801" s="T2260">ĭmbi</ta>
            <ta e="T2262" id="Seg_802" s="T2261">no</ta>
            <ta e="T2263" id="Seg_803" s="T2262">i-ʔ</ta>
            <ta e="T2264" id="Seg_804" s="T2263">kareški</ta>
            <ta e="T2265" id="Seg_805" s="T2264">a</ta>
            <ta e="T2266" id="Seg_806" s="T2265">măn</ta>
            <ta e="T2267" id="Seg_807" s="T2266">i-le-m</ta>
            <ta e="T2268" id="Seg_808" s="T2267">verški</ta>
            <ta e="T2269" id="Seg_809" s="T2268">dĭgəttə</ta>
            <ta e="T2270" id="Seg_810" s="T2269">dĭ</ta>
            <ta e="T2271" id="Seg_811" s="T2270">i-luʔ-pi</ta>
            <ta e="T2272" id="Seg_812" s="T2271">aš</ta>
            <ta e="T2273" id="Seg_813" s="T2272">kon-luʔ-pi</ta>
            <ta e="T2274" id="Seg_814" s="T2273">a</ta>
            <ta e="T2275" id="Seg_815" s="T2274">dĭ</ta>
            <ta e="T2276" id="Seg_816" s="T2275">ĭmbi=də</ta>
            <ta e="T2277" id="Seg_817" s="T2276">ej</ta>
            <ta e="T2278" id="Seg_818" s="T2277">mo-bi</ta>
            <ta e="T2279" id="Seg_819" s="T2278">a-zittə</ta>
            <ta e="T2280" id="Seg_820" s="T2279">dĭgəttə</ta>
            <ta e="T2281" id="Seg_821" s="T2280">dĭ-zeŋ</ta>
            <ta e="T2282" id="Seg_822" s="T2281">kü-laːm-bi</ta>
            <ta e="T2283" id="Seg_823" s="T2282">dĭ-n</ta>
            <ta e="T2284" id="Seg_824" s="T2283">dĭ-zeŋ</ta>
            <ta e="T2285" id="Seg_825" s="T2284">družba-t</ta>
            <ta e="T2286" id="Seg_826" s="T2285">urgaːba</ta>
            <ta e="T2287" id="Seg_827" s="T2286">kuro-luʔ-pi</ta>
            <ta e="T2288" id="Seg_828" s="T2287">tibi-nə</ta>
            <ta e="T2289" id="Seg_829" s="T2288">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2179" id="Seg_830" s="T2178">onʼiʔ</ta>
            <ta e="T2180" id="Seg_831" s="T2179">kuza</ta>
            <ta e="T2181" id="Seg_832" s="T2180">kuʔ-bi</ta>
            <ta e="T2182" id="Seg_833" s="T2181">rʼepa</ta>
            <ta e="T2183" id="Seg_834" s="T2182">a</ta>
            <ta e="T2184" id="Seg_835" s="T2183">urgaːba</ta>
            <ta e="T2185" id="Seg_836" s="T2184">šo-bi</ta>
            <ta e="T2186" id="Seg_837" s="T2185">ĭmbi</ta>
            <ta e="T2187" id="Seg_838" s="T2186">a-laʔbə-l</ta>
            <ta e="T2188" id="Seg_839" s="T2187">kuʔ-laʔbə-m</ta>
            <ta e="T2189" id="Seg_840" s="T2188">rʼepa</ta>
            <ta e="T2190" id="Seg_841" s="T2189">nu</ta>
            <ta e="T2191" id="Seg_842" s="T2190">tak</ta>
            <ta e="T2192" id="Seg_843" s="T2191">a</ta>
            <ta e="T2193" id="Seg_844" s="T2192">măna</ta>
            <ta e="T2195" id="Seg_845" s="T2194">i-ʔ</ta>
            <ta e="T2197" id="Seg_846" s="T2196">hele-ziʔ</ta>
            <ta e="T2198" id="Seg_847" s="T2197">nu</ta>
            <ta e="T2201" id="Seg_848" s="T2200">măn</ta>
            <ta e="T2202" id="Seg_849" s="T2201">i-lV-m</ta>
            <ta e="T2203" id="Seg_850" s="T2202">kareški</ta>
            <ta e="T2204" id="Seg_851" s="T2203">a</ta>
            <ta e="T2205" id="Seg_852" s="T2204">tăn</ta>
            <ta e="T2206" id="Seg_853" s="T2205">tănan</ta>
            <ta e="T2207" id="Seg_854" s="T2206">mo-lV-m</ta>
            <ta e="T2208" id="Seg_855" s="T2207">verški</ta>
            <ta e="T2209" id="Seg_856" s="T2208">dĭgəttə</ta>
            <ta e="T2210" id="Seg_857" s="T2209">urgaːba</ta>
            <ta e="T2211" id="Seg_858" s="T2210">kan-lAʔ</ta>
            <ta e="T2212" id="Seg_859" s="T2211">tʼür-bi</ta>
            <ta e="T2213" id="Seg_860" s="T2212">rʼepa-t</ta>
            <ta e="T2214" id="Seg_861" s="T2213">özer-bi</ta>
            <ta e="T2215" id="Seg_862" s="T2214">tibi</ta>
            <ta e="T2216" id="Seg_863" s="T2215">šo-bi</ta>
            <ta e="T2217" id="Seg_864" s="T2216">rʼepa-bə</ta>
            <ta e="T2218" id="Seg_865" s="T2217">nĭŋgə-bi</ta>
            <ta e="T2219" id="Seg_866" s="T2218">i</ta>
            <ta e="T2220" id="Seg_867" s="T2219">dĭ-Tə</ta>
            <ta e="T2221" id="Seg_868" s="T2220">mĭ-luʔbdə-bi</ta>
            <ta e="T2222" id="Seg_869" s="T2221">dĭ</ta>
            <ta e="T2224" id="Seg_870" s="T2223">no</ta>
            <ta e="T2225" id="Seg_871" s="T2224">a</ta>
            <ta e="T2226" id="Seg_872" s="T2225">bostə-gəndə</ta>
            <ta e="T2227" id="Seg_873" s="T2226">rʼepa</ta>
            <ta e="T2228" id="Seg_874" s="T2227">i-bi</ta>
            <ta e="T2230" id="Seg_875" s="T2229">kan-bi</ta>
            <ta e="T2231" id="Seg_876" s="T2230">sădar-zittə</ta>
            <ta e="T2232" id="Seg_877" s="T2231">a</ta>
            <ta e="T2233" id="Seg_878" s="T2232">urgaːba</ta>
            <ta e="T2234" id="Seg_879" s="T2233">šonə-gA</ta>
            <ta e="T2235" id="Seg_880" s="T2234">det-ʔ</ta>
            <ta e="T2236" id="Seg_881" s="T2235">măna</ta>
            <ta e="T2237" id="Seg_882" s="T2236">onʼiʔ</ta>
            <ta e="T2238" id="Seg_883" s="T2237">am-zittə</ta>
            <ta e="T2239" id="Seg_884" s="T2238">dĭ</ta>
            <ta e="T2240" id="Seg_885" s="T2239">mĭ-bi</ta>
            <ta e="T2241" id="Seg_886" s="T2240">o</ta>
            <ta e="T2242" id="Seg_887" s="T2241">girgit</ta>
            <ta e="T2243" id="Seg_888" s="T2242">nʼamga</ta>
            <ta e="T2244" id="Seg_889" s="T2243">măn</ta>
            <ta e="T2245" id="Seg_890" s="T2244">tănan</ta>
            <ta e="T2246" id="Seg_891" s="T2245">tüj</ta>
            <ta e="T2247" id="Seg_892" s="T2246">bar</ta>
            <ta e="T2248" id="Seg_893" s="T2247">am-luʔbdə-bi-m</ta>
            <ta e="T2249" id="Seg_894" s="T2248">dĭgəttə</ta>
            <ta e="T2250" id="Seg_895" s="T2249">dĭ</ta>
            <ta e="T2251" id="Seg_896" s="T2250">bar</ta>
            <ta e="T2252" id="Seg_897" s="T2251">davaj</ta>
            <ta e="T2253" id="Seg_898" s="T2252">aš</ta>
            <ta e="T2254" id="Seg_899" s="T2253">kuʔ-zittə</ta>
            <ta e="T2255" id="Seg_900" s="T2254">bazoʔ</ta>
            <ta e="T2256" id="Seg_901" s="T2255">urgaːba</ta>
            <ta e="T2257" id="Seg_902" s="T2256">šo-bi</ta>
            <ta e="T2258" id="Seg_903" s="T2257">no</ta>
            <ta e="T2259" id="Seg_904" s="T2258">mĭ-lV-l</ta>
            <ta e="T2260" id="Seg_905" s="T2259">măna</ta>
            <ta e="T2261" id="Seg_906" s="T2260">ĭmbi</ta>
            <ta e="T2262" id="Seg_907" s="T2261">no</ta>
            <ta e="T2263" id="Seg_908" s="T2262">i-ʔ</ta>
            <ta e="T2264" id="Seg_909" s="T2263">kareški</ta>
            <ta e="T2265" id="Seg_910" s="T2264">a</ta>
            <ta e="T2266" id="Seg_911" s="T2265">măn</ta>
            <ta e="T2267" id="Seg_912" s="T2266">i-lV-m</ta>
            <ta e="T2268" id="Seg_913" s="T2267">verški</ta>
            <ta e="T2269" id="Seg_914" s="T2268">dĭgəttə</ta>
            <ta e="T2270" id="Seg_915" s="T2269">dĭ</ta>
            <ta e="T2271" id="Seg_916" s="T2270">i-luʔbdə-bi</ta>
            <ta e="T2272" id="Seg_917" s="T2271">aš</ta>
            <ta e="T2273" id="Seg_918" s="T2272">kon-luʔbdə-bi</ta>
            <ta e="T2274" id="Seg_919" s="T2273">a</ta>
            <ta e="T2275" id="Seg_920" s="T2274">dĭ</ta>
            <ta e="T2276" id="Seg_921" s="T2275">ĭmbi=də</ta>
            <ta e="T2277" id="Seg_922" s="T2276">ej</ta>
            <ta e="T2278" id="Seg_923" s="T2277">mo-bi</ta>
            <ta e="T2279" id="Seg_924" s="T2278">a-zittə</ta>
            <ta e="T2280" id="Seg_925" s="T2279">dĭgəttə</ta>
            <ta e="T2281" id="Seg_926" s="T2280">dĭ-zAŋ</ta>
            <ta e="T2282" id="Seg_927" s="T2281">kü-laːm-bi</ta>
            <ta e="T2283" id="Seg_928" s="T2282">dĭ-n</ta>
            <ta e="T2284" id="Seg_929" s="T2283">dĭ-zAŋ</ta>
            <ta e="T2285" id="Seg_930" s="T2284">družba-t</ta>
            <ta e="T2286" id="Seg_931" s="T2285">urgaːba</ta>
            <ta e="T2287" id="Seg_932" s="T2286">kuroː-luʔbdə-bi</ta>
            <ta e="T2288" id="Seg_933" s="T2287">tibi-Tə</ta>
            <ta e="T2289" id="Seg_934" s="T2288">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2179" id="Seg_935" s="T2178">one.[NOM.SG]</ta>
            <ta e="T2180" id="Seg_936" s="T2179">man.[NOM.SG]</ta>
            <ta e="T2181" id="Seg_937" s="T2180">sow-PST.[3SG]</ta>
            <ta e="T2182" id="Seg_938" s="T2181">turnip.[NOM.SG]</ta>
            <ta e="T2183" id="Seg_939" s="T2182">and</ta>
            <ta e="T2184" id="Seg_940" s="T2183">bear.[NOM.SG]</ta>
            <ta e="T2185" id="Seg_941" s="T2184">come-PST.[3SG]</ta>
            <ta e="T2186" id="Seg_942" s="T2185">what.[NOM.SG]</ta>
            <ta e="T2187" id="Seg_943" s="T2186">make-DUR-2SG</ta>
            <ta e="T2188" id="Seg_944" s="T2187">sow-DUR-1SG</ta>
            <ta e="T2189" id="Seg_945" s="T2188">turnip.[NOM.SG]</ta>
            <ta e="T2190" id="Seg_946" s="T2189">well</ta>
            <ta e="T2191" id="Seg_947" s="T2190">so</ta>
            <ta e="T2192" id="Seg_948" s="T2191">and</ta>
            <ta e="T2193" id="Seg_949" s="T2192">I.ACC</ta>
            <ta e="T2195" id="Seg_950" s="T2194">take-IMP.2SG</ta>
            <ta e="T2197" id="Seg_951" s="T2196">companion-INS</ta>
            <ta e="T2198" id="Seg_952" s="T2197">well</ta>
            <ta e="T2201" id="Seg_953" s="T2200">I.NOM</ta>
            <ta e="T2202" id="Seg_954" s="T2201">take-FUT-1SG</ta>
            <ta e="T2203" id="Seg_955" s="T2202">roots</ta>
            <ta e="T2204" id="Seg_956" s="T2203">and</ta>
            <ta e="T2205" id="Seg_957" s="T2204">you.NOM</ta>
            <ta e="T2206" id="Seg_958" s="T2205">you.DAT</ta>
            <ta e="T2207" id="Seg_959" s="T2206">become-FUT-1SG</ta>
            <ta e="T2208" id="Seg_960" s="T2207">tops</ta>
            <ta e="T2209" id="Seg_961" s="T2208">then</ta>
            <ta e="T2210" id="Seg_962" s="T2209">bear.[NOM.SG]</ta>
            <ta e="T2211" id="Seg_963" s="T2210">go-CVB</ta>
            <ta e="T2212" id="Seg_964" s="T2211">disappear-PST.[3SG]</ta>
            <ta e="T2213" id="Seg_965" s="T2212">turnip-NOM/GEN.3SG</ta>
            <ta e="T2214" id="Seg_966" s="T2213">grow-PST.[3SG]</ta>
            <ta e="T2215" id="Seg_967" s="T2214">man.[NOM.SG]</ta>
            <ta e="T2216" id="Seg_968" s="T2215">come-PST.[3SG]</ta>
            <ta e="T2217" id="Seg_969" s="T2216">turnip-ACC.3SG</ta>
            <ta e="T2218" id="Seg_970" s="T2217">tear-PST.[3SG]</ta>
            <ta e="T2219" id="Seg_971" s="T2218">and</ta>
            <ta e="T2220" id="Seg_972" s="T2219">this-LAT</ta>
            <ta e="T2221" id="Seg_973" s="T2220">give-MOM-PST.[3SG]</ta>
            <ta e="T2222" id="Seg_974" s="T2221">this.[NOM.SG]</ta>
            <ta e="T2224" id="Seg_975" s="T2223">well</ta>
            <ta e="T2225" id="Seg_976" s="T2224">and</ta>
            <ta e="T2226" id="Seg_977" s="T2225">self-LAT/LOC.3SG</ta>
            <ta e="T2227" id="Seg_978" s="T2226">turnip.[NOM.SG]</ta>
            <ta e="T2228" id="Seg_979" s="T2227">take-PST.[3SG]</ta>
            <ta e="T2230" id="Seg_980" s="T2229">go-PST.[3SG]</ta>
            <ta e="T2231" id="Seg_981" s="T2230">sell-INF.LAT</ta>
            <ta e="T2232" id="Seg_982" s="T2231">and</ta>
            <ta e="T2233" id="Seg_983" s="T2232">bear.[NOM.SG]</ta>
            <ta e="T2234" id="Seg_984" s="T2233">come-PRS.[3SG]</ta>
            <ta e="T2235" id="Seg_985" s="T2234">bring-IMP.2SG</ta>
            <ta e="T2236" id="Seg_986" s="T2235">I.LAT</ta>
            <ta e="T2237" id="Seg_987" s="T2236">one.[NOM.SG]</ta>
            <ta e="T2238" id="Seg_988" s="T2237">eat-INF.LAT</ta>
            <ta e="T2239" id="Seg_989" s="T2238">this.[NOM.SG]</ta>
            <ta e="T2240" id="Seg_990" s="T2239">give-PST.[3SG]</ta>
            <ta e="T2241" id="Seg_991" s="T2240">oh</ta>
            <ta e="T2242" id="Seg_992" s="T2241">what.kind</ta>
            <ta e="T2243" id="Seg_993" s="T2242">sweet.[NOM.SG]</ta>
            <ta e="T2244" id="Seg_994" s="T2243">I.NOM</ta>
            <ta e="T2245" id="Seg_995" s="T2244">you.DAT</ta>
            <ta e="T2246" id="Seg_996" s="T2245">now</ta>
            <ta e="T2247" id="Seg_997" s="T2246">all</ta>
            <ta e="T2248" id="Seg_998" s="T2247">eat-MOM-PST-1SG</ta>
            <ta e="T2249" id="Seg_999" s="T2248">then</ta>
            <ta e="T2250" id="Seg_1000" s="T2249">this.[NOM.SG]</ta>
            <ta e="T2251" id="Seg_1001" s="T2250">PTCL</ta>
            <ta e="T2252" id="Seg_1002" s="T2251">INCH</ta>
            <ta e="T2253" id="Seg_1003" s="T2252">rye.[NOM.SG]</ta>
            <ta e="T2254" id="Seg_1004" s="T2253">sow-INF.LAT</ta>
            <ta e="T2255" id="Seg_1005" s="T2254">again</ta>
            <ta e="T2256" id="Seg_1006" s="T2255">bear.[NOM.SG]</ta>
            <ta e="T2257" id="Seg_1007" s="T2256">come-PST.[3SG]</ta>
            <ta e="T2258" id="Seg_1008" s="T2257">well</ta>
            <ta e="T2259" id="Seg_1009" s="T2258">give-FUT-2SG</ta>
            <ta e="T2260" id="Seg_1010" s="T2259">I.LAT</ta>
            <ta e="T2261" id="Seg_1011" s="T2260">what.[NOM.SG]</ta>
            <ta e="T2262" id="Seg_1012" s="T2261">well</ta>
            <ta e="T2263" id="Seg_1013" s="T2262">take-IMP.2SG</ta>
            <ta e="T2264" id="Seg_1014" s="T2263">roots</ta>
            <ta e="T2265" id="Seg_1015" s="T2264">and</ta>
            <ta e="T2266" id="Seg_1016" s="T2265">I.NOM</ta>
            <ta e="T2267" id="Seg_1017" s="T2266">take-FUT-1SG</ta>
            <ta e="T2268" id="Seg_1018" s="T2267">tops</ta>
            <ta e="T2269" id="Seg_1019" s="T2268">then</ta>
            <ta e="T2270" id="Seg_1020" s="T2269">this.[NOM.SG]</ta>
            <ta e="T2271" id="Seg_1021" s="T2270">take-MOM-PST.[3SG]</ta>
            <ta e="T2272" id="Seg_1022" s="T2271">rye.[NOM.SG]</ta>
            <ta e="T2273" id="Seg_1023" s="T2272">take.away-MOM-PST.[3SG]</ta>
            <ta e="T2274" id="Seg_1024" s="T2273">and</ta>
            <ta e="T2275" id="Seg_1025" s="T2274">this.[NOM.SG]</ta>
            <ta e="T2276" id="Seg_1026" s="T2275">what.[NOM.SG]=INDEF</ta>
            <ta e="T2277" id="Seg_1027" s="T2276">NEG</ta>
            <ta e="T2278" id="Seg_1028" s="T2277">can-PST.[3SG]</ta>
            <ta e="T2279" id="Seg_1029" s="T2278">make-INF.LAT</ta>
            <ta e="T2280" id="Seg_1030" s="T2279">then</ta>
            <ta e="T2281" id="Seg_1031" s="T2280">this-PL</ta>
            <ta e="T2282" id="Seg_1032" s="T2281">die-RES-PST.[3SG]</ta>
            <ta e="T2283" id="Seg_1033" s="T2282">this-GEN</ta>
            <ta e="T2284" id="Seg_1034" s="T2283">this-PL</ta>
            <ta e="T2285" id="Seg_1035" s="T2284">friendship-NOM/GEN.3SG</ta>
            <ta e="T2286" id="Seg_1036" s="T2285">bear.[NOM.SG]</ta>
            <ta e="T2287" id="Seg_1037" s="T2286">be.angry-MOM-PST.[3SG]</ta>
            <ta e="T2288" id="Seg_1038" s="T2287">man-LAT</ta>
            <ta e="T2289" id="Seg_1039" s="T2288">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2179" id="Seg_1040" s="T2178">один.[NOM.SG]</ta>
            <ta e="T2180" id="Seg_1041" s="T2179">мужчина.[NOM.SG]</ta>
            <ta e="T2181" id="Seg_1042" s="T2180">сеять-PST.[3SG]</ta>
            <ta e="T2182" id="Seg_1043" s="T2181">репа.[NOM.SG]</ta>
            <ta e="T2183" id="Seg_1044" s="T2182">а</ta>
            <ta e="T2184" id="Seg_1045" s="T2183">медведь.[NOM.SG]</ta>
            <ta e="T2185" id="Seg_1046" s="T2184">прийти-PST.[3SG]</ta>
            <ta e="T2186" id="Seg_1047" s="T2185">что.[NOM.SG]</ta>
            <ta e="T2187" id="Seg_1048" s="T2186">делать-DUR-2SG</ta>
            <ta e="T2188" id="Seg_1049" s="T2187">сеять-DUR-1SG</ta>
            <ta e="T2189" id="Seg_1050" s="T2188">репа.[NOM.SG]</ta>
            <ta e="T2190" id="Seg_1051" s="T2189">ну</ta>
            <ta e="T2191" id="Seg_1052" s="T2190">так</ta>
            <ta e="T2192" id="Seg_1053" s="T2191">а</ta>
            <ta e="T2193" id="Seg_1054" s="T2192">я.ACC</ta>
            <ta e="T2195" id="Seg_1055" s="T2194">взять-IMP.2SG</ta>
            <ta e="T2197" id="Seg_1056" s="T2196">товарищ-INS</ta>
            <ta e="T2198" id="Seg_1057" s="T2197">ну</ta>
            <ta e="T2201" id="Seg_1058" s="T2200">я.NOM</ta>
            <ta e="T2202" id="Seg_1059" s="T2201">взять-FUT-1SG</ta>
            <ta e="T2203" id="Seg_1060" s="T2202">корешки</ta>
            <ta e="T2204" id="Seg_1061" s="T2203">а</ta>
            <ta e="T2205" id="Seg_1062" s="T2204">ты.NOM</ta>
            <ta e="T2206" id="Seg_1063" s="T2205">ты.DAT</ta>
            <ta e="T2207" id="Seg_1064" s="T2206">мочь-FUT-1SG</ta>
            <ta e="T2208" id="Seg_1065" s="T2207">вершки</ta>
            <ta e="T2209" id="Seg_1066" s="T2208">тогда</ta>
            <ta e="T2210" id="Seg_1067" s="T2209">медведь.[NOM.SG]</ta>
            <ta e="T2211" id="Seg_1068" s="T2210">пойти-CVB</ta>
            <ta e="T2212" id="Seg_1069" s="T2211">исчезнуть-PST.[3SG]</ta>
            <ta e="T2213" id="Seg_1070" s="T2212">репа-NOM/GEN.3SG</ta>
            <ta e="T2214" id="Seg_1071" s="T2213">расти-PST.[3SG]</ta>
            <ta e="T2215" id="Seg_1072" s="T2214">мужчина.[NOM.SG]</ta>
            <ta e="T2216" id="Seg_1073" s="T2215">прийти-PST.[3SG]</ta>
            <ta e="T2217" id="Seg_1074" s="T2216">репа-ACC.3SG</ta>
            <ta e="T2218" id="Seg_1075" s="T2217">рвать-PST.[3SG]</ta>
            <ta e="T2219" id="Seg_1076" s="T2218">и</ta>
            <ta e="T2220" id="Seg_1077" s="T2219">этот-LAT</ta>
            <ta e="T2221" id="Seg_1078" s="T2220">дать-MOM-PST.[3SG]</ta>
            <ta e="T2222" id="Seg_1079" s="T2221">этот.[NOM.SG]</ta>
            <ta e="T2224" id="Seg_1080" s="T2223">ну</ta>
            <ta e="T2225" id="Seg_1081" s="T2224">а</ta>
            <ta e="T2226" id="Seg_1082" s="T2225">сам-LAT/LOC.3SG</ta>
            <ta e="T2227" id="Seg_1083" s="T2226">репа.[NOM.SG]</ta>
            <ta e="T2228" id="Seg_1084" s="T2227">взять-PST.[3SG]</ta>
            <ta e="T2230" id="Seg_1085" s="T2229">пойти-PST.[3SG]</ta>
            <ta e="T2231" id="Seg_1086" s="T2230">продавать-INF.LAT</ta>
            <ta e="T2232" id="Seg_1087" s="T2231">а</ta>
            <ta e="T2233" id="Seg_1088" s="T2232">медведь.[NOM.SG]</ta>
            <ta e="T2234" id="Seg_1089" s="T2233">прийти-PRS.[3SG]</ta>
            <ta e="T2235" id="Seg_1090" s="T2234">принести-IMP.2SG</ta>
            <ta e="T2236" id="Seg_1091" s="T2235">я.LAT</ta>
            <ta e="T2237" id="Seg_1092" s="T2236">один.[NOM.SG]</ta>
            <ta e="T2238" id="Seg_1093" s="T2237">съесть-INF.LAT</ta>
            <ta e="T2239" id="Seg_1094" s="T2238">этот.[NOM.SG]</ta>
            <ta e="T2240" id="Seg_1095" s="T2239">дать-PST.[3SG]</ta>
            <ta e="T2241" id="Seg_1096" s="T2240">о</ta>
            <ta e="T2242" id="Seg_1097" s="T2241">какой</ta>
            <ta e="T2243" id="Seg_1098" s="T2242">сладкий.[NOM.SG]</ta>
            <ta e="T2244" id="Seg_1099" s="T2243">я.NOM</ta>
            <ta e="T2245" id="Seg_1100" s="T2244">ты.DAT</ta>
            <ta e="T2246" id="Seg_1101" s="T2245">сейчас</ta>
            <ta e="T2247" id="Seg_1102" s="T2246">весь</ta>
            <ta e="T2248" id="Seg_1103" s="T2247">съесть-MOM-PST-1SG</ta>
            <ta e="T2249" id="Seg_1104" s="T2248">тогда</ta>
            <ta e="T2250" id="Seg_1105" s="T2249">этот.[NOM.SG]</ta>
            <ta e="T2251" id="Seg_1106" s="T2250">PTCL</ta>
            <ta e="T2252" id="Seg_1107" s="T2251">INCH</ta>
            <ta e="T2253" id="Seg_1108" s="T2252">рожь.[NOM.SG]</ta>
            <ta e="T2254" id="Seg_1109" s="T2253">сеять-INF.LAT</ta>
            <ta e="T2255" id="Seg_1110" s="T2254">опять</ta>
            <ta e="T2256" id="Seg_1111" s="T2255">медведь.[NOM.SG]</ta>
            <ta e="T2257" id="Seg_1112" s="T2256">прийти-PST.[3SG]</ta>
            <ta e="T2258" id="Seg_1113" s="T2257">ну</ta>
            <ta e="T2259" id="Seg_1114" s="T2258">дать-FUT-2SG</ta>
            <ta e="T2260" id="Seg_1115" s="T2259">я.LAT</ta>
            <ta e="T2261" id="Seg_1116" s="T2260">что.[NOM.SG]</ta>
            <ta e="T2262" id="Seg_1117" s="T2261">ну</ta>
            <ta e="T2263" id="Seg_1118" s="T2262">взять-IMP.2SG</ta>
            <ta e="T2264" id="Seg_1119" s="T2263">корешки</ta>
            <ta e="T2265" id="Seg_1120" s="T2264">а</ta>
            <ta e="T2266" id="Seg_1121" s="T2265">я.NOM</ta>
            <ta e="T2267" id="Seg_1122" s="T2266">взять-FUT-1SG</ta>
            <ta e="T2268" id="Seg_1123" s="T2267">вершки</ta>
            <ta e="T2269" id="Seg_1124" s="T2268">тогда</ta>
            <ta e="T2270" id="Seg_1125" s="T2269">этот.[NOM.SG]</ta>
            <ta e="T2271" id="Seg_1126" s="T2270">взять-MOM-PST.[3SG]</ta>
            <ta e="T2272" id="Seg_1127" s="T2271">рожь.[NOM.SG]</ta>
            <ta e="T2273" id="Seg_1128" s="T2272">унести-MOM-PST.[3SG]</ta>
            <ta e="T2274" id="Seg_1129" s="T2273">а</ta>
            <ta e="T2275" id="Seg_1130" s="T2274">этот.[NOM.SG]</ta>
            <ta e="T2276" id="Seg_1131" s="T2275">что.[NOM.SG]=INDEF</ta>
            <ta e="T2277" id="Seg_1132" s="T2276">NEG</ta>
            <ta e="T2278" id="Seg_1133" s="T2277">мочь-PST.[3SG]</ta>
            <ta e="T2279" id="Seg_1134" s="T2278">делать-INF.LAT</ta>
            <ta e="T2280" id="Seg_1135" s="T2279">тогда</ta>
            <ta e="T2281" id="Seg_1136" s="T2280">этот-PL</ta>
            <ta e="T2282" id="Seg_1137" s="T2281">умереть-RES-PST.[3SG]</ta>
            <ta e="T2283" id="Seg_1138" s="T2282">этот-GEN</ta>
            <ta e="T2284" id="Seg_1139" s="T2283">этот-PL</ta>
            <ta e="T2285" id="Seg_1140" s="T2284">дружба-NOM/GEN.3SG</ta>
            <ta e="T2286" id="Seg_1141" s="T2285">медведь.[NOM.SG]</ta>
            <ta e="T2287" id="Seg_1142" s="T2286">сердиться-MOM-PST.[3SG]</ta>
            <ta e="T2288" id="Seg_1143" s="T2287">мужчина-LAT</ta>
            <ta e="T2289" id="Seg_1144" s="T2288">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2179" id="Seg_1145" s="T2178">num-n:case</ta>
            <ta e="T2180" id="Seg_1146" s="T2179">n-n:case</ta>
            <ta e="T2181" id="Seg_1147" s="T2180">v-v:tense-v:pn</ta>
            <ta e="T2182" id="Seg_1148" s="T2181">n-n:case</ta>
            <ta e="T2183" id="Seg_1149" s="T2182">conj</ta>
            <ta e="T2184" id="Seg_1150" s="T2183">n-n:case</ta>
            <ta e="T2185" id="Seg_1151" s="T2184">v-v:tense-v:pn</ta>
            <ta e="T2186" id="Seg_1152" s="T2185">que-n:case</ta>
            <ta e="T2187" id="Seg_1153" s="T2186">v-v&gt;v-v:pn</ta>
            <ta e="T2188" id="Seg_1154" s="T2187">v-v&gt;v-v:pn</ta>
            <ta e="T2189" id="Seg_1155" s="T2188">n-n:case</ta>
            <ta e="T2190" id="Seg_1156" s="T2189">ptcl</ta>
            <ta e="T2191" id="Seg_1157" s="T2190">ptcl</ta>
            <ta e="T2192" id="Seg_1158" s="T2191">conj</ta>
            <ta e="T2193" id="Seg_1159" s="T2192">pers</ta>
            <ta e="T2195" id="Seg_1160" s="T2194">v-v:mood.pn</ta>
            <ta e="T2197" id="Seg_1161" s="T2196">n-n:case</ta>
            <ta e="T2198" id="Seg_1162" s="T2197">ptcl</ta>
            <ta e="T2201" id="Seg_1163" s="T2200">pers</ta>
            <ta e="T2202" id="Seg_1164" s="T2201">v-v:tense-v:pn</ta>
            <ta e="T2203" id="Seg_1165" s="T2202">n</ta>
            <ta e="T2204" id="Seg_1166" s="T2203">conj</ta>
            <ta e="T2205" id="Seg_1167" s="T2204">pers</ta>
            <ta e="T2206" id="Seg_1168" s="T2205">pers</ta>
            <ta e="T2207" id="Seg_1169" s="T2206">v-v:tense-v:pn</ta>
            <ta e="T2208" id="Seg_1170" s="T2207">n</ta>
            <ta e="T2209" id="Seg_1171" s="T2208">adv</ta>
            <ta e="T2210" id="Seg_1172" s="T2209">n-n:case</ta>
            <ta e="T2211" id="Seg_1173" s="T2210">v-v:n.fin</ta>
            <ta e="T2212" id="Seg_1174" s="T2211">v-v:tense-v:pn</ta>
            <ta e="T2213" id="Seg_1175" s="T2212">n-n:case.poss</ta>
            <ta e="T2214" id="Seg_1176" s="T2213">v-v:tense-v:pn</ta>
            <ta e="T2215" id="Seg_1177" s="T2214">n-n:case</ta>
            <ta e="T2216" id="Seg_1178" s="T2215">v-v:tense-v:pn</ta>
            <ta e="T2217" id="Seg_1179" s="T2216">n-n:case.poss</ta>
            <ta e="T2218" id="Seg_1180" s="T2217">v-v:tense-v:pn</ta>
            <ta e="T2219" id="Seg_1181" s="T2218">conj</ta>
            <ta e="T2220" id="Seg_1182" s="T2219">dempro-n:case</ta>
            <ta e="T2221" id="Seg_1183" s="T2220">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2222" id="Seg_1184" s="T2221">dempro-n:case</ta>
            <ta e="T2224" id="Seg_1185" s="T2223">ptcl</ta>
            <ta e="T2225" id="Seg_1186" s="T2224">conj</ta>
            <ta e="T2226" id="Seg_1187" s="T2225">refl-n:case.poss</ta>
            <ta e="T2227" id="Seg_1188" s="T2226">n-n:case</ta>
            <ta e="T2228" id="Seg_1189" s="T2227">v-v:tense-v:pn</ta>
            <ta e="T2230" id="Seg_1190" s="T2229">v-v:tense-v:pn</ta>
            <ta e="T2231" id="Seg_1191" s="T2230">v-v:n.fin</ta>
            <ta e="T2232" id="Seg_1192" s="T2231">conj</ta>
            <ta e="T2233" id="Seg_1193" s="T2232">n-n:case</ta>
            <ta e="T2234" id="Seg_1194" s="T2233">v-v:tense-v:pn</ta>
            <ta e="T2235" id="Seg_1195" s="T2234">v-v:mood.pn</ta>
            <ta e="T2236" id="Seg_1196" s="T2235">pers</ta>
            <ta e="T2237" id="Seg_1197" s="T2236">num-n:case</ta>
            <ta e="T2238" id="Seg_1198" s="T2237">v-v:n.fin</ta>
            <ta e="T2239" id="Seg_1199" s="T2238">dempro-n:case</ta>
            <ta e="T2240" id="Seg_1200" s="T2239">v-v:tense-v:pn</ta>
            <ta e="T2241" id="Seg_1201" s="T2240">interj</ta>
            <ta e="T2242" id="Seg_1202" s="T2241">que</ta>
            <ta e="T2243" id="Seg_1203" s="T2242">adj-n:case</ta>
            <ta e="T2244" id="Seg_1204" s="T2243">pers</ta>
            <ta e="T2245" id="Seg_1205" s="T2244">pers</ta>
            <ta e="T2246" id="Seg_1206" s="T2245">adv</ta>
            <ta e="T2247" id="Seg_1207" s="T2246">quant</ta>
            <ta e="T2248" id="Seg_1208" s="T2247">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2249" id="Seg_1209" s="T2248">adv</ta>
            <ta e="T2250" id="Seg_1210" s="T2249">dempro-n:case</ta>
            <ta e="T2251" id="Seg_1211" s="T2250">ptcl</ta>
            <ta e="T2252" id="Seg_1212" s="T2251">ptcl</ta>
            <ta e="T2253" id="Seg_1213" s="T2252">n-n:case</ta>
            <ta e="T2254" id="Seg_1214" s="T2253">v-v:n.fin</ta>
            <ta e="T2255" id="Seg_1215" s="T2254">adv</ta>
            <ta e="T2256" id="Seg_1216" s="T2255">n-n:case</ta>
            <ta e="T2257" id="Seg_1217" s="T2256">v-v:tense-v:pn</ta>
            <ta e="T2258" id="Seg_1218" s="T2257">ptcl</ta>
            <ta e="T2259" id="Seg_1219" s="T2258">v-v:tense-v:pn</ta>
            <ta e="T2260" id="Seg_1220" s="T2259">pers</ta>
            <ta e="T2261" id="Seg_1221" s="T2260">que-n:case</ta>
            <ta e="T2262" id="Seg_1222" s="T2261">ptcl</ta>
            <ta e="T2263" id="Seg_1223" s="T2262">v-v:mood.pn</ta>
            <ta e="T2264" id="Seg_1224" s="T2263">n</ta>
            <ta e="T2265" id="Seg_1225" s="T2264">conj</ta>
            <ta e="T2266" id="Seg_1226" s="T2265">pers</ta>
            <ta e="T2267" id="Seg_1227" s="T2266">v-v:tense-v:pn</ta>
            <ta e="T2268" id="Seg_1228" s="T2267">n</ta>
            <ta e="T2269" id="Seg_1229" s="T2268">adv</ta>
            <ta e="T2270" id="Seg_1230" s="T2269">dempro-n:case</ta>
            <ta e="T2271" id="Seg_1231" s="T2270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2272" id="Seg_1232" s="T2271">n-n:case</ta>
            <ta e="T2273" id="Seg_1233" s="T2272">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2274" id="Seg_1234" s="T2273">conj</ta>
            <ta e="T2275" id="Seg_1235" s="T2274">dempro-n:case</ta>
            <ta e="T2276" id="Seg_1236" s="T2275">que-n:case=ptcl</ta>
            <ta e="T2277" id="Seg_1237" s="T2276">ptcl</ta>
            <ta e="T2278" id="Seg_1238" s="T2277">v-v:tense-v:pn</ta>
            <ta e="T2279" id="Seg_1239" s="T2278">v-v:n.fin</ta>
            <ta e="T2280" id="Seg_1240" s="T2279">adv</ta>
            <ta e="T2281" id="Seg_1241" s="T2280">dempro-n:num</ta>
            <ta e="T2282" id="Seg_1242" s="T2281">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2283" id="Seg_1243" s="T2282">dempro-n:case</ta>
            <ta e="T2284" id="Seg_1244" s="T2283">dempro-n:num</ta>
            <ta e="T2285" id="Seg_1245" s="T2284">n-n:case.poss</ta>
            <ta e="T2286" id="Seg_1246" s="T2285">n-n:case</ta>
            <ta e="T2287" id="Seg_1247" s="T2286">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2288" id="Seg_1248" s="T2287">n-n:case</ta>
            <ta e="T2289" id="Seg_1249" s="T2288">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2179" id="Seg_1250" s="T2178">num</ta>
            <ta e="T2180" id="Seg_1251" s="T2179">n</ta>
            <ta e="T2181" id="Seg_1252" s="T2180">v</ta>
            <ta e="T2182" id="Seg_1253" s="T2181">n</ta>
            <ta e="T2183" id="Seg_1254" s="T2182">conj</ta>
            <ta e="T2184" id="Seg_1255" s="T2183">n</ta>
            <ta e="T2185" id="Seg_1256" s="T2184">v</ta>
            <ta e="T2186" id="Seg_1257" s="T2185">que</ta>
            <ta e="T2187" id="Seg_1258" s="T2186">v</ta>
            <ta e="T2188" id="Seg_1259" s="T2187">v</ta>
            <ta e="T2189" id="Seg_1260" s="T2188">n</ta>
            <ta e="T2190" id="Seg_1261" s="T2189">ptcl</ta>
            <ta e="T2191" id="Seg_1262" s="T2190">ptcl</ta>
            <ta e="T2192" id="Seg_1263" s="T2191">conj</ta>
            <ta e="T2193" id="Seg_1264" s="T2192">pers</ta>
            <ta e="T2195" id="Seg_1265" s="T2194">v</ta>
            <ta e="T2197" id="Seg_1266" s="T2196">n</ta>
            <ta e="T2198" id="Seg_1267" s="T2197">ptcl</ta>
            <ta e="T2201" id="Seg_1268" s="T2200">pers</ta>
            <ta e="T2202" id="Seg_1269" s="T2201">v</ta>
            <ta e="T2203" id="Seg_1270" s="T2202">n</ta>
            <ta e="T2204" id="Seg_1271" s="T2203">conj</ta>
            <ta e="T2205" id="Seg_1272" s="T2204">pers</ta>
            <ta e="T2206" id="Seg_1273" s="T2205">pers</ta>
            <ta e="T2207" id="Seg_1274" s="T2206">v</ta>
            <ta e="T2208" id="Seg_1275" s="T2207">n</ta>
            <ta e="T2209" id="Seg_1276" s="T2208">adv</ta>
            <ta e="T2210" id="Seg_1277" s="T2209">n</ta>
            <ta e="T2211" id="Seg_1278" s="T2210">v</ta>
            <ta e="T2212" id="Seg_1279" s="T2211">v</ta>
            <ta e="T2213" id="Seg_1280" s="T2212">n</ta>
            <ta e="T2214" id="Seg_1281" s="T2213">v</ta>
            <ta e="T2215" id="Seg_1282" s="T2214">n</ta>
            <ta e="T2216" id="Seg_1283" s="T2215">v</ta>
            <ta e="T2217" id="Seg_1284" s="T2216">n</ta>
            <ta e="T2218" id="Seg_1285" s="T2217">v</ta>
            <ta e="T2219" id="Seg_1286" s="T2218">conj</ta>
            <ta e="T2220" id="Seg_1287" s="T2219">dempro</ta>
            <ta e="T2221" id="Seg_1288" s="T2220">v</ta>
            <ta e="T2222" id="Seg_1289" s="T2221">dempro</ta>
            <ta e="T2224" id="Seg_1290" s="T2223">ptcl</ta>
            <ta e="T2225" id="Seg_1291" s="T2224">conj</ta>
            <ta e="T2226" id="Seg_1292" s="T2225">refl</ta>
            <ta e="T2227" id="Seg_1293" s="T2226">n</ta>
            <ta e="T2228" id="Seg_1294" s="T2227">v</ta>
            <ta e="T2230" id="Seg_1295" s="T2229">v</ta>
            <ta e="T2231" id="Seg_1296" s="T2230">v</ta>
            <ta e="T2232" id="Seg_1297" s="T2231">conj</ta>
            <ta e="T2233" id="Seg_1298" s="T2232">n</ta>
            <ta e="T2234" id="Seg_1299" s="T2233">v</ta>
            <ta e="T2235" id="Seg_1300" s="T2234">v</ta>
            <ta e="T2236" id="Seg_1301" s="T2235">pers</ta>
            <ta e="T2237" id="Seg_1302" s="T2236">num</ta>
            <ta e="T2238" id="Seg_1303" s="T2237">v</ta>
            <ta e="T2239" id="Seg_1304" s="T2238">dempro</ta>
            <ta e="T2240" id="Seg_1305" s="T2239">v</ta>
            <ta e="T2241" id="Seg_1306" s="T2240">interj</ta>
            <ta e="T2242" id="Seg_1307" s="T2241">que</ta>
            <ta e="T2243" id="Seg_1308" s="T2242">adj</ta>
            <ta e="T2244" id="Seg_1309" s="T2243">pers</ta>
            <ta e="T2245" id="Seg_1310" s="T2244">pers</ta>
            <ta e="T2246" id="Seg_1311" s="T2245">adv</ta>
            <ta e="T2247" id="Seg_1312" s="T2246">quant</ta>
            <ta e="T2248" id="Seg_1313" s="T2247">v</ta>
            <ta e="T2249" id="Seg_1314" s="T2248">adv</ta>
            <ta e="T2250" id="Seg_1315" s="T2249">dempro</ta>
            <ta e="T2251" id="Seg_1316" s="T2250">ptcl</ta>
            <ta e="T2252" id="Seg_1317" s="T2251">ptcl</ta>
            <ta e="T2253" id="Seg_1318" s="T2252">n</ta>
            <ta e="T2254" id="Seg_1319" s="T2253">v</ta>
            <ta e="T2255" id="Seg_1320" s="T2254">adv</ta>
            <ta e="T2256" id="Seg_1321" s="T2255">n</ta>
            <ta e="T2257" id="Seg_1322" s="T2256">v</ta>
            <ta e="T2258" id="Seg_1323" s="T2257">ptcl</ta>
            <ta e="T2259" id="Seg_1324" s="T2258">v</ta>
            <ta e="T2260" id="Seg_1325" s="T2259">pers</ta>
            <ta e="T2261" id="Seg_1326" s="T2260">que</ta>
            <ta e="T2262" id="Seg_1327" s="T2261">ptcl</ta>
            <ta e="T2263" id="Seg_1328" s="T2262">v</ta>
            <ta e="T2264" id="Seg_1329" s="T2263">n</ta>
            <ta e="T2265" id="Seg_1330" s="T2264">conj</ta>
            <ta e="T2266" id="Seg_1331" s="T2265">pers</ta>
            <ta e="T2267" id="Seg_1332" s="T2266">v</ta>
            <ta e="T2268" id="Seg_1333" s="T2267">n</ta>
            <ta e="T2269" id="Seg_1334" s="T2268">adv</ta>
            <ta e="T2270" id="Seg_1335" s="T2269">dempro</ta>
            <ta e="T2271" id="Seg_1336" s="T2270">v</ta>
            <ta e="T2272" id="Seg_1337" s="T2271">n</ta>
            <ta e="T2273" id="Seg_1338" s="T2272">v</ta>
            <ta e="T2274" id="Seg_1339" s="T2273">conj</ta>
            <ta e="T2275" id="Seg_1340" s="T2274">dempro</ta>
            <ta e="T2276" id="Seg_1341" s="T2275">que</ta>
            <ta e="T2277" id="Seg_1342" s="T2276">ptcl</ta>
            <ta e="T2278" id="Seg_1343" s="T2277">v</ta>
            <ta e="T2279" id="Seg_1344" s="T2278">v</ta>
            <ta e="T2280" id="Seg_1345" s="T2279">adv</ta>
            <ta e="T2281" id="Seg_1346" s="T2280">dempro</ta>
            <ta e="T2282" id="Seg_1347" s="T2281">v</ta>
            <ta e="T2283" id="Seg_1348" s="T2282">dempro</ta>
            <ta e="T2284" id="Seg_1349" s="T2283">dempro</ta>
            <ta e="T2285" id="Seg_1350" s="T2284">n</ta>
            <ta e="T2286" id="Seg_1351" s="T2285">n</ta>
            <ta e="T2287" id="Seg_1352" s="T2286">v</ta>
            <ta e="T2288" id="Seg_1353" s="T2287">n</ta>
            <ta e="T2289" id="Seg_1354" s="T2288">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2180" id="Seg_1355" s="T2179">np.h:A</ta>
            <ta e="T2182" id="Seg_1356" s="T2181">np:Th</ta>
            <ta e="T2184" id="Seg_1357" s="T2183">np.h:A</ta>
            <ta e="T2187" id="Seg_1358" s="T2186">0.2.h:A</ta>
            <ta e="T2188" id="Seg_1359" s="T2187">0.1.h:A</ta>
            <ta e="T2189" id="Seg_1360" s="T2188">np:Th</ta>
            <ta e="T2193" id="Seg_1361" s="T2192">pro.h:Th</ta>
            <ta e="T2195" id="Seg_1362" s="T2194">0.2.h:A</ta>
            <ta e="T2197" id="Seg_1363" s="T2196">np.h:Th</ta>
            <ta e="T2201" id="Seg_1364" s="T2200">pro.h:A</ta>
            <ta e="T2203" id="Seg_1365" s="T2202">np:Th</ta>
            <ta e="T2206" id="Seg_1366" s="T2205">pro.h:Poss</ta>
            <ta e="T2208" id="Seg_1367" s="T2207">np:Th</ta>
            <ta e="T2209" id="Seg_1368" s="T2208">adv:Time</ta>
            <ta e="T2210" id="Seg_1369" s="T2209">np.h:A</ta>
            <ta e="T2213" id="Seg_1370" s="T2212">np:P</ta>
            <ta e="T2215" id="Seg_1371" s="T2214">np.h:A</ta>
            <ta e="T2217" id="Seg_1372" s="T2216">np:P</ta>
            <ta e="T2218" id="Seg_1373" s="T2217">0.3.h:A</ta>
            <ta e="T2220" id="Seg_1374" s="T2219">pro.h:R</ta>
            <ta e="T2221" id="Seg_1375" s="T2220">0.3.h:A</ta>
            <ta e="T2222" id="Seg_1376" s="T2221">pro.h:A</ta>
            <ta e="T2226" id="Seg_1377" s="T2225">pro:G</ta>
            <ta e="T2227" id="Seg_1378" s="T2226">np:Th</ta>
            <ta e="T2230" id="Seg_1379" s="T2229">0.3.h:A</ta>
            <ta e="T2233" id="Seg_1380" s="T2232">np.h:A</ta>
            <ta e="T2235" id="Seg_1381" s="T2234">0.2.h:A</ta>
            <ta e="T2236" id="Seg_1382" s="T2235">pro.h:R</ta>
            <ta e="T2237" id="Seg_1383" s="T2236">np:Th</ta>
            <ta e="T2239" id="Seg_1384" s="T2238">pro.h:A</ta>
            <ta e="T2244" id="Seg_1385" s="T2243">pro.h:A</ta>
            <ta e="T2245" id="Seg_1386" s="T2244">pro.h:P</ta>
            <ta e="T2246" id="Seg_1387" s="T2245">adv:Time</ta>
            <ta e="T2249" id="Seg_1388" s="T2248">adv:Time</ta>
            <ta e="T2250" id="Seg_1389" s="T2249">pro.h:A</ta>
            <ta e="T2253" id="Seg_1390" s="T2252">np:Th</ta>
            <ta e="T2256" id="Seg_1391" s="T2255">np.h:A</ta>
            <ta e="T2259" id="Seg_1392" s="T2258">0.2.h:A</ta>
            <ta e="T2260" id="Seg_1393" s="T2259">pro.h:R</ta>
            <ta e="T2261" id="Seg_1394" s="T2260">pro:Th</ta>
            <ta e="T2263" id="Seg_1395" s="T2262">0.2.h:A</ta>
            <ta e="T2264" id="Seg_1396" s="T2263">np:Th</ta>
            <ta e="T2266" id="Seg_1397" s="T2265">pro.h:A</ta>
            <ta e="T2268" id="Seg_1398" s="T2267">np:Th</ta>
            <ta e="T2269" id="Seg_1399" s="T2268">adv:Time</ta>
            <ta e="T2270" id="Seg_1400" s="T2269">pro.h:A</ta>
            <ta e="T2272" id="Seg_1401" s="T2271">np:Th</ta>
            <ta e="T2273" id="Seg_1402" s="T2272">0.3.h:A</ta>
            <ta e="T2275" id="Seg_1403" s="T2274">pro.h:A</ta>
            <ta e="T2276" id="Seg_1404" s="T2275">pro:Th</ta>
            <ta e="T2280" id="Seg_1405" s="T2279">adv:Time</ta>
            <ta e="T2284" id="Seg_1406" s="T2283">pro.h:Poss</ta>
            <ta e="T2285" id="Seg_1407" s="T2284">np:P</ta>
            <ta e="T2286" id="Seg_1408" s="T2285">np.h:E</ta>
            <ta e="T2288" id="Seg_1409" s="T2287">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2180" id="Seg_1410" s="T2179">np.h:S</ta>
            <ta e="T2181" id="Seg_1411" s="T2180">v:pred</ta>
            <ta e="T2182" id="Seg_1412" s="T2181">np:O</ta>
            <ta e="T2184" id="Seg_1413" s="T2183">np.h:S</ta>
            <ta e="T2185" id="Seg_1414" s="T2184">v:pred</ta>
            <ta e="T2187" id="Seg_1415" s="T2186">v:pred 0.2.h:S</ta>
            <ta e="T2188" id="Seg_1416" s="T2187">v:pred 0.1.h:S</ta>
            <ta e="T2189" id="Seg_1417" s="T2188">np:O</ta>
            <ta e="T2193" id="Seg_1418" s="T2192">pro.h:O</ta>
            <ta e="T2195" id="Seg_1419" s="T2194">v:pred 0.2.h:S</ta>
            <ta e="T2201" id="Seg_1420" s="T2200">pro.h:S</ta>
            <ta e="T2202" id="Seg_1421" s="T2201">v:pred</ta>
            <ta e="T2203" id="Seg_1422" s="T2202">np:O</ta>
            <ta e="T2206" id="Seg_1423" s="T2205">n:pred</ta>
            <ta e="T2207" id="Seg_1424" s="T2206">cop</ta>
            <ta e="T2208" id="Seg_1425" s="T2207">np:S</ta>
            <ta e="T2210" id="Seg_1426" s="T2209">np.h:S</ta>
            <ta e="T2211" id="Seg_1427" s="T2210">conv:pred</ta>
            <ta e="T2212" id="Seg_1428" s="T2211">v:pred</ta>
            <ta e="T2213" id="Seg_1429" s="T2212">np:S</ta>
            <ta e="T2214" id="Seg_1430" s="T2213">v:pred</ta>
            <ta e="T2215" id="Seg_1431" s="T2214">np.h:S</ta>
            <ta e="T2216" id="Seg_1432" s="T2215">v:pred</ta>
            <ta e="T2217" id="Seg_1433" s="T2216">np:O</ta>
            <ta e="T2218" id="Seg_1434" s="T2217">v:pred 0.3.h:S</ta>
            <ta e="T2221" id="Seg_1435" s="T2220">v:pred 0.3.h:S</ta>
            <ta e="T2222" id="Seg_1436" s="T2221">pro.h:S</ta>
            <ta e="T2227" id="Seg_1437" s="T2226">np:O</ta>
            <ta e="T2228" id="Seg_1438" s="T2227">v:pred</ta>
            <ta e="T2230" id="Seg_1439" s="T2229">v:pred 0.3.h:S</ta>
            <ta e="T2231" id="Seg_1440" s="T2230">s:purp</ta>
            <ta e="T2233" id="Seg_1441" s="T2232">np.h:S</ta>
            <ta e="T2234" id="Seg_1442" s="T2233">v:pred</ta>
            <ta e="T2235" id="Seg_1443" s="T2234">v:pred 0.2.h:S</ta>
            <ta e="T2237" id="Seg_1444" s="T2236">np:O</ta>
            <ta e="T2239" id="Seg_1445" s="T2238">pro.h:S</ta>
            <ta e="T2240" id="Seg_1446" s="T2239">v:pred</ta>
            <ta e="T2244" id="Seg_1447" s="T2243">pro.h:S</ta>
            <ta e="T2245" id="Seg_1448" s="T2244">pro.h:O</ta>
            <ta e="T2248" id="Seg_1449" s="T2247">v:pred</ta>
            <ta e="T2252" id="Seg_1450" s="T2251">ptcl:pred</ta>
            <ta e="T2253" id="Seg_1451" s="T2252">np:O</ta>
            <ta e="T2256" id="Seg_1452" s="T2255">np.h:S</ta>
            <ta e="T2257" id="Seg_1453" s="T2256">v:pred</ta>
            <ta e="T2259" id="Seg_1454" s="T2258">v:pred 0.2.h:S</ta>
            <ta e="T2261" id="Seg_1455" s="T2260">pro:O</ta>
            <ta e="T2263" id="Seg_1456" s="T2262">v:pred 0.2.h:S</ta>
            <ta e="T2264" id="Seg_1457" s="T2263">np:O</ta>
            <ta e="T2266" id="Seg_1458" s="T2265">pro.h:S</ta>
            <ta e="T2267" id="Seg_1459" s="T2266">v:pred</ta>
            <ta e="T2268" id="Seg_1460" s="T2267">np:O</ta>
            <ta e="T2270" id="Seg_1461" s="T2269">pro.h:S</ta>
            <ta e="T2271" id="Seg_1462" s="T2270">v:pred</ta>
            <ta e="T2272" id="Seg_1463" s="T2271">np:O</ta>
            <ta e="T2273" id="Seg_1464" s="T2272">v:pred 0.3.h:S</ta>
            <ta e="T2275" id="Seg_1465" s="T2274">pro.h:S</ta>
            <ta e="T2276" id="Seg_1466" s="T2275">pro:O</ta>
            <ta e="T2277" id="Seg_1467" s="T2276">ptcl.neg</ta>
            <ta e="T2278" id="Seg_1468" s="T2277">v:pred</ta>
            <ta e="T2282" id="Seg_1469" s="T2281">v:pred</ta>
            <ta e="T2285" id="Seg_1470" s="T2284">np:S</ta>
            <ta e="T2286" id="Seg_1471" s="T2285">np.h:S</ta>
            <ta e="T2287" id="Seg_1472" s="T2286">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2182" id="Seg_1473" s="T2181">RUS:cult</ta>
            <ta e="T2183" id="Seg_1474" s="T2182">RUS:gram</ta>
            <ta e="T2189" id="Seg_1475" s="T2188">RUS:cult</ta>
            <ta e="T2191" id="Seg_1476" s="T2190">RUS:gram</ta>
            <ta e="T2192" id="Seg_1477" s="T2191">RUS:gram</ta>
            <ta e="T2203" id="Seg_1478" s="T2202">RUS:cult</ta>
            <ta e="T2204" id="Seg_1479" s="T2203">RUS:gram</ta>
            <ta e="T2208" id="Seg_1480" s="T2207">RUS:cult</ta>
            <ta e="T2213" id="Seg_1481" s="T2212">RUS:cult</ta>
            <ta e="T2217" id="Seg_1482" s="T2216">RUS:cult</ta>
            <ta e="T2219" id="Seg_1483" s="T2218">RUS:gram</ta>
            <ta e="T2224" id="Seg_1484" s="T2223">RUS:disc</ta>
            <ta e="T2225" id="Seg_1485" s="T2224">RUS:gram</ta>
            <ta e="T2227" id="Seg_1486" s="T2226">RUS:cult</ta>
            <ta e="T2231" id="Seg_1487" s="T2230">TURK:cult</ta>
            <ta e="T2232" id="Seg_1488" s="T2231">RUS:gram</ta>
            <ta e="T2241" id="Seg_1489" s="T2240">RUS:mod</ta>
            <ta e="T2247" id="Seg_1490" s="T2246">TURK:disc</ta>
            <ta e="T2251" id="Seg_1491" s="T2250">TURK:disc</ta>
            <ta e="T2252" id="Seg_1492" s="T2251">RUS:gram</ta>
            <ta e="T2253" id="Seg_1493" s="T2252">TURK:cult</ta>
            <ta e="T2258" id="Seg_1494" s="T2257">RUS:disc</ta>
            <ta e="T2262" id="Seg_1495" s="T2261">RUS:disc</ta>
            <ta e="T2264" id="Seg_1496" s="T2263">RUS:cult</ta>
            <ta e="T2265" id="Seg_1497" s="T2264">RUS:gram</ta>
            <ta e="T2268" id="Seg_1498" s="T2267">RUS:cult</ta>
            <ta e="T2272" id="Seg_1499" s="T2271">TURK:cult</ta>
            <ta e="T2274" id="Seg_1500" s="T2273">RUS:gram</ta>
            <ta e="T2276" id="Seg_1501" s="T2275">TURK:gram(INDEF)</ta>
            <ta e="T2285" id="Seg_1502" s="T2284">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T2197" id="Seg_1503" s="T2192">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2182" id="Seg_1504" s="T2178">Один человек сеял репу.</ta>
            <ta e="T2185" id="Seg_1505" s="T2182">А [тут] медведь пришёл.</ta>
            <ta e="T2187" id="Seg_1506" s="T2185">«Ты что делаешь?»</ta>
            <ta e="T2189" id="Seg_1507" s="T2187">«Репу сею».</ta>
            <ta e="T2197" id="Seg_1508" s="T2189">«Ну так возьми меня товарищем [=давай вместе сеять]».</ta>
            <ta e="T2208" id="Seg_1509" s="T2197">«Ну [ладно], я возьму корешки, а тебе могу [отдать] вершки».</ta>
            <ta e="T2212" id="Seg_1510" s="T2208">Потом медведь ушёл.</ta>
            <ta e="T2214" id="Seg_1511" s="T2212">Репа выросла.</ta>
            <ta e="T2216" id="Seg_1512" s="T2214">Мужик пришёл.</ta>
            <ta e="T2221" id="Seg_1513" s="T2216">Вырвал репу и дал ему [ботву].</ta>
            <ta e="T2231" id="Seg_1514" s="T2221">Ну, а сам взял репу и поехал продавать.</ta>
            <ta e="T2234" id="Seg_1515" s="T2231">А [тут] медведь идёт.</ta>
            <ta e="T2238" id="Seg_1516" s="T2234">«Дай мне одну попробовать!»</ta>
            <ta e="T2240" id="Seg_1517" s="T2238">Он дал.</ta>
            <ta e="T2243" id="Seg_1518" s="T2240">«Ох, какая вкусная.</ta>
            <ta e="T2248" id="Seg_1519" s="T2243">Я (тебя?) сейчас (съел?)!» [?]</ta>
            <ta e="T2254" id="Seg_1520" s="T2248">Потом он стал рожь сеять.</ta>
            <ta e="T2257" id="Seg_1521" s="T2254">Снова медведь пришёл.</ta>
            <ta e="T2261" id="Seg_1522" s="T2257">«Ну, ты дашь мне что-нибудь?»</ta>
            <ta e="T2268" id="Seg_1523" s="T2261">«Ну, бери корешки, а я возьму вершки».</ta>
            <ta e="T2271" id="Seg_1524" s="T2268">Потом он взял [колосья].</ta>
            <ta e="T2273" id="Seg_1525" s="T2271">Рожь увёз.</ta>
            <ta e="T2279" id="Seg_1526" s="T2273">А [медведь] ничего поделать не смог [с корешками].</ta>
            <ta e="T2288" id="Seg_1527" s="T2279">Потом их дружба умерла [=кончилась], медведь рассердился на мужика.</ta>
            <ta e="T2289" id="Seg_1528" s="T2288">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2182" id="Seg_1529" s="T2178">One man sowed turnip.</ta>
            <ta e="T2185" id="Seg_1530" s="T2182">But a bear came.</ta>
            <ta e="T2187" id="Seg_1531" s="T2185">"What are you doing?"</ta>
            <ta e="T2189" id="Seg_1532" s="T2187">"I am sowing turnip."</ta>
            <ta e="T2197" id="Seg_1533" s="T2189">"So take me as a friend [=let's sow together]."</ta>
            <ta e="T2208" id="Seg_1534" s="T2197">"Well, I will take roots, you will get upper parts."</ta>
            <ta e="T2212" id="Seg_1535" s="T2208">Then the bear went away.</ta>
            <ta e="T2214" id="Seg_1536" s="T2212">His turnip grew.</ta>
            <ta e="T2216" id="Seg_1537" s="T2214">Old man came.</ta>
            <ta e="T2221" id="Seg_1538" s="T2216">Tore out the turnip and gave the bear [the tops].</ta>
            <ta e="T2231" id="Seg_1539" s="T2221">Well, and to himself he took the turnip, [and] went to sell [it].</ta>
            <ta e="T2234" id="Seg_1540" s="T2231">And there comes the bear.</ta>
            <ta e="T2238" id="Seg_1541" s="T2234">"Give me one to eat!"</ta>
            <ta e="T2240" id="Seg_1542" s="T2238">He gave.</ta>
            <ta e="T2243" id="Seg_1543" s="T2240">"Oh, so sweet.</ta>
            <ta e="T2248" id="Seg_1544" s="T2243">I (ate?) (you?) now!" [?]</ta>
            <ta e="T2254" id="Seg_1545" s="T2248">Then he started to sow rye.</ta>
            <ta e="T2257" id="Seg_1546" s="T2254">The bear came again.</ta>
            <ta e="T2261" id="Seg_1547" s="T2257">"Well, will you give me something?"</ta>
            <ta e="T2268" id="Seg_1548" s="T2261">"Well, take the roots, and I will take the upper parts."</ta>
            <ta e="T2271" id="Seg_1549" s="T2268">Then he took [the ears].</ta>
            <ta e="T2273" id="Seg_1550" s="T2271">Took the rye away.</ta>
            <ta e="T2279" id="Seg_1551" s="T2273">And [the bear] could not do anything [with the roots].</ta>
            <ta e="T2288" id="Seg_1552" s="T2279">Then their friendship died [=ended], the bear got angry at the man.</ta>
            <ta e="T2289" id="Seg_1553" s="T2288">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2182" id="Seg_1554" s="T2178">Ein Mann säte Weißrüben</ta>
            <ta e="T2185" id="Seg_1555" s="T2182">Aber es kam ein Bär.</ta>
            <ta e="T2187" id="Seg_1556" s="T2185">„Was machst du?“</ta>
            <ta e="T2189" id="Seg_1557" s="T2187">„Ich sähe Weißrüben.“</ta>
            <ta e="T2197" id="Seg_1558" s="T2189">„Also nimm mich als Freund [=lass uns zusammen säen].“</ta>
            <ta e="T2208" id="Seg_1559" s="T2197">„Also, ich nehme Wurzeln, du bekommst die oberen Teile.“</ta>
            <ta e="T2212" id="Seg_1560" s="T2208">Dann ging der Bär weg.</ta>
            <ta e="T2214" id="Seg_1561" s="T2212">Seine Weißrüben wuchsen.</ta>
            <ta e="T2216" id="Seg_1562" s="T2214">Alter Mann kam.</ta>
            <ta e="T2221" id="Seg_1563" s="T2216">Rupfte die Weißrüben heraus und gab ihm [die Kraut].</ta>
            <ta e="T2231" id="Seg_1564" s="T2221">Also, und für sich selbst nahm er die Weißrüben, [und] ging [sie] verkaufen.</ta>
            <ta e="T2234" id="Seg_1565" s="T2231">Und da kommt der Bär.</ta>
            <ta e="T2238" id="Seg_1566" s="T2234">„Gib mir eine zu fressen!“</ta>
            <ta e="T2240" id="Seg_1567" s="T2238">Er gab.</ta>
            <ta e="T2243" id="Seg_1568" s="T2240">„Oh, so süß.</ta>
            <ta e="T2248" id="Seg_1569" s="T2243">Ich (habe gefressen?) nun (du?)!“ [?]</ta>
            <ta e="T2254" id="Seg_1570" s="T2248">Dann fing er an, Roggen zu sähen.</ta>
            <ta e="T2257" id="Seg_1571" s="T2254">Der Bär kommt wieder.</ta>
            <ta e="T2261" id="Seg_1572" s="T2257">„Also, gibst du mir etwas?“</ta>
            <ta e="T2268" id="Seg_1573" s="T2261">„Also, nimm die Wurzeln, und ich nehme die oberen Teile.“</ta>
            <ta e="T2271" id="Seg_1574" s="T2268">Dann nahm er [die Ähre].</ta>
            <ta e="T2273" id="Seg_1575" s="T2271">Nahm den Roggen weg.</ta>
            <ta e="T2279" id="Seg_1576" s="T2273">Und [der Bär] konnte nichts tun [mit den Wurzeln].</ta>
            <ta e="T2288" id="Seg_1577" s="T2279">Dann starb [=endet] ihre Freundschaft, der Bär wurde wütend auf den Mann.</ta>
            <ta e="T2289" id="Seg_1578" s="T2288">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2182" id="Seg_1579" s="T2178">[GVY:] There are various versions of the tale; the closest one is http://mirckazok.ru/view_post.php?id=4853.</ta>
            <ta e="T2208" id="Seg_1580" s="T2197">[KlT:] Ru. корешки 'roots', вершки 'upper parts'.</ta>
            <ta e="T2248" id="Seg_1581" s="T2243">[GVY:] Rather "I'll eat you"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2178" />
            <conversion-tli id="T2179" />
            <conversion-tli id="T2180" />
            <conversion-tli id="T2181" />
            <conversion-tli id="T2182" />
            <conversion-tli id="T2183" />
            <conversion-tli id="T2184" />
            <conversion-tli id="T2185" />
            <conversion-tli id="T2186" />
            <conversion-tli id="T2187" />
            <conversion-tli id="T2188" />
            <conversion-tli id="T2189" />
            <conversion-tli id="T2190" />
            <conversion-tli id="T2191" />
            <conversion-tli id="T2192" />
            <conversion-tli id="T2193" />
            <conversion-tli id="T2194" />
            <conversion-tli id="T2195" />
            <conversion-tli id="T2196" />
            <conversion-tli id="T2197" />
            <conversion-tli id="T2198" />
            <conversion-tli id="T2199" />
            <conversion-tli id="T2200" />
            <conversion-tli id="T2201" />
            <conversion-tli id="T2202" />
            <conversion-tli id="T2203" />
            <conversion-tli id="T2204" />
            <conversion-tli id="T2205" />
            <conversion-tli id="T2206" />
            <conversion-tli id="T2207" />
            <conversion-tli id="T2208" />
            <conversion-tli id="T2209" />
            <conversion-tli id="T2210" />
            <conversion-tli id="T2211" />
            <conversion-tli id="T2212" />
            <conversion-tli id="T2213" />
            <conversion-tli id="T2214" />
            <conversion-tli id="T2215" />
            <conversion-tli id="T2216" />
            <conversion-tli id="T2217" />
            <conversion-tli id="T2218" />
            <conversion-tli id="T2219" />
            <conversion-tli id="T2220" />
            <conversion-tli id="T2221" />
            <conversion-tli id="T2222" />
            <conversion-tli id="T2223" />
            <conversion-tli id="T2224" />
            <conversion-tli id="T2225" />
            <conversion-tli id="T2226" />
            <conversion-tli id="T2227" />
            <conversion-tli id="T2228" />
            <conversion-tli id="T2229" />
            <conversion-tli id="T2230" />
            <conversion-tli id="T2231" />
            <conversion-tli id="T2232" />
            <conversion-tli id="T2233" />
            <conversion-tli id="T2234" />
            <conversion-tli id="T2235" />
            <conversion-tli id="T2236" />
            <conversion-tli id="T2237" />
            <conversion-tli id="T2238" />
            <conversion-tli id="T2239" />
            <conversion-tli id="T2240" />
            <conversion-tli id="T2241" />
            <conversion-tli id="T2242" />
            <conversion-tli id="T2243" />
            <conversion-tli id="T2244" />
            <conversion-tli id="T2245" />
            <conversion-tli id="T2246" />
            <conversion-tli id="T2247" />
            <conversion-tli id="T2248" />
            <conversion-tli id="T2249" />
            <conversion-tli id="T2250" />
            <conversion-tli id="T2251" />
            <conversion-tli id="T2252" />
            <conversion-tli id="T2253" />
            <conversion-tli id="T2254" />
            <conversion-tli id="T2255" />
            <conversion-tli id="T2256" />
            <conversion-tli id="T2257" />
            <conversion-tli id="T2258" />
            <conversion-tli id="T2259" />
            <conversion-tli id="T2260" />
            <conversion-tli id="T2261" />
            <conversion-tli id="T2262" />
            <conversion-tli id="T2263" />
            <conversion-tli id="T2264" />
            <conversion-tli id="T2265" />
            <conversion-tli id="T2266" />
            <conversion-tli id="T2267" />
            <conversion-tli id="T2268" />
            <conversion-tli id="T2269" />
            <conversion-tli id="T2270" />
            <conversion-tli id="T2271" />
            <conversion-tli id="T2272" />
            <conversion-tli id="T2273" />
            <conversion-tli id="T2274" />
            <conversion-tli id="T2275" />
            <conversion-tli id="T2276" />
            <conversion-tli id="T2277" />
            <conversion-tli id="T2278" />
            <conversion-tli id="T2279" />
            <conversion-tli id="T2280" />
            <conversion-tli id="T2281" />
            <conversion-tli id="T2282" />
            <conversion-tli id="T2283" />
            <conversion-tli id="T2284" />
            <conversion-tli id="T2285" />
            <conversion-tli id="T2286" />
            <conversion-tli id="T2287" />
            <conversion-tli id="T2288" />
            <conversion-tli id="T2289" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
