<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFEB10005-4559-D5B0-0378-360D24C7645B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_DuckFrog_flk.wav" />
         <referenced-file url="PKZ_196X_DuckFrog_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_DuckFrog_flk\PKZ_196X_DuckFrog_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">66</ud-information>
            <ud-information attribute-name="# HIAT:w">47</ud-information>
            <ud-information attribute-name="# e">47</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T559" time="0.012" type="appl" />
         <tli id="T560" time="0.589" type="appl" />
         <tli id="T561" time="1.167" type="appl" />
         <tli id="T562" time="1.744" type="appl" />
         <tli id="T563" time="2.505" type="appl" />
         <tli id="T564" time="3.265" type="appl" />
         <tli id="T565" time="4.026" type="appl" />
         <tli id="T566" time="4.786" type="appl" />
         <tli id="T567" time="5.859492626366269" />
         <tli id="T568" time="6.372" type="appl" />
         <tli id="T569" time="6.81" type="appl" />
         <tli id="T570" time="7.248" type="appl" />
         <tli id="T571" time="7.686" type="appl" />
         <tli id="T572" time="8.312613543889348" />
         <tli id="T573" time="8.724" type="appl" />
         <tli id="T574" time="9.165" type="appl" />
         <tli id="T575" time="9.605" type="appl" />
         <tli id="T576" time="10.046" type="appl" />
         <tli id="T577" time="10.745736193063056" />
         <tli id="T578" time="11.125" type="appl" />
         <tli id="T579" time="11.583" type="appl" />
         <tli id="T580" time="12.042" type="appl" />
         <tli id="T581" time="12.5" type="appl" />
         <tli id="T582" time="12.958" type="appl" />
         <tli id="T583" time="13.858799966115441" />
         <tli id="T584" time="15.462" type="appl" />
         <tli id="T585" time="16.874" type="appl" />
         <tli id="T586" time="18.286" type="appl" />
         <tli id="T587" time="19.698" type="appl" />
         <tli id="T588" time="20.772" type="appl" />
         <tli id="T589" time="21.846" type="appl" />
         <tli id="T590" time="22.92" type="appl" />
         <tli id="T591" time="23.994" type="appl" />
         <tli id="T592" time="27.97757742077273" />
         <tli id="T593" time="28.603" type="appl" />
         <tli id="T594" time="29.096" type="appl" />
         <tli id="T595" time="29.59" type="appl" />
         <tli id="T596" time="30.083" type="appl" />
         <tli id="T597" time="31.217296893371145" />
         <tli id="T598" time="31.991" type="appl" />
         <tli id="T599" time="32.756" type="appl" />
         <tli id="T600" time="33.522" type="appl" />
         <tli id="T601" time="34.288" type="appl" />
         <tli id="T602" time="35.054" type="appl" />
         <tli id="T603" time="35.82" type="appl" />
         <tli id="T604" time="36.585" type="appl" />
         <tli id="T605" time="37.351" type="appl" />
         <tli id="T606" time="38.27" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T606" id="Seg_0" n="sc" s="T559">
               <ts e="T562" id="Seg_2" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_4" n="HIAT:w" s="T559">Nab</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_7" n="HIAT:w" s="T560">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_10" n="HIAT:w" s="T561">lʼagušen</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_14" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_16" n="HIAT:w" s="T562">Nab</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_19" n="HIAT:w" s="T563">nʼergölaʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_22" n="HIAT:w" s="T564">šobi</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_26" n="HIAT:w" s="T565">bünə</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_29" n="HIAT:w" s="T566">saʔməluʔpi</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_33" n="HIAT:u" s="T567">
                  <nts id="Seg_34" n="HIAT:ip">"</nts>
                  <ts e="T568" id="Seg_36" n="HIAT:w" s="T567">Ĭmbi</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_39" n="HIAT:w" s="T568">tăn</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_42" n="HIAT:w" s="T569">măna</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_45" n="HIAT:w" s="T570">ej</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_48" n="HIAT:w" s="T571">tĭmniel</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_52" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_54" n="HIAT:w" s="T572">Măn</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">(</nts>
                  <ts e="T574" id="Seg_58" n="HIAT:w" s="T573">ig-</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_61" n="HIAT:w" s="T574">ne-</ts>
                  <nts id="Seg_62" n="HIAT:ip">)</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_65" n="HIAT:w" s="T575">nab</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_68" n="HIAT:w" s="T576">igem</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_72" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_74" n="HIAT:w" s="T577">A</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_77" n="HIAT:w" s="T578">tăn</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_80" n="HIAT:w" s="T579">lʼaguška</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_83" n="HIAT:w" s="T580">ej</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_86" n="HIAT:w" s="T581">tĭmniel</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_89" n="HIAT:w" s="T582">măna</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_93" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_95" n="HIAT:w" s="T583">Măn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_98" n="HIAT:w" s="T584">bügən</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_101" n="HIAT:w" s="T585">moliam</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_104" n="HIAT:w" s="T586">mĭnzittə</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_108" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_110" n="HIAT:w" s="T587">I</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_113" n="HIAT:w" s="T588">dʼügən</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_116" n="HIAT:w" s="T589">moliam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_119" n="HIAT:w" s="T590">i</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_122" n="HIAT:w" s="T591">nʼuʔdə</ts>
                  <nts id="Seg_123" n="HIAT:ip">"</nts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_127" n="HIAT:u" s="T592">
                  <nts id="Seg_128" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_130" n="HIAT:w" s="T592">A-</ts>
                  <nts id="Seg_131" n="HIAT:ip">)</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_134" n="HIAT:w" s="T593">A</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_137" n="HIAT:w" s="T594">dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_140" n="HIAT:w" s="T595">dĭʔnə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_143" n="HIAT:w" s="T596">măndə:</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <ts e="T598" id="Seg_147" n="HIAT:w" s="T597">Onʼiʔsiʔ</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_150" n="HIAT:w" s="T598">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_153" n="HIAT:w" s="T599">nada</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_156" n="HIAT:w" s="T600">togonorzittə</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_160" n="HIAT:w" s="T601">a</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_163" n="HIAT:w" s="T602">iʔgö</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_166" n="HIAT:w" s="T603">iʔ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_169" n="HIAT:w" s="T604">iʔ</ts>
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_174" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_176" n="HIAT:w" s="T605">Kabarləj</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T606" id="Seg_179" n="sc" s="T559">
               <ts e="T560" id="Seg_181" n="e" s="T559">Nab </ts>
               <ts e="T561" id="Seg_183" n="e" s="T560">i </ts>
               <ts e="T562" id="Seg_185" n="e" s="T561">lʼagušen. </ts>
               <ts e="T563" id="Seg_187" n="e" s="T562">Nab </ts>
               <ts e="T564" id="Seg_189" n="e" s="T563">nʼergölaʔ </ts>
               <ts e="T565" id="Seg_191" n="e" s="T564">šobi, </ts>
               <ts e="T566" id="Seg_193" n="e" s="T565">bünə </ts>
               <ts e="T567" id="Seg_195" n="e" s="T566">saʔməluʔpi. </ts>
               <ts e="T568" id="Seg_197" n="e" s="T567">"Ĭmbi </ts>
               <ts e="T569" id="Seg_199" n="e" s="T568">tăn </ts>
               <ts e="T570" id="Seg_201" n="e" s="T569">măna </ts>
               <ts e="T571" id="Seg_203" n="e" s="T570">ej </ts>
               <ts e="T572" id="Seg_205" n="e" s="T571">tĭmniel. </ts>
               <ts e="T573" id="Seg_207" n="e" s="T572">Măn </ts>
               <ts e="T574" id="Seg_209" n="e" s="T573">(ig- </ts>
               <ts e="T575" id="Seg_211" n="e" s="T574">ne-) </ts>
               <ts e="T576" id="Seg_213" n="e" s="T575">nab </ts>
               <ts e="T577" id="Seg_215" n="e" s="T576">igem. </ts>
               <ts e="T578" id="Seg_217" n="e" s="T577">A </ts>
               <ts e="T579" id="Seg_219" n="e" s="T578">tăn </ts>
               <ts e="T580" id="Seg_221" n="e" s="T579">lʼaguška </ts>
               <ts e="T581" id="Seg_223" n="e" s="T580">ej </ts>
               <ts e="T582" id="Seg_225" n="e" s="T581">tĭmniel </ts>
               <ts e="T583" id="Seg_227" n="e" s="T582">măna. </ts>
               <ts e="T584" id="Seg_229" n="e" s="T583">Măn </ts>
               <ts e="T585" id="Seg_231" n="e" s="T584">bügən </ts>
               <ts e="T586" id="Seg_233" n="e" s="T585">moliam </ts>
               <ts e="T587" id="Seg_235" n="e" s="T586">mĭnzittə. </ts>
               <ts e="T588" id="Seg_237" n="e" s="T587">I </ts>
               <ts e="T589" id="Seg_239" n="e" s="T588">dʼügən </ts>
               <ts e="T590" id="Seg_241" n="e" s="T589">moliam </ts>
               <ts e="T591" id="Seg_243" n="e" s="T590">i </ts>
               <ts e="T592" id="Seg_245" n="e" s="T591">nʼuʔdə". </ts>
               <ts e="T593" id="Seg_247" n="e" s="T592">(A-) </ts>
               <ts e="T594" id="Seg_249" n="e" s="T593">A </ts>
               <ts e="T595" id="Seg_251" n="e" s="T594">dĭ </ts>
               <ts e="T596" id="Seg_253" n="e" s="T595">dĭʔnə </ts>
               <ts e="T597" id="Seg_255" n="e" s="T596">măndə: </ts>
               <ts e="T598" id="Seg_257" n="e" s="T597">"Onʼiʔsiʔ </ts>
               <ts e="T599" id="Seg_259" n="e" s="T598">ĭmbi-nʼibudʼ </ts>
               <ts e="T600" id="Seg_261" n="e" s="T599">nada </ts>
               <ts e="T601" id="Seg_263" n="e" s="T600">togonorzittə, </ts>
               <ts e="T602" id="Seg_265" n="e" s="T601">a </ts>
               <ts e="T603" id="Seg_267" n="e" s="T602">iʔgö </ts>
               <ts e="T604" id="Seg_269" n="e" s="T603">iʔ </ts>
               <ts e="T605" id="Seg_271" n="e" s="T604">iʔ". </ts>
               <ts e="T606" id="Seg_273" n="e" s="T605">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T562" id="Seg_274" s="T559">PKZ_196X_DuckFrog_flk.001 (001)</ta>
            <ta e="T567" id="Seg_275" s="T562">PKZ_196X_DuckFrog_flk.002 (002)</ta>
            <ta e="T572" id="Seg_276" s="T567">PKZ_196X_DuckFrog_flk.003 (003)</ta>
            <ta e="T577" id="Seg_277" s="T572">PKZ_196X_DuckFrog_flk.004 (004)</ta>
            <ta e="T583" id="Seg_278" s="T577">PKZ_196X_DuckFrog_flk.005 (005)</ta>
            <ta e="T587" id="Seg_279" s="T583">PKZ_196X_DuckFrog_flk.006 (006)</ta>
            <ta e="T592" id="Seg_280" s="T587">PKZ_196X_DuckFrog_flk.007 (007)</ta>
            <ta e="T605" id="Seg_281" s="T592">PKZ_196X_DuckFrog_flk.008 (008)</ta>
            <ta e="T606" id="Seg_282" s="T605">PKZ_196X_DuckFrog_flk.009 (010)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T562" id="Seg_283" s="T559">Nab i lʼagušen. </ta>
            <ta e="T567" id="Seg_284" s="T562">Nab nʼergölaʔ šobi, bünə saʔməluʔpi. </ta>
            <ta e="T572" id="Seg_285" s="T567">"Ĭmbi tăn măna ej tĭmniel. </ta>
            <ta e="T577" id="Seg_286" s="T572">Măn (ig- ne-) nab igem. </ta>
            <ta e="T583" id="Seg_287" s="T577">A tăn lʼaguška ej tĭmniel măna. </ta>
            <ta e="T587" id="Seg_288" s="T583">Măn bügən moliam mĭnzittə. </ta>
            <ta e="T592" id="Seg_289" s="T587">I dʼügən moliam i nʼuʔdə". </ta>
            <ta e="T605" id="Seg_290" s="T592">(A-) A dĭ dĭʔnə măndə: "Onʼiʔsiʔ ĭmbi-nʼibudʼ nada togonorzittə, a iʔgö i iʔ". </ta>
            <ta e="T606" id="Seg_291" s="T605">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T560" id="Seg_292" s="T559">nab</ta>
            <ta e="T561" id="Seg_293" s="T560">i</ta>
            <ta e="T562" id="Seg_294" s="T561">lʼagušen</ta>
            <ta e="T563" id="Seg_295" s="T562">nab</ta>
            <ta e="T564" id="Seg_296" s="T563">nʼergö-laʔ</ta>
            <ta e="T565" id="Seg_297" s="T564">šo-bi</ta>
            <ta e="T566" id="Seg_298" s="T565">bü-nə</ta>
            <ta e="T567" id="Seg_299" s="T566">saʔmə-luʔ-pi</ta>
            <ta e="T568" id="Seg_300" s="T567">ĭmbi</ta>
            <ta e="T569" id="Seg_301" s="T568">tăn</ta>
            <ta e="T570" id="Seg_302" s="T569">măna</ta>
            <ta e="T571" id="Seg_303" s="T570">ej</ta>
            <ta e="T572" id="Seg_304" s="T571">tĭm-nie-l</ta>
            <ta e="T573" id="Seg_305" s="T572">măn</ta>
            <ta e="T576" id="Seg_306" s="T575">nab</ta>
            <ta e="T577" id="Seg_307" s="T576">i-ge-m</ta>
            <ta e="T578" id="Seg_308" s="T577">a</ta>
            <ta e="T579" id="Seg_309" s="T578">tăn</ta>
            <ta e="T580" id="Seg_310" s="T579">lʼaguška</ta>
            <ta e="T581" id="Seg_311" s="T580">ej</ta>
            <ta e="T582" id="Seg_312" s="T581">tĭm-nie-l</ta>
            <ta e="T583" id="Seg_313" s="T582">măna</ta>
            <ta e="T584" id="Seg_314" s="T583">măn</ta>
            <ta e="T585" id="Seg_315" s="T584">bü-gən</ta>
            <ta e="T586" id="Seg_316" s="T585">mo-lia-m</ta>
            <ta e="T587" id="Seg_317" s="T586">mĭn-zittə</ta>
            <ta e="T588" id="Seg_318" s="T587">i</ta>
            <ta e="T589" id="Seg_319" s="T588">dʼü-gən</ta>
            <ta e="T590" id="Seg_320" s="T589">mo-lia-m</ta>
            <ta e="T591" id="Seg_321" s="T590">i</ta>
            <ta e="T592" id="Seg_322" s="T591">nʼuʔdə</ta>
            <ta e="T594" id="Seg_323" s="T593">a</ta>
            <ta e="T595" id="Seg_324" s="T594">dĭ</ta>
            <ta e="T596" id="Seg_325" s="T595">dĭʔ-nə</ta>
            <ta e="T597" id="Seg_326" s="T596">măn-də</ta>
            <ta e="T598" id="Seg_327" s="T597">onʼiʔ-siʔ</ta>
            <ta e="T599" id="Seg_328" s="T598">ĭmbi=nʼibudʼ</ta>
            <ta e="T600" id="Seg_329" s="T599">nada</ta>
            <ta e="T601" id="Seg_330" s="T600">togonor-zittə</ta>
            <ta e="T602" id="Seg_331" s="T601">a</ta>
            <ta e="T603" id="Seg_332" s="T602">iʔgö</ta>
            <ta e="T604" id="Seg_333" s="T603">i-ʔ</ta>
            <ta e="T605" id="Seg_334" s="T604">i-ʔ</ta>
            <ta e="T606" id="Seg_335" s="T605">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T560" id="Seg_336" s="T559">nabə</ta>
            <ta e="T561" id="Seg_337" s="T560">i</ta>
            <ta e="T562" id="Seg_338" s="T561">lʼaguška</ta>
            <ta e="T563" id="Seg_339" s="T562">nabə</ta>
            <ta e="T564" id="Seg_340" s="T563">nʼergö-lAʔ</ta>
            <ta e="T565" id="Seg_341" s="T564">šo-bi</ta>
            <ta e="T566" id="Seg_342" s="T565">bü-Tə</ta>
            <ta e="T567" id="Seg_343" s="T566">saʔmə-luʔbdə-bi</ta>
            <ta e="T568" id="Seg_344" s="T567">ĭmbi</ta>
            <ta e="T569" id="Seg_345" s="T568">tăn</ta>
            <ta e="T570" id="Seg_346" s="T569">măna</ta>
            <ta e="T571" id="Seg_347" s="T570">ej</ta>
            <ta e="T572" id="Seg_348" s="T571">tĭm-liA-l</ta>
            <ta e="T573" id="Seg_349" s="T572">măn</ta>
            <ta e="T576" id="Seg_350" s="T575">nabə</ta>
            <ta e="T577" id="Seg_351" s="T576">i-gA-m</ta>
            <ta e="T578" id="Seg_352" s="T577">a</ta>
            <ta e="T579" id="Seg_353" s="T578">tăn</ta>
            <ta e="T580" id="Seg_354" s="T579">lʼaguška</ta>
            <ta e="T581" id="Seg_355" s="T580">ej</ta>
            <ta e="T582" id="Seg_356" s="T581">tĭm-liA-l</ta>
            <ta e="T583" id="Seg_357" s="T582">măna</ta>
            <ta e="T584" id="Seg_358" s="T583">măn</ta>
            <ta e="T585" id="Seg_359" s="T584">bü-Kən</ta>
            <ta e="T586" id="Seg_360" s="T585">mo-liA-m</ta>
            <ta e="T587" id="Seg_361" s="T586">mĭn-zittə</ta>
            <ta e="T588" id="Seg_362" s="T587">i</ta>
            <ta e="T589" id="Seg_363" s="T588">tʼo-Kən</ta>
            <ta e="T590" id="Seg_364" s="T589">mo-liA-m</ta>
            <ta e="T591" id="Seg_365" s="T590">i</ta>
            <ta e="T592" id="Seg_366" s="T591">nʼuʔdə</ta>
            <ta e="T594" id="Seg_367" s="T593">a</ta>
            <ta e="T595" id="Seg_368" s="T594">dĭ</ta>
            <ta e="T596" id="Seg_369" s="T595">dĭ-Tə</ta>
            <ta e="T597" id="Seg_370" s="T596">măn-ntə</ta>
            <ta e="T598" id="Seg_371" s="T597">onʼiʔ-ziʔ</ta>
            <ta e="T599" id="Seg_372" s="T598">ĭmbi=nʼibudʼ</ta>
            <ta e="T600" id="Seg_373" s="T599">nadə</ta>
            <ta e="T601" id="Seg_374" s="T600">togonər-zittə</ta>
            <ta e="T602" id="Seg_375" s="T601">a</ta>
            <ta e="T603" id="Seg_376" s="T602">iʔgö</ta>
            <ta e="T604" id="Seg_377" s="T603">e-ʔ</ta>
            <ta e="T605" id="Seg_378" s="T604">i-ʔ</ta>
            <ta e="T606" id="Seg_379" s="T605">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T560" id="Seg_380" s="T559">duck.[NOM.SG]</ta>
            <ta e="T561" id="Seg_381" s="T560">and</ta>
            <ta e="T562" id="Seg_382" s="T561">frog.[NOM.SG]</ta>
            <ta e="T563" id="Seg_383" s="T562">duck.[NOM.SG]</ta>
            <ta e="T564" id="Seg_384" s="T563">fly-CVB</ta>
            <ta e="T565" id="Seg_385" s="T564">come-PST.[3SG]</ta>
            <ta e="T566" id="Seg_386" s="T565">water-LAT</ta>
            <ta e="T567" id="Seg_387" s="T566">fall-MOM-PST.[3SG]</ta>
            <ta e="T568" id="Seg_388" s="T567">what.[NOM.SG]</ta>
            <ta e="T569" id="Seg_389" s="T568">you.NOM</ta>
            <ta e="T570" id="Seg_390" s="T569">I.ACC</ta>
            <ta e="T571" id="Seg_391" s="T570">NEG</ta>
            <ta e="T572" id="Seg_392" s="T571">know-PRS-2SG</ta>
            <ta e="T573" id="Seg_393" s="T572">I.NOM</ta>
            <ta e="T576" id="Seg_394" s="T575">duck.[NOM.SG]</ta>
            <ta e="T577" id="Seg_395" s="T576">be-PRS-1SG</ta>
            <ta e="T578" id="Seg_396" s="T577">and</ta>
            <ta e="T579" id="Seg_397" s="T578">you.NOM</ta>
            <ta e="T580" id="Seg_398" s="T579">frog.[NOM.SG]</ta>
            <ta e="T581" id="Seg_399" s="T580">NEG</ta>
            <ta e="T582" id="Seg_400" s="T581">know-PRS-2SG</ta>
            <ta e="T583" id="Seg_401" s="T582">I.ACC</ta>
            <ta e="T584" id="Seg_402" s="T583">I.NOM</ta>
            <ta e="T585" id="Seg_403" s="T584">water-LOC</ta>
            <ta e="T586" id="Seg_404" s="T585">can-PRS-1SG</ta>
            <ta e="T587" id="Seg_405" s="T586">go-INF.LAT</ta>
            <ta e="T588" id="Seg_406" s="T587">and</ta>
            <ta e="T589" id="Seg_407" s="T588">earth-LOC</ta>
            <ta e="T590" id="Seg_408" s="T589">can-PRS-1SG</ta>
            <ta e="T591" id="Seg_409" s="T590">and</ta>
            <ta e="T592" id="Seg_410" s="T591">up</ta>
            <ta e="T594" id="Seg_411" s="T593">and</ta>
            <ta e="T595" id="Seg_412" s="T594">this.[NOM.SG]</ta>
            <ta e="T596" id="Seg_413" s="T595">this-LAT</ta>
            <ta e="T597" id="Seg_414" s="T596">say-IPFVZ.[3SG]</ta>
            <ta e="T598" id="Seg_415" s="T597">one-INS</ta>
            <ta e="T599" id="Seg_416" s="T598">what=INDEF</ta>
            <ta e="T600" id="Seg_417" s="T599">one.should</ta>
            <ta e="T601" id="Seg_418" s="T600">work-INF.LAT</ta>
            <ta e="T602" id="Seg_419" s="T601">and</ta>
            <ta e="T603" id="Seg_420" s="T602">many</ta>
            <ta e="T604" id="Seg_421" s="T603">NEG.AUX-IMP.2SG</ta>
            <ta e="T605" id="Seg_422" s="T604">take-CNG</ta>
            <ta e="T606" id="Seg_423" s="T605">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T560" id="Seg_424" s="T559">утка.[NOM.SG]</ta>
            <ta e="T561" id="Seg_425" s="T560">и</ta>
            <ta e="T562" id="Seg_426" s="T561">лягушка.[NOM.SG]</ta>
            <ta e="T563" id="Seg_427" s="T562">утка.[NOM.SG]</ta>
            <ta e="T564" id="Seg_428" s="T563">лететь-CVB</ta>
            <ta e="T565" id="Seg_429" s="T564">прийти-PST.[3SG]</ta>
            <ta e="T566" id="Seg_430" s="T565">вода-LAT</ta>
            <ta e="T567" id="Seg_431" s="T566">упасть-MOM-PST.[3SG]</ta>
            <ta e="T568" id="Seg_432" s="T567">что.[NOM.SG]</ta>
            <ta e="T569" id="Seg_433" s="T568">ты.NOM</ta>
            <ta e="T570" id="Seg_434" s="T569">я.ACC</ta>
            <ta e="T571" id="Seg_435" s="T570">NEG</ta>
            <ta e="T572" id="Seg_436" s="T571">знать-PRS-2SG</ta>
            <ta e="T573" id="Seg_437" s="T572">я.NOM</ta>
            <ta e="T576" id="Seg_438" s="T575">утка.[NOM.SG]</ta>
            <ta e="T577" id="Seg_439" s="T576">быть-PRS-1SG</ta>
            <ta e="T578" id="Seg_440" s="T577">а</ta>
            <ta e="T579" id="Seg_441" s="T578">ты.NOM</ta>
            <ta e="T580" id="Seg_442" s="T579">лягушка.[NOM.SG]</ta>
            <ta e="T581" id="Seg_443" s="T580">NEG</ta>
            <ta e="T582" id="Seg_444" s="T581">знать-PRS-2SG</ta>
            <ta e="T583" id="Seg_445" s="T582">я.ACC</ta>
            <ta e="T584" id="Seg_446" s="T583">я.NOM</ta>
            <ta e="T585" id="Seg_447" s="T584">вода-LOC</ta>
            <ta e="T586" id="Seg_448" s="T585">мочь-PRS-1SG</ta>
            <ta e="T587" id="Seg_449" s="T586">идти-INF.LAT</ta>
            <ta e="T588" id="Seg_450" s="T587">и</ta>
            <ta e="T589" id="Seg_451" s="T588">земля-LOC</ta>
            <ta e="T590" id="Seg_452" s="T589">мочь-PRS-1SG</ta>
            <ta e="T591" id="Seg_453" s="T590">и</ta>
            <ta e="T592" id="Seg_454" s="T591">вверх</ta>
            <ta e="T594" id="Seg_455" s="T593">а</ta>
            <ta e="T595" id="Seg_456" s="T594">этот.[NOM.SG]</ta>
            <ta e="T596" id="Seg_457" s="T595">этот-LAT</ta>
            <ta e="T597" id="Seg_458" s="T596">сказать-IPFVZ.[3SG]</ta>
            <ta e="T598" id="Seg_459" s="T597">один-INS</ta>
            <ta e="T599" id="Seg_460" s="T598">что=INDEF</ta>
            <ta e="T600" id="Seg_461" s="T599">надо</ta>
            <ta e="T601" id="Seg_462" s="T600">работать-INF.LAT</ta>
            <ta e="T602" id="Seg_463" s="T601">а</ta>
            <ta e="T603" id="Seg_464" s="T602">много</ta>
            <ta e="T604" id="Seg_465" s="T603">NEG.AUX-IMP.2SG</ta>
            <ta e="T605" id="Seg_466" s="T604">взять-CNG</ta>
            <ta e="T606" id="Seg_467" s="T605">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T560" id="Seg_468" s="T559">n-n:case</ta>
            <ta e="T561" id="Seg_469" s="T560">conj</ta>
            <ta e="T562" id="Seg_470" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_471" s="T562">n-n:case</ta>
            <ta e="T564" id="Seg_472" s="T563">v-v:n.fin</ta>
            <ta e="T565" id="Seg_473" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_474" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_475" s="T566">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_476" s="T567">que-n:case</ta>
            <ta e="T569" id="Seg_477" s="T568">pers</ta>
            <ta e="T570" id="Seg_478" s="T569">pers</ta>
            <ta e="T571" id="Seg_479" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_480" s="T571">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_481" s="T572">pers</ta>
            <ta e="T576" id="Seg_482" s="T575">n-n:case</ta>
            <ta e="T577" id="Seg_483" s="T576">v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_484" s="T577">conj</ta>
            <ta e="T579" id="Seg_485" s="T578">pers</ta>
            <ta e="T580" id="Seg_486" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_487" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_488" s="T581">v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_489" s="T582">pers</ta>
            <ta e="T584" id="Seg_490" s="T583">pers</ta>
            <ta e="T585" id="Seg_491" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_492" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_493" s="T586">v-v:n.fin</ta>
            <ta e="T588" id="Seg_494" s="T587">conj</ta>
            <ta e="T589" id="Seg_495" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_496" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_497" s="T590">conj</ta>
            <ta e="T592" id="Seg_498" s="T591">adv</ta>
            <ta e="T594" id="Seg_499" s="T593">conj</ta>
            <ta e="T595" id="Seg_500" s="T594">dempro-n:case</ta>
            <ta e="T596" id="Seg_501" s="T595">dempro-n:case</ta>
            <ta e="T597" id="Seg_502" s="T596">v-v&gt;v-v:pn</ta>
            <ta e="T598" id="Seg_503" s="T597">num-n:case</ta>
            <ta e="T599" id="Seg_504" s="T598">que=ptcl</ta>
            <ta e="T600" id="Seg_505" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_506" s="T600">v-v:n.fin</ta>
            <ta e="T602" id="Seg_507" s="T601">conj</ta>
            <ta e="T603" id="Seg_508" s="T602">quant</ta>
            <ta e="T604" id="Seg_509" s="T603">aux-v:mood.pn</ta>
            <ta e="T605" id="Seg_510" s="T604">v-v:n.fin</ta>
            <ta e="T606" id="Seg_511" s="T605">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T560" id="Seg_512" s="T559">n</ta>
            <ta e="T561" id="Seg_513" s="T560">conj</ta>
            <ta e="T562" id="Seg_514" s="T561">n</ta>
            <ta e="T563" id="Seg_515" s="T562">n</ta>
            <ta e="T564" id="Seg_516" s="T563">v</ta>
            <ta e="T565" id="Seg_517" s="T564">v</ta>
            <ta e="T566" id="Seg_518" s="T565">n</ta>
            <ta e="T567" id="Seg_519" s="T566">v</ta>
            <ta e="T568" id="Seg_520" s="T567">que</ta>
            <ta e="T569" id="Seg_521" s="T568">pers</ta>
            <ta e="T570" id="Seg_522" s="T569">pers</ta>
            <ta e="T571" id="Seg_523" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_524" s="T571">v</ta>
            <ta e="T573" id="Seg_525" s="T572">pers</ta>
            <ta e="T576" id="Seg_526" s="T575">n</ta>
            <ta e="T577" id="Seg_527" s="T576">v</ta>
            <ta e="T578" id="Seg_528" s="T577">conj</ta>
            <ta e="T579" id="Seg_529" s="T578">pers</ta>
            <ta e="T580" id="Seg_530" s="T579">n</ta>
            <ta e="T581" id="Seg_531" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_532" s="T581">v</ta>
            <ta e="T583" id="Seg_533" s="T582">pers</ta>
            <ta e="T584" id="Seg_534" s="T583">pers</ta>
            <ta e="T585" id="Seg_535" s="T584">n</ta>
            <ta e="T586" id="Seg_536" s="T585">v</ta>
            <ta e="T587" id="Seg_537" s="T586">v</ta>
            <ta e="T588" id="Seg_538" s="T587">conj</ta>
            <ta e="T589" id="Seg_539" s="T588">n</ta>
            <ta e="T590" id="Seg_540" s="T589">v</ta>
            <ta e="T591" id="Seg_541" s="T590">conj</ta>
            <ta e="T592" id="Seg_542" s="T591">adv</ta>
            <ta e="T594" id="Seg_543" s="T593">conj</ta>
            <ta e="T595" id="Seg_544" s="T594">dempro</ta>
            <ta e="T596" id="Seg_545" s="T595">dempro</ta>
            <ta e="T597" id="Seg_546" s="T596">v</ta>
            <ta e="T598" id="Seg_547" s="T597">num</ta>
            <ta e="T599" id="Seg_548" s="T598">que</ta>
            <ta e="T600" id="Seg_549" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_550" s="T600">v</ta>
            <ta e="T602" id="Seg_551" s="T601">conj</ta>
            <ta e="T603" id="Seg_552" s="T602">quant</ta>
            <ta e="T604" id="Seg_553" s="T603">aux</ta>
            <ta e="T605" id="Seg_554" s="T604">v</ta>
            <ta e="T606" id="Seg_555" s="T605">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T563" id="Seg_556" s="T562">np.h:S</ta>
            <ta e="T565" id="Seg_557" s="T564">v:pred</ta>
            <ta e="T567" id="Seg_558" s="T566">0.3.h:S v:pred</ta>
            <ta e="T569" id="Seg_559" s="T568">pro.h:S</ta>
            <ta e="T570" id="Seg_560" s="T569">pro.h:O</ta>
            <ta e="T572" id="Seg_561" s="T571">v:pred</ta>
            <ta e="T573" id="Seg_562" s="T572">pro.h:S</ta>
            <ta e="T576" id="Seg_563" s="T575">n:pred</ta>
            <ta e="T577" id="Seg_564" s="T576">cop</ta>
            <ta e="T579" id="Seg_565" s="T578">pro.h:S</ta>
            <ta e="T582" id="Seg_566" s="T581">v:pred</ta>
            <ta e="T583" id="Seg_567" s="T582">pro.h:O</ta>
            <ta e="T584" id="Seg_568" s="T583">pro.h:S</ta>
            <ta e="T586" id="Seg_569" s="T585">v:pred</ta>
            <ta e="T590" id="Seg_570" s="T589">0.1.h:S v:pred</ta>
            <ta e="T595" id="Seg_571" s="T594">pro.h:S</ta>
            <ta e="T597" id="Seg_572" s="T596">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T563" id="Seg_573" s="T562">np.h:A</ta>
            <ta e="T566" id="Seg_574" s="T565">np:G</ta>
            <ta e="T569" id="Seg_575" s="T568">pro.h:E</ta>
            <ta e="T570" id="Seg_576" s="T569">pro.h:Th</ta>
            <ta e="T573" id="Seg_577" s="T572">pro.h:Th</ta>
            <ta e="T576" id="Seg_578" s="T575">np:Th</ta>
            <ta e="T579" id="Seg_579" s="T578">pro.h:E</ta>
            <ta e="T583" id="Seg_580" s="T582">pro.h:Th</ta>
            <ta e="T584" id="Seg_581" s="T583">pro.h:A</ta>
            <ta e="T585" id="Seg_582" s="T584">np:G</ta>
            <ta e="T589" id="Seg_583" s="T588">np:L</ta>
            <ta e="T590" id="Seg_584" s="T589">0.1.h:A</ta>
            <ta e="T595" id="Seg_585" s="T594">pro.h:A</ta>
            <ta e="T596" id="Seg_586" s="T595">pro.h:R</ta>
            <ta e="T599" id="Seg_587" s="T598">pro:Th</ta>
            <ta e="T604" id="Seg_588" s="T603">0.2.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T561" id="Seg_589" s="T560">RUS:gram</ta>
            <ta e="T562" id="Seg_590" s="T561">RUS:cult</ta>
            <ta e="T578" id="Seg_591" s="T577">RUS:gram</ta>
            <ta e="T580" id="Seg_592" s="T579">RUS:cult</ta>
            <ta e="T588" id="Seg_593" s="T587">RUS:gram</ta>
            <ta e="T591" id="Seg_594" s="T590">RUS:gram</ta>
            <ta e="T594" id="Seg_595" s="T593">RUS:gram</ta>
            <ta e="T599" id="Seg_596" s="T598">RUS:gram(INDEF)</ta>
            <ta e="T600" id="Seg_597" s="T599">RUS:mod</ta>
            <ta e="T602" id="Seg_598" s="T601">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T568" id="Seg_599" s="T567">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T562" id="Seg_600" s="T559">Утка и лягушка.</ta>
            <ta e="T567" id="Seg_601" s="T562">Прилетела утка, села на воду.</ta>
            <ta e="T572" id="Seg_602" s="T567">«Почему ты меня не знаешь?</ta>
            <ta e="T577" id="Seg_603" s="T572">Я утка.</ta>
            <ta e="T583" id="Seg_604" s="T577">А ты, лягушка, меня не знаешь.</ta>
            <ta e="T587" id="Seg_605" s="T583">Я могу по воде ходить [=плавать].</ta>
            <ta e="T592" id="Seg_606" s="T587">И по земле могу, и сверху [по воздуху]».</ta>
            <ta e="T597" id="Seg_607" s="T592">А та ей отвечает:</ta>
            <ta e="T605" id="Seg_608" s="T597">«Надо работать с чем-нибудь одним, много не бери».</ta>
            <ta e="T606" id="Seg_609" s="T605">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T562" id="Seg_610" s="T559">Duck and frog.</ta>
            <ta e="T567" id="Seg_611" s="T562">A duck came flying, landed on the water.</ta>
            <ta e="T572" id="Seg_612" s="T567">"Why don't you know me?</ta>
            <ta e="T577" id="Seg_613" s="T572">I am a duck.</ta>
            <ta e="T583" id="Seg_614" s="T577">And you, frog, do not know me.</ta>
            <ta e="T587" id="Seg_615" s="T583">I can go on the water.</ta>
            <ta e="T592" id="Seg_616" s="T587">And I can [walk] on the ground and above [in the air]."</ta>
            <ta e="T597" id="Seg_617" s="T592">And [the frog] said to [the duck]:</ta>
            <ta e="T605" id="Seg_618" s="T597">"[You] should work with one thing, don't take many."</ta>
            <ta e="T606" id="Seg_619" s="T605">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T562" id="Seg_620" s="T559">Ente und Frosch.</ta>
            <ta e="T567" id="Seg_621" s="T562">Eine Ente kam geflogen, setzte auf das Wasser auf.</ta>
            <ta e="T572" id="Seg_622" s="T567">„Warum kennst du mich nicht?</ta>
            <ta e="T577" id="Seg_623" s="T572">Ich bin eine Ente.</ta>
            <ta e="T583" id="Seg_624" s="T577">Aber du, Frosch, kennst mich nicht.</ta>
            <ta e="T587" id="Seg_625" s="T583">Ich kann aufs Wasser gehen.</ta>
            <ta e="T592" id="Seg_626" s="T587">Und ich kann auf der Erde [gehen] und oberhalb [in der Luft].“</ta>
            <ta e="T597" id="Seg_627" s="T592">Und [der Frosch] sagte zu [der Ente]:</ta>
            <ta e="T605" id="Seg_628" s="T597">„[Du] solltest mit einer Sache arbeiten, nimm nicht vielen.“</ta>
            <ta e="T606" id="Seg_629" s="T605">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T562" id="Seg_630" s="T559">[GVY:] Lʼagušen is a strange form, maybe from a diminutive like lʼagušen'ka? Below is a normal Russian form lʼaguška.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
