<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1A6A0EE3-6E97-AB9E-0DB9-81458674ADDB">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SoldierSchool_flk.wav" />
         <referenced-file url="PKZ_196X_SoldierSchool_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SoldierSchool_flk\PKZ_196X_SoldierSchool_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">237</ud-information>
            <ud-information attribute-name="# HIAT:w">152</ud-information>
            <ud-information attribute-name="# e">152</ud-information>
            <ud-information attribute-name="# HIAT:u">39</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.006" type="appl" />
         <tli id="T1" time="0.745" type="appl" />
         <tli id="T2" time="1.484" type="appl" />
         <tli id="T3" time="2.566559400423493" />
         <tli id="T4" time="3.344" type="appl" />
         <tli id="T5" time="4.138" type="appl" />
         <tli id="T6" time="4.931" type="appl" />
         <tli id="T7" time="5.725" type="appl" />
         <tli id="T8" time="7.939668171180208" />
         <tli id="T9" time="8.93" type="appl" />
         <tli id="T10" time="9.508" type="appl" />
         <tli id="T11" time="10.086" type="appl" />
         <tli id="T12" time="10.664" type="appl" />
         <tli id="T13" time="11.041" type="appl" />
         <tli id="T14" time="11.419" type="appl" />
         <tli id="T15" time="11.796" type="appl" />
         <tli id="T16" time="12.173" type="appl" />
         <tli id="T17" time="12.551" type="appl" />
         <tli id="T18" time="12.928" type="appl" />
         <tli id="T19" time="13.525" type="appl" />
         <tli id="T20" time="14.122" type="appl" />
         <tli id="T21" time="14.719" type="appl" />
         <tli id="T22" time="15.511" type="appl" />
         <tli id="T23" time="16.153" type="appl" />
         <tli id="T24" time="17.00595592332553" />
         <tli id="T25" time="18.121" type="appl" />
         <tli id="T26" time="19.048" type="appl" />
         <tli id="T27" time="19.976" type="appl" />
         <tli id="T28" time="20.903" type="appl" />
         <tli id="T29" time="22.019079739217652" />
         <tli id="T30" time="22.764" type="appl" />
         <tli id="T31" time="23.286" type="appl" />
         <tli id="T32" time="23.807" type="appl" />
         <tli id="T33" time="24.329" type="appl" />
         <tli id="T34" time="24.85" type="appl" />
         <tli id="T35" time="25.372" type="appl" />
         <tli id="T36" time="26.407" type="appl" />
         <tli id="T37" time="27.442" type="appl" />
         <tli id="T38" time="28.457000778267965" />
         <tli id="T39" time="28.862" type="appl" />
         <tli id="T40" time="29.248" type="appl" />
         <tli id="T41" time="29.633" type="appl" />
         <tli id="T42" time="30.49872534269475" />
         <tli id="T43" time="31.022" type="appl" />
         <tli id="T44" time="31.552" type="appl" />
         <tli id="T45" time="32.73196534046584" />
         <tli id="T46" time="33.966" type="appl" />
         <tli id="T47" time="35.41185333779115" />
         <tli id="T48" time="36.36" type="appl" />
         <tli id="T49" time="37.124" type="appl" />
         <tli id="T50" time="37.888" type="appl" />
         <tli id="T51" time="38.652" type="appl" />
         <tli id="T52" time="39.416" type="appl" />
         <tli id="T152" time="39.839847709796054" type="intp" />
         <tli id="T53" time="40.40497798952413" />
         <tli id="T54" time="41.586" type="appl" />
         <tli id="T55" time="42.67" type="appl" />
         <tli id="T56" time="43.755" type="appl" />
         <tli id="T57" time="44.356" type="appl" />
         <tli id="T58" time="44.956" type="appl" />
         <tli id="T59" time="45.557" type="appl" />
         <tli id="T60" time="46.157" type="appl" />
         <tli id="T61" time="47.23802574389836" />
         <tli id="T62" time="48.3" type="appl" />
         <tli id="T63" time="48.996" type="appl" />
         <tli id="T64" time="49.693" type="appl" />
         <tli id="T65" time="50.389" type="appl" />
         <tli id="T66" time="51.245" type="appl" />
         <tli id="T67" time="52.101" type="appl" />
         <tli id="T68" time="53.191110275270255" />
         <tli id="T69" time="54.393" type="appl" />
         <tli id="T70" time="55.39" type="appl" />
         <tli id="T71" time="56.388" type="appl" />
         <tli id="T72" time="57.385" type="appl" />
         <tli id="T73" time="58.382" type="appl" />
         <tli id="T74" time="59.379" type="appl" />
         <tli id="T75" time="60.143" type="appl" />
         <tli id="T76" time="61.137444834503505" />
         <tli id="T77" time="62.214" type="appl" />
         <tli id="T78" time="63.016" type="appl" />
         <tli id="T79" time="63.818" type="appl" />
         <tli id="T80" time="64.785" type="appl" />
         <tli id="T81" time="65.752" type="appl" />
         <tli id="T82" time="66.719" type="appl" />
         <tli id="T83" time="67.282" type="appl" />
         <tli id="T84" time="67.845" type="appl" />
         <tli id="T85" time="68.409" type="appl" />
         <tli id="T86" time="68.972" type="appl" />
         <tli id="T87" time="69.557" type="appl" />
         <tli id="T88" time="70.072" type="appl" />
         <tli id="T89" time="70.57466760470213" />
         <tli id="T90" time="71.092" type="appl" />
         <tli id="T91" time="71.596" type="appl" />
         <tli id="T92" time="72.1" type="appl" />
         <tli id="T93" time="73.24360553883874" />
         <tli id="T94" time="74.119" type="appl" />
         <tli id="T95" time="74.818" type="appl" />
         <tli id="T96" time="75.518" type="appl" />
         <tli id="T97" time="76.217" type="appl" />
         <tli id="T151" time="76.54007692307692" type="intp" />
         <tli id="T98" time="76.917" type="appl" />
         <tli id="T99" time="77.616" type="appl" />
         <tli id="T100" time="78.69004457817898" />
         <tli id="T101" time="79.658" type="appl" />
         <tli id="T102" time="80.485" type="appl" />
         <tli id="T103" time="81.311" type="appl" />
         <tli id="T104" time="82.138" type="appl" />
         <tli id="T105" time="82.964" type="appl" />
         <tli id="T106" time="83.791" type="appl" />
         <tli id="T107" time="84.617" type="appl" />
         <tli id="T108" time="85.334" type="appl" />
         <tli id="T109" time="85.923" type="appl" />
         <tli id="T110" time="86.46" type="appl" />
         <tli id="T111" time="86.998" type="appl" />
         <tli id="T112" time="87.363" type="appl" />
         <tli id="T113" time="87.727" type="appl" />
         <tli id="T114" time="88.092" type="appl" />
         <tli id="T115" time="88.628" type="appl" />
         <tli id="T116" time="89.165" type="appl" />
         <tli id="T117" time="89.701" type="appl" />
         <tli id="T118" time="90.39622199933133" />
         <tli id="T119" time="91.553" type="appl" />
         <tli id="T120" time="92.5294661763067" />
         <tli id="T121" time="92.993" type="appl" />
         <tli id="T122" time="93.92940766744678" />
         <tli id="T123" time="94.964" type="appl" />
         <tli id="T124" time="95.892" type="appl" />
         <tli id="T125" time="97.26926808202384" />
         <tli id="T126" time="98.005" type="appl" />
         <tli id="T127" time="98.535" type="appl" />
         <tli id="T128" time="99.064" type="appl" />
         <tli id="T129" time="99.593" type="appl" />
         <tli id="T130" time="100.123" type="appl" />
         <tli id="T131" time="101.06244288420818" />
         <tli id="T132" time="103.568" type="appl" />
         <tli id="T133" time="106.092" type="appl" />
         <tli id="T134" time="108.617" type="appl" />
         <tli id="T135" time="111.141" type="appl" />
         <tli id="T136" time="111.752" type="appl" />
         <tli id="T137" time="112.364" type="appl" />
         <tli id="T138" time="112.975" type="appl" />
         <tli id="T139" time="113.646" type="appl" />
         <tli id="T140" time="114.47521564694082" />
         <tli id="T141" time="114.948" type="appl" />
         <tli id="T142" time="115.326" type="appl" />
         <tli id="T143" time="115.705" type="appl" />
         <tli id="T144" time="116.083" type="appl" />
         <tli id="T145" time="116.61512621196924" />
         <tli id="T146" time="116.939" type="appl" />
         <tli id="T147" time="117.417" type="appl" />
         <tli id="T148" time="117.896" type="appl" />
         <tli id="T149" time="118.374" type="appl" />
         <tli id="T150" time="119.635" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T150" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">soldat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nükenə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Măndə:</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">"</nts>
                  <nts id="Seg_19" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">M-</ts>
                  <nts id="Seg_22" n="HIAT:ip">)</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Miʔ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">tüšəlleʔbəbaʔ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">školagən</ts>
                  <nts id="Seg_32" n="HIAT:ip">"</nts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_36" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_38" n="HIAT:w" s="T8">Bar</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">ĭmbi</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">nörbəbi</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">dĭʔnə</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_51" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">A</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">dĭ</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">nüke</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">ĭmbidə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">ej</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">tĭmne</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Gijendə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">ej</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">ibi</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Ĭmbidə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">ej</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">kubi</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_96" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">Dĭgəttə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">măndə:</ts>
                  <nts id="Seg_102" n="HIAT:ip">"</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">Kăde</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">šiʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">tüšəllieleʔ</ts>
                  <nts id="Seg_112" n="HIAT:ip">?</nts>
                  <nts id="Seg_113" n="HIAT:ip">"</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_116" n="HIAT:u" s="T29">
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <nts id="Seg_118" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">Da</ts>
                  <nts id="Seg_121" n="HIAT:ip">)</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T30">tăŋ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">tüšəllieʔi</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">nʼe</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">tolʼkă</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">il</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_141" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_143" n="HIAT:w" s="T35">I</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_146" n="HIAT:w" s="T36">bugaʔi</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">tüšəllieʔi</ts>
                  <nts id="Seg_150" n="HIAT:ip">"</nts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_154" n="HIAT:u" s="T38">
                  <nts id="Seg_155" n="HIAT:ip">"</nts>
                  <ts e="T39" id="Seg_157" n="HIAT:w" s="T38">Nu</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_161" n="HIAT:w" s="T39">măn</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_164" n="HIAT:w" s="T40">buga</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_167" n="HIAT:w" s="T41">ige</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_171" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">Iʔ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">dĭ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_179" n="HIAT:w" s="T44">bugam</ts>
                  <nts id="Seg_180" n="HIAT:ip">!</nts>
                  <nts id="Seg_181" n="HIAT:ip">"</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_184" n="HIAT:u" s="T45">
                  <nts id="Seg_185" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_187" n="HIAT:w" s="T45">Nu</ts>
                  <nts id="Seg_188" n="HIAT:ip">,</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">kundə</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip">"</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_196" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_198" n="HIAT:w" s="T47">Dĭgəttə</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_201" n="HIAT:w" s="T48">dĭ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_204" n="HIAT:w" s="T49">ibi</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_208" n="HIAT:w" s="T50">kumbi</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_211" n="HIAT:w" s="T51">dibər</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_214" n="HIAT:w" s="T52">kazan</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_217" n="HIAT:w" s="T152">turanə</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_221" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_223" n="HIAT:w" s="T53">Dĭgəttə</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_226" n="HIAT:w" s="T54">soldatəʔi</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_229" n="HIAT:w" s="T55">mănliaʔi:</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">Deʔ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_236" n="HIAT:w" s="T57">aktʼa</ts>
                  <nts id="Seg_237" n="HIAT:ip">,</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">bădəsʼtə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">nada</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_246" n="HIAT:w" s="T60">dĭm</ts>
                  <nts id="Seg_247" n="HIAT:ip">"</nts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_251" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_253" n="HIAT:w" s="T61">Dĭ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_256" n="HIAT:w" s="T62">nüke</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">öʔlubi</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_262" n="HIAT:w" s="T64">bugagəʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_266" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_268" n="HIAT:w" s="T65">Bostə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_271" n="HIAT:w" s="T66">maːndə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_274" n="HIAT:w" s="T67">šobi</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_278" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_280" n="HIAT:w" s="T68">Onʼiʔ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_283" n="HIAT:w" s="T69">kö</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_286" n="HIAT:w" s="T70">dĭ</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_289" n="HIAT:w" s="T71">soldatəʔi</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_292" n="HIAT:w" s="T72">bugam</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_295" n="HIAT:w" s="T73">băʔpiʔi</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_299" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_301" n="HIAT:w" s="T74">Ujam</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_304" n="HIAT:w" s="T75">ambiʔi</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_308" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_310" n="HIAT:w" s="T76">Kubabə</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_313" n="HIAT:w" s="T77">sadarlaʔ</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_316" n="HIAT:w" s="T78">ibiʔi</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_320" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_322" n="HIAT:w" s="T79">Ara</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_325" n="HIAT:w" s="T80">ibiʔi</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_329" n="HIAT:w" s="T81">bĭʔpiʔi</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_333" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_335" n="HIAT:w" s="T82">Dĭgəttə</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_338" n="HIAT:w" s="T83">onʼiʔ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_341" n="HIAT:w" s="T84">kö</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_344" n="HIAT:w" s="T85">šobi</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_348" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_350" n="HIAT:w" s="T86">Dĭ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_353" n="HIAT:w" s="T87">nüke</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_356" n="HIAT:w" s="T88">šobi</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_360" n="HIAT:u" s="T89">
                  <nts id="Seg_361" n="HIAT:ip">"</nts>
                  <ts e="T90" id="Seg_363" n="HIAT:w" s="T89">Gijen</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_365" n="HIAT:ip">(</nts>
                  <ts e="T91" id="Seg_367" n="HIAT:w" s="T90">miʔ-</ts>
                  <nts id="Seg_368" n="HIAT:ip">)</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_371" n="HIAT:w" s="T91">măn</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_374" n="HIAT:w" s="T92">buga</ts>
                  <nts id="Seg_375" n="HIAT:ip">?</nts>
                  <nts id="Seg_376" n="HIAT:ip">"</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_379" n="HIAT:u" s="T93">
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <ts e="T94" id="Seg_382" n="HIAT:w" s="T93">O</ts>
                  <nts id="Seg_383" n="HIAT:ip">,</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_386" n="HIAT:w" s="T94">urgaja</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_390" n="HIAT:w" s="T95">tăn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_393" n="HIAT:w" s="T96">buga</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_396" n="HIAT:w" s="T97">kalla</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_399" n="HIAT:w" s="T151">dʼürbi</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_402" n="HIAT:w" s="T98">už</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_405" n="HIAT:w" s="T99">tüj</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_409" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_411" n="HIAT:w" s="T100">Maʔdə</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_414" n="HIAT:w" s="T101">von</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_418" n="HIAT:w" s="T102">pigəʔ</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_420" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_422" n="HIAT:w" s="T103">a-</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_425" n="HIAT:w" s="T104">anə-</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_428" n="HIAT:w" s="T105">aluʔ-</ts>
                  <nts id="Seg_429" n="HIAT:ip">)</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_432" n="HIAT:w" s="T106">abi</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_436" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_438" n="HIAT:w" s="T107">Dĭn</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">amnolaʔbə</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_445" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_447" n="HIAT:w" s="T109">Kanaʔ</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_450" n="HIAT:w" s="T110">dibər</ts>
                  <nts id="Seg_451" n="HIAT:ip">!</nts>
                  <nts id="Seg_452" n="HIAT:ip">"</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_455" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_457" n="HIAT:w" s="T111">Dĭ</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_460" n="HIAT:w" s="T112">dibər</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_462" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_464" n="HIAT:w" s="T113">kambi</ts>
                  <nts id="Seg_465" n="HIAT:ip">)</nts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_469" n="HIAT:u" s="T114">
                  <nts id="Seg_470" n="HIAT:ip">"</nts>
                  <ts e="T115" id="Seg_472" n="HIAT:w" s="T114">Dön</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_475" n="HIAT:w" s="T115">buga</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_478" n="HIAT:w" s="T116">amnolaʔbə</ts>
                  <nts id="Seg_479" n="HIAT:ip">?</nts>
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_483" n="HIAT:u" s="T117">
                  <nts id="Seg_484" n="HIAT:ip">"</nts>
                  <ts e="T118" id="Seg_486" n="HIAT:w" s="T117">Dön</ts>
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_491" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_493" n="HIAT:w" s="T118">Dĭm</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_496" n="HIAT:w" s="T119">öʔlüʔbiʔi</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_500" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_502" n="HIAT:w" s="T120">Dĭ</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_505" n="HIAT:w" s="T121">măndə:</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_507" n="HIAT:ip">"</nts>
                  <ts e="T123" id="Seg_509" n="HIAT:w" s="T122">Ugaːndə</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_512" n="HIAT:w" s="T123">kuvas</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_515" n="HIAT:w" s="T124">molaːmbial</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_519" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_521" n="HIAT:w" s="T125">Buga</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_524" n="HIAT:w" s="T126">ibiel</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_528" n="HIAT:w" s="T127">a</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_531" n="HIAT:w" s="T128">tüj</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_534" n="HIAT:w" s="T129">kuza</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_537" n="HIAT:w" s="T130">molaːmbial</ts>
                  <nts id="Seg_538" n="HIAT:ip">"</nts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_542" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_544" n="HIAT:w" s="T131">Xatʼel</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_547" n="HIAT:w" s="T132">dĭʔnə</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_550" n="HIAT:w" s="T133">baʔsittə</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_553" n="HIAT:w" s="T134">ulundə</ts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_557" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_559" n="HIAT:w" s="T135">Dĭ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_562" n="HIAT:w" s="T136">dĭm</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_565" n="HIAT:w" s="T137">sürerlüʔpi</ts>
                  <nts id="Seg_566" n="HIAT:ip">.</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_569" n="HIAT:u" s="T138">
                  <nts id="Seg_570" n="HIAT:ip">"</nts>
                  <ts e="T139" id="Seg_572" n="HIAT:w" s="T138">Kanaʔ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_575" n="HIAT:w" s="T139">döʔə</ts>
                  <nts id="Seg_576" n="HIAT:ip">!</nts>
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_580" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_582" n="HIAT:w" s="T140">I</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_585" n="HIAT:w" s="T141">dĭ</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_588" n="HIAT:w" s="T142">nüke</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_591" n="HIAT:w" s="T143">kambi</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_594" n="HIAT:w" s="T144">maːʔndə</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_598" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_600" n="HIAT:w" s="T145">I</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_602" n="HIAT:ip">(</nts>
                  <ts e="T147" id="Seg_604" n="HIAT:w" s="T146">düj-</ts>
                  <nts id="Seg_605" n="HIAT:ip">)</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_608" n="HIAT:w" s="T147">tüj</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_611" n="HIAT:w" s="T148">amnolaʔbə</ts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_615" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_617" n="HIAT:w" s="T149">Kabarləj</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T150" id="Seg_620" n="sc" s="T0">
               <ts e="T1" id="Seg_622" n="e" s="T0">Šobi </ts>
               <ts e="T2" id="Seg_624" n="e" s="T1">soldat </ts>
               <ts e="T3" id="Seg_626" n="e" s="T2">nükenə. </ts>
               <ts e="T4" id="Seg_628" n="e" s="T3">Măndə: </ts>
               <ts e="T5" id="Seg_630" n="e" s="T4">"(M-) </ts>
               <ts e="T6" id="Seg_632" n="e" s="T5">Miʔ </ts>
               <ts e="T7" id="Seg_634" n="e" s="T6">tüšəlleʔbəbaʔ </ts>
               <ts e="T8" id="Seg_636" n="e" s="T7">školagən". </ts>
               <ts e="T9" id="Seg_638" n="e" s="T8">Bar </ts>
               <ts e="T10" id="Seg_640" n="e" s="T9">ĭmbi </ts>
               <ts e="T11" id="Seg_642" n="e" s="T10">nörbəbi </ts>
               <ts e="T12" id="Seg_644" n="e" s="T11">dĭʔnə. </ts>
               <ts e="T13" id="Seg_646" n="e" s="T12">A </ts>
               <ts e="T14" id="Seg_648" n="e" s="T13">dĭ </ts>
               <ts e="T15" id="Seg_650" n="e" s="T14">nüke </ts>
               <ts e="T16" id="Seg_652" n="e" s="T15">ĭmbidə </ts>
               <ts e="T17" id="Seg_654" n="e" s="T16">ej </ts>
               <ts e="T18" id="Seg_656" n="e" s="T17">tĭmne. </ts>
               <ts e="T19" id="Seg_658" n="e" s="T18">Gijendə </ts>
               <ts e="T20" id="Seg_660" n="e" s="T19">ej </ts>
               <ts e="T21" id="Seg_662" n="e" s="T20">ibi. </ts>
               <ts e="T22" id="Seg_664" n="e" s="T21">Ĭmbidə </ts>
               <ts e="T23" id="Seg_666" n="e" s="T22">ej </ts>
               <ts e="T24" id="Seg_668" n="e" s="T23">kubi. </ts>
               <ts e="T25" id="Seg_670" n="e" s="T24">Dĭgəttə </ts>
               <ts e="T26" id="Seg_672" n="e" s="T25">măndə:" </ts>
               <ts e="T27" id="Seg_674" n="e" s="T26">Kăde </ts>
               <ts e="T28" id="Seg_676" n="e" s="T27">šiʔ </ts>
               <ts e="T29" id="Seg_678" n="e" s="T28">tüšəllieleʔ?" </ts>
               <ts e="T30" id="Seg_680" n="e" s="T29">"(Da) </ts>
               <ts e="T31" id="Seg_682" n="e" s="T30">tăŋ </ts>
               <ts e="T32" id="Seg_684" n="e" s="T31">tüšəllieʔi, </ts>
               <ts e="T33" id="Seg_686" n="e" s="T32">nʼe </ts>
               <ts e="T34" id="Seg_688" n="e" s="T33">tolʼkă </ts>
               <ts e="T35" id="Seg_690" n="e" s="T34">il. </ts>
               <ts e="T36" id="Seg_692" n="e" s="T35">I </ts>
               <ts e="T37" id="Seg_694" n="e" s="T36">bugaʔi </ts>
               <ts e="T38" id="Seg_696" n="e" s="T37">tüšəllieʔi". </ts>
               <ts e="T39" id="Seg_698" n="e" s="T38">"Nu, </ts>
               <ts e="T40" id="Seg_700" n="e" s="T39">măn </ts>
               <ts e="T41" id="Seg_702" n="e" s="T40">buga </ts>
               <ts e="T42" id="Seg_704" n="e" s="T41">ige. </ts>
               <ts e="T43" id="Seg_706" n="e" s="T42">Iʔ </ts>
               <ts e="T44" id="Seg_708" n="e" s="T43">dĭ </ts>
               <ts e="T45" id="Seg_710" n="e" s="T44">bugam!" </ts>
               <ts e="T46" id="Seg_712" n="e" s="T45">"Nu, </ts>
               <ts e="T47" id="Seg_714" n="e" s="T46">kundə." </ts>
               <ts e="T48" id="Seg_716" n="e" s="T47">Dĭgəttə </ts>
               <ts e="T49" id="Seg_718" n="e" s="T48">dĭ </ts>
               <ts e="T50" id="Seg_720" n="e" s="T49">ibi, </ts>
               <ts e="T51" id="Seg_722" n="e" s="T50">kumbi </ts>
               <ts e="T52" id="Seg_724" n="e" s="T51">dibər </ts>
               <ts e="T152" id="Seg_726" n="e" s="T52">kazan </ts>
               <ts e="T53" id="Seg_728" n="e" s="T152">turanə. </ts>
               <ts e="T54" id="Seg_730" n="e" s="T53">Dĭgəttə </ts>
               <ts e="T55" id="Seg_732" n="e" s="T54">soldatəʔi </ts>
               <ts e="T56" id="Seg_734" n="e" s="T55">mănliaʔi: </ts>
               <ts e="T57" id="Seg_736" n="e" s="T56">"Deʔ </ts>
               <ts e="T58" id="Seg_738" n="e" s="T57">aktʼa, </ts>
               <ts e="T59" id="Seg_740" n="e" s="T58">bădəsʼtə </ts>
               <ts e="T60" id="Seg_742" n="e" s="T59">nada </ts>
               <ts e="T61" id="Seg_744" n="e" s="T60">dĭm". </ts>
               <ts e="T62" id="Seg_746" n="e" s="T61">Dĭ </ts>
               <ts e="T63" id="Seg_748" n="e" s="T62">nüke </ts>
               <ts e="T64" id="Seg_750" n="e" s="T63">öʔlubi </ts>
               <ts e="T65" id="Seg_752" n="e" s="T64">bugagəʔ. </ts>
               <ts e="T66" id="Seg_754" n="e" s="T65">Bostə </ts>
               <ts e="T67" id="Seg_756" n="e" s="T66">maːndə </ts>
               <ts e="T68" id="Seg_758" n="e" s="T67">šobi. </ts>
               <ts e="T69" id="Seg_760" n="e" s="T68">Onʼiʔ </ts>
               <ts e="T70" id="Seg_762" n="e" s="T69">kö </ts>
               <ts e="T71" id="Seg_764" n="e" s="T70">dĭ </ts>
               <ts e="T72" id="Seg_766" n="e" s="T71">soldatəʔi </ts>
               <ts e="T73" id="Seg_768" n="e" s="T72">bugam </ts>
               <ts e="T74" id="Seg_770" n="e" s="T73">băʔpiʔi. </ts>
               <ts e="T75" id="Seg_772" n="e" s="T74">Ujam </ts>
               <ts e="T76" id="Seg_774" n="e" s="T75">ambiʔi. </ts>
               <ts e="T77" id="Seg_776" n="e" s="T76">Kubabə </ts>
               <ts e="T78" id="Seg_778" n="e" s="T77">sadarlaʔ </ts>
               <ts e="T79" id="Seg_780" n="e" s="T78">ibiʔi. </ts>
               <ts e="T80" id="Seg_782" n="e" s="T79">Ara </ts>
               <ts e="T81" id="Seg_784" n="e" s="T80">ibiʔi, </ts>
               <ts e="T82" id="Seg_786" n="e" s="T81">bĭʔpiʔi. </ts>
               <ts e="T83" id="Seg_788" n="e" s="T82">Dĭgəttə </ts>
               <ts e="T84" id="Seg_790" n="e" s="T83">onʼiʔ </ts>
               <ts e="T85" id="Seg_792" n="e" s="T84">kö </ts>
               <ts e="T86" id="Seg_794" n="e" s="T85">šobi. </ts>
               <ts e="T87" id="Seg_796" n="e" s="T86">Dĭ </ts>
               <ts e="T88" id="Seg_798" n="e" s="T87">nüke </ts>
               <ts e="T89" id="Seg_800" n="e" s="T88">šobi. </ts>
               <ts e="T90" id="Seg_802" n="e" s="T89">"Gijen </ts>
               <ts e="T91" id="Seg_804" n="e" s="T90">(miʔ-) </ts>
               <ts e="T92" id="Seg_806" n="e" s="T91">măn </ts>
               <ts e="T93" id="Seg_808" n="e" s="T92">buga?" </ts>
               <ts e="T94" id="Seg_810" n="e" s="T93">"O, </ts>
               <ts e="T95" id="Seg_812" n="e" s="T94">urgaja, </ts>
               <ts e="T96" id="Seg_814" n="e" s="T95">tăn </ts>
               <ts e="T97" id="Seg_816" n="e" s="T96">buga </ts>
               <ts e="T151" id="Seg_818" n="e" s="T97">kalla </ts>
               <ts e="T98" id="Seg_820" n="e" s="T151">dʼürbi </ts>
               <ts e="T99" id="Seg_822" n="e" s="T98">už </ts>
               <ts e="T100" id="Seg_824" n="e" s="T99">tüj. </ts>
               <ts e="T101" id="Seg_826" n="e" s="T100">Maʔdə </ts>
               <ts e="T102" id="Seg_828" n="e" s="T101">von, </ts>
               <ts e="T103" id="Seg_830" n="e" s="T102">pigəʔ </ts>
               <ts e="T104" id="Seg_832" n="e" s="T103">(a- </ts>
               <ts e="T105" id="Seg_834" n="e" s="T104">anə- </ts>
               <ts e="T106" id="Seg_836" n="e" s="T105">aluʔ-) </ts>
               <ts e="T107" id="Seg_838" n="e" s="T106">abi. </ts>
               <ts e="T108" id="Seg_840" n="e" s="T107">Dĭn </ts>
               <ts e="T109" id="Seg_842" n="e" s="T108">amnolaʔbə. </ts>
               <ts e="T110" id="Seg_844" n="e" s="T109">Kanaʔ </ts>
               <ts e="T111" id="Seg_846" n="e" s="T110">dibər!" </ts>
               <ts e="T112" id="Seg_848" n="e" s="T111">Dĭ </ts>
               <ts e="T113" id="Seg_850" n="e" s="T112">dibər </ts>
               <ts e="T114" id="Seg_852" n="e" s="T113">(kambi). </ts>
               <ts e="T115" id="Seg_854" n="e" s="T114">"Dön </ts>
               <ts e="T116" id="Seg_856" n="e" s="T115">buga </ts>
               <ts e="T117" id="Seg_858" n="e" s="T116">amnolaʔbə?" </ts>
               <ts e="T118" id="Seg_860" n="e" s="T117">"Dön". </ts>
               <ts e="T119" id="Seg_862" n="e" s="T118">Dĭm </ts>
               <ts e="T120" id="Seg_864" n="e" s="T119">öʔlüʔbiʔi. </ts>
               <ts e="T121" id="Seg_866" n="e" s="T120">Dĭ </ts>
               <ts e="T122" id="Seg_868" n="e" s="T121">măndə: </ts>
               <ts e="T123" id="Seg_870" n="e" s="T122">"Ugaːndə </ts>
               <ts e="T124" id="Seg_872" n="e" s="T123">kuvas </ts>
               <ts e="T125" id="Seg_874" n="e" s="T124">molaːmbial. </ts>
               <ts e="T126" id="Seg_876" n="e" s="T125">Buga </ts>
               <ts e="T127" id="Seg_878" n="e" s="T126">ibiel, </ts>
               <ts e="T128" id="Seg_880" n="e" s="T127">a </ts>
               <ts e="T129" id="Seg_882" n="e" s="T128">tüj </ts>
               <ts e="T130" id="Seg_884" n="e" s="T129">kuza </ts>
               <ts e="T131" id="Seg_886" n="e" s="T130">molaːmbial". </ts>
               <ts e="T132" id="Seg_888" n="e" s="T131">Xatʼel </ts>
               <ts e="T133" id="Seg_890" n="e" s="T132">dĭʔnə </ts>
               <ts e="T134" id="Seg_892" n="e" s="T133">baʔsittə </ts>
               <ts e="T135" id="Seg_894" n="e" s="T134">ulundə. </ts>
               <ts e="T136" id="Seg_896" n="e" s="T135">Dĭ </ts>
               <ts e="T137" id="Seg_898" n="e" s="T136">dĭm </ts>
               <ts e="T138" id="Seg_900" n="e" s="T137">sürerlüʔpi. </ts>
               <ts e="T139" id="Seg_902" n="e" s="T138">"Kanaʔ </ts>
               <ts e="T140" id="Seg_904" n="e" s="T139">döʔə!" </ts>
               <ts e="T141" id="Seg_906" n="e" s="T140">I </ts>
               <ts e="T142" id="Seg_908" n="e" s="T141">dĭ </ts>
               <ts e="T143" id="Seg_910" n="e" s="T142">nüke </ts>
               <ts e="T144" id="Seg_912" n="e" s="T143">kambi </ts>
               <ts e="T145" id="Seg_914" n="e" s="T144">maːʔndə. </ts>
               <ts e="T146" id="Seg_916" n="e" s="T145">I </ts>
               <ts e="T147" id="Seg_918" n="e" s="T146">(düj-) </ts>
               <ts e="T148" id="Seg_920" n="e" s="T147">tüj </ts>
               <ts e="T149" id="Seg_922" n="e" s="T148">amnolaʔbə. </ts>
               <ts e="T150" id="Seg_924" n="e" s="T149">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_925" s="T0">PKZ_196X_SoldierSchool_flk.001 (001)</ta>
            <ta e="T8" id="Seg_926" s="T3">PKZ_196X_SoldierSchool_flk.002 (002)</ta>
            <ta e="T12" id="Seg_927" s="T8">PKZ_196X_SoldierSchool_flk.003 (003)</ta>
            <ta e="T18" id="Seg_928" s="T12">PKZ_196X_SoldierSchool_flk.004 (004)</ta>
            <ta e="T21" id="Seg_929" s="T18">PKZ_196X_SoldierSchool_flk.005 (005)</ta>
            <ta e="T24" id="Seg_930" s="T21">PKZ_196X_SoldierSchool_flk.006 (006)</ta>
            <ta e="T29" id="Seg_931" s="T24">PKZ_196X_SoldierSchool_flk.007 (007)</ta>
            <ta e="T35" id="Seg_932" s="T29">PKZ_196X_SoldierSchool_flk.008 (008)</ta>
            <ta e="T38" id="Seg_933" s="T35">PKZ_196X_SoldierSchool_flk.009 (009)</ta>
            <ta e="T42" id="Seg_934" s="T38">PKZ_196X_SoldierSchool_flk.010 (010)</ta>
            <ta e="T45" id="Seg_935" s="T42">PKZ_196X_SoldierSchool_flk.011 (011)</ta>
            <ta e="T47" id="Seg_936" s="T45">PKZ_196X_SoldierSchool_flk.012 (012)</ta>
            <ta e="T53" id="Seg_937" s="T47">PKZ_196X_SoldierSchool_flk.013 (013)</ta>
            <ta e="T61" id="Seg_938" s="T53">PKZ_196X_SoldierSchool_flk.014 (014)</ta>
            <ta e="T65" id="Seg_939" s="T61">PKZ_196X_SoldierSchool_flk.015 (016)</ta>
            <ta e="T68" id="Seg_940" s="T65">PKZ_196X_SoldierSchool_flk.016 (017)</ta>
            <ta e="T74" id="Seg_941" s="T68">PKZ_196X_SoldierSchool_flk.017 (018)</ta>
            <ta e="T76" id="Seg_942" s="T74">PKZ_196X_SoldierSchool_flk.018 (019)</ta>
            <ta e="T79" id="Seg_943" s="T76">PKZ_196X_SoldierSchool_flk.019 (020)</ta>
            <ta e="T82" id="Seg_944" s="T79">PKZ_196X_SoldierSchool_flk.020 (021)</ta>
            <ta e="T86" id="Seg_945" s="T82">PKZ_196X_SoldierSchool_flk.021 (022)</ta>
            <ta e="T89" id="Seg_946" s="T86">PKZ_196X_SoldierSchool_flk.022 (023)</ta>
            <ta e="T93" id="Seg_947" s="T89">PKZ_196X_SoldierSchool_flk.023 (024)</ta>
            <ta e="T100" id="Seg_948" s="T93">PKZ_196X_SoldierSchool_flk.024 (025)</ta>
            <ta e="T107" id="Seg_949" s="T100">PKZ_196X_SoldierSchool_flk.025 (026)</ta>
            <ta e="T109" id="Seg_950" s="T107">PKZ_196X_SoldierSchool_flk.026 (027)</ta>
            <ta e="T111" id="Seg_951" s="T109">PKZ_196X_SoldierSchool_flk.027 (028)</ta>
            <ta e="T114" id="Seg_952" s="T111">PKZ_196X_SoldierSchool_flk.028 (029)</ta>
            <ta e="T117" id="Seg_953" s="T114">PKZ_196X_SoldierSchool_flk.029 (030)</ta>
            <ta e="T118" id="Seg_954" s="T117">PKZ_196X_SoldierSchool_flk.030 (031)</ta>
            <ta e="T120" id="Seg_955" s="T118">PKZ_196X_SoldierSchool_flk.031 (032)</ta>
            <ta e="T125" id="Seg_956" s="T120">PKZ_196X_SoldierSchool_flk.032 (033)</ta>
            <ta e="T131" id="Seg_957" s="T125">PKZ_196X_SoldierSchool_flk.033 (035)</ta>
            <ta e="T135" id="Seg_958" s="T131">PKZ_196X_SoldierSchool_flk.034 (036)</ta>
            <ta e="T138" id="Seg_959" s="T135">PKZ_196X_SoldierSchool_flk.035 (037)</ta>
            <ta e="T140" id="Seg_960" s="T138">PKZ_196X_SoldierSchool_flk.036 (038)</ta>
            <ta e="T145" id="Seg_961" s="T140">PKZ_196X_SoldierSchool_flk.037 (039)</ta>
            <ta e="T149" id="Seg_962" s="T145">PKZ_196X_SoldierSchool_flk.038 (040)</ta>
            <ta e="T150" id="Seg_963" s="T149">PKZ_196X_SoldierSchool_flk.039 (041)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_964" s="T0">Šobi soldat nükenə. </ta>
            <ta e="T8" id="Seg_965" s="T3">Măndə: "(M-) Miʔ tüšəlleʔbəbaʔ školagən". </ta>
            <ta e="T12" id="Seg_966" s="T8">Bar ĭmbi nörbəbi dĭʔnə. </ta>
            <ta e="T18" id="Seg_967" s="T12">A dĭ nüke ĭmbidə ej tĭmne. </ta>
            <ta e="T21" id="Seg_968" s="T18">Gijendə ej ibi. </ta>
            <ta e="T24" id="Seg_969" s="T21">Ĭmbidə ej kubi. </ta>
            <ta e="T29" id="Seg_970" s="T24">Dĭgəttə măndə:" Kăde šiʔ tüšəllieleʔ?" </ta>
            <ta e="T35" id="Seg_971" s="T29">"(Da) tăŋ tüšəllieʔi, nʼe tolʼkă il. </ta>
            <ta e="T38" id="Seg_972" s="T35">I bugaʔi tüšəllieʔi". </ta>
            <ta e="T42" id="Seg_973" s="T38">"Nu, măn buga ige. </ta>
            <ta e="T45" id="Seg_974" s="T42">Iʔ dĭ bugam!" </ta>
            <ta e="T47" id="Seg_975" s="T45">"Nu, kundə." </ta>
            <ta e="T53" id="Seg_976" s="T47">Dĭgəttə dĭ ibi, kumbi dibər kazan turanə. </ta>
            <ta e="T61" id="Seg_977" s="T53">Dĭgəttə soldatəʔi mănliaʔi: "Deʔ aktʼa, bădəsʼtə nada dĭm". </ta>
            <ta e="T65" id="Seg_978" s="T61">Dĭ nüke öʔlubi bugagəʔ. </ta>
            <ta e="T68" id="Seg_979" s="T65">Bostə maːndə šobi. </ta>
            <ta e="T74" id="Seg_980" s="T68">Onʼiʔ kö dĭ soldatəʔi bugam băʔpiʔi. </ta>
            <ta e="T76" id="Seg_981" s="T74">Ujam ambiʔi. </ta>
            <ta e="T79" id="Seg_982" s="T76">Kubabə sadarlaʔ ibiʔi. </ta>
            <ta e="T82" id="Seg_983" s="T79">Ara ibiʔi, bĭʔpiʔi. </ta>
            <ta e="T86" id="Seg_984" s="T82">Dĭgəttə onʼiʔ kö šobi. </ta>
            <ta e="T89" id="Seg_985" s="T86">Dĭ nüke šobi. </ta>
            <ta e="T93" id="Seg_986" s="T89">"Gijen (miʔ-) măn buga?" </ta>
            <ta e="T100" id="Seg_987" s="T93">"O, urgaja, tăn buga kalla dʼürbi už tüj. </ta>
            <ta e="T107" id="Seg_988" s="T100">Maʔdə von, pigəʔ (a- anə- aluʔ-) abi. </ta>
            <ta e="T109" id="Seg_989" s="T107">Dĭn amnolaʔbə. </ta>
            <ta e="T111" id="Seg_990" s="T109">Kanaʔ dibər!" </ta>
            <ta e="T114" id="Seg_991" s="T111">Dĭ dibər (kambi). </ta>
            <ta e="T117" id="Seg_992" s="T114">"Dön buga amnolaʔbə?" </ta>
            <ta e="T118" id="Seg_993" s="T117">"Dön". </ta>
            <ta e="T120" id="Seg_994" s="T118">Dĭm öʔlüʔbiʔi. </ta>
            <ta e="T125" id="Seg_995" s="T120">Dĭ măndə: "Ugaːndə kuvas molaːmbial. </ta>
            <ta e="T131" id="Seg_996" s="T125">Buga ibiel, a tüj kuza molaːmbial". </ta>
            <ta e="T135" id="Seg_997" s="T131">Xatʼel dĭʔnə baʔsittə ulundə. </ta>
            <ta e="T138" id="Seg_998" s="T135">Dĭ dĭm sürerlüʔpi. </ta>
            <ta e="T140" id="Seg_999" s="T138">"Kanaʔ döʔə!" </ta>
            <ta e="T145" id="Seg_1000" s="T140">I dĭ nüke kambi maːʔndə. </ta>
            <ta e="T149" id="Seg_1001" s="T145">I (düj-) tüj amnolaʔbə. </ta>
            <ta e="T150" id="Seg_1002" s="T149">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1003" s="T0">šo-bi</ta>
            <ta e="T2" id="Seg_1004" s="T1">soldat</ta>
            <ta e="T3" id="Seg_1005" s="T2">nüke-nə</ta>
            <ta e="T4" id="Seg_1006" s="T3">măn-də</ta>
            <ta e="T6" id="Seg_1007" s="T5">miʔ</ta>
            <ta e="T7" id="Seg_1008" s="T6">tüšəl-leʔbə-baʔ</ta>
            <ta e="T8" id="Seg_1009" s="T7">škola-gən</ta>
            <ta e="T9" id="Seg_1010" s="T8">bar</ta>
            <ta e="T10" id="Seg_1011" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_1012" s="T10">nörbə-bi</ta>
            <ta e="T12" id="Seg_1013" s="T11">dĭʔ-nə</ta>
            <ta e="T13" id="Seg_1014" s="T12">a</ta>
            <ta e="T14" id="Seg_1015" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_1016" s="T14">nüke</ta>
            <ta e="T16" id="Seg_1017" s="T15">ĭmbi=də</ta>
            <ta e="T17" id="Seg_1018" s="T16">ej</ta>
            <ta e="T18" id="Seg_1019" s="T17">tĭmne</ta>
            <ta e="T19" id="Seg_1020" s="T18">gijen=də</ta>
            <ta e="T20" id="Seg_1021" s="T19">ej</ta>
            <ta e="T21" id="Seg_1022" s="T20">i-bi</ta>
            <ta e="T22" id="Seg_1023" s="T21">ĭmbi=də</ta>
            <ta e="T23" id="Seg_1024" s="T22">ej</ta>
            <ta e="T24" id="Seg_1025" s="T23">ku-bi</ta>
            <ta e="T25" id="Seg_1026" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_1027" s="T25">măn-də</ta>
            <ta e="T27" id="Seg_1028" s="T26">kăde</ta>
            <ta e="T28" id="Seg_1029" s="T27">šiʔ</ta>
            <ta e="T29" id="Seg_1030" s="T28">tüšəl-lie-leʔ</ta>
            <ta e="T30" id="Seg_1031" s="T29">da</ta>
            <ta e="T31" id="Seg_1032" s="T30">tăŋ</ta>
            <ta e="T32" id="Seg_1033" s="T31">tüšə-l-lie-ʔi</ta>
            <ta e="T33" id="Seg_1034" s="T32">nʼe</ta>
            <ta e="T34" id="Seg_1035" s="T33">tolʼkă</ta>
            <ta e="T35" id="Seg_1036" s="T34">il</ta>
            <ta e="T36" id="Seg_1037" s="T35">i</ta>
            <ta e="T37" id="Seg_1038" s="T36">buga-ʔi</ta>
            <ta e="T38" id="Seg_1039" s="T37">tüšə-l-lie-ʔi</ta>
            <ta e="T39" id="Seg_1040" s="T38">nu</ta>
            <ta e="T40" id="Seg_1041" s="T39">măn</ta>
            <ta e="T41" id="Seg_1042" s="T40">buga</ta>
            <ta e="T42" id="Seg_1043" s="T41">i-ge</ta>
            <ta e="T43" id="Seg_1044" s="T42">i-ʔ</ta>
            <ta e="T44" id="Seg_1045" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_1046" s="T44">buga-m</ta>
            <ta e="T46" id="Seg_1047" s="T45">nu</ta>
            <ta e="T47" id="Seg_1048" s="T46">kun-də</ta>
            <ta e="T48" id="Seg_1049" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_1050" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1051" s="T49">i-bi</ta>
            <ta e="T51" id="Seg_1052" s="T50">kum-bi</ta>
            <ta e="T52" id="Seg_1053" s="T51">dibər</ta>
            <ta e="T152" id="Seg_1054" s="T52">Kazan</ta>
            <ta e="T53" id="Seg_1055" s="T152">tura-nə</ta>
            <ta e="T54" id="Seg_1056" s="T53">dĭgəttə</ta>
            <ta e="T55" id="Seg_1057" s="T54">soldat-əʔi</ta>
            <ta e="T56" id="Seg_1058" s="T55">măn-lia-ʔi</ta>
            <ta e="T57" id="Seg_1059" s="T56">de-ʔ</ta>
            <ta e="T58" id="Seg_1060" s="T57">aktʼa</ta>
            <ta e="T59" id="Seg_1061" s="T58">bădə-sʼtə</ta>
            <ta e="T60" id="Seg_1062" s="T59">nada</ta>
            <ta e="T61" id="Seg_1063" s="T60">dĭ-m</ta>
            <ta e="T62" id="Seg_1064" s="T61">dĭ</ta>
            <ta e="T63" id="Seg_1065" s="T62">nüke</ta>
            <ta e="T64" id="Seg_1066" s="T63">öʔlu-bi</ta>
            <ta e="T65" id="Seg_1067" s="T64">buga-gəʔ</ta>
            <ta e="T66" id="Seg_1068" s="T65">bos-tə</ta>
            <ta e="T67" id="Seg_1069" s="T66">ma-ndə</ta>
            <ta e="T68" id="Seg_1070" s="T67">šo-bi</ta>
            <ta e="T69" id="Seg_1071" s="T68">onʼiʔ</ta>
            <ta e="T70" id="Seg_1072" s="T69">kö</ta>
            <ta e="T71" id="Seg_1073" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_1074" s="T71">soldat-əʔi</ta>
            <ta e="T73" id="Seg_1075" s="T72">buga-m</ta>
            <ta e="T74" id="Seg_1076" s="T73">băʔ-pi-ʔi</ta>
            <ta e="T75" id="Seg_1077" s="T74">uja-m</ta>
            <ta e="T76" id="Seg_1078" s="T75">am-bi-ʔi</ta>
            <ta e="T77" id="Seg_1079" s="T76">kuba-bə</ta>
            <ta e="T78" id="Seg_1080" s="T77">sadar-laʔ</ta>
            <ta e="T79" id="Seg_1081" s="T78">i-bi-ʔi</ta>
            <ta e="T80" id="Seg_1082" s="T79">ara</ta>
            <ta e="T81" id="Seg_1083" s="T80">i-bi-ʔi</ta>
            <ta e="T82" id="Seg_1084" s="T81">bĭʔ-pi-ʔi</ta>
            <ta e="T83" id="Seg_1085" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1086" s="T83">onʼiʔ</ta>
            <ta e="T85" id="Seg_1087" s="T84">kö</ta>
            <ta e="T86" id="Seg_1088" s="T85">šo-bi</ta>
            <ta e="T87" id="Seg_1089" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1090" s="T87">nüke</ta>
            <ta e="T89" id="Seg_1091" s="T88">šo-bi</ta>
            <ta e="T90" id="Seg_1092" s="T89">gijen</ta>
            <ta e="T92" id="Seg_1093" s="T91">măn</ta>
            <ta e="T93" id="Seg_1094" s="T92">buga</ta>
            <ta e="T94" id="Seg_1095" s="T93">o</ta>
            <ta e="T95" id="Seg_1096" s="T94">urgaja</ta>
            <ta e="T96" id="Seg_1097" s="T95">tăn</ta>
            <ta e="T97" id="Seg_1098" s="T96">buga</ta>
            <ta e="T151" id="Seg_1099" s="T97">kal-la</ta>
            <ta e="T98" id="Seg_1100" s="T151">dʼür-bi</ta>
            <ta e="T99" id="Seg_1101" s="T98">už</ta>
            <ta e="T100" id="Seg_1102" s="T99">tüj</ta>
            <ta e="T101" id="Seg_1103" s="T100">maʔ-də</ta>
            <ta e="T102" id="Seg_1104" s="T101">von</ta>
            <ta e="T103" id="Seg_1105" s="T102">pi-gəʔ</ta>
            <ta e="T107" id="Seg_1106" s="T106">a-bi</ta>
            <ta e="T108" id="Seg_1107" s="T107">dĭn</ta>
            <ta e="T109" id="Seg_1108" s="T108">amno-laʔbə</ta>
            <ta e="T110" id="Seg_1109" s="T109">kan-a-ʔ</ta>
            <ta e="T111" id="Seg_1110" s="T110">dibər</ta>
            <ta e="T112" id="Seg_1111" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_1112" s="T112">dibər</ta>
            <ta e="T114" id="Seg_1113" s="T113">kam-bi</ta>
            <ta e="T115" id="Seg_1114" s="T114">dön</ta>
            <ta e="T116" id="Seg_1115" s="T115">buga</ta>
            <ta e="T117" id="Seg_1116" s="T116">amno-laʔbə</ta>
            <ta e="T118" id="Seg_1117" s="T117">dön</ta>
            <ta e="T119" id="Seg_1118" s="T118">dĭ-m</ta>
            <ta e="T120" id="Seg_1119" s="T119">öʔ-lüʔ-bi-ʔi</ta>
            <ta e="T121" id="Seg_1120" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_1121" s="T121">măn-də</ta>
            <ta e="T123" id="Seg_1122" s="T122">ugaːndə</ta>
            <ta e="T124" id="Seg_1123" s="T123">kuvas</ta>
            <ta e="T125" id="Seg_1124" s="T124">mo-laːm-bia-l</ta>
            <ta e="T126" id="Seg_1125" s="T125">buga</ta>
            <ta e="T127" id="Seg_1126" s="T126">i-bie-l</ta>
            <ta e="T128" id="Seg_1127" s="T127">a</ta>
            <ta e="T129" id="Seg_1128" s="T128">tüj</ta>
            <ta e="T130" id="Seg_1129" s="T129">kuza</ta>
            <ta e="T131" id="Seg_1130" s="T130">mo-laːm-bia-l</ta>
            <ta e="T132" id="Seg_1131" s="T131">xatʼel</ta>
            <ta e="T133" id="Seg_1132" s="T132">dĭʔ-nə</ta>
            <ta e="T134" id="Seg_1133" s="T133">baʔ-sittə</ta>
            <ta e="T135" id="Seg_1134" s="T134">ulu-ndə</ta>
            <ta e="T136" id="Seg_1135" s="T135">dĭ</ta>
            <ta e="T137" id="Seg_1136" s="T136">dĭ-m</ta>
            <ta e="T138" id="Seg_1137" s="T137">sürer-lüʔ-pi</ta>
            <ta e="T139" id="Seg_1138" s="T138">kan-a-ʔ</ta>
            <ta e="T140" id="Seg_1139" s="T139">döʔə</ta>
            <ta e="T141" id="Seg_1140" s="T140">i</ta>
            <ta e="T142" id="Seg_1141" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_1142" s="T142">nüke</ta>
            <ta e="T144" id="Seg_1143" s="T143">kam-bi</ta>
            <ta e="T145" id="Seg_1144" s="T144">maːʔ-ndə</ta>
            <ta e="T146" id="Seg_1145" s="T145">i</ta>
            <ta e="T148" id="Seg_1146" s="T147">tüj</ta>
            <ta e="T149" id="Seg_1147" s="T148">amno-laʔbə</ta>
            <ta e="T150" id="Seg_1148" s="T149">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1149" s="T0">šo-bi</ta>
            <ta e="T2" id="Seg_1150" s="T1">soldat</ta>
            <ta e="T3" id="Seg_1151" s="T2">nüke-Tə</ta>
            <ta e="T4" id="Seg_1152" s="T3">măn-ntə</ta>
            <ta e="T6" id="Seg_1153" s="T5">miʔ</ta>
            <ta e="T7" id="Seg_1154" s="T6">tüšəl-laʔbə-bAʔ</ta>
            <ta e="T8" id="Seg_1155" s="T7">škola-Kən</ta>
            <ta e="T9" id="Seg_1156" s="T8">bar</ta>
            <ta e="T10" id="Seg_1157" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_1158" s="T10">nörbə-bi</ta>
            <ta e="T12" id="Seg_1159" s="T11">dĭ-Tə</ta>
            <ta e="T13" id="Seg_1160" s="T12">a</ta>
            <ta e="T14" id="Seg_1161" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_1162" s="T14">nüke</ta>
            <ta e="T16" id="Seg_1163" s="T15">ĭmbi=də</ta>
            <ta e="T17" id="Seg_1164" s="T16">ej</ta>
            <ta e="T18" id="Seg_1165" s="T17">tĭmne</ta>
            <ta e="T19" id="Seg_1166" s="T18">gijen=də</ta>
            <ta e="T20" id="Seg_1167" s="T19">ej</ta>
            <ta e="T21" id="Seg_1168" s="T20">i-bi</ta>
            <ta e="T22" id="Seg_1169" s="T21">ĭmbi=də</ta>
            <ta e="T23" id="Seg_1170" s="T22">ej</ta>
            <ta e="T24" id="Seg_1171" s="T23">ku-bi</ta>
            <ta e="T25" id="Seg_1172" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_1173" s="T25">măn-ntə</ta>
            <ta e="T27" id="Seg_1174" s="T26">kădaʔ</ta>
            <ta e="T28" id="Seg_1175" s="T27">šiʔ</ta>
            <ta e="T29" id="Seg_1176" s="T28">tüšəl-liA-lAʔ</ta>
            <ta e="T30" id="Seg_1177" s="T29">da</ta>
            <ta e="T31" id="Seg_1178" s="T30">tăŋ</ta>
            <ta e="T32" id="Seg_1179" s="T31">tüšə-lə-liA-jəʔ</ta>
            <ta e="T33" id="Seg_1180" s="T32">nʼe</ta>
            <ta e="T34" id="Seg_1181" s="T33">tolʼko</ta>
            <ta e="T35" id="Seg_1182" s="T34">il</ta>
            <ta e="T36" id="Seg_1183" s="T35">i</ta>
            <ta e="T37" id="Seg_1184" s="T36">buga-jəʔ</ta>
            <ta e="T38" id="Seg_1185" s="T37">tüšə-lə-liA-jəʔ</ta>
            <ta e="T39" id="Seg_1186" s="T38">nu</ta>
            <ta e="T40" id="Seg_1187" s="T39">măn</ta>
            <ta e="T41" id="Seg_1188" s="T40">buga</ta>
            <ta e="T42" id="Seg_1189" s="T41">i-gA</ta>
            <ta e="T43" id="Seg_1190" s="T42">i-ʔ</ta>
            <ta e="T44" id="Seg_1191" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_1192" s="T44">buga-m</ta>
            <ta e="T46" id="Seg_1193" s="T45">nu</ta>
            <ta e="T47" id="Seg_1194" s="T46">kun-t</ta>
            <ta e="T48" id="Seg_1195" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_1196" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1197" s="T49">i-bi</ta>
            <ta e="T51" id="Seg_1198" s="T50">kun-bi</ta>
            <ta e="T52" id="Seg_1199" s="T51">dĭbər</ta>
            <ta e="T152" id="Seg_1200" s="T52">Kazan</ta>
            <ta e="T53" id="Seg_1201" s="T152">tura-Tə</ta>
            <ta e="T54" id="Seg_1202" s="T53">dĭgəttə</ta>
            <ta e="T55" id="Seg_1203" s="T54">soldat-jəʔ</ta>
            <ta e="T56" id="Seg_1204" s="T55">măn-liA-jəʔ</ta>
            <ta e="T57" id="Seg_1205" s="T56">det-ʔ</ta>
            <ta e="T58" id="Seg_1206" s="T57">aktʼa</ta>
            <ta e="T59" id="Seg_1207" s="T58">bădə-zittə</ta>
            <ta e="T60" id="Seg_1208" s="T59">nadə</ta>
            <ta e="T61" id="Seg_1209" s="T60">dĭ-m</ta>
            <ta e="T62" id="Seg_1210" s="T61">dĭ</ta>
            <ta e="T63" id="Seg_1211" s="T62">nüke</ta>
            <ta e="T64" id="Seg_1212" s="T63">öʔlu-bi</ta>
            <ta e="T65" id="Seg_1213" s="T64">buga-gəʔ</ta>
            <ta e="T66" id="Seg_1214" s="T65">bos-də</ta>
            <ta e="T67" id="Seg_1215" s="T66">maʔ-gəndə</ta>
            <ta e="T68" id="Seg_1216" s="T67">šo-bi</ta>
            <ta e="T69" id="Seg_1217" s="T68">onʼiʔ</ta>
            <ta e="T70" id="Seg_1218" s="T69">kö</ta>
            <ta e="T71" id="Seg_1219" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_1220" s="T71">soldat-jəʔ</ta>
            <ta e="T73" id="Seg_1221" s="T72">buga-m</ta>
            <ta e="T74" id="Seg_1222" s="T73">băt-bi-jəʔ</ta>
            <ta e="T75" id="Seg_1223" s="T74">uja-m</ta>
            <ta e="T76" id="Seg_1224" s="T75">am-bi-jəʔ</ta>
            <ta e="T77" id="Seg_1225" s="T76">kuba-bə</ta>
            <ta e="T78" id="Seg_1226" s="T77">sădar-lAʔ</ta>
            <ta e="T79" id="Seg_1227" s="T78">i-bi-jəʔ</ta>
            <ta e="T80" id="Seg_1228" s="T79">ara</ta>
            <ta e="T81" id="Seg_1229" s="T80">i-bi-jəʔ</ta>
            <ta e="T82" id="Seg_1230" s="T81">bĭs-bi-jəʔ</ta>
            <ta e="T83" id="Seg_1231" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1232" s="T83">onʼiʔ</ta>
            <ta e="T85" id="Seg_1233" s="T84">kö</ta>
            <ta e="T86" id="Seg_1234" s="T85">šo-bi</ta>
            <ta e="T87" id="Seg_1235" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1236" s="T87">nüke</ta>
            <ta e="T89" id="Seg_1237" s="T88">šo-bi</ta>
            <ta e="T90" id="Seg_1238" s="T89">gijen</ta>
            <ta e="T92" id="Seg_1239" s="T91">măn</ta>
            <ta e="T93" id="Seg_1240" s="T92">buga</ta>
            <ta e="T94" id="Seg_1241" s="T93">o</ta>
            <ta e="T95" id="Seg_1242" s="T94">urgaja</ta>
            <ta e="T96" id="Seg_1243" s="T95">tăn</ta>
            <ta e="T97" id="Seg_1244" s="T96">buga</ta>
            <ta e="T151" id="Seg_1245" s="T97">kan-lAʔ</ta>
            <ta e="T98" id="Seg_1246" s="T151">tʼür-bi</ta>
            <ta e="T99" id="Seg_1247" s="T98">už</ta>
            <ta e="T100" id="Seg_1248" s="T99">tüj</ta>
            <ta e="T101" id="Seg_1249" s="T100">maʔ-də</ta>
            <ta e="T102" id="Seg_1250" s="T101">von</ta>
            <ta e="T103" id="Seg_1251" s="T102">pi-gəʔ</ta>
            <ta e="T107" id="Seg_1252" s="T106">a-bi</ta>
            <ta e="T108" id="Seg_1253" s="T107">dĭn</ta>
            <ta e="T109" id="Seg_1254" s="T108">amno-laʔbə</ta>
            <ta e="T110" id="Seg_1255" s="T109">kan-ə-ʔ</ta>
            <ta e="T111" id="Seg_1256" s="T110">dĭbər</ta>
            <ta e="T112" id="Seg_1257" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_1258" s="T112">dĭbər</ta>
            <ta e="T114" id="Seg_1259" s="T113">kan-bi</ta>
            <ta e="T115" id="Seg_1260" s="T114">dön</ta>
            <ta e="T116" id="Seg_1261" s="T115">buga</ta>
            <ta e="T117" id="Seg_1262" s="T116">amno-laʔbə</ta>
            <ta e="T118" id="Seg_1263" s="T117">dön</ta>
            <ta e="T119" id="Seg_1264" s="T118">dĭ-m</ta>
            <ta e="T120" id="Seg_1265" s="T119">öʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T121" id="Seg_1266" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_1267" s="T121">măn-ntə</ta>
            <ta e="T123" id="Seg_1268" s="T122">ugaːndə</ta>
            <ta e="T124" id="Seg_1269" s="T123">kuvas</ta>
            <ta e="T125" id="Seg_1270" s="T124">mo-laːm-bi-l</ta>
            <ta e="T126" id="Seg_1271" s="T125">buga</ta>
            <ta e="T127" id="Seg_1272" s="T126">i-bi-l</ta>
            <ta e="T128" id="Seg_1273" s="T127">a</ta>
            <ta e="T129" id="Seg_1274" s="T128">tüj</ta>
            <ta e="T130" id="Seg_1275" s="T129">kuza</ta>
            <ta e="T131" id="Seg_1276" s="T130">mo-laːm-bi-l</ta>
            <ta e="T132" id="Seg_1277" s="T131">xatʼel</ta>
            <ta e="T133" id="Seg_1278" s="T132">dĭ-Tə</ta>
            <ta e="T134" id="Seg_1279" s="T133">baʔbdə-zittə</ta>
            <ta e="T135" id="Seg_1280" s="T134">ulu-gəndə</ta>
            <ta e="T136" id="Seg_1281" s="T135">dĭ</ta>
            <ta e="T137" id="Seg_1282" s="T136">dĭ-m</ta>
            <ta e="T138" id="Seg_1283" s="T137">sürer-luʔbdə-bi</ta>
            <ta e="T139" id="Seg_1284" s="T138">kan-ə-ʔ</ta>
            <ta e="T140" id="Seg_1285" s="T139">döʔə</ta>
            <ta e="T141" id="Seg_1286" s="T140">i</ta>
            <ta e="T142" id="Seg_1287" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_1288" s="T142">nüke</ta>
            <ta e="T144" id="Seg_1289" s="T143">kan-bi</ta>
            <ta e="T145" id="Seg_1290" s="T144">maʔ-gəndə</ta>
            <ta e="T146" id="Seg_1291" s="T145">i</ta>
            <ta e="T148" id="Seg_1292" s="T147">tüj</ta>
            <ta e="T149" id="Seg_1293" s="T148">amno-laʔbə</ta>
            <ta e="T150" id="Seg_1294" s="T149">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1295" s="T0">come-PST.[3SG]</ta>
            <ta e="T2" id="Seg_1296" s="T1">soldier.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1297" s="T2">woman-LAT</ta>
            <ta e="T4" id="Seg_1298" s="T3">say-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_1299" s="T5">we.NOM</ta>
            <ta e="T7" id="Seg_1300" s="T6">study-DUR-1PL</ta>
            <ta e="T8" id="Seg_1301" s="T7">school-LOC</ta>
            <ta e="T9" id="Seg_1302" s="T8">all</ta>
            <ta e="T10" id="Seg_1303" s="T9">what.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1304" s="T10">tell-PST.[3SG]</ta>
            <ta e="T12" id="Seg_1305" s="T11">this-LAT</ta>
            <ta e="T13" id="Seg_1306" s="T12">and</ta>
            <ta e="T14" id="Seg_1307" s="T13">this.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1308" s="T14">woman.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1309" s="T15">what.[NOM.SG]=INDEF</ta>
            <ta e="T17" id="Seg_1310" s="T16">NEG</ta>
            <ta e="T18" id="Seg_1311" s="T17">know.[3SG]</ta>
            <ta e="T19" id="Seg_1312" s="T18">where=INDEF</ta>
            <ta e="T20" id="Seg_1313" s="T19">NEG</ta>
            <ta e="T21" id="Seg_1314" s="T20">be-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1315" s="T21">what.[NOM.SG]=INDEF</ta>
            <ta e="T23" id="Seg_1316" s="T22">NEG</ta>
            <ta e="T24" id="Seg_1317" s="T23">see-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1318" s="T24">then</ta>
            <ta e="T26" id="Seg_1319" s="T25">say-IPFVZ.[3SG]</ta>
            <ta e="T27" id="Seg_1320" s="T26">how</ta>
            <ta e="T28" id="Seg_1321" s="T27">you.PL.NOM</ta>
            <ta e="T29" id="Seg_1322" s="T28">study-PRS-2PL</ta>
            <ta e="T30" id="Seg_1323" s="T29">and</ta>
            <ta e="T31" id="Seg_1324" s="T30">strongly</ta>
            <ta e="T32" id="Seg_1325" s="T31">learn-TR-PRS-3PL</ta>
            <ta e="T33" id="Seg_1326" s="T32">not</ta>
            <ta e="T34" id="Seg_1327" s="T33">only</ta>
            <ta e="T35" id="Seg_1328" s="T34">people.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1329" s="T35">and</ta>
            <ta e="T37" id="Seg_1330" s="T36">bull-PL</ta>
            <ta e="T38" id="Seg_1331" s="T37">learn-TR-PRS-3PL</ta>
            <ta e="T39" id="Seg_1332" s="T38">well</ta>
            <ta e="T40" id="Seg_1333" s="T39">I.NOM</ta>
            <ta e="T41" id="Seg_1334" s="T40">bull.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1335" s="T41">be-PRS.[3SG]</ta>
            <ta e="T43" id="Seg_1336" s="T42">take-IMP.2SG</ta>
            <ta e="T44" id="Seg_1337" s="T43">this.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1338" s="T44">bull-ACC</ta>
            <ta e="T46" id="Seg_1339" s="T45">well</ta>
            <ta e="T47" id="Seg_1340" s="T46">bring-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_1341" s="T47">then</ta>
            <ta e="T49" id="Seg_1342" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1343" s="T49">take-PST.[3SG]</ta>
            <ta e="T51" id="Seg_1344" s="T50">bring-PST.[3SG]</ta>
            <ta e="T52" id="Seg_1345" s="T51">there</ta>
            <ta e="T152" id="Seg_1346" s="T52">Aginskoe</ta>
            <ta e="T53" id="Seg_1347" s="T152">settlement-LAT</ta>
            <ta e="T54" id="Seg_1348" s="T53">then</ta>
            <ta e="T55" id="Seg_1349" s="T54">soldier-PL</ta>
            <ta e="T56" id="Seg_1350" s="T55">say-PRS-3PL</ta>
            <ta e="T57" id="Seg_1351" s="T56">bring-IMP.2SG</ta>
            <ta e="T58" id="Seg_1352" s="T57">money.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1353" s="T58">feed-INF.LAT</ta>
            <ta e="T60" id="Seg_1354" s="T59">one.should</ta>
            <ta e="T61" id="Seg_1355" s="T60">this-ACC</ta>
            <ta e="T62" id="Seg_1356" s="T61">this.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1357" s="T62">woman.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1358" s="T63">send-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1359" s="T64">bull-ABL</ta>
            <ta e="T66" id="Seg_1360" s="T65">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T67" id="Seg_1361" s="T66">tent-LAT/LOC.3SG</ta>
            <ta e="T68" id="Seg_1362" s="T67">come-PST.[3SG]</ta>
            <ta e="T69" id="Seg_1363" s="T68">one.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1364" s="T69">winter.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1365" s="T70">this.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1366" s="T71">soldier-PL</ta>
            <ta e="T73" id="Seg_1367" s="T72">bull-ACC</ta>
            <ta e="T74" id="Seg_1368" s="T73">cut-PST-3PL</ta>
            <ta e="T75" id="Seg_1369" s="T74">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_1370" s="T75">eat-PST-3PL</ta>
            <ta e="T77" id="Seg_1371" s="T76">skin-ACC.3SG</ta>
            <ta e="T78" id="Seg_1372" s="T77">sell-CVB</ta>
            <ta e="T79" id="Seg_1373" s="T78">take-PST-3PL</ta>
            <ta e="T80" id="Seg_1374" s="T79">vodka.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1375" s="T80">take-PST-3PL</ta>
            <ta e="T82" id="Seg_1376" s="T81">drink-PST-3PL</ta>
            <ta e="T83" id="Seg_1377" s="T82">then</ta>
            <ta e="T84" id="Seg_1378" s="T83">one.[NOM.SG]</ta>
            <ta e="T85" id="Seg_1379" s="T84">winter.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1380" s="T85">come-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1381" s="T86">this.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1382" s="T87">woman.[NOM.SG]</ta>
            <ta e="T89" id="Seg_1383" s="T88">come-PST.[3SG]</ta>
            <ta e="T90" id="Seg_1384" s="T89">where</ta>
            <ta e="T92" id="Seg_1385" s="T91">I.NOM</ta>
            <ta e="T93" id="Seg_1386" s="T92">bull.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1387" s="T93">oh</ta>
            <ta e="T95" id="Seg_1388" s="T94">grandmother.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1389" s="T95">you.NOM</ta>
            <ta e="T97" id="Seg_1390" s="T96">bull.[NOM.SG]</ta>
            <ta e="T151" id="Seg_1391" s="T97">go-CVB</ta>
            <ta e="T98" id="Seg_1392" s="T151">disappear-PST.[3SG]</ta>
            <ta e="T99" id="Seg_1393" s="T98">PTCL</ta>
            <ta e="T100" id="Seg_1394" s="T99">now</ta>
            <ta e="T101" id="Seg_1395" s="T100">tent-NOM/GEN/ACC.3SG</ta>
            <ta e="T102" id="Seg_1396" s="T101">there</ta>
            <ta e="T103" id="Seg_1397" s="T102">stone-ABL</ta>
            <ta e="T107" id="Seg_1398" s="T106">make-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1399" s="T107">there</ta>
            <ta e="T109" id="Seg_1400" s="T108">live-DUR.[3SG]</ta>
            <ta e="T110" id="Seg_1401" s="T109">go-EP-IMP.2SG</ta>
            <ta e="T111" id="Seg_1402" s="T110">there</ta>
            <ta e="T112" id="Seg_1403" s="T111">this.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1404" s="T112">there</ta>
            <ta e="T114" id="Seg_1405" s="T113">go-PST.[3SG]</ta>
            <ta e="T115" id="Seg_1406" s="T114">here</ta>
            <ta e="T116" id="Seg_1407" s="T115">bull.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1408" s="T116">live-DUR.[3SG]</ta>
            <ta e="T118" id="Seg_1409" s="T117">here</ta>
            <ta e="T119" id="Seg_1410" s="T118">this-ACC</ta>
            <ta e="T120" id="Seg_1411" s="T119">send-MOM-PST-3PL</ta>
            <ta e="T121" id="Seg_1412" s="T120">this.[NOM.SG]</ta>
            <ta e="T122" id="Seg_1413" s="T121">say-IPFVZ.[3SG]</ta>
            <ta e="T123" id="Seg_1414" s="T122">very</ta>
            <ta e="T124" id="Seg_1415" s="T123">beautiful.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1416" s="T124">become-RES-PST-2SG</ta>
            <ta e="T126" id="Seg_1417" s="T125">bull.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1418" s="T126">be-PST-2SG</ta>
            <ta e="T128" id="Seg_1419" s="T127">and</ta>
            <ta e="T129" id="Seg_1420" s="T128">now</ta>
            <ta e="T130" id="Seg_1421" s="T129">man.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1422" s="T130">become-RES-PST-2SG</ta>
            <ta e="T132" id="Seg_1423" s="T131">want.PST.M.SG</ta>
            <ta e="T133" id="Seg_1424" s="T132">this-LAT</ta>
            <ta e="T134" id="Seg_1425" s="T133">throw-INF.LAT</ta>
            <ta e="T135" id="Seg_1426" s="T134">head-LAT/LOC.3SG</ta>
            <ta e="T136" id="Seg_1427" s="T135">this.[NOM.SG]</ta>
            <ta e="T137" id="Seg_1428" s="T136">this-ACC</ta>
            <ta e="T138" id="Seg_1429" s="T137">drive-MOM-PST.[3SG]</ta>
            <ta e="T139" id="Seg_1430" s="T138">go-EP-IMP.2SG</ta>
            <ta e="T140" id="Seg_1431" s="T139">hence</ta>
            <ta e="T141" id="Seg_1432" s="T140">and</ta>
            <ta e="T142" id="Seg_1433" s="T141">this.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1434" s="T142">woman.[NOM.SG]</ta>
            <ta e="T144" id="Seg_1435" s="T143">go-PST.[3SG]</ta>
            <ta e="T145" id="Seg_1436" s="T144">tent-LAT/LOC.3SG</ta>
            <ta e="T146" id="Seg_1437" s="T145">and</ta>
            <ta e="T148" id="Seg_1438" s="T147">now</ta>
            <ta e="T149" id="Seg_1439" s="T148">live-DUR.[3SG]</ta>
            <ta e="T150" id="Seg_1440" s="T149">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1441" s="T0">прийти-PST.[3SG]</ta>
            <ta e="T2" id="Seg_1442" s="T1">солдат.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1443" s="T2">женщина-LAT</ta>
            <ta e="T4" id="Seg_1444" s="T3">сказать-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_1445" s="T5">мы.NOM</ta>
            <ta e="T7" id="Seg_1446" s="T6">учиться-DUR-1PL</ta>
            <ta e="T8" id="Seg_1447" s="T7">школа-LOC</ta>
            <ta e="T9" id="Seg_1448" s="T8">весь</ta>
            <ta e="T10" id="Seg_1449" s="T9">что.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1450" s="T10">сказать-PST.[3SG]</ta>
            <ta e="T12" id="Seg_1451" s="T11">этот-LAT</ta>
            <ta e="T13" id="Seg_1452" s="T12">а</ta>
            <ta e="T14" id="Seg_1453" s="T13">этот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1454" s="T14">женщина.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1455" s="T15">что.[NOM.SG]=INDEF</ta>
            <ta e="T17" id="Seg_1456" s="T16">NEG</ta>
            <ta e="T18" id="Seg_1457" s="T17">знать.[3SG]</ta>
            <ta e="T19" id="Seg_1458" s="T18">где=INDEF</ta>
            <ta e="T20" id="Seg_1459" s="T19">NEG</ta>
            <ta e="T21" id="Seg_1460" s="T20">быть-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1461" s="T21">что.[NOM.SG]=INDEF</ta>
            <ta e="T23" id="Seg_1462" s="T22">NEG</ta>
            <ta e="T24" id="Seg_1463" s="T23">видеть-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1464" s="T24">тогда</ta>
            <ta e="T26" id="Seg_1465" s="T25">сказать-IPFVZ.[3SG]</ta>
            <ta e="T27" id="Seg_1466" s="T26">как</ta>
            <ta e="T28" id="Seg_1467" s="T27">вы.NOM</ta>
            <ta e="T29" id="Seg_1468" s="T28">учиться-PRS-2PL</ta>
            <ta e="T30" id="Seg_1469" s="T29">и</ta>
            <ta e="T31" id="Seg_1470" s="T30">сильно</ta>
            <ta e="T32" id="Seg_1471" s="T31">научиться-TR-PRS-3PL</ta>
            <ta e="T33" id="Seg_1472" s="T32">не</ta>
            <ta e="T34" id="Seg_1473" s="T33">только</ta>
            <ta e="T35" id="Seg_1474" s="T34">люди.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1475" s="T35">и</ta>
            <ta e="T37" id="Seg_1476" s="T36">бык-PL</ta>
            <ta e="T38" id="Seg_1477" s="T37">научиться-TR-PRS-3PL</ta>
            <ta e="T39" id="Seg_1478" s="T38">ну</ta>
            <ta e="T40" id="Seg_1479" s="T39">я.NOM</ta>
            <ta e="T41" id="Seg_1480" s="T40">бык.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1481" s="T41">быть-PRS.[3SG]</ta>
            <ta e="T43" id="Seg_1482" s="T42">взять-IMP.2SG</ta>
            <ta e="T44" id="Seg_1483" s="T43">этот.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1484" s="T44">бык-ACC</ta>
            <ta e="T46" id="Seg_1485" s="T45">ну</ta>
            <ta e="T47" id="Seg_1486" s="T46">нести-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_1487" s="T47">тогда</ta>
            <ta e="T49" id="Seg_1488" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1489" s="T49">взять-PST.[3SG]</ta>
            <ta e="T51" id="Seg_1490" s="T50">нести-PST.[3SG]</ta>
            <ta e="T52" id="Seg_1491" s="T51">там</ta>
            <ta e="T152" id="Seg_1492" s="T52">Агинское</ta>
            <ta e="T53" id="Seg_1493" s="T152">поселение-LAT</ta>
            <ta e="T54" id="Seg_1494" s="T53">тогда</ta>
            <ta e="T55" id="Seg_1495" s="T54">солдат-PL</ta>
            <ta e="T56" id="Seg_1496" s="T55">сказать-PRS-3PL</ta>
            <ta e="T57" id="Seg_1497" s="T56">принести-IMP.2SG</ta>
            <ta e="T58" id="Seg_1498" s="T57">деньги.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1499" s="T58">кормить-INF.LAT</ta>
            <ta e="T60" id="Seg_1500" s="T59">надо</ta>
            <ta e="T61" id="Seg_1501" s="T60">этот-ACC</ta>
            <ta e="T62" id="Seg_1502" s="T61">этот.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1503" s="T62">женщина.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1504" s="T63">посылать-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1505" s="T64">бык-ABL</ta>
            <ta e="T66" id="Seg_1506" s="T65">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T67" id="Seg_1507" s="T66">чум-LAT/LOC.3SG</ta>
            <ta e="T68" id="Seg_1508" s="T67">прийти-PST.[3SG]</ta>
            <ta e="T69" id="Seg_1509" s="T68">один.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1510" s="T69">зима.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1511" s="T70">этот.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1512" s="T71">солдат-PL</ta>
            <ta e="T73" id="Seg_1513" s="T72">бык-ACC</ta>
            <ta e="T74" id="Seg_1514" s="T73">резать-PST-3PL</ta>
            <ta e="T75" id="Seg_1515" s="T74">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_1516" s="T75">съесть-PST-3PL</ta>
            <ta e="T77" id="Seg_1517" s="T76">кожа-ACC.3SG</ta>
            <ta e="T78" id="Seg_1518" s="T77">продавать-CVB</ta>
            <ta e="T79" id="Seg_1519" s="T78">взять-PST-3PL</ta>
            <ta e="T80" id="Seg_1520" s="T79">водка.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1521" s="T80">взять-PST-3PL</ta>
            <ta e="T82" id="Seg_1522" s="T81">пить-PST-3PL</ta>
            <ta e="T83" id="Seg_1523" s="T82">тогда</ta>
            <ta e="T84" id="Seg_1524" s="T83">один.[NOM.SG]</ta>
            <ta e="T85" id="Seg_1525" s="T84">зима.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1526" s="T85">прийти-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1527" s="T86">этот.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1528" s="T87">женщина.[NOM.SG]</ta>
            <ta e="T89" id="Seg_1529" s="T88">прийти-PST.[3SG]</ta>
            <ta e="T90" id="Seg_1530" s="T89">где</ta>
            <ta e="T92" id="Seg_1531" s="T91">я.NOM</ta>
            <ta e="T93" id="Seg_1532" s="T92">бык.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1533" s="T93">о</ta>
            <ta e="T95" id="Seg_1534" s="T94">бабушка.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1535" s="T95">ты.NOM</ta>
            <ta e="T97" id="Seg_1536" s="T96">бык.[NOM.SG]</ta>
            <ta e="T151" id="Seg_1537" s="T97">пойти-CVB</ta>
            <ta e="T98" id="Seg_1538" s="T151">исчезнуть-PST.[3SG]</ta>
            <ta e="T99" id="Seg_1539" s="T98">PTCL</ta>
            <ta e="T100" id="Seg_1540" s="T99">сейчас</ta>
            <ta e="T101" id="Seg_1541" s="T100">чум-NOM/GEN/ACC.3SG</ta>
            <ta e="T102" id="Seg_1542" s="T101">вон</ta>
            <ta e="T103" id="Seg_1543" s="T102">камень-ABL</ta>
            <ta e="T107" id="Seg_1544" s="T106">делать-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1545" s="T107">там</ta>
            <ta e="T109" id="Seg_1546" s="T108">жить-DUR.[3SG]</ta>
            <ta e="T110" id="Seg_1547" s="T109">пойти-EP-IMP.2SG</ta>
            <ta e="T111" id="Seg_1548" s="T110">там</ta>
            <ta e="T112" id="Seg_1549" s="T111">этот.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1550" s="T112">там</ta>
            <ta e="T114" id="Seg_1551" s="T113">пойти-PST.[3SG]</ta>
            <ta e="T115" id="Seg_1552" s="T114">здесь</ta>
            <ta e="T116" id="Seg_1553" s="T115">бык.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1554" s="T116">жить-DUR.[3SG]</ta>
            <ta e="T118" id="Seg_1555" s="T117">здесь</ta>
            <ta e="T119" id="Seg_1556" s="T118">этот-ACC</ta>
            <ta e="T120" id="Seg_1557" s="T119">послать-MOM-PST-3PL</ta>
            <ta e="T121" id="Seg_1558" s="T120">этот.[NOM.SG]</ta>
            <ta e="T122" id="Seg_1559" s="T121">сказать-IPFVZ.[3SG]</ta>
            <ta e="T123" id="Seg_1560" s="T122">очень</ta>
            <ta e="T124" id="Seg_1561" s="T123">красивый.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1562" s="T124">стать-RES-PST-2SG</ta>
            <ta e="T126" id="Seg_1563" s="T125">бык.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1564" s="T126">быть-PST-2SG</ta>
            <ta e="T128" id="Seg_1565" s="T127">а</ta>
            <ta e="T129" id="Seg_1566" s="T128">сейчас</ta>
            <ta e="T130" id="Seg_1567" s="T129">мужчина.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1568" s="T130">стать-RES-PST-2SG</ta>
            <ta e="T132" id="Seg_1569" s="T131">хотеть.PST.M.SG</ta>
            <ta e="T133" id="Seg_1570" s="T132">этот-LAT</ta>
            <ta e="T134" id="Seg_1571" s="T133">бросить-INF.LAT</ta>
            <ta e="T135" id="Seg_1572" s="T134">голова-LAT/LOC.3SG</ta>
            <ta e="T136" id="Seg_1573" s="T135">этот.[NOM.SG]</ta>
            <ta e="T137" id="Seg_1574" s="T136">этот-ACC</ta>
            <ta e="T138" id="Seg_1575" s="T137">гнать-MOM-PST.[3SG]</ta>
            <ta e="T139" id="Seg_1576" s="T138">пойти-EP-IMP.2SG</ta>
            <ta e="T140" id="Seg_1577" s="T139">отсюда</ta>
            <ta e="T141" id="Seg_1578" s="T140">и</ta>
            <ta e="T142" id="Seg_1579" s="T141">этот.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1580" s="T142">женщина.[NOM.SG]</ta>
            <ta e="T144" id="Seg_1581" s="T143">пойти-PST.[3SG]</ta>
            <ta e="T145" id="Seg_1582" s="T144">чум-LAT/LOC.3SG</ta>
            <ta e="T146" id="Seg_1583" s="T145">и</ta>
            <ta e="T148" id="Seg_1584" s="T147">сейчас</ta>
            <ta e="T149" id="Seg_1585" s="T148">жить-DUR.[3SG]</ta>
            <ta e="T150" id="Seg_1586" s="T149">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1587" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_1588" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1589" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1590" s="T3">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_1591" s="T5">pers</ta>
            <ta e="T7" id="Seg_1592" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_1593" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1594" s="T8">quant</ta>
            <ta e="T10" id="Seg_1595" s="T9">que-n:case</ta>
            <ta e="T11" id="Seg_1596" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1597" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_1598" s="T12">conj</ta>
            <ta e="T14" id="Seg_1599" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_1600" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1601" s="T15">que-n:case=ptcl</ta>
            <ta e="T17" id="Seg_1602" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1603" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_1604" s="T18">que=ptcl</ta>
            <ta e="T20" id="Seg_1605" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_1606" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1607" s="T21">que-n:case=ptcl</ta>
            <ta e="T23" id="Seg_1608" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1609" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1610" s="T24">adv</ta>
            <ta e="T26" id="Seg_1611" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_1612" s="T26">que</ta>
            <ta e="T28" id="Seg_1613" s="T27">pers</ta>
            <ta e="T29" id="Seg_1614" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1615" s="T29">conj</ta>
            <ta e="T31" id="Seg_1616" s="T30">adv</ta>
            <ta e="T32" id="Seg_1617" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1618" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_1619" s="T33">adv</ta>
            <ta e="T35" id="Seg_1620" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1621" s="T35">conj</ta>
            <ta e="T37" id="Seg_1622" s="T36">n-n:num</ta>
            <ta e="T38" id="Seg_1623" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_1624" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1625" s="T39">pers</ta>
            <ta e="T41" id="Seg_1626" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1627" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_1628" s="T42">v-v:mood.pn</ta>
            <ta e="T44" id="Seg_1629" s="T43">dempro-n:case</ta>
            <ta e="T45" id="Seg_1630" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1631" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1632" s="T46">v-v:mood.pn</ta>
            <ta e="T48" id="Seg_1633" s="T47">adv</ta>
            <ta e="T49" id="Seg_1634" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_1635" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1636" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1637" s="T51">adv</ta>
            <ta e="T152" id="Seg_1638" s="T52">propr</ta>
            <ta e="T53" id="Seg_1639" s="T152">n-n:case</ta>
            <ta e="T54" id="Seg_1640" s="T53">adv</ta>
            <ta e="T55" id="Seg_1641" s="T54">n-n:num</ta>
            <ta e="T56" id="Seg_1642" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1643" s="T56">v-v:mood.pn</ta>
            <ta e="T58" id="Seg_1644" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1645" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_1646" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_1647" s="T60">dempro-n:case</ta>
            <ta e="T62" id="Seg_1648" s="T61">dempro-n:case</ta>
            <ta e="T63" id="Seg_1649" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1650" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1651" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1652" s="T65">refl-n:case.poss</ta>
            <ta e="T67" id="Seg_1653" s="T66">n-n:case.poss</ta>
            <ta e="T68" id="Seg_1654" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1655" s="T68">num-n:case</ta>
            <ta e="T70" id="Seg_1656" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1657" s="T70">dempro-n:case</ta>
            <ta e="T72" id="Seg_1658" s="T71">n-n:num</ta>
            <ta e="T73" id="Seg_1659" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1660" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1661" s="T74">n-n:case.poss</ta>
            <ta e="T76" id="Seg_1662" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1663" s="T76">n-n:case.poss</ta>
            <ta e="T78" id="Seg_1664" s="T77">v-v:n.fin</ta>
            <ta e="T79" id="Seg_1665" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_1666" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1667" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1668" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1669" s="T82">adv</ta>
            <ta e="T84" id="Seg_1670" s="T83">num-n:case</ta>
            <ta e="T85" id="Seg_1671" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1672" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1673" s="T86">dempro-n:case</ta>
            <ta e="T88" id="Seg_1674" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1675" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1676" s="T89">que</ta>
            <ta e="T92" id="Seg_1677" s="T91">pers</ta>
            <ta e="T93" id="Seg_1678" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1679" s="T93">interj</ta>
            <ta e="T95" id="Seg_1680" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1681" s="T95">pers</ta>
            <ta e="T97" id="Seg_1682" s="T96">n-n:case</ta>
            <ta e="T151" id="Seg_1683" s="T97">v-v:n-fin</ta>
            <ta e="T98" id="Seg_1684" s="T151">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1685" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1686" s="T99">adv</ta>
            <ta e="T101" id="Seg_1687" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_1688" s="T101">adv</ta>
            <ta e="T103" id="Seg_1689" s="T102">n-n:case</ta>
            <ta e="T107" id="Seg_1690" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1691" s="T107">adv</ta>
            <ta e="T109" id="Seg_1692" s="T108">v-v&gt;v-v:pn</ta>
            <ta e="T110" id="Seg_1693" s="T109">v-v:ins-v:mood.pn</ta>
            <ta e="T111" id="Seg_1694" s="T110">adv</ta>
            <ta e="T112" id="Seg_1695" s="T111">dempro-n:case</ta>
            <ta e="T113" id="Seg_1696" s="T112">adv</ta>
            <ta e="T114" id="Seg_1697" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1698" s="T114">adv</ta>
            <ta e="T116" id="Seg_1699" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_1700" s="T116">v-v&gt;v-v:pn</ta>
            <ta e="T118" id="Seg_1701" s="T117">adv</ta>
            <ta e="T119" id="Seg_1702" s="T118">dempro-n:case</ta>
            <ta e="T120" id="Seg_1703" s="T119">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1704" s="T120">dempro-n:case</ta>
            <ta e="T122" id="Seg_1705" s="T121">v-v&gt;v-v:pn</ta>
            <ta e="T123" id="Seg_1706" s="T122">adv</ta>
            <ta e="T124" id="Seg_1707" s="T123">adj-n:case</ta>
            <ta e="T125" id="Seg_1708" s="T124">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1709" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_1710" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_1711" s="T127">conj</ta>
            <ta e="T129" id="Seg_1712" s="T128">adv</ta>
            <ta e="T130" id="Seg_1713" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1714" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1715" s="T131">v</ta>
            <ta e="T133" id="Seg_1716" s="T132">dempro-n:case</ta>
            <ta e="T134" id="Seg_1717" s="T133">v-v:n.fin</ta>
            <ta e="T135" id="Seg_1718" s="T134">n-n:case.poss</ta>
            <ta e="T136" id="Seg_1719" s="T135">dempro-n:case</ta>
            <ta e="T137" id="Seg_1720" s="T136">dempro-n:case</ta>
            <ta e="T138" id="Seg_1721" s="T137">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_1722" s="T138">v-v:ins-v:mood.pn</ta>
            <ta e="T140" id="Seg_1723" s="T139">adv</ta>
            <ta e="T141" id="Seg_1724" s="T140">conj</ta>
            <ta e="T142" id="Seg_1725" s="T141">dempro-n:case</ta>
            <ta e="T143" id="Seg_1726" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_1727" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1728" s="T144">n-n:case.poss</ta>
            <ta e="T146" id="Seg_1729" s="T145">conj</ta>
            <ta e="T148" id="Seg_1730" s="T147">adv</ta>
            <ta e="T149" id="Seg_1731" s="T148">v-v&gt;v-v:pn</ta>
            <ta e="T150" id="Seg_1732" s="T149">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1733" s="T0">v</ta>
            <ta e="T2" id="Seg_1734" s="T1">n</ta>
            <ta e="T3" id="Seg_1735" s="T2">n</ta>
            <ta e="T4" id="Seg_1736" s="T3">v</ta>
            <ta e="T6" id="Seg_1737" s="T5">pers</ta>
            <ta e="T7" id="Seg_1738" s="T6">v</ta>
            <ta e="T8" id="Seg_1739" s="T7">n</ta>
            <ta e="T9" id="Seg_1740" s="T8">quant</ta>
            <ta e="T10" id="Seg_1741" s="T9">que</ta>
            <ta e="T11" id="Seg_1742" s="T10">v</ta>
            <ta e="T12" id="Seg_1743" s="T11">dempro</ta>
            <ta e="T13" id="Seg_1744" s="T12">conj</ta>
            <ta e="T14" id="Seg_1745" s="T13">dempro</ta>
            <ta e="T15" id="Seg_1746" s="T14">n</ta>
            <ta e="T16" id="Seg_1747" s="T15">que</ta>
            <ta e="T17" id="Seg_1748" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1749" s="T17">v</ta>
            <ta e="T19" id="Seg_1750" s="T18">que</ta>
            <ta e="T20" id="Seg_1751" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_1752" s="T20">v</ta>
            <ta e="T22" id="Seg_1753" s="T21">que</ta>
            <ta e="T23" id="Seg_1754" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1755" s="T23">v</ta>
            <ta e="T25" id="Seg_1756" s="T24">adv</ta>
            <ta e="T26" id="Seg_1757" s="T25">v</ta>
            <ta e="T27" id="Seg_1758" s="T26">que</ta>
            <ta e="T28" id="Seg_1759" s="T27">pers</ta>
            <ta e="T29" id="Seg_1760" s="T28">v</ta>
            <ta e="T30" id="Seg_1761" s="T29">conj</ta>
            <ta e="T31" id="Seg_1762" s="T30">adv</ta>
            <ta e="T32" id="Seg_1763" s="T31">v</ta>
            <ta e="T33" id="Seg_1764" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_1765" s="T33">adv</ta>
            <ta e="T35" id="Seg_1766" s="T34">n</ta>
            <ta e="T36" id="Seg_1767" s="T35">conj</ta>
            <ta e="T37" id="Seg_1768" s="T36">n</ta>
            <ta e="T38" id="Seg_1769" s="T37">v</ta>
            <ta e="T39" id="Seg_1770" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1771" s="T39">pers</ta>
            <ta e="T41" id="Seg_1772" s="T40">n</ta>
            <ta e="T42" id="Seg_1773" s="T41">v</ta>
            <ta e="T43" id="Seg_1774" s="T42">v</ta>
            <ta e="T44" id="Seg_1775" s="T43">dempro</ta>
            <ta e="T45" id="Seg_1776" s="T44">n</ta>
            <ta e="T46" id="Seg_1777" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1778" s="T46">v</ta>
            <ta e="T48" id="Seg_1779" s="T47">adv</ta>
            <ta e="T49" id="Seg_1780" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1781" s="T49">v</ta>
            <ta e="T51" id="Seg_1782" s="T50">v</ta>
            <ta e="T52" id="Seg_1783" s="T51">adv</ta>
            <ta e="T152" id="Seg_1784" s="T52">propr</ta>
            <ta e="T53" id="Seg_1785" s="T152">n</ta>
            <ta e="T54" id="Seg_1786" s="T53">adv</ta>
            <ta e="T55" id="Seg_1787" s="T54">n</ta>
            <ta e="T56" id="Seg_1788" s="T55">v</ta>
            <ta e="T57" id="Seg_1789" s="T56">v</ta>
            <ta e="T58" id="Seg_1790" s="T57">n</ta>
            <ta e="T59" id="Seg_1791" s="T58">v</ta>
            <ta e="T60" id="Seg_1792" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_1793" s="T60">dempro</ta>
            <ta e="T62" id="Seg_1794" s="T61">dempro</ta>
            <ta e="T63" id="Seg_1795" s="T62">n</ta>
            <ta e="T64" id="Seg_1796" s="T63">v</ta>
            <ta e="T65" id="Seg_1797" s="T64">n</ta>
            <ta e="T66" id="Seg_1798" s="T65">refl</ta>
            <ta e="T67" id="Seg_1799" s="T66">n</ta>
            <ta e="T68" id="Seg_1800" s="T67">v</ta>
            <ta e="T69" id="Seg_1801" s="T68">num</ta>
            <ta e="T70" id="Seg_1802" s="T69">n</ta>
            <ta e="T71" id="Seg_1803" s="T70">dempro</ta>
            <ta e="T72" id="Seg_1804" s="T71">n</ta>
            <ta e="T73" id="Seg_1805" s="T72">n</ta>
            <ta e="T74" id="Seg_1806" s="T73">v</ta>
            <ta e="T75" id="Seg_1807" s="T74">n</ta>
            <ta e="T76" id="Seg_1808" s="T75">v</ta>
            <ta e="T77" id="Seg_1809" s="T76">n</ta>
            <ta e="T78" id="Seg_1810" s="T77">v</ta>
            <ta e="T79" id="Seg_1811" s="T78">v</ta>
            <ta e="T80" id="Seg_1812" s="T79">n</ta>
            <ta e="T81" id="Seg_1813" s="T80">v</ta>
            <ta e="T82" id="Seg_1814" s="T81">v</ta>
            <ta e="T83" id="Seg_1815" s="T82">adv</ta>
            <ta e="T84" id="Seg_1816" s="T83">num</ta>
            <ta e="T85" id="Seg_1817" s="T84">n</ta>
            <ta e="T86" id="Seg_1818" s="T85">v</ta>
            <ta e="T87" id="Seg_1819" s="T86">dempro</ta>
            <ta e="T88" id="Seg_1820" s="T87">n</ta>
            <ta e="T89" id="Seg_1821" s="T88">v</ta>
            <ta e="T90" id="Seg_1822" s="T89">que</ta>
            <ta e="T92" id="Seg_1823" s="T91">pers</ta>
            <ta e="T93" id="Seg_1824" s="T92">n</ta>
            <ta e="T94" id="Seg_1825" s="T93">interj</ta>
            <ta e="T95" id="Seg_1826" s="T94">n</ta>
            <ta e="T96" id="Seg_1827" s="T95">pers</ta>
            <ta e="T97" id="Seg_1828" s="T96">n</ta>
            <ta e="T151" id="Seg_1829" s="T97">v</ta>
            <ta e="T98" id="Seg_1830" s="T151">v</ta>
            <ta e="T99" id="Seg_1831" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1832" s="T99">adv</ta>
            <ta e="T101" id="Seg_1833" s="T100">n</ta>
            <ta e="T102" id="Seg_1834" s="T101">adv</ta>
            <ta e="T103" id="Seg_1835" s="T102">n</ta>
            <ta e="T107" id="Seg_1836" s="T106">v</ta>
            <ta e="T108" id="Seg_1837" s="T107">adv</ta>
            <ta e="T109" id="Seg_1838" s="T108">v</ta>
            <ta e="T110" id="Seg_1839" s="T109">v</ta>
            <ta e="T111" id="Seg_1840" s="T110">adv</ta>
            <ta e="T112" id="Seg_1841" s="T111">dempro</ta>
            <ta e="T113" id="Seg_1842" s="T112">adv</ta>
            <ta e="T114" id="Seg_1843" s="T113">v</ta>
            <ta e="T115" id="Seg_1844" s="T114">adv</ta>
            <ta e="T116" id="Seg_1845" s="T115">n</ta>
            <ta e="T117" id="Seg_1846" s="T116">v</ta>
            <ta e="T118" id="Seg_1847" s="T117">adv</ta>
            <ta e="T119" id="Seg_1848" s="T118">dempro</ta>
            <ta e="T120" id="Seg_1849" s="T119">v</ta>
            <ta e="T121" id="Seg_1850" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1851" s="T121">v</ta>
            <ta e="T123" id="Seg_1852" s="T122">adv</ta>
            <ta e="T124" id="Seg_1853" s="T123">adj</ta>
            <ta e="T125" id="Seg_1854" s="T124">v</ta>
            <ta e="T126" id="Seg_1855" s="T125">n</ta>
            <ta e="T127" id="Seg_1856" s="T126">v</ta>
            <ta e="T128" id="Seg_1857" s="T127">conj</ta>
            <ta e="T129" id="Seg_1858" s="T128">adv</ta>
            <ta e="T130" id="Seg_1859" s="T129">n</ta>
            <ta e="T131" id="Seg_1860" s="T130">v</ta>
            <ta e="T132" id="Seg_1861" s="T131">v</ta>
            <ta e="T133" id="Seg_1862" s="T132">dempro</ta>
            <ta e="T134" id="Seg_1863" s="T133">v</ta>
            <ta e="T135" id="Seg_1864" s="T134">n</ta>
            <ta e="T136" id="Seg_1865" s="T135">dempro</ta>
            <ta e="T137" id="Seg_1866" s="T136">dempro</ta>
            <ta e="T138" id="Seg_1867" s="T137">v</ta>
            <ta e="T139" id="Seg_1868" s="T138">v</ta>
            <ta e="T140" id="Seg_1869" s="T139">adv</ta>
            <ta e="T141" id="Seg_1870" s="T140">conj</ta>
            <ta e="T142" id="Seg_1871" s="T141">dempro</ta>
            <ta e="T143" id="Seg_1872" s="T142">n</ta>
            <ta e="T144" id="Seg_1873" s="T143">v</ta>
            <ta e="T145" id="Seg_1874" s="T144">n</ta>
            <ta e="T146" id="Seg_1875" s="T145">conj</ta>
            <ta e="T148" id="Seg_1876" s="T147">adv</ta>
            <ta e="T149" id="Seg_1877" s="T148">v</ta>
            <ta e="T150" id="Seg_1878" s="T149">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1879" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_1880" s="T2">np:G</ta>
            <ta e="T4" id="Seg_1881" s="T3">0.3.h:A</ta>
            <ta e="T6" id="Seg_1882" s="T5">pro.h:A</ta>
            <ta e="T8" id="Seg_1883" s="T7">np:L</ta>
            <ta e="T10" id="Seg_1884" s="T9">pro:Th</ta>
            <ta e="T11" id="Seg_1885" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_1886" s="T11">pro.h:R</ta>
            <ta e="T15" id="Seg_1887" s="T14">np.h:E</ta>
            <ta e="T16" id="Seg_1888" s="T15">pro:Th</ta>
            <ta e="T21" id="Seg_1889" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_1890" s="T21">pro:Th</ta>
            <ta e="T24" id="Seg_1891" s="T23">0.3.h:E</ta>
            <ta e="T25" id="Seg_1892" s="T24">adv:Time</ta>
            <ta e="T26" id="Seg_1893" s="T25">0.3.h:A</ta>
            <ta e="T28" id="Seg_1894" s="T27">pro.h:A</ta>
            <ta e="T32" id="Seg_1895" s="T31">0.3.h:A</ta>
            <ta e="T35" id="Seg_1896" s="T34">np.h:Th</ta>
            <ta e="T37" id="Seg_1897" s="T36">np:Th</ta>
            <ta e="T38" id="Seg_1898" s="T37">0.3.h:A</ta>
            <ta e="T40" id="Seg_1899" s="T39">pro.h:Poss</ta>
            <ta e="T41" id="Seg_1900" s="T40">np:Th</ta>
            <ta e="T43" id="Seg_1901" s="T42">0.2.h:A</ta>
            <ta e="T45" id="Seg_1902" s="T44">np:Th</ta>
            <ta e="T47" id="Seg_1903" s="T46">0.2.h:A</ta>
            <ta e="T48" id="Seg_1904" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_1905" s="T48">pro.h:A</ta>
            <ta e="T51" id="Seg_1906" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1907" s="T51">adv:L</ta>
            <ta e="T53" id="Seg_1908" s="T52">np:G</ta>
            <ta e="T54" id="Seg_1909" s="T53">adv:Time</ta>
            <ta e="T55" id="Seg_1910" s="T54">np.h:A</ta>
            <ta e="T57" id="Seg_1911" s="T56">0.2.h:A</ta>
            <ta e="T58" id="Seg_1912" s="T57">np:Th</ta>
            <ta e="T61" id="Seg_1913" s="T60">pro:R</ta>
            <ta e="T63" id="Seg_1914" s="T62">np.h:A</ta>
            <ta e="T65" id="Seg_1915" s="T64">np:So</ta>
            <ta e="T67" id="Seg_1916" s="T66">np:G</ta>
            <ta e="T68" id="Seg_1917" s="T67">0.3.h:A</ta>
            <ta e="T70" id="Seg_1918" s="T69">n:Time</ta>
            <ta e="T72" id="Seg_1919" s="T71">np.h:A</ta>
            <ta e="T73" id="Seg_1920" s="T72">np:P</ta>
            <ta e="T75" id="Seg_1921" s="T74">np:P</ta>
            <ta e="T76" id="Seg_1922" s="T75">0.3.h:A</ta>
            <ta e="T77" id="Seg_1923" s="T76">np:Th</ta>
            <ta e="T79" id="Seg_1924" s="T78">0.3.h:A</ta>
            <ta e="T80" id="Seg_1925" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_1926" s="T80">0.3.h:A</ta>
            <ta e="T82" id="Seg_1927" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1928" s="T82">adv:Time</ta>
            <ta e="T85" id="Seg_1929" s="T84">np:Th</ta>
            <ta e="T88" id="Seg_1930" s="T87">np.h:A</ta>
            <ta e="T92" id="Seg_1931" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_1932" s="T92">np:Th</ta>
            <ta e="T96" id="Seg_1933" s="T95">pro.h:Poss</ta>
            <ta e="T97" id="Seg_1934" s="T96">np:A</ta>
            <ta e="T100" id="Seg_1935" s="T99">adv:Time</ta>
            <ta e="T101" id="Seg_1936" s="T100">np:P</ta>
            <ta e="T102" id="Seg_1937" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_1938" s="T102">np:Ins</ta>
            <ta e="T107" id="Seg_1939" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1940" s="T107">adv:L</ta>
            <ta e="T109" id="Seg_1941" s="T108">0.3.h:E</ta>
            <ta e="T110" id="Seg_1942" s="T109">0.2.h:A</ta>
            <ta e="T111" id="Seg_1943" s="T110">adv:L</ta>
            <ta e="T112" id="Seg_1944" s="T111">pro.h:A</ta>
            <ta e="T113" id="Seg_1945" s="T112">adv:L</ta>
            <ta e="T115" id="Seg_1946" s="T114">adv:L</ta>
            <ta e="T116" id="Seg_1947" s="T115">np:E</ta>
            <ta e="T118" id="Seg_1948" s="T117">adv:L</ta>
            <ta e="T119" id="Seg_1949" s="T118">pro.h:Th</ta>
            <ta e="T120" id="Seg_1950" s="T119">0.3.h:A</ta>
            <ta e="T121" id="Seg_1951" s="T120">pro.h:A</ta>
            <ta e="T125" id="Seg_1952" s="T124">0.2.h:Th</ta>
            <ta e="T126" id="Seg_1953" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_1954" s="T126">0.2.h:Th</ta>
            <ta e="T129" id="Seg_1955" s="T128">adv:Time</ta>
            <ta e="T130" id="Seg_1956" s="T129">np.h:Th</ta>
            <ta e="T131" id="Seg_1957" s="T130">0.2.h:Th</ta>
            <ta e="T133" id="Seg_1958" s="T132">pro:G</ta>
            <ta e="T135" id="Seg_1959" s="T134">np:G</ta>
            <ta e="T136" id="Seg_1960" s="T135">pro.h:A</ta>
            <ta e="T137" id="Seg_1961" s="T136">pro.h:Th</ta>
            <ta e="T139" id="Seg_1962" s="T138">0.2.h:A</ta>
            <ta e="T140" id="Seg_1963" s="T139">adv:So</ta>
            <ta e="T143" id="Seg_1964" s="T142">np.h:A</ta>
            <ta e="T145" id="Seg_1965" s="T144">np:G</ta>
            <ta e="T148" id="Seg_1966" s="T147">adv:Time</ta>
            <ta e="T149" id="Seg_1967" s="T148">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1968" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_1969" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1970" s="T3">v:pred 0.3.h:S</ta>
            <ta e="T6" id="Seg_1971" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_1972" s="T6">v:pred</ta>
            <ta e="T10" id="Seg_1973" s="T9">pro:O</ta>
            <ta e="T11" id="Seg_1974" s="T10">v:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_1975" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_1976" s="T15">pro:O</ta>
            <ta e="T17" id="Seg_1977" s="T16">ptcl.neg</ta>
            <ta e="T18" id="Seg_1978" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_1979" s="T19">ptcl.neg</ta>
            <ta e="T21" id="Seg_1980" s="T20">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_1981" s="T21">pro:O</ta>
            <ta e="T23" id="Seg_1982" s="T22">ptcl.neg</ta>
            <ta e="T24" id="Seg_1983" s="T23">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_1984" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_1985" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_1986" s="T28">v:pred</ta>
            <ta e="T32" id="Seg_1987" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_1988" s="T32">ptcl.neg</ta>
            <ta e="T35" id="Seg_1989" s="T34">np.h:O</ta>
            <ta e="T37" id="Seg_1990" s="T36">np:O</ta>
            <ta e="T38" id="Seg_1991" s="T37">v:pred 0.3.h:S</ta>
            <ta e="T41" id="Seg_1992" s="T40">np:S</ta>
            <ta e="T42" id="Seg_1993" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_1994" s="T42">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_1995" s="T44">np:O</ta>
            <ta e="T47" id="Seg_1996" s="T46">v:pred 0.2.h:S</ta>
            <ta e="T49" id="Seg_1997" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1998" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_1999" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T55" id="Seg_2000" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_2001" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2002" s="T56">v:pred 0.2.h:S</ta>
            <ta e="T58" id="Seg_2003" s="T57">np:O</ta>
            <ta e="T60" id="Seg_2004" s="T59">ptcl:pred</ta>
            <ta e="T61" id="Seg_2005" s="T60">pro:O</ta>
            <ta e="T63" id="Seg_2006" s="T62">np.h:S</ta>
            <ta e="T64" id="Seg_2007" s="T63">v:pred</ta>
            <ta e="T68" id="Seg_2008" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_2009" s="T71">np.h:S</ta>
            <ta e="T73" id="Seg_2010" s="T72">np:O</ta>
            <ta e="T74" id="Seg_2011" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_2012" s="T74">np:O</ta>
            <ta e="T76" id="Seg_2013" s="T75">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_2014" s="T76">np:O</ta>
            <ta e="T78" id="Seg_2015" s="T77">conv:pred</ta>
            <ta e="T79" id="Seg_2016" s="T78">v:pred 0.3.h:S</ta>
            <ta e="T80" id="Seg_2017" s="T79">np:O</ta>
            <ta e="T81" id="Seg_2018" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_2019" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T85" id="Seg_2020" s="T84">np:S</ta>
            <ta e="T86" id="Seg_2021" s="T85">v:pred</ta>
            <ta e="T88" id="Seg_2022" s="T87">np.h:S</ta>
            <ta e="T89" id="Seg_2023" s="T88">v:pred</ta>
            <ta e="T93" id="Seg_2024" s="T92">np:S</ta>
            <ta e="T97" id="Seg_2025" s="T96">np:S</ta>
            <ta e="T151" id="Seg_2026" s="T97">conv:pred</ta>
            <ta e="T98" id="Seg_2027" s="T151">v:pred</ta>
            <ta e="T101" id="Seg_2028" s="T100">np:O</ta>
            <ta e="T107" id="Seg_2029" s="T106">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_2030" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_2031" s="T109">v:pred 0.2.h:S</ta>
            <ta e="T112" id="Seg_2032" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_2033" s="T113">v:pred</ta>
            <ta e="T116" id="Seg_2034" s="T115">np:S</ta>
            <ta e="T117" id="Seg_2035" s="T116">v:pred</ta>
            <ta e="T119" id="Seg_2036" s="T118">pro.h:O</ta>
            <ta e="T120" id="Seg_2037" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_2038" s="T120">pro.h:S</ta>
            <ta e="T122" id="Seg_2039" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_2040" s="T123">adj:pred</ta>
            <ta e="T125" id="Seg_2041" s="T124">cop 0.2.h:S</ta>
            <ta e="T126" id="Seg_2042" s="T125">n:pred</ta>
            <ta e="T127" id="Seg_2043" s="T126">cop 0.2.h:S</ta>
            <ta e="T130" id="Seg_2044" s="T129">n:pred</ta>
            <ta e="T131" id="Seg_2045" s="T130">cop 0.2.h:S</ta>
            <ta e="T132" id="Seg_2046" s="T131">ptcl:pred</ta>
            <ta e="T136" id="Seg_2047" s="T135">pro.h:S</ta>
            <ta e="T137" id="Seg_2048" s="T136">pro.h:O</ta>
            <ta e="T138" id="Seg_2049" s="T137">v:pred</ta>
            <ta e="T139" id="Seg_2050" s="T138">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_2051" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_2052" s="T143">v:pred</ta>
            <ta e="T149" id="Seg_2053" s="T148">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_2054" s="T1">RUS:cult</ta>
            <ta e="T8" id="Seg_2055" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_2056" s="T8">TURK:disc</ta>
            <ta e="T13" id="Seg_2057" s="T12">RUS:gram</ta>
            <ta e="T16" id="Seg_2058" s="T15">TURK:gram(INDEF)</ta>
            <ta e="T19" id="Seg_2059" s="T18">TURK:gram(INDEF)</ta>
            <ta e="T22" id="Seg_2060" s="T21">TURK:gram(INDEF)</ta>
            <ta e="T30" id="Seg_2061" s="T29">RUS:gram</ta>
            <ta e="T33" id="Seg_2062" s="T32">RUS:gram</ta>
            <ta e="T34" id="Seg_2063" s="T33">RUS:mod</ta>
            <ta e="T36" id="Seg_2064" s="T35">RUS:gram</ta>
            <ta e="T53" id="Seg_2065" s="T152">TAT:cult</ta>
            <ta e="T55" id="Seg_2066" s="T54">RUS:cult</ta>
            <ta e="T60" id="Seg_2067" s="T59">RUS:mod</ta>
            <ta e="T72" id="Seg_2068" s="T71">RUS:cult</ta>
            <ta e="T78" id="Seg_2069" s="T77">TURK:cult</ta>
            <ta e="T80" id="Seg_2070" s="T79">TURK:cult</ta>
            <ta e="T94" id="Seg_2071" s="T93">RUS:mod</ta>
            <ta e="T99" id="Seg_2072" s="T98">RUS:disc</ta>
            <ta e="T102" id="Seg_2073" s="T101">RUS:core</ta>
            <ta e="T128" id="Seg_2074" s="T127">RUS:gram</ta>
            <ta e="T132" id="Seg_2075" s="T131">RUS:mod</ta>
            <ta e="T141" id="Seg_2076" s="T140">RUS:gram</ta>
            <ta e="T146" id="Seg_2077" s="T145">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2078" s="T0">Солдат пришел к женщине.</ta>
            <ta e="T8" id="Seg_2079" s="T3">Он говорит: "Мы учимся в школе".</ta>
            <ta e="T12" id="Seg_2080" s="T8">Рассказал ей всякие вещи.</ta>
            <ta e="T18" id="Seg_2081" s="T12">А эта женщина ничего не знает.</ta>
            <ta e="T21" id="Seg_2082" s="T18">Нигде не была.</ta>
            <ta e="T24" id="Seg_2083" s="T21">Ничего не видела.</ta>
            <ta e="T29" id="Seg_2084" s="T24">Она говорит: "Как вы учитесь?"</ta>
            <ta e="T35" id="Seg_2085" s="T29">"Да хорошо учат, не только людей.</ta>
            <ta e="T38" id="Seg_2086" s="T35">И быков учат".</ta>
            <ta e="T42" id="Seg_2087" s="T38">"Ну, у меня есть бык.</ta>
            <ta e="T45" id="Seg_2088" s="T42">Возьми этого быка!"</ta>
            <ta e="T47" id="Seg_2089" s="T45">"Ну, приводит".</ta>
            <ta e="T53" id="Seg_2090" s="T47">Тогда он(а) его взял(а), привел(а) в Агинское.</ta>
            <ta e="T56" id="Seg_2091" s="T53">Потом солдаты говорят:</ta>
            <ta e="T61" id="Seg_2092" s="T56">"Принеси деньги, его надо кормить".</ta>
            <ta e="T65" id="Seg_2093" s="T61">Женщина (попрощалась?) с быком.</ta>
            <ta e="T68" id="Seg_2094" s="T65">Сама пришла домой.</ta>
            <ta e="T74" id="Seg_2095" s="T68">В одну зиму солдаты убили быка.</ta>
            <ta e="T76" id="Seg_2096" s="T74">Мясо съели.</ta>
            <ta e="T79" id="Seg_2097" s="T76">Шкуру продали.</ta>
            <ta e="T82" id="Seg_2098" s="T79">Купили водку, выпили.</ta>
            <ta e="T86" id="Seg_2099" s="T82">Потом одна зима [=год] прошла.</ta>
            <ta e="T89" id="Seg_2100" s="T86">Женщина пришла.</ta>
            <ta e="T93" id="Seg_2101" s="T89">"Где мой бык?"</ta>
            <ta e="T100" id="Seg_2102" s="T93">"О, бабушка, твой бык уже ушел [и стал человеком].</ta>
            <ta e="T107" id="Seg_2103" s="T100">Вон он сделал себе дом из камня.</ta>
            <ta e="T109" id="Seg_2104" s="T107">Там он живет.</ta>
            <ta e="T111" id="Seg_2105" s="T109">Иди туда!"</ta>
            <ta e="T114" id="Seg_2106" s="T111">Она пошла туда.</ta>
            <ta e="T117" id="Seg_2107" s="T114">"Здесь живет бык?"</ta>
            <ta e="T118" id="Seg_2108" s="T117">"Здесь".</ta>
            <ta e="T120" id="Seg_2109" s="T118">Ее впустили.</ta>
            <ta e="T122" id="Seg_2110" s="T120">Она говорит:</ta>
            <ta e="T125" id="Seg_2111" s="T122">"Ты стал очень красивым!</ta>
            <ta e="T131" id="Seg_2112" s="T125">Ты был быком, а теперь стал человеком!"</ta>
            <ta e="T135" id="Seg_2113" s="T131">Хотела набросить ему [веревку] на голову.</ta>
            <ta e="T138" id="Seg_2114" s="T135">Он ее выгнал.</ta>
            <ta e="T140" id="Seg_2115" s="T138">"Иди отсюда!"</ta>
            <ta e="T145" id="Seg_2116" s="T140">И женщина ушла домой.</ta>
            <ta e="T149" id="Seg_2117" s="T145">И сейчас живет.</ta>
            <ta e="T150" id="Seg_2118" s="T149">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2119" s="T0">A soldier came to a woman.</ta>
            <ta e="T8" id="Seg_2120" s="T3">He says: "We learn at school."</ta>
            <ta e="T12" id="Seg_2121" s="T8">He told her all kinds of things.</ta>
            <ta e="T18" id="Seg_2122" s="T12">But the woman did not know anything.</ta>
            <ta e="T21" id="Seg_2123" s="T18">She has never been anywhere.</ta>
            <ta e="T24" id="Seg_2124" s="T21">She has not see anything.</ta>
            <ta e="T29" id="Seg_2125" s="T24">Then she says: "How do you learn?"</ta>
            <ta e="T35" id="Seg_2126" s="T29">"Well, they teach well, and not only people.</ta>
            <ta e="T38" id="Seg_2127" s="T35">They also will teach bulls."</ta>
            <ta e="T42" id="Seg_2128" s="T38">"Well, I have a bull.</ta>
            <ta e="T45" id="Seg_2129" s="T42">Take this bull!"</ta>
            <ta e="T47" id="Seg_2130" s="T45">"Well, take it [there]!".</ta>
            <ta e="T53" id="Seg_2131" s="T47">Then s/he took it, brought it here, to Aginskoye.</ta>
            <ta e="T56" id="Seg_2132" s="T53">Then the soldiers say:</ta>
            <ta e="T61" id="Seg_2133" s="T56">"Give money, [we] have to feed it."</ta>
            <ta e="T65" id="Seg_2134" s="T61">The woman said (goodbye?) to the bull.</ta>
            <ta e="T68" id="Seg_2135" s="T65">She herself came home.</ta>
            <ta e="T74" id="Seg_2136" s="T68">During one winter the soldiers killed the bull.</ta>
            <ta e="T76" id="Seg_2137" s="T74">They ate the meat.</ta>
            <ta e="T79" id="Seg_2138" s="T76">They sold its skin.</ta>
            <ta e="T82" id="Seg_2139" s="T79">They bought vodka, they drank.</ta>
            <ta e="T86" id="Seg_2140" s="T82">Then one winter [=year] passed.</ta>
            <ta e="T89" id="Seg_2141" s="T86">The woman came.</ta>
            <ta e="T93" id="Seg_2142" s="T89">"Where is my bull?".</ta>
            <ta e="T100" id="Seg_2143" s="T93">"Oh, grandmother, your bull already left now [and became a man].</ta>
            <ta e="T107" id="Seg_2144" s="T100">[Look] over there it made its house of stones.</ta>
            <ta e="T109" id="Seg_2145" s="T107">There it is living.</ta>
            <ta e="T111" id="Seg_2146" s="T109">Go there!"</ta>
            <ta e="T114" id="Seg_2147" s="T111">She went there.</ta>
            <ta e="T117" id="Seg_2148" s="T114">"There lives a bull?"</ta>
            <ta e="T118" id="Seg_2149" s="T117">"There."</ta>
            <ta e="T120" id="Seg_2150" s="T118">They let her in.</ta>
            <ta e="T122" id="Seg_2151" s="T120">She says:</ta>
            <ta e="T125" id="Seg_2152" s="T122">"You became very beautiful!</ta>
            <ta e="T131" id="Seg_2153" s="T125">You were a bull but now you are a man!".</ta>
            <ta e="T135" id="Seg_2154" s="T131">She wanted to throw [a rope] on his head.</ta>
            <ta e="T138" id="Seg_2155" s="T135">He drove her away.</ta>
            <ta e="T140" id="Seg_2156" s="T138">"Go from here!".</ta>
            <ta e="T145" id="Seg_2157" s="T140">And the woman went home.</ta>
            <ta e="T149" id="Seg_2158" s="T145">And she is still living.</ta>
            <ta e="T150" id="Seg_2159" s="T149">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2160" s="T0">Ein Soldat kam zu einer Frau.</ta>
            <ta e="T8" id="Seg_2161" s="T3">Er sagt: "Wir lernen in der Schule."</ta>
            <ta e="T12" id="Seg_2162" s="T8">Er erzählte ihr alles mögliche.</ta>
            <ta e="T18" id="Seg_2163" s="T12">Aber die Frau wusste nichts.</ta>
            <ta e="T21" id="Seg_2164" s="T18">Sie war nirgendwo gewesen.</ta>
            <ta e="T24" id="Seg_2165" s="T21">Sie hatte nichts gesehen.</ta>
            <ta e="T29" id="Seg_2166" s="T24">Dann sagt sie: "Wie lernst du?"</ta>
            <ta e="T35" id="Seg_2167" s="T29">"Nun, sie lehren gut, und nicht nur Menschen.</ta>
            <ta e="T38" id="Seg_2168" s="T35">Sie werden auch Bullen lehren."</ta>
            <ta e="T42" id="Seg_2169" s="T38">"Nun, ich habe einen Bullen.</ta>
            <ta e="T45" id="Seg_2170" s="T42">Nimm diesen Bullen!</ta>
            <ta e="T47" id="Seg_2171" s="T45">Nun, bring ihn [dahin]!"</ta>
            <ta e="T53" id="Seg_2172" s="T47">Dann nahm er/sie ihn, brachte ihn hierher, nach Aginskoje.</ta>
            <ta e="T56" id="Seg_2173" s="T53">Dann sagen die Soldaten:</ta>
            <ta e="T61" id="Seg_2174" s="T56">"Gib Geld, [wir] müssen ihn füttern."</ta>
            <ta e="T65" id="Seg_2175" s="T61">Die Frau (verabschiedete?) sich vom Bullen.</ta>
            <ta e="T68" id="Seg_2176" s="T65">Sie selbst kam nach Hause.</ta>
            <ta e="T74" id="Seg_2177" s="T68">Eines Winters töteten die Soldaten den Bullen.</ta>
            <ta e="T76" id="Seg_2178" s="T74">Sie aßen das Fleisch.</ta>
            <ta e="T79" id="Seg_2179" s="T76">Sie verkauften sein Fell.</ta>
            <ta e="T82" id="Seg_2180" s="T79">Sie kauften Wodka, sie tranken.</ta>
            <ta e="T86" id="Seg_2181" s="T82">Dann verging ein Winter [=Jahr].</ta>
            <ta e="T89" id="Seg_2182" s="T86">Die Frau kam.</ta>
            <ta e="T93" id="Seg_2183" s="T89">"Wo ist mein Bulle?"</ta>
            <ta e="T100" id="Seg_2184" s="T93">"Oh, Großmutter, dein Bulle ist schon gegangen [und ist ein Mann geworden].</ta>
            <ta e="T107" id="Seg_2185" s="T100">[Schau mal], hat er dort sein Haus aus Steinen gemacht.</ta>
            <ta e="T109" id="Seg_2186" s="T107">Dort lebt er.</ta>
            <ta e="T111" id="Seg_2187" s="T109">Geh dorthin!"</ta>
            <ta e="T114" id="Seg_2188" s="T111">Sie ging dorthin.</ta>
            <ta e="T117" id="Seg_2189" s="T114">"Dort lebt ein Bulle?"</ta>
            <ta e="T118" id="Seg_2190" s="T117">"Dort."</ta>
            <ta e="T120" id="Seg_2191" s="T118">Sie lassen sie ein.</ta>
            <ta e="T122" id="Seg_2192" s="T120">Sie sagt:</ta>
            <ta e="T125" id="Seg_2193" s="T122">"Du bist sehr schön geworden!</ta>
            <ta e="T131" id="Seg_2194" s="T125">Du warst ein Bulle, aber jetzt bist du ein Mann!"</ta>
            <ta e="T135" id="Seg_2195" s="T131">Sie wollte [ein Seil] auf seinen Kopf werfen.</ta>
            <ta e="T138" id="Seg_2196" s="T135">Er jagte sie fort.</ta>
            <ta e="T140" id="Seg_2197" s="T138">"Geh weg von hier!"</ta>
            <ta e="T145" id="Seg_2198" s="T140">Und die Frau ging nach Hause.</ta>
            <ta e="T149" id="Seg_2199" s="T145">Und sie lebt immer noch.</ta>
            <ta e="T150" id="Seg_2200" s="T149">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_2201" s="T0">[GVY:] http://hyaenidae.narod.ru/story2/097.html</ta>
            <ta e="T53" id="Seg_2202" s="T47">[GVY:] in the original story, "to the town" ("в город").</ta>
            <ta e="T74" id="Seg_2203" s="T68">GVY:] In the original tale, "one year". It seems that Klavdiya Plotnikova always uses the word kö 'winter' as 'year'. Probably, the correct translation would be "After a year, the soldiers killed the bull."</ta>
            <ta e="T79" id="Seg_2204" s="T76">[GVY:] sadarlaʔ ibiʔi normally means 'bought'</ta>
            <ta e="T114" id="Seg_2205" s="T111">[GVY:] kabi = kambi</ta>
            <ta e="T117" id="Seg_2206" s="T114">[GVY:] In the original tale, the man that lived there had a family name Бычков from бычок 'bull'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T152" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T151" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
