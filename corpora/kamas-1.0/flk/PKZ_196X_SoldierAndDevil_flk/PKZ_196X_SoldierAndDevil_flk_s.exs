<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD65C3314-615D-C75B-CB70-1922B33E38E9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SoldierAndDevil_flk.wav" />
         <referenced-file url="PKZ_196X_SoldierAndDevil_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SoldierAndDevil_flk\PKZ_196X_SoldierAndDevil_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">139</ud-information>
            <ud-information attribute-name="# HIAT:w">86</ud-information>
            <ud-information attribute-name="# e">86</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.012" type="appl" />
         <tli id="T1" time="0.62" type="appl" />
         <tli id="T2" time="1.227" type="appl" />
         <tli id="T3" time="2.2133101352059534" />
         <tli id="T4" time="3.038" type="appl" />
         <tli id="T5" time="3.792" type="appl" />
         <tli id="T6" time="4.547" type="appl" />
         <tli id="T7" time="5.393" type="appl" />
         <tli id="T8" time="5.88" type="appl" />
         <tli id="T9" time="7.913250393040561" />
         <tli id="T10" time="8.819" type="appl" />
         <tli id="T11" time="9.355" type="appl" />
         <tli id="T12" time="9.892" type="appl" />
         <tli id="T13" time="10.428" type="appl" />
         <tli id="T14" time="10.965" type="appl" />
         <tli id="T15" time="11.501" type="appl" />
         <tli id="T16" time="12.038" type="appl" />
         <tli id="T17" time="12.548" type="appl" />
         <tli id="T18" time="13.058" type="appl" />
         <tli id="T19" time="13.567" type="appl" />
         <tli id="T20" time="14.077" type="appl" />
         <tli id="T21" time="14.587" type="appl" />
         <tli id="T22" time="16.326495545540297" />
         <tli id="T23" time="17.076" type="appl" />
         <tli id="T24" time="17.826" type="appl" />
         <tli id="T25" time="18.577" type="appl" />
         <tli id="T26" time="19.327" type="appl" />
         <tli id="T27" time="20.078" type="appl" />
         <tli id="T28" time="21.162" type="appl" />
         <tli id="T29" time="21.874" type="appl" />
         <tli id="T30" time="22.457" type="appl" />
         <tli id="T31" time="23.04" type="appl" />
         <tli id="T32" time="23.475" type="appl" />
         <tli id="T33" time="24.46641022953569" />
         <tli id="T34" time="25.332" type="appl" />
         <tli id="T35" time="26.042" type="appl" />
         <tli id="T36" time="26.752" type="appl" />
         <tli id="T37" time="27.462" type="appl" />
         <tli id="T38" time="28.172" type="appl" />
         <tli id="T39" time="28.881" type="appl" />
         <tli id="T40" time="29.591" type="appl" />
         <tli id="T41" time="30.301" type="appl" />
         <tli id="T42" time="31.011" type="appl" />
         <tli id="T43" time="31.721" type="appl" />
         <tli id="T44" time="32.431" type="appl" />
         <tli id="T45" time="33.144" type="appl" />
         <tli id="T46" time="33.857" type="appl" />
         <tli id="T47" time="34.57" type="appl" />
         <tli id="T48" time="35.282" type="appl" />
         <tli id="T49" time="35.995" type="appl" />
         <tli id="T50" time="36.708" type="appl" />
         <tli id="T51" time="37.421" type="appl" />
         <tli id="T52" time="38.134" type="appl" />
         <tli id="T53" time="38.846" type="appl" />
         <tli id="T54" time="39.559" type="appl" />
         <tli id="T55" time="40.272" type="appl" />
         <tli id="T56" time="40.985" type="appl" />
         <tli id="T57" time="41.917" type="appl" />
         <tli id="T58" time="42.673" type="appl" />
         <tli id="T59" time="43.429" type="appl" />
         <tli id="T60" time="44.185" type="appl" />
         <tli id="T61" time="45.059" type="appl" />
         <tli id="T62" time="45.756" type="appl" />
         <tli id="T63" time="46.454" type="appl" />
         <tli id="T64" time="47.151" type="appl" />
         <tli id="T65" time="47.848" type="appl" />
         <tli id="T66" time="48.545" type="appl" />
         <tli id="T67" time="49.574" type="appl" />
         <tli id="T68" time="50.469" type="appl" />
         <tli id="T69" time="51.364" type="appl" />
         <tli id="T86" time="51.747142857142855" type="intp" />
         <tli id="T70" time="52.258" type="appl" />
         <tli id="T71" time="52.888" type="appl" />
         <tli id="T72" time="53.374" type="appl" />
         <tli id="T73" time="53.861" type="appl" />
         <tli id="T74" time="54.347" type="appl" />
         <tli id="T75" time="55.48608510638298" />
         <tli id="T76" time="56.392" type="appl" />
         <tli id="T77" time="57.214" type="appl" />
         <tli id="T78" time="58.037" type="appl" />
         <tli id="T79" time="58.859" type="appl" />
         <tli id="T80" time="59.682" type="appl" />
         <tli id="T81" time="60.306" type="appl" />
         <tli id="T82" time="60.931" type="appl" />
         <tli id="T83" time="61.556" type="appl" />
         <tli id="T84" time="62.18" type="appl" />
         <tli id="T85" time="63.606" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T85" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kuza</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">soldat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ibi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">I</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">eneidəne</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">šobi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T6">
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">Da</ts>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">kuza</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">nuga</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_40" n="HIAT:u" s="T9">
                  <nts id="Seg_41" n="HIAT:ip">"</nts>
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">Eneidəne</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">xotʼ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">bɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">šobi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">măna</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">maːndə</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">kanzittə</ts>
                  <nts id="Seg_62" n="HIAT:ip">"</nts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_66" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">Eneidəne</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">šobi:</ts>
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">Tăn</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">măna</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">kăštəbiam</ts>
                  <nts id="Seg_82" n="HIAT:ip">?</nts>
                  <nts id="Seg_83" n="HIAT:ip">"</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_86" n="HIAT:u" s="T21">
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">Kăštəbiam</ts>
                  <nts id="Seg_90" n="HIAT:ip">"</nts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_94" n="HIAT:u" s="T22">
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <ts e="T23" id="Seg_97" n="HIAT:w" s="T22">Nu</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_102" n="HIAT:w" s="T23">mĭ-</ts>
                  <nts id="Seg_103" n="HIAT:ip">)</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_106" n="HIAT:w" s="T24">mĭʔ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_109" n="HIAT:w" s="T25">măna</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_111" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_113" n="HIAT:w" s="T26">dušal</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip">"</nts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_119" n="HIAT:u" s="T27">
                  <nts id="Seg_120" n="HIAT:ip">"</nts>
                  <ts e="T28" id="Seg_122" n="HIAT:w" s="T27">Mĭlim</ts>
                  <nts id="Seg_123" n="HIAT:ip">!</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_126" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_128" n="HIAT:w" s="T28">Štobɨ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_131" n="HIAT:w" s="T29">maːndə</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_134" n="HIAT:w" s="T30">kanzittə</ts>
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_139" n="HIAT:u" s="T31">
                  <nts id="Seg_140" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_142" n="HIAT:w" s="T31">Nu</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_146" n="HIAT:w" s="T32">kanaʔ</ts>
                  <nts id="Seg_147" n="HIAT:ip">!</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_150" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_152" n="HIAT:w" s="T33">Onʼiʔ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_154" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_156" n="HIAT:w" s="T34">gö-</ts>
                  <nts id="Seg_157" n="HIAT:ip">)</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_160" n="HIAT:w" s="T35">kö</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_163" n="HIAT:w" s="T36">amnoʔ</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_167" n="HIAT:w" s="T37">a</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_170" n="HIAT:w" s="T38">măn</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_173" n="HIAT:w" s="T39">dĭn</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_176" n="HIAT:w" s="T40">tăn</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_179" n="HIAT:w" s="T41">dăreʔaʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_182" n="HIAT:w" s="T42">molam</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_185" n="HIAT:w" s="T43">nuzittə</ts>
                  <nts id="Seg_186" n="HIAT:ip">"</nts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_190" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_192" n="HIAT:w" s="T44">Dĭgəttə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_195" n="HIAT:w" s="T45">dĭ</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_197" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_199" n="HIAT:w" s="T46">nu-</ts>
                  <nts id="Seg_200" n="HIAT:ip">)</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_203" n="HIAT:w" s="T47">selɨj</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_206" n="HIAT:w" s="T48">kö</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_209" n="HIAT:w" s="T49">nubi</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_213" n="HIAT:w" s="T50">nubi</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_217" n="HIAT:w" s="T51">a</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_220" n="HIAT:w" s="T52">rʼemniʔi</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_223" n="HIAT:w" s="T53">kros-nakres</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_226" n="HIAT:w" s="T54">ej</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_229" n="HIAT:w" s="T55">ellie</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_233" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_235" n="HIAT:w" s="T56">Dĭm</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_238" n="HIAT:w" s="T57">i</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_241" n="HIAT:w" s="T58">münörlüʔpiʔi</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_244" n="HIAT:w" s="T59">fsʼakə</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_248" n="HIAT:u" s="T60">
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_251" n="HIAT:w" s="T60">Girgit</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_254" n="HIAT:w" s="T61">jakšə</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_258" n="HIAT:w" s="T62">a</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_261" n="HIAT:w" s="T63">ĭmbi</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_264" n="HIAT:w" s="T64">abi</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_267" n="HIAT:w" s="T65">dăre</ts>
                  <nts id="Seg_268" n="HIAT:ip">?</nts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_272" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_274" n="HIAT:w" s="T66">Dĭgəttə</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_277" n="HIAT:w" s="T67">dĭ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_280" n="HIAT:w" s="T68">kö</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_283" n="HIAT:w" s="T69">kalla</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_286" n="HIAT:w" s="T86">dʼürbi</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_290" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_292" n="HIAT:w" s="T70">Dĭgəttə</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_294" n="HIAT:ip">(</nts>
                  <ts e="T72" id="Seg_296" n="HIAT:w" s="T71">šo-</ts>
                  <nts id="Seg_297" n="HIAT:ip">)</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_300" n="HIAT:w" s="T72">šobi</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_303" n="HIAT:w" s="T73">dĭ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_306" n="HIAT:w" s="T74">kuza</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_310" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_312" n="HIAT:w" s="T75">Dĭ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_315" n="HIAT:w" s="T76">eneidəne</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_318" n="HIAT:w" s="T77">i</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_321" n="HIAT:w" s="T78">dušabə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_323" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">nöməlluʔpi</ts>
                  <nts id="Seg_326" n="HIAT:ip">)</nts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_330" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_332" n="HIAT:w" s="T80">Bar</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_335" n="HIAT:w" s="T81">baʔluʔpi</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_338" n="HIAT:w" s="T82">i</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_341" n="HIAT:w" s="T83">nuʔməluʔpi</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_345" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_347" n="HIAT:w" s="T84">Kabarləj</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T85" id="Seg_350" n="sc" s="T0">
               <ts e="T1" id="Seg_352" n="e" s="T0">Kuza </ts>
               <ts e="T2" id="Seg_354" n="e" s="T1">soldat </ts>
               <ts e="T3" id="Seg_356" n="e" s="T2">ibi. </ts>
               <ts e="T4" id="Seg_358" n="e" s="T3">I </ts>
               <ts e="T5" id="Seg_360" n="e" s="T4">eneidəne </ts>
               <ts e="T6" id="Seg_362" n="e" s="T5">šobi. </ts>
               <ts e="T7" id="Seg_364" n="e" s="T6">(Da) </ts>
               <ts e="T8" id="Seg_366" n="e" s="T7">kuza </ts>
               <ts e="T9" id="Seg_368" n="e" s="T8">nuga. </ts>
               <ts e="T10" id="Seg_370" n="e" s="T9">"Eneidəne </ts>
               <ts e="T11" id="Seg_372" n="e" s="T10">xotʼ </ts>
               <ts e="T12" id="Seg_374" n="e" s="T11">bɨ </ts>
               <ts e="T13" id="Seg_376" n="e" s="T12">šobi </ts>
               <ts e="T14" id="Seg_378" n="e" s="T13">măna </ts>
               <ts e="T15" id="Seg_380" n="e" s="T14">maːndə </ts>
               <ts e="T16" id="Seg_382" n="e" s="T15">kanzittə". </ts>
               <ts e="T17" id="Seg_384" n="e" s="T16">Eneidəne </ts>
               <ts e="T18" id="Seg_386" n="e" s="T17">šobi:" </ts>
               <ts e="T19" id="Seg_388" n="e" s="T18">Tăn </ts>
               <ts e="T20" id="Seg_390" n="e" s="T19">măna </ts>
               <ts e="T21" id="Seg_392" n="e" s="T20">kăštəbiam?" </ts>
               <ts e="T22" id="Seg_394" n="e" s="T21">"Kăštəbiam". </ts>
               <ts e="T23" id="Seg_396" n="e" s="T22">"Nu, </ts>
               <ts e="T24" id="Seg_398" n="e" s="T23">(mĭ-) </ts>
               <ts e="T25" id="Seg_400" n="e" s="T24">mĭʔ </ts>
               <ts e="T26" id="Seg_402" n="e" s="T25">măna </ts>
               <ts e="T27" id="Seg_404" n="e" s="T26">(dušal)". </ts>
               <ts e="T28" id="Seg_406" n="e" s="T27">"Mĭlim! </ts>
               <ts e="T29" id="Seg_408" n="e" s="T28">Štobɨ </ts>
               <ts e="T30" id="Seg_410" n="e" s="T29">maːndə </ts>
               <ts e="T31" id="Seg_412" n="e" s="T30">kanzittə". </ts>
               <ts e="T32" id="Seg_414" n="e" s="T31">"Nu, </ts>
               <ts e="T33" id="Seg_416" n="e" s="T32">kanaʔ! </ts>
               <ts e="T34" id="Seg_418" n="e" s="T33">Onʼiʔ </ts>
               <ts e="T35" id="Seg_420" n="e" s="T34">(gö-) </ts>
               <ts e="T36" id="Seg_422" n="e" s="T35">kö </ts>
               <ts e="T37" id="Seg_424" n="e" s="T36">amnoʔ, </ts>
               <ts e="T38" id="Seg_426" n="e" s="T37">a </ts>
               <ts e="T39" id="Seg_428" n="e" s="T38">măn </ts>
               <ts e="T40" id="Seg_430" n="e" s="T39">dĭn </ts>
               <ts e="T41" id="Seg_432" n="e" s="T40">tăn </ts>
               <ts e="T42" id="Seg_434" n="e" s="T41">dăreʔaʔ </ts>
               <ts e="T43" id="Seg_436" n="e" s="T42">molam </ts>
               <ts e="T44" id="Seg_438" n="e" s="T43">nuzittə". </ts>
               <ts e="T45" id="Seg_440" n="e" s="T44">Dĭgəttə </ts>
               <ts e="T46" id="Seg_442" n="e" s="T45">dĭ </ts>
               <ts e="T47" id="Seg_444" n="e" s="T46">(nu-) </ts>
               <ts e="T48" id="Seg_446" n="e" s="T47">selɨj </ts>
               <ts e="T49" id="Seg_448" n="e" s="T48">kö </ts>
               <ts e="T50" id="Seg_450" n="e" s="T49">nubi, </ts>
               <ts e="T51" id="Seg_452" n="e" s="T50">nubi, </ts>
               <ts e="T52" id="Seg_454" n="e" s="T51">a </ts>
               <ts e="T53" id="Seg_456" n="e" s="T52">rʼemniʔi </ts>
               <ts e="T54" id="Seg_458" n="e" s="T53">kros-nakres </ts>
               <ts e="T55" id="Seg_460" n="e" s="T54">ej </ts>
               <ts e="T56" id="Seg_462" n="e" s="T55">ellie. </ts>
               <ts e="T57" id="Seg_464" n="e" s="T56">Dĭm </ts>
               <ts e="T58" id="Seg_466" n="e" s="T57">i </ts>
               <ts e="T59" id="Seg_468" n="e" s="T58">münörlüʔpiʔi </ts>
               <ts e="T60" id="Seg_470" n="e" s="T59">fsʼakə. </ts>
               <ts e="T61" id="Seg_472" n="e" s="T60">"Girgit </ts>
               <ts e="T62" id="Seg_474" n="e" s="T61">jakšə, </ts>
               <ts e="T63" id="Seg_476" n="e" s="T62">a </ts>
               <ts e="T64" id="Seg_478" n="e" s="T63">ĭmbi </ts>
               <ts e="T65" id="Seg_480" n="e" s="T64">abi </ts>
               <ts e="T66" id="Seg_482" n="e" s="T65">dăre?" </ts>
               <ts e="T67" id="Seg_484" n="e" s="T66">Dĭgəttə </ts>
               <ts e="T68" id="Seg_486" n="e" s="T67">dĭ </ts>
               <ts e="T69" id="Seg_488" n="e" s="T68">kö </ts>
               <ts e="T86" id="Seg_490" n="e" s="T69">kalla </ts>
               <ts e="T70" id="Seg_492" n="e" s="T86">dʼürbi. </ts>
               <ts e="T71" id="Seg_494" n="e" s="T70">Dĭgəttə </ts>
               <ts e="T72" id="Seg_496" n="e" s="T71">(šo-) </ts>
               <ts e="T73" id="Seg_498" n="e" s="T72">šobi </ts>
               <ts e="T74" id="Seg_500" n="e" s="T73">dĭ </ts>
               <ts e="T75" id="Seg_502" n="e" s="T74">kuza. </ts>
               <ts e="T76" id="Seg_504" n="e" s="T75">Dĭ </ts>
               <ts e="T77" id="Seg_506" n="e" s="T76">eneidəne </ts>
               <ts e="T78" id="Seg_508" n="e" s="T77">i </ts>
               <ts e="T79" id="Seg_510" n="e" s="T78">dušabə </ts>
               <ts e="T80" id="Seg_512" n="e" s="T79">(nöməlluʔpi). </ts>
               <ts e="T81" id="Seg_514" n="e" s="T80">Bar </ts>
               <ts e="T82" id="Seg_516" n="e" s="T81">baʔluʔpi </ts>
               <ts e="T83" id="Seg_518" n="e" s="T82">i </ts>
               <ts e="T84" id="Seg_520" n="e" s="T83">nuʔməluʔpi. </ts>
               <ts e="T85" id="Seg_522" n="e" s="T84">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_523" s="T0">PKZ_196X_SoldierAndDevil_flk.001 (001)</ta>
            <ta e="T6" id="Seg_524" s="T3">PKZ_196X_SoldierAndDevil_flk.002 (002)</ta>
            <ta e="T9" id="Seg_525" s="T6">PKZ_196X_SoldierAndDevil_flk.003 (003)</ta>
            <ta e="T16" id="Seg_526" s="T9">PKZ_196X_SoldierAndDevil_flk.004 (004)</ta>
            <ta e="T21" id="Seg_527" s="T16">PKZ_196X_SoldierAndDevil_flk.005 (005)</ta>
            <ta e="T22" id="Seg_528" s="T21">PKZ_196X_SoldierAndDevil_flk.006 (006)</ta>
            <ta e="T27" id="Seg_529" s="T22">PKZ_196X_SoldierAndDevil_flk.007 (007)</ta>
            <ta e="T28" id="Seg_530" s="T27">PKZ_196X_SoldierAndDevil_flk.008 (008)</ta>
            <ta e="T31" id="Seg_531" s="T28">PKZ_196X_SoldierAndDevil_flk.009 (009)</ta>
            <ta e="T33" id="Seg_532" s="T31">PKZ_196X_SoldierAndDevil_flk.010 (010)</ta>
            <ta e="T44" id="Seg_533" s="T33">PKZ_196X_SoldierAndDevil_flk.011 (011)</ta>
            <ta e="T56" id="Seg_534" s="T44">PKZ_196X_SoldierAndDevil_flk.012 (012)</ta>
            <ta e="T60" id="Seg_535" s="T56">PKZ_196X_SoldierAndDevil_flk.013 (013)</ta>
            <ta e="T66" id="Seg_536" s="T60">PKZ_196X_SoldierAndDevil_flk.014 (014)</ta>
            <ta e="T70" id="Seg_537" s="T66">PKZ_196X_SoldierAndDevil_flk.015 (015)</ta>
            <ta e="T75" id="Seg_538" s="T70">PKZ_196X_SoldierAndDevil_flk.016 (016)</ta>
            <ta e="T80" id="Seg_539" s="T75">PKZ_196X_SoldierAndDevil_flk.017 (017)</ta>
            <ta e="T84" id="Seg_540" s="T80">PKZ_196X_SoldierAndDevil_flk.018 (018)</ta>
            <ta e="T85" id="Seg_541" s="T84">PKZ_196X_SoldierAndDevil_flk.019 (019)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_542" s="T0">Kuza soldat ibi. </ta>
            <ta e="T6" id="Seg_543" s="T3">I eneidəne šobi. </ta>
            <ta e="T9" id="Seg_544" s="T6">(Da) kuza nuga. </ta>
            <ta e="T16" id="Seg_545" s="T9">"Eneidəne xotʼ bɨ šobi măna maːndə kanzittə". </ta>
            <ta e="T21" id="Seg_546" s="T16">Eneidəne šobi:" Tăn măna kăštəbiam?" </ta>
            <ta e="T22" id="Seg_547" s="T21">"Kăštəbiam". </ta>
            <ta e="T27" id="Seg_548" s="T22">"Nu, (mĭ-) mĭʔ măna (dušal)". </ta>
            <ta e="T28" id="Seg_549" s="T27">"Mĭlim! </ta>
            <ta e="T31" id="Seg_550" s="T28">Štobɨ maːndə kanzittə". </ta>
            <ta e="T33" id="Seg_551" s="T31">"Nu, kanaʔ! </ta>
            <ta e="T44" id="Seg_552" s="T33">Onʼiʔ (gö-) kö amnoʔ, a măn dĭn tăn dăreʔaʔ molam nuzittə". </ta>
            <ta e="T56" id="Seg_553" s="T44">Dĭgəttə dĭ (nu-) selɨj kö nubi, nubi, a rʼemniʔi kros-nakres ej ellie. </ta>
            <ta e="T60" id="Seg_554" s="T56">Dĭm i münörlüʔpiʔi fsʼakə. </ta>
            <ta e="T66" id="Seg_555" s="T60">"Girgit jakšə, a ĭmbi abi dăre?" </ta>
            <ta e="T70" id="Seg_556" s="T66">Dĭgəttə dĭ kö kalla dʼürbi. </ta>
            <ta e="T75" id="Seg_557" s="T70">Dĭgəttə (šo-) šobi dĭ kuza. </ta>
            <ta e="T80" id="Seg_558" s="T75">Dĭ eneidəne i dušabə (nöməlluʔpi). </ta>
            <ta e="T84" id="Seg_559" s="T80">Bar baʔluʔpi i nuʔməluʔpi. </ta>
            <ta e="T85" id="Seg_560" s="T84">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_561" s="T0">kuza</ta>
            <ta e="T2" id="Seg_562" s="T1">soldat</ta>
            <ta e="T3" id="Seg_563" s="T2">i-bi</ta>
            <ta e="T4" id="Seg_564" s="T3">i</ta>
            <ta e="T5" id="Seg_565" s="T4">eneidəne</ta>
            <ta e="T6" id="Seg_566" s="T5">šo-bi</ta>
            <ta e="T7" id="Seg_567" s="T6">da</ta>
            <ta e="T8" id="Seg_568" s="T7">kuza</ta>
            <ta e="T9" id="Seg_569" s="T8">nu-ga</ta>
            <ta e="T10" id="Seg_570" s="T9">eneidəne</ta>
            <ta e="T11" id="Seg_571" s="T10">xotʼ</ta>
            <ta e="T12" id="Seg_572" s="T11">bɨ</ta>
            <ta e="T13" id="Seg_573" s="T12">šo-bi</ta>
            <ta e="T14" id="Seg_574" s="T13">măna</ta>
            <ta e="T15" id="Seg_575" s="T14">ma-ndə</ta>
            <ta e="T16" id="Seg_576" s="T15">kan-zittə</ta>
            <ta e="T17" id="Seg_577" s="T16">eneidəne</ta>
            <ta e="T18" id="Seg_578" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_579" s="T18">tăn</ta>
            <ta e="T20" id="Seg_580" s="T19">măna</ta>
            <ta e="T21" id="Seg_581" s="T20">kăštə-bia-m</ta>
            <ta e="T22" id="Seg_582" s="T21">kăštə-bia-m</ta>
            <ta e="T23" id="Seg_583" s="T22">nu</ta>
            <ta e="T25" id="Seg_584" s="T24">mĭ-ʔ</ta>
            <ta e="T26" id="Seg_585" s="T25">măna</ta>
            <ta e="T27" id="Seg_586" s="T26">duša-l</ta>
            <ta e="T28" id="Seg_587" s="T27">mĭ-li-m</ta>
            <ta e="T29" id="Seg_588" s="T28">štobɨ</ta>
            <ta e="T30" id="Seg_589" s="T29">ma-ndə</ta>
            <ta e="T31" id="Seg_590" s="T30">kan-zittə</ta>
            <ta e="T32" id="Seg_591" s="T31">nu</ta>
            <ta e="T33" id="Seg_592" s="T32">kan-a-ʔ</ta>
            <ta e="T34" id="Seg_593" s="T33">onʼiʔ</ta>
            <ta e="T36" id="Seg_594" s="T35">kö</ta>
            <ta e="T37" id="Seg_595" s="T36">amno-ʔ</ta>
            <ta e="T38" id="Seg_596" s="T37">a</ta>
            <ta e="T39" id="Seg_597" s="T38">măn</ta>
            <ta e="T40" id="Seg_598" s="T39">dĭn</ta>
            <ta e="T41" id="Seg_599" s="T40">tăn</ta>
            <ta e="T42" id="Seg_600" s="T41">dăreʔaʔ</ta>
            <ta e="T43" id="Seg_601" s="T42">mo-la-m</ta>
            <ta e="T44" id="Seg_602" s="T43">nu-zittə</ta>
            <ta e="T45" id="Seg_603" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_604" s="T45">dĭ</ta>
            <ta e="T48" id="Seg_605" s="T47">selɨj</ta>
            <ta e="T49" id="Seg_606" s="T48">kö</ta>
            <ta e="T50" id="Seg_607" s="T49">nu-bi</ta>
            <ta e="T51" id="Seg_608" s="T50">nu-bi</ta>
            <ta e="T52" id="Seg_609" s="T51">a</ta>
            <ta e="T53" id="Seg_610" s="T52">rʼemni-ʔi</ta>
            <ta e="T54" id="Seg_611" s="T53">kros_nakres</ta>
            <ta e="T55" id="Seg_612" s="T54">ej</ta>
            <ta e="T56" id="Seg_613" s="T55">el-lie</ta>
            <ta e="T57" id="Seg_614" s="T56">dĭ-m</ta>
            <ta e="T58" id="Seg_615" s="T57">i</ta>
            <ta e="T59" id="Seg_616" s="T58">münör-lüʔ-pi-ʔi</ta>
            <ta e="T61" id="Seg_617" s="T60">girgit</ta>
            <ta e="T62" id="Seg_618" s="T61">jakšə</ta>
            <ta e="T63" id="Seg_619" s="T62">a</ta>
            <ta e="T64" id="Seg_620" s="T63">ĭmbi</ta>
            <ta e="T65" id="Seg_621" s="T64">a-bi</ta>
            <ta e="T66" id="Seg_622" s="T65">dăre</ta>
            <ta e="T67" id="Seg_623" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_624" s="T67">dĭ</ta>
            <ta e="T69" id="Seg_625" s="T68">kö</ta>
            <ta e="T86" id="Seg_626" s="T69">kal-la</ta>
            <ta e="T70" id="Seg_627" s="T86">dʼür-bi</ta>
            <ta e="T71" id="Seg_628" s="T70">dĭgəttə</ta>
            <ta e="T73" id="Seg_629" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_630" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_631" s="T74">kuza</ta>
            <ta e="T76" id="Seg_632" s="T75">dĭ</ta>
            <ta e="T77" id="Seg_633" s="T76">eneidəne</ta>
            <ta e="T78" id="Seg_634" s="T77">i</ta>
            <ta e="T79" id="Seg_635" s="T78">duša-bə</ta>
            <ta e="T80" id="Seg_636" s="T79">nöməl-luʔ-pi</ta>
            <ta e="T81" id="Seg_637" s="T80">bar</ta>
            <ta e="T82" id="Seg_638" s="T81">baʔ-luʔ-pi</ta>
            <ta e="T83" id="Seg_639" s="T82">i</ta>
            <ta e="T84" id="Seg_640" s="T83">nuʔmə-luʔ-pi</ta>
            <ta e="T85" id="Seg_641" s="T84">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_642" s="T0">kuza</ta>
            <ta e="T2" id="Seg_643" s="T1">soldat</ta>
            <ta e="T3" id="Seg_644" s="T2">i-bi</ta>
            <ta e="T4" id="Seg_645" s="T3">i</ta>
            <ta e="T5" id="Seg_646" s="T4">eneidəne</ta>
            <ta e="T6" id="Seg_647" s="T5">šo-bi</ta>
            <ta e="T7" id="Seg_648" s="T6">da</ta>
            <ta e="T8" id="Seg_649" s="T7">kuza</ta>
            <ta e="T9" id="Seg_650" s="T8">nu-gA</ta>
            <ta e="T10" id="Seg_651" s="T9">eneidəne</ta>
            <ta e="T11" id="Seg_652" s="T10">xotʼ</ta>
            <ta e="T12" id="Seg_653" s="T11">bɨ</ta>
            <ta e="T13" id="Seg_654" s="T12">šo-bi</ta>
            <ta e="T14" id="Seg_655" s="T13">măna</ta>
            <ta e="T15" id="Seg_656" s="T14">maʔ-gəndə</ta>
            <ta e="T16" id="Seg_657" s="T15">kan-zittə</ta>
            <ta e="T17" id="Seg_658" s="T16">eneidəne</ta>
            <ta e="T18" id="Seg_659" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_660" s="T18">tăn</ta>
            <ta e="T20" id="Seg_661" s="T19">măna</ta>
            <ta e="T21" id="Seg_662" s="T20">kăštə-bi-m</ta>
            <ta e="T22" id="Seg_663" s="T21">kăštə-bi-m</ta>
            <ta e="T23" id="Seg_664" s="T22">nu</ta>
            <ta e="T25" id="Seg_665" s="T24">mĭ-ʔ</ta>
            <ta e="T26" id="Seg_666" s="T25">măna</ta>
            <ta e="T27" id="Seg_667" s="T26">duša-l</ta>
            <ta e="T28" id="Seg_668" s="T27">mĭ-lV-m</ta>
            <ta e="T29" id="Seg_669" s="T28">štobɨ</ta>
            <ta e="T30" id="Seg_670" s="T29">maʔ-gəndə</ta>
            <ta e="T31" id="Seg_671" s="T30">kan-zittə</ta>
            <ta e="T32" id="Seg_672" s="T31">nu</ta>
            <ta e="T33" id="Seg_673" s="T32">kan-ə-ʔ</ta>
            <ta e="T34" id="Seg_674" s="T33">onʼiʔ</ta>
            <ta e="T36" id="Seg_675" s="T35">kö</ta>
            <ta e="T37" id="Seg_676" s="T36">amno-ʔ</ta>
            <ta e="T38" id="Seg_677" s="T37">a</ta>
            <ta e="T39" id="Seg_678" s="T38">măn</ta>
            <ta e="T40" id="Seg_679" s="T39">dĭn</ta>
            <ta e="T41" id="Seg_680" s="T40">tăn</ta>
            <ta e="T42" id="Seg_681" s="T41">dăreʔaʔ</ta>
            <ta e="T43" id="Seg_682" s="T42">mo-lV-m</ta>
            <ta e="T44" id="Seg_683" s="T43">nu-zittə</ta>
            <ta e="T45" id="Seg_684" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_685" s="T45">dĭ</ta>
            <ta e="T48" id="Seg_686" s="T47">celɨj</ta>
            <ta e="T49" id="Seg_687" s="T48">kö</ta>
            <ta e="T50" id="Seg_688" s="T49">nu-bi</ta>
            <ta e="T51" id="Seg_689" s="T50">nu-bi</ta>
            <ta e="T52" id="Seg_690" s="T51">a</ta>
            <ta e="T53" id="Seg_691" s="T52">rʼemni-jəʔ</ta>
            <ta e="T54" id="Seg_692" s="T53">kros_nakres</ta>
            <ta e="T55" id="Seg_693" s="T54">ej</ta>
            <ta e="T56" id="Seg_694" s="T55">hen-liA</ta>
            <ta e="T57" id="Seg_695" s="T56">dĭ-m</ta>
            <ta e="T58" id="Seg_696" s="T57">i</ta>
            <ta e="T59" id="Seg_697" s="T58">münör-luʔbdə-bi-jəʔ</ta>
            <ta e="T61" id="Seg_698" s="T60">girgit</ta>
            <ta e="T62" id="Seg_699" s="T61">jakšə</ta>
            <ta e="T63" id="Seg_700" s="T62">a</ta>
            <ta e="T64" id="Seg_701" s="T63">ĭmbi</ta>
            <ta e="T65" id="Seg_702" s="T64">a-bi</ta>
            <ta e="T66" id="Seg_703" s="T65">dărəʔ</ta>
            <ta e="T67" id="Seg_704" s="T66">dĭgəttə</ta>
            <ta e="T68" id="Seg_705" s="T67">dĭ</ta>
            <ta e="T69" id="Seg_706" s="T68">kö</ta>
            <ta e="T86" id="Seg_707" s="T69">kan-lAʔ</ta>
            <ta e="T70" id="Seg_708" s="T86">tʼür-bi</ta>
            <ta e="T71" id="Seg_709" s="T70">dĭgəttə</ta>
            <ta e="T73" id="Seg_710" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_711" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_712" s="T74">kuza</ta>
            <ta e="T76" id="Seg_713" s="T75">dĭ</ta>
            <ta e="T77" id="Seg_714" s="T76">eneidəne</ta>
            <ta e="T78" id="Seg_715" s="T77">i</ta>
            <ta e="T79" id="Seg_716" s="T78">duša-bə</ta>
            <ta e="T80" id="Seg_717" s="T79">nöməl-luʔbdə-bi</ta>
            <ta e="T81" id="Seg_718" s="T80">bar</ta>
            <ta e="T82" id="Seg_719" s="T81">baʔbdə-luʔbdə-bi</ta>
            <ta e="T83" id="Seg_720" s="T82">i</ta>
            <ta e="T84" id="Seg_721" s="T83">nuʔmə-luʔbdə-bi</ta>
            <ta e="T85" id="Seg_722" s="T84">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_723" s="T0">man.[NOM.SG]</ta>
            <ta e="T2" id="Seg_724" s="T1">soldier.[NOM.SG]</ta>
            <ta e="T3" id="Seg_725" s="T2">be-PST.[3SG]</ta>
            <ta e="T4" id="Seg_726" s="T3">and</ta>
            <ta e="T5" id="Seg_727" s="T4">devil.[NOM.SG]</ta>
            <ta e="T6" id="Seg_728" s="T5">come-PST.[3SG]</ta>
            <ta e="T7" id="Seg_729" s="T6">and</ta>
            <ta e="T8" id="Seg_730" s="T7">man.[NOM.SG]</ta>
            <ta e="T9" id="Seg_731" s="T8">stand-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_732" s="T9">devil.[NOM.SG]</ta>
            <ta e="T11" id="Seg_733" s="T10">although</ta>
            <ta e="T12" id="Seg_734" s="T11">IRREAL</ta>
            <ta e="T13" id="Seg_735" s="T12">come-PST.[3SG]</ta>
            <ta e="T14" id="Seg_736" s="T13">I.LAT</ta>
            <ta e="T15" id="Seg_737" s="T14">tent-LAT/LOC.3SG</ta>
            <ta e="T16" id="Seg_738" s="T15">go-INF.LAT</ta>
            <ta e="T17" id="Seg_739" s="T16">devil.[NOM.SG]</ta>
            <ta e="T18" id="Seg_740" s="T17">come-PST.[3SG]</ta>
            <ta e="T19" id="Seg_741" s="T18">you.NOM</ta>
            <ta e="T20" id="Seg_742" s="T19">I.ACC</ta>
            <ta e="T21" id="Seg_743" s="T20">call-PST-1SG</ta>
            <ta e="T22" id="Seg_744" s="T21">call-PST-1SG</ta>
            <ta e="T23" id="Seg_745" s="T22">well</ta>
            <ta e="T25" id="Seg_746" s="T24">give-IMP.2SG</ta>
            <ta e="T26" id="Seg_747" s="T25">I.LAT</ta>
            <ta e="T27" id="Seg_748" s="T26">soul-NOM/GEN/ACC.2SG</ta>
            <ta e="T28" id="Seg_749" s="T27">give-FUT-1SG</ta>
            <ta e="T29" id="Seg_750" s="T28">so.that</ta>
            <ta e="T30" id="Seg_751" s="T29">tent-LAT/LOC.3SG</ta>
            <ta e="T31" id="Seg_752" s="T30">go-INF.LAT</ta>
            <ta e="T32" id="Seg_753" s="T31">well</ta>
            <ta e="T33" id="Seg_754" s="T32">go-EP-IMP.2SG</ta>
            <ta e="T34" id="Seg_755" s="T33">one.[NOM.SG]</ta>
            <ta e="T36" id="Seg_756" s="T35">winter.[NOM.SG]</ta>
            <ta e="T37" id="Seg_757" s="T36">live-IMP.2SG</ta>
            <ta e="T38" id="Seg_758" s="T37">and</ta>
            <ta e="T39" id="Seg_759" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_760" s="T39">there</ta>
            <ta e="T41" id="Seg_761" s="T40">you.NOM</ta>
            <ta e="T42" id="Seg_762" s="T41">like</ta>
            <ta e="T43" id="Seg_763" s="T42">become-FUT-1SG</ta>
            <ta e="T44" id="Seg_764" s="T43">stand-INF.LAT</ta>
            <ta e="T45" id="Seg_765" s="T44">then</ta>
            <ta e="T46" id="Seg_766" s="T45">this.[NOM.SG]</ta>
            <ta e="T48" id="Seg_767" s="T47">whole</ta>
            <ta e="T49" id="Seg_768" s="T48">winter.[NOM.SG]</ta>
            <ta e="T50" id="Seg_769" s="T49">stand-PST.[3SG]</ta>
            <ta e="T51" id="Seg_770" s="T50">stand-PST.[3SG]</ta>
            <ta e="T52" id="Seg_771" s="T51">and</ta>
            <ta e="T53" id="Seg_772" s="T52">belt.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T54" id="Seg_773" s="T53">crosswise</ta>
            <ta e="T55" id="Seg_774" s="T54">NEG</ta>
            <ta e="T56" id="Seg_775" s="T55">put-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_776" s="T56">this-ACC</ta>
            <ta e="T58" id="Seg_777" s="T57">and</ta>
            <ta e="T59" id="Seg_778" s="T58">beat-MOM-PST-3PL</ta>
            <ta e="T61" id="Seg_779" s="T60">what.kind</ta>
            <ta e="T62" id="Seg_780" s="T61">good.[NOM.SG]</ta>
            <ta e="T63" id="Seg_781" s="T62">and</ta>
            <ta e="T64" id="Seg_782" s="T63">what.[NOM.SG]</ta>
            <ta e="T65" id="Seg_783" s="T64">make-PST.[3SG]</ta>
            <ta e="T66" id="Seg_784" s="T65">so</ta>
            <ta e="T67" id="Seg_785" s="T66">then</ta>
            <ta e="T68" id="Seg_786" s="T67">this.[NOM.SG]</ta>
            <ta e="T69" id="Seg_787" s="T68">winter.[NOM.SG]</ta>
            <ta e="T86" id="Seg_788" s="T69">go-CVB</ta>
            <ta e="T70" id="Seg_789" s="T86">disappear-PST.[3SG]</ta>
            <ta e="T71" id="Seg_790" s="T70">then</ta>
            <ta e="T73" id="Seg_791" s="T72">come-PST.[3SG]</ta>
            <ta e="T74" id="Seg_792" s="T73">this.[NOM.SG]</ta>
            <ta e="T75" id="Seg_793" s="T74">man.[NOM.SG]</ta>
            <ta e="T76" id="Seg_794" s="T75">this.[NOM.SG]</ta>
            <ta e="T77" id="Seg_795" s="T76">devil.[NOM.SG]</ta>
            <ta e="T78" id="Seg_796" s="T77">and</ta>
            <ta e="T79" id="Seg_797" s="T78">soul-ACC.3SG</ta>
            <ta e="T80" id="Seg_798" s="T79">forget-MOM-PST.[3SG]</ta>
            <ta e="T81" id="Seg_799" s="T80">all</ta>
            <ta e="T82" id="Seg_800" s="T81">throw-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_801" s="T82">and</ta>
            <ta e="T84" id="Seg_802" s="T83">run-MOM-PST.[3SG]</ta>
            <ta e="T85" id="Seg_803" s="T84">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_804" s="T0">мужчина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_805" s="T1">солдат.[NOM.SG]</ta>
            <ta e="T3" id="Seg_806" s="T2">быть-PST.[3SG]</ta>
            <ta e="T4" id="Seg_807" s="T3">и</ta>
            <ta e="T5" id="Seg_808" s="T4">черт.[NOM.SG]</ta>
            <ta e="T6" id="Seg_809" s="T5">прийти-PST.[3SG]</ta>
            <ta e="T7" id="Seg_810" s="T6">и</ta>
            <ta e="T8" id="Seg_811" s="T7">мужчина.[NOM.SG]</ta>
            <ta e="T9" id="Seg_812" s="T8">стоять-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_813" s="T9">черт.[NOM.SG]</ta>
            <ta e="T11" id="Seg_814" s="T10">хотя</ta>
            <ta e="T12" id="Seg_815" s="T11">IRREAL</ta>
            <ta e="T13" id="Seg_816" s="T12">прийти-PST.[3SG]</ta>
            <ta e="T14" id="Seg_817" s="T13">я.LAT</ta>
            <ta e="T15" id="Seg_818" s="T14">чум-LAT/LOC.3SG</ta>
            <ta e="T16" id="Seg_819" s="T15">пойти-INF.LAT</ta>
            <ta e="T17" id="Seg_820" s="T16">черт.[NOM.SG]</ta>
            <ta e="T18" id="Seg_821" s="T17">прийти-PST.[3SG]</ta>
            <ta e="T19" id="Seg_822" s="T18">ты.NOM</ta>
            <ta e="T20" id="Seg_823" s="T19">я.ACC</ta>
            <ta e="T21" id="Seg_824" s="T20">позвать-PST-1SG</ta>
            <ta e="T22" id="Seg_825" s="T21">позвать-PST-1SG</ta>
            <ta e="T23" id="Seg_826" s="T22">ну</ta>
            <ta e="T25" id="Seg_827" s="T24">дать-IMP.2SG</ta>
            <ta e="T26" id="Seg_828" s="T25">я.LAT</ta>
            <ta e="T27" id="Seg_829" s="T26">душа-NOM/GEN/ACC.2SG</ta>
            <ta e="T28" id="Seg_830" s="T27">дать-FUT-1SG</ta>
            <ta e="T29" id="Seg_831" s="T28">чтобы</ta>
            <ta e="T30" id="Seg_832" s="T29">чум-LAT/LOC.3SG</ta>
            <ta e="T31" id="Seg_833" s="T30">пойти-INF.LAT</ta>
            <ta e="T32" id="Seg_834" s="T31">ну</ta>
            <ta e="T33" id="Seg_835" s="T32">пойти-EP-IMP.2SG</ta>
            <ta e="T34" id="Seg_836" s="T33">один.[NOM.SG]</ta>
            <ta e="T36" id="Seg_837" s="T35">зима.[NOM.SG]</ta>
            <ta e="T37" id="Seg_838" s="T36">жить-IMP.2SG</ta>
            <ta e="T38" id="Seg_839" s="T37">а</ta>
            <ta e="T39" id="Seg_840" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_841" s="T39">там</ta>
            <ta e="T41" id="Seg_842" s="T40">ты.NOM</ta>
            <ta e="T42" id="Seg_843" s="T41">как</ta>
            <ta e="T43" id="Seg_844" s="T42">стать-FUT-1SG</ta>
            <ta e="T44" id="Seg_845" s="T43">стоять-INF.LAT</ta>
            <ta e="T45" id="Seg_846" s="T44">тогда</ta>
            <ta e="T46" id="Seg_847" s="T45">этот.[NOM.SG]</ta>
            <ta e="T48" id="Seg_848" s="T47">целый</ta>
            <ta e="T49" id="Seg_849" s="T48">зима.[NOM.SG]</ta>
            <ta e="T50" id="Seg_850" s="T49">стоять-PST.[3SG]</ta>
            <ta e="T51" id="Seg_851" s="T50">стоять-PST.[3SG]</ta>
            <ta e="T52" id="Seg_852" s="T51">а</ta>
            <ta e="T53" id="Seg_853" s="T52">ремень.PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T54" id="Seg_854" s="T53">крест_накрест</ta>
            <ta e="T55" id="Seg_855" s="T54">NEG</ta>
            <ta e="T56" id="Seg_856" s="T55">класть-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_857" s="T56">этот-ACC</ta>
            <ta e="T58" id="Seg_858" s="T57">и</ta>
            <ta e="T59" id="Seg_859" s="T58">бить-MOM-PST-3PL</ta>
            <ta e="T61" id="Seg_860" s="T60">какой</ta>
            <ta e="T62" id="Seg_861" s="T61">хороший.[NOM.SG]</ta>
            <ta e="T63" id="Seg_862" s="T62">а</ta>
            <ta e="T64" id="Seg_863" s="T63">что.[NOM.SG]</ta>
            <ta e="T65" id="Seg_864" s="T64">делать-PST.[3SG]</ta>
            <ta e="T66" id="Seg_865" s="T65">так</ta>
            <ta e="T67" id="Seg_866" s="T66">тогда</ta>
            <ta e="T68" id="Seg_867" s="T67">этот.[NOM.SG]</ta>
            <ta e="T69" id="Seg_868" s="T68">зима.[NOM.SG]</ta>
            <ta e="T86" id="Seg_869" s="T69">пойти-CVB</ta>
            <ta e="T70" id="Seg_870" s="T86">исчезнуть-PST.[3SG]</ta>
            <ta e="T71" id="Seg_871" s="T70">тогда</ta>
            <ta e="T73" id="Seg_872" s="T72">прийти-PST.[3SG]</ta>
            <ta e="T74" id="Seg_873" s="T73">этот.[NOM.SG]</ta>
            <ta e="T75" id="Seg_874" s="T74">мужчина.[NOM.SG]</ta>
            <ta e="T76" id="Seg_875" s="T75">этот.[NOM.SG]</ta>
            <ta e="T77" id="Seg_876" s="T76">черт.[NOM.SG]</ta>
            <ta e="T78" id="Seg_877" s="T77">и</ta>
            <ta e="T79" id="Seg_878" s="T78">душа-ACC.3SG</ta>
            <ta e="T80" id="Seg_879" s="T79">забыть-MOM-PST.[3SG]</ta>
            <ta e="T81" id="Seg_880" s="T80">весь</ta>
            <ta e="T82" id="Seg_881" s="T81">бросить-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_882" s="T82">и</ta>
            <ta e="T84" id="Seg_883" s="T83">бежать-MOM-PST.[3SG]</ta>
            <ta e="T85" id="Seg_884" s="T84">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_885" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_886" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_887" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_888" s="T3">conj</ta>
            <ta e="T5" id="Seg_889" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_890" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_891" s="T6">conj</ta>
            <ta e="T8" id="Seg_892" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_893" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_894" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_895" s="T10">conj</ta>
            <ta e="T12" id="Seg_896" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_897" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_898" s="T13">pers</ta>
            <ta e="T15" id="Seg_899" s="T14">n-n:case.poss</ta>
            <ta e="T16" id="Seg_900" s="T15">v-v:n.fin</ta>
            <ta e="T17" id="Seg_901" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_902" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_903" s="T18">pers</ta>
            <ta e="T20" id="Seg_904" s="T19">pers</ta>
            <ta e="T21" id="Seg_905" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_906" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_907" s="T22">ptcl</ta>
            <ta e="T25" id="Seg_908" s="T24">v-v:mood.pn</ta>
            <ta e="T26" id="Seg_909" s="T25">pers</ta>
            <ta e="T27" id="Seg_910" s="T26">n-n:case.poss</ta>
            <ta e="T28" id="Seg_911" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_912" s="T28">conj</ta>
            <ta e="T30" id="Seg_913" s="T29">n-n:case.poss</ta>
            <ta e="T31" id="Seg_914" s="T30">v-v:n.fin</ta>
            <ta e="T32" id="Seg_915" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_916" s="T32">v-v:ins-v:mood.pn</ta>
            <ta e="T34" id="Seg_917" s="T33">num-n:case</ta>
            <ta e="T36" id="Seg_918" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_919" s="T36">v-v:mood.pn</ta>
            <ta e="T38" id="Seg_920" s="T37">conj</ta>
            <ta e="T39" id="Seg_921" s="T38">pers</ta>
            <ta e="T40" id="Seg_922" s="T39">adv</ta>
            <ta e="T41" id="Seg_923" s="T40">pers</ta>
            <ta e="T42" id="Seg_924" s="T41">post</ta>
            <ta e="T43" id="Seg_925" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_926" s="T43">v-v:n.fin</ta>
            <ta e="T45" id="Seg_927" s="T44">adv</ta>
            <ta e="T46" id="Seg_928" s="T45">dempro-n:case</ta>
            <ta e="T48" id="Seg_929" s="T47">adj</ta>
            <ta e="T49" id="Seg_930" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_931" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_932" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_933" s="T51">conj</ta>
            <ta e="T53" id="Seg_934" s="T52">n-n:case.poss</ta>
            <ta e="T54" id="Seg_935" s="T53">adv</ta>
            <ta e="T55" id="Seg_936" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_937" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_938" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_939" s="T57">conj</ta>
            <ta e="T59" id="Seg_940" s="T58">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_941" s="T60">que</ta>
            <ta e="T62" id="Seg_942" s="T61">adj-n:case</ta>
            <ta e="T63" id="Seg_943" s="T62">conj</ta>
            <ta e="T64" id="Seg_944" s="T63">que-n:case</ta>
            <ta e="T65" id="Seg_945" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_946" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_947" s="T66">adv</ta>
            <ta e="T68" id="Seg_948" s="T67">dempro-n:case</ta>
            <ta e="T69" id="Seg_949" s="T68">n-n:case</ta>
            <ta e="T86" id="Seg_950" s="T69">v-v:n-fin</ta>
            <ta e="T70" id="Seg_951" s="T86">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_952" s="T70">adv</ta>
            <ta e="T73" id="Seg_953" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_954" s="T73">dempro-n:case</ta>
            <ta e="T75" id="Seg_955" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_956" s="T75">dempro-n:case</ta>
            <ta e="T77" id="Seg_957" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_958" s="T77">conj</ta>
            <ta e="T79" id="Seg_959" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_960" s="T79">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_961" s="T80">quant</ta>
            <ta e="T82" id="Seg_962" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_963" s="T82">conj</ta>
            <ta e="T84" id="Seg_964" s="T83">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_965" s="T84">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_966" s="T0">n</ta>
            <ta e="T2" id="Seg_967" s="T1">n</ta>
            <ta e="T3" id="Seg_968" s="T2">v</ta>
            <ta e="T4" id="Seg_969" s="T3">conj</ta>
            <ta e="T5" id="Seg_970" s="T4">n</ta>
            <ta e="T6" id="Seg_971" s="T5">v</ta>
            <ta e="T7" id="Seg_972" s="T6">conj</ta>
            <ta e="T8" id="Seg_973" s="T7">n</ta>
            <ta e="T9" id="Seg_974" s="T8">v</ta>
            <ta e="T10" id="Seg_975" s="T9">n</ta>
            <ta e="T11" id="Seg_976" s="T10">conj</ta>
            <ta e="T12" id="Seg_977" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_978" s="T12">v</ta>
            <ta e="T14" id="Seg_979" s="T13">pers</ta>
            <ta e="T15" id="Seg_980" s="T14">n</ta>
            <ta e="T16" id="Seg_981" s="T15">v</ta>
            <ta e="T17" id="Seg_982" s="T16">n</ta>
            <ta e="T18" id="Seg_983" s="T17">v</ta>
            <ta e="T19" id="Seg_984" s="T18">pers</ta>
            <ta e="T20" id="Seg_985" s="T19">pers</ta>
            <ta e="T21" id="Seg_986" s="T20">v</ta>
            <ta e="T22" id="Seg_987" s="T21">v</ta>
            <ta e="T23" id="Seg_988" s="T22">ptcl</ta>
            <ta e="T25" id="Seg_989" s="T24">pers</ta>
            <ta e="T26" id="Seg_990" s="T25">pers</ta>
            <ta e="T27" id="Seg_991" s="T26">n</ta>
            <ta e="T28" id="Seg_992" s="T27">v</ta>
            <ta e="T29" id="Seg_993" s="T28">conj</ta>
            <ta e="T30" id="Seg_994" s="T29">n</ta>
            <ta e="T31" id="Seg_995" s="T30">v</ta>
            <ta e="T32" id="Seg_996" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_997" s="T32">v</ta>
            <ta e="T34" id="Seg_998" s="T33">num</ta>
            <ta e="T36" id="Seg_999" s="T35">n</ta>
            <ta e="T37" id="Seg_1000" s="T36">v</ta>
            <ta e="T38" id="Seg_1001" s="T37">conj</ta>
            <ta e="T39" id="Seg_1002" s="T38">pers</ta>
            <ta e="T40" id="Seg_1003" s="T39">adv</ta>
            <ta e="T41" id="Seg_1004" s="T40">pers</ta>
            <ta e="T42" id="Seg_1005" s="T41">post</ta>
            <ta e="T43" id="Seg_1006" s="T42">v</ta>
            <ta e="T44" id="Seg_1007" s="T43">v</ta>
            <ta e="T45" id="Seg_1008" s="T44">adv</ta>
            <ta e="T46" id="Seg_1009" s="T45">dempro</ta>
            <ta e="T48" id="Seg_1010" s="T47">adj</ta>
            <ta e="T49" id="Seg_1011" s="T48">n</ta>
            <ta e="T50" id="Seg_1012" s="T49">v</ta>
            <ta e="T51" id="Seg_1013" s="T50">v</ta>
            <ta e="T52" id="Seg_1014" s="T51">conj</ta>
            <ta e="T53" id="Seg_1015" s="T52">n</ta>
            <ta e="T54" id="Seg_1016" s="T53">adv</ta>
            <ta e="T55" id="Seg_1017" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1018" s="T55">v</ta>
            <ta e="T57" id="Seg_1019" s="T56">dempro</ta>
            <ta e="T58" id="Seg_1020" s="T57">conj</ta>
            <ta e="T59" id="Seg_1021" s="T58">v</ta>
            <ta e="T61" id="Seg_1022" s="T60">que</ta>
            <ta e="T62" id="Seg_1023" s="T61">adj</ta>
            <ta e="T63" id="Seg_1024" s="T62">conj</ta>
            <ta e="T64" id="Seg_1025" s="T63">que</ta>
            <ta e="T65" id="Seg_1026" s="T64">v</ta>
            <ta e="T66" id="Seg_1027" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1028" s="T66">adv</ta>
            <ta e="T68" id="Seg_1029" s="T67">dempro</ta>
            <ta e="T69" id="Seg_1030" s="T68">n</ta>
            <ta e="T86" id="Seg_1031" s="T69">v</ta>
            <ta e="T70" id="Seg_1032" s="T86">v</ta>
            <ta e="T71" id="Seg_1033" s="T70">adv</ta>
            <ta e="T73" id="Seg_1034" s="T72">v</ta>
            <ta e="T74" id="Seg_1035" s="T73">dempro</ta>
            <ta e="T75" id="Seg_1036" s="T74">n</ta>
            <ta e="T76" id="Seg_1037" s="T75">dempro</ta>
            <ta e="T77" id="Seg_1038" s="T76">n</ta>
            <ta e="T78" id="Seg_1039" s="T77">conj</ta>
            <ta e="T79" id="Seg_1040" s="T78">n</ta>
            <ta e="T80" id="Seg_1041" s="T79">v</ta>
            <ta e="T81" id="Seg_1042" s="T80">quant</ta>
            <ta e="T82" id="Seg_1043" s="T81">v</ta>
            <ta e="T83" id="Seg_1044" s="T82">conj</ta>
            <ta e="T84" id="Seg_1045" s="T83">v</ta>
            <ta e="T85" id="Seg_1046" s="T84">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1047" s="T0">np.h:Th</ta>
            <ta e="T2" id="Seg_1048" s="T1">np.h:Th</ta>
            <ta e="T5" id="Seg_1049" s="T4">np.h:A</ta>
            <ta e="T8" id="Seg_1050" s="T7">np.h:Th</ta>
            <ta e="T10" id="Seg_1051" s="T9">np.h:A</ta>
            <ta e="T15" id="Seg_1052" s="T14">np:G</ta>
            <ta e="T17" id="Seg_1053" s="T16">np.h:A</ta>
            <ta e="T19" id="Seg_1054" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_1055" s="T19">pro.h:R</ta>
            <ta e="T22" id="Seg_1056" s="T21">0.1.h:A</ta>
            <ta e="T25" id="Seg_1057" s="T24">0.2.h:A</ta>
            <ta e="T26" id="Seg_1058" s="T25">pro:G</ta>
            <ta e="T27" id="Seg_1059" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_1060" s="T27">0.1.h:A</ta>
            <ta e="T30" id="Seg_1061" s="T29">np:G</ta>
            <ta e="T33" id="Seg_1062" s="T32">0.2.h:A</ta>
            <ta e="T36" id="Seg_1063" s="T35">n:Time</ta>
            <ta e="T37" id="Seg_1064" s="T36">0.2.h:E</ta>
            <ta e="T39" id="Seg_1065" s="T38">pro.h:A</ta>
            <ta e="T40" id="Seg_1066" s="T39">adv:L</ta>
            <ta e="T41" id="Seg_1067" s="T40">pro.h:Th</ta>
            <ta e="T45" id="Seg_1068" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_1069" s="T45">pro.h:Th</ta>
            <ta e="T49" id="Seg_1070" s="T48">n:Time</ta>
            <ta e="T51" id="Seg_1071" s="T50">0.3.h:Th</ta>
            <ta e="T53" id="Seg_1072" s="T52">np:Th</ta>
            <ta e="T56" id="Seg_1073" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_1074" s="T56">pro.h:E</ta>
            <ta e="T59" id="Seg_1075" s="T58">0.3.h:A</ta>
            <ta e="T65" id="Seg_1076" s="T64">0.3.h:A</ta>
            <ta e="T67" id="Seg_1077" s="T66">adv:Time</ta>
            <ta e="T69" id="Seg_1078" s="T68">np:Th</ta>
            <ta e="T71" id="Seg_1079" s="T70">adv:Time</ta>
            <ta e="T75" id="Seg_1080" s="T74">np.h:A</ta>
            <ta e="T77" id="Seg_1081" s="T76">np.h:E</ta>
            <ta e="T79" id="Seg_1082" s="T78">np:Th</ta>
            <ta e="T82" id="Seg_1083" s="T81">0.3.h:A</ta>
            <ta e="T84" id="Seg_1084" s="T83">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1085" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_1086" s="T1">n:pred</ta>
            <ta e="T3" id="Seg_1087" s="T2">cop</ta>
            <ta e="T5" id="Seg_1088" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_1089" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_1090" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_1091" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_1092" s="T9">np.h:S</ta>
            <ta e="T13" id="Seg_1093" s="T12">v:pred</ta>
            <ta e="T17" id="Seg_1094" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_1095" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_1096" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_1097" s="T19">pro.h:O</ta>
            <ta e="T21" id="Seg_1098" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_1099" s="T21">v:pred 0.1.h:S</ta>
            <ta e="T25" id="Seg_1100" s="T24">v:pred 0.2.h:S</ta>
            <ta e="T27" id="Seg_1101" s="T26">np:O</ta>
            <ta e="T28" id="Seg_1102" s="T27">v:pred 0.1.h:S</ta>
            <ta e="T33" id="Seg_1103" s="T32">v:pred 0.2.h:S</ta>
            <ta e="T37" id="Seg_1104" s="T36">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_1105" s="T38">pro.h:S</ta>
            <ta e="T43" id="Seg_1106" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_1107" s="T45">pro.h:S</ta>
            <ta e="T50" id="Seg_1108" s="T49">v:pred</ta>
            <ta e="T51" id="Seg_1109" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T53" id="Seg_1110" s="T52">np:O</ta>
            <ta e="T55" id="Seg_1111" s="T54">ptcl.neg</ta>
            <ta e="T56" id="Seg_1112" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_1113" s="T56">pro.h:O</ta>
            <ta e="T59" id="Seg_1114" s="T58">v:pred 0.3.h:S</ta>
            <ta e="T62" id="Seg_1115" s="T61">adj:pred</ta>
            <ta e="T65" id="Seg_1116" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_1117" s="T68">np:S</ta>
            <ta e="T86" id="Seg_1118" s="T69">conv:pred</ta>
            <ta e="T70" id="Seg_1119" s="T86">v:pred</ta>
            <ta e="T73" id="Seg_1120" s="T72">v:pred</ta>
            <ta e="T75" id="Seg_1121" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_1122" s="T76">np.h:S</ta>
            <ta e="T79" id="Seg_1123" s="T78">np:O</ta>
            <ta e="T80" id="Seg_1124" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_1125" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_1126" s="T83">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1127" s="T1">RUS:cult</ta>
            <ta e="T4" id="Seg_1128" s="T3">RUS:gram</ta>
            <ta e="T7" id="Seg_1129" s="T6">RUS:gram</ta>
            <ta e="T11" id="Seg_1130" s="T10">RUS:gram</ta>
            <ta e="T12" id="Seg_1131" s="T11">RUS:gram</ta>
            <ta e="T27" id="Seg_1132" s="T26">RUS:cult</ta>
            <ta e="T29" id="Seg_1133" s="T28">RUS:gram</ta>
            <ta e="T38" id="Seg_1134" s="T37">RUS:gram</ta>
            <ta e="T48" id="Seg_1135" s="T47">RUS:core</ta>
            <ta e="T52" id="Seg_1136" s="T51">RUS:gram</ta>
            <ta e="T53" id="Seg_1137" s="T52">RUS:cult</ta>
            <ta e="T58" id="Seg_1138" s="T57">RUS:gram</ta>
            <ta e="T62" id="Seg_1139" s="T61">TURK:core</ta>
            <ta e="T63" id="Seg_1140" s="T62">RUS:gram</ta>
            <ta e="T78" id="Seg_1141" s="T77">RUS:gram</ta>
            <ta e="T79" id="Seg_1142" s="T78">RUS:cult</ta>
            <ta e="T81" id="Seg_1143" s="T80">TURK:disc</ta>
            <ta e="T83" id="Seg_1144" s="T82">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1145" s="T0">Один человек был солдатом.</ta>
            <ta e="T6" id="Seg_1146" s="T3">И пришел черт.</ta>
            <ta e="T9" id="Seg_1147" s="T6">Человек стоит.</ta>
            <ta e="T16" id="Seg_1148" s="T9">"Хоть бы черт пришел, чтобы мне домой попасть".</ta>
            <ta e="T21" id="Seg_1149" s="T16">Черт пришел: "Ты звал меня?"</ta>
            <ta e="T22" id="Seg_1150" s="T21">"Звал".</ta>
            <ta e="T27" id="Seg_1151" s="T22">"Ну, дай мне свою душу".</ta>
            <ta e="T28" id="Seg_1152" s="T27">"Дам!</ta>
            <ta e="T31" id="Seg_1153" s="T28">Чтобы домой пойти".</ta>
            <ta e="T33" id="Seg_1154" s="T31">"Ну, иди!</ta>
            <ta e="T44" id="Seg_1155" s="T33">Один год живи [дома], а я постою тут, как ты".</ta>
            <ta e="T56" id="Seg_1156" s="T44">Он стоял там целый год, но не надел ремни крест-накрест.</ta>
            <ta e="T60" id="Seg_1157" s="T56">Его били по-всякому.</ta>
            <ta e="T66" id="Seg_1158" s="T60">"Какой хороший [был солдат], зачем он так сделал?"</ta>
            <ta e="T70" id="Seg_1159" s="T66">Этот год прошел.</ta>
            <ta e="T75" id="Seg_1160" s="T70">Пришел тот человек.</ta>
            <ta e="T80" id="Seg_1161" s="T75">Черт и про душу его забыл.</ta>
            <ta e="T84" id="Seg_1162" s="T80">Все бросил и убежал.</ta>
            <ta e="T85" id="Seg_1163" s="T84">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1164" s="T0">A man was [serving] as a soldier.</ta>
            <ta e="T6" id="Seg_1165" s="T3">And an evil spirit came.</ta>
            <ta e="T9" id="Seg_1166" s="T6">And the man stands.</ta>
            <ta e="T16" id="Seg_1167" s="T9">"If only the evil spirit came to let me go home."</ta>
            <ta e="T21" id="Seg_1168" s="T16">The evil spirit came: "You called me?"</ta>
            <ta e="T22" id="Seg_1169" s="T21">"I called."</ta>
            <ta e="T27" id="Seg_1170" s="T22">"Well, give me your soul."</ta>
            <ta e="T28" id="Seg_1171" s="T27">"I will give!</ta>
            <ta e="T31" id="Seg_1172" s="T28">To go home."</ta>
            <ta e="T33" id="Seg_1173" s="T31">"Well, go!</ta>
            <ta e="T44" id="Seg_1174" s="T33">Live one year [at home], and I'll stand here like you."</ta>
            <ta e="T56" id="Seg_1175" s="T44">Then he stood there the whole year, but he won't put the shoulder-belt crosswise.</ta>
            <ta e="T60" id="Seg_1176" s="T56">They beat him in all ways.</ta>
            <ta e="T66" id="Seg_1177" s="T60">"He was so good, why did he do so?"</ta>
            <ta e="T70" id="Seg_1178" s="T66">Then this year passed.</ta>
            <ta e="T75" id="Seg_1179" s="T70">Then the man came.</ta>
            <ta e="T80" id="Seg_1180" s="T75">The devil forgot his soul.</ta>
            <ta e="T84" id="Seg_1181" s="T80">He left everything and ran [away].</ta>
            <ta e="T85" id="Seg_1182" s="T84">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1183" s="T0">Ein Mann diente als Soldat.</ta>
            <ta e="T6" id="Seg_1184" s="T3">Und ein böser Geist kam.</ta>
            <ta e="T9" id="Seg_1185" s="T6">Und der Mann steht.</ta>
            <ta e="T16" id="Seg_1186" s="T9">"Wenn der böse Geist nur käme, um mich nach Hause zu lassen."</ta>
            <ta e="T21" id="Seg_1187" s="T16">Der böse Geist kam: "Du hast mich gerufen?"</ta>
            <ta e="T22" id="Seg_1188" s="T21">"Ich habe gerufen."</ta>
            <ta e="T27" id="Seg_1189" s="T22">"Gut, gib mir deine Seele."</ta>
            <ta e="T28" id="Seg_1190" s="T27">"Gebe ich!</ta>
            <ta e="T31" id="Seg_1191" s="T28">Um nach Hause zu gehen."</ta>
            <ta e="T33" id="Seg_1192" s="T31">"Gut, geh!</ta>
            <ta e="T44" id="Seg_1193" s="T33">Lebe ein Jahr [zuhause] und ich stehe hier wie du."</ta>
            <ta e="T56" id="Seg_1194" s="T44">Dann stand er dort das ganze Jahr, aber er zieht den Schultergurt nicht über Kreuz an.</ta>
            <ta e="T60" id="Seg_1195" s="T56">Sie schlagen ihn auf alle Art und Weise.</ta>
            <ta e="T66" id="Seg_1196" s="T60">"Er war so gut, warum hat er das so gemacht?"</ta>
            <ta e="T70" id="Seg_1197" s="T66">Dann verging das Jahr.</ta>
            <ta e="T75" id="Seg_1198" s="T70">Dann kam der Mann.</ta>
            <ta e="T80" id="Seg_1199" s="T75">Der Teufel vergaß seine Seele.</ta>
            <ta e="T84" id="Seg_1200" s="T80">Er ließ alles und rannte [weg].</ta>
            <ta e="T85" id="Seg_1201" s="T84">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_1202" s="T0">[GVY:] http://hyaenidae.narod.ru/story2/096.html</ta>
            <ta e="T16" id="Seg_1203" s="T9">[GVY:] Syntax unclear</ta>
            <ta e="T21" id="Seg_1204" s="T16">[GVY:] kăštəbiam = kăštəbial</ta>
            <ta e="T44" id="Seg_1205" s="T33">[GVY:] dăreʔaʔ - a separate postposition 'like' (cf. dərgit D 13a) or just a mispronounciation of dăreʔ? 3. Molam nuzittə - a loan translation from Russian "стану стоять"</ta>
            <ta e="T60" id="Seg_1206" s="T56">[GVY:] Unclear</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T86" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
