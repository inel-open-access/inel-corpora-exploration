<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE65B392F-D30B-E22B-6DE0-C7F434807853">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SunMoonAndRaven_flk.wav" />
         <referenced-file url="PKZ_196X_SunMoonAndRaven_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SunMoonAndRaven_flk\PKZ_196X_SunMoonAndRaven_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">421</ud-information>
            <ud-information attribute-name="# HIAT:w">261</ud-information>
            <ud-information attribute-name="# e">261</ud-information>
            <ud-information attribute-name="# HIAT:u">67</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2145" time="0.007" type="appl" />
         <tli id="T2146" time="0.729" type="appl" />
         <tli id="T2147" time="1.451" type="appl" />
         <tli id="T2148" time="2.173" type="appl" />
         <tli id="T2149" time="2.673" type="appl" />
         <tli id="T2150" time="3.173" type="appl" />
         <tli id="T2151" time="3.673" type="appl" />
         <tli id="T2152" time="4.173" type="appl" />
         <tli id="T2153" time="4.901" type="appl" />
         <tli id="T2154" time="5.492" type="appl" />
         <tli id="T2155" time="6.082" type="appl" />
         <tli id="T2156" time="6.672" type="appl" />
         <tli id="T2157" time="7.263" type="appl" />
         <tli id="T2158" time="7.853" type="appl" />
         <tli id="T2159" time="8.444" type="appl" />
         <tli id="T2160" time="9.034" type="appl" />
         <tli id="T2161" time="9.546" type="appl" />
         <tli id="T2162" time="10.059" type="appl" />
         <tli id="T2163" time="10.572" type="appl" />
         <tli id="T2164" time="11.084" type="appl" />
         <tli id="T2165" time="11.959" type="appl" />
         <tli id="T2166" time="12.675" type="appl" />
         <tli id="T2167" time="13.391" type="appl" />
         <tli id="T2168" time="14.107" type="appl" />
         <tli id="T2169" time="14.823" type="appl" />
         <tli id="T2170" time="15.539" type="appl" />
         <tli id="T2171" time="16.317" type="appl" />
         <tli id="T2172" time="16.88" type="appl" />
         <tli id="T2173" time="17.444" type="appl" />
         <tli id="T2174" time="18.007" type="appl" />
         <tli id="T2175" time="18.89978420373312" />
         <tli id="T2176" time="19.637" type="appl" />
         <tli id="T2177" time="20.3" type="appl" />
         <tli id="T2178" time="20.964" type="appl" />
         <tli id="T2179" time="23.639730083399524" />
         <tli id="T2180" time="24.853" type="appl" />
         <tli id="T2181" time="25.892" type="appl" />
         <tli id="T2182" time="27.093023987291502" />
         <tli id="T2183" time="27.784" type="appl" />
         <tli id="T2184" time="28.638" type="appl" />
         <tli id="T2185" time="29.832992702541702" />
         <tli id="T2186" time="30.41" type="appl" />
         <tli id="T2187" time="31.07" type="appl" />
         <tli id="T2188" time="31.731" type="appl" />
         <tli id="T2189" time="32.391" type="appl" />
         <tli id="T2190" time="33.49961750397141" />
         <tli id="T2191" time="34.432" type="appl" />
         <tli id="T2192" time="35.269" type="appl" />
         <tli id="T2193" time="35.99" type="appl" />
         <tli id="T2194" time="36.566" type="appl" />
         <tli id="T2195" time="37.142" type="appl" />
         <tli id="T2196" time="38.03956566719619" />
         <tli id="T2197" time="38.582" type="appl" />
         <tli id="T2198" time="39.298" type="appl" />
         <tli id="T2199" time="40.013" type="appl" />
         <tli id="T2200" time="40.729" type="appl" />
         <tli id="T2201" time="41.85952205123114" />
         <tli id="T2202" time="42.825" type="appl" />
         <tli id="T2203" time="43.79949990071485" />
         <tli id="T2204" time="44.736" type="appl" />
         <tli id="T2205" time="45.54" type="appl" />
         <tli id="T2206" time="46.344" type="appl" />
         <tli id="T2207" time="47.147" type="appl" />
         <tli id="T2208" time="47.837" type="appl" />
         <tli id="T2209" time="48.526" type="appl" />
         <tli id="T2210" time="49.216" type="appl" />
         <tli id="T2211" time="49.905" type="appl" />
         <tli id="T2212" time="50.595" type="appl" />
         <tli id="T2213" time="51.371" type="appl" />
         <tli id="T2214" time="52.125" type="appl" />
         <tli id="T2215" time="52.88" type="appl" />
         <tli id="T2216" time="53.544" type="appl" />
         <tli id="T2217" time="54.208" type="appl" />
         <tli id="T2218" time="54.872" type="appl" />
         <tli id="T2219" time="55.536" type="appl" />
         <tli id="T2220" time="56.486021713661636" />
         <tli id="T2221" time="57.453" type="appl" />
         <tli id="T2222" time="58.265" type="appl" />
         <tli id="T2223" time="59.077" type="appl" />
         <tli id="T2224" time="60.418" type="appl" />
         <tli id="T2225" time="61.68" type="appl" />
         <tli id="T2226" time="63.919270174741854" />
         <tli id="T2227" time="64.612" type="appl" />
         <tli id="T2228" time="65.125" type="appl" />
         <tli id="T2229" time="65.637" type="appl" />
         <tli id="T2230" time="66.15" type="appl" />
         <tli id="T2231" time="66.662" type="appl" />
         <tli id="T2232" time="67.175" type="appl" />
         <tli id="T2233" time="67.623" type="appl" />
         <tli id="T2234" time="68.071" type="appl" />
         <tli id="T2235" time="68.963" type="appl" />
         <tli id="T2236" time="69.77" type="appl" />
         <tli id="T2237" time="70.67919299046862" />
         <tli id="T2238" time="71.657" type="appl" />
         <tli id="T2239" time="72.682" type="appl" />
         <tli id="T2240" time="73.708" type="appl" />
         <tli id="T2241" time="74.734" type="appl" />
         <tli id="T2242" time="75.203" type="appl" />
         <tli id="T2243" time="75.672" type="appl" />
         <tli id="T2244" time="76.14" type="appl" />
         <tli id="T2245" time="76.609" type="appl" />
         <tli id="T2246" time="77.078" type="appl" />
         <tli id="T2247" time="77.547" type="appl" />
         <tli id="T2248" time="77.998" type="appl" />
         <tli id="T2249" time="78.448" type="appl" />
         <tli id="T2250" time="79.72" type="appl" />
         <tli id="T2251" time="80.715" type="appl" />
         <tli id="T2252" time="81.71" type="appl" />
         <tli id="T2253" time="82.705" type="appl" />
         <tli id="T2254" time="83.588" type="appl" />
         <tli id="T2255" time="84.446" type="appl" />
         <tli id="T2256" time="85.304" type="appl" />
         <tli id="T2257" time="86.162" type="appl" />
         <tli id="T2258" time="87.02" type="appl" />
         <tli id="T2259" time="87.878" type="appl" />
         <tli id="T2260" time="88.736" type="appl" />
         <tli id="T2261" time="89.594" type="appl" />
         <tli id="T2262" time="90.452" type="appl" />
         <tli id="T2263" time="91.31" type="appl" />
         <tli id="T2264" time="92.38561181493249" />
         <tli id="T2265" time="92.845" type="appl" />
         <tli id="T2266" time="93.378" type="appl" />
         <tli id="T2267" time="93.91" type="appl" />
         <tli id="T2268" time="94.684" type="appl" />
         <tli id="T2269" time="95.459" type="appl" />
         <tli id="T2270" time="96.233" type="appl" />
         <tli id="T2271" time="97.007" type="appl" />
         <tli id="T2272" time="97.782" type="appl" />
         <tli id="T2273" time="98.556" type="appl" />
         <tli id="T2274" time="99.773" type="appl" />
         <tli id="T2275" time="100.84" type="appl" />
         <tli id="T2276" time="101.906" type="appl" />
         <tli id="T2277" time="102.973" type="appl" />
         <tli id="T2278" time="104.039" type="appl" />
         <tli id="T2279" time="105.106" type="appl" />
         <tli id="T2280" time="106.172" type="appl" />
         <tli id="T2281" time="107.239" type="appl" />
         <tli id="T2282" time="108.83209069698172" />
         <tli id="T2283" time="109.685" type="appl" />
         <tli id="T2284" time="110.465" type="appl" />
         <tli id="T2285" time="111.246" type="appl" />
         <tli id="T2286" time="112.45204936457506" />
         <tli id="T2287" time="112.658" type="appl" />
         <tli id="T2288" time="113.29" type="appl" />
         <tli id="T2289" time="113.922" type="appl" />
         <tli id="T2290" time="114.554" type="appl" />
         <tli id="T2291" time="115.186" type="appl" />
         <tli id="T2292" time="116.07867462271645" />
         <tli id="T2293" time="116.795" type="appl" />
         <tli id="T2294" time="117.763" type="appl" />
         <tli id="T2295" time="118.566" type="appl" />
         <tli id="T2296" time="119.332" type="appl" />
         <tli id="T2297" time="119.891" type="appl" />
         <tli id="T2298" time="120.449" type="appl" />
         <tli id="T2299" time="121.008" type="appl" />
         <tli id="T2300" time="121.66" type="appl" />
         <tli id="T2301" time="122.311" type="appl" />
         <tli id="T2302" time="122.963" type="appl" />
         <tli id="T2303" time="123.615" type="appl" />
         <tli id="T2304" time="124.267" type="appl" />
         <tli id="T2305" time="124.918" type="appl" />
         <tli id="T2306" time="125.57" type="appl" />
         <tli id="T2307" time="126.355" type="appl" />
         <tli id="T2308" time="127.14" type="appl" />
         <tli id="T2309" time="127.925" type="appl" />
         <tli id="T2310" time="128.71" type="appl" />
         <tli id="T2311" time="129.58" type="appl" />
         <tli id="T2312" time="130.451" type="appl" />
         <tli id="T2313" time="130.861" type="appl" />
         <tli id="T2314" time="131.272" type="appl" />
         <tli id="T2315" time="131.682" type="appl" />
         <tli id="T2316" time="132.589" type="appl" />
         <tli id="T2317" time="133.495" type="appl" />
         <tli id="T2318" time="135.99844718030184" />
         <tli id="T2319" time="137.51842982525815" />
         <tli id="T2320" time="137.975" type="appl" />
         <tli id="T2321" time="138.682" type="appl" />
         <tli id="T2322" time="139.39" type="appl" />
         <tli id="T2323" time="140.097" type="appl" />
         <tli id="T2324" time="141.478" type="appl" />
         <tli id="T2325" time="141.999" type="appl" />
         <tli id="T2326" time="142.519" type="appl" />
         <tli id="T2327" time="143.04" type="appl" />
         <tli id="T2328" time="143.428" type="appl" />
         <tli id="T2329" time="143.815" type="appl" />
         <tli id="T2330" time="144.203" type="appl" />
         <tli id="T2331" time="144.59" type="appl" />
         <tli id="T2332" time="144.978" type="appl" />
         <tli id="T2333" time="146.034" type="appl" />
         <tli id="T2334" time="146.553" type="appl" />
         <tli id="T2335" time="147.072" type="appl" />
         <tli id="T2336" time="147.944" type="appl" />
         <tli id="T2337" time="148.693" type="appl" />
         <tli id="T2338" time="149.442" type="appl" />
         <tli id="T2339" time="150.191" type="appl" />
         <tli id="T2340" time="150.941" type="appl" />
         <tli id="T2341" time="151.69" type="appl" />
         <tli id="T2342" time="152.439" type="appl" />
         <tli id="T2343" time="153.188" type="appl" />
         <tli id="T2344" time="154.28490505361398" />
         <tli id="T2345" time="155.175" type="appl" />
         <tli id="T2346" time="155.796" type="appl" />
         <tli id="T2347" time="156.417" type="appl" />
         <tli id="T2348" time="157.452" type="appl" />
         <tli id="T2349" time="159.2848479646545" />
         <tli id="T2350" time="160.252" type="appl" />
         <tli id="T2351" time="161.17" type="appl" />
         <tli id="T2352" time="161.778" type="appl" />
         <tli id="T2353" time="162.386" type="appl" />
         <tli id="T2354" time="162.994" type="appl" />
         <tli id="T2355" time="163.488" type="appl" />
         <tli id="T2356" time="163.983" type="appl" />
         <tli id="T2357" time="164.477" type="appl" />
         <tli id="T2358" time="164.971" type="appl" />
         <tli id="T2359" time="165.833" type="appl" />
         <tli id="T2360" time="166.352" type="appl" />
         <tli id="T2361" time="166.87" type="appl" />
         <tli id="T2362" time="167.389" type="appl" />
         <tli id="T2363" time="168.274" type="appl" />
         <tli id="T2364" time="169.159" type="appl" />
         <tli id="T2365" time="170.35805486497222" />
         <tli id="T2366" time="171.002" type="appl" />
         <tli id="T2367" time="171.939" type="appl" />
         <tli id="T2368" time="173.16468948570292" />
         <tli id="T2369" time="173.758" type="appl" />
         <tli id="T2370" time="174.334" type="appl" />
         <tli id="T2371" time="174.909" type="appl" />
         <tli id="T2372" time="176.1579886417792" />
         <tli id="T2373" time="177.651" type="appl" />
         <tli id="T2374" time="180.66460385226367" />
         <tli id="T2375" time="181.612" type="appl" />
         <tli id="T2376" time="182.57" type="appl" />
         <tli id="T2377" time="183.48" type="appl" />
         <tli id="T2378" time="184.263" type="appl" />
         <tli id="T2379" time="185.046" type="appl" />
         <tli id="T2380" time="185.828" type="appl" />
         <tli id="T2381" time="186.61" type="appl" />
         <tli id="T2382" time="188.9311761318507" />
         <tli id="T2383" time="189.331" type="appl" />
         <tli id="T2384" time="190.504" type="appl" />
         <tli id="T2385" time="191.678" type="appl" />
         <tli id="T2386" time="192.852" type="appl" />
         <tli id="T2387" time="193.336" type="appl" />
         <tli id="T2388" time="193.82" type="appl" />
         <tli id="T2389" time="194.304" type="appl" />
         <tli id="T2390" time="194.788" type="appl" />
         <tli id="T2391" time="196.02442847498014" />
         <tli id="T2392" time="196.713" type="appl" />
         <tli id="T2393" time="197.391" type="appl" />
         <tli id="T2394" time="198.07" type="appl" />
         <tli id="T2395" time="199.033" type="appl" />
         <tli id="T2396" time="199.965" type="appl" />
         <tli id="T2397" time="200.897" type="appl" />
         <tli id="T2398" time="201.829" type="appl" />
         <tli id="T2399" time="202.85101719618746" />
         <tli id="T2400" time="204.469" type="appl" />
         <tli id="T2401" time="206.15" type="appl" />
         <tli id="T2402" time="206.906" type="appl" />
         <tli id="T2403" time="207.662" type="appl" />
         <tli id="T2404" time="208.27" type="appl" />
         <tli id="T2405" time="208.99761368149325" />
         <tli id="T2406" time="209.831" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2406" id="Seg_0" n="sc" s="T2145">
               <ts e="T2148" id="Seg_2" n="HIAT:u" s="T2145">
                  <ts e="T2146" id="Seg_4" n="HIAT:w" s="T2145">Amnolaʔpi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2147" id="Seg_7" n="HIAT:w" s="T2146">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2148" id="Seg_11" n="HIAT:w" s="T2147">büzʼe</ts>
                  <nts id="Seg_12" n="HIAT:ip">.</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2152" id="Seg_15" n="HIAT:u" s="T2148">
                  <ts e="T2149" id="Seg_17" n="HIAT:w" s="T2148">Dĭn</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2150" id="Seg_20" n="HIAT:w" s="T2149">nagur</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2151" id="Seg_23" n="HIAT:w" s="T2150">koʔbdo</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2152" id="Seg_26" n="HIAT:w" s="T2151">ibi</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2160" id="Seg_30" n="HIAT:u" s="T2152">
                  <ts e="T2153" id="Seg_32" n="HIAT:w" s="T2152">Dĭ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2154" id="Seg_35" n="HIAT:w" s="T2153">kambi</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_37" n="HIAT:ip">(</nts>
                  <ts e="T2155" id="Seg_39" n="HIAT:w" s="T2154">a-</ts>
                  <nts id="Seg_40" n="HIAT:ip">)</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2156" id="Seg_43" n="HIAT:w" s="T2155">anbardə</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2157" id="Seg_47" n="HIAT:w" s="T2156">krupa</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2158" id="Seg_50" n="HIAT:w" s="T2157">ibi</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2159" id="Seg_53" n="HIAT:w" s="T2158">băranə</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2160" id="Seg_57" n="HIAT:w" s="T2159">šobi</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2164" id="Seg_61" n="HIAT:u" s="T2160">
                  <ts e="T2161" id="Seg_63" n="HIAT:w" s="T2160">A</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2162" id="Seg_66" n="HIAT:w" s="T2161">băragən</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2163" id="Seg_69" n="HIAT:w" s="T2162">ši</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2164" id="Seg_72" n="HIAT:w" s="T2163">ibi</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2170" id="Seg_76" n="HIAT:u" s="T2164">
                  <ts e="T2165" id="Seg_78" n="HIAT:w" s="T2164">Šonəbi</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2166" id="Seg_82" n="HIAT:w" s="T2165">šonəbi</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2167" id="Seg_86" n="HIAT:w" s="T2166">a</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2168" id="Seg_89" n="HIAT:w" s="T2167">krupat</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2169" id="Seg_92" n="HIAT:w" s="T2168">bar</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2170" id="Seg_95" n="HIAT:w" s="T2169">kamluʔpi</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2175" id="Seg_99" n="HIAT:u" s="T2170">
                  <ts e="T2171" id="Seg_101" n="HIAT:w" s="T2170">Nüket</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2172" id="Seg_104" n="HIAT:w" s="T2171">măndə:</ts>
                  <nts id="Seg_105" n="HIAT:ip">"</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2173" id="Seg_108" n="HIAT:w" s="T2172">Gijen</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2174" id="Seg_111" n="HIAT:w" s="T2173">tăn</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2175" id="Seg_114" n="HIAT:w" s="T2174">krupa</ts>
                  <nts id="Seg_115" n="HIAT:ip">?</nts>
                  <nts id="Seg_116" n="HIAT:ip">"</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2179" id="Seg_119" n="HIAT:u" s="T2175">
                  <nts id="Seg_120" n="HIAT:ip">"</nts>
                  <ts e="T2176" id="Seg_122" n="HIAT:w" s="T2175">Da</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2177" id="Seg_125" n="HIAT:w" s="T2176">kamluʔpiam</ts>
                  <nts id="Seg_126" n="HIAT:ip">"</nts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2178" id="Seg_130" n="HIAT:w" s="T2177">dĭgəttə</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2179" id="Seg_133" n="HIAT:w" s="T2178">kambi</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2182" id="Seg_137" n="HIAT:u" s="T2179">
                  <ts e="T2180" id="Seg_139" n="HIAT:w" s="T2179">Măndə:</ts>
                  <nts id="Seg_140" n="HIAT:ip">"</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2181" id="Seg_143" n="HIAT:w" s="T2180">Kuja</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2182" id="Seg_146" n="HIAT:w" s="T2181">kabažəraʔ</ts>
                  <nts id="Seg_147" n="HIAT:ip">!</nts>
                  <nts id="Seg_148" n="HIAT:ip">"</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2185" id="Seg_151" n="HIAT:u" s="T2182">
                  <ts e="T2183" id="Seg_153" n="HIAT:w" s="T2182">Mesʼatsdə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2184" id="Seg_156" n="HIAT:w" s="T2183">măndə:</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2185" id="Seg_160" n="HIAT:w" s="T2184">Kabažəraʔ</ts>
                  <nts id="Seg_161" n="HIAT:ip">!</nts>
                  <nts id="Seg_162" n="HIAT:ip">"</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2190" id="Seg_165" n="HIAT:u" s="T2185">
                  <ts e="T2186" id="Seg_167" n="HIAT:w" s="T2185">I</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_169" n="HIAT:ip">(</nts>
                  <ts e="T2187" id="Seg_171" n="HIAT:w" s="T2186">voron=</ts>
                  <nts id="Seg_172" n="HIAT:ip">)</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2188" id="Seg_175" n="HIAT:w" s="T2187">vorondə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2189" id="Seg_178" n="HIAT:w" s="T2188">măndə:</ts>
                  <nts id="Seg_179" n="HIAT:ip">"</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2190" id="Seg_182" n="HIAT:w" s="T2189">Kabažəraʔ</ts>
                  <nts id="Seg_183" n="HIAT:ip">!</nts>
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2192" id="Seg_187" n="HIAT:u" s="T2190">
                  <ts e="T2191" id="Seg_189" n="HIAT:w" s="T2190">Dĭzeŋ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2192" id="Seg_192" n="HIAT:w" s="T2191">šobiʔi</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2196" id="Seg_196" n="HIAT:u" s="T2192">
                  <ts e="T2193" id="Seg_198" n="HIAT:w" s="T2192">I</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_200" n="HIAT:ip">(</nts>
                  <ts e="T2194" id="Seg_202" n="HIAT:w" s="T2193">ka-</ts>
                  <nts id="Seg_203" n="HIAT:ip">)</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2195" id="Seg_206" n="HIAT:w" s="T2194">kabažərbiʔi</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2196" id="Seg_209" n="HIAT:w" s="T2195">dĭ</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2201" id="Seg_213" n="HIAT:u" s="T2196">
                  <nts id="Seg_214" n="HIAT:ip">"</nts>
                  <ts e="T2197" id="Seg_216" n="HIAT:w" s="T2196">Măn</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2198" id="Seg_219" n="HIAT:w" s="T2197">koʔbsaŋdə</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2199" id="Seg_222" n="HIAT:w" s="T2198">šiʔnʼileʔ</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2200" id="Seg_225" n="HIAT:w" s="T2199">mĭlem</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2201" id="Seg_228" n="HIAT:w" s="T2200">tibinə</ts>
                  <nts id="Seg_229" n="HIAT:ip">"</nts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2203" id="Seg_233" n="HIAT:u" s="T2201">
                  <ts e="T2202" id="Seg_235" n="HIAT:w" s="T2201">Dĭgəttə</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2203" id="Seg_238" n="HIAT:w" s="T2202">šobi</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2207" id="Seg_242" n="HIAT:u" s="T2203">
                  <ts e="T2204" id="Seg_244" n="HIAT:w" s="T2203">Deppi</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2205" id="Seg_247" n="HIAT:w" s="T2204">dĭ</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2206" id="Seg_250" n="HIAT:w" s="T2205">krupam</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2207" id="Seg_253" n="HIAT:w" s="T2206">nükendə</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2215" id="Seg_257" n="HIAT:u" s="T2207">
                  <ts e="T2208" id="Seg_259" n="HIAT:w" s="T2207">Dĭgəttə</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2209" id="Seg_262" n="HIAT:w" s="T2208">onʼiʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2210" id="Seg_265" n="HIAT:w" s="T2209">urgo</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2211" id="Seg_268" n="HIAT:w" s="T2210">koʔbdonə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2212" id="Seg_271" n="HIAT:w" s="T2211">măndə:</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_273" n="HIAT:ip">"</nts>
                  <ts e="T2213" id="Seg_275" n="HIAT:w" s="T2212">Šereʔ</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2214" id="Seg_278" n="HIAT:w" s="T2213">oldʼa</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2215" id="Seg_281" n="HIAT:w" s="T2214">kuvas</ts>
                  <nts id="Seg_282" n="HIAT:ip">"</nts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2220" id="Seg_286" n="HIAT:u" s="T2215">
                  <ts e="T2216" id="Seg_288" n="HIAT:w" s="T2215">Dĭ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2217" id="Seg_291" n="HIAT:w" s="T2216">šerbi</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2218" id="Seg_294" n="HIAT:w" s="T2217">da</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2219" id="Seg_297" n="HIAT:w" s="T2218">kambi</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2220" id="Seg_300" n="HIAT:w" s="T2219">nʼiʔdə</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2223" id="Seg_304" n="HIAT:u" s="T2220">
                  <ts e="T2221" id="Seg_306" n="HIAT:w" s="T2220">Dĭm</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2222" id="Seg_309" n="HIAT:w" s="T2221">iluʔpi</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2223" id="Seg_312" n="HIAT:w" s="T2222">kuja</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2226" id="Seg_316" n="HIAT:u" s="T2223">
                  <ts e="T2224" id="Seg_318" n="HIAT:w" s="T2223">Dĭgəttə</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2225" id="Seg_321" n="HIAT:w" s="T2224">baška</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_323" n="HIAT:ip">(</nts>
                  <ts e="T2226" id="Seg_325" n="HIAT:w" s="T2225">koʔbdotsiʔ</ts>
                  <nts id="Seg_326" n="HIAT:ip">)</nts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2232" id="Seg_330" n="HIAT:u" s="T2226">
                  <nts id="Seg_331" n="HIAT:ip">"</nts>
                  <ts e="T2227" id="Seg_333" n="HIAT:w" s="T2226">Šereʔ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2228" id="Seg_336" n="HIAT:w" s="T2227">oldʼa</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2229" id="Seg_339" n="HIAT:w" s="T2228">kuvas</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2230" id="Seg_342" n="HIAT:w" s="T2229">i</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2231" id="Seg_345" n="HIAT:w" s="T2230">kanaʔ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2232" id="Seg_348" n="HIAT:w" s="T2231">nʼiʔdə</ts>
                  <nts id="Seg_349" n="HIAT:ip">"</nts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2234" id="Seg_353" n="HIAT:u" s="T2232">
                  <ts e="T2233" id="Seg_355" n="HIAT:w" s="T2232">Dĭ</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2234" id="Seg_358" n="HIAT:w" s="T2233">kambi</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2237" id="Seg_362" n="HIAT:u" s="T2234">
                  <ts e="T2235" id="Seg_364" n="HIAT:w" s="T2234">Dĭm</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2236" id="Seg_367" n="HIAT:w" s="T2235">mesʼats</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2237" id="Seg_370" n="HIAT:w" s="T2236">ibi</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2247" id="Seg_374" n="HIAT:u" s="T2237">
                  <ts e="T2238" id="Seg_376" n="HIAT:w" s="T2237">Dĭgəttə</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2239" id="Seg_379" n="HIAT:w" s="T2238">nagurgo</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2240" id="Seg_382" n="HIAT:w" s="T2239">koʔbdonə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2241" id="Seg_385" n="HIAT:w" s="T2240">măndə:</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_387" n="HIAT:ip">"</nts>
                  <ts e="T2242" id="Seg_389" n="HIAT:w" s="T2241">Šereʔ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2243" id="Seg_392" n="HIAT:w" s="T2242">oldʼa</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2244" id="Seg_395" n="HIAT:w" s="T2243">kuvas</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2245" id="Seg_398" n="HIAT:w" s="T2244">i</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2246" id="Seg_401" n="HIAT:w" s="T2245">kanaʔ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2247" id="Seg_404" n="HIAT:w" s="T2246">nʼiʔdə</ts>
                  <nts id="Seg_405" n="HIAT:ip">"</nts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2249" id="Seg_409" n="HIAT:u" s="T2247">
                  <ts e="T2248" id="Seg_411" n="HIAT:w" s="T2247">Dĭ</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2249" id="Seg_414" n="HIAT:w" s="T2248">kambi</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2253" id="Seg_418" n="HIAT:u" s="T2249">
                  <ts e="T2250" id="Seg_420" n="HIAT:w" s="T2249">Dĭm</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2251" id="Seg_423" n="HIAT:w" s="T2250">tože</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2252" id="Seg_426" n="HIAT:w" s="T2251">iluʔpi</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2253" id="Seg_429" n="HIAT:w" s="T2252">voron</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2263" id="Seg_433" n="HIAT:u" s="T2253">
                  <ts e="T2254" id="Seg_435" n="HIAT:w" s="T2253">Dĭgəttə</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2255" id="Seg_438" n="HIAT:w" s="T2254">dĭ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_440" n="HIAT:ip">(</nts>
                  <ts e="T2256" id="Seg_442" n="HIAT:w" s="T2255">bü-</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2257" id="Seg_445" n="HIAT:w" s="T2256">dĭ</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2258" id="Seg_448" n="HIAT:w" s="T2257">kuʔ-</ts>
                  <nts id="Seg_449" n="HIAT:ip">)</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2259" id="Seg_452" n="HIAT:w" s="T2258">büzʼe</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2260" id="Seg_455" n="HIAT:w" s="T2259">măndə:</ts>
                  <nts id="Seg_456" n="HIAT:ip">"</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2261" id="Seg_459" n="HIAT:w" s="T2260">Kallam</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2262" id="Seg_462" n="HIAT:w" s="T2261">koʔbdonə</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2263" id="Seg_465" n="HIAT:w" s="T2262">jadajlaʔ</ts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2264" id="Seg_470" n="HIAT:u" s="T2263">
                  <ts e="T2264" id="Seg_472" n="HIAT:w" s="T2263">Šobi</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2267" id="Seg_476" n="HIAT:u" s="T2264">
                  <nts id="Seg_477" n="HIAT:ip">"</nts>
                  <ts e="T2265" id="Seg_479" n="HIAT:w" s="T2264">Ĭmbiziʔ</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2266" id="Seg_482" n="HIAT:w" s="T2265">tănan</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2267" id="Seg_485" n="HIAT:w" s="T2266">bădəsʼtə</ts>
                  <nts id="Seg_486" n="HIAT:ip">?</nts>
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2273" id="Seg_490" n="HIAT:u" s="T2267">
                  <nts id="Seg_491" n="HIAT:ip">"</nts>
                  <ts e="T2268" id="Seg_493" n="HIAT:w" s="T2267">Da</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <ts e="T2269" id="Seg_497" n="HIAT:w" s="T2268">măn=</ts>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2270" id="Seg_501" n="HIAT:w" s="T2269">măn</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2271" id="Seg_504" n="HIAT:w" s="T2270">nʼe</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2272" id="Seg_507" n="HIAT:w" s="T2271">axota</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2273" id="Seg_510" n="HIAT:w" s="T2272">amzittə</ts>
                  <nts id="Seg_511" n="HIAT:ip">"</nts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2282" id="Seg_515" n="HIAT:u" s="T2273">
                  <ts e="T2274" id="Seg_517" n="HIAT:w" s="T2273">Dĭgəttə</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2275" id="Seg_520" n="HIAT:w" s="T2274">dĭ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2276" id="Seg_523" n="HIAT:w" s="T2275">aladʼaʔi</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_525" n="HIAT:ip">(</nts>
                  <ts e="T2277" id="Seg_527" n="HIAT:w" s="T2276">da</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2278" id="Seg_530" n="HIAT:w" s="T2277">bə-</ts>
                  <nts id="Seg_531" n="HIAT:ip">)</nts>
                  <nts id="Seg_532" n="HIAT:ip">,</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2279" id="Seg_535" n="HIAT:w" s="T2278">koʔbdot</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2280" id="Seg_538" n="HIAT:w" s="T2279">aladʼaʔi</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2281" id="Seg_541" n="HIAT:w" s="T2280">embi</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2282" id="Seg_545" n="HIAT:w" s="T2281">skavaradandə</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2286" id="Seg_549" n="HIAT:u" s="T2282">
                  <ts e="T2283" id="Seg_551" n="HIAT:w" s="T2282">Nuldəbi</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2284" id="Seg_554" n="HIAT:w" s="T2283">sontsənə</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2285" id="Seg_558" n="HIAT:w" s="T2284">dĭ</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2286" id="Seg_561" n="HIAT:w" s="T2285">pürluʔpiʔi</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2292" id="Seg_565" n="HIAT:u" s="T2286">
                  <ts e="T2287" id="Seg_567" n="HIAT:w" s="T2286">Dĭ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2288" id="Seg_570" n="HIAT:w" s="T2287">büzʼe</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2289" id="Seg_573" n="HIAT:w" s="T2288">ambi</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2290" id="Seg_576" n="HIAT:w" s="T2289">i</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2291" id="Seg_579" n="HIAT:w" s="T2290">šobi</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2292" id="Seg_582" n="HIAT:w" s="T2291">maːʔndə</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2296" id="Seg_586" n="HIAT:u" s="T2292">
                  <ts e="T2293" id="Seg_588" n="HIAT:w" s="T2292">Nükendə</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2294" id="Seg_591" n="HIAT:w" s="T2293">măndə:</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_593" n="HIAT:ip">"</nts>
                  <ts e="T2295" id="Seg_595" n="HIAT:w" s="T2294">Pürəʔ</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2296" id="Seg_598" n="HIAT:w" s="T2295">aladʼaʔi</ts>
                  <nts id="Seg_599" n="HIAT:ip">!</nts>
                  <nts id="Seg_600" n="HIAT:ip">"</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2299" id="Seg_603" n="HIAT:u" s="T2296">
                  <ts e="T2297" id="Seg_605" n="HIAT:w" s="T2296">Dĭ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2298" id="Seg_608" n="HIAT:w" s="T2297">davaj</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2299" id="Seg_611" n="HIAT:w" s="T2298">pürzittə</ts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2306" id="Seg_615" n="HIAT:u" s="T2299">
                  <ts e="T2300" id="Seg_617" n="HIAT:w" s="T2299">I</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_619" n="HIAT:ip">(</nts>
                  <ts e="T2301" id="Seg_621" n="HIAT:w" s="T2300">aŋdə</ts>
                  <nts id="Seg_622" n="HIAT:ip">)</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2302" id="Seg_625" n="HIAT:w" s="T2301">nuldəbi</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2303" id="Seg_629" n="HIAT:w" s="T2302">nulaʔbiʔi</ts>
                  <nts id="Seg_630" n="HIAT:ip">,</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2304" id="Seg_633" n="HIAT:w" s="T2303">nulaʔbiʔi</ts>
                  <nts id="Seg_634" n="HIAT:ip">,</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2305" id="Seg_637" n="HIAT:w" s="T2304">ej</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2306" id="Seg_640" n="HIAT:w" s="T2305">pürbiʔi</ts>
                  <nts id="Seg_641" n="HIAT:ip">.</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2310" id="Seg_644" n="HIAT:u" s="T2306">
                  <ts e="T2307" id="Seg_646" n="HIAT:w" s="T2306">Dĭgəttə</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2308" id="Seg_649" n="HIAT:w" s="T2307">dĭ</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2309" id="Seg_652" n="HIAT:w" s="T2308">pʼešdə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2310" id="Seg_655" n="HIAT:w" s="T2309">embi</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2312" id="Seg_659" n="HIAT:u" s="T2310">
                  <ts e="T2311" id="Seg_661" n="HIAT:w" s="T2310">Dĭn</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2312" id="Seg_664" n="HIAT:w" s="T2311">pürbi</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2315" id="Seg_668" n="HIAT:u" s="T2312">
                  <ts e="T2313" id="Seg_670" n="HIAT:w" s="T2312">I</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2314" id="Seg_673" n="HIAT:w" s="T2313">büzʼe</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2315" id="Seg_676" n="HIAT:w" s="T2314">ambi</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2318" id="Seg_680" n="HIAT:u" s="T2315">
                  <ts e="T2316" id="Seg_682" n="HIAT:w" s="T2315">Dĭgəttə</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2317" id="Seg_685" n="HIAT:w" s="T2316">kandəga</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2318" id="Seg_688" n="HIAT:w" s="T2317">mesʼatsdə</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2319" id="Seg_692" n="HIAT:u" s="T2318">
                  <ts e="T2319" id="Seg_694" n="HIAT:w" s="T2318">Šobi</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2323" id="Seg_698" n="HIAT:u" s="T2319">
                  <ts e="T2320" id="Seg_700" n="HIAT:w" s="T2319">A</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2321" id="Seg_703" n="HIAT:w" s="T2320">dĭ</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2322" id="Seg_706" n="HIAT:w" s="T2321">moltʼa</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2323" id="Seg_709" n="HIAT:w" s="T2322">nendəbi</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2327" id="Seg_713" n="HIAT:u" s="T2323">
                  <ts e="T2324" id="Seg_715" n="HIAT:w" s="T2323">Măndə:</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_717" n="HIAT:ip">"</nts>
                  <ts e="T2325" id="Seg_719" n="HIAT:w" s="T2324">No</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2326" id="Seg_723" n="HIAT:w" s="T2325">kanaʔ</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2327" id="Seg_726" n="HIAT:w" s="T2326">moltʼanə</ts>
                  <nts id="Seg_727" n="HIAT:ip">"</nts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2332" id="Seg_731" n="HIAT:u" s="T2327">
                  <nts id="Seg_732" n="HIAT:ip">"</nts>
                  <ts e="T2328" id="Seg_734" n="HIAT:w" s="T2327">A</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2329" id="Seg_737" n="HIAT:w" s="T2328">dĭn</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2330" id="Seg_740" n="HIAT:w" s="T2329">ĭmbidə</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2331" id="Seg_743" n="HIAT:w" s="T2330">ej</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2332" id="Seg_746" n="HIAT:w" s="T2331">edlia</ts>
                  <nts id="Seg_747" n="HIAT:ip">"</nts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2333" id="Seg_751" n="HIAT:u" s="T2332">
                  <nts id="Seg_752" n="HIAT:ip">"</nts>
                  <ts e="T2333" id="Seg_754" n="HIAT:w" s="T2332">Kanaʔ</ts>
                  <nts id="Seg_755" n="HIAT:ip">"</nts>
                  <nts id="Seg_756" n="HIAT:ip">.</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2335" id="Seg_759" n="HIAT:u" s="T2333">
                  <ts e="T2334" id="Seg_761" n="HIAT:w" s="T2333">Dĭ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2335" id="Seg_764" n="HIAT:w" s="T2334">kambi</ts>
                  <nts id="Seg_765" n="HIAT:ip">.</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2344" id="Seg_768" n="HIAT:u" s="T2335">
                  <ts e="T2336" id="Seg_770" n="HIAT:w" s="T2335">Dĭ</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2337" id="Seg_773" n="HIAT:w" s="T2336">müjəbə</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2338" id="Seg_776" n="HIAT:w" s="T2337">dibər</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2339" id="Seg_779" n="HIAT:w" s="T2338">paʔluʔpi</ts>
                  <nts id="Seg_780" n="HIAT:ip">,</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2340" id="Seg_783" n="HIAT:w" s="T2339">dĭ</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_785" n="HIAT:ip">(</nts>
                  <ts e="T2341" id="Seg_787" n="HIAT:w" s="T2340">băzajdəbi</ts>
                  <nts id="Seg_788" n="HIAT:ip">)</nts>
                  <nts id="Seg_789" n="HIAT:ip">,</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2342" id="Seg_792" n="HIAT:w" s="T2341">dĭn</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2343" id="Seg_795" n="HIAT:w" s="T2342">bar</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2344" id="Seg_798" n="HIAT:w" s="T2343">edlaʔbə</ts>
                  <nts id="Seg_799" n="HIAT:ip">.</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2347" id="Seg_802" n="HIAT:u" s="T2344">
                  <ts e="T2345" id="Seg_804" n="HIAT:w" s="T2344">Dĭgəttə</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2346" id="Seg_807" n="HIAT:w" s="T2345">maːndə</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2347" id="Seg_810" n="HIAT:w" s="T2346">šobi</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2349" id="Seg_814" n="HIAT:u" s="T2347">
                  <nts id="Seg_815" n="HIAT:ip">"</nts>
                  <ts e="T2348" id="Seg_817" n="HIAT:w" s="T2347">Nendəʔ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2349" id="Seg_820" n="HIAT:w" s="T2348">moltʼa</ts>
                  <nts id="Seg_821" n="HIAT:ip">!</nts>
                  <nts id="Seg_822" n="HIAT:ip">"</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2351" id="Seg_825" n="HIAT:u" s="T2349">
                  <ts e="T2350" id="Seg_827" n="HIAT:w" s="T2349">Nüket</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2351" id="Seg_830" n="HIAT:w" s="T2350">nendəbi</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2354" id="Seg_834" n="HIAT:u" s="T2351">
                  <nts id="Seg_835" n="HIAT:ip">"</nts>
                  <ts e="T2352" id="Seg_837" n="HIAT:w" s="T2351">No</ts>
                  <nts id="Seg_838" n="HIAT:ip">,</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_840" n="HIAT:ip">(</nts>
                  <ts e="T2353" id="Seg_842" n="HIAT:w" s="T2352">m-</ts>
                  <nts id="Seg_843" n="HIAT:ip">)</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2354" id="Seg_846" n="HIAT:w" s="T2353">băzaʔ</ts>
                  <nts id="Seg_847" n="HIAT:ip">!</nts>
                  <nts id="Seg_848" n="HIAT:ip">"</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2358" id="Seg_851" n="HIAT:u" s="T2354">
                  <nts id="Seg_852" n="HIAT:ip">"</nts>
                  <ts e="T2355" id="Seg_854" n="HIAT:w" s="T2354">Da</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2356" id="Seg_857" n="HIAT:w" s="T2355">dĭn</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2357" id="Seg_860" n="HIAT:w" s="T2356">šü</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2358" id="Seg_863" n="HIAT:w" s="T2357">naga</ts>
                  <nts id="Seg_864" n="HIAT:ip">"</nts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2359" id="Seg_868" n="HIAT:u" s="T2358">
                  <nts id="Seg_869" n="HIAT:ip">"</nts>
                  <ts e="T2359" id="Seg_871" n="HIAT:w" s="T2358">Kanaʔ</ts>
                  <nts id="Seg_872" n="HIAT:ip">"</nts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2362" id="Seg_876" n="HIAT:u" s="T2359">
                  <ts e="T2360" id="Seg_878" n="HIAT:w" s="T2359">Dĭ</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2361" id="Seg_881" n="HIAT:w" s="T2360">ši</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2362" id="Seg_884" n="HIAT:w" s="T2361">abi</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2365" id="Seg_888" n="HIAT:u" s="T2362">
                  <ts e="T2363" id="Seg_890" n="HIAT:w" s="T2362">Dibər</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2364" id="Seg_893" n="HIAT:w" s="T2363">müjət</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2365" id="Seg_896" n="HIAT:w" s="T2364">paʔdəbi</ts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2368" id="Seg_900" n="HIAT:u" s="T2365">
                  <ts e="T2366" id="Seg_902" n="HIAT:w" s="T2365">Nüket</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2367" id="Seg_905" n="HIAT:w" s="T2366">uge</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2368" id="Seg_908" n="HIAT:w" s="T2367">kirgarlaʔbə</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2372" id="Seg_912" n="HIAT:u" s="T2368">
                  <nts id="Seg_913" n="HIAT:ip">"</nts>
                  <ts e="T2369" id="Seg_915" n="HIAT:w" s="T2368">Naga</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2370" id="Seg_918" n="HIAT:w" s="T2369">šü</ts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2371" id="Seg_922" n="HIAT:w" s="T2370">naga</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2372" id="Seg_925" n="HIAT:w" s="T2371">šü</ts>
                  <nts id="Seg_926" n="HIAT:ip">!</nts>
                  <nts id="Seg_927" n="HIAT:ip">"</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2374" id="Seg_930" n="HIAT:u" s="T2372">
                  <ts e="T2373" id="Seg_932" n="HIAT:w" s="T2372">Dĭgəttə</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2374" id="Seg_935" n="HIAT:w" s="T2373">băzajdəbiʔi</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2376" id="Seg_939" n="HIAT:u" s="T2374">
                  <ts e="T2375" id="Seg_941" n="HIAT:w" s="T2374">Šü</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2376" id="Seg_944" n="HIAT:w" s="T2375">nendəbiʔi</ts>
                  <nts id="Seg_945" n="HIAT:ip">.</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2382" id="Seg_948" n="HIAT:u" s="T2376">
                  <ts e="T2377" id="Seg_950" n="HIAT:w" s="T2376">Dĭgəttə</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2378" id="Seg_953" n="HIAT:w" s="T2377">erten</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2379" id="Seg_956" n="HIAT:w" s="T2378">uʔlaʔbə</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_959" n="HIAT:ip">(</nts>
                  <ts e="T2380" id="Seg_961" n="HIAT:w" s="T2379">ka-</ts>
                  <nts id="Seg_962" n="HIAT:ip">,</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2381" id="Seg_965" n="HIAT:w" s="T2380">ka-</ts>
                  <nts id="Seg_966" n="HIAT:ip">)</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2382" id="Seg_969" n="HIAT:w" s="T2381">kambi</ts>
                  <nts id="Seg_970" n="HIAT:ip">.</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2386" id="Seg_973" n="HIAT:u" s="T2382">
                  <ts e="T2383" id="Seg_975" n="HIAT:w" s="T2382">Baška</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2384" id="Seg_978" n="HIAT:w" s="T2383">koʔbdonə</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2385" id="Seg_981" n="HIAT:w" s="T2384">dĭn</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2386" id="Seg_984" n="HIAT:w" s="T2385">vorondə</ts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2391" id="Seg_988" n="HIAT:u" s="T2386">
                  <ts e="T2387" id="Seg_990" n="HIAT:w" s="T2386">Dĭn</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2388" id="Seg_993" n="HIAT:w" s="T2387">măndə:</ts>
                  <nts id="Seg_994" n="HIAT:ip">"</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2389" id="Seg_997" n="HIAT:w" s="T2388">Ĭmbi</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2390" id="Seg_1000" n="HIAT:w" s="T2389">tănan</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2391" id="Seg_1003" n="HIAT:w" s="T2390">mĭzittə</ts>
                  <nts id="Seg_1004" n="HIAT:ip">?</nts>
                  <nts id="Seg_1005" n="HIAT:ip">"</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2394" id="Seg_1008" n="HIAT:u" s="T2391">
                  <nts id="Seg_1009" n="HIAT:ip">"</nts>
                  <ts e="T2392" id="Seg_1011" n="HIAT:w" s="T2391">Nu</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2393" id="Seg_1014" n="HIAT:w" s="T2392">davaj</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2394" id="Seg_1017" n="HIAT:w" s="T2393">kunolzittə</ts>
                  <nts id="Seg_1018" n="HIAT:ip">"</nts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2399" id="Seg_1022" n="HIAT:u" s="T2394">
                  <nts id="Seg_1023" n="HIAT:ip">(</nts>
                  <ts e="T2395" id="Seg_1025" n="HIAT:w" s="T2394">Dĭ=</ts>
                  <nts id="Seg_1026" n="HIAT:ip">)</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2396" id="Seg_1029" n="HIAT:w" s="T2395">Dĭ</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2397" id="Seg_1032" n="HIAT:w" s="T2396">ibi</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1034" n="HIAT:ip">(</nts>
                  <ts e="T2398" id="Seg_1036" n="HIAT:w" s="T2397">dĭm</ts>
                  <nts id="Seg_1037" n="HIAT:ip">)</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1039" n="HIAT:ip">(</nts>
                  <ts e="T2399" id="Seg_1041" n="HIAT:w" s="T2398">krɨlondə</ts>
                  <nts id="Seg_1042" n="HIAT:ip">)</nts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2401" id="Seg_1046" n="HIAT:u" s="T2399">
                  <ts e="T2400" id="Seg_1048" n="HIAT:w" s="T2399">Šidegöʔ</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2401" id="Seg_1051" n="HIAT:w" s="T2400">kunolluʔpiʔi</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2403" id="Seg_1055" n="HIAT:u" s="T2401">
                  <ts e="T2402" id="Seg_1057" n="HIAT:w" s="T2401">I</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2403" id="Seg_1060" n="HIAT:w" s="T2402">uzleʔpiʔi</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2405" id="Seg_1064" n="HIAT:u" s="T2403">
                  <ts e="T2404" id="Seg_1066" n="HIAT:w" s="T2403">I</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2405" id="Seg_1069" n="HIAT:w" s="T2404">külaːmbiʔi</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2406" id="Seg_1073" n="HIAT:u" s="T2405">
                  <ts e="T2406" id="Seg_1075" n="HIAT:w" s="T2405">Kabarləj</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2406" id="Seg_1078" n="sc" s="T2145">
               <ts e="T2146" id="Seg_1080" n="e" s="T2145">Amnolaʔpi </ts>
               <ts e="T2147" id="Seg_1082" n="e" s="T2146">nüke, </ts>
               <ts e="T2148" id="Seg_1084" n="e" s="T2147">büzʼe. </ts>
               <ts e="T2149" id="Seg_1086" n="e" s="T2148">Dĭn </ts>
               <ts e="T2150" id="Seg_1088" n="e" s="T2149">nagur </ts>
               <ts e="T2151" id="Seg_1090" n="e" s="T2150">koʔbdo </ts>
               <ts e="T2152" id="Seg_1092" n="e" s="T2151">ibi. </ts>
               <ts e="T2153" id="Seg_1094" n="e" s="T2152">Dĭ </ts>
               <ts e="T2154" id="Seg_1096" n="e" s="T2153">kambi </ts>
               <ts e="T2155" id="Seg_1098" n="e" s="T2154">(a-) </ts>
               <ts e="T2156" id="Seg_1100" n="e" s="T2155">anbardə, </ts>
               <ts e="T2157" id="Seg_1102" n="e" s="T2156">krupa </ts>
               <ts e="T2158" id="Seg_1104" n="e" s="T2157">ibi </ts>
               <ts e="T2159" id="Seg_1106" n="e" s="T2158">băranə, </ts>
               <ts e="T2160" id="Seg_1108" n="e" s="T2159">šobi. </ts>
               <ts e="T2161" id="Seg_1110" n="e" s="T2160">A </ts>
               <ts e="T2162" id="Seg_1112" n="e" s="T2161">băragən </ts>
               <ts e="T2163" id="Seg_1114" n="e" s="T2162">ši </ts>
               <ts e="T2164" id="Seg_1116" n="e" s="T2163">ibi. </ts>
               <ts e="T2165" id="Seg_1118" n="e" s="T2164">Šonəbi, </ts>
               <ts e="T2166" id="Seg_1120" n="e" s="T2165">šonəbi, </ts>
               <ts e="T2167" id="Seg_1122" n="e" s="T2166">a </ts>
               <ts e="T2168" id="Seg_1124" n="e" s="T2167">krupat </ts>
               <ts e="T2169" id="Seg_1126" n="e" s="T2168">bar </ts>
               <ts e="T2170" id="Seg_1128" n="e" s="T2169">kamluʔpi. </ts>
               <ts e="T2171" id="Seg_1130" n="e" s="T2170">Nüket </ts>
               <ts e="T2172" id="Seg_1132" n="e" s="T2171">măndə:" </ts>
               <ts e="T2173" id="Seg_1134" n="e" s="T2172">Gijen </ts>
               <ts e="T2174" id="Seg_1136" n="e" s="T2173">tăn </ts>
               <ts e="T2175" id="Seg_1138" n="e" s="T2174">krupa?" </ts>
               <ts e="T2176" id="Seg_1140" n="e" s="T2175">"Da </ts>
               <ts e="T2177" id="Seg_1142" n="e" s="T2176">kamluʔpiam", </ts>
               <ts e="T2178" id="Seg_1144" n="e" s="T2177">dĭgəttə </ts>
               <ts e="T2179" id="Seg_1146" n="e" s="T2178">kambi. </ts>
               <ts e="T2180" id="Seg_1148" n="e" s="T2179">Măndə:" </ts>
               <ts e="T2181" id="Seg_1150" n="e" s="T2180">Kuja </ts>
               <ts e="T2182" id="Seg_1152" n="e" s="T2181">kabažəraʔ!" </ts>
               <ts e="T2183" id="Seg_1154" n="e" s="T2182">Mesʼatsdə </ts>
               <ts e="T2184" id="Seg_1156" n="e" s="T2183">măndə:" </ts>
               <ts e="T2185" id="Seg_1158" n="e" s="T2184">Kabažəraʔ!" </ts>
               <ts e="T2186" id="Seg_1160" n="e" s="T2185">I </ts>
               <ts e="T2187" id="Seg_1162" n="e" s="T2186">(voron=) </ts>
               <ts e="T2188" id="Seg_1164" n="e" s="T2187">vorondə </ts>
               <ts e="T2189" id="Seg_1166" n="e" s="T2188">măndə:" </ts>
               <ts e="T2190" id="Seg_1168" n="e" s="T2189">Kabažəraʔ!" </ts>
               <ts e="T2191" id="Seg_1170" n="e" s="T2190">Dĭzeŋ </ts>
               <ts e="T2192" id="Seg_1172" n="e" s="T2191">šobiʔi. </ts>
               <ts e="T2193" id="Seg_1174" n="e" s="T2192">I </ts>
               <ts e="T2194" id="Seg_1176" n="e" s="T2193">(ka-) </ts>
               <ts e="T2195" id="Seg_1178" n="e" s="T2194">kabažərbiʔi </ts>
               <ts e="T2196" id="Seg_1180" n="e" s="T2195">dĭ. </ts>
               <ts e="T2197" id="Seg_1182" n="e" s="T2196">"Măn </ts>
               <ts e="T2198" id="Seg_1184" n="e" s="T2197">koʔbsaŋdə </ts>
               <ts e="T2199" id="Seg_1186" n="e" s="T2198">šiʔnʼileʔ </ts>
               <ts e="T2200" id="Seg_1188" n="e" s="T2199">mĭlem </ts>
               <ts e="T2201" id="Seg_1190" n="e" s="T2200">tibinə". </ts>
               <ts e="T2202" id="Seg_1192" n="e" s="T2201">Dĭgəttə </ts>
               <ts e="T2203" id="Seg_1194" n="e" s="T2202">šobi. </ts>
               <ts e="T2204" id="Seg_1196" n="e" s="T2203">Deppi </ts>
               <ts e="T2205" id="Seg_1198" n="e" s="T2204">dĭ </ts>
               <ts e="T2206" id="Seg_1200" n="e" s="T2205">krupam </ts>
               <ts e="T2207" id="Seg_1202" n="e" s="T2206">nükendə. </ts>
               <ts e="T2208" id="Seg_1204" n="e" s="T2207">Dĭgəttə </ts>
               <ts e="T2209" id="Seg_1206" n="e" s="T2208">onʼiʔ </ts>
               <ts e="T2210" id="Seg_1208" n="e" s="T2209">urgo </ts>
               <ts e="T2211" id="Seg_1210" n="e" s="T2210">koʔbdonə </ts>
               <ts e="T2212" id="Seg_1212" n="e" s="T2211">măndə: </ts>
               <ts e="T2213" id="Seg_1214" n="e" s="T2212">"Šereʔ </ts>
               <ts e="T2214" id="Seg_1216" n="e" s="T2213">oldʼa </ts>
               <ts e="T2215" id="Seg_1218" n="e" s="T2214">kuvas". </ts>
               <ts e="T2216" id="Seg_1220" n="e" s="T2215">Dĭ </ts>
               <ts e="T2217" id="Seg_1222" n="e" s="T2216">šerbi </ts>
               <ts e="T2218" id="Seg_1224" n="e" s="T2217">da </ts>
               <ts e="T2219" id="Seg_1226" n="e" s="T2218">kambi </ts>
               <ts e="T2220" id="Seg_1228" n="e" s="T2219">nʼiʔdə. </ts>
               <ts e="T2221" id="Seg_1230" n="e" s="T2220">Dĭm </ts>
               <ts e="T2222" id="Seg_1232" n="e" s="T2221">iluʔpi </ts>
               <ts e="T2223" id="Seg_1234" n="e" s="T2222">kuja. </ts>
               <ts e="T2224" id="Seg_1236" n="e" s="T2223">Dĭgəttə </ts>
               <ts e="T2225" id="Seg_1238" n="e" s="T2224">baška </ts>
               <ts e="T2226" id="Seg_1240" n="e" s="T2225">(koʔbdotsiʔ). </ts>
               <ts e="T2227" id="Seg_1242" n="e" s="T2226">"Šereʔ </ts>
               <ts e="T2228" id="Seg_1244" n="e" s="T2227">oldʼa </ts>
               <ts e="T2229" id="Seg_1246" n="e" s="T2228">kuvas </ts>
               <ts e="T2230" id="Seg_1248" n="e" s="T2229">i </ts>
               <ts e="T2231" id="Seg_1250" n="e" s="T2230">kanaʔ </ts>
               <ts e="T2232" id="Seg_1252" n="e" s="T2231">nʼiʔdə". </ts>
               <ts e="T2233" id="Seg_1254" n="e" s="T2232">Dĭ </ts>
               <ts e="T2234" id="Seg_1256" n="e" s="T2233">kambi. </ts>
               <ts e="T2235" id="Seg_1258" n="e" s="T2234">Dĭm </ts>
               <ts e="T2236" id="Seg_1260" n="e" s="T2235">mesʼats </ts>
               <ts e="T2237" id="Seg_1262" n="e" s="T2236">ibi. </ts>
               <ts e="T2238" id="Seg_1264" n="e" s="T2237">Dĭgəttə </ts>
               <ts e="T2239" id="Seg_1266" n="e" s="T2238">nagurgo </ts>
               <ts e="T2240" id="Seg_1268" n="e" s="T2239">koʔbdonə </ts>
               <ts e="T2241" id="Seg_1270" n="e" s="T2240">măndə: </ts>
               <ts e="T2242" id="Seg_1272" n="e" s="T2241">"Šereʔ </ts>
               <ts e="T2243" id="Seg_1274" n="e" s="T2242">oldʼa </ts>
               <ts e="T2244" id="Seg_1276" n="e" s="T2243">kuvas </ts>
               <ts e="T2245" id="Seg_1278" n="e" s="T2244">i </ts>
               <ts e="T2246" id="Seg_1280" n="e" s="T2245">kanaʔ </ts>
               <ts e="T2247" id="Seg_1282" n="e" s="T2246">nʼiʔdə". </ts>
               <ts e="T2248" id="Seg_1284" n="e" s="T2247">Dĭ </ts>
               <ts e="T2249" id="Seg_1286" n="e" s="T2248">kambi. </ts>
               <ts e="T2250" id="Seg_1288" n="e" s="T2249">Dĭm </ts>
               <ts e="T2251" id="Seg_1290" n="e" s="T2250">tože </ts>
               <ts e="T2252" id="Seg_1292" n="e" s="T2251">iluʔpi </ts>
               <ts e="T2253" id="Seg_1294" n="e" s="T2252">voron. </ts>
               <ts e="T2254" id="Seg_1296" n="e" s="T2253">Dĭgəttə </ts>
               <ts e="T2255" id="Seg_1298" n="e" s="T2254">dĭ </ts>
               <ts e="T2256" id="Seg_1300" n="e" s="T2255">(bü- </ts>
               <ts e="T2257" id="Seg_1302" n="e" s="T2256">dĭ </ts>
               <ts e="T2258" id="Seg_1304" n="e" s="T2257">kuʔ-) </ts>
               <ts e="T2259" id="Seg_1306" n="e" s="T2258">büzʼe </ts>
               <ts e="T2260" id="Seg_1308" n="e" s="T2259">măndə:" </ts>
               <ts e="T2261" id="Seg_1310" n="e" s="T2260">Kallam </ts>
               <ts e="T2262" id="Seg_1312" n="e" s="T2261">koʔbdonə </ts>
               <ts e="T2263" id="Seg_1314" n="e" s="T2262">jadajlaʔ". </ts>
               <ts e="T2264" id="Seg_1316" n="e" s="T2263">Šobi. </ts>
               <ts e="T2265" id="Seg_1318" n="e" s="T2264">"Ĭmbiziʔ </ts>
               <ts e="T2266" id="Seg_1320" n="e" s="T2265">tănan </ts>
               <ts e="T2267" id="Seg_1322" n="e" s="T2266">bădəsʼtə?" </ts>
               <ts e="T2268" id="Seg_1324" n="e" s="T2267">"Da </ts>
               <ts e="T2269" id="Seg_1326" n="e" s="T2268">(măn=) </ts>
               <ts e="T2270" id="Seg_1328" n="e" s="T2269">măn </ts>
               <ts e="T2271" id="Seg_1330" n="e" s="T2270">nʼe </ts>
               <ts e="T2272" id="Seg_1332" n="e" s="T2271">axota </ts>
               <ts e="T2273" id="Seg_1334" n="e" s="T2272">amzittə". </ts>
               <ts e="T2274" id="Seg_1336" n="e" s="T2273">Dĭgəttə </ts>
               <ts e="T2275" id="Seg_1338" n="e" s="T2274">dĭ </ts>
               <ts e="T2276" id="Seg_1340" n="e" s="T2275">aladʼaʔi </ts>
               <ts e="T2277" id="Seg_1342" n="e" s="T2276">(da </ts>
               <ts e="T2278" id="Seg_1344" n="e" s="T2277">bə-), </ts>
               <ts e="T2279" id="Seg_1346" n="e" s="T2278">koʔbdot </ts>
               <ts e="T2280" id="Seg_1348" n="e" s="T2279">aladʼaʔi </ts>
               <ts e="T2281" id="Seg_1350" n="e" s="T2280">embi, </ts>
               <ts e="T2282" id="Seg_1352" n="e" s="T2281">skavaradandə. </ts>
               <ts e="T2283" id="Seg_1354" n="e" s="T2282">Nuldəbi </ts>
               <ts e="T2284" id="Seg_1356" n="e" s="T2283">sontsənə, </ts>
               <ts e="T2285" id="Seg_1358" n="e" s="T2284">dĭ </ts>
               <ts e="T2286" id="Seg_1360" n="e" s="T2285">pürluʔpiʔi. </ts>
               <ts e="T2287" id="Seg_1362" n="e" s="T2286">Dĭ </ts>
               <ts e="T2288" id="Seg_1364" n="e" s="T2287">büzʼe </ts>
               <ts e="T2289" id="Seg_1366" n="e" s="T2288">ambi </ts>
               <ts e="T2290" id="Seg_1368" n="e" s="T2289">i </ts>
               <ts e="T2291" id="Seg_1370" n="e" s="T2290">šobi </ts>
               <ts e="T2292" id="Seg_1372" n="e" s="T2291">maːʔndə. </ts>
               <ts e="T2293" id="Seg_1374" n="e" s="T2292">Nükendə </ts>
               <ts e="T2294" id="Seg_1376" n="e" s="T2293">măndə: </ts>
               <ts e="T2295" id="Seg_1378" n="e" s="T2294">"Pürəʔ </ts>
               <ts e="T2296" id="Seg_1380" n="e" s="T2295">aladʼaʔi!" </ts>
               <ts e="T2297" id="Seg_1382" n="e" s="T2296">Dĭ </ts>
               <ts e="T2298" id="Seg_1384" n="e" s="T2297">davaj </ts>
               <ts e="T2299" id="Seg_1386" n="e" s="T2298">pürzittə. </ts>
               <ts e="T2300" id="Seg_1388" n="e" s="T2299">I </ts>
               <ts e="T2301" id="Seg_1390" n="e" s="T2300">(aŋdə) </ts>
               <ts e="T2302" id="Seg_1392" n="e" s="T2301">nuldəbi, </ts>
               <ts e="T2303" id="Seg_1394" n="e" s="T2302">nulaʔbiʔi, </ts>
               <ts e="T2304" id="Seg_1396" n="e" s="T2303">nulaʔbiʔi, </ts>
               <ts e="T2305" id="Seg_1398" n="e" s="T2304">ej </ts>
               <ts e="T2306" id="Seg_1400" n="e" s="T2305">pürbiʔi. </ts>
               <ts e="T2307" id="Seg_1402" n="e" s="T2306">Dĭgəttə </ts>
               <ts e="T2308" id="Seg_1404" n="e" s="T2307">dĭ </ts>
               <ts e="T2309" id="Seg_1406" n="e" s="T2308">pʼešdə </ts>
               <ts e="T2310" id="Seg_1408" n="e" s="T2309">embi. </ts>
               <ts e="T2311" id="Seg_1410" n="e" s="T2310">Dĭn </ts>
               <ts e="T2312" id="Seg_1412" n="e" s="T2311">pürbi. </ts>
               <ts e="T2313" id="Seg_1414" n="e" s="T2312">I </ts>
               <ts e="T2314" id="Seg_1416" n="e" s="T2313">büzʼe </ts>
               <ts e="T2315" id="Seg_1418" n="e" s="T2314">ambi. </ts>
               <ts e="T2316" id="Seg_1420" n="e" s="T2315">Dĭgəttə </ts>
               <ts e="T2317" id="Seg_1422" n="e" s="T2316">kandəga </ts>
               <ts e="T2318" id="Seg_1424" n="e" s="T2317">mesʼatsdə. </ts>
               <ts e="T2319" id="Seg_1426" n="e" s="T2318">Šobi. </ts>
               <ts e="T2320" id="Seg_1428" n="e" s="T2319">A </ts>
               <ts e="T2321" id="Seg_1430" n="e" s="T2320">dĭ </ts>
               <ts e="T2322" id="Seg_1432" n="e" s="T2321">moltʼa </ts>
               <ts e="T2323" id="Seg_1434" n="e" s="T2322">nendəbi. </ts>
               <ts e="T2324" id="Seg_1436" n="e" s="T2323">Măndə: </ts>
               <ts e="T2325" id="Seg_1438" n="e" s="T2324">"No, </ts>
               <ts e="T2326" id="Seg_1440" n="e" s="T2325">kanaʔ </ts>
               <ts e="T2327" id="Seg_1442" n="e" s="T2326">moltʼanə". </ts>
               <ts e="T2328" id="Seg_1444" n="e" s="T2327">"A </ts>
               <ts e="T2329" id="Seg_1446" n="e" s="T2328">dĭn </ts>
               <ts e="T2330" id="Seg_1448" n="e" s="T2329">ĭmbidə </ts>
               <ts e="T2331" id="Seg_1450" n="e" s="T2330">ej </ts>
               <ts e="T2332" id="Seg_1452" n="e" s="T2331">edlia". </ts>
               <ts e="T2333" id="Seg_1454" n="e" s="T2332">"Kanaʔ". </ts>
               <ts e="T2334" id="Seg_1456" n="e" s="T2333">Dĭ </ts>
               <ts e="T2335" id="Seg_1458" n="e" s="T2334">kambi. </ts>
               <ts e="T2336" id="Seg_1460" n="e" s="T2335">Dĭ </ts>
               <ts e="T2337" id="Seg_1462" n="e" s="T2336">müjəbə </ts>
               <ts e="T2338" id="Seg_1464" n="e" s="T2337">dibər </ts>
               <ts e="T2339" id="Seg_1466" n="e" s="T2338">paʔluʔpi, </ts>
               <ts e="T2340" id="Seg_1468" n="e" s="T2339">dĭ </ts>
               <ts e="T2341" id="Seg_1470" n="e" s="T2340">(băzajdəbi), </ts>
               <ts e="T2342" id="Seg_1472" n="e" s="T2341">dĭn </ts>
               <ts e="T2343" id="Seg_1474" n="e" s="T2342">bar </ts>
               <ts e="T2344" id="Seg_1476" n="e" s="T2343">edlaʔbə. </ts>
               <ts e="T2345" id="Seg_1478" n="e" s="T2344">Dĭgəttə </ts>
               <ts e="T2346" id="Seg_1480" n="e" s="T2345">maːndə </ts>
               <ts e="T2347" id="Seg_1482" n="e" s="T2346">šobi. </ts>
               <ts e="T2348" id="Seg_1484" n="e" s="T2347">"Nendəʔ </ts>
               <ts e="T2349" id="Seg_1486" n="e" s="T2348">moltʼa!" </ts>
               <ts e="T2350" id="Seg_1488" n="e" s="T2349">Nüket </ts>
               <ts e="T2351" id="Seg_1490" n="e" s="T2350">nendəbi. </ts>
               <ts e="T2352" id="Seg_1492" n="e" s="T2351">"No, </ts>
               <ts e="T2353" id="Seg_1494" n="e" s="T2352">(m-) </ts>
               <ts e="T2354" id="Seg_1496" n="e" s="T2353">băzaʔ!" </ts>
               <ts e="T2355" id="Seg_1498" n="e" s="T2354">"Da </ts>
               <ts e="T2356" id="Seg_1500" n="e" s="T2355">dĭn </ts>
               <ts e="T2357" id="Seg_1502" n="e" s="T2356">šü </ts>
               <ts e="T2358" id="Seg_1504" n="e" s="T2357">naga". </ts>
               <ts e="T2359" id="Seg_1506" n="e" s="T2358">"Kanaʔ". </ts>
               <ts e="T2360" id="Seg_1508" n="e" s="T2359">Dĭ </ts>
               <ts e="T2361" id="Seg_1510" n="e" s="T2360">ši </ts>
               <ts e="T2362" id="Seg_1512" n="e" s="T2361">abi. </ts>
               <ts e="T2363" id="Seg_1514" n="e" s="T2362">Dibər </ts>
               <ts e="T2364" id="Seg_1516" n="e" s="T2363">müjət </ts>
               <ts e="T2365" id="Seg_1518" n="e" s="T2364">paʔdəbi. </ts>
               <ts e="T2366" id="Seg_1520" n="e" s="T2365">Nüket </ts>
               <ts e="T2367" id="Seg_1522" n="e" s="T2366">uge </ts>
               <ts e="T2368" id="Seg_1524" n="e" s="T2367">kirgarlaʔbə. </ts>
               <ts e="T2369" id="Seg_1526" n="e" s="T2368">"Naga </ts>
               <ts e="T2370" id="Seg_1528" n="e" s="T2369">šü, </ts>
               <ts e="T2371" id="Seg_1530" n="e" s="T2370">naga </ts>
               <ts e="T2372" id="Seg_1532" n="e" s="T2371">šü!" </ts>
               <ts e="T2373" id="Seg_1534" n="e" s="T2372">Dĭgəttə </ts>
               <ts e="T2374" id="Seg_1536" n="e" s="T2373">băzajdəbiʔi. </ts>
               <ts e="T2375" id="Seg_1538" n="e" s="T2374">Šü </ts>
               <ts e="T2376" id="Seg_1540" n="e" s="T2375">nendəbiʔi. </ts>
               <ts e="T2377" id="Seg_1542" n="e" s="T2376">Dĭgəttə </ts>
               <ts e="T2378" id="Seg_1544" n="e" s="T2377">erten </ts>
               <ts e="T2379" id="Seg_1546" n="e" s="T2378">uʔlaʔbə, </ts>
               <ts e="T2380" id="Seg_1548" n="e" s="T2379">(ka-, </ts>
               <ts e="T2381" id="Seg_1550" n="e" s="T2380">ka-) </ts>
               <ts e="T2382" id="Seg_1552" n="e" s="T2381">kambi. </ts>
               <ts e="T2383" id="Seg_1554" n="e" s="T2382">Baška </ts>
               <ts e="T2384" id="Seg_1556" n="e" s="T2383">koʔbdonə </ts>
               <ts e="T2385" id="Seg_1558" n="e" s="T2384">dĭn </ts>
               <ts e="T2386" id="Seg_1560" n="e" s="T2385">vorondə. </ts>
               <ts e="T2387" id="Seg_1562" n="e" s="T2386">Dĭn </ts>
               <ts e="T2388" id="Seg_1564" n="e" s="T2387">măndə:" </ts>
               <ts e="T2389" id="Seg_1566" n="e" s="T2388">Ĭmbi </ts>
               <ts e="T2390" id="Seg_1568" n="e" s="T2389">tănan </ts>
               <ts e="T2391" id="Seg_1570" n="e" s="T2390">mĭzittə?" </ts>
               <ts e="T2392" id="Seg_1572" n="e" s="T2391">"Nu </ts>
               <ts e="T2393" id="Seg_1574" n="e" s="T2392">davaj </ts>
               <ts e="T2394" id="Seg_1576" n="e" s="T2393">kunolzittə". </ts>
               <ts e="T2395" id="Seg_1578" n="e" s="T2394">(Dĭ=) </ts>
               <ts e="T2396" id="Seg_1580" n="e" s="T2395">Dĭ </ts>
               <ts e="T2397" id="Seg_1582" n="e" s="T2396">ibi </ts>
               <ts e="T2398" id="Seg_1584" n="e" s="T2397">(dĭm) </ts>
               <ts e="T2399" id="Seg_1586" n="e" s="T2398">(krɨlondə). </ts>
               <ts e="T2400" id="Seg_1588" n="e" s="T2399">Šidegöʔ </ts>
               <ts e="T2401" id="Seg_1590" n="e" s="T2400">kunolluʔpiʔi. </ts>
               <ts e="T2402" id="Seg_1592" n="e" s="T2401">I </ts>
               <ts e="T2403" id="Seg_1594" n="e" s="T2402">uzleʔpiʔi. </ts>
               <ts e="T2404" id="Seg_1596" n="e" s="T2403">I </ts>
               <ts e="T2405" id="Seg_1598" n="e" s="T2404">külaːmbiʔi. </ts>
               <ts e="T2406" id="Seg_1600" n="e" s="T2405">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2148" id="Seg_1601" s="T2145">PKZ_196X_SunMoonAndRaven_flk.001 (001)</ta>
            <ta e="T2152" id="Seg_1602" s="T2148">PKZ_196X_SunMoonAndRaven_flk.002 (002)np.h:S</ta>
            <ta e="T2160" id="Seg_1603" s="T2152">PKZ_196X_SunMoonAndRaven_flk.003 (003)np.h:S</ta>
            <ta e="T2164" id="Seg_1604" s="T2160">PKZ_196X_SunMoonAndRaven_flk.004 (004)</ta>
            <ta e="T2170" id="Seg_1605" s="T2164">PKZ_196X_SunMoonAndRaven_flk.005 (005)</ta>
            <ta e="T2175" id="Seg_1606" s="T2170">PKZ_196X_SunMoonAndRaven_flk.006 (006)</ta>
            <ta e="T2179" id="Seg_1607" s="T2175">PKZ_196X_SunMoonAndRaven_flk.007 (007)</ta>
            <ta e="T2182" id="Seg_1608" s="T2179">PKZ_196X_SunMoonAndRaven_flk.008 (008)</ta>
            <ta e="T2185" id="Seg_1609" s="T2182">PKZ_196X_SunMoonAndRaven_flk.009 (009)</ta>
            <ta e="T2190" id="Seg_1610" s="T2185">PKZ_196X_SunMoonAndRaven_flk.010 (010)</ta>
            <ta e="T2192" id="Seg_1611" s="T2190">PKZ_196X_SunMoonAndRaven_flk.011 (011)</ta>
            <ta e="T2196" id="Seg_1612" s="T2192">PKZ_196X_SunMoonAndRaven_flk.012 (012)</ta>
            <ta e="T2201" id="Seg_1613" s="T2196">PKZ_196X_SunMoonAndRaven_flk.013 (013)</ta>
            <ta e="T2203" id="Seg_1614" s="T2201">PKZ_196X_SunMoonAndRaven_flk.014 (014)</ta>
            <ta e="T2207" id="Seg_1615" s="T2203">PKZ_196X_SunMoonAndRaven_flk.015 (015)</ta>
            <ta e="T2215" id="Seg_1616" s="T2207">PKZ_196X_SunMoonAndRaven_flk.016 (016)</ta>
            <ta e="T2220" id="Seg_1617" s="T2215">PKZ_196X_SunMoonAndRaven_flk.017 (018)</ta>
            <ta e="T2223" id="Seg_1618" s="T2220">PKZ_196X_SunMoonAndRaven_flk.018 (019)</ta>
            <ta e="T2226" id="Seg_1619" s="T2223">PKZ_196X_SunMoonAndRaven_flk.019 (020)</ta>
            <ta e="T2232" id="Seg_1620" s="T2226">PKZ_196X_SunMoonAndRaven_flk.020 (021)</ta>
            <ta e="T2234" id="Seg_1621" s="T2232">PKZ_196X_SunMoonAndRaven_flk.021 (022)</ta>
            <ta e="T2237" id="Seg_1622" s="T2234">PKZ_196X_SunMoonAndRaven_flk.022 (023)</ta>
            <ta e="T2247" id="Seg_1623" s="T2237">PKZ_196X_SunMoonAndRaven_flk.023 (024)</ta>
            <ta e="T2249" id="Seg_1624" s="T2247">PKZ_196X_SunMoonAndRaven_flk.024 (026)</ta>
            <ta e="T2253" id="Seg_1625" s="T2249">PKZ_196X_SunMoonAndRaven_flk.025 (027)</ta>
            <ta e="T2263" id="Seg_1626" s="T2253">PKZ_196X_SunMoonAndRaven_flk.026 (028)</ta>
            <ta e="T2264" id="Seg_1627" s="T2263">PKZ_196X_SunMoonAndRaven_flk.027 (029)</ta>
            <ta e="T2267" id="Seg_1628" s="T2264">PKZ_196X_SunMoonAndRaven_flk.028 (030)</ta>
            <ta e="T2273" id="Seg_1629" s="T2267">PKZ_196X_SunMoonAndRaven_flk.029 (031)</ta>
            <ta e="T2282" id="Seg_1630" s="T2273">PKZ_196X_SunMoonAndRaven_flk.030 (032)</ta>
            <ta e="T2286" id="Seg_1631" s="T2282">PKZ_196X_SunMoonAndRaven_flk.031 (033)</ta>
            <ta e="T2292" id="Seg_1632" s="T2286">PKZ_196X_SunMoonAndRaven_flk.032 (034)</ta>
            <ta e="T2296" id="Seg_1633" s="T2292">PKZ_196X_SunMoonAndRaven_flk.033 (035)</ta>
            <ta e="T2299" id="Seg_1634" s="T2296">PKZ_196X_SunMoonAndRaven_flk.034 (037)</ta>
            <ta e="T2306" id="Seg_1635" s="T2299">PKZ_196X_SunMoonAndRaven_flk.035 (038)</ta>
            <ta e="T2310" id="Seg_1636" s="T2306">PKZ_196X_SunMoonAndRaven_flk.036 (039)</ta>
            <ta e="T2312" id="Seg_1637" s="T2310">PKZ_196X_SunMoonAndRaven_flk.037 (040)</ta>
            <ta e="T2315" id="Seg_1638" s="T2312">PKZ_196X_SunMoonAndRaven_flk.038 (041)</ta>
            <ta e="T2318" id="Seg_1639" s="T2315">PKZ_196X_SunMoonAndRaven_flk.039 (042)</ta>
            <ta e="T2319" id="Seg_1640" s="T2318">PKZ_196X_SunMoonAndRaven_flk.040 (043)</ta>
            <ta e="T2323" id="Seg_1641" s="T2319">PKZ_196X_SunMoonAndRaven_flk.041 (044)</ta>
            <ta e="T2327" id="Seg_1642" s="T2323">PKZ_196X_SunMoonAndRaven_flk.042 (045)</ta>
            <ta e="T2332" id="Seg_1643" s="T2327">PKZ_196X_SunMoonAndRaven_flk.043 (047)</ta>
            <ta e="T2333" id="Seg_1644" s="T2332">PKZ_196X_SunMoonAndRaven_flk.044 (048)</ta>
            <ta e="T2335" id="Seg_1645" s="T2333">PKZ_196X_SunMoonAndRaven_flk.045 (049)</ta>
            <ta e="T2344" id="Seg_1646" s="T2335">PKZ_196X_SunMoonAndRaven_flk.046 (050)</ta>
            <ta e="T2347" id="Seg_1647" s="T2344">PKZ_196X_SunMoonAndRaven_flk.047 (051)</ta>
            <ta e="T2349" id="Seg_1648" s="T2347">PKZ_196X_SunMoonAndRaven_flk.048 (052)</ta>
            <ta e="T2351" id="Seg_1649" s="T2349">PKZ_196X_SunMoonAndRaven_flk.049 (053)</ta>
            <ta e="T2354" id="Seg_1650" s="T2351">PKZ_196X_SunMoonAndRaven_flk.050 (054)</ta>
            <ta e="T2358" id="Seg_1651" s="T2354">PKZ_196X_SunMoonAndRaven_flk.051 (055)</ta>
            <ta e="T2359" id="Seg_1652" s="T2358">PKZ_196X_SunMoonAndRaven_flk.052 (056)</ta>
            <ta e="T2362" id="Seg_1653" s="T2359">PKZ_196X_SunMoonAndRaven_flk.053 (057)</ta>
            <ta e="T2365" id="Seg_1654" s="T2362">PKZ_196X_SunMoonAndRaven_flk.054 (058)</ta>
            <ta e="T2368" id="Seg_1655" s="T2365">PKZ_196X_SunMoonAndRaven_flk.055 (059)</ta>
            <ta e="T2372" id="Seg_1656" s="T2368">PKZ_196X_SunMoonAndRaven_flk.056 (060)</ta>
            <ta e="T2374" id="Seg_1657" s="T2372">PKZ_196X_SunMoonAndRaven_flk.057 (061)</ta>
            <ta e="T2376" id="Seg_1658" s="T2374">PKZ_196X_SunMoonAndRaven_flk.058 (062)</ta>
            <ta e="T2382" id="Seg_1659" s="T2376">PKZ_196X_SunMoonAndRaven_flk.059 (063)</ta>
            <ta e="T2386" id="Seg_1660" s="T2382">PKZ_196X_SunMoonAndRaven_flk.060 (064)</ta>
            <ta e="T2391" id="Seg_1661" s="T2386">PKZ_196X_SunMoonAndRaven_flk.061 (065)</ta>
            <ta e="T2394" id="Seg_1662" s="T2391">PKZ_196X_SunMoonAndRaven_flk.062 (066)</ta>
            <ta e="T2399" id="Seg_1663" s="T2394">PKZ_196X_SunMoonAndRaven_flk.063 (067)</ta>
            <ta e="T2401" id="Seg_1664" s="T2399">PKZ_196X_SunMoonAndRaven_flk.064 (068)</ta>
            <ta e="T2403" id="Seg_1665" s="T2401">PKZ_196X_SunMoonAndRaven_flk.065 (069)</ta>
            <ta e="T2405" id="Seg_1666" s="T2403">PKZ_196X_SunMoonAndRaven_flk.066 (070)</ta>
            <ta e="T2406" id="Seg_1667" s="T2405">PKZ_196X_SunMoonAndRaven_flk.067 (071)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2148" id="Seg_1668" s="T2145">Amnolaʔpi nüke, büzʼe. </ta>
            <ta e="T2152" id="Seg_1669" s="T2148">Dĭn nagur koʔbdo ibi. </ta>
            <ta e="T2160" id="Seg_1670" s="T2152">Dĭ kambi (a-) anbardə, krupa ibi băranə, šobi. </ta>
            <ta e="T2164" id="Seg_1671" s="T2160">A băragən ši ibi. </ta>
            <ta e="T2170" id="Seg_1672" s="T2164">Šonəbi, šonəbi, a krupat bar kamluʔpi. </ta>
            <ta e="T2175" id="Seg_1673" s="T2170">Nüket măndə:" Gijen tăn krupa?" </ta>
            <ta e="T2179" id="Seg_1674" s="T2175">"Da kamluʔpiam", dĭgəttə kambi. </ta>
            <ta e="T2182" id="Seg_1675" s="T2179">Măndə:" Kuja kabažəraʔ!" </ta>
            <ta e="T2185" id="Seg_1676" s="T2182">Mesʼatsdə măndə:" Kabažəraʔ!" </ta>
            <ta e="T2190" id="Seg_1677" s="T2185">I (voron=) vorondə măndə:" Kabažəraʔ!" </ta>
            <ta e="T2192" id="Seg_1678" s="T2190">Dĭzeŋ šobiʔi. </ta>
            <ta e="T2196" id="Seg_1679" s="T2192">I (ka-) kabažərbiʔi dĭ. </ta>
            <ta e="T2201" id="Seg_1680" s="T2196">"Măn koʔbsaŋdə šiʔnʼileʔ mĭlem tibinə". </ta>
            <ta e="T2203" id="Seg_1681" s="T2201">Dĭgəttə šobi. </ta>
            <ta e="T2207" id="Seg_1682" s="T2203">Deppi dĭ krupam nükendə. </ta>
            <ta e="T2215" id="Seg_1683" s="T2207">Dĭgəttə onʼiʔ urgo koʔbdonə măndə: "Šereʔ oldʼa kuvas". </ta>
            <ta e="T2220" id="Seg_1684" s="T2215">Dĭ šerbi da kambi nʼiʔdə. </ta>
            <ta e="T2223" id="Seg_1685" s="T2220">Dĭm iluʔpi kuja. </ta>
            <ta e="T2226" id="Seg_1686" s="T2223">Dĭgəttə baška (koʔbdotsiʔ). </ta>
            <ta e="T2232" id="Seg_1687" s="T2226">"Šereʔ oldʼa kuvas i kanaʔ nʼiʔdə". </ta>
            <ta e="T2234" id="Seg_1688" s="T2232">Dĭ kambi. </ta>
            <ta e="T2237" id="Seg_1689" s="T2234">Dĭm mesʼats ibi. </ta>
            <ta e="T2247" id="Seg_1690" s="T2237">Dĭgəttə nagurgo koʔbdonə măndə: "Šereʔ oldʼa kuvas i kanaʔ nʼiʔdə". </ta>
            <ta e="T2249" id="Seg_1691" s="T2247">Dĭ kambi. </ta>
            <ta e="T2253" id="Seg_1692" s="T2249">Dĭm tože iluʔpi voron. </ta>
            <ta e="T2263" id="Seg_1693" s="T2253">Dĭgəttə dĭ (bü- dĭ kuʔ-) büzʼe măndə:" Kallam koʔbdonə jadajlaʔ". </ta>
            <ta e="T2264" id="Seg_1694" s="T2263">Šobi. </ta>
            <ta e="T2267" id="Seg_1695" s="T2264">"Ĭmbiziʔ tănan bădəsʼtə?" </ta>
            <ta e="T2273" id="Seg_1696" s="T2267">"Da (măn=) măn nʼe axota amzittə". </ta>
            <ta e="T2282" id="Seg_1697" s="T2273">Dĭgəttə dĭ aladʼaʔi (da bə-), koʔbdot aladʼaʔi embi, skavaradandə. </ta>
            <ta e="T2286" id="Seg_1698" s="T2282">Nuldəbi sontsənə, dĭ pürluʔpiʔi. </ta>
            <ta e="T2292" id="Seg_1699" s="T2286">Dĭ büzʼe ambi i šobi maːʔndə. </ta>
            <ta e="T2296" id="Seg_1700" s="T2292">Nükendə măndə: "Pürəʔ aladʼaʔi!" </ta>
            <ta e="T2299" id="Seg_1701" s="T2296">Dĭ davaj pürzittə. </ta>
            <ta e="T2306" id="Seg_1702" s="T2299">I (aŋdə) nuldəbi, nulaʔbiʔi, nulaʔbiʔi, ej pürbiʔi. </ta>
            <ta e="T2310" id="Seg_1703" s="T2306">Dĭgəttə dĭ pʼešdə embi. </ta>
            <ta e="T2312" id="Seg_1704" s="T2310">Dĭn pürbi. </ta>
            <ta e="T2315" id="Seg_1705" s="T2312">I büzʼe ambi. </ta>
            <ta e="T2318" id="Seg_1706" s="T2315">Dĭgəttə kandəga mesʼatsdə. </ta>
            <ta e="T2319" id="Seg_1707" s="T2318">Šobi. </ta>
            <ta e="T2323" id="Seg_1708" s="T2319">A dĭ moltʼa nendəbi. </ta>
            <ta e="T2327" id="Seg_1709" s="T2323">Măndə: "No, kanaʔ moltʼanə". </ta>
            <ta e="T2332" id="Seg_1710" s="T2327">"A dĭn ĭmbidə ej edlia". </ta>
            <ta e="T2333" id="Seg_1711" s="T2332">"Kanaʔ". </ta>
            <ta e="T2335" id="Seg_1712" s="T2333">Dĭ kambi. </ta>
            <ta e="T2344" id="Seg_1713" s="T2335">Dĭ müjəbə dibər paʔluʔpi, dĭ (băzajdəbi), dĭn bar edlaʔbə. </ta>
            <ta e="T2347" id="Seg_1714" s="T2344">Dĭgəttə maːndə šobi. </ta>
            <ta e="T2349" id="Seg_1715" s="T2347">"Nendəʔ moltʼa!" </ta>
            <ta e="T2351" id="Seg_1716" s="T2349">Nüket nendəbi. </ta>
            <ta e="T2354" id="Seg_1717" s="T2351">"No, (m-) băzaʔ!" </ta>
            <ta e="T2358" id="Seg_1718" s="T2354">"Da dĭn šü naga". </ta>
            <ta e="T2359" id="Seg_1719" s="T2358">"Kanaʔ". </ta>
            <ta e="T2362" id="Seg_1720" s="T2359">Dĭ ši abi. </ta>
            <ta e="T2365" id="Seg_1721" s="T2362">Dibər müjət paʔdəbi. </ta>
            <ta e="T2368" id="Seg_1722" s="T2365">Nüket uge kirgarlaʔbə. </ta>
            <ta e="T2372" id="Seg_1723" s="T2368">"Naga šü, naga šü!" </ta>
            <ta e="T2374" id="Seg_1724" s="T2372">Dĭgəttə băzajdəbiʔi. </ta>
            <ta e="T2376" id="Seg_1725" s="T2374">Šü nendəbiʔi. </ta>
            <ta e="T2382" id="Seg_1726" s="T2376">Dĭgəttə erten uʔlaʔbə, (ka-, ka-) kambi. </ta>
            <ta e="T2386" id="Seg_1727" s="T2382">Baška koʔbdonə dĭn vorondə. </ta>
            <ta e="T2391" id="Seg_1728" s="T2386">Dĭn măndə:" Ĭmbi tănan mĭzittə?" </ta>
            <ta e="T2394" id="Seg_1729" s="T2391">"Nu davaj kunolzittə". </ta>
            <ta e="T2399" id="Seg_1730" s="T2394">(Dĭ=) Dĭ ibi (dĭm) (krɨlondə). </ta>
            <ta e="T2401" id="Seg_1731" s="T2399">Šidegöʔ kunolluʔpiʔi. </ta>
            <ta e="T2403" id="Seg_1732" s="T2401">I uzleʔpiʔi. </ta>
            <ta e="T2405" id="Seg_1733" s="T2403">I külaːmbiʔi. </ta>
            <ta e="T2406" id="Seg_1734" s="T2405">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2146" id="Seg_1735" s="T2145">amno-laʔ-pi</ta>
            <ta e="T2147" id="Seg_1736" s="T2146">nüke</ta>
            <ta e="T2148" id="Seg_1737" s="T2147">büzʼe</ta>
            <ta e="T2149" id="Seg_1738" s="T2148">dĭ-n</ta>
            <ta e="T2150" id="Seg_1739" s="T2149">nagur</ta>
            <ta e="T2151" id="Seg_1740" s="T2150">koʔbdo</ta>
            <ta e="T2152" id="Seg_1741" s="T2151">i-bi</ta>
            <ta e="T2153" id="Seg_1742" s="T2152">dĭ</ta>
            <ta e="T2154" id="Seg_1743" s="T2153">kam-bi</ta>
            <ta e="T2156" id="Seg_1744" s="T2155">anbar-də</ta>
            <ta e="T2157" id="Seg_1745" s="T2156">krupa</ta>
            <ta e="T2158" id="Seg_1746" s="T2157">i-bi</ta>
            <ta e="T2159" id="Seg_1747" s="T2158">băra-nə</ta>
            <ta e="T2160" id="Seg_1748" s="T2159">šo-bi</ta>
            <ta e="T2161" id="Seg_1749" s="T2160">a</ta>
            <ta e="T2162" id="Seg_1750" s="T2161">băra-gən</ta>
            <ta e="T2163" id="Seg_1751" s="T2162">ši</ta>
            <ta e="T2164" id="Seg_1752" s="T2163">i-bi</ta>
            <ta e="T2165" id="Seg_1753" s="T2164">šonə-bi</ta>
            <ta e="T2166" id="Seg_1754" s="T2165">šonə-bi</ta>
            <ta e="T2167" id="Seg_1755" s="T2166">a</ta>
            <ta e="T2168" id="Seg_1756" s="T2167">krupa-t</ta>
            <ta e="T2169" id="Seg_1757" s="T2168">bar</ta>
            <ta e="T2170" id="Seg_1758" s="T2169">kam-luʔ-pi</ta>
            <ta e="T2171" id="Seg_1759" s="T2170">nüke-t</ta>
            <ta e="T2172" id="Seg_1760" s="T2171">măn-də</ta>
            <ta e="T2173" id="Seg_1761" s="T2172">gijen</ta>
            <ta e="T2174" id="Seg_1762" s="T2173">tăn</ta>
            <ta e="T2175" id="Seg_1763" s="T2174">krupa</ta>
            <ta e="T2176" id="Seg_1764" s="T2175">da</ta>
            <ta e="T2177" id="Seg_1765" s="T2176">kam-luʔ-pia-m</ta>
            <ta e="T2178" id="Seg_1766" s="T2177">dĭgəttə</ta>
            <ta e="T2179" id="Seg_1767" s="T2178">kam-bi</ta>
            <ta e="T2180" id="Seg_1768" s="T2179">măn-də</ta>
            <ta e="T2181" id="Seg_1769" s="T2180">kuja</ta>
            <ta e="T2182" id="Seg_1770" s="T2181">kabažər-a-ʔ</ta>
            <ta e="T2183" id="Seg_1771" s="T2182">mesʼats-də</ta>
            <ta e="T2184" id="Seg_1772" s="T2183">măn-də</ta>
            <ta e="T2185" id="Seg_1773" s="T2184">kabažər-a-ʔ</ta>
            <ta e="T2186" id="Seg_1774" s="T2185">i</ta>
            <ta e="T2187" id="Seg_1775" s="T2186">voron</ta>
            <ta e="T2188" id="Seg_1776" s="T2187">voron-də</ta>
            <ta e="T2189" id="Seg_1777" s="T2188">măn-də</ta>
            <ta e="T2190" id="Seg_1778" s="T2189">kabažər-a-ʔ</ta>
            <ta e="T2191" id="Seg_1779" s="T2190">dĭ-zeŋ</ta>
            <ta e="T2192" id="Seg_1780" s="T2191">šo-bi-ʔi</ta>
            <ta e="T2193" id="Seg_1781" s="T2192">i</ta>
            <ta e="T2195" id="Seg_1782" s="T2194">kabažər-bi-ʔi</ta>
            <ta e="T2196" id="Seg_1783" s="T2195">dĭ</ta>
            <ta e="T2197" id="Seg_1784" s="T2196">măn</ta>
            <ta e="T2198" id="Seg_1785" s="T2197">koʔb-saŋ-də</ta>
            <ta e="T2199" id="Seg_1786" s="T2198">šiʔnʼileʔ</ta>
            <ta e="T2200" id="Seg_1787" s="T2199">mĭ-le-m</ta>
            <ta e="T2201" id="Seg_1788" s="T2200">tibi-nə</ta>
            <ta e="T2202" id="Seg_1789" s="T2201">dĭgəttə</ta>
            <ta e="T2203" id="Seg_1790" s="T2202">šo-bi</ta>
            <ta e="T2204" id="Seg_1791" s="T2203">dep-pi</ta>
            <ta e="T2205" id="Seg_1792" s="T2204">dĭ</ta>
            <ta e="T2206" id="Seg_1793" s="T2205">krupa-m</ta>
            <ta e="T2207" id="Seg_1794" s="T2206">nüke-ndə</ta>
            <ta e="T2208" id="Seg_1795" s="T2207">dĭgəttə</ta>
            <ta e="T2209" id="Seg_1796" s="T2208">onʼiʔ</ta>
            <ta e="T2210" id="Seg_1797" s="T2209">urgo</ta>
            <ta e="T2211" id="Seg_1798" s="T2210">koʔbdo-nə</ta>
            <ta e="T2212" id="Seg_1799" s="T2211">măn-də</ta>
            <ta e="T2213" id="Seg_1800" s="T2212">šer-e-ʔ</ta>
            <ta e="T2214" id="Seg_1801" s="T2213">oldʼa</ta>
            <ta e="T2215" id="Seg_1802" s="T2214">kuvas</ta>
            <ta e="T2216" id="Seg_1803" s="T2215">dĭ</ta>
            <ta e="T2217" id="Seg_1804" s="T2216">šer-bi</ta>
            <ta e="T2218" id="Seg_1805" s="T2217">da</ta>
            <ta e="T2219" id="Seg_1806" s="T2218">kam-bi</ta>
            <ta e="T2220" id="Seg_1807" s="T2219">nʼiʔdə</ta>
            <ta e="T2221" id="Seg_1808" s="T2220">dĭ-m</ta>
            <ta e="T2222" id="Seg_1809" s="T2221">i-luʔ-pi</ta>
            <ta e="T2223" id="Seg_1810" s="T2222">kuja</ta>
            <ta e="T2224" id="Seg_1811" s="T2223">dĭgəttə</ta>
            <ta e="T2225" id="Seg_1812" s="T2224">baška</ta>
            <ta e="T2226" id="Seg_1813" s="T2225">koʔbdo-t-siʔ</ta>
            <ta e="T2227" id="Seg_1814" s="T2226">šer-e-ʔ</ta>
            <ta e="T2228" id="Seg_1815" s="T2227">oldʼa</ta>
            <ta e="T2229" id="Seg_1816" s="T2228">kuvas</ta>
            <ta e="T2230" id="Seg_1817" s="T2229">i</ta>
            <ta e="T2231" id="Seg_1818" s="T2230">kan-a-ʔ</ta>
            <ta e="T2232" id="Seg_1819" s="T2231">nʼiʔdə</ta>
            <ta e="T2233" id="Seg_1820" s="T2232">dĭ</ta>
            <ta e="T2234" id="Seg_1821" s="T2233">kam-bi</ta>
            <ta e="T2235" id="Seg_1822" s="T2234">dĭ-m</ta>
            <ta e="T2236" id="Seg_1823" s="T2235">mesʼats</ta>
            <ta e="T2237" id="Seg_1824" s="T2236">i-bi</ta>
            <ta e="T2238" id="Seg_1825" s="T2237">dĭgəttə</ta>
            <ta e="T2239" id="Seg_1826" s="T2238">nagur-go</ta>
            <ta e="T2240" id="Seg_1827" s="T2239">koʔbdo-nə</ta>
            <ta e="T2241" id="Seg_1828" s="T2240">măn-də</ta>
            <ta e="T2242" id="Seg_1829" s="T2241">šer-e-ʔ</ta>
            <ta e="T2243" id="Seg_1830" s="T2242">oldʼa</ta>
            <ta e="T2244" id="Seg_1831" s="T2243">kuvas</ta>
            <ta e="T2245" id="Seg_1832" s="T2244">i</ta>
            <ta e="T2246" id="Seg_1833" s="T2245">kan-a-ʔ</ta>
            <ta e="T2247" id="Seg_1834" s="T2246">nʼiʔdə</ta>
            <ta e="T2248" id="Seg_1835" s="T2247">dĭ</ta>
            <ta e="T2249" id="Seg_1836" s="T2248">kam-bi</ta>
            <ta e="T2250" id="Seg_1837" s="T2249">dĭ-m</ta>
            <ta e="T2251" id="Seg_1838" s="T2250">tože</ta>
            <ta e="T2252" id="Seg_1839" s="T2251">i-luʔ-pi</ta>
            <ta e="T2253" id="Seg_1840" s="T2252">voron</ta>
            <ta e="T2254" id="Seg_1841" s="T2253">dĭgəttə</ta>
            <ta e="T2255" id="Seg_1842" s="T2254">dĭ</ta>
            <ta e="T2257" id="Seg_1843" s="T2256">dĭ</ta>
            <ta e="T2259" id="Seg_1844" s="T2258">büzʼe</ta>
            <ta e="T2260" id="Seg_1845" s="T2259">măn-də</ta>
            <ta e="T2261" id="Seg_1846" s="T2260">kal-la-m</ta>
            <ta e="T2262" id="Seg_1847" s="T2261">koʔbdo-nə</ta>
            <ta e="T2263" id="Seg_1848" s="T2262">jada-j-laʔ</ta>
            <ta e="T2264" id="Seg_1849" s="T2263">šo-bi</ta>
            <ta e="T2265" id="Seg_1850" s="T2264">ĭmbi-ziʔ</ta>
            <ta e="T2266" id="Seg_1851" s="T2265">tănan</ta>
            <ta e="T2267" id="Seg_1852" s="T2266">bădə-sʼtə</ta>
            <ta e="T2268" id="Seg_1853" s="T2267">da</ta>
            <ta e="T2269" id="Seg_1854" s="T2268">măn</ta>
            <ta e="T2270" id="Seg_1855" s="T2269">măn</ta>
            <ta e="T2271" id="Seg_1856" s="T2270">nʼe</ta>
            <ta e="T2272" id="Seg_1857" s="T2271">axota</ta>
            <ta e="T2273" id="Seg_1858" s="T2272">am-zittə</ta>
            <ta e="T2274" id="Seg_1859" s="T2273">dĭgəttə</ta>
            <ta e="T2275" id="Seg_1860" s="T2274">dĭ</ta>
            <ta e="T2276" id="Seg_1861" s="T2275">aladʼa-ʔi</ta>
            <ta e="T2277" id="Seg_1862" s="T2276">da</ta>
            <ta e="T2279" id="Seg_1863" s="T2278">koʔbdo-t</ta>
            <ta e="T2280" id="Seg_1864" s="T2279">aladʼa-ʔi</ta>
            <ta e="T2281" id="Seg_1865" s="T2280">em-bi</ta>
            <ta e="T2282" id="Seg_1866" s="T2281">skavarada-ndə</ta>
            <ta e="T2283" id="Seg_1867" s="T2282">nuldə-bi</ta>
            <ta e="T2284" id="Seg_1868" s="T2283">sontsə-nə</ta>
            <ta e="T2285" id="Seg_1869" s="T2284">dĭ</ta>
            <ta e="T2286" id="Seg_1870" s="T2285">pür-luʔ-pi-ʔi</ta>
            <ta e="T2287" id="Seg_1871" s="T2286">dĭ</ta>
            <ta e="T2288" id="Seg_1872" s="T2287">büzʼe</ta>
            <ta e="T2289" id="Seg_1873" s="T2288">am-bi</ta>
            <ta e="T2290" id="Seg_1874" s="T2289">i</ta>
            <ta e="T2291" id="Seg_1875" s="T2290">šo-bi</ta>
            <ta e="T2292" id="Seg_1876" s="T2291">maːʔ-ndə</ta>
            <ta e="T2293" id="Seg_1877" s="T2292">nüke-ndə</ta>
            <ta e="T2294" id="Seg_1878" s="T2293">măn-də</ta>
            <ta e="T2295" id="Seg_1879" s="T2294">pür-ə-ʔ</ta>
            <ta e="T2296" id="Seg_1880" s="T2295">aladʼa-ʔi</ta>
            <ta e="T2297" id="Seg_1881" s="T2296">dĭ</ta>
            <ta e="T2298" id="Seg_1882" s="T2297">davaj</ta>
            <ta e="T2299" id="Seg_1883" s="T2298">pür-zittə</ta>
            <ta e="T2300" id="Seg_1884" s="T2299">i</ta>
            <ta e="T2301" id="Seg_1885" s="T2300">aŋ-də</ta>
            <ta e="T2302" id="Seg_1886" s="T2301">nuldə-bi</ta>
            <ta e="T2303" id="Seg_1887" s="T2302">nulaʔ-bi-ʔi</ta>
            <ta e="T2304" id="Seg_1888" s="T2303">nulaʔ-bi-ʔi</ta>
            <ta e="T2305" id="Seg_1889" s="T2304">ej</ta>
            <ta e="T2306" id="Seg_1890" s="T2305">pür-bi-ʔi</ta>
            <ta e="T2307" id="Seg_1891" s="T2306">dĭgəttə</ta>
            <ta e="T2308" id="Seg_1892" s="T2307">dĭ</ta>
            <ta e="T2309" id="Seg_1893" s="T2308">pʼeš-də</ta>
            <ta e="T2310" id="Seg_1894" s="T2309">em-bi</ta>
            <ta e="T2311" id="Seg_1895" s="T2310">dĭn</ta>
            <ta e="T2312" id="Seg_1896" s="T2311">pür-bi</ta>
            <ta e="T2313" id="Seg_1897" s="T2312">i</ta>
            <ta e="T2314" id="Seg_1898" s="T2313">büzʼe</ta>
            <ta e="T2315" id="Seg_1899" s="T2314">am-bi</ta>
            <ta e="T2316" id="Seg_1900" s="T2315">dĭgəttə</ta>
            <ta e="T2317" id="Seg_1901" s="T2316">kandə-ga</ta>
            <ta e="T2318" id="Seg_1902" s="T2317">mesʼats-də</ta>
            <ta e="T2319" id="Seg_1903" s="T2318">šo-bi</ta>
            <ta e="T2320" id="Seg_1904" s="T2319">a</ta>
            <ta e="T2321" id="Seg_1905" s="T2320">dĭ</ta>
            <ta e="T2322" id="Seg_1906" s="T2321">moltʼa</ta>
            <ta e="T2323" id="Seg_1907" s="T2322">nendə-bi</ta>
            <ta e="T2324" id="Seg_1908" s="T2323">măn-də</ta>
            <ta e="T2325" id="Seg_1909" s="T2324">no</ta>
            <ta e="T2326" id="Seg_1910" s="T2325">kan-a-ʔ</ta>
            <ta e="T2327" id="Seg_1911" s="T2326">moltʼa-nə</ta>
            <ta e="T2328" id="Seg_1912" s="T2327">a</ta>
            <ta e="T2329" id="Seg_1913" s="T2328">dĭn</ta>
            <ta e="T2330" id="Seg_1914" s="T2329">ĭmbi=də</ta>
            <ta e="T2331" id="Seg_1915" s="T2330">ej</ta>
            <ta e="T2332" id="Seg_1916" s="T2331">ed-lia</ta>
            <ta e="T2333" id="Seg_1917" s="T2332">kan-a-ʔ</ta>
            <ta e="T2334" id="Seg_1918" s="T2333">dĭ</ta>
            <ta e="T2335" id="Seg_1919" s="T2334">kam-bi</ta>
            <ta e="T2336" id="Seg_1920" s="T2335">dĭ</ta>
            <ta e="T2337" id="Seg_1921" s="T2336">müjə-bə</ta>
            <ta e="T2338" id="Seg_1922" s="T2337">dibər</ta>
            <ta e="T2339" id="Seg_1923" s="T2338">paʔ-luʔ-pi</ta>
            <ta e="T2340" id="Seg_1924" s="T2339">dĭ</ta>
            <ta e="T2341" id="Seg_1925" s="T2340">băzaj-də-bi</ta>
            <ta e="T2342" id="Seg_1926" s="T2341">dĭn</ta>
            <ta e="T2343" id="Seg_1927" s="T2342">bar</ta>
            <ta e="T2344" id="Seg_1928" s="T2343">ed-laʔbə</ta>
            <ta e="T2345" id="Seg_1929" s="T2344">dĭgəttə</ta>
            <ta e="T2346" id="Seg_1930" s="T2345">ma-ndə</ta>
            <ta e="T2347" id="Seg_1931" s="T2346">šo-bi</ta>
            <ta e="T2348" id="Seg_1932" s="T2347">nendə-ʔ</ta>
            <ta e="T2349" id="Seg_1933" s="T2348">moltʼa</ta>
            <ta e="T2350" id="Seg_1934" s="T2349">nüke-t</ta>
            <ta e="T2351" id="Seg_1935" s="T2350">nendə-bi</ta>
            <ta e="T2352" id="Seg_1936" s="T2351">no</ta>
            <ta e="T2354" id="Seg_1937" s="T2353">băza-ʔ</ta>
            <ta e="T2355" id="Seg_1938" s="T2354">da</ta>
            <ta e="T2356" id="Seg_1939" s="T2355">dĭn</ta>
            <ta e="T2357" id="Seg_1940" s="T2356">šü</ta>
            <ta e="T2358" id="Seg_1941" s="T2357">naga</ta>
            <ta e="T2359" id="Seg_1942" s="T2358">kan-a-ʔ</ta>
            <ta e="T2360" id="Seg_1943" s="T2359">dĭ</ta>
            <ta e="T2361" id="Seg_1944" s="T2360">ši</ta>
            <ta e="T2362" id="Seg_1945" s="T2361">a-bi</ta>
            <ta e="T2363" id="Seg_1946" s="T2362">dibər</ta>
            <ta e="T2364" id="Seg_1947" s="T2363">müjə-t</ta>
            <ta e="T2365" id="Seg_1948" s="T2364">paʔ-də-bi</ta>
            <ta e="T2366" id="Seg_1949" s="T2365">nüke-t</ta>
            <ta e="T2367" id="Seg_1950" s="T2366">uge</ta>
            <ta e="T2368" id="Seg_1951" s="T2367">kirgar-laʔbə</ta>
            <ta e="T2369" id="Seg_1952" s="T2368">naga</ta>
            <ta e="T2370" id="Seg_1953" s="T2369">šü</ta>
            <ta e="T2371" id="Seg_1954" s="T2370">naga</ta>
            <ta e="T2372" id="Seg_1955" s="T2371">šü</ta>
            <ta e="T2373" id="Seg_1956" s="T2372">dĭgəttə</ta>
            <ta e="T2374" id="Seg_1957" s="T2373">băzaj-də-bi-ʔi</ta>
            <ta e="T2375" id="Seg_1958" s="T2374">šü</ta>
            <ta e="T2376" id="Seg_1959" s="T2375">nendə-bi-ʔi</ta>
            <ta e="T2377" id="Seg_1960" s="T2376">dĭgəttə</ta>
            <ta e="T2378" id="Seg_1961" s="T2377">erte-n</ta>
            <ta e="T2379" id="Seg_1962" s="T2378">uʔ-laʔbə</ta>
            <ta e="T2382" id="Seg_1963" s="T2381">kam-bi</ta>
            <ta e="T2383" id="Seg_1964" s="T2382">baška</ta>
            <ta e="T2384" id="Seg_1965" s="T2383">koʔbdo-nə</ta>
            <ta e="T2385" id="Seg_1966" s="T2384">dĭn</ta>
            <ta e="T2386" id="Seg_1967" s="T2385">voron-də</ta>
            <ta e="T2387" id="Seg_1968" s="T2386">dĭn</ta>
            <ta e="T2388" id="Seg_1969" s="T2387">măn-də</ta>
            <ta e="T2389" id="Seg_1970" s="T2388">ĭmbi</ta>
            <ta e="T2390" id="Seg_1971" s="T2389">tănan</ta>
            <ta e="T2391" id="Seg_1972" s="T2390">mĭ-zittə</ta>
            <ta e="T2392" id="Seg_1973" s="T2391">nu</ta>
            <ta e="T2393" id="Seg_1974" s="T2392">davaj</ta>
            <ta e="T2394" id="Seg_1975" s="T2393">kunol-zittə</ta>
            <ta e="T2395" id="Seg_1976" s="T2394">dĭ</ta>
            <ta e="T2396" id="Seg_1977" s="T2395">dĭ</ta>
            <ta e="T2397" id="Seg_1978" s="T2396">i-bi</ta>
            <ta e="T2398" id="Seg_1979" s="T2397">dĭ-m</ta>
            <ta e="T2399" id="Seg_1980" s="T2398">krɨlo-ndə</ta>
            <ta e="T2400" id="Seg_1981" s="T2399">šide-göʔ</ta>
            <ta e="T2401" id="Seg_1982" s="T2400">kunol-luʔ-pi-ʔi</ta>
            <ta e="T2402" id="Seg_1983" s="T2401">i</ta>
            <ta e="T2403" id="Seg_1984" s="T2402">uz-leʔ-pi-ʔi</ta>
            <ta e="T2404" id="Seg_1985" s="T2403">i</ta>
            <ta e="T2405" id="Seg_1986" s="T2404">kü-laːm-bi-ʔi</ta>
            <ta e="T2406" id="Seg_1987" s="T2405">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2146" id="Seg_1988" s="T2145">amno-laʔbə-bi</ta>
            <ta e="T2147" id="Seg_1989" s="T2146">nüke</ta>
            <ta e="T2148" id="Seg_1990" s="T2147">büzʼe</ta>
            <ta e="T2149" id="Seg_1991" s="T2148">dĭ-n</ta>
            <ta e="T2150" id="Seg_1992" s="T2149">nagur</ta>
            <ta e="T2151" id="Seg_1993" s="T2150">koʔbdo</ta>
            <ta e="T2152" id="Seg_1994" s="T2151">i-bi</ta>
            <ta e="T2153" id="Seg_1995" s="T2152">dĭ</ta>
            <ta e="T2154" id="Seg_1996" s="T2153">kan-bi</ta>
            <ta e="T2156" id="Seg_1997" s="T2155">anbar-Tə</ta>
            <ta e="T2157" id="Seg_1998" s="T2156">krupa</ta>
            <ta e="T2158" id="Seg_1999" s="T2157">i-bi</ta>
            <ta e="T2159" id="Seg_2000" s="T2158">băra-Tə</ta>
            <ta e="T2160" id="Seg_2001" s="T2159">šo-bi</ta>
            <ta e="T2161" id="Seg_2002" s="T2160">a</ta>
            <ta e="T2162" id="Seg_2003" s="T2161">băra-Kən</ta>
            <ta e="T2163" id="Seg_2004" s="T2162">ši</ta>
            <ta e="T2164" id="Seg_2005" s="T2163">i-bi</ta>
            <ta e="T2165" id="Seg_2006" s="T2164">šonə-bi</ta>
            <ta e="T2166" id="Seg_2007" s="T2165">šonə-bi</ta>
            <ta e="T2167" id="Seg_2008" s="T2166">a</ta>
            <ta e="T2168" id="Seg_2009" s="T2167">krupa-t</ta>
            <ta e="T2169" id="Seg_2010" s="T2168">bar</ta>
            <ta e="T2170" id="Seg_2011" s="T2169">kamə-luʔbdə-bi</ta>
            <ta e="T2171" id="Seg_2012" s="T2170">nüke-t</ta>
            <ta e="T2172" id="Seg_2013" s="T2171">măn-ntə</ta>
            <ta e="T2173" id="Seg_2014" s="T2172">gijen</ta>
            <ta e="T2174" id="Seg_2015" s="T2173">tăn</ta>
            <ta e="T2175" id="Seg_2016" s="T2174">krupa</ta>
            <ta e="T2176" id="Seg_2017" s="T2175">da</ta>
            <ta e="T2177" id="Seg_2018" s="T2176">kan-luʔbdə-bi-m</ta>
            <ta e="T2178" id="Seg_2019" s="T2177">dĭgəttə</ta>
            <ta e="T2179" id="Seg_2020" s="T2178">kan-bi</ta>
            <ta e="T2180" id="Seg_2021" s="T2179">măn-ntə</ta>
            <ta e="T2181" id="Seg_2022" s="T2180">kuja</ta>
            <ta e="T2182" id="Seg_2023" s="T2181">kabažar-ə-ʔ</ta>
            <ta e="T2183" id="Seg_2024" s="T2182">mesʼats-Tə</ta>
            <ta e="T2184" id="Seg_2025" s="T2183">măn-ntə</ta>
            <ta e="T2185" id="Seg_2026" s="T2184">kabažar-ə-ʔ</ta>
            <ta e="T2186" id="Seg_2027" s="T2185">i</ta>
            <ta e="T2187" id="Seg_2028" s="T2186">voron</ta>
            <ta e="T2188" id="Seg_2029" s="T2187">voron-Tə</ta>
            <ta e="T2189" id="Seg_2030" s="T2188">măn-ntə</ta>
            <ta e="T2190" id="Seg_2031" s="T2189">kabažar-ə-ʔ</ta>
            <ta e="T2191" id="Seg_2032" s="T2190">dĭ-zAŋ</ta>
            <ta e="T2192" id="Seg_2033" s="T2191">šo-bi-jəʔ</ta>
            <ta e="T2193" id="Seg_2034" s="T2192">i</ta>
            <ta e="T2195" id="Seg_2035" s="T2194">kabažar-bi-jəʔ</ta>
            <ta e="T2196" id="Seg_2036" s="T2195">dĭ</ta>
            <ta e="T2197" id="Seg_2037" s="T2196">măn</ta>
            <ta e="T2198" id="Seg_2038" s="T2197">koʔbdo-zAŋ-də</ta>
            <ta e="T2199" id="Seg_2039" s="T2198">šiʔnʼileʔ</ta>
            <ta e="T2200" id="Seg_2040" s="T2199">mĭ-lV-m</ta>
            <ta e="T2201" id="Seg_2041" s="T2200">tibi-Tə</ta>
            <ta e="T2202" id="Seg_2042" s="T2201">dĭgəttə</ta>
            <ta e="T2203" id="Seg_2043" s="T2202">šo-bi</ta>
            <ta e="T2204" id="Seg_2044" s="T2203">det-bi</ta>
            <ta e="T2205" id="Seg_2045" s="T2204">dĭ</ta>
            <ta e="T2206" id="Seg_2046" s="T2205">krupa-m</ta>
            <ta e="T2207" id="Seg_2047" s="T2206">nüke-gəndə</ta>
            <ta e="T2208" id="Seg_2048" s="T2207">dĭgəttə</ta>
            <ta e="T2209" id="Seg_2049" s="T2208">onʼiʔ</ta>
            <ta e="T2210" id="Seg_2050" s="T2209">urgo</ta>
            <ta e="T2211" id="Seg_2051" s="T2210">koʔbdo-nə</ta>
            <ta e="T2212" id="Seg_2052" s="T2211">măn-ntə</ta>
            <ta e="T2213" id="Seg_2053" s="T2212">šer-ə-ʔ</ta>
            <ta e="T2214" id="Seg_2054" s="T2213">oldʼa</ta>
            <ta e="T2215" id="Seg_2055" s="T2214">kuvas</ta>
            <ta e="T2216" id="Seg_2056" s="T2215">dĭ</ta>
            <ta e="T2217" id="Seg_2057" s="T2216">šer-bi</ta>
            <ta e="T2218" id="Seg_2058" s="T2217">da</ta>
            <ta e="T2219" id="Seg_2059" s="T2218">kan-bi</ta>
            <ta e="T2220" id="Seg_2060" s="T2219">nʼiʔdə</ta>
            <ta e="T2221" id="Seg_2061" s="T2220">dĭ-m</ta>
            <ta e="T2222" id="Seg_2062" s="T2221">i-luʔbdə-bi</ta>
            <ta e="T2223" id="Seg_2063" s="T2222">kuja</ta>
            <ta e="T2224" id="Seg_2064" s="T2223">dĭgəttə</ta>
            <ta e="T2225" id="Seg_2065" s="T2224">baška</ta>
            <ta e="T2226" id="Seg_2066" s="T2225">koʔbdo-t-ziʔ</ta>
            <ta e="T2227" id="Seg_2067" s="T2226">šer-ə-ʔ</ta>
            <ta e="T2228" id="Seg_2068" s="T2227">oldʼa</ta>
            <ta e="T2229" id="Seg_2069" s="T2228">kuvas</ta>
            <ta e="T2230" id="Seg_2070" s="T2229">i</ta>
            <ta e="T2231" id="Seg_2071" s="T2230">kan-ə-ʔ</ta>
            <ta e="T2232" id="Seg_2072" s="T2231">nʼiʔdə</ta>
            <ta e="T2233" id="Seg_2073" s="T2232">dĭ</ta>
            <ta e="T2234" id="Seg_2074" s="T2233">kan-bi</ta>
            <ta e="T2235" id="Seg_2075" s="T2234">dĭ-m</ta>
            <ta e="T2236" id="Seg_2076" s="T2235">mesʼats</ta>
            <ta e="T2237" id="Seg_2077" s="T2236">i-bi</ta>
            <ta e="T2238" id="Seg_2078" s="T2237">dĭgəttə</ta>
            <ta e="T2239" id="Seg_2079" s="T2238">nagur-git</ta>
            <ta e="T2240" id="Seg_2080" s="T2239">koʔbdo-nə</ta>
            <ta e="T2241" id="Seg_2081" s="T2240">măn-ntə</ta>
            <ta e="T2242" id="Seg_2082" s="T2241">šer-ə-ʔ</ta>
            <ta e="T2243" id="Seg_2083" s="T2242">oldʼa</ta>
            <ta e="T2244" id="Seg_2084" s="T2243">kuvas</ta>
            <ta e="T2245" id="Seg_2085" s="T2244">i</ta>
            <ta e="T2246" id="Seg_2086" s="T2245">kan-ə-ʔ</ta>
            <ta e="T2247" id="Seg_2087" s="T2246">nʼiʔdə</ta>
            <ta e="T2248" id="Seg_2088" s="T2247">dĭ</ta>
            <ta e="T2249" id="Seg_2089" s="T2248">kan-bi</ta>
            <ta e="T2250" id="Seg_2090" s="T2249">dĭ-m</ta>
            <ta e="T2251" id="Seg_2091" s="T2250">tože</ta>
            <ta e="T2252" id="Seg_2092" s="T2251">i-luʔbdə-bi</ta>
            <ta e="T2253" id="Seg_2093" s="T2252">voron</ta>
            <ta e="T2254" id="Seg_2094" s="T2253">dĭgəttə</ta>
            <ta e="T2255" id="Seg_2095" s="T2254">dĭ</ta>
            <ta e="T2257" id="Seg_2096" s="T2256">dĭ</ta>
            <ta e="T2259" id="Seg_2097" s="T2258">büzʼe</ta>
            <ta e="T2260" id="Seg_2098" s="T2259">măn-ntə</ta>
            <ta e="T2261" id="Seg_2099" s="T2260">kan-lV-m</ta>
            <ta e="T2262" id="Seg_2100" s="T2261">koʔbdo-nə</ta>
            <ta e="T2263" id="Seg_2101" s="T2262">jada-j-lAʔ</ta>
            <ta e="T2264" id="Seg_2102" s="T2263">šo-bi</ta>
            <ta e="T2265" id="Seg_2103" s="T2264">ĭmbi-ziʔ</ta>
            <ta e="T2266" id="Seg_2104" s="T2265">tănan</ta>
            <ta e="T2267" id="Seg_2105" s="T2266">bădə-zittə</ta>
            <ta e="T2268" id="Seg_2106" s="T2267">da</ta>
            <ta e="T2269" id="Seg_2107" s="T2268">măn</ta>
            <ta e="T2270" id="Seg_2108" s="T2269">măn</ta>
            <ta e="T2271" id="Seg_2109" s="T2270">nʼe</ta>
            <ta e="T2272" id="Seg_2110" s="T2271">axota</ta>
            <ta e="T2273" id="Seg_2111" s="T2272">am-zittə</ta>
            <ta e="T2274" id="Seg_2112" s="T2273">dĭgəttə</ta>
            <ta e="T2275" id="Seg_2113" s="T2274">dĭ</ta>
            <ta e="T2276" id="Seg_2114" s="T2275">aladʼa-jəʔ</ta>
            <ta e="T2277" id="Seg_2115" s="T2276">da</ta>
            <ta e="T2279" id="Seg_2116" s="T2278">koʔbdo-t</ta>
            <ta e="T2280" id="Seg_2117" s="T2279">aladʼa-jəʔ</ta>
            <ta e="T2281" id="Seg_2118" s="T2280">hen-bi</ta>
            <ta e="T2282" id="Seg_2119" s="T2281">skavarada-gəndə</ta>
            <ta e="T2283" id="Seg_2120" s="T2282">nuldə-bi</ta>
            <ta e="T2284" id="Seg_2121" s="T2283">sontsə-Tə</ta>
            <ta e="T2285" id="Seg_2122" s="T2284">dĭ</ta>
            <ta e="T2286" id="Seg_2123" s="T2285">pür-luʔbdə-bi-jəʔ</ta>
            <ta e="T2287" id="Seg_2124" s="T2286">dĭ</ta>
            <ta e="T2288" id="Seg_2125" s="T2287">büzʼe</ta>
            <ta e="T2289" id="Seg_2126" s="T2288">am-bi</ta>
            <ta e="T2290" id="Seg_2127" s="T2289">i</ta>
            <ta e="T2291" id="Seg_2128" s="T2290">šo-bi</ta>
            <ta e="T2292" id="Seg_2129" s="T2291">maʔ-gəndə</ta>
            <ta e="T2293" id="Seg_2130" s="T2292">nüke-gəndə</ta>
            <ta e="T2294" id="Seg_2131" s="T2293">măn-ntə</ta>
            <ta e="T2295" id="Seg_2132" s="T2294">pür-ə-ʔ</ta>
            <ta e="T2296" id="Seg_2133" s="T2295">aladʼa-jəʔ</ta>
            <ta e="T2297" id="Seg_2134" s="T2296">dĭ</ta>
            <ta e="T2298" id="Seg_2135" s="T2297">davaj</ta>
            <ta e="T2299" id="Seg_2136" s="T2298">pür-zittə</ta>
            <ta e="T2300" id="Seg_2137" s="T2299">i</ta>
            <ta e="T2301" id="Seg_2138" s="T2300">aŋ-Tə</ta>
            <ta e="T2302" id="Seg_2139" s="T2301">nuldə-bi</ta>
            <ta e="T2303" id="Seg_2140" s="T2302">nulaʔ-bi-jəʔ</ta>
            <ta e="T2304" id="Seg_2141" s="T2303">nulaʔ-bi-jəʔ</ta>
            <ta e="T2305" id="Seg_2142" s="T2304">ej</ta>
            <ta e="T2306" id="Seg_2143" s="T2305">pür-bi-jəʔ</ta>
            <ta e="T2307" id="Seg_2144" s="T2306">dĭgəttə</ta>
            <ta e="T2308" id="Seg_2145" s="T2307">dĭ</ta>
            <ta e="T2309" id="Seg_2146" s="T2308">pʼeːš-Tə</ta>
            <ta e="T2310" id="Seg_2147" s="T2309">hen-bi</ta>
            <ta e="T2311" id="Seg_2148" s="T2310">dĭn</ta>
            <ta e="T2312" id="Seg_2149" s="T2311">pür-bi</ta>
            <ta e="T2313" id="Seg_2150" s="T2312">i</ta>
            <ta e="T2314" id="Seg_2151" s="T2313">büzʼe</ta>
            <ta e="T2315" id="Seg_2152" s="T2314">am-bi</ta>
            <ta e="T2316" id="Seg_2153" s="T2315">dĭgəttə</ta>
            <ta e="T2317" id="Seg_2154" s="T2316">kandə-gA</ta>
            <ta e="T2318" id="Seg_2155" s="T2317">mesʼats-Tə</ta>
            <ta e="T2319" id="Seg_2156" s="T2318">šo-bi</ta>
            <ta e="T2320" id="Seg_2157" s="T2319">a</ta>
            <ta e="T2321" id="Seg_2158" s="T2320">dĭ</ta>
            <ta e="T2322" id="Seg_2159" s="T2321">multʼa</ta>
            <ta e="T2323" id="Seg_2160" s="T2322">nendə-bi</ta>
            <ta e="T2324" id="Seg_2161" s="T2323">măn-ntə</ta>
            <ta e="T2325" id="Seg_2162" s="T2324">no</ta>
            <ta e="T2326" id="Seg_2163" s="T2325">kan-ə-ʔ</ta>
            <ta e="T2327" id="Seg_2164" s="T2326">multʼa-Tə</ta>
            <ta e="T2328" id="Seg_2165" s="T2327">a</ta>
            <ta e="T2329" id="Seg_2166" s="T2328">dĭn</ta>
            <ta e="T2330" id="Seg_2167" s="T2329">ĭmbi=də</ta>
            <ta e="T2331" id="Seg_2168" s="T2330">ej</ta>
            <ta e="T2332" id="Seg_2169" s="T2331">ed-liA</ta>
            <ta e="T2333" id="Seg_2170" s="T2332">kan-ə-ʔ</ta>
            <ta e="T2334" id="Seg_2171" s="T2333">dĭ</ta>
            <ta e="T2335" id="Seg_2172" s="T2334">kan-bi</ta>
            <ta e="T2336" id="Seg_2173" s="T2335">dĭ</ta>
            <ta e="T2337" id="Seg_2174" s="T2336">müjə-bə</ta>
            <ta e="T2338" id="Seg_2175" s="T2337">dĭbər</ta>
            <ta e="T2339" id="Seg_2176" s="T2338">paʔ-luʔbdə-bi</ta>
            <ta e="T2340" id="Seg_2177" s="T2339">dĭ</ta>
            <ta e="T2341" id="Seg_2178" s="T2340">băzəj-ntə-bi</ta>
            <ta e="T2342" id="Seg_2179" s="T2341">dĭn</ta>
            <ta e="T2343" id="Seg_2180" s="T2342">bar</ta>
            <ta e="T2344" id="Seg_2181" s="T2343">ed-laʔbə</ta>
            <ta e="T2345" id="Seg_2182" s="T2344">dĭgəttə</ta>
            <ta e="T2346" id="Seg_2183" s="T2345">maʔ-gəndə</ta>
            <ta e="T2347" id="Seg_2184" s="T2346">šo-bi</ta>
            <ta e="T2348" id="Seg_2185" s="T2347">nendə-ʔ</ta>
            <ta e="T2349" id="Seg_2186" s="T2348">multʼa</ta>
            <ta e="T2350" id="Seg_2187" s="T2349">nüke-t</ta>
            <ta e="T2351" id="Seg_2188" s="T2350">nendə-bi</ta>
            <ta e="T2352" id="Seg_2189" s="T2351">no</ta>
            <ta e="T2354" id="Seg_2190" s="T2353">bazə-ʔ</ta>
            <ta e="T2355" id="Seg_2191" s="T2354">da</ta>
            <ta e="T2356" id="Seg_2192" s="T2355">dĭn</ta>
            <ta e="T2357" id="Seg_2193" s="T2356">šü</ta>
            <ta e="T2358" id="Seg_2194" s="T2357">naga</ta>
            <ta e="T2359" id="Seg_2195" s="T2358">kan-ə-ʔ</ta>
            <ta e="T2360" id="Seg_2196" s="T2359">dĭ</ta>
            <ta e="T2361" id="Seg_2197" s="T2360">ši</ta>
            <ta e="T2362" id="Seg_2198" s="T2361">a-bi</ta>
            <ta e="T2363" id="Seg_2199" s="T2362">dĭbər</ta>
            <ta e="T2364" id="Seg_2200" s="T2363">müjə-t</ta>
            <ta e="T2365" id="Seg_2201" s="T2364">paʔ-də-bi</ta>
            <ta e="T2366" id="Seg_2202" s="T2365">nüke-t</ta>
            <ta e="T2367" id="Seg_2203" s="T2366">üge</ta>
            <ta e="T2368" id="Seg_2204" s="T2367">kirgaːr-laʔbə</ta>
            <ta e="T2369" id="Seg_2205" s="T2368">naga</ta>
            <ta e="T2370" id="Seg_2206" s="T2369">šü</ta>
            <ta e="T2371" id="Seg_2207" s="T2370">naga</ta>
            <ta e="T2372" id="Seg_2208" s="T2371">šü</ta>
            <ta e="T2373" id="Seg_2209" s="T2372">dĭgəttə</ta>
            <ta e="T2374" id="Seg_2210" s="T2373">băzəj-ntə-bi-jəʔ</ta>
            <ta e="T2375" id="Seg_2211" s="T2374">šü</ta>
            <ta e="T2376" id="Seg_2212" s="T2375">nendə-bi-jəʔ</ta>
            <ta e="T2377" id="Seg_2213" s="T2376">dĭgəttə</ta>
            <ta e="T2378" id="Seg_2214" s="T2377">ertə-n</ta>
            <ta e="T2379" id="Seg_2215" s="T2378">uʔbdə-laʔbə</ta>
            <ta e="T2382" id="Seg_2216" s="T2381">kan-bi</ta>
            <ta e="T2383" id="Seg_2217" s="T2382">baška</ta>
            <ta e="T2384" id="Seg_2218" s="T2383">koʔbdo-nə</ta>
            <ta e="T2385" id="Seg_2219" s="T2384">dĭn</ta>
            <ta e="T2386" id="Seg_2220" s="T2385">voron-Tə</ta>
            <ta e="T2387" id="Seg_2221" s="T2386">dĭn</ta>
            <ta e="T2388" id="Seg_2222" s="T2387">măn-ntə</ta>
            <ta e="T2389" id="Seg_2223" s="T2388">ĭmbi</ta>
            <ta e="T2390" id="Seg_2224" s="T2389">tănan</ta>
            <ta e="T2391" id="Seg_2225" s="T2390">mĭ-zittə</ta>
            <ta e="T2392" id="Seg_2226" s="T2391">nu</ta>
            <ta e="T2393" id="Seg_2227" s="T2392">davaj</ta>
            <ta e="T2394" id="Seg_2228" s="T2393">kunol-zittə</ta>
            <ta e="T2395" id="Seg_2229" s="T2394">dĭ</ta>
            <ta e="T2396" id="Seg_2230" s="T2395">dĭ</ta>
            <ta e="T2397" id="Seg_2231" s="T2396">i-bi</ta>
            <ta e="T2398" id="Seg_2232" s="T2397">dĭ-m</ta>
            <ta e="T2399" id="Seg_2233" s="T2398">krɨlo-gəndə</ta>
            <ta e="T2400" id="Seg_2234" s="T2399">šide-göʔ</ta>
            <ta e="T2401" id="Seg_2235" s="T2400">kunol-luʔbdə-bi-jəʔ</ta>
            <ta e="T2402" id="Seg_2236" s="T2401">i</ta>
            <ta e="T2403" id="Seg_2237" s="T2402">üzə-laʔbə-bi-jəʔ</ta>
            <ta e="T2404" id="Seg_2238" s="T2403">i</ta>
            <ta e="T2405" id="Seg_2239" s="T2404">kü-laːm-bi-jəʔ</ta>
            <ta e="T2406" id="Seg_2240" s="T2405">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2146" id="Seg_2241" s="T2145">live-DUR-PST</ta>
            <ta e="T2147" id="Seg_2242" s="T2146">woman.[NOM.SG]</ta>
            <ta e="T2148" id="Seg_2243" s="T2147">man.[NOM.SG]</ta>
            <ta e="T2149" id="Seg_2244" s="T2148">this-GEN</ta>
            <ta e="T2150" id="Seg_2245" s="T2149">three.[NOM.SG]</ta>
            <ta e="T2151" id="Seg_2246" s="T2150">girl.[NOM.SG]</ta>
            <ta e="T2152" id="Seg_2247" s="T2151">be-PST.[3SG]</ta>
            <ta e="T2153" id="Seg_2248" s="T2152">this.[NOM.SG]</ta>
            <ta e="T2154" id="Seg_2249" s="T2153">go-PST.[3SG]</ta>
            <ta e="T2156" id="Seg_2250" s="T2155">barn-LAT</ta>
            <ta e="T2157" id="Seg_2251" s="T2156">groats.[NOM.SG]</ta>
            <ta e="T2158" id="Seg_2252" s="T2157">take-PST.[3SG]</ta>
            <ta e="T2159" id="Seg_2253" s="T2158">sack-LAT</ta>
            <ta e="T2160" id="Seg_2254" s="T2159">come-PST.[3SG]</ta>
            <ta e="T2161" id="Seg_2255" s="T2160">and</ta>
            <ta e="T2162" id="Seg_2256" s="T2161">sack-LOC</ta>
            <ta e="T2163" id="Seg_2257" s="T2162">hole.[NOM.SG]</ta>
            <ta e="T2164" id="Seg_2258" s="T2163">be-PST.[3SG]</ta>
            <ta e="T2165" id="Seg_2259" s="T2164">come-PST.[3SG]</ta>
            <ta e="T2166" id="Seg_2260" s="T2165">come-PST.[3SG]</ta>
            <ta e="T2167" id="Seg_2261" s="T2166">and</ta>
            <ta e="T2168" id="Seg_2262" s="T2167">groats-NOM/GEN.3SG</ta>
            <ta e="T2169" id="Seg_2263" s="T2168">all</ta>
            <ta e="T2170" id="Seg_2264" s="T2169">pour-MOM-PST.[3SG]</ta>
            <ta e="T2171" id="Seg_2265" s="T2170">woman-NOM/GEN.3SG</ta>
            <ta e="T2172" id="Seg_2266" s="T2171">say-IPFVZ.[3SG]</ta>
            <ta e="T2173" id="Seg_2267" s="T2172">where</ta>
            <ta e="T2174" id="Seg_2268" s="T2173">you.GEN</ta>
            <ta e="T2175" id="Seg_2269" s="T2174">groats.[NOM.SG]</ta>
            <ta e="T2176" id="Seg_2270" s="T2175">well</ta>
            <ta e="T2177" id="Seg_2271" s="T2176">go-MOM-PST-1SG</ta>
            <ta e="T2178" id="Seg_2272" s="T2177">then</ta>
            <ta e="T2179" id="Seg_2273" s="T2178">go-PST.[3SG]</ta>
            <ta e="T2180" id="Seg_2274" s="T2179">say-IPFVZ.[3SG]</ta>
            <ta e="T2181" id="Seg_2275" s="T2180">sun.[NOM.SG]</ta>
            <ta e="T2182" id="Seg_2276" s="T2181">help-EP-IMP.2SG</ta>
            <ta e="T2183" id="Seg_2277" s="T2182">month-LAT</ta>
            <ta e="T2184" id="Seg_2278" s="T2183">say-IPFVZ.[3SG]</ta>
            <ta e="T2185" id="Seg_2279" s="T2184">help-EP-IMP.2SG</ta>
            <ta e="T2186" id="Seg_2280" s="T2185">and</ta>
            <ta e="T2187" id="Seg_2281" s="T2186">raven.[NOM.SG]</ta>
            <ta e="T2188" id="Seg_2282" s="T2187">raven-LAT</ta>
            <ta e="T2189" id="Seg_2283" s="T2188">say-IPFVZ.[3SG]</ta>
            <ta e="T2190" id="Seg_2284" s="T2189">help-EP-IMP.2SG</ta>
            <ta e="T2191" id="Seg_2285" s="T2190">this-PL</ta>
            <ta e="T2192" id="Seg_2286" s="T2191">come-PST-3PL</ta>
            <ta e="T2193" id="Seg_2287" s="T2192">and</ta>
            <ta e="T2195" id="Seg_2288" s="T2194">help-PST-3PL</ta>
            <ta e="T2196" id="Seg_2289" s="T2195">this.[NOM.SG]</ta>
            <ta e="T2197" id="Seg_2290" s="T2196">I.NOM</ta>
            <ta e="T2198" id="Seg_2291" s="T2197">daughter-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2199" id="Seg_2292" s="T2198">you.PL.ACC</ta>
            <ta e="T2200" id="Seg_2293" s="T2199">give-FUT-1SG</ta>
            <ta e="T2201" id="Seg_2294" s="T2200">man-LAT</ta>
            <ta e="T2202" id="Seg_2295" s="T2201">then</ta>
            <ta e="T2203" id="Seg_2296" s="T2202">come-PST.[3SG]</ta>
            <ta e="T2204" id="Seg_2297" s="T2203">bring-PST.[3SG]</ta>
            <ta e="T2205" id="Seg_2298" s="T2204">this.[NOM.SG]</ta>
            <ta e="T2206" id="Seg_2299" s="T2205">groats-ACC</ta>
            <ta e="T2207" id="Seg_2300" s="T2206">woman-LAT/LOC.3SG</ta>
            <ta e="T2208" id="Seg_2301" s="T2207">then</ta>
            <ta e="T2209" id="Seg_2302" s="T2208">one.[NOM.SG]</ta>
            <ta e="T2210" id="Seg_2303" s="T2209">big.[NOM.SG]</ta>
            <ta e="T2211" id="Seg_2304" s="T2210">daughter-GEN.1SG</ta>
            <ta e="T2212" id="Seg_2305" s="T2211">say-IPFVZ.[3SG]</ta>
            <ta e="T2213" id="Seg_2306" s="T2212">dress-EP-IMP.2SG</ta>
            <ta e="T2214" id="Seg_2307" s="T2213">clothing.[NOM.SG]</ta>
            <ta e="T2215" id="Seg_2308" s="T2214">beautiful.[NOM.SG]</ta>
            <ta e="T2216" id="Seg_2309" s="T2215">this.[NOM.SG]</ta>
            <ta e="T2217" id="Seg_2310" s="T2216">dress-PST.[3SG]</ta>
            <ta e="T2218" id="Seg_2311" s="T2217">and</ta>
            <ta e="T2219" id="Seg_2312" s="T2218">go-PST.[3SG]</ta>
            <ta e="T2220" id="Seg_2313" s="T2219">outwards</ta>
            <ta e="T2221" id="Seg_2314" s="T2220">this-ACC</ta>
            <ta e="T2222" id="Seg_2315" s="T2221">take-MOM-PST.[3SG]</ta>
            <ta e="T2223" id="Seg_2316" s="T2222">sun.[NOM.SG]</ta>
            <ta e="T2224" id="Seg_2317" s="T2223">then</ta>
            <ta e="T2225" id="Seg_2318" s="T2224">another.[NOM.SG]</ta>
            <ta e="T2226" id="Seg_2319" s="T2225">daughter-3SG-INS</ta>
            <ta e="T2227" id="Seg_2320" s="T2226">dress-EP-IMP.2SG</ta>
            <ta e="T2228" id="Seg_2321" s="T2227">clothing.[NOM.SG]</ta>
            <ta e="T2229" id="Seg_2322" s="T2228">beautiful.[NOM.SG]</ta>
            <ta e="T2230" id="Seg_2323" s="T2229">and</ta>
            <ta e="T2231" id="Seg_2324" s="T2230">go-EP-IMP.2SG</ta>
            <ta e="T2232" id="Seg_2325" s="T2231">outwards</ta>
            <ta e="T2233" id="Seg_2326" s="T2232">this.[NOM.SG]</ta>
            <ta e="T2234" id="Seg_2327" s="T2233">go-PST.[3SG]</ta>
            <ta e="T2235" id="Seg_2328" s="T2234">this-ACC</ta>
            <ta e="T2236" id="Seg_2329" s="T2235">month.[NOM.SG]</ta>
            <ta e="T2237" id="Seg_2330" s="T2236">take-PST.[3SG]</ta>
            <ta e="T2238" id="Seg_2331" s="T2237">then</ta>
            <ta e="T2239" id="Seg_2332" s="T2238">three-ORD</ta>
            <ta e="T2240" id="Seg_2333" s="T2239">daughter-GEN.1SG</ta>
            <ta e="T2241" id="Seg_2334" s="T2240">say-IPFVZ.[3SG]</ta>
            <ta e="T2242" id="Seg_2335" s="T2241">dress-EP-IMP.2SG</ta>
            <ta e="T2243" id="Seg_2336" s="T2242">clothing.[NOM.SG]</ta>
            <ta e="T2244" id="Seg_2337" s="T2243">beautiful.[NOM.SG]</ta>
            <ta e="T2245" id="Seg_2338" s="T2244">and</ta>
            <ta e="T2246" id="Seg_2339" s="T2245">go-EP-IMP.2SG</ta>
            <ta e="T2247" id="Seg_2340" s="T2246">outwards</ta>
            <ta e="T2248" id="Seg_2341" s="T2247">this.[NOM.SG]</ta>
            <ta e="T2249" id="Seg_2342" s="T2248">go-PST.[3SG]</ta>
            <ta e="T2250" id="Seg_2343" s="T2249">this-ACC</ta>
            <ta e="T2251" id="Seg_2344" s="T2250">also</ta>
            <ta e="T2252" id="Seg_2345" s="T2251">take-MOM-PST.[3SG]</ta>
            <ta e="T2253" id="Seg_2346" s="T2252">raven.[NOM.SG]</ta>
            <ta e="T2254" id="Seg_2347" s="T2253">then</ta>
            <ta e="T2255" id="Seg_2348" s="T2254">this.[NOM.SG]</ta>
            <ta e="T2257" id="Seg_2349" s="T2256">this.[NOM.SG]</ta>
            <ta e="T2259" id="Seg_2350" s="T2258">man.[NOM.SG]</ta>
            <ta e="T2260" id="Seg_2351" s="T2259">say-IPFVZ.[3SG]</ta>
            <ta e="T2261" id="Seg_2352" s="T2260">go-FUT-1SG</ta>
            <ta e="T2262" id="Seg_2353" s="T2261">daughter-GEN.1SG</ta>
            <ta e="T2263" id="Seg_2354" s="T2262">village-VBLZ-CVB</ta>
            <ta e="T2264" id="Seg_2355" s="T2263">come-PST.[3SG]</ta>
            <ta e="T2265" id="Seg_2356" s="T2264">what-INS</ta>
            <ta e="T2266" id="Seg_2357" s="T2265">you.ACC</ta>
            <ta e="T2267" id="Seg_2358" s="T2266">feed-INF.LAT</ta>
            <ta e="T2268" id="Seg_2359" s="T2267">well</ta>
            <ta e="T2269" id="Seg_2360" s="T2268">I.NOM</ta>
            <ta e="T2270" id="Seg_2361" s="T2269">I.NOM</ta>
            <ta e="T2271" id="Seg_2362" s="T2270">not</ta>
            <ta e="T2272" id="Seg_2363" s="T2271">one.wants</ta>
            <ta e="T2273" id="Seg_2364" s="T2272">eat-INF.LAT</ta>
            <ta e="T2274" id="Seg_2365" s="T2273">then</ta>
            <ta e="T2275" id="Seg_2366" s="T2274">this.[NOM.SG]</ta>
            <ta e="T2276" id="Seg_2367" s="T2275">pancake-PL</ta>
            <ta e="T2277" id="Seg_2368" s="T2276">and</ta>
            <ta e="T2279" id="Seg_2369" s="T2278">daughter-NOM/GEN.3SG</ta>
            <ta e="T2280" id="Seg_2370" s="T2279">pancake-PL</ta>
            <ta e="T2281" id="Seg_2371" s="T2280">put-PST.[3SG]</ta>
            <ta e="T2282" id="Seg_2372" s="T2281">pan-LAT/LOC.3SG</ta>
            <ta e="T2283" id="Seg_2373" s="T2282">place-PST.[3SG]</ta>
            <ta e="T2284" id="Seg_2374" s="T2283">sun-LAT</ta>
            <ta e="T2285" id="Seg_2375" s="T2284">this.[NOM.SG]</ta>
            <ta e="T2286" id="Seg_2376" s="T2285">bake-MOM-PST-3PL</ta>
            <ta e="T2287" id="Seg_2377" s="T2286">this.[NOM.SG]</ta>
            <ta e="T2288" id="Seg_2378" s="T2287">man.[NOM.SG]</ta>
            <ta e="T2289" id="Seg_2379" s="T2288">eat-PST.[3SG]</ta>
            <ta e="T2290" id="Seg_2380" s="T2289">and</ta>
            <ta e="T2291" id="Seg_2381" s="T2290">come-PST.[3SG]</ta>
            <ta e="T2292" id="Seg_2382" s="T2291">tent-LAT/LOC.3SG</ta>
            <ta e="T2293" id="Seg_2383" s="T2292">woman-LAT/LOC.3SG</ta>
            <ta e="T2294" id="Seg_2384" s="T2293">say-IPFVZ.[3SG]</ta>
            <ta e="T2295" id="Seg_2385" s="T2294">bake-EP-IMP.2SG</ta>
            <ta e="T2296" id="Seg_2386" s="T2295">pancake-PL</ta>
            <ta e="T2297" id="Seg_2387" s="T2296">this.[NOM.SG]</ta>
            <ta e="T2298" id="Seg_2388" s="T2297">INCH</ta>
            <ta e="T2299" id="Seg_2389" s="T2298">bake-INF.LAT</ta>
            <ta e="T2300" id="Seg_2390" s="T2299">and</ta>
            <ta e="T2301" id="Seg_2391" s="T2300">mouth-LAT</ta>
            <ta e="T2302" id="Seg_2392" s="T2301">place-PST.[3SG]</ta>
            <ta e="T2303" id="Seg_2393" s="T2302">stand-PST-3PL</ta>
            <ta e="T2304" id="Seg_2394" s="T2303">stand-PST-3PL</ta>
            <ta e="T2305" id="Seg_2395" s="T2304">NEG</ta>
            <ta e="T2306" id="Seg_2396" s="T2305">bake-PST-3PL</ta>
            <ta e="T2307" id="Seg_2397" s="T2306">then</ta>
            <ta e="T2308" id="Seg_2398" s="T2307">this.[NOM.SG]</ta>
            <ta e="T2309" id="Seg_2399" s="T2308">stove-LAT</ta>
            <ta e="T2310" id="Seg_2400" s="T2309">put-PST.[3SG]</ta>
            <ta e="T2311" id="Seg_2401" s="T2310">there</ta>
            <ta e="T2312" id="Seg_2402" s="T2311">bake-PST.[3SG]</ta>
            <ta e="T2313" id="Seg_2403" s="T2312">and</ta>
            <ta e="T2314" id="Seg_2404" s="T2313">man.[NOM.SG]</ta>
            <ta e="T2315" id="Seg_2405" s="T2314">eat-PST.[3SG]</ta>
            <ta e="T2316" id="Seg_2406" s="T2315">then</ta>
            <ta e="T2317" id="Seg_2407" s="T2316">walk-PRS.[3SG]</ta>
            <ta e="T2318" id="Seg_2408" s="T2317">month-LAT</ta>
            <ta e="T2319" id="Seg_2409" s="T2318">come-PST.[3SG]</ta>
            <ta e="T2320" id="Seg_2410" s="T2319">and</ta>
            <ta e="T2321" id="Seg_2411" s="T2320">this.[NOM.SG]</ta>
            <ta e="T2322" id="Seg_2412" s="T2321">sauna.[NOM.SG]</ta>
            <ta e="T2323" id="Seg_2413" s="T2322">light-PST.[3SG]</ta>
            <ta e="T2324" id="Seg_2414" s="T2323">say-IPFVZ.[3SG]</ta>
            <ta e="T2325" id="Seg_2415" s="T2324">well</ta>
            <ta e="T2326" id="Seg_2416" s="T2325">go-EP-IMP.2SG</ta>
            <ta e="T2327" id="Seg_2417" s="T2326">sauna-LAT</ta>
            <ta e="T2328" id="Seg_2418" s="T2327">and</ta>
            <ta e="T2329" id="Seg_2419" s="T2328">there</ta>
            <ta e="T2330" id="Seg_2420" s="T2329">what.[NOM.SG]=INDEF</ta>
            <ta e="T2331" id="Seg_2421" s="T2330">NEG</ta>
            <ta e="T2332" id="Seg_2422" s="T2331">be.visible-PRS.[3SG]</ta>
            <ta e="T2333" id="Seg_2423" s="T2332">go-EP-IMP.2SG</ta>
            <ta e="T2334" id="Seg_2424" s="T2333">this.[NOM.SG]</ta>
            <ta e="T2335" id="Seg_2425" s="T2334">go-PST.[3SG]</ta>
            <ta e="T2336" id="Seg_2426" s="T2335">this.[NOM.SG]</ta>
            <ta e="T2337" id="Seg_2427" s="T2336">finger-ACC.3SG</ta>
            <ta e="T2338" id="Seg_2428" s="T2337">there</ta>
            <ta e="T2339" id="Seg_2429" s="T2338">climb-MOM-PST.[3SG]</ta>
            <ta e="T2340" id="Seg_2430" s="T2339">this.[NOM.SG]</ta>
            <ta e="T2341" id="Seg_2431" s="T2340">wash.oneself-IPFVZ-PST.[3SG]</ta>
            <ta e="T2342" id="Seg_2432" s="T2341">there</ta>
            <ta e="T2343" id="Seg_2433" s="T2342">all</ta>
            <ta e="T2344" id="Seg_2434" s="T2343">be.visible-DUR.[3SG]</ta>
            <ta e="T2345" id="Seg_2435" s="T2344">then</ta>
            <ta e="T2346" id="Seg_2436" s="T2345">tent-LAT/LOC.3SG</ta>
            <ta e="T2347" id="Seg_2437" s="T2346">come-PST.[3SG]</ta>
            <ta e="T2348" id="Seg_2438" s="T2347">light-IMP.2SG</ta>
            <ta e="T2349" id="Seg_2439" s="T2348">sauna.[NOM.SG]</ta>
            <ta e="T2350" id="Seg_2440" s="T2349">woman-NOM/GEN.3SG</ta>
            <ta e="T2351" id="Seg_2441" s="T2350">light-PST.[3SG]</ta>
            <ta e="T2352" id="Seg_2442" s="T2351">well</ta>
            <ta e="T2354" id="Seg_2443" s="T2353">wash-IMP.2SG</ta>
            <ta e="T2355" id="Seg_2444" s="T2354">and</ta>
            <ta e="T2356" id="Seg_2445" s="T2355">there</ta>
            <ta e="T2357" id="Seg_2446" s="T2356">fire.[NOM.SG]</ta>
            <ta e="T2358" id="Seg_2447" s="T2357">NEG.EX</ta>
            <ta e="T2359" id="Seg_2448" s="T2358">go-EP-IMP.2SG</ta>
            <ta e="T2360" id="Seg_2449" s="T2359">this.[NOM.SG]</ta>
            <ta e="T2361" id="Seg_2450" s="T2360">hole.[NOM.SG]</ta>
            <ta e="T2362" id="Seg_2451" s="T2361">make-PST.[3SG]</ta>
            <ta e="T2363" id="Seg_2452" s="T2362">there</ta>
            <ta e="T2364" id="Seg_2453" s="T2363">finger-NOM/GEN.3SG</ta>
            <ta e="T2365" id="Seg_2454" s="T2364">climb-TR-PST.[3SG]</ta>
            <ta e="T2366" id="Seg_2455" s="T2365">woman-NOM/GEN.3SG</ta>
            <ta e="T2367" id="Seg_2456" s="T2366">always</ta>
            <ta e="T2368" id="Seg_2457" s="T2367">shout-DUR.[3SG]</ta>
            <ta e="T2369" id="Seg_2458" s="T2368">NEG.EX</ta>
            <ta e="T2370" id="Seg_2459" s="T2369">fire.[NOM.SG]</ta>
            <ta e="T2371" id="Seg_2460" s="T2370">NEG.EX</ta>
            <ta e="T2372" id="Seg_2461" s="T2371">fire.[NOM.SG]</ta>
            <ta e="T2373" id="Seg_2462" s="T2372">then</ta>
            <ta e="T2374" id="Seg_2463" s="T2373">wash.oneself-IPFVZ-PST-3PL</ta>
            <ta e="T2375" id="Seg_2464" s="T2374">fire.[NOM.SG]</ta>
            <ta e="T2376" id="Seg_2465" s="T2375">light-PST-3PL</ta>
            <ta e="T2377" id="Seg_2466" s="T2376">then</ta>
            <ta e="T2378" id="Seg_2467" s="T2377">morning-GEN</ta>
            <ta e="T2379" id="Seg_2468" s="T2378">get.up-DUR.[3SG]</ta>
            <ta e="T2382" id="Seg_2469" s="T2381">go-PST.[3SG]</ta>
            <ta e="T2383" id="Seg_2470" s="T2382">another.[NOM.SG]</ta>
            <ta e="T2384" id="Seg_2471" s="T2383">daughter-GEN.1SG</ta>
            <ta e="T2385" id="Seg_2472" s="T2384">there</ta>
            <ta e="T2386" id="Seg_2473" s="T2385">raven-LAT</ta>
            <ta e="T2387" id="Seg_2474" s="T2386">there</ta>
            <ta e="T2388" id="Seg_2475" s="T2387">say-IPFVZ.[3SG]</ta>
            <ta e="T2389" id="Seg_2476" s="T2388">what.[NOM.SG]</ta>
            <ta e="T2390" id="Seg_2477" s="T2389">you.DAT</ta>
            <ta e="T2391" id="Seg_2478" s="T2390">give-INF.LAT</ta>
            <ta e="T2392" id="Seg_2479" s="T2391">well</ta>
            <ta e="T2393" id="Seg_2480" s="T2392">HORT</ta>
            <ta e="T2394" id="Seg_2481" s="T2393">sleep-INF.LAT</ta>
            <ta e="T2395" id="Seg_2482" s="T2394">this</ta>
            <ta e="T2396" id="Seg_2483" s="T2395">this</ta>
            <ta e="T2397" id="Seg_2484" s="T2396">take-PST.[3SG]</ta>
            <ta e="T2398" id="Seg_2485" s="T2397">this-ACC</ta>
            <ta e="T2399" id="Seg_2486" s="T2398">wing-LAT/LOC.3SG</ta>
            <ta e="T2400" id="Seg_2487" s="T2399">two-COLL</ta>
            <ta e="T2401" id="Seg_2488" s="T2400">sleep-MOM-PST-3PL</ta>
            <ta e="T2402" id="Seg_2489" s="T2401">and</ta>
            <ta e="T2403" id="Seg_2490" s="T2402">fall-DUR-PST-3PL</ta>
            <ta e="T2404" id="Seg_2491" s="T2403">and</ta>
            <ta e="T2405" id="Seg_2492" s="T2404">die-RES-PST-3PL</ta>
            <ta e="T2406" id="Seg_2493" s="T2405">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2146" id="Seg_2494" s="T2145">жить-DUR-PST</ta>
            <ta e="T2147" id="Seg_2495" s="T2146">женщина.[NOM.SG]</ta>
            <ta e="T2148" id="Seg_2496" s="T2147">мужчина.[NOM.SG]</ta>
            <ta e="T2149" id="Seg_2497" s="T2148">этот-GEN</ta>
            <ta e="T2150" id="Seg_2498" s="T2149">три.[NOM.SG]</ta>
            <ta e="T2151" id="Seg_2499" s="T2150">девушка.[NOM.SG]</ta>
            <ta e="T2152" id="Seg_2500" s="T2151">быть-PST.[3SG]</ta>
            <ta e="T2153" id="Seg_2501" s="T2152">этот.[NOM.SG]</ta>
            <ta e="T2154" id="Seg_2502" s="T2153">пойти-PST.[3SG]</ta>
            <ta e="T2156" id="Seg_2503" s="T2155">амбар-LAT</ta>
            <ta e="T2157" id="Seg_2504" s="T2156">крупа.[NOM.SG]</ta>
            <ta e="T2158" id="Seg_2505" s="T2157">взять-PST.[3SG]</ta>
            <ta e="T2159" id="Seg_2506" s="T2158">мешок-LAT</ta>
            <ta e="T2160" id="Seg_2507" s="T2159">прийти-PST.[3SG]</ta>
            <ta e="T2161" id="Seg_2508" s="T2160">а</ta>
            <ta e="T2162" id="Seg_2509" s="T2161">мешок-LOC</ta>
            <ta e="T2163" id="Seg_2510" s="T2162">отверстие.[NOM.SG]</ta>
            <ta e="T2164" id="Seg_2511" s="T2163">быть-PST.[3SG]</ta>
            <ta e="T2165" id="Seg_2512" s="T2164">прийти-PST.[3SG]</ta>
            <ta e="T2166" id="Seg_2513" s="T2165">прийти-PST.[3SG]</ta>
            <ta e="T2167" id="Seg_2514" s="T2166">а</ta>
            <ta e="T2168" id="Seg_2515" s="T2167">крупа-NOM/GEN.3SG</ta>
            <ta e="T2169" id="Seg_2516" s="T2168">весь</ta>
            <ta e="T2170" id="Seg_2517" s="T2169">лить-MOM-PST.[3SG]</ta>
            <ta e="T2171" id="Seg_2518" s="T2170">женщина-NOM/GEN.3SG</ta>
            <ta e="T2172" id="Seg_2519" s="T2171">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2173" id="Seg_2520" s="T2172">где</ta>
            <ta e="T2174" id="Seg_2521" s="T2173">ты.GEN</ta>
            <ta e="T2175" id="Seg_2522" s="T2174">крупа.[NOM.SG]</ta>
            <ta e="T2176" id="Seg_2523" s="T2175">да</ta>
            <ta e="T2177" id="Seg_2524" s="T2176">пойти-MOM-PST-1SG</ta>
            <ta e="T2178" id="Seg_2525" s="T2177">тогда</ta>
            <ta e="T2179" id="Seg_2526" s="T2178">пойти-PST.[3SG]</ta>
            <ta e="T2180" id="Seg_2527" s="T2179">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2181" id="Seg_2528" s="T2180">солнце.[NOM.SG]</ta>
            <ta e="T2182" id="Seg_2529" s="T2181">помогать-EP-IMP.2SG</ta>
            <ta e="T2183" id="Seg_2530" s="T2182">месяц-LAT</ta>
            <ta e="T2184" id="Seg_2531" s="T2183">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2185" id="Seg_2532" s="T2184">помогать-EP-IMP.2SG</ta>
            <ta e="T2186" id="Seg_2533" s="T2185">и</ta>
            <ta e="T2187" id="Seg_2534" s="T2186">ворон.[NOM.SG]</ta>
            <ta e="T2188" id="Seg_2535" s="T2187">ворон-LAT</ta>
            <ta e="T2189" id="Seg_2536" s="T2188">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2190" id="Seg_2537" s="T2189">помогать-EP-IMP.2SG</ta>
            <ta e="T2191" id="Seg_2538" s="T2190">этот-PL</ta>
            <ta e="T2192" id="Seg_2539" s="T2191">прийти-PST-3PL</ta>
            <ta e="T2193" id="Seg_2540" s="T2192">и</ta>
            <ta e="T2195" id="Seg_2541" s="T2194">помогать-PST-3PL</ta>
            <ta e="T2196" id="Seg_2542" s="T2195">этот.[NOM.SG]</ta>
            <ta e="T2197" id="Seg_2543" s="T2196">я.NOM</ta>
            <ta e="T2198" id="Seg_2544" s="T2197">дочь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2199" id="Seg_2545" s="T2198">вы.ACC</ta>
            <ta e="T2200" id="Seg_2546" s="T2199">дать-FUT-1SG</ta>
            <ta e="T2201" id="Seg_2547" s="T2200">мужчина-LAT</ta>
            <ta e="T2202" id="Seg_2548" s="T2201">тогда</ta>
            <ta e="T2203" id="Seg_2549" s="T2202">прийти-PST.[3SG]</ta>
            <ta e="T2204" id="Seg_2550" s="T2203">принести-PST.[3SG]</ta>
            <ta e="T2205" id="Seg_2551" s="T2204">этот.[NOM.SG]</ta>
            <ta e="T2206" id="Seg_2552" s="T2205">крупа-ACC</ta>
            <ta e="T2207" id="Seg_2553" s="T2206">женщина-LAT/LOC.3SG</ta>
            <ta e="T2208" id="Seg_2554" s="T2207">тогда</ta>
            <ta e="T2209" id="Seg_2555" s="T2208">один.[NOM.SG]</ta>
            <ta e="T2210" id="Seg_2556" s="T2209">большой.[NOM.SG]</ta>
            <ta e="T2211" id="Seg_2557" s="T2210">дочь-GEN.1SG</ta>
            <ta e="T2212" id="Seg_2558" s="T2211">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2213" id="Seg_2559" s="T2212">надеть-EP-IMP.2SG</ta>
            <ta e="T2214" id="Seg_2560" s="T2213">одежда.[NOM.SG]</ta>
            <ta e="T2215" id="Seg_2561" s="T2214">красивый.[NOM.SG]</ta>
            <ta e="T2216" id="Seg_2562" s="T2215">этот.[NOM.SG]</ta>
            <ta e="T2217" id="Seg_2563" s="T2216">надеть-PST.[3SG]</ta>
            <ta e="T2218" id="Seg_2564" s="T2217">и</ta>
            <ta e="T2219" id="Seg_2565" s="T2218">пойти-PST.[3SG]</ta>
            <ta e="T2220" id="Seg_2566" s="T2219">наружу</ta>
            <ta e="T2221" id="Seg_2567" s="T2220">этот-ACC</ta>
            <ta e="T2222" id="Seg_2568" s="T2221">взять-MOM-PST.[3SG]</ta>
            <ta e="T2223" id="Seg_2569" s="T2222">солнце.[NOM.SG]</ta>
            <ta e="T2224" id="Seg_2570" s="T2223">тогда</ta>
            <ta e="T2225" id="Seg_2571" s="T2224">другой.[NOM.SG]</ta>
            <ta e="T2226" id="Seg_2572" s="T2225">дочь-3SG-INS</ta>
            <ta e="T2227" id="Seg_2573" s="T2226">надеть-EP-IMP.2SG</ta>
            <ta e="T2228" id="Seg_2574" s="T2227">одежда.[NOM.SG]</ta>
            <ta e="T2229" id="Seg_2575" s="T2228">красивый.[NOM.SG]</ta>
            <ta e="T2230" id="Seg_2576" s="T2229">и</ta>
            <ta e="T2231" id="Seg_2577" s="T2230">пойти-EP-IMP.2SG</ta>
            <ta e="T2232" id="Seg_2578" s="T2231">наружу</ta>
            <ta e="T2233" id="Seg_2579" s="T2232">этот.[NOM.SG]</ta>
            <ta e="T2234" id="Seg_2580" s="T2233">пойти-PST.[3SG]</ta>
            <ta e="T2235" id="Seg_2581" s="T2234">этот-ACC</ta>
            <ta e="T2236" id="Seg_2582" s="T2235">месяц.[NOM.SG]</ta>
            <ta e="T2237" id="Seg_2583" s="T2236">взять-PST.[3SG]</ta>
            <ta e="T2238" id="Seg_2584" s="T2237">тогда</ta>
            <ta e="T2239" id="Seg_2585" s="T2238">три-ORD</ta>
            <ta e="T2240" id="Seg_2586" s="T2239">дочь-GEN.1SG</ta>
            <ta e="T2241" id="Seg_2587" s="T2240">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2242" id="Seg_2588" s="T2241">надеть-EP-IMP.2SG</ta>
            <ta e="T2243" id="Seg_2589" s="T2242">одежда.[NOM.SG]</ta>
            <ta e="T2244" id="Seg_2590" s="T2243">красивый.[NOM.SG]</ta>
            <ta e="T2245" id="Seg_2591" s="T2244">и</ta>
            <ta e="T2246" id="Seg_2592" s="T2245">пойти-EP-IMP.2SG</ta>
            <ta e="T2247" id="Seg_2593" s="T2246">наружу</ta>
            <ta e="T2248" id="Seg_2594" s="T2247">этот.[NOM.SG]</ta>
            <ta e="T2249" id="Seg_2595" s="T2248">пойти-PST.[3SG]</ta>
            <ta e="T2250" id="Seg_2596" s="T2249">этот-ACC</ta>
            <ta e="T2251" id="Seg_2597" s="T2250">тоже</ta>
            <ta e="T2252" id="Seg_2598" s="T2251">взять-MOM-PST.[3SG]</ta>
            <ta e="T2253" id="Seg_2599" s="T2252">ворон.[NOM.SG]</ta>
            <ta e="T2254" id="Seg_2600" s="T2253">тогда</ta>
            <ta e="T2255" id="Seg_2601" s="T2254">этот.[NOM.SG]</ta>
            <ta e="T2257" id="Seg_2602" s="T2256">этот.[NOM.SG]</ta>
            <ta e="T2259" id="Seg_2603" s="T2258">мужчина.[NOM.SG]</ta>
            <ta e="T2260" id="Seg_2604" s="T2259">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2261" id="Seg_2605" s="T2260">пойти-FUT-1SG</ta>
            <ta e="T2262" id="Seg_2606" s="T2261">дочь-GEN.1SG</ta>
            <ta e="T2263" id="Seg_2607" s="T2262">деревня-VBLZ-CVB</ta>
            <ta e="T2264" id="Seg_2608" s="T2263">прийти-PST.[3SG]</ta>
            <ta e="T2265" id="Seg_2609" s="T2264">что-INS</ta>
            <ta e="T2266" id="Seg_2610" s="T2265">ты.ACC</ta>
            <ta e="T2267" id="Seg_2611" s="T2266">кормить-INF.LAT</ta>
            <ta e="T2268" id="Seg_2612" s="T2267">да</ta>
            <ta e="T2269" id="Seg_2613" s="T2268">я.NOM</ta>
            <ta e="T2270" id="Seg_2614" s="T2269">я.NOM</ta>
            <ta e="T2271" id="Seg_2615" s="T2270">не</ta>
            <ta e="T2272" id="Seg_2616" s="T2271">хочется</ta>
            <ta e="T2273" id="Seg_2617" s="T2272">съесть-INF.LAT</ta>
            <ta e="T2274" id="Seg_2618" s="T2273">тогда</ta>
            <ta e="T2275" id="Seg_2619" s="T2274">этот.[NOM.SG]</ta>
            <ta e="T2276" id="Seg_2620" s="T2275">оладьи-PL</ta>
            <ta e="T2277" id="Seg_2621" s="T2276">и</ta>
            <ta e="T2279" id="Seg_2622" s="T2278">дочь-NOM/GEN.3SG</ta>
            <ta e="T2280" id="Seg_2623" s="T2279">оладьи-PL</ta>
            <ta e="T2281" id="Seg_2624" s="T2280">класть-PST.[3SG]</ta>
            <ta e="T2282" id="Seg_2625" s="T2281">сковорода-LAT/LOC.3SG</ta>
            <ta e="T2283" id="Seg_2626" s="T2282">поставить-PST.[3SG]</ta>
            <ta e="T2284" id="Seg_2627" s="T2283">солнце-LAT</ta>
            <ta e="T2285" id="Seg_2628" s="T2284">этот.[NOM.SG]</ta>
            <ta e="T2286" id="Seg_2629" s="T2285">печь-MOM-PST-3PL</ta>
            <ta e="T2287" id="Seg_2630" s="T2286">этот.[NOM.SG]</ta>
            <ta e="T2288" id="Seg_2631" s="T2287">мужчина.[NOM.SG]</ta>
            <ta e="T2289" id="Seg_2632" s="T2288">съесть-PST.[3SG]</ta>
            <ta e="T2290" id="Seg_2633" s="T2289">и</ta>
            <ta e="T2291" id="Seg_2634" s="T2290">прийти-PST.[3SG]</ta>
            <ta e="T2292" id="Seg_2635" s="T2291">чум-LAT/LOC.3SG</ta>
            <ta e="T2293" id="Seg_2636" s="T2292">женщина-LAT/LOC.3SG</ta>
            <ta e="T2294" id="Seg_2637" s="T2293">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2295" id="Seg_2638" s="T2294">печь-EP-IMP.2SG</ta>
            <ta e="T2296" id="Seg_2639" s="T2295">оладьи-PL</ta>
            <ta e="T2297" id="Seg_2640" s="T2296">этот.[NOM.SG]</ta>
            <ta e="T2298" id="Seg_2641" s="T2297">INCH</ta>
            <ta e="T2299" id="Seg_2642" s="T2298">печь-INF.LAT</ta>
            <ta e="T2300" id="Seg_2643" s="T2299">и</ta>
            <ta e="T2301" id="Seg_2644" s="T2300">рот-LAT</ta>
            <ta e="T2302" id="Seg_2645" s="T2301">поставить-PST.[3SG]</ta>
            <ta e="T2303" id="Seg_2646" s="T2302">стоять-PST-3PL</ta>
            <ta e="T2304" id="Seg_2647" s="T2303">стоять-PST-3PL</ta>
            <ta e="T2305" id="Seg_2648" s="T2304">NEG</ta>
            <ta e="T2306" id="Seg_2649" s="T2305">печь-PST-3PL</ta>
            <ta e="T2307" id="Seg_2650" s="T2306">тогда</ta>
            <ta e="T2308" id="Seg_2651" s="T2307">этот.[NOM.SG]</ta>
            <ta e="T2309" id="Seg_2652" s="T2308">печь-LAT</ta>
            <ta e="T2310" id="Seg_2653" s="T2309">класть-PST.[3SG]</ta>
            <ta e="T2311" id="Seg_2654" s="T2310">там</ta>
            <ta e="T2312" id="Seg_2655" s="T2311">печь-PST.[3SG]</ta>
            <ta e="T2313" id="Seg_2656" s="T2312">и</ta>
            <ta e="T2314" id="Seg_2657" s="T2313">мужчина.[NOM.SG]</ta>
            <ta e="T2315" id="Seg_2658" s="T2314">съесть-PST.[3SG]</ta>
            <ta e="T2316" id="Seg_2659" s="T2315">тогда</ta>
            <ta e="T2317" id="Seg_2660" s="T2316">идти-PRS.[3SG]</ta>
            <ta e="T2318" id="Seg_2661" s="T2317">месяц-LAT</ta>
            <ta e="T2319" id="Seg_2662" s="T2318">прийти-PST.[3SG]</ta>
            <ta e="T2320" id="Seg_2663" s="T2319">а</ta>
            <ta e="T2321" id="Seg_2664" s="T2320">этот.[NOM.SG]</ta>
            <ta e="T2322" id="Seg_2665" s="T2321">баня.[NOM.SG]</ta>
            <ta e="T2323" id="Seg_2666" s="T2322">светить-PST.[3SG]</ta>
            <ta e="T2324" id="Seg_2667" s="T2323">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2325" id="Seg_2668" s="T2324">ну</ta>
            <ta e="T2326" id="Seg_2669" s="T2325">пойти-EP-IMP.2SG</ta>
            <ta e="T2327" id="Seg_2670" s="T2326">баня-LAT</ta>
            <ta e="T2328" id="Seg_2671" s="T2327">а</ta>
            <ta e="T2329" id="Seg_2672" s="T2328">там</ta>
            <ta e="T2330" id="Seg_2673" s="T2329">что.[NOM.SG]=INDEF</ta>
            <ta e="T2331" id="Seg_2674" s="T2330">NEG</ta>
            <ta e="T2332" id="Seg_2675" s="T2331">быть.видным-PRS.[3SG]</ta>
            <ta e="T2333" id="Seg_2676" s="T2332">пойти-EP-IMP.2SG</ta>
            <ta e="T2334" id="Seg_2677" s="T2333">этот.[NOM.SG]</ta>
            <ta e="T2335" id="Seg_2678" s="T2334">пойти-PST.[3SG]</ta>
            <ta e="T2336" id="Seg_2679" s="T2335">этот.[NOM.SG]</ta>
            <ta e="T2337" id="Seg_2680" s="T2336">палец-ACC.3SG</ta>
            <ta e="T2338" id="Seg_2681" s="T2337">там</ta>
            <ta e="T2339" id="Seg_2682" s="T2338">лезть-MOM-PST.[3SG]</ta>
            <ta e="T2340" id="Seg_2683" s="T2339">этот.[NOM.SG]</ta>
            <ta e="T2341" id="Seg_2684" s="T2340">мыться-IPFVZ-PST.[3SG]</ta>
            <ta e="T2342" id="Seg_2685" s="T2341">там</ta>
            <ta e="T2343" id="Seg_2686" s="T2342">весь</ta>
            <ta e="T2344" id="Seg_2687" s="T2343">быть.видным-DUR.[3SG]</ta>
            <ta e="T2345" id="Seg_2688" s="T2344">тогда</ta>
            <ta e="T2346" id="Seg_2689" s="T2345">чум-LAT/LOC.3SG</ta>
            <ta e="T2347" id="Seg_2690" s="T2346">прийти-PST.[3SG]</ta>
            <ta e="T2348" id="Seg_2691" s="T2347">светить-IMP.2SG</ta>
            <ta e="T2349" id="Seg_2692" s="T2348">баня.[NOM.SG]</ta>
            <ta e="T2350" id="Seg_2693" s="T2349">женщина-NOM/GEN.3SG</ta>
            <ta e="T2351" id="Seg_2694" s="T2350">светить-PST.[3SG]</ta>
            <ta e="T2352" id="Seg_2695" s="T2351">ну</ta>
            <ta e="T2354" id="Seg_2696" s="T2353">мыть-IMP.2SG</ta>
            <ta e="T2355" id="Seg_2697" s="T2354">и</ta>
            <ta e="T2356" id="Seg_2698" s="T2355">там</ta>
            <ta e="T2357" id="Seg_2699" s="T2356">огонь.[NOM.SG]</ta>
            <ta e="T2358" id="Seg_2700" s="T2357">NEG.EX</ta>
            <ta e="T2359" id="Seg_2701" s="T2358">пойти-EP-IMP.2SG</ta>
            <ta e="T2360" id="Seg_2702" s="T2359">этот.[NOM.SG]</ta>
            <ta e="T2361" id="Seg_2703" s="T2360">отверстие.[NOM.SG]</ta>
            <ta e="T2362" id="Seg_2704" s="T2361">делать-PST.[3SG]</ta>
            <ta e="T2363" id="Seg_2705" s="T2362">там</ta>
            <ta e="T2364" id="Seg_2706" s="T2363">палец-NOM/GEN.3SG</ta>
            <ta e="T2365" id="Seg_2707" s="T2364">лезть-TR-PST.[3SG]</ta>
            <ta e="T2366" id="Seg_2708" s="T2365">женщина-NOM/GEN.3SG</ta>
            <ta e="T2367" id="Seg_2709" s="T2366">всегда</ta>
            <ta e="T2368" id="Seg_2710" s="T2367">кричать-DUR.[3SG]</ta>
            <ta e="T2369" id="Seg_2711" s="T2368">NEG.EX</ta>
            <ta e="T2370" id="Seg_2712" s="T2369">огонь.[NOM.SG]</ta>
            <ta e="T2371" id="Seg_2713" s="T2370">NEG.EX</ta>
            <ta e="T2372" id="Seg_2714" s="T2371">огонь.[NOM.SG]</ta>
            <ta e="T2373" id="Seg_2715" s="T2372">тогда</ta>
            <ta e="T2374" id="Seg_2716" s="T2373">мыться-IPFVZ-PST-3PL</ta>
            <ta e="T2375" id="Seg_2717" s="T2374">огонь.[NOM.SG]</ta>
            <ta e="T2376" id="Seg_2718" s="T2375">светить-PST-3PL</ta>
            <ta e="T2377" id="Seg_2719" s="T2376">тогда</ta>
            <ta e="T2378" id="Seg_2720" s="T2377">утро-GEN</ta>
            <ta e="T2379" id="Seg_2721" s="T2378">встать-DUR.[3SG]</ta>
            <ta e="T2382" id="Seg_2722" s="T2381">пойти-PST.[3SG]</ta>
            <ta e="T2383" id="Seg_2723" s="T2382">другой.[NOM.SG]</ta>
            <ta e="T2384" id="Seg_2724" s="T2383">дочь-GEN.1SG</ta>
            <ta e="T2385" id="Seg_2725" s="T2384">там</ta>
            <ta e="T2386" id="Seg_2726" s="T2385">ворон-LAT</ta>
            <ta e="T2387" id="Seg_2727" s="T2386">там</ta>
            <ta e="T2388" id="Seg_2728" s="T2387">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2389" id="Seg_2729" s="T2388">что.[NOM.SG]</ta>
            <ta e="T2390" id="Seg_2730" s="T2389">ты.DAT</ta>
            <ta e="T2391" id="Seg_2731" s="T2390">дать-INF.LAT</ta>
            <ta e="T2392" id="Seg_2732" s="T2391">ну</ta>
            <ta e="T2393" id="Seg_2733" s="T2392">HORT</ta>
            <ta e="T2394" id="Seg_2734" s="T2393">спать-INF.LAT</ta>
            <ta e="T2395" id="Seg_2735" s="T2394">этот</ta>
            <ta e="T2396" id="Seg_2736" s="T2395">этот</ta>
            <ta e="T2397" id="Seg_2737" s="T2396">взять-PST.[3SG]</ta>
            <ta e="T2398" id="Seg_2738" s="T2397">этот-ACC</ta>
            <ta e="T2399" id="Seg_2739" s="T2398">крыло-LAT/LOC.3SG</ta>
            <ta e="T2400" id="Seg_2740" s="T2399">два-COLL</ta>
            <ta e="T2401" id="Seg_2741" s="T2400">спать-MOM-PST-3PL</ta>
            <ta e="T2402" id="Seg_2742" s="T2401">и</ta>
            <ta e="T2403" id="Seg_2743" s="T2402">упасть-DUR-PST-3PL</ta>
            <ta e="T2404" id="Seg_2744" s="T2403">и</ta>
            <ta e="T2405" id="Seg_2745" s="T2404">умереть-RES-PST-3PL</ta>
            <ta e="T2406" id="Seg_2746" s="T2405">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2146" id="Seg_2747" s="T2145">v-v&gt;v-v:tense</ta>
            <ta e="T2147" id="Seg_2748" s="T2146">n-n:case</ta>
            <ta e="T2148" id="Seg_2749" s="T2147">n-n:case</ta>
            <ta e="T2149" id="Seg_2750" s="T2148">dempro-n:case</ta>
            <ta e="T2150" id="Seg_2751" s="T2149">num-n:case</ta>
            <ta e="T2151" id="Seg_2752" s="T2150">n-n:case</ta>
            <ta e="T2152" id="Seg_2753" s="T2151">v-v:tense-v:pn</ta>
            <ta e="T2153" id="Seg_2754" s="T2152">dempro-n:case</ta>
            <ta e="T2154" id="Seg_2755" s="T2153">v-v:tense-v:pn</ta>
            <ta e="T2156" id="Seg_2756" s="T2155">n-n:case</ta>
            <ta e="T2157" id="Seg_2757" s="T2156">n-n:case</ta>
            <ta e="T2158" id="Seg_2758" s="T2157">v-v:tense-v:pn</ta>
            <ta e="T2159" id="Seg_2759" s="T2158">n-n:case</ta>
            <ta e="T2160" id="Seg_2760" s="T2159">v-v:tense-v:pn</ta>
            <ta e="T2161" id="Seg_2761" s="T2160">conj</ta>
            <ta e="T2162" id="Seg_2762" s="T2161">n-n:case</ta>
            <ta e="T2163" id="Seg_2763" s="T2162">n-n:case</ta>
            <ta e="T2164" id="Seg_2764" s="T2163">v-v:tense-v:pn</ta>
            <ta e="T2165" id="Seg_2765" s="T2164">v-v:tense-v:pn</ta>
            <ta e="T2166" id="Seg_2766" s="T2165">v-v:tense-v:pn</ta>
            <ta e="T2167" id="Seg_2767" s="T2166">conj</ta>
            <ta e="T2168" id="Seg_2768" s="T2167">n-n:case.poss</ta>
            <ta e="T2169" id="Seg_2769" s="T2168">quant</ta>
            <ta e="T2170" id="Seg_2770" s="T2169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2171" id="Seg_2771" s="T2170">n-n:case.poss</ta>
            <ta e="T2172" id="Seg_2772" s="T2171">v-v&gt;v-v:pn</ta>
            <ta e="T2173" id="Seg_2773" s="T2172">que</ta>
            <ta e="T2174" id="Seg_2774" s="T2173">pers</ta>
            <ta e="T2175" id="Seg_2775" s="T2174">n-n:case</ta>
            <ta e="T2176" id="Seg_2776" s="T2175">ptcl</ta>
            <ta e="T2177" id="Seg_2777" s="T2176">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2178" id="Seg_2778" s="T2177">adv</ta>
            <ta e="T2179" id="Seg_2779" s="T2178">v-v:tense-v:pn</ta>
            <ta e="T2180" id="Seg_2780" s="T2179">v-v&gt;v-v:pn</ta>
            <ta e="T2181" id="Seg_2781" s="T2180">n-n:case</ta>
            <ta e="T2182" id="Seg_2782" s="T2181">v-v:ins-v:mood.pn</ta>
            <ta e="T2183" id="Seg_2783" s="T2182">n-n:case</ta>
            <ta e="T2184" id="Seg_2784" s="T2183">v-v&gt;v-v:pn</ta>
            <ta e="T2185" id="Seg_2785" s="T2184">v-v:ins-v:mood.pn</ta>
            <ta e="T2186" id="Seg_2786" s="T2185">conj</ta>
            <ta e="T2187" id="Seg_2787" s="T2186">n-n:case</ta>
            <ta e="T2188" id="Seg_2788" s="T2187">n-n:case</ta>
            <ta e="T2189" id="Seg_2789" s="T2188">v-v&gt;v-v:pn</ta>
            <ta e="T2190" id="Seg_2790" s="T2189">v-v:ins-v:mood.pn</ta>
            <ta e="T2191" id="Seg_2791" s="T2190">dempro-n:num</ta>
            <ta e="T2192" id="Seg_2792" s="T2191">v-v:tense-v:pn</ta>
            <ta e="T2193" id="Seg_2793" s="T2192">conj</ta>
            <ta e="T2195" id="Seg_2794" s="T2194">v-v:tense-v:pn</ta>
            <ta e="T2196" id="Seg_2795" s="T2195">dempro-n:case</ta>
            <ta e="T2197" id="Seg_2796" s="T2196">pers</ta>
            <ta e="T2198" id="Seg_2797" s="T2197">n-n:num-n:case.poss</ta>
            <ta e="T2199" id="Seg_2798" s="T2198">pers</ta>
            <ta e="T2200" id="Seg_2799" s="T2199">v-v:tense-v:pn</ta>
            <ta e="T2201" id="Seg_2800" s="T2200">n-n:case</ta>
            <ta e="T2202" id="Seg_2801" s="T2201">adv</ta>
            <ta e="T2203" id="Seg_2802" s="T2202">v-v:tense-v:pn</ta>
            <ta e="T2204" id="Seg_2803" s="T2203">v-v:tense-v:pn</ta>
            <ta e="T2205" id="Seg_2804" s="T2204">dempro-n:case</ta>
            <ta e="T2206" id="Seg_2805" s="T2205">n-n:case</ta>
            <ta e="T2207" id="Seg_2806" s="T2206">n-n:case.poss</ta>
            <ta e="T2208" id="Seg_2807" s="T2207">adv</ta>
            <ta e="T2209" id="Seg_2808" s="T2208">num-n:case</ta>
            <ta e="T2210" id="Seg_2809" s="T2209">adj-n:case</ta>
            <ta e="T2211" id="Seg_2810" s="T2210">n-n:case.poss</ta>
            <ta e="T2212" id="Seg_2811" s="T2211">v-v&gt;v-v:pn</ta>
            <ta e="T2213" id="Seg_2812" s="T2212">v-v:ins-v:mood.pn</ta>
            <ta e="T2214" id="Seg_2813" s="T2213">n-n:case</ta>
            <ta e="T2215" id="Seg_2814" s="T2214">adj-n:case</ta>
            <ta e="T2216" id="Seg_2815" s="T2215">dempro-n:case</ta>
            <ta e="T2217" id="Seg_2816" s="T2216">v-v:tense-v:pn</ta>
            <ta e="T2218" id="Seg_2817" s="T2217">conj</ta>
            <ta e="T2219" id="Seg_2818" s="T2218">v-v:tense-v:pn</ta>
            <ta e="T2220" id="Seg_2819" s="T2219">adv</ta>
            <ta e="T2221" id="Seg_2820" s="T2220">dempro-n:case</ta>
            <ta e="T2222" id="Seg_2821" s="T2221">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2223" id="Seg_2822" s="T2222">n-n:case</ta>
            <ta e="T2224" id="Seg_2823" s="T2223">adv</ta>
            <ta e="T2225" id="Seg_2824" s="T2224">adj-n:case</ta>
            <ta e="T2226" id="Seg_2825" s="T2225">n-n:case.poss-n:case</ta>
            <ta e="T2227" id="Seg_2826" s="T2226">v-v:ins-v:mood.pn</ta>
            <ta e="T2228" id="Seg_2827" s="T2227">n-n:case</ta>
            <ta e="T2229" id="Seg_2828" s="T2228">adj-n:case</ta>
            <ta e="T2230" id="Seg_2829" s="T2229">conj</ta>
            <ta e="T2231" id="Seg_2830" s="T2230">v-v:ins-v:mood.pn</ta>
            <ta e="T2232" id="Seg_2831" s="T2231">adv</ta>
            <ta e="T2233" id="Seg_2832" s="T2232">dempro-n:case</ta>
            <ta e="T2234" id="Seg_2833" s="T2233">v-v:tense-v:pn</ta>
            <ta e="T2235" id="Seg_2834" s="T2234">dempro-n:case</ta>
            <ta e="T2236" id="Seg_2835" s="T2235">n-n:case</ta>
            <ta e="T2237" id="Seg_2836" s="T2236">v-v:tense-v:pn</ta>
            <ta e="T2238" id="Seg_2837" s="T2237">adv</ta>
            <ta e="T2239" id="Seg_2838" s="T2238">num-num&gt;num</ta>
            <ta e="T2240" id="Seg_2839" s="T2239">n-n:case.poss</ta>
            <ta e="T2241" id="Seg_2840" s="T2240">v-v&gt;v-v:pn</ta>
            <ta e="T2242" id="Seg_2841" s="T2241">v-v:ins-v:mood.pn</ta>
            <ta e="T2243" id="Seg_2842" s="T2242">n-n:case</ta>
            <ta e="T2244" id="Seg_2843" s="T2243">adj-n:case</ta>
            <ta e="T2245" id="Seg_2844" s="T2244">conj</ta>
            <ta e="T2246" id="Seg_2845" s="T2245">v-v:ins-v:mood.pn</ta>
            <ta e="T2247" id="Seg_2846" s="T2246">adv</ta>
            <ta e="T2248" id="Seg_2847" s="T2247">dempro-n:case</ta>
            <ta e="T2249" id="Seg_2848" s="T2248">v-v:tense-v:pn</ta>
            <ta e="T2250" id="Seg_2849" s="T2249">dempro-n:case</ta>
            <ta e="T2251" id="Seg_2850" s="T2250">ptcl</ta>
            <ta e="T2252" id="Seg_2851" s="T2251">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2253" id="Seg_2852" s="T2252">n-n:case</ta>
            <ta e="T2254" id="Seg_2853" s="T2253">adv</ta>
            <ta e="T2255" id="Seg_2854" s="T2254">dempro-n:case</ta>
            <ta e="T2257" id="Seg_2855" s="T2256">dempro-n:case</ta>
            <ta e="T2259" id="Seg_2856" s="T2258">n-n:case</ta>
            <ta e="T2260" id="Seg_2857" s="T2259">v-v&gt;v-v:pn</ta>
            <ta e="T2261" id="Seg_2858" s="T2260">v-v:tense-v:pn</ta>
            <ta e="T2262" id="Seg_2859" s="T2261">n-n:case.poss</ta>
            <ta e="T2263" id="Seg_2860" s="T2262">n-n&gt;v-v:n.fin</ta>
            <ta e="T2264" id="Seg_2861" s="T2263">v-v:tense-v:pn</ta>
            <ta e="T2265" id="Seg_2862" s="T2264">que-n:case</ta>
            <ta e="T2266" id="Seg_2863" s="T2265">pers</ta>
            <ta e="T2267" id="Seg_2864" s="T2266">v-v:n.fin</ta>
            <ta e="T2268" id="Seg_2865" s="T2267">ptcl</ta>
            <ta e="T2269" id="Seg_2866" s="T2268">pers</ta>
            <ta e="T2270" id="Seg_2867" s="T2269">pers</ta>
            <ta e="T2271" id="Seg_2868" s="T2270">ptcl</ta>
            <ta e="T2272" id="Seg_2869" s="T2271">ptcl</ta>
            <ta e="T2273" id="Seg_2870" s="T2272">v-v:n.fin</ta>
            <ta e="T2274" id="Seg_2871" s="T2273">adv</ta>
            <ta e="T2275" id="Seg_2872" s="T2274">dempro-n:case</ta>
            <ta e="T2276" id="Seg_2873" s="T2275">n-n:num</ta>
            <ta e="T2277" id="Seg_2874" s="T2276">conj</ta>
            <ta e="T2279" id="Seg_2875" s="T2278">n-n:case.poss</ta>
            <ta e="T2280" id="Seg_2876" s="T2279">n-n:num</ta>
            <ta e="T2281" id="Seg_2877" s="T2280">v-v:tense-v:pn</ta>
            <ta e="T2282" id="Seg_2878" s="T2281">n-n:case.poss</ta>
            <ta e="T2283" id="Seg_2879" s="T2282">v-v:tense-v:pn</ta>
            <ta e="T2284" id="Seg_2880" s="T2283">n-n:case</ta>
            <ta e="T2285" id="Seg_2881" s="T2284">dempro-n:case</ta>
            <ta e="T2286" id="Seg_2882" s="T2285">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2287" id="Seg_2883" s="T2286">dempro-n:case</ta>
            <ta e="T2288" id="Seg_2884" s="T2287">n-n:case</ta>
            <ta e="T2289" id="Seg_2885" s="T2288">v-v:tense-v:pn</ta>
            <ta e="T2290" id="Seg_2886" s="T2289">conj</ta>
            <ta e="T2291" id="Seg_2887" s="T2290">v-v:tense-v:pn</ta>
            <ta e="T2292" id="Seg_2888" s="T2291">n-n:case.poss</ta>
            <ta e="T2293" id="Seg_2889" s="T2292">n-n:case.poss</ta>
            <ta e="T2294" id="Seg_2890" s="T2293">v-v&gt;v-v:pn</ta>
            <ta e="T2295" id="Seg_2891" s="T2294">v-v:ins-v:mood.pn</ta>
            <ta e="T2296" id="Seg_2892" s="T2295">n-n:num</ta>
            <ta e="T2297" id="Seg_2893" s="T2296">dempro-n:case</ta>
            <ta e="T2298" id="Seg_2894" s="T2297">ptcl</ta>
            <ta e="T2299" id="Seg_2895" s="T2298">v-v:n.fin</ta>
            <ta e="T2300" id="Seg_2896" s="T2299">conj</ta>
            <ta e="T2301" id="Seg_2897" s="T2300">n-n:case</ta>
            <ta e="T2302" id="Seg_2898" s="T2301">v-v:tense-v:pn</ta>
            <ta e="T2303" id="Seg_2899" s="T2302">v-v:tense-v:pn</ta>
            <ta e="T2304" id="Seg_2900" s="T2303">v-v:tense-v:pn</ta>
            <ta e="T2305" id="Seg_2901" s="T2304">ptcl</ta>
            <ta e="T2306" id="Seg_2902" s="T2305">v-v:tense-v:pn</ta>
            <ta e="T2307" id="Seg_2903" s="T2306">adv</ta>
            <ta e="T2308" id="Seg_2904" s="T2307">dempro-n:case</ta>
            <ta e="T2309" id="Seg_2905" s="T2308">n-n:case</ta>
            <ta e="T2310" id="Seg_2906" s="T2309">v-v:tense-v:pn</ta>
            <ta e="T2311" id="Seg_2907" s="T2310">adv</ta>
            <ta e="T2312" id="Seg_2908" s="T2311">v-v:tense-v:pn</ta>
            <ta e="T2313" id="Seg_2909" s="T2312">conj</ta>
            <ta e="T2314" id="Seg_2910" s="T2313">n-n:case</ta>
            <ta e="T2315" id="Seg_2911" s="T2314">v-v:tense-v:pn</ta>
            <ta e="T2316" id="Seg_2912" s="T2315">adv</ta>
            <ta e="T2317" id="Seg_2913" s="T2316">v-v:tense-v:pn</ta>
            <ta e="T2318" id="Seg_2914" s="T2317">n-n:case</ta>
            <ta e="T2319" id="Seg_2915" s="T2318">v-v:tense-v:pn</ta>
            <ta e="T2320" id="Seg_2916" s="T2319">conj</ta>
            <ta e="T2321" id="Seg_2917" s="T2320">dempro-n:case</ta>
            <ta e="T2322" id="Seg_2918" s="T2321">n-n:case</ta>
            <ta e="T2323" id="Seg_2919" s="T2322">v-v:tense-v:pn</ta>
            <ta e="T2324" id="Seg_2920" s="T2323">v-v&gt;v-v:pn</ta>
            <ta e="T2325" id="Seg_2921" s="T2324">ptcl</ta>
            <ta e="T2326" id="Seg_2922" s="T2325">v-v:ins-v:mood.pn</ta>
            <ta e="T2327" id="Seg_2923" s="T2326">n-n:case</ta>
            <ta e="T2328" id="Seg_2924" s="T2327">conj</ta>
            <ta e="T2329" id="Seg_2925" s="T2328">adv</ta>
            <ta e="T2330" id="Seg_2926" s="T2329">que-n:case=ptcl</ta>
            <ta e="T2331" id="Seg_2927" s="T2330">ptcl</ta>
            <ta e="T2332" id="Seg_2928" s="T2331">v-v:tense-v:pn</ta>
            <ta e="T2333" id="Seg_2929" s="T2332">v-v:ins-v:mood.pn</ta>
            <ta e="T2334" id="Seg_2930" s="T2333">dempro-n:case</ta>
            <ta e="T2335" id="Seg_2931" s="T2334">v-v:tense-v:pn</ta>
            <ta e="T2336" id="Seg_2932" s="T2335">dempro-n:case</ta>
            <ta e="T2337" id="Seg_2933" s="T2336">n-n:case.poss</ta>
            <ta e="T2338" id="Seg_2934" s="T2337">adv</ta>
            <ta e="T2339" id="Seg_2935" s="T2338">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2340" id="Seg_2936" s="T2339">dempro-n:case</ta>
            <ta e="T2341" id="Seg_2937" s="T2340">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2342" id="Seg_2938" s="T2341">adv</ta>
            <ta e="T2343" id="Seg_2939" s="T2342">quant</ta>
            <ta e="T2344" id="Seg_2940" s="T2343">v-v&gt;v-v:pn</ta>
            <ta e="T2345" id="Seg_2941" s="T2344">adv</ta>
            <ta e="T2346" id="Seg_2942" s="T2345">n-n:case.poss</ta>
            <ta e="T2347" id="Seg_2943" s="T2346">v-v:tense-v:pn</ta>
            <ta e="T2348" id="Seg_2944" s="T2347">v-v:mood.pn</ta>
            <ta e="T2349" id="Seg_2945" s="T2348">n-n:case</ta>
            <ta e="T2350" id="Seg_2946" s="T2349">n-n:case.poss</ta>
            <ta e="T2351" id="Seg_2947" s="T2350">v-v:tense-v:pn</ta>
            <ta e="T2352" id="Seg_2948" s="T2351">ptcl</ta>
            <ta e="T2354" id="Seg_2949" s="T2353">v-v:mood.pn</ta>
            <ta e="T2355" id="Seg_2950" s="T2354">conj</ta>
            <ta e="T2356" id="Seg_2951" s="T2355">adv</ta>
            <ta e="T2357" id="Seg_2952" s="T2356">n-n:case</ta>
            <ta e="T2358" id="Seg_2953" s="T2357">v</ta>
            <ta e="T2359" id="Seg_2954" s="T2358">v-v:ins-v:mood.pn</ta>
            <ta e="T2360" id="Seg_2955" s="T2359">dempro-n:case</ta>
            <ta e="T2361" id="Seg_2956" s="T2360">n-n:case</ta>
            <ta e="T2362" id="Seg_2957" s="T2361">v-v:tense-v:pn</ta>
            <ta e="T2363" id="Seg_2958" s="T2362">adv</ta>
            <ta e="T2364" id="Seg_2959" s="T2363">n-n:case.poss</ta>
            <ta e="T2365" id="Seg_2960" s="T2364">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2366" id="Seg_2961" s="T2365">n-n:case.poss</ta>
            <ta e="T2367" id="Seg_2962" s="T2366">adv</ta>
            <ta e="T2368" id="Seg_2963" s="T2367">v-v&gt;v-v:pn</ta>
            <ta e="T2369" id="Seg_2964" s="T2368">v</ta>
            <ta e="T2370" id="Seg_2965" s="T2369">n-n:case</ta>
            <ta e="T2371" id="Seg_2966" s="T2370">v</ta>
            <ta e="T2372" id="Seg_2967" s="T2371">n-n:case</ta>
            <ta e="T2373" id="Seg_2968" s="T2372">adv</ta>
            <ta e="T2374" id="Seg_2969" s="T2373">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2375" id="Seg_2970" s="T2374">n-n:case</ta>
            <ta e="T2376" id="Seg_2971" s="T2375">v-v:tense-v:pn</ta>
            <ta e="T2377" id="Seg_2972" s="T2376">adv</ta>
            <ta e="T2378" id="Seg_2973" s="T2377">n-n:case</ta>
            <ta e="T2379" id="Seg_2974" s="T2378">v-v&gt;v-v:pn</ta>
            <ta e="T2382" id="Seg_2975" s="T2381">v-v:tense-v:pn</ta>
            <ta e="T2383" id="Seg_2976" s="T2382">adj-n:case</ta>
            <ta e="T2384" id="Seg_2977" s="T2383">n-n:case.poss</ta>
            <ta e="T2385" id="Seg_2978" s="T2384">adv</ta>
            <ta e="T2386" id="Seg_2979" s="T2385">n-n:case</ta>
            <ta e="T2387" id="Seg_2980" s="T2386">adv</ta>
            <ta e="T2388" id="Seg_2981" s="T2387">v-v&gt;v-v:pn</ta>
            <ta e="T2389" id="Seg_2982" s="T2388">que-n:case</ta>
            <ta e="T2390" id="Seg_2983" s="T2389">pers</ta>
            <ta e="T2391" id="Seg_2984" s="T2390">v-v:n.fin</ta>
            <ta e="T2392" id="Seg_2985" s="T2391">ptcl</ta>
            <ta e="T2393" id="Seg_2986" s="T2392">ptcl</ta>
            <ta e="T2394" id="Seg_2987" s="T2393">v-v:n.fin</ta>
            <ta e="T2395" id="Seg_2988" s="T2394">dempro</ta>
            <ta e="T2396" id="Seg_2989" s="T2395">dempro</ta>
            <ta e="T2397" id="Seg_2990" s="T2396">v-v:tense-v:pn</ta>
            <ta e="T2398" id="Seg_2991" s="T2397">dempro-n:case</ta>
            <ta e="T2399" id="Seg_2992" s="T2398">n-n:case.poss</ta>
            <ta e="T2400" id="Seg_2993" s="T2399">num-num&gt;num</ta>
            <ta e="T2401" id="Seg_2994" s="T2400">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2402" id="Seg_2995" s="T2401">conj</ta>
            <ta e="T2403" id="Seg_2996" s="T2402">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2404" id="Seg_2997" s="T2403">conj</ta>
            <ta e="T2405" id="Seg_2998" s="T2404">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2406" id="Seg_2999" s="T2405">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2146" id="Seg_3000" s="T2145">v</ta>
            <ta e="T2147" id="Seg_3001" s="T2146">n</ta>
            <ta e="T2148" id="Seg_3002" s="T2147">n</ta>
            <ta e="T2149" id="Seg_3003" s="T2148">dempro</ta>
            <ta e="T2150" id="Seg_3004" s="T2149">num</ta>
            <ta e="T2151" id="Seg_3005" s="T2150">n</ta>
            <ta e="T2152" id="Seg_3006" s="T2151">v</ta>
            <ta e="T2153" id="Seg_3007" s="T2152">dempro</ta>
            <ta e="T2154" id="Seg_3008" s="T2153">v</ta>
            <ta e="T2156" id="Seg_3009" s="T2155">n</ta>
            <ta e="T2157" id="Seg_3010" s="T2156">n</ta>
            <ta e="T2158" id="Seg_3011" s="T2157">v</ta>
            <ta e="T2159" id="Seg_3012" s="T2158">n</ta>
            <ta e="T2160" id="Seg_3013" s="T2159">v</ta>
            <ta e="T2161" id="Seg_3014" s="T2160">conj</ta>
            <ta e="T2162" id="Seg_3015" s="T2161">n</ta>
            <ta e="T2163" id="Seg_3016" s="T2162">n</ta>
            <ta e="T2164" id="Seg_3017" s="T2163">v</ta>
            <ta e="T2165" id="Seg_3018" s="T2164">v</ta>
            <ta e="T2166" id="Seg_3019" s="T2165">v</ta>
            <ta e="T2167" id="Seg_3020" s="T2166">conj</ta>
            <ta e="T2168" id="Seg_3021" s="T2167">n</ta>
            <ta e="T2169" id="Seg_3022" s="T2168">quant</ta>
            <ta e="T2170" id="Seg_3023" s="T2169">v</ta>
            <ta e="T2171" id="Seg_3024" s="T2170">n</ta>
            <ta e="T2172" id="Seg_3025" s="T2171">v</ta>
            <ta e="T2173" id="Seg_3026" s="T2172">que</ta>
            <ta e="T2174" id="Seg_3027" s="T2173">pers</ta>
            <ta e="T2175" id="Seg_3028" s="T2174">n</ta>
            <ta e="T2176" id="Seg_3029" s="T2175">ptcl</ta>
            <ta e="T2177" id="Seg_3030" s="T2176">v</ta>
            <ta e="T2178" id="Seg_3031" s="T2177">adv</ta>
            <ta e="T2179" id="Seg_3032" s="T2178">v</ta>
            <ta e="T2180" id="Seg_3033" s="T2179">v</ta>
            <ta e="T2181" id="Seg_3034" s="T2180">n</ta>
            <ta e="T2182" id="Seg_3035" s="T2181">v</ta>
            <ta e="T2183" id="Seg_3036" s="T2182">n</ta>
            <ta e="T2184" id="Seg_3037" s="T2183">v</ta>
            <ta e="T2185" id="Seg_3038" s="T2184">v</ta>
            <ta e="T2186" id="Seg_3039" s="T2185">conj</ta>
            <ta e="T2187" id="Seg_3040" s="T2186">n</ta>
            <ta e="T2188" id="Seg_3041" s="T2187">n</ta>
            <ta e="T2189" id="Seg_3042" s="T2188">v</ta>
            <ta e="T2190" id="Seg_3043" s="T2189">v</ta>
            <ta e="T2191" id="Seg_3044" s="T2190">dempro</ta>
            <ta e="T2192" id="Seg_3045" s="T2191">v</ta>
            <ta e="T2193" id="Seg_3046" s="T2192">conj</ta>
            <ta e="T2195" id="Seg_3047" s="T2194">v</ta>
            <ta e="T2196" id="Seg_3048" s="T2195">dempro</ta>
            <ta e="T2197" id="Seg_3049" s="T2196">pers</ta>
            <ta e="T2198" id="Seg_3050" s="T2197">n</ta>
            <ta e="T2199" id="Seg_3051" s="T2198">pers</ta>
            <ta e="T2200" id="Seg_3052" s="T2199">v</ta>
            <ta e="T2201" id="Seg_3053" s="T2200">n</ta>
            <ta e="T2202" id="Seg_3054" s="T2201">adv</ta>
            <ta e="T2203" id="Seg_3055" s="T2202">v</ta>
            <ta e="T2204" id="Seg_3056" s="T2203">v</ta>
            <ta e="T2205" id="Seg_3057" s="T2204">dempro</ta>
            <ta e="T2206" id="Seg_3058" s="T2205">n</ta>
            <ta e="T2207" id="Seg_3059" s="T2206">n</ta>
            <ta e="T2208" id="Seg_3060" s="T2207">adv</ta>
            <ta e="T2209" id="Seg_3061" s="T2208">num</ta>
            <ta e="T2210" id="Seg_3062" s="T2209">adj</ta>
            <ta e="T2211" id="Seg_3063" s="T2210">n</ta>
            <ta e="T2212" id="Seg_3064" s="T2211">v</ta>
            <ta e="T2213" id="Seg_3065" s="T2212">v</ta>
            <ta e="T2214" id="Seg_3066" s="T2213">n</ta>
            <ta e="T2215" id="Seg_3067" s="T2214">adj</ta>
            <ta e="T2216" id="Seg_3068" s="T2215">dempro</ta>
            <ta e="T2217" id="Seg_3069" s="T2216">v</ta>
            <ta e="T2218" id="Seg_3070" s="T2217">conj</ta>
            <ta e="T2219" id="Seg_3071" s="T2218">v</ta>
            <ta e="T2220" id="Seg_3072" s="T2219">adv</ta>
            <ta e="T2221" id="Seg_3073" s="T2220">dempro</ta>
            <ta e="T2222" id="Seg_3074" s="T2221">v</ta>
            <ta e="T2223" id="Seg_3075" s="T2222">n</ta>
            <ta e="T2224" id="Seg_3076" s="T2223">adv</ta>
            <ta e="T2225" id="Seg_3077" s="T2224">adj</ta>
            <ta e="T2226" id="Seg_3078" s="T2225">n</ta>
            <ta e="T2227" id="Seg_3079" s="T2226">v</ta>
            <ta e="T2228" id="Seg_3080" s="T2227">n</ta>
            <ta e="T2229" id="Seg_3081" s="T2228">adj</ta>
            <ta e="T2230" id="Seg_3082" s="T2229">conj</ta>
            <ta e="T2231" id="Seg_3083" s="T2230">v</ta>
            <ta e="T2232" id="Seg_3084" s="T2231">adv</ta>
            <ta e="T2233" id="Seg_3085" s="T2232">dempro</ta>
            <ta e="T2234" id="Seg_3086" s="T2233">v</ta>
            <ta e="T2235" id="Seg_3087" s="T2234">dempro</ta>
            <ta e="T2236" id="Seg_3088" s="T2235">n</ta>
            <ta e="T2237" id="Seg_3089" s="T2236">v</ta>
            <ta e="T2238" id="Seg_3090" s="T2237">adv</ta>
            <ta e="T2239" id="Seg_3091" s="T2238">num</ta>
            <ta e="T2240" id="Seg_3092" s="T2239">n</ta>
            <ta e="T2241" id="Seg_3093" s="T2240">v</ta>
            <ta e="T2242" id="Seg_3094" s="T2241">v</ta>
            <ta e="T2243" id="Seg_3095" s="T2242">n</ta>
            <ta e="T2244" id="Seg_3096" s="T2243">adj</ta>
            <ta e="T2245" id="Seg_3097" s="T2244">conj</ta>
            <ta e="T2246" id="Seg_3098" s="T2245">v</ta>
            <ta e="T2247" id="Seg_3099" s="T2246">adv</ta>
            <ta e="T2248" id="Seg_3100" s="T2247">dempro</ta>
            <ta e="T2249" id="Seg_3101" s="T2248">v</ta>
            <ta e="T2250" id="Seg_3102" s="T2249">dempro</ta>
            <ta e="T2251" id="Seg_3103" s="T2250">ptcl</ta>
            <ta e="T2252" id="Seg_3104" s="T2251">v</ta>
            <ta e="T2253" id="Seg_3105" s="T2252">n</ta>
            <ta e="T2254" id="Seg_3106" s="T2253">adv</ta>
            <ta e="T2255" id="Seg_3107" s="T2254">dempro</ta>
            <ta e="T2257" id="Seg_3108" s="T2256">dempro</ta>
            <ta e="T2259" id="Seg_3109" s="T2258">n</ta>
            <ta e="T2260" id="Seg_3110" s="T2259">v</ta>
            <ta e="T2261" id="Seg_3111" s="T2260">v</ta>
            <ta e="T2262" id="Seg_3112" s="T2261">n</ta>
            <ta e="T2263" id="Seg_3113" s="T2262">v</ta>
            <ta e="T2264" id="Seg_3114" s="T2263">v</ta>
            <ta e="T2265" id="Seg_3115" s="T2264">que</ta>
            <ta e="T2266" id="Seg_3116" s="T2265">pers</ta>
            <ta e="T2267" id="Seg_3117" s="T2266">v</ta>
            <ta e="T2268" id="Seg_3118" s="T2267">ptcl</ta>
            <ta e="T2269" id="Seg_3119" s="T2268">pers</ta>
            <ta e="T2270" id="Seg_3120" s="T2269">pers</ta>
            <ta e="T2271" id="Seg_3121" s="T2270">ptcl</ta>
            <ta e="T2272" id="Seg_3122" s="T2271">ptcl</ta>
            <ta e="T2273" id="Seg_3123" s="T2272">v</ta>
            <ta e="T2274" id="Seg_3124" s="T2273">adv</ta>
            <ta e="T2275" id="Seg_3125" s="T2274">dempro</ta>
            <ta e="T2276" id="Seg_3126" s="T2275">n</ta>
            <ta e="T2277" id="Seg_3127" s="T2276">conj</ta>
            <ta e="T2279" id="Seg_3128" s="T2278">n</ta>
            <ta e="T2280" id="Seg_3129" s="T2279">n</ta>
            <ta e="T2281" id="Seg_3130" s="T2280">v</ta>
            <ta e="T2282" id="Seg_3131" s="T2281">n</ta>
            <ta e="T2283" id="Seg_3132" s="T2282">v</ta>
            <ta e="T2284" id="Seg_3133" s="T2283">n</ta>
            <ta e="T2285" id="Seg_3134" s="T2284">dempro</ta>
            <ta e="T2286" id="Seg_3135" s="T2285">v</ta>
            <ta e="T2287" id="Seg_3136" s="T2286">dempro</ta>
            <ta e="T2288" id="Seg_3137" s="T2287">n</ta>
            <ta e="T2289" id="Seg_3138" s="T2288">v</ta>
            <ta e="T2290" id="Seg_3139" s="T2289">conj</ta>
            <ta e="T2291" id="Seg_3140" s="T2290">v</ta>
            <ta e="T2292" id="Seg_3141" s="T2291">n</ta>
            <ta e="T2293" id="Seg_3142" s="T2292">n</ta>
            <ta e="T2294" id="Seg_3143" s="T2293">v</ta>
            <ta e="T2295" id="Seg_3144" s="T2294">v</ta>
            <ta e="T2296" id="Seg_3145" s="T2295">n</ta>
            <ta e="T2297" id="Seg_3146" s="T2296">dempro</ta>
            <ta e="T2298" id="Seg_3147" s="T2297">ptcl</ta>
            <ta e="T2299" id="Seg_3148" s="T2298">v</ta>
            <ta e="T2300" id="Seg_3149" s="T2299">conj</ta>
            <ta e="T2301" id="Seg_3150" s="T2300">n</ta>
            <ta e="T2302" id="Seg_3151" s="T2301">v</ta>
            <ta e="T2303" id="Seg_3152" s="T2302">v</ta>
            <ta e="T2304" id="Seg_3153" s="T2303">v</ta>
            <ta e="T2305" id="Seg_3154" s="T2304">ptcl</ta>
            <ta e="T2306" id="Seg_3155" s="T2305">v</ta>
            <ta e="T2307" id="Seg_3156" s="T2306">adv</ta>
            <ta e="T2308" id="Seg_3157" s="T2307">dempro</ta>
            <ta e="T2309" id="Seg_3158" s="T2308">n</ta>
            <ta e="T2310" id="Seg_3159" s="T2309">v</ta>
            <ta e="T2311" id="Seg_3160" s="T2310">adv</ta>
            <ta e="T2312" id="Seg_3161" s="T2311">v</ta>
            <ta e="T2313" id="Seg_3162" s="T2312">conj</ta>
            <ta e="T2314" id="Seg_3163" s="T2313">n</ta>
            <ta e="T2315" id="Seg_3164" s="T2314">v</ta>
            <ta e="T2316" id="Seg_3165" s="T2315">adv</ta>
            <ta e="T2317" id="Seg_3166" s="T2316">v</ta>
            <ta e="T2318" id="Seg_3167" s="T2317">n</ta>
            <ta e="T2319" id="Seg_3168" s="T2318">v</ta>
            <ta e="T2320" id="Seg_3169" s="T2319">conj</ta>
            <ta e="T2321" id="Seg_3170" s="T2320">dempro</ta>
            <ta e="T2322" id="Seg_3171" s="T2321">n</ta>
            <ta e="T2323" id="Seg_3172" s="T2322">v</ta>
            <ta e="T2324" id="Seg_3173" s="T2323">v</ta>
            <ta e="T2325" id="Seg_3174" s="T2324">ptcl</ta>
            <ta e="T2326" id="Seg_3175" s="T2325">v</ta>
            <ta e="T2327" id="Seg_3176" s="T2326">n</ta>
            <ta e="T2328" id="Seg_3177" s="T2327">conj</ta>
            <ta e="T2329" id="Seg_3178" s="T2328">adv</ta>
            <ta e="T2330" id="Seg_3179" s="T2329">que</ta>
            <ta e="T2331" id="Seg_3180" s="T2330">ptcl</ta>
            <ta e="T2332" id="Seg_3181" s="T2331">v</ta>
            <ta e="T2333" id="Seg_3182" s="T2332">v</ta>
            <ta e="T2334" id="Seg_3183" s="T2333">dempro</ta>
            <ta e="T2335" id="Seg_3184" s="T2334">v</ta>
            <ta e="T2336" id="Seg_3185" s="T2335">dempro</ta>
            <ta e="T2337" id="Seg_3186" s="T2336">n</ta>
            <ta e="T2338" id="Seg_3187" s="T2337">adv</ta>
            <ta e="T2339" id="Seg_3188" s="T2338">v</ta>
            <ta e="T2340" id="Seg_3189" s="T2339">dempro</ta>
            <ta e="T2341" id="Seg_3190" s="T2340">v</ta>
            <ta e="T2342" id="Seg_3191" s="T2341">adv</ta>
            <ta e="T2343" id="Seg_3192" s="T2342">quant</ta>
            <ta e="T2344" id="Seg_3193" s="T2343">v</ta>
            <ta e="T2345" id="Seg_3194" s="T2344">adv</ta>
            <ta e="T2346" id="Seg_3195" s="T2345">n</ta>
            <ta e="T2347" id="Seg_3196" s="T2346">v</ta>
            <ta e="T2348" id="Seg_3197" s="T2347">v</ta>
            <ta e="T2349" id="Seg_3198" s="T2348">n</ta>
            <ta e="T2350" id="Seg_3199" s="T2349">n</ta>
            <ta e="T2351" id="Seg_3200" s="T2350">v</ta>
            <ta e="T2352" id="Seg_3201" s="T2351">ptcl</ta>
            <ta e="T2354" id="Seg_3202" s="T2353">v</ta>
            <ta e="T2355" id="Seg_3203" s="T2354">conj</ta>
            <ta e="T2356" id="Seg_3204" s="T2355">adv</ta>
            <ta e="T2357" id="Seg_3205" s="T2356">n</ta>
            <ta e="T2358" id="Seg_3206" s="T2357">v</ta>
            <ta e="T2359" id="Seg_3207" s="T2358">v</ta>
            <ta e="T2360" id="Seg_3208" s="T2359">dempro</ta>
            <ta e="T2361" id="Seg_3209" s="T2360">n</ta>
            <ta e="T2362" id="Seg_3210" s="T2361">v</ta>
            <ta e="T2363" id="Seg_3211" s="T2362">adv</ta>
            <ta e="T2364" id="Seg_3212" s="T2363">n</ta>
            <ta e="T2365" id="Seg_3213" s="T2364">v</ta>
            <ta e="T2366" id="Seg_3214" s="T2365">n</ta>
            <ta e="T2367" id="Seg_3215" s="T2366">adv</ta>
            <ta e="T2368" id="Seg_3216" s="T2367">v</ta>
            <ta e="T2369" id="Seg_3217" s="T2368">v</ta>
            <ta e="T2370" id="Seg_3218" s="T2369">n</ta>
            <ta e="T2371" id="Seg_3219" s="T2370">v</ta>
            <ta e="T2372" id="Seg_3220" s="T2371">n</ta>
            <ta e="T2373" id="Seg_3221" s="T2372">adv</ta>
            <ta e="T2374" id="Seg_3222" s="T2373">v</ta>
            <ta e="T2375" id="Seg_3223" s="T2374">n</ta>
            <ta e="T2376" id="Seg_3224" s="T2375">v</ta>
            <ta e="T2377" id="Seg_3225" s="T2376">adv</ta>
            <ta e="T2378" id="Seg_3226" s="T2377">n</ta>
            <ta e="T2379" id="Seg_3227" s="T2378">v</ta>
            <ta e="T2382" id="Seg_3228" s="T2381">v</ta>
            <ta e="T2383" id="Seg_3229" s="T2382">adj</ta>
            <ta e="T2384" id="Seg_3230" s="T2383">n</ta>
            <ta e="T2385" id="Seg_3231" s="T2384">adv</ta>
            <ta e="T2386" id="Seg_3232" s="T2385">n</ta>
            <ta e="T2387" id="Seg_3233" s="T2386">adv</ta>
            <ta e="T2388" id="Seg_3234" s="T2387">v</ta>
            <ta e="T2389" id="Seg_3235" s="T2388">que</ta>
            <ta e="T2390" id="Seg_3236" s="T2389">pers</ta>
            <ta e="T2391" id="Seg_3237" s="T2390">v</ta>
            <ta e="T2392" id="Seg_3238" s="T2391">ptcl</ta>
            <ta e="T2393" id="Seg_3239" s="T2392">ptcl</ta>
            <ta e="T2394" id="Seg_3240" s="T2393">v</ta>
            <ta e="T2395" id="Seg_3241" s="T2394">dempro</ta>
            <ta e="T2396" id="Seg_3242" s="T2395">dempro</ta>
            <ta e="T2397" id="Seg_3243" s="T2396">v</ta>
            <ta e="T2398" id="Seg_3244" s="T2397">dempro</ta>
            <ta e="T2399" id="Seg_3245" s="T2398">n</ta>
            <ta e="T2400" id="Seg_3246" s="T2399">num</ta>
            <ta e="T2401" id="Seg_3247" s="T2400">v</ta>
            <ta e="T2402" id="Seg_3248" s="T2401">conj</ta>
            <ta e="T2403" id="Seg_3249" s="T2402">v</ta>
            <ta e="T2404" id="Seg_3250" s="T2403">conj</ta>
            <ta e="T2405" id="Seg_3251" s="T2404">v</ta>
            <ta e="T2406" id="Seg_3252" s="T2405">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2147" id="Seg_3253" s="T2146">np.h:E</ta>
            <ta e="T2148" id="Seg_3254" s="T2147">np.h:E</ta>
            <ta e="T2149" id="Seg_3255" s="T2148">pro.h:Poss</ta>
            <ta e="T2151" id="Seg_3256" s="T2150">np.h:Th</ta>
            <ta e="T2153" id="Seg_3257" s="T2152">pro.h:A</ta>
            <ta e="T2156" id="Seg_3258" s="T2155">np:G</ta>
            <ta e="T2157" id="Seg_3259" s="T2156">np:Th</ta>
            <ta e="T2158" id="Seg_3260" s="T2157">0.3.h:A</ta>
            <ta e="T2159" id="Seg_3261" s="T2158">np:L</ta>
            <ta e="T2160" id="Seg_3262" s="T2159">0.3.h:A</ta>
            <ta e="T2162" id="Seg_3263" s="T2161">np:L</ta>
            <ta e="T2163" id="Seg_3264" s="T2162">np:Th</ta>
            <ta e="T2165" id="Seg_3265" s="T2164">0.3.h:A</ta>
            <ta e="T2166" id="Seg_3266" s="T2165">0.3.h:A</ta>
            <ta e="T2168" id="Seg_3267" s="T2167">np:Th</ta>
            <ta e="T2170" id="Seg_3268" s="T2169">0.3.h:Th</ta>
            <ta e="T2171" id="Seg_3269" s="T2170">np.h:A</ta>
            <ta e="T2174" id="Seg_3270" s="T2173">pro.h:Poss</ta>
            <ta e="T2175" id="Seg_3271" s="T2174">np:Th</ta>
            <ta e="T2177" id="Seg_3272" s="T2176">0.1.h:A</ta>
            <ta e="T2179" id="Seg_3273" s="T2178">0.3.h:A</ta>
            <ta e="T2180" id="Seg_3274" s="T2179">0.3.h:A</ta>
            <ta e="T2182" id="Seg_3275" s="T2181">0.2.h:A</ta>
            <ta e="T2183" id="Seg_3276" s="T2182">np.h:R</ta>
            <ta e="T2184" id="Seg_3277" s="T2183">0.3.h:A</ta>
            <ta e="T2185" id="Seg_3278" s="T2184">0.2.h:A</ta>
            <ta e="T2188" id="Seg_3279" s="T2187">np.h:R</ta>
            <ta e="T2189" id="Seg_3280" s="T2188">0.3.h:A</ta>
            <ta e="T2190" id="Seg_3281" s="T2189">0.2.h:A</ta>
            <ta e="T2191" id="Seg_3282" s="T2190">pro.h:A</ta>
            <ta e="T2195" id="Seg_3283" s="T2194">0.3.h:A</ta>
            <ta e="T2196" id="Seg_3284" s="T2195">pro.h:B</ta>
            <ta e="T2197" id="Seg_3285" s="T2196">pro.h:A</ta>
            <ta e="T2198" id="Seg_3286" s="T2197">np.h:Th</ta>
            <ta e="T2199" id="Seg_3287" s="T2198">pro.h:R</ta>
            <ta e="T2202" id="Seg_3288" s="T2201">adv:Time</ta>
            <ta e="T2203" id="Seg_3289" s="T2202">0.3.h:A</ta>
            <ta e="T2205" id="Seg_3290" s="T2204">pro.h:A</ta>
            <ta e="T2206" id="Seg_3291" s="T2205">np:Th</ta>
            <ta e="T2207" id="Seg_3292" s="T2206">np:G</ta>
            <ta e="T2208" id="Seg_3293" s="T2207">adv:Time</ta>
            <ta e="T2211" id="Seg_3294" s="T2210">np.h:R</ta>
            <ta e="T2212" id="Seg_3295" s="T2211">0.3.h:A</ta>
            <ta e="T2213" id="Seg_3296" s="T2212">0.2.h:A</ta>
            <ta e="T2214" id="Seg_3297" s="T2213">np:Th</ta>
            <ta e="T2216" id="Seg_3298" s="T2215">pro.h:A</ta>
            <ta e="T2219" id="Seg_3299" s="T2218">0.3.h:A</ta>
            <ta e="T2220" id="Seg_3300" s="T2219">adv:G</ta>
            <ta e="T2221" id="Seg_3301" s="T2220">pro.h:Th</ta>
            <ta e="T2223" id="Seg_3302" s="T2222">np.h:A</ta>
            <ta e="T2224" id="Seg_3303" s="T2223">adv:Time</ta>
            <ta e="T2226" id="Seg_3304" s="T2225">np.h:Com</ta>
            <ta e="T2227" id="Seg_3305" s="T2226">0.2.h:A</ta>
            <ta e="T2228" id="Seg_3306" s="T2227">np:Th</ta>
            <ta e="T2231" id="Seg_3307" s="T2230">0.2.h:A</ta>
            <ta e="T2232" id="Seg_3308" s="T2231">adv:G</ta>
            <ta e="T2233" id="Seg_3309" s="T2232">pro.h:A</ta>
            <ta e="T2235" id="Seg_3310" s="T2234">pro.h:Th</ta>
            <ta e="T2236" id="Seg_3311" s="T2235">np.h:A</ta>
            <ta e="T2238" id="Seg_3312" s="T2237">adv:Time</ta>
            <ta e="T2240" id="Seg_3313" s="T2239">np.h:R</ta>
            <ta e="T2241" id="Seg_3314" s="T2240">0.3.h:A</ta>
            <ta e="T2242" id="Seg_3315" s="T2241">0.2.h:A</ta>
            <ta e="T2243" id="Seg_3316" s="T2242">np:Th</ta>
            <ta e="T2246" id="Seg_3317" s="T2245">0.2.h:A</ta>
            <ta e="T2247" id="Seg_3318" s="T2246">adv:G</ta>
            <ta e="T2248" id="Seg_3319" s="T2247">pro.h:A</ta>
            <ta e="T2250" id="Seg_3320" s="T2249">pro.h:Th</ta>
            <ta e="T2253" id="Seg_3321" s="T2252">np.h:A</ta>
            <ta e="T2254" id="Seg_3322" s="T2253">adv:Time</ta>
            <ta e="T2255" id="Seg_3323" s="T2254">pro.h:A</ta>
            <ta e="T2259" id="Seg_3324" s="T2258">np.h:A</ta>
            <ta e="T2261" id="Seg_3325" s="T2260">0.1.h:A</ta>
            <ta e="T2262" id="Seg_3326" s="T2261">np:G</ta>
            <ta e="T2264" id="Seg_3327" s="T2263">0.3.h:A</ta>
            <ta e="T2265" id="Seg_3328" s="T2264">pro:Ins</ta>
            <ta e="T2266" id="Seg_3329" s="T2265">pro.h:R</ta>
            <ta e="T2270" id="Seg_3330" s="T2269">pro.h:A</ta>
            <ta e="T2274" id="Seg_3331" s="T2273">adv:Time</ta>
            <ta e="T2275" id="Seg_3332" s="T2274">pro.h:A</ta>
            <ta e="T2276" id="Seg_3333" s="T2275">np:Th</ta>
            <ta e="T2279" id="Seg_3334" s="T2278">np.h:A</ta>
            <ta e="T2280" id="Seg_3335" s="T2279">np:Th</ta>
            <ta e="T2282" id="Seg_3336" s="T2281">np:G</ta>
            <ta e="T2284" id="Seg_3337" s="T2283">np:L</ta>
            <ta e="T2285" id="Seg_3338" s="T2284">pro:A</ta>
            <ta e="T2288" id="Seg_3339" s="T2287">np.h:A</ta>
            <ta e="T2292" id="Seg_3340" s="T2291">np:G</ta>
            <ta e="T2293" id="Seg_3341" s="T2292">np.h:R</ta>
            <ta e="T2294" id="Seg_3342" s="T2293">0.3.h:A</ta>
            <ta e="T2295" id="Seg_3343" s="T2294">0.2.h:A</ta>
            <ta e="T2296" id="Seg_3344" s="T2295">np:P</ta>
            <ta e="T2297" id="Seg_3345" s="T2296">pro.h:A</ta>
            <ta e="T2301" id="Seg_3346" s="T2300">np:L</ta>
            <ta e="T2302" id="Seg_3347" s="T2301">0.3.h:A</ta>
            <ta e="T2303" id="Seg_3348" s="T2302">0.3:E</ta>
            <ta e="T2304" id="Seg_3349" s="T2303">0.3:E</ta>
            <ta e="T2306" id="Seg_3350" s="T2305">0.3:P</ta>
            <ta e="T2307" id="Seg_3351" s="T2306">adv:Time</ta>
            <ta e="T2308" id="Seg_3352" s="T2307">pro.h:A</ta>
            <ta e="T2309" id="Seg_3353" s="T2308">np:L</ta>
            <ta e="T2310" id="Seg_3354" s="T2309">adv:L</ta>
            <ta e="T2311" id="Seg_3355" s="T2310">adv:L</ta>
            <ta e="T2312" id="Seg_3356" s="T2311">0.3:P</ta>
            <ta e="T2314" id="Seg_3357" s="T2313">np.h:A</ta>
            <ta e="T2316" id="Seg_3358" s="T2315">adv:Time</ta>
            <ta e="T2317" id="Seg_3359" s="T2316">0.3.h:A</ta>
            <ta e="T2318" id="Seg_3360" s="T2317">np:G</ta>
            <ta e="T2319" id="Seg_3361" s="T2318">0.3.h:A</ta>
            <ta e="T2321" id="Seg_3362" s="T2320">pro.h:A</ta>
            <ta e="T2322" id="Seg_3363" s="T2321">np:Th</ta>
            <ta e="T2324" id="Seg_3364" s="T2323">0.3.h:A</ta>
            <ta e="T2326" id="Seg_3365" s="T2325">0.2.h:A</ta>
            <ta e="T2327" id="Seg_3366" s="T2326">np:G</ta>
            <ta e="T2329" id="Seg_3367" s="T2328">adv:L</ta>
            <ta e="T2330" id="Seg_3368" s="T2329">pro:Th</ta>
            <ta e="T2333" id="Seg_3369" s="T2332">0.2.h:A</ta>
            <ta e="T2334" id="Seg_3370" s="T2333">pro.h:A</ta>
            <ta e="T2336" id="Seg_3371" s="T2335">pro.h:A</ta>
            <ta e="T2337" id="Seg_3372" s="T2336">np:Th</ta>
            <ta e="T2338" id="Seg_3373" s="T2337">adv:L</ta>
            <ta e="T2340" id="Seg_3374" s="T2339">pro.h:A</ta>
            <ta e="T2342" id="Seg_3375" s="T2341">adv:L</ta>
            <ta e="T2343" id="Seg_3376" s="T2342">np:Th</ta>
            <ta e="T2345" id="Seg_3377" s="T2344">adv:Time</ta>
            <ta e="T2346" id="Seg_3378" s="T2345">np:G</ta>
            <ta e="T2347" id="Seg_3379" s="T2346">0.3.h:A</ta>
            <ta e="T2348" id="Seg_3380" s="T2347">0.2.h:A</ta>
            <ta e="T2349" id="Seg_3381" s="T2348">np:Th</ta>
            <ta e="T2350" id="Seg_3382" s="T2349">np.h:A</ta>
            <ta e="T2354" id="Seg_3383" s="T2353">0.2.h:A</ta>
            <ta e="T2356" id="Seg_3384" s="T2355">adv:L</ta>
            <ta e="T2357" id="Seg_3385" s="T2356">np:Th</ta>
            <ta e="T2359" id="Seg_3386" s="T2358">0.2.h:A</ta>
            <ta e="T2360" id="Seg_3387" s="T2359">pro.h:A</ta>
            <ta e="T2361" id="Seg_3388" s="T2360">np:P</ta>
            <ta e="T2363" id="Seg_3389" s="T2362">adv:L</ta>
            <ta e="T2364" id="Seg_3390" s="T2363">np:A</ta>
            <ta e="T2366" id="Seg_3391" s="T2365">np.h:A</ta>
            <ta e="T2370" id="Seg_3392" s="T2369">np:Th</ta>
            <ta e="T2372" id="Seg_3393" s="T2371">np:Th</ta>
            <ta e="T2373" id="Seg_3394" s="T2372">adv:Time</ta>
            <ta e="T2374" id="Seg_3395" s="T2373">0.3.h:A</ta>
            <ta e="T2375" id="Seg_3396" s="T2374">np:P</ta>
            <ta e="T2376" id="Seg_3397" s="T2375">0.3.h:A</ta>
            <ta e="T2377" id="Seg_3398" s="T2376">adv:Time</ta>
            <ta e="T2378" id="Seg_3399" s="T2377">n:Time</ta>
            <ta e="T2379" id="Seg_3400" s="T2378">0.3.h:A</ta>
            <ta e="T2382" id="Seg_3401" s="T2381">0.3.h:A</ta>
            <ta e="T2384" id="Seg_3402" s="T2383">np:G</ta>
            <ta e="T2385" id="Seg_3403" s="T2384">adv:L</ta>
            <ta e="T2386" id="Seg_3404" s="T2385">np:G</ta>
            <ta e="T2387" id="Seg_3405" s="T2386">adv:L</ta>
            <ta e="T2388" id="Seg_3406" s="T2387">0.3.h:A</ta>
            <ta e="T2389" id="Seg_3407" s="T2388">pro:Th</ta>
            <ta e="T2390" id="Seg_3408" s="T2389">pro.h:R</ta>
            <ta e="T2396" id="Seg_3409" s="T2395">pro.h:A</ta>
            <ta e="T2398" id="Seg_3410" s="T2397">pro.h:Th</ta>
            <ta e="T2399" id="Seg_3411" s="T2398">np:L</ta>
            <ta e="T2400" id="Seg_3412" s="T2399">np.h:E</ta>
            <ta e="T2403" id="Seg_3413" s="T2402">0.3.h:E</ta>
            <ta e="T2405" id="Seg_3414" s="T2404">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2146" id="Seg_3415" s="T2145">v:pred</ta>
            <ta e="T2147" id="Seg_3416" s="T2146">np.h:S</ta>
            <ta e="T2148" id="Seg_3417" s="T2147">np.h:S</ta>
            <ta e="T2151" id="Seg_3418" s="T2150">np.h:S</ta>
            <ta e="T2152" id="Seg_3419" s="T2151">v:pred</ta>
            <ta e="T2153" id="Seg_3420" s="T2152">pro.h:S</ta>
            <ta e="T2154" id="Seg_3421" s="T2153">v:pred</ta>
            <ta e="T2157" id="Seg_3422" s="T2156">np:O</ta>
            <ta e="T2158" id="Seg_3423" s="T2157">v:pred 0.3.h:S</ta>
            <ta e="T2160" id="Seg_3424" s="T2159">v:pred 0.3.h:S</ta>
            <ta e="T2163" id="Seg_3425" s="T2162">np:S</ta>
            <ta e="T2164" id="Seg_3426" s="T2163">v:pred</ta>
            <ta e="T2165" id="Seg_3427" s="T2164">v:pred 0.3.h:S</ta>
            <ta e="T2166" id="Seg_3428" s="T2165">v:pred 0.3.h:S</ta>
            <ta e="T2168" id="Seg_3429" s="T2167">np:O</ta>
            <ta e="T2170" id="Seg_3430" s="T2169">v:pred 0.3.h:S</ta>
            <ta e="T2171" id="Seg_3431" s="T2170">np.h:S</ta>
            <ta e="T2172" id="Seg_3432" s="T2171">v:pred</ta>
            <ta e="T2175" id="Seg_3433" s="T2174">np:S</ta>
            <ta e="T2177" id="Seg_3434" s="T2176">v:pred 0.1.h:S</ta>
            <ta e="T2179" id="Seg_3435" s="T2178">v:pred 0.3.h:S</ta>
            <ta e="T2180" id="Seg_3436" s="T2179">v:pred 0.3.h:S</ta>
            <ta e="T2182" id="Seg_3437" s="T2181">v:pred 0.2.h:S</ta>
            <ta e="T2184" id="Seg_3438" s="T2183">v:pred 0.3.h:S</ta>
            <ta e="T2185" id="Seg_3439" s="T2184">v:pred 0.2.h:S</ta>
            <ta e="T2189" id="Seg_3440" s="T2188">v:pred 0.3.h:S</ta>
            <ta e="T2190" id="Seg_3441" s="T2189">v:pred 0.2.h:S</ta>
            <ta e="T2191" id="Seg_3442" s="T2190">pro.h:S</ta>
            <ta e="T2192" id="Seg_3443" s="T2191">v:pred</ta>
            <ta e="T2195" id="Seg_3444" s="T2194">v:pred 0.3.h:S</ta>
            <ta e="T2196" id="Seg_3445" s="T2195">pro.h:O</ta>
            <ta e="T2197" id="Seg_3446" s="T2196">pro.h:S</ta>
            <ta e="T2198" id="Seg_3447" s="T2197">np.h:O</ta>
            <ta e="T2200" id="Seg_3448" s="T2199">v:pred</ta>
            <ta e="T2203" id="Seg_3449" s="T2202">v:pred 0.3.h:S</ta>
            <ta e="T2204" id="Seg_3450" s="T2203">v:pred</ta>
            <ta e="T2205" id="Seg_3451" s="T2204">pro.h:S</ta>
            <ta e="T2206" id="Seg_3452" s="T2205">np:O</ta>
            <ta e="T2212" id="Seg_3453" s="T2211">v:pred 0.3.h:S</ta>
            <ta e="T2213" id="Seg_3454" s="T2212">v:pred 0.2.h:S</ta>
            <ta e="T2214" id="Seg_3455" s="T2213">np:O</ta>
            <ta e="T2216" id="Seg_3456" s="T2215">pro.h:S</ta>
            <ta e="T2217" id="Seg_3457" s="T2216">v:pred</ta>
            <ta e="T2219" id="Seg_3458" s="T2218">v:pred 0.3.h:S</ta>
            <ta e="T2221" id="Seg_3459" s="T2220">pro.h:O</ta>
            <ta e="T2222" id="Seg_3460" s="T2221">v:pred</ta>
            <ta e="T2223" id="Seg_3461" s="T2222">np.h:S</ta>
            <ta e="T2227" id="Seg_3462" s="T2226">v:pred 0.2.h:S</ta>
            <ta e="T2228" id="Seg_3463" s="T2227">np:O</ta>
            <ta e="T2231" id="Seg_3464" s="T2230">v:pred 0.2.h:S</ta>
            <ta e="T2233" id="Seg_3465" s="T2232">pro.h:S</ta>
            <ta e="T2234" id="Seg_3466" s="T2233">v:pred</ta>
            <ta e="T2235" id="Seg_3467" s="T2234">pro.h:O</ta>
            <ta e="T2236" id="Seg_3468" s="T2235">np.h:S</ta>
            <ta e="T2237" id="Seg_3469" s="T2236">v:pred</ta>
            <ta e="T2241" id="Seg_3470" s="T2240">v:pred 0.3.h:S</ta>
            <ta e="T2242" id="Seg_3471" s="T2241">v:pred 0.2.h:S</ta>
            <ta e="T2243" id="Seg_3472" s="T2242">np:O</ta>
            <ta e="T2246" id="Seg_3473" s="T2245">v:pred 0.2.h:S</ta>
            <ta e="T2248" id="Seg_3474" s="T2247">pro.h:S</ta>
            <ta e="T2249" id="Seg_3475" s="T2248">v:pred</ta>
            <ta e="T2250" id="Seg_3476" s="T2249">pro.h:O</ta>
            <ta e="T2252" id="Seg_3477" s="T2251">v:pred</ta>
            <ta e="T2253" id="Seg_3478" s="T2252">np.h:S</ta>
            <ta e="T2259" id="Seg_3479" s="T2258">np.h:S</ta>
            <ta e="T2260" id="Seg_3480" s="T2259">v:pred</ta>
            <ta e="T2261" id="Seg_3481" s="T2260">v:pred 0.1.h:S</ta>
            <ta e="T2263" id="Seg_3482" s="T2262">conv:pred</ta>
            <ta e="T2264" id="Seg_3483" s="T2263">v:pred 0.3.h:S</ta>
            <ta e="T2266" id="Seg_3484" s="T2265">pro.h:O</ta>
            <ta e="T2267" id="Seg_3485" s="T2266">v:pred</ta>
            <ta e="T2272" id="Seg_3486" s="T2271">ptcl:pred</ta>
            <ta e="T2275" id="Seg_3487" s="T2274">pro.h:S</ta>
            <ta e="T2276" id="Seg_3488" s="T2275">np:O</ta>
            <ta e="T2279" id="Seg_3489" s="T2278">np.h:S</ta>
            <ta e="T2280" id="Seg_3490" s="T2279">np:O</ta>
            <ta e="T2281" id="Seg_3491" s="T2280">v:pred</ta>
            <ta e="T2283" id="Seg_3492" s="T2282">v:pred</ta>
            <ta e="T2285" id="Seg_3493" s="T2284">pro:S</ta>
            <ta e="T2286" id="Seg_3494" s="T2285">v:pred</ta>
            <ta e="T2288" id="Seg_3495" s="T2287">np.h:S</ta>
            <ta e="T2289" id="Seg_3496" s="T2288">v:pred</ta>
            <ta e="T2291" id="Seg_3497" s="T2290">v:pred</ta>
            <ta e="T2294" id="Seg_3498" s="T2293">v:pred 0.3.h:S</ta>
            <ta e="T2295" id="Seg_3499" s="T2294">v:pred 0.2.h:S</ta>
            <ta e="T2296" id="Seg_3500" s="T2295">np:O</ta>
            <ta e="T2298" id="Seg_3501" s="T2297">ptcl:pred</ta>
            <ta e="T2302" id="Seg_3502" s="T2301">v:pred 0.3.h:S</ta>
            <ta e="T2303" id="Seg_3503" s="T2302">v:pred 0.3:S</ta>
            <ta e="T2304" id="Seg_3504" s="T2303">v:pred 0.3:S</ta>
            <ta e="T2305" id="Seg_3505" s="T2304">ptcl.neg</ta>
            <ta e="T2306" id="Seg_3506" s="T2305">v:pred 0.3:S</ta>
            <ta e="T2308" id="Seg_3507" s="T2307">pro.h:S</ta>
            <ta e="T2310" id="Seg_3508" s="T2309">v:pred</ta>
            <ta e="T2312" id="Seg_3509" s="T2311">v:pred 0.3:S</ta>
            <ta e="T2314" id="Seg_3510" s="T2313">np.h:S</ta>
            <ta e="T2315" id="Seg_3511" s="T2314">v:pred</ta>
            <ta e="T2317" id="Seg_3512" s="T2316">v:pred 0.3.h:S</ta>
            <ta e="T2319" id="Seg_3513" s="T2318">v:pred 0.3.h:S</ta>
            <ta e="T2321" id="Seg_3514" s="T2320">pro.h:S</ta>
            <ta e="T2322" id="Seg_3515" s="T2321">np:O</ta>
            <ta e="T2323" id="Seg_3516" s="T2322">v:pred</ta>
            <ta e="T2324" id="Seg_3517" s="T2323">v:pred 0.3.h:S</ta>
            <ta e="T2326" id="Seg_3518" s="T2325">v:pred 0.2.h:S</ta>
            <ta e="T2330" id="Seg_3519" s="T2329">pro:S</ta>
            <ta e="T2331" id="Seg_3520" s="T2330">ptcl.neg</ta>
            <ta e="T2332" id="Seg_3521" s="T2331">v:pred</ta>
            <ta e="T2333" id="Seg_3522" s="T2332">v:pred 0.2.h:S</ta>
            <ta e="T2334" id="Seg_3523" s="T2333">pro.h:S</ta>
            <ta e="T2335" id="Seg_3524" s="T2334">v:pred</ta>
            <ta e="T2336" id="Seg_3525" s="T2335">pro.h:S</ta>
            <ta e="T2337" id="Seg_3526" s="T2336">np:O</ta>
            <ta e="T2339" id="Seg_3527" s="T2338">v:pred</ta>
            <ta e="T2340" id="Seg_3528" s="T2339">pro.h:S</ta>
            <ta e="T2341" id="Seg_3529" s="T2340">v:pred</ta>
            <ta e="T2343" id="Seg_3530" s="T2342">np:S</ta>
            <ta e="T2344" id="Seg_3531" s="T2343">v:pred</ta>
            <ta e="T2347" id="Seg_3532" s="T2346">v:pred 0.3.h:S</ta>
            <ta e="T2348" id="Seg_3533" s="T2347">v:pred 0.2.h:S</ta>
            <ta e="T2349" id="Seg_3534" s="T2348">np:O</ta>
            <ta e="T2350" id="Seg_3535" s="T2349">np.h:S</ta>
            <ta e="T2351" id="Seg_3536" s="T2350">v:pred</ta>
            <ta e="T2354" id="Seg_3537" s="T2353">v:pred 0.2.h:S</ta>
            <ta e="T2357" id="Seg_3538" s="T2356">np:S</ta>
            <ta e="T2358" id="Seg_3539" s="T2357">v:pred</ta>
            <ta e="T2359" id="Seg_3540" s="T2358">v:pred 0.2.h:S</ta>
            <ta e="T2360" id="Seg_3541" s="T2359">pro.h:S</ta>
            <ta e="T2361" id="Seg_3542" s="T2360">np:O</ta>
            <ta e="T2362" id="Seg_3543" s="T2361">v:pred</ta>
            <ta e="T2364" id="Seg_3544" s="T2363">np:S</ta>
            <ta e="T2365" id="Seg_3545" s="T2364">v:pred</ta>
            <ta e="T2366" id="Seg_3546" s="T2365">np.h:S</ta>
            <ta e="T2368" id="Seg_3547" s="T2367">v:pred</ta>
            <ta e="T2369" id="Seg_3548" s="T2368">v:pred</ta>
            <ta e="T2370" id="Seg_3549" s="T2369">np:S</ta>
            <ta e="T2371" id="Seg_3550" s="T2370">v:pred</ta>
            <ta e="T2372" id="Seg_3551" s="T2371">np:S</ta>
            <ta e="T2374" id="Seg_3552" s="T2373">v:pred 0.3.h:S</ta>
            <ta e="T2375" id="Seg_3553" s="T2374">np:O</ta>
            <ta e="T2376" id="Seg_3554" s="T2375">v:pred 0.3.h:S</ta>
            <ta e="T2379" id="Seg_3555" s="T2378">v:pred 0.3.h:S</ta>
            <ta e="T2382" id="Seg_3556" s="T2381">v:pred 0.3.h:S</ta>
            <ta e="T2388" id="Seg_3557" s="T2387">v:pred 0.3.h:S</ta>
            <ta e="T2389" id="Seg_3558" s="T2388">pro:O</ta>
            <ta e="T2391" id="Seg_3559" s="T2390">v:pred</ta>
            <ta e="T2393" id="Seg_3560" s="T2392">ptcl:pred</ta>
            <ta e="T2396" id="Seg_3561" s="T2395">pro.h:S</ta>
            <ta e="T2397" id="Seg_3562" s="T2396">v:pred</ta>
            <ta e="T2398" id="Seg_3563" s="T2397">pro.h:O</ta>
            <ta e="T2400" id="Seg_3564" s="T2399">np.h:S</ta>
            <ta e="T2401" id="Seg_3565" s="T2400">v:pred</ta>
            <ta e="T2403" id="Seg_3566" s="T2402">v:pred 0.3.h:S</ta>
            <ta e="T2405" id="Seg_3567" s="T2404">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2156" id="Seg_3568" s="T2155">RUS:cult</ta>
            <ta e="T2157" id="Seg_3569" s="T2156">RUS:cult</ta>
            <ta e="T2161" id="Seg_3570" s="T2160">RUS:gram</ta>
            <ta e="T2167" id="Seg_3571" s="T2166">RUS:gram</ta>
            <ta e="T2168" id="Seg_3572" s="T2167">RUS:cult</ta>
            <ta e="T2169" id="Seg_3573" s="T2168">TURK:disc</ta>
            <ta e="T2175" id="Seg_3574" s="T2174">RUS:cult</ta>
            <ta e="T2176" id="Seg_3575" s="T2175">RUS:disc</ta>
            <ta e="T2186" id="Seg_3576" s="T2185">RUS:gram</ta>
            <ta e="T2187" id="Seg_3577" s="T2186">RUS:cult</ta>
            <ta e="T2188" id="Seg_3578" s="T2187">RUS:cult</ta>
            <ta e="T2193" id="Seg_3579" s="T2192">RUS:gram</ta>
            <ta e="T2206" id="Seg_3580" s="T2205">RUS:cult</ta>
            <ta e="T2218" id="Seg_3581" s="T2217">RUS:gram</ta>
            <ta e="T2225" id="Seg_3582" s="T2224">TURK:core</ta>
            <ta e="T2230" id="Seg_3583" s="T2229">RUS:gram</ta>
            <ta e="T2245" id="Seg_3584" s="T2244">RUS:gram</ta>
            <ta e="T2251" id="Seg_3585" s="T2250">RUS:mod</ta>
            <ta e="T2253" id="Seg_3586" s="T2252">RUS:cult</ta>
            <ta e="T2268" id="Seg_3587" s="T2267">RUS:disc</ta>
            <ta e="T2271" id="Seg_3588" s="T2270">RUS:gram</ta>
            <ta e="T2272" id="Seg_3589" s="T2271">RUS:mod</ta>
            <ta e="T2276" id="Seg_3590" s="T2275">RUS:cult</ta>
            <ta e="T2277" id="Seg_3591" s="T2276">RUS:gram</ta>
            <ta e="T2280" id="Seg_3592" s="T2279">RUS:cult</ta>
            <ta e="T2282" id="Seg_3593" s="T2281">RUS:cult</ta>
            <ta e="T2284" id="Seg_3594" s="T2283">RUS:core</ta>
            <ta e="T2290" id="Seg_3595" s="T2289">RUS:gram</ta>
            <ta e="T2296" id="Seg_3596" s="T2295">RUS:cult</ta>
            <ta e="T2298" id="Seg_3597" s="T2297">RUS:gram</ta>
            <ta e="T2300" id="Seg_3598" s="T2299">RUS:gram</ta>
            <ta e="T2309" id="Seg_3599" s="T2308">RUS:cult</ta>
            <ta e="T2313" id="Seg_3600" s="T2312">RUS:gram</ta>
            <ta e="T2320" id="Seg_3601" s="T2319">RUS:gram</ta>
            <ta e="T2325" id="Seg_3602" s="T2324">RUS:disc</ta>
            <ta e="T2328" id="Seg_3603" s="T2327">RUS:gram</ta>
            <ta e="T2330" id="Seg_3604" s="T2329">TURK:gram(INDEF)</ta>
            <ta e="T2343" id="Seg_3605" s="T2342">TURK:disc</ta>
            <ta e="T2352" id="Seg_3606" s="T2351">RUS:disc</ta>
            <ta e="T2355" id="Seg_3607" s="T2354">RUS:gram</ta>
            <ta e="T2383" id="Seg_3608" s="T2382">TURK:core</ta>
            <ta e="T2386" id="Seg_3609" s="T2385">RUS:cult</ta>
            <ta e="T2393" id="Seg_3610" s="T2392">RUS:gram</ta>
            <ta e="T2399" id="Seg_3611" s="T2398">RUS:core</ta>
            <ta e="T2402" id="Seg_3612" s="T2401">RUS:gram</ta>
            <ta e="T2404" id="Seg_3613" s="T2403">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T2156" id="Seg_3614" s="T2155">dir:infl</ta>
            <ta e="T2157" id="Seg_3615" s="T2156">dir:bare</ta>
            <ta e="T2161" id="Seg_3616" s="T2160">dir:bare</ta>
            <ta e="T2167" id="Seg_3617" s="T2166">dir:bare</ta>
            <ta e="T2168" id="Seg_3618" s="T2167">dir:infl</ta>
            <ta e="T2175" id="Seg_3619" s="T2174">dir:bare</ta>
            <ta e="T2176" id="Seg_3620" s="T2175">dir:bare</ta>
            <ta e="T2186" id="Seg_3621" s="T2185">dir:bare</ta>
            <ta e="T2188" id="Seg_3622" s="T2187">dir:infl</ta>
            <ta e="T2193" id="Seg_3623" s="T2192">dir:bare</ta>
            <ta e="T2206" id="Seg_3624" s="T2205">dir:infl</ta>
            <ta e="T2218" id="Seg_3625" s="T2217">dir:bare</ta>
            <ta e="T2225" id="Seg_3626" s="T2224">dir:bare</ta>
            <ta e="T2230" id="Seg_3627" s="T2229">dir:bare</ta>
            <ta e="T2245" id="Seg_3628" s="T2244">dir:bare</ta>
            <ta e="T2251" id="Seg_3629" s="T2250">dir:bare</ta>
            <ta e="T2253" id="Seg_3630" s="T2252">dir:bare</ta>
            <ta e="T2268" id="Seg_3631" s="T2267">dir:bare</ta>
            <ta e="T2271" id="Seg_3632" s="T2270">dir:bare</ta>
            <ta e="T2272" id="Seg_3633" s="T2271">dir:bare</ta>
            <ta e="T2276" id="Seg_3634" s="T2275">parad:infl</ta>
            <ta e="T2277" id="Seg_3635" s="T2276">dir:bare</ta>
            <ta e="T2280" id="Seg_3636" s="T2279">parad:bare</ta>
            <ta e="T2282" id="Seg_3637" s="T2281">dir:infl</ta>
            <ta e="T2284" id="Seg_3638" s="T2283">dir:infl</ta>
            <ta e="T2290" id="Seg_3639" s="T2289">dir:bare</ta>
            <ta e="T2296" id="Seg_3640" s="T2295">parad:infl</ta>
            <ta e="T2298" id="Seg_3641" s="T2297">dir:bare</ta>
            <ta e="T2300" id="Seg_3642" s="T2299">dir:bare</ta>
            <ta e="T2309" id="Seg_3643" s="T2308">dir:infl</ta>
            <ta e="T2313" id="Seg_3644" s="T2312">dir:bare</ta>
            <ta e="T2320" id="Seg_3645" s="T2319">dir:bare</ta>
            <ta e="T2325" id="Seg_3646" s="T2324">dir:bare</ta>
            <ta e="T2328" id="Seg_3647" s="T2327">dir:bare</ta>
            <ta e="T2352" id="Seg_3648" s="T2351">dir:bare</ta>
            <ta e="T2355" id="Seg_3649" s="T2354">dir:bare</ta>
            <ta e="T2383" id="Seg_3650" s="T2382">dir:bare</ta>
            <ta e="T2386" id="Seg_3651" s="T2385">dir:infl</ta>
            <ta e="T2393" id="Seg_3652" s="T2392">dir:bare</ta>
            <ta e="T2399" id="Seg_3653" s="T2398">dir:infl</ta>
            <ta e="T2402" id="Seg_3654" s="T2401">dir:bare</ta>
            <ta e="T2404" id="Seg_3655" s="T2403">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2148" id="Seg_3656" s="T2145">Жили жена и муж.</ta>
            <ta e="T2152" id="Seg_3657" s="T2148">У неё/у него было три дочери.</ta>
            <ta e="T2160" id="Seg_3658" s="T2152">Он пошёл в амбар, взял крупу в мешке, пришёл.</ta>
            <ta e="T2164" id="Seg_3659" s="T2160">А в мешке была дырка.</ta>
            <ta e="T2170" id="Seg_3660" s="T2164">Он шёл, шёл, а крупа вся высыпалась.</ta>
            <ta e="T2175" id="Seg_3661" s="T2170">Жена говорит: «Где твоя крупа?»</ta>
            <ta e="T2179" id="Seg_3662" s="T2175">«Да просыпал», [сказал он] и ушёл.</ta>
            <ta e="T2182" id="Seg_3663" s="T2179">Говорит: «Солнце, помоги [мне]!»</ta>
            <ta e="T2185" id="Seg_3664" s="T2182">Месяцу говорит: «Помоги!»</ta>
            <ta e="T2190" id="Seg_3665" s="T2185">И ворону говорит: «Помоги!»</ta>
            <ta e="T2192" id="Seg_3666" s="T2190">Они пришли.</ta>
            <ta e="T2196" id="Seg_3667" s="T2192">Помогли ему.</ta>
            <ta e="T2201" id="Seg_3668" s="T2196">«Я дочерей за вас выдам замуж».</ta>
            <ta e="T2203" id="Seg_3669" s="T2201">Потом пришёл.</ta>
            <ta e="T2207" id="Seg_3670" s="T2203">Принёс крупу жене.</ta>
            <ta e="T2212" id="Seg_3671" s="T2207">Потом говорит одной, старшей дочери:</ta>
            <ta e="T2215" id="Seg_3672" s="T2212">«Надевай красивую одежду!»</ta>
            <ta e="T2220" id="Seg_3673" s="T2215">Она надела и вышла наружу.</ta>
            <ta e="T2223" id="Seg_3674" s="T2220">Солнце её забрало.</ta>
            <ta e="T2226" id="Seg_3675" s="T2223">Потом с другой дочерью.</ta>
            <ta e="T2232" id="Seg_3676" s="T2226">«Надень красивую одежду и выйди наружу!»</ta>
            <ta e="T2234" id="Seg_3677" s="T2232">Она пошла.</ta>
            <ta e="T2237" id="Seg_3678" s="T2234">Месяц её забрал.</ta>
            <ta e="T2241" id="Seg_3679" s="T2237">Потом третьей дочери говорит:</ta>
            <ta e="T2247" id="Seg_3680" s="T2241">«Надень красивую одежду и выйди наружу!»</ta>
            <ta e="T2249" id="Seg_3681" s="T2247">Она пошла.</ta>
            <ta e="T2253" id="Seg_3682" s="T2249">Её тоже ворон забрал.</ta>
            <ta e="T2263" id="Seg_3683" s="T2253">Потом этот человек говорит: «Пойду к дочери в гости».</ta>
            <ta e="T2264" id="Seg_3684" s="T2263">Пришёл.</ta>
            <ta e="T2267" id="Seg_3685" s="T2264">«Чем тебя угостить?»</ta>
            <ta e="T2273" id="Seg_3686" s="T2267">«Да мне есть не хочется».</ta>
            <ta e="T2282" id="Seg_3687" s="T2273">Тогда она оладьи… дочь оладьи положила на сковороду.</ta>
            <ta e="T2286" id="Seg_3688" s="T2282">Поставила на солнце, они испеклись.</ta>
            <ta e="T2292" id="Seg_3689" s="T2286">Этот человек поел и пришёл домой.</ta>
            <ta e="T2294" id="Seg_3690" s="T2292">Жене говорит:</ta>
            <ta e="T2296" id="Seg_3691" s="T2294">«Испеки оладьи!»</ta>
            <ta e="T2299" id="Seg_3692" s="T2296">Она стала печь.</ta>
            <ta e="T2306" id="Seg_3693" s="T2299">И поставила (на него?), они стояли, стояли, не испеклись.</ta>
            <ta e="T2310" id="Seg_3694" s="T2306">Тогда поставила в печь.</ta>
            <ta e="T2312" id="Seg_3695" s="T2310">Там испекла.</ta>
            <ta e="T2315" id="Seg_3696" s="T2312">И муж поел.</ta>
            <ta e="T2318" id="Seg_3697" s="T2315">Потом он пошёл к месяцу.</ta>
            <ta e="T2319" id="Seg_3698" s="T2318">Пришёл.</ta>
            <ta e="T2323" id="Seg_3699" s="T2319">А он [месяц] баню затопил.</ta>
            <ta e="T2324" id="Seg_3700" s="T2323">Говорит:</ta>
            <ta e="T2327" id="Seg_3701" s="T2324">«Ну, иди в баню».</ta>
            <ta e="T2332" id="Seg_3702" s="T2327">«А там ничего не видно».</ta>
            <ta e="T2333" id="Seg_3703" s="T2332">«Иди!»</ta>
            <ta e="T2335" id="Seg_3704" s="T2333">Он пошёл.</ta>
            <ta e="T2344" id="Seg_3705" s="T2335">Он [месяц] вставил туда свой палец, он стал мыться, всё там видно.</ta>
            <ta e="T2347" id="Seg_3706" s="T2344">Потом пришёл домой.</ta>
            <ta e="T2349" id="Seg_3707" s="T2347">[Говорит жене:] «Затопи баню!»</ta>
            <ta e="T2351" id="Seg_3708" s="T2349">Жена затопила.</ta>
            <ta e="T2354" id="Seg_3709" s="T2351">«Ну, мойся!»</ta>
            <ta e="T2358" id="Seg_3710" s="T2354">«Да там света нет».</ta>
            <ta e="T2359" id="Seg_3711" s="T2358">«Иди!»</ta>
            <ta e="T2362" id="Seg_3712" s="T2359">Он сделал дырку.</ta>
            <ta e="T2365" id="Seg_3713" s="T2362">Потом засунул туда палец.</ta>
            <ta e="T2368" id="Seg_3714" s="T2365">Жена всё кричит.</ta>
            <ta e="T2372" id="Seg_3715" s="T2368">«Света нет, света нет!»</ta>
            <ta e="T2374" id="Seg_3716" s="T2372">Потом помылись.</ta>
            <ta e="T2376" id="Seg_3717" s="T2374">Зажгли огонь.</ta>
            <ta e="T2382" id="Seg_3718" s="T2376">Потом утром он проснулся, пошёл.</ta>
            <ta e="T2386" id="Seg_3719" s="T2382">К другой дочери, к ворону.</ta>
            <ta e="T2391" id="Seg_3720" s="T2386">Там [ворон] говорит: «Что тебе дать?»</ta>
            <ta e="T2394" id="Seg_3721" s="T2391">«Ну, давай спать!»</ta>
            <ta e="T2399" id="Seg_3722" s="T2394">Он взял его под крыло.</ta>
            <ta e="T2401" id="Seg_3723" s="T2399">Оба заснули.</ta>
            <ta e="T2403" id="Seg_3724" s="T2401">И упали.</ta>
            <ta e="T2405" id="Seg_3725" s="T2403">И умерли.</ta>
            <ta e="T2406" id="Seg_3726" s="T2405">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2148" id="Seg_3727" s="T2145">There lived a woman and a man.</ta>
            <ta e="T2152" id="Seg_3728" s="T2148">(S)he had three daughters.</ta>
            <ta e="T2160" id="Seg_3729" s="T2152">He went to the granary, took groats in a sack, came.</ta>
            <ta e="T2164" id="Seg_3730" s="T2160">But there was a hole in the sack.</ta>
            <ta e="T2170" id="Seg_3731" s="T2164">While he was coming, he spilled all his groats.</ta>
            <ta e="T2175" id="Seg_3732" s="T2170">His wife says: "Where is your groats?"</ta>
            <ta e="T2179" id="Seg_3733" s="T2175">"Well, I spilled them", then he went.</ta>
            <ta e="T2182" id="Seg_3734" s="T2179">He said: "Sun, help [me]!"</ta>
            <ta e="T2185" id="Seg_3735" s="T2182">To the moon he said: “Help!”</ta>
            <ta e="T2190" id="Seg_3736" s="T2185">And to the raven he said: "Help!"</ta>
            <ta e="T2192" id="Seg_3737" s="T2190">They came.</ta>
            <ta e="T2196" id="Seg_3738" s="T2192">They helped him.</ta>
            <ta e="T2201" id="Seg_3739" s="T2196">"I will give you my daughters to marry."</ta>
            <ta e="T2203" id="Seg_3740" s="T2201">Then he came.</ta>
            <ta e="T2207" id="Seg_3741" s="T2203">Brought the groats to his wife.</ta>
            <ta e="T2212" id="Seg_3742" s="T2207">Then he says to one, to the elder daughter:</ta>
            <ta e="T2215" id="Seg_3743" s="T2212">"Put on beautiful clothes!"</ta>
            <ta e="T2220" id="Seg_3744" s="T2215">She put [them] on and went outside.</ta>
            <ta e="T2223" id="Seg_3745" s="T2220">The sun took her.</ta>
            <ta e="T2226" id="Seg_3746" s="T2223">Then with her other daughter.</ta>
            <ta e="T2232" id="Seg_3747" s="T2226">"Put on beautiful clothes and go outside!"</ta>
            <ta e="T2234" id="Seg_3748" s="T2232">She went.</ta>
            <ta e="T2237" id="Seg_3749" s="T2234">The moon took her.</ta>
            <ta e="T2241" id="Seg_3750" s="T2237">Then he says to the third daughter:</ta>
            <ta e="T2247" id="Seg_3751" s="T2241">"Put on beautiful clothes and go outside!"</ta>
            <ta e="T2249" id="Seg_3752" s="T2247">She went.</ta>
            <ta e="T2253" id="Seg_3753" s="T2249">The raven took her too.</ta>
            <ta e="T2263" id="Seg_3754" s="T2253">Then the man says: "I will go to visit [my] daughter."</ta>
            <ta e="T2264" id="Seg_3755" s="T2263">He came.</ta>
            <ta e="T2267" id="Seg_3756" s="T2264">"What to feed you with?"</ta>
            <ta e="T2273" id="Seg_3757" s="T2267">"Well, I don’t want to eat."</ta>
            <ta e="T2282" id="Seg_3758" s="T2273">Then she [put] pancakes, his daughter put pancakes on a pan.</ta>
            <ta e="T2286" id="Seg_3759" s="T2282">Set it at the sun, [and] they got fried.</ta>
            <ta e="T2292" id="Seg_3760" s="T2286">The man ate and came home.</ta>
            <ta e="T2294" id="Seg_3761" s="T2292">He says to his wife:</ta>
            <ta e="T2296" id="Seg_3762" s="T2294">"Bake pancakes!"</ta>
            <ta e="T2299" id="Seg_3763" s="T2296">She started to cook.</ta>
            <ta e="T2306" id="Seg_3764" s="T2299">And set them on (him?), they were standing, they were standing, they did not cook.</ta>
            <ta e="T2310" id="Seg_3765" s="T2306">Then she put it in the oven.</ta>
            <ta e="T2312" id="Seg_3766" s="T2310">[She] cooked [them] there.</ta>
            <ta e="T2315" id="Seg_3767" s="T2312">And the man ate.</ta>
            <ta e="T2318" id="Seg_3768" s="T2315">Then he goes to the moon.</ta>
            <ta e="T2319" id="Seg_3769" s="T2318">He came.</ta>
            <ta e="T2323" id="Seg_3770" s="T2319">And it [the moon] warmed up the sauna.</ta>
            <ta e="T2324" id="Seg_3771" s="T2323">It says:</ta>
            <ta e="T2327" id="Seg_3772" s="T2324">"Well, go to the sauna."</ta>
            <ta e="T2332" id="Seg_3773" s="T2327">"But one can't see anything there."</ta>
            <ta e="T2333" id="Seg_3774" s="T2332">"Go!"</ta>
            <ta e="T2335" id="Seg_3775" s="T2333">He went.</ta>
            <ta e="T2344" id="Seg_3776" s="T2335">He [the Moon] put his finger in there, he washed, everything is visible there.</ta>
            <ta e="T2347" id="Seg_3777" s="T2344">Then he came home.</ta>
            <ta e="T2349" id="Seg_3778" s="T2347">[He says to his wife:] "Warm up the sauna!"</ta>
            <ta e="T2351" id="Seg_3779" s="T2349">His wife warmed it up.</ta>
            <ta e="T2354" id="Seg_3780" s="T2351">"Well, wash!"</ta>
            <ta e="T2358" id="Seg_3781" s="T2354">"But there is no light."</ta>
            <ta e="T2359" id="Seg_3782" s="T2358">"Go!"</ta>
            <ta e="T2362" id="Seg_3783" s="T2359">He made a hole.</ta>
            <ta e="T2365" id="Seg_3784" s="T2362">There he put his finger there.</ta>
            <ta e="T2368" id="Seg_3785" s="T2365">His wife keeps shouting.</ta>
            <ta e="T2372" id="Seg_3786" s="T2368">"No light, no light!"</ta>
            <ta e="T2374" id="Seg_3787" s="T2372">Then they washed.</ta>
            <ta e="T2376" id="Seg_3788" s="T2374">They lit fire.</ta>
            <ta e="T2382" id="Seg_3789" s="T2376">Then in the morning, he is getting up, he went.</ta>
            <ta e="T2386" id="Seg_3790" s="T2382">To his other daughter, to the raven.</ta>
            <ta e="T2391" id="Seg_3791" s="T2386">There [the raven] says: "What should I give to you?"</ta>
            <ta e="T2394" id="Seg_3792" s="T2391">"Well, let's sleep!"</ta>
            <ta e="T2399" id="Seg_3793" s="T2394">It take him under the wing.</ta>
            <ta e="T2401" id="Seg_3794" s="T2399">Two of them fell asleep.</ta>
            <ta e="T2403" id="Seg_3795" s="T2401">And fell down.</ta>
            <ta e="T2405" id="Seg_3796" s="T2403">And died.</ta>
            <ta e="T2406" id="Seg_3797" s="T2405">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2148" id="Seg_3798" s="T2145">Es lebte eine Frau und ein Mann.</ta>
            <ta e="T2152" id="Seg_3799" s="T2148">Sie/Er hatte drei Töchter.</ta>
            <ta e="T2160" id="Seg_3800" s="T2152">Er ging zum Kornspeicher, nahm Graupen in einem Sack, kam.</ta>
            <ta e="T2164" id="Seg_3801" s="T2160">Aber es war ein Loch im Sack.</ta>
            <ta e="T2170" id="Seg_3802" s="T2164">Er kam, er kam, aber er verschüttete seine Graupen.</ta>
            <ta e="T2175" id="Seg_3803" s="T2170">Seine Frau sagt: „Wo sind deine Graupen?“</ta>
            <ta e="T2179" id="Seg_3804" s="T2175">„Ich habe sie verschüttet“, dann ging er.</ta>
            <ta e="T2182" id="Seg_3805" s="T2179">Er sagte: „Sonne, hilf [mir]!“</ta>
            <ta e="T2185" id="Seg_3806" s="T2182">Zum Mond sagte er: „Hilf!“</ta>
            <ta e="T2190" id="Seg_3807" s="T2185">Und zum Raben sagte er: „Hilf!“</ta>
            <ta e="T2192" id="Seg_3808" s="T2190">Sie kamen.</ta>
            <ta e="T2196" id="Seg_3809" s="T2192">Sie halfen ihm.</ta>
            <ta e="T2201" id="Seg_3810" s="T2196">„Ich werde euch meine Töchter geben, zum Heiraten.“</ta>
            <ta e="T2203" id="Seg_3811" s="T2201">Dann kam er.</ta>
            <ta e="T2207" id="Seg_3812" s="T2203">Brachte die Graupen seiner Frau.</ta>
            <ta e="T2212" id="Seg_3813" s="T2207">Dann sagt er zu einer, zu der älteren Tochter:</ta>
            <ta e="T2215" id="Seg_3814" s="T2212">„Zieh schöne Kleider an!“</ta>
            <ta e="T2220" id="Seg_3815" s="T2215">Sie zog [sie] an und ging hinaus.</ta>
            <ta e="T2223" id="Seg_3816" s="T2220">Di Sonne nahm sie.</ta>
            <ta e="T2226" id="Seg_3817" s="T2223">Dann mit ihrer anderen Tochter.</ta>
            <ta e="T2232" id="Seg_3818" s="T2226">„Zieh schöne Kleider an und geh hinaus!“</ta>
            <ta e="T2234" id="Seg_3819" s="T2232">Sie ging.</ta>
            <ta e="T2237" id="Seg_3820" s="T2234">Der Mond nahm sie.</ta>
            <ta e="T2241" id="Seg_3821" s="T2237">Dann sagt er zur dritten Tochter:</ta>
            <ta e="T2247" id="Seg_3822" s="T2241">„Zieh schöne Kleider an und geh hinaus!“</ta>
            <ta e="T2249" id="Seg_3823" s="T2247">Sie ging.</ta>
            <ta e="T2253" id="Seg_3824" s="T2249">Der Rabe nahm sie auch.</ta>
            <ta e="T2263" id="Seg_3825" s="T2253">Dann sagt der Mann: „Ich werde gehen [meine] Tochter besuchen.“</ta>
            <ta e="T2264" id="Seg_3826" s="T2263">Er kam.</ta>
            <ta e="T2267" id="Seg_3827" s="T2264">„Womit dich füttern?“</ta>
            <ta e="T2273" id="Seg_3828" s="T2267">„Ich will nicht essen.“</ta>
            <ta e="T2282" id="Seg_3829" s="T2273">Dann [setzt] sie Pfannkuchen, seine Tochter setzt Pfannkuchen in eine Pfanne.</ta>
            <ta e="T2286" id="Seg_3830" s="T2282">Stell sie zur Sonne, [und] sie worden gebraten.</ta>
            <ta e="T2292" id="Seg_3831" s="T2286">Der Mann aß und kam nach Hause.</ta>
            <ta e="T2294" id="Seg_3832" s="T2292">Er sagt seiner Frau:</ta>
            <ta e="T2296" id="Seg_3833" s="T2294">„Back Pfannkuchen!“</ta>
            <ta e="T2299" id="Seg_3834" s="T2296">Sie fing an zu kochen.</ta>
            <ta e="T2306" id="Seg_3835" s="T2299">Und setzt sie auf (ihm?), sie standen, sie standen, sie kochten nicht.</ta>
            <ta e="T2310" id="Seg_3836" s="T2306">Dann stellte sie sie in den Ofen.</ta>
            <ta e="T2312" id="Seg_3837" s="T2310">[Sie] kochte [sie] da.</ta>
            <ta e="T2315" id="Seg_3838" s="T2312">Und der Man aß.</ta>
            <ta e="T2318" id="Seg_3839" s="T2315">Dann ging er zum Mond.</ta>
            <ta e="T2319" id="Seg_3840" s="T2318">Er kam.</ta>
            <ta e="T2323" id="Seg_3841" s="T2319">Und er [der Mond] erhitzte die Sauna.</ta>
            <ta e="T2324" id="Seg_3842" s="T2323">Er sagt:</ta>
            <ta e="T2327" id="Seg_3843" s="T2324">„Also, geh in die Sauna.“</ta>
            <ta e="T2332" id="Seg_3844" s="T2327">„Aber man kann nichts da sehen.“</ta>
            <ta e="T2333" id="Seg_3845" s="T2332">„Geh!“</ta>
            <ta e="T2335" id="Seg_3846" s="T2333">Er ging.</ta>
            <ta e="T2344" id="Seg_3847" s="T2335">Er [der Mond] steckte den Finger dort hinein, er wusch, alles ist dort sichtbar.</ta>
            <ta e="T2347" id="Seg_3848" s="T2344">Dann kam er nach Hause.</ta>
            <ta e="T2349" id="Seg_3849" s="T2347">[Er sagte zu seiner Frau:] „Erhitze die Sauna!“</ta>
            <ta e="T2351" id="Seg_3850" s="T2349">Seine Frau erhitzte sie.</ta>
            <ta e="T2354" id="Seg_3851" s="T2351">„Also, wasch dich!“</ta>
            <ta e="T2358" id="Seg_3852" s="T2354">„Aber es ist kein Licht.“</ta>
            <ta e="T2359" id="Seg_3853" s="T2358">„Geh!“</ta>
            <ta e="T2362" id="Seg_3854" s="T2359">Er machte ein Loch.</ta>
            <ta e="T2365" id="Seg_3855" s="T2362">Dort steckte er den Finger.</ta>
            <ta e="T2368" id="Seg_3856" s="T2365">Seine Frau ruft weiter.</ta>
            <ta e="T2372" id="Seg_3857" s="T2368">„Kein Licht, kein Licht!“</ta>
            <ta e="T2374" id="Seg_3858" s="T2372">Dann wuschen sie sich.</ta>
            <ta e="T2376" id="Seg_3859" s="T2374">Sie zündeten Feuer.</ta>
            <ta e="T2382" id="Seg_3860" s="T2376">Dann am Morgen, steht er auf, er ging.</ta>
            <ta e="T2386" id="Seg_3861" s="T2382">Zu seiner anderen Tochter, zu dem Raben.</ta>
            <ta e="T2391" id="Seg_3862" s="T2386">Dort sagt [der Rabe]: „Was sollte ich dir geben?“</ta>
            <ta e="T2394" id="Seg_3863" s="T2391">„Also, schlafen wir!“</ta>
            <ta e="T2399" id="Seg_3864" s="T2394">Er nahm ihn unter die Schwinge.</ta>
            <ta e="T2401" id="Seg_3865" s="T2399">Zwei von ihnen schliefen ein.</ta>
            <ta e="T2403" id="Seg_3866" s="T2401">Und fielen hinunter.</ta>
            <ta e="T2405" id="Seg_3867" s="T2403">Und starben.</ta>
            <ta e="T2406" id="Seg_3868" s="T2405">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2148" id="Seg_3869" s="T2145">[GVY:] The Russian tale "Sun, Moon and a Raven", see e.g. http://hyaenidae.narod.ru/story2/082.html</ta>
            <ta e="T2160" id="Seg_3870" s="T2152">[KlT:] Anbar, ambar ’store house’, Castrén’s manuscript.</ta>
            <ta e="T2170" id="Seg_3871" s="T2164">Kamə- ’to pour’.</ta>
            <ta e="T2182" id="Seg_3872" s="T2179">[GVY:] here pronounced kabazəraʔ</ta>
            <ta e="T2286" id="Seg_3873" s="T2282">[GVY:] NB dĭ singular, referring to the pancakes</ta>
            <ta e="T2306" id="Seg_3874" s="T2299">[GVY:] ondə from Ru. он 'he'?</ta>
            <ta e="T2374" id="Seg_3875" s="T2372">[GVY:] bozaj-?</ta>
            <ta e="T2399" id="Seg_3876" s="T2394">[GVY:] krɨlondə jil?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2145" />
            <conversion-tli id="T2146" />
            <conversion-tli id="T2147" />
            <conversion-tli id="T2148" />
            <conversion-tli id="T2149" />
            <conversion-tli id="T2150" />
            <conversion-tli id="T2151" />
            <conversion-tli id="T2152" />
            <conversion-tli id="T2153" />
            <conversion-tli id="T2154" />
            <conversion-tli id="T2155" />
            <conversion-tli id="T2156" />
            <conversion-tli id="T2157" />
            <conversion-tli id="T2158" />
            <conversion-tli id="T2159" />
            <conversion-tli id="T2160" />
            <conversion-tli id="T2161" />
            <conversion-tli id="T2162" />
            <conversion-tli id="T2163" />
            <conversion-tli id="T2164" />
            <conversion-tli id="T2165" />
            <conversion-tli id="T2166" />
            <conversion-tli id="T2167" />
            <conversion-tli id="T2168" />
            <conversion-tli id="T2169" />
            <conversion-tli id="T2170" />
            <conversion-tli id="T2171" />
            <conversion-tli id="T2172" />
            <conversion-tli id="T2173" />
            <conversion-tli id="T2174" />
            <conversion-tli id="T2175" />
            <conversion-tli id="T2176" />
            <conversion-tli id="T2177" />
            <conversion-tli id="T2178" />
            <conversion-tli id="T2179" />
            <conversion-tli id="T2180" />
            <conversion-tli id="T2181" />
            <conversion-tli id="T2182" />
            <conversion-tli id="T2183" />
            <conversion-tli id="T2184" />
            <conversion-tli id="T2185" />
            <conversion-tli id="T2186" />
            <conversion-tli id="T2187" />
            <conversion-tli id="T2188" />
            <conversion-tli id="T2189" />
            <conversion-tli id="T2190" />
            <conversion-tli id="T2191" />
            <conversion-tli id="T2192" />
            <conversion-tli id="T2193" />
            <conversion-tli id="T2194" />
            <conversion-tli id="T2195" />
            <conversion-tli id="T2196" />
            <conversion-tli id="T2197" />
            <conversion-tli id="T2198" />
            <conversion-tli id="T2199" />
            <conversion-tli id="T2200" />
            <conversion-tli id="T2201" />
            <conversion-tli id="T2202" />
            <conversion-tli id="T2203" />
            <conversion-tli id="T2204" />
            <conversion-tli id="T2205" />
            <conversion-tli id="T2206" />
            <conversion-tli id="T2207" />
            <conversion-tli id="T2208" />
            <conversion-tli id="T2209" />
            <conversion-tli id="T2210" />
            <conversion-tli id="T2211" />
            <conversion-tli id="T2212" />
            <conversion-tli id="T2213" />
            <conversion-tli id="T2214" />
            <conversion-tli id="T2215" />
            <conversion-tli id="T2216" />
            <conversion-tli id="T2217" />
            <conversion-tli id="T2218" />
            <conversion-tli id="T2219" />
            <conversion-tli id="T2220" />
            <conversion-tli id="T2221" />
            <conversion-tli id="T2222" />
            <conversion-tli id="T2223" />
            <conversion-tli id="T2224" />
            <conversion-tli id="T2225" />
            <conversion-tli id="T2226" />
            <conversion-tli id="T2227" />
            <conversion-tli id="T2228" />
            <conversion-tli id="T2229" />
            <conversion-tli id="T2230" />
            <conversion-tli id="T2231" />
            <conversion-tli id="T2232" />
            <conversion-tli id="T2233" />
            <conversion-tli id="T2234" />
            <conversion-tli id="T2235" />
            <conversion-tli id="T2236" />
            <conversion-tli id="T2237" />
            <conversion-tli id="T2238" />
            <conversion-tli id="T2239" />
            <conversion-tli id="T2240" />
            <conversion-tli id="T2241" />
            <conversion-tli id="T2242" />
            <conversion-tli id="T2243" />
            <conversion-tli id="T2244" />
            <conversion-tli id="T2245" />
            <conversion-tli id="T2246" />
            <conversion-tli id="T2247" />
            <conversion-tli id="T2248" />
            <conversion-tli id="T2249" />
            <conversion-tli id="T2250" />
            <conversion-tli id="T2251" />
            <conversion-tli id="T2252" />
            <conversion-tli id="T2253" />
            <conversion-tli id="T2254" />
            <conversion-tli id="T2255" />
            <conversion-tli id="T2256" />
            <conversion-tli id="T2257" />
            <conversion-tli id="T2258" />
            <conversion-tli id="T2259" />
            <conversion-tli id="T2260" />
            <conversion-tli id="T2261" />
            <conversion-tli id="T2262" />
            <conversion-tli id="T2263" />
            <conversion-tli id="T2264" />
            <conversion-tli id="T2265" />
            <conversion-tli id="T2266" />
            <conversion-tli id="T2267" />
            <conversion-tli id="T2268" />
            <conversion-tli id="T2269" />
            <conversion-tli id="T2270" />
            <conversion-tli id="T2271" />
            <conversion-tli id="T2272" />
            <conversion-tli id="T2273" />
            <conversion-tli id="T2274" />
            <conversion-tli id="T2275" />
            <conversion-tli id="T2276" />
            <conversion-tli id="T2277" />
            <conversion-tli id="T2278" />
            <conversion-tli id="T2279" />
            <conversion-tli id="T2280" />
            <conversion-tli id="T2281" />
            <conversion-tli id="T2282" />
            <conversion-tli id="T2283" />
            <conversion-tli id="T2284" />
            <conversion-tli id="T2285" />
            <conversion-tli id="T2286" />
            <conversion-tli id="T2287" />
            <conversion-tli id="T2288" />
            <conversion-tli id="T2289" />
            <conversion-tli id="T2290" />
            <conversion-tli id="T2291" />
            <conversion-tli id="T2292" />
            <conversion-tli id="T2293" />
            <conversion-tli id="T2294" />
            <conversion-tli id="T2295" />
            <conversion-tli id="T2296" />
            <conversion-tli id="T2297" />
            <conversion-tli id="T2298" />
            <conversion-tli id="T2299" />
            <conversion-tli id="T2300" />
            <conversion-tli id="T2301" />
            <conversion-tli id="T2302" />
            <conversion-tli id="T2303" />
            <conversion-tli id="T2304" />
            <conversion-tli id="T2305" />
            <conversion-tli id="T2306" />
            <conversion-tli id="T2307" />
            <conversion-tli id="T2308" />
            <conversion-tli id="T2309" />
            <conversion-tli id="T2310" />
            <conversion-tli id="T2311" />
            <conversion-tli id="T2312" />
            <conversion-tli id="T2313" />
            <conversion-tli id="T2314" />
            <conversion-tli id="T2315" />
            <conversion-tli id="T2316" />
            <conversion-tli id="T2317" />
            <conversion-tli id="T2318" />
            <conversion-tli id="T2319" />
            <conversion-tli id="T2320" />
            <conversion-tli id="T2321" />
            <conversion-tli id="T2322" />
            <conversion-tli id="T2323" />
            <conversion-tli id="T2324" />
            <conversion-tli id="T2325" />
            <conversion-tli id="T2326" />
            <conversion-tli id="T2327" />
            <conversion-tli id="T2328" />
            <conversion-tli id="T2329" />
            <conversion-tli id="T2330" />
            <conversion-tli id="T2331" />
            <conversion-tli id="T2332" />
            <conversion-tli id="T2333" />
            <conversion-tli id="T2334" />
            <conversion-tli id="T2335" />
            <conversion-tli id="T2336" />
            <conversion-tli id="T2337" />
            <conversion-tli id="T2338" />
            <conversion-tli id="T2339" />
            <conversion-tli id="T2340" />
            <conversion-tli id="T2341" />
            <conversion-tli id="T2342" />
            <conversion-tli id="T2343" />
            <conversion-tli id="T2344" />
            <conversion-tli id="T2345" />
            <conversion-tli id="T2346" />
            <conversion-tli id="T2347" />
            <conversion-tli id="T2348" />
            <conversion-tli id="T2349" />
            <conversion-tli id="T2350" />
            <conversion-tli id="T2351" />
            <conversion-tli id="T2352" />
            <conversion-tli id="T2353" />
            <conversion-tli id="T2354" />
            <conversion-tli id="T2355" />
            <conversion-tli id="T2356" />
            <conversion-tli id="T2357" />
            <conversion-tli id="T2358" />
            <conversion-tli id="T2359" />
            <conversion-tli id="T2360" />
            <conversion-tli id="T2361" />
            <conversion-tli id="T2362" />
            <conversion-tli id="T2363" />
            <conversion-tli id="T2364" />
            <conversion-tli id="T2365" />
            <conversion-tli id="T2366" />
            <conversion-tli id="T2367" />
            <conversion-tli id="T2368" />
            <conversion-tli id="T2369" />
            <conversion-tli id="T2370" />
            <conversion-tli id="T2371" />
            <conversion-tli id="T2372" />
            <conversion-tli id="T2373" />
            <conversion-tli id="T2374" />
            <conversion-tli id="T2375" />
            <conversion-tli id="T2376" />
            <conversion-tli id="T2377" />
            <conversion-tli id="T2378" />
            <conversion-tli id="T2379" />
            <conversion-tli id="T2380" />
            <conversion-tli id="T2381" />
            <conversion-tli id="T2382" />
            <conversion-tli id="T2383" />
            <conversion-tli id="T2384" />
            <conversion-tli id="T2385" />
            <conversion-tli id="T2386" />
            <conversion-tli id="T2387" />
            <conversion-tli id="T2388" />
            <conversion-tli id="T2389" />
            <conversion-tli id="T2390" />
            <conversion-tli id="T2391" />
            <conversion-tli id="T2392" />
            <conversion-tli id="T2393" />
            <conversion-tli id="T2394" />
            <conversion-tli id="T2395" />
            <conversion-tli id="T2396" />
            <conversion-tli id="T2397" />
            <conversion-tli id="T2398" />
            <conversion-tli id="T2399" />
            <conversion-tli id="T2400" />
            <conversion-tli id="T2401" />
            <conversion-tli id="T2402" />
            <conversion-tli id="T2403" />
            <conversion-tli id="T2404" />
            <conversion-tli id="T2405" />
            <conversion-tli id="T2406" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
