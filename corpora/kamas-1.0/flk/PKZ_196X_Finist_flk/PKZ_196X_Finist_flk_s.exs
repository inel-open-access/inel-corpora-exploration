<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID50746378-6A22-6CC5-6748-D7F92F3495E1">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Finist_flk.wav" />
         <referenced-file url="PKZ_196X_Finist_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Finist_flk\PKZ_196X_Finist_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">725</ud-information>
            <ud-information attribute-name="# HIAT:w">456</ud-information>
            <ud-information attribute-name="# e">455</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">97</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.625" type="appl" />
         <tli id="T2" time="1.25" type="appl" />
         <tli id="T3" time="1.875" type="appl" />
         <tli id="T4" time="2.356" type="appl" />
         <tli id="T5" time="2.838" type="appl" />
         <tli id="T6" time="3.319" type="appl" />
         <tli id="T7" time="3.8" type="appl" />
         <tli id="T8" time="4.768" type="appl" />
         <tli id="T9" time="5.737" type="appl" />
         <tli id="T10" time="6.705" type="appl" />
         <tli id="T11" time="7.674" type="appl" />
         <tli id="T12" time="8.642" type="appl" />
         <tli id="T13" time="9.254" type="appl" />
         <tli id="T14" time="9.855" type="appl" />
         <tli id="T15" time="10.693294970223148" />
         <tli id="T16" time="11.658" type="appl" />
         <tli id="T17" time="12.493" type="appl" />
         <tli id="T18" time="13.328" type="appl" />
         <tli id="T19" time="14.163" type="appl" />
         <tli id="T20" time="14.998" type="appl" />
         <tli id="T21" time="15.833" type="appl" />
         <tli id="T22" time="16.538" type="appl" />
         <tli id="T23" time="17.242" type="appl" />
         <tli id="T24" time="17.947" type="appl" />
         <tli id="T25" time="18.652" type="appl" />
         <tli id="T26" time="19.357" type="appl" />
         <tli id="T27" time="20.061" type="appl" />
         <tli id="T28" time="20.766" type="appl" />
         <tli id="T29" time="21.471" type="appl" />
         <tli id="T30" time="22.176" type="appl" />
         <tli id="T31" time="22.88" type="appl" />
         <tli id="T32" time="24.439912319724474" />
         <tli id="T33" time="25.056" type="appl" />
         <tli id="T34" time="25.533" type="appl" />
         <tli id="T35" time="26.011" type="appl" />
         <tli id="T36" time="26.488" type="appl" />
         <tli id="T37" time="26.966" type="appl" />
         <tli id="T38" time="27.443" type="appl" />
         <tli id="T39" time="27.921" type="appl" />
         <tli id="T40" time="29.041" type="appl" />
         <tli id="T41" time="30.144" type="appl" />
         <tli id="T42" time="31.247" type="appl" />
         <tli id="T43" time="32.35" type="appl" />
         <tli id="T44" time="37.65319824926455" />
         <tli id="T45" time="38.579" type="appl" />
         <tli id="T46" time="39.339" type="appl" />
         <tli id="T47" time="40.099" type="appl" />
         <tli id="T48" time="40.859" type="appl" />
         <tli id="T49" time="41.619" type="appl" />
         <tli id="T50" time="42.379" type="appl" />
         <tli id="T51" time="43.011" type="appl" />
         <tli id="T52" time="43.638" type="appl" />
         <tli id="T53" time="44.264" type="appl" />
         <tli id="T54" time="44.891" type="appl" />
         <tli id="T55" time="45.517" type="appl" />
         <tli id="T56" time="46.143" type="appl" />
         <tli id="T57" time="46.77" type="appl" />
         <tli id="T58" time="47.396" type="appl" />
         <tli id="T59" time="48.022" type="appl" />
         <tli id="T60" time="48.649" type="appl" />
         <tli id="T61" time="49.275" type="appl" />
         <tli id="T62" time="49.902" type="appl" />
         <tli id="T63" time="50.83981760780655" />
         <tli id="T64" time="51.801" type="appl" />
         <tli id="T65" time="52.668" type="appl" />
         <tli id="T66" time="53.534" type="appl" />
         <tli id="T67" time="54.733136973523706" />
         <tli id="T68" time="55.496" type="appl" />
         <tli id="T69" time="56.239" type="appl" />
         <tli id="T70" time="57.579793427566905" />
         <tli id="T71" time="58.37" type="appl" />
         <tli id="T72" time="58.969" type="appl" />
         <tli id="T73" time="59.567" type="appl" />
         <tli id="T74" time="60.166" type="appl" />
         <tli id="T75" time="60.79978187558298" />
         <tli id="T76" time="61.173" type="appl" />
         <tli id="T77" time="61.58" type="appl" />
         <tli id="T78" time="61.988" type="appl" />
         <tli id="T79" time="62.395" type="appl" />
         <tli id="T80" time="62.803" type="appl" />
         <tli id="T81" time="63.21" type="appl" />
         <tli id="T82" time="63.618" type="appl" />
         <tli id="T83" time="63.988" type="appl" />
         <tli id="T84" time="64.326" type="appl" />
         <tli id="T85" time="64.664" type="appl" />
         <tli id="T86" time="65.002" type="appl" />
         <tli id="T87" time="65.341" type="appl" />
         <tli id="T88" time="65.679" type="appl" />
         <tli id="T89" time="66.017" type="appl" />
         <tli id="T90" time="66.355" type="appl" />
         <tli id="T91" time="66.99309298988304" />
         <tli id="T92" time="67.722" type="appl" />
         <tli id="T93" time="68.418" type="appl" />
         <tli id="T94" time="69.115" type="appl" />
         <tli id="T95" time="69.811" type="appl" />
         <tli id="T96" time="70.507" type="appl" />
         <tli id="T97" time="71.015" type="appl" />
         <tli id="T98" time="71.523" type="appl" />
         <tli id="T99" time="72.482" type="appl" />
         <tli id="T100" time="73.44" type="appl" />
         <tli id="T101" time="75.13306378704169" />
         <tli id="T102" time="76.455" type="appl" />
         <tli id="T103" time="77.747" type="appl" />
         <tli id="T104" time="79.52638135897251" />
         <tli id="T105" time="80.128" type="appl" />
         <tli id="T106" time="80.598" type="appl" />
         <tli id="T107" time="81.068" type="appl" />
         <tli id="T108" time="81.52466846077998" />
         <tli id="T109" time="82.57" type="appl" />
         <tli id="T110" time="83.594" type="appl" />
         <tli id="T111" time="84.618" type="appl" />
         <tli id="T112" time="85.643" type="appl" />
         <tli id="T113" time="86.667" type="appl" />
         <tli id="T114" time="87.691" type="appl" />
         <tli id="T115" time="88.715" type="appl" />
         <tli id="T116" time="90.25300954294325" />
         <tli id="T117" time="91.181" type="appl" />
         <tli id="T118" time="91.971" type="appl" />
         <tli id="T119" time="92.762" type="appl" />
         <tli id="T120" time="94.07966248116524" />
         <tli id="T121" time="94.532" type="appl" />
         <tli id="T122" time="94.956" type="appl" />
         <tli id="T123" time="95.381" type="appl" />
         <tli id="T124" time="95.805" type="appl" />
         <tli id="T125" time="96.705" type="appl" />
         <tli id="T126" time="97.604" type="appl" />
         <tli id="T127" time="98.504" type="appl" />
         <tli id="T128" time="99.225" type="appl" />
         <tli id="T129" time="99.946" type="appl" />
         <tli id="T130" time="100.666" type="appl" />
         <tli id="T131" time="101.387" type="appl" />
         <tli id="T132" time="102.108" type="appl" />
         <tli id="T133" time="102.701" type="appl" />
         <tli id="T134" time="103.295" type="appl" />
         <tli id="T135" time="103.888" type="appl" />
         <tli id="T136" time="104.481" type="appl" />
         <tli id="T137" time="105.074" type="appl" />
         <tli id="T138" time="105.668" type="appl" />
         <tli id="T139" time="107.29961505345483" />
         <tli id="T140" time="108.041" type="appl" />
         <tli id="T141" time="108.753" type="appl" />
         <tli id="T142" time="109.465" type="appl" />
         <tli id="T143" time="110.177" type="appl" />
         <tli id="T144" time="111.06626820693118" />
         <tli id="T145" time="111.576" type="appl" />
         <tli id="T146" time="112.09" type="appl" />
         <tli id="T147" time="112.605" type="appl" />
         <tli id="T148" time="113.318" type="appl" />
         <tli id="T149" time="114.031" type="appl" />
         <tli id="T150" time="114.673" type="appl" />
         <tli id="T151" time="115.316" type="appl" />
         <tli id="T152" time="115.958" type="appl" />
         <tli id="T153" time="116.601" type="appl" />
         <tli id="T154" time="117.243" type="appl" />
         <tli id="T155" time="117.885" type="appl" />
         <tli id="T156" time="118.528" type="appl" />
         <tli id="T157" time="120.35290155700652" />
         <tli id="T158" time="121.134" type="appl" />
         <tli id="T159" time="121.87" type="appl" />
         <tli id="T160" time="122.605" type="appl" />
         <tli id="T161" time="123.341" type="appl" />
         <tli id="T162" time="124.077" type="appl" />
         <tli id="T163" time="124.812" type="appl" />
         <tli id="T164" time="125.548" type="appl" />
         <tli id="T165" time="126.97287780727561" />
         <tli id="T166" time="127.76" type="appl" />
         <tli id="T167" time="128.52" type="appl" />
         <tli id="T168" time="129.28" type="appl" />
         <tli id="T169" time="130.04" type="appl" />
         <tli id="T170" time="130.8" type="appl" />
         <tli id="T171" time="131.56" type="appl" />
         <tli id="T172" time="132.32" type="appl" />
         <tli id="T173" time="133.091" type="appl" />
         <tli id="T174" time="133.863" type="appl" />
         <tli id="T175" time="134.634" type="appl" />
         <tli id="T176" time="135.405" type="appl" />
         <tli id="T177" time="136.177" type="appl" />
         <tli id="T178" time="136.948" type="appl" />
         <tli id="T179" time="137.49" type="appl" />
         <tli id="T180" time="138.027" type="appl" />
         <tli id="T181" time="138.564" type="appl" />
         <tli id="T182" time="139.1" type="appl" />
         <tli id="T183" time="139.74" type="appl" />
         <tli id="T184" time="140.38" type="appl" />
         <tli id="T185" time="141.021" type="appl" />
         <tli id="T186" time="142.16615663342185" />
         <tli id="T187" time="142.858" type="appl" />
         <tli id="T188" time="143.386" type="appl" />
         <tli id="T189" time="144.4" type="appl" />
         <tli id="T190" time="145.413" type="appl" />
         <tli id="T191" time="146.426" type="appl" />
         <tli id="T192" time="147.70613675826937" />
         <tli id="T193" time="148.393" type="appl" />
         <tli id="T453" time="148.80142857142857" type="intp" />
         <tli id="T194" time="149.346" type="appl" />
         <tli id="T195" time="152.29278696993615" />
         <tli id="T196" time="153.075" type="appl" />
         <tli id="T197" time="153.804" type="appl" />
         <tli id="T198" time="154.533" type="appl" />
         <tli id="T199" time="155.262" type="appl" />
         <tli id="T200" time="155.992" type="appl" />
         <tli id="T201" time="156.721" type="appl" />
         <tli id="T202" time="157.45" type="appl" />
         <tli id="T203" time="158.179" type="appl" />
         <tli id="T204" time="158.737" type="appl" />
         <tli id="T205" time="159.296" type="appl" />
         <tli id="T206" time="159.854" type="appl" />
         <tli id="T207" time="160.56" type="appl" />
         <tli id="T208" time="161.265" type="appl" />
         <tli id="T209" time="163.61274635861375" />
         <tli id="T210" time="164.503" type="appl" />
         <tli id="T211" time="165.364" type="appl" />
         <tli id="T212" time="166.226" type="appl" />
         <tli id="T213" time="167.087" type="appl" />
         <tli id="T214" time="168.55272863600487" />
         <tli id="T215" time="169.27" type="appl" />
         <tli id="T216" time="169.921" type="appl" />
         <tli id="T217" time="170.573" type="appl" />
         <tli id="T218" time="171.224" type="appl" />
         <tli id="T219" time="172.576" type="appl" />
         <tli id="T220" time="173.929" type="appl" />
         <tli id="T221" time="175.281" type="appl" />
         <tli id="T222" time="175.904" type="appl" />
         <tli id="T223" time="176.528" type="appl" />
         <tli id="T224" time="177.259" type="appl" />
         <tli id="T225" time="177.989" type="appl" />
         <tli id="T226" time="179.25269024897753" />
         <tli id="T227" time="180.436" type="appl" />
         <tli id="T228" time="181.399" type="appl" />
         <tli id="T229" time="182.363" type="appl" />
         <tli id="T230" time="183.327" type="appl" />
         <tli id="T231" time="184.29" type="appl" />
         <tli id="T232" time="185.254" type="appl" />
         <tli id="T233" time="186.217" type="appl" />
         <tli id="T234" time="187.181" type="appl" />
         <tli id="T235" time="188.145" type="appl" />
         <tli id="T236" time="189.108" type="appl" />
         <tli id="T237" time="190.07265143144147" />
         <tli id="T238" time="192.4726428212671" />
         <tli id="T239" time="193.426" type="appl" />
         <tli id="T240" time="194.1" type="appl" />
         <tli id="T241" time="194.93930063858792" />
         <tli id="T242" time="195.559" type="appl" />
         <tli id="T243" time="196.342" type="appl" />
         <tli id="T244" time="197.126" type="appl" />
         <tli id="T245" time="198.2126222286001" />
         <tli id="T246" time="199.037" type="appl" />
         <tli id="T247" time="199.639" type="appl" />
         <tli id="T248" time="200.242" type="appl" />
         <tli id="T249" time="200.867" type="appl" />
         <tli id="T250" time="201.482" type="appl" />
         <tli id="T251" time="202.097" type="appl" />
         <tli id="T252" time="202.712" type="appl" />
         <tli id="T253" time="203.327" type="appl" />
         <tli id="T254" time="204.46593312764583" />
         <tli id="T255" time="205.391" type="appl" />
         <tli id="T256" time="206.155" type="appl" />
         <tli id="T257" time="206.919" type="appl" />
         <tli id="T258" time="207.645" type="appl" />
         <tli id="T259" time="208.371" type="appl" />
         <tli id="T260" time="209.097" type="appl" />
         <tli id="T261" time="209.823" type="appl" />
         <tli id="T262" time="210.549" type="appl" />
         <tli id="T263" time="211.275" type="appl" />
         <tli id="T264" time="212.254" type="appl" />
         <tli id="T265" time="215.4258938078496" />
         <tli id="T266" time="216.475" type="appl" />
         <tli id="T267" time="217.286" type="appl" />
         <tli id="T268" time="218.098" type="appl" />
         <tli id="T269" time="218.909" type="appl" />
         <tli id="T270" time="219.721" type="appl" />
         <tli id="T271" time="220.796" type="appl" />
         <tli id="T272" time="221.872" type="appl" />
         <tli id="T273" time="222.947" type="appl" />
         <tli id="T274" time="224.022" type="appl" />
         <tli id="T275" time="225.097" type="appl" />
         <tli id="T276" time="226.173" type="appl" />
         <tli id="T277" time="227.248" type="appl" />
         <tli id="T278" time="227.93" type="appl" />
         <tli id="T279" time="228.612" type="appl" />
         <tli id="T280" time="229.294" type="appl" />
         <tli id="T281" time="229.994" type="appl" />
         <tli id="T282" time="230.693" type="appl" />
         <tli id="T283" time="231.393" type="appl" />
         <tli id="T284" time="232.232" type="appl" />
         <tli id="T285" time="233.034" type="appl" />
         <tli id="T286" time="233.836" type="appl" />
         <tli id="T287" time="234.638" type="appl" />
         <tli id="T288" time="235.44" type="appl" />
         <tli id="T289" time="236.242" type="appl" />
         <tli id="T290" time="237.044" type="appl" />
         <tli id="T291" time="237.846" type="appl" />
         <tli id="T454" time="238.1668" type="intp" />
         <tli id="T292" time="238.648" type="appl" />
         <tli id="T293" time="239.304" type="appl" />
         <tli id="T294" time="239.96" type="appl" />
         <tli id="T295" time="240.615" type="appl" />
         <tli id="T296" time="241.271" type="appl" />
         <tli id="T297" time="241.927" type="appl" />
         <tli id="T298" time="242.582" type="appl" />
         <tli id="T299" time="243.238" type="appl" />
         <tli id="T300" time="243.894" type="appl" />
         <tli id="T301" time="244.767" type="appl" />
         <tli id="T302" time="245.641" type="appl" />
         <tli id="T303" time="246.514" type="appl" />
         <tli id="T304" time="247.362" type="appl" />
         <tli id="T305" time="248.166" type="appl" />
         <tli id="T306" time="248.99" type="appl" />
         <tli id="T307" time="249.58" type="appl" />
         <tli id="T308" time="250.502" type="appl" />
         <tli id="T309" time="251.424" type="appl" />
         <tli id="T310" time="252.346" type="appl" />
         <tli id="T311" time="253.269" type="appl" />
         <tli id="T312" time="254.191" type="appl" />
         <tli id="T313" time="255.113" type="appl" />
         <tli id="T314" time="256.65907921360406" />
         <tli id="T315" time="257.819" type="appl" />
         <tli id="T316" time="258.265" type="appl" />
         <tli id="T317" time="258.712" type="appl" />
         <tli id="T318" time="259.098" type="appl" />
         <tli id="T319" time="259.483" type="appl" />
         <tli id="T320" time="259.869" type="appl" />
         <tli id="T321" time="260.255" type="appl" />
         <tli id="T322" time="260.64" type="appl" />
         <tli id="T323" time="261.026" type="appl" />
         <tli id="T324" time="261.606" type="appl" />
         <tli id="T325" time="262.185" type="appl" />
         <tli id="T326" time="262.765" type="appl" />
         <tli id="T327" time="265.39904785821915" />
         <tli id="T328" time="266.128" type="appl" />
         <tli id="T329" time="266.674" type="appl" />
         <tli id="T330" time="267.219" type="appl" />
         <tli id="T331" time="267.9790386022817" />
         <tli id="T332" time="268.718" type="appl" />
         <tli id="T333" time="269.549" type="appl" />
         <tli id="T334" time="270.4723629906006" />
         <tli id="T335" time="271.078" type="appl" />
         <tli id="T336" time="271.738" type="appl" />
         <tli id="T337" time="272.399" type="appl" />
         <tli id="T338" time="273.059" type="appl" />
         <tli id="T339" time="273.72" type="appl" />
         <tli id="T340" time="274.38" type="appl" />
         <tli id="T341" time="275.041" type="appl" />
         <tli id="T342" time="275.869" type="appl" />
         <tli id="T343" time="277.252338666858" />
         <tli id="T344" time="278.271" type="appl" />
         <tli id="T345" time="278.943" type="appl" />
         <tli id="T346" time="279.616" type="appl" />
         <tli id="T347" time="280.288" type="appl" />
         <tli id="T348" time="280.96" type="appl" />
         <tli id="T349" time="282.32565379923943" />
         <tli id="T350" time="282.91" type="appl" />
         <tli id="T351" time="283.593" type="appl" />
         <tli id="T352" time="284.276" type="appl" />
         <tli id="T353" time="284.958" type="appl" />
         <tli id="T354" time="285.64" type="appl" />
         <tli id="T355" time="286.323" type="appl" />
         <tli id="T356" time="287.006" type="appl" />
         <tli id="T455" time="287.2788" type="intp" />
         <tli id="T357" time="287.688" type="appl" />
         <tli id="T358" time="288.226" type="appl" />
         <tli id="T359" time="288.763" type="appl" />
         <tli id="T360" time="289.73896053670086" />
         <tli id="T361" time="290.613" type="appl" />
         <tli id="T362" time="291.197" type="appl" />
         <tli id="T363" time="291.782" type="appl" />
         <tli id="T364" time="292.367" type="appl" />
         <tli id="T365" time="292.952" type="appl" />
         <tli id="T366" time="293.536" type="appl" />
         <tli id="T367" time="294.35894396211523" />
         <tli id="T368" time="295.433" type="appl" />
         <tli id="T369" time="296.31" type="appl" />
         <tli id="T370" time="297.186" type="appl" />
         <tli id="T371" time="298.063" type="appl" />
         <tli id="T372" time="299.74559130372387" />
         <tli id="T373" time="301.258" type="appl" />
         <tli id="T374" time="302.706" type="appl" />
         <tli id="T375" time="306.0122354882687" />
         <tli id="T376" time="306.904" type="appl" />
         <tli id="T377" time="307.519" type="appl" />
         <tli id="T378" time="308.134" type="appl" />
         <tli id="T379" time="308.748" type="appl" />
         <tli id="T380" time="309.362" type="appl" />
         <tli id="T381" time="310.16555392121694" />
         <tli id="T382" time="311.126" type="appl" />
         <tli id="T383" time="311.807" type="appl" />
         <tli id="T384" time="312.488" type="appl" />
         <tli id="T385" time="313.169" type="appl" />
         <tli id="T386" time="313.801" type="appl" />
         <tli id="T387" time="314.273" type="appl" />
         <tli id="T388" time="314.745" type="appl" />
         <tli id="T389" time="315.217" type="appl" />
         <tli id="T390" time="315.69" type="appl" />
         <tli id="T391" time="316.162" type="appl" />
         <tli id="T392" time="316.634" type="appl" />
         <tli id="T393" time="317.106" type="appl" />
         <tli id="T394" time="317.578" type="appl" />
         <tli id="T395" time="319.0921885628184" />
         <tli id="T396" time="320.127" type="appl" />
         <tli id="T397" time="320.788" type="appl" />
         <tli id="T398" time="321.45" type="appl" />
         <tli id="T399" time="322.8855082872928" />
         <tli id="T400" time="323.797" type="appl" />
         <tli id="T401" time="324.49" type="appl" />
         <tli id="T402" time="325.184" type="appl" />
         <tli id="T403" time="325.877" type="appl" />
         <tli id="T404" time="326.571" type="appl" />
         <tli id="T405" time="327.9854899906723" />
         <tli id="T406" time="329.144" type="appl" />
         <tli id="T407" time="330.4388145224941" />
         <tli id="T408" time="331.283" type="appl" />
         <tli id="T409" time="332.034" type="appl" />
         <tli id="T410" time="332.9854720528091" />
         <tli id="T411" time="334.5787996699433" />
         <tli id="T412" time="335.391" type="appl" />
         <tli id="T413" time="336.087" type="appl" />
         <tli id="T414" time="336.784" type="appl" />
         <tli id="T415" time="337.481" type="appl" />
         <tli id="T416" time="338.178" type="appl" />
         <tli id="T417" time="338.874" type="appl" />
         <tli id="T418" time="339.571" type="appl" />
         <tli id="T419" time="340.268" type="appl" />
         <tli id="T420" time="340.964" type="appl" />
         <tli id="T456" time="341.4072424481596" type="intp" />
         <tli id="T421" time="342.072106120399" />
         <tli id="T422" time="343.023" type="appl" />
         <tli id="T423" time="343.859" type="appl" />
         <tli id="T424" time="346.18542469685013" />
         <tli id="T425" time="347.449" type="appl" />
         <tli id="T426" time="348.469" type="appl" />
         <tli id="T427" time="349.49" type="appl" />
         <tli id="T428" time="350.51" type="appl" />
         <tli id="T429" time="351.531" type="appl" />
         <tli id="T430" time="352.552" type="appl" />
         <tli id="T431" time="353.572" type="appl" />
         <tli id="T432" time="355.7187238286575" />
         <tli id="T433" time="356.822" type="appl" />
         <tli id="T434" time="357.676" type="appl" />
         <tli id="T435" time="358.529" type="appl" />
         <tli id="T436" time="359.383" type="appl" />
         <tli id="T437" time="360.237" type="appl" />
         <tli id="T438" time="362.3320334361771" />
         <tli id="T439" time="363.511" type="appl" />
         <tli id="T440" time="364.362" type="appl" />
         <tli id="T441" time="365.081" type="appl" />
         <tli id="T442" time="365.8" type="appl" />
         <tli id="T443" time="366.518" type="appl" />
         <tli id="T444" time="367.237" type="appl" />
         <tli id="T445" time="367.956" type="appl" />
         <tli id="T446" time="368.627" type="appl" />
         <tli id="T447" time="369.213" type="appl" />
         <tli id="T448" time="369.799" type="appl" />
         <tli id="T449" time="370.386" type="appl" />
         <tli id="T450" time="370.972" type="appl" />
         <tli id="T451" time="371.558" type="appl" />
         <tli id="T452" time="371.652" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T42" start="T41">
            <tli id="T41.tx.1" />
            <tli id="T41.tx.2" />
         </timeline-fork>
         <timeline-fork end="T49" start="T48">
            <tli id="T48.tx.1" />
         </timeline-fork>
         <timeline-fork end="T115" start="T114">
            <tli id="T114.tx.1" />
            <tli id="T114.tx.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T451" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">nagur</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">koʔbdo</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ibi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Dĭ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_33" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">ka-</ts>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">kandəga</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">ipek</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">sadarzittə</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_49" n="HIAT:u" s="T12">
                  <nts id="Seg_50" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Ĭmbi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">šiʔnʼileʔ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">izittə</ts>
                  <nts id="Seg_59" n="HIAT:ip">?</nts>
                  <nts id="Seg_60" n="HIAT:ip">"</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">Onʼiʔ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">măndə:</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">"</nts>
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">Măna</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">iʔ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">kuvas</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">palʼto</ts>
                  <nts id="Seg_82" n="HIAT:ip">"</nts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">Onʼiʔ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">măndə:</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">"</nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Măna</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">kuvas</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">šalʼ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_103" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">ib-</ts>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">iʔ</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">i</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">kuvas</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">platʼtʼa</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">iʔ</ts>
                  <nts id="Seg_123" n="HIAT:ip">"</nts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_127" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">A</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">üdʼüge</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">samăjə:</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_137" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">tănan</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">ĭmbi</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">izittə</ts>
                  <nts id="Seg_149" n="HIAT:ip">?</nts>
                  <nts id="Seg_150" n="HIAT:ip">"</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_153" n="HIAT:u" s="T39">
                  <nts id="Seg_154" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">Măna</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">iʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41.tx.1" id="Seg_162" n="HIAT:w" s="T41">Filisnəj</ts>
                  <nts id="Seg_163" n="HIAT:ip">_</nts>
                  <ts e="T41.tx.2" id="Seg_165" n="HIAT:w" s="T41.tx.1">jasnəj</ts>
                  <nts id="Seg_166" n="HIAT:ip">_</nts>
                  <ts e="T42" id="Seg_168" n="HIAT:w" s="T41.tx.2">săkolika</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_171" n="HIAT:w" s="T42">pʼoruš</ts>
                  <nts id="Seg_172" n="HIAT:ip">"</nts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_176" n="HIAT:u" s="T43">
                  <nts id="Seg_177" n="HIAT:ip">(</nts>
                  <nts id="Seg_178" n="HIAT:ip">(</nts>
                  <ats e="T44" id="Seg_179" n="HIAT:non-pho" s="T43">BRK</ats>
                  <nts id="Seg_180" n="HIAT:ip">)</nts>
                  <nts id="Seg_181" n="HIAT:ip">)</nts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_185" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_187" n="HIAT:w" s="T44">Dĭ</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_190" n="HIAT:w" s="T45">măndə:</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_192" n="HIAT:ip">"</nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">Iʔ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">măna</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48.tx.1" id="Seg_200" n="HIAT:w" s="T48">Filišnəj</ts>
                  <nts id="Seg_201" n="HIAT:ip">_</nts>
                  <ts e="T49" id="Seg_203" n="HIAT:w" s="T48.tx.1">jasnəj</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_206" n="HIAT:w" s="T49">pʼoruškă</ts>
                  <nts id="Seg_207" n="HIAT:ip">"</nts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_211" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_213" n="HIAT:w" s="T50">Dĭ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_216" n="HIAT:w" s="T51">kambi</ts>
                  <nts id="Seg_217" n="HIAT:ip">,</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_220" n="HIAT:w" s="T52">dĭʔnə</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_223" n="HIAT:w" s="T53">ibi</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_226" n="HIAT:w" s="T54">palʼto</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">dĭʔnə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">šalʼ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_236" n="HIAT:w" s="T57">ibi</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">ku-</ts>
                  <nts id="Seg_244" n="HIAT:ip">)</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_247" n="HIAT:w" s="T60">i</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_250" n="HIAT:w" s="T61">platʼtʼa</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_253" n="HIAT:w" s="T62">ibi</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_257" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">Măndərbi</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_263" n="HIAT:w" s="T64">măndərbi</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_267" n="HIAT:w" s="T65">ej</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_270" n="HIAT:w" s="T66">kubi</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_274" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_276" n="HIAT:w" s="T67">Dĭgəttə</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_279" n="HIAT:w" s="T68">šobi</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_282" n="HIAT:w" s="T69">maːʔndə</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_286" n="HIAT:u" s="T70">
                  <nts id="Seg_287" n="HIAT:ip">(</nts>
                  <ts e="T71" id="Seg_289" n="HIAT:w" s="T70">Dĭ</ts>
                  <nts id="Seg_290" n="HIAT:ip">)</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_293" n="HIAT:w" s="T71">Dĭ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_296" n="HIAT:w" s="T72">šidö</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_299" n="HIAT:w" s="T73">koʔbdo</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_301" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_303" n="HIAT:w" s="T74">kaknarlaʔbəʔjə</ts>
                  <nts id="Seg_304" n="HIAT:ip">)</nts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_308" n="HIAT:u" s="T75">
                  <nts id="Seg_309" n="HIAT:ip">"</nts>
                  <ts e="T76" id="Seg_311" n="HIAT:w" s="T75">Tănan</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_314" n="HIAT:w" s="T76">ĭmbidə</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_317" n="HIAT:w" s="T77">ej</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_320" n="HIAT:w" s="T78">ibi</ts>
                  <nts id="Seg_321" n="HIAT:ip">,</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_324" n="HIAT:w" s="T79">a</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_327" n="HIAT:w" s="T80">miʔnʼibeʔ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_330" n="HIAT:w" s="T81">ibi</ts>
                  <nts id="Seg_331" n="HIAT:ip">"</nts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_335" n="HIAT:u" s="T82">
                  <nts id="Seg_336" n="HIAT:ip">"</nts>
                  <ts e="T83" id="Seg_338" n="HIAT:w" s="T82">Nu</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_341" n="HIAT:w" s="T83">i</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_344" n="HIAT:w" s="T84">što</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_348" n="HIAT:w" s="T85">ej</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_351" n="HIAT:w" s="T86">ibi</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_354" n="HIAT:w" s="T87">dak</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_357" n="HIAT:w" s="T88">puskaj</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_360" n="HIAT:w" s="T89">ej</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_363" n="HIAT:w" s="T90">iləj</ts>
                  <nts id="Seg_364" n="HIAT:ip">"</nts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_368" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_370" n="HIAT:w" s="T91">Dĭgəttə</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_373" n="HIAT:w" s="T92">dĭ</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_376" n="HIAT:w" s="T93">bazoʔ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_379" n="HIAT:w" s="T94">kambi</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_383" n="HIAT:w" s="T95">ipeksi</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_387" n="HIAT:u" s="T96">
                  <nts id="Seg_388" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_390" n="HIAT:w" s="T96">Ĭmbi</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_393" n="HIAT:w" s="T97">izittə</ts>
                  <nts id="Seg_394" n="HIAT:ip">?</nts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_398" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_400" n="HIAT:w" s="T98">Dĭgəttə</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_403" n="HIAT:w" s="T99">dĭzeŋ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_406" n="HIAT:w" s="T100">măndəʔi:</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_408" n="HIAT:ip">"</nts>
                  <ts e="T102" id="Seg_410" n="HIAT:w" s="T101">Bătinkaʔi</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_413" n="HIAT:w" s="T102">iʔ</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_417" n="HIAT:w" s="T103">kuvas</ts>
                  <nts id="Seg_418" n="HIAT:ip">"</nts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_422" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_424" n="HIAT:w" s="T104">Dĭ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_427" n="HIAT:w" s="T105">kambi</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_431" n="HIAT:w" s="T106">dĭzeŋdə</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_434" n="HIAT:w" s="T107">ibi</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_438" n="HIAT:u" s="T108">
                  <nts id="Seg_439" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">A=</ts>
                  <nts id="Seg_442" n="HIAT:ip">)</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_445" n="HIAT:w" s="T109">A</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_448" n="HIAT:w" s="T110">üdʼüge</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_451" n="HIAT:w" s="T111">măndə:</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_453" n="HIAT:ip">"</nts>
                  <ts e="T113" id="Seg_455" n="HIAT:w" s="T112">Măna</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_458" n="HIAT:w" s="T113">iʔ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114.tx.1" id="Seg_461" n="HIAT:w" s="T114">Filisnəj</ts>
                  <nts id="Seg_462" n="HIAT:ip">_</nts>
                  <ts e="T114.tx.2" id="Seg_464" n="HIAT:w" s="T114.tx.1">jasnəj</ts>
                  <nts id="Seg_465" n="HIAT:ip">_</nts>
                  <ts e="T115" id="Seg_467" n="HIAT:w" s="T114.tx.2">săkolika</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_470" n="HIAT:w" s="T115">pʼoruškă</ts>
                  <nts id="Seg_471" n="HIAT:ip">"</nts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_475" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_477" n="HIAT:w" s="T116">Dĭ</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_480" n="HIAT:w" s="T117">kambi</ts>
                  <nts id="Seg_481" n="HIAT:ip">,</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_483" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_485" n="HIAT:w" s="T118">dĭzeŋdə</ts>
                  <nts id="Seg_486" n="HIAT:ip">)</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_489" n="HIAT:w" s="T119">ibi</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_493" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_495" n="HIAT:w" s="T120">A</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_498" n="HIAT:w" s="T121">dĭm</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_501" n="HIAT:w" s="T122">ej</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_504" n="HIAT:w" s="T123">kubi</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_508" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_510" n="HIAT:w" s="T124">Dĭgəttə</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_513" n="HIAT:w" s="T125">nagurəŋ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_516" n="HIAT:w" s="T126">kambi</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_520" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_522" n="HIAT:w" s="T127">Dĭgəttə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_525" n="HIAT:w" s="T128">dĭzeŋdə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_528" n="HIAT:w" s="T129">ibi</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_531" n="HIAT:w" s="T130">bar</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_534" n="HIAT:w" s="T131">ĭmbi</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_538" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_540" n="HIAT:w" s="T132">A</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_543" n="HIAT:w" s="T133">dĭʔnə</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_546" n="HIAT:w" s="T134">kubi</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_549" n="HIAT:w" s="T135">dĭ</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_552" n="HIAT:w" s="T136">pʼoruškă</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_555" n="HIAT:w" s="T137">i</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_558" n="HIAT:w" s="T138">deʔpi</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_562" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_564" n="HIAT:w" s="T139">Dĭgəttə</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_567" n="HIAT:w" s="T140">dĭ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_570" n="HIAT:w" s="T141">bostə</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_572" n="HIAT:ip">(</nts>
                  <ts e="T143" id="Seg_574" n="HIAT:w" s="T142">kien</ts>
                  <nts id="Seg_575" n="HIAT:ip">)</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_578" n="HIAT:w" s="T143">kunolbi</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_582" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_584" n="HIAT:w" s="T144">Ibi</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_587" n="HIAT:w" s="T145">dĭ</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_590" n="HIAT:w" s="T146">pʼoruškă</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_594" n="HIAT:u" s="T147">
                  <nts id="Seg_595" n="HIAT:ip">(</nts>
                  <ts e="T148" id="Seg_597" n="HIAT:w" s="T147">Bambi</ts>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_601" n="HIAT:w" s="T148">udandə</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_605" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_607" n="HIAT:w" s="T149">Udandə</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_609" n="HIAT:ip">(</nts>
                  <ts e="T151" id="Seg_611" n="HIAT:w" s="T150">baʔluʔpi</ts>
                  <nts id="Seg_612" n="HIAT:ip">)</nts>
                  <nts id="Seg_613" n="HIAT:ip">,</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_616" n="HIAT:w" s="T151">dĭ</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_619" n="HIAT:w" s="T152">bar</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_622" n="HIAT:w" s="T153">nʼi</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_625" n="HIAT:w" s="T154">moluʔpi</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_629" n="HIAT:w" s="T155">bar</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_632" n="HIAT:w" s="T156">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_636" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_638" n="HIAT:w" s="T157">A</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_641" n="HIAT:w" s="T158">dĭn</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_644" n="HIAT:w" s="T159">sʼestrazaŋdə</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_647" n="HIAT:w" s="T160">măndobiʔi</ts>
                  <nts id="Seg_648" n="HIAT:ip">,</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_651" n="HIAT:w" s="T161">što</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_654" n="HIAT:w" s="T162">dĭn</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_657" n="HIAT:w" s="T163">nʼi</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_660" n="HIAT:w" s="T164">ige</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_664" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_666" n="HIAT:w" s="T165">Dĭgəttə</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_669" n="HIAT:w" s="T166">dĭ</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_671" n="HIAT:ip">(</nts>
                  <ts e="T168" id="Seg_673" n="HIAT:w" s="T167">dĭ-</ts>
                  <nts id="Seg_674" n="HIAT:ip">)</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_677" n="HIAT:w" s="T168">dĭn</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_680" n="HIAT:w" s="T169">kuznektə</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_683" n="HIAT:w" s="T170">ibi</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_686" n="HIAT:w" s="T171">üdʼüge</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_690" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_692" n="HIAT:w" s="T172">Dĭn</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_695" n="HIAT:w" s="T173">bar</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_698" n="HIAT:w" s="T174">dĭbər</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_701" n="HIAT:w" s="T175">tagajʔi</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_703" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_705" n="HIAT:w" s="T176">n-</ts>
                  <nts id="Seg_706" n="HIAT:ip">)</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_709" n="HIAT:w" s="T177">müʔtəbiʔi</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_713" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_715" n="HIAT:w" s="T178">A</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_718" n="HIAT:w" s="T179">dĭm</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_720" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_722" n="HIAT:w" s="T180">bĭ-</ts>
                  <nts id="Seg_723" n="HIAT:ip">)</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_726" n="HIAT:w" s="T181">bĭtəlbiʔi</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_730" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_732" n="HIAT:w" s="T182">Dĭrgit</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_735" n="HIAT:w" s="T183">bü</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_738" n="HIAT:w" s="T184">što</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_741" n="HIAT:w" s="T185">kunolbi</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_745" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_747" n="HIAT:w" s="T186">Dĭ</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_750" n="HIAT:w" s="T187">šobi</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_754" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_756" n="HIAT:w" s="T188">Kirgarbi</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_760" n="HIAT:w" s="T189">kirgarbi</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_764" n="HIAT:w" s="T190">üjüttə</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_767" n="HIAT:w" s="T191">băʔpi</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_771" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_773" n="HIAT:w" s="T192">I</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_776" n="HIAT:w" s="T193">kalla</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_779" n="HIAT:w" s="T453">dʼürbi</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_783" n="HIAT:u" s="T194">
                  <nts id="Seg_784" n="HIAT:ip">(</nts>
                  <nts id="Seg_785" n="HIAT:ip">(</nts>
                  <ats e="T195" id="Seg_786" n="HIAT:non-pho" s="T194">BRK</ats>
                  <nts id="Seg_787" n="HIAT:ip">)</nts>
                  <nts id="Seg_788" n="HIAT:ip">)</nts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_792" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_794" n="HIAT:w" s="T195">Dĭn</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_797" n="HIAT:w" s="T196">sʼestrazaŋdə</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_800" n="HIAT:w" s="T197">dĭʔnə</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_803" n="HIAT:w" s="T198">mĭbiʔi</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_806" n="HIAT:w" s="T199">kaplʼaʔi</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_810" n="HIAT:w" s="T200">štobɨ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_813" n="HIAT:w" s="T201">dĭ</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_816" n="HIAT:w" s="T202">kunolluʔpi</ts>
                  <nts id="Seg_817" n="HIAT:ip">.</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_820" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_822" n="HIAT:w" s="T203">Dĭ</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_825" n="HIAT:w" s="T204">bar</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_828" n="HIAT:w" s="T205">kunolluʔpi</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_832" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_834" n="HIAT:w" s="T206">A</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_837" n="HIAT:w" s="T207">dĭ</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_840" n="HIAT:w" s="T208">šobi</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_844" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_846" n="HIAT:w" s="T209">Kuznektə</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_849" n="HIAT:w" s="T210">dĭzeŋ</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_852" n="HIAT:w" s="T211">dĭn</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_855" n="HIAT:w" s="T212">müʔpiʔi</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_858" n="HIAT:w" s="T213">tagajʔi</ts>
                  <nts id="Seg_859" n="HIAT:ip">.</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_862" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_864" n="HIAT:w" s="T214">Dĭ</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_867" n="HIAT:w" s="T215">bar</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_870" n="HIAT:w" s="T216">šüzittə</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_873" n="HIAT:w" s="T217">xatʼel</ts>
                  <nts id="Seg_874" n="HIAT:ip">.</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_877" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_879" n="HIAT:w" s="T218">Üjübə</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_882" n="HIAT:w" s="T219">bar</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_885" n="HIAT:w" s="T220">bătluʔpi</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_889" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_891" n="HIAT:w" s="T221">Kem</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_894" n="HIAT:w" s="T222">mʼaŋnuʔpi</ts>
                  <nts id="Seg_895" n="HIAT:ip">.</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_898" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_900" n="HIAT:w" s="T223">Dĭgəttə</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_903" n="HIAT:w" s="T224">sazən</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_906" n="HIAT:w" s="T225">pʼaŋdəbi</ts>
                  <nts id="Seg_907" n="HIAT:ip">.</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_910" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_912" n="HIAT:w" s="T226">I</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_914" n="HIAT:ip">(</nts>
                  <ts e="T228" id="Seg_916" n="HIAT:w" s="T227">baʔ-</ts>
                  <nts id="Seg_917" n="HIAT:ip">)</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_920" n="HIAT:w" s="T228">embi:</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_922" n="HIAT:ip">"</nts>
                  <ts e="T230" id="Seg_924" n="HIAT:w" s="T229">Kamen</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_927" n="HIAT:w" s="T230">bazaj</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_930" n="HIAT:w" s="T231">jamaʔi</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_933" n="HIAT:w" s="T232">aləl</ts>
                  <nts id="Seg_934" n="HIAT:ip">,</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_937" n="HIAT:w" s="T233">iʔtləl</ts>
                  <nts id="Seg_938" n="HIAT:ip">,</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_941" n="HIAT:w" s="T234">dĭgəttə</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_944" n="HIAT:w" s="T235">măna</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_947" n="HIAT:w" s="T236">kulal</ts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_951" n="HIAT:u" s="T237">
                  <nts id="Seg_952" n="HIAT:ip">(</nts>
                  <nts id="Seg_953" n="HIAT:ip">(</nts>
                  <ats e="T238" id="Seg_954" n="HIAT:non-pho" s="T237">BRK</ats>
                  <nts id="Seg_955" n="HIAT:ip">)</nts>
                  <nts id="Seg_956" n="HIAT:ip">)</nts>
                  <nts id="Seg_957" n="HIAT:ip">.</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_960" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_962" n="HIAT:w" s="T238">Dĭgəttə</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_965" n="HIAT:w" s="T239">dĭ</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_968" n="HIAT:w" s="T240">uʔbdəbi</ts>
                  <nts id="Seg_969" n="HIAT:ip">.</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_972" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_974" n="HIAT:w" s="T241">Kuiot:</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_977" n="HIAT:w" s="T242">bar</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_980" n="HIAT:w" s="T243">kem</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_983" n="HIAT:w" s="T244">dön</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_987" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_989" n="HIAT:w" s="T245">I</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_992" n="HIAT:w" s="T246">sazən</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_995" n="HIAT:w" s="T247">iʔbolaʔbə</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_999" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_1001" n="HIAT:w" s="T248">Dĭ</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1004" n="HIAT:w" s="T249">sazən</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1007" n="HIAT:w" s="T250">măndəbi</ts>
                  <nts id="Seg_1008" n="HIAT:ip">,</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1011" n="HIAT:w" s="T251">bar</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1014" n="HIAT:w" s="T252">dʼorbi</ts>
                  <nts id="Seg_1015" n="HIAT:ip">,</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1018" n="HIAT:w" s="T253">dʼorbi</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_1022" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_1024" n="HIAT:w" s="T254">Da</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1027" n="HIAT:w" s="T255">kambi</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1030" n="HIAT:w" s="T256">kuznʼitsanə</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1034" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_1036" n="HIAT:w" s="T257">Jamaʔi</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1039" n="HIAT:w" s="T258">abi</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1042" n="HIAT:w" s="T259">bazaj</ts>
                  <nts id="Seg_1043" n="HIAT:ip">,</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1046" n="HIAT:w" s="T260">pa</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1049" n="HIAT:w" s="T261">abi</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1052" n="HIAT:w" s="T262">bazaj</ts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1056" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1058" n="HIAT:w" s="T263">Dĭgəttə</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1061" n="HIAT:w" s="T264">kambi</ts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1065" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1067" n="HIAT:w" s="T265">Kambi</ts>
                  <nts id="Seg_1068" n="HIAT:ip">,</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1071" n="HIAT:w" s="T266">kambi</ts>
                  <nts id="Seg_1072" n="HIAT:ip">,</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1075" n="HIAT:w" s="T267">kundʼom</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1077" n="HIAT:ip">(</nts>
                  <ts e="T269" id="Seg_1079" n="HIAT:w" s="T268">šonəga=</ts>
                  <nts id="Seg_1080" n="HIAT:ip">)</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1083" n="HIAT:w" s="T269">šonəbi</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1087" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1089" n="HIAT:w" s="T270">Dĭgəttə</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1091" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1093" n="HIAT:w" s="T271">kuiot=</ts>
                  <nts id="Seg_1094" n="HIAT:ip">)</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1097" n="HIAT:w" s="T272">kulaʔbə:</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1100" n="HIAT:w" s="T273">tura</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1102" n="HIAT:ip">(</nts>
                  <ts e="T275" id="Seg_1104" n="HIAT:w" s="T274">i-</ts>
                  <nts id="Seg_1105" n="HIAT:ip">)</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1108" n="HIAT:w" s="T275">nuga</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1110" n="HIAT:ip">(</nts>
                  <ts e="T277" id="Seg_1112" n="HIAT:w" s="T276">idʼiʔeje</ts>
                  <nts id="Seg_1113" n="HIAT:ip">)</nts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1117" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1119" n="HIAT:w" s="T277">Dĭ</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1122" n="HIAT:w" s="T278">turanə</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1125" n="HIAT:w" s="T279">šobi</ts>
                  <nts id="Seg_1126" n="HIAT:ip">.</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1129" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1131" n="HIAT:w" s="T280">Dĭn</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1134" n="HIAT:w" s="T281">nüke</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1137" n="HIAT:w" s="T282">amnolaʔbə</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1141" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1143" n="HIAT:w" s="T283">Dĭ</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1146" n="HIAT:w" s="T284">surarlaʔbə:</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1148" n="HIAT:ip">"</nts>
                  <ts e="T286" id="Seg_1150" n="HIAT:w" s="T285">Nüke</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1154" n="HIAT:w" s="T286">nüke</ts>
                  <nts id="Seg_1155" n="HIAT:ip">,</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1158" n="HIAT:w" s="T287">nörbit</ts>
                  <nts id="Seg_1159" n="HIAT:ip">,</nts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1162" n="HIAT:w" s="T288">gijen</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1165" n="HIAT:w" s="T289">măn</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1168" n="HIAT:w" s="T290">tibim</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1171" n="HIAT:w" s="T291">kalla</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1174" n="HIAT:w" s="T454">dʼürbi</ts>
                  <nts id="Seg_1175" n="HIAT:ip">?</nts>
                  <nts id="Seg_1176" n="HIAT:ip">"</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1179" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1181" n="HIAT:w" s="T292">Dĭ</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1184" n="HIAT:w" s="T293">măndə:</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1186" n="HIAT:ip">"</nts>
                  <ts e="T295" id="Seg_1188" n="HIAT:w" s="T294">Măn</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1191" n="HIAT:w" s="T295">ej</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1194" n="HIAT:w" s="T296">tĭmnem</ts>
                  <nts id="Seg_1195" n="HIAT:ip">,</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1198" n="HIAT:w" s="T297">gijen</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1200" n="HIAT:ip">(</nts>
                  <ts e="T299" id="Seg_1202" n="HIAT:w" s="T298">dʼüʔ-</ts>
                  <nts id="Seg_1203" n="HIAT:ip">)</nts>
                  <nts id="Seg_1204" n="HIAT:ip">,</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1207" n="HIAT:w" s="T299">gijendə</ts>
                  <nts id="Seg_1208" n="HIAT:ip">.</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1211" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1213" n="HIAT:w" s="T300">Kanaʔ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1216" n="HIAT:w" s="T301">măn</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1219" n="HIAT:w" s="T302">sʼestrandə</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1223" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1225" n="HIAT:w" s="T303">Dĭ</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1228" n="HIAT:w" s="T304">tĭmnet</ts>
                  <nts id="Seg_1229" n="HIAT:ip">.</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1232" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1234" n="HIAT:w" s="T305">Nörbələj</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1237" n="HIAT:w" s="T306">tănan</ts>
                  <nts id="Seg_1238" n="HIAT:ip">"</nts>
                  <nts id="Seg_1239" n="HIAT:ip">.</nts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1242" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1244" n="HIAT:w" s="T307">Dĭgəttə</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1247" n="HIAT:w" s="T308">dĭʔnə</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1250" n="HIAT:w" s="T309">mĭbi</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1253" n="HIAT:w" s="T310">takše</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1256" n="HIAT:w" s="T311">zălătoj</ts>
                  <nts id="Seg_1257" n="HIAT:ip">,</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1260" n="HIAT:w" s="T312">munəjʔ</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1263" n="HIAT:w" s="T313">zălătoj</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1267" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1269" n="HIAT:w" s="T314">I</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1272" n="HIAT:w" s="T315">nʼitkaʔi</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1275" n="HIAT:w" s="T316">klubok</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1279" n="HIAT:u" s="T317">
                  <nts id="Seg_1280" n="HIAT:ip">"</nts>
                  <ts e="T318" id="Seg_1282" n="HIAT:w" s="T317">Kanaʔ</ts>
                  <nts id="Seg_1283" n="HIAT:ip">,</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1286" n="HIAT:w" s="T318">dĭ</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1289" n="HIAT:w" s="T319">nuʔmələj</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1292" n="HIAT:w" s="T320">i</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1295" n="HIAT:w" s="T321">tăn</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1298" n="HIAT:w" s="T322">kanaʔ</ts>
                  <nts id="Seg_1299" n="HIAT:ip">"</nts>
                  <nts id="Seg_1300" n="HIAT:ip">.</nts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1303" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1305" n="HIAT:w" s="T323">Dĭgəttə</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1308" n="HIAT:w" s="T324">dĭ</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1311" n="HIAT:w" s="T325">kambi</ts>
                  <nts id="Seg_1312" n="HIAT:ip">.</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1315" n="HIAT:u" s="T326">
                  <nts id="Seg_1316" n="HIAT:ip">(</nts>
                  <nts id="Seg_1317" n="HIAT:ip">(</nts>
                  <ats e="T327" id="Seg_1318" n="HIAT:non-pho" s="T326">BRK</ats>
                  <nts id="Seg_1319" n="HIAT:ip">)</nts>
                  <nts id="Seg_1320" n="HIAT:ip">)</nts>
                  <nts id="Seg_1321" n="HIAT:ip">.</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1324" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1326" n="HIAT:w" s="T327">Dĭgəttə</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1329" n="HIAT:w" s="T328">dĭ</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1332" n="HIAT:w" s="T329">kambi</ts>
                  <nts id="Seg_1333" n="HIAT:ip">,</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1336" n="HIAT:w" s="T330">kambi</ts>
                  <nts id="Seg_1337" n="HIAT:ip">.</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1340" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1342" n="HIAT:w" s="T331">Baška</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1345" n="HIAT:w" s="T332">sʼestrandə</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1348" n="HIAT:w" s="T333">šobi</ts>
                  <nts id="Seg_1349" n="HIAT:ip">.</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1352" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1354" n="HIAT:w" s="T334">Tože</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1356" n="HIAT:ip">(</nts>
                  <ts e="T336" id="Seg_1358" n="HIAT:w" s="T335">turat</ts>
                  <nts id="Seg_1359" n="HIAT:ip">)</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1361" n="HIAT:ip">(</nts>
                  <ts e="T337" id="Seg_1363" n="HIAT:w" s="T336">ej</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1366" n="HIAT:w" s="T337">üdʼü-</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1369" n="HIAT:w" s="T338">ej-</ts>
                  <nts id="Seg_1370" n="HIAT:ip">)</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1373" n="HIAT:w" s="T339">üdʼüge</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1376" n="HIAT:w" s="T340">turat</ts>
                  <nts id="Seg_1377" n="HIAT:ip">.</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1380" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1382" n="HIAT:w" s="T341">Šübi</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1385" n="HIAT:w" s="T342">turanə</ts>
                  <nts id="Seg_1386" n="HIAT:ip">.</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1389" n="HIAT:u" s="T343">
                  <nts id="Seg_1390" n="HIAT:ip">(</nts>
                  <ts e="T344" id="Seg_1392" n="HIAT:w" s="T343">Măndə-</ts>
                  <nts id="Seg_1393" n="HIAT:ip">,</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1396" n="HIAT:w" s="T344">dĭn=</ts>
                  <nts id="Seg_1397" n="HIAT:ip">)</nts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1400" n="HIAT:w" s="T345">Dĭn</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1403" n="HIAT:w" s="T346">nüke</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1406" n="HIAT:w" s="T347">amnolaʔbə</ts>
                  <nts id="Seg_1407" n="HIAT:ip">.</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1410" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1412" n="HIAT:w" s="T348">Măndə:</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1414" n="HIAT:ip">"</nts>
                  <ts e="T350" id="Seg_1416" n="HIAT:w" s="T349">Nüke</ts>
                  <nts id="Seg_1417" n="HIAT:ip">,</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1420" n="HIAT:w" s="T350">nüke</ts>
                  <nts id="Seg_1421" n="HIAT:ip">,</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1424" n="HIAT:w" s="T351">nörbeʔ</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1427" n="HIAT:w" s="T352">măna</ts>
                  <nts id="Seg_1428" n="HIAT:ip">,</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1431" n="HIAT:w" s="T353">gijen</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1434" n="HIAT:w" s="T354">măn</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1437" n="HIAT:w" s="T355">tibi</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1440" n="HIAT:w" s="T356">kalla</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1443" n="HIAT:w" s="T455">dʼürbi</ts>
                  <nts id="Seg_1444" n="HIAT:ip">?</nts>
                  <nts id="Seg_1445" n="HIAT:ip">"</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1448" n="HIAT:u" s="T357">
                  <nts id="Seg_1449" n="HIAT:ip">"</nts>
                  <ts e="T358" id="Seg_1451" n="HIAT:w" s="T357">Măn</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1454" n="HIAT:w" s="T358">ej</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1457" n="HIAT:w" s="T359">tĭmnem</ts>
                  <nts id="Seg_1458" n="HIAT:ip">.</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1461" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1463" n="HIAT:w" s="T360">Kanaʔ</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1466" n="HIAT:w" s="T361">măn</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1469" n="HIAT:w" s="T362">sʼestrandə</ts>
                  <nts id="Seg_1470" n="HIAT:ip">,</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1473" n="HIAT:w" s="T363">dĭ</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1476" n="HIAT:w" s="T364">nörbələj</ts>
                  <nts id="Seg_1477" n="HIAT:ip">,</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1480" n="HIAT:w" s="T365">gijen</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1483" n="HIAT:w" s="T366">dĭ</ts>
                  <nts id="Seg_1484" n="HIAT:ip">"</nts>
                  <nts id="Seg_1485" n="HIAT:ip">.</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1488" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1490" n="HIAT:w" s="T367">Dĭ</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1493" n="HIAT:w" s="T368">dĭʔnə</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1495" n="HIAT:ip">(</nts>
                  <ts e="T370" id="Seg_1497" n="HIAT:w" s="T369">mĭmbi=</ts>
                  <nts id="Seg_1498" n="HIAT:ip">)</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1501" n="HIAT:w" s="T370">mĭbi</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1504" n="HIAT:w" s="T371">prʼalka</ts>
                  <nts id="Seg_1505" n="HIAT:ip">.</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1508" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1510" n="HIAT:w" s="T372">I</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1513" n="HIAT:w" s="T373">mĭbi</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1516" n="HIAT:w" s="T374">kugʼelʼi</ts>
                  <nts id="Seg_1517" n="HIAT:ip">.</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1520" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1522" n="HIAT:w" s="T375">Prʼalka</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1525" n="HIAT:w" s="T376">bar</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1528" n="HIAT:w" s="T377">zălătoj</ts>
                  <nts id="Seg_1529" n="HIAT:ip">,</nts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1532" n="HIAT:w" s="T378">i</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1535" n="HIAT:w" s="T379">kugʼelʼi</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1538" n="HIAT:w" s="T380">zălătoj</ts>
                  <nts id="Seg_1539" n="HIAT:ip">.</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1542" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1544" n="HIAT:w" s="T381">Dĭgəttə</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1546" n="HIAT:ip">(</nts>
                  <ts e="T383" id="Seg_1548" n="HIAT:w" s="T382">mĭ-</ts>
                  <nts id="Seg_1549" n="HIAT:ip">)</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1552" n="HIAT:w" s="T383">mĭbi</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1555" n="HIAT:w" s="T384">klubok</ts>
                  <nts id="Seg_1556" n="HIAT:ip">.</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1559" n="HIAT:u" s="T385">
                  <nts id="Seg_1560" n="HIAT:ip">"</nts>
                  <ts e="T386" id="Seg_1562" n="HIAT:w" s="T385">Dĭ</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1564" n="HIAT:ip">(</nts>
                  <ts e="T387" id="Seg_1566" n="HIAT:w" s="T386">kandlo-</ts>
                  <nts id="Seg_1567" n="HIAT:ip">)</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1570" n="HIAT:w" s="T387">kandluj</ts>
                  <nts id="Seg_1571" n="HIAT:ip">,</nts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1574" n="HIAT:w" s="T388">tăn</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1576" n="HIAT:ip">(</nts>
                  <ts e="T390" id="Seg_1578" n="HIAT:w" s="T389">u-</ts>
                  <nts id="Seg_1579" n="HIAT:ip">)</nts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1582" n="HIAT:w" s="T390">kanaʔ</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1585" n="HIAT:w" s="T391">dĭbər</ts>
                  <nts id="Seg_1586" n="HIAT:ip">,</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1589" n="HIAT:w" s="T392">gibər</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1592" n="HIAT:w" s="T393">dĭ</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1595" n="HIAT:w" s="T394">kandluj</ts>
                  <nts id="Seg_1596" n="HIAT:ip">"</nts>
                  <nts id="Seg_1597" n="HIAT:ip">.</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1600" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1602" n="HIAT:w" s="T395">Dĭgəttə</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1605" n="HIAT:w" s="T396">dö</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1608" n="HIAT:w" s="T397">kambi</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1612" n="HIAT:w" s="T398">kambi</ts>
                  <nts id="Seg_1613" n="HIAT:ip">.</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1616" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1618" n="HIAT:w" s="T399">Nagur</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1621" n="HIAT:w" s="T400">sʼestrandə</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1624" n="HIAT:w" s="T401">šobi</ts>
                  <nts id="Seg_1625" n="HIAT:ip">,</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1628" n="HIAT:w" s="T402">tože</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1631" n="HIAT:w" s="T403">tura</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1634" n="HIAT:w" s="T404">nulaʔbə</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1638" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1640" n="HIAT:w" s="T405">Dĭ</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1643" n="HIAT:w" s="T406">šobi</ts>
                  <nts id="Seg_1644" n="HIAT:ip">.</nts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1647" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1649" n="HIAT:w" s="T407">Dĭn</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1652" n="HIAT:w" s="T408">nüke</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1655" n="HIAT:w" s="T409">amnolaʔbə</ts>
                  <nts id="Seg_1656" n="HIAT:ip">.</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1659" n="HIAT:u" s="T410">
                  <nts id="Seg_1660" n="HIAT:ip">(</nts>
                  <ts e="T411" id="Seg_1662" n="HIAT:w" s="T410">ugandə</ts>
                  <nts id="Seg_1663" n="HIAT:ip">)</nts>
                  <nts id="Seg_1664" n="HIAT:ip">.</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1667" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1669" n="HIAT:w" s="T411">Dĭgəttə</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1672" n="HIAT:w" s="T412">măndə:</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1674" n="HIAT:ip">"</nts>
                  <ts e="T414" id="Seg_1676" n="HIAT:w" s="T413">Nüke</ts>
                  <nts id="Seg_1677" n="HIAT:ip">,</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1680" n="HIAT:w" s="T414">nüke</ts>
                  <nts id="Seg_1681" n="HIAT:ip">,</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1684" n="HIAT:w" s="T415">nörbeʔ</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1687" n="HIAT:w" s="T416">măna</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1691" n="HIAT:w" s="T417">gijən</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1694" n="HIAT:w" s="T418">măn</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1697" n="HIAT:w" s="T419">tibim</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1700" n="HIAT:w" s="T420">kalla</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1703" n="HIAT:w" s="T456">dʼürbi</ts>
                  <nts id="Seg_1704" n="HIAT:ip">?</nts>
                  <nts id="Seg_1705" n="HIAT:ip">"</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1708" n="HIAT:u" s="T421">
                  <nts id="Seg_1709" n="HIAT:ip">"</nts>
                  <ts e="T422" id="Seg_1711" n="HIAT:w" s="T421">Măn</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1714" n="HIAT:w" s="T422">tĭmnem</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1718" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1720" n="HIAT:w" s="T423">Kanaʔ</ts>
                  <nts id="Seg_1721" n="HIAT:ip">"</nts>
                  <nts id="Seg_1722" n="HIAT:ip">.</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1725" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1727" n="HIAT:w" s="T424">Dĭgəttə</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1730" n="HIAT:w" s="T425">dĭ</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1733" n="HIAT:w" s="T426">mĭbi</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1736" n="HIAT:w" s="T427">dĭʔnə</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1738" n="HIAT:ip">(</nts>
                  <ts e="T429" id="Seg_1740" n="HIAT:w" s="T428">korniš</ts>
                  <nts id="Seg_1741" n="HIAT:ip">)</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1744" n="HIAT:w" s="T429">kuvas</ts>
                  <nts id="Seg_1745" n="HIAT:ip">,</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1748" n="HIAT:w" s="T430">zălătoj</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1751" n="HIAT:w" s="T431">bar</ts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1755" n="HIAT:u" s="T432">
                  <nts id="Seg_1756" n="HIAT:ip">(</nts>
                  <ts e="T433" id="Seg_1758" n="HIAT:w" s="T432">Igol'-</ts>
                  <nts id="Seg_1759" n="HIAT:ip">)</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1762" n="HIAT:w" s="T433">Kălʼečkă</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1765" n="HIAT:w" s="T434">mĭbi</ts>
                  <nts id="Seg_1766" n="HIAT:ip">,</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1769" n="HIAT:w" s="T435">zălătoj</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1772" n="HIAT:w" s="T436">udandə</ts>
                  <nts id="Seg_1773" n="HIAT:ip">.</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1776" n="HIAT:u" s="T437">
                  <nts id="Seg_1777" n="HIAT:ip">"</nts>
                  <ts e="T438" id="Seg_1779" n="HIAT:w" s="T437">Kanaʔ</ts>
                  <nts id="Seg_1780" n="HIAT:ip">.</nts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1783" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1785" n="HIAT:w" s="T438">Gijendə</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1788" n="HIAT:w" s="T439">amnolaʔbə</ts>
                  <nts id="Seg_1789" n="HIAT:ip">"</nts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1793" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1795" n="HIAT:w" s="T440">Dĭʔnə</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1798" n="HIAT:w" s="T441">mĭbi</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1801" n="HIAT:w" s="T442">tože</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1803" n="HIAT:ip">(</nts>
                  <ts e="T444" id="Seg_1805" n="HIAT:w" s="T443">k-</ts>
                  <nts id="Seg_1806" n="HIAT:ip">)</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1809" n="HIAT:w" s="T444">klubok</ts>
                  <nts id="Seg_1810" n="HIAT:ip">.</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1813" n="HIAT:u" s="T445">
                  <nts id="Seg_1814" n="HIAT:ip">"</nts>
                  <ts e="T446" id="Seg_1816" n="HIAT:w" s="T445">Gibər</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1819" n="HIAT:w" s="T446">dĭ</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1822" n="HIAT:w" s="T447">kandləj</ts>
                  <nts id="Seg_1823" n="HIAT:ip">,</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1826" n="HIAT:w" s="T448">tăn</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1829" n="HIAT:w" s="T449">dĭbər</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1832" n="HIAT:w" s="T450">kanaʔ</ts>
                  <nts id="Seg_1833" n="HIAT:ip">"</nts>
                  <nts id="Seg_1834" n="HIAT:ip">.</nts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T451" id="Seg_1836" n="sc" s="T0">
               <ts e="T1" id="Seg_1838" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_1840" n="e" s="T1">büzʼe </ts>
               <ts e="T3" id="Seg_1842" n="e" s="T2">amnobi. </ts>
               <ts e="T4" id="Seg_1844" n="e" s="T3">Dĭn </ts>
               <ts e="T5" id="Seg_1846" n="e" s="T4">nagur </ts>
               <ts e="T6" id="Seg_1848" n="e" s="T5">koʔbdo </ts>
               <ts e="T7" id="Seg_1850" n="e" s="T6">ibi. </ts>
               <ts e="T8" id="Seg_1852" n="e" s="T7">Dĭ </ts>
               <ts e="T9" id="Seg_1854" n="e" s="T8">(ka-) </ts>
               <ts e="T10" id="Seg_1856" n="e" s="T9">kandəga </ts>
               <ts e="T11" id="Seg_1858" n="e" s="T10">ipek </ts>
               <ts e="T12" id="Seg_1860" n="e" s="T11">sadarzittə. </ts>
               <ts e="T13" id="Seg_1862" n="e" s="T12">"Ĭmbi </ts>
               <ts e="T14" id="Seg_1864" n="e" s="T13">šiʔnʼileʔ </ts>
               <ts e="T15" id="Seg_1866" n="e" s="T14">izittə?" </ts>
               <ts e="T16" id="Seg_1868" n="e" s="T15">Onʼiʔ </ts>
               <ts e="T17" id="Seg_1870" n="e" s="T16">măndə: </ts>
               <ts e="T18" id="Seg_1872" n="e" s="T17">"Măna </ts>
               <ts e="T19" id="Seg_1874" n="e" s="T18">iʔ </ts>
               <ts e="T20" id="Seg_1876" n="e" s="T19">kuvas </ts>
               <ts e="T21" id="Seg_1878" n="e" s="T20">palʼto". </ts>
               <ts e="T22" id="Seg_1880" n="e" s="T21">Onʼiʔ </ts>
               <ts e="T23" id="Seg_1882" n="e" s="T22">măndə: </ts>
               <ts e="T24" id="Seg_1884" n="e" s="T23">"Măna </ts>
               <ts e="T25" id="Seg_1886" n="e" s="T24">kuvas </ts>
               <ts e="T26" id="Seg_1888" n="e" s="T25">šalʼ </ts>
               <ts e="T27" id="Seg_1890" n="e" s="T26">(ib-) </ts>
               <ts e="T28" id="Seg_1892" n="e" s="T27">iʔ, </ts>
               <ts e="T29" id="Seg_1894" n="e" s="T28">i </ts>
               <ts e="T30" id="Seg_1896" n="e" s="T29">kuvas </ts>
               <ts e="T31" id="Seg_1898" n="e" s="T30">platʼtʼa </ts>
               <ts e="T32" id="Seg_1900" n="e" s="T31">iʔ". </ts>
               <ts e="T33" id="Seg_1902" n="e" s="T32">A </ts>
               <ts e="T34" id="Seg_1904" n="e" s="T33">üdʼüge </ts>
               <ts e="T35" id="Seg_1906" n="e" s="T34">samăjə: </ts>
               <ts e="T36" id="Seg_1908" n="e" s="T35">"A </ts>
               <ts e="T37" id="Seg_1910" n="e" s="T36">tănan </ts>
               <ts e="T38" id="Seg_1912" n="e" s="T37">ĭmbi </ts>
               <ts e="T39" id="Seg_1914" n="e" s="T38">izittə?" </ts>
               <ts e="T40" id="Seg_1916" n="e" s="T39">"Măna </ts>
               <ts e="T41" id="Seg_1918" n="e" s="T40">iʔ </ts>
               <ts e="T42" id="Seg_1920" n="e" s="T41">Filisnəj_jasnəj_săkolika </ts>
               <ts e="T43" id="Seg_1922" n="e" s="T42">pʼoruš". </ts>
               <ts e="T44" id="Seg_1924" n="e" s="T43">((BRK)). </ts>
               <ts e="T45" id="Seg_1926" n="e" s="T44">Dĭ </ts>
               <ts e="T46" id="Seg_1928" n="e" s="T45">măndə: </ts>
               <ts e="T47" id="Seg_1930" n="e" s="T46">"Iʔ </ts>
               <ts e="T48" id="Seg_1932" n="e" s="T47">măna </ts>
               <ts e="T49" id="Seg_1934" n="e" s="T48">Filišnəj_jasnəj </ts>
               <ts e="T50" id="Seg_1936" n="e" s="T49">pʼoruškă". </ts>
               <ts e="T51" id="Seg_1938" n="e" s="T50">Dĭ </ts>
               <ts e="T52" id="Seg_1940" n="e" s="T51">kambi, </ts>
               <ts e="T53" id="Seg_1942" n="e" s="T52">dĭʔnə </ts>
               <ts e="T54" id="Seg_1944" n="e" s="T53">ibi </ts>
               <ts e="T55" id="Seg_1946" n="e" s="T54">palʼto, </ts>
               <ts e="T56" id="Seg_1948" n="e" s="T55">dĭʔnə </ts>
               <ts e="T57" id="Seg_1950" n="e" s="T56">šalʼ </ts>
               <ts e="T58" id="Seg_1952" n="e" s="T57">ibi </ts>
               <ts e="T59" id="Seg_1954" n="e" s="T58">(i </ts>
               <ts e="T60" id="Seg_1956" n="e" s="T59">ku-) </ts>
               <ts e="T61" id="Seg_1958" n="e" s="T60">i </ts>
               <ts e="T62" id="Seg_1960" n="e" s="T61">platʼtʼa </ts>
               <ts e="T63" id="Seg_1962" n="e" s="T62">ibi. </ts>
               <ts e="T64" id="Seg_1964" n="e" s="T63">Măndərbi, </ts>
               <ts e="T65" id="Seg_1966" n="e" s="T64">măndərbi, </ts>
               <ts e="T66" id="Seg_1968" n="e" s="T65">ej </ts>
               <ts e="T67" id="Seg_1970" n="e" s="T66">kubi. </ts>
               <ts e="T68" id="Seg_1972" n="e" s="T67">Dĭgəttə </ts>
               <ts e="T69" id="Seg_1974" n="e" s="T68">šobi </ts>
               <ts e="T70" id="Seg_1976" n="e" s="T69">maːʔndə. </ts>
               <ts e="T71" id="Seg_1978" n="e" s="T70">(Dĭ) </ts>
               <ts e="T72" id="Seg_1980" n="e" s="T71">Dĭ </ts>
               <ts e="T73" id="Seg_1982" n="e" s="T72">šidö </ts>
               <ts e="T74" id="Seg_1984" n="e" s="T73">koʔbdo </ts>
               <ts e="T75" id="Seg_1986" n="e" s="T74">(kaknarlaʔbəʔjə). </ts>
               <ts e="T76" id="Seg_1988" n="e" s="T75">"Tănan </ts>
               <ts e="T77" id="Seg_1990" n="e" s="T76">ĭmbidə </ts>
               <ts e="T78" id="Seg_1992" n="e" s="T77">ej </ts>
               <ts e="T79" id="Seg_1994" n="e" s="T78">ibi, </ts>
               <ts e="T80" id="Seg_1996" n="e" s="T79">a </ts>
               <ts e="T81" id="Seg_1998" n="e" s="T80">miʔnʼibeʔ </ts>
               <ts e="T82" id="Seg_2000" n="e" s="T81">ibi". </ts>
               <ts e="T83" id="Seg_2002" n="e" s="T82">"Nu </ts>
               <ts e="T84" id="Seg_2004" n="e" s="T83">i </ts>
               <ts e="T85" id="Seg_2006" n="e" s="T84">što, </ts>
               <ts e="T86" id="Seg_2008" n="e" s="T85">ej </ts>
               <ts e="T87" id="Seg_2010" n="e" s="T86">ibi </ts>
               <ts e="T88" id="Seg_2012" n="e" s="T87">dak </ts>
               <ts e="T89" id="Seg_2014" n="e" s="T88">puskaj </ts>
               <ts e="T90" id="Seg_2016" n="e" s="T89">ej </ts>
               <ts e="T91" id="Seg_2018" n="e" s="T90">iləj". </ts>
               <ts e="T92" id="Seg_2020" n="e" s="T91">Dĭgəttə </ts>
               <ts e="T93" id="Seg_2022" n="e" s="T92">dĭ </ts>
               <ts e="T94" id="Seg_2024" n="e" s="T93">bazoʔ </ts>
               <ts e="T95" id="Seg_2026" n="e" s="T94">kambi, </ts>
               <ts e="T96" id="Seg_2028" n="e" s="T95">ipeksi. </ts>
               <ts e="T97" id="Seg_2030" n="e" s="T96">"Ĭmbi </ts>
               <ts e="T98" id="Seg_2032" n="e" s="T97">izittə?" </ts>
               <ts e="T99" id="Seg_2034" n="e" s="T98">Dĭgəttə </ts>
               <ts e="T100" id="Seg_2036" n="e" s="T99">dĭzeŋ </ts>
               <ts e="T101" id="Seg_2038" n="e" s="T100">măndəʔi: </ts>
               <ts e="T102" id="Seg_2040" n="e" s="T101">"Bătinkaʔi </ts>
               <ts e="T103" id="Seg_2042" n="e" s="T102">iʔ, </ts>
               <ts e="T104" id="Seg_2044" n="e" s="T103">kuvas". </ts>
               <ts e="T105" id="Seg_2046" n="e" s="T104">Dĭ </ts>
               <ts e="T106" id="Seg_2048" n="e" s="T105">kambi, </ts>
               <ts e="T107" id="Seg_2050" n="e" s="T106">dĭzeŋdə </ts>
               <ts e="T108" id="Seg_2052" n="e" s="T107">ibi. </ts>
               <ts e="T109" id="Seg_2054" n="e" s="T108">(A=) </ts>
               <ts e="T110" id="Seg_2056" n="e" s="T109">A </ts>
               <ts e="T111" id="Seg_2058" n="e" s="T110">üdʼüge </ts>
               <ts e="T112" id="Seg_2060" n="e" s="T111">măndə: </ts>
               <ts e="T113" id="Seg_2062" n="e" s="T112">"Măna </ts>
               <ts e="T114" id="Seg_2064" n="e" s="T113">iʔ </ts>
               <ts e="T115" id="Seg_2066" n="e" s="T114">Filisnəj_jasnəj_săkolika </ts>
               <ts e="T116" id="Seg_2068" n="e" s="T115">pʼoruškă". </ts>
               <ts e="T117" id="Seg_2070" n="e" s="T116">Dĭ </ts>
               <ts e="T118" id="Seg_2072" n="e" s="T117">kambi, </ts>
               <ts e="T119" id="Seg_2074" n="e" s="T118">(dĭzeŋdə) </ts>
               <ts e="T120" id="Seg_2076" n="e" s="T119">ibi. </ts>
               <ts e="T121" id="Seg_2078" n="e" s="T120">A </ts>
               <ts e="T122" id="Seg_2080" n="e" s="T121">dĭm </ts>
               <ts e="T123" id="Seg_2082" n="e" s="T122">ej </ts>
               <ts e="T124" id="Seg_2084" n="e" s="T123">kubi. </ts>
               <ts e="T125" id="Seg_2086" n="e" s="T124">Dĭgəttə </ts>
               <ts e="T126" id="Seg_2088" n="e" s="T125">nagurəŋ </ts>
               <ts e="T127" id="Seg_2090" n="e" s="T126">kambi. </ts>
               <ts e="T128" id="Seg_2092" n="e" s="T127">Dĭgəttə </ts>
               <ts e="T129" id="Seg_2094" n="e" s="T128">dĭzeŋdə </ts>
               <ts e="T130" id="Seg_2096" n="e" s="T129">ibi </ts>
               <ts e="T131" id="Seg_2098" n="e" s="T130">bar </ts>
               <ts e="T132" id="Seg_2100" n="e" s="T131">ĭmbi. </ts>
               <ts e="T133" id="Seg_2102" n="e" s="T132">A </ts>
               <ts e="T134" id="Seg_2104" n="e" s="T133">dĭʔnə </ts>
               <ts e="T135" id="Seg_2106" n="e" s="T134">kubi </ts>
               <ts e="T136" id="Seg_2108" n="e" s="T135">dĭ </ts>
               <ts e="T137" id="Seg_2110" n="e" s="T136">pʼoruškă </ts>
               <ts e="T138" id="Seg_2112" n="e" s="T137">i </ts>
               <ts e="T139" id="Seg_2114" n="e" s="T138">deʔpi. </ts>
               <ts e="T140" id="Seg_2116" n="e" s="T139">Dĭgəttə </ts>
               <ts e="T141" id="Seg_2118" n="e" s="T140">dĭ </ts>
               <ts e="T142" id="Seg_2120" n="e" s="T141">bostə </ts>
               <ts e="T143" id="Seg_2122" n="e" s="T142">(kien) </ts>
               <ts e="T144" id="Seg_2124" n="e" s="T143">kunolbi. </ts>
               <ts e="T145" id="Seg_2126" n="e" s="T144">Ibi </ts>
               <ts e="T146" id="Seg_2128" n="e" s="T145">dĭ </ts>
               <ts e="T147" id="Seg_2130" n="e" s="T146">pʼoruškă. </ts>
               <ts e="T148" id="Seg_2132" n="e" s="T147">(Bambi) </ts>
               <ts e="T149" id="Seg_2134" n="e" s="T148">udandə. </ts>
               <ts e="T150" id="Seg_2136" n="e" s="T149">Udandə </ts>
               <ts e="T151" id="Seg_2138" n="e" s="T150">(baʔluʔpi), </ts>
               <ts e="T152" id="Seg_2140" n="e" s="T151">dĭ </ts>
               <ts e="T153" id="Seg_2142" n="e" s="T152">bar </ts>
               <ts e="T154" id="Seg_2144" n="e" s="T153">nʼi </ts>
               <ts e="T155" id="Seg_2146" n="e" s="T154">moluʔpi, </ts>
               <ts e="T156" id="Seg_2148" n="e" s="T155">bar </ts>
               <ts e="T157" id="Seg_2150" n="e" s="T156">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T158" id="Seg_2152" n="e" s="T157">A </ts>
               <ts e="T159" id="Seg_2154" n="e" s="T158">dĭn </ts>
               <ts e="T160" id="Seg_2156" n="e" s="T159">sʼestrazaŋdə </ts>
               <ts e="T161" id="Seg_2158" n="e" s="T160">măndobiʔi, </ts>
               <ts e="T162" id="Seg_2160" n="e" s="T161">što </ts>
               <ts e="T163" id="Seg_2162" n="e" s="T162">dĭn </ts>
               <ts e="T164" id="Seg_2164" n="e" s="T163">nʼi </ts>
               <ts e="T165" id="Seg_2166" n="e" s="T164">ige. </ts>
               <ts e="T166" id="Seg_2168" n="e" s="T165">Dĭgəttə </ts>
               <ts e="T167" id="Seg_2170" n="e" s="T166">dĭ </ts>
               <ts e="T168" id="Seg_2172" n="e" s="T167">(dĭ-) </ts>
               <ts e="T169" id="Seg_2174" n="e" s="T168">dĭn </ts>
               <ts e="T170" id="Seg_2176" n="e" s="T169">kuznektə </ts>
               <ts e="T171" id="Seg_2178" n="e" s="T170">ibi </ts>
               <ts e="T172" id="Seg_2180" n="e" s="T171">üdʼüge. </ts>
               <ts e="T173" id="Seg_2182" n="e" s="T172">Dĭn </ts>
               <ts e="T174" id="Seg_2184" n="e" s="T173">bar </ts>
               <ts e="T175" id="Seg_2186" n="e" s="T174">dĭbər </ts>
               <ts e="T176" id="Seg_2188" n="e" s="T175">tagajʔi </ts>
               <ts e="T177" id="Seg_2190" n="e" s="T176">(n-) </ts>
               <ts e="T178" id="Seg_2192" n="e" s="T177">müʔtəbiʔi. </ts>
               <ts e="T179" id="Seg_2194" n="e" s="T178">A </ts>
               <ts e="T180" id="Seg_2196" n="e" s="T179">dĭm </ts>
               <ts e="T181" id="Seg_2198" n="e" s="T180">(bĭ-) </ts>
               <ts e="T182" id="Seg_2200" n="e" s="T181">bĭtəlbiʔi. </ts>
               <ts e="T183" id="Seg_2202" n="e" s="T182">Dĭrgit </ts>
               <ts e="T184" id="Seg_2204" n="e" s="T183">bü </ts>
               <ts e="T185" id="Seg_2206" n="e" s="T184">što </ts>
               <ts e="T186" id="Seg_2208" n="e" s="T185">kunolbi. </ts>
               <ts e="T187" id="Seg_2210" n="e" s="T186">Dĭ </ts>
               <ts e="T188" id="Seg_2212" n="e" s="T187">šobi. </ts>
               <ts e="T189" id="Seg_2214" n="e" s="T188">Kirgarbi, </ts>
               <ts e="T190" id="Seg_2216" n="e" s="T189">kirgarbi, </ts>
               <ts e="T191" id="Seg_2218" n="e" s="T190">üjüttə </ts>
               <ts e="T192" id="Seg_2220" n="e" s="T191">băʔpi. </ts>
               <ts e="T193" id="Seg_2222" n="e" s="T192">I </ts>
               <ts e="T453" id="Seg_2224" n="e" s="T193">kalla </ts>
               <ts e="T194" id="Seg_2226" n="e" s="T453">dʼürbi. </ts>
               <ts e="T195" id="Seg_2228" n="e" s="T194">((BRK)). </ts>
               <ts e="T196" id="Seg_2230" n="e" s="T195">Dĭn </ts>
               <ts e="T197" id="Seg_2232" n="e" s="T196">sʼestrazaŋdə </ts>
               <ts e="T198" id="Seg_2234" n="e" s="T197">dĭʔnə </ts>
               <ts e="T199" id="Seg_2236" n="e" s="T198">mĭbiʔi </ts>
               <ts e="T200" id="Seg_2238" n="e" s="T199">kaplʼaʔi, </ts>
               <ts e="T201" id="Seg_2240" n="e" s="T200">štobɨ </ts>
               <ts e="T202" id="Seg_2242" n="e" s="T201">dĭ </ts>
               <ts e="T203" id="Seg_2244" n="e" s="T202">kunolluʔpi. </ts>
               <ts e="T204" id="Seg_2246" n="e" s="T203">Dĭ </ts>
               <ts e="T205" id="Seg_2248" n="e" s="T204">bar </ts>
               <ts e="T206" id="Seg_2250" n="e" s="T205">kunolluʔpi. </ts>
               <ts e="T207" id="Seg_2252" n="e" s="T206">A </ts>
               <ts e="T208" id="Seg_2254" n="e" s="T207">dĭ </ts>
               <ts e="T209" id="Seg_2256" n="e" s="T208">šobi. </ts>
               <ts e="T210" id="Seg_2258" n="e" s="T209">Kuznektə </ts>
               <ts e="T211" id="Seg_2260" n="e" s="T210">dĭzeŋ </ts>
               <ts e="T212" id="Seg_2262" n="e" s="T211">dĭn </ts>
               <ts e="T213" id="Seg_2264" n="e" s="T212">müʔpiʔi </ts>
               <ts e="T214" id="Seg_2266" n="e" s="T213">tagajʔi. </ts>
               <ts e="T215" id="Seg_2268" n="e" s="T214">Dĭ </ts>
               <ts e="T216" id="Seg_2270" n="e" s="T215">bar </ts>
               <ts e="T217" id="Seg_2272" n="e" s="T216">šüzittə </ts>
               <ts e="T218" id="Seg_2274" n="e" s="T217">xatʼel. </ts>
               <ts e="T219" id="Seg_2276" n="e" s="T218">Üjübə </ts>
               <ts e="T220" id="Seg_2278" n="e" s="T219">bar </ts>
               <ts e="T221" id="Seg_2280" n="e" s="T220">bătluʔpi. </ts>
               <ts e="T222" id="Seg_2282" n="e" s="T221">Kem </ts>
               <ts e="T223" id="Seg_2284" n="e" s="T222">mʼaŋnuʔpi. </ts>
               <ts e="T224" id="Seg_2286" n="e" s="T223">Dĭgəttə </ts>
               <ts e="T225" id="Seg_2288" n="e" s="T224">sazən </ts>
               <ts e="T226" id="Seg_2290" n="e" s="T225">pʼaŋdəbi. </ts>
               <ts e="T227" id="Seg_2292" n="e" s="T226">I </ts>
               <ts e="T228" id="Seg_2294" n="e" s="T227">(baʔ-) </ts>
               <ts e="T229" id="Seg_2296" n="e" s="T228">embi: </ts>
               <ts e="T230" id="Seg_2298" n="e" s="T229">"Kamen </ts>
               <ts e="T231" id="Seg_2300" n="e" s="T230">bazaj </ts>
               <ts e="T232" id="Seg_2302" n="e" s="T231">jamaʔi </ts>
               <ts e="T233" id="Seg_2304" n="e" s="T232">aləl, </ts>
               <ts e="T234" id="Seg_2306" n="e" s="T233">iʔtləl, </ts>
               <ts e="T235" id="Seg_2308" n="e" s="T234">dĭgəttə </ts>
               <ts e="T236" id="Seg_2310" n="e" s="T235">măna </ts>
               <ts e="T237" id="Seg_2312" n="e" s="T236">kulal. </ts>
               <ts e="T238" id="Seg_2314" n="e" s="T237">((BRK)). </ts>
               <ts e="T239" id="Seg_2316" n="e" s="T238">Dĭgəttə </ts>
               <ts e="T240" id="Seg_2318" n="e" s="T239">dĭ </ts>
               <ts e="T241" id="Seg_2320" n="e" s="T240">uʔbdəbi. </ts>
               <ts e="T242" id="Seg_2322" n="e" s="T241">Kuiot: </ts>
               <ts e="T243" id="Seg_2324" n="e" s="T242">bar </ts>
               <ts e="T244" id="Seg_2326" n="e" s="T243">kem </ts>
               <ts e="T245" id="Seg_2328" n="e" s="T244">dön. </ts>
               <ts e="T246" id="Seg_2330" n="e" s="T245">I </ts>
               <ts e="T247" id="Seg_2332" n="e" s="T246">sazən </ts>
               <ts e="T248" id="Seg_2334" n="e" s="T247">iʔbolaʔbə. </ts>
               <ts e="T249" id="Seg_2336" n="e" s="T248">Dĭ </ts>
               <ts e="T250" id="Seg_2338" n="e" s="T249">sazən </ts>
               <ts e="T251" id="Seg_2340" n="e" s="T250">măndəbi, </ts>
               <ts e="T252" id="Seg_2342" n="e" s="T251">bar </ts>
               <ts e="T253" id="Seg_2344" n="e" s="T252">dʼorbi, </ts>
               <ts e="T254" id="Seg_2346" n="e" s="T253">dʼorbi. </ts>
               <ts e="T255" id="Seg_2348" n="e" s="T254">Da </ts>
               <ts e="T256" id="Seg_2350" n="e" s="T255">kambi </ts>
               <ts e="T257" id="Seg_2352" n="e" s="T256">kuznʼitsanə. </ts>
               <ts e="T258" id="Seg_2354" n="e" s="T257">Jamaʔi </ts>
               <ts e="T259" id="Seg_2356" n="e" s="T258">abi </ts>
               <ts e="T260" id="Seg_2358" n="e" s="T259">bazaj, </ts>
               <ts e="T261" id="Seg_2360" n="e" s="T260">pa </ts>
               <ts e="T262" id="Seg_2362" n="e" s="T261">abi </ts>
               <ts e="T263" id="Seg_2364" n="e" s="T262">bazaj. </ts>
               <ts e="T264" id="Seg_2366" n="e" s="T263">Dĭgəttə </ts>
               <ts e="T265" id="Seg_2368" n="e" s="T264">kambi. </ts>
               <ts e="T266" id="Seg_2370" n="e" s="T265">Kambi, </ts>
               <ts e="T267" id="Seg_2372" n="e" s="T266">kambi, </ts>
               <ts e="T268" id="Seg_2374" n="e" s="T267">kundʼom </ts>
               <ts e="T269" id="Seg_2376" n="e" s="T268">(šonəga=) </ts>
               <ts e="T270" id="Seg_2378" n="e" s="T269">šonəbi. </ts>
               <ts e="T271" id="Seg_2380" n="e" s="T270">Dĭgəttə </ts>
               <ts e="T272" id="Seg_2382" n="e" s="T271">(kuiot=) </ts>
               <ts e="T273" id="Seg_2384" n="e" s="T272">kulaʔbə: </ts>
               <ts e="T274" id="Seg_2386" n="e" s="T273">tura </ts>
               <ts e="T275" id="Seg_2388" n="e" s="T274">(i-) </ts>
               <ts e="T276" id="Seg_2390" n="e" s="T275">nuga </ts>
               <ts e="T277" id="Seg_2392" n="e" s="T276">(idʼiʔeje). </ts>
               <ts e="T278" id="Seg_2394" n="e" s="T277">Dĭ </ts>
               <ts e="T279" id="Seg_2396" n="e" s="T278">turanə </ts>
               <ts e="T280" id="Seg_2398" n="e" s="T279">šobi. </ts>
               <ts e="T281" id="Seg_2400" n="e" s="T280">Dĭn </ts>
               <ts e="T282" id="Seg_2402" n="e" s="T281">nüke </ts>
               <ts e="T283" id="Seg_2404" n="e" s="T282">amnolaʔbə. </ts>
               <ts e="T284" id="Seg_2406" n="e" s="T283">Dĭ </ts>
               <ts e="T285" id="Seg_2408" n="e" s="T284">surarlaʔbə: </ts>
               <ts e="T286" id="Seg_2410" n="e" s="T285">"Nüke, </ts>
               <ts e="T287" id="Seg_2412" n="e" s="T286">nüke, </ts>
               <ts e="T288" id="Seg_2414" n="e" s="T287">nörbit, </ts>
               <ts e="T289" id="Seg_2416" n="e" s="T288">gijen </ts>
               <ts e="T290" id="Seg_2418" n="e" s="T289">măn </ts>
               <ts e="T291" id="Seg_2420" n="e" s="T290">tibim </ts>
               <ts e="T454" id="Seg_2422" n="e" s="T291">kalla </ts>
               <ts e="T292" id="Seg_2424" n="e" s="T454">dʼürbi?" </ts>
               <ts e="T293" id="Seg_2426" n="e" s="T292">Dĭ </ts>
               <ts e="T294" id="Seg_2428" n="e" s="T293">măndə: </ts>
               <ts e="T295" id="Seg_2430" n="e" s="T294">"Măn </ts>
               <ts e="T296" id="Seg_2432" n="e" s="T295">ej </ts>
               <ts e="T297" id="Seg_2434" n="e" s="T296">tĭmnem, </ts>
               <ts e="T298" id="Seg_2436" n="e" s="T297">gijen </ts>
               <ts e="T299" id="Seg_2438" n="e" s="T298">(dʼüʔ-), </ts>
               <ts e="T300" id="Seg_2440" n="e" s="T299">gijendə. </ts>
               <ts e="T301" id="Seg_2442" n="e" s="T300">Kanaʔ </ts>
               <ts e="T302" id="Seg_2444" n="e" s="T301">măn </ts>
               <ts e="T303" id="Seg_2446" n="e" s="T302">sʼestrandə. </ts>
               <ts e="T304" id="Seg_2448" n="e" s="T303">Dĭ </ts>
               <ts e="T305" id="Seg_2450" n="e" s="T304">tĭmnet. </ts>
               <ts e="T306" id="Seg_2452" n="e" s="T305">Nörbələj </ts>
               <ts e="T307" id="Seg_2454" n="e" s="T306">tănan". </ts>
               <ts e="T308" id="Seg_2456" n="e" s="T307">Dĭgəttə </ts>
               <ts e="T309" id="Seg_2458" n="e" s="T308">dĭʔnə </ts>
               <ts e="T310" id="Seg_2460" n="e" s="T309">mĭbi </ts>
               <ts e="T311" id="Seg_2462" n="e" s="T310">takše </ts>
               <ts e="T312" id="Seg_2464" n="e" s="T311">zălătoj, </ts>
               <ts e="T313" id="Seg_2466" n="e" s="T312">munəjʔ </ts>
               <ts e="T314" id="Seg_2468" n="e" s="T313">zălătoj. </ts>
               <ts e="T315" id="Seg_2470" n="e" s="T314">I </ts>
               <ts e="T316" id="Seg_2472" n="e" s="T315">nʼitkaʔi </ts>
               <ts e="T317" id="Seg_2474" n="e" s="T316">klubok. </ts>
               <ts e="T318" id="Seg_2476" n="e" s="T317">"Kanaʔ, </ts>
               <ts e="T319" id="Seg_2478" n="e" s="T318">dĭ </ts>
               <ts e="T320" id="Seg_2480" n="e" s="T319">nuʔmələj </ts>
               <ts e="T321" id="Seg_2482" n="e" s="T320">i </ts>
               <ts e="T322" id="Seg_2484" n="e" s="T321">tăn </ts>
               <ts e="T323" id="Seg_2486" n="e" s="T322">kanaʔ". </ts>
               <ts e="T324" id="Seg_2488" n="e" s="T323">Dĭgəttə </ts>
               <ts e="T325" id="Seg_2490" n="e" s="T324">dĭ </ts>
               <ts e="T326" id="Seg_2492" n="e" s="T325">kambi. </ts>
               <ts e="T327" id="Seg_2494" n="e" s="T326">((BRK)). </ts>
               <ts e="T328" id="Seg_2496" n="e" s="T327">Dĭgəttə </ts>
               <ts e="T329" id="Seg_2498" n="e" s="T328">dĭ </ts>
               <ts e="T330" id="Seg_2500" n="e" s="T329">kambi, </ts>
               <ts e="T331" id="Seg_2502" n="e" s="T330">kambi. </ts>
               <ts e="T332" id="Seg_2504" n="e" s="T331">Baška </ts>
               <ts e="T333" id="Seg_2506" n="e" s="T332">sʼestrandə </ts>
               <ts e="T334" id="Seg_2508" n="e" s="T333">šobi. </ts>
               <ts e="T335" id="Seg_2510" n="e" s="T334">Tože </ts>
               <ts e="T336" id="Seg_2512" n="e" s="T335">(turat) </ts>
               <ts e="T337" id="Seg_2514" n="e" s="T336">(ej </ts>
               <ts e="T338" id="Seg_2516" n="e" s="T337">üdʼü- </ts>
               <ts e="T339" id="Seg_2518" n="e" s="T338">ej-) </ts>
               <ts e="T340" id="Seg_2520" n="e" s="T339">üdʼüge </ts>
               <ts e="T341" id="Seg_2522" n="e" s="T340">turat. </ts>
               <ts e="T342" id="Seg_2524" n="e" s="T341">Šübi </ts>
               <ts e="T343" id="Seg_2526" n="e" s="T342">turanə. </ts>
               <ts e="T344" id="Seg_2528" n="e" s="T343">(Măndə-, </ts>
               <ts e="T345" id="Seg_2530" n="e" s="T344">dĭn=) </ts>
               <ts e="T346" id="Seg_2532" n="e" s="T345">Dĭn </ts>
               <ts e="T347" id="Seg_2534" n="e" s="T346">nüke </ts>
               <ts e="T348" id="Seg_2536" n="e" s="T347">amnolaʔbə. </ts>
               <ts e="T349" id="Seg_2538" n="e" s="T348">Măndə: </ts>
               <ts e="T350" id="Seg_2540" n="e" s="T349">"Nüke, </ts>
               <ts e="T351" id="Seg_2542" n="e" s="T350">nüke, </ts>
               <ts e="T352" id="Seg_2544" n="e" s="T351">nörbeʔ </ts>
               <ts e="T353" id="Seg_2546" n="e" s="T352">măna, </ts>
               <ts e="T354" id="Seg_2548" n="e" s="T353">gijen </ts>
               <ts e="T355" id="Seg_2550" n="e" s="T354">măn </ts>
               <ts e="T356" id="Seg_2552" n="e" s="T355">tibi </ts>
               <ts e="T455" id="Seg_2554" n="e" s="T356">kalla </ts>
               <ts e="T357" id="Seg_2556" n="e" s="T455">dʼürbi?" </ts>
               <ts e="T358" id="Seg_2558" n="e" s="T357">"Măn </ts>
               <ts e="T359" id="Seg_2560" n="e" s="T358">ej </ts>
               <ts e="T360" id="Seg_2562" n="e" s="T359">tĭmnem. </ts>
               <ts e="T361" id="Seg_2564" n="e" s="T360">Kanaʔ </ts>
               <ts e="T362" id="Seg_2566" n="e" s="T361">măn </ts>
               <ts e="T363" id="Seg_2568" n="e" s="T362">sʼestrandə, </ts>
               <ts e="T364" id="Seg_2570" n="e" s="T363">dĭ </ts>
               <ts e="T365" id="Seg_2572" n="e" s="T364">nörbələj, </ts>
               <ts e="T366" id="Seg_2574" n="e" s="T365">gijen </ts>
               <ts e="T367" id="Seg_2576" n="e" s="T366">dĭ". </ts>
               <ts e="T368" id="Seg_2578" n="e" s="T367">Dĭ </ts>
               <ts e="T369" id="Seg_2580" n="e" s="T368">dĭʔnə </ts>
               <ts e="T370" id="Seg_2582" n="e" s="T369">(mĭmbi=) </ts>
               <ts e="T371" id="Seg_2584" n="e" s="T370">mĭbi </ts>
               <ts e="T372" id="Seg_2586" n="e" s="T371">prʼalka. </ts>
               <ts e="T373" id="Seg_2588" n="e" s="T372">I </ts>
               <ts e="T374" id="Seg_2590" n="e" s="T373">mĭbi </ts>
               <ts e="T375" id="Seg_2592" n="e" s="T374">kugʼelʼi. </ts>
               <ts e="T376" id="Seg_2594" n="e" s="T375">Prʼalka </ts>
               <ts e="T377" id="Seg_2596" n="e" s="T376">bar </ts>
               <ts e="T378" id="Seg_2598" n="e" s="T377">zălătoj, </ts>
               <ts e="T379" id="Seg_2600" n="e" s="T378">i </ts>
               <ts e="T380" id="Seg_2602" n="e" s="T379">kugʼelʼi </ts>
               <ts e="T381" id="Seg_2604" n="e" s="T380">zălătoj. </ts>
               <ts e="T382" id="Seg_2606" n="e" s="T381">Dĭgəttə </ts>
               <ts e="T383" id="Seg_2608" n="e" s="T382">(mĭ-) </ts>
               <ts e="T384" id="Seg_2610" n="e" s="T383">mĭbi </ts>
               <ts e="T385" id="Seg_2612" n="e" s="T384">klubok. </ts>
               <ts e="T386" id="Seg_2614" n="e" s="T385">"Dĭ </ts>
               <ts e="T387" id="Seg_2616" n="e" s="T386">(kandlo-) </ts>
               <ts e="T388" id="Seg_2618" n="e" s="T387">kandluj, </ts>
               <ts e="T389" id="Seg_2620" n="e" s="T388">tăn </ts>
               <ts e="T390" id="Seg_2622" n="e" s="T389">(u-) </ts>
               <ts e="T391" id="Seg_2624" n="e" s="T390">kanaʔ </ts>
               <ts e="T392" id="Seg_2626" n="e" s="T391">dĭbər, </ts>
               <ts e="T393" id="Seg_2628" n="e" s="T392">gibər </ts>
               <ts e="T394" id="Seg_2630" n="e" s="T393">dĭ </ts>
               <ts e="T395" id="Seg_2632" n="e" s="T394">kandluj". </ts>
               <ts e="T396" id="Seg_2634" n="e" s="T395">Dĭgəttə </ts>
               <ts e="T397" id="Seg_2636" n="e" s="T396">dö </ts>
               <ts e="T398" id="Seg_2638" n="e" s="T397">kambi, </ts>
               <ts e="T399" id="Seg_2640" n="e" s="T398">kambi. </ts>
               <ts e="T400" id="Seg_2642" n="e" s="T399">Nagur </ts>
               <ts e="T401" id="Seg_2644" n="e" s="T400">sʼestrandə </ts>
               <ts e="T402" id="Seg_2646" n="e" s="T401">šobi, </ts>
               <ts e="T403" id="Seg_2648" n="e" s="T402">tože </ts>
               <ts e="T404" id="Seg_2650" n="e" s="T403">tura </ts>
               <ts e="T405" id="Seg_2652" n="e" s="T404">nulaʔbə. </ts>
               <ts e="T406" id="Seg_2654" n="e" s="T405">Dĭ </ts>
               <ts e="T407" id="Seg_2656" n="e" s="T406">šobi. </ts>
               <ts e="T408" id="Seg_2658" n="e" s="T407">Dĭn </ts>
               <ts e="T409" id="Seg_2660" n="e" s="T408">nüke </ts>
               <ts e="T410" id="Seg_2662" n="e" s="T409">amnolaʔbə. </ts>
               <ts e="T411" id="Seg_2664" n="e" s="T410">(ugandə). </ts>
               <ts e="T412" id="Seg_2666" n="e" s="T411">Dĭgəttə </ts>
               <ts e="T413" id="Seg_2668" n="e" s="T412">măndə: </ts>
               <ts e="T414" id="Seg_2670" n="e" s="T413">"Nüke, </ts>
               <ts e="T415" id="Seg_2672" n="e" s="T414">nüke, </ts>
               <ts e="T416" id="Seg_2674" n="e" s="T415">nörbeʔ </ts>
               <ts e="T417" id="Seg_2676" n="e" s="T416">măna, </ts>
               <ts e="T418" id="Seg_2678" n="e" s="T417">gijən </ts>
               <ts e="T419" id="Seg_2680" n="e" s="T418">măn </ts>
               <ts e="T420" id="Seg_2682" n="e" s="T419">tibim </ts>
               <ts e="T456" id="Seg_2684" n="e" s="T420">kalla </ts>
               <ts e="T421" id="Seg_2686" n="e" s="T456">dʼürbi?" </ts>
               <ts e="T422" id="Seg_2688" n="e" s="T421">"Măn </ts>
               <ts e="T423" id="Seg_2690" n="e" s="T422">tĭmnem. </ts>
               <ts e="T424" id="Seg_2692" n="e" s="T423">Kanaʔ". </ts>
               <ts e="T425" id="Seg_2694" n="e" s="T424">Dĭgəttə </ts>
               <ts e="T426" id="Seg_2696" n="e" s="T425">dĭ </ts>
               <ts e="T427" id="Seg_2698" n="e" s="T426">mĭbi </ts>
               <ts e="T428" id="Seg_2700" n="e" s="T427">dĭʔnə </ts>
               <ts e="T429" id="Seg_2702" n="e" s="T428">(korniš) </ts>
               <ts e="T430" id="Seg_2704" n="e" s="T429">kuvas, </ts>
               <ts e="T431" id="Seg_2706" n="e" s="T430">zălătoj </ts>
               <ts e="T432" id="Seg_2708" n="e" s="T431">bar. </ts>
               <ts e="T433" id="Seg_2710" n="e" s="T432">(Igol'-) </ts>
               <ts e="T434" id="Seg_2712" n="e" s="T433">Kălʼečkă </ts>
               <ts e="T435" id="Seg_2714" n="e" s="T434">mĭbi, </ts>
               <ts e="T436" id="Seg_2716" n="e" s="T435">zălătoj </ts>
               <ts e="T437" id="Seg_2718" n="e" s="T436">udandə. </ts>
               <ts e="T438" id="Seg_2720" n="e" s="T437">"Kanaʔ. </ts>
               <ts e="T439" id="Seg_2722" n="e" s="T438">Gijendə </ts>
               <ts e="T440" id="Seg_2724" n="e" s="T439">amnolaʔbə". </ts>
               <ts e="T441" id="Seg_2726" n="e" s="T440">Dĭʔnə </ts>
               <ts e="T442" id="Seg_2728" n="e" s="T441">mĭbi </ts>
               <ts e="T443" id="Seg_2730" n="e" s="T442">tože </ts>
               <ts e="T444" id="Seg_2732" n="e" s="T443">(k-) </ts>
               <ts e="T445" id="Seg_2734" n="e" s="T444">klubok. </ts>
               <ts e="T446" id="Seg_2736" n="e" s="T445">"Gibər </ts>
               <ts e="T447" id="Seg_2738" n="e" s="T446">dĭ </ts>
               <ts e="T448" id="Seg_2740" n="e" s="T447">kandləj, </ts>
               <ts e="T449" id="Seg_2742" n="e" s="T448">tăn </ts>
               <ts e="T450" id="Seg_2744" n="e" s="T449">dĭbər </ts>
               <ts e="T451" id="Seg_2746" n="e" s="T450">kanaʔ". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2747" s="T0">PKZ_196X_Finist_flk.001 (001)</ta>
            <ta e="T7" id="Seg_2748" s="T3">PKZ_196X_Finist_flk.002 (002)</ta>
            <ta e="T12" id="Seg_2749" s="T7">PKZ_196X_Finist_flk.003 (003)</ta>
            <ta e="T15" id="Seg_2750" s="T12">PKZ_196X_Finist_flk.004 (004)</ta>
            <ta e="T21" id="Seg_2751" s="T15">PKZ_196X_Finist_flk.005 (005)</ta>
            <ta e="T32" id="Seg_2752" s="T21">PKZ_196X_Finist_flk.006 (006)</ta>
            <ta e="T39" id="Seg_2753" s="T32">PKZ_196X_Finist_flk.007 (007)</ta>
            <ta e="T43" id="Seg_2754" s="T39">PKZ_196X_Finist_flk.008 (008)</ta>
            <ta e="T44" id="Seg_2755" s="T43">PKZ_196X_Finist_flk.009 (009)</ta>
            <ta e="T50" id="Seg_2756" s="T44">PKZ_196X_Finist_flk.010 (010)</ta>
            <ta e="T63" id="Seg_2757" s="T50">PKZ_196X_Finist_flk.011 (011)</ta>
            <ta e="T67" id="Seg_2758" s="T63">PKZ_196X_Finist_flk.012 (012)</ta>
            <ta e="T70" id="Seg_2759" s="T67">PKZ_196X_Finist_flk.013 (013)</ta>
            <ta e="T75" id="Seg_2760" s="T70">PKZ_196X_Finist_flk.014 (014)</ta>
            <ta e="T82" id="Seg_2761" s="T75">PKZ_196X_Finist_flk.015 (015)</ta>
            <ta e="T91" id="Seg_2762" s="T82">PKZ_196X_Finist_flk.016 (016)</ta>
            <ta e="T96" id="Seg_2763" s="T91">PKZ_196X_Finist_flk.017 (017)</ta>
            <ta e="T98" id="Seg_2764" s="T96">PKZ_196X_Finist_flk.018 (018)</ta>
            <ta e="T104" id="Seg_2765" s="T98">PKZ_196X_Finist_flk.019 (019) </ta>
            <ta e="T108" id="Seg_2766" s="T104">PKZ_196X_Finist_flk.020 (021)</ta>
            <ta e="T116" id="Seg_2767" s="T108">PKZ_196X_Finist_flk.021 (022)</ta>
            <ta e="T120" id="Seg_2768" s="T116">PKZ_196X_Finist_flk.022 (023)</ta>
            <ta e="T124" id="Seg_2769" s="T120">PKZ_196X_Finist_flk.023 (024)</ta>
            <ta e="T127" id="Seg_2770" s="T124">PKZ_196X_Finist_flk.024 (025)</ta>
            <ta e="T132" id="Seg_2771" s="T127">PKZ_196X_Finist_flk.025 (026)</ta>
            <ta e="T139" id="Seg_2772" s="T132">PKZ_196X_Finist_flk.026 (027)</ta>
            <ta e="T144" id="Seg_2773" s="T139">PKZ_196X_Finist_flk.027 (028)</ta>
            <ta e="T147" id="Seg_2774" s="T144">PKZ_196X_Finist_flk.028 (029)</ta>
            <ta e="T149" id="Seg_2775" s="T147">PKZ_196X_Finist_flk.029 (030)</ta>
            <ta e="T157" id="Seg_2776" s="T149">PKZ_196X_Finist_flk.030 (031)</ta>
            <ta e="T165" id="Seg_2777" s="T157">PKZ_196X_Finist_flk.031 (032)</ta>
            <ta e="T172" id="Seg_2778" s="T165">PKZ_196X_Finist_flk.032 (033)</ta>
            <ta e="T178" id="Seg_2779" s="T172">PKZ_196X_Finist_flk.033 (034)</ta>
            <ta e="T182" id="Seg_2780" s="T178">PKZ_196X_Finist_flk.034 (035)</ta>
            <ta e="T186" id="Seg_2781" s="T182">PKZ_196X_Finist_flk.035 (036)</ta>
            <ta e="T188" id="Seg_2782" s="T186">PKZ_196X_Finist_flk.036 (037)</ta>
            <ta e="T192" id="Seg_2783" s="T188">PKZ_196X_Finist_flk.037 (038)</ta>
            <ta e="T194" id="Seg_2784" s="T192">PKZ_196X_Finist_flk.038 (039)</ta>
            <ta e="T195" id="Seg_2785" s="T194">PKZ_196X_Finist_flk.039 (040)</ta>
            <ta e="T203" id="Seg_2786" s="T195">PKZ_196X_Finist_flk.040 (041)</ta>
            <ta e="T206" id="Seg_2787" s="T203">PKZ_196X_Finist_flk.041 (042)</ta>
            <ta e="T209" id="Seg_2788" s="T206">PKZ_196X_Finist_flk.042 (043)</ta>
            <ta e="T214" id="Seg_2789" s="T209">PKZ_196X_Finist_flk.043 (044)</ta>
            <ta e="T218" id="Seg_2790" s="T214">PKZ_196X_Finist_flk.044 (045)</ta>
            <ta e="T221" id="Seg_2791" s="T218">PKZ_196X_Finist_flk.045 (046)</ta>
            <ta e="T223" id="Seg_2792" s="T221">PKZ_196X_Finist_flk.046 (047)</ta>
            <ta e="T226" id="Seg_2793" s="T223">PKZ_196X_Finist_flk.047 (048)</ta>
            <ta e="T237" id="Seg_2794" s="T226">PKZ_196X_Finist_flk.048 (049)</ta>
            <ta e="T238" id="Seg_2795" s="T237">PKZ_196X_Finist_flk.049 (050)</ta>
            <ta e="T241" id="Seg_2796" s="T238">PKZ_196X_Finist_flk.050 (051)</ta>
            <ta e="T245" id="Seg_2797" s="T241">PKZ_196X_Finist_flk.051 (052)</ta>
            <ta e="T248" id="Seg_2798" s="T245">PKZ_196X_Finist_flk.052 (053)</ta>
            <ta e="T254" id="Seg_2799" s="T248">PKZ_196X_Finist_flk.053 (054)</ta>
            <ta e="T257" id="Seg_2800" s="T254">PKZ_196X_Finist_flk.054 (055)</ta>
            <ta e="T263" id="Seg_2801" s="T257">PKZ_196X_Finist_flk.055 (056)</ta>
            <ta e="T265" id="Seg_2802" s="T263">PKZ_196X_Finist_flk.056 (057)</ta>
            <ta e="T270" id="Seg_2803" s="T265">PKZ_196X_Finist_flk.057 (058)</ta>
            <ta e="T277" id="Seg_2804" s="T270">PKZ_196X_Finist_flk.058 (059)</ta>
            <ta e="T280" id="Seg_2805" s="T277">PKZ_196X_Finist_flk.059 (060)</ta>
            <ta e="T283" id="Seg_2806" s="T280">PKZ_196X_Finist_flk.060 (061)</ta>
            <ta e="T292" id="Seg_2807" s="T283">PKZ_196X_Finist_flk.061 (062)</ta>
            <ta e="T300" id="Seg_2808" s="T292">PKZ_196X_Finist_flk.062 (063)</ta>
            <ta e="T303" id="Seg_2809" s="T300">PKZ_196X_Finist_flk.063 (064)</ta>
            <ta e="T305" id="Seg_2810" s="T303">PKZ_196X_Finist_flk.064 (065)</ta>
            <ta e="T307" id="Seg_2811" s="T305">PKZ_196X_Finist_flk.065 (066)</ta>
            <ta e="T314" id="Seg_2812" s="T307">PKZ_196X_Finist_flk.066 (067)</ta>
            <ta e="T317" id="Seg_2813" s="T314">PKZ_196X_Finist_flk.067 (068)</ta>
            <ta e="T323" id="Seg_2814" s="T317">PKZ_196X_Finist_flk.068 (069)</ta>
            <ta e="T326" id="Seg_2815" s="T323">PKZ_196X_Finist_flk.069 (070)</ta>
            <ta e="T327" id="Seg_2816" s="T326">PKZ_196X_Finist_flk.070 (071)</ta>
            <ta e="T331" id="Seg_2817" s="T327">PKZ_196X_Finist_flk.071 (072)</ta>
            <ta e="T334" id="Seg_2818" s="T331">PKZ_196X_Finist_flk.072 (073)</ta>
            <ta e="T341" id="Seg_2819" s="T334">PKZ_196X_Finist_flk.073 (074)</ta>
            <ta e="T343" id="Seg_2820" s="T341">PKZ_196X_Finist_flk.074 (075)</ta>
            <ta e="T348" id="Seg_2821" s="T343">PKZ_196X_Finist_flk.075 (076.001)</ta>
            <ta e="T357" id="Seg_2822" s="T348">PKZ_196X_Finist_flk.076 (076.002) </ta>
            <ta e="T360" id="Seg_2823" s="T357">PKZ_196X_Finist_flk.077 (078)</ta>
            <ta e="T367" id="Seg_2824" s="T360">PKZ_196X_Finist_flk.078 (079)</ta>
            <ta e="T372" id="Seg_2825" s="T367">PKZ_196X_Finist_flk.079 (080)</ta>
            <ta e="T375" id="Seg_2826" s="T372">PKZ_196X_Finist_flk.080 (081)</ta>
            <ta e="T381" id="Seg_2827" s="T375">PKZ_196X_Finist_flk.081 (082)</ta>
            <ta e="T385" id="Seg_2828" s="T381">PKZ_196X_Finist_flk.082 (083)</ta>
            <ta e="T395" id="Seg_2829" s="T385">PKZ_196X_Finist_flk.083 (084)</ta>
            <ta e="T399" id="Seg_2830" s="T395">PKZ_196X_Finist_flk.084 (085)</ta>
            <ta e="T405" id="Seg_2831" s="T399">PKZ_196X_Finist_flk.085 (086)</ta>
            <ta e="T407" id="Seg_2832" s="T405">PKZ_196X_Finist_flk.086 (087)</ta>
            <ta e="T410" id="Seg_2833" s="T407">PKZ_196X_Finist_flk.087 (088)</ta>
            <ta e="T411" id="Seg_2834" s="T410">PKZ_196X_Finist_flk.088 (089)</ta>
            <ta e="T421" id="Seg_2835" s="T411">PKZ_196X_Finist_flk.089 (090)</ta>
            <ta e="T423" id="Seg_2836" s="T421">PKZ_196X_Finist_flk.090 (091)</ta>
            <ta e="T424" id="Seg_2837" s="T423">PKZ_196X_Finist_flk.091 (092)</ta>
            <ta e="T432" id="Seg_2838" s="T424">PKZ_196X_Finist_flk.092 (093)</ta>
            <ta e="T437" id="Seg_2839" s="T432">PKZ_196X_Finist_flk.093 (094)</ta>
            <ta e="T438" id="Seg_2840" s="T437">PKZ_196X_Finist_flk.094 (095)</ta>
            <ta e="T440" id="Seg_2841" s="T438">PKZ_196X_Finist_flk.095 (096)</ta>
            <ta e="T445" id="Seg_2842" s="T440">PKZ_196X_Finist_flk.096 (097)</ta>
            <ta e="T451" id="Seg_2843" s="T445">PKZ_196X_Finist_flk.097 (098)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2844" s="T0">Onʼiʔ büzʼe amnobi. </ta>
            <ta e="T7" id="Seg_2845" s="T3">Dĭn nagur koʔbdo ibi. </ta>
            <ta e="T12" id="Seg_2846" s="T7">Dĭ (ka-) kandəga ipek sadarzittə. </ta>
            <ta e="T15" id="Seg_2847" s="T12">"Ĭmbi šiʔnʼileʔ izittə?" </ta>
            <ta e="T21" id="Seg_2848" s="T15">Onʼiʔ măndə: "Măna iʔ kuvas palʼto". </ta>
            <ta e="T32" id="Seg_2849" s="T21">Onʼiʔ măndə: "Măna kuvas šalʼ (ib-) iʔ, i kuvas platʼtʼa iʔ". </ta>
            <ta e="T39" id="Seg_2850" s="T32">A üdʼüge samăjə: "A tănan ĭmbi izittə?" </ta>
            <ta e="T43" id="Seg_2851" s="T39">"Măna iʔ Filisnəj jasnəj săkolika pʼoruš". </ta>
            <ta e="T44" id="Seg_2852" s="T43">((BRK)). </ta>
            <ta e="T50" id="Seg_2853" s="T44">Dĭ măndə: "Iʔ măna Filišnəj jasnəj pʼoruškă". </ta>
            <ta e="T63" id="Seg_2854" s="T50">Dĭ kambi, dĭʔnə ibi palʼto, dĭʔnə šalʼ ibi (i ku-) i platʼtʼa ibi. </ta>
            <ta e="T67" id="Seg_2855" s="T63">Măndərbi, măndərbi, ej kubi. </ta>
            <ta e="T70" id="Seg_2856" s="T67">Dĭgəttə šobi maːʔndə. </ta>
            <ta e="T75" id="Seg_2857" s="T70">(Dĭ) Dĭ šidö koʔbdo (kaknarlaʔbəʔjə). </ta>
            <ta e="T82" id="Seg_2858" s="T75">"Tănan ĭmbidə ej ibi, a miʔnʼibeʔ ibi". </ta>
            <ta e="T91" id="Seg_2859" s="T82">"Nu i što, ej ibi dak puskaj ej iləj". </ta>
            <ta e="T96" id="Seg_2860" s="T91">Dĭgəttə dĭ bazoʔ kambi, ipeksi. </ta>
            <ta e="T98" id="Seg_2861" s="T96">"Ĭmbi izittə?" </ta>
            <ta e="T104" id="Seg_2862" s="T98">Dĭgəttə dĭzeŋ măndəʔi: "Bătinkaʔi iʔ, kuvas". </ta>
            <ta e="T108" id="Seg_2863" s="T104">Dĭ kambi, dĭzeŋdə ibi. </ta>
            <ta e="T116" id="Seg_2864" s="T108">(A=) A üdʼüge măndə: "Măna iʔ Filisnəj jasnəj săkolika pʼoruškă". </ta>
            <ta e="T120" id="Seg_2865" s="T116">Dĭ kambi, (dĭzeŋdə) ibi. </ta>
            <ta e="T124" id="Seg_2866" s="T120">A dĭm ej kubi. </ta>
            <ta e="T127" id="Seg_2867" s="T124">Dĭgəttə nagurəŋ kambi. </ta>
            <ta e="T132" id="Seg_2868" s="T127">Dĭgəttə dĭzeŋdə ibi bar ĭmbi. </ta>
            <ta e="T139" id="Seg_2869" s="T132">A dĭʔnə kubi dĭ pʼoruškă i deʔpi. </ta>
            <ta e="T144" id="Seg_2870" s="T139">Dĭgəttə dĭ bostə (kien) kunolbi. </ta>
            <ta e="T147" id="Seg_2871" s="T144">Ibi dĭ pʼoruškă. </ta>
            <ta e="T149" id="Seg_2872" s="T147">(Bambi) udandə. </ta>
            <ta e="T157" id="Seg_2873" s="T149">Udandə (baʔluʔpi), dĭ bar nʼi moluʔpi, bar dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T165" id="Seg_2874" s="T157">A dĭn sʼestrazaŋdə măndobiʔi, što dĭn nʼi ige. </ta>
            <ta e="T172" id="Seg_2875" s="T165">Dĭgəttə dĭ (dĭ-) dĭn kuznektə ibi üdʼüge. </ta>
            <ta e="T178" id="Seg_2876" s="T172">Dĭn bar dĭbər tagajʔi (n-) müʔtəbiʔi. </ta>
            <ta e="T182" id="Seg_2877" s="T178">A dĭm (bĭ-) bĭtəlbiʔi. </ta>
            <ta e="T186" id="Seg_2878" s="T182">Dĭrgit bü što kunolbi. </ta>
            <ta e="T188" id="Seg_2879" s="T186">Dĭ šobi. </ta>
            <ta e="T192" id="Seg_2880" s="T188">Kirgarbi, kirgarbi, üjüttə băʔpi. </ta>
            <ta e="T194" id="Seg_2881" s="T192">I kalla dʼürbi. </ta>
            <ta e="T195" id="Seg_2882" s="T194">((BRK)). </ta>
            <ta e="T203" id="Seg_2883" s="T195">Dĭn sʼestrazaŋdə dĭʔnə mĭbiʔi kaplʼaʔi, štobɨ dĭ kunolluʔpi. </ta>
            <ta e="T206" id="Seg_2884" s="T203">Dĭ bar kunolluʔpi. </ta>
            <ta e="T209" id="Seg_2885" s="T206">A dĭ šobi. </ta>
            <ta e="T214" id="Seg_2886" s="T209">Kuznektə dĭzeŋ dĭn müʔpiʔi tagajʔi. </ta>
            <ta e="T218" id="Seg_2887" s="T214">Dĭ bar šüzittə xatʼel. </ta>
            <ta e="T221" id="Seg_2888" s="T218">Üjübə bar bătluʔpi. </ta>
            <ta e="T223" id="Seg_2889" s="T221">Kem mʼaŋnuʔpi. </ta>
            <ta e="T226" id="Seg_2890" s="T223">Dĭgəttə sazən pʼaŋdəbi. </ta>
            <ta e="T237" id="Seg_2891" s="T226">I (baʔ-) embi: "Kamen bazaj jamaʔi aləl, iʔtləl, dĭgəttə măna kulal. </ta>
            <ta e="T238" id="Seg_2892" s="T237">((BRK)). </ta>
            <ta e="T241" id="Seg_2893" s="T238">Dĭgəttə dĭ uʔbdəbi. </ta>
            <ta e="T245" id="Seg_2894" s="T241">Kuiot: bar kem dön. </ta>
            <ta e="T248" id="Seg_2895" s="T245">I sazən iʔbolaʔbə. </ta>
            <ta e="T254" id="Seg_2896" s="T248">Dĭ sazən măndəbi, bar dʼorbi, dʼorbi. </ta>
            <ta e="T257" id="Seg_2897" s="T254">Da kambi kuznʼitsanə. </ta>
            <ta e="T263" id="Seg_2898" s="T257">Jamaʔi abi bazaj, pa abi bazaj. </ta>
            <ta e="T265" id="Seg_2899" s="T263">Dĭgəttə kambi. </ta>
            <ta e="T270" id="Seg_2900" s="T265">Kambi, kambi, kundʼom (šonəga=) šonəbi. </ta>
            <ta e="T277" id="Seg_2901" s="T270">Dĭgəttə (kuiot=) kulaʔbə: tura (i-) nuga (idʼiʔeje). </ta>
            <ta e="T280" id="Seg_2902" s="T277">Dĭ turanə šobi. </ta>
            <ta e="T283" id="Seg_2903" s="T280">Dĭn nüke amnolaʔbə. </ta>
            <ta e="T292" id="Seg_2904" s="T283">Dĭ surarlaʔbə: "Nüke, nüke, nörbit, gijen măn tibim kalla dʼürbi?" </ta>
            <ta e="T300" id="Seg_2905" s="T292">Dĭ măndə: "Măn ej tĭmnem, gijen (dʼüʔ-), gijendə. </ta>
            <ta e="T303" id="Seg_2906" s="T300">Kanaʔ măn sʼestrandə. </ta>
            <ta e="T305" id="Seg_2907" s="T303">Dĭ tĭmnet. </ta>
            <ta e="T307" id="Seg_2908" s="T305">Nörbələj tănan". </ta>
            <ta e="T314" id="Seg_2909" s="T307">Dĭgəttə dĭʔnə mĭbi takše zălătoj, munəjʔ zălătoj. </ta>
            <ta e="T317" id="Seg_2910" s="T314">I nʼitkaʔi klubok. </ta>
            <ta e="T323" id="Seg_2911" s="T317">"Kanaʔ, dĭ nuʔmələj i tăn kanaʔ". </ta>
            <ta e="T326" id="Seg_2912" s="T323">Dĭgəttə dĭ kambi. </ta>
            <ta e="T327" id="Seg_2913" s="T326">((BRK)). </ta>
            <ta e="T331" id="Seg_2914" s="T327">Dĭgəttə dĭ kambi, kambi. </ta>
            <ta e="T334" id="Seg_2915" s="T331">Baška sʼestrandə šobi. </ta>
            <ta e="T341" id="Seg_2916" s="T334">Tože (turat) (ej üdʼü- ej-) üdʼüge turat. </ta>
            <ta e="T343" id="Seg_2917" s="T341">Šübi turanə. </ta>
            <ta e="T348" id="Seg_2918" s="T343">(Măndə-, dĭn=) Dĭn nüke amnolaʔbə. </ta>
            <ta e="T357" id="Seg_2919" s="T348">Măndə: "Nüke, nüke, nörbeʔ măna, gijen măn tibi kalla dʼürbi?" </ta>
            <ta e="T360" id="Seg_2920" s="T357">"Măn ej tĭmnem. </ta>
            <ta e="T367" id="Seg_2921" s="T360">Kanaʔ măn sʼestrandə, dĭ nörbələj, gijen dĭ". </ta>
            <ta e="T372" id="Seg_2922" s="T367">Dĭ dĭʔnə (mĭmbi=) mĭbi prʼalka. </ta>
            <ta e="T375" id="Seg_2923" s="T372">I mĭbi kugʼelʼi. </ta>
            <ta e="T381" id="Seg_2924" s="T375">Prʼalka bar zălătoj, i kugʼelʼi zălătoj. </ta>
            <ta e="T385" id="Seg_2925" s="T381">Dĭgəttə (mĭ-) mĭbi klubok. </ta>
            <ta e="T395" id="Seg_2926" s="T385">"Dĭ (kandlo-) kandluj, tăn (u-) kanaʔ dĭbər, gibər dĭ kandluj". </ta>
            <ta e="T399" id="Seg_2927" s="T395">Dĭgəttə dö kambi, kambi. </ta>
            <ta e="T405" id="Seg_2928" s="T399">Nagur sʼestrandə šobi, tože tura nulaʔbə. </ta>
            <ta e="T407" id="Seg_2929" s="T405">Dĭ šobi. </ta>
            <ta e="T410" id="Seg_2930" s="T407">Dĭn nüke amnolaʔbə. </ta>
            <ta e="T411" id="Seg_2931" s="T410">(ugandə). </ta>
            <ta e="T421" id="Seg_2932" s="T411">Dĭgəttə măndə:" Nüke, nüke, nörbeʔ măna, gijən măn tibim kalla dʼürbi?" </ta>
            <ta e="T423" id="Seg_2933" s="T421">"Măn tĭmnem. </ta>
            <ta e="T424" id="Seg_2934" s="T423">Kanaʔ." </ta>
            <ta e="T432" id="Seg_2935" s="T424">Dĭgəttə dĭ mĭbi dĭʔnə (kormiš) kuvas, zălătoj bar. </ta>
            <ta e="T437" id="Seg_2936" s="T432">(Igol'-) Kălʼečkă mĭbi, zălătoj udandə. </ta>
            <ta e="T438" id="Seg_2937" s="T437">"Kanaʔ. </ta>
            <ta e="T440" id="Seg_2938" s="T438">Gijendə amnolaʔbə". </ta>
            <ta e="T445" id="Seg_2939" s="T440">Dĭʔnə mĭbi tože (k-) klubok. </ta>
            <ta e="T451" id="Seg_2940" s="T445">"Gibər dĭ kandləj, tăn dĭbər kanaʔ". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2941" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_2942" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_2943" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_2944" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_2945" s="T4">nagur</ta>
            <ta e="T6" id="Seg_2946" s="T5">koʔbdo</ta>
            <ta e="T7" id="Seg_2947" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_2948" s="T7">dĭ</ta>
            <ta e="T10" id="Seg_2949" s="T9">kandə-ga</ta>
            <ta e="T11" id="Seg_2950" s="T10">ipek</ta>
            <ta e="T12" id="Seg_2951" s="T11">sadar-zittə</ta>
            <ta e="T13" id="Seg_2952" s="T12">ĭmbi</ta>
            <ta e="T14" id="Seg_2953" s="T13">šiʔnʼileʔ</ta>
            <ta e="T15" id="Seg_2954" s="T14">i-zittə</ta>
            <ta e="T16" id="Seg_2955" s="T15">onʼiʔ</ta>
            <ta e="T17" id="Seg_2956" s="T16">măn-də</ta>
            <ta e="T18" id="Seg_2957" s="T17">măna</ta>
            <ta e="T19" id="Seg_2958" s="T18">i-ʔ</ta>
            <ta e="T20" id="Seg_2959" s="T19">kuvas</ta>
            <ta e="T21" id="Seg_2960" s="T20">palʼto</ta>
            <ta e="T22" id="Seg_2961" s="T21">onʼiʔ</ta>
            <ta e="T23" id="Seg_2962" s="T22">măn-də</ta>
            <ta e="T24" id="Seg_2963" s="T23">măna</ta>
            <ta e="T25" id="Seg_2964" s="T24">kuvas</ta>
            <ta e="T26" id="Seg_2965" s="T25">šalʼ</ta>
            <ta e="T28" id="Seg_2966" s="T27">i-ʔ</ta>
            <ta e="T29" id="Seg_2967" s="T28">i</ta>
            <ta e="T30" id="Seg_2968" s="T29">kuvas</ta>
            <ta e="T31" id="Seg_2969" s="T30">platʼtʼa</ta>
            <ta e="T32" id="Seg_2970" s="T31">i-ʔ</ta>
            <ta e="T33" id="Seg_2971" s="T32">a</ta>
            <ta e="T34" id="Seg_2972" s="T33">üdʼüge</ta>
            <ta e="T36" id="Seg_2973" s="T35">a</ta>
            <ta e="T37" id="Seg_2974" s="T36">tănan</ta>
            <ta e="T38" id="Seg_2975" s="T37">ĭmbi</ta>
            <ta e="T39" id="Seg_2976" s="T38">i-zittə</ta>
            <ta e="T40" id="Seg_2977" s="T39">măna</ta>
            <ta e="T41" id="Seg_2978" s="T40">i-ʔ</ta>
            <ta e="T42" id="Seg_2979" s="T41">Filisnəj_jasnəj_săkolika</ta>
            <ta e="T43" id="Seg_2980" s="T42">pʼoruš</ta>
            <ta e="T45" id="Seg_2981" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_2982" s="T45">măn-də</ta>
            <ta e="T47" id="Seg_2983" s="T46">i-ʔ</ta>
            <ta e="T48" id="Seg_2984" s="T47">măna</ta>
            <ta e="T49" id="Seg_2985" s="T48">Filišnəj_jasnəj</ta>
            <ta e="T50" id="Seg_2986" s="T49">pʼoruškă</ta>
            <ta e="T51" id="Seg_2987" s="T50">dĭ</ta>
            <ta e="T52" id="Seg_2988" s="T51">kam-bi</ta>
            <ta e="T53" id="Seg_2989" s="T52">dĭʔ-nə</ta>
            <ta e="T54" id="Seg_2990" s="T53">i-bi</ta>
            <ta e="T55" id="Seg_2991" s="T54">palʼto</ta>
            <ta e="T56" id="Seg_2992" s="T55">dĭʔ-nə</ta>
            <ta e="T57" id="Seg_2993" s="T56">šalʼ</ta>
            <ta e="T58" id="Seg_2994" s="T57">i-bi</ta>
            <ta e="T59" id="Seg_2995" s="T58">i</ta>
            <ta e="T61" id="Seg_2996" s="T60">i</ta>
            <ta e="T62" id="Seg_2997" s="T61">platʼtʼa</ta>
            <ta e="T63" id="Seg_2998" s="T62">i-bi</ta>
            <ta e="T64" id="Seg_2999" s="T63">măndə-r-bi</ta>
            <ta e="T65" id="Seg_3000" s="T64">măndə-r-bi</ta>
            <ta e="T66" id="Seg_3001" s="T65">ej</ta>
            <ta e="T67" id="Seg_3002" s="T66">ku-bi</ta>
            <ta e="T68" id="Seg_3003" s="T67">dĭgəttə</ta>
            <ta e="T69" id="Seg_3004" s="T68">šo-bi</ta>
            <ta e="T70" id="Seg_3005" s="T69">maːʔ-ndə</ta>
            <ta e="T71" id="Seg_3006" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_3007" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_3008" s="T72">šidö</ta>
            <ta e="T74" id="Seg_3009" s="T73">koʔbdo</ta>
            <ta e="T75" id="Seg_3010" s="T74">kaknar-laʔbə-ʔjə</ta>
            <ta e="T76" id="Seg_3011" s="T75">tănan</ta>
            <ta e="T77" id="Seg_3012" s="T76">ĭmbi=də</ta>
            <ta e="T78" id="Seg_3013" s="T77">ej</ta>
            <ta e="T79" id="Seg_3014" s="T78">i-bi</ta>
            <ta e="T80" id="Seg_3015" s="T79">a</ta>
            <ta e="T81" id="Seg_3016" s="T80">miʔnʼibeʔ</ta>
            <ta e="T82" id="Seg_3017" s="T81">i-bi</ta>
            <ta e="T83" id="Seg_3018" s="T82">nu</ta>
            <ta e="T84" id="Seg_3019" s="T83">i</ta>
            <ta e="T85" id="Seg_3020" s="T84">što</ta>
            <ta e="T86" id="Seg_3021" s="T85">ej</ta>
            <ta e="T87" id="Seg_3022" s="T86">i-bi</ta>
            <ta e="T88" id="Seg_3023" s="T87">dak</ta>
            <ta e="T89" id="Seg_3024" s="T88">puskaj</ta>
            <ta e="T90" id="Seg_3025" s="T89">ej</ta>
            <ta e="T91" id="Seg_3026" s="T90">i-lə-j</ta>
            <ta e="T92" id="Seg_3027" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_3028" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_3029" s="T93">bazoʔ</ta>
            <ta e="T95" id="Seg_3030" s="T94">kam-bi</ta>
            <ta e="T96" id="Seg_3031" s="T95">ipek-si</ta>
            <ta e="T97" id="Seg_3032" s="T96">ĭmbi</ta>
            <ta e="T98" id="Seg_3033" s="T97">i-zittə</ta>
            <ta e="T99" id="Seg_3034" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_3035" s="T99">dĭ-zeŋ</ta>
            <ta e="T101" id="Seg_3036" s="T100">măn-də-ʔi</ta>
            <ta e="T102" id="Seg_3037" s="T101">bătinka-ʔi</ta>
            <ta e="T103" id="Seg_3038" s="T102">i-ʔ</ta>
            <ta e="T104" id="Seg_3039" s="T103">kuvas</ta>
            <ta e="T105" id="Seg_3040" s="T104">dĭ</ta>
            <ta e="T106" id="Seg_3041" s="T105">kam-bi</ta>
            <ta e="T107" id="Seg_3042" s="T106">dĭ-zeŋ-də</ta>
            <ta e="T108" id="Seg_3043" s="T107">i-bi</ta>
            <ta e="T109" id="Seg_3044" s="T108">a</ta>
            <ta e="T110" id="Seg_3045" s="T109">a</ta>
            <ta e="T111" id="Seg_3046" s="T110">üdʼüge</ta>
            <ta e="T112" id="Seg_3047" s="T111">măn-də</ta>
            <ta e="T113" id="Seg_3048" s="T112">măna</ta>
            <ta e="T114" id="Seg_3049" s="T113">i-ʔ</ta>
            <ta e="T115" id="Seg_3050" s="T114">Filisnəj_jasnəj_săkolika</ta>
            <ta e="T116" id="Seg_3051" s="T115">pʼoruškă</ta>
            <ta e="T117" id="Seg_3052" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_3053" s="T117">kam-bi</ta>
            <ta e="T119" id="Seg_3054" s="T118">dĭ-zeŋ-də</ta>
            <ta e="T120" id="Seg_3055" s="T119">i-bi</ta>
            <ta e="T121" id="Seg_3056" s="T120">a</ta>
            <ta e="T122" id="Seg_3057" s="T121">dĭ-m</ta>
            <ta e="T123" id="Seg_3058" s="T122">ej</ta>
            <ta e="T124" id="Seg_3059" s="T123">ku-bi</ta>
            <ta e="T125" id="Seg_3060" s="T124">dĭgəttə</ta>
            <ta e="T126" id="Seg_3061" s="T125">nagur-əŋ</ta>
            <ta e="T127" id="Seg_3062" s="T126">kam-bi</ta>
            <ta e="T128" id="Seg_3063" s="T127">dĭgəttə</ta>
            <ta e="T129" id="Seg_3064" s="T128">dĭ-zeŋ-də</ta>
            <ta e="T130" id="Seg_3065" s="T129">i-bi</ta>
            <ta e="T131" id="Seg_3066" s="T130">bar</ta>
            <ta e="T132" id="Seg_3067" s="T131">ĭmbi</ta>
            <ta e="T133" id="Seg_3068" s="T132">a</ta>
            <ta e="T134" id="Seg_3069" s="T133">dĭʔ-nə</ta>
            <ta e="T135" id="Seg_3070" s="T134">ku-bi</ta>
            <ta e="T136" id="Seg_3071" s="T135">dĭ</ta>
            <ta e="T137" id="Seg_3072" s="T136">pʼoruškă</ta>
            <ta e="T138" id="Seg_3073" s="T137">i</ta>
            <ta e="T139" id="Seg_3074" s="T138">deʔ-pi</ta>
            <ta e="T140" id="Seg_3075" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_3076" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_3077" s="T141">bos-tə</ta>
            <ta e="T143" id="Seg_3078" s="T142">kien</ta>
            <ta e="T144" id="Seg_3079" s="T143">kunol-bi</ta>
            <ta e="T145" id="Seg_3080" s="T144">i-bi</ta>
            <ta e="T146" id="Seg_3081" s="T145">dĭ</ta>
            <ta e="T147" id="Seg_3082" s="T146">pʼoruškă</ta>
            <ta e="T148" id="Seg_3083" s="T147">bam-bi</ta>
            <ta e="T149" id="Seg_3084" s="T148">uda-ndə</ta>
            <ta e="T150" id="Seg_3085" s="T149">uda-ndə</ta>
            <ta e="T151" id="Seg_3086" s="T150">baʔ-luʔ-pi</ta>
            <ta e="T152" id="Seg_3087" s="T151">dĭ</ta>
            <ta e="T153" id="Seg_3088" s="T152">bar</ta>
            <ta e="T154" id="Seg_3089" s="T153">nʼi</ta>
            <ta e="T155" id="Seg_3090" s="T154">mo-luʔ-pi</ta>
            <ta e="T156" id="Seg_3091" s="T155">bar</ta>
            <ta e="T157" id="Seg_3092" s="T156">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T158" id="Seg_3093" s="T157">a</ta>
            <ta e="T159" id="Seg_3094" s="T158">dĭ-n</ta>
            <ta e="T160" id="Seg_3095" s="T159">sʼestra-zaŋ-də</ta>
            <ta e="T161" id="Seg_3096" s="T160">măndo-bi-ʔi</ta>
            <ta e="T162" id="Seg_3097" s="T161">što</ta>
            <ta e="T163" id="Seg_3098" s="T162">dĭn</ta>
            <ta e="T164" id="Seg_3099" s="T163">nʼi</ta>
            <ta e="T165" id="Seg_3100" s="T164">i-ge</ta>
            <ta e="T166" id="Seg_3101" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_3102" s="T166">dĭ</ta>
            <ta e="T169" id="Seg_3103" s="T168">dĭn</ta>
            <ta e="T170" id="Seg_3104" s="T169">kuznek-tə</ta>
            <ta e="T171" id="Seg_3105" s="T170">i-bi</ta>
            <ta e="T172" id="Seg_3106" s="T171">üdʼüge</ta>
            <ta e="T173" id="Seg_3107" s="T172">dĭn</ta>
            <ta e="T174" id="Seg_3108" s="T173">bar</ta>
            <ta e="T175" id="Seg_3109" s="T174">dĭbər</ta>
            <ta e="T176" id="Seg_3110" s="T175">tagaj-ʔi</ta>
            <ta e="T178" id="Seg_3111" s="T177">müʔ-tə-bi-ʔi</ta>
            <ta e="T179" id="Seg_3112" s="T178">a</ta>
            <ta e="T180" id="Seg_3113" s="T179">dĭ-m</ta>
            <ta e="T182" id="Seg_3114" s="T181">bĭt-əl-bi-ʔi</ta>
            <ta e="T183" id="Seg_3115" s="T182">dĭrgit</ta>
            <ta e="T184" id="Seg_3116" s="T183">bü</ta>
            <ta e="T185" id="Seg_3117" s="T184">što</ta>
            <ta e="T186" id="Seg_3118" s="T185">kunol-bi</ta>
            <ta e="T187" id="Seg_3119" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_3120" s="T187">šo-bi</ta>
            <ta e="T189" id="Seg_3121" s="T188">kirgar-bi</ta>
            <ta e="T190" id="Seg_3122" s="T189">kirgar-bi</ta>
            <ta e="T191" id="Seg_3123" s="T190">üjü-ttə</ta>
            <ta e="T192" id="Seg_3124" s="T191">băʔ-pi</ta>
            <ta e="T193" id="Seg_3125" s="T192">i</ta>
            <ta e="T453" id="Seg_3126" s="T193">kal-la</ta>
            <ta e="T194" id="Seg_3127" s="T453">dʼür-bi</ta>
            <ta e="T196" id="Seg_3128" s="T195">dĭ-n</ta>
            <ta e="T197" id="Seg_3129" s="T196">sʼestra-zaŋ-də</ta>
            <ta e="T198" id="Seg_3130" s="T197">dĭʔ-nə</ta>
            <ta e="T199" id="Seg_3131" s="T198">mĭ-bi-ʔi</ta>
            <ta e="T200" id="Seg_3132" s="T199">kaplʼa-ʔi</ta>
            <ta e="T201" id="Seg_3133" s="T200">štobɨ</ta>
            <ta e="T202" id="Seg_3134" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_3135" s="T202">kunol-luʔ-pi</ta>
            <ta e="T204" id="Seg_3136" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_3137" s="T204">bar</ta>
            <ta e="T206" id="Seg_3138" s="T205">kunol-luʔ-pi</ta>
            <ta e="T207" id="Seg_3139" s="T206">a</ta>
            <ta e="T208" id="Seg_3140" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_3141" s="T208">šo-bi</ta>
            <ta e="T210" id="Seg_3142" s="T209">kuznek-tə</ta>
            <ta e="T211" id="Seg_3143" s="T210">dĭ-zeŋ</ta>
            <ta e="T212" id="Seg_3144" s="T211">dĭn</ta>
            <ta e="T213" id="Seg_3145" s="T212">müʔ-pi-ʔi</ta>
            <ta e="T214" id="Seg_3146" s="T213">tagaj-ʔi</ta>
            <ta e="T215" id="Seg_3147" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_3148" s="T215">bar</ta>
            <ta e="T217" id="Seg_3149" s="T216">šü-zittə</ta>
            <ta e="T218" id="Seg_3150" s="T217">xatʼel</ta>
            <ta e="T219" id="Seg_3151" s="T218">üjü-bə</ta>
            <ta e="T220" id="Seg_3152" s="T219">bar</ta>
            <ta e="T221" id="Seg_3153" s="T220">băt-luʔ-pi</ta>
            <ta e="T222" id="Seg_3154" s="T221">kem</ta>
            <ta e="T223" id="Seg_3155" s="T222">mʼaŋ-nuʔ-pi</ta>
            <ta e="T224" id="Seg_3156" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_3157" s="T224">sazən</ta>
            <ta e="T226" id="Seg_3158" s="T225">pʼaŋdə-bi</ta>
            <ta e="T227" id="Seg_3159" s="T226">i</ta>
            <ta e="T229" id="Seg_3160" s="T228">em-bi</ta>
            <ta e="T230" id="Seg_3161" s="T229">kamen</ta>
            <ta e="T231" id="Seg_3162" s="T230">baza-j</ta>
            <ta e="T232" id="Seg_3163" s="T231">jama-ʔi</ta>
            <ta e="T233" id="Seg_3164" s="T232">a-lə-l</ta>
            <ta e="T234" id="Seg_3165" s="T233">iʔt-lə-l</ta>
            <ta e="T235" id="Seg_3166" s="T234">dĭgəttə</ta>
            <ta e="T236" id="Seg_3167" s="T235">măna</ta>
            <ta e="T237" id="Seg_3168" s="T236">ku-la-l</ta>
            <ta e="T239" id="Seg_3169" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_3170" s="T239">dĭ</ta>
            <ta e="T241" id="Seg_3171" s="T240">uʔbdə-bi</ta>
            <ta e="T242" id="Seg_3172" s="T241">ku-io-t</ta>
            <ta e="T243" id="Seg_3173" s="T242">bar</ta>
            <ta e="T244" id="Seg_3174" s="T243">kem</ta>
            <ta e="T245" id="Seg_3175" s="T244">dön</ta>
            <ta e="T246" id="Seg_3176" s="T245">i</ta>
            <ta e="T247" id="Seg_3177" s="T246">sazən</ta>
            <ta e="T248" id="Seg_3178" s="T247">iʔbo-laʔbə</ta>
            <ta e="T249" id="Seg_3179" s="T248">dĭ</ta>
            <ta e="T250" id="Seg_3180" s="T249">sazən</ta>
            <ta e="T251" id="Seg_3181" s="T250">măndə-bi</ta>
            <ta e="T252" id="Seg_3182" s="T251">bar</ta>
            <ta e="T253" id="Seg_3183" s="T252">dʼor-bi</ta>
            <ta e="T254" id="Seg_3184" s="T253">dʼor-bi</ta>
            <ta e="T255" id="Seg_3185" s="T254">da</ta>
            <ta e="T256" id="Seg_3186" s="T255">kam-bi</ta>
            <ta e="T257" id="Seg_3187" s="T256">kuznʼitsa-nə</ta>
            <ta e="T258" id="Seg_3188" s="T257">jama-ʔi</ta>
            <ta e="T259" id="Seg_3189" s="T258">a-bi</ta>
            <ta e="T260" id="Seg_3190" s="T259">baza-j</ta>
            <ta e="T261" id="Seg_3191" s="T260">pa</ta>
            <ta e="T262" id="Seg_3192" s="T261">a-bi</ta>
            <ta e="T263" id="Seg_3193" s="T262">baza-j</ta>
            <ta e="T264" id="Seg_3194" s="T263">dĭgəttə</ta>
            <ta e="T265" id="Seg_3195" s="T264">kam-bi</ta>
            <ta e="T266" id="Seg_3196" s="T265">kam-bi</ta>
            <ta e="T267" id="Seg_3197" s="T266">kam-bi</ta>
            <ta e="T268" id="Seg_3198" s="T267">kundʼo-m</ta>
            <ta e="T269" id="Seg_3199" s="T268">šonə-ga</ta>
            <ta e="T270" id="Seg_3200" s="T269">šonə-bi</ta>
            <ta e="T271" id="Seg_3201" s="T270">dĭgəttə</ta>
            <ta e="T272" id="Seg_3202" s="T271">ku-io-t</ta>
            <ta e="T273" id="Seg_3203" s="T272">ku-laʔbə</ta>
            <ta e="T274" id="Seg_3204" s="T273">tura</ta>
            <ta e="T276" id="Seg_3205" s="T275">nu-ga</ta>
            <ta e="T277" id="Seg_3206" s="T276">idʼiʔeje</ta>
            <ta e="T278" id="Seg_3207" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_3208" s="T278">tura-nə</ta>
            <ta e="T280" id="Seg_3209" s="T279">šo-bi</ta>
            <ta e="T281" id="Seg_3210" s="T280">dĭn</ta>
            <ta e="T282" id="Seg_3211" s="T281">nüke</ta>
            <ta e="T283" id="Seg_3212" s="T282">amno-laʔbə</ta>
            <ta e="T284" id="Seg_3213" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_3214" s="T284">surar-laʔbə</ta>
            <ta e="T286" id="Seg_3215" s="T285">nüke</ta>
            <ta e="T287" id="Seg_3216" s="T286">nüke</ta>
            <ta e="T288" id="Seg_3217" s="T287">nörbi-t</ta>
            <ta e="T289" id="Seg_3218" s="T288">gijen</ta>
            <ta e="T290" id="Seg_3219" s="T289">măn</ta>
            <ta e="T291" id="Seg_3220" s="T290">tibi-m</ta>
            <ta e="T454" id="Seg_3221" s="T291">kal-la</ta>
            <ta e="T292" id="Seg_3222" s="T454">dʼür-bi</ta>
            <ta e="T293" id="Seg_3223" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_3224" s="T293">măn-də</ta>
            <ta e="T295" id="Seg_3225" s="T294">măn</ta>
            <ta e="T296" id="Seg_3226" s="T295">ej</ta>
            <ta e="T297" id="Seg_3227" s="T296">tĭmne-m</ta>
            <ta e="T298" id="Seg_3228" s="T297">gijen</ta>
            <ta e="T300" id="Seg_3229" s="T299">gijen=də</ta>
            <ta e="T301" id="Seg_3230" s="T300">kan-a-ʔ</ta>
            <ta e="T302" id="Seg_3231" s="T301">măn</ta>
            <ta e="T303" id="Seg_3232" s="T302">sʼestra-ndə</ta>
            <ta e="T304" id="Seg_3233" s="T303">dĭ</ta>
            <ta e="T305" id="Seg_3234" s="T304">tĭmne-t</ta>
            <ta e="T306" id="Seg_3235" s="T305">nörbə-lə-j</ta>
            <ta e="T307" id="Seg_3236" s="T306">tănan</ta>
            <ta e="T308" id="Seg_3237" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_3238" s="T308">dĭʔ-nə</ta>
            <ta e="T310" id="Seg_3239" s="T309">mĭ-bi</ta>
            <ta e="T311" id="Seg_3240" s="T310">takše</ta>
            <ta e="T312" id="Seg_3241" s="T311">zălătoj</ta>
            <ta e="T313" id="Seg_3242" s="T312">munəj-ʔ</ta>
            <ta e="T314" id="Seg_3243" s="T313">zălătoj</ta>
            <ta e="T315" id="Seg_3244" s="T314">i</ta>
            <ta e="T316" id="Seg_3245" s="T315">nʼitka-ʔi</ta>
            <ta e="T317" id="Seg_3246" s="T316">klubok</ta>
            <ta e="T318" id="Seg_3247" s="T317">kan-a-ʔ</ta>
            <ta e="T319" id="Seg_3248" s="T318">dĭ</ta>
            <ta e="T320" id="Seg_3249" s="T319">nuʔmə-lə-j</ta>
            <ta e="T321" id="Seg_3250" s="T320">i</ta>
            <ta e="T322" id="Seg_3251" s="T321">tăn</ta>
            <ta e="T323" id="Seg_3252" s="T322">kan-a-ʔ</ta>
            <ta e="T324" id="Seg_3253" s="T323">dĭgəttə</ta>
            <ta e="T325" id="Seg_3254" s="T324">dĭ</ta>
            <ta e="T326" id="Seg_3255" s="T325">kam-bi</ta>
            <ta e="T328" id="Seg_3256" s="T327">dĭgəttə</ta>
            <ta e="T329" id="Seg_3257" s="T328">dĭ</ta>
            <ta e="T330" id="Seg_3258" s="T329">kam-bi</ta>
            <ta e="T331" id="Seg_3259" s="T330">kam-bi</ta>
            <ta e="T332" id="Seg_3260" s="T331">baška</ta>
            <ta e="T333" id="Seg_3261" s="T332">sʼestra-ndə</ta>
            <ta e="T334" id="Seg_3262" s="T333">šo-bi</ta>
            <ta e="T335" id="Seg_3263" s="T334">tože</ta>
            <ta e="T336" id="Seg_3264" s="T335">tura-t</ta>
            <ta e="T337" id="Seg_3265" s="T336">ej</ta>
            <ta e="T340" id="Seg_3266" s="T339">üdʼüge</ta>
            <ta e="T341" id="Seg_3267" s="T340">tura-t</ta>
            <ta e="T342" id="Seg_3268" s="T341">šü-bi</ta>
            <ta e="T343" id="Seg_3269" s="T342">tura-nə</ta>
            <ta e="T345" id="Seg_3270" s="T344">dĭn</ta>
            <ta e="T346" id="Seg_3271" s="T345">dĭn</ta>
            <ta e="T347" id="Seg_3272" s="T346">nüke</ta>
            <ta e="T348" id="Seg_3273" s="T347">amno-laʔbə</ta>
            <ta e="T349" id="Seg_3274" s="T348">măn-də</ta>
            <ta e="T350" id="Seg_3275" s="T349">nüke</ta>
            <ta e="T351" id="Seg_3276" s="T350">nüke</ta>
            <ta e="T352" id="Seg_3277" s="T351">nörbe-ʔ</ta>
            <ta e="T353" id="Seg_3278" s="T352">măna</ta>
            <ta e="T354" id="Seg_3279" s="T353">gijen</ta>
            <ta e="T355" id="Seg_3280" s="T354">măn</ta>
            <ta e="T356" id="Seg_3281" s="T355">tibi</ta>
            <ta e="T455" id="Seg_3282" s="T356">kal-la</ta>
            <ta e="T357" id="Seg_3283" s="T455">dʼür-bi</ta>
            <ta e="T358" id="Seg_3284" s="T357">măn</ta>
            <ta e="T359" id="Seg_3285" s="T358">ej</ta>
            <ta e="T360" id="Seg_3286" s="T359">tĭmne-m</ta>
            <ta e="T361" id="Seg_3287" s="T360">kan-a-ʔ</ta>
            <ta e="T362" id="Seg_3288" s="T361">măn</ta>
            <ta e="T363" id="Seg_3289" s="T362">sʼestra-ndə</ta>
            <ta e="T364" id="Seg_3290" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_3291" s="T364">nörbə-lə-j</ta>
            <ta e="T366" id="Seg_3292" s="T365">gijen</ta>
            <ta e="T367" id="Seg_3293" s="T366">dĭ</ta>
            <ta e="T368" id="Seg_3294" s="T367">dĭ</ta>
            <ta e="T369" id="Seg_3295" s="T368">dĭʔ-nə</ta>
            <ta e="T370" id="Seg_3296" s="T369">mĭm-bi</ta>
            <ta e="T371" id="Seg_3297" s="T370">mĭ-bi</ta>
            <ta e="T372" id="Seg_3298" s="T371">prʼalka</ta>
            <ta e="T373" id="Seg_3299" s="T372">i</ta>
            <ta e="T374" id="Seg_3300" s="T373">mĭ-bi</ta>
            <ta e="T375" id="Seg_3301" s="T374">kugʼelʼi</ta>
            <ta e="T376" id="Seg_3302" s="T375">prʼalka</ta>
            <ta e="T377" id="Seg_3303" s="T376">bar</ta>
            <ta e="T378" id="Seg_3304" s="T377">zălătoj</ta>
            <ta e="T379" id="Seg_3305" s="T378">i</ta>
            <ta e="T380" id="Seg_3306" s="T379">kugʼelʼi</ta>
            <ta e="T381" id="Seg_3307" s="T380">zălătoj</ta>
            <ta e="T382" id="Seg_3308" s="T381">dĭgəttə</ta>
            <ta e="T384" id="Seg_3309" s="T383">mĭ-bi</ta>
            <ta e="T385" id="Seg_3310" s="T384">klubok</ta>
            <ta e="T386" id="Seg_3311" s="T385">dĭ</ta>
            <ta e="T388" id="Seg_3312" s="T387">kand-lu-j</ta>
            <ta e="T389" id="Seg_3313" s="T388">tăn</ta>
            <ta e="T391" id="Seg_3314" s="T390">kan-a-ʔ</ta>
            <ta e="T392" id="Seg_3315" s="T391">dĭbər</ta>
            <ta e="T393" id="Seg_3316" s="T392">gibər</ta>
            <ta e="T394" id="Seg_3317" s="T393">dĭ</ta>
            <ta e="T395" id="Seg_3318" s="T394">kand-lu-j</ta>
            <ta e="T396" id="Seg_3319" s="T395">dĭgəttə</ta>
            <ta e="T397" id="Seg_3320" s="T396">dö</ta>
            <ta e="T398" id="Seg_3321" s="T397">kam-bi</ta>
            <ta e="T399" id="Seg_3322" s="T398">kam-bi</ta>
            <ta e="T400" id="Seg_3323" s="T399">nagur</ta>
            <ta e="T401" id="Seg_3324" s="T400">sʼestra-ndə</ta>
            <ta e="T402" id="Seg_3325" s="T401">šo-bi</ta>
            <ta e="T403" id="Seg_3326" s="T402">tože</ta>
            <ta e="T404" id="Seg_3327" s="T403">tura</ta>
            <ta e="T405" id="Seg_3328" s="T404">nu-laʔbə</ta>
            <ta e="T406" id="Seg_3329" s="T405">dĭ</ta>
            <ta e="T407" id="Seg_3330" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_3331" s="T407">dĭn</ta>
            <ta e="T409" id="Seg_3332" s="T408">nüke</ta>
            <ta e="T410" id="Seg_3333" s="T409">amno-laʔbə</ta>
            <ta e="T411" id="Seg_3334" s="T410">ugandə</ta>
            <ta e="T412" id="Seg_3335" s="T411">dĭgəttə</ta>
            <ta e="T413" id="Seg_3336" s="T412">măn-də</ta>
            <ta e="T414" id="Seg_3337" s="T413">nüke</ta>
            <ta e="T415" id="Seg_3338" s="T414">nüke</ta>
            <ta e="T416" id="Seg_3339" s="T415">nörbe-ʔ</ta>
            <ta e="T417" id="Seg_3340" s="T416">măna</ta>
            <ta e="T418" id="Seg_3341" s="T417">gijən</ta>
            <ta e="T419" id="Seg_3342" s="T418">măn</ta>
            <ta e="T420" id="Seg_3343" s="T419">tibi-m</ta>
            <ta e="T456" id="Seg_3344" s="T420">kal-la</ta>
            <ta e="T421" id="Seg_3345" s="T456">dʼür-bi</ta>
            <ta e="T422" id="Seg_3346" s="T421">măn</ta>
            <ta e="T423" id="Seg_3347" s="T422">tĭmne-m</ta>
            <ta e="T424" id="Seg_3348" s="T423">kan-a-ʔ</ta>
            <ta e="T425" id="Seg_3349" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_3350" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_3351" s="T426">mĭ-bi</ta>
            <ta e="T428" id="Seg_3352" s="T427">dĭʔ-nə</ta>
            <ta e="T429" id="Seg_3353" s="T428">korniš</ta>
            <ta e="T430" id="Seg_3354" s="T429">kuvas</ta>
            <ta e="T431" id="Seg_3355" s="T430">zălătoj</ta>
            <ta e="T432" id="Seg_3356" s="T431">bar</ta>
            <ta e="T434" id="Seg_3357" s="T433">kălʼečkă</ta>
            <ta e="T435" id="Seg_3358" s="T434">mĭ-bi</ta>
            <ta e="T436" id="Seg_3359" s="T435">zălătoj</ta>
            <ta e="T437" id="Seg_3360" s="T436">uda-ndə</ta>
            <ta e="T438" id="Seg_3361" s="T437">kan-a-ʔ</ta>
            <ta e="T439" id="Seg_3362" s="T438">gijen=də</ta>
            <ta e="T440" id="Seg_3363" s="T439">amno-laʔbə</ta>
            <ta e="T441" id="Seg_3364" s="T440">dĭʔ-nə</ta>
            <ta e="T442" id="Seg_3365" s="T441">mĭ-bi</ta>
            <ta e="T443" id="Seg_3366" s="T442">tože</ta>
            <ta e="T445" id="Seg_3367" s="T444">klubok</ta>
            <ta e="T446" id="Seg_3368" s="T445">gibər</ta>
            <ta e="T447" id="Seg_3369" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_3370" s="T447">kand-lə-j</ta>
            <ta e="T449" id="Seg_3371" s="T448">tăn</ta>
            <ta e="T450" id="Seg_3372" s="T449">dĭbər</ta>
            <ta e="T451" id="Seg_3373" s="T450">kan-a-ʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3374" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_3375" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_3376" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_3377" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_3378" s="T4">nagur</ta>
            <ta e="T6" id="Seg_3379" s="T5">koʔbdo</ta>
            <ta e="T7" id="Seg_3380" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_3381" s="T7">dĭ</ta>
            <ta e="T10" id="Seg_3382" s="T9">kandə-gA</ta>
            <ta e="T11" id="Seg_3383" s="T10">ipek</ta>
            <ta e="T12" id="Seg_3384" s="T11">sădar-zittə</ta>
            <ta e="T13" id="Seg_3385" s="T12">ĭmbi</ta>
            <ta e="T14" id="Seg_3386" s="T13">šiʔnʼileʔ</ta>
            <ta e="T15" id="Seg_3387" s="T14">i-zittə</ta>
            <ta e="T16" id="Seg_3388" s="T15">onʼiʔ</ta>
            <ta e="T17" id="Seg_3389" s="T16">măn-ntə</ta>
            <ta e="T18" id="Seg_3390" s="T17">măna</ta>
            <ta e="T19" id="Seg_3391" s="T18">i-ʔ</ta>
            <ta e="T20" id="Seg_3392" s="T19">kuvas</ta>
            <ta e="T21" id="Seg_3393" s="T20">palʼto</ta>
            <ta e="T22" id="Seg_3394" s="T21">onʼiʔ</ta>
            <ta e="T23" id="Seg_3395" s="T22">măn-ntə</ta>
            <ta e="T24" id="Seg_3396" s="T23">măna</ta>
            <ta e="T25" id="Seg_3397" s="T24">kuvas</ta>
            <ta e="T26" id="Seg_3398" s="T25">šalʼ</ta>
            <ta e="T28" id="Seg_3399" s="T27">i-ʔ</ta>
            <ta e="T29" id="Seg_3400" s="T28">i</ta>
            <ta e="T30" id="Seg_3401" s="T29">kuvas</ta>
            <ta e="T31" id="Seg_3402" s="T30">platʼtʼa</ta>
            <ta e="T32" id="Seg_3403" s="T31">i-ʔ</ta>
            <ta e="T33" id="Seg_3404" s="T32">a</ta>
            <ta e="T34" id="Seg_3405" s="T33">üdʼüge</ta>
            <ta e="T36" id="Seg_3406" s="T35">a</ta>
            <ta e="T37" id="Seg_3407" s="T36">tănan</ta>
            <ta e="T38" id="Seg_3408" s="T37">ĭmbi</ta>
            <ta e="T39" id="Seg_3409" s="T38">i-zittə</ta>
            <ta e="T40" id="Seg_3410" s="T39">măna</ta>
            <ta e="T41" id="Seg_3411" s="T40">i-ʔ</ta>
            <ta e="T42" id="Seg_3412" s="T41">Filisnəj_jasnəj_săkolika</ta>
            <ta e="T43" id="Seg_3413" s="T42">pʼoruškă</ta>
            <ta e="T45" id="Seg_3414" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_3415" s="T45">măn-ntə</ta>
            <ta e="T47" id="Seg_3416" s="T46">i-ʔ</ta>
            <ta e="T48" id="Seg_3417" s="T47">măna</ta>
            <ta e="T49" id="Seg_3418" s="T48">Filisnəj_jasnəj_săkolika</ta>
            <ta e="T50" id="Seg_3419" s="T49">pʼoruškă</ta>
            <ta e="T51" id="Seg_3420" s="T50">dĭ</ta>
            <ta e="T52" id="Seg_3421" s="T51">kan-bi</ta>
            <ta e="T53" id="Seg_3422" s="T52">dĭ-Tə</ta>
            <ta e="T54" id="Seg_3423" s="T53">i-bi</ta>
            <ta e="T55" id="Seg_3424" s="T54">palʼto</ta>
            <ta e="T56" id="Seg_3425" s="T55">dĭ-Tə</ta>
            <ta e="T57" id="Seg_3426" s="T56">šalʼ</ta>
            <ta e="T58" id="Seg_3427" s="T57">i-bi</ta>
            <ta e="T59" id="Seg_3428" s="T58">i</ta>
            <ta e="T61" id="Seg_3429" s="T60">i</ta>
            <ta e="T62" id="Seg_3430" s="T61">platʼtʼa</ta>
            <ta e="T63" id="Seg_3431" s="T62">i-bi</ta>
            <ta e="T64" id="Seg_3432" s="T63">măndo-r-bi</ta>
            <ta e="T65" id="Seg_3433" s="T64">măndo-r-bi</ta>
            <ta e="T66" id="Seg_3434" s="T65">ej</ta>
            <ta e="T67" id="Seg_3435" s="T66">ku-bi</ta>
            <ta e="T68" id="Seg_3436" s="T67">dĭgəttə</ta>
            <ta e="T69" id="Seg_3437" s="T68">šo-bi</ta>
            <ta e="T70" id="Seg_3438" s="T69">maʔ-gəndə</ta>
            <ta e="T71" id="Seg_3439" s="T70">dĭ</ta>
            <ta e="T72" id="Seg_3440" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_3441" s="T72">šide</ta>
            <ta e="T74" id="Seg_3442" s="T73">koʔbdo</ta>
            <ta e="T75" id="Seg_3443" s="T74">kakənar-laʔbə-jəʔ</ta>
            <ta e="T76" id="Seg_3444" s="T75">tănan</ta>
            <ta e="T77" id="Seg_3445" s="T76">ĭmbi=də</ta>
            <ta e="T78" id="Seg_3446" s="T77">ej</ta>
            <ta e="T79" id="Seg_3447" s="T78">i-bi</ta>
            <ta e="T80" id="Seg_3448" s="T79">a</ta>
            <ta e="T81" id="Seg_3449" s="T80">miʔnʼibeʔ</ta>
            <ta e="T82" id="Seg_3450" s="T81">i-bi</ta>
            <ta e="T83" id="Seg_3451" s="T82">nu</ta>
            <ta e="T84" id="Seg_3452" s="T83">i</ta>
            <ta e="T85" id="Seg_3453" s="T84">što</ta>
            <ta e="T86" id="Seg_3454" s="T85">ej</ta>
            <ta e="T87" id="Seg_3455" s="T86">i-bi</ta>
            <ta e="T88" id="Seg_3456" s="T87">tak</ta>
            <ta e="T89" id="Seg_3457" s="T88">puskaj</ta>
            <ta e="T90" id="Seg_3458" s="T89">ej</ta>
            <ta e="T91" id="Seg_3459" s="T90">i-lV-j</ta>
            <ta e="T92" id="Seg_3460" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_3461" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_3462" s="T93">bazoʔ</ta>
            <ta e="T95" id="Seg_3463" s="T94">kan-bi</ta>
            <ta e="T96" id="Seg_3464" s="T95">ipek-ziʔ</ta>
            <ta e="T97" id="Seg_3465" s="T96">ĭmbi</ta>
            <ta e="T98" id="Seg_3466" s="T97">i-zittə</ta>
            <ta e="T99" id="Seg_3467" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_3468" s="T99">dĭ-zAŋ</ta>
            <ta e="T101" id="Seg_3469" s="T100">măn-ntə-jəʔ</ta>
            <ta e="T102" id="Seg_3470" s="T101">bătʼinkă-jəʔ</ta>
            <ta e="T103" id="Seg_3471" s="T102">i-ʔ</ta>
            <ta e="T104" id="Seg_3472" s="T103">kuvas</ta>
            <ta e="T105" id="Seg_3473" s="T104">dĭ</ta>
            <ta e="T106" id="Seg_3474" s="T105">kan-bi</ta>
            <ta e="T107" id="Seg_3475" s="T106">dĭ-zAŋ-Tə</ta>
            <ta e="T108" id="Seg_3476" s="T107">i-bi</ta>
            <ta e="T109" id="Seg_3477" s="T108">a</ta>
            <ta e="T110" id="Seg_3478" s="T109">a</ta>
            <ta e="T111" id="Seg_3479" s="T110">üdʼüge</ta>
            <ta e="T112" id="Seg_3480" s="T111">măn-ntə</ta>
            <ta e="T113" id="Seg_3481" s="T112">măna</ta>
            <ta e="T114" id="Seg_3482" s="T113">i-ʔ</ta>
            <ta e="T115" id="Seg_3483" s="T114">Filisnəj_jasnəj_săkolika</ta>
            <ta e="T116" id="Seg_3484" s="T115">pʼoruškă</ta>
            <ta e="T117" id="Seg_3485" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_3486" s="T117">kan-bi</ta>
            <ta e="T119" id="Seg_3487" s="T118">dĭ-zAŋ-Tə</ta>
            <ta e="T120" id="Seg_3488" s="T119">i-bi</ta>
            <ta e="T121" id="Seg_3489" s="T120">a</ta>
            <ta e="T122" id="Seg_3490" s="T121">dĭ-m</ta>
            <ta e="T123" id="Seg_3491" s="T122">ej</ta>
            <ta e="T124" id="Seg_3492" s="T123">ku-bi</ta>
            <ta e="T125" id="Seg_3493" s="T124">dĭgəttə</ta>
            <ta e="T126" id="Seg_3494" s="T125">nagur-ŋ</ta>
            <ta e="T127" id="Seg_3495" s="T126">kan-bi</ta>
            <ta e="T128" id="Seg_3496" s="T127">dĭgəttə</ta>
            <ta e="T129" id="Seg_3497" s="T128">dĭ-zAŋ-Tə</ta>
            <ta e="T130" id="Seg_3498" s="T129">i-bi</ta>
            <ta e="T131" id="Seg_3499" s="T130">bar</ta>
            <ta e="T132" id="Seg_3500" s="T131">ĭmbi</ta>
            <ta e="T133" id="Seg_3501" s="T132">a</ta>
            <ta e="T134" id="Seg_3502" s="T133">dĭ-Tə</ta>
            <ta e="T135" id="Seg_3503" s="T134">ku-bi</ta>
            <ta e="T136" id="Seg_3504" s="T135">dĭ</ta>
            <ta e="T137" id="Seg_3505" s="T136">pʼoruš</ta>
            <ta e="T138" id="Seg_3506" s="T137">i</ta>
            <ta e="T139" id="Seg_3507" s="T138">det-bi</ta>
            <ta e="T140" id="Seg_3508" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_3509" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_3510" s="T141">bos-də</ta>
            <ta e="T143" id="Seg_3511" s="T142">kien</ta>
            <ta e="T144" id="Seg_3512" s="T143">kunol-bi</ta>
            <ta e="T145" id="Seg_3513" s="T144">i-bi</ta>
            <ta e="T146" id="Seg_3514" s="T145">dĭ</ta>
            <ta e="T147" id="Seg_3515" s="T146">pʼoruš</ta>
            <ta e="T148" id="Seg_3516" s="T147">bam-bi</ta>
            <ta e="T149" id="Seg_3517" s="T148">uda-gəndə</ta>
            <ta e="T150" id="Seg_3518" s="T149">uda-gəndə</ta>
            <ta e="T151" id="Seg_3519" s="T150">baʔbdə-luʔbdə-bi</ta>
            <ta e="T152" id="Seg_3520" s="T151">dĭ</ta>
            <ta e="T153" id="Seg_3521" s="T152">bar</ta>
            <ta e="T154" id="Seg_3522" s="T153">nʼi</ta>
            <ta e="T155" id="Seg_3523" s="T154">mo-luʔbdə-bi</ta>
            <ta e="T156" id="Seg_3524" s="T155">bar</ta>
            <ta e="T157" id="Seg_3525" s="T156">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T158" id="Seg_3526" s="T157">a</ta>
            <ta e="T159" id="Seg_3527" s="T158">dĭ-n</ta>
            <ta e="T160" id="Seg_3528" s="T159">sʼestra-zAŋ-də</ta>
            <ta e="T161" id="Seg_3529" s="T160">măndo-bi-jəʔ</ta>
            <ta e="T162" id="Seg_3530" s="T161">što</ta>
            <ta e="T163" id="Seg_3531" s="T162">dĭn</ta>
            <ta e="T164" id="Seg_3532" s="T163">nʼi</ta>
            <ta e="T165" id="Seg_3533" s="T164">i-gA</ta>
            <ta e="T166" id="Seg_3534" s="T165">dĭgəttə</ta>
            <ta e="T167" id="Seg_3535" s="T166">dĭ</ta>
            <ta e="T169" id="Seg_3536" s="T168">dĭn</ta>
            <ta e="T170" id="Seg_3537" s="T169">kuznek-də</ta>
            <ta e="T171" id="Seg_3538" s="T170">i-bi</ta>
            <ta e="T172" id="Seg_3539" s="T171">üdʼüge</ta>
            <ta e="T173" id="Seg_3540" s="T172">dĭn</ta>
            <ta e="T174" id="Seg_3541" s="T173">bar</ta>
            <ta e="T175" id="Seg_3542" s="T174">dĭbər</ta>
            <ta e="T176" id="Seg_3543" s="T175">tagaj-jəʔ</ta>
            <ta e="T178" id="Seg_3544" s="T177">müʔbdə-də-bi-jəʔ</ta>
            <ta e="T179" id="Seg_3545" s="T178">a</ta>
            <ta e="T180" id="Seg_3546" s="T179">dĭ-m</ta>
            <ta e="T182" id="Seg_3547" s="T181">bĭs-lə-bi-jəʔ</ta>
            <ta e="T183" id="Seg_3548" s="T182">dĭrgit</ta>
            <ta e="T184" id="Seg_3549" s="T183">bü</ta>
            <ta e="T185" id="Seg_3550" s="T184">što</ta>
            <ta e="T186" id="Seg_3551" s="T185">kunol-bi</ta>
            <ta e="T187" id="Seg_3552" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_3553" s="T187">šo-bi</ta>
            <ta e="T189" id="Seg_3554" s="T188">kirgaːr-bi</ta>
            <ta e="T190" id="Seg_3555" s="T189">kirgaːr-bi</ta>
            <ta e="T191" id="Seg_3556" s="T190">üjü-ttə</ta>
            <ta e="T192" id="Seg_3557" s="T191">băt-bi</ta>
            <ta e="T193" id="Seg_3558" s="T192">i</ta>
            <ta e="T453" id="Seg_3559" s="T193">kan-lAʔ</ta>
            <ta e="T194" id="Seg_3560" s="T453">tʼür-bi</ta>
            <ta e="T196" id="Seg_3561" s="T195">dĭ-n</ta>
            <ta e="T197" id="Seg_3562" s="T196">sʼestra-zAŋ-də</ta>
            <ta e="T198" id="Seg_3563" s="T197">dĭ-Tə</ta>
            <ta e="T199" id="Seg_3564" s="T198">mĭ-bi-jəʔ</ta>
            <ta e="T200" id="Seg_3565" s="T199">kaplʼa-jəʔ</ta>
            <ta e="T201" id="Seg_3566" s="T200">štobɨ</ta>
            <ta e="T202" id="Seg_3567" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_3568" s="T202">kunol-luʔbdə-bi</ta>
            <ta e="T204" id="Seg_3569" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_3570" s="T204">bar</ta>
            <ta e="T206" id="Seg_3571" s="T205">kunol-luʔbdə-bi</ta>
            <ta e="T207" id="Seg_3572" s="T206">a</ta>
            <ta e="T208" id="Seg_3573" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_3574" s="T208">šo-bi</ta>
            <ta e="T210" id="Seg_3575" s="T209">kuznek-də</ta>
            <ta e="T211" id="Seg_3576" s="T210">dĭ-zAŋ</ta>
            <ta e="T212" id="Seg_3577" s="T211">dĭn</ta>
            <ta e="T213" id="Seg_3578" s="T212">müʔbdə-bi-jəʔ</ta>
            <ta e="T214" id="Seg_3579" s="T213">tagaj-jəʔ</ta>
            <ta e="T215" id="Seg_3580" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_3581" s="T215">bar</ta>
            <ta e="T217" id="Seg_3582" s="T216">šü-zittə</ta>
            <ta e="T218" id="Seg_3583" s="T217">xatʼel</ta>
            <ta e="T219" id="Seg_3584" s="T218">üjü-bə</ta>
            <ta e="T220" id="Seg_3585" s="T219">bar</ta>
            <ta e="T221" id="Seg_3586" s="T220">băt-luʔbdə-bi</ta>
            <ta e="T222" id="Seg_3587" s="T221">kem</ta>
            <ta e="T223" id="Seg_3588" s="T222">mʼaŋ-luʔbdə-bi</ta>
            <ta e="T224" id="Seg_3589" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_3590" s="T224">sazən</ta>
            <ta e="T226" id="Seg_3591" s="T225">pʼaŋdə-bi</ta>
            <ta e="T227" id="Seg_3592" s="T226">i</ta>
            <ta e="T229" id="Seg_3593" s="T228">hen-bi</ta>
            <ta e="T230" id="Seg_3594" s="T229">kamən</ta>
            <ta e="T231" id="Seg_3595" s="T230">baza-j</ta>
            <ta e="T232" id="Seg_3596" s="T231">jama-jəʔ</ta>
            <ta e="T233" id="Seg_3597" s="T232">a-lV-l</ta>
            <ta e="T234" id="Seg_3598" s="T233">iʔt-lV-l</ta>
            <ta e="T235" id="Seg_3599" s="T234">dĭgəttə</ta>
            <ta e="T236" id="Seg_3600" s="T235">măna</ta>
            <ta e="T237" id="Seg_3601" s="T236">ku-lV-l</ta>
            <ta e="T239" id="Seg_3602" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_3603" s="T239">dĭ</ta>
            <ta e="T241" id="Seg_3604" s="T240">uʔbdə-bi</ta>
            <ta e="T242" id="Seg_3605" s="T241">ku-liA-t</ta>
            <ta e="T243" id="Seg_3606" s="T242">bar</ta>
            <ta e="T244" id="Seg_3607" s="T243">kem</ta>
            <ta e="T245" id="Seg_3608" s="T244">dön</ta>
            <ta e="T246" id="Seg_3609" s="T245">i</ta>
            <ta e="T247" id="Seg_3610" s="T246">sazən</ta>
            <ta e="T248" id="Seg_3611" s="T247">iʔbö-laʔbə</ta>
            <ta e="T249" id="Seg_3612" s="T248">dĭ</ta>
            <ta e="T250" id="Seg_3613" s="T249">sazən</ta>
            <ta e="T251" id="Seg_3614" s="T250">măndo-bi</ta>
            <ta e="T252" id="Seg_3615" s="T251">bar</ta>
            <ta e="T253" id="Seg_3616" s="T252">tʼor-bi</ta>
            <ta e="T254" id="Seg_3617" s="T253">tʼor-bi</ta>
            <ta e="T255" id="Seg_3618" s="T254">da</ta>
            <ta e="T256" id="Seg_3619" s="T255">kan-bi</ta>
            <ta e="T257" id="Seg_3620" s="T256">kuznʼitsa-Tə</ta>
            <ta e="T258" id="Seg_3621" s="T257">jama-jəʔ</ta>
            <ta e="T259" id="Seg_3622" s="T258">a-bi</ta>
            <ta e="T260" id="Seg_3623" s="T259">baza-j</ta>
            <ta e="T261" id="Seg_3624" s="T260">pa</ta>
            <ta e="T262" id="Seg_3625" s="T261">a-bi</ta>
            <ta e="T263" id="Seg_3626" s="T262">baza-j</ta>
            <ta e="T264" id="Seg_3627" s="T263">dĭgəttə</ta>
            <ta e="T265" id="Seg_3628" s="T264">kan-bi</ta>
            <ta e="T266" id="Seg_3629" s="T265">kan-bi</ta>
            <ta e="T267" id="Seg_3630" s="T266">kan-bi</ta>
            <ta e="T268" id="Seg_3631" s="T267">kondʼo-m</ta>
            <ta e="T269" id="Seg_3632" s="T268">šonə-gA</ta>
            <ta e="T270" id="Seg_3633" s="T269">šonə-bi</ta>
            <ta e="T271" id="Seg_3634" s="T270">dĭgəttə</ta>
            <ta e="T272" id="Seg_3635" s="T271">ku-liA-t</ta>
            <ta e="T273" id="Seg_3636" s="T272">ku-laʔbə</ta>
            <ta e="T274" id="Seg_3637" s="T273">tura</ta>
            <ta e="T276" id="Seg_3638" s="T275">nu-gA</ta>
            <ta e="T277" id="Seg_3639" s="T276">idʼiʔeʔe</ta>
            <ta e="T278" id="Seg_3640" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_3641" s="T278">tura-Tə</ta>
            <ta e="T280" id="Seg_3642" s="T279">šo-bi</ta>
            <ta e="T281" id="Seg_3643" s="T280">dĭn</ta>
            <ta e="T282" id="Seg_3644" s="T281">nüke</ta>
            <ta e="T283" id="Seg_3645" s="T282">amno-laʔbə</ta>
            <ta e="T284" id="Seg_3646" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_3647" s="T284">surar-laʔbə</ta>
            <ta e="T286" id="Seg_3648" s="T285">nüke</ta>
            <ta e="T287" id="Seg_3649" s="T286">nüke</ta>
            <ta e="T288" id="Seg_3650" s="T287">nörbə-t</ta>
            <ta e="T289" id="Seg_3651" s="T288">gijen</ta>
            <ta e="T290" id="Seg_3652" s="T289">măn</ta>
            <ta e="T291" id="Seg_3653" s="T290">tibi-m</ta>
            <ta e="T454" id="Seg_3654" s="T291">kan-lAʔ</ta>
            <ta e="T292" id="Seg_3655" s="T454">tʼür-bi</ta>
            <ta e="T293" id="Seg_3656" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_3657" s="T293">măn-ntə</ta>
            <ta e="T295" id="Seg_3658" s="T294">măn</ta>
            <ta e="T296" id="Seg_3659" s="T295">ej</ta>
            <ta e="T297" id="Seg_3660" s="T296">tĭmne-m</ta>
            <ta e="T298" id="Seg_3661" s="T297">gijen</ta>
            <ta e="T300" id="Seg_3662" s="T299">gijen=də</ta>
            <ta e="T301" id="Seg_3663" s="T300">kan-ə-ʔ</ta>
            <ta e="T302" id="Seg_3664" s="T301">măn</ta>
            <ta e="T303" id="Seg_3665" s="T302">sʼestra-gəndə</ta>
            <ta e="T304" id="Seg_3666" s="T303">dĭ</ta>
            <ta e="T305" id="Seg_3667" s="T304">tĭmne-t</ta>
            <ta e="T306" id="Seg_3668" s="T305">nörbə-lV-j</ta>
            <ta e="T307" id="Seg_3669" s="T306">tănan</ta>
            <ta e="T308" id="Seg_3670" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_3671" s="T308">dĭ-Tə</ta>
            <ta e="T310" id="Seg_3672" s="T309">mĭ-bi</ta>
            <ta e="T311" id="Seg_3673" s="T310">takše</ta>
            <ta e="T312" id="Seg_3674" s="T311">zălătoj</ta>
            <ta e="T313" id="Seg_3675" s="T312">munəj-jəʔ</ta>
            <ta e="T314" id="Seg_3676" s="T313">zălătoj</ta>
            <ta e="T315" id="Seg_3677" s="T314">i</ta>
            <ta e="T316" id="Seg_3678" s="T315">nʼitka-jəʔ</ta>
            <ta e="T317" id="Seg_3679" s="T316">klubok</ta>
            <ta e="T318" id="Seg_3680" s="T317">kan-ə-ʔ</ta>
            <ta e="T319" id="Seg_3681" s="T318">dĭ</ta>
            <ta e="T320" id="Seg_3682" s="T319">nuʔmə-lV-j</ta>
            <ta e="T321" id="Seg_3683" s="T320">i</ta>
            <ta e="T322" id="Seg_3684" s="T321">tăn</ta>
            <ta e="T323" id="Seg_3685" s="T322">kan-ə-ʔ</ta>
            <ta e="T324" id="Seg_3686" s="T323">dĭgəttə</ta>
            <ta e="T325" id="Seg_3687" s="T324">dĭ</ta>
            <ta e="T326" id="Seg_3688" s="T325">kan-bi</ta>
            <ta e="T328" id="Seg_3689" s="T327">dĭgəttə</ta>
            <ta e="T329" id="Seg_3690" s="T328">dĭ</ta>
            <ta e="T330" id="Seg_3691" s="T329">kan-bi</ta>
            <ta e="T331" id="Seg_3692" s="T330">kan-bi</ta>
            <ta e="T332" id="Seg_3693" s="T331">baška</ta>
            <ta e="T333" id="Seg_3694" s="T332">sʼestra-gəndə</ta>
            <ta e="T334" id="Seg_3695" s="T333">šo-bi</ta>
            <ta e="T335" id="Seg_3696" s="T334">tože</ta>
            <ta e="T336" id="Seg_3697" s="T335">tura-t</ta>
            <ta e="T337" id="Seg_3698" s="T336">ej</ta>
            <ta e="T340" id="Seg_3699" s="T339">üdʼüge</ta>
            <ta e="T341" id="Seg_3700" s="T340">tura-t</ta>
            <ta e="T342" id="Seg_3701" s="T341">šü-bi</ta>
            <ta e="T343" id="Seg_3702" s="T342">tura-Tə</ta>
            <ta e="T345" id="Seg_3703" s="T344">dĭn</ta>
            <ta e="T346" id="Seg_3704" s="T345">dĭn</ta>
            <ta e="T347" id="Seg_3705" s="T346">nüke</ta>
            <ta e="T348" id="Seg_3706" s="T347">amno-laʔbə</ta>
            <ta e="T349" id="Seg_3707" s="T348">măn-ntə</ta>
            <ta e="T350" id="Seg_3708" s="T349">nüke</ta>
            <ta e="T351" id="Seg_3709" s="T350">nüke</ta>
            <ta e="T352" id="Seg_3710" s="T351">nörbə-ʔ</ta>
            <ta e="T353" id="Seg_3711" s="T352">măna</ta>
            <ta e="T354" id="Seg_3712" s="T353">gijen</ta>
            <ta e="T355" id="Seg_3713" s="T354">măn</ta>
            <ta e="T356" id="Seg_3714" s="T355">tibi</ta>
            <ta e="T455" id="Seg_3715" s="T356">kan-lAʔ</ta>
            <ta e="T357" id="Seg_3716" s="T455">tʼür-bi</ta>
            <ta e="T358" id="Seg_3717" s="T357">măn</ta>
            <ta e="T359" id="Seg_3718" s="T358">ej</ta>
            <ta e="T360" id="Seg_3719" s="T359">tĭmne-m</ta>
            <ta e="T361" id="Seg_3720" s="T360">kan-ə-ʔ</ta>
            <ta e="T362" id="Seg_3721" s="T361">măn</ta>
            <ta e="T363" id="Seg_3722" s="T362">sʼestra-gəndə</ta>
            <ta e="T364" id="Seg_3723" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_3724" s="T364">nörbə-lV-j</ta>
            <ta e="T366" id="Seg_3725" s="T365">gijen</ta>
            <ta e="T367" id="Seg_3726" s="T366">dĭ</ta>
            <ta e="T368" id="Seg_3727" s="T367">dĭ</ta>
            <ta e="T369" id="Seg_3728" s="T368">dĭ-Tə</ta>
            <ta e="T370" id="Seg_3729" s="T369">mĭn-bi</ta>
            <ta e="T371" id="Seg_3730" s="T370">mĭ-bi</ta>
            <ta e="T372" id="Seg_3731" s="T371">prʼalka</ta>
            <ta e="T373" id="Seg_3732" s="T372">i</ta>
            <ta e="T374" id="Seg_3733" s="T373">mĭ-bi</ta>
            <ta e="T375" id="Seg_3734" s="T374">kugʼelʼi</ta>
            <ta e="T376" id="Seg_3735" s="T375">prʼalka</ta>
            <ta e="T377" id="Seg_3736" s="T376">bar</ta>
            <ta e="T378" id="Seg_3737" s="T377">zălătoj</ta>
            <ta e="T379" id="Seg_3738" s="T378">i</ta>
            <ta e="T380" id="Seg_3739" s="T379">kugʼelʼi</ta>
            <ta e="T381" id="Seg_3740" s="T380">zălătoj</ta>
            <ta e="T382" id="Seg_3741" s="T381">dĭgəttə</ta>
            <ta e="T384" id="Seg_3742" s="T383">mĭ-bi</ta>
            <ta e="T385" id="Seg_3743" s="T384">klubok</ta>
            <ta e="T386" id="Seg_3744" s="T385">dĭ</ta>
            <ta e="T388" id="Seg_3745" s="T387">kandə-lV-j</ta>
            <ta e="T389" id="Seg_3746" s="T388">tăn</ta>
            <ta e="T391" id="Seg_3747" s="T390">kan-ə-ʔ</ta>
            <ta e="T392" id="Seg_3748" s="T391">dĭbər</ta>
            <ta e="T393" id="Seg_3749" s="T392">gibər</ta>
            <ta e="T394" id="Seg_3750" s="T393">dĭ</ta>
            <ta e="T395" id="Seg_3751" s="T394">kandə-lV-j</ta>
            <ta e="T396" id="Seg_3752" s="T395">dĭgəttə</ta>
            <ta e="T397" id="Seg_3753" s="T396">dö</ta>
            <ta e="T398" id="Seg_3754" s="T397">kan-bi</ta>
            <ta e="T399" id="Seg_3755" s="T398">kan-bi</ta>
            <ta e="T400" id="Seg_3756" s="T399">nagur</ta>
            <ta e="T401" id="Seg_3757" s="T400">sʼestra-gəndə</ta>
            <ta e="T402" id="Seg_3758" s="T401">šo-bi</ta>
            <ta e="T403" id="Seg_3759" s="T402">tože</ta>
            <ta e="T404" id="Seg_3760" s="T403">tura</ta>
            <ta e="T405" id="Seg_3761" s="T404">nu-laʔbə</ta>
            <ta e="T406" id="Seg_3762" s="T405">dĭ</ta>
            <ta e="T407" id="Seg_3763" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_3764" s="T407">dĭn</ta>
            <ta e="T409" id="Seg_3765" s="T408">nüke</ta>
            <ta e="T410" id="Seg_3766" s="T409">amno-laʔbə</ta>
            <ta e="T411" id="Seg_3767" s="T410">ugaːndə</ta>
            <ta e="T412" id="Seg_3768" s="T411">dĭgəttə</ta>
            <ta e="T413" id="Seg_3769" s="T412">măn-ntə</ta>
            <ta e="T414" id="Seg_3770" s="T413">nüke</ta>
            <ta e="T415" id="Seg_3771" s="T414">nüke</ta>
            <ta e="T416" id="Seg_3772" s="T415">nörbə-ʔ</ta>
            <ta e="T417" id="Seg_3773" s="T416">măna</ta>
            <ta e="T418" id="Seg_3774" s="T417">gijen</ta>
            <ta e="T419" id="Seg_3775" s="T418">măn</ta>
            <ta e="T420" id="Seg_3776" s="T419">tibi-m</ta>
            <ta e="T456" id="Seg_3777" s="T420">kan-lAʔ</ta>
            <ta e="T421" id="Seg_3778" s="T456">tʼür-bi</ta>
            <ta e="T422" id="Seg_3779" s="T421">măn</ta>
            <ta e="T423" id="Seg_3780" s="T422">tĭmne-m</ta>
            <ta e="T424" id="Seg_3781" s="T423">kan-ə-ʔ</ta>
            <ta e="T425" id="Seg_3782" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_3783" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_3784" s="T426">mĭ-bi</ta>
            <ta e="T428" id="Seg_3785" s="T427">dĭ-Tə</ta>
            <ta e="T429" id="Seg_3786" s="T428">korniš</ta>
            <ta e="T430" id="Seg_3787" s="T429">kuvas</ta>
            <ta e="T431" id="Seg_3788" s="T430">zălătoj</ta>
            <ta e="T432" id="Seg_3789" s="T431">bar</ta>
            <ta e="T434" id="Seg_3790" s="T433">kalʼečka</ta>
            <ta e="T435" id="Seg_3791" s="T434">mĭ-bi</ta>
            <ta e="T436" id="Seg_3792" s="T435">zălătoj</ta>
            <ta e="T437" id="Seg_3793" s="T436">uda-gəndə</ta>
            <ta e="T438" id="Seg_3794" s="T437">kan-ə-ʔ</ta>
            <ta e="T439" id="Seg_3795" s="T438">gijen=də</ta>
            <ta e="T440" id="Seg_3796" s="T439">amno-laʔbə</ta>
            <ta e="T441" id="Seg_3797" s="T440">dĭ-Tə</ta>
            <ta e="T442" id="Seg_3798" s="T441">mĭ-bi</ta>
            <ta e="T443" id="Seg_3799" s="T442">tože</ta>
            <ta e="T445" id="Seg_3800" s="T444">klubok</ta>
            <ta e="T446" id="Seg_3801" s="T445">gibər</ta>
            <ta e="T447" id="Seg_3802" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_3803" s="T447">kandə-lV-j</ta>
            <ta e="T449" id="Seg_3804" s="T448">tăn</ta>
            <ta e="T450" id="Seg_3805" s="T449">dĭbər</ta>
            <ta e="T451" id="Seg_3806" s="T450">kan-ə-ʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3807" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_3808" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3809" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_3810" s="T3">this-GEN</ta>
            <ta e="T5" id="Seg_3811" s="T4">three.[NOM.SG]</ta>
            <ta e="T6" id="Seg_3812" s="T5">girl.[NOM.SG]</ta>
            <ta e="T7" id="Seg_3813" s="T6">be-PST.[3SG]</ta>
            <ta e="T8" id="Seg_3814" s="T7">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_3815" s="T9">walk-PRS.[3SG]</ta>
            <ta e="T11" id="Seg_3816" s="T10">bread.[NOM.SG]</ta>
            <ta e="T12" id="Seg_3817" s="T11">sell-INF.LAT</ta>
            <ta e="T13" id="Seg_3818" s="T12">what.[NOM.SG]</ta>
            <ta e="T14" id="Seg_3819" s="T13">you.PL.LAT</ta>
            <ta e="T15" id="Seg_3820" s="T14">take-INF.LAT</ta>
            <ta e="T16" id="Seg_3821" s="T15">one.[NOM.SG]</ta>
            <ta e="T17" id="Seg_3822" s="T16">say-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_3823" s="T17">I.LAT</ta>
            <ta e="T19" id="Seg_3824" s="T18">take-IMP.2SG</ta>
            <ta e="T20" id="Seg_3825" s="T19">beautiful.[NOM.SG]</ta>
            <ta e="T21" id="Seg_3826" s="T20">coat.[NOM.SG]</ta>
            <ta e="T22" id="Seg_3827" s="T21">one.[NOM.SG]</ta>
            <ta e="T23" id="Seg_3828" s="T22">say-IPFVZ.[3SG]</ta>
            <ta e="T24" id="Seg_3829" s="T23">I.LAT</ta>
            <ta e="T25" id="Seg_3830" s="T24">beautiful.[NOM.SG]</ta>
            <ta e="T26" id="Seg_3831" s="T25">scarp.[NOM.SG]</ta>
            <ta e="T28" id="Seg_3832" s="T27">take-IMP.2SG</ta>
            <ta e="T29" id="Seg_3833" s="T28">and</ta>
            <ta e="T30" id="Seg_3834" s="T29">beautiful.[NOM.SG]</ta>
            <ta e="T31" id="Seg_3835" s="T30">dress.[NOM.SG]</ta>
            <ta e="T32" id="Seg_3836" s="T31">take-IMP.2SG</ta>
            <ta e="T33" id="Seg_3837" s="T32">and</ta>
            <ta e="T34" id="Seg_3838" s="T33">small.[NOM.SG]</ta>
            <ta e="T36" id="Seg_3839" s="T35">and</ta>
            <ta e="T37" id="Seg_3840" s="T36">you.DAT</ta>
            <ta e="T38" id="Seg_3841" s="T37">what.[NOM.SG]</ta>
            <ta e="T39" id="Seg_3842" s="T38">take-INF.LAT</ta>
            <ta e="T40" id="Seg_3843" s="T39">I.LAT</ta>
            <ta e="T41" id="Seg_3844" s="T40">take-IMP.2SG</ta>
            <ta e="T42" id="Seg_3845" s="T41">Finist.the.Falcon.[NOM.SG]</ta>
            <ta e="T43" id="Seg_3846" s="T42">feather.DIM.[NOM.SG]</ta>
            <ta e="T45" id="Seg_3847" s="T44">this.[NOM.SG]</ta>
            <ta e="T46" id="Seg_3848" s="T45">say-IPFVZ.[3SG]</ta>
            <ta e="T47" id="Seg_3849" s="T46">take-IMP.2SG</ta>
            <ta e="T48" id="Seg_3850" s="T47">I.LAT</ta>
            <ta e="T49" id="Seg_3851" s="T48">Finist.the.Falcon.[NOM.SG]</ta>
            <ta e="T50" id="Seg_3852" s="T49">feather.DIM.[NOM.SG]</ta>
            <ta e="T51" id="Seg_3853" s="T50">this.[NOM.SG]</ta>
            <ta e="T52" id="Seg_3854" s="T51">go-PST.[3SG]</ta>
            <ta e="T53" id="Seg_3855" s="T52">this-LAT</ta>
            <ta e="T54" id="Seg_3856" s="T53">take-PST.[3SG]</ta>
            <ta e="T55" id="Seg_3857" s="T54">coat.[NOM.SG]</ta>
            <ta e="T56" id="Seg_3858" s="T55">this-LAT</ta>
            <ta e="T57" id="Seg_3859" s="T56">scarp.[NOM.SG]</ta>
            <ta e="T58" id="Seg_3860" s="T57">take-PST.[3SG]</ta>
            <ta e="T59" id="Seg_3861" s="T58">and</ta>
            <ta e="T61" id="Seg_3862" s="T60">and</ta>
            <ta e="T62" id="Seg_3863" s="T61">dress.[NOM.SG]</ta>
            <ta e="T63" id="Seg_3864" s="T62">take-PST.[3SG]</ta>
            <ta e="T64" id="Seg_3865" s="T63">look-FRQ-PST.[3SG]</ta>
            <ta e="T65" id="Seg_3866" s="T64">look-FRQ-PST.[3SG]</ta>
            <ta e="T66" id="Seg_3867" s="T65">NEG</ta>
            <ta e="T67" id="Seg_3868" s="T66">find-PST.[3SG]</ta>
            <ta e="T68" id="Seg_3869" s="T67">then</ta>
            <ta e="T69" id="Seg_3870" s="T68">come-PST.[3SG]</ta>
            <ta e="T70" id="Seg_3871" s="T69">tent-LAT/LOC.3SG</ta>
            <ta e="T71" id="Seg_3872" s="T70">this</ta>
            <ta e="T72" id="Seg_3873" s="T71">this</ta>
            <ta e="T73" id="Seg_3874" s="T72">two.[NOM.SG]</ta>
            <ta e="T74" id="Seg_3875" s="T73">daughter.[NOM.SG]</ta>
            <ta e="T75" id="Seg_3876" s="T74">laugh-DUR-3PL</ta>
            <ta e="T76" id="Seg_3877" s="T75">you.DAT</ta>
            <ta e="T77" id="Seg_3878" s="T76">what.[NOM.SG]=INDEF</ta>
            <ta e="T78" id="Seg_3879" s="T77">NEG</ta>
            <ta e="T79" id="Seg_3880" s="T78">take-PST.[3SG]</ta>
            <ta e="T80" id="Seg_3881" s="T79">and</ta>
            <ta e="T81" id="Seg_3882" s="T80">we.LAT</ta>
            <ta e="T82" id="Seg_3883" s="T81">take-PST.[3SG]</ta>
            <ta e="T83" id="Seg_3884" s="T82">well</ta>
            <ta e="T84" id="Seg_3885" s="T83">and</ta>
            <ta e="T85" id="Seg_3886" s="T84">that</ta>
            <ta e="T86" id="Seg_3887" s="T85">NEG</ta>
            <ta e="T87" id="Seg_3888" s="T86">take-PST.[3SG]</ta>
            <ta e="T88" id="Seg_3889" s="T87">so</ta>
            <ta e="T89" id="Seg_3890" s="T88">JUSS</ta>
            <ta e="T90" id="Seg_3891" s="T89">NEG</ta>
            <ta e="T91" id="Seg_3892" s="T90">take-FUT-3SG</ta>
            <ta e="T92" id="Seg_3893" s="T91">then</ta>
            <ta e="T93" id="Seg_3894" s="T92">this.[NOM.SG]</ta>
            <ta e="T94" id="Seg_3895" s="T93">again</ta>
            <ta e="T95" id="Seg_3896" s="T94">go-PST.[3SG]</ta>
            <ta e="T96" id="Seg_3897" s="T95">bread-INS</ta>
            <ta e="T97" id="Seg_3898" s="T96">what.[NOM.SG]</ta>
            <ta e="T98" id="Seg_3899" s="T97">take-INF.LAT</ta>
            <ta e="T99" id="Seg_3900" s="T98">then</ta>
            <ta e="T100" id="Seg_3901" s="T99">this-PL</ta>
            <ta e="T101" id="Seg_3902" s="T100">say-IPFVZ-3PL</ta>
            <ta e="T102" id="Seg_3903" s="T101">shoe-PL</ta>
            <ta e="T103" id="Seg_3904" s="T102">take-IMP.2SG</ta>
            <ta e="T104" id="Seg_3905" s="T103">beautiful.[NOM.SG]</ta>
            <ta e="T105" id="Seg_3906" s="T104">this.[NOM.SG]</ta>
            <ta e="T106" id="Seg_3907" s="T105">go-PST.[3SG]</ta>
            <ta e="T107" id="Seg_3908" s="T106">this-PL-LAT</ta>
            <ta e="T108" id="Seg_3909" s="T107">take-PST.[3SG]</ta>
            <ta e="T109" id="Seg_3910" s="T108">and</ta>
            <ta e="T110" id="Seg_3911" s="T109">and</ta>
            <ta e="T111" id="Seg_3912" s="T110">small.[NOM.SG]</ta>
            <ta e="T112" id="Seg_3913" s="T111">say-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_3914" s="T112">I.LAT</ta>
            <ta e="T114" id="Seg_3915" s="T113">take-IMP.2SG</ta>
            <ta e="T115" id="Seg_3916" s="T114">Finist.the.Falcon.[NOM.SG]</ta>
            <ta e="T116" id="Seg_3917" s="T115">feather.DIM.[NOM.SG]</ta>
            <ta e="T117" id="Seg_3918" s="T116">this.[NOM.SG]</ta>
            <ta e="T118" id="Seg_3919" s="T117">go-PST.[3SG]</ta>
            <ta e="T119" id="Seg_3920" s="T118">this-PL-LAT</ta>
            <ta e="T120" id="Seg_3921" s="T119">take-PST.[3SG]</ta>
            <ta e="T121" id="Seg_3922" s="T120">and</ta>
            <ta e="T122" id="Seg_3923" s="T121">this-ACC</ta>
            <ta e="T123" id="Seg_3924" s="T122">NEG</ta>
            <ta e="T124" id="Seg_3925" s="T123">find-PST.[3SG]</ta>
            <ta e="T125" id="Seg_3926" s="T124">then</ta>
            <ta e="T126" id="Seg_3927" s="T125">three-LAT.ADV</ta>
            <ta e="T127" id="Seg_3928" s="T126">go-PST.[3SG]</ta>
            <ta e="T128" id="Seg_3929" s="T127">then</ta>
            <ta e="T129" id="Seg_3930" s="T128">this-PL-LAT</ta>
            <ta e="T130" id="Seg_3931" s="T129">take-PST.[3SG]</ta>
            <ta e="T131" id="Seg_3932" s="T130">all</ta>
            <ta e="T132" id="Seg_3933" s="T131">what.[NOM.SG]</ta>
            <ta e="T133" id="Seg_3934" s="T132">and</ta>
            <ta e="T134" id="Seg_3935" s="T133">this-LAT</ta>
            <ta e="T135" id="Seg_3936" s="T134">find-PST.[3SG]</ta>
            <ta e="T136" id="Seg_3937" s="T135">this.[NOM.SG]</ta>
            <ta e="T137" id="Seg_3938" s="T136">feather.[NOM.SG]</ta>
            <ta e="T138" id="Seg_3939" s="T137">and</ta>
            <ta e="T139" id="Seg_3940" s="T138">bring-PST.[3SG]</ta>
            <ta e="T140" id="Seg_3941" s="T139">then</ta>
            <ta e="T141" id="Seg_3942" s="T140">this.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3943" s="T141">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T143" id="Seg_3944" s="T142">%%</ta>
            <ta e="T144" id="Seg_3945" s="T143">sleep-PST.[3SG]</ta>
            <ta e="T145" id="Seg_3946" s="T144">take-PST.[3SG]</ta>
            <ta e="T146" id="Seg_3947" s="T145">this.[NOM.SG]</ta>
            <ta e="T147" id="Seg_3948" s="T146">feather.[NOM.SG]</ta>
            <ta e="T148" id="Seg_3949" s="T147">%%-PST.[3SG]</ta>
            <ta e="T149" id="Seg_3950" s="T148">hand-LAT/LOC.3SG</ta>
            <ta e="T150" id="Seg_3951" s="T149">hand-LAT/LOC.3SG</ta>
            <ta e="T151" id="Seg_3952" s="T150">throw-MOM-PST.[3SG]</ta>
            <ta e="T152" id="Seg_3953" s="T151">this.[NOM.SG]</ta>
            <ta e="T153" id="Seg_3954" s="T152">PTCL</ta>
            <ta e="T154" id="Seg_3955" s="T153">boy.[NOM.SG]</ta>
            <ta e="T155" id="Seg_3956" s="T154">become-MOM-PST.[3SG]</ta>
            <ta e="T156" id="Seg_3957" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_3958" s="T156">speak-DUR-3PL</ta>
            <ta e="T158" id="Seg_3959" s="T157">and</ta>
            <ta e="T159" id="Seg_3960" s="T158">this-GEN</ta>
            <ta e="T160" id="Seg_3961" s="T159">sister-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T161" id="Seg_3962" s="T160">look-PST-3PL</ta>
            <ta e="T162" id="Seg_3963" s="T161">that</ta>
            <ta e="T163" id="Seg_3964" s="T162">there</ta>
            <ta e="T164" id="Seg_3965" s="T163">boy.[NOM.SG]</ta>
            <ta e="T165" id="Seg_3966" s="T164">be-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_3967" s="T165">then</ta>
            <ta e="T167" id="Seg_3968" s="T166">this.[NOM.SG]</ta>
            <ta e="T169" id="Seg_3969" s="T168">there</ta>
            <ta e="T170" id="Seg_3970" s="T169">window-NOM/GEN/ACC.3SG</ta>
            <ta e="T171" id="Seg_3971" s="T170">be-PST.[3SG]</ta>
            <ta e="T172" id="Seg_3972" s="T171">small.[NOM.SG]</ta>
            <ta e="T173" id="Seg_3973" s="T172">there</ta>
            <ta e="T174" id="Seg_3974" s="T173">PTCL</ta>
            <ta e="T175" id="Seg_3975" s="T174">there</ta>
            <ta e="T176" id="Seg_3976" s="T175">knife-PL</ta>
            <ta e="T178" id="Seg_3977" s="T177">push-TR-PST-3PL</ta>
            <ta e="T179" id="Seg_3978" s="T178">and</ta>
            <ta e="T180" id="Seg_3979" s="T179">this-ACC</ta>
            <ta e="T182" id="Seg_3980" s="T181">drink-TR-PST-3PL</ta>
            <ta e="T183" id="Seg_3981" s="T182">such.[NOM.SG]</ta>
            <ta e="T184" id="Seg_3982" s="T183">water.[NOM.SG]</ta>
            <ta e="T185" id="Seg_3983" s="T184">that</ta>
            <ta e="T186" id="Seg_3984" s="T185">sleep-PST.[3SG]</ta>
            <ta e="T187" id="Seg_3985" s="T186">this.[NOM.SG]</ta>
            <ta e="T188" id="Seg_3986" s="T187">come-PST.[3SG]</ta>
            <ta e="T189" id="Seg_3987" s="T188">shout-PST.[3SG]</ta>
            <ta e="T190" id="Seg_3988" s="T189">shout-PST.[3SG]</ta>
            <ta e="T191" id="Seg_3989" s="T190">foot-ABL.3SG</ta>
            <ta e="T192" id="Seg_3990" s="T191">cut-PST.[3SG]</ta>
            <ta e="T193" id="Seg_3991" s="T192">and</ta>
            <ta e="T453" id="Seg_3992" s="T193">go-CVB</ta>
            <ta e="T194" id="Seg_3993" s="T453">disappear-PST.[3SG]</ta>
            <ta e="T196" id="Seg_3994" s="T195">this-GEN</ta>
            <ta e="T197" id="Seg_3995" s="T196">sister-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T198" id="Seg_3996" s="T197">this-LAT</ta>
            <ta e="T199" id="Seg_3997" s="T198">give-PST-3PL</ta>
            <ta e="T200" id="Seg_3998" s="T199">drop-PL</ta>
            <ta e="T201" id="Seg_3999" s="T200">so.that</ta>
            <ta e="T202" id="Seg_4000" s="T201">this.[NOM.SG]</ta>
            <ta e="T203" id="Seg_4001" s="T202">sleep-MOM-PST.[3SG]</ta>
            <ta e="T204" id="Seg_4002" s="T203">this.[NOM.SG]</ta>
            <ta e="T205" id="Seg_4003" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_4004" s="T205">sleep-MOM-PST.[3SG]</ta>
            <ta e="T207" id="Seg_4005" s="T206">and</ta>
            <ta e="T208" id="Seg_4006" s="T207">this.[NOM.SG]</ta>
            <ta e="T209" id="Seg_4007" s="T208">come-PST.[3SG]</ta>
            <ta e="T210" id="Seg_4008" s="T209">window-NOM/GEN/ACC.3SG</ta>
            <ta e="T211" id="Seg_4009" s="T210">this-PL</ta>
            <ta e="T212" id="Seg_4010" s="T211">there</ta>
            <ta e="T213" id="Seg_4011" s="T212">push-PST-3PL</ta>
            <ta e="T214" id="Seg_4012" s="T213">knife-PL</ta>
            <ta e="T215" id="Seg_4013" s="T214">this.[NOM.SG]</ta>
            <ta e="T216" id="Seg_4014" s="T215">PTCL</ta>
            <ta e="T217" id="Seg_4015" s="T216">enter-INF.LAT</ta>
            <ta e="T218" id="Seg_4016" s="T217">want.PST.M.SG</ta>
            <ta e="T219" id="Seg_4017" s="T218">foot-ACC.3SG</ta>
            <ta e="T220" id="Seg_4018" s="T219">PTCL</ta>
            <ta e="T221" id="Seg_4019" s="T220">cut-MOM-PST.[3SG]</ta>
            <ta e="T222" id="Seg_4020" s="T221">blood.[NOM.SG]</ta>
            <ta e="T223" id="Seg_4021" s="T222">flow-MOM-PST.[3SG]</ta>
            <ta e="T224" id="Seg_4022" s="T223">then</ta>
            <ta e="T225" id="Seg_4023" s="T224">paper.[NOM.SG]</ta>
            <ta e="T226" id="Seg_4024" s="T225">write-PST.[3SG]</ta>
            <ta e="T227" id="Seg_4025" s="T226">and</ta>
            <ta e="T229" id="Seg_4026" s="T228">put-PST.[3SG]</ta>
            <ta e="T230" id="Seg_4027" s="T229">when</ta>
            <ta e="T231" id="Seg_4028" s="T230">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T232" id="Seg_4029" s="T231">boot-PL</ta>
            <ta e="T233" id="Seg_4030" s="T232">make-FUT-2SG</ta>
            <ta e="T234" id="Seg_4031" s="T233">wear.out-FUT-2SG</ta>
            <ta e="T235" id="Seg_4032" s="T234">then</ta>
            <ta e="T236" id="Seg_4033" s="T235">I.ACC</ta>
            <ta e="T237" id="Seg_4034" s="T236">find-FUT-2SG</ta>
            <ta e="T239" id="Seg_4035" s="T238">then</ta>
            <ta e="T240" id="Seg_4036" s="T239">this.[NOM.SG]</ta>
            <ta e="T241" id="Seg_4037" s="T240">get.up-PST.[3SG]</ta>
            <ta e="T242" id="Seg_4038" s="T241">see-PRS-3SG.O</ta>
            <ta e="T243" id="Seg_4039" s="T242">PTCL</ta>
            <ta e="T244" id="Seg_4040" s="T243">blood.[NOM.SG]</ta>
            <ta e="T245" id="Seg_4041" s="T244">here</ta>
            <ta e="T246" id="Seg_4042" s="T245">and</ta>
            <ta e="T247" id="Seg_4043" s="T246">paper.[NOM.SG]</ta>
            <ta e="T248" id="Seg_4044" s="T247">lie-DUR.[3SG]</ta>
            <ta e="T249" id="Seg_4045" s="T248">this.[NOM.SG]</ta>
            <ta e="T250" id="Seg_4046" s="T249">paper.[NOM.SG]</ta>
            <ta e="T251" id="Seg_4047" s="T250">look-PST.[3SG]</ta>
            <ta e="T252" id="Seg_4048" s="T251">PTCL</ta>
            <ta e="T253" id="Seg_4049" s="T252">cry-PST.[3SG]</ta>
            <ta e="T254" id="Seg_4050" s="T253">cry-PST.[3SG]</ta>
            <ta e="T255" id="Seg_4051" s="T254">and</ta>
            <ta e="T256" id="Seg_4052" s="T255">go-PST.[3SG]</ta>
            <ta e="T257" id="Seg_4053" s="T256">smithy-LAT</ta>
            <ta e="T258" id="Seg_4054" s="T257">boot-PL</ta>
            <ta e="T259" id="Seg_4055" s="T258">make-PST.[3SG]</ta>
            <ta e="T260" id="Seg_4056" s="T259">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T261" id="Seg_4057" s="T260">tree.[NOM.SG]</ta>
            <ta e="T262" id="Seg_4058" s="T261">make-PST.[3SG]</ta>
            <ta e="T263" id="Seg_4059" s="T262">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T264" id="Seg_4060" s="T263">then</ta>
            <ta e="T265" id="Seg_4061" s="T264">go-PST.[3SG]</ta>
            <ta e="T266" id="Seg_4062" s="T265">go-PST.[3SG]</ta>
            <ta e="T267" id="Seg_4063" s="T266">go-PST.[3SG]</ta>
            <ta e="T268" id="Seg_4064" s="T267">long.time-%%</ta>
            <ta e="T269" id="Seg_4065" s="T268">come-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_4066" s="T269">come-PST.[3SG]</ta>
            <ta e="T271" id="Seg_4067" s="T270">then</ta>
            <ta e="T272" id="Seg_4068" s="T271">see-PRS-3SG.O</ta>
            <ta e="T273" id="Seg_4069" s="T272">see-DUR.[3SG]</ta>
            <ta e="T274" id="Seg_4070" s="T273">house.[NOM.SG]</ta>
            <ta e="T276" id="Seg_4071" s="T275">stand-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_4072" s="T276">a.few</ta>
            <ta e="T278" id="Seg_4073" s="T277">this.[NOM.SG]</ta>
            <ta e="T279" id="Seg_4074" s="T278">house-LAT</ta>
            <ta e="T280" id="Seg_4075" s="T279">come-PST.[3SG]</ta>
            <ta e="T281" id="Seg_4076" s="T280">there</ta>
            <ta e="T282" id="Seg_4077" s="T281">woman.[NOM.SG]</ta>
            <ta e="T283" id="Seg_4078" s="T282">live-DUR.[3SG]</ta>
            <ta e="T284" id="Seg_4079" s="T283">this.[NOM.SG]</ta>
            <ta e="T285" id="Seg_4080" s="T284">ask-DUR.[3SG]</ta>
            <ta e="T286" id="Seg_4081" s="T285">woman.[NOM.SG]</ta>
            <ta e="T287" id="Seg_4082" s="T286">woman.[NOM.SG]</ta>
            <ta e="T288" id="Seg_4083" s="T287">tell-IMP.2SG.O</ta>
            <ta e="T289" id="Seg_4084" s="T288">where</ta>
            <ta e="T290" id="Seg_4085" s="T289">I.NOM</ta>
            <ta e="T291" id="Seg_4086" s="T290">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T454" id="Seg_4087" s="T291">go-CVB</ta>
            <ta e="T292" id="Seg_4088" s="T454">disappear-PST.[3SG]</ta>
            <ta e="T293" id="Seg_4089" s="T292">this.[NOM.SG]</ta>
            <ta e="T294" id="Seg_4090" s="T293">say-IPFVZ.[3SG]</ta>
            <ta e="T295" id="Seg_4091" s="T294">I.NOM</ta>
            <ta e="T296" id="Seg_4092" s="T295">NEG</ta>
            <ta e="T297" id="Seg_4093" s="T296">know-1SG</ta>
            <ta e="T298" id="Seg_4094" s="T297">where</ta>
            <ta e="T300" id="Seg_4095" s="T299">where=INDEF</ta>
            <ta e="T301" id="Seg_4096" s="T300">go-EP-IMP.2SG</ta>
            <ta e="T302" id="Seg_4097" s="T301">I.NOM</ta>
            <ta e="T303" id="Seg_4098" s="T302">sister-LAT/LOC.3SG</ta>
            <ta e="T304" id="Seg_4099" s="T303">this.[NOM.SG]</ta>
            <ta e="T305" id="Seg_4100" s="T304">know-3SG.O</ta>
            <ta e="T306" id="Seg_4101" s="T305">tell-FUT-3SG</ta>
            <ta e="T307" id="Seg_4102" s="T306">you.DAT</ta>
            <ta e="T308" id="Seg_4103" s="T307">then</ta>
            <ta e="T309" id="Seg_4104" s="T308">this-LAT</ta>
            <ta e="T310" id="Seg_4105" s="T309">give-PST.[3SG]</ta>
            <ta e="T311" id="Seg_4106" s="T310">cup.[NOM.SG]</ta>
            <ta e="T312" id="Seg_4107" s="T311">golden.[NOM.SG]</ta>
            <ta e="T313" id="Seg_4108" s="T312">egg-PL</ta>
            <ta e="T314" id="Seg_4109" s="T313">golden.[NOM.SG]</ta>
            <ta e="T315" id="Seg_4110" s="T314">and</ta>
            <ta e="T316" id="Seg_4111" s="T315">thread-PL</ta>
            <ta e="T317" id="Seg_4112" s="T316">clew.[NOM.SG]</ta>
            <ta e="T318" id="Seg_4113" s="T317">go-EP-IMP.2SG</ta>
            <ta e="T319" id="Seg_4114" s="T318">this.[NOM.SG]</ta>
            <ta e="T320" id="Seg_4115" s="T319">run-FUT-3SG</ta>
            <ta e="T321" id="Seg_4116" s="T320">and</ta>
            <ta e="T322" id="Seg_4117" s="T321">you.NOM</ta>
            <ta e="T323" id="Seg_4118" s="T322">go-EP-IMP.2SG</ta>
            <ta e="T324" id="Seg_4119" s="T323">then</ta>
            <ta e="T325" id="Seg_4120" s="T324">this.[NOM.SG]</ta>
            <ta e="T326" id="Seg_4121" s="T325">go-PST.[3SG]</ta>
            <ta e="T328" id="Seg_4122" s="T327">then</ta>
            <ta e="T329" id="Seg_4123" s="T328">this.[NOM.SG]</ta>
            <ta e="T330" id="Seg_4124" s="T329">go-PST.[3SG]</ta>
            <ta e="T331" id="Seg_4125" s="T330">go-PST.[3SG]</ta>
            <ta e="T332" id="Seg_4126" s="T331">another.[NOM.SG]</ta>
            <ta e="T333" id="Seg_4127" s="T332">sister-LAT/LOC.3SG</ta>
            <ta e="T334" id="Seg_4128" s="T333">come-PST.[3SG]</ta>
            <ta e="T335" id="Seg_4129" s="T334">also</ta>
            <ta e="T336" id="Seg_4130" s="T335">house-NOM/GEN.3SG</ta>
            <ta e="T337" id="Seg_4131" s="T336">NEG</ta>
            <ta e="T340" id="Seg_4132" s="T339">small.[NOM.SG]</ta>
            <ta e="T341" id="Seg_4133" s="T340">house-NOM/GEN.3SG</ta>
            <ta e="T342" id="Seg_4134" s="T341">enter-PST.[3SG]</ta>
            <ta e="T343" id="Seg_4135" s="T342">house-LAT</ta>
            <ta e="T345" id="Seg_4136" s="T344">there</ta>
            <ta e="T346" id="Seg_4137" s="T345">there</ta>
            <ta e="T347" id="Seg_4138" s="T346">woman.[NOM.SG]</ta>
            <ta e="T348" id="Seg_4139" s="T347">live-DUR.[3SG]</ta>
            <ta e="T349" id="Seg_4140" s="T348">say-IPFVZ.[3SG]</ta>
            <ta e="T350" id="Seg_4141" s="T349">woman.[NOM.SG]</ta>
            <ta e="T351" id="Seg_4142" s="T350">woman.[NOM.SG]</ta>
            <ta e="T352" id="Seg_4143" s="T351">tell-IMP.2SG</ta>
            <ta e="T353" id="Seg_4144" s="T352">I.LAT</ta>
            <ta e="T354" id="Seg_4145" s="T353">where</ta>
            <ta e="T355" id="Seg_4146" s="T354">I.NOM</ta>
            <ta e="T356" id="Seg_4147" s="T355">man.[NOM.SG]</ta>
            <ta e="T455" id="Seg_4148" s="T356">go-CVB</ta>
            <ta e="T357" id="Seg_4149" s="T455">disappear-PST.[3SG]</ta>
            <ta e="T358" id="Seg_4150" s="T357">I.NOM</ta>
            <ta e="T359" id="Seg_4151" s="T358">NEG</ta>
            <ta e="T360" id="Seg_4152" s="T359">know-1SG</ta>
            <ta e="T361" id="Seg_4153" s="T360">go-EP-IMP.2SG</ta>
            <ta e="T362" id="Seg_4154" s="T361">I.NOM</ta>
            <ta e="T363" id="Seg_4155" s="T362">sister-LAT/LOC.3SG</ta>
            <ta e="T364" id="Seg_4156" s="T363">this.[NOM.SG]</ta>
            <ta e="T365" id="Seg_4157" s="T364">tell-FUT-3SG</ta>
            <ta e="T366" id="Seg_4158" s="T365">where</ta>
            <ta e="T367" id="Seg_4159" s="T366">this.[NOM.SG]</ta>
            <ta e="T368" id="Seg_4160" s="T367">this.[NOM.SG]</ta>
            <ta e="T369" id="Seg_4161" s="T368">this-LAT</ta>
            <ta e="T370" id="Seg_4162" s="T369">go-PST.[3SG]</ta>
            <ta e="T371" id="Seg_4163" s="T370">give-PST.[3SG]</ta>
            <ta e="T372" id="Seg_4164" s="T371">spinning_wheel.[NOM.SG]</ta>
            <ta e="T373" id="Seg_4165" s="T372">and</ta>
            <ta e="T374" id="Seg_4166" s="T373">give-PST.[3SG]</ta>
            <ta e="T375" id="Seg_4167" s="T374">sliver.[NOM.SG]</ta>
            <ta e="T376" id="Seg_4168" s="T375">spinning_wheel.[NOM.SG]</ta>
            <ta e="T377" id="Seg_4169" s="T376">PTCL</ta>
            <ta e="T378" id="Seg_4170" s="T377">golden.[NOM.SG]</ta>
            <ta e="T379" id="Seg_4171" s="T378">and</ta>
            <ta e="T380" id="Seg_4172" s="T379">sliver.[NOM.SG]</ta>
            <ta e="T381" id="Seg_4173" s="T380">golden.[NOM.SG]</ta>
            <ta e="T382" id="Seg_4174" s="T381">then</ta>
            <ta e="T384" id="Seg_4175" s="T383">give-PST.[3SG]</ta>
            <ta e="T385" id="Seg_4176" s="T384">clew.[NOM.SG]</ta>
            <ta e="T386" id="Seg_4177" s="T385">this.[NOM.SG]</ta>
            <ta e="T388" id="Seg_4178" s="T387">walk-FUT-3SG</ta>
            <ta e="T389" id="Seg_4179" s="T388">you.NOM</ta>
            <ta e="T391" id="Seg_4180" s="T390">go-EP-IMP.2SG</ta>
            <ta e="T392" id="Seg_4181" s="T391">there</ta>
            <ta e="T393" id="Seg_4182" s="T392">where.to</ta>
            <ta e="T394" id="Seg_4183" s="T393">this.[NOM.SG]</ta>
            <ta e="T395" id="Seg_4184" s="T394">walk-FUT-3SG</ta>
            <ta e="T396" id="Seg_4185" s="T395">then</ta>
            <ta e="T397" id="Seg_4186" s="T396">that.[NOM.SG]</ta>
            <ta e="T398" id="Seg_4187" s="T397">go-PST.[3SG]</ta>
            <ta e="T399" id="Seg_4188" s="T398">go-PST.[3SG]</ta>
            <ta e="T400" id="Seg_4189" s="T399">three.[NOM.SG]</ta>
            <ta e="T401" id="Seg_4190" s="T400">sister-LAT/LOC.3SG</ta>
            <ta e="T402" id="Seg_4191" s="T401">come-PST.[3SG]</ta>
            <ta e="T403" id="Seg_4192" s="T402">also</ta>
            <ta e="T404" id="Seg_4193" s="T403">house.[NOM.SG]</ta>
            <ta e="T405" id="Seg_4194" s="T404">stand-DUR.[3SG]</ta>
            <ta e="T406" id="Seg_4195" s="T405">this.[NOM.SG]</ta>
            <ta e="T407" id="Seg_4196" s="T406">come-PST.[3SG]</ta>
            <ta e="T408" id="Seg_4197" s="T407">there</ta>
            <ta e="T409" id="Seg_4198" s="T408">woman.[NOM.SG]</ta>
            <ta e="T410" id="Seg_4199" s="T409">live-DUR.[3SG]</ta>
            <ta e="T411" id="Seg_4200" s="T410">very</ta>
            <ta e="T412" id="Seg_4201" s="T411">then</ta>
            <ta e="T413" id="Seg_4202" s="T412">say-IPFVZ.[3SG]</ta>
            <ta e="T414" id="Seg_4203" s="T413">woman.[NOM.SG]</ta>
            <ta e="T415" id="Seg_4204" s="T414">woman.[NOM.SG]</ta>
            <ta e="T416" id="Seg_4205" s="T415">tell-IMP.2SG</ta>
            <ta e="T417" id="Seg_4206" s="T416">I.LAT</ta>
            <ta e="T418" id="Seg_4207" s="T417">where</ta>
            <ta e="T419" id="Seg_4208" s="T418">I.NOM</ta>
            <ta e="T420" id="Seg_4209" s="T419">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T456" id="Seg_4210" s="T420">go-CVB</ta>
            <ta e="T421" id="Seg_4211" s="T456">disappear-PST.[3SG]</ta>
            <ta e="T422" id="Seg_4212" s="T421">I.NOM</ta>
            <ta e="T423" id="Seg_4213" s="T422">know-1SG</ta>
            <ta e="T424" id="Seg_4214" s="T423">go-EP-IMP.2SG</ta>
            <ta e="T425" id="Seg_4215" s="T424">then</ta>
            <ta e="T426" id="Seg_4216" s="T425">this.[NOM.SG]</ta>
            <ta e="T427" id="Seg_4217" s="T426">give-PST.[3SG]</ta>
            <ta e="T428" id="Seg_4218" s="T427">this-LAT</ta>
            <ta e="T429" id="Seg_4219" s="T428">%spindle.[NOM.SG]</ta>
            <ta e="T430" id="Seg_4220" s="T429">beautiful.[NOM.SG]</ta>
            <ta e="T431" id="Seg_4221" s="T430">golden.[NOM.SG]</ta>
            <ta e="T432" id="Seg_4222" s="T431">PTCL</ta>
            <ta e="T434" id="Seg_4223" s="T433">ring.[NOM.SG]</ta>
            <ta e="T435" id="Seg_4224" s="T434">give-PST.[3SG]</ta>
            <ta e="T436" id="Seg_4225" s="T435">golden.[NOM.SG]</ta>
            <ta e="T437" id="Seg_4226" s="T436">hand-LAT/LOC.3SG</ta>
            <ta e="T438" id="Seg_4227" s="T437">go-EP-IMP.2SG</ta>
            <ta e="T439" id="Seg_4228" s="T438">where=INDEF</ta>
            <ta e="T440" id="Seg_4229" s="T439">live-DUR.[3SG]</ta>
            <ta e="T441" id="Seg_4230" s="T440">this-LAT</ta>
            <ta e="T442" id="Seg_4231" s="T441">give-PST.[3SG]</ta>
            <ta e="T443" id="Seg_4232" s="T442">also</ta>
            <ta e="T445" id="Seg_4233" s="T444">clew.[NOM.SG]</ta>
            <ta e="T446" id="Seg_4234" s="T445">where.to</ta>
            <ta e="T447" id="Seg_4235" s="T446">this.[NOM.SG]</ta>
            <ta e="T448" id="Seg_4236" s="T447">walk-FUT-3SG</ta>
            <ta e="T449" id="Seg_4237" s="T448">you.NOM</ta>
            <ta e="T450" id="Seg_4238" s="T449">there</ta>
            <ta e="T451" id="Seg_4239" s="T450">go-EP-IMP.2SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4240" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_4241" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_4242" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_4243" s="T3">этот-GEN</ta>
            <ta e="T5" id="Seg_4244" s="T4">три.[NOM.SG]</ta>
            <ta e="T6" id="Seg_4245" s="T5">девушка.[NOM.SG]</ta>
            <ta e="T7" id="Seg_4246" s="T6">быть-PST.[3SG]</ta>
            <ta e="T8" id="Seg_4247" s="T7">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_4248" s="T9">идти-PRS.[3SG]</ta>
            <ta e="T11" id="Seg_4249" s="T10">хлеб.[NOM.SG]</ta>
            <ta e="T12" id="Seg_4250" s="T11">продавать-INF.LAT</ta>
            <ta e="T13" id="Seg_4251" s="T12">что.[NOM.SG]</ta>
            <ta e="T14" id="Seg_4252" s="T13">вы.LAT</ta>
            <ta e="T15" id="Seg_4253" s="T14">взять-INF.LAT</ta>
            <ta e="T16" id="Seg_4254" s="T15">один.[NOM.SG]</ta>
            <ta e="T17" id="Seg_4255" s="T16">сказать-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_4256" s="T17">я.LAT</ta>
            <ta e="T19" id="Seg_4257" s="T18">взять-IMP.2SG</ta>
            <ta e="T20" id="Seg_4258" s="T19">красивый.[NOM.SG]</ta>
            <ta e="T21" id="Seg_4259" s="T20">пальто.[NOM.SG]</ta>
            <ta e="T22" id="Seg_4260" s="T21">один.[NOM.SG]</ta>
            <ta e="T23" id="Seg_4261" s="T22">сказать-IPFVZ.[3SG]</ta>
            <ta e="T24" id="Seg_4262" s="T23">я.LAT</ta>
            <ta e="T25" id="Seg_4263" s="T24">красивый.[NOM.SG]</ta>
            <ta e="T26" id="Seg_4264" s="T25">шаль.[NOM.SG]</ta>
            <ta e="T28" id="Seg_4265" s="T27">взять-IMP.2SG</ta>
            <ta e="T29" id="Seg_4266" s="T28">и</ta>
            <ta e="T30" id="Seg_4267" s="T29">красивый.[NOM.SG]</ta>
            <ta e="T31" id="Seg_4268" s="T30">платье.[NOM.SG]</ta>
            <ta e="T32" id="Seg_4269" s="T31">взять-IMP.2SG</ta>
            <ta e="T33" id="Seg_4270" s="T32">а</ta>
            <ta e="T34" id="Seg_4271" s="T33">маленький.[NOM.SG]</ta>
            <ta e="T36" id="Seg_4272" s="T35">а</ta>
            <ta e="T37" id="Seg_4273" s="T36">ты.DAT</ta>
            <ta e="T38" id="Seg_4274" s="T37">что.[NOM.SG]</ta>
            <ta e="T39" id="Seg_4275" s="T38">взять-INF.LAT</ta>
            <ta e="T40" id="Seg_4276" s="T39">я.LAT</ta>
            <ta e="T41" id="Seg_4277" s="T40">взять-IMP.2SG</ta>
            <ta e="T42" id="Seg_4278" s="T41">Финист.Ясный.сокол.[NOM.SG]</ta>
            <ta e="T43" id="Seg_4279" s="T42">перо.DIM.[NOM.SG]</ta>
            <ta e="T45" id="Seg_4280" s="T44">этот.[NOM.SG]</ta>
            <ta e="T46" id="Seg_4281" s="T45">сказать-IPFVZ.[3SG]</ta>
            <ta e="T47" id="Seg_4282" s="T46">взять-IMP.2SG</ta>
            <ta e="T48" id="Seg_4283" s="T47">я.LAT</ta>
            <ta e="T49" id="Seg_4284" s="T48">Финист.Ясный.сокол.[NOM.SG]</ta>
            <ta e="T50" id="Seg_4285" s="T49">перо.DIM.[NOM.SG]</ta>
            <ta e="T51" id="Seg_4286" s="T50">этот.[NOM.SG]</ta>
            <ta e="T52" id="Seg_4287" s="T51">пойти-PST.[3SG]</ta>
            <ta e="T53" id="Seg_4288" s="T52">этот-LAT</ta>
            <ta e="T54" id="Seg_4289" s="T53">взять-PST.[3SG]</ta>
            <ta e="T55" id="Seg_4290" s="T54">пальто.[NOM.SG]</ta>
            <ta e="T56" id="Seg_4291" s="T55">этот-LAT</ta>
            <ta e="T57" id="Seg_4292" s="T56">шаль.[NOM.SG]</ta>
            <ta e="T58" id="Seg_4293" s="T57">взять-PST.[3SG]</ta>
            <ta e="T59" id="Seg_4294" s="T58">и</ta>
            <ta e="T61" id="Seg_4295" s="T60">и</ta>
            <ta e="T62" id="Seg_4296" s="T61">платье.[NOM.SG]</ta>
            <ta e="T63" id="Seg_4297" s="T62">взять-PST.[3SG]</ta>
            <ta e="T64" id="Seg_4298" s="T63">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T65" id="Seg_4299" s="T64">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T66" id="Seg_4300" s="T65">NEG</ta>
            <ta e="T67" id="Seg_4301" s="T66">найти-PST.[3SG]</ta>
            <ta e="T68" id="Seg_4302" s="T67">тогда</ta>
            <ta e="T69" id="Seg_4303" s="T68">прийти-PST.[3SG]</ta>
            <ta e="T70" id="Seg_4304" s="T69">чум-LAT/LOC.3SG</ta>
            <ta e="T71" id="Seg_4305" s="T70">этот</ta>
            <ta e="T72" id="Seg_4306" s="T71">этот</ta>
            <ta e="T73" id="Seg_4307" s="T72">два.[NOM.SG]</ta>
            <ta e="T74" id="Seg_4308" s="T73">дочь.[NOM.SG]</ta>
            <ta e="T75" id="Seg_4309" s="T74">смеяться-DUR-3PL</ta>
            <ta e="T76" id="Seg_4310" s="T75">ты.DAT</ta>
            <ta e="T77" id="Seg_4311" s="T76">что.[NOM.SG]=INDEF</ta>
            <ta e="T78" id="Seg_4312" s="T77">NEG</ta>
            <ta e="T79" id="Seg_4313" s="T78">взять-PST.[3SG]</ta>
            <ta e="T80" id="Seg_4314" s="T79">а</ta>
            <ta e="T81" id="Seg_4315" s="T80">мы.LAT</ta>
            <ta e="T82" id="Seg_4316" s="T81">взять-PST.[3SG]</ta>
            <ta e="T83" id="Seg_4317" s="T82">ну</ta>
            <ta e="T84" id="Seg_4318" s="T83">и</ta>
            <ta e="T85" id="Seg_4319" s="T84">что</ta>
            <ta e="T86" id="Seg_4320" s="T85">NEG</ta>
            <ta e="T87" id="Seg_4321" s="T86">взять-PST.[3SG]</ta>
            <ta e="T88" id="Seg_4322" s="T87">так</ta>
            <ta e="T89" id="Seg_4323" s="T88">JUSS</ta>
            <ta e="T90" id="Seg_4324" s="T89">NEG</ta>
            <ta e="T91" id="Seg_4325" s="T90">взять-FUT-3SG</ta>
            <ta e="T92" id="Seg_4326" s="T91">тогда</ta>
            <ta e="T93" id="Seg_4327" s="T92">этот.[NOM.SG]</ta>
            <ta e="T94" id="Seg_4328" s="T93">опять</ta>
            <ta e="T95" id="Seg_4329" s="T94">пойти-PST.[3SG]</ta>
            <ta e="T96" id="Seg_4330" s="T95">хлеб-INS</ta>
            <ta e="T97" id="Seg_4331" s="T96">что.[NOM.SG]</ta>
            <ta e="T98" id="Seg_4332" s="T97">взять-INF.LAT</ta>
            <ta e="T99" id="Seg_4333" s="T98">тогда</ta>
            <ta e="T100" id="Seg_4334" s="T99">этот-PL</ta>
            <ta e="T101" id="Seg_4335" s="T100">сказать-IPFVZ-3PL</ta>
            <ta e="T102" id="Seg_4336" s="T101">ботинок-PL</ta>
            <ta e="T103" id="Seg_4337" s="T102">взять-IMP.2SG</ta>
            <ta e="T104" id="Seg_4338" s="T103">красивый.[NOM.SG]</ta>
            <ta e="T105" id="Seg_4339" s="T104">этот.[NOM.SG]</ta>
            <ta e="T106" id="Seg_4340" s="T105">пойти-PST.[3SG]</ta>
            <ta e="T107" id="Seg_4341" s="T106">этот-PL-LAT</ta>
            <ta e="T108" id="Seg_4342" s="T107">взять-PST.[3SG]</ta>
            <ta e="T109" id="Seg_4343" s="T108">а</ta>
            <ta e="T110" id="Seg_4344" s="T109">а</ta>
            <ta e="T111" id="Seg_4345" s="T110">маленький.[NOM.SG]</ta>
            <ta e="T112" id="Seg_4346" s="T111">сказать-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_4347" s="T112">я.LAT</ta>
            <ta e="T114" id="Seg_4348" s="T113">взять-IMP.2SG</ta>
            <ta e="T115" id="Seg_4349" s="T114">Финист.Ясный.сокол.[NOM.SG]</ta>
            <ta e="T116" id="Seg_4350" s="T115">перо.DIM.[NOM.SG]</ta>
            <ta e="T117" id="Seg_4351" s="T116">этот.[NOM.SG]</ta>
            <ta e="T118" id="Seg_4352" s="T117">пойти-PST.[3SG]</ta>
            <ta e="T119" id="Seg_4353" s="T118">этот-PL-LAT</ta>
            <ta e="T120" id="Seg_4354" s="T119">взять-PST.[3SG]</ta>
            <ta e="T121" id="Seg_4355" s="T120">а</ta>
            <ta e="T122" id="Seg_4356" s="T121">этот-ACC</ta>
            <ta e="T123" id="Seg_4357" s="T122">NEG</ta>
            <ta e="T124" id="Seg_4358" s="T123">найти-PST.[3SG]</ta>
            <ta e="T125" id="Seg_4359" s="T124">тогда</ta>
            <ta e="T126" id="Seg_4360" s="T125">три-LAT.ADV</ta>
            <ta e="T127" id="Seg_4361" s="T126">пойти-PST.[3SG]</ta>
            <ta e="T128" id="Seg_4362" s="T127">тогда</ta>
            <ta e="T129" id="Seg_4363" s="T128">этот-PL-LAT</ta>
            <ta e="T130" id="Seg_4364" s="T129">взять-PST.[3SG]</ta>
            <ta e="T131" id="Seg_4365" s="T130">весь</ta>
            <ta e="T132" id="Seg_4366" s="T131">что.[NOM.SG]</ta>
            <ta e="T133" id="Seg_4367" s="T132">а</ta>
            <ta e="T134" id="Seg_4368" s="T133">этот-LAT</ta>
            <ta e="T135" id="Seg_4369" s="T134">найти-PST.[3SG]</ta>
            <ta e="T136" id="Seg_4370" s="T135">этот.[NOM.SG]</ta>
            <ta e="T137" id="Seg_4371" s="T136">перышко.[NOM.SG]</ta>
            <ta e="T138" id="Seg_4372" s="T137">и</ta>
            <ta e="T139" id="Seg_4373" s="T138">принести-PST.[3SG]</ta>
            <ta e="T140" id="Seg_4374" s="T139">тогда</ta>
            <ta e="T141" id="Seg_4375" s="T140">этот.[NOM.SG]</ta>
            <ta e="T142" id="Seg_4376" s="T141">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T143" id="Seg_4377" s="T142">%%</ta>
            <ta e="T144" id="Seg_4378" s="T143">спать-PST.[3SG]</ta>
            <ta e="T145" id="Seg_4379" s="T144">взять-PST.[3SG]</ta>
            <ta e="T146" id="Seg_4380" s="T145">этот.[NOM.SG]</ta>
            <ta e="T147" id="Seg_4381" s="T146">перышко.[NOM.SG]</ta>
            <ta e="T148" id="Seg_4382" s="T147">%%-PST.[3SG]</ta>
            <ta e="T149" id="Seg_4383" s="T148">рука-LAT/LOC.3SG</ta>
            <ta e="T150" id="Seg_4384" s="T149">рука-LAT/LOC.3SG</ta>
            <ta e="T151" id="Seg_4385" s="T150">бросить-MOM-PST.[3SG]</ta>
            <ta e="T152" id="Seg_4386" s="T151">этот.[NOM.SG]</ta>
            <ta e="T153" id="Seg_4387" s="T152">PTCL</ta>
            <ta e="T154" id="Seg_4388" s="T153">мальчик.[NOM.SG]</ta>
            <ta e="T155" id="Seg_4389" s="T154">стать-MOM-PST.[3SG]</ta>
            <ta e="T156" id="Seg_4390" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_4391" s="T156">говорить-DUR-3PL</ta>
            <ta e="T158" id="Seg_4392" s="T157">а</ta>
            <ta e="T159" id="Seg_4393" s="T158">этот-GEN</ta>
            <ta e="T160" id="Seg_4394" s="T159">сестра-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T161" id="Seg_4395" s="T160">смотреть-PST-3PL</ta>
            <ta e="T162" id="Seg_4396" s="T161">что</ta>
            <ta e="T163" id="Seg_4397" s="T162">там</ta>
            <ta e="T164" id="Seg_4398" s="T163">мальчик.[NOM.SG]</ta>
            <ta e="T165" id="Seg_4399" s="T164">быть-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_4400" s="T165">тогда</ta>
            <ta e="T167" id="Seg_4401" s="T166">этот.[NOM.SG]</ta>
            <ta e="T169" id="Seg_4402" s="T168">там</ta>
            <ta e="T170" id="Seg_4403" s="T169">окно-NOM/GEN/ACC.3SG</ta>
            <ta e="T171" id="Seg_4404" s="T170">быть-PST.[3SG]</ta>
            <ta e="T172" id="Seg_4405" s="T171">маленький.[NOM.SG]</ta>
            <ta e="T173" id="Seg_4406" s="T172">там</ta>
            <ta e="T174" id="Seg_4407" s="T173">PTCL</ta>
            <ta e="T175" id="Seg_4408" s="T174">там</ta>
            <ta e="T176" id="Seg_4409" s="T175">нож-PL</ta>
            <ta e="T178" id="Seg_4410" s="T177">вставить-TR-PST-3PL</ta>
            <ta e="T179" id="Seg_4411" s="T178">а</ta>
            <ta e="T180" id="Seg_4412" s="T179">этот-ACC</ta>
            <ta e="T182" id="Seg_4413" s="T181">пить-TR-PST-3PL</ta>
            <ta e="T183" id="Seg_4414" s="T182">такой.[NOM.SG]</ta>
            <ta e="T184" id="Seg_4415" s="T183">вода.[NOM.SG]</ta>
            <ta e="T185" id="Seg_4416" s="T184">что</ta>
            <ta e="T186" id="Seg_4417" s="T185">спать-PST.[3SG]</ta>
            <ta e="T187" id="Seg_4418" s="T186">этот.[NOM.SG]</ta>
            <ta e="T188" id="Seg_4419" s="T187">прийти-PST.[3SG]</ta>
            <ta e="T189" id="Seg_4420" s="T188">кричать-PST.[3SG]</ta>
            <ta e="T190" id="Seg_4421" s="T189">кричать-PST.[3SG]</ta>
            <ta e="T191" id="Seg_4422" s="T190">нога-ABL.3SG</ta>
            <ta e="T192" id="Seg_4423" s="T191">резать-PST.[3SG]</ta>
            <ta e="T193" id="Seg_4424" s="T192">и</ta>
            <ta e="T453" id="Seg_4425" s="T193">пойти-CVB</ta>
            <ta e="T194" id="Seg_4426" s="T453">исчезнуть-PST.[3SG]</ta>
            <ta e="T196" id="Seg_4427" s="T195">этот-GEN</ta>
            <ta e="T197" id="Seg_4428" s="T196">сестра-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T198" id="Seg_4429" s="T197">этот-LAT</ta>
            <ta e="T199" id="Seg_4430" s="T198">дать-PST-3PL</ta>
            <ta e="T200" id="Seg_4431" s="T199">капля-PL</ta>
            <ta e="T201" id="Seg_4432" s="T200">чтобы</ta>
            <ta e="T202" id="Seg_4433" s="T201">этот.[NOM.SG]</ta>
            <ta e="T203" id="Seg_4434" s="T202">спать-MOM-PST.[3SG]</ta>
            <ta e="T204" id="Seg_4435" s="T203">этот.[NOM.SG]</ta>
            <ta e="T205" id="Seg_4436" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_4437" s="T205">спать-MOM-PST.[3SG]</ta>
            <ta e="T207" id="Seg_4438" s="T206">а</ta>
            <ta e="T208" id="Seg_4439" s="T207">этот.[NOM.SG]</ta>
            <ta e="T209" id="Seg_4440" s="T208">прийти-PST.[3SG]</ta>
            <ta e="T210" id="Seg_4441" s="T209">окно-NOM/GEN/ACC.3SG</ta>
            <ta e="T211" id="Seg_4442" s="T210">этот-PL</ta>
            <ta e="T212" id="Seg_4443" s="T211">там</ta>
            <ta e="T213" id="Seg_4444" s="T212">вставить-PST-3PL</ta>
            <ta e="T214" id="Seg_4445" s="T213">нож-PL</ta>
            <ta e="T215" id="Seg_4446" s="T214">этот.[NOM.SG]</ta>
            <ta e="T216" id="Seg_4447" s="T215">PTCL</ta>
            <ta e="T217" id="Seg_4448" s="T216">войти-INF.LAT</ta>
            <ta e="T218" id="Seg_4449" s="T217">хотеть.PST.M.SG</ta>
            <ta e="T219" id="Seg_4450" s="T218">нога-ACC.3SG</ta>
            <ta e="T220" id="Seg_4451" s="T219">PTCL</ta>
            <ta e="T221" id="Seg_4452" s="T220">резать-MOM-PST.[3SG]</ta>
            <ta e="T222" id="Seg_4453" s="T221">кровь.[NOM.SG]</ta>
            <ta e="T223" id="Seg_4454" s="T222">течь-MOM-PST.[3SG]</ta>
            <ta e="T224" id="Seg_4455" s="T223">тогда</ta>
            <ta e="T225" id="Seg_4456" s="T224">бумага.[NOM.SG]</ta>
            <ta e="T226" id="Seg_4457" s="T225">писать-PST.[3SG]</ta>
            <ta e="T227" id="Seg_4458" s="T226">и</ta>
            <ta e="T229" id="Seg_4459" s="T228">класть-PST.[3SG]</ta>
            <ta e="T230" id="Seg_4460" s="T229">когда</ta>
            <ta e="T231" id="Seg_4461" s="T230">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T232" id="Seg_4462" s="T231">сапог-PL</ta>
            <ta e="T233" id="Seg_4463" s="T232">делать-FUT-2SG</ta>
            <ta e="T234" id="Seg_4464" s="T233">сносить-FUT-2SG</ta>
            <ta e="T235" id="Seg_4465" s="T234">тогда</ta>
            <ta e="T236" id="Seg_4466" s="T235">я.ACC</ta>
            <ta e="T237" id="Seg_4467" s="T236">найти-FUT-2SG</ta>
            <ta e="T239" id="Seg_4468" s="T238">тогда</ta>
            <ta e="T240" id="Seg_4469" s="T239">этот.[NOM.SG]</ta>
            <ta e="T241" id="Seg_4470" s="T240">встать-PST.[3SG]</ta>
            <ta e="T242" id="Seg_4471" s="T241">видеть-PRS-3SG.O</ta>
            <ta e="T243" id="Seg_4472" s="T242">PTCL</ta>
            <ta e="T244" id="Seg_4473" s="T243">кровь.[NOM.SG]</ta>
            <ta e="T245" id="Seg_4474" s="T244">здесь</ta>
            <ta e="T246" id="Seg_4475" s="T245">и</ta>
            <ta e="T247" id="Seg_4476" s="T246">бумага.[NOM.SG]</ta>
            <ta e="T248" id="Seg_4477" s="T247">лежать-DUR.[3SG]</ta>
            <ta e="T249" id="Seg_4478" s="T248">этот.[NOM.SG]</ta>
            <ta e="T250" id="Seg_4479" s="T249">бумага.[NOM.SG]</ta>
            <ta e="T251" id="Seg_4480" s="T250">смотреть-PST.[3SG]</ta>
            <ta e="T252" id="Seg_4481" s="T251">PTCL</ta>
            <ta e="T253" id="Seg_4482" s="T252">плакать-PST.[3SG]</ta>
            <ta e="T254" id="Seg_4483" s="T253">плакать-PST.[3SG]</ta>
            <ta e="T255" id="Seg_4484" s="T254">и</ta>
            <ta e="T256" id="Seg_4485" s="T255">пойти-PST.[3SG]</ta>
            <ta e="T257" id="Seg_4486" s="T256">кузница-LAT</ta>
            <ta e="T258" id="Seg_4487" s="T257">сапог-PL</ta>
            <ta e="T259" id="Seg_4488" s="T258">делать-PST.[3SG]</ta>
            <ta e="T260" id="Seg_4489" s="T259">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T261" id="Seg_4490" s="T260">дерево.[NOM.SG]</ta>
            <ta e="T262" id="Seg_4491" s="T261">делать-PST.[3SG]</ta>
            <ta e="T263" id="Seg_4492" s="T262">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T264" id="Seg_4493" s="T263">тогда</ta>
            <ta e="T265" id="Seg_4494" s="T264">пойти-PST.[3SG]</ta>
            <ta e="T266" id="Seg_4495" s="T265">пойти-PST.[3SG]</ta>
            <ta e="T267" id="Seg_4496" s="T266">пойти-PST.[3SG]</ta>
            <ta e="T268" id="Seg_4497" s="T267">долго-%%</ta>
            <ta e="T269" id="Seg_4498" s="T268">прийти-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_4499" s="T269">прийти-PST.[3SG]</ta>
            <ta e="T271" id="Seg_4500" s="T270">тогда</ta>
            <ta e="T272" id="Seg_4501" s="T271">видеть-PRS-3SG.O</ta>
            <ta e="T273" id="Seg_4502" s="T272">видеть-DUR.[3SG]</ta>
            <ta e="T274" id="Seg_4503" s="T273">дом.[NOM.SG]</ta>
            <ta e="T276" id="Seg_4504" s="T275">стоять-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_4505" s="T276">немного</ta>
            <ta e="T278" id="Seg_4506" s="T277">этот.[NOM.SG]</ta>
            <ta e="T279" id="Seg_4507" s="T278">дом-LAT</ta>
            <ta e="T280" id="Seg_4508" s="T279">прийти-PST.[3SG]</ta>
            <ta e="T281" id="Seg_4509" s="T280">там</ta>
            <ta e="T282" id="Seg_4510" s="T281">женщина.[NOM.SG]</ta>
            <ta e="T283" id="Seg_4511" s="T282">жить-DUR.[3SG]</ta>
            <ta e="T284" id="Seg_4512" s="T283">этот.[NOM.SG]</ta>
            <ta e="T285" id="Seg_4513" s="T284">спросить-DUR.[3SG]</ta>
            <ta e="T286" id="Seg_4514" s="T285">женщина.[NOM.SG]</ta>
            <ta e="T287" id="Seg_4515" s="T286">женщина.[NOM.SG]</ta>
            <ta e="T288" id="Seg_4516" s="T287">сказать-IMP.2SG.O</ta>
            <ta e="T289" id="Seg_4517" s="T288">где</ta>
            <ta e="T290" id="Seg_4518" s="T289">я.NOM</ta>
            <ta e="T291" id="Seg_4519" s="T290">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T454" id="Seg_4520" s="T291">пойти-CVB</ta>
            <ta e="T292" id="Seg_4521" s="T454">исчезнуть-PST.[3SG]</ta>
            <ta e="T293" id="Seg_4522" s="T292">этот.[NOM.SG]</ta>
            <ta e="T294" id="Seg_4523" s="T293">сказать-IPFVZ.[3SG]</ta>
            <ta e="T295" id="Seg_4524" s="T294">я.NOM</ta>
            <ta e="T296" id="Seg_4525" s="T295">NEG</ta>
            <ta e="T297" id="Seg_4526" s="T296">знать-1SG</ta>
            <ta e="T298" id="Seg_4527" s="T297">где</ta>
            <ta e="T300" id="Seg_4528" s="T299">где=INDEF</ta>
            <ta e="T301" id="Seg_4529" s="T300">пойти-EP-IMP.2SG</ta>
            <ta e="T302" id="Seg_4530" s="T301">я.NOM</ta>
            <ta e="T303" id="Seg_4531" s="T302">сестра-LAT/LOC.3SG</ta>
            <ta e="T304" id="Seg_4532" s="T303">этот.[NOM.SG]</ta>
            <ta e="T305" id="Seg_4533" s="T304">знать-3SG.O</ta>
            <ta e="T306" id="Seg_4534" s="T305">сказать-FUT-3SG</ta>
            <ta e="T307" id="Seg_4535" s="T306">ты.DAT</ta>
            <ta e="T308" id="Seg_4536" s="T307">тогда</ta>
            <ta e="T309" id="Seg_4537" s="T308">этот-LAT</ta>
            <ta e="T310" id="Seg_4538" s="T309">дать-PST.[3SG]</ta>
            <ta e="T311" id="Seg_4539" s="T310">чашка.[NOM.SG]</ta>
            <ta e="T312" id="Seg_4540" s="T311">золотой.[NOM.SG]</ta>
            <ta e="T313" id="Seg_4541" s="T312">яйцо-PL</ta>
            <ta e="T314" id="Seg_4542" s="T313">золотой.[NOM.SG]</ta>
            <ta e="T315" id="Seg_4543" s="T314">и</ta>
            <ta e="T316" id="Seg_4544" s="T315">нитка-PL</ta>
            <ta e="T317" id="Seg_4545" s="T316">клубок.[NOM.SG]</ta>
            <ta e="T318" id="Seg_4546" s="T317">пойти-EP-IMP.2SG</ta>
            <ta e="T319" id="Seg_4547" s="T318">этот.[NOM.SG]</ta>
            <ta e="T320" id="Seg_4548" s="T319">бежать-FUT-3SG</ta>
            <ta e="T321" id="Seg_4549" s="T320">и</ta>
            <ta e="T322" id="Seg_4550" s="T321">ты.NOM</ta>
            <ta e="T323" id="Seg_4551" s="T322">пойти-EP-IMP.2SG</ta>
            <ta e="T324" id="Seg_4552" s="T323">тогда</ta>
            <ta e="T325" id="Seg_4553" s="T324">этот.[NOM.SG]</ta>
            <ta e="T326" id="Seg_4554" s="T325">пойти-PST.[3SG]</ta>
            <ta e="T328" id="Seg_4555" s="T327">тогда</ta>
            <ta e="T329" id="Seg_4556" s="T328">этот.[NOM.SG]</ta>
            <ta e="T330" id="Seg_4557" s="T329">пойти-PST.[3SG]</ta>
            <ta e="T331" id="Seg_4558" s="T330">пойти-PST.[3SG]</ta>
            <ta e="T332" id="Seg_4559" s="T331">другой.[NOM.SG]</ta>
            <ta e="T333" id="Seg_4560" s="T332">сестра-LAT/LOC.3SG</ta>
            <ta e="T334" id="Seg_4561" s="T333">прийти-PST.[3SG]</ta>
            <ta e="T335" id="Seg_4562" s="T334">тоже</ta>
            <ta e="T336" id="Seg_4563" s="T335">дом-NOM/GEN.3SG</ta>
            <ta e="T337" id="Seg_4564" s="T336">NEG</ta>
            <ta e="T340" id="Seg_4565" s="T339">маленький.[NOM.SG]</ta>
            <ta e="T341" id="Seg_4566" s="T340">дом-NOM/GEN.3SG</ta>
            <ta e="T342" id="Seg_4567" s="T341">войти-PST.[3SG]</ta>
            <ta e="T343" id="Seg_4568" s="T342">дом-LAT</ta>
            <ta e="T345" id="Seg_4569" s="T344">там</ta>
            <ta e="T346" id="Seg_4570" s="T345">там</ta>
            <ta e="T347" id="Seg_4571" s="T346">женщина.[NOM.SG]</ta>
            <ta e="T348" id="Seg_4572" s="T347">жить-DUR.[3SG]</ta>
            <ta e="T349" id="Seg_4573" s="T348">сказать-IPFVZ.[3SG]</ta>
            <ta e="T350" id="Seg_4574" s="T349">женщина.[NOM.SG]</ta>
            <ta e="T351" id="Seg_4575" s="T350">женщина.[NOM.SG]</ta>
            <ta e="T352" id="Seg_4576" s="T351">сказать-IMP.2SG</ta>
            <ta e="T353" id="Seg_4577" s="T352">я.LAT</ta>
            <ta e="T354" id="Seg_4578" s="T353">где</ta>
            <ta e="T355" id="Seg_4579" s="T354">я.NOM</ta>
            <ta e="T356" id="Seg_4580" s="T355">мужчина.[NOM.SG]</ta>
            <ta e="T455" id="Seg_4581" s="T356">пойти-CVB</ta>
            <ta e="T357" id="Seg_4582" s="T455">исчезнуть-PST.[3SG]</ta>
            <ta e="T358" id="Seg_4583" s="T357">я.NOM</ta>
            <ta e="T359" id="Seg_4584" s="T358">NEG</ta>
            <ta e="T360" id="Seg_4585" s="T359">знать-1SG</ta>
            <ta e="T361" id="Seg_4586" s="T360">пойти-EP-IMP.2SG</ta>
            <ta e="T362" id="Seg_4587" s="T361">я.NOM</ta>
            <ta e="T363" id="Seg_4588" s="T362">сестра-LAT/LOC.3SG</ta>
            <ta e="T364" id="Seg_4589" s="T363">этот.[NOM.SG]</ta>
            <ta e="T365" id="Seg_4590" s="T364">сказать-FUT-3SG</ta>
            <ta e="T366" id="Seg_4591" s="T365">где</ta>
            <ta e="T367" id="Seg_4592" s="T366">этот.[NOM.SG]</ta>
            <ta e="T368" id="Seg_4593" s="T367">этот.[NOM.SG]</ta>
            <ta e="T369" id="Seg_4594" s="T368">этот-LAT</ta>
            <ta e="T370" id="Seg_4595" s="T369">идти-PST.[3SG]</ta>
            <ta e="T371" id="Seg_4596" s="T370">дать-PST.[3SG]</ta>
            <ta e="T372" id="Seg_4597" s="T371">прялка.[NOM.SG]</ta>
            <ta e="T373" id="Seg_4598" s="T372">и</ta>
            <ta e="T374" id="Seg_4599" s="T373">дать-PST.[3SG]</ta>
            <ta e="T375" id="Seg_4600" s="T374">кудель.[NOM.SG]</ta>
            <ta e="T376" id="Seg_4601" s="T375">прялка.[NOM.SG]</ta>
            <ta e="T377" id="Seg_4602" s="T376">PTCL</ta>
            <ta e="T378" id="Seg_4603" s="T377">золотой.[NOM.SG]</ta>
            <ta e="T379" id="Seg_4604" s="T378">и</ta>
            <ta e="T380" id="Seg_4605" s="T379">кудель.[NOM.SG]</ta>
            <ta e="T381" id="Seg_4606" s="T380">золотой.[NOM.SG]</ta>
            <ta e="T382" id="Seg_4607" s="T381">тогда</ta>
            <ta e="T384" id="Seg_4608" s="T383">дать-PST.[3SG]</ta>
            <ta e="T385" id="Seg_4609" s="T384">клубок.[NOM.SG]</ta>
            <ta e="T386" id="Seg_4610" s="T385">этот.[NOM.SG]</ta>
            <ta e="T388" id="Seg_4611" s="T387">идти-FUT-3SG</ta>
            <ta e="T389" id="Seg_4612" s="T388">ты.NOM</ta>
            <ta e="T391" id="Seg_4613" s="T390">пойти-EP-IMP.2SG</ta>
            <ta e="T392" id="Seg_4614" s="T391">там</ta>
            <ta e="T393" id="Seg_4615" s="T392">куда</ta>
            <ta e="T394" id="Seg_4616" s="T393">этот.[NOM.SG]</ta>
            <ta e="T395" id="Seg_4617" s="T394">идти-FUT-3SG</ta>
            <ta e="T396" id="Seg_4618" s="T395">тогда</ta>
            <ta e="T397" id="Seg_4619" s="T396">тот.[NOM.SG]</ta>
            <ta e="T398" id="Seg_4620" s="T397">пойти-PST.[3SG]</ta>
            <ta e="T399" id="Seg_4621" s="T398">пойти-PST.[3SG]</ta>
            <ta e="T400" id="Seg_4622" s="T399">три.[NOM.SG]</ta>
            <ta e="T401" id="Seg_4623" s="T400">сестра-LAT/LOC.3SG</ta>
            <ta e="T402" id="Seg_4624" s="T401">прийти-PST.[3SG]</ta>
            <ta e="T403" id="Seg_4625" s="T402">тоже</ta>
            <ta e="T404" id="Seg_4626" s="T403">дом.[NOM.SG]</ta>
            <ta e="T405" id="Seg_4627" s="T404">стоять-DUR.[3SG]</ta>
            <ta e="T406" id="Seg_4628" s="T405">этот.[NOM.SG]</ta>
            <ta e="T407" id="Seg_4629" s="T406">прийти-PST.[3SG]</ta>
            <ta e="T408" id="Seg_4630" s="T407">там</ta>
            <ta e="T409" id="Seg_4631" s="T408">женщина.[NOM.SG]</ta>
            <ta e="T410" id="Seg_4632" s="T409">жить-DUR.[3SG]</ta>
            <ta e="T411" id="Seg_4633" s="T410">очень</ta>
            <ta e="T412" id="Seg_4634" s="T411">тогда</ta>
            <ta e="T413" id="Seg_4635" s="T412">сказать-IPFVZ.[3SG]</ta>
            <ta e="T414" id="Seg_4636" s="T413">женщина.[NOM.SG]</ta>
            <ta e="T415" id="Seg_4637" s="T414">женщина.[NOM.SG]</ta>
            <ta e="T416" id="Seg_4638" s="T415">сказать-IMP.2SG</ta>
            <ta e="T417" id="Seg_4639" s="T416">я.LAT</ta>
            <ta e="T418" id="Seg_4640" s="T417">где</ta>
            <ta e="T419" id="Seg_4641" s="T418">я.NOM</ta>
            <ta e="T420" id="Seg_4642" s="T419">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T456" id="Seg_4643" s="T420">пойти-CVB</ta>
            <ta e="T421" id="Seg_4644" s="T456">исчезнуть-PST.[3SG]</ta>
            <ta e="T422" id="Seg_4645" s="T421">я.NOM</ta>
            <ta e="T423" id="Seg_4646" s="T422">знать-1SG</ta>
            <ta e="T424" id="Seg_4647" s="T423">пойти-EP-IMP.2SG</ta>
            <ta e="T425" id="Seg_4648" s="T424">тогда</ta>
            <ta e="T426" id="Seg_4649" s="T425">этот.[NOM.SG]</ta>
            <ta e="T427" id="Seg_4650" s="T426">дать-PST.[3SG]</ta>
            <ta e="T428" id="Seg_4651" s="T427">этот-LAT</ta>
            <ta e="T429" id="Seg_4652" s="T428">%веретено.[NOM.SG]</ta>
            <ta e="T430" id="Seg_4653" s="T429">красивый.[NOM.SG]</ta>
            <ta e="T431" id="Seg_4654" s="T430">золотой.[NOM.SG]</ta>
            <ta e="T432" id="Seg_4655" s="T431">PTCL</ta>
            <ta e="T434" id="Seg_4656" s="T433">колечко.[NOM.SG]</ta>
            <ta e="T435" id="Seg_4657" s="T434">дать-PST.[3SG]</ta>
            <ta e="T436" id="Seg_4658" s="T435">золотой.[NOM.SG]</ta>
            <ta e="T437" id="Seg_4659" s="T436">рука-LAT/LOC.3SG</ta>
            <ta e="T438" id="Seg_4660" s="T437">пойти-EP-IMP.2SG</ta>
            <ta e="T439" id="Seg_4661" s="T438">где=INDEF</ta>
            <ta e="T440" id="Seg_4662" s="T439">жить-DUR.[3SG]</ta>
            <ta e="T441" id="Seg_4663" s="T440">этот-LAT</ta>
            <ta e="T442" id="Seg_4664" s="T441">дать-PST.[3SG]</ta>
            <ta e="T443" id="Seg_4665" s="T442">тоже</ta>
            <ta e="T445" id="Seg_4666" s="T444">клубок.[NOM.SG]</ta>
            <ta e="T446" id="Seg_4667" s="T445">куда</ta>
            <ta e="T447" id="Seg_4668" s="T446">этот.[NOM.SG]</ta>
            <ta e="T448" id="Seg_4669" s="T447">идти-FUT-3SG</ta>
            <ta e="T449" id="Seg_4670" s="T448">ты.NOM</ta>
            <ta e="T450" id="Seg_4671" s="T449">там</ta>
            <ta e="T451" id="Seg_4672" s="T450">пойти-EP-IMP.2SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4673" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_4674" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_4675" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_4676" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_4677" s="T4">num-n:case</ta>
            <ta e="T6" id="Seg_4678" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_4679" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_4680" s="T7">dempro-n:case</ta>
            <ta e="T10" id="Seg_4681" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_4682" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_4683" s="T11">v-v:n.fin</ta>
            <ta e="T13" id="Seg_4684" s="T12">que-n:case</ta>
            <ta e="T14" id="Seg_4685" s="T13">pers</ta>
            <ta e="T15" id="Seg_4686" s="T14">v-v:n.fin</ta>
            <ta e="T16" id="Seg_4687" s="T15">num-n:case</ta>
            <ta e="T17" id="Seg_4688" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_4689" s="T17">pers</ta>
            <ta e="T19" id="Seg_4690" s="T18">v-v:mood.pn</ta>
            <ta e="T20" id="Seg_4691" s="T19">adj-n:case</ta>
            <ta e="T21" id="Seg_4692" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_4693" s="T21">num-n:case</ta>
            <ta e="T23" id="Seg_4694" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_4695" s="T23">pers</ta>
            <ta e="T25" id="Seg_4696" s="T24">adj-n:case</ta>
            <ta e="T26" id="Seg_4697" s="T25">n-n:case</ta>
            <ta e="T28" id="Seg_4698" s="T27">v-v:mood.pn</ta>
            <ta e="T29" id="Seg_4699" s="T28">conj</ta>
            <ta e="T30" id="Seg_4700" s="T29">adj-n:case</ta>
            <ta e="T31" id="Seg_4701" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_4702" s="T31">v-v:mood.pn</ta>
            <ta e="T33" id="Seg_4703" s="T32">conj</ta>
            <ta e="T34" id="Seg_4704" s="T33">adj-n:case</ta>
            <ta e="T36" id="Seg_4705" s="T35">conj</ta>
            <ta e="T37" id="Seg_4706" s="T36">pers</ta>
            <ta e="T38" id="Seg_4707" s="T37">que-n:case</ta>
            <ta e="T39" id="Seg_4708" s="T38">v-v:n.fin</ta>
            <ta e="T40" id="Seg_4709" s="T39">pers</ta>
            <ta e="T41" id="Seg_4710" s="T40">v-v:mood.pn</ta>
            <ta e="T42" id="Seg_4711" s="T41">propr-n:case</ta>
            <ta e="T43" id="Seg_4712" s="T42">n-n:case</ta>
            <ta e="T45" id="Seg_4713" s="T44">dempro-n:case</ta>
            <ta e="T46" id="Seg_4714" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_4715" s="T46">v-v:mood.pn</ta>
            <ta e="T48" id="Seg_4716" s="T47">pers</ta>
            <ta e="T49" id="Seg_4717" s="T48">propr-n:case</ta>
            <ta e="T50" id="Seg_4718" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_4719" s="T50">dempro-n:case</ta>
            <ta e="T52" id="Seg_4720" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_4721" s="T52">dempro-n:case</ta>
            <ta e="T54" id="Seg_4722" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_4723" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_4724" s="T55">dempro-n:case</ta>
            <ta e="T57" id="Seg_4725" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_4726" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_4727" s="T58">conj</ta>
            <ta e="T61" id="Seg_4728" s="T60">conj</ta>
            <ta e="T62" id="Seg_4729" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_4730" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_4731" s="T63">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_4732" s="T64">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_4733" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4734" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_4735" s="T67">adv</ta>
            <ta e="T69" id="Seg_4736" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_4737" s="T69">n-n:case.poss</ta>
            <ta e="T71" id="Seg_4738" s="T70">dempro</ta>
            <ta e="T72" id="Seg_4739" s="T71">dempro</ta>
            <ta e="T73" id="Seg_4740" s="T72">num-n:case</ta>
            <ta e="T74" id="Seg_4741" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_4742" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_4743" s="T75">pers</ta>
            <ta e="T77" id="Seg_4744" s="T76">que-n:case=ptcl</ta>
            <ta e="T78" id="Seg_4745" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_4746" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_4747" s="T79">conj</ta>
            <ta e="T81" id="Seg_4748" s="T80">pers</ta>
            <ta e="T82" id="Seg_4749" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_4750" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_4751" s="T83">conj</ta>
            <ta e="T85" id="Seg_4752" s="T84">conj</ta>
            <ta e="T86" id="Seg_4753" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_4754" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_4755" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_4756" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_4757" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_4758" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_4759" s="T91">adv</ta>
            <ta e="T93" id="Seg_4760" s="T92">dempro-n:case</ta>
            <ta e="T94" id="Seg_4761" s="T93">adv</ta>
            <ta e="T95" id="Seg_4762" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_4763" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_4764" s="T96">que-n:case</ta>
            <ta e="T98" id="Seg_4765" s="T97">v-v:n.fin</ta>
            <ta e="T99" id="Seg_4766" s="T98">adv</ta>
            <ta e="T100" id="Seg_4767" s="T99">dempro-n:num</ta>
            <ta e="T101" id="Seg_4768" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_4769" s="T101">n-n:num</ta>
            <ta e="T103" id="Seg_4770" s="T102">v-v:mood.pn</ta>
            <ta e="T104" id="Seg_4771" s="T103">adj-n:case</ta>
            <ta e="T105" id="Seg_4772" s="T104">dempro-n:case</ta>
            <ta e="T106" id="Seg_4773" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_4774" s="T106">dempro-n:num-n:case</ta>
            <ta e="T108" id="Seg_4775" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_4776" s="T108">conj</ta>
            <ta e="T110" id="Seg_4777" s="T109">conj</ta>
            <ta e="T111" id="Seg_4778" s="T110">adj-n:case</ta>
            <ta e="T112" id="Seg_4779" s="T111">v-v&gt;v-v:pn</ta>
            <ta e="T113" id="Seg_4780" s="T112">pers</ta>
            <ta e="T114" id="Seg_4781" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_4782" s="T114">propr-n:case</ta>
            <ta e="T116" id="Seg_4783" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_4784" s="T116">dempro-n:case</ta>
            <ta e="T118" id="Seg_4785" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_4786" s="T118">dempro-n:num-n:case</ta>
            <ta e="T120" id="Seg_4787" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_4788" s="T120">conj</ta>
            <ta e="T122" id="Seg_4789" s="T121">dempro-n:case</ta>
            <ta e="T123" id="Seg_4790" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_4791" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_4792" s="T124">adv</ta>
            <ta e="T126" id="Seg_4793" s="T125">num-num&gt;adv</ta>
            <ta e="T127" id="Seg_4794" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_4795" s="T127">adv</ta>
            <ta e="T129" id="Seg_4796" s="T128">dempro-n:num-n:case</ta>
            <ta e="T130" id="Seg_4797" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_4798" s="T130">quant</ta>
            <ta e="T132" id="Seg_4799" s="T131">que-n:case</ta>
            <ta e="T133" id="Seg_4800" s="T132">conj</ta>
            <ta e="T134" id="Seg_4801" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_4802" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_4803" s="T135">dempro-n:case</ta>
            <ta e="T137" id="Seg_4804" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_4805" s="T137">conj</ta>
            <ta e="T139" id="Seg_4806" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_4807" s="T139">adv</ta>
            <ta e="T141" id="Seg_4808" s="T140">dempro-n:case</ta>
            <ta e="T142" id="Seg_4809" s="T141">refl-n:case.poss</ta>
            <ta e="T143" id="Seg_4810" s="T142">%%</ta>
            <ta e="T144" id="Seg_4811" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_4812" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_4813" s="T145">dempro-n:case</ta>
            <ta e="T147" id="Seg_4814" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_4815" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_4816" s="T148">n-n:case.poss</ta>
            <ta e="T150" id="Seg_4817" s="T149">n-n:case.poss</ta>
            <ta e="T151" id="Seg_4818" s="T150">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_4819" s="T151">dempro-n:case</ta>
            <ta e="T153" id="Seg_4820" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_4821" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_4822" s="T154">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_4823" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_4824" s="T156">v-v&gt;v-v:pn</ta>
            <ta e="T158" id="Seg_4825" s="T157">conj</ta>
            <ta e="T159" id="Seg_4826" s="T158">dempro-n:case</ta>
            <ta e="T160" id="Seg_4827" s="T159">n-n:num-n:case.poss</ta>
            <ta e="T161" id="Seg_4828" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_4829" s="T161">conj</ta>
            <ta e="T163" id="Seg_4830" s="T162">adv</ta>
            <ta e="T164" id="Seg_4831" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_4832" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_4833" s="T165">adv</ta>
            <ta e="T167" id="Seg_4834" s="T166">dempro-n:case</ta>
            <ta e="T169" id="Seg_4835" s="T168">adv</ta>
            <ta e="T170" id="Seg_4836" s="T169">n-n:case.poss</ta>
            <ta e="T171" id="Seg_4837" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_4838" s="T171">adj-n:case</ta>
            <ta e="T173" id="Seg_4839" s="T172">adv</ta>
            <ta e="T174" id="Seg_4840" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_4841" s="T174">adv</ta>
            <ta e="T176" id="Seg_4842" s="T175">n-n:num</ta>
            <ta e="T178" id="Seg_4843" s="T177">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_4844" s="T178">conj</ta>
            <ta e="T180" id="Seg_4845" s="T179">dempro-n:case</ta>
            <ta e="T182" id="Seg_4846" s="T181">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_4847" s="T182">adj-n:case</ta>
            <ta e="T184" id="Seg_4848" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_4849" s="T184">conj</ta>
            <ta e="T186" id="Seg_4850" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_4851" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_4852" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_4853" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_4854" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_4855" s="T190">n-n:case.poss</ta>
            <ta e="T192" id="Seg_4856" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_4857" s="T192">conj</ta>
            <ta e="T453" id="Seg_4858" s="T193">v-v:n-fin</ta>
            <ta e="T194" id="Seg_4859" s="T453">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_4860" s="T195">dempro-n:case</ta>
            <ta e="T197" id="Seg_4861" s="T196">n-n:num-n:case.poss</ta>
            <ta e="T198" id="Seg_4862" s="T197">dempro-n:case</ta>
            <ta e="T199" id="Seg_4863" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_4864" s="T199">n-n:num</ta>
            <ta e="T201" id="Seg_4865" s="T200">conj</ta>
            <ta e="T202" id="Seg_4866" s="T201">dempro-n:case</ta>
            <ta e="T203" id="Seg_4867" s="T202">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_4868" s="T203">dempro-n:case</ta>
            <ta e="T205" id="Seg_4869" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_4870" s="T205">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_4871" s="T206">conj</ta>
            <ta e="T208" id="Seg_4872" s="T207">dempro-n:case</ta>
            <ta e="T209" id="Seg_4873" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_4874" s="T209">n-n:case.poss</ta>
            <ta e="T211" id="Seg_4875" s="T210">dempro-n:num</ta>
            <ta e="T212" id="Seg_4876" s="T211">adv</ta>
            <ta e="T213" id="Seg_4877" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_4878" s="T213">n-n:num</ta>
            <ta e="T215" id="Seg_4879" s="T214">dempro-n:case</ta>
            <ta e="T216" id="Seg_4880" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_4881" s="T216">v-v:n.fin</ta>
            <ta e="T218" id="Seg_4882" s="T217">v</ta>
            <ta e="T219" id="Seg_4883" s="T218">n-n:case.poss</ta>
            <ta e="T220" id="Seg_4884" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4885" s="T220">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_4886" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_4887" s="T222">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_4888" s="T223">adv</ta>
            <ta e="T225" id="Seg_4889" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_4890" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_4891" s="T226">conj</ta>
            <ta e="T229" id="Seg_4892" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_4893" s="T229">que</ta>
            <ta e="T231" id="Seg_4894" s="T230">n-n&gt;adj-n:case</ta>
            <ta e="T232" id="Seg_4895" s="T231">n-n:num</ta>
            <ta e="T233" id="Seg_4896" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_4897" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_4898" s="T234">adv</ta>
            <ta e="T236" id="Seg_4899" s="T235">pers</ta>
            <ta e="T237" id="Seg_4900" s="T236">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_4901" s="T238">adv</ta>
            <ta e="T240" id="Seg_4902" s="T239">dempro-n:case</ta>
            <ta e="T241" id="Seg_4903" s="T240">v-v:tense-v:pn</ta>
            <ta e="T242" id="Seg_4904" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_4905" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_4906" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_4907" s="T244">adv</ta>
            <ta e="T246" id="Seg_4908" s="T245">conj</ta>
            <ta e="T247" id="Seg_4909" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_4910" s="T247">v-v&gt;v-v:pn</ta>
            <ta e="T249" id="Seg_4911" s="T248">dempro-n:case</ta>
            <ta e="T250" id="Seg_4912" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_4913" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_4914" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_4915" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_4916" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_4917" s="T254">conj</ta>
            <ta e="T256" id="Seg_4918" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_4919" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_4920" s="T257">n-n:num</ta>
            <ta e="T259" id="Seg_4921" s="T258">v-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_4922" s="T259">n-n&gt;adj-n:case</ta>
            <ta e="T261" id="Seg_4923" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_4924" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_4925" s="T262">n-n&gt;adj-n:case</ta>
            <ta e="T264" id="Seg_4926" s="T263">adv</ta>
            <ta e="T265" id="Seg_4927" s="T264">v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_4928" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_4929" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_4930" s="T267">adv-%%</ta>
            <ta e="T269" id="Seg_4931" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_4932" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_4933" s="T270">adv</ta>
            <ta e="T272" id="Seg_4934" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_4935" s="T272">v-v&gt;v-v:pn</ta>
            <ta e="T274" id="Seg_4936" s="T273">n-n:case</ta>
            <ta e="T276" id="Seg_4937" s="T275">v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_4938" s="T276">adv</ta>
            <ta e="T278" id="Seg_4939" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_4940" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_4941" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_4942" s="T280">adv</ta>
            <ta e="T282" id="Seg_4943" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_4944" s="T282">v-v&gt;v-v:pn</ta>
            <ta e="T284" id="Seg_4945" s="T283">dempro-n:case</ta>
            <ta e="T285" id="Seg_4946" s="T284">v-v&gt;v-v:pn</ta>
            <ta e="T286" id="Seg_4947" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_4948" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_4949" s="T287">v-v:mood.pn</ta>
            <ta e="T289" id="Seg_4950" s="T288">que</ta>
            <ta e="T290" id="Seg_4951" s="T289">pers</ta>
            <ta e="T291" id="Seg_4952" s="T290">n-n:case.poss</ta>
            <ta e="T454" id="Seg_4953" s="T291">v-v:n-fin</ta>
            <ta e="T292" id="Seg_4954" s="T454">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_4955" s="T292">dempro-n:case</ta>
            <ta e="T294" id="Seg_4956" s="T293">v-v&gt;v-v:pn</ta>
            <ta e="T295" id="Seg_4957" s="T294">pers</ta>
            <ta e="T296" id="Seg_4958" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4959" s="T296">v-v:pn</ta>
            <ta e="T298" id="Seg_4960" s="T297">que</ta>
            <ta e="T300" id="Seg_4961" s="T299">que=ptcl</ta>
            <ta e="T301" id="Seg_4962" s="T300">v-v:ins-v:mood.pn</ta>
            <ta e="T302" id="Seg_4963" s="T301">pers</ta>
            <ta e="T303" id="Seg_4964" s="T302">n-n:case.poss</ta>
            <ta e="T304" id="Seg_4965" s="T303">dempro-n:case</ta>
            <ta e="T305" id="Seg_4966" s="T304">v-v:pn</ta>
            <ta e="T306" id="Seg_4967" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_4968" s="T306">pers</ta>
            <ta e="T308" id="Seg_4969" s="T307">adv</ta>
            <ta e="T309" id="Seg_4970" s="T308">dempro-n:case</ta>
            <ta e="T310" id="Seg_4971" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_4972" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_4973" s="T311">adj-n:case</ta>
            <ta e="T313" id="Seg_4974" s="T312">n-n:num</ta>
            <ta e="T314" id="Seg_4975" s="T313">adj-n:case</ta>
            <ta e="T315" id="Seg_4976" s="T314">conj</ta>
            <ta e="T316" id="Seg_4977" s="T315">n-n:num</ta>
            <ta e="T317" id="Seg_4978" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_4979" s="T317">v-v:ins-v:mood.pn</ta>
            <ta e="T319" id="Seg_4980" s="T318">dempro-n:case</ta>
            <ta e="T320" id="Seg_4981" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_4982" s="T320">conj</ta>
            <ta e="T322" id="Seg_4983" s="T321">pers</ta>
            <ta e="T323" id="Seg_4984" s="T322">v-v:ins-v:mood.pn</ta>
            <ta e="T324" id="Seg_4985" s="T323">adv</ta>
            <ta e="T325" id="Seg_4986" s="T324">dempro-n:case</ta>
            <ta e="T326" id="Seg_4987" s="T325">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_4988" s="T327">adv</ta>
            <ta e="T329" id="Seg_4989" s="T328">dempro-n:case</ta>
            <ta e="T330" id="Seg_4990" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_4991" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_4992" s="T331">adj-n:case</ta>
            <ta e="T333" id="Seg_4993" s="T332">n-n:case.poss</ta>
            <ta e="T334" id="Seg_4994" s="T333">v-v:tense-v:pn</ta>
            <ta e="T335" id="Seg_4995" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_4996" s="T335">n-n:case.poss</ta>
            <ta e="T337" id="Seg_4997" s="T336">ptcl</ta>
            <ta e="T340" id="Seg_4998" s="T339">adj-n:case</ta>
            <ta e="T341" id="Seg_4999" s="T340">n-n:case.poss</ta>
            <ta e="T342" id="Seg_5000" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_5001" s="T342">n-n:case</ta>
            <ta e="T345" id="Seg_5002" s="T344">adv</ta>
            <ta e="T346" id="Seg_5003" s="T345">adv</ta>
            <ta e="T347" id="Seg_5004" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_5005" s="T347">v-v&gt;v-v:pn</ta>
            <ta e="T349" id="Seg_5006" s="T348">v-v&gt;v-v:pn</ta>
            <ta e="T350" id="Seg_5007" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_5008" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_5009" s="T351">v-v:mood.pn</ta>
            <ta e="T353" id="Seg_5010" s="T352">pers</ta>
            <ta e="T354" id="Seg_5011" s="T353">que</ta>
            <ta e="T355" id="Seg_5012" s="T354">pers</ta>
            <ta e="T356" id="Seg_5013" s="T355">n-n:case</ta>
            <ta e="T455" id="Seg_5014" s="T356">v-v:n-fin</ta>
            <ta e="T357" id="Seg_5015" s="T455">v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_5016" s="T357">pers</ta>
            <ta e="T359" id="Seg_5017" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5018" s="T359">v-v:pn</ta>
            <ta e="T361" id="Seg_5019" s="T360">v-v:ins-v:mood.pn</ta>
            <ta e="T362" id="Seg_5020" s="T361">pers</ta>
            <ta e="T363" id="Seg_5021" s="T362">n-n:case.poss</ta>
            <ta e="T364" id="Seg_5022" s="T363">dempro-n:case</ta>
            <ta e="T365" id="Seg_5023" s="T364">v-v:tense-v:pn</ta>
            <ta e="T366" id="Seg_5024" s="T365">que</ta>
            <ta e="T367" id="Seg_5025" s="T366">dempro-n:case</ta>
            <ta e="T368" id="Seg_5026" s="T367">dempro-n:case</ta>
            <ta e="T369" id="Seg_5027" s="T368">dempro-n:case</ta>
            <ta e="T370" id="Seg_5028" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_5029" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_5030" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_5031" s="T372">conj</ta>
            <ta e="T374" id="Seg_5032" s="T373">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_5033" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_5034" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_5035" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_5036" s="T377">adj-n:case</ta>
            <ta e="T379" id="Seg_5037" s="T378">conj</ta>
            <ta e="T380" id="Seg_5038" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_5039" s="T380">adj-n:case</ta>
            <ta e="T382" id="Seg_5040" s="T381">adv</ta>
            <ta e="T384" id="Seg_5041" s="T383">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_5042" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_5043" s="T385">dempro-n:case</ta>
            <ta e="T388" id="Seg_5044" s="T387">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_5045" s="T388">pers</ta>
            <ta e="T391" id="Seg_5046" s="T390">v-v:ins-v:mood.pn</ta>
            <ta e="T392" id="Seg_5047" s="T391">adv</ta>
            <ta e="T393" id="Seg_5048" s="T392">que</ta>
            <ta e="T394" id="Seg_5049" s="T393">dempro-n:case</ta>
            <ta e="T395" id="Seg_5050" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_5051" s="T395">adv</ta>
            <ta e="T397" id="Seg_5052" s="T396">dempro-n:case</ta>
            <ta e="T398" id="Seg_5053" s="T397">v-v:tense-v:pn</ta>
            <ta e="T399" id="Seg_5054" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_5055" s="T399">num-n:case</ta>
            <ta e="T401" id="Seg_5056" s="T400">n-n:case.poss</ta>
            <ta e="T402" id="Seg_5057" s="T401">v-v:tense-v:pn</ta>
            <ta e="T403" id="Seg_5058" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_5059" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_5060" s="T404">v-v&gt;v-v:pn</ta>
            <ta e="T406" id="Seg_5061" s="T405">dempro-n:case</ta>
            <ta e="T407" id="Seg_5062" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_5063" s="T407">adv</ta>
            <ta e="T409" id="Seg_5064" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_5065" s="T409">v-v&gt;v-v:pn</ta>
            <ta e="T411" id="Seg_5066" s="T410">adv</ta>
            <ta e="T412" id="Seg_5067" s="T411">adv</ta>
            <ta e="T413" id="Seg_5068" s="T412">v-v&gt;v-v:pn</ta>
            <ta e="T414" id="Seg_5069" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_5070" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_5071" s="T415">v-v:mood.pn</ta>
            <ta e="T417" id="Seg_5072" s="T416">pers</ta>
            <ta e="T418" id="Seg_5073" s="T417">que</ta>
            <ta e="T419" id="Seg_5074" s="T418">pers</ta>
            <ta e="T420" id="Seg_5075" s="T419">n-n:case.poss</ta>
            <ta e="T456" id="Seg_5076" s="T420">v-v:n-fin</ta>
            <ta e="T421" id="Seg_5077" s="T456">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_5078" s="T421">pers</ta>
            <ta e="T423" id="Seg_5079" s="T422">v-v:pn</ta>
            <ta e="T424" id="Seg_5080" s="T423">v-v:ins-v:mood.pn</ta>
            <ta e="T425" id="Seg_5081" s="T424">adv</ta>
            <ta e="T426" id="Seg_5082" s="T425">dempro-n:case</ta>
            <ta e="T427" id="Seg_5083" s="T426">v-v:tense-v:pn</ta>
            <ta e="T428" id="Seg_5084" s="T427">dempro-n:case</ta>
            <ta e="T429" id="Seg_5085" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_5086" s="T429">adj-n:case</ta>
            <ta e="T431" id="Seg_5087" s="T430">adj-n:case</ta>
            <ta e="T432" id="Seg_5088" s="T431">ptcl</ta>
            <ta e="T434" id="Seg_5089" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_5090" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_5091" s="T435">adj-n:case</ta>
            <ta e="T437" id="Seg_5092" s="T436">n-n:case.poss</ta>
            <ta e="T438" id="Seg_5093" s="T437">v-v:ins-v:mood.pn</ta>
            <ta e="T439" id="Seg_5094" s="T438">que=ptcl</ta>
            <ta e="T440" id="Seg_5095" s="T439">v-v&gt;v-v:pn</ta>
            <ta e="T441" id="Seg_5096" s="T440">dempro-n:case</ta>
            <ta e="T442" id="Seg_5097" s="T441">v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_5098" s="T442">ptcl</ta>
            <ta e="T445" id="Seg_5099" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_5100" s="T445">que</ta>
            <ta e="T447" id="Seg_5101" s="T446">dempro-n:case</ta>
            <ta e="T448" id="Seg_5102" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_5103" s="T448">pers</ta>
            <ta e="T450" id="Seg_5104" s="T449">adv</ta>
            <ta e="T451" id="Seg_5105" s="T450">v-v:ins-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5106" s="T0">num</ta>
            <ta e="T2" id="Seg_5107" s="T1">n</ta>
            <ta e="T3" id="Seg_5108" s="T2">v</ta>
            <ta e="T4" id="Seg_5109" s="T3">dempro</ta>
            <ta e="T5" id="Seg_5110" s="T4">num</ta>
            <ta e="T6" id="Seg_5111" s="T5">n</ta>
            <ta e="T7" id="Seg_5112" s="T6">v</ta>
            <ta e="T8" id="Seg_5113" s="T7">dempro</ta>
            <ta e="T10" id="Seg_5114" s="T9">v</ta>
            <ta e="T11" id="Seg_5115" s="T10">n</ta>
            <ta e="T12" id="Seg_5116" s="T11">v</ta>
            <ta e="T13" id="Seg_5117" s="T12">que</ta>
            <ta e="T14" id="Seg_5118" s="T13">pers</ta>
            <ta e="T15" id="Seg_5119" s="T14">v</ta>
            <ta e="T16" id="Seg_5120" s="T15">num</ta>
            <ta e="T17" id="Seg_5121" s="T16">v</ta>
            <ta e="T18" id="Seg_5122" s="T17">pers</ta>
            <ta e="T19" id="Seg_5123" s="T18">v</ta>
            <ta e="T20" id="Seg_5124" s="T19">adj</ta>
            <ta e="T21" id="Seg_5125" s="T20">n</ta>
            <ta e="T22" id="Seg_5126" s="T21">num</ta>
            <ta e="T23" id="Seg_5127" s="T22">v</ta>
            <ta e="T24" id="Seg_5128" s="T23">pers</ta>
            <ta e="T25" id="Seg_5129" s="T24">adj</ta>
            <ta e="T26" id="Seg_5130" s="T25">n</ta>
            <ta e="T28" id="Seg_5131" s="T27">v</ta>
            <ta e="T29" id="Seg_5132" s="T28">conj</ta>
            <ta e="T30" id="Seg_5133" s="T29">adj</ta>
            <ta e="T31" id="Seg_5134" s="T30">n</ta>
            <ta e="T32" id="Seg_5135" s="T31">v</ta>
            <ta e="T33" id="Seg_5136" s="T32">conj</ta>
            <ta e="T34" id="Seg_5137" s="T33">adj</ta>
            <ta e="T36" id="Seg_5138" s="T35">conj</ta>
            <ta e="T37" id="Seg_5139" s="T36">pers</ta>
            <ta e="T38" id="Seg_5140" s="T37">que</ta>
            <ta e="T39" id="Seg_5141" s="T38">v</ta>
            <ta e="T40" id="Seg_5142" s="T39">pers</ta>
            <ta e="T41" id="Seg_5143" s="T40">v</ta>
            <ta e="T42" id="Seg_5144" s="T41">propr</ta>
            <ta e="T43" id="Seg_5145" s="T42">n</ta>
            <ta e="T45" id="Seg_5146" s="T44">dempro</ta>
            <ta e="T46" id="Seg_5147" s="T45">v</ta>
            <ta e="T47" id="Seg_5148" s="T46">v</ta>
            <ta e="T48" id="Seg_5149" s="T47">pers</ta>
            <ta e="T49" id="Seg_5150" s="T48">propr</ta>
            <ta e="T50" id="Seg_5151" s="T49">n</ta>
            <ta e="T51" id="Seg_5152" s="T50">dempro</ta>
            <ta e="T52" id="Seg_5153" s="T51">v</ta>
            <ta e="T53" id="Seg_5154" s="T52">dempro</ta>
            <ta e="T54" id="Seg_5155" s="T53">v</ta>
            <ta e="T55" id="Seg_5156" s="T54">n</ta>
            <ta e="T56" id="Seg_5157" s="T55">dempro</ta>
            <ta e="T57" id="Seg_5158" s="T56">n</ta>
            <ta e="T58" id="Seg_5159" s="T57">v</ta>
            <ta e="T59" id="Seg_5160" s="T58">conj</ta>
            <ta e="T61" id="Seg_5161" s="T60">conj</ta>
            <ta e="T62" id="Seg_5162" s="T61">n</ta>
            <ta e="T63" id="Seg_5163" s="T62">v</ta>
            <ta e="T64" id="Seg_5164" s="T63">v</ta>
            <ta e="T65" id="Seg_5165" s="T64">v</ta>
            <ta e="T66" id="Seg_5166" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_5167" s="T66">v</ta>
            <ta e="T68" id="Seg_5168" s="T67">adv</ta>
            <ta e="T69" id="Seg_5169" s="T68">v</ta>
            <ta e="T70" id="Seg_5170" s="T69">n</ta>
            <ta e="T71" id="Seg_5171" s="T70">dempro</ta>
            <ta e="T72" id="Seg_5172" s="T71">dempro</ta>
            <ta e="T73" id="Seg_5173" s="T72">num</ta>
            <ta e="T74" id="Seg_5174" s="T73">n</ta>
            <ta e="T75" id="Seg_5175" s="T74">v</ta>
            <ta e="T76" id="Seg_5176" s="T75">pers</ta>
            <ta e="T77" id="Seg_5177" s="T76">que</ta>
            <ta e="T78" id="Seg_5178" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_5179" s="T78">v</ta>
            <ta e="T80" id="Seg_5180" s="T79">conj</ta>
            <ta e="T81" id="Seg_5181" s="T80">pers</ta>
            <ta e="T82" id="Seg_5182" s="T81">v</ta>
            <ta e="T83" id="Seg_5183" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_5184" s="T83">conj</ta>
            <ta e="T85" id="Seg_5185" s="T84">conj</ta>
            <ta e="T86" id="Seg_5186" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_5187" s="T86">v</ta>
            <ta e="T88" id="Seg_5188" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_5189" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_5190" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_5191" s="T90">v</ta>
            <ta e="T92" id="Seg_5192" s="T91">adv</ta>
            <ta e="T93" id="Seg_5193" s="T92">dempro</ta>
            <ta e="T94" id="Seg_5194" s="T93">adv</ta>
            <ta e="T95" id="Seg_5195" s="T94">v</ta>
            <ta e="T96" id="Seg_5196" s="T95">n</ta>
            <ta e="T97" id="Seg_5197" s="T96">que</ta>
            <ta e="T98" id="Seg_5198" s="T97">v</ta>
            <ta e="T99" id="Seg_5199" s="T98">adv</ta>
            <ta e="T100" id="Seg_5200" s="T99">dempro</ta>
            <ta e="T101" id="Seg_5201" s="T100">v</ta>
            <ta e="T102" id="Seg_5202" s="T101">n</ta>
            <ta e="T103" id="Seg_5203" s="T102">v</ta>
            <ta e="T104" id="Seg_5204" s="T103">adj</ta>
            <ta e="T105" id="Seg_5205" s="T104">dempro</ta>
            <ta e="T106" id="Seg_5206" s="T105">v</ta>
            <ta e="T107" id="Seg_5207" s="T106">dempro</ta>
            <ta e="T108" id="Seg_5208" s="T107">v</ta>
            <ta e="T109" id="Seg_5209" s="T108">conj</ta>
            <ta e="T110" id="Seg_5210" s="T109">conj</ta>
            <ta e="T111" id="Seg_5211" s="T110">adj</ta>
            <ta e="T112" id="Seg_5212" s="T111">v</ta>
            <ta e="T113" id="Seg_5213" s="T112">pers</ta>
            <ta e="T114" id="Seg_5214" s="T113">v</ta>
            <ta e="T115" id="Seg_5215" s="T114">propr</ta>
            <ta e="T116" id="Seg_5216" s="T115">n</ta>
            <ta e="T117" id="Seg_5217" s="T116">dempro</ta>
            <ta e="T118" id="Seg_5218" s="T117">v</ta>
            <ta e="T119" id="Seg_5219" s="T118">dempro</ta>
            <ta e="T120" id="Seg_5220" s="T119">v</ta>
            <ta e="T121" id="Seg_5221" s="T120">conj</ta>
            <ta e="T122" id="Seg_5222" s="T121">dempro</ta>
            <ta e="T123" id="Seg_5223" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_5224" s="T123">v</ta>
            <ta e="T125" id="Seg_5225" s="T124">adv</ta>
            <ta e="T126" id="Seg_5226" s="T125">num</ta>
            <ta e="T127" id="Seg_5227" s="T126">v</ta>
            <ta e="T128" id="Seg_5228" s="T127">adv</ta>
            <ta e="T129" id="Seg_5229" s="T128">dempro</ta>
            <ta e="T130" id="Seg_5230" s="T129">v</ta>
            <ta e="T131" id="Seg_5231" s="T130">quant</ta>
            <ta e="T132" id="Seg_5232" s="T131">que</ta>
            <ta e="T133" id="Seg_5233" s="T132">conj</ta>
            <ta e="T134" id="Seg_5234" s="T133">dempro</ta>
            <ta e="T135" id="Seg_5235" s="T134">v</ta>
            <ta e="T136" id="Seg_5236" s="T135">dempro</ta>
            <ta e="T137" id="Seg_5237" s="T136">n</ta>
            <ta e="T138" id="Seg_5238" s="T137">conj</ta>
            <ta e="T139" id="Seg_5239" s="T138">v</ta>
            <ta e="T140" id="Seg_5240" s="T139">adv</ta>
            <ta e="T141" id="Seg_5241" s="T140">dempro</ta>
            <ta e="T142" id="Seg_5242" s="T141">refl</ta>
            <ta e="T144" id="Seg_5243" s="T143">v</ta>
            <ta e="T145" id="Seg_5244" s="T144">v</ta>
            <ta e="T146" id="Seg_5245" s="T145">dempro</ta>
            <ta e="T147" id="Seg_5246" s="T146">n</ta>
            <ta e="T148" id="Seg_5247" s="T147">v</ta>
            <ta e="T149" id="Seg_5248" s="T148">n</ta>
            <ta e="T150" id="Seg_5249" s="T149">n</ta>
            <ta e="T151" id="Seg_5250" s="T150">v</ta>
            <ta e="T152" id="Seg_5251" s="T151">dempro</ta>
            <ta e="T153" id="Seg_5252" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_5253" s="T153">n</ta>
            <ta e="T155" id="Seg_5254" s="T154">v</ta>
            <ta e="T156" id="Seg_5255" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_5256" s="T156">v</ta>
            <ta e="T158" id="Seg_5257" s="T157">conj</ta>
            <ta e="T159" id="Seg_5258" s="T158">dempro</ta>
            <ta e="T160" id="Seg_5259" s="T159">n</ta>
            <ta e="T161" id="Seg_5260" s="T160">v</ta>
            <ta e="T162" id="Seg_5261" s="T161">conj</ta>
            <ta e="T163" id="Seg_5262" s="T162">adv</ta>
            <ta e="T164" id="Seg_5263" s="T163">n</ta>
            <ta e="T165" id="Seg_5264" s="T164">v</ta>
            <ta e="T166" id="Seg_5265" s="T165">adv</ta>
            <ta e="T167" id="Seg_5266" s="T166">dempro</ta>
            <ta e="T169" id="Seg_5267" s="T168">adv</ta>
            <ta e="T170" id="Seg_5268" s="T169">n</ta>
            <ta e="T171" id="Seg_5269" s="T170">v</ta>
            <ta e="T172" id="Seg_5270" s="T171">adj</ta>
            <ta e="T173" id="Seg_5271" s="T172">adv</ta>
            <ta e="T174" id="Seg_5272" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_5273" s="T174">adv</ta>
            <ta e="T176" id="Seg_5274" s="T175">n</ta>
            <ta e="T178" id="Seg_5275" s="T177">v</ta>
            <ta e="T179" id="Seg_5276" s="T178">conj</ta>
            <ta e="T180" id="Seg_5277" s="T179">dempro</ta>
            <ta e="T182" id="Seg_5278" s="T181">v</ta>
            <ta e="T183" id="Seg_5279" s="T182">adj</ta>
            <ta e="T184" id="Seg_5280" s="T183">n</ta>
            <ta e="T185" id="Seg_5281" s="T184">conj</ta>
            <ta e="T186" id="Seg_5282" s="T185">v</ta>
            <ta e="T187" id="Seg_5283" s="T186">dempro</ta>
            <ta e="T188" id="Seg_5284" s="T187">v</ta>
            <ta e="T189" id="Seg_5285" s="T188">v</ta>
            <ta e="T190" id="Seg_5286" s="T189">v</ta>
            <ta e="T191" id="Seg_5287" s="T190">n</ta>
            <ta e="T192" id="Seg_5288" s="T191">v</ta>
            <ta e="T193" id="Seg_5289" s="T192">conj</ta>
            <ta e="T453" id="Seg_5290" s="T193">v</ta>
            <ta e="T194" id="Seg_5291" s="T453">v</ta>
            <ta e="T196" id="Seg_5292" s="T195">dempro</ta>
            <ta e="T197" id="Seg_5293" s="T196">n</ta>
            <ta e="T198" id="Seg_5294" s="T197">dempro</ta>
            <ta e="T199" id="Seg_5295" s="T198">v</ta>
            <ta e="T200" id="Seg_5296" s="T199">n</ta>
            <ta e="T201" id="Seg_5297" s="T200">conj</ta>
            <ta e="T202" id="Seg_5298" s="T201">dempro</ta>
            <ta e="T203" id="Seg_5299" s="T202">v</ta>
            <ta e="T204" id="Seg_5300" s="T203">dempro</ta>
            <ta e="T205" id="Seg_5301" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_5302" s="T205">v</ta>
            <ta e="T207" id="Seg_5303" s="T206">conj</ta>
            <ta e="T208" id="Seg_5304" s="T207">dempro</ta>
            <ta e="T209" id="Seg_5305" s="T208">v</ta>
            <ta e="T210" id="Seg_5306" s="T209">n</ta>
            <ta e="T211" id="Seg_5307" s="T210">dempro</ta>
            <ta e="T212" id="Seg_5308" s="T211">adv</ta>
            <ta e="T213" id="Seg_5309" s="T212">v</ta>
            <ta e="T214" id="Seg_5310" s="T213">n</ta>
            <ta e="T215" id="Seg_5311" s="T214">dempro</ta>
            <ta e="T216" id="Seg_5312" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_5313" s="T216">v</ta>
            <ta e="T218" id="Seg_5314" s="T217">v</ta>
            <ta e="T219" id="Seg_5315" s="T218">n</ta>
            <ta e="T220" id="Seg_5316" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_5317" s="T220">v</ta>
            <ta e="T222" id="Seg_5318" s="T221">n</ta>
            <ta e="T223" id="Seg_5319" s="T222">v</ta>
            <ta e="T224" id="Seg_5320" s="T223">adv</ta>
            <ta e="T225" id="Seg_5321" s="T224">n</ta>
            <ta e="T226" id="Seg_5322" s="T225">v</ta>
            <ta e="T227" id="Seg_5323" s="T226">conj</ta>
            <ta e="T229" id="Seg_5324" s="T228">v</ta>
            <ta e="T230" id="Seg_5325" s="T229">que</ta>
            <ta e="T231" id="Seg_5326" s="T230">n</ta>
            <ta e="T232" id="Seg_5327" s="T231">n</ta>
            <ta e="T233" id="Seg_5328" s="T232">v</ta>
            <ta e="T234" id="Seg_5329" s="T233">v</ta>
            <ta e="T235" id="Seg_5330" s="T234">adv</ta>
            <ta e="T236" id="Seg_5331" s="T235">pers</ta>
            <ta e="T237" id="Seg_5332" s="T236">v</ta>
            <ta e="T239" id="Seg_5333" s="T238">adv</ta>
            <ta e="T240" id="Seg_5334" s="T239">dempro</ta>
            <ta e="T241" id="Seg_5335" s="T240">v</ta>
            <ta e="T242" id="Seg_5336" s="T241">v</ta>
            <ta e="T243" id="Seg_5337" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_5338" s="T243">n</ta>
            <ta e="T245" id="Seg_5339" s="T244">adv</ta>
            <ta e="T246" id="Seg_5340" s="T245">conj</ta>
            <ta e="T247" id="Seg_5341" s="T246">n</ta>
            <ta e="T248" id="Seg_5342" s="T247">v</ta>
            <ta e="T249" id="Seg_5343" s="T248">dempro</ta>
            <ta e="T250" id="Seg_5344" s="T249">n</ta>
            <ta e="T251" id="Seg_5345" s="T250">v</ta>
            <ta e="T252" id="Seg_5346" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_5347" s="T252">v</ta>
            <ta e="T254" id="Seg_5348" s="T253">v</ta>
            <ta e="T255" id="Seg_5349" s="T254">conj</ta>
            <ta e="T256" id="Seg_5350" s="T255">v</ta>
            <ta e="T257" id="Seg_5351" s="T256">n</ta>
            <ta e="T258" id="Seg_5352" s="T257">n</ta>
            <ta e="T259" id="Seg_5353" s="T258">v</ta>
            <ta e="T260" id="Seg_5354" s="T259">n</ta>
            <ta e="T261" id="Seg_5355" s="T260">n</ta>
            <ta e="T262" id="Seg_5356" s="T261">v</ta>
            <ta e="T263" id="Seg_5357" s="T262">n</ta>
            <ta e="T264" id="Seg_5358" s="T263">adv</ta>
            <ta e="T265" id="Seg_5359" s="T264">v</ta>
            <ta e="T266" id="Seg_5360" s="T265">v</ta>
            <ta e="T267" id="Seg_5361" s="T266">v</ta>
            <ta e="T268" id="Seg_5362" s="T267">adv</ta>
            <ta e="T269" id="Seg_5363" s="T268">v</ta>
            <ta e="T270" id="Seg_5364" s="T269">v</ta>
            <ta e="T271" id="Seg_5365" s="T270">adv</ta>
            <ta e="T272" id="Seg_5366" s="T271">v</ta>
            <ta e="T273" id="Seg_5367" s="T272">v</ta>
            <ta e="T274" id="Seg_5368" s="T273">n</ta>
            <ta e="T276" id="Seg_5369" s="T275">v</ta>
            <ta e="T277" id="Seg_5370" s="T276">adv</ta>
            <ta e="T278" id="Seg_5371" s="T277">dempro</ta>
            <ta e="T279" id="Seg_5372" s="T278">n</ta>
            <ta e="T280" id="Seg_5373" s="T279">v</ta>
            <ta e="T281" id="Seg_5374" s="T280">adv</ta>
            <ta e="T282" id="Seg_5375" s="T281">n</ta>
            <ta e="T283" id="Seg_5376" s="T282">v</ta>
            <ta e="T284" id="Seg_5377" s="T283">dempro</ta>
            <ta e="T285" id="Seg_5378" s="T284">v</ta>
            <ta e="T286" id="Seg_5379" s="T285">n</ta>
            <ta e="T287" id="Seg_5380" s="T286">n</ta>
            <ta e="T288" id="Seg_5381" s="T287">v</ta>
            <ta e="T289" id="Seg_5382" s="T288">que</ta>
            <ta e="T290" id="Seg_5383" s="T289">pers</ta>
            <ta e="T291" id="Seg_5384" s="T290">n</ta>
            <ta e="T454" id="Seg_5385" s="T291">v</ta>
            <ta e="T292" id="Seg_5386" s="T454">v</ta>
            <ta e="T293" id="Seg_5387" s="T292">dempro</ta>
            <ta e="T294" id="Seg_5388" s="T293">v</ta>
            <ta e="T295" id="Seg_5389" s="T294">pers</ta>
            <ta e="T296" id="Seg_5390" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_5391" s="T296">v</ta>
            <ta e="T298" id="Seg_5392" s="T297">que</ta>
            <ta e="T300" id="Seg_5393" s="T299">que</ta>
            <ta e="T301" id="Seg_5394" s="T300">v</ta>
            <ta e="T302" id="Seg_5395" s="T301">pers</ta>
            <ta e="T303" id="Seg_5396" s="T302">n</ta>
            <ta e="T304" id="Seg_5397" s="T303">dempro</ta>
            <ta e="T305" id="Seg_5398" s="T304">v</ta>
            <ta e="T306" id="Seg_5399" s="T305">v</ta>
            <ta e="T307" id="Seg_5400" s="T306">pers</ta>
            <ta e="T308" id="Seg_5401" s="T307">adv</ta>
            <ta e="T309" id="Seg_5402" s="T308">dempro</ta>
            <ta e="T310" id="Seg_5403" s="T309">v</ta>
            <ta e="T311" id="Seg_5404" s="T310">n</ta>
            <ta e="T312" id="Seg_5405" s="T311">adj</ta>
            <ta e="T313" id="Seg_5406" s="T312">n</ta>
            <ta e="T314" id="Seg_5407" s="T313">adj</ta>
            <ta e="T315" id="Seg_5408" s="T314">conj</ta>
            <ta e="T316" id="Seg_5409" s="T315">n</ta>
            <ta e="T317" id="Seg_5410" s="T316">n</ta>
            <ta e="T318" id="Seg_5411" s="T317">v</ta>
            <ta e="T319" id="Seg_5412" s="T318">dempro</ta>
            <ta e="T320" id="Seg_5413" s="T319">v</ta>
            <ta e="T321" id="Seg_5414" s="T320">conj</ta>
            <ta e="T322" id="Seg_5415" s="T321">pers</ta>
            <ta e="T323" id="Seg_5416" s="T322">v</ta>
            <ta e="T324" id="Seg_5417" s="T323">adv</ta>
            <ta e="T325" id="Seg_5418" s="T324">dempro</ta>
            <ta e="T326" id="Seg_5419" s="T325">v</ta>
            <ta e="T328" id="Seg_5420" s="T327">adv</ta>
            <ta e="T329" id="Seg_5421" s="T328">dempro</ta>
            <ta e="T330" id="Seg_5422" s="T329">v</ta>
            <ta e="T331" id="Seg_5423" s="T330">v</ta>
            <ta e="T332" id="Seg_5424" s="T331">adj</ta>
            <ta e="T333" id="Seg_5425" s="T332">n</ta>
            <ta e="T334" id="Seg_5426" s="T333">v</ta>
            <ta e="T335" id="Seg_5427" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5428" s="T335">n</ta>
            <ta e="T337" id="Seg_5429" s="T336">ptcl</ta>
            <ta e="T340" id="Seg_5430" s="T339">adj</ta>
            <ta e="T341" id="Seg_5431" s="T340">n</ta>
            <ta e="T342" id="Seg_5432" s="T341">v</ta>
            <ta e="T343" id="Seg_5433" s="T342">n</ta>
            <ta e="T345" id="Seg_5434" s="T344">adv</ta>
            <ta e="T346" id="Seg_5435" s="T345">adv</ta>
            <ta e="T347" id="Seg_5436" s="T346">n</ta>
            <ta e="T348" id="Seg_5437" s="T347">v</ta>
            <ta e="T349" id="Seg_5438" s="T348">v</ta>
            <ta e="T350" id="Seg_5439" s="T349">n</ta>
            <ta e="T351" id="Seg_5440" s="T350">n</ta>
            <ta e="T352" id="Seg_5441" s="T351">v</ta>
            <ta e="T353" id="Seg_5442" s="T352">pers</ta>
            <ta e="T354" id="Seg_5443" s="T353">que</ta>
            <ta e="T355" id="Seg_5444" s="T354">pers</ta>
            <ta e="T356" id="Seg_5445" s="T355">n</ta>
            <ta e="T455" id="Seg_5446" s="T356">v</ta>
            <ta e="T357" id="Seg_5447" s="T455">v</ta>
            <ta e="T358" id="Seg_5448" s="T357">pers</ta>
            <ta e="T359" id="Seg_5449" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5450" s="T359">v</ta>
            <ta e="T361" id="Seg_5451" s="T360">v</ta>
            <ta e="T362" id="Seg_5452" s="T361">pers</ta>
            <ta e="T363" id="Seg_5453" s="T362">n</ta>
            <ta e="T364" id="Seg_5454" s="T363">dempro</ta>
            <ta e="T365" id="Seg_5455" s="T364">v</ta>
            <ta e="T366" id="Seg_5456" s="T365">que</ta>
            <ta e="T367" id="Seg_5457" s="T366">dempro</ta>
            <ta e="T368" id="Seg_5458" s="T367">dempro</ta>
            <ta e="T369" id="Seg_5459" s="T368">dempro</ta>
            <ta e="T370" id="Seg_5460" s="T369">v</ta>
            <ta e="T371" id="Seg_5461" s="T370">v</ta>
            <ta e="T372" id="Seg_5462" s="T371">n</ta>
            <ta e="T373" id="Seg_5463" s="T372">conj</ta>
            <ta e="T374" id="Seg_5464" s="T373">v</ta>
            <ta e="T375" id="Seg_5465" s="T374">n</ta>
            <ta e="T376" id="Seg_5466" s="T375">n</ta>
            <ta e="T377" id="Seg_5467" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_5468" s="T377">adj</ta>
            <ta e="T379" id="Seg_5469" s="T378">conj</ta>
            <ta e="T380" id="Seg_5470" s="T379">n</ta>
            <ta e="T381" id="Seg_5471" s="T380">adj</ta>
            <ta e="T382" id="Seg_5472" s="T381">adv</ta>
            <ta e="T384" id="Seg_5473" s="T383">v</ta>
            <ta e="T385" id="Seg_5474" s="T384">n</ta>
            <ta e="T386" id="Seg_5475" s="T385">dempro</ta>
            <ta e="T388" id="Seg_5476" s="T387">v</ta>
            <ta e="T389" id="Seg_5477" s="T388">pers</ta>
            <ta e="T391" id="Seg_5478" s="T390">v</ta>
            <ta e="T392" id="Seg_5479" s="T391">adv</ta>
            <ta e="T393" id="Seg_5480" s="T392">que</ta>
            <ta e="T394" id="Seg_5481" s="T393">dempro</ta>
            <ta e="T395" id="Seg_5482" s="T394">v</ta>
            <ta e="T396" id="Seg_5483" s="T395">adv</ta>
            <ta e="T397" id="Seg_5484" s="T396">dempro</ta>
            <ta e="T398" id="Seg_5485" s="T397">v</ta>
            <ta e="T399" id="Seg_5486" s="T398">v</ta>
            <ta e="T400" id="Seg_5487" s="T399">num</ta>
            <ta e="T401" id="Seg_5488" s="T400">n</ta>
            <ta e="T402" id="Seg_5489" s="T401">v</ta>
            <ta e="T403" id="Seg_5490" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_5491" s="T403">n</ta>
            <ta e="T405" id="Seg_5492" s="T404">v</ta>
            <ta e="T406" id="Seg_5493" s="T405">dempro</ta>
            <ta e="T407" id="Seg_5494" s="T406">v</ta>
            <ta e="T408" id="Seg_5495" s="T407">adv</ta>
            <ta e="T409" id="Seg_5496" s="T408">n</ta>
            <ta e="T410" id="Seg_5497" s="T409">v</ta>
            <ta e="T411" id="Seg_5498" s="T410">adv</ta>
            <ta e="T412" id="Seg_5499" s="T411">adv</ta>
            <ta e="T413" id="Seg_5500" s="T412">v</ta>
            <ta e="T414" id="Seg_5501" s="T413">n</ta>
            <ta e="T415" id="Seg_5502" s="T414">n</ta>
            <ta e="T416" id="Seg_5503" s="T415">v</ta>
            <ta e="T417" id="Seg_5504" s="T416">pers</ta>
            <ta e="T418" id="Seg_5505" s="T417">que</ta>
            <ta e="T419" id="Seg_5506" s="T418">pers</ta>
            <ta e="T420" id="Seg_5507" s="T419">n</ta>
            <ta e="T456" id="Seg_5508" s="T420">v</ta>
            <ta e="T421" id="Seg_5509" s="T456">v</ta>
            <ta e="T422" id="Seg_5510" s="T421">pers</ta>
            <ta e="T423" id="Seg_5511" s="T422">v</ta>
            <ta e="T424" id="Seg_5512" s="T423">v</ta>
            <ta e="T425" id="Seg_5513" s="T424">adv</ta>
            <ta e="T426" id="Seg_5514" s="T425">dempro</ta>
            <ta e="T427" id="Seg_5515" s="T426">v</ta>
            <ta e="T428" id="Seg_5516" s="T427">dempro</ta>
            <ta e="T429" id="Seg_5517" s="T428">n</ta>
            <ta e="T430" id="Seg_5518" s="T429">adj</ta>
            <ta e="T431" id="Seg_5519" s="T430">adj</ta>
            <ta e="T432" id="Seg_5520" s="T431">ptcl</ta>
            <ta e="T434" id="Seg_5521" s="T433">n</ta>
            <ta e="T435" id="Seg_5522" s="T434">v</ta>
            <ta e="T436" id="Seg_5523" s="T435">adj</ta>
            <ta e="T437" id="Seg_5524" s="T436">n</ta>
            <ta e="T438" id="Seg_5525" s="T437">v</ta>
            <ta e="T439" id="Seg_5526" s="T438">que</ta>
            <ta e="T440" id="Seg_5527" s="T439">v</ta>
            <ta e="T441" id="Seg_5528" s="T440">dempro</ta>
            <ta e="T442" id="Seg_5529" s="T441">v</ta>
            <ta e="T443" id="Seg_5530" s="T442">ptcl</ta>
            <ta e="T445" id="Seg_5531" s="T444">n</ta>
            <ta e="T446" id="Seg_5532" s="T445">que</ta>
            <ta e="T447" id="Seg_5533" s="T446">dempro</ta>
            <ta e="T448" id="Seg_5534" s="T447">v</ta>
            <ta e="T449" id="Seg_5535" s="T448">pers</ta>
            <ta e="T450" id="Seg_5536" s="T449">adv</ta>
            <ta e="T451" id="Seg_5537" s="T450">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_5538" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_5539" s="T3">pro.h:Poss</ta>
            <ta e="T6" id="Seg_5540" s="T5">np.h:Th</ta>
            <ta e="T8" id="Seg_5541" s="T7">pro.h:A</ta>
            <ta e="T11" id="Seg_5542" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_5543" s="T12">pro:Th</ta>
            <ta e="T16" id="Seg_5544" s="T15">np.h:A</ta>
            <ta e="T18" id="Seg_5545" s="T17">pro.h:B</ta>
            <ta e="T19" id="Seg_5546" s="T18">0.2.h:A</ta>
            <ta e="T21" id="Seg_5547" s="T20">np:Th</ta>
            <ta e="T22" id="Seg_5548" s="T21">np.h:A</ta>
            <ta e="T24" id="Seg_5549" s="T23">pro.h:B</ta>
            <ta e="T26" id="Seg_5550" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_5551" s="T27">0.2.h:A</ta>
            <ta e="T31" id="Seg_5552" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_5553" s="T31">0.2.h:A</ta>
            <ta e="T34" id="Seg_5554" s="T33">np.h:A</ta>
            <ta e="T37" id="Seg_5555" s="T36">pro.h:B</ta>
            <ta e="T38" id="Seg_5556" s="T37">pro:Th</ta>
            <ta e="T40" id="Seg_5557" s="T39">pro.h:B</ta>
            <ta e="T41" id="Seg_5558" s="T40">0.2.h:A</ta>
            <ta e="T42" id="Seg_5559" s="T41">np:Poss</ta>
            <ta e="T43" id="Seg_5560" s="T42">np:Th</ta>
            <ta e="T45" id="Seg_5561" s="T44">pro.h:A</ta>
            <ta e="T47" id="Seg_5562" s="T46">0.2.h:A</ta>
            <ta e="T48" id="Seg_5563" s="T47">pro.h:B</ta>
            <ta e="T49" id="Seg_5564" s="T48">np:Poss</ta>
            <ta e="T50" id="Seg_5565" s="T49">np:Th</ta>
            <ta e="T51" id="Seg_5566" s="T50">pro.h:A</ta>
            <ta e="T53" id="Seg_5567" s="T52">pro.h:B</ta>
            <ta e="T54" id="Seg_5568" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_5569" s="T54">np:Th</ta>
            <ta e="T56" id="Seg_5570" s="T55">pro.h:B</ta>
            <ta e="T57" id="Seg_5571" s="T56">np:Th</ta>
            <ta e="T58" id="Seg_5572" s="T57">0.3.h:A</ta>
            <ta e="T62" id="Seg_5573" s="T61">np:Th</ta>
            <ta e="T63" id="Seg_5574" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_5575" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_5576" s="T64">0.3.h:A</ta>
            <ta e="T67" id="Seg_5577" s="T66">0.3.h:E</ta>
            <ta e="T68" id="Seg_5578" s="T67">adv:Time</ta>
            <ta e="T69" id="Seg_5579" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_5580" s="T69">np:G</ta>
            <ta e="T74" id="Seg_5581" s="T73">np.h:A</ta>
            <ta e="T76" id="Seg_5582" s="T75">pro.h:B</ta>
            <ta e="T77" id="Seg_5583" s="T76">pro:Th</ta>
            <ta e="T79" id="Seg_5584" s="T78">0.3.h:A</ta>
            <ta e="T81" id="Seg_5585" s="T80">pro.h:B</ta>
            <ta e="T82" id="Seg_5586" s="T81">0.3.h:A</ta>
            <ta e="T87" id="Seg_5587" s="T86">0.3.h:A</ta>
            <ta e="T91" id="Seg_5588" s="T90">0.3.h:A</ta>
            <ta e="T92" id="Seg_5589" s="T91">adv:Time</ta>
            <ta e="T93" id="Seg_5590" s="T92">pro.h:A</ta>
            <ta e="T96" id="Seg_5591" s="T95">np:Th</ta>
            <ta e="T97" id="Seg_5592" s="T96">pro:Th</ta>
            <ta e="T99" id="Seg_5593" s="T98">adv:Time</ta>
            <ta e="T100" id="Seg_5594" s="T99">pro.h:A</ta>
            <ta e="T102" id="Seg_5595" s="T101">np:Th</ta>
            <ta e="T103" id="Seg_5596" s="T102">0.2.h:A</ta>
            <ta e="T105" id="Seg_5597" s="T104">pro.h:A</ta>
            <ta e="T107" id="Seg_5598" s="T106">pro.h:B</ta>
            <ta e="T108" id="Seg_5599" s="T107">0.3.h:A</ta>
            <ta e="T111" id="Seg_5600" s="T110">np.h:A</ta>
            <ta e="T113" id="Seg_5601" s="T112">pro.h:B</ta>
            <ta e="T114" id="Seg_5602" s="T113">0.2.h:A</ta>
            <ta e="T115" id="Seg_5603" s="T114">np:Poss</ta>
            <ta e="T116" id="Seg_5604" s="T115">np:Th</ta>
            <ta e="T117" id="Seg_5605" s="T116">pro.h:A</ta>
            <ta e="T119" id="Seg_5606" s="T118">pro.h:B</ta>
            <ta e="T120" id="Seg_5607" s="T119">0.3.h:A</ta>
            <ta e="T122" id="Seg_5608" s="T121">pro:Th</ta>
            <ta e="T124" id="Seg_5609" s="T123">0.3.h:E</ta>
            <ta e="T125" id="Seg_5610" s="T124">adv:Time</ta>
            <ta e="T127" id="Seg_5611" s="T126">0.3.h:A</ta>
            <ta e="T128" id="Seg_5612" s="T127">adv:Time</ta>
            <ta e="T129" id="Seg_5613" s="T128">pro.h:B</ta>
            <ta e="T130" id="Seg_5614" s="T129">0.3.h:A</ta>
            <ta e="T131" id="Seg_5615" s="T130">np:Th</ta>
            <ta e="T134" id="Seg_5616" s="T133">pro.h:B</ta>
            <ta e="T135" id="Seg_5617" s="T134">0.3.h:A</ta>
            <ta e="T137" id="Seg_5618" s="T136">np:Th</ta>
            <ta e="T139" id="Seg_5619" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_5620" s="T139">adv:Time</ta>
            <ta e="T141" id="Seg_5621" s="T140">pro.h:E</ta>
            <ta e="T145" id="Seg_5622" s="T144">0.3.h:A</ta>
            <ta e="T147" id="Seg_5623" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_5624" s="T148">np:L</ta>
            <ta e="T150" id="Seg_5625" s="T149">np:Ins</ta>
            <ta e="T151" id="Seg_5626" s="T150">0.3.h:A</ta>
            <ta e="T152" id="Seg_5627" s="T151">pro:Th</ta>
            <ta e="T155" id="Seg_5628" s="T154">0.3:P</ta>
            <ta e="T157" id="Seg_5629" s="T156">0.3.h:A</ta>
            <ta e="T159" id="Seg_5630" s="T158">pro.h:Poss</ta>
            <ta e="T160" id="Seg_5631" s="T159">np.h:A</ta>
            <ta e="T163" id="Seg_5632" s="T162">adv:L</ta>
            <ta e="T164" id="Seg_5633" s="T163">np.h:Th</ta>
            <ta e="T165" id="Seg_5634" s="T164">0.3:Th</ta>
            <ta e="T166" id="Seg_5635" s="T165">adv:Time</ta>
            <ta e="T169" id="Seg_5636" s="T168">adv:L</ta>
            <ta e="T170" id="Seg_5637" s="T169">np:Th</ta>
            <ta e="T173" id="Seg_5638" s="T172">adv:L</ta>
            <ta e="T175" id="Seg_5639" s="T174">adv:L</ta>
            <ta e="T176" id="Seg_5640" s="T175">np:Th</ta>
            <ta e="T178" id="Seg_5641" s="T177">0.3.h:A</ta>
            <ta e="T180" id="Seg_5642" s="T179">pro.h:R</ta>
            <ta e="T182" id="Seg_5643" s="T181">0.3.h:A</ta>
            <ta e="T184" id="Seg_5644" s="T183">0.3.h:E</ta>
            <ta e="T187" id="Seg_5645" s="T186">pro.h:A</ta>
            <ta e="T189" id="Seg_5646" s="T188">0.3.h:A</ta>
            <ta e="T190" id="Seg_5647" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_5648" s="T190">np:P</ta>
            <ta e="T192" id="Seg_5649" s="T191">0.3.h:E</ta>
            <ta e="T194" id="Seg_5650" s="T453">0.3.h:A</ta>
            <ta e="T196" id="Seg_5651" s="T195">pro.h:Poss</ta>
            <ta e="T197" id="Seg_5652" s="T196">np.h:A</ta>
            <ta e="T198" id="Seg_5653" s="T197">pro.h:R</ta>
            <ta e="T200" id="Seg_5654" s="T199">np:Th</ta>
            <ta e="T202" id="Seg_5655" s="T201">pro.h:E</ta>
            <ta e="T204" id="Seg_5656" s="T203">pro.h:E</ta>
            <ta e="T208" id="Seg_5657" s="T207">pro.h:A</ta>
            <ta e="T210" id="Seg_5658" s="T209">np:L</ta>
            <ta e="T211" id="Seg_5659" s="T210">pro.h:A</ta>
            <ta e="T212" id="Seg_5660" s="T211">adv:L</ta>
            <ta e="T214" id="Seg_5661" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_5662" s="T214">pro.h:A</ta>
            <ta e="T219" id="Seg_5663" s="T218">np:P</ta>
            <ta e="T221" id="Seg_5664" s="T220">0.3.h:E</ta>
            <ta e="T222" id="Seg_5665" s="T221">np:Th</ta>
            <ta e="T224" id="Seg_5666" s="T223">adv:Time</ta>
            <ta e="T225" id="Seg_5667" s="T224">np:P</ta>
            <ta e="T226" id="Seg_5668" s="T225">0.3.h:A</ta>
            <ta e="T229" id="Seg_5669" s="T228">0.3.h:A</ta>
            <ta e="T232" id="Seg_5670" s="T231">np:P</ta>
            <ta e="T233" id="Seg_5671" s="T232">0.2.h:A</ta>
            <ta e="T234" id="Seg_5672" s="T233">0.2.h:A</ta>
            <ta e="T235" id="Seg_5673" s="T234">adv:Time</ta>
            <ta e="T236" id="Seg_5674" s="T235">pro.h:Th</ta>
            <ta e="T237" id="Seg_5675" s="T236">0.2.h:A</ta>
            <ta e="T239" id="Seg_5676" s="T238">adv:Time</ta>
            <ta e="T240" id="Seg_5677" s="T239">pro.h:A</ta>
            <ta e="T242" id="Seg_5678" s="T241">0.3.h:E</ta>
            <ta e="T244" id="Seg_5679" s="T243">np:Th</ta>
            <ta e="T245" id="Seg_5680" s="T244">adv:L</ta>
            <ta e="T247" id="Seg_5681" s="T246">np:Th</ta>
            <ta e="T250" id="Seg_5682" s="T249">np:Th</ta>
            <ta e="T251" id="Seg_5683" s="T250">0.3.h:A</ta>
            <ta e="T253" id="Seg_5684" s="T252">0.3.h:E</ta>
            <ta e="T254" id="Seg_5685" s="T253">0.3.h:E</ta>
            <ta e="T256" id="Seg_5686" s="T255">0.3.h:A</ta>
            <ta e="T257" id="Seg_5687" s="T256">np:G</ta>
            <ta e="T258" id="Seg_5688" s="T257">np:P</ta>
            <ta e="T259" id="Seg_5689" s="T258">0.3.h:A</ta>
            <ta e="T261" id="Seg_5690" s="T260">np:P</ta>
            <ta e="T262" id="Seg_5691" s="T261">0.3.h:A</ta>
            <ta e="T264" id="Seg_5692" s="T263">adv:Time</ta>
            <ta e="T265" id="Seg_5693" s="T264">0.3.h:A</ta>
            <ta e="T266" id="Seg_5694" s="T265">0.3.h:A</ta>
            <ta e="T267" id="Seg_5695" s="T266">0.3.h:A</ta>
            <ta e="T270" id="Seg_5696" s="T269">0.3.h:A</ta>
            <ta e="T271" id="Seg_5697" s="T270">adv:Time</ta>
            <ta e="T273" id="Seg_5698" s="T272">0.3.h:E</ta>
            <ta e="T274" id="Seg_5699" s="T273">np:Th</ta>
            <ta e="T278" id="Seg_5700" s="T277">pro.h:A</ta>
            <ta e="T279" id="Seg_5701" s="T278">np:G</ta>
            <ta e="T281" id="Seg_5702" s="T280">adv:L</ta>
            <ta e="T282" id="Seg_5703" s="T281">np.h:E</ta>
            <ta e="T284" id="Seg_5704" s="T283">pro.h:A</ta>
            <ta e="T288" id="Seg_5705" s="T287">0.2.h:A</ta>
            <ta e="T290" id="Seg_5706" s="T289">pro.h:Poss</ta>
            <ta e="T291" id="Seg_5707" s="T290">np.h:A</ta>
            <ta e="T293" id="Seg_5708" s="T292">pro.h:A</ta>
            <ta e="T295" id="Seg_5709" s="T294">pro.h:E</ta>
            <ta e="T301" id="Seg_5710" s="T300">0.2.h:A</ta>
            <ta e="T302" id="Seg_5711" s="T301">pro.h:Poss</ta>
            <ta e="T303" id="Seg_5712" s="T302">np:G</ta>
            <ta e="T304" id="Seg_5713" s="T303">pro.h:E</ta>
            <ta e="T306" id="Seg_5714" s="T305">0.3.h:A</ta>
            <ta e="T307" id="Seg_5715" s="T306">pro.h:R</ta>
            <ta e="T308" id="Seg_5716" s="T307">adv:Time</ta>
            <ta e="T309" id="Seg_5717" s="T308">pro.h:R</ta>
            <ta e="T310" id="Seg_5718" s="T309">0.3.h:A</ta>
            <ta e="T311" id="Seg_5719" s="T310">np:Th</ta>
            <ta e="T313" id="Seg_5720" s="T312">np:Th</ta>
            <ta e="T316" id="Seg_5721" s="T315">np:Th</ta>
            <ta e="T318" id="Seg_5722" s="T317">0.2.h:A</ta>
            <ta e="T319" id="Seg_5723" s="T318">pro:Th</ta>
            <ta e="T322" id="Seg_5724" s="T321">pro.h:A</ta>
            <ta e="T324" id="Seg_5725" s="T323">adv:Time</ta>
            <ta e="T325" id="Seg_5726" s="T324">pro.h:A</ta>
            <ta e="T328" id="Seg_5727" s="T327">adv:Time</ta>
            <ta e="T329" id="Seg_5728" s="T328">pro.h:A</ta>
            <ta e="T331" id="Seg_5729" s="T330">0.3.h:A</ta>
            <ta e="T333" id="Seg_5730" s="T332">np:G</ta>
            <ta e="T334" id="Seg_5731" s="T333">0.3.h:A</ta>
            <ta e="T341" id="Seg_5732" s="T340">np:Th 0.3.h:Poss</ta>
            <ta e="T342" id="Seg_5733" s="T341">0.3.h:A</ta>
            <ta e="T343" id="Seg_5734" s="T342">np:G</ta>
            <ta e="T346" id="Seg_5735" s="T345">adv:L</ta>
            <ta e="T347" id="Seg_5736" s="T346">np.h:A</ta>
            <ta e="T349" id="Seg_5737" s="T348">0.3.h:A</ta>
            <ta e="T352" id="Seg_5738" s="T351">0.2.h:A</ta>
            <ta e="T353" id="Seg_5739" s="T352">pro.h:R</ta>
            <ta e="T355" id="Seg_5740" s="T354">pro.h:Poss</ta>
            <ta e="T356" id="Seg_5741" s="T355">np.h:A</ta>
            <ta e="T358" id="Seg_5742" s="T357">pro.h:E</ta>
            <ta e="T361" id="Seg_5743" s="T360">0.2.h:A</ta>
            <ta e="T362" id="Seg_5744" s="T361">pro.h:Poss</ta>
            <ta e="T363" id="Seg_5745" s="T362">np:G</ta>
            <ta e="T364" id="Seg_5746" s="T363">pro.h:A</ta>
            <ta e="T367" id="Seg_5747" s="T366">pro.h:Th</ta>
            <ta e="T368" id="Seg_5748" s="T367">pro.h:A</ta>
            <ta e="T369" id="Seg_5749" s="T368">pro.h:R</ta>
            <ta e="T372" id="Seg_5750" s="T371">np:Th</ta>
            <ta e="T374" id="Seg_5751" s="T373">0.3.h:A</ta>
            <ta e="T375" id="Seg_5752" s="T374">np:Th</ta>
            <ta e="T376" id="Seg_5753" s="T375">np:Th</ta>
            <ta e="T380" id="Seg_5754" s="T379">np:Th</ta>
            <ta e="T382" id="Seg_5755" s="T381">adv:Time</ta>
            <ta e="T384" id="Seg_5756" s="T383">0.3.h:A</ta>
            <ta e="T385" id="Seg_5757" s="T384">np:Th</ta>
            <ta e="T386" id="Seg_5758" s="T385">pro:Th</ta>
            <ta e="T389" id="Seg_5759" s="T388">pro.h:A</ta>
            <ta e="T392" id="Seg_5760" s="T391">adv:L</ta>
            <ta e="T394" id="Seg_5761" s="T393">pro:Th</ta>
            <ta e="T396" id="Seg_5762" s="T395">adv:Time</ta>
            <ta e="T397" id="Seg_5763" s="T396">pro.h:A</ta>
            <ta e="T399" id="Seg_5764" s="T398">0.3.h:A</ta>
            <ta e="T401" id="Seg_5765" s="T400">np:G</ta>
            <ta e="T402" id="Seg_5766" s="T401">0.3.h:A</ta>
            <ta e="T404" id="Seg_5767" s="T403">np:Th</ta>
            <ta e="T406" id="Seg_5768" s="T405">pro.h:A</ta>
            <ta e="T408" id="Seg_5769" s="T407">adv:L</ta>
            <ta e="T409" id="Seg_5770" s="T408">np.h:E</ta>
            <ta e="T412" id="Seg_5771" s="T411">adv:Time</ta>
            <ta e="T413" id="Seg_5772" s="T412">0.3.h:A</ta>
            <ta e="T416" id="Seg_5773" s="T415">0.2.h:A</ta>
            <ta e="T417" id="Seg_5774" s="T416">pro.h:R</ta>
            <ta e="T419" id="Seg_5775" s="T418">pro.h:Poss</ta>
            <ta e="T420" id="Seg_5776" s="T419">np.h:A</ta>
            <ta e="T422" id="Seg_5777" s="T421">pro.h:A</ta>
            <ta e="T424" id="Seg_5778" s="T423">0.2.h:A</ta>
            <ta e="T425" id="Seg_5779" s="T424">adv:Time</ta>
            <ta e="T426" id="Seg_5780" s="T425">pro.h:A</ta>
            <ta e="T428" id="Seg_5781" s="T427">pro.h:R</ta>
            <ta e="T429" id="Seg_5782" s="T428">np:Th</ta>
            <ta e="T434" id="Seg_5783" s="T433">np:Th</ta>
            <ta e="T435" id="Seg_5784" s="T434">0.3.h:A</ta>
            <ta e="T437" id="Seg_5785" s="T436">np:G</ta>
            <ta e="T438" id="Seg_5786" s="T437">0.2.h:A</ta>
            <ta e="T439" id="Seg_5787" s="T438">pro:L</ta>
            <ta e="T440" id="Seg_5788" s="T439">0.3.h:E</ta>
            <ta e="T441" id="Seg_5789" s="T440">pro.h:R</ta>
            <ta e="T442" id="Seg_5790" s="T441">0.3.h:A</ta>
            <ta e="T445" id="Seg_5791" s="T444">np:Th</ta>
            <ta e="T446" id="Seg_5792" s="T445">pro:G</ta>
            <ta e="T447" id="Seg_5793" s="T446">pro:Th</ta>
            <ta e="T449" id="Seg_5794" s="T448">pro.h:A</ta>
            <ta e="T450" id="Seg_5795" s="T449">adv:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_5796" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_5797" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_5798" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_5799" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_5800" s="T7">pro.h:S</ta>
            <ta e="T10" id="Seg_5801" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_5802" s="T10">np:O</ta>
            <ta e="T12" id="Seg_5803" s="T11">s:purp</ta>
            <ta e="T13" id="Seg_5804" s="T12">pro:O</ta>
            <ta e="T15" id="Seg_5805" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_5806" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_5807" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_5808" s="T18">v:pred 0.2.h:S</ta>
            <ta e="T21" id="Seg_5809" s="T20">np:O</ta>
            <ta e="T22" id="Seg_5810" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_5811" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_5812" s="T25">np:O</ta>
            <ta e="T28" id="Seg_5813" s="T27">v:pred 0.2.h:S</ta>
            <ta e="T31" id="Seg_5814" s="T30">np:O</ta>
            <ta e="T32" id="Seg_5815" s="T31">v:pred 0.2.h:S</ta>
            <ta e="T34" id="Seg_5816" s="T33">np.h:S</ta>
            <ta e="T38" id="Seg_5817" s="T37">pro:O</ta>
            <ta e="T39" id="Seg_5818" s="T38">v:pred</ta>
            <ta e="T41" id="Seg_5819" s="T40">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_5820" s="T44">pro.h:S</ta>
            <ta e="T46" id="Seg_5821" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_5822" s="T46">v:pred 0.2.h:S</ta>
            <ta e="T51" id="Seg_5823" s="T50">pro.h:S</ta>
            <ta e="T52" id="Seg_5824" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_5825" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T55" id="Seg_5826" s="T54">np:O</ta>
            <ta e="T57" id="Seg_5827" s="T56">np:O</ta>
            <ta e="T58" id="Seg_5828" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T62" id="Seg_5829" s="T61">np:O</ta>
            <ta e="T63" id="Seg_5830" s="T62">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_5831" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_5832" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T66" id="Seg_5833" s="T65">ptcl.neg</ta>
            <ta e="T67" id="Seg_5834" s="T66">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_5835" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_5836" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_5837" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_5838" s="T76">pro:O</ta>
            <ta e="T78" id="Seg_5839" s="T77">ptcl.neg</ta>
            <ta e="T79" id="Seg_5840" s="T78">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_5841" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T86" id="Seg_5842" s="T85">ptcl.neg</ta>
            <ta e="T87" id="Seg_5843" s="T86">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_5844" s="T89">ptcl.neg</ta>
            <ta e="T91" id="Seg_5845" s="T90">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_5846" s="T92">pro.h:S</ta>
            <ta e="T95" id="Seg_5847" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_5848" s="T96">pro:O</ta>
            <ta e="T98" id="Seg_5849" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_5850" s="T99">pro.h:S</ta>
            <ta e="T101" id="Seg_5851" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_5852" s="T101">np:O</ta>
            <ta e="T103" id="Seg_5853" s="T102">v:pred 0.2.h:S</ta>
            <ta e="T105" id="Seg_5854" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_5855" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_5856" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_5857" s="T110">np.h:S</ta>
            <ta e="T112" id="Seg_5858" s="T111">v:pred</ta>
            <ta e="T114" id="Seg_5859" s="T113">v:pred 0.2.h:S</ta>
            <ta e="T116" id="Seg_5860" s="T115">np:O</ta>
            <ta e="T117" id="Seg_5861" s="T116">pro.h:S</ta>
            <ta e="T118" id="Seg_5862" s="T117">v:pred</ta>
            <ta e="T120" id="Seg_5863" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T122" id="Seg_5864" s="T121">pro:O</ta>
            <ta e="T123" id="Seg_5865" s="T122">ptcl.neg</ta>
            <ta e="T124" id="Seg_5866" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T127" id="Seg_5867" s="T126">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_5868" s="T129">v:pred 0.3.h:S</ta>
            <ta e="T131" id="Seg_5869" s="T130">np:O</ta>
            <ta e="T135" id="Seg_5870" s="T134">v:pred 0.3.h:S</ta>
            <ta e="T137" id="Seg_5871" s="T136">np:O</ta>
            <ta e="T139" id="Seg_5872" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_5873" s="T140">pro.h:S</ta>
            <ta e="T144" id="Seg_5874" s="T143">v:pred</ta>
            <ta e="T145" id="Seg_5875" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_5876" s="T146">np:O</ta>
            <ta e="T148" id="Seg_5877" s="T147">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_5878" s="T150">v:pred 0.3.h:S</ta>
            <ta e="T152" id="Seg_5879" s="T151">pro:O</ta>
            <ta e="T154" id="Seg_5880" s="T153">n:pred</ta>
            <ta e="T155" id="Seg_5881" s="T154">cop 0.3:S</ta>
            <ta e="T157" id="Seg_5882" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_5883" s="T159">np.h:S</ta>
            <ta e="T161" id="Seg_5884" s="T160">v:pred</ta>
            <ta e="T164" id="Seg_5885" s="T163">n:pred</ta>
            <ta e="T165" id="Seg_5886" s="T164">cop 0.3:S</ta>
            <ta e="T170" id="Seg_5887" s="T169">np:S</ta>
            <ta e="T171" id="Seg_5888" s="T170">v:pred</ta>
            <ta e="T176" id="Seg_5889" s="T175">np:O</ta>
            <ta e="T178" id="Seg_5890" s="T177">v:pred 0.3.h:S</ta>
            <ta e="T180" id="Seg_5891" s="T179">pro.h:O</ta>
            <ta e="T182" id="Seg_5892" s="T181">v:pred 0.3.h:S</ta>
            <ta e="T186" id="Seg_5893" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T187" id="Seg_5894" s="T186">pro.h:S</ta>
            <ta e="T188" id="Seg_5895" s="T187">v:pred</ta>
            <ta e="T189" id="Seg_5896" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_5897" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_5898" s="T190">np:O</ta>
            <ta e="T192" id="Seg_5899" s="T191">v:pred 0.3.h:S</ta>
            <ta e="T453" id="Seg_5900" s="T193">conv:pred</ta>
            <ta e="T194" id="Seg_5901" s="T453">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_5902" s="T196">np.h:S</ta>
            <ta e="T199" id="Seg_5903" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_5904" s="T199">np:O</ta>
            <ta e="T202" id="Seg_5905" s="T201">pro.h:S</ta>
            <ta e="T203" id="Seg_5906" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_5907" s="T203">pro.h:S</ta>
            <ta e="T206" id="Seg_5908" s="T205">v:pred</ta>
            <ta e="T208" id="Seg_5909" s="T207">pro.h:S</ta>
            <ta e="T209" id="Seg_5910" s="T208">v:pred</ta>
            <ta e="T211" id="Seg_5911" s="T210">pro.h:S</ta>
            <ta e="T213" id="Seg_5912" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_5913" s="T213">np:O</ta>
            <ta e="T218" id="Seg_5914" s="T217">ptcl:pred</ta>
            <ta e="T219" id="Seg_5915" s="T218">np:O</ta>
            <ta e="T221" id="Seg_5916" s="T220">v:pred 0.3.h:S</ta>
            <ta e="T222" id="Seg_5917" s="T221">np:S</ta>
            <ta e="T223" id="Seg_5918" s="T222">v:pred</ta>
            <ta e="T225" id="Seg_5919" s="T224">np:O</ta>
            <ta e="T226" id="Seg_5920" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T229" id="Seg_5921" s="T228">v:pred 0.3.h:S</ta>
            <ta e="T232" id="Seg_5922" s="T231">np:O</ta>
            <ta e="T233" id="Seg_5923" s="T232">v:pred 0.2.h:S</ta>
            <ta e="T234" id="Seg_5924" s="T233">v:pred 0.2.h:S</ta>
            <ta e="T236" id="Seg_5925" s="T235">pro.h:O</ta>
            <ta e="T237" id="Seg_5926" s="T236">v:pred 0.2.h:S</ta>
            <ta e="T240" id="Seg_5927" s="T239">pro.h:S</ta>
            <ta e="T241" id="Seg_5928" s="T240">v:pred</ta>
            <ta e="T242" id="Seg_5929" s="T241">v:pred 0.3.h:S</ta>
            <ta e="T244" id="Seg_5930" s="T243">np:S</ta>
            <ta e="T245" id="Seg_5931" s="T244">adj:pred</ta>
            <ta e="T247" id="Seg_5932" s="T246">np:S</ta>
            <ta e="T248" id="Seg_5933" s="T247">v:pred</ta>
            <ta e="T250" id="Seg_5934" s="T249">np:O</ta>
            <ta e="T251" id="Seg_5935" s="T250">v:pred 0.3.h:S</ta>
            <ta e="T253" id="Seg_5936" s="T252">v:pred 0.3.h:S</ta>
            <ta e="T254" id="Seg_5937" s="T253">v:pred 0.3.h:S</ta>
            <ta e="T256" id="Seg_5938" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T258" id="Seg_5939" s="T257">np:O</ta>
            <ta e="T259" id="Seg_5940" s="T258">v:pred 0.3.h:S</ta>
            <ta e="T261" id="Seg_5941" s="T260">np:O</ta>
            <ta e="T262" id="Seg_5942" s="T261">v:pred 0.3.h:S</ta>
            <ta e="T265" id="Seg_5943" s="T264">v:pred 0.3.h:S</ta>
            <ta e="T266" id="Seg_5944" s="T265">v:pred 0.3.h:S</ta>
            <ta e="T267" id="Seg_5945" s="T266">v:pred 0.3.h:S</ta>
            <ta e="T270" id="Seg_5946" s="T269">v:pred 0.3.h:S</ta>
            <ta e="T273" id="Seg_5947" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_5948" s="T273">np:S</ta>
            <ta e="T276" id="Seg_5949" s="T275">v:pred</ta>
            <ta e="T278" id="Seg_5950" s="T277">pro.h:S</ta>
            <ta e="T280" id="Seg_5951" s="T279">v:pred</ta>
            <ta e="T282" id="Seg_5952" s="T281">np.h:S</ta>
            <ta e="T283" id="Seg_5953" s="T282">v:pred</ta>
            <ta e="T284" id="Seg_5954" s="T283">pro.h:S</ta>
            <ta e="T285" id="Seg_5955" s="T284">v:pred</ta>
            <ta e="T288" id="Seg_5956" s="T287">v:pred 0.2.h:S</ta>
            <ta e="T291" id="Seg_5957" s="T290">np.h:S</ta>
            <ta e="T292" id="Seg_5958" s="T454">v:pred</ta>
            <ta e="T293" id="Seg_5959" s="T292">pro.h:S</ta>
            <ta e="T294" id="Seg_5960" s="T293">v:pred</ta>
            <ta e="T295" id="Seg_5961" s="T294">pro.h:S</ta>
            <ta e="T296" id="Seg_5962" s="T295">ptcl.neg</ta>
            <ta e="T297" id="Seg_5963" s="T296">v:pred</ta>
            <ta e="T301" id="Seg_5964" s="T300">v:pred 0.2.h:S</ta>
            <ta e="T304" id="Seg_5965" s="T303">pro.h:S</ta>
            <ta e="T305" id="Seg_5966" s="T304">v:pred</ta>
            <ta e="T306" id="Seg_5967" s="T305">v:pred 0.3.h:S</ta>
            <ta e="T310" id="Seg_5968" s="T309">v:pred 0.3.h:S</ta>
            <ta e="T311" id="Seg_5969" s="T310">np:O</ta>
            <ta e="T313" id="Seg_5970" s="T312">np:O</ta>
            <ta e="T316" id="Seg_5971" s="T315">np:O</ta>
            <ta e="T318" id="Seg_5972" s="T317">v:pred 0.2.h:S</ta>
            <ta e="T319" id="Seg_5973" s="T318">pro:S</ta>
            <ta e="T320" id="Seg_5974" s="T319">v:pred</ta>
            <ta e="T322" id="Seg_5975" s="T321">pro.h:S</ta>
            <ta e="T323" id="Seg_5976" s="T322">v:pred</ta>
            <ta e="T325" id="Seg_5977" s="T324">pro.h:S</ta>
            <ta e="T326" id="Seg_5978" s="T325">v:pred</ta>
            <ta e="T329" id="Seg_5979" s="T328">pro.h:S</ta>
            <ta e="T330" id="Seg_5980" s="T329">v:pred</ta>
            <ta e="T331" id="Seg_5981" s="T330">v:pred 0.3.h:S</ta>
            <ta e="T334" id="Seg_5982" s="T333">v:pred 0.3.h:S</ta>
            <ta e="T340" id="Seg_5983" s="T339">adj:pred</ta>
            <ta e="T341" id="Seg_5984" s="T340">np:S</ta>
            <ta e="T342" id="Seg_5985" s="T341">v:pred 0.3.h:S</ta>
            <ta e="T347" id="Seg_5986" s="T346">np.h:S</ta>
            <ta e="T348" id="Seg_5987" s="T347">v:pred</ta>
            <ta e="T349" id="Seg_5988" s="T348">v:pred 0.3.h:S</ta>
            <ta e="T352" id="Seg_5989" s="T351">v:pred 0.2.h:S</ta>
            <ta e="T356" id="Seg_5990" s="T355">np.h:S</ta>
            <ta e="T455" id="Seg_5991" s="T356">conv:pred</ta>
            <ta e="T357" id="Seg_5992" s="T455">v:pred</ta>
            <ta e="T358" id="Seg_5993" s="T357">pro.h:S</ta>
            <ta e="T359" id="Seg_5994" s="T358">ptcl.neg</ta>
            <ta e="T360" id="Seg_5995" s="T359">v:pred</ta>
            <ta e="T361" id="Seg_5996" s="T360">v:pred 0.2.h:S</ta>
            <ta e="T364" id="Seg_5997" s="T363">pro.h:S</ta>
            <ta e="T365" id="Seg_5998" s="T364">v:pred</ta>
            <ta e="T366" id="Seg_5999" s="T365">n:pred</ta>
            <ta e="T367" id="Seg_6000" s="T366">pro.h:S</ta>
            <ta e="T368" id="Seg_6001" s="T367">pro.h:S</ta>
            <ta e="T371" id="Seg_6002" s="T370">v:pred</ta>
            <ta e="T372" id="Seg_6003" s="T371">np:O</ta>
            <ta e="T374" id="Seg_6004" s="T373">v:pred 0.3.h:S</ta>
            <ta e="T375" id="Seg_6005" s="T374">np:O</ta>
            <ta e="T376" id="Seg_6006" s="T375">np:S</ta>
            <ta e="T378" id="Seg_6007" s="T377">adj:pred</ta>
            <ta e="T380" id="Seg_6008" s="T379">np:S</ta>
            <ta e="T381" id="Seg_6009" s="T380">adj:pred</ta>
            <ta e="T384" id="Seg_6010" s="T383">v:pred 0.3.h:S</ta>
            <ta e="T385" id="Seg_6011" s="T384">np:O</ta>
            <ta e="T386" id="Seg_6012" s="T385">pro:S</ta>
            <ta e="T388" id="Seg_6013" s="T387">v:pred</ta>
            <ta e="T389" id="Seg_6014" s="T388">pro.h:S</ta>
            <ta e="T391" id="Seg_6015" s="T390">v:pred</ta>
            <ta e="T394" id="Seg_6016" s="T393">pro:S</ta>
            <ta e="T395" id="Seg_6017" s="T394">v:pred</ta>
            <ta e="T397" id="Seg_6018" s="T396">pro.h:S</ta>
            <ta e="T398" id="Seg_6019" s="T397">v:pred</ta>
            <ta e="T399" id="Seg_6020" s="T398">v:pred 0.3.h:S</ta>
            <ta e="T402" id="Seg_6021" s="T401">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_6022" s="T403">np:S</ta>
            <ta e="T405" id="Seg_6023" s="T404">v:pred</ta>
            <ta e="T406" id="Seg_6024" s="T405">pro.h:S</ta>
            <ta e="T407" id="Seg_6025" s="T406">v:pred</ta>
            <ta e="T409" id="Seg_6026" s="T408">np.h:S</ta>
            <ta e="T410" id="Seg_6027" s="T409">v:pred</ta>
            <ta e="T413" id="Seg_6028" s="T412">v:pred 0.3.h:S</ta>
            <ta e="T416" id="Seg_6029" s="T415">v:pred 0.2.h:S</ta>
            <ta e="T420" id="Seg_6030" s="T419">np.h:S</ta>
            <ta e="T456" id="Seg_6031" s="T420">conv:pred</ta>
            <ta e="T421" id="Seg_6032" s="T456">v:pred</ta>
            <ta e="T422" id="Seg_6033" s="T421">pro.h:S</ta>
            <ta e="T423" id="Seg_6034" s="T422">v:pred</ta>
            <ta e="T424" id="Seg_6035" s="T423">v:pred 0.2.h:S</ta>
            <ta e="T426" id="Seg_6036" s="T425">pro.h:S</ta>
            <ta e="T427" id="Seg_6037" s="T426">v:pred</ta>
            <ta e="T429" id="Seg_6038" s="T428">np:O</ta>
            <ta e="T434" id="Seg_6039" s="T433">np:O</ta>
            <ta e="T435" id="Seg_6040" s="T434">v:pred 0.3.h:S</ta>
            <ta e="T438" id="Seg_6041" s="T437">v:pred 0.2.h:S</ta>
            <ta e="T440" id="Seg_6042" s="T439">v:pred 0.3.h:S</ta>
            <ta e="T442" id="Seg_6043" s="T441">v:pred 0.3.h:S</ta>
            <ta e="T445" id="Seg_6044" s="T444">np:O</ta>
            <ta e="T447" id="Seg_6045" s="T446">pro:S</ta>
            <ta e="T448" id="Seg_6046" s="T447">v:pred</ta>
            <ta e="T449" id="Seg_6047" s="T448">pro.h:S</ta>
            <ta e="T451" id="Seg_6048" s="T450">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_6049" s="T11">TURK:cult</ta>
            <ta e="T21" id="Seg_6050" s="T20">RUS:cult</ta>
            <ta e="T26" id="Seg_6051" s="T25">RUS:cult</ta>
            <ta e="T29" id="Seg_6052" s="T28">RUS:gram</ta>
            <ta e="T31" id="Seg_6053" s="T30">RUS:cult</ta>
            <ta e="T33" id="Seg_6054" s="T32">RUS:gram</ta>
            <ta e="T36" id="Seg_6055" s="T35">RUS:gram</ta>
            <ta e="T42" id="Seg_6056" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_6057" s="T42">RUS:core</ta>
            <ta e="T49" id="Seg_6058" s="T48">RUS:cult</ta>
            <ta e="T50" id="Seg_6059" s="T49">RUS:core</ta>
            <ta e="T55" id="Seg_6060" s="T54">RUS:cult</ta>
            <ta e="T57" id="Seg_6061" s="T56">RUS:cult</ta>
            <ta e="T59" id="Seg_6062" s="T58">RUS:gram</ta>
            <ta e="T61" id="Seg_6063" s="T60">RUS:gram</ta>
            <ta e="T62" id="Seg_6064" s="T61">RUS:cult</ta>
            <ta e="T77" id="Seg_6065" s="T76">TURK:gram(INDEF)</ta>
            <ta e="T80" id="Seg_6066" s="T79">RUS:gram</ta>
            <ta e="T84" id="Seg_6067" s="T83">RUS:gram</ta>
            <ta e="T85" id="Seg_6068" s="T84">RUS:gram</ta>
            <ta e="T88" id="Seg_6069" s="T87">RUS:gram</ta>
            <ta e="T89" id="Seg_6070" s="T88">RUS:mod</ta>
            <ta e="T102" id="Seg_6071" s="T101">RUS:cult</ta>
            <ta e="T109" id="Seg_6072" s="T108">RUS:gram</ta>
            <ta e="T110" id="Seg_6073" s="T109">RUS:gram</ta>
            <ta e="T115" id="Seg_6074" s="T114">RUS:cult</ta>
            <ta e="T116" id="Seg_6075" s="T115">RUS:core</ta>
            <ta e="T121" id="Seg_6076" s="T120">RUS:gram</ta>
            <ta e="T131" id="Seg_6077" s="T130">TURK:disc</ta>
            <ta e="T133" id="Seg_6078" s="T132">RUS:gram</ta>
            <ta e="T137" id="Seg_6079" s="T136">RUS:core</ta>
            <ta e="T138" id="Seg_6080" s="T137">RUS:gram</ta>
            <ta e="T147" id="Seg_6081" s="T146">RUS:core</ta>
            <ta e="T153" id="Seg_6082" s="T152">TURK:disc</ta>
            <ta e="T156" id="Seg_6083" s="T155">TURK:disc</ta>
            <ta e="T157" id="Seg_6084" s="T156">%TURK:core</ta>
            <ta e="T158" id="Seg_6085" s="T157">RUS:gram</ta>
            <ta e="T160" id="Seg_6086" s="T159">RUS:core</ta>
            <ta e="T162" id="Seg_6087" s="T161">RUS:gram</ta>
            <ta e="T174" id="Seg_6088" s="T173">TURK:disc</ta>
            <ta e="T179" id="Seg_6089" s="T178">RUS:gram</ta>
            <ta e="T185" id="Seg_6090" s="T184">RUS:gram</ta>
            <ta e="T193" id="Seg_6091" s="T192">RUS:gram</ta>
            <ta e="T197" id="Seg_6092" s="T196">RUS:core</ta>
            <ta e="T200" id="Seg_6093" s="T199">RUS:cult</ta>
            <ta e="T201" id="Seg_6094" s="T200">RUS:gram</ta>
            <ta e="T205" id="Seg_6095" s="T204">TURK:disc</ta>
            <ta e="T207" id="Seg_6096" s="T206">RUS:gram</ta>
            <ta e="T216" id="Seg_6097" s="T215">TURK:disc</ta>
            <ta e="T218" id="Seg_6098" s="T217">RUS:mod</ta>
            <ta e="T220" id="Seg_6099" s="T219">TURK:disc</ta>
            <ta e="T227" id="Seg_6100" s="T226">RUS:gram</ta>
            <ta e="T243" id="Seg_6101" s="T242">TURK:disc</ta>
            <ta e="T246" id="Seg_6102" s="T245">RUS:gram</ta>
            <ta e="T252" id="Seg_6103" s="T251">TURK:disc</ta>
            <ta e="T255" id="Seg_6104" s="T254">RUS:gram</ta>
            <ta e="T257" id="Seg_6105" s="T256">RUS:cult</ta>
            <ta e="T274" id="Seg_6106" s="T273">TAT:cult</ta>
            <ta e="T279" id="Seg_6107" s="T278">TAT:cult</ta>
            <ta e="T300" id="Seg_6108" s="T299">TURK:gram(INDEF)</ta>
            <ta e="T303" id="Seg_6109" s="T302">RUS:core</ta>
            <ta e="T312" id="Seg_6110" s="T311">RUS:cult</ta>
            <ta e="T314" id="Seg_6111" s="T313">RUS:cult</ta>
            <ta e="T315" id="Seg_6112" s="T314">RUS:gram</ta>
            <ta e="T316" id="Seg_6113" s="T315">RUS:cult</ta>
            <ta e="T317" id="Seg_6114" s="T316">RUS:cult</ta>
            <ta e="T321" id="Seg_6115" s="T320">RUS:gram</ta>
            <ta e="T332" id="Seg_6116" s="T331">TURK:core</ta>
            <ta e="T333" id="Seg_6117" s="T332">RUS:core</ta>
            <ta e="T335" id="Seg_6118" s="T334">RUS:mod</ta>
            <ta e="T336" id="Seg_6119" s="T335">TAT:cult</ta>
            <ta e="T341" id="Seg_6120" s="T340">TAT:cult</ta>
            <ta e="T343" id="Seg_6121" s="T342">TAT:cult</ta>
            <ta e="T363" id="Seg_6122" s="T362">RUS:core</ta>
            <ta e="T372" id="Seg_6123" s="T371">RUS:cult</ta>
            <ta e="T373" id="Seg_6124" s="T372">RUS:gram</ta>
            <ta e="T375" id="Seg_6125" s="T374">RUS:cult</ta>
            <ta e="T376" id="Seg_6126" s="T375">RUS:cult</ta>
            <ta e="T377" id="Seg_6127" s="T376">TURK:disc</ta>
            <ta e="T378" id="Seg_6128" s="T377">RUS:cult</ta>
            <ta e="T379" id="Seg_6129" s="T378">RUS:gram</ta>
            <ta e="T380" id="Seg_6130" s="T379">RUS:cult</ta>
            <ta e="T381" id="Seg_6131" s="T380">RUS:cult</ta>
            <ta e="T385" id="Seg_6132" s="T384">RUS:cult</ta>
            <ta e="T401" id="Seg_6133" s="T400">RUS:core</ta>
            <ta e="T403" id="Seg_6134" s="T402">RUS:mod</ta>
            <ta e="T404" id="Seg_6135" s="T403">TAT:cult</ta>
            <ta e="T431" id="Seg_6136" s="T430">RUS:cult</ta>
            <ta e="T432" id="Seg_6137" s="T431">TURK:disc</ta>
            <ta e="T434" id="Seg_6138" s="T433">RUS:cult</ta>
            <ta e="T436" id="Seg_6139" s="T435">RUS:cult</ta>
            <ta e="T439" id="Seg_6140" s="T438">TURK:gram(INDEF)</ta>
            <ta e="T443" id="Seg_6141" s="T442">RUS:mod</ta>
            <ta e="T445" id="Seg_6142" s="T444">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T35" id="Seg_6143" s="T34">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_6144" s="T0">Жил один человек.</ta>
            <ta e="T7" id="Seg_6145" s="T3">У него было три дочери.</ta>
            <ta e="T12" id="Seg_6146" s="T7">Он поехал продавать хлеб.</ta>
            <ta e="T15" id="Seg_6147" s="T12">"Что вам купить?"</ta>
            <ta e="T21" id="Seg_6148" s="T15">Ожна говорит: "Купи мне красивое пальто".</ta>
            <ta e="T32" id="Seg_6149" s="T21">Другая говорит: "Купи мне красивую шаль и красивое платье".</ta>
            <ta e="T39" id="Seg_6150" s="T32">У младшей дочери [он спрашивает]: "Что тебе купить?"</ta>
            <ta e="T43" id="Seg_6151" s="T39">"Купи мне перышко Финиста Ясного сокола".</ta>
            <ta e="T50" id="Seg_6152" s="T44">Она говорит: "Купи мне перышко Финиста Ясного сокола".</ta>
            <ta e="T63" id="Seg_6153" s="T50">Он поехал, ей купил пальто, ей купил шаль и платье.</ta>
            <ta e="T67" id="Seg_6154" s="T63">Искал-искал, не нашел.</ta>
            <ta e="T70" id="Seg_6155" s="T67">Он приехал домой.</ta>
            <ta e="T75" id="Seg_6156" s="T70">Те две дочери смеются.</ta>
            <ta e="T82" id="Seg_6157" s="T75">"Тебе он ничего не купил, а нам купил".</ta>
            <ta e="T91" id="Seg_6158" s="T82">"Ну и что, что не купил, и пусть не покупает".</ta>
            <ta e="T96" id="Seg_6159" s="T91">Потом он опять поехал [продавать] хлеб.</ta>
            <ta e="T98" id="Seg_6160" s="T96">"Что купить?"</ta>
            <ta e="T101" id="Seg_6161" s="T98">Тогда они говорят:</ta>
            <ta e="T104" id="Seg_6162" s="T101">"Купи [нам] красивые ботинки".</ta>
            <ta e="T108" id="Seg_6163" s="T104">Он поехал, купил им [ботинки].</ta>
            <ta e="T116" id="Seg_6164" s="T108">А младшая говорит: "Купи мне перо Финиста Ясного сокола".</ta>
            <ta e="T120" id="Seg_6165" s="T116">Он поехал, им купил [ботинки].</ta>
            <ta e="T124" id="Seg_6166" s="T120">А это [=перо] не нашел.</ta>
            <ta e="T127" id="Seg_6167" s="T124">Потом он в третий раз поехал.</ta>
            <ta e="T132" id="Seg_6168" s="T127">Он им все купил.</ta>
            <ta e="T139" id="Seg_6169" s="T132">А для нее нашел это перышко и купил.</ta>
            <ta e="T144" id="Seg_6170" s="T139">Потом он (?) заснул.</ta>
            <ta e="T147" id="Seg_6171" s="T144">[Она] взяла это перышко.</ta>
            <ta e="T149" id="Seg_6172" s="T147">(…) в руку.</ta>
            <ta e="T157" id="Seg_6173" s="T149">Она бросила его (рукой?), оно превратилось в юношу, они разговаривают.</ta>
            <ta e="T165" id="Seg_6174" s="T157">А ее сестры увидели, что там юноша.</ta>
            <ta e="T172" id="Seg_6175" s="T165">Тогда… у нее было маленькое окошко.</ta>
            <ta e="T178" id="Seg_6176" s="T172">Они вставили туда ножи.</ta>
            <ta e="T182" id="Seg_6177" s="T178">А ее напоили.</ta>
            <ta e="T186" id="Seg_6178" s="T182">Такой водой, [от которой] она уснула.</ta>
            <ta e="T188" id="Seg_6179" s="T186">Он прилетел.</ta>
            <ta e="T192" id="Seg_6180" s="T188">Кричал-кричал, он поранил себе ногу. [?]</ta>
            <ta e="T194" id="Seg_6181" s="T192">И улетел.</ta>
            <ta e="T203" id="Seg_6182" s="T195">Ее сестры дали ей капли, чтобы она уснула.</ta>
            <ta e="T206" id="Seg_6183" s="T203">Она уснула.</ta>
            <ta e="T209" id="Seg_6184" s="T206">А он прилетел.</ta>
            <ta e="T214" id="Seg_6185" s="T209">Они воткнули ножи в окно.</ta>
            <ta e="T218" id="Seg_6186" s="T214">Он хотел войти.</ta>
            <ta e="T221" id="Seg_6187" s="T218">[И] порезал ногу.</ta>
            <ta e="T223" id="Seg_6188" s="T221">Кровь течет.</ta>
            <ta e="T226" id="Seg_6189" s="T223">Тогда он написал письмо.</ta>
            <ta e="T237" id="Seg_6190" s="T226">И положил его: "Когда ты железные башмаки сделаешь и их сносишь, тогда ты меня найдешь".</ta>
            <ta e="T241" id="Seg_6191" s="T238">Потом она встала.</ta>
            <ta e="T245" id="Seg_6192" s="T241">Смотрит: там кровь.</ta>
            <ta e="T248" id="Seg_6193" s="T245">И письмо лежит.</ta>
            <ta e="T254" id="Seg_6194" s="T248">Она посмотрела на письмо, плакала-плакала.</ta>
            <ta e="T257" id="Seg_6195" s="T254">И пошла в кузницу.</ta>
            <ta e="T263" id="Seg_6196" s="T257">[] сделал железные башмаки, железный посох. </ta>
            <ta e="T265" id="Seg_6197" s="T263">И пошла.</ta>
            <ta e="T270" id="Seg_6198" s="T265">Шла-шла, долго шла.</ta>
            <ta e="T277" id="Seg_6199" s="T270">Потом видит: стоит маленький дом.</ta>
            <ta e="T280" id="Seg_6200" s="T277">Она подошла к этому дому.</ta>
            <ta e="T283" id="Seg_6201" s="T280">Там женщина живет.</ta>
            <ta e="T292" id="Seg_6202" s="T283">Она спрашивает: "Женщина, скажи мне, куда мой муж ушел?"</ta>
            <ta e="T300" id="Seg_6203" s="T292">Та говорит: "Я не знаю, куда, куда-то".</ta>
            <ta e="T303" id="Seg_6204" s="T300">Иди к моей сестре.</ta>
            <ta e="T305" id="Seg_6205" s="T303">Она знает.</ta>
            <ta e="T307" id="Seg_6206" s="T305">Она тебе скажет".</ta>
            <ta e="T314" id="Seg_6207" s="T307">Она дала ей золотое блюдце, золотое яйцо.</ta>
            <ta e="T317" id="Seg_6208" s="T314">И ниток клубок.</ta>
            <ta e="T323" id="Seg_6209" s="T317">"Иди, он покатится, и ты иди [за ним]".</ta>
            <ta e="T326" id="Seg_6210" s="T323">Она пошла.</ta>
            <ta e="T331" id="Seg_6211" s="T327">Она шла-шла.</ta>
            <ta e="T334" id="Seg_6212" s="T331">Пришла к другой сестре.</ta>
            <ta e="T341" id="Seg_6213" s="T334">У нее тоже маленький домик.</ta>
            <ta e="T343" id="Seg_6214" s="T341">Она вошла в дом.</ta>
            <ta e="T348" id="Seg_6215" s="T343">Там женщина живет.</ta>
            <ta e="T349" id="Seg_6216" s="T348">Она спрашивает.</ta>
            <ta e="T357" id="Seg_6217" s="T349">"Женщина, скажи мне, куда мой муж ушел?"</ta>
            <ta e="T360" id="Seg_6218" s="T357">"Я не знаю.</ta>
            <ta e="T367" id="Seg_6219" s="T360">Иди к моей сестре, она расскажет, где он".</ta>
            <ta e="T372" id="Seg_6220" s="T367">Она дала ей прялку.</ta>
            <ta e="T375" id="Seg_6221" s="T372">И дала кудель.</ta>
            <ta e="T381" id="Seg_6222" s="T375">Прялка золотая, и кугель золотой.</ta>
            <ta e="T385" id="Seg_6223" s="T381">Потом дала клубок.</ta>
            <ta e="T395" id="Seg_6224" s="T385">"Он покатится, и ты иди туда, куда он катится".</ta>
            <ta e="T399" id="Seg_6225" s="T395">Потом она шла, шла.</ta>
            <ta e="T405" id="Seg_6226" s="T399">Пришла к третьей сестре, тоже дом стоит.</ta>
            <ta e="T407" id="Seg_6227" s="T405">Она пришла.</ta>
            <ta e="T410" id="Seg_6228" s="T407">Там живет женщина.</ta>
            <ta e="T411" id="Seg_6229" s="T410">очень… (?)</ta>
            <ta e="T421" id="Seg_6230" s="T411">Она говорит: "Женщина, скажи мне, куда мой муж ушел?"</ta>
            <ta e="T423" id="Seg_6231" s="T421">"Я знаю.</ta>
            <ta e="T424" id="Seg_6232" s="T423">Иди!"</ta>
            <ta e="T432" id="Seg_6233" s="T424">Она дала ей золотое (веретено?).</ta>
            <ta e="T437" id="Seg_6234" s="T432">И дала колечко, золотое.</ta>
            <ta e="T438" id="Seg_6235" s="T437">"Иди!</ta>
            <ta e="T440" id="Seg_6236" s="T438">Где-то он живет.</ta>
            <ta e="T445" id="Seg_6237" s="T440">Тоже дала ей клубок.</ta>
            <ta e="T451" id="Seg_6238" s="T445">"Куда он покатится, туда иди".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_6239" s="T0">There lived a man.</ta>
            <ta e="T7" id="Seg_6240" s="T3">He had three daughters.</ta>
            <ta e="T12" id="Seg_6241" s="T7">He went to sell bread.</ta>
            <ta e="T15" id="Seg_6242" s="T12">"What do you want me to buy for you?"</ta>
            <ta e="T21" id="Seg_6243" s="T15">One says: "Buy me a beautiful coat".</ta>
            <ta e="T32" id="Seg_6244" s="T21">Another says: "Buy me a beautiful scarp and a beautiful dress."</ta>
            <ta e="T39" id="Seg_6245" s="T32">And [to] the youngest [daughter he says]: "What should I buy for you?"</ta>
            <ta e="T43" id="Seg_6246" s="T39">"Bring me a feather of the Filist the Falcon."</ta>
            <ta e="T50" id="Seg_6247" s="T44">She said: "Bring me a feather of Filist the falcon."</ta>
            <ta e="T63" id="Seg_6248" s="T50">He went [there], bought a coat for this [daughter], bought a scarf and a dress for that [daughter].</ta>
            <ta e="T67" id="Seg_6249" s="T63">He looked and looked [for the feather, but] didn't find.</ta>
            <ta e="T70" id="Seg_6250" s="T67">Then he came home.</ta>
            <ta e="T75" id="Seg_6251" s="T70">These two girls are laughing.</ta>
            <ta e="T82" id="Seg_6252" s="T75">"For you he didn't buy anything, and for us he did."</ta>
            <ta e="T91" id="Seg_6253" s="T82">"So what, he didn't buy, let it not be."</ta>
            <ta e="T96" id="Seg_6254" s="T91">Then he went again [to sell] the grain.</ta>
            <ta e="T98" id="Seg_6255" s="T96">"What should I buy?"</ta>
            <ta e="T101" id="Seg_6256" s="T98">Then they say:</ta>
            <ta e="T104" id="Seg_6257" s="T101">"Buy [us] beautiful shoes."</ta>
            <ta e="T108" id="Seg_6258" s="T104">He went, bought them [the shoes].</ta>
            <ta e="T116" id="Seg_6259" s="T108">And the young [sister] says: "Buy me the feather of Filist the Falcon".</ta>
            <ta e="T120" id="Seg_6260" s="T116">He went, bought [shoes] for them.</ta>
            <ta e="T124" id="Seg_6261" s="T120">But didn't find it [= the feather].</ta>
            <ta e="T127" id="Seg_6262" s="T124">Then he went for a third time.</ta>
            <ta e="T132" id="Seg_6263" s="T127">Then he bought them everything.</ta>
            <ta e="T139" id="Seg_6264" s="T132">And for her he found the feather and brought it.</ta>
            <ta e="T144" id="Seg_6265" s="T139">Then he (?) went to sleep.</ta>
            <ta e="T147" id="Seg_6266" s="T144">She took this feather.</ta>
            <ta e="T149" id="Seg_6267" s="T147">(…) [it] in her hand.</ta>
            <ta e="T157" id="Seg_6268" s="T149">She threw it (with?) her hand, it became a boy, they are speaking.</ta>
            <ta e="T165" id="Seg_6269" s="T157">And her sisters saw that there is a boy.</ta>
            <ta e="T172" id="Seg_6270" s="T165">Then… there was a small window.</ta>
            <ta e="T178" id="Seg_6271" s="T172">They put knives there.</ta>
            <ta e="T182" id="Seg_6272" s="T178">And gave her to drink.</ta>
            <ta e="T186" id="Seg_6273" s="T182">[With] that water that [made] her sleep.</ta>
            <ta e="T188" id="Seg_6274" s="T186">He came.</ta>
            <ta e="T192" id="Seg_6275" s="T188">He shouted, shouted, he cut his arm. [?]</ta>
            <ta e="T194" id="Seg_6276" s="T192">And he left.</ta>
            <ta e="T203" id="Seg_6277" s="T195">Her sisters gave her drops to make her sleep.</ta>
            <ta e="T206" id="Seg_6278" s="T203">She fell asleep.</ta>
            <ta e="T209" id="Seg_6279" s="T206">And he came.</ta>
            <ta e="T214" id="Seg_6280" s="T209">They put knives in the window.</ta>
            <ta e="T218" id="Seg_6281" s="T214">He wanted to enter.</ta>
            <ta e="T221" id="Seg_6282" s="T218">[And] cut his leg.</ta>
            <ta e="T223" id="Seg_6283" s="T221">Blood is flowing.</ta>
            <ta e="T226" id="Seg_6284" s="T223">Then he wrote a letter.</ta>
            <ta e="T237" id="Seg_6285" s="T226">And laid it down: "When you make iron shoes, and wear them out, then you'll find me."</ta>
            <ta e="T241" id="Seg_6286" s="T238">Then she stood up.</ta>
            <ta e="T245" id="Seg_6287" s="T241">Sees: there is blood.</ta>
            <ta e="T248" id="Seg_6288" s="T245">And a paper is lying.</ta>
            <ta e="T254" id="Seg_6289" s="T248">She looked at this paper, she cried, she cried.</ta>
            <ta e="T257" id="Seg_6290" s="T254">And went to the forgery.</ta>
            <ta e="T263" id="Seg_6291" s="T257">[The smith] made iron shoes, an iron staff.</ta>
            <ta e="T265" id="Seg_6292" s="T263">Then she went.</ta>
            <ta e="T270" id="Seg_6293" s="T265">She went and went, she went for a long time.</ta>
            <ta e="T277" id="Seg_6294" s="T270">Then she sees: there is small house.</ta>
            <ta e="T280" id="Seg_6295" s="T277">She came to that house.</ta>
            <ta e="T283" id="Seg_6296" s="T280">A woman lives there.</ta>
            <ta e="T292" id="Seg_6297" s="T283">She asks: "Woman, tell me, where my husband went to?"</ta>
            <ta e="T300" id="Seg_6298" s="T292">She says: "I don't know where [he is], [he is] somewhere."</ta>
            <ta e="T303" id="Seg_6299" s="T300">Go to my sister.</ta>
            <ta e="T305" id="Seg_6300" s="T303">She knows that.</ta>
            <ta e="T307" id="Seg_6301" s="T305">She'll tell you."</ta>
            <ta e="T314" id="Seg_6302" s="T307">Then she gave her a golden dish, a golden egg.</ta>
            <ta e="T317" id="Seg_6303" s="T314">And a clew of thread.</ta>
            <ta e="T323" id="Seg_6304" s="T317">"Go, it will run, and you go [after it]."</ta>
            <ta e="T326" id="Seg_6305" s="T323">Then she left.</ta>
            <ta e="T331" id="Seg_6306" s="T327">Then she went, she went.</ta>
            <ta e="T334" id="Seg_6307" s="T331">She came to the second sister.</ta>
            <ta e="T341" id="Seg_6308" s="T334">Her house is also small.</ta>
            <ta e="T343" id="Seg_6309" s="T341">She entered the house</ta>
            <ta e="T348" id="Seg_6310" s="T343">A woman is living there.</ta>
            <ta e="T349" id="Seg_6311" s="T348">She asks:</ta>
            <ta e="T357" id="Seg_6312" s="T349">"Woman, tell me, where my husband went?"</ta>
            <ta e="T360" id="Seg_6313" s="T357">I don't know.</ta>
            <ta e="T367" id="Seg_6314" s="T360">Go to my sister, she'll tell you, where he is."</ta>
            <ta e="T372" id="Seg_6315" s="T367">She gave her a spinning-wheel.</ta>
            <ta e="T375" id="Seg_6316" s="T372">And gave a sliver.</ta>
            <ta e="T381" id="Seg_6317" s="T375">The spinning-wheel is from gold, and the sliver is from gold.</ta>
            <ta e="T385" id="Seg_6318" s="T381">Then she gave a clew.</ta>
            <ta e="T395" id="Seg_6319" s="T385">"It will go, and you go where it goes."</ta>
            <ta e="T399" id="Seg_6320" s="T395">Then she went and went.</ta>
            <ta e="T405" id="Seg_6321" s="T399">She came to the third sister, [her] house is standing, too.</ta>
            <ta e="T407" id="Seg_6322" s="T405">She came.</ta>
            <ta e="T410" id="Seg_6323" s="T407">There lives a woman.</ta>
            <ta e="T411" id="Seg_6324" s="T410">very…(?)</ta>
            <ta e="T421" id="Seg_6325" s="T411">Then she says: "Woman, tell me, where my husband went?"</ta>
            <ta e="T423" id="Seg_6326" s="T421">"I know.</ta>
            <ta e="T424" id="Seg_6327" s="T423">Go!"</ta>
            <ta e="T432" id="Seg_6328" s="T424">Then she gave her a golden (spindle?).</ta>
            <ta e="T437" id="Seg_6329" s="T432">And gave a golden ring.</ta>
            <ta e="T438" id="Seg_6330" s="T437">"Go!</ta>
            <ta e="T440" id="Seg_6331" s="T438">He lives somewhere."</ta>
            <ta e="T445" id="Seg_6332" s="T440">She gave her also a clew.</ta>
            <ta e="T451" id="Seg_6333" s="T445">"Where it goes, go there."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_6334" s="T0">Es lebte ein Mann.</ta>
            <ta e="T7" id="Seg_6335" s="T3">Er hatte drei Töchter.</ta>
            <ta e="T12" id="Seg_6336" s="T7">Er ging Brot verkaufen.</ta>
            <ta e="T15" id="Seg_6337" s="T12">"Was möchtest du, dass ich dir kaufe?"</ta>
            <ta e="T21" id="Seg_6338" s="T15">Eine sagt: "Kauf mir einen schönen Mantel."</ta>
            <ta e="T32" id="Seg_6339" s="T21">Die andere sagt: "Kauf mir einen schönen Schal und ein schönes Kleid."</ta>
            <ta e="T39" id="Seg_6340" s="T32">Und [zu] der jüngsten [Tochter sagt er]: "Was soll ich dir kaufen?"</ta>
            <ta e="T43" id="Seg_6341" s="T39">"Bring mir eine Feder von Filist dem Falken."</ta>
            <ta e="T50" id="Seg_6342" s="T44">Sie sagte: "Bring mit eine Feder von Filist dem Falken."</ta>
            <ta e="T63" id="Seg_6343" s="T50">Er ging [dorthin], kaufte einen Mantel für diese [Tochter], kaufte einen Schal und ein Kleid für jene [Tochter].</ta>
            <ta e="T67" id="Seg_6344" s="T63">Er suchte und suchte [nach der Feder, aber] fand keine.</ta>
            <ta e="T70" id="Seg_6345" s="T67">Dann kam er nach Hause.</ta>
            <ta e="T75" id="Seg_6346" s="T70">Diese beiden Mädchen lachen.</ta>
            <ta e="T82" id="Seg_6347" s="T75">"Für dich hat er nichts gekauft, aber für uns hat er."</ta>
            <ta e="T91" id="Seg_6348" s="T82">"Na und, er hat nichts gekauft, sei es so."</ta>
            <ta e="T96" id="Seg_6349" s="T91">Dann ging er wieder, um das Korn [zu verkaufen].</ta>
            <ta e="T98" id="Seg_6350" s="T96">"What soll ich kaufen?"</ta>
            <ta e="T101" id="Seg_6351" s="T98">Dann sagen sie:</ta>
            <ta e="T104" id="Seg_6352" s="T101">"Kauf [uns] schöne Schuhe."</ta>
            <ta e="T108" id="Seg_6353" s="T104">Er ging, kaufte ihnen [die Schuhe].</ta>
            <ta e="T116" id="Seg_6354" s="T108">Und die kleine [Schwester] sagt: "Kauf mir die Feder von Filist dem Falken."</ta>
            <ta e="T120" id="Seg_6355" s="T116">Er ging, kaufte [Schuhe] für sie.</ta>
            <ta e="T124" id="Seg_6356" s="T120">Aber er fand sie [= die Feder] nicht.</ta>
            <ta e="T127" id="Seg_6357" s="T124">Dann ging er zum dritten Mal.</ta>
            <ta e="T132" id="Seg_6358" s="T127">Dann kaufte er ihnen alles.</ta>
            <ta e="T139" id="Seg_6359" s="T132">Und für sie fand er die Feder und brachte sie.</ta>
            <ta e="T144" id="Seg_6360" s="T139">Dann er (?) ging schlafen.</ta>
            <ta e="T147" id="Seg_6361" s="T144">Sie nahm diese Feder.</ta>
            <ta e="T149" id="Seg_6362" s="T147">(…) [es] in ihrer Hand.</ta>
            <ta e="T157" id="Seg_6363" s="T149">Sie warf sie (mit?) der Hand, sie wurde zu einem Jungen, sie sprechen.</ta>
            <ta e="T165" id="Seg_6364" s="T157">Und ihrer Schwestern sahen, dass dort ein Junge ist.</ta>
            <ta e="T172" id="Seg_6365" s="T165">Dann… es gab ein kleines Fenster.</ta>
            <ta e="T178" id="Seg_6366" s="T172">Sie taten Messer dorthin.</ta>
            <ta e="T182" id="Seg_6367" s="T178">Und gaben ihr zu trinken.</ta>
            <ta e="T186" id="Seg_6368" s="T182">[Mit] dem Wasser, das sie schlafen [ließ].</ta>
            <ta e="T188" id="Seg_6369" s="T186">Er kam.</ta>
            <ta e="T192" id="Seg_6370" s="T188">Er schrie, er schrie, er schnitt sein Bein. [?]</ta>
            <ta e="T194" id="Seg_6371" s="T192">Und er ging.</ta>
            <ta e="T203" id="Seg_6372" s="T195">Ihre Schwestern gaben ihr Tropfen, um sie schlafen zu lassen.</ta>
            <ta e="T206" id="Seg_6373" s="T203">Sie schlief ein.</ta>
            <ta e="T209" id="Seg_6374" s="T206">Und er kam.</ta>
            <ta e="T214" id="Seg_6375" s="T209">Sie packten Messer ins Fenster.</ta>
            <ta e="T218" id="Seg_6376" s="T214">Er wollte hineingehen.</ta>
            <ta e="T221" id="Seg_6377" s="T218">[Und] schnitt sein Bein.</ta>
            <ta e="T223" id="Seg_6378" s="T221">Es fließt Blut.</ta>
            <ta e="T226" id="Seg_6379" s="T223">Dann schrieb er einen Brief.</ta>
            <ta e="T237" id="Seg_6380" s="T226">Und legte ihn hin: "Wenn du eiseren Schuhe machst und sie abträgst, dann findest du mich."</ta>
            <ta e="T241" id="Seg_6381" s="T238">Dann stand sie auf.</ta>
            <ta e="T245" id="Seg_6382" s="T241">Sie sieht: Dort ist Blut.</ta>
            <ta e="T248" id="Seg_6383" s="T245">Und ein Stück Papier liegt dort.</ta>
            <ta e="T254" id="Seg_6384" s="T248">Sie guckte auf das Papier, sie weinte, sie weinte.</ta>
            <ta e="T257" id="Seg_6385" s="T254">Und ging zur Schmiede.</ta>
            <ta e="T263" id="Seg_6386" s="T257">[Der Schmied] machte eiserne Schuhe, einen eisernen Stab.</ta>
            <ta e="T265" id="Seg_6387" s="T263">Dann ging sie.</ta>
            <ta e="T270" id="Seg_6388" s="T265">Sie ging und ging, sie ging lange.</ta>
            <ta e="T277" id="Seg_6389" s="T270">Dann sieht sie: Dort ist (ein) kleines Haus.</ta>
            <ta e="T280" id="Seg_6390" s="T277">Sie kam zu diesem Haus.</ta>
            <ta e="T283" id="Seg_6391" s="T280">Eine Frau wohnt dort.</ta>
            <ta e="T292" id="Seg_6392" s="T283">Sie fragt: "Frau, sag mir, wo mein Mann hingegangen ist?"</ta>
            <ta e="T300" id="Seg_6393" s="T292">Sie sagt: "Ich weiß nicht, wo [er ist], [er ist] irgendwo.</ta>
            <ta e="T303" id="Seg_6394" s="T300">Geh zu meiner Schwester.</ta>
            <ta e="T305" id="Seg_6395" s="T303">Sie weiß es.</ta>
            <ta e="T307" id="Seg_6396" s="T305">Sie sagt es dir."</ta>
            <ta e="T314" id="Seg_6397" s="T307">Dann gab sie ihr ein goldenes Mahl, ein goldenes Ei.</ta>
            <ta e="T317" id="Seg_6398" s="T314">Und ein Knäuel Garn.</ta>
            <ta e="T323" id="Seg_6399" s="T317">"Geh, es läuft, und du läufst [hinterher]."</ta>
            <ta e="T326" id="Seg_6400" s="T323">Dann ging sie weg.</ta>
            <ta e="T331" id="Seg_6401" s="T327">Dann ging sie, sie ging.</ta>
            <ta e="T334" id="Seg_6402" s="T331">Sie kam zur zweiten Schwester.</ta>
            <ta e="T341" id="Seg_6403" s="T334">Ihr Haus ist auch klein.</ta>
            <ta e="T343" id="Seg_6404" s="T341">Sie ging ins Haus hinein.</ta>
            <ta e="T349" id="Seg_6405" s="T348">Sie fragt:</ta>
            <ta e="T357" id="Seg_6406" s="T349">"Frau, sag mir, wo mein Mann hingegangen ist?"</ta>
            <ta e="T360" id="Seg_6407" s="T357">"Ich weiß es nicht.</ta>
            <ta e="T367" id="Seg_6408" s="T360">Geh zu meiner Schwester, sie wird dir sagen, wo er ist."</ta>
            <ta e="T372" id="Seg_6409" s="T367">Sie gab ihr ein Spinnrad.</ta>
            <ta e="T375" id="Seg_6410" s="T372">Und gab ihr ein Faserband.</ta>
            <ta e="T381" id="Seg_6411" s="T375">Das Spinnrad ist aus Gold und das Faserband ist aus Gold.</ta>
            <ta e="T385" id="Seg_6412" s="T381">Dann gab sie [ihr] ein Knäuel.</ta>
            <ta e="T395" id="Seg_6413" s="T385">"Es geht und du gehst, wohin es geht."</ta>
            <ta e="T399" id="Seg_6414" s="T395">Dann ging und ging sie.</ta>
            <ta e="T405" id="Seg_6415" s="T399">Sie kam zur dritten Schwester, [ihr] Haus steht auch.</ta>
            <ta e="T407" id="Seg_6416" s="T405">Sie kam.</ta>
            <ta e="T410" id="Seg_6417" s="T407">Dort wohnt eine Frau.</ta>
            <ta e="T411" id="Seg_6418" s="T410">sehr… (?)</ta>
            <ta e="T421" id="Seg_6419" s="T411">Dann sagt sie: "Frau, sag mir, wo mein Mann hingegangen ist?"</ta>
            <ta e="T423" id="Seg_6420" s="T421">"Ich weiß.</ta>
            <ta e="T424" id="Seg_6421" s="T423">Geh!"</ta>
            <ta e="T432" id="Seg_6422" s="T424">Dann gab sie ihr eine goldene (Spindel?).</ta>
            <ta e="T437" id="Seg_6423" s="T432">Und gab ihr einen goldenen Ring.</ta>
            <ta e="T438" id="Seg_6424" s="T437">"Geh!</ta>
            <ta e="T440" id="Seg_6425" s="T438">Er lebt irgendwo."</ta>
            <ta e="T445" id="Seg_6426" s="T440">Sie gibt ihr auch ein Knäuel.</ta>
            <ta e="T451" id="Seg_6427" s="T445">"Wo es hingeht, geh auch hin."</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_6428" s="T0">[GVY:] The tale "Finist the falcon'.</ta>
            <ta e="T12" id="Seg_6429" s="T7">[GVY:] Or grain?</ta>
            <ta e="T32" id="Seg_6430" s="T21">[GVY:] Or kuvas platʼtʼaʔi 'beautiful dresses'</ta>
            <ta e="T270" id="Seg_6431" s="T265">[GVY:] kunʼdʼom = kunʼdʼoŋ?</ta>
            <ta e="T277" id="Seg_6432" s="T270">[GVY:] it seems that she hesitates between singular and plural (singular is correct); and between idʼiʔeje and üdʼüge.</ta>
            <ta e="T375" id="Seg_6433" s="T372">[GVY:] kug'Elʼi.</ta>
            <ta e="T395" id="Seg_6434" s="T385">[GVY:] kandlüj? The first one [kandlɯj], the second one [-uj]</ta>
            <ta e="T410" id="Seg_6435" s="T407">[GVY:] or 'sits'.</ta>
            <ta e="T432" id="Seg_6436" s="T424">[GVY:] korniš or kormiš.</ta>
            <ta e="T437" id="Seg_6437" s="T432">[GVY:] in the original text, there is иголочка 'needle' - what sho was going to say first.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T453" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T454" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T455" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T456" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
