<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3DAA73A5-3EB6-F491-9B6B-5988A530CE49">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Head_flk\AA_1914_Head_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">302</ud-information>
            <ud-information attribute-name="# HIAT:w">221</ud-information>
            <ud-information attribute-name="# e">220</ud-information>
            <ud-information attribute-name="# HIAT:u">50</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <timeline-fork end="T159" start="T158">
            <tli id="T158.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T222" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nüke</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼezʼəʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobijəʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭzen</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">amzʼəttən</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nagouʔbi</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">onʼiʔ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">tüžöjdən</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">kuppijəʔ</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">büzʼe</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">baltu</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">ibi</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">nükebə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">sürerlüʔbi</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_57" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Nüket</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">kallaʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">tʼürbi</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_69" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">Kandəga</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">maʔ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">nuga</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_82" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">Maan</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">šüjööndə</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">sil</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">uja</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">edöle</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">nuga</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_103" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">Nüke</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">paʔbi</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">ambi</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_118" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">Šaʔlaːmbi</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">nünəbində</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">kegə</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_130" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">Măllaʔbə:</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">kur</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">ulu</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">mal</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">sürerlamnə</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_148" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Tulamnəbində</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">kegərerie:</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">Ajə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">karoʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip">!</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">Mal</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">ine</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">ular</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">tüžöj</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_181" n="HIAT:w" s="T47">bar</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_184" n="HIAT:w" s="T48">oʔb</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_187" n="HIAT:w" s="T49">tʼogən</ts>
                  <nts id="Seg_188" n="HIAT:ip">,</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">šedenən</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">ajə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">karoma</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_201" n="HIAT:w" s="T53">šübiiʔ</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_205" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">Kajomandə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">maʔdə</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">tĭrlöleʔ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">šobi</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_220" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">Kur</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">ulu</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">kegərerie</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">maan</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">ajə</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">karouʔbi</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_242" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">Tĭrlöleʔ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">šobi</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">kur</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">ulu</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_257" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_259" n="HIAT:w" s="T68">Aspaʔ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_262" n="HIAT:w" s="T69">edəʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">ujaj</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_269" n="HIAT:w" s="T71">sil</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_272" n="HIAT:w" s="T72">padoʔ</ts>
                  <nts id="Seg_273" n="HIAT:ip">!</nts>
               </ts>
               <ts e="T74" id="Seg_275" n="HIAT:u" s="T73">
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_279" n="HIAT:w" s="T73">mălia</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_283" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">Šindidə</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">ibi</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">dĭgən</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_295" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">Uʔbdaʔ</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_300" n="HIAT:w" s="T78">döbər</ts>
                  <nts id="Seg_301" n="HIAT:ip">!</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_304" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">Büzʼe</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">kuza</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">ibində</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">büzʼem</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_318" n="HIAT:w" s="T83">moləj</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_322" n="HIAT:w" s="T84">ne</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_325" n="HIAT:w" s="T85">kuza</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">ibində</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_331" n="HIAT:w" s="T87">helem</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">moləj</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_338" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">Uʔbdəbi</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_343" n="HIAT:w" s="T90">šaʔlaːmda</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_346" n="HIAT:w" s="T91">nüke</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_350" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_352" n="HIAT:w" s="T92">Šün</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">toʔgəndə</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_358" n="HIAT:w" s="T94">amnəbi</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">uluzʼəʔ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">amorzʼəttə</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_368" n="HIAT:w" s="T97">amzəbi</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_372" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_374" n="HIAT:w" s="T98">Tʼăbaktərlaʔ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_377" n="HIAT:w" s="T99">amnaiʔ:</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_380" n="HIAT:w" s="T100">Tăn</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_383" n="HIAT:w" s="T101">kădəʔ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_386" n="HIAT:w" s="T102">mĭlliel</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_389" n="HIAT:w" s="T103">kur</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_392" n="HIAT:w" s="T104">uluzʼəʔ</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_396" n="HIAT:w" s="T105">udaldaʔ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_399" n="HIAT:w" s="T107">naga</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_403" n="HIAT:w" s="T108">üjüldeʔ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_406" n="HIAT:w" s="T110">naga</ts>
                  <nts id="Seg_407" n="HIAT:ip">?</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_410" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_412" n="HIAT:w" s="T111">Măn</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_415" n="HIAT:w" s="T112">bar</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_418" n="HIAT:w" s="T113">tʼogən</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_421" n="HIAT:w" s="T114">mĭlliem</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_425" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_427" n="HIAT:w" s="T115">Tăn</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_430" n="HIAT:w" s="T116">oronə</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_433" n="HIAT:w" s="T117">tĭrlöleʔ</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_436" n="HIAT:w" s="T118">molala</ts>
                  <nts id="Seg_437" n="HIAT:ip">?</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_440" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_442" n="HIAT:w" s="T119">Molam</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_445" n="HIAT:w" s="T120">baːdəlaʔ</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_449" n="HIAT:w" s="T121">mălia</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_453" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_455" n="HIAT:w" s="T122">Tĭrlöʔ</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_458" n="HIAT:w" s="T123">tʼeʔbdə</ts>
                  <nts id="Seg_459" n="HIAT:ip">!</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_462" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_464" n="HIAT:w" s="T124">Mălia</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_468" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_470" n="HIAT:w" s="T125">Ulut</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_473" n="HIAT:w" s="T126">tĭrəllüʔbi</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_477" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_479" n="HIAT:w" s="T127">Baltu</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_482" n="HIAT:w" s="T128">ibi</ts>
                  <nts id="Seg_483" n="HIAT:ip">,</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_486" n="HIAT:w" s="T129">păktəj</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_489" n="HIAT:w" s="T130">toʔluʔbi</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_493" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_495" n="HIAT:w" s="T131">Kujubə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_498" n="HIAT:w" s="T132">săbojʔbi</ts>
                  <nts id="Seg_499" n="HIAT:ip">,</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_502" n="HIAT:w" s="T133">paʔbi</ts>
                  <nts id="Seg_503" n="HIAT:ip">.</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_506" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_508" n="HIAT:w" s="T134">Ujaj</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_511" n="HIAT:w" s="T135">sil</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">ibi</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_518" n="HIAT:w" s="T137">suzəjdə</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_521" n="HIAT:w" s="T138">păʔdəbi</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_525" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_527" n="HIAT:w" s="T139">Mĭllüʔbdəbi</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_531" n="HIAT:w" s="T140">büzʼeendə</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_534" n="HIAT:w" s="T141">kambi</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_538" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_540" n="HIAT:w" s="T142">Mazəronə</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_543" n="HIAT:w" s="T143">sʼabi</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_547" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_549" n="HIAT:w" s="T144">Müjəbə</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_552" n="HIAT:w" s="T145">büzʼe</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_555" n="HIAT:w" s="T146">köjelleʔ</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_558" n="HIAT:w" s="T147">amna:</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_561" n="HIAT:w" s="T148">Dĭm</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_564" n="HIAT:w" s="T149">teinen</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_567" n="HIAT:w" s="T150">amnəm</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_571" n="HIAT:w" s="T151">amim</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_574" n="HIAT:w" s="T152">karəldʼaːn</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_578" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_580" n="HIAT:w" s="T153">Dĭ</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_583" n="HIAT:w" s="T154">nüke</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_586" n="HIAT:w" s="T155">süzəjdəbə</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_589" n="HIAT:w" s="T156">üštəlüʔbi</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_592" n="HIAT:w" s="T157">mazərogəʔ</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_596" n="HIAT:u" s="T158">
                  <ts e="T158.tx.1" id="Seg_598" n="HIAT:w" s="T158">Numan</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_601" n="HIAT:w" s="T158.tx.1">üzəleʔbə:</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_604" n="HIAT:w" s="T159">Kudaj</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_607" n="HIAT:w" s="T160">möbi</ts>
                  <nts id="Seg_608" n="HIAT:ip">,</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_611" n="HIAT:w" s="T161">ĭššo</ts>
                  <nts id="Seg_612" n="HIAT:ip">,</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_615" n="HIAT:w" s="T162">možet</ts>
                  <nts id="Seg_616" n="HIAT:ip">,</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_619" n="HIAT:w" s="T163">mĭgəj</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_622" n="HIAT:w" s="T164">ĭššo</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_625" n="HIAT:w" s="T165">măna</ts>
                  <nts id="Seg_626" n="HIAT:ip">!</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_629" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_631" n="HIAT:w" s="T166">Dĭ</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_634" n="HIAT:w" s="T167">nüke:</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_637" n="HIAT:w" s="T168">Pim</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_640" n="HIAT:w" s="T169">tănan</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_643" n="HIAT:w" s="T170">mĭləj</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_646" n="HIAT:w" s="T171">kudaj</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_650" n="HIAT:w" s="T172">măn</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_653" n="HIAT:w" s="T173">mĭbiem</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_657" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_659" n="HIAT:w" s="T174">Măn</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_662" n="HIAT:w" s="T175">tănzʼəʔ</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_665" n="HIAT:w" s="T176">kallama</ts>
                  <nts id="Seg_666" n="HIAT:ip">?</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_669" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_671" n="HIAT:w" s="T177">Kanžəbəj</ts>
                  <nts id="Seg_672" n="HIAT:ip">!</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_675" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_677" n="HIAT:w" s="T178">Kunnaːmbi</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_681" n="HIAT:w" s="T179">ulun</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_684" n="HIAT:w" s="T180">maʔgəndə</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_687" n="HIAT:w" s="T181">šobiiʔ</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_691" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_693" n="HIAT:w" s="T182">Uja</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_696" n="HIAT:w" s="T183">sil</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_699" n="HIAT:w" s="T184">paʔbi</ts>
                  <nts id="Seg_700" n="HIAT:ip">,</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_703" n="HIAT:w" s="T185">badəbi</ts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_707" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_709" n="HIAT:w" s="T186">Dĭ</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_712" n="HIAT:w" s="T187">büzʼe:</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_715" n="HIAT:w" s="T188">Pünözərliem</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_719" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_721" n="HIAT:w" s="T189">Paʔdʼət</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_724" n="HIAT:w" s="T190">šuktoʔ</ts>
                  <nts id="Seg_725" n="HIAT:ip">!</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_728" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_730" n="HIAT:w" s="T191">Tăn</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_733" n="HIAT:w" s="T192">iʔ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_736" n="HIAT:w" s="T193">pünöreʔ</ts>
                  <nts id="Seg_737" n="HIAT:ip">!</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_740" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_742" n="HIAT:w" s="T194">Maləʔim</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_745" n="HIAT:w" s="T195">nereʔlil</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_749" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_751" n="HIAT:w" s="T196">Büzʼe</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_754" n="HIAT:w" s="T197">püʔlüʔbi</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_758" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_760" n="HIAT:w" s="T198">Maləʔim</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_763" n="HIAT:w" s="T199">tunolaʔbdəbi</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_767" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_769" n="HIAT:w" s="T200">Nüke</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_772" n="HIAT:w" s="T201">süt</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_775" n="HIAT:w" s="T202">ibi</ts>
                  <nts id="Seg_776" n="HIAT:ip">,</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_779" n="HIAT:w" s="T203">kuʔluʔbi</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_782" n="HIAT:w" s="T204">pineʔdən</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_785" n="HIAT:w" s="T205">dăra</ts>
                  <nts id="Seg_786" n="HIAT:ip">.</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_789" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_791" n="HIAT:w" s="T206">Ulariʔ</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_794" n="HIAT:w" s="T207">poːduʔ</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_797" n="HIAT:w" s="T208">mogəj</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_801" n="HIAT:w" s="T209">ineiʔ</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_804" n="HIAT:w" s="T210">bulan</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_807" n="HIAT:w" s="T211">mogəj</ts>
                  <nts id="Seg_808" n="HIAT:ip">,</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_811" n="HIAT:w" s="T212">tüžöj</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_814" n="HIAT:w" s="T213">sĭgən</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_817" n="HIAT:w" s="T214">mogəj</ts>
                  <nts id="Seg_818" n="HIAT:ip">!</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_821" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_823" n="HIAT:w" s="T215">Dĭ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_826" n="HIAT:w" s="T216">nüke</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_829" n="HIAT:w" s="T217">büzʼe</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_832" n="HIAT:w" s="T218">ĭmbidəndə</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_835" n="HIAT:w" s="T219">naga</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_839" n="HIAT:w" s="T220">bazoʔ</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_842" n="HIAT:w" s="T221">püjölaːmnəbi</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T222" id="Seg_845" n="sc" s="T0">
               <ts e="T1" id="Seg_847" n="e" s="T0">Nüke </ts>
               <ts e="T2" id="Seg_849" n="e" s="T1">büzʼezʼəʔ </ts>
               <ts e="T3" id="Seg_851" n="e" s="T2">amnobijəʔ. </ts>
               <ts e="T4" id="Seg_853" n="e" s="T3">Dĭzen </ts>
               <ts e="T5" id="Seg_855" n="e" s="T4">amzʼəttən </ts>
               <ts e="T6" id="Seg_857" n="e" s="T5">nagouʔbi, </ts>
               <ts e="T7" id="Seg_859" n="e" s="T6">onʼiʔ </ts>
               <ts e="T8" id="Seg_861" n="e" s="T7">tüžöjdən </ts>
               <ts e="T9" id="Seg_863" n="e" s="T8">kuppijəʔ. </ts>
               <ts e="T10" id="Seg_865" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_867" n="e" s="T10">büzʼe </ts>
               <ts e="T12" id="Seg_869" n="e" s="T11">baltu </ts>
               <ts e="T13" id="Seg_871" n="e" s="T12">ibi </ts>
               <ts e="T14" id="Seg_873" n="e" s="T13">nükebə </ts>
               <ts e="T15" id="Seg_875" n="e" s="T14">sürerlüʔbi. </ts>
               <ts e="T16" id="Seg_877" n="e" s="T15">Nüket </ts>
               <ts e="T17" id="Seg_879" n="e" s="T16">kallaʔ </ts>
               <ts e="T18" id="Seg_881" n="e" s="T17">tʼürbi. </ts>
               <ts e="T19" id="Seg_883" n="e" s="T18">Kandəga, </ts>
               <ts e="T20" id="Seg_885" n="e" s="T19">maʔ </ts>
               <ts e="T21" id="Seg_887" n="e" s="T20">nuga. </ts>
               <ts e="T22" id="Seg_889" n="e" s="T21">Maan </ts>
               <ts e="T23" id="Seg_891" n="e" s="T22">šüjööndə </ts>
               <ts e="T24" id="Seg_893" n="e" s="T23">sil </ts>
               <ts e="T25" id="Seg_895" n="e" s="T24">uja </ts>
               <ts e="T26" id="Seg_897" n="e" s="T25">edöle </ts>
               <ts e="T27" id="Seg_899" n="e" s="T26">nuga. </ts>
               <ts e="T28" id="Seg_901" n="e" s="T27">Nüke </ts>
               <ts e="T29" id="Seg_903" n="e" s="T28">paʔbi </ts>
               <ts e="T30" id="Seg_905" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_907" n="e" s="T30">ambi. </ts>
               <ts e="T32" id="Seg_909" n="e" s="T31">Šaʔlaːmbi </ts>
               <ts e="T33" id="Seg_911" n="e" s="T32">nünəbində </ts>
               <ts e="T34" id="Seg_913" n="e" s="T33">kegə. </ts>
               <ts e="T35" id="Seg_915" n="e" s="T34">Măllaʔbə: </ts>
               <ts e="T36" id="Seg_917" n="e" s="T35">kur </ts>
               <ts e="T37" id="Seg_919" n="e" s="T36">ulu </ts>
               <ts e="T38" id="Seg_921" n="e" s="T37">mal </ts>
               <ts e="T39" id="Seg_923" n="e" s="T38">sürerlamnə. </ts>
               <ts e="T40" id="Seg_925" n="e" s="T39">Tulamnəbində </ts>
               <ts e="T41" id="Seg_927" n="e" s="T40">kegərerie: </ts>
               <ts e="T42" id="Seg_929" n="e" s="T41">Ajə </ts>
               <ts e="T43" id="Seg_931" n="e" s="T42">karoʔ! </ts>
               <ts e="T44" id="Seg_933" n="e" s="T43">Mal, </ts>
               <ts e="T45" id="Seg_935" n="e" s="T44">ine, </ts>
               <ts e="T46" id="Seg_937" n="e" s="T45">ular, </ts>
               <ts e="T47" id="Seg_939" n="e" s="T46">tüžöj, </ts>
               <ts e="T48" id="Seg_941" n="e" s="T47">bar </ts>
               <ts e="T49" id="Seg_943" n="e" s="T48">oʔb </ts>
               <ts e="T50" id="Seg_945" n="e" s="T49">tʼogən, </ts>
               <ts e="T51" id="Seg_947" n="e" s="T50">šedenən </ts>
               <ts e="T52" id="Seg_949" n="e" s="T51">ajə </ts>
               <ts e="T53" id="Seg_951" n="e" s="T52">karoma, </ts>
               <ts e="T54" id="Seg_953" n="e" s="T53">šübiiʔ. </ts>
               <ts e="T55" id="Seg_955" n="e" s="T54">Kajomandə </ts>
               <ts e="T56" id="Seg_957" n="e" s="T55">maʔdə </ts>
               <ts e="T57" id="Seg_959" n="e" s="T56">tĭrlöleʔ </ts>
               <ts e="T58" id="Seg_961" n="e" s="T57">šobi. </ts>
               <ts e="T59" id="Seg_963" n="e" s="T58">Kur </ts>
               <ts e="T60" id="Seg_965" n="e" s="T59">ulu </ts>
               <ts e="T61" id="Seg_967" n="e" s="T60">kegərerie, </ts>
               <ts e="T62" id="Seg_969" n="e" s="T61">maan </ts>
               <ts e="T63" id="Seg_971" n="e" s="T62">ajə </ts>
               <ts e="T64" id="Seg_973" n="e" s="T63">karouʔbi. </ts>
               <ts e="T65" id="Seg_975" n="e" s="T64">Tĭrlöleʔ </ts>
               <ts e="T66" id="Seg_977" n="e" s="T65">šobi </ts>
               <ts e="T67" id="Seg_979" n="e" s="T66">kur </ts>
               <ts e="T68" id="Seg_981" n="e" s="T67">ulu. </ts>
               <ts e="T69" id="Seg_983" n="e" s="T68">Aspaʔ </ts>
               <ts e="T70" id="Seg_985" n="e" s="T69">edəʔ, </ts>
               <ts e="T71" id="Seg_987" n="e" s="T70">ujaj </ts>
               <ts e="T72" id="Seg_989" n="e" s="T71">sil </ts>
               <ts e="T73" id="Seg_991" n="e" s="T72">padoʔ!, </ts>
               <ts e="T74" id="Seg_993" n="e" s="T73">mălia. </ts>
               <ts e="T75" id="Seg_995" n="e" s="T74">Šindidə </ts>
               <ts e="T76" id="Seg_997" n="e" s="T75">ibi </ts>
               <ts e="T77" id="Seg_999" n="e" s="T76">dĭgən. </ts>
               <ts e="T78" id="Seg_1001" n="e" s="T77">Uʔbdaʔ </ts>
               <ts e="T79" id="Seg_1003" n="e" s="T78">döbər! </ts>
               <ts e="T80" id="Seg_1005" n="e" s="T79">Büzʼe </ts>
               <ts e="T81" id="Seg_1007" n="e" s="T80">kuza </ts>
               <ts e="T82" id="Seg_1009" n="e" s="T81">ibində </ts>
               <ts e="T83" id="Seg_1011" n="e" s="T82">büzʼem </ts>
               <ts e="T84" id="Seg_1013" n="e" s="T83">moləj, </ts>
               <ts e="T85" id="Seg_1015" n="e" s="T84">ne </ts>
               <ts e="T86" id="Seg_1017" n="e" s="T85">kuza </ts>
               <ts e="T87" id="Seg_1019" n="e" s="T86">ibində </ts>
               <ts e="T88" id="Seg_1021" n="e" s="T87">helem </ts>
               <ts e="T89" id="Seg_1023" n="e" s="T88">moləj. </ts>
               <ts e="T90" id="Seg_1025" n="e" s="T89">Uʔbdəbi </ts>
               <ts e="T91" id="Seg_1027" n="e" s="T90">šaʔlaːmda </ts>
               <ts e="T92" id="Seg_1029" n="e" s="T91">nüke. </ts>
               <ts e="T93" id="Seg_1031" n="e" s="T92">Šün </ts>
               <ts e="T94" id="Seg_1033" n="e" s="T93">toʔgəndə </ts>
               <ts e="T95" id="Seg_1035" n="e" s="T94">amnəbi, </ts>
               <ts e="T96" id="Seg_1037" n="e" s="T95">uluzʼəʔ </ts>
               <ts e="T97" id="Seg_1039" n="e" s="T96">amorzʼəttə </ts>
               <ts e="T98" id="Seg_1041" n="e" s="T97">amzəbi. </ts>
               <ts e="T99" id="Seg_1043" n="e" s="T98">Tʼăbaktərlaʔ </ts>
               <ts e="T100" id="Seg_1045" n="e" s="T99">amnaiʔ: </ts>
               <ts e="T101" id="Seg_1047" n="e" s="T100">Tăn </ts>
               <ts e="T102" id="Seg_1049" n="e" s="T101">kădəʔ </ts>
               <ts e="T103" id="Seg_1051" n="e" s="T102">mĭlliel </ts>
               <ts e="T104" id="Seg_1053" n="e" s="T103">kur </ts>
               <ts e="T105" id="Seg_1055" n="e" s="T104">uluzʼəʔ, </ts>
               <ts e="T107" id="Seg_1057" n="e" s="T105">udaldaʔ </ts>
               <ts e="T108" id="Seg_1059" n="e" s="T107">naga, </ts>
               <ts e="T110" id="Seg_1061" n="e" s="T108">üjüldeʔ </ts>
               <ts e="T111" id="Seg_1063" n="e" s="T110">naga? </ts>
               <ts e="T112" id="Seg_1065" n="e" s="T111">Măn </ts>
               <ts e="T113" id="Seg_1067" n="e" s="T112">bar </ts>
               <ts e="T114" id="Seg_1069" n="e" s="T113">tʼogən </ts>
               <ts e="T115" id="Seg_1071" n="e" s="T114">mĭlliem. </ts>
               <ts e="T116" id="Seg_1073" n="e" s="T115">Tăn </ts>
               <ts e="T117" id="Seg_1075" n="e" s="T116">oronə </ts>
               <ts e="T118" id="Seg_1077" n="e" s="T117">tĭrlöleʔ </ts>
               <ts e="T119" id="Seg_1079" n="e" s="T118">molala? </ts>
               <ts e="T120" id="Seg_1081" n="e" s="T119">Molam </ts>
               <ts e="T121" id="Seg_1083" n="e" s="T120">baːdəlaʔ, </ts>
               <ts e="T122" id="Seg_1085" n="e" s="T121">mălia. </ts>
               <ts e="T123" id="Seg_1087" n="e" s="T122">Tĭrlöʔ </ts>
               <ts e="T124" id="Seg_1089" n="e" s="T123">tʼeʔbdə! </ts>
               <ts e="T125" id="Seg_1091" n="e" s="T124">Mălia. </ts>
               <ts e="T126" id="Seg_1093" n="e" s="T125">Ulut </ts>
               <ts e="T127" id="Seg_1095" n="e" s="T126">tĭrəllüʔbi. </ts>
               <ts e="T128" id="Seg_1097" n="e" s="T127">Baltu </ts>
               <ts e="T129" id="Seg_1099" n="e" s="T128">ibi, </ts>
               <ts e="T130" id="Seg_1101" n="e" s="T129">păktəj </ts>
               <ts e="T131" id="Seg_1103" n="e" s="T130">toʔluʔbi. </ts>
               <ts e="T132" id="Seg_1105" n="e" s="T131">Kujubə </ts>
               <ts e="T133" id="Seg_1107" n="e" s="T132">săbojʔbi, </ts>
               <ts e="T134" id="Seg_1109" n="e" s="T133">paʔbi. </ts>
               <ts e="T135" id="Seg_1111" n="e" s="T134">Ujaj </ts>
               <ts e="T136" id="Seg_1113" n="e" s="T135">sil </ts>
               <ts e="T137" id="Seg_1115" n="e" s="T136">ibi, </ts>
               <ts e="T138" id="Seg_1117" n="e" s="T137">suzəjdə </ts>
               <ts e="T139" id="Seg_1119" n="e" s="T138">păʔdəbi. </ts>
               <ts e="T140" id="Seg_1121" n="e" s="T139">Mĭllüʔbdəbi, </ts>
               <ts e="T141" id="Seg_1123" n="e" s="T140">büzʼeendə </ts>
               <ts e="T142" id="Seg_1125" n="e" s="T141">kambi. </ts>
               <ts e="T143" id="Seg_1127" n="e" s="T142">Mazəronə </ts>
               <ts e="T144" id="Seg_1129" n="e" s="T143">sʼabi. </ts>
               <ts e="T145" id="Seg_1131" n="e" s="T144">Müjəbə </ts>
               <ts e="T146" id="Seg_1133" n="e" s="T145">büzʼe </ts>
               <ts e="T147" id="Seg_1135" n="e" s="T146">köjelleʔ </ts>
               <ts e="T148" id="Seg_1137" n="e" s="T147">amna: </ts>
               <ts e="T149" id="Seg_1139" n="e" s="T148">Dĭm </ts>
               <ts e="T150" id="Seg_1141" n="e" s="T149">teinen </ts>
               <ts e="T151" id="Seg_1143" n="e" s="T150">amnəm, </ts>
               <ts e="T152" id="Seg_1145" n="e" s="T151">amim </ts>
               <ts e="T153" id="Seg_1147" n="e" s="T152">karəldʼaːn. </ts>
               <ts e="T154" id="Seg_1149" n="e" s="T153">Dĭ </ts>
               <ts e="T155" id="Seg_1151" n="e" s="T154">nüke </ts>
               <ts e="T156" id="Seg_1153" n="e" s="T155">süzəjdəbə </ts>
               <ts e="T157" id="Seg_1155" n="e" s="T156">üštəlüʔbi </ts>
               <ts e="T158" id="Seg_1157" n="e" s="T157">mazərogəʔ. </ts>
               <ts e="T159" id="Seg_1159" n="e" s="T158">Numan üzəleʔbə: </ts>
               <ts e="T160" id="Seg_1161" n="e" s="T159">Kudaj </ts>
               <ts e="T161" id="Seg_1163" n="e" s="T160">möbi, </ts>
               <ts e="T162" id="Seg_1165" n="e" s="T161">ĭššo, </ts>
               <ts e="T163" id="Seg_1167" n="e" s="T162">možet, </ts>
               <ts e="T164" id="Seg_1169" n="e" s="T163">mĭgəj </ts>
               <ts e="T165" id="Seg_1171" n="e" s="T164">ĭššo </ts>
               <ts e="T166" id="Seg_1173" n="e" s="T165">măna! </ts>
               <ts e="T167" id="Seg_1175" n="e" s="T166">Dĭ </ts>
               <ts e="T168" id="Seg_1177" n="e" s="T167">nüke: </ts>
               <ts e="T169" id="Seg_1179" n="e" s="T168">Pim </ts>
               <ts e="T170" id="Seg_1181" n="e" s="T169">tănan </ts>
               <ts e="T171" id="Seg_1183" n="e" s="T170">mĭləj </ts>
               <ts e="T172" id="Seg_1185" n="e" s="T171">kudaj, </ts>
               <ts e="T173" id="Seg_1187" n="e" s="T172">măn </ts>
               <ts e="T174" id="Seg_1189" n="e" s="T173">mĭbiem. </ts>
               <ts e="T175" id="Seg_1191" n="e" s="T174">Măn </ts>
               <ts e="T176" id="Seg_1193" n="e" s="T175">tănzʼəʔ </ts>
               <ts e="T177" id="Seg_1195" n="e" s="T176">kallama? </ts>
               <ts e="T178" id="Seg_1197" n="e" s="T177">Kanžəbəj! </ts>
               <ts e="T179" id="Seg_1199" n="e" s="T178">Kunnaːmbi, </ts>
               <ts e="T180" id="Seg_1201" n="e" s="T179">ulun </ts>
               <ts e="T181" id="Seg_1203" n="e" s="T180">maʔgəndə </ts>
               <ts e="T182" id="Seg_1205" n="e" s="T181">šobiiʔ. </ts>
               <ts e="T183" id="Seg_1207" n="e" s="T182">Uja </ts>
               <ts e="T184" id="Seg_1209" n="e" s="T183">sil </ts>
               <ts e="T185" id="Seg_1211" n="e" s="T184">paʔbi, </ts>
               <ts e="T186" id="Seg_1213" n="e" s="T185">badəbi. </ts>
               <ts e="T187" id="Seg_1215" n="e" s="T186">Dĭ </ts>
               <ts e="T188" id="Seg_1217" n="e" s="T187">büzʼe: </ts>
               <ts e="T189" id="Seg_1219" n="e" s="T188">Pünözərliem. </ts>
               <ts e="T190" id="Seg_1221" n="e" s="T189">Paʔdʼət </ts>
               <ts e="T191" id="Seg_1223" n="e" s="T190">šuktoʔ! </ts>
               <ts e="T192" id="Seg_1225" n="e" s="T191">Tăn </ts>
               <ts e="T193" id="Seg_1227" n="e" s="T192">iʔ </ts>
               <ts e="T194" id="Seg_1229" n="e" s="T193">pünöreʔ! </ts>
               <ts e="T195" id="Seg_1231" n="e" s="T194">Maləʔim </ts>
               <ts e="T196" id="Seg_1233" n="e" s="T195">nereʔlil. </ts>
               <ts e="T197" id="Seg_1235" n="e" s="T196">Büzʼe </ts>
               <ts e="T198" id="Seg_1237" n="e" s="T197">püʔlüʔbi. </ts>
               <ts e="T199" id="Seg_1239" n="e" s="T198">Maləʔim </ts>
               <ts e="T200" id="Seg_1241" n="e" s="T199">tunolaʔbdəbi. </ts>
               <ts e="T201" id="Seg_1243" n="e" s="T200">Nüke </ts>
               <ts e="T202" id="Seg_1245" n="e" s="T201">süt </ts>
               <ts e="T203" id="Seg_1247" n="e" s="T202">ibi, </ts>
               <ts e="T204" id="Seg_1249" n="e" s="T203">kuʔluʔbi </ts>
               <ts e="T205" id="Seg_1251" n="e" s="T204">pineʔdən </ts>
               <ts e="T206" id="Seg_1253" n="e" s="T205">dăra. </ts>
               <ts e="T207" id="Seg_1255" n="e" s="T206">Ulariʔ </ts>
               <ts e="T208" id="Seg_1257" n="e" s="T207">poːduʔ </ts>
               <ts e="T209" id="Seg_1259" n="e" s="T208">mogəj, </ts>
               <ts e="T210" id="Seg_1261" n="e" s="T209">ineiʔ </ts>
               <ts e="T211" id="Seg_1263" n="e" s="T210">bulan </ts>
               <ts e="T212" id="Seg_1265" n="e" s="T211">mogəj, </ts>
               <ts e="T213" id="Seg_1267" n="e" s="T212">tüžöj </ts>
               <ts e="T214" id="Seg_1269" n="e" s="T213">sĭgən </ts>
               <ts e="T215" id="Seg_1271" n="e" s="T214">mogəj! </ts>
               <ts e="T216" id="Seg_1273" n="e" s="T215">Dĭ </ts>
               <ts e="T217" id="Seg_1275" n="e" s="T216">nüke </ts>
               <ts e="T218" id="Seg_1277" n="e" s="T217">büzʼe </ts>
               <ts e="T219" id="Seg_1279" n="e" s="T218">ĭmbidəndə </ts>
               <ts e="T220" id="Seg_1281" n="e" s="T219">naga, </ts>
               <ts e="T221" id="Seg_1283" n="e" s="T220">bazoʔ </ts>
               <ts e="T222" id="Seg_1285" n="e" s="T221">püjölaːmnəbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1286" s="T0">AA_1914_Head_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_1287" s="T3">AA_1914_Head_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_1288" s="T9">AA_1914_Head_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_1289" s="T15">AA_1914_Head_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_1290" s="T18">AA_1914_Head_flk.005 (002.001)</ta>
            <ta e="T27" id="Seg_1291" s="T21">AA_1914_Head_flk.006 (002.002)</ta>
            <ta e="T31" id="Seg_1292" s="T27">AA_1914_Head_flk.007 (002.003)</ta>
            <ta e="T34" id="Seg_1293" s="T31">AA_1914_Head_flk.008 (002.004)</ta>
            <ta e="T39" id="Seg_1294" s="T34">AA_1914_Head_flk.009 (002.005)</ta>
            <ta e="T43" id="Seg_1295" s="T39">AA_1914_Head_flk.010 (002.006) </ta>
            <ta e="T54" id="Seg_1296" s="T43">AA_1914_Head_flk.011 (002.008)</ta>
            <ta e="T58" id="Seg_1297" s="T54">AA_1914_Head_flk.012 (002.009)</ta>
            <ta e="T64" id="Seg_1298" s="T58">AA_1914_Head_flk.013 (002.010)</ta>
            <ta e="T68" id="Seg_1299" s="T64">AA_1914_Head_flk.014 (002.011)</ta>
            <ta e="T73" id="Seg_1300" s="T68">AA_1914_Head_flk.015 (002.012)</ta>
            <ta e="T74" id="Seg_1301" s="T73">AA_1914_Head_flk.016 (002.013)</ta>
            <ta e="T77" id="Seg_1302" s="T74">AA_1914_Head_flk.017 (002.014)</ta>
            <ta e="T79" id="Seg_1303" s="T77">AA_1914_Head_flk.018 (002.015)</ta>
            <ta e="T89" id="Seg_1304" s="T79">AA_1914_Head_flk.019 (002.016)</ta>
            <ta e="T92" id="Seg_1305" s="T89">AA_1914_Head_flk.020 (002.017)</ta>
            <ta e="T98" id="Seg_1306" s="T92">AA_1914_Head_flk.021 (002.018)</ta>
            <ta e="T111" id="Seg_1307" s="T98">AA_1914_Head_flk.022 (002.019)</ta>
            <ta e="T115" id="Seg_1308" s="T111">AA_1914_Head_flk.023 (002.021)</ta>
            <ta e="T119" id="Seg_1309" s="T115">AA_1914_Head_flk.024 (002.022)</ta>
            <ta e="T122" id="Seg_1310" s="T119">AA_1914_Head_flk.025 (002.023)</ta>
            <ta e="T124" id="Seg_1311" s="T122">AA_1914_Head_flk.026 (002.024)</ta>
            <ta e="T125" id="Seg_1312" s="T124">AA_1914_Head_flk.027 (002.025)</ta>
            <ta e="T127" id="Seg_1313" s="T125">AA_1914_Head_flk.028 (002.026)</ta>
            <ta e="T131" id="Seg_1314" s="T127">AA_1914_Head_flk.029 (002.027)</ta>
            <ta e="T134" id="Seg_1315" s="T131">AA_1914_Head_flk.030 (002.028)</ta>
            <ta e="T139" id="Seg_1316" s="T134">AA_1914_Head_flk.031 (002.029)</ta>
            <ta e="T142" id="Seg_1317" s="T139">AA_1914_Head_flk.032 (002.030)</ta>
            <ta e="T144" id="Seg_1318" s="T142">AA_1914_Head_flk.033 (002.031)</ta>
            <ta e="T153" id="Seg_1319" s="T144">AA_1914_Head_flk.034 (002.032)</ta>
            <ta e="T158" id="Seg_1320" s="T153">AA_1914_Head_flk.035 (002.034)</ta>
            <ta e="T166" id="Seg_1321" s="T158">AA_1914_Head_flk.036 (002.035)</ta>
            <ta e="T174" id="Seg_1322" s="T166">AA_1914_Head_flk.037 (002.037)</ta>
            <ta e="T177" id="Seg_1323" s="T174">AA_1914_Head_flk.038 (002.039)</ta>
            <ta e="T178" id="Seg_1324" s="T177">AA_1914_Head_flk.039 (002.040)</ta>
            <ta e="T182" id="Seg_1325" s="T178">AA_1914_Head_flk.040 (002.041)</ta>
            <ta e="T186" id="Seg_1326" s="T182">AA_1914_Head_flk.041 (002.042)</ta>
            <ta e="T189" id="Seg_1327" s="T186">AA_1914_Head_flk.042 (002.043)</ta>
            <ta e="T191" id="Seg_1328" s="T189">AA_1914_Head_flk.043 (002.044)</ta>
            <ta e="T194" id="Seg_1329" s="T191">AA_1914_Head_flk.044 (002.045)</ta>
            <ta e="T196" id="Seg_1330" s="T194">AA_1914_Head_flk.045 (002.046)</ta>
            <ta e="T198" id="Seg_1331" s="T196">AA_1914_Head_flk.046 (002.047)</ta>
            <ta e="T200" id="Seg_1332" s="T198">AA_1914_Head_flk.047 (002.048)</ta>
            <ta e="T206" id="Seg_1333" s="T200">AA_1914_Head_flk.048 (002.049)</ta>
            <ta e="T215" id="Seg_1334" s="T206">AA_1914_Head_flk.049 (002.050)</ta>
            <ta e="T222" id="Seg_1335" s="T215">AA_1914_Head_flk.050 (002.051)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1336" s="T0">Nüke büzʼezʼəʔ amnobijəʔ. </ta>
            <ta e="T9" id="Seg_1337" s="T3">Dĭzen amzʼəttən nagouʔbi, onʼiʔ tüžöjdən kuppijəʔ. </ta>
            <ta e="T15" id="Seg_1338" s="T9">Dĭ büzʼe baltu ibi nükebə sürerlüʔbi. </ta>
            <ta e="T18" id="Seg_1339" s="T15">Nüket kallaʔ tʼürbi. </ta>
            <ta e="T21" id="Seg_1340" s="T18">Kandəga, maʔ nuga. </ta>
            <ta e="T27" id="Seg_1341" s="T21">Maan šüjööndə sil uja edöle nuga. </ta>
            <ta e="T31" id="Seg_1342" s="T27">Nüke paʔbi i ambi. </ta>
            <ta e="T34" id="Seg_1343" s="T31">Šaʔlaːmbi nünəbində kegə. </ta>
            <ta e="T39" id="Seg_1344" s="T34">Măllaʔbə: kur ulu mal sürerlamnə. </ta>
            <ta e="T43" id="Seg_1345" s="T39">Tulamnəbində kegərerie: "Ajə karoʔ!" </ta>
            <ta e="T54" id="Seg_1346" s="T43">Mal, ine, ular, tüžöj, bar oʔb tʼogən, šedenən ajə karoma, šübiiʔ. </ta>
            <ta e="T58" id="Seg_1347" s="T54">Kajomandə maʔdə tĭrlöleʔ šobi. </ta>
            <ta e="T64" id="Seg_1348" s="T58">Kur ulu kegərerie, maan ajə karouʔbi. </ta>
            <ta e="T68" id="Seg_1349" s="T64">Tĭrlöleʔ šobi kur ulu. </ta>
            <ta e="T73" id="Seg_1350" s="T68">"Aspaʔ edəʔ, ujaj sil padoʔ!", </ta>
            <ta e="T74" id="Seg_1351" s="T73">mălia. </ta>
            <ta e="T77" id="Seg_1352" s="T74">"Šindidə ibi dĭgən. </ta>
            <ta e="T79" id="Seg_1353" s="T77">Uʔbdaʔ döbər! </ta>
            <ta e="T89" id="Seg_1354" s="T79">Büzʼe kuza ibində büzʼem moləj, ne kuza ibində helem moləj." </ta>
            <ta e="T92" id="Seg_1355" s="T89">Uʔbdəbi šaʔlaːmda nüke. </ta>
            <ta e="T98" id="Seg_1356" s="T92">Šün toʔgəndə amnəbi, uluzʼəʔ amorzʼəttə amzəbi. </ta>
            <ta e="T111" id="Seg_1357" s="T98">Tʼăbaktərlaʔ amnaiʔ: "Tăn kădəʔ mĭlliel kur uluzʼəʔ, udaldaʔ naga, üjüldeʔ naga?" </ta>
            <ta e="T115" id="Seg_1358" s="T111">"Măn bar tʼogən mĭlliem. </ta>
            <ta e="T119" id="Seg_1359" s="T115">"Tăn oronə tĭrlöleʔ molala?" </ta>
            <ta e="T122" id="Seg_1360" s="T119">"Molam baːdəlaʔ", mălia. </ta>
            <ta e="T124" id="Seg_1361" s="T122">"Tĭrlöʔ tʼeʔbdə!" </ta>
            <ta e="T125" id="Seg_1362" s="T124">Mălia. </ta>
            <ta e="T127" id="Seg_1363" s="T125">Ulut tĭrəllüʔbi. </ta>
            <ta e="T131" id="Seg_1364" s="T127">Baltu ibi, păktəj toʔluʔbi. </ta>
            <ta e="T134" id="Seg_1365" s="T131">Kujubə săbojʔbi, paʔbi. </ta>
            <ta e="T139" id="Seg_1366" s="T134">Ujaj sil ibi, suzəjdə păʔdəbi. </ta>
            <ta e="T142" id="Seg_1367" s="T139">Mĭllüʔbdəbi, büzʼeendə kambi. </ta>
            <ta e="T144" id="Seg_1368" s="T142">Mazəronə sʼabi. </ta>
            <ta e="T153" id="Seg_1369" s="T144">Müjəbə büzʼe köjelleʔ amna: "Dĭm teinen amnəm, amim karəldʼaːn." </ta>
            <ta e="T158" id="Seg_1370" s="T153">Dĭ nüke süzəjdəbə üštəlüʔbi mazərogəʔ. </ta>
            <ta e="T166" id="Seg_1371" s="T158">Numan üzəleʔbə: "Kudaj möbi, ĭššo, možet, mĭgəj ĭššo măna!" </ta>
            <ta e="T174" id="Seg_1372" s="T166">Dĭ nüke: "Pim tănan mĭləj kudaj, măn mĭbiem." </ta>
            <ta e="T177" id="Seg_1373" s="T174">"Măn tănzʼəʔ kallama?" </ta>
            <ta e="T178" id="Seg_1374" s="T177">"Kanžəbəj!" </ta>
            <ta e="T182" id="Seg_1375" s="T178">Kunnaːmbi, ulun maʔgəndə šobiiʔ. </ta>
            <ta e="T186" id="Seg_1376" s="T182">Uja sil paʔbi, badəbi. </ta>
            <ta e="T189" id="Seg_1377" s="T186">Dĭ büzʼe: "Pünözərliem." </ta>
            <ta e="T191" id="Seg_1378" s="T189">"Paʔdʼət šuktoʔ! </ta>
            <ta e="T194" id="Seg_1379" s="T191">Tăn iʔ pünöreʔ! </ta>
            <ta e="T196" id="Seg_1380" s="T194">Maləʔim nereʔlil." </ta>
            <ta e="T198" id="Seg_1381" s="T196">Büzʼe püʔlüʔbi. </ta>
            <ta e="T200" id="Seg_1382" s="T198">Maləʔim tunolaʔbdəbi. </ta>
            <ta e="T206" id="Seg_1383" s="T200">Nüke süt ibi, kuʔluʔbi pineʔdən dăra. </ta>
            <ta e="T215" id="Seg_1384" s="T206">"Ulariʔ poːduʔ mogəj, ineiʔ bulan mogəj, tüžöj sĭgən mogəj!" </ta>
            <ta e="T222" id="Seg_1385" s="T215">Dĭ nüke büzʼe ĭmbidəndə naga, bazoʔ püjölaːmnəbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1386" s="T0">nüke</ta>
            <ta e="T2" id="Seg_1387" s="T1">büzʼe-zʼəʔ</ta>
            <ta e="T3" id="Seg_1388" s="T2">amno-bi-jəʔ</ta>
            <ta e="T4" id="Seg_1389" s="T3">dĭ-zen</ta>
            <ta e="T5" id="Seg_1390" s="T4">am-zʼət-tən</ta>
            <ta e="T6" id="Seg_1391" s="T5">nag-o-uʔ-bi</ta>
            <ta e="T7" id="Seg_1392" s="T6">onʼiʔ</ta>
            <ta e="T8" id="Seg_1393" s="T7">tüžöj-dən</ta>
            <ta e="T9" id="Seg_1394" s="T8">kup-pi-jəʔ</ta>
            <ta e="T10" id="Seg_1395" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1396" s="T10">büzʼe</ta>
            <ta e="T12" id="Seg_1397" s="T11">baltu</ta>
            <ta e="T13" id="Seg_1398" s="T12">i-bi</ta>
            <ta e="T14" id="Seg_1399" s="T13">nüke-bə</ta>
            <ta e="T15" id="Seg_1400" s="T14">sürer-lüʔ-bi</ta>
            <ta e="T16" id="Seg_1401" s="T15">nüke-t</ta>
            <ta e="T17" id="Seg_1402" s="T16">kal-laʔ</ta>
            <ta e="T18" id="Seg_1403" s="T17">tʼür-bi</ta>
            <ta e="T19" id="Seg_1404" s="T18">kandə-ga</ta>
            <ta e="T20" id="Seg_1405" s="T19">maʔ</ta>
            <ta e="T21" id="Seg_1406" s="T20">nu-ga</ta>
            <ta e="T22" id="Seg_1407" s="T21">ma-a-n</ta>
            <ta e="T23" id="Seg_1408" s="T22">šüjö-öndə</ta>
            <ta e="T24" id="Seg_1409" s="T23">sil</ta>
            <ta e="T25" id="Seg_1410" s="T24">uja</ta>
            <ta e="T26" id="Seg_1411" s="T25">ed-ö-le</ta>
            <ta e="T27" id="Seg_1412" s="T26">nu-ga</ta>
            <ta e="T28" id="Seg_1413" s="T27">nüke</ta>
            <ta e="T29" id="Seg_1414" s="T28">paʔ-bi</ta>
            <ta e="T30" id="Seg_1415" s="T29">i</ta>
            <ta e="T31" id="Seg_1416" s="T30">am-bi</ta>
            <ta e="T32" id="Seg_1417" s="T31">šaʔ-laːm-bi</ta>
            <ta e="T33" id="Seg_1418" s="T32">nünə-bi-ndə</ta>
            <ta e="T34" id="Seg_1419" s="T33">kegə</ta>
            <ta e="T35" id="Seg_1420" s="T34">măl-laʔbə</ta>
            <ta e="T36" id="Seg_1421" s="T35">kur</ta>
            <ta e="T37" id="Seg_1422" s="T36">ulu</ta>
            <ta e="T38" id="Seg_1423" s="T37">mal</ta>
            <ta e="T39" id="Seg_1424" s="T38">sürer-lamnə</ta>
            <ta e="T40" id="Seg_1425" s="T39">tu-lamnə-bi-ndə</ta>
            <ta e="T41" id="Seg_1426" s="T40">kegərer-ie</ta>
            <ta e="T42" id="Seg_1427" s="T41">ajə</ta>
            <ta e="T43" id="Seg_1428" s="T42">kar-o-ʔ</ta>
            <ta e="T44" id="Seg_1429" s="T43">mal</ta>
            <ta e="T45" id="Seg_1430" s="T44">ine</ta>
            <ta e="T46" id="Seg_1431" s="T45">ular</ta>
            <ta e="T47" id="Seg_1432" s="T46">tüžöj</ta>
            <ta e="T48" id="Seg_1433" s="T47">bar</ta>
            <ta e="T49" id="Seg_1434" s="T48">oʔb</ta>
            <ta e="T50" id="Seg_1435" s="T49">tʼo-gən</ta>
            <ta e="T51" id="Seg_1436" s="T50">šeden-ə-n</ta>
            <ta e="T52" id="Seg_1437" s="T51">ajə</ta>
            <ta e="T53" id="Seg_1438" s="T52">kar-OmA</ta>
            <ta e="T54" id="Seg_1439" s="T53">šü-bi-iʔ</ta>
            <ta e="T55" id="Seg_1440" s="T54">kaj-OmA-ndə</ta>
            <ta e="T56" id="Seg_1441" s="T55">maʔ-də</ta>
            <ta e="T57" id="Seg_1442" s="T56">tĭrlö-leʔ</ta>
            <ta e="T58" id="Seg_1443" s="T57">šo-bi</ta>
            <ta e="T59" id="Seg_1444" s="T58">kur</ta>
            <ta e="T60" id="Seg_1445" s="T59">ulu</ta>
            <ta e="T61" id="Seg_1446" s="T60">kegərer-ie</ta>
            <ta e="T62" id="Seg_1447" s="T61">ma-a-n</ta>
            <ta e="T63" id="Seg_1448" s="T62">ajə</ta>
            <ta e="T64" id="Seg_1449" s="T63">kar-o-uʔ-bi</ta>
            <ta e="T65" id="Seg_1450" s="T64">tĭrlö-leʔ</ta>
            <ta e="T66" id="Seg_1451" s="T65">šo-bi</ta>
            <ta e="T67" id="Seg_1452" s="T66">kur</ta>
            <ta e="T68" id="Seg_1453" s="T67">ulu</ta>
            <ta e="T69" id="Seg_1454" s="T68">aspaʔ</ta>
            <ta e="T70" id="Seg_1455" s="T69">edə-ʔ</ta>
            <ta e="T71" id="Seg_1456" s="T70">uja-j</ta>
            <ta e="T72" id="Seg_1457" s="T71">sil</ta>
            <ta e="T73" id="Seg_1458" s="T72">pad-o-ʔ</ta>
            <ta e="T74" id="Seg_1459" s="T73">mă-lia</ta>
            <ta e="T75" id="Seg_1460" s="T74">šindi=də</ta>
            <ta e="T76" id="Seg_1461" s="T75">i-bi</ta>
            <ta e="T77" id="Seg_1462" s="T76">dĭ-gən</ta>
            <ta e="T78" id="Seg_1463" s="T77">uʔbd-aʔ</ta>
            <ta e="T79" id="Seg_1464" s="T78">döbər</ta>
            <ta e="T80" id="Seg_1465" s="T79">büzʼe</ta>
            <ta e="T81" id="Seg_1466" s="T80">kuza</ta>
            <ta e="T82" id="Seg_1467" s="T81">i-bi-ndə</ta>
            <ta e="T83" id="Seg_1468" s="T82">büzʼe-m</ta>
            <ta e="T84" id="Seg_1469" s="T83">mo-lə-j</ta>
            <ta e="T85" id="Seg_1470" s="T84">ne</ta>
            <ta e="T86" id="Seg_1471" s="T85">kuza</ta>
            <ta e="T87" id="Seg_1472" s="T86">i-bi-ndə</ta>
            <ta e="T88" id="Seg_1473" s="T87">hele-m</ta>
            <ta e="T89" id="Seg_1474" s="T88">mo-lə-j</ta>
            <ta e="T90" id="Seg_1475" s="T89">uʔbdə-bi</ta>
            <ta e="T91" id="Seg_1476" s="T90">šaʔ-laːm-da</ta>
            <ta e="T92" id="Seg_1477" s="T91">nüke</ta>
            <ta e="T93" id="Seg_1478" s="T92">šü-n</ta>
            <ta e="T94" id="Seg_1479" s="T93">toʔ-gəndə</ta>
            <ta e="T95" id="Seg_1480" s="T94">amnə-bi</ta>
            <ta e="T96" id="Seg_1481" s="T95">ulu-zʼəʔ</ta>
            <ta e="T97" id="Seg_1482" s="T96">amor-zʼəttə</ta>
            <ta e="T98" id="Seg_1483" s="T97">am-zə-bi</ta>
            <ta e="T99" id="Seg_1484" s="T98">tʼăbaktər-laʔ</ta>
            <ta e="T100" id="Seg_1485" s="T99">amna-iʔ</ta>
            <ta e="T101" id="Seg_1486" s="T100">tăn</ta>
            <ta e="T102" id="Seg_1487" s="T101">kădəʔ</ta>
            <ta e="T103" id="Seg_1488" s="T102">mĭl-lie-l</ta>
            <ta e="T104" id="Seg_1489" s="T103">kur</ta>
            <ta e="T105" id="Seg_1490" s="T104">ulu-zʼəʔ</ta>
            <ta e="T107" id="Seg_1491" s="T105">uda-l=daʔ</ta>
            <ta e="T108" id="Seg_1492" s="T107">naga</ta>
            <ta e="T110" id="Seg_1493" s="T108">üjü-l=deʔ</ta>
            <ta e="T111" id="Seg_1494" s="T110">naga</ta>
            <ta e="T112" id="Seg_1495" s="T111">măn</ta>
            <ta e="T113" id="Seg_1496" s="T112">bar</ta>
            <ta e="T114" id="Seg_1497" s="T113">tʼo-gən</ta>
            <ta e="T115" id="Seg_1498" s="T114">mĭl-lie-m</ta>
            <ta e="T116" id="Seg_1499" s="T115">tăn</ta>
            <ta e="T117" id="Seg_1500" s="T116">oro-nə</ta>
            <ta e="T118" id="Seg_1501" s="T117">tĭrlö-leʔ</ta>
            <ta e="T119" id="Seg_1502" s="T118">mo-la-l=a</ta>
            <ta e="T120" id="Seg_1503" s="T119">mo-la-m</ta>
            <ta e="T121" id="Seg_1504" s="T120">baːdə-laʔ</ta>
            <ta e="T122" id="Seg_1505" s="T121">mă-lia</ta>
            <ta e="T123" id="Seg_1506" s="T122">tĭrlö-ʔ</ta>
            <ta e="T124" id="Seg_1507" s="T123">tʼeʔb-də</ta>
            <ta e="T125" id="Seg_1508" s="T124">mă-lia</ta>
            <ta e="T126" id="Seg_1509" s="T125">ulu-t</ta>
            <ta e="T127" id="Seg_1510" s="T126">tĭrəl-lüʔ-bi</ta>
            <ta e="T128" id="Seg_1511" s="T127">baltu</ta>
            <ta e="T129" id="Seg_1512" s="T128">i-bi</ta>
            <ta e="T130" id="Seg_1513" s="T129">păktə-j</ta>
            <ta e="T131" id="Seg_1514" s="T130">toʔ-luʔ-bi</ta>
            <ta e="T132" id="Seg_1515" s="T131">kuju-bə</ta>
            <ta e="T133" id="Seg_1516" s="T132">săbojʔ-bi</ta>
            <ta e="T134" id="Seg_1517" s="T133">paʔ-bi</ta>
            <ta e="T135" id="Seg_1518" s="T134">uja-j</ta>
            <ta e="T136" id="Seg_1519" s="T135">sil</ta>
            <ta e="T137" id="Seg_1520" s="T136">i-bi</ta>
            <ta e="T138" id="Seg_1521" s="T137">suzəj-də</ta>
            <ta e="T139" id="Seg_1522" s="T138">păʔ-də-bi</ta>
            <ta e="T140" id="Seg_1523" s="T139">mĭl-lüʔbdə-bi</ta>
            <ta e="T141" id="Seg_1524" s="T140">büzʼe-endə</ta>
            <ta e="T142" id="Seg_1525" s="T141">kam-bi</ta>
            <ta e="T143" id="Seg_1526" s="T142">mazəro-nə</ta>
            <ta e="T144" id="Seg_1527" s="T143">sʼa-bi</ta>
            <ta e="T145" id="Seg_1528" s="T144">müjə-bə</ta>
            <ta e="T146" id="Seg_1529" s="T145">büzʼe</ta>
            <ta e="T147" id="Seg_1530" s="T146">köjel-leʔ</ta>
            <ta e="T148" id="Seg_1531" s="T147">amna</ta>
            <ta e="T149" id="Seg_1532" s="T148">dĭ-m</ta>
            <ta e="T150" id="Seg_1533" s="T149">teinen</ta>
            <ta e="T151" id="Seg_1534" s="T150">am-nə-m</ta>
            <ta e="T152" id="Seg_1535" s="T151">ami-m</ta>
            <ta e="T153" id="Seg_1536" s="T152">karəldʼaːn</ta>
            <ta e="T154" id="Seg_1537" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1538" s="T154">nüke</ta>
            <ta e="T156" id="Seg_1539" s="T155">süzəj-də-bə</ta>
            <ta e="T157" id="Seg_1540" s="T156">üštə-lüʔ-bi</ta>
            <ta e="T158" id="Seg_1541" s="T157">mazəro-gəʔ</ta>
            <ta e="T159" id="Seg_1542" s="T158">numan üzə-leʔbə</ta>
            <ta e="T160" id="Seg_1543" s="T159">kudaj</ta>
            <ta e="T161" id="Seg_1544" s="T160">mö-bi</ta>
            <ta e="T162" id="Seg_1545" s="T161">ĭššo</ta>
            <ta e="T163" id="Seg_1546" s="T162">možet</ta>
            <ta e="T164" id="Seg_1547" s="T163">mĭ-gə-j</ta>
            <ta e="T165" id="Seg_1548" s="T164">ĭššo</ta>
            <ta e="T166" id="Seg_1549" s="T165">măna</ta>
            <ta e="T167" id="Seg_1550" s="T166">dĭ</ta>
            <ta e="T168" id="Seg_1551" s="T167">nüke</ta>
            <ta e="T169" id="Seg_1552" s="T168">pi-m</ta>
            <ta e="T170" id="Seg_1553" s="T169">tănan</ta>
            <ta e="T171" id="Seg_1554" s="T170">mĭ-lə-j</ta>
            <ta e="T172" id="Seg_1555" s="T171">kudaj</ta>
            <ta e="T173" id="Seg_1556" s="T172">măn</ta>
            <ta e="T174" id="Seg_1557" s="T173">mĭ-bie-m</ta>
            <ta e="T175" id="Seg_1558" s="T174">măn</ta>
            <ta e="T176" id="Seg_1559" s="T175">tăn-zʼəʔ</ta>
            <ta e="T177" id="Seg_1560" s="T176">kal-la-m=a</ta>
            <ta e="T178" id="Seg_1561" s="T177">kan-žə-bəj</ta>
            <ta e="T179" id="Seg_1562" s="T178">kun-naːm-bi</ta>
            <ta e="T180" id="Seg_1563" s="T179">ulu-n</ta>
            <ta e="T181" id="Seg_1564" s="T180">maʔ-gəndə</ta>
            <ta e="T182" id="Seg_1565" s="T181">šo-bi-iʔ</ta>
            <ta e="T183" id="Seg_1566" s="T182">uja</ta>
            <ta e="T184" id="Seg_1567" s="T183">sil</ta>
            <ta e="T185" id="Seg_1568" s="T184">paʔ-bi</ta>
            <ta e="T186" id="Seg_1569" s="T185">bădə-bi</ta>
            <ta e="T187" id="Seg_1570" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_1571" s="T187">büzʼe</ta>
            <ta e="T189" id="Seg_1572" s="T188">pünö-zə-r-lie-m</ta>
            <ta e="T190" id="Seg_1573" s="T189">paʔdʼə-t</ta>
            <ta e="T191" id="Seg_1574" s="T190">šuktoʔ</ta>
            <ta e="T192" id="Seg_1575" s="T191">tăn</ta>
            <ta e="T193" id="Seg_1576" s="T192">i-ʔ</ta>
            <ta e="T194" id="Seg_1577" s="T193">pünö-r-e-ʔ</ta>
            <ta e="T195" id="Seg_1578" s="T194">mal-ʔi-m</ta>
            <ta e="T196" id="Seg_1579" s="T195">nereʔ-li-l</ta>
            <ta e="T197" id="Seg_1580" s="T196">büzʼe</ta>
            <ta e="T198" id="Seg_1581" s="T197">püʔ-lüʔ-bi</ta>
            <ta e="T199" id="Seg_1582" s="T198">mal-əʔi-m</ta>
            <ta e="T200" id="Seg_1583" s="T199">tuno-laʔbdə-bi</ta>
            <ta e="T201" id="Seg_1584" s="T200">nüke</ta>
            <ta e="T202" id="Seg_1585" s="T201">süt</ta>
            <ta e="T203" id="Seg_1586" s="T202">i-bi</ta>
            <ta e="T204" id="Seg_1587" s="T203">kuʔ-luʔ-bi</ta>
            <ta e="T205" id="Seg_1588" s="T204">pineʔ-dən</ta>
            <ta e="T206" id="Seg_1589" s="T205">dăra</ta>
            <ta e="T207" id="Seg_1590" s="T206">ular-iʔ</ta>
            <ta e="T208" id="Seg_1591" s="T207">poːduʔ</ta>
            <ta e="T209" id="Seg_1592" s="T208">mo-gə-j</ta>
            <ta e="T210" id="Seg_1593" s="T209">ine-iʔ</ta>
            <ta e="T211" id="Seg_1594" s="T210">bulan</ta>
            <ta e="T212" id="Seg_1595" s="T211">mo-gə-j</ta>
            <ta e="T213" id="Seg_1596" s="T212">tüžöj</ta>
            <ta e="T214" id="Seg_1597" s="T213">sĭgən</ta>
            <ta e="T215" id="Seg_1598" s="T214">mo-gə-j</ta>
            <ta e="T216" id="Seg_1599" s="T215">dĭ</ta>
            <ta e="T217" id="Seg_1600" s="T216">nüke</ta>
            <ta e="T218" id="Seg_1601" s="T217">büzʼe</ta>
            <ta e="T219" id="Seg_1602" s="T218">ĭmbi-dən=də</ta>
            <ta e="T220" id="Seg_1603" s="T219">naga</ta>
            <ta e="T221" id="Seg_1604" s="T220">bazoʔ</ta>
            <ta e="T222" id="Seg_1605" s="T221">püjö-laːmnə-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1606" s="T0">nüke</ta>
            <ta e="T2" id="Seg_1607" s="T1">büzʼe-ziʔ</ta>
            <ta e="T3" id="Seg_1608" s="T2">amno-bi-jəʔ</ta>
            <ta e="T4" id="Seg_1609" s="T3">dĭ-zen</ta>
            <ta e="T5" id="Seg_1610" s="T4">am-zit-dən</ta>
            <ta e="T6" id="Seg_1611" s="T5">naga-ö-luʔbdə-bi</ta>
            <ta e="T7" id="Seg_1612" s="T6">onʼiʔ</ta>
            <ta e="T8" id="Seg_1613" s="T7">tüžöj-dən</ta>
            <ta e="T9" id="Seg_1614" s="T8">kut-bi-jəʔ</ta>
            <ta e="T10" id="Seg_1615" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1616" s="T10">büzʼe</ta>
            <ta e="T12" id="Seg_1617" s="T11">baltu</ta>
            <ta e="T13" id="Seg_1618" s="T12">i-bi</ta>
            <ta e="T14" id="Seg_1619" s="T13">nüke-bə</ta>
            <ta e="T15" id="Seg_1620" s="T14">sürer-luʔbdə-bi</ta>
            <ta e="T16" id="Seg_1621" s="T15">nüke-t</ta>
            <ta e="T17" id="Seg_1622" s="T16">kan-lAʔ</ta>
            <ta e="T18" id="Seg_1623" s="T17">tʼür-bi</ta>
            <ta e="T19" id="Seg_1624" s="T18">kandə-gA</ta>
            <ta e="T20" id="Seg_1625" s="T19">maʔ</ta>
            <ta e="T21" id="Seg_1626" s="T20">nu-gA</ta>
            <ta e="T22" id="Seg_1627" s="T21">maʔ-ə-n</ta>
            <ta e="T23" id="Seg_1628" s="T22">šüjə-gəndə</ta>
            <ta e="T24" id="Seg_1629" s="T23">sil</ta>
            <ta e="T25" id="Seg_1630" s="T24">uja</ta>
            <ta e="T26" id="Seg_1631" s="T25">edə-o-lAʔ</ta>
            <ta e="T27" id="Seg_1632" s="T26">nu-gA</ta>
            <ta e="T28" id="Seg_1633" s="T27">nüke</ta>
            <ta e="T29" id="Seg_1634" s="T28">pada-bi</ta>
            <ta e="T30" id="Seg_1635" s="T29">i</ta>
            <ta e="T31" id="Seg_1636" s="T30">am-bi</ta>
            <ta e="T32" id="Seg_1637" s="T31">šaʔ-laːm-bi</ta>
            <ta e="T33" id="Seg_1638" s="T32">nünə-bi-gəndə</ta>
            <ta e="T34" id="Seg_1639" s="T33">kegə</ta>
            <ta e="T35" id="Seg_1640" s="T34">măndo-laʔbə</ta>
            <ta e="T36" id="Seg_1641" s="T35">kur</ta>
            <ta e="T37" id="Seg_1642" s="T36">ulu</ta>
            <ta e="T38" id="Seg_1643" s="T37">mal</ta>
            <ta e="T39" id="Seg_1644" s="T38">sürer-lamnə</ta>
            <ta e="T40" id="Seg_1645" s="T39">tu-lamnə-bi-gəndə</ta>
            <ta e="T41" id="Seg_1646" s="T40">kegərer-liA</ta>
            <ta e="T42" id="Seg_1647" s="T41">ajə</ta>
            <ta e="T43" id="Seg_1648" s="T42">kar-o-ʔ</ta>
            <ta e="T44" id="Seg_1649" s="T43">mal</ta>
            <ta e="T45" id="Seg_1650" s="T44">ine</ta>
            <ta e="T46" id="Seg_1651" s="T45">ular</ta>
            <ta e="T47" id="Seg_1652" s="T46">tüžöj</ta>
            <ta e="T48" id="Seg_1653" s="T47">bar</ta>
            <ta e="T49" id="Seg_1654" s="T48">oʔb</ta>
            <ta e="T50" id="Seg_1655" s="T49">tʼo-Kən</ta>
            <ta e="T51" id="Seg_1656" s="T50">šeden-ə-n</ta>
            <ta e="T52" id="Seg_1657" s="T51">ajə</ta>
            <ta e="T53" id="Seg_1658" s="T52">kar-OmA</ta>
            <ta e="T54" id="Seg_1659" s="T53">šü-bi-jəʔ</ta>
            <ta e="T55" id="Seg_1660" s="T54">kaj-OmA-gəndə</ta>
            <ta e="T56" id="Seg_1661" s="T55">maʔ-Tə</ta>
            <ta e="T57" id="Seg_1662" s="T56">tĭrlö-lAʔ</ta>
            <ta e="T58" id="Seg_1663" s="T57">šo-bi</ta>
            <ta e="T59" id="Seg_1664" s="T58">kur</ta>
            <ta e="T60" id="Seg_1665" s="T59">ulu</ta>
            <ta e="T61" id="Seg_1666" s="T60">kegərer-liA</ta>
            <ta e="T62" id="Seg_1667" s="T61">maʔ-ə-n</ta>
            <ta e="T63" id="Seg_1668" s="T62">ajə</ta>
            <ta e="T64" id="Seg_1669" s="T63">kar-o-luʔbdə-bi</ta>
            <ta e="T65" id="Seg_1670" s="T64">tĭrlö-lAʔ</ta>
            <ta e="T66" id="Seg_1671" s="T65">šo-bi</ta>
            <ta e="T67" id="Seg_1672" s="T66">kur</ta>
            <ta e="T68" id="Seg_1673" s="T67">ulu</ta>
            <ta e="T69" id="Seg_1674" s="T68">aspaʔ</ta>
            <ta e="T70" id="Seg_1675" s="T69">edə-ʔ</ta>
            <ta e="T71" id="Seg_1676" s="T70">uja-j</ta>
            <ta e="T72" id="Seg_1677" s="T71">sil</ta>
            <ta e="T73" id="Seg_1678" s="T72">pada-o-ʔ</ta>
            <ta e="T74" id="Seg_1679" s="T73">măn-liA</ta>
            <ta e="T75" id="Seg_1680" s="T74">šində=də</ta>
            <ta e="T76" id="Seg_1681" s="T75">i-bi</ta>
            <ta e="T77" id="Seg_1682" s="T76">dĭ-Kən</ta>
            <ta e="T78" id="Seg_1683" s="T77">uʔbdə-ʔ</ta>
            <ta e="T79" id="Seg_1684" s="T78">döbər</ta>
            <ta e="T80" id="Seg_1685" s="T79">büzʼe</ta>
            <ta e="T81" id="Seg_1686" s="T80">kuza</ta>
            <ta e="T82" id="Seg_1687" s="T81">i-bi-gəndə</ta>
            <ta e="T83" id="Seg_1688" s="T82">büzʼe-m</ta>
            <ta e="T84" id="Seg_1689" s="T83">mo-lV-j</ta>
            <ta e="T85" id="Seg_1690" s="T84">ne</ta>
            <ta e="T86" id="Seg_1691" s="T85">kuza</ta>
            <ta e="T87" id="Seg_1692" s="T86">i-bi-gəndə</ta>
            <ta e="T88" id="Seg_1693" s="T87">hele-m</ta>
            <ta e="T89" id="Seg_1694" s="T88">mo-lV-j</ta>
            <ta e="T90" id="Seg_1695" s="T89">uʔbdə-bi</ta>
            <ta e="T91" id="Seg_1696" s="T90">šaʔ-laːm-NTA</ta>
            <ta e="T92" id="Seg_1697" s="T91">nüke</ta>
            <ta e="T93" id="Seg_1698" s="T92">šü-n</ta>
            <ta e="T94" id="Seg_1699" s="T93">toʔ-gəndə</ta>
            <ta e="T95" id="Seg_1700" s="T94">amnə-bi</ta>
            <ta e="T96" id="Seg_1701" s="T95">ulu-ziʔ</ta>
            <ta e="T97" id="Seg_1702" s="T96">amor-zittə</ta>
            <ta e="T98" id="Seg_1703" s="T97">am-nzə-bi</ta>
            <ta e="T99" id="Seg_1704" s="T98">tʼăbaktər-lAʔ</ta>
            <ta e="T100" id="Seg_1705" s="T99">amnə-jəʔ</ta>
            <ta e="T101" id="Seg_1706" s="T100">tăn</ta>
            <ta e="T102" id="Seg_1707" s="T101">kădaʔ</ta>
            <ta e="T103" id="Seg_1708" s="T102">mĭn-liA-l</ta>
            <ta e="T104" id="Seg_1709" s="T103">kur</ta>
            <ta e="T105" id="Seg_1710" s="T104">ulu-ziʔ</ta>
            <ta e="T107" id="Seg_1711" s="T105">uda-l=da</ta>
            <ta e="T108" id="Seg_1712" s="T107">naga</ta>
            <ta e="T110" id="Seg_1713" s="T108">üjü-l=da</ta>
            <ta e="T111" id="Seg_1714" s="T110">naga</ta>
            <ta e="T112" id="Seg_1715" s="T111">măn</ta>
            <ta e="T113" id="Seg_1716" s="T112">bar</ta>
            <ta e="T114" id="Seg_1717" s="T113">tʼo-Kən</ta>
            <ta e="T115" id="Seg_1718" s="T114">mĭn-liA-m</ta>
            <ta e="T116" id="Seg_1719" s="T115">tăn</ta>
            <ta e="T117" id="Seg_1720" s="T116">oro-Tə</ta>
            <ta e="T118" id="Seg_1721" s="T117">tĭrlö-lAʔ</ta>
            <ta e="T119" id="Seg_1722" s="T118">mo-lV-l=a</ta>
            <ta e="T120" id="Seg_1723" s="T119">mo-lV-m</ta>
            <ta e="T121" id="Seg_1724" s="T120">baːdə-lAʔ</ta>
            <ta e="T122" id="Seg_1725" s="T121">măn-liA</ta>
            <ta e="T123" id="Seg_1726" s="T122">tĭrlö-ʔ</ta>
            <ta e="T124" id="Seg_1727" s="T123">tʼeʔb-Tə</ta>
            <ta e="T125" id="Seg_1728" s="T124">măn-liA</ta>
            <ta e="T126" id="Seg_1729" s="T125">ulu-t</ta>
            <ta e="T127" id="Seg_1730" s="T126">tĭrlö-luʔbdə-bi</ta>
            <ta e="T128" id="Seg_1731" s="T127">baltu</ta>
            <ta e="T129" id="Seg_1732" s="T128">i-bi</ta>
            <ta e="T130" id="Seg_1733" s="T129">păktə-j</ta>
            <ta e="T131" id="Seg_1734" s="T130">toʔbdə-luʔbdə-bi</ta>
            <ta e="T132" id="Seg_1735" s="T131">kuju-bə</ta>
            <ta e="T133" id="Seg_1736" s="T132">săbəj-bi</ta>
            <ta e="T134" id="Seg_1737" s="T133">pada-bi</ta>
            <ta e="T135" id="Seg_1738" s="T134">uja-j</ta>
            <ta e="T136" id="Seg_1739" s="T135">sil</ta>
            <ta e="T137" id="Seg_1740" s="T136">i-bi</ta>
            <ta e="T138" id="Seg_1741" s="T137">suzəj-Tə</ta>
            <ta e="T139" id="Seg_1742" s="T138">păda-də-bi</ta>
            <ta e="T140" id="Seg_1743" s="T139">mĭn-luʔbdə-bi</ta>
            <ta e="T141" id="Seg_1744" s="T140">büzʼe-gəndə</ta>
            <ta e="T142" id="Seg_1745" s="T141">kan-bi</ta>
            <ta e="T143" id="Seg_1746" s="T142">mazəro-Tə</ta>
            <ta e="T144" id="Seg_1747" s="T143">sʼa-bi</ta>
            <ta e="T145" id="Seg_1748" s="T144">müjə-bə</ta>
            <ta e="T146" id="Seg_1749" s="T145">büzʼe</ta>
            <ta e="T147" id="Seg_1750" s="T146">köjel-lAʔ</ta>
            <ta e="T148" id="Seg_1751" s="T147">amnə</ta>
            <ta e="T149" id="Seg_1752" s="T148">dĭ-m</ta>
            <ta e="T150" id="Seg_1753" s="T149">teinen</ta>
            <ta e="T151" id="Seg_1754" s="T150">am-lV-m</ta>
            <ta e="T152" id="Seg_1755" s="T151">ami-m</ta>
            <ta e="T153" id="Seg_1756" s="T152">karəldʼaːn</ta>
            <ta e="T154" id="Seg_1757" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1758" s="T154">nüke</ta>
            <ta e="T156" id="Seg_1759" s="T155">suzəj-də-bə</ta>
            <ta e="T157" id="Seg_1760" s="T156">üštə-luʔbdə-bi</ta>
            <ta e="T158" id="Seg_1761" s="T157">mazəro-gəʔ</ta>
            <ta e="T159" id="Seg_1762" s="T158">numan üzə-laʔbə</ta>
            <ta e="T160" id="Seg_1763" s="T159">kudaj</ta>
            <ta e="T161" id="Seg_1764" s="T160">mĭ-bi</ta>
            <ta e="T162" id="Seg_1765" s="T161">ĭššo</ta>
            <ta e="T163" id="Seg_1766" s="T162">možet</ta>
            <ta e="T164" id="Seg_1767" s="T163">mĭ-KV-j</ta>
            <ta e="T165" id="Seg_1768" s="T164">ĭššo</ta>
            <ta e="T166" id="Seg_1769" s="T165">măna</ta>
            <ta e="T167" id="Seg_1770" s="T166">dĭ</ta>
            <ta e="T168" id="Seg_1771" s="T167">nüke</ta>
            <ta e="T169" id="Seg_1772" s="T168">pi-m</ta>
            <ta e="T170" id="Seg_1773" s="T169">tănan</ta>
            <ta e="T171" id="Seg_1774" s="T170">mĭ-lV-j</ta>
            <ta e="T172" id="Seg_1775" s="T171">kudaj</ta>
            <ta e="T173" id="Seg_1776" s="T172">măn</ta>
            <ta e="T174" id="Seg_1777" s="T173">mĭ-bi-m</ta>
            <ta e="T175" id="Seg_1778" s="T174">măn</ta>
            <ta e="T176" id="Seg_1779" s="T175">tăn-ziʔ</ta>
            <ta e="T177" id="Seg_1780" s="T176">kan-lV-m=a</ta>
            <ta e="T178" id="Seg_1781" s="T177">kan-žə-bəj</ta>
            <ta e="T179" id="Seg_1782" s="T178">kun-laːm-bi</ta>
            <ta e="T180" id="Seg_1783" s="T179">ulu-n</ta>
            <ta e="T181" id="Seg_1784" s="T180">maʔ-gəndə</ta>
            <ta e="T182" id="Seg_1785" s="T181">šo-bi-jəʔ</ta>
            <ta e="T183" id="Seg_1786" s="T182">uja</ta>
            <ta e="T184" id="Seg_1787" s="T183">sil</ta>
            <ta e="T185" id="Seg_1788" s="T184">pada-bi</ta>
            <ta e="T186" id="Seg_1789" s="T185">bădə-bi</ta>
            <ta e="T187" id="Seg_1790" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_1791" s="T187">büzʼe</ta>
            <ta e="T189" id="Seg_1792" s="T188">pünö-nzə-r-liA-m</ta>
            <ta e="T190" id="Seg_1793" s="T189">padə-t</ta>
            <ta e="T191" id="Seg_1794" s="T190">šuktoʔ</ta>
            <ta e="T192" id="Seg_1795" s="T191">tăn</ta>
            <ta e="T193" id="Seg_1796" s="T192">e-ʔ</ta>
            <ta e="T194" id="Seg_1797" s="T193">pünö-r-ə-ʔ</ta>
            <ta e="T195" id="Seg_1798" s="T194">mal-jəʔ-m</ta>
            <ta e="T196" id="Seg_1799" s="T195">nereʔ-lV-l</ta>
            <ta e="T197" id="Seg_1800" s="T196">büzʼe</ta>
            <ta e="T198" id="Seg_1801" s="T197">püʔbdə-luʔbdə-bi</ta>
            <ta e="T199" id="Seg_1802" s="T198">mal-jəʔ-m</ta>
            <ta e="T200" id="Seg_1803" s="T199">tuno-laʔbdə-bi</ta>
            <ta e="T201" id="Seg_1804" s="T200">nüke</ta>
            <ta e="T202" id="Seg_1805" s="T201">süt</ta>
            <ta e="T203" id="Seg_1806" s="T202">i-bi</ta>
            <ta e="T204" id="Seg_1807" s="T203">kuʔ-luʔbdə-bi</ta>
            <ta e="T205" id="Seg_1808" s="T204">piʔne-dən</ta>
            <ta e="T206" id="Seg_1809" s="T205">dăra</ta>
            <ta e="T207" id="Seg_1810" s="T206">ular-jəʔ</ta>
            <ta e="T208" id="Seg_1811" s="T207">poʔto</ta>
            <ta e="T209" id="Seg_1812" s="T208">mo-KV-j</ta>
            <ta e="T210" id="Seg_1813" s="T209">ine-jəʔ</ta>
            <ta e="T211" id="Seg_1814" s="T210">bulan</ta>
            <ta e="T212" id="Seg_1815" s="T211">mo-KV-j</ta>
            <ta e="T213" id="Seg_1816" s="T212">tüžöj</ta>
            <ta e="T214" id="Seg_1817" s="T213">sĭgən</ta>
            <ta e="T215" id="Seg_1818" s="T214">mo-KV-j</ta>
            <ta e="T216" id="Seg_1819" s="T215">dĭ</ta>
            <ta e="T217" id="Seg_1820" s="T216">nüke</ta>
            <ta e="T218" id="Seg_1821" s="T217">büzʼe</ta>
            <ta e="T219" id="Seg_1822" s="T218">ĭmbi-dən=də</ta>
            <ta e="T220" id="Seg_1823" s="T219">naga</ta>
            <ta e="T221" id="Seg_1824" s="T220">bazoʔ</ta>
            <ta e="T222" id="Seg_1825" s="T221">püjö-lamnə-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1826" s="T0">woman.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1827" s="T1">man-INS</ta>
            <ta e="T3" id="Seg_1828" s="T2">live-PST-3PL</ta>
            <ta e="T4" id="Seg_1829" s="T3">this-GEN.PL</ta>
            <ta e="T5" id="Seg_1830" s="T4">eat-INF-NOM/GEN/ACC.3PL</ta>
            <ta e="T6" id="Seg_1831" s="T5">NEG.EX-DUR-MOM-PST.[3SG]</ta>
            <ta e="T7" id="Seg_1832" s="T6">single.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1833" s="T7">cow-NOM/GEN/ACC.3PL</ta>
            <ta e="T9" id="Seg_1834" s="T8">kill-PST-3PL</ta>
            <ta e="T10" id="Seg_1835" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1836" s="T10">man.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1837" s="T11">axe.[NOM.SG]</ta>
            <ta e="T13" id="Seg_1838" s="T12">take-PST.[3SG]</ta>
            <ta e="T14" id="Seg_1839" s="T13">woman-ACC.3SG</ta>
            <ta e="T15" id="Seg_1840" s="T14">drive-MOM-PST.[3SG]</ta>
            <ta e="T16" id="Seg_1841" s="T15">woman-NOM/GEN.3SG</ta>
            <ta e="T17" id="Seg_1842" s="T16">go-CVB</ta>
            <ta e="T18" id="Seg_1843" s="T17">disappear-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1844" s="T18">walk-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_1845" s="T19">tent.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1846" s="T20">stand-PRS.[3SG]</ta>
            <ta e="T22" id="Seg_1847" s="T21">tent-EP-GEN</ta>
            <ta e="T23" id="Seg_1848" s="T22">inside-LAT/LOC.3SG</ta>
            <ta e="T24" id="Seg_1849" s="T23">fat.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1850" s="T24">meat.[NOM.SG]</ta>
            <ta e="T26" id="Seg_1851" s="T25">hang.up-DETR-CVB</ta>
            <ta e="T27" id="Seg_1852" s="T26">stand-PRS.[3SG]</ta>
            <ta e="T28" id="Seg_1853" s="T27">woman.[NOM.SG]</ta>
            <ta e="T29" id="Seg_1854" s="T28">cook-PST.[3SG]</ta>
            <ta e="T30" id="Seg_1855" s="T29">and</ta>
            <ta e="T31" id="Seg_1856" s="T30">eat-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1857" s="T31">hide-RES-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1858" s="T32">hear-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T34" id="Seg_1859" s="T33">call.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1860" s="T34">look-DUR.[3SG]</ta>
            <ta e="T36" id="Seg_1861" s="T35">just</ta>
            <ta e="T37" id="Seg_1862" s="T36">head.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1863" s="T37">cattle.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1864" s="T38">drive-DUR.[3SG]</ta>
            <ta e="T40" id="Seg_1865" s="T39">arrive-DUR-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T41" id="Seg_1866" s="T40">call.out-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_1867" s="T41">door.[NOM.SG]</ta>
            <ta e="T43" id="Seg_1868" s="T42">open-DETR-IMP.2SG</ta>
            <ta e="T44" id="Seg_1869" s="T43">cattle.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1870" s="T44">horse.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1871" s="T45">sheep.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1872" s="T46">cow.[NOM.SG]</ta>
            <ta e="T48" id="Seg_1873" s="T47">all</ta>
            <ta e="T49" id="Seg_1874" s="T48">one.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1875" s="T49">place-LOC</ta>
            <ta e="T51" id="Seg_1876" s="T50">corral-EP-GEN</ta>
            <ta e="T52" id="Seg_1877" s="T51">door.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1878" s="T52">open-PTCP.PASS</ta>
            <ta e="T54" id="Seg_1879" s="T53">enter-PST-3PL</ta>
            <ta e="T55" id="Seg_1880" s="T54">close-PTCP.PASS-LAT/LOC.3SG</ta>
            <ta e="T56" id="Seg_1881" s="T55">tent-LAT</ta>
            <ta e="T57" id="Seg_1882" s="T56">roll-CVB</ta>
            <ta e="T58" id="Seg_1883" s="T57">come-PST.[3SG]</ta>
            <ta e="T59" id="Seg_1884" s="T58">just</ta>
            <ta e="T60" id="Seg_1885" s="T59">head.[NOM.SG]</ta>
            <ta e="T61" id="Seg_1886" s="T60">call.out-PRS.[3SG]</ta>
            <ta e="T62" id="Seg_1887" s="T61">tent-EP-GEN</ta>
            <ta e="T63" id="Seg_1888" s="T62">door.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1889" s="T63">open-DETR-MOM-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1890" s="T64">roll-CVB</ta>
            <ta e="T66" id="Seg_1891" s="T65">come-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1892" s="T66">just</ta>
            <ta e="T68" id="Seg_1893" s="T67">head.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1894" s="T68">cauldron.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1895" s="T69">hang.up-IMP.2SG</ta>
            <ta e="T71" id="Seg_1896" s="T70">meat-ADJZ.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1897" s="T71">fat.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1898" s="T72">cook-RFL-IMP.2SG</ta>
            <ta e="T74" id="Seg_1899" s="T73">say-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_1900" s="T74">who.[NOM.SG]=INDEF</ta>
            <ta e="T76" id="Seg_1901" s="T75">be-PST.[3SG]</ta>
            <ta e="T77" id="Seg_1902" s="T76">this-LOC</ta>
            <ta e="T78" id="Seg_1903" s="T77">get.up-IMP.2SG</ta>
            <ta e="T79" id="Seg_1904" s="T78">here</ta>
            <ta e="T80" id="Seg_1905" s="T79">man.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1906" s="T80">human.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1907" s="T81">be-PST-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_1908" s="T82">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T84" id="Seg_1909" s="T83">become-FUT-3SG</ta>
            <ta e="T85" id="Seg_1910" s="T84">woman.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1911" s="T85">human.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1912" s="T86">be-PST-LAT/LOC.3SG</ta>
            <ta e="T88" id="Seg_1913" s="T87">companion-NOM/GEN/ACC.1SG</ta>
            <ta e="T89" id="Seg_1914" s="T88">become-FUT-3SG</ta>
            <ta e="T90" id="Seg_1915" s="T89">get.up-PST.[3SG]</ta>
            <ta e="T91" id="Seg_1916" s="T90">hide-RES-PTCP</ta>
            <ta e="T92" id="Seg_1917" s="T91">woman.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1918" s="T92">fire-GEN</ta>
            <ta e="T94" id="Seg_1919" s="T93">edge-LAT/LOC.3SG</ta>
            <ta e="T95" id="Seg_1920" s="T94">sit-PST.[3SG]</ta>
            <ta e="T96" id="Seg_1921" s="T95">head-INS</ta>
            <ta e="T97" id="Seg_1922" s="T96">eat-INF.LAT</ta>
            <ta e="T98" id="Seg_1923" s="T97">eat-DES-PST.[3SG]</ta>
            <ta e="T99" id="Seg_1924" s="T98">speak-CVB</ta>
            <ta e="T100" id="Seg_1925" s="T99">sit-3PL</ta>
            <ta e="T101" id="Seg_1926" s="T100">you.NOM</ta>
            <ta e="T102" id="Seg_1927" s="T101">how</ta>
            <ta e="T103" id="Seg_1928" s="T102">go-PRS-2SG</ta>
            <ta e="T104" id="Seg_1929" s="T103">just</ta>
            <ta e="T105" id="Seg_1930" s="T104">head-INS</ta>
            <ta e="T107" id="Seg_1931" s="T105">hand-NOM/GEN/ACC.2SG=PTCL</ta>
            <ta e="T108" id="Seg_1932" s="T107">NEG.EX.[3SG]</ta>
            <ta e="T110" id="Seg_1933" s="T108">foot-NOM/GEN/ACC.2SG=PTCL</ta>
            <ta e="T111" id="Seg_1934" s="T110">NEG.EX.[3SG]</ta>
            <ta e="T112" id="Seg_1935" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_1936" s="T112">all</ta>
            <ta e="T114" id="Seg_1937" s="T113">place-LOC</ta>
            <ta e="T115" id="Seg_1938" s="T114">go-PRS-1SG</ta>
            <ta e="T116" id="Seg_1939" s="T115">you.NOM</ta>
            <ta e="T117" id="Seg_1940" s="T116">den-LAT</ta>
            <ta e="T118" id="Seg_1941" s="T117">roll-CVB</ta>
            <ta e="T119" id="Seg_1942" s="T118">can-FUT-2SG=QP</ta>
            <ta e="T120" id="Seg_1943" s="T119">can-FUT-1SG</ta>
            <ta e="T121" id="Seg_1944" s="T120">step-CVB</ta>
            <ta e="T122" id="Seg_1945" s="T121">say-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_1946" s="T122">roll-IMP.2SG</ta>
            <ta e="T124" id="Seg_1947" s="T123">ground-LAT</ta>
            <ta e="T125" id="Seg_1948" s="T124">say-PRS.[3SG]</ta>
            <ta e="T126" id="Seg_1949" s="T125">head-NOM/GEN.3SG</ta>
            <ta e="T127" id="Seg_1950" s="T126">roll-MOM-PST.[3SG]</ta>
            <ta e="T128" id="Seg_1951" s="T127">axe.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1952" s="T128">take-PST.[3SG]</ta>
            <ta e="T130" id="Seg_1953" s="T129">burst-CVB</ta>
            <ta e="T131" id="Seg_1954" s="T130">hit-MOM-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1955" s="T131">brain-ACC.3SG</ta>
            <ta e="T133" id="Seg_1956" s="T132">take.out-PST.[3SG]</ta>
            <ta e="T134" id="Seg_1957" s="T133">cook-PST.[3SG]</ta>
            <ta e="T135" id="Seg_1958" s="T134">meat-ADJZ.[NOM.SG]</ta>
            <ta e="T136" id="Seg_1959" s="T135">fat.[NOM.SG]</ta>
            <ta e="T137" id="Seg_1960" s="T136">take-PST.[3SG]</ta>
            <ta e="T138" id="Seg_1961" s="T137">bladder-LAT</ta>
            <ta e="T139" id="Seg_1962" s="T138">creep.into-TR-PST.[3SG]</ta>
            <ta e="T140" id="Seg_1963" s="T139">go-MOM-PST.[3SG]</ta>
            <ta e="T141" id="Seg_1964" s="T140">man-LAT/LOC.3SG</ta>
            <ta e="T142" id="Seg_1965" s="T141">go-PST.[3SG]</ta>
            <ta e="T143" id="Seg_1966" s="T142">smoke.hole-LAT</ta>
            <ta e="T144" id="Seg_1967" s="T143">climb-PST.[3SG]</ta>
            <ta e="T145" id="Seg_1968" s="T144">finger-ACC.3SG</ta>
            <ta e="T146" id="Seg_1969" s="T145">man.[NOM.SG]</ta>
            <ta e="T147" id="Seg_1970" s="T146">mark-CVB</ta>
            <ta e="T148" id="Seg_1971" s="T147">sit.[3SG]</ta>
            <ta e="T149" id="Seg_1972" s="T148">this-ACC</ta>
            <ta e="T150" id="Seg_1973" s="T149">today</ta>
            <ta e="T151" id="Seg_1974" s="T150">eat-FUT-1SG</ta>
            <ta e="T152" id="Seg_1975" s="T151">other-ACC</ta>
            <ta e="T153" id="Seg_1976" s="T152">tomorrow</ta>
            <ta e="T154" id="Seg_1977" s="T153">this.[NOM.SG]</ta>
            <ta e="T155" id="Seg_1978" s="T154">woman.[NOM.SG]</ta>
            <ta e="T156" id="Seg_1979" s="T155">bladder-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T157" id="Seg_1980" s="T156">drop-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_1981" s="T157">smoke.hole-ABL</ta>
            <ta e="T159" id="Seg_1982" s="T158">pray-DUR.[3SG]</ta>
            <ta e="T160" id="Seg_1983" s="T159">God.[NOM.SG]</ta>
            <ta e="T161" id="Seg_1984" s="T160">give-PST.[3SG]</ta>
            <ta e="T162" id="Seg_1985" s="T161">more</ta>
            <ta e="T163" id="Seg_1986" s="T162">maybe</ta>
            <ta e="T164" id="Seg_1987" s="T163">give-IMP-3SG</ta>
            <ta e="T165" id="Seg_1988" s="T164">more</ta>
            <ta e="T166" id="Seg_1989" s="T165">I.LAT</ta>
            <ta e="T167" id="Seg_1990" s="T166">this.[NOM.SG]</ta>
            <ta e="T168" id="Seg_1991" s="T167">woman.[NOM.SG]</ta>
            <ta e="T169" id="Seg_1992" s="T168">stone-ACC</ta>
            <ta e="T170" id="Seg_1993" s="T169">you.DAT</ta>
            <ta e="T171" id="Seg_1994" s="T170">give-FUT-3SG</ta>
            <ta e="T172" id="Seg_1995" s="T171">God.[NOM.SG]</ta>
            <ta e="T173" id="Seg_1996" s="T172">I.NOM</ta>
            <ta e="T174" id="Seg_1997" s="T173">give-PST-1SG</ta>
            <ta e="T175" id="Seg_1998" s="T174">I.NOM</ta>
            <ta e="T176" id="Seg_1999" s="T175">you.NOM-INS</ta>
            <ta e="T177" id="Seg_2000" s="T176">go-FUT-1SG=QP</ta>
            <ta e="T178" id="Seg_2001" s="T177">go-OPT.DU/PL-1DU</ta>
            <ta e="T179" id="Seg_2002" s="T178">bring-RES-PST.[3SG]</ta>
            <ta e="T180" id="Seg_2003" s="T179">head-GEN</ta>
            <ta e="T181" id="Seg_2004" s="T180">tent-LAT/LOC.3SG</ta>
            <ta e="T182" id="Seg_2005" s="T181">come-PST-3PL</ta>
            <ta e="T183" id="Seg_2006" s="T182">meat.[NOM.SG]</ta>
            <ta e="T184" id="Seg_2007" s="T183">fat.[NOM.SG]</ta>
            <ta e="T185" id="Seg_2008" s="T184">cook-PST.[3SG]</ta>
            <ta e="T186" id="Seg_2009" s="T185">feed-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2010" s="T186">this.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2011" s="T187">man.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2012" s="T188">fart-DES-FRQ-PRS-1SG</ta>
            <ta e="T190" id="Seg_2013" s="T189">put-IMP.2SG.O</ta>
            <ta e="T191" id="Seg_2014" s="T190">bed.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2015" s="T191">you.NOM</ta>
            <ta e="T193" id="Seg_2016" s="T192">NEG.AUX-IMP.2SG</ta>
            <ta e="T194" id="Seg_2017" s="T193">fart-FRQ-EP-CNG</ta>
            <ta e="T195" id="Seg_2018" s="T194">cattle-PL-ACC</ta>
            <ta e="T196" id="Seg_2019" s="T195">frighten-FUT-2SG</ta>
            <ta e="T197" id="Seg_2020" s="T196">man.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2021" s="T197">fart-MOM-PST.[3SG]</ta>
            <ta e="T199" id="Seg_2022" s="T198">cattle-PL-ACC</ta>
            <ta e="T200" id="Seg_2023" s="T199">gallop-TR.RES-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2024" s="T200">woman.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2025" s="T201">milk.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2026" s="T202">take-PST.[3SG]</ta>
            <ta e="T204" id="Seg_2027" s="T203">sprinkle-MOM-PST.[3SG]</ta>
            <ta e="T205" id="Seg_2028" s="T204">behind-NOM/GEN/ACC.3PL</ta>
            <ta e="T206" id="Seg_2029" s="T205">along</ta>
            <ta e="T207" id="Seg_2030" s="T206">sheep-PL</ta>
            <ta e="T208" id="Seg_2031" s="T207">goat.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2032" s="T208">become-IMP-3SG</ta>
            <ta e="T210" id="Seg_2033" s="T209">horse-PL</ta>
            <ta e="T211" id="Seg_2034" s="T210">moose.[NOM.SG]</ta>
            <ta e="T212" id="Seg_2035" s="T211">become-IMP-3SG</ta>
            <ta e="T213" id="Seg_2036" s="T212">cow.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2037" s="T213">deer.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2038" s="T214">become-IMP-3SG</ta>
            <ta e="T216" id="Seg_2039" s="T215">this.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2040" s="T216">woman.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2041" s="T217">man.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2042" s="T218">what-NOM/GEN/ACC.3PL=INDEF</ta>
            <ta e="T220" id="Seg_2043" s="T219">NEG.EX.[3SG]</ta>
            <ta e="T221" id="Seg_2044" s="T220">again</ta>
            <ta e="T222" id="Seg_2045" s="T221">be.hungry-DUR-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2046" s="T0">женщина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2047" s="T1">мужчина-INS</ta>
            <ta e="T3" id="Seg_2048" s="T2">жить-PST-3PL</ta>
            <ta e="T4" id="Seg_2049" s="T3">этот-GEN.PL</ta>
            <ta e="T5" id="Seg_2050" s="T4">съесть-INF-NOM/GEN/ACC.3PL</ta>
            <ta e="T6" id="Seg_2051" s="T5">NEG.EX-DUR-MOM-PST.[3SG]</ta>
            <ta e="T7" id="Seg_2052" s="T6">один.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2053" s="T7">корова-NOM/GEN/ACC.3PL</ta>
            <ta e="T9" id="Seg_2054" s="T8">убить-PST-3PL</ta>
            <ta e="T10" id="Seg_2055" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2056" s="T10">мужчина.[NOM.SG]</ta>
            <ta e="T12" id="Seg_2057" s="T11">топор.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2058" s="T12">взять-PST.[3SG]</ta>
            <ta e="T14" id="Seg_2059" s="T13">женщина-ACC.3SG</ta>
            <ta e="T15" id="Seg_2060" s="T14">гнать-MOM-PST.[3SG]</ta>
            <ta e="T16" id="Seg_2061" s="T15">женщина-NOM/GEN.3SG</ta>
            <ta e="T17" id="Seg_2062" s="T16">пойти-CVB</ta>
            <ta e="T18" id="Seg_2063" s="T17">исчезнуть-PST.[3SG]</ta>
            <ta e="T19" id="Seg_2064" s="T18">идти-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_2065" s="T19">чум.[NOM.SG]</ta>
            <ta e="T21" id="Seg_2066" s="T20">стоять-PRS.[3SG]</ta>
            <ta e="T22" id="Seg_2067" s="T21">чум-EP-GEN</ta>
            <ta e="T23" id="Seg_2068" s="T22">внутри-LAT/LOC.3SG</ta>
            <ta e="T24" id="Seg_2069" s="T23">жир.[NOM.SG]</ta>
            <ta e="T25" id="Seg_2070" s="T24">мясо.[NOM.SG]</ta>
            <ta e="T26" id="Seg_2071" s="T25">вешать-DETR-CVB</ta>
            <ta e="T27" id="Seg_2072" s="T26">стоять-PRS.[3SG]</ta>
            <ta e="T28" id="Seg_2073" s="T27">женщина.[NOM.SG]</ta>
            <ta e="T29" id="Seg_2074" s="T28">варить-PST.[3SG]</ta>
            <ta e="T30" id="Seg_2075" s="T29">и</ta>
            <ta e="T31" id="Seg_2076" s="T30">съесть-PST.[3SG]</ta>
            <ta e="T32" id="Seg_2077" s="T31">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T33" id="Seg_2078" s="T32">слышать-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T34" id="Seg_2079" s="T33">зов.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2080" s="T34">смотреть-DUR.[3SG]</ta>
            <ta e="T36" id="Seg_2081" s="T35">только</ta>
            <ta e="T37" id="Seg_2082" s="T36">голова.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2083" s="T37">скот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_2084" s="T38">гнать-DUR.[3SG]</ta>
            <ta e="T40" id="Seg_2085" s="T39">прибыть-DUR-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T41" id="Seg_2086" s="T40">кричать-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_2087" s="T41">дверь.[NOM.SG]</ta>
            <ta e="T43" id="Seg_2088" s="T42">открыть-DETR-IMP.2SG</ta>
            <ta e="T44" id="Seg_2089" s="T43">скот.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2090" s="T44">лошадь.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2091" s="T45">овца.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2092" s="T46">корова.[NOM.SG]</ta>
            <ta e="T48" id="Seg_2093" s="T47">весь</ta>
            <ta e="T49" id="Seg_2094" s="T48">один.[NOM.SG]</ta>
            <ta e="T50" id="Seg_2095" s="T49">место-LOC</ta>
            <ta e="T51" id="Seg_2096" s="T50">кораль-EP-GEN</ta>
            <ta e="T52" id="Seg_2097" s="T51">дверь.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2098" s="T52">открыть-PTCP.PASS</ta>
            <ta e="T54" id="Seg_2099" s="T53">войти-PST-3PL</ta>
            <ta e="T55" id="Seg_2100" s="T54">закрыть-PTCP.PASS-LAT/LOC.3SG</ta>
            <ta e="T56" id="Seg_2101" s="T55">чум-LAT</ta>
            <ta e="T57" id="Seg_2102" s="T56">катиться-CVB</ta>
            <ta e="T58" id="Seg_2103" s="T57">прийти-PST.[3SG]</ta>
            <ta e="T59" id="Seg_2104" s="T58">только</ta>
            <ta e="T60" id="Seg_2105" s="T59">голова.[NOM.SG]</ta>
            <ta e="T61" id="Seg_2106" s="T60">кричать-PRS.[3SG]</ta>
            <ta e="T62" id="Seg_2107" s="T61">чум-EP-GEN</ta>
            <ta e="T63" id="Seg_2108" s="T62">дверь.[NOM.SG]</ta>
            <ta e="T64" id="Seg_2109" s="T63">открыть-DETR-MOM-PST.[3SG]</ta>
            <ta e="T65" id="Seg_2110" s="T64">катиться-CVB</ta>
            <ta e="T66" id="Seg_2111" s="T65">прийти-PST.[3SG]</ta>
            <ta e="T67" id="Seg_2112" s="T66">только</ta>
            <ta e="T68" id="Seg_2113" s="T67">голова.[NOM.SG]</ta>
            <ta e="T69" id="Seg_2114" s="T68">котел.[NOM.SG]</ta>
            <ta e="T70" id="Seg_2115" s="T69">вешать-IMP.2SG</ta>
            <ta e="T71" id="Seg_2116" s="T70">мясо-ADJZ.[NOM.SG]</ta>
            <ta e="T72" id="Seg_2117" s="T71">жир.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2118" s="T72">варить-RFL-IMP.2SG</ta>
            <ta e="T74" id="Seg_2119" s="T73">сказать-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_2120" s="T74">кто.[NOM.SG]=INDEF</ta>
            <ta e="T76" id="Seg_2121" s="T75">быть-PST.[3SG]</ta>
            <ta e="T77" id="Seg_2122" s="T76">этот-LOC</ta>
            <ta e="T78" id="Seg_2123" s="T77">встать-IMP.2SG</ta>
            <ta e="T79" id="Seg_2124" s="T78">здесь</ta>
            <ta e="T80" id="Seg_2125" s="T79">мужчина.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2126" s="T80">человек.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2127" s="T81">быть-PST-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_2128" s="T82">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T84" id="Seg_2129" s="T83">стать-FUT-3SG</ta>
            <ta e="T85" id="Seg_2130" s="T84">женщина.[NOM.SG]</ta>
            <ta e="T86" id="Seg_2131" s="T85">человек.[NOM.SG]</ta>
            <ta e="T87" id="Seg_2132" s="T86">быть-PST-LAT/LOC.3SG</ta>
            <ta e="T88" id="Seg_2133" s="T87">товарищ-NOM/GEN/ACC.1SG</ta>
            <ta e="T89" id="Seg_2134" s="T88">стать-FUT-3SG</ta>
            <ta e="T90" id="Seg_2135" s="T89">встать-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2136" s="T90">спрятаться-RES-PTCP</ta>
            <ta e="T92" id="Seg_2137" s="T91">женщина.[NOM.SG]</ta>
            <ta e="T93" id="Seg_2138" s="T92">огонь-GEN</ta>
            <ta e="T94" id="Seg_2139" s="T93">край-LAT/LOC.3SG</ta>
            <ta e="T95" id="Seg_2140" s="T94">сидеть-PST.[3SG]</ta>
            <ta e="T96" id="Seg_2141" s="T95">голова-INS</ta>
            <ta e="T97" id="Seg_2142" s="T96">есть-INF.LAT</ta>
            <ta e="T98" id="Seg_2143" s="T97">съесть-DES-PST.[3SG]</ta>
            <ta e="T99" id="Seg_2144" s="T98">говорить-CVB</ta>
            <ta e="T100" id="Seg_2145" s="T99">сидеть-3PL</ta>
            <ta e="T101" id="Seg_2146" s="T100">ты.NOM</ta>
            <ta e="T102" id="Seg_2147" s="T101">как</ta>
            <ta e="T103" id="Seg_2148" s="T102">идти-PRS-2SG</ta>
            <ta e="T104" id="Seg_2149" s="T103">только</ta>
            <ta e="T105" id="Seg_2150" s="T104">голова-INS</ta>
            <ta e="T107" id="Seg_2151" s="T105">рука-NOM/GEN/ACC.2SG=PTCL</ta>
            <ta e="T108" id="Seg_2152" s="T107">NEG.EX.[3SG]</ta>
            <ta e="T110" id="Seg_2153" s="T108">нога-NOM/GEN/ACC.2SG=PTCL</ta>
            <ta e="T111" id="Seg_2154" s="T110">NEG.EX.[3SG]</ta>
            <ta e="T112" id="Seg_2155" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_2156" s="T112">весь</ta>
            <ta e="T114" id="Seg_2157" s="T113">место-LOC</ta>
            <ta e="T115" id="Seg_2158" s="T114">идти-PRS-1SG</ta>
            <ta e="T116" id="Seg_2159" s="T115">ты.NOM</ta>
            <ta e="T117" id="Seg_2160" s="T116">берлога-LAT</ta>
            <ta e="T118" id="Seg_2161" s="T117">катиться-CVB</ta>
            <ta e="T119" id="Seg_2162" s="T118">мочь-FUT-2SG=QP</ta>
            <ta e="T120" id="Seg_2163" s="T119">мочь-FUT-1SG</ta>
            <ta e="T121" id="Seg_2164" s="T120">шагать-CVB</ta>
            <ta e="T122" id="Seg_2165" s="T121">сказать-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2166" s="T122">катиться-IMP.2SG</ta>
            <ta e="T124" id="Seg_2167" s="T123">пол-LAT</ta>
            <ta e="T125" id="Seg_2168" s="T124">сказать-PRS.[3SG]</ta>
            <ta e="T126" id="Seg_2169" s="T125">голова-NOM/GEN.3SG</ta>
            <ta e="T127" id="Seg_2170" s="T126">катиться-MOM-PST.[3SG]</ta>
            <ta e="T128" id="Seg_2171" s="T127">топор.[NOM.SG]</ta>
            <ta e="T129" id="Seg_2172" s="T128">взять-PST.[3SG]</ta>
            <ta e="T130" id="Seg_2173" s="T129">лопнуть-CVB</ta>
            <ta e="T131" id="Seg_2174" s="T130">ударить-MOM-PST.[3SG]</ta>
            <ta e="T132" id="Seg_2175" s="T131">мозг-ACC.3SG</ta>
            <ta e="T133" id="Seg_2176" s="T132">вынимать-PST.[3SG]</ta>
            <ta e="T134" id="Seg_2177" s="T133">варить-PST.[3SG]</ta>
            <ta e="T135" id="Seg_2178" s="T134">мясо-ADJZ.[NOM.SG]</ta>
            <ta e="T136" id="Seg_2179" s="T135">жир.[NOM.SG]</ta>
            <ta e="T137" id="Seg_2180" s="T136">взять-PST.[3SG]</ta>
            <ta e="T138" id="Seg_2181" s="T137">пузырь-LAT</ta>
            <ta e="T139" id="Seg_2182" s="T138">ползти-TR-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2183" s="T139">идти-MOM-PST.[3SG]</ta>
            <ta e="T141" id="Seg_2184" s="T140">мужчина-LAT/LOC.3SG</ta>
            <ta e="T142" id="Seg_2185" s="T141">пойти-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2186" s="T142">дымовое.отверстие-LAT</ta>
            <ta e="T144" id="Seg_2187" s="T143">влезать-PST.[3SG]</ta>
            <ta e="T145" id="Seg_2188" s="T144">палец-ACC.3SG</ta>
            <ta e="T146" id="Seg_2189" s="T145">мужчина.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2190" s="T146">помечать-CVB</ta>
            <ta e="T148" id="Seg_2191" s="T147">сидеть.[3SG]</ta>
            <ta e="T149" id="Seg_2192" s="T148">этот-ACC</ta>
            <ta e="T150" id="Seg_2193" s="T149">сегодня</ta>
            <ta e="T151" id="Seg_2194" s="T150">съесть-FUT-1SG</ta>
            <ta e="T152" id="Seg_2195" s="T151">другой-ACC</ta>
            <ta e="T153" id="Seg_2196" s="T152">завтра</ta>
            <ta e="T154" id="Seg_2197" s="T153">этот.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2198" s="T154">женщина.[NOM.SG]</ta>
            <ta e="T156" id="Seg_2199" s="T155">пузырь-NOM/GEN/ACC.3SG-ACC.3SG</ta>
            <ta e="T157" id="Seg_2200" s="T156">уронить-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_2201" s="T157">дымовое.отверстие-ABL</ta>
            <ta e="T159" id="Seg_2202" s="T158">молиться-DUR.[3SG]</ta>
            <ta e="T160" id="Seg_2203" s="T159">бог.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2204" s="T160">дать-PST.[3SG]</ta>
            <ta e="T162" id="Seg_2205" s="T161">еще</ta>
            <ta e="T163" id="Seg_2206" s="T162">может.быть</ta>
            <ta e="T164" id="Seg_2207" s="T163">дать-IMP-3SG</ta>
            <ta e="T165" id="Seg_2208" s="T164">еще</ta>
            <ta e="T166" id="Seg_2209" s="T165">я.LAT</ta>
            <ta e="T167" id="Seg_2210" s="T166">этот.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2211" s="T167">женщина.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2212" s="T168">камень-ACC</ta>
            <ta e="T170" id="Seg_2213" s="T169">ты.DAT</ta>
            <ta e="T171" id="Seg_2214" s="T170">дать-FUT-3SG</ta>
            <ta e="T172" id="Seg_2215" s="T171">бог.[NOM.SG]</ta>
            <ta e="T173" id="Seg_2216" s="T172">я.NOM</ta>
            <ta e="T174" id="Seg_2217" s="T173">дать-PST-1SG</ta>
            <ta e="T175" id="Seg_2218" s="T174">я.NOM</ta>
            <ta e="T176" id="Seg_2219" s="T175">ты.NOM-INS</ta>
            <ta e="T177" id="Seg_2220" s="T176">пойти-FUT-1SG=QP</ta>
            <ta e="T178" id="Seg_2221" s="T177">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T179" id="Seg_2222" s="T178">нести-RES-PST.[3SG]</ta>
            <ta e="T180" id="Seg_2223" s="T179">голова-GEN</ta>
            <ta e="T181" id="Seg_2224" s="T180">чум-LAT/LOC.3SG</ta>
            <ta e="T182" id="Seg_2225" s="T181">прийти-PST-3PL</ta>
            <ta e="T183" id="Seg_2226" s="T182">мясо.[NOM.SG]</ta>
            <ta e="T184" id="Seg_2227" s="T183">жир.[NOM.SG]</ta>
            <ta e="T185" id="Seg_2228" s="T184">варить-PST.[3SG]</ta>
            <ta e="T186" id="Seg_2229" s="T185">кормить-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2230" s="T186">этот.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2231" s="T187">мужчина.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2232" s="T188">пукать-DES-FRQ-PRS-1SG</ta>
            <ta e="T190" id="Seg_2233" s="T189">вставлять-IMP.2SG.O</ta>
            <ta e="T191" id="Seg_2234" s="T190">постель.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2235" s="T191">ты.NOM</ta>
            <ta e="T193" id="Seg_2236" s="T192">NEG.AUX-IMP.2SG</ta>
            <ta e="T194" id="Seg_2237" s="T193">пукать-FRQ-EP-CNG</ta>
            <ta e="T195" id="Seg_2238" s="T194">скот-PL-ACC</ta>
            <ta e="T196" id="Seg_2239" s="T195">пугать-FUT-2SG</ta>
            <ta e="T197" id="Seg_2240" s="T196">мужчина.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2241" s="T197">пукать-MOM-PST.[3SG]</ta>
            <ta e="T199" id="Seg_2242" s="T198">скот-PL-ACC</ta>
            <ta e="T200" id="Seg_2243" s="T199">скакать-TR.RES-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2244" s="T200">женщина.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2245" s="T201">молоко.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2246" s="T202">взять-PST.[3SG]</ta>
            <ta e="T204" id="Seg_2247" s="T203">брызгать-MOM-PST.[3SG]</ta>
            <ta e="T205" id="Seg_2248" s="T204">позади-NOM/GEN/ACC.3PL</ta>
            <ta e="T206" id="Seg_2249" s="T205">вдоль</ta>
            <ta e="T207" id="Seg_2250" s="T206">овца-PL</ta>
            <ta e="T208" id="Seg_2251" s="T207">коза.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2252" s="T208">стать-IMP-3SG</ta>
            <ta e="T210" id="Seg_2253" s="T209">лошадь-PL</ta>
            <ta e="T211" id="Seg_2254" s="T210">лось.[NOM.SG]</ta>
            <ta e="T212" id="Seg_2255" s="T211">стать-IMP-3SG</ta>
            <ta e="T213" id="Seg_2256" s="T212">корова.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2257" s="T213">олень.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2258" s="T214">стать-IMP-3SG</ta>
            <ta e="T216" id="Seg_2259" s="T215">этот.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2260" s="T216">женщина.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2261" s="T217">мужчина.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2262" s="T218">что-NOM/GEN/ACC.3PL=INDEF</ta>
            <ta e="T220" id="Seg_2263" s="T219">NEG.EX.[3SG]</ta>
            <ta e="T221" id="Seg_2264" s="T220">опять</ta>
            <ta e="T222" id="Seg_2265" s="T221">быть.голодным-DUR-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2266" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2267" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2268" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2269" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_2270" s="T4">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T6" id="Seg_2271" s="T5">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_2272" s="T6">adj-n:case</ta>
            <ta e="T8" id="Seg_2273" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_2274" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_2275" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_2276" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2277" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2278" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_2279" s="T13">n-n:case.poss</ta>
            <ta e="T15" id="Seg_2280" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_2281" s="T15">n-n:case.poss</ta>
            <ta e="T17" id="Seg_2282" s="T16">v-v:n.fin</ta>
            <ta e="T18" id="Seg_2283" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_2284" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_2285" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_2286" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_2287" s="T21">n-n:ins-n:case</ta>
            <ta e="T23" id="Seg_2288" s="T22">n-n:case.poss</ta>
            <ta e="T24" id="Seg_2289" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_2290" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_2291" s="T25">v-v&gt;v-v:n.fin</ta>
            <ta e="T27" id="Seg_2292" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2293" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_2294" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_2295" s="T29">conj</ta>
            <ta e="T31" id="Seg_2296" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2297" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2298" s="T32">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T34" id="Seg_2299" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_2300" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_2301" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2302" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_2303" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2304" s="T38">v-v&gt;v-v:pn</ta>
            <ta e="T40" id="Seg_2305" s="T39">v-v&gt;v-v:n.fin-v:(case.poss)</ta>
            <ta e="T41" id="Seg_2306" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_2307" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_2308" s="T42">v-v&gt;v-v:mood.pn</ta>
            <ta e="T44" id="Seg_2309" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_2310" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2311" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2312" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_2313" s="T47">quant</ta>
            <ta e="T49" id="Seg_2314" s="T48">num-n:case</ta>
            <ta e="T50" id="Seg_2315" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_2316" s="T50">n-n:ins-n:case</ta>
            <ta e="T52" id="Seg_2317" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2318" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_2319" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2320" s="T54">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T56" id="Seg_2321" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_2322" s="T56">v-v:n.fin</ta>
            <ta e="T58" id="Seg_2323" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_2324" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2325" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_2326" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2327" s="T61">n-n:ins-n:case</ta>
            <ta e="T63" id="Seg_2328" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_2329" s="T63">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_2330" s="T64">v-v:n.fin</ta>
            <ta e="T66" id="Seg_2331" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2332" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2333" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_2334" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_2335" s="T69">v-v:mood.pn</ta>
            <ta e="T71" id="Seg_2336" s="T70">n-n&gt;adj-n:case</ta>
            <ta e="T72" id="Seg_2337" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2338" s="T72">v-v&gt;v-v:mood.pn</ta>
            <ta e="T74" id="Seg_2339" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_2340" s="T74">que-n:case=ptcl</ta>
            <ta e="T76" id="Seg_2341" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2342" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_2343" s="T77">v-v:mood.pn</ta>
            <ta e="T79" id="Seg_2344" s="T78">adv</ta>
            <ta e="T80" id="Seg_2345" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2346" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2347" s="T81">v-v:tense-v:(case.poss)</ta>
            <ta e="T83" id="Seg_2348" s="T82">n-n:case.poss</ta>
            <ta e="T84" id="Seg_2349" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_2350" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_2351" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_2352" s="T86">v-v:tense-v:(case.poss)</ta>
            <ta e="T88" id="Seg_2353" s="T87">n-n:case.poss</ta>
            <ta e="T89" id="Seg_2354" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2355" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2356" s="T90">v-v&gt;v-v:n.fin</ta>
            <ta e="T92" id="Seg_2357" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_2358" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_2359" s="T93">n-n:case.poss</ta>
            <ta e="T95" id="Seg_2360" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_2361" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2362" s="T96">v-v:n.fin</ta>
            <ta e="T98" id="Seg_2363" s="T97">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2364" s="T98">v-v:n.fin</ta>
            <ta e="T100" id="Seg_2365" s="T99">v-v:pn</ta>
            <ta e="T101" id="Seg_2366" s="T100">pers</ta>
            <ta e="T102" id="Seg_2367" s="T101">que</ta>
            <ta e="T103" id="Seg_2368" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_2369" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_2370" s="T104">n-n:case</ta>
            <ta e="T107" id="Seg_2371" s="T105">n-n:case.poss=ptcl</ta>
            <ta e="T108" id="Seg_2372" s="T107">v-v:pn</ta>
            <ta e="T110" id="Seg_2373" s="T108">n-n:case.poss=ptcl</ta>
            <ta e="T111" id="Seg_2374" s="T110">v-v:pn</ta>
            <ta e="T112" id="Seg_2375" s="T111">pers</ta>
            <ta e="T113" id="Seg_2376" s="T112">quant</ta>
            <ta e="T114" id="Seg_2377" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2378" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_2379" s="T115">pers</ta>
            <ta e="T117" id="Seg_2380" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2381" s="T117">v-v:n.fin</ta>
            <ta e="T119" id="Seg_2382" s="T118">v-v:tense-v:pn-ptcl</ta>
            <ta e="T120" id="Seg_2383" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_2384" s="T120">v-v:n.fin</ta>
            <ta e="T122" id="Seg_2385" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2386" s="T122">v-v:mood.pn</ta>
            <ta e="T124" id="Seg_2387" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2388" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_2389" s="T125">n-n:case.poss</ta>
            <ta e="T127" id="Seg_2390" s="T126">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2391" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_2392" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2393" s="T129">v-v:n.fin</ta>
            <ta e="T131" id="Seg_2394" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2395" s="T131">n-n:case.poss</ta>
            <ta e="T133" id="Seg_2396" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2397" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_2398" s="T134">n-n&gt;adj-n:case</ta>
            <ta e="T136" id="Seg_2399" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_2400" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2401" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2402" s="T138">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_2403" s="T139">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_2404" s="T140">n-n:case.poss</ta>
            <ta e="T142" id="Seg_2405" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2406" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_2407" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_2408" s="T144">n-n:case.poss</ta>
            <ta e="T146" id="Seg_2409" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_2410" s="T146">v-v:n.fin</ta>
            <ta e="T148" id="Seg_2411" s="T147">v-v:pn</ta>
            <ta e="T149" id="Seg_2412" s="T148">dempro-n:case</ta>
            <ta e="T150" id="Seg_2413" s="T149">adv</ta>
            <ta e="T151" id="Seg_2414" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_2415" s="T151">adj-n:case</ta>
            <ta e="T153" id="Seg_2416" s="T152">adv</ta>
            <ta e="T154" id="Seg_2417" s="T153">dempro-n:case</ta>
            <ta e="T155" id="Seg_2418" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_2419" s="T155">n-n:case.poss-n:case.poss</ta>
            <ta e="T157" id="Seg_2420" s="T156">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_2421" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2422" s="T158">v-v&gt;v-v:pn</ta>
            <ta e="T160" id="Seg_2423" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2424" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2425" s="T161">adv</ta>
            <ta e="T163" id="Seg_2426" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_2427" s="T163">v-v:mood-v:pn</ta>
            <ta e="T165" id="Seg_2428" s="T164">adv</ta>
            <ta e="T166" id="Seg_2429" s="T165">pers</ta>
            <ta e="T167" id="Seg_2430" s="T166">dempro-n:case</ta>
            <ta e="T168" id="Seg_2431" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_2432" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2433" s="T169">pers</ta>
            <ta e="T171" id="Seg_2434" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_2435" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_2436" s="T172">pers</ta>
            <ta e="T174" id="Seg_2437" s="T173">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_2438" s="T174">pers</ta>
            <ta e="T176" id="Seg_2439" s="T175">pers-n:case</ta>
            <ta e="T177" id="Seg_2440" s="T176">v-v:tense-v:pn-ptcl</ta>
            <ta e="T178" id="Seg_2441" s="T177">v-v:mood-v:pn</ta>
            <ta e="T179" id="Seg_2442" s="T178">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_2443" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2444" s="T180">n-n:case.poss</ta>
            <ta e="T182" id="Seg_2445" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_2446" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_2447" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2448" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_2449" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_2450" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_2451" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_2452" s="T188">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_2453" s="T189">v-v:mood.pn</ta>
            <ta e="T191" id="Seg_2454" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_2455" s="T191">pers</ta>
            <ta e="T193" id="Seg_2456" s="T192">aux-v:mood.pn</ta>
            <ta e="T194" id="Seg_2457" s="T193">v-v&gt;v-v:ins-v:n.fin</ta>
            <ta e="T195" id="Seg_2458" s="T194">n-n:num-n:case</ta>
            <ta e="T196" id="Seg_2459" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2460" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_2461" s="T197">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_2462" s="T198">n-n:num-n:case</ta>
            <ta e="T200" id="Seg_2463" s="T199">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_2464" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2465" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2466" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2467" s="T203">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_2468" s="T204">adv-n:case.poss</ta>
            <ta e="T206" id="Seg_2469" s="T205">post</ta>
            <ta e="T207" id="Seg_2470" s="T206">n-n:num</ta>
            <ta e="T208" id="Seg_2471" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_2472" s="T208">v-v:mood-v:pn</ta>
            <ta e="T210" id="Seg_2473" s="T209">n-n:num</ta>
            <ta e="T211" id="Seg_2474" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_2475" s="T211">v-v:mood-v:pn</ta>
            <ta e="T213" id="Seg_2476" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_2477" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_2478" s="T214">v-v:mood-v:pn</ta>
            <ta e="T216" id="Seg_2479" s="T215">dempro-n:case</ta>
            <ta e="T217" id="Seg_2480" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_2481" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_2482" s="T218">que-n:case.poss=ptcl</ta>
            <ta e="T220" id="Seg_2483" s="T219">v-v:pn</ta>
            <ta e="T221" id="Seg_2484" s="T220">adv</ta>
            <ta e="T222" id="Seg_2485" s="T221">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2486" s="T0">n</ta>
            <ta e="T2" id="Seg_2487" s="T1">n</ta>
            <ta e="T3" id="Seg_2488" s="T2">v</ta>
            <ta e="T4" id="Seg_2489" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2490" s="T4">v</ta>
            <ta e="T6" id="Seg_2491" s="T5">v</ta>
            <ta e="T7" id="Seg_2492" s="T6">adj</ta>
            <ta e="T8" id="Seg_2493" s="T7">n</ta>
            <ta e="T9" id="Seg_2494" s="T8">v</ta>
            <ta e="T10" id="Seg_2495" s="T9">dempro</ta>
            <ta e="T11" id="Seg_2496" s="T10">n</ta>
            <ta e="T12" id="Seg_2497" s="T11">n</ta>
            <ta e="T13" id="Seg_2498" s="T12">v</ta>
            <ta e="T14" id="Seg_2499" s="T13">n</ta>
            <ta e="T15" id="Seg_2500" s="T14">v</ta>
            <ta e="T16" id="Seg_2501" s="T15">n</ta>
            <ta e="T17" id="Seg_2502" s="T16">v</ta>
            <ta e="T18" id="Seg_2503" s="T17">v</ta>
            <ta e="T19" id="Seg_2504" s="T18">v</ta>
            <ta e="T20" id="Seg_2505" s="T19">n</ta>
            <ta e="T21" id="Seg_2506" s="T20">v</ta>
            <ta e="T22" id="Seg_2507" s="T21">n</ta>
            <ta e="T23" id="Seg_2508" s="T22">n</ta>
            <ta e="T24" id="Seg_2509" s="T23">n</ta>
            <ta e="T25" id="Seg_2510" s="T24">n</ta>
            <ta e="T26" id="Seg_2511" s="T25">v</ta>
            <ta e="T27" id="Seg_2512" s="T26">v</ta>
            <ta e="T28" id="Seg_2513" s="T27">n</ta>
            <ta e="T29" id="Seg_2514" s="T28">v</ta>
            <ta e="T30" id="Seg_2515" s="T29">conj</ta>
            <ta e="T31" id="Seg_2516" s="T30">v</ta>
            <ta e="T32" id="Seg_2517" s="T31">v</ta>
            <ta e="T33" id="Seg_2518" s="T32">v</ta>
            <ta e="T34" id="Seg_2519" s="T33">n</ta>
            <ta e="T35" id="Seg_2520" s="T34">v</ta>
            <ta e="T36" id="Seg_2521" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2522" s="T36">n</ta>
            <ta e="T38" id="Seg_2523" s="T37">n</ta>
            <ta e="T39" id="Seg_2524" s="T38">v</ta>
            <ta e="T40" id="Seg_2525" s="T39">v</ta>
            <ta e="T41" id="Seg_2526" s="T40">v</ta>
            <ta e="T42" id="Seg_2527" s="T41">n</ta>
            <ta e="T43" id="Seg_2528" s="T42">v</ta>
            <ta e="T44" id="Seg_2529" s="T43">n</ta>
            <ta e="T45" id="Seg_2530" s="T44">n</ta>
            <ta e="T46" id="Seg_2531" s="T45">n</ta>
            <ta e="T47" id="Seg_2532" s="T46">n</ta>
            <ta e="T48" id="Seg_2533" s="T47">quant</ta>
            <ta e="T49" id="Seg_2534" s="T48">num</ta>
            <ta e="T50" id="Seg_2535" s="T49">n</ta>
            <ta e="T51" id="Seg_2536" s="T50">n</ta>
            <ta e="T52" id="Seg_2537" s="T51">n</ta>
            <ta e="T53" id="Seg_2538" s="T52">adj</ta>
            <ta e="T54" id="Seg_2539" s="T53">v</ta>
            <ta e="T55" id="Seg_2540" s="T54">adj</ta>
            <ta e="T56" id="Seg_2541" s="T55">n</ta>
            <ta e="T57" id="Seg_2542" s="T56">v</ta>
            <ta e="T58" id="Seg_2543" s="T57">v</ta>
            <ta e="T59" id="Seg_2544" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2545" s="T59">n</ta>
            <ta e="T61" id="Seg_2546" s="T60">v</ta>
            <ta e="T62" id="Seg_2547" s="T61">n</ta>
            <ta e="T63" id="Seg_2548" s="T62">n</ta>
            <ta e="T64" id="Seg_2549" s="T63">v</ta>
            <ta e="T65" id="Seg_2550" s="T64">v</ta>
            <ta e="T66" id="Seg_2551" s="T65">v</ta>
            <ta e="T67" id="Seg_2552" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2553" s="T67">n</ta>
            <ta e="T69" id="Seg_2554" s="T68">n</ta>
            <ta e="T70" id="Seg_2555" s="T69">v</ta>
            <ta e="T71" id="Seg_2556" s="T70">adj</ta>
            <ta e="T72" id="Seg_2557" s="T71">n</ta>
            <ta e="T73" id="Seg_2558" s="T72">v</ta>
            <ta e="T74" id="Seg_2559" s="T73">v</ta>
            <ta e="T75" id="Seg_2560" s="T74">que</ta>
            <ta e="T76" id="Seg_2561" s="T75">v</ta>
            <ta e="T77" id="Seg_2562" s="T76">dempro</ta>
            <ta e="T78" id="Seg_2563" s="T77">v</ta>
            <ta e="T79" id="Seg_2564" s="T78">adv</ta>
            <ta e="T80" id="Seg_2565" s="T79">n</ta>
            <ta e="T81" id="Seg_2566" s="T80">n</ta>
            <ta e="T82" id="Seg_2567" s="T81">v</ta>
            <ta e="T83" id="Seg_2568" s="T82">n</ta>
            <ta e="T84" id="Seg_2569" s="T83">v</ta>
            <ta e="T85" id="Seg_2570" s="T84">n</ta>
            <ta e="T86" id="Seg_2571" s="T85">n</ta>
            <ta e="T87" id="Seg_2572" s="T86">v</ta>
            <ta e="T88" id="Seg_2573" s="T87">n</ta>
            <ta e="T89" id="Seg_2574" s="T88">v</ta>
            <ta e="T90" id="Seg_2575" s="T89">v</ta>
            <ta e="T91" id="Seg_2576" s="T90">adj</ta>
            <ta e="T92" id="Seg_2577" s="T91">n</ta>
            <ta e="T93" id="Seg_2578" s="T92">n</ta>
            <ta e="T94" id="Seg_2579" s="T93">n</ta>
            <ta e="T95" id="Seg_2580" s="T94">v</ta>
            <ta e="T96" id="Seg_2581" s="T95">n</ta>
            <ta e="T97" id="Seg_2582" s="T96">v</ta>
            <ta e="T98" id="Seg_2583" s="T97">v</ta>
            <ta e="T99" id="Seg_2584" s="T98">v</ta>
            <ta e="T100" id="Seg_2585" s="T99">v</ta>
            <ta e="T101" id="Seg_2586" s="T100">pers</ta>
            <ta e="T102" id="Seg_2587" s="T101">que</ta>
            <ta e="T103" id="Seg_2588" s="T102">v</ta>
            <ta e="T104" id="Seg_2589" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_2590" s="T104">n</ta>
            <ta e="T107" id="Seg_2591" s="T105">ncl</ta>
            <ta e="T108" id="Seg_2592" s="T107">v</ta>
            <ta e="T110" id="Seg_2593" s="T108">n</ta>
            <ta e="T111" id="Seg_2594" s="T110">v</ta>
            <ta e="T112" id="Seg_2595" s="T111">pers</ta>
            <ta e="T113" id="Seg_2596" s="T112">quant</ta>
            <ta e="T114" id="Seg_2597" s="T113">n</ta>
            <ta e="T115" id="Seg_2598" s="T114">v</ta>
            <ta e="T116" id="Seg_2599" s="T115">pers</ta>
            <ta e="T117" id="Seg_2600" s="T116">n</ta>
            <ta e="T118" id="Seg_2601" s="T117">v</ta>
            <ta e="T119" id="Seg_2602" s="T118">v</ta>
            <ta e="T120" id="Seg_2603" s="T119">v</ta>
            <ta e="T121" id="Seg_2604" s="T120">v</ta>
            <ta e="T122" id="Seg_2605" s="T121">v</ta>
            <ta e="T123" id="Seg_2606" s="T122">v</ta>
            <ta e="T124" id="Seg_2607" s="T123">n</ta>
            <ta e="T125" id="Seg_2608" s="T124">v</ta>
            <ta e="T126" id="Seg_2609" s="T125">n</ta>
            <ta e="T127" id="Seg_2610" s="T126">v</ta>
            <ta e="T128" id="Seg_2611" s="T127">n</ta>
            <ta e="T129" id="Seg_2612" s="T128">v</ta>
            <ta e="T130" id="Seg_2613" s="T129">v</ta>
            <ta e="T131" id="Seg_2614" s="T130">v</ta>
            <ta e="T132" id="Seg_2615" s="T131">n</ta>
            <ta e="T133" id="Seg_2616" s="T132">v</ta>
            <ta e="T134" id="Seg_2617" s="T133">v</ta>
            <ta e="T135" id="Seg_2618" s="T134">adj</ta>
            <ta e="T136" id="Seg_2619" s="T135">n</ta>
            <ta e="T137" id="Seg_2620" s="T136">v</ta>
            <ta e="T138" id="Seg_2621" s="T137">n</ta>
            <ta e="T139" id="Seg_2622" s="T138">v</ta>
            <ta e="T140" id="Seg_2623" s="T139">v</ta>
            <ta e="T141" id="Seg_2624" s="T140">n</ta>
            <ta e="T142" id="Seg_2625" s="T141">v</ta>
            <ta e="T143" id="Seg_2626" s="T142">n</ta>
            <ta e="T144" id="Seg_2627" s="T143">v</ta>
            <ta e="T145" id="Seg_2628" s="T144">n</ta>
            <ta e="T146" id="Seg_2629" s="T145">n</ta>
            <ta e="T147" id="Seg_2630" s="T146">v</ta>
            <ta e="T148" id="Seg_2631" s="T147">v</ta>
            <ta e="T149" id="Seg_2632" s="T148">dempro</ta>
            <ta e="T150" id="Seg_2633" s="T149">adv</ta>
            <ta e="T151" id="Seg_2634" s="T150">v</ta>
            <ta e="T152" id="Seg_2635" s="T151">adj</ta>
            <ta e="T153" id="Seg_2636" s="T152">adv</ta>
            <ta e="T154" id="Seg_2637" s="T153">dempro</ta>
            <ta e="T155" id="Seg_2638" s="T154">n</ta>
            <ta e="T156" id="Seg_2639" s="T155">n</ta>
            <ta e="T157" id="Seg_2640" s="T156">v</ta>
            <ta e="T158" id="Seg_2641" s="T157">n</ta>
            <ta e="T159" id="Seg_2642" s="T158">v</ta>
            <ta e="T160" id="Seg_2643" s="T159">n</ta>
            <ta e="T161" id="Seg_2644" s="T160">v</ta>
            <ta e="T162" id="Seg_2645" s="T161">adv</ta>
            <ta e="T163" id="Seg_2646" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_2647" s="T163">v</ta>
            <ta e="T165" id="Seg_2648" s="T164">adv</ta>
            <ta e="T166" id="Seg_2649" s="T165">pers</ta>
            <ta e="T167" id="Seg_2650" s="T166">dempro</ta>
            <ta e="T168" id="Seg_2651" s="T167">n</ta>
            <ta e="T169" id="Seg_2652" s="T168">n</ta>
            <ta e="T170" id="Seg_2653" s="T169">pers</ta>
            <ta e="T171" id="Seg_2654" s="T170">v</ta>
            <ta e="T172" id="Seg_2655" s="T171">n</ta>
            <ta e="T173" id="Seg_2656" s="T172">pers</ta>
            <ta e="T174" id="Seg_2657" s="T173">v</ta>
            <ta e="T175" id="Seg_2658" s="T174">pers</ta>
            <ta e="T176" id="Seg_2659" s="T175">pers</ta>
            <ta e="T177" id="Seg_2660" s="T176">v</ta>
            <ta e="T178" id="Seg_2661" s="T177">v</ta>
            <ta e="T179" id="Seg_2662" s="T178">v</ta>
            <ta e="T180" id="Seg_2663" s="T179">n</ta>
            <ta e="T181" id="Seg_2664" s="T180">n</ta>
            <ta e="T182" id="Seg_2665" s="T181">v</ta>
            <ta e="T183" id="Seg_2666" s="T182">n</ta>
            <ta e="T184" id="Seg_2667" s="T183">n</ta>
            <ta e="T185" id="Seg_2668" s="T184">v</ta>
            <ta e="T186" id="Seg_2669" s="T185">v</ta>
            <ta e="T187" id="Seg_2670" s="T186">dempro</ta>
            <ta e="T188" id="Seg_2671" s="T187">n</ta>
            <ta e="T189" id="Seg_2672" s="T188">v</ta>
            <ta e="T190" id="Seg_2673" s="T189">v</ta>
            <ta e="T191" id="Seg_2674" s="T190">n</ta>
            <ta e="T192" id="Seg_2675" s="T191">pers</ta>
            <ta e="T193" id="Seg_2676" s="T192">aux</ta>
            <ta e="T194" id="Seg_2677" s="T193">v</ta>
            <ta e="T195" id="Seg_2678" s="T194">n</ta>
            <ta e="T196" id="Seg_2679" s="T195">v</ta>
            <ta e="T197" id="Seg_2680" s="T196">n</ta>
            <ta e="T198" id="Seg_2681" s="T197">v</ta>
            <ta e="T199" id="Seg_2682" s="T198">n</ta>
            <ta e="T200" id="Seg_2683" s="T199">v</ta>
            <ta e="T201" id="Seg_2684" s="T200">n</ta>
            <ta e="T202" id="Seg_2685" s="T201">n</ta>
            <ta e="T203" id="Seg_2686" s="T202">v</ta>
            <ta e="T204" id="Seg_2687" s="T203">v</ta>
            <ta e="T205" id="Seg_2688" s="T204">adv</ta>
            <ta e="T206" id="Seg_2689" s="T205">post</ta>
            <ta e="T207" id="Seg_2690" s="T206">n</ta>
            <ta e="T208" id="Seg_2691" s="T207">n</ta>
            <ta e="T209" id="Seg_2692" s="T208">v</ta>
            <ta e="T210" id="Seg_2693" s="T209">n</ta>
            <ta e="T211" id="Seg_2694" s="T210">n</ta>
            <ta e="T212" id="Seg_2695" s="T211">v</ta>
            <ta e="T213" id="Seg_2696" s="T212">n</ta>
            <ta e="T214" id="Seg_2697" s="T213">n</ta>
            <ta e="T215" id="Seg_2698" s="T214">v</ta>
            <ta e="T216" id="Seg_2699" s="T215">dempro</ta>
            <ta e="T217" id="Seg_2700" s="T216">n</ta>
            <ta e="T218" id="Seg_2701" s="T217">n</ta>
            <ta e="T219" id="Seg_2702" s="T218">que</ta>
            <ta e="T220" id="Seg_2703" s="T219">v</ta>
            <ta e="T221" id="Seg_2704" s="T220">adv</ta>
            <ta e="T222" id="Seg_2705" s="T221">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2706" s="T0">np.h:Th</ta>
            <ta e="T2" id="Seg_2707" s="T1">np.h:Com</ta>
            <ta e="T4" id="Seg_2708" s="T3">pro.h:Poss</ta>
            <ta e="T8" id="Seg_2709" s="T7">np:P 0.3.h:Poss</ta>
            <ta e="T9" id="Seg_2710" s="T8">0.3.h:A</ta>
            <ta e="T11" id="Seg_2711" s="T10">np.h:A</ta>
            <ta e="T12" id="Seg_2712" s="T11">np:Th</ta>
            <ta e="T14" id="Seg_2713" s="T13">np.h:P 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_2714" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_2715" s="T15">np.h:A 0.3.h:Poss</ta>
            <ta e="T19" id="Seg_2716" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_2717" s="T19">np:Th</ta>
            <ta e="T23" id="Seg_2718" s="T21">pp:L</ta>
            <ta e="T24" id="Seg_2719" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_2720" s="T24">np:Th</ta>
            <ta e="T28" id="Seg_2721" s="T27">np.h:A</ta>
            <ta e="T31" id="Seg_2722" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_2723" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_2724" s="T32">0.3.h:E</ta>
            <ta e="T34" id="Seg_2725" s="T33">np:Th</ta>
            <ta e="T35" id="Seg_2726" s="T34">0.3.h:E</ta>
            <ta e="T37" id="Seg_2727" s="T36">np.h:A</ta>
            <ta e="T38" id="Seg_2728" s="T37">np:P</ta>
            <ta e="T40" id="Seg_2729" s="T39">0.3.h:A</ta>
            <ta e="T41" id="Seg_2730" s="T40">0.3.h:A</ta>
            <ta e="T43" id="Seg_2731" s="T42">0.2.h:A</ta>
            <ta e="T44" id="Seg_2732" s="T43">np:A</ta>
            <ta e="T45" id="Seg_2733" s="T44">np:A</ta>
            <ta e="T46" id="Seg_2734" s="T45">np:A</ta>
            <ta e="T47" id="Seg_2735" s="T46">np:A</ta>
            <ta e="T50" id="Seg_2736" s="T49">np:L</ta>
            <ta e="T51" id="Seg_2737" s="T50">np:Poss</ta>
            <ta e="T52" id="Seg_2738" s="T51">np:Th</ta>
            <ta e="T55" id="Seg_2739" s="T54">0.3:A</ta>
            <ta e="T56" id="Seg_2740" s="T55">np:G</ta>
            <ta e="T58" id="Seg_2741" s="T57">0.3.h:A</ta>
            <ta e="T60" id="Seg_2742" s="T59">np.h:A</ta>
            <ta e="T62" id="Seg_2743" s="T61">np:Poss</ta>
            <ta e="T63" id="Seg_2744" s="T62">np:Th</ta>
            <ta e="T68" id="Seg_2745" s="T67">np.h:A</ta>
            <ta e="T69" id="Seg_2746" s="T68">np:Th</ta>
            <ta e="T70" id="Seg_2747" s="T69">0.2.h:A</ta>
            <ta e="T72" id="Seg_2748" s="T71">np:Th</ta>
            <ta e="T73" id="Seg_2749" s="T72">0.2.h:A</ta>
            <ta e="T74" id="Seg_2750" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_2751" s="T74">pro.h:Th</ta>
            <ta e="T77" id="Seg_2752" s="T76">adv:L</ta>
            <ta e="T78" id="Seg_2753" s="T77">0.2.h:A</ta>
            <ta e="T79" id="Seg_2754" s="T78">adv:G</ta>
            <ta e="T81" id="Seg_2755" s="T80">np.h:Th</ta>
            <ta e="T82" id="Seg_2756" s="T81">0.3.h:Th</ta>
            <ta e="T83" id="Seg_2757" s="T82">np.h:Th 0.1.h:Poss</ta>
            <ta e="T86" id="Seg_2758" s="T85">np.h:Th</ta>
            <ta e="T87" id="Seg_2759" s="T86">0.3.h:Th</ta>
            <ta e="T88" id="Seg_2760" s="T87">np.h:Th 0.1.h:Poss</ta>
            <ta e="T92" id="Seg_2761" s="T91">np.h:A</ta>
            <ta e="T93" id="Seg_2762" s="T92">np:Poss</ta>
            <ta e="T94" id="Seg_2763" s="T93">np:G</ta>
            <ta e="T95" id="Seg_2764" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_2765" s="T95">np.h:Com</ta>
            <ta e="T98" id="Seg_2766" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_2767" s="T98">0.3.h:A</ta>
            <ta e="T100" id="Seg_2768" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_2769" s="T100">pro.h:A</ta>
            <ta e="T105" id="Seg_2770" s="T104">np:Ins</ta>
            <ta e="T107" id="Seg_2771" s="T105">np:Th 0.2.h:Poss</ta>
            <ta e="T110" id="Seg_2772" s="T108">np:Th 0.2.h:Poss</ta>
            <ta e="T112" id="Seg_2773" s="T111">pro.h:A</ta>
            <ta e="T114" id="Seg_2774" s="T113">np:L</ta>
            <ta e="T116" id="Seg_2775" s="T115">pro.h:A</ta>
            <ta e="T117" id="Seg_2776" s="T116">np:G</ta>
            <ta e="T120" id="Seg_2777" s="T119">0.1.h:A</ta>
            <ta e="T122" id="Seg_2778" s="T121">0.3.h:A</ta>
            <ta e="T123" id="Seg_2779" s="T122">0.2.h:A</ta>
            <ta e="T124" id="Seg_2780" s="T123">np:G</ta>
            <ta e="T125" id="Seg_2781" s="T124">0.3.h:A</ta>
            <ta e="T126" id="Seg_2782" s="T125">np.h:A 0.3.h:Poss</ta>
            <ta e="T128" id="Seg_2783" s="T127">np:Th</ta>
            <ta e="T129" id="Seg_2784" s="T128">0.3.h:A</ta>
            <ta e="T131" id="Seg_2785" s="T130">0.3.h:A</ta>
            <ta e="T132" id="Seg_2786" s="T131">np:P 0.3.h:Poss</ta>
            <ta e="T133" id="Seg_2787" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_2788" s="T133">0.3.h:A</ta>
            <ta e="T136" id="Seg_2789" s="T135">np:Th</ta>
            <ta e="T137" id="Seg_2790" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_2791" s="T137">np:G</ta>
            <ta e="T139" id="Seg_2792" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_2793" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_2794" s="T140">np:G 0.3.h:Poss</ta>
            <ta e="T142" id="Seg_2795" s="T141">0.3.h:A</ta>
            <ta e="T143" id="Seg_2796" s="T142">np:G</ta>
            <ta e="T144" id="Seg_2797" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_2798" s="T144">np:P 0.3.h:Poss</ta>
            <ta e="T146" id="Seg_2799" s="T145">np.h:A</ta>
            <ta e="T148" id="Seg_2800" s="T147">0.3.h:A</ta>
            <ta e="T149" id="Seg_2801" s="T148">pro:P</ta>
            <ta e="T150" id="Seg_2802" s="T149">adv:Time</ta>
            <ta e="T151" id="Seg_2803" s="T150">0.1.h:A</ta>
            <ta e="T152" id="Seg_2804" s="T151">pro:P</ta>
            <ta e="T153" id="Seg_2805" s="T152">adv:Time</ta>
            <ta e="T155" id="Seg_2806" s="T154">np.h:A</ta>
            <ta e="T156" id="Seg_2807" s="T155">np:P 0.3.h:Poss</ta>
            <ta e="T159" id="Seg_2808" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_2809" s="T159">np.h:A</ta>
            <ta e="T164" id="Seg_2810" s="T163">0.3.h:A</ta>
            <ta e="T166" id="Seg_2811" s="T165">pro.h:R</ta>
            <ta e="T168" id="Seg_2812" s="T167">np.h:Th</ta>
            <ta e="T169" id="Seg_2813" s="T168">np:Th</ta>
            <ta e="T170" id="Seg_2814" s="T169">pro.h:R</ta>
            <ta e="T172" id="Seg_2815" s="T171">np.h:A</ta>
            <ta e="T173" id="Seg_2816" s="T172">pro.h:A</ta>
            <ta e="T175" id="Seg_2817" s="T174">pro.h:A</ta>
            <ta e="T176" id="Seg_2818" s="T175">pro.h:Com</ta>
            <ta e="T178" id="Seg_2819" s="T177">0.1.h:A</ta>
            <ta e="T179" id="Seg_2820" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_2821" s="T179">np.h:Poss</ta>
            <ta e="T181" id="Seg_2822" s="T180">np:G</ta>
            <ta e="T182" id="Seg_2823" s="T181">0.3.h:A</ta>
            <ta e="T183" id="Seg_2824" s="T182">np:P</ta>
            <ta e="T184" id="Seg_2825" s="T183">np:P</ta>
            <ta e="T185" id="Seg_2826" s="T184">0.3.h:A</ta>
            <ta e="T186" id="Seg_2827" s="T185">0.3.h:A</ta>
            <ta e="T188" id="Seg_2828" s="T187">np.h:Th</ta>
            <ta e="T189" id="Seg_2829" s="T188">0.1.h:E</ta>
            <ta e="T190" id="Seg_2830" s="T189">0.2.h:A np:Th</ta>
            <ta e="T191" id="Seg_2831" s="T190">np:G</ta>
            <ta e="T192" id="Seg_2832" s="T191">pro.h:A</ta>
            <ta e="T193" id="Seg_2833" s="T192">0.2.h:A</ta>
            <ta e="T195" id="Seg_2834" s="T194">np:E</ta>
            <ta e="T196" id="Seg_2835" s="T195">0.2.h:A</ta>
            <ta e="T197" id="Seg_2836" s="T196">np.h:A</ta>
            <ta e="T199" id="Seg_2837" s="T198">np:E</ta>
            <ta e="T200" id="Seg_2838" s="T199">0.3.h:A</ta>
            <ta e="T201" id="Seg_2839" s="T200">np.h:A</ta>
            <ta e="T202" id="Seg_2840" s="T201">np:Th</ta>
            <ta e="T204" id="Seg_2841" s="T203">0.3.h:A</ta>
            <ta e="T205" id="Seg_2842" s="T204">adv:Pth</ta>
            <ta e="T207" id="Seg_2843" s="T206">np:Th</ta>
            <ta e="T208" id="Seg_2844" s="T207">np:Th</ta>
            <ta e="T210" id="Seg_2845" s="T209">np:Th</ta>
            <ta e="T211" id="Seg_2846" s="T210">np:Th</ta>
            <ta e="T213" id="Seg_2847" s="T212">np:Th</ta>
            <ta e="T214" id="Seg_2848" s="T213">np:Th</ta>
            <ta e="T217" id="Seg_2849" s="T216">np.h:E</ta>
            <ta e="T218" id="Seg_2850" s="T217">np.h:E</ta>
            <ta e="T219" id="Seg_2851" s="T218">pro:Th</ta>
            <ta e="T221" id="Seg_2852" s="T220">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2853" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_2854" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_2855" s="T4">np:S</ta>
            <ta e="T8" id="Seg_2856" s="T7">np:O</ta>
            <ta e="T9" id="Seg_2857" s="T8">v:pred 0.3.h:S</ta>
            <ta e="T11" id="Seg_2858" s="T10">np:S</ta>
            <ta e="T12" id="Seg_2859" s="T11">np:O</ta>
            <ta e="T13" id="Seg_2860" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_2861" s="T13">np.h:O</ta>
            <ta e="T15" id="Seg_2862" s="T14">v:pred 0.3.h:S</ta>
            <ta e="T16" id="Seg_2863" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_2864" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_2865" s="T18">v:pred 0.3.h:S</ta>
            <ta e="T20" id="Seg_2866" s="T19">np:S</ta>
            <ta e="T21" id="Seg_2867" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_2868" s="T23">np:S</ta>
            <ta e="T25" id="Seg_2869" s="T24">np:S</ta>
            <ta e="T27" id="Seg_2870" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_2871" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_2872" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_2873" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_2874" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_2875" s="T32">s:temp</ta>
            <ta e="T35" id="Seg_2876" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_2877" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_2878" s="T37">np:O</ta>
            <ta e="T39" id="Seg_2879" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_2880" s="T39">s:temp</ta>
            <ta e="T41" id="Seg_2881" s="T40">v:pred 0.3.h:S</ta>
            <ta e="T43" id="Seg_2882" s="T42">v:pred 0.2:S</ta>
            <ta e="T44" id="Seg_2883" s="T43">np:S</ta>
            <ta e="T45" id="Seg_2884" s="T44">np:S</ta>
            <ta e="T46" id="Seg_2885" s="T45">np:S</ta>
            <ta e="T47" id="Seg_2886" s="T46">np:S</ta>
            <ta e="T53" id="Seg_2887" s="T50">s:temp</ta>
            <ta e="T54" id="Seg_2888" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_2889" s="T54">s:temp</ta>
            <ta e="T58" id="Seg_2890" s="T57">v:pred 0.3:S</ta>
            <ta e="T60" id="Seg_2891" s="T59">np:S</ta>
            <ta e="T61" id="Seg_2892" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_2893" s="T62">np:S</ta>
            <ta e="T64" id="Seg_2894" s="T63">v:pred</ta>
            <ta e="T66" id="Seg_2895" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_2896" s="T67">np:S</ta>
            <ta e="T70" id="Seg_2897" s="T69">v:pred 0.2:S</ta>
            <ta e="T73" id="Seg_2898" s="T72">v:pred 0.2:S</ta>
            <ta e="T74" id="Seg_2899" s="T73">v:pred 0.3:S</ta>
            <ta e="T75" id="Seg_2900" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_2901" s="T75">v:pred</ta>
            <ta e="T78" id="Seg_2902" s="T77">v:pred 0.2.h:S</ta>
            <ta e="T82" id="Seg_2903" s="T79">s:cond</ta>
            <ta e="T83" id="Seg_2904" s="T82">n:pred</ta>
            <ta e="T84" id="Seg_2905" s="T83">cop</ta>
            <ta e="T87" id="Seg_2906" s="T84">s:cond</ta>
            <ta e="T88" id="Seg_2907" s="T87">n:pred</ta>
            <ta e="T89" id="Seg_2908" s="T88">cop</ta>
            <ta e="T90" id="Seg_2909" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_2910" s="T90">s:adv</ta>
            <ta e="T92" id="Seg_2911" s="T91">np.h:S</ta>
            <ta e="T95" id="Seg_2912" s="T94">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_2913" s="T95">s:purp</ta>
            <ta e="T98" id="Seg_2914" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T99" id="Seg_2915" s="T98">s:adv</ta>
            <ta e="T100" id="Seg_2916" s="T99">v:pred 0.3.h:S</ta>
            <ta e="T101" id="Seg_2917" s="T100">pron.h:S</ta>
            <ta e="T103" id="Seg_2918" s="T102">v:pred</ta>
            <ta e="T107" id="Seg_2919" s="T105">np:S</ta>
            <ta e="T108" id="Seg_2920" s="T107">v:pred</ta>
            <ta e="T110" id="Seg_2921" s="T108">np:S</ta>
            <ta e="T111" id="Seg_2922" s="T110">v:pred</ta>
            <ta e="T112" id="Seg_2923" s="T111">pro.h:S</ta>
            <ta e="T115" id="Seg_2924" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_2925" s="T115">pro.h:S</ta>
            <ta e="T118" id="Seg_2926" s="T117">s:adv</ta>
            <ta e="T119" id="Seg_2927" s="T118">v:pred</ta>
            <ta e="T120" id="Seg_2928" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_2929" s="T120">s:adv</ta>
            <ta e="T122" id="Seg_2930" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_2931" s="T122">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_2932" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_2933" s="T125">np.h:S</ta>
            <ta e="T127" id="Seg_2934" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_2935" s="T127">np:O</ta>
            <ta e="T129" id="Seg_2936" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T131" id="Seg_2937" s="T130">v:pred 0.3.h:S</ta>
            <ta e="T132" id="Seg_2938" s="T131">np:O</ta>
            <ta e="T133" id="Seg_2939" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_2940" s="T133">v:pred 0.3.h:S</ta>
            <ta e="T136" id="Seg_2941" s="T135">np:O</ta>
            <ta e="T137" id="Seg_2942" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_2943" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_2944" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T142" id="Seg_2945" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_2946" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_2947" s="T144">np:O</ta>
            <ta e="T146" id="Seg_2948" s="T145">np.h:S</ta>
            <ta e="T147" id="Seg_2949" s="T146">s:adv</ta>
            <ta e="T148" id="Seg_2950" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_2951" s="T148">pro:O</ta>
            <ta e="T151" id="Seg_2952" s="T150">v:pred 0.1.h:S</ta>
            <ta e="T152" id="Seg_2953" s="T151">pro:O</ta>
            <ta e="T155" id="Seg_2954" s="T154">np.h:S</ta>
            <ta e="T156" id="Seg_2955" s="T155">np:O</ta>
            <ta e="T157" id="Seg_2956" s="T156">v:pred</ta>
            <ta e="T159" id="Seg_2957" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_2958" s="T159">np.h:S</ta>
            <ta e="T161" id="Seg_2959" s="T160">v:pred</ta>
            <ta e="T164" id="Seg_2960" s="T163">v:pred 0.3:S</ta>
            <ta e="T168" id="Seg_2961" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_2962" s="T168">np:O</ta>
            <ta e="T171" id="Seg_2963" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_2964" s="T171">np.h:S</ta>
            <ta e="T173" id="Seg_2965" s="T172">pro.h:S</ta>
            <ta e="T174" id="Seg_2966" s="T173">v:pred</ta>
            <ta e="T175" id="Seg_2967" s="T174">pro.h:S</ta>
            <ta e="T177" id="Seg_2968" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_2969" s="T177">v:pred 0.1.h:S</ta>
            <ta e="T179" id="Seg_2970" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T182" id="Seg_2971" s="T181">v:pred 0.3.h:S</ta>
            <ta e="T183" id="Seg_2972" s="T182">np:O</ta>
            <ta e="T184" id="Seg_2973" s="T183">np:O</ta>
            <ta e="T185" id="Seg_2974" s="T184">v:pred 0.3.h:S</ta>
            <ta e="T186" id="Seg_2975" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T188" id="Seg_2976" s="T187">np.h:S</ta>
            <ta e="T189" id="Seg_2977" s="T188">v:pred 0.1.h:S</ta>
            <ta e="T190" id="Seg_2978" s="T189">v:pred 0.2.h:S</ta>
            <ta e="T192" id="Seg_2979" s="T191">pro.h:S</ta>
            <ta e="T193" id="Seg_2980" s="T192">v:pred</ta>
            <ta e="T197" id="Seg_2981" s="T196">np.h:S</ta>
            <ta e="T198" id="Seg_2982" s="T197">v:pred</ta>
            <ta e="T199" id="Seg_2983" s="T198">np:O</ta>
            <ta e="T200" id="Seg_2984" s="T199">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_2985" s="T200">np.h:S</ta>
            <ta e="T202" id="Seg_2986" s="T201">np:O</ta>
            <ta e="T203" id="Seg_2987" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_2988" s="T203">v:pred 0.3.h:S</ta>
            <ta e="T207" id="Seg_2989" s="T206">np:S</ta>
            <ta e="T209" id="Seg_2990" s="T208">v:pred</ta>
            <ta e="T210" id="Seg_2991" s="T209">np:S</ta>
            <ta e="T212" id="Seg_2992" s="T211">v:pred</ta>
            <ta e="T213" id="Seg_2993" s="T212">np:S</ta>
            <ta e="T215" id="Seg_2994" s="T214">v:pred</ta>
            <ta e="T217" id="Seg_2995" s="T216">np.h:S</ta>
            <ta e="T218" id="Seg_2996" s="T217">np.h:S</ta>
            <ta e="T219" id="Seg_2997" s="T218">pro:S</ta>
            <ta e="T220" id="Seg_2998" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_2999" s="T221">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_3000" s="T0">new</ta>
            <ta e="T2" id="Seg_3001" s="T1">new</ta>
            <ta e="T5" id="Seg_3002" s="T4">accs-gen</ta>
            <ta e="T8" id="Seg_3003" s="T7">new</ta>
            <ta e="T9" id="Seg_3004" s="T8">0.giv-active</ta>
            <ta e="T11" id="Seg_3005" s="T10">giv-inactive</ta>
            <ta e="T12" id="Seg_3006" s="T11">new</ta>
            <ta e="T14" id="Seg_3007" s="T13">giv-inactive</ta>
            <ta e="T15" id="Seg_3008" s="T14">0.giv-active</ta>
            <ta e="T16" id="Seg_3009" s="T15">giv-active</ta>
            <ta e="T19" id="Seg_3010" s="T18">0.giv-active</ta>
            <ta e="T20" id="Seg_3011" s="T19">new</ta>
            <ta e="T23" id="Seg_3012" s="T22">accs-inf</ta>
            <ta e="T24" id="Seg_3013" s="T23">new</ta>
            <ta e="T25" id="Seg_3014" s="T24">new</ta>
            <ta e="T28" id="Seg_3015" s="T27">giv-inactive</ta>
            <ta e="T31" id="Seg_3016" s="T30">0.giv-active</ta>
            <ta e="T32" id="Seg_3017" s="T31">0.giv-active</ta>
            <ta e="T33" id="Seg_3018" s="T32">0.giv-active</ta>
            <ta e="T34" id="Seg_3019" s="T33">new</ta>
            <ta e="T35" id="Seg_3020" s="T34">0.giv-active</ta>
            <ta e="T37" id="Seg_3021" s="T36">new</ta>
            <ta e="T38" id="Seg_3022" s="T37">new</ta>
            <ta e="T40" id="Seg_3023" s="T39">0.giv-active</ta>
            <ta e="T41" id="Seg_3024" s="T40">quot-sp</ta>
            <ta e="T42" id="Seg_3025" s="T41">new-Q</ta>
            <ta e="T44" id="Seg_3026" s="T43">giv-inactive</ta>
            <ta e="T45" id="Seg_3027" s="T44">accs-inf</ta>
            <ta e="T46" id="Seg_3028" s="T45">accs-inf</ta>
            <ta e="T47" id="Seg_3029" s="T46">accs-inf</ta>
            <ta e="T51" id="Seg_3030" s="T50">accs-inf</ta>
            <ta e="T52" id="Seg_3031" s="T51">accs-inf</ta>
            <ta e="T55" id="Seg_3032" s="T54">0.giv-active</ta>
            <ta e="T56" id="Seg_3033" s="T55">giv-inactive</ta>
            <ta e="T58" id="Seg_3034" s="T57">0.giv-inactive</ta>
            <ta e="T60" id="Seg_3035" s="T59">giv-active</ta>
            <ta e="T63" id="Seg_3036" s="T62">accs-inf</ta>
            <ta e="T68" id="Seg_3037" s="T67">giv-active</ta>
            <ta e="T69" id="Seg_3038" s="T68">accs-sit-Q</ta>
            <ta e="T71" id="Seg_3039" s="T70">giv-inactive-Q</ta>
            <ta e="T72" id="Seg_3040" s="T71">giv-inactive-Q</ta>
            <ta e="T74" id="Seg_3041" s="T73">0.giv-active quot-sp</ta>
            <ta e="T81" id="Seg_3042" s="T79">accs-gen-Q</ta>
            <ta e="T83" id="Seg_3043" s="T82">accs-gen-Q</ta>
            <ta e="T86" id="Seg_3044" s="T84">accs-gen-Q</ta>
            <ta e="T88" id="Seg_3045" s="T87">accs-gen-Q</ta>
            <ta e="T92" id="Seg_3046" s="T91">giv-inactive</ta>
            <ta e="T94" id="Seg_3047" s="T93">accs-sit</ta>
            <ta e="T95" id="Seg_3048" s="T94">0.giv-active</ta>
            <ta e="T96" id="Seg_3049" s="T95">giv-inactive</ta>
            <ta e="T98" id="Seg_3050" s="T97">0.giv-active</ta>
            <ta e="T99" id="Seg_3051" s="T98">quot-sp</ta>
            <ta e="T100" id="Seg_3052" s="T99">0.accs-aggr</ta>
            <ta e="T101" id="Seg_3053" s="T100">giv-inactive-Q</ta>
            <ta e="T105" id="Seg_3054" s="T104">accs-inf-Q</ta>
            <ta e="T107" id="Seg_3055" s="T105">accs-inf-Q</ta>
            <ta e="T110" id="Seg_3056" s="T108">accs-inf-Q</ta>
            <ta e="T112" id="Seg_3057" s="T111">giv-active-Q</ta>
            <ta e="T116" id="Seg_3058" s="T115">giv-active-Q</ta>
            <ta e="T117" id="Seg_3059" s="T116">new-Q</ta>
            <ta e="T120" id="Seg_3060" s="T119">0.giv-active-Q</ta>
            <ta e="T122" id="Seg_3061" s="T121">0.giv-active quot-sp</ta>
            <ta e="T123" id="Seg_3062" s="T122">0.giv-active-Q</ta>
            <ta e="T124" id="Seg_3063" s="T123">accs-gen-Q</ta>
            <ta e="T125" id="Seg_3064" s="T124">0.giv-inactive quot-sp</ta>
            <ta e="T126" id="Seg_3065" s="T125">giv-inactive</ta>
            <ta e="T128" id="Seg_3066" s="T127">new</ta>
            <ta e="T129" id="Seg_3067" s="T128">0.giv-inactive</ta>
            <ta e="T131" id="Seg_3068" s="T130">0.giv-active</ta>
            <ta e="T132" id="Seg_3069" s="T131">accs-inf</ta>
            <ta e="T133" id="Seg_3070" s="T132">0.giv-active</ta>
            <ta e="T134" id="Seg_3071" s="T133">0.giv-active</ta>
            <ta e="T136" id="Seg_3072" s="T135">new</ta>
            <ta e="T137" id="Seg_3073" s="T136">0.giv-active</ta>
            <ta e="T138" id="Seg_3074" s="T137">new</ta>
            <ta e="T139" id="Seg_3075" s="T138">0.giv-active</ta>
            <ta e="T140" id="Seg_3076" s="T139">0.giv-active</ta>
            <ta e="T141" id="Seg_3077" s="T140">giv-inactive</ta>
            <ta e="T142" id="Seg_3078" s="T141">0.giv-active</ta>
            <ta e="T143" id="Seg_3079" s="T142">accs-sit</ta>
            <ta e="T144" id="Seg_3080" s="T143">0.giv-active</ta>
            <ta e="T145" id="Seg_3081" s="T144">accs-sit</ta>
            <ta e="T146" id="Seg_3082" s="T145">giv-inactive</ta>
            <ta e="T149" id="Seg_3083" s="T148">giv-active-Q</ta>
            <ta e="T151" id="Seg_3084" s="T150">0.giv-active-Q</ta>
            <ta e="T152" id="Seg_3085" s="T151">giv-active-Q</ta>
            <ta e="T153" id="Seg_3086" s="T152">accs-gen</ta>
            <ta e="T155" id="Seg_3087" s="T154">giv-inactive</ta>
            <ta e="T156" id="Seg_3088" s="T155">giv-inactive</ta>
            <ta e="T158" id="Seg_3089" s="T157">giv-inactive</ta>
            <ta e="T159" id="Seg_3090" s="T158">0.giv-inactive quot-sp</ta>
            <ta e="T160" id="Seg_3091" s="T159">accs-gen-Q</ta>
            <ta e="T164" id="Seg_3092" s="T163">0.giv-active-Q</ta>
            <ta e="T166" id="Seg_3093" s="T165">giv-inactive-Q</ta>
            <ta e="T168" id="Seg_3094" s="T167">giv-inactive</ta>
            <ta e="T169" id="Seg_3095" s="T168">new-Q</ta>
            <ta e="T170" id="Seg_3096" s="T169">giv-inactive-Q</ta>
            <ta e="T172" id="Seg_3097" s="T171">giv-inactive-Q</ta>
            <ta e="T173" id="Seg_3098" s="T172">giv-active-Q</ta>
            <ta e="T175" id="Seg_3099" s="T174">giv-active-Q</ta>
            <ta e="T176" id="Seg_3100" s="T175">giv-active-Q</ta>
            <ta e="T178" id="Seg_3101" s="T177">accs-aggr-Q</ta>
            <ta e="T179" id="Seg_3102" s="T178">0.giv-active</ta>
            <ta e="T181" id="Seg_3103" s="T180">giv-inactive</ta>
            <ta e="T182" id="Seg_3104" s="T181">0.giv-active</ta>
            <ta e="T183" id="Seg_3105" s="T182">new</ta>
            <ta e="T184" id="Seg_3106" s="T183">new</ta>
            <ta e="T185" id="Seg_3107" s="T184">0.giv-active</ta>
            <ta e="T186" id="Seg_3108" s="T185">0.giv-active</ta>
            <ta e="T188" id="Seg_3109" s="T187">giv-inactive</ta>
            <ta e="T189" id="Seg_3110" s="T188">0.giv-active</ta>
            <ta e="T190" id="Seg_3111" s="T189">0.giv-active</ta>
            <ta e="T191" id="Seg_3112" s="T190">accs-inf</ta>
            <ta e="T192" id="Seg_3113" s="T191">giv-inactive-Q</ta>
            <ta e="T195" id="Seg_3114" s="T194">giv-inactive-Q</ta>
            <ta e="T196" id="Seg_3115" s="T195">0.giv-active</ta>
            <ta e="T197" id="Seg_3116" s="T196">giv-inactive</ta>
            <ta e="T199" id="Seg_3117" s="T198">giv-inactive</ta>
            <ta e="T201" id="Seg_3118" s="T200">giv-inactive</ta>
            <ta e="T202" id="Seg_3119" s="T201">new</ta>
            <ta e="T204" id="Seg_3120" s="T203">0.giv-active</ta>
            <ta e="T207" id="Seg_3121" s="T206">giv-inactive-Q</ta>
            <ta e="T208" id="Seg_3122" s="T207">new-Q</ta>
            <ta e="T210" id="Seg_3123" s="T209">giv-inactive-Q</ta>
            <ta e="T211" id="Seg_3124" s="T210">new-Q</ta>
            <ta e="T213" id="Seg_3125" s="T212">giv-inactive-Q</ta>
            <ta e="T214" id="Seg_3126" s="T213">new-Q</ta>
            <ta e="T217" id="Seg_3127" s="T216">giv-inactive</ta>
            <ta e="T218" id="Seg_3128" s="T217">giv-inactive</ta>
            <ta e="T222" id="Seg_3129" s="T221">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_3130" s="T7">TURK:cult</ta>
            <ta e="T30" id="Seg_3131" s="T29">RUS:gram</ta>
            <ta e="T38" id="Seg_3132" s="T37">TURK:cult</ta>
            <ta e="T44" id="Seg_3133" s="T43">TURK:cult</ta>
            <ta e="T47" id="Seg_3134" s="T46">TURK:cult</ta>
            <ta e="T48" id="Seg_3135" s="T47">TURK:disc</ta>
            <ta e="T75" id="Seg_3136" s="T74">TURK:gram(INDEF)</ta>
            <ta e="T99" id="Seg_3137" s="T98">%TURK:core</ta>
            <ta e="T107" id="Seg_3138" s="T105">TURK:mod(PTCL)</ta>
            <ta e="T110" id="Seg_3139" s="T108">TURK:mod(PTCL)</ta>
            <ta e="T113" id="Seg_3140" s="T112">TURK:disc</ta>
            <ta e="T162" id="Seg_3141" s="T161">RUS:mod</ta>
            <ta e="T163" id="Seg_3142" s="T162">RUS:mod</ta>
            <ta e="T165" id="Seg_3143" s="T164">RUS:mod</ta>
            <ta e="T195" id="Seg_3144" s="T194">TURK:cult</ta>
            <ta e="T199" id="Seg_3145" s="T198">TURK:cult</ta>
            <ta e="T202" id="Seg_3146" s="T201">TURK:cult</ta>
            <ta e="T213" id="Seg_3147" s="T212">TURK:cult</ta>
            <ta e="T219" id="Seg_3148" s="T218">TURK:gram(INDEF)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3149" s="T0">Жили муж с женой.</ta>
            <ta e="T9" id="Seg_3150" s="T3">Есть у них стало нечего, свою единственную корову они зарезали.</ta>
            <ta e="T15" id="Seg_3151" s="T9">Муж схватил топор и жену прогнал.</ta>
            <ta e="T18" id="Seg_3152" s="T15">Убежала жена прочь.</ta>
            <ta e="T21" id="Seg_3153" s="T18">Идёт она, [видит,] стоит чум.</ta>
            <ta e="T27" id="Seg_3154" s="T21">В чуме сало да мясо висит.</ta>
            <ta e="T31" id="Seg_3155" s="T27">Женщина приготовила себе еду и поела.</ta>
            <ta e="T34" id="Seg_3156" s="T31">Услышав чей-то зов, спряталась.</ta>
            <ta e="T39" id="Seg_3157" s="T34">Видит, одна голова скот пасет, </ta>
            <ta e="T41" id="Seg_3158" s="T39">К дому приближаясь, кричит:</ta>
            <ta e="T43" id="Seg_3159" s="T41">"Ворота, откройтесь!"</ta>
            <ta e="T54" id="Seg_3160" s="T43">Весь скот — лошади, овцы, коровы — как ворота в загон открылись, все вместе внутрь вошли.</ta>
            <ta e="T58" id="Seg_3161" s="T54">Как загон закрылся, [голова] к чуму покатилась.</ta>
            <ta e="T64" id="Seg_3162" s="T58">Голова крикнула, дверь чума открылась.</ta>
            <ta e="T68" id="Seg_3163" s="T64">Голова вкатилась.</ta>
            <ta e="T73" id="Seg_3164" s="T68">"Котел, ставься на огонь, сало с мясом, варись!", </ta>
            <ta e="T74" id="Seg_3165" s="T73">сказала.</ta>
            <ta e="T77" id="Seg_3166" s="T74">"Кто-то был здесь.</ta>
            <ta e="T79" id="Seg_3167" s="T77">Выходи сюда!</ta>
            <ta e="T89" id="Seg_3168" s="T79">Если это мужчина, мужем мне будет, а если женщина — подругой мне будет."</ta>
            <ta e="T92" id="Seg_3169" s="T89">Вышла спрятавшаяся женщина. </ta>
            <ta e="T98" id="Seg_3170" s="T92">К огню села с головой поесть.</ta>
            <ta e="T100" id="Seg_3171" s="T98">Сидят, разговаривают. </ta>
            <ta e="T111" id="Seg_3172" s="T100">"А как же ты головой одной ходишь, ведь ни рук, ни ног у тебя нет." </ta>
            <ta e="T115" id="Seg_3173" s="T111">"Я хоть куда хожу."</ta>
            <ta e="T119" id="Seg_3174" s="T115">"А в яму ты закатиться сможешь?"</ta>
            <ta e="T122" id="Seg_3175" s="T119">"Смогу", говорит.</ta>
            <ta e="T124" id="Seg_3176" s="T122">"Катись на пол!," — </ta>
            <ta e="T125" id="Seg_3177" s="T124">говорит.</ta>
            <ta e="T127" id="Seg_3178" s="T125">Голова скатилась.</ta>
            <ta e="T131" id="Seg_3179" s="T127">[Женщина] топор взяла и [голову] разрубила.</ta>
            <ta e="T134" id="Seg_3180" s="T131">Мозги вытащила и сварила.</ta>
            <ta e="T139" id="Seg_3181" s="T134">Взяла сало c мясом, в пузырь положила.</ta>
            <ta e="T142" id="Seg_3182" s="T139">Отправилась, к мужу своему пошла.</ta>
            <ta e="T144" id="Seg_3183" s="T142">В дымоход залезла.</ta>
            <ta e="T153" id="Seg_3184" s="T144">Муж палец [углём] мажет: "Этот сегодня съем, другой завтра."</ta>
            <ta e="T158" id="Seg_3185" s="T153">Женщина пузырь в дымоход бросила.</ta>
            <ta e="T166" id="Seg_3186" s="T158">[Муж] молится: "Бог раз дал, может, ещё разок мне даст!"</ta>
            <ta e="T174" id="Seg_3187" s="T166">А жена: "Камень бог тебе даст, [это ведь] я [тебе] дала."</ta>
            <ta e="T177" id="Seg_3188" s="T174">"Можно мне с тобой?"</ta>
            <ta e="T178" id="Seg_3189" s="T177">"Пойдём".</ta>
            <ta e="T182" id="Seg_3190" s="T178">Привела она его, к чуму головы пришли.</ta>
            <ta e="T186" id="Seg_3191" s="T182">Она мяса, сало сварила, накормила его.</ta>
            <ta e="T189" id="Seg_3192" s="T186">Муж: "Мне пукнуть надо."</ta>
            <ta e="T191" id="Seg_3193" s="T189">"В одеяло пукни!"</ta>
            <ta e="T194" id="Seg_3194" s="T191">"[Так] не пукай! </ta>
            <ta e="T196" id="Seg_3195" s="T194">Скот напугаешь."</ta>
            <ta e="T198" id="Seg_3196" s="T196">Муж пукнул.</ta>
            <ta e="T200" id="Seg_3197" s="T198">Скот распугал.</ta>
            <ta e="T206" id="Seg_3198" s="T200">Жена взяла молоко и побрызгала им вслед:</ta>
            <ta e="T215" id="Seg_3199" s="T206">"Овцы пусть козами станут, лошади лосями, коровы оленями!"</ta>
            <ta e="T222" id="Seg_3200" s="T215">Эти жена с мужем ни с чем остались, снова голодали.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3201" s="T0">A woman and a man lived.</ta>
            <ta e="T9" id="Seg_3202" s="T3">They ran out of (things) to eat, they killed their only cow. </ta>
            <ta e="T15" id="Seg_3203" s="T9">The man took an axe and chased his wife away.</ta>
            <ta e="T18" id="Seg_3204" s="T15">His wife went off.</ta>
            <ta e="T21" id="Seg_3205" s="T18">She walks on, there’s a tent standing.</ta>
            <ta e="T27" id="Seg_3206" s="T21">Inside the tent, fat and meat is hanging.</ta>
            <ta e="T31" id="Seg_3207" s="T27">The woman cooked and ate.</ta>
            <ta e="T34" id="Seg_3208" s="T31">She hid after hearing a call.</ta>
            <ta e="T39" id="Seg_3209" s="T34">She’s looking: a loose head is driving cattle.</ta>
            <ta e="T41" id="Seg_3210" s="T39">Arriving it calls:</ta>
            <ta e="T43" id="Seg_3211" s="T41">"Gate, open!"</ta>
            <ta e="T54" id="Seg_3212" s="T43">The cattle — horses, sheep, cows — all together entered after the corral gate had opened.</ta>
            <ta e="T58" id="Seg_3213" s="T54">After it had closed, [the head] came rolling to the tent.</ta>
            <ta e="T64" id="Seg_3214" s="T58">The loose head called and the tent door opened.</ta>
            <ta e="T68" id="Seg_3215" s="T64">The loose head came rolling in.</ta>
            <ta e="T73" id="Seg_3216" s="T68">"Cauldron, hang up [yourself], meaty fat, cook yourself!"</ta>
            <ta e="T74" id="Seg_3217" s="T73">it says.</ta>
            <ta e="T77" id="Seg_3218" s="T74">"Somebody was here.</ta>
            <ta e="T79" id="Seg_3219" s="T77">Come out here!</ta>
            <ta e="T89" id="Seg_3220" s="T79">If it’s a man, he will become my husband, if it’s a woman, she’ll become my companion."</ta>
            <ta e="T92" id="Seg_3221" s="T89">The hiding woman got up.</ta>
            <ta e="T98" id="Seg_3222" s="T92">She sat down by the side of the fire, she wanted to eat together with the head.</ta>
            <ta e="T100" id="Seg_3223" s="T98">They sit and talk:</ta>
            <ta e="T111" id="Seg_3224" s="T100">"How do you walk around with just a head, you have neither hands nor feet, have you?"</ta>
            <ta e="T115" id="Seg_3225" s="T111">"I go everywhere."</ta>
            <ta e="T119" id="Seg_3226" s="T115">‎"Can you roll into a pit?"</ta>
            <ta e="T122" id="Seg_3227" s="T119">"I can in one step", it says.</ta>
            <ta e="T124" id="Seg_3228" s="T122">‎‎"Roll to the ground!"</ta>
            <ta e="T125" id="Seg_3229" s="T124">she says.</ta>
            <ta e="T127" id="Seg_3230" s="T125">The head made a roll.</ta>
            <ta e="T131" id="Seg_3231" s="T127">She took an axe and crushed it.</ta>
            <ta e="T134" id="Seg_3232" s="T131">She took its brain out and cooked it.</ta>
            <ta e="T139" id="Seg_3233" s="T134">She took meaty fat and pressed it into a bladder.</ta>
            <ta e="T142" id="Seg_3234" s="T139">She got going, went to her man.</ta>
            <ta e="T144" id="Seg_3235" s="T142">She climbed up to the smoke hole.</ta>
            <ta e="T153" id="Seg_3236" s="T144">The man is marking his finger: "This one I will eat today, the other one tomorrow."</ta>
            <ta e="T158" id="Seg_3237" s="T153">The woman dropped her bladder through the smoke hole.</ta>
            <ta e="T166" id="Seg_3238" s="T158">He is praying: ‎‎"God has given, maybe he shall give me once more."</ta>
            <ta e="T174" id="Seg_3239" s="T166">The woman: "A stone will God give you, it was me who gave you [something]."</ta>
            <ta e="T177" id="Seg_3240" s="T174">"Will I go with you?"</ta>
            <ta e="T178" id="Seg_3241" s="T177">‎‎"Let’s go!"</ta>
            <ta e="T182" id="Seg_3242" s="T178">She led him, they came to the head’s tent.</ta>
            <ta e="T186" id="Seg_3243" s="T182">She cooked meat and fat, and fed him.</ta>
            <ta e="T189" id="Seg_3244" s="T186">The man: "I need to fart."</ta>
            <ta e="T191" id="Seg_3245" s="T189">‎‎"Put it [in] the bed[sheet]!</ta>
            <ta e="T194" id="Seg_3246" s="T191">Don't you fart!</ta>
            <ta e="T196" id="Seg_3247" s="T194">You’ll frighten the animals.‎‎"</ta>
            <ta e="T198" id="Seg_3248" s="T196">The man farted.</ta>
            <ta e="T200" id="Seg_3249" s="T198">He set the animals in gallop.</ta>
            <ta e="T206" id="Seg_3250" s="T200">The woman took milk and sprinkled it after them:</ta>
            <ta e="T215" id="Seg_3251" s="T206">‎‎"The sheep shall become goats, the horses [shall become] moose, and the cows [shall become] deer!"</ta>
            <ta e="T222" id="Seg_3252" s="T215">The woman and the man were left with nothing and suffered hunger again.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3253" s="T0">Einst lebte eine Frau mit ihrem Mann.</ta>
            <ta e="T9" id="Seg_3254" s="T3">Sie hatten nichts mehr zu essen. Sie schlachteten ihre einzige Kuh.</ta>
            <ta e="T15" id="Seg_3255" s="T9">Der Mann griff nach seiner Axt und jagte seine Frau fort.</ta>
            <ta e="T18" id="Seg_3256" s="T15">Seine Frau ging fort.</ta>
            <ta e="T21" id="Seg_3257" s="T18">Sie wandert, da steht ein Zelt.</ta>
            <ta e="T27" id="Seg_3258" s="T21">In dem Zelt hängt fettes Fleisch.</ta>
            <ta e="T31" id="Seg_3259" s="T27">Die Frau kochte [das Fleisch und] aß [davon].</ta>
            <ta e="T34" id="Seg_3260" s="T31">Sie versteckte sich, als sie einen Ruf hörte.</ta>
            <ta e="T39" id="Seg_3261" s="T34">Sie schaut: Ein loser Kopf treibt Vieh.</ta>
            <ta e="T41" id="Seg_3262" s="T39">Als er ankommt, ruft er:</ta>
            <ta e="T43" id="Seg_3263" s="T41">"Tür, öffne dich!"</ta>
            <ta e="T54" id="Seg_3264" s="T43">Das ganze Vieh - Pferde, Schafe und Kühe – sie alle gingen geschlossen hinein, nachdem sich die Zauntür geöffnet hatte.</ta>
            <ta e="T58" id="Seg_3265" s="T54">Nachdem sie sich geschlossen hatte, kam er in das Zelt gerollt.</ta>
            <ta e="T64" id="Seg_3266" s="T58">Der lose Kopf ruft und die Tür des Zeltes ging auf.</ta>
            <ta e="T68" id="Seg_3267" s="T64">Rollend kam der lose Kopf herein.</ta>
            <ta e="T73" id="Seg_3268" s="T68">"Häng den Kessel auf und koch dich, Speck!",</ta>
            <ta e="T74" id="Seg_3269" s="T73">sagt er.</ta>
            <ta e="T77" id="Seg_3270" s="T74">„Jemand war hier.</ta>
            <ta e="T79" id="Seg_3271" s="T77">Komm heraus!</ta>
            <ta e="T89" id="Seg_3272" s="T79">War es ein Menschenmann, wird er mein Gatte werden, war es eine Menschenfrau, wird sie meine Gefährtin werden."</ta>
            <ta e="T92" id="Seg_3273" s="T89">Die versteckte Frau stand auf.</ta>
            <ta e="T98" id="Seg_3274" s="T92">Sie setzte sich an das Feuer und wollte zusammen mit dem Kopf essen.</ta>
            <ta e="T100" id="Seg_3275" s="T98">Sie sitzen und unterhalten sich:</ta>
            <ta e="T111" id="Seg_3276" s="T100">"Du, wie bewegst du dich nur mit dem Kopf fort, hast du doch keine Hände und keine Füße?"</ta>
            <ta e="T115" id="Seg_3277" s="T111">"Ich laufe überall."</ta>
            <ta e="T119" id="Seg_3278" s="T115">"Kannst du in eine Grube rollen?"</ta>
            <ta e="T122" id="Seg_3279" s="T119">„Das kann ich mit einem einzigen Schritt“, sagt er.</ta>
            <ta e="T124" id="Seg_3280" s="T122">"Roll auf den Boden!",</ta>
            <ta e="T125" id="Seg_3281" s="T124">sagt sie.</ta>
            <ta e="T127" id="Seg_3282" s="T125">Der Kopf machte eine Rolle. </ta>
            <ta e="T131" id="Seg_3283" s="T127">Sie griff nach der Axt und zertrümmerte ihn.</ta>
            <ta e="T134" id="Seg_3284" s="T131">Sie nahm sein Gehirn heraus und kochte es.</ta>
            <ta e="T139" id="Seg_3285" s="T134">Sie nahm den Speck und stopfte ihn in eine Blase.</ta>
            <ta e="T142" id="Seg_3286" s="T139">Sie ging fort, ging zu ihrem Mann.</ta>
            <ta e="T144" id="Seg_3287" s="T142">Sie kletterte rauf zur Rauchöffnung.</ta>
            <ta e="T153" id="Seg_3288" s="T144">Der Mann saß da und markierte seine Finger: "Diesen werde ich heute essen, den anderen morgen."</ta>
            <ta e="T158" id="Seg_3289" s="T153">Die Frau ließ die Blase durch die Rauchöffnung fallen.</ta>
            <ta e="T166" id="Seg_3290" s="T158">Er betet: "Gott hat mir gegeben, vielleicht will er mir noch mehr geben!"</ta>
            <ta e="T174" id="Seg_3291" s="T166">Die Frau: "Gott wird dir einen Stein geben, ich gab dir".</ta>
            <ta e="T177" id="Seg_3292" s="T174">"Werde ich mit dir gehen?"</ta>
            <ta e="T178" id="Seg_3293" s="T177">"Gehen wir!"</ta>
            <ta e="T182" id="Seg_3294" s="T178">Sie führte ihn hin und sie kamen zum Zelt des Kopfes.</ta>
            <ta e="T186" id="Seg_3295" s="T182">Sie kochte Fleisch und Fett und fütterte ihn.</ta>
            <ta e="T189" id="Seg_3296" s="T186">Der Mann: "Ich muss furzen."</ta>
            <ta e="T191" id="Seg_3297" s="T189">"Setz ihn ins Bett!"</ta>
            <ta e="T194" id="Seg_3298" s="T191">Furze nicht [laut]!</ta>
            <ta e="T196" id="Seg_3299" s="T194">Du wirst das Vieh erschrecken."</ta>
            <ta e="T198" id="Seg_3300" s="T196">Der Mann furzte.</ta>
            <ta e="T200" id="Seg_3301" s="T198">Er versetzte die Tiere damit in Galopp.</ta>
            <ta e="T206" id="Seg_3302" s="T200">Die Frau nahm Milch und spritzte sie ihnen nach:</ta>
            <ta e="T215" id="Seg_3303" s="T206">"Die Schafe sollen zu Ziegen werden, die Pferde sollen zu Elchen werden, die Kühe sollen zu Hirschen werden!"</ta>
            <ta e="T222" id="Seg_3304" s="T215">Die Frau und der Mann hatten nichts mehr, sie hungerten wieder.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T3" id="Seg_3305" s="T0">Eine Alte und ein Alter lebten.</ta>
            <ta e="T9" id="Seg_3306" s="T3">Zu essen hatten sie nicht. Ihre einzige Kuh töteten sie.</ta>
            <ta e="T15" id="Seg_3307" s="T9">Dieser Alte nahm die Axt, die Alte aber jagte er fort.</ta>
            <ta e="T18" id="Seg_3308" s="T15">Seine Alte ging,</ta>
            <ta e="T21" id="Seg_3309" s="T18">geht, ein Zelt steht;</ta>
            <ta e="T27" id="Seg_3310" s="T21">innerhalb des Zeltes Fett, Fleisch hängt [eig hängend steht].</ta>
            <ta e="T31" id="Seg_3311" s="T27">Die Alte kochte, ass,</ta>
            <ta e="T34" id="Seg_3312" s="T31">versteckte sich, als sie einen Ruf hörte.</ta>
            <ta e="T39" id="Seg_3313" s="T34">Kur-uлu treibt Vieh,</ta>
            <ta e="T41" id="Seg_3314" s="T39">als er ankommt, ruft:</ta>
            <ta e="T43" id="Seg_3315" s="T41">»Tür, öffne dich!»</ta>
            <ta e="T54" id="Seg_3316" s="T43">Das Vieh, Pferd, Schaf, Kuh, alle zusammen auf den Hof die Tiere kamen.</ta>
            <ta e="T58" id="Seg_3317" s="T54">Als [die Tür] sich öffnete (!!!!), in das Zelt hüpfend kam er.</ta>
            <ta e="T64" id="Seg_3318" s="T58">Kur-ūлu schreit, die Tür des Zeltes öffnete sich, hüpfend kam er.</ta>
            <ta e="T68" id="Seg_3319" s="T64">hüpfend kam er.</ta>
            <ta e="T73" id="Seg_3320" s="T68">»Den Kessel hänge auf, das Fleisch, das Fett gehe,»</ta>
            <ta e="T74" id="Seg_3321" s="T73">sagt er.</ta>
            <ta e="T77" id="Seg_3322" s="T74">»Jemand war hier.</ta>
            <ta e="T79" id="Seg_3323" s="T77">Komm hierher!</ta>
            <ta e="T89" id="Seg_3324" s="T79">Wenn es eine Mannsperson ist, wird er mein Alter, wenn es eine Weibsperson ist wird sie meine Kameradin.»</ta>
            <ta e="T92" id="Seg_3325" s="T89">Sie stand auf,</ta>
            <ta e="T98" id="Seg_3326" s="T92">an das Feuer dieses alte Weib setzte sich, am Feuer mit Uлu begann sie zu essen.</ta>
            <ta e="T111" id="Seg_3327" s="T98">Sprechend sitzen sie: »Du, wie gehst du nur mit dem Kopfe, Hände hast du ja nicht [deine Hände sind ja nicht], Füsse hast du ja nicht [deine Füsse sind ja nicht]?»</ta>
            <ta e="T115" id="Seg_3328" s="T111">»Ich gehe überall.»</ta>
            <ta e="T119" id="Seg_3329" s="T115">»Kannst du in die Grube hüpfen?»</ta>
            <ta e="T122" id="Seg_3330" s="T119">»Ich kann gehen,» sagt er.</ta>
            <ta e="T127" id="Seg_3331" s="T125">Hüpfend auf die Erde könnend Uлu sprang.</ta>
            <ta e="T131" id="Seg_3332" s="T127">Sie nahm die Axt, spaltend schlug sie.</ta>
            <ta e="T134" id="Seg_3333" s="T131">Sie zog das Gehirn hervor, kochte,</ta>
            <ta e="T139" id="Seg_3334" s="T134">nahm Fleisch, Fett, steckte in die Blase,</ta>
            <ta e="T142" id="Seg_3335" s="T139">ging fort. Zu ihrem Alten ging sie,</ta>
            <ta e="T144" id="Seg_3336" s="T142">zu der Rauchöffnung des Zeltes kletterte sie.</ta>
            <ta e="T153" id="Seg_3337" s="T144">Den Finger aber (mit Kohle) liniierend sitzt der Alte: »Diesen esse ich heute, den anderen morgen.»</ta>
            <ta e="T158" id="Seg_3338" s="T153">Diese Alte liess ihre Blase durch die Öffnung fallen.</ta>
            <ta e="T166" id="Seg_3339" s="T158">Er betet zu Gott: »Gott gab noch, vielleicht gibt er mir noch.»</ta>
            <ta e="T174" id="Seg_3340" s="T166">Diese Alte: »Einen Stein gibt dir Gott, ich gab.</ta>
            <ta e="T177" id="Seg_3341" s="T174">Ob ich mit dir wandere?»</ta>
            <ta e="T178" id="Seg_3342" s="T177">»Lass uns gehen!»</ta>
            <ta e="T182" id="Seg_3343" s="T178">Sie brachte [näml. ihren Alten], zum Zelte des Uлu kamen sie.</ta>
            <ta e="T186" id="Seg_3344" s="T182">Sie kochte Fleisch, Fett, fütterte.</ta>
            <ta e="T189" id="Seg_3345" s="T186">Dieser Alte: »Ich will mal furzen.</ta>
            <ta e="T191" id="Seg_3346" s="T189">Durchsteche das Schlaffell!»</ta>
            <ta e="T194" id="Seg_3347" s="T191">»Furze nicht!</ta>
            <ta e="T196" id="Seg_3348" s="T194">Du erschreckst das Vieh.»</ta>
            <ta e="T198" id="Seg_3349" s="T196">Der Alte furzte.</ta>
            <ta e="T200" id="Seg_3350" s="T198">Das Vieh lief seines Weges.</ta>
            <ta e="T206" id="Seg_3351" s="T200">Die Alte nahm Milch, spritzte sie ihnen nach:</ta>
            <ta e="T215" id="Seg_3352" s="T206">»Die Schafe mögen zu Ziegen werden, die Pferde mögen zu Elchen werden, die Kühe mögen zu sibirischen Hirschen werden.» </ta>
            <ta e="T222" id="Seg_3353" s="T215">Diese Alte und dieser Alte haben nichts, sie hungerten noch.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T73" id="Seg_3354" s="T68">Alternative reading: uja i sil ’meat and fat’</ta>
            <ta e="T139" id="Seg_3355" s="T134">[KlT:] Alternative reading: uja i sil 'meat and fat'</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
