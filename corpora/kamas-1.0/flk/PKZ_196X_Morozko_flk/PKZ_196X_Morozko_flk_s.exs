<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB08E45BF-CEAA-7923-D576-DE11F1E36C91">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Morozko_flk.wav" />
         <referenced-file url="PKZ_196X_Morozko_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Morozko_flk\PKZ_196X_Morozko_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">410</ud-information>
            <ud-information attribute-name="# HIAT:w">267</ud-information>
            <ud-information attribute-name="# e">267</ud-information>
            <ud-information attribute-name="# HIAT:u">68</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1403" time="0.091" type="appl" />
         <tli id="T1404" time="0.823" type="appl" />
         <tli id="T1405" time="1.554" type="appl" />
         <tli id="T1406" time="3.8265852531760993" />
         <tli id="T1407" time="4.604" type="appl" />
         <tli id="T1408" time="5.358" type="appl" />
         <tli id="T1409" time="6.113" type="appl" />
         <tli id="T1410" time="6.563" type="appl" />
         <tli id="T1411" time="7.012" type="appl" />
         <tli id="T1412" time="7.462" type="appl" />
         <tli id="T1413" time="7.912" type="appl" />
         <tli id="T1414" time="8.55" type="appl" />
         <tli id="T1415" time="9.187" type="appl" />
         <tli id="T1416" time="9.824" type="appl" />
         <tli id="T1417" time="10.462" type="appl" />
         <tli id="T1418" time="11.1" type="appl" />
         <tli id="T1419" time="11.737" type="appl" />
         <tli id="T1420" time="12.938" type="appl" />
         <tli id="T1421" time="14.219697465199683" />
         <tli id="T1422" time="14.812" type="appl" />
         <tli id="T1423" time="15.5" type="appl" />
         <tli id="T1424" time="16.188" type="appl" />
         <tli id="T1425" time="16.876" type="appl" />
         <tli id="T1426" time="18.086281867363688" />
         <tli id="T1427" time="18.965" type="appl" />
         <tli id="T1428" time="20.058" type="appl" />
         <tli id="T1429" time="21.31954641055255" />
         <tli id="T1430" time="22.04" type="appl" />
         <tli id="T1431" time="22.675" type="appl" />
         <tli id="T1432" time="23.31" type="appl" />
         <tli id="T1433" time="23.946" type="appl" />
         <tli id="T1434" time="24.595" type="appl" />
         <tli id="T1435" time="25.245" type="appl" />
         <tli id="T1436" time="26.379438757522337" />
         <tli id="T1437" time="27.193" type="appl" />
         <tli id="T1438" time="27.895" type="appl" />
         <tli id="T1439" time="28.597" type="appl" />
         <tli id="T1440" time="29.3" type="appl" />
         <tli id="T1441" time="30.002" type="appl" />
         <tli id="T1442" time="30.704" type="appl" />
         <tli id="T1443" time="31.645993374262964" />
         <tli id="T1444" time="32.448" type="appl" />
         <tli id="T1445" time="33.23" type="appl" />
         <tli id="T1446" time="34.011" type="appl" />
         <tli id="T1447" time="34.793" type="appl" />
         <tli id="T1448" time="35.84590401799283" />
         <tli id="T1449" time="37.53" type="appl" />
         <tli id="T1450" time="39.205832532976714" />
         <tli id="T1451" time="39.958" type="appl" />
         <tli id="T1452" time="40.718" type="appl" />
         <tli id="T1453" time="41.477" type="appl" />
         <tli id="T1454" time="42.237" type="appl" />
         <tli id="T1455" time="42.996" type="appl" />
         <tli id="T1456" time="43.756" type="appl" />
         <tli id="T1457" time="44.515" type="appl" />
         <tli id="T1458" time="45.477" type="appl" />
         <tli id="T1459" time="46.439" type="appl" />
         <tli id="T1460" time="47.401" type="appl" />
         <tli id="T1461" time="48.401" type="appl" />
         <tli id="T1462" time="49.53227949668713" />
         <tli id="T1463" time="50.376" type="appl" />
         <tli id="T1464" time="51.075" type="appl" />
         <tli id="T1465" time="51.775" type="appl" />
         <tli id="T1466" time="52.474" type="appl" />
         <tli id="T1467" time="53.27886645188743" />
         <tli id="T1468" time="54.032" type="appl" />
         <tli id="T1469" time="54.891" type="appl" />
         <tli id="T1470" time="55.66" type="appl" />
         <tli id="T1471" time="56.427" type="appl" />
         <tli id="T1472" time="56.974" type="appl" />
         <tli id="T1473" time="57.522" type="appl" />
         <tli id="T1474" time="58.069" type="appl" />
         <tli id="T1475" time="58.616" type="appl" />
         <tli id="T1476" time="59.164" type="appl" />
         <tli id="T1477" time="59.958724332867305" />
         <tli id="T1478" time="61.352028022612615" />
         <tli id="T1479" time="62.171" type="appl" />
         <tli id="T1480" time="62.935" type="appl" />
         <tli id="T1481" time="63.575" type="appl" />
         <tli id="T1482" time="64.214" type="appl" />
         <tli id="T1483" time="64.854" type="appl" />
         <tli id="T1484" time="65.377" type="appl" />
         <tli id="T1485" time="65.9" type="appl" />
         <tli id="T1486" time="66.424" type="appl" />
         <tli id="T1487" time="66.947" type="appl" />
         <tli id="T1488" time="67.47" type="appl" />
         <tli id="T1489" time="68.566" type="appl" />
         <tli id="T1490" time="69.663" type="appl" />
         <tli id="T1491" time="70.759" type="appl" />
         <tli id="T1492" time="71.499" type="appl" />
         <tli id="T1493" time="72.33846094462344" />
         <tli id="T1494" time="72.782" type="appl" />
         <tli id="T1495" time="73.325" type="appl" />
         <tli id="T1496" time="73.869" type="appl" />
         <tli id="T1497" time="74.412" type="appl" />
         <tli id="T1498" time="75.29173144489697" />
         <tli id="T1499" time="76.405" type="appl" />
         <tli id="T1500" time="77.565" type="appl" />
         <tli id="T1501" time="78.724" type="appl" />
         <tli id="T1502" time="79.884" type="appl" />
         <tli id="T1503" time="81.40493471521488" />
         <tli id="T1504" time="82.465" type="appl" />
         <tli id="T1505" time="83.344" type="appl" />
         <tli id="T1506" time="84.202" type="appl" />
         <tli id="T1507" time="85.35818394018601" />
         <tli id="T1508" time="86.40133231933328" />
         <tli id="T1509" time="87.576" type="appl" />
         <tli id="T1510" time="88.744" type="appl" />
         <tli id="T1511" time="90.884733025348" />
         <tli id="T1512" time="91.978" type="appl" />
         <tli id="T1513" time="92.853" type="appl" />
         <tli id="T1514" time="93.728" type="appl" />
         <tli id="T1515" time="94.603" type="appl" />
         <tli id="T1516" time="95.482" type="appl" />
         <tli id="T1517" time="96.361" type="appl" />
         <tli id="T1518" time="97.239" type="appl" />
         <tli id="T1519" time="98.118" type="appl" />
         <tli id="T1520" time="99.67787927785545" />
         <tli id="T1521" time="100.595" type="appl" />
         <tli id="T1522" time="101.55783928028691" />
         <tli id="T1523" time="102.512" type="appl" />
         <tli id="T1524" time="103.454" type="appl" />
         <tli id="T1525" time="104.55777545437968" />
         <tli id="T1526" time="105.096" type="appl" />
         <tli id="T1527" time="105.795" type="appl" />
         <tli id="T1528" time="106.494" type="appl" />
         <tli id="T1529" time="107.192" type="appl" />
         <tli id="T1530" time="107.891" type="appl" />
         <tli id="T1531" time="108.59" type="appl" />
         <tli id="T1532" time="109.71766567381923" />
         <tli id="T1533" time="110.475" type="appl" />
         <tli id="T1534" time="111.195" type="appl" />
         <tli id="T1535" time="111.914" type="appl" />
         <tli id="T1536" time="112.634" type="appl" />
         <tli id="T1537" time="114.11090553765727" />
         <tli id="T1538" time="115.037" type="appl" />
         <tli id="T1539" time="116.088" type="appl" />
         <tli id="T1540" time="117.139" type="appl" />
         <tli id="T1541" time="118.19" type="appl" />
         <tli id="T1542" time="118.649" type="appl" />
         <tli id="T1543" time="119.109" type="appl" />
         <tli id="T1544" time="119.568" type="appl" />
         <tli id="T1545" time="120.027" type="appl" />
         <tli id="T1546" time="120.487" type="appl" />
         <tli id="T1547" time="120.946" type="appl" />
         <tli id="T1548" time="121.405" type="appl" />
         <tli id="T1549" time="121.865" type="appl" />
         <tli id="T1550" time="122.59072512309282" />
         <tli id="T1551" time="123.409" type="appl" />
         <tli id="T1552" time="124.101" type="appl" />
         <tli id="T1553" time="126.82396839097926" />
         <tli id="T1554" time="127.578" type="appl" />
         <tli id="T1555" time="128.243" type="appl" />
         <tli id="T1556" time="128.908" type="appl" />
         <tli id="T1557" time="129.522" type="appl" />
         <tli id="T1558" time="130.135" type="appl" />
         <tli id="T1559" time="130.93721421190202" />
         <tli id="T1560" time="131.729" type="appl" />
         <tli id="T1561" time="132.437" type="appl" />
         <tli id="T1562" time="133.146" type="appl" />
         <tli id="T1563" time="133.691" type="appl" />
         <tli id="T1564" time="134.236" type="appl" />
         <tli id="T1565" time="134.99712783417422" />
         <tli id="T1566" time="135.647" type="appl" />
         <tli id="T1567" time="136.36" type="appl" />
         <tli id="T1568" time="137.283745851316" />
         <tli id="T1569" time="138.151" type="appl" />
         <tli id="T1570" time="138.862" type="appl" />
         <tli id="T1571" time="139.574" type="appl" />
         <tli id="T1572" time="140.286" type="appl" />
         <tli id="T1573" time="141.261" type="appl" />
         <tli id="T1574" time="141.833" type="appl" />
         <tli id="T1575" time="142.404" type="appl" />
         <tli id="T1576" time="142.976" type="appl" />
         <tli id="T1577" time="143.548" type="appl" />
         <tli id="T1578" time="144.119" type="appl" />
         <tli id="T1579" time="144.691" type="appl" />
         <tli id="T1580" time="145.262" type="appl" />
         <tli id="T1581" time="145.834" type="appl" />
         <tli id="T1582" time="146.51" type="appl" />
         <tli id="T1583" time="147.185" type="appl" />
         <tli id="T1584" time="147.861" type="appl" />
         <tli id="T1585" time="148.536" type="appl" />
         <tli id="T1586" time="149.212" type="appl" />
         <tli id="T1587" time="149.832" type="appl" />
         <tli id="T1588" time="150.452" type="appl" />
         <tli id="T1589" time="151.072" type="appl" />
         <tli id="T1590" time="151.692" type="appl" />
         <tli id="T1591" time="152.224" type="appl" />
         <tli id="T1592" time="152.755" type="appl" />
         <tli id="T1593" time="153.287" type="appl" />
         <tli id="T1594" time="153.818" type="appl" />
         <tli id="T1595" time="154.35" type="appl" />
         <tli id="T1596" time="154.881" type="appl" />
         <tli id="T1597" time="155.413" type="appl" />
         <tli id="T1598" time="156.22" type="appl" />
         <tli id="T1599" time="157.026" type="appl" />
         <tli id="T1600" time="157.833" type="appl" />
         <tli id="T1601" time="158.472" type="appl" />
         <tli id="T1602" time="159.111" type="appl" />
         <tli id="T1603" time="161.08323949911858" />
         <tli id="T1604" time="161.865" type="appl" />
         <tli id="T1605" time="162.554" type="appl" />
         <tli id="T1606" time="163.52985411221204" />
         <tli id="T1607" time="164.173" type="appl" />
         <tli id="T1608" time="165.091" type="appl" />
         <tli id="T1609" time="166.98978049966567" />
         <tli id="T1610" time="167.866" type="appl" />
         <tli id="T1611" time="168.72" type="appl" />
         <tli id="T1612" time="169.574" type="appl" />
         <tli id="T1613" time="170.427" type="appl" />
         <tli id="T1614" time="171.28" type="appl" />
         <tli id="T1615" time="172.134" type="appl" />
         <tli id="T1616" time="173.224" type="appl" />
         <tli id="T1617" time="174.106" type="appl" />
         <tli id="T1618" time="174.989" type="appl" />
         <tli id="T1619" time="175.871" type="appl" />
         <tli id="T1620" time="176.421" type="appl" />
         <tli id="T1621" time="176.971" type="appl" />
         <tli id="T1622" time="177.522" type="appl" />
         <tli id="T1623" time="178.072" type="appl" />
         <tli id="T1624" time="178.622" type="appl" />
         <tli id="T1625" time="179.172" type="appl" />
         <tli id="T1626" time="180.139" type="appl" />
         <tli id="T1627" time="181.104" type="appl" />
         <tli id="T1628" time="182.07" type="appl" />
         <tli id="T1629" time="183.035" type="appl" />
         <tli id="T1630" time="183.622" type="appl" />
         <tli id="T1631" time="184.209" type="appl" />
         <tli id="T1632" time="184.796" type="appl" />
         <tli id="T1633" time="185.782713999149" />
         <tli id="T1634" time="186.544" type="appl" />
         <tli id="T1635" time="187.42267910765304" />
         <tli id="T1636" time="188.261" type="appl" />
         <tli id="T1637" time="189.082" type="appl" />
         <tli id="T1638" time="189.902" type="appl" />
         <tli id="T1639" time="190.722" type="appl" />
         <tli id="T1640" time="191.542" type="appl" />
         <tli id="T1641" time="192.363" type="appl" />
         <tli id="T1642" time="193.64254677527202" />
         <tli id="T1643" time="194.991" type="appl" />
         <tli id="T1644" time="196.345" type="appl" />
         <tli id="T1645" time="197.551" type="appl" />
         <tli id="T1646" time="198.757" type="appl" />
         <tli id="T1647" time="199.963" type="appl" />
         <tli id="T1648" time="200.525" type="appl" />
         <tli id="T1649" time="201.087" type="appl" />
         <tli id="T1650" time="201.649" type="appl" />
         <tli id="T1651" time="202.211" type="appl" />
         <tli id="T1652" time="202.773" type="appl" />
         <tli id="T1653" time="203.335" type="appl" />
         <tli id="T1654" time="203.897" type="appl" />
         <tli id="T1655" time="204.619" type="appl" />
         <tli id="T1656" time="205.342" type="appl" />
         <tli id="T1657" time="206.064" type="appl" />
         <tli id="T1658" time="207.58" type="appl" />
         <tli id="T1659" time="209.097" type="appl" />
         <tli id="T1660" time="209.884" type="appl" />
         <tli id="T1661" time="210.671" type="appl" />
         <tli id="T1662" time="211.999" type="appl" />
         <tli id="T1663" time="213.315" type="appl" />
         <tli id="T1664" time="214.631" type="appl" />
         <tli id="T1665" time="215.924" type="appl" />
         <tli id="T1666" time="216.872" type="appl" />
         <tli id="T1667" time="217.362" type="appl" />
         <tli id="T1668" time="217.851" type="appl" />
         <tli id="T1669" time="218.341" type="appl" />
         <tli id="T1670" time="219.342" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1670" id="Seg_0" n="sc" s="T1403">
               <ts e="T1406" id="Seg_2" n="HIAT:u" s="T1403">
                  <ts e="T1404" id="Seg_4" n="HIAT:w" s="T1403">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1405" id="Seg_7" n="HIAT:w" s="T1404">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1406" id="Seg_11" n="HIAT:w" s="T1405">büzʼe</ts>
                  <nts id="Seg_12" n="HIAT:ip">.</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1409" id="Seg_15" n="HIAT:u" s="T1406">
                  <ts e="T1407" id="Seg_17" n="HIAT:w" s="T1406">Nüken</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1408" id="Seg_20" n="HIAT:w" s="T1407">ibi</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1409" id="Seg_23" n="HIAT:w" s="T1408">koʔbdot</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1413" id="Seg_27" n="HIAT:u" s="T1409">
                  <ts e="T1410" id="Seg_29" n="HIAT:w" s="T1409">I</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1411" id="Seg_32" n="HIAT:w" s="T1410">büzʼen</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1412" id="Seg_35" n="HIAT:w" s="T1411">ibi</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1413" id="Seg_38" n="HIAT:w" s="T1412">koʔbdot</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1419" id="Seg_42" n="HIAT:u" s="T1413">
                  <ts e="T1414" id="Seg_44" n="HIAT:w" s="T1413">Dĭ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1415" id="Seg_47" n="HIAT:w" s="T1414">nüke</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1416" id="Seg_50" n="HIAT:w" s="T1415">büzʼen</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_53" n="HIAT:w" s="T1416">koʔbdobə</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_56" n="HIAT:w" s="T1417">ej</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1419" id="Seg_59" n="HIAT:w" s="T1418">ajirbi</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1421" id="Seg_63" n="HIAT:u" s="T1419">
                  <ts e="T1420" id="Seg_65" n="HIAT:w" s="T1419">Münörbi</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_69" n="HIAT:w" s="T1420">kudonzlaʔpi</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1426" id="Seg_73" n="HIAT:u" s="T1421">
                  <ts e="T1422" id="Seg_75" n="HIAT:w" s="T1421">A</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1423" id="Seg_78" n="HIAT:w" s="T1422">bostə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_81" n="HIAT:w" s="T1423">koʔbdobə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_84" n="HIAT:w" s="T1424">uge</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1426" id="Seg_87" n="HIAT:w" s="T1425">ajirlaʔbə</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1429" id="Seg_91" n="HIAT:u" s="T1426">
                  <ts e="T1427" id="Seg_93" n="HIAT:w" s="T1426">Ulut</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1428" id="Seg_96" n="HIAT:w" s="T1427">bar</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_99" n="HIAT:w" s="T1428">dʼüʔlaʔbə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1436" id="Seg_103" n="HIAT:u" s="T1429">
                  <ts e="T1430" id="Seg_105" n="HIAT:w" s="T1429">Dĭgəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T1431" id="Seg_109" n="HIAT:w" s="T1430">m-</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1432" id="Seg_113" n="HIAT:w" s="T1431">büzʼenə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1433" id="Seg_116" n="HIAT:w" s="T1432">măndə:</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_118" n="HIAT:ip">"</nts>
                  <ts e="T1434" id="Seg_120" n="HIAT:w" s="T1433">Kundə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1435" id="Seg_123" n="HIAT:w" s="T1434">boslə</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_126" n="HIAT:w" s="T1435">koʔbdo</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1443" id="Seg_130" n="HIAT:u" s="T1436">
                  <nts id="Seg_131" n="HIAT:ip">(</nts>
                  <ts e="T1437" id="Seg_133" n="HIAT:w" s="T1436">Măn</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_136" n="HIAT:w" s="T1437">dĭm=</ts>
                  <nts id="Seg_137" n="HIAT:ip">)</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_140" n="HIAT:w" s="T1438">Dĭ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1440" id="Seg_143" n="HIAT:w" s="T1439">măna</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_146" n="HIAT:w" s="T1440">ĭmbidə</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_149" n="HIAT:w" s="T1441">ej</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_152" n="HIAT:w" s="T1442">alia</ts>
                  <nts id="Seg_153" n="HIAT:ip">"</nts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1448" id="Seg_157" n="HIAT:u" s="T1443">
                  <ts e="T1444" id="Seg_159" n="HIAT:w" s="T1443">Dĭ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_162" n="HIAT:w" s="T1444">inem</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1446" id="Seg_165" n="HIAT:w" s="T1445">kürerbi</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_169" n="HIAT:w" s="T1446">koʔbdobə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1448" id="Seg_172" n="HIAT:w" s="T1447">amnolbi</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1450" id="Seg_176" n="HIAT:u" s="T1448">
                  <ts e="T1449" id="Seg_178" n="HIAT:w" s="T1448">Kunnaːmbi</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1450" id="Seg_181" n="HIAT:w" s="T1449">dʼijenə</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1457" id="Seg_185" n="HIAT:u" s="T1450">
                  <ts e="T1451" id="Seg_187" n="HIAT:w" s="T1450">Barəʔluʔpi</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <ts e="T1452" id="Seg_191" n="HIAT:w" s="T1451">pan</ts>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_194" n="HIAT:ip">(</nts>
                  <ts e="T1453" id="Seg_196" n="HIAT:w" s="T1452">tondə</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_199" n="HIAT:ip">(</nts>
                  <ts e="T1454" id="Seg_201" n="HIAT:w" s="T1453">nə-</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T1455" id="Seg_207" n="HIAT:w" s="T1454">š-</ts>
                  <nts id="Seg_208" n="HIAT:ip">)</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1456" id="Seg_211" n="HIAT:w" s="T1455">sĭre</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_214" n="HIAT:w" s="T1456">urgo</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1460" id="Seg_218" n="HIAT:u" s="T1457">
                  <ts e="T1458" id="Seg_220" n="HIAT:w" s="T1457">Dĭ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1459" id="Seg_223" n="HIAT:w" s="T1458">amnolaʔbə</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1460" id="Seg_227" n="HIAT:w" s="T1459">kănnalaʔbə</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1462" id="Seg_231" n="HIAT:u" s="T1460">
                  <ts e="T1461" id="Seg_233" n="HIAT:w" s="T1460">Šĭšəge</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1462" id="Seg_236" n="HIAT:w" s="T1461">šonəga</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1467" id="Seg_240" n="HIAT:u" s="T1462">
                  <nts id="Seg_241" n="HIAT:ip">(</nts>
                  <ts e="T1463" id="Seg_243" n="HIAT:w" s="T1462">Pa-</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1464" id="Seg_246" n="HIAT:w" s="T1463">paʔ-</ts>
                  <nts id="Seg_247" n="HIAT:ip">)</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1465" id="Seg_250" n="HIAT:w" s="T1464">Pagəʔ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1466" id="Seg_253" n="HIAT:w" s="T1465">panə</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1467" id="Seg_256" n="HIAT:w" s="T1466">suʔmileʔbə</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1471" id="Seg_260" n="HIAT:u" s="T1467">
                  <ts e="T1468" id="Seg_262" n="HIAT:w" s="T1467">Măndə</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1469" id="Seg_265" n="HIAT:w" s="T1468">dĭʔnə:</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_267" n="HIAT:ip">"</nts>
                  <ts e="T1470" id="Seg_269" n="HIAT:w" s="T1469">Tănan</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471" id="Seg_272" n="HIAT:w" s="T1470">šĭšəge</ts>
                  <nts id="Seg_273" n="HIAT:ip">!</nts>
                  <nts id="Seg_274" n="HIAT:ip">"</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1477" id="Seg_277" n="HIAT:u" s="T1471">
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <ts e="T1472" id="Seg_280" n="HIAT:w" s="T1471">Dʼok</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1473" id="Seg_284" n="HIAT:w" s="T1472">măna</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_286" n="HIAT:ip">(</nts>
                  <ts e="T1474" id="Seg_288" n="HIAT:w" s="T1473">ej=</ts>
                  <nts id="Seg_289" n="HIAT:ip">)</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1475" id="Seg_292" n="HIAT:w" s="T1474">ej</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1476" id="Seg_295" n="HIAT:w" s="T1475">šĭšəge</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1477" id="Seg_298" n="HIAT:w" s="T1476">iššo</ts>
                  <nts id="Seg_299" n="HIAT:ip">"</nts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1478" id="Seg_303" n="HIAT:u" s="T1477">
                  <ts e="T1478" id="Seg_305" n="HIAT:w" s="T1477">Šonəga</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1480" id="Seg_309" n="HIAT:u" s="T1478">
                  <nts id="Seg_310" n="HIAT:ip">"</nts>
                  <ts e="T1479" id="Seg_312" n="HIAT:w" s="T1478">Šĭšəge</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1480" id="Seg_315" n="HIAT:w" s="T1479">tănan</ts>
                  <nts id="Seg_316" n="HIAT:ip">?</nts>
                  <nts id="Seg_317" n="HIAT:ip">"</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1483" id="Seg_320" n="HIAT:u" s="T1480">
                  <nts id="Seg_321" n="HIAT:ip">"</nts>
                  <ts e="T1481" id="Seg_323" n="HIAT:w" s="T1480">Ej</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1482" id="Seg_326" n="HIAT:w" s="T1481">šĭšəge</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1483" id="Seg_330" n="HIAT:w" s="T1482">ejü</ts>
                  <nts id="Seg_331" n="HIAT:ip">"</nts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1488" id="Seg_335" n="HIAT:u" s="T1483">
                  <ts e="T1484" id="Seg_337" n="HIAT:w" s="T1483">A</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1485" id="Seg_340" n="HIAT:w" s="T1484">bostə</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1486" id="Seg_343" n="HIAT:w" s="T1485">dʼăbaktərzittə</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1487" id="Seg_346" n="HIAT:w" s="T1486">ej</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1488" id="Seg_349" n="HIAT:w" s="T1487">molia</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1491" id="Seg_353" n="HIAT:u" s="T1488">
                  <ts e="T1489" id="Seg_355" n="HIAT:w" s="T1488">Dĭgəttə</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1490" id="Seg_358" n="HIAT:w" s="T1489">bazoʔ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_361" n="HIAT:w" s="T1490">surarlaʔbə</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1493" id="Seg_365" n="HIAT:u" s="T1491">
                  <nts id="Seg_366" n="HIAT:ip">"</nts>
                  <ts e="T1492" id="Seg_368" n="HIAT:w" s="T1491">Ej</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1493" id="Seg_371" n="HIAT:w" s="T1492">šĭšəge</ts>
                  <nts id="Seg_372" n="HIAT:ip">"</nts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1498" id="Seg_376" n="HIAT:u" s="T1493">
                  <ts e="T1494" id="Seg_378" n="HIAT:w" s="T1493">Bostən</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495" id="Seg_381" n="HIAT:w" s="T1494">šĭket</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1496" id="Seg_384" n="HIAT:w" s="T1495">dĭ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1497" id="Seg_387" n="HIAT:w" s="T1496">bar</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1498" id="Seg_390" n="HIAT:w" s="T1497">kănnaːmbi</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1503" id="Seg_394" n="HIAT:u" s="T1498">
                  <ts e="T1499" id="Seg_396" n="HIAT:w" s="T1498">Dĭgəttə</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1500" id="Seg_399" n="HIAT:w" s="T1499">dĭ</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1501" id="Seg_402" n="HIAT:w" s="T1500">dĭm</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1502" id="Seg_405" n="HIAT:w" s="T1501">adʼejalaziʔ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1503" id="Seg_408" n="HIAT:w" s="T1502">šurbi</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1505" id="Seg_412" n="HIAT:u" s="T1503">
                  <ts e="T1504" id="Seg_414" n="HIAT:w" s="T1503">Palʼto</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_417" n="HIAT:w" s="T1504">šerbi</ts>
                  <nts id="Seg_418" n="HIAT:ip">.</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1507" id="Seg_421" n="HIAT:u" s="T1505">
                  <ts e="T1506" id="Seg_423" n="HIAT:w" s="T1505">Dĭ</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1507" id="Seg_426" n="HIAT:w" s="T1506">ejümneʔpi</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1511" id="Seg_430" n="HIAT:u" s="T1507">
                  <ts e="T1508" id="Seg_432" n="HIAT:w" s="T1507">Dĭgəttə</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1509" id="Seg_435" n="HIAT:w" s="T1508">nüke</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1510" id="Seg_438" n="HIAT:w" s="T1509">pürleʔbə</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1511" id="Seg_441" n="HIAT:w" s="T1510">blinəʔi</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1515" id="Seg_445" n="HIAT:u" s="T1511">
                  <nts id="Seg_446" n="HIAT:ip">"</nts>
                  <ts e="T1512" id="Seg_448" n="HIAT:w" s="T1511">Kanaʔ</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1513" id="Seg_452" n="HIAT:w" s="T1512">koʔbdo</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1514" id="Seg_455" n="HIAT:w" s="T1513">külaːmbi</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1515" id="Seg_459" n="HIAT:w" s="T1514">dettə</ts>
                  <nts id="Seg_460" n="HIAT:ip">!</nts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1520" id="Seg_464" n="HIAT:u" s="T1515">
                  <ts e="T1516" id="Seg_466" n="HIAT:w" s="T1515">Dĭgəttə</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1517" id="Seg_469" n="HIAT:w" s="T1516">men</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1518" id="Seg_472" n="HIAT:w" s="T1517">toldə</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1519" id="Seg_475" n="HIAT:w" s="T1518">jilgəndə</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1520" id="Seg_478" n="HIAT:w" s="T1519">amnolaʔbə</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1525" id="Seg_482" n="HIAT:u" s="T1520">
                  <ts e="T1521" id="Seg_484" n="HIAT:w" s="T1520">Bostə</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1522" id="Seg_487" n="HIAT:w" s="T1521">măllaʔbə:</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_489" n="HIAT:ip">"</nts>
                  <ts e="T1523" id="Seg_491" n="HIAT:w" s="T1522">Büzʼen</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_494" n="HIAT:w" s="T1523">koʔbdot</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1525" id="Seg_497" n="HIAT:w" s="T1524">šonəga</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1532" id="Seg_501" n="HIAT:u" s="T1525">
                  <nts id="Seg_502" n="HIAT:ip">(</nts>
                  <ts e="T1526" id="Seg_504" n="HIAT:w" s="T1525">Iʔg-</ts>
                  <nts id="Seg_505" n="HIAT:ip">)</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1527" id="Seg_508" n="HIAT:w" s="T1526">Iʔgö</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1528" id="Seg_511" n="HIAT:w" s="T1527">bar</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1529" id="Seg_514" n="HIAT:w" s="T1528">aktʼa</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1530" id="Seg_517" n="HIAT:w" s="T1529">detleʔbə</ts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1531" id="Seg_521" n="HIAT:w" s="T1530">oldʼa</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1532" id="Seg_524" n="HIAT:w" s="T1531">iʔgö</ts>
                  <nts id="Seg_525" n="HIAT:ip">"</nts>
                  <nts id="Seg_526" n="HIAT:ip">.</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1537" id="Seg_529" n="HIAT:u" s="T1532">
                  <ts e="T1533" id="Seg_531" n="HIAT:w" s="T1532">Dĭ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1534" id="Seg_534" n="HIAT:w" s="T1533">măndə:</ts>
                  <nts id="Seg_535" n="HIAT:ip">"</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1535" id="Seg_538" n="HIAT:w" s="T1534">Dăre</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1536" id="Seg_541" n="HIAT:w" s="T1535">iʔ</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1537" id="Seg_544" n="HIAT:w" s="T1536">mănaʔ</ts>
                  <nts id="Seg_545" n="HIAT:ip">!</nts>
                  <nts id="Seg_546" n="HIAT:ip">"</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1541" id="Seg_549" n="HIAT:u" s="T1537">
                  <ts e="T1538" id="Seg_551" n="HIAT:w" s="T1537">Büzʼen</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1539" id="Seg_554" n="HIAT:w" s="T1538">koʔbdobə</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1540" id="Seg_557" n="HIAT:w" s="T1539">băragən</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1541" id="Seg_560" n="HIAT:w" s="T1540">detleʔbə</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip">"</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1550" id="Seg_565" n="HIAT:u" s="T1541">
                  <ts e="T1542" id="Seg_567" n="HIAT:w" s="T1541">A</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_569" n="HIAT:ip">(</nts>
                  <ts e="T1543" id="Seg_571" n="HIAT:w" s="T1542">men=</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1544" id="Seg_574" n="HIAT:w" s="T1543">üge=</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1545" id="Seg_577" n="HIAT:w" s="T1544">dĭ-</ts>
                  <nts id="Seg_578" n="HIAT:ip">)</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1546" id="Seg_581" n="HIAT:w" s="T1545">men</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1547" id="Seg_584" n="HIAT:w" s="T1546">üge</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1548" id="Seg_587" n="HIAT:w" s="T1547">diʔnə</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1549" id="Seg_590" n="HIAT:w" s="T1548">dăre</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1550" id="Seg_593" n="HIAT:w" s="T1549">măllaʔbə</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1553" id="Seg_597" n="HIAT:u" s="T1550">
                  <ts e="T1551" id="Seg_599" n="HIAT:w" s="T1550">Dĭgəttə</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1552" id="Seg_602" n="HIAT:w" s="T1551">büzʼe</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1553" id="Seg_605" n="HIAT:w" s="T1552">šobi</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1556" id="Seg_609" n="HIAT:u" s="T1553">
                  <ts e="T1554" id="Seg_611" n="HIAT:w" s="T1553">Karzʼina</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1555" id="Seg_614" n="HIAT:w" s="T1554">aktʼa</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1556" id="Seg_617" n="HIAT:w" s="T1555">deʔpi</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1559" id="Seg_621" n="HIAT:u" s="T1556">
                  <ts e="T1557" id="Seg_623" n="HIAT:w" s="T1556">Bar</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1558" id="Seg_626" n="HIAT:w" s="T1557">oldʼa</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1559" id="Seg_629" n="HIAT:w" s="T1558">iʔgö</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1562" id="Seg_633" n="HIAT:u" s="T1559">
                  <ts e="T1560" id="Seg_635" n="HIAT:w" s="T1559">Koʔbdot</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1561" id="Seg_638" n="HIAT:w" s="T1560">turanə</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1562" id="Seg_641" n="HIAT:w" s="T1561">šobi</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1568" id="Seg_645" n="HIAT:u" s="T1562">
                  <ts e="T1563" id="Seg_647" n="HIAT:w" s="T1562">Dĭgəttə</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1564" id="Seg_650" n="HIAT:w" s="T1563">dĭ</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1565" id="Seg_653" n="HIAT:w" s="T1564">nüke:</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_655" n="HIAT:ip">"</nts>
                  <ts e="T1566" id="Seg_657" n="HIAT:w" s="T1565">Kundə</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1567" id="Seg_660" n="HIAT:w" s="T1566">măn</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1568" id="Seg_663" n="HIAT:w" s="T1567">koʔbdobə</ts>
                  <nts id="Seg_664" n="HIAT:ip">!</nts>
                  <nts id="Seg_665" n="HIAT:ip">"</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1572" id="Seg_668" n="HIAT:u" s="T1568">
                  <ts e="T1569" id="Seg_670" n="HIAT:w" s="T1568">Dĭ</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1570" id="Seg_673" n="HIAT:w" s="T1569">büzʼe</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_675" n="HIAT:ip">(</nts>
                  <ts e="T1571" id="Seg_677" n="HIAT:w" s="T1570">körerbi</ts>
                  <nts id="Seg_678" n="HIAT:ip">)</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1572" id="Seg_681" n="HIAT:w" s="T1571">ine</ts>
                  <nts id="Seg_682" n="HIAT:ip">.</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1573" id="Seg_685" n="HIAT:u" s="T1572">
                  <ts e="T1573" id="Seg_687" n="HIAT:w" s="T1572">Kumbi</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1581" id="Seg_691" n="HIAT:u" s="T1573">
                  <ts e="T1574" id="Seg_693" n="HIAT:w" s="T1573">Dĭ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1575" id="Seg_696" n="HIAT:w" s="T1574">koʔbdobə</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1576" id="Seg_699" n="HIAT:w" s="T1575">dĭn</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1577" id="Seg_702" n="HIAT:w" s="T1576">že</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1578" id="Seg_705" n="HIAT:w" s="T1577">maluʔpi</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1579" id="Seg_708" n="HIAT:w" s="T1578">i</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1580" id="Seg_711" n="HIAT:w" s="T1579">bostə</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1581" id="Seg_714" n="HIAT:w" s="T1580">šobi</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1586" id="Seg_718" n="HIAT:u" s="T1581">
                  <ts e="T1582" id="Seg_720" n="HIAT:w" s="T1581">Dĭgəttə</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1583" id="Seg_723" n="HIAT:w" s="T1582">bazoʔ</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1584" id="Seg_726" n="HIAT:w" s="T1583">šĭšəge</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1585" id="Seg_729" n="HIAT:w" s="T1584">dĭʔnə</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1586" id="Seg_732" n="HIAT:w" s="T1585">šonəga</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1590" id="Seg_736" n="HIAT:u" s="T1586">
                  <ts e="T1587" id="Seg_738" n="HIAT:w" s="T1586">Dĭ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1588" id="Seg_741" n="HIAT:w" s="T1587">bar:</ts>
                  <nts id="Seg_742" n="HIAT:ip">"</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1589" id="Seg_745" n="HIAT:w" s="T1588">Šĭšəge</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1590" id="Seg_748" n="HIAT:w" s="T1589">tănan</ts>
                  <nts id="Seg_749" n="HIAT:ip">?</nts>
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1597" id="Seg_753" n="HIAT:u" s="T1590">
                  <nts id="Seg_754" n="HIAT:ip">"</nts>
                  <ts e="T1591" id="Seg_756" n="HIAT:w" s="T1590">Šĭšəge</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1592" id="Seg_760" n="HIAT:w" s="T1591">kănnaːmbiam</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_763" n="HIAT:ip">(</nts>
                  <ts e="T1593" id="Seg_765" n="HIAT:w" s="T1592">iʔ=</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1594" id="Seg_768" n="HIAT:w" s="T1593">ha-</ts>
                  <nts id="Seg_769" n="HIAT:ip">)</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1595" id="Seg_772" n="HIAT:w" s="T1594">iʔ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1596" id="Seg_775" n="HIAT:w" s="T1595">šoʔ</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1597" id="Seg_778" n="HIAT:w" s="T1596">măna</ts>
                  <nts id="Seg_779" n="HIAT:ip">!</nts>
                  <nts id="Seg_780" n="HIAT:ip">"</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1600" id="Seg_783" n="HIAT:u" s="T1597">
                  <ts e="T1598" id="Seg_785" n="HIAT:w" s="T1597">Dĭ</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1599" id="Seg_788" n="HIAT:w" s="T1598">bazoʔ</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1600" id="Seg_791" n="HIAT:w" s="T1599">šonəga</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1602" id="Seg_795" n="HIAT:u" s="T1600">
                  <nts id="Seg_796" n="HIAT:ip">"</nts>
                  <ts e="T1601" id="Seg_798" n="HIAT:w" s="T1600">Šĭšəge</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1602" id="Seg_801" n="HIAT:w" s="T1601">tănan</ts>
                  <nts id="Seg_802" n="HIAT:ip">?</nts>
                  <nts id="Seg_803" n="HIAT:ip">"</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1603" id="Seg_806" n="HIAT:u" s="T1602">
                  <nts id="Seg_807" n="HIAT:ip">"</nts>
                  <ts e="T1603" id="Seg_809" n="HIAT:w" s="T1602">Šĭšəge</ts>
                  <nts id="Seg_810" n="HIAT:ip">!</nts>
                  <nts id="Seg_811" n="HIAT:ip">"</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1606" id="Seg_814" n="HIAT:u" s="T1603">
                  <ts e="T1604" id="Seg_816" n="HIAT:w" s="T1603">Davaj</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1605" id="Seg_819" n="HIAT:w" s="T1604">kudolzittə</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1606" id="Seg_822" n="HIAT:w" s="T1605">dĭm</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1609" id="Seg_826" n="HIAT:u" s="T1606">
                  <ts e="T1607" id="Seg_828" n="HIAT:w" s="T1606">Dĭgəttə</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1608" id="Seg_831" n="HIAT:w" s="T1607">iššo</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1609" id="Seg_834" n="HIAT:w" s="T1608">surarbi</ts>
                  <nts id="Seg_835" n="HIAT:ip">.</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1615" id="Seg_838" n="HIAT:u" s="T1609">
                  <ts e="T1610" id="Seg_840" n="HIAT:w" s="T1609">Dĭ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1611" id="Seg_843" n="HIAT:w" s="T1610">bazoʔ</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1612" id="Seg_846" n="HIAT:w" s="T1611">kudolbi</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1613" id="Seg_849" n="HIAT:w" s="T1612">vsʼaka</ts>
                  <nts id="Seg_850" n="HIAT:ip">,</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1614" id="Seg_853" n="HIAT:w" s="T1613">mut-mat</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1615" id="Seg_856" n="HIAT:w" s="T1614">kudolbi</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1619" id="Seg_860" n="HIAT:u" s="T1615">
                  <ts e="T1616" id="Seg_862" n="HIAT:w" s="T1615">Dĭ</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1617" id="Seg_865" n="HIAT:w" s="T1616">dĭm</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1618" id="Seg_868" n="HIAT:w" s="T1617">bar</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1619" id="Seg_871" n="HIAT:w" s="T1618">kăndəbi</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1625" id="Seg_875" n="HIAT:u" s="T1619">
                  <ts e="T1620" id="Seg_877" n="HIAT:w" s="T1619">I</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1621" id="Seg_880" n="HIAT:w" s="T1620">külaːmbi</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1622" id="Seg_883" n="HIAT:w" s="T1621">bar</ts>
                  <nts id="Seg_884" n="HIAT:ip">,</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1623" id="Seg_887" n="HIAT:w" s="T1622">pa</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1624" id="Seg_890" n="HIAT:w" s="T1623">dĭrgit</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1625" id="Seg_893" n="HIAT:w" s="T1624">molaːmbi</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1629" id="Seg_897" n="HIAT:u" s="T1625">
                  <ts e="T1626" id="Seg_899" n="HIAT:w" s="T1625">Dĭgəttə</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1627" id="Seg_902" n="HIAT:w" s="T1626">ertən</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1628" id="Seg_905" n="HIAT:w" s="T1627">büzʼe</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1629" id="Seg_908" n="HIAT:w" s="T1628">uʔbdəbi</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1633" id="Seg_912" n="HIAT:u" s="T1629">
                  <nts id="Seg_913" n="HIAT:ip">"</nts>
                  <ts e="T1630" id="Seg_915" n="HIAT:w" s="T1629">Kanaʔ</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1631" id="Seg_918" n="HIAT:w" s="T1630">măn</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1632" id="Seg_921" n="HIAT:w" s="T1631">koʔbdo</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1633" id="Seg_924" n="HIAT:w" s="T1632">ileʔ</ts>
                  <nts id="Seg_925" n="HIAT:ip">!</nts>
                  <nts id="Seg_926" n="HIAT:ip">"</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1635" id="Seg_929" n="HIAT:u" s="T1633">
                  <ts e="T1634" id="Seg_931" n="HIAT:w" s="T1633">Dĭ</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1635" id="Seg_934" n="HIAT:w" s="T1634">kambi</ts>
                  <nts id="Seg_935" n="HIAT:ip">.</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1642" id="Seg_938" n="HIAT:u" s="T1635">
                  <ts e="T1636" id="Seg_940" n="HIAT:w" s="T1635">Men</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1637" id="Seg_943" n="HIAT:w" s="T1636">măndə:</ts>
                  <nts id="Seg_944" n="HIAT:ip">"</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1638" id="Seg_947" n="HIAT:w" s="T1637">Nüken</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1639" id="Seg_950" n="HIAT:w" s="T1638">koʔbdobə</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1640" id="Seg_953" n="HIAT:w" s="T1639">detleʔbə</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1641" id="Seg_956" n="HIAT:w" s="T1640">băranə</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1642" id="Seg_959" n="HIAT:w" s="T1641">bar</ts>
                  <nts id="Seg_960" n="HIAT:ip">!</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1643" id="Seg_963" n="HIAT:u" s="T1642">
                  <ts e="T1643" id="Seg_965" n="HIAT:w" s="T1642">Külaːmbi</ts>
                  <nts id="Seg_966" n="HIAT:ip">"</nts>
                  <nts id="Seg_967" n="HIAT:ip">.</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1647" id="Seg_970" n="HIAT:u" s="T1643">
                  <ts e="T1644" id="Seg_972" n="HIAT:w" s="T1643">Dĭgəttə:</ts>
                  <nts id="Seg_973" n="HIAT:ip">"</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1645" id="Seg_976" n="HIAT:w" s="T1644">Dăre</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1646" id="Seg_979" n="HIAT:w" s="T1645">iʔ</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1647" id="Seg_982" n="HIAT:w" s="T1646">mănaʔ</ts>
                  <nts id="Seg_983" n="HIAT:ip">!</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1654" id="Seg_986" n="HIAT:u" s="T1647">
                  <ts e="T1648" id="Seg_988" n="HIAT:w" s="T1647">A</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1649" id="Seg_991" n="HIAT:w" s="T1648">mănaʔ:</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1650" id="Seg_994" n="HIAT:w" s="T1649">nüken</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1651" id="Seg_997" n="HIAT:w" s="T1650">koʔbdot</ts>
                  <nts id="Seg_998" n="HIAT:ip">,</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1652" id="Seg_1001" n="HIAT:w" s="T1651">dĭ</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1653" id="Seg_1004" n="HIAT:w" s="T1652">üge</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1654" id="Seg_1007" n="HIAT:w" s="T1653">dăre</ts>
                  <nts id="Seg_1008" n="HIAT:ip">"</nts>
                  <nts id="Seg_1009" n="HIAT:ip">.</nts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1657" id="Seg_1012" n="HIAT:u" s="T1654">
                  <ts e="T1655" id="Seg_1014" n="HIAT:w" s="T1654">Dĭgəttə</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1656" id="Seg_1017" n="HIAT:w" s="T1655">büzʼe</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1657" id="Seg_1020" n="HIAT:w" s="T1656">šonəga</ts>
                  <nts id="Seg_1021" n="HIAT:ip">.</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1659" id="Seg_1024" n="HIAT:u" s="T1657">
                  <ts e="T1658" id="Seg_1026" n="HIAT:w" s="T1657">Nüke</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1659" id="Seg_1029" n="HIAT:w" s="T1658">nuʔməluʔpi</ts>
                  <nts id="Seg_1030" n="HIAT:ip">.</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1661" id="Seg_1033" n="HIAT:u" s="T1659">
                  <ts e="T1660" id="Seg_1035" n="HIAT:w" s="T1659">Kuliot</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1661" id="Seg_1038" n="HIAT:w" s="T1660">bar</ts>
                  <nts id="Seg_1039" n="HIAT:ip">.</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1664" id="Seg_1042" n="HIAT:u" s="T1661">
                  <ts e="T1662" id="Seg_1044" n="HIAT:w" s="T1661">Koʔbdot</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1663" id="Seg_1047" n="HIAT:w" s="T1662">külaːmbi</ts>
                  <nts id="Seg_1048" n="HIAT:ip">,</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1664" id="Seg_1051" n="HIAT:w" s="T1663">iʔbölaʔbə</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1666" id="Seg_1055" n="HIAT:u" s="T1664">
                  <ts e="T1665" id="Seg_1057" n="HIAT:w" s="T1664">Davaj</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1666" id="Seg_1060" n="HIAT:w" s="T1665">dʼorzittə</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1669" id="Seg_1064" n="HIAT:u" s="T1666">
                  <ts e="T1667" id="Seg_1066" n="HIAT:w" s="T1666">Ĭmbidə</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1668" id="Seg_1069" n="HIAT:w" s="T1667">ej</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1669" id="Seg_1072" n="HIAT:w" s="T1668">alal</ts>
                  <nts id="Seg_1073" n="HIAT:ip">.</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1670" id="Seg_1076" n="HIAT:u" s="T1669">
                  <ts e="T1670" id="Seg_1078" n="HIAT:w" s="T1669">Kabarləj</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1670" id="Seg_1081" n="sc" s="T1403">
               <ts e="T1404" id="Seg_1083" n="e" s="T1403">Amnobiʔi </ts>
               <ts e="T1405" id="Seg_1085" n="e" s="T1404">nüke, </ts>
               <ts e="T1406" id="Seg_1087" n="e" s="T1405">büzʼe. </ts>
               <ts e="T1407" id="Seg_1089" n="e" s="T1406">Nüken </ts>
               <ts e="T1408" id="Seg_1091" n="e" s="T1407">ibi </ts>
               <ts e="T1409" id="Seg_1093" n="e" s="T1408">koʔbdot. </ts>
               <ts e="T1410" id="Seg_1095" n="e" s="T1409">I </ts>
               <ts e="T1411" id="Seg_1097" n="e" s="T1410">büzʼen </ts>
               <ts e="T1412" id="Seg_1099" n="e" s="T1411">ibi </ts>
               <ts e="T1413" id="Seg_1101" n="e" s="T1412">koʔbdot. </ts>
               <ts e="T1414" id="Seg_1103" n="e" s="T1413">Dĭ </ts>
               <ts e="T1415" id="Seg_1105" n="e" s="T1414">nüke </ts>
               <ts e="T1416" id="Seg_1107" n="e" s="T1415">büzʼen </ts>
               <ts e="T1417" id="Seg_1109" n="e" s="T1416">koʔbdobə </ts>
               <ts e="T1418" id="Seg_1111" n="e" s="T1417">ej </ts>
               <ts e="T1419" id="Seg_1113" n="e" s="T1418">ajirbi. </ts>
               <ts e="T1420" id="Seg_1115" n="e" s="T1419">Münörbi, </ts>
               <ts e="T1421" id="Seg_1117" n="e" s="T1420">kudonzlaʔpi. </ts>
               <ts e="T1422" id="Seg_1119" n="e" s="T1421">A </ts>
               <ts e="T1423" id="Seg_1121" n="e" s="T1422">bostə </ts>
               <ts e="T1424" id="Seg_1123" n="e" s="T1423">koʔbdobə </ts>
               <ts e="T1425" id="Seg_1125" n="e" s="T1424">uge </ts>
               <ts e="T1426" id="Seg_1127" n="e" s="T1425">ajirlaʔbə. </ts>
               <ts e="T1427" id="Seg_1129" n="e" s="T1426">Ulut </ts>
               <ts e="T1428" id="Seg_1131" n="e" s="T1427">bar </ts>
               <ts e="T1429" id="Seg_1133" n="e" s="T1428">dʼüʔlaʔbə. </ts>
               <ts e="T1430" id="Seg_1135" n="e" s="T1429">Dĭgəttə </ts>
               <ts e="T1431" id="Seg_1137" n="e" s="T1430">(m-) </ts>
               <ts e="T1432" id="Seg_1139" n="e" s="T1431">büzʼenə </ts>
               <ts e="T1433" id="Seg_1141" n="e" s="T1432">măndə: </ts>
               <ts e="T1434" id="Seg_1143" n="e" s="T1433">"Kundə </ts>
               <ts e="T1435" id="Seg_1145" n="e" s="T1434">boslə </ts>
               <ts e="T1436" id="Seg_1147" n="e" s="T1435">koʔbdo. </ts>
               <ts e="T1437" id="Seg_1149" n="e" s="T1436">(Măn </ts>
               <ts e="T1438" id="Seg_1151" n="e" s="T1437">dĭm=) </ts>
               <ts e="T1439" id="Seg_1153" n="e" s="T1438">Dĭ </ts>
               <ts e="T1440" id="Seg_1155" n="e" s="T1439">măna </ts>
               <ts e="T1441" id="Seg_1157" n="e" s="T1440">ĭmbidə </ts>
               <ts e="T1442" id="Seg_1159" n="e" s="T1441">ej </ts>
               <ts e="T1443" id="Seg_1161" n="e" s="T1442">alia". </ts>
               <ts e="T1444" id="Seg_1163" n="e" s="T1443">Dĭ </ts>
               <ts e="T1445" id="Seg_1165" n="e" s="T1444">inem </ts>
               <ts e="T1446" id="Seg_1167" n="e" s="T1445">kürerbi, </ts>
               <ts e="T1447" id="Seg_1169" n="e" s="T1446">koʔbdobə </ts>
               <ts e="T1448" id="Seg_1171" n="e" s="T1447">amnolbi. </ts>
               <ts e="T1449" id="Seg_1173" n="e" s="T1448">Kunnaːmbi </ts>
               <ts e="T1450" id="Seg_1175" n="e" s="T1449">dʼijenə. </ts>
               <ts e="T1451" id="Seg_1177" n="e" s="T1450">Barəʔluʔpi </ts>
               <ts e="T1452" id="Seg_1179" n="e" s="T1451">(pan) </ts>
               <ts e="T1453" id="Seg_1181" n="e" s="T1452">(tondə) </ts>
               <ts e="T1454" id="Seg_1183" n="e" s="T1453">(nə-), </ts>
               <ts e="T1455" id="Seg_1185" n="e" s="T1454">(š-) </ts>
               <ts e="T1456" id="Seg_1187" n="e" s="T1455">sĭre </ts>
               <ts e="T1457" id="Seg_1189" n="e" s="T1456">urgo. </ts>
               <ts e="T1458" id="Seg_1191" n="e" s="T1457">Dĭ </ts>
               <ts e="T1459" id="Seg_1193" n="e" s="T1458">amnolaʔbə, </ts>
               <ts e="T1460" id="Seg_1195" n="e" s="T1459">kănnalaʔbə. </ts>
               <ts e="T1461" id="Seg_1197" n="e" s="T1460">Šĭšəge </ts>
               <ts e="T1462" id="Seg_1199" n="e" s="T1461">šonəga. </ts>
               <ts e="T1463" id="Seg_1201" n="e" s="T1462">(Pa- </ts>
               <ts e="T1464" id="Seg_1203" n="e" s="T1463">paʔ-) </ts>
               <ts e="T1465" id="Seg_1205" n="e" s="T1464">Pagəʔ </ts>
               <ts e="T1466" id="Seg_1207" n="e" s="T1465">panə </ts>
               <ts e="T1467" id="Seg_1209" n="e" s="T1466">suʔmileʔbə. </ts>
               <ts e="T1468" id="Seg_1211" n="e" s="T1467">Măndə </ts>
               <ts e="T1469" id="Seg_1213" n="e" s="T1468">dĭʔnə: </ts>
               <ts e="T1470" id="Seg_1215" n="e" s="T1469">"Tănan </ts>
               <ts e="T1471" id="Seg_1217" n="e" s="T1470">šĭšəge!" </ts>
               <ts e="T1472" id="Seg_1219" n="e" s="T1471">"Dʼok, </ts>
               <ts e="T1473" id="Seg_1221" n="e" s="T1472">măna </ts>
               <ts e="T1474" id="Seg_1223" n="e" s="T1473">(ej=) </ts>
               <ts e="T1475" id="Seg_1225" n="e" s="T1474">ej </ts>
               <ts e="T1476" id="Seg_1227" n="e" s="T1475">šĭšəge </ts>
               <ts e="T1477" id="Seg_1229" n="e" s="T1476">iššo". </ts>
               <ts e="T1478" id="Seg_1231" n="e" s="T1477">Šonəga. </ts>
               <ts e="T1479" id="Seg_1233" n="e" s="T1478">"Šĭšəge </ts>
               <ts e="T1480" id="Seg_1235" n="e" s="T1479">tănan?" </ts>
               <ts e="T1481" id="Seg_1237" n="e" s="T1480">"Ej </ts>
               <ts e="T1482" id="Seg_1239" n="e" s="T1481">šĭšəge, </ts>
               <ts e="T1483" id="Seg_1241" n="e" s="T1482">ejü". </ts>
               <ts e="T1484" id="Seg_1243" n="e" s="T1483">A </ts>
               <ts e="T1485" id="Seg_1245" n="e" s="T1484">bostə </ts>
               <ts e="T1486" id="Seg_1247" n="e" s="T1485">dʼăbaktərzittə </ts>
               <ts e="T1487" id="Seg_1249" n="e" s="T1486">ej </ts>
               <ts e="T1488" id="Seg_1251" n="e" s="T1487">molia. </ts>
               <ts e="T1489" id="Seg_1253" n="e" s="T1488">Dĭgəttə </ts>
               <ts e="T1490" id="Seg_1255" n="e" s="T1489">bazoʔ </ts>
               <ts e="T1491" id="Seg_1257" n="e" s="T1490">surarlaʔbə. </ts>
               <ts e="T1492" id="Seg_1259" n="e" s="T1491">"Ej </ts>
               <ts e="T1493" id="Seg_1261" n="e" s="T1492">šĭšəge". </ts>
               <ts e="T1494" id="Seg_1263" n="e" s="T1493">Bostən </ts>
               <ts e="T1495" id="Seg_1265" n="e" s="T1494">šĭket </ts>
               <ts e="T1496" id="Seg_1267" n="e" s="T1495">dĭ </ts>
               <ts e="T1497" id="Seg_1269" n="e" s="T1496">bar </ts>
               <ts e="T1498" id="Seg_1271" n="e" s="T1497">kănnaːmbi. </ts>
               <ts e="T1499" id="Seg_1273" n="e" s="T1498">Dĭgəttə </ts>
               <ts e="T1500" id="Seg_1275" n="e" s="T1499">dĭ </ts>
               <ts e="T1501" id="Seg_1277" n="e" s="T1500">dĭm </ts>
               <ts e="T1502" id="Seg_1279" n="e" s="T1501">adʼejalaziʔ </ts>
               <ts e="T1503" id="Seg_1281" n="e" s="T1502">šurbi. </ts>
               <ts e="T1504" id="Seg_1283" n="e" s="T1503">Palʼto </ts>
               <ts e="T1505" id="Seg_1285" n="e" s="T1504">šerbi. </ts>
               <ts e="T1506" id="Seg_1287" n="e" s="T1505">Dĭ </ts>
               <ts e="T1507" id="Seg_1289" n="e" s="T1506">ejümneʔpi. </ts>
               <ts e="T1508" id="Seg_1291" n="e" s="T1507">Dĭgəttə </ts>
               <ts e="T1509" id="Seg_1293" n="e" s="T1508">nüke </ts>
               <ts e="T1510" id="Seg_1295" n="e" s="T1509">pürleʔbə </ts>
               <ts e="T1511" id="Seg_1297" n="e" s="T1510">blinəʔi. </ts>
               <ts e="T1512" id="Seg_1299" n="e" s="T1511">"Kanaʔ, </ts>
               <ts e="T1513" id="Seg_1301" n="e" s="T1512">koʔbdo </ts>
               <ts e="T1514" id="Seg_1303" n="e" s="T1513">külaːmbi, </ts>
               <ts e="T1515" id="Seg_1305" n="e" s="T1514">dettə!" </ts>
               <ts e="T1516" id="Seg_1307" n="e" s="T1515">Dĭgəttə </ts>
               <ts e="T1517" id="Seg_1309" n="e" s="T1516">men </ts>
               <ts e="T1518" id="Seg_1311" n="e" s="T1517">toldə </ts>
               <ts e="T1519" id="Seg_1313" n="e" s="T1518">jilgəndə </ts>
               <ts e="T1520" id="Seg_1315" n="e" s="T1519">amnolaʔbə. </ts>
               <ts e="T1521" id="Seg_1317" n="e" s="T1520">Bostə </ts>
               <ts e="T1522" id="Seg_1319" n="e" s="T1521">măllaʔbə: </ts>
               <ts e="T1523" id="Seg_1321" n="e" s="T1522">"Büzʼen </ts>
               <ts e="T1524" id="Seg_1323" n="e" s="T1523">koʔbdot </ts>
               <ts e="T1525" id="Seg_1325" n="e" s="T1524">šonəga. </ts>
               <ts e="T1526" id="Seg_1327" n="e" s="T1525">(Iʔg-) </ts>
               <ts e="T1527" id="Seg_1329" n="e" s="T1526">Iʔgö </ts>
               <ts e="T1528" id="Seg_1331" n="e" s="T1527">bar </ts>
               <ts e="T1529" id="Seg_1333" n="e" s="T1528">aktʼa </ts>
               <ts e="T1530" id="Seg_1335" n="e" s="T1529">detleʔbə, </ts>
               <ts e="T1531" id="Seg_1337" n="e" s="T1530">oldʼa </ts>
               <ts e="T1532" id="Seg_1339" n="e" s="T1531">iʔgö". </ts>
               <ts e="T1533" id="Seg_1341" n="e" s="T1532">Dĭ </ts>
               <ts e="T1534" id="Seg_1343" n="e" s="T1533">măndə:" </ts>
               <ts e="T1535" id="Seg_1345" n="e" s="T1534">Dăre </ts>
               <ts e="T1536" id="Seg_1347" n="e" s="T1535">iʔ </ts>
               <ts e="T1537" id="Seg_1349" n="e" s="T1536">mănaʔ!" </ts>
               <ts e="T1538" id="Seg_1351" n="e" s="T1537">Büzʼen </ts>
               <ts e="T1539" id="Seg_1353" n="e" s="T1538">koʔbdobə </ts>
               <ts e="T1540" id="Seg_1355" n="e" s="T1539">băragən </ts>
               <ts e="T1541" id="Seg_1357" n="e" s="T1540">detleʔbə." </ts>
               <ts e="T1542" id="Seg_1359" n="e" s="T1541">A </ts>
               <ts e="T1543" id="Seg_1361" n="e" s="T1542">(men= </ts>
               <ts e="T1544" id="Seg_1363" n="e" s="T1543">üge= </ts>
               <ts e="T1545" id="Seg_1365" n="e" s="T1544">dĭ-) </ts>
               <ts e="T1546" id="Seg_1367" n="e" s="T1545">men </ts>
               <ts e="T1547" id="Seg_1369" n="e" s="T1546">üge </ts>
               <ts e="T1548" id="Seg_1371" n="e" s="T1547">diʔnə </ts>
               <ts e="T1549" id="Seg_1373" n="e" s="T1548">dăre </ts>
               <ts e="T1550" id="Seg_1375" n="e" s="T1549">măllaʔbə. </ts>
               <ts e="T1551" id="Seg_1377" n="e" s="T1550">Dĭgəttə </ts>
               <ts e="T1552" id="Seg_1379" n="e" s="T1551">büzʼe </ts>
               <ts e="T1553" id="Seg_1381" n="e" s="T1552">šobi. </ts>
               <ts e="T1554" id="Seg_1383" n="e" s="T1553">Karzʼina </ts>
               <ts e="T1555" id="Seg_1385" n="e" s="T1554">aktʼa </ts>
               <ts e="T1556" id="Seg_1387" n="e" s="T1555">deʔpi. </ts>
               <ts e="T1557" id="Seg_1389" n="e" s="T1556">Bar </ts>
               <ts e="T1558" id="Seg_1391" n="e" s="T1557">oldʼa </ts>
               <ts e="T1559" id="Seg_1393" n="e" s="T1558">iʔgö. </ts>
               <ts e="T1560" id="Seg_1395" n="e" s="T1559">Koʔbdot </ts>
               <ts e="T1561" id="Seg_1397" n="e" s="T1560">turanə </ts>
               <ts e="T1562" id="Seg_1399" n="e" s="T1561">šobi. </ts>
               <ts e="T1563" id="Seg_1401" n="e" s="T1562">Dĭgəttə </ts>
               <ts e="T1564" id="Seg_1403" n="e" s="T1563">dĭ </ts>
               <ts e="T1565" id="Seg_1405" n="e" s="T1564">nüke: </ts>
               <ts e="T1566" id="Seg_1407" n="e" s="T1565">"Kundə </ts>
               <ts e="T1567" id="Seg_1409" n="e" s="T1566">măn </ts>
               <ts e="T1568" id="Seg_1411" n="e" s="T1567">koʔbdobə!" </ts>
               <ts e="T1569" id="Seg_1413" n="e" s="T1568">Dĭ </ts>
               <ts e="T1570" id="Seg_1415" n="e" s="T1569">büzʼe </ts>
               <ts e="T1571" id="Seg_1417" n="e" s="T1570">(körerbi) </ts>
               <ts e="T1572" id="Seg_1419" n="e" s="T1571">ine. </ts>
               <ts e="T1573" id="Seg_1421" n="e" s="T1572">Kumbi. </ts>
               <ts e="T1574" id="Seg_1423" n="e" s="T1573">Dĭ </ts>
               <ts e="T1575" id="Seg_1425" n="e" s="T1574">koʔbdobə </ts>
               <ts e="T1576" id="Seg_1427" n="e" s="T1575">dĭn </ts>
               <ts e="T1577" id="Seg_1429" n="e" s="T1576">že </ts>
               <ts e="T1578" id="Seg_1431" n="e" s="T1577">maluʔpi </ts>
               <ts e="T1579" id="Seg_1433" n="e" s="T1578">i </ts>
               <ts e="T1580" id="Seg_1435" n="e" s="T1579">bostə </ts>
               <ts e="T1581" id="Seg_1437" n="e" s="T1580">šobi. </ts>
               <ts e="T1582" id="Seg_1439" n="e" s="T1581">Dĭgəttə </ts>
               <ts e="T1583" id="Seg_1441" n="e" s="T1582">bazoʔ </ts>
               <ts e="T1584" id="Seg_1443" n="e" s="T1583">šĭšəge </ts>
               <ts e="T1585" id="Seg_1445" n="e" s="T1584">dĭʔnə </ts>
               <ts e="T1586" id="Seg_1447" n="e" s="T1585">šonəga. </ts>
               <ts e="T1587" id="Seg_1449" n="e" s="T1586">Dĭ </ts>
               <ts e="T1588" id="Seg_1451" n="e" s="T1587">bar:" </ts>
               <ts e="T1589" id="Seg_1453" n="e" s="T1588">Šĭšəge </ts>
               <ts e="T1590" id="Seg_1455" n="e" s="T1589">tănan?" </ts>
               <ts e="T1591" id="Seg_1457" n="e" s="T1590">"Šĭšəge, </ts>
               <ts e="T1592" id="Seg_1459" n="e" s="T1591">kănnaːmbiam, </ts>
               <ts e="T1593" id="Seg_1461" n="e" s="T1592">(iʔ= </ts>
               <ts e="T1594" id="Seg_1463" n="e" s="T1593">ha-) </ts>
               <ts e="T1595" id="Seg_1465" n="e" s="T1594">iʔ </ts>
               <ts e="T1596" id="Seg_1467" n="e" s="T1595">šoʔ </ts>
               <ts e="T1597" id="Seg_1469" n="e" s="T1596">măna!" </ts>
               <ts e="T1598" id="Seg_1471" n="e" s="T1597">Dĭ </ts>
               <ts e="T1599" id="Seg_1473" n="e" s="T1598">bazoʔ </ts>
               <ts e="T1600" id="Seg_1475" n="e" s="T1599">šonəga. </ts>
               <ts e="T1601" id="Seg_1477" n="e" s="T1600">"Šĭšəge </ts>
               <ts e="T1602" id="Seg_1479" n="e" s="T1601">tănan?" </ts>
               <ts e="T1603" id="Seg_1481" n="e" s="T1602">"Šĭšəge!" </ts>
               <ts e="T1604" id="Seg_1483" n="e" s="T1603">Davaj </ts>
               <ts e="T1605" id="Seg_1485" n="e" s="T1604">kudolzittə </ts>
               <ts e="T1606" id="Seg_1487" n="e" s="T1605">dĭm. </ts>
               <ts e="T1607" id="Seg_1489" n="e" s="T1606">Dĭgəttə </ts>
               <ts e="T1608" id="Seg_1491" n="e" s="T1607">iššo </ts>
               <ts e="T1609" id="Seg_1493" n="e" s="T1608">surarbi. </ts>
               <ts e="T1610" id="Seg_1495" n="e" s="T1609">Dĭ </ts>
               <ts e="T1611" id="Seg_1497" n="e" s="T1610">bazoʔ </ts>
               <ts e="T1612" id="Seg_1499" n="e" s="T1611">kudolbi </ts>
               <ts e="T1613" id="Seg_1501" n="e" s="T1612">vsʼaka, </ts>
               <ts e="T1614" id="Seg_1503" n="e" s="T1613">mut-mat </ts>
               <ts e="T1615" id="Seg_1505" n="e" s="T1614">kudolbi. </ts>
               <ts e="T1616" id="Seg_1507" n="e" s="T1615">Dĭ </ts>
               <ts e="T1617" id="Seg_1509" n="e" s="T1616">dĭm </ts>
               <ts e="T1618" id="Seg_1511" n="e" s="T1617">bar </ts>
               <ts e="T1619" id="Seg_1513" n="e" s="T1618">kăndəbi. </ts>
               <ts e="T1620" id="Seg_1515" n="e" s="T1619">I </ts>
               <ts e="T1621" id="Seg_1517" n="e" s="T1620">külaːmbi </ts>
               <ts e="T1622" id="Seg_1519" n="e" s="T1621">bar, </ts>
               <ts e="T1623" id="Seg_1521" n="e" s="T1622">pa </ts>
               <ts e="T1624" id="Seg_1523" n="e" s="T1623">dĭrgit </ts>
               <ts e="T1625" id="Seg_1525" n="e" s="T1624">molaːmbi. </ts>
               <ts e="T1626" id="Seg_1527" n="e" s="T1625">Dĭgəttə </ts>
               <ts e="T1627" id="Seg_1529" n="e" s="T1626">ertən </ts>
               <ts e="T1628" id="Seg_1531" n="e" s="T1627">büzʼe </ts>
               <ts e="T1629" id="Seg_1533" n="e" s="T1628">uʔbdəbi. </ts>
               <ts e="T1630" id="Seg_1535" n="e" s="T1629">"Kanaʔ </ts>
               <ts e="T1631" id="Seg_1537" n="e" s="T1630">măn </ts>
               <ts e="T1632" id="Seg_1539" n="e" s="T1631">koʔbdo </ts>
               <ts e="T1633" id="Seg_1541" n="e" s="T1632">ileʔ!" </ts>
               <ts e="T1634" id="Seg_1543" n="e" s="T1633">Dĭ </ts>
               <ts e="T1635" id="Seg_1545" n="e" s="T1634">kambi. </ts>
               <ts e="T1636" id="Seg_1547" n="e" s="T1635">Men </ts>
               <ts e="T1637" id="Seg_1549" n="e" s="T1636">măndə:" </ts>
               <ts e="T1638" id="Seg_1551" n="e" s="T1637">Nüken </ts>
               <ts e="T1639" id="Seg_1553" n="e" s="T1638">koʔbdobə </ts>
               <ts e="T1640" id="Seg_1555" n="e" s="T1639">detleʔbə </ts>
               <ts e="T1641" id="Seg_1557" n="e" s="T1640">băranə </ts>
               <ts e="T1642" id="Seg_1559" n="e" s="T1641">bar! </ts>
               <ts e="T1643" id="Seg_1561" n="e" s="T1642">Külaːmbi". </ts>
               <ts e="T1644" id="Seg_1563" n="e" s="T1643">Dĭgəttə:" </ts>
               <ts e="T1645" id="Seg_1565" n="e" s="T1644">Dăre </ts>
               <ts e="T1646" id="Seg_1567" n="e" s="T1645">iʔ </ts>
               <ts e="T1647" id="Seg_1569" n="e" s="T1646">mănaʔ! </ts>
               <ts e="T1648" id="Seg_1571" n="e" s="T1647">A </ts>
               <ts e="T1649" id="Seg_1573" n="e" s="T1648">mănaʔ: </ts>
               <ts e="T1650" id="Seg_1575" n="e" s="T1649">nüken </ts>
               <ts e="T1651" id="Seg_1577" n="e" s="T1650">koʔbdot, </ts>
               <ts e="T1652" id="Seg_1579" n="e" s="T1651">dĭ </ts>
               <ts e="T1653" id="Seg_1581" n="e" s="T1652">üge </ts>
               <ts e="T1654" id="Seg_1583" n="e" s="T1653">dăre". </ts>
               <ts e="T1655" id="Seg_1585" n="e" s="T1654">Dĭgəttə </ts>
               <ts e="T1656" id="Seg_1587" n="e" s="T1655">büzʼe </ts>
               <ts e="T1657" id="Seg_1589" n="e" s="T1656">šonəga. </ts>
               <ts e="T1658" id="Seg_1591" n="e" s="T1657">Nüke </ts>
               <ts e="T1659" id="Seg_1593" n="e" s="T1658">nuʔməluʔpi. </ts>
               <ts e="T1660" id="Seg_1595" n="e" s="T1659">Kuliot </ts>
               <ts e="T1661" id="Seg_1597" n="e" s="T1660">bar. </ts>
               <ts e="T1662" id="Seg_1599" n="e" s="T1661">Koʔbdot </ts>
               <ts e="T1663" id="Seg_1601" n="e" s="T1662">külaːmbi, </ts>
               <ts e="T1664" id="Seg_1603" n="e" s="T1663">iʔbölaʔbə. </ts>
               <ts e="T1665" id="Seg_1605" n="e" s="T1664">Davaj </ts>
               <ts e="T1666" id="Seg_1607" n="e" s="T1665">dʼorzittə. </ts>
               <ts e="T1667" id="Seg_1609" n="e" s="T1666">Ĭmbidə </ts>
               <ts e="T1668" id="Seg_1611" n="e" s="T1667">ej </ts>
               <ts e="T1669" id="Seg_1613" n="e" s="T1668">alal. </ts>
               <ts e="T1670" id="Seg_1615" n="e" s="T1669">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1406" id="Seg_1616" s="T1403">PKZ_196X_Morozko_flk.001 (001)</ta>
            <ta e="T1409" id="Seg_1617" s="T1406">PKZ_196X_Morozko_flk.002 (002)</ta>
            <ta e="T1413" id="Seg_1618" s="T1409">PKZ_196X_Morozko_flk.003 (003)</ta>
            <ta e="T1419" id="Seg_1619" s="T1413">PKZ_196X_Morozko_flk.004 (004)</ta>
            <ta e="T1421" id="Seg_1620" s="T1419">PKZ_196X_Morozko_flk.005 (005)</ta>
            <ta e="T1426" id="Seg_1621" s="T1421">PKZ_196X_Morozko_flk.006 (006)</ta>
            <ta e="T1429" id="Seg_1622" s="T1426">PKZ_196X_Morozko_flk.007 (007)</ta>
            <ta e="T1436" id="Seg_1623" s="T1429">PKZ_196X_Morozko_flk.008 (008) </ta>
            <ta e="T1443" id="Seg_1624" s="T1436">PKZ_196X_Morozko_flk.009 (010)</ta>
            <ta e="T1448" id="Seg_1625" s="T1443">PKZ_196X_Morozko_flk.010 (011)</ta>
            <ta e="T1450" id="Seg_1626" s="T1448">PKZ_196X_Morozko_flk.011 (012)</ta>
            <ta e="T1457" id="Seg_1627" s="T1450">PKZ_196X_Morozko_flk.012 (013)</ta>
            <ta e="T1460" id="Seg_1628" s="T1457">PKZ_196X_Morozko_flk.013 (014)</ta>
            <ta e="T1462" id="Seg_1629" s="T1460">PKZ_196X_Morozko_flk.014 (015)</ta>
            <ta e="T1467" id="Seg_1630" s="T1462">PKZ_196X_Morozko_flk.015 (016)</ta>
            <ta e="T1471" id="Seg_1631" s="T1467">PKZ_196X_Morozko_flk.016 (017) </ta>
            <ta e="T1477" id="Seg_1632" s="T1471">PKZ_196X_Morozko_flk.017 (019)</ta>
            <ta e="T1478" id="Seg_1633" s="T1477">PKZ_196X_Morozko_flk.018 (020)</ta>
            <ta e="T1480" id="Seg_1634" s="T1478">PKZ_196X_Morozko_flk.019 (021)</ta>
            <ta e="T1483" id="Seg_1635" s="T1480">PKZ_196X_Morozko_flk.020 (022)</ta>
            <ta e="T1488" id="Seg_1636" s="T1483">PKZ_196X_Morozko_flk.021 (023)</ta>
            <ta e="T1491" id="Seg_1637" s="T1488">PKZ_196X_Morozko_flk.022 (024)</ta>
            <ta e="T1493" id="Seg_1638" s="T1491">PKZ_196X_Morozko_flk.023 (025)</ta>
            <ta e="T1498" id="Seg_1639" s="T1493">PKZ_196X_Morozko_flk.024 (026)</ta>
            <ta e="T1503" id="Seg_1640" s="T1498">PKZ_196X_Morozko_flk.025 (027)</ta>
            <ta e="T1505" id="Seg_1641" s="T1503">PKZ_196X_Morozko_flk.026 (028)</ta>
            <ta e="T1507" id="Seg_1642" s="T1505">PKZ_196X_Morozko_flk.027 (029)</ta>
            <ta e="T1511" id="Seg_1643" s="T1507">PKZ_196X_Morozko_flk.028 (030)</ta>
            <ta e="T1515" id="Seg_1644" s="T1511">PKZ_196X_Morozko_flk.029 (031)</ta>
            <ta e="T1520" id="Seg_1645" s="T1515">PKZ_196X_Morozko_flk.030 (032)</ta>
            <ta e="T1525" id="Seg_1646" s="T1520">PKZ_196X_Morozko_flk.031 (033) </ta>
            <ta e="T1532" id="Seg_1647" s="T1525">PKZ_196X_Morozko_flk.032 (035)</ta>
            <ta e="T1537" id="Seg_1648" s="T1532">PKZ_196X_Morozko_flk.033 (036)</ta>
            <ta e="T1541" id="Seg_1649" s="T1537">PKZ_196X_Morozko_flk.034 (037)</ta>
            <ta e="T1550" id="Seg_1650" s="T1541">PKZ_196X_Morozko_flk.035 (038)</ta>
            <ta e="T1553" id="Seg_1651" s="T1550">PKZ_196X_Morozko_flk.036 (039)</ta>
            <ta e="T1556" id="Seg_1652" s="T1553">PKZ_196X_Morozko_flk.037 (040)</ta>
            <ta e="T1559" id="Seg_1653" s="T1556">PKZ_196X_Morozko_flk.038 (041)</ta>
            <ta e="T1562" id="Seg_1654" s="T1559">PKZ_196X_Morozko_flk.039 (042)</ta>
            <ta e="T1568" id="Seg_1655" s="T1562">PKZ_196X_Morozko_flk.040 (043)</ta>
            <ta e="T1572" id="Seg_1656" s="T1568">PKZ_196X_Morozko_flk.041 (045)</ta>
            <ta e="T1573" id="Seg_1657" s="T1572">PKZ_196X_Morozko_flk.042 (046)</ta>
            <ta e="T1581" id="Seg_1658" s="T1573">PKZ_196X_Morozko_flk.043 (047)</ta>
            <ta e="T1586" id="Seg_1659" s="T1581">PKZ_196X_Morozko_flk.044 (048)</ta>
            <ta e="T1590" id="Seg_1660" s="T1586">PKZ_196X_Morozko_flk.045 (049)</ta>
            <ta e="T1597" id="Seg_1661" s="T1590">PKZ_196X_Morozko_flk.046 (050)</ta>
            <ta e="T1600" id="Seg_1662" s="T1597">PKZ_196X_Morozko_flk.047 (051)</ta>
            <ta e="T1602" id="Seg_1663" s="T1600">PKZ_196X_Morozko_flk.048 (052)</ta>
            <ta e="T1603" id="Seg_1664" s="T1602">PKZ_196X_Morozko_flk.049 (053)</ta>
            <ta e="T1606" id="Seg_1665" s="T1603">PKZ_196X_Morozko_flk.050 (054)</ta>
            <ta e="T1609" id="Seg_1666" s="T1606">PKZ_196X_Morozko_flk.051 (055)</ta>
            <ta e="T1615" id="Seg_1667" s="T1609">PKZ_196X_Morozko_flk.052 (056)</ta>
            <ta e="T1619" id="Seg_1668" s="T1615">PKZ_196X_Morozko_flk.053 (057)</ta>
            <ta e="T1625" id="Seg_1669" s="T1619">PKZ_196X_Morozko_flk.054 (058)</ta>
            <ta e="T1629" id="Seg_1670" s="T1625">PKZ_196X_Morozko_flk.055 (059)</ta>
            <ta e="T1633" id="Seg_1671" s="T1629">PKZ_196X_Morozko_flk.056 (060)</ta>
            <ta e="T1635" id="Seg_1672" s="T1633">PKZ_196X_Morozko_flk.057 (061)</ta>
            <ta e="T1642" id="Seg_1673" s="T1635">PKZ_196X_Morozko_flk.058 (062)</ta>
            <ta e="T1643" id="Seg_1674" s="T1642">PKZ_196X_Morozko_flk.059 (063)</ta>
            <ta e="T1647" id="Seg_1675" s="T1643">PKZ_196X_Morozko_flk.060 (064)</ta>
            <ta e="T1654" id="Seg_1676" s="T1647">PKZ_196X_Morozko_flk.061 (065)</ta>
            <ta e="T1657" id="Seg_1677" s="T1654">PKZ_196X_Morozko_flk.062 (066)</ta>
            <ta e="T1659" id="Seg_1678" s="T1657">PKZ_196X_Morozko_flk.063 (067)</ta>
            <ta e="T1661" id="Seg_1679" s="T1659">PKZ_196X_Morozko_flk.064 (068)</ta>
            <ta e="T1664" id="Seg_1680" s="T1661">PKZ_196X_Morozko_flk.065 (069)</ta>
            <ta e="T1666" id="Seg_1681" s="T1664">PKZ_196X_Morozko_flk.066 (070)</ta>
            <ta e="T1669" id="Seg_1682" s="T1666">PKZ_196X_Morozko_flk.067 (071)</ta>
            <ta e="T1670" id="Seg_1683" s="T1669">PKZ_196X_Morozko_flk.068 (072)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1406" id="Seg_1684" s="T1403">Amnobiʔi nüke, büzʼe. </ta>
            <ta e="T1409" id="Seg_1685" s="T1406">Nüken ibi koʔbdot. </ta>
            <ta e="T1413" id="Seg_1686" s="T1409">I büzʼen ibi koʔbdot. </ta>
            <ta e="T1419" id="Seg_1687" s="T1413">Dĭ nüke büzʼen koʔbdobə ej ajirbi. </ta>
            <ta e="T1421" id="Seg_1688" s="T1419">Münörbi, kudonzlaʔpi. </ta>
            <ta e="T1426" id="Seg_1689" s="T1421">A bostə koʔbdobə uge ajirlaʔbə. </ta>
            <ta e="T1429" id="Seg_1690" s="T1426">Ulut bar dʼüʔlaʔbə. </ta>
            <ta e="T1436" id="Seg_1691" s="T1429">Dĭgəttə (m-) büzʼenə măndə: "Kundə boslə koʔbdo. </ta>
            <ta e="T1443" id="Seg_1692" s="T1436">(Măn dĭm=) Dĭ măna ĭmbidə ej alia". </ta>
            <ta e="T1448" id="Seg_1693" s="T1443">Dĭ inem kürerbi, koʔbdobə amnolbi. </ta>
            <ta e="T1450" id="Seg_1694" s="T1448">Kunnaːmbi dʼijenə. </ta>
            <ta e="T1457" id="Seg_1695" s="T1450">Barəʔluʔpi (pan) (tondə) (nə-), (š-) sĭre urgo. </ta>
            <ta e="T1460" id="Seg_1696" s="T1457">Dĭ amnolaʔbə, kănnalaʔbə. </ta>
            <ta e="T1462" id="Seg_1697" s="T1460">Šĭšəge šonəga. </ta>
            <ta e="T1467" id="Seg_1698" s="T1462">(Pa- paʔ-) Pagəʔ panə suʔmileʔbə. </ta>
            <ta e="T1471" id="Seg_1699" s="T1467">Măndə dĭʔnə: "Tănan šĭšəge!" </ta>
            <ta e="T1477" id="Seg_1700" s="T1471">"Dʼok, măna (ej=) ej šĭšəge iššo". </ta>
            <ta e="T1478" id="Seg_1701" s="T1477">Šonəga. </ta>
            <ta e="T1480" id="Seg_1702" s="T1478">"Šĭšəge tănan?" </ta>
            <ta e="T1483" id="Seg_1703" s="T1480">"Ej šĭšəge, ejü". </ta>
            <ta e="T1488" id="Seg_1704" s="T1483">A bostə dʼăbaktərzittə ej molia. </ta>
            <ta e="T1491" id="Seg_1705" s="T1488">Dĭgəttə bazoʔ surarlaʔbə. </ta>
            <ta e="T1493" id="Seg_1706" s="T1491">"Ej šĭšəge". </ta>
            <ta e="T1498" id="Seg_1707" s="T1493">Bostən šĭket dĭ bar kănnaːmbi. </ta>
            <ta e="T1503" id="Seg_1708" s="T1498">Dĭgəttə dĭ dĭm adʼejalaziʔ šurbi. </ta>
            <ta e="T1505" id="Seg_1709" s="T1503">Palʼto šerbi. </ta>
            <ta e="T1507" id="Seg_1710" s="T1505">Dĭ ejümneʔpi. </ta>
            <ta e="T1511" id="Seg_1711" s="T1507">Dĭgəttə nüke pürleʔbə blinəʔi. </ta>
            <ta e="T1515" id="Seg_1712" s="T1511">"Kanaʔ, koʔbdo külaːmbi, dettə!" </ta>
            <ta e="T1520" id="Seg_1713" s="T1515">Dĭgəttə men toldə jilgəndə amnolaʔbə. </ta>
            <ta e="T1525" id="Seg_1714" s="T1520">Bostə măllaʔbə: "Büzʼen koʔbdot šonəga. </ta>
            <ta e="T1532" id="Seg_1715" s="T1525">(Iʔg-) Iʔgö bar aktʼa detleʔbə, oldʼa iʔgö". </ta>
            <ta e="T1537" id="Seg_1716" s="T1532">Dĭ măndə:" Dăre iʔ mănaʔ! </ta>
            <ta e="T1541" id="Seg_1717" s="T1537">Büzʼen koʔbdobə băragən detleʔbə." </ta>
            <ta e="T1550" id="Seg_1718" s="T1541">A (men= üge= dĭ-) men üge diʔnə dăre măllaʔbə. </ta>
            <ta e="T1553" id="Seg_1719" s="T1550">Dĭgəttə büzʼe šobi. </ta>
            <ta e="T1556" id="Seg_1720" s="T1553">Karzʼina aktʼa deʔpi. </ta>
            <ta e="T1559" id="Seg_1721" s="T1556">Bar oldʼa iʔgö. </ta>
            <ta e="T1562" id="Seg_1722" s="T1559">Koʔbdot turanə šobi. </ta>
            <ta e="T1568" id="Seg_1723" s="T1562">Dĭgəttə dĭ nüke: "Kundə măn koʔbdobə!" </ta>
            <ta e="T1572" id="Seg_1724" s="T1568">Dĭ büzʼe (körerbi) ine. </ta>
            <ta e="T1573" id="Seg_1725" s="T1572">Kumbi. </ta>
            <ta e="T1581" id="Seg_1726" s="T1573">Dĭ koʔbdobə dĭn že maluʔpi i bostə šobi. </ta>
            <ta e="T1586" id="Seg_1727" s="T1581">Dĭgəttə bazoʔ šĭšəge dĭʔnə šonəga. </ta>
            <ta e="T1590" id="Seg_1728" s="T1586">Dĭ bar:" Šĭšəge tănan?" </ta>
            <ta e="T1597" id="Seg_1729" s="T1590">"Šĭšəge, kănnaːmbiam, (iʔ= ha-) iʔ šoʔ măna!" </ta>
            <ta e="T1600" id="Seg_1730" s="T1597">Dĭ bazoʔ šonəga. </ta>
            <ta e="T1602" id="Seg_1731" s="T1600">"Šĭšəge tănan?" </ta>
            <ta e="T1603" id="Seg_1732" s="T1602">"Šĭšəge!" </ta>
            <ta e="T1606" id="Seg_1733" s="T1603">Davaj kudolzittə dĭm. </ta>
            <ta e="T1609" id="Seg_1734" s="T1606">Dĭgəttə iššo surarbi. </ta>
            <ta e="T1615" id="Seg_1735" s="T1609">Dĭ bazoʔ kudolbi vsʼaka, mut-mat kudolbi. </ta>
            <ta e="T1619" id="Seg_1736" s="T1615">Dĭ dĭm bar kăndəbi. </ta>
            <ta e="T1625" id="Seg_1737" s="T1619">I külaːmbi bar, pa dĭrgit molaːmbi. </ta>
            <ta e="T1629" id="Seg_1738" s="T1625">Dĭgəttə ertən büzʼe uʔbdəbi. </ta>
            <ta e="T1633" id="Seg_1739" s="T1629">"Kanaʔ măn koʔbdo ileʔ!" </ta>
            <ta e="T1635" id="Seg_1740" s="T1633">Dĭ kambi. </ta>
            <ta e="T1642" id="Seg_1741" s="T1635">Men măndə:" Nüken koʔbdobə detleʔbə băranə bar! </ta>
            <ta e="T1643" id="Seg_1742" s="T1642">Külaːmbi". </ta>
            <ta e="T1647" id="Seg_1743" s="T1643">Dĭgəttə:" Dăre iʔ mănaʔ! </ta>
            <ta e="T1654" id="Seg_1744" s="T1647">A mănaʔ: nüken koʔbdot, dĭ üge dăre". </ta>
            <ta e="T1657" id="Seg_1745" s="T1654">Dĭgəttə büzʼe šonəga. </ta>
            <ta e="T1659" id="Seg_1746" s="T1657">Nüke nuʔməluʔpi. </ta>
            <ta e="T1661" id="Seg_1747" s="T1659">Kuliot bar. </ta>
            <ta e="T1664" id="Seg_1748" s="T1661">Koʔbdot külaːmbi, iʔbölaʔbə. </ta>
            <ta e="T1666" id="Seg_1749" s="T1664">Davaj dʼorzittə. </ta>
            <ta e="T1669" id="Seg_1750" s="T1666">Ĭmbidə ej alal. </ta>
            <ta e="T1670" id="Seg_1751" s="T1669">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1404" id="Seg_1752" s="T1403">amno-bi-ʔi</ta>
            <ta e="T1405" id="Seg_1753" s="T1404">nüke</ta>
            <ta e="T1406" id="Seg_1754" s="T1405">büzʼe</ta>
            <ta e="T1407" id="Seg_1755" s="T1406">nüke-n</ta>
            <ta e="T1408" id="Seg_1756" s="T1407">i-bi</ta>
            <ta e="T1409" id="Seg_1757" s="T1408">koʔbdo-t</ta>
            <ta e="T1410" id="Seg_1758" s="T1409">i</ta>
            <ta e="T1411" id="Seg_1759" s="T1410">büzʼe-n</ta>
            <ta e="T1412" id="Seg_1760" s="T1411">i-bi</ta>
            <ta e="T1413" id="Seg_1761" s="T1412">koʔbdo-t</ta>
            <ta e="T1414" id="Seg_1762" s="T1413">dĭ</ta>
            <ta e="T1415" id="Seg_1763" s="T1414">nüke</ta>
            <ta e="T1416" id="Seg_1764" s="T1415">büzʼe-n</ta>
            <ta e="T1417" id="Seg_1765" s="T1416">koʔbdo-bə</ta>
            <ta e="T1418" id="Seg_1766" s="T1417">ej</ta>
            <ta e="T1419" id="Seg_1767" s="T1418">ajir-bi</ta>
            <ta e="T1420" id="Seg_1768" s="T1419">münör-bi</ta>
            <ta e="T1421" id="Seg_1769" s="T1420">kudo-nz-laʔ-pi</ta>
            <ta e="T1422" id="Seg_1770" s="T1421">a</ta>
            <ta e="T1423" id="Seg_1771" s="T1422">bos-tə</ta>
            <ta e="T1424" id="Seg_1772" s="T1423">koʔbdo-bə</ta>
            <ta e="T1425" id="Seg_1773" s="T1424">uge</ta>
            <ta e="T1426" id="Seg_1774" s="T1425">ajir-laʔbə</ta>
            <ta e="T1427" id="Seg_1775" s="T1426">ulu-t</ta>
            <ta e="T1428" id="Seg_1776" s="T1427">bar</ta>
            <ta e="T1429" id="Seg_1777" s="T1428">dʼüʔ-laʔbə</ta>
            <ta e="T1430" id="Seg_1778" s="T1429">dĭgəttə</ta>
            <ta e="T1432" id="Seg_1779" s="T1431">büzʼe-nə</ta>
            <ta e="T1433" id="Seg_1780" s="T1432">măn-də</ta>
            <ta e="T1434" id="Seg_1781" s="T1433">kun-də</ta>
            <ta e="T1435" id="Seg_1782" s="T1434">bos-lə</ta>
            <ta e="T1436" id="Seg_1783" s="T1435">koʔbdo</ta>
            <ta e="T1437" id="Seg_1784" s="T1436">măn</ta>
            <ta e="T1438" id="Seg_1785" s="T1437">dĭ-m</ta>
            <ta e="T1439" id="Seg_1786" s="T1438">dĭ</ta>
            <ta e="T1440" id="Seg_1787" s="T1439">măna</ta>
            <ta e="T1441" id="Seg_1788" s="T1440">ĭmbi=də</ta>
            <ta e="T1442" id="Seg_1789" s="T1441">ej</ta>
            <ta e="T1443" id="Seg_1790" s="T1442">a-lia</ta>
            <ta e="T1444" id="Seg_1791" s="T1443">dĭ</ta>
            <ta e="T1445" id="Seg_1792" s="T1444">ine-m</ta>
            <ta e="T1446" id="Seg_1793" s="T1445">kürer-bi</ta>
            <ta e="T1447" id="Seg_1794" s="T1446">koʔbdo-bə</ta>
            <ta e="T1448" id="Seg_1795" s="T1447">amnol-bi</ta>
            <ta e="T1449" id="Seg_1796" s="T1448">kun-naːm-bi</ta>
            <ta e="T1450" id="Seg_1797" s="T1449">dʼije-nə</ta>
            <ta e="T1451" id="Seg_1798" s="T1450">barəʔ-luʔ-pi</ta>
            <ta e="T1452" id="Seg_1799" s="T1451">pa-n</ta>
            <ta e="T1453" id="Seg_1800" s="T1452">to-ndə</ta>
            <ta e="T1456" id="Seg_1801" s="T1455">sĭre</ta>
            <ta e="T1457" id="Seg_1802" s="T1456">urgo</ta>
            <ta e="T1458" id="Seg_1803" s="T1457">dĭ</ta>
            <ta e="T1459" id="Seg_1804" s="T1458">amno-laʔbə</ta>
            <ta e="T1460" id="Seg_1805" s="T1459">kănna-laʔbə</ta>
            <ta e="T1461" id="Seg_1806" s="T1460">šĭšəge</ta>
            <ta e="T1462" id="Seg_1807" s="T1461">šonə-ga</ta>
            <ta e="T1465" id="Seg_1808" s="T1464">pa-gəʔ</ta>
            <ta e="T1466" id="Seg_1809" s="T1465">pa-nə</ta>
            <ta e="T1467" id="Seg_1810" s="T1466">suʔmi-leʔbə</ta>
            <ta e="T1468" id="Seg_1811" s="T1467">măn-də</ta>
            <ta e="T1469" id="Seg_1812" s="T1468">dĭʔ-nə</ta>
            <ta e="T1470" id="Seg_1813" s="T1469">tănan</ta>
            <ta e="T1471" id="Seg_1814" s="T1470">šĭšəge</ta>
            <ta e="T1472" id="Seg_1815" s="T1471">dʼok</ta>
            <ta e="T1473" id="Seg_1816" s="T1472">măna</ta>
            <ta e="T1474" id="Seg_1817" s="T1473">ej</ta>
            <ta e="T1475" id="Seg_1818" s="T1474">ej</ta>
            <ta e="T1476" id="Seg_1819" s="T1475">šĭšəge</ta>
            <ta e="T1477" id="Seg_1820" s="T1476">iššo</ta>
            <ta e="T1478" id="Seg_1821" s="T1477">šonə-ga</ta>
            <ta e="T1479" id="Seg_1822" s="T1478">šĭšəge</ta>
            <ta e="T1480" id="Seg_1823" s="T1479">tănan</ta>
            <ta e="T1481" id="Seg_1824" s="T1480">ej</ta>
            <ta e="T1482" id="Seg_1825" s="T1481">šĭšəge</ta>
            <ta e="T1483" id="Seg_1826" s="T1482">ejü</ta>
            <ta e="T1484" id="Seg_1827" s="T1483">a</ta>
            <ta e="T1485" id="Seg_1828" s="T1484">bos-tə</ta>
            <ta e="T1486" id="Seg_1829" s="T1485">dʼăbaktər-zittə</ta>
            <ta e="T1487" id="Seg_1830" s="T1486">ej</ta>
            <ta e="T1488" id="Seg_1831" s="T1487">mo-lia</ta>
            <ta e="T1489" id="Seg_1832" s="T1488">dĭgəttə</ta>
            <ta e="T1490" id="Seg_1833" s="T1489">bazoʔ</ta>
            <ta e="T1491" id="Seg_1834" s="T1490">surar-laʔbə</ta>
            <ta e="T1492" id="Seg_1835" s="T1491">ej</ta>
            <ta e="T1493" id="Seg_1836" s="T1492">šĭšəge</ta>
            <ta e="T1494" id="Seg_1837" s="T1493">bostə-n</ta>
            <ta e="T1495" id="Seg_1838" s="T1494">šĭke-t</ta>
            <ta e="T1496" id="Seg_1839" s="T1495">dĭ</ta>
            <ta e="T1497" id="Seg_1840" s="T1496">bar</ta>
            <ta e="T1498" id="Seg_1841" s="T1497">kăn-naːm-bi</ta>
            <ta e="T1499" id="Seg_1842" s="T1498">dĭgəttə</ta>
            <ta e="T1500" id="Seg_1843" s="T1499">dĭ</ta>
            <ta e="T1501" id="Seg_1844" s="T1500">dĭ-m</ta>
            <ta e="T1502" id="Seg_1845" s="T1501">adʼejala-ziʔ</ta>
            <ta e="T1503" id="Seg_1846" s="T1502">šur-bi</ta>
            <ta e="T1504" id="Seg_1847" s="T1503">palʼto</ta>
            <ta e="T1505" id="Seg_1848" s="T1504">šer-bi</ta>
            <ta e="T1506" id="Seg_1849" s="T1505">dĭ</ta>
            <ta e="T1507" id="Seg_1850" s="T1506">ejü-m-neʔ-pi</ta>
            <ta e="T1508" id="Seg_1851" s="T1507">dĭgəttə</ta>
            <ta e="T1509" id="Seg_1852" s="T1508">nüke</ta>
            <ta e="T1510" id="Seg_1853" s="T1509">pür-leʔbə</ta>
            <ta e="T1511" id="Seg_1854" s="T1510">blin-əʔi</ta>
            <ta e="T1512" id="Seg_1855" s="T1511">kan-a-ʔ</ta>
            <ta e="T1513" id="Seg_1856" s="T1512">koʔbdo</ta>
            <ta e="T1514" id="Seg_1857" s="T1513">kü-laːm-bi</ta>
            <ta e="T1515" id="Seg_1858" s="T1514">det-tə</ta>
            <ta e="T1516" id="Seg_1859" s="T1515">dĭgəttə</ta>
            <ta e="T1517" id="Seg_1860" s="T1516">men</ta>
            <ta e="T1518" id="Seg_1861" s="T1517">tol-də</ta>
            <ta e="T1519" id="Seg_1862" s="T1518">jil-gəndə</ta>
            <ta e="T1520" id="Seg_1863" s="T1519">amno-laʔbə</ta>
            <ta e="T1521" id="Seg_1864" s="T1520">bos-tə</ta>
            <ta e="T1522" id="Seg_1865" s="T1521">măl-laʔbə</ta>
            <ta e="T1523" id="Seg_1866" s="T1522">büzʼe-n</ta>
            <ta e="T1524" id="Seg_1867" s="T1523">koʔbdo-t</ta>
            <ta e="T1525" id="Seg_1868" s="T1524">šonə-ga</ta>
            <ta e="T1527" id="Seg_1869" s="T1526">iʔgö</ta>
            <ta e="T1528" id="Seg_1870" s="T1527">bar</ta>
            <ta e="T1529" id="Seg_1871" s="T1528">aktʼa</ta>
            <ta e="T1530" id="Seg_1872" s="T1529">det-leʔbə</ta>
            <ta e="T1531" id="Seg_1873" s="T1530">oldʼa</ta>
            <ta e="T1532" id="Seg_1874" s="T1531">iʔgö</ta>
            <ta e="T1533" id="Seg_1875" s="T1532">dĭ</ta>
            <ta e="T1534" id="Seg_1876" s="T1533">măn-də</ta>
            <ta e="T1535" id="Seg_1877" s="T1534">dăre</ta>
            <ta e="T1536" id="Seg_1878" s="T1535">i-ʔ</ta>
            <ta e="T1537" id="Seg_1879" s="T1536">măn-a-ʔ</ta>
            <ta e="T1538" id="Seg_1880" s="T1537">büzʼe-n</ta>
            <ta e="T1539" id="Seg_1881" s="T1538">koʔbdo-bə</ta>
            <ta e="T1540" id="Seg_1882" s="T1539">băra-gən</ta>
            <ta e="T1541" id="Seg_1883" s="T1540">det-leʔbə</ta>
            <ta e="T1542" id="Seg_1884" s="T1541">a</ta>
            <ta e="T1543" id="Seg_1885" s="T1542">men</ta>
            <ta e="T1544" id="Seg_1886" s="T1543">üge</ta>
            <ta e="T1546" id="Seg_1887" s="T1545">men</ta>
            <ta e="T1547" id="Seg_1888" s="T1546">üge</ta>
            <ta e="T1548" id="Seg_1889" s="T1547">diʔ-nə</ta>
            <ta e="T1549" id="Seg_1890" s="T1548">dăre</ta>
            <ta e="T1550" id="Seg_1891" s="T1549">măl-laʔbə</ta>
            <ta e="T1551" id="Seg_1892" s="T1550">dĭgəttə</ta>
            <ta e="T1552" id="Seg_1893" s="T1551">büzʼe</ta>
            <ta e="T1553" id="Seg_1894" s="T1552">šo-bi</ta>
            <ta e="T1554" id="Seg_1895" s="T1553">karzʼina</ta>
            <ta e="T1555" id="Seg_1896" s="T1554">aktʼa</ta>
            <ta e="T1556" id="Seg_1897" s="T1555">deʔ-pi</ta>
            <ta e="T1557" id="Seg_1898" s="T1556">bar</ta>
            <ta e="T1558" id="Seg_1899" s="T1557">oldʼa</ta>
            <ta e="T1559" id="Seg_1900" s="T1558">iʔgö</ta>
            <ta e="T1560" id="Seg_1901" s="T1559">koʔbdo-t</ta>
            <ta e="T1561" id="Seg_1902" s="T1560">tura-nə</ta>
            <ta e="T1562" id="Seg_1903" s="T1561">šo-bi</ta>
            <ta e="T1563" id="Seg_1904" s="T1562">dĭgəttə</ta>
            <ta e="T1564" id="Seg_1905" s="T1563">dĭ</ta>
            <ta e="T1565" id="Seg_1906" s="T1564">nüke</ta>
            <ta e="T1566" id="Seg_1907" s="T1565">kun-də</ta>
            <ta e="T1567" id="Seg_1908" s="T1566">măn</ta>
            <ta e="T1568" id="Seg_1909" s="T1567">koʔbdo-bə</ta>
            <ta e="T1569" id="Seg_1910" s="T1568">dĭ</ta>
            <ta e="T1570" id="Seg_1911" s="T1569">büzʼe</ta>
            <ta e="T1571" id="Seg_1912" s="T1570">körer-bi</ta>
            <ta e="T1572" id="Seg_1913" s="T1571">ine</ta>
            <ta e="T1573" id="Seg_1914" s="T1572">kum-bi</ta>
            <ta e="T1574" id="Seg_1915" s="T1573">dĭ</ta>
            <ta e="T1575" id="Seg_1916" s="T1574">koʔbdo-bə</ta>
            <ta e="T1576" id="Seg_1917" s="T1575">dĭn</ta>
            <ta e="T1577" id="Seg_1918" s="T1576">že</ta>
            <ta e="T1578" id="Seg_1919" s="T1577">ma-luʔ-pi</ta>
            <ta e="T1579" id="Seg_1920" s="T1578">i</ta>
            <ta e="T1580" id="Seg_1921" s="T1579">bos-tə</ta>
            <ta e="T1581" id="Seg_1922" s="T1580">šo-bi</ta>
            <ta e="T1582" id="Seg_1923" s="T1581">dĭgəttə</ta>
            <ta e="T1583" id="Seg_1924" s="T1582">bazoʔ</ta>
            <ta e="T1584" id="Seg_1925" s="T1583">šĭšəge</ta>
            <ta e="T1585" id="Seg_1926" s="T1584">dĭʔ-nə</ta>
            <ta e="T1586" id="Seg_1927" s="T1585">šonə-ga</ta>
            <ta e="T1587" id="Seg_1928" s="T1586">dĭ</ta>
            <ta e="T1588" id="Seg_1929" s="T1587">bar</ta>
            <ta e="T1589" id="Seg_1930" s="T1588">šĭšəge</ta>
            <ta e="T1590" id="Seg_1931" s="T1589">tănan</ta>
            <ta e="T1591" id="Seg_1932" s="T1590">šĭšəge</ta>
            <ta e="T1592" id="Seg_1933" s="T1591">kăn-naːm-bia-m</ta>
            <ta e="T1593" id="Seg_1934" s="T1592">i-ʔ</ta>
            <ta e="T1595" id="Seg_1935" s="T1594">i-ʔ</ta>
            <ta e="T1596" id="Seg_1936" s="T1595">šo-ʔ</ta>
            <ta e="T1597" id="Seg_1937" s="T1596">măna</ta>
            <ta e="T1598" id="Seg_1938" s="T1597">dĭ</ta>
            <ta e="T1599" id="Seg_1939" s="T1598">bazoʔ</ta>
            <ta e="T1600" id="Seg_1940" s="T1599">šonə-ga</ta>
            <ta e="T1601" id="Seg_1941" s="T1600">šĭšəge</ta>
            <ta e="T1602" id="Seg_1942" s="T1601">tănan</ta>
            <ta e="T1603" id="Seg_1943" s="T1602">šĭšəge</ta>
            <ta e="T1604" id="Seg_1944" s="T1603">davaj</ta>
            <ta e="T1605" id="Seg_1945" s="T1604">kudo-l-zittə</ta>
            <ta e="T1606" id="Seg_1946" s="T1605">dĭ-m</ta>
            <ta e="T1607" id="Seg_1947" s="T1606">dĭgəttə</ta>
            <ta e="T1608" id="Seg_1948" s="T1607">iššo</ta>
            <ta e="T1609" id="Seg_1949" s="T1608">surar-bi</ta>
            <ta e="T1610" id="Seg_1950" s="T1609">dĭ</ta>
            <ta e="T1611" id="Seg_1951" s="T1610">bazoʔ</ta>
            <ta e="T1612" id="Seg_1952" s="T1611">kudo-l-bi</ta>
            <ta e="T1613" id="Seg_1953" s="T1612">vsʼaka</ta>
            <ta e="T1614" id="Seg_1954" s="T1613">mut_mat</ta>
            <ta e="T1615" id="Seg_1955" s="T1614">kudo-l-bi</ta>
            <ta e="T1616" id="Seg_1956" s="T1615">dĭ</ta>
            <ta e="T1617" id="Seg_1957" s="T1616">dĭ-m</ta>
            <ta e="T1618" id="Seg_1958" s="T1617">bar</ta>
            <ta e="T1619" id="Seg_1959" s="T1618">kăn-də-bi</ta>
            <ta e="T1620" id="Seg_1960" s="T1619">i</ta>
            <ta e="T1621" id="Seg_1961" s="T1620">kü-laːm-bi</ta>
            <ta e="T1622" id="Seg_1962" s="T1621">bar</ta>
            <ta e="T1623" id="Seg_1963" s="T1622">pa</ta>
            <ta e="T1624" id="Seg_1964" s="T1623">dĭrgit</ta>
            <ta e="T1625" id="Seg_1965" s="T1624">mo-laːm-bi</ta>
            <ta e="T1626" id="Seg_1966" s="T1625">dĭgəttə</ta>
            <ta e="T1627" id="Seg_1967" s="T1626">ertə-n</ta>
            <ta e="T1628" id="Seg_1968" s="T1627">büzʼe</ta>
            <ta e="T1629" id="Seg_1969" s="T1628">uʔbdə-bi</ta>
            <ta e="T1630" id="Seg_1970" s="T1629">kan-a-ʔ</ta>
            <ta e="T1631" id="Seg_1971" s="T1630">măn</ta>
            <ta e="T1632" id="Seg_1972" s="T1631">koʔbdo</ta>
            <ta e="T1633" id="Seg_1973" s="T1632">i-leʔ</ta>
            <ta e="T1634" id="Seg_1974" s="T1633">dĭ</ta>
            <ta e="T1635" id="Seg_1975" s="T1634">kam-bi</ta>
            <ta e="T1636" id="Seg_1976" s="T1635">men</ta>
            <ta e="T1637" id="Seg_1977" s="T1636">măn-də</ta>
            <ta e="T1638" id="Seg_1978" s="T1637">nüke-n</ta>
            <ta e="T1639" id="Seg_1979" s="T1638">koʔbdo-bə</ta>
            <ta e="T1640" id="Seg_1980" s="T1639">det-leʔbə</ta>
            <ta e="T1641" id="Seg_1981" s="T1640">băra-nə</ta>
            <ta e="T1642" id="Seg_1982" s="T1641">bar</ta>
            <ta e="T1643" id="Seg_1983" s="T1642">kü-laːm-bi</ta>
            <ta e="T1644" id="Seg_1984" s="T1643">dĭgəttə</ta>
            <ta e="T1645" id="Seg_1985" s="T1644">dăre</ta>
            <ta e="T1646" id="Seg_1986" s="T1645">i-ʔ</ta>
            <ta e="T1647" id="Seg_1987" s="T1646">măn-a-ʔ</ta>
            <ta e="T1648" id="Seg_1988" s="T1647">a</ta>
            <ta e="T1649" id="Seg_1989" s="T1648">măn-a-ʔ</ta>
            <ta e="T1650" id="Seg_1990" s="T1649">nüke-n</ta>
            <ta e="T1651" id="Seg_1991" s="T1650">koʔbdo-t</ta>
            <ta e="T1652" id="Seg_1992" s="T1651">dĭ</ta>
            <ta e="T1653" id="Seg_1993" s="T1652">üge</ta>
            <ta e="T1654" id="Seg_1994" s="T1653">dăre</ta>
            <ta e="T1655" id="Seg_1995" s="T1654">dĭgəttə</ta>
            <ta e="T1656" id="Seg_1996" s="T1655">büzʼe</ta>
            <ta e="T1657" id="Seg_1997" s="T1656">šonə-ga</ta>
            <ta e="T1658" id="Seg_1998" s="T1657">nüke</ta>
            <ta e="T1659" id="Seg_1999" s="T1658">nuʔmə-luʔ-pi</ta>
            <ta e="T1660" id="Seg_2000" s="T1659">ku-lio-t</ta>
            <ta e="T1661" id="Seg_2001" s="T1660">bar</ta>
            <ta e="T1662" id="Seg_2002" s="T1661">koʔbdo-t</ta>
            <ta e="T1663" id="Seg_2003" s="T1662">kü-laːm-bi</ta>
            <ta e="T1664" id="Seg_2004" s="T1663">iʔbö-laʔbə</ta>
            <ta e="T1665" id="Seg_2005" s="T1664">davaj</ta>
            <ta e="T1666" id="Seg_2006" s="T1665">dʼor-zittə</ta>
            <ta e="T1667" id="Seg_2007" s="T1666">ĭmbi=də</ta>
            <ta e="T1668" id="Seg_2008" s="T1667">ej</ta>
            <ta e="T1669" id="Seg_2009" s="T1668">a-la-l</ta>
            <ta e="T1670" id="Seg_2010" s="T1669">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1404" id="Seg_2011" s="T1403">amno-bi-jəʔ</ta>
            <ta e="T1405" id="Seg_2012" s="T1404">nüke</ta>
            <ta e="T1406" id="Seg_2013" s="T1405">büzʼe</ta>
            <ta e="T1407" id="Seg_2014" s="T1406">nüke-n</ta>
            <ta e="T1408" id="Seg_2015" s="T1407">i-bi</ta>
            <ta e="T1409" id="Seg_2016" s="T1408">koʔbdo-t</ta>
            <ta e="T1410" id="Seg_2017" s="T1409">i</ta>
            <ta e="T1411" id="Seg_2018" s="T1410">büzʼe-n</ta>
            <ta e="T1412" id="Seg_2019" s="T1411">i-bi</ta>
            <ta e="T1413" id="Seg_2020" s="T1412">koʔbdo-t</ta>
            <ta e="T1414" id="Seg_2021" s="T1413">dĭ</ta>
            <ta e="T1415" id="Seg_2022" s="T1414">nüke</ta>
            <ta e="T1416" id="Seg_2023" s="T1415">büzʼe-n</ta>
            <ta e="T1417" id="Seg_2024" s="T1416">koʔbdo-bə</ta>
            <ta e="T1418" id="Seg_2025" s="T1417">ej</ta>
            <ta e="T1419" id="Seg_2026" s="T1418">ajir-bi</ta>
            <ta e="T1420" id="Seg_2027" s="T1419">münör-bi</ta>
            <ta e="T1421" id="Seg_2028" s="T1420">kudo-nzə-laʔbə-bi</ta>
            <ta e="T1422" id="Seg_2029" s="T1421">a</ta>
            <ta e="T1423" id="Seg_2030" s="T1422">bos-də</ta>
            <ta e="T1424" id="Seg_2031" s="T1423">koʔbdo-bə</ta>
            <ta e="T1425" id="Seg_2032" s="T1424">üge</ta>
            <ta e="T1426" id="Seg_2033" s="T1425">ajir-laʔbə</ta>
            <ta e="T1427" id="Seg_2034" s="T1426">ulu-t</ta>
            <ta e="T1428" id="Seg_2035" s="T1427">bar</ta>
            <ta e="T1429" id="Seg_2036" s="T1428">dʼüʔ-laʔbə</ta>
            <ta e="T1430" id="Seg_2037" s="T1429">dĭgəttə</ta>
            <ta e="T1432" id="Seg_2038" s="T1431">büzʼe-Tə</ta>
            <ta e="T1433" id="Seg_2039" s="T1432">măn-ntə</ta>
            <ta e="T1434" id="Seg_2040" s="T1433">kun-t</ta>
            <ta e="T1435" id="Seg_2041" s="T1434">bos-l</ta>
            <ta e="T1436" id="Seg_2042" s="T1435">koʔbdo</ta>
            <ta e="T1437" id="Seg_2043" s="T1436">măn</ta>
            <ta e="T1438" id="Seg_2044" s="T1437">dĭ-m</ta>
            <ta e="T1439" id="Seg_2045" s="T1438">dĭ</ta>
            <ta e="T1440" id="Seg_2046" s="T1439">măna</ta>
            <ta e="T1441" id="Seg_2047" s="T1440">ĭmbi=də</ta>
            <ta e="T1442" id="Seg_2048" s="T1441">ej</ta>
            <ta e="T1443" id="Seg_2049" s="T1442">a-liA</ta>
            <ta e="T1444" id="Seg_2050" s="T1443">dĭ</ta>
            <ta e="T1445" id="Seg_2051" s="T1444">ine-m</ta>
            <ta e="T1446" id="Seg_2052" s="T1445">körer-bi</ta>
            <ta e="T1447" id="Seg_2053" s="T1446">koʔbdo-bə</ta>
            <ta e="T1448" id="Seg_2054" s="T1447">amnol-bi</ta>
            <ta e="T1449" id="Seg_2055" s="T1448">kun-laːm-bi</ta>
            <ta e="T1450" id="Seg_2056" s="T1449">dʼije-Tə</ta>
            <ta e="T1451" id="Seg_2057" s="T1450">barəʔ-luʔbdə-bi</ta>
            <ta e="T1452" id="Seg_2058" s="T1451">pa-n</ta>
            <ta e="T1453" id="Seg_2059" s="T1452">toʔ-gəndə</ta>
            <ta e="T1456" id="Seg_2060" s="T1455">sĭri</ta>
            <ta e="T1457" id="Seg_2061" s="T1456">urgo</ta>
            <ta e="T1458" id="Seg_2062" s="T1457">dĭ</ta>
            <ta e="T1459" id="Seg_2063" s="T1458">amnə-laʔbə</ta>
            <ta e="T1460" id="Seg_2064" s="T1459">kănna-laʔbə</ta>
            <ta e="T1461" id="Seg_2065" s="T1460">šišəge</ta>
            <ta e="T1462" id="Seg_2066" s="T1461">šonə-gA</ta>
            <ta e="T1465" id="Seg_2067" s="T1464">pa-gəʔ</ta>
            <ta e="T1466" id="Seg_2068" s="T1465">pa-Tə</ta>
            <ta e="T1467" id="Seg_2069" s="T1466">süʔmə-laʔbə</ta>
            <ta e="T1468" id="Seg_2070" s="T1467">măn-ntə</ta>
            <ta e="T1469" id="Seg_2071" s="T1468">dĭ-Tə</ta>
            <ta e="T1470" id="Seg_2072" s="T1469">tănan</ta>
            <ta e="T1471" id="Seg_2073" s="T1470">šišəge</ta>
            <ta e="T1472" id="Seg_2074" s="T1471">dʼok</ta>
            <ta e="T1473" id="Seg_2075" s="T1472">măna</ta>
            <ta e="T1474" id="Seg_2076" s="T1473">ej</ta>
            <ta e="T1475" id="Seg_2077" s="T1474">ej</ta>
            <ta e="T1476" id="Seg_2078" s="T1475">šišəge</ta>
            <ta e="T1477" id="Seg_2079" s="T1476">ĭššo</ta>
            <ta e="T1478" id="Seg_2080" s="T1477">šonə-gA</ta>
            <ta e="T1479" id="Seg_2081" s="T1478">šišəge</ta>
            <ta e="T1480" id="Seg_2082" s="T1479">tănan</ta>
            <ta e="T1481" id="Seg_2083" s="T1480">ej</ta>
            <ta e="T1482" id="Seg_2084" s="T1481">šišəge</ta>
            <ta e="T1483" id="Seg_2085" s="T1482">ejü</ta>
            <ta e="T1484" id="Seg_2086" s="T1483">a</ta>
            <ta e="T1485" id="Seg_2087" s="T1484">bos-də</ta>
            <ta e="T1486" id="Seg_2088" s="T1485">tʼăbaktər-zittə</ta>
            <ta e="T1487" id="Seg_2089" s="T1486">ej</ta>
            <ta e="T1488" id="Seg_2090" s="T1487">mo-liA</ta>
            <ta e="T1489" id="Seg_2091" s="T1488">dĭgəttə</ta>
            <ta e="T1490" id="Seg_2092" s="T1489">bazoʔ</ta>
            <ta e="T1491" id="Seg_2093" s="T1490">surar-laʔbə</ta>
            <ta e="T1492" id="Seg_2094" s="T1491">ej</ta>
            <ta e="T1493" id="Seg_2095" s="T1492">šišəge</ta>
            <ta e="T1494" id="Seg_2096" s="T1493">bostə-n</ta>
            <ta e="T1495" id="Seg_2097" s="T1494">šĭkə-t</ta>
            <ta e="T1496" id="Seg_2098" s="T1495">dĭ</ta>
            <ta e="T1497" id="Seg_2099" s="T1496">bar</ta>
            <ta e="T1498" id="Seg_2100" s="T1497">kănzə-laːm-bi</ta>
            <ta e="T1499" id="Seg_2101" s="T1498">dĭgəttə</ta>
            <ta e="T1500" id="Seg_2102" s="T1499">dĭ</ta>
            <ta e="T1501" id="Seg_2103" s="T1500">dĭ-m</ta>
            <ta e="T1502" id="Seg_2104" s="T1501">adʼejala-ziʔ</ta>
            <ta e="T1503" id="Seg_2105" s="T1502">%%-bi</ta>
            <ta e="T1504" id="Seg_2106" s="T1503">palʼto</ta>
            <ta e="T1505" id="Seg_2107" s="T1504">šer-bi</ta>
            <ta e="T1506" id="Seg_2108" s="T1505">dĭ</ta>
            <ta e="T1507" id="Seg_2109" s="T1506">ejü-m-luʔbdə-bi</ta>
            <ta e="T1508" id="Seg_2110" s="T1507">dĭgəttə</ta>
            <ta e="T1509" id="Seg_2111" s="T1508">nüke</ta>
            <ta e="T1510" id="Seg_2112" s="T1509">pür-laʔbə</ta>
            <ta e="T1511" id="Seg_2113" s="T1510">blin-jəʔ</ta>
            <ta e="T1512" id="Seg_2114" s="T1511">kan-ə-ʔ</ta>
            <ta e="T1513" id="Seg_2115" s="T1512">koʔbdo</ta>
            <ta e="T1514" id="Seg_2116" s="T1513">kü-laːm-bi</ta>
            <ta e="T1515" id="Seg_2117" s="T1514">det-t</ta>
            <ta e="T1516" id="Seg_2118" s="T1515">dĭgəttə</ta>
            <ta e="T1517" id="Seg_2119" s="T1516">men</ta>
            <ta e="T1518" id="Seg_2120" s="T1517">stol-də</ta>
            <ta e="T1519" id="Seg_2121" s="T1518">il-gəndə</ta>
            <ta e="T1520" id="Seg_2122" s="T1519">amno-laʔbə</ta>
            <ta e="T1521" id="Seg_2123" s="T1520">bos-də</ta>
            <ta e="T1522" id="Seg_2124" s="T1521">măn-laʔbə</ta>
            <ta e="T1523" id="Seg_2125" s="T1522">büzʼe-n</ta>
            <ta e="T1524" id="Seg_2126" s="T1523">koʔbdo-t</ta>
            <ta e="T1525" id="Seg_2127" s="T1524">šonə-gA</ta>
            <ta e="T1527" id="Seg_2128" s="T1526">iʔgö</ta>
            <ta e="T1528" id="Seg_2129" s="T1527">bar</ta>
            <ta e="T1529" id="Seg_2130" s="T1528">aktʼa</ta>
            <ta e="T1530" id="Seg_2131" s="T1529">det-laʔbə</ta>
            <ta e="T1531" id="Seg_2132" s="T1530">oldʼa</ta>
            <ta e="T1532" id="Seg_2133" s="T1531">iʔgö</ta>
            <ta e="T1533" id="Seg_2134" s="T1532">dĭ</ta>
            <ta e="T1534" id="Seg_2135" s="T1533">măn-ntə</ta>
            <ta e="T1535" id="Seg_2136" s="T1534">dărəʔ</ta>
            <ta e="T1536" id="Seg_2137" s="T1535">e-ʔ</ta>
            <ta e="T1537" id="Seg_2138" s="T1536">măn-ə-ʔ</ta>
            <ta e="T1538" id="Seg_2139" s="T1537">büzʼe-n</ta>
            <ta e="T1539" id="Seg_2140" s="T1538">koʔbdo-bə</ta>
            <ta e="T1540" id="Seg_2141" s="T1539">băra-Kən</ta>
            <ta e="T1541" id="Seg_2142" s="T1540">det-laʔbə</ta>
            <ta e="T1542" id="Seg_2143" s="T1541">a</ta>
            <ta e="T1543" id="Seg_2144" s="T1542">men</ta>
            <ta e="T1544" id="Seg_2145" s="T1543">üge</ta>
            <ta e="T1546" id="Seg_2146" s="T1545">men</ta>
            <ta e="T1547" id="Seg_2147" s="T1546">üge</ta>
            <ta e="T1548" id="Seg_2148" s="T1547">dĭ-Tə</ta>
            <ta e="T1549" id="Seg_2149" s="T1548">dărəʔ</ta>
            <ta e="T1550" id="Seg_2150" s="T1549">măn-laʔbə</ta>
            <ta e="T1551" id="Seg_2151" s="T1550">dĭgəttə</ta>
            <ta e="T1552" id="Seg_2152" s="T1551">büzʼe</ta>
            <ta e="T1553" id="Seg_2153" s="T1552">šo-bi</ta>
            <ta e="T1554" id="Seg_2154" s="T1553">karzʼina</ta>
            <ta e="T1555" id="Seg_2155" s="T1554">aktʼa</ta>
            <ta e="T1556" id="Seg_2156" s="T1555">det-bi</ta>
            <ta e="T1557" id="Seg_2157" s="T1556">bar</ta>
            <ta e="T1558" id="Seg_2158" s="T1557">oldʼa</ta>
            <ta e="T1559" id="Seg_2159" s="T1558">iʔgö</ta>
            <ta e="T1560" id="Seg_2160" s="T1559">koʔbdo-t</ta>
            <ta e="T1561" id="Seg_2161" s="T1560">tura-Tə</ta>
            <ta e="T1562" id="Seg_2162" s="T1561">šo-bi</ta>
            <ta e="T1563" id="Seg_2163" s="T1562">dĭgəttə</ta>
            <ta e="T1564" id="Seg_2164" s="T1563">dĭ</ta>
            <ta e="T1565" id="Seg_2165" s="T1564">nüke</ta>
            <ta e="T1566" id="Seg_2166" s="T1565">kun-t</ta>
            <ta e="T1567" id="Seg_2167" s="T1566">măn</ta>
            <ta e="T1568" id="Seg_2168" s="T1567">koʔbdo-m</ta>
            <ta e="T1569" id="Seg_2169" s="T1568">dĭ</ta>
            <ta e="T1570" id="Seg_2170" s="T1569">büzʼe</ta>
            <ta e="T1571" id="Seg_2171" s="T1570">körer-bi</ta>
            <ta e="T1572" id="Seg_2172" s="T1571">ine</ta>
            <ta e="T1573" id="Seg_2173" s="T1572">kun-bi</ta>
            <ta e="T1574" id="Seg_2174" s="T1573">dĭ</ta>
            <ta e="T1575" id="Seg_2175" s="T1574">koʔbdo-bə</ta>
            <ta e="T1576" id="Seg_2176" s="T1575">dĭn</ta>
            <ta e="T1577" id="Seg_2177" s="T1576">že</ta>
            <ta e="T1578" id="Seg_2178" s="T1577">ma-luʔbdə-bi</ta>
            <ta e="T1579" id="Seg_2179" s="T1578">i</ta>
            <ta e="T1580" id="Seg_2180" s="T1579">bos-də</ta>
            <ta e="T1581" id="Seg_2181" s="T1580">šo-bi</ta>
            <ta e="T1582" id="Seg_2182" s="T1581">dĭgəttə</ta>
            <ta e="T1583" id="Seg_2183" s="T1582">bazoʔ</ta>
            <ta e="T1584" id="Seg_2184" s="T1583">šišəge</ta>
            <ta e="T1585" id="Seg_2185" s="T1584">dĭ-Tə</ta>
            <ta e="T1586" id="Seg_2186" s="T1585">šonə-gA</ta>
            <ta e="T1587" id="Seg_2187" s="T1586">dĭ</ta>
            <ta e="T1588" id="Seg_2188" s="T1587">bar</ta>
            <ta e="T1589" id="Seg_2189" s="T1588">šišəge</ta>
            <ta e="T1590" id="Seg_2190" s="T1589">tănan</ta>
            <ta e="T1591" id="Seg_2191" s="T1590">šišəge</ta>
            <ta e="T1592" id="Seg_2192" s="T1591">kănzə-laːm-bi-m</ta>
            <ta e="T1593" id="Seg_2193" s="T1592">e-ʔ</ta>
            <ta e="T1595" id="Seg_2194" s="T1594">e-ʔ</ta>
            <ta e="T1596" id="Seg_2195" s="T1595">šo-ʔ</ta>
            <ta e="T1597" id="Seg_2196" s="T1596">măna</ta>
            <ta e="T1598" id="Seg_2197" s="T1597">dĭ</ta>
            <ta e="T1599" id="Seg_2198" s="T1598">bazoʔ</ta>
            <ta e="T1600" id="Seg_2199" s="T1599">šonə-gA</ta>
            <ta e="T1601" id="Seg_2200" s="T1600">šišəge</ta>
            <ta e="T1602" id="Seg_2201" s="T1601">tănan</ta>
            <ta e="T1603" id="Seg_2202" s="T1602">šišəge</ta>
            <ta e="T1604" id="Seg_2203" s="T1603">davaj</ta>
            <ta e="T1605" id="Seg_2204" s="T1604">kudo-l-zittə</ta>
            <ta e="T1606" id="Seg_2205" s="T1605">dĭ-m</ta>
            <ta e="T1607" id="Seg_2206" s="T1606">dĭgəttə</ta>
            <ta e="T1608" id="Seg_2207" s="T1607">ĭššo</ta>
            <ta e="T1609" id="Seg_2208" s="T1608">surar-bi</ta>
            <ta e="T1610" id="Seg_2209" s="T1609">dĭ</ta>
            <ta e="T1611" id="Seg_2210" s="T1610">bazoʔ</ta>
            <ta e="T1612" id="Seg_2211" s="T1611">kudo-l-bi</ta>
            <ta e="T1613" id="Seg_2212" s="T1612">vsʼaka</ta>
            <ta e="T1614" id="Seg_2213" s="T1613">mut_mat</ta>
            <ta e="T1615" id="Seg_2214" s="T1614">kudo-l-bi</ta>
            <ta e="T1616" id="Seg_2215" s="T1615">dĭ</ta>
            <ta e="T1617" id="Seg_2216" s="T1616">dĭ-m</ta>
            <ta e="T1618" id="Seg_2217" s="T1617">bar</ta>
            <ta e="T1619" id="Seg_2218" s="T1618">kăn-də-bi</ta>
            <ta e="T1620" id="Seg_2219" s="T1619">i</ta>
            <ta e="T1621" id="Seg_2220" s="T1620">kü-laːm-bi</ta>
            <ta e="T1622" id="Seg_2221" s="T1621">bar</ta>
            <ta e="T1623" id="Seg_2222" s="T1622">pa</ta>
            <ta e="T1624" id="Seg_2223" s="T1623">dĭrgit</ta>
            <ta e="T1625" id="Seg_2224" s="T1624">mo-laːm-bi</ta>
            <ta e="T1626" id="Seg_2225" s="T1625">dĭgəttə</ta>
            <ta e="T1627" id="Seg_2226" s="T1626">ertə-n</ta>
            <ta e="T1628" id="Seg_2227" s="T1627">büzʼe</ta>
            <ta e="T1629" id="Seg_2228" s="T1628">uʔbdə-bi</ta>
            <ta e="T1630" id="Seg_2229" s="T1629">kan-ə-ʔ</ta>
            <ta e="T1631" id="Seg_2230" s="T1630">măn</ta>
            <ta e="T1632" id="Seg_2231" s="T1631">koʔbdo</ta>
            <ta e="T1633" id="Seg_2232" s="T1632">i-lAʔ</ta>
            <ta e="T1634" id="Seg_2233" s="T1633">dĭ</ta>
            <ta e="T1635" id="Seg_2234" s="T1634">kan-bi</ta>
            <ta e="T1636" id="Seg_2235" s="T1635">men</ta>
            <ta e="T1637" id="Seg_2236" s="T1636">măn-ntə</ta>
            <ta e="T1638" id="Seg_2237" s="T1637">nüke-n</ta>
            <ta e="T1639" id="Seg_2238" s="T1638">koʔbdo-bə</ta>
            <ta e="T1640" id="Seg_2239" s="T1639">det-laʔbə</ta>
            <ta e="T1641" id="Seg_2240" s="T1640">băra-Tə</ta>
            <ta e="T1642" id="Seg_2241" s="T1641">bar</ta>
            <ta e="T1643" id="Seg_2242" s="T1642">kü-laːm-bi</ta>
            <ta e="T1644" id="Seg_2243" s="T1643">dĭgəttə</ta>
            <ta e="T1645" id="Seg_2244" s="T1644">dărəʔ</ta>
            <ta e="T1646" id="Seg_2245" s="T1645">e-ʔ</ta>
            <ta e="T1647" id="Seg_2246" s="T1646">măn-ə-ʔ</ta>
            <ta e="T1648" id="Seg_2247" s="T1647">a</ta>
            <ta e="T1649" id="Seg_2248" s="T1648">măn-ə-ʔ</ta>
            <ta e="T1650" id="Seg_2249" s="T1649">nüke-n</ta>
            <ta e="T1651" id="Seg_2250" s="T1650">koʔbdo-t</ta>
            <ta e="T1652" id="Seg_2251" s="T1651">dĭ</ta>
            <ta e="T1653" id="Seg_2252" s="T1652">üge</ta>
            <ta e="T1654" id="Seg_2253" s="T1653">dărəʔ</ta>
            <ta e="T1655" id="Seg_2254" s="T1654">dĭgəttə</ta>
            <ta e="T1656" id="Seg_2255" s="T1655">büzʼe</ta>
            <ta e="T1657" id="Seg_2256" s="T1656">šonə-gA</ta>
            <ta e="T1658" id="Seg_2257" s="T1657">nüke</ta>
            <ta e="T1659" id="Seg_2258" s="T1658">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1660" id="Seg_2259" s="T1659">ku-liA-t</ta>
            <ta e="T1661" id="Seg_2260" s="T1660">bar</ta>
            <ta e="T1662" id="Seg_2261" s="T1661">koʔbdo-t</ta>
            <ta e="T1663" id="Seg_2262" s="T1662">kü-laːm-bi</ta>
            <ta e="T1664" id="Seg_2263" s="T1663">iʔbö-laʔbə</ta>
            <ta e="T1665" id="Seg_2264" s="T1664">davaj</ta>
            <ta e="T1666" id="Seg_2265" s="T1665">tʼor-zittə</ta>
            <ta e="T1667" id="Seg_2266" s="T1666">ĭmbi=də</ta>
            <ta e="T1668" id="Seg_2267" s="T1667">ej</ta>
            <ta e="T1669" id="Seg_2268" s="T1668">a-lV-l</ta>
            <ta e="T1670" id="Seg_2269" s="T1669">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1404" id="Seg_2270" s="T1403">live-PST-3PL</ta>
            <ta e="T1405" id="Seg_2271" s="T1404">woman.[NOM.SG]</ta>
            <ta e="T1406" id="Seg_2272" s="T1405">man.[NOM.SG]</ta>
            <ta e="T1407" id="Seg_2273" s="T1406">woman-GEN</ta>
            <ta e="T1408" id="Seg_2274" s="T1407">be-PST.[3SG]</ta>
            <ta e="T1409" id="Seg_2275" s="T1408">daughter-NOM/GEN.3SG</ta>
            <ta e="T1410" id="Seg_2276" s="T1409">and</ta>
            <ta e="T1411" id="Seg_2277" s="T1410">man-GEN</ta>
            <ta e="T1412" id="Seg_2278" s="T1411">be-PST.[3SG]</ta>
            <ta e="T1413" id="Seg_2279" s="T1412">daughter-NOM/GEN.3SG</ta>
            <ta e="T1414" id="Seg_2280" s="T1413">this.[NOM.SG]</ta>
            <ta e="T1415" id="Seg_2281" s="T1414">woman.[NOM.SG]</ta>
            <ta e="T1416" id="Seg_2282" s="T1415">man-GEN</ta>
            <ta e="T1417" id="Seg_2283" s="T1416">daughter-ACC.3SG</ta>
            <ta e="T1418" id="Seg_2284" s="T1417">NEG</ta>
            <ta e="T1419" id="Seg_2285" s="T1418">pity-PST.[3SG]</ta>
            <ta e="T1420" id="Seg_2286" s="T1419">beat-PST.[3SG]</ta>
            <ta e="T1421" id="Seg_2287" s="T1420">scold-DES-DUR-PST.[3SG]</ta>
            <ta e="T1422" id="Seg_2288" s="T1421">and</ta>
            <ta e="T1423" id="Seg_2289" s="T1422">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1424" id="Seg_2290" s="T1423">daughter-ACC.3SG</ta>
            <ta e="T1425" id="Seg_2291" s="T1424">always</ta>
            <ta e="T1426" id="Seg_2292" s="T1425">pity-DUR.[3SG]</ta>
            <ta e="T1427" id="Seg_2293" s="T1426">head-NOM/GEN.3SG</ta>
            <ta e="T1428" id="Seg_2294" s="T1427">PTCL</ta>
            <ta e="T1429" id="Seg_2295" s="T1428">smear-DUR.[3SG]</ta>
            <ta e="T1430" id="Seg_2296" s="T1429">then</ta>
            <ta e="T1432" id="Seg_2297" s="T1431">man-LAT</ta>
            <ta e="T1433" id="Seg_2298" s="T1432">say-IPFVZ.[3SG]</ta>
            <ta e="T1434" id="Seg_2299" s="T1433">bring-IMP.2SG.O</ta>
            <ta e="T1435" id="Seg_2300" s="T1434">self-NOM/GEN/ACC.2SG</ta>
            <ta e="T1436" id="Seg_2301" s="T1435">girl.[NOM.SG]</ta>
            <ta e="T1437" id="Seg_2302" s="T1436">I.NOM</ta>
            <ta e="T1438" id="Seg_2303" s="T1437">this-ACC</ta>
            <ta e="T1439" id="Seg_2304" s="T1438">this.[NOM.SG]</ta>
            <ta e="T1440" id="Seg_2305" s="T1439">I.LAT</ta>
            <ta e="T1441" id="Seg_2306" s="T1440">what.[NOM.SG]=INDEF</ta>
            <ta e="T1442" id="Seg_2307" s="T1441">NEG</ta>
            <ta e="T1443" id="Seg_2308" s="T1442">make-PRS.[3SG]</ta>
            <ta e="T1444" id="Seg_2309" s="T1443">this.[NOM.SG]</ta>
            <ta e="T1445" id="Seg_2310" s="T1444">horse-ACC</ta>
            <ta e="T1446" id="Seg_2311" s="T1445">harness-PST.[3SG]</ta>
            <ta e="T1447" id="Seg_2312" s="T1446">daughter-ACC.3SG</ta>
            <ta e="T1448" id="Seg_2313" s="T1447">seat-PST.[3SG]</ta>
            <ta e="T1449" id="Seg_2314" s="T1448">bring-RES-PST.[3SG]</ta>
            <ta e="T1450" id="Seg_2315" s="T1449">forest-LAT</ta>
            <ta e="T1451" id="Seg_2316" s="T1450">leave-MOM-PST.[3SG]</ta>
            <ta e="T1452" id="Seg_2317" s="T1451">tree-GEN</ta>
            <ta e="T1453" id="Seg_2318" s="T1452">edge-LAT/LOC.3SG</ta>
            <ta e="T1456" id="Seg_2319" s="T1455">snow.[NOM.SG]</ta>
            <ta e="T1457" id="Seg_2320" s="T1456">big.[NOM.SG]</ta>
            <ta e="T1458" id="Seg_2321" s="T1457">this.[NOM.SG]</ta>
            <ta e="T1459" id="Seg_2322" s="T1458">sit-DUR.[3SG]</ta>
            <ta e="T1460" id="Seg_2323" s="T1459">freeze-DUR.[3SG]</ta>
            <ta e="T1461" id="Seg_2324" s="T1460">cold.[NOM.SG]</ta>
            <ta e="T1462" id="Seg_2325" s="T1461">come-PRS.[3SG]</ta>
            <ta e="T1465" id="Seg_2326" s="T1464">tree-ABL</ta>
            <ta e="T1466" id="Seg_2327" s="T1465">tree-LAT</ta>
            <ta e="T1467" id="Seg_2328" s="T1466">jump-DUR.[3SG]</ta>
            <ta e="T1468" id="Seg_2329" s="T1467">say-IPFVZ.[3SG]</ta>
            <ta e="T1469" id="Seg_2330" s="T1468">this-LAT</ta>
            <ta e="T1470" id="Seg_2331" s="T1469">you.DAT</ta>
            <ta e="T1471" id="Seg_2332" s="T1470">cold.[NOM.SG]</ta>
            <ta e="T1472" id="Seg_2333" s="T1471">no</ta>
            <ta e="T1473" id="Seg_2334" s="T1472">I.LAT</ta>
            <ta e="T1474" id="Seg_2335" s="T1473">NEG</ta>
            <ta e="T1475" id="Seg_2336" s="T1474">NEG</ta>
            <ta e="T1476" id="Seg_2337" s="T1475">cold.[NOM.SG]</ta>
            <ta e="T1477" id="Seg_2338" s="T1476">more</ta>
            <ta e="T1478" id="Seg_2339" s="T1477">come-PRS.[3SG]</ta>
            <ta e="T1479" id="Seg_2340" s="T1478">cold.[NOM.SG]</ta>
            <ta e="T1480" id="Seg_2341" s="T1479">you.DAT</ta>
            <ta e="T1481" id="Seg_2342" s="T1480">NEG</ta>
            <ta e="T1482" id="Seg_2343" s="T1481">cold.[NOM.SG]</ta>
            <ta e="T1483" id="Seg_2344" s="T1482">warm.[NOM.SG]</ta>
            <ta e="T1484" id="Seg_2345" s="T1483">and</ta>
            <ta e="T1485" id="Seg_2346" s="T1484">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1486" id="Seg_2347" s="T1485">speak-INF.LAT</ta>
            <ta e="T1487" id="Seg_2348" s="T1486">NEG</ta>
            <ta e="T1488" id="Seg_2349" s="T1487">can-PRS.[3SG]</ta>
            <ta e="T1489" id="Seg_2350" s="T1488">then</ta>
            <ta e="T1490" id="Seg_2351" s="T1489">again</ta>
            <ta e="T1491" id="Seg_2352" s="T1490">ask-DUR.[3SG]</ta>
            <ta e="T1492" id="Seg_2353" s="T1491">NEG</ta>
            <ta e="T1493" id="Seg_2354" s="T1492">cold.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_2355" s="T1493">self-GEN</ta>
            <ta e="T1495" id="Seg_2356" s="T1494">tongue-NOM/GEN.3SG</ta>
            <ta e="T1496" id="Seg_2357" s="T1495">this.[NOM.SG]</ta>
            <ta e="T1497" id="Seg_2358" s="T1496">PTCL</ta>
            <ta e="T1498" id="Seg_2359" s="T1497">freeze-RES-PST.[3SG]</ta>
            <ta e="T1499" id="Seg_2360" s="T1498">then</ta>
            <ta e="T1500" id="Seg_2361" s="T1499">this.[NOM.SG]</ta>
            <ta e="T1501" id="Seg_2362" s="T1500">this-ACC</ta>
            <ta e="T1502" id="Seg_2363" s="T1501">blanket-INS</ta>
            <ta e="T1503" id="Seg_2364" s="T1502">%%-PST.[3SG]</ta>
            <ta e="T1504" id="Seg_2365" s="T1503">coat.[NOM.SG]</ta>
            <ta e="T1505" id="Seg_2366" s="T1504">dress-PST.[3SG]</ta>
            <ta e="T1506" id="Seg_2367" s="T1505">this.[NOM.SG]</ta>
            <ta e="T1507" id="Seg_2368" s="T1506">warm-FACT-MOM-PST.[3SG]</ta>
            <ta e="T1508" id="Seg_2369" s="T1507">then</ta>
            <ta e="T1509" id="Seg_2370" s="T1508">woman.[NOM.SG]</ta>
            <ta e="T1510" id="Seg_2371" s="T1509">bake-DUR.[3SG]</ta>
            <ta e="T1511" id="Seg_2372" s="T1510">pancake-PL</ta>
            <ta e="T1512" id="Seg_2373" s="T1511">go-EP-IMP.2SG</ta>
            <ta e="T1513" id="Seg_2374" s="T1512">girl.[NOM.SG]</ta>
            <ta e="T1514" id="Seg_2375" s="T1513">see-RES-PST.[3SG]</ta>
            <ta e="T1515" id="Seg_2376" s="T1514">bring-IMP.2SG.O</ta>
            <ta e="T1516" id="Seg_2377" s="T1515">then</ta>
            <ta e="T1517" id="Seg_2378" s="T1516">dog.[NOM.SG]</ta>
            <ta e="T1518" id="Seg_2379" s="T1517">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T1519" id="Seg_2380" s="T1518">underpart-LAT/LOC.3SG</ta>
            <ta e="T1520" id="Seg_2381" s="T1519">sit-DUR.[3SG]</ta>
            <ta e="T1521" id="Seg_2382" s="T1520">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1522" id="Seg_2383" s="T1521">say-DUR.[3SG]</ta>
            <ta e="T1523" id="Seg_2384" s="T1522">man-GEN</ta>
            <ta e="T1524" id="Seg_2385" s="T1523">daughter-NOM/GEN.3SG</ta>
            <ta e="T1525" id="Seg_2386" s="T1524">come-PRS.[3SG]</ta>
            <ta e="T1527" id="Seg_2387" s="T1526">many</ta>
            <ta e="T1528" id="Seg_2388" s="T1527">PTCL</ta>
            <ta e="T1529" id="Seg_2389" s="T1528">money.[NOM.SG]</ta>
            <ta e="T1530" id="Seg_2390" s="T1529">bring-DUR.[3SG]</ta>
            <ta e="T1531" id="Seg_2391" s="T1530">clothing.[NOM.SG]</ta>
            <ta e="T1532" id="Seg_2392" s="T1531">many</ta>
            <ta e="T1533" id="Seg_2393" s="T1532">this.[NOM.SG]</ta>
            <ta e="T1534" id="Seg_2394" s="T1533">say-IPFVZ.[3SG]</ta>
            <ta e="T1535" id="Seg_2395" s="T1534">so</ta>
            <ta e="T1536" id="Seg_2396" s="T1535">NEG.AUX-IMP.2SG</ta>
            <ta e="T1537" id="Seg_2397" s="T1536">say-EP-CNG</ta>
            <ta e="T1538" id="Seg_2398" s="T1537">man-GEN</ta>
            <ta e="T1539" id="Seg_2399" s="T1538">daughter-ACC.3SG</ta>
            <ta e="T1540" id="Seg_2400" s="T1539">sack-LOC</ta>
            <ta e="T1541" id="Seg_2401" s="T1540">bring-DUR.[3SG]</ta>
            <ta e="T1542" id="Seg_2402" s="T1541">and</ta>
            <ta e="T1543" id="Seg_2403" s="T1542">dog.[NOM.SG]</ta>
            <ta e="T1544" id="Seg_2404" s="T1543">always</ta>
            <ta e="T1546" id="Seg_2405" s="T1545">dog.[NOM.SG]</ta>
            <ta e="T1547" id="Seg_2406" s="T1546">always</ta>
            <ta e="T1548" id="Seg_2407" s="T1547">this-LAT</ta>
            <ta e="T1549" id="Seg_2408" s="T1548">so</ta>
            <ta e="T1550" id="Seg_2409" s="T1549">say-DUR.[3SG]</ta>
            <ta e="T1551" id="Seg_2410" s="T1550">then</ta>
            <ta e="T1552" id="Seg_2411" s="T1551">man.[NOM.SG]</ta>
            <ta e="T1553" id="Seg_2412" s="T1552">come-PST.[3SG]</ta>
            <ta e="T1554" id="Seg_2413" s="T1553">basket.[NOM.SG]</ta>
            <ta e="T1555" id="Seg_2414" s="T1554">money.[NOM.SG]</ta>
            <ta e="T1556" id="Seg_2415" s="T1555">bring-PST.[3SG]</ta>
            <ta e="T1557" id="Seg_2416" s="T1556">PTCL</ta>
            <ta e="T1558" id="Seg_2417" s="T1557">clothing.[NOM.SG]</ta>
            <ta e="T1559" id="Seg_2418" s="T1558">many</ta>
            <ta e="T1560" id="Seg_2419" s="T1559">daughter-NOM/GEN.3SG</ta>
            <ta e="T1561" id="Seg_2420" s="T1560">house-LAT</ta>
            <ta e="T1562" id="Seg_2421" s="T1561">come-PST.[3SG]</ta>
            <ta e="T1563" id="Seg_2422" s="T1562">then</ta>
            <ta e="T1564" id="Seg_2423" s="T1563">this.[NOM.SG]</ta>
            <ta e="T1565" id="Seg_2424" s="T1564">woman.[NOM.SG]</ta>
            <ta e="T1566" id="Seg_2425" s="T1565">bring-IMP.2SG.O</ta>
            <ta e="T1567" id="Seg_2426" s="T1566">I.GEN</ta>
            <ta e="T1568" id="Seg_2427" s="T1567">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T1569" id="Seg_2428" s="T1568">this.[NOM.SG]</ta>
            <ta e="T1570" id="Seg_2429" s="T1569">man.[NOM.SG]</ta>
            <ta e="T1571" id="Seg_2430" s="T1570">harness-PST.[3SG]</ta>
            <ta e="T1572" id="Seg_2431" s="T1571">horse.[NOM.SG]</ta>
            <ta e="T1573" id="Seg_2432" s="T1572">bring-PST.[3SG]</ta>
            <ta e="T1574" id="Seg_2433" s="T1573">this.[NOM.SG]</ta>
            <ta e="T1575" id="Seg_2434" s="T1574">daughter-ACC.3SG</ta>
            <ta e="T1576" id="Seg_2435" s="T1575">there</ta>
            <ta e="T1577" id="Seg_2436" s="T1576">PTCL</ta>
            <ta e="T1578" id="Seg_2437" s="T1577">remain-MOM-PST.[3SG]</ta>
            <ta e="T1579" id="Seg_2438" s="T1578">and</ta>
            <ta e="T1580" id="Seg_2439" s="T1579">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1581" id="Seg_2440" s="T1580">come-PST.[3SG]</ta>
            <ta e="T1582" id="Seg_2441" s="T1581">then</ta>
            <ta e="T1583" id="Seg_2442" s="T1582">again</ta>
            <ta e="T1584" id="Seg_2443" s="T1583">cold.[NOM.SG]</ta>
            <ta e="T1585" id="Seg_2444" s="T1584">this-LAT</ta>
            <ta e="T1586" id="Seg_2445" s="T1585">come-PRS.[3SG]</ta>
            <ta e="T1587" id="Seg_2446" s="T1586">this.[NOM.SG]</ta>
            <ta e="T1588" id="Seg_2447" s="T1587">PTCL</ta>
            <ta e="T1589" id="Seg_2448" s="T1588">cold.[NOM.SG]</ta>
            <ta e="T1590" id="Seg_2449" s="T1589">you.DAT</ta>
            <ta e="T1591" id="Seg_2450" s="T1590">cold.[NOM.SG]</ta>
            <ta e="T1592" id="Seg_2451" s="T1591">freeze-RES-PST-1SG</ta>
            <ta e="T1593" id="Seg_2452" s="T1592">NEG.AUX-IMP.2SG</ta>
            <ta e="T1595" id="Seg_2453" s="T1594">NEG.AUX-IMP.2SG</ta>
            <ta e="T1596" id="Seg_2454" s="T1595">come-CNG</ta>
            <ta e="T1597" id="Seg_2455" s="T1596">I.LAT</ta>
            <ta e="T1598" id="Seg_2456" s="T1597">this.[NOM.SG]</ta>
            <ta e="T1599" id="Seg_2457" s="T1598">again</ta>
            <ta e="T1600" id="Seg_2458" s="T1599">come-PRS.[3SG]</ta>
            <ta e="T1601" id="Seg_2459" s="T1600">cold.[NOM.SG]</ta>
            <ta e="T1602" id="Seg_2460" s="T1601">you.DAT</ta>
            <ta e="T1603" id="Seg_2461" s="T1602">cold.[NOM.SG]</ta>
            <ta e="T1604" id="Seg_2462" s="T1603">INCH</ta>
            <ta e="T1605" id="Seg_2463" s="T1604">scold-FRQ-INF.LAT</ta>
            <ta e="T1606" id="Seg_2464" s="T1605">this-ACC</ta>
            <ta e="T1607" id="Seg_2465" s="T1606">then</ta>
            <ta e="T1608" id="Seg_2466" s="T1607">more</ta>
            <ta e="T1609" id="Seg_2467" s="T1608">ask-PST.[3SG]</ta>
            <ta e="T1610" id="Seg_2468" s="T1609">this.[NOM.SG]</ta>
            <ta e="T1611" id="Seg_2469" s="T1610">again</ta>
            <ta e="T1612" id="Seg_2470" s="T1611">scold-FRQ-PST.[3SG]</ta>
            <ta e="T1613" id="Seg_2471" s="T1612">sundry</ta>
            <ta e="T1614" id="Seg_2472" s="T1613">various</ta>
            <ta e="T1615" id="Seg_2473" s="T1614">scold-FRQ-PST.[3SG]</ta>
            <ta e="T1616" id="Seg_2474" s="T1615">this.[NOM.SG]</ta>
            <ta e="T1617" id="Seg_2475" s="T1616">this-ACC</ta>
            <ta e="T1618" id="Seg_2476" s="T1617">PTCL</ta>
            <ta e="T1619" id="Seg_2477" s="T1618">freeze-TR-PST.[3SG]</ta>
            <ta e="T1620" id="Seg_2478" s="T1619">and</ta>
            <ta e="T1621" id="Seg_2479" s="T1620">see-RES-PST.[3SG]</ta>
            <ta e="T1622" id="Seg_2480" s="T1621">PTCL</ta>
            <ta e="T1623" id="Seg_2481" s="T1622">tree.[NOM.SG]</ta>
            <ta e="T1624" id="Seg_2482" s="T1623">such.[NOM.SG]</ta>
            <ta e="T1625" id="Seg_2483" s="T1624">become-RES-PST.[3SG]</ta>
            <ta e="T1626" id="Seg_2484" s="T1625">then</ta>
            <ta e="T1627" id="Seg_2485" s="T1626">morning-GEN</ta>
            <ta e="T1628" id="Seg_2486" s="T1627">man.[NOM.SG]</ta>
            <ta e="T1629" id="Seg_2487" s="T1628">get.up-PST.[3SG]</ta>
            <ta e="T1630" id="Seg_2488" s="T1629">go-EP-IMP.2SG</ta>
            <ta e="T1631" id="Seg_2489" s="T1630">I.GEN</ta>
            <ta e="T1632" id="Seg_2490" s="T1631">girl.[NOM.SG]</ta>
            <ta e="T1633" id="Seg_2491" s="T1632">take-CVB</ta>
            <ta e="T1634" id="Seg_2492" s="T1633">this.[NOM.SG]</ta>
            <ta e="T1635" id="Seg_2493" s="T1634">go-PST.[3SG]</ta>
            <ta e="T1636" id="Seg_2494" s="T1635">dog.[NOM.SG]</ta>
            <ta e="T1637" id="Seg_2495" s="T1636">say-IPFVZ.[3SG]</ta>
            <ta e="T1638" id="Seg_2496" s="T1637">woman-GEN</ta>
            <ta e="T1639" id="Seg_2497" s="T1638">daughter-ACC.3SG</ta>
            <ta e="T1640" id="Seg_2498" s="T1639">bring-DUR.[3SG]</ta>
            <ta e="T1641" id="Seg_2499" s="T1640">sack-LAT</ta>
            <ta e="T1642" id="Seg_2500" s="T1641">PTCL</ta>
            <ta e="T1643" id="Seg_2501" s="T1642">see-RES-PST.[3SG]</ta>
            <ta e="T1644" id="Seg_2502" s="T1643">then</ta>
            <ta e="T1645" id="Seg_2503" s="T1644">so</ta>
            <ta e="T1646" id="Seg_2504" s="T1645">NEG.AUX-IMP.2SG</ta>
            <ta e="T1647" id="Seg_2505" s="T1646">say-EP-CNG</ta>
            <ta e="T1648" id="Seg_2506" s="T1647">and</ta>
            <ta e="T1649" id="Seg_2507" s="T1648">say-EP-IMP.2SG</ta>
            <ta e="T1650" id="Seg_2508" s="T1649">woman-GEN</ta>
            <ta e="T1651" id="Seg_2509" s="T1650">daughter-NOM/GEN.3SG</ta>
            <ta e="T1652" id="Seg_2510" s="T1651">this.[NOM.SG]</ta>
            <ta e="T1653" id="Seg_2511" s="T1652">always</ta>
            <ta e="T1654" id="Seg_2512" s="T1653">so</ta>
            <ta e="T1655" id="Seg_2513" s="T1654">then</ta>
            <ta e="T1656" id="Seg_2514" s="T1655">man.[NOM.SG]</ta>
            <ta e="T1657" id="Seg_2515" s="T1656">come-PRS.[3SG]</ta>
            <ta e="T1658" id="Seg_2516" s="T1657">woman.[NOM.SG]</ta>
            <ta e="T1659" id="Seg_2517" s="T1658">run-MOM-PST.[3SG]</ta>
            <ta e="T1660" id="Seg_2518" s="T1659">see-PRS-3SG.O</ta>
            <ta e="T1661" id="Seg_2519" s="T1660">PTCL</ta>
            <ta e="T1662" id="Seg_2520" s="T1661">daughter-NOM/GEN.3SG</ta>
            <ta e="T1663" id="Seg_2521" s="T1662">die-RES-PST.[3SG]</ta>
            <ta e="T1664" id="Seg_2522" s="T1663">lie-DUR.[3SG]</ta>
            <ta e="T1665" id="Seg_2523" s="T1664">INCH</ta>
            <ta e="T1666" id="Seg_2524" s="T1665">cry-INF.LAT</ta>
            <ta e="T1667" id="Seg_2525" s="T1666">what.[NOM.SG]=INDEF</ta>
            <ta e="T1668" id="Seg_2526" s="T1667">NEG</ta>
            <ta e="T1669" id="Seg_2527" s="T1668">make-FUT-2SG</ta>
            <ta e="T1670" id="Seg_2528" s="T1669">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1404" id="Seg_2529" s="T1403">жить-PST-3PL</ta>
            <ta e="T1405" id="Seg_2530" s="T1404">женщина.[NOM.SG]</ta>
            <ta e="T1406" id="Seg_2531" s="T1405">мужчина.[NOM.SG]</ta>
            <ta e="T1407" id="Seg_2532" s="T1406">женщина-GEN</ta>
            <ta e="T1408" id="Seg_2533" s="T1407">быть-PST.[3SG]</ta>
            <ta e="T1409" id="Seg_2534" s="T1408">дочь-NOM/GEN.3SG</ta>
            <ta e="T1410" id="Seg_2535" s="T1409">и</ta>
            <ta e="T1411" id="Seg_2536" s="T1410">мужчина-GEN</ta>
            <ta e="T1412" id="Seg_2537" s="T1411">быть-PST.[3SG]</ta>
            <ta e="T1413" id="Seg_2538" s="T1412">дочь-NOM/GEN.3SG</ta>
            <ta e="T1414" id="Seg_2539" s="T1413">этот.[NOM.SG]</ta>
            <ta e="T1415" id="Seg_2540" s="T1414">женщина.[NOM.SG]</ta>
            <ta e="T1416" id="Seg_2541" s="T1415">мужчина-GEN</ta>
            <ta e="T1417" id="Seg_2542" s="T1416">дочь-ACC.3SG</ta>
            <ta e="T1418" id="Seg_2543" s="T1417">NEG</ta>
            <ta e="T1419" id="Seg_2544" s="T1418">жалеть-PST.[3SG]</ta>
            <ta e="T1420" id="Seg_2545" s="T1419">бить-PST.[3SG]</ta>
            <ta e="T1421" id="Seg_2546" s="T1420">ругать-DES-DUR-PST.[3SG]</ta>
            <ta e="T1422" id="Seg_2547" s="T1421">а</ta>
            <ta e="T1423" id="Seg_2548" s="T1422">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1424" id="Seg_2549" s="T1423">дочь-ACC.3SG</ta>
            <ta e="T1425" id="Seg_2550" s="T1424">всегда</ta>
            <ta e="T1426" id="Seg_2551" s="T1425">жалеть-DUR.[3SG]</ta>
            <ta e="T1427" id="Seg_2552" s="T1426">голова-NOM/GEN.3SG</ta>
            <ta e="T1428" id="Seg_2553" s="T1427">PTCL</ta>
            <ta e="T1429" id="Seg_2554" s="T1428">мазать-DUR.[3SG]</ta>
            <ta e="T1430" id="Seg_2555" s="T1429">тогда</ta>
            <ta e="T1432" id="Seg_2556" s="T1431">мужчина-LAT</ta>
            <ta e="T1433" id="Seg_2557" s="T1432">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1434" id="Seg_2558" s="T1433">нести-IMP.2SG.O</ta>
            <ta e="T1435" id="Seg_2559" s="T1434">сам-NOM/GEN/ACC.2SG</ta>
            <ta e="T1436" id="Seg_2560" s="T1435">девушка.[NOM.SG]</ta>
            <ta e="T1437" id="Seg_2561" s="T1436">я.NOM</ta>
            <ta e="T1438" id="Seg_2562" s="T1437">этот-ACC</ta>
            <ta e="T1439" id="Seg_2563" s="T1438">этот.[NOM.SG]</ta>
            <ta e="T1440" id="Seg_2564" s="T1439">я.LAT</ta>
            <ta e="T1441" id="Seg_2565" s="T1440">что.[NOM.SG]=INDEF</ta>
            <ta e="T1442" id="Seg_2566" s="T1441">NEG</ta>
            <ta e="T1443" id="Seg_2567" s="T1442">делать-PRS.[3SG]</ta>
            <ta e="T1444" id="Seg_2568" s="T1443">этот.[NOM.SG]</ta>
            <ta e="T1445" id="Seg_2569" s="T1444">лошадь-ACC</ta>
            <ta e="T1446" id="Seg_2570" s="T1445">запрячь-PST.[3SG]</ta>
            <ta e="T1447" id="Seg_2571" s="T1446">дочь-ACC.3SG</ta>
            <ta e="T1448" id="Seg_2572" s="T1447">сажать-PST.[3SG]</ta>
            <ta e="T1449" id="Seg_2573" s="T1448">нести-RES-PST.[3SG]</ta>
            <ta e="T1450" id="Seg_2574" s="T1449">лес-LAT</ta>
            <ta e="T1451" id="Seg_2575" s="T1450">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1452" id="Seg_2576" s="T1451">дерево-GEN</ta>
            <ta e="T1453" id="Seg_2577" s="T1452">край-LAT/LOC.3SG</ta>
            <ta e="T1456" id="Seg_2578" s="T1455">снег.[NOM.SG]</ta>
            <ta e="T1457" id="Seg_2579" s="T1456">большой.[NOM.SG]</ta>
            <ta e="T1458" id="Seg_2580" s="T1457">этот.[NOM.SG]</ta>
            <ta e="T1459" id="Seg_2581" s="T1458">сидеть-DUR.[3SG]</ta>
            <ta e="T1460" id="Seg_2582" s="T1459">мерзнуть-DUR.[3SG]</ta>
            <ta e="T1461" id="Seg_2583" s="T1460">холодный.[NOM.SG]</ta>
            <ta e="T1462" id="Seg_2584" s="T1461">прийти-PRS.[3SG]</ta>
            <ta e="T1465" id="Seg_2585" s="T1464">дерево-ABL</ta>
            <ta e="T1466" id="Seg_2586" s="T1465">дерево-LAT</ta>
            <ta e="T1467" id="Seg_2587" s="T1466">прыгнуть-DUR.[3SG]</ta>
            <ta e="T1468" id="Seg_2588" s="T1467">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1469" id="Seg_2589" s="T1468">этот-LAT</ta>
            <ta e="T1470" id="Seg_2590" s="T1469">ты.DAT</ta>
            <ta e="T1471" id="Seg_2591" s="T1470">холодный.[NOM.SG]</ta>
            <ta e="T1472" id="Seg_2592" s="T1471">нет</ta>
            <ta e="T1473" id="Seg_2593" s="T1472">я.LAT</ta>
            <ta e="T1474" id="Seg_2594" s="T1473">NEG</ta>
            <ta e="T1475" id="Seg_2595" s="T1474">NEG</ta>
            <ta e="T1476" id="Seg_2596" s="T1475">холодный.[NOM.SG]</ta>
            <ta e="T1477" id="Seg_2597" s="T1476">еще</ta>
            <ta e="T1478" id="Seg_2598" s="T1477">прийти-PRS.[3SG]</ta>
            <ta e="T1479" id="Seg_2599" s="T1478">холодный.[NOM.SG]</ta>
            <ta e="T1480" id="Seg_2600" s="T1479">ты.DAT</ta>
            <ta e="T1481" id="Seg_2601" s="T1480">NEG</ta>
            <ta e="T1482" id="Seg_2602" s="T1481">холодный.[NOM.SG]</ta>
            <ta e="T1483" id="Seg_2603" s="T1482">теплый.[NOM.SG]</ta>
            <ta e="T1484" id="Seg_2604" s="T1483">а</ta>
            <ta e="T1485" id="Seg_2605" s="T1484">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1486" id="Seg_2606" s="T1485">говорить-INF.LAT</ta>
            <ta e="T1487" id="Seg_2607" s="T1486">NEG</ta>
            <ta e="T1488" id="Seg_2608" s="T1487">мочь-PRS.[3SG]</ta>
            <ta e="T1489" id="Seg_2609" s="T1488">тогда</ta>
            <ta e="T1490" id="Seg_2610" s="T1489">опять</ta>
            <ta e="T1491" id="Seg_2611" s="T1490">спросить-DUR.[3SG]</ta>
            <ta e="T1492" id="Seg_2612" s="T1491">NEG</ta>
            <ta e="T1493" id="Seg_2613" s="T1492">холодный.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_2614" s="T1493">сам-GEN</ta>
            <ta e="T1495" id="Seg_2615" s="T1494">язык-NOM/GEN.3SG</ta>
            <ta e="T1496" id="Seg_2616" s="T1495">этот.[NOM.SG]</ta>
            <ta e="T1497" id="Seg_2617" s="T1496">PTCL</ta>
            <ta e="T1498" id="Seg_2618" s="T1497">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T1499" id="Seg_2619" s="T1498">тогда</ta>
            <ta e="T1500" id="Seg_2620" s="T1499">этот.[NOM.SG]</ta>
            <ta e="T1501" id="Seg_2621" s="T1500">этот-ACC</ta>
            <ta e="T1502" id="Seg_2622" s="T1501">одеяло-INS</ta>
            <ta e="T1503" id="Seg_2623" s="T1502">%%-PST.[3SG]</ta>
            <ta e="T1504" id="Seg_2624" s="T1503">пальто.[NOM.SG]</ta>
            <ta e="T1505" id="Seg_2625" s="T1504">надеть-PST.[3SG]</ta>
            <ta e="T1506" id="Seg_2626" s="T1505">этот.[NOM.SG]</ta>
            <ta e="T1507" id="Seg_2627" s="T1506">теплый-FACT-MOM-PST.[3SG]</ta>
            <ta e="T1508" id="Seg_2628" s="T1507">тогда</ta>
            <ta e="T1509" id="Seg_2629" s="T1508">женщина.[NOM.SG]</ta>
            <ta e="T1510" id="Seg_2630" s="T1509">печь-DUR.[3SG]</ta>
            <ta e="T1511" id="Seg_2631" s="T1510">блин-PL</ta>
            <ta e="T1512" id="Seg_2632" s="T1511">пойти-EP-IMP.2SG</ta>
            <ta e="T1513" id="Seg_2633" s="T1512">девушка.[NOM.SG]</ta>
            <ta e="T1514" id="Seg_2634" s="T1513">видеть-RES-PST.[3SG]</ta>
            <ta e="T1515" id="Seg_2635" s="T1514">принести-IMP.2SG.O</ta>
            <ta e="T1516" id="Seg_2636" s="T1515">тогда</ta>
            <ta e="T1517" id="Seg_2637" s="T1516">собака.[NOM.SG]</ta>
            <ta e="T1518" id="Seg_2638" s="T1517">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T1519" id="Seg_2639" s="T1518">низ-LAT/LOC.3SG</ta>
            <ta e="T1520" id="Seg_2640" s="T1519">сидеть-DUR.[3SG]</ta>
            <ta e="T1521" id="Seg_2641" s="T1520">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1522" id="Seg_2642" s="T1521">сказать-DUR.[3SG]</ta>
            <ta e="T1523" id="Seg_2643" s="T1522">мужчина-GEN</ta>
            <ta e="T1524" id="Seg_2644" s="T1523">дочь-NOM/GEN.3SG</ta>
            <ta e="T1525" id="Seg_2645" s="T1524">прийти-PRS.[3SG]</ta>
            <ta e="T1527" id="Seg_2646" s="T1526">много</ta>
            <ta e="T1528" id="Seg_2647" s="T1527">PTCL</ta>
            <ta e="T1529" id="Seg_2648" s="T1528">деньги.[NOM.SG]</ta>
            <ta e="T1530" id="Seg_2649" s="T1529">принести-DUR.[3SG]</ta>
            <ta e="T1531" id="Seg_2650" s="T1530">одежда.[NOM.SG]</ta>
            <ta e="T1532" id="Seg_2651" s="T1531">много</ta>
            <ta e="T1533" id="Seg_2652" s="T1532">этот.[NOM.SG]</ta>
            <ta e="T1534" id="Seg_2653" s="T1533">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1535" id="Seg_2654" s="T1534">так</ta>
            <ta e="T1536" id="Seg_2655" s="T1535">NEG.AUX-IMP.2SG</ta>
            <ta e="T1537" id="Seg_2656" s="T1536">сказать-EP-CNG</ta>
            <ta e="T1538" id="Seg_2657" s="T1537">мужчина-GEN</ta>
            <ta e="T1539" id="Seg_2658" s="T1538">дочь-ACC.3SG</ta>
            <ta e="T1540" id="Seg_2659" s="T1539">мешок-LOC</ta>
            <ta e="T1541" id="Seg_2660" s="T1540">принести-DUR.[3SG]</ta>
            <ta e="T1542" id="Seg_2661" s="T1541">а</ta>
            <ta e="T1543" id="Seg_2662" s="T1542">собака.[NOM.SG]</ta>
            <ta e="T1544" id="Seg_2663" s="T1543">всегда</ta>
            <ta e="T1546" id="Seg_2664" s="T1545">собака.[NOM.SG]</ta>
            <ta e="T1547" id="Seg_2665" s="T1546">всегда</ta>
            <ta e="T1548" id="Seg_2666" s="T1547">этот-LAT</ta>
            <ta e="T1549" id="Seg_2667" s="T1548">так</ta>
            <ta e="T1550" id="Seg_2668" s="T1549">сказать-DUR.[3SG]</ta>
            <ta e="T1551" id="Seg_2669" s="T1550">тогда</ta>
            <ta e="T1552" id="Seg_2670" s="T1551">мужчина.[NOM.SG]</ta>
            <ta e="T1553" id="Seg_2671" s="T1552">прийти-PST.[3SG]</ta>
            <ta e="T1554" id="Seg_2672" s="T1553">корзина.[NOM.SG]</ta>
            <ta e="T1555" id="Seg_2673" s="T1554">деньги.[NOM.SG]</ta>
            <ta e="T1556" id="Seg_2674" s="T1555">принести-PST.[3SG]</ta>
            <ta e="T1557" id="Seg_2675" s="T1556">PTCL</ta>
            <ta e="T1558" id="Seg_2676" s="T1557">одежда.[NOM.SG]</ta>
            <ta e="T1559" id="Seg_2677" s="T1558">много</ta>
            <ta e="T1560" id="Seg_2678" s="T1559">дочь-NOM/GEN.3SG</ta>
            <ta e="T1561" id="Seg_2679" s="T1560">дом-LAT</ta>
            <ta e="T1562" id="Seg_2680" s="T1561">прийти-PST.[3SG]</ta>
            <ta e="T1563" id="Seg_2681" s="T1562">тогда</ta>
            <ta e="T1564" id="Seg_2682" s="T1563">этот.[NOM.SG]</ta>
            <ta e="T1565" id="Seg_2683" s="T1564">женщина.[NOM.SG]</ta>
            <ta e="T1566" id="Seg_2684" s="T1565">нести-IMP.2SG.O</ta>
            <ta e="T1567" id="Seg_2685" s="T1566">я.GEN</ta>
            <ta e="T1568" id="Seg_2686" s="T1567">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T1569" id="Seg_2687" s="T1568">этот.[NOM.SG]</ta>
            <ta e="T1570" id="Seg_2688" s="T1569">мужчина.[NOM.SG]</ta>
            <ta e="T1571" id="Seg_2689" s="T1570">запрячь-PST.[3SG]</ta>
            <ta e="T1572" id="Seg_2690" s="T1571">лошадь.[NOM.SG]</ta>
            <ta e="T1573" id="Seg_2691" s="T1572">нести-PST.[3SG]</ta>
            <ta e="T1574" id="Seg_2692" s="T1573">этот.[NOM.SG]</ta>
            <ta e="T1575" id="Seg_2693" s="T1574">дочь-ACC.3SG</ta>
            <ta e="T1576" id="Seg_2694" s="T1575">там</ta>
            <ta e="T1577" id="Seg_2695" s="T1576">же</ta>
            <ta e="T1578" id="Seg_2696" s="T1577">остаться-MOM-PST.[3SG]</ta>
            <ta e="T1579" id="Seg_2697" s="T1578">и</ta>
            <ta e="T1580" id="Seg_2698" s="T1579">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1581" id="Seg_2699" s="T1580">прийти-PST.[3SG]</ta>
            <ta e="T1582" id="Seg_2700" s="T1581">тогда</ta>
            <ta e="T1583" id="Seg_2701" s="T1582">опять</ta>
            <ta e="T1584" id="Seg_2702" s="T1583">холодный.[NOM.SG]</ta>
            <ta e="T1585" id="Seg_2703" s="T1584">этот-LAT</ta>
            <ta e="T1586" id="Seg_2704" s="T1585">прийти-PRS.[3SG]</ta>
            <ta e="T1587" id="Seg_2705" s="T1586">этот.[NOM.SG]</ta>
            <ta e="T1588" id="Seg_2706" s="T1587">PTCL</ta>
            <ta e="T1589" id="Seg_2707" s="T1588">холодный.[NOM.SG]</ta>
            <ta e="T1590" id="Seg_2708" s="T1589">ты.DAT</ta>
            <ta e="T1591" id="Seg_2709" s="T1590">холодный.[NOM.SG]</ta>
            <ta e="T1592" id="Seg_2710" s="T1591">замерзнуть-RES-PST-1SG</ta>
            <ta e="T1593" id="Seg_2711" s="T1592">NEG.AUX-IMP.2SG</ta>
            <ta e="T1595" id="Seg_2712" s="T1594">NEG.AUX-IMP.2SG</ta>
            <ta e="T1596" id="Seg_2713" s="T1595">прийти-CNG</ta>
            <ta e="T1597" id="Seg_2714" s="T1596">я.LAT</ta>
            <ta e="T1598" id="Seg_2715" s="T1597">этот.[NOM.SG]</ta>
            <ta e="T1599" id="Seg_2716" s="T1598">опять</ta>
            <ta e="T1600" id="Seg_2717" s="T1599">прийти-PRS.[3SG]</ta>
            <ta e="T1601" id="Seg_2718" s="T1600">холодный.[NOM.SG]</ta>
            <ta e="T1602" id="Seg_2719" s="T1601">ты.DAT</ta>
            <ta e="T1603" id="Seg_2720" s="T1602">холодный.[NOM.SG]</ta>
            <ta e="T1604" id="Seg_2721" s="T1603">INCH</ta>
            <ta e="T1605" id="Seg_2722" s="T1604">ругать-FRQ-INF.LAT</ta>
            <ta e="T1606" id="Seg_2723" s="T1605">этот-ACC</ta>
            <ta e="T1607" id="Seg_2724" s="T1606">тогда</ta>
            <ta e="T1608" id="Seg_2725" s="T1607">еще</ta>
            <ta e="T1609" id="Seg_2726" s="T1608">спросить-PST.[3SG]</ta>
            <ta e="T1610" id="Seg_2727" s="T1609">этот.[NOM.SG]</ta>
            <ta e="T1611" id="Seg_2728" s="T1610">опять</ta>
            <ta e="T1612" id="Seg_2729" s="T1611">ругать-FRQ-PST.[3SG]</ta>
            <ta e="T1613" id="Seg_2730" s="T1612">всякий</ta>
            <ta e="T1614" id="Seg_2731" s="T1613">разный</ta>
            <ta e="T1615" id="Seg_2732" s="T1614">ругать-FRQ-PST.[3SG]</ta>
            <ta e="T1616" id="Seg_2733" s="T1615">этот.[NOM.SG]</ta>
            <ta e="T1617" id="Seg_2734" s="T1616">этот-ACC</ta>
            <ta e="T1618" id="Seg_2735" s="T1617">PTCL</ta>
            <ta e="T1619" id="Seg_2736" s="T1618">мерзнуть-TR-PST.[3SG]</ta>
            <ta e="T1620" id="Seg_2737" s="T1619">и</ta>
            <ta e="T1621" id="Seg_2738" s="T1620">видеть-RES-PST.[3SG]</ta>
            <ta e="T1622" id="Seg_2739" s="T1621">PTCL</ta>
            <ta e="T1623" id="Seg_2740" s="T1622">дерево.[NOM.SG]</ta>
            <ta e="T1624" id="Seg_2741" s="T1623">такой.[NOM.SG]</ta>
            <ta e="T1625" id="Seg_2742" s="T1624">стать-RES-PST.[3SG]</ta>
            <ta e="T1626" id="Seg_2743" s="T1625">тогда</ta>
            <ta e="T1627" id="Seg_2744" s="T1626">утро-GEN</ta>
            <ta e="T1628" id="Seg_2745" s="T1627">мужчина.[NOM.SG]</ta>
            <ta e="T1629" id="Seg_2746" s="T1628">встать-PST.[3SG]</ta>
            <ta e="T1630" id="Seg_2747" s="T1629">пойти-EP-IMP.2SG</ta>
            <ta e="T1631" id="Seg_2748" s="T1630">я.GEN</ta>
            <ta e="T1632" id="Seg_2749" s="T1631">девушка.[NOM.SG]</ta>
            <ta e="T1633" id="Seg_2750" s="T1632">взять-CVB</ta>
            <ta e="T1634" id="Seg_2751" s="T1633">этот.[NOM.SG]</ta>
            <ta e="T1635" id="Seg_2752" s="T1634">пойти-PST.[3SG]</ta>
            <ta e="T1636" id="Seg_2753" s="T1635">собака.[NOM.SG]</ta>
            <ta e="T1637" id="Seg_2754" s="T1636">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1638" id="Seg_2755" s="T1637">женщина-GEN</ta>
            <ta e="T1639" id="Seg_2756" s="T1638">дочь-ACC.3SG</ta>
            <ta e="T1640" id="Seg_2757" s="T1639">принести-DUR.[3SG]</ta>
            <ta e="T1641" id="Seg_2758" s="T1640">мешок-LAT</ta>
            <ta e="T1642" id="Seg_2759" s="T1641">PTCL</ta>
            <ta e="T1643" id="Seg_2760" s="T1642">видеть-RES-PST.[3SG]</ta>
            <ta e="T1644" id="Seg_2761" s="T1643">тогда</ta>
            <ta e="T1645" id="Seg_2762" s="T1644">так</ta>
            <ta e="T1646" id="Seg_2763" s="T1645">NEG.AUX-IMP.2SG</ta>
            <ta e="T1647" id="Seg_2764" s="T1646">сказать-EP-CNG</ta>
            <ta e="T1648" id="Seg_2765" s="T1647">а</ta>
            <ta e="T1649" id="Seg_2766" s="T1648">сказать-EP-IMP.2SG</ta>
            <ta e="T1650" id="Seg_2767" s="T1649">женщина-GEN</ta>
            <ta e="T1651" id="Seg_2768" s="T1650">дочь-NOM/GEN.3SG</ta>
            <ta e="T1652" id="Seg_2769" s="T1651">этот.[NOM.SG]</ta>
            <ta e="T1653" id="Seg_2770" s="T1652">всегда</ta>
            <ta e="T1654" id="Seg_2771" s="T1653">так</ta>
            <ta e="T1655" id="Seg_2772" s="T1654">тогда</ta>
            <ta e="T1656" id="Seg_2773" s="T1655">мужчина.[NOM.SG]</ta>
            <ta e="T1657" id="Seg_2774" s="T1656">прийти-PRS.[3SG]</ta>
            <ta e="T1658" id="Seg_2775" s="T1657">женщина.[NOM.SG]</ta>
            <ta e="T1659" id="Seg_2776" s="T1658">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1660" id="Seg_2777" s="T1659">видеть-PRS-3SG.O</ta>
            <ta e="T1661" id="Seg_2778" s="T1660">PTCL</ta>
            <ta e="T1662" id="Seg_2779" s="T1661">дочь-NOM/GEN.3SG</ta>
            <ta e="T1663" id="Seg_2780" s="T1662">умереть-RES-PST.[3SG]</ta>
            <ta e="T1664" id="Seg_2781" s="T1663">лежать-DUR.[3SG]</ta>
            <ta e="T1665" id="Seg_2782" s="T1664">INCH</ta>
            <ta e="T1666" id="Seg_2783" s="T1665">плакать-INF.LAT</ta>
            <ta e="T1667" id="Seg_2784" s="T1666">что.[NOM.SG]=INDEF</ta>
            <ta e="T1668" id="Seg_2785" s="T1667">NEG</ta>
            <ta e="T1669" id="Seg_2786" s="T1668">делать-FUT-2SG</ta>
            <ta e="T1670" id="Seg_2787" s="T1669">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1404" id="Seg_2788" s="T1403">v-v:tense-v:pn</ta>
            <ta e="T1405" id="Seg_2789" s="T1404">n-n:case</ta>
            <ta e="T1406" id="Seg_2790" s="T1405">n-n:case</ta>
            <ta e="T1407" id="Seg_2791" s="T1406">n-n:case</ta>
            <ta e="T1408" id="Seg_2792" s="T1407">v-v:tense-v:pn</ta>
            <ta e="T1409" id="Seg_2793" s="T1408">n-n:case.poss</ta>
            <ta e="T1410" id="Seg_2794" s="T1409">conj</ta>
            <ta e="T1411" id="Seg_2795" s="T1410">n-n:case</ta>
            <ta e="T1412" id="Seg_2796" s="T1411">v-v:tense-v:pn</ta>
            <ta e="T1413" id="Seg_2797" s="T1412">n-n:case.poss</ta>
            <ta e="T1414" id="Seg_2798" s="T1413">dempro-n:case</ta>
            <ta e="T1415" id="Seg_2799" s="T1414">n-n:case</ta>
            <ta e="T1416" id="Seg_2800" s="T1415">n-n:case</ta>
            <ta e="T1417" id="Seg_2801" s="T1416">n-n:case.poss</ta>
            <ta e="T1418" id="Seg_2802" s="T1417">ptcl</ta>
            <ta e="T1419" id="Seg_2803" s="T1418">v-v:tense-v:pn</ta>
            <ta e="T1420" id="Seg_2804" s="T1419">v-v:tense-v:pn</ta>
            <ta e="T1421" id="Seg_2805" s="T1420">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1422" id="Seg_2806" s="T1421">conj</ta>
            <ta e="T1423" id="Seg_2807" s="T1422">refl-n:case.poss</ta>
            <ta e="T1424" id="Seg_2808" s="T1423">n-n:case.poss</ta>
            <ta e="T1425" id="Seg_2809" s="T1424">adv</ta>
            <ta e="T1426" id="Seg_2810" s="T1425">v-v&gt;v-v:pn</ta>
            <ta e="T1427" id="Seg_2811" s="T1426">n-n:case.poss</ta>
            <ta e="T1428" id="Seg_2812" s="T1427">ptcl</ta>
            <ta e="T1429" id="Seg_2813" s="T1428">v-v&gt;v-v:pn</ta>
            <ta e="T1430" id="Seg_2814" s="T1429">adv</ta>
            <ta e="T1432" id="Seg_2815" s="T1431">n-n:case</ta>
            <ta e="T1433" id="Seg_2816" s="T1432">v-v&gt;v-v:pn</ta>
            <ta e="T1434" id="Seg_2817" s="T1433">v-v:mood.pn</ta>
            <ta e="T1435" id="Seg_2818" s="T1434">refl-n:case.poss</ta>
            <ta e="T1436" id="Seg_2819" s="T1435">n-n:case</ta>
            <ta e="T1437" id="Seg_2820" s="T1436">pers</ta>
            <ta e="T1438" id="Seg_2821" s="T1437">dempro-n:case</ta>
            <ta e="T1439" id="Seg_2822" s="T1438">dempro-n:case</ta>
            <ta e="T1440" id="Seg_2823" s="T1439">pers</ta>
            <ta e="T1441" id="Seg_2824" s="T1440">que-n:case=ptcl</ta>
            <ta e="T1442" id="Seg_2825" s="T1441">ptcl</ta>
            <ta e="T1443" id="Seg_2826" s="T1442">v-v:tense-v:pn</ta>
            <ta e="T1444" id="Seg_2827" s="T1443">dempro-n:case</ta>
            <ta e="T1445" id="Seg_2828" s="T1444">n-n:case</ta>
            <ta e="T1446" id="Seg_2829" s="T1445">v-v:tense-v:pn</ta>
            <ta e="T1447" id="Seg_2830" s="T1446">n-n:case.poss</ta>
            <ta e="T1448" id="Seg_2831" s="T1447">v-v:tense-v:pn</ta>
            <ta e="T1449" id="Seg_2832" s="T1448">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1450" id="Seg_2833" s="T1449">n-n:case</ta>
            <ta e="T1451" id="Seg_2834" s="T1450">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1452" id="Seg_2835" s="T1451">n-n:case</ta>
            <ta e="T1453" id="Seg_2836" s="T1452">n-n:case.poss</ta>
            <ta e="T1456" id="Seg_2837" s="T1455">n-n:case</ta>
            <ta e="T1457" id="Seg_2838" s="T1456">adj-n:case</ta>
            <ta e="T1458" id="Seg_2839" s="T1457">dempro-n:case</ta>
            <ta e="T1459" id="Seg_2840" s="T1458">v-v&gt;v-v:pn</ta>
            <ta e="T1460" id="Seg_2841" s="T1459">v-v&gt;v-v:pn</ta>
            <ta e="T1461" id="Seg_2842" s="T1460">adj-n:case</ta>
            <ta e="T1462" id="Seg_2843" s="T1461">v-v:tense-v:pn</ta>
            <ta e="T1465" id="Seg_2844" s="T1464">n-n:case</ta>
            <ta e="T1466" id="Seg_2845" s="T1465">n-n:case</ta>
            <ta e="T1467" id="Seg_2846" s="T1466">v-v&gt;v-v:pn</ta>
            <ta e="T1468" id="Seg_2847" s="T1467">v-v&gt;v-v:pn</ta>
            <ta e="T1469" id="Seg_2848" s="T1468">dempro-n:case</ta>
            <ta e="T1470" id="Seg_2849" s="T1469">pers</ta>
            <ta e="T1471" id="Seg_2850" s="T1470">adj-n:case</ta>
            <ta e="T1472" id="Seg_2851" s="T1471">ptcl</ta>
            <ta e="T1473" id="Seg_2852" s="T1472">pers</ta>
            <ta e="T1474" id="Seg_2853" s="T1473">ptcl</ta>
            <ta e="T1475" id="Seg_2854" s="T1474">ptcl</ta>
            <ta e="T1476" id="Seg_2855" s="T1475">adj-n:case</ta>
            <ta e="T1477" id="Seg_2856" s="T1476">adv</ta>
            <ta e="T1478" id="Seg_2857" s="T1477">v-v:tense-v:pn</ta>
            <ta e="T1479" id="Seg_2858" s="T1478">adj-n:case</ta>
            <ta e="T1480" id="Seg_2859" s="T1479">pers</ta>
            <ta e="T1481" id="Seg_2860" s="T1480">ptcl</ta>
            <ta e="T1482" id="Seg_2861" s="T1481">adj-n:case</ta>
            <ta e="T1483" id="Seg_2862" s="T1482">adj-n:case</ta>
            <ta e="T1484" id="Seg_2863" s="T1483">conj</ta>
            <ta e="T1485" id="Seg_2864" s="T1484">refl-n:case.poss</ta>
            <ta e="T1486" id="Seg_2865" s="T1485">v-v:n.fin</ta>
            <ta e="T1487" id="Seg_2866" s="T1486">ptcl</ta>
            <ta e="T1488" id="Seg_2867" s="T1487">v-v:tense-v:pn</ta>
            <ta e="T1489" id="Seg_2868" s="T1488">adv</ta>
            <ta e="T1490" id="Seg_2869" s="T1489">adv</ta>
            <ta e="T1491" id="Seg_2870" s="T1490">v-v&gt;v-v:pn</ta>
            <ta e="T1492" id="Seg_2871" s="T1491">ptcl</ta>
            <ta e="T1493" id="Seg_2872" s="T1492">adj-n:case</ta>
            <ta e="T1494" id="Seg_2873" s="T1493">refl-n:case</ta>
            <ta e="T1495" id="Seg_2874" s="T1494">n-n:case.poss</ta>
            <ta e="T1496" id="Seg_2875" s="T1495">dempro-n:case</ta>
            <ta e="T1497" id="Seg_2876" s="T1496">ptcl</ta>
            <ta e="T1498" id="Seg_2877" s="T1497">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1499" id="Seg_2878" s="T1498">adv</ta>
            <ta e="T1500" id="Seg_2879" s="T1499">dempro-n:case</ta>
            <ta e="T1501" id="Seg_2880" s="T1500">dempro-n:case</ta>
            <ta e="T1502" id="Seg_2881" s="T1501">n-n:case</ta>
            <ta e="T1503" id="Seg_2882" s="T1502">v-v:tense-v:pn</ta>
            <ta e="T1504" id="Seg_2883" s="T1503">n-n:case</ta>
            <ta e="T1505" id="Seg_2884" s="T1504">v-v:tense-v:pn</ta>
            <ta e="T1506" id="Seg_2885" s="T1505">dempro-n:case</ta>
            <ta e="T1507" id="Seg_2886" s="T1506">adj-adj&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1508" id="Seg_2887" s="T1507">adv</ta>
            <ta e="T1509" id="Seg_2888" s="T1508">n-n:case</ta>
            <ta e="T1510" id="Seg_2889" s="T1509">v-v&gt;v-v:pn</ta>
            <ta e="T1511" id="Seg_2890" s="T1510">n-n:num</ta>
            <ta e="T1512" id="Seg_2891" s="T1511">v-v:ins-v:mood.pn</ta>
            <ta e="T1513" id="Seg_2892" s="T1512">n-n:case</ta>
            <ta e="T1514" id="Seg_2893" s="T1513">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1515" id="Seg_2894" s="T1514">v-v:mood.pn</ta>
            <ta e="T1516" id="Seg_2895" s="T1515">adv</ta>
            <ta e="T1517" id="Seg_2896" s="T1516">n-n:case</ta>
            <ta e="T1518" id="Seg_2897" s="T1517">n-n:case.poss</ta>
            <ta e="T1519" id="Seg_2898" s="T1518">n-n:case.poss</ta>
            <ta e="T1520" id="Seg_2899" s="T1519">v-v&gt;v-v:pn</ta>
            <ta e="T1521" id="Seg_2900" s="T1520">refl-n:case.poss</ta>
            <ta e="T1522" id="Seg_2901" s="T1521">v-v&gt;v-v:pn</ta>
            <ta e="T1523" id="Seg_2902" s="T1522">n-n:case</ta>
            <ta e="T1524" id="Seg_2903" s="T1523">n-n:case.poss</ta>
            <ta e="T1525" id="Seg_2904" s="T1524">v-v:tense-v:pn</ta>
            <ta e="T1527" id="Seg_2905" s="T1526">quant</ta>
            <ta e="T1528" id="Seg_2906" s="T1527">ptcl</ta>
            <ta e="T1529" id="Seg_2907" s="T1528">n-n:case</ta>
            <ta e="T1530" id="Seg_2908" s="T1529">v-v&gt;v-v:pn</ta>
            <ta e="T1531" id="Seg_2909" s="T1530">n-n:case</ta>
            <ta e="T1532" id="Seg_2910" s="T1531">quant</ta>
            <ta e="T1533" id="Seg_2911" s="T1532">dempro-n:case</ta>
            <ta e="T1534" id="Seg_2912" s="T1533">v-v&gt;v-v:pn</ta>
            <ta e="T1535" id="Seg_2913" s="T1534">ptcl</ta>
            <ta e="T1536" id="Seg_2914" s="T1535">aux-v:mood.pn</ta>
            <ta e="T1537" id="Seg_2915" s="T1536">v-v:ins-v:mood.pn</ta>
            <ta e="T1538" id="Seg_2916" s="T1537">n-n:case</ta>
            <ta e="T1539" id="Seg_2917" s="T1538">n-n:case.poss</ta>
            <ta e="T1540" id="Seg_2918" s="T1539">n-n:case</ta>
            <ta e="T1541" id="Seg_2919" s="T1540">v-v&gt;v-v:pn</ta>
            <ta e="T1542" id="Seg_2920" s="T1541">conj</ta>
            <ta e="T1543" id="Seg_2921" s="T1542">n-n:case</ta>
            <ta e="T1544" id="Seg_2922" s="T1543">adv</ta>
            <ta e="T1546" id="Seg_2923" s="T1545">n-n:case</ta>
            <ta e="T1547" id="Seg_2924" s="T1546">adv</ta>
            <ta e="T1548" id="Seg_2925" s="T1547">dempro-n:case</ta>
            <ta e="T1549" id="Seg_2926" s="T1548">ptcl</ta>
            <ta e="T1550" id="Seg_2927" s="T1549">v-v&gt;v-v:pn</ta>
            <ta e="T1551" id="Seg_2928" s="T1550">adv</ta>
            <ta e="T1552" id="Seg_2929" s="T1551">n-n:case</ta>
            <ta e="T1553" id="Seg_2930" s="T1552">v-v:tense-v:pn</ta>
            <ta e="T1554" id="Seg_2931" s="T1553">n-n:case</ta>
            <ta e="T1555" id="Seg_2932" s="T1554">n-n:case</ta>
            <ta e="T1556" id="Seg_2933" s="T1555">v-v:tense-v:pn</ta>
            <ta e="T1557" id="Seg_2934" s="T1556">ptcl</ta>
            <ta e="T1558" id="Seg_2935" s="T1557">n-n:case</ta>
            <ta e="T1559" id="Seg_2936" s="T1558">quant</ta>
            <ta e="T1560" id="Seg_2937" s="T1559">n-n:case.poss</ta>
            <ta e="T1561" id="Seg_2938" s="T1560">n-n:case</ta>
            <ta e="T1562" id="Seg_2939" s="T1561">v-v:tense-v:pn</ta>
            <ta e="T1563" id="Seg_2940" s="T1562">adv</ta>
            <ta e="T1564" id="Seg_2941" s="T1563">dempro-n:case</ta>
            <ta e="T1565" id="Seg_2942" s="T1564">n-n:case</ta>
            <ta e="T1566" id="Seg_2943" s="T1565">v-v:mood.pn</ta>
            <ta e="T1567" id="Seg_2944" s="T1566">pers</ta>
            <ta e="T1568" id="Seg_2945" s="T1567">n-n:case.poss</ta>
            <ta e="T1569" id="Seg_2946" s="T1568">dempro-n:case</ta>
            <ta e="T1570" id="Seg_2947" s="T1569">n-n:case</ta>
            <ta e="T1571" id="Seg_2948" s="T1570">v-v:tense-v:pn</ta>
            <ta e="T1572" id="Seg_2949" s="T1571">n-n:case</ta>
            <ta e="T1573" id="Seg_2950" s="T1572">v-v:tense-v:pn</ta>
            <ta e="T1574" id="Seg_2951" s="T1573">dempro-n:case</ta>
            <ta e="T1575" id="Seg_2952" s="T1574">n-n:case.poss</ta>
            <ta e="T1576" id="Seg_2953" s="T1575">adv</ta>
            <ta e="T1577" id="Seg_2954" s="T1576">ptcl</ta>
            <ta e="T1578" id="Seg_2955" s="T1577">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1579" id="Seg_2956" s="T1578">conj</ta>
            <ta e="T1580" id="Seg_2957" s="T1579">refl-n:case.poss</ta>
            <ta e="T1581" id="Seg_2958" s="T1580">v-v:tense-v:pn</ta>
            <ta e="T1582" id="Seg_2959" s="T1581">adv</ta>
            <ta e="T1583" id="Seg_2960" s="T1582">adv</ta>
            <ta e="T1584" id="Seg_2961" s="T1583">adj-n:case</ta>
            <ta e="T1585" id="Seg_2962" s="T1584">dempro-n:case</ta>
            <ta e="T1586" id="Seg_2963" s="T1585">v-v:tense-v:pn</ta>
            <ta e="T1587" id="Seg_2964" s="T1586">dempro-n:case</ta>
            <ta e="T1588" id="Seg_2965" s="T1587">ptcl</ta>
            <ta e="T1589" id="Seg_2966" s="T1588">adj-n:case</ta>
            <ta e="T1590" id="Seg_2967" s="T1589">pers</ta>
            <ta e="T1591" id="Seg_2968" s="T1590">adj-n:case</ta>
            <ta e="T1592" id="Seg_2969" s="T1591">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1593" id="Seg_2970" s="T1592">aux-v:mood.pn</ta>
            <ta e="T1595" id="Seg_2971" s="T1594">aux-v:mood.pn</ta>
            <ta e="T1596" id="Seg_2972" s="T1595">v-v:mood.pn</ta>
            <ta e="T1597" id="Seg_2973" s="T1596">pers</ta>
            <ta e="T1598" id="Seg_2974" s="T1597">dempro-n:case</ta>
            <ta e="T1599" id="Seg_2975" s="T1598">adv</ta>
            <ta e="T1600" id="Seg_2976" s="T1599">v-v:tense-v:pn</ta>
            <ta e="T1601" id="Seg_2977" s="T1600">adj-n:case</ta>
            <ta e="T1602" id="Seg_2978" s="T1601">pers</ta>
            <ta e="T1603" id="Seg_2979" s="T1602">adj-n:case</ta>
            <ta e="T1604" id="Seg_2980" s="T1603">ptcl</ta>
            <ta e="T1605" id="Seg_2981" s="T1604">v-v&gt;v-v:n.fin</ta>
            <ta e="T1606" id="Seg_2982" s="T1605">dempro-n:case</ta>
            <ta e="T1607" id="Seg_2983" s="T1606">adv</ta>
            <ta e="T1608" id="Seg_2984" s="T1607">adv</ta>
            <ta e="T1609" id="Seg_2985" s="T1608">v-v:tense-v:pn</ta>
            <ta e="T1610" id="Seg_2986" s="T1609">dempro-n:case</ta>
            <ta e="T1611" id="Seg_2987" s="T1610">adv</ta>
            <ta e="T1612" id="Seg_2988" s="T1611">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1613" id="Seg_2989" s="T1612">adj</ta>
            <ta e="T1614" id="Seg_2990" s="T1613">adj</ta>
            <ta e="T1615" id="Seg_2991" s="T1614">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1616" id="Seg_2992" s="T1615">dempro-n:case</ta>
            <ta e="T1617" id="Seg_2993" s="T1616">dempro-n:case</ta>
            <ta e="T1618" id="Seg_2994" s="T1617">ptcl</ta>
            <ta e="T1619" id="Seg_2995" s="T1618">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1620" id="Seg_2996" s="T1619">conj</ta>
            <ta e="T1621" id="Seg_2997" s="T1620">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1622" id="Seg_2998" s="T1621">ptcl</ta>
            <ta e="T1623" id="Seg_2999" s="T1622">n-n:case</ta>
            <ta e="T1624" id="Seg_3000" s="T1623">adj-n:case</ta>
            <ta e="T1625" id="Seg_3001" s="T1624">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1626" id="Seg_3002" s="T1625">adv</ta>
            <ta e="T1627" id="Seg_3003" s="T1626">n-n:case</ta>
            <ta e="T1628" id="Seg_3004" s="T1627">n-n:case</ta>
            <ta e="T1629" id="Seg_3005" s="T1628">v-v:tense-v:pn</ta>
            <ta e="T1630" id="Seg_3006" s="T1629">v-v:ins-v:mood.pn</ta>
            <ta e="T1631" id="Seg_3007" s="T1630">pers</ta>
            <ta e="T1632" id="Seg_3008" s="T1631">n-n:case</ta>
            <ta e="T1633" id="Seg_3009" s="T1632">v-v:n.fin</ta>
            <ta e="T1634" id="Seg_3010" s="T1633">dempro-n:case</ta>
            <ta e="T1635" id="Seg_3011" s="T1634">v-v:tense-v:pn</ta>
            <ta e="T1636" id="Seg_3012" s="T1635">n-n:case</ta>
            <ta e="T1637" id="Seg_3013" s="T1636">v-v&gt;v-v:pn</ta>
            <ta e="T1638" id="Seg_3014" s="T1637">n-n:case</ta>
            <ta e="T1639" id="Seg_3015" s="T1638">n-n:case.poss</ta>
            <ta e="T1640" id="Seg_3016" s="T1639">v-v&gt;v-v:pn</ta>
            <ta e="T1641" id="Seg_3017" s="T1640">n-n:case</ta>
            <ta e="T1642" id="Seg_3018" s="T1641">ptcl</ta>
            <ta e="T1643" id="Seg_3019" s="T1642">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1644" id="Seg_3020" s="T1643">adv</ta>
            <ta e="T1645" id="Seg_3021" s="T1644">ptcl</ta>
            <ta e="T1646" id="Seg_3022" s="T1645">aux-v:mood.pn</ta>
            <ta e="T1647" id="Seg_3023" s="T1646">v-v:ins-v:mood.pn</ta>
            <ta e="T1648" id="Seg_3024" s="T1647">conj</ta>
            <ta e="T1649" id="Seg_3025" s="T1648">v-v:ins-v:mood.pn</ta>
            <ta e="T1650" id="Seg_3026" s="T1649">n-n:case</ta>
            <ta e="T1651" id="Seg_3027" s="T1650">n-n:case.poss</ta>
            <ta e="T1652" id="Seg_3028" s="T1651">dempro-n:case</ta>
            <ta e="T1653" id="Seg_3029" s="T1652">adv</ta>
            <ta e="T1654" id="Seg_3030" s="T1653">ptcl</ta>
            <ta e="T1655" id="Seg_3031" s="T1654">adv</ta>
            <ta e="T1656" id="Seg_3032" s="T1655">n-n:case</ta>
            <ta e="T1657" id="Seg_3033" s="T1656">v-v:tense-v:pn</ta>
            <ta e="T1658" id="Seg_3034" s="T1657">n-n:case</ta>
            <ta e="T1659" id="Seg_3035" s="T1658">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1660" id="Seg_3036" s="T1659">v-v:tense-v:pn</ta>
            <ta e="T1661" id="Seg_3037" s="T1660">ptcl</ta>
            <ta e="T1662" id="Seg_3038" s="T1661">n-n:case.poss</ta>
            <ta e="T1663" id="Seg_3039" s="T1662">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1664" id="Seg_3040" s="T1663">v-v&gt;v-v:pn</ta>
            <ta e="T1665" id="Seg_3041" s="T1664">ptcl</ta>
            <ta e="T1666" id="Seg_3042" s="T1665">v-v:n.fin</ta>
            <ta e="T1667" id="Seg_3043" s="T1666">que-n:case=ptcl</ta>
            <ta e="T1668" id="Seg_3044" s="T1667">ptcl</ta>
            <ta e="T1669" id="Seg_3045" s="T1668">v-v:tense-v:pn</ta>
            <ta e="T1670" id="Seg_3046" s="T1669">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1404" id="Seg_3047" s="T1403">v</ta>
            <ta e="T1405" id="Seg_3048" s="T1404">n</ta>
            <ta e="T1406" id="Seg_3049" s="T1405">n</ta>
            <ta e="T1407" id="Seg_3050" s="T1406">n</ta>
            <ta e="T1408" id="Seg_3051" s="T1407">v</ta>
            <ta e="T1409" id="Seg_3052" s="T1408">n</ta>
            <ta e="T1410" id="Seg_3053" s="T1409">conj</ta>
            <ta e="T1411" id="Seg_3054" s="T1410">n</ta>
            <ta e="T1412" id="Seg_3055" s="T1411">v</ta>
            <ta e="T1413" id="Seg_3056" s="T1412">n</ta>
            <ta e="T1414" id="Seg_3057" s="T1413">dempro</ta>
            <ta e="T1415" id="Seg_3058" s="T1414">n</ta>
            <ta e="T1416" id="Seg_3059" s="T1415">n</ta>
            <ta e="T1417" id="Seg_3060" s="T1416">n</ta>
            <ta e="T1418" id="Seg_3061" s="T1417">ptcl</ta>
            <ta e="T1419" id="Seg_3062" s="T1418">v</ta>
            <ta e="T1420" id="Seg_3063" s="T1419">v</ta>
            <ta e="T1421" id="Seg_3064" s="T1420">v</ta>
            <ta e="T1422" id="Seg_3065" s="T1421">conj</ta>
            <ta e="T1423" id="Seg_3066" s="T1422">refl</ta>
            <ta e="T1424" id="Seg_3067" s="T1423">n</ta>
            <ta e="T1425" id="Seg_3068" s="T1424">adv</ta>
            <ta e="T1426" id="Seg_3069" s="T1425">v</ta>
            <ta e="T1427" id="Seg_3070" s="T1426">n</ta>
            <ta e="T1428" id="Seg_3071" s="T1427">ptcl</ta>
            <ta e="T1429" id="Seg_3072" s="T1428">v</ta>
            <ta e="T1430" id="Seg_3073" s="T1429">adv</ta>
            <ta e="T1432" id="Seg_3074" s="T1431">n</ta>
            <ta e="T1433" id="Seg_3075" s="T1432">v</ta>
            <ta e="T1434" id="Seg_3076" s="T1433">v</ta>
            <ta e="T1435" id="Seg_3077" s="T1434">refl</ta>
            <ta e="T1436" id="Seg_3078" s="T1435">n</ta>
            <ta e="T1437" id="Seg_3079" s="T1436">pers</ta>
            <ta e="T1438" id="Seg_3080" s="T1437">dempro</ta>
            <ta e="T1439" id="Seg_3081" s="T1438">dempro</ta>
            <ta e="T1440" id="Seg_3082" s="T1439">pers</ta>
            <ta e="T1441" id="Seg_3083" s="T1440">que</ta>
            <ta e="T1442" id="Seg_3084" s="T1441">ptcl</ta>
            <ta e="T1443" id="Seg_3085" s="T1442">v</ta>
            <ta e="T1444" id="Seg_3086" s="T1443">dempro</ta>
            <ta e="T1445" id="Seg_3087" s="T1444">n</ta>
            <ta e="T1446" id="Seg_3088" s="T1445">v</ta>
            <ta e="T1447" id="Seg_3089" s="T1446">n</ta>
            <ta e="T1448" id="Seg_3090" s="T1447">v</ta>
            <ta e="T1449" id="Seg_3091" s="T1448">v</ta>
            <ta e="T1450" id="Seg_3092" s="T1449">n</ta>
            <ta e="T1451" id="Seg_3093" s="T1450">v</ta>
            <ta e="T1452" id="Seg_3094" s="T1451">n</ta>
            <ta e="T1453" id="Seg_3095" s="T1452">n</ta>
            <ta e="T1456" id="Seg_3096" s="T1455">n</ta>
            <ta e="T1457" id="Seg_3097" s="T1456">adj</ta>
            <ta e="T1458" id="Seg_3098" s="T1457">dempro</ta>
            <ta e="T1459" id="Seg_3099" s="T1458">v</ta>
            <ta e="T1460" id="Seg_3100" s="T1459">v</ta>
            <ta e="T1461" id="Seg_3101" s="T1460">adj</ta>
            <ta e="T1462" id="Seg_3102" s="T1461">v</ta>
            <ta e="T1465" id="Seg_3103" s="T1464">n</ta>
            <ta e="T1466" id="Seg_3104" s="T1465">n</ta>
            <ta e="T1467" id="Seg_3105" s="T1466">v</ta>
            <ta e="T1468" id="Seg_3106" s="T1467">v</ta>
            <ta e="T1469" id="Seg_3107" s="T1468">dempro</ta>
            <ta e="T1470" id="Seg_3108" s="T1469">pers</ta>
            <ta e="T1471" id="Seg_3109" s="T1470">adj</ta>
            <ta e="T1472" id="Seg_3110" s="T1471">ptcl</ta>
            <ta e="T1473" id="Seg_3111" s="T1472">pers</ta>
            <ta e="T1474" id="Seg_3112" s="T1473">ptcl</ta>
            <ta e="T1475" id="Seg_3113" s="T1474">ptcl</ta>
            <ta e="T1476" id="Seg_3114" s="T1475">adj</ta>
            <ta e="T1477" id="Seg_3115" s="T1476">adv</ta>
            <ta e="T1478" id="Seg_3116" s="T1477">v</ta>
            <ta e="T1479" id="Seg_3117" s="T1478">adj</ta>
            <ta e="T1480" id="Seg_3118" s="T1479">pers</ta>
            <ta e="T1481" id="Seg_3119" s="T1480">ptcl</ta>
            <ta e="T1482" id="Seg_3120" s="T1481">adj</ta>
            <ta e="T1483" id="Seg_3121" s="T1482">adj</ta>
            <ta e="T1484" id="Seg_3122" s="T1483">conj</ta>
            <ta e="T1485" id="Seg_3123" s="T1484">refl</ta>
            <ta e="T1486" id="Seg_3124" s="T1485">v</ta>
            <ta e="T1487" id="Seg_3125" s="T1486">ptcl</ta>
            <ta e="T1488" id="Seg_3126" s="T1487">v</ta>
            <ta e="T1489" id="Seg_3127" s="T1488">adv</ta>
            <ta e="T1490" id="Seg_3128" s="T1489">adv</ta>
            <ta e="T1491" id="Seg_3129" s="T1490">v</ta>
            <ta e="T1492" id="Seg_3130" s="T1491">ptcl</ta>
            <ta e="T1493" id="Seg_3131" s="T1492">adj</ta>
            <ta e="T1494" id="Seg_3132" s="T1493">refl</ta>
            <ta e="T1495" id="Seg_3133" s="T1494">n</ta>
            <ta e="T1496" id="Seg_3134" s="T1495">dempro</ta>
            <ta e="T1497" id="Seg_3135" s="T1496">ptcl</ta>
            <ta e="T1498" id="Seg_3136" s="T1497">v</ta>
            <ta e="T1499" id="Seg_3137" s="T1498">adv</ta>
            <ta e="T1500" id="Seg_3138" s="T1499">dempro</ta>
            <ta e="T1501" id="Seg_3139" s="T1500">dempro</ta>
            <ta e="T1502" id="Seg_3140" s="T1501">n</ta>
            <ta e="T1503" id="Seg_3141" s="T1502">v</ta>
            <ta e="T1504" id="Seg_3142" s="T1503">n</ta>
            <ta e="T1505" id="Seg_3143" s="T1504">v</ta>
            <ta e="T1506" id="Seg_3144" s="T1505">dempro</ta>
            <ta e="T1507" id="Seg_3145" s="T1506">v</ta>
            <ta e="T1508" id="Seg_3146" s="T1507">adv</ta>
            <ta e="T1509" id="Seg_3147" s="T1508">n</ta>
            <ta e="T1510" id="Seg_3148" s="T1509">v</ta>
            <ta e="T1511" id="Seg_3149" s="T1510">n</ta>
            <ta e="T1512" id="Seg_3150" s="T1511">v</ta>
            <ta e="T1513" id="Seg_3151" s="T1512">n</ta>
            <ta e="T1514" id="Seg_3152" s="T1513">v</ta>
            <ta e="T1515" id="Seg_3153" s="T1514">v</ta>
            <ta e="T1516" id="Seg_3154" s="T1515">adv</ta>
            <ta e="T1517" id="Seg_3155" s="T1516">n</ta>
            <ta e="T1518" id="Seg_3156" s="T1517">n</ta>
            <ta e="T1519" id="Seg_3157" s="T1518">n</ta>
            <ta e="T1520" id="Seg_3158" s="T1519">v</ta>
            <ta e="T1521" id="Seg_3159" s="T1520">refl</ta>
            <ta e="T1522" id="Seg_3160" s="T1521">v</ta>
            <ta e="T1523" id="Seg_3161" s="T1522">n</ta>
            <ta e="T1524" id="Seg_3162" s="T1523">n</ta>
            <ta e="T1525" id="Seg_3163" s="T1524">v</ta>
            <ta e="T1527" id="Seg_3164" s="T1526">quant</ta>
            <ta e="T1528" id="Seg_3165" s="T1527">ptcl</ta>
            <ta e="T1529" id="Seg_3166" s="T1528">n</ta>
            <ta e="T1530" id="Seg_3167" s="T1529">v</ta>
            <ta e="T1531" id="Seg_3168" s="T1530">n</ta>
            <ta e="T1532" id="Seg_3169" s="T1531">quant</ta>
            <ta e="T1533" id="Seg_3170" s="T1532">dempro</ta>
            <ta e="T1534" id="Seg_3171" s="T1533">v</ta>
            <ta e="T1535" id="Seg_3172" s="T1534">ptcl</ta>
            <ta e="T1536" id="Seg_3173" s="T1535">aux</ta>
            <ta e="T1537" id="Seg_3174" s="T1536">v</ta>
            <ta e="T1538" id="Seg_3175" s="T1537">n</ta>
            <ta e="T1539" id="Seg_3176" s="T1538">n</ta>
            <ta e="T1540" id="Seg_3177" s="T1539">n</ta>
            <ta e="T1541" id="Seg_3178" s="T1540">v</ta>
            <ta e="T1542" id="Seg_3179" s="T1541">conj</ta>
            <ta e="T1543" id="Seg_3180" s="T1542">n</ta>
            <ta e="T1544" id="Seg_3181" s="T1543">adv</ta>
            <ta e="T1546" id="Seg_3182" s="T1545">n</ta>
            <ta e="T1547" id="Seg_3183" s="T1546">adv</ta>
            <ta e="T1548" id="Seg_3184" s="T1547">dempro</ta>
            <ta e="T1549" id="Seg_3185" s="T1548">ptcl</ta>
            <ta e="T1550" id="Seg_3186" s="T1549">v</ta>
            <ta e="T1551" id="Seg_3187" s="T1550">adv</ta>
            <ta e="T1552" id="Seg_3188" s="T1551">n</ta>
            <ta e="T1553" id="Seg_3189" s="T1552">v</ta>
            <ta e="T1554" id="Seg_3190" s="T1553">n</ta>
            <ta e="T1555" id="Seg_3191" s="T1554">n</ta>
            <ta e="T1556" id="Seg_3192" s="T1555">v</ta>
            <ta e="T1557" id="Seg_3193" s="T1556">ptcl</ta>
            <ta e="T1558" id="Seg_3194" s="T1557">n</ta>
            <ta e="T1559" id="Seg_3195" s="T1558">quant</ta>
            <ta e="T1560" id="Seg_3196" s="T1559">n</ta>
            <ta e="T1561" id="Seg_3197" s="T1560">n</ta>
            <ta e="T1562" id="Seg_3198" s="T1561">v</ta>
            <ta e="T1563" id="Seg_3199" s="T1562">adv</ta>
            <ta e="T1564" id="Seg_3200" s="T1563">dempro</ta>
            <ta e="T1565" id="Seg_3201" s="T1564">n</ta>
            <ta e="T1566" id="Seg_3202" s="T1565">v</ta>
            <ta e="T1567" id="Seg_3203" s="T1566">pers</ta>
            <ta e="T1568" id="Seg_3204" s="T1567">n</ta>
            <ta e="T1569" id="Seg_3205" s="T1568">dempro</ta>
            <ta e="T1570" id="Seg_3206" s="T1569">n</ta>
            <ta e="T1571" id="Seg_3207" s="T1570">v</ta>
            <ta e="T1572" id="Seg_3208" s="T1571">n</ta>
            <ta e="T1573" id="Seg_3209" s="T1572">v</ta>
            <ta e="T1574" id="Seg_3210" s="T1573">dempro</ta>
            <ta e="T1575" id="Seg_3211" s="T1574">n</ta>
            <ta e="T1576" id="Seg_3212" s="T1575">adv</ta>
            <ta e="T1577" id="Seg_3213" s="T1576">ptcl</ta>
            <ta e="T1578" id="Seg_3214" s="T1577">v</ta>
            <ta e="T1579" id="Seg_3215" s="T1578">conj</ta>
            <ta e="T1580" id="Seg_3216" s="T1579">refl</ta>
            <ta e="T1581" id="Seg_3217" s="T1580">v</ta>
            <ta e="T1582" id="Seg_3218" s="T1581">adv</ta>
            <ta e="T1583" id="Seg_3219" s="T1582">adv</ta>
            <ta e="T1584" id="Seg_3220" s="T1583">adj</ta>
            <ta e="T1585" id="Seg_3221" s="T1584">dempro</ta>
            <ta e="T1586" id="Seg_3222" s="T1585">v</ta>
            <ta e="T1587" id="Seg_3223" s="T1586">dempro</ta>
            <ta e="T1588" id="Seg_3224" s="T1587">ptcl</ta>
            <ta e="T1589" id="Seg_3225" s="T1588">adj</ta>
            <ta e="T1590" id="Seg_3226" s="T1589">pers</ta>
            <ta e="T1591" id="Seg_3227" s="T1590">adj</ta>
            <ta e="T1593" id="Seg_3228" s="T1592">aux</ta>
            <ta e="T1595" id="Seg_3229" s="T1594">aux</ta>
            <ta e="T1596" id="Seg_3230" s="T1595">v</ta>
            <ta e="T1597" id="Seg_3231" s="T1596">pers</ta>
            <ta e="T1598" id="Seg_3232" s="T1597">dempro</ta>
            <ta e="T1599" id="Seg_3233" s="T1598">adv</ta>
            <ta e="T1600" id="Seg_3234" s="T1599">v</ta>
            <ta e="T1601" id="Seg_3235" s="T1600">adj</ta>
            <ta e="T1602" id="Seg_3236" s="T1601">pers</ta>
            <ta e="T1603" id="Seg_3237" s="T1602">adj</ta>
            <ta e="T1604" id="Seg_3238" s="T1603">ptcl</ta>
            <ta e="T1605" id="Seg_3239" s="T1604">v</ta>
            <ta e="T1606" id="Seg_3240" s="T1605">dempro</ta>
            <ta e="T1607" id="Seg_3241" s="T1606">adv</ta>
            <ta e="T1608" id="Seg_3242" s="T1607">adv</ta>
            <ta e="T1609" id="Seg_3243" s="T1608">v</ta>
            <ta e="T1610" id="Seg_3244" s="T1609">dempro</ta>
            <ta e="T1611" id="Seg_3245" s="T1610">adv</ta>
            <ta e="T1612" id="Seg_3246" s="T1611">v</ta>
            <ta e="T1613" id="Seg_3247" s="T1612">adj</ta>
            <ta e="T1614" id="Seg_3248" s="T1613">adj</ta>
            <ta e="T1615" id="Seg_3249" s="T1614">v</ta>
            <ta e="T1616" id="Seg_3250" s="T1615">dempro</ta>
            <ta e="T1617" id="Seg_3251" s="T1616">dempro</ta>
            <ta e="T1618" id="Seg_3252" s="T1617">ptcl</ta>
            <ta e="T1619" id="Seg_3253" s="T1618">v</ta>
            <ta e="T1620" id="Seg_3254" s="T1619">conj</ta>
            <ta e="T1621" id="Seg_3255" s="T1620">v</ta>
            <ta e="T1622" id="Seg_3256" s="T1621">ptcl</ta>
            <ta e="T1623" id="Seg_3257" s="T1622">n</ta>
            <ta e="T1624" id="Seg_3258" s="T1623">adj</ta>
            <ta e="T1625" id="Seg_3259" s="T1624">v</ta>
            <ta e="T1626" id="Seg_3260" s="T1625">adv</ta>
            <ta e="T1627" id="Seg_3261" s="T1626">n</ta>
            <ta e="T1628" id="Seg_3262" s="T1627">n</ta>
            <ta e="T1629" id="Seg_3263" s="T1628">v</ta>
            <ta e="T1630" id="Seg_3264" s="T1629">v</ta>
            <ta e="T1631" id="Seg_3265" s="T1630">pers</ta>
            <ta e="T1632" id="Seg_3266" s="T1631">n</ta>
            <ta e="T1633" id="Seg_3267" s="T1632">v</ta>
            <ta e="T1634" id="Seg_3268" s="T1633">dempro</ta>
            <ta e="T1635" id="Seg_3269" s="T1634">v</ta>
            <ta e="T1636" id="Seg_3270" s="T1635">n</ta>
            <ta e="T1637" id="Seg_3271" s="T1636">v</ta>
            <ta e="T1638" id="Seg_3272" s="T1637">n</ta>
            <ta e="T1639" id="Seg_3273" s="T1638">n</ta>
            <ta e="T1640" id="Seg_3274" s="T1639">v</ta>
            <ta e="T1641" id="Seg_3275" s="T1640">n</ta>
            <ta e="T1642" id="Seg_3276" s="T1641">ptcl</ta>
            <ta e="T1643" id="Seg_3277" s="T1642">v</ta>
            <ta e="T1644" id="Seg_3278" s="T1643">adv</ta>
            <ta e="T1645" id="Seg_3279" s="T1644">ptcl</ta>
            <ta e="T1646" id="Seg_3280" s="T1645">aux</ta>
            <ta e="T1647" id="Seg_3281" s="T1646">v</ta>
            <ta e="T1648" id="Seg_3282" s="T1647">conj</ta>
            <ta e="T1649" id="Seg_3283" s="T1648">v</ta>
            <ta e="T1650" id="Seg_3284" s="T1649">n</ta>
            <ta e="T1651" id="Seg_3285" s="T1650">n</ta>
            <ta e="T1652" id="Seg_3286" s="T1651">dempro</ta>
            <ta e="T1653" id="Seg_3287" s="T1652">adv</ta>
            <ta e="T1654" id="Seg_3288" s="T1653">ptcl</ta>
            <ta e="T1655" id="Seg_3289" s="T1654">adv</ta>
            <ta e="T1656" id="Seg_3290" s="T1655">n</ta>
            <ta e="T1657" id="Seg_3291" s="T1656">v</ta>
            <ta e="T1658" id="Seg_3292" s="T1657">n</ta>
            <ta e="T1659" id="Seg_3293" s="T1658">v</ta>
            <ta e="T1660" id="Seg_3294" s="T1659">v</ta>
            <ta e="T1661" id="Seg_3295" s="T1660">ptcl</ta>
            <ta e="T1662" id="Seg_3296" s="T1661">n</ta>
            <ta e="T1663" id="Seg_3297" s="T1662">v</ta>
            <ta e="T1664" id="Seg_3298" s="T1663">v</ta>
            <ta e="T1665" id="Seg_3299" s="T1664">ptcl</ta>
            <ta e="T1666" id="Seg_3300" s="T1665">v</ta>
            <ta e="T1667" id="Seg_3301" s="T1666">que</ta>
            <ta e="T1668" id="Seg_3302" s="T1667">ptcl</ta>
            <ta e="T1669" id="Seg_3303" s="T1668">v</ta>
            <ta e="T1670" id="Seg_3304" s="T1669">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1405" id="Seg_3305" s="T1404">np.h:E</ta>
            <ta e="T1406" id="Seg_3306" s="T1405">np.h:E</ta>
            <ta e="T1407" id="Seg_3307" s="T1406">np.h:Poss</ta>
            <ta e="T1409" id="Seg_3308" s="T1408">np.h:E</ta>
            <ta e="T1411" id="Seg_3309" s="T1410">np.h:Poss</ta>
            <ta e="T1413" id="Seg_3310" s="T1412">np.h:E</ta>
            <ta e="T1415" id="Seg_3311" s="T1414">np.h:E</ta>
            <ta e="T1416" id="Seg_3312" s="T1415">np.h:Poss</ta>
            <ta e="T1417" id="Seg_3313" s="T1416">np.h:Th</ta>
            <ta e="T1420" id="Seg_3314" s="T1419">0.3.h:A</ta>
            <ta e="T1421" id="Seg_3315" s="T1420">0.3.h:A</ta>
            <ta e="T1423" id="Seg_3316" s="T1422">pro.h:Poss</ta>
            <ta e="T1424" id="Seg_3317" s="T1423">np.h:Th</ta>
            <ta e="T1426" id="Seg_3318" s="T1425">0.3.h:E</ta>
            <ta e="T1427" id="Seg_3319" s="T1426">np:Th</ta>
            <ta e="T1429" id="Seg_3320" s="T1428">0.3.h:A</ta>
            <ta e="T1430" id="Seg_3321" s="T1429">adv:Time</ta>
            <ta e="T1432" id="Seg_3322" s="T1431">np.h:R</ta>
            <ta e="T1433" id="Seg_3323" s="T1432">0.3.h:A</ta>
            <ta e="T1434" id="Seg_3324" s="T1433">0.2.h:A</ta>
            <ta e="T1435" id="Seg_3325" s="T1434">pro.h:Poss</ta>
            <ta e="T1436" id="Seg_3326" s="T1435">np.h:Th</ta>
            <ta e="T1439" id="Seg_3327" s="T1438">pro.h:A</ta>
            <ta e="T1440" id="Seg_3328" s="T1439">pro.h:B</ta>
            <ta e="T1441" id="Seg_3329" s="T1440">pro:Th</ta>
            <ta e="T1444" id="Seg_3330" s="T1443">pro.h:A</ta>
            <ta e="T1445" id="Seg_3331" s="T1444">np:Th</ta>
            <ta e="T1447" id="Seg_3332" s="T1446">np.h:Th</ta>
            <ta e="T1448" id="Seg_3333" s="T1447">0.3.h:A</ta>
            <ta e="T1449" id="Seg_3334" s="T1448">0.3.h:A</ta>
            <ta e="T1450" id="Seg_3335" s="T1449">np:G</ta>
            <ta e="T1451" id="Seg_3336" s="T1450">0.3.h:A</ta>
            <ta e="T1452" id="Seg_3337" s="T1451">np:Poss</ta>
            <ta e="T1453" id="Seg_3338" s="T1452">np:L</ta>
            <ta e="T1456" id="Seg_3339" s="T1455">np:Th</ta>
            <ta e="T1458" id="Seg_3340" s="T1457">pro.h:E</ta>
            <ta e="T1460" id="Seg_3341" s="T1459">0.3.h:E</ta>
            <ta e="T1461" id="Seg_3342" s="T1460">np:Th</ta>
            <ta e="T1465" id="Seg_3343" s="T1464">np:So</ta>
            <ta e="T1466" id="Seg_3344" s="T1465">np:G</ta>
            <ta e="T1467" id="Seg_3345" s="T1466">0.3:A</ta>
            <ta e="T1468" id="Seg_3346" s="T1467">0.3.h:A</ta>
            <ta e="T1469" id="Seg_3347" s="T1468">pro.h:R</ta>
            <ta e="T1470" id="Seg_3348" s="T1469">np.h:Th</ta>
            <ta e="T1473" id="Seg_3349" s="T1472">pro.h:Th</ta>
            <ta e="T1476" id="Seg_3350" s="T1475">np:Th</ta>
            <ta e="T1478" id="Seg_3351" s="T1477">0.3:Th</ta>
            <ta e="T1480" id="Seg_3352" s="T1479">pro.h:Th</ta>
            <ta e="T1488" id="Seg_3353" s="T1487">0.3.h:A</ta>
            <ta e="T1489" id="Seg_3354" s="T1488">adv:Time</ta>
            <ta e="T1491" id="Seg_3355" s="T1490">0.3.h:A</ta>
            <ta e="T1494" id="Seg_3356" s="T1493">pro.h:Poss</ta>
            <ta e="T1495" id="Seg_3357" s="T1494">np:P</ta>
            <ta e="T1499" id="Seg_3358" s="T1498">adv:Time</ta>
            <ta e="T1500" id="Seg_3359" s="T1499">pro.h:A</ta>
            <ta e="T1501" id="Seg_3360" s="T1500">pro.h:Th</ta>
            <ta e="T1502" id="Seg_3361" s="T1501">np:Ins</ta>
            <ta e="T1504" id="Seg_3362" s="T1503">np:Th</ta>
            <ta e="T1505" id="Seg_3363" s="T1504">0.3.h:A</ta>
            <ta e="T1506" id="Seg_3364" s="T1505">pro.h:P</ta>
            <ta e="T1508" id="Seg_3365" s="T1507">adv:Time</ta>
            <ta e="T1509" id="Seg_3366" s="T1508">np.h:A</ta>
            <ta e="T1511" id="Seg_3367" s="T1510">np:P</ta>
            <ta e="T1512" id="Seg_3368" s="T1511">0.2.h:A</ta>
            <ta e="T1513" id="Seg_3369" s="T1512">np.h:P</ta>
            <ta e="T1515" id="Seg_3370" s="T1514">0.2.h:A</ta>
            <ta e="T1516" id="Seg_3371" s="T1515">adv:Time</ta>
            <ta e="T1517" id="Seg_3372" s="T1516">np.h:A</ta>
            <ta e="T1518" id="Seg_3373" s="T1517">np:Poss</ta>
            <ta e="T1519" id="Seg_3374" s="T1518">np:L</ta>
            <ta e="T1522" id="Seg_3375" s="T1521">0.3.h:A</ta>
            <ta e="T1523" id="Seg_3376" s="T1522">np.h:Poss</ta>
            <ta e="T1524" id="Seg_3377" s="T1523">np.h:A</ta>
            <ta e="T1529" id="Seg_3378" s="T1528">np:Th</ta>
            <ta e="T1530" id="Seg_3379" s="T1529">0.3.h:A</ta>
            <ta e="T1531" id="Seg_3380" s="T1530">np:Th</ta>
            <ta e="T1533" id="Seg_3381" s="T1532">pro.h:A</ta>
            <ta e="T1536" id="Seg_3382" s="T1535">0.2.h:A</ta>
            <ta e="T1538" id="Seg_3383" s="T1537">np.h:Poss</ta>
            <ta e="T1539" id="Seg_3384" s="T1538">np.h:Th</ta>
            <ta e="T1540" id="Seg_3385" s="T1539">np:L</ta>
            <ta e="T1546" id="Seg_3386" s="T1545">np.h:A</ta>
            <ta e="T1548" id="Seg_3387" s="T1547">pro.h:R</ta>
            <ta e="T1551" id="Seg_3388" s="T1550">adv:Time</ta>
            <ta e="T1552" id="Seg_3389" s="T1551">np.h:A</ta>
            <ta e="T1554" id="Seg_3390" s="T1553">np:Th</ta>
            <ta e="T1556" id="Seg_3391" s="T1555">0.3.h:A</ta>
            <ta e="T1558" id="Seg_3392" s="T1557">np:Th</ta>
            <ta e="T1560" id="Seg_3393" s="T1559">np.h:A 0.3.h:Poss</ta>
            <ta e="T1561" id="Seg_3394" s="T1560">np:G</ta>
            <ta e="T1563" id="Seg_3395" s="T1562">adv:Time</ta>
            <ta e="T1565" id="Seg_3396" s="T1564">np.h:A</ta>
            <ta e="T1566" id="Seg_3397" s="T1565">0.2.h:A</ta>
            <ta e="T1567" id="Seg_3398" s="T1566">pro.h:Poss</ta>
            <ta e="T1568" id="Seg_3399" s="T1567">np.h:Th</ta>
            <ta e="T1570" id="Seg_3400" s="T1569">np.h:A</ta>
            <ta e="T1572" id="Seg_3401" s="T1571">np:Th</ta>
            <ta e="T1573" id="Seg_3402" s="T1572">0.2.h:A</ta>
            <ta e="T1574" id="Seg_3403" s="T1573">pro.h:A</ta>
            <ta e="T1575" id="Seg_3404" s="T1574">np.h:Th</ta>
            <ta e="T1576" id="Seg_3405" s="T1575">adv:L</ta>
            <ta e="T1581" id="Seg_3406" s="T1580">0.3.h:A</ta>
            <ta e="T1582" id="Seg_3407" s="T1581">adv:Time</ta>
            <ta e="T1584" id="Seg_3408" s="T1583">np:Th</ta>
            <ta e="T1585" id="Seg_3409" s="T1584">pro:G</ta>
            <ta e="T1587" id="Seg_3410" s="T1586">pro.h:A</ta>
            <ta e="T1590" id="Seg_3411" s="T1589">pro.h:Th</ta>
            <ta e="T1592" id="Seg_3412" s="T1591">0.1.h:E</ta>
            <ta e="T1595" id="Seg_3413" s="T1594">0.2.h:A</ta>
            <ta e="T1597" id="Seg_3414" s="T1596">pro:G</ta>
            <ta e="T1598" id="Seg_3415" s="T1597">pro.h:A</ta>
            <ta e="T1602" id="Seg_3416" s="T1601">pro.h:Th</ta>
            <ta e="T1606" id="Seg_3417" s="T1605">pro.h:R</ta>
            <ta e="T1607" id="Seg_3418" s="T1606">adv:Time</ta>
            <ta e="T1609" id="Seg_3419" s="T1608">0.3.h:A</ta>
            <ta e="T1610" id="Seg_3420" s="T1609">pro.h:A</ta>
            <ta e="T1615" id="Seg_3421" s="T1614">0.3.h:A</ta>
            <ta e="T1616" id="Seg_3422" s="T1615">pro.h:A</ta>
            <ta e="T1617" id="Seg_3423" s="T1616">pro.h:P</ta>
            <ta e="T1621" id="Seg_3424" s="T1620">0.3.h:P</ta>
            <ta e="T1623" id="Seg_3425" s="T1622">np:Th</ta>
            <ta e="T1625" id="Seg_3426" s="T1624">0.3.h:Th</ta>
            <ta e="T1626" id="Seg_3427" s="T1625">adv:Time</ta>
            <ta e="T1627" id="Seg_3428" s="T1626">n:Time</ta>
            <ta e="T1628" id="Seg_3429" s="T1627">np.h:A</ta>
            <ta e="T1630" id="Seg_3430" s="T1629">0.2.h:A</ta>
            <ta e="T1631" id="Seg_3431" s="T1630">pro.h:Poss</ta>
            <ta e="T1632" id="Seg_3432" s="T1631">np.h:Th</ta>
            <ta e="T1634" id="Seg_3433" s="T1633">pro.h:A</ta>
            <ta e="T1636" id="Seg_3434" s="T1635">np.h:A</ta>
            <ta e="T1638" id="Seg_3435" s="T1637">np.h:Poss</ta>
            <ta e="T1639" id="Seg_3436" s="T1638">np:Th</ta>
            <ta e="T1640" id="Seg_3437" s="T1639">0.3.h:A</ta>
            <ta e="T1641" id="Seg_3438" s="T1640">np:L</ta>
            <ta e="T1643" id="Seg_3439" s="T1642">0.3:P</ta>
            <ta e="T1644" id="Seg_3440" s="T1643">adv:Time</ta>
            <ta e="T1646" id="Seg_3441" s="T1645">0.2.h:A</ta>
            <ta e="T1649" id="Seg_3442" s="T1648">0.2.h:A</ta>
            <ta e="T1650" id="Seg_3443" s="T1649">np.h:Poss</ta>
            <ta e="T1651" id="Seg_3444" s="T1650">np:Th</ta>
            <ta e="T1652" id="Seg_3445" s="T1651">pro:Th</ta>
            <ta e="T1655" id="Seg_3446" s="T1654">adv:Time</ta>
            <ta e="T1656" id="Seg_3447" s="T1655">np.h:A</ta>
            <ta e="T1658" id="Seg_3448" s="T1657">np.h:A</ta>
            <ta e="T1660" id="Seg_3449" s="T1659">0.3.h:A 0.3:Th</ta>
            <ta e="T1662" id="Seg_3450" s="T1661">np:P 0.3.h:Poss</ta>
            <ta e="T1664" id="Seg_3451" s="T1663">0.3:A</ta>
            <ta e="T1667" id="Seg_3452" s="T1666">pro:Th</ta>
            <ta e="T1669" id="Seg_3453" s="T1668">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1404" id="Seg_3454" s="T1403">v:pred</ta>
            <ta e="T1405" id="Seg_3455" s="T1404">np.h:S</ta>
            <ta e="T1406" id="Seg_3456" s="T1405">np.h:S</ta>
            <ta e="T1408" id="Seg_3457" s="T1407">v:pred</ta>
            <ta e="T1409" id="Seg_3458" s="T1408">np.h:S</ta>
            <ta e="T1412" id="Seg_3459" s="T1411">v:pred</ta>
            <ta e="T1413" id="Seg_3460" s="T1412">np.h:S</ta>
            <ta e="T1415" id="Seg_3461" s="T1414">np.h:S</ta>
            <ta e="T1417" id="Seg_3462" s="T1416">np.h:O</ta>
            <ta e="T1418" id="Seg_3463" s="T1417">ptcl.neg</ta>
            <ta e="T1419" id="Seg_3464" s="T1418">v:pred</ta>
            <ta e="T1420" id="Seg_3465" s="T1419">v:pred 0.3.h:S</ta>
            <ta e="T1421" id="Seg_3466" s="T1420">v:pred 0.3.h:S</ta>
            <ta e="T1424" id="Seg_3467" s="T1423">np.h:O</ta>
            <ta e="T1426" id="Seg_3468" s="T1425">v:pred 0.3.h:S</ta>
            <ta e="T1427" id="Seg_3469" s="T1426">np:O</ta>
            <ta e="T1429" id="Seg_3470" s="T1428">v:pred 0.3.h:S</ta>
            <ta e="T1433" id="Seg_3471" s="T1432">v:pred 0.3.h:S</ta>
            <ta e="T1434" id="Seg_3472" s="T1433">v:pred 0.2.h:S</ta>
            <ta e="T1436" id="Seg_3473" s="T1435">np.h:O</ta>
            <ta e="T1439" id="Seg_3474" s="T1438">pro.h:S</ta>
            <ta e="T1441" id="Seg_3475" s="T1440">pro:O</ta>
            <ta e="T1442" id="Seg_3476" s="T1441">ptcl.neg</ta>
            <ta e="T1443" id="Seg_3477" s="T1442">v:pred</ta>
            <ta e="T1444" id="Seg_3478" s="T1443">pro.h:S</ta>
            <ta e="T1445" id="Seg_3479" s="T1444">np:O</ta>
            <ta e="T1446" id="Seg_3480" s="T1445">v:pred</ta>
            <ta e="T1447" id="Seg_3481" s="T1446">np.h:O</ta>
            <ta e="T1448" id="Seg_3482" s="T1447">v:pred 0.3.h:S</ta>
            <ta e="T1449" id="Seg_3483" s="T1448">v:pred 0.3.h:S</ta>
            <ta e="T1451" id="Seg_3484" s="T1450">v:pred 0.3.h:S</ta>
            <ta e="T1456" id="Seg_3485" s="T1455">np:S</ta>
            <ta e="T1457" id="Seg_3486" s="T1456">adj:pred</ta>
            <ta e="T1458" id="Seg_3487" s="T1457">pro.h:S</ta>
            <ta e="T1459" id="Seg_3488" s="T1458">v:pred</ta>
            <ta e="T1460" id="Seg_3489" s="T1459">v:pred 0.3.h:S</ta>
            <ta e="T1461" id="Seg_3490" s="T1460">np:S</ta>
            <ta e="T1462" id="Seg_3491" s="T1461">v:pred</ta>
            <ta e="T1467" id="Seg_3492" s="T1466">v:pred 0.3:S</ta>
            <ta e="T1468" id="Seg_3493" s="T1467">v:pred 0.3.h:S</ta>
            <ta e="T1470" id="Seg_3494" s="T1469">np.h:S</ta>
            <ta e="T1471" id="Seg_3495" s="T1470">adj:pred</ta>
            <ta e="T1472" id="Seg_3496" s="T1471">ptcl.neg</ta>
            <ta e="T1475" id="Seg_3497" s="T1474">ptcl.neg</ta>
            <ta e="T1476" id="Seg_3498" s="T1475">np:S</ta>
            <ta e="T1477" id="Seg_3499" s="T1476">adj:pred</ta>
            <ta e="T1478" id="Seg_3500" s="T1477">v:pred 0.3:S</ta>
            <ta e="T1479" id="Seg_3501" s="T1478">adj:pred</ta>
            <ta e="T1480" id="Seg_3502" s="T1479">pro.h:S</ta>
            <ta e="T1481" id="Seg_3503" s="T1480">ptcl.neg</ta>
            <ta e="T1482" id="Seg_3504" s="T1481">adj:pred</ta>
            <ta e="T1483" id="Seg_3505" s="T1482">adj:pred</ta>
            <ta e="T1487" id="Seg_3506" s="T1486">ptcl.neg</ta>
            <ta e="T1488" id="Seg_3507" s="T1487">v:pred 0.3.h:S</ta>
            <ta e="T1491" id="Seg_3508" s="T1490">v:pred 0.3.h:S</ta>
            <ta e="T1492" id="Seg_3509" s="T1491">ptcl.neg</ta>
            <ta e="T1493" id="Seg_3510" s="T1492">adj:pred</ta>
            <ta e="T1496" id="Seg_3511" s="T1495">pro:S</ta>
            <ta e="T1498" id="Seg_3512" s="T1497">v:pred</ta>
            <ta e="T1500" id="Seg_3513" s="T1499">pro.h:S</ta>
            <ta e="T1501" id="Seg_3514" s="T1500">pro.h:O</ta>
            <ta e="T1503" id="Seg_3515" s="T1502">v:pred</ta>
            <ta e="T1505" id="Seg_3516" s="T1504">v:pred 0.3.h:S</ta>
            <ta e="T1506" id="Seg_3517" s="T1505">pro.h:S</ta>
            <ta e="T1507" id="Seg_3518" s="T1506">v:pred</ta>
            <ta e="T1509" id="Seg_3519" s="T1508">np.h:S</ta>
            <ta e="T1510" id="Seg_3520" s="T1509">v:pred</ta>
            <ta e="T1511" id="Seg_3521" s="T1510">np:O</ta>
            <ta e="T1512" id="Seg_3522" s="T1511">v:pred 0.2.h:S</ta>
            <ta e="T1513" id="Seg_3523" s="T1512">np.h:S</ta>
            <ta e="T1514" id="Seg_3524" s="T1513">v:pred</ta>
            <ta e="T1515" id="Seg_3525" s="T1514">v:pred 0.2.h:S</ta>
            <ta e="T1517" id="Seg_3526" s="T1516">np.h:S</ta>
            <ta e="T1520" id="Seg_3527" s="T1519">v:pred</ta>
            <ta e="T1522" id="Seg_3528" s="T1521">v:pred 0.3.h:S</ta>
            <ta e="T1524" id="Seg_3529" s="T1523">np.h:S</ta>
            <ta e="T1525" id="Seg_3530" s="T1524">v:pred</ta>
            <ta e="T1529" id="Seg_3531" s="T1528">np:O</ta>
            <ta e="T1530" id="Seg_3532" s="T1529">v:pred 0.3.h:S</ta>
            <ta e="T1531" id="Seg_3533" s="T1530">np:O</ta>
            <ta e="T1533" id="Seg_3534" s="T1532">pro.h:S</ta>
            <ta e="T1534" id="Seg_3535" s="T1533">v:pred</ta>
            <ta e="T1536" id="Seg_3536" s="T1535">v:pred 0.2.h:S</ta>
            <ta e="T1539" id="Seg_3537" s="T1538">np.h:S</ta>
            <ta e="T1540" id="Seg_3538" s="T1539">np:O</ta>
            <ta e="T1541" id="Seg_3539" s="T1540">v:pred</ta>
            <ta e="T1546" id="Seg_3540" s="T1545">np.h:S</ta>
            <ta e="T1550" id="Seg_3541" s="T1549">v:pred</ta>
            <ta e="T1552" id="Seg_3542" s="T1551">np.h:S</ta>
            <ta e="T1553" id="Seg_3543" s="T1552">v:pred</ta>
            <ta e="T1554" id="Seg_3544" s="T1553">np:O</ta>
            <ta e="T1556" id="Seg_3545" s="T1555">v:pred 0.3.h:S</ta>
            <ta e="T1558" id="Seg_3546" s="T1557">np:O</ta>
            <ta e="T1560" id="Seg_3547" s="T1559">np.h:S</ta>
            <ta e="T1562" id="Seg_3548" s="T1561">v:pred</ta>
            <ta e="T1565" id="Seg_3549" s="T1564">np.h:S</ta>
            <ta e="T1566" id="Seg_3550" s="T1565">v:pred 0.2.h:S</ta>
            <ta e="T1568" id="Seg_3551" s="T1567">np.h:O</ta>
            <ta e="T1570" id="Seg_3552" s="T1569">np.h:S</ta>
            <ta e="T1571" id="Seg_3553" s="T1570">v:pred</ta>
            <ta e="T1572" id="Seg_3554" s="T1571">np:O</ta>
            <ta e="T1573" id="Seg_3555" s="T1572">v:pred 0.3.h:S</ta>
            <ta e="T1574" id="Seg_3556" s="T1573">pro.h:S</ta>
            <ta e="T1575" id="Seg_3557" s="T1574">np.h:O</ta>
            <ta e="T1578" id="Seg_3558" s="T1577">v:pred</ta>
            <ta e="T1581" id="Seg_3559" s="T1580">v:pred 0.3.h:S</ta>
            <ta e="T1584" id="Seg_3560" s="T1583">np:S</ta>
            <ta e="T1586" id="Seg_3561" s="T1585">v:pred</ta>
            <ta e="T1587" id="Seg_3562" s="T1586">pro.h:S</ta>
            <ta e="T1589" id="Seg_3563" s="T1588">adj:pred</ta>
            <ta e="T1590" id="Seg_3564" s="T1589">pro.h:S</ta>
            <ta e="T1591" id="Seg_3565" s="T1590">adj:pred</ta>
            <ta e="T1592" id="Seg_3566" s="T1591">v:pred 0.1.h:S</ta>
            <ta e="T1595" id="Seg_3567" s="T1594">v:pred 0.2.h:S</ta>
            <ta e="T1598" id="Seg_3568" s="T1597">pro.h:S</ta>
            <ta e="T1600" id="Seg_3569" s="T1599">v:pred</ta>
            <ta e="T1601" id="Seg_3570" s="T1600">adj:pred</ta>
            <ta e="T1602" id="Seg_3571" s="T1601">pro.h:S</ta>
            <ta e="T1603" id="Seg_3572" s="T1602">adj:pred</ta>
            <ta e="T1604" id="Seg_3573" s="T1603">ptcl:pred</ta>
            <ta e="T1606" id="Seg_3574" s="T1605">pro.h:O</ta>
            <ta e="T1609" id="Seg_3575" s="T1608">v:pred 0.3.h:S</ta>
            <ta e="T1610" id="Seg_3576" s="T1609">pro.h:S</ta>
            <ta e="T1612" id="Seg_3577" s="T1611">v:pred</ta>
            <ta e="T1615" id="Seg_3578" s="T1614">v:pred 0.3.h:S</ta>
            <ta e="T1616" id="Seg_3579" s="T1615">pro.h:S</ta>
            <ta e="T1617" id="Seg_3580" s="T1616">pro.h:O</ta>
            <ta e="T1619" id="Seg_3581" s="T1618">v:pred</ta>
            <ta e="T1621" id="Seg_3582" s="T1620">v:pred 0.3.h:S</ta>
            <ta e="T1623" id="Seg_3583" s="T1622">n:pred</ta>
            <ta e="T1625" id="Seg_3584" s="T1624">cop 0.3.h:S</ta>
            <ta e="T1628" id="Seg_3585" s="T1627">np.h:S</ta>
            <ta e="T1629" id="Seg_3586" s="T1628">v:pred</ta>
            <ta e="T1630" id="Seg_3587" s="T1629">v:pred 0.2.h:S</ta>
            <ta e="T1632" id="Seg_3588" s="T1631">np.h:O</ta>
            <ta e="T1633" id="Seg_3589" s="T1632">conv:pred</ta>
            <ta e="T1634" id="Seg_3590" s="T1633">pro.h:S</ta>
            <ta e="T1635" id="Seg_3591" s="T1634">v:pred</ta>
            <ta e="T1636" id="Seg_3592" s="T1635">np.h:S</ta>
            <ta e="T1637" id="Seg_3593" s="T1636">v:pred</ta>
            <ta e="T1639" id="Seg_3594" s="T1638">np:O</ta>
            <ta e="T1640" id="Seg_3595" s="T1639">v:pred 0.3.h:S</ta>
            <ta e="T1643" id="Seg_3596" s="T1642">v:pred 0.3:S</ta>
            <ta e="T1646" id="Seg_3597" s="T1645">v:pred 0.2.h:S</ta>
            <ta e="T1649" id="Seg_3598" s="T1648">v:pred 0.2.h:S</ta>
            <ta e="T1652" id="Seg_3599" s="T1651">pro:S</ta>
            <ta e="T1656" id="Seg_3600" s="T1655">np.h:S</ta>
            <ta e="T1657" id="Seg_3601" s="T1656">v:pred</ta>
            <ta e="T1658" id="Seg_3602" s="T1657">np.h:S</ta>
            <ta e="T1659" id="Seg_3603" s="T1658">v:pred</ta>
            <ta e="T1660" id="Seg_3604" s="T1659">v:pred 0.3.h:S 0.3:O</ta>
            <ta e="T1662" id="Seg_3605" s="T1661">np:S</ta>
            <ta e="T1663" id="Seg_3606" s="T1662">v:pred </ta>
            <ta e="T1664" id="Seg_3607" s="T1663">v:pred 0.3:S</ta>
            <ta e="T1665" id="Seg_3608" s="T1664">ptcl:pred</ta>
            <ta e="T1667" id="Seg_3609" s="T1666">pro:O</ta>
            <ta e="T1668" id="Seg_3610" s="T1667">ptcl.neg</ta>
            <ta e="T1669" id="Seg_3611" s="T1668">v:pred 0.2.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1410" id="Seg_3612" s="T1409">RUS:gram</ta>
            <ta e="T1422" id="Seg_3613" s="T1421">RUS:gram</ta>
            <ta e="T1428" id="Seg_3614" s="T1427">TURK:disc</ta>
            <ta e="T1441" id="Seg_3615" s="T1440">TURK:gram(INDEF)</ta>
            <ta e="T1472" id="Seg_3616" s="T1471">TURK:disc</ta>
            <ta e="T1477" id="Seg_3617" s="T1476">RUS:mod</ta>
            <ta e="T1484" id="Seg_3618" s="T1483">RUS:gram</ta>
            <ta e="T1486" id="Seg_3619" s="T1485">%TURK:core</ta>
            <ta e="T1497" id="Seg_3620" s="T1496">TURK:disc</ta>
            <ta e="T1502" id="Seg_3621" s="T1501">RUS:cult</ta>
            <ta e="T1504" id="Seg_3622" s="T1503">RUS:cult</ta>
            <ta e="T1511" id="Seg_3623" s="T1510">RUS:cult</ta>
            <ta e="T1518" id="Seg_3624" s="T1517">RUS:cult</ta>
            <ta e="T1528" id="Seg_3625" s="T1527">TURK:disc</ta>
            <ta e="T1542" id="Seg_3626" s="T1541">RUS:gram</ta>
            <ta e="T1554" id="Seg_3627" s="T1553">RUS:cult</ta>
            <ta e="T1557" id="Seg_3628" s="T1556">TURK:disc</ta>
            <ta e="T1561" id="Seg_3629" s="T1560">TAT:cult</ta>
            <ta e="T1577" id="Seg_3630" s="T1576">RUS:disc</ta>
            <ta e="T1579" id="Seg_3631" s="T1578">RUS:gram</ta>
            <ta e="T1588" id="Seg_3632" s="T1587">TURK:disc</ta>
            <ta e="T1604" id="Seg_3633" s="T1603">RUS:gram</ta>
            <ta e="T1608" id="Seg_3634" s="T1607">RUS:mod</ta>
            <ta e="T1613" id="Seg_3635" s="T1612">RUS:core</ta>
            <ta e="T1618" id="Seg_3636" s="T1617">TURK:disc</ta>
            <ta e="T1620" id="Seg_3637" s="T1619">RUS:gram</ta>
            <ta e="T1622" id="Seg_3638" s="T1621">TURK:disc</ta>
            <ta e="T1642" id="Seg_3639" s="T1641">TURK:disc</ta>
            <ta e="T1648" id="Seg_3640" s="T1647">RUS:gram</ta>
            <ta e="T1661" id="Seg_3641" s="T1660">TURK:disc</ta>
            <ta e="T1665" id="Seg_3642" s="T1664">RUS:gram</ta>
            <ta e="T1667" id="Seg_3643" s="T1666">TURK:gram(INDEF)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1406" id="Seg_3644" s="T1403">Жили муж с женой.</ta>
            <ta e="T1409" id="Seg_3645" s="T1406">У жены была дочь.</ta>
            <ta e="T1413" id="Seg_3646" s="T1409">И у мужа была дочь.</ta>
            <ta e="T1419" id="Seg_3647" s="T1413">Жена не любила дочь мужа.</ta>
            <ta e="T1421" id="Seg_3648" s="T1419">Она била ее, ругала.</ta>
            <ta e="T1426" id="Seg_3649" s="T1421">А свою дочь всегда жалеет.</ta>
            <ta e="T1429" id="Seg_3650" s="T1426">Всегда гладит её по голове.</ta>
            <ta e="T1433" id="Seg_3651" s="T1429">[Однажды] она говорит мужу:</ta>
            <ta e="T1436" id="Seg_3652" s="T1433">"Забери свою дочь.</ta>
            <ta e="T1443" id="Seg_3653" s="T1436">Она для меня ничего не делает".</ta>
            <ta e="T1448" id="Seg_3654" s="T1443">Он лошадь запряг, дочь посадил.</ta>
            <ta e="T1450" id="Seg_3655" s="T1448">Увёз её в лес.</ta>
            <ta e="T1457" id="Seg_3656" s="T1450">Оставил её около дерева, снега много.</ta>
            <ta e="T1460" id="Seg_3657" s="T1457">Она сидит, замёрзла.</ta>
            <ta e="T1462" id="Seg_3658" s="T1460">Мороз идёт.</ta>
            <ta e="T1467" id="Seg_3659" s="T1462">Прыгает с дерева на дерево.</ta>
            <ta e="T1469" id="Seg_3660" s="T1467">Говорит ей:</ta>
            <ta e="T1471" id="Seg_3661" s="T1469">"Тебе холодно!"</ta>
            <ta e="T1477" id="Seg_3662" s="T1471">"Нет, мне ещё не холодно".</ta>
            <ta e="T1478" id="Seg_3663" s="T1477">[Мороз] идёт.</ta>
            <ta e="T1480" id="Seg_3664" s="T1478">"Холодно тебе?"</ta>
            <ta e="T1483" id="Seg_3665" s="T1480">"Не холодно, тепло".</ta>
            <ta e="T1488" id="Seg_3666" s="T1483">А сама говорить не может.</ta>
            <ta e="T1491" id="Seg_3667" s="T1488">[Тот] опять спрашивает.</ta>
            <ta e="T1493" id="Seg_3668" s="T1491">"Не холодно?"</ta>
            <ta e="T1498" id="Seg_3669" s="T1493">У нее язык весь замёрз.</ta>
            <ta e="T1503" id="Seg_3670" s="T1498">Тогда он (укрыл?) её одеялом.</ta>
            <ta e="T1505" id="Seg_3671" s="T1503">Укрыл пальто.</ta>
            <ta e="T1507" id="Seg_3672" s="T1505">Она согрелась.</ta>
            <ta e="T1511" id="Seg_3673" s="T1507">А женщина печёт блины.</ta>
            <ta e="T1515" id="Seg_3674" s="T1511">[Говорит мужу]: "Иди, девушка умерла, привези её!"</ta>
            <ta e="T1520" id="Seg_3675" s="T1515">А собака сидит под столом.</ta>
            <ta e="T1522" id="Seg_3676" s="T1520">И говорит:</ta>
            <ta e="T1525" id="Seg_3677" s="T1522">"Дочь мужчины идет.</ta>
            <ta e="T1532" id="Seg_3678" s="T1525">Много денег несёт, много одежды".</ta>
            <ta e="T1537" id="Seg_3679" s="T1532">[Жена] говорит: "Не говори так!</ta>
            <ta e="T1541" id="Seg_3680" s="T1537">Дочь мужчины в мешке несут".</ta>
            <ta e="T1550" id="Seg_3681" s="T1541">А собака все так же говорит.</ta>
            <ta e="T1553" id="Seg_3682" s="T1550">Муж пришёл.</ta>
            <ta e="T1556" id="Seg_3683" s="T1553">Корзину денег принес.</ta>
            <ta e="T1559" id="Seg_3684" s="T1556">Одежды много.</ta>
            <ta e="T1562" id="Seg_3685" s="T1559">Его дочь в дом вошла.</ta>
            <ta e="T1565" id="Seg_3686" s="T1562">Тогда женщина [говорит]:</ta>
            <ta e="T1568" id="Seg_3687" s="T1565">"Увези мою дочь!"</ta>
            <ta e="T1572" id="Seg_3688" s="T1568">Муж лошадь запряг.</ta>
            <ta e="T1573" id="Seg_3689" s="T1572">Увёз [её].</ta>
            <ta e="T1581" id="Seg_3690" s="T1573">Её дочь там же оставил и сам вернулся [домой].</ta>
            <ta e="T1586" id="Seg_3691" s="T1581">Опять мороз к ней пришел.</ta>
            <ta e="T1590" id="Seg_3692" s="T1586">[Спрашивает]: "Холодно тебе?"</ta>
            <ta e="T1597" id="Seg_3693" s="T1590">"Мне холодно, я замерзаю, не подходи ко мне!"</ta>
            <ta e="T1600" id="Seg_3694" s="T1597">Он опять идет.</ta>
            <ta e="T1602" id="Seg_3695" s="T1600">"Холодно тебе?"</ta>
            <ta e="T1603" id="Seg_3696" s="T1602">"Холодно!"</ta>
            <ta e="T1606" id="Seg_3697" s="T1603">Стала его ругать.</ta>
            <ta e="T1609" id="Seg_3698" s="T1606">Тот опять спросил.</ta>
            <ta e="T1615" id="Seg_3699" s="T1609">Та опять на него заругалась по-всякому.</ta>
            <ta e="T1619" id="Seg_3700" s="T1615">Он заморозил её.</ta>
            <ta e="T1625" id="Seg_3701" s="T1619">И она умерла, стала как дерево.</ta>
            <ta e="T1629" id="Seg_3702" s="T1625">Утром мужчина проснулся.</ta>
            <ta e="T1633" id="Seg_3703" s="T1629">[Жена говорит:] "Иди привези мою дочь!"</ta>
            <ta e="T1635" id="Seg_3704" s="T1633">Он поехал.</ta>
            <ta e="T1642" id="Seg_3705" s="T1635">Собака говорит: "Он женщины дочь в мешке несет.</ta>
            <ta e="T1643" id="Seg_3706" s="T1642">Она умерла".</ta>
            <ta e="T1647" id="Seg_3707" s="T1643">[Женщина говорит]: "Не говори так!</ta>
            <ta e="T1654" id="Seg_3708" s="T1647">Скажи: женщины дочь так же, как та [= как дочь мужчины]".</ta>
            <ta e="T1657" id="Seg_3709" s="T1654">Муж приехал.</ta>
            <ta e="T1659" id="Seg_3710" s="T1657">Жена побежала [встречать дочь].</ta>
            <ta e="T1661" id="Seg_3711" s="T1659">Смотрит.</ta>
            <ta e="T1664" id="Seg_3712" s="T1661">Её дочь мертвая лежит.</ta>
            <ta e="T1666" id="Seg_3713" s="T1664">Стала плакать.</ta>
            <ta e="T1669" id="Seg_3714" s="T1666">Ничего не поделаешь.</ta>
            <ta e="T1670" id="Seg_3715" s="T1669">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1406" id="Seg_3716" s="T1403">There lived a woman, a man.</ta>
            <ta e="T1409" id="Seg_3717" s="T1406">The woman had a daughter.</ta>
            <ta e="T1413" id="Seg_3718" s="T1409">And the man had a daughter.</ta>
            <ta e="T1419" id="Seg_3719" s="T1413">The woman did not love the man's daughter.</ta>
            <ta e="T1421" id="Seg_3720" s="T1419">She beat [her], scolded.</ta>
            <ta e="T1426" id="Seg_3721" s="T1421">But she always cared for her own daughter.</ta>
            <ta e="T1429" id="Seg_3722" s="T1426">She is always anointing (=caressing?) her head.</ta>
            <ta e="T1433" id="Seg_3723" s="T1429">Then she says to the man:</ta>
            <ta e="T1436" id="Seg_3724" s="T1433">"Take your daughter away.</ta>
            <ta e="T1443" id="Seg_3725" s="T1436">She does not do anything for me."</ta>
            <ta e="T1448" id="Seg_3726" s="T1443">He harnessed the horse, seated the daughter.</ta>
            <ta e="T1450" id="Seg_3727" s="T1448">Took [her] into the forest.</ta>
            <ta e="T1457" id="Seg_3728" s="T1450">He left [her] by the side of a tree, there is much snow.</ta>
            <ta e="T1460" id="Seg_3729" s="T1457">She is sitting, freezing.</ta>
            <ta e="T1462" id="Seg_3730" s="T1460">The frost comes.</ta>
            <ta e="T1467" id="Seg_3731" s="T1462">Jumps from tree to tree.</ta>
            <ta e="T1469" id="Seg_3732" s="T1467">He says to her:</ta>
            <ta e="T1471" id="Seg_3733" s="T1469">"You are cold!"</ta>
            <ta e="T1477" id="Seg_3734" s="T1471">"No, I am not cold yet."</ta>
            <ta e="T1478" id="Seg_3735" s="T1477">It comes.</ta>
            <ta e="T1480" id="Seg_3736" s="T1478">"Are you cold?"</ta>
            <ta e="T1483" id="Seg_3737" s="T1480">"Not cold, warm."</ta>
            <ta e="T1488" id="Seg_3738" s="T1483">But herself she cannot speak.</ta>
            <ta e="T1491" id="Seg_3739" s="T1488">Then again he is asking.</ta>
            <ta e="T1493" id="Seg_3740" s="T1491">"Not cold?"</ta>
            <ta e="T1498" id="Seg_3741" s="T1493">Her tongue, it has all frozen.</ta>
            <ta e="T1503" id="Seg_3742" s="T1498">Then he (dressed?) her with a blanket.</ta>
            <ta e="T1505" id="Seg_3743" s="T1503">Put on a coat.</ta>
            <ta e="T1507" id="Seg_3744" s="T1505">She got warm.</ta>
            <ta e="T1511" id="Seg_3745" s="T1507">Then the woman is baking pancakes.</ta>
            <ta e="T1515" id="Seg_3746" s="T1511">[She says to the husband:] "Go, the girl died, bring her!"</ta>
            <ta e="T1520" id="Seg_3747" s="T1515">Then the dog is sitting under the table.</ta>
            <ta e="T1522" id="Seg_3748" s="T1520">[The dog] is saying:</ta>
            <ta e="T1525" id="Seg_3749" s="T1522">"The man's daughter comes.</ta>
            <ta e="T1532" id="Seg_3750" s="T1525">She is bringing a lot of money, [she has] a lot of clothes."</ta>
            <ta e="T1537" id="Seg_3751" s="T1532">She says: "Don’t say so!</ta>
            <ta e="T1541" id="Seg_3752" s="T1537">The man's daughter is carried in a bag."</ta>
            <ta e="T1550" id="Seg_3753" s="T1541">But the dog is still saying so.</ta>
            <ta e="T1553" id="Seg_3754" s="T1550">Then the man came.</ta>
            <ta e="T1556" id="Seg_3755" s="T1553">Brought a basket of money.</ta>
            <ta e="T1559" id="Seg_3756" s="T1556">A lot of clothes.</ta>
            <ta e="T1562" id="Seg_3757" s="T1559">His daughter came into the house.</ta>
            <ta e="T1565" id="Seg_3758" s="T1562">Then the woman [says him]:</ta>
            <ta e="T1568" id="Seg_3759" s="T1565">"Take my daughter!"</ta>
            <ta e="T1572" id="Seg_3760" s="T1568">The man harnessed the horse.</ta>
            <ta e="T1573" id="Seg_3761" s="T1572">Took [her].</ta>
            <ta e="T1581" id="Seg_3762" s="T1573">He left her daughter there and he himself came [home].</ta>
            <ta e="T1586" id="Seg_3763" s="T1581">Then again the frost comes to her.</ta>
            <ta e="T1590" id="Seg_3764" s="T1586">He [Morozko asks]: "Are you cold?"</ta>
            <ta e="T1597" id="Seg_3765" s="T1590">"I am cold, I am freezing, don't come to me!"</ta>
            <ta e="T1600" id="Seg_3766" s="T1597">He comes again.</ta>
            <ta e="T1602" id="Seg_3767" s="T1600">"Are you cold?"</ta>
            <ta e="T1603" id="Seg_3768" s="T1602">"Cold!"</ta>
            <ta e="T1606" id="Seg_3769" s="T1603">She started to scold him.</ta>
            <ta e="T1609" id="Seg_3770" s="T1606">Then he asked [once] more.</ta>
            <ta e="T1615" id="Seg_3771" s="T1609">She sweared [at him] again with all kinds [of words], sweared with dirty words.</ta>
            <ta e="T1619" id="Seg_3772" s="T1615">He froze her up.</ta>
            <ta e="T1625" id="Seg_3773" s="T1619">And she died, she became like a tree.</ta>
            <ta e="T1629" id="Seg_3774" s="T1625">Then in the morning the man got up.</ta>
            <ta e="T1633" id="Seg_3775" s="T1629">"Go bring my daughter!"</ta>
            <ta e="T1635" id="Seg_3776" s="T1633">He went.</ta>
            <ta e="T1642" id="Seg_3777" s="T1635">The dog says: "He is bringing the woman’s daughter in a bag.</ta>
            <ta e="T1643" id="Seg_3778" s="T1642">She died."</ta>
            <ta e="T1647" id="Seg_3779" s="T1643">Then [she says]: "Don’t say so!</ta>
            <ta e="T1654" id="Seg_3780" s="T1647">But say, the woman’s daughter; she is still like this [= like the man's daughter]."</ta>
            <ta e="T1657" id="Seg_3781" s="T1654">Then the man comes.</ta>
            <ta e="T1659" id="Seg_3782" s="T1657">The woman ran [to meet her daughter].</ta>
            <ta e="T1661" id="Seg_3783" s="T1659">She sees.</ta>
            <ta e="T1664" id="Seg_3784" s="T1661">Her daughter died, she is lying.</ta>
            <ta e="T1666" id="Seg_3785" s="T1664">She started to cry.</ta>
            <ta e="T1669" id="Seg_3786" s="T1666">You [cannot] do anything.</ta>
            <ta e="T1670" id="Seg_3787" s="T1669">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1406" id="Seg_3788" s="T1403">Es lebte eine Frau, ein Mann.</ta>
            <ta e="T1409" id="Seg_3789" s="T1406">Die Frau hatte eine Tochter.</ta>
            <ta e="T1413" id="Seg_3790" s="T1409">Und der Mann hatte eine Tochter.</ta>
            <ta e="T1419" id="Seg_3791" s="T1413">Die Frau kümmerte sich nicht um die Tochter des Mannes.</ta>
            <ta e="T1421" id="Seg_3792" s="T1419">Sie schlug [sie], schimpfte.</ta>
            <ta e="T1426" id="Seg_3793" s="T1421">Aber sie kümmerte sich immer um die eigene Tochter.</ta>
            <ta e="T1429" id="Seg_3794" s="T1426">Sie salbt (=streichelt?) immer am Kopf.</ta>
            <ta e="T1433" id="Seg_3795" s="T1429">Dann sagt sie zum Mann:</ta>
            <ta e="T1436" id="Seg_3796" s="T1433">„Nimm deine Tochter.</ta>
            <ta e="T1443" id="Seg_3797" s="T1436">Sie macht nichts für mich.“</ta>
            <ta e="T1448" id="Seg_3798" s="T1443">Er sattelte das Pferd, setzte seine Tochter.</ta>
            <ta e="T1450" id="Seg_3799" s="T1448">Nahm [sie] auf die Taiga.</ta>
            <ta e="T1457" id="Seg_3800" s="T1450">Er ließ [sie] an der Seites eines Baumes, der Schnee ist groß.</ta>
            <ta e="T1460" id="Seg_3801" s="T1457">Sie sitzt, friert.</ta>
            <ta e="T1462" id="Seg_3802" s="T1460">Der Frost kommt.</ta>
            <ta e="T1467" id="Seg_3803" s="T1462">Springt von Baum zu Baum.</ta>
            <ta e="T1469" id="Seg_3804" s="T1467">Er sagt zu ihr:</ta>
            <ta e="T1471" id="Seg_3805" s="T1469">„Dir ist kalt!“</ta>
            <ta e="T1477" id="Seg_3806" s="T1471">„Nein, mir ist noch nicht kalt.“</ta>
            <ta e="T1478" id="Seg_3807" s="T1477">Es kommt.</ta>
            <ta e="T1480" id="Seg_3808" s="T1478">„Ist dir kalt?“</ta>
            <ta e="T1483" id="Seg_3809" s="T1480">„Nicht kalt, warm.“</ta>
            <ta e="T1488" id="Seg_3810" s="T1483">Aber selber kann sie nicht sprechen.</ta>
            <ta e="T1491" id="Seg_3811" s="T1488">Dann fragt er wieder.</ta>
            <ta e="T1493" id="Seg_3812" s="T1491">„Nicht kalt.“</ta>
            <ta e="T1498" id="Seg_3813" s="T1493">Ihre Zunge, sie ist durchgefroren.</ta>
            <ta e="T1503" id="Seg_3814" s="T1498">Dann (kleidet?) er sie mit einer Decke.</ta>
            <ta e="T1505" id="Seg_3815" s="T1503">Zieh den Mantel an.</ta>
            <ta e="T1507" id="Seg_3816" s="T1505">Ihr wurde warm.</ta>
            <ta e="T1511" id="Seg_3817" s="T1507">Dann backt die Frau Pfannkuchen.</ta>
            <ta e="T1515" id="Seg_3818" s="T1511">[Sie sagt zum Ehemann:] „Geh, das Mädchen ist gestorben, bring sie!“</ta>
            <ta e="T1520" id="Seg_3819" s="T1515">Dann sitzt der Hund unter dem Tisch.</ta>
            <ta e="T1522" id="Seg_3820" s="T1520">[Der Hund] sagt:</ta>
            <ta e="T1525" id="Seg_3821" s="T1522">„Die Tochter des Mannes kommt.</ta>
            <ta e="T1532" id="Seg_3822" s="T1525">Sie bringt viel Geld, [sie hat] viele Kleider.“</ta>
            <ta e="T1537" id="Seg_3823" s="T1532">Sie sagt: „Sag nicht sowas!</ta>
            <ta e="T1541" id="Seg_3824" s="T1537">[Der Mann] bringt seine Tochter in eine Tasche.“</ta>
            <ta e="T1550" id="Seg_3825" s="T1541">Aber der Hund sagt immer noch dasselbe.</ta>
            <ta e="T1553" id="Seg_3826" s="T1550">Dann kam der Mann.</ta>
            <ta e="T1556" id="Seg_3827" s="T1553">Brachte einen Korb voll Geld.</ta>
            <ta e="T1559" id="Seg_3828" s="T1556">Viele Kleider.</ta>
            <ta e="T1562" id="Seg_3829" s="T1559">Seine Tochter kam ins Haus.</ta>
            <ta e="T1565" id="Seg_3830" s="T1562">Dann [sagt ihm] die Frau:</ta>
            <ta e="T1568" id="Seg_3831" s="T1565">„Nimm meine Tochter!“</ta>
            <ta e="T1572" id="Seg_3832" s="T1568">Der Mann schirrte das Pferd an.</ta>
            <ta e="T1573" id="Seg_3833" s="T1572">Nahm [sie].</ta>
            <ta e="T1581" id="Seg_3834" s="T1573">Er ließ ihre Tochter da und er kam selber [nach Hause].</ta>
            <ta e="T1586" id="Seg_3835" s="T1581">Dann kam der Frost wieder zu ihr.</ta>
            <ta e="T1590" id="Seg_3836" s="T1586">Er [Morozko fragt]: „Ist dir kalt?“</ta>
            <ta e="T1597" id="Seg_3837" s="T1590">Mir ist kalt, ich friere, komm nicht zu mir!“</ta>
            <ta e="T1600" id="Seg_3838" s="T1597">Er kommt wieder.</ta>
            <ta e="T1602" id="Seg_3839" s="T1600">„Ist dir kalt?“</ta>
            <ta e="T1603" id="Seg_3840" s="T1602">„Kalt!“</ta>
            <ta e="T1606" id="Seg_3841" s="T1603">Sie fängt an mit ihm zu schimpfen.</ta>
            <ta e="T1609" id="Seg_3842" s="T1606">Dann fragt er noch [einmal].</ta>
            <ta e="T1615" id="Seg_3843" s="T1609">Sie fluchte [ihn an] mit allen Sorten [von Wörtern, flucht mit schmutzigen Wörtern.</ta>
            <ta e="T1619" id="Seg_3844" s="T1615">Er fror sie ein.</ta>
            <ta e="T1625" id="Seg_3845" s="T1619">Und sie starb, wurde wie ein Baum.</ta>
            <ta e="T1629" id="Seg_3846" s="T1625">Dann am Morgen stand der Mann auf.</ta>
            <ta e="T1633" id="Seg_3847" s="T1629">„Geh, bring meine Tochter!“</ta>
            <ta e="T1635" id="Seg_3848" s="T1633">Er ging.</ta>
            <ta e="T1642" id="Seg_3849" s="T1635">Der Hund sagt: „Er bring die Frau ihre Tochter in einer Tasche.</ta>
            <ta e="T1643" id="Seg_3850" s="T1642">Sie starb.“</ta>
            <ta e="T1647" id="Seg_3851" s="T1643">Dann [sagt sie]: „Sag nicht sowas!</ta>
            <ta e="T1654" id="Seg_3852" s="T1647">Sondern sag, der Frau ihre Tochter; sie ist noch so [= wie die Tochter des Mannes.]</ta>
            <ta e="T1657" id="Seg_3853" s="T1654">Dann kommt der Mann.</ta>
            <ta e="T1659" id="Seg_3854" s="T1657">Die Frau rennt.</ta>
            <ta e="T1661" id="Seg_3855" s="T1659">Sie sieht.</ta>
            <ta e="T1664" id="Seg_3856" s="T1661">Ihre Tochter ist gestorben, sie liegt.</ta>
            <ta e="T1666" id="Seg_3857" s="T1664">Sie fing an zu weinen.</ta>
            <ta e="T1669" id="Seg_3858" s="T1666">Du wirst (kannst) nichts machen.</ta>
            <ta e="T1670" id="Seg_3859" s="T1669">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1406" id="Seg_3860" s="T1403">[GVY:] A well-known tale "Morozko", see e.g. http://www.smart-kiddy.ru/narodnye-skazki/russkie-narodnye/1780-skazka-morozko/</ta>
            <ta e="T1429" id="Seg_3861" s="T1426">Tʼübdə ’to smear’ in the meaning of ’caress?</ta>
            <ta e="T1457" id="Seg_3862" s="T1450">[KlT:] or toːndənə with a double lative on toʔ</ta>
            <ta e="T1498" id="Seg_3863" s="T1493">[KlT:] Alternative reading: reanalyzed ’šĭket’ as NOM form + -tə as POSS.3SG. [GVY:] dĭ may be tə &lt; Russian -то. Bostən - a Genitive form of bostə?</ta>
            <ta e="T1503" id="Seg_3864" s="T1498">[KlT:] šurbi: possible contamination šerbi 'dressed' with šuktəl- ’spread’?</ta>
            <ta e="T1572" id="Seg_3865" s="T1568">[KlT:] Koibal ’kuler-’ (körer-). </ta>
            <ta e="T1581" id="Seg_3866" s="T1573">[KlT:] Incorrect transitive use of ma-.</ta>
            <ta e="T1615" id="Seg_3867" s="T1609">Mut-mat in Janurik's word list 'swearing with dirty words'.</ta>
            <ta e="T1633" id="Seg_3868" s="T1629">[KlT:] Koʔbdojlaʔ? could be considered.</ta>
            <ta e="T1642" id="Seg_3869" s="T1635">[KlT:] LAT instead of LOC.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1534" />
            <conversion-tli id="T1535" />
            <conversion-tli id="T1536" />
            <conversion-tli id="T1537" />
            <conversion-tli id="T1538" />
            <conversion-tli id="T1539" />
            <conversion-tli id="T1540" />
            <conversion-tli id="T1541" />
            <conversion-tli id="T1542" />
            <conversion-tli id="T1543" />
            <conversion-tli id="T1544" />
            <conversion-tli id="T1545" />
            <conversion-tli id="T1546" />
            <conversion-tli id="T1547" />
            <conversion-tli id="T1548" />
            <conversion-tli id="T1549" />
            <conversion-tli id="T1550" />
            <conversion-tli id="T1551" />
            <conversion-tli id="T1552" />
            <conversion-tli id="T1553" />
            <conversion-tli id="T1554" />
            <conversion-tli id="T1555" />
            <conversion-tli id="T1556" />
            <conversion-tli id="T1557" />
            <conversion-tli id="T1558" />
            <conversion-tli id="T1559" />
            <conversion-tli id="T1560" />
            <conversion-tli id="T1561" />
            <conversion-tli id="T1562" />
            <conversion-tli id="T1563" />
            <conversion-tli id="T1564" />
            <conversion-tli id="T1565" />
            <conversion-tli id="T1566" />
            <conversion-tli id="T1567" />
            <conversion-tli id="T1568" />
            <conversion-tli id="T1569" />
            <conversion-tli id="T1570" />
            <conversion-tli id="T1571" />
            <conversion-tli id="T1572" />
            <conversion-tli id="T1573" />
            <conversion-tli id="T1574" />
            <conversion-tli id="T1575" />
            <conversion-tli id="T1576" />
            <conversion-tli id="T1577" />
            <conversion-tli id="T1578" />
            <conversion-tli id="T1579" />
            <conversion-tli id="T1580" />
            <conversion-tli id="T1581" />
            <conversion-tli id="T1582" />
            <conversion-tli id="T1583" />
            <conversion-tli id="T1584" />
            <conversion-tli id="T1585" />
            <conversion-tli id="T1586" />
            <conversion-tli id="T1587" />
            <conversion-tli id="T1588" />
            <conversion-tli id="T1589" />
            <conversion-tli id="T1590" />
            <conversion-tli id="T1591" />
            <conversion-tli id="T1592" />
            <conversion-tli id="T1593" />
            <conversion-tli id="T1594" />
            <conversion-tli id="T1595" />
            <conversion-tli id="T1596" />
            <conversion-tli id="T1597" />
            <conversion-tli id="T1598" />
            <conversion-tli id="T1599" />
            <conversion-tli id="T1600" />
            <conversion-tli id="T1601" />
            <conversion-tli id="T1602" />
            <conversion-tli id="T1603" />
            <conversion-tli id="T1604" />
            <conversion-tli id="T1605" />
            <conversion-tli id="T1606" />
            <conversion-tli id="T1607" />
            <conversion-tli id="T1608" />
            <conversion-tli id="T1609" />
            <conversion-tli id="T1610" />
            <conversion-tli id="T1611" />
            <conversion-tli id="T1612" />
            <conversion-tli id="T1613" />
            <conversion-tli id="T1614" />
            <conversion-tli id="T1615" />
            <conversion-tli id="T1616" />
            <conversion-tli id="T1617" />
            <conversion-tli id="T1618" />
            <conversion-tli id="T1619" />
            <conversion-tli id="T1620" />
            <conversion-tli id="T1621" />
            <conversion-tli id="T1622" />
            <conversion-tli id="T1623" />
            <conversion-tli id="T1624" />
            <conversion-tli id="T1625" />
            <conversion-tli id="T1626" />
            <conversion-tli id="T1627" />
            <conversion-tli id="T1628" />
            <conversion-tli id="T1629" />
            <conversion-tli id="T1630" />
            <conversion-tli id="T1631" />
            <conversion-tli id="T1632" />
            <conversion-tli id="T1633" />
            <conversion-tli id="T1634" />
            <conversion-tli id="T1635" />
            <conversion-tli id="T1636" />
            <conversion-tli id="T1637" />
            <conversion-tli id="T1638" />
            <conversion-tli id="T1639" />
            <conversion-tli id="T1640" />
            <conversion-tli id="T1641" />
            <conversion-tli id="T1642" />
            <conversion-tli id="T1643" />
            <conversion-tli id="T1644" />
            <conversion-tli id="T1645" />
            <conversion-tli id="T1646" />
            <conversion-tli id="T1647" />
            <conversion-tli id="T1648" />
            <conversion-tli id="T1649" />
            <conversion-tli id="T1650" />
            <conversion-tli id="T1651" />
            <conversion-tli id="T1652" />
            <conversion-tli id="T1653" />
            <conversion-tli id="T1654" />
            <conversion-tli id="T1655" />
            <conversion-tli id="T1656" />
            <conversion-tli id="T1657" />
            <conversion-tli id="T1658" />
            <conversion-tli id="T1659" />
            <conversion-tli id="T1660" />
            <conversion-tli id="T1661" />
            <conversion-tli id="T1662" />
            <conversion-tli id="T1663" />
            <conversion-tli id="T1664" />
            <conversion-tli id="T1665" />
            <conversion-tli id="T1666" />
            <conversion-tli id="T1667" />
            <conversion-tli id="T1668" />
            <conversion-tli id="T1669" />
            <conversion-tli id="T1670" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
