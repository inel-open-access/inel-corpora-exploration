<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE6D22AC1-3362-0491-9229-52A2D1D27C06">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_CraneAndHeron_flk.wav" />
         <referenced-file url="PKZ_196X_CraneAndHeron_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_CraneAndHeron_flk\PKZ_196X_CraneAndHeron_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">114</ud-information>
            <ud-information attribute-name="# HIAT:w">74</ud-information>
            <ud-information attribute-name="# e">74</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.072" type="appl" />
         <tli id="T1" time="0.984" type="appl" />
         <tli id="T2" time="1.896" type="appl" />
         <tli id="T3" time="2.808" type="appl" />
         <tli id="T4" time="3.72" type="appl" />
         <tli id="T5" time="4.632" type="appl" />
         <tli id="T6" time="5.506" type="appl" />
         <tli id="T7" time="6.381" type="appl" />
         <tli id="T8" time="7.255" type="appl" />
         <tli id="T9" time="8.13" type="appl" />
         <tli id="T10" time="9.11904046204794" />
         <tli id="T11" time="10.014" type="appl" />
         <tli id="T12" time="11.025" type="appl" />
         <tli id="T13" time="11.656" type="appl" />
         <tli id="T14" time="12.288" type="appl" />
         <tli id="T15" time="12.919" type="appl" />
         <tli id="T16" time="13.721" type="appl" />
         <tli id="T17" time="14.523" type="appl" />
         <tli id="T18" time="15.294" type="appl" />
         <tli id="T19" time="16.065" type="appl" />
         <tli id="T20" time="16.836" type="appl" />
         <tli id="T21" time="17.607" type="appl" />
         <tli id="T22" time="18.234" type="appl" />
         <tli id="T23" time="18.861" type="appl" />
         <tli id="T24" time="19.488" type="appl" />
         <tli id="T25" time="20.118" type="appl" />
         <tli id="T26" time="20.749" type="appl" />
         <tli id="T27" time="21.38" type="appl" />
         <tli id="T28" time="21.991019359865607" />
         <tli id="T29" time="22.427" type="appl" />
         <tli id="T30" time="22.84399926773667" />
         <tli id="T31" time="23.334" type="appl" />
         <tli id="T32" time="23.823" type="appl" />
         <tli id="T33" time="24.312" type="appl" />
         <tli id="T34" time="24.802" type="appl" />
         <tli id="T35" time="25.174" type="appl" />
         <tli id="T36" time="25.545" type="appl" />
         <tli id="T37" time="25.917" type="appl" />
         <tli id="T38" time="26.288" type="appl" />
         <tli id="T39" time="26.66" type="appl" />
         <tli id="T40" time="27.031" type="appl" />
         <tli id="T41" time="27.650423857145356" />
         <tli id="T42" time="28.374" type="appl" />
         <tli id="T43" time="29.019" type="appl" />
         <tli id="T74" time="29.316692307692307" type="intp" />
         <tli id="T44" time="29.664" type="appl" />
         <tli id="T45" time="30.463461192660148" />
         <tli id="T46" time="31.22" type="appl" />
         <tli id="T47" time="31.665" type="appl" />
         <tli id="T48" time="32.11" type="appl" />
         <tli id="T49" time="32.556" type="appl" />
         <tli id="T50" time="33.001" type="appl" />
         <tli id="T51" time="33.446" type="appl" />
         <tli id="T52" time="33.97" type="appl" />
         <tli id="T53" time="34.494" type="appl" />
         <tli id="T54" time="35.017" type="appl" />
         <tli id="T55" time="35.541" type="appl" />
         <tli id="T56" time="36.83612397169365" />
         <tli id="T57" time="37.319" type="appl" />
         <tli id="T58" time="37.709" type="appl" />
         <tli id="T59" time="38.098" type="appl" />
         <tli id="T60" time="38.487" type="appl" />
         <tli id="T61" time="38.877" type="appl" />
         <tli id="T62" time="39.302531114206616" />
         <tli id="T63" time="40.056" type="appl" />
         <tli id="T64" time="40.845" type="appl" />
         <tli id="T65" time="41.78227018721965" />
         <tli id="T66" time="42.453" type="appl" />
         <tli id="T67" time="43.093" type="appl" />
         <tli id="T68" time="43.78532764753829" />
         <tli id="T69" time="44.369" type="appl" />
         <tli id="T70" time="45.006" type="appl" />
         <tli id="T71" time="45.644" type="appl" />
         <tli id="T72" time="46.31432911511152" />
         <tli id="T73" time="47.555" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T73" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">žura-</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">žuravelʼ</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">saplʼa</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_22" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">Dĭgəttə</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">žuravelʼ:</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_29" n="HIAT:ip">"</nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">Kal-</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">Kalam</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">monoʔkosʼtə</ts>
                  <nts id="Seg_40" n="HIAT:ip">!</nts>
                  <nts id="Seg_41" n="HIAT:ip">"</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_44" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Šobi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">sablʼanə</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_53" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_55" n="HIAT:w" s="T12">Măndə:</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_58" n="HIAT:w" s="T13">Ej</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">kalam</ts>
                  <nts id="Seg_62" n="HIAT:ip">!</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_65" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_67" n="HIAT:w" s="T15">Unnʼa</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_70" n="HIAT:w" s="T16">amnolam</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_74" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_76" n="HIAT:w" s="T17">Dĭgəttə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_79" n="HIAT:w" s="T18">dĭ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_82" n="HIAT:w" s="T19">kambi</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">maːndə</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_89" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_91" n="HIAT:w" s="T21">Dĭgəttə</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_94" n="HIAT:w" s="T22">dĭ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_97" n="HIAT:w" s="T23">măndə:</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">Ădnakă</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">kalam</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">dĭʔnə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">tibinə</ts>
                  <nts id="Seg_111" n="HIAT:ip">"</nts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_115" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_117" n="HIAT:w" s="T28">Kambi</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">dĭʔnə</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_124" n="HIAT:u" s="T30">
                  <nts id="Seg_125" n="HIAT:ip">"</nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">Măn</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_130" n="HIAT:w" s="T31">kalam</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_133" n="HIAT:w" s="T32">tănan</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_136" n="HIAT:w" s="T33">tibinə</ts>
                  <nts id="Seg_137" n="HIAT:ip">"</nts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_143" n="HIAT:w" s="T34">A</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_146" n="HIAT:w" s="T35">dĭ</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_149" n="HIAT:w" s="T36">măndə:</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_151" n="HIAT:ip">"</nts>
                  <ts e="T38" id="Seg_153" n="HIAT:w" s="T37">Dʼok</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_157" n="HIAT:w" s="T38">măn</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_160" n="HIAT:w" s="T39">ej</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_163" n="HIAT:w" s="T40">ilem</ts>
                  <nts id="Seg_164" n="HIAT:ip">!</nts>
                  <nts id="Seg_165" n="HIAT:ip">"</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_168" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_170" n="HIAT:w" s="T41">Dĭgəttə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">dĭ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_176" n="HIAT:w" s="T43">kalla</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_179" n="HIAT:w" s="T74">dʼürbi</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_182" n="HIAT:w" s="T44">maːndə</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_186" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_188" n="HIAT:w" s="T45">Dĭgəttə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">žurav:</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">"</nts>
                  <ts e="T48" id="Seg_195" n="HIAT:w" s="T47">Zrʼa</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_198" n="HIAT:w" s="T48">măndəm</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_201" n="HIAT:w" s="T49">ej</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_204" n="HIAT:w" s="T50">ibiem</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_208" n="HIAT:u" s="T51">
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_211" n="HIAT:w" s="T51">Ka-</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_214" n="HIAT:w" s="T52">kal-</ts>
                  <nts id="Seg_215" n="HIAT:ip">)</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_218" n="HIAT:w" s="T53">Kalam</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_221" n="HIAT:w" s="T54">monoʔkosʼtə</ts>
                  <nts id="Seg_222" n="HIAT:ip">"</nts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_226" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_228" n="HIAT:w" s="T55">Šobi</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_232" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_234" n="HIAT:w" s="T56">Dĭ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">măndə:</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_239" n="HIAT:ip">"</nts>
                  <ts e="T59" id="Seg_241" n="HIAT:w" s="T58">Dʼok</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_245" n="HIAT:w" s="T59">măn</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_248" n="HIAT:w" s="T60">ej</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_251" n="HIAT:w" s="T61">kalam</ts>
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_256" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_258" n="HIAT:w" s="T62">Dĭgəttə</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_261" n="HIAT:w" s="T63">mĭmbiʔi</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_265" n="HIAT:w" s="T64">mĭmbiʔi</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_269" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_271" n="HIAT:w" s="T65">I</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_274" n="HIAT:w" s="T66">dăre</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_277" n="HIAT:w" s="T67">maːluʔpiʔi</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_281" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_283" n="HIAT:w" s="T68">I</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_286" n="HIAT:w" s="T69">tüjö</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_289" n="HIAT:w" s="T70">dăre</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_292" n="HIAT:w" s="T71">amnolaʔbəʔjə</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_296" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_298" n="HIAT:w" s="T72">Kabarləj</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T73" id="Seg_301" n="sc" s="T0">
               <ts e="T1" id="Seg_303" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_305" n="e" s="T1">(žura-) </ts>
               <ts e="T3" id="Seg_307" n="e" s="T2">žuravelʼ </ts>
               <ts e="T4" id="Seg_309" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_311" n="e" s="T4">saplʼa. </ts>
               <ts e="T6" id="Seg_313" n="e" s="T5">Dĭgəttə </ts>
               <ts e="T7" id="Seg_315" n="e" s="T6">žuravelʼ: </ts>
               <ts e="T8" id="Seg_317" n="e" s="T7">"(Kal-) </ts>
               <ts e="T9" id="Seg_319" n="e" s="T8">Kalam </ts>
               <ts e="T10" id="Seg_321" n="e" s="T9">monoʔkosʼtə!" </ts>
               <ts e="T11" id="Seg_323" n="e" s="T10">Šobi </ts>
               <ts e="T12" id="Seg_325" n="e" s="T11">sablʼanə. </ts>
               <ts e="T13" id="Seg_327" n="e" s="T12">Măndə: </ts>
               <ts e="T14" id="Seg_329" n="e" s="T13">Ej </ts>
               <ts e="T15" id="Seg_331" n="e" s="T14">kalam! </ts>
               <ts e="T16" id="Seg_333" n="e" s="T15">Unnʼa </ts>
               <ts e="T17" id="Seg_335" n="e" s="T16">amnolam. </ts>
               <ts e="T18" id="Seg_337" n="e" s="T17">Dĭgəttə </ts>
               <ts e="T19" id="Seg_339" n="e" s="T18">dĭ </ts>
               <ts e="T20" id="Seg_341" n="e" s="T19">kambi </ts>
               <ts e="T21" id="Seg_343" n="e" s="T20">maːndə. </ts>
               <ts e="T22" id="Seg_345" n="e" s="T21">Dĭgəttə </ts>
               <ts e="T23" id="Seg_347" n="e" s="T22">dĭ </ts>
               <ts e="T24" id="Seg_349" n="e" s="T23">măndə: </ts>
               <ts e="T25" id="Seg_351" n="e" s="T24">"Ădnakă </ts>
               <ts e="T26" id="Seg_353" n="e" s="T25">kalam </ts>
               <ts e="T27" id="Seg_355" n="e" s="T26">dĭʔnə </ts>
               <ts e="T28" id="Seg_357" n="e" s="T27">tibinə". </ts>
               <ts e="T29" id="Seg_359" n="e" s="T28">Kambi </ts>
               <ts e="T30" id="Seg_361" n="e" s="T29">dĭʔnə. </ts>
               <ts e="T31" id="Seg_363" n="e" s="T30">"Măn </ts>
               <ts e="T32" id="Seg_365" n="e" s="T31">kalam </ts>
               <ts e="T33" id="Seg_367" n="e" s="T32">tănan </ts>
               <ts e="T34" id="Seg_369" n="e" s="T33">tibinə". </ts>
               <ts e="T35" id="Seg_371" n="e" s="T34">A </ts>
               <ts e="T36" id="Seg_373" n="e" s="T35">dĭ </ts>
               <ts e="T37" id="Seg_375" n="e" s="T36">măndə: </ts>
               <ts e="T38" id="Seg_377" n="e" s="T37">"Dʼok, </ts>
               <ts e="T39" id="Seg_379" n="e" s="T38">măn </ts>
               <ts e="T40" id="Seg_381" n="e" s="T39">ej </ts>
               <ts e="T41" id="Seg_383" n="e" s="T40">ilem!" </ts>
               <ts e="T42" id="Seg_385" n="e" s="T41">Dĭgəttə </ts>
               <ts e="T43" id="Seg_387" n="e" s="T42">dĭ </ts>
               <ts e="T74" id="Seg_389" n="e" s="T43">kalla </ts>
               <ts e="T44" id="Seg_391" n="e" s="T74">dʼürbi </ts>
               <ts e="T45" id="Seg_393" n="e" s="T44">maːndə. </ts>
               <ts e="T46" id="Seg_395" n="e" s="T45">Dĭgəttə </ts>
               <ts e="T47" id="Seg_397" n="e" s="T46">žurav: </ts>
               <ts e="T48" id="Seg_399" n="e" s="T47">"Zrʼa </ts>
               <ts e="T49" id="Seg_401" n="e" s="T48">măndəm </ts>
               <ts e="T50" id="Seg_403" n="e" s="T49">ej </ts>
               <ts e="T51" id="Seg_405" n="e" s="T50">ibiem. </ts>
               <ts e="T52" id="Seg_407" n="e" s="T51">(Ka- </ts>
               <ts e="T53" id="Seg_409" n="e" s="T52">kal-) </ts>
               <ts e="T54" id="Seg_411" n="e" s="T53">Kalam </ts>
               <ts e="T55" id="Seg_413" n="e" s="T54">monoʔkosʼtə". </ts>
               <ts e="T56" id="Seg_415" n="e" s="T55">Šobi. </ts>
               <ts e="T57" id="Seg_417" n="e" s="T56">Dĭ </ts>
               <ts e="T58" id="Seg_419" n="e" s="T57">măndə: </ts>
               <ts e="T59" id="Seg_421" n="e" s="T58">"Dʼok, </ts>
               <ts e="T60" id="Seg_423" n="e" s="T59">măn </ts>
               <ts e="T61" id="Seg_425" n="e" s="T60">ej </ts>
               <ts e="T62" id="Seg_427" n="e" s="T61">kalam". </ts>
               <ts e="T63" id="Seg_429" n="e" s="T62">Dĭgəttə </ts>
               <ts e="T64" id="Seg_431" n="e" s="T63">mĭmbiʔi, </ts>
               <ts e="T65" id="Seg_433" n="e" s="T64">mĭmbiʔi. </ts>
               <ts e="T66" id="Seg_435" n="e" s="T65">I </ts>
               <ts e="T67" id="Seg_437" n="e" s="T66">dăre </ts>
               <ts e="T68" id="Seg_439" n="e" s="T67">maːluʔpiʔi. </ts>
               <ts e="T69" id="Seg_441" n="e" s="T68">I </ts>
               <ts e="T70" id="Seg_443" n="e" s="T69">tüjö </ts>
               <ts e="T71" id="Seg_445" n="e" s="T70">dăre </ts>
               <ts e="T72" id="Seg_447" n="e" s="T71">amnolaʔbəʔjə. </ts>
               <ts e="T73" id="Seg_449" n="e" s="T72">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_450" s="T0">PKZ_196X_CraneAndHeron_flk.001 (001)</ta>
            <ta e="T10" id="Seg_451" s="T5">PKZ_196X_CraneAndHeron_flk.002 (002)</ta>
            <ta e="T12" id="Seg_452" s="T10">PKZ_196X_CraneAndHeron_flk.003 (003)</ta>
            <ta e="T15" id="Seg_453" s="T12">PKZ_196X_CraneAndHeron_flk.004 (004)</ta>
            <ta e="T17" id="Seg_454" s="T15">PKZ_196X_CraneAndHeron_flk.005 (005)</ta>
            <ta e="T21" id="Seg_455" s="T17">PKZ_196X_CraneAndHeron_flk.006 (006)</ta>
            <ta e="T28" id="Seg_456" s="T21">PKZ_196X_CraneAndHeron_flk.007 (007)</ta>
            <ta e="T30" id="Seg_457" s="T28">PKZ_196X_CraneAndHeron_flk.008 (009)</ta>
            <ta e="T34" id="Seg_458" s="T30">PKZ_196X_CraneAndHeron_flk.009 (010)</ta>
            <ta e="T41" id="Seg_459" s="T34">PKZ_196X_CraneAndHeron_flk.010 (011)</ta>
            <ta e="T45" id="Seg_460" s="T41">PKZ_196X_CraneAndHeron_flk.011 (012)</ta>
            <ta e="T51" id="Seg_461" s="T45">PKZ_196X_CraneAndHeron_flk.012 (013)</ta>
            <ta e="T55" id="Seg_462" s="T51">PKZ_196X_CraneAndHeron_flk.013 (014)</ta>
            <ta e="T56" id="Seg_463" s="T55">PKZ_196X_CraneAndHeron_flk.014 (015)</ta>
            <ta e="T62" id="Seg_464" s="T56">PKZ_196X_CraneAndHeron_flk.015 (016)</ta>
            <ta e="T65" id="Seg_465" s="T62">PKZ_196X_CraneAndHeron_flk.016 (017)</ta>
            <ta e="T68" id="Seg_466" s="T65">PKZ_196X_CraneAndHeron_flk.017 (018)</ta>
            <ta e="T72" id="Seg_467" s="T68">PKZ_196X_CraneAndHeron_flk.018 (019)</ta>
            <ta e="T73" id="Seg_468" s="T72">PKZ_196X_CraneAndHeron_flk.019 (020)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_469" s="T0">Amnobiʔi (žura-) žuravelʼ i saplʼa. </ta>
            <ta e="T10" id="Seg_470" s="T5">Dĭgəttə žuravelʼ: "(Kal-) Kalam monoʔkosʼtə!" </ta>
            <ta e="T12" id="Seg_471" s="T10">Šobi sablʼanə. </ta>
            <ta e="T15" id="Seg_472" s="T12">Măndə: "Ej kalam! </ta>
            <ta e="T17" id="Seg_473" s="T15">Unnʼa amnolam". </ta>
            <ta e="T21" id="Seg_474" s="T17">Dĭgəttə dĭ kambi maːndə. </ta>
            <ta e="T28" id="Seg_475" s="T21">Dĭgəttə dĭ măndə: "Ădnakă kalam dĭʔnə tibinə". </ta>
            <ta e="T30" id="Seg_476" s="T28">Kambi dĭʔnə. </ta>
            <ta e="T34" id="Seg_477" s="T30">"Măn kalam tănan tibinə". </ta>
            <ta e="T41" id="Seg_478" s="T34">A dĭ măndə: "Dʼok, măn ej ilem!" </ta>
            <ta e="T45" id="Seg_479" s="T41">Dĭgəttə dĭ kalla dʼürbi maːndə. </ta>
            <ta e="T51" id="Seg_480" s="T45">Dĭgəttə žurav: "Zrʼa măndəm ej ibiem. </ta>
            <ta e="T55" id="Seg_481" s="T51">(Ka- kal-) Kalam monoʔkosʼtə". </ta>
            <ta e="T56" id="Seg_482" s="T55">Šobi. </ta>
            <ta e="T62" id="Seg_483" s="T56">Dĭ măndə: "Dʼok, măn ej kalam". </ta>
            <ta e="T65" id="Seg_484" s="T62">Dĭgəttə mĭmbiʔi, mĭmbiʔi. </ta>
            <ta e="T68" id="Seg_485" s="T65">I dăre maːluʔpiʔi. </ta>
            <ta e="T72" id="Seg_486" s="T68">I tüjö dăre amnolaʔbəʔjə. </ta>
            <ta e="T73" id="Seg_487" s="T72">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_488" s="T0">amno-bi-ʔi</ta>
            <ta e="T3" id="Seg_489" s="T2">žuravelʼ</ta>
            <ta e="T4" id="Seg_490" s="T3">i</ta>
            <ta e="T5" id="Seg_491" s="T4">saplʼa</ta>
            <ta e="T6" id="Seg_492" s="T5">dĭgəttə</ta>
            <ta e="T7" id="Seg_493" s="T6">žuravelʼ</ta>
            <ta e="T9" id="Seg_494" s="T8">ka-la-m</ta>
            <ta e="T10" id="Seg_495" s="T9">monoʔko-sʼtə</ta>
            <ta e="T11" id="Seg_496" s="T10">šo-bi</ta>
            <ta e="T12" id="Seg_497" s="T11">sablʼa-nə</ta>
            <ta e="T13" id="Seg_498" s="T12">măn-də</ta>
            <ta e="T14" id="Seg_499" s="T13">ej</ta>
            <ta e="T15" id="Seg_500" s="T14">ka-la-m</ta>
            <ta e="T16" id="Seg_501" s="T15">unnʼa</ta>
            <ta e="T17" id="Seg_502" s="T16">amno-la-m</ta>
            <ta e="T18" id="Seg_503" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_504" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_505" s="T19">kam-bi</ta>
            <ta e="T21" id="Seg_506" s="T20">ma-ndə</ta>
            <ta e="T22" id="Seg_507" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_508" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_509" s="T23">măn-də</ta>
            <ta e="T25" id="Seg_510" s="T24">ădnakă</ta>
            <ta e="T26" id="Seg_511" s="T25">ka-la-m</ta>
            <ta e="T27" id="Seg_512" s="T26">dĭʔ-nə</ta>
            <ta e="T28" id="Seg_513" s="T27">tibi-nə</ta>
            <ta e="T29" id="Seg_514" s="T28">kam-bi</ta>
            <ta e="T30" id="Seg_515" s="T29">dĭʔ-nə</ta>
            <ta e="T31" id="Seg_516" s="T30">măn</ta>
            <ta e="T32" id="Seg_517" s="T31">ka-la-m</ta>
            <ta e="T33" id="Seg_518" s="T32">tănan</ta>
            <ta e="T34" id="Seg_519" s="T33">tibi-nə</ta>
            <ta e="T35" id="Seg_520" s="T34">a</ta>
            <ta e="T36" id="Seg_521" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_522" s="T36">măn-də</ta>
            <ta e="T38" id="Seg_523" s="T37">dʼok</ta>
            <ta e="T39" id="Seg_524" s="T38">măn</ta>
            <ta e="T40" id="Seg_525" s="T39">ej</ta>
            <ta e="T41" id="Seg_526" s="T40">i-le-m</ta>
            <ta e="T42" id="Seg_527" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_528" s="T42">dĭ</ta>
            <ta e="T74" id="Seg_529" s="T43">kal-la</ta>
            <ta e="T44" id="Seg_530" s="T74">dʼür-bi</ta>
            <ta e="T45" id="Seg_531" s="T44">ma-ndə</ta>
            <ta e="T46" id="Seg_532" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_533" s="T46">žurav</ta>
            <ta e="T49" id="Seg_534" s="T48">măn-də-m</ta>
            <ta e="T50" id="Seg_535" s="T49">ej</ta>
            <ta e="T51" id="Seg_536" s="T50">i-bie-m</ta>
            <ta e="T54" id="Seg_537" s="T53">ka-la-m</ta>
            <ta e="T55" id="Seg_538" s="T54">monoʔko-sʼtə</ta>
            <ta e="T56" id="Seg_539" s="T55">šo-bi</ta>
            <ta e="T57" id="Seg_540" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_541" s="T57">măn-də</ta>
            <ta e="T59" id="Seg_542" s="T58">dʼok</ta>
            <ta e="T60" id="Seg_543" s="T59">măn</ta>
            <ta e="T61" id="Seg_544" s="T60">ej</ta>
            <ta e="T62" id="Seg_545" s="T61">ka-la-m</ta>
            <ta e="T63" id="Seg_546" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_547" s="T63">mĭm-bi-ʔi</ta>
            <ta e="T65" id="Seg_548" s="T64">mĭm-bi-ʔi</ta>
            <ta e="T66" id="Seg_549" s="T65">i</ta>
            <ta e="T67" id="Seg_550" s="T66">dăre</ta>
            <ta e="T68" id="Seg_551" s="T67">maː-luʔ-pi-ʔi</ta>
            <ta e="T69" id="Seg_552" s="T68">i</ta>
            <ta e="T70" id="Seg_553" s="T69">tüjö</ta>
            <ta e="T71" id="Seg_554" s="T70">dăre</ta>
            <ta e="T72" id="Seg_555" s="T71">amno-laʔbə-ʔjə</ta>
            <ta e="T73" id="Seg_556" s="T72">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_557" s="T0">amno-bi-jəʔ</ta>
            <ta e="T3" id="Seg_558" s="T2">žuravlʼ</ta>
            <ta e="T4" id="Seg_559" s="T3">i</ta>
            <ta e="T5" id="Seg_560" s="T4">saplʼa</ta>
            <ta e="T6" id="Seg_561" s="T5">dĭgəttə</ta>
            <ta e="T7" id="Seg_562" s="T6">žuravlʼ</ta>
            <ta e="T9" id="Seg_563" s="T8">kan-lV-m</ta>
            <ta e="T10" id="Seg_564" s="T9">monoʔko-zittə</ta>
            <ta e="T11" id="Seg_565" s="T10">šo-bi</ta>
            <ta e="T12" id="Seg_566" s="T11">saplʼa-Tə</ta>
            <ta e="T13" id="Seg_567" s="T12">măn-ntə</ta>
            <ta e="T14" id="Seg_568" s="T13">ej</ta>
            <ta e="T15" id="Seg_569" s="T14">kan-lV-m</ta>
            <ta e="T16" id="Seg_570" s="T15">unʼə</ta>
            <ta e="T17" id="Seg_571" s="T16">amno-lV-m</ta>
            <ta e="T18" id="Seg_572" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_573" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_574" s="T19">kan-bi</ta>
            <ta e="T21" id="Seg_575" s="T20">maʔ-gəndə</ta>
            <ta e="T22" id="Seg_576" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_577" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_578" s="T23">măn-ntə</ta>
            <ta e="T25" id="Seg_579" s="T24">adnaka</ta>
            <ta e="T26" id="Seg_580" s="T25">kan-lV-m</ta>
            <ta e="T27" id="Seg_581" s="T26">dĭ-Tə</ta>
            <ta e="T28" id="Seg_582" s="T27">tibi-Tə</ta>
            <ta e="T29" id="Seg_583" s="T28">kan-bi</ta>
            <ta e="T30" id="Seg_584" s="T29">dĭ-Tə</ta>
            <ta e="T31" id="Seg_585" s="T30">măn</ta>
            <ta e="T32" id="Seg_586" s="T31">kan-lV-m</ta>
            <ta e="T33" id="Seg_587" s="T32">tănan</ta>
            <ta e="T34" id="Seg_588" s="T33">tibi-Tə</ta>
            <ta e="T35" id="Seg_589" s="T34">a</ta>
            <ta e="T36" id="Seg_590" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_591" s="T36">măn-ntə</ta>
            <ta e="T38" id="Seg_592" s="T37">dʼok</ta>
            <ta e="T39" id="Seg_593" s="T38">măn</ta>
            <ta e="T40" id="Seg_594" s="T39">ej</ta>
            <ta e="T41" id="Seg_595" s="T40">i-lV-m</ta>
            <ta e="T42" id="Seg_596" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_597" s="T42">dĭ</ta>
            <ta e="T74" id="Seg_598" s="T43">kan-lAʔ</ta>
            <ta e="T44" id="Seg_599" s="T74">tʼür-bi</ta>
            <ta e="T45" id="Seg_600" s="T44">maʔ-gəndə</ta>
            <ta e="T46" id="Seg_601" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_602" s="T46">žuravlʼ</ta>
            <ta e="T49" id="Seg_603" s="T48">măn-ntə-m</ta>
            <ta e="T50" id="Seg_604" s="T49">ej</ta>
            <ta e="T51" id="Seg_605" s="T50">i-bi-m</ta>
            <ta e="T54" id="Seg_606" s="T53">kan-lV-m</ta>
            <ta e="T55" id="Seg_607" s="T54">monoʔko-zittə</ta>
            <ta e="T56" id="Seg_608" s="T55">šo-bi</ta>
            <ta e="T57" id="Seg_609" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_610" s="T57">măn-ntə</ta>
            <ta e="T59" id="Seg_611" s="T58">dʼok</ta>
            <ta e="T60" id="Seg_612" s="T59">măn</ta>
            <ta e="T61" id="Seg_613" s="T60">ej</ta>
            <ta e="T62" id="Seg_614" s="T61">kan-lV-m</ta>
            <ta e="T63" id="Seg_615" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_616" s="T63">mĭn-bi-jəʔ</ta>
            <ta e="T65" id="Seg_617" s="T64">mĭn-bi-jəʔ</ta>
            <ta e="T66" id="Seg_618" s="T65">i</ta>
            <ta e="T67" id="Seg_619" s="T66">dărəʔ</ta>
            <ta e="T68" id="Seg_620" s="T67">ma-luʔbdə-bi-jəʔ</ta>
            <ta e="T69" id="Seg_621" s="T68">i</ta>
            <ta e="T70" id="Seg_622" s="T69">tüjö</ta>
            <ta e="T71" id="Seg_623" s="T70">dărəʔ</ta>
            <ta e="T72" id="Seg_624" s="T71">amno-laʔbə-jəʔ</ta>
            <ta e="T73" id="Seg_625" s="T72">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_626" s="T0">live-PST-3PL</ta>
            <ta e="T3" id="Seg_627" s="T2">crane.[NOM.SG]</ta>
            <ta e="T4" id="Seg_628" s="T3">and</ta>
            <ta e="T5" id="Seg_629" s="T4">heron.[NOM.SG]</ta>
            <ta e="T6" id="Seg_630" s="T5">then</ta>
            <ta e="T7" id="Seg_631" s="T6">crane.[NOM.SG]</ta>
            <ta e="T9" id="Seg_632" s="T8">go-FUT-1SG</ta>
            <ta e="T10" id="Seg_633" s="T9">marry-INF.LAT</ta>
            <ta e="T11" id="Seg_634" s="T10">come-PST.[3SG]</ta>
            <ta e="T12" id="Seg_635" s="T11">heron-LAT</ta>
            <ta e="T13" id="Seg_636" s="T12">say-IPFVZ.[3SG]</ta>
            <ta e="T14" id="Seg_637" s="T13">NEG</ta>
            <ta e="T15" id="Seg_638" s="T14">go-FUT-1SG</ta>
            <ta e="T16" id="Seg_639" s="T15">alone</ta>
            <ta e="T17" id="Seg_640" s="T16">live-FUT-1SG</ta>
            <ta e="T18" id="Seg_641" s="T17">then</ta>
            <ta e="T19" id="Seg_642" s="T18">this.[NOM.SG]</ta>
            <ta e="T20" id="Seg_643" s="T19">go-PST.[3SG]</ta>
            <ta e="T21" id="Seg_644" s="T20">tent-LAT/LOC.3SG</ta>
            <ta e="T22" id="Seg_645" s="T21">then</ta>
            <ta e="T23" id="Seg_646" s="T22">this.[NOM.SG]</ta>
            <ta e="T24" id="Seg_647" s="T23">say-IPFVZ.[3SG]</ta>
            <ta e="T25" id="Seg_648" s="T24">however</ta>
            <ta e="T26" id="Seg_649" s="T25">go-FUT-1SG</ta>
            <ta e="T27" id="Seg_650" s="T26">this-LAT</ta>
            <ta e="T28" id="Seg_651" s="T27">man-LAT</ta>
            <ta e="T29" id="Seg_652" s="T28">go-PST.[3SG]</ta>
            <ta e="T30" id="Seg_653" s="T29">this-LAT</ta>
            <ta e="T31" id="Seg_654" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_655" s="T31">go-FUT-1SG</ta>
            <ta e="T33" id="Seg_656" s="T32">you.DAT</ta>
            <ta e="T34" id="Seg_657" s="T33">man-LAT</ta>
            <ta e="T35" id="Seg_658" s="T34">and</ta>
            <ta e="T36" id="Seg_659" s="T35">this.[NOM.SG]</ta>
            <ta e="T37" id="Seg_660" s="T36">say-IPFVZ.[3SG]</ta>
            <ta e="T38" id="Seg_661" s="T37">no</ta>
            <ta e="T39" id="Seg_662" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_663" s="T39">NEG</ta>
            <ta e="T41" id="Seg_664" s="T40">take-FUT-1SG</ta>
            <ta e="T42" id="Seg_665" s="T41">then</ta>
            <ta e="T43" id="Seg_666" s="T42">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_667" s="T43">go-CVB</ta>
            <ta e="T44" id="Seg_668" s="T74">disappear-PST.[3SG]</ta>
            <ta e="T45" id="Seg_669" s="T44">tent-LAT/LOC.3SG</ta>
            <ta e="T46" id="Seg_670" s="T45">then</ta>
            <ta e="T47" id="Seg_671" s="T46">crane.[NOM.SG]</ta>
            <ta e="T49" id="Seg_672" s="T48">say-IPFVZ-1SG</ta>
            <ta e="T50" id="Seg_673" s="T49">NEG</ta>
            <ta e="T51" id="Seg_674" s="T50">take-PST-1SG</ta>
            <ta e="T54" id="Seg_675" s="T53">go-FUT-1SG</ta>
            <ta e="T55" id="Seg_676" s="T54">marry-INF.LAT</ta>
            <ta e="T56" id="Seg_677" s="T55">come-PST.[3SG]</ta>
            <ta e="T57" id="Seg_678" s="T56">this.[NOM.SG]</ta>
            <ta e="T58" id="Seg_679" s="T57">say-IPFVZ.[3SG]</ta>
            <ta e="T59" id="Seg_680" s="T58">no</ta>
            <ta e="T60" id="Seg_681" s="T59">I.NOM</ta>
            <ta e="T61" id="Seg_682" s="T60">NEG</ta>
            <ta e="T62" id="Seg_683" s="T61">go-FUT-1SG</ta>
            <ta e="T63" id="Seg_684" s="T62">then</ta>
            <ta e="T64" id="Seg_685" s="T63">go-PST-3PL</ta>
            <ta e="T65" id="Seg_686" s="T64">go-PST-3PL</ta>
            <ta e="T66" id="Seg_687" s="T65">and</ta>
            <ta e="T67" id="Seg_688" s="T66">so</ta>
            <ta e="T68" id="Seg_689" s="T67">remain-MOM-PST-3PL</ta>
            <ta e="T69" id="Seg_690" s="T68">and</ta>
            <ta e="T70" id="Seg_691" s="T69">soon</ta>
            <ta e="T71" id="Seg_692" s="T70">so</ta>
            <ta e="T72" id="Seg_693" s="T71">live-DUR-3PL</ta>
            <ta e="T73" id="Seg_694" s="T72">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_695" s="T0">жить-PST-3PL</ta>
            <ta e="T3" id="Seg_696" s="T2">журавль.[NOM.SG]</ta>
            <ta e="T4" id="Seg_697" s="T3">и</ta>
            <ta e="T5" id="Seg_698" s="T4">цапля.[NOM.SG]</ta>
            <ta e="T6" id="Seg_699" s="T5">тогда</ta>
            <ta e="T7" id="Seg_700" s="T6">журавль.[NOM.SG]</ta>
            <ta e="T9" id="Seg_701" s="T8">пойти-FUT-1SG</ta>
            <ta e="T10" id="Seg_702" s="T9">жениться-INF.LAT</ta>
            <ta e="T11" id="Seg_703" s="T10">прийти-PST.[3SG]</ta>
            <ta e="T12" id="Seg_704" s="T11">цапля-LAT</ta>
            <ta e="T13" id="Seg_705" s="T12">сказать-IPFVZ.[3SG]</ta>
            <ta e="T14" id="Seg_706" s="T13">NEG</ta>
            <ta e="T15" id="Seg_707" s="T14">пойти-FUT-1SG</ta>
            <ta e="T16" id="Seg_708" s="T15">один</ta>
            <ta e="T17" id="Seg_709" s="T16">жить-FUT-1SG</ta>
            <ta e="T18" id="Seg_710" s="T17">тогда</ta>
            <ta e="T19" id="Seg_711" s="T18">этот.[NOM.SG]</ta>
            <ta e="T20" id="Seg_712" s="T19">пойти-PST.[3SG]</ta>
            <ta e="T21" id="Seg_713" s="T20">чум-LAT/LOC.3SG</ta>
            <ta e="T22" id="Seg_714" s="T21">тогда</ta>
            <ta e="T23" id="Seg_715" s="T22">этот.[NOM.SG]</ta>
            <ta e="T24" id="Seg_716" s="T23">сказать-IPFVZ.[3SG]</ta>
            <ta e="T25" id="Seg_717" s="T24">однако</ta>
            <ta e="T26" id="Seg_718" s="T25">пойти-FUT-1SG</ta>
            <ta e="T27" id="Seg_719" s="T26">этот-LAT</ta>
            <ta e="T28" id="Seg_720" s="T27">мужчина-LAT</ta>
            <ta e="T29" id="Seg_721" s="T28">пойти-PST.[3SG]</ta>
            <ta e="T30" id="Seg_722" s="T29">этот-LAT</ta>
            <ta e="T31" id="Seg_723" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_724" s="T31">пойти-FUT-1SG</ta>
            <ta e="T33" id="Seg_725" s="T32">ты.DAT</ta>
            <ta e="T34" id="Seg_726" s="T33">мужчина-LAT</ta>
            <ta e="T35" id="Seg_727" s="T34">а</ta>
            <ta e="T36" id="Seg_728" s="T35">этот.[NOM.SG]</ta>
            <ta e="T37" id="Seg_729" s="T36">сказать-IPFVZ.[3SG]</ta>
            <ta e="T38" id="Seg_730" s="T37">нет</ta>
            <ta e="T39" id="Seg_731" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_732" s="T39">NEG</ta>
            <ta e="T41" id="Seg_733" s="T40">взять-FUT-1SG</ta>
            <ta e="T42" id="Seg_734" s="T41">тогда</ta>
            <ta e="T43" id="Seg_735" s="T42">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_736" s="T43">пойти-CVB</ta>
            <ta e="T44" id="Seg_737" s="T74">исчезнуть-PST.[3SG]</ta>
            <ta e="T45" id="Seg_738" s="T44">чум-LAT/LOC.3SG</ta>
            <ta e="T46" id="Seg_739" s="T45">тогда</ta>
            <ta e="T47" id="Seg_740" s="T46">журавль.[NOM.SG]</ta>
            <ta e="T49" id="Seg_741" s="T48">сказать-IPFVZ-1SG</ta>
            <ta e="T50" id="Seg_742" s="T49">NEG</ta>
            <ta e="T51" id="Seg_743" s="T50">взять-PST-1SG</ta>
            <ta e="T54" id="Seg_744" s="T53">пойти-FUT-1SG</ta>
            <ta e="T55" id="Seg_745" s="T54">жениться-INF.LAT</ta>
            <ta e="T56" id="Seg_746" s="T55">прийти-PST.[3SG]</ta>
            <ta e="T57" id="Seg_747" s="T56">этот.[NOM.SG]</ta>
            <ta e="T58" id="Seg_748" s="T57">сказать-IPFVZ.[3SG]</ta>
            <ta e="T59" id="Seg_749" s="T58">нет</ta>
            <ta e="T60" id="Seg_750" s="T59">я.NOM</ta>
            <ta e="T61" id="Seg_751" s="T60">NEG</ta>
            <ta e="T62" id="Seg_752" s="T61">пойти-FUT-1SG</ta>
            <ta e="T63" id="Seg_753" s="T62">тогда</ta>
            <ta e="T64" id="Seg_754" s="T63">идти-PST-3PL</ta>
            <ta e="T65" id="Seg_755" s="T64">идти-PST-3PL</ta>
            <ta e="T66" id="Seg_756" s="T65">и</ta>
            <ta e="T67" id="Seg_757" s="T66">так</ta>
            <ta e="T68" id="Seg_758" s="T67">остаться-MOM-PST-3PL</ta>
            <ta e="T69" id="Seg_759" s="T68">и</ta>
            <ta e="T70" id="Seg_760" s="T69">скоро</ta>
            <ta e="T71" id="Seg_761" s="T70">так</ta>
            <ta e="T72" id="Seg_762" s="T71">жить-DUR-3PL</ta>
            <ta e="T73" id="Seg_763" s="T72">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_764" s="T0">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_765" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_766" s="T3">conj</ta>
            <ta e="T5" id="Seg_767" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_768" s="T5">adv</ta>
            <ta e="T7" id="Seg_769" s="T6">n-n:case</ta>
            <ta e="T9" id="Seg_770" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_771" s="T9">v-v:n.fin</ta>
            <ta e="T11" id="Seg_772" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_773" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_774" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_775" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_776" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_777" s="T15">adv</ta>
            <ta e="T17" id="Seg_778" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_779" s="T17">adv</ta>
            <ta e="T19" id="Seg_780" s="T18">dempro-n:case</ta>
            <ta e="T20" id="Seg_781" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_782" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_783" s="T21">adv</ta>
            <ta e="T23" id="Seg_784" s="T22">dempro-n:case</ta>
            <ta e="T24" id="Seg_785" s="T23">v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_786" s="T24">adv</ta>
            <ta e="T26" id="Seg_787" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_788" s="T26">dempro-n:case</ta>
            <ta e="T28" id="Seg_789" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_790" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_791" s="T29">dempro-n:case</ta>
            <ta e="T31" id="Seg_792" s="T30">pers</ta>
            <ta e="T32" id="Seg_793" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_794" s="T32">pers</ta>
            <ta e="T34" id="Seg_795" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_796" s="T34">conj</ta>
            <ta e="T36" id="Seg_797" s="T35">dempro-n:case</ta>
            <ta e="T37" id="Seg_798" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_799" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_800" s="T38">pers</ta>
            <ta e="T40" id="Seg_801" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_802" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_803" s="T41">adv</ta>
            <ta e="T43" id="Seg_804" s="T42">dempro-n:case</ta>
            <ta e="T74" id="Seg_805" s="T43">v-v:n-fin</ta>
            <ta e="T44" id="Seg_806" s="T74">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_807" s="T44">n-n:case.poss</ta>
            <ta e="T46" id="Seg_808" s="T45">adv</ta>
            <ta e="T47" id="Seg_809" s="T46">n-n:case</ta>
            <ta e="T49" id="Seg_810" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_811" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_812" s="T50">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_813" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_814" s="T54">v-v:n.fin</ta>
            <ta e="T56" id="Seg_815" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_816" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_817" s="T57">v-v&gt;v-v:pn</ta>
            <ta e="T59" id="Seg_818" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_819" s="T59">pers</ta>
            <ta e="T61" id="Seg_820" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_821" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_822" s="T62">adv</ta>
            <ta e="T64" id="Seg_823" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_824" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_825" s="T65">conj</ta>
            <ta e="T67" id="Seg_826" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_827" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_828" s="T68">conj</ta>
            <ta e="T70" id="Seg_829" s="T69">adv</ta>
            <ta e="T71" id="Seg_830" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_831" s="T71">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_832" s="T72">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_833" s="T0">v</ta>
            <ta e="T3" id="Seg_834" s="T2">n</ta>
            <ta e="T4" id="Seg_835" s="T3">conj</ta>
            <ta e="T5" id="Seg_836" s="T4">n</ta>
            <ta e="T6" id="Seg_837" s="T5">adv</ta>
            <ta e="T7" id="Seg_838" s="T6">n</ta>
            <ta e="T9" id="Seg_839" s="T8">v</ta>
            <ta e="T10" id="Seg_840" s="T9">v</ta>
            <ta e="T11" id="Seg_841" s="T10">v</ta>
            <ta e="T12" id="Seg_842" s="T11">n</ta>
            <ta e="T13" id="Seg_843" s="T12">v</ta>
            <ta e="T14" id="Seg_844" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_845" s="T14">v</ta>
            <ta e="T16" id="Seg_846" s="T15">adv</ta>
            <ta e="T17" id="Seg_847" s="T16">v</ta>
            <ta e="T18" id="Seg_848" s="T17">adv</ta>
            <ta e="T19" id="Seg_849" s="T18">dempro</ta>
            <ta e="T20" id="Seg_850" s="T19">v</ta>
            <ta e="T21" id="Seg_851" s="T20">n</ta>
            <ta e="T22" id="Seg_852" s="T21">adv</ta>
            <ta e="T23" id="Seg_853" s="T22">dempro</ta>
            <ta e="T24" id="Seg_854" s="T23">v</ta>
            <ta e="T25" id="Seg_855" s="T24">adv</ta>
            <ta e="T26" id="Seg_856" s="T25">v</ta>
            <ta e="T27" id="Seg_857" s="T26">dempro</ta>
            <ta e="T28" id="Seg_858" s="T27">n</ta>
            <ta e="T29" id="Seg_859" s="T28">v</ta>
            <ta e="T30" id="Seg_860" s="T29">dempro</ta>
            <ta e="T31" id="Seg_861" s="T30">pers</ta>
            <ta e="T32" id="Seg_862" s="T31">v</ta>
            <ta e="T33" id="Seg_863" s="T32">pers</ta>
            <ta e="T34" id="Seg_864" s="T33">n</ta>
            <ta e="T35" id="Seg_865" s="T34">conj</ta>
            <ta e="T36" id="Seg_866" s="T35">dempro</ta>
            <ta e="T37" id="Seg_867" s="T36">v</ta>
            <ta e="T38" id="Seg_868" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_869" s="T38">pers</ta>
            <ta e="T40" id="Seg_870" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_871" s="T40">v</ta>
            <ta e="T42" id="Seg_872" s="T41">adv</ta>
            <ta e="T43" id="Seg_873" s="T42">dempro</ta>
            <ta e="T74" id="Seg_874" s="T43">v</ta>
            <ta e="T44" id="Seg_875" s="T74">v</ta>
            <ta e="T45" id="Seg_876" s="T44">n</ta>
            <ta e="T46" id="Seg_877" s="T45">adv</ta>
            <ta e="T47" id="Seg_878" s="T46">n</ta>
            <ta e="T49" id="Seg_879" s="T48">v</ta>
            <ta e="T50" id="Seg_880" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_881" s="T50">v</ta>
            <ta e="T54" id="Seg_882" s="T53">v</ta>
            <ta e="T55" id="Seg_883" s="T54">v</ta>
            <ta e="T56" id="Seg_884" s="T55">v</ta>
            <ta e="T57" id="Seg_885" s="T56">dempro</ta>
            <ta e="T58" id="Seg_886" s="T57">v</ta>
            <ta e="T59" id="Seg_887" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_888" s="T59">pers</ta>
            <ta e="T61" id="Seg_889" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_890" s="T61">v</ta>
            <ta e="T63" id="Seg_891" s="T62">adv</ta>
            <ta e="T64" id="Seg_892" s="T63">v</ta>
            <ta e="T65" id="Seg_893" s="T64">v</ta>
            <ta e="T66" id="Seg_894" s="T65">conj</ta>
            <ta e="T67" id="Seg_895" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_896" s="T67">v</ta>
            <ta e="T69" id="Seg_897" s="T68">conj</ta>
            <ta e="T70" id="Seg_898" s="T69">adv</ta>
            <ta e="T71" id="Seg_899" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_900" s="T71">v</ta>
            <ta e="T73" id="Seg_901" s="T72">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_902" s="T2">np.h:E</ta>
            <ta e="T5" id="Seg_903" s="T4">np.h:E</ta>
            <ta e="T6" id="Seg_904" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_905" s="T6">np.h:A</ta>
            <ta e="T9" id="Seg_906" s="T8">0.1.h:A</ta>
            <ta e="T11" id="Seg_907" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_908" s="T11">np:G</ta>
            <ta e="T13" id="Seg_909" s="T12">0.3.h:A</ta>
            <ta e="T15" id="Seg_910" s="T14">0.1.h:A</ta>
            <ta e="T17" id="Seg_911" s="T16">0.1.h:E</ta>
            <ta e="T18" id="Seg_912" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_913" s="T18">pro.h:A</ta>
            <ta e="T21" id="Seg_914" s="T20">np:G</ta>
            <ta e="T22" id="Seg_915" s="T21">adv:Time</ta>
            <ta e="T23" id="Seg_916" s="T22">pro.h:E</ta>
            <ta e="T26" id="Seg_917" s="T25">0.1.h:A</ta>
            <ta e="T27" id="Seg_918" s="T26">pro.h:R</ta>
            <ta e="T29" id="Seg_919" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_920" s="T29">pro:G</ta>
            <ta e="T31" id="Seg_921" s="T30">pro.h:A</ta>
            <ta e="T33" id="Seg_922" s="T32">pro:G</ta>
            <ta e="T36" id="Seg_923" s="T35">pro.h:A</ta>
            <ta e="T39" id="Seg_924" s="T38">pro.h:A</ta>
            <ta e="T42" id="Seg_925" s="T41">adv:Time</ta>
            <ta e="T43" id="Seg_926" s="T42">pro.h:A</ta>
            <ta e="T45" id="Seg_927" s="T44">np:G</ta>
            <ta e="T46" id="Seg_928" s="T45">adv:Time</ta>
            <ta e="T47" id="Seg_929" s="T46">np.h:A</ta>
            <ta e="T49" id="Seg_930" s="T48">0.1.h:A</ta>
            <ta e="T51" id="Seg_931" s="T50">0.1.h:A</ta>
            <ta e="T54" id="Seg_932" s="T53">0.1.h:A</ta>
            <ta e="T56" id="Seg_933" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_934" s="T56">pro.h:A</ta>
            <ta e="T60" id="Seg_935" s="T59">pro.h:A</ta>
            <ta e="T63" id="Seg_936" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_937" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_938" s="T64">0.3.h:A</ta>
            <ta e="T68" id="Seg_939" s="T67">0.3.h:Th</ta>
            <ta e="T72" id="Seg_940" s="T71">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_941" s="T0">v:pred</ta>
            <ta e="T3" id="Seg_942" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_943" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_944" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_945" s="T8">v:pred 0.1.h:S</ta>
            <ta e="T11" id="Seg_946" s="T10">v:pred 0.3.h:S</ta>
            <ta e="T13" id="Seg_947" s="T12">v:pred 0.3.h:S</ta>
            <ta e="T14" id="Seg_948" s="T13">ptcl.neg</ta>
            <ta e="T15" id="Seg_949" s="T14">v:pred 0.1.h:S</ta>
            <ta e="T17" id="Seg_950" s="T16">v:pred 0.1.h:S</ta>
            <ta e="T19" id="Seg_951" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_952" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_953" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_954" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_955" s="T25">v:pred 0.1.h:S</ta>
            <ta e="T29" id="Seg_956" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_957" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_958" s="T31">v:pred</ta>
            <ta e="T36" id="Seg_959" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_960" s="T36">v:pred</ta>
            <ta e="T39" id="Seg_961" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_962" s="T39">ptcl.neg</ta>
            <ta e="T41" id="Seg_963" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_964" s="T42">pro.h:S</ta>
            <ta e="T74" id="Seg_965" s="T43">conv:pred</ta>
            <ta e="T44" id="Seg_966" s="T74">v:pred</ta>
            <ta e="T47" id="Seg_967" s="T46">np.h:S</ta>
            <ta e="T49" id="Seg_968" s="T48">v:pred 0.1.h:S</ta>
            <ta e="T50" id="Seg_969" s="T49">ptcl.neg</ta>
            <ta e="T51" id="Seg_970" s="T50">v:pred 0.1.h:S</ta>
            <ta e="T54" id="Seg_971" s="T53">v:pred 0.1.h:S</ta>
            <ta e="T56" id="Seg_972" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_973" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_974" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_975" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_976" s="T60">ptcl.neg</ta>
            <ta e="T62" id="Seg_977" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_978" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_979" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_980" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_981" s="T71">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_982" s="T2">RUS:core</ta>
            <ta e="T4" id="Seg_983" s="T3">RUS:gram</ta>
            <ta e="T5" id="Seg_984" s="T4">RUS:core</ta>
            <ta e="T7" id="Seg_985" s="T6">RUS:core</ta>
            <ta e="T12" id="Seg_986" s="T11">RUS:core</ta>
            <ta e="T25" id="Seg_987" s="T24">RUS:mod</ta>
            <ta e="T35" id="Seg_988" s="T34">RUS:gram</ta>
            <ta e="T38" id="Seg_989" s="T37">TURK:disc</ta>
            <ta e="T47" id="Seg_990" s="T46">RUS:core</ta>
            <ta e="T59" id="Seg_991" s="T58">TURK:disc</ta>
            <ta e="T66" id="Seg_992" s="T65">RUS:gram</ta>
            <ta e="T69" id="Seg_993" s="T68">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T3" id="Seg_994" s="T2">medVins</ta>
            <ta e="T7" id="Seg_995" s="T6">medVins</ta>
            <ta e="T12" id="Seg_996" s="T11">Csub</ta>
            <ta e="T47" id="Seg_997" s="T46">finVdel finCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_998" s="T0">Жили журавль и цапля.</ta>
            <ta e="T10" id="Seg_999" s="T5">Журавль: "Пойду посватаюсь к ней".</ta>
            <ta e="T12" id="Seg_1000" s="T10">Пришел к цапле.</ta>
            <ta e="T15" id="Seg_1001" s="T12">[Она] говорит: "Не пойду!</ta>
            <ta e="T17" id="Seg_1002" s="T15">Одна буду жить".</ta>
            <ta e="T21" id="Seg_1003" s="T17">Тогда он пошел домой.</ta>
            <ta e="T24" id="Seg_1004" s="T21">Потом она думает:</ta>
            <ta e="T28" id="Seg_1005" s="T24">"А все-таки я выйду за него замуж".</ta>
            <ta e="T30" id="Seg_1006" s="T28">Пошла к нему.</ta>
            <ta e="T34" id="Seg_1007" s="T30">"Я выйду за тебя замуж".</ta>
            <ta e="T41" id="Seg_1008" s="T34">А он говорит: "Нет, я не возьму [тебя]!"</ta>
            <ta e="T45" id="Seg_1009" s="T41">Тогда она пошла домой.</ta>
            <ta e="T51" id="Seg_1010" s="T45">Потом журавль: "Зря я сказал, что не возьму".</ta>
            <ta e="T55" id="Seg_1011" s="T51">Пойду посватаюсь.</ta>
            <ta e="T56" id="Seg_1012" s="T55">Пришел.</ta>
            <ta e="T62" id="Seg_1013" s="T56">Она говорит: "Нет, я не пойду".</ta>
            <ta e="T65" id="Seg_1014" s="T62">Они ходили, ходили.</ta>
            <ta e="T68" id="Seg_1015" s="T65">И так и остались.</ta>
            <ta e="T72" id="Seg_1016" s="T68">И сейчас так живут.</ta>
            <ta e="T73" id="Seg_1017" s="T72">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1018" s="T0">There lived a crane and a heron.</ta>
            <ta e="T10" id="Seg_1019" s="T5">Then the crane: "I'll go and ask [her] in marriage."</ta>
            <ta e="T12" id="Seg_1020" s="T10">He came to the heron.</ta>
            <ta e="T15" id="Seg_1021" s="T12">[She] says: "I will not go!</ta>
            <ta e="T17" id="Seg_1022" s="T15">I will live alone."</ta>
            <ta e="T21" id="Seg_1023" s="T17">Then he went home.</ta>
            <ta e="T24" id="Seg_1024" s="T21">Then she thinks:</ta>
            <ta e="T28" id="Seg_1025" s="T24">"However, I will marry him."</ta>
            <ta e="T30" id="Seg_1026" s="T28">She went to him.</ta>
            <ta e="T34" id="Seg_1027" s="T30">"I will marry you!"</ta>
            <ta e="T41" id="Seg_1028" s="T34">But he says: "No, I will not take [you]!"</ta>
            <ta e="T45" id="Seg_1029" s="T41">Then she went home.</ta>
            <ta e="T51" id="Seg_1030" s="T45">Then the crane: "I shouldn't say that I won't take [her].</ta>
            <ta e="T55" id="Seg_1031" s="T51">I will go to propose her to marry me.</ta>
            <ta e="T56" id="Seg_1032" s="T55">He came.</ta>
            <ta e="T62" id="Seg_1033" s="T56">She says: "No, I will not go."</ta>
            <ta e="T65" id="Seg_1034" s="T62">Then they were going [and] going [to one another].</ta>
            <ta e="T68" id="Seg_1035" s="T65">And so they stayed.</ta>
            <ta e="T72" id="Seg_1036" s="T68">And now they are still living like this.</ta>
            <ta e="T73" id="Seg_1037" s="T72">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1038" s="T0">Es lebten ein Kranich und ein Reiher.</ta>
            <ta e="T10" id="Seg_1039" s="T5">Dann der Kranich: "Ich werde gehen, um [ihr] vorzuschlagen, mich zu heiraten!"</ta>
            <ta e="T12" id="Seg_1040" s="T10">Er kam zum Reiher.</ta>
            <ta e="T15" id="Seg_1041" s="T12">[Sie] sagt: "Ich werde nicht gehen!</ta>
            <ta e="T17" id="Seg_1042" s="T15">Ich werde alleine leben."</ta>
            <ta e="T21" id="Seg_1043" s="T17">Dann ging er nach Hause.</ta>
            <ta e="T24" id="Seg_1044" s="T21">Dann denkt sie:</ta>
            <ta e="T28" id="Seg_1045" s="T24">"Wie auch immer, ich werde ihn heiraten."</ta>
            <ta e="T30" id="Seg_1046" s="T28">Sie ging zu ihm.</ta>
            <ta e="T34" id="Seg_1047" s="T30">"Ich werde dich heiraten!"</ta>
            <ta e="T41" id="Seg_1048" s="T34">Aber er sagt: "Nein, ich werde [dich] nicht nehmen!"</ta>
            <ta e="T45" id="Seg_1049" s="T41">Dann ging sie nach Hause.</ta>
            <ta e="T51" id="Seg_1050" s="T45">Dann der Kranich: "Ich sollte nicht sagen, dass ich [sie] nicht nehme.</ta>
            <ta e="T55" id="Seg_1051" s="T51">Ich werde gehen, um ihr vorzuschlagen, mich zu heiraten.</ta>
            <ta e="T56" id="Seg_1052" s="T55">Er kam.</ta>
            <ta e="T62" id="Seg_1053" s="T56">Sie sagt: "Nein, ich werde nicht gehen."</ta>
            <ta e="T65" id="Seg_1054" s="T62">Dann gingen und gingen sie [zu einander].</ta>
            <ta e="T68" id="Seg_1055" s="T65">Und so blieben sie.</ta>
            <ta e="T72" id="Seg_1056" s="T68">Und jetzt leben sie immer noch so.</ta>
            <ta e="T73" id="Seg_1057" s="T72">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_1058" s="T0">[GVY:] Crane is masculine and heron is feminine in Russian, here they will be referred to in the translation as "he" and "she", respectively. The original text of the tale see e.g. http://hyaenidae.narod.ru/story1/028.html</ta>
            <ta e="T51" id="Seg_1059" s="T45">[GVY:] here pronounced [žuraf]</ta>
            <ta e="T68" id="Seg_1060" s="T65">[AAV] maːluʔpiʔi</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T74" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
