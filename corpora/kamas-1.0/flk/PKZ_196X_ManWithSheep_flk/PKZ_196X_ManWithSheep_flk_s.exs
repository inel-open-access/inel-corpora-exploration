<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8565A686-2F37-B7A7-E391-9D5F0624ACF3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ManWithSheep_flk.wav" />
         <referenced-file url="PKZ_196X_ManWithSheep_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ManWithSheep_flk\PKZ_196X_ManWithSheep_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">121</ud-information>
            <ud-information attribute-name="# HIAT:w">85</ud-information>
            <ud-information attribute-name="# e">84</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1681" time="0.0" type="appl" />
         <tli id="T1682" time="0.604" type="appl" />
         <tli id="T1683" time="1.207" type="appl" />
         <tli id="T1684" time="1.811" type="appl" />
         <tli id="T1685" time="2.414" type="appl" />
         <tli id="T1686" time="2.984669815137593" />
         <tli id="T1687" time="3.824" type="appl" />
         <tli id="T1688" time="4.63" type="appl" />
         <tli id="T1689" time="5.436" type="appl" />
         <tli id="T1690" time="6.241" type="appl" />
         <tli id="T1691" time="7.047" type="appl" />
         <tli id="T1692" time="7.853" type="appl" />
         <tli id="T1693" time="8.659" type="appl" />
         <tli id="T1694" time="9.474" type="appl" />
         <tli id="T1695" time="10.156" type="appl" />
         <tli id="T1696" time="10.838" type="appl" />
         <tli id="T1697" time="11.665587658177989" />
         <tli id="T1698" time="12.164" type="appl" />
         <tli id="T1699" time="12.681" type="appl" />
         <tli id="T1700" time="13.198" type="appl" />
         <tli id="T1701" time="13.714" type="appl" />
         <tli id="T1702" time="14.231" type="appl" />
         <tli id="T1703" time="14.748" type="appl" />
         <tli id="T1704" time="15.551894860873853" />
         <tli id="T1705" time="16.022" type="appl" />
         <tli id="T1706" time="16.563" type="appl" />
         <tli id="T1707" time="17.104" type="appl" />
         <tli id="T1708" time="17.646" type="appl" />
         <tli id="T1709" time="18.188" type="appl" />
         <tli id="T1710" time="18.92491620660989" />
         <tli id="T1711" time="19.458" type="appl" />
         <tli id="T1712" time="20.187" type="appl" />
         <tli id="T1713" time="20.916" type="appl" />
         <tli id="T1714" time="22.251275201713216" />
         <tli id="T1715" time="22.943" type="appl" />
         <tli id="T1716" time="23.56" type="appl" />
         <tli id="T1717" time="24.177" type="appl" />
         <tli id="T1718" time="24.794" type="appl" />
         <tli id="T1719" time="25.41" type="appl" />
         <tli id="T1720" time="26.027" type="appl" />
         <tli id="T1721" time="26.644" type="appl" />
         <tli id="T1722" time="27.261" type="appl" />
         <tli id="T1723" time="27.734" type="appl" />
         <tli id="T1724" time="28.206" type="appl" />
         <tli id="T1725" time="28.679" type="appl" />
         <tli id="T1726" time="29.152" type="appl" />
         <tli id="T1727" time="29.625" type="appl" />
         <tli id="T1728" time="30.097" type="appl" />
         <tli id="T1729" time="31.27710702409778" />
         <tli id="T1730" time="31.752" type="appl" />
         <tli id="T1731" time="32.183" type="appl" />
         <tli id="T1732" time="32.615" type="appl" />
         <tli id="T1733" time="33.046" type="appl" />
         <tli id="T1734" time="33.477" type="appl" />
         <tli id="T1735" time="33.908" type="appl" />
         <tli id="T1736" time="34.34" type="appl" />
         <tli id="T1737" time="34.771" type="appl" />
         <tli id="T1738" time="35.37672782968605" />
         <tli id="T1739" time="36.8" type="appl" />
         <tli id="T1740" time="37.67" type="appl" />
         <tli id="T1741" time="38.43" type="appl" />
         <tli id="T1742" time="39.191" type="appl" />
         <tli id="T1743" time="39.593" type="appl" />
         <tli id="T1744" time="39.995" type="appl" />
         <tli id="T1745" time="40.398" type="appl" />
         <tli id="T1746" time="40.8" type="appl" />
         <tli id="T1747" time="41.202" type="appl" />
         <tli id="T1748" time="41.604" type="appl" />
         <tli id="T1749" time="42.3" type="appl" />
         <tli id="T1750" time="42.717" type="appl" />
         <tli id="T1751" time="43.133" type="appl" />
         <tli id="T1752" time="43.55" type="appl" />
         <tli id="T1753" time="43.966" type="appl" />
         <tli id="T1754" time="44.382" type="appl" />
         <tli id="T1755" time="44.799" type="appl" />
         <tli id="T1756" time="45.215" type="appl" />
         <tli id="T1757" time="45.982" type="appl" />
         <tli id="T1758" time="46.626" type="appl" />
         <tli id="T1759" time="47.269" type="appl" />
         <tli id="T1760" time="47.874" type="appl" />
         <tli id="T1761" time="48.342" type="appl" />
         <tli id="T1762" time="48.811" type="appl" />
         <tli id="T1763" time="49.28" type="appl" />
         <tli id="T1764" time="49.748" type="appl" />
         <tli id="T1765" time="50.217" type="appl" />
         <tli id="T0" time="50.242" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T1743" start="T1742">
            <tli id="T1742.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1765" id="Seg_0" n="sc" s="T1681">
               <ts e="T1686" id="Seg_2" n="HIAT:u" s="T1681">
                  <ts e="T1682" id="Seg_4" n="HIAT:w" s="T1681">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1683" id="Seg_7" n="HIAT:w" s="T1682">kuzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1684" id="Seg_10" n="HIAT:w" s="T1683">iʔgö</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1685" id="Seg_13" n="HIAT:w" s="T1684">ular</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1686" id="Seg_16" n="HIAT:w" s="T1685">ibiʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1693" id="Seg_20" n="HIAT:u" s="T1686">
                  <ts e="T1687" id="Seg_22" n="HIAT:w" s="T1686">A</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1688" id="Seg_25" n="HIAT:w" s="T1687">onʼiʔ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T1689" id="Seg_29" n="HIAT:w" s="T1688">u-</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1690" id="Seg_32" n="HIAT:w" s="T1689">onʼiʔ</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1691" id="Seg_36" n="HIAT:w" s="T1690">onʼiʔ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1692" id="Seg_39" n="HIAT:w" s="T1691">ular</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1693" id="Seg_42" n="HIAT:w" s="T1692">ibi</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1697" id="Seg_46" n="HIAT:u" s="T1693">
                  <ts e="T1694" id="Seg_48" n="HIAT:w" s="T1693">Dĭʔnə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1695" id="Seg_51" n="HIAT:w" s="T1694">šobi</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1696" id="Seg_54" n="HIAT:w" s="T1695">jadajlaʔ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1697" id="Seg_57" n="HIAT:w" s="T1696">kuza</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1704" id="Seg_61" n="HIAT:u" s="T1697">
                  <ts e="T1698" id="Seg_63" n="HIAT:w" s="T1697">Da</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1699" id="Seg_66" n="HIAT:w" s="T1698">kambi</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1700" id="Seg_69" n="HIAT:w" s="T1699">da</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1701" id="Seg_72" n="HIAT:w" s="T1700">dĭ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1702" id="Seg_75" n="HIAT:w" s="T1701">ibi</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1703" id="Seg_78" n="HIAT:w" s="T1702">onʼiʔ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1704" id="Seg_81" n="HIAT:w" s="T1703">ulardə</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1710" id="Seg_85" n="HIAT:u" s="T1704">
                  <ts e="T1705" id="Seg_87" n="HIAT:w" s="T1704">Băʔpi</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1706" id="Seg_90" n="HIAT:w" s="T1705">da</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1707" id="Seg_93" n="HIAT:w" s="T1706">davaj</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1708" id="Seg_96" n="HIAT:w" s="T1707">bădəsʼtə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1709" id="Seg_99" n="HIAT:w" s="T1708">dĭ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1710" id="Seg_102" n="HIAT:w" s="T1709">kuzam</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1714" id="Seg_106" n="HIAT:u" s="T1710">
                  <ts e="T1711" id="Seg_108" n="HIAT:w" s="T1710">Dĭgəttə</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1712" id="Seg_111" n="HIAT:w" s="T1711">šobi</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1713" id="Seg_114" n="HIAT:w" s="T1712">iššo</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1714" id="Seg_117" n="HIAT:w" s="T1713">kuza</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1722" id="Seg_121" n="HIAT:u" s="T1714">
                  <ts e="T1715" id="Seg_123" n="HIAT:w" s="T1714">Ĭmbi</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1716" id="Seg_126" n="HIAT:w" s="T1715">dĭ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1717" id="Seg_129" n="HIAT:w" s="T1716">kuzaziʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1718" id="Seg_132" n="HIAT:w" s="T1717">azittə</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1719" id="Seg_136" n="HIAT:w" s="T1718">dĭn</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1720" id="Seg_139" n="HIAT:w" s="T1719">iʔgö</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1721" id="Seg_142" n="HIAT:w" s="T1720">ularzaŋdə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1722" id="Seg_145" n="HIAT:w" s="T1721">ibiʔi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1729" id="Seg_149" n="HIAT:u" s="T1722">
                  <ts e="T1723" id="Seg_151" n="HIAT:w" s="T1722">A</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1724" id="Seg_154" n="HIAT:w" s="T1723">dĭ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1725" id="Seg_157" n="HIAT:w" s="T1724">kambi</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1726" id="Seg_160" n="HIAT:w" s="T1725">da</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1727" id="Seg_163" n="HIAT:w" s="T1726">ibi</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1728" id="Seg_166" n="HIAT:w" s="T1727">onʼiʔ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1729" id="Seg_169" n="HIAT:w" s="T1728">ular</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1738" id="Seg_173" n="HIAT:u" s="T1729">
                  <ts e="T1730" id="Seg_175" n="HIAT:w" s="T1729">Kuzagən</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1731" id="Seg_178" n="HIAT:w" s="T1730">onʼiʔ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1732" id="Seg_181" n="HIAT:w" s="T1731">ular</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1733" id="Seg_184" n="HIAT:w" s="T1732">ibi</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1734" id="Seg_188" n="HIAT:w" s="T1733">dĭ</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1735" id="Seg_191" n="HIAT:w" s="T1734">ibi</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1736" id="Seg_194" n="HIAT:w" s="T1735">da</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1737" id="Seg_197" n="HIAT:w" s="T1736">băʔpi</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_199" n="HIAT:ip">(</nts>
                  <ts e="T1738" id="Seg_201" n="HIAT:w" s="T1737">dĭ</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1739" id="Seg_206" n="HIAT:u" s="T1738">
                  <ts e="T1739" id="Seg_208" n="HIAT:w" s="T1738">Amnuʔpiʔi</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1742" id="Seg_212" n="HIAT:u" s="T1739">
                  <ts e="T1740" id="Seg_214" n="HIAT:w" s="T1739">Nada</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1741" id="Seg_217" n="HIAT:w" s="T1740">dĭm</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1742" id="Seg_220" n="HIAT:w" s="T1741">kuʔsittə</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1748" id="Seg_224" n="HIAT:u" s="T1742">
                  <ts e="T1742.tx.1" id="Seg_226" n="HIAT:w" s="T1742">Na</ts>
                  <nts id="Seg_227" n="HIAT:ip">_</nts>
                  <ts e="T1743" id="Seg_229" n="HIAT:w" s="T1742.tx.1">što</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <ts e="T1744" id="Seg_233" n="HIAT:w" s="T1743">tă-</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1745" id="Seg_236" n="HIAT:w" s="T1744">dĭ-</ts>
                  <nts id="Seg_237" n="HIAT:ip">)</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1746" id="Seg_240" n="HIAT:w" s="T1745">dĭ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ts e="T1747" id="Seg_244" n="HIAT:w" s="T1746">dăreʔ</ts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1748" id="Seg_248" n="HIAT:w" s="T1747">abi</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1756" id="Seg_252" n="HIAT:u" s="T1748">
                  <ts e="T1749" id="Seg_254" n="HIAT:w" s="T1748">A</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1750" id="Seg_257" n="HIAT:w" s="T1749">dĭ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1751" id="Seg_260" n="HIAT:w" s="T1750">măndə:</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_262" n="HIAT:ip">"</nts>
                  <nts id="Seg_263" n="HIAT:ip">(</nts>
                  <ts e="T1752" id="Seg_265" n="HIAT:w" s="T1751">Tăn=</ts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1753" id="Seg_269" n="HIAT:w" s="T1752">Dĭ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1754" id="Seg_272" n="HIAT:w" s="T1753">tăn</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_274" n="HIAT:ip">(</nts>
                  <ts e="T1755" id="Seg_276" n="HIAT:w" s="T1754">dăreʔ</ts>
                  <nts id="Seg_277" n="HIAT:ip">)</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1756" id="Seg_280" n="HIAT:w" s="T1755">abial</ts>
                  <nts id="Seg_281" n="HIAT:ip">"</nts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1759" id="Seg_285" n="HIAT:u" s="T1756">
                  <ts e="T1757" id="Seg_287" n="HIAT:w" s="T1756">Dĭ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1758" id="Seg_290" n="HIAT:w" s="T1757">davaj</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1759" id="Seg_293" n="HIAT:w" s="T1758">dʼorzittə</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1765" id="Seg_297" n="HIAT:u" s="T1759">
                  <nts id="Seg_298" n="HIAT:ip">"</nts>
                  <ts e="T1760" id="Seg_300" n="HIAT:w" s="T1759">Iʔ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1761" id="Seg_303" n="HIAT:w" s="T1760">dʼoraʔ</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_306" n="HIAT:ip">(</nts>
                  <ts e="T1762" id="Seg_308" n="HIAT:w" s="T1761">dĭ</ts>
                  <nts id="Seg_309" n="HIAT:ip">)</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1763" id="Seg_312" n="HIAT:w" s="T1762">tăn</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1764" id="Seg_315" n="HIAT:w" s="T1763">dăra</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1765" id="Seg_318" n="HIAT:w" s="T1764">abial</ts>
                  <nts id="Seg_319" n="HIAT:ip">"</nts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1765" id="Seg_322" n="sc" s="T1681">
               <ts e="T1682" id="Seg_324" n="e" s="T1681">Onʼiʔ </ts>
               <ts e="T1683" id="Seg_326" n="e" s="T1682">kuzan </ts>
               <ts e="T1684" id="Seg_328" n="e" s="T1683">iʔgö </ts>
               <ts e="T1685" id="Seg_330" n="e" s="T1684">ular </ts>
               <ts e="T1686" id="Seg_332" n="e" s="T1685">ibiʔi. </ts>
               <ts e="T1687" id="Seg_334" n="e" s="T1686">A </ts>
               <ts e="T1688" id="Seg_336" n="e" s="T1687">onʼiʔ </ts>
               <ts e="T1689" id="Seg_338" n="e" s="T1688">(u- </ts>
               <ts e="T1690" id="Seg_340" n="e" s="T1689">onʼiʔ) </ts>
               <ts e="T1691" id="Seg_342" n="e" s="T1690">onʼiʔ </ts>
               <ts e="T1692" id="Seg_344" n="e" s="T1691">ular </ts>
               <ts e="T1693" id="Seg_346" n="e" s="T1692">ibi. </ts>
               <ts e="T1694" id="Seg_348" n="e" s="T1693">Dĭʔnə </ts>
               <ts e="T1695" id="Seg_350" n="e" s="T1694">šobi </ts>
               <ts e="T1696" id="Seg_352" n="e" s="T1695">jadajlaʔ </ts>
               <ts e="T1697" id="Seg_354" n="e" s="T1696">kuza. </ts>
               <ts e="T1698" id="Seg_356" n="e" s="T1697">Da </ts>
               <ts e="T1699" id="Seg_358" n="e" s="T1698">kambi </ts>
               <ts e="T1700" id="Seg_360" n="e" s="T1699">da </ts>
               <ts e="T1701" id="Seg_362" n="e" s="T1700">dĭ </ts>
               <ts e="T1702" id="Seg_364" n="e" s="T1701">ibi </ts>
               <ts e="T1703" id="Seg_366" n="e" s="T1702">onʼiʔ </ts>
               <ts e="T1704" id="Seg_368" n="e" s="T1703">ulardə. </ts>
               <ts e="T1705" id="Seg_370" n="e" s="T1704">Băʔpi </ts>
               <ts e="T1706" id="Seg_372" n="e" s="T1705">da </ts>
               <ts e="T1707" id="Seg_374" n="e" s="T1706">davaj </ts>
               <ts e="T1708" id="Seg_376" n="e" s="T1707">bădəsʼtə </ts>
               <ts e="T1709" id="Seg_378" n="e" s="T1708">dĭ </ts>
               <ts e="T1710" id="Seg_380" n="e" s="T1709">kuzam. </ts>
               <ts e="T1711" id="Seg_382" n="e" s="T1710">Dĭgəttə </ts>
               <ts e="T1712" id="Seg_384" n="e" s="T1711">šobi </ts>
               <ts e="T1713" id="Seg_386" n="e" s="T1712">iššo </ts>
               <ts e="T1714" id="Seg_388" n="e" s="T1713">kuza. </ts>
               <ts e="T1715" id="Seg_390" n="e" s="T1714">Ĭmbi </ts>
               <ts e="T1716" id="Seg_392" n="e" s="T1715">dĭ </ts>
               <ts e="T1717" id="Seg_394" n="e" s="T1716">kuzaziʔ </ts>
               <ts e="T1718" id="Seg_396" n="e" s="T1717">azittə, </ts>
               <ts e="T1719" id="Seg_398" n="e" s="T1718">dĭn </ts>
               <ts e="T1720" id="Seg_400" n="e" s="T1719">iʔgö </ts>
               <ts e="T1721" id="Seg_402" n="e" s="T1720">ularzaŋdə </ts>
               <ts e="T1722" id="Seg_404" n="e" s="T1721">ibiʔi. </ts>
               <ts e="T1723" id="Seg_406" n="e" s="T1722">A </ts>
               <ts e="T1724" id="Seg_408" n="e" s="T1723">dĭ </ts>
               <ts e="T1725" id="Seg_410" n="e" s="T1724">kambi </ts>
               <ts e="T1726" id="Seg_412" n="e" s="T1725">da </ts>
               <ts e="T1727" id="Seg_414" n="e" s="T1726">ibi </ts>
               <ts e="T1728" id="Seg_416" n="e" s="T1727">onʼiʔ </ts>
               <ts e="T1729" id="Seg_418" n="e" s="T1728">ular. </ts>
               <ts e="T1730" id="Seg_420" n="e" s="T1729">Kuzagən </ts>
               <ts e="T1731" id="Seg_422" n="e" s="T1730">onʼiʔ </ts>
               <ts e="T1732" id="Seg_424" n="e" s="T1731">ular </ts>
               <ts e="T1733" id="Seg_426" n="e" s="T1732">ibi, </ts>
               <ts e="T1734" id="Seg_428" n="e" s="T1733">dĭ </ts>
               <ts e="T1735" id="Seg_430" n="e" s="T1734">ibi </ts>
               <ts e="T1736" id="Seg_432" n="e" s="T1735">da </ts>
               <ts e="T1737" id="Seg_434" n="e" s="T1736">băʔpi </ts>
               <ts e="T1738" id="Seg_436" n="e" s="T1737">(dĭ). </ts>
               <ts e="T1739" id="Seg_438" n="e" s="T1738">Amnuʔpiʔi. </ts>
               <ts e="T1740" id="Seg_440" n="e" s="T1739">Nada </ts>
               <ts e="T1741" id="Seg_442" n="e" s="T1740">dĭm </ts>
               <ts e="T1742" id="Seg_444" n="e" s="T1741">kuʔsittə. </ts>
               <ts e="T1743" id="Seg_446" n="e" s="T1742">Na_što </ts>
               <ts e="T1744" id="Seg_448" n="e" s="T1743">(tă- </ts>
               <ts e="T1745" id="Seg_450" n="e" s="T1744">dĭ-) </ts>
               <ts e="T1746" id="Seg_452" n="e" s="T1745">dĭ </ts>
               <ts e="T1747" id="Seg_454" n="e" s="T1746">(dăreʔ) </ts>
               <ts e="T1748" id="Seg_456" n="e" s="T1747">abi. </ts>
               <ts e="T1749" id="Seg_458" n="e" s="T1748">A </ts>
               <ts e="T1750" id="Seg_460" n="e" s="T1749">dĭ </ts>
               <ts e="T1751" id="Seg_462" n="e" s="T1750">măndə: </ts>
               <ts e="T1752" id="Seg_464" n="e" s="T1751">"(Tăn=) </ts>
               <ts e="T1753" id="Seg_466" n="e" s="T1752">Dĭ </ts>
               <ts e="T1754" id="Seg_468" n="e" s="T1753">tăn </ts>
               <ts e="T1755" id="Seg_470" n="e" s="T1754">(dăreʔ) </ts>
               <ts e="T1756" id="Seg_472" n="e" s="T1755">abial". </ts>
               <ts e="T1757" id="Seg_474" n="e" s="T1756">Dĭ </ts>
               <ts e="T1758" id="Seg_476" n="e" s="T1757">davaj </ts>
               <ts e="T1759" id="Seg_478" n="e" s="T1758">dʼorzittə. </ts>
               <ts e="T1760" id="Seg_480" n="e" s="T1759">"Iʔ </ts>
               <ts e="T1761" id="Seg_482" n="e" s="T1760">dʼoraʔ, </ts>
               <ts e="T1762" id="Seg_484" n="e" s="T1761">(dĭ) </ts>
               <ts e="T1763" id="Seg_486" n="e" s="T1762">tăn </ts>
               <ts e="T1764" id="Seg_488" n="e" s="T1763">dăra </ts>
               <ts e="T1765" id="Seg_490" n="e" s="T1764">abial". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1686" id="Seg_491" s="T1681">PKZ_196X_ManWithSheep_flk.001 (001)</ta>
            <ta e="T1693" id="Seg_492" s="T1686">PKZ_196X_ManWithSheep_flk.002 (002)</ta>
            <ta e="T1697" id="Seg_493" s="T1693">PKZ_196X_ManWithSheep_flk.003 (003)</ta>
            <ta e="T1704" id="Seg_494" s="T1697">PKZ_196X_ManWithSheep_flk.004 (004)</ta>
            <ta e="T1710" id="Seg_495" s="T1704">PKZ_196X_ManWithSheep_flk.005 (005)</ta>
            <ta e="T1714" id="Seg_496" s="T1710">PKZ_196X_ManWithSheep_flk.006 (006)</ta>
            <ta e="T1722" id="Seg_497" s="T1714">PKZ_196X_ManWithSheep_flk.007 (007)</ta>
            <ta e="T1729" id="Seg_498" s="T1722">PKZ_196X_ManWithSheep_flk.008 (008)</ta>
            <ta e="T1738" id="Seg_499" s="T1729">PKZ_196X_ManWithSheep_flk.009 (009)</ta>
            <ta e="T1739" id="Seg_500" s="T1738">PKZ_196X_ManWithSheep_flk.010 (010)</ta>
            <ta e="T1742" id="Seg_501" s="T1739">PKZ_196X_ManWithSheep_flk.011 (011)</ta>
            <ta e="T1748" id="Seg_502" s="T1742">PKZ_196X_ManWithSheep_flk.012 (012)</ta>
            <ta e="T1756" id="Seg_503" s="T1748">PKZ_196X_ManWithSheep_flk.013 (013)</ta>
            <ta e="T1759" id="Seg_504" s="T1756">PKZ_196X_ManWithSheep_flk.014 (014)</ta>
            <ta e="T1765" id="Seg_505" s="T1759">PKZ_196X_ManWithSheep_flk.015 (015)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1686" id="Seg_506" s="T1681">Onʼiʔ kuzan iʔgö ular ibiʔi. </ta>
            <ta e="T1693" id="Seg_507" s="T1686">A onʼiʔ (u- onʼiʔ) onʼiʔ ular ibi. </ta>
            <ta e="T1697" id="Seg_508" s="T1693">Dĭʔnə šobi jadajlaʔ kuza. </ta>
            <ta e="T1704" id="Seg_509" s="T1697">Da kambi da dĭ ibi onʼiʔ ulardə. </ta>
            <ta e="T1710" id="Seg_510" s="T1704">Băʔpi da davaj bădəsʼtə dĭ kuzam. </ta>
            <ta e="T1714" id="Seg_511" s="T1710">Dĭgəttə šobi iššo kuza. </ta>
            <ta e="T1722" id="Seg_512" s="T1714">Ĭmbi dĭ kuzaziʔ azittə, dĭn iʔgö ularzaŋdə ibiʔi. </ta>
            <ta e="T1729" id="Seg_513" s="T1722">A dĭ kambi da ibi onʼiʔ ular. </ta>
            <ta e="T1738" id="Seg_514" s="T1729">Kuzagən onʼiʔ ular ibi, dĭ ibi da băʔpi (dĭ). </ta>
            <ta e="T1739" id="Seg_515" s="T1738">Amnuʔpiʔi. </ta>
            <ta e="T1742" id="Seg_516" s="T1739">Nada dĭm kuʔsittə. </ta>
            <ta e="T1748" id="Seg_517" s="T1742">Na što (tă- dĭ-) dĭ (dăreʔ) abi. </ta>
            <ta e="T1756" id="Seg_518" s="T1748">A dĭ măndə: "(Tăn=) Dĭ tăn (dăreʔ) abial". </ta>
            <ta e="T1759" id="Seg_519" s="T1756">Dĭ davaj dʼorzittə. </ta>
            <ta e="T1765" id="Seg_520" s="T1759">"Iʔ dʼoraʔ, (dĭ) tăn dăra abial". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1682" id="Seg_521" s="T1681">onʼiʔ</ta>
            <ta e="T1683" id="Seg_522" s="T1682">kuza-n</ta>
            <ta e="T1684" id="Seg_523" s="T1683">iʔgö</ta>
            <ta e="T1685" id="Seg_524" s="T1684">ular</ta>
            <ta e="T1686" id="Seg_525" s="T1685">i-bi-ʔi</ta>
            <ta e="T1687" id="Seg_526" s="T1686">a</ta>
            <ta e="T1688" id="Seg_527" s="T1687">onʼiʔ</ta>
            <ta e="T1690" id="Seg_528" s="T1689">onʼiʔ</ta>
            <ta e="T1691" id="Seg_529" s="T1690">onʼiʔ</ta>
            <ta e="T1692" id="Seg_530" s="T1691">ular</ta>
            <ta e="T1693" id="Seg_531" s="T1692">i-bi</ta>
            <ta e="T1694" id="Seg_532" s="T1693">dĭʔ-nə</ta>
            <ta e="T1695" id="Seg_533" s="T1694">šo-bi</ta>
            <ta e="T1696" id="Seg_534" s="T1695">jada-j-laʔ</ta>
            <ta e="T1697" id="Seg_535" s="T1696">kuza</ta>
            <ta e="T1698" id="Seg_536" s="T1697">da</ta>
            <ta e="T1699" id="Seg_537" s="T1698">kam-bi</ta>
            <ta e="T1700" id="Seg_538" s="T1699">da</ta>
            <ta e="T1701" id="Seg_539" s="T1700">dĭ</ta>
            <ta e="T1702" id="Seg_540" s="T1701">i-bi</ta>
            <ta e="T1703" id="Seg_541" s="T1702">onʼiʔ</ta>
            <ta e="T1704" id="Seg_542" s="T1703">ular-də</ta>
            <ta e="T1705" id="Seg_543" s="T1704">băʔ-pi</ta>
            <ta e="T1706" id="Seg_544" s="T1705">da</ta>
            <ta e="T1707" id="Seg_545" s="T1706">davaj</ta>
            <ta e="T1708" id="Seg_546" s="T1707">bădə-sʼtə</ta>
            <ta e="T1709" id="Seg_547" s="T1708">dĭ</ta>
            <ta e="T1710" id="Seg_548" s="T1709">kuza-m</ta>
            <ta e="T1711" id="Seg_549" s="T1710">dĭgəttə</ta>
            <ta e="T1712" id="Seg_550" s="T1711">šo-bi</ta>
            <ta e="T1713" id="Seg_551" s="T1712">iššo</ta>
            <ta e="T1714" id="Seg_552" s="T1713">kuza</ta>
            <ta e="T1715" id="Seg_553" s="T1714">ĭmbi</ta>
            <ta e="T1716" id="Seg_554" s="T1715">dĭ</ta>
            <ta e="T1717" id="Seg_555" s="T1716">kuza-ziʔ</ta>
            <ta e="T1718" id="Seg_556" s="T1717">a-zittə</ta>
            <ta e="T1719" id="Seg_557" s="T1718">dĭ-n</ta>
            <ta e="T1720" id="Seg_558" s="T1719">iʔgö</ta>
            <ta e="T1721" id="Seg_559" s="T1720">ular-zaŋ-də</ta>
            <ta e="T1722" id="Seg_560" s="T1721">i-bi-ʔi</ta>
            <ta e="T1723" id="Seg_561" s="T1722">a</ta>
            <ta e="T1724" id="Seg_562" s="T1723">dĭ</ta>
            <ta e="T1725" id="Seg_563" s="T1724">kam-bi</ta>
            <ta e="T1726" id="Seg_564" s="T1725">da</ta>
            <ta e="T1727" id="Seg_565" s="T1726">i-bi</ta>
            <ta e="T1728" id="Seg_566" s="T1727">onʼiʔ</ta>
            <ta e="T1729" id="Seg_567" s="T1728">ular</ta>
            <ta e="T1730" id="Seg_568" s="T1729">kuza-gən</ta>
            <ta e="T1731" id="Seg_569" s="T1730">onʼiʔ</ta>
            <ta e="T1732" id="Seg_570" s="T1731">ular</ta>
            <ta e="T1733" id="Seg_571" s="T1732">i-bi</ta>
            <ta e="T1734" id="Seg_572" s="T1733">dĭ</ta>
            <ta e="T1735" id="Seg_573" s="T1734">i-bi</ta>
            <ta e="T1736" id="Seg_574" s="T1735">da</ta>
            <ta e="T1737" id="Seg_575" s="T1736">băʔ-pi</ta>
            <ta e="T1738" id="Seg_576" s="T1737">dĭ</ta>
            <ta e="T1739" id="Seg_577" s="T1738">am-nuʔ-pi-ʔi</ta>
            <ta e="T1740" id="Seg_578" s="T1739">nada</ta>
            <ta e="T1741" id="Seg_579" s="T1740">dĭ-m</ta>
            <ta e="T1742" id="Seg_580" s="T1741">kuʔ-sittə</ta>
            <ta e="T1743" id="Seg_581" s="T1742">našto</ta>
            <ta e="T1746" id="Seg_582" s="T1745">dĭ</ta>
            <ta e="T1747" id="Seg_583" s="T1746">dăreʔ</ta>
            <ta e="T1748" id="Seg_584" s="T1747">a-bi</ta>
            <ta e="T1749" id="Seg_585" s="T1748">a</ta>
            <ta e="T1750" id="Seg_586" s="T1749">dĭ</ta>
            <ta e="T1751" id="Seg_587" s="T1750">măn-də</ta>
            <ta e="T1752" id="Seg_588" s="T1751">tăn</ta>
            <ta e="T1753" id="Seg_589" s="T1752">dĭ</ta>
            <ta e="T1754" id="Seg_590" s="T1753">tăn</ta>
            <ta e="T1755" id="Seg_591" s="T1754">dăreʔ</ta>
            <ta e="T1756" id="Seg_592" s="T1755">a-bia-l</ta>
            <ta e="T1757" id="Seg_593" s="T1756">dĭ</ta>
            <ta e="T1758" id="Seg_594" s="T1757">davaj</ta>
            <ta e="T1759" id="Seg_595" s="T1758">dʼor-zittə</ta>
            <ta e="T1760" id="Seg_596" s="T1759">i-ʔ</ta>
            <ta e="T1761" id="Seg_597" s="T1760">dʼor-a-ʔ</ta>
            <ta e="T1762" id="Seg_598" s="T1761">dĭ</ta>
            <ta e="T1763" id="Seg_599" s="T1762">tăn</ta>
            <ta e="T1764" id="Seg_600" s="T1763">dăra</ta>
            <ta e="T1765" id="Seg_601" s="T1764">a-bia-l</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1682" id="Seg_602" s="T1681">onʼiʔ</ta>
            <ta e="T1683" id="Seg_603" s="T1682">kuza-n</ta>
            <ta e="T1684" id="Seg_604" s="T1683">iʔgö</ta>
            <ta e="T1685" id="Seg_605" s="T1684">ular</ta>
            <ta e="T1686" id="Seg_606" s="T1685">i-bi-jəʔ</ta>
            <ta e="T1687" id="Seg_607" s="T1686">a</ta>
            <ta e="T1688" id="Seg_608" s="T1687">onʼiʔ</ta>
            <ta e="T1690" id="Seg_609" s="T1689">onʼiʔ</ta>
            <ta e="T1691" id="Seg_610" s="T1690">onʼiʔ</ta>
            <ta e="T1692" id="Seg_611" s="T1691">ular</ta>
            <ta e="T1693" id="Seg_612" s="T1692">i-bi</ta>
            <ta e="T1694" id="Seg_613" s="T1693">dĭ-Tə</ta>
            <ta e="T1695" id="Seg_614" s="T1694">šo-bi</ta>
            <ta e="T1696" id="Seg_615" s="T1695">jada-j-lAʔ</ta>
            <ta e="T1697" id="Seg_616" s="T1696">kuza</ta>
            <ta e="T1698" id="Seg_617" s="T1697">da</ta>
            <ta e="T1699" id="Seg_618" s="T1698">kan-bi</ta>
            <ta e="T1700" id="Seg_619" s="T1699">da</ta>
            <ta e="T1701" id="Seg_620" s="T1700">dĭ</ta>
            <ta e="T1702" id="Seg_621" s="T1701">i-bi</ta>
            <ta e="T1703" id="Seg_622" s="T1702">onʼiʔ</ta>
            <ta e="T1704" id="Seg_623" s="T1703">ular-də</ta>
            <ta e="T1705" id="Seg_624" s="T1704">băd-bi</ta>
            <ta e="T1706" id="Seg_625" s="T1705">da</ta>
            <ta e="T1707" id="Seg_626" s="T1706">davaj</ta>
            <ta e="T1708" id="Seg_627" s="T1707">bădə-zittə</ta>
            <ta e="T1709" id="Seg_628" s="T1708">dĭ</ta>
            <ta e="T1710" id="Seg_629" s="T1709">kuza-m</ta>
            <ta e="T1711" id="Seg_630" s="T1710">dĭgəttə</ta>
            <ta e="T1712" id="Seg_631" s="T1711">šo-bi</ta>
            <ta e="T1713" id="Seg_632" s="T1712">ĭššo</ta>
            <ta e="T1714" id="Seg_633" s="T1713">kuza</ta>
            <ta e="T1715" id="Seg_634" s="T1714">ĭmbi</ta>
            <ta e="T1716" id="Seg_635" s="T1715">dĭ</ta>
            <ta e="T1717" id="Seg_636" s="T1716">kuza-ziʔ</ta>
            <ta e="T1718" id="Seg_637" s="T1717">a-zittə</ta>
            <ta e="T1719" id="Seg_638" s="T1718">dĭ-n</ta>
            <ta e="T1720" id="Seg_639" s="T1719">iʔgö</ta>
            <ta e="T1721" id="Seg_640" s="T1720">ular-zAŋ-də</ta>
            <ta e="T1722" id="Seg_641" s="T1721">i-bi-jəʔ</ta>
            <ta e="T1723" id="Seg_642" s="T1722">a</ta>
            <ta e="T1724" id="Seg_643" s="T1723">dĭ</ta>
            <ta e="T1725" id="Seg_644" s="T1724">kan-bi</ta>
            <ta e="T1726" id="Seg_645" s="T1725">da</ta>
            <ta e="T1727" id="Seg_646" s="T1726">i-bi</ta>
            <ta e="T1728" id="Seg_647" s="T1727">onʼiʔ</ta>
            <ta e="T1729" id="Seg_648" s="T1728">ular</ta>
            <ta e="T1730" id="Seg_649" s="T1729">kuza-Kən</ta>
            <ta e="T1731" id="Seg_650" s="T1730">onʼiʔ</ta>
            <ta e="T1732" id="Seg_651" s="T1731">ular</ta>
            <ta e="T1733" id="Seg_652" s="T1732">i-bi</ta>
            <ta e="T1734" id="Seg_653" s="T1733">dĭ</ta>
            <ta e="T1735" id="Seg_654" s="T1734">i-bi</ta>
            <ta e="T1736" id="Seg_655" s="T1735">da</ta>
            <ta e="T1737" id="Seg_656" s="T1736">băd-bi</ta>
            <ta e="T1738" id="Seg_657" s="T1737">dĭ</ta>
            <ta e="T1739" id="Seg_658" s="T1738">am-luʔbdə-bi-jəʔ</ta>
            <ta e="T1740" id="Seg_659" s="T1739">nadə</ta>
            <ta e="T1741" id="Seg_660" s="T1740">dĭ-m</ta>
            <ta e="T1742" id="Seg_661" s="T1741">kut-zittə</ta>
            <ta e="T1743" id="Seg_662" s="T1742">našto</ta>
            <ta e="T1746" id="Seg_663" s="T1745">dĭ</ta>
            <ta e="T1747" id="Seg_664" s="T1746">dărəʔ</ta>
            <ta e="T1748" id="Seg_665" s="T1747">a-bi</ta>
            <ta e="T1749" id="Seg_666" s="T1748">a</ta>
            <ta e="T1750" id="Seg_667" s="T1749">dĭ</ta>
            <ta e="T1751" id="Seg_668" s="T1750">măn-ntə</ta>
            <ta e="T1752" id="Seg_669" s="T1751">tăn</ta>
            <ta e="T1753" id="Seg_670" s="T1752">dĭ</ta>
            <ta e="T1754" id="Seg_671" s="T1753">tăn</ta>
            <ta e="T1755" id="Seg_672" s="T1754">dărəʔ</ta>
            <ta e="T1756" id="Seg_673" s="T1755">a-bi-l</ta>
            <ta e="T1757" id="Seg_674" s="T1756">dĭ</ta>
            <ta e="T1758" id="Seg_675" s="T1757">davaj</ta>
            <ta e="T1759" id="Seg_676" s="T1758">tʼor-zittə</ta>
            <ta e="T1760" id="Seg_677" s="T1759">e-ʔ</ta>
            <ta e="T1761" id="Seg_678" s="T1760">tʼor-ə-ʔ</ta>
            <ta e="T1762" id="Seg_679" s="T1761">dĭ</ta>
            <ta e="T1763" id="Seg_680" s="T1762">tăn</ta>
            <ta e="T1764" id="Seg_681" s="T1763">dăra</ta>
            <ta e="T1765" id="Seg_682" s="T1764">a-bi-l</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1682" id="Seg_683" s="T1681">one.[NOM.SG]</ta>
            <ta e="T1683" id="Seg_684" s="T1682">man-GEN</ta>
            <ta e="T1684" id="Seg_685" s="T1683">many</ta>
            <ta e="T1685" id="Seg_686" s="T1684">sheep.[NOM.SG]</ta>
            <ta e="T1686" id="Seg_687" s="T1685">be-PST-3PL</ta>
            <ta e="T1687" id="Seg_688" s="T1686">and</ta>
            <ta e="T1688" id="Seg_689" s="T1687">one.[NOM.SG]</ta>
            <ta e="T1690" id="Seg_690" s="T1689">one.[NOM.SG]</ta>
            <ta e="T1691" id="Seg_691" s="T1690">one.[NOM.SG]</ta>
            <ta e="T1692" id="Seg_692" s="T1691">sheep.[NOM.SG]</ta>
            <ta e="T1693" id="Seg_693" s="T1692">be-PST.[3SG]</ta>
            <ta e="T1694" id="Seg_694" s="T1693">this-LAT</ta>
            <ta e="T1695" id="Seg_695" s="T1694">come-PST.[3SG]</ta>
            <ta e="T1696" id="Seg_696" s="T1695">village-VBLZ-CVB</ta>
            <ta e="T1697" id="Seg_697" s="T1696">man.[NOM.SG]</ta>
            <ta e="T1698" id="Seg_698" s="T1697">and</ta>
            <ta e="T1699" id="Seg_699" s="T1698">go-PST.[3SG]</ta>
            <ta e="T1700" id="Seg_700" s="T1699">and</ta>
            <ta e="T1701" id="Seg_701" s="T1700">this.[NOM.SG]</ta>
            <ta e="T1702" id="Seg_702" s="T1701">take-PST.[3SG]</ta>
            <ta e="T1703" id="Seg_703" s="T1702">one.[NOM.SG]</ta>
            <ta e="T1704" id="Seg_704" s="T1703">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T1705" id="Seg_705" s="T1704">%%-PST.[3SG]</ta>
            <ta e="T1706" id="Seg_706" s="T1705">and</ta>
            <ta e="T1707" id="Seg_707" s="T1706">INCH</ta>
            <ta e="T1708" id="Seg_708" s="T1707">feed-INF.LAT</ta>
            <ta e="T1709" id="Seg_709" s="T1708">this.[NOM.SG]</ta>
            <ta e="T1710" id="Seg_710" s="T1709">man-ACC</ta>
            <ta e="T1711" id="Seg_711" s="T1710">then</ta>
            <ta e="T1712" id="Seg_712" s="T1711">come-PST.[3SG]</ta>
            <ta e="T1713" id="Seg_713" s="T1712">more</ta>
            <ta e="T1714" id="Seg_714" s="T1713">man.[NOM.SG]</ta>
            <ta e="T1715" id="Seg_715" s="T1714">what.[NOM.SG]</ta>
            <ta e="T1716" id="Seg_716" s="T1715">this.[NOM.SG]</ta>
            <ta e="T1717" id="Seg_717" s="T1716">man-INS</ta>
            <ta e="T1718" id="Seg_718" s="T1717">make-INF.LAT</ta>
            <ta e="T1719" id="Seg_719" s="T1718">this-GEN</ta>
            <ta e="T1720" id="Seg_720" s="T1719">many</ta>
            <ta e="T1721" id="Seg_721" s="T1720">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1722" id="Seg_722" s="T1721">be-PST-3PL</ta>
            <ta e="T1723" id="Seg_723" s="T1722">and</ta>
            <ta e="T1724" id="Seg_724" s="T1723">this.[NOM.SG]</ta>
            <ta e="T1725" id="Seg_725" s="T1724">go-PST.[3SG]</ta>
            <ta e="T1726" id="Seg_726" s="T1725">and</ta>
            <ta e="T1727" id="Seg_727" s="T1726">take-PST.[3SG]</ta>
            <ta e="T1728" id="Seg_728" s="T1727">one.[NOM.SG]</ta>
            <ta e="T1729" id="Seg_729" s="T1728">sheep.[NOM.SG]</ta>
            <ta e="T1730" id="Seg_730" s="T1729">man-LOC</ta>
            <ta e="T1731" id="Seg_731" s="T1730">one.[NOM.SG]</ta>
            <ta e="T1732" id="Seg_732" s="T1731">sheep.[NOM.SG]</ta>
            <ta e="T1733" id="Seg_733" s="T1732">be-PST.[3SG]</ta>
            <ta e="T1734" id="Seg_734" s="T1733">this.[NOM.SG]</ta>
            <ta e="T1735" id="Seg_735" s="T1734">take-PST.[3SG]</ta>
            <ta e="T1736" id="Seg_736" s="T1735">and</ta>
            <ta e="T1737" id="Seg_737" s="T1736">%%-PST.[3SG]</ta>
            <ta e="T1738" id="Seg_738" s="T1737">this.[NOM.SG]</ta>
            <ta e="T1739" id="Seg_739" s="T1738">eat-MOM-PST-3PL</ta>
            <ta e="T1740" id="Seg_740" s="T1739">one.should</ta>
            <ta e="T1741" id="Seg_741" s="T1740">this-ACC</ta>
            <ta e="T1742" id="Seg_742" s="T1741">kill-INF.LAT</ta>
            <ta e="T1743" id="Seg_743" s="T1742">what.for</ta>
            <ta e="T1746" id="Seg_744" s="T1745">this.[NOM.SG]</ta>
            <ta e="T1747" id="Seg_745" s="T1746">so</ta>
            <ta e="T1748" id="Seg_746" s="T1747">make-PST.[3SG]</ta>
            <ta e="T1749" id="Seg_747" s="T1748">and</ta>
            <ta e="T1750" id="Seg_748" s="T1749">this.[NOM.SG]</ta>
            <ta e="T1751" id="Seg_749" s="T1750">say-IPFVZ.[3SG]</ta>
            <ta e="T1752" id="Seg_750" s="T1751">you.NOM</ta>
            <ta e="T1753" id="Seg_751" s="T1752">this.[NOM.SG]</ta>
            <ta e="T1754" id="Seg_752" s="T1753">you.NOM</ta>
            <ta e="T1755" id="Seg_753" s="T1754">so</ta>
            <ta e="T1756" id="Seg_754" s="T1755">make-PST-2SG</ta>
            <ta e="T1757" id="Seg_755" s="T1756">this.[NOM.SG]</ta>
            <ta e="T1758" id="Seg_756" s="T1757">INCH</ta>
            <ta e="T1759" id="Seg_757" s="T1758">cry-INF.LAT</ta>
            <ta e="T1760" id="Seg_758" s="T1759">NEG.AUX-IMP.2SG</ta>
            <ta e="T1761" id="Seg_759" s="T1760">cry-EP-CNG</ta>
            <ta e="T1762" id="Seg_760" s="T1761">this.[NOM.SG]</ta>
            <ta e="T1763" id="Seg_761" s="T1762">you.NOM</ta>
            <ta e="T1764" id="Seg_762" s="T1763">along</ta>
            <ta e="T1765" id="Seg_763" s="T1764">make-PST-2SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1682" id="Seg_764" s="T1681">один.[NOM.SG]</ta>
            <ta e="T1683" id="Seg_765" s="T1682">мужчина-GEN</ta>
            <ta e="T1684" id="Seg_766" s="T1683">много</ta>
            <ta e="T1685" id="Seg_767" s="T1684">овца.[NOM.SG]</ta>
            <ta e="T1686" id="Seg_768" s="T1685">быть-PST-3PL</ta>
            <ta e="T1687" id="Seg_769" s="T1686">а</ta>
            <ta e="T1688" id="Seg_770" s="T1687">один.[NOM.SG]</ta>
            <ta e="T1690" id="Seg_771" s="T1689">один.[NOM.SG]</ta>
            <ta e="T1691" id="Seg_772" s="T1690">один.[NOM.SG]</ta>
            <ta e="T1692" id="Seg_773" s="T1691">овца.[NOM.SG]</ta>
            <ta e="T1693" id="Seg_774" s="T1692">быть-PST.[3SG]</ta>
            <ta e="T1694" id="Seg_775" s="T1693">этот-LAT</ta>
            <ta e="T1695" id="Seg_776" s="T1694">прийти-PST.[3SG]</ta>
            <ta e="T1696" id="Seg_777" s="T1695">деревня-VBLZ-CVB</ta>
            <ta e="T1697" id="Seg_778" s="T1696">мужчина.[NOM.SG]</ta>
            <ta e="T1698" id="Seg_779" s="T1697">и</ta>
            <ta e="T1699" id="Seg_780" s="T1698">пойти-PST.[3SG]</ta>
            <ta e="T1700" id="Seg_781" s="T1699">и</ta>
            <ta e="T1701" id="Seg_782" s="T1700">этот.[NOM.SG]</ta>
            <ta e="T1702" id="Seg_783" s="T1701">взять-PST.[3SG]</ta>
            <ta e="T1703" id="Seg_784" s="T1702">один.[NOM.SG]</ta>
            <ta e="T1704" id="Seg_785" s="T1703">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T1705" id="Seg_786" s="T1704">%%-PST.[3SG]</ta>
            <ta e="T1706" id="Seg_787" s="T1705">и</ta>
            <ta e="T1707" id="Seg_788" s="T1706">INCH</ta>
            <ta e="T1708" id="Seg_789" s="T1707">кормить-INF.LAT</ta>
            <ta e="T1709" id="Seg_790" s="T1708">этот.[NOM.SG]</ta>
            <ta e="T1710" id="Seg_791" s="T1709">мужчина-ACC</ta>
            <ta e="T1711" id="Seg_792" s="T1710">тогда</ta>
            <ta e="T1712" id="Seg_793" s="T1711">прийти-PST.[3SG]</ta>
            <ta e="T1713" id="Seg_794" s="T1712">еще</ta>
            <ta e="T1714" id="Seg_795" s="T1713">мужчина.[NOM.SG]</ta>
            <ta e="T1715" id="Seg_796" s="T1714">что.[NOM.SG]</ta>
            <ta e="T1716" id="Seg_797" s="T1715">этот.[NOM.SG]</ta>
            <ta e="T1717" id="Seg_798" s="T1716">мужчина-INS</ta>
            <ta e="T1718" id="Seg_799" s="T1717">делать-INF.LAT</ta>
            <ta e="T1719" id="Seg_800" s="T1718">этот-GEN</ta>
            <ta e="T1720" id="Seg_801" s="T1719">много</ta>
            <ta e="T1721" id="Seg_802" s="T1720">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1722" id="Seg_803" s="T1721">быть-PST-3PL</ta>
            <ta e="T1723" id="Seg_804" s="T1722">а</ta>
            <ta e="T1724" id="Seg_805" s="T1723">этот.[NOM.SG]</ta>
            <ta e="T1725" id="Seg_806" s="T1724">пойти-PST.[3SG]</ta>
            <ta e="T1726" id="Seg_807" s="T1725">и</ta>
            <ta e="T1727" id="Seg_808" s="T1726">взять-PST.[3SG]</ta>
            <ta e="T1728" id="Seg_809" s="T1727">один.[NOM.SG]</ta>
            <ta e="T1729" id="Seg_810" s="T1728">овца.[NOM.SG]</ta>
            <ta e="T1730" id="Seg_811" s="T1729">мужчина-LOC</ta>
            <ta e="T1731" id="Seg_812" s="T1730">один.[NOM.SG]</ta>
            <ta e="T1732" id="Seg_813" s="T1731">овца.[NOM.SG]</ta>
            <ta e="T1733" id="Seg_814" s="T1732">быть-PST.[3SG]</ta>
            <ta e="T1734" id="Seg_815" s="T1733">этот.[NOM.SG]</ta>
            <ta e="T1735" id="Seg_816" s="T1734">взять-PST.[3SG]</ta>
            <ta e="T1736" id="Seg_817" s="T1735">и</ta>
            <ta e="T1737" id="Seg_818" s="T1736">%%-PST.[3SG]</ta>
            <ta e="T1738" id="Seg_819" s="T1737">этот.[NOM.SG]</ta>
            <ta e="T1739" id="Seg_820" s="T1738">съесть-MOM-PST-3PL</ta>
            <ta e="T1740" id="Seg_821" s="T1739">надо</ta>
            <ta e="T1741" id="Seg_822" s="T1740">этот-ACC</ta>
            <ta e="T1742" id="Seg_823" s="T1741">убить-INF.LAT</ta>
            <ta e="T1743" id="Seg_824" s="T1742">зачем</ta>
            <ta e="T1746" id="Seg_825" s="T1745">этот.[NOM.SG]</ta>
            <ta e="T1747" id="Seg_826" s="T1746">так</ta>
            <ta e="T1748" id="Seg_827" s="T1747">делать-PST.[3SG]</ta>
            <ta e="T1749" id="Seg_828" s="T1748">а</ta>
            <ta e="T1750" id="Seg_829" s="T1749">этот.[NOM.SG]</ta>
            <ta e="T1751" id="Seg_830" s="T1750">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1752" id="Seg_831" s="T1751">ты.NOM</ta>
            <ta e="T1753" id="Seg_832" s="T1752">этот.[NOM.SG]</ta>
            <ta e="T1754" id="Seg_833" s="T1753">ты.NOM</ta>
            <ta e="T1755" id="Seg_834" s="T1754">так</ta>
            <ta e="T1756" id="Seg_835" s="T1755">делать-PST-2SG</ta>
            <ta e="T1757" id="Seg_836" s="T1756">этот.[NOM.SG]</ta>
            <ta e="T1758" id="Seg_837" s="T1757">INCH</ta>
            <ta e="T1759" id="Seg_838" s="T1758">плакать-INF.LAT</ta>
            <ta e="T1760" id="Seg_839" s="T1759">NEG.AUX-IMP.2SG</ta>
            <ta e="T1761" id="Seg_840" s="T1760">плакать-EP-CNG</ta>
            <ta e="T1762" id="Seg_841" s="T1761">этот.[NOM.SG]</ta>
            <ta e="T1763" id="Seg_842" s="T1762">ты.NOM</ta>
            <ta e="T1764" id="Seg_843" s="T1763">вдоль</ta>
            <ta e="T1765" id="Seg_844" s="T1764">делать-PST-2SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1682" id="Seg_845" s="T1681">num-n:case</ta>
            <ta e="T1683" id="Seg_846" s="T1682">n-n:case</ta>
            <ta e="T1684" id="Seg_847" s="T1683">quant</ta>
            <ta e="T1685" id="Seg_848" s="T1684">n-n:case</ta>
            <ta e="T1686" id="Seg_849" s="T1685">v-v:tense-v:pn</ta>
            <ta e="T1687" id="Seg_850" s="T1686">conj</ta>
            <ta e="T1688" id="Seg_851" s="T1687">num-n:case</ta>
            <ta e="T1690" id="Seg_852" s="T1689">num-n:case</ta>
            <ta e="T1691" id="Seg_853" s="T1690">num-n:case</ta>
            <ta e="T1692" id="Seg_854" s="T1691">n-n:case</ta>
            <ta e="T1693" id="Seg_855" s="T1692">v-v:tense-v:pn</ta>
            <ta e="T1694" id="Seg_856" s="T1693">dempro-n:case</ta>
            <ta e="T1695" id="Seg_857" s="T1694">v-v:tense-v:pn</ta>
            <ta e="T1696" id="Seg_858" s="T1695">n-n&gt;v-v:n.fin</ta>
            <ta e="T1697" id="Seg_859" s="T1696">n-n:case</ta>
            <ta e="T1698" id="Seg_860" s="T1697">conj</ta>
            <ta e="T1699" id="Seg_861" s="T1698">v-v:tense-v:pn</ta>
            <ta e="T1700" id="Seg_862" s="T1699">conj</ta>
            <ta e="T1701" id="Seg_863" s="T1700">dempro-n:case</ta>
            <ta e="T1702" id="Seg_864" s="T1701">v-v:tense-v:pn</ta>
            <ta e="T1703" id="Seg_865" s="T1702">num-n:case</ta>
            <ta e="T1704" id="Seg_866" s="T1703">n-n:case.poss</ta>
            <ta e="T1705" id="Seg_867" s="T1704">v-v:tense-v:pn</ta>
            <ta e="T1706" id="Seg_868" s="T1705">conj</ta>
            <ta e="T1707" id="Seg_869" s="T1706">ptcl</ta>
            <ta e="T1708" id="Seg_870" s="T1707">v-v:n.fin</ta>
            <ta e="T1709" id="Seg_871" s="T1708">dempro-n:case</ta>
            <ta e="T1710" id="Seg_872" s="T1709">n-n:case</ta>
            <ta e="T1711" id="Seg_873" s="T1710">adv</ta>
            <ta e="T1712" id="Seg_874" s="T1711">v-v:tense-v:pn</ta>
            <ta e="T1713" id="Seg_875" s="T1712">adv</ta>
            <ta e="T1714" id="Seg_876" s="T1713">n-n:case</ta>
            <ta e="T1715" id="Seg_877" s="T1714">que-n:case</ta>
            <ta e="T1716" id="Seg_878" s="T1715">dempro-n:case</ta>
            <ta e="T1717" id="Seg_879" s="T1716">n-n:case</ta>
            <ta e="T1718" id="Seg_880" s="T1717">v-v:n.fin</ta>
            <ta e="T1719" id="Seg_881" s="T1718">dempro-n:case</ta>
            <ta e="T1720" id="Seg_882" s="T1719">quant</ta>
            <ta e="T1721" id="Seg_883" s="T1720">n-n:num-n:case.poss</ta>
            <ta e="T1722" id="Seg_884" s="T1721">v-v:tense-v:pn</ta>
            <ta e="T1723" id="Seg_885" s="T1722">conj</ta>
            <ta e="T1724" id="Seg_886" s="T1723">dempro-n:case</ta>
            <ta e="T1725" id="Seg_887" s="T1724">v-v:tense-v:pn</ta>
            <ta e="T1726" id="Seg_888" s="T1725">conj</ta>
            <ta e="T1727" id="Seg_889" s="T1726">v-v:tense-v:pn</ta>
            <ta e="T1728" id="Seg_890" s="T1727">num-n:case</ta>
            <ta e="T1729" id="Seg_891" s="T1728">n-n:case</ta>
            <ta e="T1730" id="Seg_892" s="T1729">n-n:case</ta>
            <ta e="T1731" id="Seg_893" s="T1730">num-n:case</ta>
            <ta e="T1732" id="Seg_894" s="T1731">n-n:case</ta>
            <ta e="T1733" id="Seg_895" s="T1732">v-v:tense-v:pn</ta>
            <ta e="T1734" id="Seg_896" s="T1733">dempro-n:case</ta>
            <ta e="T1735" id="Seg_897" s="T1734">v-v:tense-v:pn</ta>
            <ta e="T1736" id="Seg_898" s="T1735">conj</ta>
            <ta e="T1737" id="Seg_899" s="T1736">v-v:tense-v:pn</ta>
            <ta e="T1738" id="Seg_900" s="T1737">dempro-n:case</ta>
            <ta e="T1739" id="Seg_901" s="T1738">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1740" id="Seg_902" s="T1739">ptcl</ta>
            <ta e="T1741" id="Seg_903" s="T1740">dempro-n:case</ta>
            <ta e="T1742" id="Seg_904" s="T1741">v-v:n.fin</ta>
            <ta e="T1743" id="Seg_905" s="T1742">adv</ta>
            <ta e="T1746" id="Seg_906" s="T1745">dempro-n:case</ta>
            <ta e="T1747" id="Seg_907" s="T1746">ptcl</ta>
            <ta e="T1748" id="Seg_908" s="T1747">v-v:tense-v:pn</ta>
            <ta e="T1749" id="Seg_909" s="T1748">conj</ta>
            <ta e="T1750" id="Seg_910" s="T1749">dempro-n:case</ta>
            <ta e="T1751" id="Seg_911" s="T1750">v-v&gt;v-v:pn</ta>
            <ta e="T1752" id="Seg_912" s="T1751">pers</ta>
            <ta e="T1753" id="Seg_913" s="T1752">dempro-n:case</ta>
            <ta e="T1754" id="Seg_914" s="T1753">pers</ta>
            <ta e="T1755" id="Seg_915" s="T1754">ptcl</ta>
            <ta e="T1756" id="Seg_916" s="T1755">v-v:tense-v:pn</ta>
            <ta e="T1757" id="Seg_917" s="T1756">dempro-n:case</ta>
            <ta e="T1758" id="Seg_918" s="T1757">ptcl</ta>
            <ta e="T1759" id="Seg_919" s="T1758">v-v:n.fin</ta>
            <ta e="T1760" id="Seg_920" s="T1759">aux-v:mood.pn</ta>
            <ta e="T1761" id="Seg_921" s="T1760">v-v:ins-v:n.fin</ta>
            <ta e="T1762" id="Seg_922" s="T1761">dempro-n:case</ta>
            <ta e="T1763" id="Seg_923" s="T1762">pers</ta>
            <ta e="T1764" id="Seg_924" s="T1763">post</ta>
            <ta e="T1765" id="Seg_925" s="T1764">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1682" id="Seg_926" s="T1681">num</ta>
            <ta e="T1683" id="Seg_927" s="T1682">n</ta>
            <ta e="T1684" id="Seg_928" s="T1683">quant</ta>
            <ta e="T1685" id="Seg_929" s="T1684">n</ta>
            <ta e="T1686" id="Seg_930" s="T1685">v</ta>
            <ta e="T1687" id="Seg_931" s="T1686">conj</ta>
            <ta e="T1688" id="Seg_932" s="T1687">num</ta>
            <ta e="T1690" id="Seg_933" s="T1689">num</ta>
            <ta e="T1691" id="Seg_934" s="T1690">num</ta>
            <ta e="T1692" id="Seg_935" s="T1691">n</ta>
            <ta e="T1693" id="Seg_936" s="T1692">v</ta>
            <ta e="T1694" id="Seg_937" s="T1693">dempro</ta>
            <ta e="T1695" id="Seg_938" s="T1694">v</ta>
            <ta e="T1696" id="Seg_939" s="T1695">v</ta>
            <ta e="T1697" id="Seg_940" s="T1696">n</ta>
            <ta e="T1698" id="Seg_941" s="T1697">conj</ta>
            <ta e="T1699" id="Seg_942" s="T1698">v</ta>
            <ta e="T1700" id="Seg_943" s="T1699">conj</ta>
            <ta e="T1701" id="Seg_944" s="T1700">dempro</ta>
            <ta e="T1702" id="Seg_945" s="T1701">v</ta>
            <ta e="T1703" id="Seg_946" s="T1702">num</ta>
            <ta e="T1704" id="Seg_947" s="T1703">n</ta>
            <ta e="T1705" id="Seg_948" s="T1704">v</ta>
            <ta e="T1706" id="Seg_949" s="T1705">conj</ta>
            <ta e="T1707" id="Seg_950" s="T1706">ptcl</ta>
            <ta e="T1708" id="Seg_951" s="T1707">v</ta>
            <ta e="T1709" id="Seg_952" s="T1708">dempro</ta>
            <ta e="T1710" id="Seg_953" s="T1709">n</ta>
            <ta e="T1711" id="Seg_954" s="T1710">adv</ta>
            <ta e="T1712" id="Seg_955" s="T1711">v</ta>
            <ta e="T1713" id="Seg_956" s="T1712">adv</ta>
            <ta e="T1714" id="Seg_957" s="T1713">n</ta>
            <ta e="T1715" id="Seg_958" s="T1714">que</ta>
            <ta e="T1716" id="Seg_959" s="T1715">dempro</ta>
            <ta e="T1717" id="Seg_960" s="T1716">n</ta>
            <ta e="T1718" id="Seg_961" s="T1717">v</ta>
            <ta e="T1719" id="Seg_962" s="T1718">dempro</ta>
            <ta e="T1720" id="Seg_963" s="T1719">quant</ta>
            <ta e="T1721" id="Seg_964" s="T1720">n</ta>
            <ta e="T1722" id="Seg_965" s="T1721">v</ta>
            <ta e="T1723" id="Seg_966" s="T1722">conj</ta>
            <ta e="T1724" id="Seg_967" s="T1723">dempro</ta>
            <ta e="T1725" id="Seg_968" s="T1724">v</ta>
            <ta e="T1726" id="Seg_969" s="T1725">conj</ta>
            <ta e="T1727" id="Seg_970" s="T1726">v</ta>
            <ta e="T1728" id="Seg_971" s="T1727">num</ta>
            <ta e="T1729" id="Seg_972" s="T1728">n</ta>
            <ta e="T1730" id="Seg_973" s="T1729">n</ta>
            <ta e="T1731" id="Seg_974" s="T1730">num</ta>
            <ta e="T1732" id="Seg_975" s="T1731">n</ta>
            <ta e="T1733" id="Seg_976" s="T1732">v</ta>
            <ta e="T1734" id="Seg_977" s="T1733">dempro</ta>
            <ta e="T1735" id="Seg_978" s="T1734">v</ta>
            <ta e="T1736" id="Seg_979" s="T1735">conj</ta>
            <ta e="T1737" id="Seg_980" s="T1736">v</ta>
            <ta e="T1738" id="Seg_981" s="T1737">dempro</ta>
            <ta e="T1739" id="Seg_982" s="T1738">v</ta>
            <ta e="T1740" id="Seg_983" s="T1739">ptcl</ta>
            <ta e="T1741" id="Seg_984" s="T1740">dempro</ta>
            <ta e="T1742" id="Seg_985" s="T1741">v</ta>
            <ta e="T1743" id="Seg_986" s="T1742">adv</ta>
            <ta e="T1746" id="Seg_987" s="T1745">dempro</ta>
            <ta e="T1747" id="Seg_988" s="T1746">ptcl</ta>
            <ta e="T1748" id="Seg_989" s="T1747">v</ta>
            <ta e="T1749" id="Seg_990" s="T1748">conj</ta>
            <ta e="T1750" id="Seg_991" s="T1749">dempro</ta>
            <ta e="T1751" id="Seg_992" s="T1750">v</ta>
            <ta e="T1752" id="Seg_993" s="T1751">pers</ta>
            <ta e="T1753" id="Seg_994" s="T1752">dempro</ta>
            <ta e="T1754" id="Seg_995" s="T1753">pers</ta>
            <ta e="T1755" id="Seg_996" s="T1754">ptcl</ta>
            <ta e="T1756" id="Seg_997" s="T1755">v</ta>
            <ta e="T1757" id="Seg_998" s="T1756">dempro</ta>
            <ta e="T1758" id="Seg_999" s="T1757">ptcl</ta>
            <ta e="T1759" id="Seg_1000" s="T1758">v</ta>
            <ta e="T1760" id="Seg_1001" s="T1759">aux</ta>
            <ta e="T1761" id="Seg_1002" s="T1760">v</ta>
            <ta e="T1762" id="Seg_1003" s="T1761">dempro</ta>
            <ta e="T1763" id="Seg_1004" s="T1762">pers</ta>
            <ta e="T1764" id="Seg_1005" s="T1763">post</ta>
            <ta e="T1765" id="Seg_1006" s="T1764">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1683" id="Seg_1007" s="T1682">np.h:Poss</ta>
            <ta e="T1685" id="Seg_1008" s="T1684">np:Th</ta>
            <ta e="T1692" id="Seg_1009" s="T1691">np:Th</ta>
            <ta e="T1694" id="Seg_1010" s="T1693">pro:G</ta>
            <ta e="T1697" id="Seg_1011" s="T1696">np.h:A</ta>
            <ta e="T1699" id="Seg_1012" s="T1698">0.3.h:A</ta>
            <ta e="T1701" id="Seg_1013" s="T1700">pro.h:A</ta>
            <ta e="T1704" id="Seg_1014" s="T1703">np:Th</ta>
            <ta e="T1705" id="Seg_1015" s="T1704">0.3.h:A</ta>
            <ta e="T1710" id="Seg_1016" s="T1709">np.h:R</ta>
            <ta e="T1711" id="Seg_1017" s="T1710">adv:Time</ta>
            <ta e="T1714" id="Seg_1018" s="T1713">np.h:A</ta>
            <ta e="T1717" id="Seg_1019" s="T1716">np:Ins</ta>
            <ta e="T1719" id="Seg_1020" s="T1718">pro.h:Poss</ta>
            <ta e="T1721" id="Seg_1021" s="T1720">np:Th</ta>
            <ta e="T1724" id="Seg_1022" s="T1723">pro.h:A</ta>
            <ta e="T1727" id="Seg_1023" s="T1726">0.3.h:A</ta>
            <ta e="T1729" id="Seg_1024" s="T1728">np:Th</ta>
            <ta e="T1730" id="Seg_1025" s="T1729">np:L</ta>
            <ta e="T1732" id="Seg_1026" s="T1731">np:Th</ta>
            <ta e="T1734" id="Seg_1027" s="T1733">pro.h:A</ta>
            <ta e="T1737" id="Seg_1028" s="T1736">0.3.h:A</ta>
            <ta e="T1738" id="Seg_1029" s="T1737">pro:P</ta>
            <ta e="T1739" id="Seg_1030" s="T1738">0.3.h:A</ta>
            <ta e="T1741" id="Seg_1031" s="T1740">pro:P</ta>
            <ta e="T1746" id="Seg_1032" s="T1745">pro.h:A</ta>
            <ta e="T1750" id="Seg_1033" s="T1749">pro.h:A</ta>
            <ta e="T1754" id="Seg_1034" s="T1753">pro.h:A</ta>
            <ta e="T1757" id="Seg_1035" s="T1756">pro.h:E</ta>
            <ta e="T1760" id="Seg_1036" s="T1759">0.2.h:E</ta>
            <ta e="T1763" id="Seg_1037" s="T1762">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1685" id="Seg_1038" s="T1684">np:S</ta>
            <ta e="T1686" id="Seg_1039" s="T1685">v:pred</ta>
            <ta e="T1692" id="Seg_1040" s="T1691">np:S</ta>
            <ta e="T1693" id="Seg_1041" s="T1692">v:pred</ta>
            <ta e="T1695" id="Seg_1042" s="T1694">v:pred</ta>
            <ta e="T1696" id="Seg_1043" s="T1695">conv:pred</ta>
            <ta e="T1697" id="Seg_1044" s="T1696">np.h:S</ta>
            <ta e="T1699" id="Seg_1045" s="T1698">v:pred 0.3.h:S</ta>
            <ta e="T1701" id="Seg_1046" s="T1700">pro.h:S</ta>
            <ta e="T1702" id="Seg_1047" s="T1701">v:pred</ta>
            <ta e="T1704" id="Seg_1048" s="T1703">np:O</ta>
            <ta e="T1705" id="Seg_1049" s="T1704">v:pred 0.3.h:S</ta>
            <ta e="T1707" id="Seg_1050" s="T1706">ptcl:pred</ta>
            <ta e="T1710" id="Seg_1051" s="T1709">np.h:O</ta>
            <ta e="T1712" id="Seg_1052" s="T1711">v:pred</ta>
            <ta e="T1714" id="Seg_1053" s="T1713">np.h:S</ta>
            <ta e="T1718" id="Seg_1054" s="T1717">v:pred</ta>
            <ta e="T1721" id="Seg_1055" s="T1720">np:S</ta>
            <ta e="T1722" id="Seg_1056" s="T1721">v:pred</ta>
            <ta e="T1724" id="Seg_1057" s="T1723">pro.h:S</ta>
            <ta e="T1725" id="Seg_1058" s="T1724">v:pred</ta>
            <ta e="T1727" id="Seg_1059" s="T1726">v:pred 0.3.h:S</ta>
            <ta e="T1729" id="Seg_1060" s="T1728">np:O</ta>
            <ta e="T1732" id="Seg_1061" s="T1731">np:S</ta>
            <ta e="T1733" id="Seg_1062" s="T1732">v:pred</ta>
            <ta e="T1734" id="Seg_1063" s="T1733">pro.h:S</ta>
            <ta e="T1735" id="Seg_1064" s="T1734">v:pred</ta>
            <ta e="T1737" id="Seg_1065" s="T1736">v:pred 0.3.h:S</ta>
            <ta e="T1738" id="Seg_1066" s="T1737">pro:O</ta>
            <ta e="T1739" id="Seg_1067" s="T1738">v:pred 0.3.h:S</ta>
            <ta e="T1740" id="Seg_1068" s="T1739">ptcl:pred</ta>
            <ta e="T1741" id="Seg_1069" s="T1740">pro:O</ta>
            <ta e="T1746" id="Seg_1070" s="T1745">pro.h:S</ta>
            <ta e="T1748" id="Seg_1071" s="T1747">v:pred</ta>
            <ta e="T1750" id="Seg_1072" s="T1749">pro.h:S</ta>
            <ta e="T1751" id="Seg_1073" s="T1750">v:pred</ta>
            <ta e="T1754" id="Seg_1074" s="T1753">pro.h:S</ta>
            <ta e="T1756" id="Seg_1075" s="T1755">v:pred</ta>
            <ta e="T1758" id="Seg_1076" s="T1757">ptcl:pred</ta>
            <ta e="T1760" id="Seg_1077" s="T1759">v:pred 0.2.h:S</ta>
            <ta e="T1763" id="Seg_1078" s="T1762">pro.h:S</ta>
            <ta e="T1765" id="Seg_1079" s="T1764">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1687" id="Seg_1080" s="T1686">RUS:gram</ta>
            <ta e="T1698" id="Seg_1081" s="T1697">RUS:gram</ta>
            <ta e="T1700" id="Seg_1082" s="T1699">RUS:gram</ta>
            <ta e="T1706" id="Seg_1083" s="T1705">RUS:gram</ta>
            <ta e="T1707" id="Seg_1084" s="T1706">RUS:gram</ta>
            <ta e="T1713" id="Seg_1085" s="T1712">RUS:mod</ta>
            <ta e="T1723" id="Seg_1086" s="T1722">RUS:gram</ta>
            <ta e="T1726" id="Seg_1087" s="T1725">RUS:gram</ta>
            <ta e="T1736" id="Seg_1088" s="T1735">RUS:gram</ta>
            <ta e="T1740" id="Seg_1089" s="T1739">RUS:mod</ta>
            <ta e="T1743" id="Seg_1090" s="T1742">RUS:mod</ta>
            <ta e="T1749" id="Seg_1091" s="T1748">RUS:gram</ta>
            <ta e="T1758" id="Seg_1092" s="T1757">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1686" id="Seg_1093" s="T1681">У одного человека было много овец.</ta>
            <ta e="T1693" id="Seg_1094" s="T1686">А у [другого] была одна овца.</ta>
            <ta e="T1697" id="Seg_1095" s="T1693">Этот человек пришёл к нему в гости.</ta>
            <ta e="T1704" id="Seg_1096" s="T1697">И пришёл, и он взял свою единственную овцу.</ta>
            <ta e="T1710" id="Seg_1097" s="T1704">Зарезал(?) её и стал кормить этого человека.</ta>
            <ta e="T1714" id="Seg_1098" s="T1710">Потом ещё один человек пришёл.</ta>
            <ta e="T1722" id="Seg_1099" s="T1714">Что с этим человеком делать, у него много овец было.</ta>
            <ta e="T1729" id="Seg_1100" s="T1722">А он пошёл и взял одну овцу.</ta>
            <ta e="T1738" id="Seg_1101" s="T1729">У человека была одна овца, он взял и зарезал её.</ta>
            <ta e="T1739" id="Seg_1102" s="T1738">Они её съели.</ta>
            <ta e="T1742" id="Seg_1103" s="T1739">Надо её убить.</ta>
            <ta e="T1748" id="Seg_1104" s="T1742">Почему он так сделал?</ta>
            <ta e="T1756" id="Seg_1105" s="T1748">А он говорит: "Ты это так сделал".</ta>
            <ta e="T1759" id="Seg_1106" s="T1756">Он заплакал.</ta>
            <ta e="T1765" id="Seg_1107" s="T1759">"Не плачь, это ты так сделал".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1686" id="Seg_1108" s="T1681">One man had many sheep.</ta>
            <ta e="T1693" id="Seg_1109" s="T1686">And one [other man] had one sheep.</ta>
            <ta e="T1697" id="Seg_1110" s="T1693">The man came to him to visit.</ta>
            <ta e="T1704" id="Seg_1111" s="T1697">And he went and he took his one sheep.</ta>
            <ta e="T1710" id="Seg_1112" s="T1704">He slaughtered it and started to feed the man.</ta>
            <ta e="T1714" id="Seg_1113" s="T1710">Then one more man came.</ta>
            <ta e="T1722" id="Seg_1114" s="T1714">What to do with this man, he had many sheep.</ta>
            <ta e="T1729" id="Seg_1115" s="T1722">But he went and took one sheep.</ta>
            <ta e="T1738" id="Seg_1116" s="T1729">With the man there was one sheep, he took it and killed it.</ta>
            <ta e="T1739" id="Seg_1117" s="T1738">They ate it.</ta>
            <ta e="T1742" id="Seg_1118" s="T1739">We need to kill it.</ta>
            <ta e="T1748" id="Seg_1119" s="T1742">But why did he do like this?</ta>
            <ta e="T1756" id="Seg_1120" s="T1748">But he says: "You did it like that."</ta>
            <ta e="T1759" id="Seg_1121" s="T1756">He started to cry.</ta>
            <ta e="T1765" id="Seg_1122" s="T1759">"Don't cry, YOU did it like this."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1686" id="Seg_1123" s="T1681">Eine Mann hat viele Schafe.</ta>
            <ta e="T1693" id="Seg_1124" s="T1686">Und einer [ein Anderer] hatte ein Schaf.</ta>
            <ta e="T1697" id="Seg_1125" s="T1693">Zu ihm kam der Mann zu Besuch.</ta>
            <ta e="T1704" id="Seg_1126" s="T1697">Und er ging und nahm sein einziges Schaf.</ta>
            <ta e="T1710" id="Seg_1127" s="T1704">Er schlachtete es und fing an, es dem Mann zu füttern.</ta>
            <ta e="T1714" id="Seg_1128" s="T1710">Dann kam ein weiterer Mann.</ta>
            <ta e="T1722" id="Seg_1129" s="T1714">Was tun mit diesem Mann, er hatte viele Schafe.</ta>
            <ta e="T1729" id="Seg_1130" s="T1722">Doch er ging und nahm ein Schaf.</ta>
            <ta e="T1738" id="Seg_1131" s="T1729">Beim Mann war ein Schaf, er nahm es und tötete es.</ta>
            <ta e="T1739" id="Seg_1132" s="T1738">Sie aßen es.</ta>
            <ta e="T1742" id="Seg_1133" s="T1739">Wir müssen es töten.</ta>
            <ta e="T1748" id="Seg_1134" s="T1742">Aber warum hat er so getan?</ta>
            <ta e="T1756" id="Seg_1135" s="T1748">Doch er sagt: „Du hast es so getan.“</ta>
            <ta e="T1759" id="Seg_1136" s="T1756">Er fing an, zu weinen.</ta>
            <ta e="T1765" id="Seg_1137" s="T1759">„Wein nicht, DU hast es so getan.“</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1681" />
            <conversion-tli id="T1682" />
            <conversion-tli id="T1683" />
            <conversion-tli id="T1684" />
            <conversion-tli id="T1685" />
            <conversion-tli id="T1686" />
            <conversion-tli id="T1687" />
            <conversion-tli id="T1688" />
            <conversion-tli id="T1689" />
            <conversion-tli id="T1690" />
            <conversion-tli id="T1691" />
            <conversion-tli id="T1692" />
            <conversion-tli id="T1693" />
            <conversion-tli id="T1694" />
            <conversion-tli id="T1695" />
            <conversion-tli id="T1696" />
            <conversion-tli id="T1697" />
            <conversion-tli id="T1698" />
            <conversion-tli id="T1699" />
            <conversion-tli id="T1700" />
            <conversion-tli id="T1701" />
            <conversion-tli id="T1702" />
            <conversion-tli id="T1703" />
            <conversion-tli id="T1704" />
            <conversion-tli id="T1705" />
            <conversion-tli id="T1706" />
            <conversion-tli id="T1707" />
            <conversion-tli id="T1708" />
            <conversion-tli id="T1709" />
            <conversion-tli id="T1710" />
            <conversion-tli id="T1711" />
            <conversion-tli id="T1712" />
            <conversion-tli id="T1713" />
            <conversion-tli id="T1714" />
            <conversion-tli id="T1715" />
            <conversion-tli id="T1716" />
            <conversion-tli id="T1717" />
            <conversion-tli id="T1718" />
            <conversion-tli id="T1719" />
            <conversion-tli id="T1720" />
            <conversion-tli id="T1721" />
            <conversion-tli id="T1722" />
            <conversion-tli id="T1723" />
            <conversion-tli id="T1724" />
            <conversion-tli id="T1725" />
            <conversion-tli id="T1726" />
            <conversion-tli id="T1727" />
            <conversion-tli id="T1728" />
            <conversion-tli id="T1729" />
            <conversion-tli id="T1730" />
            <conversion-tli id="T1731" />
            <conversion-tli id="T1732" />
            <conversion-tli id="T1733" />
            <conversion-tli id="T1734" />
            <conversion-tli id="T1735" />
            <conversion-tli id="T1736" />
            <conversion-tli id="T1737" />
            <conversion-tli id="T1738" />
            <conversion-tli id="T1739" />
            <conversion-tli id="T1740" />
            <conversion-tli id="T1741" />
            <conversion-tli id="T1742" />
            <conversion-tli id="T1743" />
            <conversion-tli id="T1744" />
            <conversion-tli id="T1745" />
            <conversion-tli id="T1746" />
            <conversion-tli id="T1747" />
            <conversion-tli id="T1748" />
            <conversion-tli id="T1749" />
            <conversion-tli id="T1750" />
            <conversion-tli id="T1751" />
            <conversion-tli id="T1752" />
            <conversion-tli id="T1753" />
            <conversion-tli id="T1754" />
            <conversion-tli id="T1755" />
            <conversion-tli id="T1756" />
            <conversion-tli id="T1757" />
            <conversion-tli id="T1758" />
            <conversion-tli id="T1759" />
            <conversion-tli id="T1760" />
            <conversion-tli id="T1761" />
            <conversion-tli id="T1762" />
            <conversion-tli id="T1763" />
            <conversion-tli id="T1764" />
            <conversion-tli id="T1765" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
