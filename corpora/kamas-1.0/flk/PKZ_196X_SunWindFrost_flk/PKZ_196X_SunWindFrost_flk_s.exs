<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID775BFBC0-DB20-EE0D-7CEF-A6D650E24020">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SunWindFrost_flk.wav" />
         <referenced-file url="PKZ_196X_SunWindFrost_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SunWindFrost_flk\PKZ_196X_SunWindFrost_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">65</ud-information>
            <ud-information attribute-name="# HIAT:w">44</ud-information>
            <ud-information attribute-name="# e">44</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.008" type="appl" />
         <tli id="T1" time="1.36" type="appl" />
         <tli id="T2" time="2.713" type="appl" />
         <tli id="T3" time="4.065" type="appl" />
         <tli id="T4" time="4.928" type="appl" />
         <tli id="T5" time="5.79" type="appl" />
         <tli id="T6" time="6.653" type="appl" />
         <tli id="T7" time="8.819552396526356" />
         <tli id="T8" time="10.04" type="appl" />
         <tli id="T9" time="10.986" type="appl" />
         <tli id="T10" time="11.931" type="appl" />
         <tli id="T11" time="12.876" type="appl" />
         <tli id="T12" time="13.822" type="appl" />
         <tli id="T13" time="15.13256533644356" />
         <tli id="T14" time="15.941" type="appl" />
         <tli id="T15" time="16.712" type="appl" />
         <tli id="T16" time="17.484" type="appl" />
         <tli id="T17" time="18.255" type="appl" />
         <tli id="T18" time="19.026" type="appl" />
         <tli id="T19" time="20.222" type="appl" />
         <tli id="T20" time="21.419" type="appl" />
         <tli id="T21" time="22.615" type="appl" />
         <tli id="T22" time="23.811" type="appl" />
         <tli id="T23" time="25.007" type="appl" />
         <tli id="T24" time="26.204" type="appl" />
         <tli id="T25" time="27.4" type="appl" />
         <tli id="T26" time="28.477" type="appl" />
         <tli id="T27" time="29.554" type="appl" />
         <tli id="T28" time="30.632" type="appl" />
         <tli id="T29" time="31.709" type="appl" />
         <tli id="T30" time="32.786" type="appl" />
         <tli id="T31" time="33.869" type="appl" />
         <tli id="T32" time="34.92" type="appl" />
         <tli id="T33" time="35.97" type="appl" />
         <tli id="T34" time="37.02" type="appl" />
         <tli id="T35" time="37.99" type="appl" />
         <tli id="T36" time="39.1913443228862" />
         <tli id="T37" time="39.751" type="appl" />
         <tli id="T38" time="40.541" type="appl" />
         <tli id="T39" time="41.331" type="appl" />
         <tli id="T40" time="42.122" type="appl" />
         <tli id="T41" time="42.912" type="appl" />
         <tli id="T42" time="43.702" type="appl" />
         <tli id="T43" time="44.492" type="appl" />
         <tli id="T44" time="45.591" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T44" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nagurgöʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">stalʼi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kudonzittə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Šĭšəge</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">măndə:</ts>
                  <nts id="Seg_20" n="HIAT:ip">"</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Măn</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">kănnaːmbim</ts>
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_31" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">A</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">beržə</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">măndə:</ts>
                  <nts id="Seg_40" n="HIAT:ip">"</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Măn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">tože</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">kănnaːllal</ts>
                  <nts id="Seg_50" n="HIAT:ip">"</nts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_54" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">A</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">kuja:</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">"</nts>
                  <nts id="Seg_62" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">Măn=</ts>
                  <nts id="Seg_65" n="HIAT:ip">)</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_68" n="HIAT:w" s="T16">Măn</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">ejümlim</ts>
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_76" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">Dĭgəttə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">šĭšəge</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">bar</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_87" n="HIAT:w" s="T21">ugaːndə</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_90" n="HIAT:w" s="T22">kănnaːmbi</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">kănnaːmbi</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">kuza</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_101" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">Dĭgəttə</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">beržə:</ts>
                  <nts id="Seg_107" n="HIAT:ip">"</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">Măn</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">tože</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">kănnaːmnim</ts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_121" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">Dĭgəttə</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">kuja</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">bar</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">uʔbdəbi</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_136" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">Ejü</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">molaːmbi</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_145" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_147" n="HIAT:w" s="T36">I</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">bar</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">beržə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">külaːmbi</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">šĭšəge</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">naga</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_169" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">Kabarləj</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T44" id="Seg_174" n="sc" s="T0">
               <ts e="T1" id="Seg_176" n="e" s="T0">Nagurgöʔ </ts>
               <ts e="T2" id="Seg_178" n="e" s="T1">stalʼi </ts>
               <ts e="T3" id="Seg_180" n="e" s="T2">kudonzittə. </ts>
               <ts e="T4" id="Seg_182" n="e" s="T3">Šĭšəge </ts>
               <ts e="T5" id="Seg_184" n="e" s="T4">măndə:" </ts>
               <ts e="T6" id="Seg_186" n="e" s="T5">Măn </ts>
               <ts e="T7" id="Seg_188" n="e" s="T6">kănnaːmbim". </ts>
               <ts e="T8" id="Seg_190" n="e" s="T7">A </ts>
               <ts e="T9" id="Seg_192" n="e" s="T8">beržə </ts>
               <ts e="T10" id="Seg_194" n="e" s="T9">măndə:" </ts>
               <ts e="T11" id="Seg_196" n="e" s="T10">Măn </ts>
               <ts e="T12" id="Seg_198" n="e" s="T11">tože </ts>
               <ts e="T13" id="Seg_200" n="e" s="T12">kănnaːllal". </ts>
               <ts e="T14" id="Seg_202" n="e" s="T13">A </ts>
               <ts e="T15" id="Seg_204" n="e" s="T14">kuja: </ts>
               <ts e="T16" id="Seg_206" n="e" s="T15">"(Măn=) </ts>
               <ts e="T17" id="Seg_208" n="e" s="T16">Măn </ts>
               <ts e="T18" id="Seg_210" n="e" s="T17">ejümlim". </ts>
               <ts e="T19" id="Seg_212" n="e" s="T18">Dĭgəttə </ts>
               <ts e="T20" id="Seg_214" n="e" s="T19">šĭšəge </ts>
               <ts e="T21" id="Seg_216" n="e" s="T20">bar </ts>
               <ts e="T22" id="Seg_218" n="e" s="T21">ugaːndə </ts>
               <ts e="T23" id="Seg_220" n="e" s="T22">kănnaːmbi, </ts>
               <ts e="T24" id="Seg_222" n="e" s="T23">kănnaːmbi </ts>
               <ts e="T25" id="Seg_224" n="e" s="T24">kuza. </ts>
               <ts e="T26" id="Seg_226" n="e" s="T25">Dĭgəttə </ts>
               <ts e="T27" id="Seg_228" n="e" s="T26">beržə:" </ts>
               <ts e="T28" id="Seg_230" n="e" s="T27">Măn </ts>
               <ts e="T29" id="Seg_232" n="e" s="T28">tože </ts>
               <ts e="T30" id="Seg_234" n="e" s="T29">kănnaːmnim". </ts>
               <ts e="T31" id="Seg_236" n="e" s="T30">Dĭgəttə </ts>
               <ts e="T32" id="Seg_238" n="e" s="T31">kuja </ts>
               <ts e="T33" id="Seg_240" n="e" s="T32">bar </ts>
               <ts e="T34" id="Seg_242" n="e" s="T33">uʔbdəbi. </ts>
               <ts e="T35" id="Seg_244" n="e" s="T34">Ejü </ts>
               <ts e="T36" id="Seg_246" n="e" s="T35">molaːmbi. </ts>
               <ts e="T37" id="Seg_248" n="e" s="T36">I </ts>
               <ts e="T38" id="Seg_250" n="e" s="T37">bar </ts>
               <ts e="T39" id="Seg_252" n="e" s="T38">beržə </ts>
               <ts e="T40" id="Seg_254" n="e" s="T39">külaːmbi </ts>
               <ts e="T41" id="Seg_256" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_258" n="e" s="T41">šĭšəge </ts>
               <ts e="T43" id="Seg_260" n="e" s="T42">naga. </ts>
               <ts e="T44" id="Seg_262" n="e" s="T43">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_263" s="T0">PKZ_196X_SunWindFrost_flk.001 (002)</ta>
            <ta e="T7" id="Seg_264" s="T3">PKZ_196X_SunWindFrost_flk.002 (003)</ta>
            <ta e="T13" id="Seg_265" s="T7">PKZ_196X_SunWindFrost_flk.003 (004)</ta>
            <ta e="T18" id="Seg_266" s="T13">PKZ_196X_SunWindFrost_flk.004 (005)</ta>
            <ta e="T25" id="Seg_267" s="T18">PKZ_196X_SunWindFrost_flk.005 (006)</ta>
            <ta e="T30" id="Seg_268" s="T25">PKZ_196X_SunWindFrost_flk.006 (007)</ta>
            <ta e="T34" id="Seg_269" s="T30">PKZ_196X_SunWindFrost_flk.007 (008)</ta>
            <ta e="T36" id="Seg_270" s="T34">PKZ_196X_SunWindFrost_flk.008 (009)</ta>
            <ta e="T43" id="Seg_271" s="T36">PKZ_196X_SunWindFrost_flk.009 (010)</ta>
            <ta e="T44" id="Seg_272" s="T43">PKZ_196X_SunWindFrost_flk.010 (011)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_273" s="T0">Nagurgöʔ stalʼi kudonzittə. </ta>
            <ta e="T7" id="Seg_274" s="T3">Šĭšəge măndə:" Măn kănnaːmbim". </ta>
            <ta e="T13" id="Seg_275" s="T7">A beržə măndə:" Măn tože kănnaːllal". </ta>
            <ta e="T18" id="Seg_276" s="T13">A kuja: "(Măn=) Măn ejümlim". </ta>
            <ta e="T25" id="Seg_277" s="T18">Dĭgəttə šĭšəge bar ugaːndə kănnaːmbi, kănnaːmbi kuza. </ta>
            <ta e="T30" id="Seg_278" s="T25">Dĭgəttə beržə:" Măn tože kănnaːmnim". </ta>
            <ta e="T34" id="Seg_279" s="T30">Dĭgəttə kuja bar uʔbdəbi. </ta>
            <ta e="T36" id="Seg_280" s="T34">Ejü molaːmbi. </ta>
            <ta e="T43" id="Seg_281" s="T36">I bar beržə külaːmbi i šĭšəge naga. </ta>
            <ta e="T44" id="Seg_282" s="T43">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_283" s="T0">nagur-göʔ</ta>
            <ta e="T3" id="Seg_284" s="T2">kudon-zittə</ta>
            <ta e="T4" id="Seg_285" s="T3">šĭšəge</ta>
            <ta e="T5" id="Seg_286" s="T4">măn-də</ta>
            <ta e="T6" id="Seg_287" s="T5">măn</ta>
            <ta e="T7" id="Seg_288" s="T6">kăn-naːm-bi-m</ta>
            <ta e="T8" id="Seg_289" s="T7">a</ta>
            <ta e="T9" id="Seg_290" s="T8">beržə</ta>
            <ta e="T10" id="Seg_291" s="T9">măn-də</ta>
            <ta e="T11" id="Seg_292" s="T10">măn</ta>
            <ta e="T12" id="Seg_293" s="T11">tože</ta>
            <ta e="T13" id="Seg_294" s="T12">kăn-naːl-la-l</ta>
            <ta e="T14" id="Seg_295" s="T13">a</ta>
            <ta e="T15" id="Seg_296" s="T14">kuja</ta>
            <ta e="T16" id="Seg_297" s="T15">măn</ta>
            <ta e="T17" id="Seg_298" s="T16">măn</ta>
            <ta e="T18" id="Seg_299" s="T17">ejüm-li-m</ta>
            <ta e="T19" id="Seg_300" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_301" s="T19">šĭšəge</ta>
            <ta e="T21" id="Seg_302" s="T20">bar</ta>
            <ta e="T22" id="Seg_303" s="T21">ugaːndə</ta>
            <ta e="T23" id="Seg_304" s="T22">kăn-naːm-bi</ta>
            <ta e="T24" id="Seg_305" s="T23">kăn-naːm-bi</ta>
            <ta e="T25" id="Seg_306" s="T24">kuza</ta>
            <ta e="T26" id="Seg_307" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_308" s="T26">beržə</ta>
            <ta e="T28" id="Seg_309" s="T27">măn</ta>
            <ta e="T29" id="Seg_310" s="T28">tože</ta>
            <ta e="T30" id="Seg_311" s="T29">kăn-naːm-ni-m</ta>
            <ta e="T31" id="Seg_312" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_313" s="T31">kuja</ta>
            <ta e="T33" id="Seg_314" s="T32">bar</ta>
            <ta e="T34" id="Seg_315" s="T33">uʔbdə-bi</ta>
            <ta e="T35" id="Seg_316" s="T34">ejü</ta>
            <ta e="T36" id="Seg_317" s="T35">mo-laːm-bi</ta>
            <ta e="T37" id="Seg_318" s="T36">i</ta>
            <ta e="T38" id="Seg_319" s="T37">bar</ta>
            <ta e="T39" id="Seg_320" s="T38">beržə</ta>
            <ta e="T40" id="Seg_321" s="T39">kü-laːm-bi</ta>
            <ta e="T41" id="Seg_322" s="T40">i</ta>
            <ta e="T42" id="Seg_323" s="T41">šĭšəge</ta>
            <ta e="T43" id="Seg_324" s="T42">naga</ta>
            <ta e="T44" id="Seg_325" s="T43">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_326" s="T0">nagur-göʔ</ta>
            <ta e="T3" id="Seg_327" s="T2">kudonz-zittə</ta>
            <ta e="T4" id="Seg_328" s="T3">šišəge</ta>
            <ta e="T5" id="Seg_329" s="T4">măn-ntə</ta>
            <ta e="T6" id="Seg_330" s="T5">măn</ta>
            <ta e="T7" id="Seg_331" s="T6">kănzə-laːm-bi-m</ta>
            <ta e="T8" id="Seg_332" s="T7">a</ta>
            <ta e="T9" id="Seg_333" s="T8">beržə</ta>
            <ta e="T10" id="Seg_334" s="T9">măn-ntə</ta>
            <ta e="T11" id="Seg_335" s="T10">măn</ta>
            <ta e="T12" id="Seg_336" s="T11">tože</ta>
            <ta e="T13" id="Seg_337" s="T12">kănzə-laːm-lV-l</ta>
            <ta e="T14" id="Seg_338" s="T13">a</ta>
            <ta e="T15" id="Seg_339" s="T14">kuja</ta>
            <ta e="T16" id="Seg_340" s="T15">măn</ta>
            <ta e="T17" id="Seg_341" s="T16">măn</ta>
            <ta e="T18" id="Seg_342" s="T17">ejüm-lV-m</ta>
            <ta e="T19" id="Seg_343" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_344" s="T19">šišəge</ta>
            <ta e="T21" id="Seg_345" s="T20">bar</ta>
            <ta e="T22" id="Seg_346" s="T21">ugaːndə</ta>
            <ta e="T23" id="Seg_347" s="T22">kănzə-laːm-bi</ta>
            <ta e="T24" id="Seg_348" s="T23">kănzə-laːm-bi</ta>
            <ta e="T25" id="Seg_349" s="T24">kuza</ta>
            <ta e="T26" id="Seg_350" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_351" s="T26">beržə</ta>
            <ta e="T28" id="Seg_352" s="T27">măn</ta>
            <ta e="T29" id="Seg_353" s="T28">tože</ta>
            <ta e="T30" id="Seg_354" s="T29">kănzə-laːm-lV-m</ta>
            <ta e="T31" id="Seg_355" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_356" s="T31">kuja</ta>
            <ta e="T33" id="Seg_357" s="T32">bar</ta>
            <ta e="T34" id="Seg_358" s="T33">uʔbdə-bi</ta>
            <ta e="T35" id="Seg_359" s="T34">ejü</ta>
            <ta e="T36" id="Seg_360" s="T35">mo-laːm-bi</ta>
            <ta e="T37" id="Seg_361" s="T36">i</ta>
            <ta e="T38" id="Seg_362" s="T37">bar</ta>
            <ta e="T39" id="Seg_363" s="T38">beržə</ta>
            <ta e="T40" id="Seg_364" s="T39">kü-laːm-bi</ta>
            <ta e="T41" id="Seg_365" s="T40">i</ta>
            <ta e="T42" id="Seg_366" s="T41">šišəge</ta>
            <ta e="T43" id="Seg_367" s="T42">naga</ta>
            <ta e="T44" id="Seg_368" s="T43">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_369" s="T0">three-COLL</ta>
            <ta e="T3" id="Seg_370" s="T2">scold-INF.LAT</ta>
            <ta e="T4" id="Seg_371" s="T3">cold.[NOM.SG]</ta>
            <ta e="T5" id="Seg_372" s="T4">say-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_373" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_374" s="T6">freeze-RES-PST-1SG</ta>
            <ta e="T8" id="Seg_375" s="T7">and</ta>
            <ta e="T9" id="Seg_376" s="T8">wind.[NOM.SG]</ta>
            <ta e="T10" id="Seg_377" s="T9">say-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_378" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_379" s="T11">also</ta>
            <ta e="T13" id="Seg_380" s="T12">freeze-RES-FUT-2SG</ta>
            <ta e="T14" id="Seg_381" s="T13">and</ta>
            <ta e="T15" id="Seg_382" s="T14">sun.[NOM.SG]</ta>
            <ta e="T16" id="Seg_383" s="T15">I.NOM</ta>
            <ta e="T17" id="Seg_384" s="T16">I.NOM</ta>
            <ta e="T18" id="Seg_385" s="T17">become.warm-FUT-1SG</ta>
            <ta e="T19" id="Seg_386" s="T18">then</ta>
            <ta e="T20" id="Seg_387" s="T19">cold.[NOM.SG]</ta>
            <ta e="T21" id="Seg_388" s="T20">PTCL</ta>
            <ta e="T22" id="Seg_389" s="T21">very</ta>
            <ta e="T23" id="Seg_390" s="T22">freeze-RES-PST.[3SG]</ta>
            <ta e="T24" id="Seg_391" s="T23">freeze-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_392" s="T24">man.[NOM.SG]</ta>
            <ta e="T26" id="Seg_393" s="T25">then</ta>
            <ta e="T27" id="Seg_394" s="T26">wind.[NOM.SG]</ta>
            <ta e="T28" id="Seg_395" s="T27">I.NOM</ta>
            <ta e="T29" id="Seg_396" s="T28">also</ta>
            <ta e="T30" id="Seg_397" s="T29">freeze-RES-FUT-1SG</ta>
            <ta e="T31" id="Seg_398" s="T30">then</ta>
            <ta e="T32" id="Seg_399" s="T31">sun.[NOM.SG]</ta>
            <ta e="T33" id="Seg_400" s="T32">PTCL</ta>
            <ta e="T34" id="Seg_401" s="T33">get.up-PST.[3SG]</ta>
            <ta e="T35" id="Seg_402" s="T34">warm.[NOM.SG]</ta>
            <ta e="T36" id="Seg_403" s="T35">become-RES-PST.[3SG]</ta>
            <ta e="T37" id="Seg_404" s="T36">and</ta>
            <ta e="T38" id="Seg_405" s="T37">PTCL</ta>
            <ta e="T39" id="Seg_406" s="T38">wind.[NOM.SG]</ta>
            <ta e="T40" id="Seg_407" s="T39">die-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_408" s="T40">and</ta>
            <ta e="T42" id="Seg_409" s="T41">cold.[NOM.SG]</ta>
            <ta e="T43" id="Seg_410" s="T42">NEG.EX</ta>
            <ta e="T44" id="Seg_411" s="T43">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_412" s="T0">три-COLL</ta>
            <ta e="T3" id="Seg_413" s="T2">ругать-INF.LAT</ta>
            <ta e="T4" id="Seg_414" s="T3">холодный.[NOM.SG]</ta>
            <ta e="T5" id="Seg_415" s="T4">сказать-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_416" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_417" s="T6">замерзнуть-RES-PST-1SG</ta>
            <ta e="T8" id="Seg_418" s="T7">а</ta>
            <ta e="T9" id="Seg_419" s="T8">ветер.[NOM.SG]</ta>
            <ta e="T10" id="Seg_420" s="T9">сказать-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_421" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_422" s="T11">тоже</ta>
            <ta e="T13" id="Seg_423" s="T12">замерзнуть-RES-FUT-2SG</ta>
            <ta e="T14" id="Seg_424" s="T13">а</ta>
            <ta e="T15" id="Seg_425" s="T14">солнце.[NOM.SG]</ta>
            <ta e="T16" id="Seg_426" s="T15">я.NOM</ta>
            <ta e="T17" id="Seg_427" s="T16">я.NOM</ta>
            <ta e="T18" id="Seg_428" s="T17">нагреться-FUT-1SG</ta>
            <ta e="T19" id="Seg_429" s="T18">тогда</ta>
            <ta e="T20" id="Seg_430" s="T19">холодный.[NOM.SG]</ta>
            <ta e="T21" id="Seg_431" s="T20">PTCL</ta>
            <ta e="T22" id="Seg_432" s="T21">очень</ta>
            <ta e="T23" id="Seg_433" s="T22">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T24" id="Seg_434" s="T23">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T25" id="Seg_435" s="T24">мужчина.[NOM.SG]</ta>
            <ta e="T26" id="Seg_436" s="T25">тогда</ta>
            <ta e="T27" id="Seg_437" s="T26">ветер.[NOM.SG]</ta>
            <ta e="T28" id="Seg_438" s="T27">я.NOM</ta>
            <ta e="T29" id="Seg_439" s="T28">тоже</ta>
            <ta e="T30" id="Seg_440" s="T29">замерзнуть-RES-FUT-1SG</ta>
            <ta e="T31" id="Seg_441" s="T30">тогда</ta>
            <ta e="T32" id="Seg_442" s="T31">солнце.[NOM.SG]</ta>
            <ta e="T33" id="Seg_443" s="T32">PTCL</ta>
            <ta e="T34" id="Seg_444" s="T33">встать-PST.[3SG]</ta>
            <ta e="T35" id="Seg_445" s="T34">теплый.[NOM.SG]</ta>
            <ta e="T36" id="Seg_446" s="T35">стать-RES-PST.[3SG]</ta>
            <ta e="T37" id="Seg_447" s="T36">и</ta>
            <ta e="T38" id="Seg_448" s="T37">PTCL</ta>
            <ta e="T39" id="Seg_449" s="T38">ветер.[NOM.SG]</ta>
            <ta e="T40" id="Seg_450" s="T39">умереть-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_451" s="T40">и</ta>
            <ta e="T42" id="Seg_452" s="T41">холодный.[NOM.SG]</ta>
            <ta e="T43" id="Seg_453" s="T42">NEG.EX</ta>
            <ta e="T44" id="Seg_454" s="T43">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_455" s="T0">num-num&gt;num</ta>
            <ta e="T3" id="Seg_456" s="T2">v-v:n.fin</ta>
            <ta e="T4" id="Seg_457" s="T3">adj-n:case</ta>
            <ta e="T5" id="Seg_458" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_459" s="T5">pers</ta>
            <ta e="T7" id="Seg_460" s="T6">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_461" s="T7">conj</ta>
            <ta e="T9" id="Seg_462" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_463" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_464" s="T10">pers</ta>
            <ta e="T12" id="Seg_465" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_466" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_467" s="T13">conj</ta>
            <ta e="T15" id="Seg_468" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_469" s="T15">pers</ta>
            <ta e="T17" id="Seg_470" s="T16">pers</ta>
            <ta e="T18" id="Seg_471" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_472" s="T18">adv</ta>
            <ta e="T20" id="Seg_473" s="T19">adj-n:case</ta>
            <ta e="T21" id="Seg_474" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_475" s="T21">adv</ta>
            <ta e="T23" id="Seg_476" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_477" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_478" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_479" s="T25">adv</ta>
            <ta e="T27" id="Seg_480" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_481" s="T27">pers</ta>
            <ta e="T29" id="Seg_482" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_483" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_484" s="T30">adv</ta>
            <ta e="T32" id="Seg_485" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_486" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_487" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_488" s="T34">adj-n:case</ta>
            <ta e="T36" id="Seg_489" s="T35">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_490" s="T36">conj</ta>
            <ta e="T38" id="Seg_491" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_492" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_493" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_494" s="T40">conj</ta>
            <ta e="T42" id="Seg_495" s="T41">adj-n:case</ta>
            <ta e="T43" id="Seg_496" s="T42">v</ta>
            <ta e="T44" id="Seg_497" s="T43">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_498" s="T0">num</ta>
            <ta e="T3" id="Seg_499" s="T2">v</ta>
            <ta e="T4" id="Seg_500" s="T3">adj</ta>
            <ta e="T5" id="Seg_501" s="T4">v</ta>
            <ta e="T6" id="Seg_502" s="T5">pers</ta>
            <ta e="T7" id="Seg_503" s="T6">v</ta>
            <ta e="T8" id="Seg_504" s="T7">conj</ta>
            <ta e="T9" id="Seg_505" s="T8">n</ta>
            <ta e="T10" id="Seg_506" s="T9">v</ta>
            <ta e="T11" id="Seg_507" s="T10">pers</ta>
            <ta e="T12" id="Seg_508" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_509" s="T12">v</ta>
            <ta e="T14" id="Seg_510" s="T13">conj</ta>
            <ta e="T15" id="Seg_511" s="T14">n</ta>
            <ta e="T16" id="Seg_512" s="T15">pers</ta>
            <ta e="T17" id="Seg_513" s="T16">pers</ta>
            <ta e="T18" id="Seg_514" s="T17">v</ta>
            <ta e="T19" id="Seg_515" s="T18">adv</ta>
            <ta e="T20" id="Seg_516" s="T19">adj</ta>
            <ta e="T21" id="Seg_517" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_518" s="T21">adv</ta>
            <ta e="T23" id="Seg_519" s="T22">v</ta>
            <ta e="T24" id="Seg_520" s="T23">v</ta>
            <ta e="T25" id="Seg_521" s="T24">n</ta>
            <ta e="T26" id="Seg_522" s="T25">adv</ta>
            <ta e="T27" id="Seg_523" s="T26">n</ta>
            <ta e="T28" id="Seg_524" s="T27">pers</ta>
            <ta e="T29" id="Seg_525" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_526" s="T29">v</ta>
            <ta e="T31" id="Seg_527" s="T30">adv</ta>
            <ta e="T32" id="Seg_528" s="T31">n</ta>
            <ta e="T33" id="Seg_529" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_530" s="T33">v</ta>
            <ta e="T35" id="Seg_531" s="T34">adj</ta>
            <ta e="T36" id="Seg_532" s="T35">v</ta>
            <ta e="T37" id="Seg_533" s="T36">conj</ta>
            <ta e="T38" id="Seg_534" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_535" s="T38">n</ta>
            <ta e="T40" id="Seg_536" s="T39">v</ta>
            <ta e="T41" id="Seg_537" s="T40">conj</ta>
            <ta e="T42" id="Seg_538" s="T41">adj</ta>
            <ta e="T43" id="Seg_539" s="T42">v</ta>
            <ta e="T44" id="Seg_540" s="T43">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_541" s="T0">np.h:A</ta>
            <ta e="T4" id="Seg_542" s="T3">np.h:A</ta>
            <ta e="T6" id="Seg_543" s="T5">pro.h:A</ta>
            <ta e="T9" id="Seg_544" s="T8">np.h:A</ta>
            <ta e="T11" id="Seg_545" s="T10">pro.h:A</ta>
            <ta e="T15" id="Seg_546" s="T14">np.h:A</ta>
            <ta e="T17" id="Seg_547" s="T16">pro.h:A</ta>
            <ta e="T19" id="Seg_548" s="T18">adv:Time</ta>
            <ta e="T20" id="Seg_549" s="T19">np.h:A</ta>
            <ta e="T25" id="Seg_550" s="T24">np.h:E</ta>
            <ta e="T26" id="Seg_551" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_552" s="T26">np.h:A</ta>
            <ta e="T28" id="Seg_553" s="T27">pro.h:A</ta>
            <ta e="T31" id="Seg_554" s="T30">adv:Time</ta>
            <ta e="T32" id="Seg_555" s="T31">np.h:A</ta>
            <ta e="T39" id="Seg_556" s="T38">np.h:P</ta>
            <ta e="T42" id="Seg_557" s="T41">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_558" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_559" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_560" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_561" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_562" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_563" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_564" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_565" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_566" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_567" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_568" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_569" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_570" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_571" s="T19">np.h:S</ta>
            <ta e="T23" id="Seg_572" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_573" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_574" s="T24">np.h:O</ta>
            <ta e="T27" id="Seg_575" s="T26">np.h:S</ta>
            <ta e="T28" id="Seg_576" s="T27">pro.h:S</ta>
            <ta e="T30" id="Seg_577" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_578" s="T31">np.h:S</ta>
            <ta e="T34" id="Seg_579" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_580" s="T34">adj:pred</ta>
            <ta e="T36" id="Seg_581" s="T35">cop 0.3:S</ta>
            <ta e="T39" id="Seg_582" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_583" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_584" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_585" s="T42">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_586" s="T7">RUS:gram</ta>
            <ta e="T12" id="Seg_587" s="T11">RUS:mod</ta>
            <ta e="T14" id="Seg_588" s="T13">RUS:gram</ta>
            <ta e="T21" id="Seg_589" s="T20">TURK:disc</ta>
            <ta e="T29" id="Seg_590" s="T28">RUS:mod</ta>
            <ta e="T33" id="Seg_591" s="T32">TURK:disc</ta>
            <ta e="T37" id="Seg_592" s="T36">RUS:gram</ta>
            <ta e="T38" id="Seg_593" s="T37">TURK:disc</ta>
            <ta e="T41" id="Seg_594" s="T40">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T2" id="Seg_595" s="T1">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_596" s="T0">Трое начали спорить.</ta>
            <ta e="T7" id="Seg_597" s="T3">Холод говорит: "Я заморожу".</ta>
            <ta e="T13" id="Seg_598" s="T7">А ветер говорит: "Я тоже заморожу".</ta>
            <ta e="T18" id="Seg_599" s="T13">А солнце: "Я согрею".</ta>
            <ta e="T25" id="Seg_600" s="T18">Потом холод сильно заморозил</ta>
            <ta e="T30" id="Seg_601" s="T25">А ветер: "Я тоже заморожу".</ta>
            <ta e="T34" id="Seg_602" s="T30">Тогда солнце встало.</ta>
            <ta e="T36" id="Seg_603" s="T34">Стало тепло.</ta>
            <ta e="T43" id="Seg_604" s="T36">И ветер умер, и холода больше нет.</ta>
            <ta e="T44" id="Seg_605" s="T43">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_606" s="T0">Three [persons] started discussing.</ta>
            <ta e="T7" id="Seg_607" s="T3">The frost says: "I [will] freeze."</ta>
            <ta e="T13" id="Seg_608" s="T7">And the wind says: "I will also freeze."</ta>
            <ta e="T18" id="Seg_609" s="T13">And the sun: "I will warm."</ta>
            <ta e="T25" id="Seg_610" s="T18">Then the frost froze very [much], froze the man.</ta>
            <ta e="T30" id="Seg_611" s="T25">Then the wind: "I also freeze."</ta>
            <ta e="T34" id="Seg_612" s="T30">Then the sun got up.</ta>
            <ta e="T36" id="Seg_613" s="T34">It became warm.</ta>
            <ta e="T43" id="Seg_614" s="T36">And the wind died and there was no frost.</ta>
            <ta e="T44" id="Seg_615" s="T43">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_616" s="T0">Drei fingen an zu streiten.</ta>
            <ta e="T7" id="Seg_617" s="T3">Der Frost sagt: "Ich [werde] frieren lassen."</ta>
            <ta e="T13" id="Seg_618" s="T7">Und der Wind sagt: "Ich [werde] auch frieren lassen."</ta>
            <ta e="T18" id="Seg_619" s="T13">Und die Sonne: "Ich [werde] es wärmen."</ta>
            <ta e="T25" id="Seg_620" s="T18">Dann ließ der Frost sehr frieren, er ließ den Mann frieren.</ta>
            <ta e="T30" id="Seg_621" s="T25">Dann der Wind: "Ich lasse auch frieren."</ta>
            <ta e="T34" id="Seg_622" s="T30">Dann ging die Sonne auf.</ta>
            <ta e="T36" id="Seg_623" s="T34">Es wurde warm.</ta>
            <ta e="T43" id="Seg_624" s="T36">Und der Wind starb und es war kein Frost mehr da.</ta>
            <ta e="T44" id="Seg_625" s="T43">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_626" s="T0">[GVY:] A rather approximative recounting of a story about Sun, Wind and Frost, see e.g. http://hyaenidae.narod.ru/story5/308.html </ta>
            <ta e="T13" id="Seg_627" s="T7">[KlT:] 2SG instead of 1SG.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
