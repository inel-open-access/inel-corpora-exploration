<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC8ECC963-695C-E644-382E-0F3851B76537">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_NikitaKozhemyaka_flk.wav" />
         <referenced-file url="PKZ_196X_NikitaKozhemyaka_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_NikitaKozhemyaka_flk\PKZ_196X_NikitaKozhemyaka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">249</ud-information>
            <ud-information attribute-name="# HIAT:w">169</ud-information>
            <ud-information attribute-name="# e">167</ud-information>
            <ud-information attribute-name="# HIAT:u">39</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1898" time="0.0" type="appl" />
         <tli id="T1899" time="1.631" type="appl" />
         <tli id="T1900" time="3.6666497532173987" />
         <tli id="T1901" time="4.134" type="appl" />
         <tli id="T1902" time="5.006" type="appl" />
         <tli id="T1903" time="5.878" type="appl" />
         <tli id="T1904" time="6.75" type="appl" />
         <tli id="T1905" time="7.81" type="appl" />
         <tli id="T1906" time="8.87" type="appl" />
         <tli id="T1907" time="9.931" type="appl" />
         <tli id="T1908" time="10.991" type="appl" />
         <tli id="T1909" time="11.541" type="appl" />
         <tli id="T1910" time="12.092" type="appl" />
         <tli id="T1911" time="12.642" type="appl" />
         <tli id="T1912" time="13.335" type="appl" />
         <tli id="T1913" time="14.028" type="appl" />
         <tli id="T1914" time="14.721" type="appl" />
         <tli id="T1915" time="15.556" type="appl" />
         <tli id="T1916" time="16.39" type="appl" />
         <tli id="T1917" time="17.224" type="appl" />
         <tli id="T1918" time="18.059" type="appl" />
         <tli id="T1919" time="18.894" type="appl" />
         <tli id="T1920" time="19.728" type="appl" />
         <tli id="T1921" time="20.451" type="appl" />
         <tli id="T1922" time="21.166" type="appl" />
         <tli id="T1923" time="21.88" type="appl" />
         <tli id="T1924" time="22.595" type="appl" />
         <tli id="T1925" time="23.309" type="appl" />
         <tli id="T1926" time="24.024" type="appl" />
         <tli id="T1927" time="24.86655196272891" />
         <tli id="T1928" time="25.68" type="appl" />
         <tli id="T1929" time="26.621" type="appl" />
         <tli id="T1930" time="27.563" type="appl" />
         <tli id="T1931" time="28.505" type="appl" />
         <tli id="T1932" time="29.447" type="appl" />
         <tli id="T1933" time="30.388" type="appl" />
         <tli id="T1934" time="31.33" type="appl" />
         <tli id="T1935" time="32.733182342358965" />
         <tli id="T1936" time="33.395" type="appl" />
         <tli id="T1937" time="33.996" type="appl" />
         <tli id="T1938" time="34.596" type="appl" />
         <tli id="T1939" time="35.196" type="appl" />
         <tli id="T1940" time="35.797" type="appl" />
         <tli id="T1941" time="37.426494026477236" />
         <tli id="T1942" time="38.422" type="appl" />
         <tli id="T1943" time="39.461" type="appl" />
         <tli id="T1944" time="40.5" type="appl" />
         <tli id="T1945" time="41.538" type="appl" />
         <tli id="T1946" time="42.232" type="appl" />
         <tli id="T1947" time="42.927" type="appl" />
         <tli id="T1948" time="43.621" type="appl" />
         <tli id="T1949" time="44.479" type="appl" />
         <tli id="T1950" time="45.338" type="appl" />
         <tli id="T1951" time="46.833117302458604" />
         <tli id="T1952" time="47.535" type="appl" />
         <tli id="T1953" time="48.283" type="appl" />
         <tli id="T1954" time="49.031" type="appl" />
         <tli id="T1955" time="49.591" type="appl" />
         <tli id="T1956" time="50.151" type="appl" />
         <tli id="T1957" time="50.711" type="appl" />
         <tli id="T1958" time="51.514" type="appl" />
         <tli id="T1959" time="52.317" type="appl" />
         <tli id="T1960" time="53.12" type="appl" />
         <tli id="T1961" time="53.924" type="appl" />
         <tli id="T1962" time="54.727" type="appl" />
         <tli id="T1963" time="56.546405830527235" />
         <tli id="T1964" time="57.212" type="appl" />
         <tli id="T1965" time="57.832" type="appl" />
         <tli id="T1966" time="58.452" type="appl" />
         <tli id="T1967" time="59.072" type="appl" />
         <tli id="T1968" time="59.885" type="appl" />
         <tli id="T1969" time="60.698" type="appl" />
         <tli id="T1970" time="61.511" type="appl" />
         <tli id="T1971" time="62.324" type="appl" />
         <tli id="T1972" time="62.982" type="appl" />
         <tli id="T1973" time="63.641" type="appl" />
         <tli id="T1974" time="64.299" type="appl" />
         <tli id="T1975" time="64.957" type="appl" />
         <tli id="T1976" time="65.615" type="appl" />
         <tli id="T1977" time="66.274" type="appl" />
         <tli id="T1978" time="66.932" type="appl" />
         <tli id="T1979" time="67.629" type="appl" />
         <tli id="T1980" time="68.326" type="appl" />
         <tli id="T1981" time="69.024" type="appl" />
         <tli id="T1982" time="69.721" type="appl" />
         <tli id="T1983" time="70.418" type="appl" />
         <tli id="T1984" time="71.167" type="appl" />
         <tli id="T1985" time="71.916" type="appl" />
         <tli id="T1986" time="72.81966409889756" />
         <tli id="T1987" time="74.039" type="appl" />
         <tli id="T1988" time="75.414" type="appl" />
         <tli id="T1989" time="76.788" type="appl" />
         <tli id="T1990" time="78.084" type="appl" />
         <tli id="T1991" time="79.6529659117118" />
         <tli id="T1992" time="81.448" type="appl" />
         <tli id="T1993" time="82.186" type="appl" />
         <tli id="T1994" time="84.45294377046912" />
         <tli id="T1995" time="85.862" type="appl" />
         <tli id="T1996" time="87.64626237372572" />
         <tli id="T1997" time="88.462" type="appl" />
         <tli id="T1998" time="89.19" type="appl" />
         <tli id="T1999" time="89.918" type="appl" />
         <tli id="T2000" time="90.646" type="appl" />
         <tli id="T2001" time="91.374" type="appl" />
         <tli id="T2002" time="92.03" type="appl" />
         <tli id="T2003" time="92.686" type="appl" />
         <tli id="T2004" time="93.343" type="appl" />
         <tli id="T2005" time="93.999" type="appl" />
         <tli id="T2006" time="94.576" type="appl" />
         <tli id="T2007" time="95.152" type="appl" />
         <tli id="T2008" time="95.729" type="appl" />
         <tli id="T2009" time="96.483" type="appl" />
         <tli id="T2010" time="97.238" type="appl" />
         <tli id="T2011" time="98.389" type="appl" />
         <tli id="T2012" time="99.399" type="appl" />
         <tli id="T2013" time="100.031" type="appl" />
         <tli id="T2014" time="100.662" type="appl" />
         <tli id="T2015" time="101.294" type="appl" />
         <tli id="T2016" time="102.116" type="appl" />
         <tli id="T2017" time="102.939" type="appl" />
         <tli id="T2018" time="103.762" type="appl" />
         <tli id="T2019" time="105.17951483001984" />
         <tli id="T2020" time="106.002" type="appl" />
         <tli id="T2021" time="106.556" type="appl" />
         <tli id="T2022" time="107.11" type="appl" />
         <tli id="T2023" time="107.665" type="appl" />
         <tli id="T2024" time="108.22" type="appl" />
         <tli id="T2025" time="108.774" type="appl" />
         <tli id="T2026" time="109.347" type="appl" />
         <tli id="T2027" time="109.92" type="appl" />
         <tli id="T2028" time="110.492" type="appl" />
         <tli id="T2029" time="111.065" type="appl" />
         <tli id="T2030" time="112.358" type="appl" />
         <tli id="T2031" time="113.321" type="appl" />
         <tli id="T2032" time="114.283" type="appl" />
         <tli id="T2033" time="115.063" type="appl" />
         <tli id="T2034" time="115.844" type="appl" />
         <tli id="T2035" time="116.624" type="appl" />
         <tli id="T2036" time="117.773" type="appl" />
         <tli id="T2037" time="118.922" type="appl" />
         <tli id="T2038" time="120.072" type="appl" />
         <tli id="T2039" time="121.221" type="appl" />
         <tli id="T2040" time="122.37" type="appl" />
         <tli id="T2041" time="123.519" type="appl" />
         <tli id="T2042" time="124.309" type="appl" />
         <tli id="T2043" time="125.099" type="appl" />
         <tli id="T2044" time="125.889" type="appl" />
         <tli id="T2045" time="126.68" type="appl" />
         <tli id="T2046" time="127.47" type="appl" />
         <tli id="T2047" time="128.26" type="appl" />
         <tli id="T2048" time="129.05" type="appl" />
         <tli id="T2049" time="129.84" type="appl" />
         <tli id="T2050" time="130.495" type="appl" />
         <tli id="T2051" time="131.149" type="appl" />
         <tli id="T2052" time="131.804" type="appl" />
         <tli id="T2053" time="132.459" type="appl" />
         <tli id="T2054" time="133.114" type="appl" />
         <tli id="T2055" time="133.768" type="appl" />
         <tli id="T2056" time="134.423" type="appl" />
         <tli id="T2057" time="135.861" type="appl" />
         <tli id="T2058" time="137.3" type="appl" />
         <tli id="T2059" time="138.738" type="appl" />
         <tli id="T2060" time="140.176" type="appl" />
         <tli id="T2061" time="141.043" type="appl" />
         <tli id="T2062" time="141.91" type="appl" />
         <tli id="T2063" time="142.778" type="appl" />
         <tli id="T2064" time="143.645" type="appl" />
         <tli id="T2065" time="144.526" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T1998" start="T1997">
            <tli id="T1997.tx.1" />
         </timeline-fork>
         <timeline-fork end="T1999" start="T1998">
            <tli id="T1998.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2065" id="Seg_0" n="sc" s="T1898">
               <ts e="T1900" id="Seg_2" n="HIAT:u" s="T1898">
                  <ts e="T1899" id="Seg_4" n="HIAT:w" s="T1898">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1900" id="Seg_7" n="HIAT:w" s="T1899">pĭnzə</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1904" id="Seg_11" n="HIAT:u" s="T1900">
                  <ts e="T1901" id="Seg_13" n="HIAT:w" s="T1900">Ugaːndə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1902" id="Seg_16" n="HIAT:w" s="T1901">esseŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1903" id="Seg_19" n="HIAT:w" s="T1902">iʔgö</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1904" id="Seg_22" n="HIAT:w" s="T1903">ambi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1908" id="Seg_26" n="HIAT:u" s="T1904">
                  <ts e="T1905" id="Seg_28" n="HIAT:w" s="T1904">Dĭgəttə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1906" id="Seg_31" n="HIAT:w" s="T1905">koŋgən</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1907" id="Seg_34" n="HIAT:w" s="T1906">koʔbdot</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1908" id="Seg_37" n="HIAT:w" s="T1907">kunnaːmbi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1911" id="Seg_41" n="HIAT:u" s="T1908">
                  <ts e="T1909" id="Seg_43" n="HIAT:w" s="T1908">Dĭm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1910" id="Seg_46" n="HIAT:w" s="T1909">ej</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1911" id="Seg_49" n="HIAT:w" s="T1910">ambi</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1914" id="Seg_53" n="HIAT:u" s="T1911">
                  <ts e="T1912" id="Seg_55" n="HIAT:w" s="T1911">Dĭn</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1913" id="Seg_58" n="HIAT:w" s="T1912">men</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1914" id="Seg_61" n="HIAT:w" s="T1913">ibi</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1920" id="Seg_65" n="HIAT:u" s="T1914">
                  <ts e="T1915" id="Seg_67" n="HIAT:w" s="T1914">Dĭgəttə</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1916" id="Seg_70" n="HIAT:w" s="T1915">dĭ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1917" id="Seg_73" n="HIAT:w" s="T1916">pʼaŋdələj</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1918" id="Seg_76" n="HIAT:w" s="T1917">sazəndə</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1919" id="Seg_80" n="HIAT:w" s="T1918">abandə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1920" id="Seg_83" n="HIAT:w" s="T1919">öʔləj</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1927" id="Seg_87" n="HIAT:u" s="T1920">
                  <ts e="T1921" id="Seg_89" n="HIAT:w" s="T1920">Men</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ts e="T1922" id="Seg_93" n="HIAT:w" s="T1921">mene-</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1923" id="Seg_96" n="HIAT:w" s="T1922">di-</ts>
                  <nts id="Seg_97" n="HIAT:ip">)</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1924" id="Seg_100" n="HIAT:w" s="T1923">dĭzeŋgəʔ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1925" id="Seg_103" n="HIAT:w" s="T1924">detləj</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1926" id="Seg_106" n="HIAT:w" s="T1925">sazən</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1927" id="Seg_109" n="HIAT:w" s="T1926">dĭʔnə</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1935" id="Seg_113" n="HIAT:u" s="T1927">
                  <ts e="T1928" id="Seg_115" n="HIAT:w" s="T1927">Dĭgəttə</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1929" id="Seg_118" n="HIAT:w" s="T1928">ijat</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1930" id="Seg_122" n="HIAT:w" s="T1929">abat</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1931" id="Seg_125" n="HIAT:w" s="T1930">măndə:</ts>
                  <nts id="Seg_126" n="HIAT:ip">"</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1932" id="Seg_129" n="HIAT:w" s="T1931">Suraraʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1933" id="Seg_133" n="HIAT:w" s="T1932">šində</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1934" id="Seg_136" n="HIAT:w" s="T1933">kuštü</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1935" id="Seg_139" n="HIAT:w" s="T1934">dĭʔə</ts>
                  <nts id="Seg_140" n="HIAT:ip">"</nts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1945" id="Seg_144" n="HIAT:u" s="T1935">
                  <ts e="T1936" id="Seg_146" n="HIAT:w" s="T1935">Dĭ</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1937" id="Seg_149" n="HIAT:w" s="T1936">davaj</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1938" id="Seg_152" n="HIAT:w" s="T1937">surarzittə</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1939" id="Seg_156" n="HIAT:w" s="T1938">dĭ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1940" id="Seg_159" n="HIAT:w" s="T1939">bar</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1941" id="Seg_162" n="HIAT:w" s="T1940">nörbəbi:</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T1942" id="Seg_166" n="HIAT:w" s="T1941">Nikita</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1943" id="Seg_169" n="HIAT:w" s="T1942">Kožemʼaka</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1944" id="Seg_172" n="HIAT:w" s="T1943">kuštü</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1945" id="Seg_175" n="HIAT:w" s="T1944">măna</ts>
                  <nts id="Seg_176" n="HIAT:ip">"</nts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1948" id="Seg_180" n="HIAT:u" s="T1945">
                  <ts e="T1946" id="Seg_182" n="HIAT:w" s="T1945">Dĭgəttə</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1947" id="Seg_185" n="HIAT:w" s="T1946">dĭ</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1948" id="Seg_188" n="HIAT:w" s="T1947">öʔlüʔbi</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1951" id="Seg_192" n="HIAT:u" s="T1948">
                  <ts e="T1949" id="Seg_194" n="HIAT:w" s="T1948">Dĭzeŋ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1950" id="Seg_197" n="HIAT:w" s="T1949">kambiʔi</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1951" id="Seg_201" n="HIAT:w" s="T1950">šidegöʔ</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1954" id="Seg_205" n="HIAT:u" s="T1951">
                  <ts e="T1952" id="Seg_207" n="HIAT:w" s="T1951">Dĭ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1953" id="Seg_210" n="HIAT:w" s="T1952">kubaʔi</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1954" id="Seg_213" n="HIAT:w" s="T1953">abiʔi</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1957" id="Seg_217" n="HIAT:u" s="T1954">
                  <nts id="Seg_218" n="HIAT:ip">(</nts>
                  <ts e="T1955" id="Seg_220" n="HIAT:w" s="T1954">Ab-</ts>
                  <nts id="Seg_221" n="HIAT:ip">)</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1956" id="Seg_224" n="HIAT:w" s="T1955">Alaʔbi</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1957" id="Seg_227" n="HIAT:w" s="T1956">iʔgö</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1963" id="Seg_231" n="HIAT:u" s="T1957">
                  <ts e="T1958" id="Seg_233" n="HIAT:w" s="T1957">Neröluʔpi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1959" id="Seg_236" n="HIAT:w" s="T1958">što</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1960" id="Seg_239" n="HIAT:w" s="T1959">koŋ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1961" id="Seg_242" n="HIAT:w" s="T1960">bostə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1962" id="Seg_245" n="HIAT:w" s="T1961">netsiʔ</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1963" id="Seg_248" n="HIAT:w" s="T1962">šonəga</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1967" id="Seg_252" n="HIAT:u" s="T1963">
                  <ts e="T1964" id="Seg_254" n="HIAT:w" s="T1963">I</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1965" id="Seg_257" n="HIAT:w" s="T1964">kubaʔi</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T1966" id="Seg_261" n="HIAT:w" s="T1965">ša-</ts>
                  <nts id="Seg_262" n="HIAT:ip">)</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1967" id="Seg_265" n="HIAT:w" s="T1966">sajnʼeʔluʔpi</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1971" id="Seg_269" n="HIAT:u" s="T1967">
                  <ts e="T1968" id="Seg_271" n="HIAT:w" s="T1967">Dĭgəttə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1969" id="Seg_274" n="HIAT:w" s="T1968">ej</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1970" id="Seg_277" n="HIAT:w" s="T1969">kambi</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1971" id="Seg_280" n="HIAT:w" s="T1970">dĭzeŋ</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1978" id="Seg_284" n="HIAT:u" s="T1971">
                  <ts e="T1972" id="Seg_286" n="HIAT:w" s="T1971">A</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_288" n="HIAT:ip">(</nts>
                  <ts e="T1973" id="Seg_290" n="HIAT:w" s="T1972">dĭgət-</ts>
                  <nts id="Seg_291" n="HIAT:ip">)</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1974" id="Seg_294" n="HIAT:w" s="T1973">dĭgəttə</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1975" id="Seg_297" n="HIAT:w" s="T1974">dĭ</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1976" id="Seg_300" n="HIAT:w" s="T1975">iʔgö</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1977" id="Seg_303" n="HIAT:w" s="T1976">esseŋ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1978" id="Seg_306" n="HIAT:w" s="T1977">oʔbdobi</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1983" id="Seg_310" n="HIAT:u" s="T1978">
                  <ts e="T1979" id="Seg_312" n="HIAT:w" s="T1978">Esseŋ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_314" n="HIAT:ip">(</nts>
                  <ts e="T1980" id="Seg_316" n="HIAT:w" s="T1979">kabi-</ts>
                  <nts id="Seg_317" n="HIAT:ip">)</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1981" id="Seg_320" n="HIAT:w" s="T1980">kambiʔi</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1982" id="Seg_323" n="HIAT:w" s="T1981">bar</ts>
                  <nts id="Seg_324" n="HIAT:ip">,</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1983" id="Seg_327" n="HIAT:w" s="T1982">dʼorbiʔi</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1986" id="Seg_331" n="HIAT:u" s="T1983">
                  <ts e="T1984" id="Seg_333" n="HIAT:w" s="T1983">Dĭ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1985" id="Seg_336" n="HIAT:w" s="T1984">dĭgəttə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1986" id="Seg_339" n="HIAT:w" s="T1985">kambi</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1989" id="Seg_343" n="HIAT:u" s="T1986">
                  <ts e="T1987" id="Seg_345" n="HIAT:w" s="T1986">Iʔgö</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1988" id="Seg_348" n="HIAT:w" s="T1987">kudʼelʼə</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1989" id="Seg_351" n="HIAT:w" s="T1988">ibi</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1991" id="Seg_355" n="HIAT:u" s="T1989">
                  <nts id="Seg_356" n="HIAT:ip">(</nts>
                  <ts e="T1990" id="Seg_358" n="HIAT:w" s="T1989">Dʼögatʼsiʔ</ts>
                  <nts id="Seg_359" n="HIAT:ip">)</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1991" id="Seg_362" n="HIAT:w" s="T1990">tʼuʔpi</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1992" id="Seg_366" n="HIAT:u" s="T1991">
                  <ts e="T1992" id="Seg_368" n="HIAT:w" s="T1991">Kürleʔpi</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1994" id="Seg_372" n="HIAT:u" s="T1992">
                  <ts e="T1993" id="Seg_374" n="HIAT:w" s="T1992">Dĭgəttə</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1994" id="Seg_377" n="HIAT:w" s="T1993">šobi</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1996" id="Seg_381" n="HIAT:u" s="T1994">
                  <ts e="T1995" id="Seg_383" n="HIAT:w" s="T1994">Pĭnzət</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1996" id="Seg_386" n="HIAT:w" s="T1995">berlogəttə</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2001" id="Seg_390" n="HIAT:u" s="T1996">
                  <nts id="Seg_391" n="HIAT:ip">"</nts>
                  <ts e="T1997" id="Seg_393" n="HIAT:w" s="T1996">Supsoʔ</ts>
                  <nts id="Seg_394" n="HIAT:ip">,</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1997.tx.1" id="Seg_397" n="HIAT:w" s="T1997">a</ts>
                  <nts id="Seg_398" n="HIAT:ip">_</nts>
                  <ts e="T1998" id="Seg_400" n="HIAT:w" s="T1997.tx.1">to</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1998.tx.1" id="Seg_403" n="HIAT:w" s="T1998">vsʼo</ts>
                  <nts id="Seg_404" n="HIAT:ip">_</nts>
                  <ts e="T1999" id="Seg_406" n="HIAT:w" s="T1998.tx.1">ravno</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2000" id="Seg_409" n="HIAT:w" s="T1999">tănan</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2001" id="Seg_412" n="HIAT:w" s="T2000">kutlam</ts>
                  <nts id="Seg_413" n="HIAT:ip">!</nts>
                  <nts id="Seg_414" n="HIAT:ip">"</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2005" id="Seg_417" n="HIAT:u" s="T2001">
                  <ts e="T2002" id="Seg_419" n="HIAT:w" s="T2001">Dĭgəttə</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2003" id="Seg_422" n="HIAT:w" s="T2002">paʔi</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2004" id="Seg_425" n="HIAT:w" s="T2003">bar</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2005" id="Seg_428" n="HIAT:w" s="T2004">ileʔbə</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2008" id="Seg_432" n="HIAT:u" s="T2005">
                  <ts e="T2006" id="Seg_434" n="HIAT:w" s="T2005">Dĭgəttə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2007" id="Seg_437" n="HIAT:w" s="T2006">dĭ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2008" id="Seg_440" n="HIAT:w" s="T2007">supsobi</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2010" id="Seg_444" n="HIAT:u" s="T2008">
                  <ts e="T2009" id="Seg_446" n="HIAT:w" s="T2008">Davaj</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_448" n="HIAT:ip">(</nts>
                  <ts e="T2010" id="Seg_450" n="HIAT:w" s="T2009">dʼabərolbi</ts>
                  <nts id="Seg_451" n="HIAT:ip">)</nts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2012" id="Seg_455" n="HIAT:u" s="T2010">
                  <ts e="T2011" id="Seg_457" n="HIAT:w" s="T2010">Dʼabərobiʔi</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2012" id="Seg_461" n="HIAT:w" s="T2011">dʼabərobiʔi</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2019" id="Seg_465" n="HIAT:u" s="T2012">
                  <ts e="T2013" id="Seg_467" n="HIAT:w" s="T2012">Dĭgəttə</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2014" id="Seg_470" n="HIAT:w" s="T2013">pĭnzə</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2015" id="Seg_473" n="HIAT:w" s="T2014">măndə:</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_475" n="HIAT:ip">"</nts>
                  <ts e="T2016" id="Seg_477" n="HIAT:w" s="T2015">Ĭmbi</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_479" n="HIAT:ip">(</nts>
                  <ts e="T2017" id="Seg_481" n="HIAT:w" s="T2016">n-</ts>
                  <nts id="Seg_482" n="HIAT:ip">)</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2018" id="Seg_485" n="HIAT:w" s="T2017">miʔnʼibeʔ</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2019" id="Seg_488" n="HIAT:w" s="T2018">dʼabərozittə</ts>
                  <nts id="Seg_489" n="HIAT:ip">?</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2025" id="Seg_492" n="HIAT:u" s="T2019">
                  <ts e="T2020" id="Seg_494" n="HIAT:w" s="T2019">Tăn</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2021" id="Seg_497" n="HIAT:w" s="T2020">pʼeldə</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_499" n="HIAT:ip">(</nts>
                  <ts e="T2022" id="Seg_501" n="HIAT:w" s="T2021">budi-</ts>
                  <nts id="Seg_502" n="HIAT:ip">)</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2023" id="Seg_505" n="HIAT:w" s="T2022">budʼet</ts>
                  <nts id="Seg_506" n="HIAT:ip">,</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2024" id="Seg_509" n="HIAT:w" s="T2023">măn</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2025" id="Seg_512" n="HIAT:w" s="T2024">pʼeldə</ts>
                  <nts id="Seg_513" n="HIAT:ip">"</nts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2032" id="Seg_517" n="HIAT:u" s="T2025">
                  <ts e="T2026" id="Seg_519" n="HIAT:w" s="T2025">Dĭgəttə</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2027" id="Seg_522" n="HIAT:w" s="T2026">dĭ</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_524" n="HIAT:ip">(</nts>
                  <ts e="T2028" id="Seg_526" n="HIAT:w" s="T2027">mănə-</ts>
                  <nts id="Seg_527" n="HIAT:ip">)</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2029" id="Seg_530" n="HIAT:w" s="T2028">măndə:</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_532" n="HIAT:ip">"</nts>
                  <ts e="T2030" id="Seg_534" n="HIAT:w" s="T2029">Nada</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2031" id="Seg_537" n="HIAT:w" s="T2030">granʼ</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2032" id="Seg_540" n="HIAT:w" s="T2031">enzittə</ts>
                  <nts id="Seg_541" n="HIAT:ip">"</nts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2035" id="Seg_545" n="HIAT:u" s="T2032">
                  <ts e="T2033" id="Seg_547" n="HIAT:w" s="T2032">Dĭgəttə</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2034" id="Seg_550" n="HIAT:w" s="T2033">dĭm</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2035" id="Seg_553" n="HIAT:w" s="T2034">körerbi</ts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2041" id="Seg_557" n="HIAT:u" s="T2035">
                  <ts e="T2036" id="Seg_559" n="HIAT:w" s="T2035">Dĭ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2037" id="Seg_562" n="HIAT:w" s="T2036">bar</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2038" id="Seg_565" n="HIAT:w" s="T2037">dʼübə</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2039" id="Seg_568" n="HIAT:w" s="T2038">ugaːndə</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2040" id="Seg_571" n="HIAT:w" s="T2039">iʔgö</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2041" id="Seg_574" n="HIAT:w" s="T2040">tĭlbi</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2049" id="Seg_578" n="HIAT:u" s="T2041">
                  <ts e="T2042" id="Seg_580" n="HIAT:w" s="T2041">Dĭgəttə</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2043" id="Seg_583" n="HIAT:w" s="T2042">măndə:</ts>
                  <nts id="Seg_584" n="HIAT:ip">"</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2044" id="Seg_587" n="HIAT:w" s="T2043">Nada</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2045" id="Seg_590" n="HIAT:w" s="T2044">bü</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2046" id="Seg_593" n="HIAT:w" s="T2045">tože</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2047" id="Seg_596" n="HIAT:w" s="T2046">šide</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2048" id="Seg_599" n="HIAT:w" s="T2047">pʼeldə</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2049" id="Seg_602" n="HIAT:w" s="T2048">azittə</ts>
                  <nts id="Seg_603" n="HIAT:ip">"</nts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2056" id="Seg_607" n="HIAT:u" s="T2049">
                  <ts e="T2050" id="Seg_609" n="HIAT:w" s="T2049">Dĭ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2051" id="Seg_612" n="HIAT:w" s="T2050">bünə</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2052" id="Seg_615" n="HIAT:w" s="T2051">kambi</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2053" id="Seg_618" n="HIAT:w" s="T2052">i</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2054" id="Seg_621" n="HIAT:w" s="T2053">dĭ</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2055" id="Seg_624" n="HIAT:w" s="T2054">dĭn</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2056" id="Seg_627" n="HIAT:w" s="T2055">külaːmbi</ts>
                  <nts id="Seg_628" n="HIAT:ip">.</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2060" id="Seg_631" n="HIAT:u" s="T2056">
                  <ts e="T2057" id="Seg_633" n="HIAT:w" s="T2056">Dĭgəttə</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2058" id="Seg_636" n="HIAT:w" s="T2057">il</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2059" id="Seg_639" n="HIAT:w" s="T2058">bar</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2060" id="Seg_642" n="HIAT:w" s="T2059">tajirluʔpi</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2064" id="Seg_646" n="HIAT:u" s="T2060">
                  <ts e="T2061" id="Seg_648" n="HIAT:w" s="T2060">A</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2062" id="Seg_651" n="HIAT:w" s="T2061">dĭ</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2063" id="Seg_654" n="HIAT:w" s="T2062">dʼü</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2064" id="Seg_657" n="HIAT:w" s="T2063">ugaːndə</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2065" id="Seg_661" n="HIAT:u" s="T2064">
                  <ts e="T2065" id="Seg_663" n="HIAT:w" s="T2064">Kabarləj</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2065" id="Seg_666" n="sc" s="T1898">
               <ts e="T1899" id="Seg_668" n="e" s="T1898">Amnobi </ts>
               <ts e="T1900" id="Seg_670" n="e" s="T1899">pĭnzə. </ts>
               <ts e="T1901" id="Seg_672" n="e" s="T1900">Ugaːndə </ts>
               <ts e="T1902" id="Seg_674" n="e" s="T1901">esseŋ </ts>
               <ts e="T1903" id="Seg_676" n="e" s="T1902">iʔgö </ts>
               <ts e="T1904" id="Seg_678" n="e" s="T1903">ambi. </ts>
               <ts e="T1905" id="Seg_680" n="e" s="T1904">Dĭgəttə </ts>
               <ts e="T1906" id="Seg_682" n="e" s="T1905">koŋgən </ts>
               <ts e="T1907" id="Seg_684" n="e" s="T1906">koʔbdot </ts>
               <ts e="T1908" id="Seg_686" n="e" s="T1907">kunnaːmbi. </ts>
               <ts e="T1909" id="Seg_688" n="e" s="T1908">Dĭm </ts>
               <ts e="T1910" id="Seg_690" n="e" s="T1909">ej </ts>
               <ts e="T1911" id="Seg_692" n="e" s="T1910">ambi. </ts>
               <ts e="T1912" id="Seg_694" n="e" s="T1911">Dĭn </ts>
               <ts e="T1913" id="Seg_696" n="e" s="T1912">men </ts>
               <ts e="T1914" id="Seg_698" n="e" s="T1913">ibi. </ts>
               <ts e="T1915" id="Seg_700" n="e" s="T1914">Dĭgəttə </ts>
               <ts e="T1916" id="Seg_702" n="e" s="T1915">dĭ </ts>
               <ts e="T1917" id="Seg_704" n="e" s="T1916">pʼaŋdələj </ts>
               <ts e="T1918" id="Seg_706" n="e" s="T1917">sazəndə, </ts>
               <ts e="T1919" id="Seg_708" n="e" s="T1918">abandə </ts>
               <ts e="T1920" id="Seg_710" n="e" s="T1919">öʔləj. </ts>
               <ts e="T1921" id="Seg_712" n="e" s="T1920">Men </ts>
               <ts e="T1922" id="Seg_714" n="e" s="T1921">(mene- </ts>
               <ts e="T1923" id="Seg_716" n="e" s="T1922">di-) </ts>
               <ts e="T1924" id="Seg_718" n="e" s="T1923">dĭzeŋgəʔ </ts>
               <ts e="T1925" id="Seg_720" n="e" s="T1924">detləj </ts>
               <ts e="T1926" id="Seg_722" n="e" s="T1925">sazən </ts>
               <ts e="T1927" id="Seg_724" n="e" s="T1926">dĭʔnə. </ts>
               <ts e="T1928" id="Seg_726" n="e" s="T1927">Dĭgəttə </ts>
               <ts e="T1929" id="Seg_728" n="e" s="T1928">ijat, </ts>
               <ts e="T1930" id="Seg_730" n="e" s="T1929">abat </ts>
               <ts e="T1931" id="Seg_732" n="e" s="T1930">măndə:" </ts>
               <ts e="T1932" id="Seg_734" n="e" s="T1931">Suraraʔ, </ts>
               <ts e="T1933" id="Seg_736" n="e" s="T1932">šində </ts>
               <ts e="T1934" id="Seg_738" n="e" s="T1933">kuštü </ts>
               <ts e="T1935" id="Seg_740" n="e" s="T1934">dĭʔə". </ts>
               <ts e="T1936" id="Seg_742" n="e" s="T1935">Dĭ </ts>
               <ts e="T1937" id="Seg_744" n="e" s="T1936">davaj </ts>
               <ts e="T1938" id="Seg_746" n="e" s="T1937">surarzittə, </ts>
               <ts e="T1939" id="Seg_748" n="e" s="T1938">dĭ </ts>
               <ts e="T1940" id="Seg_750" n="e" s="T1939">bar </ts>
               <ts e="T1941" id="Seg_752" n="e" s="T1940">nörbəbi: </ts>
               <ts e="T1942" id="Seg_754" n="e" s="T1941">"Nikita </ts>
               <ts e="T1943" id="Seg_756" n="e" s="T1942">Kožemʼaka </ts>
               <ts e="T1944" id="Seg_758" n="e" s="T1943">kuštü </ts>
               <ts e="T1945" id="Seg_760" n="e" s="T1944">măna". </ts>
               <ts e="T1946" id="Seg_762" n="e" s="T1945">Dĭgəttə </ts>
               <ts e="T1947" id="Seg_764" n="e" s="T1946">dĭ </ts>
               <ts e="T1948" id="Seg_766" n="e" s="T1947">öʔlüʔbi. </ts>
               <ts e="T1949" id="Seg_768" n="e" s="T1948">Dĭzeŋ </ts>
               <ts e="T1950" id="Seg_770" n="e" s="T1949">kambiʔi, </ts>
               <ts e="T1951" id="Seg_772" n="e" s="T1950">šidegöʔ. </ts>
               <ts e="T1952" id="Seg_774" n="e" s="T1951">Dĭ </ts>
               <ts e="T1953" id="Seg_776" n="e" s="T1952">kubaʔi </ts>
               <ts e="T1954" id="Seg_778" n="e" s="T1953">abiʔi. </ts>
               <ts e="T1955" id="Seg_780" n="e" s="T1954">(Ab-) </ts>
               <ts e="T1956" id="Seg_782" n="e" s="T1955">Alaʔbi </ts>
               <ts e="T1957" id="Seg_784" n="e" s="T1956">iʔgö. </ts>
               <ts e="T1958" id="Seg_786" n="e" s="T1957">Neröluʔpi </ts>
               <ts e="T1959" id="Seg_788" n="e" s="T1958">što </ts>
               <ts e="T1960" id="Seg_790" n="e" s="T1959">koŋ </ts>
               <ts e="T1961" id="Seg_792" n="e" s="T1960">bostə </ts>
               <ts e="T1962" id="Seg_794" n="e" s="T1961">netsiʔ </ts>
               <ts e="T1963" id="Seg_796" n="e" s="T1962">šonəga. </ts>
               <ts e="T1964" id="Seg_798" n="e" s="T1963">I </ts>
               <ts e="T1965" id="Seg_800" n="e" s="T1964">kubaʔi </ts>
               <ts e="T1966" id="Seg_802" n="e" s="T1965">(ša-) </ts>
               <ts e="T1967" id="Seg_804" n="e" s="T1966">sajnʼeʔluʔpi. </ts>
               <ts e="T1968" id="Seg_806" n="e" s="T1967">Dĭgəttə </ts>
               <ts e="T1969" id="Seg_808" n="e" s="T1968">ej </ts>
               <ts e="T1970" id="Seg_810" n="e" s="T1969">kambi </ts>
               <ts e="T1971" id="Seg_812" n="e" s="T1970">dĭzeŋ. </ts>
               <ts e="T1972" id="Seg_814" n="e" s="T1971">A </ts>
               <ts e="T1973" id="Seg_816" n="e" s="T1972">(dĭgət-) </ts>
               <ts e="T1974" id="Seg_818" n="e" s="T1973">dĭgəttə </ts>
               <ts e="T1975" id="Seg_820" n="e" s="T1974">dĭ </ts>
               <ts e="T1976" id="Seg_822" n="e" s="T1975">iʔgö </ts>
               <ts e="T1977" id="Seg_824" n="e" s="T1976">esseŋ </ts>
               <ts e="T1978" id="Seg_826" n="e" s="T1977">oʔbdobi. </ts>
               <ts e="T1979" id="Seg_828" n="e" s="T1978">Esseŋ </ts>
               <ts e="T1980" id="Seg_830" n="e" s="T1979">(kabi-) </ts>
               <ts e="T1981" id="Seg_832" n="e" s="T1980">kambiʔi </ts>
               <ts e="T1982" id="Seg_834" n="e" s="T1981">bar, </ts>
               <ts e="T1983" id="Seg_836" n="e" s="T1982">dʼorbiʔi. </ts>
               <ts e="T1984" id="Seg_838" n="e" s="T1983">Dĭ </ts>
               <ts e="T1985" id="Seg_840" n="e" s="T1984">dĭgəttə </ts>
               <ts e="T1986" id="Seg_842" n="e" s="T1985">kambi. </ts>
               <ts e="T1987" id="Seg_844" n="e" s="T1986">Iʔgö </ts>
               <ts e="T1988" id="Seg_846" n="e" s="T1987">kudʼelʼə </ts>
               <ts e="T1989" id="Seg_848" n="e" s="T1988">ibi. </ts>
               <ts e="T1990" id="Seg_850" n="e" s="T1989">(Dʼögatʼsiʔ) </ts>
               <ts e="T1991" id="Seg_852" n="e" s="T1990">tʼuʔpi. </ts>
               <ts e="T1992" id="Seg_854" n="e" s="T1991">Kürleʔpi. </ts>
               <ts e="T1993" id="Seg_856" n="e" s="T1992">Dĭgəttə </ts>
               <ts e="T1994" id="Seg_858" n="e" s="T1993">šobi. </ts>
               <ts e="T1995" id="Seg_860" n="e" s="T1994">Pĭnzət </ts>
               <ts e="T1996" id="Seg_862" n="e" s="T1995">berlogəttə. </ts>
               <ts e="T1997" id="Seg_864" n="e" s="T1996">"Supsoʔ, </ts>
               <ts e="T1998" id="Seg_866" n="e" s="T1997">a_to </ts>
               <ts e="T1999" id="Seg_868" n="e" s="T1998">vsʼo_ravno </ts>
               <ts e="T2000" id="Seg_870" n="e" s="T1999">tănan </ts>
               <ts e="T2001" id="Seg_872" n="e" s="T2000">kutlam!" </ts>
               <ts e="T2002" id="Seg_874" n="e" s="T2001">Dĭgəttə </ts>
               <ts e="T2003" id="Seg_876" n="e" s="T2002">paʔi </ts>
               <ts e="T2004" id="Seg_878" n="e" s="T2003">bar </ts>
               <ts e="T2005" id="Seg_880" n="e" s="T2004">ileʔbə. </ts>
               <ts e="T2006" id="Seg_882" n="e" s="T2005">Dĭgəttə </ts>
               <ts e="T2007" id="Seg_884" n="e" s="T2006">dĭ </ts>
               <ts e="T2008" id="Seg_886" n="e" s="T2007">supsobi. </ts>
               <ts e="T2009" id="Seg_888" n="e" s="T2008">Davaj </ts>
               <ts e="T2010" id="Seg_890" n="e" s="T2009">(dʼabərolbi). </ts>
               <ts e="T2011" id="Seg_892" n="e" s="T2010">Dʼabərobiʔi, </ts>
               <ts e="T2012" id="Seg_894" n="e" s="T2011">dʼabərobiʔi. </ts>
               <ts e="T2013" id="Seg_896" n="e" s="T2012">Dĭgəttə </ts>
               <ts e="T2014" id="Seg_898" n="e" s="T2013">pĭnzə </ts>
               <ts e="T2015" id="Seg_900" n="e" s="T2014">măndə: </ts>
               <ts e="T2016" id="Seg_902" n="e" s="T2015">"Ĭmbi </ts>
               <ts e="T2017" id="Seg_904" n="e" s="T2016">(n-) </ts>
               <ts e="T2018" id="Seg_906" n="e" s="T2017">miʔnʼibeʔ </ts>
               <ts e="T2019" id="Seg_908" n="e" s="T2018">dʼabərozittə? </ts>
               <ts e="T2020" id="Seg_910" n="e" s="T2019">Tăn </ts>
               <ts e="T2021" id="Seg_912" n="e" s="T2020">pʼeldə </ts>
               <ts e="T2022" id="Seg_914" n="e" s="T2021">(budi-) </ts>
               <ts e="T2023" id="Seg_916" n="e" s="T2022">budʼet, </ts>
               <ts e="T2024" id="Seg_918" n="e" s="T2023">măn </ts>
               <ts e="T2025" id="Seg_920" n="e" s="T2024">pʼeldə". </ts>
               <ts e="T2026" id="Seg_922" n="e" s="T2025">Dĭgəttə </ts>
               <ts e="T2027" id="Seg_924" n="e" s="T2026">dĭ </ts>
               <ts e="T2028" id="Seg_926" n="e" s="T2027">(mănə-) </ts>
               <ts e="T2029" id="Seg_928" n="e" s="T2028">măndə: </ts>
               <ts e="T2030" id="Seg_930" n="e" s="T2029">"Nada </ts>
               <ts e="T2031" id="Seg_932" n="e" s="T2030">granʼ </ts>
               <ts e="T2032" id="Seg_934" n="e" s="T2031">enzittə". </ts>
               <ts e="T2033" id="Seg_936" n="e" s="T2032">Dĭgəttə </ts>
               <ts e="T2034" id="Seg_938" n="e" s="T2033">dĭm </ts>
               <ts e="T2035" id="Seg_940" n="e" s="T2034">körerbi. </ts>
               <ts e="T2036" id="Seg_942" n="e" s="T2035">Dĭ </ts>
               <ts e="T2037" id="Seg_944" n="e" s="T2036">bar </ts>
               <ts e="T2038" id="Seg_946" n="e" s="T2037">dʼübə </ts>
               <ts e="T2039" id="Seg_948" n="e" s="T2038">ugaːndə </ts>
               <ts e="T2040" id="Seg_950" n="e" s="T2039">iʔgö </ts>
               <ts e="T2041" id="Seg_952" n="e" s="T2040">tĭlbi. </ts>
               <ts e="T2042" id="Seg_954" n="e" s="T2041">Dĭgəttə </ts>
               <ts e="T2043" id="Seg_956" n="e" s="T2042">măndə:" </ts>
               <ts e="T2044" id="Seg_958" n="e" s="T2043">Nada </ts>
               <ts e="T2045" id="Seg_960" n="e" s="T2044">bü </ts>
               <ts e="T2046" id="Seg_962" n="e" s="T2045">tože </ts>
               <ts e="T2047" id="Seg_964" n="e" s="T2046">šide </ts>
               <ts e="T2048" id="Seg_966" n="e" s="T2047">pʼeldə </ts>
               <ts e="T2049" id="Seg_968" n="e" s="T2048">azittə". </ts>
               <ts e="T2050" id="Seg_970" n="e" s="T2049">Dĭ </ts>
               <ts e="T2051" id="Seg_972" n="e" s="T2050">bünə </ts>
               <ts e="T2052" id="Seg_974" n="e" s="T2051">kambi </ts>
               <ts e="T2053" id="Seg_976" n="e" s="T2052">i </ts>
               <ts e="T2054" id="Seg_978" n="e" s="T2053">dĭ </ts>
               <ts e="T2055" id="Seg_980" n="e" s="T2054">dĭn </ts>
               <ts e="T2056" id="Seg_982" n="e" s="T2055">külaːmbi. </ts>
               <ts e="T2057" id="Seg_984" n="e" s="T2056">Dĭgəttə </ts>
               <ts e="T2058" id="Seg_986" n="e" s="T2057">il </ts>
               <ts e="T2059" id="Seg_988" n="e" s="T2058">bar </ts>
               <ts e="T2060" id="Seg_990" n="e" s="T2059">tajirluʔpi. </ts>
               <ts e="T2061" id="Seg_992" n="e" s="T2060">A </ts>
               <ts e="T2062" id="Seg_994" n="e" s="T2061">dĭ </ts>
               <ts e="T2063" id="Seg_996" n="e" s="T2062">dʼü </ts>
               <ts e="T2064" id="Seg_998" n="e" s="T2063">ugaːndə. </ts>
               <ts e="T2065" id="Seg_1000" n="e" s="T2064">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1900" id="Seg_1001" s="T1898">PKZ_196X_NikitaKozhemyaka_flk.001 (001)</ta>
            <ta e="T1904" id="Seg_1002" s="T1900">PKZ_196X_NikitaKozhemyaka_flk.002 (002)</ta>
            <ta e="T1908" id="Seg_1003" s="T1904">PKZ_196X_NikitaKozhemyaka_flk.003 (003)</ta>
            <ta e="T1911" id="Seg_1004" s="T1908">PKZ_196X_NikitaKozhemyaka_flk.004 (004)</ta>
            <ta e="T1914" id="Seg_1005" s="T1911">PKZ_196X_NikitaKozhemyaka_flk.005 (005)</ta>
            <ta e="T1920" id="Seg_1006" s="T1914">PKZ_196X_NikitaKozhemyaka_flk.006 (006)</ta>
            <ta e="T1927" id="Seg_1007" s="T1920">PKZ_196X_NikitaKozhemyaka_flk.007 (007)</ta>
            <ta e="T1935" id="Seg_1008" s="T1927">PKZ_196X_NikitaKozhemyaka_flk.008 (008)</ta>
            <ta e="T1945" id="Seg_1009" s="T1935">PKZ_196X_NikitaKozhemyaka_flk.009 (009) </ta>
            <ta e="T1948" id="Seg_1010" s="T1945">PKZ_196X_NikitaKozhemyaka_flk.010 (011)</ta>
            <ta e="T1951" id="Seg_1011" s="T1948">PKZ_196X_NikitaKozhemyaka_flk.011 (012)</ta>
            <ta e="T1954" id="Seg_1012" s="T1951">PKZ_196X_NikitaKozhemyaka_flk.012 (013)</ta>
            <ta e="T1957" id="Seg_1013" s="T1954">PKZ_196X_NikitaKozhemyaka_flk.013 (014)</ta>
            <ta e="T1963" id="Seg_1014" s="T1957">PKZ_196X_NikitaKozhemyaka_flk.014 (015)</ta>
            <ta e="T1967" id="Seg_1015" s="T1963">PKZ_196X_NikitaKozhemyaka_flk.015 (016)</ta>
            <ta e="T1971" id="Seg_1016" s="T1967">PKZ_196X_NikitaKozhemyaka_flk.016 (017)</ta>
            <ta e="T1978" id="Seg_1017" s="T1971">PKZ_196X_NikitaKozhemyaka_flk.017 (018)</ta>
            <ta e="T1983" id="Seg_1018" s="T1978">PKZ_196X_NikitaKozhemyaka_flk.018 (019)</ta>
            <ta e="T1986" id="Seg_1019" s="T1983">PKZ_196X_NikitaKozhemyaka_flk.019 (020)</ta>
            <ta e="T1989" id="Seg_1020" s="T1986">PKZ_196X_NikitaKozhemyaka_flk.020 (021)</ta>
            <ta e="T1991" id="Seg_1021" s="T1989">PKZ_196X_NikitaKozhemyaka_flk.021 (022)</ta>
            <ta e="T1992" id="Seg_1022" s="T1991">PKZ_196X_NikitaKozhemyaka_flk.022 (023)</ta>
            <ta e="T1994" id="Seg_1023" s="T1992">PKZ_196X_NikitaKozhemyaka_flk.023 (024)</ta>
            <ta e="T1996" id="Seg_1024" s="T1994">PKZ_196X_NikitaKozhemyaka_flk.024 (025)</ta>
            <ta e="T2001" id="Seg_1025" s="T1996">PKZ_196X_NikitaKozhemyaka_flk.025 (026)</ta>
            <ta e="T2005" id="Seg_1026" s="T2001">PKZ_196X_NikitaKozhemyaka_flk.026 (027)</ta>
            <ta e="T2008" id="Seg_1027" s="T2005">PKZ_196X_NikitaKozhemyaka_flk.027 (028)</ta>
            <ta e="T2010" id="Seg_1028" s="T2008">PKZ_196X_NikitaKozhemyaka_flk.028 (029)</ta>
            <ta e="T2012" id="Seg_1029" s="T2010">PKZ_196X_NikitaKozhemyaka_flk.029 (030)</ta>
            <ta e="T2019" id="Seg_1030" s="T2012">PKZ_196X_NikitaKozhemyaka_flk.030 (031) </ta>
            <ta e="T2025" id="Seg_1031" s="T2019">PKZ_196X_NikitaKozhemyaka_flk.031 (033)</ta>
            <ta e="T2032" id="Seg_1032" s="T2025">PKZ_196X_NikitaKozhemyaka_flk.032 (034) </ta>
            <ta e="T2035" id="Seg_1033" s="T2032">PKZ_196X_NikitaKozhemyaka_flk.033 (036)</ta>
            <ta e="T2041" id="Seg_1034" s="T2035">PKZ_196X_NikitaKozhemyaka_flk.034 (037)</ta>
            <ta e="T2049" id="Seg_1035" s="T2041">PKZ_196X_NikitaKozhemyaka_flk.035 (038)</ta>
            <ta e="T2056" id="Seg_1036" s="T2049">PKZ_196X_NikitaKozhemyaka_flk.036 (039)</ta>
            <ta e="T2060" id="Seg_1037" s="T2056">PKZ_196X_NikitaKozhemyaka_flk.037 (040)</ta>
            <ta e="T2064" id="Seg_1038" s="T2060">PKZ_196X_NikitaKozhemyaka_flk.038 (041)</ta>
            <ta e="T2065" id="Seg_1039" s="T2064">PKZ_196X_NikitaKozhemyaka_flk.039 (042)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1900" id="Seg_1040" s="T1898">Amnobi pĭnzə. </ta>
            <ta e="T1904" id="Seg_1041" s="T1900">Ugaːndə esseŋ iʔgö ambi. </ta>
            <ta e="T1908" id="Seg_1042" s="T1904">Dĭgəttə koŋgən koʔbdot kunnaːmbi. </ta>
            <ta e="T1911" id="Seg_1043" s="T1908">Dĭm ej ambi. </ta>
            <ta e="T1914" id="Seg_1044" s="T1911">Dĭn men ibi. </ta>
            <ta e="T1920" id="Seg_1045" s="T1914">Dĭgəttə dĭ pʼaŋdələj sazəndə, abandə öʔləj. </ta>
            <ta e="T1927" id="Seg_1046" s="T1920">Men (mene- di-) dĭzeŋgəʔ detləj sazən dĭʔnə. </ta>
            <ta e="T1935" id="Seg_1047" s="T1927">Dĭgəttə ijat, abat măndə:" Suraraʔ, šində kuštü dĭʔə". </ta>
            <ta e="T1945" id="Seg_1048" s="T1935">Dĭ davaj surarzittə, dĭ bar nörbəbi: "Nikita Kožemʼaka kuštü măna". </ta>
            <ta e="T1948" id="Seg_1049" s="T1945">Dĭgəttə dĭ öʔlüʔbi. </ta>
            <ta e="T1951" id="Seg_1050" s="T1948">Dĭzeŋ kambiʔi, šidegöʔ. </ta>
            <ta e="T1954" id="Seg_1051" s="T1951">Dĭ kubaʔi abiʔi. </ta>
            <ta e="T1957" id="Seg_1052" s="T1954">(Ab-) Alaʔbi iʔgö. </ta>
            <ta e="T1963" id="Seg_1053" s="T1957">Neröluʔpi što koŋ bostə netsiʔ šonəga. </ta>
            <ta e="T1967" id="Seg_1054" s="T1963">I kubaʔi (ša-) sajnʼeʔluʔpi. </ta>
            <ta e="T1971" id="Seg_1055" s="T1967">Dĭgəttə ej kambi dĭzeŋ. </ta>
            <ta e="T1978" id="Seg_1056" s="T1971">A (dĭgət-) dĭgəttə dĭ iʔgö esseŋ oʔbdobi. </ta>
            <ta e="T1983" id="Seg_1057" s="T1978">Esseŋ (kabi-) kambiʔi bar, dʼorbiʔi. </ta>
            <ta e="T1986" id="Seg_1058" s="T1983">Dĭ dĭgəttə kambi. </ta>
            <ta e="T1989" id="Seg_1059" s="T1986">Iʔgö kudʼelʼə ibi. </ta>
            <ta e="T1991" id="Seg_1060" s="T1989">(Dʼögatʼsiʔ) tʼuʔpi. </ta>
            <ta e="T1992" id="Seg_1061" s="T1991">Kürleʔpi. </ta>
            <ta e="T1994" id="Seg_1062" s="T1992">Dĭgəttə šobi. </ta>
            <ta e="T1996" id="Seg_1063" s="T1994">Pĭnzət berlogəttə. </ta>
            <ta e="T2001" id="Seg_1064" s="T1996">"Supsoʔ, a to vsʼo ravno tănan kutlam!" </ta>
            <ta e="T2005" id="Seg_1065" s="T2001">Dĭgəttə paʔi bar ileʔbə. </ta>
            <ta e="T2008" id="Seg_1066" s="T2005">Dĭgəttə dĭ supsobi. </ta>
            <ta e="T2010" id="Seg_1067" s="T2008">Davaj (dʼabərolbi). </ta>
            <ta e="T2012" id="Seg_1068" s="T2010">Dʼabərobiʔi, dʼabərobiʔi. </ta>
            <ta e="T2019" id="Seg_1069" s="T2012">Dĭgəttə pĭnzə măndə: "Ĭmbi (n-) miʔnʼibeʔ dʼabərozittə? </ta>
            <ta e="T2025" id="Seg_1070" s="T2019">Tăn pʼeldə (budi-) budʼet, măn pʼeldə". </ta>
            <ta e="T2032" id="Seg_1071" s="T2025">Dĭgəttə dĭ (mănə-) măndə: "Nada granʼ enzittə". </ta>
            <ta e="T2035" id="Seg_1072" s="T2032">Dĭgəttə dĭm körerbi. </ta>
            <ta e="T2041" id="Seg_1073" s="T2035">Dĭ bar dʼübə ugaːndə iʔgö tĭlbi. </ta>
            <ta e="T2049" id="Seg_1074" s="T2041">Dĭgəttə măndə:" Nada bü tože šide pʼeldə azittə". </ta>
            <ta e="T2056" id="Seg_1075" s="T2049">Dĭ bünə kambi i dĭ dĭn külaːmbi. </ta>
            <ta e="T2060" id="Seg_1076" s="T2056">Dĭgəttə il bar tajirluʔpi. </ta>
            <ta e="T2064" id="Seg_1077" s="T2060">A dĭ dʼü ugaːndə. </ta>
            <ta e="T2065" id="Seg_1078" s="T2064">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1899" id="Seg_1079" s="T1898">amno-bi</ta>
            <ta e="T1900" id="Seg_1080" s="T1899">pĭnzə</ta>
            <ta e="T1901" id="Seg_1081" s="T1900">ugaːndə</ta>
            <ta e="T1902" id="Seg_1082" s="T1901">es-seŋ</ta>
            <ta e="T1903" id="Seg_1083" s="T1902">iʔgö</ta>
            <ta e="T1904" id="Seg_1084" s="T1903">am-bi</ta>
            <ta e="T1905" id="Seg_1085" s="T1904">dĭgəttə</ta>
            <ta e="T1906" id="Seg_1086" s="T1905">koŋ-gən</ta>
            <ta e="T1907" id="Seg_1087" s="T1906">koʔbdo-t</ta>
            <ta e="T1908" id="Seg_1088" s="T1907">kun-naːm-bi</ta>
            <ta e="T1909" id="Seg_1089" s="T1908">dĭ-m</ta>
            <ta e="T1910" id="Seg_1090" s="T1909">ej</ta>
            <ta e="T1911" id="Seg_1091" s="T1910">am-bi</ta>
            <ta e="T1912" id="Seg_1092" s="T1911">dĭn</ta>
            <ta e="T1913" id="Seg_1093" s="T1912">men</ta>
            <ta e="T1914" id="Seg_1094" s="T1913">i-bi</ta>
            <ta e="T1915" id="Seg_1095" s="T1914">dĭgəttə</ta>
            <ta e="T1916" id="Seg_1096" s="T1915">dĭ</ta>
            <ta e="T1917" id="Seg_1097" s="T1916">pʼaŋdə-lə-j</ta>
            <ta e="T1918" id="Seg_1098" s="T1917">sazən-də</ta>
            <ta e="T1919" id="Seg_1099" s="T1918">aba-ndə</ta>
            <ta e="T1920" id="Seg_1100" s="T1919">öʔ-lə-j</ta>
            <ta e="T1921" id="Seg_1101" s="T1920">men</ta>
            <ta e="T1924" id="Seg_1102" s="T1923">dĭ-zeŋ-gəʔ</ta>
            <ta e="T1925" id="Seg_1103" s="T1924">det-lə-j</ta>
            <ta e="T1926" id="Seg_1104" s="T1925">sazən</ta>
            <ta e="T1927" id="Seg_1105" s="T1926">dĭʔ-nə</ta>
            <ta e="T1928" id="Seg_1106" s="T1927">dĭgəttə</ta>
            <ta e="T1929" id="Seg_1107" s="T1928">ija-t</ta>
            <ta e="T1930" id="Seg_1108" s="T1929">aba-t</ta>
            <ta e="T1931" id="Seg_1109" s="T1930">măn-də</ta>
            <ta e="T1932" id="Seg_1110" s="T1931">surar-ʔ</ta>
            <ta e="T1933" id="Seg_1111" s="T1932">šində</ta>
            <ta e="T1934" id="Seg_1112" s="T1933">kuštü</ta>
            <ta e="T1935" id="Seg_1113" s="T1934">dĭʔə</ta>
            <ta e="T1936" id="Seg_1114" s="T1935">dĭ</ta>
            <ta e="T1937" id="Seg_1115" s="T1936">davaj</ta>
            <ta e="T1938" id="Seg_1116" s="T1937">surar-zittə</ta>
            <ta e="T1939" id="Seg_1117" s="T1938">dĭ</ta>
            <ta e="T1940" id="Seg_1118" s="T1939">bar</ta>
            <ta e="T1941" id="Seg_1119" s="T1940">nörbə-bi</ta>
            <ta e="T1942" id="Seg_1120" s="T1941">Nikita</ta>
            <ta e="T1943" id="Seg_1121" s="T1942">Kožemʼaka</ta>
            <ta e="T1944" id="Seg_1122" s="T1943">kuštü</ta>
            <ta e="T1945" id="Seg_1123" s="T1944">măna</ta>
            <ta e="T1946" id="Seg_1124" s="T1945">dĭgəttə</ta>
            <ta e="T1947" id="Seg_1125" s="T1946">dĭ</ta>
            <ta e="T1948" id="Seg_1126" s="T1947">öʔ-lüʔ-bi</ta>
            <ta e="T1949" id="Seg_1127" s="T1948">dĭ-zeŋ</ta>
            <ta e="T1950" id="Seg_1128" s="T1949">kam-bi-ʔi</ta>
            <ta e="T1951" id="Seg_1129" s="T1950">šide-göʔ</ta>
            <ta e="T1952" id="Seg_1130" s="T1951">dĭ</ta>
            <ta e="T1953" id="Seg_1131" s="T1952">kuba-ʔi</ta>
            <ta e="T1954" id="Seg_1132" s="T1953">a-bi-ʔi</ta>
            <ta e="T1956" id="Seg_1133" s="T1955">a-laʔ-bi</ta>
            <ta e="T1957" id="Seg_1134" s="T1956">iʔgö</ta>
            <ta e="T1958" id="Seg_1135" s="T1957">nerö-luʔ-pi</ta>
            <ta e="T1959" id="Seg_1136" s="T1958">što</ta>
            <ta e="T1960" id="Seg_1137" s="T1959">koŋ</ta>
            <ta e="T1961" id="Seg_1138" s="T1960">bos-tə</ta>
            <ta e="T1962" id="Seg_1139" s="T1961">ne-t-siʔ</ta>
            <ta e="T1963" id="Seg_1140" s="T1962">šonə-ga</ta>
            <ta e="T1964" id="Seg_1141" s="T1963">i</ta>
            <ta e="T1965" id="Seg_1142" s="T1964">kuba-ʔi</ta>
            <ta e="T1967" id="Seg_1143" s="T1966">saj-nʼeʔ-luʔ-pi</ta>
            <ta e="T1968" id="Seg_1144" s="T1967">dĭgəttə</ta>
            <ta e="T1969" id="Seg_1145" s="T1968">ej</ta>
            <ta e="T1970" id="Seg_1146" s="T1969">kam-bi</ta>
            <ta e="T1971" id="Seg_1147" s="T1970">dĭ-zeŋ</ta>
            <ta e="T1972" id="Seg_1148" s="T1971">a</ta>
            <ta e="T1974" id="Seg_1149" s="T1973">dĭgəttə</ta>
            <ta e="T1975" id="Seg_1150" s="T1974">dĭ</ta>
            <ta e="T1976" id="Seg_1151" s="T1975">iʔgö</ta>
            <ta e="T1977" id="Seg_1152" s="T1976">es-seŋ</ta>
            <ta e="T1978" id="Seg_1153" s="T1977">oʔbdo-bi</ta>
            <ta e="T1979" id="Seg_1154" s="T1978">es-seŋ</ta>
            <ta e="T1981" id="Seg_1155" s="T1980">kam-bi-ʔi</ta>
            <ta e="T1982" id="Seg_1156" s="T1981">bar</ta>
            <ta e="T1983" id="Seg_1157" s="T1982">dʼor-bi-ʔi</ta>
            <ta e="T1984" id="Seg_1158" s="T1983">dĭ</ta>
            <ta e="T1985" id="Seg_1159" s="T1984">dĭgəttə</ta>
            <ta e="T1986" id="Seg_1160" s="T1985">kam-bi</ta>
            <ta e="T1987" id="Seg_1161" s="T1986">iʔgö</ta>
            <ta e="T1988" id="Seg_1162" s="T1987">kudʼelʼə</ta>
            <ta e="T1989" id="Seg_1163" s="T1988">i-bi</ta>
            <ta e="T1990" id="Seg_1164" s="T1989">dʼögatʼ-siʔ</ta>
            <ta e="T1991" id="Seg_1165" s="T1990">tʼuʔ-pi</ta>
            <ta e="T1992" id="Seg_1166" s="T1991">kür-leʔ-pi</ta>
            <ta e="T1993" id="Seg_1167" s="T1992">dĭgəttə</ta>
            <ta e="T1994" id="Seg_1168" s="T1993">šo-bi</ta>
            <ta e="T1995" id="Seg_1169" s="T1994">pĭnzə-t</ta>
            <ta e="T1996" id="Seg_1170" s="T1995">berlogə-ttə</ta>
            <ta e="T1997" id="Seg_1171" s="T1996">supso-ʔ</ta>
            <ta e="T1998" id="Seg_1172" s="T1997">ato</ta>
            <ta e="T1999" id="Seg_1173" s="T1998">vsʼoravno</ta>
            <ta e="T2000" id="Seg_1174" s="T1999">tănan</ta>
            <ta e="T2001" id="Seg_1175" s="T2000">kut-la-m</ta>
            <ta e="T2002" id="Seg_1176" s="T2001">dĭgəttə</ta>
            <ta e="T2003" id="Seg_1177" s="T2002">pa-ʔi</ta>
            <ta e="T2004" id="Seg_1178" s="T2003">bar</ta>
            <ta e="T2005" id="Seg_1179" s="T2004">i-leʔbə</ta>
            <ta e="T2006" id="Seg_1180" s="T2005">dĭgəttə</ta>
            <ta e="T2007" id="Seg_1181" s="T2006">dĭ</ta>
            <ta e="T2008" id="Seg_1182" s="T2007">supso-bi</ta>
            <ta e="T2009" id="Seg_1183" s="T2008">davaj</ta>
            <ta e="T2010" id="Seg_1184" s="T2009">dʼabəro-l-bi</ta>
            <ta e="T2011" id="Seg_1185" s="T2010">dʼabəro-bi-ʔi</ta>
            <ta e="T2012" id="Seg_1186" s="T2011">dʼabəro-bi-ʔi</ta>
            <ta e="T2013" id="Seg_1187" s="T2012">dĭgəttə</ta>
            <ta e="T2014" id="Seg_1188" s="T2013">pĭnzə</ta>
            <ta e="T2015" id="Seg_1189" s="T2014">măn-də</ta>
            <ta e="T2016" id="Seg_1190" s="T2015">ĭmbi</ta>
            <ta e="T2018" id="Seg_1191" s="T2017">miʔnʼibeʔ</ta>
            <ta e="T2019" id="Seg_1192" s="T2018">dʼabəro-zittə</ta>
            <ta e="T2020" id="Seg_1193" s="T2019">tăn</ta>
            <ta e="T2021" id="Seg_1194" s="T2020">pʼel-də</ta>
            <ta e="T2024" id="Seg_1195" s="T2023">măn</ta>
            <ta e="T2025" id="Seg_1196" s="T2024">pʼel-də</ta>
            <ta e="T2026" id="Seg_1197" s="T2025">dĭgəttə</ta>
            <ta e="T2027" id="Seg_1198" s="T2026">dĭ</ta>
            <ta e="T2029" id="Seg_1199" s="T2028">măn-də</ta>
            <ta e="T2030" id="Seg_1200" s="T2029">nada</ta>
            <ta e="T2031" id="Seg_1201" s="T2030">granʼ</ta>
            <ta e="T2032" id="Seg_1202" s="T2031">en-zittə</ta>
            <ta e="T2033" id="Seg_1203" s="T2032">dĭgəttə</ta>
            <ta e="T2034" id="Seg_1204" s="T2033">dĭ-m</ta>
            <ta e="T2035" id="Seg_1205" s="T2034">körer-bi</ta>
            <ta e="T2036" id="Seg_1206" s="T2035">dĭ</ta>
            <ta e="T2037" id="Seg_1207" s="T2036">bar</ta>
            <ta e="T2038" id="Seg_1208" s="T2037">dʼü-bə</ta>
            <ta e="T2039" id="Seg_1209" s="T2038">ugaːndə</ta>
            <ta e="T2040" id="Seg_1210" s="T2039">iʔgö</ta>
            <ta e="T2041" id="Seg_1211" s="T2040">tĭl-bi</ta>
            <ta e="T2042" id="Seg_1212" s="T2041">dĭgəttə</ta>
            <ta e="T2043" id="Seg_1213" s="T2042">măn-də</ta>
            <ta e="T2044" id="Seg_1214" s="T2043">nada</ta>
            <ta e="T2045" id="Seg_1215" s="T2044">bü</ta>
            <ta e="T2046" id="Seg_1216" s="T2045">tože</ta>
            <ta e="T2047" id="Seg_1217" s="T2046">šide</ta>
            <ta e="T2048" id="Seg_1218" s="T2047">pʼel-də</ta>
            <ta e="T2049" id="Seg_1219" s="T2048">a-zittə</ta>
            <ta e="T2050" id="Seg_1220" s="T2049">dĭ</ta>
            <ta e="T2051" id="Seg_1221" s="T2050">bü-nə</ta>
            <ta e="T2052" id="Seg_1222" s="T2051">kam-bi</ta>
            <ta e="T2053" id="Seg_1223" s="T2052">i</ta>
            <ta e="T2054" id="Seg_1224" s="T2053">dĭ</ta>
            <ta e="T2055" id="Seg_1225" s="T2054">dĭn</ta>
            <ta e="T2056" id="Seg_1226" s="T2055">kü-laːm-bi</ta>
            <ta e="T2057" id="Seg_1227" s="T2056">dĭgəttə</ta>
            <ta e="T2058" id="Seg_1228" s="T2057">il</ta>
            <ta e="T2059" id="Seg_1229" s="T2058">bar</ta>
            <ta e="T2060" id="Seg_1230" s="T2059">tajir-luʔ-pi</ta>
            <ta e="T2061" id="Seg_1231" s="T2060">a</ta>
            <ta e="T2062" id="Seg_1232" s="T2061">dĭ</ta>
            <ta e="T2063" id="Seg_1233" s="T2062">dʼü</ta>
            <ta e="T2064" id="Seg_1234" s="T2063">ugaːndə</ta>
            <ta e="T2065" id="Seg_1235" s="T2064">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1899" id="Seg_1236" s="T1898">amno-bi</ta>
            <ta e="T1900" id="Seg_1237" s="T1899">pĭnzə</ta>
            <ta e="T1901" id="Seg_1238" s="T1900">ugaːndə</ta>
            <ta e="T1902" id="Seg_1239" s="T1901">ešši-zAŋ</ta>
            <ta e="T1903" id="Seg_1240" s="T1902">iʔgö</ta>
            <ta e="T1904" id="Seg_1241" s="T1903">am-bi</ta>
            <ta e="T1905" id="Seg_1242" s="T1904">dĭgəttə</ta>
            <ta e="T1906" id="Seg_1243" s="T1905">koŋ-Kən</ta>
            <ta e="T1907" id="Seg_1244" s="T1906">koʔbdo-t</ta>
            <ta e="T1908" id="Seg_1245" s="T1907">kun-laːm-bi</ta>
            <ta e="T1909" id="Seg_1246" s="T1908">dĭ-m</ta>
            <ta e="T1910" id="Seg_1247" s="T1909">ej</ta>
            <ta e="T1911" id="Seg_1248" s="T1910">am-bi</ta>
            <ta e="T1912" id="Seg_1249" s="T1911">dĭn</ta>
            <ta e="T1913" id="Seg_1250" s="T1912">men</ta>
            <ta e="T1914" id="Seg_1251" s="T1913">i-bi</ta>
            <ta e="T1915" id="Seg_1252" s="T1914">dĭgəttə</ta>
            <ta e="T1916" id="Seg_1253" s="T1915">dĭ</ta>
            <ta e="T1917" id="Seg_1254" s="T1916">pʼaŋdə-lV-j</ta>
            <ta e="T1918" id="Seg_1255" s="T1917">sazən-Tə</ta>
            <ta e="T1919" id="Seg_1256" s="T1918">aba-gəndə</ta>
            <ta e="T1920" id="Seg_1257" s="T1919">öʔ-lV-j</ta>
            <ta e="T1921" id="Seg_1258" s="T1920">men</ta>
            <ta e="T1924" id="Seg_1259" s="T1923">dĭ-zAŋ-gəʔ</ta>
            <ta e="T1925" id="Seg_1260" s="T1924">det-lV-j</ta>
            <ta e="T1926" id="Seg_1261" s="T1925">sazən</ta>
            <ta e="T1927" id="Seg_1262" s="T1926">dĭ-Tə</ta>
            <ta e="T1928" id="Seg_1263" s="T1927">dĭgəttə</ta>
            <ta e="T1929" id="Seg_1264" s="T1928">ija-t</ta>
            <ta e="T1930" id="Seg_1265" s="T1929">aba-t</ta>
            <ta e="T1931" id="Seg_1266" s="T1930">măn-ntə</ta>
            <ta e="T1932" id="Seg_1267" s="T1931">surar-ʔ</ta>
            <ta e="T1933" id="Seg_1268" s="T1932">šində</ta>
            <ta e="T1934" id="Seg_1269" s="T1933">kuštü</ta>
            <ta e="T1935" id="Seg_1270" s="T1934">dĭʔə</ta>
            <ta e="T1936" id="Seg_1271" s="T1935">dĭ</ta>
            <ta e="T1937" id="Seg_1272" s="T1936">davaj</ta>
            <ta e="T1938" id="Seg_1273" s="T1937">surar-zittə</ta>
            <ta e="T1939" id="Seg_1274" s="T1938">dĭ</ta>
            <ta e="T1940" id="Seg_1275" s="T1939">bar</ta>
            <ta e="T1941" id="Seg_1276" s="T1940">nörbə-bi</ta>
            <ta e="T1942" id="Seg_1277" s="T1941">Nikita</ta>
            <ta e="T1943" id="Seg_1278" s="T1942">Kožemʼaka</ta>
            <ta e="T1944" id="Seg_1279" s="T1943">kuštü</ta>
            <ta e="T1945" id="Seg_1280" s="T1944">măna</ta>
            <ta e="T1946" id="Seg_1281" s="T1945">dĭgəttə</ta>
            <ta e="T1947" id="Seg_1282" s="T1946">dĭ</ta>
            <ta e="T1948" id="Seg_1283" s="T1947">öʔ-luʔbdə-bi</ta>
            <ta e="T1949" id="Seg_1284" s="T1948">dĭ-zAŋ</ta>
            <ta e="T1950" id="Seg_1285" s="T1949">kan-bi-jəʔ</ta>
            <ta e="T1951" id="Seg_1286" s="T1950">šide-göʔ</ta>
            <ta e="T1952" id="Seg_1287" s="T1951">dĭ</ta>
            <ta e="T1953" id="Seg_1288" s="T1952">kuba-jəʔ</ta>
            <ta e="T1954" id="Seg_1289" s="T1953">a-bi-jəʔ</ta>
            <ta e="T1956" id="Seg_1290" s="T1955">a-laʔbə-bi</ta>
            <ta e="T1957" id="Seg_1291" s="T1956">iʔgö</ta>
            <ta e="T1958" id="Seg_1292" s="T1957">nerö-luʔbdə-bi</ta>
            <ta e="T1959" id="Seg_1293" s="T1958">što</ta>
            <ta e="T1960" id="Seg_1294" s="T1959">koŋ</ta>
            <ta e="T1961" id="Seg_1295" s="T1960">bos-də</ta>
            <ta e="T1962" id="Seg_1296" s="T1961">ne-t-ziʔ</ta>
            <ta e="T1963" id="Seg_1297" s="T1962">šonə-gA</ta>
            <ta e="T1964" id="Seg_1298" s="T1963">i</ta>
            <ta e="T1965" id="Seg_1299" s="T1964">kuba-jəʔ</ta>
            <ta e="T1967" id="Seg_1300" s="T1966">săj-nʼeʔbdə-luʔbdə-bi</ta>
            <ta e="T1968" id="Seg_1301" s="T1967">dĭgəttə</ta>
            <ta e="T1969" id="Seg_1302" s="T1968">ej</ta>
            <ta e="T1970" id="Seg_1303" s="T1969">kan-bi</ta>
            <ta e="T1971" id="Seg_1304" s="T1970">dĭ-zAŋ</ta>
            <ta e="T1972" id="Seg_1305" s="T1971">a</ta>
            <ta e="T1974" id="Seg_1306" s="T1973">dĭgəttə</ta>
            <ta e="T1975" id="Seg_1307" s="T1974">dĭ</ta>
            <ta e="T1976" id="Seg_1308" s="T1975">iʔgö</ta>
            <ta e="T1977" id="Seg_1309" s="T1976">ešši-zAŋ</ta>
            <ta e="T1978" id="Seg_1310" s="T1977">oʔbdo-bi</ta>
            <ta e="T1979" id="Seg_1311" s="T1978">ešši-zAŋ</ta>
            <ta e="T1981" id="Seg_1312" s="T1980">kan-bi-jəʔ</ta>
            <ta e="T1982" id="Seg_1313" s="T1981">bar</ta>
            <ta e="T1983" id="Seg_1314" s="T1982">tʼor-bi-jəʔ</ta>
            <ta e="T1984" id="Seg_1315" s="T1983">dĭ</ta>
            <ta e="T1985" id="Seg_1316" s="T1984">dĭgəttə</ta>
            <ta e="T1986" id="Seg_1317" s="T1985">kan-bi</ta>
            <ta e="T1987" id="Seg_1318" s="T1986">iʔgö</ta>
            <ta e="T1988" id="Seg_1319" s="T1987">kudʼelʼə</ta>
            <ta e="T1989" id="Seg_1320" s="T1988">i-bi</ta>
            <ta e="T1990" id="Seg_1321" s="T1989">dʼögatʼ-ziʔ</ta>
            <ta e="T1991" id="Seg_1322" s="T1990">tʼuʔbdə-bi</ta>
            <ta e="T1992" id="Seg_1323" s="T1991">kür-laʔbə-bi</ta>
            <ta e="T1993" id="Seg_1324" s="T1992">dĭgəttə</ta>
            <ta e="T1994" id="Seg_1325" s="T1993">šo-bi</ta>
            <ta e="T1995" id="Seg_1326" s="T1994">pĭnzə-t</ta>
            <ta e="T1996" id="Seg_1327" s="T1995">berlogə-ttə</ta>
            <ta e="T1997" id="Seg_1328" s="T1996">supso-ʔ</ta>
            <ta e="T1998" id="Seg_1329" s="T1997">ato</ta>
            <ta e="T1999" id="Seg_1330" s="T1998">vsʼoravno</ta>
            <ta e="T2000" id="Seg_1331" s="T1999">tănan</ta>
            <ta e="T2001" id="Seg_1332" s="T2000">kut-lV-m</ta>
            <ta e="T2002" id="Seg_1333" s="T2001">dĭgəttə</ta>
            <ta e="T2003" id="Seg_1334" s="T2002">pa-jəʔ</ta>
            <ta e="T2004" id="Seg_1335" s="T2003">bar</ta>
            <ta e="T2005" id="Seg_1336" s="T2004">i-laʔbə</ta>
            <ta e="T2006" id="Seg_1337" s="T2005">dĭgəttə</ta>
            <ta e="T2007" id="Seg_1338" s="T2006">dĭ</ta>
            <ta e="T2008" id="Seg_1339" s="T2007">supso-bi</ta>
            <ta e="T2009" id="Seg_1340" s="T2008">davaj</ta>
            <ta e="T2010" id="Seg_1341" s="T2009">tʼabəro-l-bi</ta>
            <ta e="T2011" id="Seg_1342" s="T2010">tʼabəro-bi-jəʔ</ta>
            <ta e="T2012" id="Seg_1343" s="T2011">tʼabəro-bi-jəʔ</ta>
            <ta e="T2013" id="Seg_1344" s="T2012">dĭgəttə</ta>
            <ta e="T2014" id="Seg_1345" s="T2013">pĭnzə</ta>
            <ta e="T2015" id="Seg_1346" s="T2014">măn-ntə</ta>
            <ta e="T2016" id="Seg_1347" s="T2015">ĭmbi</ta>
            <ta e="T2018" id="Seg_1348" s="T2017">miʔnʼibeʔ</ta>
            <ta e="T2019" id="Seg_1349" s="T2018">tʼabəro-zittə</ta>
            <ta e="T2020" id="Seg_1350" s="T2019">tăn</ta>
            <ta e="T2021" id="Seg_1351" s="T2020">pʼel-də</ta>
            <ta e="T2024" id="Seg_1352" s="T2023">măn</ta>
            <ta e="T2025" id="Seg_1353" s="T2024">pʼel-də</ta>
            <ta e="T2026" id="Seg_1354" s="T2025">dĭgəttə</ta>
            <ta e="T2027" id="Seg_1355" s="T2026">dĭ</ta>
            <ta e="T2029" id="Seg_1356" s="T2028">măn-ntə</ta>
            <ta e="T2030" id="Seg_1357" s="T2029">nadə</ta>
            <ta e="T2031" id="Seg_1358" s="T2030">granʼ</ta>
            <ta e="T2032" id="Seg_1359" s="T2031">hen-zittə</ta>
            <ta e="T2033" id="Seg_1360" s="T2032">dĭgəttə</ta>
            <ta e="T2034" id="Seg_1361" s="T2033">dĭ-m</ta>
            <ta e="T2035" id="Seg_1362" s="T2034">körer-bi</ta>
            <ta e="T2036" id="Seg_1363" s="T2035">dĭ</ta>
            <ta e="T2037" id="Seg_1364" s="T2036">bar</ta>
            <ta e="T2038" id="Seg_1365" s="T2037">tʼo-bə</ta>
            <ta e="T2039" id="Seg_1366" s="T2038">ugaːndə</ta>
            <ta e="T2040" id="Seg_1367" s="T2039">iʔgö</ta>
            <ta e="T2041" id="Seg_1368" s="T2040">tĭl-bi</ta>
            <ta e="T2042" id="Seg_1369" s="T2041">dĭgəttə</ta>
            <ta e="T2043" id="Seg_1370" s="T2042">măn-ntə</ta>
            <ta e="T2044" id="Seg_1371" s="T2043">nadə</ta>
            <ta e="T2045" id="Seg_1372" s="T2044">bü</ta>
            <ta e="T2046" id="Seg_1373" s="T2045">tože</ta>
            <ta e="T2047" id="Seg_1374" s="T2046">šide</ta>
            <ta e="T2048" id="Seg_1375" s="T2047">pʼel-də</ta>
            <ta e="T2049" id="Seg_1376" s="T2048">a-zittə</ta>
            <ta e="T2050" id="Seg_1377" s="T2049">dĭ</ta>
            <ta e="T2051" id="Seg_1378" s="T2050">bü-Tə</ta>
            <ta e="T2052" id="Seg_1379" s="T2051">kan-bi</ta>
            <ta e="T2053" id="Seg_1380" s="T2052">i</ta>
            <ta e="T2054" id="Seg_1381" s="T2053">dĭ</ta>
            <ta e="T2055" id="Seg_1382" s="T2054">dĭn</ta>
            <ta e="T2056" id="Seg_1383" s="T2055">kü-laːm-bi</ta>
            <ta e="T2057" id="Seg_1384" s="T2056">dĭgəttə</ta>
            <ta e="T2058" id="Seg_1385" s="T2057">il</ta>
            <ta e="T2059" id="Seg_1386" s="T2058">bar</ta>
            <ta e="T2060" id="Seg_1387" s="T2059">tajər-luʔbdə-bi</ta>
            <ta e="T2061" id="Seg_1388" s="T2060">a</ta>
            <ta e="T2062" id="Seg_1389" s="T2061">dĭ</ta>
            <ta e="T2063" id="Seg_1390" s="T2062">tʼo</ta>
            <ta e="T2064" id="Seg_1391" s="T2063">ugaːndə</ta>
            <ta e="T2065" id="Seg_1392" s="T2064">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1899" id="Seg_1393" s="T1898">live-PST.[3SG]</ta>
            <ta e="T1900" id="Seg_1394" s="T1899">serpent.[NOM.SG]</ta>
            <ta e="T1901" id="Seg_1395" s="T1900">very</ta>
            <ta e="T1902" id="Seg_1396" s="T1901">child-PL</ta>
            <ta e="T1903" id="Seg_1397" s="T1902">many</ta>
            <ta e="T1904" id="Seg_1398" s="T1903">eat-PST.[3SG]</ta>
            <ta e="T1905" id="Seg_1399" s="T1904">then</ta>
            <ta e="T1906" id="Seg_1400" s="T1905">chief-LOC</ta>
            <ta e="T1907" id="Seg_1401" s="T1906">daughter-NOM/GEN.3SG</ta>
            <ta e="T1908" id="Seg_1402" s="T1907">bring-RES-PST.[3SG]</ta>
            <ta e="T1909" id="Seg_1403" s="T1908">this-ACC</ta>
            <ta e="T1910" id="Seg_1404" s="T1909">NEG</ta>
            <ta e="T1911" id="Seg_1405" s="T1910">eat-PST.[3SG]</ta>
            <ta e="T1912" id="Seg_1406" s="T1911">there</ta>
            <ta e="T1913" id="Seg_1407" s="T1912">dog.[NOM.SG]</ta>
            <ta e="T1914" id="Seg_1408" s="T1913">be-PST.[3SG]</ta>
            <ta e="T1915" id="Seg_1409" s="T1914">then</ta>
            <ta e="T1916" id="Seg_1410" s="T1915">this.[NOM.SG]</ta>
            <ta e="T1917" id="Seg_1411" s="T1916">write-FUT-3SG</ta>
            <ta e="T1918" id="Seg_1412" s="T1917">paper-LAT</ta>
            <ta e="T1919" id="Seg_1413" s="T1918">father-LAT/LOC.3SG</ta>
            <ta e="T1920" id="Seg_1414" s="T1919">let-FUT-3SG</ta>
            <ta e="T1921" id="Seg_1415" s="T1920">dog.[NOM.SG]</ta>
            <ta e="T1924" id="Seg_1416" s="T1923">this-PL-ABL</ta>
            <ta e="T1925" id="Seg_1417" s="T1924">bring-FUT-3SG</ta>
            <ta e="T1926" id="Seg_1418" s="T1925">paper.[NOM.SG]</ta>
            <ta e="T1927" id="Seg_1419" s="T1926">this-LAT</ta>
            <ta e="T1928" id="Seg_1420" s="T1927">then</ta>
            <ta e="T1929" id="Seg_1421" s="T1928">mother-NOM/GEN.3SG</ta>
            <ta e="T1930" id="Seg_1422" s="T1929">father-NOM/GEN.3SG</ta>
            <ta e="T1931" id="Seg_1423" s="T1930">say-IPFVZ.[3SG]</ta>
            <ta e="T1932" id="Seg_1424" s="T1931">ask-IMP.2SG</ta>
            <ta e="T1933" id="Seg_1425" s="T1932">who.[NOM.SG]</ta>
            <ta e="T1934" id="Seg_1426" s="T1933">powerful.[NOM.SG]</ta>
            <ta e="T1935" id="Seg_1427" s="T1934">that.ABL</ta>
            <ta e="T1936" id="Seg_1428" s="T1935">this.[NOM.SG]</ta>
            <ta e="T1937" id="Seg_1429" s="T1936">INCH</ta>
            <ta e="T1938" id="Seg_1430" s="T1937">ask-INF.LAT</ta>
            <ta e="T1939" id="Seg_1431" s="T1938">this.[NOM.SG]</ta>
            <ta e="T1940" id="Seg_1432" s="T1939">PTCL</ta>
            <ta e="T1941" id="Seg_1433" s="T1940">tell-PST.[3SG]</ta>
            <ta e="T1942" id="Seg_1434" s="T1941">Nikita.[NOM.SG]</ta>
            <ta e="T1943" id="Seg_1435" s="T1942">Kozhemyaka.[NOM.SG]</ta>
            <ta e="T1944" id="Seg_1436" s="T1943">powerful.[NOM.SG]</ta>
            <ta e="T1945" id="Seg_1437" s="T1944">I.LAT</ta>
            <ta e="T1946" id="Seg_1438" s="T1945">then</ta>
            <ta e="T1947" id="Seg_1439" s="T1946">this.[NOM.SG]</ta>
            <ta e="T1948" id="Seg_1440" s="T1947">send-MOM-PST.[3SG]</ta>
            <ta e="T1949" id="Seg_1441" s="T1948">this-PL</ta>
            <ta e="T1950" id="Seg_1442" s="T1949">go-PST-3PL</ta>
            <ta e="T1951" id="Seg_1443" s="T1950">two-COLL</ta>
            <ta e="T1952" id="Seg_1444" s="T1951">this.[NOM.SG]</ta>
            <ta e="T1953" id="Seg_1445" s="T1952">skin-PL</ta>
            <ta e="T1954" id="Seg_1446" s="T1953">make-PST-3PL</ta>
            <ta e="T1956" id="Seg_1447" s="T1955">make-DUR-PST.[3SG]</ta>
            <ta e="T1957" id="Seg_1448" s="T1956">many</ta>
            <ta e="T1958" id="Seg_1449" s="T1957">get.frightened-MOM-PST.[3SG]</ta>
            <ta e="T1959" id="Seg_1450" s="T1958">that</ta>
            <ta e="T1960" id="Seg_1451" s="T1959">chief.[NOM.SG]</ta>
            <ta e="T1961" id="Seg_1452" s="T1960">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1962" id="Seg_1453" s="T1961">woman-3SG-INS</ta>
            <ta e="T1963" id="Seg_1454" s="T1962">come-PRS.[3SG]</ta>
            <ta e="T1964" id="Seg_1455" s="T1963">and</ta>
            <ta e="T1965" id="Seg_1456" s="T1964">skin-PL</ta>
            <ta e="T1967" id="Seg_1457" s="T1966">off-pull-MOM-PST.[3SG]</ta>
            <ta e="T1968" id="Seg_1458" s="T1967">then</ta>
            <ta e="T1969" id="Seg_1459" s="T1968">NEG</ta>
            <ta e="T1970" id="Seg_1460" s="T1969">go-PST.[3SG]</ta>
            <ta e="T1971" id="Seg_1461" s="T1970">this-PL</ta>
            <ta e="T1972" id="Seg_1462" s="T1971">and</ta>
            <ta e="T1974" id="Seg_1463" s="T1973">then</ta>
            <ta e="T1975" id="Seg_1464" s="T1974">this.[NOM.SG]</ta>
            <ta e="T1976" id="Seg_1465" s="T1975">many</ta>
            <ta e="T1977" id="Seg_1466" s="T1976">child-PL</ta>
            <ta e="T1978" id="Seg_1467" s="T1977">gather-PST.[3SG]</ta>
            <ta e="T1979" id="Seg_1468" s="T1978">child-PL</ta>
            <ta e="T1981" id="Seg_1469" s="T1980">go-PST-3PL</ta>
            <ta e="T1982" id="Seg_1470" s="T1981">all</ta>
            <ta e="T1983" id="Seg_1471" s="T1982">cry-PST-3PL</ta>
            <ta e="T1984" id="Seg_1472" s="T1983">this.[NOM.SG]</ta>
            <ta e="T1985" id="Seg_1473" s="T1984">then</ta>
            <ta e="T1986" id="Seg_1474" s="T1985">go-PST.[3SG]</ta>
            <ta e="T1987" id="Seg_1475" s="T1986">many</ta>
            <ta e="T1988" id="Seg_1476" s="T1987">tow.[NOM.SG]</ta>
            <ta e="T1989" id="Seg_1477" s="T1988">take-PST.[3SG]</ta>
            <ta e="T1990" id="Seg_1478" s="T1989">tar-INS</ta>
            <ta e="T1991" id="Seg_1479" s="T1990">smear-PST.[3SG]</ta>
            <ta e="T1992" id="Seg_1480" s="T1991">plait-DUR-PST.[3SG]</ta>
            <ta e="T1993" id="Seg_1481" s="T1992">then</ta>
            <ta e="T1994" id="Seg_1482" s="T1993">come-PST.[3SG]</ta>
            <ta e="T1995" id="Seg_1483" s="T1994">serpent-NOM/GEN.3SG</ta>
            <ta e="T1996" id="Seg_1484" s="T1995">lair-ABL.3SG</ta>
            <ta e="T1997" id="Seg_1485" s="T1996">depart-IMP.2SG</ta>
            <ta e="T1998" id="Seg_1486" s="T1997">otherwise</ta>
            <ta e="T1999" id="Seg_1487" s="T1998">anyway</ta>
            <ta e="T2000" id="Seg_1488" s="T1999">you.ACC</ta>
            <ta e="T2001" id="Seg_1489" s="T2000">kill-FUT-1SG</ta>
            <ta e="T2002" id="Seg_1490" s="T2001">then</ta>
            <ta e="T2003" id="Seg_1491" s="T2002">tree-PL</ta>
            <ta e="T2004" id="Seg_1492" s="T2003">PTCL</ta>
            <ta e="T2005" id="Seg_1493" s="T2004">take-DUR.[3SG]</ta>
            <ta e="T2006" id="Seg_1494" s="T2005">then</ta>
            <ta e="T2007" id="Seg_1495" s="T2006">this.[NOM.SG]</ta>
            <ta e="T2008" id="Seg_1496" s="T2007">depart-PST.[3SG]</ta>
            <ta e="T2009" id="Seg_1497" s="T2008">INCH</ta>
            <ta e="T2010" id="Seg_1498" s="T2009">fight-FRQ-PST.[3SG]</ta>
            <ta e="T2011" id="Seg_1499" s="T2010">fight-PST-3PL</ta>
            <ta e="T2012" id="Seg_1500" s="T2011">fight-PST-3PL</ta>
            <ta e="T2013" id="Seg_1501" s="T2012">then</ta>
            <ta e="T2014" id="Seg_1502" s="T2013">serpent.[NOM.SG]</ta>
            <ta e="T2015" id="Seg_1503" s="T2014">say-IPFVZ.[3SG]</ta>
            <ta e="T2016" id="Seg_1504" s="T2015">what.[NOM.SG]</ta>
            <ta e="T2018" id="Seg_1505" s="T2017">we.LAT</ta>
            <ta e="T2019" id="Seg_1506" s="T2018">fight-INF.LAT</ta>
            <ta e="T2020" id="Seg_1507" s="T2019">you.GEN</ta>
            <ta e="T2021" id="Seg_1508" s="T2020">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T2024" id="Seg_1509" s="T2023">I.GEN</ta>
            <ta e="T2025" id="Seg_1510" s="T2024">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T2026" id="Seg_1511" s="T2025">then</ta>
            <ta e="T2027" id="Seg_1512" s="T2026">this.[NOM.SG]</ta>
            <ta e="T2029" id="Seg_1513" s="T2028">say-IPFVZ.[3SG]</ta>
            <ta e="T2030" id="Seg_1514" s="T2029">one.should</ta>
            <ta e="T2031" id="Seg_1515" s="T2030">border.[NOM.SG]</ta>
            <ta e="T2032" id="Seg_1516" s="T2031">put-INF.LAT</ta>
            <ta e="T2033" id="Seg_1517" s="T2032">then</ta>
            <ta e="T2034" id="Seg_1518" s="T2033">this-ACC</ta>
            <ta e="T2035" id="Seg_1519" s="T2034">harness-PST.[3SG]</ta>
            <ta e="T2036" id="Seg_1520" s="T2035">this.[NOM.SG]</ta>
            <ta e="T2037" id="Seg_1521" s="T2036">all</ta>
            <ta e="T2038" id="Seg_1522" s="T2037">earth-ACC.3SG</ta>
            <ta e="T2039" id="Seg_1523" s="T2038">very</ta>
            <ta e="T2040" id="Seg_1524" s="T2039">many</ta>
            <ta e="T2041" id="Seg_1525" s="T2040">dig-PST.[3SG]</ta>
            <ta e="T2042" id="Seg_1526" s="T2041">then</ta>
            <ta e="T2043" id="Seg_1527" s="T2042">say-IPFVZ.[3SG]</ta>
            <ta e="T2044" id="Seg_1528" s="T2043">one.should</ta>
            <ta e="T2045" id="Seg_1529" s="T2044">water.[NOM.SG]</ta>
            <ta e="T2046" id="Seg_1530" s="T2045">also</ta>
            <ta e="T2047" id="Seg_1531" s="T2046">two.[NOM.SG]</ta>
            <ta e="T2048" id="Seg_1532" s="T2047">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T2049" id="Seg_1533" s="T2048">make-INF.LAT</ta>
            <ta e="T2050" id="Seg_1534" s="T2049">this.[NOM.SG]</ta>
            <ta e="T2051" id="Seg_1535" s="T2050">water-LAT</ta>
            <ta e="T2052" id="Seg_1536" s="T2051">go-PST.[3SG]</ta>
            <ta e="T2053" id="Seg_1537" s="T2052">and</ta>
            <ta e="T2054" id="Seg_1538" s="T2053">this.[NOM.SG]</ta>
            <ta e="T2055" id="Seg_1539" s="T2054">there</ta>
            <ta e="T2056" id="Seg_1540" s="T2055">die-RES-PST.[3SG]</ta>
            <ta e="T2057" id="Seg_1541" s="T2056">then</ta>
            <ta e="T2058" id="Seg_1542" s="T2057">people.[NOM.SG]</ta>
            <ta e="T2059" id="Seg_1543" s="T2058">all</ta>
            <ta e="T2060" id="Seg_1544" s="T2059">plough-MOM-PST.[3SG]</ta>
            <ta e="T2061" id="Seg_1545" s="T2060">and</ta>
            <ta e="T2062" id="Seg_1546" s="T2061">this.[NOM.SG]</ta>
            <ta e="T2063" id="Seg_1547" s="T2062">earth.[NOM.SG]</ta>
            <ta e="T2064" id="Seg_1548" s="T2063">very</ta>
            <ta e="T2065" id="Seg_1549" s="T2064">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1899" id="Seg_1550" s="T1898">жить-PST.[3SG]</ta>
            <ta e="T1900" id="Seg_1551" s="T1899">змей.[NOM.SG]</ta>
            <ta e="T1901" id="Seg_1552" s="T1900">очень</ta>
            <ta e="T1902" id="Seg_1553" s="T1901">ребенок-PL</ta>
            <ta e="T1903" id="Seg_1554" s="T1902">много</ta>
            <ta e="T1904" id="Seg_1555" s="T1903">съесть-PST.[3SG]</ta>
            <ta e="T1905" id="Seg_1556" s="T1904">тогда</ta>
            <ta e="T1906" id="Seg_1557" s="T1905">вождь-LOC</ta>
            <ta e="T1907" id="Seg_1558" s="T1906">дочь-NOM/GEN.3SG</ta>
            <ta e="T1908" id="Seg_1559" s="T1907">нести-RES-PST.[3SG]</ta>
            <ta e="T1909" id="Seg_1560" s="T1908">этот-ACC</ta>
            <ta e="T1910" id="Seg_1561" s="T1909">NEG</ta>
            <ta e="T1911" id="Seg_1562" s="T1910">съесть-PST.[3SG]</ta>
            <ta e="T1912" id="Seg_1563" s="T1911">там</ta>
            <ta e="T1913" id="Seg_1564" s="T1912">собака.[NOM.SG]</ta>
            <ta e="T1914" id="Seg_1565" s="T1913">быть-PST.[3SG]</ta>
            <ta e="T1915" id="Seg_1566" s="T1914">тогда</ta>
            <ta e="T1916" id="Seg_1567" s="T1915">этот.[NOM.SG]</ta>
            <ta e="T1917" id="Seg_1568" s="T1916">писать-FUT-3SG</ta>
            <ta e="T1918" id="Seg_1569" s="T1917">бумага-LAT</ta>
            <ta e="T1919" id="Seg_1570" s="T1918">отец-LAT/LOC.3SG</ta>
            <ta e="T1920" id="Seg_1571" s="T1919">пускать-FUT-3SG</ta>
            <ta e="T1921" id="Seg_1572" s="T1920">собака.[NOM.SG]</ta>
            <ta e="T1924" id="Seg_1573" s="T1923">этот-PL-ABL</ta>
            <ta e="T1925" id="Seg_1574" s="T1924">принести-FUT-3SG</ta>
            <ta e="T1926" id="Seg_1575" s="T1925">бумага.[NOM.SG]</ta>
            <ta e="T1927" id="Seg_1576" s="T1926">этот-LAT</ta>
            <ta e="T1928" id="Seg_1577" s="T1927">тогда</ta>
            <ta e="T1929" id="Seg_1578" s="T1928">мать-NOM/GEN.3SG</ta>
            <ta e="T1930" id="Seg_1579" s="T1929">отец-NOM/GEN.3SG</ta>
            <ta e="T1931" id="Seg_1580" s="T1930">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1932" id="Seg_1581" s="T1931">спросить-IMP.2SG</ta>
            <ta e="T1933" id="Seg_1582" s="T1932">кто.[NOM.SG]</ta>
            <ta e="T1934" id="Seg_1583" s="T1933">сильный.[NOM.SG]</ta>
            <ta e="T1935" id="Seg_1584" s="T1934">тот.ABL</ta>
            <ta e="T1936" id="Seg_1585" s="T1935">этот.[NOM.SG]</ta>
            <ta e="T1937" id="Seg_1586" s="T1936">INCH</ta>
            <ta e="T1938" id="Seg_1587" s="T1937">спросить-INF.LAT</ta>
            <ta e="T1939" id="Seg_1588" s="T1938">этот.[NOM.SG]</ta>
            <ta e="T1940" id="Seg_1589" s="T1939">PTCL</ta>
            <ta e="T1941" id="Seg_1590" s="T1940">сказать-PST.[3SG]</ta>
            <ta e="T1942" id="Seg_1591" s="T1941">Никита.[NOM.SG]</ta>
            <ta e="T1943" id="Seg_1592" s="T1942">Кожемяка.[NOM.SG]</ta>
            <ta e="T1944" id="Seg_1593" s="T1943">сильный.[NOM.SG]</ta>
            <ta e="T1945" id="Seg_1594" s="T1944">я.LAT</ta>
            <ta e="T1946" id="Seg_1595" s="T1945">тогда</ta>
            <ta e="T1947" id="Seg_1596" s="T1946">этот.[NOM.SG]</ta>
            <ta e="T1948" id="Seg_1597" s="T1947">послать-MOM-PST.[3SG]</ta>
            <ta e="T1949" id="Seg_1598" s="T1948">этот-PL</ta>
            <ta e="T1950" id="Seg_1599" s="T1949">пойти-PST-3PL</ta>
            <ta e="T1951" id="Seg_1600" s="T1950">два-COLL</ta>
            <ta e="T1952" id="Seg_1601" s="T1951">этот.[NOM.SG]</ta>
            <ta e="T1953" id="Seg_1602" s="T1952">кожа-PL</ta>
            <ta e="T1954" id="Seg_1603" s="T1953">делать-PST-3PL</ta>
            <ta e="T1956" id="Seg_1604" s="T1955">делать-DUR-PST.[3SG]</ta>
            <ta e="T1957" id="Seg_1605" s="T1956">много</ta>
            <ta e="T1958" id="Seg_1606" s="T1957">пугаться-MOM-PST.[3SG]</ta>
            <ta e="T1959" id="Seg_1607" s="T1958">что</ta>
            <ta e="T1960" id="Seg_1608" s="T1959">вождь.[NOM.SG]</ta>
            <ta e="T1961" id="Seg_1609" s="T1960">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1962" id="Seg_1610" s="T1961">женщина-3SG-INS</ta>
            <ta e="T1963" id="Seg_1611" s="T1962">прийти-PRS.[3SG]</ta>
            <ta e="T1964" id="Seg_1612" s="T1963">и</ta>
            <ta e="T1965" id="Seg_1613" s="T1964">кожа-PL</ta>
            <ta e="T1967" id="Seg_1614" s="T1966">от-тянуть-MOM-PST.[3SG]</ta>
            <ta e="T1968" id="Seg_1615" s="T1967">тогда</ta>
            <ta e="T1969" id="Seg_1616" s="T1968">NEG</ta>
            <ta e="T1970" id="Seg_1617" s="T1969">пойти-PST.[3SG]</ta>
            <ta e="T1971" id="Seg_1618" s="T1970">этот-PL</ta>
            <ta e="T1972" id="Seg_1619" s="T1971">а</ta>
            <ta e="T1974" id="Seg_1620" s="T1973">тогда</ta>
            <ta e="T1975" id="Seg_1621" s="T1974">этот.[NOM.SG]</ta>
            <ta e="T1976" id="Seg_1622" s="T1975">много</ta>
            <ta e="T1977" id="Seg_1623" s="T1976">ребенок-PL</ta>
            <ta e="T1978" id="Seg_1624" s="T1977">собрать-PST.[3SG]</ta>
            <ta e="T1979" id="Seg_1625" s="T1978">ребенок-PL</ta>
            <ta e="T1981" id="Seg_1626" s="T1980">пойти-PST-3PL</ta>
            <ta e="T1982" id="Seg_1627" s="T1981">весь</ta>
            <ta e="T1983" id="Seg_1628" s="T1982">плакать-PST-3PL</ta>
            <ta e="T1984" id="Seg_1629" s="T1983">этот.[NOM.SG]</ta>
            <ta e="T1985" id="Seg_1630" s="T1984">тогда</ta>
            <ta e="T1986" id="Seg_1631" s="T1985">пойти-PST.[3SG]</ta>
            <ta e="T1987" id="Seg_1632" s="T1986">много</ta>
            <ta e="T1988" id="Seg_1633" s="T1987">кудель.[NOM.SG]</ta>
            <ta e="T1989" id="Seg_1634" s="T1988">взять-PST.[3SG]</ta>
            <ta e="T1990" id="Seg_1635" s="T1989">деготь-INS</ta>
            <ta e="T1991" id="Seg_1636" s="T1990">мазать-PST.[3SG]</ta>
            <ta e="T1992" id="Seg_1637" s="T1991">плести-DUR-PST.[3SG]</ta>
            <ta e="T1993" id="Seg_1638" s="T1992">тогда</ta>
            <ta e="T1994" id="Seg_1639" s="T1993">прийти-PST.[3SG]</ta>
            <ta e="T1995" id="Seg_1640" s="T1994">змей-NOM/GEN.3SG</ta>
            <ta e="T1996" id="Seg_1641" s="T1995">берлога-ABL.3SG</ta>
            <ta e="T1997" id="Seg_1642" s="T1996">уйти-IMP.2SG</ta>
            <ta e="T1998" id="Seg_1643" s="T1997">а.то</ta>
            <ta e="T1999" id="Seg_1644" s="T1998">всё.равно</ta>
            <ta e="T2000" id="Seg_1645" s="T1999">ты.ACC</ta>
            <ta e="T2001" id="Seg_1646" s="T2000">убить-FUT-1SG</ta>
            <ta e="T2002" id="Seg_1647" s="T2001">тогда</ta>
            <ta e="T2003" id="Seg_1648" s="T2002">дерево-PL</ta>
            <ta e="T2004" id="Seg_1649" s="T2003">PTCL</ta>
            <ta e="T2005" id="Seg_1650" s="T2004">взять-DUR.[3SG]</ta>
            <ta e="T2006" id="Seg_1651" s="T2005">тогда</ta>
            <ta e="T2007" id="Seg_1652" s="T2006">этот.[NOM.SG]</ta>
            <ta e="T2008" id="Seg_1653" s="T2007">уйти-PST.[3SG]</ta>
            <ta e="T2009" id="Seg_1654" s="T2008">INCH</ta>
            <ta e="T2010" id="Seg_1655" s="T2009">бороться-FRQ-PST.[3SG]</ta>
            <ta e="T2011" id="Seg_1656" s="T2010">бороться-PST-3PL</ta>
            <ta e="T2012" id="Seg_1657" s="T2011">бороться-PST-3PL</ta>
            <ta e="T2013" id="Seg_1658" s="T2012">тогда</ta>
            <ta e="T2014" id="Seg_1659" s="T2013">змей.[NOM.SG]</ta>
            <ta e="T2015" id="Seg_1660" s="T2014">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2016" id="Seg_1661" s="T2015">что.[NOM.SG]</ta>
            <ta e="T2018" id="Seg_1662" s="T2017">мы.LAT</ta>
            <ta e="T2019" id="Seg_1663" s="T2018">бороться-INF.LAT</ta>
            <ta e="T2020" id="Seg_1664" s="T2019">ты.GEN</ta>
            <ta e="T2021" id="Seg_1665" s="T2020">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T2024" id="Seg_1666" s="T2023">я.GEN</ta>
            <ta e="T2025" id="Seg_1667" s="T2024">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T2026" id="Seg_1668" s="T2025">тогда</ta>
            <ta e="T2027" id="Seg_1669" s="T2026">этот.[NOM.SG]</ta>
            <ta e="T2029" id="Seg_1670" s="T2028">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2030" id="Seg_1671" s="T2029">надо</ta>
            <ta e="T2031" id="Seg_1672" s="T2030">граница.[NOM.SG]</ta>
            <ta e="T2032" id="Seg_1673" s="T2031">класть-INF.LAT</ta>
            <ta e="T2033" id="Seg_1674" s="T2032">тогда</ta>
            <ta e="T2034" id="Seg_1675" s="T2033">этот-ACC</ta>
            <ta e="T2035" id="Seg_1676" s="T2034">запрячь-PST.[3SG]</ta>
            <ta e="T2036" id="Seg_1677" s="T2035">этот.[NOM.SG]</ta>
            <ta e="T2037" id="Seg_1678" s="T2036">весь</ta>
            <ta e="T2038" id="Seg_1679" s="T2037">земля-ACC.3SG</ta>
            <ta e="T2039" id="Seg_1680" s="T2038">очень</ta>
            <ta e="T2040" id="Seg_1681" s="T2039">много</ta>
            <ta e="T2041" id="Seg_1682" s="T2040">копать-PST.[3SG]</ta>
            <ta e="T2042" id="Seg_1683" s="T2041">тогда</ta>
            <ta e="T2043" id="Seg_1684" s="T2042">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2044" id="Seg_1685" s="T2043">надо</ta>
            <ta e="T2045" id="Seg_1686" s="T2044">вода.[NOM.SG]</ta>
            <ta e="T2046" id="Seg_1687" s="T2045">тоже</ta>
            <ta e="T2047" id="Seg_1688" s="T2046">два.[NOM.SG]</ta>
            <ta e="T2048" id="Seg_1689" s="T2047">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T2049" id="Seg_1690" s="T2048">делать-INF.LAT</ta>
            <ta e="T2050" id="Seg_1691" s="T2049">этот.[NOM.SG]</ta>
            <ta e="T2051" id="Seg_1692" s="T2050">вода-LAT</ta>
            <ta e="T2052" id="Seg_1693" s="T2051">пойти-PST.[3SG]</ta>
            <ta e="T2053" id="Seg_1694" s="T2052">и</ta>
            <ta e="T2054" id="Seg_1695" s="T2053">этот.[NOM.SG]</ta>
            <ta e="T2055" id="Seg_1696" s="T2054">там</ta>
            <ta e="T2056" id="Seg_1697" s="T2055">умереть-RES-PST.[3SG]</ta>
            <ta e="T2057" id="Seg_1698" s="T2056">тогда</ta>
            <ta e="T2058" id="Seg_1699" s="T2057">люди.[NOM.SG]</ta>
            <ta e="T2059" id="Seg_1700" s="T2058">весь</ta>
            <ta e="T2060" id="Seg_1701" s="T2059">пахать-MOM-PST.[3SG]</ta>
            <ta e="T2061" id="Seg_1702" s="T2060">а</ta>
            <ta e="T2062" id="Seg_1703" s="T2061">этот.[NOM.SG]</ta>
            <ta e="T2063" id="Seg_1704" s="T2062">земля.[NOM.SG]</ta>
            <ta e="T2064" id="Seg_1705" s="T2063">очень</ta>
            <ta e="T2065" id="Seg_1706" s="T2064">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1899" id="Seg_1707" s="T1898">v-v:tense-v:pn</ta>
            <ta e="T1900" id="Seg_1708" s="T1899">n-n:case</ta>
            <ta e="T1901" id="Seg_1709" s="T1900">adv</ta>
            <ta e="T1902" id="Seg_1710" s="T1901">n-n:num</ta>
            <ta e="T1903" id="Seg_1711" s="T1902">quant</ta>
            <ta e="T1904" id="Seg_1712" s="T1903">v-v:tense-v:pn</ta>
            <ta e="T1905" id="Seg_1713" s="T1904">adv</ta>
            <ta e="T1906" id="Seg_1714" s="T1905">n-n:case</ta>
            <ta e="T1907" id="Seg_1715" s="T1906">n-n:case.poss</ta>
            <ta e="T1908" id="Seg_1716" s="T1907">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1909" id="Seg_1717" s="T1908">dempro-n:case</ta>
            <ta e="T1910" id="Seg_1718" s="T1909">ptcl</ta>
            <ta e="T1911" id="Seg_1719" s="T1910">v-v:tense-v:pn</ta>
            <ta e="T1912" id="Seg_1720" s="T1911">adv</ta>
            <ta e="T1913" id="Seg_1721" s="T1912">n-n:case</ta>
            <ta e="T1914" id="Seg_1722" s="T1913">v-v:tense-v:pn</ta>
            <ta e="T1915" id="Seg_1723" s="T1914">adv</ta>
            <ta e="T1916" id="Seg_1724" s="T1915">dempro-n:case</ta>
            <ta e="T1917" id="Seg_1725" s="T1916">v-v:tense-v:pn</ta>
            <ta e="T1918" id="Seg_1726" s="T1917">n-n:case</ta>
            <ta e="T1919" id="Seg_1727" s="T1918">n-n:case.poss</ta>
            <ta e="T1920" id="Seg_1728" s="T1919">v-v:tense-v:pn</ta>
            <ta e="T1921" id="Seg_1729" s="T1920">n-n:case</ta>
            <ta e="T1924" id="Seg_1730" s="T1923">dempro-n:num-n:case</ta>
            <ta e="T1925" id="Seg_1731" s="T1924">v-v:tense-v:pn</ta>
            <ta e="T1926" id="Seg_1732" s="T1925">n-n:case</ta>
            <ta e="T1927" id="Seg_1733" s="T1926">dempro-n:case</ta>
            <ta e="T1928" id="Seg_1734" s="T1927">adv</ta>
            <ta e="T1929" id="Seg_1735" s="T1928">n-n:case.poss</ta>
            <ta e="T1930" id="Seg_1736" s="T1929">n-n:case.poss</ta>
            <ta e="T1931" id="Seg_1737" s="T1930">v-v&gt;v-v:pn</ta>
            <ta e="T1932" id="Seg_1738" s="T1931">v-v:mood.pn</ta>
            <ta e="T1933" id="Seg_1739" s="T1932">que</ta>
            <ta e="T1934" id="Seg_1740" s="T1933">adj-n:case</ta>
            <ta e="T1935" id="Seg_1741" s="T1934">dempro</ta>
            <ta e="T1936" id="Seg_1742" s="T1935">dempro-n:case</ta>
            <ta e="T1937" id="Seg_1743" s="T1936">ptcl</ta>
            <ta e="T1938" id="Seg_1744" s="T1937">v-v:n.fin</ta>
            <ta e="T1939" id="Seg_1745" s="T1938">dempro-n:case</ta>
            <ta e="T1940" id="Seg_1746" s="T1939">ptcl</ta>
            <ta e="T1941" id="Seg_1747" s="T1940">v-v:tense-v:pn</ta>
            <ta e="T1942" id="Seg_1748" s="T1941">propr-n:case</ta>
            <ta e="T1943" id="Seg_1749" s="T1942">propr-n:case</ta>
            <ta e="T1944" id="Seg_1750" s="T1943">adj-n:case</ta>
            <ta e="T1945" id="Seg_1751" s="T1944">pers</ta>
            <ta e="T1946" id="Seg_1752" s="T1945">adv</ta>
            <ta e="T1947" id="Seg_1753" s="T1946">dempro-n:case</ta>
            <ta e="T1948" id="Seg_1754" s="T1947">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1949" id="Seg_1755" s="T1948">dempro-n:num</ta>
            <ta e="T1950" id="Seg_1756" s="T1949">v-v:tense-v:pn</ta>
            <ta e="T1951" id="Seg_1757" s="T1950">num-num&gt;num</ta>
            <ta e="T1952" id="Seg_1758" s="T1951">dempro-n:case</ta>
            <ta e="T1953" id="Seg_1759" s="T1952">n-n:num</ta>
            <ta e="T1954" id="Seg_1760" s="T1953">v-v:tense-v:pn</ta>
            <ta e="T1956" id="Seg_1761" s="T1955">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1957" id="Seg_1762" s="T1956">quant</ta>
            <ta e="T1958" id="Seg_1763" s="T1957">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1959" id="Seg_1764" s="T1958">conj</ta>
            <ta e="T1960" id="Seg_1765" s="T1959">n-n:case</ta>
            <ta e="T1961" id="Seg_1766" s="T1960">refl-n:case.poss</ta>
            <ta e="T1962" id="Seg_1767" s="T1961">n-n:case.poss-n:case</ta>
            <ta e="T1963" id="Seg_1768" s="T1962">v-v:tense-v:pn</ta>
            <ta e="T1964" id="Seg_1769" s="T1963">conj</ta>
            <ta e="T1965" id="Seg_1770" s="T1964">n-n:num</ta>
            <ta e="T1967" id="Seg_1771" s="T1966">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1968" id="Seg_1772" s="T1967">adv</ta>
            <ta e="T1969" id="Seg_1773" s="T1968">ptcl</ta>
            <ta e="T1970" id="Seg_1774" s="T1969">v-v:tense-v:pn</ta>
            <ta e="T1971" id="Seg_1775" s="T1970">dempro-n:num</ta>
            <ta e="T1972" id="Seg_1776" s="T1971">conj</ta>
            <ta e="T1974" id="Seg_1777" s="T1973">adv</ta>
            <ta e="T1975" id="Seg_1778" s="T1974">dempro-n:case</ta>
            <ta e="T1976" id="Seg_1779" s="T1975">quant</ta>
            <ta e="T1977" id="Seg_1780" s="T1976">n-n:num</ta>
            <ta e="T1978" id="Seg_1781" s="T1977">v-v:tense-v:pn</ta>
            <ta e="T1979" id="Seg_1782" s="T1978">n-n:num</ta>
            <ta e="T1981" id="Seg_1783" s="T1980">v-v:tense-v:pn</ta>
            <ta e="T1982" id="Seg_1784" s="T1981">quant</ta>
            <ta e="T1983" id="Seg_1785" s="T1982">v-v:tense-v:pn</ta>
            <ta e="T1984" id="Seg_1786" s="T1983">dempro-n:case</ta>
            <ta e="T1985" id="Seg_1787" s="T1984">adv</ta>
            <ta e="T1986" id="Seg_1788" s="T1985">v-v:tense-v:pn</ta>
            <ta e="T1987" id="Seg_1789" s="T1986">quant</ta>
            <ta e="T1988" id="Seg_1790" s="T1987">n-n:case</ta>
            <ta e="T1989" id="Seg_1791" s="T1988">v-v:tense-v:pn</ta>
            <ta e="T1990" id="Seg_1792" s="T1989">n-n:case</ta>
            <ta e="T1991" id="Seg_1793" s="T1990">v-v:tense-v:pn</ta>
            <ta e="T1992" id="Seg_1794" s="T1991">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1993" id="Seg_1795" s="T1992">adv</ta>
            <ta e="T1994" id="Seg_1796" s="T1993">v-v:tense-v:pn</ta>
            <ta e="T1995" id="Seg_1797" s="T1994">n-n:case.poss</ta>
            <ta e="T1996" id="Seg_1798" s="T1995">n-n:case.poss</ta>
            <ta e="T1997" id="Seg_1799" s="T1996">v-v:mood.pn</ta>
            <ta e="T1998" id="Seg_1800" s="T1997">ptcl</ta>
            <ta e="T1999" id="Seg_1801" s="T1998">adv</ta>
            <ta e="T2000" id="Seg_1802" s="T1999">pers</ta>
            <ta e="T2001" id="Seg_1803" s="T2000">v-v:tense-v:pn</ta>
            <ta e="T2002" id="Seg_1804" s="T2001">adv</ta>
            <ta e="T2003" id="Seg_1805" s="T2002">n-n:num</ta>
            <ta e="T2004" id="Seg_1806" s="T2003">ptcl</ta>
            <ta e="T2005" id="Seg_1807" s="T2004">v-v&gt;v-v:pn</ta>
            <ta e="T2006" id="Seg_1808" s="T2005">adv</ta>
            <ta e="T2007" id="Seg_1809" s="T2006">dempro-n:case</ta>
            <ta e="T2008" id="Seg_1810" s="T2007">v-v:tense-v:pn</ta>
            <ta e="T2009" id="Seg_1811" s="T2008">ptcl</ta>
            <ta e="T2010" id="Seg_1812" s="T2009">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2011" id="Seg_1813" s="T2010">v-v:tense-v:pn</ta>
            <ta e="T2012" id="Seg_1814" s="T2011">v-v:tense-v:pn</ta>
            <ta e="T2013" id="Seg_1815" s="T2012">adv</ta>
            <ta e="T2014" id="Seg_1816" s="T2013">n-n:case</ta>
            <ta e="T2015" id="Seg_1817" s="T2014">v-v&gt;v-v:pn</ta>
            <ta e="T2016" id="Seg_1818" s="T2015">que-n:case</ta>
            <ta e="T2018" id="Seg_1819" s="T2017">pers</ta>
            <ta e="T2019" id="Seg_1820" s="T2018">v-v:n.fin</ta>
            <ta e="T2020" id="Seg_1821" s="T2019">pers</ta>
            <ta e="T2021" id="Seg_1822" s="T2020">n-n:case.poss</ta>
            <ta e="T2024" id="Seg_1823" s="T2023">pers</ta>
            <ta e="T2025" id="Seg_1824" s="T2024">n-n:case.poss</ta>
            <ta e="T2026" id="Seg_1825" s="T2025">adv</ta>
            <ta e="T2027" id="Seg_1826" s="T2026">dempro-n:case</ta>
            <ta e="T2029" id="Seg_1827" s="T2028">v-v&gt;v-v:pn</ta>
            <ta e="T2030" id="Seg_1828" s="T2029">ptcl</ta>
            <ta e="T2031" id="Seg_1829" s="T2030">n-n:case</ta>
            <ta e="T2032" id="Seg_1830" s="T2031">v-v:n.fin</ta>
            <ta e="T2033" id="Seg_1831" s="T2032">adv</ta>
            <ta e="T2034" id="Seg_1832" s="T2033">dempro-n:case</ta>
            <ta e="T2035" id="Seg_1833" s="T2034">v-v:tense-v:pn</ta>
            <ta e="T2036" id="Seg_1834" s="T2035">dempro-n:case</ta>
            <ta e="T2037" id="Seg_1835" s="T2036">quant</ta>
            <ta e="T2038" id="Seg_1836" s="T2037">n-n:case.poss</ta>
            <ta e="T2039" id="Seg_1837" s="T2038">adv</ta>
            <ta e="T2040" id="Seg_1838" s="T2039">quant</ta>
            <ta e="T2041" id="Seg_1839" s="T2040">v-v:tense-v:pn</ta>
            <ta e="T2042" id="Seg_1840" s="T2041">adv</ta>
            <ta e="T2043" id="Seg_1841" s="T2042">v-v&gt;v-v:pn</ta>
            <ta e="T2044" id="Seg_1842" s="T2043">ptcl</ta>
            <ta e="T2045" id="Seg_1843" s="T2044">n-n:case</ta>
            <ta e="T2046" id="Seg_1844" s="T2045">ptcl</ta>
            <ta e="T2047" id="Seg_1845" s="T2046">num-n:case</ta>
            <ta e="T2048" id="Seg_1846" s="T2047">n-n:case.poss</ta>
            <ta e="T2049" id="Seg_1847" s="T2048">v-v:n.fin</ta>
            <ta e="T2050" id="Seg_1848" s="T2049">dempro-n:case</ta>
            <ta e="T2051" id="Seg_1849" s="T2050">n-n:case</ta>
            <ta e="T2052" id="Seg_1850" s="T2051">v-v:tense-v:pn</ta>
            <ta e="T2053" id="Seg_1851" s="T2052">conj</ta>
            <ta e="T2054" id="Seg_1852" s="T2053">dempro-n:case</ta>
            <ta e="T2055" id="Seg_1853" s="T2054">adv</ta>
            <ta e="T2056" id="Seg_1854" s="T2055">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2057" id="Seg_1855" s="T2056">adv</ta>
            <ta e="T2058" id="Seg_1856" s="T2057">n-n:case</ta>
            <ta e="T2059" id="Seg_1857" s="T2058">quant</ta>
            <ta e="T2060" id="Seg_1858" s="T2059">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2061" id="Seg_1859" s="T2060">conj</ta>
            <ta e="T2062" id="Seg_1860" s="T2061">dempro-n:case</ta>
            <ta e="T2063" id="Seg_1861" s="T2062">n-n:case</ta>
            <ta e="T2064" id="Seg_1862" s="T2063">adv</ta>
            <ta e="T2065" id="Seg_1863" s="T2064">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1899" id="Seg_1864" s="T1898">v</ta>
            <ta e="T1900" id="Seg_1865" s="T1899">n</ta>
            <ta e="T1901" id="Seg_1866" s="T1900">adv</ta>
            <ta e="T1902" id="Seg_1867" s="T1901">n</ta>
            <ta e="T1903" id="Seg_1868" s="T1902">quant</ta>
            <ta e="T1904" id="Seg_1869" s="T1903">v</ta>
            <ta e="T1905" id="Seg_1870" s="T1904">adv</ta>
            <ta e="T1906" id="Seg_1871" s="T1905">n</ta>
            <ta e="T1907" id="Seg_1872" s="T1906">n</ta>
            <ta e="T1908" id="Seg_1873" s="T1907">v</ta>
            <ta e="T1909" id="Seg_1874" s="T1908">dempro</ta>
            <ta e="T1910" id="Seg_1875" s="T1909">ptcl</ta>
            <ta e="T1911" id="Seg_1876" s="T1910">v</ta>
            <ta e="T1912" id="Seg_1877" s="T1911">adv</ta>
            <ta e="T1913" id="Seg_1878" s="T1912">n</ta>
            <ta e="T1914" id="Seg_1879" s="T1913">v</ta>
            <ta e="T1915" id="Seg_1880" s="T1914">adv</ta>
            <ta e="T1916" id="Seg_1881" s="T1915">dempro</ta>
            <ta e="T1917" id="Seg_1882" s="T1916">v</ta>
            <ta e="T1918" id="Seg_1883" s="T1917">n</ta>
            <ta e="T1919" id="Seg_1884" s="T1918">n</ta>
            <ta e="T1920" id="Seg_1885" s="T1919">v</ta>
            <ta e="T1921" id="Seg_1886" s="T1920">n</ta>
            <ta e="T1924" id="Seg_1887" s="T1923">dempro</ta>
            <ta e="T1925" id="Seg_1888" s="T1924">v</ta>
            <ta e="T1926" id="Seg_1889" s="T1925">n</ta>
            <ta e="T1927" id="Seg_1890" s="T1926">dempro</ta>
            <ta e="T1928" id="Seg_1891" s="T1927">adv</ta>
            <ta e="T1929" id="Seg_1892" s="T1928">n</ta>
            <ta e="T1930" id="Seg_1893" s="T1929">n</ta>
            <ta e="T1931" id="Seg_1894" s="T1930">v</ta>
            <ta e="T1932" id="Seg_1895" s="T1931">v</ta>
            <ta e="T1933" id="Seg_1896" s="T1932">que</ta>
            <ta e="T1934" id="Seg_1897" s="T1933">adj</ta>
            <ta e="T1935" id="Seg_1898" s="T1934">pers</ta>
            <ta e="T1936" id="Seg_1899" s="T1935">dempro</ta>
            <ta e="T1937" id="Seg_1900" s="T1936">ptcl</ta>
            <ta e="T1938" id="Seg_1901" s="T1937">v</ta>
            <ta e="T1939" id="Seg_1902" s="T1938">dempro</ta>
            <ta e="T1940" id="Seg_1903" s="T1939">ptcl</ta>
            <ta e="T1941" id="Seg_1904" s="T1940">v</ta>
            <ta e="T1942" id="Seg_1905" s="T1941">propr</ta>
            <ta e="T1943" id="Seg_1906" s="T1942">propr</ta>
            <ta e="T1944" id="Seg_1907" s="T1943">adj</ta>
            <ta e="T1945" id="Seg_1908" s="T1944">pers</ta>
            <ta e="T1946" id="Seg_1909" s="T1945">adv</ta>
            <ta e="T1947" id="Seg_1910" s="T1946">dempro</ta>
            <ta e="T1948" id="Seg_1911" s="T1947">v</ta>
            <ta e="T1949" id="Seg_1912" s="T1948">dempro</ta>
            <ta e="T1950" id="Seg_1913" s="T1949">v</ta>
            <ta e="T1951" id="Seg_1914" s="T1950">num</ta>
            <ta e="T1952" id="Seg_1915" s="T1951">dempro</ta>
            <ta e="T1953" id="Seg_1916" s="T1952">n</ta>
            <ta e="T1954" id="Seg_1917" s="T1953">v</ta>
            <ta e="T1956" id="Seg_1918" s="T1955">v</ta>
            <ta e="T1957" id="Seg_1919" s="T1956">quant</ta>
            <ta e="T1958" id="Seg_1920" s="T1957">v</ta>
            <ta e="T1959" id="Seg_1921" s="T1958">conj</ta>
            <ta e="T1960" id="Seg_1922" s="T1959">n</ta>
            <ta e="T1961" id="Seg_1923" s="T1960">refl</ta>
            <ta e="T1962" id="Seg_1924" s="T1961">n</ta>
            <ta e="T1963" id="Seg_1925" s="T1962">v</ta>
            <ta e="T1964" id="Seg_1926" s="T1963">conj</ta>
            <ta e="T1965" id="Seg_1927" s="T1964">n</ta>
            <ta e="T1967" id="Seg_1928" s="T1966">v</ta>
            <ta e="T1968" id="Seg_1929" s="T1967">adv</ta>
            <ta e="T1969" id="Seg_1930" s="T1968">ptcl</ta>
            <ta e="T1970" id="Seg_1931" s="T1969">v</ta>
            <ta e="T1971" id="Seg_1932" s="T1970">dempro</ta>
            <ta e="T1972" id="Seg_1933" s="T1971">conj</ta>
            <ta e="T1974" id="Seg_1934" s="T1973">adv</ta>
            <ta e="T1975" id="Seg_1935" s="T1974">dempro</ta>
            <ta e="T1976" id="Seg_1936" s="T1975">quant</ta>
            <ta e="T1977" id="Seg_1937" s="T1976">n</ta>
            <ta e="T1978" id="Seg_1938" s="T1977">v</ta>
            <ta e="T1979" id="Seg_1939" s="T1978">n</ta>
            <ta e="T1981" id="Seg_1940" s="T1980">v</ta>
            <ta e="T1982" id="Seg_1941" s="T1981">quant</ta>
            <ta e="T1983" id="Seg_1942" s="T1982">v</ta>
            <ta e="T1984" id="Seg_1943" s="T1983">dempro</ta>
            <ta e="T1985" id="Seg_1944" s="T1984">adv</ta>
            <ta e="T1986" id="Seg_1945" s="T1985">v</ta>
            <ta e="T1987" id="Seg_1946" s="T1986">quant</ta>
            <ta e="T1988" id="Seg_1947" s="T1987">n</ta>
            <ta e="T1989" id="Seg_1948" s="T1988">v</ta>
            <ta e="T1990" id="Seg_1949" s="T1989">n</ta>
            <ta e="T1991" id="Seg_1950" s="T1990">v</ta>
            <ta e="T1992" id="Seg_1951" s="T1991">v</ta>
            <ta e="T1993" id="Seg_1952" s="T1992">adv</ta>
            <ta e="T1994" id="Seg_1953" s="T1993">v</ta>
            <ta e="T1995" id="Seg_1954" s="T1994">n</ta>
            <ta e="T1996" id="Seg_1955" s="T1995">n</ta>
            <ta e="T1997" id="Seg_1956" s="T1996">v</ta>
            <ta e="T1998" id="Seg_1957" s="T1997">ptcl</ta>
            <ta e="T1999" id="Seg_1958" s="T1998">adv</ta>
            <ta e="T2000" id="Seg_1959" s="T1999">pers</ta>
            <ta e="T2001" id="Seg_1960" s="T2000">v</ta>
            <ta e="T2002" id="Seg_1961" s="T2001">adv</ta>
            <ta e="T2003" id="Seg_1962" s="T2002">n</ta>
            <ta e="T2004" id="Seg_1963" s="T2003">ptcl</ta>
            <ta e="T2005" id="Seg_1964" s="T2004">v</ta>
            <ta e="T2006" id="Seg_1965" s="T2005">adv</ta>
            <ta e="T2007" id="Seg_1966" s="T2006">dempro</ta>
            <ta e="T2008" id="Seg_1967" s="T2007">v</ta>
            <ta e="T2009" id="Seg_1968" s="T2008">ptcl</ta>
            <ta e="T2010" id="Seg_1969" s="T2009">v</ta>
            <ta e="T2011" id="Seg_1970" s="T2010">v</ta>
            <ta e="T2012" id="Seg_1971" s="T2011">v</ta>
            <ta e="T2013" id="Seg_1972" s="T2012">adv</ta>
            <ta e="T2014" id="Seg_1973" s="T2013">n</ta>
            <ta e="T2015" id="Seg_1974" s="T2014">v</ta>
            <ta e="T2016" id="Seg_1975" s="T2015">que</ta>
            <ta e="T2018" id="Seg_1976" s="T2017">pers</ta>
            <ta e="T2019" id="Seg_1977" s="T2018">v</ta>
            <ta e="T2020" id="Seg_1978" s="T2019">pers</ta>
            <ta e="T2021" id="Seg_1979" s="T2020">n</ta>
            <ta e="T2024" id="Seg_1980" s="T2023">pers</ta>
            <ta e="T2025" id="Seg_1981" s="T2024">n</ta>
            <ta e="T2026" id="Seg_1982" s="T2025">adv</ta>
            <ta e="T2027" id="Seg_1983" s="T2026">dempro</ta>
            <ta e="T2029" id="Seg_1984" s="T2028">v</ta>
            <ta e="T2030" id="Seg_1985" s="T2029">ptcl</ta>
            <ta e="T2031" id="Seg_1986" s="T2030">n</ta>
            <ta e="T2032" id="Seg_1987" s="T2031">v</ta>
            <ta e="T2033" id="Seg_1988" s="T2032">adv</ta>
            <ta e="T2034" id="Seg_1989" s="T2033">dempro</ta>
            <ta e="T2035" id="Seg_1990" s="T2034">v</ta>
            <ta e="T2036" id="Seg_1991" s="T2035">dempro</ta>
            <ta e="T2037" id="Seg_1992" s="T2036">quant</ta>
            <ta e="T2038" id="Seg_1993" s="T2037">n</ta>
            <ta e="T2039" id="Seg_1994" s="T2038">adv</ta>
            <ta e="T2040" id="Seg_1995" s="T2039">quant</ta>
            <ta e="T2041" id="Seg_1996" s="T2040">v</ta>
            <ta e="T2042" id="Seg_1997" s="T2041">adv</ta>
            <ta e="T2043" id="Seg_1998" s="T2042">v</ta>
            <ta e="T2044" id="Seg_1999" s="T2043">ptcl</ta>
            <ta e="T2045" id="Seg_2000" s="T2044">n</ta>
            <ta e="T2046" id="Seg_2001" s="T2045">ptcl</ta>
            <ta e="T2047" id="Seg_2002" s="T2046">num</ta>
            <ta e="T2048" id="Seg_2003" s="T2047">n</ta>
            <ta e="T2049" id="Seg_2004" s="T2048">v</ta>
            <ta e="T2050" id="Seg_2005" s="T2049">dempro</ta>
            <ta e="T2051" id="Seg_2006" s="T2050">n</ta>
            <ta e="T2052" id="Seg_2007" s="T2051">v</ta>
            <ta e="T2053" id="Seg_2008" s="T2052">conj</ta>
            <ta e="T2054" id="Seg_2009" s="T2053">dempro</ta>
            <ta e="T2055" id="Seg_2010" s="T2054">adv</ta>
            <ta e="T2056" id="Seg_2011" s="T2055">v</ta>
            <ta e="T2057" id="Seg_2012" s="T2056">adv</ta>
            <ta e="T2058" id="Seg_2013" s="T2057">n</ta>
            <ta e="T2059" id="Seg_2014" s="T2058">quant</ta>
            <ta e="T2060" id="Seg_2015" s="T2059">v</ta>
            <ta e="T2061" id="Seg_2016" s="T2060">conj</ta>
            <ta e="T2062" id="Seg_2017" s="T2061">dempro</ta>
            <ta e="T2063" id="Seg_2018" s="T2062">n</ta>
            <ta e="T2064" id="Seg_2019" s="T2063">adv</ta>
            <ta e="T2065" id="Seg_2020" s="T2064">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1900" id="Seg_2021" s="T1899">np.h:E</ta>
            <ta e="T1902" id="Seg_2022" s="T1901">np.h:P</ta>
            <ta e="T1904" id="Seg_2023" s="T1903">0.3.h:A</ta>
            <ta e="T1905" id="Seg_2024" s="T1904">adv:Time</ta>
            <ta e="T1906" id="Seg_2025" s="T1905">np.h:Poss</ta>
            <ta e="T1907" id="Seg_2026" s="T1906">np.h:Th</ta>
            <ta e="T1908" id="Seg_2027" s="T1907">0.3.h:A</ta>
            <ta e="T1909" id="Seg_2028" s="T1908">pro.h:P</ta>
            <ta e="T1911" id="Seg_2029" s="T1910">0.3.h:A</ta>
            <ta e="T1912" id="Seg_2030" s="T1911">adv:L</ta>
            <ta e="T1913" id="Seg_2031" s="T1912">np:E</ta>
            <ta e="T1915" id="Seg_2032" s="T1914">adv:Time</ta>
            <ta e="T1916" id="Seg_2033" s="T1915">pro.h:A</ta>
            <ta e="T1918" id="Seg_2034" s="T1917">np:L</ta>
            <ta e="T1919" id="Seg_2035" s="T1918">np.h:R</ta>
            <ta e="T1920" id="Seg_2036" s="T1919">0.3.h:A</ta>
            <ta e="T1921" id="Seg_2037" s="T1920">np:A</ta>
            <ta e="T1924" id="Seg_2038" s="T1923">pro:So</ta>
            <ta e="T1926" id="Seg_2039" s="T1925">np:Th</ta>
            <ta e="T1927" id="Seg_2040" s="T1926">pro.h:R</ta>
            <ta e="T1928" id="Seg_2041" s="T1927">adv:Time</ta>
            <ta e="T1929" id="Seg_2042" s="T1928">np.h:A</ta>
            <ta e="T1930" id="Seg_2043" s="T1929">np.h:A</ta>
            <ta e="T1932" id="Seg_2044" s="T1931">0.2.h:A</ta>
            <ta e="T1933" id="Seg_2045" s="T1932">pro.h:Th</ta>
            <ta e="T1936" id="Seg_2046" s="T1935">pro.h:A</ta>
            <ta e="T1939" id="Seg_2047" s="T1938">pro.h:A</ta>
            <ta e="T1943" id="Seg_2048" s="T1942">np.h:Th</ta>
            <ta e="T1946" id="Seg_2049" s="T1945">adv:Time</ta>
            <ta e="T1947" id="Seg_2050" s="T1946">pro.h:A</ta>
            <ta e="T1949" id="Seg_2051" s="T1948">pro.h:A</ta>
            <ta e="T1952" id="Seg_2052" s="T1951">pro.h:A</ta>
            <ta e="T1953" id="Seg_2053" s="T1952">np:P</ta>
            <ta e="T1956" id="Seg_2054" s="T1955">0.3.h:A</ta>
            <ta e="T1957" id="Seg_2055" s="T1956">np:P</ta>
            <ta e="T1958" id="Seg_2056" s="T1957">0.3.h:E</ta>
            <ta e="T1960" id="Seg_2057" s="T1959">np.h:A</ta>
            <ta e="T1962" id="Seg_2058" s="T1961">np.h:Com</ta>
            <ta e="T1965" id="Seg_2059" s="T1964">np:P</ta>
            <ta e="T1967" id="Seg_2060" s="T1966">0.3.h:A</ta>
            <ta e="T1968" id="Seg_2061" s="T1967">adv:Time</ta>
            <ta e="T1970" id="Seg_2062" s="T1969">0.3.h:A</ta>
            <ta e="T1971" id="Seg_2063" s="T1970">pro.h:Com</ta>
            <ta e="T1974" id="Seg_2064" s="T1973">adv:Time</ta>
            <ta e="T1975" id="Seg_2065" s="T1974">pro.h:A</ta>
            <ta e="T1977" id="Seg_2066" s="T1976">np.h:Th</ta>
            <ta e="T1979" id="Seg_2067" s="T1978">np.h:A</ta>
            <ta e="T1983" id="Seg_2068" s="T1982">0.3.h:E</ta>
            <ta e="T1984" id="Seg_2069" s="T1983">pro.h:A</ta>
            <ta e="T1985" id="Seg_2070" s="T1984">adv:Time</ta>
            <ta e="T1988" id="Seg_2071" s="T1987">np:Th</ta>
            <ta e="T1989" id="Seg_2072" s="T1988">0.3.h:A</ta>
            <ta e="T1990" id="Seg_2073" s="T1989">np:Ins</ta>
            <ta e="T1991" id="Seg_2074" s="T1990">0.3.h:A</ta>
            <ta e="T1992" id="Seg_2075" s="T1991">0.3.h:A</ta>
            <ta e="T1993" id="Seg_2076" s="T1992">adv:Time</ta>
            <ta e="T1994" id="Seg_2077" s="T1993">0.3.h:A</ta>
            <ta e="T1997" id="Seg_2078" s="T1996">0.2.h:A</ta>
            <ta e="T2000" id="Seg_2079" s="T1999">pro.h:P</ta>
            <ta e="T2001" id="Seg_2080" s="T2000">0.1.h:A</ta>
            <ta e="T2002" id="Seg_2081" s="T2001">adv:Time</ta>
            <ta e="T2003" id="Seg_2082" s="T2002">np:Th</ta>
            <ta e="T2005" id="Seg_2083" s="T2004">0.3.h:A</ta>
            <ta e="T2006" id="Seg_2084" s="T2005">adv:Time</ta>
            <ta e="T2007" id="Seg_2085" s="T2006">pro.h:A</ta>
            <ta e="T2010" id="Seg_2086" s="T2009">0.3.h:E</ta>
            <ta e="T2011" id="Seg_2087" s="T2010">0.3.h:A</ta>
            <ta e="T2012" id="Seg_2088" s="T2011">0.3.h:A</ta>
            <ta e="T2013" id="Seg_2089" s="T2012">adv:Time</ta>
            <ta e="T2014" id="Seg_2090" s="T2013">np.h:A</ta>
            <ta e="T2018" id="Seg_2091" s="T2017">pro.h:A</ta>
            <ta e="T2020" id="Seg_2092" s="T2019">pro.h:Poss</ta>
            <ta e="T2024" id="Seg_2093" s="T2023">pro.h:Poss</ta>
            <ta e="T2026" id="Seg_2094" s="T2025">adv:Time</ta>
            <ta e="T2027" id="Seg_2095" s="T2026">pro.h:A</ta>
            <ta e="T2031" id="Seg_2096" s="T2030">np:Th</ta>
            <ta e="T2033" id="Seg_2097" s="T2032">adv:Time</ta>
            <ta e="T2034" id="Seg_2098" s="T2033">pro.h:Th</ta>
            <ta e="T2035" id="Seg_2099" s="T2034">0.3.h:A</ta>
            <ta e="T2036" id="Seg_2100" s="T2035">pro.h:A</ta>
            <ta e="T2038" id="Seg_2101" s="T2037">np:P</ta>
            <ta e="T2042" id="Seg_2102" s="T2041">adv:Time</ta>
            <ta e="T2043" id="Seg_2103" s="T2042">0.3.h:A</ta>
            <ta e="T2045" id="Seg_2104" s="T2044">np:Th</ta>
            <ta e="T2050" id="Seg_2105" s="T2049">pro.h:A</ta>
            <ta e="T2051" id="Seg_2106" s="T2050">np:G</ta>
            <ta e="T2054" id="Seg_2107" s="T2053">pro.h:P</ta>
            <ta e="T2055" id="Seg_2108" s="T2054">adv:L</ta>
            <ta e="T2057" id="Seg_2109" s="T2056">adv:Time</ta>
            <ta e="T2058" id="Seg_2110" s="T2057">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1899" id="Seg_2111" s="T1898">v:pred</ta>
            <ta e="T1900" id="Seg_2112" s="T1899">np.h:S</ta>
            <ta e="T1902" id="Seg_2113" s="T1901">np.h:O</ta>
            <ta e="T1904" id="Seg_2114" s="T1903">v:pred 0.3.h:S</ta>
            <ta e="T1907" id="Seg_2115" s="T1906">np.h:O</ta>
            <ta e="T1908" id="Seg_2116" s="T1907">v:pred 0.3.h:S</ta>
            <ta e="T1909" id="Seg_2117" s="T1908">pro.h:O</ta>
            <ta e="T1910" id="Seg_2118" s="T1909">ptcl.neg</ta>
            <ta e="T1911" id="Seg_2119" s="T1910">v:pred 0.3.h:S</ta>
            <ta e="T1913" id="Seg_2120" s="T1912">np:S</ta>
            <ta e="T1914" id="Seg_2121" s="T1913">v:pred</ta>
            <ta e="T1916" id="Seg_2122" s="T1915">pro.h:S</ta>
            <ta e="T1917" id="Seg_2123" s="T1916">v:pred</ta>
            <ta e="T1920" id="Seg_2124" s="T1919">v:pred 0.3.h:S</ta>
            <ta e="T1921" id="Seg_2125" s="T1920">np:S</ta>
            <ta e="T1925" id="Seg_2126" s="T1924">v:pred</ta>
            <ta e="T1926" id="Seg_2127" s="T1925">np:O</ta>
            <ta e="T1929" id="Seg_2128" s="T1928">np.h:S</ta>
            <ta e="T1930" id="Seg_2129" s="T1929">np.h:S</ta>
            <ta e="T1931" id="Seg_2130" s="T1930">v:pred</ta>
            <ta e="T1932" id="Seg_2131" s="T1931">v:pred 0.2.h:S</ta>
            <ta e="T1933" id="Seg_2132" s="T1932">pro.h:S</ta>
            <ta e="T1934" id="Seg_2133" s="T1933">adj:pred</ta>
            <ta e="T1937" id="Seg_2134" s="T1936">ptcl:pred</ta>
            <ta e="T1939" id="Seg_2135" s="T1938">pro.h:S</ta>
            <ta e="T1941" id="Seg_2136" s="T1940">v:pred</ta>
            <ta e="T1943" id="Seg_2137" s="T1942">np.h:S</ta>
            <ta e="T1944" id="Seg_2138" s="T1943">adj:pred</ta>
            <ta e="T1947" id="Seg_2139" s="T1946">pro.h:S</ta>
            <ta e="T1948" id="Seg_2140" s="T1947">v:pred</ta>
            <ta e="T1949" id="Seg_2141" s="T1948">pro.h:S</ta>
            <ta e="T1950" id="Seg_2142" s="T1949">v:pred</ta>
            <ta e="T1952" id="Seg_2143" s="T1951">pro.h:S</ta>
            <ta e="T1953" id="Seg_2144" s="T1952">np:O</ta>
            <ta e="T1954" id="Seg_2145" s="T1953">v:pred</ta>
            <ta e="T1956" id="Seg_2146" s="T1955">v:pred 0.3.h:S</ta>
            <ta e="T1957" id="Seg_2147" s="T1956">np:O</ta>
            <ta e="T1958" id="Seg_2148" s="T1957">v:pred 0.3.h:S</ta>
            <ta e="T1960" id="Seg_2149" s="T1959">np.h:S</ta>
            <ta e="T1963" id="Seg_2150" s="T1962">v:pred</ta>
            <ta e="T1965" id="Seg_2151" s="T1964">np:O</ta>
            <ta e="T1967" id="Seg_2152" s="T1966">v:pred 0.3.h:S</ta>
            <ta e="T1969" id="Seg_2153" s="T1968">ptcl.neg</ta>
            <ta e="T1970" id="Seg_2154" s="T1969">v:pred 0.3.h:S</ta>
            <ta e="T1975" id="Seg_2155" s="T1974">pro.h:S</ta>
            <ta e="T1977" id="Seg_2156" s="T1976">np.h:O</ta>
            <ta e="T1978" id="Seg_2157" s="T1977">v:pred</ta>
            <ta e="T1979" id="Seg_2158" s="T1978">np.h:S</ta>
            <ta e="T1981" id="Seg_2159" s="T1980">v:pred</ta>
            <ta e="T1983" id="Seg_2160" s="T1982">v:pred 0.3.h:S</ta>
            <ta e="T1984" id="Seg_2161" s="T1983">pro.h:S</ta>
            <ta e="T1986" id="Seg_2162" s="T1985">v:pred</ta>
            <ta e="T1988" id="Seg_2163" s="T1987">np:O</ta>
            <ta e="T1989" id="Seg_2164" s="T1988">v:pred 0.3.h:S</ta>
            <ta e="T1991" id="Seg_2165" s="T1990">v:pred 0.3.h:S</ta>
            <ta e="T1992" id="Seg_2166" s="T1991">v:pred 0.3.h:S</ta>
            <ta e="T1994" id="Seg_2167" s="T1993">v:pred 0.3.h:S</ta>
            <ta e="T1997" id="Seg_2168" s="T1996">v:pred 0.2.h:S</ta>
            <ta e="T2000" id="Seg_2169" s="T1999">pro.h:O</ta>
            <ta e="T2001" id="Seg_2170" s="T2000">v:pred 0.1.h:S</ta>
            <ta e="T2003" id="Seg_2171" s="T2002">np:O</ta>
            <ta e="T2005" id="Seg_2172" s="T2004">v:pred 0.3.h:S</ta>
            <ta e="T2007" id="Seg_2173" s="T2006">pro.h:S</ta>
            <ta e="T2008" id="Seg_2174" s="T2007">v:pred</ta>
            <ta e="T2009" id="Seg_2175" s="T2008">ptcl:pred</ta>
            <ta e="T2011" id="Seg_2176" s="T2010">v:pred 0.3.h:S</ta>
            <ta e="T2012" id="Seg_2177" s="T2011">v:pred 0.3.h:S</ta>
            <ta e="T2014" id="Seg_2178" s="T2013">np.h:S</ta>
            <ta e="T2015" id="Seg_2179" s="T2014">v:pred</ta>
            <ta e="T2018" id="Seg_2180" s="T2017">pro.h:S</ta>
            <ta e="T2019" id="Seg_2181" s="T2018">v:pred</ta>
            <ta e="T2027" id="Seg_2182" s="T2026">pro.h:S</ta>
            <ta e="T2029" id="Seg_2183" s="T2028">v:pred</ta>
            <ta e="T2030" id="Seg_2184" s="T2029">ptcl:pred</ta>
            <ta e="T2031" id="Seg_2185" s="T2030">np:O</ta>
            <ta e="T2034" id="Seg_2186" s="T2033">pro.h:O</ta>
            <ta e="T2035" id="Seg_2187" s="T2034">v:pred 0.3.h:S</ta>
            <ta e="T2036" id="Seg_2188" s="T2035">pro.h:S</ta>
            <ta e="T2038" id="Seg_2189" s="T2037">np:O</ta>
            <ta e="T2041" id="Seg_2190" s="T2040">v:pred</ta>
            <ta e="T2043" id="Seg_2191" s="T2042">v:pred 0.3.h:S</ta>
            <ta e="T2044" id="Seg_2192" s="T2043">ptcl:pred</ta>
            <ta e="T2045" id="Seg_2193" s="T2044">np:O</ta>
            <ta e="T2050" id="Seg_2194" s="T2049">pro.h:S</ta>
            <ta e="T2052" id="Seg_2195" s="T2051">v:pred</ta>
            <ta e="T2054" id="Seg_2196" s="T2053">pro.h:S</ta>
            <ta e="T2056" id="Seg_2197" s="T2055">v:pred</ta>
            <ta e="T2058" id="Seg_2198" s="T2057">np.h:S</ta>
            <ta e="T2060" id="Seg_2199" s="T2059">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1906" id="Seg_2200" s="T1905">TURK:cult</ta>
            <ta e="T1937" id="Seg_2201" s="T1936">RUS:gram</ta>
            <ta e="T1940" id="Seg_2202" s="T1939">TURK:disc</ta>
            <ta e="T1959" id="Seg_2203" s="T1958">RUS:gram</ta>
            <ta e="T1960" id="Seg_2204" s="T1959">TURK:cult</ta>
            <ta e="T1964" id="Seg_2205" s="T1963">RUS:gram</ta>
            <ta e="T1972" id="Seg_2206" s="T1971">RUS:gram</ta>
            <ta e="T1982" id="Seg_2207" s="T1981">TURK:disc</ta>
            <ta e="T1988" id="Seg_2208" s="T1987">RUS:cult</ta>
            <ta e="T1990" id="Seg_2209" s="T1989">RUS:cult</ta>
            <ta e="T1996" id="Seg_2210" s="T1995">RUS:cult</ta>
            <ta e="T1998" id="Seg_2211" s="T1997">RUS:gram</ta>
            <ta e="T1999" id="Seg_2212" s="T1998">RUS:mod</ta>
            <ta e="T2004" id="Seg_2213" s="T2003">TURK:disc</ta>
            <ta e="T2009" id="Seg_2214" s="T2008">RUS:gram</ta>
            <ta e="T2030" id="Seg_2215" s="T2029">RUS:mod</ta>
            <ta e="T2031" id="Seg_2216" s="T2030">RUS:cult</ta>
            <ta e="T2037" id="Seg_2217" s="T2036">TURK:disc</ta>
            <ta e="T2044" id="Seg_2218" s="T2043">RUS:mod</ta>
            <ta e="T2046" id="Seg_2219" s="T2045">RUS:mod</ta>
            <ta e="T2053" id="Seg_2220" s="T2052">RUS:gram</ta>
            <ta e="T2059" id="Seg_2221" s="T2058">TURK:disc</ta>
            <ta e="T2061" id="Seg_2222" s="T2060">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T1917" id="Seg_2223" s="T1916">RUS:calq</ta>
            <ta e="T1920" id="Seg_2224" s="T1919">RUS:calq</ta>
            <ta e="T1925" id="Seg_2225" s="T1924">RUS:calq</ta>
            <ta e="T2023" id="Seg_2226" s="T2022">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1900" id="Seg_2227" s="T1898">Жил змей.</ta>
            <ta e="T1904" id="Seg_2228" s="T1900">Он съел очень много детей.</ta>
            <ta e="T1908" id="Seg_2229" s="T1904">Потом он унёс дочь царя.</ta>
            <ta e="T1911" id="Seg_2230" s="T1908">Он её не съел.</ta>
            <ta e="T1914" id="Seg_2231" s="T1911">Там была собака.</ta>
            <ta e="T1920" id="Seg_2232" s="T1914">Тогда она напишет на бумаге, отправит своему отцу.</ta>
            <ta e="T1927" id="Seg_2233" s="T1920">Собака принесёт ей от них [от родителей] письмо.</ta>
            <ta e="T1935" id="Seg_2234" s="T1927">Потом мать и отец говорят: «Спроси [змея], кто сильнее его».</ta>
            <ta e="T1941" id="Seg_2235" s="T1935">Она стала спрашивать, он сказал: </ta>
            <ta e="T1945" id="Seg_2236" s="T1941">«Никита Кожемяка сильнее меня».</ta>
            <ta e="T1948" id="Seg_2237" s="T1945">Потом она отправила [письмо родителям].</ta>
            <ta e="T1951" id="Seg_2238" s="T1948">Они пошли вдвоём.</ta>
            <ta e="T1954" id="Seg_2239" s="T1951">Он [Никита] делал кожи.</ta>
            <ta e="T1957" id="Seg_2240" s="T1954">Много делал.</ta>
            <ta e="T1963" id="Seg_2241" s="T1957">Он испугался, что [сам] царь со своей женой пришёл.</ta>
            <ta e="T1967" id="Seg_2242" s="T1963">И кожи разорвал.</ta>
            <ta e="T1971" id="Seg_2243" s="T1967">Потом он не пошёл [с] ними.</ta>
            <ta e="T1978" id="Seg_2244" s="T1971">А он [царь] тогда собрал много детей.</ta>
            <ta e="T1983" id="Seg_2245" s="T1978">Дети все пришли [к нему], плача.</ta>
            <ta e="T1986" id="Seg_2246" s="T1983">Тогда он пошёл.</ta>
            <ta e="T1989" id="Seg_2247" s="T1986">Он взял много кудели.</ta>
            <ta e="T1991" id="Seg_2248" s="T1989">Обмазал смолой.</ta>
            <ta e="T1992" id="Seg_2249" s="T1991">Обернулся [ею].</ta>
            <ta e="T1994" id="Seg_2250" s="T1992">Потом пришёл.</ta>
            <ta e="T1996" id="Seg_2251" s="T1994">Змей из берлоги [не выходит].</ta>
            <ta e="T2001" id="Seg_2252" s="T1996">«Выходи, всё равно тебя убью».</ta>
            <ta e="T2005" id="Seg_2253" s="T2001">Потом разобрал брёвна.</ta>
            <ta e="T2008" id="Seg_2254" s="T2005">Потом тот вышел.</ta>
            <ta e="T2010" id="Seg_2255" s="T2008">Стали они бороться.</ta>
            <ta e="T2012" id="Seg_2256" s="T2010">Боролись, боролись.</ta>
            <ta e="T2015" id="Seg_2257" s="T2012">Потом змей говорит:</ta>
            <ta e="T2019" id="Seg_2258" s="T2015">«Что мы боремся?</ta>
            <ta e="T2025" id="Seg_2259" s="T2019">[Пусть] у тебя половина [мира] будет и у меня половина».</ta>
            <ta e="T2029" id="Seg_2260" s="T2025">Тогда он говорит:</ta>
            <ta e="T2032" id="Seg_2261" s="T2029">«Надо границу проложить».</ta>
            <ta e="T2035" id="Seg_2262" s="T2032">Потом запряг его [змея].</ta>
            <ta e="T2041" id="Seg_2263" s="T2035">Он землю очень глубоко вскопал.</ta>
            <ta e="T2049" id="Seg_2264" s="T2041">Потом говорит: «Надо воду тоже на две половины разделить».</ta>
            <ta e="T2056" id="Seg_2265" s="T2049">Он [змей] вошёл в воду и там умер.</ta>
            <ta e="T2060" id="Seg_2266" s="T2056">Потом люди везде пахали [вокруг].</ta>
            <ta e="T2064" id="Seg_2267" s="T2060">А это место всегда [оставалось].</ta>
            <ta e="T2065" id="Seg_2268" s="T2064">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1900" id="Seg_2269" s="T1898">A serpent lived.</ta>
            <ta e="T1904" id="Seg_2270" s="T1900">He ate very many children.</ta>
            <ta e="T1908" id="Seg_2271" s="T1904">Then he took the king’s daughter.</ta>
            <ta e="T1911" id="Seg_2272" s="T1908">It did not eat her.</ta>
            <ta e="T1914" id="Seg_2273" s="T1911">There was a dog.</ta>
            <ta e="T1920" id="Seg_2274" s="T1914">Then she will write on paper, will send to her father.</ta>
            <ta e="T1927" id="Seg_2275" s="T1920">The dog will bring her a letter from them.</ta>
            <ta e="T1935" id="Seg_2276" s="T1927">Then her mother, her father say: "Ask [the serpent], who is stronger than it."</ta>
            <ta e="T1941" id="Seg_2277" s="T1935">She starts to ask, it tells:</ta>
            <ta e="T1945" id="Seg_2278" s="T1941">"Nikita Kozhemyaka is stronger than me."</ta>
            <ta e="T1948" id="Seg_2279" s="T1945">Then she sent [a letter about this to her parents].</ta>
            <ta e="T1951" id="Seg_2280" s="T1948">They went, two of them.</ta>
            <ta e="T1954" id="Seg_2281" s="T1951">He [Nikita] made skins.</ta>
            <ta e="T1957" id="Seg_2282" s="T1954">He was making a lot.</ta>
            <ta e="T1963" id="Seg_2283" s="T1957">He got scared that the king [himself] was coming with his wife.</ta>
            <ta e="T1967" id="Seg_2284" s="T1963">And he tore apart skins.</ta>
            <ta e="T1971" id="Seg_2285" s="T1967">Then he did not go [with] them.</ta>
            <ta e="T1978" id="Seg_2286" s="T1971">But then he [the king] gathered many children.</ta>
            <ta e="T1983" id="Seg_2287" s="T1978">Children all went [to him], crying.</ta>
            <ta e="T1986" id="Seg_2288" s="T1983">Then he went.</ta>
            <ta e="T1989" id="Seg_2289" s="T1986">He took a lot of tow.</ta>
            <ta e="T1991" id="Seg_2290" s="T1989">Smeared with resin.</ta>
            <ta e="T1992" id="Seg_2291" s="T1991">He wrapped [it around himself].</ta>
            <ta e="T1994" id="Seg_2292" s="T1992">Then he came.</ta>
            <ta e="T1996" id="Seg_2293" s="T1994">The serpent [doesn't come] out of its cave [and covered it with logs].</ta>
            <ta e="T2001" id="Seg_2294" s="T1996">“Come out, otherwise I will kill you anyway!”</ta>
            <ta e="T2005" id="Seg_2295" s="T2001">Then he took away all the logs.</ta>
            <ta e="T2008" id="Seg_2296" s="T2005">Then he came out.</ta>
            <ta e="T2010" id="Seg_2297" s="T2008">They started to fight.</ta>
            <ta e="T2012" id="Seg_2298" s="T2010">They fought, they fought.</ta>
            <ta e="T2015" id="Seg_2299" s="T2012">Then the snake says:</ta>
            <ta e="T2019" id="Seg_2300" s="T2015">"Why do we fight?</ta>
            <ta e="T2025" id="Seg_2301" s="T2019">You will have a half of the world, and I [will have] a half [because we are the strongest]."</ta>
            <ta e="T2029" id="Seg_2302" s="T2025">Then he said:</ta>
            <ta e="T2032" id="Seg_2303" s="T2029">“We should set a border.”</ta>
            <ta e="T2035" id="Seg_2304" s="T2032">Then he harnessed it.</ta>
            <ta e="T2041" id="Seg_2305" s="T2035">He deeply dug the ground [= dug a long furrow].</ta>
            <ta e="T2049" id="Seg_2306" s="T2041">Then he says: "We should also divide the water into two parts."</ta>
            <ta e="T2056" id="Seg_2307" s="T2049">It went into the water and died there.</ta>
            <ta e="T2060" id="Seg_2308" s="T2056">Then people ploughed [around].</ta>
            <ta e="T2064" id="Seg_2309" s="T2060">But this place always [remained there].</ta>
            <ta e="T2065" id="Seg_2310" s="T2064">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1900" id="Seg_2311" s="T1898">Es lebte eine Schlange.</ta>
            <ta e="T1904" id="Seg_2312" s="T1900">Sie fraß sehr viele Kinder</ta>
            <ta e="T1908" id="Seg_2313" s="T1904">Dann nahm sie die Tochter des Zaren.</ta>
            <ta e="T1911" id="Seg_2314" s="T1908">Sie fraß sie nicht.</ta>
            <ta e="T1914" id="Seg_2315" s="T1911">Es war ein Hund.</ta>
            <ta e="T1920" id="Seg_2316" s="T1914">Dann will sie auf Papier schreiben, will zu ihrem Vater schicken. </ta>
            <ta e="T1927" id="Seg_2317" s="T1920">Der Hund wird den Brief von ihnen an sie bringen.</ta>
            <ta e="T1935" id="Seg_2318" s="T1927">Dann sagen ihre Mutter, ihr Vater: „Frag [der Schlange], wer stärker als sie ist.“</ta>
            <ta e="T1941" id="Seg_2319" s="T1935">Sie fängt an zu fragen, sie erzählt:</ta>
            <ta e="T1945" id="Seg_2320" s="T1941">„Nikita Kozhemyaka ist stärker als ich.“</ta>
            <ta e="T1948" id="Seg_2321" s="T1945">Dann schickte sie [darüber einen Brief an ihre Eltern].</ta>
            <ta e="T1951" id="Seg_2322" s="T1948">Sie gingen, zwei von ihnen.</ta>
            <ta e="T1954" id="Seg_2323" s="T1951">Er [Nikita] machte Häute.</ta>
            <ta e="T1957" id="Seg_2324" s="T1954">Er machte viele.</ta>
            <ta e="T1963" id="Seg_2325" s="T1957">Er bekam Angst davor, dass der König [selbst] kommt mit seiner Frau.</ta>
            <ta e="T1967" id="Seg_2326" s="T1963">Und er riss Häute auseinander.</ta>
            <ta e="T1971" id="Seg_2327" s="T1967">Dann ging er nicht [mit] ihnen.</ta>
            <ta e="T1978" id="Seg_2328" s="T1971">Aber dann sammelte er [der König] viele Kinder. </ta>
            <ta e="T1983" id="Seg_2329" s="T1978">Kinder gingen alle [zu ihm], weinend.</ta>
            <ta e="T1986" id="Seg_2330" s="T1983">Dann ging er.</ta>
            <ta e="T1989" id="Seg_2331" s="T1986">Er nahm viel Tau.</ta>
            <ta e="T1991" id="Seg_2332" s="T1989">Mit Harz eingeschmiert.</ta>
            <ta e="T1992" id="Seg_2333" s="T1991">Er wickelte [es um sich].</ta>
            <ta e="T1994" id="Seg_2334" s="T1992">Dann kam er.</ta>
            <ta e="T1996" id="Seg_2335" s="T1994">Die Schlange [kommt nicht][und bedeckte sie mit Baumstämmen].</ta>
            <ta e="T2001" id="Seg_2336" s="T1996">„Komm heraus, sonst werde ich dich sowieso töten!“</ta>
            <ta e="T2005" id="Seg_2337" s="T2001">Dann nahm er alle Scheite weg.</ta>
            <ta e="T2008" id="Seg_2338" s="T2005">Dann kam er heraus</ta>
            <ta e="T2010" id="Seg_2339" s="T2008">Sie fingen an zu kämpfen</ta>
            <ta e="T2012" id="Seg_2340" s="T2010">Sie kämpften, sie kämpften.</ta>
            <ta e="T2015" id="Seg_2341" s="T2012">Dann sagte die Schlange:</ta>
            <ta e="T2019" id="Seg_2342" s="T2015">„Warum kämpfen wir?</ta>
            <ta e="T2025" id="Seg_2343" s="T2019">Du werdest eine Hälfte der Welt haben, und ich [werde haben] eine Hälfte [weil wir die Stärksten sind].“</ta>
            <ta e="T2029" id="Seg_2344" s="T2025">Dann sagte er:</ta>
            <ta e="T2032" id="Seg_2345" s="T2029">„Wir sollten eine Grenze ziehen.“</ta>
            <ta e="T2035" id="Seg_2346" s="T2032">Dann spannte er sie ein.</ta>
            <ta e="T2041" id="Seg_2347" s="T2035">Er grub tief in die Erde [= grub eine lange Furche].</ta>
            <ta e="T2049" id="Seg_2348" s="T2041">Dann sagte er: „Wir sollten auch das Wasser in zwei teilen.“</ta>
            <ta e="T2056" id="Seg_2349" s="T2049">Sie ging ins Wasser und starb dort.</ta>
            <ta e="T2060" id="Seg_2350" s="T2056">Dann pflügten die Leute [ringsherum]</ta>
            <ta e="T2064" id="Seg_2351" s="T2060">Aber dieser Ort [blieb] ewig [dort].</ta>
            <ta e="T2065" id="Seg_2352" s="T2064">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1900" id="Seg_2353" s="T1898">[KlT:] Cf. pĭnzəl- ’to swallow’, Ru. глот ’all-swallowing creature’. [GVY:] A tale about Nikita Kozhemyaka, see e.g. http://www.planetaskazok.ru/rusnarskz/nikita-kojemeaka.</ta>
            <ta e="T1904" id="Seg_2354" s="T1900">Http://rasskajem.ru/nikita-kozhemyaka.</ta>
            <ta e="T1908" id="Seg_2355" s="T1904">[GVY:] koŋgən [chief-LOC] should be koŋən [chief-GEN]?</ta>
            <ta e="T1948" id="Seg_2356" s="T1945">[GVY:] öʔlüʔbi here pronounced öʔlübi (without the glottal stop).</ta>
            <ta e="T1971" id="Seg_2357" s="T1967">[AAV:] He didn't go because he got angry at them for having frightened him and having made him lose skins.</ta>
            <ta e="T1978" id="Seg_2358" s="T1971">[AAV] The king gathered orphans whose parents had been eaten by the serpent.</ta>
            <ta e="T1986" id="Seg_2359" s="T1983">[AAV] He agreed to fight the serpent.</ta>
            <ta e="T1989" id="Seg_2360" s="T1986">[KlT:] Tow Ru. кудель Est. 'takud'. [AAV] </ta>
            <ta e="T1991" id="Seg_2361" s="T1989">[KlT:] Ru. дёготь ’tar’.</ta>
            <ta e="T2010" id="Seg_2362" s="T2008">[GVY:] The verb form may be incorrect; the speaker then seems to correct herself to dʼabərozittə.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1898" />
            <conversion-tli id="T1899" />
            <conversion-tli id="T1900" />
            <conversion-tli id="T1901" />
            <conversion-tli id="T1902" />
            <conversion-tli id="T1903" />
            <conversion-tli id="T1904" />
            <conversion-tli id="T1905" />
            <conversion-tli id="T1906" />
            <conversion-tli id="T1907" />
            <conversion-tli id="T1908" />
            <conversion-tli id="T1909" />
            <conversion-tli id="T1910" />
            <conversion-tli id="T1911" />
            <conversion-tli id="T1912" />
            <conversion-tli id="T1913" />
            <conversion-tli id="T1914" />
            <conversion-tli id="T1915" />
            <conversion-tli id="T1916" />
            <conversion-tli id="T1917" />
            <conversion-tli id="T1918" />
            <conversion-tli id="T1919" />
            <conversion-tli id="T1920" />
            <conversion-tli id="T1921" />
            <conversion-tli id="T1922" />
            <conversion-tli id="T1923" />
            <conversion-tli id="T1924" />
            <conversion-tli id="T1925" />
            <conversion-tli id="T1926" />
            <conversion-tli id="T1927" />
            <conversion-tli id="T1928" />
            <conversion-tli id="T1929" />
            <conversion-tli id="T1930" />
            <conversion-tli id="T1931" />
            <conversion-tli id="T1932" />
            <conversion-tli id="T1933" />
            <conversion-tli id="T1934" />
            <conversion-tli id="T1935" />
            <conversion-tli id="T1936" />
            <conversion-tli id="T1937" />
            <conversion-tli id="T1938" />
            <conversion-tli id="T1939" />
            <conversion-tli id="T1940" />
            <conversion-tli id="T1941" />
            <conversion-tli id="T1942" />
            <conversion-tli id="T1943" />
            <conversion-tli id="T1944" />
            <conversion-tli id="T1945" />
            <conversion-tli id="T1946" />
            <conversion-tli id="T1947" />
            <conversion-tli id="T1948" />
            <conversion-tli id="T1949" />
            <conversion-tli id="T1950" />
            <conversion-tli id="T1951" />
            <conversion-tli id="T1952" />
            <conversion-tli id="T1953" />
            <conversion-tli id="T1954" />
            <conversion-tli id="T1955" />
            <conversion-tli id="T1956" />
            <conversion-tli id="T1957" />
            <conversion-tli id="T1958" />
            <conversion-tli id="T1959" />
            <conversion-tli id="T1960" />
            <conversion-tli id="T1961" />
            <conversion-tli id="T1962" />
            <conversion-tli id="T1963" />
            <conversion-tli id="T1964" />
            <conversion-tli id="T1965" />
            <conversion-tli id="T1966" />
            <conversion-tli id="T1967" />
            <conversion-tli id="T1968" />
            <conversion-tli id="T1969" />
            <conversion-tli id="T1970" />
            <conversion-tli id="T1971" />
            <conversion-tli id="T1972" />
            <conversion-tli id="T1973" />
            <conversion-tli id="T1974" />
            <conversion-tli id="T1975" />
            <conversion-tli id="T1976" />
            <conversion-tli id="T1977" />
            <conversion-tli id="T1978" />
            <conversion-tli id="T1979" />
            <conversion-tli id="T1980" />
            <conversion-tli id="T1981" />
            <conversion-tli id="T1982" />
            <conversion-tli id="T1983" />
            <conversion-tli id="T1984" />
            <conversion-tli id="T1985" />
            <conversion-tli id="T1986" />
            <conversion-tli id="T1987" />
            <conversion-tli id="T1988" />
            <conversion-tli id="T1989" />
            <conversion-tli id="T1990" />
            <conversion-tli id="T1991" />
            <conversion-tli id="T1992" />
            <conversion-tli id="T1993" />
            <conversion-tli id="T1994" />
            <conversion-tli id="T1995" />
            <conversion-tli id="T1996" />
            <conversion-tli id="T1997" />
            <conversion-tli id="T1998" />
            <conversion-tli id="T1999" />
            <conversion-tli id="T2000" />
            <conversion-tli id="T2001" />
            <conversion-tli id="T2002" />
            <conversion-tli id="T2003" />
            <conversion-tli id="T2004" />
            <conversion-tli id="T2005" />
            <conversion-tli id="T2006" />
            <conversion-tli id="T2007" />
            <conversion-tli id="T2008" />
            <conversion-tli id="T2009" />
            <conversion-tli id="T2010" />
            <conversion-tli id="T2011" />
            <conversion-tli id="T2012" />
            <conversion-tli id="T2013" />
            <conversion-tli id="T2014" />
            <conversion-tli id="T2015" />
            <conversion-tli id="T2016" />
            <conversion-tli id="T2017" />
            <conversion-tli id="T2018" />
            <conversion-tli id="T2019" />
            <conversion-tli id="T2020" />
            <conversion-tli id="T2021" />
            <conversion-tli id="T2022" />
            <conversion-tli id="T2023" />
            <conversion-tli id="T2024" />
            <conversion-tli id="T2025" />
            <conversion-tli id="T2026" />
            <conversion-tli id="T2027" />
            <conversion-tli id="T2028" />
            <conversion-tli id="T2029" />
            <conversion-tli id="T2030" />
            <conversion-tli id="T2031" />
            <conversion-tli id="T2032" />
            <conversion-tli id="T2033" />
            <conversion-tli id="T2034" />
            <conversion-tli id="T2035" />
            <conversion-tli id="T2036" />
            <conversion-tli id="T2037" />
            <conversion-tli id="T2038" />
            <conversion-tli id="T2039" />
            <conversion-tli id="T2040" />
            <conversion-tli id="T2041" />
            <conversion-tli id="T2042" />
            <conversion-tli id="T2043" />
            <conversion-tli id="T2044" />
            <conversion-tli id="T2045" />
            <conversion-tli id="T2046" />
            <conversion-tli id="T2047" />
            <conversion-tli id="T2048" />
            <conversion-tli id="T2049" />
            <conversion-tli id="T2050" />
            <conversion-tli id="T2051" />
            <conversion-tli id="T2052" />
            <conversion-tli id="T2053" />
            <conversion-tli id="T2054" />
            <conversion-tli id="T2055" />
            <conversion-tli id="T2056" />
            <conversion-tli id="T2057" />
            <conversion-tli id="T2058" />
            <conversion-tli id="T2059" />
            <conversion-tli id="T2060" />
            <conversion-tli id="T2061" />
            <conversion-tli id="T2062" />
            <conversion-tli id="T2063" />
            <conversion-tli id="T2064" />
            <conversion-tli id="T2065" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
