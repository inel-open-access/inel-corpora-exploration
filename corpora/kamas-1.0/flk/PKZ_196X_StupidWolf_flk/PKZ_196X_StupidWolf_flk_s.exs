<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID99388C30-84BE-255B-64EF-EBB461F8ACAE">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_StupidWolf_flk.wav" />
         <referenced-file url="PKZ_196X_StupidWolf_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_StupidWolf_flk\PKZ_196X_StupidWolf_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">443</ud-information>
            <ud-information attribute-name="# HIAT:w">286</ud-information>
            <ud-information attribute-name="# e">287</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">62</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1888" time="0.007" type="appl" />
         <tli id="T1889" time="2.038" type="appl" />
         <tli id="T1890" time="2.69" type="appl" />
         <tli id="T1891" time="3.341" type="appl" />
         <tli id="T1892" time="3.993" type="appl" />
         <tli id="T1893" time="4.644" type="appl" />
         <tli id="T1894" time="6.1466232318386735" />
         <tli id="T1895" time="7.189" type="appl" />
         <tli id="T1896" time="8.194" type="appl" />
         <tli id="T1897" time="9.199" type="appl" />
         <tli id="T1898" time="10.204" type="appl" />
         <tli id="T1899" time="11.209" type="appl" />
         <tli id="T1900" time="12.214" type="appl" />
         <tli id="T1901" time="13.219" type="appl" />
         <tli id="T1902" time="14.224" type="appl" />
         <tli id="T1903" time="15.539890188086712" />
         <tli id="T1904" time="16.456" type="appl" />
         <tli id="T1905" time="17.302" type="appl" />
         <tli id="T1906" time="19.77319360697777" />
         <tli id="T1907" time="20.448" type="appl" />
         <tli id="T1908" time="24.19982899303079" />
         <tli id="T1909" time="24.882" type="appl" />
         <tli id="T1910" time="25.155" type="appl" />
         <tli id="T1911" time="25.428" type="appl" />
         <tli id="T1912" time="29.16646056322581" />
         <tli id="T1913" time="30.14" type="appl" />
         <tli id="T1914" time="30.975" type="appl" />
         <tli id="T1915" time="31.81" type="appl" />
         <tli id="T1916" time="32.644" type="appl" />
         <tli id="T1917" time="33.227" type="appl" />
         <tli id="T1918" time="33.81" type="appl" />
         <tli id="T1919" time="34.394" type="appl" />
         <tli id="T1920" time="35.11975182790253" />
         <tli id="T1921" time="36.73307376077125" />
         <tli id="T1922" time="38.063" type="appl" />
         <tli id="T1923" time="39.061" type="appl" />
         <tli id="T1924" time="41.35970773354353" />
         <tli id="T1925" time="44.039" type="appl" />
         <tli id="T1926" time="46.644" type="appl" />
         <tli id="T1927" time="49.25" type="appl" />
         <tli id="T1928" time="49.923" type="appl" />
         <tli id="T1929" time="50.596" type="appl" />
         <tli id="T1930" time="51.269" type="appl" />
         <tli id="T1931" time="51.942" type="appl" />
         <tli id="T1932" time="52.571" type="appl" />
         <tli id="T1933" time="53.144" type="appl" />
         <tli id="T1934" time="53.718" type="appl" />
         <tli id="T1935" time="54.292" type="appl" />
         <tli id="T1936" time="54.865" type="appl" />
         <tli id="T1937" time="55.47294133636617" />
         <tli id="T1938" time="55.874" type="appl" />
         <tli id="T1939" time="56.31" type="appl" />
         <tli id="T1940" time="56.746" type="appl" />
         <tli id="T1941" time="57.181" type="appl" />
         <tli id="T1942" time="58.186" type="appl" />
         <tli id="T1943" time="58.93" type="appl" />
         <tli id="T1944" time="59.673" type="appl" />
         <tli id="T1945" time="60.447" type="appl" />
         <tli id="T1946" time="61.089" type="appl" />
         <tli id="T1947" time="62.23289356747725" />
         <tli id="T1948" time="62.757" type="appl" />
         <tli id="T1949" time="63.314" type="appl" />
         <tli id="T1950" time="63.87" type="appl" />
         <tli id="T1951" time="64.426" type="appl" />
         <tli id="T1952" time="64.983" type="appl" />
         <tli id="T1953" time="65.81953488930937" />
         <tli id="T1954" time="66.615" type="appl" />
         <tli id="T1955" time="67.293" type="appl" />
         <tli id="T1956" time="67.97" type="appl" />
         <tli id="T1957" time="68.389" type="appl" />
         <tli id="T1958" time="68.774" type="appl" />
         <tli id="T1959" time="69.158" type="appl" />
         <tli id="T1960" time="69.543" type="appl" />
         <tli id="T1961" time="69.927" type="appl" />
         <tli id="T1962" time="70.867" type="appl" />
         <tli id="T1963" time="71.678" type="appl" />
         <tli id="T1964" time="72.488" type="appl" />
         <tli id="T1965" time="73.299" type="appl" />
         <tli id="T1966" time="74.44614059646688" />
         <tli id="T1967" time="75.274" type="appl" />
         <tli id="T1968" time="76.109" type="appl" />
         <tli id="T1969" time="76.943" type="appl" />
         <tli id="T1970" time="77.475" type="appl" />
         <tli id="T1971" time="78.006" type="appl" />
         <tli id="T1972" time="78.538" type="appl" />
         <tli id="T1973" time="79.07" type="appl" />
         <tli id="T1974" time="79.601" type="appl" />
         <tli id="T1975" time="80.133" type="appl" />
         <tli id="T1976" time="80.759" type="appl" />
         <tli id="T1977" time="81.385" type="appl" />
         <tli id="T1978" time="82.012" type="appl" />
         <tli id="T1979" time="82.638" type="appl" />
         <tli id="T1980" time="83.264" type="appl" />
         <tli id="T1981" time="83.676" type="appl" />
         <tli id="T1982" time="84.089" type="appl" />
         <tli id="T1983" time="84.702" type="appl" />
         <tli id="T1984" time="85.315" type="appl" />
         <tli id="T1985" time="85.929" type="appl" />
         <tli id="T1986" time="86.542" type="appl" />
         <tli id="T1987" time="87.155" type="appl" />
         <tli id="T1988" time="87.768" type="appl" />
         <tli id="T0" time="87.997875" type="intp" />
         <tli id="T1989" time="88.381" type="appl" />
         <tli id="T1990" time="88.995" type="appl" />
         <tli id="T1991" time="89.608" type="appl" />
         <tli id="T1992" time="90.221" type="appl" />
         <tli id="T1993" time="90.98" type="appl" />
         <tli id="T1994" time="91.568" type="appl" />
         <tli id="T1995" time="92.155" type="appl" />
         <tli id="T1996" time="92.742" type="appl" />
         <tli id="T1997" time="93.329" type="appl" />
         <tli id="T1998" time="93.917" type="appl" />
         <tli id="T1999" time="94.66599771378434" />
         <tli id="T2000" time="95.299" type="appl" />
         <tli id="T2001" time="95.912" type="appl" />
         <tli id="T2002" time="96.524" type="appl" />
         <tli id="T2003" time="97.137" type="appl" />
         <tli id="T2004" time="98.29930537251761" />
         <tli id="T2005" time="98.987" type="appl" />
         <tli id="T2006" time="99.527" type="appl" />
         <tli id="T2007" time="100.067" type="appl" />
         <tli id="T2008" time="100.77928784783647" />
         <tli id="T2009" time="101.385" type="appl" />
         <tli id="T2010" time="102.018" type="appl" />
         <tli id="T2011" time="102.65" type="appl" />
         <tli id="T2012" time="103.282" type="appl" />
         <tli id="T2013" time="104.276" type="appl" />
         <tli id="T2014" time="105.058" type="appl" />
         <tli id="T2015" time="106.1259167327981" />
         <tli id="T2016" time="106.555" type="appl" />
         <tli id="T2017" time="107.142" type="appl" />
         <tli id="T2018" time="107.729" type="appl" />
         <tli id="T2019" time="108.316" type="appl" />
         <tli id="T2020" time="109.0125630011128" />
         <tli id="T2021" time="109.574" type="appl" />
         <tli id="T2022" time="110.112" type="appl" />
         <tli id="T2023" time="110.651" type="appl" />
         <tli id="T2024" time="111.19" type="appl" />
         <tli id="T2025" time="111.902" type="appl" />
         <tli id="T2026" time="112.418" type="appl" />
         <tli id="T2027" time="112.935" type="appl" />
         <tli id="T2028" time="113.44" type="appl" />
         <tli id="T2029" time="113.944" type="appl" />
         <tli id="T2030" time="114.90585468977372" />
         <tli id="T2031" time="115.771" type="appl" />
         <tli id="T2032" time="118.89915980460168" />
         <tli id="T2033" time="119.631" type="appl" />
         <tli id="T2034" time="120.242" type="appl" />
         <tli id="T2035" time="120.854" type="appl" />
         <tli id="T2036" time="121.465" type="appl" />
         <tli id="T2037" time="122.076" type="appl" />
         <tli id="T2038" time="122.89246491942963" />
         <tli id="T2039" time="123.631" type="appl" />
         <tli id="T2040" time="124.431" type="appl" />
         <tli id="T2041" time="125.232" type="appl" />
         <tli id="T2042" time="126.032" type="appl" />
         <tli id="T2043" time="126.833" type="appl" />
         <tli id="T2044" time="127.633" type="appl" />
         <tli id="T2045" time="128.434" type="appl" />
         <tli id="T2046" time="129.234" type="appl" />
         <tli id="T2047" time="130.035" type="appl" />
         <tli id="T2048" time="130.722" type="appl" />
         <tli id="T2049" time="131.409" type="appl" />
         <tli id="T2050" time="132.095" type="appl" />
         <tli id="T2051" time="132.782" type="appl" />
         <tli id="T2052" time="134.33238407977143" />
         <tli id="T2053" time="135.53" type="appl" />
         <tli id="T2054" time="136.811" type="appl" />
         <tli id="T2055" time="138.092" type="appl" />
         <tli id="T2056" time="140.19234267064584" />
         <tli id="T2057" time="140.772" type="appl" />
         <tli id="T2058" time="141.313" type="appl" />
         <tli id="T2059" time="141.855" type="appl" />
         <tli id="T2060" time="142.396" type="appl" />
         <tli id="T2061" time="142.938" type="appl" />
         <tli id="T2062" time="143.79" type="appl" />
         <tli id="T2063" time="144.642" type="appl" />
         <tli id="T2064" time="145.494" type="appl" />
         <tli id="T2065" time="146.347" type="appl" />
         <tli id="T2066" time="147.199" type="appl" />
         <tli id="T2067" time="148.051" type="appl" />
         <tli id="T2068" time="149.2" type="appl" />
         <tli id="T2069" time="150.348" type="appl" />
         <tli id="T2070" time="151.497" type="appl" />
         <tli id="T2071" time="153.68558065463876" />
         <tli id="T2072" time="154.692" type="appl" />
         <tli id="T2073" time="155.702" type="appl" />
         <tli id="T2074" time="156.711" type="appl" />
         <tli id="T2075" time="157.379" type="appl" />
         <tli id="T2076" time="158.047" type="appl" />
         <tli id="T2077" time="158.581" type="appl" />
         <tli id="T2078" time="159.115" type="appl" />
         <tli id="T2079" time="159.648" type="appl" />
         <tli id="T2080" time="160.182" type="appl" />
         <tli id="T2081" time="160.716" type="appl" />
         <tli id="T2082" time="161.97885538393086" />
         <tli id="T2083" time="162.508" type="appl" />
         <tli id="T2084" time="163.044" type="appl" />
         <tli id="T2085" time="163.93884153377962" />
         <tli id="T2086" time="164.761" type="appl" />
         <tli id="T2087" time="165.941" type="appl" />
         <tli id="T2088" time="167.122" type="appl" />
         <tli id="T2089" time="168.302" type="appl" />
         <tli id="T2090" time="169.483" type="appl" />
         <tli id="T2091" time="170.663" type="appl" />
         <tli id="T2092" time="171.844" type="appl" />
         <tli id="T2093" time="173.024" type="appl" />
         <tli id="T2094" time="174.205" type="appl" />
         <tli id="T2095" time="175.385" type="appl" />
         <tli id="T2096" time="176.566" type="appl" />
         <tli id="T2097" time="177.746" type="appl" />
         <tli id="T2098" time="178.9736701866936" />
         <tli id="T2099" time="179.606" type="appl" />
         <tli id="T2100" time="180.285" type="appl" />
         <tli id="T2101" time="180.964" type="appl" />
         <tli id="T2102" time="181.711" type="appl" />
         <tli id="T2103" time="182.457" type="appl" />
         <tli id="T2104" time="183.204" type="appl" />
         <tli id="T2105" time="183.95" type="appl" />
         <tli id="T2106" time="184.697" type="appl" />
         <tli id="T2107" time="185.273" type="appl" />
         <tli id="T2108" time="185.81" type="appl" />
         <tli id="T2109" time="186.346" type="appl" />
         <tli id="T2110" time="186.882" type="appl" />
         <tli id="T2111" time="187.418" type="appl" />
         <tli id="T2112" time="187.955" type="appl" />
         <tli id="T2113" time="188.491" type="appl" />
         <tli id="T2114" time="189.027" type="appl" />
         <tli id="T2115" time="189.564" type="appl" />
         <tli id="T2116" time="190.1" type="appl" />
         <tli id="T2117" time="190.9" type="appl" />
         <tli id="T2118" time="191.701" type="appl" />
         <tli id="T2119" time="192.501" type="appl" />
         <tli id="T2120" time="193.302" type="appl" />
         <tli id="T2121" time="194.102" type="appl" />
         <tli id="T2122" time="194.902" type="appl" />
         <tli id="T2123" time="195.703" type="appl" />
         <tli id="T2124" time="196.50299944150143" />
         <tli id="T2125" time="197.304" type="appl" />
         <tli id="T2126" time="198.104" type="appl" />
         <tli id="T2127" time="198.763" type="appl" />
         <tli id="T2128" time="199.89858742590306" />
         <tli id="T2129" time="200.597" type="appl" />
         <tli id="T2130" time="201.123" type="appl" />
         <tli id="T2131" time="201.65" type="appl" />
         <tli id="T2132" time="202.177" type="appl" />
         <tli id="T2133" time="202.703" type="appl" />
         <tli id="T2134" time="203.39189607393286" />
         <tli id="T2135" time="203.823" type="appl" />
         <tli id="T2136" time="204.246" type="appl" />
         <tli id="T2137" time="204.669" type="appl" />
         <tli id="T2138" time="205.092" type="appl" />
         <tli id="T2139" time="205.516" type="appl" />
         <tli id="T2140" time="205.939" type="appl" />
         <tli id="T2141" time="206.362" type="appl" />
         <tli id="T2142" time="206.785" type="appl" />
         <tli id="T2143" time="207.466" type="appl" />
         <tli id="T2144" time="207.998" type="appl" />
         <tli id="T2145" time="209.05185607808798" />
         <tli id="T2146" time="210.151" type="appl" />
         <tli id="T2147" time="211.09" type="appl" />
         <tli id="T2148" time="212.028" type="appl" />
         <tli id="T2149" time="212.967" type="appl" />
         <tli id="T2150" time="214.09848708297073" />
         <tli id="T2151" time="215.184" type="appl" />
         <tli id="T2152" time="216.264" type="appl" />
         <tli id="T2153" time="216.929" type="appl" />
         <tli id="T2154" time="217.594" type="appl" />
         <tli id="T2155" time="218.258" type="appl" />
         <tli id="T2156" time="218.923" type="appl" />
         <tli id="T2157" time="219.588" type="appl" />
         <tli id="T2158" time="220.192" type="appl" />
         <tli id="T2159" time="220.796" type="appl" />
         <tli id="T2160" time="221.4" type="appl" />
         <tli id="T2161" time="222.003" type="appl" />
         <tli id="T2162" time="222.607" type="appl" />
         <tli id="T2163" time="224.02508360380344" />
         <tli id="T2164" time="224.962" type="appl" />
         <tli id="T2165" time="225.807" type="appl" />
         <tli id="T2166" time="226.653" type="appl" />
         <tli id="T2167" time="227.498" type="appl" />
         <tli id="T2168" time="228.344" type="appl" />
         <tli id="T2169" time="229.189" type="appl" />
         <tli id="T2170" time="230.1383737378556" />
         <tli id="T2171" time="230.738" type="appl" />
         <tli id="T2172" time="231.154" type="appl" />
         <tli id="T2173" time="231.569" type="appl" />
         <tli id="T2174" time="231.984" type="appl" />
         <tli id="T2175" time="232.399" type="appl" />
         <tli id="T2176" time="232.814" type="appl" />
         <tli id="T2177" time="233.23" type="appl" />
         <tli id="T2178" time="233.645" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2178" id="Seg_0" n="sc" s="T1888">
               <ts e="T1889" id="Seg_2" n="HIAT:u" s="T1888">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1889" id="Seg_5" n="HIAT:non-pho" s="T1888">DMG</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1894" id="Seg_11" n="HIAT:u" s="T1889">
                  <ts e="T1890" id="Seg_13" n="HIAT:w" s="T1889">Onʼiʔ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1891" id="Seg_16" n="HIAT:w" s="T1890">kuzan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T1892" id="Seg_20" n="HIAT:w" s="T1891">bɨl-</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1893" id="Seg_24" n="HIAT:w" s="T1892">ibi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1894" id="Seg_27" n="HIAT:w" s="T1893">men</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1903" id="Seg_31" n="HIAT:u" s="T1894">
                  <ts e="T1895" id="Seg_33" n="HIAT:w" s="T1894">Dĭ</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1896" id="Seg_36" n="HIAT:w" s="T1895">üge</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1897" id="Seg_39" n="HIAT:w" s="T1896">dĭn</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1898" id="Seg_42" n="HIAT:w" s="T1897">bar</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ts e="T1899" id="Seg_46" n="HIAT:w" s="T1898">ulazaŋ-</ts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1900" id="Seg_50" n="HIAT:w" s="T1899">ularazaŋdə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1901" id="Seg_53" n="HIAT:w" s="T1900">ej</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1902" id="Seg_56" n="HIAT:w" s="T1901">mĭbi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1903" id="Seg_59" n="HIAT:w" s="T1902">volkdə</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1906" id="Seg_63" n="HIAT:u" s="T1903">
                  <ts e="T1904" id="Seg_65" n="HIAT:w" s="T1903">Dĭʔnə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1905" id="Seg_68" n="HIAT:w" s="T1904">üge</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1906" id="Seg_71" n="HIAT:w" s="T1905">kabažərbi</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1908" id="Seg_75" n="HIAT:u" s="T1906">
                  <ts e="T1908" id="Seg_77" n="HIAT:w" s="T1906">Dĭgəttə</ts>
                  <nts id="Seg_78" n="HIAT:ip">…</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1912" id="Seg_81" n="HIAT:u" s="T1908">
                  <ts e="T1909" id="Seg_83" n="HIAT:w" s="T1908">Ne</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1910" id="Seg_86" n="HIAT:w" s="T1909">znaju</ts>
                  <nts id="Seg_87" n="HIAT:ip">,</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1912" id="Seg_90" n="HIAT:w" s="T1910">kak</ts>
                  <nts id="Seg_91" n="HIAT:ip">…</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1916" id="Seg_94" n="HIAT:u" s="T1912">
                  <ts e="T1913" id="Seg_96" n="HIAT:w" s="T1912">Dĭgəttə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_98" n="HIAT:ip">(</nts>
                  <ts e="T1914" id="Seg_100" n="HIAT:w" s="T1913">dʼiʔ-</ts>
                  <nts id="Seg_101" n="HIAT:ip">)</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1915" id="Seg_104" n="HIAT:w" s="T1914">dʼiʔtə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1916" id="Seg_107" n="HIAT:w" s="T1915">molaːmbi</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1920" id="Seg_111" n="HIAT:u" s="T1916">
                  <ts e="T1917" id="Seg_113" n="HIAT:w" s="T1916">Dĭ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T1918" id="Seg_117" n="HIAT:w" s="T1917">dĭʔnə=</ts>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1919" id="Seg_121" n="HIAT:w" s="T1918">dĭm</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1920" id="Seg_124" n="HIAT:w" s="T1919">ibi</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1921" id="Seg_128" n="HIAT:u" s="T1920">
                  <ts e="T1921" id="Seg_130" n="HIAT:w" s="T1920">Sarobi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1924" id="Seg_134" n="HIAT:u" s="T1921">
                  <ts e="T1922" id="Seg_136" n="HIAT:w" s="T1921">I</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1923" id="Seg_139" n="HIAT:w" s="T1922">kumbi</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1924" id="Seg_142" n="HIAT:w" s="T1923">dʼijenə</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1927" id="Seg_146" n="HIAT:u" s="T1924">
                  <ts e="T1925" id="Seg_148" n="HIAT:w" s="T1924">Xatʼel</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1926" id="Seg_151" n="HIAT:w" s="T1925">dĭm</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1927" id="Seg_154" n="HIAT:w" s="T1926">pʼaŋdəsʼtə</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1931" id="Seg_158" n="HIAT:u" s="T1927">
                  <nts id="Seg_159" n="HIAT:ip">(</nts>
                  <ts e="T1928" id="Seg_161" n="HIAT:w" s="T1927">Dĭn</ts>
                  <nts id="Seg_162" n="HIAT:ip">)</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1929" id="Seg_165" n="HIAT:w" s="T1928">bar</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1930" id="Seg_168" n="HIAT:w" s="T1929">tʼorlaʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1931" id="Seg_171" n="HIAT:w" s="T1930">amnolaʔbə</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1937" id="Seg_175" n="HIAT:u" s="T1931">
                  <ts e="T1932" id="Seg_177" n="HIAT:w" s="T1931">Dĭgəttə</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_179" n="HIAT:ip">(</nts>
                  <ts e="T1933" id="Seg_181" n="HIAT:w" s="T1932">dĭm=</ts>
                  <nts id="Seg_182" n="HIAT:ip">)</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1934" id="Seg_185" n="HIAT:w" s="T1933">dĭm</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_187" n="HIAT:ip">(</nts>
                  <ts e="T1935" id="Seg_189" n="HIAT:w" s="T1934">sʼ-</ts>
                  <nts id="Seg_190" n="HIAT:ip">)</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1936" id="Seg_193" n="HIAT:w" s="T1935">sarbi</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_195" n="HIAT:ip">(</nts>
                  <ts e="T1937" id="Seg_197" n="HIAT:w" s="T1936">panə</ts>
                  <nts id="Seg_198" n="HIAT:ip">)</nts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1941" id="Seg_202" n="HIAT:u" s="T1937">
                  <ts e="T1938" id="Seg_204" n="HIAT:w" s="T1937">Da</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1939" id="Seg_207" n="HIAT:w" s="T1938">bostə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1940" id="Seg_210" n="HIAT:w" s="T1939">kalla</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1941" id="Seg_213" n="HIAT:w" s="T1940">dʼürbi</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1944" id="Seg_217" n="HIAT:u" s="T1941">
                  <ts e="T1942" id="Seg_219" n="HIAT:w" s="T1941">Dĭgəttə</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1943" id="Seg_222" n="HIAT:w" s="T1942">volkdə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1944" id="Seg_225" n="HIAT:w" s="T1943">šonəga</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1947" id="Seg_229" n="HIAT:u" s="T1944">
                  <nts id="Seg_230" n="HIAT:ip">"</nts>
                  <ts e="T1945" id="Seg_232" n="HIAT:w" s="T1944">Tăn</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1946" id="Seg_235" n="HIAT:w" s="T1945">dön</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1947" id="Seg_238" n="HIAT:w" s="T1946">amnolaʔbəl</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1953" id="Seg_242" n="HIAT:u" s="T1947">
                  <ts e="T1948" id="Seg_244" n="HIAT:w" s="T1947">Măna</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1949" id="Seg_247" n="HIAT:w" s="T1948">ej</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1950" id="Seg_250" n="HIAT:w" s="T1949">öʔlubiel</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1951" id="Seg_254" n="HIAT:w" s="T1950">kamən</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1952" id="Seg_257" n="HIAT:w" s="T1951">măn</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1953" id="Seg_260" n="HIAT:w" s="T1952">šobiam</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1956" id="Seg_264" n="HIAT:u" s="T1953">
                  <ts e="T1954" id="Seg_266" n="HIAT:w" s="T1953">Tüjö</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1955" id="Seg_269" n="HIAT:w" s="T1954">tănan</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1956" id="Seg_272" n="HIAT:w" s="T1955">amluʔlam</ts>
                  <nts id="Seg_273" n="HIAT:ip">"</nts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1961" id="Seg_277" n="HIAT:u" s="T1956">
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <ts e="T1957" id="Seg_280" n="HIAT:w" s="T1956">Da</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1958" id="Seg_283" n="HIAT:w" s="T1957">ĭmbi</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1959" id="Seg_286" n="HIAT:w" s="T1958">tăn</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1960" id="Seg_289" n="HIAT:w" s="T1959">măna</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1961" id="Seg_292" n="HIAT:w" s="T1960">amnal</ts>
                  <nts id="Seg_293" n="HIAT:ip">?</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1966" id="Seg_296" n="HIAT:u" s="T1961">
                  <ts e="T1962" id="Seg_298" n="HIAT:w" s="T1961">Măn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1964" id="Seg_301" n="HIAT:w" s="T1962">tolʼko</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1965" id="Seg_304" n="HIAT:w" s="T1964">uja</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1966" id="Seg_307" n="HIAT:w" s="T1965">naga</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1969" id="Seg_311" n="HIAT:u" s="T1966">
                  <ts e="T1967" id="Seg_313" n="HIAT:w" s="T1966">Da</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1968" id="Seg_316" n="HIAT:w" s="T1967">dʼaktə</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1969" id="Seg_319" n="HIAT:w" s="T1968">molaːmbiam</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1975" id="Seg_323" n="HIAT:u" s="T1969">
                  <ts e="T1970" id="Seg_325" n="HIAT:w" s="T1969">Kanaʔ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1971" id="Seg_328" n="HIAT:w" s="T1970">da</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1972" id="Seg_331" n="HIAT:w" s="T1971">uja</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1973" id="Seg_334" n="HIAT:w" s="T1972">dettə</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1974" id="Seg_337" n="HIAT:w" s="T1973">inen</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1975" id="Seg_340" n="HIAT:w" s="T1974">măna</ts>
                  <nts id="Seg_341" n="HIAT:ip">"</nts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1980" id="Seg_345" n="HIAT:u" s="T1975">
                  <ts e="T1976" id="Seg_347" n="HIAT:w" s="T1975">Da</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1977" id="Seg_350" n="HIAT:w" s="T1976">kambi</ts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1978" id="Seg_354" n="HIAT:w" s="T1977">deʔpi</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1979" id="Seg_357" n="HIAT:w" s="T1978">uja</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1980" id="Seg_360" n="HIAT:w" s="T1979">inen</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1982" id="Seg_364" n="HIAT:u" s="T1980">
                  <ts e="T1981" id="Seg_366" n="HIAT:w" s="T1980">Dĭ</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1982" id="Seg_369" n="HIAT:w" s="T1981">ambi</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1992" id="Seg_373" n="HIAT:u" s="T1982">
                  <nts id="Seg_374" n="HIAT:ip">(</nts>
                  <ts e="T1983" id="Seg_376" n="HIAT:w" s="T1982">Dĭgəttə</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1985" id="Seg_379" n="HIAT:w" s="T1983">măndə:</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1986" id="Seg_382" n="HIAT:w" s="T1985">Šobi</ts>
                  <nts id="Seg_383" n="HIAT:ip">)</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1987" id="Seg_386" n="HIAT:w" s="T1986">Šide</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1988" id="Seg_389" n="HIAT:w" s="T1987">dʼala</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_392" n="HIAT:w" s="T1988">kalla</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1989" id="Seg_395" n="HIAT:w" s="T0">dʼürbiʔi</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1990" id="Seg_399" n="HIAT:w" s="T1989">dĭ</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1991" id="Seg_402" n="HIAT:w" s="T1990">šobi</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1992" id="Seg_405" n="HIAT:w" s="T1991">dĭʔnə</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1999" id="Seg_409" n="HIAT:u" s="T1992">
                  <nts id="Seg_410" n="HIAT:ip">"</nts>
                  <ts e="T1993" id="Seg_412" n="HIAT:w" s="T1992">No</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1994" id="Seg_416" n="HIAT:w" s="T1993">ĭmbi</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1995" id="Seg_420" n="HIAT:w" s="T1994">iššo</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1996" id="Seg_423" n="HIAT:w" s="T1995">deʔtə</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1997" id="Seg_426" n="HIAT:w" s="T1996">măna</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1998" id="Seg_429" n="HIAT:w" s="T1997">ularən</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1999" id="Seg_432" n="HIAT:w" s="T1998">uja</ts>
                  <nts id="Seg_433" n="HIAT:ip">"</nts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2004" id="Seg_437" n="HIAT:u" s="T1999">
                  <ts e="T2000" id="Seg_439" n="HIAT:w" s="T1999">Dĭ</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2001" id="Seg_442" n="HIAT:w" s="T2000">kambi</ts>
                  <nts id="Seg_443" n="HIAT:ip">,</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2002" id="Seg_446" n="HIAT:w" s="T2001">ular</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2003" id="Seg_449" n="HIAT:w" s="T2002">dĭʔnə</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2004" id="Seg_452" n="HIAT:w" s="T2003">deʔpi</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2008" id="Seg_456" n="HIAT:u" s="T2004">
                  <ts e="T2005" id="Seg_458" n="HIAT:w" s="T2004">Dĭ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2006" id="Seg_461" n="HIAT:w" s="T2005">amnuʔpi</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2007" id="Seg_464" n="HIAT:w" s="T2006">dĭ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2008" id="Seg_467" n="HIAT:w" s="T2007">ulardə</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2012" id="Seg_471" n="HIAT:u" s="T2008">
                  <ts e="T2009" id="Seg_473" n="HIAT:w" s="T2008">Dĭgəttə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2010" id="Seg_476" n="HIAT:w" s="T2009">bazoʔ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2011" id="Seg_479" n="HIAT:w" s="T2010">šobi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2012" id="Seg_482" n="HIAT:w" s="T2011">volk</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2015" id="Seg_486" n="HIAT:u" s="T2012">
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <ts e="T2013" id="Seg_489" n="HIAT:w" s="T2012">Ĭmbi</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2014" id="Seg_493" n="HIAT:w" s="T2013">jakšə</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2015" id="Seg_496" n="HIAT:w" s="T2014">molaːmbiam</ts>
                  <nts id="Seg_497" n="HIAT:ip">?</nts>
                  <nts id="Seg_498" n="HIAT:ip">"</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2020" id="Seg_501" n="HIAT:u" s="T2015">
                  <nts id="Seg_502" n="HIAT:ip">"</nts>
                  <ts e="T2016" id="Seg_504" n="HIAT:w" s="T2015">Dʼok</ts>
                  <nts id="Seg_505" n="HIAT:ip">,</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2017" id="Seg_508" n="HIAT:w" s="T2016">iššo</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2018" id="Seg_511" n="HIAT:w" s="T2017">šoškan</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2019" id="Seg_514" n="HIAT:w" s="T2018">uja</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2020" id="Seg_517" n="HIAT:w" s="T2019">detləl</ts>
                  <nts id="Seg_518" n="HIAT:ip">!</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2024" id="Seg_521" n="HIAT:u" s="T2020">
                  <ts e="T2021" id="Seg_523" n="HIAT:w" s="T2020">Dĭgəttə</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2022" id="Seg_526" n="HIAT:w" s="T2021">măn</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2023" id="Seg_529" n="HIAT:w" s="T2022">jakšə</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2024" id="Seg_532" n="HIAT:w" s="T2023">molam</ts>
                  <nts id="Seg_533" n="HIAT:ip">"</nts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2027" id="Seg_537" n="HIAT:u" s="T2024">
                  <ts e="T2025" id="Seg_539" n="HIAT:w" s="T2024">Dĭ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2026" id="Seg_542" n="HIAT:w" s="T2025">šoškabə</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2027" id="Seg_545" n="HIAT:w" s="T2026">deʔpi</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2030" id="Seg_549" n="HIAT:u" s="T2027">
                  <ts e="T2028" id="Seg_551" n="HIAT:w" s="T2027">Dĭ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2029" id="Seg_554" n="HIAT:w" s="T2028">men</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2030" id="Seg_557" n="HIAT:w" s="T2029">amluʔpi</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2032" id="Seg_561" n="HIAT:u" s="T2030">
                  <ts e="T2031" id="Seg_563" n="HIAT:w" s="T2030">Dĭgəttə</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2032" id="Seg_566" n="HIAT:w" s="T2031">šobi</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2038" id="Seg_570" n="HIAT:u" s="T2032">
                  <ts e="T2033" id="Seg_572" n="HIAT:w" s="T2032">Dĭgəttə</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2034" id="Seg_575" n="HIAT:w" s="T2033">davaj</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_577" n="HIAT:ip">(</nts>
                  <ts e="T2035" id="Seg_579" n="HIAT:w" s="T2034">m-</ts>
                  <nts id="Seg_580" n="HIAT:ip">)</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2036" id="Seg_583" n="HIAT:w" s="T2035">men</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2037" id="Seg_586" n="HIAT:w" s="T2036">dĭm</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2038" id="Seg_589" n="HIAT:w" s="T2037">amnosʼtə</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2047" id="Seg_593" n="HIAT:u" s="T2038">
                  <ts e="T2039" id="Seg_595" n="HIAT:w" s="T2038">Dʼabərobiʔi</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2040" id="Seg_599" n="HIAT:w" s="T2039">dʼabərobiʔi</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2041" id="Seg_602" n="HIAT:w" s="T2040">dak</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2042" id="Seg_605" n="HIAT:w" s="T2041">volk</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2043" id="Seg_608" n="HIAT:w" s="T2042">nuʔməluʔpi</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_610" n="HIAT:ip">(</nts>
                  <ts e="T2044" id="Seg_612" n="HIAT:w" s="T2043">dĭg-</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2045" id="Seg_615" n="HIAT:w" s="T2044">dĭg-</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2046" id="Seg_618" n="HIAT:w" s="T2045">dĭg-</ts>
                  <nts id="Seg_619" n="HIAT:ip">)</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2047" id="Seg_622" n="HIAT:w" s="T2046">dĭgəʔ</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2052" id="Seg_626" n="HIAT:u" s="T2047">
                  <ts e="T2048" id="Seg_628" n="HIAT:w" s="T2047">I</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2049" id="Seg_631" n="HIAT:w" s="T2048">amnolaʔbə</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2050" id="Seg_634" n="HIAT:w" s="T2049">kustə</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2051" id="Seg_637" n="HIAT:w" s="T2050">toːndə</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2052" id="Seg_640" n="HIAT:w" s="T2051">da</ts>
                  <nts id="Seg_641" n="HIAT:ip">…</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2056" id="Seg_644" n="HIAT:u" s="T2052">
                  <ts e="T2053" id="Seg_646" n="HIAT:w" s="T2052">Bostə</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2054" id="Seg_649" n="HIAT:w" s="T2053">kem</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2055" id="Seg_652" n="HIAT:w" s="T2054">šĭketsiʔ</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2056" id="Seg_655" n="HIAT:w" s="T2055">amnaʔbə</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2061" id="Seg_659" n="HIAT:u" s="T2056">
                  <ts e="T2057" id="Seg_661" n="HIAT:w" s="T2056">Da</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2058" id="Seg_664" n="HIAT:w" s="T2057">kudonzlaʔbə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_666" n="HIAT:ip">(</nts>
                  <ts e="T2059" id="Seg_668" n="HIAT:w" s="T2058">to</ts>
                  <nts id="Seg_669" n="HIAT:ip">)</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_671" n="HIAT:ip">(</nts>
                  <ts e="T2060" id="Seg_673" n="HIAT:w" s="T2059">men=</ts>
                  <nts id="Seg_674" n="HIAT:ip">)</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2061" id="Seg_677" n="HIAT:w" s="T2060">mendə</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2067" id="Seg_681" n="HIAT:u" s="T2061">
                  <nts id="Seg_682" n="HIAT:ip">"</nts>
                  <ts e="T2062" id="Seg_684" n="HIAT:w" s="T2061">Tüj</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_686" n="HIAT:ip">(</nts>
                  <ts e="T2063" id="Seg_688" n="HIAT:w" s="T2062">šindənədə=</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2064" id="Seg_691" n="HIAT:w" s="T2063">šindin-</ts>
                  <nts id="Seg_692" n="HIAT:ip">)</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2065" id="Seg_695" n="HIAT:w" s="T2064">šindəmdə</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2066" id="Seg_698" n="HIAT:w" s="T2065">ej</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2067" id="Seg_701" n="HIAT:w" s="T2066">ajirlam</ts>
                  <nts id="Seg_702" n="HIAT:ip">!</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2070" id="Seg_705" n="HIAT:u" s="T2067">
                  <nts id="Seg_706" n="HIAT:ip">(</nts>
                  <ts e="T2068" id="Seg_708" n="HIAT:w" s="T2067">Šindi=</ts>
                  <nts id="Seg_709" n="HIAT:ip">)</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2069" id="Seg_712" n="HIAT:w" s="T2068">Bar</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2070" id="Seg_715" n="HIAT:w" s="T2069">amnuʔlam</ts>
                  <nts id="Seg_716" n="HIAT:ip">!</nts>
                  <nts id="Seg_717" n="HIAT:ip">"</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2071" id="Seg_720" n="HIAT:u" s="T2070">
                  <ts e="T2071" id="Seg_722" n="HIAT:w" s="T2070">Kabarləj</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2074" id="Seg_726" n="HIAT:u" s="T2071">
                  <ts e="T2072" id="Seg_728" n="HIAT:w" s="T2071">Dĭgəttə</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2073" id="Seg_731" n="HIAT:w" s="T2072">dĭ</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2074" id="Seg_734" n="HIAT:w" s="T2073">kandəlaʔbə</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2076" id="Seg_738" n="HIAT:u" s="T2074">
                  <ts e="T2075" id="Seg_740" n="HIAT:w" s="T2074">Poʔto</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2076" id="Seg_743" n="HIAT:w" s="T2075">nulaʔbə</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2082" id="Seg_747" n="HIAT:u" s="T2076">
                  <nts id="Seg_748" n="HIAT:ip">"</nts>
                  <nts id="Seg_749" n="HIAT:ip">(</nts>
                  <ts e="T2077" id="Seg_751" n="HIAT:w" s="T2076">Măn=</ts>
                  <nts id="Seg_752" n="HIAT:ip">)</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2078" id="Seg_755" n="HIAT:w" s="T2077">Poʔto</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2079" id="Seg_759" n="HIAT:w" s="T2078">poʔto</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2080" id="Seg_763" n="HIAT:w" s="T2079">măn</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2081" id="Seg_766" n="HIAT:w" s="T2080">tănan</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2082" id="Seg_769" n="HIAT:w" s="T2081">amnam</ts>
                  <nts id="Seg_770" n="HIAT:ip">!</nts>
                  <nts id="Seg_771" n="HIAT:ip">"</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2085" id="Seg_774" n="HIAT:u" s="T2082">
                  <nts id="Seg_775" n="HIAT:ip">"</nts>
                  <ts e="T2083" id="Seg_777" n="HIAT:w" s="T2082">Iʔ</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2084" id="Seg_780" n="HIAT:w" s="T2083">amaʔ</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2085" id="Seg_783" n="HIAT:w" s="T2084">măna</ts>
                  <nts id="Seg_784" n="HIAT:ip">!</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2098" id="Seg_787" n="HIAT:u" s="T2085">
                  <nts id="Seg_788" n="HIAT:ip">(</nts>
                  <ts e="T2086" id="Seg_790" n="HIAT:w" s="T2085">Tăn=</ts>
                  <nts id="Seg_791" n="HIAT:ip">)</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2087" id="Seg_794" n="HIAT:w" s="T2086">Măn</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2088" id="Seg_797" n="HIAT:w" s="T2087">nʼuʔdən</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2089" id="Seg_801" n="HIAT:w" s="T2088">a</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_803" n="HIAT:ip">(</nts>
                  <ts e="T2090" id="Seg_805" n="HIAT:w" s="T2089">tăn=</ts>
                  <nts id="Seg_806" n="HIAT:ip">)</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2091" id="Seg_809" n="HIAT:w" s="T2090">dĭn</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_811" n="HIAT:ip">(</nts>
                  <ts e="T2092" id="Seg_813" n="HIAT:w" s="T2091">măn=</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2093" id="Seg_816" n="HIAT:w" s="T2092">kaj-</ts>
                  <nts id="Seg_817" n="HIAT:ip">)</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2094" id="Seg_820" n="HIAT:w" s="T2093">aŋdə</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ts e="T2095" id="Seg_824" n="HIAT:w" s="T2094">kajdə=</ts>
                  <nts id="Seg_825" n="HIAT:ip">)</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2096" id="Seg_828" n="HIAT:w" s="T2095">kardə</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_830" n="HIAT:ip">(</nts>
                  <ts e="T2097" id="Seg_832" n="HIAT:w" s="T2096">ăndə-</ts>
                  <nts id="Seg_833" n="HIAT:ip">)</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2098" id="Seg_836" n="HIAT:w" s="T2097">aŋdə</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2101" id="Seg_840" n="HIAT:u" s="T2098">
                  <ts e="T2099" id="Seg_842" n="HIAT:w" s="T2098">A</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2100" id="Seg_845" n="HIAT:w" s="T2099">măn</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2101" id="Seg_848" n="HIAT:w" s="T2100">nuʔməluʔpim</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2106" id="Seg_852" n="HIAT:u" s="T2101">
                  <ts e="T2102" id="Seg_854" n="HIAT:w" s="T2101">I</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2103" id="Seg_857" n="HIAT:w" s="T2102">prʼamă</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2104" id="Seg_860" n="HIAT:w" s="T2103">tăn</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2105" id="Seg_863" n="HIAT:w" s="T2104">aŋgəndə</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2106" id="Seg_866" n="HIAT:w" s="T2105">suʔmiluʔləm</ts>
                  <nts id="Seg_867" n="HIAT:ip">"</nts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2116" id="Seg_871" n="HIAT:u" s="T2106">
                  <ts e="T2107" id="Seg_873" n="HIAT:w" s="T2106">Dĭgəttə</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2108" id="Seg_876" n="HIAT:w" s="T2107">kozʼol</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2109" id="Seg_879" n="HIAT:w" s="T2108">dĭ</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2110" id="Seg_882" n="HIAT:w" s="T2109">kak</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2111" id="Seg_885" n="HIAT:w" s="T2110">nuʔmiluʔpi</ts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2112" id="Seg_889" n="HIAT:w" s="T2111">dĭ</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2113" id="Seg_892" n="HIAT:w" s="T2112">dĭm</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2114" id="Seg_895" n="HIAT:w" s="T2113">ulundə</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2115" id="Seg_898" n="HIAT:w" s="T2114">kak</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2116" id="Seg_901" n="HIAT:w" s="T2115">toʔnarluʔpi</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2126" id="Seg_905" n="HIAT:u" s="T2116">
                  <ts e="T2117" id="Seg_907" n="HIAT:w" s="T2116">A</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2118" id="Seg_910" n="HIAT:w" s="T2117">dĭ</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2119" id="Seg_913" n="HIAT:w" s="T2118">saʔməluʔpi</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2120" id="Seg_916" n="HIAT:w" s="T2119">i</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_918" n="HIAT:ip">(</nts>
                  <ts e="T2121" id="Seg_920" n="HIAT:w" s="T2120">aʔpi</ts>
                  <nts id="Seg_921" n="HIAT:ip">)</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2122" id="Seg_924" n="HIAT:w" s="T2121">ej</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2123" id="Seg_927" n="HIAT:w" s="T2122">kübi</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2124" id="Seg_931" n="HIAT:w" s="T2123">a</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2125" id="Seg_934" n="HIAT:w" s="T2124">poʔto</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2126" id="Seg_937" n="HIAT:w" s="T2125">nuʔməluʔpi</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2128" id="Seg_941" n="HIAT:u" s="T2126">
                  <ts e="T2127" id="Seg_943" n="HIAT:w" s="T2126">Dĭ</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2128" id="Seg_946" n="HIAT:w" s="T2127">uʔbdəbi</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2134" id="Seg_950" n="HIAT:u" s="T2128">
                  <nts id="Seg_951" n="HIAT:ip">"</nts>
                  <ts e="T2129" id="Seg_953" n="HIAT:w" s="T2128">Ambiam</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2130" id="Seg_956" n="HIAT:w" s="T2129">măn</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2131" id="Seg_959" n="HIAT:w" s="T2130">poʔto</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2132" id="Seg_962" n="HIAT:w" s="T2131">alʼi</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2133" id="Seg_965" n="HIAT:w" s="T2132">ej</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2134" id="Seg_968" n="HIAT:w" s="T2133">ambiam</ts>
                  <nts id="Seg_969" n="HIAT:ip">?</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2142" id="Seg_972" n="HIAT:u" s="T2134">
                  <ts e="T2135" id="Seg_974" n="HIAT:w" s="T2134">Ambiam</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2136" id="Seg_977" n="HIAT:w" s="T2135">bɨ</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2137" id="Seg_980" n="HIAT:w" s="T2136">dak</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2138" id="Seg_983" n="HIAT:w" s="T2137">măn</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2139" id="Seg_986" n="HIAT:w" s="T2138">nanəm</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2140" id="Seg_989" n="HIAT:w" s="T2139">bɨ</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2141" id="Seg_992" n="HIAT:w" s="T2140">urgo</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2142" id="Seg_995" n="HIAT:w" s="T2141">ibi</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2145" id="Seg_999" n="HIAT:u" s="T2142">
                  <ts e="T2143" id="Seg_1001" n="HIAT:w" s="T2142">Naverno</ts>
                  <nts id="Seg_1002" n="HIAT:ip">,</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2144" id="Seg_1005" n="HIAT:w" s="T2143">ej</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2145" id="Seg_1008" n="HIAT:w" s="T2144">ambiam</ts>
                  <nts id="Seg_1009" n="HIAT:ip">"</nts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2150" id="Seg_1013" n="HIAT:u" s="T2145">
                  <ts e="T2146" id="Seg_1015" n="HIAT:w" s="T2145">Kambi</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2147" id="Seg_1018" n="HIAT:w" s="T2146">turazaŋdə</ts>
                  <nts id="Seg_1019" n="HIAT:ip">,</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2148" id="Seg_1022" n="HIAT:w" s="T2147">kuliot</ts>
                  <nts id="Seg_1023" n="HIAT:ip">,</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2149" id="Seg_1026" n="HIAT:w" s="T2148">šoška</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2150" id="Seg_1029" n="HIAT:w" s="T2149">mĭnge</ts>
                  <nts id="Seg_1030" n="HIAT:ip">.</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2152" id="Seg_1033" n="HIAT:u" s="T2150">
                  <ts e="T2151" id="Seg_1035" n="HIAT:w" s="T2150">Üdʼügeʔi</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2152" id="Seg_1038" n="HIAT:w" s="T2151">šoškaʔi</ts>
                  <nts id="Seg_1039" n="HIAT:ip">.</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2157" id="Seg_1042" n="HIAT:u" s="T2152">
                  <ts e="T2153" id="Seg_1044" n="HIAT:w" s="T2152">Xatʼel</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2154" id="Seg_1047" n="HIAT:w" s="T2153">kabarzittə</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2155" id="Seg_1050" n="HIAT:w" s="T2154">onʼiʔ</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2156" id="Seg_1053" n="HIAT:w" s="T2155">üdʼüge</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2157" id="Seg_1056" n="HIAT:w" s="T2156">šoška</ts>
                  <nts id="Seg_1057" n="HIAT:ip">.</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2163" id="Seg_1060" n="HIAT:u" s="T2157">
                  <ts e="T2158" id="Seg_1062" n="HIAT:w" s="T2157">Šoška</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2159" id="Seg_1065" n="HIAT:w" s="T2158">măndə:</ts>
                  <nts id="Seg_1066" n="HIAT:ip">"</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2160" id="Seg_1069" n="HIAT:w" s="T2159">Iʔ</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2161" id="Seg_1072" n="HIAT:w" s="T2160">kabarəʔ</ts>
                  <nts id="Seg_1073" n="HIAT:ip">,</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2162" id="Seg_1076" n="HIAT:w" s="T2161">dĭzeŋ</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2163" id="Seg_1079" n="HIAT:w" s="T2162">üdʼügeʔi</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2170" id="Seg_1083" n="HIAT:u" s="T2163">
                  <ts e="T2164" id="Seg_1085" n="HIAT:w" s="T2163">Kanžəbəj</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2165" id="Seg_1088" n="HIAT:w" s="T2164">bünə</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2166" id="Seg_1091" n="HIAT:w" s="T2165">da</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1093" n="HIAT:ip">(</nts>
                  <ts e="T2167" id="Seg_1095" n="HIAT:w" s="T2166">dĭziŋ-</ts>
                  <nts id="Seg_1096" n="HIAT:ip">)</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2168" id="Seg_1099" n="HIAT:w" s="T2167">dĭzem</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1101" n="HIAT:ip">(</nts>
                  <ts e="T2169" id="Seg_1103" n="HIAT:w" s="T2168">kros-</ts>
                  <nts id="Seg_1104" n="HIAT:ip">)</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2170" id="Seg_1107" n="HIAT:w" s="T2169">krossʼtə</ts>
                  <nts id="Seg_1108" n="HIAT:ip">"</nts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2178" id="Seg_1112" n="HIAT:u" s="T2170">
                  <ts e="T2171" id="Seg_1114" n="HIAT:w" s="T2170">Dĭgəttə</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2172" id="Seg_1117" n="HIAT:w" s="T2171">šobiʔi:</ts>
                  <nts id="Seg_1118" n="HIAT:ip">"</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2173" id="Seg_1121" n="HIAT:w" s="T2172">Tăn</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2174" id="Seg_1124" n="HIAT:w" s="T2173">nuʔ</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2175" id="Seg_1127" n="HIAT:w" s="T2174">dĭn</ts>
                  <nts id="Seg_1128" n="HIAT:ip">,</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2176" id="Seg_1131" n="HIAT:w" s="T2175">a</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2177" id="Seg_1134" n="HIAT:w" s="T2176">măn</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2178" id="Seg_1137" n="HIAT:w" s="T2177">döbər</ts>
                  <nts id="Seg_1138" n="HIAT:ip">"</nts>
                  <nts id="Seg_1139" n="HIAT:ip">.</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2178" id="Seg_1141" n="sc" s="T1888">
               <ts e="T1889" id="Seg_1143" n="e" s="T1888">((DMG)). </ts>
               <ts e="T1890" id="Seg_1145" n="e" s="T1889">Onʼiʔ </ts>
               <ts e="T1891" id="Seg_1147" n="e" s="T1890">kuzan </ts>
               <ts e="T1892" id="Seg_1149" n="e" s="T1891">(bɨl-) </ts>
               <ts e="T1893" id="Seg_1151" n="e" s="T1892">ibi </ts>
               <ts e="T1894" id="Seg_1153" n="e" s="T1893">men. </ts>
               <ts e="T1895" id="Seg_1155" n="e" s="T1894">Dĭ </ts>
               <ts e="T1896" id="Seg_1157" n="e" s="T1895">üge </ts>
               <ts e="T1897" id="Seg_1159" n="e" s="T1896">dĭn </ts>
               <ts e="T1898" id="Seg_1161" n="e" s="T1897">bar </ts>
               <ts e="T1899" id="Seg_1163" n="e" s="T1898">(ulazaŋ-) </ts>
               <ts e="T1900" id="Seg_1165" n="e" s="T1899">ularazaŋdə </ts>
               <ts e="T1901" id="Seg_1167" n="e" s="T1900">ej </ts>
               <ts e="T1902" id="Seg_1169" n="e" s="T1901">mĭbi </ts>
               <ts e="T1903" id="Seg_1171" n="e" s="T1902">volkdə. </ts>
               <ts e="T1904" id="Seg_1173" n="e" s="T1903">Dĭʔnə </ts>
               <ts e="T1905" id="Seg_1175" n="e" s="T1904">üge </ts>
               <ts e="T1906" id="Seg_1177" n="e" s="T1905">kabažərbi. </ts>
               <ts e="T1908" id="Seg_1179" n="e" s="T1906">Dĭgəttə… </ts>
               <ts e="T1909" id="Seg_1181" n="e" s="T1908">Ne </ts>
               <ts e="T1910" id="Seg_1183" n="e" s="T1909">znaju, </ts>
               <ts e="T1912" id="Seg_1185" n="e" s="T1910">kak… </ts>
               <ts e="T1913" id="Seg_1187" n="e" s="T1912">Dĭgəttə </ts>
               <ts e="T1914" id="Seg_1189" n="e" s="T1913">(dʼiʔ-) </ts>
               <ts e="T1915" id="Seg_1191" n="e" s="T1914">dʼiʔtə </ts>
               <ts e="T1916" id="Seg_1193" n="e" s="T1915">molaːmbi. </ts>
               <ts e="T1917" id="Seg_1195" n="e" s="T1916">Dĭ </ts>
               <ts e="T1918" id="Seg_1197" n="e" s="T1917">(dĭʔnə=) </ts>
               <ts e="T1919" id="Seg_1199" n="e" s="T1918">dĭm </ts>
               <ts e="T1920" id="Seg_1201" n="e" s="T1919">ibi. </ts>
               <ts e="T1921" id="Seg_1203" n="e" s="T1920">Sarobi. </ts>
               <ts e="T1922" id="Seg_1205" n="e" s="T1921">I </ts>
               <ts e="T1923" id="Seg_1207" n="e" s="T1922">kumbi </ts>
               <ts e="T1924" id="Seg_1209" n="e" s="T1923">dʼijenə. </ts>
               <ts e="T1925" id="Seg_1211" n="e" s="T1924">Xatʼel </ts>
               <ts e="T1926" id="Seg_1213" n="e" s="T1925">dĭm </ts>
               <ts e="T1927" id="Seg_1215" n="e" s="T1926">pʼaŋdəsʼtə. </ts>
               <ts e="T1928" id="Seg_1217" n="e" s="T1927">(Dĭn) </ts>
               <ts e="T1929" id="Seg_1219" n="e" s="T1928">bar </ts>
               <ts e="T1930" id="Seg_1221" n="e" s="T1929">tʼorlaʔ </ts>
               <ts e="T1931" id="Seg_1223" n="e" s="T1930">amnolaʔbə. </ts>
               <ts e="T1932" id="Seg_1225" n="e" s="T1931">Dĭgəttə </ts>
               <ts e="T1933" id="Seg_1227" n="e" s="T1932">(dĭm=) </ts>
               <ts e="T1934" id="Seg_1229" n="e" s="T1933">dĭm </ts>
               <ts e="T1935" id="Seg_1231" n="e" s="T1934">(sʼ-) </ts>
               <ts e="T1936" id="Seg_1233" n="e" s="T1935">sarbi </ts>
               <ts e="T1937" id="Seg_1235" n="e" s="T1936">(panə). </ts>
               <ts e="T1938" id="Seg_1237" n="e" s="T1937">Da </ts>
               <ts e="T1939" id="Seg_1239" n="e" s="T1938">bostə </ts>
               <ts e="T1940" id="Seg_1241" n="e" s="T1939">kalla </ts>
               <ts e="T1941" id="Seg_1243" n="e" s="T1940">dʼürbi. </ts>
               <ts e="T1942" id="Seg_1245" n="e" s="T1941">Dĭgəttə </ts>
               <ts e="T1943" id="Seg_1247" n="e" s="T1942">volkdə </ts>
               <ts e="T1944" id="Seg_1249" n="e" s="T1943">šonəga. </ts>
               <ts e="T1945" id="Seg_1251" n="e" s="T1944">"Tăn </ts>
               <ts e="T1946" id="Seg_1253" n="e" s="T1945">dön </ts>
               <ts e="T1947" id="Seg_1255" n="e" s="T1946">amnolaʔbəl. </ts>
               <ts e="T1948" id="Seg_1257" n="e" s="T1947">Măna </ts>
               <ts e="T1949" id="Seg_1259" n="e" s="T1948">ej </ts>
               <ts e="T1950" id="Seg_1261" n="e" s="T1949">öʔlubiel, </ts>
               <ts e="T1951" id="Seg_1263" n="e" s="T1950">kamən </ts>
               <ts e="T1952" id="Seg_1265" n="e" s="T1951">măn </ts>
               <ts e="T1953" id="Seg_1267" n="e" s="T1952">šobiam. </ts>
               <ts e="T1954" id="Seg_1269" n="e" s="T1953">Tüjö </ts>
               <ts e="T1955" id="Seg_1271" n="e" s="T1954">tănan </ts>
               <ts e="T1956" id="Seg_1273" n="e" s="T1955">amluʔlam". </ts>
               <ts e="T1957" id="Seg_1275" n="e" s="T1956">"Da </ts>
               <ts e="T1958" id="Seg_1277" n="e" s="T1957">ĭmbi </ts>
               <ts e="T1959" id="Seg_1279" n="e" s="T1958">tăn </ts>
               <ts e="T1960" id="Seg_1281" n="e" s="T1959">măna </ts>
               <ts e="T1961" id="Seg_1283" n="e" s="T1960">amnal? </ts>
               <ts e="T1962" id="Seg_1285" n="e" s="T1961">Măn </ts>
               <ts e="T1964" id="Seg_1287" n="e" s="T1962">tolʼko </ts>
               <ts e="T1965" id="Seg_1289" n="e" s="T1964">uja </ts>
               <ts e="T1966" id="Seg_1291" n="e" s="T1965">naga. </ts>
               <ts e="T1967" id="Seg_1293" n="e" s="T1966">Da </ts>
               <ts e="T1968" id="Seg_1295" n="e" s="T1967">dʼaktə </ts>
               <ts e="T1969" id="Seg_1297" n="e" s="T1968">molaːmbiam. </ts>
               <ts e="T1970" id="Seg_1299" n="e" s="T1969">Kanaʔ </ts>
               <ts e="T1971" id="Seg_1301" n="e" s="T1970">da </ts>
               <ts e="T1972" id="Seg_1303" n="e" s="T1971">uja </ts>
               <ts e="T1973" id="Seg_1305" n="e" s="T1972">dettə </ts>
               <ts e="T1974" id="Seg_1307" n="e" s="T1973">inen </ts>
               <ts e="T1975" id="Seg_1309" n="e" s="T1974">măna". </ts>
               <ts e="T1976" id="Seg_1311" n="e" s="T1975">Da </ts>
               <ts e="T1977" id="Seg_1313" n="e" s="T1976">kambi, </ts>
               <ts e="T1978" id="Seg_1315" n="e" s="T1977">deʔpi </ts>
               <ts e="T1979" id="Seg_1317" n="e" s="T1978">uja </ts>
               <ts e="T1980" id="Seg_1319" n="e" s="T1979">inen. </ts>
               <ts e="T1981" id="Seg_1321" n="e" s="T1980">Dĭ </ts>
               <ts e="T1982" id="Seg_1323" n="e" s="T1981">ambi. </ts>
               <ts e="T1983" id="Seg_1325" n="e" s="T1982">(Dĭgəttə </ts>
               <ts e="T1985" id="Seg_1327" n="e" s="T1983">măndə: </ts>
               <ts e="T1986" id="Seg_1329" n="e" s="T1985">Šobi) </ts>
               <ts e="T1987" id="Seg_1331" n="e" s="T1986">Šide </ts>
               <ts e="T1988" id="Seg_1333" n="e" s="T1987">dʼala </ts>
               <ts e="T0" id="Seg_1335" n="e" s="T1988">kalla </ts>
               <ts e="T1989" id="Seg_1337" n="e" s="T0">dʼürbiʔi, </ts>
               <ts e="T1990" id="Seg_1339" n="e" s="T1989">dĭ </ts>
               <ts e="T1991" id="Seg_1341" n="e" s="T1990">šobi </ts>
               <ts e="T1992" id="Seg_1343" n="e" s="T1991">dĭʔnə. </ts>
               <ts e="T1993" id="Seg_1345" n="e" s="T1992">"No, </ts>
               <ts e="T1994" id="Seg_1347" n="e" s="T1993">ĭmbi, </ts>
               <ts e="T1995" id="Seg_1349" n="e" s="T1994">iššo </ts>
               <ts e="T1996" id="Seg_1351" n="e" s="T1995">deʔtə </ts>
               <ts e="T1997" id="Seg_1353" n="e" s="T1996">măna </ts>
               <ts e="T1998" id="Seg_1355" n="e" s="T1997">ularən </ts>
               <ts e="T1999" id="Seg_1357" n="e" s="T1998">uja". </ts>
               <ts e="T2000" id="Seg_1359" n="e" s="T1999">Dĭ </ts>
               <ts e="T2001" id="Seg_1361" n="e" s="T2000">kambi, </ts>
               <ts e="T2002" id="Seg_1363" n="e" s="T2001">ular </ts>
               <ts e="T2003" id="Seg_1365" n="e" s="T2002">dĭʔnə </ts>
               <ts e="T2004" id="Seg_1367" n="e" s="T2003">deʔpi. </ts>
               <ts e="T2005" id="Seg_1369" n="e" s="T2004">Dĭ </ts>
               <ts e="T2006" id="Seg_1371" n="e" s="T2005">amnuʔpi </ts>
               <ts e="T2007" id="Seg_1373" n="e" s="T2006">dĭ </ts>
               <ts e="T2008" id="Seg_1375" n="e" s="T2007">ulardə. </ts>
               <ts e="T2009" id="Seg_1377" n="e" s="T2008">Dĭgəttə </ts>
               <ts e="T2010" id="Seg_1379" n="e" s="T2009">bazoʔ </ts>
               <ts e="T2011" id="Seg_1381" n="e" s="T2010">šobi </ts>
               <ts e="T2012" id="Seg_1383" n="e" s="T2011">volk. </ts>
               <ts e="T2013" id="Seg_1385" n="e" s="T2012">"Ĭmbi, </ts>
               <ts e="T2014" id="Seg_1387" n="e" s="T2013">jakšə </ts>
               <ts e="T2015" id="Seg_1389" n="e" s="T2014">molaːmbiam?" </ts>
               <ts e="T2016" id="Seg_1391" n="e" s="T2015">"Dʼok, </ts>
               <ts e="T2017" id="Seg_1393" n="e" s="T2016">iššo </ts>
               <ts e="T2018" id="Seg_1395" n="e" s="T2017">šoškan </ts>
               <ts e="T2019" id="Seg_1397" n="e" s="T2018">uja </ts>
               <ts e="T2020" id="Seg_1399" n="e" s="T2019">detləl! </ts>
               <ts e="T2021" id="Seg_1401" n="e" s="T2020">Dĭgəttə </ts>
               <ts e="T2022" id="Seg_1403" n="e" s="T2021">măn </ts>
               <ts e="T2023" id="Seg_1405" n="e" s="T2022">jakšə </ts>
               <ts e="T2024" id="Seg_1407" n="e" s="T2023">molam". </ts>
               <ts e="T2025" id="Seg_1409" n="e" s="T2024">Dĭ </ts>
               <ts e="T2026" id="Seg_1411" n="e" s="T2025">šoškabə </ts>
               <ts e="T2027" id="Seg_1413" n="e" s="T2026">deʔpi. </ts>
               <ts e="T2028" id="Seg_1415" n="e" s="T2027">Dĭ </ts>
               <ts e="T2029" id="Seg_1417" n="e" s="T2028">men </ts>
               <ts e="T2030" id="Seg_1419" n="e" s="T2029">amluʔpi. </ts>
               <ts e="T2031" id="Seg_1421" n="e" s="T2030">Dĭgəttə </ts>
               <ts e="T2032" id="Seg_1423" n="e" s="T2031">šobi. </ts>
               <ts e="T2033" id="Seg_1425" n="e" s="T2032">Dĭgəttə </ts>
               <ts e="T2034" id="Seg_1427" n="e" s="T2033">davaj </ts>
               <ts e="T2035" id="Seg_1429" n="e" s="T2034">(m-) </ts>
               <ts e="T2036" id="Seg_1431" n="e" s="T2035">men </ts>
               <ts e="T2037" id="Seg_1433" n="e" s="T2036">dĭm </ts>
               <ts e="T2038" id="Seg_1435" n="e" s="T2037">amnosʼtə. </ts>
               <ts e="T2039" id="Seg_1437" n="e" s="T2038">Dʼabərobiʔi, </ts>
               <ts e="T2040" id="Seg_1439" n="e" s="T2039">dʼabərobiʔi </ts>
               <ts e="T2041" id="Seg_1441" n="e" s="T2040">dak </ts>
               <ts e="T2042" id="Seg_1443" n="e" s="T2041">volk </ts>
               <ts e="T2043" id="Seg_1445" n="e" s="T2042">nuʔməluʔpi </ts>
               <ts e="T2044" id="Seg_1447" n="e" s="T2043">(dĭg- </ts>
               <ts e="T2045" id="Seg_1449" n="e" s="T2044">dĭg- </ts>
               <ts e="T2046" id="Seg_1451" n="e" s="T2045">dĭg-) </ts>
               <ts e="T2047" id="Seg_1453" n="e" s="T2046">dĭgəʔ. </ts>
               <ts e="T2048" id="Seg_1455" n="e" s="T2047">I </ts>
               <ts e="T2049" id="Seg_1457" n="e" s="T2048">amnolaʔbə </ts>
               <ts e="T2050" id="Seg_1459" n="e" s="T2049">kustə </ts>
               <ts e="T2051" id="Seg_1461" n="e" s="T2050">toːndə </ts>
               <ts e="T2052" id="Seg_1463" n="e" s="T2051">da… </ts>
               <ts e="T2053" id="Seg_1465" n="e" s="T2052">Bostə </ts>
               <ts e="T2054" id="Seg_1467" n="e" s="T2053">kem </ts>
               <ts e="T2055" id="Seg_1469" n="e" s="T2054">šĭketsiʔ </ts>
               <ts e="T2056" id="Seg_1471" n="e" s="T2055">amnaʔbə. </ts>
               <ts e="T2057" id="Seg_1473" n="e" s="T2056">Da </ts>
               <ts e="T2058" id="Seg_1475" n="e" s="T2057">kudonzlaʔbə </ts>
               <ts e="T2059" id="Seg_1477" n="e" s="T2058">(to) </ts>
               <ts e="T2060" id="Seg_1479" n="e" s="T2059">(men=) </ts>
               <ts e="T2061" id="Seg_1481" n="e" s="T2060">mendə. </ts>
               <ts e="T2062" id="Seg_1483" n="e" s="T2061">"Tüj </ts>
               <ts e="T2063" id="Seg_1485" n="e" s="T2062">(šindənədə= </ts>
               <ts e="T2064" id="Seg_1487" n="e" s="T2063">šindin-) </ts>
               <ts e="T2065" id="Seg_1489" n="e" s="T2064">šindəmdə </ts>
               <ts e="T2066" id="Seg_1491" n="e" s="T2065">ej </ts>
               <ts e="T2067" id="Seg_1493" n="e" s="T2066">ajirlam! </ts>
               <ts e="T2068" id="Seg_1495" n="e" s="T2067">(Šindi=) </ts>
               <ts e="T2069" id="Seg_1497" n="e" s="T2068">Bar </ts>
               <ts e="T2070" id="Seg_1499" n="e" s="T2069">amnuʔlam!" </ts>
               <ts e="T2071" id="Seg_1501" n="e" s="T2070">Kabarləj. </ts>
               <ts e="T2072" id="Seg_1503" n="e" s="T2071">Dĭgəttə </ts>
               <ts e="T2073" id="Seg_1505" n="e" s="T2072">dĭ </ts>
               <ts e="T2074" id="Seg_1507" n="e" s="T2073">kandəlaʔbə. </ts>
               <ts e="T2075" id="Seg_1509" n="e" s="T2074">Poʔto </ts>
               <ts e="T2076" id="Seg_1511" n="e" s="T2075">nulaʔbə. </ts>
               <ts e="T2077" id="Seg_1513" n="e" s="T2076">"(Măn=) </ts>
               <ts e="T2078" id="Seg_1515" n="e" s="T2077">Poʔto, </ts>
               <ts e="T2079" id="Seg_1517" n="e" s="T2078">poʔto, </ts>
               <ts e="T2080" id="Seg_1519" n="e" s="T2079">măn </ts>
               <ts e="T2081" id="Seg_1521" n="e" s="T2080">tănan </ts>
               <ts e="T2082" id="Seg_1523" n="e" s="T2081">amnam!" </ts>
               <ts e="T2083" id="Seg_1525" n="e" s="T2082">"Iʔ </ts>
               <ts e="T2084" id="Seg_1527" n="e" s="T2083">amaʔ </ts>
               <ts e="T2085" id="Seg_1529" n="e" s="T2084">măna! </ts>
               <ts e="T2086" id="Seg_1531" n="e" s="T2085">(Tăn=) </ts>
               <ts e="T2087" id="Seg_1533" n="e" s="T2086">Măn </ts>
               <ts e="T2088" id="Seg_1535" n="e" s="T2087">nʼuʔdən, </ts>
               <ts e="T2089" id="Seg_1537" n="e" s="T2088">a </ts>
               <ts e="T2090" id="Seg_1539" n="e" s="T2089">(tăn=) </ts>
               <ts e="T2091" id="Seg_1541" n="e" s="T2090">dĭn </ts>
               <ts e="T2092" id="Seg_1543" n="e" s="T2091">(măn= </ts>
               <ts e="T2093" id="Seg_1545" n="e" s="T2092">kaj-) </ts>
               <ts e="T2094" id="Seg_1547" n="e" s="T2093">aŋdə </ts>
               <ts e="T2095" id="Seg_1549" n="e" s="T2094">(kajdə=) </ts>
               <ts e="T2096" id="Seg_1551" n="e" s="T2095">kardə </ts>
               <ts e="T2097" id="Seg_1553" n="e" s="T2096">(ăndə-) </ts>
               <ts e="T2098" id="Seg_1555" n="e" s="T2097">aŋdə. </ts>
               <ts e="T2099" id="Seg_1557" n="e" s="T2098">A </ts>
               <ts e="T2100" id="Seg_1559" n="e" s="T2099">măn </ts>
               <ts e="T2101" id="Seg_1561" n="e" s="T2100">nuʔməluʔpim. </ts>
               <ts e="T2102" id="Seg_1563" n="e" s="T2101">I </ts>
               <ts e="T2103" id="Seg_1565" n="e" s="T2102">prʼamă </ts>
               <ts e="T2104" id="Seg_1567" n="e" s="T2103">tăn </ts>
               <ts e="T2105" id="Seg_1569" n="e" s="T2104">aŋgəndə </ts>
               <ts e="T2106" id="Seg_1571" n="e" s="T2105">suʔmiluʔləm". </ts>
               <ts e="T2107" id="Seg_1573" n="e" s="T2106">Dĭgəttə </ts>
               <ts e="T2108" id="Seg_1575" n="e" s="T2107">kozʼol </ts>
               <ts e="T2109" id="Seg_1577" n="e" s="T2108">dĭ </ts>
               <ts e="T2110" id="Seg_1579" n="e" s="T2109">kak </ts>
               <ts e="T2111" id="Seg_1581" n="e" s="T2110">nuʔmiluʔpi, </ts>
               <ts e="T2112" id="Seg_1583" n="e" s="T2111">dĭ </ts>
               <ts e="T2113" id="Seg_1585" n="e" s="T2112">dĭm </ts>
               <ts e="T2114" id="Seg_1587" n="e" s="T2113">ulundə </ts>
               <ts e="T2115" id="Seg_1589" n="e" s="T2114">kak </ts>
               <ts e="T2116" id="Seg_1591" n="e" s="T2115">toʔnarluʔpi. </ts>
               <ts e="T2117" id="Seg_1593" n="e" s="T2116">A </ts>
               <ts e="T2118" id="Seg_1595" n="e" s="T2117">dĭ </ts>
               <ts e="T2119" id="Seg_1597" n="e" s="T2118">saʔməluʔpi </ts>
               <ts e="T2120" id="Seg_1599" n="e" s="T2119">i </ts>
               <ts e="T2121" id="Seg_1601" n="e" s="T2120">(aʔpi) </ts>
               <ts e="T2122" id="Seg_1603" n="e" s="T2121">ej </ts>
               <ts e="T2123" id="Seg_1605" n="e" s="T2122">kübi, </ts>
               <ts e="T2124" id="Seg_1607" n="e" s="T2123">a </ts>
               <ts e="T2125" id="Seg_1609" n="e" s="T2124">poʔto </ts>
               <ts e="T2126" id="Seg_1611" n="e" s="T2125">nuʔməluʔpi. </ts>
               <ts e="T2127" id="Seg_1613" n="e" s="T2126">Dĭ </ts>
               <ts e="T2128" id="Seg_1615" n="e" s="T2127">uʔbdəbi. </ts>
               <ts e="T2129" id="Seg_1617" n="e" s="T2128">"Ambiam </ts>
               <ts e="T2130" id="Seg_1619" n="e" s="T2129">măn </ts>
               <ts e="T2131" id="Seg_1621" n="e" s="T2130">poʔto </ts>
               <ts e="T2132" id="Seg_1623" n="e" s="T2131">alʼi </ts>
               <ts e="T2133" id="Seg_1625" n="e" s="T2132">ej </ts>
               <ts e="T2134" id="Seg_1627" n="e" s="T2133">ambiam? </ts>
               <ts e="T2135" id="Seg_1629" n="e" s="T2134">Ambiam </ts>
               <ts e="T2136" id="Seg_1631" n="e" s="T2135">bɨ </ts>
               <ts e="T2137" id="Seg_1633" n="e" s="T2136">dak </ts>
               <ts e="T2138" id="Seg_1635" n="e" s="T2137">măn </ts>
               <ts e="T2139" id="Seg_1637" n="e" s="T2138">nanəm </ts>
               <ts e="T2140" id="Seg_1639" n="e" s="T2139">bɨ </ts>
               <ts e="T2141" id="Seg_1641" n="e" s="T2140">urgo </ts>
               <ts e="T2142" id="Seg_1643" n="e" s="T2141">ibi. </ts>
               <ts e="T2143" id="Seg_1645" n="e" s="T2142">Naverno, </ts>
               <ts e="T2144" id="Seg_1647" n="e" s="T2143">ej </ts>
               <ts e="T2145" id="Seg_1649" n="e" s="T2144">ambiam". </ts>
               <ts e="T2146" id="Seg_1651" n="e" s="T2145">Kambi </ts>
               <ts e="T2147" id="Seg_1653" n="e" s="T2146">turazaŋdə, </ts>
               <ts e="T2148" id="Seg_1655" n="e" s="T2147">kuliot, </ts>
               <ts e="T2149" id="Seg_1657" n="e" s="T2148">šoška </ts>
               <ts e="T2150" id="Seg_1659" n="e" s="T2149">mĭnge. </ts>
               <ts e="T2151" id="Seg_1661" n="e" s="T2150">Üdʼügeʔi </ts>
               <ts e="T2152" id="Seg_1663" n="e" s="T2151">šoškaʔi. </ts>
               <ts e="T2153" id="Seg_1665" n="e" s="T2152">Xatʼel </ts>
               <ts e="T2154" id="Seg_1667" n="e" s="T2153">kabarzittə </ts>
               <ts e="T2155" id="Seg_1669" n="e" s="T2154">onʼiʔ </ts>
               <ts e="T2156" id="Seg_1671" n="e" s="T2155">üdʼüge </ts>
               <ts e="T2157" id="Seg_1673" n="e" s="T2156">šoška. </ts>
               <ts e="T2158" id="Seg_1675" n="e" s="T2157">Šoška </ts>
               <ts e="T2159" id="Seg_1677" n="e" s="T2158">măndə:" </ts>
               <ts e="T2160" id="Seg_1679" n="e" s="T2159">Iʔ </ts>
               <ts e="T2161" id="Seg_1681" n="e" s="T2160">kabarəʔ, </ts>
               <ts e="T2162" id="Seg_1683" n="e" s="T2161">dĭzeŋ </ts>
               <ts e="T2163" id="Seg_1685" n="e" s="T2162">üdʼügeʔi. </ts>
               <ts e="T2164" id="Seg_1687" n="e" s="T2163">Kanžəbəj </ts>
               <ts e="T2165" id="Seg_1689" n="e" s="T2164">bünə </ts>
               <ts e="T2166" id="Seg_1691" n="e" s="T2165">da </ts>
               <ts e="T2167" id="Seg_1693" n="e" s="T2166">(dĭziŋ-) </ts>
               <ts e="T2168" id="Seg_1695" n="e" s="T2167">dĭzem </ts>
               <ts e="T2169" id="Seg_1697" n="e" s="T2168">(kros-) </ts>
               <ts e="T2170" id="Seg_1699" n="e" s="T2169">krossʼtə". </ts>
               <ts e="T2171" id="Seg_1701" n="e" s="T2170">Dĭgəttə </ts>
               <ts e="T2172" id="Seg_1703" n="e" s="T2171">šobiʔi:" </ts>
               <ts e="T2173" id="Seg_1705" n="e" s="T2172">Tăn </ts>
               <ts e="T2174" id="Seg_1707" n="e" s="T2173">nuʔ </ts>
               <ts e="T2175" id="Seg_1709" n="e" s="T2174">dĭn, </ts>
               <ts e="T2176" id="Seg_1711" n="e" s="T2175">a </ts>
               <ts e="T2177" id="Seg_1713" n="e" s="T2176">măn </ts>
               <ts e="T2178" id="Seg_1715" n="e" s="T2177">döbər". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1889" id="Seg_1716" s="T1888">PKZ_196X_StupidWolf_flk.001 (001)</ta>
            <ta e="T1894" id="Seg_1717" s="T1889">PKZ_196X_StupidWolf_flk.002 (002)</ta>
            <ta e="T1903" id="Seg_1718" s="T1894">PKZ_196X_StupidWolf_flk.003 (003)</ta>
            <ta e="T1906" id="Seg_1719" s="T1903">PKZ_196X_StupidWolf_flk.004 (004)</ta>
            <ta e="T1908" id="Seg_1720" s="T1906">PKZ_196X_StupidWolf_flk.005 (005)</ta>
            <ta e="T1912" id="Seg_1721" s="T1908">PKZ_196X_StupidWolf_flk.006 (006)</ta>
            <ta e="T1916" id="Seg_1722" s="T1912">PKZ_196X_StupidWolf_flk.007 (007)</ta>
            <ta e="T1920" id="Seg_1723" s="T1916">PKZ_196X_StupidWolf_flk.008 (008)</ta>
            <ta e="T1921" id="Seg_1724" s="T1920">PKZ_196X_StupidWolf_flk.009 (009)</ta>
            <ta e="T1924" id="Seg_1725" s="T1921">PKZ_196X_StupidWolf_flk.010 (010)</ta>
            <ta e="T1927" id="Seg_1726" s="T1924">PKZ_196X_StupidWolf_flk.011 (011)</ta>
            <ta e="T1931" id="Seg_1727" s="T1927">PKZ_196X_StupidWolf_flk.012 (012)</ta>
            <ta e="T1937" id="Seg_1728" s="T1931">PKZ_196X_StupidWolf_flk.013 (013)</ta>
            <ta e="T1941" id="Seg_1729" s="T1937">PKZ_196X_StupidWolf_flk.014 (014)</ta>
            <ta e="T1944" id="Seg_1730" s="T1941">PKZ_196X_StupidWolf_flk.015 (015)</ta>
            <ta e="T1947" id="Seg_1731" s="T1944">PKZ_196X_StupidWolf_flk.016 (016)</ta>
            <ta e="T1953" id="Seg_1732" s="T1947">PKZ_196X_StupidWolf_flk.017 (017)</ta>
            <ta e="T1956" id="Seg_1733" s="T1953">PKZ_196X_StupidWolf_flk.018 (018)</ta>
            <ta e="T1961" id="Seg_1734" s="T1956">PKZ_196X_StupidWolf_flk.019 (019)</ta>
            <ta e="T1966" id="Seg_1735" s="T1961">PKZ_196X_StupidWolf_flk.020 (020)</ta>
            <ta e="T1969" id="Seg_1736" s="T1966">PKZ_196X_StupidWolf_flk.021 (021)</ta>
            <ta e="T1975" id="Seg_1737" s="T1969">PKZ_196X_StupidWolf_flk.022 (022)</ta>
            <ta e="T1980" id="Seg_1738" s="T1975">PKZ_196X_StupidWolf_flk.023 (023)</ta>
            <ta e="T1982" id="Seg_1739" s="T1980">PKZ_196X_StupidWolf_flk.024 (024)</ta>
            <ta e="T1992" id="Seg_1740" s="T1982">PKZ_196X_StupidWolf_flk.025 (025)</ta>
            <ta e="T1999" id="Seg_1741" s="T1992">PKZ_196X_StupidWolf_flk.026 (026)</ta>
            <ta e="T2004" id="Seg_1742" s="T1999">PKZ_196X_StupidWolf_flk.027 (027)</ta>
            <ta e="T2008" id="Seg_1743" s="T2004">PKZ_196X_StupidWolf_flk.028 (028)</ta>
            <ta e="T2012" id="Seg_1744" s="T2008">PKZ_196X_StupidWolf_flk.029 (029)</ta>
            <ta e="T2015" id="Seg_1745" s="T2012">PKZ_196X_StupidWolf_flk.030 (030)</ta>
            <ta e="T2020" id="Seg_1746" s="T2015">PKZ_196X_StupidWolf_flk.031 (031)</ta>
            <ta e="T2024" id="Seg_1747" s="T2020">PKZ_196X_StupidWolf_flk.032 (032)</ta>
            <ta e="T2027" id="Seg_1748" s="T2024">PKZ_196X_StupidWolf_flk.033 (033)</ta>
            <ta e="T2030" id="Seg_1749" s="T2027">PKZ_196X_StupidWolf_flk.034 (034)</ta>
            <ta e="T2032" id="Seg_1750" s="T2030">PKZ_196X_StupidWolf_flk.035 (035)</ta>
            <ta e="T2038" id="Seg_1751" s="T2032">PKZ_196X_StupidWolf_flk.036 (036)</ta>
            <ta e="T2047" id="Seg_1752" s="T2038">PKZ_196X_StupidWolf_flk.037 (037)</ta>
            <ta e="T2052" id="Seg_1753" s="T2047">PKZ_196X_StupidWolf_flk.038 (038)</ta>
            <ta e="T2056" id="Seg_1754" s="T2052">PKZ_196X_StupidWolf_flk.039 (039)</ta>
            <ta e="T2061" id="Seg_1755" s="T2056">PKZ_196X_StupidWolf_flk.040 (040)</ta>
            <ta e="T2067" id="Seg_1756" s="T2061">PKZ_196X_StupidWolf_flk.041 (041)</ta>
            <ta e="T2070" id="Seg_1757" s="T2067">PKZ_196X_StupidWolf_flk.042 (042)</ta>
            <ta e="T2071" id="Seg_1758" s="T2070">PKZ_196X_StupidWolf_flk.043 (043)</ta>
            <ta e="T2074" id="Seg_1759" s="T2071">PKZ_196X_StupidWolf_flk.044 (044)</ta>
            <ta e="T2076" id="Seg_1760" s="T2074">PKZ_196X_StupidWolf_flk.045 (045)</ta>
            <ta e="T2082" id="Seg_1761" s="T2076">PKZ_196X_StupidWolf_flk.046 (046)</ta>
            <ta e="T2085" id="Seg_1762" s="T2082">PKZ_196X_StupidWolf_flk.047 (047)</ta>
            <ta e="T2098" id="Seg_1763" s="T2085">PKZ_196X_StupidWolf_flk.048 (048)</ta>
            <ta e="T2101" id="Seg_1764" s="T2098">PKZ_196X_StupidWolf_flk.049 (049)</ta>
            <ta e="T2106" id="Seg_1765" s="T2101">PKZ_196X_StupidWolf_flk.050 (050)</ta>
            <ta e="T2116" id="Seg_1766" s="T2106">PKZ_196X_StupidWolf_flk.051 (051)</ta>
            <ta e="T2126" id="Seg_1767" s="T2116">PKZ_196X_StupidWolf_flk.052 (052)</ta>
            <ta e="T2128" id="Seg_1768" s="T2126">PKZ_196X_StupidWolf_flk.053 (053)</ta>
            <ta e="T2134" id="Seg_1769" s="T2128">PKZ_196X_StupidWolf_flk.054 (054)</ta>
            <ta e="T2142" id="Seg_1770" s="T2134">PKZ_196X_StupidWolf_flk.055 (055)</ta>
            <ta e="T2145" id="Seg_1771" s="T2142">PKZ_196X_StupidWolf_flk.056 (056)</ta>
            <ta e="T2150" id="Seg_1772" s="T2145">PKZ_196X_StupidWolf_flk.057 (057)</ta>
            <ta e="T2152" id="Seg_1773" s="T2150">PKZ_196X_StupidWolf_flk.058 (058)</ta>
            <ta e="T2157" id="Seg_1774" s="T2152">PKZ_196X_StupidWolf_flk.059 (059)</ta>
            <ta e="T2163" id="Seg_1775" s="T2157">PKZ_196X_StupidWolf_flk.060 (060)</ta>
            <ta e="T2170" id="Seg_1776" s="T2163">PKZ_196X_StupidWolf_flk.061 (061)</ta>
            <ta e="T2178" id="Seg_1777" s="T2170">PKZ_196X_StupidWolf_flk.062 (062)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1889" id="Seg_1778" s="T1888">((DMG)). </ta>
            <ta e="T1894" id="Seg_1779" s="T1889">Onʼiʔ kuzan (bɨl-) ibi men. </ta>
            <ta e="T1903" id="Seg_1780" s="T1894">Dĭ üge dĭn bar (ulazaŋ-) ularazaŋdə ej mĭbi volkdə. </ta>
            <ta e="T1906" id="Seg_1781" s="T1903">Dĭʔnə üge kabažərbi. </ta>
            <ta e="T1908" id="Seg_1782" s="T1906">Dĭgəttə… </ta>
            <ta e="T1912" id="Seg_1783" s="T1908">Ne znaju, kak… </ta>
            <ta e="T1916" id="Seg_1784" s="T1912">Dĭgəttə (dʼiʔ-) dʼiʔtə molaːmbi. </ta>
            <ta e="T1920" id="Seg_1785" s="T1916">Dĭ (dĭʔnə=) dĭm ibi. </ta>
            <ta e="T1921" id="Seg_1786" s="T1920">Sarobi. </ta>
            <ta e="T1924" id="Seg_1787" s="T1921">I kumbi dʼijenə. </ta>
            <ta e="T1927" id="Seg_1788" s="T1924">Xatʼel dĭm pʼaŋdəsʼtə. </ta>
            <ta e="T1931" id="Seg_1789" s="T1927">(Dĭn) bar tʼorlaʔ amnolaʔbə. </ta>
            <ta e="T1937" id="Seg_1790" s="T1931">Dĭgəttə (dĭm=) dĭm (sʼ-) sarbi (panə). </ta>
            <ta e="T1941" id="Seg_1791" s="T1937">Da bostə kalla dʼürbi. </ta>
            <ta e="T1944" id="Seg_1792" s="T1941">Dĭgəttə volkdə šonəga. </ta>
            <ta e="T1947" id="Seg_1793" s="T1944">"Tăn dön amnolaʔbəl. </ta>
            <ta e="T1953" id="Seg_1794" s="T1947">Măna ej öʔlubiel, kamən măn šobiam. </ta>
            <ta e="T1956" id="Seg_1795" s="T1953">Tüjö tănan amluʔlam". </ta>
            <ta e="T1961" id="Seg_1796" s="T1956">"Da ĭmbi tăn măna amnal? </ta>
            <ta e="T1966" id="Seg_1797" s="T1961">Măn tolʼko ((PAUSE)) uja naga. </ta>
            <ta e="T1969" id="Seg_1798" s="T1966">Da dʼaktə molaːmbiam. </ta>
            <ta e="T1975" id="Seg_1799" s="T1969">Kanaʔ da uja dettə inen măna". </ta>
            <ta e="T1980" id="Seg_1800" s="T1975">Da kambi, deʔpi uja inen. </ta>
            <ta e="T1982" id="Seg_1801" s="T1980">Dĭ ambi. </ta>
            <ta e="T1992" id="Seg_1802" s="T1982">(Dĭgəttə măndə … Šobi) Šide dʼala kalla dʼürbiʔi, dĭ šobi dĭʔnə. </ta>
            <ta e="T1999" id="Seg_1803" s="T1992">"No, ĭmbi, iššo deʔtə măna ularən uja". </ta>
            <ta e="T2004" id="Seg_1804" s="T1999">Dĭ kambi, ular dĭʔnə deʔpi. </ta>
            <ta e="T2008" id="Seg_1805" s="T2004">Dĭ amnuʔpi dĭ ulardə. </ta>
            <ta e="T2012" id="Seg_1806" s="T2008">Dĭgəttə bazoʔ šobi volk. </ta>
            <ta e="T2015" id="Seg_1807" s="T2012">"Ĭmbi, jakšə molaːmbiam?" </ta>
            <ta e="T2020" id="Seg_1808" s="T2015">"Dʼok, iššo šoškan uja detləl! </ta>
            <ta e="T2024" id="Seg_1809" s="T2020">Dĭgəttə măn jakšə molam". </ta>
            <ta e="T2027" id="Seg_1810" s="T2024">Dĭ šoškabə deʔpi. </ta>
            <ta e="T2030" id="Seg_1811" s="T2027">Dĭ men amluʔpi. </ta>
            <ta e="T2032" id="Seg_1812" s="T2030">Dĭgəttə šobi. </ta>
            <ta e="T2038" id="Seg_1813" s="T2032">Dĭgəttə davaj (m-) men dĭm amnosʼtə. </ta>
            <ta e="T2047" id="Seg_1814" s="T2038">Dʼabərobiʔi, dʼabərobiʔi dak volk nuʔməluʔpi (dĭg- dĭg- dĭg-) dĭgəʔ. </ta>
            <ta e="T2052" id="Seg_1815" s="T2047">I amnolaʔbə kustə toːndə da… </ta>
            <ta e="T2056" id="Seg_1816" s="T2052">Bostə kem šĭketsiʔ amnaʔbə. </ta>
            <ta e="T2061" id="Seg_1817" s="T2056">Da kudonzlaʔbə (to) (men=) mendə. </ta>
            <ta e="T2067" id="Seg_1818" s="T2061">"Tüj (šindənədə= šindin-) šindəmdə ej ajirlam! </ta>
            <ta e="T2070" id="Seg_1819" s="T2067">(Šindi=) Bar amnuʔlam!" </ta>
            <ta e="T2071" id="Seg_1820" s="T2070">Kabarləj. </ta>
            <ta e="T2074" id="Seg_1821" s="T2071">Dĭgəttə dĭ kandəlaʔbə. </ta>
            <ta e="T2076" id="Seg_1822" s="T2074">Poʔto nulaʔbə. </ta>
            <ta e="T2082" id="Seg_1823" s="T2076">"(Măn=) Poʔto, poʔto, măn tănan amnam!" </ta>
            <ta e="T2085" id="Seg_1824" s="T2082">"Iʔ amaʔ măna! </ta>
            <ta e="T2098" id="Seg_1825" s="T2085">(Tăn=) Măn nʼuʔdən, a (tăn=) dĭn (măn= kaj-) aŋdə (kajdə=) kardə (ăndə-) aŋdə. </ta>
            <ta e="T2101" id="Seg_1826" s="T2098">A măn nuʔməluʔpim. </ta>
            <ta e="T2106" id="Seg_1827" s="T2101">I prʼamă tăn aŋgəndə suʔmiluʔləm". </ta>
            <ta e="T2116" id="Seg_1828" s="T2106">Dĭgəttə kozʼol dĭ kak nuʔmiluʔpi, dĭ dĭm ulundə kak toʔnarluʔpi. </ta>
            <ta e="T2126" id="Seg_1829" s="T2116">A dĭ saʔməluʔpi i (aʔpi) ej kübi, a poʔto nuʔməluʔpi. </ta>
            <ta e="T2128" id="Seg_1830" s="T2126">Dĭ uʔbdəbi. </ta>
            <ta e="T2134" id="Seg_1831" s="T2128">"Ambiam măn poʔto alʼi ej ambiam? </ta>
            <ta e="T2142" id="Seg_1832" s="T2134">Ambiam bɨ dak măn nanəm bɨ urgo ibi. </ta>
            <ta e="T2145" id="Seg_1833" s="T2142">Naverno, ej ambiam". </ta>
            <ta e="T2150" id="Seg_1834" s="T2145">Kambi turazaŋdə, kuliot, šoška mĭnge. </ta>
            <ta e="T2152" id="Seg_1835" s="T2150">Üdʼügeʔi šoškaʔi. </ta>
            <ta e="T2157" id="Seg_1836" s="T2152">Xatʼel kabarzittə onʼiʔ üdʼüge šoška. </ta>
            <ta e="T2163" id="Seg_1837" s="T2157">Šoška măndə:" Iʔ kabarəʔ, dĭzeŋ üdʼügeʔi. </ta>
            <ta e="T2170" id="Seg_1838" s="T2163">Kanžəbəj bünə da (dĭziŋ-) dĭzem (kros-) krossʼtə". </ta>
            <ta e="T2178" id="Seg_1839" s="T2170">Dĭgəttə šobiʔi:" Tăn nuʔ dĭn, a măn döbər". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1890" id="Seg_1840" s="T1889">onʼiʔ</ta>
            <ta e="T1891" id="Seg_1841" s="T1890">kuza-n</ta>
            <ta e="T1893" id="Seg_1842" s="T1892">i-bi</ta>
            <ta e="T1894" id="Seg_1843" s="T1893">men</ta>
            <ta e="T1895" id="Seg_1844" s="T1894">dĭ</ta>
            <ta e="T1896" id="Seg_1845" s="T1895">üge</ta>
            <ta e="T1897" id="Seg_1846" s="T1896">dĭ-n</ta>
            <ta e="T1898" id="Seg_1847" s="T1897">bar</ta>
            <ta e="T1900" id="Seg_1848" s="T1899">ulara-zaŋ-də</ta>
            <ta e="T1901" id="Seg_1849" s="T1900">ej</ta>
            <ta e="T1902" id="Seg_1850" s="T1901">mĭ-bi</ta>
            <ta e="T1903" id="Seg_1851" s="T1902">volk-də</ta>
            <ta e="T1904" id="Seg_1852" s="T1903">dĭʔ-nə</ta>
            <ta e="T1905" id="Seg_1853" s="T1904">üge</ta>
            <ta e="T1906" id="Seg_1854" s="T1905">kabažər-bi</ta>
            <ta e="T1908" id="Seg_1855" s="T1906">dĭgəttə</ta>
            <ta e="T1913" id="Seg_1856" s="T1912">dĭgəttə</ta>
            <ta e="T1915" id="Seg_1857" s="T1914">dʼiʔtə</ta>
            <ta e="T1916" id="Seg_1858" s="T1915">mo-laːm-bi</ta>
            <ta e="T1917" id="Seg_1859" s="T1916">dĭ</ta>
            <ta e="T1918" id="Seg_1860" s="T1917">dĭʔ-nə</ta>
            <ta e="T1919" id="Seg_1861" s="T1918">dĭ-m</ta>
            <ta e="T1920" id="Seg_1862" s="T1919">i-bi</ta>
            <ta e="T1921" id="Seg_1863" s="T1920">saro-bi</ta>
            <ta e="T1922" id="Seg_1864" s="T1921">i</ta>
            <ta e="T1923" id="Seg_1865" s="T1922">kum-bi</ta>
            <ta e="T1924" id="Seg_1866" s="T1923">dʼije-nə</ta>
            <ta e="T1925" id="Seg_1867" s="T1924">xatʼel</ta>
            <ta e="T1926" id="Seg_1868" s="T1925">dĭ-m</ta>
            <ta e="T1927" id="Seg_1869" s="T1926">pʼaŋdə-sʼtə</ta>
            <ta e="T1928" id="Seg_1870" s="T1927">dĭn</ta>
            <ta e="T1929" id="Seg_1871" s="T1928">bar</ta>
            <ta e="T1930" id="Seg_1872" s="T1929">tʼor-laʔ</ta>
            <ta e="T1931" id="Seg_1873" s="T1930">amno-laʔbə</ta>
            <ta e="T1932" id="Seg_1874" s="T1931">dĭgəttə</ta>
            <ta e="T1933" id="Seg_1875" s="T1932">dĭ-m</ta>
            <ta e="T1934" id="Seg_1876" s="T1933">dĭ-m</ta>
            <ta e="T1936" id="Seg_1877" s="T1935">sar-bi</ta>
            <ta e="T1937" id="Seg_1878" s="T1936">pa-nə</ta>
            <ta e="T1938" id="Seg_1879" s="T1937">da</ta>
            <ta e="T1939" id="Seg_1880" s="T1938">bos-tə</ta>
            <ta e="T1940" id="Seg_1881" s="T1939">kal-la</ta>
            <ta e="T1941" id="Seg_1882" s="T1940">dʼür-bi</ta>
            <ta e="T1942" id="Seg_1883" s="T1941">dĭgəttə</ta>
            <ta e="T1943" id="Seg_1884" s="T1942">volk-də</ta>
            <ta e="T1944" id="Seg_1885" s="T1943">šonə-ga</ta>
            <ta e="T1945" id="Seg_1886" s="T1944">tăn</ta>
            <ta e="T1946" id="Seg_1887" s="T1945">dön</ta>
            <ta e="T1947" id="Seg_1888" s="T1946">amno-laʔbə-l</ta>
            <ta e="T1948" id="Seg_1889" s="T1947">măna</ta>
            <ta e="T1949" id="Seg_1890" s="T1948">ej</ta>
            <ta e="T1950" id="Seg_1891" s="T1949">öʔlu-bie-l</ta>
            <ta e="T1951" id="Seg_1892" s="T1950">kamən</ta>
            <ta e="T1952" id="Seg_1893" s="T1951">măn</ta>
            <ta e="T1953" id="Seg_1894" s="T1952">šo-bia-m</ta>
            <ta e="T1954" id="Seg_1895" s="T1953">tüjö</ta>
            <ta e="T1955" id="Seg_1896" s="T1954">tănan</ta>
            <ta e="T1956" id="Seg_1897" s="T1955">am-luʔ-la-m</ta>
            <ta e="T1957" id="Seg_1898" s="T1956">da</ta>
            <ta e="T1958" id="Seg_1899" s="T1957">ĭmbi</ta>
            <ta e="T1959" id="Seg_1900" s="T1958">tăn</ta>
            <ta e="T1960" id="Seg_1901" s="T1959">măna</ta>
            <ta e="T1961" id="Seg_1902" s="T1960">am-na-l</ta>
            <ta e="T1962" id="Seg_1903" s="T1961">măn</ta>
            <ta e="T1964" id="Seg_1904" s="T1962">tolʼko</ta>
            <ta e="T1965" id="Seg_1905" s="T1964">uja</ta>
            <ta e="T1966" id="Seg_1906" s="T1965">naga</ta>
            <ta e="T1967" id="Seg_1907" s="T1966">da</ta>
            <ta e="T1968" id="Seg_1908" s="T1967">dʼaktə</ta>
            <ta e="T1969" id="Seg_1909" s="T1968">mo-laːm-bia-m</ta>
            <ta e="T1970" id="Seg_1910" s="T1969">kan-a-ʔ</ta>
            <ta e="T1971" id="Seg_1911" s="T1970">da</ta>
            <ta e="T1972" id="Seg_1912" s="T1971">uja</ta>
            <ta e="T1973" id="Seg_1913" s="T1972">det-tə</ta>
            <ta e="T1974" id="Seg_1914" s="T1973">ine-n</ta>
            <ta e="T1975" id="Seg_1915" s="T1974">măna</ta>
            <ta e="T1976" id="Seg_1916" s="T1975">da</ta>
            <ta e="T1977" id="Seg_1917" s="T1976">kam-bi</ta>
            <ta e="T1978" id="Seg_1918" s="T1977">deʔ-pi</ta>
            <ta e="T1979" id="Seg_1919" s="T1978">uja</ta>
            <ta e="T1980" id="Seg_1920" s="T1979">ine-n</ta>
            <ta e="T1981" id="Seg_1921" s="T1980">dĭ</ta>
            <ta e="T1982" id="Seg_1922" s="T1981">am-bi</ta>
            <ta e="T1983" id="Seg_1923" s="T1982">dĭgəttə</ta>
            <ta e="T1985" id="Seg_1924" s="T1983">măn-də</ta>
            <ta e="T1986" id="Seg_1925" s="T1985">šo-bi</ta>
            <ta e="T1987" id="Seg_1926" s="T1986">šide</ta>
            <ta e="T1988" id="Seg_1927" s="T1987">dʼala</ta>
            <ta e="T0" id="Seg_1928" s="T1988">kal-la</ta>
            <ta e="T1989" id="Seg_1929" s="T0">dʼür-bi-ʔi</ta>
            <ta e="T1990" id="Seg_1930" s="T1989">dĭ</ta>
            <ta e="T1991" id="Seg_1931" s="T1990">šo-bi</ta>
            <ta e="T1992" id="Seg_1932" s="T1991">dĭʔ-nə</ta>
            <ta e="T1993" id="Seg_1933" s="T1992">no</ta>
            <ta e="T1994" id="Seg_1934" s="T1993">ĭmbi</ta>
            <ta e="T1995" id="Seg_1935" s="T1994">iššo</ta>
            <ta e="T1996" id="Seg_1936" s="T1995">deʔ-tə</ta>
            <ta e="T1997" id="Seg_1937" s="T1996">măna</ta>
            <ta e="T1998" id="Seg_1938" s="T1997">ular-ən</ta>
            <ta e="T1999" id="Seg_1939" s="T1998">uja</ta>
            <ta e="T2000" id="Seg_1940" s="T1999">dĭ</ta>
            <ta e="T2001" id="Seg_1941" s="T2000">kam-bi</ta>
            <ta e="T2002" id="Seg_1942" s="T2001">ular</ta>
            <ta e="T2003" id="Seg_1943" s="T2002">dĭʔ-nə</ta>
            <ta e="T2004" id="Seg_1944" s="T2003">deʔ-pi</ta>
            <ta e="T2005" id="Seg_1945" s="T2004">dĭ</ta>
            <ta e="T2006" id="Seg_1946" s="T2005">am-nuʔ-pi</ta>
            <ta e="T2007" id="Seg_1947" s="T2006">dĭ</ta>
            <ta e="T2008" id="Seg_1948" s="T2007">ular-də</ta>
            <ta e="T2009" id="Seg_1949" s="T2008">dĭgəttə</ta>
            <ta e="T2010" id="Seg_1950" s="T2009">bazoʔ</ta>
            <ta e="T2011" id="Seg_1951" s="T2010">šo-bi</ta>
            <ta e="T2012" id="Seg_1952" s="T2011">volk</ta>
            <ta e="T2013" id="Seg_1953" s="T2012">ĭmbi</ta>
            <ta e="T2014" id="Seg_1954" s="T2013">jakšə</ta>
            <ta e="T2015" id="Seg_1955" s="T2014">mo-laːm-bia-m</ta>
            <ta e="T2016" id="Seg_1956" s="T2015">dʼok</ta>
            <ta e="T2017" id="Seg_1957" s="T2016">iššo</ta>
            <ta e="T2018" id="Seg_1958" s="T2017">šoška-n</ta>
            <ta e="T2019" id="Seg_1959" s="T2018">uja</ta>
            <ta e="T2020" id="Seg_1960" s="T2019">det-lə-l</ta>
            <ta e="T2021" id="Seg_1961" s="T2020">dĭgəttə</ta>
            <ta e="T2022" id="Seg_1962" s="T2021">măn</ta>
            <ta e="T2023" id="Seg_1963" s="T2022">jakšə</ta>
            <ta e="T2024" id="Seg_1964" s="T2023">mo-la-m</ta>
            <ta e="T2025" id="Seg_1965" s="T2024">dĭ</ta>
            <ta e="T2026" id="Seg_1966" s="T2025">šoška-bə</ta>
            <ta e="T2027" id="Seg_1967" s="T2026">deʔ-pi</ta>
            <ta e="T2028" id="Seg_1968" s="T2027">dĭ</ta>
            <ta e="T2029" id="Seg_1969" s="T2028">men</ta>
            <ta e="T2030" id="Seg_1970" s="T2029">am-luʔ-pi</ta>
            <ta e="T2031" id="Seg_1971" s="T2030">dĭgəttə</ta>
            <ta e="T2032" id="Seg_1972" s="T2031">šo-bi</ta>
            <ta e="T2033" id="Seg_1973" s="T2032">dĭgəttə</ta>
            <ta e="T2034" id="Seg_1974" s="T2033">davaj</ta>
            <ta e="T2036" id="Seg_1975" s="T2035">men</ta>
            <ta e="T2037" id="Seg_1976" s="T2036">dĭ-m</ta>
            <ta e="T2038" id="Seg_1977" s="T2037">amno-sʼtə</ta>
            <ta e="T2039" id="Seg_1978" s="T2038">dʼabəro-bi-ʔi</ta>
            <ta e="T2040" id="Seg_1979" s="T2039">dʼabəro-bi-ʔi</ta>
            <ta e="T2041" id="Seg_1980" s="T2040">dak</ta>
            <ta e="T2042" id="Seg_1981" s="T2041">volk</ta>
            <ta e="T2043" id="Seg_1982" s="T2042">nuʔmə-luʔ-pi</ta>
            <ta e="T2047" id="Seg_1983" s="T2046">dĭ-gəʔ</ta>
            <ta e="T2048" id="Seg_1984" s="T2047">i</ta>
            <ta e="T2049" id="Seg_1985" s="T2048">amno-laʔbə</ta>
            <ta e="T2050" id="Seg_1986" s="T2049">kustə</ta>
            <ta e="T2051" id="Seg_1987" s="T2050">toː-ndə</ta>
            <ta e="T2052" id="Seg_1988" s="T2051">da</ta>
            <ta e="T2053" id="Seg_1989" s="T2052">bos-tə</ta>
            <ta e="T2054" id="Seg_1990" s="T2053">kem</ta>
            <ta e="T2055" id="Seg_1991" s="T2054">šĭke-t-siʔ</ta>
            <ta e="T2056" id="Seg_1992" s="T2055">am-naʔbə</ta>
            <ta e="T2057" id="Seg_1993" s="T2056">da</ta>
            <ta e="T2058" id="Seg_1994" s="T2057">kudo-nz-laʔbə</ta>
            <ta e="T2059" id="Seg_1995" s="T2058">to</ta>
            <ta e="T2060" id="Seg_1996" s="T2059">men</ta>
            <ta e="T2061" id="Seg_1997" s="T2060">men-də</ta>
            <ta e="T2062" id="Seg_1998" s="T2061">tüj</ta>
            <ta e="T2063" id="Seg_1999" s="T2062">šində-nə=də</ta>
            <ta e="T2065" id="Seg_2000" s="T2064">šində-m=də</ta>
            <ta e="T2066" id="Seg_2001" s="T2065">ej</ta>
            <ta e="T2067" id="Seg_2002" s="T2066">ajir-la-m</ta>
            <ta e="T2068" id="Seg_2003" s="T2067">šindi</ta>
            <ta e="T2069" id="Seg_2004" s="T2068">bar</ta>
            <ta e="T2070" id="Seg_2005" s="T2069">am-nuʔ-la-m</ta>
            <ta e="T2071" id="Seg_2006" s="T2070">kabarləj</ta>
            <ta e="T2072" id="Seg_2007" s="T2071">dĭgəttə</ta>
            <ta e="T2073" id="Seg_2008" s="T2072">dĭ</ta>
            <ta e="T2074" id="Seg_2009" s="T2073">kan-də-laʔbə</ta>
            <ta e="T2075" id="Seg_2010" s="T2074">poʔto</ta>
            <ta e="T2076" id="Seg_2011" s="T2075">nu-laʔbə</ta>
            <ta e="T2077" id="Seg_2012" s="T2076">măn</ta>
            <ta e="T2078" id="Seg_2013" s="T2077">poʔto</ta>
            <ta e="T2079" id="Seg_2014" s="T2078">poʔto</ta>
            <ta e="T2080" id="Seg_2015" s="T2079">măn</ta>
            <ta e="T2081" id="Seg_2016" s="T2080">tănan</ta>
            <ta e="T2082" id="Seg_2017" s="T2081">am-na-m</ta>
            <ta e="T2083" id="Seg_2018" s="T2082">i-ʔ</ta>
            <ta e="T2084" id="Seg_2019" s="T2083">am-a-ʔ</ta>
            <ta e="T2085" id="Seg_2020" s="T2084">măna</ta>
            <ta e="T2086" id="Seg_2021" s="T2085">tăn</ta>
            <ta e="T2087" id="Seg_2022" s="T2086">măn</ta>
            <ta e="T2088" id="Seg_2023" s="T2087">nʼuʔdə-n</ta>
            <ta e="T2089" id="Seg_2024" s="T2088">a</ta>
            <ta e="T2090" id="Seg_2025" s="T2089">tăn</ta>
            <ta e="T2091" id="Seg_2026" s="T2090">dĭn</ta>
            <ta e="T2092" id="Seg_2027" s="T2091">măn</ta>
            <ta e="T2094" id="Seg_2028" s="T2093">aŋ-də</ta>
            <ta e="T2095" id="Seg_2029" s="T2094">kaj-də</ta>
            <ta e="T2096" id="Seg_2030" s="T2095">kar-də</ta>
            <ta e="T2098" id="Seg_2031" s="T2097">aŋ-də</ta>
            <ta e="T2099" id="Seg_2032" s="T2098">a</ta>
            <ta e="T2100" id="Seg_2033" s="T2099">măn</ta>
            <ta e="T2101" id="Seg_2034" s="T2100">nuʔmə-luʔ-pi-m</ta>
            <ta e="T2102" id="Seg_2035" s="T2101">i</ta>
            <ta e="T2104" id="Seg_2036" s="T2103">tăn</ta>
            <ta e="T2105" id="Seg_2037" s="T2104">aŋ-gəndə</ta>
            <ta e="T2106" id="Seg_2038" s="T2105">suʔmi-luʔ-lə-m</ta>
            <ta e="T2107" id="Seg_2039" s="T2106">dĭgəttə</ta>
            <ta e="T2109" id="Seg_2040" s="T2108">dĭ</ta>
            <ta e="T2110" id="Seg_2041" s="T2109">kak</ta>
            <ta e="T2111" id="Seg_2042" s="T2110">nuʔmi-luʔ-pi</ta>
            <ta e="T2112" id="Seg_2043" s="T2111">dĭ</ta>
            <ta e="T2113" id="Seg_2044" s="T2112">dĭ-m</ta>
            <ta e="T2114" id="Seg_2045" s="T2113">ulu-ndə</ta>
            <ta e="T2115" id="Seg_2046" s="T2114">kak</ta>
            <ta e="T2116" id="Seg_2047" s="T2115">toʔ-nar-luʔ-pi</ta>
            <ta e="T2117" id="Seg_2048" s="T2116">a</ta>
            <ta e="T2118" id="Seg_2049" s="T2117">dĭ</ta>
            <ta e="T2119" id="Seg_2050" s="T2118">saʔmə-luʔ-pi</ta>
            <ta e="T2120" id="Seg_2051" s="T2119">i</ta>
            <ta e="T2121" id="Seg_2052" s="T2120">aʔpi</ta>
            <ta e="T2122" id="Seg_2053" s="T2121">ej</ta>
            <ta e="T2123" id="Seg_2054" s="T2122">kü-bi</ta>
            <ta e="T2124" id="Seg_2055" s="T2123">a</ta>
            <ta e="T2125" id="Seg_2056" s="T2124">poʔto</ta>
            <ta e="T2126" id="Seg_2057" s="T2125">nuʔmə-luʔ-pi</ta>
            <ta e="T2127" id="Seg_2058" s="T2126">dĭ</ta>
            <ta e="T2128" id="Seg_2059" s="T2127">uʔbdə-bi</ta>
            <ta e="T2129" id="Seg_2060" s="T2128">am-bia-m</ta>
            <ta e="T2130" id="Seg_2061" s="T2129">măn</ta>
            <ta e="T2131" id="Seg_2062" s="T2130">poʔto</ta>
            <ta e="T2132" id="Seg_2063" s="T2131">alʼi</ta>
            <ta e="T2133" id="Seg_2064" s="T2132">ej</ta>
            <ta e="T2134" id="Seg_2065" s="T2133">am-bia-m</ta>
            <ta e="T2135" id="Seg_2066" s="T2134">am-bia-m</ta>
            <ta e="T2136" id="Seg_2067" s="T2135">bɨ</ta>
            <ta e="T2137" id="Seg_2068" s="T2136">dak</ta>
            <ta e="T2138" id="Seg_2069" s="T2137">măn</ta>
            <ta e="T2139" id="Seg_2070" s="T2138">nanə-m</ta>
            <ta e="T2140" id="Seg_2071" s="T2139">bɨ</ta>
            <ta e="T2141" id="Seg_2072" s="T2140">urgo</ta>
            <ta e="T2142" id="Seg_2073" s="T2141">i-bi</ta>
            <ta e="T2144" id="Seg_2074" s="T2143">ej</ta>
            <ta e="T2145" id="Seg_2075" s="T2144">am-bia-m</ta>
            <ta e="T2146" id="Seg_2076" s="T2145">kam-bi</ta>
            <ta e="T2147" id="Seg_2077" s="T2146">tura-zaŋ-də</ta>
            <ta e="T2148" id="Seg_2078" s="T2147">ku-lio-t</ta>
            <ta e="T2149" id="Seg_2079" s="T2148">šoška</ta>
            <ta e="T2150" id="Seg_2080" s="T2149">mĭn-ge</ta>
            <ta e="T2151" id="Seg_2081" s="T2150">üdʼüge-ʔi</ta>
            <ta e="T2152" id="Seg_2082" s="T2151">šoška-ʔi</ta>
            <ta e="T2153" id="Seg_2083" s="T2152">xatʼel</ta>
            <ta e="T2154" id="Seg_2084" s="T2153">kabar-zittə</ta>
            <ta e="T2155" id="Seg_2085" s="T2154">onʼiʔ</ta>
            <ta e="T2156" id="Seg_2086" s="T2155">üdʼüge</ta>
            <ta e="T2157" id="Seg_2087" s="T2156">šoška</ta>
            <ta e="T2158" id="Seg_2088" s="T2157">šoška</ta>
            <ta e="T2159" id="Seg_2089" s="T2158">măn-də</ta>
            <ta e="T2160" id="Seg_2090" s="T2159">i-ʔ</ta>
            <ta e="T2161" id="Seg_2091" s="T2160">kabar-ə-ʔ</ta>
            <ta e="T2162" id="Seg_2092" s="T2161">dĭ-zeŋ</ta>
            <ta e="T2163" id="Seg_2093" s="T2162">üdʼüge-ʔi</ta>
            <ta e="T2164" id="Seg_2094" s="T2163">kan-žə-bəj</ta>
            <ta e="T2165" id="Seg_2095" s="T2164">bü-nə</ta>
            <ta e="T2166" id="Seg_2096" s="T2165">da</ta>
            <ta e="T2168" id="Seg_2097" s="T2167">dĭ-zem</ta>
            <ta e="T2170" id="Seg_2098" s="T2169">kros-sʼtə</ta>
            <ta e="T2171" id="Seg_2099" s="T2170">dĭgəttə</ta>
            <ta e="T2172" id="Seg_2100" s="T2171">šo-bi-ʔi</ta>
            <ta e="T2173" id="Seg_2101" s="T2172">tăn</ta>
            <ta e="T2174" id="Seg_2102" s="T2173">nu-ʔ</ta>
            <ta e="T2175" id="Seg_2103" s="T2174">dĭn</ta>
            <ta e="T2176" id="Seg_2104" s="T2175">a</ta>
            <ta e="T2177" id="Seg_2105" s="T2176">măn</ta>
            <ta e="T2178" id="Seg_2106" s="T2177">döbər</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1890" id="Seg_2107" s="T1889">onʼiʔ</ta>
            <ta e="T1891" id="Seg_2108" s="T1890">kuza-n</ta>
            <ta e="T1893" id="Seg_2109" s="T1892">i-bi</ta>
            <ta e="T1894" id="Seg_2110" s="T1893">men</ta>
            <ta e="T1895" id="Seg_2111" s="T1894">dĭ</ta>
            <ta e="T1896" id="Seg_2112" s="T1895">üge</ta>
            <ta e="T1897" id="Seg_2113" s="T1896">dĭ-n</ta>
            <ta e="T1898" id="Seg_2114" s="T1897">bar</ta>
            <ta e="T1900" id="Seg_2115" s="T1899">ular-zAŋ-də</ta>
            <ta e="T1901" id="Seg_2116" s="T1900">ej</ta>
            <ta e="T1902" id="Seg_2117" s="T1901">mĭ-bi</ta>
            <ta e="T1903" id="Seg_2118" s="T1902">volk-Tə</ta>
            <ta e="T1904" id="Seg_2119" s="T1903">dĭ-Tə</ta>
            <ta e="T1905" id="Seg_2120" s="T1904">üge</ta>
            <ta e="T1906" id="Seg_2121" s="T1905">kabažar-bi</ta>
            <ta e="T1908" id="Seg_2122" s="T1906">dĭgəttə</ta>
            <ta e="T1913" id="Seg_2123" s="T1912">dĭgəttə</ta>
            <ta e="T1915" id="Seg_2124" s="T1914">dʼiʔtə</ta>
            <ta e="T1916" id="Seg_2125" s="T1915">mo-laːm-bi</ta>
            <ta e="T1917" id="Seg_2126" s="T1916">dĭ</ta>
            <ta e="T1918" id="Seg_2127" s="T1917">dĭ-Tə</ta>
            <ta e="T1919" id="Seg_2128" s="T1918">dĭ-m</ta>
            <ta e="T1920" id="Seg_2129" s="T1919">i-bi</ta>
            <ta e="T1921" id="Seg_2130" s="T1920">saro-bi</ta>
            <ta e="T1922" id="Seg_2131" s="T1921">i</ta>
            <ta e="T1923" id="Seg_2132" s="T1922">kun-bi</ta>
            <ta e="T1924" id="Seg_2133" s="T1923">dʼije-Tə</ta>
            <ta e="T1925" id="Seg_2134" s="T1924">xatʼel</ta>
            <ta e="T1926" id="Seg_2135" s="T1925">dĭ-m</ta>
            <ta e="T1927" id="Seg_2136" s="T1926">pʼaŋdə-zittə</ta>
            <ta e="T1928" id="Seg_2137" s="T1927">dĭn</ta>
            <ta e="T1929" id="Seg_2138" s="T1928">bar</ta>
            <ta e="T1930" id="Seg_2139" s="T1929">tʼor-lAʔ</ta>
            <ta e="T1931" id="Seg_2140" s="T1930">amnə-laʔbə</ta>
            <ta e="T1932" id="Seg_2141" s="T1931">dĭgəttə</ta>
            <ta e="T1933" id="Seg_2142" s="T1932">dĭ-m</ta>
            <ta e="T1934" id="Seg_2143" s="T1933">dĭ-m</ta>
            <ta e="T1936" id="Seg_2144" s="T1935">sar-bi</ta>
            <ta e="T1937" id="Seg_2145" s="T1936">pa-Tə</ta>
            <ta e="T1938" id="Seg_2146" s="T1937">da</ta>
            <ta e="T1939" id="Seg_2147" s="T1938">bos-də</ta>
            <ta e="T1940" id="Seg_2148" s="T1939">kan-lAʔ</ta>
            <ta e="T1941" id="Seg_2149" s="T1940">tʼür-bi</ta>
            <ta e="T1942" id="Seg_2150" s="T1941">dĭgəttə</ta>
            <ta e="T1943" id="Seg_2151" s="T1942">volk-Tə</ta>
            <ta e="T1944" id="Seg_2152" s="T1943">šonə-gA</ta>
            <ta e="T1945" id="Seg_2153" s="T1944">tăn</ta>
            <ta e="T1946" id="Seg_2154" s="T1945">dön</ta>
            <ta e="T1947" id="Seg_2155" s="T1946">amnə-laʔbə-l</ta>
            <ta e="T1948" id="Seg_2156" s="T1947">măna</ta>
            <ta e="T1949" id="Seg_2157" s="T1948">ej</ta>
            <ta e="T1950" id="Seg_2158" s="T1949">öʔlu-bi-l</ta>
            <ta e="T1951" id="Seg_2159" s="T1950">kamən</ta>
            <ta e="T1952" id="Seg_2160" s="T1951">măn</ta>
            <ta e="T1953" id="Seg_2161" s="T1952">šo-bi-m</ta>
            <ta e="T1954" id="Seg_2162" s="T1953">tüjö</ta>
            <ta e="T1955" id="Seg_2163" s="T1954">tănan</ta>
            <ta e="T1956" id="Seg_2164" s="T1955">am-luʔbdə-lV-m</ta>
            <ta e="T1957" id="Seg_2165" s="T1956">da</ta>
            <ta e="T1958" id="Seg_2166" s="T1957">ĭmbi</ta>
            <ta e="T1959" id="Seg_2167" s="T1958">tăn</ta>
            <ta e="T1960" id="Seg_2168" s="T1959">măna</ta>
            <ta e="T1961" id="Seg_2169" s="T1960">am-lV-l</ta>
            <ta e="T1962" id="Seg_2170" s="T1961">măn</ta>
            <ta e="T1964" id="Seg_2171" s="T1962">tolʼko</ta>
            <ta e="T1965" id="Seg_2172" s="T1964">uja</ta>
            <ta e="T1966" id="Seg_2173" s="T1965">naga</ta>
            <ta e="T1967" id="Seg_2174" s="T1966">da</ta>
            <ta e="T1968" id="Seg_2175" s="T1967">tʼaktə</ta>
            <ta e="T1969" id="Seg_2176" s="T1968">mo-laːm-bi-m</ta>
            <ta e="T1970" id="Seg_2177" s="T1969">kan-ə-ʔ</ta>
            <ta e="T1971" id="Seg_2178" s="T1970">da</ta>
            <ta e="T1972" id="Seg_2179" s="T1971">uja</ta>
            <ta e="T1973" id="Seg_2180" s="T1972">det-t</ta>
            <ta e="T1974" id="Seg_2181" s="T1973">ine-n</ta>
            <ta e="T1975" id="Seg_2182" s="T1974">măna</ta>
            <ta e="T1976" id="Seg_2183" s="T1975">da</ta>
            <ta e="T1977" id="Seg_2184" s="T1976">kan-bi</ta>
            <ta e="T1978" id="Seg_2185" s="T1977">det-bi</ta>
            <ta e="T1979" id="Seg_2186" s="T1978">uja</ta>
            <ta e="T1980" id="Seg_2187" s="T1979">ine-n</ta>
            <ta e="T1981" id="Seg_2188" s="T1980">dĭ</ta>
            <ta e="T1982" id="Seg_2189" s="T1981">am-bi</ta>
            <ta e="T1983" id="Seg_2190" s="T1982">dĭgəttə</ta>
            <ta e="T1985" id="Seg_2191" s="T1983">măn-ntə</ta>
            <ta e="T1986" id="Seg_2192" s="T1985">šo-bi</ta>
            <ta e="T1987" id="Seg_2193" s="T1986">šide</ta>
            <ta e="T1988" id="Seg_2194" s="T1987">tʼala</ta>
            <ta e="T0" id="Seg_2195" s="T1988">kan-lAʔ</ta>
            <ta e="T1989" id="Seg_2196" s="T0">tʼür-bi-jəʔ</ta>
            <ta e="T1990" id="Seg_2197" s="T1989">dĭ</ta>
            <ta e="T1991" id="Seg_2198" s="T1990">šo-bi</ta>
            <ta e="T1992" id="Seg_2199" s="T1991">dĭ-Tə</ta>
            <ta e="T1993" id="Seg_2200" s="T1992">no</ta>
            <ta e="T1994" id="Seg_2201" s="T1993">ĭmbi</ta>
            <ta e="T1995" id="Seg_2202" s="T1994">ĭššo</ta>
            <ta e="T1996" id="Seg_2203" s="T1995">det-t</ta>
            <ta e="T1997" id="Seg_2204" s="T1996">măna</ta>
            <ta e="T1998" id="Seg_2205" s="T1997">ular-n</ta>
            <ta e="T1999" id="Seg_2206" s="T1998">uja</ta>
            <ta e="T2000" id="Seg_2207" s="T1999">dĭ</ta>
            <ta e="T2001" id="Seg_2208" s="T2000">kan-bi</ta>
            <ta e="T2002" id="Seg_2209" s="T2001">ular</ta>
            <ta e="T2003" id="Seg_2210" s="T2002">dĭ-Tə</ta>
            <ta e="T2004" id="Seg_2211" s="T2003">det-bi</ta>
            <ta e="T2005" id="Seg_2212" s="T2004">dĭ</ta>
            <ta e="T2006" id="Seg_2213" s="T2005">am-luʔbdə-bi</ta>
            <ta e="T2007" id="Seg_2214" s="T2006">dĭ</ta>
            <ta e="T2008" id="Seg_2215" s="T2007">ular-də</ta>
            <ta e="T2009" id="Seg_2216" s="T2008">dĭgəttə</ta>
            <ta e="T2010" id="Seg_2217" s="T2009">bazoʔ</ta>
            <ta e="T2011" id="Seg_2218" s="T2010">šo-bi</ta>
            <ta e="T2012" id="Seg_2219" s="T2011">volk</ta>
            <ta e="T2013" id="Seg_2220" s="T2012">ĭmbi</ta>
            <ta e="T2014" id="Seg_2221" s="T2013">jakšə</ta>
            <ta e="T2015" id="Seg_2222" s="T2014">mo-laːm-bi-m</ta>
            <ta e="T2016" id="Seg_2223" s="T2015">dʼok</ta>
            <ta e="T2017" id="Seg_2224" s="T2016">ĭššo</ta>
            <ta e="T2018" id="Seg_2225" s="T2017">šoška-n</ta>
            <ta e="T2019" id="Seg_2226" s="T2018">uja</ta>
            <ta e="T2020" id="Seg_2227" s="T2019">det-lV-l</ta>
            <ta e="T2021" id="Seg_2228" s="T2020">dĭgəttə</ta>
            <ta e="T2022" id="Seg_2229" s="T2021">măn</ta>
            <ta e="T2023" id="Seg_2230" s="T2022">jakšə</ta>
            <ta e="T2024" id="Seg_2231" s="T2023">mo-lV-m</ta>
            <ta e="T2025" id="Seg_2232" s="T2024">dĭ</ta>
            <ta e="T2026" id="Seg_2233" s="T2025">šoška-bə</ta>
            <ta e="T2027" id="Seg_2234" s="T2026">det-bi</ta>
            <ta e="T2028" id="Seg_2235" s="T2027">dĭ</ta>
            <ta e="T2029" id="Seg_2236" s="T2028">men</ta>
            <ta e="T2030" id="Seg_2237" s="T2029">am-luʔbdə-bi</ta>
            <ta e="T2031" id="Seg_2238" s="T2030">dĭgəttə</ta>
            <ta e="T2032" id="Seg_2239" s="T2031">šo-bi</ta>
            <ta e="T2033" id="Seg_2240" s="T2032">dĭgəttə</ta>
            <ta e="T2034" id="Seg_2241" s="T2033">davaj</ta>
            <ta e="T2036" id="Seg_2242" s="T2035">men</ta>
            <ta e="T2037" id="Seg_2243" s="T2036">dĭ-m</ta>
            <ta e="T2038" id="Seg_2244" s="T2037">amno-zittə</ta>
            <ta e="T2039" id="Seg_2245" s="T2038">tʼabəro-bi-jəʔ</ta>
            <ta e="T2040" id="Seg_2246" s="T2039">tʼabəro-bi-jəʔ</ta>
            <ta e="T2041" id="Seg_2247" s="T2040">tak</ta>
            <ta e="T2042" id="Seg_2248" s="T2041">volk</ta>
            <ta e="T2043" id="Seg_2249" s="T2042">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2047" id="Seg_2250" s="T2046">dĭ-gəʔ</ta>
            <ta e="T2048" id="Seg_2251" s="T2047">i</ta>
            <ta e="T2049" id="Seg_2252" s="T2048">amno-laʔbə</ta>
            <ta e="T2050" id="Seg_2253" s="T2049">kustə</ta>
            <ta e="T2051" id="Seg_2254" s="T2050">toʔ-gəndə</ta>
            <ta e="T2052" id="Seg_2255" s="T2051">da</ta>
            <ta e="T2053" id="Seg_2256" s="T2052">bos-də</ta>
            <ta e="T2054" id="Seg_2257" s="T2053">kem</ta>
            <ta e="T2055" id="Seg_2258" s="T2054">šĭkə-t-ziʔ</ta>
            <ta e="T2056" id="Seg_2259" s="T2055">am-laʔbə</ta>
            <ta e="T2057" id="Seg_2260" s="T2056">da</ta>
            <ta e="T2058" id="Seg_2261" s="T2057">kudo-nzə-laʔbə</ta>
            <ta e="T2059" id="Seg_2262" s="T2058">to</ta>
            <ta e="T2060" id="Seg_2263" s="T2059">men</ta>
            <ta e="T2061" id="Seg_2264" s="T2060">men-Tə</ta>
            <ta e="T2062" id="Seg_2265" s="T2061">tüj</ta>
            <ta e="T2063" id="Seg_2266" s="T2062">šində-Tə=də</ta>
            <ta e="T2065" id="Seg_2267" s="T2064">šində-m=də</ta>
            <ta e="T2066" id="Seg_2268" s="T2065">ej</ta>
            <ta e="T2067" id="Seg_2269" s="T2066">ajir-lV-m</ta>
            <ta e="T2068" id="Seg_2270" s="T2067">šində</ta>
            <ta e="T2069" id="Seg_2271" s="T2068">bar</ta>
            <ta e="T2070" id="Seg_2272" s="T2069">am-luʔbdə-lV-m</ta>
            <ta e="T2071" id="Seg_2273" s="T2070">kabarləj</ta>
            <ta e="T2072" id="Seg_2274" s="T2071">dĭgəttə</ta>
            <ta e="T2073" id="Seg_2275" s="T2072">dĭ</ta>
            <ta e="T2074" id="Seg_2276" s="T2073">kan-ntə-laʔbə</ta>
            <ta e="T2075" id="Seg_2277" s="T2074">poʔto</ta>
            <ta e="T2076" id="Seg_2278" s="T2075">nu-laʔbə</ta>
            <ta e="T2077" id="Seg_2279" s="T2076">măn</ta>
            <ta e="T2078" id="Seg_2280" s="T2077">poʔto</ta>
            <ta e="T2079" id="Seg_2281" s="T2078">poʔto</ta>
            <ta e="T2080" id="Seg_2282" s="T2079">măn</ta>
            <ta e="T2081" id="Seg_2283" s="T2080">tănan</ta>
            <ta e="T2082" id="Seg_2284" s="T2081">am-lV-m</ta>
            <ta e="T2083" id="Seg_2285" s="T2082">e-ʔ</ta>
            <ta e="T2084" id="Seg_2286" s="T2083">am-ə-ʔ</ta>
            <ta e="T2085" id="Seg_2287" s="T2084">măna</ta>
            <ta e="T2086" id="Seg_2288" s="T2085">tăn</ta>
            <ta e="T2087" id="Seg_2289" s="T2086">măn</ta>
            <ta e="T2088" id="Seg_2290" s="T2087">nʼuʔdə-n</ta>
            <ta e="T2089" id="Seg_2291" s="T2088">a</ta>
            <ta e="T2090" id="Seg_2292" s="T2089">tăn</ta>
            <ta e="T2091" id="Seg_2293" s="T2090">dĭn</ta>
            <ta e="T2092" id="Seg_2294" s="T2091">măn</ta>
            <ta e="T2094" id="Seg_2295" s="T2093">aŋ-də</ta>
            <ta e="T2095" id="Seg_2296" s="T2094">kaj-t</ta>
            <ta e="T2096" id="Seg_2297" s="T2095">kar-t</ta>
            <ta e="T2098" id="Seg_2298" s="T2097">aŋ-də</ta>
            <ta e="T2099" id="Seg_2299" s="T2098">a</ta>
            <ta e="T2100" id="Seg_2300" s="T2099">măn</ta>
            <ta e="T2101" id="Seg_2301" s="T2100">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T2102" id="Seg_2302" s="T2101">i</ta>
            <ta e="T2104" id="Seg_2303" s="T2103">tăn</ta>
            <ta e="T2105" id="Seg_2304" s="T2104">aŋ-gəndə</ta>
            <ta e="T2106" id="Seg_2305" s="T2105">süʔmə-luʔbdə-lV-m</ta>
            <ta e="T2107" id="Seg_2306" s="T2106">dĭgəttə</ta>
            <ta e="T2109" id="Seg_2307" s="T2108">dĭ</ta>
            <ta e="T2110" id="Seg_2308" s="T2109">kak</ta>
            <ta e="T2111" id="Seg_2309" s="T2110">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2112" id="Seg_2310" s="T2111">dĭ</ta>
            <ta e="T2113" id="Seg_2311" s="T2112">dĭ-m</ta>
            <ta e="T2114" id="Seg_2312" s="T2113">ulu-gəndə</ta>
            <ta e="T2115" id="Seg_2313" s="T2114">kak</ta>
            <ta e="T2116" id="Seg_2314" s="T2115">toʔbdə-nar-luʔbdə-bi</ta>
            <ta e="T2117" id="Seg_2315" s="T2116">a</ta>
            <ta e="T2118" id="Seg_2316" s="T2117">dĭ</ta>
            <ta e="T2119" id="Seg_2317" s="T2118">saʔmə-luʔbdə-bi</ta>
            <ta e="T2120" id="Seg_2318" s="T2119">i</ta>
            <ta e="T2121" id="Seg_2319" s="T2120">aʔpi</ta>
            <ta e="T2122" id="Seg_2320" s="T2121">ej</ta>
            <ta e="T2123" id="Seg_2321" s="T2122">kü-bi</ta>
            <ta e="T2124" id="Seg_2322" s="T2123">a</ta>
            <ta e="T2125" id="Seg_2323" s="T2124">poʔto</ta>
            <ta e="T2126" id="Seg_2324" s="T2125">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2127" id="Seg_2325" s="T2126">dĭ</ta>
            <ta e="T2128" id="Seg_2326" s="T2127">uʔbdə-bi</ta>
            <ta e="T2129" id="Seg_2327" s="T2128">am-bi-m</ta>
            <ta e="T2130" id="Seg_2328" s="T2129">măn</ta>
            <ta e="T2131" id="Seg_2329" s="T2130">poʔto</ta>
            <ta e="T2132" id="Seg_2330" s="T2131">aľi</ta>
            <ta e="T2133" id="Seg_2331" s="T2132">ej</ta>
            <ta e="T2134" id="Seg_2332" s="T2133">am-bi-m</ta>
            <ta e="T2135" id="Seg_2333" s="T2134">am-bi-m</ta>
            <ta e="T2136" id="Seg_2334" s="T2135">bɨ</ta>
            <ta e="T2137" id="Seg_2335" s="T2136">tak</ta>
            <ta e="T2138" id="Seg_2336" s="T2137">măn</ta>
            <ta e="T2139" id="Seg_2337" s="T2138">nanə-m</ta>
            <ta e="T2140" id="Seg_2338" s="T2139">bɨ</ta>
            <ta e="T2141" id="Seg_2339" s="T2140">urgo</ta>
            <ta e="T2142" id="Seg_2340" s="T2141">i-bi</ta>
            <ta e="T2144" id="Seg_2341" s="T2143">ej</ta>
            <ta e="T2145" id="Seg_2342" s="T2144">am-bi-m</ta>
            <ta e="T2146" id="Seg_2343" s="T2145">kan-bi</ta>
            <ta e="T2147" id="Seg_2344" s="T2146">tura-zAŋ-Tə</ta>
            <ta e="T2148" id="Seg_2345" s="T2147">ku-liA-t</ta>
            <ta e="T2149" id="Seg_2346" s="T2148">šoška</ta>
            <ta e="T2150" id="Seg_2347" s="T2149">mĭn-gA</ta>
            <ta e="T2151" id="Seg_2348" s="T2150">üdʼüge-jəʔ</ta>
            <ta e="T2152" id="Seg_2349" s="T2151">šoška-jəʔ</ta>
            <ta e="T2153" id="Seg_2350" s="T2152">xatʼel</ta>
            <ta e="T2154" id="Seg_2351" s="T2153">kabar-zittə</ta>
            <ta e="T2155" id="Seg_2352" s="T2154">onʼiʔ</ta>
            <ta e="T2156" id="Seg_2353" s="T2155">üdʼüge</ta>
            <ta e="T2157" id="Seg_2354" s="T2156">šoška</ta>
            <ta e="T2158" id="Seg_2355" s="T2157">šoška</ta>
            <ta e="T2159" id="Seg_2356" s="T2158">măn-ntə</ta>
            <ta e="T2160" id="Seg_2357" s="T2159">e-ʔ</ta>
            <ta e="T2161" id="Seg_2358" s="T2160">kabar-ə-ʔ</ta>
            <ta e="T2162" id="Seg_2359" s="T2161">dĭ-zAŋ</ta>
            <ta e="T2163" id="Seg_2360" s="T2162">üdʼüge-jəʔ</ta>
            <ta e="T2164" id="Seg_2361" s="T2163">kan-žə-bəj</ta>
            <ta e="T2165" id="Seg_2362" s="T2164">bü-Tə</ta>
            <ta e="T2166" id="Seg_2363" s="T2165">da</ta>
            <ta e="T2168" id="Seg_2364" s="T2167">dĭ-zem</ta>
            <ta e="T2170" id="Seg_2365" s="T2169">kroːs-zittə</ta>
            <ta e="T2171" id="Seg_2366" s="T2170">dĭgəttə</ta>
            <ta e="T2172" id="Seg_2367" s="T2171">šo-bi-jəʔ</ta>
            <ta e="T2173" id="Seg_2368" s="T2172">tăn</ta>
            <ta e="T2174" id="Seg_2369" s="T2173">nu-ʔ</ta>
            <ta e="T2175" id="Seg_2370" s="T2174">dĭn</ta>
            <ta e="T2176" id="Seg_2371" s="T2175">a</ta>
            <ta e="T2177" id="Seg_2372" s="T2176">măn</ta>
            <ta e="T2178" id="Seg_2373" s="T2177">döbər</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1890" id="Seg_2374" s="T1889">one.[NOM.SG]</ta>
            <ta e="T1891" id="Seg_2375" s="T1890">man-GEN</ta>
            <ta e="T1893" id="Seg_2376" s="T1892">be-PST.[3SG]</ta>
            <ta e="T1894" id="Seg_2377" s="T1893">dog.[NOM.SG]</ta>
            <ta e="T1895" id="Seg_2378" s="T1894">this.[NOM.SG]</ta>
            <ta e="T1896" id="Seg_2379" s="T1895">always</ta>
            <ta e="T1897" id="Seg_2380" s="T1896">this-GEN</ta>
            <ta e="T1898" id="Seg_2381" s="T1897">PTCL</ta>
            <ta e="T1900" id="Seg_2382" s="T1899">sheep-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1901" id="Seg_2383" s="T1900">NEG</ta>
            <ta e="T1902" id="Seg_2384" s="T1901">give-PST.[3SG]</ta>
            <ta e="T1903" id="Seg_2385" s="T1902">wolf-LAT</ta>
            <ta e="T1904" id="Seg_2386" s="T1903">this-LAT</ta>
            <ta e="T1905" id="Seg_2387" s="T1904">always</ta>
            <ta e="T1906" id="Seg_2388" s="T1905">help-PST.[3SG]</ta>
            <ta e="T1908" id="Seg_2389" s="T1906">then</ta>
            <ta e="T1913" id="Seg_2390" s="T1912">then</ta>
            <ta e="T1915" id="Seg_2391" s="T1914">%ill</ta>
            <ta e="T1916" id="Seg_2392" s="T1915">become-RES-PST.[3SG]</ta>
            <ta e="T1917" id="Seg_2393" s="T1916">this.[NOM.SG]</ta>
            <ta e="T1918" id="Seg_2394" s="T1917">this-LAT</ta>
            <ta e="T1919" id="Seg_2395" s="T1918">this-ACC</ta>
            <ta e="T1920" id="Seg_2396" s="T1919">take-PST.[3SG]</ta>
            <ta e="T1921" id="Seg_2397" s="T1920">tie-PST</ta>
            <ta e="T1922" id="Seg_2398" s="T1921">and</ta>
            <ta e="T1923" id="Seg_2399" s="T1922">bring-PST.[3SG]</ta>
            <ta e="T1924" id="Seg_2400" s="T1923">forest-LAT</ta>
            <ta e="T1925" id="Seg_2401" s="T1924">want.PST.M.SG</ta>
            <ta e="T1926" id="Seg_2402" s="T1925">this-ACC</ta>
            <ta e="T1927" id="Seg_2403" s="T1926">press-INF.LAT</ta>
            <ta e="T1928" id="Seg_2404" s="T1927">there</ta>
            <ta e="T1929" id="Seg_2405" s="T1928">PTCL</ta>
            <ta e="T1930" id="Seg_2406" s="T1929">cry-CVB</ta>
            <ta e="T1931" id="Seg_2407" s="T1930">sit-DUR.[3SG]</ta>
            <ta e="T1932" id="Seg_2408" s="T1931">then</ta>
            <ta e="T1933" id="Seg_2409" s="T1932">this-ACC</ta>
            <ta e="T1934" id="Seg_2410" s="T1933">this-ACC</ta>
            <ta e="T1936" id="Seg_2411" s="T1935">bind-PST.[3SG]</ta>
            <ta e="T1937" id="Seg_2412" s="T1936">tree-LAT</ta>
            <ta e="T1938" id="Seg_2413" s="T1937">and</ta>
            <ta e="T1939" id="Seg_2414" s="T1938">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1940" id="Seg_2415" s="T1939">go-CVB</ta>
            <ta e="T1941" id="Seg_2416" s="T1940">disappear-PST.[3SG]</ta>
            <ta e="T1942" id="Seg_2417" s="T1941">then</ta>
            <ta e="T1943" id="Seg_2418" s="T1942">wolf-LAT</ta>
            <ta e="T1944" id="Seg_2419" s="T1943">come-PRS.[3SG]</ta>
            <ta e="T1945" id="Seg_2420" s="T1944">you.NOM</ta>
            <ta e="T1946" id="Seg_2421" s="T1945">here</ta>
            <ta e="T1947" id="Seg_2422" s="T1946">sit-DUR-2SG</ta>
            <ta e="T1948" id="Seg_2423" s="T1947">I.ACC</ta>
            <ta e="T1949" id="Seg_2424" s="T1948">NEG</ta>
            <ta e="T1950" id="Seg_2425" s="T1949">send-PST-2SG</ta>
            <ta e="T1951" id="Seg_2426" s="T1950">when</ta>
            <ta e="T1952" id="Seg_2427" s="T1951">I.NOM</ta>
            <ta e="T1953" id="Seg_2428" s="T1952">come-PST-1SG</ta>
            <ta e="T1954" id="Seg_2429" s="T1953">soon</ta>
            <ta e="T1955" id="Seg_2430" s="T1954">you.ACC</ta>
            <ta e="T1956" id="Seg_2431" s="T1955">eat-MOM-FUT-1SG</ta>
            <ta e="T1957" id="Seg_2432" s="T1956">and</ta>
            <ta e="T1958" id="Seg_2433" s="T1957">what.[NOM.SG]</ta>
            <ta e="T1959" id="Seg_2434" s="T1958">you.NOM</ta>
            <ta e="T1960" id="Seg_2435" s="T1959">I.ACC</ta>
            <ta e="T1961" id="Seg_2436" s="T1960">eat-FUT-2SG</ta>
            <ta e="T1962" id="Seg_2437" s="T1961">I.NOM</ta>
            <ta e="T1964" id="Seg_2438" s="T1962">only</ta>
            <ta e="T1965" id="Seg_2439" s="T1964">meat.[NOM.SG]</ta>
            <ta e="T1966" id="Seg_2440" s="T1965">NEG.EX</ta>
            <ta e="T1967" id="Seg_2441" s="T1966">and</ta>
            <ta e="T1968" id="Seg_2442" s="T1967">old.[NOM.SG]</ta>
            <ta e="T1969" id="Seg_2443" s="T1968">become-RES-PST-1SG</ta>
            <ta e="T1970" id="Seg_2444" s="T1969">go-EP-IMP.2SG</ta>
            <ta e="T1971" id="Seg_2445" s="T1970">and</ta>
            <ta e="T1972" id="Seg_2446" s="T1971">meat.[NOM.SG]</ta>
            <ta e="T1973" id="Seg_2447" s="T1972">bring-IMP.2SG.O</ta>
            <ta e="T1974" id="Seg_2448" s="T1973">horse-GEN</ta>
            <ta e="T1975" id="Seg_2449" s="T1974">I.LAT</ta>
            <ta e="T1976" id="Seg_2450" s="T1975">and</ta>
            <ta e="T1977" id="Seg_2451" s="T1976">go-PST.[3SG]</ta>
            <ta e="T1978" id="Seg_2452" s="T1977">bring-PST.[3SG]</ta>
            <ta e="T1979" id="Seg_2453" s="T1978">meat.[NOM.SG]</ta>
            <ta e="T1980" id="Seg_2454" s="T1979">horse-GEN</ta>
            <ta e="T1981" id="Seg_2455" s="T1980">this.[NOM.SG]</ta>
            <ta e="T1982" id="Seg_2456" s="T1981">eat-PST.[3SG]</ta>
            <ta e="T1983" id="Seg_2457" s="T1982">then</ta>
            <ta e="T1985" id="Seg_2458" s="T1983">say-IPFVZ.[3SG]</ta>
            <ta e="T1986" id="Seg_2459" s="T1985">come-PST.[3SG]</ta>
            <ta e="T1987" id="Seg_2460" s="T1986">two.[NOM.SG]</ta>
            <ta e="T1988" id="Seg_2461" s="T1987">day.[NOM.SG]</ta>
            <ta e="T0" id="Seg_2462" s="T1988">go-CVB</ta>
            <ta e="T1989" id="Seg_2463" s="T0">disappear-PST-3PL</ta>
            <ta e="T1990" id="Seg_2464" s="T1989">this.[NOM.SG]</ta>
            <ta e="T1991" id="Seg_2465" s="T1990">come-PST.[3SG]</ta>
            <ta e="T1992" id="Seg_2466" s="T1991">this-LAT</ta>
            <ta e="T1993" id="Seg_2467" s="T1992">well</ta>
            <ta e="T1994" id="Seg_2468" s="T1993">what.[NOM.SG]</ta>
            <ta e="T1995" id="Seg_2469" s="T1994">more</ta>
            <ta e="T1996" id="Seg_2470" s="T1995">bring-IMP.2SG.O</ta>
            <ta e="T1997" id="Seg_2471" s="T1996">I.LAT</ta>
            <ta e="T1998" id="Seg_2472" s="T1997">sheep-GEN</ta>
            <ta e="T1999" id="Seg_2473" s="T1998">meat.[NOM.SG]</ta>
            <ta e="T2000" id="Seg_2474" s="T1999">this.[NOM.SG]</ta>
            <ta e="T2001" id="Seg_2475" s="T2000">go-PST.[3SG]</ta>
            <ta e="T2002" id="Seg_2476" s="T2001">sheep.[NOM.SG]</ta>
            <ta e="T2003" id="Seg_2477" s="T2002">this-LAT</ta>
            <ta e="T2004" id="Seg_2478" s="T2003">bring-PST.[3SG]</ta>
            <ta e="T2005" id="Seg_2479" s="T2004">this.[NOM.SG]</ta>
            <ta e="T2006" id="Seg_2480" s="T2005">eat-MOM-PST.[3SG]</ta>
            <ta e="T2007" id="Seg_2481" s="T2006">this.[NOM.SG]</ta>
            <ta e="T2008" id="Seg_2482" s="T2007">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T2009" id="Seg_2483" s="T2008">then</ta>
            <ta e="T2010" id="Seg_2484" s="T2009">again</ta>
            <ta e="T2011" id="Seg_2485" s="T2010">come-PST.[3SG]</ta>
            <ta e="T2012" id="Seg_2486" s="T2011">wolf.[NOM.SG]</ta>
            <ta e="T2013" id="Seg_2487" s="T2012">what.[NOM.SG]</ta>
            <ta e="T2014" id="Seg_2488" s="T2013">good.[NOM.SG]</ta>
            <ta e="T2015" id="Seg_2489" s="T2014">become-RES-PST-1SG</ta>
            <ta e="T2016" id="Seg_2490" s="T2015">no</ta>
            <ta e="T2017" id="Seg_2491" s="T2016">more</ta>
            <ta e="T2018" id="Seg_2492" s="T2017">pig-GEN</ta>
            <ta e="T2019" id="Seg_2493" s="T2018">meat.[NOM.SG]</ta>
            <ta e="T2020" id="Seg_2494" s="T2019">bring-FUT-2SG</ta>
            <ta e="T2021" id="Seg_2495" s="T2020">then</ta>
            <ta e="T2022" id="Seg_2496" s="T2021">I.NOM</ta>
            <ta e="T2023" id="Seg_2497" s="T2022">good.[NOM.SG]</ta>
            <ta e="T2024" id="Seg_2498" s="T2023">can-FUT-1SG</ta>
            <ta e="T2025" id="Seg_2499" s="T2024">this.[NOM.SG]</ta>
            <ta e="T2026" id="Seg_2500" s="T2025">pig-ACC.3SG</ta>
            <ta e="T2027" id="Seg_2501" s="T2026">bring-PST.[3SG]</ta>
            <ta e="T2028" id="Seg_2502" s="T2027">this.[NOM.SG]</ta>
            <ta e="T2029" id="Seg_2503" s="T2028">dog.[NOM.SG]</ta>
            <ta e="T2030" id="Seg_2504" s="T2029">eat-MOM-PST.[3SG]</ta>
            <ta e="T2031" id="Seg_2505" s="T2030">then</ta>
            <ta e="T2032" id="Seg_2506" s="T2031">come-PST.[3SG]</ta>
            <ta e="T2033" id="Seg_2507" s="T2032">then</ta>
            <ta e="T2034" id="Seg_2508" s="T2033">INCH</ta>
            <ta e="T2036" id="Seg_2509" s="T2035">dog.[NOM.SG]</ta>
            <ta e="T2037" id="Seg_2510" s="T2036">this-ACC</ta>
            <ta e="T2038" id="Seg_2511" s="T2037">live-INF.LAT</ta>
            <ta e="T2039" id="Seg_2512" s="T2038">fight-PST-3PL</ta>
            <ta e="T2040" id="Seg_2513" s="T2039">fight-PST-3PL</ta>
            <ta e="T2041" id="Seg_2514" s="T2040">so</ta>
            <ta e="T2042" id="Seg_2515" s="T2041">wolf.[NOM.SG]</ta>
            <ta e="T2043" id="Seg_2516" s="T2042">run-MOM-PST.[3SG]</ta>
            <ta e="T2047" id="Seg_2517" s="T2046">this-ABL</ta>
            <ta e="T2048" id="Seg_2518" s="T2047">and</ta>
            <ta e="T2049" id="Seg_2519" s="T2048">sit-DUR.[3SG]</ta>
            <ta e="T2050" id="Seg_2520" s="T2049">bush</ta>
            <ta e="T2051" id="Seg_2521" s="T2050">edge-LAT/LOC.3SG</ta>
            <ta e="T2052" id="Seg_2522" s="T2051">and</ta>
            <ta e="T2053" id="Seg_2523" s="T2052">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T2054" id="Seg_2524" s="T2053">blood.[NOM.SG]</ta>
            <ta e="T2055" id="Seg_2525" s="T2054">language-3SG-INS</ta>
            <ta e="T2056" id="Seg_2526" s="T2055">eat-DUR.[3SG]</ta>
            <ta e="T2057" id="Seg_2527" s="T2056">and</ta>
            <ta e="T2058" id="Seg_2528" s="T2057">scold-DES-DUR.[3SG]</ta>
            <ta e="T2059" id="Seg_2529" s="T2058">then</ta>
            <ta e="T2060" id="Seg_2530" s="T2059">dog.[NOM.SG]</ta>
            <ta e="T2061" id="Seg_2531" s="T2060">dog-LAT</ta>
            <ta e="T2062" id="Seg_2532" s="T2061">now</ta>
            <ta e="T2063" id="Seg_2533" s="T2062">who-LAT=INDEF</ta>
            <ta e="T2065" id="Seg_2534" s="T2064">who-ACC=INDEF</ta>
            <ta e="T2066" id="Seg_2535" s="T2065">NEG</ta>
            <ta e="T2067" id="Seg_2536" s="T2066">pity-FUT-1SG</ta>
            <ta e="T2068" id="Seg_2537" s="T2067">who.[NOM.SG]</ta>
            <ta e="T2069" id="Seg_2538" s="T2068">all</ta>
            <ta e="T2070" id="Seg_2539" s="T2069">eat-MOM-FUT-1SG</ta>
            <ta e="T2071" id="Seg_2540" s="T2070">enough</ta>
            <ta e="T2072" id="Seg_2541" s="T2071">then</ta>
            <ta e="T2073" id="Seg_2542" s="T2072">this.[NOM.SG]</ta>
            <ta e="T2074" id="Seg_2543" s="T2073">go-IPFVZ-DUR.[3SG]</ta>
            <ta e="T2075" id="Seg_2544" s="T2074">goat.[NOM.SG]</ta>
            <ta e="T2076" id="Seg_2545" s="T2075">stand-DUR.[3SG]</ta>
            <ta e="T2077" id="Seg_2546" s="T2076">I.NOM</ta>
            <ta e="T2078" id="Seg_2547" s="T2077">goat.[NOM.SG]</ta>
            <ta e="T2079" id="Seg_2548" s="T2078">goat.[NOM.SG]</ta>
            <ta e="T2080" id="Seg_2549" s="T2079">I.NOM</ta>
            <ta e="T2081" id="Seg_2550" s="T2080">you.ACC</ta>
            <ta e="T2082" id="Seg_2551" s="T2081">eat-FUT-1SG</ta>
            <ta e="T2083" id="Seg_2552" s="T2082">NEG.AUX-IMP.2SG</ta>
            <ta e="T2084" id="Seg_2553" s="T2083">eat-EP-CNG</ta>
            <ta e="T2085" id="Seg_2554" s="T2084">I.ACC</ta>
            <ta e="T2086" id="Seg_2555" s="T2085">you.NOM</ta>
            <ta e="T2087" id="Seg_2556" s="T2086">I.NOM</ta>
            <ta e="T2088" id="Seg_2557" s="T2087">up-LOC.ADV</ta>
            <ta e="T2089" id="Seg_2558" s="T2088">and</ta>
            <ta e="T2090" id="Seg_2559" s="T2089">you.NOM</ta>
            <ta e="T2091" id="Seg_2560" s="T2090">there</ta>
            <ta e="T2092" id="Seg_2561" s="T2091">I.NOM</ta>
            <ta e="T2094" id="Seg_2562" s="T2093">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T2095" id="Seg_2563" s="T2094">close-IMP.2SG.O</ta>
            <ta e="T2096" id="Seg_2564" s="T2095">open-IMP.2SG.O</ta>
            <ta e="T2098" id="Seg_2565" s="T2097">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T2099" id="Seg_2566" s="T2098">and</ta>
            <ta e="T2100" id="Seg_2567" s="T2099">I.NOM</ta>
            <ta e="T2101" id="Seg_2568" s="T2100">run-MOM-PST-1SG</ta>
            <ta e="T2102" id="Seg_2569" s="T2101">and</ta>
            <ta e="T2104" id="Seg_2570" s="T2103">you.GEN</ta>
            <ta e="T2105" id="Seg_2571" s="T2104">mouth-LAT/LOC.3SG</ta>
            <ta e="T2106" id="Seg_2572" s="T2105">jump-MOM-FUT-1SG</ta>
            <ta e="T2107" id="Seg_2573" s="T2106">then</ta>
            <ta e="T2109" id="Seg_2574" s="T2108">this.[NOM.SG]</ta>
            <ta e="T2110" id="Seg_2575" s="T2109">like</ta>
            <ta e="T2111" id="Seg_2576" s="T2110">run-MOM-PST.[3SG]</ta>
            <ta e="T2112" id="Seg_2577" s="T2111">this.[NOM.SG]</ta>
            <ta e="T2113" id="Seg_2578" s="T2112">this-ACC</ta>
            <ta e="T2114" id="Seg_2579" s="T2113">head-LAT/LOC.3SG</ta>
            <ta e="T2115" id="Seg_2580" s="T2114">like</ta>
            <ta e="T2116" id="Seg_2581" s="T2115">hit-MULT-MOM-PST.[3SG]</ta>
            <ta e="T2117" id="Seg_2582" s="T2116">and</ta>
            <ta e="T2118" id="Seg_2583" s="T2117">this.[NOM.SG]</ta>
            <ta e="T2119" id="Seg_2584" s="T2118">fall-MOM-PST.[3SG]</ta>
            <ta e="T2120" id="Seg_2585" s="T2119">and</ta>
            <ta e="T2121" id="Seg_2586" s="T2120">%nearly</ta>
            <ta e="T2122" id="Seg_2587" s="T2121">NEG</ta>
            <ta e="T2123" id="Seg_2588" s="T2122">die-PST.[3SG]</ta>
            <ta e="T2124" id="Seg_2589" s="T2123">and</ta>
            <ta e="T2125" id="Seg_2590" s="T2124">goat.[NOM.SG]</ta>
            <ta e="T2126" id="Seg_2591" s="T2125">run-MOM-PST.[3SG]</ta>
            <ta e="T2127" id="Seg_2592" s="T2126">this.[NOM.SG]</ta>
            <ta e="T2128" id="Seg_2593" s="T2127">get.up-PST.[3SG]</ta>
            <ta e="T2129" id="Seg_2594" s="T2128">eat-PST-1SG</ta>
            <ta e="T2130" id="Seg_2595" s="T2129">I.NOM</ta>
            <ta e="T2131" id="Seg_2596" s="T2130">goat.[NOM.SG]</ta>
            <ta e="T2132" id="Seg_2597" s="T2131">or</ta>
            <ta e="T2133" id="Seg_2598" s="T2132">NEG</ta>
            <ta e="T2134" id="Seg_2599" s="T2133">eat-PST-1SG</ta>
            <ta e="T2135" id="Seg_2600" s="T2134">eat-PST-1SG</ta>
            <ta e="T2136" id="Seg_2601" s="T2135">IRREAL</ta>
            <ta e="T2137" id="Seg_2602" s="T2136">so</ta>
            <ta e="T2138" id="Seg_2603" s="T2137">I.NOM</ta>
            <ta e="T2139" id="Seg_2604" s="T2138">belly-NOM/GEN/ACC.1SG</ta>
            <ta e="T2140" id="Seg_2605" s="T2139">IRREAL</ta>
            <ta e="T2141" id="Seg_2606" s="T2140">big.[NOM.SG]</ta>
            <ta e="T2142" id="Seg_2607" s="T2141">take-PST.[3SG]</ta>
            <ta e="T2144" id="Seg_2608" s="T2143">NEG</ta>
            <ta e="T2145" id="Seg_2609" s="T2144">eat-PST-1SG</ta>
            <ta e="T2146" id="Seg_2610" s="T2145">go-PST.[3SG]</ta>
            <ta e="T2147" id="Seg_2611" s="T2146">house-PL-LAT</ta>
            <ta e="T2148" id="Seg_2612" s="T2147">see-PRS-3SG.O</ta>
            <ta e="T2149" id="Seg_2613" s="T2148">pig.[NOM.SG]</ta>
            <ta e="T2150" id="Seg_2614" s="T2149">go-PRS.[3SG]</ta>
            <ta e="T2151" id="Seg_2615" s="T2150">small-PL</ta>
            <ta e="T2152" id="Seg_2616" s="T2151">pig-PL</ta>
            <ta e="T2153" id="Seg_2617" s="T2152">want.PST.M.SG</ta>
            <ta e="T2154" id="Seg_2618" s="T2153">grab-INF.LAT</ta>
            <ta e="T2155" id="Seg_2619" s="T2154">one.[NOM.SG]</ta>
            <ta e="T2156" id="Seg_2620" s="T2155">small.[NOM.SG]</ta>
            <ta e="T2157" id="Seg_2621" s="T2156">pig.[NOM.SG]</ta>
            <ta e="T2158" id="Seg_2622" s="T2157">pig.[NOM.SG]</ta>
            <ta e="T2159" id="Seg_2623" s="T2158">say-IPFVZ.[3SG]</ta>
            <ta e="T2160" id="Seg_2624" s="T2159">NEG.AUX-IMP.2SG</ta>
            <ta e="T2161" id="Seg_2625" s="T2160">grab-EP-CNG</ta>
            <ta e="T2162" id="Seg_2626" s="T2161">this-PL</ta>
            <ta e="T2163" id="Seg_2627" s="T2162">small-PL</ta>
            <ta e="T2164" id="Seg_2628" s="T2163">go-OPT.DU/PL-1DU</ta>
            <ta e="T2165" id="Seg_2629" s="T2164">water-LAT</ta>
            <ta e="T2166" id="Seg_2630" s="T2165">and</ta>
            <ta e="T2168" id="Seg_2631" s="T2167">this-ACC.PL</ta>
            <ta e="T2170" id="Seg_2632" s="T2169">baptize-INF.LAT</ta>
            <ta e="T2171" id="Seg_2633" s="T2170">then</ta>
            <ta e="T2172" id="Seg_2634" s="T2171">come-PST-3PL</ta>
            <ta e="T2173" id="Seg_2635" s="T2172">you.NOM</ta>
            <ta e="T2174" id="Seg_2636" s="T2173">stand-IMP.2SG</ta>
            <ta e="T2175" id="Seg_2637" s="T2174">there</ta>
            <ta e="T2176" id="Seg_2638" s="T2175">and</ta>
            <ta e="T2177" id="Seg_2639" s="T2176">I.NOM</ta>
            <ta e="T2178" id="Seg_2640" s="T2177">here</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1890" id="Seg_2641" s="T1889">один.[NOM.SG]</ta>
            <ta e="T1891" id="Seg_2642" s="T1890">мужчина-GEN</ta>
            <ta e="T1893" id="Seg_2643" s="T1892">быть-PST.[3SG]</ta>
            <ta e="T1894" id="Seg_2644" s="T1893">собака.[NOM.SG]</ta>
            <ta e="T1895" id="Seg_2645" s="T1894">этот.[NOM.SG]</ta>
            <ta e="T1896" id="Seg_2646" s="T1895">всегда</ta>
            <ta e="T1897" id="Seg_2647" s="T1896">этот-GEN</ta>
            <ta e="T1898" id="Seg_2648" s="T1897">PTCL</ta>
            <ta e="T1900" id="Seg_2649" s="T1899">овца-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1901" id="Seg_2650" s="T1900">NEG</ta>
            <ta e="T1902" id="Seg_2651" s="T1901">дать-PST.[3SG]</ta>
            <ta e="T1903" id="Seg_2652" s="T1902">волк-LAT</ta>
            <ta e="T1904" id="Seg_2653" s="T1903">этот-LAT</ta>
            <ta e="T1905" id="Seg_2654" s="T1904">всегда</ta>
            <ta e="T1906" id="Seg_2655" s="T1905">помогать-PST.[3SG]</ta>
            <ta e="T1908" id="Seg_2656" s="T1906">тогда</ta>
            <ta e="T1913" id="Seg_2657" s="T1912">тогда</ta>
            <ta e="T1915" id="Seg_2658" s="T1914">%больной</ta>
            <ta e="T1916" id="Seg_2659" s="T1915">стать-RES-PST.[3SG]</ta>
            <ta e="T1917" id="Seg_2660" s="T1916">этот.[NOM.SG]</ta>
            <ta e="T1918" id="Seg_2661" s="T1917">этот-LAT</ta>
            <ta e="T1919" id="Seg_2662" s="T1918">этот-ACC</ta>
            <ta e="T1920" id="Seg_2663" s="T1919">взять-PST.[3SG]</ta>
            <ta e="T1921" id="Seg_2664" s="T1920">привязывать-PST</ta>
            <ta e="T1922" id="Seg_2665" s="T1921">и</ta>
            <ta e="T1923" id="Seg_2666" s="T1922">нести-PST.[3SG]</ta>
            <ta e="T1924" id="Seg_2667" s="T1923">лес-LAT</ta>
            <ta e="T1925" id="Seg_2668" s="T1924">хотеть.PST.M.SG</ta>
            <ta e="T1926" id="Seg_2669" s="T1925">этот-ACC</ta>
            <ta e="T1927" id="Seg_2670" s="T1926">нажимать-INF.LAT</ta>
            <ta e="T1928" id="Seg_2671" s="T1927">там</ta>
            <ta e="T1929" id="Seg_2672" s="T1928">PTCL</ta>
            <ta e="T1930" id="Seg_2673" s="T1929">плакать-CVB</ta>
            <ta e="T1931" id="Seg_2674" s="T1930">сидеть-DUR.[3SG]</ta>
            <ta e="T1932" id="Seg_2675" s="T1931">тогда</ta>
            <ta e="T1933" id="Seg_2676" s="T1932">этот-ACC</ta>
            <ta e="T1934" id="Seg_2677" s="T1933">этот-ACC</ta>
            <ta e="T1936" id="Seg_2678" s="T1935">завязать-PST.[3SG]</ta>
            <ta e="T1937" id="Seg_2679" s="T1936">дерево-LAT</ta>
            <ta e="T1938" id="Seg_2680" s="T1937">и</ta>
            <ta e="T1939" id="Seg_2681" s="T1938">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1940" id="Seg_2682" s="T1939">пойти-CVB</ta>
            <ta e="T1941" id="Seg_2683" s="T1940">исчезнуть-PST.[3SG]</ta>
            <ta e="T1942" id="Seg_2684" s="T1941">тогда</ta>
            <ta e="T1943" id="Seg_2685" s="T1942">волк-LAT</ta>
            <ta e="T1944" id="Seg_2686" s="T1943">прийти-PRS.[3SG]</ta>
            <ta e="T1945" id="Seg_2687" s="T1944">ты.NOM</ta>
            <ta e="T1946" id="Seg_2688" s="T1945">здесь</ta>
            <ta e="T1947" id="Seg_2689" s="T1946">сидеть-DUR-2SG</ta>
            <ta e="T1948" id="Seg_2690" s="T1947">я.ACC</ta>
            <ta e="T1949" id="Seg_2691" s="T1948">NEG</ta>
            <ta e="T1950" id="Seg_2692" s="T1949">посылать-PST-2SG</ta>
            <ta e="T1951" id="Seg_2693" s="T1950">когда</ta>
            <ta e="T1952" id="Seg_2694" s="T1951">я.NOM</ta>
            <ta e="T1953" id="Seg_2695" s="T1952">прийти-PST-1SG</ta>
            <ta e="T1954" id="Seg_2696" s="T1953">скоро</ta>
            <ta e="T1955" id="Seg_2697" s="T1954">ты.ACC</ta>
            <ta e="T1956" id="Seg_2698" s="T1955">съесть-MOM-FUT-1SG</ta>
            <ta e="T1957" id="Seg_2699" s="T1956">и</ta>
            <ta e="T1958" id="Seg_2700" s="T1957">что.[NOM.SG]</ta>
            <ta e="T1959" id="Seg_2701" s="T1958">ты.NOM</ta>
            <ta e="T1960" id="Seg_2702" s="T1959">я.ACC</ta>
            <ta e="T1961" id="Seg_2703" s="T1960">съесть-FUT-2SG</ta>
            <ta e="T1962" id="Seg_2704" s="T1961">я.NOM</ta>
            <ta e="T1964" id="Seg_2705" s="T1962">только</ta>
            <ta e="T1965" id="Seg_2706" s="T1964">мясо.[NOM.SG]</ta>
            <ta e="T1966" id="Seg_2707" s="T1965">NEG.EX</ta>
            <ta e="T1967" id="Seg_2708" s="T1966">и</ta>
            <ta e="T1968" id="Seg_2709" s="T1967">старый.[NOM.SG]</ta>
            <ta e="T1969" id="Seg_2710" s="T1968">стать-RES-PST-1SG</ta>
            <ta e="T1970" id="Seg_2711" s="T1969">пойти-EP-IMP.2SG</ta>
            <ta e="T1971" id="Seg_2712" s="T1970">и</ta>
            <ta e="T1972" id="Seg_2713" s="T1971">мясо.[NOM.SG]</ta>
            <ta e="T1973" id="Seg_2714" s="T1972">принести-IMP.2SG.O</ta>
            <ta e="T1974" id="Seg_2715" s="T1973">лошадь-GEN</ta>
            <ta e="T1975" id="Seg_2716" s="T1974">я.LAT</ta>
            <ta e="T1976" id="Seg_2717" s="T1975">и</ta>
            <ta e="T1977" id="Seg_2718" s="T1976">пойти-PST.[3SG]</ta>
            <ta e="T1978" id="Seg_2719" s="T1977">принести-PST.[3SG]</ta>
            <ta e="T1979" id="Seg_2720" s="T1978">мясо.[NOM.SG]</ta>
            <ta e="T1980" id="Seg_2721" s="T1979">лошадь-GEN</ta>
            <ta e="T1981" id="Seg_2722" s="T1980">этот.[NOM.SG]</ta>
            <ta e="T1982" id="Seg_2723" s="T1981">съесть-PST.[3SG]</ta>
            <ta e="T1983" id="Seg_2724" s="T1982">тогда</ta>
            <ta e="T1985" id="Seg_2725" s="T1983">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1986" id="Seg_2726" s="T1985">прийти-PST.[3SG]</ta>
            <ta e="T1987" id="Seg_2727" s="T1986">два.[NOM.SG]</ta>
            <ta e="T1988" id="Seg_2728" s="T1987">день.[NOM.SG]</ta>
            <ta e="T0" id="Seg_2729" s="T1988">пойти-CVB</ta>
            <ta e="T1989" id="Seg_2730" s="T0">исчезнуть-PST-3PL</ta>
            <ta e="T1990" id="Seg_2731" s="T1989">этот.[NOM.SG]</ta>
            <ta e="T1991" id="Seg_2732" s="T1990">прийти-PST.[3SG]</ta>
            <ta e="T1992" id="Seg_2733" s="T1991">этот-LAT</ta>
            <ta e="T1993" id="Seg_2734" s="T1992">ну</ta>
            <ta e="T1994" id="Seg_2735" s="T1993">что.[NOM.SG]</ta>
            <ta e="T1995" id="Seg_2736" s="T1994">еще</ta>
            <ta e="T1996" id="Seg_2737" s="T1995">принести-IMP.2SG.O</ta>
            <ta e="T1997" id="Seg_2738" s="T1996">я.LAT</ta>
            <ta e="T1998" id="Seg_2739" s="T1997">овца-GEN</ta>
            <ta e="T1999" id="Seg_2740" s="T1998">мясо.[NOM.SG]</ta>
            <ta e="T2000" id="Seg_2741" s="T1999">этот.[NOM.SG]</ta>
            <ta e="T2001" id="Seg_2742" s="T2000">пойти-PST.[3SG]</ta>
            <ta e="T2002" id="Seg_2743" s="T2001">овца.[NOM.SG]</ta>
            <ta e="T2003" id="Seg_2744" s="T2002">этот-LAT</ta>
            <ta e="T2004" id="Seg_2745" s="T2003">принести-PST.[3SG]</ta>
            <ta e="T2005" id="Seg_2746" s="T2004">этот.[NOM.SG]</ta>
            <ta e="T2006" id="Seg_2747" s="T2005">съесть-MOM-PST.[3SG]</ta>
            <ta e="T2007" id="Seg_2748" s="T2006">этот.[NOM.SG]</ta>
            <ta e="T2008" id="Seg_2749" s="T2007">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T2009" id="Seg_2750" s="T2008">тогда</ta>
            <ta e="T2010" id="Seg_2751" s="T2009">опять</ta>
            <ta e="T2011" id="Seg_2752" s="T2010">прийти-PST.[3SG]</ta>
            <ta e="T2012" id="Seg_2753" s="T2011">волк.[NOM.SG]</ta>
            <ta e="T2013" id="Seg_2754" s="T2012">что.[NOM.SG]</ta>
            <ta e="T2014" id="Seg_2755" s="T2013">хороший.[NOM.SG]</ta>
            <ta e="T2015" id="Seg_2756" s="T2014">стать-RES-PST-1SG</ta>
            <ta e="T2016" id="Seg_2757" s="T2015">нет</ta>
            <ta e="T2017" id="Seg_2758" s="T2016">еще</ta>
            <ta e="T2018" id="Seg_2759" s="T2017">свинья-GEN</ta>
            <ta e="T2019" id="Seg_2760" s="T2018">мясо.[NOM.SG]</ta>
            <ta e="T2020" id="Seg_2761" s="T2019">принести-FUT-2SG</ta>
            <ta e="T2021" id="Seg_2762" s="T2020">тогда</ta>
            <ta e="T2022" id="Seg_2763" s="T2021">я.NOM</ta>
            <ta e="T2023" id="Seg_2764" s="T2022">хороший.[NOM.SG]</ta>
            <ta e="T2024" id="Seg_2765" s="T2023">мочь-FUT-1SG</ta>
            <ta e="T2025" id="Seg_2766" s="T2024">этот.[NOM.SG]</ta>
            <ta e="T2026" id="Seg_2767" s="T2025">свинья-ACC.3SG</ta>
            <ta e="T2027" id="Seg_2768" s="T2026">принести-PST.[3SG]</ta>
            <ta e="T2028" id="Seg_2769" s="T2027">этот.[NOM.SG]</ta>
            <ta e="T2029" id="Seg_2770" s="T2028">собака.[NOM.SG]</ta>
            <ta e="T2030" id="Seg_2771" s="T2029">съесть-MOM-PST.[3SG]</ta>
            <ta e="T2031" id="Seg_2772" s="T2030">тогда</ta>
            <ta e="T2032" id="Seg_2773" s="T2031">прийти-PST.[3SG]</ta>
            <ta e="T2033" id="Seg_2774" s="T2032">тогда</ta>
            <ta e="T2034" id="Seg_2775" s="T2033">INCH</ta>
            <ta e="T2036" id="Seg_2776" s="T2035">собака.[NOM.SG]</ta>
            <ta e="T2037" id="Seg_2777" s="T2036">этот-ACC</ta>
            <ta e="T2038" id="Seg_2778" s="T2037">жить-INF.LAT</ta>
            <ta e="T2039" id="Seg_2779" s="T2038">бороться-PST-3PL</ta>
            <ta e="T2040" id="Seg_2780" s="T2039">бороться-PST-3PL</ta>
            <ta e="T2041" id="Seg_2781" s="T2040">так</ta>
            <ta e="T2042" id="Seg_2782" s="T2041">волк.[NOM.SG]</ta>
            <ta e="T2043" id="Seg_2783" s="T2042">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2047" id="Seg_2784" s="T2046">этот-ABL</ta>
            <ta e="T2048" id="Seg_2785" s="T2047">и</ta>
            <ta e="T2049" id="Seg_2786" s="T2048">сидеть-DUR.[3SG]</ta>
            <ta e="T2050" id="Seg_2787" s="T2049">куст</ta>
            <ta e="T2051" id="Seg_2788" s="T2050">край-LAT/LOC.3SG</ta>
            <ta e="T2052" id="Seg_2789" s="T2051">и</ta>
            <ta e="T2053" id="Seg_2790" s="T2052">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T2054" id="Seg_2791" s="T2053">кровь.[NOM.SG]</ta>
            <ta e="T2055" id="Seg_2792" s="T2054">язык-3SG-INS</ta>
            <ta e="T2056" id="Seg_2793" s="T2055">съесть-DUR.[3SG]</ta>
            <ta e="T2057" id="Seg_2794" s="T2056">и</ta>
            <ta e="T2058" id="Seg_2795" s="T2057">ругать-DES-DUR.[3SG]</ta>
            <ta e="T2059" id="Seg_2796" s="T2058">то</ta>
            <ta e="T2060" id="Seg_2797" s="T2059">собака.[NOM.SG]</ta>
            <ta e="T2061" id="Seg_2798" s="T2060">собака-LAT</ta>
            <ta e="T2062" id="Seg_2799" s="T2061">сейчас</ta>
            <ta e="T2063" id="Seg_2800" s="T2062">кто-LAT=INDEF</ta>
            <ta e="T2065" id="Seg_2801" s="T2064">кто-ACC=INDEF</ta>
            <ta e="T2066" id="Seg_2802" s="T2065">NEG</ta>
            <ta e="T2067" id="Seg_2803" s="T2066">жалеть-FUT-1SG</ta>
            <ta e="T2068" id="Seg_2804" s="T2067">кто.[NOM.SG]</ta>
            <ta e="T2069" id="Seg_2805" s="T2068">весь</ta>
            <ta e="T2070" id="Seg_2806" s="T2069">съесть-MOM-FUT-1SG</ta>
            <ta e="T2071" id="Seg_2807" s="T2070">хватит</ta>
            <ta e="T2072" id="Seg_2808" s="T2071">тогда</ta>
            <ta e="T2073" id="Seg_2809" s="T2072">этот.[NOM.SG]</ta>
            <ta e="T2074" id="Seg_2810" s="T2073">пойти-IPFVZ-DUR.[3SG]</ta>
            <ta e="T2075" id="Seg_2811" s="T2074">коза.[NOM.SG]</ta>
            <ta e="T2076" id="Seg_2812" s="T2075">стоять-DUR.[3SG]</ta>
            <ta e="T2077" id="Seg_2813" s="T2076">я.NOM</ta>
            <ta e="T2078" id="Seg_2814" s="T2077">коза.[NOM.SG]</ta>
            <ta e="T2079" id="Seg_2815" s="T2078">коза.[NOM.SG]</ta>
            <ta e="T2080" id="Seg_2816" s="T2079">я.NOM</ta>
            <ta e="T2081" id="Seg_2817" s="T2080">ты.ACC</ta>
            <ta e="T2082" id="Seg_2818" s="T2081">съесть-FUT-1SG</ta>
            <ta e="T2083" id="Seg_2819" s="T2082">NEG.AUX-IMP.2SG</ta>
            <ta e="T2084" id="Seg_2820" s="T2083">съесть-EP-CNG</ta>
            <ta e="T2085" id="Seg_2821" s="T2084">я.ACC</ta>
            <ta e="T2086" id="Seg_2822" s="T2085">ты.NOM</ta>
            <ta e="T2087" id="Seg_2823" s="T2086">я.NOM</ta>
            <ta e="T2088" id="Seg_2824" s="T2087">вверх-LOC.ADV</ta>
            <ta e="T2089" id="Seg_2825" s="T2088">а</ta>
            <ta e="T2090" id="Seg_2826" s="T2089">ты.NOM</ta>
            <ta e="T2091" id="Seg_2827" s="T2090">там</ta>
            <ta e="T2092" id="Seg_2828" s="T2091">я.NOM</ta>
            <ta e="T2094" id="Seg_2829" s="T2093">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T2095" id="Seg_2830" s="T2094">закрыть-IMP.2SG.O</ta>
            <ta e="T2096" id="Seg_2831" s="T2095">открыть-IMP.2SG.O</ta>
            <ta e="T2098" id="Seg_2832" s="T2097">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T2099" id="Seg_2833" s="T2098">а</ta>
            <ta e="T2100" id="Seg_2834" s="T2099">я.NOM</ta>
            <ta e="T2101" id="Seg_2835" s="T2100">бежать-MOM-PST-1SG</ta>
            <ta e="T2102" id="Seg_2836" s="T2101">и</ta>
            <ta e="T2104" id="Seg_2837" s="T2103">ты.GEN</ta>
            <ta e="T2105" id="Seg_2838" s="T2104">рот-LAT/LOC.3SG</ta>
            <ta e="T2106" id="Seg_2839" s="T2105">прыгнуть-MOM-FUT-1SG</ta>
            <ta e="T2107" id="Seg_2840" s="T2106">тогда</ta>
            <ta e="T2109" id="Seg_2841" s="T2108">этот.[NOM.SG]</ta>
            <ta e="T2110" id="Seg_2842" s="T2109">как</ta>
            <ta e="T2111" id="Seg_2843" s="T2110">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2112" id="Seg_2844" s="T2111">этот.[NOM.SG]</ta>
            <ta e="T2113" id="Seg_2845" s="T2112">этот-ACC</ta>
            <ta e="T2114" id="Seg_2846" s="T2113">голова-LAT/LOC.3SG</ta>
            <ta e="T2115" id="Seg_2847" s="T2114">как</ta>
            <ta e="T2116" id="Seg_2848" s="T2115">ударить-MULT-MOM-PST.[3SG]</ta>
            <ta e="T2117" id="Seg_2849" s="T2116">а</ta>
            <ta e="T2118" id="Seg_2850" s="T2117">этот.[NOM.SG]</ta>
            <ta e="T2119" id="Seg_2851" s="T2118">упасть-MOM-PST.[3SG]</ta>
            <ta e="T2120" id="Seg_2852" s="T2119">и</ta>
            <ta e="T2121" id="Seg_2853" s="T2120">%едва</ta>
            <ta e="T2122" id="Seg_2854" s="T2121">NEG</ta>
            <ta e="T2123" id="Seg_2855" s="T2122">умереть-PST.[3SG]</ta>
            <ta e="T2124" id="Seg_2856" s="T2123">а</ta>
            <ta e="T2125" id="Seg_2857" s="T2124">коза.[NOM.SG]</ta>
            <ta e="T2126" id="Seg_2858" s="T2125">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2127" id="Seg_2859" s="T2126">этот.[NOM.SG]</ta>
            <ta e="T2128" id="Seg_2860" s="T2127">встать-PST.[3SG]</ta>
            <ta e="T2129" id="Seg_2861" s="T2128">съесть-PST-1SG</ta>
            <ta e="T2130" id="Seg_2862" s="T2129">я.NOM</ta>
            <ta e="T2131" id="Seg_2863" s="T2130">коза.[NOM.SG]</ta>
            <ta e="T2132" id="Seg_2864" s="T2131">или</ta>
            <ta e="T2133" id="Seg_2865" s="T2132">NEG</ta>
            <ta e="T2134" id="Seg_2866" s="T2133">съесть-PST-1SG</ta>
            <ta e="T2135" id="Seg_2867" s="T2134">съесть-PST-1SG</ta>
            <ta e="T2136" id="Seg_2868" s="T2135">IRREAL</ta>
            <ta e="T2137" id="Seg_2869" s="T2136">так</ta>
            <ta e="T2138" id="Seg_2870" s="T2137">я.NOM</ta>
            <ta e="T2139" id="Seg_2871" s="T2138">живот-NOM/GEN/ACC.1SG</ta>
            <ta e="T2140" id="Seg_2872" s="T2139">IRREAL</ta>
            <ta e="T2141" id="Seg_2873" s="T2140">большой.[NOM.SG]</ta>
            <ta e="T2142" id="Seg_2874" s="T2141">взять-PST.[3SG]</ta>
            <ta e="T2144" id="Seg_2875" s="T2143">NEG</ta>
            <ta e="T2145" id="Seg_2876" s="T2144">съесть-PST-1SG</ta>
            <ta e="T2146" id="Seg_2877" s="T2145">пойти-PST.[3SG]</ta>
            <ta e="T2147" id="Seg_2878" s="T2146">дом-PL-LAT</ta>
            <ta e="T2148" id="Seg_2879" s="T2147">видеть-PRS-3SG.O</ta>
            <ta e="T2149" id="Seg_2880" s="T2148">свинья.[NOM.SG]</ta>
            <ta e="T2150" id="Seg_2881" s="T2149">идти-PRS.[3SG]</ta>
            <ta e="T2151" id="Seg_2882" s="T2150">маленький-PL</ta>
            <ta e="T2152" id="Seg_2883" s="T2151">свинья-PL</ta>
            <ta e="T2153" id="Seg_2884" s="T2152">хотеть.PST.M.SG</ta>
            <ta e="T2154" id="Seg_2885" s="T2153">хватать-INF.LAT</ta>
            <ta e="T2155" id="Seg_2886" s="T2154">один.[NOM.SG]</ta>
            <ta e="T2156" id="Seg_2887" s="T2155">маленький.[NOM.SG]</ta>
            <ta e="T2157" id="Seg_2888" s="T2156">свинья.[NOM.SG]</ta>
            <ta e="T2158" id="Seg_2889" s="T2157">свинья.[NOM.SG]</ta>
            <ta e="T2159" id="Seg_2890" s="T2158">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2160" id="Seg_2891" s="T2159">NEG.AUX-IMP.2SG</ta>
            <ta e="T2161" id="Seg_2892" s="T2160">хватать-EP-CNG</ta>
            <ta e="T2162" id="Seg_2893" s="T2161">этот-PL</ta>
            <ta e="T2163" id="Seg_2894" s="T2162">маленький-PL</ta>
            <ta e="T2164" id="Seg_2895" s="T2163">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T2165" id="Seg_2896" s="T2164">вода-LAT</ta>
            <ta e="T2166" id="Seg_2897" s="T2165">и</ta>
            <ta e="T2168" id="Seg_2898" s="T2167">этот-ACC.PL</ta>
            <ta e="T2170" id="Seg_2899" s="T2169">крестить-INF.LAT</ta>
            <ta e="T2171" id="Seg_2900" s="T2170">тогда</ta>
            <ta e="T2172" id="Seg_2901" s="T2171">прийти-PST-3PL</ta>
            <ta e="T2173" id="Seg_2902" s="T2172">ты.NOM</ta>
            <ta e="T2174" id="Seg_2903" s="T2173">стоять-IMP.2SG</ta>
            <ta e="T2175" id="Seg_2904" s="T2174">там</ta>
            <ta e="T2176" id="Seg_2905" s="T2175">а</ta>
            <ta e="T2177" id="Seg_2906" s="T2176">я.NOM</ta>
            <ta e="T2178" id="Seg_2907" s="T2177">здесь</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1890" id="Seg_2908" s="T1889">num-n:case</ta>
            <ta e="T1891" id="Seg_2909" s="T1890">n-n:case</ta>
            <ta e="T1893" id="Seg_2910" s="T1892">v-v:tense-v:pn</ta>
            <ta e="T1894" id="Seg_2911" s="T1893">n-n:case</ta>
            <ta e="T1895" id="Seg_2912" s="T1894">dempro-n:case</ta>
            <ta e="T1896" id="Seg_2913" s="T1895">adv</ta>
            <ta e="T1897" id="Seg_2914" s="T1896">dempro-n:case</ta>
            <ta e="T1898" id="Seg_2915" s="T1897">ptcl</ta>
            <ta e="T1900" id="Seg_2916" s="T1899">n-n:num-n:case.poss</ta>
            <ta e="T1901" id="Seg_2917" s="T1900">ptcl</ta>
            <ta e="T1902" id="Seg_2918" s="T1901">v-v:tense-v:pn</ta>
            <ta e="T1903" id="Seg_2919" s="T1902">n-n:case</ta>
            <ta e="T1904" id="Seg_2920" s="T1903">dempro-n:case</ta>
            <ta e="T1905" id="Seg_2921" s="T1904">adv</ta>
            <ta e="T1906" id="Seg_2922" s="T1905">v-v:tense-v:pn</ta>
            <ta e="T1908" id="Seg_2923" s="T1906">adv</ta>
            <ta e="T1913" id="Seg_2924" s="T1912">adv</ta>
            <ta e="T1915" id="Seg_2925" s="T1914">adj</ta>
            <ta e="T1916" id="Seg_2926" s="T1915">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1917" id="Seg_2927" s="T1916">dempro-n:case</ta>
            <ta e="T1918" id="Seg_2928" s="T1917">dempro-n:case</ta>
            <ta e="T1919" id="Seg_2929" s="T1918">dempro-n:case</ta>
            <ta e="T1920" id="Seg_2930" s="T1919">v-v:tense-v:pn</ta>
            <ta e="T1921" id="Seg_2931" s="T1920">v-v:tense</ta>
            <ta e="T1922" id="Seg_2932" s="T1921">conj</ta>
            <ta e="T1923" id="Seg_2933" s="T1922">v-v:tense-v:pn</ta>
            <ta e="T1924" id="Seg_2934" s="T1923">n-n:case</ta>
            <ta e="T1925" id="Seg_2935" s="T1924">v</ta>
            <ta e="T1926" id="Seg_2936" s="T1925">dempro-n:case</ta>
            <ta e="T1927" id="Seg_2937" s="T1926">v-v:n.fin</ta>
            <ta e="T1928" id="Seg_2938" s="T1927">adv</ta>
            <ta e="T1929" id="Seg_2939" s="T1928">ptcl</ta>
            <ta e="T1930" id="Seg_2940" s="T1929">v-v:n.fin</ta>
            <ta e="T1931" id="Seg_2941" s="T1930">v-v&gt;v-v:pn</ta>
            <ta e="T1932" id="Seg_2942" s="T1931">adv</ta>
            <ta e="T1933" id="Seg_2943" s="T1932">dempro-n:case</ta>
            <ta e="T1934" id="Seg_2944" s="T1933">dempro-n:case</ta>
            <ta e="T1936" id="Seg_2945" s="T1935">v-v:tense-v:pn</ta>
            <ta e="T1937" id="Seg_2946" s="T1936">n-n:case</ta>
            <ta e="T1938" id="Seg_2947" s="T1937">conj</ta>
            <ta e="T1939" id="Seg_2948" s="T1938">refl-n:case.poss</ta>
            <ta e="T1940" id="Seg_2949" s="T1939">v-v:n.fin</ta>
            <ta e="T1941" id="Seg_2950" s="T1940">v-v:tense-v:pn</ta>
            <ta e="T1942" id="Seg_2951" s="T1941">adv</ta>
            <ta e="T1943" id="Seg_2952" s="T1942">n-n:case</ta>
            <ta e="T1944" id="Seg_2953" s="T1943">v-v:tense-v:pn</ta>
            <ta e="T1945" id="Seg_2954" s="T1944">pers</ta>
            <ta e="T1946" id="Seg_2955" s="T1945">adv</ta>
            <ta e="T1947" id="Seg_2956" s="T1946">v-v&gt;v-v:pn</ta>
            <ta e="T1948" id="Seg_2957" s="T1947">pers</ta>
            <ta e="T1949" id="Seg_2958" s="T1948">ptcl</ta>
            <ta e="T1950" id="Seg_2959" s="T1949">v-v:tense-v:pn</ta>
            <ta e="T1951" id="Seg_2960" s="T1950">que</ta>
            <ta e="T1952" id="Seg_2961" s="T1951">pers</ta>
            <ta e="T1953" id="Seg_2962" s="T1952">v-v:tense-v:pn</ta>
            <ta e="T1954" id="Seg_2963" s="T1953">adv</ta>
            <ta e="T1955" id="Seg_2964" s="T1954">pers</ta>
            <ta e="T1956" id="Seg_2965" s="T1955">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1957" id="Seg_2966" s="T1956">conj</ta>
            <ta e="T1958" id="Seg_2967" s="T1957">que-n:case</ta>
            <ta e="T1959" id="Seg_2968" s="T1958">pers</ta>
            <ta e="T1960" id="Seg_2969" s="T1959">pers</ta>
            <ta e="T1961" id="Seg_2970" s="T1960">v-v:tense-v:pn</ta>
            <ta e="T1962" id="Seg_2971" s="T1961">pers</ta>
            <ta e="T1964" id="Seg_2972" s="T1962">adv</ta>
            <ta e="T1965" id="Seg_2973" s="T1964">n-n:case</ta>
            <ta e="T1966" id="Seg_2974" s="T1965">v</ta>
            <ta e="T1967" id="Seg_2975" s="T1966">conj</ta>
            <ta e="T1968" id="Seg_2976" s="T1967">adj-n:case</ta>
            <ta e="T1969" id="Seg_2977" s="T1968">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1970" id="Seg_2978" s="T1969">v-v:ins-v:mood.pn</ta>
            <ta e="T1971" id="Seg_2979" s="T1970">conj</ta>
            <ta e="T1972" id="Seg_2980" s="T1971">n-n:case</ta>
            <ta e="T1973" id="Seg_2981" s="T1972">v-v:mood.pn</ta>
            <ta e="T1974" id="Seg_2982" s="T1973">n-n:case</ta>
            <ta e="T1975" id="Seg_2983" s="T1974">pers</ta>
            <ta e="T1976" id="Seg_2984" s="T1975">conj</ta>
            <ta e="T1977" id="Seg_2985" s="T1976">v-v:tense-v:pn</ta>
            <ta e="T1978" id="Seg_2986" s="T1977">v-v:tense-v:pn</ta>
            <ta e="T1979" id="Seg_2987" s="T1978">n-n:case</ta>
            <ta e="T1980" id="Seg_2988" s="T1979">n-n:case</ta>
            <ta e="T1981" id="Seg_2989" s="T1980">dempro-n:case</ta>
            <ta e="T1982" id="Seg_2990" s="T1981">v-v:tense-v:pn</ta>
            <ta e="T1983" id="Seg_2991" s="T1982">adv</ta>
            <ta e="T1985" id="Seg_2992" s="T1983">v-v&gt;v-v:pn</ta>
            <ta e="T1986" id="Seg_2993" s="T1985">v-v:tense-v:pn</ta>
            <ta e="T1987" id="Seg_2994" s="T1986">num-n:case</ta>
            <ta e="T1988" id="Seg_2995" s="T1987">n-n:case</ta>
            <ta e="T0" id="Seg_2996" s="T1988">v-v:n-fin</ta>
            <ta e="T1989" id="Seg_2997" s="T0">v-v:tense-v:pn</ta>
            <ta e="T1990" id="Seg_2998" s="T1989">dempro-n:case</ta>
            <ta e="T1991" id="Seg_2999" s="T1990">v-v:tense-v:pn</ta>
            <ta e="T1992" id="Seg_3000" s="T1991">dempro-n:case</ta>
            <ta e="T1993" id="Seg_3001" s="T1992">ptcl</ta>
            <ta e="T1994" id="Seg_3002" s="T1993">que-n:case</ta>
            <ta e="T1995" id="Seg_3003" s="T1994">adv</ta>
            <ta e="T1996" id="Seg_3004" s="T1995">v-v:mood.pn</ta>
            <ta e="T1997" id="Seg_3005" s="T1996">pers</ta>
            <ta e="T1998" id="Seg_3006" s="T1997">n-n:case</ta>
            <ta e="T1999" id="Seg_3007" s="T1998">n-n:case</ta>
            <ta e="T2000" id="Seg_3008" s="T1999">dempro-n:case</ta>
            <ta e="T2001" id="Seg_3009" s="T2000">v-v:tense-v:pn</ta>
            <ta e="T2002" id="Seg_3010" s="T2001">n-n:case</ta>
            <ta e="T2003" id="Seg_3011" s="T2002">dempro-n:case</ta>
            <ta e="T2004" id="Seg_3012" s="T2003">v-v:tense-v:pn</ta>
            <ta e="T2005" id="Seg_3013" s="T2004">dempro-n:case</ta>
            <ta e="T2006" id="Seg_3014" s="T2005">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2007" id="Seg_3015" s="T2006">dempro-n:case</ta>
            <ta e="T2008" id="Seg_3016" s="T2007">n-n:case.poss</ta>
            <ta e="T2009" id="Seg_3017" s="T2008">adv</ta>
            <ta e="T2010" id="Seg_3018" s="T2009">adv</ta>
            <ta e="T2011" id="Seg_3019" s="T2010">v-v:tense-v:pn</ta>
            <ta e="T2012" id="Seg_3020" s="T2011">n-n:case</ta>
            <ta e="T2013" id="Seg_3021" s="T2012">que-n:case</ta>
            <ta e="T2014" id="Seg_3022" s="T2013">adj-n:case</ta>
            <ta e="T2015" id="Seg_3023" s="T2014">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2016" id="Seg_3024" s="T2015">ptcl</ta>
            <ta e="T2017" id="Seg_3025" s="T2016">adv</ta>
            <ta e="T2018" id="Seg_3026" s="T2017">n-n:case</ta>
            <ta e="T2019" id="Seg_3027" s="T2018">n-n:case</ta>
            <ta e="T2020" id="Seg_3028" s="T2019">v-v:tense-v:pn</ta>
            <ta e="T2021" id="Seg_3029" s="T2020">adv</ta>
            <ta e="T2022" id="Seg_3030" s="T2021">pers</ta>
            <ta e="T2023" id="Seg_3031" s="T2022">adj-n:case</ta>
            <ta e="T2024" id="Seg_3032" s="T2023">v-v:tense-v:pn</ta>
            <ta e="T2025" id="Seg_3033" s="T2024">dempro-n:case</ta>
            <ta e="T2026" id="Seg_3034" s="T2025">n-n:case.poss</ta>
            <ta e="T2027" id="Seg_3035" s="T2026">v-v:tense-v:pn</ta>
            <ta e="T2028" id="Seg_3036" s="T2027">dempro-n:case</ta>
            <ta e="T2029" id="Seg_3037" s="T2028">n-n:case</ta>
            <ta e="T2030" id="Seg_3038" s="T2029">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2031" id="Seg_3039" s="T2030">adv</ta>
            <ta e="T2032" id="Seg_3040" s="T2031">v-v:tense-v:pn</ta>
            <ta e="T2033" id="Seg_3041" s="T2032">adv</ta>
            <ta e="T2034" id="Seg_3042" s="T2033">ptcl</ta>
            <ta e="T2036" id="Seg_3043" s="T2035">n-n:case</ta>
            <ta e="T2037" id="Seg_3044" s="T2036">dempro-n:case</ta>
            <ta e="T2038" id="Seg_3045" s="T2037">v-v:n.fin</ta>
            <ta e="T2039" id="Seg_3046" s="T2038">v-v:tense-v:pn</ta>
            <ta e="T2040" id="Seg_3047" s="T2039">v-v:tense-v:pn</ta>
            <ta e="T2041" id="Seg_3048" s="T2040">ptcl</ta>
            <ta e="T2042" id="Seg_3049" s="T2041">n-n:case</ta>
            <ta e="T2043" id="Seg_3050" s="T2042">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2047" id="Seg_3051" s="T2046">dempro-n:case</ta>
            <ta e="T2048" id="Seg_3052" s="T2047">conj</ta>
            <ta e="T2049" id="Seg_3053" s="T2048">v-v&gt;v-v:pn</ta>
            <ta e="T2050" id="Seg_3054" s="T2049">n</ta>
            <ta e="T2051" id="Seg_3055" s="T2050">n-n:case.poss</ta>
            <ta e="T2052" id="Seg_3056" s="T2051">conj</ta>
            <ta e="T2053" id="Seg_3057" s="T2052">refl-n:case.poss</ta>
            <ta e="T2054" id="Seg_3058" s="T2053">n-n:case</ta>
            <ta e="T2055" id="Seg_3059" s="T2054">n-n:case.poss-n:case</ta>
            <ta e="T2056" id="Seg_3060" s="T2055">v-v&gt;v-v:pn</ta>
            <ta e="T2057" id="Seg_3061" s="T2056">conj</ta>
            <ta e="T2058" id="Seg_3062" s="T2057">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T2059" id="Seg_3063" s="T2058">conj</ta>
            <ta e="T2060" id="Seg_3064" s="T2059">n-n:case</ta>
            <ta e="T2061" id="Seg_3065" s="T2060">n-n:case</ta>
            <ta e="T2062" id="Seg_3066" s="T2061">adv</ta>
            <ta e="T2063" id="Seg_3067" s="T2062">que-n:case=ptcl</ta>
            <ta e="T2065" id="Seg_3068" s="T2064">que-n:case=ptcl</ta>
            <ta e="T2066" id="Seg_3069" s="T2065">ptcl</ta>
            <ta e="T2067" id="Seg_3070" s="T2066">v-v:tense-v:pn</ta>
            <ta e="T2068" id="Seg_3071" s="T2067">que-n:case</ta>
            <ta e="T2069" id="Seg_3072" s="T2068">quant</ta>
            <ta e="T2070" id="Seg_3073" s="T2069">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2071" id="Seg_3074" s="T2070">ptcl</ta>
            <ta e="T2072" id="Seg_3075" s="T2071">adv</ta>
            <ta e="T2073" id="Seg_3076" s="T2072">dempro-n:case</ta>
            <ta e="T2074" id="Seg_3077" s="T2073">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T2075" id="Seg_3078" s="T2074">n-n:case</ta>
            <ta e="T2076" id="Seg_3079" s="T2075">v-v&gt;v-v:pn</ta>
            <ta e="T2077" id="Seg_3080" s="T2076">pers</ta>
            <ta e="T2078" id="Seg_3081" s="T2077">n-n:case</ta>
            <ta e="T2079" id="Seg_3082" s="T2078">n-n:case</ta>
            <ta e="T2080" id="Seg_3083" s="T2079">pers</ta>
            <ta e="T2081" id="Seg_3084" s="T2080">pers</ta>
            <ta e="T2082" id="Seg_3085" s="T2081">v-v:tense-v:pn</ta>
            <ta e="T2083" id="Seg_3086" s="T2082">aux-v:mood.pn</ta>
            <ta e="T2084" id="Seg_3087" s="T2083">v-v:ins-v:mood.pn</ta>
            <ta e="T2085" id="Seg_3088" s="T2084">pers</ta>
            <ta e="T2086" id="Seg_3089" s="T2085">pers</ta>
            <ta e="T2087" id="Seg_3090" s="T2086">pers</ta>
            <ta e="T2088" id="Seg_3091" s="T2087">adv-n:case</ta>
            <ta e="T2089" id="Seg_3092" s="T2088">conj</ta>
            <ta e="T2090" id="Seg_3093" s="T2089">pers</ta>
            <ta e="T2091" id="Seg_3094" s="T2090">adv</ta>
            <ta e="T2092" id="Seg_3095" s="T2091">pers</ta>
            <ta e="T2094" id="Seg_3096" s="T2093">n-n:case.poss</ta>
            <ta e="T2095" id="Seg_3097" s="T2094">v-v:mood.pn</ta>
            <ta e="T2096" id="Seg_3098" s="T2095">v-v:mood.pn</ta>
            <ta e="T2098" id="Seg_3099" s="T2097">n-n:case.poss</ta>
            <ta e="T2099" id="Seg_3100" s="T2098">conj</ta>
            <ta e="T2100" id="Seg_3101" s="T2099">pers</ta>
            <ta e="T2101" id="Seg_3102" s="T2100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2102" id="Seg_3103" s="T2101">conj</ta>
            <ta e="T2104" id="Seg_3104" s="T2103">pers</ta>
            <ta e="T2105" id="Seg_3105" s="T2104">n-n:case.poss</ta>
            <ta e="T2106" id="Seg_3106" s="T2105">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2107" id="Seg_3107" s="T2106">adv</ta>
            <ta e="T2109" id="Seg_3108" s="T2108">dempro-n:case</ta>
            <ta e="T2110" id="Seg_3109" s="T2109">ptcl</ta>
            <ta e="T2111" id="Seg_3110" s="T2110">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2112" id="Seg_3111" s="T2111">dempro-n:case</ta>
            <ta e="T2113" id="Seg_3112" s="T2112">dempro-n:case</ta>
            <ta e="T2114" id="Seg_3113" s="T2113">n-n:case.poss</ta>
            <ta e="T2115" id="Seg_3114" s="T2114">ptcl</ta>
            <ta e="T2116" id="Seg_3115" s="T2115">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2117" id="Seg_3116" s="T2116">conj</ta>
            <ta e="T2118" id="Seg_3117" s="T2117">dempro-n:case</ta>
            <ta e="T2119" id="Seg_3118" s="T2118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2120" id="Seg_3119" s="T2119">conj</ta>
            <ta e="T2121" id="Seg_3120" s="T2120">adv</ta>
            <ta e="T2122" id="Seg_3121" s="T2121">ptcl</ta>
            <ta e="T2123" id="Seg_3122" s="T2122">v-v:tense-v:pn</ta>
            <ta e="T2124" id="Seg_3123" s="T2123">conj</ta>
            <ta e="T2125" id="Seg_3124" s="T2124">n-n:case</ta>
            <ta e="T2126" id="Seg_3125" s="T2125">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2127" id="Seg_3126" s="T2126">dempro-n:case</ta>
            <ta e="T2128" id="Seg_3127" s="T2127">v-v:tense-v:pn</ta>
            <ta e="T2129" id="Seg_3128" s="T2128">v-v:tense-v:pn</ta>
            <ta e="T2130" id="Seg_3129" s="T2129">pers</ta>
            <ta e="T2131" id="Seg_3130" s="T2130">n-n:case</ta>
            <ta e="T2132" id="Seg_3131" s="T2131">conj</ta>
            <ta e="T2133" id="Seg_3132" s="T2132">ptcl</ta>
            <ta e="T2134" id="Seg_3133" s="T2133">v-v:tense-v:pn</ta>
            <ta e="T2135" id="Seg_3134" s="T2134">v-v:tense-v:pn</ta>
            <ta e="T2136" id="Seg_3135" s="T2135">ptcl</ta>
            <ta e="T2137" id="Seg_3136" s="T2136">ptcl</ta>
            <ta e="T2138" id="Seg_3137" s="T2137">pers</ta>
            <ta e="T2139" id="Seg_3138" s="T2138">n-n:case.poss</ta>
            <ta e="T2140" id="Seg_3139" s="T2139">ptcl</ta>
            <ta e="T2141" id="Seg_3140" s="T2140">adj-n:case</ta>
            <ta e="T2142" id="Seg_3141" s="T2141">v-v:tense-v:pn</ta>
            <ta e="T2144" id="Seg_3142" s="T2143">ptcl</ta>
            <ta e="T2145" id="Seg_3143" s="T2144">v-v:tense-v:pn</ta>
            <ta e="T2146" id="Seg_3144" s="T2145">v-v:tense-v:pn</ta>
            <ta e="T2147" id="Seg_3145" s="T2146">n-n:num-n:case</ta>
            <ta e="T2148" id="Seg_3146" s="T2147">v-v:tense-v:pn</ta>
            <ta e="T2149" id="Seg_3147" s="T2148">n-n:case</ta>
            <ta e="T2150" id="Seg_3148" s="T2149">v-v:tense-v:pn</ta>
            <ta e="T2151" id="Seg_3149" s="T2150">adj-n:num</ta>
            <ta e="T2152" id="Seg_3150" s="T2151">n-n:num</ta>
            <ta e="T2153" id="Seg_3151" s="T2152">v</ta>
            <ta e="T2154" id="Seg_3152" s="T2153">v-v:n.fin</ta>
            <ta e="T2155" id="Seg_3153" s="T2154">num-n:case</ta>
            <ta e="T2156" id="Seg_3154" s="T2155">adj-n:case</ta>
            <ta e="T2157" id="Seg_3155" s="T2156">n-n:case</ta>
            <ta e="T2158" id="Seg_3156" s="T2157">n-n:case</ta>
            <ta e="T2159" id="Seg_3157" s="T2158">v-v&gt;v-v:pn</ta>
            <ta e="T2160" id="Seg_3158" s="T2159">aux-v:mood.pn</ta>
            <ta e="T2161" id="Seg_3159" s="T2160">v-v:ins-v:mood.pn</ta>
            <ta e="T2162" id="Seg_3160" s="T2161">dempro-n:num</ta>
            <ta e="T2163" id="Seg_3161" s="T2162">adj-n:num</ta>
            <ta e="T2164" id="Seg_3162" s="T2163">v-v:mood-v:pn</ta>
            <ta e="T2165" id="Seg_3163" s="T2164">n-n:case</ta>
            <ta e="T2166" id="Seg_3164" s="T2165">conj</ta>
            <ta e="T2168" id="Seg_3165" s="T2167">dempro-n:case</ta>
            <ta e="T2170" id="Seg_3166" s="T2169">v-v:n.fin</ta>
            <ta e="T2171" id="Seg_3167" s="T2170">adv</ta>
            <ta e="T2172" id="Seg_3168" s="T2171">v-v:tense-v:pn</ta>
            <ta e="T2173" id="Seg_3169" s="T2172">pers</ta>
            <ta e="T2174" id="Seg_3170" s="T2173">v-v:mood.pn</ta>
            <ta e="T2175" id="Seg_3171" s="T2174">adv</ta>
            <ta e="T2176" id="Seg_3172" s="T2175">conj</ta>
            <ta e="T2177" id="Seg_3173" s="T2176">pers</ta>
            <ta e="T2178" id="Seg_3174" s="T2177">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1890" id="Seg_3175" s="T1889">num</ta>
            <ta e="T1891" id="Seg_3176" s="T1890">n</ta>
            <ta e="T1893" id="Seg_3177" s="T1892">v</ta>
            <ta e="T1894" id="Seg_3178" s="T1893">n</ta>
            <ta e="T1895" id="Seg_3179" s="T1894">dempro</ta>
            <ta e="T1896" id="Seg_3180" s="T1895">adv</ta>
            <ta e="T1897" id="Seg_3181" s="T1896">dempro</ta>
            <ta e="T1898" id="Seg_3182" s="T1897">ptcl</ta>
            <ta e="T1900" id="Seg_3183" s="T1899">n</ta>
            <ta e="T1901" id="Seg_3184" s="T1900">ptcl</ta>
            <ta e="T1902" id="Seg_3185" s="T1901">v</ta>
            <ta e="T1903" id="Seg_3186" s="T1902">n</ta>
            <ta e="T1904" id="Seg_3187" s="T1903">dempro</ta>
            <ta e="T1905" id="Seg_3188" s="T1904">adv</ta>
            <ta e="T1906" id="Seg_3189" s="T1905">v</ta>
            <ta e="T1908" id="Seg_3190" s="T1906">adv</ta>
            <ta e="T1913" id="Seg_3191" s="T1912">adv</ta>
            <ta e="T1915" id="Seg_3192" s="T1914">adj</ta>
            <ta e="T1916" id="Seg_3193" s="T1915">v</ta>
            <ta e="T1917" id="Seg_3194" s="T1916">dempro</ta>
            <ta e="T1918" id="Seg_3195" s="T1917">dempro</ta>
            <ta e="T1919" id="Seg_3196" s="T1918">dempro</ta>
            <ta e="T1920" id="Seg_3197" s="T1919">v</ta>
            <ta e="T1921" id="Seg_3198" s="T1920">v</ta>
            <ta e="T1922" id="Seg_3199" s="T1921">conj</ta>
            <ta e="T1923" id="Seg_3200" s="T1922">v</ta>
            <ta e="T1924" id="Seg_3201" s="T1923">n</ta>
            <ta e="T1925" id="Seg_3202" s="T1924">v</ta>
            <ta e="T1926" id="Seg_3203" s="T1925">dempro</ta>
            <ta e="T1927" id="Seg_3204" s="T1926">v</ta>
            <ta e="T1928" id="Seg_3205" s="T1927">adv</ta>
            <ta e="T1929" id="Seg_3206" s="T1928">ptcl</ta>
            <ta e="T1930" id="Seg_3207" s="T1929">v</ta>
            <ta e="T1931" id="Seg_3208" s="T1930">v</ta>
            <ta e="T1932" id="Seg_3209" s="T1931">adv</ta>
            <ta e="T1933" id="Seg_3210" s="T1932">dempro</ta>
            <ta e="T1934" id="Seg_3211" s="T1933">dempro</ta>
            <ta e="T1936" id="Seg_3212" s="T1935">v</ta>
            <ta e="T1937" id="Seg_3213" s="T1936">n</ta>
            <ta e="T1938" id="Seg_3214" s="T1937">conj</ta>
            <ta e="T1939" id="Seg_3215" s="T1938">refl</ta>
            <ta e="T1940" id="Seg_3216" s="T1939">v</ta>
            <ta e="T1941" id="Seg_3217" s="T1940">v</ta>
            <ta e="T1942" id="Seg_3218" s="T1941">adv</ta>
            <ta e="T1943" id="Seg_3219" s="T1942">n</ta>
            <ta e="T1944" id="Seg_3220" s="T1943">v</ta>
            <ta e="T1945" id="Seg_3221" s="T1944">pers</ta>
            <ta e="T1946" id="Seg_3222" s="T1945">adv</ta>
            <ta e="T1947" id="Seg_3223" s="T1946">v</ta>
            <ta e="T1948" id="Seg_3224" s="T1947">pers</ta>
            <ta e="T1949" id="Seg_3225" s="T1948">ptcl</ta>
            <ta e="T1950" id="Seg_3226" s="T1949">v</ta>
            <ta e="T1951" id="Seg_3227" s="T1950">conj</ta>
            <ta e="T1952" id="Seg_3228" s="T1951">pers</ta>
            <ta e="T1953" id="Seg_3229" s="T1952">v</ta>
            <ta e="T1954" id="Seg_3230" s="T1953">adv</ta>
            <ta e="T1955" id="Seg_3231" s="T1954">pers</ta>
            <ta e="T1956" id="Seg_3232" s="T1955">v</ta>
            <ta e="T1957" id="Seg_3233" s="T1956">conj</ta>
            <ta e="T1958" id="Seg_3234" s="T1957">que</ta>
            <ta e="T1959" id="Seg_3235" s="T1958">pers</ta>
            <ta e="T1960" id="Seg_3236" s="T1959">pers</ta>
            <ta e="T1961" id="Seg_3237" s="T1960">v</ta>
            <ta e="T1962" id="Seg_3238" s="T1961">pers</ta>
            <ta e="T1964" id="Seg_3239" s="T1962">adv</ta>
            <ta e="T1965" id="Seg_3240" s="T1964">n</ta>
            <ta e="T1966" id="Seg_3241" s="T1965">v</ta>
            <ta e="T1967" id="Seg_3242" s="T1966">conj</ta>
            <ta e="T1968" id="Seg_3243" s="T1967">adj</ta>
            <ta e="T1969" id="Seg_3244" s="T1968">v</ta>
            <ta e="T1970" id="Seg_3245" s="T1969">v</ta>
            <ta e="T1971" id="Seg_3246" s="T1970">conj</ta>
            <ta e="T1972" id="Seg_3247" s="T1971">n</ta>
            <ta e="T1973" id="Seg_3248" s="T1972">v</ta>
            <ta e="T1974" id="Seg_3249" s="T1973">n</ta>
            <ta e="T1975" id="Seg_3250" s="T1974">pers</ta>
            <ta e="T1976" id="Seg_3251" s="T1975">conj</ta>
            <ta e="T1977" id="Seg_3252" s="T1976">v</ta>
            <ta e="T1978" id="Seg_3253" s="T1977">v</ta>
            <ta e="T1979" id="Seg_3254" s="T1978">n</ta>
            <ta e="T1980" id="Seg_3255" s="T1979">n</ta>
            <ta e="T1981" id="Seg_3256" s="T1980">dempro</ta>
            <ta e="T1982" id="Seg_3257" s="T1981">v</ta>
            <ta e="T1983" id="Seg_3258" s="T1982">adv</ta>
            <ta e="T1985" id="Seg_3259" s="T1983">v</ta>
            <ta e="T1986" id="Seg_3260" s="T1985">v</ta>
            <ta e="T1987" id="Seg_3261" s="T1986">num</ta>
            <ta e="T1988" id="Seg_3262" s="T1987">n</ta>
            <ta e="T0" id="Seg_3263" s="T1988">v</ta>
            <ta e="T1989" id="Seg_3264" s="T0">v</ta>
            <ta e="T1990" id="Seg_3265" s="T1989">dempro</ta>
            <ta e="T1991" id="Seg_3266" s="T1990">v</ta>
            <ta e="T1992" id="Seg_3267" s="T1991">dempro</ta>
            <ta e="T1993" id="Seg_3268" s="T1992">ptcl</ta>
            <ta e="T1994" id="Seg_3269" s="T1993">que</ta>
            <ta e="T1995" id="Seg_3270" s="T1994">adv</ta>
            <ta e="T1996" id="Seg_3271" s="T1995">v</ta>
            <ta e="T1997" id="Seg_3272" s="T1996">pers</ta>
            <ta e="T1998" id="Seg_3273" s="T1997">n</ta>
            <ta e="T1999" id="Seg_3274" s="T1998">n</ta>
            <ta e="T2000" id="Seg_3275" s="T1999">dempro</ta>
            <ta e="T2001" id="Seg_3276" s="T2000">v</ta>
            <ta e="T2002" id="Seg_3277" s="T2001">n</ta>
            <ta e="T2003" id="Seg_3278" s="T2002">dempro</ta>
            <ta e="T2004" id="Seg_3279" s="T2003">v</ta>
            <ta e="T2005" id="Seg_3280" s="T2004">dempro</ta>
            <ta e="T2006" id="Seg_3281" s="T2005">v</ta>
            <ta e="T2007" id="Seg_3282" s="T2006">dempro</ta>
            <ta e="T2008" id="Seg_3283" s="T2007">n</ta>
            <ta e="T2009" id="Seg_3284" s="T2008">adv</ta>
            <ta e="T2010" id="Seg_3285" s="T2009">adv</ta>
            <ta e="T2011" id="Seg_3286" s="T2010">v</ta>
            <ta e="T2012" id="Seg_3287" s="T2011">n</ta>
            <ta e="T2013" id="Seg_3288" s="T2012">que</ta>
            <ta e="T2014" id="Seg_3289" s="T2013">adj</ta>
            <ta e="T2015" id="Seg_3290" s="T2014">v</ta>
            <ta e="T2016" id="Seg_3291" s="T2015">ptcl</ta>
            <ta e="T2017" id="Seg_3292" s="T2016">adv</ta>
            <ta e="T2018" id="Seg_3293" s="T2017">n</ta>
            <ta e="T2019" id="Seg_3294" s="T2018">n</ta>
            <ta e="T2020" id="Seg_3295" s="T2019">v</ta>
            <ta e="T2021" id="Seg_3296" s="T2020">adv</ta>
            <ta e="T2022" id="Seg_3297" s="T2021">pers</ta>
            <ta e="T2023" id="Seg_3298" s="T2022">adj</ta>
            <ta e="T2024" id="Seg_3299" s="T2023">v</ta>
            <ta e="T2025" id="Seg_3300" s="T2024">dempro</ta>
            <ta e="T2026" id="Seg_3301" s="T2025">n</ta>
            <ta e="T2027" id="Seg_3302" s="T2026">v</ta>
            <ta e="T2028" id="Seg_3303" s="T2027">dempro</ta>
            <ta e="T2029" id="Seg_3304" s="T2028">n</ta>
            <ta e="T2030" id="Seg_3305" s="T2029">v</ta>
            <ta e="T2031" id="Seg_3306" s="T2030">adv</ta>
            <ta e="T2032" id="Seg_3307" s="T2031">v</ta>
            <ta e="T2033" id="Seg_3308" s="T2032">adv</ta>
            <ta e="T2034" id="Seg_3309" s="T2033">ptcl</ta>
            <ta e="T2036" id="Seg_3310" s="T2035">n</ta>
            <ta e="T2037" id="Seg_3311" s="T2036">dempro</ta>
            <ta e="T2038" id="Seg_3312" s="T2037">v</ta>
            <ta e="T2039" id="Seg_3313" s="T2038">v</ta>
            <ta e="T2040" id="Seg_3314" s="T2039">v</ta>
            <ta e="T2041" id="Seg_3315" s="T2040">ptcl</ta>
            <ta e="T2042" id="Seg_3316" s="T2041">n</ta>
            <ta e="T2043" id="Seg_3317" s="T2042">v</ta>
            <ta e="T2047" id="Seg_3318" s="T2046">dempro</ta>
            <ta e="T2048" id="Seg_3319" s="T2047">conj</ta>
            <ta e="T2049" id="Seg_3320" s="T2048">v</ta>
            <ta e="T2050" id="Seg_3321" s="T2049">n</ta>
            <ta e="T2051" id="Seg_3322" s="T2050">n</ta>
            <ta e="T2052" id="Seg_3323" s="T2051">conj</ta>
            <ta e="T2053" id="Seg_3324" s="T2052">refl</ta>
            <ta e="T2054" id="Seg_3325" s="T2053">n</ta>
            <ta e="T2055" id="Seg_3326" s="T2054">n</ta>
            <ta e="T2056" id="Seg_3327" s="T2055">v</ta>
            <ta e="T2057" id="Seg_3328" s="T2056">conj</ta>
            <ta e="T2058" id="Seg_3329" s="T2057">v</ta>
            <ta e="T2059" id="Seg_3330" s="T2058">conj</ta>
            <ta e="T2060" id="Seg_3331" s="T2059">n</ta>
            <ta e="T2061" id="Seg_3332" s="T2060">n</ta>
            <ta e="T2062" id="Seg_3333" s="T2061">adv</ta>
            <ta e="T2063" id="Seg_3334" s="T2062">que</ta>
            <ta e="T2065" id="Seg_3335" s="T2064">que</ta>
            <ta e="T2066" id="Seg_3336" s="T2065">ptcl</ta>
            <ta e="T2067" id="Seg_3337" s="T2066">v</ta>
            <ta e="T2068" id="Seg_3338" s="T2067">que</ta>
            <ta e="T2069" id="Seg_3339" s="T2068">quant</ta>
            <ta e="T2070" id="Seg_3340" s="T2069">v</ta>
            <ta e="T2071" id="Seg_3341" s="T2070">ptcl</ta>
            <ta e="T2072" id="Seg_3342" s="T2071">adv</ta>
            <ta e="T2073" id="Seg_3343" s="T2072">dempro</ta>
            <ta e="T2074" id="Seg_3344" s="T2073">v</ta>
            <ta e="T2075" id="Seg_3345" s="T2074">n</ta>
            <ta e="T2076" id="Seg_3346" s="T2075">v</ta>
            <ta e="T2077" id="Seg_3347" s="T2076">pers</ta>
            <ta e="T2078" id="Seg_3348" s="T2077">n</ta>
            <ta e="T2079" id="Seg_3349" s="T2078">n</ta>
            <ta e="T2080" id="Seg_3350" s="T2079">pers</ta>
            <ta e="T2081" id="Seg_3351" s="T2080">pers</ta>
            <ta e="T2082" id="Seg_3352" s="T2081">v</ta>
            <ta e="T2083" id="Seg_3353" s="T2082">aux</ta>
            <ta e="T2084" id="Seg_3354" s="T2083">v</ta>
            <ta e="T2085" id="Seg_3355" s="T2084">pers</ta>
            <ta e="T2086" id="Seg_3356" s="T2085">pers</ta>
            <ta e="T2087" id="Seg_3357" s="T2086">pers</ta>
            <ta e="T2088" id="Seg_3358" s="T2087">adv</ta>
            <ta e="T2089" id="Seg_3359" s="T2088">conj</ta>
            <ta e="T2090" id="Seg_3360" s="T2089">pers</ta>
            <ta e="T2091" id="Seg_3361" s="T2090">adv</ta>
            <ta e="T2092" id="Seg_3362" s="T2091">pers</ta>
            <ta e="T2094" id="Seg_3363" s="T2093">n</ta>
            <ta e="T2095" id="Seg_3364" s="T2094">v</ta>
            <ta e="T2096" id="Seg_3365" s="T2095">v</ta>
            <ta e="T2098" id="Seg_3366" s="T2097">n</ta>
            <ta e="T2099" id="Seg_3367" s="T2098">conj</ta>
            <ta e="T2100" id="Seg_3368" s="T2099">pers</ta>
            <ta e="T2101" id="Seg_3369" s="T2100">v</ta>
            <ta e="T2102" id="Seg_3370" s="T2101">conj</ta>
            <ta e="T2104" id="Seg_3371" s="T2103">pers</ta>
            <ta e="T2105" id="Seg_3372" s="T2104">n</ta>
            <ta e="T2106" id="Seg_3373" s="T2105">v</ta>
            <ta e="T2107" id="Seg_3374" s="T2106">adv</ta>
            <ta e="T2109" id="Seg_3375" s="T2108">dempro</ta>
            <ta e="T2110" id="Seg_3376" s="T2109">ptcl</ta>
            <ta e="T2111" id="Seg_3377" s="T2110">v</ta>
            <ta e="T2112" id="Seg_3378" s="T2111">dempro</ta>
            <ta e="T2113" id="Seg_3379" s="T2112">dempro</ta>
            <ta e="T2114" id="Seg_3380" s="T2113">n</ta>
            <ta e="T2115" id="Seg_3381" s="T2114">ptcl</ta>
            <ta e="T2116" id="Seg_3382" s="T2115">v</ta>
            <ta e="T2117" id="Seg_3383" s="T2116">conj</ta>
            <ta e="T2118" id="Seg_3384" s="T2117">dempro</ta>
            <ta e="T2119" id="Seg_3385" s="T2118">v</ta>
            <ta e="T2120" id="Seg_3386" s="T2119">conj</ta>
            <ta e="T2121" id="Seg_3387" s="T2120">adv</ta>
            <ta e="T2122" id="Seg_3388" s="T2121">ptcl</ta>
            <ta e="T2123" id="Seg_3389" s="T2122">v</ta>
            <ta e="T2124" id="Seg_3390" s="T2123">conj</ta>
            <ta e="T2125" id="Seg_3391" s="T2124">n</ta>
            <ta e="T2126" id="Seg_3392" s="T2125">v</ta>
            <ta e="T2127" id="Seg_3393" s="T2126">dempro</ta>
            <ta e="T2128" id="Seg_3394" s="T2127">v</ta>
            <ta e="T2129" id="Seg_3395" s="T2128">v</ta>
            <ta e="T2130" id="Seg_3396" s="T2129">pers</ta>
            <ta e="T2131" id="Seg_3397" s="T2130">n</ta>
            <ta e="T2132" id="Seg_3398" s="T2131">conj</ta>
            <ta e="T2133" id="Seg_3399" s="T2132">ptcl</ta>
            <ta e="T2134" id="Seg_3400" s="T2133">v</ta>
            <ta e="T2135" id="Seg_3401" s="T2134">v</ta>
            <ta e="T2136" id="Seg_3402" s="T2135">ptcl</ta>
            <ta e="T2137" id="Seg_3403" s="T2136">ptcl</ta>
            <ta e="T2138" id="Seg_3404" s="T2137">pers</ta>
            <ta e="T2139" id="Seg_3405" s="T2138">n</ta>
            <ta e="T2140" id="Seg_3406" s="T2139">ptcl</ta>
            <ta e="T2141" id="Seg_3407" s="T2140">adj</ta>
            <ta e="T2142" id="Seg_3408" s="T2141">v</ta>
            <ta e="T2144" id="Seg_3409" s="T2143">ptcl</ta>
            <ta e="T2145" id="Seg_3410" s="T2144">v</ta>
            <ta e="T2146" id="Seg_3411" s="T2145">v</ta>
            <ta e="T2147" id="Seg_3412" s="T2146">n</ta>
            <ta e="T2148" id="Seg_3413" s="T2147">v</ta>
            <ta e="T2149" id="Seg_3414" s="T2148">n</ta>
            <ta e="T2150" id="Seg_3415" s="T2149">v</ta>
            <ta e="T2151" id="Seg_3416" s="T2150">adj</ta>
            <ta e="T2152" id="Seg_3417" s="T2151">n</ta>
            <ta e="T2153" id="Seg_3418" s="T2152">v</ta>
            <ta e="T2154" id="Seg_3419" s="T2153">v</ta>
            <ta e="T2155" id="Seg_3420" s="T2154">num</ta>
            <ta e="T2156" id="Seg_3421" s="T2155">adj</ta>
            <ta e="T2157" id="Seg_3422" s="T2156">n</ta>
            <ta e="T2158" id="Seg_3423" s="T2157">n</ta>
            <ta e="T2159" id="Seg_3424" s="T2158">v</ta>
            <ta e="T2160" id="Seg_3425" s="T2159">aux</ta>
            <ta e="T2161" id="Seg_3426" s="T2160">v</ta>
            <ta e="T2162" id="Seg_3427" s="T2161">dempro</ta>
            <ta e="T2163" id="Seg_3428" s="T2162">adj</ta>
            <ta e="T2164" id="Seg_3429" s="T2163">v</ta>
            <ta e="T2165" id="Seg_3430" s="T2164">n</ta>
            <ta e="T2166" id="Seg_3431" s="T2165">conj</ta>
            <ta e="T2168" id="Seg_3432" s="T2167">dempro</ta>
            <ta e="T2170" id="Seg_3433" s="T2169">v</ta>
            <ta e="T2171" id="Seg_3434" s="T2170">adv</ta>
            <ta e="T2172" id="Seg_3435" s="T2171">v</ta>
            <ta e="T2173" id="Seg_3436" s="T2172">pers</ta>
            <ta e="T2174" id="Seg_3437" s="T2173">v</ta>
            <ta e="T2175" id="Seg_3438" s="T2174">adv</ta>
            <ta e="T2176" id="Seg_3439" s="T2175">conj</ta>
            <ta e="T2177" id="Seg_3440" s="T2176">pers</ta>
            <ta e="T2178" id="Seg_3441" s="T2177">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1891" id="Seg_3442" s="T1890">np.h:Poss</ta>
            <ta e="T1894" id="Seg_3443" s="T1893">np:Th</ta>
            <ta e="T1895" id="Seg_3444" s="T1894">pro.h:A</ta>
            <ta e="T1896" id="Seg_3445" s="T1895">adv:Time</ta>
            <ta e="T1897" id="Seg_3446" s="T1896">pro.h:Poss</ta>
            <ta e="T1900" id="Seg_3447" s="T1899">np:Th</ta>
            <ta e="T1903" id="Seg_3448" s="T1902">np.h:R</ta>
            <ta e="T1904" id="Seg_3449" s="T1903">pro.h:B</ta>
            <ta e="T1905" id="Seg_3450" s="T1904">adv:Time</ta>
            <ta e="T1906" id="Seg_3451" s="T1905">0.3.h:A</ta>
            <ta e="T1908" id="Seg_3452" s="T1906">adv:Time</ta>
            <ta e="T1913" id="Seg_3453" s="T1912">adv:Time</ta>
            <ta e="T1916" id="Seg_3454" s="T1915">0.3.h:E</ta>
            <ta e="T1917" id="Seg_3455" s="T1916">pro.h:A</ta>
            <ta e="T1919" id="Seg_3456" s="T1918">pro.h:Th</ta>
            <ta e="T1923" id="Seg_3457" s="T1922">0.3.h:A</ta>
            <ta e="T1924" id="Seg_3458" s="T1923">np:G</ta>
            <ta e="T1926" id="Seg_3459" s="T1925">pro.h:P</ta>
            <ta e="T1928" id="Seg_3460" s="T1927">adv:L</ta>
            <ta e="T1931" id="Seg_3461" s="T1930">0.3.h:E</ta>
            <ta e="T1932" id="Seg_3462" s="T1931">adv:Time</ta>
            <ta e="T1933" id="Seg_3463" s="T1932">pro.h:Th</ta>
            <ta e="T1934" id="Seg_3464" s="T1933">pro:Th</ta>
            <ta e="T1936" id="Seg_3465" s="T1935">0.3.h:A</ta>
            <ta e="T1937" id="Seg_3466" s="T1936">np:L</ta>
            <ta e="T1941" id="Seg_3467" s="T1940">0.3.h:A</ta>
            <ta e="T1942" id="Seg_3468" s="T1941">adv:Time</ta>
            <ta e="T1943" id="Seg_3469" s="T1942">np.h:A</ta>
            <ta e="T1945" id="Seg_3470" s="T1944">pro.h:E</ta>
            <ta e="T1946" id="Seg_3471" s="T1945">adv:L</ta>
            <ta e="T1948" id="Seg_3472" s="T1947">pro.h:Th</ta>
            <ta e="T1950" id="Seg_3473" s="T1949">0.2.h:A</ta>
            <ta e="T1952" id="Seg_3474" s="T1951">pro.h:A</ta>
            <ta e="T1954" id="Seg_3475" s="T1953">adv:Time</ta>
            <ta e="T1955" id="Seg_3476" s="T1954">pro.h:P</ta>
            <ta e="T1956" id="Seg_3477" s="T1955">0.1.h:A</ta>
            <ta e="T1959" id="Seg_3478" s="T1958">pro.h:A</ta>
            <ta e="T1960" id="Seg_3479" s="T1959">pro.h:P</ta>
            <ta e="T1962" id="Seg_3480" s="T1961">pro.h:Poss</ta>
            <ta e="T1965" id="Seg_3481" s="T1964">np:Th</ta>
            <ta e="T1969" id="Seg_3482" s="T1968">0.1.h:P</ta>
            <ta e="T1972" id="Seg_3483" s="T1971">np:Th</ta>
            <ta e="T1974" id="Seg_3484" s="T1973">np:Poss</ta>
            <ta e="T1975" id="Seg_3485" s="T1974">pro.h:R</ta>
            <ta e="T1977" id="Seg_3486" s="T1976">0.3.h:A</ta>
            <ta e="T1979" id="Seg_3487" s="T1978">np:Th</ta>
            <ta e="T1980" id="Seg_3488" s="T1979">np:Poss</ta>
            <ta e="T1981" id="Seg_3489" s="T1980">pro.h:A</ta>
            <ta e="T1983" id="Seg_3490" s="T1982">adv:Time</ta>
            <ta e="T1985" id="Seg_3491" s="T1983">0.3.h:A</ta>
            <ta e="T1986" id="Seg_3492" s="T1985">0.3.h:A</ta>
            <ta e="T1988" id="Seg_3493" s="T1987">n:Time</ta>
            <ta e="T1990" id="Seg_3494" s="T1989">pro.h:A</ta>
            <ta e="T1992" id="Seg_3495" s="T1991">pro:G</ta>
            <ta e="T1997" id="Seg_3496" s="T1996">pro.h:R</ta>
            <ta e="T1998" id="Seg_3497" s="T1997">np:Poss</ta>
            <ta e="T1999" id="Seg_3498" s="T1998">np:Th</ta>
            <ta e="T2000" id="Seg_3499" s="T1999">pro.h:A</ta>
            <ta e="T2002" id="Seg_3500" s="T2001">np:Th</ta>
            <ta e="T2003" id="Seg_3501" s="T2002">pro.h:R</ta>
            <ta e="T2004" id="Seg_3502" s="T2003">0.3.h:A</ta>
            <ta e="T2005" id="Seg_3503" s="T2004">pro.h:A</ta>
            <ta e="T2008" id="Seg_3504" s="T2007">np:Th</ta>
            <ta e="T2009" id="Seg_3505" s="T2008">adv:Time</ta>
            <ta e="T2012" id="Seg_3506" s="T2011">np.h:A</ta>
            <ta e="T2015" id="Seg_3507" s="T2014">0.1.h:E</ta>
            <ta e="T2018" id="Seg_3508" s="T2017">np:Poss</ta>
            <ta e="T2019" id="Seg_3509" s="T2018">np:Th</ta>
            <ta e="T2020" id="Seg_3510" s="T2019">0.2.h:A</ta>
            <ta e="T2021" id="Seg_3511" s="T2020">adv:Time</ta>
            <ta e="T2022" id="Seg_3512" s="T2021">pro.h:E</ta>
            <ta e="T2025" id="Seg_3513" s="T2024">pro.h:A</ta>
            <ta e="T2026" id="Seg_3514" s="T2025">np:Th</ta>
            <ta e="T2029" id="Seg_3515" s="T2028">np:A</ta>
            <ta e="T2031" id="Seg_3516" s="T2030">adv:Time</ta>
            <ta e="T2032" id="Seg_3517" s="T2031">0.3.h:A</ta>
            <ta e="T2033" id="Seg_3518" s="T2032">adv:Time</ta>
            <ta e="T2036" id="Seg_3519" s="T2035">np:A</ta>
            <ta e="T2037" id="Seg_3520" s="T2036">pro.h:P</ta>
            <ta e="T2039" id="Seg_3521" s="T2038">0.3:A</ta>
            <ta e="T2040" id="Seg_3522" s="T2039">0.3:A</ta>
            <ta e="T2042" id="Seg_3523" s="T2041">np.h:A</ta>
            <ta e="T2047" id="Seg_3524" s="T2046">pro:So</ta>
            <ta e="T2049" id="Seg_3525" s="T2048">0.3.h:E</ta>
            <ta e="T2050" id="Seg_3526" s="T2049">np:Poss</ta>
            <ta e="T2051" id="Seg_3527" s="T2050">np:L</ta>
            <ta e="T2054" id="Seg_3528" s="T2053">np:P</ta>
            <ta e="T2055" id="Seg_3529" s="T2054">np:Ins</ta>
            <ta e="T2056" id="Seg_3530" s="T2055">0.3.h:A</ta>
            <ta e="T2058" id="Seg_3531" s="T2057">0.3.h:A</ta>
            <ta e="T2059" id="Seg_3532" s="T2058">adv:Time</ta>
            <ta e="T2061" id="Seg_3533" s="T2060">np:Th</ta>
            <ta e="T2062" id="Seg_3534" s="T2061">adv:Time</ta>
            <ta e="T2063" id="Seg_3535" s="T2062">pro:B</ta>
            <ta e="T2065" id="Seg_3536" s="T2064">pro:B</ta>
            <ta e="T2067" id="Seg_3537" s="T2066">0.1.h:E</ta>
            <ta e="T2068" id="Seg_3538" s="T2067">pro:P</ta>
            <ta e="T2070" id="Seg_3539" s="T2069">0.1.h:A</ta>
            <ta e="T2072" id="Seg_3540" s="T2071">adv:Time</ta>
            <ta e="T2073" id="Seg_3541" s="T2072">pro.h:A</ta>
            <ta e="T2075" id="Seg_3542" s="T2074">np.h:Th</ta>
            <ta e="T2077" id="Seg_3543" s="T2076">pro.h:A</ta>
            <ta e="T2080" id="Seg_3544" s="T2079">pro.h:A</ta>
            <ta e="T2081" id="Seg_3545" s="T2080">pro.h:P</ta>
            <ta e="T2083" id="Seg_3546" s="T2082">0.2.h:A</ta>
            <ta e="T2085" id="Seg_3547" s="T2084">pro.h:P</ta>
            <ta e="T2087" id="Seg_3548" s="T2086">pro.h:Th</ta>
            <ta e="T2088" id="Seg_3549" s="T2087">adv:L</ta>
            <ta e="T2090" id="Seg_3550" s="T2089">pro.h:Th</ta>
            <ta e="T2091" id="Seg_3551" s="T2090">adv:L</ta>
            <ta e="T2094" id="Seg_3552" s="T2093">np:Th 0.2.h:Poss</ta>
            <ta e="T2095" id="Seg_3553" s="T2094">0.2.h:A</ta>
            <ta e="T2096" id="Seg_3554" s="T2095">0.2.h:A</ta>
            <ta e="T2098" id="Seg_3555" s="T2097">np:Th 0.2.h:Poss</ta>
            <ta e="T2100" id="Seg_3556" s="T2099">pro.h:A</ta>
            <ta e="T2104" id="Seg_3557" s="T2103">pro.h:Poss</ta>
            <ta e="T2105" id="Seg_3558" s="T2104">np:G</ta>
            <ta e="T2106" id="Seg_3559" s="T2105">0.1.h:A</ta>
            <ta e="T2107" id="Seg_3560" s="T2106">adv:Time</ta>
            <ta e="T2109" id="Seg_3561" s="T2108">pro.h:A</ta>
            <ta e="T2112" id="Seg_3562" s="T2111">pro.h:A</ta>
            <ta e="T2113" id="Seg_3563" s="T2112">pro.h:E</ta>
            <ta e="T2114" id="Seg_3564" s="T2113">np:L</ta>
            <ta e="T2118" id="Seg_3565" s="T2117">pro.h:E</ta>
            <ta e="T2123" id="Seg_3566" s="T2122">0.3.h:P</ta>
            <ta e="T2125" id="Seg_3567" s="T2124">np.h:A</ta>
            <ta e="T2127" id="Seg_3568" s="T2126">pro.h:A</ta>
            <ta e="T2130" id="Seg_3569" s="T2129">pro.h:A</ta>
            <ta e="T2131" id="Seg_3570" s="T2130">np.h:P</ta>
            <ta e="T2134" id="Seg_3571" s="T2133">0.1.h:A</ta>
            <ta e="T2135" id="Seg_3572" s="T2134">0.1.h:A</ta>
            <ta e="T2138" id="Seg_3573" s="T2137">pro.h:Poss</ta>
            <ta e="T2139" id="Seg_3574" s="T2138">np:Th</ta>
            <ta e="T2145" id="Seg_3575" s="T2144">0.1.h:A</ta>
            <ta e="T2146" id="Seg_3576" s="T2145">0.3.h:A</ta>
            <ta e="T2147" id="Seg_3577" s="T2146">np:G</ta>
            <ta e="T2148" id="Seg_3578" s="T2147">0.3.h:E</ta>
            <ta e="T2149" id="Seg_3579" s="T2148">np.h:A</ta>
            <ta e="T2157" id="Seg_3580" s="T2156">np:Th</ta>
            <ta e="T2158" id="Seg_3581" s="T2157">np.h:A</ta>
            <ta e="T2160" id="Seg_3582" s="T2159">0.2.h:A</ta>
            <ta e="T2162" id="Seg_3583" s="T2161">pro:Th</ta>
            <ta e="T2164" id="Seg_3584" s="T2163">0.1.h:A</ta>
            <ta e="T2165" id="Seg_3585" s="T2164">np:G</ta>
            <ta e="T2168" id="Seg_3586" s="T2167">np:Th</ta>
            <ta e="T2172" id="Seg_3587" s="T2171">0.3.h:A</ta>
            <ta e="T2173" id="Seg_3588" s="T2172">pro.h:E</ta>
            <ta e="T2175" id="Seg_3589" s="T2174">adv:L</ta>
            <ta e="T2177" id="Seg_3590" s="T2176">pro.h:E</ta>
            <ta e="T2178" id="Seg_3591" s="T2177">adv:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1893" id="Seg_3592" s="T1892">cop</ta>
            <ta e="T1894" id="Seg_3593" s="T1893">np:S</ta>
            <ta e="T1895" id="Seg_3594" s="T1894">pro.h:S</ta>
            <ta e="T1900" id="Seg_3595" s="T1899">np:O</ta>
            <ta e="T1901" id="Seg_3596" s="T1900">ptcl.neg</ta>
            <ta e="T1902" id="Seg_3597" s="T1901">v:pred</ta>
            <ta e="T1906" id="Seg_3598" s="T1905">v:pred 0.3.h:S</ta>
            <ta e="T1915" id="Seg_3599" s="T1914">adj:pred</ta>
            <ta e="T1916" id="Seg_3600" s="T1915">cop 0.3.h:S</ta>
            <ta e="T1917" id="Seg_3601" s="T1916">pro.h:S</ta>
            <ta e="T1919" id="Seg_3602" s="T1918">pro.h:O</ta>
            <ta e="T1920" id="Seg_3603" s="T1919">v:pred</ta>
            <ta e="T1921" id="Seg_3604" s="T1920">v:pred</ta>
            <ta e="T1923" id="Seg_3605" s="T1922">v:pred 0.3.h:S</ta>
            <ta e="T1925" id="Seg_3606" s="T1924">v:pred</ta>
            <ta e="T1926" id="Seg_3607" s="T1925">pro.h:O</ta>
            <ta e="T1927" id="Seg_3608" s="T1926">v:pred</ta>
            <ta e="T1930" id="Seg_3609" s="T1929">conv:pred</ta>
            <ta e="T1931" id="Seg_3610" s="T1930">v:pred 0.3.h:S</ta>
            <ta e="T1933" id="Seg_3611" s="T1932">pro.h:O</ta>
            <ta e="T1934" id="Seg_3612" s="T1933">pro:O</ta>
            <ta e="T1936" id="Seg_3613" s="T1935">v:pred 0.3.h:S</ta>
            <ta e="T1940" id="Seg_3614" s="T1939">conv:pred</ta>
            <ta e="T1941" id="Seg_3615" s="T1940">v:pred 0.3.h:S</ta>
            <ta e="T1943" id="Seg_3616" s="T1942">np.h:S</ta>
            <ta e="T1944" id="Seg_3617" s="T1943">v:pred</ta>
            <ta e="T1945" id="Seg_3618" s="T1944">pro.h:S</ta>
            <ta e="T1947" id="Seg_3619" s="T1946">v:pred</ta>
            <ta e="T1948" id="Seg_3620" s="T1947">pro.h:O</ta>
            <ta e="T1949" id="Seg_3621" s="T1948">ptcl.neg</ta>
            <ta e="T1950" id="Seg_3622" s="T1949">v:pred 0.2.h:S</ta>
            <ta e="T1952" id="Seg_3623" s="T1951">pro.h:S</ta>
            <ta e="T1953" id="Seg_3624" s="T1952">v:pred</ta>
            <ta e="T1955" id="Seg_3625" s="T1954">pro.h:O</ta>
            <ta e="T1956" id="Seg_3626" s="T1955">v:pred 0.1.h:S</ta>
            <ta e="T1959" id="Seg_3627" s="T1958">pro.h:S</ta>
            <ta e="T1960" id="Seg_3628" s="T1959">pro.h:O</ta>
            <ta e="T1961" id="Seg_3629" s="T1960">v:pred</ta>
            <ta e="T1965" id="Seg_3630" s="T1964">np:S</ta>
            <ta e="T1966" id="Seg_3631" s="T1965">v:pred</ta>
            <ta e="T1968" id="Seg_3632" s="T1967">adj:pred</ta>
            <ta e="T1969" id="Seg_3633" s="T1968">cop 0.1.h:S</ta>
            <ta e="T1970" id="Seg_3634" s="T1969">v:pred 0.2.h:S</ta>
            <ta e="T1972" id="Seg_3635" s="T1971">np:O</ta>
            <ta e="T1973" id="Seg_3636" s="T1972">v:pred 0.2.h:S</ta>
            <ta e="T1977" id="Seg_3637" s="T1976">v:pred 0.3.h:S</ta>
            <ta e="T1978" id="Seg_3638" s="T1977">v:pred 0.3.h:S</ta>
            <ta e="T1979" id="Seg_3639" s="T1978">np:O</ta>
            <ta e="T1981" id="Seg_3640" s="T1980">pro.h:S</ta>
            <ta e="T1982" id="Seg_3641" s="T1981">v:pred</ta>
            <ta e="T1985" id="Seg_3642" s="T1983">v:pred 0.3.h:S</ta>
            <ta e="T1986" id="Seg_3643" s="T1985">v:pred 0.3.h:S</ta>
            <ta e="T1988" id="Seg_3644" s="T1987">np:S</ta>
            <ta e="T0" id="Seg_3645" s="T1988">conv:pred</ta>
            <ta e="T1989" id="Seg_3646" s="T0">v:pred</ta>
            <ta e="T1990" id="Seg_3647" s="T1989">pro.h:S</ta>
            <ta e="T1991" id="Seg_3648" s="T1990">v:pred</ta>
            <ta e="T1996" id="Seg_3649" s="T1995">v:pred 0.2.h:S</ta>
            <ta e="T1999" id="Seg_3650" s="T1998">np:O</ta>
            <ta e="T2000" id="Seg_3651" s="T1999">pro.h:S</ta>
            <ta e="T2001" id="Seg_3652" s="T2000">v:pred</ta>
            <ta e="T2002" id="Seg_3653" s="T2001">np:O</ta>
            <ta e="T2004" id="Seg_3654" s="T2003">v:pred 0.3.h:S</ta>
            <ta e="T2005" id="Seg_3655" s="T2004">pro.h:S</ta>
            <ta e="T2006" id="Seg_3656" s="T2005">v:pred</ta>
            <ta e="T2008" id="Seg_3657" s="T2007">np:O</ta>
            <ta e="T2011" id="Seg_3658" s="T2010">v:pred</ta>
            <ta e="T2012" id="Seg_3659" s="T2011">np.h:S</ta>
            <ta e="T2014" id="Seg_3660" s="T2013">adj:pred</ta>
            <ta e="T2015" id="Seg_3661" s="T2014">cop 0.1.h:S</ta>
            <ta e="T2019" id="Seg_3662" s="T2018">np:O</ta>
            <ta e="T2020" id="Seg_3663" s="T2019">v:pred 0.2.h:S</ta>
            <ta e="T2022" id="Seg_3664" s="T2021">pro.h:S</ta>
            <ta e="T2024" id="Seg_3665" s="T2023">v:pred</ta>
            <ta e="T2025" id="Seg_3666" s="T2024">pro.h:S</ta>
            <ta e="T2026" id="Seg_3667" s="T2025">np:O</ta>
            <ta e="T2027" id="Seg_3668" s="T2026">v:pred</ta>
            <ta e="T2029" id="Seg_3669" s="T2028">np:S</ta>
            <ta e="T2030" id="Seg_3670" s="T2029">v:pred</ta>
            <ta e="T2032" id="Seg_3671" s="T2031">v:pred 0.3.h:S</ta>
            <ta e="T2034" id="Seg_3672" s="T2033">ptcl:pred</ta>
            <ta e="T2037" id="Seg_3673" s="T2036">pro.h:O</ta>
            <ta e="T2039" id="Seg_3674" s="T2038">v:pred 0.3:S</ta>
            <ta e="T2040" id="Seg_3675" s="T2039">v:pred 0.3:S</ta>
            <ta e="T2042" id="Seg_3676" s="T2041">np.h:S</ta>
            <ta e="T2043" id="Seg_3677" s="T2042">v:pred</ta>
            <ta e="T2049" id="Seg_3678" s="T2048">v:pred 0.3.h:S</ta>
            <ta e="T2054" id="Seg_3679" s="T2053">np:O</ta>
            <ta e="T2056" id="Seg_3680" s="T2055">v:pred 0.3.h:S</ta>
            <ta e="T2058" id="Seg_3681" s="T2057">v:pred 0.3.h:S</ta>
            <ta e="T2066" id="Seg_3682" s="T2065">ptcl.neg</ta>
            <ta e="T2067" id="Seg_3683" s="T2066">v:pred 0.1.h:S</ta>
            <ta e="T2068" id="Seg_3684" s="T2067">pro:O</ta>
            <ta e="T2070" id="Seg_3685" s="T2069">v:pred 0.1.h:S</ta>
            <ta e="T2073" id="Seg_3686" s="T2072">pro.h:S</ta>
            <ta e="T2074" id="Seg_3687" s="T2073">v:pred</ta>
            <ta e="T2075" id="Seg_3688" s="T2074">np.h:S</ta>
            <ta e="T2076" id="Seg_3689" s="T2075">v:pred</ta>
            <ta e="T2077" id="Seg_3690" s="T2076">pro.h:S</ta>
            <ta e="T2080" id="Seg_3691" s="T2079">pro.h:S</ta>
            <ta e="T2081" id="Seg_3692" s="T2080">pro.h:O</ta>
            <ta e="T2082" id="Seg_3693" s="T2081">v:pred</ta>
            <ta e="T2083" id="Seg_3694" s="T2082">v:pred 0.2.h:S</ta>
            <ta e="T2085" id="Seg_3695" s="T2084">pro.h:O</ta>
            <ta e="T2087" id="Seg_3696" s="T2086">pro.h:S</ta>
            <ta e="T2090" id="Seg_3697" s="T2089">pro.h:S</ta>
            <ta e="T2094" id="Seg_3698" s="T2093">np:O</ta>
            <ta e="T2095" id="Seg_3699" s="T2094">v:pred 0.2:S</ta>
            <ta e="T2096" id="Seg_3700" s="T2095">v:pred 0.2:S</ta>
            <ta e="T2098" id="Seg_3701" s="T2097">np:O</ta>
            <ta e="T2100" id="Seg_3702" s="T2099">pro.h:S</ta>
            <ta e="T2101" id="Seg_3703" s="T2100">v:pred</ta>
            <ta e="T2106" id="Seg_3704" s="T2105">v:pred 0.1.h:S</ta>
            <ta e="T2109" id="Seg_3705" s="T2108">pro.h:S</ta>
            <ta e="T2111" id="Seg_3706" s="T2110">v:pred</ta>
            <ta e="T2112" id="Seg_3707" s="T2111">pro.h:S</ta>
            <ta e="T2113" id="Seg_3708" s="T2112">pro.h:O</ta>
            <ta e="T2116" id="Seg_3709" s="T2115">v:pred</ta>
            <ta e="T2118" id="Seg_3710" s="T2117">pro.h:S</ta>
            <ta e="T2119" id="Seg_3711" s="T2118">v:pred</ta>
            <ta e="T2122" id="Seg_3712" s="T2121">ptcl.neg</ta>
            <ta e="T2123" id="Seg_3713" s="T2122">v:pred 0.3.h:S</ta>
            <ta e="T2125" id="Seg_3714" s="T2124">np.h:S</ta>
            <ta e="T2126" id="Seg_3715" s="T2125">v:pred</ta>
            <ta e="T2127" id="Seg_3716" s="T2126">pro.h:S</ta>
            <ta e="T2128" id="Seg_3717" s="T2127">v:pred</ta>
            <ta e="T2129" id="Seg_3718" s="T2128">v:pred</ta>
            <ta e="T2130" id="Seg_3719" s="T2129">pro.h:S</ta>
            <ta e="T2131" id="Seg_3720" s="T2130">np.h:O</ta>
            <ta e="T2133" id="Seg_3721" s="T2132">ptcl.neg</ta>
            <ta e="T2134" id="Seg_3722" s="T2133">v:pred 0.1.h:S</ta>
            <ta e="T2135" id="Seg_3723" s="T2134">v:pred 0.1.h:S s:cond</ta>
            <ta e="T2139" id="Seg_3724" s="T2138">np:S</ta>
            <ta e="T2142" id="Seg_3725" s="T2141">v:pred</ta>
            <ta e="T2144" id="Seg_3726" s="T2143">ptcl.neg</ta>
            <ta e="T2145" id="Seg_3727" s="T2144">v:pred 0.1.h:S</ta>
            <ta e="T2146" id="Seg_3728" s="T2145">v:pred 0.3.h:S</ta>
            <ta e="T2148" id="Seg_3729" s="T2147">v:pred 0.3.h:S</ta>
            <ta e="T2149" id="Seg_3730" s="T2148">np.h:S</ta>
            <ta e="T2150" id="Seg_3731" s="T2149">v:pred</ta>
            <ta e="T2153" id="Seg_3732" s="T2152">ptcl:pred</ta>
            <ta e="T2157" id="Seg_3733" s="T2156">np:O</ta>
            <ta e="T2158" id="Seg_3734" s="T2157">np.h:S</ta>
            <ta e="T2159" id="Seg_3735" s="T2158">v:pred</ta>
            <ta e="T2160" id="Seg_3736" s="T2159">v:pred 0.2.h:S</ta>
            <ta e="T2162" id="Seg_3737" s="T2161">pro:O</ta>
            <ta e="T2164" id="Seg_3738" s="T2163">v:pred 0.1.h:S</ta>
            <ta e="T2168" id="Seg_3739" s="T2167">np:O</ta>
            <ta e="T2170" id="Seg_3740" s="T2169">v:pred</ta>
            <ta e="T2172" id="Seg_3741" s="T2171">v:pred 0.3.h:S</ta>
            <ta e="T2173" id="Seg_3742" s="T2172">pro.h:S</ta>
            <ta e="T2174" id="Seg_3743" s="T2173">v:pred</ta>
            <ta e="T2177" id="Seg_3744" s="T2176">pro.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1898" id="Seg_3745" s="T1897">TURK:disc</ta>
            <ta e="T1903" id="Seg_3746" s="T1902">RUS:cult</ta>
            <ta e="T1922" id="Seg_3747" s="T1921">RUS:gram</ta>
            <ta e="T1925" id="Seg_3748" s="T1924">RUS:mod</ta>
            <ta e="T1929" id="Seg_3749" s="T1928">TURK:disc</ta>
            <ta e="T1938" id="Seg_3750" s="T1937">RUS:gram</ta>
            <ta e="T1943" id="Seg_3751" s="T1942">RUS:cult</ta>
            <ta e="T1957" id="Seg_3752" s="T1956">RUS:gram</ta>
            <ta e="T1964" id="Seg_3753" s="T1962">RUS:mod</ta>
            <ta e="T1967" id="Seg_3754" s="T1966">RUS:gram</ta>
            <ta e="T1971" id="Seg_3755" s="T1970">RUS:gram</ta>
            <ta e="T1976" id="Seg_3756" s="T1975">RUS:gram</ta>
            <ta e="T1993" id="Seg_3757" s="T1992">RUS:disc</ta>
            <ta e="T1995" id="Seg_3758" s="T1994">RUS:mod</ta>
            <ta e="T2012" id="Seg_3759" s="T2011">RUS:cult</ta>
            <ta e="T2014" id="Seg_3760" s="T2013">TURK:core</ta>
            <ta e="T2016" id="Seg_3761" s="T2015">TURK:disc</ta>
            <ta e="T2017" id="Seg_3762" s="T2016">RUS:mod</ta>
            <ta e="T2023" id="Seg_3763" s="T2022">TURK:core</ta>
            <ta e="T2034" id="Seg_3764" s="T2033">RUS:gram</ta>
            <ta e="T2041" id="Seg_3765" s="T2040">RUS:gram</ta>
            <ta e="T2042" id="Seg_3766" s="T2041">RUS:cult</ta>
            <ta e="T2048" id="Seg_3767" s="T2047">RUS:gram</ta>
            <ta e="T2050" id="Seg_3768" s="T2049">RUS:core</ta>
            <ta e="T2052" id="Seg_3769" s="T2051">RUS:gram</ta>
            <ta e="T2057" id="Seg_3770" s="T2056">RUS:gram</ta>
            <ta e="T2059" id="Seg_3771" s="T2058">RUS:gram</ta>
            <ta e="T2063" id="Seg_3772" s="T2062">TURK:gram(INDEF)</ta>
            <ta e="T2065" id="Seg_3773" s="T2064">TURK:gram(INDEF)</ta>
            <ta e="T2069" id="Seg_3774" s="T2068">TURK:disc</ta>
            <ta e="T2089" id="Seg_3775" s="T2088">RUS:gram</ta>
            <ta e="T2099" id="Seg_3776" s="T2098">RUS:gram</ta>
            <ta e="T2102" id="Seg_3777" s="T2101">RUS:gram</ta>
            <ta e="T2110" id="Seg_3778" s="T2109">RUS:gram</ta>
            <ta e="T2115" id="Seg_3779" s="T2114">RUS:gram</ta>
            <ta e="T2117" id="Seg_3780" s="T2116">RUS:gram</ta>
            <ta e="T2120" id="Seg_3781" s="T2119">RUS:gram</ta>
            <ta e="T2124" id="Seg_3782" s="T2123">RUS:gram</ta>
            <ta e="T2132" id="Seg_3783" s="T2131">RUS:gram</ta>
            <ta e="T2136" id="Seg_3784" s="T2135">RUS:gram</ta>
            <ta e="T2137" id="Seg_3785" s="T2136">RUS:gram</ta>
            <ta e="T2140" id="Seg_3786" s="T2139">RUS:gram</ta>
            <ta e="T2147" id="Seg_3787" s="T2146">TAT:cult</ta>
            <ta e="T2153" id="Seg_3788" s="T2152">RUS:mod</ta>
            <ta e="T2166" id="Seg_3789" s="T2165">RUS:gram</ta>
            <ta e="T2170" id="Seg_3790" s="T2169">RUS:cult</ta>
            <ta e="T2176" id="Seg_3791" s="T2175">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T1912" id="Seg_3792" s="T1908">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1894" id="Seg_3793" s="T1889">У одного человека была собака.</ta>
            <ta e="T1903" id="Seg_3794" s="T1894">Она его овец всегда защищала от волка.</ta>
            <ta e="T1906" id="Seg_3795" s="T1903">Всегда помогала ему.</ta>
            <ta e="T1908" id="Seg_3796" s="T1906">Потом…</ta>
            <ta e="T1912" id="Seg_3797" s="T1908">Не знаю, как…</ta>
            <ta e="T1916" id="Seg_3798" s="T1912">Потом она (заболела?).</ta>
            <ta e="T1920" id="Seg_3799" s="T1916">Он ее взял.</ta>
            <ta e="T1921" id="Seg_3800" s="T1920">Привязал.</ta>
            <ta e="T1924" id="Seg_3801" s="T1921">И отвел в лес.</ta>
            <ta e="T1927" id="Seg_3802" s="T1924">Он хотел ее задушить.</ta>
            <ta e="T1931" id="Seg_3803" s="T1927">Она сидит плачет.</ta>
            <ta e="T1937" id="Seg_3804" s="T1931">Тогда он привязал ее к дереву.</ta>
            <ta e="T1941" id="Seg_3805" s="T1937">А сам ушел.</ta>
            <ta e="T1944" id="Seg_3806" s="T1941">Пришел волк.</ta>
            <ta e="T1947" id="Seg_3807" s="T1944">"Ты тут сидишь.</ta>
            <ta e="T1953" id="Seg_3808" s="T1947">Когда я приходил, ты не давала мне [охотиться на овец].</ta>
            <ta e="T1956" id="Seg_3809" s="T1953">Теперь я тебя съем".</ta>
            <ta e="T1961" id="Seg_3810" s="T1956">"Да зачем тебе меня есть?</ta>
            <ta e="T1966" id="Seg_3811" s="T1961">У меня мяса-то нет.</ta>
            <ta e="T1969" id="Seg_3812" s="T1966">И я стала старой.</ta>
            <ta e="T1975" id="Seg_3813" s="T1969">Иди принеси мне лошадиного мяса".</ta>
            <ta e="T1980" id="Seg_3814" s="T1975">Тот пошел, принес лошадиного мяса.</ta>
            <ta e="T1982" id="Seg_3815" s="T1980">Она поела.</ta>
            <ta e="T1992" id="Seg_3816" s="T1982">(Потом говорит=) Два дня прошло, он к ней пришел.</ta>
            <ta e="T1999" id="Seg_3817" s="T1992">"Ну что, принеси мне еще бараньего мяса".</ta>
            <ta e="T2004" id="Seg_3818" s="T1999">Тот пошел, принес ей овцу.</ta>
            <ta e="T2008" id="Seg_3819" s="T2004">Она съела эту овцу.</ta>
            <ta e="T2012" id="Seg_3820" s="T2008">Потом опять пришел волк.</ta>
            <ta e="T2015" id="Seg_3821" s="T2012">"Ну что, я (/ты) стала хорошей (/вкусной)?"</ta>
            <ta e="T2020" id="Seg_3822" s="T2015">"Нет, принеси мне еще свиного мяса.</ta>
            <ta e="T2024" id="Seg_3823" s="T2020">Тогда я стану вкусной".</ta>
            <ta e="T2027" id="Seg_3824" s="T2024">Он принес свинью.</ta>
            <ta e="T2030" id="Seg_3825" s="T2027">Собака [ее] съела.</ta>
            <ta e="T2032" id="Seg_3826" s="T2030">[Волк] опять пришел.</ta>
            <ta e="T2038" id="Seg_3827" s="T2032">Собака стала его есть [=напала на него].</ta>
            <ta e="T2047" id="Seg_3828" s="T2038">Они боролись-боролись, и волк убежал.</ta>
            <ta e="T2052" id="Seg_3829" s="T2047">Сел за кустом и…</ta>
            <ta e="T2056" id="Seg_3830" s="T2052">И языком кровь слизывает.</ta>
            <ta e="T2061" id="Seg_3831" s="T2056">И ругается на собаку.</ta>
            <ta e="T2067" id="Seg_3832" s="T2061">"Теперь я никого не буду жалеть!</ta>
            <ta e="T2070" id="Seg_3833" s="T2067">Всех съем!"</ta>
            <ta e="T2071" id="Seg_3834" s="T2070">Стоп.</ta>
            <ta e="T2074" id="Seg_3835" s="T2071">Вот он идет.</ta>
            <ta e="T2076" id="Seg_3836" s="T2074">Коза стоит.</ta>
            <ta e="T2082" id="Seg_3837" s="T2076">"Коза-коза, я тебя съем!"</ta>
            <ta e="T2085" id="Seg_3838" s="T2082">"Не ешь меня!</ta>
            <ta e="T2098" id="Seg_3839" s="T2085">[Давай] я буду сверху, а ты рот (закрой=) открой.</ta>
            <ta e="T2101" id="Seg_3840" s="T2098">И я побегу.</ta>
            <ta e="T2106" id="Seg_3841" s="T2101">И прямо тебе в рот прыгну".</ta>
            <ta e="T2116" id="Seg_3842" s="T2106">Тогда козел как разбежался и ударил его по голове.</ta>
            <ta e="T2126" id="Seg_3843" s="T2116">[Волк] упал и чуть не умер, а козел убежал.</ta>
            <ta e="T2128" id="Seg_3844" s="T2126">[Волк] встал.</ta>
            <ta e="T2134" id="Seg_3845" s="T2128">"Съел я козла или не съел?</ta>
            <ta e="T2142" id="Seg_3846" s="T2134">Если бы я его съел, у меня живот был бы большой.</ta>
            <ta e="T2145" id="Seg_3847" s="T2142">Наверное, не съел".</ta>
            <ta e="T2150" id="Seg_3848" s="T2145">Пошел к домам, видит: свинья идет.</ta>
            <ta e="T2152" id="Seg_3849" s="T2150">[С] маленькими поросятами.</ta>
            <ta e="T2157" id="Seg_3850" s="T2152">Хотел схватить одного поросенка.</ta>
            <ta e="T2163" id="Seg_3851" s="T2157">Свинья говорит: "Не хватай их, они маленькие.</ta>
            <ta e="T2170" id="Seg_3852" s="T2163">Пойдем к реке окрестим их!"</ta>
            <ta e="T2178" id="Seg_3853" s="T2170">Пришли [к реке]: "Ты стой здесь, а я [буду стоять] там." </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1894" id="Seg_3854" s="T1889">One man had a dog.</ta>
            <ta e="T1903" id="Seg_3855" s="T1894">It never gave his sheep to the wolf (/protected the sheep).</ta>
            <ta e="T1906" id="Seg_3856" s="T1903">It always helped him.</ta>
            <ta e="T1908" id="Seg_3857" s="T1906">Then…</ta>
            <ta e="T1912" id="Seg_3858" s="T1908">I don't know how…</ta>
            <ta e="T1916" id="Seg_3859" s="T1912">Then it got (ill?).</ta>
            <ta e="T1920" id="Seg_3860" s="T1916">He took it.</ta>
            <ta e="T1921" id="Seg_3861" s="T1920">Tied it up.</ta>
            <ta e="T1924" id="Seg_3862" s="T1921">And took to the forest.</ta>
            <ta e="T1927" id="Seg_3863" s="T1924">He wanted to strangle it.</ta>
            <ta e="T1931" id="Seg_3864" s="T1927">It is sitting there crying.</ta>
            <ta e="T1937" id="Seg_3865" s="T1931">Then it tied it to a tree.</ta>
            <ta e="T1941" id="Seg_3866" s="T1937">And went away himself.</ta>
            <ta e="T1944" id="Seg_3867" s="T1941">Then the wolf comes.</ta>
            <ta e="T1947" id="Seg_3868" s="T1944">"You are sitting there.</ta>
            <ta e="T1953" id="Seg_3869" s="T1947">You did not let me [kill sheep] when I came.</ta>
            <ta e="T1956" id="Seg_3870" s="T1953">Now I will eat you."</ta>
            <ta e="T1961" id="Seg_3871" s="T1956">"But why do you eat me?</ta>
            <ta e="T1966" id="Seg_3872" s="T1961">I only… I have no flesh (/I am skinny).</ta>
            <ta e="T1969" id="Seg_3873" s="T1966">And I became old.</ta>
            <ta e="T1975" id="Seg_3874" s="T1969">Go and bring me horse meat."</ta>
            <ta e="T1980" id="Seg_3875" s="T1975">And it went, brought horse meat.</ta>
            <ta e="T1982" id="Seg_3876" s="T1980">It ate.</ta>
            <ta e="T1992" id="Seg_3877" s="T1982">(Then it says=) Two days went by, it [the wolf] came to it.</ta>
            <ta e="T1999" id="Seg_3878" s="T1992">"Well, what, now bring me sheep meat."</ta>
            <ta e="T2004" id="Seg_3879" s="T1999">It went, brought it a sheep.</ta>
            <ta e="T2008" id="Seg_3880" s="T2004">It ate its sheep.</ta>
            <ta e="T2012" id="Seg_3881" s="T2008">Then again a wolf came.</ta>
            <ta e="T2015" id="Seg_3882" s="T2012">"What, I (/you) became good?"</ta>
            <ta e="T2020" id="Seg_3883" s="T2015">"No, bring me also pig's meat!</ta>
            <ta e="T2024" id="Seg_3884" s="T2020">Then I will become good."</ta>
            <ta e="T2027" id="Seg_3885" s="T2024">He brought its pig.</ta>
            <ta e="T2030" id="Seg_3886" s="T2027">The dog ate [it].</ta>
            <ta e="T2032" id="Seg_3887" s="T2030">Then [the wolf] came [again].</ta>
            <ta e="T2038" id="Seg_3888" s="T2032">Then the dog started to eat it [=attacked the wolf].</ta>
            <ta e="T2047" id="Seg_3889" s="T2038">They fought, they fought, then the wolf ran away from there.</ta>
            <ta e="T2052" id="Seg_3890" s="T2047">And sat on the side of a bush and…</ta>
            <ta e="T2056" id="Seg_3891" s="T2052">It leaks its blood with its tongue.</ta>
            <ta e="T2061" id="Seg_3892" s="T2056">And is swearing at the dog.</ta>
            <ta e="T2067" id="Seg_3893" s="T2061">"Now I will not have mercy for anyone!</ta>
            <ta e="T2070" id="Seg_3894" s="T2067">I will eat everyone!"</ta>
            <ta e="T2071" id="Seg_3895" s="T2070">Enough!</ta>
            <ta e="T2074" id="Seg_3896" s="T2071">Then it is going.</ta>
            <ta e="T2076" id="Seg_3897" s="T2074">A goat is standing.</ta>
            <ta e="T2082" id="Seg_3898" s="T2076">"Goat, goat, I will eat you!"</ta>
            <ta e="T2085" id="Seg_3899" s="T2082">"Don't eat me!</ta>
            <ta e="T2098" id="Seg_3900" s="T2085">I [will be] up [here] (?), but you there, (close your mouth=) open your mouth.</ta>
            <ta e="T2101" id="Seg_3901" s="T2098">But I will run.</ta>
            <ta e="T2106" id="Seg_3902" s="T2101">And I will jump straight into your mouth."</ta>
            <ta e="T2116" id="Seg_3903" s="T2106">Then the goat ran and hit it in its head.</ta>
            <ta e="T2126" id="Seg_3904" s="T2116">But it fell down and nearly died, and the goat ran away.</ta>
            <ta e="T2128" id="Seg_3905" s="T2126">It got up.</ta>
            <ta e="T2134" id="Seg_3906" s="T2128">"Have I eaten the goat or haven’t I eaten [it]?</ta>
            <ta e="T2142" id="Seg_3907" s="T2134">If I had eaten it, my belly would be big.</ta>
            <ta e="T2145" id="Seg_3908" s="T2142">Probably I haven’t eaten [it]."</ta>
            <ta e="T2150" id="Seg_3909" s="T2145">He went to houses, he sees, a pig is walking.</ta>
            <ta e="T2152" id="Seg_3910" s="T2150">[With] piglets.</ta>
            <ta e="T2157" id="Seg_3911" s="T2152">He wanted to grab one piglet.</ta>
            <ta e="T2163" id="Seg_3912" s="T2157">The pig says: "Don't grab [them], they are small.</ta>
            <ta e="T2170" id="Seg_3913" s="T2163">Let’s go to the river and baptize them!"</ta>
            <ta e="T2178" id="Seg_3914" s="T2170">Then they came: "You stand there, but I [will stand] here."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1894" id="Seg_3915" s="T1889">Ein Mann hatte ein Hund.</ta>
            <ta e="T1903" id="Seg_3916" s="T1894">Er gab dem Wolf nie seine Schafe (/beschützte die Schafe).</ta>
            <ta e="T1906" id="Seg_3917" s="T1903">Er half ihm immer.</ta>
            <ta e="T1908" id="Seg_3918" s="T1906">Dann…</ta>
            <ta e="T1912" id="Seg_3919" s="T1908">Ich weiss nicht wie…</ta>
            <ta e="T1916" id="Seg_3920" s="T1912">Dann wurde er (krank?). </ta>
            <ta e="T1920" id="Seg_3921" s="T1916">Er nahm ihn.</ta>
            <ta e="T1921" id="Seg_3922" s="T1920">Fesselte ihn.</ta>
            <ta e="T1924" id="Seg_3923" s="T1921">Und begab sich zur Taiga.</ta>
            <ta e="T1927" id="Seg_3924" s="T1924">Er wollte ihn erdrosseln.</ta>
            <ta e="T1931" id="Seg_3925" s="T1927">Er sitzt dort und weint.</ta>
            <ta e="T1937" id="Seg_3926" s="T1931">Dann band er ihn an einem Baum fest.</ta>
            <ta e="T1941" id="Seg_3927" s="T1937">Und ging selber weg.</ta>
            <ta e="T1944" id="Seg_3928" s="T1941">Dann kommt der Wolf.</ta>
            <ta e="T1947" id="Seg_3929" s="T1944">„Du sitzt da.</ta>
            <ta e="T1953" id="Seg_3930" s="T1947">Du hast mich nicht [Schafe töten] lassen, als ich kam.</ta>
            <ta e="T1956" id="Seg_3931" s="T1953">Nun werde ich dich fressen.“</ta>
            <ta e="T1961" id="Seg_3932" s="T1956">„Aber warum frisst du mich?</ta>
            <ta e="T1966" id="Seg_3933" s="T1961">Ich bloß… Ich habe kein Fleisch (/ich bin dürre). </ta>
            <ta e="T1969" id="Seg_3934" s="T1966">Und ich bin alt geworden.</ta>
            <ta e="T1975" id="Seg_3935" s="T1969">Geh und bring mir Pferdefleisch.“</ta>
            <ta e="T1980" id="Seg_3936" s="T1975">Und er ging, brachte Pferdefleisch.</ta>
            <ta e="T1982" id="Seg_3937" s="T1980">Er aß.</ta>
            <ta e="T1992" id="Seg_3938" s="T1982">(Dann sagt er=) Zwei Tage sind vergangen, er [der Wolf] kam zu ihm.</ta>
            <ta e="T1999" id="Seg_3939" s="T1992">„Also, was, bring mir mehr, Schaffleisch.“</ta>
            <ta e="T2004" id="Seg_3940" s="T1999">Er ging, brachte ihm ein Schaf.</ta>
            <ta e="T2008" id="Seg_3941" s="T2004">Er aß sein Schaf.</ta>
            <ta e="T2012" id="Seg_3942" s="T2008">Dann kam wieder ein Wolf.</ta>
            <ta e="T2015" id="Seg_3943" s="T2012">„Was, ich (/du) wurdest gut?“</ta>
            <ta e="T2020" id="Seg_3944" s="T2015">„Nein, bring mir also Schweinefleisch!</ta>
            <ta e="T2024" id="Seg_3945" s="T2020">Dann werde ich gut werden.“</ta>
            <ta e="T2027" id="Seg_3946" s="T2024">Er brachte sein Schwein.</ta>
            <ta e="T2030" id="Seg_3947" s="T2027">Der Hund fraß [es].</ta>
            <ta e="T2032" id="Seg_3948" s="T2030">Dann kam [wieder der Wolf].</ta>
            <ta e="T2038" id="Seg_3949" s="T2032">Dann fing der Hund an, ihn zu fressen [=griff den Wolf an].</ta>
            <ta e="T2047" id="Seg_3950" s="T2038">Sie kämpften, sie kämpften, dann lief der Wolf von dort weg.</ta>
            <ta e="T2052" id="Seg_3951" s="T2047">Und saß an der Seite eines Busches und…</ta>
            <ta e="T2056" id="Seg_3952" s="T2052">Er leckt sein Blut mit der Zunge.</ta>
            <ta e="T2061" id="Seg_3953" s="T2056">Und flucht den Hund an.</ta>
            <ta e="T2067" id="Seg_3954" s="T2061">Jetzt werde ich keine Gnade für niemanden haben!</ta>
            <ta e="T2070" id="Seg_3955" s="T2067">Ich wird jeden fressen!</ta>
            <ta e="T2071" id="Seg_3956" s="T2070">Genug!</ta>
            <ta e="T2074" id="Seg_3957" s="T2071">Dann geht er.</ta>
            <ta e="T2076" id="Seg_3958" s="T2074">Eine Ziege steht.</ta>
            <ta e="T2082" id="Seg_3959" s="T2076">„Ziege, Ziege, ich werde dich fressen!“</ta>
            <ta e="T2085" id="Seg_3960" s="T2082">Friss mich nicht!</ta>
            <ta e="T2098" id="Seg_3961" s="T2085">Ich [werde hier] oben [stehen] (?), aber du dort, (mach das Maul zu=) öffne das Maul.</ta>
            <ta e="T2101" id="Seg_3962" s="T2098">Aber ich werde rennen.</ta>
            <ta e="T2106" id="Seg_3963" s="T2101">Und ich werde dir direkt ins Maul springen.“</ta>
            <ta e="T2116" id="Seg_3964" s="T2106">Dann, als die Ziege rannte, stieß sie ihn mit dem Kopf.</ta>
            <ta e="T2126" id="Seg_3965" s="T2116">Aber er fiel hin und wurde fast gestorben, und die Ziege lief weg.</ta>
            <ta e="T2128" id="Seg_3966" s="T2126">Er stand auf.</ta>
            <ta e="T2134" id="Seg_3967" s="T2128">„Habe ich die Ziege gegessen oder habe ich [sie] nicht gegessen?</ta>
            <ta e="T2142" id="Seg_3968" s="T2134">Wenn ich sie gegessen hätte, wäre mein Bauch groß.</ta>
            <ta e="T2145" id="Seg_3969" s="T2142">Wahrscheinlich habe ich nicht gegessen.“</ta>
            <ta e="T2150" id="Seg_3970" s="T2145">Er ging zu Häusern, er schaut, ein Schwein läuft.</ta>
            <ta e="T2152" id="Seg_3971" s="T2150">[Mit] kleinen Ferkeln.</ta>
            <ta e="T2157" id="Seg_3972" s="T2152">Er wollte sich ein kleines Schwein schnappen.</ta>
            <ta e="T2163" id="Seg_3973" s="T2157">Das Schwein sagt: „Schnapp [sie] nicht, sie sind klein.</ta>
            <ta e="T2170" id="Seg_3974" s="T2163">Gehen wir zum Fluss und taufen sie!“</ta>
            <ta e="T2178" id="Seg_3975" s="T2170">Dann kamen sie: „Du stehst da, aber ich [werde] hier [stehen].“</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1894" id="Seg_3976" s="T1889">[GVY:] see: http://raskonyaga.ru/skazki/russkie-narodnye/706-volk-duren.html</ta>
            <ta e="T1916" id="Seg_3977" s="T1912">[KlT:] Participle form of tʼit-. [GVY:] cf. tšɯt 'krank D79b</ta>
            <ta e="T1921" id="Seg_3978" s="T1920">[KlT:] Intr. instead of tr.</ta>
            <ta e="T1944" id="Seg_3979" s="T1941">[KlT:] Волк-то?</ta>
            <ta e="T2015" id="Seg_3980" s="T2012">[KlT:] 1SG instead of 2SG.</ta>
            <ta e="T2038" id="Seg_3981" s="T2032">[AAV] amzittə instead of amnosʼtə?</ta>
            <ta e="T2052" id="Seg_3982" s="T2047">[GVY:] da most probably means "and" and refers to the next sentence.</ta>
            <ta e="T2061" id="Seg_3983" s="T2056">[GVY:] to = dö 'that'?</ta>
            <ta e="T2071" id="Seg_3984" s="T2070">[GVY:] a request to stop the recording.</ta>
            <ta e="T2098" id="Seg_3985" s="T2085">Contaminated form nʼuʔdə + nʼuʔnən in LOC meaning?; POSS.3SG instead of POSS.2SG.</ta>
            <ta e="T2101" id="Seg_3986" s="T2098">[KlT:] PST instead of FUT?, unclear consonant.</ta>
            <ta e="T2106" id="Seg_3987" s="T2101">[KlT:] POSS.3SG instead of POSS.2SG.</ta>
            <ta e="T2126" id="Seg_3988" s="T2116">[KlT:] Khakas ’ap’ intensifier proclitic particle?</ta>
            <ta e="T2178" id="Seg_3989" s="T2170">[KlT:] The tale continues on tape 192!</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1888" />
            <conversion-tli id="T1889" />
            <conversion-tli id="T1890" />
            <conversion-tli id="T1891" />
            <conversion-tli id="T1892" />
            <conversion-tli id="T1893" />
            <conversion-tli id="T1894" />
            <conversion-tli id="T1895" />
            <conversion-tli id="T1896" />
            <conversion-tli id="T1897" />
            <conversion-tli id="T1898" />
            <conversion-tli id="T1899" />
            <conversion-tli id="T1900" />
            <conversion-tli id="T1901" />
            <conversion-tli id="T1902" />
            <conversion-tli id="T1903" />
            <conversion-tli id="T1904" />
            <conversion-tli id="T1905" />
            <conversion-tli id="T1906" />
            <conversion-tli id="T1907" />
            <conversion-tli id="T1908" />
            <conversion-tli id="T1909" />
            <conversion-tli id="T1910" />
            <conversion-tli id="T1911" />
            <conversion-tli id="T1912" />
            <conversion-tli id="T1913" />
            <conversion-tli id="T1914" />
            <conversion-tli id="T1915" />
            <conversion-tli id="T1916" />
            <conversion-tli id="T1917" />
            <conversion-tli id="T1918" />
            <conversion-tli id="T1919" />
            <conversion-tli id="T1920" />
            <conversion-tli id="T1921" />
            <conversion-tli id="T1922" />
            <conversion-tli id="T1923" />
            <conversion-tli id="T1924" />
            <conversion-tli id="T1925" />
            <conversion-tli id="T1926" />
            <conversion-tli id="T1927" />
            <conversion-tli id="T1928" />
            <conversion-tli id="T1929" />
            <conversion-tli id="T1930" />
            <conversion-tli id="T1931" />
            <conversion-tli id="T1932" />
            <conversion-tli id="T1933" />
            <conversion-tli id="T1934" />
            <conversion-tli id="T1935" />
            <conversion-tli id="T1936" />
            <conversion-tli id="T1937" />
            <conversion-tli id="T1938" />
            <conversion-tli id="T1939" />
            <conversion-tli id="T1940" />
            <conversion-tli id="T1941" />
            <conversion-tli id="T1942" />
            <conversion-tli id="T1943" />
            <conversion-tli id="T1944" />
            <conversion-tli id="T1945" />
            <conversion-tli id="T1946" />
            <conversion-tli id="T1947" />
            <conversion-tli id="T1948" />
            <conversion-tli id="T1949" />
            <conversion-tli id="T1950" />
            <conversion-tli id="T1951" />
            <conversion-tli id="T1952" />
            <conversion-tli id="T1953" />
            <conversion-tli id="T1954" />
            <conversion-tli id="T1955" />
            <conversion-tli id="T1956" />
            <conversion-tli id="T1957" />
            <conversion-tli id="T1958" />
            <conversion-tli id="T1959" />
            <conversion-tli id="T1960" />
            <conversion-tli id="T1961" />
            <conversion-tli id="T1962" />
            <conversion-tli id="T1963" />
            <conversion-tli id="T1964" />
            <conversion-tli id="T1965" />
            <conversion-tli id="T1966" />
            <conversion-tli id="T1967" />
            <conversion-tli id="T1968" />
            <conversion-tli id="T1969" />
            <conversion-tli id="T1970" />
            <conversion-tli id="T1971" />
            <conversion-tli id="T1972" />
            <conversion-tli id="T1973" />
            <conversion-tli id="T1974" />
            <conversion-tli id="T1975" />
            <conversion-tli id="T1976" />
            <conversion-tli id="T1977" />
            <conversion-tli id="T1978" />
            <conversion-tli id="T1979" />
            <conversion-tli id="T1980" />
            <conversion-tli id="T1981" />
            <conversion-tli id="T1982" />
            <conversion-tli id="T1983" />
            <conversion-tli id="T1984" />
            <conversion-tli id="T1985" />
            <conversion-tli id="T1986" />
            <conversion-tli id="T1987" />
            <conversion-tli id="T1988" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1989" />
            <conversion-tli id="T1990" />
            <conversion-tli id="T1991" />
            <conversion-tli id="T1992" />
            <conversion-tli id="T1993" />
            <conversion-tli id="T1994" />
            <conversion-tli id="T1995" />
            <conversion-tli id="T1996" />
            <conversion-tli id="T1997" />
            <conversion-tli id="T1998" />
            <conversion-tli id="T1999" />
            <conversion-tli id="T2000" />
            <conversion-tli id="T2001" />
            <conversion-tli id="T2002" />
            <conversion-tli id="T2003" />
            <conversion-tli id="T2004" />
            <conversion-tli id="T2005" />
            <conversion-tli id="T2006" />
            <conversion-tli id="T2007" />
            <conversion-tli id="T2008" />
            <conversion-tli id="T2009" />
            <conversion-tli id="T2010" />
            <conversion-tli id="T2011" />
            <conversion-tli id="T2012" />
            <conversion-tli id="T2013" />
            <conversion-tli id="T2014" />
            <conversion-tli id="T2015" />
            <conversion-tli id="T2016" />
            <conversion-tli id="T2017" />
            <conversion-tli id="T2018" />
            <conversion-tli id="T2019" />
            <conversion-tli id="T2020" />
            <conversion-tli id="T2021" />
            <conversion-tli id="T2022" />
            <conversion-tli id="T2023" />
            <conversion-tli id="T2024" />
            <conversion-tli id="T2025" />
            <conversion-tli id="T2026" />
            <conversion-tli id="T2027" />
            <conversion-tli id="T2028" />
            <conversion-tli id="T2029" />
            <conversion-tli id="T2030" />
            <conversion-tli id="T2031" />
            <conversion-tli id="T2032" />
            <conversion-tli id="T2033" />
            <conversion-tli id="T2034" />
            <conversion-tli id="T2035" />
            <conversion-tli id="T2036" />
            <conversion-tli id="T2037" />
            <conversion-tli id="T2038" />
            <conversion-tli id="T2039" />
            <conversion-tli id="T2040" />
            <conversion-tli id="T2041" />
            <conversion-tli id="T2042" />
            <conversion-tli id="T2043" />
            <conversion-tli id="T2044" />
            <conversion-tli id="T2045" />
            <conversion-tli id="T2046" />
            <conversion-tli id="T2047" />
            <conversion-tli id="T2048" />
            <conversion-tli id="T2049" />
            <conversion-tli id="T2050" />
            <conversion-tli id="T2051" />
            <conversion-tli id="T2052" />
            <conversion-tli id="T2053" />
            <conversion-tli id="T2054" />
            <conversion-tli id="T2055" />
            <conversion-tli id="T2056" />
            <conversion-tli id="T2057" />
            <conversion-tli id="T2058" />
            <conversion-tli id="T2059" />
            <conversion-tli id="T2060" />
            <conversion-tli id="T2061" />
            <conversion-tli id="T2062" />
            <conversion-tli id="T2063" />
            <conversion-tli id="T2064" />
            <conversion-tli id="T2065" />
            <conversion-tli id="T2066" />
            <conversion-tli id="T2067" />
            <conversion-tli id="T2068" />
            <conversion-tli id="T2069" />
            <conversion-tli id="T2070" />
            <conversion-tli id="T2071" />
            <conversion-tli id="T2072" />
            <conversion-tli id="T2073" />
            <conversion-tli id="T2074" />
            <conversion-tli id="T2075" />
            <conversion-tli id="T2076" />
            <conversion-tli id="T2077" />
            <conversion-tli id="T2078" />
            <conversion-tli id="T2079" />
            <conversion-tli id="T2080" />
            <conversion-tli id="T2081" />
            <conversion-tli id="T2082" />
            <conversion-tli id="T2083" />
            <conversion-tli id="T2084" />
            <conversion-tli id="T2085" />
            <conversion-tli id="T2086" />
            <conversion-tli id="T2087" />
            <conversion-tli id="T2088" />
            <conversion-tli id="T2089" />
            <conversion-tli id="T2090" />
            <conversion-tli id="T2091" />
            <conversion-tli id="T2092" />
            <conversion-tli id="T2093" />
            <conversion-tli id="T2094" />
            <conversion-tli id="T2095" />
            <conversion-tli id="T2096" />
            <conversion-tli id="T2097" />
            <conversion-tli id="T2098" />
            <conversion-tli id="T2099" />
            <conversion-tli id="T2100" />
            <conversion-tli id="T2101" />
            <conversion-tli id="T2102" />
            <conversion-tli id="T2103" />
            <conversion-tli id="T2104" />
            <conversion-tli id="T2105" />
            <conversion-tli id="T2106" />
            <conversion-tli id="T2107" />
            <conversion-tli id="T2108" />
            <conversion-tli id="T2109" />
            <conversion-tli id="T2110" />
            <conversion-tli id="T2111" />
            <conversion-tli id="T2112" />
            <conversion-tli id="T2113" />
            <conversion-tli id="T2114" />
            <conversion-tli id="T2115" />
            <conversion-tli id="T2116" />
            <conversion-tli id="T2117" />
            <conversion-tli id="T2118" />
            <conversion-tli id="T2119" />
            <conversion-tli id="T2120" />
            <conversion-tli id="T2121" />
            <conversion-tli id="T2122" />
            <conversion-tli id="T2123" />
            <conversion-tli id="T2124" />
            <conversion-tli id="T2125" />
            <conversion-tli id="T2126" />
            <conversion-tli id="T2127" />
            <conversion-tli id="T2128" />
            <conversion-tli id="T2129" />
            <conversion-tli id="T2130" />
            <conversion-tli id="T2131" />
            <conversion-tli id="T2132" />
            <conversion-tli id="T2133" />
            <conversion-tli id="T2134" />
            <conversion-tli id="T2135" />
            <conversion-tli id="T2136" />
            <conversion-tli id="T2137" />
            <conversion-tli id="T2138" />
            <conversion-tli id="T2139" />
            <conversion-tli id="T2140" />
            <conversion-tli id="T2141" />
            <conversion-tli id="T2142" />
            <conversion-tli id="T2143" />
            <conversion-tli id="T2144" />
            <conversion-tli id="T2145" />
            <conversion-tli id="T2146" />
            <conversion-tli id="T2147" />
            <conversion-tli id="T2148" />
            <conversion-tli id="T2149" />
            <conversion-tli id="T2150" />
            <conversion-tli id="T2151" />
            <conversion-tli id="T2152" />
            <conversion-tli id="T2153" />
            <conversion-tli id="T2154" />
            <conversion-tli id="T2155" />
            <conversion-tli id="T2156" />
            <conversion-tli id="T2157" />
            <conversion-tli id="T2158" />
            <conversion-tli id="T2159" />
            <conversion-tli id="T2160" />
            <conversion-tli id="T2161" />
            <conversion-tli id="T2162" />
            <conversion-tli id="T2163" />
            <conversion-tli id="T2164" />
            <conversion-tli id="T2165" />
            <conversion-tli id="T2166" />
            <conversion-tli id="T2167" />
            <conversion-tli id="T2168" />
            <conversion-tli id="T2169" />
            <conversion-tli id="T2170" />
            <conversion-tli id="T2171" />
            <conversion-tli id="T2172" />
            <conversion-tli id="T2173" />
            <conversion-tli id="T2174" />
            <conversion-tli id="T2175" />
            <conversion-tli id="T2176" />
            <conversion-tli id="T2177" />
            <conversion-tli id="T2178" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
