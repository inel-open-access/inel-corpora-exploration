<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID99697113-515E-08EE-E768-D26CFA8B8B87">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Magpie_flk.wav" />
         <referenced-file url="PKZ_196X_Magpie_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Magpie_flk\PKZ_196X_Magpie_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">118</ud-information>
            <ud-information attribute-name="# HIAT:w">70</ud-information>
            <ud-information attribute-name="# e">70</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1305" time="0.295" type="appl" />
         <tli id="T1306" time="0.991" type="appl" />
         <tli id="T1307" time="1.688" type="appl" />
         <tli id="T1308" time="2.384" type="appl" />
         <tli id="T1309" time="3.08" type="appl" />
         <tli id="T1310" time="3.777" type="appl" />
         <tli id="T1311" time="5.073277485062514" />
         <tli id="T1312" time="5.908" type="appl" />
         <tli id="T1313" time="6.964" type="appl" />
         <tli id="T1314" time="8.104" type="appl" />
         <tli id="T1315" time="9.245" type="appl" />
         <tli id="T1316" time="10.239" type="appl" />
         <tli id="T1317" time="11.234" type="appl" />
         <tli id="T1318" time="12.228" type="appl" />
         <tli id="T1319" time="13.453185236341856" />
         <tli id="T1320" time="14.392" type="appl" />
         <tli id="T1321" time="15.2" type="appl" />
         <tli id="T1322" time="16.008" type="appl" />
         <tli id="T1323" time="18.15980009107791" />
         <tli id="T1324" time="18.864" type="appl" />
         <tli id="T1325" time="19.459" type="appl" />
         <tli id="T1326" time="20.098" type="appl" />
         <tli id="T1327" time="20.736" type="appl" />
         <tli id="T1328" time="21.375" type="appl" />
         <tli id="T1329" time="22.013" type="appl" />
         <tli id="T1330" time="22.652" type="appl" />
         <tli id="T1331" time="23.29" type="appl" />
         <tli id="T1332" time="24.253066347775857" />
         <tli id="T1333" time="24.858" type="appl" />
         <tli id="T1334" time="25.402" type="appl" />
         <tli id="T1335" time="25.947" type="appl" />
         <tli id="T1336" time="26.491" type="appl" />
         <tli id="T1337" time="27.035" type="appl" />
         <tli id="T1338" time="27.58" type="appl" />
         <tli id="T1339" time="28.124" type="appl" />
         <tli id="T1340" time="29.013013948741207" />
         <tli id="T1341" time="29.53" type="appl" />
         <tli id="T1342" time="30.245" type="appl" />
         <tli id="T1343" time="30.96" type="appl" />
         <tli id="T1344" time="31.879649058566287" />
         <tli id="T1345" time="32.803" type="appl" />
         <tli id="T1346" time="33.69" type="appl" />
         <tli id="T1347" time="34.577" type="appl" />
         <tli id="T1348" time="35.464" type="appl" />
         <tli id="T1349" time="36.352" type="appl" />
         <tli id="T1350" time="37.239" type="appl" />
         <tli id="T1351" time="38.126" type="appl" />
         <tli id="T1352" time="39.33290034411148" />
         <tli id="T1353" time="39.894" type="appl" />
         <tli id="T1354" time="40.626" type="appl" />
         <tli id="T1355" time="41.359" type="appl" />
         <tli id="T1356" time="42.091" type="appl" />
         <tli id="T1357" time="42.823" type="appl" />
         <tli id="T1358" time="43.555" type="appl" />
         <tli id="T1359" time="44.288" type="appl" />
         <tli id="T1360" time="45.02" type="appl" />
         <tli id="T1361" time="45.952827469823795" />
         <tli id="T1362" time="46.735" type="appl" />
         <tli id="T1363" time="47.378" type="appl" />
         <tli id="T1364" time="48.02" type="appl" />
         <tli id="T1365" time="48.662" type="appl" />
         <tli id="T1366" time="49.806" type="appl" />
         <tli id="T1367" time="50.941" type="appl" />
         <tli id="T1368" time="52.077" type="appl" />
         <tli id="T1369" time="53.213" type="appl" />
         <tli id="T1370" time="54.349" type="appl" />
         <tli id="T1371" time="55.484" type="appl" />
         <tli id="T1372" time="56.62" type="appl" />
         <tli id="T1373" time="57.342" type="appl" />
         <tli id="T1374" time="58.064" type="appl" />
         <tli id="T1375" time="58.786" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1375" id="Seg_0" n="sc" s="T1305">
               <ts e="T1311" id="Seg_2" n="HIAT:u" s="T1305">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T1306" id="Seg_5" n="HIAT:w" s="T1305">Saroka</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_9" n="HIAT:w" s="T1306">bostə</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1308" id="Seg_12" n="HIAT:w" s="T1307">esseŋdə</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1309" id="Seg_15" n="HIAT:w" s="T1308">măndə:</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_17" n="HIAT:ip">"</nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T1310" id="Seg_20" n="HIAT:w" s="T1309">Kam-</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1311" id="Seg_24" n="HIAT:w" s="T1310">Kangaʔ</ts>
                  <nts id="Seg_25" n="HIAT:ip">!</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1313" id="Seg_28" n="HIAT:u" s="T1311">
                  <ts e="T1312" id="Seg_30" n="HIAT:w" s="T1311">Amorgaʔ</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_32" n="HIAT:ip">(</nts>
                  <ts e="T1313" id="Seg_34" n="HIAT:w" s="T1312">boskə</ts>
                  <nts id="Seg_35" n="HIAT:ip">)</nts>
                  <nts id="Seg_36" n="HIAT:ip">!</nts>
                  <nts id="Seg_37" n="HIAT:ip">"</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1315" id="Seg_40" n="HIAT:u" s="T1313">
                  <ts e="T1314" id="Seg_42" n="HIAT:w" s="T1313">Dĭzeŋ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1315" id="Seg_45" n="HIAT:w" s="T1314">kambiʔi</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1319" id="Seg_49" n="HIAT:u" s="T1315">
                  <ts e="T1316" id="Seg_51" n="HIAT:w" s="T1315">Dĭgəttə:</ts>
                  <nts id="Seg_52" n="HIAT:ip">"</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1317" id="Seg_55" n="HIAT:w" s="T1316">Miʔ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_57" n="HIAT:ip">(</nts>
                  <ts e="T1318" id="Seg_59" n="HIAT:w" s="T1317">pimniem-</ts>
                  <nts id="Seg_60" n="HIAT:ip">)</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_63" n="HIAT:w" s="T1318">pimniebeʔ</ts>
                  <nts id="Seg_64" n="HIAT:ip">!</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1323" id="Seg_67" n="HIAT:u" s="T1319">
                  <ts e="T1320" id="Seg_69" n="HIAT:w" s="T1319">Girgit-nʼibudʼ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_72" n="HIAT:w" s="T1320">kuza</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_75" n="HIAT:w" s="T1321">kutləj</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_78" n="HIAT:w" s="T1322">miʔnʼibeʔ</ts>
                  <nts id="Seg_79" n="HIAT:ip">!</nts>
                  <nts id="Seg_80" n="HIAT:ip">"</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1325" id="Seg_83" n="HIAT:u" s="T1323">
                  <nts id="Seg_84" n="HIAT:ip">"</nts>
                  <ts e="T1324" id="Seg_86" n="HIAT:w" s="T1323">Iʔ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_89" n="HIAT:w" s="T1324">pimgeʔ</ts>
                  <nts id="Seg_90" n="HIAT:ip">!</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1332" id="Seg_93" n="HIAT:u" s="T1325">
                  <ts e="T1326" id="Seg_95" n="HIAT:w" s="T1325">Kamən</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1327" id="Seg_98" n="HIAT:w" s="T1326">kutləj</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1328" id="Seg_102" n="HIAT:w" s="T1327">a</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_105" n="HIAT:w" s="T1328">šiʔ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1330" id="Seg_108" n="HIAT:w" s="T1329">nʼergöleʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1331" id="Seg_111" n="HIAT:w" s="T1330">kalla</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1332" id="Seg_114" n="HIAT:w" s="T1331">dʼürbilaʔ</ts>
                  <nts id="Seg_115" n="HIAT:ip">!</nts>
                  <nts id="Seg_116" n="HIAT:ip">"</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1340" id="Seg_119" n="HIAT:u" s="T1332">
                  <ts e="T1333" id="Seg_121" n="HIAT:w" s="T1332">Dĭgəttə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1334" id="Seg_124" n="HIAT:w" s="T1333">măndə:</ts>
                  <nts id="Seg_125" n="HIAT:ip">"</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1335" id="Seg_128" n="HIAT:w" s="T1334">A</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_130" n="HIAT:ip">(</nts>
                  <ts e="T1336" id="Seg_132" n="HIAT:w" s="T1335">šində=</ts>
                  <nts id="Seg_133" n="HIAT:ip">)</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_136" n="HIAT:w" s="T1336">girgit</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1338" id="Seg_139" n="HIAT:w" s="T1337">kuza</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1339" id="Seg_142" n="HIAT:w" s="T1338">pi</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1340" id="Seg_145" n="HIAT:w" s="T1339">iləj</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1344" id="Seg_149" n="HIAT:u" s="T1340">
                  <ts e="T1341" id="Seg_151" n="HIAT:w" s="T1340">I</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1342" id="Seg_154" n="HIAT:w" s="T1341">piziʔ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1343" id="Seg_157" n="HIAT:w" s="T1342">kutləj</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_160" n="HIAT:w" s="T1343">miʔnʼibeʔ</ts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1352" id="Seg_165" n="HIAT:u" s="T1344">
                  <nts id="Seg_166" n="HIAT:ip">"</nts>
                  <ts e="T1345" id="Seg_168" n="HIAT:w" s="T1344">A</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1346" id="Seg_171" n="HIAT:w" s="T1345">kamən</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1347" id="Seg_174" n="HIAT:w" s="T1346">dĭ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1348" id="Seg_177" n="HIAT:w" s="T1347">dʼünə</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_180" n="HIAT:w" s="T1348">iləj</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1350" id="Seg_184" n="HIAT:w" s="T1349">a</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_187" n="HIAT:w" s="T1350">šiʔ</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1352" id="Seg_190" n="HIAT:w" s="T1351">nʼergöluʔpibeʔ</ts>
                  <nts id="Seg_191" n="HIAT:ip">"</nts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1361" id="Seg_195" n="HIAT:u" s="T1352">
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip">(</nts>
                  <ts e="T1353" id="Seg_199" n="HIAT:w" s="T1352">A=</ts>
                  <nts id="Seg_200" n="HIAT:ip">)</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_203" n="HIAT:w" s="T1353">A</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_206" n="HIAT:w" s="T1354">dĭn</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <ts e="T1356" id="Seg_210" n="HIAT:w" s="T1355">iʔ-</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1357" id="Seg_213" n="HIAT:w" s="T1356">mə-</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358" id="Seg_217" n="HIAT:w" s="T1357">udandə</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_220" n="HIAT:w" s="T1358">možet</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_223" n="HIAT:w" s="T1359">ige</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_226" n="HIAT:w" s="T1360">pi</ts>
                  <nts id="Seg_227" n="HIAT:ip">"</nts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1365" id="Seg_231" n="HIAT:u" s="T1361">
                  <nts id="Seg_232" n="HIAT:ip">"</nts>
                  <ts e="T1362" id="Seg_234" n="HIAT:w" s="T1361">A</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1363" id="Seg_237" n="HIAT:w" s="T1362">šiʔ</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_240" n="HIAT:w" s="T1363">tože</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_243" n="HIAT:w" s="T1364">nʼergöluʔpileʔ</ts>
                  <nts id="Seg_244" n="HIAT:ip">"</nts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1372" id="Seg_248" n="HIAT:u" s="T1365">
                  <ts e="T1366" id="Seg_250" n="HIAT:w" s="T1365">Dĭgəttə</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1367" id="Seg_253" n="HIAT:w" s="T1366">ijat</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_256" n="HIAT:w" s="T1367">măndə:</ts>
                  <nts id="Seg_257" n="HIAT:ip">"</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1369" id="Seg_260" n="HIAT:w" s="T1368">No</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1370" id="Seg_264" n="HIAT:w" s="T1369">kangaʔ</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1371" id="Seg_268" n="HIAT:w" s="T1370">boslaʔ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_271" n="HIAT:w" s="T1371">amorgaʔ</ts>
                  <nts id="Seg_272" n="HIAT:ip">"</nts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1375" id="Seg_276" n="HIAT:u" s="T1372">
                  <ts e="T1373" id="Seg_278" n="HIAT:w" s="T1372">Bostə</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1374" id="Seg_281" n="HIAT:w" s="T1373">nʼergölüʔpi</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1375" id="Seg_284" n="HIAT:w" s="T1374">bar</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1375" id="Seg_287" n="sc" s="T1305">
               <ts e="T1306" id="Seg_289" n="e" s="T1305">(Saroka) </ts>
               <ts e="T1307" id="Seg_291" n="e" s="T1306">bostə </ts>
               <ts e="T1308" id="Seg_293" n="e" s="T1307">esseŋdə </ts>
               <ts e="T1309" id="Seg_295" n="e" s="T1308">măndə: </ts>
               <ts e="T1310" id="Seg_297" n="e" s="T1309">"(Kam-) </ts>
               <ts e="T1311" id="Seg_299" n="e" s="T1310">Kangaʔ! </ts>
               <ts e="T1312" id="Seg_301" n="e" s="T1311">Amorgaʔ </ts>
               <ts e="T1313" id="Seg_303" n="e" s="T1312">(boskə)!" </ts>
               <ts e="T1314" id="Seg_305" n="e" s="T1313">Dĭzeŋ </ts>
               <ts e="T1315" id="Seg_307" n="e" s="T1314">kambiʔi. </ts>
               <ts e="T1316" id="Seg_309" n="e" s="T1315">Dĭgəttə:" </ts>
               <ts e="T1317" id="Seg_311" n="e" s="T1316">Miʔ </ts>
               <ts e="T1318" id="Seg_313" n="e" s="T1317">(pimniem-) </ts>
               <ts e="T1319" id="Seg_315" n="e" s="T1318">pimniebeʔ! </ts>
               <ts e="T1320" id="Seg_317" n="e" s="T1319">Girgit-nʼibudʼ </ts>
               <ts e="T1321" id="Seg_319" n="e" s="T1320">kuza </ts>
               <ts e="T1322" id="Seg_321" n="e" s="T1321">kutləj </ts>
               <ts e="T1323" id="Seg_323" n="e" s="T1322">miʔnʼibeʔ!" </ts>
               <ts e="T1324" id="Seg_325" n="e" s="T1323">"Iʔ </ts>
               <ts e="T1325" id="Seg_327" n="e" s="T1324">pimgeʔ! </ts>
               <ts e="T1326" id="Seg_329" n="e" s="T1325">Kamən </ts>
               <ts e="T1327" id="Seg_331" n="e" s="T1326">kutləj, </ts>
               <ts e="T1328" id="Seg_333" n="e" s="T1327">a </ts>
               <ts e="T1329" id="Seg_335" n="e" s="T1328">šiʔ </ts>
               <ts e="T1330" id="Seg_337" n="e" s="T1329">nʼergöleʔ </ts>
               <ts e="T1331" id="Seg_339" n="e" s="T1330">kalla </ts>
               <ts e="T1332" id="Seg_341" n="e" s="T1331">dʼürbilaʔ!" </ts>
               <ts e="T1333" id="Seg_343" n="e" s="T1332">Dĭgəttə </ts>
               <ts e="T1334" id="Seg_345" n="e" s="T1333">măndə:" </ts>
               <ts e="T1335" id="Seg_347" n="e" s="T1334">A </ts>
               <ts e="T1336" id="Seg_349" n="e" s="T1335">(šində=) </ts>
               <ts e="T1337" id="Seg_351" n="e" s="T1336">girgit </ts>
               <ts e="T1338" id="Seg_353" n="e" s="T1337">kuza </ts>
               <ts e="T1339" id="Seg_355" n="e" s="T1338">pi </ts>
               <ts e="T1340" id="Seg_357" n="e" s="T1339">iləj. </ts>
               <ts e="T1341" id="Seg_359" n="e" s="T1340">I </ts>
               <ts e="T1342" id="Seg_361" n="e" s="T1341">piziʔ </ts>
               <ts e="T1343" id="Seg_363" n="e" s="T1342">kutləj </ts>
               <ts e="T1344" id="Seg_365" n="e" s="T1343">miʔnʼibeʔ". </ts>
               <ts e="T1345" id="Seg_367" n="e" s="T1344">"A </ts>
               <ts e="T1346" id="Seg_369" n="e" s="T1345">kamən </ts>
               <ts e="T1347" id="Seg_371" n="e" s="T1346">dĭ </ts>
               <ts e="T1348" id="Seg_373" n="e" s="T1347">dʼünə </ts>
               <ts e="T1349" id="Seg_375" n="e" s="T1348">iləj, </ts>
               <ts e="T1350" id="Seg_377" n="e" s="T1349">a </ts>
               <ts e="T1351" id="Seg_379" n="e" s="T1350">šiʔ </ts>
               <ts e="T1352" id="Seg_381" n="e" s="T1351">nʼergöluʔpibeʔ". </ts>
               <ts e="T1353" id="Seg_383" n="e" s="T1352">"(A=) </ts>
               <ts e="T1354" id="Seg_385" n="e" s="T1353">A </ts>
               <ts e="T1355" id="Seg_387" n="e" s="T1354">dĭn </ts>
               <ts e="T1356" id="Seg_389" n="e" s="T1355">(iʔ- </ts>
               <ts e="T1357" id="Seg_391" n="e" s="T1356">mə-) </ts>
               <ts e="T1358" id="Seg_393" n="e" s="T1357">udandə </ts>
               <ts e="T1359" id="Seg_395" n="e" s="T1358">možet </ts>
               <ts e="T1360" id="Seg_397" n="e" s="T1359">ige </ts>
               <ts e="T1361" id="Seg_399" n="e" s="T1360">pi". </ts>
               <ts e="T1362" id="Seg_401" n="e" s="T1361">"A </ts>
               <ts e="T1363" id="Seg_403" n="e" s="T1362">šiʔ </ts>
               <ts e="T1364" id="Seg_405" n="e" s="T1363">tože </ts>
               <ts e="T1365" id="Seg_407" n="e" s="T1364">nʼergöluʔpileʔ". </ts>
               <ts e="T1366" id="Seg_409" n="e" s="T1365">Dĭgəttə </ts>
               <ts e="T1367" id="Seg_411" n="e" s="T1366">ijat </ts>
               <ts e="T1368" id="Seg_413" n="e" s="T1367">măndə:" </ts>
               <ts e="T1369" id="Seg_415" n="e" s="T1368">No, </ts>
               <ts e="T1370" id="Seg_417" n="e" s="T1369">kangaʔ, </ts>
               <ts e="T1371" id="Seg_419" n="e" s="T1370">boslaʔ </ts>
               <ts e="T1372" id="Seg_421" n="e" s="T1371">amorgaʔ". </ts>
               <ts e="T1373" id="Seg_423" n="e" s="T1372">Bostə </ts>
               <ts e="T1374" id="Seg_425" n="e" s="T1373">nʼergölüʔpi </ts>
               <ts e="T1375" id="Seg_427" n="e" s="T1374">bar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1311" id="Seg_428" s="T1305">PKZ_196X_Magpie_flk.001 (001)</ta>
            <ta e="T1313" id="Seg_429" s="T1311">PKZ_196X_Magpie_flk.002 (002)</ta>
            <ta e="T1315" id="Seg_430" s="T1313">PKZ_196X_Magpie_flk.003 (003)</ta>
            <ta e="T1319" id="Seg_431" s="T1315">PKZ_196X_Magpie_flk.004 (004)</ta>
            <ta e="T1323" id="Seg_432" s="T1319">PKZ_196X_Magpie_flk.005 (005)</ta>
            <ta e="T1325" id="Seg_433" s="T1323">PKZ_196X_Magpie_flk.006 (006)</ta>
            <ta e="T1332" id="Seg_434" s="T1325">PKZ_196X_Magpie_flk.007 (007)</ta>
            <ta e="T1340" id="Seg_435" s="T1332">PKZ_196X_Magpie_flk.008 (008)</ta>
            <ta e="T1344" id="Seg_436" s="T1340">PKZ_196X_Magpie_flk.009 (009)</ta>
            <ta e="T1352" id="Seg_437" s="T1344">PKZ_196X_Magpie_flk.010 (010)</ta>
            <ta e="T1361" id="Seg_438" s="T1352">PKZ_196X_Magpie_flk.011 (011)</ta>
            <ta e="T1365" id="Seg_439" s="T1361">PKZ_196X_Magpie_flk.012 (012)</ta>
            <ta e="T1372" id="Seg_440" s="T1365">PKZ_196X_Magpie_flk.013 (013)</ta>
            <ta e="T1375" id="Seg_441" s="T1372">PKZ_196X_Magpie_flk.014 (014)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1311" id="Seg_442" s="T1305">(Saroka) bostə esseŋdə măndə: "(Kam-) Kangaʔ! </ta>
            <ta e="T1313" id="Seg_443" s="T1311">Amorgaʔ (boskə)!" </ta>
            <ta e="T1315" id="Seg_444" s="T1313">Dĭzeŋ kambiʔi. </ta>
            <ta e="T1319" id="Seg_445" s="T1315">Dĭgəttə:" Miʔ (pimniem-) pimniebeʔ! </ta>
            <ta e="T1323" id="Seg_446" s="T1319">Girgit-nʼibudʼ kuza kutləj miʔnʼibeʔ!" </ta>
            <ta e="T1325" id="Seg_447" s="T1323">"Iʔ pimgeʔ! </ta>
            <ta e="T1332" id="Seg_448" s="T1325">Kamən kutləj, a šiʔ nʼergöleʔ kalla dʼürbilaʔ!" </ta>
            <ta e="T1340" id="Seg_449" s="T1332">Dĭgəttə măndə:" A (šində=) girgit kuza pi iləj. </ta>
            <ta e="T1344" id="Seg_450" s="T1340">I piziʔ kutləj miʔnʼibeʔ". </ta>
            <ta e="T1352" id="Seg_451" s="T1344">"A kamən dĭ dʼünə iləj, a šiʔ nʼergöluʔpibeʔ". </ta>
            <ta e="T1361" id="Seg_452" s="T1352">"(A=) A dĭn (iʔ- mə-) udandə možet ige pi". </ta>
            <ta e="T1365" id="Seg_453" s="T1361">"A šiʔ tože nʼergöluʔpileʔ". </ta>
            <ta e="T1372" id="Seg_454" s="T1365">Dĭgəttə ijat măndə:" No, kangaʔ, boslaʔ amorgaʔ". </ta>
            <ta e="T1375" id="Seg_455" s="T1372">Bostə nʼergölüʔpi bar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1306" id="Seg_456" s="T1305">saroka</ta>
            <ta e="T1307" id="Seg_457" s="T1306">bos-tə</ta>
            <ta e="T1308" id="Seg_458" s="T1307">es-seŋ-də</ta>
            <ta e="T1309" id="Seg_459" s="T1308">măn-də</ta>
            <ta e="T1311" id="Seg_460" s="T1310">kan-gaʔ</ta>
            <ta e="T1312" id="Seg_461" s="T1311">amor-gaʔ</ta>
            <ta e="T1313" id="Seg_462" s="T1312">boskə</ta>
            <ta e="T1314" id="Seg_463" s="T1313">dĭ-zeŋ</ta>
            <ta e="T1315" id="Seg_464" s="T1314">kam-bi-ʔi</ta>
            <ta e="T1316" id="Seg_465" s="T1315">dĭgəttə</ta>
            <ta e="T1317" id="Seg_466" s="T1316">miʔ</ta>
            <ta e="T1319" id="Seg_467" s="T1318">pim-nie-beʔ</ta>
            <ta e="T1320" id="Seg_468" s="T1319">girgit=nʼibudʼ</ta>
            <ta e="T1321" id="Seg_469" s="T1320">kuza</ta>
            <ta e="T1322" id="Seg_470" s="T1321">kut-lə-j</ta>
            <ta e="T1323" id="Seg_471" s="T1322">miʔnʼibeʔ</ta>
            <ta e="T1324" id="Seg_472" s="T1323">i-ʔ</ta>
            <ta e="T1325" id="Seg_473" s="T1324">pim-geʔ</ta>
            <ta e="T1326" id="Seg_474" s="T1325">kamən</ta>
            <ta e="T1327" id="Seg_475" s="T1326">kut-lə-j</ta>
            <ta e="T1328" id="Seg_476" s="T1327">a</ta>
            <ta e="T1329" id="Seg_477" s="T1328">šiʔ</ta>
            <ta e="T1330" id="Seg_478" s="T1329">nʼergö-leʔ</ta>
            <ta e="T1331" id="Seg_479" s="T1330">kal-la</ta>
            <ta e="T1332" id="Seg_480" s="T1331">dʼür-bi-laʔ</ta>
            <ta e="T1333" id="Seg_481" s="T1332">dĭgəttə</ta>
            <ta e="T1334" id="Seg_482" s="T1333">măn-də</ta>
            <ta e="T1335" id="Seg_483" s="T1334">a</ta>
            <ta e="T1336" id="Seg_484" s="T1335">šində</ta>
            <ta e="T1337" id="Seg_485" s="T1336">girgit</ta>
            <ta e="T1338" id="Seg_486" s="T1337">kuza</ta>
            <ta e="T1339" id="Seg_487" s="T1338">pi</ta>
            <ta e="T1340" id="Seg_488" s="T1339">i-lə-j</ta>
            <ta e="T1341" id="Seg_489" s="T1340">i</ta>
            <ta e="T1342" id="Seg_490" s="T1341">pi-ziʔ</ta>
            <ta e="T1343" id="Seg_491" s="T1342">kut-lə-j</ta>
            <ta e="T1344" id="Seg_492" s="T1343">miʔnʼibeʔ</ta>
            <ta e="T1345" id="Seg_493" s="T1344">a</ta>
            <ta e="T1346" id="Seg_494" s="T1345">kamən</ta>
            <ta e="T1347" id="Seg_495" s="T1346">dĭ</ta>
            <ta e="T1348" id="Seg_496" s="T1347">dʼü-nə</ta>
            <ta e="T1349" id="Seg_497" s="T1348">i-lə-j</ta>
            <ta e="T1350" id="Seg_498" s="T1349">a</ta>
            <ta e="T1351" id="Seg_499" s="T1350">šiʔ</ta>
            <ta e="T1352" id="Seg_500" s="T1351">nʼergö-luʔ-pi-beʔ</ta>
            <ta e="T1353" id="Seg_501" s="T1352">a</ta>
            <ta e="T1354" id="Seg_502" s="T1353">a</ta>
            <ta e="T1355" id="Seg_503" s="T1354">dĭn</ta>
            <ta e="T1358" id="Seg_504" s="T1357">uda-ndə</ta>
            <ta e="T1359" id="Seg_505" s="T1358">možet</ta>
            <ta e="T1360" id="Seg_506" s="T1359">i-ge</ta>
            <ta e="T1361" id="Seg_507" s="T1360">pi</ta>
            <ta e="T1362" id="Seg_508" s="T1361">a</ta>
            <ta e="T1363" id="Seg_509" s="T1362">šiʔ</ta>
            <ta e="T1364" id="Seg_510" s="T1363">tože</ta>
            <ta e="T1365" id="Seg_511" s="T1364">nʼergö-luʔ-pi-leʔ</ta>
            <ta e="T1366" id="Seg_512" s="T1365">dĭgəttə</ta>
            <ta e="T1367" id="Seg_513" s="T1366">ija-t</ta>
            <ta e="T1368" id="Seg_514" s="T1367">măn-də</ta>
            <ta e="T1369" id="Seg_515" s="T1368">no</ta>
            <ta e="T1370" id="Seg_516" s="T1369">kan-gaʔ</ta>
            <ta e="T1371" id="Seg_517" s="T1370">bos-laʔ</ta>
            <ta e="T1372" id="Seg_518" s="T1371">amor-gaʔ</ta>
            <ta e="T1373" id="Seg_519" s="T1372">bos-tə</ta>
            <ta e="T1374" id="Seg_520" s="T1373">nʼergö-lüʔ-pi</ta>
            <ta e="T1375" id="Seg_521" s="T1374">bar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1306" id="Seg_522" s="T1305">soroka</ta>
            <ta e="T1307" id="Seg_523" s="T1306">bos-də</ta>
            <ta e="T1308" id="Seg_524" s="T1307">ešši-zAŋ-Tə</ta>
            <ta e="T1309" id="Seg_525" s="T1308">măn-ntə</ta>
            <ta e="T1311" id="Seg_526" s="T1310">kan-KAʔ</ta>
            <ta e="T1312" id="Seg_527" s="T1311">amor-KAʔ</ta>
            <ta e="T1313" id="Seg_528" s="T1312">bos</ta>
            <ta e="T1314" id="Seg_529" s="T1313">dĭ-zAŋ</ta>
            <ta e="T1315" id="Seg_530" s="T1314">kan-bi-jəʔ</ta>
            <ta e="T1316" id="Seg_531" s="T1315">dĭgəttə</ta>
            <ta e="T1317" id="Seg_532" s="T1316">miʔ</ta>
            <ta e="T1319" id="Seg_533" s="T1318">pim-liA-bAʔ</ta>
            <ta e="T1320" id="Seg_534" s="T1319">girgit=nʼibudʼ</ta>
            <ta e="T1321" id="Seg_535" s="T1320">kuza</ta>
            <ta e="T1322" id="Seg_536" s="T1321">kut-lV-j</ta>
            <ta e="T1323" id="Seg_537" s="T1322">miʔnʼibeʔ</ta>
            <ta e="T1324" id="Seg_538" s="T1323">e-ʔ</ta>
            <ta e="T1325" id="Seg_539" s="T1324">pim-KAʔ</ta>
            <ta e="T1326" id="Seg_540" s="T1325">kamən</ta>
            <ta e="T1327" id="Seg_541" s="T1326">kut-lV-j</ta>
            <ta e="T1328" id="Seg_542" s="T1327">a</ta>
            <ta e="T1329" id="Seg_543" s="T1328">šiʔ</ta>
            <ta e="T1330" id="Seg_544" s="T1329">nʼergö-lAʔ</ta>
            <ta e="T1331" id="Seg_545" s="T1330">kan-lAʔ</ta>
            <ta e="T1332" id="Seg_546" s="T1331">tʼür-bi-lAʔ</ta>
            <ta e="T1333" id="Seg_547" s="T1332">dĭgəttə</ta>
            <ta e="T1334" id="Seg_548" s="T1333">măn-ntə</ta>
            <ta e="T1335" id="Seg_549" s="T1334">a</ta>
            <ta e="T1336" id="Seg_550" s="T1335">šində</ta>
            <ta e="T1337" id="Seg_551" s="T1336">girgit</ta>
            <ta e="T1338" id="Seg_552" s="T1337">kuza</ta>
            <ta e="T1339" id="Seg_553" s="T1338">pi</ta>
            <ta e="T1340" id="Seg_554" s="T1339">i-lV-j</ta>
            <ta e="T1341" id="Seg_555" s="T1340">i</ta>
            <ta e="T1342" id="Seg_556" s="T1341">pi-ziʔ</ta>
            <ta e="T1343" id="Seg_557" s="T1342">kut-lV-j</ta>
            <ta e="T1344" id="Seg_558" s="T1343">miʔnʼibeʔ</ta>
            <ta e="T1345" id="Seg_559" s="T1344">a</ta>
            <ta e="T1346" id="Seg_560" s="T1345">kamən</ta>
            <ta e="T1347" id="Seg_561" s="T1346">dĭ</ta>
            <ta e="T1348" id="Seg_562" s="T1347">tʼo-Tə</ta>
            <ta e="T1349" id="Seg_563" s="T1348">i-lV-j</ta>
            <ta e="T1350" id="Seg_564" s="T1349">a</ta>
            <ta e="T1351" id="Seg_565" s="T1350">šiʔ</ta>
            <ta e="T1352" id="Seg_566" s="T1351">nʼergö-luʔbdə-bi-bAʔ</ta>
            <ta e="T1353" id="Seg_567" s="T1352">a</ta>
            <ta e="T1354" id="Seg_568" s="T1353">a</ta>
            <ta e="T1355" id="Seg_569" s="T1354">dĭn</ta>
            <ta e="T1358" id="Seg_570" s="T1357">uda-gəndə</ta>
            <ta e="T1359" id="Seg_571" s="T1358">možet</ta>
            <ta e="T1360" id="Seg_572" s="T1359">i-gA</ta>
            <ta e="T1361" id="Seg_573" s="T1360">pi</ta>
            <ta e="T1362" id="Seg_574" s="T1361">a</ta>
            <ta e="T1363" id="Seg_575" s="T1362">šiʔ</ta>
            <ta e="T1364" id="Seg_576" s="T1363">tože</ta>
            <ta e="T1365" id="Seg_577" s="T1364">nʼergö-luʔbdə-bi-lAʔ</ta>
            <ta e="T1366" id="Seg_578" s="T1365">dĭgəttə</ta>
            <ta e="T1367" id="Seg_579" s="T1366">ija-t</ta>
            <ta e="T1368" id="Seg_580" s="T1367">măn-ntə</ta>
            <ta e="T1369" id="Seg_581" s="T1368">no</ta>
            <ta e="T1370" id="Seg_582" s="T1369">kan-KAʔ</ta>
            <ta e="T1371" id="Seg_583" s="T1370">bos-lAʔ</ta>
            <ta e="T1372" id="Seg_584" s="T1371">amor-KAʔ</ta>
            <ta e="T1373" id="Seg_585" s="T1372">bos-də</ta>
            <ta e="T1374" id="Seg_586" s="T1373">nʼergö-luʔbdə-bi</ta>
            <ta e="T1375" id="Seg_587" s="T1374">bar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1306" id="Seg_588" s="T1305">magpie.[NOM.SG]</ta>
            <ta e="T1307" id="Seg_589" s="T1306">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1308" id="Seg_590" s="T1307">child-PL-LAT</ta>
            <ta e="T1309" id="Seg_591" s="T1308">say-IPFVZ.[3SG]</ta>
            <ta e="T1311" id="Seg_592" s="T1310">go-IMP.2PL</ta>
            <ta e="T1312" id="Seg_593" s="T1311">eat-IMP.2PL</ta>
            <ta e="T1313" id="Seg_594" s="T1312">self</ta>
            <ta e="T1314" id="Seg_595" s="T1313">this-PL</ta>
            <ta e="T1315" id="Seg_596" s="T1314">go-PST-3PL</ta>
            <ta e="T1316" id="Seg_597" s="T1315">then</ta>
            <ta e="T1317" id="Seg_598" s="T1316">we.NOM</ta>
            <ta e="T1319" id="Seg_599" s="T1318">fear-PRS-1PL</ta>
            <ta e="T1320" id="Seg_600" s="T1319">what.kind=INDEF</ta>
            <ta e="T1321" id="Seg_601" s="T1320">man.[NOM.SG]</ta>
            <ta e="T1322" id="Seg_602" s="T1321">kill-FUT-3SG</ta>
            <ta e="T1323" id="Seg_603" s="T1322">we.ACC</ta>
            <ta e="T1324" id="Seg_604" s="T1323">NEG.AUX-IMP.2SG</ta>
            <ta e="T1325" id="Seg_605" s="T1324">fear-CNG</ta>
            <ta e="T1326" id="Seg_606" s="T1325">when</ta>
            <ta e="T1327" id="Seg_607" s="T1326">kill-FUT-3SG</ta>
            <ta e="T1328" id="Seg_608" s="T1327">and</ta>
            <ta e="T1329" id="Seg_609" s="T1328">you.PL.NOM</ta>
            <ta e="T1330" id="Seg_610" s="T1329">fly-CVB</ta>
            <ta e="T1331" id="Seg_611" s="T1330">go-CVB</ta>
            <ta e="T1332" id="Seg_612" s="T1331">disappear-PST-2PL</ta>
            <ta e="T1333" id="Seg_613" s="T1332">then</ta>
            <ta e="T1334" id="Seg_614" s="T1333">say-IPFVZ.[3SG]</ta>
            <ta e="T1335" id="Seg_615" s="T1334">and</ta>
            <ta e="T1336" id="Seg_616" s="T1335">who.[NOM.SG]</ta>
            <ta e="T1337" id="Seg_617" s="T1336">what.kind</ta>
            <ta e="T1338" id="Seg_618" s="T1337">man.[NOM.SG]</ta>
            <ta e="T1339" id="Seg_619" s="T1338">stone.[NOM.SG]</ta>
            <ta e="T1340" id="Seg_620" s="T1339">take-FUT-3SG</ta>
            <ta e="T1341" id="Seg_621" s="T1340">and</ta>
            <ta e="T1342" id="Seg_622" s="T1341">stone-INS</ta>
            <ta e="T1343" id="Seg_623" s="T1342">kill-FUT-3SG</ta>
            <ta e="T1344" id="Seg_624" s="T1343">we.ACC</ta>
            <ta e="T1345" id="Seg_625" s="T1344">and</ta>
            <ta e="T1346" id="Seg_626" s="T1345">when</ta>
            <ta e="T1347" id="Seg_627" s="T1346">this.[NOM.SG]</ta>
            <ta e="T1348" id="Seg_628" s="T1347">earth-LAT</ta>
            <ta e="T1349" id="Seg_629" s="T1348">be-FUT-3SG</ta>
            <ta e="T1350" id="Seg_630" s="T1349">and</ta>
            <ta e="T1351" id="Seg_631" s="T1350">you.PL.NOM</ta>
            <ta e="T1352" id="Seg_632" s="T1351">fly-MOM-PST-1PL</ta>
            <ta e="T1353" id="Seg_633" s="T1352">and</ta>
            <ta e="T1354" id="Seg_634" s="T1353">and</ta>
            <ta e="T1355" id="Seg_635" s="T1354">there</ta>
            <ta e="T1358" id="Seg_636" s="T1357">hand-LAT/LOC.3SG</ta>
            <ta e="T1359" id="Seg_637" s="T1358">maybe</ta>
            <ta e="T1360" id="Seg_638" s="T1359">be-PRS.[3SG]</ta>
            <ta e="T1361" id="Seg_639" s="T1360">stone.[NOM.SG]</ta>
            <ta e="T1362" id="Seg_640" s="T1361">and</ta>
            <ta e="T1363" id="Seg_641" s="T1362">you.PL.NOM</ta>
            <ta e="T1364" id="Seg_642" s="T1363">also</ta>
            <ta e="T1365" id="Seg_643" s="T1364">fly-MOM-PST-2PL</ta>
            <ta e="T1366" id="Seg_644" s="T1365">then</ta>
            <ta e="T1367" id="Seg_645" s="T1366">mother-NOM/GEN.3SG</ta>
            <ta e="T1368" id="Seg_646" s="T1367">say-IPFVZ.[3SG]</ta>
            <ta e="T1369" id="Seg_647" s="T1368">well</ta>
            <ta e="T1370" id="Seg_648" s="T1369">go-IMP.2PL</ta>
            <ta e="T1371" id="Seg_649" s="T1370">self-NOM/GEN/ACC.2PL</ta>
            <ta e="T1372" id="Seg_650" s="T1371">eat-IMP.2PL</ta>
            <ta e="T1373" id="Seg_651" s="T1372">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1374" id="Seg_652" s="T1373">fly-MOM-PST.[3SG]</ta>
            <ta e="T1375" id="Seg_653" s="T1374">PTCL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1306" id="Seg_654" s="T1305">сорока.[NOM.SG]</ta>
            <ta e="T1307" id="Seg_655" s="T1306">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1308" id="Seg_656" s="T1307">ребенок-PL-LAT</ta>
            <ta e="T1309" id="Seg_657" s="T1308">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1311" id="Seg_658" s="T1310">пойти-IMP.2PL</ta>
            <ta e="T1312" id="Seg_659" s="T1311">есть-IMP.2PL</ta>
            <ta e="T1313" id="Seg_660" s="T1312">сам</ta>
            <ta e="T1314" id="Seg_661" s="T1313">этот-PL</ta>
            <ta e="T1315" id="Seg_662" s="T1314">пойти-PST-3PL</ta>
            <ta e="T1316" id="Seg_663" s="T1315">тогда</ta>
            <ta e="T1317" id="Seg_664" s="T1316">мы.NOM</ta>
            <ta e="T1319" id="Seg_665" s="T1318">бояться-PRS-1PL</ta>
            <ta e="T1320" id="Seg_666" s="T1319">какой=INDEF</ta>
            <ta e="T1321" id="Seg_667" s="T1320">мужчина.[NOM.SG]</ta>
            <ta e="T1322" id="Seg_668" s="T1321">убить-FUT-3SG</ta>
            <ta e="T1323" id="Seg_669" s="T1322">мы.ACC</ta>
            <ta e="T1324" id="Seg_670" s="T1323">NEG.AUX-IMP.2SG</ta>
            <ta e="T1325" id="Seg_671" s="T1324">бояться-CNG</ta>
            <ta e="T1326" id="Seg_672" s="T1325">когда</ta>
            <ta e="T1327" id="Seg_673" s="T1326">убить-FUT-3SG</ta>
            <ta e="T1328" id="Seg_674" s="T1327">а</ta>
            <ta e="T1329" id="Seg_675" s="T1328">вы.NOM</ta>
            <ta e="T1330" id="Seg_676" s="T1329">лететь-CVB</ta>
            <ta e="T1331" id="Seg_677" s="T1330">пойти-CVB</ta>
            <ta e="T1332" id="Seg_678" s="T1331">исчезнуть-PST-2PL</ta>
            <ta e="T1333" id="Seg_679" s="T1332">тогда</ta>
            <ta e="T1334" id="Seg_680" s="T1333">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1335" id="Seg_681" s="T1334">а</ta>
            <ta e="T1336" id="Seg_682" s="T1335">кто.[NOM.SG]</ta>
            <ta e="T1337" id="Seg_683" s="T1336">какой</ta>
            <ta e="T1338" id="Seg_684" s="T1337">мужчина.[NOM.SG]</ta>
            <ta e="T1339" id="Seg_685" s="T1338">камень.[NOM.SG]</ta>
            <ta e="T1340" id="Seg_686" s="T1339">взять-FUT-3SG</ta>
            <ta e="T1341" id="Seg_687" s="T1340">и</ta>
            <ta e="T1342" id="Seg_688" s="T1341">камень-INS</ta>
            <ta e="T1343" id="Seg_689" s="T1342">убить-FUT-3SG</ta>
            <ta e="T1344" id="Seg_690" s="T1343">мы.ACC</ta>
            <ta e="T1345" id="Seg_691" s="T1344">а</ta>
            <ta e="T1346" id="Seg_692" s="T1345">когда</ta>
            <ta e="T1347" id="Seg_693" s="T1346">этот.[NOM.SG]</ta>
            <ta e="T1348" id="Seg_694" s="T1347">земля-LAT</ta>
            <ta e="T1349" id="Seg_695" s="T1348">быть-FUT-3SG</ta>
            <ta e="T1350" id="Seg_696" s="T1349">а</ta>
            <ta e="T1351" id="Seg_697" s="T1350">вы.NOM</ta>
            <ta e="T1352" id="Seg_698" s="T1351">лететь-MOM-PST-1PL</ta>
            <ta e="T1353" id="Seg_699" s="T1352">а</ta>
            <ta e="T1354" id="Seg_700" s="T1353">а</ta>
            <ta e="T1355" id="Seg_701" s="T1354">там</ta>
            <ta e="T1358" id="Seg_702" s="T1357">рука-LAT/LOC.3SG</ta>
            <ta e="T1359" id="Seg_703" s="T1358">может.быть</ta>
            <ta e="T1360" id="Seg_704" s="T1359">быть-PRS.[3SG]</ta>
            <ta e="T1361" id="Seg_705" s="T1360">камень.[NOM.SG]</ta>
            <ta e="T1362" id="Seg_706" s="T1361">а</ta>
            <ta e="T1363" id="Seg_707" s="T1362">вы.NOM</ta>
            <ta e="T1364" id="Seg_708" s="T1363">тоже</ta>
            <ta e="T1365" id="Seg_709" s="T1364">лететь-MOM-PST-2PL</ta>
            <ta e="T1366" id="Seg_710" s="T1365">тогда</ta>
            <ta e="T1367" id="Seg_711" s="T1366">мать-NOM/GEN.3SG</ta>
            <ta e="T1368" id="Seg_712" s="T1367">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1369" id="Seg_713" s="T1368">ну</ta>
            <ta e="T1370" id="Seg_714" s="T1369">пойти-IMP.2PL</ta>
            <ta e="T1371" id="Seg_715" s="T1370">сам-NOM/GEN/ACC.2PL</ta>
            <ta e="T1372" id="Seg_716" s="T1371">есть-IMP.2PL</ta>
            <ta e="T1373" id="Seg_717" s="T1372">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1374" id="Seg_718" s="T1373">лететь-MOM-PST.[3SG]</ta>
            <ta e="T1375" id="Seg_719" s="T1374">PTCL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1306" id="Seg_720" s="T1305">n-n:case</ta>
            <ta e="T1307" id="Seg_721" s="T1306">refl-n:case.poss</ta>
            <ta e="T1308" id="Seg_722" s="T1307">n-n:num-n:case</ta>
            <ta e="T1309" id="Seg_723" s="T1308">v-v&gt;v-v:pn</ta>
            <ta e="T1311" id="Seg_724" s="T1310">v-v:mood.pn</ta>
            <ta e="T1312" id="Seg_725" s="T1311">v-v:mood.pn</ta>
            <ta e="T1313" id="Seg_726" s="T1312">refl</ta>
            <ta e="T1314" id="Seg_727" s="T1313">dempro-n:num</ta>
            <ta e="T1315" id="Seg_728" s="T1314">v-v:tense-v:pn</ta>
            <ta e="T1316" id="Seg_729" s="T1315">adv</ta>
            <ta e="T1317" id="Seg_730" s="T1316">pers</ta>
            <ta e="T1319" id="Seg_731" s="T1318">v-v:tense-v:pn</ta>
            <ta e="T1320" id="Seg_732" s="T1319">que=ptcl</ta>
            <ta e="T1321" id="Seg_733" s="T1320">n-n:case</ta>
            <ta e="T1322" id="Seg_734" s="T1321">v-v:tense-v:pn</ta>
            <ta e="T1323" id="Seg_735" s="T1322">pers</ta>
            <ta e="T1324" id="Seg_736" s="T1323">aux-v:mood.pn</ta>
            <ta e="T1325" id="Seg_737" s="T1324">v-v:mood.pn</ta>
            <ta e="T1326" id="Seg_738" s="T1325">que</ta>
            <ta e="T1327" id="Seg_739" s="T1326">v-v:tense-v:pn</ta>
            <ta e="T1328" id="Seg_740" s="T1327">conj</ta>
            <ta e="T1329" id="Seg_741" s="T1328">pers</ta>
            <ta e="T1330" id="Seg_742" s="T1329">v-v:n.fin</ta>
            <ta e="T1331" id="Seg_743" s="T1330">v-v:n.fin</ta>
            <ta e="T1332" id="Seg_744" s="T1331">v-v:tense-v:pn</ta>
            <ta e="T1333" id="Seg_745" s="T1332">adv</ta>
            <ta e="T1334" id="Seg_746" s="T1333">v-v&gt;v-v:pn</ta>
            <ta e="T1335" id="Seg_747" s="T1334">conj</ta>
            <ta e="T1336" id="Seg_748" s="T1335">que</ta>
            <ta e="T1337" id="Seg_749" s="T1336">que</ta>
            <ta e="T1338" id="Seg_750" s="T1337">n-n:case</ta>
            <ta e="T1339" id="Seg_751" s="T1338">n-n:case</ta>
            <ta e="T1340" id="Seg_752" s="T1339">v-v:tense-v:pn</ta>
            <ta e="T1341" id="Seg_753" s="T1340">conj</ta>
            <ta e="T1342" id="Seg_754" s="T1341">n-n:case</ta>
            <ta e="T1343" id="Seg_755" s="T1342">v-v:tense-v:pn</ta>
            <ta e="T1344" id="Seg_756" s="T1343">pers</ta>
            <ta e="T1345" id="Seg_757" s="T1344">conj</ta>
            <ta e="T1346" id="Seg_758" s="T1345">que</ta>
            <ta e="T1347" id="Seg_759" s="T1346">dempro-n:case</ta>
            <ta e="T1348" id="Seg_760" s="T1347">n-n:case</ta>
            <ta e="T1349" id="Seg_761" s="T1348">v-v:tense-v:pn</ta>
            <ta e="T1350" id="Seg_762" s="T1349">conj</ta>
            <ta e="T1351" id="Seg_763" s="T1350">pers</ta>
            <ta e="T1352" id="Seg_764" s="T1351">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1353" id="Seg_765" s="T1352">conj</ta>
            <ta e="T1354" id="Seg_766" s="T1353">conj</ta>
            <ta e="T1355" id="Seg_767" s="T1354">adv</ta>
            <ta e="T1358" id="Seg_768" s="T1357">n-n:case.poss</ta>
            <ta e="T1359" id="Seg_769" s="T1358">ptcl</ta>
            <ta e="T1360" id="Seg_770" s="T1359">v-v:tense-v:pn</ta>
            <ta e="T1361" id="Seg_771" s="T1360">n-n:case</ta>
            <ta e="T1362" id="Seg_772" s="T1361">conj</ta>
            <ta e="T1363" id="Seg_773" s="T1362">pers</ta>
            <ta e="T1364" id="Seg_774" s="T1363">ptcl</ta>
            <ta e="T1365" id="Seg_775" s="T1364">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1366" id="Seg_776" s="T1365">adv</ta>
            <ta e="T1367" id="Seg_777" s="T1366">n-n:case.poss</ta>
            <ta e="T1368" id="Seg_778" s="T1367">v-v&gt;v-v:pn</ta>
            <ta e="T1369" id="Seg_779" s="T1368">ptcl</ta>
            <ta e="T1370" id="Seg_780" s="T1369">v-v:mood.pn</ta>
            <ta e="T1371" id="Seg_781" s="T1370">refl-n:case.poss</ta>
            <ta e="T1372" id="Seg_782" s="T1371">v-v:mood.pn</ta>
            <ta e="T1373" id="Seg_783" s="T1372">refl-n:case.poss</ta>
            <ta e="T1374" id="Seg_784" s="T1373">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1375" id="Seg_785" s="T1374">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1306" id="Seg_786" s="T1305">n</ta>
            <ta e="T1307" id="Seg_787" s="T1306">refl</ta>
            <ta e="T1308" id="Seg_788" s="T1307">n</ta>
            <ta e="T1309" id="Seg_789" s="T1308">v</ta>
            <ta e="T1311" id="Seg_790" s="T1310">v</ta>
            <ta e="T1312" id="Seg_791" s="T1311">v</ta>
            <ta e="T1313" id="Seg_792" s="T1312">refl</ta>
            <ta e="T1314" id="Seg_793" s="T1313">dempro</ta>
            <ta e="T1315" id="Seg_794" s="T1314">v</ta>
            <ta e="T1316" id="Seg_795" s="T1315">adv</ta>
            <ta e="T1317" id="Seg_796" s="T1316">pers</ta>
            <ta e="T1319" id="Seg_797" s="T1318">v</ta>
            <ta e="T1320" id="Seg_798" s="T1319">que</ta>
            <ta e="T1321" id="Seg_799" s="T1320">n</ta>
            <ta e="T1322" id="Seg_800" s="T1321">v</ta>
            <ta e="T1323" id="Seg_801" s="T1322">pers</ta>
            <ta e="T1324" id="Seg_802" s="T1323">aux</ta>
            <ta e="T1325" id="Seg_803" s="T1324">v</ta>
            <ta e="T1326" id="Seg_804" s="T1325">conj</ta>
            <ta e="T1327" id="Seg_805" s="T1326">v</ta>
            <ta e="T1328" id="Seg_806" s="T1327">conj</ta>
            <ta e="T1329" id="Seg_807" s="T1328">pers</ta>
            <ta e="T1330" id="Seg_808" s="T1329">v</ta>
            <ta e="T1331" id="Seg_809" s="T1330">v</ta>
            <ta e="T1332" id="Seg_810" s="T1331">v</ta>
            <ta e="T1333" id="Seg_811" s="T1332">adv</ta>
            <ta e="T1334" id="Seg_812" s="T1333">v</ta>
            <ta e="T1335" id="Seg_813" s="T1334">conj</ta>
            <ta e="T1336" id="Seg_814" s="T1335">que</ta>
            <ta e="T1337" id="Seg_815" s="T1336">que</ta>
            <ta e="T1338" id="Seg_816" s="T1337">n</ta>
            <ta e="T1339" id="Seg_817" s="T1338">n</ta>
            <ta e="T1340" id="Seg_818" s="T1339">v</ta>
            <ta e="T1341" id="Seg_819" s="T1340">conj</ta>
            <ta e="T1342" id="Seg_820" s="T1341">n</ta>
            <ta e="T1343" id="Seg_821" s="T1342">v</ta>
            <ta e="T1344" id="Seg_822" s="T1343">pers</ta>
            <ta e="T1345" id="Seg_823" s="T1344">conj</ta>
            <ta e="T1346" id="Seg_824" s="T1345">conj</ta>
            <ta e="T1347" id="Seg_825" s="T1346">dempro</ta>
            <ta e="T1348" id="Seg_826" s="T1347">n</ta>
            <ta e="T1349" id="Seg_827" s="T1348">v</ta>
            <ta e="T1350" id="Seg_828" s="T1349">conj</ta>
            <ta e="T1351" id="Seg_829" s="T1350">pers</ta>
            <ta e="T1352" id="Seg_830" s="T1351">v</ta>
            <ta e="T1353" id="Seg_831" s="T1352">conj</ta>
            <ta e="T1354" id="Seg_832" s="T1353">conj</ta>
            <ta e="T1355" id="Seg_833" s="T1354">adv</ta>
            <ta e="T1358" id="Seg_834" s="T1357">n</ta>
            <ta e="T1359" id="Seg_835" s="T1358">ptcl</ta>
            <ta e="T1360" id="Seg_836" s="T1359">v</ta>
            <ta e="T1361" id="Seg_837" s="T1360">n</ta>
            <ta e="T1362" id="Seg_838" s="T1361">conj</ta>
            <ta e="T1363" id="Seg_839" s="T1362">pers</ta>
            <ta e="T1364" id="Seg_840" s="T1363">ptcl</ta>
            <ta e="T1365" id="Seg_841" s="T1364">v</ta>
            <ta e="T1366" id="Seg_842" s="T1365">adv</ta>
            <ta e="T1367" id="Seg_843" s="T1366">n</ta>
            <ta e="T1368" id="Seg_844" s="T1367">v</ta>
            <ta e="T1369" id="Seg_845" s="T1368">ptcl</ta>
            <ta e="T1370" id="Seg_846" s="T1369">v</ta>
            <ta e="T1371" id="Seg_847" s="T1370">refl</ta>
            <ta e="T1372" id="Seg_848" s="T1371">v</ta>
            <ta e="T1373" id="Seg_849" s="T1372">refl</ta>
            <ta e="T1374" id="Seg_850" s="T1373">v</ta>
            <ta e="T1375" id="Seg_851" s="T1374">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1306" id="Seg_852" s="T1305">np.h:A</ta>
            <ta e="T1308" id="Seg_853" s="T1307">np.h:R</ta>
            <ta e="T1311" id="Seg_854" s="T1310">0.2.h:A</ta>
            <ta e="T1312" id="Seg_855" s="T1311">0.2.h:A</ta>
            <ta e="T1314" id="Seg_856" s="T1313">pro.h:A</ta>
            <ta e="T1316" id="Seg_857" s="T1315">adv:Time</ta>
            <ta e="T1317" id="Seg_858" s="T1316">pro.h:E</ta>
            <ta e="T1321" id="Seg_859" s="T1320">np.h:A</ta>
            <ta e="T1323" id="Seg_860" s="T1322">pro.h:P</ta>
            <ta e="T1324" id="Seg_861" s="T1323">0.2.h:E</ta>
            <ta e="T1327" id="Seg_862" s="T1326">0.3.h:A</ta>
            <ta e="T1329" id="Seg_863" s="T1328">pro.h:A</ta>
            <ta e="T1333" id="Seg_864" s="T1332">adv:Time</ta>
            <ta e="T1334" id="Seg_865" s="T1333">0.3.h:A</ta>
            <ta e="T1338" id="Seg_866" s="T1337">np.h:A</ta>
            <ta e="T1339" id="Seg_867" s="T1338">np:Th</ta>
            <ta e="T1342" id="Seg_868" s="T1341">np:Ins</ta>
            <ta e="T1343" id="Seg_869" s="T1342">0.3.h:A</ta>
            <ta e="T1344" id="Seg_870" s="T1343">pro.h:P</ta>
            <ta e="T1347" id="Seg_871" s="T1346">pro.h:A</ta>
            <ta e="T1348" id="Seg_872" s="T1347">np:G</ta>
            <ta e="T1351" id="Seg_873" s="T1350">pro.h:A</ta>
            <ta e="T1355" id="Seg_874" s="T1354">adv:L</ta>
            <ta e="T1358" id="Seg_875" s="T1357">np:L</ta>
            <ta e="T1361" id="Seg_876" s="T1360">np:Th</ta>
            <ta e="T1363" id="Seg_877" s="T1362">pro.h:A</ta>
            <ta e="T1366" id="Seg_878" s="T1365">adv:Time</ta>
            <ta e="T1367" id="Seg_879" s="T1366">np.h:A 0.3.h:Poss</ta>
            <ta e="T1370" id="Seg_880" s="T1369">0.2.h:A</ta>
            <ta e="T1372" id="Seg_881" s="T1371">0.2.h:A</ta>
            <ta e="T1374" id="Seg_882" s="T1373">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1306" id="Seg_883" s="T1305">np.h:S</ta>
            <ta e="T1309" id="Seg_884" s="T1308">v:pred</ta>
            <ta e="T1311" id="Seg_885" s="T1310">v:pred 0.2.h:S</ta>
            <ta e="T1312" id="Seg_886" s="T1311">v:pred 0.2.h:S</ta>
            <ta e="T1314" id="Seg_887" s="T1313">pro.h:S</ta>
            <ta e="T1315" id="Seg_888" s="T1314">v:pred</ta>
            <ta e="T1317" id="Seg_889" s="T1316">pro.h:S</ta>
            <ta e="T1319" id="Seg_890" s="T1318">v:pred</ta>
            <ta e="T1321" id="Seg_891" s="T1320">np.h:S</ta>
            <ta e="T1322" id="Seg_892" s="T1321">v:pred</ta>
            <ta e="T1323" id="Seg_893" s="T1322">pro.h:O</ta>
            <ta e="T1324" id="Seg_894" s="T1323">v:pred 0.2.h:S</ta>
            <ta e="T1326" id="Seg_895" s="T1325">s:temp</ta>
            <ta e="T1327" id="Seg_896" s="T1326">v:pred 0.3.h:S</ta>
            <ta e="T1329" id="Seg_897" s="T1328">pro.h:S</ta>
            <ta e="T1330" id="Seg_898" s="T1329">conv:pred</ta>
            <ta e="T1331" id="Seg_899" s="T1330">conv:pred</ta>
            <ta e="T1332" id="Seg_900" s="T1331">v:pred</ta>
            <ta e="T1334" id="Seg_901" s="T1333">v:pred 0.3.h:S</ta>
            <ta e="T1338" id="Seg_902" s="T1337">np.h:S</ta>
            <ta e="T1339" id="Seg_903" s="T1338">np:O</ta>
            <ta e="T1340" id="Seg_904" s="T1339">v:pred</ta>
            <ta e="T1343" id="Seg_905" s="T1342">v:pred 0.3.h:S</ta>
            <ta e="T1344" id="Seg_906" s="T1343">pro.h:O</ta>
            <ta e="T1346" id="Seg_907" s="T1345">s:temp</ta>
            <ta e="T1347" id="Seg_908" s="T1346">pro.h:S</ta>
            <ta e="T1349" id="Seg_909" s="T1348">v:pred</ta>
            <ta e="T1351" id="Seg_910" s="T1350">pro.h:S</ta>
            <ta e="T1352" id="Seg_911" s="T1351">v:pred</ta>
            <ta e="T1360" id="Seg_912" s="T1359">v:pred</ta>
            <ta e="T1361" id="Seg_913" s="T1360">np:S</ta>
            <ta e="T1363" id="Seg_914" s="T1362">pro.h:S</ta>
            <ta e="T1365" id="Seg_915" s="T1364">v:pred</ta>
            <ta e="T1367" id="Seg_916" s="T1366">np.h:S</ta>
            <ta e="T1368" id="Seg_917" s="T1367">v:pred</ta>
            <ta e="T1370" id="Seg_918" s="T1369">v:pred 0.2.h:S</ta>
            <ta e="T1372" id="Seg_919" s="T1371">v:pred 0.2.h:S</ta>
            <ta e="T1374" id="Seg_920" s="T1373">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1306" id="Seg_921" s="T1305">RUS:cult</ta>
            <ta e="T1320" id="Seg_922" s="T1319">RUS:gram(INDEF)</ta>
            <ta e="T1328" id="Seg_923" s="T1327">RUS:gram</ta>
            <ta e="T1335" id="Seg_924" s="T1334">RUS:gram</ta>
            <ta e="T1341" id="Seg_925" s="T1340">RUS:gram</ta>
            <ta e="T1345" id="Seg_926" s="T1344">RUS:gram</ta>
            <ta e="T1350" id="Seg_927" s="T1349">RUS:gram</ta>
            <ta e="T1353" id="Seg_928" s="T1352">RUS:gram</ta>
            <ta e="T1354" id="Seg_929" s="T1353">RUS:gram</ta>
            <ta e="T1359" id="Seg_930" s="T1358">RUS:mod</ta>
            <ta e="T1362" id="Seg_931" s="T1361">RUS:gram</ta>
            <ta e="T1364" id="Seg_932" s="T1363">RUS:mod</ta>
            <ta e="T1369" id="Seg_933" s="T1368">RUS:disc</ta>
            <ta e="T1375" id="Seg_934" s="T1374">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1311" id="Seg_935" s="T1305">Сорока говорит своим детям: «Идите!</ta>
            <ta e="T1313" id="Seg_936" s="T1311">Ешьте сами!»</ta>
            <ta e="T1315" id="Seg_937" s="T1313">Они полетели.</ta>
            <ta e="T1319" id="Seg_938" s="T1315">Потом [говорят]: «Мы боимся!</ta>
            <ta e="T1323" id="Seg_939" s="T1319">Какой-нибудь человек нас убьёт!»</ta>
            <ta e="T1325" id="Seg_940" s="T1323">«Не бойтесь!</ta>
            <ta e="T1332" id="Seg_941" s="T1325">Если [захочет] убить, вы улетели [=улетите]».</ta>
            <ta e="T1340" id="Seg_942" s="T1332">Тогда [они] говорят: «А если какой-нибудь человек возьмёт камень?</ta>
            <ta e="T1344" id="Seg_943" s="T1340">И камнем нас убьёт».</ta>
            <ta e="T1352" id="Seg_944" s="T1344">«А когда он к земле нагнётся, вы улетели [=улетите]».</ta>
            <ta e="T1361" id="Seg_945" s="T1352">«А у него там… может быть, у него в руке камень».</ta>
            <ta e="T1365" id="Seg_946" s="T1361">«А вы тоже улетели [=улетите]».</ta>
            <ta e="T1372" id="Seg_947" s="T1365">Потом их мать говорит: «Ну, идите, ешьте сами».</ta>
            <ta e="T1375" id="Seg_948" s="T1372">[И] сама улетела.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1311" id="Seg_949" s="T1305">The magpie says to its children: "Go!</ta>
            <ta e="T1313" id="Seg_950" s="T1311">Eat by yourselves!"</ta>
            <ta e="T1315" id="Seg_951" s="T1313">They went.</ta>
            <ta e="T1319" id="Seg_952" s="T1315">Then [they say]: "We are afraid!</ta>
            <ta e="T1323" id="Seg_953" s="T1319">Some man will kill us!"</ta>
            <ta e="T1325" id="Seg_954" s="T1323">"Don't be afraid!</ta>
            <ta e="T1332" id="Seg_955" s="T1325">When he [wants to] kill you, you will go away flying!"</ta>
            <ta e="T1340" id="Seg_956" s="T1332">Then [they] say: "But if some man will take a stone?</ta>
            <ta e="T1344" id="Seg_957" s="T1340">And with a stone will kill us."</ta>
            <ta e="T1352" id="Seg_958" s="T1344">"But when it will reach to the ground, you will have flown away."</ta>
            <ta e="T1361" id="Seg_959" s="T1352">"But there… maybe there is a stone in his hand."</ta>
            <ta e="T1365" id="Seg_960" s="T1361">"But you also (will have) flown away."</ta>
            <ta e="T1372" id="Seg_961" s="T1365">Then their mother says: "Well, go, eat by yourselves".</ta>
            <ta e="T1375" id="Seg_962" s="T1372">[And] herself flew away.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1311" id="Seg_963" s="T1305">Die Elster sagt zu ihren Kindern: „Geht!“</ta>
            <ta e="T1313" id="Seg_964" s="T1311">„Frisst alleine!“</ta>
            <ta e="T1315" id="Seg_965" s="T1313">Sie gingen.</ta>
            <ta e="T1319" id="Seg_966" s="T1315">Dann [sagten sie]: „Wir haben Angst!</ta>
            <ta e="T1323" id="Seg_967" s="T1319">Irgendein Mann wird uns töten!“</ta>
            <ta e="T1325" id="Seg_968" s="T1323">„Habt keine Angst!</ta>
            <ta e="T1332" id="Seg_969" s="T1325">Wenn er euch töten [will], wird ihr fliegend abhauen!“</ta>
            <ta e="T1340" id="Seg_970" s="T1332">Dann sagten [sie]: „Aber wenn irgendein Mann einen Stein nimmt?</ta>
            <ta e="T1344" id="Seg_971" s="T1340">Und mit einem Stein uns töten will.“</ta>
            <ta e="T1352" id="Seg_972" s="T1344">„Aber bis er den Boden erreicht, wird ihr weg geflogen sein.“</ta>
            <ta e="T1361" id="Seg_973" s="T1352">„Aber er…, vielleicht ist ein Stein schon in seiner Hand.“</ta>
            <ta e="T1365" id="Seg_974" s="T1361">„Doch ihr (wird) auch weggeflogen sein.“ </ta>
            <ta e="T1372" id="Seg_975" s="T1365">Dann sagt ihre Mutter: „Also, geht, und frisst alleine.“</ta>
            <ta e="T1375" id="Seg_976" s="T1372">[Und] sie selber flog weg.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1313" id="Seg_977" s="T1311">[GVY:] boskə = bostə?</ta>
            <ta e="T1325" id="Seg_978" s="T1323">Igeʔ would be correct.</ta>
            <ta e="T1352" id="Seg_979" s="T1344">[KlT] 1PL instead of 2PL.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
