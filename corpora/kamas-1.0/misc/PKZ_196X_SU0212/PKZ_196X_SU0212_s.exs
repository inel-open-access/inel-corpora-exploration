<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID195F7657-4B4B-372A-2296-4066CB7B2FE6">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0212.wav" />
         <referenced-file url="PKZ_196X_SU0212.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0212\PKZ_196X_SU0212.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1036</ud-information>
            <ud-information attribute-name="# HIAT:w">687</ud-information>
            <ud-information attribute-name="# e">682</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">151</ud-information>
            <ud-information attribute-name="# sc">8</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.061" type="appl" />
         <tli id="T1" time="0.742" type="appl" />
         <tli id="T2" time="1.422" type="appl" />
         <tli id="T3" time="2.102" type="appl" />
         <tli id="T4" time="3.113312385498706" />
         <tli id="T5" time="5.199965012181993" />
         <tli id="T6" time="6.133" type="appl" />
         <tli id="T7" time="6.964" type="appl" />
         <tli id="T8" time="7.795" type="appl" />
         <tli id="T9" time="8.627" type="appl" />
         <tli id="T10" time="9.458" type="appl" />
         <tli id="T11" time="10.289" type="appl" />
         <tli id="T12" time="11.786587360945852" />
         <tli id="T13" time="12.775" type="appl" />
         <tli id="T14" time="14.493235816004685" />
         <tli id="T15" time="15.224" type="appl" />
         <tli id="T16" time="15.724" type="appl" />
         <tli id="T17" time="16.225" type="appl" />
         <tli id="T18" time="16.61988817355091" />
         <tli id="T19" time="17.667" type="appl" />
         <tli id="T20" time="18.61" type="appl" />
         <tli id="T21" time="19.552" type="appl" />
         <tli id="T22" time="20.55319504173985" />
         <tli id="T23" time="21.102" type="appl" />
         <tli id="T24" time="21.454" type="appl" />
         <tli id="T25" time="21.806" type="appl" />
         <tli id="T26" time="22.91984578446371" />
         <tli id="T27" time="23.693" type="appl" />
         <tli id="T28" time="24.397" type="appl" />
         <tli id="T29" time="25.101" type="appl" />
         <tli id="T30" time="25.805" type="appl" />
         <tli id="T31" time="27.61314753904848" />
         <tli id="T32" time="28.328" type="appl" />
         <tli id="T33" time="28.886" type="appl" />
         <tli id="T34" time="29.501" type="appl" />
         <tli id="T35" time="30.117" type="appl" />
         <tli id="T36" time="30.732" type="appl" />
         <tli id="T37" time="32.17311685742346" />
         <tli id="T38" time="32.948" type="appl" />
         <tli id="T39" time="33.665" type="appl" />
         <tli id="T40" time="34.381" type="appl" />
         <tli id="T41" time="35.098" type="appl" />
         <tli id="T42" time="35.814" type="appl" />
         <tli id="T43" time="36.531" type="appl" />
         <tli id="T44" time="37.247" type="appl" />
         <tli id="T45" time="37.964" type="appl" />
         <tli id="T46" time="38.92640475144956" />
         <tli id="T47" time="39.931" type="appl" />
         <tli id="T48" time="40.645" type="appl" />
         <tli id="T49" time="41.358" type="appl" />
         <tli id="T50" time="42.072" type="appl" />
         <tli id="T51" time="42.786" type="appl" />
         <tli id="T52" time="43.5" type="appl" />
         <tli id="T53" time="44.213" type="appl" />
         <tli id="T54" time="44.927" type="appl" />
         <tli id="T55" time="46.40635442281904" />
         <tli id="T56" time="47.39" type="appl" />
         <tli id="T57" time="48.282" type="appl" />
         <tli id="T58" time="49.174" type="appl" />
         <tli id="T59" time="50.065" type="appl" />
         <tli id="T60" time="50.957" type="appl" />
         <tli id="T61" time="52.49964675760666" />
         <tli id="T62" time="53.466" type="appl" />
         <tli id="T63" time="54.43" type="appl" />
         <tli id="T64" time="55.394" type="appl" />
         <tli id="T65" time="56.359" type="appl" />
         <tli id="T66" time="57.323" type="appl" />
         <tli id="T67" time="59.2996010043062" />
         <tli id="T68" time="60.552" type="appl" />
         <tli id="T69" time="61.637" type="appl" />
         <tli id="T70" time="67.20621447154701" />
         <tli id="T71" time="68.15" type="appl" />
         <tli id="T72" time="68.948" type="appl" />
         <tli id="T73" time="70.15952793359396" />
         <tli id="T74" time="71.031" type="appl" />
         <tli id="T75" time="71.65" type="appl" />
         <tli id="T76" time="72.24233292991593" />
         <tli id="T77" time="73.122" type="appl" />
         <tli id="T78" time="73.975" type="appl" />
         <tli id="T79" time="75.55282497828016" />
         <tli id="T80" time="76.102" type="appl" />
         <tli id="T81" time="79.23946683948098" />
         <tli id="T82" time="80.003" type="appl" />
         <tli id="T83" time="80.861" type="appl" />
         <tli id="T84" time="82.85277586076643" />
         <tli id="T85" time="83.619" type="appl" />
         <tli id="T86" time="84.408" type="appl" />
         <tli id="T87" time="86.15942027876933" />
         <tli id="T88" time="86.607" type="appl" />
         <tli id="T89" time="87.13" type="appl" />
         <tli id="T90" time="88.01940776389597" />
         <tli id="T91" time="88.713" type="appl" />
         <tli id="T92" time="89.434" type="appl" />
         <tli id="T93" time="90.154" type="appl" />
         <tli id="T94" time="90.875" type="appl" />
         <tli id="T95" time="91.596" type="appl" />
         <tli id="T96" time="92.16" type="appl" />
         <tli id="T97" time="92.723" type="appl" />
         <tli id="T98" time="93.286" type="appl" />
         <tli id="T99" time="94.97269431223677" />
         <tli id="T100" time="95.474" type="appl" />
         <tli id="T101" time="95.993" type="appl" />
         <tli id="T102" time="96.512" type="appl" />
         <tli id="T696" time="96.734" type="intp" />
         <tli id="T103" time="97.03" type="appl" />
         <tli id="T104" time="97.756" type="appl" />
         <tli id="T105" time="99.4993305215593" />
         <tli id="T106" time="100.337" type="appl" />
         <tli id="T107" time="101.3" type="appl" />
         <tli id="T108" time="102.264" type="appl" />
         <tli id="T109" time="103.227" type="appl" />
         <tli id="T110" time="105.51262339462103" />
         <tli id="T111" time="106.881" type="appl" />
         <tli id="T112" time="107.656" type="appl" />
         <tli id="T113" time="108.431" type="appl" />
         <tli id="T114" time="109.096" type="appl" />
         <tli id="T115" time="109.761" type="appl" />
         <tli id="T116" time="110.427" type="appl" />
         <tli id="T117" time="111.092" type="appl" />
         <tli id="T118" time="111.757" type="appl" />
         <tli id="T119" time="112.422" type="appl" />
         <tli id="T120" time="113.087" type="appl" />
         <tli id="T121" time="113.752" type="appl" />
         <tli id="T122" time="114.418" type="appl" />
         <tli id="T123" time="115.083" type="appl" />
         <tli id="T124" time="115.748" type="appl" />
         <tli id="T125" time="117.43254319177667" />
         <tli id="T126" time="118.412" type="appl" />
         <tli id="T127" time="119.301" type="appl" />
         <tli id="T128" time="120.191" type="appl" />
         <tli id="T129" time="121.08" type="appl" />
         <tli id="T130" time="121.97" type="appl" />
         <tli id="T131" time="122.794" type="appl" />
         <tli id="T132" time="123.618" type="appl" />
         <tli id="T133" time="124.443" type="appl" />
         <tli id="T134" time="125.267" type="appl" />
         <tli id="T135" time="126.091" type="appl" />
         <tli id="T136" time="126.916" type="appl" />
         <tli id="T137" time="127.74" type="appl" />
         <tli id="T138" time="128.564" type="appl" />
         <tli id="T139" time="129.166" type="appl" />
         <tli id="T140" time="129.616" type="appl" />
         <tli id="T141" time="130.065" type="appl" />
         <tli id="T142" time="130.515" type="appl" />
         <tli id="T143" time="131.09245128147012" />
         <tli id="T144" time="131.748" type="appl" />
         <tli id="T145" time="132.291" type="appl" />
         <tli id="T146" time="132.834" type="appl" />
         <tli id="T147" time="133.377" type="appl" />
         <tli id="T148" time="133.92" type="appl" />
         <tli id="T149" time="134.618" type="appl" />
         <tli id="T150" time="135.19" type="appl" />
         <tli id="T151" time="135.763" type="appl" />
         <tli id="T152" time="137.34574253970953" />
         <tli id="T153" time="137.957" type="appl" />
         <tli id="T154" time="138.616" type="appl" />
         <tli id="T155" time="139.276" type="appl" />
         <tli id="T156" time="139.935" type="appl" />
         <tli id="T157" time="140.594" type="appl" />
         <tli id="T158" time="141.253" type="appl" />
         <tli id="T159" time="141.911" type="appl" />
         <tli id="T160" time="142.57" type="appl" />
         <tli id="T161" time="146.64567996534268" />
         <tli id="T162" time="147.401" type="appl" />
         <tli id="T163" time="148.219" type="appl" />
         <tli id="T164" time="149.45899436936938" />
         <tli id="T165" time="150.653" type="appl" />
         <tli id="T166" time="152.269" type="appl" />
         <tli id="T167" time="154.19896247662757" />
         <tli id="T168" time="155.498" type="appl" />
         <tli id="T169" time="156.818" type="appl" />
         <tli id="T170" time="158.4122674608571" />
         <tli id="T171" time="159.89" type="appl" />
         <tli id="T172" time="161.321" type="appl" />
         <tli id="T173" time="162.752" type="appl" />
         <tli id="T174" time="164.182" type="appl" />
         <tli id="T175" time="165.613" type="appl" />
         <tli id="T176" time="167.34554068691332" />
         <tli id="T177" time="169.094" type="appl" />
         <tli id="T178" time="170.464" type="appl" />
         <tli id="T179" time="171.7796644978696" />
         <tli id="T180" time="173.514" type="appl" />
         <tli id="T181" time="175.194" type="appl" />
         <tli id="T182" time="177.5588053005836" />
         <tli id="T183" time="179.226" type="appl" />
         <tli id="T184" time="180.587" type="appl" />
         <tli id="T185" time="181.949" type="appl" />
         <tli id="T186" time="183.31" type="appl" />
         <tli id="T187" time="185.06542145919502" />
         <tli id="T188" time="186.407" type="appl" />
         <tli id="T189" time="187.773" type="appl" />
         <tli id="T190" time="189.14" type="appl" />
         <tli id="T191" time="190.506" type="appl" />
         <tli id="T192" time="193.38536547868625" />
         <tli id="T193" time="194.611" type="appl" />
         <tli id="T194" time="195.686" type="appl" />
         <tli id="T195" time="197.4853378921374" />
         <tli id="T196" time="199.778" type="appl" />
         <tli id="T197" time="201.998640857839" />
         <tli id="T198" time="204.664" type="appl" />
         <tli id="T199" time="207.79860183296506" />
         <tli id="T200" time="209.487" type="appl" />
         <tli id="T201" time="210.94" type="appl" />
         <tli id="T202" time="212.394" type="appl" />
         <tli id="T203" time="213.847" type="appl" />
         <tli id="T204" time="216.2718781540975" />
         <tli id="T205" time="217.514" type="appl" />
         <tli id="T206" time="219.586" type="appl" />
         <tli id="T207" time="221.277" type="appl" />
         <tli id="T208" time="222.893" type="appl" />
         <tli id="T209" time="225.79181409947685" />
         <tli id="T210" time="226.511" type="appl" />
         <tli id="T211" time="227.305" type="appl" />
         <tli id="T212" time="228.099" type="appl" />
         <tli id="T213" time="228.894" type="appl" />
         <tli id="T214" time="229.688" type="appl" />
         <tli id="T215" time="230.482" type="appl" />
         <tli id="T216" time="231.276" type="appl" />
         <tli id="T217" time="232.39843631367216" />
         <tli id="T218" time="233.095" type="appl" />
         <tli id="T219" time="233.695" type="appl" />
         <tli id="T220" time="234.294" type="appl" />
         <tli id="T221" time="234.894" type="appl" />
         <tli id="T222" time="235.494" type="appl" />
         <tli id="T223" time="236.094" type="appl" />
         <tli id="T224" time="236.694" type="appl" />
         <tli id="T225" time="237.293" type="appl" />
         <tli id="T226" time="237.893" type="appl" />
         <tli id="T227" time="241.83170617551517" />
         <tli id="T228" time="242.572" type="appl" />
         <tli id="T229" time="243.32" type="appl" />
         <tli id="T230" time="244.068" type="appl" />
         <tli id="T231" time="244.817" type="appl" />
         <tli id="T232" time="245.565" type="appl" />
         <tli id="T233" time="246.313" type="appl" />
         <tli id="T234" time="248.62499380040418" />
         <tli id="T235" time="249.36" type="appl" />
         <tli id="T236" time="250.134" type="appl" />
         <tli id="T237" time="250.909" type="appl" />
         <tli id="T238" time="251.684" type="appl" />
         <tli id="T239" time="252.296" type="appl" />
         <tli id="T240" time="252.894" type="appl" />
         <tli id="T241" time="253.492" type="appl" />
         <tli id="T242" time="254.09" type="appl" />
         <tli id="T243" time="254.688" type="appl" />
         <tli id="T244" time="257.15826971783105" />
         <tli id="T245" time="257.952" type="appl" />
         <tli id="T246" time="259.438" type="appl" />
         <tli id="T247" time="261.2515755094717" />
         <tli id="T248" time="262.805" type="appl" />
         <tli id="T249" time="263.795" type="appl" />
         <tli id="T250" time="264.786" type="appl" />
         <tli id="T251" time="265.776" type="appl" />
         <tli id="T252" time="266.767" type="appl" />
         <tli id="T253" time="267.625" type="appl" />
         <tli id="T254" time="268.443" type="appl" />
         <tli id="T255" time="269.26" type="appl" />
         <tli id="T256" time="270.078" type="appl" />
         <tli id="T257" time="270.895" type="appl" />
         <tli id="T258" time="271.713" type="appl" />
         <tli id="T259" time="274.09815573828547" />
         <tli id="T260" time="275.001" type="appl" />
         <tli id="T261" time="275.86" type="appl" />
         <tli id="T262" time="276.719" type="appl" />
         <tli id="T263" time="277.577" type="appl" />
         <tli id="T264" time="278.436" type="appl" />
         <tli id="T265" time="279.295" type="appl" />
         <tli id="T266" time="281.4314397298242" />
         <tli id="T267" time="281.89" type="appl" />
         <tli id="T268" time="282.383" type="appl" />
         <tli id="T269" time="282.877" type="appl" />
         <tli id="T270" time="283.37" type="appl" />
         <tli id="T271" time="283.863" type="appl" />
         <tli id="T272" time="284.356" type="appl" />
         <tli id="T273" time="284.849" type="appl" />
         <tli id="T274" time="285.343" type="appl" />
         <tli id="T275" time="285.836" type="appl" />
         <tli id="T276" time="287.77806369341045" />
         <tli id="T277" time="288.622" type="appl" />
         <tli id="T278" time="289.279" type="appl" />
         <tli id="T279" time="289.935" type="appl" />
         <tli id="T280" time="290.591" type="appl" />
         <tli id="T281" time="291.247" type="appl" />
         <tli id="T282" time="291.904" type="appl" />
         <tli id="T697" time="292.62675861150063" type="intp" />
         <tli id="T283" time="293.83135629733505" />
         <tli id="T284" time="294.635" type="appl" />
         <tli id="T285" time="295.73134351332465" />
         <tli id="T286" time="296.39" type="appl" />
         <tli id="T287" time="297.087" type="appl" />
         <tli id="T288" time="297.785" type="appl" />
         <tli id="T289" time="298.482" type="appl" />
         <tli id="T290" time="299.18" type="appl" />
         <tli id="T291" time="300.347" type="appl" />
         <tli id="T292" time="302.5912973563186" />
         <tli id="T293" time="303.509" type="appl" />
         <tli id="T294" time="304.163" type="appl" />
         <tli id="T295" time="304.817" type="appl" />
         <tli id="T296" time="305.471" type="appl" />
         <tli id="T297" time="307.44459803435507" />
         <tli id="T298" time="308.61" type="appl" />
         <tli id="T299" time="309.61" type="appl" />
         <tli id="T300" time="310.609" type="appl" />
         <tli id="T301" time="311.609" type="appl" />
         <tli id="T302" time="312.207" type="appl" />
         <tli id="T303" time="312.755" type="appl" />
         <tli id="T304" time="313.304" type="appl" />
         <tli id="T305" time="313.852" type="appl" />
         <tli id="T306" time="314.4" type="appl" />
         <tli id="T307" time="314.948" type="appl" />
         <tli id="T308" time="315.662" type="appl" />
         <tli id="T309" time="316.362" type="appl" />
         <tli id="T310" time="317.061" type="appl" />
         <tli id="T311" time="317.76" type="appl" />
         <tli id="T312" time="318.46" type="appl" />
         <tli id="T313" time="319.159" type="appl" />
         <tli id="T314" time="319.858" type="appl" />
         <tli id="T315" time="320.557" type="appl" />
         <tli id="T316" time="321.257" type="appl" />
         <tli id="T317" time="322.37116426804164" />
         <tli id="T318" time="323.618" type="appl" />
         <tli id="T319" time="324.39" type="appl" />
         <tli id="T320" time="325.162" type="appl" />
         <tli id="T321" time="325.934" type="appl" />
         <tli id="T322" time="326.706" type="appl" />
         <tli id="T323" time="328.47112322463977" />
         <tli id="T324" time="329.12" type="appl" />
         <tli id="T325" time="329.519" type="appl" />
         <tli id="T326" time="329.917" type="appl" />
         <tli id="T327" time="331.17777167969854" />
         <tli id="T328" time="331.792" type="appl" />
         <tli id="T329" time="332.498" type="appl" />
         <tli id="T330" time="333.205" type="appl" />
         <tli id="T331" time="333.912" type="appl" />
         <tli id="T332" time="334.618" type="appl" />
         <tli id="T333" time="335.325" type="appl" />
         <tli id="T334" time="336.031" type="appl" />
         <tli id="T335" time="337.81772700294636" />
         <tli id="T336" time="338.708" type="appl" />
         <tli id="T337" time="339.543" type="appl" />
         <tli id="T338" time="340.377" type="appl" />
         <tli id="T339" time="341.211" type="appl" />
         <tli id="T340" time="342.046" type="appl" />
         <tli id="T341" time="342.88" type="appl" />
         <tli id="T342" time="343.495" type="appl" />
         <tli id="T343" time="344.11" type="appl" />
         <tli id="T344" time="344.724" type="appl" />
         <tli id="T345" time="345.339" type="appl" />
         <tli id="T346" time="345.954" type="appl" />
         <tli id="T347" time="346.494" type="appl" />
         <tli id="T348" time="347.035" type="appl" />
         <tli id="T349" time="347.576" type="appl" />
         <tli id="T350" time="348.116" type="appl" />
         <tli id="T351" time="349.664" type="appl" />
         <tli id="T352" time="350.328" type="appl" />
         <tli id="T353" time="351.89763226670067" />
         <tli id="T354" time="352.69" type="appl" />
         <tli id="T355" time="353.471" type="appl" />
         <tli id="T356" time="354.252" type="appl" />
         <tli id="T357" time="355.034" type="appl" />
         <tli id="T358" time="355.563" type="appl" />
         <tli id="T359" time="356.089" type="appl" />
         <tli id="T360" time="356.615" type="appl" />
         <tli id="T361" time="357.863" type="appl" />
         <tli id="T362" time="358.344" type="appl" />
         <tli id="T363" time="358.784" type="appl" />
         <tli id="T364" time="359.223" type="appl" />
         <tli id="T365" time="359.662" type="appl" />
         <tli id="T366" time="360.102" type="appl" />
         <tli id="T367" time="362.11756350218144" />
         <tli id="T368" time="363.132" type="appl" />
         <tli id="T369" time="363.831" type="appl" />
         <tli id="T370" time="364.529" type="appl" />
         <tli id="T371" time="367.1841960781536" />
         <tli id="T372" time="368.06" type="appl" />
         <tli id="T373" time="371.7308321529077" />
         <tli id="T374" time="372.375" type="appl" />
         <tli id="T375" time="373.344" type="appl" />
         <tli id="T376" time="374.298" type="appl" />
         <tli id="T377" time="376.71079864534346" />
         <tli id="T378" time="376.807" type="appl" />
         <tli id="T379" time="377.806" type="appl" />
         <tli id="T380" time="378.806" type="appl" />
         <tli id="T381" time="383.7707511426521" />
         <tli id="T382" time="384.78" type="appl" />
         <tli id="T383" time="386.2040681034808" />
         <tli id="T384" time="386.882" type="appl" />
         <tli id="T385" time="387.572" type="appl" />
         <tli id="T386" time="388.261" type="appl" />
         <tli id="T387" time="389.2040479182012" />
         <tli id="T388" time="389.757" type="appl" />
         <tli id="T389" time="390.304" type="appl" />
         <tli id="T390" time="390.852" type="appl" />
         <tli id="T391" time="392.6240249069824" />
         <tli id="T392" time="393.331" type="appl" />
         <tli id="T393" time="393.84" type="appl" />
         <tli id="T394" time="394.348" type="appl" />
         <tli id="T395" time="394.856" type="appl" />
         <tli id="T396" time="395.365" type="appl" />
         <tli id="T397" time="395.873" type="appl" />
         <tli id="T398" time="396.517" type="appl" />
         <tli id="T399" time="397.129" type="appl" />
         <tli id="T400" time="398.9306491397057" />
         <tli id="T401" time="399.618" type="appl" />
         <tli id="T402" time="400.323" type="appl" />
         <tli id="T403" time="401.029" type="appl" />
         <tli id="T404" time="402.53729153918067" />
         <tli id="T405" time="403.105" type="appl" />
         <tli id="T406" time="403.702" type="appl" />
         <tli id="T407" time="404.3" type="appl" />
         <tli id="T408" time="405.1039409362192" />
         <tli id="T409" time="406.042" type="appl" />
         <tli id="T410" time="408.2972527834438" />
         <tli id="T411" time="408.986" type="appl" />
         <tli id="T412" time="409.549" type="appl" />
         <tli id="T413" time="410.113" type="appl" />
         <tli id="T414" time="410.676" type="appl" />
         <tli id="T415" time="411.24" type="appl" />
         <tli id="T416" time="412.441" type="appl" />
         <tli id="T417" time="414.670543234272" />
         <tli id="T418" time="415.509" type="appl" />
         <tli id="T419" time="416.141" type="appl" />
         <tli id="T420" time="416.772" type="appl" />
         <tli id="T421" time="417.404" type="appl" />
         <tli id="T422" time="423.6038164603283" />
         <tli id="T423" time="424.735" type="appl" />
         <tli id="T424" time="425.463" type="appl" />
         <tli id="T425" time="426.191" type="appl" />
         <tli id="T426" time="426.919" type="appl" />
         <tli id="T427" time="427.647" type="appl" />
         <tli id="T428" time="428.375" type="appl" />
         <tli id="T429" time="431.0237665354033" />
         <tli id="T430" time="431.933" type="appl" />
         <tli id="T431" time="432.561" type="appl" />
         <tli id="T432" time="433.188" type="appl" />
         <tli id="T433" time="433.816" type="appl" />
         <tli id="T434" time="434.443" type="appl" />
         <tli id="T435" time="435.07" type="appl" />
         <tli id="T436" time="435.698" type="appl" />
         <tli id="T437" time="436.325" type="appl" />
         <tli id="T438" time="436.845" type="appl" />
         <tli id="T439" time="437.213" type="appl" />
         <tli id="T440" time="437.581" type="appl" />
         <tli id="T441" time="437.949" type="appl" />
         <tli id="T442" time="438.316" type="appl" />
         <tli id="T443" time="438.684" type="appl" />
         <tli id="T444" time="439.052" type="appl" />
         <tli id="T445" time="439.42" type="appl" />
         <tli id="T446" time="439.788" type="appl" />
         <tli id="T447" time="441.69702805399737" />
         <tli id="T448" time="442.661" type="appl" />
         <tli id="T449" time="443.281" type="appl" />
         <tli id="T450" time="443.901" type="appl" />
         <tli id="T451" time="444.521" type="appl" />
         <tli id="T452" time="445.325" type="appl" />
         <tli id="T453" time="446.128" type="appl" />
         <tli id="T454" time="446.932" type="appl" />
         <tli id="T455" time="447.735" type="appl" />
         <tli id="T456" time="449.5369753031334" />
         <tli id="T457" time="450.448" type="appl" />
         <tli id="T458" time="451.279" type="appl" />
         <tli id="T698" time="452.0682310329197" type="intp" />
         <tli id="T459" time="453.3836160877859" />
         <tli id="T460" time="454.406" type="appl" />
         <tli id="T461" time="455.186" type="appl" />
         <tli id="T462" time="455.965" type="appl" />
         <tli id="T463" time="456.744" type="appl" />
         <tli id="T464" time="457.523" type="appl" />
         <tli id="T465" time="458.302" type="appl" />
         <tli id="T466" time="459.082" type="appl" />
         <tli id="T467" time="461.69689348546655" />
         <tli id="T468" time="462.77" type="appl" />
         <tli id="T469" time="463.761" type="appl" />
         <tli id="T470" time="464.752" type="appl" />
         <tli id="T471" time="465.743" type="appl" />
         <tli id="T472" time="466.95685809394297" />
         <tli id="T473" time="468.194" type="appl" />
         <tli id="T474" time="469.7035062798648" />
         <tli id="T475" time="470.547" type="appl" />
         <tli id="T476" time="471.201" type="appl" />
         <tli id="T477" time="471.856" type="appl" />
         <tli id="T478" time="472.51" type="appl" />
         <tli id="T479" time="473.165" type="appl" />
         <tli id="T480" time="473.82" type="appl" />
         <tli id="T481" time="474.474" type="appl" />
         <tli id="T482" time="475.9767974035356" />
         <tli id="T483" time="476.68" type="appl" />
         <tli id="T484" time="477.288" type="appl" />
         <tli id="T485" time="477.897" type="appl" />
         <tli id="T486" time="478.505" type="appl" />
         <tli id="T487" time="479.956770624398" />
         <tli id="T488" time="480.621" type="appl" />
         <tli id="T489" time="481.19" type="appl" />
         <tli id="T490" time="481.76" type="appl" />
         <tli id="T491" time="482.329" type="appl" />
         <tli id="T492" time="482.898" type="appl" />
         <tli id="T493" time="483.467" type="appl" />
         <tli id="T494" time="484.169" type="appl" />
         <tli id="T495" time="484.871" type="appl" />
         <tli id="T496" time="485.573" type="appl" />
         <tli id="T497" time="486.275" type="appl" />
         <tli id="T498" time="486.977" type="appl" />
         <tli id="T499" time="487.776" type="appl" />
         <tli id="T500" time="488.51" type="appl" />
         <tli id="T501" time="489.243" type="appl" />
         <tli id="T502" time="489.976" type="appl" />
         <tli id="T503" time="490.709" type="appl" />
         <tli id="T504" time="491.443" type="appl" />
         <tli id="T505" time="494.7900041527376" />
         <tli id="T506" time="495.598" type="appl" />
         <tli id="T507" time="496.396" type="appl" />
         <tli id="T508" time="497.195" type="appl" />
         <tli id="T509" time="499.66330469620567" />
         <tli id="T510" time="500.457" type="appl" />
         <tli id="T511" time="501.244" type="appl" />
         <tli id="T512" time="502.031" type="appl" />
         <tli id="T513" time="502.818" type="appl" />
         <tli id="T514" time="503.851" type="appl" />
         <tli id="T515" time="504.803" type="appl" />
         <tli id="T516" time="505.756" type="appl" />
         <tli id="T517" time="506.708" type="appl" />
         <tli id="T518" time="509.84990282263396" />
         <tli id="T519" time="511.021" type="appl" />
         <tli id="T520" time="511.894" type="appl" />
         <tli id="T521" time="512.768" type="appl" />
         <tli id="T522" time="513.642" type="appl" />
         <tli id="T523" time="514.516" type="appl" />
         <tli id="T524" time="515.389" type="appl" />
         <tli id="T525" time="518.3631788746293" />
         <tli id="T526" time="519.887" type="appl" />
         <tli id="T527" time="521.146" type="appl" />
         <tli id="T528" time="522.405" type="appl" />
         <tli id="T529" time="523.664" type="appl" />
         <tli id="T530" time="524.923" type="appl" />
         <tli id="T531" time="526.182" type="appl" />
         <tli id="T532" time="527.441" type="appl" />
         <tli id="T533" time="528.426" type="appl" />
         <tli id="T534" time="529.237" type="appl" />
         <tli id="T535" time="530.7230957112773" />
         <tli id="T536" time="531.38" type="appl" />
         <tli id="T537" time="531.959" type="appl" />
         <tli id="T538" time="532.537" type="appl" />
         <tli id="T539" time="533.116" type="appl" />
         <tli id="T540" time="533.694" type="appl" />
         <tli id="T541" time="534.272" type="appl" />
         <tli id="T542" time="534.851" type="appl" />
         <tli id="T543" time="535.429" type="appl" />
         <tli id="T544" time="537.1630523802104" />
         <tli id="T545" time="538.379" type="appl" />
         <tli id="T546" time="539.489" type="appl" />
         <tli id="T547" time="540.6" type="appl" />
         <tli id="T548" time="541.71" type="appl" />
         <tli id="T549" time="544.1696719030351" />
         <tli id="T550" time="544.97" type="appl" />
         <tli id="T551" time="545.714" type="appl" />
         <tli id="T552" time="546.457" type="appl" />
         <tli id="T553" time="547.201" type="appl" />
         <tli id="T554" time="548.0963121494135" />
         <tli id="T555" time="549.076" type="appl" />
         <tli id="T556" time="549.874" type="appl" />
         <tli id="T557" time="550.876" type="appl" />
         <tli id="T558" time="551.802" type="appl" />
         <tli id="T559" time="552.8162803912403" />
         <tli id="T560" time="553.89" type="appl" />
         <tli id="T561" time="554.749" type="appl" />
         <tli id="T562" time="555.608" type="appl" />
         <tli id="T563" time="556.468" type="appl" />
         <tli id="T564" time="557.534" type="appl" />
         <tli id="T565" time="558.548" type="appl" />
         <tli id="T566" time="560.0428984338112" />
         <tli id="T567" time="560.724" type="appl" />
         <tli id="T568" time="561.444" type="appl" />
         <tli id="T569" time="562.163" type="appl" />
         <tli id="T570" time="562.882" type="appl" />
         <tli id="T571" time="563.602" type="appl" />
         <tli id="T572" time="566.4428553718813" />
         <tli id="T573" time="567.27" type="appl" />
         <tli id="T574" time="568.128" type="appl" />
         <tli id="T575" time="568.986" type="appl" />
         <tli id="T576" time="569.843" type="appl" />
         <tli id="T577" time="571.02" type="appl" />
         <tli id="T578" time="572.196" type="appl" />
         <tli id="T579" time="573.373" type="appl" />
         <tli id="T580" time="574.55" type="appl" />
         <tli id="T581" time="576.065" type="appl" />
         <tli id="T582" time="577.58" type="appl" />
         <tli id="T583" time="579.095" type="appl" />
         <tli id="T584" time="580.61" type="appl" />
         <tli id="T585" time="584.2227357404574" />
         <tli id="T586" time="585.097" type="appl" />
         <tli id="T587" time="585.824" type="appl" />
         <tli id="T588" time="586.552" type="appl" />
         <tli id="T589" time="587.279" type="appl" />
         <tli id="T590" time="588.2827084230457" />
         <tli id="T591" time="589.569" type="appl" />
         <tli id="T592" time="590.613" type="appl" />
         <tli id="T593" time="591.656" type="appl" />
         <tli id="T594" time="592.7" type="appl" />
         <tli id="T595" time="593.744" type="appl" />
         <tli id="T596" time="594.788" type="appl" />
         <tli id="T597" time="595.831" type="appl" />
         <tli id="T598" time="596.875" type="appl" />
         <tli id="T599" time="597.919" type="appl" />
         <tli id="T600" time="598.963" type="appl" />
         <tli id="T601" time="600.006" type="appl" />
         <tli id="T602" time="601.05" type="appl" />
         <tli id="T603" time="605.1625948472057" />
         <tli id="T604" time="606.386" type="appl" />
         <tli id="T605" time="607.483" type="appl" />
         <tli id="T606" time="609.4425660495401" />
         <tli id="T607" time="610.206" type="appl" />
         <tli id="T608" time="610.893" type="appl" />
         <tli id="T609" time="611.579" type="appl" />
         <tli id="T610" time="612.266" type="appl" />
         <tli id="T611" time="612.952" type="appl" />
         <tli id="T612" time="613.706" type="appl" />
         <tli id="T613" time="614.446" type="appl" />
         <tli id="T614" time="615.185" type="appl" />
         <tli id="T615" time="615.925" type="appl" />
         <tli id="T616" time="616.664" type="appl" />
         <tli id="T617" time="617.404" type="appl" />
         <tli id="T618" time="618.7358368533628" />
         <tli id="T619" time="619.806" type="appl" />
         <tli id="T620" time="620.71" type="appl" />
         <tli id="T621" time="621.613" type="appl" />
         <tli id="T622" time="622.517" type="appl" />
         <tli id="T623" time="624.3491324177951" />
         <tli id="T624" time="625.443" type="appl" />
         <tli id="T625" time="626.437" type="appl" />
         <tli id="T626" time="627.9491081954595" />
         <tli id="T627" time="629.128" type="appl" />
         <tli id="T628" time="630.198" type="appl" />
         <tli id="T629" time="631.267" type="appl" />
         <tli id="T630" time="631.949" type="appl" />
         <tli id="T631" time="632.631" type="appl" />
         <tli id="T632" time="633.313" type="appl" />
         <tli id="T633" time="633.994" type="appl" />
         <tli id="T634" time="634.676" type="appl" />
         <tli id="T635" time="635.358" type="appl" />
         <tli id="T636" time="636.04" type="appl" />
         <tli id="T637" time="637.6090431988592" />
         <tli id="T638" time="638.938" type="appl" />
         <tli id="T639" time="641.909014266625" />
         <tli id="T640" time="642.714" type="appl" />
         <tli id="T641" time="643.352" type="appl" />
         <tli id="T642" time="643.99" type="appl" />
         <tli id="T643" time="644.628" type="appl" />
         <tli id="T644" time="645.266" type="appl" />
         <tli id="T645" time="645.904" type="appl" />
         <tli id="T646" time="647.0623129261337" />
         <tli id="T647" time="648.017" type="appl" />
         <tli id="T648" time="648.804" type="appl" />
         <tli id="T649" time="649.592" type="appl" />
         <tli id="T650" time="652.1422787457269" />
         <tli id="T651" time="653.197" type="appl" />
         <tli id="T652" time="653.953" type="appl" />
         <tli id="T653" time="654.709" type="appl" />
         <tli id="T654" time="655.466" type="appl" />
         <tli id="T655" time="656.222" type="appl" />
         <tli id="T656" time="656.978" type="appl" />
         <tli id="T657" time="659.1755647557935" />
         <tli id="T658" time="663.9755324593461" />
         <tli id="T659" time="664.878" type="appl" />
         <tli id="T660" time="665.461" type="appl" />
         <tli id="T661" time="666.044" type="appl" />
         <tli id="T662" time="666.626" type="appl" />
         <tli id="T663" time="667.209" type="appl" />
         <tli id="T664" time="667.791" type="appl" />
         <tli id="T665" time="668.374" type="appl" />
         <tli id="T666" time="670.3888226410373" />
         <tli id="T667" time="671.452" type="appl" />
         <tli id="T668" time="672.165" type="appl" />
         <tli id="T669" time="673.3754692121367" />
         <tli id="T670" time="674.213" type="appl" />
         <tli id="T671" time="675.0" type="appl" />
         <tli id="T672" time="675.788" type="appl" />
         <tli id="T673" time="676.575" type="appl" />
         <tli id="T674" time="679.995424669953" />
         <tli id="T675" time="681.173" type="appl" />
         <tli id="T676" time="682.326" type="appl" />
         <tli id="T677" time="683.479" type="appl" />
         <tli id="T678" time="684.632" type="appl" />
         <tli id="T679" time="685.785" type="appl" />
         <tli id="T680" time="686.938" type="appl" />
         <tli id="T681" time="688.091" type="appl" />
         <tli id="T682" time="691.648679594689" />
         <tli id="T683" time="692.763" type="appl" />
         <tli id="T684" time="693.874" type="appl" />
         <tli id="T685" time="694.986" type="appl" />
         <tli id="T686" time="696.097" type="appl" />
         <tli id="T699" time="697.2088611429825" type="intp" />
         <tli id="T687" time="699.0619630479536" />
         <tli id="T688" time="699.938" type="appl" />
         <tli id="T689" time="700.546" type="appl" />
         <tli id="T690" time="701.155" type="appl" />
         <tli id="T691" time="702.5552728766503" />
         <tli id="T692" time="703.618" type="appl" />
         <tli id="T693" time="704.438" type="appl" />
         <tli id="T694" time="705.258" type="appl" />
         <tli id="T695" time="705.955" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T26" start="T22">
            <tli id="T22.tx-KA.1" />
            <tli id="T22.tx-KA.2" />
            <tli id="T22.tx-KA.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T5" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klaudia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">üheksas</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_18" n="HIAT:u" s="T4">
                  <nts id="Seg_19" n="HIAT:ip">(</nts>
                  <nts id="Seg_20" n="HIAT:ip">(</nts>
                  <ats e="T5" id="Seg_21" n="HIAT:non-pho" s="T4">…</ats>
                  <nts id="Seg_22" n="HIAT:ip">)</nts>
                  <nts id="Seg_23" n="HIAT:ip">)</nts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_26" n="sc" s="T22">
               <ts e="T26" id="Seg_28" n="HIAT:u" s="T22">
                  <ts e="T22.tx-KA.1" id="Seg_30" n="HIAT:w" s="T22">Под</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22.tx-KA.2" id="Seg_33" n="HIAT:w" s="T22.tx-KA.1">деревом</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22.tx-KA.3" id="Seg_36" n="HIAT:w" s="T22.tx-KA.2">там</ts>
                  <nts id="Seg_37" n="HIAT:ip">,</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_39" n="HIAT:ip">(</nts>
                  <nts id="Seg_40" n="HIAT:ip">(</nts>
                  <ats e="T26" id="Seg_41" n="HIAT:non-pho" s="T22.tx-KA.3">…</ats>
                  <nts id="Seg_42" n="HIAT:ip">)</nts>
                  <nts id="Seg_43" n="HIAT:ip">)</nts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T5" id="Seg_46" n="sc" s="T0">
               <ts e="T4" id="Seg_48" n="e" s="T0">Klaudia Plotnikova, üheksas lint. </ts>
               <ts e="T5" id="Seg_50" n="e" s="T4">((…)). </ts>
            </ts>
            <ts e="T26" id="Seg_51" n="sc" s="T22">
               <ts e="T26" id="Seg_53" n="e" s="T22">Под деревом там, ((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_54" s="T0">PKZ_196X_SU0212.KA.001 (001)</ta>
            <ta e="T5" id="Seg_55" s="T4">PKZ_196X_SU0212.KA.002 (002)</ta>
            <ta e="T26" id="Seg_56" s="T22">PKZ_196X_SU0212.KA.003 (007)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_57" s="T0">Klaudia Plotnikova, üheksas lint. </ta>
            <ta e="T5" id="Seg_58" s="T4">((…)). </ta>
            <ta e="T26" id="Seg_59" s="T22">Под деревом там, ((…)). </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_60" s="T0">EST:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_61" s="T0">Клавдия Плотникова, девятая пленка.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_62" s="T0">Klavdiya Plotnikova, ninth tape.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_63" s="T0">Klavdija Plotnikova, neuntes Band.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T4" id="Seg_64" s="T0">[GVY:] The transcription on this tape is in the Ekaterinburg archive, under Box 5, Recording 9.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T34" start="T33">
            <tli id="T33.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T327" start="T323">
            <tli id="T323.tx-PKZ.1" />
            <tli id="T323.tx-PKZ.2" />
            <tli id="T323.tx-PKZ.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T22" id="Seg_65" n="sc" s="T5">
               <ts e="T12" id="Seg_67" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_69" n="HIAT:w" s="T5">Măndəraʔ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_71" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_73" n="HIAT:w" s="T6">aʔ-</ts>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_77" n="HIAT:w" s="T7">beškeʔi</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_81" n="HIAT:w" s="T8">kuiol</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_84" n="HIAT:w" s="T9">bar</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_88" n="HIAT:w" s="T10">nugaʔi</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_91" n="HIAT:w" s="T11">dĭn</ts>
                  <nts id="Seg_92" n="HIAT:ip">!</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_95" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_97" n="HIAT:w" s="T12">Dibər</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_100" n="HIAT:w" s="T13">kanaʔ</ts>
                  <nts id="Seg_101" n="HIAT:ip">!</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_104" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_106" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_109" n="HIAT:w" s="T15">măn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_112" n="HIAT:w" s="T16">döbər</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_115" n="HIAT:w" s="T17">kalam</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_119" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_121" n="HIAT:w" s="T18">Nada</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_124" n="HIAT:w" s="T19">iʔgö</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_127" n="HIAT:w" s="T20">oʔbdəsʼtə</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_130" n="HIAT:w" s="T21">beškeʔi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T694" id="Seg_133" n="sc" s="T26">
               <ts e="T31" id="Seg_135" n="HIAT:u" s="T26">
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_138" n="HIAT:w" s="T26">Pa-</ts>
                  <nts id="Seg_139" n="HIAT:ip">)</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_142" n="HIAT:w" s="T27">Paʔin</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_145" n="HIAT:w" s="T28">toːndə</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_148" n="HIAT:w" s="T29">măndəraʔ</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_152" n="HIAT:w" s="T30">jakšəŋ</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_156" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_158" n="HIAT:w" s="T31">Iʔ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_161" n="HIAT:w" s="T32">dʼabit</ts>
                  <nts id="Seg_162" n="HIAT:ip">!</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_165" n="HIAT:u" s="T33">
                  <ts e="T33.tx-PKZ.1" id="Seg_167" n="HIAT:w" s="T33">Na</ts>
                  <nts id="Seg_168" n="HIAT:ip">_</nts>
                  <ts e="T34" id="Seg_170" n="HIAT:w" s="T33.tx-PKZ.1">što</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_173" n="HIAT:w" s="T34">dʼaʔpil</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_178" n="HIAT:w" s="T35">ö-</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_182" n="HIAT:w" s="T36">öʔlit</ts>
                  <nts id="Seg_183" n="HIAT:ip">!</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_186" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_188" n="HIAT:w" s="T37">Dĭ</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_191" n="HIAT:w" s="T38">il</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_194" n="HIAT:w" s="T39">šobiʔi</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_197" n="HIAT:w" s="T40">miʔnʼibeʔ</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_200" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_202" n="HIAT:w" s="T41">bostə-</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_205" n="HIAT:w" s="T42">bos-</ts>
                  <nts id="Seg_206" n="HIAT:ip">)</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_209" n="HIAT:w" s="T43">bostə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_212" n="HIAT:w" s="T44">koŋ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_215" n="HIAT:w" s="T45">nuldlaʔbəʔjə</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_219" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_221" n="HIAT:w" s="T46">Dĭzeŋ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_224" n="HIAT:w" s="T47">tĭmneʔi</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_227" n="HIAT:w" s="T48">sazən</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_229" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_231" n="HIAT:w" s="T49">pʼ-</ts>
                  <nts id="Seg_232" n="HIAT:ip">)</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_235" n="HIAT:w" s="T50">pʼaŋzittə</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_239" n="HIAT:w" s="T51">a</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_242" n="HIAT:w" s="T52">miʔ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_245" n="HIAT:w" s="T53">ej</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_248" n="HIAT:w" s="T54">tĭmnebeʔ</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_252" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_254" n="HIAT:w" s="T55">Dĭgəttə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_257" n="HIAT:w" s="T56">dĭzeŋ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_260" n="HIAT:w" s="T57">bar</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_263" n="HIAT:w" s="T58">bostə</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_265" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_267" n="HIAT:w" s="T59">koŋzeŋdə</ts>
                  <nts id="Seg_268" n="HIAT:ip">)</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_271" n="HIAT:w" s="T60">nubiʔi</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_275" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_277" n="HIAT:w" s="T61">Tüj</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_280" n="HIAT:w" s="T62">miʔ</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_283" n="HIAT:w" s="T63">bar</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_286" n="HIAT:w" s="T64">kazak</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_289" n="HIAT:w" s="T65">šĭke</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_292" n="HIAT:w" s="T66">dʼăbaktərlaʔbəbaʔ</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_296" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_298" n="HIAT:w" s="T67">Kazan</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_301" n="HIAT:w" s="T68">šĭket</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_304" n="HIAT:w" s="T69">tüšəlleʔbəbaʔ</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_308" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_310" n="HIAT:w" s="T70">Kăde</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_313" n="HIAT:w" s="T71">tănan</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_316" n="HIAT:w" s="T72">kăštəliaʔi</ts>
                  <nts id="Seg_317" n="HIAT:ip">?</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_320" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_322" n="HIAT:w" s="T73">I</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_325" n="HIAT:w" s="T74">kăde</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_328" n="HIAT:w" s="T75">numəllieʔi</ts>
                  <nts id="Seg_329" n="HIAT:ip">?</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_332" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_334" n="HIAT:w" s="T76">Măna</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_337" n="HIAT:w" s="T77">kăštəliaʔi</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_340" n="HIAT:w" s="T78">Klawdʼa</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_344" n="HIAT:u" s="T79">
                  <ts e="T81" id="Seg_346" n="HIAT:w" s="T79">A</ts>
                  <nts id="Seg_347" n="HIAT:ip">…</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_350" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_352" n="HIAT:w" s="T81">A</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_355" n="HIAT:w" s="T82">numəjlieʔi</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_358" n="HIAT:w" s="T83">Захаровна</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_362" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_364" n="HIAT:w" s="T84">Kăde</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_367" n="HIAT:w" s="T85">šobial</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_370" n="HIAT:w" s="T86">döbər</ts>
                  <nts id="Seg_371" n="HIAT:ip">?</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_374" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_376" n="HIAT:w" s="T87">Dö</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_379" n="HIAT:w" s="T88">măn</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_382" n="HIAT:w" s="T89">koŋ</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_386" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_388" n="HIAT:w" s="T90">A</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_391" n="HIAT:w" s="T91">dö</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_394" n="HIAT:w" s="T92">dʼăbaktərzittə</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_396" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_398" n="HIAT:w" s="T93">kon-</ts>
                  <nts id="Seg_399" n="HIAT:ip">)</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_402" n="HIAT:w" s="T94">koŋ</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_406" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_408" n="HIAT:w" s="T95">A</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_411" n="HIAT:w" s="T96">măn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_414" n="HIAT:w" s="T97">dĭzeŋ</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_417" n="HIAT:w" s="T98">tüšəlliem</ts>
                  <nts id="Seg_418" n="HIAT:ip">.</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_421" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_423" n="HIAT:w" s="T99">Tăn</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_426" n="HIAT:w" s="T100">tibi</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_429" n="HIAT:w" s="T101">gibər</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_432" n="HIAT:w" s="T102">kalla</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_435" n="HIAT:w" s="T696">dʼürbi</ts>
                  <nts id="Seg_436" n="HIAT:ip">?</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_439" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_441" n="HIAT:w" s="T103">Kazan</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_444" n="HIAT:w" s="T104">turanə</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_448" n="HIAT:u" s="T105">
                  <nts id="Seg_449" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_451" n="HIAT:w" s="T105">Măn</ts>
                  <nts id="Seg_452" n="HIAT:ip">)</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_455" n="HIAT:w" s="T106">iʔgö</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_457" n="HIAT:ip">(</nts>
                  <ts e="T108" id="Seg_459" n="HIAT:w" s="T107">doʔpiel</ts>
                  <nts id="Seg_460" n="HIAT:ip">)</nts>
                  <nts id="Seg_461" n="HIAT:ip">,</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_464" n="HIAT:w" s="T108">tenəlüʔpiem</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_467" n="HIAT:w" s="T109">dʼăbaktərzittə</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_471" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_473" n="HIAT:w" s="T110">Măn</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_476" n="HIAT:w" s="T111">šoškam</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_479" n="HIAT:w" s="T112">külaːmbi</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_483" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_485" n="HIAT:w" s="T113">Măn</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_488" n="HIAT:w" s="T114">dʼü</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_491" n="HIAT:w" s="T115">tĭlbiem</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_495" n="HIAT:w" s="T116">i</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_498" n="HIAT:w" s="T117">dibər</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_500" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_502" n="HIAT:w" s="T118">dĭ-</ts>
                  <nts id="Seg_503" n="HIAT:ip">)</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_506" n="HIAT:w" s="T119">dĭm</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_509" n="HIAT:w" s="T120">embiem</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_512" n="HIAT:w" s="T121">i</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_515" n="HIAT:w" s="T122">dʼüzʼiʔ</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_518" n="HIAT:w" s="T123">kămnabiam</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_522" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_524" n="HIAT:w" s="T124">Закрыла</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_528" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_530" n="HIAT:w" s="T125">Miʔ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_533" n="HIAT:w" s="T126">bar</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_536" n="HIAT:w" s="T127">старо</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_539" n="HIAT:w" s="T128">кладбище</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_542" n="HIAT:w" s="T129">ibi</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_546" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_548" n="HIAT:w" s="T130">Dĭn</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_551" n="HIAT:w" s="T131">iʔgö</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_554" n="HIAT:w" s="T132">krospaʔi</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_556" n="HIAT:ip">(</nts>
                  <ts e="T134" id="Seg_558" n="HIAT:w" s="T133">nugaʔi-</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_561" n="HIAT:w" s="T134">nuga-</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_564" n="HIAT:w" s="T135">nugaʔi-</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_567" n="HIAT:w" s="T136">nugal-</ts>
                  <nts id="Seg_568" n="HIAT:ip">)</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_570" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_572" n="HIAT:w" s="T137">nulaʔbəʔjə</ts>
                  <nts id="Seg_573" n="HIAT:ip">)</nts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_577" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_579" n="HIAT:w" s="T138">A</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_582" n="HIAT:w" s="T139">tüj</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_585" n="HIAT:w" s="T140">onʼiʔda</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_588" n="HIAT:w" s="T142">naga</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_592" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_594" n="HIAT:w" s="T143">Ĭmbidə</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_596" n="HIAT:ip">(</nts>
                  <ts e="T145" id="Seg_598" n="HIAT:w" s="T144">tan-</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_601" n="HIAT:w" s="T145">tăn-</ts>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_605" n="HIAT:w" s="T146">dĭn</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_608" n="HIAT:w" s="T147">naga</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_612" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_614" n="HIAT:w" s="T148">Tüj</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_617" n="HIAT:w" s="T149">dĭn</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_620" n="HIAT:w" s="T150">сушилка</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_623" n="HIAT:w" s="T151">nulaʔbə</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_627" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_629" n="HIAT:w" s="T152">Tüj</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_632" n="HIAT:w" s="T153">miʔ</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_635" n="HIAT:w" s="T154">toʔbdə</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_638" n="HIAT:w" s="T155">кладбище</ts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_642" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_644" n="HIAT:w" s="T156">Dĭn</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_647" n="HIAT:w" s="T157">iʔgö</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_650" n="HIAT:w" s="T158">krospaʔi</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_653" n="HIAT:w" s="T159">nugaʔi</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_656" n="HIAT:w" s="T160">tože</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_660" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_662" n="HIAT:w" s="T161">Tăn</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_665" n="HIAT:w" s="T162">kudaj</ts>
                  <nts id="Seg_666" n="HIAT:ip">…</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_669" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_671" n="HIAT:w" s="T164">Tăn</ts>
                  <nts id="Seg_672" n="HIAT:ip">,</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_675" n="HIAT:w" s="T165">kudaj</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_679" n="HIAT:w" s="T166">kudaj</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_683" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_685" n="HIAT:w" s="T167">Iʔ</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_688" n="HIAT:w" s="T168">maʔdə</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_691" n="HIAT:w" s="T169">măna</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_695" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_697" n="HIAT:w" s="T170">Iʔ</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_700" n="HIAT:w" s="T171">maʔdə</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_703" n="HIAT:w" s="T172">măna</ts>
                  <nts id="Seg_704" n="HIAT:ip">,</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_707" n="HIAT:w" s="T173">iʔ</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_710" n="HIAT:w" s="T174">barəʔdə</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_713" n="HIAT:w" s="T175">măna</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_717" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_719" n="HIAT:w" s="T176">Tăn</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_721" n="HIAT:ip">(</nts>
                  <ts e="T178" id="Seg_723" n="HIAT:w" s="T177">uget</ts>
                  <nts id="Seg_724" n="HIAT:ip">)</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_727" n="HIAT:w" s="T178">măna</ts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_731" n="HIAT:u" s="T179">
                  <nts id="Seg_732" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_734" n="HIAT:w" s="T179">Sildə-</ts>
                  <nts id="Seg_735" n="HIAT:ip">)</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_738" n="HIAT:w" s="T180">Sĭjdə</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_741" n="HIAT:w" s="T181">sagəšsəbi</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_745" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_747" n="HIAT:w" s="T182">Sĭjdə</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_750" n="HIAT:w" s="T183">sagəšsəbi</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_754" n="HIAT:w" s="T184">tăn</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_757" n="HIAT:w" s="T185">öʔlit</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_760" n="HIAT:w" s="T186">măna</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_764" n="HIAT:u" s="T187">
                  <nts id="Seg_765" n="HIAT:ip">(</nts>
                  <ts e="T188" id="Seg_767" n="HIAT:w" s="T187">Sĭ-</ts>
                  <nts id="Seg_768" n="HIAT:ip">)</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_771" n="HIAT:w" s="T188">Sĭjbə</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_773" n="HIAT:ip">(</nts>
                  <ts e="T190" id="Seg_775" n="HIAT:w" s="T189">sagəš-</ts>
                  <nts id="Seg_776" n="HIAT:ip">)</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_779" n="HIAT:w" s="T190">sagəštə</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_782" n="HIAT:w" s="T191">ige</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_786" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_788" n="HIAT:w" s="T192">Sĭjbə</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_791" n="HIAT:w" s="T193">sagəštə</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_794" n="HIAT:w" s="T194">ige</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_798" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_800" n="HIAT:w" s="T195">Tüšəlleʔ</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_803" n="HIAT:w" s="T196">măna</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_807" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_809" n="HIAT:w" s="T197">Maktanzərzittə</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_812" n="HIAT:w" s="T198">tănzʼiʔ</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_816" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_818" n="HIAT:w" s="T199">Tüšəlleʔ</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_821" n="HIAT:w" s="T200">măna</ts>
                  <nts id="Seg_822" n="HIAT:ip">,</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_825" n="HIAT:w" s="T201">maktanərzittə</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_827" n="HIAT:ip">(</nts>
                  <ts e="T203" id="Seg_829" n="HIAT:w" s="T202">telə-</ts>
                  <nts id="Seg_830" n="HIAT:ip">)</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_833" n="HIAT:w" s="T203">tănzʼiʔ</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_837" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_839" n="HIAT:w" s="T204">Tüšəlleʔ</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_842" n="HIAT:w" s="T205">măna</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_846" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_848" n="HIAT:w" s="T206">Aktʼit</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_851" n="HIAT:w" s="T207">tăn</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_854" n="HIAT:w" s="T208">mĭnzittə</ts>
                  <nts id="Seg_855" n="HIAT:ip">.</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_858" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_860" n="HIAT:w" s="T209">Măn</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_863" n="HIAT:w" s="T210">kagam</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_865" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_867" n="HIAT:w" s="T211">bə-</ts>
                  <nts id="Seg_868" n="HIAT:ip">)</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_871" n="HIAT:w" s="T212">bar</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_874" n="HIAT:w" s="T213">bostə</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_877" n="HIAT:w" s="T214">tüšəlbi</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_880" n="HIAT:w" s="T215">pʼaŋdəsʼtə</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_883" n="HIAT:w" s="T216">dibər</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_887" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_889" n="HIAT:w" s="T217">Ildə</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_891" n="HIAT:ip">(</nts>
                  <ts e="T219" id="Seg_893" n="HIAT:w" s="T218">mănl-</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_896" n="HIAT:w" s="T219">măndəga-</ts>
                  <nts id="Seg_897" n="HIAT:ip">)</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_900" n="HIAT:w" s="T220">mĭnge</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_902" n="HIAT:ip">(</nts>
                  <ts e="T222" id="Seg_904" n="HIAT:w" s="T221">i</ts>
                  <nts id="Seg_905" n="HIAT:ip">)</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_908" n="HIAT:w" s="T222">iššo</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_911" n="HIAT:w" s="T223">куда</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_914" n="HIAT:w" s="T224">-</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_917" n="HIAT:w" s="T225">нибудь</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_920" n="HIAT:w" s="T226">kaləj</ts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_924" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_926" n="HIAT:w" s="T227">Dĭn</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_929" n="HIAT:w" s="T228">šaldə</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_932" n="HIAT:w" s="T229">mĭzittə</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_936" n="HIAT:w" s="T230">a</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_939" n="HIAT:w" s="T231">dĭn</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_942" n="HIAT:w" s="T232">šaldə</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_945" n="HIAT:w" s="T233">izittə</ts>
                  <nts id="Seg_946" n="HIAT:ip">.</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_949" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_951" n="HIAT:w" s="T234">Nada</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_954" n="HIAT:w" s="T235">măna</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_957" n="HIAT:w" s="T236">salba</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_960" n="HIAT:w" s="T237">sarzittə</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_964" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_966" n="HIAT:w" s="T238">Kola</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_968" n="HIAT:ip">(</nts>
                  <ts e="T240" id="Seg_970" n="HIAT:w" s="T239">ja-</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_973" n="HIAT:w" s="T240">jam-</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_976" n="HIAT:w" s="T241">jal-</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_979" n="HIAT:w" s="T242">jam-</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_982" n="HIAT:w" s="T243">l-</ts>
                  <nts id="Seg_983" n="HIAT:ip">)</nts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_987" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_989" n="HIAT:w" s="T244">Kola</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_991" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_993" n="HIAT:w" s="T245">dʼabəzittə</ts>
                  <nts id="Seg_994" n="HIAT:ip">)</nts>
                  <nts id="Seg_995" n="HIAT:ip">.</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_998" n="HIAT:u" s="T246">
                  <nts id="Seg_999" n="HIAT:ip">(</nts>
                  <nts id="Seg_1000" n="HIAT:ip">(</nts>
                  <ats e="T247" id="Seg_1001" n="HIAT:non-pho" s="T246">…</ats>
                  <nts id="Seg_1002" n="HIAT:ip">)</nts>
                  <nts id="Seg_1003" n="HIAT:ip">)</nts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_1007" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_1009" n="HIAT:w" s="T247">Dĭzeŋ</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1012" n="HIAT:w" s="T248">nuzaŋ</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1015" n="HIAT:w" s="T249">bar</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1018" n="HIAT:w" s="T250">saliktə</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1021" n="HIAT:w" s="T251">kandlaʔbəʔjə</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_1025" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1027" n="HIAT:w" s="T252">I</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1029" n="HIAT:ip">(</nts>
                  <ts e="T254" id="Seg_1031" n="HIAT:w" s="T253">salba</ts>
                  <nts id="Seg_1032" n="HIAT:ip">)</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1035" n="HIAT:w" s="T254">barəʔlaʔbəʔjə</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1038" n="HIAT:w" s="T255">i</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1041" n="HIAT:w" s="T256">kola</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1043" n="HIAT:ip">(</nts>
                  <ts e="T258" id="Seg_1045" n="HIAT:w" s="T257">dʼab-</ts>
                  <nts id="Seg_1046" n="HIAT:ip">)</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1049" n="HIAT:w" s="T258">dʼabəlaʔbəʔjə</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1053" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_1055" n="HIAT:w" s="T259">Piʔi</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1058" n="HIAT:w" s="T260">bar</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1061" n="HIAT:w" s="T261">šonə</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1064" n="HIAT:w" s="T262">šödörlaʔbəʔjə</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1067" n="HIAT:w" s="T263">i</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1070" n="HIAT:w" s="T264">salbanə</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1073" n="HIAT:w" s="T265">sarlaʔbəʔjə</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1077" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1079" n="HIAT:w" s="T266">Dĭ</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1082" n="HIAT:w" s="T267">kudanə</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1085" n="HIAT:w" s="T268">bar</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1088" n="HIAT:w" s="T269">ĭmbi</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1091" n="HIAT:w" s="T270">detleʔbə</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1094" n="HIAT:w" s="T271">kudaj</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1097" n="HIAT:w" s="T272">ilʼi</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1100" n="HIAT:w" s="T273">šindi</ts>
                  <nts id="Seg_1101" n="HIAT:ip">,</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1104" n="HIAT:w" s="T274">ej</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1107" n="HIAT:w" s="T275">tĭmnem</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1111" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1113" n="HIAT:w" s="T276">Sĭrenə</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1116" n="HIAT:w" s="T277">bar</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1119" n="HIAT:w" s="T278">suʔməluʔpiem</ts>
                  <nts id="Seg_1120" n="HIAT:ip">,</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1123" n="HIAT:w" s="T279">pʼel</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1125" n="HIAT:ip">(</nts>
                  <ts e="T281" id="Seg_1127" n="HIAT:w" s="T280">dak</ts>
                  <nts id="Seg_1128" n="HIAT:ip">)</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1131" n="HIAT:w" s="T281">bar</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_1134" n="HIAT:w" s="T282">kalla</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1137" n="HIAT:w" s="T697">dʼürbiem</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1141" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1143" n="HIAT:w" s="T283">Măn</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1146" n="HIAT:w" s="T284">šonəgam</ts>
                  <nts id="Seg_1147" n="HIAT:ip">.</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1150" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1152" n="HIAT:w" s="T285">Sĭre</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1155" n="HIAT:w" s="T286">ugaːndə</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1157" n="HIAT:ip">(</nts>
                  <ts e="T288" id="Seg_1159" n="HIAT:w" s="T287">i-</ts>
                  <nts id="Seg_1160" n="HIAT:ip">)</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1163" n="HIAT:w" s="T288">iʔgö</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1166" n="HIAT:w" s="T289">iʔbolaʔbə</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1170" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1172" n="HIAT:w" s="T290">Măn</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1175" n="HIAT:w" s="T291">kambiam</ts>
                  <nts id="Seg_1176" n="HIAT:ip">.</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1179" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1181" n="HIAT:w" s="T292">I</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1184" n="HIAT:w" s="T293">dĭbər</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1187" n="HIAT:w" s="T294">suʔməluʔpiem</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1190" n="HIAT:w" s="T295">bar</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1193" n="HIAT:w" s="T296">pʼeldə</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1197" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1199" n="HIAT:w" s="T297">Măn</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1202" n="HIAT:w" s="T298">болото</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1204" n="HIAT:ip">(</nts>
                  <ts e="T300" id="Seg_1206" n="HIAT:w" s="T299">sĭʔitsə</ts>
                  <nts id="Seg_1207" n="HIAT:ip">)</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1210" n="HIAT:w" s="T300">šobiam</ts>
                  <nts id="Seg_1211" n="HIAT:ip">.</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1214" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1216" n="HIAT:w" s="T301">Dĭn</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1219" n="HIAT:w" s="T302">bar</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1222" n="HIAT:w" s="T303">ugaːndə</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1225" n="HIAT:w" s="T304">balgaš</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1227" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1229" n="HIAT:w" s="T305">ig-</ts>
                  <nts id="Seg_1230" n="HIAT:ip">)</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1233" n="HIAT:w" s="T306">iʔgö</ts>
                  <nts id="Seg_1234" n="HIAT:ip">.</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1237" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1239" n="HIAT:w" s="T307">Măn</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1242" n="HIAT:w" s="T308">nubiam</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1245" n="HIAT:w" s="T309">i</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1248" n="HIAT:w" s="T310">dibər</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1251" n="HIAT:w" s="T311">saʔməluʔpiam</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1254" n="HIAT:w" s="T312">bar</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1256" n="HIAT:ip">(</nts>
                  <ts e="T314" id="Seg_1258" n="HIAT:w" s="T313">uda-</ts>
                  <nts id="Seg_1259" n="HIAT:ip">)</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1262" n="HIAT:w" s="T314">udazaŋdə</ts>
                  <nts id="Seg_1263" n="HIAT:ip">,</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1266" n="HIAT:w" s="T315">до</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1269" n="HIAT:w" s="T316">рук</ts>
                  <nts id="Seg_1270" n="HIAT:ip">.</nts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1273" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1275" n="HIAT:w" s="T317">Dĭgəttə</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1278" n="HIAT:w" s="T318">kočkam</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1281" n="HIAT:w" s="T319">udazʼiʔ</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1284" n="HIAT:w" s="T320">dʼaʔpiom</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1287" n="HIAT:w" s="T321">i</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1290" n="HIAT:w" s="T322">parbiam</ts>
                  <nts id="Seg_1291" n="HIAT:ip">.</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1294" n="HIAT:u" s="T323">
                  <ts e="T323.tx-PKZ.1" id="Seg_1296" n="HIAT:w" s="T323">Ну</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323.tx-PKZ.2" id="Seg_1299" n="HIAT:w" s="T323.tx-PKZ.1">я</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323.tx-PKZ.3" id="Seg_1302" n="HIAT:w" s="T323.tx-PKZ.2">же</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1305" n="HIAT:w" s="T323.tx-PKZ.3">говорила</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1309" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1311" n="HIAT:w" s="T327">Dĭ</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1314" n="HIAT:w" s="T328">ineʔi</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1317" n="HIAT:w" s="T329">bădəbiʔi</ts>
                  <nts id="Seg_1318" n="HIAT:ip">,</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1321" n="HIAT:w" s="T330">a</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1324" n="HIAT:w" s="T331">dö</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1327" n="HIAT:w" s="T332">ineʔi</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1330" n="HIAT:w" s="T333">ej</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1333" n="HIAT:w" s="T334">bădəbiʔi</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1337" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1339" n="HIAT:w" s="T335">Tăn</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1342" n="HIAT:w" s="T336">ularəʔi</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1345" n="HIAT:w" s="T337">mĭbiel</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1348" n="HIAT:w" s="T338">noʔ</ts>
                  <nts id="Seg_1349" n="HIAT:ip">,</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1352" n="HIAT:w" s="T339">dĭzen</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1355" n="HIAT:w" s="T340">ambiʔi</ts>
                  <nts id="Seg_1356" n="HIAT:ip">.</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1359" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1361" n="HIAT:w" s="T341">Tüžöjdə</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1364" n="HIAT:w" s="T342">mĭbiel</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1367" n="HIAT:w" s="T343">noʔ</ts>
                  <nts id="Seg_1368" n="HIAT:ip">,</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1371" n="HIAT:w" s="T344">dĭ</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1374" n="HIAT:w" s="T345">ambi</ts>
                  <nts id="Seg_1375" n="HIAT:ip">.</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1378" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1380" n="HIAT:w" s="T346">Büzəjnə</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1383" n="HIAT:w" s="T347">padʼi</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1386" n="HIAT:w" s="T348">ej</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1389" n="HIAT:w" s="T349">mĭbiel</ts>
                  <nts id="Seg_1390" n="HIAT:ip">.</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1393" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1395" n="HIAT:w" s="T350">Mĭbiem</ts>
                  <nts id="Seg_1396" n="HIAT:ip">.</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1399" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1401" n="HIAT:w" s="T351">Bar</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1404" n="HIAT:w" s="T352">ambiʔi</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1408" n="HIAT:u" s="T353">
                  <nts id="Seg_1409" n="HIAT:ip">(</nts>
                  <ts e="T354" id="Seg_1411" n="HIAT:w" s="T353">Kun</ts>
                  <nts id="Seg_1412" n="HIAT:ip">)</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1414" n="HIAT:ip">(</nts>
                  <ts e="T355" id="Seg_1416" n="HIAT:w" s="T354">maʔ-</ts>
                  <nts id="Seg_1417" n="HIAT:ip">)</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1419" n="HIAT:ip">(</nts>
                  <nts id="Seg_1420" n="HIAT:ip">(</nts>
                  <ats e="T356" id="Seg_1421" n="HIAT:non-pho" s="T355">DMG</ats>
                  <nts id="Seg_1422" n="HIAT:ip">)</nts>
                  <nts id="Seg_1423" n="HIAT:ip">)</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1426" n="HIAT:w" s="T356">eššizi</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1430" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1432" n="HIAT:w" s="T357">Bădəbiol</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1435" n="HIAT:w" s="T358">li</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1438" n="HIAT:w" s="T359">dĭm</ts>
                  <nts id="Seg_1439" n="HIAT:ip">?</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1442" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1444" n="HIAT:w" s="T360">Bădəbiom</ts>
                  <nts id="Seg_1445" n="HIAT:ip">!</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1448" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1450" n="HIAT:w" s="T361">I</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1453" n="HIAT:w" s="T362">süt</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1456" n="HIAT:w" s="T363">mĭbiem</ts>
                  <nts id="Seg_1457" n="HIAT:ip">,</nts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1460" n="HIAT:w" s="T364">i</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1463" n="HIAT:w" s="T365">bü</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1466" n="HIAT:w" s="T366">bĭʔpi</ts>
                  <nts id="Seg_1467" n="HIAT:ip">.</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1470" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1472" n="HIAT:w" s="T367">Šenap</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1474" n="HIAT:ip">(</nts>
                  <ts e="T369" id="Seg_1476" n="HIAT:w" s="T368">ambi=</ts>
                  <nts id="Seg_1477" n="HIAT:ip">)</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1480" n="HIAT:w" s="T369">ambi</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1483" n="HIAT:w" s="T370">ešši</ts>
                  <nts id="Seg_1484" n="HIAT:ip">.</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1487" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1489" n="HIAT:w" s="T371">Ej</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1492" n="HIAT:w" s="T372">amnia</ts>
                  <nts id="Seg_1493" n="HIAT:ip">.</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1496" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1498" n="HIAT:w" s="T373">Ej</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1501" n="HIAT:w" s="T374">šamnial</ts>
                  <nts id="Seg_1502" n="HIAT:ip">!</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1505" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1507" n="HIAT:w" s="T375">Šenap</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1510" n="HIAT:w" s="T376">nörbəbiel</ts>
                  <nts id="Seg_1511" n="HIAT:ip">.</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1514" n="HIAT:u" s="T377">
                  <ts e="T379" id="Seg_1516" n="HIAT:w" s="T377">Dĭzeŋ</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1519" n="HIAT:w" s="T379">paʔi</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1522" n="HIAT:w" s="T380">baltuzʼiʔ</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1526" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1528" n="HIAT:w" s="T381">Baltuzʼiʔ</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1531" n="HIAT:w" s="T382">abiʔi</ts>
                  <nts id="Seg_1532" n="HIAT:ip">.</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1535" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1537" n="HIAT:w" s="T383">I</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1540" n="HIAT:w" s="T384">dĭgəttə</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1543" n="HIAT:w" s="T385">bünə</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1546" n="HIAT:w" s="T386">kambiʔi</ts>
                  <nts id="Seg_1547" n="HIAT:ip">.</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1550" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1552" n="HIAT:w" s="T387">I</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1555" n="HIAT:w" s="T388">kola</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1558" n="HIAT:w" s="T389">dʼaʔpiʔi</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1561" n="HIAT:w" s="T390">dĭn</ts>
                  <nts id="Seg_1562" n="HIAT:ip">.</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1565" n="HIAT:u" s="T391">
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1567" n="HIAT:ip">(</nts>
                  <ts e="T393" id="Seg_1569" n="HIAT:w" s="T391">-zaŋ</ts>
                  <nts id="Seg_1570" n="HIAT:ip">)</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1573" n="HIAT:w" s="T393">bar</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1576" n="HIAT:w" s="T394">onʼiʔ</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1579" n="HIAT:w" s="T395">onʼiʔdə</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1582" n="HIAT:w" s="T396">sarbiʔi</ts>
                  <nts id="Seg_1583" n="HIAT:ip">.</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1586" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1588" n="HIAT:w" s="T397">I</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1591" n="HIAT:w" s="T398">bünə</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1594" n="HIAT:w" s="T399">kambiʔi</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1598" n="HIAT:u" s="T400">
                  <nts id="Seg_1599" n="HIAT:ip">(</nts>
                  <ts e="T401" id="Seg_1601" n="HIAT:w" s="T400">Baža-</ts>
                  <nts id="Seg_1602" n="HIAT:ip">)</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1605" n="HIAT:w" s="T401">Băzaʔ</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1608" n="HIAT:w" s="T402">kadəldə</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1611" n="HIAT:w" s="T403">büzʼiʔ</ts>
                  <nts id="Seg_1612" n="HIAT:ip">.</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1615" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1617" n="HIAT:w" s="T404">I</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1620" n="HIAT:w" s="T405">kĭškit</ts>
                  <nts id="Seg_1621" n="HIAT:ip">,</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1624" n="HIAT:w" s="T406">sĭre</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1627" n="HIAT:w" s="T407">molal</ts>
                  <nts id="Seg_1628" n="HIAT:ip">.</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1631" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1633" n="HIAT:w" s="T408">Kuvas</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1636" n="HIAT:w" s="T409">molal</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1640" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1642" n="HIAT:w" s="T410">Šiʔ</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1645" n="HIAT:w" s="T411">il</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1647" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1649" n="HIAT:w" s="T412">gije-</ts>
                  <nts id="Seg_1650" n="HIAT:ip">)</nts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1653" n="HIAT:w" s="T413">gibər</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1656" n="HIAT:w" s="T414">kandəbiʔi</ts>
                  <nts id="Seg_1657" n="HIAT:ip">?</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1660" n="HIAT:u" s="T415">
                  <nts id="Seg_1661" n="HIAT:ip">(</nts>
                  <ts e="T416" id="Seg_1663" n="HIAT:w" s="T415">Rɨbranə</ts>
                  <nts id="Seg_1664" n="HIAT:ip">)</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1667" n="HIAT:w" s="T416">kandəbiʔi</ts>
                  <nts id="Seg_1668" n="HIAT:ip">?</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1671" n="HIAT:u" s="T417">
                  <nts id="Seg_1672" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1674" n="HIAT:w" s="T417">Ul-</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1677" n="HIAT:w" s="T418">urgo-</ts>
                  <nts id="Seg_1678" n="HIAT:ip">)</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1681" n="HIAT:w" s="T419">Iʔgö</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1684" n="HIAT:w" s="T420">buragən</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1687" n="HIAT:w" s="T421">bar</ts>
                  <nts id="Seg_1688" n="HIAT:ip">.</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1691" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1693" n="HIAT:w" s="T422">Albugaʔi</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1696" n="HIAT:w" s="T423">bar</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1699" n="HIAT:w" s="T424">sarbi</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1702" n="HIAT:w" s="T425">teʔmezi</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1705" n="HIAT:w" s="T426">i</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1708" n="HIAT:w" s="T427">deʔpi</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1711" n="HIAT:w" s="T428">maʔndə</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1715" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1717" n="HIAT:w" s="T429">Dĭ</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1720" n="HIAT:w" s="T430">kuza</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1723" n="HIAT:w" s="T431">bar</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1726" n="HIAT:w" s="T432">togonorlia</ts>
                  <nts id="Seg_1727" n="HIAT:ip">,</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1730" n="HIAT:w" s="T433">togonorlia</ts>
                  <nts id="Seg_1731" n="HIAT:ip">,</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1734" n="HIAT:w" s="T434">a</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1737" n="HIAT:w" s="T435">ĭmbidə</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1740" n="HIAT:w" s="T436">naga</ts>
                  <nts id="Seg_1741" n="HIAT:ip">.</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1744" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1746" n="HIAT:w" s="T437">A</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1749" n="HIAT:w" s="T438">dö</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1752" n="HIAT:w" s="T439">kuza</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1755" n="HIAT:w" s="T440">bar</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1758" n="HIAT:w" s="T441">ej</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1761" n="HIAT:w" s="T442">tăŋ</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1764" n="HIAT:w" s="T443">togonorlia</ts>
                  <nts id="Seg_1765" n="HIAT:ip">,</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1768" n="HIAT:w" s="T444">bar</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1771" n="HIAT:w" s="T445">ĭmbi</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1774" n="HIAT:w" s="T446">ige</ts>
                  <nts id="Seg_1775" n="HIAT:ip">.</nts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1778" n="HIAT:u" s="T447">
                  <nts id="Seg_1779" n="HIAT:ip">(</nts>
                  <ts e="T448" id="Seg_1781" n="HIAT:w" s="T447">Iʔ</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1784" n="HIAT:w" s="T448">iʔbdoʔ-</ts>
                  <nts id="Seg_1785" n="HIAT:ip">)</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1788" n="HIAT:w" s="T449">Iʔ</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1791" n="HIAT:w" s="T450">iʔboʔ</ts>
                  <nts id="Seg_1792" n="HIAT:ip">.</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1795" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1797" n="HIAT:w" s="T451">Uʔbdaʔ</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1800" n="HIAT:w" s="T452">da</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1803" n="HIAT:w" s="T453">togonoraʔ</ts>
                  <nts id="Seg_1804" n="HIAT:ip">,</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1807" n="HIAT:w" s="T454">ĭmbi</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1809" n="HIAT:ip">(</nts>
                  <ts e="T456" id="Seg_1811" n="HIAT:w" s="T455">iʔbozittə</ts>
                  <nts id="Seg_1812" n="HIAT:ip">)</nts>
                  <nts id="Seg_1813" n="HIAT:ip">?</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1816" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1818" n="HIAT:w" s="T456">Iʔgö</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1821" n="HIAT:w" s="T457">pʼe</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_1824" n="HIAT:w" s="T458">kalla</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1827" n="HIAT:w" s="T698">dʼürbiʔi</ts>
                  <nts id="Seg_1828" n="HIAT:ip">.</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1831" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1833" n="HIAT:w" s="T459">Šobiʔi</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1836" n="HIAT:w" s="T460">kömə</ts>
                  <nts id="Seg_1837" n="HIAT:ip">,</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1840" n="HIAT:w" s="T461">sĭreʔi</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1843" n="HIAT:w" s="T462">šobiʔi</ts>
                  <nts id="Seg_1844" n="HIAT:ip">,</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1847" n="HIAT:w" s="T463">a</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1850" n="HIAT:w" s="T464">il</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1853" n="HIAT:w" s="T465">ugandə</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1856" n="HIAT:w" s="T466">pimnieʔi</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1860" n="HIAT:u" s="T467">
                  <nts id="Seg_1861" n="HIAT:ip">(</nts>
                  <ts e="T468" id="Seg_1863" n="HIAT:w" s="T467">Il</ts>
                  <nts id="Seg_1864" n="HIAT:ip">)</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1866" n="HIAT:ip">(</nts>
                  <ts e="T469" id="Seg_1868" n="HIAT:w" s="T468">bal-</ts>
                  <nts id="Seg_1869" n="HIAT:ip">)</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1872" n="HIAT:w" s="T469">bar</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1875" n="HIAT:w" s="T470">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1878" n="HIAT:w" s="T471">dʼijenə</ts>
                  <nts id="Seg_1879" n="HIAT:ip">.</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1882" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1884" n="HIAT:w" s="T472">Ugandə</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1887" n="HIAT:w" s="T473">pimbiʔi</ts>
                  <nts id="Seg_1888" n="HIAT:ip">.</nts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1891" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1893" n="HIAT:w" s="T474">A</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1896" n="HIAT:w" s="T475">kamən</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1899" n="HIAT:w" s="T476">šoləʔi</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1902" n="HIAT:w" s="T477">maʔnə</ts>
                  <nts id="Seg_1903" n="HIAT:ip">,</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1906" n="HIAT:w" s="T478">dĭzeŋ</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1909" n="HIAT:w" s="T479">bar</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1911" n="HIAT:ip">(</nts>
                  <ts e="T481" id="Seg_1913" n="HIAT:w" s="T480">šolə-</ts>
                  <nts id="Seg_1914" n="HIAT:ip">)</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1917" n="HIAT:w" s="T481">šobiʔi</ts>
                  <nts id="Seg_1918" n="HIAT:ip">.</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1921" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1923" n="HIAT:w" s="T482">Dĭzeŋ</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1926" n="HIAT:w" s="T483">bar</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1929" n="HIAT:w" s="T484">šobiʔi</ts>
                  <nts id="Seg_1930" n="HIAT:ip">,</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1933" n="HIAT:w" s="T485">sĭre</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1936" n="HIAT:w" s="T486">il</ts>
                  <nts id="Seg_1937" n="HIAT:ip">.</nts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1940" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1942" n="HIAT:w" s="T487">Mĭʔ</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1945" n="HIAT:w" s="T488">ilbeʔ</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1948" n="HIAT:w" s="T489">bar</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1951" n="HIAT:w" s="T490">köməʔi</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1953" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_1955" n="HIAT:w" s="T491">m-</ts>
                  <nts id="Seg_1956" n="HIAT:ip">)</nts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1959" n="HIAT:w" s="T492">ajirbiʔi</ts>
                  <nts id="Seg_1960" n="HIAT:ip">.</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1963" n="HIAT:u" s="T493">
                  <nts id="Seg_1964" n="HIAT:ip">(</nts>
                  <ts e="T494" id="Seg_1966" n="HIAT:w" s="T493">Dət-</ts>
                  <nts id="Seg_1967" n="HIAT:ip">)</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1970" n="HIAT:w" s="T494">Dĭzeŋdə</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1973" n="HIAT:w" s="T495">ipek</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1975" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_1977" n="HIAT:w" s="T496">mĭm-</ts>
                  <nts id="Seg_1978" n="HIAT:ip">)</nts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1981" n="HIAT:w" s="T497">mĭbiʔi</ts>
                  <nts id="Seg_1982" n="HIAT:ip">.</nts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1985" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1987" n="HIAT:w" s="T498">Ĭmbi</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1990" n="HIAT:w" s="T499">ige</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1993" n="HIAT:w" s="T500">bar</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1996" n="HIAT:w" s="T501">mĭbiʔi:</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1999" n="HIAT:w" s="T502">tus</ts>
                  <nts id="Seg_2000" n="HIAT:ip">,</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2003" n="HIAT:w" s="T503">aspaʔ</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2006" n="HIAT:w" s="T504">mĭbiʔi</ts>
                  <nts id="Seg_2007" n="HIAT:ip">.</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2010" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2012" n="HIAT:w" s="T505">Iššo</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2014" n="HIAT:ip">(</nts>
                  <ts e="T507" id="Seg_2016" n="HIAT:w" s="T506">toltanoʔ</ts>
                  <nts id="Seg_2017" n="HIAT:ip">)</nts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2020" n="HIAT:w" s="T507">dĭzeŋdə</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2023" n="HIAT:w" s="T508">mĭbiʔi</ts>
                  <nts id="Seg_2024" n="HIAT:ip">.</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2027" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2029" n="HIAT:w" s="T509">Köməʔi</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2032" n="HIAT:w" s="T510">bar</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2035" n="HIAT:w" s="T511">dʼijegən</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2038" n="HIAT:w" s="T512">amnobiʔi</ts>
                  <nts id="Seg_2039" n="HIAT:ip">.</nts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_2042" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_2044" n="HIAT:w" s="T513">A</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2047" n="HIAT:w" s="T514">nüdʼin</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2050" n="HIAT:w" s="T515">döbər</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2053" n="HIAT:w" s="T516">šobiʔi</ts>
                  <nts id="Seg_2054" n="HIAT:ip">,</nts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2057" n="HIAT:w" s="T517">amorzittə</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2061" n="HIAT:u" s="T518">
                  <nts id="Seg_2062" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_2064" n="HIAT:w" s="T518">O-</ts>
                  <nts id="Seg_2065" n="HIAT:ip">)</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2068" n="HIAT:w" s="T519">onʼiʔ</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2071" n="HIAT:w" s="T520">раз</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2074" n="HIAT:w" s="T521">šobiʔi</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2077" n="HIAT:w" s="T522">šide</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2080" n="HIAT:w" s="T523">отряд</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2083" n="HIAT:w" s="T524">köməʔi</ts>
                  <nts id="Seg_2084" n="HIAT:ip">.</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_2087" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2089" n="HIAT:w" s="T525">Multuksiʔ</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2092" n="HIAT:w" s="T526">один</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2095" n="HIAT:w" s="T527">onʼiʔdə</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2098" n="HIAT:w" s="T528">bar</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2101" n="HIAT:w" s="T529">чуть</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2104" n="HIAT:w" s="T530">ej</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2107" n="HIAT:w" s="T531">kuʔpiʔi</ts>
                  <nts id="Seg_2108" n="HIAT:ip">.</nts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_2111" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2113" n="HIAT:w" s="T532">Nüke</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2116" n="HIAT:w" s="T533">miʔnʼibeʔ</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2119" n="HIAT:w" s="T534">šonəga</ts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_2123" n="HIAT:u" s="T535">
                  <nts id="Seg_2124" n="HIAT:ip">"</nts>
                  <ts e="T536" id="Seg_2126" n="HIAT:w" s="T535">Šiʔ</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2129" n="HIAT:w" s="T536">bar</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2132" n="HIAT:w" s="T537">dön</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2135" n="HIAT:w" s="T538">dʼăbaktərlialaʔ</ts>
                  <nts id="Seg_2136" n="HIAT:ip">,</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2139" n="HIAT:w" s="T539">a</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2142" n="HIAT:w" s="T540">kuza</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2145" n="HIAT:w" s="T541">kuznekkən</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2148" n="HIAT:w" s="T542">nuga</ts>
                  <nts id="Seg_2149" n="HIAT:ip">.</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2152" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2154" n="HIAT:w" s="T543">Nʼilgölaʔbə</ts>
                  <nts id="Seg_2155" n="HIAT:ip">"</nts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2159" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2161" n="HIAT:w" s="T544">Miʔ</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2164" n="HIAT:w" s="T545">bar</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2166" n="HIAT:ip">(</nts>
                  <ts e="T547" id="Seg_2168" n="HIAT:w" s="T546">šut-</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2171" n="HIAT:w" s="T547">šu-</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2173" n="HIAT:ip">)</nts>
                  <nts id="Seg_2174" n="HIAT:ip">…</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2177" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2179" n="HIAT:w" s="T549">Onʼiʔ</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2181" n="HIAT:ip">(</nts>
                  <ts e="T551" id="Seg_2183" n="HIAT:w" s="T550">nüže-</ts>
                  <nts id="Seg_2184" n="HIAT:ip">)</nts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2187" n="HIAT:w" s="T551">nüdʼin</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2190" n="HIAT:w" s="T552">šobiʔi</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2193" n="HIAT:w" s="T553">köməʔi</ts>
                  <nts id="Seg_2194" n="HIAT:ip">.</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2197" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2199" n="HIAT:w" s="T554">Kuza</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2202" n="HIAT:w" s="T555">deʔpiʔi</ts>
                  <nts id="Seg_2203" n="HIAT:ip">.</nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2206" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2208" n="HIAT:w" s="T556">Ajəndə</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2211" n="HIAT:w" s="T557">tondə</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2214" n="HIAT:w" s="T558">kuʔpiʔi</ts>
                  <nts id="Seg_2215" n="HIAT:ip">.</nts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2218" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2220" n="HIAT:w" s="T559">Dĭgəttə</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2223" n="HIAT:w" s="T560">bar</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2226" n="HIAT:w" s="T561">tibizeŋ</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2229" n="HIAT:w" s="T562">oʔbdəbiʔi</ts>
                  <nts id="Seg_2230" n="HIAT:ip">.</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2233" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2235" n="HIAT:w" s="T563">Münörbiʔi</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2238" n="HIAT:w" s="T564">bar</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2241" n="HIAT:w" s="T565">plʼotkaʔizi</ts>
                  <nts id="Seg_2242" n="HIAT:ip">.</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2245" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2247" n="HIAT:w" s="T566">Onʼiʔ</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2250" n="HIAT:w" s="T567">kuza</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2253" n="HIAT:w" s="T568">bar</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2256" n="HIAT:w" s="T569">piʔməndə</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2259" n="HIAT:w" s="T570">bar</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2262" n="HIAT:w" s="T571">tüʔpi</ts>
                  <nts id="Seg_2263" n="HIAT:ip">.</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2266" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2268" n="HIAT:w" s="T572">Măn</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2271" n="HIAT:w" s="T573">abam</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2274" n="HIAT:w" s="T574">kăštəbiʔi</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2277" n="HIAT:w" s="T575">Zaxar</ts>
                  <nts id="Seg_2278" n="HIAT:ip">.</nts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2281" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2283" n="HIAT:w" s="T576">A</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2285" n="HIAT:ip">(</nts>
                  <ts e="T578" id="Seg_2287" n="HIAT:w" s="T577">nüməjləʔlʼi-</ts>
                  <nts id="Seg_2288" n="HIAT:ip">)</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2291" n="HIAT:w" s="T578">numəjluʔpiʔi</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2294" n="HIAT:w" s="T579">Stepanovič</ts>
                  <nts id="Seg_2295" n="HIAT:ip">.</nts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2298" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_2300" n="HIAT:w" s="T580">A</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2303" n="HIAT:w" s="T581">iam</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2306" n="HIAT:w" s="T582">Afanasʼa</ts>
                  <nts id="Seg_2307" n="HIAT:ip">,</nts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2310" n="HIAT:w" s="T583">numəjluʔpiʔi</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2313" n="HIAT:w" s="T584">Antonovna</ts>
                  <nts id="Seg_2314" n="HIAT:ip">.</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2317" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2319" n="HIAT:w" s="T585">A</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2322" n="HIAT:w" s="T586">familija</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2325" n="HIAT:w" s="T587">dĭzen</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2328" n="HIAT:w" s="T588">ibi</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2331" n="HIAT:w" s="T589">Andʼigatəʔi</ts>
                  <nts id="Seg_2332" n="HIAT:ip">.</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2335" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2337" n="HIAT:w" s="T590">Esseŋdə</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2340" n="HIAT:w" s="T591">ibi</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2342" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_2344" n="HIAT:w" s="T592">o-</ts>
                  <nts id="Seg_2345" n="HIAT:ip">)</nts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2348" n="HIAT:w" s="T593">oʔb</ts>
                  <nts id="Seg_2349" n="HIAT:ip">,</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2352" n="HIAT:w" s="T594">šide</ts>
                  <nts id="Seg_2353" n="HIAT:ip">,</nts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2356" n="HIAT:w" s="T595">nagur</ts>
                  <nts id="Seg_2357" n="HIAT:ip">,</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2360" n="HIAT:w" s="T596">teʔtə</ts>
                  <nts id="Seg_2361" n="HIAT:ip">,</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2363" n="HIAT:ip">(</nts>
                  <ts e="T598" id="Seg_2365" n="HIAT:w" s="T597">sumna</ts>
                  <nts id="Seg_2366" n="HIAT:ip">)</nts>
                  <nts id="Seg_2367" n="HIAT:ip">,</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2370" n="HIAT:w" s="T598">sejʔpü</ts>
                  <nts id="Seg_2371" n="HIAT:ip">,</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2374" n="HIAT:w" s="T599">šĭnteʔtə</ts>
                  <nts id="Seg_2375" n="HIAT:ip">,</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2378" n="HIAT:w" s="T600">amitun</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2381" n="HIAT:w" s="T602">восемь</ts>
                  <nts id="Seg_2382" n="HIAT:ip">.</nts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2385" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2387" n="HIAT:w" s="T603">Urgo</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2390" n="HIAT:w" s="T604">kăštəbiʔi</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2393" n="HIAT:w" s="T605">Lena</ts>
                  <nts id="Seg_2394" n="HIAT:ip">.</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2397" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2399" n="HIAT:w" s="T606">A</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2402" n="HIAT:w" s="T607">nʼibə</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2404" n="HIAT:ip">(</nts>
                  <ts e="T609" id="Seg_2406" n="HIAT:w" s="T608">ku-</ts>
                  <nts id="Seg_2407" n="HIAT:ip">)</nts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2410" n="HIAT:w" s="T609">kăštəbiʔi</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2413" n="HIAT:w" s="T610">Djoma</ts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_2417" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2419" n="HIAT:w" s="T611">Dĭgəttə</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2421" n="HIAT:ip">(</nts>
                  <ts e="T613" id="Seg_2423" n="HIAT:w" s="T612">măn=</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2426" n="HIAT:w" s="T613">măn=</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2429" n="HIAT:w" s="T614">š-</ts>
                  <nts id="Seg_2430" n="HIAT:ip">)</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2433" n="HIAT:w" s="T615">măna</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2436" n="HIAT:w" s="T616">kăštəbiʔi</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2439" n="HIAT:w" s="T617">Klawdʼeja</ts>
                  <nts id="Seg_2440" n="HIAT:ip">.</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2443" n="HIAT:u" s="T618">
                  <nts id="Seg_2444" n="HIAT:ip">(</nts>
                  <ts e="T619" id="Seg_2446" n="HIAT:w" s="T618">Măn=</ts>
                  <nts id="Seg_2447" n="HIAT:ip">)</nts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2450" n="HIAT:w" s="T619">Măn</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2453" n="HIAT:w" s="T620">sʼestram</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2456" n="HIAT:w" s="T621">kăštəbiʔi</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2459" n="HIAT:w" s="T622">Nadja</ts>
                  <nts id="Seg_2460" n="HIAT:ip">.</nts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2463" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2465" n="HIAT:w" s="T623">Dĭgəttə</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2468" n="HIAT:w" s="T624">ibi</ts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2471" n="HIAT:w" s="T625">Ăprosʼa</ts>
                  <nts id="Seg_2472" n="HIAT:ip">.</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2475" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2477" n="HIAT:w" s="T626">Dĭgəttə</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2480" n="HIAT:w" s="T627">ibi</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2483" n="HIAT:w" s="T628">Vera</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2487" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2489" n="HIAT:w" s="T629">Dĭgəttə</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2492" n="HIAT:w" s="T630">Manja</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2495" n="HIAT:w" s="T631">ibi</ts>
                  <nts id="Seg_2496" n="HIAT:ip">,</nts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2499" n="HIAT:w" s="T632">dĭgəttə</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2502" n="HIAT:w" s="T633">nʼi</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2505" n="HIAT:w" s="T634">šobi</ts>
                  <nts id="Seg_2506" n="HIAT:ip">,</nts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2508" n="HIAT:ip">(</nts>
                  <ts e="T636" id="Seg_2510" n="HIAT:w" s="T635">Ma-</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2513" n="HIAT:w" s="T636">Maksʼi-</ts>
                  <nts id="Seg_2514" n="HIAT:ip">)</nts>
                  <nts id="Seg_2515" n="HIAT:ip">.</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2518" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2520" n="HIAT:w" s="T637">Kăštəbiʔi</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2523" n="HIAT:w" s="T638">Maksim</ts>
                  <nts id="Seg_2524" n="HIAT:ip">.</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2527" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2529" n="HIAT:w" s="T639">Onʼiʔ</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2532" n="HIAT:w" s="T640">nʼi</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2535" n="HIAT:w" s="T641">külaːmbi</ts>
                  <nts id="Seg_2536" n="HIAT:ip">,</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2539" n="HIAT:w" s="T642">măna</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2542" n="HIAT:w" s="T643">iššo</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2545" n="HIAT:w" s="T644">naga</ts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2548" n="HIAT:w" s="T645">ibi</ts>
                  <nts id="Seg_2549" n="HIAT:ip">.</nts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2552" n="HIAT:u" s="T646">
                  <nts id="Seg_2553" n="HIAT:ip">(</nts>
                  <ts e="T647" id="Seg_2555" n="HIAT:w" s="T646">A=</ts>
                  <nts id="Seg_2556" n="HIAT:ip">)</nts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2559" n="HIAT:w" s="T647">A</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2562" n="HIAT:w" s="T648">koʔbdo</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2565" n="HIAT:w" s="T649">külaːmbi</ts>
                  <nts id="Seg_2566" n="HIAT:ip">.</nts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2569" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2571" n="HIAT:w" s="T650">Oʔb</ts>
                  <nts id="Seg_2572" n="HIAT:ip">,</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2575" n="HIAT:w" s="T651">šide</ts>
                  <nts id="Seg_2576" n="HIAT:ip">,</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2579" n="HIAT:w" s="T652">nagur</ts>
                  <nts id="Seg_2580" n="HIAT:ip">,</nts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2583" n="HIAT:w" s="T653">teʔtə</ts>
                  <nts id="Seg_2584" n="HIAT:ip">,</nts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2587" n="HIAT:w" s="T654">sumna</ts>
                  <nts id="Seg_2588" n="HIAT:ip">,</nts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2591" n="HIAT:w" s="T655">muktuʔ</ts>
                  <nts id="Seg_2592" n="HIAT:ip">,</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2595" n="HIAT:w" s="T656">sejʔpü</ts>
                  <nts id="Seg_2596" n="HIAT:ip">.</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2599" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2601" n="HIAT:w" s="T657">Šĭnteʔtə</ts>
                  <nts id="Seg_2602" n="HIAT:ip">.</nts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2605" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2607" n="HIAT:w" s="T658">Amitun</ts>
                  <nts id="Seg_2608" n="HIAT:ip">,</nts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2611" n="HIAT:w" s="T659">bʼeʔ</ts>
                  <nts id="Seg_2612" n="HIAT:ip">,</nts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2615" n="HIAT:w" s="T660">bʼeʔ</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2618" n="HIAT:w" s="T661">šide</ts>
                  <nts id="Seg_2619" n="HIAT:ip">,</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2622" n="HIAT:w" s="T662">bʼeʔ</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2625" n="HIAT:w" s="T663">onʼiʔ</ts>
                  <nts id="Seg_2626" n="HIAT:ip">,</nts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2629" n="HIAT:w" s="T664">bʼeʔ</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2632" n="HIAT:w" s="T665">nagur</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_2636" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_2638" n="HIAT:w" s="T666">Urgo</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2641" n="HIAT:w" s="T667">koʔbdo</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2644" n="HIAT:w" s="T668">külaːmbi</ts>
                  <nts id="Seg_2645" n="HIAT:ip">.</nts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2648" n="HIAT:u" s="T669">
                  <ts e="T670" id="Seg_2650" n="HIAT:w" s="T669">Bʼeʔ</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2653" n="HIAT:w" s="T670">nagur</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2656" n="HIAT:w" s="T671">kö</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2659" n="HIAT:w" s="T672">ibi</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2662" n="HIAT:w" s="T673">dĭʔnə</ts>
                  <nts id="Seg_2663" n="HIAT:ip">.</nts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2666" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2668" n="HIAT:w" s="T674">A</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2671" n="HIAT:w" s="T675">Ăprosʼam</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2674" n="HIAT:w" s="T676">kuʔpi</ts>
                  <nts id="Seg_2675" n="HIAT:ip">,</nts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2678" n="HIAT:w" s="T678">dĭn</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2681" n="HIAT:w" s="T679">tibi</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2684" n="HIAT:w" s="T680">kuʔpi</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2687" n="HIAT:w" s="T681">dĭm</ts>
                  <nts id="Seg_2688" n="HIAT:ip">.</nts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2691" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2693" n="HIAT:w" s="T682">Nadja</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2696" n="HIAT:w" s="T683">külaːmbi</ts>
                  <nts id="Seg_2697" n="HIAT:ip">,</nts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2700" n="HIAT:w" s="T684">sumna</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2703" n="HIAT:w" s="T685">ki</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2706" n="HIAT:w" s="T686">kalla</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2709" n="HIAT:w" s="T699">dʼürbiʔi</ts>
                  <nts id="Seg_2710" n="HIAT:ip">.</nts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2713" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2715" n="HIAT:w" s="T687">A</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2718" n="HIAT:w" s="T688">miʔ</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2721" n="HIAT:w" s="T689">šidegöʔ</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2724" n="HIAT:w" s="T690">maluʔpibaʔ</ts>
                  <nts id="Seg_2725" n="HIAT:ip">.</nts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2728" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2730" n="HIAT:w" s="T691">Klawdʼa</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2733" n="HIAT:w" s="T692">i</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2736" n="HIAT:w" s="T693">Maksim</ts>
                  <nts id="Seg_2737" n="HIAT:ip">.</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T22" id="Seg_2739" n="sc" s="T5">
               <ts e="T6" id="Seg_2741" n="e" s="T5">Măndəraʔ </ts>
               <ts e="T7" id="Seg_2743" n="e" s="T6">(aʔ-) </ts>
               <ts e="T8" id="Seg_2745" n="e" s="T7">beškeʔi, </ts>
               <ts e="T9" id="Seg_2747" n="e" s="T8">kuiol </ts>
               <ts e="T10" id="Seg_2749" n="e" s="T9">bar, </ts>
               <ts e="T11" id="Seg_2751" n="e" s="T10">nugaʔi </ts>
               <ts e="T12" id="Seg_2753" n="e" s="T11">dĭn! </ts>
               <ts e="T13" id="Seg_2755" n="e" s="T12">Dibər </ts>
               <ts e="T14" id="Seg_2757" n="e" s="T13">kanaʔ! </ts>
               <ts e="T15" id="Seg_2759" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_2761" n="e" s="T15">măn </ts>
               <ts e="T17" id="Seg_2763" n="e" s="T16">döbər </ts>
               <ts e="T18" id="Seg_2765" n="e" s="T17">kalam. </ts>
               <ts e="T19" id="Seg_2767" n="e" s="T18">Nada </ts>
               <ts e="T20" id="Seg_2769" n="e" s="T19">iʔgö </ts>
               <ts e="T21" id="Seg_2771" n="e" s="T20">oʔbdəsʼtə </ts>
               <ts e="T22" id="Seg_2773" n="e" s="T21">beškeʔi. </ts>
            </ts>
            <ts e="T694" id="Seg_2774" n="sc" s="T26">
               <ts e="T27" id="Seg_2776" n="e" s="T26">(Pa-) </ts>
               <ts e="T28" id="Seg_2778" n="e" s="T27">Paʔin </ts>
               <ts e="T29" id="Seg_2780" n="e" s="T28">toːndə </ts>
               <ts e="T30" id="Seg_2782" n="e" s="T29">măndəraʔ, </ts>
               <ts e="T31" id="Seg_2784" n="e" s="T30">jakšəŋ. </ts>
               <ts e="T32" id="Seg_2786" n="e" s="T31">Iʔ </ts>
               <ts e="T33" id="Seg_2788" n="e" s="T32">dʼabit! </ts>
               <ts e="T34" id="Seg_2790" n="e" s="T33">Na_što </ts>
               <ts e="T35" id="Seg_2792" n="e" s="T34">dʼaʔpil, </ts>
               <ts e="T36" id="Seg_2794" n="e" s="T35">(ö-) </ts>
               <ts e="T37" id="Seg_2796" n="e" s="T36">öʔlit! </ts>
               <ts e="T38" id="Seg_2798" n="e" s="T37">Dĭ </ts>
               <ts e="T39" id="Seg_2800" n="e" s="T38">il </ts>
               <ts e="T40" id="Seg_2802" n="e" s="T39">šobiʔi </ts>
               <ts e="T41" id="Seg_2804" n="e" s="T40">miʔnʼibeʔ, </ts>
               <ts e="T42" id="Seg_2806" n="e" s="T41">(bostə- </ts>
               <ts e="T43" id="Seg_2808" n="e" s="T42">bos-) </ts>
               <ts e="T44" id="Seg_2810" n="e" s="T43">bostə </ts>
               <ts e="T45" id="Seg_2812" n="e" s="T44">koŋ </ts>
               <ts e="T46" id="Seg_2814" n="e" s="T45">nuldlaʔbəʔjə. </ts>
               <ts e="T47" id="Seg_2816" n="e" s="T46">Dĭzeŋ </ts>
               <ts e="T48" id="Seg_2818" n="e" s="T47">tĭmneʔi </ts>
               <ts e="T49" id="Seg_2820" n="e" s="T48">sazən </ts>
               <ts e="T50" id="Seg_2822" n="e" s="T49">(pʼ-) </ts>
               <ts e="T51" id="Seg_2824" n="e" s="T50">pʼaŋzittə, </ts>
               <ts e="T52" id="Seg_2826" n="e" s="T51">a </ts>
               <ts e="T53" id="Seg_2828" n="e" s="T52">miʔ </ts>
               <ts e="T54" id="Seg_2830" n="e" s="T53">ej </ts>
               <ts e="T55" id="Seg_2832" n="e" s="T54">tĭmnebeʔ. </ts>
               <ts e="T56" id="Seg_2834" n="e" s="T55">Dĭgəttə </ts>
               <ts e="T57" id="Seg_2836" n="e" s="T56">dĭzeŋ </ts>
               <ts e="T58" id="Seg_2838" n="e" s="T57">bar </ts>
               <ts e="T59" id="Seg_2840" n="e" s="T58">bostə </ts>
               <ts e="T60" id="Seg_2842" n="e" s="T59">(koŋzeŋdə) </ts>
               <ts e="T61" id="Seg_2844" n="e" s="T60">nubiʔi. </ts>
               <ts e="T62" id="Seg_2846" n="e" s="T61">Tüj </ts>
               <ts e="T63" id="Seg_2848" n="e" s="T62">miʔ </ts>
               <ts e="T64" id="Seg_2850" n="e" s="T63">bar </ts>
               <ts e="T65" id="Seg_2852" n="e" s="T64">kazak </ts>
               <ts e="T66" id="Seg_2854" n="e" s="T65">šĭke </ts>
               <ts e="T67" id="Seg_2856" n="e" s="T66">dʼăbaktərlaʔbəbaʔ. </ts>
               <ts e="T68" id="Seg_2858" n="e" s="T67">Kazan </ts>
               <ts e="T69" id="Seg_2860" n="e" s="T68">šĭket </ts>
               <ts e="T70" id="Seg_2862" n="e" s="T69">tüšəlleʔbəbaʔ. </ts>
               <ts e="T71" id="Seg_2864" n="e" s="T70">Kăde </ts>
               <ts e="T72" id="Seg_2866" n="e" s="T71">tănan </ts>
               <ts e="T73" id="Seg_2868" n="e" s="T72">kăštəliaʔi? </ts>
               <ts e="T74" id="Seg_2870" n="e" s="T73">I </ts>
               <ts e="T75" id="Seg_2872" n="e" s="T74">kăde </ts>
               <ts e="T76" id="Seg_2874" n="e" s="T75">numəllieʔi? </ts>
               <ts e="T77" id="Seg_2876" n="e" s="T76">Măna </ts>
               <ts e="T78" id="Seg_2878" n="e" s="T77">kăštəliaʔi </ts>
               <ts e="T79" id="Seg_2880" n="e" s="T78">Klawdʼa. </ts>
               <ts e="T81" id="Seg_2882" n="e" s="T79">A… </ts>
               <ts e="T82" id="Seg_2884" n="e" s="T81">A </ts>
               <ts e="T83" id="Seg_2886" n="e" s="T82">numəjlieʔi </ts>
               <ts e="T84" id="Seg_2888" n="e" s="T83">Захаровна. </ts>
               <ts e="T85" id="Seg_2890" n="e" s="T84">Kăde </ts>
               <ts e="T86" id="Seg_2892" n="e" s="T85">šobial </ts>
               <ts e="T87" id="Seg_2894" n="e" s="T86">döbər? </ts>
               <ts e="T88" id="Seg_2896" n="e" s="T87">Dö </ts>
               <ts e="T89" id="Seg_2898" n="e" s="T88">măn </ts>
               <ts e="T90" id="Seg_2900" n="e" s="T89">koŋ. </ts>
               <ts e="T91" id="Seg_2902" n="e" s="T90">A </ts>
               <ts e="T92" id="Seg_2904" n="e" s="T91">dö </ts>
               <ts e="T93" id="Seg_2906" n="e" s="T92">dʼăbaktərzittə </ts>
               <ts e="T94" id="Seg_2908" n="e" s="T93">(kon-) </ts>
               <ts e="T95" id="Seg_2910" n="e" s="T94">koŋ. </ts>
               <ts e="T96" id="Seg_2912" n="e" s="T95">A </ts>
               <ts e="T97" id="Seg_2914" n="e" s="T96">măn </ts>
               <ts e="T98" id="Seg_2916" n="e" s="T97">dĭzeŋ </ts>
               <ts e="T99" id="Seg_2918" n="e" s="T98">tüšəlliem. </ts>
               <ts e="T100" id="Seg_2920" n="e" s="T99">Tăn </ts>
               <ts e="T101" id="Seg_2922" n="e" s="T100">tibi </ts>
               <ts e="T102" id="Seg_2924" n="e" s="T101">gibər </ts>
               <ts e="T696" id="Seg_2926" n="e" s="T102">kalla </ts>
               <ts e="T103" id="Seg_2928" n="e" s="T696">dʼürbi? </ts>
               <ts e="T104" id="Seg_2930" n="e" s="T103">Kazan </ts>
               <ts e="T105" id="Seg_2932" n="e" s="T104">turanə. </ts>
               <ts e="T106" id="Seg_2934" n="e" s="T105">(Măn) </ts>
               <ts e="T107" id="Seg_2936" n="e" s="T106">iʔgö </ts>
               <ts e="T108" id="Seg_2938" n="e" s="T107">(doʔpiel), </ts>
               <ts e="T109" id="Seg_2940" n="e" s="T108">tenəlüʔpiem </ts>
               <ts e="T110" id="Seg_2942" n="e" s="T109">dʼăbaktərzittə. </ts>
               <ts e="T111" id="Seg_2944" n="e" s="T110">Măn </ts>
               <ts e="T112" id="Seg_2946" n="e" s="T111">šoškam </ts>
               <ts e="T113" id="Seg_2948" n="e" s="T112">külaːmbi. </ts>
               <ts e="T114" id="Seg_2950" n="e" s="T113">Măn </ts>
               <ts e="T115" id="Seg_2952" n="e" s="T114">dʼü </ts>
               <ts e="T116" id="Seg_2954" n="e" s="T115">tĭlbiem, </ts>
               <ts e="T117" id="Seg_2956" n="e" s="T116">i </ts>
               <ts e="T118" id="Seg_2958" n="e" s="T117">dibər </ts>
               <ts e="T119" id="Seg_2960" n="e" s="T118">(dĭ-) </ts>
               <ts e="T120" id="Seg_2962" n="e" s="T119">dĭm </ts>
               <ts e="T121" id="Seg_2964" n="e" s="T120">embiem </ts>
               <ts e="T122" id="Seg_2966" n="e" s="T121">i </ts>
               <ts e="T123" id="Seg_2968" n="e" s="T122">dʼüzʼiʔ </ts>
               <ts e="T124" id="Seg_2970" n="e" s="T123">kămnabiam. </ts>
               <ts e="T125" id="Seg_2972" n="e" s="T124">Закрыла. </ts>
               <ts e="T126" id="Seg_2974" n="e" s="T125">Miʔ </ts>
               <ts e="T127" id="Seg_2976" n="e" s="T126">bar </ts>
               <ts e="T128" id="Seg_2978" n="e" s="T127">старо </ts>
               <ts e="T129" id="Seg_2980" n="e" s="T128">кладбище </ts>
               <ts e="T130" id="Seg_2982" n="e" s="T129">ibi. </ts>
               <ts e="T131" id="Seg_2984" n="e" s="T130">Dĭn </ts>
               <ts e="T132" id="Seg_2986" n="e" s="T131">iʔgö </ts>
               <ts e="T133" id="Seg_2988" n="e" s="T132">krospaʔi </ts>
               <ts e="T134" id="Seg_2990" n="e" s="T133">(nugaʔi- </ts>
               <ts e="T135" id="Seg_2992" n="e" s="T134">nuga- </ts>
               <ts e="T136" id="Seg_2994" n="e" s="T135">nugaʔi- </ts>
               <ts e="T137" id="Seg_2996" n="e" s="T136">nugal-) </ts>
               <ts e="T138" id="Seg_2998" n="e" s="T137">(nulaʔbəʔjə). </ts>
               <ts e="T139" id="Seg_3000" n="e" s="T138">A </ts>
               <ts e="T140" id="Seg_3002" n="e" s="T139">tüj </ts>
               <ts e="T142" id="Seg_3004" n="e" s="T140">onʼiʔda </ts>
               <ts e="T143" id="Seg_3006" n="e" s="T142">naga. </ts>
               <ts e="T144" id="Seg_3008" n="e" s="T143">Ĭmbidə </ts>
               <ts e="T145" id="Seg_3010" n="e" s="T144">(tan- </ts>
               <ts e="T146" id="Seg_3012" n="e" s="T145">tăn-) </ts>
               <ts e="T147" id="Seg_3014" n="e" s="T146">dĭn </ts>
               <ts e="T148" id="Seg_3016" n="e" s="T147">naga. </ts>
               <ts e="T149" id="Seg_3018" n="e" s="T148">Tüj </ts>
               <ts e="T150" id="Seg_3020" n="e" s="T149">dĭn </ts>
               <ts e="T151" id="Seg_3022" n="e" s="T150">сушилка </ts>
               <ts e="T152" id="Seg_3024" n="e" s="T151">nulaʔbə. </ts>
               <ts e="T153" id="Seg_3026" n="e" s="T152">Tüj </ts>
               <ts e="T154" id="Seg_3028" n="e" s="T153">miʔ </ts>
               <ts e="T155" id="Seg_3030" n="e" s="T154">toʔbdə </ts>
               <ts e="T156" id="Seg_3032" n="e" s="T155">кладбище. </ts>
               <ts e="T157" id="Seg_3034" n="e" s="T156">Dĭn </ts>
               <ts e="T158" id="Seg_3036" n="e" s="T157">iʔgö </ts>
               <ts e="T159" id="Seg_3038" n="e" s="T158">krospaʔi </ts>
               <ts e="T160" id="Seg_3040" n="e" s="T159">nugaʔi </ts>
               <ts e="T161" id="Seg_3042" n="e" s="T160">tože. </ts>
               <ts e="T162" id="Seg_3044" n="e" s="T161">Tăn </ts>
               <ts e="T164" id="Seg_3046" n="e" s="T162">kudaj… </ts>
               <ts e="T165" id="Seg_3048" n="e" s="T164">Tăn, </ts>
               <ts e="T166" id="Seg_3050" n="e" s="T165">kudaj, </ts>
               <ts e="T167" id="Seg_3052" n="e" s="T166">kudaj. </ts>
               <ts e="T168" id="Seg_3054" n="e" s="T167">Iʔ </ts>
               <ts e="T169" id="Seg_3056" n="e" s="T168">maʔdə </ts>
               <ts e="T170" id="Seg_3058" n="e" s="T169">măna. </ts>
               <ts e="T171" id="Seg_3060" n="e" s="T170">Iʔ </ts>
               <ts e="T172" id="Seg_3062" n="e" s="T171">maʔdə </ts>
               <ts e="T173" id="Seg_3064" n="e" s="T172">măna, </ts>
               <ts e="T174" id="Seg_3066" n="e" s="T173">iʔ </ts>
               <ts e="T175" id="Seg_3068" n="e" s="T174">barəʔdə </ts>
               <ts e="T176" id="Seg_3070" n="e" s="T175">măna. </ts>
               <ts e="T177" id="Seg_3072" n="e" s="T176">Tăn </ts>
               <ts e="T178" id="Seg_3074" n="e" s="T177">(uget) </ts>
               <ts e="T179" id="Seg_3076" n="e" s="T178">măna. </ts>
               <ts e="T180" id="Seg_3078" n="e" s="T179">(Sildə-) </ts>
               <ts e="T181" id="Seg_3080" n="e" s="T180">Sĭjdə </ts>
               <ts e="T182" id="Seg_3082" n="e" s="T181">sagəšsəbi. </ts>
               <ts e="T183" id="Seg_3084" n="e" s="T182">Sĭjdə </ts>
               <ts e="T184" id="Seg_3086" n="e" s="T183">sagəšsəbi, </ts>
               <ts e="T185" id="Seg_3088" n="e" s="T184">tăn </ts>
               <ts e="T186" id="Seg_3090" n="e" s="T185">öʔlit </ts>
               <ts e="T187" id="Seg_3092" n="e" s="T186">măna. </ts>
               <ts e="T188" id="Seg_3094" n="e" s="T187">(Sĭ-) </ts>
               <ts e="T189" id="Seg_3096" n="e" s="T188">Sĭjbə </ts>
               <ts e="T190" id="Seg_3098" n="e" s="T189">(sagəš-) </ts>
               <ts e="T191" id="Seg_3100" n="e" s="T190">sagəštə </ts>
               <ts e="T192" id="Seg_3102" n="e" s="T191">ige. </ts>
               <ts e="T193" id="Seg_3104" n="e" s="T192">Sĭjbə </ts>
               <ts e="T194" id="Seg_3106" n="e" s="T193">sagəštə </ts>
               <ts e="T195" id="Seg_3108" n="e" s="T194">ige. </ts>
               <ts e="T196" id="Seg_3110" n="e" s="T195">Tüšəlleʔ </ts>
               <ts e="T197" id="Seg_3112" n="e" s="T196">măna. </ts>
               <ts e="T198" id="Seg_3114" n="e" s="T197">Maktanzərzittə </ts>
               <ts e="T199" id="Seg_3116" n="e" s="T198">tănzʼiʔ. </ts>
               <ts e="T200" id="Seg_3118" n="e" s="T199">Tüšəlleʔ </ts>
               <ts e="T201" id="Seg_3120" n="e" s="T200">măna, </ts>
               <ts e="T202" id="Seg_3122" n="e" s="T201">maktanərzittə </ts>
               <ts e="T203" id="Seg_3124" n="e" s="T202">(telə-) </ts>
               <ts e="T204" id="Seg_3126" n="e" s="T203">tănzʼiʔ. </ts>
               <ts e="T205" id="Seg_3128" n="e" s="T204">Tüšəlleʔ </ts>
               <ts e="T206" id="Seg_3130" n="e" s="T205">măna. </ts>
               <ts e="T207" id="Seg_3132" n="e" s="T206">Aktʼit </ts>
               <ts e="T208" id="Seg_3134" n="e" s="T207">tăn </ts>
               <ts e="T209" id="Seg_3136" n="e" s="T208">mĭnzittə. </ts>
               <ts e="T210" id="Seg_3138" n="e" s="T209">Măn </ts>
               <ts e="T211" id="Seg_3140" n="e" s="T210">kagam </ts>
               <ts e="T212" id="Seg_3142" n="e" s="T211">(bə-) </ts>
               <ts e="T213" id="Seg_3144" n="e" s="T212">bar </ts>
               <ts e="T214" id="Seg_3146" n="e" s="T213">bostə </ts>
               <ts e="T215" id="Seg_3148" n="e" s="T214">tüšəlbi </ts>
               <ts e="T216" id="Seg_3150" n="e" s="T215">pʼaŋdəsʼtə </ts>
               <ts e="T217" id="Seg_3152" n="e" s="T216">dibər. </ts>
               <ts e="T218" id="Seg_3154" n="e" s="T217">Ildə </ts>
               <ts e="T219" id="Seg_3156" n="e" s="T218">(mănl- </ts>
               <ts e="T220" id="Seg_3158" n="e" s="T219">măndəga-) </ts>
               <ts e="T221" id="Seg_3160" n="e" s="T220">mĭnge </ts>
               <ts e="T222" id="Seg_3162" n="e" s="T221">(i) </ts>
               <ts e="T223" id="Seg_3164" n="e" s="T222">iššo </ts>
               <ts e="T224" id="Seg_3166" n="e" s="T223">куда </ts>
               <ts e="T225" id="Seg_3168" n="e" s="T224">- </ts>
               <ts e="T226" id="Seg_3170" n="e" s="T225">нибудь </ts>
               <ts e="T227" id="Seg_3172" n="e" s="T226">kaləj. </ts>
               <ts e="T228" id="Seg_3174" n="e" s="T227">Dĭn </ts>
               <ts e="T229" id="Seg_3176" n="e" s="T228">šaldə </ts>
               <ts e="T230" id="Seg_3178" n="e" s="T229">mĭzittə, </ts>
               <ts e="T231" id="Seg_3180" n="e" s="T230">a </ts>
               <ts e="T232" id="Seg_3182" n="e" s="T231">dĭn </ts>
               <ts e="T233" id="Seg_3184" n="e" s="T232">šaldə </ts>
               <ts e="T234" id="Seg_3186" n="e" s="T233">izittə. </ts>
               <ts e="T235" id="Seg_3188" n="e" s="T234">Nada </ts>
               <ts e="T236" id="Seg_3190" n="e" s="T235">măna </ts>
               <ts e="T237" id="Seg_3192" n="e" s="T236">salba </ts>
               <ts e="T238" id="Seg_3194" n="e" s="T237">sarzittə. </ts>
               <ts e="T239" id="Seg_3196" n="e" s="T238">Kola </ts>
               <ts e="T240" id="Seg_3198" n="e" s="T239">(ja- </ts>
               <ts e="T241" id="Seg_3200" n="e" s="T240">jam- </ts>
               <ts e="T242" id="Seg_3202" n="e" s="T241">jal- </ts>
               <ts e="T243" id="Seg_3204" n="e" s="T242">jam- </ts>
               <ts e="T244" id="Seg_3206" n="e" s="T243">l-). </ts>
               <ts e="T245" id="Seg_3208" n="e" s="T244">Kola </ts>
               <ts e="T246" id="Seg_3210" n="e" s="T245">(dʼabəzittə). </ts>
               <ts e="T247" id="Seg_3212" n="e" s="T246">((…)). </ts>
               <ts e="T248" id="Seg_3214" n="e" s="T247">Dĭzeŋ </ts>
               <ts e="T249" id="Seg_3216" n="e" s="T248">nuzaŋ </ts>
               <ts e="T250" id="Seg_3218" n="e" s="T249">bar </ts>
               <ts e="T251" id="Seg_3220" n="e" s="T250">saliktə </ts>
               <ts e="T252" id="Seg_3222" n="e" s="T251">kandlaʔbəʔjə. </ts>
               <ts e="T253" id="Seg_3224" n="e" s="T252">I </ts>
               <ts e="T254" id="Seg_3226" n="e" s="T253">(salba) </ts>
               <ts e="T255" id="Seg_3228" n="e" s="T254">barəʔlaʔbəʔjə </ts>
               <ts e="T256" id="Seg_3230" n="e" s="T255">i </ts>
               <ts e="T257" id="Seg_3232" n="e" s="T256">kola </ts>
               <ts e="T258" id="Seg_3234" n="e" s="T257">(dʼab-) </ts>
               <ts e="T259" id="Seg_3236" n="e" s="T258">dʼabəlaʔbəʔjə. </ts>
               <ts e="T260" id="Seg_3238" n="e" s="T259">Piʔi </ts>
               <ts e="T261" id="Seg_3240" n="e" s="T260">bar </ts>
               <ts e="T262" id="Seg_3242" n="e" s="T261">šonə </ts>
               <ts e="T263" id="Seg_3244" n="e" s="T262">šödörlaʔbəʔjə </ts>
               <ts e="T264" id="Seg_3246" n="e" s="T263">i </ts>
               <ts e="T265" id="Seg_3248" n="e" s="T264">salbanə </ts>
               <ts e="T266" id="Seg_3250" n="e" s="T265">sarlaʔbəʔjə. </ts>
               <ts e="T267" id="Seg_3252" n="e" s="T266">Dĭ </ts>
               <ts e="T268" id="Seg_3254" n="e" s="T267">kudanə </ts>
               <ts e="T269" id="Seg_3256" n="e" s="T268">bar </ts>
               <ts e="T270" id="Seg_3258" n="e" s="T269">ĭmbi </ts>
               <ts e="T271" id="Seg_3260" n="e" s="T270">detleʔbə </ts>
               <ts e="T272" id="Seg_3262" n="e" s="T271">kudaj </ts>
               <ts e="T273" id="Seg_3264" n="e" s="T272">ilʼi </ts>
               <ts e="T274" id="Seg_3266" n="e" s="T273">šindi, </ts>
               <ts e="T275" id="Seg_3268" n="e" s="T274">ej </ts>
               <ts e="T276" id="Seg_3270" n="e" s="T275">tĭmnem. </ts>
               <ts e="T277" id="Seg_3272" n="e" s="T276">Sĭrenə </ts>
               <ts e="T278" id="Seg_3274" n="e" s="T277">bar </ts>
               <ts e="T279" id="Seg_3276" n="e" s="T278">suʔməluʔpiem, </ts>
               <ts e="T280" id="Seg_3278" n="e" s="T279">pʼel </ts>
               <ts e="T281" id="Seg_3280" n="e" s="T280">(dak) </ts>
               <ts e="T282" id="Seg_3282" n="e" s="T281">bar </ts>
               <ts e="T697" id="Seg_3284" n="e" s="T282">kalla </ts>
               <ts e="T283" id="Seg_3286" n="e" s="T697">dʼürbiem. </ts>
               <ts e="T284" id="Seg_3288" n="e" s="T283">Măn </ts>
               <ts e="T285" id="Seg_3290" n="e" s="T284">šonəgam. </ts>
               <ts e="T286" id="Seg_3292" n="e" s="T285">Sĭre </ts>
               <ts e="T287" id="Seg_3294" n="e" s="T286">ugaːndə </ts>
               <ts e="T288" id="Seg_3296" n="e" s="T287">(i-) </ts>
               <ts e="T289" id="Seg_3298" n="e" s="T288">iʔgö </ts>
               <ts e="T290" id="Seg_3300" n="e" s="T289">iʔbolaʔbə. </ts>
               <ts e="T291" id="Seg_3302" n="e" s="T290">Măn </ts>
               <ts e="T292" id="Seg_3304" n="e" s="T291">kambiam. </ts>
               <ts e="T293" id="Seg_3306" n="e" s="T292">I </ts>
               <ts e="T294" id="Seg_3308" n="e" s="T293">dĭbər </ts>
               <ts e="T295" id="Seg_3310" n="e" s="T294">suʔməluʔpiem </ts>
               <ts e="T296" id="Seg_3312" n="e" s="T295">bar </ts>
               <ts e="T297" id="Seg_3314" n="e" s="T296">pʼeldə. </ts>
               <ts e="T298" id="Seg_3316" n="e" s="T297">Măn </ts>
               <ts e="T299" id="Seg_3318" n="e" s="T298">болото </ts>
               <ts e="T300" id="Seg_3320" n="e" s="T299">(sĭʔitsə) </ts>
               <ts e="T301" id="Seg_3322" n="e" s="T300">šobiam. </ts>
               <ts e="T302" id="Seg_3324" n="e" s="T301">Dĭn </ts>
               <ts e="T303" id="Seg_3326" n="e" s="T302">bar </ts>
               <ts e="T304" id="Seg_3328" n="e" s="T303">ugaːndə </ts>
               <ts e="T305" id="Seg_3330" n="e" s="T304">balgaš </ts>
               <ts e="T306" id="Seg_3332" n="e" s="T305">(ig-) </ts>
               <ts e="T307" id="Seg_3334" n="e" s="T306">iʔgö. </ts>
               <ts e="T308" id="Seg_3336" n="e" s="T307">Măn </ts>
               <ts e="T309" id="Seg_3338" n="e" s="T308">nubiam </ts>
               <ts e="T310" id="Seg_3340" n="e" s="T309">i </ts>
               <ts e="T311" id="Seg_3342" n="e" s="T310">dibər </ts>
               <ts e="T312" id="Seg_3344" n="e" s="T311">saʔməluʔpiam </ts>
               <ts e="T313" id="Seg_3346" n="e" s="T312">bar </ts>
               <ts e="T314" id="Seg_3348" n="e" s="T313">(uda-) </ts>
               <ts e="T315" id="Seg_3350" n="e" s="T314">udazaŋdə, </ts>
               <ts e="T316" id="Seg_3352" n="e" s="T315">до </ts>
               <ts e="T317" id="Seg_3354" n="e" s="T316">рук. </ts>
               <ts e="T318" id="Seg_3356" n="e" s="T317">Dĭgəttə </ts>
               <ts e="T319" id="Seg_3358" n="e" s="T318">kočkam </ts>
               <ts e="T320" id="Seg_3360" n="e" s="T319">udazʼiʔ </ts>
               <ts e="T321" id="Seg_3362" n="e" s="T320">dʼaʔpiom </ts>
               <ts e="T322" id="Seg_3364" n="e" s="T321">i </ts>
               <ts e="T323" id="Seg_3366" n="e" s="T322">parbiam. </ts>
               <ts e="T327" id="Seg_3368" n="e" s="T323">Ну я же говорила. </ts>
               <ts e="T328" id="Seg_3370" n="e" s="T327">Dĭ </ts>
               <ts e="T329" id="Seg_3372" n="e" s="T328">ineʔi </ts>
               <ts e="T330" id="Seg_3374" n="e" s="T329">bădəbiʔi, </ts>
               <ts e="T331" id="Seg_3376" n="e" s="T330">a </ts>
               <ts e="T332" id="Seg_3378" n="e" s="T331">dö </ts>
               <ts e="T333" id="Seg_3380" n="e" s="T332">ineʔi </ts>
               <ts e="T334" id="Seg_3382" n="e" s="T333">ej </ts>
               <ts e="T335" id="Seg_3384" n="e" s="T334">bădəbiʔi. </ts>
               <ts e="T336" id="Seg_3386" n="e" s="T335">Tăn </ts>
               <ts e="T337" id="Seg_3388" n="e" s="T336">ularəʔi </ts>
               <ts e="T338" id="Seg_3390" n="e" s="T337">mĭbiel </ts>
               <ts e="T339" id="Seg_3392" n="e" s="T338">noʔ, </ts>
               <ts e="T340" id="Seg_3394" n="e" s="T339">dĭzen </ts>
               <ts e="T341" id="Seg_3396" n="e" s="T340">ambiʔi. </ts>
               <ts e="T342" id="Seg_3398" n="e" s="T341">Tüžöjdə </ts>
               <ts e="T343" id="Seg_3400" n="e" s="T342">mĭbiel </ts>
               <ts e="T344" id="Seg_3402" n="e" s="T343">noʔ, </ts>
               <ts e="T345" id="Seg_3404" n="e" s="T344">dĭ </ts>
               <ts e="T346" id="Seg_3406" n="e" s="T345">ambi. </ts>
               <ts e="T347" id="Seg_3408" n="e" s="T346">Büzəjnə </ts>
               <ts e="T348" id="Seg_3410" n="e" s="T347">padʼi </ts>
               <ts e="T349" id="Seg_3412" n="e" s="T348">ej </ts>
               <ts e="T350" id="Seg_3414" n="e" s="T349">mĭbiel. </ts>
               <ts e="T351" id="Seg_3416" n="e" s="T350">Mĭbiem. </ts>
               <ts e="T352" id="Seg_3418" n="e" s="T351">Bar </ts>
               <ts e="T353" id="Seg_3420" n="e" s="T352">ambiʔi. </ts>
               <ts e="T354" id="Seg_3422" n="e" s="T353">(Kun) </ts>
               <ts e="T355" id="Seg_3424" n="e" s="T354">(maʔ-) </ts>
               <ts e="T356" id="Seg_3426" n="e" s="T355">((DMG)) </ts>
               <ts e="T357" id="Seg_3428" n="e" s="T356">eššizi. </ts>
               <ts e="T358" id="Seg_3430" n="e" s="T357">Bădəbiol </ts>
               <ts e="T359" id="Seg_3432" n="e" s="T358">li </ts>
               <ts e="T360" id="Seg_3434" n="e" s="T359">dĭm? </ts>
               <ts e="T361" id="Seg_3436" n="e" s="T360">Bădəbiom! </ts>
               <ts e="T362" id="Seg_3438" n="e" s="T361">I </ts>
               <ts e="T363" id="Seg_3440" n="e" s="T362">süt </ts>
               <ts e="T364" id="Seg_3442" n="e" s="T363">mĭbiem, </ts>
               <ts e="T365" id="Seg_3444" n="e" s="T364">i </ts>
               <ts e="T366" id="Seg_3446" n="e" s="T365">bü </ts>
               <ts e="T367" id="Seg_3448" n="e" s="T366">bĭʔpi. </ts>
               <ts e="T368" id="Seg_3450" n="e" s="T367">Šenap </ts>
               <ts e="T369" id="Seg_3452" n="e" s="T368">(ambi=) </ts>
               <ts e="T370" id="Seg_3454" n="e" s="T369">ambi </ts>
               <ts e="T371" id="Seg_3456" n="e" s="T370">ešši. </ts>
               <ts e="T372" id="Seg_3458" n="e" s="T371">Ej </ts>
               <ts e="T373" id="Seg_3460" n="e" s="T372">amnia. </ts>
               <ts e="T374" id="Seg_3462" n="e" s="T373">Ej </ts>
               <ts e="T375" id="Seg_3464" n="e" s="T374">šamnial! </ts>
               <ts e="T376" id="Seg_3466" n="e" s="T375">Šenap </ts>
               <ts e="T377" id="Seg_3468" n="e" s="T376">nörbəbiel. </ts>
               <ts e="T379" id="Seg_3470" n="e" s="T377">Dĭzeŋ </ts>
               <ts e="T380" id="Seg_3472" n="e" s="T379">paʔi </ts>
               <ts e="T381" id="Seg_3474" n="e" s="T380">baltuzʼiʔ. </ts>
               <ts e="T382" id="Seg_3476" n="e" s="T381">Baltuzʼiʔ </ts>
               <ts e="T383" id="Seg_3478" n="e" s="T382">abiʔi. </ts>
               <ts e="T384" id="Seg_3480" n="e" s="T383">I </ts>
               <ts e="T385" id="Seg_3482" n="e" s="T384">dĭgəttə </ts>
               <ts e="T386" id="Seg_3484" n="e" s="T385">bünə </ts>
               <ts e="T387" id="Seg_3486" n="e" s="T386">kambiʔi. </ts>
               <ts e="T388" id="Seg_3488" n="e" s="T387">I </ts>
               <ts e="T389" id="Seg_3490" n="e" s="T388">kola </ts>
               <ts e="T390" id="Seg_3492" n="e" s="T389">dʼaʔpiʔi </ts>
               <ts e="T391" id="Seg_3494" n="e" s="T390">dĭn. </ts>
               <ts e="T393" id="Seg_3496" n="e" s="T391"> (-zaŋ) </ts>
               <ts e="T394" id="Seg_3498" n="e" s="T393">bar </ts>
               <ts e="T395" id="Seg_3500" n="e" s="T394">onʼiʔ </ts>
               <ts e="T396" id="Seg_3502" n="e" s="T395">onʼiʔdə </ts>
               <ts e="T397" id="Seg_3504" n="e" s="T396">sarbiʔi. </ts>
               <ts e="T398" id="Seg_3506" n="e" s="T397">I </ts>
               <ts e="T399" id="Seg_3508" n="e" s="T398">bünə </ts>
               <ts e="T400" id="Seg_3510" n="e" s="T399">kambiʔi. </ts>
               <ts e="T401" id="Seg_3512" n="e" s="T400">(Baža-) </ts>
               <ts e="T402" id="Seg_3514" n="e" s="T401">Băzaʔ </ts>
               <ts e="T403" id="Seg_3516" n="e" s="T402">kadəldə </ts>
               <ts e="T404" id="Seg_3518" n="e" s="T403">büzʼiʔ. </ts>
               <ts e="T405" id="Seg_3520" n="e" s="T404">I </ts>
               <ts e="T406" id="Seg_3522" n="e" s="T405">kĭškit, </ts>
               <ts e="T407" id="Seg_3524" n="e" s="T406">sĭre </ts>
               <ts e="T408" id="Seg_3526" n="e" s="T407">molal. </ts>
               <ts e="T409" id="Seg_3528" n="e" s="T408">Kuvas </ts>
               <ts e="T410" id="Seg_3530" n="e" s="T409">molal. </ts>
               <ts e="T411" id="Seg_3532" n="e" s="T410">Šiʔ </ts>
               <ts e="T412" id="Seg_3534" n="e" s="T411">il </ts>
               <ts e="T413" id="Seg_3536" n="e" s="T412">(gije-) </ts>
               <ts e="T414" id="Seg_3538" n="e" s="T413">gibər </ts>
               <ts e="T415" id="Seg_3540" n="e" s="T414">kandəbiʔi? </ts>
               <ts e="T416" id="Seg_3542" n="e" s="T415">(Rɨbranə) </ts>
               <ts e="T417" id="Seg_3544" n="e" s="T416">kandəbiʔi? </ts>
               <ts e="T418" id="Seg_3546" n="e" s="T417">(Ul- </ts>
               <ts e="T419" id="Seg_3548" n="e" s="T418">urgo-) </ts>
               <ts e="T420" id="Seg_3550" n="e" s="T419">Iʔgö </ts>
               <ts e="T421" id="Seg_3552" n="e" s="T420">buragən </ts>
               <ts e="T422" id="Seg_3554" n="e" s="T421">bar. </ts>
               <ts e="T423" id="Seg_3556" n="e" s="T422">Albugaʔi </ts>
               <ts e="T424" id="Seg_3558" n="e" s="T423">bar </ts>
               <ts e="T425" id="Seg_3560" n="e" s="T424">sarbi </ts>
               <ts e="T426" id="Seg_3562" n="e" s="T425">teʔmezi </ts>
               <ts e="T427" id="Seg_3564" n="e" s="T426">i </ts>
               <ts e="T428" id="Seg_3566" n="e" s="T427">deʔpi </ts>
               <ts e="T429" id="Seg_3568" n="e" s="T428">maʔndə. </ts>
               <ts e="T430" id="Seg_3570" n="e" s="T429">Dĭ </ts>
               <ts e="T431" id="Seg_3572" n="e" s="T430">kuza </ts>
               <ts e="T432" id="Seg_3574" n="e" s="T431">bar </ts>
               <ts e="T433" id="Seg_3576" n="e" s="T432">togonorlia, </ts>
               <ts e="T434" id="Seg_3578" n="e" s="T433">togonorlia, </ts>
               <ts e="T435" id="Seg_3580" n="e" s="T434">a </ts>
               <ts e="T436" id="Seg_3582" n="e" s="T435">ĭmbidə </ts>
               <ts e="T437" id="Seg_3584" n="e" s="T436">naga. </ts>
               <ts e="T438" id="Seg_3586" n="e" s="T437">A </ts>
               <ts e="T439" id="Seg_3588" n="e" s="T438">dö </ts>
               <ts e="T440" id="Seg_3590" n="e" s="T439">kuza </ts>
               <ts e="T441" id="Seg_3592" n="e" s="T440">bar </ts>
               <ts e="T442" id="Seg_3594" n="e" s="T441">ej </ts>
               <ts e="T443" id="Seg_3596" n="e" s="T442">tăŋ </ts>
               <ts e="T444" id="Seg_3598" n="e" s="T443">togonorlia, </ts>
               <ts e="T445" id="Seg_3600" n="e" s="T444">bar </ts>
               <ts e="T446" id="Seg_3602" n="e" s="T445">ĭmbi </ts>
               <ts e="T447" id="Seg_3604" n="e" s="T446">ige. </ts>
               <ts e="T448" id="Seg_3606" n="e" s="T447">(Iʔ </ts>
               <ts e="T449" id="Seg_3608" n="e" s="T448">iʔbdoʔ-) </ts>
               <ts e="T450" id="Seg_3610" n="e" s="T449">Iʔ </ts>
               <ts e="T451" id="Seg_3612" n="e" s="T450">iʔboʔ. </ts>
               <ts e="T452" id="Seg_3614" n="e" s="T451">Uʔbdaʔ </ts>
               <ts e="T453" id="Seg_3616" n="e" s="T452">da </ts>
               <ts e="T454" id="Seg_3618" n="e" s="T453">togonoraʔ, </ts>
               <ts e="T455" id="Seg_3620" n="e" s="T454">ĭmbi </ts>
               <ts e="T456" id="Seg_3622" n="e" s="T455">(iʔbozittə)? </ts>
               <ts e="T457" id="Seg_3624" n="e" s="T456">Iʔgö </ts>
               <ts e="T458" id="Seg_3626" n="e" s="T457">pʼe </ts>
               <ts e="T698" id="Seg_3628" n="e" s="T458">kalla </ts>
               <ts e="T459" id="Seg_3630" n="e" s="T698">dʼürbiʔi. </ts>
               <ts e="T460" id="Seg_3632" n="e" s="T459">Šobiʔi </ts>
               <ts e="T461" id="Seg_3634" n="e" s="T460">kömə, </ts>
               <ts e="T462" id="Seg_3636" n="e" s="T461">sĭreʔi </ts>
               <ts e="T463" id="Seg_3638" n="e" s="T462">šobiʔi, </ts>
               <ts e="T464" id="Seg_3640" n="e" s="T463">a </ts>
               <ts e="T465" id="Seg_3642" n="e" s="T464">il </ts>
               <ts e="T466" id="Seg_3644" n="e" s="T465">ugandə </ts>
               <ts e="T467" id="Seg_3646" n="e" s="T466">pimnieʔi. </ts>
               <ts e="T468" id="Seg_3648" n="e" s="T467">(Il) </ts>
               <ts e="T469" id="Seg_3650" n="e" s="T468">(bal-) </ts>
               <ts e="T470" id="Seg_3652" n="e" s="T469">bar </ts>
               <ts e="T471" id="Seg_3654" n="e" s="T470">nuʔməluʔpiʔi </ts>
               <ts e="T472" id="Seg_3656" n="e" s="T471">dʼijenə. </ts>
               <ts e="T473" id="Seg_3658" n="e" s="T472">Ugandə </ts>
               <ts e="T474" id="Seg_3660" n="e" s="T473">pimbiʔi. </ts>
               <ts e="T475" id="Seg_3662" n="e" s="T474">A </ts>
               <ts e="T476" id="Seg_3664" n="e" s="T475">kamən </ts>
               <ts e="T477" id="Seg_3666" n="e" s="T476">šoləʔi </ts>
               <ts e="T478" id="Seg_3668" n="e" s="T477">maʔnə, </ts>
               <ts e="T479" id="Seg_3670" n="e" s="T478">dĭzeŋ </ts>
               <ts e="T480" id="Seg_3672" n="e" s="T479">bar </ts>
               <ts e="T481" id="Seg_3674" n="e" s="T480">(šolə-) </ts>
               <ts e="T482" id="Seg_3676" n="e" s="T481">šobiʔi. </ts>
               <ts e="T483" id="Seg_3678" n="e" s="T482">Dĭzeŋ </ts>
               <ts e="T484" id="Seg_3680" n="e" s="T483">bar </ts>
               <ts e="T485" id="Seg_3682" n="e" s="T484">šobiʔi, </ts>
               <ts e="T486" id="Seg_3684" n="e" s="T485">sĭre </ts>
               <ts e="T487" id="Seg_3686" n="e" s="T486">il. </ts>
               <ts e="T488" id="Seg_3688" n="e" s="T487">Mĭʔ </ts>
               <ts e="T489" id="Seg_3690" n="e" s="T488">ilbeʔ </ts>
               <ts e="T490" id="Seg_3692" n="e" s="T489">bar </ts>
               <ts e="T491" id="Seg_3694" n="e" s="T490">köməʔi </ts>
               <ts e="T492" id="Seg_3696" n="e" s="T491">(m-) </ts>
               <ts e="T493" id="Seg_3698" n="e" s="T492">ajirbiʔi. </ts>
               <ts e="T494" id="Seg_3700" n="e" s="T493">(Dət-) </ts>
               <ts e="T495" id="Seg_3702" n="e" s="T494">Dĭzeŋdə </ts>
               <ts e="T496" id="Seg_3704" n="e" s="T495">ipek </ts>
               <ts e="T497" id="Seg_3706" n="e" s="T496">(mĭm-) </ts>
               <ts e="T498" id="Seg_3708" n="e" s="T497">mĭbiʔi. </ts>
               <ts e="T499" id="Seg_3710" n="e" s="T498">Ĭmbi </ts>
               <ts e="T500" id="Seg_3712" n="e" s="T499">ige </ts>
               <ts e="T501" id="Seg_3714" n="e" s="T500">bar </ts>
               <ts e="T502" id="Seg_3716" n="e" s="T501">mĭbiʔi: </ts>
               <ts e="T503" id="Seg_3718" n="e" s="T502">tus, </ts>
               <ts e="T504" id="Seg_3720" n="e" s="T503">aspaʔ </ts>
               <ts e="T505" id="Seg_3722" n="e" s="T504">mĭbiʔi. </ts>
               <ts e="T506" id="Seg_3724" n="e" s="T505">Iššo </ts>
               <ts e="T507" id="Seg_3726" n="e" s="T506">(toltanoʔ) </ts>
               <ts e="T508" id="Seg_3728" n="e" s="T507">dĭzeŋdə </ts>
               <ts e="T509" id="Seg_3730" n="e" s="T508">mĭbiʔi. </ts>
               <ts e="T510" id="Seg_3732" n="e" s="T509">Köməʔi </ts>
               <ts e="T511" id="Seg_3734" n="e" s="T510">bar </ts>
               <ts e="T512" id="Seg_3736" n="e" s="T511">dʼijegən </ts>
               <ts e="T513" id="Seg_3738" n="e" s="T512">amnobiʔi. </ts>
               <ts e="T514" id="Seg_3740" n="e" s="T513">A </ts>
               <ts e="T515" id="Seg_3742" n="e" s="T514">nüdʼin </ts>
               <ts e="T516" id="Seg_3744" n="e" s="T515">döbər </ts>
               <ts e="T517" id="Seg_3746" n="e" s="T516">šobiʔi, </ts>
               <ts e="T518" id="Seg_3748" n="e" s="T517">amorzittə. </ts>
               <ts e="T519" id="Seg_3750" n="e" s="T518">(O-) </ts>
               <ts e="T520" id="Seg_3752" n="e" s="T519">onʼiʔ </ts>
               <ts e="T521" id="Seg_3754" n="e" s="T520">раз </ts>
               <ts e="T522" id="Seg_3756" n="e" s="T521">šobiʔi </ts>
               <ts e="T523" id="Seg_3758" n="e" s="T522">šide </ts>
               <ts e="T524" id="Seg_3760" n="e" s="T523">отряд </ts>
               <ts e="T525" id="Seg_3762" n="e" s="T524">köməʔi. </ts>
               <ts e="T526" id="Seg_3764" n="e" s="T525">Multuksiʔ </ts>
               <ts e="T527" id="Seg_3766" n="e" s="T526">один </ts>
               <ts e="T528" id="Seg_3768" n="e" s="T527">onʼiʔdə </ts>
               <ts e="T529" id="Seg_3770" n="e" s="T528">bar </ts>
               <ts e="T530" id="Seg_3772" n="e" s="T529">чуть </ts>
               <ts e="T531" id="Seg_3774" n="e" s="T530">ej </ts>
               <ts e="T532" id="Seg_3776" n="e" s="T531">kuʔpiʔi. </ts>
               <ts e="T533" id="Seg_3778" n="e" s="T532">Nüke </ts>
               <ts e="T534" id="Seg_3780" n="e" s="T533">miʔnʼibeʔ </ts>
               <ts e="T535" id="Seg_3782" n="e" s="T534">šonəga. </ts>
               <ts e="T536" id="Seg_3784" n="e" s="T535">"Šiʔ </ts>
               <ts e="T537" id="Seg_3786" n="e" s="T536">bar </ts>
               <ts e="T538" id="Seg_3788" n="e" s="T537">dön </ts>
               <ts e="T539" id="Seg_3790" n="e" s="T538">dʼăbaktərlialaʔ, </ts>
               <ts e="T540" id="Seg_3792" n="e" s="T539">a </ts>
               <ts e="T541" id="Seg_3794" n="e" s="T540">kuza </ts>
               <ts e="T542" id="Seg_3796" n="e" s="T541">kuznekkən </ts>
               <ts e="T543" id="Seg_3798" n="e" s="T542">nuga. </ts>
               <ts e="T544" id="Seg_3800" n="e" s="T543">Nʼilgölaʔbə". </ts>
               <ts e="T545" id="Seg_3802" n="e" s="T544">Miʔ </ts>
               <ts e="T546" id="Seg_3804" n="e" s="T545">bar </ts>
               <ts e="T547" id="Seg_3806" n="e" s="T546">(šut- </ts>
               <ts e="T548" id="Seg_3808" n="e" s="T547">šu- </ts>
               <ts e="T549" id="Seg_3810" n="e" s="T548">)… </ts>
               <ts e="T550" id="Seg_3812" n="e" s="T549">Onʼiʔ </ts>
               <ts e="T551" id="Seg_3814" n="e" s="T550">(nüže-) </ts>
               <ts e="T552" id="Seg_3816" n="e" s="T551">nüdʼin </ts>
               <ts e="T553" id="Seg_3818" n="e" s="T552">šobiʔi </ts>
               <ts e="T554" id="Seg_3820" n="e" s="T553">köməʔi. </ts>
               <ts e="T555" id="Seg_3822" n="e" s="T554">Kuza </ts>
               <ts e="T556" id="Seg_3824" n="e" s="T555">deʔpiʔi. </ts>
               <ts e="T557" id="Seg_3826" n="e" s="T556">Ajəndə </ts>
               <ts e="T558" id="Seg_3828" n="e" s="T557">tondə </ts>
               <ts e="T559" id="Seg_3830" n="e" s="T558">kuʔpiʔi. </ts>
               <ts e="T560" id="Seg_3832" n="e" s="T559">Dĭgəttə </ts>
               <ts e="T561" id="Seg_3834" n="e" s="T560">bar </ts>
               <ts e="T562" id="Seg_3836" n="e" s="T561">tibizeŋ </ts>
               <ts e="T563" id="Seg_3838" n="e" s="T562">oʔbdəbiʔi. </ts>
               <ts e="T564" id="Seg_3840" n="e" s="T563">Münörbiʔi </ts>
               <ts e="T565" id="Seg_3842" n="e" s="T564">bar </ts>
               <ts e="T566" id="Seg_3844" n="e" s="T565">plʼotkaʔizi. </ts>
               <ts e="T567" id="Seg_3846" n="e" s="T566">Onʼiʔ </ts>
               <ts e="T568" id="Seg_3848" n="e" s="T567">kuza </ts>
               <ts e="T569" id="Seg_3850" n="e" s="T568">bar </ts>
               <ts e="T570" id="Seg_3852" n="e" s="T569">piʔməndə </ts>
               <ts e="T571" id="Seg_3854" n="e" s="T570">bar </ts>
               <ts e="T572" id="Seg_3856" n="e" s="T571">tüʔpi. </ts>
               <ts e="T573" id="Seg_3858" n="e" s="T572">Măn </ts>
               <ts e="T574" id="Seg_3860" n="e" s="T573">abam </ts>
               <ts e="T575" id="Seg_3862" n="e" s="T574">kăštəbiʔi </ts>
               <ts e="T576" id="Seg_3864" n="e" s="T575">Zaxar. </ts>
               <ts e="T577" id="Seg_3866" n="e" s="T576">A </ts>
               <ts e="T578" id="Seg_3868" n="e" s="T577">(nüməjləʔlʼi-) </ts>
               <ts e="T579" id="Seg_3870" n="e" s="T578">numəjluʔpiʔi </ts>
               <ts e="T580" id="Seg_3872" n="e" s="T579">Stepanovič. </ts>
               <ts e="T581" id="Seg_3874" n="e" s="T580">A </ts>
               <ts e="T582" id="Seg_3876" n="e" s="T581">iam </ts>
               <ts e="T583" id="Seg_3878" n="e" s="T582">Afanasʼa, </ts>
               <ts e="T584" id="Seg_3880" n="e" s="T583">numəjluʔpiʔi </ts>
               <ts e="T585" id="Seg_3882" n="e" s="T584">Antonovna. </ts>
               <ts e="T586" id="Seg_3884" n="e" s="T585">A </ts>
               <ts e="T587" id="Seg_3886" n="e" s="T586">familija </ts>
               <ts e="T588" id="Seg_3888" n="e" s="T587">dĭzen </ts>
               <ts e="T589" id="Seg_3890" n="e" s="T588">ibi </ts>
               <ts e="T590" id="Seg_3892" n="e" s="T589">Andʼigatəʔi. </ts>
               <ts e="T591" id="Seg_3894" n="e" s="T590">Esseŋdə </ts>
               <ts e="T592" id="Seg_3896" n="e" s="T591">ibi </ts>
               <ts e="T593" id="Seg_3898" n="e" s="T592">(o-) </ts>
               <ts e="T594" id="Seg_3900" n="e" s="T593">oʔb, </ts>
               <ts e="T595" id="Seg_3902" n="e" s="T594">šide, </ts>
               <ts e="T596" id="Seg_3904" n="e" s="T595">nagur, </ts>
               <ts e="T597" id="Seg_3906" n="e" s="T596">teʔtə, </ts>
               <ts e="T598" id="Seg_3908" n="e" s="T597">(sumna), </ts>
               <ts e="T599" id="Seg_3910" n="e" s="T598">sejʔpü, </ts>
               <ts e="T600" id="Seg_3912" n="e" s="T599">šĭnteʔtə, </ts>
               <ts e="T602" id="Seg_3914" n="e" s="T600">amitun </ts>
               <ts e="T603" id="Seg_3916" n="e" s="T602">восемь. </ts>
               <ts e="T604" id="Seg_3918" n="e" s="T603">Urgo </ts>
               <ts e="T605" id="Seg_3920" n="e" s="T604">kăštəbiʔi </ts>
               <ts e="T606" id="Seg_3922" n="e" s="T605">Lena. </ts>
               <ts e="T607" id="Seg_3924" n="e" s="T606">A </ts>
               <ts e="T608" id="Seg_3926" n="e" s="T607">nʼibə </ts>
               <ts e="T609" id="Seg_3928" n="e" s="T608">(ku-) </ts>
               <ts e="T610" id="Seg_3930" n="e" s="T609">kăštəbiʔi </ts>
               <ts e="T611" id="Seg_3932" n="e" s="T610">Djoma. </ts>
               <ts e="T612" id="Seg_3934" n="e" s="T611">Dĭgəttə </ts>
               <ts e="T613" id="Seg_3936" n="e" s="T612">(măn= </ts>
               <ts e="T614" id="Seg_3938" n="e" s="T613">măn= </ts>
               <ts e="T615" id="Seg_3940" n="e" s="T614">š-) </ts>
               <ts e="T616" id="Seg_3942" n="e" s="T615">măna </ts>
               <ts e="T617" id="Seg_3944" n="e" s="T616">kăštəbiʔi </ts>
               <ts e="T618" id="Seg_3946" n="e" s="T617">Klawdʼeja. </ts>
               <ts e="T619" id="Seg_3948" n="e" s="T618">(Măn=) </ts>
               <ts e="T620" id="Seg_3950" n="e" s="T619">Măn </ts>
               <ts e="T621" id="Seg_3952" n="e" s="T620">sʼestram </ts>
               <ts e="T622" id="Seg_3954" n="e" s="T621">kăštəbiʔi </ts>
               <ts e="T623" id="Seg_3956" n="e" s="T622">Nadja. </ts>
               <ts e="T624" id="Seg_3958" n="e" s="T623">Dĭgəttə </ts>
               <ts e="T625" id="Seg_3960" n="e" s="T624">ibi </ts>
               <ts e="T626" id="Seg_3962" n="e" s="T625">Ăprosʼa. </ts>
               <ts e="T627" id="Seg_3964" n="e" s="T626">Dĭgəttə </ts>
               <ts e="T628" id="Seg_3966" n="e" s="T627">ibi </ts>
               <ts e="T629" id="Seg_3968" n="e" s="T628">Vera. </ts>
               <ts e="T630" id="Seg_3970" n="e" s="T629">Dĭgəttə </ts>
               <ts e="T631" id="Seg_3972" n="e" s="T630">Manja </ts>
               <ts e="T632" id="Seg_3974" n="e" s="T631">ibi, </ts>
               <ts e="T633" id="Seg_3976" n="e" s="T632">dĭgəttə </ts>
               <ts e="T634" id="Seg_3978" n="e" s="T633">nʼi </ts>
               <ts e="T635" id="Seg_3980" n="e" s="T634">šobi, </ts>
               <ts e="T636" id="Seg_3982" n="e" s="T635">(Ma- </ts>
               <ts e="T637" id="Seg_3984" n="e" s="T636">Maksʼi-). </ts>
               <ts e="T638" id="Seg_3986" n="e" s="T637">Kăštəbiʔi </ts>
               <ts e="T639" id="Seg_3988" n="e" s="T638">Maksim. </ts>
               <ts e="T640" id="Seg_3990" n="e" s="T639">Onʼiʔ </ts>
               <ts e="T641" id="Seg_3992" n="e" s="T640">nʼi </ts>
               <ts e="T642" id="Seg_3994" n="e" s="T641">külaːmbi, </ts>
               <ts e="T643" id="Seg_3996" n="e" s="T642">măna </ts>
               <ts e="T644" id="Seg_3998" n="e" s="T643">iššo </ts>
               <ts e="T645" id="Seg_4000" n="e" s="T644">naga </ts>
               <ts e="T646" id="Seg_4002" n="e" s="T645">ibi. </ts>
               <ts e="T647" id="Seg_4004" n="e" s="T646">(A=) </ts>
               <ts e="T648" id="Seg_4006" n="e" s="T647">A </ts>
               <ts e="T649" id="Seg_4008" n="e" s="T648">koʔbdo </ts>
               <ts e="T650" id="Seg_4010" n="e" s="T649">külaːmbi. </ts>
               <ts e="T651" id="Seg_4012" n="e" s="T650">Oʔb, </ts>
               <ts e="T652" id="Seg_4014" n="e" s="T651">šide, </ts>
               <ts e="T653" id="Seg_4016" n="e" s="T652">nagur, </ts>
               <ts e="T654" id="Seg_4018" n="e" s="T653">teʔtə, </ts>
               <ts e="T655" id="Seg_4020" n="e" s="T654">sumna, </ts>
               <ts e="T656" id="Seg_4022" n="e" s="T655">muktuʔ, </ts>
               <ts e="T657" id="Seg_4024" n="e" s="T656">sejʔpü. </ts>
               <ts e="T658" id="Seg_4026" n="e" s="T657">Šĭnteʔtə. </ts>
               <ts e="T659" id="Seg_4028" n="e" s="T658">Amitun, </ts>
               <ts e="T660" id="Seg_4030" n="e" s="T659">bʼeʔ, </ts>
               <ts e="T661" id="Seg_4032" n="e" s="T660">bʼeʔ </ts>
               <ts e="T662" id="Seg_4034" n="e" s="T661">šide, </ts>
               <ts e="T663" id="Seg_4036" n="e" s="T662">bʼeʔ </ts>
               <ts e="T664" id="Seg_4038" n="e" s="T663">onʼiʔ, </ts>
               <ts e="T665" id="Seg_4040" n="e" s="T664">bʼeʔ </ts>
               <ts e="T666" id="Seg_4042" n="e" s="T665">nagur. </ts>
               <ts e="T667" id="Seg_4044" n="e" s="T666">Urgo </ts>
               <ts e="T668" id="Seg_4046" n="e" s="T667">koʔbdo </ts>
               <ts e="T669" id="Seg_4048" n="e" s="T668">külaːmbi. </ts>
               <ts e="T670" id="Seg_4050" n="e" s="T669">Bʼeʔ </ts>
               <ts e="T671" id="Seg_4052" n="e" s="T670">nagur </ts>
               <ts e="T672" id="Seg_4054" n="e" s="T671">kö </ts>
               <ts e="T673" id="Seg_4056" n="e" s="T672">ibi </ts>
               <ts e="T674" id="Seg_4058" n="e" s="T673">dĭʔnə. </ts>
               <ts e="T675" id="Seg_4060" n="e" s="T674">A </ts>
               <ts e="T676" id="Seg_4062" n="e" s="T675">Ăprosʼam </ts>
               <ts e="T678" id="Seg_4064" n="e" s="T676">kuʔpi, </ts>
               <ts e="T679" id="Seg_4066" n="e" s="T678">dĭn </ts>
               <ts e="T680" id="Seg_4068" n="e" s="T679">tibi </ts>
               <ts e="T681" id="Seg_4070" n="e" s="T680">kuʔpi </ts>
               <ts e="T682" id="Seg_4072" n="e" s="T681">dĭm. </ts>
               <ts e="T683" id="Seg_4074" n="e" s="T682">Nadja </ts>
               <ts e="T684" id="Seg_4076" n="e" s="T683">külaːmbi, </ts>
               <ts e="T685" id="Seg_4078" n="e" s="T684">sumna </ts>
               <ts e="T686" id="Seg_4080" n="e" s="T685">ki </ts>
               <ts e="T699" id="Seg_4082" n="e" s="T686">kalla </ts>
               <ts e="T687" id="Seg_4084" n="e" s="T699">dʼürbiʔi. </ts>
               <ts e="T688" id="Seg_4086" n="e" s="T687">A </ts>
               <ts e="T689" id="Seg_4088" n="e" s="T688">miʔ </ts>
               <ts e="T690" id="Seg_4090" n="e" s="T689">šidegöʔ </ts>
               <ts e="T691" id="Seg_4092" n="e" s="T690">maluʔpibaʔ. </ts>
               <ts e="T692" id="Seg_4094" n="e" s="T691">Klawdʼa </ts>
               <ts e="T693" id="Seg_4096" n="e" s="T692">i </ts>
               <ts e="T694" id="Seg_4098" n="e" s="T693">Maksim. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T12" id="Seg_4099" s="T5">PKZ_196X_SU0212.PKZ.001 (003)</ta>
            <ta e="T14" id="Seg_4100" s="T12">PKZ_196X_SU0212.PKZ.002 (004)</ta>
            <ta e="T18" id="Seg_4101" s="T14">PKZ_196X_SU0212.PKZ.003 (005)</ta>
            <ta e="T22" id="Seg_4102" s="T18">PKZ_196X_SU0212.PKZ.004 (006)</ta>
            <ta e="T31" id="Seg_4103" s="T26">PKZ_196X_SU0212.PKZ.005 (008)</ta>
            <ta e="T33" id="Seg_4104" s="T31">PKZ_196X_SU0212.PKZ.006 (009)</ta>
            <ta e="T37" id="Seg_4105" s="T33">PKZ_196X_SU0212.PKZ.007 (010)</ta>
            <ta e="T46" id="Seg_4106" s="T37">PKZ_196X_SU0212.PKZ.008 (011)</ta>
            <ta e="T55" id="Seg_4107" s="T46">PKZ_196X_SU0212.PKZ.009 (012)</ta>
            <ta e="T61" id="Seg_4108" s="T55">PKZ_196X_SU0212.PKZ.010 (013)</ta>
            <ta e="T67" id="Seg_4109" s="T61">PKZ_196X_SU0212.PKZ.011 (014)</ta>
            <ta e="T70" id="Seg_4110" s="T67">PKZ_196X_SU0212.PKZ.012 (015)</ta>
            <ta e="T73" id="Seg_4111" s="T70">PKZ_196X_SU0212.PKZ.013 (016)</ta>
            <ta e="T76" id="Seg_4112" s="T73">PKZ_196X_SU0212.PKZ.014 (017)</ta>
            <ta e="T79" id="Seg_4113" s="T76">PKZ_196X_SU0212.PKZ.015 (018)</ta>
            <ta e="T81" id="Seg_4114" s="T79">PKZ_196X_SU0212.PKZ.016 (019)</ta>
            <ta e="T84" id="Seg_4115" s="T81">PKZ_196X_SU0212.PKZ.017 (020)</ta>
            <ta e="T87" id="Seg_4116" s="T84">PKZ_196X_SU0212.PKZ.018 (021)</ta>
            <ta e="T90" id="Seg_4117" s="T87">PKZ_196X_SU0212.PKZ.019 (022)</ta>
            <ta e="T95" id="Seg_4118" s="T90">PKZ_196X_SU0212.PKZ.020 (023)</ta>
            <ta e="T99" id="Seg_4119" s="T95">PKZ_196X_SU0212.PKZ.021 (024)</ta>
            <ta e="T103" id="Seg_4120" s="T99">PKZ_196X_SU0212.PKZ.022 (025)</ta>
            <ta e="T105" id="Seg_4121" s="T103">PKZ_196X_SU0212.PKZ.023 (026)</ta>
            <ta e="T110" id="Seg_4122" s="T105">PKZ_196X_SU0212.PKZ.024 (027)</ta>
            <ta e="T113" id="Seg_4123" s="T110">PKZ_196X_SU0212.PKZ.025 (028)</ta>
            <ta e="T124" id="Seg_4124" s="T113">PKZ_196X_SU0212.PKZ.026 (029)</ta>
            <ta e="T125" id="Seg_4125" s="T124">PKZ_196X_SU0212.PKZ.027 (030)</ta>
            <ta e="T130" id="Seg_4126" s="T125">PKZ_196X_SU0212.PKZ.028 (031)</ta>
            <ta e="T138" id="Seg_4127" s="T130">PKZ_196X_SU0212.PKZ.029 (032)</ta>
            <ta e="T143" id="Seg_4128" s="T138">PKZ_196X_SU0212.PKZ.030 (033)</ta>
            <ta e="T148" id="Seg_4129" s="T143">PKZ_196X_SU0212.PKZ.031 (034)</ta>
            <ta e="T152" id="Seg_4130" s="T148">PKZ_196X_SU0212.PKZ.032 (035)</ta>
            <ta e="T156" id="Seg_4131" s="T152">PKZ_196X_SU0212.PKZ.033 (036)</ta>
            <ta e="T161" id="Seg_4132" s="T156">PKZ_196X_SU0212.PKZ.034 (037)</ta>
            <ta e="T164" id="Seg_4133" s="T161">PKZ_196X_SU0212.PKZ.035 (038)</ta>
            <ta e="T167" id="Seg_4134" s="T164">PKZ_196X_SU0212.PKZ.036 (039)</ta>
            <ta e="T170" id="Seg_4135" s="T167">PKZ_196X_SU0212.PKZ.037 (040)</ta>
            <ta e="T176" id="Seg_4136" s="T170">PKZ_196X_SU0212.PKZ.038 (041)</ta>
            <ta e="T179" id="Seg_4137" s="T176">PKZ_196X_SU0212.PKZ.039 (042)</ta>
            <ta e="T182" id="Seg_4138" s="T179">PKZ_196X_SU0212.PKZ.040 (043)</ta>
            <ta e="T187" id="Seg_4139" s="T182">PKZ_196X_SU0212.PKZ.041 (044)</ta>
            <ta e="T192" id="Seg_4140" s="T187">PKZ_196X_SU0212.PKZ.042 (045)</ta>
            <ta e="T195" id="Seg_4141" s="T192">PKZ_196X_SU0212.PKZ.043 (046)</ta>
            <ta e="T197" id="Seg_4142" s="T195">PKZ_196X_SU0212.PKZ.044 (047)</ta>
            <ta e="T199" id="Seg_4143" s="T197">PKZ_196X_SU0212.PKZ.045 (048)</ta>
            <ta e="T204" id="Seg_4144" s="T199">PKZ_196X_SU0212.PKZ.046 (049)</ta>
            <ta e="T206" id="Seg_4145" s="T204">PKZ_196X_SU0212.PKZ.047 (050)</ta>
            <ta e="T209" id="Seg_4146" s="T206">PKZ_196X_SU0212.PKZ.048 (051)</ta>
            <ta e="T217" id="Seg_4147" s="T209">PKZ_196X_SU0212.PKZ.049 (052)</ta>
            <ta e="T227" id="Seg_4148" s="T217">PKZ_196X_SU0212.PKZ.050 (053)</ta>
            <ta e="T234" id="Seg_4149" s="T227">PKZ_196X_SU0212.PKZ.051 (054)</ta>
            <ta e="T238" id="Seg_4150" s="T234">PKZ_196X_SU0212.PKZ.052 (055)</ta>
            <ta e="T244" id="Seg_4151" s="T238">PKZ_196X_SU0212.PKZ.053 (056)</ta>
            <ta e="T246" id="Seg_4152" s="T244">PKZ_196X_SU0212.PKZ.054 (057)</ta>
            <ta e="T247" id="Seg_4153" s="T246">PKZ_196X_SU0212.PKZ.055 (058)</ta>
            <ta e="T252" id="Seg_4154" s="T247">PKZ_196X_SU0212.PKZ.056 (059)</ta>
            <ta e="T259" id="Seg_4155" s="T252">PKZ_196X_SU0212.PKZ.057 (060)</ta>
            <ta e="T266" id="Seg_4156" s="T259">PKZ_196X_SU0212.PKZ.058 (061)</ta>
            <ta e="T276" id="Seg_4157" s="T266">PKZ_196X_SU0212.PKZ.059 (062)</ta>
            <ta e="T283" id="Seg_4158" s="T276">PKZ_196X_SU0212.PKZ.060 (063)</ta>
            <ta e="T285" id="Seg_4159" s="T283">PKZ_196X_SU0212.PKZ.061 (064)</ta>
            <ta e="T290" id="Seg_4160" s="T285">PKZ_196X_SU0212.PKZ.062 (065)</ta>
            <ta e="T292" id="Seg_4161" s="T290">PKZ_196X_SU0212.PKZ.063 (066)</ta>
            <ta e="T297" id="Seg_4162" s="T292">PKZ_196X_SU0212.PKZ.064 (067)</ta>
            <ta e="T301" id="Seg_4163" s="T297">PKZ_196X_SU0212.PKZ.065 (068)</ta>
            <ta e="T307" id="Seg_4164" s="T301">PKZ_196X_SU0212.PKZ.066 (069)</ta>
            <ta e="T317" id="Seg_4165" s="T307">PKZ_196X_SU0212.PKZ.067 (070)</ta>
            <ta e="T323" id="Seg_4166" s="T317">PKZ_196X_SU0212.PKZ.068 (071)</ta>
            <ta e="T327" id="Seg_4167" s="T323">PKZ_196X_SU0212.PKZ.069 (072)</ta>
            <ta e="T335" id="Seg_4168" s="T327">PKZ_196X_SU0212.PKZ.070 (073)</ta>
            <ta e="T341" id="Seg_4169" s="T335">PKZ_196X_SU0212.PKZ.071 (074)</ta>
            <ta e="T346" id="Seg_4170" s="T341">PKZ_196X_SU0212.PKZ.072 (075)</ta>
            <ta e="T350" id="Seg_4171" s="T346">PKZ_196X_SU0212.PKZ.073 (076)</ta>
            <ta e="T351" id="Seg_4172" s="T350">PKZ_196X_SU0212.PKZ.074 (077)</ta>
            <ta e="T353" id="Seg_4173" s="T351">PKZ_196X_SU0212.PKZ.075 (078)</ta>
            <ta e="T357" id="Seg_4174" s="T353">PKZ_196X_SU0212.PKZ.076 (079)</ta>
            <ta e="T360" id="Seg_4175" s="T357">PKZ_196X_SU0212.PKZ.077 (080)</ta>
            <ta e="T361" id="Seg_4176" s="T360">PKZ_196X_SU0212.PKZ.078 (081)</ta>
            <ta e="T367" id="Seg_4177" s="T361">PKZ_196X_SU0212.PKZ.079 (082)</ta>
            <ta e="T371" id="Seg_4178" s="T367">PKZ_196X_SU0212.PKZ.080 (083)</ta>
            <ta e="T373" id="Seg_4179" s="T371">PKZ_196X_SU0212.PKZ.081 (084)</ta>
            <ta e="T375" id="Seg_4180" s="T373">PKZ_196X_SU0212.PKZ.082 (085)</ta>
            <ta e="T377" id="Seg_4181" s="T375">PKZ_196X_SU0212.PKZ.083 (086)</ta>
            <ta e="T381" id="Seg_4182" s="T377">PKZ_196X_SU0212.PKZ.084 (087)</ta>
            <ta e="T383" id="Seg_4183" s="T381">PKZ_196X_SU0212.PKZ.085 (088)</ta>
            <ta e="T387" id="Seg_4184" s="T383">PKZ_196X_SU0212.PKZ.086 (089)</ta>
            <ta e="T391" id="Seg_4185" s="T387">PKZ_196X_SU0212.PKZ.087 (090)</ta>
            <ta e="T397" id="Seg_4186" s="T391">PKZ_196X_SU0212.PKZ.088 (091)</ta>
            <ta e="T400" id="Seg_4187" s="T397">PKZ_196X_SU0212.PKZ.089 (092)</ta>
            <ta e="T404" id="Seg_4188" s="T400">PKZ_196X_SU0212.PKZ.090 (093)</ta>
            <ta e="T408" id="Seg_4189" s="T404">PKZ_196X_SU0212.PKZ.091 (094)</ta>
            <ta e="T410" id="Seg_4190" s="T408">PKZ_196X_SU0212.PKZ.092 (095)</ta>
            <ta e="T415" id="Seg_4191" s="T410">PKZ_196X_SU0212.PKZ.093 (096)</ta>
            <ta e="T417" id="Seg_4192" s="T415">PKZ_196X_SU0212.PKZ.094 (097)</ta>
            <ta e="T422" id="Seg_4193" s="T417">PKZ_196X_SU0212.PKZ.095 (098)</ta>
            <ta e="T429" id="Seg_4194" s="T422">PKZ_196X_SU0212.PKZ.096 (099)</ta>
            <ta e="T437" id="Seg_4195" s="T429">PKZ_196X_SU0212.PKZ.097 (100)</ta>
            <ta e="T447" id="Seg_4196" s="T437">PKZ_196X_SU0212.PKZ.098 (101)</ta>
            <ta e="T451" id="Seg_4197" s="T447">PKZ_196X_SU0212.PKZ.099 (102)</ta>
            <ta e="T456" id="Seg_4198" s="T451">PKZ_196X_SU0212.PKZ.100 (103)</ta>
            <ta e="T459" id="Seg_4199" s="T456">PKZ_196X_SU0212.PKZ.101 (104)</ta>
            <ta e="T467" id="Seg_4200" s="T459">PKZ_196X_SU0212.PKZ.102 (105)</ta>
            <ta e="T472" id="Seg_4201" s="T467">PKZ_196X_SU0212.PKZ.103 (106)</ta>
            <ta e="T474" id="Seg_4202" s="T472">PKZ_196X_SU0212.PKZ.104 (107)</ta>
            <ta e="T482" id="Seg_4203" s="T474">PKZ_196X_SU0212.PKZ.105 (108)</ta>
            <ta e="T487" id="Seg_4204" s="T482">PKZ_196X_SU0212.PKZ.106 (109)</ta>
            <ta e="T493" id="Seg_4205" s="T487">PKZ_196X_SU0212.PKZ.107 (110)</ta>
            <ta e="T498" id="Seg_4206" s="T493">PKZ_196X_SU0212.PKZ.108 (111)</ta>
            <ta e="T505" id="Seg_4207" s="T498">PKZ_196X_SU0212.PKZ.109 (112)</ta>
            <ta e="T509" id="Seg_4208" s="T505">PKZ_196X_SU0212.PKZ.110 (113)</ta>
            <ta e="T513" id="Seg_4209" s="T509">PKZ_196X_SU0212.PKZ.111 (114)</ta>
            <ta e="T518" id="Seg_4210" s="T513">PKZ_196X_SU0212.PKZ.112 (115)</ta>
            <ta e="T525" id="Seg_4211" s="T518">PKZ_196X_SU0212.PKZ.113 (116)</ta>
            <ta e="T532" id="Seg_4212" s="T525">PKZ_196X_SU0212.PKZ.114 (117)</ta>
            <ta e="T535" id="Seg_4213" s="T532">PKZ_196X_SU0212.PKZ.115 (118)</ta>
            <ta e="T543" id="Seg_4214" s="T535">PKZ_196X_SU0212.PKZ.116 (119)</ta>
            <ta e="T544" id="Seg_4215" s="T543">PKZ_196X_SU0212.PKZ.117 (120)</ta>
            <ta e="T549" id="Seg_4216" s="T544">PKZ_196X_SU0212.PKZ.118 (121)</ta>
            <ta e="T554" id="Seg_4217" s="T549">PKZ_196X_SU0212.PKZ.119 (122)</ta>
            <ta e="T556" id="Seg_4218" s="T554">PKZ_196X_SU0212.PKZ.120 (123)</ta>
            <ta e="T559" id="Seg_4219" s="T556">PKZ_196X_SU0212.PKZ.121 (124)</ta>
            <ta e="T563" id="Seg_4220" s="T559">PKZ_196X_SU0212.PKZ.122 (125)</ta>
            <ta e="T566" id="Seg_4221" s="T563">PKZ_196X_SU0212.PKZ.123 (126)</ta>
            <ta e="T572" id="Seg_4222" s="T566">PKZ_196X_SU0212.PKZ.124 (127)</ta>
            <ta e="T576" id="Seg_4223" s="T572">PKZ_196X_SU0212.PKZ.125 (128)</ta>
            <ta e="T580" id="Seg_4224" s="T576">PKZ_196X_SU0212.PKZ.126 (129)</ta>
            <ta e="T585" id="Seg_4225" s="T580">PKZ_196X_SU0212.PKZ.127 (130)</ta>
            <ta e="T590" id="Seg_4226" s="T585">PKZ_196X_SU0212.PKZ.128 (131)</ta>
            <ta e="T603" id="Seg_4227" s="T590">PKZ_196X_SU0212.PKZ.129 (132)</ta>
            <ta e="T606" id="Seg_4228" s="T603">PKZ_196X_SU0212.PKZ.130 (133)</ta>
            <ta e="T611" id="Seg_4229" s="T606">PKZ_196X_SU0212.PKZ.131 (134)</ta>
            <ta e="T618" id="Seg_4230" s="T611">PKZ_196X_SU0212.PKZ.132 (135)</ta>
            <ta e="T623" id="Seg_4231" s="T618">PKZ_196X_SU0212.PKZ.133 (136)</ta>
            <ta e="T626" id="Seg_4232" s="T623">PKZ_196X_SU0212.PKZ.134 (137)</ta>
            <ta e="T629" id="Seg_4233" s="T626">PKZ_196X_SU0212.PKZ.135 (138)</ta>
            <ta e="T637" id="Seg_4234" s="T629">PKZ_196X_SU0212.PKZ.136 (139)</ta>
            <ta e="T639" id="Seg_4235" s="T637">PKZ_196X_SU0212.PKZ.137 (140)</ta>
            <ta e="T646" id="Seg_4236" s="T639">PKZ_196X_SU0212.PKZ.138 (141)</ta>
            <ta e="T650" id="Seg_4237" s="T646">PKZ_196X_SU0212.PKZ.139 (142)</ta>
            <ta e="T657" id="Seg_4238" s="T650">PKZ_196X_SU0212.PKZ.140 (143)</ta>
            <ta e="T658" id="Seg_4239" s="T657">PKZ_196X_SU0212.PKZ.141 (144)</ta>
            <ta e="T666" id="Seg_4240" s="T658">PKZ_196X_SU0212.PKZ.142 (145)</ta>
            <ta e="T669" id="Seg_4241" s="T666">PKZ_196X_SU0212.PKZ.143 (146)</ta>
            <ta e="T674" id="Seg_4242" s="T669">PKZ_196X_SU0212.PKZ.144 (147)</ta>
            <ta e="T682" id="Seg_4243" s="T674">PKZ_196X_SU0212.PKZ.145 (148)</ta>
            <ta e="T687" id="Seg_4244" s="T682">PKZ_196X_SU0212.PKZ.146 (149)</ta>
            <ta e="T691" id="Seg_4245" s="T687">PKZ_196X_SU0212.PKZ.147 (150)</ta>
            <ta e="T694" id="Seg_4246" s="T691">PKZ_196X_SU0212.PKZ.148 (151)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T12" id="Seg_4247" s="T5">Măndəraʔ (aʔ-) beškeʔi, kuiol bar, nugaʔi dĭn! </ta>
            <ta e="T14" id="Seg_4248" s="T12">Dibər kanaʔ! </ta>
            <ta e="T18" id="Seg_4249" s="T14">A măn döbər kalam. </ta>
            <ta e="T22" id="Seg_4250" s="T18">Nada iʔgö oʔbdəsʼtə beškeʔi. </ta>
            <ta e="T31" id="Seg_4251" s="T26">(Pa-) Paʔin toːndə măndəraʔ, jakšəŋ. </ta>
            <ta e="T33" id="Seg_4252" s="T31">Iʔ dʼabit! </ta>
            <ta e="T37" id="Seg_4253" s="T33">Na što dʼaʔpil, (ö-) öʔlit! </ta>
            <ta e="T46" id="Seg_4254" s="T37">Dĭ il šobiʔi miʔnʼibeʔ, (bostə- bos-) bostə koŋ nuldlaʔbəʔjə. </ta>
            <ta e="T55" id="Seg_4255" s="T46">((DMG)) Dĭzeŋ tĭmneʔi sazən (pʼ-) pʼaŋzittə, a miʔ ej tĭmnebeʔ. </ta>
            <ta e="T61" id="Seg_4256" s="T55">((DMG)) Dĭgəttə dĭzeŋ bar bostə (koŋzeŋdə) nubiʔi. </ta>
            <ta e="T67" id="Seg_4257" s="T61">Tüj miʔ bar kazak šĭke dʼăbaktərlaʔbəbaʔ. </ta>
            <ta e="T70" id="Seg_4258" s="T67">Kazan šĭket tüšəlleʔbəbaʔ. </ta>
            <ta e="T73" id="Seg_4259" s="T70">Kăde tănan kăštəliaʔi? </ta>
            <ta e="T76" id="Seg_4260" s="T73">I kăde numəllieʔi? </ta>
            <ta e="T79" id="Seg_4261" s="T76">Măna kăštəliaʔi Klawdʼa. </ta>
            <ta e="T81" id="Seg_4262" s="T79">A … </ta>
            <ta e="T84" id="Seg_4263" s="T81">A numəjlieʔi Захаровна. </ta>
            <ta e="T87" id="Seg_4264" s="T84">Kăde šobial döbər? </ta>
            <ta e="T90" id="Seg_4265" s="T87">Dö măn koŋ. </ta>
            <ta e="T95" id="Seg_4266" s="T90">A dö dʼăbaktərzittə (kon-) koŋ. </ta>
            <ta e="T99" id="Seg_4267" s="T95">A măn dĭzeŋ tüšəlliem. </ta>
            <ta e="T103" id="Seg_4268" s="T99">Tăn tibi gibər kalla dʼürbi? </ta>
            <ta e="T105" id="Seg_4269" s="T103">Kazan turanə. </ta>
            <ta e="T110" id="Seg_4270" s="T105">(Măn) iʔgö (doʔpiel), tenəlüʔpiem dʼăbaktərzittə. </ta>
            <ta e="T113" id="Seg_4271" s="T110">Măn šoškam külaːmbi. </ta>
            <ta e="T124" id="Seg_4272" s="T113">Măn dʼü tĭlbiem, i dibər (dĭ-) dĭm embiem i dʼüzʼiʔ kămnabiam. </ta>
            <ta e="T125" id="Seg_4273" s="T124">Закрыла. </ta>
            <ta e="T130" id="Seg_4274" s="T125">Miʔ bar старо кладбище ibi. </ta>
            <ta e="T138" id="Seg_4275" s="T130">Dĭn iʔgö krospaʔi (nugaʔi- nuga- nugaʔi- nugal-) (nulaʔbəʔjə). </ta>
            <ta e="T143" id="Seg_4276" s="T138">A tüj onʼiʔ da naga. </ta>
            <ta e="T148" id="Seg_4277" s="T143">Ĭmbidə (tan- tăn-) dĭn naga. </ta>
            <ta e="T152" id="Seg_4278" s="T148">Tüj dĭn сушилка nulaʔbə. </ta>
            <ta e="T156" id="Seg_4279" s="T152">Tüj miʔ toʔbdə кладбище. </ta>
            <ta e="T161" id="Seg_4280" s="T156">Dĭn iʔgö krospaʔi nugaʔi tože. </ta>
            <ta e="T164" id="Seg_4281" s="T161">Tăn kudaj … </ta>
            <ta e="T167" id="Seg_4282" s="T164">Tăn, kudaj, kudaj. </ta>
            <ta e="T170" id="Seg_4283" s="T167">Iʔ maʔdə măna. </ta>
            <ta e="T176" id="Seg_4284" s="T170">Iʔ maʔdə măna, iʔ barəʔdə măna. </ta>
            <ta e="T179" id="Seg_4285" s="T176">Tăn (uget) măna. </ta>
            <ta e="T182" id="Seg_4286" s="T179">(Sildə-) Sĭjdə sagəšsəbi. </ta>
            <ta e="T187" id="Seg_4287" s="T182">Sĭjdə sagəšsəbi, tăn öʔlit măna. </ta>
            <ta e="T192" id="Seg_4288" s="T187">(Sĭ-) Sĭjbə (sagəš-) sagəštə ige. </ta>
            <ta e="T195" id="Seg_4289" s="T192">Sĭjbə sagəštə ige. </ta>
            <ta e="T197" id="Seg_4290" s="T195">Tüšəlleʔ măna. </ta>
            <ta e="T199" id="Seg_4291" s="T197">Maktanzərzittə tănzʼiʔ. </ta>
            <ta e="T204" id="Seg_4292" s="T199">Tüšəlleʔ măna, maktanərzittə (telə-) tănzʼiʔ. </ta>
            <ta e="T206" id="Seg_4293" s="T204">Tüšəlleʔ măna. </ta>
            <ta e="T209" id="Seg_4294" s="T206">Aktʼit tăn mĭnzittə. </ta>
            <ta e="T217" id="Seg_4295" s="T209">Măn kagam (bə-) bar bostə ((NOISE)) tüšəlbi pʼaŋdəsʼtə ((NOISE)) dibər. </ta>
            <ta e="T227" id="Seg_4296" s="T217">Ildə (mănl- măndəga-) mĭnge (i) iššo куда - нибудь kaləj. </ta>
            <ta e="T234" id="Seg_4297" s="T227">Dĭn šaldə mĭzittə, a dĭn šaldə izittə. </ta>
            <ta e="T238" id="Seg_4298" s="T234">Nada măna salba sarzittə. </ta>
            <ta e="T244" id="Seg_4299" s="T238">Kola (ja- jam- jal- jam- l-). (…)</ta>
            <ta e="T246" id="Seg_4300" s="T244">Kola (dʼabəzittə). </ta>
            <ta e="T247" id="Seg_4301" s="T246">((…)).</ta>
            <ta e="T252" id="Seg_4302" s="T247">((DMG)) Dĭzeŋ nuzaŋ bar saliktə kandlaʔbəʔjə. </ta>
            <ta e="T259" id="Seg_4303" s="T252">I (salba) barəʔlaʔbəʔjə i kola (dʼab-) dʼabəlaʔbəʔjə. </ta>
            <ta e="T266" id="Seg_4304" s="T259">Piʔi bar šonə šödörlaʔbəʔjə i salbanə sarlaʔbəʔjə. </ta>
            <ta e="T276" id="Seg_4305" s="T266">Dĭ kudanə bar ĭmbi detleʔbə kudaj ilʼi šindi, ej tĭmnem. </ta>
            <ta e="T283" id="Seg_4306" s="T276">Sĭrenə bar suʔməluʔpiem, pʼel (dak) bar kalla dʼürbiem. </ta>
            <ta e="T285" id="Seg_4307" s="T283">Măn šonəgam. </ta>
            <ta e="T290" id="Seg_4308" s="T285">Sĭre ugaːndə (i-) iʔgö iʔbolaʔbə. </ta>
            <ta e="T292" id="Seg_4309" s="T290">Măn kambiam. </ta>
            <ta e="T297" id="Seg_4310" s="T292">I dĭbər suʔməluʔpiem bar pʼeldə. </ta>
            <ta e="T301" id="Seg_4311" s="T297">Măn болото (sĭʔitsə) šobiam. </ta>
            <ta e="T307" id="Seg_4312" s="T301">Dĭn bar ugaːndə balgaš (ig-) iʔgö. </ta>
            <ta e="T317" id="Seg_4313" s="T307">Măn nubiam i dibər saʔməluʔpiam bar (uda-) udazaŋdə, до рук. </ta>
            <ta e="T323" id="Seg_4314" s="T317">((DMG)) Dĭgəttə kočkam udazʼiʔ dʼaʔpiom i parbiam. </ta>
            <ta e="T327" id="Seg_4315" s="T323">Ну я же говорила. </ta>
            <ta e="T335" id="Seg_4316" s="T327">Dĭ ineʔi bădəbiʔi, a dö ineʔi ej bădəbiʔi. </ta>
            <ta e="T341" id="Seg_4317" s="T335">Tăn ularəʔi mĭbiel noʔ, dĭzen ambiʔi. </ta>
            <ta e="T346" id="Seg_4318" s="T341">Tüžöjdə mĭbiel noʔ, dĭ ambi. </ta>
            <ta e="T350" id="Seg_4319" s="T346">Büzəjnə padʼi ej mĭbiel. </ta>
            <ta e="T351" id="Seg_4320" s="T350">Mĭbiem. </ta>
            <ta e="T353" id="Seg_4321" s="T351">Bar ambiʔi. </ta>
            <ta e="T357" id="Seg_4322" s="T353">(Kun) (maʔ-) ((DMG)) eššizi. </ta>
            <ta e="T360" id="Seg_4323" s="T357">Bădəbiol li dĭm? </ta>
            <ta e="T361" id="Seg_4324" s="T360">Bădəbiom! </ta>
            <ta e="T367" id="Seg_4325" s="T361">I süt mĭbiem, i bü bĭʔpi. </ta>
            <ta e="T371" id="Seg_4326" s="T367">Šenap (ambi=) ambi ešši. </ta>
            <ta e="T373" id="Seg_4327" s="T371">Ej amnia. </ta>
            <ta e="T375" id="Seg_4328" s="T373">Ej šamnial! </ta>
            <ta e="T377" id="Seg_4329" s="T375">Šenap nörbəbiel. (…)</ta>
            <ta e="T381" id="Seg_4330" s="T377">Dĭzeŋ ((DMG)) paʔi baltuzʼiʔ. </ta>
            <ta e="T383" id="Seg_4331" s="T381">Baltuzʼiʔ abiʔi. </ta>
            <ta e="T387" id="Seg_4332" s="T383">I dĭgəttə bünə kambiʔi. </ta>
            <ta e="T391" id="Seg_4333" s="T387">I kola dʼaʔpiʔi dĭn. </ta>
            <ta e="T397" id="Seg_4334" s="T391">((DMG)) (-zaŋ) bar onʼiʔ onʼiʔdə sarbiʔi. </ta>
            <ta e="T400" id="Seg_4335" s="T397">I bünə kambiʔi. </ta>
            <ta e="T404" id="Seg_4336" s="T400">(Baža-) Băzaʔ kadəldə büzʼiʔ. </ta>
            <ta e="T408" id="Seg_4337" s="T404">I kĭškit, sĭre molal. </ta>
            <ta e="T410" id="Seg_4338" s="T408">Kuvas molal. </ta>
            <ta e="T415" id="Seg_4339" s="T410">Šiʔ il (gije-) gibər kandəbiʔi? </ta>
            <ta e="T417" id="Seg_4340" s="T415">(Rɨbranə) kandəbiʔi? </ta>
            <ta e="T422" id="Seg_4341" s="T417">(Ul- urgo-) Iʔgö buragən bar. </ta>
            <ta e="T429" id="Seg_4342" s="T422">Albugaʔi bar sarbi teʔmezi i deʔpi maʔndə. </ta>
            <ta e="T437" id="Seg_4343" s="T429">Dĭ kuza bar togonorlia, togonorlia, a ĭmbidə naga. </ta>
            <ta e="T447" id="Seg_4344" s="T437">A dö kuza bar ej tăŋ togonorlia, bar ĭmbi ige. </ta>
            <ta e="T451" id="Seg_4345" s="T447">(Iʔ iʔbdoʔ-) Iʔ iʔboʔ. </ta>
            <ta e="T456" id="Seg_4346" s="T451">Uʔbdaʔ da togonoraʔ, ĭmbi (iʔbozittə)? </ta>
            <ta e="T459" id="Seg_4347" s="T456">((DMG)) Iʔgö pʼe kalla dʼürbiʔi. </ta>
            <ta e="T467" id="Seg_4348" s="T459">Šobiʔi kömə, sĭreʔi šobiʔi, a il ugandə pimnieʔi. </ta>
            <ta e="T472" id="Seg_4349" s="T467">((DMG)) (Il) (bal-) bar nuʔməluʔpiʔi dʼijenə. </ta>
            <ta e="T474" id="Seg_4350" s="T472">Ugandə pimbiʔi. </ta>
            <ta e="T482" id="Seg_4351" s="T474">A kamən šoləʔi maʔnə, dĭzeŋ bar (šolə-) šobiʔi. </ta>
            <ta e="T487" id="Seg_4352" s="T482">((DMG)) Dĭzeŋ bar šobiʔi, sĭre il. </ta>
            <ta e="T493" id="Seg_4353" s="T487">Mĭʔ ilbeʔ bar köməʔi (m-) ajirbiʔi. </ta>
            <ta e="T498" id="Seg_4354" s="T493">(Dət-) Dĭzeŋdə ipek (mĭm-) mĭbiʔi. </ta>
            <ta e="T505" id="Seg_4355" s="T498">Ĭmbi ige bar mĭbiʔi: tus, aspaʔ mĭbiʔi. </ta>
            <ta e="T509" id="Seg_4356" s="T505">Iššo (toltanoʔ) dĭzeŋdə mĭbiʔi. </ta>
            <ta e="T513" id="Seg_4357" s="T509">Köməʔi bar dʼijegən amnobiʔi. </ta>
            <ta e="T518" id="Seg_4358" s="T513">A nüdʼin döbər šobiʔi, amorzittə. </ta>
            <ta e="T525" id="Seg_4359" s="T518">(O-) onʼiʔ раз šobiʔi šide отряд köməʔi. </ta>
            <ta e="T532" id="Seg_4360" s="T525">Multuksiʔ один onʼiʔdə bar чуть ej kuʔpiʔi. </ta>
            <ta e="T535" id="Seg_4361" s="T532">Nüke miʔnʼibeʔ šonəga. </ta>
            <ta e="T543" id="Seg_4362" s="T535">"Šiʔ bar dön dʼăbaktərlialaʔ, a kuza kuznekkən nuga. </ta>
            <ta e="T544" id="Seg_4363" s="T543">Nʼilgölaʔbə". </ta>
            <ta e="T549" id="Seg_4364" s="T544">Miʔ bar (šut- šu- )… </ta>
            <ta e="T554" id="Seg_4365" s="T549">Onʼiʔ (nüže-) nüdʼin šobiʔi köməʔi. </ta>
            <ta e="T556" id="Seg_4366" s="T554">Kuza deʔpiʔi. </ta>
            <ta e="T559" id="Seg_4367" s="T556">Ajəndə tondə kuʔpiʔi. </ta>
            <ta e="T563" id="Seg_4368" s="T559">Dĭgəttə bar tibizeŋ oʔbdəbiʔi. </ta>
            <ta e="T566" id="Seg_4369" s="T563">Münörbiʔi bar plʼotkaʔizi. </ta>
            <ta e="T572" id="Seg_4370" s="T566">Onʼiʔ kuza bar piʔməndə bar tüʔpi. </ta>
            <ta e="T576" id="Seg_4371" s="T572">Măn abam kăštəbiʔi Zaxar. </ta>
            <ta e="T580" id="Seg_4372" s="T576">A (nüməjləʔlʼi-) numəjluʔpiʔi Степанович. </ta>
            <ta e="T585" id="Seg_4373" s="T580">A iam Afanasʼa, numəjluʔpiʔi Антоновна. </ta>
            <ta e="T590" id="Seg_4374" s="T585">A familija dĭzen ibi Andʼigatəʔi. </ta>
            <ta e="T603" id="Seg_4375" s="T590">Esseŋdə ibi (o-) oʔb, šide, nagur, teʔtə, (sumna), sejʔpü, šĭnteʔtə, amitun ((PAUSE)) восемь. </ta>
            <ta e="T606" id="Seg_4376" s="T603">Urgo kăštəbiʔi Лена. </ta>
            <ta e="T611" id="Seg_4377" s="T606">A nʼibə (ku-) kăštəbiʔi Дёма. </ta>
            <ta e="T618" id="Seg_4378" s="T611">Dĭgəttə (măn= măn= š-) măna kăštəbiʔi Klawdʼeja. </ta>
            <ta e="T623" id="Seg_4379" s="T618">(Măn=) Măn sʼestram kăštəbiʔi Надя. </ta>
            <ta e="T626" id="Seg_4380" s="T623">Dĭgəttə ibi Ăprosʼa. </ta>
            <ta e="T629" id="Seg_4381" s="T626">Dĭgəttə ibi Вера. </ta>
            <ta e="T637" id="Seg_4382" s="T629">Dĭgəttə Маня ibi, dĭgəttə nʼi šobi, (Ma- Maksʼi-). </ta>
            <ta e="T639" id="Seg_4383" s="T637">Kăštəbiʔi Максим. </ta>
            <ta e="T646" id="Seg_4384" s="T639">Onʼiʔ nʼi külaːmbi, măna iššo naga ibi. </ta>
            <ta e="T650" id="Seg_4385" s="T646">(A=) A koʔbdo külaːmbi. </ta>
            <ta e="T657" id="Seg_4386" s="T650">Oʔb, šide, nagur, teʔtə, sumna, muktuʔ, sejʔpü. </ta>
            <ta e="T658" id="Seg_4387" s="T657">Šĭnteʔtə. </ta>
            <ta e="T666" id="Seg_4388" s="T658">Amitun, bʼeʔ, bʼeʔ šide, bʼeʔ onʼiʔ, bʼeʔ nagur. </ta>
            <ta e="T669" id="Seg_4389" s="T666">Urgo koʔbdo külaːmbi. </ta>
            <ta e="T674" id="Seg_4390" s="T669">Bʼeʔ nagur kö ibi dĭʔnə. </ta>
            <ta e="T682" id="Seg_4391" s="T674">A Ăprosʼam kuʔpi, dĭn tibi kuʔpi dĭm. </ta>
            <ta e="T687" id="Seg_4392" s="T682">Надя külaːmbi, sumna ki kalla dʼürbiʔi. </ta>
            <ta e="T691" id="Seg_4393" s="T687">((DMG)) A miʔ šidegöʔ maluʔpibaʔ. </ta>
            <ta e="T694" id="Seg_4394" s="T691">Klawdʼa i Максим. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T6" id="Seg_4395" s="T5">măndə-r-a-ʔ</ta>
            <ta e="T8" id="Seg_4396" s="T7">beške-ʔi</ta>
            <ta e="T9" id="Seg_4397" s="T8">ku-io-l</ta>
            <ta e="T10" id="Seg_4398" s="T9">bar</ta>
            <ta e="T11" id="Seg_4399" s="T10">nu-ga-ʔi</ta>
            <ta e="T12" id="Seg_4400" s="T11">dĭn</ta>
            <ta e="T13" id="Seg_4401" s="T12">dibər</ta>
            <ta e="T14" id="Seg_4402" s="T13">kan-a-ʔ</ta>
            <ta e="T15" id="Seg_4403" s="T14">a</ta>
            <ta e="T16" id="Seg_4404" s="T15">măn</ta>
            <ta e="T17" id="Seg_4405" s="T16">döbər</ta>
            <ta e="T18" id="Seg_4406" s="T17">ka-la-m</ta>
            <ta e="T19" id="Seg_4407" s="T18">nada</ta>
            <ta e="T20" id="Seg_4408" s="T19">iʔgö</ta>
            <ta e="T21" id="Seg_4409" s="T20">oʔbdə-sʼtə</ta>
            <ta e="T22" id="Seg_4410" s="T21">beške-ʔi</ta>
            <ta e="T28" id="Seg_4411" s="T27">pa-ʔi-n</ta>
            <ta e="T29" id="Seg_4412" s="T28">toː-ndə</ta>
            <ta e="T30" id="Seg_4413" s="T29">măndə-r-a-ʔ</ta>
            <ta e="T31" id="Seg_4414" s="T30">jakšə-ŋ</ta>
            <ta e="T32" id="Seg_4415" s="T31">i-ʔ</ta>
            <ta e="T33" id="Seg_4416" s="T32">dʼabi-t</ta>
            <ta e="T34" id="Seg_4417" s="T33">našto</ta>
            <ta e="T35" id="Seg_4418" s="T34">dʼaʔ-pi-l</ta>
            <ta e="T37" id="Seg_4419" s="T36">öʔli-t</ta>
            <ta e="T38" id="Seg_4420" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_4421" s="T38">il</ta>
            <ta e="T40" id="Seg_4422" s="T39">šo-bi-ʔi</ta>
            <ta e="T41" id="Seg_4423" s="T40">miʔnʼibeʔ</ta>
            <ta e="T44" id="Seg_4424" s="T43">bos-tə</ta>
            <ta e="T45" id="Seg_4425" s="T44">koŋ</ta>
            <ta e="T46" id="Seg_4426" s="T45">nuld-laʔbə-ʔjə</ta>
            <ta e="T47" id="Seg_4427" s="T46">dĭ-zeŋ</ta>
            <ta e="T48" id="Seg_4428" s="T47">tĭmne-ʔi</ta>
            <ta e="T49" id="Seg_4429" s="T48">sazən</ta>
            <ta e="T51" id="Seg_4430" s="T50">pʼaŋ-zittə</ta>
            <ta e="T52" id="Seg_4431" s="T51">a</ta>
            <ta e="T53" id="Seg_4432" s="T52">miʔ</ta>
            <ta e="T54" id="Seg_4433" s="T53">ej</ta>
            <ta e="T55" id="Seg_4434" s="T54">tĭmne-beʔ</ta>
            <ta e="T56" id="Seg_4435" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_4436" s="T56">dĭ-zeŋ</ta>
            <ta e="T58" id="Seg_4437" s="T57">bar</ta>
            <ta e="T59" id="Seg_4438" s="T58">bos-tə</ta>
            <ta e="T60" id="Seg_4439" s="T59">koŋ-zeŋ-də</ta>
            <ta e="T61" id="Seg_4440" s="T60">nu-bi-ʔi</ta>
            <ta e="T62" id="Seg_4441" s="T61">tüj</ta>
            <ta e="T63" id="Seg_4442" s="T62">miʔ</ta>
            <ta e="T64" id="Seg_4443" s="T63">bar</ta>
            <ta e="T65" id="Seg_4444" s="T64">kazak</ta>
            <ta e="T66" id="Seg_4445" s="T65">šĭke</ta>
            <ta e="T67" id="Seg_4446" s="T66">dʼăbaktər-laʔbə-baʔ</ta>
            <ta e="T68" id="Seg_4447" s="T67">kaza-n</ta>
            <ta e="T69" id="Seg_4448" s="T68">šĭke-t</ta>
            <ta e="T70" id="Seg_4449" s="T69">tüšəl-leʔbə-baʔ</ta>
            <ta e="T71" id="Seg_4450" s="T70">kăde</ta>
            <ta e="T72" id="Seg_4451" s="T71">tănan</ta>
            <ta e="T73" id="Seg_4452" s="T72">kăštə-lia-ʔi</ta>
            <ta e="T74" id="Seg_4453" s="T73">i</ta>
            <ta e="T75" id="Seg_4454" s="T74">kăde</ta>
            <ta e="T76" id="Seg_4455" s="T75">numəl-lie-ʔi</ta>
            <ta e="T77" id="Seg_4456" s="T76">măna</ta>
            <ta e="T78" id="Seg_4457" s="T77">kăštə-lia-ʔi</ta>
            <ta e="T79" id="Seg_4458" s="T78">Klawdʼa</ta>
            <ta e="T81" id="Seg_4459" s="T79">a</ta>
            <ta e="T82" id="Seg_4460" s="T81">a</ta>
            <ta e="T83" id="Seg_4461" s="T82">numəj-lie-ʔi</ta>
            <ta e="T85" id="Seg_4462" s="T84">kăde</ta>
            <ta e="T86" id="Seg_4463" s="T85">šo-bia-l</ta>
            <ta e="T87" id="Seg_4464" s="T86">döbər</ta>
            <ta e="T88" id="Seg_4465" s="T87">dö</ta>
            <ta e="T89" id="Seg_4466" s="T88">măn</ta>
            <ta e="T90" id="Seg_4467" s="T89">koŋ</ta>
            <ta e="T91" id="Seg_4468" s="T90">a</ta>
            <ta e="T92" id="Seg_4469" s="T91">dö</ta>
            <ta e="T93" id="Seg_4470" s="T92">dʼăbaktər-zittə</ta>
            <ta e="T95" id="Seg_4471" s="T94">koŋ</ta>
            <ta e="T96" id="Seg_4472" s="T95">a</ta>
            <ta e="T97" id="Seg_4473" s="T96">măn</ta>
            <ta e="T98" id="Seg_4474" s="T97">dĭ-zeŋ</ta>
            <ta e="T99" id="Seg_4475" s="T98">tüšə-l-lie-m</ta>
            <ta e="T100" id="Seg_4476" s="T99">tăn</ta>
            <ta e="T101" id="Seg_4477" s="T100">tibi</ta>
            <ta e="T102" id="Seg_4478" s="T101">gibər</ta>
            <ta e="T696" id="Seg_4479" s="T102">kal-la</ta>
            <ta e="T103" id="Seg_4480" s="T696">dʼür-bi</ta>
            <ta e="T104" id="Seg_4481" s="T103">kaza-n</ta>
            <ta e="T105" id="Seg_4482" s="T104">tura-nə</ta>
            <ta e="T106" id="Seg_4483" s="T105">măn</ta>
            <ta e="T107" id="Seg_4484" s="T106">iʔgö</ta>
            <ta e="T108" id="Seg_4485" s="T107">doʔ-pie-l</ta>
            <ta e="T109" id="Seg_4486" s="T108">tenə-lüʔ-pie-m</ta>
            <ta e="T110" id="Seg_4487" s="T109">dʼăbaktər-zittə</ta>
            <ta e="T111" id="Seg_4488" s="T110">măn</ta>
            <ta e="T112" id="Seg_4489" s="T111">šoška-m</ta>
            <ta e="T113" id="Seg_4490" s="T112">kü-laːm-bi</ta>
            <ta e="T114" id="Seg_4491" s="T113">măn</ta>
            <ta e="T115" id="Seg_4492" s="T114">dʼü</ta>
            <ta e="T116" id="Seg_4493" s="T115">tĭl-bie-m</ta>
            <ta e="T117" id="Seg_4494" s="T116">i</ta>
            <ta e="T118" id="Seg_4495" s="T117">dibər</ta>
            <ta e="T120" id="Seg_4496" s="T119">dĭ-m</ta>
            <ta e="T121" id="Seg_4497" s="T120">em-bie-m</ta>
            <ta e="T122" id="Seg_4498" s="T121">i</ta>
            <ta e="T123" id="Seg_4499" s="T122">dʼü-zʼiʔ</ta>
            <ta e="T124" id="Seg_4500" s="T123">kămna-bia-m</ta>
            <ta e="T126" id="Seg_4501" s="T125">miʔ</ta>
            <ta e="T127" id="Seg_4502" s="T126">bar</ta>
            <ta e="T130" id="Seg_4503" s="T129">i-bi</ta>
            <ta e="T131" id="Seg_4504" s="T130">dĭn</ta>
            <ta e="T132" id="Seg_4505" s="T131">iʔgö</ta>
            <ta e="T133" id="Seg_4506" s="T132">krospa-ʔi</ta>
            <ta e="T138" id="Seg_4507" s="T137">nu-laʔbə-ʔjə</ta>
            <ta e="T139" id="Seg_4508" s="T138">a</ta>
            <ta e="T140" id="Seg_4509" s="T139">tüj</ta>
            <ta e="T142" id="Seg_4510" s="T140">onʼiʔ=da</ta>
            <ta e="T143" id="Seg_4511" s="T142">naga</ta>
            <ta e="T144" id="Seg_4512" s="T143">ĭmbi=də</ta>
            <ta e="T147" id="Seg_4513" s="T146">dĭn</ta>
            <ta e="T148" id="Seg_4514" s="T147">naga</ta>
            <ta e="T149" id="Seg_4515" s="T148">tüj</ta>
            <ta e="T150" id="Seg_4516" s="T149">dĭn</ta>
            <ta e="T152" id="Seg_4517" s="T151">nu-laʔbə</ta>
            <ta e="T153" id="Seg_4518" s="T152">tüj</ta>
            <ta e="T154" id="Seg_4519" s="T153">miʔ</ta>
            <ta e="T155" id="Seg_4520" s="T154">toʔbdə</ta>
            <ta e="T157" id="Seg_4521" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_4522" s="T157">iʔgö</ta>
            <ta e="T159" id="Seg_4523" s="T158">krospa-ʔi</ta>
            <ta e="T160" id="Seg_4524" s="T159">nu-ga-ʔi</ta>
            <ta e="T161" id="Seg_4525" s="T160">tože</ta>
            <ta e="T162" id="Seg_4526" s="T161">tăn</ta>
            <ta e="T164" id="Seg_4527" s="T162">kudaj</ta>
            <ta e="T165" id="Seg_4528" s="T164">tăn</ta>
            <ta e="T166" id="Seg_4529" s="T165">kudaj</ta>
            <ta e="T167" id="Seg_4530" s="T166">kudaj</ta>
            <ta e="T168" id="Seg_4531" s="T167">i-ʔ</ta>
            <ta e="T169" id="Seg_4532" s="T168">maʔ-də</ta>
            <ta e="T170" id="Seg_4533" s="T169">măna</ta>
            <ta e="T171" id="Seg_4534" s="T170">i-ʔ</ta>
            <ta e="T172" id="Seg_4535" s="T171">maʔ-də</ta>
            <ta e="T173" id="Seg_4536" s="T172">măna</ta>
            <ta e="T174" id="Seg_4537" s="T173">i-ʔ</ta>
            <ta e="T175" id="Seg_4538" s="T174">barəʔ-də</ta>
            <ta e="T176" id="Seg_4539" s="T175">măna</ta>
            <ta e="T177" id="Seg_4540" s="T176">tăn</ta>
            <ta e="T178" id="Seg_4541" s="T177">uget</ta>
            <ta e="T179" id="Seg_4542" s="T178">măna</ta>
            <ta e="T181" id="Seg_4543" s="T180">sĭj-də</ta>
            <ta e="T182" id="Seg_4544" s="T181">sagəš-səbi</ta>
            <ta e="T183" id="Seg_4545" s="T182">sĭj-də</ta>
            <ta e="T184" id="Seg_4546" s="T183">sagəš-səbi</ta>
            <ta e="T185" id="Seg_4547" s="T184">tăn</ta>
            <ta e="T186" id="Seg_4548" s="T185">öʔli-t</ta>
            <ta e="T187" id="Seg_4549" s="T186">măna</ta>
            <ta e="T189" id="Seg_4550" s="T188">sĭj-bə</ta>
            <ta e="T191" id="Seg_4551" s="T190">sagəštə</ta>
            <ta e="T192" id="Seg_4552" s="T191">i-ge</ta>
            <ta e="T193" id="Seg_4553" s="T192">sĭj-bə</ta>
            <ta e="T194" id="Seg_4554" s="T193">sagəštə</ta>
            <ta e="T195" id="Seg_4555" s="T194">i-ge</ta>
            <ta e="T196" id="Seg_4556" s="T195">tüšəl-l-eʔ</ta>
            <ta e="T197" id="Seg_4557" s="T196">măna</ta>
            <ta e="T198" id="Seg_4558" s="T197">maktanzər-zittə</ta>
            <ta e="T199" id="Seg_4559" s="T198">tăn-zʼiʔ</ta>
            <ta e="T200" id="Seg_4560" s="T199">tüšəl-l-eʔ</ta>
            <ta e="T201" id="Seg_4561" s="T200">măna</ta>
            <ta e="T202" id="Seg_4562" s="T201">maktanər-zittə</ta>
            <ta e="T204" id="Seg_4563" s="T203">tăn-zʼiʔ</ta>
            <ta e="T205" id="Seg_4564" s="T204">tüšəl-l-eʔ</ta>
            <ta e="T206" id="Seg_4565" s="T205">măna</ta>
            <ta e="T207" id="Seg_4566" s="T206">aktʼi-t</ta>
            <ta e="T208" id="Seg_4567" s="T207">tăn</ta>
            <ta e="T209" id="Seg_4568" s="T208">mĭn-zittə</ta>
            <ta e="T210" id="Seg_4569" s="T209">măn</ta>
            <ta e="T211" id="Seg_4570" s="T210">kaga-m</ta>
            <ta e="T213" id="Seg_4571" s="T212">bar</ta>
            <ta e="T214" id="Seg_4572" s="T213">bos-tə</ta>
            <ta e="T215" id="Seg_4573" s="T214">tüšəl-bi</ta>
            <ta e="T216" id="Seg_4574" s="T215">pʼaŋdə-sʼtə</ta>
            <ta e="T217" id="Seg_4575" s="T216">dibər</ta>
            <ta e="T218" id="Seg_4576" s="T217">il-də</ta>
            <ta e="T221" id="Seg_4577" s="T220">mĭn-ge</ta>
            <ta e="T222" id="Seg_4578" s="T221">i</ta>
            <ta e="T223" id="Seg_4579" s="T222">iššo</ta>
            <ta e="T227" id="Seg_4580" s="T226">ka-lə-j</ta>
            <ta e="T228" id="Seg_4581" s="T227">dĭ-n</ta>
            <ta e="T229" id="Seg_4582" s="T228">šal-də</ta>
            <ta e="T230" id="Seg_4583" s="T229">mĭ-zittə</ta>
            <ta e="T231" id="Seg_4584" s="T230">a</ta>
            <ta e="T232" id="Seg_4585" s="T231">dĭ-n</ta>
            <ta e="T233" id="Seg_4586" s="T232">šal-də</ta>
            <ta e="T234" id="Seg_4587" s="T233">i-zittə</ta>
            <ta e="T235" id="Seg_4588" s="T234">nada</ta>
            <ta e="T236" id="Seg_4589" s="T235">măna</ta>
            <ta e="T237" id="Seg_4590" s="T236">salba</ta>
            <ta e="T238" id="Seg_4591" s="T237">sar-zittə</ta>
            <ta e="T239" id="Seg_4592" s="T238">kola</ta>
            <ta e="T245" id="Seg_4593" s="T244">kola</ta>
            <ta e="T246" id="Seg_4594" s="T245">dʼabə-zittə</ta>
            <ta e="T248" id="Seg_4595" s="T247">dĭ-zeŋ</ta>
            <ta e="T249" id="Seg_4596" s="T248">nu-zaŋ</ta>
            <ta e="T250" id="Seg_4597" s="T249">bar</ta>
            <ta e="T251" id="Seg_4598" s="T250">salik-tə</ta>
            <ta e="T252" id="Seg_4599" s="T251">kand-laʔbə-ʔjə</ta>
            <ta e="T253" id="Seg_4600" s="T252">i</ta>
            <ta e="T254" id="Seg_4601" s="T253">salba</ta>
            <ta e="T255" id="Seg_4602" s="T254">barəʔ-laʔbə-ʔjə</ta>
            <ta e="T256" id="Seg_4603" s="T255">i</ta>
            <ta e="T257" id="Seg_4604" s="T256">kola</ta>
            <ta e="T259" id="Seg_4605" s="T258">dʼabə-laʔbə-ʔjə</ta>
            <ta e="T260" id="Seg_4606" s="T259">pi-ʔi</ta>
            <ta e="T261" id="Seg_4607" s="T260">bar</ta>
            <ta e="T262" id="Seg_4608" s="T261">šo-nə</ta>
            <ta e="T263" id="Seg_4609" s="T262">šödör-laʔbə-ʔjə</ta>
            <ta e="T264" id="Seg_4610" s="T263">i</ta>
            <ta e="T265" id="Seg_4611" s="T264">salba-nə</ta>
            <ta e="T266" id="Seg_4612" s="T265">sar-laʔbə-ʔjə</ta>
            <ta e="T267" id="Seg_4613" s="T266">dĭ</ta>
            <ta e="T268" id="Seg_4614" s="T267">kuda-nə</ta>
            <ta e="T269" id="Seg_4615" s="T268">bar</ta>
            <ta e="T270" id="Seg_4616" s="T269">ĭmbi</ta>
            <ta e="T271" id="Seg_4617" s="T270">det-leʔbə</ta>
            <ta e="T272" id="Seg_4618" s="T271">kudaj</ta>
            <ta e="T273" id="Seg_4619" s="T272">ilʼi</ta>
            <ta e="T274" id="Seg_4620" s="T273">šindi</ta>
            <ta e="T275" id="Seg_4621" s="T274">ej</ta>
            <ta e="T276" id="Seg_4622" s="T275">tĭmne-m</ta>
            <ta e="T277" id="Seg_4623" s="T276">sĭre-nə</ta>
            <ta e="T278" id="Seg_4624" s="T277">bar</ta>
            <ta e="T279" id="Seg_4625" s="T278">suʔmə-luʔ-pie-m</ta>
            <ta e="T280" id="Seg_4626" s="T279">pʼel</ta>
            <ta e="T281" id="Seg_4627" s="T280">dak</ta>
            <ta e="T282" id="Seg_4628" s="T281">bar</ta>
            <ta e="T697" id="Seg_4629" s="T282">kal-la</ta>
            <ta e="T283" id="Seg_4630" s="T697">dʼür-bie-m</ta>
            <ta e="T284" id="Seg_4631" s="T283">măn</ta>
            <ta e="T285" id="Seg_4632" s="T284">šonə-ga-m</ta>
            <ta e="T286" id="Seg_4633" s="T285">sĭre</ta>
            <ta e="T287" id="Seg_4634" s="T286">ugaːndə</ta>
            <ta e="T289" id="Seg_4635" s="T288">iʔgö</ta>
            <ta e="T290" id="Seg_4636" s="T289">iʔbo-laʔbə</ta>
            <ta e="T291" id="Seg_4637" s="T290">măn</ta>
            <ta e="T292" id="Seg_4638" s="T291">kam-bia-m</ta>
            <ta e="T293" id="Seg_4639" s="T292">i</ta>
            <ta e="T294" id="Seg_4640" s="T293">dĭbər</ta>
            <ta e="T295" id="Seg_4641" s="T294">suʔmə-luʔ-pie-m</ta>
            <ta e="T296" id="Seg_4642" s="T295">bar</ta>
            <ta e="T297" id="Seg_4643" s="T296">pʼel-də</ta>
            <ta e="T298" id="Seg_4644" s="T297">măn</ta>
            <ta e="T300" id="Seg_4645" s="T299">sĭʔitsə</ta>
            <ta e="T301" id="Seg_4646" s="T300">šo-bia-m</ta>
            <ta e="T302" id="Seg_4647" s="T301">dĭn</ta>
            <ta e="T303" id="Seg_4648" s="T302">bar</ta>
            <ta e="T304" id="Seg_4649" s="T303">ugaːndə</ta>
            <ta e="T305" id="Seg_4650" s="T304">balgaš</ta>
            <ta e="T307" id="Seg_4651" s="T306">iʔgö</ta>
            <ta e="T308" id="Seg_4652" s="T307">măn</ta>
            <ta e="T309" id="Seg_4653" s="T308">nu-bia-m</ta>
            <ta e="T310" id="Seg_4654" s="T309">i</ta>
            <ta e="T311" id="Seg_4655" s="T310">dibər</ta>
            <ta e="T312" id="Seg_4656" s="T311">saʔmə-luʔ-pia-m</ta>
            <ta e="T313" id="Seg_4657" s="T312">bar</ta>
            <ta e="T315" id="Seg_4658" s="T314">uda-zaŋ-də</ta>
            <ta e="T318" id="Seg_4659" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_4660" s="T318">kočka-m</ta>
            <ta e="T320" id="Seg_4661" s="T319">uda-zʼiʔ</ta>
            <ta e="T321" id="Seg_4662" s="T320">dʼaʔ-pio-m</ta>
            <ta e="T322" id="Seg_4663" s="T321">i</ta>
            <ta e="T323" id="Seg_4664" s="T322">par-bia-m</ta>
            <ta e="T328" id="Seg_4665" s="T327">dĭ</ta>
            <ta e="T329" id="Seg_4666" s="T328">ine-ʔi</ta>
            <ta e="T330" id="Seg_4667" s="T329">bădə-bi-ʔi</ta>
            <ta e="T331" id="Seg_4668" s="T330">a</ta>
            <ta e="T332" id="Seg_4669" s="T331">dö</ta>
            <ta e="T333" id="Seg_4670" s="T332">ine-ʔi</ta>
            <ta e="T334" id="Seg_4671" s="T333">ej</ta>
            <ta e="T335" id="Seg_4672" s="T334">bădə-bi-ʔi</ta>
            <ta e="T336" id="Seg_4673" s="T335">tăn</ta>
            <ta e="T337" id="Seg_4674" s="T336">ular-əʔi</ta>
            <ta e="T338" id="Seg_4675" s="T337">mĭ-bie-l</ta>
            <ta e="T339" id="Seg_4676" s="T338">noʔ</ta>
            <ta e="T340" id="Seg_4677" s="T339">dĭ-zen</ta>
            <ta e="T341" id="Seg_4678" s="T340">am-bi-ʔi</ta>
            <ta e="T342" id="Seg_4679" s="T341">tüžöj-də</ta>
            <ta e="T343" id="Seg_4680" s="T342">mĭ-bie-l</ta>
            <ta e="T344" id="Seg_4681" s="T343">noʔ</ta>
            <ta e="T345" id="Seg_4682" s="T344">dĭ</ta>
            <ta e="T346" id="Seg_4683" s="T345">am-bi</ta>
            <ta e="T347" id="Seg_4684" s="T346">büzəj-nə</ta>
            <ta e="T348" id="Seg_4685" s="T347">padʼi</ta>
            <ta e="T349" id="Seg_4686" s="T348">ej</ta>
            <ta e="T350" id="Seg_4687" s="T349">mĭ-bie-l</ta>
            <ta e="T351" id="Seg_4688" s="T350">mĭ-bie-m</ta>
            <ta e="T352" id="Seg_4689" s="T351">bar</ta>
            <ta e="T353" id="Seg_4690" s="T352">am-bi-ʔi</ta>
            <ta e="T357" id="Seg_4691" s="T356">ešši-zi</ta>
            <ta e="T358" id="Seg_4692" s="T357">bădə-bio-l</ta>
            <ta e="T359" id="Seg_4693" s="T358">li</ta>
            <ta e="T360" id="Seg_4694" s="T359">dĭ-m</ta>
            <ta e="T361" id="Seg_4695" s="T360">bădə-bio-m</ta>
            <ta e="T362" id="Seg_4696" s="T361">i</ta>
            <ta e="T363" id="Seg_4697" s="T362">süt</ta>
            <ta e="T364" id="Seg_4698" s="T363">mĭ-bie-m</ta>
            <ta e="T365" id="Seg_4699" s="T364">i</ta>
            <ta e="T366" id="Seg_4700" s="T365">bü</ta>
            <ta e="T367" id="Seg_4701" s="T366">bĭʔ-pi</ta>
            <ta e="T368" id="Seg_4702" s="T367">šenap</ta>
            <ta e="T369" id="Seg_4703" s="T368">am-bi</ta>
            <ta e="T370" id="Seg_4704" s="T369">am-bi</ta>
            <ta e="T371" id="Seg_4705" s="T370">ešši</ta>
            <ta e="T372" id="Seg_4706" s="T371">ej</ta>
            <ta e="T373" id="Seg_4707" s="T372">am-nia</ta>
            <ta e="T374" id="Seg_4708" s="T373">ej</ta>
            <ta e="T375" id="Seg_4709" s="T374">šam-nia-l</ta>
            <ta e="T376" id="Seg_4710" s="T375">šenap</ta>
            <ta e="T377" id="Seg_4711" s="T376">nörbə-bie-l</ta>
            <ta e="T379" id="Seg_4712" s="T377">dĭ-zeŋ</ta>
            <ta e="T380" id="Seg_4713" s="T379">pa-ʔi</ta>
            <ta e="T381" id="Seg_4714" s="T380">baltu-zʼiʔ</ta>
            <ta e="T382" id="Seg_4715" s="T381">baltu-zʼiʔ</ta>
            <ta e="T383" id="Seg_4716" s="T382">a-bi-ʔi</ta>
            <ta e="T384" id="Seg_4717" s="T383">i</ta>
            <ta e="T385" id="Seg_4718" s="T384">dĭgəttə</ta>
            <ta e="T386" id="Seg_4719" s="T385">bü-nə</ta>
            <ta e="T387" id="Seg_4720" s="T386">kam-bi-ʔi</ta>
            <ta e="T388" id="Seg_4721" s="T387">i</ta>
            <ta e="T389" id="Seg_4722" s="T388">kola</ta>
            <ta e="T390" id="Seg_4723" s="T389">dʼaʔ-pi-ʔi</ta>
            <ta e="T391" id="Seg_4724" s="T390">dĭn</ta>
            <ta e="T394" id="Seg_4725" s="T393">bar</ta>
            <ta e="T395" id="Seg_4726" s="T394">onʼiʔ</ta>
            <ta e="T396" id="Seg_4727" s="T395">onʼiʔ-də</ta>
            <ta e="T397" id="Seg_4728" s="T396">sar-bi-ʔi</ta>
            <ta e="T398" id="Seg_4729" s="T397">i</ta>
            <ta e="T399" id="Seg_4730" s="T398">bü-nə</ta>
            <ta e="T400" id="Seg_4731" s="T399">kam-bi-ʔi</ta>
            <ta e="T402" id="Seg_4732" s="T401">băza-ʔ</ta>
            <ta e="T403" id="Seg_4733" s="T402">kadəl-də</ta>
            <ta e="T404" id="Seg_4734" s="T403">bü-zʼiʔ</ta>
            <ta e="T405" id="Seg_4735" s="T404">i</ta>
            <ta e="T406" id="Seg_4736" s="T405">kĭški-t</ta>
            <ta e="T407" id="Seg_4737" s="T406">sĭre</ta>
            <ta e="T408" id="Seg_4738" s="T407">mo-la-l</ta>
            <ta e="T409" id="Seg_4739" s="T408">kuvas</ta>
            <ta e="T410" id="Seg_4740" s="T409">mo-la-l</ta>
            <ta e="T411" id="Seg_4741" s="T410">šiʔ</ta>
            <ta e="T412" id="Seg_4742" s="T411">il</ta>
            <ta e="T414" id="Seg_4743" s="T413">gibər</ta>
            <ta e="T415" id="Seg_4744" s="T414">kandə-bi-ʔi</ta>
            <ta e="T416" id="Seg_4745" s="T415">Rɨbra-nə</ta>
            <ta e="T417" id="Seg_4746" s="T416">kandə-bi-ʔi</ta>
            <ta e="T420" id="Seg_4747" s="T419">iʔgö</ta>
            <ta e="T421" id="Seg_4748" s="T420">bura-gən</ta>
            <ta e="T422" id="Seg_4749" s="T421">bar</ta>
            <ta e="T423" id="Seg_4750" s="T422">albuga-ʔi</ta>
            <ta e="T424" id="Seg_4751" s="T423">bar</ta>
            <ta e="T425" id="Seg_4752" s="T424">sar-bi</ta>
            <ta e="T426" id="Seg_4753" s="T425">teʔme-zi</ta>
            <ta e="T427" id="Seg_4754" s="T426">i</ta>
            <ta e="T428" id="Seg_4755" s="T427">deʔ-pi</ta>
            <ta e="T429" id="Seg_4756" s="T428">maʔ-ndə</ta>
            <ta e="T430" id="Seg_4757" s="T429">dĭ</ta>
            <ta e="T431" id="Seg_4758" s="T430">kuza</ta>
            <ta e="T432" id="Seg_4759" s="T431">bar</ta>
            <ta e="T433" id="Seg_4760" s="T432">togonor-lia</ta>
            <ta e="T434" id="Seg_4761" s="T433">togonor-lia</ta>
            <ta e="T435" id="Seg_4762" s="T434">a</ta>
            <ta e="T436" id="Seg_4763" s="T435">ĭmbi=də</ta>
            <ta e="T437" id="Seg_4764" s="T436">naga</ta>
            <ta e="T438" id="Seg_4765" s="T437">a</ta>
            <ta e="T439" id="Seg_4766" s="T438">dö</ta>
            <ta e="T440" id="Seg_4767" s="T439">kuza</ta>
            <ta e="T441" id="Seg_4768" s="T440">bar</ta>
            <ta e="T442" id="Seg_4769" s="T441">ej</ta>
            <ta e="T443" id="Seg_4770" s="T442">tăŋ</ta>
            <ta e="T444" id="Seg_4771" s="T443">togonor-lia</ta>
            <ta e="T445" id="Seg_4772" s="T444">bar</ta>
            <ta e="T446" id="Seg_4773" s="T445">ĭmbi</ta>
            <ta e="T447" id="Seg_4774" s="T446">i-ge</ta>
            <ta e="T450" id="Seg_4775" s="T449">i-ʔ</ta>
            <ta e="T451" id="Seg_4776" s="T450">iʔbo-ʔ</ta>
            <ta e="T452" id="Seg_4777" s="T451">uʔbda-ʔ</ta>
            <ta e="T453" id="Seg_4778" s="T452">da</ta>
            <ta e="T454" id="Seg_4779" s="T453">togonor-a-ʔ</ta>
            <ta e="T455" id="Seg_4780" s="T454">ĭmbi</ta>
            <ta e="T456" id="Seg_4781" s="T455">iʔbo-zittə</ta>
            <ta e="T457" id="Seg_4782" s="T456">iʔgö</ta>
            <ta e="T458" id="Seg_4783" s="T457">pʼe</ta>
            <ta e="T698" id="Seg_4784" s="T458">kal-la</ta>
            <ta e="T459" id="Seg_4785" s="T698">dʼür-bi-ʔi</ta>
            <ta e="T460" id="Seg_4786" s="T459">šo-bi-ʔi</ta>
            <ta e="T461" id="Seg_4787" s="T460">kömə</ta>
            <ta e="T462" id="Seg_4788" s="T461">sĭre-ʔi</ta>
            <ta e="T463" id="Seg_4789" s="T462">šo-bi-ʔi</ta>
            <ta e="T464" id="Seg_4790" s="T463">a</ta>
            <ta e="T465" id="Seg_4791" s="T464">il</ta>
            <ta e="T466" id="Seg_4792" s="T465">ugandə</ta>
            <ta e="T467" id="Seg_4793" s="T466">pim-nie-ʔi</ta>
            <ta e="T468" id="Seg_4794" s="T467">il</ta>
            <ta e="T470" id="Seg_4795" s="T469">bar</ta>
            <ta e="T471" id="Seg_4796" s="T470">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T472" id="Seg_4797" s="T471">dʼije-nə</ta>
            <ta e="T473" id="Seg_4798" s="T472">ugandə</ta>
            <ta e="T474" id="Seg_4799" s="T473">pim-bi-ʔi</ta>
            <ta e="T475" id="Seg_4800" s="T474">a</ta>
            <ta e="T476" id="Seg_4801" s="T475">kamən</ta>
            <ta e="T477" id="Seg_4802" s="T476">šo-lə-ʔi</ta>
            <ta e="T478" id="Seg_4803" s="T477">maʔ-nə</ta>
            <ta e="T479" id="Seg_4804" s="T478">dĭ-zeŋ</ta>
            <ta e="T480" id="Seg_4805" s="T479">bar</ta>
            <ta e="T482" id="Seg_4806" s="T481">šo-bi-ʔi</ta>
            <ta e="T483" id="Seg_4807" s="T482">dĭ-zeŋ</ta>
            <ta e="T484" id="Seg_4808" s="T483">bar</ta>
            <ta e="T485" id="Seg_4809" s="T484">šo-bi-ʔi</ta>
            <ta e="T486" id="Seg_4810" s="T485">sĭre</ta>
            <ta e="T487" id="Seg_4811" s="T486">il</ta>
            <ta e="T488" id="Seg_4812" s="T487">mĭʔ</ta>
            <ta e="T489" id="Seg_4813" s="T488">il-beʔ</ta>
            <ta e="T490" id="Seg_4814" s="T489">bar</ta>
            <ta e="T491" id="Seg_4815" s="T490">kömə-ʔi</ta>
            <ta e="T493" id="Seg_4816" s="T492">ajir-bi-ʔi</ta>
            <ta e="T495" id="Seg_4817" s="T494">dĭ-zeŋ-də</ta>
            <ta e="T496" id="Seg_4818" s="T495">ipek</ta>
            <ta e="T498" id="Seg_4819" s="T497">mĭ-bi-ʔi</ta>
            <ta e="T499" id="Seg_4820" s="T498">ĭmbi</ta>
            <ta e="T500" id="Seg_4821" s="T499">i-ge</ta>
            <ta e="T501" id="Seg_4822" s="T500">bar</ta>
            <ta e="T502" id="Seg_4823" s="T501">mĭ-bi-ʔi</ta>
            <ta e="T503" id="Seg_4824" s="T502">tus</ta>
            <ta e="T504" id="Seg_4825" s="T503">aspaʔ</ta>
            <ta e="T505" id="Seg_4826" s="T504">mĭ-bi-ʔi</ta>
            <ta e="T506" id="Seg_4827" s="T505">iššo</ta>
            <ta e="T507" id="Seg_4828" s="T506">toltanoʔ</ta>
            <ta e="T508" id="Seg_4829" s="T507">dĭ-zeŋ-də</ta>
            <ta e="T509" id="Seg_4830" s="T508">mĭ-bi-ʔi</ta>
            <ta e="T510" id="Seg_4831" s="T509">kömə-ʔi</ta>
            <ta e="T511" id="Seg_4832" s="T510">bar</ta>
            <ta e="T512" id="Seg_4833" s="T511">dʼije-gən</ta>
            <ta e="T513" id="Seg_4834" s="T512">amno-bi-ʔi</ta>
            <ta e="T514" id="Seg_4835" s="T513">a</ta>
            <ta e="T515" id="Seg_4836" s="T514">nüdʼi-n</ta>
            <ta e="T516" id="Seg_4837" s="T515">döbər</ta>
            <ta e="T517" id="Seg_4838" s="T516">šo-bi-ʔi</ta>
            <ta e="T518" id="Seg_4839" s="T517">amor-zittə</ta>
            <ta e="T520" id="Seg_4840" s="T519">onʼiʔ</ta>
            <ta e="T522" id="Seg_4841" s="T521">šo-bi-ʔi</ta>
            <ta e="T523" id="Seg_4842" s="T522">šide</ta>
            <ta e="T525" id="Seg_4843" s="T524">kömə-ʔi</ta>
            <ta e="T526" id="Seg_4844" s="T525">multuk-siʔ</ta>
            <ta e="T528" id="Seg_4845" s="T527">onʼiʔ-də</ta>
            <ta e="T529" id="Seg_4846" s="T528">bar</ta>
            <ta e="T531" id="Seg_4847" s="T530">ej</ta>
            <ta e="T532" id="Seg_4848" s="T531">kuʔ-pi-ʔi</ta>
            <ta e="T533" id="Seg_4849" s="T532">nüke</ta>
            <ta e="T534" id="Seg_4850" s="T533">miʔnʼibeʔ</ta>
            <ta e="T535" id="Seg_4851" s="T534">šonə-ga</ta>
            <ta e="T536" id="Seg_4852" s="T535">šiʔ</ta>
            <ta e="T537" id="Seg_4853" s="T536">bar</ta>
            <ta e="T538" id="Seg_4854" s="T537">dön</ta>
            <ta e="T539" id="Seg_4855" s="T538">dʼăbaktər-lia-laʔ</ta>
            <ta e="T540" id="Seg_4856" s="T539">a</ta>
            <ta e="T541" id="Seg_4857" s="T540">kuza</ta>
            <ta e="T542" id="Seg_4858" s="T541">kuznek-kən</ta>
            <ta e="T543" id="Seg_4859" s="T542">nu-ga</ta>
            <ta e="T544" id="Seg_4860" s="T543">nʼilgö-laʔbə</ta>
            <ta e="T545" id="Seg_4861" s="T544">miʔ</ta>
            <ta e="T546" id="Seg_4862" s="T545">bar</ta>
            <ta e="T550" id="Seg_4863" s="T549">onʼiʔ</ta>
            <ta e="T552" id="Seg_4864" s="T551">nüdʼi-n</ta>
            <ta e="T553" id="Seg_4865" s="T552">šo-bi-ʔi</ta>
            <ta e="T554" id="Seg_4866" s="T553">kömə-ʔi</ta>
            <ta e="T555" id="Seg_4867" s="T554">kuza</ta>
            <ta e="T556" id="Seg_4868" s="T555">deʔ-pi-ʔi</ta>
            <ta e="T557" id="Seg_4869" s="T556">ajə-ndə</ta>
            <ta e="T558" id="Seg_4870" s="T557">to-ndə</ta>
            <ta e="T559" id="Seg_4871" s="T558">kuʔ-pi-ʔi</ta>
            <ta e="T560" id="Seg_4872" s="T559">dĭgəttə</ta>
            <ta e="T561" id="Seg_4873" s="T560">bar</ta>
            <ta e="T562" id="Seg_4874" s="T561">tibi-zeŋ</ta>
            <ta e="T563" id="Seg_4875" s="T562">oʔbdə-bi-ʔi</ta>
            <ta e="T564" id="Seg_4876" s="T563">münör-bi-ʔi</ta>
            <ta e="T565" id="Seg_4877" s="T564">bar</ta>
            <ta e="T566" id="Seg_4878" s="T565">plʼotka-ʔi-zi</ta>
            <ta e="T567" id="Seg_4879" s="T566">onʼiʔ</ta>
            <ta e="T568" id="Seg_4880" s="T567">kuza</ta>
            <ta e="T569" id="Seg_4881" s="T568">bar</ta>
            <ta e="T570" id="Seg_4882" s="T569">piʔmə-ndə</ta>
            <ta e="T571" id="Seg_4883" s="T570">bar</ta>
            <ta e="T572" id="Seg_4884" s="T571">tüʔ-pi</ta>
            <ta e="T573" id="Seg_4885" s="T572">măn</ta>
            <ta e="T574" id="Seg_4886" s="T573">aba-m</ta>
            <ta e="T575" id="Seg_4887" s="T574">kăštə-bi-ʔi</ta>
            <ta e="T576" id="Seg_4888" s="T575">Zaxar</ta>
            <ta e="T577" id="Seg_4889" s="T576">a</ta>
            <ta e="T579" id="Seg_4890" s="T578">numəj-luʔ-pi-ʔi</ta>
            <ta e="T580" id="Seg_4891" s="T579">Stepanovič</ta>
            <ta e="T581" id="Seg_4892" s="T580">a</ta>
            <ta e="T582" id="Seg_4893" s="T581">ia-m</ta>
            <ta e="T583" id="Seg_4894" s="T582">Afanasʼa</ta>
            <ta e="T584" id="Seg_4895" s="T583">numəj-luʔ-pi-ʔi</ta>
            <ta e="T585" id="Seg_4896" s="T584">Antonovna</ta>
            <ta e="T586" id="Seg_4897" s="T585">a</ta>
            <ta e="T587" id="Seg_4898" s="T586">familija</ta>
            <ta e="T588" id="Seg_4899" s="T587">dĭ-zen</ta>
            <ta e="T589" id="Seg_4900" s="T588">i-bi</ta>
            <ta e="T590" id="Seg_4901" s="T589">Andʼigatə-ʔi</ta>
            <ta e="T591" id="Seg_4902" s="T590">es-seŋ-də</ta>
            <ta e="T592" id="Seg_4903" s="T591">i-bi</ta>
            <ta e="T594" id="Seg_4904" s="T593">oʔb</ta>
            <ta e="T595" id="Seg_4905" s="T594">šide</ta>
            <ta e="T596" id="Seg_4906" s="T595">nagur</ta>
            <ta e="T597" id="Seg_4907" s="T596">teʔtə</ta>
            <ta e="T598" id="Seg_4908" s="T597">sumna</ta>
            <ta e="T599" id="Seg_4909" s="T598">sejʔpü</ta>
            <ta e="T600" id="Seg_4910" s="T599">šĭnteʔtə</ta>
            <ta e="T602" id="Seg_4911" s="T600">amitun</ta>
            <ta e="T604" id="Seg_4912" s="T603">urgo</ta>
            <ta e="T605" id="Seg_4913" s="T604">kăštə-bi-ʔi</ta>
            <ta e="T606" id="Seg_4914" s="T605">Lena</ta>
            <ta e="T607" id="Seg_4915" s="T606">a</ta>
            <ta e="T608" id="Seg_4916" s="T607">nʼi-bə</ta>
            <ta e="T610" id="Seg_4917" s="T609">kăštə-bi-ʔi</ta>
            <ta e="T611" id="Seg_4918" s="T610">Djoma</ta>
            <ta e="T612" id="Seg_4919" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_4920" s="T612">măn</ta>
            <ta e="T614" id="Seg_4921" s="T613">măn</ta>
            <ta e="T616" id="Seg_4922" s="T615">măna</ta>
            <ta e="T617" id="Seg_4923" s="T616">kăštə-bi-ʔi</ta>
            <ta e="T618" id="Seg_4924" s="T617">Klawdʼeja</ta>
            <ta e="T619" id="Seg_4925" s="T618">măn</ta>
            <ta e="T620" id="Seg_4926" s="T619">măn</ta>
            <ta e="T621" id="Seg_4927" s="T620">sʼestra-m</ta>
            <ta e="T622" id="Seg_4928" s="T621">kăštə-bi-ʔi</ta>
            <ta e="T623" id="Seg_4929" s="T622">Nadja</ta>
            <ta e="T624" id="Seg_4930" s="T623">dĭgəttə</ta>
            <ta e="T625" id="Seg_4931" s="T624">i-bi</ta>
            <ta e="T626" id="Seg_4932" s="T625">Ăprosʼa</ta>
            <ta e="T627" id="Seg_4933" s="T626">dĭgəttə</ta>
            <ta e="T628" id="Seg_4934" s="T627">i-bi</ta>
            <ta e="T629" id="Seg_4935" s="T628">Vera</ta>
            <ta e="T630" id="Seg_4936" s="T629">dĭgəttə</ta>
            <ta e="T631" id="Seg_4937" s="T630">Manja</ta>
            <ta e="T632" id="Seg_4938" s="T631">i-bi</ta>
            <ta e="T633" id="Seg_4939" s="T632">dĭgəttə</ta>
            <ta e="T634" id="Seg_4940" s="T633">nʼi</ta>
            <ta e="T635" id="Seg_4941" s="T634">šo-bi</ta>
            <ta e="T638" id="Seg_4942" s="T637">kăštə-bi-ʔi</ta>
            <ta e="T639" id="Seg_4943" s="T638">Maksim</ta>
            <ta e="T640" id="Seg_4944" s="T639">onʼiʔ</ta>
            <ta e="T641" id="Seg_4945" s="T640">nʼi</ta>
            <ta e="T642" id="Seg_4946" s="T641">kü-laːm-bi</ta>
            <ta e="T643" id="Seg_4947" s="T642">măna</ta>
            <ta e="T644" id="Seg_4948" s="T643">iššo</ta>
            <ta e="T645" id="Seg_4949" s="T644">naga</ta>
            <ta e="T646" id="Seg_4950" s="T645">i-bi</ta>
            <ta e="T647" id="Seg_4951" s="T646">a</ta>
            <ta e="T648" id="Seg_4952" s="T647">a</ta>
            <ta e="T649" id="Seg_4953" s="T648">koʔbdo</ta>
            <ta e="T650" id="Seg_4954" s="T649">kü-laːm-bi</ta>
            <ta e="T651" id="Seg_4955" s="T650">oʔb</ta>
            <ta e="T652" id="Seg_4956" s="T651">šide</ta>
            <ta e="T653" id="Seg_4957" s="T652">nagur</ta>
            <ta e="T654" id="Seg_4958" s="T653">teʔtə</ta>
            <ta e="T655" id="Seg_4959" s="T654">sumna</ta>
            <ta e="T656" id="Seg_4960" s="T655">muktuʔ</ta>
            <ta e="T657" id="Seg_4961" s="T656">sejʔpü</ta>
            <ta e="T658" id="Seg_4962" s="T657">šĭnteʔtə</ta>
            <ta e="T659" id="Seg_4963" s="T658">amitun</ta>
            <ta e="T660" id="Seg_4964" s="T659">bʼeʔ</ta>
            <ta e="T661" id="Seg_4965" s="T660">bʼeʔ</ta>
            <ta e="T662" id="Seg_4966" s="T661">šide</ta>
            <ta e="T663" id="Seg_4967" s="T662">bʼeʔ</ta>
            <ta e="T664" id="Seg_4968" s="T663">onʼiʔ</ta>
            <ta e="T665" id="Seg_4969" s="T664">bʼeʔ</ta>
            <ta e="T666" id="Seg_4970" s="T665">nagur</ta>
            <ta e="T667" id="Seg_4971" s="T666">urgo</ta>
            <ta e="T668" id="Seg_4972" s="T667">koʔbdo</ta>
            <ta e="T669" id="Seg_4973" s="T668">kü-laːm-bi</ta>
            <ta e="T670" id="Seg_4974" s="T669">bʼeʔ</ta>
            <ta e="T671" id="Seg_4975" s="T670">nagur</ta>
            <ta e="T672" id="Seg_4976" s="T671">kö</ta>
            <ta e="T673" id="Seg_4977" s="T672">i-bi</ta>
            <ta e="T674" id="Seg_4978" s="T673">dĭʔ-nə</ta>
            <ta e="T675" id="Seg_4979" s="T674">a</ta>
            <ta e="T676" id="Seg_4980" s="T675">Ăprosʼa-m</ta>
            <ta e="T678" id="Seg_4981" s="T676">kuʔ-pi</ta>
            <ta e="T679" id="Seg_4982" s="T678">dĭ-n</ta>
            <ta e="T680" id="Seg_4983" s="T679">tibi</ta>
            <ta e="T681" id="Seg_4984" s="T680">kuʔ-pi</ta>
            <ta e="T682" id="Seg_4985" s="T681">dĭ-m</ta>
            <ta e="T683" id="Seg_4986" s="T682">Nadja</ta>
            <ta e="T684" id="Seg_4987" s="T683">kü-laːm-bi</ta>
            <ta e="T685" id="Seg_4988" s="T684">sumna</ta>
            <ta e="T686" id="Seg_4989" s="T685">ki</ta>
            <ta e="T699" id="Seg_4990" s="T686">kal-la</ta>
            <ta e="T687" id="Seg_4991" s="T699">dʼür-bi-ʔi</ta>
            <ta e="T688" id="Seg_4992" s="T687">a</ta>
            <ta e="T689" id="Seg_4993" s="T688">miʔ</ta>
            <ta e="T690" id="Seg_4994" s="T689">šide-göʔ</ta>
            <ta e="T691" id="Seg_4995" s="T690">ma-luʔ-pi-baʔ</ta>
            <ta e="T692" id="Seg_4996" s="T691">Klawdʼa</ta>
            <ta e="T693" id="Seg_4997" s="T692">i</ta>
            <ta e="T694" id="Seg_4998" s="T693">Maksim</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T6" id="Seg_4999" s="T5">măndo-r-ə-ʔ</ta>
            <ta e="T8" id="Seg_5000" s="T7">beške-jəʔ</ta>
            <ta e="T9" id="Seg_5001" s="T8">ku-liA-l</ta>
            <ta e="T10" id="Seg_5002" s="T9">bar</ta>
            <ta e="T11" id="Seg_5003" s="T10">nu-gA-jəʔ</ta>
            <ta e="T12" id="Seg_5004" s="T11">dĭn</ta>
            <ta e="T13" id="Seg_5005" s="T12">dĭbər</ta>
            <ta e="T14" id="Seg_5006" s="T13">kan-ə-ʔ</ta>
            <ta e="T15" id="Seg_5007" s="T14">a</ta>
            <ta e="T16" id="Seg_5008" s="T15">măn</ta>
            <ta e="T17" id="Seg_5009" s="T16">döbər</ta>
            <ta e="T18" id="Seg_5010" s="T17">kan-lV-m</ta>
            <ta e="T19" id="Seg_5011" s="T18">nadə</ta>
            <ta e="T20" id="Seg_5012" s="T19">iʔgö</ta>
            <ta e="T21" id="Seg_5013" s="T20">oʔbdə-zittə</ta>
            <ta e="T22" id="Seg_5014" s="T21">beške-jəʔ</ta>
            <ta e="T28" id="Seg_5015" s="T27">pa-jəʔ-n</ta>
            <ta e="T29" id="Seg_5016" s="T28">toʔ-gəndə</ta>
            <ta e="T30" id="Seg_5017" s="T29">măndo-r-ə-ʔ</ta>
            <ta e="T31" id="Seg_5018" s="T30">jakšə-ŋ</ta>
            <ta e="T32" id="Seg_5019" s="T31">e-ʔ</ta>
            <ta e="T33" id="Seg_5020" s="T32">dʼabə-t</ta>
            <ta e="T34" id="Seg_5021" s="T33">našto</ta>
            <ta e="T35" id="Seg_5022" s="T34">dʼabə-bi-l</ta>
            <ta e="T37" id="Seg_5023" s="T36">öʔlu-t</ta>
            <ta e="T38" id="Seg_5024" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_5025" s="T38">il</ta>
            <ta e="T40" id="Seg_5026" s="T39">šo-bi-jəʔ</ta>
            <ta e="T41" id="Seg_5027" s="T40">miʔnʼibeʔ</ta>
            <ta e="T44" id="Seg_5028" s="T43">bos-də</ta>
            <ta e="T45" id="Seg_5029" s="T44">koŋ</ta>
            <ta e="T46" id="Seg_5030" s="T45">nuldə-laʔbə-jəʔ</ta>
            <ta e="T47" id="Seg_5031" s="T46">dĭ-zAŋ</ta>
            <ta e="T48" id="Seg_5032" s="T47">tĭmne-jəʔ</ta>
            <ta e="T49" id="Seg_5033" s="T48">sazən</ta>
            <ta e="T51" id="Seg_5034" s="T50">pʼaŋdə-zittə</ta>
            <ta e="T52" id="Seg_5035" s="T51">a</ta>
            <ta e="T53" id="Seg_5036" s="T52">miʔ</ta>
            <ta e="T54" id="Seg_5037" s="T53">ej</ta>
            <ta e="T55" id="Seg_5038" s="T54">tĭmne-bAʔ</ta>
            <ta e="T56" id="Seg_5039" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_5040" s="T56">dĭ-zAŋ</ta>
            <ta e="T58" id="Seg_5041" s="T57">bar</ta>
            <ta e="T59" id="Seg_5042" s="T58">bos-də</ta>
            <ta e="T60" id="Seg_5043" s="T59">koŋ-zAŋ-də</ta>
            <ta e="T61" id="Seg_5044" s="T60">nu-bi-jəʔ</ta>
            <ta e="T62" id="Seg_5045" s="T61">tüj</ta>
            <ta e="T63" id="Seg_5046" s="T62">miʔ</ta>
            <ta e="T64" id="Seg_5047" s="T63">bar</ta>
            <ta e="T65" id="Seg_5048" s="T64">kazak</ta>
            <ta e="T66" id="Seg_5049" s="T65">šĭkə</ta>
            <ta e="T67" id="Seg_5050" s="T66">tʼăbaktər-laʔbə-bAʔ</ta>
            <ta e="T68" id="Seg_5051" s="T67">kazak-n</ta>
            <ta e="T69" id="Seg_5052" s="T68">šĭkə-t</ta>
            <ta e="T70" id="Seg_5053" s="T69">tüšəl-laʔbə-bAʔ</ta>
            <ta e="T71" id="Seg_5054" s="T70">kădaʔ</ta>
            <ta e="T72" id="Seg_5055" s="T71">tănan</ta>
            <ta e="T73" id="Seg_5056" s="T72">kăštə-liA-jəʔ</ta>
            <ta e="T74" id="Seg_5057" s="T73">i</ta>
            <ta e="T75" id="Seg_5058" s="T74">kădaʔ</ta>
            <ta e="T76" id="Seg_5059" s="T75">numəj-liA-jəʔ</ta>
            <ta e="T77" id="Seg_5060" s="T76">măna</ta>
            <ta e="T78" id="Seg_5061" s="T77">kăštə-liA-jəʔ</ta>
            <ta e="T79" id="Seg_5062" s="T78">Klawdʼa</ta>
            <ta e="T81" id="Seg_5063" s="T79">a</ta>
            <ta e="T82" id="Seg_5064" s="T81">a</ta>
            <ta e="T83" id="Seg_5065" s="T82">numəj-liA-jəʔ</ta>
            <ta e="T85" id="Seg_5066" s="T84">kădaʔ</ta>
            <ta e="T86" id="Seg_5067" s="T85">šo-bi-l</ta>
            <ta e="T87" id="Seg_5068" s="T86">döbər</ta>
            <ta e="T88" id="Seg_5069" s="T87">dö</ta>
            <ta e="T89" id="Seg_5070" s="T88">măn</ta>
            <ta e="T90" id="Seg_5071" s="T89">koŋ</ta>
            <ta e="T91" id="Seg_5072" s="T90">a</ta>
            <ta e="T92" id="Seg_5073" s="T91">dö</ta>
            <ta e="T93" id="Seg_5074" s="T92">tʼăbaktər-zittə</ta>
            <ta e="T95" id="Seg_5075" s="T94">koŋ</ta>
            <ta e="T96" id="Seg_5076" s="T95">a</ta>
            <ta e="T97" id="Seg_5077" s="T96">măn</ta>
            <ta e="T98" id="Seg_5078" s="T97">dĭ-zAŋ</ta>
            <ta e="T99" id="Seg_5079" s="T98">tüšə-lə-liA-m</ta>
            <ta e="T100" id="Seg_5080" s="T99">tăn</ta>
            <ta e="T101" id="Seg_5081" s="T100">tibi</ta>
            <ta e="T102" id="Seg_5082" s="T101">gibər</ta>
            <ta e="T696" id="Seg_5083" s="T102">kan-lAʔ</ta>
            <ta e="T103" id="Seg_5084" s="T696">tʼür-bi</ta>
            <ta e="T104" id="Seg_5085" s="T103">kazak-n</ta>
            <ta e="T105" id="Seg_5086" s="T104">tura-Tə</ta>
            <ta e="T106" id="Seg_5087" s="T105">măn</ta>
            <ta e="T107" id="Seg_5088" s="T106">iʔgö</ta>
            <ta e="T108" id="Seg_5089" s="T107">doʔ-bi-l</ta>
            <ta e="T109" id="Seg_5090" s="T108">tene-luʔbdə-bi-m</ta>
            <ta e="T110" id="Seg_5091" s="T109">tʼăbaktər-zittə</ta>
            <ta e="T111" id="Seg_5092" s="T110">măn</ta>
            <ta e="T112" id="Seg_5093" s="T111">šoška-m</ta>
            <ta e="T113" id="Seg_5094" s="T112">kü-laːm-bi</ta>
            <ta e="T114" id="Seg_5095" s="T113">măn</ta>
            <ta e="T115" id="Seg_5096" s="T114">tʼo</ta>
            <ta e="T116" id="Seg_5097" s="T115">tĭl-bi-m</ta>
            <ta e="T117" id="Seg_5098" s="T116">i</ta>
            <ta e="T118" id="Seg_5099" s="T117">dĭbər</ta>
            <ta e="T120" id="Seg_5100" s="T119">dĭ-m</ta>
            <ta e="T121" id="Seg_5101" s="T120">hen-bi-m</ta>
            <ta e="T122" id="Seg_5102" s="T121">i</ta>
            <ta e="T123" id="Seg_5103" s="T122">tʼo-ziʔ</ta>
            <ta e="T124" id="Seg_5104" s="T123">kămnə-bi-m</ta>
            <ta e="T126" id="Seg_5105" s="T125">miʔ</ta>
            <ta e="T127" id="Seg_5106" s="T126">bar</ta>
            <ta e="T130" id="Seg_5107" s="T129">i-bi</ta>
            <ta e="T131" id="Seg_5108" s="T130">dĭn</ta>
            <ta e="T132" id="Seg_5109" s="T131">iʔgö</ta>
            <ta e="T133" id="Seg_5110" s="T132">krospa-jəʔ</ta>
            <ta e="T138" id="Seg_5111" s="T137">nu-laʔbə-jəʔ</ta>
            <ta e="T139" id="Seg_5112" s="T138">a</ta>
            <ta e="T140" id="Seg_5113" s="T139">tüj</ta>
            <ta e="T142" id="Seg_5114" s="T140">onʼiʔ=da</ta>
            <ta e="T143" id="Seg_5115" s="T142">naga</ta>
            <ta e="T144" id="Seg_5116" s="T143">ĭmbi=də</ta>
            <ta e="T147" id="Seg_5117" s="T146">dĭn</ta>
            <ta e="T148" id="Seg_5118" s="T147">naga</ta>
            <ta e="T149" id="Seg_5119" s="T148">tüj</ta>
            <ta e="T150" id="Seg_5120" s="T149">dĭn</ta>
            <ta e="T152" id="Seg_5121" s="T151">nu-laʔbə</ta>
            <ta e="T153" id="Seg_5122" s="T152">tüj</ta>
            <ta e="T154" id="Seg_5123" s="T153">miʔ</ta>
            <ta e="T155" id="Seg_5124" s="T154">toʔbdə</ta>
            <ta e="T157" id="Seg_5125" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_5126" s="T157">iʔgö</ta>
            <ta e="T159" id="Seg_5127" s="T158">krospa-jəʔ</ta>
            <ta e="T160" id="Seg_5128" s="T159">nu-gA-jəʔ</ta>
            <ta e="T161" id="Seg_5129" s="T160">tože</ta>
            <ta e="T162" id="Seg_5130" s="T161">tăn</ta>
            <ta e="T164" id="Seg_5131" s="T162">kudaj</ta>
            <ta e="T165" id="Seg_5132" s="T164">tăn</ta>
            <ta e="T166" id="Seg_5133" s="T165">kudaj</ta>
            <ta e="T167" id="Seg_5134" s="T166">kudaj</ta>
            <ta e="T168" id="Seg_5135" s="T167">e-ʔ</ta>
            <ta e="T169" id="Seg_5136" s="T168">ma-t</ta>
            <ta e="T170" id="Seg_5137" s="T169">măna</ta>
            <ta e="T171" id="Seg_5138" s="T170">e-ʔ</ta>
            <ta e="T172" id="Seg_5139" s="T171">ma-t</ta>
            <ta e="T173" id="Seg_5140" s="T172">măna</ta>
            <ta e="T174" id="Seg_5141" s="T173">e-ʔ</ta>
            <ta e="T175" id="Seg_5142" s="T174">barəʔ-t</ta>
            <ta e="T176" id="Seg_5143" s="T175">măna</ta>
            <ta e="T177" id="Seg_5144" s="T176">tăn</ta>
            <ta e="T178" id="Seg_5145" s="T177">uget</ta>
            <ta e="T179" id="Seg_5146" s="T178">măna</ta>
            <ta e="T181" id="Seg_5147" s="T180">sĭj-də</ta>
            <ta e="T182" id="Seg_5148" s="T181">sagəš-zəbi</ta>
            <ta e="T183" id="Seg_5149" s="T182">sĭj-də</ta>
            <ta e="T184" id="Seg_5150" s="T183">sagəš-zəbi</ta>
            <ta e="T185" id="Seg_5151" s="T184">tăn</ta>
            <ta e="T186" id="Seg_5152" s="T185">öʔlu-t</ta>
            <ta e="T187" id="Seg_5153" s="T186">măna</ta>
            <ta e="T189" id="Seg_5154" s="T188">sĭj-m</ta>
            <ta e="T191" id="Seg_5155" s="T190">sagəštə</ta>
            <ta e="T192" id="Seg_5156" s="T191">i-gA</ta>
            <ta e="T193" id="Seg_5157" s="T192">sĭj-m</ta>
            <ta e="T194" id="Seg_5158" s="T193">sagəštə</ta>
            <ta e="T195" id="Seg_5159" s="T194">i-gA</ta>
            <ta e="T196" id="Seg_5160" s="T195">tüšəl-lə-ʔ</ta>
            <ta e="T197" id="Seg_5161" s="T196">măna</ta>
            <ta e="T198" id="Seg_5162" s="T197">maktanər-zittə</ta>
            <ta e="T199" id="Seg_5163" s="T198">tăn-ziʔ</ta>
            <ta e="T200" id="Seg_5164" s="T199">tüšəl-lə-ʔ</ta>
            <ta e="T201" id="Seg_5165" s="T200">măna</ta>
            <ta e="T202" id="Seg_5166" s="T201">maktanər-zittə</ta>
            <ta e="T204" id="Seg_5167" s="T203">tăn-ziʔ</ta>
            <ta e="T205" id="Seg_5168" s="T204">tüšəl-lə-ʔ</ta>
            <ta e="T206" id="Seg_5169" s="T205">măna</ta>
            <ta e="T207" id="Seg_5170" s="T206">aʔtʼi-t</ta>
            <ta e="T208" id="Seg_5171" s="T207">tăn</ta>
            <ta e="T209" id="Seg_5172" s="T208">mĭn-zittə</ta>
            <ta e="T210" id="Seg_5173" s="T209">măn</ta>
            <ta e="T211" id="Seg_5174" s="T210">kaga-m</ta>
            <ta e="T213" id="Seg_5175" s="T212">bar</ta>
            <ta e="T214" id="Seg_5176" s="T213">bos-də</ta>
            <ta e="T215" id="Seg_5177" s="T214">tüšəl-bi</ta>
            <ta e="T216" id="Seg_5178" s="T215">pʼaŋdə-zittə</ta>
            <ta e="T217" id="Seg_5179" s="T216">dĭbər</ta>
            <ta e="T218" id="Seg_5180" s="T217">il-də</ta>
            <ta e="T221" id="Seg_5181" s="T220">mĭn-gA</ta>
            <ta e="T222" id="Seg_5182" s="T221">i</ta>
            <ta e="T223" id="Seg_5183" s="T222">ĭššo</ta>
            <ta e="T227" id="Seg_5184" s="T226">kan-lV-j</ta>
            <ta e="T228" id="Seg_5185" s="T227">dĭ-n</ta>
            <ta e="T229" id="Seg_5186" s="T228">šal-l</ta>
            <ta e="T230" id="Seg_5187" s="T229">mĭ-zittə</ta>
            <ta e="T231" id="Seg_5188" s="T230">a</ta>
            <ta e="T232" id="Seg_5189" s="T231">dĭ-n</ta>
            <ta e="T233" id="Seg_5190" s="T232">šal-l</ta>
            <ta e="T234" id="Seg_5191" s="T233">i-zittə</ta>
            <ta e="T235" id="Seg_5192" s="T234">nadə</ta>
            <ta e="T236" id="Seg_5193" s="T235">măna</ta>
            <ta e="T237" id="Seg_5194" s="T236">salba</ta>
            <ta e="T238" id="Seg_5195" s="T237">sar-zittə</ta>
            <ta e="T239" id="Seg_5196" s="T238">kola</ta>
            <ta e="T245" id="Seg_5197" s="T244">kola</ta>
            <ta e="T246" id="Seg_5198" s="T245">dʼabə-zittə</ta>
            <ta e="T248" id="Seg_5199" s="T247">dĭ-zAŋ</ta>
            <ta e="T249" id="Seg_5200" s="T248">nu-zAŋ</ta>
            <ta e="T250" id="Seg_5201" s="T249">bar</ta>
            <ta e="T251" id="Seg_5202" s="T250">salik-Tə</ta>
            <ta e="T252" id="Seg_5203" s="T251">kandə-laʔbə-jəʔ</ta>
            <ta e="T253" id="Seg_5204" s="T252">i</ta>
            <ta e="T254" id="Seg_5205" s="T253">salba</ta>
            <ta e="T255" id="Seg_5206" s="T254">barəʔ-laʔbə-jəʔ</ta>
            <ta e="T256" id="Seg_5207" s="T255">i</ta>
            <ta e="T257" id="Seg_5208" s="T256">kola</ta>
            <ta e="T259" id="Seg_5209" s="T258">dʼabə-laʔbə-jəʔ</ta>
            <ta e="T260" id="Seg_5210" s="T259">pi-jəʔ</ta>
            <ta e="T261" id="Seg_5211" s="T260">bar</ta>
            <ta e="T262" id="Seg_5212" s="T261">šo-Tə</ta>
            <ta e="T263" id="Seg_5213" s="T262">šödör-laʔbə-jəʔ</ta>
            <ta e="T264" id="Seg_5214" s="T263">i</ta>
            <ta e="T265" id="Seg_5215" s="T264">salba-Tə</ta>
            <ta e="T266" id="Seg_5216" s="T265">sar-laʔbə-jəʔ</ta>
            <ta e="T267" id="Seg_5217" s="T266">dĭ</ta>
            <ta e="T268" id="Seg_5218" s="T267">%%-Tə</ta>
            <ta e="T269" id="Seg_5219" s="T268">bar</ta>
            <ta e="T270" id="Seg_5220" s="T269">ĭmbi</ta>
            <ta e="T271" id="Seg_5221" s="T270">det-laʔbə</ta>
            <ta e="T272" id="Seg_5222" s="T271">kudaj</ta>
            <ta e="T273" id="Seg_5223" s="T272">ilʼi</ta>
            <ta e="T274" id="Seg_5224" s="T273">šində</ta>
            <ta e="T275" id="Seg_5225" s="T274">ej</ta>
            <ta e="T276" id="Seg_5226" s="T275">tĭmne-m</ta>
            <ta e="T277" id="Seg_5227" s="T276">sĭri-Tə</ta>
            <ta e="T278" id="Seg_5228" s="T277">bar</ta>
            <ta e="T279" id="Seg_5229" s="T278">süʔmə-luʔbdə-bi-m</ta>
            <ta e="T280" id="Seg_5230" s="T279">pʼel</ta>
            <ta e="T281" id="Seg_5231" s="T280">tak</ta>
            <ta e="T282" id="Seg_5232" s="T281">bar</ta>
            <ta e="T697" id="Seg_5233" s="T282">kan-lAʔ</ta>
            <ta e="T283" id="Seg_5234" s="T697">tʼür-bi-m</ta>
            <ta e="T284" id="Seg_5235" s="T283">măn</ta>
            <ta e="T285" id="Seg_5236" s="T284">šonə-gA-m</ta>
            <ta e="T286" id="Seg_5237" s="T285">sĭri</ta>
            <ta e="T287" id="Seg_5238" s="T286">ugaːndə</ta>
            <ta e="T289" id="Seg_5239" s="T288">iʔgö</ta>
            <ta e="T290" id="Seg_5240" s="T289">iʔbö-laʔbə</ta>
            <ta e="T291" id="Seg_5241" s="T290">măn</ta>
            <ta e="T292" id="Seg_5242" s="T291">kan-bi-m</ta>
            <ta e="T293" id="Seg_5243" s="T292">i</ta>
            <ta e="T294" id="Seg_5244" s="T293">dĭbər</ta>
            <ta e="T295" id="Seg_5245" s="T294">süʔmə-luʔbdə-bi-m</ta>
            <ta e="T296" id="Seg_5246" s="T295">bar</ta>
            <ta e="T297" id="Seg_5247" s="T296">pʼel-də</ta>
            <ta e="T298" id="Seg_5248" s="T297">măn</ta>
            <ta e="T300" id="Seg_5249" s="T299">sĭʔitsə</ta>
            <ta e="T301" id="Seg_5250" s="T300">šo-bi-m</ta>
            <ta e="T302" id="Seg_5251" s="T301">dĭn</ta>
            <ta e="T303" id="Seg_5252" s="T302">bar</ta>
            <ta e="T304" id="Seg_5253" s="T303">ugaːndə</ta>
            <ta e="T305" id="Seg_5254" s="T304">balgaš</ta>
            <ta e="T307" id="Seg_5255" s="T306">iʔgö</ta>
            <ta e="T308" id="Seg_5256" s="T307">măn</ta>
            <ta e="T309" id="Seg_5257" s="T308">nu-bi-m</ta>
            <ta e="T310" id="Seg_5258" s="T309">i</ta>
            <ta e="T311" id="Seg_5259" s="T310">dĭbər</ta>
            <ta e="T312" id="Seg_5260" s="T311">saʔmə-luʔbdə-bi-m</ta>
            <ta e="T313" id="Seg_5261" s="T312">bar</ta>
            <ta e="T315" id="Seg_5262" s="T314">uda-zAŋ-də</ta>
            <ta e="T318" id="Seg_5263" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_5264" s="T318">kočka-m</ta>
            <ta e="T320" id="Seg_5265" s="T319">uda-ziʔ</ta>
            <ta e="T321" id="Seg_5266" s="T320">dʼabə-bi-m</ta>
            <ta e="T322" id="Seg_5267" s="T321">i</ta>
            <ta e="T323" id="Seg_5268" s="T322">par-bi-m</ta>
            <ta e="T328" id="Seg_5269" s="T327">dĭ</ta>
            <ta e="T329" id="Seg_5270" s="T328">ine-jəʔ</ta>
            <ta e="T330" id="Seg_5271" s="T329">bădə-bi-jəʔ</ta>
            <ta e="T331" id="Seg_5272" s="T330">a</ta>
            <ta e="T332" id="Seg_5273" s="T331">dö</ta>
            <ta e="T333" id="Seg_5274" s="T332">ine-jəʔ</ta>
            <ta e="T334" id="Seg_5275" s="T333">ej</ta>
            <ta e="T335" id="Seg_5276" s="T334">bădə-bi-jəʔ</ta>
            <ta e="T336" id="Seg_5277" s="T335">tăn</ta>
            <ta e="T337" id="Seg_5278" s="T336">ular-jəʔ</ta>
            <ta e="T338" id="Seg_5279" s="T337">mĭ-bi-l</ta>
            <ta e="T339" id="Seg_5280" s="T338">noʔ</ta>
            <ta e="T340" id="Seg_5281" s="T339">dĭ-zen</ta>
            <ta e="T341" id="Seg_5282" s="T340">am-bi-jəʔ</ta>
            <ta e="T342" id="Seg_5283" s="T341">tüžöj-də</ta>
            <ta e="T343" id="Seg_5284" s="T342">mĭ-bi-l</ta>
            <ta e="T344" id="Seg_5285" s="T343">noʔ</ta>
            <ta e="T345" id="Seg_5286" s="T344">dĭ</ta>
            <ta e="T346" id="Seg_5287" s="T345">am-bi</ta>
            <ta e="T347" id="Seg_5288" s="T346">büzəj-Tə</ta>
            <ta e="T348" id="Seg_5289" s="T347">padʼi</ta>
            <ta e="T349" id="Seg_5290" s="T348">ej</ta>
            <ta e="T350" id="Seg_5291" s="T349">mĭ-bi-l</ta>
            <ta e="T351" id="Seg_5292" s="T350">mĭ-bi-m</ta>
            <ta e="T352" id="Seg_5293" s="T351">bar</ta>
            <ta e="T353" id="Seg_5294" s="T352">am-bi-jəʔ</ta>
            <ta e="T357" id="Seg_5295" s="T356">ešši-ziʔ</ta>
            <ta e="T358" id="Seg_5296" s="T357">bădə-bi-l</ta>
            <ta e="T359" id="Seg_5297" s="T358">li</ta>
            <ta e="T360" id="Seg_5298" s="T359">dĭ-m</ta>
            <ta e="T361" id="Seg_5299" s="T360">bădə-bi-m</ta>
            <ta e="T362" id="Seg_5300" s="T361">i</ta>
            <ta e="T363" id="Seg_5301" s="T362">süt</ta>
            <ta e="T364" id="Seg_5302" s="T363">mĭ-bi-m</ta>
            <ta e="T365" id="Seg_5303" s="T364">i</ta>
            <ta e="T366" id="Seg_5304" s="T365">bü</ta>
            <ta e="T367" id="Seg_5305" s="T366">bĭs-bi</ta>
            <ta e="T368" id="Seg_5306" s="T367">šenap</ta>
            <ta e="T369" id="Seg_5307" s="T368">am-bi</ta>
            <ta e="T370" id="Seg_5308" s="T369">am-bi</ta>
            <ta e="T371" id="Seg_5309" s="T370">ešši</ta>
            <ta e="T372" id="Seg_5310" s="T371">ej</ta>
            <ta e="T373" id="Seg_5311" s="T372">am-liA</ta>
            <ta e="T374" id="Seg_5312" s="T373">ej</ta>
            <ta e="T375" id="Seg_5313" s="T374">šʼaːm-liA-l</ta>
            <ta e="T376" id="Seg_5314" s="T375">šenap</ta>
            <ta e="T377" id="Seg_5315" s="T376">nörbə-bi-l</ta>
            <ta e="T379" id="Seg_5316" s="T377">dĭ-zAŋ</ta>
            <ta e="T380" id="Seg_5317" s="T379">pa-jəʔ</ta>
            <ta e="T381" id="Seg_5318" s="T380">baltu-ziʔ</ta>
            <ta e="T382" id="Seg_5319" s="T381">baltu-ziʔ</ta>
            <ta e="T383" id="Seg_5320" s="T382">a-bi-jəʔ</ta>
            <ta e="T384" id="Seg_5321" s="T383">i</ta>
            <ta e="T385" id="Seg_5322" s="T384">dĭgəttə</ta>
            <ta e="T386" id="Seg_5323" s="T385">bü-Tə</ta>
            <ta e="T387" id="Seg_5324" s="T386">kan-bi-jəʔ</ta>
            <ta e="T388" id="Seg_5325" s="T387">i</ta>
            <ta e="T389" id="Seg_5326" s="T388">kola</ta>
            <ta e="T390" id="Seg_5327" s="T389">dʼabə-bi-jəʔ</ta>
            <ta e="T391" id="Seg_5328" s="T390">dĭn</ta>
            <ta e="T394" id="Seg_5329" s="T393">bar</ta>
            <ta e="T395" id="Seg_5330" s="T394">onʼiʔ</ta>
            <ta e="T396" id="Seg_5331" s="T395">onʼiʔ-Tə</ta>
            <ta e="T397" id="Seg_5332" s="T396">sar-bi-jəʔ</ta>
            <ta e="T398" id="Seg_5333" s="T397">i</ta>
            <ta e="T399" id="Seg_5334" s="T398">bü-Tə</ta>
            <ta e="T400" id="Seg_5335" s="T399">kan-bi-jəʔ</ta>
            <ta e="T402" id="Seg_5336" s="T401">bazə-ʔ</ta>
            <ta e="T403" id="Seg_5337" s="T402">kadul-də</ta>
            <ta e="T404" id="Seg_5338" s="T403">bü-ziʔ</ta>
            <ta e="T405" id="Seg_5339" s="T404">i</ta>
            <ta e="T406" id="Seg_5340" s="T405">kĭškə-t</ta>
            <ta e="T407" id="Seg_5341" s="T406">sĭri</ta>
            <ta e="T408" id="Seg_5342" s="T407">mo-lV-l</ta>
            <ta e="T409" id="Seg_5343" s="T408">kuvas</ta>
            <ta e="T410" id="Seg_5344" s="T409">mo-lV-l</ta>
            <ta e="T411" id="Seg_5345" s="T410">šiʔ</ta>
            <ta e="T412" id="Seg_5346" s="T411">il</ta>
            <ta e="T414" id="Seg_5347" s="T413">gibər</ta>
            <ta e="T415" id="Seg_5348" s="T414">kandə-bi-jəʔ</ta>
            <ta e="T416" id="Seg_5349" s="T415">Rɨbra-Tə</ta>
            <ta e="T417" id="Seg_5350" s="T416">kandə-bi-jəʔ</ta>
            <ta e="T420" id="Seg_5351" s="T419">iʔgö</ta>
            <ta e="T421" id="Seg_5352" s="T420">băra-Kən</ta>
            <ta e="T422" id="Seg_5353" s="T421">bar</ta>
            <ta e="T423" id="Seg_5354" s="T422">albuga-jəʔ</ta>
            <ta e="T424" id="Seg_5355" s="T423">bar</ta>
            <ta e="T425" id="Seg_5356" s="T424">sar-bi</ta>
            <ta e="T426" id="Seg_5357" s="T425">teʔme-ziʔ</ta>
            <ta e="T427" id="Seg_5358" s="T426">i</ta>
            <ta e="T428" id="Seg_5359" s="T427">det-bi</ta>
            <ta e="T429" id="Seg_5360" s="T428">maʔ-gəndə</ta>
            <ta e="T430" id="Seg_5361" s="T429">dĭ</ta>
            <ta e="T431" id="Seg_5362" s="T430">kuza</ta>
            <ta e="T432" id="Seg_5363" s="T431">bar</ta>
            <ta e="T433" id="Seg_5364" s="T432">togonər-liA</ta>
            <ta e="T434" id="Seg_5365" s="T433">togonər-liA</ta>
            <ta e="T435" id="Seg_5366" s="T434">a</ta>
            <ta e="T436" id="Seg_5367" s="T435">ĭmbi=də</ta>
            <ta e="T437" id="Seg_5368" s="T436">naga</ta>
            <ta e="T438" id="Seg_5369" s="T437">a</ta>
            <ta e="T439" id="Seg_5370" s="T438">dö</ta>
            <ta e="T440" id="Seg_5371" s="T439">kuza</ta>
            <ta e="T441" id="Seg_5372" s="T440">bar</ta>
            <ta e="T442" id="Seg_5373" s="T441">ej</ta>
            <ta e="T443" id="Seg_5374" s="T442">tăŋ</ta>
            <ta e="T444" id="Seg_5375" s="T443">togonər-liA</ta>
            <ta e="T445" id="Seg_5376" s="T444">bar</ta>
            <ta e="T446" id="Seg_5377" s="T445">ĭmbi</ta>
            <ta e="T447" id="Seg_5378" s="T446">i-gA</ta>
            <ta e="T450" id="Seg_5379" s="T449">e-ʔ</ta>
            <ta e="T451" id="Seg_5380" s="T450">iʔbö-ʔ</ta>
            <ta e="T452" id="Seg_5381" s="T451">uʔbdə-ʔ</ta>
            <ta e="T453" id="Seg_5382" s="T452">da</ta>
            <ta e="T454" id="Seg_5383" s="T453">togonər-ə-ʔ</ta>
            <ta e="T455" id="Seg_5384" s="T454">ĭmbi</ta>
            <ta e="T456" id="Seg_5385" s="T455">iʔbö-zittə</ta>
            <ta e="T457" id="Seg_5386" s="T456">iʔgö</ta>
            <ta e="T458" id="Seg_5387" s="T457">pʼe</ta>
            <ta e="T698" id="Seg_5388" s="T458">kan-lAʔ</ta>
            <ta e="T459" id="Seg_5389" s="T698">tʼür-bi-jəʔ</ta>
            <ta e="T460" id="Seg_5390" s="T459">šo-bi-jəʔ</ta>
            <ta e="T461" id="Seg_5391" s="T460">kömə</ta>
            <ta e="T462" id="Seg_5392" s="T461">sĭri-jəʔ</ta>
            <ta e="T463" id="Seg_5393" s="T462">šo-bi-jəʔ</ta>
            <ta e="T464" id="Seg_5394" s="T463">a</ta>
            <ta e="T465" id="Seg_5395" s="T464">il</ta>
            <ta e="T466" id="Seg_5396" s="T465">ugaːndə</ta>
            <ta e="T467" id="Seg_5397" s="T466">pim-liA-jəʔ</ta>
            <ta e="T468" id="Seg_5398" s="T467">il</ta>
            <ta e="T470" id="Seg_5399" s="T469">bar</ta>
            <ta e="T471" id="Seg_5400" s="T470">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T472" id="Seg_5401" s="T471">dʼije-Tə</ta>
            <ta e="T473" id="Seg_5402" s="T472">ugaːndə</ta>
            <ta e="T474" id="Seg_5403" s="T473">pim-bi-jəʔ</ta>
            <ta e="T475" id="Seg_5404" s="T474">a</ta>
            <ta e="T476" id="Seg_5405" s="T475">kamən</ta>
            <ta e="T477" id="Seg_5406" s="T476">šo-lV-jəʔ</ta>
            <ta e="T478" id="Seg_5407" s="T477">maʔ-Tə</ta>
            <ta e="T479" id="Seg_5408" s="T478">dĭ-zAŋ</ta>
            <ta e="T480" id="Seg_5409" s="T479">bar</ta>
            <ta e="T482" id="Seg_5410" s="T481">šo-bi-jəʔ</ta>
            <ta e="T483" id="Seg_5411" s="T482">dĭ-zAŋ</ta>
            <ta e="T484" id="Seg_5412" s="T483">bar</ta>
            <ta e="T485" id="Seg_5413" s="T484">šo-bi-jəʔ</ta>
            <ta e="T486" id="Seg_5414" s="T485">sĭri</ta>
            <ta e="T487" id="Seg_5415" s="T486">il</ta>
            <ta e="T488" id="Seg_5416" s="T487">miʔ</ta>
            <ta e="T489" id="Seg_5417" s="T488">il-bAʔ</ta>
            <ta e="T490" id="Seg_5418" s="T489">bar</ta>
            <ta e="T491" id="Seg_5419" s="T490">kömə-jəʔ</ta>
            <ta e="T493" id="Seg_5420" s="T492">ajir-bi-jəʔ</ta>
            <ta e="T495" id="Seg_5421" s="T494">dĭ-zAŋ-Tə</ta>
            <ta e="T496" id="Seg_5422" s="T495">ipek</ta>
            <ta e="T498" id="Seg_5423" s="T497">mĭ-bi-jəʔ</ta>
            <ta e="T499" id="Seg_5424" s="T498">ĭmbi</ta>
            <ta e="T500" id="Seg_5425" s="T499">i-gA</ta>
            <ta e="T501" id="Seg_5426" s="T500">bar</ta>
            <ta e="T502" id="Seg_5427" s="T501">mĭ-bi-jəʔ</ta>
            <ta e="T503" id="Seg_5428" s="T502">tus</ta>
            <ta e="T504" id="Seg_5429" s="T503">aspaʔ</ta>
            <ta e="T505" id="Seg_5430" s="T504">mĭ-bi-jəʔ</ta>
            <ta e="T506" id="Seg_5431" s="T505">ĭššo</ta>
            <ta e="T507" id="Seg_5432" s="T506">toltano</ta>
            <ta e="T508" id="Seg_5433" s="T507">dĭ-zAŋ-Tə</ta>
            <ta e="T509" id="Seg_5434" s="T508">mĭ-bi-jəʔ</ta>
            <ta e="T510" id="Seg_5435" s="T509">kömə-jəʔ</ta>
            <ta e="T511" id="Seg_5436" s="T510">bar</ta>
            <ta e="T512" id="Seg_5437" s="T511">dʼije-Kən</ta>
            <ta e="T513" id="Seg_5438" s="T512">amno-bi-jəʔ</ta>
            <ta e="T514" id="Seg_5439" s="T513">a</ta>
            <ta e="T515" id="Seg_5440" s="T514">nüdʼi-n</ta>
            <ta e="T516" id="Seg_5441" s="T515">döbər</ta>
            <ta e="T517" id="Seg_5442" s="T516">šo-bi-jəʔ</ta>
            <ta e="T518" id="Seg_5443" s="T517">amor-zittə</ta>
            <ta e="T520" id="Seg_5444" s="T519">onʼiʔ</ta>
            <ta e="T522" id="Seg_5445" s="T521">šo-bi-jəʔ</ta>
            <ta e="T523" id="Seg_5446" s="T522">šide</ta>
            <ta e="T525" id="Seg_5447" s="T524">kömə-jəʔ</ta>
            <ta e="T526" id="Seg_5448" s="T525">multuk-ziʔ</ta>
            <ta e="T528" id="Seg_5449" s="T527">onʼiʔ-Tə</ta>
            <ta e="T529" id="Seg_5450" s="T528">bar</ta>
            <ta e="T531" id="Seg_5451" s="T530">ej</ta>
            <ta e="T532" id="Seg_5452" s="T531">kut-bi-jəʔ</ta>
            <ta e="T533" id="Seg_5453" s="T532">nüke</ta>
            <ta e="T534" id="Seg_5454" s="T533">miʔnʼibeʔ</ta>
            <ta e="T535" id="Seg_5455" s="T534">šonə-gA</ta>
            <ta e="T536" id="Seg_5456" s="T535">šiʔ</ta>
            <ta e="T537" id="Seg_5457" s="T536">bar</ta>
            <ta e="T538" id="Seg_5458" s="T537">dön</ta>
            <ta e="T539" id="Seg_5459" s="T538">tʼăbaktər-liA-lAʔ</ta>
            <ta e="T540" id="Seg_5460" s="T539">a</ta>
            <ta e="T541" id="Seg_5461" s="T540">kuza</ta>
            <ta e="T542" id="Seg_5462" s="T541">kuznek-Kən</ta>
            <ta e="T543" id="Seg_5463" s="T542">nu-gA</ta>
            <ta e="T544" id="Seg_5464" s="T543">nʼilgö-laʔbə</ta>
            <ta e="T545" id="Seg_5465" s="T544">miʔ</ta>
            <ta e="T546" id="Seg_5466" s="T545">bar</ta>
            <ta e="T550" id="Seg_5467" s="T549">onʼiʔ</ta>
            <ta e="T552" id="Seg_5468" s="T551">nüdʼi-n</ta>
            <ta e="T553" id="Seg_5469" s="T552">šo-bi-jəʔ</ta>
            <ta e="T554" id="Seg_5470" s="T553">kömə-jəʔ</ta>
            <ta e="T555" id="Seg_5471" s="T554">kuza</ta>
            <ta e="T556" id="Seg_5472" s="T555">det-bi-jəʔ</ta>
            <ta e="T557" id="Seg_5473" s="T556">ajə-ndə</ta>
            <ta e="T558" id="Seg_5474" s="T557">toʔ-gəndə</ta>
            <ta e="T559" id="Seg_5475" s="T558">kuʔ-bi-jəʔ</ta>
            <ta e="T560" id="Seg_5476" s="T559">dĭgəttə</ta>
            <ta e="T561" id="Seg_5477" s="T560">bar</ta>
            <ta e="T562" id="Seg_5478" s="T561">tibi-zAŋ</ta>
            <ta e="T563" id="Seg_5479" s="T562">oʔbdə-bi-jəʔ</ta>
            <ta e="T564" id="Seg_5480" s="T563">münör-bi-jəʔ</ta>
            <ta e="T565" id="Seg_5481" s="T564">bar</ta>
            <ta e="T566" id="Seg_5482" s="T565">plʼotka-jəʔ-ziʔ</ta>
            <ta e="T567" id="Seg_5483" s="T566">onʼiʔ</ta>
            <ta e="T568" id="Seg_5484" s="T567">kuza</ta>
            <ta e="T569" id="Seg_5485" s="T568">bar</ta>
            <ta e="T570" id="Seg_5486" s="T569">piʔme-gəndə</ta>
            <ta e="T571" id="Seg_5487" s="T570">bar</ta>
            <ta e="T572" id="Seg_5488" s="T571">tüʔ-bi</ta>
            <ta e="T573" id="Seg_5489" s="T572">măn</ta>
            <ta e="T574" id="Seg_5490" s="T573">aba-m</ta>
            <ta e="T575" id="Seg_5491" s="T574">kăštə-bi-jəʔ</ta>
            <ta e="T576" id="Seg_5492" s="T575">Zaxar</ta>
            <ta e="T577" id="Seg_5493" s="T576">a</ta>
            <ta e="T579" id="Seg_5494" s="T578">numəj-luʔbdə-bi-jəʔ</ta>
            <ta e="T580" id="Seg_5495" s="T579">Stepanovič</ta>
            <ta e="T581" id="Seg_5496" s="T580">a</ta>
            <ta e="T582" id="Seg_5497" s="T581">ija-m</ta>
            <ta e="T583" id="Seg_5498" s="T582">Afanasʼa</ta>
            <ta e="T584" id="Seg_5499" s="T583">numəj-luʔbdə-bi-jəʔ</ta>
            <ta e="T585" id="Seg_5500" s="T584">Anzonovna</ta>
            <ta e="T586" id="Seg_5501" s="T585">a</ta>
            <ta e="T587" id="Seg_5502" s="T586">familija</ta>
            <ta e="T588" id="Seg_5503" s="T587">dĭ-zen</ta>
            <ta e="T589" id="Seg_5504" s="T588">i-bi</ta>
            <ta e="T590" id="Seg_5505" s="T589">Andʼigatə-jəʔ</ta>
            <ta e="T591" id="Seg_5506" s="T590">ešši-zAŋ-də</ta>
            <ta e="T592" id="Seg_5507" s="T591">i-bi</ta>
            <ta e="T594" id="Seg_5508" s="T593">oʔb</ta>
            <ta e="T595" id="Seg_5509" s="T594">šide</ta>
            <ta e="T596" id="Seg_5510" s="T595">nagur</ta>
            <ta e="T597" id="Seg_5511" s="T596">teʔdə</ta>
            <ta e="T598" id="Seg_5512" s="T597">sumna</ta>
            <ta e="T599" id="Seg_5513" s="T598">sejʔpü</ta>
            <ta e="T600" id="Seg_5514" s="T599">šĭnteʔtə</ta>
            <ta e="T602" id="Seg_5515" s="T600">amitun</ta>
            <ta e="T604" id="Seg_5516" s="T603">urgo</ta>
            <ta e="T605" id="Seg_5517" s="T604">kăštə-bi-jəʔ</ta>
            <ta e="T606" id="Seg_5518" s="T605">Lena</ta>
            <ta e="T607" id="Seg_5519" s="T606">a</ta>
            <ta e="T608" id="Seg_5520" s="T607">nʼi-bə</ta>
            <ta e="T610" id="Seg_5521" s="T609">kăštə-bi-jəʔ</ta>
            <ta e="T611" id="Seg_5522" s="T610">Djoma</ta>
            <ta e="T612" id="Seg_5523" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_5524" s="T612">măn</ta>
            <ta e="T614" id="Seg_5525" s="T613">măn</ta>
            <ta e="T616" id="Seg_5526" s="T615">măna</ta>
            <ta e="T617" id="Seg_5527" s="T616">kăštə-bi-jəʔ</ta>
            <ta e="T618" id="Seg_5528" s="T617">Klawdʼa</ta>
            <ta e="T619" id="Seg_5529" s="T618">măn</ta>
            <ta e="T620" id="Seg_5530" s="T619">măn</ta>
            <ta e="T621" id="Seg_5531" s="T620">sʼestra-m</ta>
            <ta e="T622" id="Seg_5532" s="T621">kăštə-bi-jəʔ</ta>
            <ta e="T623" id="Seg_5533" s="T622">Nadja</ta>
            <ta e="T624" id="Seg_5534" s="T623">dĭgəttə</ta>
            <ta e="T625" id="Seg_5535" s="T624">i-bi</ta>
            <ta e="T626" id="Seg_5536" s="T625">Ăprosʼa</ta>
            <ta e="T627" id="Seg_5537" s="T626">dĭgəttə</ta>
            <ta e="T628" id="Seg_5538" s="T627">i-bi</ta>
            <ta e="T629" id="Seg_5539" s="T628">Vera</ta>
            <ta e="T630" id="Seg_5540" s="T629">dĭgəttə</ta>
            <ta e="T631" id="Seg_5541" s="T630">Manja</ta>
            <ta e="T632" id="Seg_5542" s="T631">i-bi</ta>
            <ta e="T633" id="Seg_5543" s="T632">dĭgəttə</ta>
            <ta e="T634" id="Seg_5544" s="T633">nʼi</ta>
            <ta e="T635" id="Seg_5545" s="T634">šo-bi</ta>
            <ta e="T638" id="Seg_5546" s="T637">kăštə-bi-jəʔ</ta>
            <ta e="T639" id="Seg_5547" s="T638">Maksim</ta>
            <ta e="T640" id="Seg_5548" s="T639">onʼiʔ</ta>
            <ta e="T641" id="Seg_5549" s="T640">nʼi</ta>
            <ta e="T642" id="Seg_5550" s="T641">kü-laːm-bi</ta>
            <ta e="T643" id="Seg_5551" s="T642">măna</ta>
            <ta e="T644" id="Seg_5552" s="T643">ĭššo</ta>
            <ta e="T645" id="Seg_5553" s="T644">naga</ta>
            <ta e="T646" id="Seg_5554" s="T645">i-bi</ta>
            <ta e="T647" id="Seg_5555" s="T646">a</ta>
            <ta e="T648" id="Seg_5556" s="T647">a</ta>
            <ta e="T649" id="Seg_5557" s="T648">koʔbdo</ta>
            <ta e="T650" id="Seg_5558" s="T649">kü-laːm-bi</ta>
            <ta e="T651" id="Seg_5559" s="T650">oʔb</ta>
            <ta e="T652" id="Seg_5560" s="T651">šide</ta>
            <ta e="T653" id="Seg_5561" s="T652">nagur</ta>
            <ta e="T654" id="Seg_5562" s="T653">teʔdə</ta>
            <ta e="T655" id="Seg_5563" s="T654">sumna</ta>
            <ta e="T656" id="Seg_5564" s="T655">muktuʔ</ta>
            <ta e="T657" id="Seg_5565" s="T656">sejʔpü</ta>
            <ta e="T658" id="Seg_5566" s="T657">šĭnteʔtə</ta>
            <ta e="T659" id="Seg_5567" s="T658">amitun</ta>
            <ta e="T660" id="Seg_5568" s="T659">biəʔ</ta>
            <ta e="T661" id="Seg_5569" s="T660">biəʔ</ta>
            <ta e="T662" id="Seg_5570" s="T661">šide</ta>
            <ta e="T663" id="Seg_5571" s="T662">biəʔ</ta>
            <ta e="T664" id="Seg_5572" s="T663">onʼiʔ</ta>
            <ta e="T665" id="Seg_5573" s="T664">biəʔ</ta>
            <ta e="T666" id="Seg_5574" s="T665">nagur</ta>
            <ta e="T667" id="Seg_5575" s="T666">urgo</ta>
            <ta e="T668" id="Seg_5576" s="T667">koʔbdo</ta>
            <ta e="T669" id="Seg_5577" s="T668">kü-laːm-bi</ta>
            <ta e="T670" id="Seg_5578" s="T669">biəʔ</ta>
            <ta e="T671" id="Seg_5579" s="T670">nagur</ta>
            <ta e="T672" id="Seg_5580" s="T671">kö</ta>
            <ta e="T673" id="Seg_5581" s="T672">i-bi</ta>
            <ta e="T674" id="Seg_5582" s="T673">dĭ-Tə</ta>
            <ta e="T675" id="Seg_5583" s="T674">a</ta>
            <ta e="T676" id="Seg_5584" s="T675">Ăprosʼa-m</ta>
            <ta e="T678" id="Seg_5585" s="T676">kut-bi</ta>
            <ta e="T679" id="Seg_5586" s="T678">dĭ-n</ta>
            <ta e="T680" id="Seg_5587" s="T679">tibi</ta>
            <ta e="T681" id="Seg_5588" s="T680">kut-bi</ta>
            <ta e="T682" id="Seg_5589" s="T681">dĭ-m</ta>
            <ta e="T683" id="Seg_5590" s="T682">Nadja</ta>
            <ta e="T684" id="Seg_5591" s="T683">kü-laːm-bi</ta>
            <ta e="T685" id="Seg_5592" s="T684">sumna</ta>
            <ta e="T686" id="Seg_5593" s="T685">ki</ta>
            <ta e="T699" id="Seg_5594" s="T686">kan-lAʔ</ta>
            <ta e="T687" id="Seg_5595" s="T699">tʼür-bi-jəʔ</ta>
            <ta e="T688" id="Seg_5596" s="T687">a</ta>
            <ta e="T689" id="Seg_5597" s="T688">miʔ</ta>
            <ta e="T690" id="Seg_5598" s="T689">šide-göʔ</ta>
            <ta e="T691" id="Seg_5599" s="T690">ma-luʔbdə-bi-bAʔ</ta>
            <ta e="T692" id="Seg_5600" s="T691">Klawdʼa</ta>
            <ta e="T693" id="Seg_5601" s="T692">i</ta>
            <ta e="T694" id="Seg_5602" s="T693">Maksim</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T6" id="Seg_5603" s="T5">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T8" id="Seg_5604" s="T7">mushroom-PL</ta>
            <ta e="T9" id="Seg_5605" s="T8">see-PRS-2SG</ta>
            <ta e="T10" id="Seg_5606" s="T9">PTCL</ta>
            <ta e="T11" id="Seg_5607" s="T10">stand-PRS-3PL</ta>
            <ta e="T12" id="Seg_5608" s="T11">there</ta>
            <ta e="T13" id="Seg_5609" s="T12">there</ta>
            <ta e="T14" id="Seg_5610" s="T13">go-EP-IMP.2SG</ta>
            <ta e="T15" id="Seg_5611" s="T14">and</ta>
            <ta e="T16" id="Seg_5612" s="T15">I.NOM</ta>
            <ta e="T17" id="Seg_5613" s="T16">here</ta>
            <ta e="T18" id="Seg_5614" s="T17">go-FUT-1SG</ta>
            <ta e="T19" id="Seg_5615" s="T18">one.should</ta>
            <ta e="T20" id="Seg_5616" s="T19">many</ta>
            <ta e="T21" id="Seg_5617" s="T20">collect-INF.LAT</ta>
            <ta e="T22" id="Seg_5618" s="T21">mushroom-NOM/GEN/ACC.3PL</ta>
            <ta e="T28" id="Seg_5619" s="T27">tree-PL-GEN</ta>
            <ta e="T29" id="Seg_5620" s="T28">edge-LAT/LOC.3SG</ta>
            <ta e="T30" id="Seg_5621" s="T29">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T31" id="Seg_5622" s="T30">good-LAT.ADV</ta>
            <ta e="T32" id="Seg_5623" s="T31">NEG.AUX-IMP.2SG</ta>
            <ta e="T33" id="Seg_5624" s="T32">capture-IMP.2SG.O</ta>
            <ta e="T34" id="Seg_5625" s="T33">what.for</ta>
            <ta e="T35" id="Seg_5626" s="T34">capture-PST-2SG</ta>
            <ta e="T37" id="Seg_5627" s="T36">send-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_5628" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_5629" s="T38">people.[NOM.SG]</ta>
            <ta e="T40" id="Seg_5630" s="T39">come-PST-3PL</ta>
            <ta e="T41" id="Seg_5631" s="T40">we.LAT</ta>
            <ta e="T44" id="Seg_5632" s="T43">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T45" id="Seg_5633" s="T44">chief.[NOM.SG]</ta>
            <ta e="T46" id="Seg_5634" s="T45">place-DUR-3PL</ta>
            <ta e="T47" id="Seg_5635" s="T46">this-PL</ta>
            <ta e="T48" id="Seg_5636" s="T47">know-3PL</ta>
            <ta e="T49" id="Seg_5637" s="T48">paper.[NOM.SG]</ta>
            <ta e="T51" id="Seg_5638" s="T50">write-INF.LAT</ta>
            <ta e="T52" id="Seg_5639" s="T51">and</ta>
            <ta e="T53" id="Seg_5640" s="T52">we.NOM</ta>
            <ta e="T54" id="Seg_5641" s="T53">NEG</ta>
            <ta e="T55" id="Seg_5642" s="T54">know-1PL</ta>
            <ta e="T56" id="Seg_5643" s="T55">then</ta>
            <ta e="T57" id="Seg_5644" s="T56">this-PL</ta>
            <ta e="T58" id="Seg_5645" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_5646" s="T58">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_5647" s="T59">chief-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T61" id="Seg_5648" s="T60">stand-PST-3PL</ta>
            <ta e="T62" id="Seg_5649" s="T61">now</ta>
            <ta e="T63" id="Seg_5650" s="T62">we.NOM</ta>
            <ta e="T64" id="Seg_5651" s="T63">PTCL</ta>
            <ta e="T65" id="Seg_5652" s="T64">Russian</ta>
            <ta e="T66" id="Seg_5653" s="T65">language.[NOM.SG]</ta>
            <ta e="T67" id="Seg_5654" s="T66">speak-DUR-1PL</ta>
            <ta e="T68" id="Seg_5655" s="T67">Russian-GEN</ta>
            <ta e="T69" id="Seg_5656" s="T68">tongue-NOM/GEN.3SG</ta>
            <ta e="T70" id="Seg_5657" s="T69">study-DUR-1PL</ta>
            <ta e="T71" id="Seg_5658" s="T70">how</ta>
            <ta e="T72" id="Seg_5659" s="T71">you.ACC</ta>
            <ta e="T73" id="Seg_5660" s="T72">call-PRS-3PL</ta>
            <ta e="T74" id="Seg_5661" s="T73">and</ta>
            <ta e="T75" id="Seg_5662" s="T74">how</ta>
            <ta e="T76" id="Seg_5663" s="T75">name-PRS-3PL</ta>
            <ta e="T77" id="Seg_5664" s="T76">I.ACC</ta>
            <ta e="T78" id="Seg_5665" s="T77">call-PRS-3PL</ta>
            <ta e="T79" id="Seg_5666" s="T78">Klavdiya.[NOM.SG]</ta>
            <ta e="T81" id="Seg_5667" s="T79">and</ta>
            <ta e="T82" id="Seg_5668" s="T81">and</ta>
            <ta e="T83" id="Seg_5669" s="T82">name-PRS-3PL</ta>
            <ta e="T85" id="Seg_5670" s="T84">how</ta>
            <ta e="T86" id="Seg_5671" s="T85">come-PST-2SG</ta>
            <ta e="T87" id="Seg_5672" s="T86">here</ta>
            <ta e="T88" id="Seg_5673" s="T87">that.[NOM.SG]</ta>
            <ta e="T89" id="Seg_5674" s="T88">I.NOM</ta>
            <ta e="T90" id="Seg_5675" s="T89">chief.[NOM.SG]</ta>
            <ta e="T91" id="Seg_5676" s="T90">and</ta>
            <ta e="T92" id="Seg_5677" s="T91">that.[NOM.SG]</ta>
            <ta e="T93" id="Seg_5678" s="T92">speak-INF.LAT</ta>
            <ta e="T95" id="Seg_5679" s="T94">chief.[NOM.SG]</ta>
            <ta e="T96" id="Seg_5680" s="T95">and</ta>
            <ta e="T97" id="Seg_5681" s="T96">I.NOM</ta>
            <ta e="T98" id="Seg_5682" s="T97">this-PL</ta>
            <ta e="T99" id="Seg_5683" s="T98">learn-TR-PRS-1SG</ta>
            <ta e="T100" id="Seg_5684" s="T99">you.NOM</ta>
            <ta e="T101" id="Seg_5685" s="T100">man.[NOM.SG]</ta>
            <ta e="T102" id="Seg_5686" s="T101">where.to</ta>
            <ta e="T696" id="Seg_5687" s="T102">go-CVB</ta>
            <ta e="T103" id="Seg_5688" s="T696">disappear-PST.[3SG]</ta>
            <ta e="T104" id="Seg_5689" s="T103">Russian-GEN</ta>
            <ta e="T105" id="Seg_5690" s="T104">settlement-LAT</ta>
            <ta e="T106" id="Seg_5691" s="T105">I.NOM</ta>
            <ta e="T107" id="Seg_5692" s="T106">many</ta>
            <ta e="T108" id="Seg_5693" s="T107">%%-PST-2SG</ta>
            <ta e="T109" id="Seg_5694" s="T108">think-MOM-PST-1SG</ta>
            <ta e="T110" id="Seg_5695" s="T109">speak-INF.LAT</ta>
            <ta e="T111" id="Seg_5696" s="T110">I.NOM</ta>
            <ta e="T112" id="Seg_5697" s="T111">pig-NOM/GEN/ACC.1SG</ta>
            <ta e="T113" id="Seg_5698" s="T112">die-RES-PST.[3SG]</ta>
            <ta e="T114" id="Seg_5699" s="T113">I.NOM</ta>
            <ta e="T115" id="Seg_5700" s="T114">earth.[NOM.SG]</ta>
            <ta e="T116" id="Seg_5701" s="T115">dig-PST-1SG</ta>
            <ta e="T117" id="Seg_5702" s="T116">and</ta>
            <ta e="T118" id="Seg_5703" s="T117">there</ta>
            <ta e="T120" id="Seg_5704" s="T119">this-ACC</ta>
            <ta e="T121" id="Seg_5705" s="T120">put-PST-1SG</ta>
            <ta e="T122" id="Seg_5706" s="T121">and</ta>
            <ta e="T123" id="Seg_5707" s="T122">earth-INS</ta>
            <ta e="T124" id="Seg_5708" s="T123">pour-PST-1SG</ta>
            <ta e="T126" id="Seg_5709" s="T125">we.NOM</ta>
            <ta e="T127" id="Seg_5710" s="T126">PTCL</ta>
            <ta e="T130" id="Seg_5711" s="T129">be-PST.[3SG]</ta>
            <ta e="T131" id="Seg_5712" s="T130">there</ta>
            <ta e="T132" id="Seg_5713" s="T131">many</ta>
            <ta e="T133" id="Seg_5714" s="T132">cross-PL</ta>
            <ta e="T138" id="Seg_5715" s="T137">stand-DUR-3PL</ta>
            <ta e="T139" id="Seg_5716" s="T138">and</ta>
            <ta e="T140" id="Seg_5717" s="T139">now</ta>
            <ta e="T142" id="Seg_5718" s="T140">one.[NOM.SG]=PTCL</ta>
            <ta e="T143" id="Seg_5719" s="T142">NEG.EX.[3SG]</ta>
            <ta e="T144" id="Seg_5720" s="T143">what.[NOM.SG]=INDEF</ta>
            <ta e="T147" id="Seg_5721" s="T146">there</ta>
            <ta e="T148" id="Seg_5722" s="T147">NEG.EX.[3SG]</ta>
            <ta e="T149" id="Seg_5723" s="T148">now</ta>
            <ta e="T150" id="Seg_5724" s="T149">there</ta>
            <ta e="T152" id="Seg_5725" s="T151">stand-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_5726" s="T152">now</ta>
            <ta e="T154" id="Seg_5727" s="T153">we.NOM</ta>
            <ta e="T155" id="Seg_5728" s="T154">new.[NOM.SG]</ta>
            <ta e="T157" id="Seg_5729" s="T156">there</ta>
            <ta e="T158" id="Seg_5730" s="T157">many</ta>
            <ta e="T159" id="Seg_5731" s="T158">cross-PL</ta>
            <ta e="T160" id="Seg_5732" s="T159">stand-PRS-3PL</ta>
            <ta e="T161" id="Seg_5733" s="T160">also</ta>
            <ta e="T162" id="Seg_5734" s="T161">you.NOM</ta>
            <ta e="T164" id="Seg_5735" s="T162">God.[NOM.SG]</ta>
            <ta e="T165" id="Seg_5736" s="T164">you.NOM</ta>
            <ta e="T166" id="Seg_5737" s="T165">God.[NOM.SG]</ta>
            <ta e="T167" id="Seg_5738" s="T166">God.[NOM.SG]</ta>
            <ta e="T168" id="Seg_5739" s="T167">NEG.AUX-IMP.2SG</ta>
            <ta e="T169" id="Seg_5740" s="T168">leave-IMP.2SG.O</ta>
            <ta e="T170" id="Seg_5741" s="T169">I.ACC</ta>
            <ta e="T171" id="Seg_5742" s="T170">NEG.AUX-IMP.2SG</ta>
            <ta e="T172" id="Seg_5743" s="T171">leave-IMP.2SG.O</ta>
            <ta e="T173" id="Seg_5744" s="T172">I.ACC</ta>
            <ta e="T174" id="Seg_5745" s="T173">NEG.AUX-IMP.2SG</ta>
            <ta e="T175" id="Seg_5746" s="T174">throw.away-IMP.2SG</ta>
            <ta e="T176" id="Seg_5747" s="T175">I.ACC</ta>
            <ta e="T177" id="Seg_5748" s="T176">you.NOM</ta>
            <ta e="T178" id="Seg_5749" s="T177">%%</ta>
            <ta e="T179" id="Seg_5750" s="T178">I.LAT</ta>
            <ta e="T181" id="Seg_5751" s="T180">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T182" id="Seg_5752" s="T181">mind-ADJZ.[NOM.SG]</ta>
            <ta e="T183" id="Seg_5753" s="T182">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_5754" s="T183">mind-ADJZ.[NOM.SG]</ta>
            <ta e="T185" id="Seg_5755" s="T184">you.NOM</ta>
            <ta e="T186" id="Seg_5756" s="T185">send-IMP.2SG.O</ta>
            <ta e="T187" id="Seg_5757" s="T186">I.LAT</ta>
            <ta e="T189" id="Seg_5758" s="T188">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T191" id="Seg_5759" s="T190">silly.[NOM.SG]</ta>
            <ta e="T192" id="Seg_5760" s="T191">be-PRS.[3SG]</ta>
            <ta e="T193" id="Seg_5761" s="T192">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T194" id="Seg_5762" s="T193">silly.[NOM.SG]</ta>
            <ta e="T195" id="Seg_5763" s="T194">be-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_5764" s="T195">study-TR-IMP.2SG</ta>
            <ta e="T197" id="Seg_5765" s="T196">I.ACC</ta>
            <ta e="T198" id="Seg_5766" s="T197">praise-INF.LAT</ta>
            <ta e="T199" id="Seg_5767" s="T198">you.NOM-INS</ta>
            <ta e="T200" id="Seg_5768" s="T199">study-TR-IMP.2SG</ta>
            <ta e="T201" id="Seg_5769" s="T200">I.ACC</ta>
            <ta e="T202" id="Seg_5770" s="T201">praise-INF.LAT</ta>
            <ta e="T204" id="Seg_5771" s="T203">you.NOM-INS</ta>
            <ta e="T205" id="Seg_5772" s="T204">study-TR-IMP.2SG</ta>
            <ta e="T206" id="Seg_5773" s="T205">I.ACC</ta>
            <ta e="T207" id="Seg_5774" s="T206">road-NOM/GEN.3SG</ta>
            <ta e="T208" id="Seg_5775" s="T207">you.NOM</ta>
            <ta e="T209" id="Seg_5776" s="T208">go-INF.LAT</ta>
            <ta e="T210" id="Seg_5777" s="T209">I.NOM</ta>
            <ta e="T211" id="Seg_5778" s="T210">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T213" id="Seg_5779" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_5780" s="T213">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T215" id="Seg_5781" s="T214">study-PST.[3SG]</ta>
            <ta e="T216" id="Seg_5782" s="T215">press-INF.LAT</ta>
            <ta e="T217" id="Seg_5783" s="T216">there</ta>
            <ta e="T218" id="Seg_5784" s="T217">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T221" id="Seg_5785" s="T220">go-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_5786" s="T221">and</ta>
            <ta e="T223" id="Seg_5787" s="T222">more</ta>
            <ta e="T227" id="Seg_5788" s="T226">go-FUT-3SG</ta>
            <ta e="T228" id="Seg_5789" s="T227">this-GEN</ta>
            <ta e="T229" id="Seg_5790" s="T228">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T230" id="Seg_5791" s="T229">give-INF.LAT</ta>
            <ta e="T231" id="Seg_5792" s="T230">and</ta>
            <ta e="T232" id="Seg_5793" s="T231">this-GEN</ta>
            <ta e="T233" id="Seg_5794" s="T232">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T234" id="Seg_5795" s="T233">take-INF.LAT</ta>
            <ta e="T235" id="Seg_5796" s="T234">one.should</ta>
            <ta e="T236" id="Seg_5797" s="T235">I.LAT</ta>
            <ta e="T237" id="Seg_5798" s="T236">net.[NOM.SG]</ta>
            <ta e="T238" id="Seg_5799" s="T237">bind-INF.LAT</ta>
            <ta e="T239" id="Seg_5800" s="T238">fish.[NOM.SG]</ta>
            <ta e="T245" id="Seg_5801" s="T244">fish.[NOM.SG]</ta>
            <ta e="T246" id="Seg_5802" s="T245">capture-INF.LAT</ta>
            <ta e="T248" id="Seg_5803" s="T247">this-PL</ta>
            <ta e="T249" id="Seg_5804" s="T248">Kamassian-PL</ta>
            <ta e="T250" id="Seg_5805" s="T249">PTCL</ta>
            <ta e="T251" id="Seg_5806" s="T250">small.raft-LAT</ta>
            <ta e="T252" id="Seg_5807" s="T251">walk-DUR-3PL</ta>
            <ta e="T253" id="Seg_5808" s="T252">and</ta>
            <ta e="T254" id="Seg_5809" s="T253">net.[NOM.SG]</ta>
            <ta e="T255" id="Seg_5810" s="T254">throw.away-DUR-3PL</ta>
            <ta e="T256" id="Seg_5811" s="T255">and</ta>
            <ta e="T257" id="Seg_5812" s="T256">fish.[NOM.SG]</ta>
            <ta e="T259" id="Seg_5813" s="T258">capture-DUR-3PL</ta>
            <ta e="T260" id="Seg_5814" s="T259">stone-PL</ta>
            <ta e="T261" id="Seg_5815" s="T260">PTCL</ta>
            <ta e="T262" id="Seg_5816" s="T261">birchbark-LAT</ta>
            <ta e="T263" id="Seg_5817" s="T262">sew-DUR-3PL</ta>
            <ta e="T264" id="Seg_5818" s="T263">and</ta>
            <ta e="T265" id="Seg_5819" s="T264">net-LAT</ta>
            <ta e="T266" id="Seg_5820" s="T265">bind-DUR-3PL</ta>
            <ta e="T267" id="Seg_5821" s="T266">this.[NOM.SG]</ta>
            <ta e="T268" id="Seg_5822" s="T267">%%-LAT</ta>
            <ta e="T269" id="Seg_5823" s="T268">all</ta>
            <ta e="T270" id="Seg_5824" s="T269">what.[NOM.SG]</ta>
            <ta e="T271" id="Seg_5825" s="T270">bring-DUR.[3SG]</ta>
            <ta e="T272" id="Seg_5826" s="T271">God.[NOM.SG]</ta>
            <ta e="T273" id="Seg_5827" s="T272">or</ta>
            <ta e="T274" id="Seg_5828" s="T273">who.[NOM.SG]</ta>
            <ta e="T275" id="Seg_5829" s="T274">NEG</ta>
            <ta e="T276" id="Seg_5830" s="T275">know-1SG</ta>
            <ta e="T277" id="Seg_5831" s="T276">snow-LAT</ta>
            <ta e="T278" id="Seg_5832" s="T277">PTCL</ta>
            <ta e="T279" id="Seg_5833" s="T278">jump-MOM-PST-1SG</ta>
            <ta e="T280" id="Seg_5834" s="T279">part.[NOM.SG]</ta>
            <ta e="T281" id="Seg_5835" s="T280">so</ta>
            <ta e="T282" id="Seg_5836" s="T281">PTCL</ta>
            <ta e="T697" id="Seg_5837" s="T282">go-CVB</ta>
            <ta e="T283" id="Seg_5838" s="T697">disappear-PST-1SG</ta>
            <ta e="T284" id="Seg_5839" s="T283">I.NOM</ta>
            <ta e="T285" id="Seg_5840" s="T284">come-PRS-1SG</ta>
            <ta e="T286" id="Seg_5841" s="T285">snow.[NOM.SG]</ta>
            <ta e="T287" id="Seg_5842" s="T286">very</ta>
            <ta e="T289" id="Seg_5843" s="T288">many</ta>
            <ta e="T290" id="Seg_5844" s="T289">lie-DUR.[3SG]</ta>
            <ta e="T291" id="Seg_5845" s="T290">I.NOM</ta>
            <ta e="T292" id="Seg_5846" s="T291">go-PST-1SG</ta>
            <ta e="T293" id="Seg_5847" s="T292">and</ta>
            <ta e="T294" id="Seg_5848" s="T293">there</ta>
            <ta e="T295" id="Seg_5849" s="T294">jump-MOM-PST-1SG</ta>
            <ta e="T296" id="Seg_5850" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_5851" s="T296">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T298" id="Seg_5852" s="T297">I.NOM</ta>
            <ta e="T300" id="Seg_5853" s="T299">%%</ta>
            <ta e="T301" id="Seg_5854" s="T300">come-PST-1SG</ta>
            <ta e="T302" id="Seg_5855" s="T301">there</ta>
            <ta e="T303" id="Seg_5856" s="T302">PTCL</ta>
            <ta e="T304" id="Seg_5857" s="T303">very</ta>
            <ta e="T305" id="Seg_5858" s="T304">dirty.[NOM.SG]</ta>
            <ta e="T307" id="Seg_5859" s="T306">many</ta>
            <ta e="T308" id="Seg_5860" s="T307">I.NOM</ta>
            <ta e="T309" id="Seg_5861" s="T308">stand-PST-1SG</ta>
            <ta e="T310" id="Seg_5862" s="T309">and</ta>
            <ta e="T311" id="Seg_5863" s="T310">there</ta>
            <ta e="T312" id="Seg_5864" s="T311">fall-MOM-PST-1SG</ta>
            <ta e="T313" id="Seg_5865" s="T312">PTCL</ta>
            <ta e="T315" id="Seg_5866" s="T314">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T318" id="Seg_5867" s="T317">then</ta>
            <ta e="T319" id="Seg_5868" s="T318">turf-NOM/GEN/ACC.1SG</ta>
            <ta e="T320" id="Seg_5869" s="T319">hand-INS</ta>
            <ta e="T321" id="Seg_5870" s="T320">capture-PST-1SG</ta>
            <ta e="T322" id="Seg_5871" s="T321">and</ta>
            <ta e="T323" id="Seg_5872" s="T322">return-PST-1SG</ta>
            <ta e="T328" id="Seg_5873" s="T327">this.[NOM.SG]</ta>
            <ta e="T329" id="Seg_5874" s="T328">horse-PL</ta>
            <ta e="T330" id="Seg_5875" s="T329">feed-PST-3PL</ta>
            <ta e="T331" id="Seg_5876" s="T330">and</ta>
            <ta e="T332" id="Seg_5877" s="T331">that.[NOM.SG]</ta>
            <ta e="T333" id="Seg_5878" s="T332">horse-PL</ta>
            <ta e="T334" id="Seg_5879" s="T333">NEG</ta>
            <ta e="T335" id="Seg_5880" s="T334">feed-PST-3PL</ta>
            <ta e="T336" id="Seg_5881" s="T335">you.NOM</ta>
            <ta e="T337" id="Seg_5882" s="T336">sheep-PL</ta>
            <ta e="T338" id="Seg_5883" s="T337">give-PST-2SG</ta>
            <ta e="T339" id="Seg_5884" s="T338">grass.[NOM.SG]</ta>
            <ta e="T340" id="Seg_5885" s="T339">this-GEN.PL</ta>
            <ta e="T341" id="Seg_5886" s="T340">eat-PST-3PL</ta>
            <ta e="T342" id="Seg_5887" s="T341">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T343" id="Seg_5888" s="T342">give-PST-2SG</ta>
            <ta e="T344" id="Seg_5889" s="T343">grass.[NOM.SG]</ta>
            <ta e="T345" id="Seg_5890" s="T344">this.[NOM.SG]</ta>
            <ta e="T346" id="Seg_5891" s="T345">eat-PST.[3SG]</ta>
            <ta e="T347" id="Seg_5892" s="T346">calf-LAT</ta>
            <ta e="T348" id="Seg_5893" s="T347">probably</ta>
            <ta e="T349" id="Seg_5894" s="T348">NEG</ta>
            <ta e="T350" id="Seg_5895" s="T349">give-PST-2SG</ta>
            <ta e="T351" id="Seg_5896" s="T350">give-PST-1SG</ta>
            <ta e="T352" id="Seg_5897" s="T351">all</ta>
            <ta e="T353" id="Seg_5898" s="T352">eat-PST-3PL</ta>
            <ta e="T357" id="Seg_5899" s="T356">child-INS</ta>
            <ta e="T358" id="Seg_5900" s="T357">feed-PST-2SG</ta>
            <ta e="T359" id="Seg_5901" s="T358">whether</ta>
            <ta e="T360" id="Seg_5902" s="T359">this-ACC</ta>
            <ta e="T361" id="Seg_5903" s="T360">feed-PST-1SG</ta>
            <ta e="T362" id="Seg_5904" s="T361">and</ta>
            <ta e="T363" id="Seg_5905" s="T362">milk.[NOM.SG]</ta>
            <ta e="T364" id="Seg_5906" s="T363">give-PST-1SG</ta>
            <ta e="T365" id="Seg_5907" s="T364">and</ta>
            <ta e="T366" id="Seg_5908" s="T365">water.[NOM.SG]</ta>
            <ta e="T367" id="Seg_5909" s="T366">drink-PST.[3SG]</ta>
            <ta e="T368" id="Seg_5910" s="T367">true</ta>
            <ta e="T369" id="Seg_5911" s="T368">eat-PST.[3SG]</ta>
            <ta e="T370" id="Seg_5912" s="T369">eat-PST.[3SG]</ta>
            <ta e="T371" id="Seg_5913" s="T370">child.[NOM.SG]</ta>
            <ta e="T372" id="Seg_5914" s="T371">NEG</ta>
            <ta e="T373" id="Seg_5915" s="T372">eat-PRS.[3SG]</ta>
            <ta e="T374" id="Seg_5916" s="T373">NEG</ta>
            <ta e="T375" id="Seg_5917" s="T374">lie-PRS-2SG</ta>
            <ta e="T376" id="Seg_5918" s="T375">true</ta>
            <ta e="T377" id="Seg_5919" s="T376">tell-PST-2SG</ta>
            <ta e="T379" id="Seg_5920" s="T377">this-PL</ta>
            <ta e="T380" id="Seg_5921" s="T379">tree-PL</ta>
            <ta e="T381" id="Seg_5922" s="T380">axe-INS</ta>
            <ta e="T382" id="Seg_5923" s="T381">axe-INS</ta>
            <ta e="T383" id="Seg_5924" s="T382">make-PST-3PL</ta>
            <ta e="T384" id="Seg_5925" s="T383">and</ta>
            <ta e="T385" id="Seg_5926" s="T384">then</ta>
            <ta e="T386" id="Seg_5927" s="T385">water-LAT</ta>
            <ta e="T387" id="Seg_5928" s="T386">go-PST-3PL</ta>
            <ta e="T388" id="Seg_5929" s="T387">and</ta>
            <ta e="T389" id="Seg_5930" s="T388">fish.[NOM.SG]</ta>
            <ta e="T390" id="Seg_5931" s="T389">capture-PST-3PL</ta>
            <ta e="T391" id="Seg_5932" s="T390">there</ta>
            <ta e="T394" id="Seg_5933" s="T393">PTCL</ta>
            <ta e="T395" id="Seg_5934" s="T394">one.[NOM.SG]</ta>
            <ta e="T396" id="Seg_5935" s="T395">one-LAT</ta>
            <ta e="T397" id="Seg_5936" s="T396">bind-PST-3PL</ta>
            <ta e="T398" id="Seg_5937" s="T397">and</ta>
            <ta e="T399" id="Seg_5938" s="T398">water-LAT</ta>
            <ta e="T400" id="Seg_5939" s="T399">go-PST-3PL</ta>
            <ta e="T402" id="Seg_5940" s="T401">wash-IMP.2SG</ta>
            <ta e="T403" id="Seg_5941" s="T402">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T404" id="Seg_5942" s="T403">water-INS</ta>
            <ta e="T405" id="Seg_5943" s="T404">and</ta>
            <ta e="T406" id="Seg_5944" s="T405">rub-IMP.2SG.O</ta>
            <ta e="T407" id="Seg_5945" s="T406">snow.[NOM.SG]</ta>
            <ta e="T408" id="Seg_5946" s="T407">become-FUT-2SG</ta>
            <ta e="T409" id="Seg_5947" s="T408">beautiful.[NOM.SG]</ta>
            <ta e="T410" id="Seg_5948" s="T409">become-FUT-2SG</ta>
            <ta e="T411" id="Seg_5949" s="T410">you.PL.NOM</ta>
            <ta e="T412" id="Seg_5950" s="T411">people.[NOM.SG]</ta>
            <ta e="T414" id="Seg_5951" s="T413">where.to</ta>
            <ta e="T415" id="Seg_5952" s="T414">walk-PST-3PL</ta>
            <ta e="T416" id="Seg_5953" s="T415">%Rybnoe-LAT</ta>
            <ta e="T417" id="Seg_5954" s="T416">walk-PST-3PL</ta>
            <ta e="T420" id="Seg_5955" s="T419">many</ta>
            <ta e="T421" id="Seg_5956" s="T420">sack-LOC</ta>
            <ta e="T422" id="Seg_5957" s="T421">PTCL</ta>
            <ta e="T423" id="Seg_5958" s="T422">sable-PL</ta>
            <ta e="T424" id="Seg_5959" s="T423">PTCL</ta>
            <ta e="T425" id="Seg_5960" s="T424">bind-PST.[3SG]</ta>
            <ta e="T426" id="Seg_5961" s="T425">rope-INS</ta>
            <ta e="T427" id="Seg_5962" s="T426">and</ta>
            <ta e="T428" id="Seg_5963" s="T427">bring-PST.[3SG]</ta>
            <ta e="T429" id="Seg_5964" s="T428">house-LAT/LOC.3SG</ta>
            <ta e="T430" id="Seg_5965" s="T429">this</ta>
            <ta e="T431" id="Seg_5966" s="T430">man.[NOM.SG]</ta>
            <ta e="T432" id="Seg_5967" s="T431">PTCL</ta>
            <ta e="T433" id="Seg_5968" s="T432">work-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_5969" s="T433">work-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_5970" s="T434">and</ta>
            <ta e="T436" id="Seg_5971" s="T435">what.[NOM.SG]=INDEF</ta>
            <ta e="T437" id="Seg_5972" s="T436">NEG.EX.[3SG]</ta>
            <ta e="T438" id="Seg_5973" s="T437">and</ta>
            <ta e="T439" id="Seg_5974" s="T438">that.[NOM.SG]</ta>
            <ta e="T440" id="Seg_5975" s="T439">man.[NOM.SG]</ta>
            <ta e="T441" id="Seg_5976" s="T440">PTCL</ta>
            <ta e="T442" id="Seg_5977" s="T441">NEG</ta>
            <ta e="T443" id="Seg_5978" s="T442">strongly</ta>
            <ta e="T444" id="Seg_5979" s="T443">work-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_5980" s="T444">all</ta>
            <ta e="T446" id="Seg_5981" s="T445">what.[NOM.SG]</ta>
            <ta e="T447" id="Seg_5982" s="T446">be-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_5983" s="T449">NEG.AUX-IMP.2SG</ta>
            <ta e="T451" id="Seg_5984" s="T450">lie-CNG</ta>
            <ta e="T452" id="Seg_5985" s="T451">get.up-IMP.2SG</ta>
            <ta e="T453" id="Seg_5986" s="T452">and</ta>
            <ta e="T454" id="Seg_5987" s="T453">work-EP-IMP.2SG</ta>
            <ta e="T455" id="Seg_5988" s="T454">what.[NOM.SG]</ta>
            <ta e="T456" id="Seg_5989" s="T455">lie-INF.LAT</ta>
            <ta e="T457" id="Seg_5990" s="T456">many</ta>
            <ta e="T458" id="Seg_5991" s="T457">year.[NOM.SG]</ta>
            <ta e="T698" id="Seg_5992" s="T458">go-CVB</ta>
            <ta e="T459" id="Seg_5993" s="T698">disappear-PST-3PL</ta>
            <ta e="T460" id="Seg_5994" s="T459">come-PST-3PL</ta>
            <ta e="T461" id="Seg_5995" s="T460">red.[NOM.SG]</ta>
            <ta e="T462" id="Seg_5996" s="T461">white-PL</ta>
            <ta e="T463" id="Seg_5997" s="T462">come-PST-3PL</ta>
            <ta e="T464" id="Seg_5998" s="T463">and</ta>
            <ta e="T465" id="Seg_5999" s="T464">people.[NOM.SG]</ta>
            <ta e="T466" id="Seg_6000" s="T465">very</ta>
            <ta e="T467" id="Seg_6001" s="T466">fear-PRS-3PL</ta>
            <ta e="T468" id="Seg_6002" s="T467">people.[NOM.SG]</ta>
            <ta e="T470" id="Seg_6003" s="T469">all</ta>
            <ta e="T471" id="Seg_6004" s="T470">run-MOM-PST-3PL</ta>
            <ta e="T472" id="Seg_6005" s="T471">forest-LAT</ta>
            <ta e="T473" id="Seg_6006" s="T472">very</ta>
            <ta e="T474" id="Seg_6007" s="T473">fear-PST-3PL</ta>
            <ta e="T475" id="Seg_6008" s="T474">and</ta>
            <ta e="T476" id="Seg_6009" s="T475">when</ta>
            <ta e="T477" id="Seg_6010" s="T476">come-FUT-3PL</ta>
            <ta e="T478" id="Seg_6011" s="T477">house-LAT</ta>
            <ta e="T479" id="Seg_6012" s="T478">this-PL</ta>
            <ta e="T480" id="Seg_6013" s="T479">PTCL</ta>
            <ta e="T482" id="Seg_6014" s="T481">come-PST-3PL</ta>
            <ta e="T483" id="Seg_6015" s="T482">this-PL</ta>
            <ta e="T484" id="Seg_6016" s="T483">PTCL</ta>
            <ta e="T485" id="Seg_6017" s="T484">come-PST-3PL</ta>
            <ta e="T486" id="Seg_6018" s="T485">white.[NOM.SG]</ta>
            <ta e="T487" id="Seg_6019" s="T486">people.[NOM.SG]</ta>
            <ta e="T488" id="Seg_6020" s="T487">we.GEN</ta>
            <ta e="T489" id="Seg_6021" s="T488">people-NOM/GEN/ACC.1PL</ta>
            <ta e="T490" id="Seg_6022" s="T489">PTCL</ta>
            <ta e="T491" id="Seg_6023" s="T490">red-PL</ta>
            <ta e="T493" id="Seg_6024" s="T492">pity-PST-3PL</ta>
            <ta e="T495" id="Seg_6025" s="T494">this-PL-LAT</ta>
            <ta e="T496" id="Seg_6026" s="T495">bread.[NOM.SG]</ta>
            <ta e="T498" id="Seg_6027" s="T497">give-PST-3PL</ta>
            <ta e="T499" id="Seg_6028" s="T498">what.[NOM.SG]</ta>
            <ta e="T500" id="Seg_6029" s="T499">be-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_6030" s="T500">all</ta>
            <ta e="T502" id="Seg_6031" s="T501">give-PST-3PL</ta>
            <ta e="T503" id="Seg_6032" s="T502">salt.[NOM.SG]</ta>
            <ta e="T504" id="Seg_6033" s="T503">cauldron.[NOM.SG]</ta>
            <ta e="T505" id="Seg_6034" s="T504">give-PST-3PL</ta>
            <ta e="T506" id="Seg_6035" s="T505">more</ta>
            <ta e="T507" id="Seg_6036" s="T506">potato.[NOM.SG]</ta>
            <ta e="T508" id="Seg_6037" s="T507">this-PL-LAT</ta>
            <ta e="T509" id="Seg_6038" s="T508">give-PST-3PL</ta>
            <ta e="T510" id="Seg_6039" s="T509">red-PL</ta>
            <ta e="T511" id="Seg_6040" s="T510">PTCL</ta>
            <ta e="T512" id="Seg_6041" s="T511">forest-LOC</ta>
            <ta e="T513" id="Seg_6042" s="T512">live-PST-3PL</ta>
            <ta e="T514" id="Seg_6043" s="T513">and</ta>
            <ta e="T515" id="Seg_6044" s="T514">evening-LOC.ADV</ta>
            <ta e="T516" id="Seg_6045" s="T515">here</ta>
            <ta e="T517" id="Seg_6046" s="T516">come-PST-3PL</ta>
            <ta e="T518" id="Seg_6047" s="T517">eat-INF.LAT</ta>
            <ta e="T520" id="Seg_6048" s="T519">one.[NOM.SG]</ta>
            <ta e="T522" id="Seg_6049" s="T521">come-PST-3PL</ta>
            <ta e="T523" id="Seg_6050" s="T522">two.[NOM.SG]</ta>
            <ta e="T525" id="Seg_6051" s="T524">red-PL</ta>
            <ta e="T526" id="Seg_6052" s="T525">gun-INS</ta>
            <ta e="T528" id="Seg_6053" s="T527">one-LAT</ta>
            <ta e="T529" id="Seg_6054" s="T528">PTCL</ta>
            <ta e="T531" id="Seg_6055" s="T530">NEG</ta>
            <ta e="T532" id="Seg_6056" s="T531">kill-PST-3PL</ta>
            <ta e="T533" id="Seg_6057" s="T532">woman.[NOM.SG]</ta>
            <ta e="T534" id="Seg_6058" s="T533">we.LAT</ta>
            <ta e="T535" id="Seg_6059" s="T534">come-PRS.[3SG]</ta>
            <ta e="T536" id="Seg_6060" s="T535">you.PL.NOM</ta>
            <ta e="T537" id="Seg_6061" s="T536">PTCL</ta>
            <ta e="T538" id="Seg_6062" s="T537">here</ta>
            <ta e="T539" id="Seg_6063" s="T538">speak-PRS-2PL</ta>
            <ta e="T540" id="Seg_6064" s="T539">and</ta>
            <ta e="T541" id="Seg_6065" s="T540">man.[NOM.SG]</ta>
            <ta e="T542" id="Seg_6066" s="T541">window-LOC</ta>
            <ta e="T543" id="Seg_6067" s="T542">stand-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_6068" s="T543">listen-DUR.[3SG]</ta>
            <ta e="T545" id="Seg_6069" s="T544">we.NOM</ta>
            <ta e="T546" id="Seg_6070" s="T545">PTCL</ta>
            <ta e="T550" id="Seg_6071" s="T549">one.[NOM.SG]</ta>
            <ta e="T552" id="Seg_6072" s="T551">evening-LOC.ADV</ta>
            <ta e="T553" id="Seg_6073" s="T552">come-PST-3PL</ta>
            <ta e="T554" id="Seg_6074" s="T553">red-PL</ta>
            <ta e="T555" id="Seg_6075" s="T554">man.[NOM.SG]</ta>
            <ta e="T556" id="Seg_6076" s="T555">bring-PST-3PL</ta>
            <ta e="T557" id="Seg_6077" s="T556">door-GEN.3SG</ta>
            <ta e="T558" id="Seg_6078" s="T557">edge-LAT/LOC.3SG</ta>
            <ta e="T559" id="Seg_6079" s="T558">sow-PST-3PL</ta>
            <ta e="T560" id="Seg_6080" s="T559">then</ta>
            <ta e="T561" id="Seg_6081" s="T560">PTCL</ta>
            <ta e="T562" id="Seg_6082" s="T561">man-PL</ta>
            <ta e="T563" id="Seg_6083" s="T562">collect-PST-3PL</ta>
            <ta e="T564" id="Seg_6084" s="T563">beat-PST-3PL</ta>
            <ta e="T565" id="Seg_6085" s="T564">PTCL</ta>
            <ta e="T566" id="Seg_6086" s="T565">whip-PL-INS</ta>
            <ta e="T567" id="Seg_6087" s="T566">one.[NOM.SG]</ta>
            <ta e="T568" id="Seg_6088" s="T567">man.[NOM.SG]</ta>
            <ta e="T569" id="Seg_6089" s="T568">PTCL</ta>
            <ta e="T570" id="Seg_6090" s="T569">pants-LAT/LOC.3SG</ta>
            <ta e="T571" id="Seg_6091" s="T570">PTCL</ta>
            <ta e="T572" id="Seg_6092" s="T571">shit-PST.[3SG]</ta>
            <ta e="T573" id="Seg_6093" s="T572">I.NOM</ta>
            <ta e="T574" id="Seg_6094" s="T573">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T575" id="Seg_6095" s="T574">call-PST-3PL</ta>
            <ta e="T576" id="Seg_6096" s="T575">Zakhar.[NOM.SG]</ta>
            <ta e="T577" id="Seg_6097" s="T576">and</ta>
            <ta e="T579" id="Seg_6098" s="T578">name-MOM-PST-3PL</ta>
            <ta e="T580" id="Seg_6099" s="T579">Stepanovich.[NOM.SG]</ta>
            <ta e="T581" id="Seg_6100" s="T580">and</ta>
            <ta e="T582" id="Seg_6101" s="T581">mother-NOM/GEN/ACC.1SG </ta>
            <ta e="T583" id="Seg_6102" s="T582">Afanasia.[NOM.SG] </ta>
            <ta e="T584" id="Seg_6103" s="T583">name-MOM-PST-3PL</ta>
            <ta e="T585" id="Seg_6104" s="T584">Antonovna.[NOM.SG] </ta>
            <ta e="T586" id="Seg_6105" s="T585">and</ta>
            <ta e="T587" id="Seg_6106" s="T586">surname.[NOM.SG] </ta>
            <ta e="T588" id="Seg_6107" s="T587">this-GEN.PL</ta>
            <ta e="T589" id="Seg_6108" s="T588">be-PST.[3SG]</ta>
            <ta e="T590" id="Seg_6109" s="T589">Andzhigatov-PL</ta>
            <ta e="T591" id="Seg_6110" s="T590">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T592" id="Seg_6111" s="T591">be-PST.[3SG]</ta>
            <ta e="T594" id="Seg_6112" s="T593">one.[NOM.SG]</ta>
            <ta e="T595" id="Seg_6113" s="T594">two.[NOM.SG]</ta>
            <ta e="T596" id="Seg_6114" s="T595">three.[NOM.SG]</ta>
            <ta e="T597" id="Seg_6115" s="T596">four.[NOM.SG]</ta>
            <ta e="T598" id="Seg_6116" s="T597">five.[NOM.SG]</ta>
            <ta e="T599" id="Seg_6117" s="T598">seven.[NOM.SG]</ta>
            <ta e="T600" id="Seg_6118" s="T599">eight.[NOM.SG]</ta>
            <ta e="T602" id="Seg_6119" s="T600">nine.[NOM.SG]</ta>
            <ta e="T604" id="Seg_6120" s="T603">big.[NOM.SG]</ta>
            <ta e="T605" id="Seg_6121" s="T604">call-PST-3PL</ta>
            <ta e="T606" id="Seg_6122" s="T605">Lena.[NOM.SG] </ta>
            <ta e="T607" id="Seg_6123" s="T606">and</ta>
            <ta e="T608" id="Seg_6124" s="T607">son-ACC.3SG</ta>
            <ta e="T610" id="Seg_6125" s="T609">call-PST-3PL</ta>
            <ta e="T611" id="Seg_6126" s="T610">Dyoma.[NOM.SG] </ta>
            <ta e="T612" id="Seg_6127" s="T611">then</ta>
            <ta e="T613" id="Seg_6128" s="T612">I.NOM</ta>
            <ta e="T614" id="Seg_6129" s="T613">I.NOM</ta>
            <ta e="T616" id="Seg_6130" s="T615">I.LAT</ta>
            <ta e="T617" id="Seg_6131" s="T616">call-PST-3PL</ta>
            <ta e="T618" id="Seg_6132" s="T617">Klavdiya.[NOM.SG]</ta>
            <ta e="T619" id="Seg_6133" s="T618">I.NOM</ta>
            <ta e="T620" id="Seg_6134" s="T619">I.NOM</ta>
            <ta e="T621" id="Seg_6135" s="T620">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_6136" s="T621">call-PST-3PL</ta>
            <ta e="T623" id="Seg_6137" s="T622">Nadya.[NOM.SG]</ta>
            <ta e="T624" id="Seg_6138" s="T623">then</ta>
            <ta e="T625" id="Seg_6139" s="T624">be-PST.[3SG]</ta>
            <ta e="T626" id="Seg_6140" s="T625">Frosya.[NOM.SG]</ta>
            <ta e="T627" id="Seg_6141" s="T626">then</ta>
            <ta e="T628" id="Seg_6142" s="T627">be-PST.[3SG]</ta>
            <ta e="T629" id="Seg_6143" s="T628">Vera.[NOM.SG]</ta>
            <ta e="T630" id="Seg_6144" s="T629">then</ta>
            <ta e="T631" id="Seg_6145" s="T630">Manya.[NOM.SG]</ta>
            <ta e="T632" id="Seg_6146" s="T631">be-PST.[3SG]</ta>
            <ta e="T633" id="Seg_6147" s="T632">then</ta>
            <ta e="T634" id="Seg_6148" s="T633">boy.[NOM.SG]</ta>
            <ta e="T635" id="Seg_6149" s="T634">come-PST.[3SG]</ta>
            <ta e="T638" id="Seg_6150" s="T637">call-PST-3PL</ta>
            <ta e="T639" id="Seg_6151" s="T638">Maksim.[NOM.SG]</ta>
            <ta e="T640" id="Seg_6152" s="T639">one.[NOM.SG]</ta>
            <ta e="T641" id="Seg_6153" s="T640">boy.[NOM.SG]</ta>
            <ta e="T642" id="Seg_6154" s="T641">die-RES-PST.[3SG]</ta>
            <ta e="T643" id="Seg_6155" s="T642">I.LAT</ta>
            <ta e="T644" id="Seg_6156" s="T643">more</ta>
            <ta e="T645" id="Seg_6157" s="T644">NEG.EX.[3SG]</ta>
            <ta e="T646" id="Seg_6158" s="T645">be-PST.[3SG]</ta>
            <ta e="T647" id="Seg_6159" s="T646">and</ta>
            <ta e="T648" id="Seg_6160" s="T647">and</ta>
            <ta e="T649" id="Seg_6161" s="T648">girl.[NOM.SG]</ta>
            <ta e="T650" id="Seg_6162" s="T649">die-RES-PST.[3SG]</ta>
            <ta e="T651" id="Seg_6163" s="T650">one.[NOM.SG]</ta>
            <ta e="T652" id="Seg_6164" s="T651">two.[NOM.SG]</ta>
            <ta e="T653" id="Seg_6165" s="T652">three.[NOM.SG]</ta>
            <ta e="T654" id="Seg_6166" s="T653">four.[NOM.SG]</ta>
            <ta e="T655" id="Seg_6167" s="T654">five.[NOM.SG]</ta>
            <ta e="T656" id="Seg_6168" s="T655">six.[NOM.SG]</ta>
            <ta e="T657" id="Seg_6169" s="T656">seven.[NOM.SG]</ta>
            <ta e="T658" id="Seg_6170" s="T657">eight.[NOM.SG]</ta>
            <ta e="T659" id="Seg_6171" s="T658">nine.[NOM.SG]</ta>
            <ta e="T660" id="Seg_6172" s="T659">ten.[NOM.SG]</ta>
            <ta e="T661" id="Seg_6173" s="T660">ten.[NOM.SG]</ta>
            <ta e="T662" id="Seg_6174" s="T661">two.[NOM.SG]</ta>
            <ta e="T663" id="Seg_6175" s="T662">ten.[NOM.SG]</ta>
            <ta e="T664" id="Seg_6176" s="T663">one.[NOM.SG]</ta>
            <ta e="T665" id="Seg_6177" s="T664">ten.[NOM.SG]</ta>
            <ta e="T666" id="Seg_6178" s="T665">three.[NOM.SG]</ta>
            <ta e="T667" id="Seg_6179" s="T666">big.[NOM.SG]</ta>
            <ta e="T668" id="Seg_6180" s="T667">girl.[NOM.SG]</ta>
            <ta e="T669" id="Seg_6181" s="T668">die-RES-PST.[3SG]</ta>
            <ta e="T670" id="Seg_6182" s="T669">ten.[NOM.SG]</ta>
            <ta e="T671" id="Seg_6183" s="T670">three.[NOM.SG]</ta>
            <ta e="T672" id="Seg_6184" s="T671">winter.[NOM.SG]</ta>
            <ta e="T673" id="Seg_6185" s="T672">be-PST.[3SG]</ta>
            <ta e="T674" id="Seg_6186" s="T673">this-LAT</ta>
            <ta e="T675" id="Seg_6187" s="T674">and</ta>
            <ta e="T676" id="Seg_6188" s="T675">Frosya-ACC</ta>
            <ta e="T678" id="Seg_6189" s="T676">kill-PST.[3SG]</ta>
            <ta e="T679" id="Seg_6190" s="T678">this-GEN</ta>
            <ta e="T680" id="Seg_6191" s="T679">man.[NOM.SG]</ta>
            <ta e="T681" id="Seg_6192" s="T680">kill-PST.[3SG]</ta>
            <ta e="T682" id="Seg_6193" s="T681">this-ACC</ta>
            <ta e="T683" id="Seg_6194" s="T682">Nadya.[NOM.SG]</ta>
            <ta e="T684" id="Seg_6195" s="T683">die-RES-PST.[3SG]</ta>
            <ta e="T685" id="Seg_6196" s="T684">five.[NOM.SG]</ta>
            <ta e="T686" id="Seg_6197" s="T685">moon.[NOM.SG]</ta>
            <ta e="T699" id="Seg_6198" s="T686">go-CVB</ta>
            <ta e="T687" id="Seg_6199" s="T699">disappear-PST-3PL</ta>
            <ta e="T688" id="Seg_6200" s="T687">and</ta>
            <ta e="T689" id="Seg_6201" s="T688">we.NOM</ta>
            <ta e="T690" id="Seg_6202" s="T689">two-COLL</ta>
            <ta e="T691" id="Seg_6203" s="T690">remain-MOM-PST-1PL</ta>
            <ta e="T692" id="Seg_6204" s="T691">Klavdiya.[NOM.SG]</ta>
            <ta e="T693" id="Seg_6205" s="T692">and</ta>
            <ta e="T694" id="Seg_6206" s="T693">Maksim.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T6" id="Seg_6207" s="T5">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T8" id="Seg_6208" s="T7">гриб-PL</ta>
            <ta e="T9" id="Seg_6209" s="T8">видеть-PRS-2SG</ta>
            <ta e="T10" id="Seg_6210" s="T9">PTCL</ta>
            <ta e="T11" id="Seg_6211" s="T10">стоять-PRS-3PL</ta>
            <ta e="T12" id="Seg_6212" s="T11">там</ta>
            <ta e="T13" id="Seg_6213" s="T12">там</ta>
            <ta e="T14" id="Seg_6214" s="T13">пойти-EP-IMP.2SG</ta>
            <ta e="T15" id="Seg_6215" s="T14">а</ta>
            <ta e="T16" id="Seg_6216" s="T15">я.NOM</ta>
            <ta e="T17" id="Seg_6217" s="T16">здесь</ta>
            <ta e="T18" id="Seg_6218" s="T17">пойти-FUT-1SG</ta>
            <ta e="T19" id="Seg_6219" s="T18">надо</ta>
            <ta e="T20" id="Seg_6220" s="T19">много</ta>
            <ta e="T21" id="Seg_6221" s="T20">собирать-INF.LAT</ta>
            <ta e="T22" id="Seg_6222" s="T21">гриб-NOM/GEN/ACC.3PL</ta>
            <ta e="T28" id="Seg_6223" s="T27">дерево-PL-GEN</ta>
            <ta e="T29" id="Seg_6224" s="T28">край-LAT/LOC.3SG</ta>
            <ta e="T30" id="Seg_6225" s="T29">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T31" id="Seg_6226" s="T30">хороший-LAT.ADV</ta>
            <ta e="T32" id="Seg_6227" s="T31">NEG.AUX-IMP.2SG</ta>
            <ta e="T33" id="Seg_6228" s="T32">ловить-IMP.2SG.O</ta>
            <ta e="T34" id="Seg_6229" s="T33">зачем</ta>
            <ta e="T35" id="Seg_6230" s="T34">ловить-PST-2SG</ta>
            <ta e="T37" id="Seg_6231" s="T36">посылать-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_6232" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_6233" s="T38">люди.[NOM.SG]</ta>
            <ta e="T40" id="Seg_6234" s="T39">прийти-PST-3PL</ta>
            <ta e="T41" id="Seg_6235" s="T40">мы.LAT</ta>
            <ta e="T44" id="Seg_6236" s="T43">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T45" id="Seg_6237" s="T44">вождь.[NOM.SG]</ta>
            <ta e="T46" id="Seg_6238" s="T45">поставить-DUR-3PL</ta>
            <ta e="T47" id="Seg_6239" s="T46">этот-PL</ta>
            <ta e="T48" id="Seg_6240" s="T47">знать-3PL</ta>
            <ta e="T49" id="Seg_6241" s="T48">бумага.[NOM.SG]</ta>
            <ta e="T51" id="Seg_6242" s="T50">писать-INF.LAT</ta>
            <ta e="T52" id="Seg_6243" s="T51">а</ta>
            <ta e="T53" id="Seg_6244" s="T52">мы.NOM</ta>
            <ta e="T54" id="Seg_6245" s="T53">NEG</ta>
            <ta e="T55" id="Seg_6246" s="T54">знать-1PL</ta>
            <ta e="T56" id="Seg_6247" s="T55">тогда</ta>
            <ta e="T57" id="Seg_6248" s="T56">этот-PL</ta>
            <ta e="T58" id="Seg_6249" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_6250" s="T58">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_6251" s="T59">вождь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T61" id="Seg_6252" s="T60">стоять-PST-3PL</ta>
            <ta e="T62" id="Seg_6253" s="T61">сейчас</ta>
            <ta e="T63" id="Seg_6254" s="T62">мы.NOM</ta>
            <ta e="T64" id="Seg_6255" s="T63">PTCL</ta>
            <ta e="T65" id="Seg_6256" s="T64">русский</ta>
            <ta e="T66" id="Seg_6257" s="T65">язык.[NOM.SG]</ta>
            <ta e="T67" id="Seg_6258" s="T66">говорить-DUR-1PL</ta>
            <ta e="T68" id="Seg_6259" s="T67">русский-GEN</ta>
            <ta e="T69" id="Seg_6260" s="T68">язык-NOM/GEN.3SG</ta>
            <ta e="T70" id="Seg_6261" s="T69">учиться-DUR-1PL</ta>
            <ta e="T71" id="Seg_6262" s="T70">как</ta>
            <ta e="T72" id="Seg_6263" s="T71">ты.ACC</ta>
            <ta e="T73" id="Seg_6264" s="T72">позвать-PRS-3PL</ta>
            <ta e="T74" id="Seg_6265" s="T73">и</ta>
            <ta e="T75" id="Seg_6266" s="T74">как</ta>
            <ta e="T76" id="Seg_6267" s="T75">называть-PRS-3PL</ta>
            <ta e="T77" id="Seg_6268" s="T76">я.ACC</ta>
            <ta e="T78" id="Seg_6269" s="T77">позвать-PRS-3PL</ta>
            <ta e="T79" id="Seg_6270" s="T78">Клавдия.[NOM.SG]</ta>
            <ta e="T81" id="Seg_6271" s="T79">а</ta>
            <ta e="T82" id="Seg_6272" s="T81">а</ta>
            <ta e="T83" id="Seg_6273" s="T82">называть-PRS-3PL</ta>
            <ta e="T85" id="Seg_6274" s="T84">как</ta>
            <ta e="T86" id="Seg_6275" s="T85">прийти-PST-2SG</ta>
            <ta e="T87" id="Seg_6276" s="T86">здесь</ta>
            <ta e="T88" id="Seg_6277" s="T87">тот.[NOM.SG]</ta>
            <ta e="T89" id="Seg_6278" s="T88">я.NOM</ta>
            <ta e="T90" id="Seg_6279" s="T89">вождь.[NOM.SG]</ta>
            <ta e="T91" id="Seg_6280" s="T90">а</ta>
            <ta e="T92" id="Seg_6281" s="T91">тот.[NOM.SG]</ta>
            <ta e="T93" id="Seg_6282" s="T92">говорить-INF.LAT</ta>
            <ta e="T95" id="Seg_6283" s="T94">вождь.[NOM.SG]</ta>
            <ta e="T96" id="Seg_6284" s="T95">а</ta>
            <ta e="T97" id="Seg_6285" s="T96">я.NOM</ta>
            <ta e="T98" id="Seg_6286" s="T97">этот-PL</ta>
            <ta e="T99" id="Seg_6287" s="T98">научиться-TR-PRS-1SG</ta>
            <ta e="T100" id="Seg_6288" s="T99">ты.NOM</ta>
            <ta e="T101" id="Seg_6289" s="T100">мужчина.[NOM.SG]</ta>
            <ta e="T102" id="Seg_6290" s="T101">куда</ta>
            <ta e="T696" id="Seg_6291" s="T102">пойти-CVB</ta>
            <ta e="T103" id="Seg_6292" s="T696">исчезнуть-PST.[3SG]</ta>
            <ta e="T104" id="Seg_6293" s="T103">русский-GEN</ta>
            <ta e="T105" id="Seg_6294" s="T104">поселение-LAT</ta>
            <ta e="T106" id="Seg_6295" s="T105">я.NOM</ta>
            <ta e="T107" id="Seg_6296" s="T106">много</ta>
            <ta e="T108" id="Seg_6297" s="T107">%%-PST-2SG</ta>
            <ta e="T109" id="Seg_6298" s="T108">думать-MOM-PST-1SG</ta>
            <ta e="T110" id="Seg_6299" s="T109">говорить-INF.LAT</ta>
            <ta e="T111" id="Seg_6300" s="T110">я.NOM</ta>
            <ta e="T112" id="Seg_6301" s="T111">свинья-NOM/GEN/ACC.1SG</ta>
            <ta e="T113" id="Seg_6302" s="T112">умереть-RES-PST.[3SG]</ta>
            <ta e="T114" id="Seg_6303" s="T113">я.NOM</ta>
            <ta e="T115" id="Seg_6304" s="T114">земля.[NOM.SG]</ta>
            <ta e="T116" id="Seg_6305" s="T115">копать-PST-1SG</ta>
            <ta e="T117" id="Seg_6306" s="T116">и</ta>
            <ta e="T118" id="Seg_6307" s="T117">там</ta>
            <ta e="T120" id="Seg_6308" s="T119">этот-ACC</ta>
            <ta e="T121" id="Seg_6309" s="T120">класть-PST-1SG</ta>
            <ta e="T122" id="Seg_6310" s="T121">и</ta>
            <ta e="T123" id="Seg_6311" s="T122">земля-INS</ta>
            <ta e="T124" id="Seg_6312" s="T123">лить-PST-1SG</ta>
            <ta e="T126" id="Seg_6313" s="T125">мы.NOM</ta>
            <ta e="T127" id="Seg_6314" s="T126">PTCL</ta>
            <ta e="T130" id="Seg_6315" s="T129">быть-PST.[3SG]</ta>
            <ta e="T131" id="Seg_6316" s="T130">там</ta>
            <ta e="T132" id="Seg_6317" s="T131">много</ta>
            <ta e="T133" id="Seg_6318" s="T132">крест-PL</ta>
            <ta e="T138" id="Seg_6319" s="T137">стоять-DUR-3PL</ta>
            <ta e="T139" id="Seg_6320" s="T138">а</ta>
            <ta e="T140" id="Seg_6321" s="T139">сейчас</ta>
            <ta e="T142" id="Seg_6322" s="T140">один.[NOM.SG]=PTCL</ta>
            <ta e="T143" id="Seg_6323" s="T142">NEG.EX.[3SG]</ta>
            <ta e="T144" id="Seg_6324" s="T143">что.[NOM.SG]=INDEF</ta>
            <ta e="T147" id="Seg_6325" s="T146">там</ta>
            <ta e="T148" id="Seg_6326" s="T147">NEG.EX.[3SG]</ta>
            <ta e="T149" id="Seg_6327" s="T148">сейчас</ta>
            <ta e="T150" id="Seg_6328" s="T149">там</ta>
            <ta e="T152" id="Seg_6329" s="T151">стоять-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_6330" s="T152">сейчас</ta>
            <ta e="T154" id="Seg_6331" s="T153">мы.NOM</ta>
            <ta e="T155" id="Seg_6332" s="T154">новый.[NOM.SG]</ta>
            <ta e="T157" id="Seg_6333" s="T156">там</ta>
            <ta e="T158" id="Seg_6334" s="T157">много</ta>
            <ta e="T159" id="Seg_6335" s="T158">крест-PL</ta>
            <ta e="T160" id="Seg_6336" s="T159">стоять-PRS-3PL</ta>
            <ta e="T161" id="Seg_6337" s="T160">тоже</ta>
            <ta e="T162" id="Seg_6338" s="T161">ты.NOM</ta>
            <ta e="T164" id="Seg_6339" s="T162">бог.[NOM.SG]</ta>
            <ta e="T165" id="Seg_6340" s="T164">ты.NOM</ta>
            <ta e="T166" id="Seg_6341" s="T165">бог.[NOM.SG]</ta>
            <ta e="T167" id="Seg_6342" s="T166">бог.[NOM.SG]</ta>
            <ta e="T168" id="Seg_6343" s="T167">NEG.AUX-IMP.2SG</ta>
            <ta e="T169" id="Seg_6344" s="T168">оставить-IMP.2SG.O</ta>
            <ta e="T170" id="Seg_6345" s="T169">я.ACC</ta>
            <ta e="T171" id="Seg_6346" s="T170">NEG.AUX-IMP.2SG</ta>
            <ta e="T172" id="Seg_6347" s="T171">оставить-IMP.2SG.O</ta>
            <ta e="T173" id="Seg_6348" s="T172">я.ACC</ta>
            <ta e="T174" id="Seg_6349" s="T173">NEG.AUX-IMP.2SG</ta>
            <ta e="T175" id="Seg_6350" s="T174">выбросить-IMP.2SG</ta>
            <ta e="T176" id="Seg_6351" s="T175">я.ACC</ta>
            <ta e="T177" id="Seg_6352" s="T176">ты.NOM</ta>
            <ta e="T178" id="Seg_6353" s="T177">%%</ta>
            <ta e="T179" id="Seg_6354" s="T178">я.LAT</ta>
            <ta e="T181" id="Seg_6355" s="T180">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T182" id="Seg_6356" s="T181">ум-ADJZ.[NOM.SG]</ta>
            <ta e="T183" id="Seg_6357" s="T182">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_6358" s="T183">ум-ADJZ.[NOM.SG]</ta>
            <ta e="T185" id="Seg_6359" s="T184">ты.NOM</ta>
            <ta e="T186" id="Seg_6360" s="T185">посылать-IMP.2SG.O</ta>
            <ta e="T187" id="Seg_6361" s="T186">я.LAT</ta>
            <ta e="T189" id="Seg_6362" s="T188">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T191" id="Seg_6363" s="T190">глупый.[NOM.SG]</ta>
            <ta e="T192" id="Seg_6364" s="T191">быть-PRS.[3SG]</ta>
            <ta e="T193" id="Seg_6365" s="T192">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T194" id="Seg_6366" s="T193">глупый.[NOM.SG]</ta>
            <ta e="T195" id="Seg_6367" s="T194">быть-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_6368" s="T195">учиться-TR-IMP.2SG</ta>
            <ta e="T197" id="Seg_6369" s="T196">я.ACC</ta>
            <ta e="T198" id="Seg_6370" s="T197">восхвалять-INF.LAT</ta>
            <ta e="T199" id="Seg_6371" s="T198">ты.NOM-INS</ta>
            <ta e="T200" id="Seg_6372" s="T199">учиться-TR-IMP.2SG</ta>
            <ta e="T201" id="Seg_6373" s="T200">я.ACC</ta>
            <ta e="T202" id="Seg_6374" s="T201">восхвалять-INF.LAT</ta>
            <ta e="T204" id="Seg_6375" s="T203">ты.NOM-INS</ta>
            <ta e="T205" id="Seg_6376" s="T204">учиться-TR-IMP.2SG</ta>
            <ta e="T206" id="Seg_6377" s="T205">я.ACC</ta>
            <ta e="T207" id="Seg_6378" s="T206">дорога-NOM/GEN.3SG</ta>
            <ta e="T208" id="Seg_6379" s="T207">ты.NOM</ta>
            <ta e="T209" id="Seg_6380" s="T208">идти-INF.LAT</ta>
            <ta e="T210" id="Seg_6381" s="T209">я.NOM</ta>
            <ta e="T211" id="Seg_6382" s="T210">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T213" id="Seg_6383" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_6384" s="T213">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T215" id="Seg_6385" s="T214">учиться-PST.[3SG]</ta>
            <ta e="T216" id="Seg_6386" s="T215">нажимать-INF.LAT</ta>
            <ta e="T217" id="Seg_6387" s="T216">там</ta>
            <ta e="T218" id="Seg_6388" s="T217">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T221" id="Seg_6389" s="T220">идти-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_6390" s="T221">и</ta>
            <ta e="T223" id="Seg_6391" s="T222">еще</ta>
            <ta e="T227" id="Seg_6392" s="T226">пойти-FUT-3SG</ta>
            <ta e="T228" id="Seg_6393" s="T227">этот-GEN</ta>
            <ta e="T229" id="Seg_6394" s="T228">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T230" id="Seg_6395" s="T229">дать-INF.LAT</ta>
            <ta e="T231" id="Seg_6396" s="T230">а</ta>
            <ta e="T232" id="Seg_6397" s="T231">этот-GEN</ta>
            <ta e="T233" id="Seg_6398" s="T232">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T234" id="Seg_6399" s="T233">взять-INF.LAT</ta>
            <ta e="T235" id="Seg_6400" s="T234">надо</ta>
            <ta e="T236" id="Seg_6401" s="T235">я.LAT</ta>
            <ta e="T237" id="Seg_6402" s="T236">сеть.[NOM.SG]</ta>
            <ta e="T238" id="Seg_6403" s="T237">завязать-INF.LAT</ta>
            <ta e="T239" id="Seg_6404" s="T238">рыба.[NOM.SG]</ta>
            <ta e="T245" id="Seg_6405" s="T244">рыба.[NOM.SG]</ta>
            <ta e="T246" id="Seg_6406" s="T245">ловить-INF.LAT</ta>
            <ta e="T248" id="Seg_6407" s="T247">этот-PL</ta>
            <ta e="T249" id="Seg_6408" s="T248">камасинец-PL</ta>
            <ta e="T250" id="Seg_6409" s="T249">PTCL</ta>
            <ta e="T251" id="Seg_6410" s="T250">плот-LAT</ta>
            <ta e="T252" id="Seg_6411" s="T251">идти-DUR-3PL</ta>
            <ta e="T253" id="Seg_6412" s="T252">и</ta>
            <ta e="T254" id="Seg_6413" s="T253">сеть.[NOM.SG]</ta>
            <ta e="T255" id="Seg_6414" s="T254">выбросить-DUR-3PL</ta>
            <ta e="T256" id="Seg_6415" s="T255">и</ta>
            <ta e="T257" id="Seg_6416" s="T256">рыба.[NOM.SG]</ta>
            <ta e="T259" id="Seg_6417" s="T258">ловить-DUR-3PL</ta>
            <ta e="T260" id="Seg_6418" s="T259">камень-PL</ta>
            <ta e="T261" id="Seg_6419" s="T260">PTCL</ta>
            <ta e="T262" id="Seg_6420" s="T261">береста-LAT</ta>
            <ta e="T263" id="Seg_6421" s="T262">шить-DUR-3PL</ta>
            <ta e="T264" id="Seg_6422" s="T263">и</ta>
            <ta e="T265" id="Seg_6423" s="T264">сеть-LAT</ta>
            <ta e="T266" id="Seg_6424" s="T265">завязать-DUR-3PL</ta>
            <ta e="T267" id="Seg_6425" s="T266">этот.[NOM.SG]</ta>
            <ta e="T268" id="Seg_6426" s="T267">%%-LAT</ta>
            <ta e="T269" id="Seg_6427" s="T268">весь</ta>
            <ta e="T270" id="Seg_6428" s="T269">что.[NOM.SG]</ta>
            <ta e="T271" id="Seg_6429" s="T270">принести-DUR.[3SG]</ta>
            <ta e="T272" id="Seg_6430" s="T271">бог.[NOM.SG]</ta>
            <ta e="T273" id="Seg_6431" s="T272">или</ta>
            <ta e="T274" id="Seg_6432" s="T273">кто.[NOM.SG]</ta>
            <ta e="T275" id="Seg_6433" s="T274">NEG</ta>
            <ta e="T276" id="Seg_6434" s="T275">знать-1SG</ta>
            <ta e="T277" id="Seg_6435" s="T276">снег-LAT</ta>
            <ta e="T278" id="Seg_6436" s="T277">PTCL</ta>
            <ta e="T279" id="Seg_6437" s="T278">прыгнуть-MOM-PST-1SG</ta>
            <ta e="T280" id="Seg_6438" s="T279">часть.[NOM.SG]</ta>
            <ta e="T281" id="Seg_6439" s="T280">так</ta>
            <ta e="T282" id="Seg_6440" s="T281">PTCL</ta>
            <ta e="T697" id="Seg_6441" s="T282">пойти-CVB</ta>
            <ta e="T283" id="Seg_6442" s="T697">исчезнуть-PST-1SG</ta>
            <ta e="T284" id="Seg_6443" s="T283">я.NOM</ta>
            <ta e="T285" id="Seg_6444" s="T284">прийти-PRS-1SG</ta>
            <ta e="T286" id="Seg_6445" s="T285">снег.[NOM.SG]</ta>
            <ta e="T287" id="Seg_6446" s="T286">очень</ta>
            <ta e="T289" id="Seg_6447" s="T288">много</ta>
            <ta e="T290" id="Seg_6448" s="T289">лежать-DUR.[3SG]</ta>
            <ta e="T291" id="Seg_6449" s="T290">я.NOM</ta>
            <ta e="T292" id="Seg_6450" s="T291">пойти-PST-1SG</ta>
            <ta e="T293" id="Seg_6451" s="T292">и</ta>
            <ta e="T294" id="Seg_6452" s="T293">там</ta>
            <ta e="T295" id="Seg_6453" s="T294">прыгнуть-MOM-PST-1SG</ta>
            <ta e="T296" id="Seg_6454" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_6455" s="T296">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T298" id="Seg_6456" s="T297">я.NOM</ta>
            <ta e="T300" id="Seg_6457" s="T299">%%</ta>
            <ta e="T301" id="Seg_6458" s="T300">прийти-PST-1SG</ta>
            <ta e="T302" id="Seg_6459" s="T301">там</ta>
            <ta e="T303" id="Seg_6460" s="T302">PTCL</ta>
            <ta e="T304" id="Seg_6461" s="T303">очень</ta>
            <ta e="T305" id="Seg_6462" s="T304">грязный.[NOM.SG]</ta>
            <ta e="T307" id="Seg_6463" s="T306">много</ta>
            <ta e="T308" id="Seg_6464" s="T307">я.NOM</ta>
            <ta e="T309" id="Seg_6465" s="T308">стоять-PST-1SG</ta>
            <ta e="T310" id="Seg_6466" s="T309">и</ta>
            <ta e="T311" id="Seg_6467" s="T310">там</ta>
            <ta e="T312" id="Seg_6468" s="T311">упасть-MOM-PST-1SG</ta>
            <ta e="T313" id="Seg_6469" s="T312">PTCL</ta>
            <ta e="T315" id="Seg_6470" s="T314">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T318" id="Seg_6471" s="T317">тогда</ta>
            <ta e="T319" id="Seg_6472" s="T318">кочка-NOM/GEN/ACC.1SG</ta>
            <ta e="T320" id="Seg_6473" s="T319">рука-INS</ta>
            <ta e="T321" id="Seg_6474" s="T320">ловить-PST-1SG</ta>
            <ta e="T322" id="Seg_6475" s="T321">и</ta>
            <ta e="T323" id="Seg_6476" s="T322">вернуться-PST-1SG</ta>
            <ta e="T328" id="Seg_6477" s="T327">этот.[NOM.SG]</ta>
            <ta e="T329" id="Seg_6478" s="T328">лошадь-PL</ta>
            <ta e="T330" id="Seg_6479" s="T329">кормить-PST-3PL</ta>
            <ta e="T331" id="Seg_6480" s="T330">а</ta>
            <ta e="T332" id="Seg_6481" s="T331">тот.[NOM.SG]</ta>
            <ta e="T333" id="Seg_6482" s="T332">лошадь-PL</ta>
            <ta e="T334" id="Seg_6483" s="T333">NEG</ta>
            <ta e="T335" id="Seg_6484" s="T334">кормить-PST-3PL</ta>
            <ta e="T336" id="Seg_6485" s="T335">ты.NOM</ta>
            <ta e="T337" id="Seg_6486" s="T336">овца-PL</ta>
            <ta e="T338" id="Seg_6487" s="T337">дать-PST-2SG</ta>
            <ta e="T339" id="Seg_6488" s="T338">трава.[NOM.SG]</ta>
            <ta e="T340" id="Seg_6489" s="T339">этот-GEN.PL</ta>
            <ta e="T341" id="Seg_6490" s="T340">съесть-PST-3PL</ta>
            <ta e="T342" id="Seg_6491" s="T341">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T343" id="Seg_6492" s="T342">дать-PST-2SG</ta>
            <ta e="T344" id="Seg_6493" s="T343">трава.[NOM.SG]</ta>
            <ta e="T345" id="Seg_6494" s="T344">этот.[NOM.SG]</ta>
            <ta e="T346" id="Seg_6495" s="T345">съесть-PST.[3SG]</ta>
            <ta e="T347" id="Seg_6496" s="T346">теленок-LAT</ta>
            <ta e="T348" id="Seg_6497" s="T347">наверное</ta>
            <ta e="T349" id="Seg_6498" s="T348">NEG</ta>
            <ta e="T350" id="Seg_6499" s="T349">дать-PST-2SG</ta>
            <ta e="T351" id="Seg_6500" s="T350">дать-PST-1SG</ta>
            <ta e="T352" id="Seg_6501" s="T351">весь</ta>
            <ta e="T353" id="Seg_6502" s="T352">съесть-PST-3PL</ta>
            <ta e="T357" id="Seg_6503" s="T356">ребенок-INS</ta>
            <ta e="T358" id="Seg_6504" s="T357">кормить-PST-2SG</ta>
            <ta e="T359" id="Seg_6505" s="T358">ли</ta>
            <ta e="T360" id="Seg_6506" s="T359">этот-ACC</ta>
            <ta e="T361" id="Seg_6507" s="T360">кормить-PST-1SG</ta>
            <ta e="T362" id="Seg_6508" s="T361">и</ta>
            <ta e="T363" id="Seg_6509" s="T362">молоко.[NOM.SG]</ta>
            <ta e="T364" id="Seg_6510" s="T363">дать-PST-1SG</ta>
            <ta e="T365" id="Seg_6511" s="T364">и</ta>
            <ta e="T366" id="Seg_6512" s="T365">вода.[NOM.SG]</ta>
            <ta e="T367" id="Seg_6513" s="T366">пить-PST.[3SG]</ta>
            <ta e="T368" id="Seg_6514" s="T367">верный</ta>
            <ta e="T369" id="Seg_6515" s="T368">съесть-PST.[3SG]</ta>
            <ta e="T370" id="Seg_6516" s="T369">съесть-PST.[3SG]</ta>
            <ta e="T371" id="Seg_6517" s="T370">ребенок.[NOM.SG]</ta>
            <ta e="T372" id="Seg_6518" s="T371">NEG</ta>
            <ta e="T373" id="Seg_6519" s="T372">съесть-PRS.[3SG]</ta>
            <ta e="T374" id="Seg_6520" s="T373">NEG</ta>
            <ta e="T375" id="Seg_6521" s="T374">лгать-PRS-2SG</ta>
            <ta e="T376" id="Seg_6522" s="T375">верный</ta>
            <ta e="T377" id="Seg_6523" s="T376">сказать-PST-2SG</ta>
            <ta e="T379" id="Seg_6524" s="T377">этот-PL</ta>
            <ta e="T380" id="Seg_6525" s="T379">дерево-PL</ta>
            <ta e="T381" id="Seg_6526" s="T380">топор-INS</ta>
            <ta e="T382" id="Seg_6527" s="T381">топор-INS</ta>
            <ta e="T383" id="Seg_6528" s="T382">делать-PST-3PL</ta>
            <ta e="T384" id="Seg_6529" s="T383">и</ta>
            <ta e="T385" id="Seg_6530" s="T384">тогда</ta>
            <ta e="T386" id="Seg_6531" s="T385">вода-LAT</ta>
            <ta e="T387" id="Seg_6532" s="T386">пойти-PST-3PL</ta>
            <ta e="T388" id="Seg_6533" s="T387">и</ta>
            <ta e="T389" id="Seg_6534" s="T388">рыба.[NOM.SG]</ta>
            <ta e="T390" id="Seg_6535" s="T389">ловить-PST-3PL</ta>
            <ta e="T391" id="Seg_6536" s="T390">там</ta>
            <ta e="T394" id="Seg_6537" s="T393">PTCL</ta>
            <ta e="T395" id="Seg_6538" s="T394">один.[NOM.SG]</ta>
            <ta e="T396" id="Seg_6539" s="T395">один-LAT</ta>
            <ta e="T397" id="Seg_6540" s="T396">завязать-PST-3PL</ta>
            <ta e="T398" id="Seg_6541" s="T397">и</ta>
            <ta e="T399" id="Seg_6542" s="T398">вода-LAT</ta>
            <ta e="T400" id="Seg_6543" s="T399">пойти-PST-3PL</ta>
            <ta e="T402" id="Seg_6544" s="T401">мыть-IMP.2SG</ta>
            <ta e="T403" id="Seg_6545" s="T402">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T404" id="Seg_6546" s="T403">вода-INS</ta>
            <ta e="T405" id="Seg_6547" s="T404">и</ta>
            <ta e="T406" id="Seg_6548" s="T405">тереть-IMP.2SG.O</ta>
            <ta e="T407" id="Seg_6549" s="T406">снег.[NOM.SG]</ta>
            <ta e="T408" id="Seg_6550" s="T407">стать-FUT-2SG</ta>
            <ta e="T409" id="Seg_6551" s="T408">красивый.[NOM.SG]</ta>
            <ta e="T410" id="Seg_6552" s="T409">стать-FUT-2SG</ta>
            <ta e="T411" id="Seg_6553" s="T410">вы.NOM</ta>
            <ta e="T412" id="Seg_6554" s="T411">люди.[NOM.SG]</ta>
            <ta e="T414" id="Seg_6555" s="T413">куда</ta>
            <ta e="T415" id="Seg_6556" s="T414">идти-PST-3PL</ta>
            <ta e="T416" id="Seg_6557" s="T415">%Рыбное-LAT</ta>
            <ta e="T417" id="Seg_6558" s="T416">идти-PST-3PL</ta>
            <ta e="T420" id="Seg_6559" s="T419">много</ta>
            <ta e="T421" id="Seg_6560" s="T420">мешок-LOC</ta>
            <ta e="T422" id="Seg_6561" s="T421">PTCL</ta>
            <ta e="T423" id="Seg_6562" s="T422">соболь-PL</ta>
            <ta e="T424" id="Seg_6563" s="T423">PTCL</ta>
            <ta e="T425" id="Seg_6564" s="T424">завязать-PST.[3SG]</ta>
            <ta e="T426" id="Seg_6565" s="T425">веревка-INS</ta>
            <ta e="T427" id="Seg_6566" s="T426">и</ta>
            <ta e="T428" id="Seg_6567" s="T427">принести-PST.[3SG]</ta>
            <ta e="T429" id="Seg_6568" s="T428">дом-LAT/LOC.3SG</ta>
            <ta e="T430" id="Seg_6569" s="T429">этот</ta>
            <ta e="T431" id="Seg_6570" s="T430">мужчина.[NOM.SG]</ta>
            <ta e="T432" id="Seg_6571" s="T431">PTCL</ta>
            <ta e="T433" id="Seg_6572" s="T432">работать-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_6573" s="T433">работать-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_6574" s="T434">а</ta>
            <ta e="T436" id="Seg_6575" s="T435">что.[NOM.SG]=INDEF</ta>
            <ta e="T437" id="Seg_6576" s="T436">NEG.EX.[3SG]</ta>
            <ta e="T438" id="Seg_6577" s="T437">а</ta>
            <ta e="T439" id="Seg_6578" s="T438">тот.[NOM.SG]</ta>
            <ta e="T440" id="Seg_6579" s="T439">мужчина.[NOM.SG]</ta>
            <ta e="T441" id="Seg_6580" s="T440">PTCL</ta>
            <ta e="T442" id="Seg_6581" s="T441">NEG</ta>
            <ta e="T443" id="Seg_6582" s="T442">сильно</ta>
            <ta e="T444" id="Seg_6583" s="T443">работать-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_6584" s="T444">весь</ta>
            <ta e="T446" id="Seg_6585" s="T445">что.[NOM.SG]</ta>
            <ta e="T447" id="Seg_6586" s="T446">быть-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_6587" s="T449">NEG.AUX-IMP.2SG</ta>
            <ta e="T451" id="Seg_6588" s="T450">лежать-CNG</ta>
            <ta e="T452" id="Seg_6589" s="T451">встать-IMP.2SG</ta>
            <ta e="T453" id="Seg_6590" s="T452">и</ta>
            <ta e="T454" id="Seg_6591" s="T453">работать-EP-IMP.2SG</ta>
            <ta e="T455" id="Seg_6592" s="T454">что.[NOM.SG]</ta>
            <ta e="T456" id="Seg_6593" s="T455">лежать-INF.LAT</ta>
            <ta e="T457" id="Seg_6594" s="T456">много</ta>
            <ta e="T458" id="Seg_6595" s="T457">год.[NOM.SG]</ta>
            <ta e="T698" id="Seg_6596" s="T458">пойти-CVB</ta>
            <ta e="T459" id="Seg_6597" s="T698">исчезнуть-PST-3PL</ta>
            <ta e="T460" id="Seg_6598" s="T459">прийти-PST-3PL</ta>
            <ta e="T461" id="Seg_6599" s="T460">красный.[NOM.SG]</ta>
            <ta e="T462" id="Seg_6600" s="T461">белый-PL</ta>
            <ta e="T463" id="Seg_6601" s="T462">прийти-PST-3PL</ta>
            <ta e="T464" id="Seg_6602" s="T463">а</ta>
            <ta e="T465" id="Seg_6603" s="T464">люди.[NOM.SG]</ta>
            <ta e="T466" id="Seg_6604" s="T465">очень</ta>
            <ta e="T467" id="Seg_6605" s="T466">бояться-PRS-3PL</ta>
            <ta e="T468" id="Seg_6606" s="T467">люди.[NOM.SG]</ta>
            <ta e="T470" id="Seg_6607" s="T469">весь</ta>
            <ta e="T471" id="Seg_6608" s="T470">бежать-MOM-PST-3PL</ta>
            <ta e="T472" id="Seg_6609" s="T471">лес-LAT</ta>
            <ta e="T473" id="Seg_6610" s="T472">очень</ta>
            <ta e="T474" id="Seg_6611" s="T473">бояться-PST-3PL</ta>
            <ta e="T475" id="Seg_6612" s="T474">а</ta>
            <ta e="T476" id="Seg_6613" s="T475">когда</ta>
            <ta e="T477" id="Seg_6614" s="T476">прийти-FUT-3PL</ta>
            <ta e="T478" id="Seg_6615" s="T477">дом-LAT</ta>
            <ta e="T479" id="Seg_6616" s="T478">этот-PL</ta>
            <ta e="T480" id="Seg_6617" s="T479">PTCL</ta>
            <ta e="T482" id="Seg_6618" s="T481">прийти-PST-3PL</ta>
            <ta e="T483" id="Seg_6619" s="T482">этот-PL</ta>
            <ta e="T484" id="Seg_6620" s="T483">PTCL</ta>
            <ta e="T485" id="Seg_6621" s="T484">прийти-PST-3PL</ta>
            <ta e="T486" id="Seg_6622" s="T485">белый.[NOM.SG]</ta>
            <ta e="T487" id="Seg_6623" s="T486">люди.[NOM.SG]</ta>
            <ta e="T488" id="Seg_6624" s="T487">мы.GEN</ta>
            <ta e="T489" id="Seg_6625" s="T488">люди-NOM/GEN/ACC.1PL</ta>
            <ta e="T490" id="Seg_6626" s="T489">PTCL</ta>
            <ta e="T491" id="Seg_6627" s="T490">красный-PL</ta>
            <ta e="T493" id="Seg_6628" s="T492">жалеть-PST-3PL</ta>
            <ta e="T495" id="Seg_6629" s="T494">этот-PL-LAT</ta>
            <ta e="T496" id="Seg_6630" s="T495">хлеб.[NOM.SG]</ta>
            <ta e="T498" id="Seg_6631" s="T497">дать-PST-3PL</ta>
            <ta e="T499" id="Seg_6632" s="T498">что.[NOM.SG]</ta>
            <ta e="T500" id="Seg_6633" s="T499">быть-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_6634" s="T500">весь</ta>
            <ta e="T502" id="Seg_6635" s="T501">дать-PST-3PL</ta>
            <ta e="T503" id="Seg_6636" s="T502">соль.[NOM.SG]</ta>
            <ta e="T504" id="Seg_6637" s="T503">котел.[NOM.SG]</ta>
            <ta e="T505" id="Seg_6638" s="T504">дать-PST-3PL</ta>
            <ta e="T506" id="Seg_6639" s="T505">еще</ta>
            <ta e="T507" id="Seg_6640" s="T506">картофель.[NOM.SG]</ta>
            <ta e="T508" id="Seg_6641" s="T507">этот-PL-LAT</ta>
            <ta e="T509" id="Seg_6642" s="T508">дать-PST-3PL</ta>
            <ta e="T510" id="Seg_6643" s="T509">красный-PL</ta>
            <ta e="T511" id="Seg_6644" s="T510">PTCL</ta>
            <ta e="T512" id="Seg_6645" s="T511">лес-LOC</ta>
            <ta e="T513" id="Seg_6646" s="T512">жить-PST-3PL</ta>
            <ta e="T514" id="Seg_6647" s="T513">а</ta>
            <ta e="T515" id="Seg_6648" s="T514">вечер-LOC.ADV</ta>
            <ta e="T516" id="Seg_6649" s="T515">здесь</ta>
            <ta e="T517" id="Seg_6650" s="T516">прийти-PST-3PL</ta>
            <ta e="T518" id="Seg_6651" s="T517">есть-INF.LAT</ta>
            <ta e="T520" id="Seg_6652" s="T519">один.[NOM.SG]</ta>
            <ta e="T522" id="Seg_6653" s="T521">прийти-PST-3PL</ta>
            <ta e="T523" id="Seg_6654" s="T522">два.[NOM.SG]</ta>
            <ta e="T525" id="Seg_6655" s="T524">красный-PL</ta>
            <ta e="T526" id="Seg_6656" s="T525">ружье-INS</ta>
            <ta e="T528" id="Seg_6657" s="T527">один-LAT</ta>
            <ta e="T529" id="Seg_6658" s="T528">PTCL</ta>
            <ta e="T531" id="Seg_6659" s="T530">NEG</ta>
            <ta e="T532" id="Seg_6660" s="T531">убить-PST-3PL</ta>
            <ta e="T533" id="Seg_6661" s="T532">женщина.[NOM.SG]</ta>
            <ta e="T534" id="Seg_6662" s="T533">мы.LAT</ta>
            <ta e="T535" id="Seg_6663" s="T534">прийти-PRS.[3SG]</ta>
            <ta e="T536" id="Seg_6664" s="T535">вы.NOM</ta>
            <ta e="T537" id="Seg_6665" s="T536">PTCL</ta>
            <ta e="T538" id="Seg_6666" s="T537">здесь</ta>
            <ta e="T539" id="Seg_6667" s="T538">говорить-PRS-2PL</ta>
            <ta e="T540" id="Seg_6668" s="T539">а</ta>
            <ta e="T541" id="Seg_6669" s="T540">мужчина.[NOM.SG]</ta>
            <ta e="T542" id="Seg_6670" s="T541">окно-LOC</ta>
            <ta e="T543" id="Seg_6671" s="T542">стоять-PRS.[3SG]</ta>
            <ta e="T544" id="Seg_6672" s="T543">слушать-DUR.[3SG]</ta>
            <ta e="T545" id="Seg_6673" s="T544">мы.NOM</ta>
            <ta e="T546" id="Seg_6674" s="T545">PTCL</ta>
            <ta e="T550" id="Seg_6675" s="T549">один.[NOM.SG]</ta>
            <ta e="T552" id="Seg_6676" s="T551">вечер-LOC.ADV</ta>
            <ta e="T553" id="Seg_6677" s="T552">прийти-PST-3PL</ta>
            <ta e="T554" id="Seg_6678" s="T553">красный-PL</ta>
            <ta e="T555" id="Seg_6679" s="T554">мужчина.[NOM.SG]</ta>
            <ta e="T556" id="Seg_6680" s="T555">принести-PST-3PL</ta>
            <ta e="T557" id="Seg_6681" s="T556">дверь-GEN.3SG</ta>
            <ta e="T558" id="Seg_6682" s="T557">край-LAT/LOC.3SG</ta>
            <ta e="T559" id="Seg_6683" s="T558">сеять-PST-3PL</ta>
            <ta e="T560" id="Seg_6684" s="T559">тогда</ta>
            <ta e="T561" id="Seg_6685" s="T560">PTCL</ta>
            <ta e="T562" id="Seg_6686" s="T561">мужчина-PL</ta>
            <ta e="T563" id="Seg_6687" s="T562">собирать-PST-3PL</ta>
            <ta e="T564" id="Seg_6688" s="T563">бить-PST-3PL</ta>
            <ta e="T565" id="Seg_6689" s="T564">PTCL</ta>
            <ta e="T566" id="Seg_6690" s="T565">плетка-PL-INS</ta>
            <ta e="T567" id="Seg_6691" s="T566">один.[NOM.SG]</ta>
            <ta e="T568" id="Seg_6692" s="T567">мужчина.[NOM.SG]</ta>
            <ta e="T569" id="Seg_6693" s="T568">PTCL</ta>
            <ta e="T570" id="Seg_6694" s="T569">штаны-LAT/LOC.3SG</ta>
            <ta e="T571" id="Seg_6695" s="T570">PTCL</ta>
            <ta e="T572" id="Seg_6696" s="T571">испражняться-PST.[3SG]</ta>
            <ta e="T573" id="Seg_6697" s="T572">я.NOM</ta>
            <ta e="T574" id="Seg_6698" s="T573">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T575" id="Seg_6699" s="T574">позвать-PST-3PL</ta>
            <ta e="T576" id="Seg_6700" s="T575">Захар.[NOM.SG]</ta>
            <ta e="T577" id="Seg_6701" s="T576">а</ta>
            <ta e="T579" id="Seg_6702" s="T578">называть-MOM-PST-3PL</ta>
            <ta e="T580" id="Seg_6703" s="T579">Степанович.[NOM.SG]</ta>
            <ta e="T581" id="Seg_6704" s="T580">а</ta>
            <ta e="T582" id="Seg_6705" s="T581">мать-NOM/GEN/ACC.1SG </ta>
            <ta e="T583" id="Seg_6706" s="T582">Афанасия.[NOM.SG] </ta>
            <ta e="T584" id="Seg_6707" s="T583">называть-MOM-PST-3PL</ta>
            <ta e="T585" id="Seg_6708" s="T584">Антоновна.[NOM.SG] </ta>
            <ta e="T586" id="Seg_6709" s="T585">а</ta>
            <ta e="T587" id="Seg_6710" s="T586">фамилия.[NOM.SG] </ta>
            <ta e="T588" id="Seg_6711" s="T587">этот-GEN.PL</ta>
            <ta e="T589" id="Seg_6712" s="T588">быть-PST.[3SG]</ta>
            <ta e="T590" id="Seg_6713" s="T589">Анджигатов-PL</ta>
            <ta e="T591" id="Seg_6714" s="T590">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T592" id="Seg_6715" s="T591">быть-PST.[3SG]</ta>
            <ta e="T594" id="Seg_6716" s="T593">один.[NOM.SG]</ta>
            <ta e="T595" id="Seg_6717" s="T594">два.[NOM.SG]</ta>
            <ta e="T596" id="Seg_6718" s="T595">три.[NOM.SG]</ta>
            <ta e="T597" id="Seg_6719" s="T596">четыре.[NOM.SG]</ta>
            <ta e="T598" id="Seg_6720" s="T597">пять.[NOM.SG]</ta>
            <ta e="T599" id="Seg_6721" s="T598">семь.[NOM.SG]</ta>
            <ta e="T600" id="Seg_6722" s="T599">восемь.[NOM.SG]</ta>
            <ta e="T602" id="Seg_6723" s="T600">девять.[NOM.SG]</ta>
            <ta e="T604" id="Seg_6724" s="T603">большой.[NOM.SG]</ta>
            <ta e="T605" id="Seg_6725" s="T604">позвать-PST-3PL</ta>
            <ta e="T606" id="Seg_6726" s="T605">Лена.[NOM.SG] </ta>
            <ta e="T607" id="Seg_6727" s="T606">а</ta>
            <ta e="T608" id="Seg_6728" s="T607">сын-ACC.3SG</ta>
            <ta e="T610" id="Seg_6729" s="T609">позвать-PST-3PL</ta>
            <ta e="T611" id="Seg_6730" s="T610">Дёма.[NOM.SG] </ta>
            <ta e="T612" id="Seg_6731" s="T611">тогда</ta>
            <ta e="T613" id="Seg_6732" s="T612">я.NOM</ta>
            <ta e="T614" id="Seg_6733" s="T613">я.NOM</ta>
            <ta e="T616" id="Seg_6734" s="T615">я.LAT</ta>
            <ta e="T617" id="Seg_6735" s="T616">позвать-PST-3PL</ta>
            <ta e="T618" id="Seg_6736" s="T617">Клавдия.[NOM.SG]</ta>
            <ta e="T619" id="Seg_6737" s="T618">я.NOM</ta>
            <ta e="T620" id="Seg_6738" s="T619">я.NOM</ta>
            <ta e="T621" id="Seg_6739" s="T620">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_6740" s="T621">позвать-PST-3PL</ta>
            <ta e="T623" id="Seg_6741" s="T622">Надя.[NOM.SG]</ta>
            <ta e="T624" id="Seg_6742" s="T623">тогда</ta>
            <ta e="T625" id="Seg_6743" s="T624">быть-PST.[3SG]</ta>
            <ta e="T626" id="Seg_6744" s="T625">Фрося.[NOM.SG]</ta>
            <ta e="T627" id="Seg_6745" s="T626">тогда</ta>
            <ta e="T628" id="Seg_6746" s="T627">быть-PST.[3SG]</ta>
            <ta e="T629" id="Seg_6747" s="T628">Вера.[NOM.SG]</ta>
            <ta e="T630" id="Seg_6748" s="T629">тогда</ta>
            <ta e="T631" id="Seg_6749" s="T630">Маня.[NOM.SG]</ta>
            <ta e="T632" id="Seg_6750" s="T631">быть-PST.[3SG]</ta>
            <ta e="T633" id="Seg_6751" s="T632">тогда</ta>
            <ta e="T634" id="Seg_6752" s="T633">мальчик.[NOM.SG]</ta>
            <ta e="T635" id="Seg_6753" s="T634">прийти-PST.[3SG]</ta>
            <ta e="T638" id="Seg_6754" s="T637">позвать-PST-3PL</ta>
            <ta e="T639" id="Seg_6755" s="T638">Максим.[NOM.SG]</ta>
            <ta e="T640" id="Seg_6756" s="T639">один.[NOM.SG]</ta>
            <ta e="T641" id="Seg_6757" s="T640">мальчик.[NOM.SG]</ta>
            <ta e="T642" id="Seg_6758" s="T641">умереть-RES-PST.[3SG]</ta>
            <ta e="T643" id="Seg_6759" s="T642">я.LAT</ta>
            <ta e="T644" id="Seg_6760" s="T643">еще</ta>
            <ta e="T645" id="Seg_6761" s="T644">NEG.EX.[3SG]</ta>
            <ta e="T646" id="Seg_6762" s="T645">быть-PST.[3SG]</ta>
            <ta e="T647" id="Seg_6763" s="T646">а</ta>
            <ta e="T648" id="Seg_6764" s="T647">а</ta>
            <ta e="T649" id="Seg_6765" s="T648">девушка.[NOM.SG]</ta>
            <ta e="T650" id="Seg_6766" s="T649">умереть-RES-PST.[3SG]</ta>
            <ta e="T651" id="Seg_6767" s="T650">один.[NOM.SG]</ta>
            <ta e="T652" id="Seg_6768" s="T651">два.[NOM.SG]</ta>
            <ta e="T653" id="Seg_6769" s="T652">три.[NOM.SG]</ta>
            <ta e="T654" id="Seg_6770" s="T653">четыре.[NOM.SG]</ta>
            <ta e="T655" id="Seg_6771" s="T654">пять.[NOM.SG]</ta>
            <ta e="T656" id="Seg_6772" s="T655">шесть.[NOM.SG]</ta>
            <ta e="T657" id="Seg_6773" s="T656">семь.[NOM.SG]</ta>
            <ta e="T658" id="Seg_6774" s="T657">восемь.[NOM.SG]</ta>
            <ta e="T659" id="Seg_6775" s="T658">девять.[NOM.SG]</ta>
            <ta e="T660" id="Seg_6776" s="T659">десять.[NOM.SG]</ta>
            <ta e="T661" id="Seg_6777" s="T660">десять.[NOM.SG]</ta>
            <ta e="T662" id="Seg_6778" s="T661">два.[NOM.SG]</ta>
            <ta e="T663" id="Seg_6779" s="T662">десять.[NOM.SG]</ta>
            <ta e="T664" id="Seg_6780" s="T663">один.[NOM.SG]</ta>
            <ta e="T665" id="Seg_6781" s="T664">десять.[NOM.SG]</ta>
            <ta e="T666" id="Seg_6782" s="T665">три.[NOM.SG]</ta>
            <ta e="T667" id="Seg_6783" s="T666">большой.[NOM.SG]</ta>
            <ta e="T668" id="Seg_6784" s="T667">девушка.[NOM.SG]</ta>
            <ta e="T669" id="Seg_6785" s="T668">умереть-RES-PST.[3SG]</ta>
            <ta e="T670" id="Seg_6786" s="T669">десять.[NOM.SG]</ta>
            <ta e="T671" id="Seg_6787" s="T670">три.[NOM.SG]</ta>
            <ta e="T672" id="Seg_6788" s="T671">зима.[NOM.SG]</ta>
            <ta e="T673" id="Seg_6789" s="T672">быть-PST.[3SG]</ta>
            <ta e="T674" id="Seg_6790" s="T673">этот-LAT</ta>
            <ta e="T675" id="Seg_6791" s="T674">а</ta>
            <ta e="T676" id="Seg_6792" s="T675">Фрося-ACC</ta>
            <ta e="T678" id="Seg_6793" s="T676">убить-PST.[3SG]</ta>
            <ta e="T679" id="Seg_6794" s="T678">этот-GEN</ta>
            <ta e="T680" id="Seg_6795" s="T679">мужчина.[NOM.SG]</ta>
            <ta e="T681" id="Seg_6796" s="T680">убить-PST.[3SG]</ta>
            <ta e="T682" id="Seg_6797" s="T681">этот-ACC</ta>
            <ta e="T683" id="Seg_6798" s="T682">Надя.[NOM.SG]</ta>
            <ta e="T684" id="Seg_6799" s="T683">умереть-RES-PST.[3SG]</ta>
            <ta e="T685" id="Seg_6800" s="T684">пять.[NOM.SG]</ta>
            <ta e="T686" id="Seg_6801" s="T685">луна.[NOM.SG]</ta>
            <ta e="T699" id="Seg_6802" s="T686">пойти-CVB</ta>
            <ta e="T687" id="Seg_6803" s="T699">исчезнуть-PST-3PL</ta>
            <ta e="T688" id="Seg_6804" s="T687">а</ta>
            <ta e="T689" id="Seg_6805" s="T688">мы.NOM</ta>
            <ta e="T690" id="Seg_6806" s="T689">два-COLL</ta>
            <ta e="T691" id="Seg_6807" s="T690">остаться-MOM-PST-1PL</ta>
            <ta e="T692" id="Seg_6808" s="T691">Клавдия.[NOM.SG]</ta>
            <ta e="T693" id="Seg_6809" s="T692">и</ta>
            <ta e="T694" id="Seg_6810" s="T693">Максим.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T6" id="Seg_6811" s="T5">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T8" id="Seg_6812" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_6813" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_6814" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_6815" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_6816" s="T11">adv</ta>
            <ta e="T13" id="Seg_6817" s="T12">adv</ta>
            <ta e="T14" id="Seg_6818" s="T13">v-v:ins-v:mood.pn</ta>
            <ta e="T15" id="Seg_6819" s="T14">conj</ta>
            <ta e="T16" id="Seg_6820" s="T15">pers</ta>
            <ta e="T17" id="Seg_6821" s="T16">adv</ta>
            <ta e="T18" id="Seg_6822" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_6823" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_6824" s="T19">quant</ta>
            <ta e="T21" id="Seg_6825" s="T20">v-v:n.fin</ta>
            <ta e="T22" id="Seg_6826" s="T21">n-n:case.poss</ta>
            <ta e="T28" id="Seg_6827" s="T27">n-n:num-n:case</ta>
            <ta e="T29" id="Seg_6828" s="T28">n-n:case.poss</ta>
            <ta e="T30" id="Seg_6829" s="T29">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T31" id="Seg_6830" s="T30">adj-adj&gt;adv</ta>
            <ta e="T32" id="Seg_6831" s="T31">aux-v:mood.pn</ta>
            <ta e="T33" id="Seg_6832" s="T32">v-v:mood.pn</ta>
            <ta e="T34" id="Seg_6833" s="T33">adv</ta>
            <ta e="T35" id="Seg_6834" s="T34">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_6835" s="T36">v-v:mood.pn</ta>
            <ta e="T38" id="Seg_6836" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_6837" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_6838" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_6839" s="T40">pers</ta>
            <ta e="T44" id="Seg_6840" s="T43">refl-n:case.poss</ta>
            <ta e="T45" id="Seg_6841" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_6842" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_6843" s="T46">dempro-n:num</ta>
            <ta e="T48" id="Seg_6844" s="T47">v-v:pn</ta>
            <ta e="T49" id="Seg_6845" s="T48">n-n:case</ta>
            <ta e="T51" id="Seg_6846" s="T50">v-v:n.fin</ta>
            <ta e="T52" id="Seg_6847" s="T51">conj</ta>
            <ta e="T53" id="Seg_6848" s="T52">pers</ta>
            <ta e="T54" id="Seg_6849" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_6850" s="T54">v-v:pn</ta>
            <ta e="T56" id="Seg_6851" s="T55">adv</ta>
            <ta e="T57" id="Seg_6852" s="T56">dempro-n:num</ta>
            <ta e="T58" id="Seg_6853" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_6854" s="T58">refl-n:case.poss</ta>
            <ta e="T60" id="Seg_6855" s="T59">n-n:num-n:case.poss</ta>
            <ta e="T61" id="Seg_6856" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_6857" s="T61">adv</ta>
            <ta e="T63" id="Seg_6858" s="T62">pers</ta>
            <ta e="T64" id="Seg_6859" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_6860" s="T64">n</ta>
            <ta e="T66" id="Seg_6861" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_6862" s="T66">v-v&gt;v-v:pn</ta>
            <ta e="T68" id="Seg_6863" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_6864" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_6865" s="T69">v-v&gt;v-v:pn</ta>
            <ta e="T71" id="Seg_6866" s="T70">que</ta>
            <ta e="T72" id="Seg_6867" s="T71">pers</ta>
            <ta e="T73" id="Seg_6868" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_6869" s="T73">conj</ta>
            <ta e="T75" id="Seg_6870" s="T74">que</ta>
            <ta e="T76" id="Seg_6871" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_6872" s="T76">pers</ta>
            <ta e="T78" id="Seg_6873" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_6874" s="T78">propr.[n:case]</ta>
            <ta e="T81" id="Seg_6875" s="T79">conj</ta>
            <ta e="T82" id="Seg_6876" s="T81">conj</ta>
            <ta e="T83" id="Seg_6877" s="T82">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_6878" s="T84">que</ta>
            <ta e="T86" id="Seg_6879" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_6880" s="T86">adv</ta>
            <ta e="T88" id="Seg_6881" s="T87">dempro-n:case</ta>
            <ta e="T89" id="Seg_6882" s="T88">pers</ta>
            <ta e="T90" id="Seg_6883" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_6884" s="T90">conj</ta>
            <ta e="T92" id="Seg_6885" s="T91">dempro-n:case</ta>
            <ta e="T93" id="Seg_6886" s="T92">v-v:n.fin</ta>
            <ta e="T95" id="Seg_6887" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_6888" s="T95">conj</ta>
            <ta e="T97" id="Seg_6889" s="T96">pers</ta>
            <ta e="T98" id="Seg_6890" s="T97">dempro-n:num</ta>
            <ta e="T99" id="Seg_6891" s="T98">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_6892" s="T99">pers</ta>
            <ta e="T101" id="Seg_6893" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_6894" s="T101">que</ta>
            <ta e="T696" id="Seg_6895" s="T102">v-v:n-fin</ta>
            <ta e="T103" id="Seg_6896" s="T696">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_6897" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6898" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_6899" s="T105">pers</ta>
            <ta e="T107" id="Seg_6900" s="T106">quant</ta>
            <ta e="T108" id="Seg_6901" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_6902" s="T108">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_6903" s="T109">v-v:n.fin</ta>
            <ta e="T111" id="Seg_6904" s="T110">pers</ta>
            <ta e="T112" id="Seg_6905" s="T111">n-n:case.poss</ta>
            <ta e="T113" id="Seg_6906" s="T112">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_6907" s="T113">pers</ta>
            <ta e="T115" id="Seg_6908" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_6909" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_6910" s="T116">conj</ta>
            <ta e="T118" id="Seg_6911" s="T117">adv</ta>
            <ta e="T120" id="Seg_6912" s="T119">dempro-n:case</ta>
            <ta e="T121" id="Seg_6913" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_6914" s="T121">conj</ta>
            <ta e="T123" id="Seg_6915" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_6916" s="T123">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_6917" s="T125">pers</ta>
            <ta e="T127" id="Seg_6918" s="T126">ptcl</ta>
            <ta e="T130" id="Seg_6919" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_6920" s="T130">adv</ta>
            <ta e="T132" id="Seg_6921" s="T131">quant</ta>
            <ta e="T133" id="Seg_6922" s="T132">n-n:num</ta>
            <ta e="T138" id="Seg_6923" s="T137">v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_6924" s="T138">conj</ta>
            <ta e="T140" id="Seg_6925" s="T139">adv</ta>
            <ta e="T142" id="Seg_6926" s="T140">num-n:case=ptcl</ta>
            <ta e="T143" id="Seg_6927" s="T142">v-v:pn</ta>
            <ta e="T144" id="Seg_6928" s="T143">que-n:case=ptcl</ta>
            <ta e="T147" id="Seg_6929" s="T146">adv</ta>
            <ta e="T148" id="Seg_6930" s="T147">v-v:pn</ta>
            <ta e="T149" id="Seg_6931" s="T148">adv</ta>
            <ta e="T150" id="Seg_6932" s="T149">adv</ta>
            <ta e="T152" id="Seg_6933" s="T151">v-v&gt;v-v:pn</ta>
            <ta e="T153" id="Seg_6934" s="T152">adv</ta>
            <ta e="T154" id="Seg_6935" s="T153">pers</ta>
            <ta e="T155" id="Seg_6936" s="T154">adj-n:case</ta>
            <ta e="T157" id="Seg_6937" s="T156">adv</ta>
            <ta e="T158" id="Seg_6938" s="T157">quant</ta>
            <ta e="T159" id="Seg_6939" s="T158">n-n:num</ta>
            <ta e="T160" id="Seg_6940" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_6941" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_6942" s="T161">pers</ta>
            <ta e="T164" id="Seg_6943" s="T162">n-n:case</ta>
            <ta e="T165" id="Seg_6944" s="T164">pers</ta>
            <ta e="T166" id="Seg_6945" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_6946" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_6947" s="T167">aux-v:mood.pn</ta>
            <ta e="T169" id="Seg_6948" s="T168">v-v:mood.pn</ta>
            <ta e="T170" id="Seg_6949" s="T169">pers</ta>
            <ta e="T171" id="Seg_6950" s="T170">aux-v:mood.pn</ta>
            <ta e="T172" id="Seg_6951" s="T171">v-v:mood.pn</ta>
            <ta e="T173" id="Seg_6952" s="T172">pers</ta>
            <ta e="T174" id="Seg_6953" s="T173">aux-v:mood.pn</ta>
            <ta e="T175" id="Seg_6954" s="T174">v-v:mood.pn</ta>
            <ta e="T176" id="Seg_6955" s="T175">pers</ta>
            <ta e="T177" id="Seg_6956" s="T176">pers</ta>
            <ta e="T178" id="Seg_6957" s="T177">%%</ta>
            <ta e="T179" id="Seg_6958" s="T178">pers</ta>
            <ta e="T181" id="Seg_6959" s="T180">n-n:case.poss</ta>
            <ta e="T182" id="Seg_6960" s="T181">n-n&gt;adj-n:case</ta>
            <ta e="T183" id="Seg_6961" s="T182">n-n:case.poss</ta>
            <ta e="T184" id="Seg_6962" s="T183">n-n&gt;adj-n:case</ta>
            <ta e="T185" id="Seg_6963" s="T184">pers</ta>
            <ta e="T186" id="Seg_6964" s="T185">v-v:mood.pn</ta>
            <ta e="T187" id="Seg_6965" s="T186">pers</ta>
            <ta e="T189" id="Seg_6966" s="T188">n-n:case.poss</ta>
            <ta e="T191" id="Seg_6967" s="T190">adj-n:case</ta>
            <ta e="T192" id="Seg_6968" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_6969" s="T192">n-n:case.poss</ta>
            <ta e="T194" id="Seg_6970" s="T193">adj-n:case</ta>
            <ta e="T195" id="Seg_6971" s="T194">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_6972" s="T195">v-v&gt;v-v:mood.pn</ta>
            <ta e="T197" id="Seg_6973" s="T196">pers</ta>
            <ta e="T198" id="Seg_6974" s="T197">v-v:n.fin</ta>
            <ta e="T199" id="Seg_6975" s="T198">pers-n:case</ta>
            <ta e="T200" id="Seg_6976" s="T199">v-v&gt;v-v:mood.pn</ta>
            <ta e="T201" id="Seg_6977" s="T200">pers</ta>
            <ta e="T202" id="Seg_6978" s="T201">v-v:n.fin</ta>
            <ta e="T204" id="Seg_6979" s="T203">pers-n:case</ta>
            <ta e="T205" id="Seg_6980" s="T204">v-v&gt;v-v:mood.pn</ta>
            <ta e="T206" id="Seg_6981" s="T205">pers</ta>
            <ta e="T207" id="Seg_6982" s="T206">n-n:case.poss</ta>
            <ta e="T208" id="Seg_6983" s="T207">pers</ta>
            <ta e="T209" id="Seg_6984" s="T208">v-v:n.fin</ta>
            <ta e="T210" id="Seg_6985" s="T209">pers</ta>
            <ta e="T211" id="Seg_6986" s="T210">n-n:case.poss</ta>
            <ta e="T213" id="Seg_6987" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_6988" s="T213">refl-n:case.poss</ta>
            <ta e="T215" id="Seg_6989" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_6990" s="T215">v-v:n.fin</ta>
            <ta e="T217" id="Seg_6991" s="T216">adv</ta>
            <ta e="T218" id="Seg_6992" s="T217">n-n:case.poss</ta>
            <ta e="T221" id="Seg_6993" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_6994" s="T221">conj</ta>
            <ta e="T223" id="Seg_6995" s="T222">adv</ta>
            <ta e="T227" id="Seg_6996" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_6997" s="T227">dempro-n:case</ta>
            <ta e="T229" id="Seg_6998" s="T228">n-n:case.poss</ta>
            <ta e="T230" id="Seg_6999" s="T229">v-v:n.fin</ta>
            <ta e="T231" id="Seg_7000" s="T230">conj</ta>
            <ta e="T232" id="Seg_7001" s="T231">dempro-n:case</ta>
            <ta e="T233" id="Seg_7002" s="T232">n-n:case.poss</ta>
            <ta e="T234" id="Seg_7003" s="T233">v-v:n.fin</ta>
            <ta e="T235" id="Seg_7004" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7005" s="T235">pers</ta>
            <ta e="T237" id="Seg_7006" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_7007" s="T237">v-v:n.fin</ta>
            <ta e="T239" id="Seg_7008" s="T238">n-n:case</ta>
            <ta e="T245" id="Seg_7009" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_7010" s="T245">v-v:n.fin</ta>
            <ta e="T248" id="Seg_7011" s="T247">dempro-n:num</ta>
            <ta e="T249" id="Seg_7012" s="T248">n-n:num</ta>
            <ta e="T250" id="Seg_7013" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_7014" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_7015" s="T251">v-v&gt;v-v:pn</ta>
            <ta e="T253" id="Seg_7016" s="T252">conj</ta>
            <ta e="T254" id="Seg_7017" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_7018" s="T254">v-v&gt;v-v:pn</ta>
            <ta e="T256" id="Seg_7019" s="T255">conj</ta>
            <ta e="T257" id="Seg_7020" s="T256">n-n:case</ta>
            <ta e="T259" id="Seg_7021" s="T258">v-v&gt;v-v:pn</ta>
            <ta e="T260" id="Seg_7022" s="T259">n-n:num</ta>
            <ta e="T261" id="Seg_7023" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_7024" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_7025" s="T262">v-v&gt;v-v:pn</ta>
            <ta e="T264" id="Seg_7026" s="T263">conj</ta>
            <ta e="T265" id="Seg_7027" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_7028" s="T265">v-v&gt;v-v:pn</ta>
            <ta e="T267" id="Seg_7029" s="T266">dempro-n:case</ta>
            <ta e="T268" id="Seg_7030" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_7031" s="T268">quant</ta>
            <ta e="T270" id="Seg_7032" s="T269">que-n:case</ta>
            <ta e="T271" id="Seg_7033" s="T270">v-v&gt;v-v:pn</ta>
            <ta e="T272" id="Seg_7034" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_7035" s="T272">conj</ta>
            <ta e="T274" id="Seg_7036" s="T273">que-n:case</ta>
            <ta e="T275" id="Seg_7037" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_7038" s="T275">v-v:pn</ta>
            <ta e="T277" id="Seg_7039" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_7040" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_7041" s="T278">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_7042" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_7043" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_7044" s="T281">ptcl</ta>
            <ta e="T697" id="Seg_7045" s="T282">v-v:n-fin</ta>
            <ta e="T283" id="Seg_7046" s="T697">v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_7047" s="T283">pers</ta>
            <ta e="T285" id="Seg_7048" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_7049" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_7050" s="T286">adv</ta>
            <ta e="T289" id="Seg_7051" s="T288">quant</ta>
            <ta e="T290" id="Seg_7052" s="T289">v-v&gt;v-v:pn</ta>
            <ta e="T291" id="Seg_7053" s="T290">pers</ta>
            <ta e="T292" id="Seg_7054" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_7055" s="T292">conj</ta>
            <ta e="T294" id="Seg_7056" s="T293">adv</ta>
            <ta e="T295" id="Seg_7057" s="T294">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_7058" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_7059" s="T296">n-n:case.poss</ta>
            <ta e="T298" id="Seg_7060" s="T297">pers</ta>
            <ta e="T300" id="Seg_7061" s="T299">%%</ta>
            <ta e="T301" id="Seg_7062" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_7063" s="T301">adv</ta>
            <ta e="T303" id="Seg_7064" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_7065" s="T303">adv</ta>
            <ta e="T305" id="Seg_7066" s="T304">adj-n:case</ta>
            <ta e="T307" id="Seg_7067" s="T306">quant</ta>
            <ta e="T308" id="Seg_7068" s="T307">pers</ta>
            <ta e="T309" id="Seg_7069" s="T308">v-v:tense-v:pn</ta>
            <ta e="T310" id="Seg_7070" s="T309">conj</ta>
            <ta e="T311" id="Seg_7071" s="T310">adv</ta>
            <ta e="T312" id="Seg_7072" s="T311">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_7073" s="T312">ptcl</ta>
            <ta e="T315" id="Seg_7074" s="T314">n-n:num-n:case.poss</ta>
            <ta e="T318" id="Seg_7075" s="T317">adv</ta>
            <ta e="T319" id="Seg_7076" s="T318">n-n:case.poss</ta>
            <ta e="T320" id="Seg_7077" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_7078" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_7079" s="T321">conj</ta>
            <ta e="T323" id="Seg_7080" s="T322">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_7081" s="T327">dempro-n:case</ta>
            <ta e="T329" id="Seg_7082" s="T328">n-n:num</ta>
            <ta e="T330" id="Seg_7083" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_7084" s="T330">conj</ta>
            <ta e="T332" id="Seg_7085" s="T331">dempro-n:case</ta>
            <ta e="T333" id="Seg_7086" s="T332">n-n:num</ta>
            <ta e="T334" id="Seg_7087" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_7088" s="T334">v-v:tense-v:pn</ta>
            <ta e="T336" id="Seg_7089" s="T335">pers</ta>
            <ta e="T337" id="Seg_7090" s="T336">n-n:num</ta>
            <ta e="T338" id="Seg_7091" s="T337">v-v:tense-v:pn</ta>
            <ta e="T339" id="Seg_7092" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_7093" s="T339">dempro-n:case</ta>
            <ta e="T341" id="Seg_7094" s="T340">v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_7095" s="T341">n-n:case.poss</ta>
            <ta e="T343" id="Seg_7096" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_7097" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_7098" s="T344">dempro-n:case</ta>
            <ta e="T346" id="Seg_7099" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_7100" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_7101" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_7102" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_7103" s="T349">v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_7104" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_7105" s="T351">quant</ta>
            <ta e="T353" id="Seg_7106" s="T352">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_7107" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_7108" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_7109" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_7110" s="T359">dempro-n:case</ta>
            <ta e="T361" id="Seg_7111" s="T360">v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_7112" s="T361">conj</ta>
            <ta e="T363" id="Seg_7113" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_7114" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_7115" s="T364">conj</ta>
            <ta e="T366" id="Seg_7116" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_7117" s="T366">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_7118" s="T367">adj</ta>
            <ta e="T369" id="Seg_7119" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_7120" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_7121" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_7122" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_7123" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_7124" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_7125" s="T374">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_7126" s="T375">adj</ta>
            <ta e="T377" id="Seg_7127" s="T376">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_7128" s="T377">dempro-n:num</ta>
            <ta e="T380" id="Seg_7129" s="T379">n-n:num</ta>
            <ta e="T381" id="Seg_7130" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_7131" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_7132" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_7133" s="T383">conj</ta>
            <ta e="T385" id="Seg_7134" s="T384">adv</ta>
            <ta e="T386" id="Seg_7135" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_7136" s="T386">v-v:tense-v:pn</ta>
            <ta e="T388" id="Seg_7137" s="T387">conj</ta>
            <ta e="T389" id="Seg_7138" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_7139" s="T389">v-v:tense-v:pn</ta>
            <ta e="T391" id="Seg_7140" s="T390">adv</ta>
            <ta e="T394" id="Seg_7141" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_7142" s="T394">num-n:case</ta>
            <ta e="T396" id="Seg_7143" s="T395">num-n:case</ta>
            <ta e="T397" id="Seg_7144" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_7145" s="T397">conj</ta>
            <ta e="T399" id="Seg_7146" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_7147" s="T399">v-v:tense-v:pn</ta>
            <ta e="T402" id="Seg_7148" s="T401">v-v:mood.pn</ta>
            <ta e="T403" id="Seg_7149" s="T402">n-n:case.poss</ta>
            <ta e="T404" id="Seg_7150" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_7151" s="T404">conj</ta>
            <ta e="T406" id="Seg_7152" s="T405">v-v:mood.pn</ta>
            <ta e="T407" id="Seg_7153" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_7154" s="T407">v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_7155" s="T408">adj-n:case</ta>
            <ta e="T410" id="Seg_7156" s="T409">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_7157" s="T410">pers</ta>
            <ta e="T412" id="Seg_7158" s="T411">n-n:case</ta>
            <ta e="T414" id="Seg_7159" s="T413">que</ta>
            <ta e="T415" id="Seg_7160" s="T414">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_7161" s="T415">propr-n:case</ta>
            <ta e="T417" id="Seg_7162" s="T416">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_7163" s="T419">quant</ta>
            <ta e="T421" id="Seg_7164" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_7165" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7166" s="T422">n-n:num</ta>
            <ta e="T424" id="Seg_7167" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_7168" s="T424">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_7169" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_7170" s="T426">conj</ta>
            <ta e="T428" id="Seg_7171" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_7172" s="T428">n-n:case.poss</ta>
            <ta e="T430" id="Seg_7173" s="T429">dempro</ta>
            <ta e="T431" id="Seg_7174" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_7175" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_7176" s="T432">v-v:tense-v:pn</ta>
            <ta e="T434" id="Seg_7177" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_7178" s="T434">conj</ta>
            <ta e="T436" id="Seg_7179" s="T435">que-n:case=ptcl</ta>
            <ta e="T437" id="Seg_7180" s="T436">v-v:pn</ta>
            <ta e="T438" id="Seg_7181" s="T437">conj</ta>
            <ta e="T439" id="Seg_7182" s="T438">dempro-n:case</ta>
            <ta e="T440" id="Seg_7183" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_7184" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_7185" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_7186" s="T442">adv</ta>
            <ta e="T444" id="Seg_7187" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_7188" s="T444">quant</ta>
            <ta e="T446" id="Seg_7189" s="T445">que-n:case</ta>
            <ta e="T447" id="Seg_7190" s="T446">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_7191" s="T449">aux-v:mood.pn</ta>
            <ta e="T451" id="Seg_7192" s="T450">v-v:n.fin</ta>
            <ta e="T452" id="Seg_7193" s="T451">v-v:mood.pn</ta>
            <ta e="T453" id="Seg_7194" s="T452">conj</ta>
            <ta e="T454" id="Seg_7195" s="T453">v-v:ins-v:mood.pn</ta>
            <ta e="T455" id="Seg_7196" s="T454">que-n:case</ta>
            <ta e="T456" id="Seg_7197" s="T455">v-v:n.fin</ta>
            <ta e="T457" id="Seg_7198" s="T456">quant</ta>
            <ta e="T458" id="Seg_7199" s="T457">n-n:case</ta>
            <ta e="T698" id="Seg_7200" s="T458">v-v:n-fin</ta>
            <ta e="T459" id="Seg_7201" s="T698">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_7202" s="T459">v-v:tense-v:pn</ta>
            <ta e="T461" id="Seg_7203" s="T460">adj-n:case</ta>
            <ta e="T462" id="Seg_7204" s="T461">adj-n:num</ta>
            <ta e="T463" id="Seg_7205" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_7206" s="T463">conj</ta>
            <ta e="T465" id="Seg_7207" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_7208" s="T465">adv</ta>
            <ta e="T467" id="Seg_7209" s="T466">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_7210" s="T467">n-n:case</ta>
            <ta e="T470" id="Seg_7211" s="T469">quant</ta>
            <ta e="T471" id="Seg_7212" s="T470">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_7213" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_7214" s="T472">adv</ta>
            <ta e="T474" id="Seg_7215" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_7216" s="T474">conj</ta>
            <ta e="T476" id="Seg_7217" s="T475">que</ta>
            <ta e="T477" id="Seg_7218" s="T476">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_7219" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_7220" s="T478">dempro-n:num</ta>
            <ta e="T480" id="Seg_7221" s="T479">ptcl</ta>
            <ta e="T482" id="Seg_7222" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_7223" s="T482">dempro-n:num</ta>
            <ta e="T484" id="Seg_7224" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_7225" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_7226" s="T485">adj-n:case</ta>
            <ta e="T487" id="Seg_7227" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_7228" s="T487">pers</ta>
            <ta e="T489" id="Seg_7229" s="T488">n-n:case.poss</ta>
            <ta e="T490" id="Seg_7230" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_7231" s="T490">adj-n:num</ta>
            <ta e="T493" id="Seg_7232" s="T492">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_7233" s="T494">dempro-n:num-n:case</ta>
            <ta e="T496" id="Seg_7234" s="T495">n-n:case</ta>
            <ta e="T498" id="Seg_7235" s="T497">v-v:tense-v:pn</ta>
            <ta e="T499" id="Seg_7236" s="T498">que-n:case</ta>
            <ta e="T500" id="Seg_7237" s="T499">v-v:tense-v:pn</ta>
            <ta e="T501" id="Seg_7238" s="T500">quant</ta>
            <ta e="T502" id="Seg_7239" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_7240" s="T502">n-n:case</ta>
            <ta e="T504" id="Seg_7241" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_7242" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_7243" s="T505">adv</ta>
            <ta e="T507" id="Seg_7244" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_7245" s="T507">dempro-n:num-n:case</ta>
            <ta e="T509" id="Seg_7246" s="T508">v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_7247" s="T509">adj-n:num</ta>
            <ta e="T511" id="Seg_7248" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_7249" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_7250" s="T512">v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_7251" s="T513">conj</ta>
            <ta e="T515" id="Seg_7252" s="T514">n-n:case</ta>
            <ta e="T516" id="Seg_7253" s="T515">adv</ta>
            <ta e="T517" id="Seg_7254" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_7255" s="T517">v-v:n.fin</ta>
            <ta e="T520" id="Seg_7256" s="T519">num-n:case</ta>
            <ta e="T522" id="Seg_7257" s="T521">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_7258" s="T522">num-n:case</ta>
            <ta e="T525" id="Seg_7259" s="T524">adj-n:num</ta>
            <ta e="T526" id="Seg_7260" s="T525">n-n:case</ta>
            <ta e="T528" id="Seg_7261" s="T527">num-n:case</ta>
            <ta e="T529" id="Seg_7262" s="T528">ptcl</ta>
            <ta e="T531" id="Seg_7263" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_7264" s="T531">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_7265" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_7266" s="T533">pers</ta>
            <ta e="T535" id="Seg_7267" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_7268" s="T535">pers</ta>
            <ta e="T537" id="Seg_7269" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_7270" s="T537">adv</ta>
            <ta e="T539" id="Seg_7271" s="T538">v-v:tense-v:pn</ta>
            <ta e="T540" id="Seg_7272" s="T539">conj</ta>
            <ta e="T541" id="Seg_7273" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_7274" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_7275" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_7276" s="T543">v-v&gt;v-v:pn</ta>
            <ta e="T545" id="Seg_7277" s="T544">pers</ta>
            <ta e="T546" id="Seg_7278" s="T545">ptcl</ta>
            <ta e="T550" id="Seg_7279" s="T549">num-n:case</ta>
            <ta e="T552" id="Seg_7280" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_7281" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_7282" s="T553">adj-n:num</ta>
            <ta e="T555" id="Seg_7283" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_7284" s="T555">v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_7285" s="T556">n-n:case.poss</ta>
            <ta e="T558" id="Seg_7286" s="T557">n-n:case.poss</ta>
            <ta e="T559" id="Seg_7287" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_7288" s="T559">adv</ta>
            <ta e="T561" id="Seg_7289" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_7290" s="T561">n-n:num</ta>
            <ta e="T563" id="Seg_7291" s="T562">v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_7292" s="T563">v-v:tense-v:pn</ta>
            <ta e="T565" id="Seg_7293" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_7294" s="T565">n-n:case-n:case</ta>
            <ta e="T567" id="Seg_7295" s="T566">num-n:case</ta>
            <ta e="T568" id="Seg_7296" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_7297" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_7298" s="T569">n-n:case.poss</ta>
            <ta e="T571" id="Seg_7299" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_7300" s="T571">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_7301" s="T572">pers</ta>
            <ta e="T574" id="Seg_7302" s="T573">n-n:case.poss</ta>
            <ta e="T575" id="Seg_7303" s="T574">v-v:tense-v:pn</ta>
            <ta e="T576" id="Seg_7304" s="T575">propr-n:case</ta>
            <ta e="T577" id="Seg_7305" s="T576">conj</ta>
            <ta e="T579" id="Seg_7306" s="T578">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T580" id="Seg_7307" s="T579">propr-n:case</ta>
            <ta e="T581" id="Seg_7308" s="T580">conj</ta>
            <ta e="T582" id="Seg_7309" s="T581">n-n:cace.poss</ta>
            <ta e="T583" id="Seg_7310" s="T582">propr-n.case</ta>
            <ta e="T584" id="Seg_7311" s="T583">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_7312" s="T584">propr</ta>
            <ta e="T586" id="Seg_7313" s="T585">conj</ta>
            <ta e="T587" id="Seg_7314" s="T586">n-n:case</ta>
            <ta e="T588" id="Seg_7315" s="T587">dempro-n:case</ta>
            <ta e="T589" id="Seg_7316" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_7317" s="T589">propr-n:num</ta>
            <ta e="T591" id="Seg_7318" s="T590">n-n:num-n:case.poss</ta>
            <ta e="T592" id="Seg_7319" s="T591">v-v:tense-v:pn</ta>
            <ta e="T594" id="Seg_7320" s="T593">num-n:case</ta>
            <ta e="T595" id="Seg_7321" s="T594">num-n:case</ta>
            <ta e="T596" id="Seg_7322" s="T595">num-n:case</ta>
            <ta e="T597" id="Seg_7323" s="T596">num-n:case</ta>
            <ta e="T598" id="Seg_7324" s="T597">num-n:case</ta>
            <ta e="T599" id="Seg_7325" s="T598">num-n:case</ta>
            <ta e="T600" id="Seg_7326" s="T599">num-n:case</ta>
            <ta e="T602" id="Seg_7327" s="T600">num-n:case</ta>
            <ta e="T604" id="Seg_7328" s="T603">adj-n:case</ta>
            <ta e="T605" id="Seg_7329" s="T604">v-v:tense-v:pn</ta>
            <ta e="T606" id="Seg_7330" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_7331" s="T606">conj</ta>
            <ta e="T608" id="Seg_7332" s="T607">n-n:case.poss</ta>
            <ta e="T610" id="Seg_7333" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_7334" s="T610">n.n:case</ta>
            <ta e="T612" id="Seg_7335" s="T611">adv</ta>
            <ta e="T613" id="Seg_7336" s="T612">pers</ta>
            <ta e="T614" id="Seg_7337" s="T613">pers</ta>
            <ta e="T616" id="Seg_7338" s="T615">pers</ta>
            <ta e="T617" id="Seg_7339" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_7340" s="T617">propr-n:case</ta>
            <ta e="T619" id="Seg_7341" s="T618">pers</ta>
            <ta e="T620" id="Seg_7342" s="T619">pers</ta>
            <ta e="T621" id="Seg_7343" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_7344" s="T621">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_7345" s="T622">propr-n:case</ta>
            <ta e="T624" id="Seg_7346" s="T623">adv</ta>
            <ta e="T625" id="Seg_7347" s="T624">v-v:tense-v:pn</ta>
            <ta e="T626" id="Seg_7348" s="T625">propr-n:case</ta>
            <ta e="T627" id="Seg_7349" s="T626">adv</ta>
            <ta e="T628" id="Seg_7350" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_7351" s="T628">n-n:case</ta>
            <ta e="T630" id="Seg_7352" s="T629">adv</ta>
            <ta e="T631" id="Seg_7353" s="T630">propr-n:case</ta>
            <ta e="T632" id="Seg_7354" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_7355" s="T632">adv</ta>
            <ta e="T634" id="Seg_7356" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_7357" s="T634">v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_7358" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_7359" s="T638">propr-n:case</ta>
            <ta e="T640" id="Seg_7360" s="T639">num-n:case</ta>
            <ta e="T641" id="Seg_7361" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_7362" s="T641">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T643" id="Seg_7363" s="T642">pers</ta>
            <ta e="T644" id="Seg_7364" s="T643">adv</ta>
            <ta e="T645" id="Seg_7365" s="T644">v-v:pn</ta>
            <ta e="T646" id="Seg_7366" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_7367" s="T646">conj</ta>
            <ta e="T648" id="Seg_7368" s="T647">conj</ta>
            <ta e="T649" id="Seg_7369" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_7370" s="T649">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T651" id="Seg_7371" s="T650">num-n:case</ta>
            <ta e="T652" id="Seg_7372" s="T651">num-n:case</ta>
            <ta e="T653" id="Seg_7373" s="T652">num-n:case</ta>
            <ta e="T654" id="Seg_7374" s="T653">num-n:case</ta>
            <ta e="T655" id="Seg_7375" s="T654">num-n:case</ta>
            <ta e="T656" id="Seg_7376" s="T655">num-n:case</ta>
            <ta e="T657" id="Seg_7377" s="T656">num-n:case</ta>
            <ta e="T658" id="Seg_7378" s="T657">num-n:case</ta>
            <ta e="T659" id="Seg_7379" s="T658">num-n:case</ta>
            <ta e="T660" id="Seg_7380" s="T659">num-n:case</ta>
            <ta e="T661" id="Seg_7381" s="T660">num-n:case</ta>
            <ta e="T662" id="Seg_7382" s="T661">num-n:case</ta>
            <ta e="T663" id="Seg_7383" s="T662">num-n:case</ta>
            <ta e="T664" id="Seg_7384" s="T663">num-n:case</ta>
            <ta e="T665" id="Seg_7385" s="T664">num-n:case</ta>
            <ta e="T666" id="Seg_7386" s="T665">num-n:case</ta>
            <ta e="T667" id="Seg_7387" s="T666">adj-n:case</ta>
            <ta e="T668" id="Seg_7388" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_7389" s="T668">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_7390" s="T669">num-n:case</ta>
            <ta e="T671" id="Seg_7391" s="T670">num-n:case</ta>
            <ta e="T672" id="Seg_7392" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_7393" s="T672">v-v:tense-v:pn</ta>
            <ta e="T674" id="Seg_7394" s="T673">dempro-n:case</ta>
            <ta e="T675" id="Seg_7395" s="T674">conj</ta>
            <ta e="T676" id="Seg_7396" s="T675">propr-n:case</ta>
            <ta e="T678" id="Seg_7397" s="T676">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_7398" s="T678">dempro-n:case</ta>
            <ta e="T680" id="Seg_7399" s="T679">n-n:case</ta>
            <ta e="T681" id="Seg_7400" s="T680">v-v:tense-v:pn</ta>
            <ta e="T682" id="Seg_7401" s="T681">dempro-n:case</ta>
            <ta e="T683" id="Seg_7402" s="T682">propr-n:case</ta>
            <ta e="T684" id="Seg_7403" s="T683">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_7404" s="T684">num-n:case</ta>
            <ta e="T686" id="Seg_7405" s="T685">n-n:case</ta>
            <ta e="T699" id="Seg_7406" s="T686">v-v:n-fin</ta>
            <ta e="T687" id="Seg_7407" s="T699">v-v:tense-v:pn</ta>
            <ta e="T688" id="Seg_7408" s="T687">conj</ta>
            <ta e="T689" id="Seg_7409" s="T688">pers</ta>
            <ta e="T690" id="Seg_7410" s="T689">num-num&gt;num</ta>
            <ta e="T691" id="Seg_7411" s="T690">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_7412" s="T691">propr-n:case</ta>
            <ta e="T693" id="Seg_7413" s="T692">conj</ta>
            <ta e="T694" id="Seg_7414" s="T693">propr-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T6" id="Seg_7415" s="T5">v</ta>
            <ta e="T8" id="Seg_7416" s="T7">n</ta>
            <ta e="T9" id="Seg_7417" s="T8">v</ta>
            <ta e="T10" id="Seg_7418" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_7419" s="T10">v</ta>
            <ta e="T12" id="Seg_7420" s="T11">adv</ta>
            <ta e="T13" id="Seg_7421" s="T12">adv</ta>
            <ta e="T14" id="Seg_7422" s="T13">v</ta>
            <ta e="T15" id="Seg_7423" s="T14">conj</ta>
            <ta e="T16" id="Seg_7424" s="T15">pers</ta>
            <ta e="T17" id="Seg_7425" s="T16">adv</ta>
            <ta e="T18" id="Seg_7426" s="T17">v</ta>
            <ta e="T19" id="Seg_7427" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_7428" s="T19">quant</ta>
            <ta e="T21" id="Seg_7429" s="T20">v</ta>
            <ta e="T22" id="Seg_7430" s="T21">n</ta>
            <ta e="T28" id="Seg_7431" s="T27">n</ta>
            <ta e="T29" id="Seg_7432" s="T28">n</ta>
            <ta e="T30" id="Seg_7433" s="T29">v</ta>
            <ta e="T31" id="Seg_7434" s="T30">adv</ta>
            <ta e="T32" id="Seg_7435" s="T31">aux</ta>
            <ta e="T33" id="Seg_7436" s="T32">v</ta>
            <ta e="T34" id="Seg_7437" s="T33">adv</ta>
            <ta e="T35" id="Seg_7438" s="T34">v</ta>
            <ta e="T37" id="Seg_7439" s="T36">v</ta>
            <ta e="T38" id="Seg_7440" s="T37">dempro</ta>
            <ta e="T39" id="Seg_7441" s="T38">n</ta>
            <ta e="T40" id="Seg_7442" s="T39">v</ta>
            <ta e="T41" id="Seg_7443" s="T40">pers</ta>
            <ta e="T44" id="Seg_7444" s="T43">refl</ta>
            <ta e="T45" id="Seg_7445" s="T44">n</ta>
            <ta e="T46" id="Seg_7446" s="T45">v</ta>
            <ta e="T47" id="Seg_7447" s="T46">dempro</ta>
            <ta e="T48" id="Seg_7448" s="T47">v</ta>
            <ta e="T49" id="Seg_7449" s="T48">n</ta>
            <ta e="T51" id="Seg_7450" s="T50">v</ta>
            <ta e="T52" id="Seg_7451" s="T51">conj</ta>
            <ta e="T53" id="Seg_7452" s="T52">pers</ta>
            <ta e="T54" id="Seg_7453" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_7454" s="T54">v</ta>
            <ta e="T56" id="Seg_7455" s="T55">adv</ta>
            <ta e="T57" id="Seg_7456" s="T56">dempro</ta>
            <ta e="T58" id="Seg_7457" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_7458" s="T58">refl</ta>
            <ta e="T60" id="Seg_7459" s="T59">n</ta>
            <ta e="T61" id="Seg_7460" s="T60">v</ta>
            <ta e="T62" id="Seg_7461" s="T61">adv</ta>
            <ta e="T63" id="Seg_7462" s="T62">pers</ta>
            <ta e="T64" id="Seg_7463" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_7464" s="T64">n</ta>
            <ta e="T66" id="Seg_7465" s="T65">n</ta>
            <ta e="T67" id="Seg_7466" s="T66">v</ta>
            <ta e="T68" id="Seg_7467" s="T67">n</ta>
            <ta e="T69" id="Seg_7468" s="T68">n</ta>
            <ta e="T70" id="Seg_7469" s="T69">v</ta>
            <ta e="T71" id="Seg_7470" s="T70">que</ta>
            <ta e="T72" id="Seg_7471" s="T71">pers</ta>
            <ta e="T73" id="Seg_7472" s="T72">v</ta>
            <ta e="T74" id="Seg_7473" s="T73">conj</ta>
            <ta e="T75" id="Seg_7474" s="T74">que</ta>
            <ta e="T76" id="Seg_7475" s="T75">v</ta>
            <ta e="T77" id="Seg_7476" s="T76">pers</ta>
            <ta e="T78" id="Seg_7477" s="T77">v</ta>
            <ta e="T79" id="Seg_7478" s="T78">propr</ta>
            <ta e="T81" id="Seg_7479" s="T79">conj</ta>
            <ta e="T82" id="Seg_7480" s="T81">conj</ta>
            <ta e="T83" id="Seg_7481" s="T82">v</ta>
            <ta e="T85" id="Seg_7482" s="T84">que</ta>
            <ta e="T86" id="Seg_7483" s="T85">v</ta>
            <ta e="T87" id="Seg_7484" s="T86">adv</ta>
            <ta e="T88" id="Seg_7485" s="T87">dempro</ta>
            <ta e="T89" id="Seg_7486" s="T88">pers</ta>
            <ta e="T90" id="Seg_7487" s="T89">n</ta>
            <ta e="T91" id="Seg_7488" s="T90">conj</ta>
            <ta e="T92" id="Seg_7489" s="T91">dempro</ta>
            <ta e="T93" id="Seg_7490" s="T92">v</ta>
            <ta e="T95" id="Seg_7491" s="T94">n</ta>
            <ta e="T96" id="Seg_7492" s="T95">conj</ta>
            <ta e="T97" id="Seg_7493" s="T96">pers</ta>
            <ta e="T98" id="Seg_7494" s="T97">dempro</ta>
            <ta e="T99" id="Seg_7495" s="T98">v</ta>
            <ta e="T100" id="Seg_7496" s="T99">pers</ta>
            <ta e="T101" id="Seg_7497" s="T100">n</ta>
            <ta e="T102" id="Seg_7498" s="T101">que</ta>
            <ta e="T696" id="Seg_7499" s="T102">v</ta>
            <ta e="T103" id="Seg_7500" s="T696">v</ta>
            <ta e="T104" id="Seg_7501" s="T103">n</ta>
            <ta e="T105" id="Seg_7502" s="T104">n</ta>
            <ta e="T106" id="Seg_7503" s="T105">pers</ta>
            <ta e="T107" id="Seg_7504" s="T106">quant</ta>
            <ta e="T108" id="Seg_7505" s="T107">v</ta>
            <ta e="T109" id="Seg_7506" s="T108">v</ta>
            <ta e="T110" id="Seg_7507" s="T109">v</ta>
            <ta e="T111" id="Seg_7508" s="T110">pers</ta>
            <ta e="T112" id="Seg_7509" s="T111">n</ta>
            <ta e="T113" id="Seg_7510" s="T112">v</ta>
            <ta e="T114" id="Seg_7511" s="T113">pers</ta>
            <ta e="T115" id="Seg_7512" s="T114">n</ta>
            <ta e="T116" id="Seg_7513" s="T115">v</ta>
            <ta e="T117" id="Seg_7514" s="T116">conj</ta>
            <ta e="T118" id="Seg_7515" s="T117">adv</ta>
            <ta e="T120" id="Seg_7516" s="T119">dempro</ta>
            <ta e="T121" id="Seg_7517" s="T120">v</ta>
            <ta e="T122" id="Seg_7518" s="T121">conj</ta>
            <ta e="T123" id="Seg_7519" s="T122">n</ta>
            <ta e="T124" id="Seg_7520" s="T123">v</ta>
            <ta e="T126" id="Seg_7521" s="T125">pers</ta>
            <ta e="T127" id="Seg_7522" s="T126">ptcl</ta>
            <ta e="T130" id="Seg_7523" s="T129">v</ta>
            <ta e="T131" id="Seg_7524" s="T130">adv</ta>
            <ta e="T132" id="Seg_7525" s="T131">quant</ta>
            <ta e="T133" id="Seg_7526" s="T132">n</ta>
            <ta e="T138" id="Seg_7527" s="T137">v</ta>
            <ta e="T139" id="Seg_7528" s="T138">conj</ta>
            <ta e="T140" id="Seg_7529" s="T139">adv</ta>
            <ta e="T142" id="Seg_7530" s="T140">num=ptcl</ta>
            <ta e="T143" id="Seg_7531" s="T142">v</ta>
            <ta e="T144" id="Seg_7532" s="T143">que</ta>
            <ta e="T147" id="Seg_7533" s="T146">adv</ta>
            <ta e="T148" id="Seg_7534" s="T147">v</ta>
            <ta e="T149" id="Seg_7535" s="T148">adv</ta>
            <ta e="T150" id="Seg_7536" s="T149">adv</ta>
            <ta e="T152" id="Seg_7537" s="T151">v</ta>
            <ta e="T153" id="Seg_7538" s="T152">adv</ta>
            <ta e="T154" id="Seg_7539" s="T153">pers</ta>
            <ta e="T155" id="Seg_7540" s="T154">adj</ta>
            <ta e="T157" id="Seg_7541" s="T156">adv</ta>
            <ta e="T158" id="Seg_7542" s="T157">quant</ta>
            <ta e="T159" id="Seg_7543" s="T158">n</ta>
            <ta e="T160" id="Seg_7544" s="T159">v</ta>
            <ta e="T161" id="Seg_7545" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_7546" s="T161">pers</ta>
            <ta e="T164" id="Seg_7547" s="T162">n</ta>
            <ta e="T165" id="Seg_7548" s="T164">pers</ta>
            <ta e="T166" id="Seg_7549" s="T165">n</ta>
            <ta e="T167" id="Seg_7550" s="T166">n</ta>
            <ta e="T168" id="Seg_7551" s="T167">aux</ta>
            <ta e="T169" id="Seg_7552" s="T168">v</ta>
            <ta e="T170" id="Seg_7553" s="T169">pers</ta>
            <ta e="T171" id="Seg_7554" s="T170">aux</ta>
            <ta e="T172" id="Seg_7555" s="T171">v</ta>
            <ta e="T173" id="Seg_7556" s="T172">pers</ta>
            <ta e="T174" id="Seg_7557" s="T173">aux</ta>
            <ta e="T175" id="Seg_7558" s="T174">v</ta>
            <ta e="T176" id="Seg_7559" s="T175">pers</ta>
            <ta e="T177" id="Seg_7560" s="T176">pers</ta>
            <ta e="T179" id="Seg_7561" s="T178">pers</ta>
            <ta e="T181" id="Seg_7562" s="T180">n</ta>
            <ta e="T182" id="Seg_7563" s="T181">adj</ta>
            <ta e="T183" id="Seg_7564" s="T182">n</ta>
            <ta e="T184" id="Seg_7565" s="T183">adj</ta>
            <ta e="T185" id="Seg_7566" s="T184">pers</ta>
            <ta e="T186" id="Seg_7567" s="T185">v</ta>
            <ta e="T187" id="Seg_7568" s="T186">pers</ta>
            <ta e="T189" id="Seg_7569" s="T188">n</ta>
            <ta e="T191" id="Seg_7570" s="T190">adj</ta>
            <ta e="T192" id="Seg_7571" s="T191">v</ta>
            <ta e="T193" id="Seg_7572" s="T192">n</ta>
            <ta e="T194" id="Seg_7573" s="T193">adj</ta>
            <ta e="T195" id="Seg_7574" s="T194">v</ta>
            <ta e="T196" id="Seg_7575" s="T195">v</ta>
            <ta e="T197" id="Seg_7576" s="T196">pers</ta>
            <ta e="T198" id="Seg_7577" s="T197">v</ta>
            <ta e="T199" id="Seg_7578" s="T198">pers</ta>
            <ta e="T200" id="Seg_7579" s="T199">v</ta>
            <ta e="T201" id="Seg_7580" s="T200">pers</ta>
            <ta e="T202" id="Seg_7581" s="T201">v</ta>
            <ta e="T204" id="Seg_7582" s="T203">pers</ta>
            <ta e="T205" id="Seg_7583" s="T204">v</ta>
            <ta e="T206" id="Seg_7584" s="T205">pers</ta>
            <ta e="T207" id="Seg_7585" s="T206">n</ta>
            <ta e="T208" id="Seg_7586" s="T207">pers</ta>
            <ta e="T209" id="Seg_7587" s="T208">v</ta>
            <ta e="T210" id="Seg_7588" s="T209">pers</ta>
            <ta e="T211" id="Seg_7589" s="T210">n</ta>
            <ta e="T213" id="Seg_7590" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_7591" s="T213">refl</ta>
            <ta e="T215" id="Seg_7592" s="T214">v</ta>
            <ta e="T216" id="Seg_7593" s="T215">v</ta>
            <ta e="T217" id="Seg_7594" s="T216">adv</ta>
            <ta e="T218" id="Seg_7595" s="T217">n</ta>
            <ta e="T221" id="Seg_7596" s="T220">v</ta>
            <ta e="T222" id="Seg_7597" s="T221">conj</ta>
            <ta e="T223" id="Seg_7598" s="T222">adv</ta>
            <ta e="T227" id="Seg_7599" s="T226">v</ta>
            <ta e="T228" id="Seg_7600" s="T227">dempro</ta>
            <ta e="T229" id="Seg_7601" s="T228">n</ta>
            <ta e="T230" id="Seg_7602" s="T229">v</ta>
            <ta e="T231" id="Seg_7603" s="T230">conj</ta>
            <ta e="T232" id="Seg_7604" s="T231">dempro</ta>
            <ta e="T233" id="Seg_7605" s="T232">n</ta>
            <ta e="T234" id="Seg_7606" s="T233">v</ta>
            <ta e="T235" id="Seg_7607" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7608" s="T235">pers</ta>
            <ta e="T237" id="Seg_7609" s="T236">n</ta>
            <ta e="T238" id="Seg_7610" s="T237">v</ta>
            <ta e="T239" id="Seg_7611" s="T238">n</ta>
            <ta e="T245" id="Seg_7612" s="T244">n</ta>
            <ta e="T246" id="Seg_7613" s="T245">v</ta>
            <ta e="T248" id="Seg_7614" s="T247">dempro</ta>
            <ta e="T249" id="Seg_7615" s="T248">n</ta>
            <ta e="T250" id="Seg_7616" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_7617" s="T250">n</ta>
            <ta e="T252" id="Seg_7618" s="T251">v</ta>
            <ta e="T253" id="Seg_7619" s="T252">conj</ta>
            <ta e="T254" id="Seg_7620" s="T253">n</ta>
            <ta e="T255" id="Seg_7621" s="T254">v</ta>
            <ta e="T256" id="Seg_7622" s="T255">conj</ta>
            <ta e="T257" id="Seg_7623" s="T256">n</ta>
            <ta e="T259" id="Seg_7624" s="T258">v</ta>
            <ta e="T260" id="Seg_7625" s="T259">n</ta>
            <ta e="T261" id="Seg_7626" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_7627" s="T261">n</ta>
            <ta e="T263" id="Seg_7628" s="T262">v</ta>
            <ta e="T264" id="Seg_7629" s="T263">conj</ta>
            <ta e="T265" id="Seg_7630" s="T264">n</ta>
            <ta e="T266" id="Seg_7631" s="T265">n</ta>
            <ta e="T267" id="Seg_7632" s="T266">dempro</ta>
            <ta e="T268" id="Seg_7633" s="T267">n</ta>
            <ta e="T269" id="Seg_7634" s="T268">quant</ta>
            <ta e="T270" id="Seg_7635" s="T269">que</ta>
            <ta e="T271" id="Seg_7636" s="T270">v</ta>
            <ta e="T272" id="Seg_7637" s="T271">n</ta>
            <ta e="T273" id="Seg_7638" s="T272">conj</ta>
            <ta e="T274" id="Seg_7639" s="T273">que</ta>
            <ta e="T275" id="Seg_7640" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_7641" s="T275">v</ta>
            <ta e="T277" id="Seg_7642" s="T276">n</ta>
            <ta e="T278" id="Seg_7643" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_7644" s="T278">v</ta>
            <ta e="T280" id="Seg_7645" s="T279">n</ta>
            <ta e="T281" id="Seg_7646" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_7647" s="T281">ptcl</ta>
            <ta e="T697" id="Seg_7648" s="T282">v</ta>
            <ta e="T283" id="Seg_7649" s="T697">v</ta>
            <ta e="T284" id="Seg_7650" s="T283">pers</ta>
            <ta e="T285" id="Seg_7651" s="T284">v</ta>
            <ta e="T286" id="Seg_7652" s="T285">n</ta>
            <ta e="T287" id="Seg_7653" s="T286">adv</ta>
            <ta e="T289" id="Seg_7654" s="T288">quant</ta>
            <ta e="T290" id="Seg_7655" s="T289">v</ta>
            <ta e="T291" id="Seg_7656" s="T290">pers</ta>
            <ta e="T292" id="Seg_7657" s="T291">v</ta>
            <ta e="T293" id="Seg_7658" s="T292">conj</ta>
            <ta e="T294" id="Seg_7659" s="T293">adv</ta>
            <ta e="T295" id="Seg_7660" s="T294">v</ta>
            <ta e="T296" id="Seg_7661" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_7662" s="T296">n</ta>
            <ta e="T298" id="Seg_7663" s="T297">pers</ta>
            <ta e="T301" id="Seg_7664" s="T300">v</ta>
            <ta e="T302" id="Seg_7665" s="T301">adv</ta>
            <ta e="T303" id="Seg_7666" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_7667" s="T303">adv</ta>
            <ta e="T305" id="Seg_7668" s="T304">adj</ta>
            <ta e="T307" id="Seg_7669" s="T306">quant</ta>
            <ta e="T308" id="Seg_7670" s="T307">pers</ta>
            <ta e="T309" id="Seg_7671" s="T308">v</ta>
            <ta e="T310" id="Seg_7672" s="T309">conj</ta>
            <ta e="T311" id="Seg_7673" s="T310">adv</ta>
            <ta e="T312" id="Seg_7674" s="T311">v</ta>
            <ta e="T313" id="Seg_7675" s="T312">ptcl</ta>
            <ta e="T315" id="Seg_7676" s="T314">n</ta>
            <ta e="T318" id="Seg_7677" s="T317">adv</ta>
            <ta e="T319" id="Seg_7678" s="T318">n</ta>
            <ta e="T320" id="Seg_7679" s="T319">n</ta>
            <ta e="T321" id="Seg_7680" s="T320">v</ta>
            <ta e="T322" id="Seg_7681" s="T321">conj</ta>
            <ta e="T323" id="Seg_7682" s="T322">v</ta>
            <ta e="T328" id="Seg_7683" s="T327">dempro</ta>
            <ta e="T329" id="Seg_7684" s="T328">n</ta>
            <ta e="T330" id="Seg_7685" s="T329">v</ta>
            <ta e="T331" id="Seg_7686" s="T330">conj</ta>
            <ta e="T332" id="Seg_7687" s="T331">dempro</ta>
            <ta e="T333" id="Seg_7688" s="T332">n</ta>
            <ta e="T334" id="Seg_7689" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_7690" s="T334">v</ta>
            <ta e="T336" id="Seg_7691" s="T335">pers</ta>
            <ta e="T337" id="Seg_7692" s="T336">n</ta>
            <ta e="T338" id="Seg_7693" s="T337">v</ta>
            <ta e="T339" id="Seg_7694" s="T338">n</ta>
            <ta e="T340" id="Seg_7695" s="T339">dempro</ta>
            <ta e="T341" id="Seg_7696" s="T340">v</ta>
            <ta e="T342" id="Seg_7697" s="T341">n</ta>
            <ta e="T343" id="Seg_7698" s="T342">v</ta>
            <ta e="T344" id="Seg_7699" s="T343">n</ta>
            <ta e="T345" id="Seg_7700" s="T344">dempro</ta>
            <ta e="T346" id="Seg_7701" s="T345">v</ta>
            <ta e="T347" id="Seg_7702" s="T346">n</ta>
            <ta e="T348" id="Seg_7703" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_7704" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_7705" s="T349">v</ta>
            <ta e="T351" id="Seg_7706" s="T350">v</ta>
            <ta e="T352" id="Seg_7707" s="T351">quant</ta>
            <ta e="T353" id="Seg_7708" s="T352">v</ta>
            <ta e="T357" id="Seg_7709" s="T356">n</ta>
            <ta e="T358" id="Seg_7710" s="T357">v</ta>
            <ta e="T359" id="Seg_7711" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_7712" s="T359">dempro</ta>
            <ta e="T361" id="Seg_7713" s="T360">v</ta>
            <ta e="T362" id="Seg_7714" s="T361">conj</ta>
            <ta e="T363" id="Seg_7715" s="T362">n</ta>
            <ta e="T364" id="Seg_7716" s="T363">v</ta>
            <ta e="T365" id="Seg_7717" s="T364">conj</ta>
            <ta e="T366" id="Seg_7718" s="T365">n</ta>
            <ta e="T367" id="Seg_7719" s="T366">v</ta>
            <ta e="T368" id="Seg_7720" s="T367">adj</ta>
            <ta e="T369" id="Seg_7721" s="T368">v</ta>
            <ta e="T370" id="Seg_7722" s="T369">v</ta>
            <ta e="T371" id="Seg_7723" s="T370">n</ta>
            <ta e="T372" id="Seg_7724" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_7725" s="T372">v</ta>
            <ta e="T374" id="Seg_7726" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_7727" s="T374">v</ta>
            <ta e="T376" id="Seg_7728" s="T375">adj</ta>
            <ta e="T377" id="Seg_7729" s="T376">v</ta>
            <ta e="T379" id="Seg_7730" s="T377">dempro</ta>
            <ta e="T380" id="Seg_7731" s="T379">n</ta>
            <ta e="T381" id="Seg_7732" s="T380">n</ta>
            <ta e="T382" id="Seg_7733" s="T381">n</ta>
            <ta e="T383" id="Seg_7734" s="T382">v</ta>
            <ta e="T384" id="Seg_7735" s="T383">conj</ta>
            <ta e="T385" id="Seg_7736" s="T384">adv</ta>
            <ta e="T386" id="Seg_7737" s="T385">n</ta>
            <ta e="T387" id="Seg_7738" s="T386">v</ta>
            <ta e="T388" id="Seg_7739" s="T387">conj</ta>
            <ta e="T389" id="Seg_7740" s="T388">n</ta>
            <ta e="T390" id="Seg_7741" s="T389">v</ta>
            <ta e="T391" id="Seg_7742" s="T390">adv</ta>
            <ta e="T394" id="Seg_7743" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_7744" s="T394">num</ta>
            <ta e="T396" id="Seg_7745" s="T395">num</ta>
            <ta e="T397" id="Seg_7746" s="T396">v</ta>
            <ta e="T398" id="Seg_7747" s="T397">conj</ta>
            <ta e="T399" id="Seg_7748" s="T398">n</ta>
            <ta e="T400" id="Seg_7749" s="T399">v</ta>
            <ta e="T402" id="Seg_7750" s="T401">v</ta>
            <ta e="T403" id="Seg_7751" s="T402">n</ta>
            <ta e="T404" id="Seg_7752" s="T403">n</ta>
            <ta e="T405" id="Seg_7753" s="T404">conj</ta>
            <ta e="T406" id="Seg_7754" s="T405">v</ta>
            <ta e="T407" id="Seg_7755" s="T406">n</ta>
            <ta e="T408" id="Seg_7756" s="T407">v</ta>
            <ta e="T409" id="Seg_7757" s="T408">adj</ta>
            <ta e="T410" id="Seg_7758" s="T409">v</ta>
            <ta e="T411" id="Seg_7759" s="T410">pers</ta>
            <ta e="T412" id="Seg_7760" s="T411">n</ta>
            <ta e="T414" id="Seg_7761" s="T413">que</ta>
            <ta e="T415" id="Seg_7762" s="T414">v</ta>
            <ta e="T416" id="Seg_7763" s="T415">propr</ta>
            <ta e="T417" id="Seg_7764" s="T416">v</ta>
            <ta e="T420" id="Seg_7765" s="T419">quant</ta>
            <ta e="T421" id="Seg_7766" s="T420">n</ta>
            <ta e="T422" id="Seg_7767" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7768" s="T422">n</ta>
            <ta e="T424" id="Seg_7769" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_7770" s="T424">v</ta>
            <ta e="T426" id="Seg_7771" s="T425">n</ta>
            <ta e="T427" id="Seg_7772" s="T426">conj</ta>
            <ta e="T428" id="Seg_7773" s="T427">v</ta>
            <ta e="T429" id="Seg_7774" s="T428">n</ta>
            <ta e="T430" id="Seg_7775" s="T429">dempro</ta>
            <ta e="T431" id="Seg_7776" s="T430">n</ta>
            <ta e="T432" id="Seg_7777" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_7778" s="T432">v</ta>
            <ta e="T434" id="Seg_7779" s="T433">v</ta>
            <ta e="T435" id="Seg_7780" s="T434">conj</ta>
            <ta e="T436" id="Seg_7781" s="T435">que</ta>
            <ta e="T437" id="Seg_7782" s="T436">v</ta>
            <ta e="T438" id="Seg_7783" s="T437">conj</ta>
            <ta e="T439" id="Seg_7784" s="T438">dempro</ta>
            <ta e="T440" id="Seg_7785" s="T439">n</ta>
            <ta e="T441" id="Seg_7786" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_7787" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_7788" s="T442">adv</ta>
            <ta e="T444" id="Seg_7789" s="T443">v</ta>
            <ta e="T445" id="Seg_7790" s="T444">quant</ta>
            <ta e="T446" id="Seg_7791" s="T445">que</ta>
            <ta e="T447" id="Seg_7792" s="T446">v</ta>
            <ta e="T450" id="Seg_7793" s="T449">aux</ta>
            <ta e="T451" id="Seg_7794" s="T450">v</ta>
            <ta e="T452" id="Seg_7795" s="T451">v</ta>
            <ta e="T453" id="Seg_7796" s="T452">conj</ta>
            <ta e="T454" id="Seg_7797" s="T453">v</ta>
            <ta e="T455" id="Seg_7798" s="T454">que</ta>
            <ta e="T456" id="Seg_7799" s="T455">v</ta>
            <ta e="T457" id="Seg_7800" s="T456">quant</ta>
            <ta e="T458" id="Seg_7801" s="T457">n</ta>
            <ta e="T698" id="Seg_7802" s="T458">v</ta>
            <ta e="T459" id="Seg_7803" s="T698">v</ta>
            <ta e="T460" id="Seg_7804" s="T459">v</ta>
            <ta e="T461" id="Seg_7805" s="T460">adj</ta>
            <ta e="T462" id="Seg_7806" s="T461">adj</ta>
            <ta e="T463" id="Seg_7807" s="T462">v</ta>
            <ta e="T464" id="Seg_7808" s="T463">conj</ta>
            <ta e="T465" id="Seg_7809" s="T464">n</ta>
            <ta e="T466" id="Seg_7810" s="T465">adv</ta>
            <ta e="T467" id="Seg_7811" s="T466">v</ta>
            <ta e="T468" id="Seg_7812" s="T467">n</ta>
            <ta e="T470" id="Seg_7813" s="T469">quant</ta>
            <ta e="T471" id="Seg_7814" s="T470">v</ta>
            <ta e="T472" id="Seg_7815" s="T471">n</ta>
            <ta e="T473" id="Seg_7816" s="T472">adv</ta>
            <ta e="T474" id="Seg_7817" s="T473">v</ta>
            <ta e="T475" id="Seg_7818" s="T474">conj</ta>
            <ta e="T476" id="Seg_7819" s="T475">conj</ta>
            <ta e="T477" id="Seg_7820" s="T476">v</ta>
            <ta e="T478" id="Seg_7821" s="T477">n</ta>
            <ta e="T479" id="Seg_7822" s="T478">dempro</ta>
            <ta e="T480" id="Seg_7823" s="T479">ptcl</ta>
            <ta e="T482" id="Seg_7824" s="T481">v</ta>
            <ta e="T483" id="Seg_7825" s="T482">dempro</ta>
            <ta e="T484" id="Seg_7826" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_7827" s="T484">v</ta>
            <ta e="T486" id="Seg_7828" s="T485">adj</ta>
            <ta e="T487" id="Seg_7829" s="T486">n</ta>
            <ta e="T488" id="Seg_7830" s="T487">pers</ta>
            <ta e="T489" id="Seg_7831" s="T488">n</ta>
            <ta e="T490" id="Seg_7832" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_7833" s="T490">adj</ta>
            <ta e="T493" id="Seg_7834" s="T492">v</ta>
            <ta e="T495" id="Seg_7835" s="T494">dempro</ta>
            <ta e="T496" id="Seg_7836" s="T495">n</ta>
            <ta e="T498" id="Seg_7837" s="T497">v</ta>
            <ta e="T499" id="Seg_7838" s="T498">que</ta>
            <ta e="T500" id="Seg_7839" s="T499">v</ta>
            <ta e="T501" id="Seg_7840" s="T500">quant</ta>
            <ta e="T502" id="Seg_7841" s="T501">v</ta>
            <ta e="T503" id="Seg_7842" s="T502">n</ta>
            <ta e="T504" id="Seg_7843" s="T503">n</ta>
            <ta e="T505" id="Seg_7844" s="T504">v</ta>
            <ta e="T506" id="Seg_7845" s="T505">adv</ta>
            <ta e="T507" id="Seg_7846" s="T506">n</ta>
            <ta e="T508" id="Seg_7847" s="T507">dempro</ta>
            <ta e="T509" id="Seg_7848" s="T508">v</ta>
            <ta e="T510" id="Seg_7849" s="T509">adj</ta>
            <ta e="T511" id="Seg_7850" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_7851" s="T511">n</ta>
            <ta e="T513" id="Seg_7852" s="T512">v</ta>
            <ta e="T514" id="Seg_7853" s="T513">conj</ta>
            <ta e="T515" id="Seg_7854" s="T514">n</ta>
            <ta e="T516" id="Seg_7855" s="T515">adv</ta>
            <ta e="T517" id="Seg_7856" s="T516">v</ta>
            <ta e="T518" id="Seg_7857" s="T517">v</ta>
            <ta e="T520" id="Seg_7858" s="T519">num</ta>
            <ta e="T522" id="Seg_7859" s="T521">v</ta>
            <ta e="T523" id="Seg_7860" s="T522">num</ta>
            <ta e="T525" id="Seg_7861" s="T524">adj</ta>
            <ta e="T526" id="Seg_7862" s="T525">n</ta>
            <ta e="T528" id="Seg_7863" s="T527">num</ta>
            <ta e="T529" id="Seg_7864" s="T528">ptcl</ta>
            <ta e="T531" id="Seg_7865" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_7866" s="T531">v</ta>
            <ta e="T533" id="Seg_7867" s="T532">n</ta>
            <ta e="T534" id="Seg_7868" s="T533">pers</ta>
            <ta e="T535" id="Seg_7869" s="T534">v</ta>
            <ta e="T536" id="Seg_7870" s="T535">pers</ta>
            <ta e="T537" id="Seg_7871" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_7872" s="T537">adv</ta>
            <ta e="T539" id="Seg_7873" s="T538">v</ta>
            <ta e="T540" id="Seg_7874" s="T539">conj</ta>
            <ta e="T541" id="Seg_7875" s="T540">n</ta>
            <ta e="T542" id="Seg_7876" s="T541">n</ta>
            <ta e="T543" id="Seg_7877" s="T542">v</ta>
            <ta e="T544" id="Seg_7878" s="T543">v</ta>
            <ta e="T545" id="Seg_7879" s="T544">pers</ta>
            <ta e="T546" id="Seg_7880" s="T545">ptcl</ta>
            <ta e="T550" id="Seg_7881" s="T549">num</ta>
            <ta e="T552" id="Seg_7882" s="T551">n</ta>
            <ta e="T553" id="Seg_7883" s="T552">v</ta>
            <ta e="T554" id="Seg_7884" s="T553">adj</ta>
            <ta e="T555" id="Seg_7885" s="T554">n</ta>
            <ta e="T556" id="Seg_7886" s="T555">v</ta>
            <ta e="T557" id="Seg_7887" s="T556">n</ta>
            <ta e="T558" id="Seg_7888" s="T557">n</ta>
            <ta e="T559" id="Seg_7889" s="T558">v</ta>
            <ta e="T560" id="Seg_7890" s="T559">adv</ta>
            <ta e="T561" id="Seg_7891" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_7892" s="T561">n</ta>
            <ta e="T563" id="Seg_7893" s="T562">v</ta>
            <ta e="T564" id="Seg_7894" s="T563">v</ta>
            <ta e="T565" id="Seg_7895" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_7896" s="T565">n</ta>
            <ta e="T567" id="Seg_7897" s="T566">num</ta>
            <ta e="T568" id="Seg_7898" s="T567">n</ta>
            <ta e="T569" id="Seg_7899" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_7900" s="T569">n</ta>
            <ta e="T571" id="Seg_7901" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_7902" s="T571">v</ta>
            <ta e="T573" id="Seg_7903" s="T572">pers</ta>
            <ta e="T574" id="Seg_7904" s="T573">n</ta>
            <ta e="T575" id="Seg_7905" s="T574">v</ta>
            <ta e="T576" id="Seg_7906" s="T575">propr</ta>
            <ta e="T577" id="Seg_7907" s="T576">conj</ta>
            <ta e="T579" id="Seg_7908" s="T578">v</ta>
            <ta e="T580" id="Seg_7909" s="T579">propr</ta>
            <ta e="T581" id="Seg_7910" s="T580">conj</ta>
            <ta e="T582" id="Seg_7911" s="T581">n</ta>
            <ta e="T583" id="Seg_7912" s="T582">propr</ta>
            <ta e="T584" id="Seg_7913" s="T583">v</ta>
            <ta e="T585" id="Seg_7914" s="T584">propr</ta>
            <ta e="T586" id="Seg_7915" s="T585">conj</ta>
            <ta e="T587" id="Seg_7916" s="T586">n</ta>
            <ta e="T588" id="Seg_7917" s="T587">dempro</ta>
            <ta e="T589" id="Seg_7918" s="T588">v</ta>
            <ta e="T590" id="Seg_7919" s="T589">propr</ta>
            <ta e="T591" id="Seg_7920" s="T590">n</ta>
            <ta e="T592" id="Seg_7921" s="T591">v</ta>
            <ta e="T594" id="Seg_7922" s="T593">num</ta>
            <ta e="T595" id="Seg_7923" s="T594">num</ta>
            <ta e="T596" id="Seg_7924" s="T595">num</ta>
            <ta e="T597" id="Seg_7925" s="T596">num</ta>
            <ta e="T598" id="Seg_7926" s="T597">num</ta>
            <ta e="T599" id="Seg_7927" s="T598">num</ta>
            <ta e="T600" id="Seg_7928" s="T599">num</ta>
            <ta e="T602" id="Seg_7929" s="T600">num</ta>
            <ta e="T604" id="Seg_7930" s="T603">adj</ta>
            <ta e="T605" id="Seg_7931" s="T604">v</ta>
            <ta e="T606" id="Seg_7932" s="T605">n</ta>
            <ta e="T607" id="Seg_7933" s="T606">conj</ta>
            <ta e="T608" id="Seg_7934" s="T607">n</ta>
            <ta e="T610" id="Seg_7935" s="T609">v</ta>
            <ta e="T611" id="Seg_7936" s="T610">propr</ta>
            <ta e="T612" id="Seg_7937" s="T611">adv</ta>
            <ta e="T613" id="Seg_7938" s="T612">pers</ta>
            <ta e="T614" id="Seg_7939" s="T613">pers</ta>
            <ta e="T616" id="Seg_7940" s="T615">pers</ta>
            <ta e="T617" id="Seg_7941" s="T616">v</ta>
            <ta e="T618" id="Seg_7942" s="T617">propr</ta>
            <ta e="T619" id="Seg_7943" s="T618">pers</ta>
            <ta e="T620" id="Seg_7944" s="T619">pers</ta>
            <ta e="T621" id="Seg_7945" s="T620">n</ta>
            <ta e="T622" id="Seg_7946" s="T621">v</ta>
            <ta e="T623" id="Seg_7947" s="T622">propr</ta>
            <ta e="T624" id="Seg_7948" s="T623">adv</ta>
            <ta e="T625" id="Seg_7949" s="T624">v</ta>
            <ta e="T626" id="Seg_7950" s="T625">propr</ta>
            <ta e="T627" id="Seg_7951" s="T626">adv</ta>
            <ta e="T628" id="Seg_7952" s="T627">v</ta>
            <ta e="T629" id="Seg_7953" s="T628">propr</ta>
            <ta e="T630" id="Seg_7954" s="T629">adv</ta>
            <ta e="T631" id="Seg_7955" s="T630">propr</ta>
            <ta e="T632" id="Seg_7956" s="T631">v</ta>
            <ta e="T633" id="Seg_7957" s="T632">adv</ta>
            <ta e="T634" id="Seg_7958" s="T633">n</ta>
            <ta e="T635" id="Seg_7959" s="T634">v</ta>
            <ta e="T638" id="Seg_7960" s="T637">v</ta>
            <ta e="T639" id="Seg_7961" s="T638">propr</ta>
            <ta e="T640" id="Seg_7962" s="T639">num</ta>
            <ta e="T641" id="Seg_7963" s="T640">n</ta>
            <ta e="T642" id="Seg_7964" s="T641">v</ta>
            <ta e="T643" id="Seg_7965" s="T642">pers</ta>
            <ta e="T644" id="Seg_7966" s="T643">adv</ta>
            <ta e="T645" id="Seg_7967" s="T644">v</ta>
            <ta e="T646" id="Seg_7968" s="T645">v</ta>
            <ta e="T647" id="Seg_7969" s="T646">conj</ta>
            <ta e="T648" id="Seg_7970" s="T647">conj</ta>
            <ta e="T649" id="Seg_7971" s="T648">n</ta>
            <ta e="T650" id="Seg_7972" s="T649">v</ta>
            <ta e="T651" id="Seg_7973" s="T650">num</ta>
            <ta e="T652" id="Seg_7974" s="T651">num</ta>
            <ta e="T653" id="Seg_7975" s="T652">num</ta>
            <ta e="T654" id="Seg_7976" s="T653">num</ta>
            <ta e="T655" id="Seg_7977" s="T654">num</ta>
            <ta e="T656" id="Seg_7978" s="T655">num</ta>
            <ta e="T657" id="Seg_7979" s="T656">num</ta>
            <ta e="T658" id="Seg_7980" s="T657">num</ta>
            <ta e="T659" id="Seg_7981" s="T658">num</ta>
            <ta e="T660" id="Seg_7982" s="T659">num</ta>
            <ta e="T661" id="Seg_7983" s="T660">num</ta>
            <ta e="T662" id="Seg_7984" s="T661">num</ta>
            <ta e="T663" id="Seg_7985" s="T662">num</ta>
            <ta e="T664" id="Seg_7986" s="T663">num</ta>
            <ta e="T665" id="Seg_7987" s="T664">num</ta>
            <ta e="T666" id="Seg_7988" s="T665">num</ta>
            <ta e="T667" id="Seg_7989" s="T666">adj</ta>
            <ta e="T668" id="Seg_7990" s="T667">n</ta>
            <ta e="T669" id="Seg_7991" s="T668">v</ta>
            <ta e="T670" id="Seg_7992" s="T669">num</ta>
            <ta e="T671" id="Seg_7993" s="T670">num</ta>
            <ta e="T672" id="Seg_7994" s="T671">n</ta>
            <ta e="T673" id="Seg_7995" s="T672">v</ta>
            <ta e="T674" id="Seg_7996" s="T673">dempro</ta>
            <ta e="T675" id="Seg_7997" s="T674">conj</ta>
            <ta e="T676" id="Seg_7998" s="T675">propr</ta>
            <ta e="T678" id="Seg_7999" s="T676">v</ta>
            <ta e="T679" id="Seg_8000" s="T678">dempro</ta>
            <ta e="T680" id="Seg_8001" s="T679">n</ta>
            <ta e="T681" id="Seg_8002" s="T680">v</ta>
            <ta e="T682" id="Seg_8003" s="T681">dempro</ta>
            <ta e="T683" id="Seg_8004" s="T682">propr</ta>
            <ta e="T684" id="Seg_8005" s="T683">v</ta>
            <ta e="T685" id="Seg_8006" s="T684">num</ta>
            <ta e="T686" id="Seg_8007" s="T685">n</ta>
            <ta e="T699" id="Seg_8008" s="T686">v</ta>
            <ta e="T687" id="Seg_8009" s="T699">v</ta>
            <ta e="T688" id="Seg_8010" s="T687">conj</ta>
            <ta e="T689" id="Seg_8011" s="T688">pers</ta>
            <ta e="T690" id="Seg_8012" s="T689">num</ta>
            <ta e="T691" id="Seg_8013" s="T690">v</ta>
            <ta e="T692" id="Seg_8014" s="T691">propr</ta>
            <ta e="T693" id="Seg_8015" s="T692">conj</ta>
            <ta e="T694" id="Seg_8016" s="T693">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T6" id="Seg_8017" s="T5">0.2.h:A</ta>
            <ta e="T8" id="Seg_8018" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_8019" s="T8">0.2.h:E</ta>
            <ta e="T11" id="Seg_8020" s="T10">0.3:Th</ta>
            <ta e="T12" id="Seg_8021" s="T11">adv:L</ta>
            <ta e="T13" id="Seg_8022" s="T12">adv:L</ta>
            <ta e="T14" id="Seg_8023" s="T13">0.2.h:A</ta>
            <ta e="T16" id="Seg_8024" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_8025" s="T16">adv:L</ta>
            <ta e="T22" id="Seg_8026" s="T21">np:Th</ta>
            <ta e="T28" id="Seg_8027" s="T27">np:Poss</ta>
            <ta e="T29" id="Seg_8028" s="T28">np:L</ta>
            <ta e="T30" id="Seg_8029" s="T29">0.2.h:A</ta>
            <ta e="T32" id="Seg_8030" s="T31">0.2.h:A</ta>
            <ta e="T35" id="Seg_8031" s="T34">0.2.h:A</ta>
            <ta e="T37" id="Seg_8032" s="T36">0.2.h:A</ta>
            <ta e="T39" id="Seg_8033" s="T38">np.h:A</ta>
            <ta e="T41" id="Seg_8034" s="T40">pro:G</ta>
            <ta e="T44" id="Seg_8035" s="T43">pro.h:Poss</ta>
            <ta e="T45" id="Seg_8036" s="T44">np.h:Th</ta>
            <ta e="T46" id="Seg_8037" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_8038" s="T46">pro.h:E</ta>
            <ta e="T49" id="Seg_8039" s="T48">np:L</ta>
            <ta e="T53" id="Seg_8040" s="T52">pro.h:E</ta>
            <ta e="T56" id="Seg_8041" s="T55">adv:Time</ta>
            <ta e="T57" id="Seg_8042" s="T56">pro.h:A</ta>
            <ta e="T60" id="Seg_8043" s="T59">np.h:Th</ta>
            <ta e="T62" id="Seg_8044" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_8045" s="T62">pro.h:A</ta>
            <ta e="T66" id="Seg_8046" s="T65">np:Th</ta>
            <ta e="T68" id="Seg_8047" s="T67">np:Poss</ta>
            <ta e="T69" id="Seg_8048" s="T68">np:Th</ta>
            <ta e="T70" id="Seg_8049" s="T69">0.1.h:A</ta>
            <ta e="T72" id="Seg_8050" s="T71">pro.h:Th</ta>
            <ta e="T73" id="Seg_8051" s="T72">0.3.h:A</ta>
            <ta e="T76" id="Seg_8052" s="T75">0.3.h:A</ta>
            <ta e="T77" id="Seg_8053" s="T76">pro.h:Th</ta>
            <ta e="T78" id="Seg_8054" s="T77">0.3.h:A</ta>
            <ta e="T83" id="Seg_8055" s="T82">0.3.h:A</ta>
            <ta e="T86" id="Seg_8056" s="T85">0.2.h:A</ta>
            <ta e="T87" id="Seg_8057" s="T86">adv:L</ta>
            <ta e="T88" id="Seg_8058" s="T87">pro.h:Th</ta>
            <ta e="T89" id="Seg_8059" s="T88">pro.h:Poss</ta>
            <ta e="T90" id="Seg_8060" s="T89">np.h:Th</ta>
            <ta e="T92" id="Seg_8061" s="T91">pro.h:Th</ta>
            <ta e="T95" id="Seg_8062" s="T94">np.h:Th</ta>
            <ta e="T97" id="Seg_8063" s="T96">pro.h:A</ta>
            <ta e="T98" id="Seg_8064" s="T97">pro.h:R</ta>
            <ta e="T100" id="Seg_8065" s="T99">pro.h:Poss</ta>
            <ta e="T101" id="Seg_8066" s="T100">np.h:A</ta>
            <ta e="T104" id="Seg_8067" s="T103">np:Poss</ta>
            <ta e="T105" id="Seg_8068" s="T104">np:G</ta>
            <ta e="T109" id="Seg_8069" s="T108">0.1.h:E</ta>
            <ta e="T111" id="Seg_8070" s="T110">pro.h:Poss</ta>
            <ta e="T112" id="Seg_8071" s="T111">np:P</ta>
            <ta e="T114" id="Seg_8072" s="T113">pro.h:A</ta>
            <ta e="T115" id="Seg_8073" s="T114">np:P</ta>
            <ta e="T118" id="Seg_8074" s="T117">adv:L</ta>
            <ta e="T120" id="Seg_8075" s="T119">pro:Th</ta>
            <ta e="T121" id="Seg_8076" s="T120">0.1.h:A</ta>
            <ta e="T123" id="Seg_8077" s="T122">np:Ins</ta>
            <ta e="T124" id="Seg_8078" s="T123">0.1.h:A</ta>
            <ta e="T126" id="Seg_8079" s="T125">pro.h:Poss</ta>
            <ta e="T131" id="Seg_8080" s="T130">adv:L</ta>
            <ta e="T133" id="Seg_8081" s="T132">np:Th</ta>
            <ta e="T140" id="Seg_8082" s="T139">adv:Time</ta>
            <ta e="T142" id="Seg_8083" s="T140">np:Th</ta>
            <ta e="T144" id="Seg_8084" s="T143">pro:Th</ta>
            <ta e="T147" id="Seg_8085" s="T146">adv:L</ta>
            <ta e="T149" id="Seg_8086" s="T148">adv:Time</ta>
            <ta e="T150" id="Seg_8087" s="T149">adv:L</ta>
            <ta e="T153" id="Seg_8088" s="T152">adv:Time</ta>
            <ta e="T154" id="Seg_8089" s="T153">pro.h:Poss</ta>
            <ta e="T157" id="Seg_8090" s="T156">adv:L</ta>
            <ta e="T159" id="Seg_8091" s="T158">np:Th</ta>
            <ta e="T168" id="Seg_8092" s="T167">0.2:A</ta>
            <ta e="T170" id="Seg_8093" s="T169">pro.h:Th</ta>
            <ta e="T171" id="Seg_8094" s="T170">0.2:A</ta>
            <ta e="T173" id="Seg_8095" s="T172">pro.h:Th</ta>
            <ta e="T174" id="Seg_8096" s="T173">0.2:A</ta>
            <ta e="T176" id="Seg_8097" s="T175">pro.h:Th</ta>
            <ta e="T177" id="Seg_8098" s="T176">pro:A</ta>
            <ta e="T179" id="Seg_8099" s="T178">pro.h:R</ta>
            <ta e="T183" id="Seg_8100" s="T182">np:Th</ta>
            <ta e="T185" id="Seg_8101" s="T184">pro:A</ta>
            <ta e="T187" id="Seg_8102" s="T186">pro.h:R</ta>
            <ta e="T189" id="Seg_8103" s="T188">np:Th</ta>
            <ta e="T193" id="Seg_8104" s="T192">np:Th</ta>
            <ta e="T196" id="Seg_8105" s="T195">0.2:A</ta>
            <ta e="T197" id="Seg_8106" s="T196">pro.h:R</ta>
            <ta e="T199" id="Seg_8107" s="T198">pro:R</ta>
            <ta e="T200" id="Seg_8108" s="T199">0.2:A</ta>
            <ta e="T201" id="Seg_8109" s="T200">pro.h:Th</ta>
            <ta e="T204" id="Seg_8110" s="T203">pro:R</ta>
            <ta e="T205" id="Seg_8111" s="T204">0.2:A</ta>
            <ta e="T206" id="Seg_8112" s="T205">pro.h:Th</ta>
            <ta e="T207" id="Seg_8113" s="T206">np:Th</ta>
            <ta e="T208" id="Seg_8114" s="T207">pro.h:Poss</ta>
            <ta e="T210" id="Seg_8115" s="T209">pro.h:Poss</ta>
            <ta e="T211" id="Seg_8116" s="T210">np.h:A</ta>
            <ta e="T217" id="Seg_8117" s="T216">adv:L</ta>
            <ta e="T218" id="Seg_8118" s="T217">np:G</ta>
            <ta e="T221" id="Seg_8119" s="T220">0.3.h:A</ta>
            <ta e="T227" id="Seg_8120" s="T226">0.3.h:A</ta>
            <ta e="T228" id="Seg_8121" s="T227">pro.h:Poss</ta>
            <ta e="T229" id="Seg_8122" s="T228">np:Th</ta>
            <ta e="T232" id="Seg_8123" s="T231">pro.h:Poss</ta>
            <ta e="T233" id="Seg_8124" s="T232">np:Th</ta>
            <ta e="T236" id="Seg_8125" s="T235">pro.h:A</ta>
            <ta e="T237" id="Seg_8126" s="T236">np:P</ta>
            <ta e="T245" id="Seg_8127" s="T244">np:P</ta>
            <ta e="T249" id="Seg_8128" s="T248">np.h:A</ta>
            <ta e="T254" id="Seg_8129" s="T253">np:Th</ta>
            <ta e="T255" id="Seg_8130" s="T254">0.3.h:A</ta>
            <ta e="T257" id="Seg_8131" s="T256">np:P</ta>
            <ta e="T259" id="Seg_8132" s="T258">0.3.h:A</ta>
            <ta e="T260" id="Seg_8133" s="T259">np:P</ta>
            <ta e="T262" id="Seg_8134" s="T261">np:G</ta>
            <ta e="T263" id="Seg_8135" s="T262">0.3.h:A</ta>
            <ta e="T265" id="Seg_8136" s="T264">np:G</ta>
            <ta e="T266" id="Seg_8137" s="T265">0.3.h:A</ta>
            <ta e="T269" id="Seg_8138" s="T268">np:Th</ta>
            <ta e="T272" id="Seg_8139" s="T271">np:A</ta>
            <ta e="T274" id="Seg_8140" s="T273">pro.h:A</ta>
            <ta e="T276" id="Seg_8141" s="T275">0.1.h:E</ta>
            <ta e="T277" id="Seg_8142" s="T276">np:G</ta>
            <ta e="T279" id="Seg_8143" s="T278">0.1.h:A</ta>
            <ta e="T283" id="Seg_8144" s="T697">0.1.h:A</ta>
            <ta e="T284" id="Seg_8145" s="T283">pro.h:A</ta>
            <ta e="T286" id="Seg_8146" s="T285">np:Th</ta>
            <ta e="T291" id="Seg_8147" s="T290">pro.h:A</ta>
            <ta e="T294" id="Seg_8148" s="T293">adv:L</ta>
            <ta e="T295" id="Seg_8149" s="T294">0.1.h:A</ta>
            <ta e="T298" id="Seg_8150" s="T297">pro.h:A</ta>
            <ta e="T302" id="Seg_8151" s="T301">adv:L</ta>
            <ta e="T308" id="Seg_8152" s="T307">pro.h:Th</ta>
            <ta e="T311" id="Seg_8153" s="T310">adv:L</ta>
            <ta e="T312" id="Seg_8154" s="T311">0.1.h:E</ta>
            <ta e="T318" id="Seg_8155" s="T317">adv:Time</ta>
            <ta e="T319" id="Seg_8156" s="T318">np:Th</ta>
            <ta e="T320" id="Seg_8157" s="T319">np:Ins</ta>
            <ta e="T321" id="Seg_8158" s="T320">0.1.h:A</ta>
            <ta e="T323" id="Seg_8159" s="T322">0.1.h:A</ta>
            <ta e="T329" id="Seg_8160" s="T328">np:R</ta>
            <ta e="T330" id="Seg_8161" s="T329">0.3.h:A</ta>
            <ta e="T333" id="Seg_8162" s="T332">np:R</ta>
            <ta e="T335" id="Seg_8163" s="T334">0.3.h:A</ta>
            <ta e="T336" id="Seg_8164" s="T335">pro.h:A</ta>
            <ta e="T337" id="Seg_8165" s="T336">np:R</ta>
            <ta e="T339" id="Seg_8166" s="T338">np:Th</ta>
            <ta e="T340" id="Seg_8167" s="T339">pro.h:A</ta>
            <ta e="T342" id="Seg_8168" s="T341">np:R</ta>
            <ta e="T343" id="Seg_8169" s="T342">0.2.h:A</ta>
            <ta e="T344" id="Seg_8170" s="T343">np:Th</ta>
            <ta e="T345" id="Seg_8171" s="T344">pro:A</ta>
            <ta e="T347" id="Seg_8172" s="T346">np:R</ta>
            <ta e="T350" id="Seg_8173" s="T349">0.2.h:A</ta>
            <ta e="T351" id="Seg_8174" s="T350">0.1.h:A</ta>
            <ta e="T352" id="Seg_8175" s="T351">np.h:A</ta>
            <ta e="T358" id="Seg_8176" s="T357">0.2.h:A</ta>
            <ta e="T360" id="Seg_8177" s="T359">pro.h:R</ta>
            <ta e="T361" id="Seg_8178" s="T360">0.1.h:A</ta>
            <ta e="T363" id="Seg_8179" s="T362">np:Th</ta>
            <ta e="T364" id="Seg_8180" s="T363">0.1.h:A</ta>
            <ta e="T366" id="Seg_8181" s="T365">np:P</ta>
            <ta e="T367" id="Seg_8182" s="T366">0.3:A</ta>
            <ta e="T371" id="Seg_8183" s="T370">np.h:A</ta>
            <ta e="T373" id="Seg_8184" s="T372">0.3.h:A</ta>
            <ta e="T375" id="Seg_8185" s="T374">0.2.h:A</ta>
            <ta e="T377" id="Seg_8186" s="T376">0.2.h:A</ta>
            <ta e="T379" id="Seg_8187" s="T377">pro.h:A</ta>
            <ta e="T381" id="Seg_8188" s="T380">np:Ins</ta>
            <ta e="T382" id="Seg_8189" s="T381">np:Ins</ta>
            <ta e="T383" id="Seg_8190" s="T382">0.3.h:A</ta>
            <ta e="T385" id="Seg_8191" s="T384">adv:Time</ta>
            <ta e="T386" id="Seg_8192" s="T385">np:G</ta>
            <ta e="T387" id="Seg_8193" s="T386">0.3.h:A</ta>
            <ta e="T389" id="Seg_8194" s="T388">np:P</ta>
            <ta e="T390" id="Seg_8195" s="T389">0.3.h:A</ta>
            <ta e="T391" id="Seg_8196" s="T390">adv:L</ta>
            <ta e="T395" id="Seg_8197" s="T394">np:Th</ta>
            <ta e="T396" id="Seg_8198" s="T395">np:G</ta>
            <ta e="T397" id="Seg_8199" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_8200" s="T398">np:G</ta>
            <ta e="T400" id="Seg_8201" s="T399">0.3.h:A</ta>
            <ta e="T402" id="Seg_8202" s="T401">0.2.h:A</ta>
            <ta e="T403" id="Seg_8203" s="T402">np:P</ta>
            <ta e="T404" id="Seg_8204" s="T403">np:Ins</ta>
            <ta e="T406" id="Seg_8205" s="T405">0.2.h:A</ta>
            <ta e="T408" id="Seg_8206" s="T407">0.2.h:P</ta>
            <ta e="T410" id="Seg_8207" s="T409">0.2.h:P</ta>
            <ta e="T411" id="Seg_8208" s="T410">pro.h:Poss</ta>
            <ta e="T412" id="Seg_8209" s="T411">np.h:A</ta>
            <ta e="T414" id="Seg_8210" s="T413">pro:G</ta>
            <ta e="T416" id="Seg_8211" s="T415">np:G</ta>
            <ta e="T417" id="Seg_8212" s="T416">0.3.h:A</ta>
            <ta e="T420" id="Seg_8213" s="T419">np:Th</ta>
            <ta e="T421" id="Seg_8214" s="T420">np:L</ta>
            <ta e="T423" id="Seg_8215" s="T422">np:Th</ta>
            <ta e="T425" id="Seg_8216" s="T424">0.3.h:A</ta>
            <ta e="T426" id="Seg_8217" s="T425">np:Ins</ta>
            <ta e="T428" id="Seg_8218" s="T427">0.3.h:A</ta>
            <ta e="T429" id="Seg_8219" s="T428">np:G</ta>
            <ta e="T431" id="Seg_8220" s="T430">np.h:A</ta>
            <ta e="T434" id="Seg_8221" s="T433">0.3.h:A</ta>
            <ta e="T436" id="Seg_8222" s="T435">pro:Th</ta>
            <ta e="T440" id="Seg_8223" s="T439">np.h:A</ta>
            <ta e="T445" id="Seg_8224" s="T444">pro:Th</ta>
            <ta e="T450" id="Seg_8225" s="T449">0.2.h:A</ta>
            <ta e="T452" id="Seg_8226" s="T451">0.2.h:A</ta>
            <ta e="T454" id="Seg_8227" s="T453">0.2.h:A</ta>
            <ta e="T458" id="Seg_8228" s="T457">np:Th</ta>
            <ta e="T461" id="Seg_8229" s="T460">np.h:A</ta>
            <ta e="T462" id="Seg_8230" s="T461">np.h:A</ta>
            <ta e="T465" id="Seg_8231" s="T464">np.h:E</ta>
            <ta e="T468" id="Seg_8232" s="T467">np.h:A</ta>
            <ta e="T472" id="Seg_8233" s="T471">np:G</ta>
            <ta e="T474" id="Seg_8234" s="T473">0.3.h:E</ta>
            <ta e="T477" id="Seg_8235" s="T476">0.3.h:A</ta>
            <ta e="T478" id="Seg_8236" s="T477">np:G</ta>
            <ta e="T479" id="Seg_8237" s="T478">pro.h:A</ta>
            <ta e="T483" id="Seg_8238" s="T482">pro.h:A</ta>
            <ta e="T488" id="Seg_8239" s="T487">pro.h:Poss</ta>
            <ta e="T489" id="Seg_8240" s="T488">np.h:E</ta>
            <ta e="T491" id="Seg_8241" s="T490">np.h:B</ta>
            <ta e="T495" id="Seg_8242" s="T494">pro.h:R</ta>
            <ta e="T496" id="Seg_8243" s="T495">np:Th</ta>
            <ta e="T498" id="Seg_8244" s="T497">0.3.h:A</ta>
            <ta e="T499" id="Seg_8245" s="T498">pro:Th</ta>
            <ta e="T501" id="Seg_8246" s="T500">np:Th</ta>
            <ta e="T502" id="Seg_8247" s="T501">0.3.h:A</ta>
            <ta e="T503" id="Seg_8248" s="T502">np:Th</ta>
            <ta e="T504" id="Seg_8249" s="T503">np:Th</ta>
            <ta e="T505" id="Seg_8250" s="T504">0.3.h:A</ta>
            <ta e="T507" id="Seg_8251" s="T506">np:Th</ta>
            <ta e="T508" id="Seg_8252" s="T507">pro.h:R</ta>
            <ta e="T509" id="Seg_8253" s="T508">0.3.h:A</ta>
            <ta e="T510" id="Seg_8254" s="T509">np.h:E</ta>
            <ta e="T512" id="Seg_8255" s="T511">np:L</ta>
            <ta e="T515" id="Seg_8256" s="T514">n:Time</ta>
            <ta e="T516" id="Seg_8257" s="T515">adv:L</ta>
            <ta e="T517" id="Seg_8258" s="T516">0.3.h:A</ta>
            <ta e="T523" id="Seg_8259" s="T522">np.h:A</ta>
            <ta e="T526" id="Seg_8260" s="T525">np:Ins</ta>
            <ta e="T532" id="Seg_8261" s="T531">0.3.h:A</ta>
            <ta e="T533" id="Seg_8262" s="T532">np.h:A</ta>
            <ta e="T534" id="Seg_8263" s="T533">pro:G</ta>
            <ta e="T536" id="Seg_8264" s="T535">pro.h:A</ta>
            <ta e="T538" id="Seg_8265" s="T537">adv:L</ta>
            <ta e="T541" id="Seg_8266" s="T540">np.h:Th</ta>
            <ta e="T542" id="Seg_8267" s="T541">np:L</ta>
            <ta e="T544" id="Seg_8268" s="T543">0.3.h:A</ta>
            <ta e="T552" id="Seg_8269" s="T551">n:Time</ta>
            <ta e="T554" id="Seg_8270" s="T553">np.h:A</ta>
            <ta e="T555" id="Seg_8271" s="T554">np.h:Th</ta>
            <ta e="T556" id="Seg_8272" s="T555">0.3.h:A</ta>
            <ta e="T557" id="Seg_8273" s="T556">np:Poss</ta>
            <ta e="T558" id="Seg_8274" s="T557">np:L</ta>
            <ta e="T559" id="Seg_8275" s="T558">0.3.h:A</ta>
            <ta e="T560" id="Seg_8276" s="T559">adv:Time</ta>
            <ta e="T562" id="Seg_8277" s="T561">np.h:Th</ta>
            <ta e="T563" id="Seg_8278" s="T562">0.3.h:A</ta>
            <ta e="T564" id="Seg_8279" s="T563">0.3.h:A</ta>
            <ta e="T566" id="Seg_8280" s="T565">np:Ins</ta>
            <ta e="T568" id="Seg_8281" s="T567">np.h:E</ta>
            <ta e="T570" id="Seg_8282" s="T569">np:L</ta>
            <ta e="T573" id="Seg_8283" s="T572">pro.h:Poss</ta>
            <ta e="T574" id="Seg_8284" s="T573">np.h:Th</ta>
            <ta e="T575" id="Seg_8285" s="T574">0.3.h:A</ta>
            <ta e="T579" id="Seg_8286" s="T578">0.3.h:A</ta>
            <ta e="T580" id="Seg_8287" s="T579">np:Th</ta>
            <ta e="T582" id="Seg_8288" s="T581">np.h:Th 0.1.h:Poss</ta>
            <ta e="T584" id="Seg_8289" s="T583">0.3.h:A</ta>
            <ta e="T585" id="Seg_8290" s="T584">np:Th</ta>
            <ta e="T587" id="Seg_8291" s="T586">np:Th</ta>
            <ta e="T588" id="Seg_8292" s="T587">pro.h:Poss</ta>
            <ta e="T590" id="Seg_8293" s="T589">np:Th</ta>
            <ta e="T591" id="Seg_8294" s="T590">np.h:Th 0.3.h:Poss</ta>
            <ta e="T604" id="Seg_8295" s="T603">np.h:Th</ta>
            <ta e="T605" id="Seg_8296" s="T604">0.3.h:A</ta>
            <ta e="T608" id="Seg_8297" s="T607">np.h:Th</ta>
            <ta e="T610" id="Seg_8298" s="T609">0.3.h:A</ta>
            <ta e="T612" id="Seg_8299" s="T611">adv:Time</ta>
            <ta e="T616" id="Seg_8300" s="T615">pro.h:Th</ta>
            <ta e="T617" id="Seg_8301" s="T616">0.3.h:A</ta>
            <ta e="T620" id="Seg_8302" s="T619">pro.h:Poss</ta>
            <ta e="T622" id="Seg_8303" s="T621">0.3.h:A</ta>
            <ta e="T624" id="Seg_8304" s="T623">adv:Time</ta>
            <ta e="T626" id="Seg_8305" s="T625">np.h:Th</ta>
            <ta e="T627" id="Seg_8306" s="T626">adv:Time</ta>
            <ta e="T629" id="Seg_8307" s="T628">np.h:Th</ta>
            <ta e="T630" id="Seg_8308" s="T629">adv:Time</ta>
            <ta e="T631" id="Seg_8309" s="T630">np.h:Th</ta>
            <ta e="T633" id="Seg_8310" s="T632">adv:Time</ta>
            <ta e="T634" id="Seg_8311" s="T633">np.h:A</ta>
            <ta e="T638" id="Seg_8312" s="T637">0.3.h:A</ta>
            <ta e="T641" id="Seg_8313" s="T640">np.h:P</ta>
            <ta e="T643" id="Seg_8314" s="T642">pro.h:Th</ta>
            <ta e="T649" id="Seg_8315" s="T648">np.h:P</ta>
            <ta e="T668" id="Seg_8316" s="T667">np.h:P</ta>
            <ta e="T672" id="Seg_8317" s="T671">np:Th</ta>
            <ta e="T674" id="Seg_8318" s="T673">pro.h:B</ta>
            <ta e="T676" id="Seg_8319" s="T675">np.h:P</ta>
            <ta e="T678" id="Seg_8320" s="T676">0.3.h:A</ta>
            <ta e="T679" id="Seg_8321" s="T678">pro.h:Poss</ta>
            <ta e="T680" id="Seg_8322" s="T679">np.h:A</ta>
            <ta e="T682" id="Seg_8323" s="T681">pro.h:P</ta>
            <ta e="T683" id="Seg_8324" s="T682">np.h:P</ta>
            <ta e="T686" id="Seg_8325" s="T685">np:Th</ta>
            <ta e="T689" id="Seg_8326" s="T688">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T6" id="Seg_8327" s="T5">v:pred 0.2.h:S</ta>
            <ta e="T8" id="Seg_8328" s="T7">np:O</ta>
            <ta e="T9" id="Seg_8329" s="T8">v:pred 0.2.h:S</ta>
            <ta e="T11" id="Seg_8330" s="T10">v:pred 0.3:S</ta>
            <ta e="T14" id="Seg_8331" s="T13">v:pred 0.2.h:S</ta>
            <ta e="T16" id="Seg_8332" s="T15">pro.h:S</ta>
            <ta e="T18" id="Seg_8333" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_8334" s="T18">ptcl:pred</ta>
            <ta e="T22" id="Seg_8335" s="T21">np:O</ta>
            <ta e="T30" id="Seg_8336" s="T29">v:pred 0.2.h:S</ta>
            <ta e="T32" id="Seg_8337" s="T31">v:pred 0.2.h:S</ta>
            <ta e="T35" id="Seg_8338" s="T34">v:pred 0.2.h:S</ta>
            <ta e="T37" id="Seg_8339" s="T36">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_8340" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_8341" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_8342" s="T44">np.h:O</ta>
            <ta e="T46" id="Seg_8343" s="T45">v:pred 0.3.h:S</ta>
            <ta e="T47" id="Seg_8344" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_8345" s="T47">v:pred</ta>
            <ta e="T53" id="Seg_8346" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_8347" s="T53">ptcl.neg</ta>
            <ta e="T55" id="Seg_8348" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_8349" s="T56">pro.h:S</ta>
            <ta e="T60" id="Seg_8350" s="T59">np.h:O</ta>
            <ta e="T61" id="Seg_8351" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_8352" s="T62">pro.h:S</ta>
            <ta e="T66" id="Seg_8353" s="T65">np:O</ta>
            <ta e="T67" id="Seg_8354" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_8355" s="T68">np:O</ta>
            <ta e="T70" id="Seg_8356" s="T69">v:pred 0.1.h:S</ta>
            <ta e="T72" id="Seg_8357" s="T71">pro.h:O</ta>
            <ta e="T73" id="Seg_8358" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_8359" s="T75">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_8360" s="T76">pro.h:O</ta>
            <ta e="T78" id="Seg_8361" s="T77">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_8362" s="T82">v:pred 0.3.h:S</ta>
            <ta e="T86" id="Seg_8363" s="T85">v:pred 0.2.h:S</ta>
            <ta e="T88" id="Seg_8364" s="T87">pro.h:S</ta>
            <ta e="T90" id="Seg_8365" s="T89">n:pred</ta>
            <ta e="T92" id="Seg_8366" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_8367" s="T92">s:purp</ta>
            <ta e="T95" id="Seg_8368" s="T94">n:pred</ta>
            <ta e="T97" id="Seg_8369" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_8370" s="T97">pro.h:O</ta>
            <ta e="T99" id="Seg_8371" s="T98">v:pred</ta>
            <ta e="T101" id="Seg_8372" s="T100">np.h:S</ta>
            <ta e="T696" id="Seg_8373" s="T102">conv:pred</ta>
            <ta e="T103" id="Seg_8374" s="T696">v:pred</ta>
            <ta e="T109" id="Seg_8375" s="T108">v:pred 0.1.h:S</ta>
            <ta e="T112" id="Seg_8376" s="T111">np:S</ta>
            <ta e="T113" id="Seg_8377" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_8378" s="T113">pro.h:S</ta>
            <ta e="T115" id="Seg_8379" s="T114">np:O</ta>
            <ta e="T116" id="Seg_8380" s="T115">v:pred</ta>
            <ta e="T120" id="Seg_8381" s="T119">pro:O</ta>
            <ta e="T121" id="Seg_8382" s="T120">v:pred 0.1.h:S</ta>
            <ta e="T124" id="Seg_8383" s="T123">v:pred 0.1.h:S</ta>
            <ta e="T130" id="Seg_8384" s="T129">v:pred</ta>
            <ta e="T133" id="Seg_8385" s="T132">np:S</ta>
            <ta e="T138" id="Seg_8386" s="T137">v:pred</ta>
            <ta e="T142" id="Seg_8387" s="T140">np:S</ta>
            <ta e="T143" id="Seg_8388" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_8389" s="T143">pro:S</ta>
            <ta e="T148" id="Seg_8390" s="T147">v:pred</ta>
            <ta e="T152" id="Seg_8391" s="T151">v:pred</ta>
            <ta e="T159" id="Seg_8392" s="T158">np:S</ta>
            <ta e="T160" id="Seg_8393" s="T159">v:pred</ta>
            <ta e="T168" id="Seg_8394" s="T167">v:pred 0.2:S</ta>
            <ta e="T170" id="Seg_8395" s="T169">pro.h:O</ta>
            <ta e="T171" id="Seg_8396" s="T170">v:pred 0.2:S</ta>
            <ta e="T173" id="Seg_8397" s="T172">pro.h:O</ta>
            <ta e="T174" id="Seg_8398" s="T173">v:pred 0.2:S</ta>
            <ta e="T176" id="Seg_8399" s="T175">pro.h:O</ta>
            <ta e="T177" id="Seg_8400" s="T176">pro:S</ta>
            <ta e="T183" id="Seg_8401" s="T182">np:O</ta>
            <ta e="T185" id="Seg_8402" s="T184">pro:S</ta>
            <ta e="T186" id="Seg_8403" s="T185">v:pred</ta>
            <ta e="T189" id="Seg_8404" s="T188">np:S</ta>
            <ta e="T191" id="Seg_8405" s="T190">adj:pred</ta>
            <ta e="T192" id="Seg_8406" s="T191">cop</ta>
            <ta e="T193" id="Seg_8407" s="T192">np:S</ta>
            <ta e="T194" id="Seg_8408" s="T193">adj:pred</ta>
            <ta e="T195" id="Seg_8409" s="T194">cop</ta>
            <ta e="T196" id="Seg_8410" s="T195">v:pred 0.2:S</ta>
            <ta e="T197" id="Seg_8411" s="T196">pro.h:O</ta>
            <ta e="T198" id="Seg_8412" s="T197">v:pred</ta>
            <ta e="T199" id="Seg_8413" s="T198">pro:O</ta>
            <ta e="T200" id="Seg_8414" s="T199">v:pred 0.2:S</ta>
            <ta e="T201" id="Seg_8415" s="T200">pro.h:O</ta>
            <ta e="T205" id="Seg_8416" s="T204">v:pred 0.2:S</ta>
            <ta e="T206" id="Seg_8417" s="T205">pro.h:O</ta>
            <ta e="T207" id="Seg_8418" s="T206">np:O</ta>
            <ta e="T209" id="Seg_8419" s="T208">v:pred</ta>
            <ta e="T211" id="Seg_8420" s="T210">np.h:S</ta>
            <ta e="T215" id="Seg_8421" s="T214">v:pred</ta>
            <ta e="T221" id="Seg_8422" s="T220">v:pred 0.3.h:S</ta>
            <ta e="T227" id="Seg_8423" s="T226">v:pred 0.3.h:S</ta>
            <ta e="T229" id="Seg_8424" s="T228">np:O</ta>
            <ta e="T230" id="Seg_8425" s="T229">v:pred</ta>
            <ta e="T233" id="Seg_8426" s="T232">np:O</ta>
            <ta e="T234" id="Seg_8427" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_8428" s="T234">ptcl:pred</ta>
            <ta e="T237" id="Seg_8429" s="T236">np:O</ta>
            <ta e="T245" id="Seg_8430" s="T244">np:O</ta>
            <ta e="T246" id="Seg_8431" s="T245">v:pred</ta>
            <ta e="T249" id="Seg_8432" s="T248">np.h:S</ta>
            <ta e="T252" id="Seg_8433" s="T251">v:pred</ta>
            <ta e="T254" id="Seg_8434" s="T253">np:O</ta>
            <ta e="T255" id="Seg_8435" s="T254">v:pred 0.3.h:S</ta>
            <ta e="T257" id="Seg_8436" s="T256">np:O</ta>
            <ta e="T259" id="Seg_8437" s="T258">v:pred 0.3.h:S</ta>
            <ta e="T260" id="Seg_8438" s="T259">np:O</ta>
            <ta e="T263" id="Seg_8439" s="T262">v:pred 0.3.h:S</ta>
            <ta e="T266" id="Seg_8440" s="T265">v:pred 0.3.h:S</ta>
            <ta e="T269" id="Seg_8441" s="T268">np:O</ta>
            <ta e="T271" id="Seg_8442" s="T270">v:pred </ta>
            <ta e="T272" id="Seg_8443" s="T271">np:S</ta>
            <ta e="T274" id="Seg_8444" s="T273">pro.h:S</ta>
            <ta e="T275" id="Seg_8445" s="T274">ptcl.neg</ta>
            <ta e="T276" id="Seg_8446" s="T275">v:pred 0.1.h:S</ta>
            <ta e="T279" id="Seg_8447" s="T278">v:pred 0.1.h:S</ta>
            <ta e="T697" id="Seg_8448" s="T282">conv:pred</ta>
            <ta e="T283" id="Seg_8449" s="T697">v:pred 0.1.h:S</ta>
            <ta e="T284" id="Seg_8450" s="T283">pro.h:S</ta>
            <ta e="T285" id="Seg_8451" s="T284">v:pred</ta>
            <ta e="T286" id="Seg_8452" s="T285">np:S</ta>
            <ta e="T290" id="Seg_8453" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_8454" s="T290">pro.h:S</ta>
            <ta e="T292" id="Seg_8455" s="T291">v:pred</ta>
            <ta e="T295" id="Seg_8456" s="T294">v:pred 0.1.h:S</ta>
            <ta e="T298" id="Seg_8457" s="T297">pro.h:S</ta>
            <ta e="T301" id="Seg_8458" s="T300">v:pred</ta>
            <ta e="T305" id="Seg_8459" s="T304">adj:pred</ta>
            <ta e="T308" id="Seg_8460" s="T307">pro.h:S</ta>
            <ta e="T309" id="Seg_8461" s="T308">v:pred</ta>
            <ta e="T312" id="Seg_8462" s="T311">v:pred 0.1.h:S</ta>
            <ta e="T319" id="Seg_8463" s="T318">np:O</ta>
            <ta e="T321" id="Seg_8464" s="T320">v:pred 0.1.h:S</ta>
            <ta e="T323" id="Seg_8465" s="T322">v:pred 0.1.h:S</ta>
            <ta e="T329" id="Seg_8466" s="T328">np:O</ta>
            <ta e="T330" id="Seg_8467" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T333" id="Seg_8468" s="T332">np:O</ta>
            <ta e="T334" id="Seg_8469" s="T333">ptcl.neg</ta>
            <ta e="T335" id="Seg_8470" s="T334">v:pred 0.3.h:S</ta>
            <ta e="T336" id="Seg_8471" s="T335">pro.h:S</ta>
            <ta e="T338" id="Seg_8472" s="T337">v:pred</ta>
            <ta e="T339" id="Seg_8473" s="T338">np:O</ta>
            <ta e="T340" id="Seg_8474" s="T339">pro.h:S</ta>
            <ta e="T341" id="Seg_8475" s="T340">v:pred</ta>
            <ta e="T343" id="Seg_8476" s="T342">v:pred 0.2.h:S</ta>
            <ta e="T344" id="Seg_8477" s="T343">np:O</ta>
            <ta e="T345" id="Seg_8478" s="T344">pro:S</ta>
            <ta e="T346" id="Seg_8479" s="T345">v:pred</ta>
            <ta e="T349" id="Seg_8480" s="T348">ptcl.neg</ta>
            <ta e="T350" id="Seg_8481" s="T349">v:pred 0.2.h:S</ta>
            <ta e="T351" id="Seg_8482" s="T350">v:pred 0.1.h:S</ta>
            <ta e="T352" id="Seg_8483" s="T351">np.h:S</ta>
            <ta e="T353" id="Seg_8484" s="T352">v:pred</ta>
            <ta e="T358" id="Seg_8485" s="T357">v:pred 0.2.h:S</ta>
            <ta e="T360" id="Seg_8486" s="T359">pro.h:O</ta>
            <ta e="T361" id="Seg_8487" s="T360">v:pred 0.1.h:S</ta>
            <ta e="T363" id="Seg_8488" s="T362">np:O</ta>
            <ta e="T364" id="Seg_8489" s="T363">v:pred 0.1.h:S</ta>
            <ta e="T366" id="Seg_8490" s="T365">np:O</ta>
            <ta e="T367" id="Seg_8491" s="T366">v:pred 0.3:S</ta>
            <ta e="T370" id="Seg_8492" s="T369">v:pred</ta>
            <ta e="T371" id="Seg_8493" s="T370">np.h:S</ta>
            <ta e="T372" id="Seg_8494" s="T371">ptcl.neg</ta>
            <ta e="T373" id="Seg_8495" s="T372">v:pred 0.3.h:S</ta>
            <ta e="T374" id="Seg_8496" s="T373">ptcl.neg</ta>
            <ta e="T375" id="Seg_8497" s="T374">v:pred 0.2.h:S</ta>
            <ta e="T377" id="Seg_8498" s="T376">v:pred 0.2.h:S</ta>
            <ta e="T379" id="Seg_8499" s="T377">pro.h:S</ta>
            <ta e="T380" id="Seg_8500" s="T379">np:O</ta>
            <ta e="T383" id="Seg_8501" s="T382">v:pred 0.3.h:S</ta>
            <ta e="T387" id="Seg_8502" s="T386">v:pred 0.3.h:S</ta>
            <ta e="T389" id="Seg_8503" s="T388">np:O</ta>
            <ta e="T390" id="Seg_8504" s="T389">v:pred 0.3.h:S</ta>
            <ta e="T395" id="Seg_8505" s="T394">np:O</ta>
            <ta e="T397" id="Seg_8506" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T400" id="Seg_8507" s="T399">v:pred 0.3.h:S</ta>
            <ta e="T402" id="Seg_8508" s="T401">v:pred 0.2.h:S</ta>
            <ta e="T403" id="Seg_8509" s="T402">np:O</ta>
            <ta e="T406" id="Seg_8510" s="T405">v:pred 0.2.h:S</ta>
            <ta e="T407" id="Seg_8511" s="T406">adj:pred</ta>
            <ta e="T408" id="Seg_8512" s="T407">cop 0.2.h:S</ta>
            <ta e="T409" id="Seg_8513" s="T408">adj:pred</ta>
            <ta e="T410" id="Seg_8514" s="T409">cop 0.2.h:S</ta>
            <ta e="T412" id="Seg_8515" s="T411">np.h:S</ta>
            <ta e="T415" id="Seg_8516" s="T414">v:pred</ta>
            <ta e="T417" id="Seg_8517" s="T416">v:pred 0.3.h:S</ta>
            <ta e="T420" id="Seg_8518" s="T419">np:S</ta>
            <ta e="T421" id="Seg_8519" s="T420">n:pred</ta>
            <ta e="T423" id="Seg_8520" s="T422">np:O</ta>
            <ta e="T425" id="Seg_8521" s="T424">v:pred 0.3.h:S</ta>
            <ta e="T428" id="Seg_8522" s="T427">v:pred 0.3.h:S</ta>
            <ta e="T431" id="Seg_8523" s="T430">np.h:S</ta>
            <ta e="T433" id="Seg_8524" s="T432">v:pred</ta>
            <ta e="T434" id="Seg_8525" s="T433">v:pred 0.3.h:S</ta>
            <ta e="T436" id="Seg_8526" s="T435">pro:S</ta>
            <ta e="T437" id="Seg_8527" s="T436">v:pred</ta>
            <ta e="T440" id="Seg_8528" s="T439">np.h:S</ta>
            <ta e="T442" id="Seg_8529" s="T441">ptcl.neg</ta>
            <ta e="T444" id="Seg_8530" s="T443">v:pred</ta>
            <ta e="T445" id="Seg_8531" s="T444">pro:S</ta>
            <ta e="T447" id="Seg_8532" s="T446">v:pred</ta>
            <ta e="T450" id="Seg_8533" s="T449">v:pred 0.2.h:S</ta>
            <ta e="T452" id="Seg_8534" s="T451">v:pred 0.2.h:S</ta>
            <ta e="T454" id="Seg_8535" s="T453">v:pred 0.2.h:S</ta>
            <ta e="T458" id="Seg_8536" s="T457">np:S</ta>
            <ta e="T698" id="Seg_8537" s="T458">conv:pred</ta>
            <ta e="T459" id="Seg_8538" s="T698">v:pred</ta>
            <ta e="T460" id="Seg_8539" s="T459">v:pred</ta>
            <ta e="T461" id="Seg_8540" s="T460">np.h:S</ta>
            <ta e="T462" id="Seg_8541" s="T461">np.h:S</ta>
            <ta e="T463" id="Seg_8542" s="T462">v:pred</ta>
            <ta e="T465" id="Seg_8543" s="T464">np.h:S</ta>
            <ta e="T467" id="Seg_8544" s="T466">v:pred</ta>
            <ta e="T468" id="Seg_8545" s="T467">np.h:S</ta>
            <ta e="T471" id="Seg_8546" s="T470">v:pred</ta>
            <ta e="T474" id="Seg_8547" s="T473">v:pred 0.3.h:S</ta>
            <ta e="T477" id="Seg_8548" s="T476">v:pred 0.3.h:S</ta>
            <ta e="T479" id="Seg_8549" s="T478">pro.h:S</ta>
            <ta e="T482" id="Seg_8550" s="T481">v:pred</ta>
            <ta e="T483" id="Seg_8551" s="T482">pro.h:S</ta>
            <ta e="T485" id="Seg_8552" s="T484">v:pred</ta>
            <ta e="T489" id="Seg_8553" s="T488">np.h:S</ta>
            <ta e="T491" id="Seg_8554" s="T490">np.h:O</ta>
            <ta e="T493" id="Seg_8555" s="T492">v:pred</ta>
            <ta e="T496" id="Seg_8556" s="T495">np:O</ta>
            <ta e="T498" id="Seg_8557" s="T497">v:pred 0.3.h:S</ta>
            <ta e="T499" id="Seg_8558" s="T498">pro:S</ta>
            <ta e="T500" id="Seg_8559" s="T499">v:pred</ta>
            <ta e="T501" id="Seg_8560" s="T500">np:O</ta>
            <ta e="T502" id="Seg_8561" s="T501">v:pred 0.3.h:S</ta>
            <ta e="T503" id="Seg_8562" s="T502">np:O</ta>
            <ta e="T504" id="Seg_8563" s="T503">np:O</ta>
            <ta e="T505" id="Seg_8564" s="T504">v:pred 0.3.h:S</ta>
            <ta e="T507" id="Seg_8565" s="T506">np:O</ta>
            <ta e="T509" id="Seg_8566" s="T508">v:pred 0.3.h:S</ta>
            <ta e="T510" id="Seg_8567" s="T509">np.h:S</ta>
            <ta e="T513" id="Seg_8568" s="T512">v:pred</ta>
            <ta e="T517" id="Seg_8569" s="T516">v:pred 0.3.h:S</ta>
            <ta e="T518" id="Seg_8570" s="T517">s:purp</ta>
            <ta e="T522" id="Seg_8571" s="T521">v:pred</ta>
            <ta e="T523" id="Seg_8572" s="T522">np.h:S</ta>
            <ta e="T531" id="Seg_8573" s="T530">ptcl.neg</ta>
            <ta e="T532" id="Seg_8574" s="T531">v:pred 0.3.h:S</ta>
            <ta e="T533" id="Seg_8575" s="T532">np.h:S</ta>
            <ta e="T535" id="Seg_8576" s="T534">v:pred</ta>
            <ta e="T536" id="Seg_8577" s="T535">pro.h:S</ta>
            <ta e="T539" id="Seg_8578" s="T538">v:pred</ta>
            <ta e="T541" id="Seg_8579" s="T540">np.h:S</ta>
            <ta e="T543" id="Seg_8580" s="T542">v:pred</ta>
            <ta e="T544" id="Seg_8581" s="T543">v:pred 0.3.h:S</ta>
            <ta e="T553" id="Seg_8582" s="T552">v:pred</ta>
            <ta e="T554" id="Seg_8583" s="T553">np.h:S</ta>
            <ta e="T555" id="Seg_8584" s="T554">np.h:O</ta>
            <ta e="T556" id="Seg_8585" s="T555">v:pred 0.3.h:S</ta>
            <ta e="T559" id="Seg_8586" s="T558">v:pred 0.3.h:S</ta>
            <ta e="T562" id="Seg_8587" s="T561">np.h:O</ta>
            <ta e="T563" id="Seg_8588" s="T562">v:pred 0.3.h:S</ta>
            <ta e="T564" id="Seg_8589" s="T563">v:pred 0.3.h:S</ta>
            <ta e="T568" id="Seg_8590" s="T567">np.h:S</ta>
            <ta e="T572" id="Seg_8591" s="T571">v:pred</ta>
            <ta e="T574" id="Seg_8592" s="T573">np.h:O</ta>
            <ta e="T575" id="Seg_8593" s="T574">v:pred 0.3.h:S</ta>
            <ta e="T579" id="Seg_8594" s="T578">v:pred 0.3.h:S</ta>
            <ta e="T580" id="Seg_8595" s="T579">np:O</ta>
            <ta e="T582" id="Seg_8596" s="T581">np.h:S</ta>
            <ta e="T583" id="Seg_8597" s="T582">adj:pred</ta>
            <ta e="T584" id="Seg_8598" s="T583">v:pred 0.3.h:S</ta>
            <ta e="T585" id="Seg_8599" s="T584">np:O</ta>
            <ta e="T587" id="Seg_8600" s="T586">np:S</ta>
            <ta e="T589" id="Seg_8601" s="T588">cop</ta>
            <ta e="T590" id="Seg_8602" s="T589">n:pred</ta>
            <ta e="T591" id="Seg_8603" s="T590">np.h:S</ta>
            <ta e="T592" id="Seg_8604" s="T591">v:pred</ta>
            <ta e="T604" id="Seg_8605" s="T603">np.h:O</ta>
            <ta e="T605" id="Seg_8606" s="T604">v:pred 0.3.h:S</ta>
            <ta e="T608" id="Seg_8607" s="T607">np.h:O</ta>
            <ta e="T610" id="Seg_8608" s="T609">v:pred 0.3.h:S</ta>
            <ta e="T617" id="Seg_8609" s="T616">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_8610" s="T620">np.h:O</ta>
            <ta e="T622" id="Seg_8611" s="T621">v:pred 0.3.h:S</ta>
            <ta e="T625" id="Seg_8612" s="T624">v:pred</ta>
            <ta e="T626" id="Seg_8613" s="T625">np.h:S</ta>
            <ta e="T628" id="Seg_8614" s="T627">v:pred</ta>
            <ta e="T629" id="Seg_8615" s="T628">np.h:S</ta>
            <ta e="T631" id="Seg_8616" s="T630">np.h:S</ta>
            <ta e="T632" id="Seg_8617" s="T631">v:pred</ta>
            <ta e="T634" id="Seg_8618" s="T633">np.h:S</ta>
            <ta e="T635" id="Seg_8619" s="T634">v:pred</ta>
            <ta e="T638" id="Seg_8620" s="T637">v:pred 0.3.h:S</ta>
            <ta e="T641" id="Seg_8621" s="T640">np.h:S</ta>
            <ta e="T642" id="Seg_8622" s="T641">v:pred</ta>
            <ta e="T643" id="Seg_8623" s="T642">pro.h:S</ta>
            <ta e="T645" id="Seg_8624" s="T644">v:pred</ta>
            <ta e="T649" id="Seg_8625" s="T648">np.h:S</ta>
            <ta e="T650" id="Seg_8626" s="T649">v:pred</ta>
            <ta e="T668" id="Seg_8627" s="T667">np.h:S</ta>
            <ta e="T669" id="Seg_8628" s="T668">v:pred</ta>
            <ta e="T672" id="Seg_8629" s="T671">np:S</ta>
            <ta e="T673" id="Seg_8630" s="T672">v:pred</ta>
            <ta e="T676" id="Seg_8631" s="T675">np.h:O</ta>
            <ta e="T678" id="Seg_8632" s="T676">v:pred 0.3.h:S</ta>
            <ta e="T680" id="Seg_8633" s="T679">np.h:S</ta>
            <ta e="T681" id="Seg_8634" s="T680">v:pred</ta>
            <ta e="T682" id="Seg_8635" s="T681">pro.h:O</ta>
            <ta e="T683" id="Seg_8636" s="T682">np.h:S</ta>
            <ta e="T684" id="Seg_8637" s="T683">v:pred</ta>
            <ta e="T686" id="Seg_8638" s="T685">np:S</ta>
            <ta e="T687" id="Seg_8639" s="T699">v:pred</ta>
            <ta e="T689" id="Seg_8640" s="T688">pro.h:S</ta>
            <ta e="T691" id="Seg_8641" s="T690">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T10" id="Seg_8642" s="T9">TURK:disc</ta>
            <ta e="T15" id="Seg_8643" s="T14">RUS:gram</ta>
            <ta e="T19" id="Seg_8644" s="T18">RUS:mod</ta>
            <ta e="T31" id="Seg_8645" s="T30">TURK:core</ta>
            <ta e="T34" id="Seg_8646" s="T33">RUS:mod</ta>
            <ta e="T45" id="Seg_8647" s="T44">TURK:cult</ta>
            <ta e="T52" id="Seg_8648" s="T51">RUS:gram</ta>
            <ta e="T58" id="Seg_8649" s="T57">TURK:disc</ta>
            <ta e="T60" id="Seg_8650" s="T59">TURK:cult</ta>
            <ta e="T64" id="Seg_8651" s="T63">TURK:disc</ta>
            <ta e="T65" id="Seg_8652" s="T64">RUS:cult</ta>
            <ta e="T67" id="Seg_8653" s="T66">%TURK:core</ta>
            <ta e="T68" id="Seg_8654" s="T67">RUS:cult</ta>
            <ta e="T74" id="Seg_8655" s="T73">RUS:gram</ta>
            <ta e="T81" id="Seg_8656" s="T79">RUS:gram</ta>
            <ta e="T82" id="Seg_8657" s="T81">RUS:gram</ta>
            <ta e="T90" id="Seg_8658" s="T89">TURK:cult</ta>
            <ta e="T91" id="Seg_8659" s="T90">RUS:gram</ta>
            <ta e="T93" id="Seg_8660" s="T92">%TURK:core</ta>
            <ta e="T95" id="Seg_8661" s="T94">TURK:cult</ta>
            <ta e="T96" id="Seg_8662" s="T95">RUS:gram</ta>
            <ta e="T104" id="Seg_8663" s="T103">RUS:cult</ta>
            <ta e="T105" id="Seg_8664" s="T104">TAT:cult</ta>
            <ta e="T110" id="Seg_8665" s="T109">%TURK:core</ta>
            <ta e="T117" id="Seg_8666" s="T116">RUS:gram</ta>
            <ta e="T122" id="Seg_8667" s="T121">RUS:gram</ta>
            <ta e="T127" id="Seg_8668" s="T126">TURK:disc</ta>
            <ta e="T139" id="Seg_8669" s="T138">RUS:gram</ta>
            <ta e="T142" id="Seg_8670" s="T140">TURK:mod(PTCL)</ta>
            <ta e="T144" id="Seg_8671" s="T143">TURK:gram(INDEF)</ta>
            <ta e="T161" id="Seg_8672" s="T160">RUS:mod</ta>
            <ta e="T213" id="Seg_8673" s="T212">TURK:disc</ta>
            <ta e="T222" id="Seg_8674" s="T221">RUS:gram</ta>
            <ta e="T223" id="Seg_8675" s="T222">RUS:mod</ta>
            <ta e="T231" id="Seg_8676" s="T230">RUS:gram</ta>
            <ta e="T235" id="Seg_8677" s="T234">RUS:mod</ta>
            <ta e="T237" id="Seg_8678" s="T236">TURK:cult</ta>
            <ta e="T250" id="Seg_8679" s="T249">TURK:disc</ta>
            <ta e="T251" id="Seg_8680" s="T250">RUS:cult</ta>
            <ta e="T253" id="Seg_8681" s="T252">RUS:gram</ta>
            <ta e="T254" id="Seg_8682" s="T253">TURK:cult</ta>
            <ta e="T256" id="Seg_8683" s="T255">RUS:gram</ta>
            <ta e="T261" id="Seg_8684" s="T260">TURK:disc</ta>
            <ta e="T264" id="Seg_8685" s="T263">RUS:gram</ta>
            <ta e="T265" id="Seg_8686" s="T264">TURK:cult</ta>
            <ta e="T269" id="Seg_8687" s="T268">TURK:disc</ta>
            <ta e="T273" id="Seg_8688" s="T272">RUS:gram</ta>
            <ta e="T278" id="Seg_8689" s="T277">TURK:disc</ta>
            <ta e="T281" id="Seg_8690" s="T280">RUS:gram</ta>
            <ta e="T282" id="Seg_8691" s="T281">TURK:disc</ta>
            <ta e="T293" id="Seg_8692" s="T292">RUS:gram</ta>
            <ta e="T296" id="Seg_8693" s="T295">TURK:disc</ta>
            <ta e="T303" id="Seg_8694" s="T302">TURK:disc</ta>
            <ta e="T310" id="Seg_8695" s="T309">RUS:gram</ta>
            <ta e="T313" id="Seg_8696" s="T312">TURK:disc</ta>
            <ta e="T319" id="Seg_8697" s="T318">RUS:core</ta>
            <ta e="T322" id="Seg_8698" s="T321">RUS:gram</ta>
            <ta e="T331" id="Seg_8699" s="T330">RUS:gram</ta>
            <ta e="T342" id="Seg_8700" s="T341">TURK:cult</ta>
            <ta e="T348" id="Seg_8701" s="T347">RUS:mod</ta>
            <ta e="T352" id="Seg_8702" s="T351">TURK:disc</ta>
            <ta e="T359" id="Seg_8703" s="T358">RUS:gram</ta>
            <ta e="T362" id="Seg_8704" s="T361">RUS:gram</ta>
            <ta e="T363" id="Seg_8705" s="T362">TURK:cult</ta>
            <ta e="T365" id="Seg_8706" s="T364">RUS:gram</ta>
            <ta e="T384" id="Seg_8707" s="T383">RUS:gram</ta>
            <ta e="T388" id="Seg_8708" s="T387">RUS:gram</ta>
            <ta e="T394" id="Seg_8709" s="T393">TURK:disc</ta>
            <ta e="T398" id="Seg_8710" s="T397">RUS:gram</ta>
            <ta e="T405" id="Seg_8711" s="T404">RUS:gram</ta>
            <ta e="T422" id="Seg_8712" s="T421">TURK:disc</ta>
            <ta e="T424" id="Seg_8713" s="T423">TURK:disc</ta>
            <ta e="T427" id="Seg_8714" s="T426">RUS:gram</ta>
            <ta e="T432" id="Seg_8715" s="T431">TURK:disc</ta>
            <ta e="T435" id="Seg_8716" s="T434">RUS:gram</ta>
            <ta e="T436" id="Seg_8717" s="T435">TURK:gram(INDEF)</ta>
            <ta e="T438" id="Seg_8718" s="T437">RUS:gram</ta>
            <ta e="T441" id="Seg_8719" s="T440">TURK:disc</ta>
            <ta e="T445" id="Seg_8720" s="T444">TURK:disc</ta>
            <ta e="T453" id="Seg_8721" s="T452">RUS:gram</ta>
            <ta e="T464" id="Seg_8722" s="T463">RUS:gram</ta>
            <ta e="T470" id="Seg_8723" s="T469">TURK:disc</ta>
            <ta e="T475" id="Seg_8724" s="T474">RUS:gram</ta>
            <ta e="T480" id="Seg_8725" s="T479">TURK:disc</ta>
            <ta e="T484" id="Seg_8726" s="T483">TURK:disc</ta>
            <ta e="T490" id="Seg_8727" s="T489">TURK:disc</ta>
            <ta e="T501" id="Seg_8728" s="T500">TURK:disc</ta>
            <ta e="T503" id="Seg_8729" s="T502">TURK:cult</ta>
            <ta e="T506" id="Seg_8730" s="T505">RUS:mod</ta>
            <ta e="T511" id="Seg_8731" s="T510">TURK:disc</ta>
            <ta e="T514" id="Seg_8732" s="T513">RUS:gram</ta>
            <ta e="T526" id="Seg_8733" s="T525">TURK:cult</ta>
            <ta e="T529" id="Seg_8734" s="T528">TURK:disc</ta>
            <ta e="T537" id="Seg_8735" s="T536">TURK:disc</ta>
            <ta e="T539" id="Seg_8736" s="T538">%TURK:core</ta>
            <ta e="T540" id="Seg_8737" s="T539">RUS:gram</ta>
            <ta e="T546" id="Seg_8738" s="T545">TURK:disc</ta>
            <ta e="T561" id="Seg_8739" s="T560">TURK:disc</ta>
            <ta e="T565" id="Seg_8740" s="T564">TURK:disc</ta>
            <ta e="T566" id="Seg_8741" s="T565">RUS:cult</ta>
            <ta e="T569" id="Seg_8742" s="T568">TURK:disc</ta>
            <ta e="T571" id="Seg_8743" s="T570">TURK:disc</ta>
            <ta e="T577" id="Seg_8744" s="T576">RUS:gram</ta>
            <ta e="T580" id="Seg_8745" s="T579">RUS:cult </ta>
            <ta e="T581" id="Seg_8746" s="T580">RUS:gram</ta>
            <ta e="T583" id="Seg_8747" s="T582">RUS:cult </ta>
            <ta e="T585" id="Seg_8748" s="T584">RUS:cult </ta>
            <ta e="T586" id="Seg_8749" s="T585">RUS:gram</ta>
            <ta e="T587" id="Seg_8750" s="T586">RUS:cult </ta>
            <ta e="T607" id="Seg_8751" s="T606">RUS:gram</ta>
            <ta e="T621" id="Seg_8752" s="T620">RUS:core</ta>
            <ta e="T623" id="Seg_8753" s="T622">RUS:cult</ta>
            <ta e="T626" id="Seg_8754" s="T625">RUS:cult</ta>
            <ta e="T629" id="Seg_8755" s="T628">RUS:cult</ta>
            <ta e="T631" id="Seg_8756" s="T630">RUS:cult</ta>
            <ta e="T644" id="Seg_8757" s="T643">RUS:mod</ta>
            <ta e="T647" id="Seg_8758" s="T646">RUS:gram</ta>
            <ta e="T648" id="Seg_8759" s="T647">RUS:gram</ta>
            <ta e="T675" id="Seg_8760" s="T674">RUS:gram</ta>
            <ta e="T688" id="Seg_8761" s="T687">RUS:gram</ta>
            <ta e="T693" id="Seg_8762" s="T692">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T84" id="Seg_8763" s="T83">RUS:int</ta>
            <ta e="T125" id="Seg_8764" s="T124">RUS: ext</ta>
            <ta e="T129" id="Seg_8765" s="T127">RUS:int</ta>
            <ta e="T151" id="Seg_8766" s="T150">RUS:int</ta>
            <ta e="T156" id="Seg_8767" s="T155">RUS:int</ta>
            <ta e="T226" id="Seg_8768" s="T223">RUS:int</ta>
            <ta e="T299" id="Seg_8769" s="T298">RUS:int</ta>
            <ta e="T317" id="Seg_8770" s="T315">RUS:int</ta>
            <ta e="T327" id="Seg_8771" s="T323">RUS:ext</ta>
            <ta e="T521" id="Seg_8772" s="T520">RUS:int</ta>
            <ta e="T524" id="Seg_8773" s="T523">RUS:int</ta>
            <ta e="T527" id="Seg_8774" s="T526">RUS:int</ta>
            <ta e="T530" id="Seg_8775" s="T529">RUS:int</ta>
            <ta e="T603" id="Seg_8776" s="T602">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T12" id="Seg_8777" s="T5">Смотри, грибы, видишь, стоят [=растут] там!</ta>
            <ta e="T14" id="Seg_8778" s="T12">Иди туда!</ta>
            <ta e="T18" id="Seg_8779" s="T14">А я сюда пойду.</ta>
            <ta e="T22" id="Seg_8780" s="T18">Надо много грибов собрать!</ta>
            <ta e="T31" id="Seg_8781" s="T26">Рядом с деревьями смотри хорошенько!</ta>
            <ta e="T33" id="Seg_8782" s="T31">Не лови!</ta>
            <ta e="T37" id="Seg_8783" s="T33">Зачем схватил, отпусти!</ta>
            <ta e="T46" id="Seg_8784" s="T37">Эти люди пришли к нам, сами начальника поставили.</ta>
            <ta e="T55" id="Seg_8785" s="T46">Они умеют на бумаге писать, а мы не умеем.</ta>
            <ta e="T61" id="Seg_8786" s="T55">Поэтому они (поставили?) своего начальника.</ta>
            <ta e="T67" id="Seg_8787" s="T61">Теперь мы говорим на русском языке.</ta>
            <ta e="T70" id="Seg_8788" s="T67">Мы учим русский язык.</ta>
            <ta e="T73" id="Seg_8789" s="T70">Как тебя зовут?</ta>
            <ta e="T76" id="Seg_8790" s="T73">И как величают [= как твое отчество]?</ta>
            <ta e="T79" id="Seg_8791" s="T76">Меня зовут Клавдия.</ta>
            <ta e="T81" id="Seg_8792" s="T79">А…</ta>
            <ta e="T84" id="Seg_8793" s="T81">А величают Захаровна.</ta>
            <ta e="T87" id="Seg_8794" s="T84">Как ты сюда приехал?</ta>
            <ta e="T90" id="Seg_8795" s="T87">Это мой начальник.</ta>
            <ta e="T95" id="Seg_8796" s="T90">А это начальник, чтобы говорить. [?]</ta>
            <ta e="T99" id="Seg_8797" s="T95">А я их учу.</ta>
            <ta e="T103" id="Seg_8798" s="T99">Куда твой муж ушел?</ta>
            <ta e="T105" id="Seg_8799" s="T103">В Агинское.</ta>
            <ta e="T110" id="Seg_8800" s="T105">Я много (?), думала [мы будем] разговаривать.</ta>
            <ta e="T113" id="Seg_8801" s="T110">Моя свинья умерла.</ta>
            <ta e="T124" id="Seg_8802" s="T113">Я землю выкопала и туда ее положила, и землей засыпала.</ta>
            <ta e="T125" id="Seg_8803" s="T124">Закрыла.</ta>
            <ta e="T130" id="Seg_8804" s="T125">У нас было старое кладбище.</ta>
            <ta e="T138" id="Seg_8805" s="T130">Там много крестов стоит [=стояло].</ta>
            <ta e="T143" id="Seg_8806" s="T138">А теперь ни одного нет.</ta>
            <ta e="T148" id="Seg_8807" s="T143">Ничего там нет.</ta>
            <ta e="T152" id="Seg_8808" s="T148">Теперь там сушилка стоит.</ta>
            <ta e="T156" id="Seg_8809" s="T152">Теперь у нас новое кладбище.</ta>
            <ta e="T161" id="Seg_8810" s="T156">Там тоже стоит много крестов.</ta>
            <ta e="T164" id="Seg_8811" s="T161">Ты, Бог…</ta>
            <ta e="T167" id="Seg_8812" s="T164">Ты, Бог, Бог.</ta>
            <ta e="T170" id="Seg_8813" s="T167">Не оставляй меня.</ta>
            <ta e="T176" id="Seg_8814" s="T170">Не оставляй меня, не бросай меня.</ta>
            <ta e="T179" id="Seg_8815" s="T176">Ты (дай?) мне.</ta>
            <ta e="T182" id="Seg_8816" s="T179">Сердце умное.</ta>
            <ta e="T187" id="Seg_8817" s="T182">Сердце умное ты пошли мне.</ta>
            <ta e="T192" id="Seg_8818" s="T187">Мой сердце глупое.</ta>
            <ta e="T195" id="Seg_8819" s="T192">Мое сердце глупое.</ta>
            <ta e="T197" id="Seg_8820" s="T195">Научи меня.</ta>
            <ta e="T199" id="Seg_8821" s="T197">Восхвалять тебя. [?]</ta>
            <ta e="T204" id="Seg_8822" s="T199">Научи меня восхвалять тебя.</ta>
            <ta e="T206" id="Seg_8823" s="T204">Научи меня.</ta>
            <ta e="T209" id="Seg_8824" s="T206">Твоей дорогой идти.</ta>
            <ta e="T217" id="Seg_8825" s="T209">Мой брат там сам научился писать.</ta>
            <ta e="T227" id="Seg_8826" s="T217">К кому-нибудь идет и еще куда-нибудь пойдет.</ta>
            <ta e="T234" id="Seg_8827" s="T227">Ему силу дать, а у него силу взять.</ta>
            <ta e="T238" id="Seg_8828" s="T234">Надо мне сеть вязать.</ta>
            <ta e="T244" id="Seg_8829" s="T238">Рыба…</ta>
            <ta e="T246" id="Seg_8830" s="T244">Рыбу ловить.</ta>
            <ta e="T252" id="Seg_8831" s="T247">Камасинцы на салике плывут.</ta>
            <ta e="T259" id="Seg_8832" s="T252">И [они] сеть забрасывают и рыбу ловят.</ta>
            <ta e="T266" id="Seg_8833" s="T259">Камни в бересту зашивают и к сети привязывают.</ta>
            <ta e="T276" id="Seg_8834" s="T266">Этому человеку все дает бог или кто, не знаю.</ta>
            <ta e="T283" id="Seg_8835" s="T276">Я прыгнула [=провалилась?] в снег, до половины ушла [в снег].</ta>
            <ta e="T285" id="Seg_8836" s="T283">Я иду</ta>
            <ta e="T290" id="Seg_8837" s="T285">Снего очень много лежит.</ta>
            <ta e="T292" id="Seg_8838" s="T290">Я шла.</ta>
            <ta e="T297" id="Seg_8839" s="T292">И провалилась туда до пояса.</ta>
            <ta e="T301" id="Seg_8840" s="T297">Я шла (по?) болоту.</ta>
            <ta e="T307" id="Seg_8841" s="T301">Там очень грязи много.</ta>
            <ta e="T317" id="Seg_8842" s="T307">Я стояла [=встала?] и туда провалилась, до рук.</ta>
            <ta e="T323" id="Seg_8843" s="T317">Потом я уцепилась за кочку и вылезла.</ta>
            <ta e="T335" id="Seg_8844" s="T327">Тех лошадей покормили, а этих лошадей не покормили.</ta>
            <ta e="T341" id="Seg_8845" s="T335">Ты дали овцам травы, они поели.</ta>
            <ta e="T346" id="Seg_8846" s="T341">Ты дал коровам травы, они поели.</ta>
            <ta e="T350" id="Seg_8847" s="T346">Теленку, видно, не дал.</ta>
            <ta e="T351" id="Seg_8848" s="T350">Я дал!</ta>
            <ta e="T353" id="Seg_8849" s="T351">Все поели.</ta>
            <ta e="T357" id="Seg_8850" s="T353">[Ты был дома] с ребенком.</ta>
            <ta e="T360" id="Seg_8851" s="T357">Ты покормил ли его?</ta>
            <ta e="T361" id="Seg_8852" s="T360">Покормил!</ta>
            <ta e="T367" id="Seg_8853" s="T361">И молока [ему] дал, и воды он попил.</ta>
            <ta e="T371" id="Seg_8854" s="T367">Правда поел ребенок.</ta>
            <ta e="T373" id="Seg_8855" s="T371">Он(а) не ест.</ta>
            <ta e="T375" id="Seg_8856" s="T373">Ты не врешь.</ta>
            <ta e="T377" id="Seg_8857" s="T375">Ты правду сказал.</ta>
            <ta e="T381" id="Seg_8858" s="T377">Они дрова топором (?).</ta>
            <ta e="T383" id="Seg_8859" s="T381">Топором сделали [=порубили].</ta>
            <ta e="T387" id="Seg_8860" s="T383">И потом к реке пошли.</ta>
            <ta e="T391" id="Seg_8861" s="T387">И рыбу там ловили.</ta>
            <ta e="T397" id="Seg_8862" s="T391">(…) привязали друг к другу.</ta>
            <ta e="T400" id="Seg_8863" s="T397">И пошли к реке.</ta>
            <ta e="T404" id="Seg_8864" s="T400">Вымой лицо водой!</ta>
            <ta e="T408" id="Seg_8865" s="T404">И вытри, станешь белым [=чистым].</ta>
            <ta e="T410" id="Seg_8866" s="T408">Красивым станешь.</ta>
            <ta e="T415" id="Seg_8867" s="T410">Куда ваши люди ушли?</ta>
            <ta e="T417" id="Seg_8868" s="T415">В (Рыбное?) поехали.</ta>
            <ta e="T422" id="Seg_8869" s="T417">Много [вещей] в мешке.</ta>
            <ta e="T429" id="Seg_8870" s="T422">Соболей связал веревкой и принес домой.</ta>
            <ta e="T437" id="Seg_8871" s="T429">Этот человек работает, работает, а у него ничего нет.</ta>
            <ta e="T447" id="Seg_8872" s="T437">А этот человек не очень работает, а все есть.</ta>
            <ta e="T451" id="Seg_8873" s="T447">Не лежи.</ta>
            <ta e="T456" id="Seg_8874" s="T451">Вставай и работай, что лежать.</ta>
            <ta e="T459" id="Seg_8875" s="T456">Много лет прошло.</ta>
            <ta e="T467" id="Seg_8876" s="T459">Красные пришли, белые пришли, а люди очень боятся.</ta>
            <ta e="T472" id="Seg_8877" s="T467">Все убежали в тайгу.</ta>
            <ta e="T474" id="Seg_8878" s="T472">Очень испугались.</ta>
            <ta e="T482" id="Seg_8879" s="T474">А когда они придут [=пришли] домой, те пришли.</ta>
            <ta e="T487" id="Seg_8880" s="T482">Они пришли, белые.</ta>
            <ta e="T493" id="Seg_8881" s="T487">Наш народ красных жалел.</ta>
            <ta e="T498" id="Seg_8882" s="T493">Хлеб им давали.</ta>
            <ta e="T505" id="Seg_8883" s="T498">Что есть, все давали: соль, котлы давали.</ta>
            <ta e="T509" id="Seg_8884" s="T505">Еще картошку давали.</ta>
            <ta e="T513" id="Seg_8885" s="T509">Красные жили в лесу.</ta>
            <ta e="T518" id="Seg_8886" s="T513">А вечером приходили сюда есть.</ta>
            <ta e="T525" id="Seg_8887" s="T518">Один раз пришли два отряда красных.</ta>
            <ta e="T532" id="Seg_8888" s="T525">Из ружья друг друга чуть не убили.</ta>
            <ta e="T535" id="Seg_8889" s="T532">Женщина к нам пришла.</ta>
            <ta e="T543" id="Seg_8890" s="T535">"Вы тут разговариваете, а человек стоит [около] окна.</ta>
            <ta e="T544" id="Seg_8891" s="T543">Слушает".</ta>
            <ta e="T549" id="Seg_8892" s="T544">Мы…</ta>
            <ta e="T554" id="Seg_8893" s="T549">Однажды ночью приехали красные.</ta>
            <ta e="T556" id="Seg_8894" s="T554">Человека привезли.</ta>
            <ta e="T559" id="Seg_8895" s="T556">[Его] убили около двери.</ta>
            <ta e="T563" id="Seg_8896" s="T559">Потом мужчин собрали.</ta>
            <ta e="T566" id="Seg_8897" s="T563">Били плетками.</ta>
            <ta e="T572" id="Seg_8898" s="T566">Один человек в штаны сходил.</ta>
            <ta e="T576" id="Seg_8899" s="T572">Моего отца звали Захар.</ta>
            <ta e="T580" id="Seg_8900" s="T576">А величали Степанович.</ta>
            <ta e="T585" id="Seg_8901" s="T580">А мать моя Афанасьевна, величали Антоновна.</ta>
            <ta e="T590" id="Seg_8902" s="T585">А фамилия их была Анджигатовы.</ta>
            <ta e="T603" id="Seg_8903" s="T590">Детей у них было один, два, три, четыре, пять, семь, восемь… девять.</ta>
            <ta e="T606" id="Seg_8904" s="T603">Старшую звали Лена.</ta>
            <ta e="T611" id="Seg_8905" s="T606">А сына ее звали Дёма.</ta>
            <ta e="T618" id="Seg_8906" s="T611">Потом меня звали Клавдея.</ta>
            <ta e="T623" id="Seg_8907" s="T618">Мою сестру звали Надя.</ta>
            <ta e="T626" id="Seg_8908" s="T623">Потом была Фрося.</ta>
            <ta e="T629" id="Seg_8909" s="T626">Потом была Вера.</ta>
            <ta e="T637" id="Seg_8910" s="T629">Потом была Маня, потом появился сын…</ta>
            <ta e="T639" id="Seg_8911" s="T637">Его звали Максим.</ta>
            <ta e="T646" id="Seg_8912" s="T639">Один сын умер, меня еще не было.</ta>
            <ta e="T650" id="Seg_8913" s="T646">А (/и?) дочь умерла.</ta>
            <ta e="T657" id="Seg_8914" s="T650">Один, два, три, четыре, пять, шесть, семь.</ta>
            <ta e="T658" id="Seg_8915" s="T657">Восемь.</ta>
            <ta e="T666" id="Seg_8916" s="T658">Девять, десять, двенадцать, одиннадцать, тринадцать.</ta>
            <ta e="T669" id="Seg_8917" s="T666">Старшая дочь умерла.</ta>
            <ta e="T674" id="Seg_8918" s="T669">Ей было тринадцать лет.</ta>
            <ta e="T682" id="Seg_8919" s="T674">А Фросю убил… ее муж убил ее.</ta>
            <ta e="T687" id="Seg_8920" s="T682">Надя умерла, пять месяцев прошло.</ta>
            <ta e="T691" id="Seg_8921" s="T687">А мы вдвоем остались.</ta>
            <ta e="T694" id="Seg_8922" s="T691">Клавдия и Максим.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T12" id="Seg_8923" s="T5">Look, [there are] mushrooms, you see, they are standing there!</ta>
            <ta e="T14" id="Seg_8924" s="T12">Go there!</ta>
            <ta e="T18" id="Seg_8925" s="T14">And I will go here.</ta>
            <ta e="T22" id="Seg_8926" s="T18">[We] must gather a lot of mushrooms!</ta>
            <ta e="T31" id="Seg_8927" s="T26">Look on next to the trees, [look] well!</ta>
            <ta e="T33" id="Seg_8928" s="T31">Don’t grasp [it]! (?)</ta>
            <ta e="T37" id="Seg_8929" s="T33">Why did you grasp it, let it/them go!</ta>
            <ta e="T46" id="Seg_8930" s="T37">These people came to us, they set their own chief.</ta>
            <ta e="T55" id="Seg_8931" s="T46">They can write on paper, and we cannot.</ta>
            <ta e="T61" id="Seg_8932" s="T55">Then [=because of this] they stood [= set?] their own chief.</ta>
            <ta e="T67" id="Seg_8933" s="T61">Now we speak the Russian language.</ta>
            <ta e="T70" id="Seg_8934" s="T67">We learn the Russian language</ta>
            <ta e="T73" id="Seg_8935" s="T70">What is your name?</ta>
            <ta e="T76" id="Seg_8936" s="T73">And how is patronyme?</ta>
            <ta e="T79" id="Seg_8937" s="T76">My name is Klavdiya.</ta>
            <ta e="T81" id="Seg_8938" s="T79">And…</ta>
            <ta e="T84" id="Seg_8939" s="T81">And my [patro]nyme is Zakharovna.</ta>
            <ta e="T87" id="Seg_8940" s="T84">How did you come here?</ta>
            <ta e="T90" id="Seg_8941" s="T87">That is my chief.</ta>
            <ta e="T95" id="Seg_8942" s="T90">And this is the boss to speak to. [?]</ta>
            <ta e="T99" id="Seg_8943" s="T95">And I teach them.</ta>
            <ta e="T103" id="Seg_8944" s="T99">Where did your husband go?</ta>
            <ta e="T105" id="Seg_8945" s="T103">To Aginskoye.</ta>
            <ta e="T110" id="Seg_8946" s="T105">I (?) a lot, I thought [we're going] to speak.</ta>
            <ta e="T113" id="Seg_8947" s="T110">My pig died.</ta>
            <ta e="T124" id="Seg_8948" s="T113">I dug the ground and I put it there and covered with soil.</ta>
            <ta e="T125" id="Seg_8949" s="T124">I covered [it].</ta>
            <ta e="T130" id="Seg_8950" s="T125">We had the old cemetery.</ta>
            <ta e="T138" id="Seg_8951" s="T130">There are [=were] a lot of crosses standing.</ta>
            <ta e="T143" id="Seg_8952" s="T138">And now there is not a single [cross].</ta>
            <ta e="T148" id="Seg_8953" s="T143">There is nothing there.</ta>
            <ta e="T152" id="Seg_8954" s="T148">Now there is a dryer.</ta>
            <ta e="T156" id="Seg_8955" s="T152">Now we [have] a new cemetery.</ta>
            <ta e="T161" id="Seg_8956" s="T156">There are a lot of crosses standing too.</ta>
            <ta e="T164" id="Seg_8957" s="T161">You, God…</ta>
            <ta e="T167" id="Seg_8958" s="T164">You, God, God.</ta>
            <ta e="T170" id="Seg_8959" s="T167">Don't leave me.</ta>
            <ta e="T176" id="Seg_8960" s="T170">Don't leave me, don’t throw [= abandon] me.</ta>
            <ta e="T179" id="Seg_8961" s="T176">You (give?) me.</ta>
            <ta e="T182" id="Seg_8962" s="T179">A smart heart.</ta>
            <ta e="T187" id="Seg_8963" s="T182">The smart heart, send it to me.</ta>
            <ta e="T192" id="Seg_8964" s="T187">My heart is foolish.</ta>
            <ta e="T195" id="Seg_8965" s="T192">My heart is foolish.</ta>
            <ta e="T197" id="Seg_8966" s="T195">Teach me.</ta>
            <ta e="T199" id="Seg_8967" s="T197">To praise you. [?]</ta>
            <ta e="T204" id="Seg_8968" s="T199">Teach me to praise.</ta>
            <ta e="T206" id="Seg_8969" s="T204">Teach me.</ta>
            <ta e="T209" id="Seg_8970" s="T206">To go your way.</ta>
            <ta e="T217" id="Seg_8971" s="T209">My brother here learnt himself to write.</ta>
            <ta e="T227" id="Seg_8972" s="T217">He goes to someone and and will go somewhere else (?_.</ta>
            <ta e="T234" id="Seg_8973" s="T227">To give one power, and to take power from another.</ta>
            <ta e="T238" id="Seg_8974" s="T234">I have to weave a net.</ta>
            <ta e="T244" id="Seg_8975" s="T238">Fish…</ta>
            <ta e="T246" id="Seg_8976" s="T244">To catch fish.</ta>
            <ta e="T252" id="Seg_8977" s="T247">They, the Kamas are going on the raft.</ta>
            <ta e="T259" id="Seg_8978" s="T252">And [they] are throwing the net and catching fish.</ta>
            <ta e="T266" id="Seg_8979" s="T259">They are sewing stones into birchbark and tying to the net.</ta>
            <ta e="T276" id="Seg_8980" s="T266">God or who, I do not know, brings everything to this person.</ta>
            <ta e="T283" id="Seg_8981" s="T276">I jumped [=sank?] into the snow, I was half in the snow.</ta>
            <ta e="T285" id="Seg_8982" s="T283">I'm coming.</ta>
            <ta e="T290" id="Seg_8983" s="T285">There is a lot of snow (lying).</ta>
            <ta e="T292" id="Seg_8984" s="T290">I was going.</ta>
            <ta e="T297" id="Seg_8985" s="T292">And I sank hip-deep there.</ta>
            <ta e="T301" id="Seg_8986" s="T297">I was walking on (?) a swamp.</ta>
            <ta e="T307" id="Seg_8987" s="T301">There is very dirty.</ta>
            <ta e="T317" id="Seg_8988" s="T307">I stood and sank there, up to my hands.</ta>
            <ta e="T323" id="Seg_8989" s="T317">Then I caught a turf with [my] hand and came back.</ta>
            <ta e="T327" id="Seg_8990" s="T323">Well, like I told you.</ta>
            <ta e="T335" id="Seg_8991" s="T327">Those horses have been feed, and these horse have not been feed.</ta>
            <ta e="T341" id="Seg_8992" s="T335">You gave the sheep grass, they ate.</ta>
            <ta e="T346" id="Seg_8993" s="T341">You gave the cow grass, it ate.</ta>
            <ta e="T350" id="Seg_8994" s="T346">Perhaps you did not give to the calf.</ta>
            <ta e="T351" id="Seg_8995" s="T350">I did give!</ta>
            <ta e="T353" id="Seg_8996" s="T351">Everyone ate.</ta>
            <ta e="T357" id="Seg_8997" s="T353">[You were at home] with the child.</ta>
            <ta e="T360" id="Seg_8998" s="T357">Did you feed him?</ta>
            <ta e="T361" id="Seg_8999" s="T360">I did!</ta>
            <ta e="T367" id="Seg_9000" s="T361">And I gave milk and it drank water.</ta>
            <ta e="T371" id="Seg_9001" s="T367">The child really ate.</ta>
            <ta e="T373" id="Seg_9002" s="T371">S/he does not eat.</ta>
            <ta e="T375" id="Seg_9003" s="T373">You do not lie.</ta>
            <ta e="T377" id="Seg_9004" s="T375">You said the truth.</ta>
            <ta e="T381" id="Seg_9005" s="T377">They (?) wood with an axe.</ta>
            <ta e="T383" id="Seg_9006" s="T381">They made with an axe.</ta>
            <ta e="T387" id="Seg_9007" s="T383">And then they went to the river.</ta>
            <ta e="T391" id="Seg_9008" s="T387">And they caught fish there.</ta>
            <ta e="T397" id="Seg_9009" s="T391">They tied (…) to each other.</ta>
            <ta e="T400" id="Seg_9010" s="T397">And they went to the river.</ta>
            <ta e="T404" id="Seg_9011" s="T400">Wash his/her face with water!</ta>
            <ta e="T408" id="Seg_9012" s="T404">And rub it, you will become white [=clean]</ta>
            <ta e="T410" id="Seg_9013" s="T408">You will become beautiful.</ta>
            <ta e="T415" id="Seg_9014" s="T410">Where did your people go?</ta>
            <ta e="T417" id="Seg_9015" s="T415">They went to (Rybnoe?).</ta>
            <ta e="T422" id="Seg_9016" s="T417">There is many [things] in the bag.</ta>
            <ta e="T429" id="Seg_9017" s="T422">He tied sables with ropes and brought [them] home.</ta>
            <ta e="T437" id="Seg_9018" s="T429">This man works and works, but [he] has nothing.</ta>
            <ta e="T447" id="Seg_9019" s="T437">And that man doesn't work so much, but [he] has everything.</ta>
            <ta e="T451" id="Seg_9020" s="T447">Don't lie.</ta>
            <ta e="T456" id="Seg_9021" s="T451">Get up and work, why are [you] lying.</ta>
            <ta e="T459" id="Seg_9022" s="T456">Many years passed.</ta>
            <ta e="T467" id="Seg_9023" s="T459">The red came, the white came, and people [here] was very scared.</ta>
            <ta e="T472" id="Seg_9024" s="T467">Everybody escaped to the forest.</ta>
            <ta e="T474" id="Seg_9025" s="T472">They were very scared.</ta>
            <ta e="T482" id="Seg_9026" s="T474">And as they came home, they came.</ta>
            <ta e="T487" id="Seg_9027" s="T482">They came, the white.</ta>
            <ta e="T493" id="Seg_9028" s="T487">Our people pitied the red.</ta>
            <ta e="T498" id="Seg_9029" s="T493">They gave them bread.</ta>
            <ta e="T505" id="Seg_9030" s="T498">They gave them all they had: the gave salt, cauldron[s].</ta>
            <ta e="T509" id="Seg_9031" s="T505">They gave potatoes, too.</ta>
            <ta e="T513" id="Seg_9032" s="T509">The red lived in the forest.</ta>
            <ta e="T518" id="Seg_9033" s="T513">And in the evening, they came here to eat.</ta>
            <ta e="T525" id="Seg_9034" s="T518">Once two detachments of red came here.</ta>
            <ta e="T532" id="Seg_9035" s="T525">Nearly killed one another with guns.</ta>
            <ta e="T535" id="Seg_9036" s="T532">A woman comes to us.</ta>
            <ta e="T543" id="Seg_9037" s="T535">"You are speaking here, and someone is standing [under] the window.</ta>
            <ta e="T544" id="Seg_9038" s="T543">Listens."</ta>
            <ta e="T549" id="Seg_9039" s="T544">We…</ta>
            <ta e="T554" id="Seg_9040" s="T549">One night the red came.</ta>
            <ta e="T556" id="Seg_9041" s="T554">They brought a man.</ta>
            <ta e="T559" id="Seg_9042" s="T556">They killed [him] near the door.</ta>
            <ta e="T563" id="Seg_9043" s="T559">Then they [ordered] the men to gather.</ta>
            <ta e="T566" id="Seg_9044" s="T563">They beat them with whips.</ta>
            <ta e="T572" id="Seg_9045" s="T566">One man defecated in his pants.</ta>
            <ta e="T576" id="Seg_9046" s="T572">My father's name was Zakhar.</ta>
            <ta e="T580" id="Seg_9047" s="T576">And his patronyme was Stepanovich.</ta>
            <ta e="T585" id="Seg_9048" s="T580">And my mother [was] Afanasia, [her] patronyme was Antonovna.</ta>
            <ta e="T590" id="Seg_9049" s="T585">And their family name was Andzhigatovs.</ta>
            <ta e="T603" id="Seg_9050" s="T590">The children they had one, two, three, four, five, seven, eight, nine… eight.</ta>
            <ta e="T606" id="Seg_9051" s="T603">The elder [daughter]'s name was Lena.</ta>
            <ta e="T611" id="Seg_9052" s="T606">And the son's name was Dyoma.</ta>
            <ta e="T618" id="Seg_9053" s="T611">Then my name was Klavdiya.</ta>
            <ta e="T623" id="Seg_9054" s="T618">My sister's name was Nadya.</ta>
            <ta e="T626" id="Seg_9055" s="T623">Then was Frosya.</ta>
            <ta e="T629" id="Seg_9056" s="T626">Then was Vera.</ta>
            <ta e="T637" id="Seg_9057" s="T629">Then was Manya, then came a son…</ta>
            <ta e="T639" id="Seg_9058" s="T637">His name was Maksim.</ta>
            <ta e="T646" id="Seg_9059" s="T639">One son died, I wasn't yet [born].</ta>
            <ta e="T650" id="Seg_9060" s="T646">And a daughter died.</ta>
            <ta e="T657" id="Seg_9061" s="T650">One, two, three, four, five, six, seven.</ta>
            <ta e="T658" id="Seg_9062" s="T657">Eight.</ta>
            <ta e="T666" id="Seg_9063" s="T658">Nine, ten, twelve, eleven, thirteen.</ta>
            <ta e="T669" id="Seg_9064" s="T666">The elder daughter died.</ta>
            <ta e="T674" id="Seg_9065" s="T669">She was thirteen years old.</ta>
            <ta e="T682" id="Seg_9066" s="T674">And Frosya was killed by her husband.</ta>
            <ta e="T687" id="Seg_9067" s="T682">Nadya died, five months have passed [since].</ta>
            <ta e="T691" id="Seg_9068" s="T687">And we two are left.</ta>
            <ta e="T694" id="Seg_9069" s="T691">Klavdiya and Maksim.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T12" id="Seg_9070" s="T5">Schau, [dort sind] Pilze, du siehst, sie stehen dort!</ta>
            <ta e="T14" id="Seg_9071" s="T12">Geh dorthin!</ta>
            <ta e="T18" id="Seg_9072" s="T14">Und ich gehe hierhin.</ta>
            <ta e="T22" id="Seg_9073" s="T18">[Wir] müssen viele Pilze sammeln!</ta>
            <ta e="T31" id="Seg_9074" s="T26">Schau neben den Bäumen, [schau] gut!</ta>
            <ta e="T33" id="Seg_9075" s="T31">Fasse [ihn] nicht an! (?)</ta>
            <ta e="T37" id="Seg_9076" s="T33">Warum hast du ihn angefasst, lass ihn/sie los!</ta>
            <ta e="T46" id="Seg_9077" s="T37">Diese Leute kamen zu uns, sie installierten ihren eigenen Chef.</ta>
            <ta e="T55" id="Seg_9078" s="T46">Sie können auf Papier schreiben und wir können nicht.</ta>
            <ta e="T61" id="Seg_9079" s="T55">Dann [=deshalb] standen [= installierten?] sie ihre eigenen Chefs.</ta>
            <ta e="T67" id="Seg_9080" s="T61">Jetzt sprechen wir Russisch.</ta>
            <ta e="T70" id="Seg_9081" s="T67">Wir lernen Russisch.</ta>
            <ta e="T73" id="Seg_9082" s="T70">Wie heißt du?</ta>
            <ta e="T76" id="Seg_9083" s="T73">Und wie ist dein Vatersname?</ta>
            <ta e="T79" id="Seg_9084" s="T76">Mein Name ist Klavdija.</ta>
            <ta e="T81" id="Seg_9085" s="T79">Und…</ta>
            <ta e="T84" id="Seg_9086" s="T81">Und mein [Vaters]name ist Sacharowna.</ta>
            <ta e="T87" id="Seg_9087" s="T84">Wie bist du hierher gekommen?</ta>
            <ta e="T90" id="Seg_9088" s="T87">Das ist mein Chef.</ta>
            <ta e="T95" id="Seg_9089" s="T90">Und das ist der Chef, zu dem man spricht. [?]</ta>
            <ta e="T99" id="Seg_9090" s="T95">Und ich lehre sie.</ta>
            <ta e="T103" id="Seg_9091" s="T99">Wohin ist dein Mann gegangen?</ta>
            <ta e="T105" id="Seg_9092" s="T103">Nach Aginskoje.</ta>
            <ta e="T110" id="Seg_9093" s="T105">Ich (?) viel, ich dachte [wir] sprechen [werden].</ta>
            <ta e="T113" id="Seg_9094" s="T110">Mein Schwein ist gestorben.</ta>
            <ta e="T124" id="Seg_9095" s="T113">Ich habe die Erde aufgegraben, es dort hingelegt und mit Erde bedeckt.</ta>
            <ta e="T125" id="Seg_9096" s="T124">Ich habe [es] zugedeckt.</ta>
            <ta e="T130" id="Seg_9097" s="T125">Wir hatten den alten Friedhof.</ta>
            <ta e="T138" id="Seg_9098" s="T130">Dort stehen [=standen] viele Kreuze.</ta>
            <ta e="T143" id="Seg_9099" s="T138">Und jetzt ist da nicht ein einziges [Kreuz].</ta>
            <ta e="T148" id="Seg_9100" s="T143">Nichts ist dort.</ta>
            <ta e="T152" id="Seg_9101" s="T148">Jetzt steht dort eine Darre.</ta>
            <ta e="T156" id="Seg_9102" s="T152">Jetzt [haben] wir einen neuen Friedhof.</ta>
            <ta e="T161" id="Seg_9103" s="T156">Dort stehen auch viele Kreuze.</ta>
            <ta e="T164" id="Seg_9104" s="T161">Du, Gott…</ta>
            <ta e="T167" id="Seg_9105" s="T164">Du, Gott, Gott.</ta>
            <ta e="T170" id="Seg_9106" s="T167">Verlass mich nicht.</ta>
            <ta e="T176" id="Seg_9107" s="T170">Verlass mich nicht, wirf [= verlass] mich nicht.</ta>
            <ta e="T179" id="Seg_9108" s="T176">Du (gib?) mir.</ta>
            <ta e="T182" id="Seg_9109" s="T179">Ein kluges Herz.</ta>
            <ta e="T187" id="Seg_9110" s="T182">Das kluge Herz, schick es mir.</ta>
            <ta e="T192" id="Seg_9111" s="T187">Mein Herz ist dumm.</ta>
            <ta e="T195" id="Seg_9112" s="T192">Mein Herz ist dumm.</ta>
            <ta e="T197" id="Seg_9113" s="T195">Lehre mich.</ta>
            <ta e="T199" id="Seg_9114" s="T197">Um dich zu loben. [?]</ta>
            <ta e="T204" id="Seg_9115" s="T199">Lehre mich [dich] zu loben.</ta>
            <ta e="T206" id="Seg_9116" s="T204">Lehre mich.</ta>
            <ta e="T209" id="Seg_9117" s="T206">Deinen Weg zu gehen.</ta>
            <ta e="T217" id="Seg_9118" s="T209">Mein Bruder hier lernte selbst schreiben.</ta>
            <ta e="T227" id="Seg_9119" s="T217">Er geht zu irgenwem und wird irgendwo anders hingehen. [?]</ta>
            <ta e="T234" id="Seg_9120" s="T227">Einem die Kraft zu geben, und dem anderen die Kraft zu nehmen.</ta>
            <ta e="T238" id="Seg_9121" s="T234">Ich muss mein Netz knüpfen.</ta>
            <ta e="T244" id="Seg_9122" s="T238">Fisch…</ta>
            <ta e="T246" id="Seg_9123" s="T244">Um Fisch zu fangen.</ta>
            <ta e="T252" id="Seg_9124" s="T247">Sie, die Kamassen fahren mit dem Floß.</ta>
            <ta e="T259" id="Seg_9125" s="T252">Und [sie] werfen das Netz und fangen Fisch.</ta>
            <ta e="T266" id="Seg_9126" s="T259">Sie nähen Steine in Birkenrinde und binden es ans Netz.</ta>
            <ta e="T276" id="Seg_9127" s="T266">Gott oder wer, ich weiß es nicht, bringt diesem Menschen alles.</ta>
            <ta e="T283" id="Seg_9128" s="T276">Ich sprang [sankte?] in den Schnee, ich war halb im Schnee [versunken].</ta>
            <ta e="T285" id="Seg_9129" s="T283">Ich komme.</ta>
            <ta e="T290" id="Seg_9130" s="T285">Es [liegt] viel Schnee.</ta>
            <ta e="T292" id="Seg_9131" s="T290">Ich ging.</ta>
            <ta e="T297" id="Seg_9132" s="T292">Und ich bin dorthin hüfttief gesunken.</ta>
            <ta e="T301" id="Seg_9133" s="T297">Ich ging durch (?) einen Sumpf.</ta>
            <ta e="T307" id="Seg_9134" s="T301">Dort ist es sehr schlammig.</ta>
            <ta e="T317" id="Seg_9135" s="T307">Ich stand und sank hin dort, bis zu meinen Händen.</ta>
            <ta e="T323" id="Seg_9136" s="T317">Dann griff ich eine Grassode mit [meiner] Hand und kam zurück.</ta>
            <ta e="T327" id="Seg_9137" s="T323">Nun, ich habe [es] gesagt.</ta>
            <ta e="T335" id="Seg_9138" s="T327">Jene Pferde wurden gefüttert, und diese Pferde wurden nicht gefüttert.</ta>
            <ta e="T341" id="Seg_9139" s="T335">Du gabst den Schafen Gras, sie fraßen.</ta>
            <ta e="T346" id="Seg_9140" s="T341">Du gabst der Kuh Gras, sie fraß.</ta>
            <ta e="T350" id="Seg_9141" s="T346">Vielleicht hast du dem Kalb nichts gegeben.</ta>
            <ta e="T351" id="Seg_9142" s="T350">Ich habe [etwas] gegeben!</ta>
            <ta e="T353" id="Seg_9143" s="T351">Alle fraßen.</ta>
            <ta e="T357" id="Seg_9144" s="T353">[Du warst zuhause] mit dem Kind.</ta>
            <ta e="T360" id="Seg_9145" s="T357">Hast du ihm zu essen gegeben?</ta>
            <ta e="T361" id="Seg_9146" s="T360">Ich habe ihm zu essen gegeben.</ta>
            <ta e="T367" id="Seg_9147" s="T361">Und ich habe [ihm] Milch gegeben und es hat Wasser getrunken.</ta>
            <ta e="T371" id="Seg_9148" s="T367">Das Kind hat wirklich gegessen.</ta>
            <ta e="T373" id="Seg_9149" s="T371">Es isst nicht.</ta>
            <ta e="T375" id="Seg_9150" s="T373">Du lügst nicht.</ta>
            <ta e="T377" id="Seg_9151" s="T375">Du hast die Wahrheit gesagt.</ta>
            <ta e="T381" id="Seg_9152" s="T377">Sie (?) Holz mit der Axt.</ta>
            <ta e="T383" id="Seg_9153" s="T381">Sie machten mit einer Axt.</ta>
            <ta e="T387" id="Seg_9154" s="T383">Und dann gingen sie zum Fluss.</ta>
            <ta e="T391" id="Seg_9155" s="T387">Und sie fingen dort Fisch.</ta>
            <ta e="T397" id="Seg_9156" s="T391">Sie banden (…) aneinander.</ta>
            <ta e="T400" id="Seg_9157" s="T397">Und sie gingen zum Fluss.</ta>
            <ta e="T404" id="Seg_9158" s="T400">Wasch sein/ihr Gesicht mit Wasser!</ta>
            <ta e="T408" id="Seg_9159" s="T404">Und schrubbe es, du wirst weiß [=sauber] werden.</ta>
            <ta e="T410" id="Seg_9160" s="T408">Du wirst schön werden.</ta>
            <ta e="T415" id="Seg_9161" s="T410">Wohin sind eure Leute gegangen?</ta>
            <ta e="T417" id="Seg_9162" s="T415">Sie gingen nach (Rybnoe?).</ta>
            <ta e="T422" id="Seg_9163" s="T417">Dort sind viele [Sachen] in der Tasche.</ta>
            <ta e="T429" id="Seg_9164" s="T422">Er band Zobel mit Seilen fest und brachte [sie] nach Hause.</ta>
            <ta e="T437" id="Seg_9165" s="T429">Dieser Mann arbeitet und arbeitet, aber [er] hat nichts.</ta>
            <ta e="T447" id="Seg_9166" s="T437">Und dieser Mann arbeitet nicht so viel, aber [er] hat alles.</ta>
            <ta e="T451" id="Seg_9167" s="T447">Lieg nicht.</ta>
            <ta e="T456" id="Seg_9168" s="T451">Steh auf und arbeite, warum liegst [du]?</ta>
            <ta e="T459" id="Seg_9169" s="T456">Viele Jahre sind vergangen.</ta>
            <ta e="T467" id="Seg_9170" s="T459">Die Roten kamen, die Weißen kamen und die Leute [hier] waren sehr verängstigt.</ta>
            <ta e="T472" id="Seg_9171" s="T467">Alle flohen in den Wald.</ta>
            <ta e="T474" id="Seg_9172" s="T472">Sie hatten große Angst.</ta>
            <ta e="T482" id="Seg_9173" s="T474">Und als sie nach Hause kamen, kamen sie.</ta>
            <ta e="T487" id="Seg_9174" s="T482">Sie kamen, die Weißen.</ta>
            <ta e="T493" id="Seg_9175" s="T487">Unsere Leute bemitleideten die Roten.</ta>
            <ta e="T498" id="Seg_9176" s="T493">Sie gaben ihnen Brot.</ta>
            <ta e="T505" id="Seg_9177" s="T498">Sie gaben ihnen alles, was sie hatten: Sie gaben Salz, Kessel.</ta>
            <ta e="T509" id="Seg_9178" s="T505">Sie gaben auch Kartoffeln.</ta>
            <ta e="T513" id="Seg_9179" s="T509">Die Roten lebten im Wald.</ta>
            <ta e="T518" id="Seg_9180" s="T513">Und am Abend kamen sie hierher um zu essen.</ta>
            <ta e="T525" id="Seg_9181" s="T518">Einmal kamen zwei Einheiten von den Roten her.</ta>
            <ta e="T532" id="Seg_9182" s="T525">Sie töteten einander fast mit Waffen.</ta>
            <ta e="T535" id="Seg_9183" s="T532">Eine Frau kommt zu uns.</ta>
            <ta e="T543" id="Seg_9184" s="T535">"Du sprichst hier, und irgendjemand steht [am] Fenster.</ta>
            <ta e="T544" id="Seg_9185" s="T543">Hört zu."</ta>
            <ta e="T549" id="Seg_9186" s="T544">Wir…</ta>
            <ta e="T554" id="Seg_9187" s="T549">In einer Nacht kamen die Roten.</ta>
            <ta e="T556" id="Seg_9188" s="T554">Sie brachten einen Mann.</ta>
            <ta e="T559" id="Seg_9189" s="T556">Sie töteten [ihn] bei der Tür.</ta>
            <ta e="T563" id="Seg_9190" s="T559">Dann [befahlen] sie den Männern sich zu versammeln.</ta>
            <ta e="T566" id="Seg_9191" s="T563">Sie schlugen sie mit Peitschen.</ta>
            <ta e="T572" id="Seg_9192" s="T566">Ein Mann kotete in seine Hosen.</ta>
            <ta e="T576" id="Seg_9193" s="T572">Der Name meines Vaters war Sachar.</ta>
            <ta e="T580" id="Seg_9194" s="T576">Und sein Vatersname war Stepanowitsch.</ta>
            <ta e="T585" id="Seg_9195" s="T580">Und meine Mutter [war] Afanasia, [ihr] Vatersname war Antonovna.</ta>
            <ta e="T590" id="Seg_9196" s="T585">Und ihr Familienname war Andschigatovs.</ta>
            <ta e="T603" id="Seg_9197" s="T590">Die Kinder, sie hatten eins, zwei, drei, vier, fünf, sieben, acht, neun… acht.</ta>
            <ta e="T606" id="Seg_9198" s="T603">Der Name der ältesten [Tochter] war Lena.</ta>
            <ta e="T611" id="Seg_9199" s="T606">Und der Name des Sohnes war Djoma.</ta>
            <ta e="T618" id="Seg_9200" s="T611">Dann mein Name war Klavdija.</ta>
            <ta e="T623" id="Seg_9201" s="T618">Der Name meiner Schwester war Nadja.</ta>
            <ta e="T626" id="Seg_9202" s="T623">Dann war Frosja.</ta>
            <ta e="T629" id="Seg_9203" s="T626">Dann war Vera.</ta>
            <ta e="T637" id="Seg_9204" s="T629">Dann war Manja, dann kam ein Sohn…</ta>
            <ta e="T639" id="Seg_9205" s="T637">Sein Name war Maxim.</ta>
            <ta e="T646" id="Seg_9206" s="T639">Ein Sohn starb, ich war noch nicht [geboren].</ta>
            <ta e="T650" id="Seg_9207" s="T646">Und eine Tochter starb.</ta>
            <ta e="T657" id="Seg_9208" s="T650">Eins, zwei, drei, vier, fünf, sechs, sieben.</ta>
            <ta e="T658" id="Seg_9209" s="T657">Acht.</ta>
            <ta e="T666" id="Seg_9210" s="T658">Neun, zehn, zwölf, elf, dreizehn.</ta>
            <ta e="T669" id="Seg_9211" s="T666">Die ältere Tochter starb.</ta>
            <ta e="T674" id="Seg_9212" s="T669">Sie war dreizehn Jahre alt.</ta>
            <ta e="T682" id="Seg_9213" s="T674">Und Frosja wurde von ihrem Mann umgebracht.</ta>
            <ta e="T687" id="Seg_9214" s="T682">Nadja starb, fünf Monate sind [seitdem] vergangen.</ta>
            <ta e="T691" id="Seg_9215" s="T687">Und wir beide sind übrig.</ta>
            <ta e="T694" id="Seg_9216" s="T691">Klavdija und Maxim.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T33" id="Seg_9217" s="T31">[GVY:] "Don't touch the mushrooms"?</ta>
            <ta e="T67" id="Seg_9218" s="T61">[GVY:]dʼăbaktərlaʔbəʔjə… -baʔ</ta>
            <ta e="T70" id="Seg_9219" s="T67">[GVY:] Or tüšəleʔbəbaʔ?</ta>
            <ta e="T84" id="Seg_9220" s="T81">[KlT:] She probably uses ”numəj-” as ’to be called by the patronym’ here</ta>
            <ta e="T90" id="Seg_9221" s="T87">KT1:8 fragment</ta>
            <ta e="T99" id="Seg_9222" s="T95">KT1:8=end</ta>
            <ta e="T110" id="Seg_9223" s="T105">[GVY:] Or "Măna…" 'You (…) much to me'? </ta>
            <ta e="T113" id="Seg_9224" s="T110">KT1:23</ta>
            <ta e="T124" id="Seg_9225" s="T113">KT1:23-end (the last sentence is not here).</ta>
            <ta e="T164" id="Seg_9226" s="T161">KT1:11 [GVY:] This fragment is missing in the Ekaterinburg cards.</ta>
            <ta e="T179" id="Seg_9227" s="T176">[GVY:] tănuget 'teach' or smth like this? </ta>
            <ta e="T192" id="Seg_9228" s="T187">[GVY:] sagəštə = sagəššət</ta>
            <ta e="T199" id="Seg_9229" s="T197">[GVY:] maktanzərzittə = maktanərzittə by analogy with maktazən (D 37a)?</ta>
            <ta e="T209" id="Seg_9230" s="T206">KT1:11-end</ta>
            <ta e="T227" id="Seg_9231" s="T217">[GVY:] Unclear. This sentence is missing in the Ekaterinburg cards. </ta>
            <ta e="T234" id="Seg_9232" s="T227">[GVY:] Unclear</ta>
            <ta e="T238" id="Seg_9233" s="T234">KT1:4, the order of sentences is different. [GVY:] salba from Khakas salba 'bast'? In the Ekaterinburg cards: "Надо мне салбу́ вязать".</ta>
            <ta e="T246" id="Seg_9234" s="T244">[GVY:] dʼabis-… zʼittə?</ta>
            <ta e="T252" id="Seg_9235" s="T247">[GVY:] salik - a dialectal Russian word for a small raft; finally from the Turkic sal.</ta>
            <ta e="T266" id="Seg_9236" s="T259">KT1:4-end</ta>
            <ta e="T276" id="Seg_9237" s="T266">KT1:9 last sentence [GVY:] kudanə = kuzanə</ta>
            <ta e="T283" id="Seg_9238" s="T276">KT1:7 fragment [GVY:] or p'eldə [half-LAT] or pʼeldək as a separate word?</ta>
            <ta e="T323" id="Seg_9239" s="T317">KT1:7-end</ta>
            <ta e="T335" id="Seg_9240" s="T327">KT1:6</ta>
            <ta e="T353" id="Seg_9241" s="T351">[GVY:] Or 'they ate everything'.</ta>
            <ta e="T357" id="Seg_9242" s="T353">[GVY:] Damaged tape. KT1: tăn maʔnəl amnobial eššiziʔ.</ta>
            <ta e="T373" id="Seg_9243" s="T371">That means that the kid had eaten before; see KT1: 6.</ta>
            <ta e="T377" id="Seg_9244" s="T375">KT1:6-end</ta>
            <ta e="T381" id="Seg_9245" s="T377">!!!</ta>
            <ta e="T404" id="Seg_9246" s="T400">Might mean ’your face’</ta>
            <ta e="T415" id="Seg_9247" s="T410">!!!</ta>
            <ta e="T422" id="Seg_9248" s="T417">KT1:9, not all the sentences and in a different order.</ta>
            <ta e="T459" id="Seg_9249" s="T456">KT1:3</ta>
            <ta e="T493" id="Seg_9250" s="T487">[GVY:] Or "loved".</ta>
            <ta e="T509" id="Seg_9251" s="T505">[GVY:] tăltonə?</ta>
            <ta e="T532" id="Seg_9252" s="T525">[GVY:] odʼin onʼiʔdə = onʼiʔ onʼiʔdə? !!!</ta>
            <ta e="T544" id="Seg_9253" s="T543">[GVY:] [nʼel-]</ta>
            <ta e="T559" id="Seg_9254" s="T556">[GVY:] ajində?</ta>
            <ta e="T576" id="Seg_9255" s="T572">KT1:1</ta>
            <ta e="T611" id="Seg_9256" s="T606">[GVY:] palatal d'</ta>
            <ta e="T618" id="Seg_9257" s="T611">[GVY:] Klavdiya with the stressed i. [AAV] and likely -eja (not -ija).</ta>
            <ta e="T687" id="Seg_9258" s="T682">[GVY:] In KT1 (probably correctly) "five years"</ta>
            <ta e="T694" id="Seg_9259" s="T691">[GVY:] here Klavdiya has the stress on "a".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T696" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T697" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T698" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T699" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
