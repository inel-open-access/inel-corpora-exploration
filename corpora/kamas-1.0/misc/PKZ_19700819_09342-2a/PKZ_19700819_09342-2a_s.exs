<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB3E12255-3D5D-0F2A-ED6D-02A104CA4835">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_19700819_09342_2a</transcription-name>
         <referenced-file url="PKZ_19700819_09342-2a.wav" />
         <referenced-file url="PKZ_19700819_09342-2a.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700819_09342-2a\PKZ_19700819_09342-2a.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">2441</ud-information>
            <ud-information attribute-name="# HIAT:w">1617</ud-information>
            <ud-information attribute-name="# e">1638</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">23</ud-information>
            <ud-information attribute-name="# HIAT:u">282</ud-information>
            <ud-information attribute-name="# sc">514</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="1.77" type="appl" />
         <tli id="T2" time="2.217142857142857" type="appl" />
         <tli id="T3" time="2.6642857142857146" type="appl" />
         <tli id="T4" time="3.1114285714285717" type="appl" />
         <tli id="T5" time="3.5585714285714287" type="appl" />
         <tli id="T6" time="4.005714285714285" type="appl" />
         <tli id="T7" time="4.01" type="appl" />
         <tli id="T8" time="4.452857142857143" type="appl" />
         <tli id="T9" time="4.66" type="appl" />
         <tli id="T10" time="4.9" type="appl" />
         <tli id="T11" time="5.0" type="appl" />
         <tli id="T12" time="5.252142857142857" type="appl" />
         <tli id="T13" time="5.5042857142857144" type="appl" />
         <tli id="T14" time="5.756428571428572" type="appl" />
         <tli id="T15" time="6.008571428571429" type="appl" />
         <tli id="T16" time="6.260714285714286" type="appl" />
         <tli id="T17" time="6.5128571428571425" type="appl" />
         <tli id="T18" time="6.765" type="appl" />
         <tli id="T19" time="7.017142857142857" type="appl" />
         <tli id="T20" time="7.269285714285714" type="appl" />
         <tli id="T21" time="7.521428571428571" type="appl" />
         <tli id="T22" time="7.773571428571428" type="appl" />
         <tli id="T23" time="8.025714285714285" type="appl" />
         <tli id="T24" time="8.277857142857142" type="appl" />
         <tli id="T25" time="8.53" type="appl" />
         <tli id="T26" time="9.9" type="appl" />
         <tli id="T1893" />
         <tli id="T1894" />
         <tli id="T27" time="17.7" type="appl" />
         <tli id="T28" time="21.22" type="appl" />
         <tli id="T1895" />
         <tli id="T1896" />
         <tli id="T29" time="25.54" type="appl" />
         <tli id="T30" time="30.16" type="appl" />
         <tli id="T31" time="30.645" type="appl" />
         <tli id="T32" time="31.193321708604138" />
         <tli id="T33" time="31.25" type="appl" />
         <tli id="T34" time="31.53142857142857" type="appl" />
         <tli id="T35" time="31.812857142857144" type="appl" />
         <tli id="T36" time="32.09428571428571" type="appl" />
         <tli id="T37" time="32.37571428571429" type="appl" />
         <tli id="T38" time="32.65714285714286" type="appl" />
         <tli id="T39" time="32.93857142857143" type="appl" />
         <tli id="T40" time="33.339987575278755" />
         <tli id="T41" time="37.09" type="appl" />
         <tli id="T1897" />
         <tli id="T1898" />
         <tli id="T42" time="41.64" type="appl" />
         <tli id="T43" time="43.53" type="appl" />
         <tli id="T44" time="44.2775" type="appl" />
         <tli id="T45" time="45.025000000000006" type="appl" />
         <tli id="T46" time="45.7725" type="appl" />
         <tli id="T47" time="46.65998261135294" />
         <tli id="T48" time="49.28" type="appl" />
         <tli id="T1899" />
         <tli id="T1900" />
         <tli id="T49" time="66.33" type="appl" />
         <tli id="T50" time="70.73" type="appl" />
         <tli id="T1901" />
         <tli id="T1902" />
         <tli id="T51" time="75.41" type="appl" />
         <tli id="T52" time="77.85" type="appl" />
         <tli id="T1903" />
         <tli id="T1904" />
         <tli id="T53" time="89.07" type="appl" />
         <tli id="T54" time="89.34" type="appl" />
         <tli id="T55" time="89.7" type="appl" />
         <tli id="T56" time="90.06" type="appl" />
         <tli id="T57" time="90.42" type="appl" />
         <tli id="T58" time="90.78" type="appl" />
         <tli id="T59" time="91.38" type="appl" />
         <tli id="T60" time="91.71249999999999" type="appl" />
         <tli id="T61" time="92.045" type="appl" />
         <tli id="T62" time="92.3775" type="appl" />
         <tli id="T63" time="92.71" type="appl" />
         <tli id="T64" time="93.0425" type="appl" />
         <tli id="T65" time="93.375" type="appl" />
         <tli id="T66" time="93.7075" type="appl" />
         <tli id="T67" time="94.04" type="appl" />
         <tli id="T68" time="94.3725" type="appl" />
         <tli id="T69" time="94.705" type="appl" />
         <tli id="T70" time="95.03750000000001" type="appl" />
         <tli id="T71" time="95.37" type="appl" />
         <tli id="T72" time="95.73" type="appl" />
         <tli id="T73" time="96.1875" type="appl" />
         <tli id="T74" time="96.64500000000001" type="appl" />
         <tli id="T75" time="97.1025" type="appl" />
         <tli id="T76" time="97.56" type="appl" />
         <tli id="T77" time="98.0175" type="appl" />
         <tli id="T78" time="98.475" type="appl" />
         <tli id="T79" time="98.9325" type="appl" />
         <tli id="T80" time="99.39" type="appl" />
         <tli id="T81" time="99.4" type="appl" />
         <tli id="T82" time="99.83909090909091" type="appl" />
         <tli id="T83" time="100.27818181818182" type="appl" />
         <tli id="T84" time="100.71727272727273" type="appl" />
         <tli id="T85" time="101.15636363636364" type="appl" />
         <tli id="T86" time="101.59545454545454" type="appl" />
         <tli id="T87" time="102.03454545454547" type="appl" />
         <tli id="T88" time="102.47363636363637" type="appl" />
         <tli id="T89" time="102.91272727272728" type="appl" />
         <tli id="T90" time="103.35181818181819" type="appl" />
         <tli id="T91" time="103.7909090909091" type="appl" />
         <tli id="T92" time="104.13" type="appl" />
         <tli id="T93" time="104.23" type="appl" />
         <tli id="T94" time="104.26" type="appl" />
         <tli id="T95" time="104.645" type="appl" />
         <tli id="T96" time="105.08" type="appl" />
         <tli id="T97" time="105.31996075070062" />
         <tli id="T98" time="105.9" type="appl" />
         <tli id="T99" time="106.72" type="appl" />
         <tli id="T100" time="106.84" type="appl" />
         <tli id="T101" time="107.388" type="appl" />
         <tli id="T102" time="107.41" type="appl" />
         <tli id="T103" time="107.87792857142857" type="appl" />
         <tli id="T104" time="107.936" type="appl" />
         <tli id="T105" time="108.34585714285714" type="appl" />
         <tli id="T106" time="108.484" type="appl" />
         <tli id="T107" time="108.81378571428571" type="appl" />
         <tli id="T108" time="109.032" type="appl" />
         <tli id="T109" time="109.28171428571429" type="appl" />
         <tli id="T110" time="109.58" type="appl" />
         <tli id="T111" time="109.74964285714286" type="appl" />
         <tli id="T112" time="110.21757142857143" type="appl" />
         <tli id="T113" time="110.68549999999999" type="appl" />
         <tli id="T114" time="111.15342857142856" type="appl" />
         <tli id="T115" time="111.62135714285714" type="appl" />
         <tli id="T116" time="112.08928571428571" type="appl" />
         <tli id="T117" time="112.55721428571428" type="appl" />
         <tli id="T118" time="112.66" type="appl" />
         <tli id="T119" time="113.02159999999999" type="appl" />
         <tli id="T120" time="113.02514285714285" type="appl" />
         <tli id="T121" time="113.3832" type="appl" />
         <tli id="T122" time="113.49307142857143" type="appl" />
         <tli id="T123" time="113.7448" type="appl" />
         <tli id="T124" time="113.961" type="appl" />
         <tli id="T125" time="114.1064" type="appl" />
         <tli id="T126" time="114.46799999999999" type="appl" />
         <tli id="T127" time="114.8296" type="appl" />
         <tli id="T128" time="115.1912" type="appl" />
         <tli id="T129" time="115.55279999999999" type="appl" />
         <tli id="T130" time="115.57" type="appl" />
         <tli id="T131" time="115.9144" type="appl" />
         <tli id="T132" time="116.2525" type="appl" />
         <tli id="T133" time="116.276" type="appl" />
         <tli id="T134" time="116.935" type="appl" />
         <tli id="T135" time="117.61749999999999" type="appl" />
         <tli id="T136" time="118.3" type="appl" />
         <tli id="T137" time="118.97" type="appl" />
         <tli id="T138" time="119.61333333333333" type="appl" />
         <tli id="T139" time="120.25666666666666" type="appl" />
         <tli id="T140" time="120.9" type="appl" />
         <tli id="T141" time="121.54333333333334" type="appl" />
         <tli id="T142" time="122.18666666666667" type="appl" />
         <tli id="T143" time="122.83" type="appl" />
         <tli id="T144" time="123.47333333333333" type="appl" />
         <tli id="T145" time="124.11666666666667" type="appl" />
         <tli id="T146" time="124.76" type="appl" />
         <tli id="T147" time="124.91" type="appl" />
         <tli id="T148" time="125.3111111111111" type="appl" />
         <tli id="T149" time="125.71222222222222" type="appl" />
         <tli id="T150" time="126.11333333333333" type="appl" />
         <tli id="T151" time="126.51444444444445" type="appl" />
         <tli id="T152" time="126.91555555555556" type="appl" />
         <tli id="T153" time="127.31666666666668" type="appl" />
         <tli id="T154" time="127.71777777777778" type="appl" />
         <tli id="T155" time="128.1188888888889" type="appl" />
         <tli id="T156" time="128.52" type="appl" />
         <tli id="T157" time="129.36" type="appl" />
         <tli id="T158" time="129.46" type="appl" />
         <tli id="T159" time="129.89166666666668" type="appl" />
         <tli id="T160" time="130.145" type="appl" />
         <tli id="T161" time="130.42333333333335" type="appl" />
         <tli id="T162" time="130.83" type="appl" />
         <tli id="T163" time="130.955" type="appl" />
         <tli id="T164" time="131.48666666666668" type="appl" />
         <tli id="T165" time="132.01833333333335" type="appl" />
         <tli id="T166" time="132.55" type="appl" />
         <tli id="T167" time="132.64" type="appl" />
         <tli id="T168" time="133.095" type="appl" />
         <tli id="T169" time="133.54999999999998" type="appl" />
         <tli id="T170" time="134.005" type="appl" />
         <tli id="T171" time="134.45999999999998" type="appl" />
         <tli id="T172" time="134.915" type="appl" />
         <tli id="T173" time="135.37" type="appl" />
         <tli id="T174" time="135.825" type="appl" />
         <tli id="T175" time="136.28" type="appl" />
         <tli id="T176" time="136.73499999999999" type="appl" />
         <tli id="T177" time="137.2532821835311" />
         <tli id="T178" time="137.31" type="appl" />
         <tli id="T179" time="137.674" type="appl" />
         <tli id="T180" time="138.038" type="appl" />
         <tli id="T181" time="138.27" type="appl" />
         <tli id="T182" time="138.402" type="appl" />
         <tli id="T183" time="138.7326666666667" type="appl" />
         <tli id="T184" time="138.766" type="appl" />
         <tli id="T185" time="139.13" type="appl" />
         <tli id="T186" time="139.19533333333334" type="appl" />
         <tli id="T187" time="139.65800000000002" type="appl" />
         <tli id="T188" time="140.12066666666666" type="appl" />
         <tli id="T189" time="140.58333333333334" type="appl" />
         <tli id="T190" time="141.04600000000002" type="appl" />
         <tli id="T191" time="141.50866666666667" type="appl" />
         <tli id="T192" time="141.97133333333335" type="appl" />
         <tli id="T193" time="142.434" type="appl" />
         <tli id="T194" time="142.89666666666668" type="appl" />
         <tli id="T195" time="143.35933333333335" type="appl" />
         <tli id="T196" time="143.822" type="appl" />
         <tli id="T197" time="144.28466666666668" type="appl" />
         <tli id="T198" time="144.74733333333333" type="appl" />
         <tli id="T199" time="145.21" type="appl" />
         <tli id="T200" time="145.29" type="appl" />
         <tli id="T201" time="145.57888888888888" type="appl" />
         <tli id="T202" time="145.86777777777777" type="appl" />
         <tli id="T203" time="146.15666666666667" type="appl" />
         <tli id="T204" time="146.44555555555556" type="appl" />
         <tli id="T205" time="146.73444444444442" type="appl" />
         <tli id="T206" time="147.0233333333333" type="appl" />
         <tli id="T207" time="147.3122222222222" type="appl" />
         <tli id="T208" time="147.6011111111111" type="appl" />
         <tli id="T209" time="147.89" type="appl" />
         <tli id="T210" time="148.56" type="appl" />
         <tli id="T211" time="148.79333333333332" type="appl" />
         <tli id="T212" time="149.02666666666667" type="appl" />
         <tli id="T213" time="149.26" type="appl" />
         <tli id="T214" time="149.49" type="appl" />
         <tli id="T215" time="149.49333333333334" type="appl" />
         <tli id="T216" time="149.72666666666666" type="appl" />
         <tli id="T217" time="149.96" type="appl" />
         <tli id="T218" time="150.19333333333333" type="appl" />
         <tli id="T219" time="150.42666666666668" type="appl" />
         <tli id="T220" time="150.585" type="appl" />
         <tli id="T221" time="150.66" type="appl" />
         <tli id="T222" time="150.69" type="appl" />
         <tli id="T223" time="151.14" type="appl" />
         <tli id="T224" time="151.555" type="appl" />
         <tli id="T225" time="151.68" type="appl" />
         <tli id="T226" time="151.94" type="appl" />
         <tli id="T227" time="152.1066099815157" />
         <tli id="T228" time="152.602" type="appl" />
         <tli id="T229" time="153.264" type="appl" />
         <tli id="T230" time="153.926" type="appl" />
         <tli id="T231" time="154.588" type="appl" />
         <tli id="T232" time="155.28999421183937" />
         <tli id="T233" time="155.41" type="appl" />
         <tli id="T234" time="155.48" type="appl" />
         <tli id="T235" time="155.65857142857143" type="appl" />
         <tli id="T236" time="155.90714285714284" type="appl" />
         <tli id="T237" time="156.15571428571428" type="appl" />
         <tli id="T238" time="156.3466084014072" />
         <tli id="T239" time="156.40428571428572" type="appl" />
         <tli id="T240" time="156.65285714285716" type="appl" />
         <tli id="T241" time="156.90142857142857" type="appl" />
         <tli id="T242" time="157.05" type="appl" />
         <tli id="T243" time="157.15" type="appl" />
         <tli id="T244" time="157.56" type="appl" />
         <tli id="T245" time="158.02" type="appl" />
         <tli id="T246" time="158.48000000000002" type="appl" />
         <tli id="T247" time="158.94" type="appl" />
         <tli id="T248" time="159.4" type="appl" />
         <tli id="T249" time="159.9" type="appl" />
         <tli id="T250" time="161.06" type="appl" />
         <tli id="T251" time="161.14" type="appl" />
         <tli id="T252" time="161.57999999999998" type="appl" />
         <tli id="T253" time="162.01999999999998" type="appl" />
         <tli id="T254" time="162.46" type="appl" />
         <tli id="T255" time="162.9" type="appl" />
         <tli id="T256" time="163.34" type="appl" />
         <tli id="T257" time="163.35327245691968" />
         <tli id="T258" time="163.85000000000002" type="appl" />
         <tli id="T259" time="164.24" type="appl" />
         <tli id="T260" time="164.63" type="appl" />
         <tli id="T261" time="165.02" type="appl" />
         <tli id="T262" time="165.41000000000003" type="appl" />
         <tli id="T263" time="165.8" type="appl" />
         <tli id="T264" time="166.19" type="appl" />
         <tli id="T265" time="166.58" type="appl" />
         <tli id="T266" time="166.97000000000003" type="appl" />
         <tli id="T267" time="167.42666937639808" />
         <tli id="T268" time="168.05327070538428" />
         <tli id="T269" time="168.226603974122" />
         <tli id="T270" time="168.6732704743307" />
         <tli id="T271" time="168.78857142857143" type="appl" />
         <tli id="T272" time="169.24142857142857" type="appl" />
         <tli id="T273" time="169.6942857142857" type="appl" />
         <tli id="T274" time="170.14714285714285" type="appl" />
         <tli id="T275" time="170.6" type="appl" />
         <tli id="T276" time="171.52" type="appl" />
         <tli id="T277" time="171.77" type="appl" />
         <tli id="T278" time="171.7799359832449" />
         <tli id="T279" time="172.1415" type="appl" />
         <tli id="T280" time="172.513" type="appl" />
         <tli id="T281" time="172.8845" type="appl" />
         <tli id="T282" time="173.256" type="appl" />
         <tli id="T283" time="173.6275" type="appl" />
         <tli id="T284" time="173.999" type="appl" />
         <tli id="T285" time="174.3705" type="appl" />
         <tli id="T286" time="174.742" type="appl" />
         <tli id="T287" time="175.1135" type="appl" />
         <tli id="T288" time="175.485" type="appl" />
         <tli id="T289" time="175.8565" type="appl" />
         <tli id="T290" time="176.228" type="appl" />
         <tli id="T291" time="176.5995" type="appl" />
         <tli id="T292" time="176.971" type="appl" />
         <tli id="T293" time="177.3425" type="appl" />
         <tli id="T294" time="177.714" type="appl" />
         <tli id="T295" time="178.0855" type="appl" />
         <tli id="T296" time="178.457" type="appl" />
         <tli id="T297" time="178.55" type="appl" />
         <tli id="T298" time="178.8285" type="appl" />
         <tli id="T299" time="178.88250000000002" type="appl" />
         <tli id="T300" time="179.2" type="appl" />
         <tli id="T301" time="179.215" type="appl" />
         <tli id="T302" time="179.5475" type="appl" />
         <tli id="T303" time="179.88" type="appl" />
         <tli id="T304" time="180.2125" type="appl" />
         <tli id="T305" time="180.54500000000002" type="appl" />
         <tli id="T306" time="180.8775" type="appl" />
         <tli id="T307" time="181.21" type="appl" />
         <tli id="T308" time="181.5425" type="appl" />
         <tli id="T309" time="181.875" type="appl" />
         <tli id="T310" time="182.20749999999998" type="appl" />
         <tli id="T311" time="182.54" type="appl" />
         <tli id="T1905" />
         <tli id="T1906" />
         <tli id="T312" time="198.91" type="appl" />
         <tli id="T313" time="199.04" type="appl" />
         <tli id="T1907" />
         <tli id="T1908" />
         <tli id="T314" time="205.4" type="appl" />
         <tli id="T315" time="207.78" type="appl" />
         <tli id="T316" time="208.34" type="appl" />
         <tli id="T317" time="208.9" type="appl" />
         <tli id="T318" time="209.46" type="appl" />
         <tli id="T319" time="210.02" type="appl" />
         <tli id="T320" time="214.41" type="appl" />
         <tli id="T321" time="215.2725" type="appl" />
         <tli id="T322" time="216.135" type="appl" />
         <tli id="T323" time="216.9975" type="appl" />
         <tli id="T324" time="217.86" type="appl" />
         <tli id="T325" time="221.19" type="appl" />
         <tli id="T326" time="221.975" type="appl" />
         <tli id="T327" time="222.76" type="appl" />
         <tli id="T328" time="223.38" type="appl" />
         <tli id="T329" time="223.8425" type="appl" />
         <tli id="T330" time="224.305" type="appl" />
         <tli id="T331" time="224.76749999999998" type="appl" />
         <tli id="T332" time="225.23" type="appl" />
         <tli id="T333" time="226.42666666666665" type="appl" />
         <tli id="T334" time="227.62333333333333" type="appl" />
         <tli id="T335" time="228.82" type="appl" />
         <tli id="T336" time="228.94" type="appl" />
         <tli id="T337" time="229.46333333333334" type="appl" />
         <tli id="T338" time="229.98666666666665" type="appl" />
         <tli id="T339" time="230.51" type="appl" />
         <tli id="T340" time="231.02" type="appl" />
         <tli id="T341" time="231.7425" type="appl" />
         <tli id="T342" time="232.465" type="appl" />
         <tli id="T343" time="233.1875" type="appl" />
         <tli id="T344" time="233.91" type="appl" />
         <tli id="T345" time="233.97" type="appl" />
         <tli id="T346" time="234.827125" type="appl" />
         <tli id="T347" time="235.68425" type="appl" />
         <tli id="T348" time="236.541375" type="appl" />
         <tli id="T349" time="237.3985" type="appl" />
         <tli id="T350" time="238.255625" type="appl" />
         <tli id="T351" time="239.11275" type="appl" />
         <tli id="T352" time="239.969875" type="appl" />
         <tli id="T353" time="240.827" type="appl" />
         <tli id="T354" time="241.05" type="appl" />
         <tli id="T355" time="241.72" type="appl" />
         <tli id="T356" time="242.39000000000001" type="appl" />
         <tli id="T357" time="243.06" type="appl" />
         <tli id="T358" time="243.73000000000002" type="appl" />
         <tli id="T359" time="244.4" type="appl" />
         <tli id="T360" time="245.07000000000002" type="appl" />
         <tli id="T361" time="245.83990838351914" />
         <tli id="T362" time="247.01" type="appl" />
         <tli id="T363" time="247.6" type="appl" />
         <tli id="T364" time="248.19" type="appl" />
         <tli id="T365" time="248.78" type="appl" />
         <tli id="T366" time="249.36999999999998" type="appl" />
         <tli id="T367" time="249.95999999999998" type="appl" />
         <tli id="T368" time="250.54999999999998" type="appl" />
         <tli id="T369" time="251.14" type="appl" />
         <tli id="T370" time="251.88" type="appl" />
         <tli id="T371" time="252.97" type="appl" />
         <tli id="T372" time="254.06" type="appl" />
         <tli id="T373" time="255.14999999999998" type="appl" />
         <tli id="T374" time="256.24" type="appl" />
         <tli id="T375" time="257.33" type="appl" />
         <tli id="T376" time="257.72" type="appl" />
         <tli id="T377" time="258.58500000000004" type="appl" />
         <tli id="T378" time="259.45000000000005" type="appl" />
         <tli id="T379" time="260.315" type="appl" />
         <tli id="T380" time="261.18" type="appl" />
         <tli id="T381" time="261.6" type="appl" />
         <tli id="T382" time="262.2475" type="appl" />
         <tli id="T383" time="262.895" type="appl" />
         <tli id="T384" time="263.5425" type="appl" />
         <tli id="T385" time="264.19" type="appl" />
         <tli id="T386" time="264.26" type="appl" />
         <tli id="T387" time="264.8566666666667" type="appl" />
         <tli id="T388" time="265.4533333333333" type="appl" />
         <tli id="T389" time="266.05" type="appl" />
         <tli id="T390" time="266.555" type="appl" />
         <tli id="T391" time="267.06" type="appl" />
         <tli id="T392" time="267.565" type="appl" />
         <tli id="T393" time="268.07" type="appl" />
         <tli id="T394" time="268.575" type="appl" />
         <tli id="T395" time="269.08" type="appl" />
         <tli id="T396" time="269.11" type="appl" />
         <tli id="T397" time="270.315" type="appl" />
         <tli id="T398" time="271.52" type="appl" />
         <tli id="T399" time="271.79" type="appl" />
         <tli id="T400" time="272.7" type="appl" />
         <tli id="T401" time="273.61" type="appl" />
         <tli id="T402" time="274.52" type="appl" />
         <tli id="T403" time="275.43" type="appl" />
         <tli id="T404" time="276.34" type="appl" />
         <tli id="T405" time="276.4" type="appl" />
         <tli id="T406" time="277.078" type="appl" />
         <tli id="T407" time="277.756" type="appl" />
         <tli id="T408" time="278.434" type="appl" />
         <tli id="T409" time="279.112" type="appl" />
         <tli id="T410" time="279.79" type="appl" />
         <tli id="T411" time="280.51" type="appl" />
         <tli id="T412" time="281.57" type="appl" />
         <tli id="T413" time="282.63" type="appl" />
         <tli id="T414" time="282.88" type="appl" />
         <tli id="T415" time="283.55" type="appl" />
         <tli id="T416" time="284.21999999999997" type="appl" />
         <tli id="T417" time="284.89" type="appl" />
         <tli id="T418" time="286.03" type="appl" />
         <tli id="T419" time="286.97066666666666" type="appl" />
         <tli id="T420" time="287.9113333333333" type="appl" />
         <tli id="T421" time="288.852" type="appl" />
         <tli id="T422" time="288.88" type="appl" />
         <tli id="T423" time="289.56666666666666" type="appl" />
         <tli id="T424" time="290.25333333333333" type="appl" />
         <tli id="T425" time="290.94" type="appl" />
         <tli id="T426" time="291.65" type="appl" />
         <tli id="T427" time="292.38" type="appl" />
         <tli id="T428" time="293.11" type="appl" />
         <tli id="T429" time="293.84" type="appl" />
         <tli id="T430" time="294.57" type="appl" />
         <tli id="T431" time="295.3" type="appl" />
         <tli id="T432" time="296.03000000000003" type="appl" />
         <tli id="T433" time="296.76" type="appl" />
         <tli id="T434" time="297.49" type="appl" />
         <tli id="T435" time="298.22" type="appl" />
         <tli id="T436" time="299.09" type="appl" />
         <tli id="T437" time="299.8825" type="appl" />
         <tli id="T438" time="300.675" type="appl" />
         <tli id="T439" time="301.4675" type="appl" />
         <tli id="T440" time="302.26" type="appl" />
         <tli id="T441" time="303.0525" type="appl" />
         <tli id="T442" time="303.845" type="appl" />
         <tli id="T443" time="304.09" type="appl" />
         <tli id="T444" time="304.65" type="appl" />
         <tli id="T445" time="305.2633237383588" />
         <tli id="T446" time="305.84999999999997" type="appl" />
         <tli id="T447" time="306.49" type="appl" />
         <tli id="T448" time="307.13" type="appl" />
         <tli id="T449" time="307.77" type="appl" />
         <tli id="T450" time="307.91" type="appl" />
         <tli id="T451" time="308.475" type="appl" />
         <tli id="T452" time="309.04" type="appl" />
         <tli id="T453" time="309.25" type="appl" />
         <tli id="T454" time="310.525" type="appl" />
         <tli id="T455" time="311.8" type="appl" />
         <tli id="T456" time="313.07500000000005" type="appl" />
         <tli id="T457" time="314.4532161469203" />
         <tli id="T458" time="314.51" type="appl" />
         <tli id="T459" time="315.19666666666666" type="appl" />
         <tli id="T460" time="315.8833333333333" type="appl" />
         <tli id="T461" time="316.57" type="appl" />
         <tli id="T462" time="317.25666666666666" type="appl" />
         <tli id="T463" time="317.9433333333333" type="appl" />
         <tli id="T464" time="318.63" type="appl" />
         <tli id="T465" time="318.72" type="appl" />
         <tli id="T466" time="319.90500000000003" type="appl" />
         <tli id="T467" time="321.09000000000003" type="appl" />
         <tli id="T468" time="322.275" type="appl" />
         <tli id="T469" time="323.46" type="appl" />
         <tli id="T470" time="325.39" type="appl" />
         <tli id="T471" time="326.49525" type="appl" />
         <tli id="T472" time="327.6005" type="appl" />
         <tli id="T473" time="328.70574999999997" type="appl" />
         <tli id="T474" time="329.811" type="appl" />
         <tli id="T475" time="329.921" type="appl" />
         <tli id="T476" time="330.567" type="appl" />
         <tli id="T477" time="331.213" type="appl" />
         <tli id="T478" time="331.859" type="appl" />
         <tli id="T479" time="332.505" type="appl" />
         <tli id="T480" time="333.2998757900543" />
         <tli id="T481" time="333.59" type="appl" />
         <tli id="T482" time="334.52" type="appl" />
         <tli id="T483" time="335.45" type="appl" />
         <tli id="T484" time="336.38" type="appl" />
         <tli id="T485" time="337.31" type="appl" />
         <tli id="T1918" time="337.7392307692308" type="intp" />
         <tli id="T486" time="338.24" type="appl" />
         <tli id="T487" time="339.22654024804723" />
         <tli id="T488" time="339.37" type="appl" />
         <tli id="T489" time="340.5" type="appl" />
         <tli id="T490" time="341.63" type="appl" />
         <tli id="T491" time="342.76" type="appl" />
         <tli id="T492" time="343.89" type="appl" />
         <tli id="T493" time="344.51" type="appl" />
         <tli id="T494" time="345.62166666666667" type="appl" />
         <tli id="T495" time="346.73333333333335" type="appl" />
         <tli id="T496" time="347.845" type="appl" />
         <tli id="T497" time="348.95666666666665" type="appl" />
         <tli id="T498" time="350.0683333333333" type="appl" />
         <tli id="T499" time="351.20666599178014" />
         <tli id="T500" time="351.2099993238712" />
         <tli id="T501" time="351.74333333333334" type="appl" />
         <tli id="T502" time="352.2966666666667" type="appl" />
         <tli id="T503" time="352.93986847087234" />
         <tli id="T504" time="352.94" type="appl" />
         <tli id="T505" time="353.47333333333336" type="appl" />
         <tli id="T506" time="354.00666666666666" type="appl" />
         <tli id="T507" time="354.53999999999996" type="appl" />
         <tli id="T508" time="355.0733333333333" type="appl" />
         <tli id="T509" time="355.6066666666667" type="appl" />
         <tli id="T510" time="356.14" type="appl" />
         <tli id="T511" time="356.58" type="appl" />
         <tli id="T512" time="357.54499999999996" type="appl" />
         <tli id="T513" time="358.51" type="appl" />
         <tli id="T514" time="359.475" type="appl" />
         <tli id="T515" time="360.44" type="appl" />
         <tli id="T516" time="361.1" type="appl" />
         <tli id="T517" time="362.40383333333335" type="appl" />
         <tli id="T518" time="363.7076666666667" type="appl" />
         <tli id="T519" time="365.0115" type="appl" />
         <tli id="T520" time="366.31533333333334" type="appl" />
         <tli id="T521" time="367.6191666666667" type="appl" />
         <tli id="T522" time="368.923" type="appl" />
         <tli id="T523" time="369.28" type="appl" />
         <tli id="T524" time="370.283125" type="appl" />
         <tli id="T525" time="371.28625" type="appl" />
         <tli id="T526" time="372.289375" type="appl" />
         <tli id="T527" time="373.2925" type="appl" />
         <tli id="T528" time="374.295625" type="appl" />
         <tli id="T529" time="375.29875" type="appl" />
         <tli id="T530" time="376.301875" type="appl" />
         <tli id="T1917" time="376.7835165021378" type="intp" />
         <tli id="T531" time="377.66652592272374" />
         <tli id="T532" time="380.77" type="appl" />
         <tli id="T533" time="381.79499999999996" type="appl" />
         <tli id="T534" time="382.82" type="appl" />
         <tli id="T535" time="383.845" type="appl" />
         <tli id="T536" time="384.87" type="appl" />
         <tli id="T537" time="384.98" type="appl" />
         <tli id="T538" time="386.03285714285715" type="appl" />
         <tli id="T539" time="387.0857142857143" type="appl" />
         <tli id="T540" time="388.1385714285714" type="appl" />
         <tli id="T541" time="389.1914285714286" type="appl" />
         <tli id="T542" time="390.24428571428575" type="appl" />
         <tli id="T543" time="391.2971428571429" type="appl" />
         <tli id="T544" time="392.4100100116435" />
         <tli id="T545" time="392.41334334373465" />
         <tli id="T546" time="393.458" type="appl" />
         <tli id="T547" time="394.556" type="appl" />
         <tli id="T548" time="395.654" type="appl" />
         <tli id="T549" time="396.752" type="appl" />
         <tli id="T550" time="397.85" type="appl" />
         <tli id="T551" time="398.948" type="appl" />
         <tli id="T552" time="400.046" type="appl" />
         <tli id="T553" time="401.144" type="appl" />
         <tli id="T554" time="402.24199999999996" type="appl" />
         <tli id="T555" time="403.34" type="appl" />
         <tli id="T556" time="404.0125" type="appl" />
         <tli id="T557" time="404.68499999999995" type="appl" />
         <tli id="T558" time="405.35749999999996" type="appl" />
         <tli id="T559" time="406.3731818913601" />
         <tli id="T560" time="407.97" type="appl" />
         <tli id="T561" time="408.56575000000004" type="appl" />
         <tli id="T562" time="409.16150000000005" type="appl" />
         <tli id="T563" time="409.75725" type="appl" />
         <tli id="T564" time="410.353" type="appl" />
         <tli id="T565" time="410.57" type="appl" />
         <tli id="T566" time="411.3917272727273" type="appl" />
         <tli id="T567" time="412.2134545454545" type="appl" />
         <tli id="T568" time="413.0351818181818" type="appl" />
         <tli id="T569" time="413.8569090909091" type="appl" />
         <tli id="T570" time="414.6786363636364" type="appl" />
         <tli id="T571" time="415.5003636363636" type="appl" />
         <tli id="T572" time="416.3220909090909" type="appl" />
         <tli id="T573" time="417.1438181818182" type="appl" />
         <tli id="T574" time="417.96554545454546" type="appl" />
         <tli id="T575" time="418.7872727272727" type="appl" />
         <tli id="T576" time="419.609" type="appl" />
         <tli id="T577" time="421.06" type="appl" />
         <tli id="T578" time="421.9416" type="appl" />
         <tli id="T579" time="422.8232" type="appl" />
         <tli id="T580" time="423.70480000000003" type="appl" />
         <tli id="T581" time="424.5864" type="appl" />
         <tli id="T582" time="425.59317472869833" />
         <tli id="T583" time="425.8" type="appl" />
         <tli id="T584" time="426.8175" type="appl" />
         <tli id="T585" time="427.83500000000004" type="appl" />
         <tli id="T586" time="428.8525" type="appl" />
         <tli id="T587" time="429.87" type="appl" />
         <tli id="T588" time="429.92" type="appl" />
         <tli id="T589" time="430.62625" type="appl" />
         <tli id="T590" time="431.3325" type="appl" />
         <tli id="T591" time="432.03875" type="appl" />
         <tli id="T592" time="432.745" type="appl" />
         <tli id="T593" time="433.45125" type="appl" />
         <tli id="T594" time="434.1575" type="appl" />
         <tli id="T595" time="434.86375" type="appl" />
         <tli id="T596" time="435.57" type="appl" />
         <tli id="T597" time="435.6" type="appl" />
         <tli id="T598" time="436.12" type="appl" />
         <tli id="T599" time="436.64" type="appl" />
         <tli id="T600" time="437.16" type="appl" />
         <tli id="T601" time="438.1931700330928" />
         <tli id="T602" time="438.74" type="appl" />
         <tli id="T603" time="440.1186" type="appl" />
         <tli id="T604" time="441.4972" type="appl" />
         <tli id="T605" time="442.87579999999997" type="appl" />
         <tli id="T606" time="444.2544" type="appl" />
         <tli id="T607" time="445.633" type="appl" />
         <tli id="T608" time="445.96" type="appl" />
         <tli id="T609" time="446.66999999999996" type="appl" />
         <tli id="T610" time="447.38" type="appl" />
         <tli id="T611" time="448.09" type="appl" />
         <tli id="T612" time="448.79999999999995" type="appl" />
         <tli id="T613" time="449.51" type="appl" />
         <tli id="T614" time="450.21999999999997" type="appl" />
         <tli id="T615" time="450.93" type="appl" />
         <tli id="T616" time="451.64" type="appl" />
         <tli id="T617" time="452.41499999999996" type="appl" />
         <tli id="T618" time="453.19" type="appl" />
         <tli id="T619" time="453.96500000000003" type="appl" />
         <tli id="T620" time="454.74" type="appl" />
         <tli id="T621" time="455.39" type="appl" />
         <tli id="T622" time="456.095" type="appl" />
         <tli id="T623" time="456.8" type="appl" />
         <tli id="T624" time="456.85" type="appl" />
         <tli id="T625" time="457.48333333333335" type="appl" />
         <tli id="T626" time="458.1166666666667" type="appl" />
         <tli id="T627" time="458.75" type="appl" />
         <tli id="T628" time="459.31" type="appl" />
         <tli id="T629" time="460.5198283793453" />
         <tli id="T630" time="460.6" type="appl" />
         <tli id="T631" time="461.34000000000003" type="appl" />
         <tli id="T632" time="462.08" type="appl" />
         <tli id="T633" time="462.82" type="appl" />
         <tli id="T634" time="464.03" type="appl" />
         <tli id="T635" time="464.5" type="appl" />
         <tli id="T636" time="464.96999999999997" type="appl" />
         <tli id="T637" time="466.1198262924095" />
         <tli id="T638" time="467.65" type="appl" />
         <tli id="T639" time="468.48" type="appl" />
         <tli id="T640" time="468.5" type="appl" />
         <tli id="T641" time="468.74" type="appl" />
         <tli id="T642" time="469.1775" type="appl" />
         <tli id="T643" time="469.59" type="appl" />
         <tli id="T644" time="469.615" type="appl" />
         <tli id="T645" time="470.0525" type="appl" />
         <tli id="T646" time="470.49" type="appl" />
         <tli id="T647" time="470.9275" type="appl" />
         <tli id="T648" time="471.365" type="appl" />
         <tli id="T649" time="471.8025" type="appl" />
         <tli id="T650" time="472.24" type="appl" />
         <tli id="T651" time="472.6775" type="appl" />
         <tli id="T652" time="473.115" type="appl" />
         <tli id="T653" time="473.25" type="appl" />
         <tli id="T654" time="473.5525" type="appl" />
         <tli id="T655" time="473.96" type="appl" />
         <tli id="T656" time="473.99" type="appl" />
         <tli id="T657" time="474.84648970693456" />
         <tli id="T658" time="475.4171428571429" type="appl" />
         <tli id="T659" time="475.8542857142857" type="appl" />
         <tli id="T660" time="476.2914285714286" type="appl" />
         <tli id="T661" time="476.72857142857146" type="appl" />
         <tli id="T662" time="477.16571428571433" type="appl" />
         <tli id="T663" time="477.60285714285715" type="appl" />
         <tli id="T664" time="478.19982179059093" />
         <tli id="T665" time="478.2" type="appl" />
         <tli id="T666" time="478.7416666666667" type="appl" />
         <tli id="T667" time="479.2833333333333" type="appl" />
         <tli id="T668" time="479.825" type="appl" />
         <tli id="T669" time="480.3666666666667" type="appl" />
         <tli id="T670" time="480.4" type="appl" />
         <tli id="T671" time="480.9083333333333" type="appl" />
         <tli id="T672" time="481.05499999999995" type="appl" />
         <tli id="T673" time="481.45" type="appl" />
         <tli id="T674" time="481.71" type="appl" />
         <tli id="T675" time="481.89315374754045" />
         <tli id="T676" time="482.05" type="appl" />
         <tli id="T677" time="482.59" type="appl" />
         <tli id="T678" time="482.98" type="appl" />
         <tli id="T1909" />
         <tli id="T1910" />
         <tli id="T679" time="484.53" type="appl" />
         <tli id="T680" time="485.84" type="appl" />
         <tli id="T681" time="486.34277777777777" type="appl" />
         <tli id="T682" time="486.8455555555555" type="appl" />
         <tli id="T683" time="487.3483333333333" type="appl" />
         <tli id="T684" time="487.8511111111111" type="appl" />
         <tli id="T685" time="488.3538888888889" type="appl" />
         <tli id="T686" time="488.8566666666666" type="appl" />
         <tli id="T687" time="489.3594444444444" type="appl" />
         <tli id="T688" time="489.8622222222222" type="appl" />
         <tli id="T689" time="490.365" type="appl" />
         <tli id="T690" time="490.86777777777775" type="appl" />
         <tli id="T691" time="491.37055555555554" type="appl" />
         <tli id="T692" time="491.87333333333333" type="appl" />
         <tli id="T693" time="492.3761111111111" type="appl" />
         <tli id="T694" time="492.87888888888887" type="appl" />
         <tli id="T695" time="493.38166666666666" type="appl" />
         <tli id="T696" time="493.88444444444445" type="appl" />
         <tli id="T697" time="494.3872222222222" type="appl" />
         <tli id="T698" time="494.9798155372369" />
         <tli id="T699" time="495.48" type="appl" />
         <tli id="T700" time="495.97707692307694" type="appl" />
         <tli id="T701" time="496.47415384615385" type="appl" />
         <tli id="T702" time="496.9712307692308" type="appl" />
         <tli id="T703" time="497.4683076923077" type="appl" />
         <tli id="T704" time="497.9653846153846" type="appl" />
         <tli id="T705" time="498.4624615384615" type="appl" />
         <tli id="T706" time="498.9595384615385" type="appl" />
         <tli id="T707" time="499.4566153846154" type="appl" />
         <tli id="T708" time="499.95369230769234" type="appl" />
         <tli id="T709" time="500.45076923076925" type="appl" />
         <tli id="T710" time="500.94784615384617" type="appl" />
         <tli id="T711" time="501.4449230769231" type="appl" />
         <tli id="T712" time="501.942" type="appl" />
         <tli id="T713" time="502.21" type="appl" />
         <tli id="T714" time="502.825" type="appl" />
         <tli id="T715" time="503.44" type="appl" />
         <tli id="T716" time="504.055" type="appl" />
         <tli id="T717" time="504.67" type="appl" />
         <tli id="T718" time="505.28499999999997" type="appl" />
         <tli id="T719" time="505.9" type="appl" />
         <tli id="T720" time="506.515" type="appl" />
         <tli id="T721" time="507.13" type="appl" />
         <tli id="T722" time="507.745" type="appl" />
         <tli id="T723" time="508.36" type="appl" />
         <tli id="T724" time="509.95" type="appl" />
         <tli id="T725" time="510.495" type="appl" />
         <tli id="T726" time="511.03999999999996" type="appl" />
         <tli id="T727" time="511.585" type="appl" />
         <tli id="T728" time="512.13" type="appl" />
         <tli id="T729" time="513.31" type="appl" />
         <tli id="T730" time="514.295" type="appl" />
         <tli id="T731" time="515.28" type="appl" />
         <tli id="T732" time="515.7183333333332" type="appl" />
         <tli id="T733" time="516.1566666666666" type="appl" />
         <tli id="T734" time="516.595" type="appl" />
         <tli id="T735" time="517.0333333333333" type="appl" />
         <tli id="T736" time="517.4716666666666" type="appl" />
         <tli id="T737" time="517.91" type="appl" />
         <tli id="T738" time="517.96" type="appl" />
         <tli id="T739" time="518.5858333333333" type="appl" />
         <tli id="T740" time="519.2116666666667" type="appl" />
         <tli id="T741" time="519.8375000000001" type="appl" />
         <tli id="T742" time="520.4633333333334" type="appl" />
         <tli id="T743" time="521.0891666666666" type="appl" />
         <tli id="T744" time="521.715" type="appl" />
         <tli id="T745" time="522.3408333333334" type="appl" />
         <tli id="T746" time="522.9666666666667" type="appl" />
         <tli id="T747" time="523.5925" type="appl" />
         <tli id="T748" time="524.2183333333334" type="appl" />
         <tli id="T749" time="524.8441666666668" type="appl" />
         <tli id="T750" time="525.47" type="appl" />
         <tli id="T751" time="525.74" type="appl" />
         <tli id="T752" time="527.0796666666666" type="appl" />
         <tli id="T753" time="528.4193333333334" type="appl" />
         <tli id="T754" time="529.759" type="appl" />
         <tli id="T755" time="529.8998025237017" />
         <tli id="T756" time="530.4857142857143" type="appl" />
         <tli id="T757" time="530.9714285714285" type="appl" />
         <tli id="T758" time="531.4571428571428" type="appl" />
         <tli id="T759" time="531.9428571428572" type="appl" />
         <tli id="T760" time="532.4285714285714" type="appl" />
         <tli id="T761" time="532.9142857142857" type="appl" />
         <tli id="T762" time="533.4" type="appl" />
         <tli id="T763" time="533.8857142857142" type="appl" />
         <tli id="T764" time="534.3714285714285" type="appl" />
         <tli id="T765" time="534.8571428571428" type="appl" />
         <tli id="T766" time="535.3428571428572" type="appl" />
         <tli id="T767" time="535.8285714285714" type="appl" />
         <tli id="T768" time="536.3142857142857" type="appl" />
         <tli id="T769" time="536.8" type="appl" />
         <tli id="T770" time="536.97" type="appl" />
         <tli id="T771" time="537.7" type="appl" />
         <tli id="T772" time="538.43" type="appl" />
         <tli id="T773" time="539.16" type="appl" />
         <tli id="T774" time="539.55" type="appl" />
         <tli id="T775" time="540.0372727272727" type="appl" />
         <tli id="T776" time="540.5245454545454" type="appl" />
         <tli id="T777" time="541.0118181818182" type="appl" />
         <tli id="T778" time="541.4990909090909" type="appl" />
         <tli id="T779" time="541.9863636363636" type="appl" />
         <tli id="T780" time="542.4736363636363" type="appl" />
         <tli id="T781" time="542.960909090909" type="appl" />
         <tli id="T782" time="543.4481818181818" type="appl" />
         <tli id="T783" time="543.9354545454545" type="appl" />
         <tli id="T784" time="544.4227272727272" type="appl" />
         <tli id="T785" time="544.91" type="appl" />
         <tli id="T786" time="545.97" type="appl" />
         <tli id="T787" time="546.4933333333333" type="appl" />
         <tli id="T788" time="547.0166666666667" type="appl" />
         <tli id="T789" time="547.54" type="appl" />
         <tli id="T790" time="547.78" type="appl" />
         <tli id="T791" time="548.3767272727273" type="appl" />
         <tli id="T792" time="548.9734545454545" type="appl" />
         <tli id="T793" time="549.5701818181818" type="appl" />
         <tli id="T794" time="550.1669090909091" type="appl" />
         <tli id="T795" time="550.7636363636364" type="appl" />
         <tli id="T796" time="551.3603636363637" type="appl" />
         <tli id="T797" time="551.9570909090909" type="appl" />
         <tli id="T798" time="552.5538181818182" type="appl" />
         <tli id="T799" time="553.1505454545455" type="appl" />
         <tli id="T800" time="553.7472727272727" type="appl" />
         <tli id="T801" time="554.4531267068153" />
         <tli id="T802" time="555.38" type="appl" />
         <tli id="T803" time="555.86" type="appl" />
         <tli id="T804" time="556.34" type="appl" />
         <tli id="T805" time="556.8199999999999" type="appl" />
         <tli id="T806" time="557.3" type="appl" />
         <tli id="T807" time="557.78" type="appl" />
         <tli id="T808" time="558.26" type="appl" />
         <tli id="T809" time="558.4131252310536" />
         <tli id="T810" time="559.1648461538462" type="appl" />
         <tli id="T811" time="559.8096923076923" type="appl" />
         <tli id="T812" time="560.4545384615385" type="appl" />
         <tli id="T813" time="561.0993846153846" type="appl" />
         <tli id="T814" time="561.7442307692307" type="appl" />
         <tli id="T815" time="562.3890769230769" type="appl" />
         <tli id="T816" time="563.0339230769231" type="appl" />
         <tli id="T817" time="563.6787692307693" type="appl" />
         <tli id="T818" time="564.3236153846154" type="appl" />
         <tli id="T819" time="564.9684615384615" type="appl" />
         <tli id="T820" time="565.6133076923077" type="appl" />
         <tli id="T821" time="566.2581538461538" type="appl" />
         <tli id="T822" time="566.9197887275654" />
         <tli id="T823" time="567.02" type="appl" />
         <tli id="T824" time="567.5314285714286" type="appl" />
         <tli id="T825" time="568.0428571428571" type="appl" />
         <tli id="T826" time="568.5542857142857" type="appl" />
         <tli id="T827" time="569.0657142857143" type="appl" />
         <tli id="T828" time="569.5771428571429" type="appl" />
         <tli id="T829" time="570.0885714285714" type="appl" />
         <tli id="T830" time="570.6" type="appl" />
         <tli id="T831" time="570.761" type="appl" />
         <tli id="T832" time="571.4467142857143" type="appl" />
         <tli id="T833" time="572.1324285714286" type="appl" />
         <tli id="T834" time="572.8181428571429" type="appl" />
         <tli id="T835" time="573.5038571428571" type="appl" />
         <tli id="T836" time="574.1895714285714" type="appl" />
         <tli id="T837" time="574.8752857142857" type="appl" />
         <tli id="T838" time="575.561" type="appl" />
         <tli id="T839" time="575.63" type="appl" />
         <tli id="T840" time="576.1888888888889" type="appl" />
         <tli id="T841" time="576.7477777777777" type="appl" />
         <tli id="T842" time="577.3066666666666" type="appl" />
         <tli id="T843" time="577.8655555555555" type="appl" />
         <tli id="T844" time="578.4244444444445" type="appl" />
         <tli id="T845" time="578.9833333333333" type="appl" />
         <tli id="T846" time="579.5422222222222" type="appl" />
         <tli id="T847" time="580.1011111111111" type="appl" />
         <tli id="T848" time="580.66" type="appl" />
         <tli id="T849" time="581.28" type="appl" />
         <tli id="T850" time="581.8928571428571" type="appl" />
         <tli id="T851" time="582.5057142857142" type="appl" />
         <tli id="T852" time="583.1185714285714" type="appl" />
         <tli id="T853" time="583.7314285714286" type="appl" />
         <tli id="T854" time="584.3442857142858" type="appl" />
         <tli id="T855" time="584.9571428571429" type="appl" />
         <tli id="T856" time="585.57" type="appl" />
         <tli id="T857" time="586.3391666666666" type="appl" />
         <tli id="T858" time="587.1083333333333" type="appl" />
         <tli id="T859" time="587.8775" type="appl" />
         <tli id="T860" time="588.6466666666666" type="appl" />
         <tli id="T861" time="589.4158333333332" type="appl" />
         <tli id="T862" time="590.2597800295152" />
         <tli id="T863" time="590.865" type="appl" />
         <tli id="T864" time="591.3571428571429" type="appl" />
         <tli id="T865" time="591.8492857142858" type="appl" />
         <tli id="T866" time="592.3414285714285" type="appl" />
         <tli id="T867" time="592.8335714285714" type="appl" />
         <tli id="T868" time="593.3257142857143" type="appl" />
         <tli id="T869" time="593.8178571428572" type="appl" />
         <tli id="T870" time="594.31" type="appl" />
         <tli id="T871" time="594.8021428571428" type="appl" />
         <tli id="T872" time="595.2942857142857" type="appl" />
         <tli id="T873" time="595.7864285714286" type="appl" />
         <tli id="T874" time="596.2785714285715" type="appl" />
         <tli id="T875" time="596.7707142857142" type="appl" />
         <tli id="T876" time="597.2628571428571" type="appl" />
         <tli id="T877" time="597.755" type="appl" />
         <tli id="T878" time="597.895" type="appl" />
         <tli id="T879" time="598.366625" type="appl" />
         <tli id="T880" time="598.83825" type="appl" />
         <tli id="T881" time="599.309875" type="appl" />
         <tli id="T882" time="599.7815" type="appl" />
         <tli id="T883" time="600.253125" type="appl" />
         <tli id="T884" time="600.72475" type="appl" />
         <tli id="T885" time="601.196375" type="appl" />
         <tli id="T886" time="601.668" type="appl" />
         <tli id="T887" time="602.115" type="appl" />
         <tli id="T888" time="602.8826666666666" type="appl" />
         <tli id="T889" time="603.6503333333334" type="appl" />
         <tli id="T890" time="604.418" type="appl" />
         <tli id="T891" time="605.1856666666666" type="appl" />
         <tli id="T892" time="605.9533333333334" type="appl" />
         <tli id="T893" time="606.721" type="appl" />
         <tli id="T894" time="607.4886666666666" type="appl" />
         <tli id="T895" time="608.2563333333334" type="appl" />
         <tli id="T896" time="609.024" type="appl" />
         <tli id="T897" time="609.7916666666666" type="appl" />
         <tli id="T898" time="610.5593333333334" type="appl" />
         <tli id="T899" time="611.327" type="appl" />
         <tli id="T900" time="612.0946666666666" type="appl" />
         <tli id="T901" time="612.8623333333334" type="appl" />
         <tli id="T902" time="613.63" type="appl" />
         <tli id="T903" time="614.06" type="appl" />
         <tli id="T904" time="614.3679166666666" type="appl" />
         <tli id="T905" time="614.6758333333332" type="appl" />
         <tli id="T906" time="614.98375" type="appl" />
         <tli id="T907" time="615.2916666666666" type="appl" />
         <tli id="T908" time="615.5995833333333" type="appl" />
         <tli id="T909" time="615.9075" type="appl" />
         <tli id="T910" time="616.2154166666667" type="appl" />
         <tli id="T911" time="616.5233333333333" type="appl" />
         <tli id="T912" time="616.83125" type="appl" />
         <tli id="T913" time="617.1391666666666" type="appl" />
         <tli id="T914" time="617.4470833333334" type="appl" />
         <tli id="T915" time="617.755" type="appl" />
         <tli id="T916" time="617.87" type="appl" />
         <tli id="T917" time="618.225" type="appl" />
         <tli id="T918" time="618.23" type="appl" />
         <tli id="T919" time="618.47" type="appl" />
         <tli id="T920" time="618.58" type="appl" />
         <tli id="T921" time="618.71" type="appl" />
         <tli id="T922" time="618.935" type="appl" />
         <tli id="T923" time="618.95" type="appl" />
         <tli id="T924" time="619.19" type="appl" />
         <tli id="T925" time="619.29" type="appl" />
         <tli id="T926" time="619.645" type="appl" />
         <tli id="T927" time="620.0" type="appl" />
         <tli id="T928" time="620.28" type="appl" />
         <tli id="T929" time="620.6025" type="appl" />
         <tli id="T930" time="620.925" type="appl" />
         <tli id="T931" time="621.2475000000001" type="appl" />
         <tli id="T932" time="621.57" type="appl" />
         <tli id="T933" time="621.89" type="appl" />
         <tli id="T934" time="622.5575" type="appl" />
         <tli id="T935" time="623.2249999999999" type="appl" />
         <tli id="T936" time="623.8924999999999" type="appl" />
         <tli id="T937" time="624.56" type="appl" />
         <tli id="T938" time="624.74" type="appl" />
         <tli id="T939" time="625.2084615384615" type="appl" />
         <tli id="T940" time="625.6769230769231" type="appl" />
         <tli id="T941" time="626.1453846153846" type="appl" />
         <tli id="T942" time="626.6138461538462" type="appl" />
         <tli id="T943" time="627.0823076923077" type="appl" />
         <tli id="T944" time="627.5507692307692" type="appl" />
         <tli id="T945" time="628.0192307692308" type="appl" />
         <tli id="T946" time="628.4876923076923" type="appl" />
         <tli id="T947" time="628.9561538461538" type="appl" />
         <tli id="T948" time="629.4246153846154" type="appl" />
         <tli id="T949" time="629.8930769230769" type="appl" />
         <tli id="T950" time="630.3615384615385" type="appl" />
         <tli id="T951" time="630.83" type="appl" />
         <tli id="T952" time="631.3242857142858" type="appl" />
         <tli id="T953" time="631.8" type="appl" />
         <tli id="T954" time="631.8185714285714" type="appl" />
         <tli id="T955" time="632.155" type="appl" />
         <tli id="T956" time="632.3128571428572" type="appl" />
         <tli id="T957" time="632.51" type="appl" />
         <tli id="T958" time="632.8071428571428" type="appl" />
         <tli id="T959" time="632.865" type="appl" />
         <tli id="T960" time="633.22" type="appl" />
         <tli id="T961" time="633.3014285714286" type="appl" />
         <tli id="T962" time="633.7957142857142" type="appl" />
         <tli id="T963" time="634.4797635501758" />
         <tli id="T964" time="635.07" type="appl" />
         <tli id="T965" time="635.4383333333334" type="appl" />
         <tli id="T966" time="635.8066666666667" type="appl" />
         <tli id="T967" time="636.175" type="appl" />
         <tli id="T968" time="636.5433333333333" type="appl" />
         <tli id="T969" time="636.9116666666666" type="appl" />
         <tli id="T970" time="637.28" type="appl" />
         <tli id="T971" time="637.4" type="appl" />
         <tli id="T972" time="637.9442142857142" type="appl" />
         <tli id="T973" time="638.4884285714286" type="appl" />
         <tli id="T974" time="639.0326428571428" type="appl" />
         <tli id="T975" time="639.5768571428571" type="appl" />
         <tli id="T976" time="640.1210714285714" type="appl" />
         <tli id="T977" time="640.6652857142857" type="appl" />
         <tli id="T978" time="641.2094999999999" type="appl" />
         <tli id="T979" time="641.7537142857143" type="appl" />
         <tli id="T980" time="642.2979285714285" type="appl" />
         <tli id="T981" time="642.8421428571429" type="appl" />
         <tli id="T982" time="643.3863571428572" type="appl" />
         <tli id="T983" time="643.9305714285714" type="appl" />
         <tli id="T984" time="644.4747857142858" type="appl" />
         <tli id="T985" time="645.019" type="appl" />
         <tli id="T986" time="645.25" type="appl" />
         <tli id="T987" time="645.7107142857143" type="appl" />
         <tli id="T988" time="646.1714285714286" type="appl" />
         <tli id="T989" time="646.6321428571429" type="appl" />
         <tli id="T990" time="647.0928571428572" type="appl" />
         <tli id="T991" time="647.5535714285714" type="appl" />
         <tli id="T992" time="648.0142857142857" type="appl" />
         <tli id="T993" time="648.475" type="appl" />
         <tli id="T994" time="648.9357142857143" type="appl" />
         <tli id="T995" time="649.3964285714286" type="appl" />
         <tli id="T996" time="649.8571428571429" type="appl" />
         <tli id="T997" time="650.3178571428572" type="appl" />
         <tli id="T998" time="650.7785714285715" type="appl" />
         <tli id="T999" time="651.2392857142858" type="appl" />
         <tli id="T1000" time="651.7" type="appl" />
         <tli id="T1001" time="651.77" type="appl" />
         <tli id="T1002" time="652.5066666666667" type="appl" />
         <tli id="T1003" time="653.2433333333333" type="appl" />
         <tli id="T1004" time="653.98" type="appl" />
         <tli id="T1005" time="653.99" type="appl" />
         <tli id="T1006" time="654.4625" type="appl" />
         <tli id="T1007" time="654.935" type="appl" />
         <tli id="T1008" time="655.4075" type="appl" />
         <tli id="T1009" time="656.1264221498419" />
         <tli id="T1010" time="659.68" type="appl" />
         <tli id="T1911" />
         <tli id="T1912" />
         <tli id="T1011" time="669.02" type="appl" />
         <tli id="T1012" time="669.42" type="appl" />
         <tli id="T1013" time="670.23" type="appl" />
         <tli id="T1014" time="670.37" type="appl" />
         <tli id="T1015" time="670.92" type="appl" />
         <tli id="T1016" time="671.2059999999999" type="appl" />
         <tli id="T1017" time="671.492" type="appl" />
         <tli id="T1018" time="671.778" type="appl" />
         <tli id="T1019" time="672.064" type="appl" />
         <tli id="T1020" time="672.3499999999999" type="appl" />
         <tli id="T1021" time="672.636" type="appl" />
         <tli id="T1022" time="672.922" type="appl" />
         <tli id="T1023" time="673.208" type="appl" />
         <tli id="T1024" time="673.4939999999999" type="appl" />
         <tli id="T1025" time="673.78" type="appl" />
         <tli id="T1026" time="674.8" type="appl" />
         <tli id="T1027" time="675.2025" type="appl" />
         <tli id="T1028" time="675.605" type="appl" />
         <tli id="T1029" time="675.7" type="appl" />
         <tli id="T1030" time="675.98" type="appl" />
         <tli id="T1031" time="676.0074999999999" type="appl" />
         <tli id="T1032" time="676.26" type="appl" />
         <tli id="T1033" time="676.41" type="appl" />
         <tli id="T1034" time="676.5400000000001" type="appl" />
         <tli id="T1035" time="676.74" type="appl" />
         <tli id="T1036" time="676.82" type="appl" />
         <tli id="T1037" time="677.1" type="appl" />
         <tli id="T1038" time="677.3" type="appl" />
         <tli id="T1039" time="677.38" type="appl" />
         <tli id="T1040" time="677.66" type="appl" />
         <tli id="T1041" time="677.86" type="appl" />
         <tli id="T1042" time="677.94" type="appl" />
         <tli id="T1043" time="678.22" type="appl" />
         <tli id="T1044" time="678.42" type="appl" />
         <tli id="T1045" time="678.5" type="appl" />
         <tli id="T1046" time="678.98" type="appl" />
         <tli id="T1047" time="679.54" type="appl" />
         <tli id="T1048" time="679.62" type="appl" />
         <tli id="T1049" time="680.1" type="appl" />
         <tli id="T1050" time="680.45" type="appl" />
         <tli id="T1051" time="680.5799999999999" type="appl" />
         <tli id="T1052" time="680.8566666666667" type="appl" />
         <tli id="T1053" time="681.06" type="appl" />
         <tli id="T1054" time="681.2633333333333" type="appl" />
         <tli id="T1055" time="681.6700000000001" type="appl" />
         <tli id="T1056" time="682.0766666666667" type="appl" />
         <tli id="T1057" time="682.4833333333333" type="appl" />
         <tli id="T1058" time="682.89" type="appl" />
         <tli id="T1059" time="683.2966666666666" type="appl" />
         <tli id="T1060" time="683.7033333333334" type="appl" />
         <tli id="T1061" time="684.11" type="appl" />
         <tli id="T1062" time="684.5166666666667" type="appl" />
         <tli id="T1063" time="684.9233333333333" type="appl" />
         <tli id="T1064" time="685.3299999999999" type="appl" />
         <tli id="T1065" time="685.7366666666667" type="appl" />
         <tli id="T1066" time="686.1433333333333" type="appl" />
         <tli id="T1067" time="686.55" type="appl" />
         <tli id="T1068" time="687.2733333333333" type="appl" />
         <tli id="T1069" time="687.9966666666667" type="appl" />
         <tli id="T1070" time="688.72" type="appl" />
         <tli id="T1071" time="688.8" type="appl" />
         <tli id="T1072" time="689.1828571428571" type="appl" />
         <tli id="T1073" time="689.5657142857142" type="appl" />
         <tli id="T1074" time="689.9485714285714" type="appl" />
         <tli id="T1075" time="690.3314285714285" type="appl" />
         <tli id="T1076" time="690.7142857142857" type="appl" />
         <tli id="T1077" time="691.0971428571428" type="appl" />
         <tli id="T1078" time="691.48" type="appl" />
         <tli id="T1079" time="691.8628571428571" type="appl" />
         <tli id="T1080" time="692.2457142857143" type="appl" />
         <tli id="T1081" time="692.6285714285714" type="appl" />
         <tli id="T1082" time="693.0114285714286" type="appl" />
         <tli id="T1083" time="693.3942857142857" type="appl" />
         <tli id="T1084" time="693.7771428571428" type="appl" />
         <tli id="T1085" time="694.16" type="appl" />
         <tli id="T1086" time="694.5428571428572" type="appl" />
         <tli id="T1087" time="694.9257142857143" type="appl" />
         <tli id="T1088" time="695.3085714285714" type="appl" />
         <tli id="T1089" time="695.6914285714286" type="appl" />
         <tli id="T1090" time="696.0742857142858" type="appl" />
         <tli id="T1091" time="696.4571428571429" type="appl" />
         <tli id="T1092" time="696.859740303202" />
         <tli id="T1093" time="696.92" type="appl" />
         <tli id="T1094" time="697.27" type="appl" />
         <tli id="T1095" time="697.62" type="appl" />
         <tli id="T1096" time="697.97" type="appl" />
         <tli id="T1097" time="698.32" type="appl" />
         <tli id="T1098" time="698.98" type="appl" />
         <tli id="T1099" time="699.6864059164628" />
         <tli id="T1100" time="699.9" type="appl" />
         <tli id="T1101" time="700.26" type="appl" />
         <tli id="T1102" time="700.62" type="appl" />
         <tli id="T1103" time="700.98" type="appl" />
         <tli id="T1104" time="701.3399999999999" type="appl" />
         <tli id="T1105" time="701.6999999999999" type="appl" />
         <tli id="T1106" time="702.2797382833463" />
         <tli id="T1107" time="702.39" type="appl" />
         <tli id="T1108" time="702.7587" type="appl" />
         <tli id="T1109" time="703.1274" type="appl" />
         <tli id="T1110" time="703.4961" type="appl" />
         <tli id="T1111" time="703.8648" type="appl" />
         <tli id="T1112" time="704.2335" type="appl" />
         <tli id="T1113" time="704.6022" type="appl" />
         <tli id="T1114" time="704.9709" type="appl" />
         <tli id="T1115" time="705.3396" type="appl" />
         <tli id="T1116" time="705.7083" type="appl" />
         <tli id="T1117" time="706.1864034941268" />
         <tli id="T1118" time="707.48" type="appl" />
         <tli id="T1119" time="707.87" type="appl" />
         <tli id="T1120" time="708.18" type="appl" />
         <tli id="T1121" time="708.26" type="appl" />
         <tli id="T1122" time="708.3974999999999" type="appl" />
         <tli id="T1123" time="708.615" type="appl" />
         <tli id="T1124" time="708.65" type="appl" />
         <tli id="T1125" time="708.8325" type="appl" />
         <tli id="T1126" time="709.04" type="appl" />
         <tli id="T1127" time="709.05" type="appl" />
         <tli id="T1128" time="709.2674999999999" type="appl" />
         <tli id="T1129" time="709.4849999999999" type="appl" />
         <tli id="T1130" time="709.5466666666666" type="appl" />
         <tli id="T1131" time="709.7025" type="appl" />
         <tli id="T1132" time="709.92" type="appl" />
         <tli id="T1133" time="710.0533333333333" type="appl" />
         <tli id="T1134" time="710.2385714285714" type="appl" />
         <tli id="T1135" time="710.5571428571428" type="appl" />
         <tli id="T1136" time="710.56" type="appl" />
         <tli id="T1137" time="710.8757142857143" type="appl" />
         <tli id="T1138" time="711.1942857142857" type="appl" />
         <tli id="T1139" time="711.5128571428571" type="appl" />
         <tli id="T1140" time="711.8314285714285" type="appl" />
         <tli id="T1141" time="712.15" type="appl" />
         <tli id="T1142" time="712.21" type="appl" />
         <tli id="T1143" time="712.527" type="appl" />
         <tli id="T1144" time="712.844" type="appl" />
         <tli id="T1145" time="713.1610000000001" type="appl" />
         <tli id="T1146" time="713.4780000000001" type="appl" />
         <tli id="T1147" time="713.7950000000001" type="appl" />
         <tli id="T1148" time="714.112" type="appl" />
         <tli id="T1149" time="714.429" type="appl" />
         <tli id="T1150" time="714.746" type="appl" />
         <tli id="T1151" time="715.063" type="appl" />
         <tli id="T1152" time="715.38" type="appl" />
         <tli id="T1153" time="715.41" type="appl" />
         <tli id="T1154" time="715.76125" type="appl" />
         <tli id="T1155" time="716.1125" type="appl" />
         <tli id="T1156" time="716.46375" type="appl" />
         <tli id="T1157" time="716.815" type="appl" />
         <tli id="T1158" time="717.16625" type="appl" />
         <tli id="T1159" time="717.5175" type="appl" />
         <tli id="T1160" time="717.86875" type="appl" />
         <tli id="T1161" time="718.22" type="appl" />
         <tli id="T1162" time="718.4" type="appl" />
         <tli id="T1163" time="718.728" type="appl" />
         <tli id="T1164" time="719.0559999999999" type="appl" />
         <tli id="T1165" time="719.384" type="appl" />
         <tli id="T1166" time="719.712" type="appl" />
         <tli id="T1167" time="720.04" type="appl" />
         <tli id="T1168" time="720.3679999999999" type="appl" />
         <tli id="T1169" time="720.6959999999999" type="appl" />
         <tli id="T1170" time="721.024" type="appl" />
         <tli id="T1171" time="721.352" type="appl" />
         <tli id="T1172" time="721.68" type="appl" />
         <tli id="T1173" time="723.83" type="appl" />
         <tli id="T1174" time="724.174705882353" type="appl" />
         <tli id="T1175" time="724.519411764706" type="appl" />
         <tli id="T1176" time="724.8641176470588" type="appl" />
         <tli id="T1177" time="725.2088235294118" type="appl" />
         <tli id="T1178" time="725.5535294117648" type="appl" />
         <tli id="T1179" time="725.8982352941177" type="appl" />
         <tli id="T1180" time="726.2429411764706" type="appl" />
         <tli id="T1181" time="726.5876470588236" type="appl" />
         <tli id="T1182" time="726.9323529411765" type="appl" />
         <tli id="T1183" time="727.2770588235295" type="appl" />
         <tli id="T1184" time="727.6217647058824" type="appl" />
         <tli id="T1185" time="727.9664705882353" type="appl" />
         <tli id="T1186" time="728.3111764705883" type="appl" />
         <tli id="T1187" time="728.6558823529413" type="appl" />
         <tli id="T1188" time="729.0005882352941" type="appl" />
         <tli id="T1189" time="729.3452941176471" type="appl" />
         <tli id="T1190" time="729.54" type="appl" />
         <tli id="T1191" time="729.69" type="appl" />
         <tli id="T1192" time="729.8333333333333" type="appl" />
         <tli id="T1193" time="730.1266666666667" type="appl" />
         <tli id="T1194" time="730.9530609310715" />
         <tli id="T1195" time="731.1" type="appl" />
         <tli id="T1196" time="731.4449999999999" type="appl" />
         <tli id="T1197" time="732.2863937675154" />
         <tli id="T1198" time="732.67" type="appl" />
         <tli id="T1199" time="733.15" type="appl" />
         <tli id="T1200" time="733.63" type="appl" />
         <tli id="T1201" time="733.9159999999999" type="appl" />
         <tli id="T1202" time="734.202" type="appl" />
         <tli id="T1203" time="734.4879999999999" type="appl" />
         <tli id="T1204" time="734.774" type="appl" />
         <tli id="T1205" time="735.06" type="appl" />
         <tli id="T1206" time="735.12" type="appl" />
         <tli id="T1207" time="735.4275" type="appl" />
         <tli id="T1208" time="735.735" type="appl" />
         <tli id="T1209" time="736.0425" type="appl" />
         <tli id="T1210" time="736.35" type="appl" />
         <tli id="T1211" time="736.6575" type="appl" />
         <tli id="T1212" time="736.9649999999999" type="appl" />
         <tli id="T1213" time="737.2724999999999" type="appl" />
         <tli id="T1214" time="737.5799999999999" type="appl" />
         <tli id="T1215" time="737.8874999999999" type="appl" />
         <tli id="T1216" time="738.1949999999999" type="appl" />
         <tli id="T1217" time="738.5024999999999" type="appl" />
         <tli id="T1218" time="738.8166517505688" />
         <tli id="T1219" time="742.25" type="appl" />
         <tli id="T1220" time="743.0387499999999" type="appl" />
         <tli id="T1221" time="743.8275" type="appl" />
         <tli id="T1222" time="744.61625" type="appl" />
         <tli id="T1223" time="745.405" type="appl" />
         <tli id="T1224" time="746.1937499999999" type="appl" />
         <tli id="T1225" time="746.9825" type="appl" />
         <tli id="T1226" time="747.77125" type="appl" />
         <tli id="T1227" time="748.56" type="appl" />
         <tli id="T1228" time="749.27" type="appl" />
         <tli id="T1229" time="749.872" type="appl" />
         <tli id="T1230" time="750.4739999999999" type="appl" />
         <tli id="T1231" time="751.076" type="appl" />
         <tli id="T1232" time="751.678" type="appl" />
         <tli id="T1233" time="752.28" type="appl" />
         <tli id="T1234" time="752.36" type="appl" />
         <tli id="T1235" time="752.9025" type="appl" />
         <tli id="T1236" time="753.4449999999999" type="appl" />
         <tli id="T1237" time="753.9875" type="appl" />
         <tli id="T1238" time="754.8530520243277" />
         <tli id="T1239" time="756.86" type="appl" />
         <tli id="T1240" time="757.359" type="appl" />
         <tli id="T1241" time="757.8580000000001" type="appl" />
         <tli id="T1242" time="758.357" type="appl" />
         <tli id="T1243" time="758.856" type="appl" />
         <tli id="T1244" time="759.355" type="appl" />
         <tli id="T1245" time="759.854" type="appl" />
         <tli id="T1246" time="760.3530000000001" type="appl" />
         <tli id="T1247" time="760.852" type="appl" />
         <tli id="T1248" time="761.351" type="appl" />
         <tli id="T1249" time="761.85" type="appl" />
         <tli id="T1250" time="762.22" type="appl" />
         <tli id="T1251" time="763.3063822073817" />
         <tli id="T1252" time="763.8197153494126" />
         <tli id="T1253" time="764.72" type="appl" />
         <tli id="T1254" time="766.2" type="appl" />
         <tli id="T1255" time="766.7" type="appl" />
         <tli id="T1256" time="766.73" type="appl" />
         <tli id="T1257" time="767.26" type="appl" />
         <tli id="T1258" time="767.95" type="appl" />
         <tli id="T1259" time="768.5" type="appl" />
         <tli id="T1260" time="769.0500000000001" type="appl" />
         <tli id="T1261" time="769.6" type="appl" />
         <tli id="T1262" time="770.0605" type="appl" />
         <tli id="T1263" time="770.521" type="appl" />
         <tli id="T1264" time="770.9815" type="appl" />
         <tli id="T1265" time="771.6197124426094" />
         <tli id="T1266" time="780.96" type="appl" />
         <tli id="T1267" time="782.97" type="appl" />
         <tli id="T1268" time="783.38" type="appl" />
         <tli id="T1269" time="783.7900000000001" type="appl" />
         <tli id="T1270" time="784.2" type="appl" />
         <tli id="T1271" time="784.61" type="appl" />
         <tli id="T1272" time="785.02" type="appl" />
         <tli id="T1273" time="785.4300000000001" type="appl" />
         <tli id="T1274" time="786.0800195537264" />
         <tli id="T1275" time="786.0833528858175" />
         <tli id="T1276" time="786.3299999999999" type="appl" />
         <tli id="T1277" time="786.7299999999999" type="appl" />
         <tli id="T1278" time="787.13" type="appl" />
         <tli id="T1279" time="787.53" type="appl" />
         <tli id="T1280" time="788.4566853346877" />
         <tli id="T1281" time="789.2633517007362" />
         <tli id="T1282" time="789.5020000000001" type="appl" />
         <tli id="T1283" time="789.774" type="appl" />
         <tli id="T1284" time="790.046" type="appl" />
         <tli id="T1285" time="790.318" type="appl" />
         <tli id="T1286" time="790.6500178506379" />
         <tli id="T1287" time="790.65668451482" />
         <tli id="T1288" time="790.8777777777777" type="appl" />
         <tli id="T1289" time="791.1455555555556" type="appl" />
         <tli id="T1290" time="791.4133333333333" type="appl" />
         <tli id="T1291" time="791.6811111111111" type="appl" />
         <tli id="T1292" time="791.9488888888889" type="appl" />
         <tli id="T1293" time="792.2166666666667" type="appl" />
         <tli id="T1294" time="792.4844444444444" type="appl" />
         <tli id="T1295" time="792.59" type="appl" />
         <tli id="T1296" time="792.7522222222223" type="appl" />
         <tli id="T1297" time="793.02" type="appl" />
         <tli id="T1298" time="793.0350000000001" type="appl" />
         <tli id="T1299" time="793.48" type="appl" />
         <tli id="T1300" time="793.58" type="appl" />
         <tli id="T1301" time="793.8666666666667" type="appl" />
         <tli id="T1302" time="793.925" type="appl" />
         <tli id="T1303" time="794.1533333333333" type="appl" />
         <tli id="T1304" time="794.37" type="appl" />
         <tli id="T1305" time="794.44" type="appl" />
         <tli id="T1306" time="794.7266666666667" type="appl" />
         <tli id="T1307" time="795.0133333333333" type="appl" />
         <tli id="T1308" time="795.3" type="appl" />
         <tli id="T1309" time="795.706" type="appl" />
         <tli id="T1310" time="796.112" type="appl" />
         <tli id="T1311" time="796.41" type="appl" />
         <tli id="T1312" time="796.518" type="appl" />
         <tli id="T1313" time="796.6999999999999" type="appl" />
         <tli id="T1314" time="796.924" type="appl" />
         <tli id="T1315" time="796.99" type="appl" />
         <tli id="T1316" time="797.28" type="appl" />
         <tli id="T1317" time="797.33" type="appl" />
         <tli id="T1318" time="797.5699999999999" type="appl" />
         <tli id="T1319" time="797.86" type="appl" />
         <tli id="T1320" time="798.15" type="appl" />
         <tli id="T1321" time="798.4399999999999" type="appl" />
         <tli id="T1322" time="798.73" type="appl" />
         <tli id="T1323" time="798.86" type="appl" />
         <tli id="T1324" time="799.02" type="appl" />
         <tli id="T1325" time="799.45" type="appl" />
         <tli id="T1326" time="799.875" type="appl" />
         <tli id="T1327" time="800.3" type="appl" />
         <tli id="T1328" time="800.725" type="appl" />
         <tli id="T1329" time="801.15" type="appl" />
         <tli id="T1330" time="818.67" type="appl" />
         <tli id="T1331" time="818.88" type="appl" />
         <tli id="T1913" />
         <tli id="T1914" />
         <tli id="T1332" time="837.51" type="appl" />
         <tli id="T1333" time="837.95" type="appl" />
         <tli id="T1334" time="838.2537500000001" type="appl" />
         <tli id="T1335" time="838.5575" type="appl" />
         <tli id="T1336" time="838.86125" type="appl" />
         <tli id="T1337" time="839.165" type="appl" />
         <tli id="T1338" time="839.46875" type="appl" />
         <tli id="T1339" time="839.7725" type="appl" />
         <tli id="T1340" time="840.07625" type="appl" />
         <tli id="T1341" time="840.38" type="appl" />
         <tli id="T1342" time="840.49" type="appl" />
         <tli id="T1343" time="840.8128571428572" type="appl" />
         <tli id="T1344" time="841.1357142857142" type="appl" />
         <tli id="T1345" time="841.4585714285714" type="appl" />
         <tli id="T1346" time="841.7814285714286" type="appl" />
         <tli id="T1347" time="842.1042857142858" type="appl" />
         <tli id="T1348" time="842.4271428571428" type="appl" />
         <tli id="T1349" time="842.75" type="appl" />
         <tli id="T1350" time="842.76" type="appl" />
         <tli id="T1351" time="843.1517647058823" type="appl" />
         <tli id="T1352" time="843.5435294117647" type="appl" />
         <tli id="T1353" time="843.935294117647" type="appl" />
         <tli id="T1354" time="844.3270588235293" type="appl" />
         <tli id="T1355" time="844.7188235294118" type="appl" />
         <tli id="T1356" time="845.1105882352941" type="appl" />
         <tli id="T1357" time="845.5023529411765" type="appl" />
         <tli id="T1358" time="845.8941176470588" type="appl" />
         <tli id="T1359" time="846.2858823529411" type="appl" />
         <tli id="T1360" time="846.6776470588235" type="appl" />
         <tli id="T1361" time="847.0694117647058" type="appl" />
         <tli id="T1362" time="847.4611764705882" type="appl" />
         <tli id="T1363" time="847.8529411764706" type="appl" />
         <tli id="T1364" time="848.244705882353" type="appl" />
         <tli id="T1365" time="848.6364705882353" type="appl" />
         <tli id="T1366" time="849.0282352941176" type="appl" />
         <tli id="T1367" time="850.0530165464194" />
         <tli id="T1368" time="850.4396830689882" />
         <tli id="T1369" time="850.944" type="appl" />
         <tli id="T1370" time="851.3679999999999" type="appl" />
         <tli id="T1371" time="851.792" type="appl" />
         <tli id="T1372" time="852.216" type="appl" />
         <tli id="T1373" time="852.64" type="appl" />
         <tli id="T1374" time="852.65" type="appl" />
         <tli id="T1375" time="853.34" type="appl" />
         <tli id="T1376" time="853.53" type="appl" />
         <tli id="T1377" time="855.56" type="appl" />
         <tli id="T1378" time="855.9899999999999" type="appl" />
         <tli id="T1379" time="856.42" type="appl" />
         <tli id="T1380" time="856.8499999999999" type="appl" />
         <tli id="T1381" time="857.28" type="appl" />
         <tli id="T1382" time="857.7099999999999" type="appl" />
         <tli id="T1383" time="858.14" type="appl" />
         <tli id="T1384" time="858.22" type="appl" />
         <tli id="T1385" time="858.5699999999999" type="appl" />
         <tli id="T1386" time="859.0" type="appl" />
         <tli id="T1387" time="859.43" type="appl" />
         <tli id="T1388" time="859.86" type="appl" />
         <tli id="T1389" time="860.4796793274104" />
         <tli id="T1390" time="861.238" type="appl" />
         <tli id="T1391" time="861.5936250000001" type="appl" />
         <tli id="T1392" time="861.94925" type="appl" />
         <tli id="T1393" time="862.304875" type="appl" />
         <tli id="T1394" time="862.6605000000001" type="appl" />
         <tli id="T1395" time="863.016125" type="appl" />
         <tli id="T1396" time="863.37175" type="appl" />
         <tli id="T1397" time="863.727375" type="appl" />
         <tli id="T1398" time="864.0830000000001" type="appl" />
         <tli id="T1399" time="864.438625" type="appl" />
         <tli id="T1400" time="864.79425" type="appl" />
         <tli id="T1401" time="865.1498750000001" type="appl" />
         <tli id="T1402" time="865.5055" type="appl" />
         <tli id="T1403" time="865.861125" type="appl" />
         <tli id="T1404" time="866.21675" type="appl" />
         <tli id="T1405" time="866.572375" type="appl" />
         <tli id="T1406" time="867.1730101663586" />
         <tli id="T1407" time="867.658" type="appl" />
         <tli id="T1408" time="868.089" type="appl" />
         <tli id="T1409" time="868.52" type="appl" />
         <tli id="T1410" time="868.951" type="appl" />
         <tli id="T1411" time="869.382" type="appl" />
         <tli id="T1412" time="869.813" type="appl" />
         <tli id="T1413" time="870.244" type="appl" />
         <tli id="T1414" time="870.675" type="appl" />
         <tli id="T1415" time="871.106" type="appl" />
         <tli id="T1416" time="871.5369999999999" type="appl" />
         <tli id="T1417" time="872.0463416835609" />
         <tli id="T1418" time="872.578" type="appl" />
         <tli id="T1419" time="873.0129999999999" type="appl" />
         <tli id="T1420" time="873.448" type="appl" />
         <tli id="T1421" time="873.883" type="appl" />
         <tli id="T1422" time="874.318" type="appl" />
         <tli id="T1423" time="874.7529999999999" type="appl" />
         <tli id="T1424" time="875.188" type="appl" />
         <tli id="T1425" time="875.623" type="appl" />
         <tli id="T1426" time="876.058" type="appl" />
         <tli id="T1427" time="876.4929999999999" type="appl" />
         <tli id="T1428" time="876.928" type="appl" />
         <tli id="T1429" time="877.558" type="appl" />
         <tli id="T1430" time="878.4130059775803" />
         <tli id="T1431" time="878.5" type="appl" />
         <tli id="T1432" time="878.7723076923077" type="appl" />
         <tli id="T1433" time="879.0446153846153" type="appl" />
         <tli id="T1434" time="879.3169230769231" type="appl" />
         <tli id="T1435" time="879.5892307692308" type="appl" />
         <tli id="T1436" time="879.8615384615384" type="appl" />
         <tli id="T1437" time="880.1338461538461" type="appl" />
         <tli id="T1438" time="880.4061538461539" type="appl" />
         <tli id="T1439" time="880.6784615384615" type="appl" />
         <tli id="T1440" time="880.9507692307692" type="appl" />
         <tli id="T1441" time="881.2230769230769" type="appl" />
         <tli id="T1442" time="881.4953846153846" type="appl" />
         <tli id="T1443" time="881.7676923076923" type="appl" />
         <tli id="T1444" time="881.98" type="appl" />
         <tli id="T1445" time="882.04" type="appl" />
         <tli id="T1446" time="882.2675" type="appl" />
         <tli id="T1447" time="882.5550000000001" type="appl" />
         <tli id="T1448" time="882.8425" type="appl" />
         <tli id="T1449" time="883.3130041515116" />
         <tli id="T1450" time="883.3166499834863" />
         <tli id="T1451" time="883.97" type="appl" />
         <tli id="T1452" time="884.65" type="appl" />
         <tli id="T1453" time="884.86" type="appl" />
         <tli id="T1454" time="885.29" type="appl" />
         <tli id="T1455" time="885.72" type="appl" />
         <tli id="T1456" time="886.088" type="appl" />
         <tli id="T1457" time="886.15" type="appl" />
         <tli id="T1458" time="886.58" type="appl" />
         <tli id="T1459" time="886.768" type="appl" />
         <tli id="T1460" time="887.01" type="appl" />
         <tli id="T1461" time="888.77" type="appl" />
         <tli id="T1462" time="889.64" type="appl" />
         <tli id="T1463" time="890.2196682442908" />
         <tli id="T1464" time="890.4796681473972" />
         <tli id="T1465" time="890.568" type="appl" />
         <tli id="T1466" time="890.63" type="appl" />
         <tli id="T1467" time="891.0219999999999" type="appl" />
         <tli id="T1468" time="891.476" type="appl" />
         <tli id="T1469" time="891.9300000000001" type="appl" />
         <tli id="T1470" time="892.384" type="appl" />
         <tli id="T1471" time="892.838" type="appl" />
         <tli id="T1472" time="893.292" type="appl" />
         <tli id="T1473" time="893.7460000000001" type="appl" />
         <tli id="T1474" time="894.2" type="appl" />
         <tli id="T1475" time="894.5125" type="appl" />
         <tli id="T1476" time="894.55" type="appl" />
         <tli id="T1477" time="894.825" type="appl" />
         <tli id="T1478" time="895.1375" type="appl" />
         <tli id="T1479" time="895.29" type="appl" />
         <tli id="T1480" time="895.45" type="appl" />
         <tli id="T1481" time="895.7625" type="appl" />
         <tli id="T1482" time="896.03" type="appl" />
         <tli id="T1483" time="896.075" type="appl" />
         <tli id="T1484" time="896.3875" type="appl" />
         <tli id="T1485" time="896.7" type="appl" />
         <tli id="T1486" time="896.72" type="appl" />
         <tli id="T1487" time="897.0840000000001" type="appl" />
         <tli id="T1488" time="897.448" type="appl" />
         <tli id="T1489" time="897.812" type="appl" />
         <tli id="T1490" time="898.176" type="appl" />
         <tli id="T1491" time="898.54" type="appl" />
         <tli id="T1492" time="898.904" type="appl" />
         <tli id="T1493" time="899.268" type="appl" />
         <tli id="T1494" time="899.6320000000001" type="appl" />
         <tli id="T1495" time="899.996" type="appl" />
         <tli id="T1496" time="900.36" type="appl" />
         <tli id="T1497" time="900.724" type="appl" />
         <tli id="T1498" time="901.088" type="appl" />
         <tli id="T1499" time="901.452" type="appl" />
         <tli id="T1500" time="901.816" type="appl" />
         <tli id="T1501" time="902.1800000000001" type="appl" />
         <tli id="T1502" time="902.544" type="appl" />
         <tli id="T1503" time="902.908" type="appl" />
         <tli id="T1504" time="903.272" type="appl" />
         <tli id="T1505" time="903.636" type="appl" />
         <tli id="T1506" time="904.0" type="appl" />
         <tli id="T1507" time="904.04" type="appl" />
         <tli id="T1508" time="904.3405263157895" type="appl" />
         <tli id="T1509" time="904.6410526315789" type="appl" />
         <tli id="T1510" time="904.9415789473684" type="appl" />
         <tli id="T1511" time="905.2421052631579" type="appl" />
         <tli id="T1512" time="905.5426315789473" type="appl" />
         <tli id="T1513" time="905.8431578947368" type="appl" />
         <tli id="T1514" time="906.1436842105263" type="appl" />
         <tli id="T1515" time="906.4442105263157" type="appl" />
         <tli id="T1516" time="906.7447368421052" type="appl" />
         <tli id="T1517" time="907.0452631578947" type="appl" />
         <tli id="T1518" time="907.3457894736842" type="appl" />
         <tli id="T1519" time="907.6463157894736" type="appl" />
         <tli id="T1520" time="907.9468421052632" type="appl" />
         <tli id="T1521" time="908.2473684210527" type="appl" />
         <tli id="T1522" time="908.5478947368421" type="appl" />
         <tli id="T1523" time="908.8484210526316" type="appl" />
         <tli id="T1524" time="909.1489473684211" type="appl" />
         <tli id="T1525" time="909.4494736842105" type="appl" />
         <tli id="T1526" time="909.75" type="appl" />
         <tli id="T1527" time="910.33" type="appl" />
         <tli id="T1528" time="910.8280000000001" type="appl" />
         <tli id="T1529" time="911.326" type="appl" />
         <tli id="T1530" time="911.8240000000001" type="appl" />
         <tli id="T1531" time="912.322" type="appl" />
         <tli id="T1532" time="912.7529931801921" />
         <tli id="T1533" time="913.74" type="appl" />
         <tli id="T1534" time="914.224" type="appl" />
         <tli id="T1535" time="914.708" type="appl" />
         <tli id="T1536" time="915.192" type="appl" />
         <tli id="T1537" time="915.676" type="appl" />
         <tli id="T1538" time="916.1600000000001" type="appl" />
         <tli id="T1539" time="916.644" type="appl" />
         <tli id="T1540" time="917.128" type="appl" />
         <tli id="T1541" time="917.6120000000001" type="appl" />
         <tli id="T1542" time="918.096" type="appl" />
         <tli id="T1543" time="918.58" type="appl" />
         <tli id="T1544" time="919.12" type="appl" />
         <tli id="T1545" time="919.4000666666667" type="appl" />
         <tli id="T1546" time="919.6801333333333" type="appl" />
         <tli id="T1547" time="919.9602" type="appl" />
         <tli id="T1548" time="920.2402666666667" type="appl" />
         <tli id="T1549" time="920.5203333333334" type="appl" />
         <tli id="T1550" time="920.8004" type="appl" />
         <tli id="T1551" time="921.0804666666667" type="appl" />
         <tli id="T1552" time="921.3605333333334" type="appl" />
         <tli id="T1553" time="921.6406000000001" type="appl" />
         <tli id="T1554" time="921.9206666666666" type="appl" />
         <tli id="T1555" time="922.2007333333333" type="appl" />
         <tli id="T1556" time="922.4808" type="appl" />
         <tli id="T1557" time="922.7608666666667" type="appl" />
         <tli id="T1558" time="923.0409333333333" type="appl" />
         <tli id="T1559" time="923.321" type="appl" />
         <tli id="T1560" time="923.42" type="appl" />
         <tli id="T1561" time="923.7918181818181" type="appl" />
         <tli id="T1562" time="924.1636363636363" type="appl" />
         <tli id="T1563" time="924.5354545454545" type="appl" />
         <tli id="T1564" time="924.9072727272727" type="appl" />
         <tli id="T1565" time="925.2790909090909" type="appl" />
         <tli id="T1566" time="925.6509090909091" type="appl" />
         <tli id="T1567" time="926.0227272727273" type="appl" />
         <tli id="T1568" time="926.3945454545454" type="appl" />
         <tli id="T1569" time="926.7663636363636" type="appl" />
         <tli id="T1570" time="927.1381818181818" type="appl" />
         <tli id="T1571" time="927.51" type="appl" />
         <tli id="T1572" time="927.78" type="appl" />
         <tli id="T1573" time="928.18" type="appl" />
         <tli id="T1574" time="928.5799999999999" type="appl" />
         <tli id="T1575" time="928.98" type="appl" />
         <tli id="T1576" time="929.38" type="appl" />
         <tli id="T1577" time="929.78" type="appl" />
         <tli id="T1578" time="930.18" type="appl" />
         <tli id="T1579" time="930.5413636363636" type="appl" />
         <tli id="T1580" time="930.9027272727272" type="appl" />
         <tli id="T1581" time="931.2640909090909" type="appl" />
         <tli id="T1582" time="931.6254545454545" type="appl" />
         <tli id="T1583" time="931.9868181818182" type="appl" />
         <tli id="T1584" time="932.3481818181817" type="appl" />
         <tli id="T1585" time="932.7095454545454" type="appl" />
         <tli id="T1586" time="933.070909090909" type="appl" />
         <tli id="T1587" time="933.4322727272727" type="appl" />
         <tli id="T1588" time="933.7936363636363" type="appl" />
         <tli id="T1589" time="934.155" type="appl" />
         <tli id="T1590" time="934.5163636363636" type="appl" />
         <tli id="T1591" time="934.8777272727273" type="appl" />
         <tli id="T1592" time="935.2390909090909" type="appl" />
         <tli id="T1593" time="935.6004545454546" type="appl" />
         <tli id="T1594" time="935.9618181818182" type="appl" />
         <tli id="T1595" time="936.3231818181818" type="appl" />
         <tli id="T1596" time="936.6845454545454" type="appl" />
         <tli id="T1597" time="937.045909090909" type="appl" />
         <tli id="T1598" time="937.4072727272727" type="appl" />
         <tli id="T1599" time="937.7686363636363" type="appl" />
         <tli id="T1600" time="938.13" type="appl" />
         <tli id="T1601" time="939.9" type="appl" />
         <tli id="T1602" time="940.4200000000001" type="appl" />
         <tli id="T1603" time="941.0396493053485" />
         <tli id="T1604" time="941.16" type="appl" />
         <tli id="T1605" time="941.8199999999999" type="appl" />
         <tli id="T1606" time="942.48" type="appl" />
         <tli id="T1607" time="943.14" type="appl" />
         <tli id="T1608" time="943.15" type="appl" />
         <tli id="T1609" time="943.66" type="appl" />
         <tli id="T1610" time="944.17" type="appl" />
         <tli id="T1611" time="944.68" type="appl" />
         <tli id="T1612" time="945.16" type="appl" />
         <tli id="T1613" time="945.6827272727272" type="appl" />
         <tli id="T1614" time="946.2054545454545" type="appl" />
         <tli id="T1615" time="946.7281818181818" type="appl" />
         <tli id="T1616" time="947.2509090909091" type="appl" />
         <tli id="T1617" time="947.7736363636363" type="appl" />
         <tli id="T1618" time="948.2963636363636" type="appl" />
         <tli id="T1619" time="948.8190909090908" type="appl" />
         <tli id="T1620" time="949.04" type="appl" />
         <tli id="T1621" time="949.3418181818181" type="appl" />
         <tli id="T1622" time="949.6066666666667" type="appl" />
         <tli id="T1623" time="949.8645454545455" type="appl" />
         <tli id="T1624" time="950.1733333333333" type="appl" />
         <tli id="T1625" time="950.3872727272727" type="appl" />
         <tli id="T1626" time="950.7863123397532" />
         <tli id="T1627" time="950.8232914926389" />
         <tli id="T1628" time="950.8299581568211" />
         <tli id="T1629" time="951.4333333333334" type="appl" />
         <tli id="T1630" time="951.9166666666666" type="appl" />
         <tli id="T1631" time="952.4" type="appl" />
         <tli id="T1632" time="952.6432908143847" />
         <tli id="T1633" time="953.1999572736" />
         <tli id="T1634" time="953.74" type="appl" />
         <tli id="T1635" time="954.215" type="appl" />
         <tli id="T1636" time="954.69" type="appl" />
         <tli id="T1637" time="955.02" type="appl" />
         <tli id="T1638" time="955.165" type="appl" />
         <tli id="T1639" time="955.324" type="appl" />
         <tli id="T1640" time="955.6279999999999" type="appl" />
         <tli id="T1641" time="955.69328967775" />
         <tli id="T1642" time="955.932" type="appl" />
         <tli id="T1643" time="956.2796436259019" />
         <tli id="T1644" time="956.3996435811819" />
         <tli id="T1645" time="956.4863102155507" />
         <tli id="T1646" time="956.6263101633773" />
         <tli id="T1647" time="957.0742857142857" type="appl" />
         <tli id="T1648" time="957.6014285714285" type="appl" />
         <tli id="T1649" time="958.1285714285715" type="appl" />
         <tli id="T1650" time="958.6557142857143" type="appl" />
         <tli id="T1651" time="959.1828571428572" type="appl" />
         <tli id="T1652" time="959.8032881460882" />
         <tli id="T1653" time="959.8099548102706" />
         <tli id="T1654" time="960.2458571428572" type="appl" />
         <tli id="T1655" time="960.7417142857142" type="appl" />
         <tli id="T1656" time="961.2375714285714" type="appl" />
         <tli id="T1657" time="961.7334285714286" type="appl" />
         <tli id="T1658" time="962.2292857142858" type="appl" />
         <tli id="T1659" time="962.7251428571428" type="appl" />
         <tli id="T1660" time="963.221" type="appl" />
         <tli id="T1661" time="965.87" type="appl" />
         <tli id="T1662" time="966.73" type="appl" />
         <tli id="T1663" time="967.59" type="appl" />
         <tli id="T1664" time="968.45" type="appl" />
         <tli id="T1665" time="968.59" type="appl" />
         <tli id="T1666" time="969.166" type="appl" />
         <tli id="T1667" time="969.742" type="appl" />
         <tli id="T1668" time="970.318" type="appl" />
         <tli id="T1669" time="970.894" type="appl" />
         <tli id="T1670" time="971.47" type="appl" />
         <tli id="T1671" time="972.046" type="appl" />
         <tli id="T1672" time="972.622" type="appl" />
         <tli id="T1673" time="973.198" type="appl" />
         <tli id="T1674" time="973.99" type="appl" />
         <tli id="T1675" time="974.595" type="appl" />
         <tli id="T1676" time="975.2" type="appl" />
         <tli id="T1677" time="975.805" type="appl" />
         <tli id="T1678" time="976.41" type="appl" />
         <tli id="T1679" time="976.43" type="appl" />
         <tli id="T1680" time="976.846" type="appl" />
         <tli id="T1681" time="977.262" type="appl" />
         <tli id="T1682" time="977.678" type="appl" />
         <tli id="T1683" time="978.0939999999999" type="appl" />
         <tli id="T1684" time="978.51" type="appl" />
         <tli id="T1685" time="978.55" type="appl" />
         <tli id="T1686" time="978.8811111111111" type="appl" />
         <tli id="T1687" time="979.2122222222222" type="appl" />
         <tli id="T1688" time="979.5433333333333" type="appl" />
         <tli id="T1689" time="979.8744444444444" type="appl" />
         <tli id="T1690" time="980.2055555555555" type="appl" />
         <tli id="T1691" time="980.5366666666666" type="appl" />
         <tli id="T1692" time="980.8677777777777" type="appl" />
         <tli id="T1693" time="981.1988888888889" type="appl" />
         <tli id="T1694" time="981.53" type="appl" />
         <tli id="T1695" time="981.8399999999999" type="appl" />
         <tli id="T1696" time="982.15" type="appl" />
         <tli id="T1697" time="982.46" type="appl" />
         <tli id="T1698" time="982.77" type="appl" />
         <tli id="T1699" time="982.78" type="appl" />
         <tli id="T1700" time="983.06" type="appl" />
         <tli id="T1701" time="983.3399999999999" type="appl" />
         <tli id="T1702" time="983.62" type="appl" />
         <tli id="T1703" time="983.9" type="appl" />
         <tli id="T1704" time="984.1" type="appl" />
         <tli id="T1705" time="984.18" type="appl" />
         <tli id="T1706" time="984.97" type="appl" />
         <tli id="T1707" time="985.48" type="appl" />
         <tli id="T1708" time="985.84" type="appl" />
         <tli id="T1709" time="986.02" type="appl" />
         <tli id="T1710" time="986.13" type="appl" />
         <tli id="T1711" time="986.45" type="appl" />
         <tli id="T1712" time="986.86" type="appl" />
         <tli id="T1713" time="988.34" type="appl" />
         <tli id="T1714" time="988.9100000000001" type="appl" />
         <tli id="T1715" time="989.48" type="appl" />
         <tli id="T1716" time="999.22" type="appl" />
         <tli id="T1717" time="1000.65" type="appl" />
         <tli id="T1718" time="1001.206" type="appl" />
         <tli id="T1719" time="1001.762" type="appl" />
         <tli id="T1720" time="1002.318" type="appl" />
         <tli id="T1721" time="1002.8739999999999" type="appl" />
         <tli id="T1722" time="1003.43" type="appl" />
         <tli id="T1723" time="1005.2" type="appl" />
         <tli id="T1724" time="1005.9866666666667" type="appl" />
         <tli id="T1725" time="1006.7733333333333" type="appl" />
         <tli id="T1726" time="1007.56" type="appl" />
         <tli id="T1727" time="1008.3466666666667" type="appl" />
         <tli id="T1728" time="1009.1333333333333" type="appl" />
         <tli id="T1729" time="1009.92" type="appl" />
         <tli id="T1730" time="1010.21" type="appl" />
         <tli id="T1731" time="1011.13" type="appl" />
         <tli id="T1732" time="1012.05" type="appl" />
         <tli id="T1733" time="1012.97" type="appl" />
         <tli id="T1734" time="1013.7967055245102" />
         <tli id="T1735" time="1014.5666666666666" type="appl" />
         <tli id="T1736" time="1015.2433333333333" type="appl" />
         <tli id="T1737" time="1015.9200000000001" type="appl" />
         <tli id="T1738" time="1016.5966666666667" type="appl" />
         <tli id="T1739" time="1017.2733333333333" type="appl" />
         <tli id="T1740" time="1017.95" type="appl" />
         <tli id="T1741" time="1018.57" type="appl" />
         <tli id="T1742" time="1019.19" type="appl" />
         <tli id="T1743" time="1019.81" type="appl" />
         <tli id="T1744" time="1020.43" type="appl" />
         <tli id="T1745" time="1021.05" type="appl" />
         <tli id="T1746" time="1021.13" type="appl" />
         <tli id="T1747" time="1023.96" type="appl" />
         <tli id="T1748" time="1027.91" type="appl" />
         <tli id="T1749" time="1028.545" type="appl" />
         <tli id="T1750" time="1029.18" type="appl" />
         <tli id="T1751" time="1029.815" type="appl" />
         <tli id="T1752" time="1030.45" type="appl" />
         <tli id="T1753" time="1030.54" type="appl" />
         <tli id="T1754" time="1030.91" type="appl" />
         <tli id="T1755" time="1031.08" type="appl" />
         <tli id="T1756" time="1031.3700000000001" type="appl" />
         <tli id="T1757" time="1031.83" type="appl" />
         <tli id="T1758" time="1032.07" type="appl" />
         <tli id="T1759" time="1032.29" type="appl" />
         <tli id="T1760" time="1032.72" type="appl" />
         <tli id="T1761" time="1032.75" type="appl" />
         <tli id="T1762" time="1032.78" type="appl" />
         <tli id="T1763" time="1033.1" type="appl" />
         <tli id="T1764" time="1033.42" type="appl" />
         <tli id="T1765" time="1033.74" type="appl" />
         <tli id="T1766" time="1034.06" type="appl" />
         <tli id="T1767" time="1034.3799999999999" type="appl" />
         <tli id="T1768" time="1034.6999999999998" type="appl" />
         <tli id="T1769" time="1035.02" type="appl" />
         <tli id="T1770" time="1035.34" type="appl" />
         <tli id="T1771" time="1035.6599999999999" type="appl" />
         <tli id="T1772" time="1035.98" type="appl" />
         <tli id="T1773" time="1036.3" type="appl" />
         <tli id="T1774" time="1036.62" type="appl" />
         <tli id="T1775" time="1038.59" type="appl" />
         <tli id="T1776" time="1039.0855555555554" type="appl" />
         <tli id="T1777" time="1039.581111111111" type="appl" />
         <tli id="T1778" time="1040.0766666666666" type="appl" />
         <tli id="T1779" time="1040.572222222222" type="appl" />
         <tli id="T1780" time="1041.0677777777778" type="appl" />
         <tli id="T1781" time="1041.5633333333333" type="appl" />
         <tli id="T1782" time="1042.0588888888888" type="appl" />
         <tli id="T1783" time="1042.5544444444445" type="appl" />
         <tli id="T1784" time="1043.05" type="appl" />
         <tli id="T1785" time="1043.47" type="appl" />
         <tli id="T1786" time="1044.0503333333334" type="appl" />
         <tli id="T1787" time="1044.6306666666667" type="appl" />
         <tli id="T1788" time="1045.211" type="appl" />
         <tli id="T1789" time="1045.7913333333333" type="appl" />
         <tli id="T1790" time="1046.3716666666667" type="appl" />
         <tli id="T1791" time="1046.952" type="appl" />
         <tli id="T1792" time="1047.5323333333333" type="appl" />
         <tli id="T1793" time="1048.1126666666667" type="appl" />
         <tli id="T1794" time="1048.693" type="appl" />
         <tli id="T1795" time="1049.2733333333333" type="appl" />
         <tli id="T1796" time="1049.8536666666666" type="appl" />
         <tli id="T1797" time="1050.434" type="appl" />
         <tli id="T1798" time="1052.42" type="appl" />
         <tli id="T1799" time="1052.99" type="appl" />
         <tli id="T1800" time="1053.39" type="appl" />
         <tli id="T1801" time="1054.379607067259" />
         <tli id="T1802" time="1055.15" type="appl" />
         <tli id="T1803" time="1055.81" type="appl" />
         <tli id="T1804" time="1056.47" type="appl" />
         <tli id="T1805" time="1057.13" type="appl" />
         <tli id="T1806" time="1057.79" type="appl" />
         <tli id="T1807" time="1058.23" type="appl" />
         <tli id="T1808" time="1058.625" type="appl" />
         <tli id="T1809" time="1059.02" type="appl" />
         <tli id="T1810" time="1059.415" type="appl" />
         <tli id="T1811" time="1059.81" type="appl" />
         <tli id="T1812" time="1060.2050000000002" type="appl" />
         <tli id="T1813" time="1060.6000000000001" type="appl" />
         <tli id="T1814" time="1060.9950000000001" type="appl" />
         <tli id="T1815" time="1061.5329377347803" />
         <tli id="T1816" time="1061.97" type="appl" />
         <tli id="T1817" time="1062.39" type="appl" />
         <tli id="T1818" time="1062.81" type="appl" />
         <tli id="T1819" time="1063.23" type="appl" />
         <tli id="T1820" time="1063.25" type="appl" />
         <tli id="T1821" time="1063.5568181818182" type="appl" />
         <tli id="T1822" time="1063.8636363636363" type="appl" />
         <tli id="T1823" time="1064.1704545454545" type="appl" />
         <tli id="T1824" time="1064.4772727272727" type="appl" />
         <tli id="T1825" time="1064.784090909091" type="appl" />
         <tli id="T1826" time="1065.090909090909" type="appl" />
         <tli id="T1827" time="1065.3977272727273" type="appl" />
         <tli id="T1828" time="1065.7045454545455" type="appl" />
         <tli id="T1829" time="1066.0113636363637" type="appl" />
         <tli id="T1830" time="1066.3181818181818" type="appl" />
         <tli id="T1831" time="1066.625" type="appl" />
         <tli id="T1832" time="1066.9318181818182" type="appl" />
         <tli id="T1833" time="1067.2386363636363" type="appl" />
         <tli id="T1834" time="1067.5454545454545" type="appl" />
         <tli id="T1835" time="1067.8522727272727" type="appl" />
         <tli id="T1836" time="1068.159090909091" type="appl" />
         <tli id="T1837" time="1068.465909090909" type="appl" />
         <tli id="T1838" time="1068.7727272727273" type="appl" />
         <tli id="T1839" time="1069.0795454545455" type="appl" />
         <tli id="T1840" time="1069.3863636363637" type="appl" />
         <tli id="T1841" time="1069.6931818181818" type="appl" />
         <tli id="T1842" time="1070.0" type="appl" />
         <tli id="T1843" time="1074.06" type="appl" />
         <tli id="T1844" time="1075.0795993530498" />
         <tli id="T1845" time="1075.1728571428573" type="appl" />
         <tli id="T1846" time="1075.61" type="appl" />
         <tli id="T1847" time="1075.7057142857143" type="appl" />
         <tli id="T1848" time="1076.2385714285715" type="appl" />
         <tli id="T1849" time="1076.7714285714285" type="appl" />
         <tli id="T1850" time="1077.3042857142857" type="appl" />
         <tli id="T1851" time="1077.8371428571427" type="appl" />
         <tli id="T1852" time="1077.86" type="appl" />
         <tli id="T1853" time="1078.37" type="appl" />
         <tli id="T1854" time="1078.3999999999999" type="appl" />
         <tli id="T1855" time="1078.9399999999998" type="appl" />
         <tli id="T1856" time="1079.48" type="appl" />
         <tli id="T1857" time="1080.02" type="appl" />
         <tli id="T1858" time="1080.56" type="appl" />
         <tli id="T1859" time="1081.1" type="appl" />
         <tli id="T1860" time="1081.81" type="appl" />
         <tli id="T1915" />
         <tli id="T1916" />
         <tli id="T1861" time="1091.82" type="appl" />
         <tli id="T1862" time="1093.106259301771" />
         <tli id="T1863" time="1093.755" type="appl" />
         <tli id="T1864" time="1094.33" type="appl" />
         <tli id="T1865" time="1095.7195916612009" />
         <tli id="T1866" time="1096.5577777777778" type="appl" />
         <tli id="T1867" time="1097.1755555555555" type="appl" />
         <tli id="T1868" time="1097.7933333333333" type="appl" />
         <tli id="T1869" time="1098.411111111111" type="appl" />
         <tli id="T1870" time="1099.028888888889" type="appl" />
         <tli id="T1871" time="1099.6466666666668" type="appl" />
         <tli id="T1872" time="1100.2644444444445" type="appl" />
         <tli id="T1873" time="1100.8822222222223" type="appl" />
         <tli id="T1874" time="1101.5" type="appl" />
         <tli id="T1875" time="1101.57" type="appl" />
         <tli id="T1876" time="1102.44" type="appl" />
         <tli id="T1877" time="1103.31" type="appl" />
         <tli id="T1878" time="1103.38" type="appl" />
         <tli id="T1879" time="1104.0025" type="appl" />
         <tli id="T1880" time="1104.625" type="appl" />
         <tli id="T1881" time="1105.2475" type="appl" />
         <tli id="T1882" time="1105.87" type="appl" />
         <tli id="T1883" time="1106.026254486912" />
         <tli id="T1884" time="1106.857142857143" type="appl" />
         <tli id="T1885" time="1107.5042857142857" type="appl" />
         <tli id="T1886" time="1108.1514285714286" type="appl" />
         <tli id="T1887" time="1108.7985714285714" type="appl" />
         <tli id="T1888" time="1109.4457142857143" type="appl" />
         <tli id="T1889" time="1110.0928571428572" type="appl" />
         <tli id="T1890" time="1110.74" type="appl" />
         <tli id="T1891" time="1111.6" type="appl" />
         <tli id="T1892" time="1113.0462518707889" />
         <tli id="T0" time="1118.066" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T457" start="T456">
            <tli id="T456.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T492" start="T491">
            <tli id="T491.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Как</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">не</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">знаю</ts>
                  <nts id="Seg_12" n="HIAT:ip">,</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_14" n="HIAT:ip">(</nts>
                  <nts id="Seg_15" n="HIAT:ip">(</nts>
                  <ats e="T5" id="Seg_16" n="HIAT:non-pho" s="T4">…</ats>
                  <nts id="Seg_17" n="HIAT:ip">)</nts>
                  <nts id="Seg_18" n="HIAT:ip">)</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">сколько</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T6">ты</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_27" n="HIAT:w" s="T8">работал</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_30" n="sc" s="T43">
               <ts e="T47" id="Seg_32" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_34" n="HIAT:w" s="T43">Да</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_37" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_39" n="HIAT:w" s="T44">мужик</ts>
                  <nts id="Seg_40" n="HIAT:ip">)</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_43" n="HIAT:w" s="T45">такой</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_47" n="HIAT:w" s="T46">наверное</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_50" n="sc" s="T92">
               <ts e="T97" id="Seg_52" n="HIAT:u" s="T92">
                  <ts e="T95" id="Seg_54" n="HIAT:w" s="T92">Ну</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_58" n="HIAT:w" s="T95">ну</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_61" n="sc" s="T102">
               <ts e="T124" id="Seg_63" n="HIAT:u" s="T102">
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <ts e="T103" id="Seg_66" n="HIAT:w" s="T102">Он-</ts>
                  <nts id="Seg_67" n="HIAT:ip">)</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_70" n="HIAT:w" s="T103">Он</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_73" n="HIAT:w" s="T105">мне</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_76" n="HIAT:w" s="T107">все</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_79" n="HIAT:w" s="T109">писал</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_83" n="HIAT:w" s="T111">звал</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_87" n="HIAT:w" s="T112">а</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_90" n="HIAT:w" s="T113">я</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_93" n="HIAT:w" s="T114">ему</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_96" n="HIAT:w" s="T115">написала:</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_99" n="HIAT:w" s="T116">Агафон</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_102" n="HIAT:w" s="T117">Иванович</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_105" n="HIAT:w" s="T120">приедет</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_109" n="HIAT:w" s="T122">тогда</ts>
                  <nts id="Seg_110" n="HIAT:ip">…</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T136" id="Seg_112" n="sc" s="T130">
               <ts e="T136" id="Seg_114" n="HIAT:u" s="T130">
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_117" n="HIAT:w" s="T130">Он</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_120" n="HIAT:w" s="T132">г-</ts>
                  <nts id="Seg_121" n="HIAT:ip">)</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_124" n="HIAT:w" s="T134">Он</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_127" n="HIAT:w" s="T135">потома-ка</ts>
                  <nts id="Seg_128" n="HIAT:ip">…</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T146" id="Seg_130" n="sc" s="T137">
               <ts e="T146" id="Seg_132" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_134" n="HIAT:w" s="T137">Он</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_137" n="HIAT:w" s="T138">мне</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_140" n="HIAT:w" s="T139">пишет:</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_142" n="HIAT:ip">"</nts>
                  <ts e="T141" id="Seg_144" n="HIAT:w" s="T140">Вот</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_147" n="HIAT:w" s="T141">приеду</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_150" n="HIAT:w" s="T142">четырнадцатого</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_153" n="HIAT:w" s="T143">числа</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_156" n="HIAT:w" s="T144">к</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_159" n="HIAT:w" s="T145">тебе</ts>
                  <nts id="Seg_160" n="HIAT:ip">"</nts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_163" n="sc" s="T147">
               <ts e="T156" id="Seg_165" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_167" n="HIAT:w" s="T147">Ну</ts>
                  <nts id="Seg_168" n="HIAT:ip">,</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_171" n="HIAT:w" s="T148">я</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_174" n="HIAT:w" s="T149">и</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_177" n="HIAT:w" s="T150">дожидаю</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_181" n="HIAT:w" s="T151">а</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_184" n="HIAT:w" s="T152">он</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_187" n="HIAT:w" s="T153">вперед</ts>
                  <nts id="Seg_188" n="HIAT:ip">,</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_191" n="HIAT:w" s="T154">скорее</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_194" n="HIAT:w" s="T155">приехал</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_197" n="sc" s="T157">
               <ts e="T166" id="Seg_199" n="HIAT:u" s="T157">
                  <ts e="T159" id="Seg_201" n="HIAT:w" s="T157">Ну</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_205" n="HIAT:w" s="T159">я</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_208" n="HIAT:w" s="T161">уже</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_211" n="HIAT:w" s="T163">не</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_214" n="HIAT:w" s="T164">отказалась</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_217" n="HIAT:w" s="T165">потом</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_220" n="sc" s="T167">
               <ts e="T177" id="Seg_222" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_224" n="HIAT:w" s="T167">Племянник</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_227" n="HIAT:w" s="T168">не</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_230" n="HIAT:w" s="T169">пускал</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_233" n="HIAT:ip">"</nts>
                  <ts e="T171" id="Seg_235" n="HIAT:w" s="T170">зачем</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_238" n="HIAT:w" s="T171">ты</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_241" n="HIAT:w" s="T172">поедешь</ts>
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_245" n="HIAT:w" s="T173">да</ts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_249" n="HIAT:w" s="T174">то</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_253" n="HIAT:w" s="T175">другое</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_256" n="HIAT:w" s="T176">да</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T199" id="Seg_259" n="sc" s="T181">
               <ts e="T199" id="Seg_261" n="HIAT:u" s="T181">
                  <ts e="T183" id="Seg_263" n="HIAT:w" s="T181">Думаю</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_267" n="HIAT:w" s="T183">ну</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_270" n="HIAT:w" s="T186">что</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_274" n="HIAT:w" s="T187">раз</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_277" n="HIAT:w" s="T188">я</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_280" n="HIAT:w" s="T189">так</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_283" n="HIAT:w" s="T190">обещалась</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_287" n="HIAT:w" s="T191">то</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_290" n="HIAT:w" s="T192">приеду</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_294" n="HIAT:w" s="T193">а</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_297" n="HIAT:w" s="T194">теперь</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_300" n="HIAT:w" s="T195">человек</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_303" n="HIAT:w" s="T196">приедет</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_307" n="HIAT:w" s="T197">один</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_310" n="HIAT:w" s="T198">поедет</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_313" n="sc" s="T200">
               <ts e="T209" id="Seg_315" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_317" n="HIAT:w" s="T200">Ну</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_321" n="HIAT:w" s="T201">он</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_324" n="HIAT:w" s="T202">у</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_327" n="HIAT:w" s="T203">меня</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_330" n="HIAT:w" s="T204">только</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_333" n="HIAT:w" s="T205">одну</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_336" n="HIAT:w" s="T206">ночь</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_339" n="HIAT:w" s="T207">и</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_342" n="HIAT:w" s="T208">ночевал</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_345" n="sc" s="T214">
               <ts e="T225" id="Seg_347" n="HIAT:u" s="T214">
                  <ts e="T220" id="Seg_349" n="HIAT:w" s="T214">Ночевал</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_353" n="HIAT:w" s="T220">ночевал</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_356" n="sc" s="T226">
               <ts e="T232" id="Seg_358" n="HIAT:u" s="T226">
                  <ts e="T228" id="Seg_360" n="HIAT:w" s="T226">И</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_363" n="HIAT:w" s="T228">в</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_366" n="HIAT:w" s="T229">баню</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_369" n="HIAT:w" s="T230">сходил</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_373" n="HIAT:w" s="T231">помылся</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_376" n="sc" s="T234">
               <ts e="T238" id="Seg_378" n="HIAT:u" s="T234">
                  <ts e="T238" id="Seg_380" n="HIAT:w" s="T234">Ходил</ts>
                  <nts id="Seg_381" n="HIAT:ip">…</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_383" n="sc" s="T242">
               <ts e="T244" id="Seg_385" n="HIAT:u" s="T242">
                  <ts e="T244" id="Seg_387" n="HIAT:w" s="T242">Ну</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_391" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_393" n="HIAT:w" s="T244">И</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_396" n="HIAT:w" s="T245">ходил</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_399" n="HIAT:w" s="T246">на</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_402" n="HIAT:w" s="T247">пруд</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T250" id="Seg_405" n="sc" s="T249">
               <ts e="T250" id="Seg_407" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_409" n="HIAT:w" s="T249">Тамо-ка</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_412" n="sc" s="T251">
               <ts e="T256" id="Seg_414" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_416" n="HIAT:w" s="T251">И</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_419" n="HIAT:w" s="T252">потом</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_422" n="HIAT:w" s="T253">зашел</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_425" n="HIAT:w" s="T254">к</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_428" n="HIAT:w" s="T255">мельнику</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T267" id="Seg_431" n="sc" s="T257">
               <ts e="T267" id="Seg_433" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_435" n="HIAT:w" s="T257">Пришел</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_439" n="HIAT:w" s="T258">говорит:</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_441" n="HIAT:ip">"</nts>
                  <ts e="T260" id="Seg_443" n="HIAT:w" s="T259">Ой</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_447" n="HIAT:w" s="T260">я</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_450" n="HIAT:w" s="T261">старушку</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_453" n="HIAT:w" s="T262">нашел</ts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_457" n="HIAT:w" s="T263">ну</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_461" n="HIAT:w" s="T264">на</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_464" n="HIAT:w" s="T265">ваш</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_467" n="HIAT:w" s="T266">язык</ts>
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T275" id="Seg_471" n="sc" s="T268">
               <ts e="T275" id="Seg_473" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_475" n="HIAT:w" s="T268">Я</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_478" n="HIAT:w" s="T269">говорю:</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <ts e="T271" id="Seg_482" n="HIAT:w" s="T270">Не</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_485" n="HIAT:w" s="T271">говори</ts>
                  <nts id="Seg_486" n="HIAT:ip">,</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_489" n="HIAT:w" s="T272">это</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_492" n="HIAT:w" s="T273">моя</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_495" n="HIAT:w" s="T274">подружка</ts>
                  <nts id="Seg_496" n="HIAT:ip">"</nts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_499" n="sc" s="T277">
               <ts e="T300" id="Seg_501" n="HIAT:u" s="T277">
                  <ts e="T279" id="Seg_503" n="HIAT:w" s="T277">Я</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_506" n="HIAT:w" s="T279">часто</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_509" n="HIAT:w" s="T280">к</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_512" n="HIAT:w" s="T281">ей</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_515" n="HIAT:w" s="T282">хожу</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_519" n="HIAT:w" s="T283">она</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_522" n="HIAT:w" s="T284">вытащит</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_525" n="HIAT:w" s="T285">Евангелие</ts>
                  <nts id="Seg_526" n="HIAT:ip">,</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_529" n="HIAT:w" s="T286">она</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_532" n="HIAT:w" s="T287">не</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_535" n="HIAT:w" s="T288">умеет</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_538" n="HIAT:w" s="T289">на</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_541" n="HIAT:w" s="T290">русский</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_545" n="HIAT:w" s="T291">а</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_548" n="HIAT:w" s="T292">я</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_551" n="HIAT:w" s="T293">ей</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_554" n="HIAT:w" s="T294">почитаю</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_558" n="HIAT:w" s="T295">с</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_561" n="HIAT:w" s="T296">ей</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_564" n="HIAT:w" s="T298">помолимся</ts>
                  <nts id="Seg_565" n="HIAT:ip">.</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_567" n="sc" s="T315">
               <ts e="T319" id="Seg_569" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_571" n="HIAT:w" s="T315">Ну</ts>
                  <nts id="Seg_572" n="HIAT:ip">,</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_575" n="HIAT:w" s="T316">теперь</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_578" n="HIAT:w" s="T317">говорить</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_581" n="HIAT:w" s="T318">можно</ts>
                  <nts id="Seg_582" n="HIAT:ip">?</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T324" id="Seg_584" n="sc" s="T320">
               <ts e="T324" id="Seg_586" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_588" n="HIAT:w" s="T320">Măna</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_590" n="HIAT:ip">(</nts>
                  <ts e="T322" id="Seg_592" n="HIAT:w" s="T321">pʼaŋdəbi</ts>
                  <nts id="Seg_593" n="HIAT:ip">)</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_596" n="HIAT:w" s="T322">măn</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_599" n="HIAT:w" s="T323">kagam</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T327" id="Seg_602" n="sc" s="T325">
               <ts e="T327" id="Seg_604" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_606" n="HIAT:w" s="T325">Teʔtə</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_609" n="HIAT:w" s="T326">kö</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T335" id="Seg_612" n="sc" s="T328">
               <ts e="T332" id="Seg_614" n="HIAT:u" s="T328">
                  <nts id="Seg_615" n="HIAT:ip">"</nts>
                  <ts e="T329" id="Seg_617" n="HIAT:w" s="T328">Šoʔ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_620" n="HIAT:w" s="T329">döber</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_624" n="HIAT:w" s="T330">šoʔ</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_627" n="HIAT:w" s="T331">döber</ts>
                  <nts id="Seg_628" n="HIAT:ip">"</nts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_632" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_634" n="HIAT:w" s="T332">Măn</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_636" n="HIAT:ip">(</nts>
                  <ts e="T334" id="Seg_638" n="HIAT:w" s="T333">šindəzi</ts>
                  <nts id="Seg_639" n="HIAT:ip">)</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_642" n="HIAT:w" s="T334">kalam</ts>
                  <nts id="Seg_643" n="HIAT:ip">?</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T339" id="Seg_645" n="sc" s="T336">
               <ts e="T339" id="Seg_647" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_649" n="HIAT:w" s="T336">Ĭmbidə</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_652" n="HIAT:w" s="T337">ej</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_655" n="HIAT:w" s="T338">tĭmnem</ts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T344" id="Seg_658" n="sc" s="T340">
               <ts e="T344" id="Seg_660" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_662" n="HIAT:w" s="T340">Dĭgəttə</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_665" n="HIAT:w" s="T341">pʼaŋdəbiam</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_668" n="HIAT:w" s="T342">dĭʔnə</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_671" n="HIAT:w" s="T343">sazən</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_674" n="sc" s="T345">
               <ts e="T353" id="Seg_676" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_678" n="HIAT:w" s="T345">Mămbiam:</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_680" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_682" n="HIAT:w" s="T346">Šoləj</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_685" n="HIAT:w" s="T347">Агафон</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_688" n="HIAT:w" s="T348">Иванович</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_690" n="HIAT:ip">(</nts>
                  <ts e="T350" id="Seg_692" n="HIAT:w" s="T349">măna</ts>
                  <nts id="Seg_693" n="HIAT:ip">)</nts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_697" n="HIAT:w" s="T350">dĭgəttə</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_700" n="HIAT:w" s="T351">măn</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_703" n="HIAT:w" s="T352">šoləm</ts>
                  <nts id="Seg_704" n="HIAT:ip">"</nts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T361" id="Seg_707" n="sc" s="T354">
               <ts e="T361" id="Seg_709" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_711" n="HIAT:w" s="T354">Dĭgəttə</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_714" n="HIAT:w" s="T355">dĭ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_717" n="HIAT:w" s="T356">măna</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_720" n="HIAT:w" s="T357">pʼaŋdlaʔbə:</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_722" n="HIAT:ip">"</nts>
                  <ts e="T359" id="Seg_724" n="HIAT:w" s="T358">Šoləm</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_727" n="HIAT:w" s="T359">bostə</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_730" n="HIAT:w" s="T360">šiʔnʼileʔ</ts>
                  <nts id="Seg_731" n="HIAT:ip">"</nts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T369" id="Seg_734" n="sc" s="T362">
               <ts e="T369" id="Seg_736" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_738" n="HIAT:w" s="T362">Dĭgəttə</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_741" n="HIAT:w" s="T363">šobi</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_744" n="HIAT:w" s="T364">miʔnʼibeʔ</ts>
                  <nts id="Seg_745" n="HIAT:ip">,</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_748" n="HIAT:w" s="T365">măn</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_751" n="HIAT:w" s="T366">ej</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_754" n="HIAT:w" s="T367">edəbiem</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_757" n="HIAT:w" s="T368">dĭm</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T375" id="Seg_760" n="sc" s="T370">
               <ts e="T375" id="Seg_762" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_764" n="HIAT:w" s="T370">Nu</ts>
                  <nts id="Seg_765" n="HIAT:ip">,</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_768" n="HIAT:w" s="T371">măn</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_771" n="HIAT:w" s="T372">nʼeveskăm</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_774" n="HIAT:w" s="T373">dĭm</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_777" n="HIAT:w" s="T374">detleʔbə</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T380" id="Seg_780" n="sc" s="T376">
               <ts e="T380" id="Seg_782" n="HIAT:u" s="T376">
                  <nts id="Seg_783" n="HIAT:ip">"</nts>
                  <ts e="T377" id="Seg_785" n="HIAT:w" s="T376">Dö</ts>
                  <nts id="Seg_786" n="HIAT:ip">,</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_789" n="HIAT:w" s="T377">tăn</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_792" n="HIAT:w" s="T378">kuza</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_795" n="HIAT:w" s="T379">šobi</ts>
                  <nts id="Seg_796" n="HIAT:ip">"</nts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T385" id="Seg_799" n="sc" s="T381">
               <ts e="T385" id="Seg_801" n="HIAT:u" s="T381">
                  <nts id="Seg_802" n="HIAT:ip">(</nts>
                  <ts e="T382" id="Seg_804" n="HIAT:w" s="T381">M-</ts>
                  <nts id="Seg_805" n="HIAT:ip">)</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_808" n="HIAT:w" s="T382">Măn</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_811" n="HIAT:w" s="T383">nuʔməbiem</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_814" n="HIAT:w" s="T384">nʼiʔnə</ts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T395" id="Seg_817" n="sc" s="T386">
               <ts e="T389" id="Seg_819" n="HIAT:u" s="T386">
                  <nts id="Seg_820" n="HIAT:ip">"</nts>
                  <ts e="T387" id="Seg_822" n="HIAT:w" s="T386">Oj</ts>
                  <nts id="Seg_823" n="HIAT:ip">,</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_826" n="HIAT:w" s="T387">büžü</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_829" n="HIAT:w" s="T388">šobial</ts>
                  <nts id="Seg_830" n="HIAT:ip">"</nts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_834" n="HIAT:u" s="T389">
                  <nts id="Seg_835" n="HIAT:ip">(</nts>
                  <ts e="T390" id="Seg_837" n="HIAT:w" s="T389">A</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_840" n="HIAT:w" s="T390">dĭ=</ts>
                  <nts id="Seg_841" n="HIAT:ip">)</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_844" n="HIAT:w" s="T391">Dĭgəttə</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_847" n="HIAT:w" s="T392">dĭ</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_850" n="HIAT:w" s="T393">šobi</ts>
                  <nts id="Seg_851" n="HIAT:ip">,</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_854" n="HIAT:w" s="T394">amorbi</ts>
                  <nts id="Seg_855" n="HIAT:ip">.</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T398" id="Seg_857" n="sc" s="T396">
               <ts e="T398" id="Seg_859" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_861" n="HIAT:w" s="T396">Iʔbəbi</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_864" n="HIAT:w" s="T397">kunolzittə</ts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T404" id="Seg_867" n="sc" s="T399">
               <ts e="T404" id="Seg_869" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_871" n="HIAT:w" s="T399">Dĭgəttə</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_874" n="HIAT:w" s="T400">uʔbdəbi</ts>
                  <nts id="Seg_875" n="HIAT:ip">,</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_878" n="HIAT:w" s="T401">kambi</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_881" n="HIAT:w" s="T402">bünə</ts>
                  <nts id="Seg_882" n="HIAT:ip">,</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_885" n="HIAT:w" s="T403">tʼerbəndə</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_888" n="sc" s="T405">
               <ts e="T410" id="Seg_890" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_892" n="HIAT:w" s="T405">Dĭn</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_895" n="HIAT:w" s="T406">tože</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_898" n="HIAT:w" s="T407">dĭrgit</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_900" n="HIAT:ip">(</nts>
                  <ts e="T409" id="Seg_902" n="HIAT:w" s="T408">ibi=</ts>
                  <nts id="Seg_903" n="HIAT:ip">)</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_906" n="HIAT:w" s="T409">kubi</ts>
                  <nts id="Seg_907" n="HIAT:ip">.</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T413" id="Seg_909" n="sc" s="T411">
               <ts e="T413" id="Seg_911" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_913" n="HIAT:w" s="T411">Dʼăbaktərbi</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_916" n="HIAT:w" s="T412">dĭn</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_919" n="sc" s="T414">
               <ts e="T417" id="Seg_921" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_923" n="HIAT:w" s="T414">Dĭgəttə</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_926" n="HIAT:w" s="T415">šobi</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_929" n="HIAT:w" s="T416">maːʔndə</ts>
                  <nts id="Seg_930" n="HIAT:ip">.</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T421" id="Seg_932" n="sc" s="T418">
               <ts e="T421" id="Seg_934" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_936" n="HIAT:w" s="T418">Dĭgəttə</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_939" n="HIAT:w" s="T419">moltʼanə</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_942" n="HIAT:w" s="T420">kambi</ts>
                  <nts id="Seg_943" n="HIAT:ip">.</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T425" id="Seg_945" n="sc" s="T422">
               <ts e="T425" id="Seg_947" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_949" n="HIAT:w" s="T422">Măn</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_952" n="HIAT:w" s="T423">dĭzi</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_955" n="HIAT:w" s="T424">mĭmbiem</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T435" id="Seg_958" n="sc" s="T426">
               <ts e="T435" id="Seg_960" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_962" n="HIAT:w" s="T426">Dĭn</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_964" n="HIAT:ip">(</nts>
                  <ts e="T428" id="Seg_966" n="HIAT:w" s="T427">b-</ts>
                  <nts id="Seg_967" n="HIAT:ip">)</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_970" n="HIAT:w" s="T428">bü</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_973" n="HIAT:w" s="T429">dĭʔnə</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_977" n="HIAT:w" s="T430">dʼibige</ts>
                  <nts id="Seg_978" n="HIAT:ip">,</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_981" n="HIAT:w" s="T431">šišəge</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_984" n="HIAT:w" s="T432">bü</ts>
                  <nts id="Seg_985" n="HIAT:ip">,</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_988" n="HIAT:w" s="T433">sabən</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_991" n="HIAT:w" s="T434">mĭbiem</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_994" n="sc" s="T436">
               <ts e="T442" id="Seg_996" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_998" n="HIAT:w" s="T436">Dĭ</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1001" n="HIAT:w" s="T437">băzəjdəbi</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1004" n="HIAT:w" s="T438">dĭn</ts>
                  <nts id="Seg_1005" n="HIAT:ip">,</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1008" n="HIAT:w" s="T439">dĭgəttə</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1011" n="HIAT:w" s="T440">šobi</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1014" n="HIAT:w" s="T441">maːʔnʼi</ts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T449" id="Seg_1017" n="sc" s="T443">
               <ts e="T445" id="Seg_1019" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1021" n="HIAT:w" s="T443">Mămbiam:</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1023" n="HIAT:ip">"</nts>
                  <ts e="T445" id="Seg_1025" n="HIAT:w" s="T444">Amaʔ</ts>
                  <nts id="Seg_1026" n="HIAT:ip">"</nts>
                  <nts id="Seg_1027" n="HIAT:ip">.</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1030" n="HIAT:u" s="T445">
                  <nts id="Seg_1031" n="HIAT:ip">"</nts>
                  <ts e="T446" id="Seg_1033" n="HIAT:w" s="T445">Măn</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1036" n="HIAT:w" s="T446">dĭn</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1039" n="HIAT:w" s="T447">ambiam</ts>
                  <nts id="Seg_1040" n="HIAT:ip">,</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1043" n="HIAT:w" s="T448">tʼerbəngən</ts>
                  <nts id="Seg_1044" n="HIAT:ip">"</nts>
                  <nts id="Seg_1045" n="HIAT:ip">.</nts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T452" id="Seg_1047" n="sc" s="T450">
               <ts e="T452" id="Seg_1049" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1051" n="HIAT:w" s="T450">Ej</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1054" n="HIAT:w" s="T451">ambi</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T457" id="Seg_1057" n="sc" s="T453">
               <ts e="T457" id="Seg_1059" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1061" n="HIAT:w" s="T453">Dĭgəttə</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1063" n="HIAT:ip">(</nts>
                  <ts e="T455" id="Seg_1065" n="HIAT:w" s="T454">i-</ts>
                  <nts id="Seg_1066" n="HIAT:ip">)</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1069" n="HIAT:w" s="T455">kudajdə</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456.tx-PKZ.1" id="Seg_1072" n="HIAT:w" s="T456">numan</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1075" n="HIAT:w" s="T456.tx-PKZ.1">üzəbibeʔ</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_1078" n="sc" s="T458">
               <ts e="T464" id="Seg_1080" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1082" n="HIAT:w" s="T458">Dĭgəttə</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1085" n="HIAT:w" s="T459">dĭ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1088" n="HIAT:w" s="T460">iʔbəbi</ts>
                  <nts id="Seg_1089" n="HIAT:ip">,</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1092" n="HIAT:w" s="T461">măn</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1095" n="HIAT:w" s="T462">tože</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1098" n="HIAT:w" s="T463">iʔbəbiem</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T469" id="Seg_1101" n="sc" s="T465">
               <ts e="T469" id="Seg_1103" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1105" n="HIAT:w" s="T465">Ertən</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1108" n="HIAT:w" s="T466">uʔbdlaʔbəm</ts>
                  <nts id="Seg_1109" n="HIAT:ip">,</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1112" n="HIAT:w" s="T467">munujʔ</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1115" n="HIAT:w" s="T468">mĭndərbiem</ts>
                  <nts id="Seg_1116" n="HIAT:ip">.</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T474" id="Seg_1118" n="sc" s="T470">
               <ts e="T474" id="Seg_1120" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1122" n="HIAT:w" s="T470">Amorbibaʔ</ts>
                  <nts id="Seg_1123" n="HIAT:ip">,</nts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1125" n="HIAT:ip">(</nts>
                  <ts e="T472" id="Seg_1127" n="HIAT:w" s="T471">kam-</ts>
                  <nts id="Seg_1128" n="HIAT:ip">)</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1131" n="HIAT:w" s="T472">kambibaʔ</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1134" n="HIAT:w" s="T473">aftobustə</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1137" n="sc" s="T475">
               <ts e="T480" id="Seg_1139" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1141" n="HIAT:w" s="T475">Dĭn</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1144" n="HIAT:w" s="T476">nubibaʔ</ts>
                  <nts id="Seg_1145" n="HIAT:ip">,</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1148" n="HIAT:w" s="T477">dĭgəttə</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1151" n="HIAT:w" s="T478">šobi</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1154" n="HIAT:w" s="T479">aftobus</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_1157" n="sc" s="T481">
               <ts e="T487" id="Seg_1159" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1161" n="HIAT:w" s="T481">Miʔ</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1163" n="HIAT:ip">(</nts>
                  <ts e="T483" id="Seg_1165" n="HIAT:w" s="T482">amno-</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1168" n="HIAT:w" s="T483">amn-</ts>
                  <nts id="Seg_1169" n="HIAT:ip">)</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1172" n="HIAT:w" s="T484">amnobibaʔ</ts>
                  <nts id="Seg_1173" n="HIAT:ip">,</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1918" id="Seg_1176" n="HIAT:w" s="T485">Kazan</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1179" n="HIAT:w" s="T1918">turanə</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1182" n="HIAT:w" s="T486">kambibaʔ</ts>
                  <nts id="Seg_1183" n="HIAT:ip">.</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_1185" n="sc" s="T488">
               <ts e="T492" id="Seg_1187" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1189" n="HIAT:w" s="T488">Dĭn</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1192" n="HIAT:w" s="T489">tože</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1195" n="HIAT:w" s="T490">kudajdə</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491.tx-PKZ.1" id="Seg_1198" n="HIAT:w" s="T491">numan</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1201" n="HIAT:w" s="T491.tx-PKZ.1">üzəbiʔi</ts>
                  <nts id="Seg_1202" n="HIAT:ip">.</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T499" id="Seg_1204" n="sc" s="T493">
               <ts e="T499" id="Seg_1206" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1208" n="HIAT:w" s="T493">Kamən</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1211" n="HIAT:w" s="T494">sumna</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1214" n="HIAT:w" s="T495">šobi</ts>
                  <nts id="Seg_1215" n="HIAT:ip">,</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1218" n="HIAT:w" s="T496">dĭgəttə</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1221" n="HIAT:w" s="T497">kambibaʔ</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1224" n="HIAT:w" s="T498">Ujardə</ts>
                  <nts id="Seg_1225" n="HIAT:ip">.</nts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T503" id="Seg_1227" n="sc" s="T500">
               <ts e="T503" id="Seg_1229" n="HIAT:u" s="T500">
                  <nts id="Seg_1230" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_1232" n="HIAT:w" s="T500">Dĭn=</ts>
                  <nts id="Seg_1233" n="HIAT:ip">)</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1236" n="HIAT:w" s="T501">Dĭn</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1239" n="HIAT:w" s="T502">šaːbibaʔ</ts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T510" id="Seg_1242" n="sc" s="T504">
               <ts e="T510" id="Seg_1244" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1246" n="HIAT:w" s="T504">Tože</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1249" n="HIAT:w" s="T505">dĭn</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1251" n="HIAT:ip">(</nts>
                  <ts e="T507" id="Seg_1253" n="HIAT:w" s="T506">măn</ts>
                  <nts id="Seg_1254" n="HIAT:ip">)</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1257" n="HIAT:w" s="T507">ige</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1260" n="HIAT:w" s="T508">il</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1263" n="HIAT:w" s="T509">tože</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T515" id="Seg_1266" n="sc" s="T511">
               <ts e="T515" id="Seg_1268" n="HIAT:u" s="T511">
                  <nts id="Seg_1269" n="HIAT:ip">(</nts>
                  <ts e="T512" id="Seg_1271" n="HIAT:w" s="T511">Dĭgəttə</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1274" n="HIAT:w" s="T512">dĭg-</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1277" n="HIAT:w" s="T513">Dĭgəttə</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1280" n="HIAT:w" s="T514">ert-</ts>
                  <nts id="Seg_1281" n="HIAT:ip">)</nts>
                  <nts id="Seg_1282" n="HIAT:ip">.</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T522" id="Seg_1284" n="sc" s="T516">
               <ts e="T522" id="Seg_1286" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1288" n="HIAT:w" s="T516">Dĭgəttə</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1291" n="HIAT:w" s="T517">ertən</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1294" n="HIAT:w" s="T518">uʔbdəbibaʔ</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1297" n="HIAT:w" s="T519">i</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1300" n="HIAT:w" s="T520">kambibaʔ</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1303" n="HIAT:w" s="T521">Krasnăjarskəgən</ts>
                  <nts id="Seg_1304" n="HIAT:ip">.</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T531" id="Seg_1306" n="sc" s="T523">
               <ts e="T531" id="Seg_1308" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_1310" n="HIAT:w" s="T523">Dĭn</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1313" n="HIAT:w" s="T524">kambibaʔ</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1316" n="HIAT:w" s="T525">gijen</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1319" n="HIAT:w" s="T526">nʼergölaʔbəʔjə</ts>
                  <nts id="Seg_1320" n="HIAT:ip">,</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1323" n="HIAT:w" s="T527">dĭn</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1326" n="HIAT:w" s="T528">amnobibaʔ</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1329" n="HIAT:w" s="T529">i</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1917" id="Seg_1332" n="HIAT:w" s="T530">kalla</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1335" n="HIAT:w" s="T1917">dʼürbibaʔ</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T536" id="Seg_1338" n="sc" s="T532">
               <ts e="T536" id="Seg_1340" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_1342" n="HIAT:w" s="T532">Năvăsibirʼskanə</ts>
                  <nts id="Seg_1343" n="HIAT:ip">,</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1346" n="HIAT:w" s="T533">dĭn</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1349" n="HIAT:w" s="T534">tože</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1352" n="HIAT:w" s="T535">šaːbibaʔ</ts>
                  <nts id="Seg_1353" n="HIAT:ip">.</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T544" id="Seg_1355" n="sc" s="T537">
               <ts e="T544" id="Seg_1357" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_1359" n="HIAT:w" s="T537">Dĭgəttə</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1362" n="HIAT:w" s="T538">dĭn</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1365" n="HIAT:w" s="T539">bazo</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1368" n="HIAT:w" s="T540">amnobibaʔ</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1371" n="HIAT:w" s="T541">i</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1374" n="HIAT:w" s="T542">šobibaʔ</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1377" n="HIAT:w" s="T543">Măskvanə</ts>
                  <nts id="Seg_1378" n="HIAT:ip">.</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T559" id="Seg_1380" n="sc" s="T545">
               <ts e="T555" id="Seg_1382" n="HIAT:u" s="T545">
                  <nts id="Seg_1383" n="HIAT:ip">(</nts>
                  <ts e="T546" id="Seg_1385" n="HIAT:w" s="T545">D-</ts>
                  <nts id="Seg_1386" n="HIAT:ip">)</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1389" n="HIAT:w" s="T546">Dön</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1392" n="HIAT:w" s="T547">dʼünə</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1395" n="HIAT:w" s="T548">amnobibaʔ</ts>
                  <nts id="Seg_1396" n="HIAT:ip">,</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1399" n="HIAT:w" s="T549">dĭgəttə</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1401" n="HIAT:ip">(</nts>
                  <ts e="T551" id="Seg_1403" n="HIAT:w" s="T550">amnobibaʔ=</ts>
                  <nts id="Seg_1404" n="HIAT:ip">)</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1407" n="HIAT:w" s="T551">amnobibaʔ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1410" n="HIAT:w" s="T552">mašinanə</ts>
                  <nts id="Seg_1411" n="HIAT:ip">,</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1414" n="HIAT:w" s="T553">kambibaʔ</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1417" n="HIAT:w" s="T554">Măskvanə</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_1421" n="HIAT:u" s="T555">
                  <nts id="Seg_1422" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_1424" n="HIAT:w" s="T555">Dĭ-</ts>
                  <nts id="Seg_1425" n="HIAT:ip">)</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1428" n="HIAT:w" s="T556">Dĭn</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1431" n="HIAT:w" s="T557">măna</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1434" n="HIAT:w" s="T558">kürbi</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T564" id="Seg_1437" n="sc" s="T560">
               <ts e="T564" id="Seg_1439" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_1441" n="HIAT:w" s="T560">Tože</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1444" n="HIAT:w" s="T561">il</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1447" n="HIAT:w" s="T562">dĭn</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1450" n="HIAT:w" s="T563">ibiʔi</ts>
                  <nts id="Seg_1451" n="HIAT:ip">.</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T576" id="Seg_1453" n="sc" s="T565">
               <ts e="T576" id="Seg_1455" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1457" n="HIAT:w" s="T565">Dĭgəttə</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1460" n="HIAT:w" s="T566">parbibaʔ</ts>
                  <nts id="Seg_1461" n="HIAT:ip">,</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1463" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_1465" n="HIAT:w" s="T567">m-</ts>
                  <nts id="Seg_1466" n="HIAT:ip">)</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1469" n="HIAT:w" s="T568">bazo</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1472" n="HIAT:w" s="T569">amnolbibaʔ</ts>
                  <nts id="Seg_1473" n="HIAT:ip">,</nts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1476" n="HIAT:w" s="T570">i</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1478" n="HIAT:ip">(</nts>
                  <ts e="T572" id="Seg_1480" n="HIAT:w" s="T571">dö=</ts>
                  <nts id="Seg_1481" n="HIAT:ip">)</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1484" n="HIAT:w" s="T572">dö</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1487" n="HIAT:w" s="T573">turanə</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1489" n="HIAT:ip">(</nts>
                  <ts e="T575" id="Seg_1491" n="HIAT:w" s="T574">so-</ts>
                  <nts id="Seg_1492" n="HIAT:ip">)</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1495" n="HIAT:w" s="T575">šobibaʔ</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T582" id="Seg_1498" n="sc" s="T577">
               <ts e="T582" id="Seg_1500" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_1502" n="HIAT:w" s="T577">Dĭgəttə</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1505" n="HIAT:w" s="T578">döʔə</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1507" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_1509" n="HIAT:w" s="T579">gu</ts>
                  <nts id="Seg_1510" n="HIAT:ip">)</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1513" n="HIAT:w" s="T580">kambibaʔ</ts>
                  <nts id="Seg_1514" n="HIAT:ip">,</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1517" n="HIAT:w" s="T581">Tartu</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T587" id="Seg_1520" n="sc" s="T583">
               <ts e="T587" id="Seg_1522" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_1524" n="HIAT:w" s="T583">Dĭn</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1527" n="HIAT:w" s="T584">nagur</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1530" n="HIAT:w" s="T585">nedʼelʼa</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1533" n="HIAT:w" s="T586">amnobiam</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T596" id="Seg_1536" n="sc" s="T588">
               <ts e="T596" id="Seg_1538" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_1540" n="HIAT:w" s="T588">Bazo</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1542" n="HIAT:ip">(</nts>
                  <ts e="T590" id="Seg_1544" n="HIAT:w" s="T589">kamb-</ts>
                  <nts id="Seg_1545" n="HIAT:ip">)</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1548" n="HIAT:w" s="T590">măna</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1551" n="HIAT:w" s="T591">ibi</ts>
                  <nts id="Seg_1552" n="HIAT:ip">,</nts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1555" n="HIAT:w" s="T592">kambibaʔ</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1558" n="HIAT:w" s="T593">gijen</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1561" n="HIAT:w" s="T594">iʔgö</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1564" n="HIAT:w" s="T595">sĭrujanə</ts>
                  <nts id="Seg_1565" n="HIAT:ip">.</nts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T601" id="Seg_1567" n="sc" s="T597">
               <ts e="T601" id="Seg_1569" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_1571" n="HIAT:w" s="T597">Dĭn</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1574" n="HIAT:w" s="T598">iʔgö</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1577" n="HIAT:w" s="T599">il</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1580" n="HIAT:w" s="T600">ibiʔi</ts>
                  <nts id="Seg_1581" n="HIAT:ip">.</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T607" id="Seg_1583" n="sc" s="T602">
               <ts e="T607" id="Seg_1585" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_1587" n="HIAT:w" s="T602">Nʼemsəʔi</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1590" n="HIAT:w" s="T603">ibiʔi</ts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1594" n="HIAT:w" s="T604">i</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_1597" n="HIAT:w" s="T605">хохлы</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1600" n="HIAT:w" s="T606">ibiʔi</ts>
                  <nts id="Seg_1601" n="HIAT:ip">.</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T620" id="Seg_1603" n="sc" s="T608">
               <ts e="T616" id="Seg_1605" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_1607" n="HIAT:w" s="T608">Iʔgö</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1610" n="HIAT:w" s="T609">dĭn</ts>
                  <nts id="Seg_1611" n="HIAT:ip">,</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1614" n="HIAT:w" s="T610">kundʼo</ts>
                  <nts id="Seg_1615" n="HIAT:ip">,</nts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1618" n="HIAT:w" s="T611">šide</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1621" n="HIAT:w" s="T612">nüdʼi</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1624" n="HIAT:w" s="T613">măn</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1627" n="HIAT:w" s="T614">šaːbiam</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1630" n="HIAT:w" s="T615">dĭn</ts>
                  <nts id="Seg_1631" n="HIAT:ip">.</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_1634" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_1636" n="HIAT:w" s="T616">Dĭgəttə</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_1639" n="HIAT:w" s="T617">bazo</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_1642" n="HIAT:w" s="T618">döbər</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_1645" n="HIAT:w" s="T619">deʔpi</ts>
                  <nts id="Seg_1646" n="HIAT:ip">.</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T623" id="Seg_1648" n="sc" s="T621">
               <ts e="T623" id="Seg_1650" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_1652" n="HIAT:w" s="T621">Dĭʔnə</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1655" n="HIAT:w" s="T622">nükenə</ts>
                  <nts id="Seg_1656" n="HIAT:ip">.</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T627" id="Seg_1658" n="sc" s="T624">
               <ts e="T627" id="Seg_1660" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_1662" n="HIAT:w" s="T624">I</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1665" n="HIAT:w" s="T625">dĭn</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_1668" n="HIAT:w" s="T626">amnolaʔbəm</ts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T629" id="Seg_1671" n="sc" s="T628">
               <ts e="T629" id="Seg_1673" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_1675" n="HIAT:w" s="T628">Kabarləj</ts>
                  <nts id="Seg_1676" n="HIAT:ip">.</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T637" id="Seg_1678" n="sc" s="T634">
               <ts e="T637" id="Seg_1680" n="HIAT:u" s="T634">
                  <nts id="Seg_1681" n="HIAT:ip">(</nts>
                  <nts id="Seg_1682" n="HIAT:ip">(</nts>
                  <ats e="T635" id="Seg_1683" n="HIAT:non-pho" s="T634">BRK</ats>
                  <nts id="Seg_1684" n="HIAT:ip">)</nts>
                  <nts id="Seg_1685" n="HIAT:ip">)</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1688" n="HIAT:w" s="T635">Останови</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_1691" n="HIAT:w" s="T636">маленько</ts>
                  <nts id="Seg_1692" n="HIAT:ip">.</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T656" id="Seg_1694" n="sc" s="T641">
               <ts e="T656" id="Seg_1696" n="HIAT:u" s="T641">
                  <nts id="Seg_1697" n="HIAT:ip">(</nts>
                  <ts e="T642" id="Seg_1699" n="HIAT:w" s="T641">Это</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_1702" n="HIAT:w" s="T642">я=</ts>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_1706" n="HIAT:w" s="T644">Это</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_1709" n="HIAT:w" s="T645">я</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_1712" n="HIAT:w" s="T646">вот</ts>
                  <nts id="Seg_1713" n="HIAT:ip">,</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_1716" n="HIAT:w" s="T647">всё</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1718" n="HIAT:ip">(</nts>
                  <ts e="T649" id="Seg_1720" n="HIAT:w" s="T648">как=</ts>
                  <nts id="Seg_1721" n="HIAT:ip">)</nts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_1724" n="HIAT:w" s="T649">сколь</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_1727" n="HIAT:w" s="T650">он</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_1730" n="HIAT:w" s="T651">мне</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_1733" n="HIAT:w" s="T652">годов</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_1736" n="HIAT:w" s="T654">писал</ts>
                  <nts id="Seg_1737" n="HIAT:ip">.</nts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T674" id="Seg_1739" n="sc" s="T670">
               <ts e="T674" id="Seg_1741" n="HIAT:u" s="T670">
                  <ts e="T672" id="Seg_1743" n="HIAT:w" s="T670">Ага</ts>
                  <nts id="Seg_1744" n="HIAT:ip">,</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_1747" n="HIAT:w" s="T672">ну-ну</ts>
                  <nts id="Seg_1748" n="HIAT:ip">.</nts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T677" id="Seg_1750" n="sc" s="T675">
               <ts e="T677" id="Seg_1752" n="HIAT:u" s="T675">
                  <ts e="T677" id="Seg_1754" n="HIAT:w" s="T675">Можно</ts>
                  <nts id="Seg_1755" n="HIAT:ip">.</nts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T698" id="Seg_1757" n="sc" s="T680">
               <ts e="T698" id="Seg_1759" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_1761" n="HIAT:w" s="T680">Когда</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_1764" n="HIAT:w" s="T681">он</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_1767" n="HIAT:w" s="T682">всё</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_1770" n="HIAT:w" s="T683">писал</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_1773" n="HIAT:w" s="T684">ко</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_1776" n="HIAT:w" s="T685">мне</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_1779" n="HIAT:w" s="T686">четыре</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_1782" n="HIAT:w" s="T687">года</ts>
                  <nts id="Seg_1783" n="HIAT:ip">,</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_1786" n="HIAT:w" s="T688">а</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_1789" n="HIAT:w" s="T689">я</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_1792" n="HIAT:w" s="T690">отписывалася:</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1794" n="HIAT:ip">"</nts>
                  <ts e="T692" id="Seg_1796" n="HIAT:w" s="T691">Приеду</ts>
                  <nts id="Seg_1797" n="HIAT:ip">,</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_1800" n="HIAT:w" s="T692">приеду</ts>
                  <nts id="Seg_1801" n="HIAT:ip">"</nts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_1805" n="HIAT:w" s="T693">но</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_1808" n="HIAT:w" s="T694">не</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_1811" n="HIAT:w" s="T695">с</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_1814" n="HIAT:w" s="T696">кем</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_1817" n="HIAT:w" s="T697">было</ts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T712" id="Seg_1820" n="sc" s="T699">
               <ts e="T712" id="Seg_1822" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_1824" n="HIAT:w" s="T699">Хотела</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_1827" n="HIAT:w" s="T700">с</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_1830" n="HIAT:w" s="T701">одним</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_1833" n="HIAT:w" s="T702">человеком</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_1836" n="HIAT:w" s="T703">приехать</ts>
                  <nts id="Seg_1837" n="HIAT:ip">,</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_1840" n="HIAT:w" s="T704">он</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_1843" n="HIAT:w" s="T705">шибко</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_1846" n="HIAT:w" s="T706">больной</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_1849" n="HIAT:w" s="T707">был</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_1853" n="HIAT:w" s="T708">хворал</ts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_1857" n="HIAT:w" s="T709">а</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_1860" n="HIAT:w" s="T710">потом</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_1863" n="HIAT:w" s="T711">помер</ts>
                  <nts id="Seg_1864" n="HIAT:ip">.</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_1866" n="sc" s="T713">
               <ts e="T723" id="Seg_1868" n="HIAT:u" s="T713">
                  <nts id="Seg_1869" n="HIAT:ip">(</nts>
                  <ts e="T714" id="Seg_1871" n="HIAT:w" s="T713">Тогда</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_1874" n="HIAT:w" s="T714">я=</ts>
                  <nts id="Seg_1875" n="HIAT:ip">)</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_1878" n="HIAT:w" s="T715">Тогда</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_1881" n="HIAT:w" s="T716">он</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_1884" n="HIAT:w" s="T717">пишет</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_1887" n="HIAT:w" s="T718">мне:</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1889" n="HIAT:ip">"</nts>
                  <ts e="T720" id="Seg_1891" n="HIAT:w" s="T719">Я</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_1894" n="HIAT:w" s="T720">приеду</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_1897" n="HIAT:w" s="T721">за</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_1900" n="HIAT:w" s="T722">тобой</ts>
                  <nts id="Seg_1901" n="HIAT:ip">"</nts>
                  <nts id="Seg_1902" n="HIAT:ip">.</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T728" id="Seg_1904" n="sc" s="T724">
               <ts e="T728" id="Seg_1906" n="HIAT:u" s="T724">
                  <nts id="Seg_1907" n="HIAT:ip">(</nts>
                  <ts e="T725" id="Seg_1909" n="HIAT:w" s="T724">И</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_1912" n="HIAT:w" s="T725">п=</ts>
                  <nts id="Seg_1913" n="HIAT:ip">)</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_1916" n="HIAT:w" s="T726">И</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_1919" n="HIAT:w" s="T727">приехал</ts>
                  <nts id="Seg_1920" n="HIAT:ip">.</nts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T737" id="Seg_1922" n="sc" s="T729">
               <ts e="T731" id="Seg_1924" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_1926" n="HIAT:w" s="T729">Четырнадцатого</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_1929" n="HIAT:w" s="T730">числа</ts>
                  <nts id="Seg_1930" n="HIAT:ip">.</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_1933" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_1935" n="HIAT:w" s="T731">Я</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_1938" n="HIAT:w" s="T732">ждала</ts>
                  <nts id="Seg_1939" n="HIAT:ip">,</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_1942" n="HIAT:w" s="T733">а</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_1945" n="HIAT:w" s="T734">он</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_1948" n="HIAT:w" s="T735">вперед</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_1951" n="HIAT:w" s="T736">приехал</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_1954" n="sc" s="T738">
               <ts e="T750" id="Seg_1956" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_1958" n="HIAT:w" s="T738">Тагды</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_1961" n="HIAT:w" s="T739">пошел</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_1964" n="HIAT:w" s="T740">на</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_1967" n="HIAT:w" s="T741">мельницу</ts>
                  <nts id="Seg_1968" n="HIAT:ip">,</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_1971" n="HIAT:w" s="T742">там</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_1974" n="HIAT:w" s="T743">нашел</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_1977" n="HIAT:w" s="T744">людей</ts>
                  <nts id="Seg_1978" n="HIAT:ip">,</nts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_1981" n="HIAT:w" s="T745">на</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_1984" n="HIAT:w" s="T746">евонной</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1986" n="HIAT:ip">(</nts>
                  <ts e="T748" id="Seg_1988" n="HIAT:w" s="T747">язы-</ts>
                  <nts id="Seg_1989" n="HIAT:ip">)</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1991" n="HIAT:ip">(</nts>
                  <ts e="T749" id="Seg_1993" n="HIAT:w" s="T748">наречиях</ts>
                  <nts id="Seg_1994" n="HIAT:ip">)</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_1997" n="HIAT:w" s="T749">говорят</ts>
                  <nts id="Seg_1998" n="HIAT:ip">.</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T754" id="Seg_2000" n="sc" s="T751">
               <ts e="T754" id="Seg_2002" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2004" n="HIAT:w" s="T751">И</ts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2007" n="HIAT:w" s="T752">пришел</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2010" n="HIAT:w" s="T753">назад</ts>
                  <nts id="Seg_2011" n="HIAT:ip">.</nts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T769" id="Seg_2013" n="sc" s="T755">
               <ts e="T769" id="Seg_2015" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_2017" n="HIAT:w" s="T755">Тады</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2020" n="HIAT:w" s="T756">пошел</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2023" n="HIAT:w" s="T757">в</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2026" n="HIAT:w" s="T758">баню</ts>
                  <nts id="Seg_2027" n="HIAT:ip">,</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2030" n="HIAT:w" s="T759">я</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2033" n="HIAT:w" s="T760">пошла</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2036" n="HIAT:w" s="T761">ему</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2039" n="HIAT:w" s="T762">показала</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2042" n="HIAT:w" s="T763">горячу</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2045" n="HIAT:w" s="T764">воду</ts>
                  <nts id="Seg_2046" n="HIAT:ip">,</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2049" n="HIAT:w" s="T765">холодну</ts>
                  <nts id="Seg_2050" n="HIAT:ip">,</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2053" n="HIAT:w" s="T766">и</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2056" n="HIAT:w" s="T767">мыло</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2059" n="HIAT:w" s="T768">внесла</ts>
                  <nts id="Seg_2060" n="HIAT:ip">.</nts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T773" id="Seg_2062" n="sc" s="T770">
               <ts e="T773" id="Seg_2064" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2066" n="HIAT:w" s="T770">Он</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2069" n="HIAT:w" s="T771">вымылся</ts>
                  <nts id="Seg_2070" n="HIAT:ip">,</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2073" n="HIAT:w" s="T772">пришел</ts>
                  <nts id="Seg_2074" n="HIAT:ip">.</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T785" id="Seg_2076" n="sc" s="T774">
               <ts e="T785" id="Seg_2078" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_2080" n="HIAT:w" s="T774">Я</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2083" n="HIAT:w" s="T775">говорю:</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2085" n="HIAT:ip">(</nts>
                  <nts id="Seg_2086" n="HIAT:ip">"</nts>
                  <ts e="T777" id="Seg_2088" n="HIAT:w" s="T776">Ешь</ts>
                  <nts id="Seg_2089" n="HIAT:ip">"</nts>
                  <nts id="Seg_2090" n="HIAT:ip">,</nts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2093" n="HIAT:w" s="T777">ему</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2096" n="HIAT:w" s="T778">ужин</ts>
                  <nts id="Seg_2097" n="HIAT:ip">,</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2100" n="HIAT:w" s="T779">а</ts>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2104" n="HIAT:w" s="T780">он</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2107" n="HIAT:w" s="T781">говорит:</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2109" n="HIAT:ip">"</nts>
                  <ts e="T783" id="Seg_2111" n="HIAT:w" s="T782">Я</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2114" n="HIAT:w" s="T783">там</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2117" n="HIAT:w" s="T784">поужинал</ts>
                  <nts id="Seg_2118" n="HIAT:ip">"</nts>
                  <nts id="Seg_2119" n="HIAT:ip">.</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T789" id="Seg_2121" n="sc" s="T786">
               <ts e="T789" id="Seg_2123" n="HIAT:u" s="T786">
                  <ts e="T787" id="Seg_2125" n="HIAT:w" s="T786">И</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2128" n="HIAT:w" s="T787">легли</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2131" n="HIAT:w" s="T788">спать</ts>
                  <nts id="Seg_2132" n="HIAT:ip">.</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T801" id="Seg_2134" n="sc" s="T790">
               <ts e="T801" id="Seg_2136" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_2138" n="HIAT:w" s="T790">Утром</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2141" n="HIAT:w" s="T791">встали</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2145" n="HIAT:w" s="T792">я</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2148" n="HIAT:w" s="T793">наварила</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2151" n="HIAT:w" s="T794">яиц</ts>
                  <nts id="Seg_2152" n="HIAT:ip">,</nts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2155" n="HIAT:w" s="T795">приготовила</ts>
                  <nts id="Seg_2156" n="HIAT:ip">,</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2159" n="HIAT:w" s="T796">поели</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2162" n="HIAT:w" s="T797">и</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2165" n="HIAT:w" s="T798">пошли</ts>
                  <nts id="Seg_2166" n="HIAT:ip">,</nts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2169" n="HIAT:w" s="T799">на</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2172" n="HIAT:w" s="T800">автобус</ts>
                  <nts id="Seg_2173" n="HIAT:ip">.</nts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T808" id="Seg_2175" n="sc" s="T802">
               <ts e="T808" id="Seg_2177" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_2179" n="HIAT:w" s="T802">Тагды</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2182" n="HIAT:w" s="T803">сяли</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2185" n="HIAT:w" s="T804">и</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2188" n="HIAT:w" s="T805">в</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2191" n="HIAT:w" s="T806">Агинско</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2194" n="HIAT:w" s="T807">приехали</ts>
                  <nts id="Seg_2195" n="HIAT:ip">.</nts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T822" id="Seg_2197" n="sc" s="T809">
               <ts e="T822" id="Seg_2199" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_2201" n="HIAT:w" s="T809">Там</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2204" n="HIAT:w" s="T810">тоже</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2206" n="HIAT:ip">(</nts>
                  <ts e="T812" id="Seg_2208" n="HIAT:w" s="T811">н-</ts>
                  <nts id="Seg_2209" n="HIAT:ip">)</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2212" n="HIAT:w" s="T812">пошли</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2215" n="HIAT:w" s="T813">к</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2218" n="HIAT:w" s="T814">сестрам</ts>
                  <nts id="Seg_2219" n="HIAT:ip">,</nts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2222" n="HIAT:w" s="T815">тамо-ка</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2225" n="HIAT:w" s="T816">были</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2228" n="HIAT:w" s="T817">до</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2231" n="HIAT:w" s="T818">вечера</ts>
                  <nts id="Seg_2232" n="HIAT:ip">,</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2235" n="HIAT:w" s="T819">до</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2238" n="HIAT:w" s="T820">пяти</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2241" n="HIAT:w" s="T821">часов</ts>
                  <nts id="Seg_2242" n="HIAT:ip">.</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T830" id="Seg_2244" n="sc" s="T823">
               <ts e="T830" id="Seg_2246" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_2248" n="HIAT:w" s="T823">А</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2251" n="HIAT:w" s="T824">в</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2254" n="HIAT:w" s="T825">пять</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2257" n="HIAT:w" s="T826">часов</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2260" n="HIAT:w" s="T827">поехали</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2263" n="HIAT:w" s="T828">в</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2266" n="HIAT:w" s="T829">Уяр</ts>
                  <nts id="Seg_2267" n="HIAT:ip">.</nts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T838" id="Seg_2269" n="sc" s="T831">
               <ts e="T838" id="Seg_2271" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_2273" n="HIAT:w" s="T831">Там</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2276" n="HIAT:w" s="T832">ночевали</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2279" n="HIAT:w" s="T833">тоже</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2282" n="HIAT:w" s="T834">у</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2285" n="HIAT:w" s="T835">верующих</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2288" n="HIAT:w" s="T836">у</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2291" n="HIAT:w" s="T837">братьев</ts>
                  <nts id="Seg_2292" n="HIAT:ip">.</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T848" id="Seg_2294" n="sc" s="T839">
               <ts e="T848" id="Seg_2296" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_2298" n="HIAT:w" s="T839">На</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2301" n="HIAT:w" s="T840">другой</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2304" n="HIAT:w" s="T841">день</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2307" n="HIAT:w" s="T842">сели</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2310" n="HIAT:w" s="T843">в</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2313" n="HIAT:w" s="T844">Красноярска</ts>
                  <nts id="Seg_2314" n="HIAT:ip">,</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2317" n="HIAT:w" s="T845">приехал</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2320" n="HIAT:w" s="T846">из</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2323" n="HIAT:w" s="T847">Красноярского</ts>
                  <nts id="Seg_2324" n="HIAT:ip">.</nts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T862" id="Seg_2326" n="sc" s="T849">
               <ts e="T856" id="Seg_2328" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_2330" n="HIAT:w" s="T849">На</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_2333" n="HIAT:w" s="T850">самолет</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2336" n="HIAT:w" s="T851">сяли</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2339" n="HIAT:w" s="T852">и</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2342" n="HIAT:w" s="T853">приехали</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2345" n="HIAT:w" s="T854">в</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2348" n="HIAT:w" s="T855">Новосибирска</ts>
                  <nts id="Seg_2349" n="HIAT:ip">.</nts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_2352" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_2354" n="HIAT:w" s="T856">С</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2357" n="HIAT:w" s="T857">Новосибирского</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2360" n="HIAT:w" s="T858">коло</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2363" n="HIAT:w" s="T859">Москвы</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2366" n="HIAT:w" s="T860">есть</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2369" n="HIAT:w" s="T861">посадка</ts>
                  <nts id="Seg_2370" n="HIAT:ip">.</nts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T877" id="Seg_2372" n="sc" s="T863">
               <ts e="T877" id="Seg_2374" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_2376" n="HIAT:w" s="T863">А</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2379" n="HIAT:w" s="T864">потом</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2382" n="HIAT:w" s="T865">с</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2385" n="HIAT:w" s="T866">этой</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2388" n="HIAT:w" s="T867">посадки</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2391" n="HIAT:w" s="T868">мы</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2393" n="HIAT:ip">(</nts>
                  <ts e="T870" id="Seg_2395" n="HIAT:w" s="T869">поехали</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2398" n="HIAT:w" s="T870">в=</ts>
                  <nts id="Seg_2399" n="HIAT:ip">)</nts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2402" n="HIAT:w" s="T871">съездили</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2405" n="HIAT:w" s="T872">на</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2408" n="HIAT:w" s="T873">легковой</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2411" n="HIAT:w" s="T874">машине</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2414" n="HIAT:w" s="T875">в</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2417" n="HIAT:w" s="T876">Москву</ts>
                  <nts id="Seg_2418" n="HIAT:ip">.</nts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T886" id="Seg_2420" n="sc" s="T878">
               <ts e="T886" id="Seg_2422" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_2424" n="HIAT:w" s="T878">Он</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_2427" n="HIAT:w" s="T879">там</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2430" n="HIAT:w" s="T880">снял</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2433" n="HIAT:w" s="T881">меня</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2435" n="HIAT:ip">(</nts>
                  <ts e="T883" id="Seg_2437" n="HIAT:w" s="T882">с</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2440" n="HIAT:w" s="T883">этим=</ts>
                  <nts id="Seg_2441" n="HIAT:ip">)</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_2444" n="HIAT:w" s="T884">с</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_2447" n="HIAT:w" s="T885">людям</ts>
                  <nts id="Seg_2448" n="HIAT:ip">.</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T902" id="Seg_2450" n="sc" s="T887">
               <ts e="T902" id="Seg_2452" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_2454" n="HIAT:w" s="T887">А</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_2457" n="HIAT:w" s="T888">потом</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2460" n="HIAT:w" s="T889">воротились</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_2463" n="HIAT:w" s="T890">назад</ts>
                  <nts id="Seg_2464" n="HIAT:ip">,</nts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_2467" n="HIAT:w" s="T891">тады</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2470" n="HIAT:w" s="T892">сяли</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2473" n="HIAT:w" s="T893">опять</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_2476" n="HIAT:w" s="T894">на</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_2479" n="HIAT:w" s="T895">самолет</ts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_2482" n="HIAT:w" s="T896">и</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2484" n="HIAT:ip">(</nts>
                  <ts e="T898" id="Seg_2486" n="HIAT:w" s="T897">при-</ts>
                  <nts id="Seg_2487" n="HIAT:ip">)</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_2490" n="HIAT:w" s="T898">приехали</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_2493" n="HIAT:w" s="T899">сюда</ts>
                  <nts id="Seg_2494" n="HIAT:ip">,</nts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2497" n="HIAT:w" s="T900">в</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_2500" n="HIAT:w" s="T901">эту</ts>
                  <nts id="Seg_2501" n="HIAT:ip">…</nts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T915" id="Seg_2503" n="sc" s="T903">
               <ts e="T915" id="Seg_2505" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_2507" n="HIAT:w" s="T903">В</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_2510" n="HIAT:w" s="T904">этот</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2513" n="HIAT:w" s="T905">город</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_2516" n="HIAT:w" s="T906">или</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_2519" n="HIAT:w" s="T907">в</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_2522" n="HIAT:w" s="T908">деревню</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2525" n="HIAT:w" s="T909">или</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2528" n="HIAT:w" s="T910">как</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_2531" n="HIAT:w" s="T911">ли</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_2534" n="HIAT:w" s="T912">я</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_2537" n="HIAT:w" s="T913">сказала</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_2540" n="HIAT:w" s="T914">уж</ts>
                  <nts id="Seg_2541" n="HIAT:ip">?</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T927" id="Seg_2543" n="sc" s="T916">
               <ts e="T927" id="Seg_2545" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_2547" n="HIAT:w" s="T916">Вот</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_2550" n="HIAT:w" s="T917">сюда</ts>
                  <nts id="Seg_2551" n="HIAT:ip">,</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_2554" n="HIAT:w" s="T920">в</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_2557" n="HIAT:w" s="T922">Таллину</ts>
                  <nts id="Seg_2558" n="HIAT:ip">,</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_2561" n="HIAT:w" s="T925">в</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_2564" n="HIAT:w" s="T926">Таллину</ts>
                  <nts id="Seg_2565" n="HIAT:ip">.</nts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T937" id="Seg_2567" n="sc" s="T933">
               <ts e="T937" id="Seg_2569" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_2571" n="HIAT:w" s="T933">А</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_2574" n="HIAT:w" s="T934">потом</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_2577" n="HIAT:w" s="T935">отседа</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2579" n="HIAT:ip">(</nts>
                  <ts e="T937" id="Seg_2581" n="HIAT:w" s="T936">уех-</ts>
                  <nts id="Seg_2582" n="HIAT:ip">)</nts>
                  <nts id="Seg_2583" n="HIAT:ip">…</nts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T963" id="Seg_2585" n="sc" s="T938">
               <ts e="T951" id="Seg_2587" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_2589" n="HIAT:w" s="T938">Вот</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_2592" n="HIAT:w" s="T939">только</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_2595" n="HIAT:w" s="T940">не</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_2598" n="HIAT:w" s="T941">сказала</ts>
                  <nts id="Seg_2599" n="HIAT:ip">,</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_2602" n="HIAT:w" s="T942">мы</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_2605" n="HIAT:w" s="T943">уехали</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_2608" n="HIAT:w" s="T944">на</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_2611" n="HIAT:w" s="T945">машине</ts>
                  <nts id="Seg_2612" n="HIAT:ip">,</nts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_2615" n="HIAT:w" s="T946">а</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_2618" n="HIAT:w" s="T947">я</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_2621" n="HIAT:w" s="T948">сказала</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_2624" n="HIAT:w" s="T949">kambibaʔ</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2626" n="HIAT:ip">(</nts>
                  <ts e="T951" id="Seg_2628" n="HIAT:w" s="T950">уе-</ts>
                  <nts id="Seg_2629" n="HIAT:ip">)</nts>
                  <nts id="Seg_2630" n="HIAT:ip">.</nts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T963" id="Seg_2633" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_2635" n="HIAT:w" s="T951">Ну</ts>
                  <nts id="Seg_2636" n="HIAT:ip">,</nts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_2639" n="HIAT:w" s="T952">все</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_2642" n="HIAT:w" s="T954">равно</ts>
                  <nts id="Seg_2643" n="HIAT:ip">,</nts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_2646" n="HIAT:w" s="T956">уехали</ts>
                  <nts id="Seg_2647" n="HIAT:ip">,</nts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_2650" n="HIAT:w" s="T958">уехали</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_2653" n="HIAT:w" s="T961">в</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_2656" n="HIAT:w" s="T962">Тарту</ts>
                  <nts id="Seg_2657" n="HIAT:ip">.</nts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T970" id="Seg_2659" n="sc" s="T964">
               <ts e="T970" id="Seg_2661" n="HIAT:u" s="T964">
                  <ts e="T965" id="Seg_2663" n="HIAT:w" s="T964">И</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_2666" n="HIAT:w" s="T965">там</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_2669" n="HIAT:w" s="T966">я</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_2672" n="HIAT:w" s="T967">три</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_2675" n="HIAT:w" s="T968">недели</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_2678" n="HIAT:w" s="T969">жила</ts>
                  <nts id="Seg_2679" n="HIAT:ip">.</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T985" id="Seg_2681" n="sc" s="T971">
               <ts e="T985" id="Seg_2683" n="HIAT:u" s="T971">
                  <ts e="T972" id="Seg_2685" n="HIAT:w" s="T971">Тады</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_2688" n="HIAT:w" s="T972">он</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_2691" n="HIAT:w" s="T973">меня</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_2694" n="HIAT:w" s="T974">увез</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_2697" n="HIAT:w" s="T975">туды</ts>
                  <nts id="Seg_2698" n="HIAT:ip">,</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_2701" n="HIAT:w" s="T976">где</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_2704" n="HIAT:w" s="T977">цурияны</ts>
                  <nts id="Seg_2705" n="HIAT:ip">,</nts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_2708" n="HIAT:w" s="T978">там</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_2711" n="HIAT:w" s="T979">две</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_2714" n="HIAT:w" s="T980">ночи</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_2717" n="HIAT:w" s="T981">ночевали</ts>
                  <nts id="Seg_2718" n="HIAT:ip">,</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_2721" n="HIAT:w" s="T982">это</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_2724" n="HIAT:w" s="T983">я</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_2727" n="HIAT:w" s="T984">говорила</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1000" id="Seg_2730" n="sc" s="T986">
               <ts e="T1000" id="Seg_2732" n="HIAT:u" s="T986">
                  <ts e="T987" id="Seg_2734" n="HIAT:w" s="T986">А</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_2737" n="HIAT:w" s="T987">потом</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_2740" n="HIAT:w" s="T988">оттель</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_2743" n="HIAT:w" s="T989">обратно</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_2746" n="HIAT:w" s="T990">сюды</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_2749" n="HIAT:w" s="T991">вот</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_2752" n="HIAT:w" s="T992">привез</ts>
                  <nts id="Seg_2753" n="HIAT:ip">,</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_2756" n="HIAT:w" s="T993">и</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_2759" n="HIAT:w" s="T994">уже</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_2762" n="HIAT:w" s="T995">тут</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_2765" n="HIAT:w" s="T996">вот</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_2768" n="HIAT:w" s="T997">более</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_2771" n="HIAT:w" s="T998">недели</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_2774" n="HIAT:w" s="T999">живу</ts>
                  <nts id="Seg_2775" n="HIAT:ip">.</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1004" id="Seg_2777" n="sc" s="T1001">
               <ts e="T1004" id="Seg_2779" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_2781" n="HIAT:w" s="T1001">У</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_2784" n="HIAT:w" s="T1002">этой</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_2787" n="HIAT:w" s="T1003">женщины</ts>
                  <nts id="Seg_2788" n="HIAT:ip">.</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1009" id="Seg_2790" n="sc" s="T1005">
               <ts e="T1009" id="Seg_2792" n="HIAT:u" s="T1005">
                  <ts e="T1006" id="Seg_2794" n="HIAT:w" s="T1005">Это</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_2797" n="HIAT:w" s="T1006">всё</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_2800" n="HIAT:w" s="T1007">я</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_2803" n="HIAT:w" s="T1008">рассказала</ts>
                  <nts id="Seg_2804" n="HIAT:ip">.</nts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1033" id="Seg_2806" n="sc" s="T1026">
               <ts e="T1033" id="Seg_2808" n="HIAT:u" s="T1026">
                  <ts e="T1027" id="Seg_2810" n="HIAT:w" s="T1026">Ну</ts>
                  <nts id="Seg_2811" n="HIAT:ip">,</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_2814" n="HIAT:w" s="T1027">не</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_2817" n="HIAT:w" s="T1028">слыхать</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_2820" n="HIAT:w" s="T1031">шума</ts>
                  <nts id="Seg_2821" n="HIAT:ip">.</nts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1047" id="Seg_2823" n="sc" s="T1035">
               <ts e="T1047" id="Seg_2825" n="HIAT:u" s="T1035">
                  <ts e="T1038" id="Seg_2827" n="HIAT:w" s="T1035">Ребятишки</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_2830" n="HIAT:w" s="T1038">где-то</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_2833" n="HIAT:w" s="T1041">притихли</ts>
                  <nts id="Seg_2834" n="HIAT:ip">,</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_2837" n="HIAT:w" s="T1044">они</ts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_2840" n="HIAT:w" s="T1046">вон</ts>
                  <nts id="Seg_2841" n="HIAT:ip">…</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1070" id="Seg_2843" n="sc" s="T1050">
               <ts e="T1067" id="Seg_2845" n="HIAT:u" s="T1050">
                  <ts e="T1052" id="Seg_2847" n="HIAT:w" s="T1050">До</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_2850" n="HIAT:w" s="T1052">этого</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_2853" n="HIAT:w" s="T1054">было</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_2856" n="HIAT:w" s="T1055">здорово</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_2859" n="HIAT:w" s="T1056">кричали</ts>
                  <nts id="Seg_2860" n="HIAT:ip">,</nts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_2863" n="HIAT:w" s="T1057">может</ts>
                  <nts id="Seg_2864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_2866" n="HIAT:w" s="T1058">пообедали</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_2869" n="HIAT:w" s="T1059">да</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_2872" n="HIAT:w" s="T1060">спать</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_2875" n="HIAT:w" s="T1061">легли</ts>
                  <nts id="Seg_2876" n="HIAT:ip">,</nts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_2879" n="HIAT:w" s="T1062">я</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_2882" n="HIAT:w" s="T1063">и</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_2885" n="HIAT:w" s="T1064">то</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_2888" n="HIAT:w" s="T1065">маленько</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_2891" n="HIAT:w" s="T1066">задремала</ts>
                  <nts id="Seg_2892" n="HIAT:ip">.</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1070" id="Seg_2895" n="HIAT:u" s="T1067">
                  <ts e="T1068" id="Seg_2897" n="HIAT:w" s="T1067">Ждали</ts>
                  <nts id="Seg_2898" n="HIAT:ip">,</nts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_2901" n="HIAT:w" s="T1068">ждали</ts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_2904" n="HIAT:w" s="T1069">вас</ts>
                  <nts id="Seg_2905" n="HIAT:ip">.</nts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1136" id="Seg_2907" n="sc" s="T1118">
               <ts e="T1126" id="Seg_2909" n="HIAT:u" s="T1118">
                  <ts e="T1119" id="Seg_2911" n="HIAT:w" s="T1118">Не</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_2914" n="HIAT:w" s="T1119">знаю</ts>
                  <nts id="Seg_2915" n="HIAT:ip">,</nts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_2918" n="HIAT:w" s="T1121">что</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_2921" n="HIAT:w" s="T1124">сказать</ts>
                  <nts id="Seg_2922" n="HIAT:ip">.</nts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1136" id="Seg_2925" n="HIAT:u" s="T1126">
                  <ts e="T1130" id="Seg_2927" n="HIAT:w" s="T1126">Что</ts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_2930" n="HIAT:w" s="T1130">еще</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1136" id="Seg_2933" n="HIAT:w" s="T1133">сказать</ts>
                  <nts id="Seg_2934" n="HIAT:ip">.</nts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1191" id="Seg_2936" n="sc" s="T1173">
               <ts e="T1191" id="Seg_2938" n="HIAT:u" s="T1173">
                  <ts e="T1174" id="Seg_2940" n="HIAT:w" s="T1173">Ну</ts>
                  <nts id="Seg_2941" n="HIAT:ip">,</nts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_2944" n="HIAT:w" s="T1174">и</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_2947" n="HIAT:w" s="T1175">я</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1177" id="Seg_2950" n="HIAT:w" s="T1176">тоже</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_2953" n="HIAT:w" s="T1177">и</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1179" id="Seg_2956" n="HIAT:w" s="T1178">так</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_2959" n="HIAT:w" s="T1179">как</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_2962" n="HIAT:w" s="T1180">племянник</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_2965" n="HIAT:w" s="T1181">меня</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_2968" n="HIAT:w" s="T1182">не</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_2971" n="HIAT:w" s="T1183">пускал</ts>
                  <nts id="Seg_2972" n="HIAT:ip">,</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_2975" n="HIAT:w" s="T1184">я</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_2978" n="HIAT:w" s="T1185">говорила</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1187" id="Seg_2981" n="HIAT:w" s="T1186">али</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_2984" n="HIAT:w" s="T1187">нет</ts>
                  <nts id="Seg_2985" n="HIAT:ip">,</nts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1189" id="Seg_2988" n="HIAT:w" s="T1188">я</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_2991" n="HIAT:w" s="T1189">забыла</ts>
                  <nts id="Seg_2992" n="HIAT:ip">.</nts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1227" id="Seg_2994" n="sc" s="T1219">
               <ts e="T1227" id="Seg_2996" n="HIAT:u" s="T1219">
                  <ts e="T1220" id="Seg_2998" n="HIAT:w" s="T1219">Dĭgəttə</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_3001" n="HIAT:w" s="T1220">Arpit</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_3004" n="HIAT:w" s="T1221">šobi</ts>
                  <nts id="Seg_3005" n="HIAT:ip">,</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_3008" n="HIAT:w" s="T1222">măn</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_3011" n="HIAT:w" s="T1223">племянник</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_3014" n="HIAT:w" s="T1224">ej</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_3017" n="HIAT:w" s="T1225">öʔlubi</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1227" id="Seg_3020" n="HIAT:w" s="T1226">меня</ts>
                  <nts id="Seg_3021" n="HIAT:ip">.</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1233" id="Seg_3023" n="sc" s="T1228">
               <ts e="T1233" id="Seg_3025" n="HIAT:u" s="T1228">
                  <ts e="T1229" id="Seg_3027" n="HIAT:w" s="T1228">A</ts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1230" id="Seg_3030" n="HIAT:w" s="T1229">невестка</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1231" id="Seg_3033" n="HIAT:w" s="T1230">măndə:</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3035" n="HIAT:ip">"</nts>
                  <ts e="T1232" id="Seg_3037" n="HIAT:w" s="T1231">Kanaʔ</ts>
                  <nts id="Seg_3038" n="HIAT:ip">,</nts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1233" id="Seg_3041" n="HIAT:w" s="T1232">kanaʔ</ts>
                  <nts id="Seg_3042" n="HIAT:ip">"</nts>
                  <nts id="Seg_3043" n="HIAT:ip">.</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1238" id="Seg_3045" n="sc" s="T1234">
               <ts e="T1238" id="Seg_3047" n="HIAT:u" s="T1234">
                  <ts e="T1235" id="Seg_3049" n="HIAT:w" s="T1234">Dĭgəttə</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1236" id="Seg_3052" n="HIAT:w" s="T1235">măn</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1237" id="Seg_3055" n="HIAT:w" s="T1236">kambiam</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1238" id="Seg_3058" n="HIAT:w" s="T1237">döbər</ts>
                  <nts id="Seg_3059" n="HIAT:ip">.</nts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1249" id="Seg_3061" n="sc" s="T1239">
               <ts e="T1249" id="Seg_3063" n="HIAT:u" s="T1239">
                  <ts e="T1240" id="Seg_3065" n="HIAT:w" s="T1239">А</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_3068" n="HIAT:w" s="T1240">потом</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_3071" n="HIAT:w" s="T1241">невестка</ts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1243" id="Seg_3074" n="HIAT:w" s="T1242">говорит:</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_3077" n="HIAT:w" s="T1243">поезжай</ts>
                  <nts id="Seg_3078" n="HIAT:ip">,</nts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_3081" n="HIAT:w" s="T1244">а</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_3084" n="HIAT:w" s="T1245">племянник</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_3087" n="HIAT:w" s="T1246">говорит:</ts>
                  <nts id="Seg_3088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1248" id="Seg_3090" n="HIAT:w" s="T1247">не</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1249" id="Seg_3093" n="HIAT:w" s="T1248">езди</ts>
                  <nts id="Seg_3094" n="HIAT:ip">.</nts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1251" id="Seg_3096" n="sc" s="T1250">
               <ts e="T1251" id="Seg_3098" n="HIAT:u" s="T1250">
                  <ts e="T1251" id="Seg_3100" n="HIAT:w" s="T1250">Вот</ts>
                  <nts id="Seg_3101" n="HIAT:ip">.</nts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1253" id="Seg_3103" n="sc" s="T1252">
               <ts e="T1253" id="Seg_3105" n="HIAT:u" s="T1252">
                  <ts e="T1253" id="Seg_3107" n="HIAT:w" s="T1252">Так</ts>
                  <nts id="Seg_3108" n="HIAT:ip">.</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1257" id="Seg_3110" n="sc" s="T1254">
               <ts e="T1257" id="Seg_3112" n="HIAT:u" s="T1254">
                  <nts id="Seg_3113" n="HIAT:ip">(</nts>
                  <ts e="T1256" id="Seg_3115" n="HIAT:w" s="T1254">Так</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1257" id="Seg_3118" n="HIAT:w" s="T1256">вроде</ts>
                  <nts id="Seg_3119" n="HIAT:ip">)</nts>
                  <nts id="Seg_3120" n="HIAT:ip">.</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1265" id="Seg_3122" n="sc" s="T1258">
               <ts e="T1261" id="Seg_3124" n="HIAT:u" s="T1258">
                  <ts e="T1259" id="Seg_3126" n="HIAT:w" s="T1258">Потом</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_3129" n="HIAT:w" s="T1259">чего</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_3132" n="HIAT:w" s="T1260">еще</ts>
                  <nts id="Seg_3133" n="HIAT:ip">.</nts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1265" id="Seg_3136" n="HIAT:u" s="T1261">
                  <ts e="T1262" id="Seg_3138" n="HIAT:w" s="T1261">Потом</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1263" id="Seg_3141" n="HIAT:w" s="T1262">чего</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_3144" n="HIAT:w" s="T1263">еще</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_3147" n="HIAT:w" s="T1264">рассказать</ts>
                  <nts id="Seg_3148" n="HIAT:ip">.</nts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1274" id="Seg_3150" n="sc" s="T1267">
               <ts e="T1274" id="Seg_3152" n="HIAT:u" s="T1267">
                  <ts e="T1268" id="Seg_3154" n="HIAT:w" s="T1267">Это</ts>
                  <nts id="Seg_3155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_3157" n="HIAT:w" s="T1268">уже</ts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1270" id="Seg_3160" n="HIAT:w" s="T1269">я</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1271" id="Seg_3163" n="HIAT:w" s="T1270">говорила</ts>
                  <nts id="Seg_3164" n="HIAT:ip">,</nts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1272" id="Seg_3167" n="HIAT:w" s="T1271">как</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_3170" n="HIAT:w" s="T1272">финны</ts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_3173" n="HIAT:w" s="T1273">были</ts>
                  <nts id="Seg_3174" n="HIAT:ip">.</nts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1280" id="Seg_3176" n="sc" s="T1275">
               <ts e="T1280" id="Seg_3178" n="HIAT:u" s="T1275">
                  <ts e="T1276" id="Seg_3180" n="HIAT:w" s="T1275">Там</ts>
                  <nts id="Seg_3181" n="HIAT:ip">,</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1277" id="Seg_3184" n="HIAT:w" s="T1276">у</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1278" id="Seg_3187" n="HIAT:w" s="T1277">вас</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1279" id="Seg_3190" n="HIAT:w" s="T1278">есть</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3192" n="HIAT:ip">(</nts>
                  <ts e="T1280" id="Seg_3194" n="HIAT:w" s="T1279">наматано</ts>
                  <nts id="Seg_3195" n="HIAT:ip">)</nts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1286" id="Seg_3198" n="sc" s="T1281">
               <ts e="T1286" id="Seg_3200" n="HIAT:u" s="T1281">
                  <ts e="T1282" id="Seg_3202" n="HIAT:w" s="T1281">Чего</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_3205" n="HIAT:w" s="T1282">еще</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_3208" n="HIAT:w" s="T1283">сказать</ts>
                  <nts id="Seg_3209" n="HIAT:ip">,</nts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1285" id="Seg_3212" n="HIAT:w" s="T1284">не</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1286" id="Seg_3215" n="HIAT:w" s="T1285">знаю</ts>
                  <nts id="Seg_3216" n="HIAT:ip">.</nts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1304" id="Seg_3218" n="sc" s="T1295">
               <ts e="T1304" id="Seg_3220" n="HIAT:u" s="T1295">
                  <ts e="T1298" id="Seg_3222" n="HIAT:w" s="T1295">Ну</ts>
                  <nts id="Seg_3223" n="HIAT:ip">,</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1299" id="Seg_3226" n="HIAT:w" s="T1298">там</ts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1302" id="Seg_3229" n="HIAT:w" s="T1299">я</ts>
                  <nts id="Seg_3230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3231" n="HIAT:ip">(</nts>
                  <ts e="T1304" id="Seg_3233" n="HIAT:w" s="T1302">расска-</ts>
                  <nts id="Seg_3234" n="HIAT:ip">)</nts>
                  <nts id="Seg_3235" n="HIAT:ip">.</nts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1324" id="Seg_3237" n="sc" s="T1311">
               <ts e="T1324" id="Seg_3239" n="HIAT:u" s="T1311">
                  <ts e="T1313" id="Seg_3241" n="HIAT:w" s="T1311">Ну</ts>
                  <nts id="Seg_3242" n="HIAT:ip">,</nts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1315" id="Seg_3245" n="HIAT:w" s="T1313">чего</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3247" n="HIAT:ip">(</nts>
                  <ts e="T1316" id="Seg_3249" n="HIAT:w" s="T1315">одно</ts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1318" id="Seg_3252" n="HIAT:w" s="T1316">слово=</ts>
                  <nts id="Seg_3253" n="HIAT:ip">)</nts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_3256" n="HIAT:w" s="T1318">одно</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1320" id="Seg_3259" n="HIAT:w" s="T1319">и</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_3262" n="HIAT:w" s="T1320">то</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_3265" n="HIAT:w" s="T1321">же</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1324" id="Seg_3268" n="HIAT:w" s="T1322">говорить</ts>
                  <nts id="Seg_3269" n="HIAT:ip">.</nts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1329" id="Seg_3271" n="sc" s="T1325">
               <ts e="T1329" id="Seg_3273" n="HIAT:u" s="T1325">
                  <ts e="T1326" id="Seg_3275" n="HIAT:w" s="T1325">Погоди</ts>
                  <nts id="Seg_3276" n="HIAT:ip">,</nts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1327" id="Seg_3279" n="HIAT:w" s="T1326">друго</ts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1328" id="Seg_3282" n="HIAT:w" s="T1327">надо</ts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3284" n="HIAT:ip">(</nts>
                  <nts id="Seg_3285" n="HIAT:ip">(</nts>
                  <ats e="T1329" id="Seg_3286" n="HIAT:non-pho" s="T1328">…</ats>
                  <nts id="Seg_3287" n="HIAT:ip">)</nts>
                  <nts id="Seg_3288" n="HIAT:ip">)</nts>
                  <nts id="Seg_3289" n="HIAT:ip">.</nts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1373" id="Seg_3291" n="sc" s="T1368">
               <ts e="T1373" id="Seg_3293" n="HIAT:u" s="T1368">
                  <nts id="Seg_3294" n="HIAT:ip">(</nts>
                  <ts e="T1369" id="Seg_3296" n="HIAT:w" s="T1368">Там=</ts>
                  <nts id="Seg_3297" n="HIAT:ip">)</nts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1370" id="Seg_3300" n="HIAT:w" s="T1369">Там</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1371" id="Seg_3303" n="HIAT:w" s="T1370">три</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_3306" n="HIAT:w" s="T1371">было</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1373" id="Seg_3309" n="HIAT:w" s="T1372">их</ts>
                  <nts id="Seg_3310" n="HIAT:ip">.</nts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1389" id="Seg_3312" n="sc" s="T1377">
               <ts e="T1389" id="Seg_3314" n="HIAT:u" s="T1377">
                  <ts e="T1378" id="Seg_3316" n="HIAT:w" s="T1377">Три</ts>
                  <nts id="Seg_3317" n="HIAT:ip">,</nts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3319" n="HIAT:ip">(</nts>
                  <ts e="T1379" id="Seg_3321" n="HIAT:w" s="T1378">и</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1380" id="Seg_3324" n="HIAT:w" s="T1379">все=</ts>
                  <nts id="Seg_3325" n="HIAT:ip">)</nts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1381" id="Seg_3328" n="HIAT:w" s="T1380">на</ts>
                  <nts id="Seg_3329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1382" id="Seg_3331" n="HIAT:w" s="T1381">всех</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_3334" n="HIAT:w" s="T1382">говорила</ts>
                  <nts id="Seg_3335" n="HIAT:ip">,</nts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_3338" n="HIAT:w" s="T1383">они</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_3341" n="HIAT:w" s="T1385">знают</ts>
                  <nts id="Seg_3342" n="HIAT:ip">,</nts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1387" id="Seg_3345" n="HIAT:w" s="T1386">где</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_3348" n="HIAT:w" s="T1387">вышло</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1389" id="Seg_3351" n="HIAT:w" s="T1388">хорошо</ts>
                  <nts id="Seg_3352" n="HIAT:ip">.</nts>
                  <nts id="Seg_3353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1449" id="Seg_3354" n="sc" s="T1444">
               <ts e="T1449" id="Seg_3356" n="HIAT:u" s="T1444">
                  <ts e="T1446" id="Seg_3358" n="HIAT:w" s="T1444">У</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_3361" n="HIAT:w" s="T1446">её</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1448" id="Seg_3364" n="HIAT:w" s="T1447">нет</ts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1449" id="Seg_3367" n="HIAT:w" s="T1448">радио</ts>
                  <nts id="Seg_3368" n="HIAT:ip">.</nts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1460" id="Seg_3370" n="sc" s="T1453">
               <ts e="T1460" id="Seg_3372" n="HIAT:u" s="T1453">
                  <ts e="T1454" id="Seg_3374" n="HIAT:w" s="T1453">А</ts>
                  <nts id="Seg_3375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1455" id="Seg_3377" n="HIAT:w" s="T1454">это</ts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_3380" n="HIAT:w" s="T1455">не</ts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1458" id="Seg_3383" n="HIAT:w" s="T1457">знаю</ts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1460" id="Seg_3386" n="HIAT:w" s="T1458">чего</ts>
                  <nts id="Seg_3387" n="HIAT:ip">.</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1485" id="Seg_3389" n="sc" s="T1463">
               <ts e="T1474" id="Seg_3391" n="HIAT:u" s="T1463">
                  <ts e="T1464" id="Seg_3393" n="HIAT:w" s="T1463">Она</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1465" id="Seg_3396" n="HIAT:w" s="T1464">вчера</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1467" id="Seg_3399" n="HIAT:w" s="T1465">говорила:</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3401" n="HIAT:ip">"</nts>
                  <ts e="T1468" id="Seg_3403" n="HIAT:w" s="T1467">Жалко</ts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1469" id="Seg_3406" n="HIAT:w" s="T1468">нету</ts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1470" id="Seg_3409" n="HIAT:w" s="T1469">радио</ts>
                  <nts id="Seg_3410" n="HIAT:ip">,</nts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471" id="Seg_3413" n="HIAT:w" s="T1470">может</ts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1472" id="Seg_3416" n="HIAT:w" s="T1471">быть</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1473" id="Seg_3419" n="HIAT:w" s="T1472">бы</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1474" id="Seg_3422" n="HIAT:w" s="T1473">слухали</ts>
                  <nts id="Seg_3423" n="HIAT:ip">"</nts>
                  <nts id="Seg_3424" n="HIAT:ip">.</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1485" id="Seg_3427" n="HIAT:u" s="T1474">
                  <ts e="T1475" id="Seg_3429" n="HIAT:w" s="T1474">А</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1477" id="Seg_3432" n="HIAT:w" s="T1475">я</ts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1478" id="Seg_3435" n="HIAT:w" s="T1477">говорю:</ts>
                  <nts id="Seg_3436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3437" n="HIAT:ip">"</nts>
                  <ts e="T1480" id="Seg_3439" n="HIAT:w" s="T1478">Может</ts>
                  <nts id="Seg_3440" n="HIAT:ip">,</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1481" id="Seg_3443" n="HIAT:w" s="T1480">еще</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1483" id="Seg_3446" n="HIAT:w" s="T1481">не</ts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1484" id="Seg_3449" n="HIAT:w" s="T1483">скоро</ts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1485" id="Seg_3452" n="HIAT:w" s="T1484">справят</ts>
                  <nts id="Seg_3453" n="HIAT:ip">"</nts>
                  <nts id="Seg_3454" n="HIAT:ip">.</nts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1532" id="Seg_3456" n="sc" s="T1527">
               <ts e="T1532" id="Seg_3458" n="HIAT:u" s="T1527">
                  <ts e="T1528" id="Seg_3460" n="HIAT:w" s="T1527">Они</ts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1529" id="Seg_3463" n="HIAT:w" s="T1528">же</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1530" id="Seg_3466" n="HIAT:w" s="T1529">скоро</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1531" id="Seg_3469" n="HIAT:w" s="T1530">пошли</ts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1532" id="Seg_3472" n="HIAT:w" s="T1531">так</ts>
                  <nts id="Seg_3473" n="HIAT:ip">.</nts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1543" id="Seg_3475" n="sc" s="T1533">
               <ts e="T1543" id="Seg_3477" n="HIAT:u" s="T1533">
                  <ts e="T1534" id="Seg_3479" n="HIAT:w" s="T1533">И</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1535" id="Seg_3482" n="HIAT:w" s="T1534">мы</ts>
                  <nts id="Seg_3483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1536" id="Seg_3485" n="HIAT:w" s="T1535">после</ts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1537" id="Seg_3488" n="HIAT:w" s="T1536">вас</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1538" id="Seg_3491" n="HIAT:w" s="T1537">тоже</ts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1539" id="Seg_3494" n="HIAT:w" s="T1538">скоро</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1540" id="Seg_3497" n="HIAT:w" s="T1539">пошли</ts>
                  <nts id="Seg_3498" n="HIAT:ip">,</nts>
                  <nts id="Seg_3499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1541" id="Seg_3501" n="HIAT:w" s="T1540">немного</ts>
                  <nts id="Seg_3502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1542" id="Seg_3504" n="HIAT:w" s="T1541">были</ts>
                  <nts id="Seg_3505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1543" id="Seg_3507" n="HIAT:w" s="T1542">там</ts>
                  <nts id="Seg_3508" n="HIAT:ip">.</nts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1607" id="Seg_3510" n="sc" s="T1604">
               <ts e="T1607" id="Seg_3512" n="HIAT:u" s="T1604">
                  <nts id="Seg_3513" n="HIAT:ip">(</nts>
                  <ts e="T1605" id="Seg_3515" n="HIAT:w" s="T1604">Погодить</ts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1606" id="Seg_3518" n="HIAT:w" s="T1605">надо</ts>
                  <nts id="Seg_3519" n="HIAT:ip">)</nts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1607" id="Seg_3522" n="HIAT:w" s="T1606">надо</ts>
                  <nts id="Seg_3523" n="HIAT:ip">.</nts>
                  <nts id="Seg_3524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1627" id="Seg_3525" n="sc" s="T1612">
               <ts e="T1627" id="Seg_3527" n="HIAT:u" s="T1612">
                  <ts e="T1613" id="Seg_3529" n="HIAT:w" s="T1612">Нас</ts>
                  <nts id="Seg_3530" n="HIAT:ip">,</nts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1614" id="Seg_3533" n="HIAT:w" s="T1613">Агафон</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1615" id="Seg_3536" n="HIAT:w" s="T1614">Иванович</ts>
                  <nts id="Seg_3537" n="HIAT:ip">,</nts>
                  <nts id="Seg_3538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1616" id="Seg_3540" n="HIAT:w" s="T1615">тоже</ts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1617" id="Seg_3543" n="HIAT:w" s="T1616">всё</ts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1618" id="Seg_3546" n="HIAT:w" s="T1617">забывается</ts>
                  <nts id="Seg_3547" n="HIAT:ip">,</nts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1619" id="Seg_3550" n="HIAT:w" s="T1618">уж</ts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1621" id="Seg_3553" n="HIAT:w" s="T1619">много</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1623" id="Seg_3556" n="HIAT:w" s="T1621">лет</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1625" id="Seg_3559" n="HIAT:w" s="T1623">не</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1627" id="Seg_3562" n="HIAT:w" s="T1625">говорила</ts>
                  <nts id="Seg_3563" n="HIAT:ip">.</nts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1631" id="Seg_3565" n="sc" s="T1628">
               <ts e="T1631" id="Seg_3567" n="HIAT:u" s="T1628">
                  <nts id="Seg_3568" n="HIAT:ip">(</nts>
                  <nts id="Seg_3569" n="HIAT:ip">(</nts>
                  <ats e="T1629" id="Seg_3570" n="HIAT:non-pho" s="T1628">…</ats>
                  <nts id="Seg_3571" n="HIAT:ip">)</nts>
                  <nts id="Seg_3572" n="HIAT:ip">)</nts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1630" id="Seg_3575" n="HIAT:w" s="T1629">еще</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1631" id="Seg_3578" n="HIAT:w" s="T1630">маленько</ts>
                  <nts id="Seg_3579" n="HIAT:ip">.</nts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1641" id="Seg_3581" n="sc" s="T1634">
               <ts e="T1641" id="Seg_3583" n="HIAT:u" s="T1634">
                  <ts e="T1635" id="Seg_3585" n="HIAT:w" s="T1634">Не</ts>
                  <nts id="Seg_3586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1636" id="Seg_3588" n="HIAT:w" s="T1635">с</ts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1638" id="Seg_3591" n="HIAT:w" s="T1636">кем</ts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1641" id="Seg_3594" n="HIAT:w" s="T1638">разговаривать</ts>
                  <nts id="Seg_3595" n="HIAT:ip">.</nts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1652" id="Seg_3597" n="sc" s="T1643">
               <ts e="T1652" id="Seg_3599" n="HIAT:u" s="T1643">
                  <ts e="T1646" id="Seg_3601" n="HIAT:w" s="T1643">Там</ts>
                  <nts id="Seg_3602" n="HIAT:ip">,</nts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1647" id="Seg_3605" n="HIAT:w" s="T1646">пойду</ts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1648" id="Seg_3608" n="HIAT:w" s="T1647">к</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1649" id="Seg_3611" n="HIAT:w" s="T1648">своей</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1650" id="Seg_3614" n="HIAT:w" s="T1649">племяннице</ts>
                  <nts id="Seg_3615" n="HIAT:ip">,</nts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1651" id="Seg_3618" n="HIAT:w" s="T1650">толмачу</ts>
                  <nts id="Seg_3619" n="HIAT:ip">,</nts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1652" id="Seg_3622" n="HIAT:w" s="T1651">толмачу</ts>
                  <nts id="Seg_3623" n="HIAT:ip">.</nts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1660" id="Seg_3625" n="sc" s="T1653">
               <ts e="T1660" id="Seg_3627" n="HIAT:u" s="T1653">
                  <ts e="T1654" id="Seg_3629" n="HIAT:w" s="T1653">Тут</ts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1655" id="Seg_3632" n="HIAT:w" s="T1654">зять</ts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1656" id="Seg_3635" n="HIAT:w" s="T1655">прибежит:</ts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3637" n="HIAT:ip">"</nts>
                  <ts e="T1657" id="Seg_3639" n="HIAT:w" s="T1656">Заболтали</ts>
                  <nts id="Seg_3640" n="HIAT:ip">"</nts>
                  <nts id="Seg_3641" n="HIAT:ip">,</nts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3643" n="HIAT:ip">—</nts>
                  <nts id="Seg_3644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1658" id="Seg_3646" n="HIAT:w" s="T1657">закричит</ts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1659" id="Seg_3649" n="HIAT:w" s="T1658">на</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1660" id="Seg_3652" n="HIAT:w" s="T1659">нас</ts>
                  <nts id="Seg_3653" n="HIAT:ip">.</nts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1664" id="Seg_3655" n="sc" s="T1661">
               <ts e="T1664" id="Seg_3657" n="HIAT:u" s="T1661">
                  <ts e="T1662" id="Seg_3659" n="HIAT:w" s="T1661">Внука</ts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1663" id="Seg_3662" n="HIAT:w" s="T1662">своего</ts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1664" id="Seg_3665" n="HIAT:w" s="T1663">заставляю</ts>
                  <nts id="Seg_3666" n="HIAT:ip">.</nts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1673" id="Seg_3668" n="sc" s="T1665">
               <ts e="T1673" id="Seg_3670" n="HIAT:u" s="T1665">
                  <ts e="T1666" id="Seg_3672" n="HIAT:w" s="T1665">Даже</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1667" id="Seg_3675" n="HIAT:w" s="T1666">который</ts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1668" id="Seg_3678" n="HIAT:w" s="T1667">настоящий</ts>
                  <nts id="Seg_3679" n="HIAT:ip">,</nts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1669" id="Seg_3682" n="HIAT:w" s="T1668">и</ts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1670" id="Seg_3685" n="HIAT:w" s="T1669">вот</ts>
                  <nts id="Seg_3686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1671" id="Seg_3688" n="HIAT:w" s="T1670">там</ts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672" id="Seg_3691" n="HIAT:w" s="T1671">эта</ts>
                  <nts id="Seg_3692" n="HIAT:ip">,</nts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1673" id="Seg_3695" n="HIAT:w" s="T1672">приходила</ts>
                  <nts id="Seg_3696" n="HIAT:ip">…</nts>
                  <nts id="Seg_3697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1678" id="Seg_3698" n="sc" s="T1674">
               <ts e="T1678" id="Seg_3700" n="HIAT:u" s="T1674">
                  <nts id="Seg_3701" n="HIAT:ip">(</nts>
                  <ts e="T1675" id="Seg_3703" n="HIAT:w" s="T1674">Ага-</ts>
                  <nts id="Seg_3704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1676" id="Seg_3706" n="HIAT:w" s="T1675">Аган-</ts>
                  <nts id="Seg_3707" n="HIAT:ip">)</nts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1677" id="Seg_3710" n="HIAT:w" s="T1676">Агафьей</ts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1678" id="Seg_3713" n="HIAT:w" s="T1677">звать</ts>
                  <nts id="Seg_3714" n="HIAT:ip">.</nts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1684" id="Seg_3716" n="sc" s="T1679">
               <ts e="T1684" id="Seg_3718" n="HIAT:u" s="T1679">
                  <ts e="T1680" id="Seg_3720" n="HIAT:w" s="T1679">И</ts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1681" id="Seg_3723" n="HIAT:w" s="T1680">то</ts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1682" id="Seg_3726" n="HIAT:w" s="T1681">она</ts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1683" id="Seg_3729" n="HIAT:w" s="T1682">не</ts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1684" id="Seg_3732" n="HIAT:w" s="T1683">знает</ts>
                  <nts id="Seg_3733" n="HIAT:ip">.</nts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1698" id="Seg_3735" n="sc" s="T1685">
               <ts e="T1694" id="Seg_3737" n="HIAT:u" s="T1685">
                  <ts e="T1686" id="Seg_3739" n="HIAT:w" s="T1685">Сказала</ts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3741" n="HIAT:ip">"</nts>
                  <ts e="T1687" id="Seg_3743" n="HIAT:w" s="T1686">сахар</ts>
                  <nts id="Seg_3744" n="HIAT:ip">"</nts>
                  <nts id="Seg_3745" n="HIAT:ip">,</nts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1688" id="Seg_3748" n="HIAT:w" s="T1687">а</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1689" id="Seg_3751" n="HIAT:w" s="T1688">она</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1690" id="Seg_3754" n="HIAT:w" s="T1689">говорит:</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3756" n="HIAT:ip">"</nts>
                  <ts e="T1691" id="Seg_3758" n="HIAT:w" s="T1690">Врет</ts>
                  <nts id="Seg_3759" n="HIAT:ip">,</nts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1692" id="Seg_3762" n="HIAT:w" s="T1691">сахар</ts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1693" id="Seg_3765" n="HIAT:w" s="T1692">не</ts>
                  <nts id="Seg_3766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1694" id="Seg_3768" n="HIAT:w" s="T1693">так</ts>
                  <nts id="Seg_3769" n="HIAT:ip">"</nts>
                  <nts id="Seg_3770" n="HIAT:ip">.</nts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1698" id="Seg_3773" n="HIAT:u" s="T1694">
                  <nts id="Seg_3774" n="HIAT:ip">"</nts>
                  <ts e="T1695" id="Seg_3776" n="HIAT:w" s="T1694">Ну</ts>
                  <nts id="Seg_3777" n="HIAT:ip">,</nts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3779" n="HIAT:ip">—</nts>
                  <nts id="Seg_3780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1696" id="Seg_3782" n="HIAT:w" s="T1695">говорит</ts>
                  <nts id="Seg_3783" n="HIAT:ip">,</nts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3785" n="HIAT:ip">—</nts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1697" id="Seg_3788" n="HIAT:w" s="T1696">скажи</ts>
                  <nts id="Seg_3789" n="HIAT:ip">,</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1698" id="Seg_3792" n="HIAT:w" s="T1697">как</ts>
                  <nts id="Seg_3793" n="HIAT:ip">"</nts>
                  <nts id="Seg_3794" n="HIAT:ip">.</nts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1705" id="Seg_3796" n="sc" s="T1699">
               <ts e="T1705" id="Seg_3798" n="HIAT:u" s="T1699">
                  <ts e="T1700" id="Seg_3800" n="HIAT:w" s="T1699">И</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1701" id="Seg_3803" n="HIAT:w" s="T1700">она</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1702" id="Seg_3806" n="HIAT:w" s="T1701">сама</ts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1703" id="Seg_3809" n="HIAT:w" s="T1702">не</ts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1705" id="Seg_3812" n="HIAT:w" s="T1703">знает</ts>
                  <nts id="Seg_3813" n="HIAT:ip">.</nts>
                  <nts id="Seg_3814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1710" id="Seg_3815" n="sc" s="T1707">
               <ts e="T1710" id="Seg_3817" n="HIAT:u" s="T1707">
                  <ts e="T1710" id="Seg_3819" n="HIAT:w" s="T1707">Ну</ts>
                  <nts id="Seg_3820" n="HIAT:ip">.</nts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1715" id="Seg_3822" n="sc" s="T1713">
               <ts e="T1715" id="Seg_3824" n="HIAT:u" s="T1713">
                  <ts e="T1714" id="Seg_3826" n="HIAT:w" s="T1713">Ну</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1715" id="Seg_3829" n="HIAT:w" s="T1714">чего</ts>
                  <nts id="Seg_3830" n="HIAT:ip">?</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1722" id="Seg_3832" n="sc" s="T1717">
               <ts e="T1722" id="Seg_3834" n="HIAT:u" s="T1717">
                  <ts e="T1718" id="Seg_3836" n="HIAT:w" s="T1717">Ну</ts>
                  <nts id="Seg_3837" n="HIAT:ip">,</nts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1719" id="Seg_3840" n="HIAT:w" s="T1718">давай</ts>
                  <nts id="Seg_3841" n="HIAT:ip">,</nts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1720" id="Seg_3844" n="HIAT:w" s="T1719">открой</ts>
                  <nts id="Seg_3845" n="HIAT:ip">,</nts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1721" id="Seg_3848" n="HIAT:w" s="T1720">будем</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1722" id="Seg_3851" n="HIAT:w" s="T1721">говорить</ts>
                  <nts id="Seg_3852" n="HIAT:ip">.</nts>
                  <nts id="Seg_3853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1729" id="Seg_3854" n="sc" s="T1723">
               <ts e="T1729" id="Seg_3856" n="HIAT:u" s="T1723">
                  <ts e="T1724" id="Seg_3858" n="HIAT:w" s="T1723">Taldʼen</ts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1725" id="Seg_3861" n="HIAT:w" s="T1724">miʔ</ts>
                  <nts id="Seg_3862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1726" id="Seg_3864" n="HIAT:w" s="T1725">jaʔtarla</ts>
                  <nts id="Seg_3865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1727" id="Seg_3867" n="HIAT:w" s="T1726">kambibaʔ</ts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1728" id="Seg_3870" n="HIAT:w" s="T1727">onʼiʔ</ts>
                  <nts id="Seg_3871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1729" id="Seg_3873" n="HIAT:w" s="T1728">nükenə</ts>
                  <nts id="Seg_3874" n="HIAT:ip">.</nts>
                  <nts id="Seg_3875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1745" id="Seg_3876" n="sc" s="T1730">
               <ts e="T1734" id="Seg_3878" n="HIAT:u" s="T1730">
                  <ts e="T1731" id="Seg_3880" n="HIAT:w" s="T1730">Amnobibaʔ</ts>
                  <nts id="Seg_3881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1732" id="Seg_3883" n="HIAT:w" s="T1731">aftobustə</ts>
                  <nts id="Seg_3884" n="HIAT:ip">,</nts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1733" id="Seg_3887" n="HIAT:w" s="T1732">dĭbər</ts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1734" id="Seg_3890" n="HIAT:w" s="T1733">šobibaʔ</ts>
                  <nts id="Seg_3891" n="HIAT:ip">.</nts>
                  <nts id="Seg_3892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1740" id="Seg_3894" n="HIAT:u" s="T1734">
                  <ts e="T1735" id="Seg_3896" n="HIAT:w" s="T1734">Dĭn</ts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1736" id="Seg_3899" n="HIAT:w" s="T1735">šobiʔi</ts>
                  <nts id="Seg_3900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1737" id="Seg_3902" n="HIAT:w" s="T1736">il</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1738" id="Seg_3905" n="HIAT:w" s="T1737">dʼăbaktərzittə</ts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1739" id="Seg_3908" n="HIAT:w" s="T1738">măn</ts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1740" id="Seg_3911" n="HIAT:w" s="T1739">šĭkenə</ts>
                  <nts id="Seg_3912" n="HIAT:ip">.</nts>
                  <nts id="Seg_3913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1745" id="Seg_3915" n="HIAT:u" s="T1740">
                  <ts e="T1741" id="Seg_3917" n="HIAT:w" s="T1740">Măn</ts>
                  <nts id="Seg_3918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1742" id="Seg_3920" n="HIAT:w" s="T1741">dĭzeŋdə</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1743" id="Seg_3923" n="HIAT:w" s="T1742">bar</ts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1744" id="Seg_3926" n="HIAT:w" s="T1743">mămbiam</ts>
                  <nts id="Seg_3927" n="HIAT:ip">,</nts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1745" id="Seg_3930" n="HIAT:w" s="T1744">mămbiam</ts>
                  <nts id="Seg_3931" n="HIAT:ip">.</nts>
                  <nts id="Seg_3932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1747" id="Seg_3933" n="sc" s="T1746">
               <ts e="T1747" id="Seg_3935" n="HIAT:u" s="T1746">
                  <ts e="T1747" id="Seg_3937" n="HIAT:w" s="T1746">Dĭgəttə</ts>
                  <nts id="Seg_3938" n="HIAT:ip">…</nts>
                  <nts id="Seg_3939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1761" id="Seg_3940" n="sc" s="T1748">
               <ts e="T1752" id="Seg_3942" n="HIAT:u" s="T1748">
                  <ts e="T1749" id="Seg_3944" n="HIAT:w" s="T1748">Надо</ts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1750" id="Seg_3947" n="HIAT:w" s="T1749">еще</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1751" id="Seg_3950" n="HIAT:w" s="T1750">надумать</ts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1752" id="Seg_3953" n="HIAT:w" s="T1751">чего</ts>
                  <nts id="Seg_3954" n="HIAT:ip">.</nts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1761" id="Seg_3957" n="HIAT:u" s="T1752">
                  <ts e="T1754" id="Seg_3959" n="HIAT:w" s="T1752">Это</ts>
                  <nts id="Seg_3960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1756" id="Seg_3962" n="HIAT:w" s="T1754">вчера</ts>
                  <nts id="Seg_3963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1757" id="Seg_3965" n="HIAT:w" s="T1756">что</ts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1759" id="Seg_3968" n="HIAT:w" s="T1757">я</ts>
                  <nts id="Seg_3969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1761" id="Seg_3971" n="HIAT:w" s="T1759">ходила</ts>
                  <nts id="Seg_3972" n="HIAT:ip">.</nts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1784" id="Seg_3974" n="sc" s="T1775">
               <ts e="T1784" id="Seg_3976" n="HIAT:u" s="T1775">
                  <ts e="T1776" id="Seg_3978" n="HIAT:w" s="T1775">Мы</ts>
                  <nts id="Seg_3979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1777" id="Seg_3981" n="HIAT:w" s="T1776">вчера</ts>
                  <nts id="Seg_3982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1778" id="Seg_3984" n="HIAT:w" s="T1777">ходили</ts>
                  <nts id="Seg_3985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1779" id="Seg_3987" n="HIAT:w" s="T1778">в</ts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1780" id="Seg_3990" n="HIAT:w" s="T1779">гости</ts>
                  <nts id="Seg_3991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1781" id="Seg_3993" n="HIAT:w" s="T1780">там</ts>
                  <nts id="Seg_3994" n="HIAT:ip">,</nts>
                  <nts id="Seg_3995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1782" id="Seg_3997" n="HIAT:w" s="T1781">к</ts>
                  <nts id="Seg_3998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1783" id="Seg_4000" n="HIAT:w" s="T1782">одной</ts>
                  <nts id="Seg_4001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1784" id="Seg_4003" n="HIAT:w" s="T1783">старушке</ts>
                  <nts id="Seg_4004" n="HIAT:ip">.</nts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1797" id="Seg_4006" n="sc" s="T1785">
               <ts e="T1797" id="Seg_4008" n="HIAT:u" s="T1785">
                  <ts e="T1786" id="Seg_4010" n="HIAT:w" s="T1785">Там</ts>
                  <nts id="Seg_4011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4012" n="HIAT:ip">(</nts>
                  <ts e="T1787" id="Seg_4014" n="HIAT:w" s="T1786">приехали=</ts>
                  <nts id="Seg_4015" n="HIAT:ip">)</nts>
                  <nts id="Seg_4016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1788" id="Seg_4018" n="HIAT:w" s="T1787">пришли</ts>
                  <nts id="Seg_4019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1789" id="Seg_4021" n="HIAT:w" s="T1788">люди</ts>
                  <nts id="Seg_4022" n="HIAT:ip">,</nts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1790" id="Seg_4025" n="HIAT:w" s="T1789">и</ts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1791" id="Seg_4028" n="HIAT:w" s="T1790">мы</ts>
                  <nts id="Seg_4029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1792" id="Seg_4031" n="HIAT:w" s="T1791">разговаривали</ts>
                  <nts id="Seg_4032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4033" n="HIAT:ip">(</nts>
                  <ts e="T1793" id="Seg_4035" n="HIAT:w" s="T1792">мо-</ts>
                  <nts id="Seg_4036" n="HIAT:ip">)</nts>
                  <nts id="Seg_4037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1794" id="Seg_4039" n="HIAT:w" s="T1793">на</ts>
                  <nts id="Seg_4040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1795" id="Seg_4042" n="HIAT:w" s="T1794">мое</ts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4044" n="HIAT:ip">(</nts>
                  <ts e="T1796" id="Seg_4046" n="HIAT:w" s="T1795">ра-</ts>
                  <nts id="Seg_4047" n="HIAT:ip">)</nts>
                  <nts id="Seg_4048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1797" id="Seg_4050" n="HIAT:w" s="T1796">наречие</ts>
                  <nts id="Seg_4051" n="HIAT:ip">.</nts>
                  <nts id="Seg_4052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1799" id="Seg_4053" n="sc" s="T1798">
               <ts e="T1799" id="Seg_4055" n="HIAT:u" s="T1798">
                  <nts id="Seg_4056" n="HIAT:ip">(</nts>
                  <nts id="Seg_4057" n="HIAT:ip">(</nts>
                  <ats e="T1799" id="Seg_4058" n="HIAT:non-pho" s="T1798">BRK</ats>
                  <nts id="Seg_4059" n="HIAT:ip">)</nts>
                  <nts id="Seg_4060" n="HIAT:ip">)</nts>
                  <nts id="Seg_4061" n="HIAT:ip">.</nts>
                  <nts id="Seg_4062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1801" id="Seg_4063" n="sc" s="T1800">
               <ts e="T1801" id="Seg_4065" n="HIAT:u" s="T1800">
                  <ts e="T1801" id="Seg_4067" n="HIAT:w" s="T1800">Ладно</ts>
                  <nts id="Seg_4068" n="HIAT:ip">.</nts>
                  <nts id="Seg_4069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1806" id="Seg_4070" n="sc" s="T1802">
               <ts e="T1806" id="Seg_4072" n="HIAT:u" s="T1802">
                  <ts e="T1803" id="Seg_4074" n="HIAT:w" s="T1802">Ну</ts>
                  <nts id="Seg_4075" n="HIAT:ip">,</nts>
                  <nts id="Seg_4076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1804" id="Seg_4078" n="HIAT:w" s="T1803">чего</ts>
                  <nts id="Seg_4079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1805" id="Seg_4081" n="HIAT:w" s="T1804">еще</ts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1806" id="Seg_4084" n="HIAT:w" s="T1805">рассказать</ts>
                  <nts id="Seg_4085" n="HIAT:ip">.</nts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1853" id="Seg_4087" n="sc" s="T1844">
               <ts e="T1853" id="Seg_4089" n="HIAT:u" s="T1844">
                  <ts e="T1845" id="Seg_4091" n="HIAT:w" s="T1844">Думаешь</ts>
                  <nts id="Seg_4092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1847" id="Seg_4094" n="HIAT:w" s="T1845">много</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1848" id="Seg_4097" n="HIAT:w" s="T1847">рассказать</ts>
                  <nts id="Seg_4098" n="HIAT:ip">,</nts>
                  <nts id="Seg_4099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1849" id="Seg_4101" n="HIAT:w" s="T1848">а</ts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1850" id="Seg_4104" n="HIAT:w" s="T1849">потом</ts>
                  <nts id="Seg_4105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1851" id="Seg_4107" n="HIAT:w" s="T1850">опять</ts>
                  <nts id="Seg_4108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1853" id="Seg_4110" n="HIAT:w" s="T1851">уходит</ts>
                  <nts id="Seg_4111" n="HIAT:ip">.</nts>
                  <nts id="Seg_4112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1864" id="Seg_4113" n="sc" s="T1862">
               <ts e="T1864" id="Seg_4115" n="HIAT:u" s="T1862">
                  <ts e="T1863" id="Seg_4117" n="HIAT:w" s="T1862">Ну</ts>
                  <nts id="Seg_4118" n="HIAT:ip">,</nts>
                  <nts id="Seg_4119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1864" id="Seg_4121" n="HIAT:w" s="T1863">открой</ts>
                  <nts id="Seg_4122" n="HIAT:ip">.</nts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1874" id="Seg_4124" n="sc" s="T1865">
               <ts e="T1874" id="Seg_4126" n="HIAT:u" s="T1865">
                  <ts e="T1866" id="Seg_4128" n="HIAT:w" s="T1865">Măn</ts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1867" id="Seg_4131" n="HIAT:w" s="T1866">oʔbdəbiam</ts>
                  <nts id="Seg_4132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4133" n="HIAT:ip">(</nts>
                  <ts e="T1868" id="Seg_4135" n="HIAT:w" s="T1867">šo-</ts>
                  <nts id="Seg_4136" n="HIAT:ip">)</nts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1869" id="Seg_4139" n="HIAT:w" s="T1868">šonəsʼtə</ts>
                  <nts id="Seg_4140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1870" id="Seg_4142" n="HIAT:w" s="T1869">döbər</ts>
                  <nts id="Seg_4143" n="HIAT:ip">,</nts>
                  <nts id="Seg_4144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1871" id="Seg_4146" n="HIAT:w" s="T1870">il</ts>
                  <nts id="Seg_4147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1872" id="Seg_4149" n="HIAT:w" s="T1871">măndəʔi:</ts>
                  <nts id="Seg_4150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4151" n="HIAT:ip">"</nts>
                  <ts e="T1873" id="Seg_4153" n="HIAT:w" s="T1872">Iʔ</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1874" id="Seg_4156" n="HIAT:w" s="T1873">kanaʔ</ts>
                  <nts id="Seg_4157" n="HIAT:ip">.</nts>
                  <nts id="Seg_4158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1877" id="Seg_4159" n="sc" s="T1875">
               <ts e="T1877" id="Seg_4161" n="HIAT:u" s="T1875">
                  <ts e="T1876" id="Seg_4163" n="HIAT:w" s="T1875">Külalləl</ts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1877" id="Seg_4166" n="HIAT:w" s="T1876">dĭn</ts>
                  <nts id="Seg_4167" n="HIAT:ip">.</nts>
                  <nts id="Seg_4168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1882" id="Seg_4169" n="sc" s="T1878">
               <ts e="T1882" id="Seg_4171" n="HIAT:u" s="T1878">
                  <ts e="T1879" id="Seg_4173" n="HIAT:w" s="T1878">Măn</ts>
                  <nts id="Seg_4174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1880" id="Seg_4176" n="HIAT:w" s="T1879">mămbiam:</ts>
                  <nts id="Seg_4177" n="HIAT:ip">"</nts>
                  <nts id="Seg_4178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4179" n="HIAT:ip">"</nts>
                  <ts e="T1881" id="Seg_4181" n="HIAT:w" s="T1880">Ej</ts>
                  <nts id="Seg_4182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1882" id="Seg_4184" n="HIAT:w" s="T1881">pĭmniem</ts>
                  <nts id="Seg_4185" n="HIAT:ip">.</nts>
                  <nts id="Seg_4186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1890" id="Seg_4187" n="sc" s="T1883">
               <ts e="T1890" id="Seg_4189" n="HIAT:u" s="T1883">
                  <nts id="Seg_4190" n="HIAT:ip">(</nts>
                  <ts e="T1884" id="Seg_4192" n="HIAT:w" s="T1883">Dĭn</ts>
                  <nts id="Seg_4193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1885" id="Seg_4195" n="HIAT:w" s="T1884">m-</ts>
                  <nts id="Seg_4196" n="HIAT:ip">)</nts>
                  <nts id="Seg_4197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1886" id="Seg_4199" n="HIAT:w" s="T1885">Măna</ts>
                  <nts id="Seg_4200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1887" id="Seg_4202" n="HIAT:w" s="T1886">dĭn</ts>
                  <nts id="Seg_4203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1888" id="Seg_4205" n="HIAT:w" s="T1887">tože</ts>
                  <nts id="Seg_4206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1889" id="Seg_4208" n="HIAT:w" s="T1888">dʼünə</ts>
                  <nts id="Seg_4209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1890" id="Seg_4211" n="HIAT:w" s="T1889">elləʔi</ts>
                  <nts id="Seg_4212" n="HIAT:ip">.</nts>
                  <nts id="Seg_4213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1892" id="Seg_4214" n="sc" s="T1891">
               <ts e="T1892" id="Seg_4216" n="HIAT:u" s="T1891">
                  <ts e="T1892" id="Seg_4218" n="HIAT:w" s="T1891">Kabarləj</ts>
                  <nts id="Seg_4219" n="HIAT:ip">.</nts>
                  <nts id="Seg_4220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T10" id="Seg_4221" n="sc" s="T1">
               <ts e="T2" id="Seg_4223" n="e" s="T1">Как, </ts>
               <ts e="T3" id="Seg_4225" n="e" s="T2">не </ts>
               <ts e="T4" id="Seg_4227" n="e" s="T3">знаю, </ts>
               <ts e="T5" id="Seg_4229" n="e" s="T4">((…)) </ts>
               <ts e="T6" id="Seg_4231" n="e" s="T5">сколько </ts>
               <ts e="T8" id="Seg_4233" n="e" s="T6">ты </ts>
               <ts e="T10" id="Seg_4235" n="e" s="T8">работал. </ts>
            </ts>
            <ts e="T47" id="Seg_4236" n="sc" s="T43">
               <ts e="T44" id="Seg_4238" n="e" s="T43">Да, </ts>
               <ts e="T45" id="Seg_4240" n="e" s="T44">(мужик) </ts>
               <ts e="T46" id="Seg_4242" n="e" s="T45">такой, </ts>
               <ts e="T47" id="Seg_4244" n="e" s="T46">наверное. </ts>
            </ts>
            <ts e="T97" id="Seg_4245" n="sc" s="T92">
               <ts e="T95" id="Seg_4247" n="e" s="T92">Ну, </ts>
               <ts e="T97" id="Seg_4249" n="e" s="T95">ну. </ts>
            </ts>
            <ts e="T124" id="Seg_4250" n="sc" s="T102">
               <ts e="T103" id="Seg_4252" n="e" s="T102">(Он-) </ts>
               <ts e="T105" id="Seg_4254" n="e" s="T103">Он </ts>
               <ts e="T107" id="Seg_4256" n="e" s="T105">мне </ts>
               <ts e="T109" id="Seg_4258" n="e" s="T107">все </ts>
               <ts e="T111" id="Seg_4260" n="e" s="T109">писал, </ts>
               <ts e="T112" id="Seg_4262" n="e" s="T111">звал, </ts>
               <ts e="T113" id="Seg_4264" n="e" s="T112">а </ts>
               <ts e="T114" id="Seg_4266" n="e" s="T113">я </ts>
               <ts e="T115" id="Seg_4268" n="e" s="T114">ему </ts>
               <ts e="T116" id="Seg_4270" n="e" s="T115">написала: </ts>
               <ts e="T117" id="Seg_4272" n="e" s="T116">Агафон </ts>
               <ts e="T120" id="Seg_4274" n="e" s="T117">Иванович </ts>
               <ts e="T122" id="Seg_4276" n="e" s="T120">приедет, </ts>
               <ts e="T124" id="Seg_4278" n="e" s="T122">тогда… </ts>
            </ts>
            <ts e="T136" id="Seg_4279" n="sc" s="T130">
               <ts e="T132" id="Seg_4281" n="e" s="T130">(Он </ts>
               <ts e="T134" id="Seg_4283" n="e" s="T132">г-) </ts>
               <ts e="T135" id="Seg_4285" n="e" s="T134">Он </ts>
               <ts e="T136" id="Seg_4287" n="e" s="T135">потома-ка… </ts>
            </ts>
            <ts e="T146" id="Seg_4288" n="sc" s="T137">
               <ts e="T138" id="Seg_4290" n="e" s="T137">Он </ts>
               <ts e="T139" id="Seg_4292" n="e" s="T138">мне </ts>
               <ts e="T140" id="Seg_4294" n="e" s="T139">пишет: </ts>
               <ts e="T141" id="Seg_4296" n="e" s="T140">"Вот </ts>
               <ts e="T142" id="Seg_4298" n="e" s="T141">приеду </ts>
               <ts e="T143" id="Seg_4300" n="e" s="T142">четырнадцатого </ts>
               <ts e="T144" id="Seg_4302" n="e" s="T143">числа </ts>
               <ts e="T145" id="Seg_4304" n="e" s="T144">к </ts>
               <ts e="T146" id="Seg_4306" n="e" s="T145">тебе". </ts>
            </ts>
            <ts e="T156" id="Seg_4307" n="sc" s="T147">
               <ts e="T148" id="Seg_4309" n="e" s="T147">Ну, </ts>
               <ts e="T149" id="Seg_4311" n="e" s="T148">я </ts>
               <ts e="T150" id="Seg_4313" n="e" s="T149">и </ts>
               <ts e="T151" id="Seg_4315" n="e" s="T150">дожидаю, </ts>
               <ts e="T152" id="Seg_4317" n="e" s="T151">а </ts>
               <ts e="T153" id="Seg_4319" n="e" s="T152">он </ts>
               <ts e="T154" id="Seg_4321" n="e" s="T153">вперед, </ts>
               <ts e="T155" id="Seg_4323" n="e" s="T154">скорее </ts>
               <ts e="T156" id="Seg_4325" n="e" s="T155">приехал. </ts>
            </ts>
            <ts e="T166" id="Seg_4326" n="sc" s="T157">
               <ts e="T159" id="Seg_4328" n="e" s="T157">Ну, </ts>
               <ts e="T161" id="Seg_4330" n="e" s="T159">я </ts>
               <ts e="T163" id="Seg_4332" n="e" s="T161">уже </ts>
               <ts e="T164" id="Seg_4334" n="e" s="T163">не </ts>
               <ts e="T165" id="Seg_4336" n="e" s="T164">отказалась </ts>
               <ts e="T166" id="Seg_4338" n="e" s="T165">потом. </ts>
            </ts>
            <ts e="T177" id="Seg_4339" n="sc" s="T167">
               <ts e="T168" id="Seg_4341" n="e" s="T167">Племянник </ts>
               <ts e="T169" id="Seg_4343" n="e" s="T168">не </ts>
               <ts e="T170" id="Seg_4345" n="e" s="T169">пускал, </ts>
               <ts e="T171" id="Seg_4347" n="e" s="T170">"зачем </ts>
               <ts e="T172" id="Seg_4349" n="e" s="T171">ты </ts>
               <ts e="T173" id="Seg_4351" n="e" s="T172">поедешь" </ts>
               <ts e="T174" id="Seg_4353" n="e" s="T173">да, </ts>
               <ts e="T175" id="Seg_4355" n="e" s="T174">то, </ts>
               <ts e="T176" id="Seg_4357" n="e" s="T175">другое </ts>
               <ts e="T177" id="Seg_4359" n="e" s="T176">да. </ts>
            </ts>
            <ts e="T199" id="Seg_4360" n="sc" s="T181">
               <ts e="T183" id="Seg_4362" n="e" s="T181">Думаю, </ts>
               <ts e="T186" id="Seg_4364" n="e" s="T183">ну </ts>
               <ts e="T187" id="Seg_4366" n="e" s="T186">что, </ts>
               <ts e="T188" id="Seg_4368" n="e" s="T187">раз </ts>
               <ts e="T189" id="Seg_4370" n="e" s="T188">я </ts>
               <ts e="T190" id="Seg_4372" n="e" s="T189">так </ts>
               <ts e="T191" id="Seg_4374" n="e" s="T190">обещалась, </ts>
               <ts e="T192" id="Seg_4376" n="e" s="T191">то </ts>
               <ts e="T193" id="Seg_4378" n="e" s="T192">приеду, </ts>
               <ts e="T194" id="Seg_4380" n="e" s="T193">а </ts>
               <ts e="T195" id="Seg_4382" n="e" s="T194">теперь </ts>
               <ts e="T196" id="Seg_4384" n="e" s="T195">человек </ts>
               <ts e="T197" id="Seg_4386" n="e" s="T196">приедет, </ts>
               <ts e="T198" id="Seg_4388" n="e" s="T197">один </ts>
               <ts e="T199" id="Seg_4390" n="e" s="T198">поедет. </ts>
            </ts>
            <ts e="T209" id="Seg_4391" n="sc" s="T200">
               <ts e="T201" id="Seg_4393" n="e" s="T200">Ну, </ts>
               <ts e="T202" id="Seg_4395" n="e" s="T201">он </ts>
               <ts e="T203" id="Seg_4397" n="e" s="T202">у </ts>
               <ts e="T204" id="Seg_4399" n="e" s="T203">меня </ts>
               <ts e="T205" id="Seg_4401" n="e" s="T204">только </ts>
               <ts e="T206" id="Seg_4403" n="e" s="T205">одну </ts>
               <ts e="T207" id="Seg_4405" n="e" s="T206">ночь </ts>
               <ts e="T208" id="Seg_4407" n="e" s="T207">и </ts>
               <ts e="T209" id="Seg_4409" n="e" s="T208">ночевал. </ts>
            </ts>
            <ts e="T225" id="Seg_4410" n="sc" s="T214">
               <ts e="T220" id="Seg_4412" n="e" s="T214">Ночевал, </ts>
               <ts e="T225" id="Seg_4414" n="e" s="T220">ночевал. </ts>
            </ts>
            <ts e="T232" id="Seg_4415" n="sc" s="T226">
               <ts e="T228" id="Seg_4417" n="e" s="T226">И </ts>
               <ts e="T229" id="Seg_4419" n="e" s="T228">в </ts>
               <ts e="T230" id="Seg_4421" n="e" s="T229">баню </ts>
               <ts e="T231" id="Seg_4423" n="e" s="T230">сходил, </ts>
               <ts e="T232" id="Seg_4425" n="e" s="T231">помылся. </ts>
            </ts>
            <ts e="T238" id="Seg_4426" n="sc" s="T234">
               <ts e="T238" id="Seg_4428" n="e" s="T234">Ходил… </ts>
            </ts>
            <ts e="T248" id="Seg_4429" n="sc" s="T242">
               <ts e="T244" id="Seg_4431" n="e" s="T242">Ну. </ts>
               <ts e="T245" id="Seg_4433" n="e" s="T244">И </ts>
               <ts e="T246" id="Seg_4435" n="e" s="T245">ходил </ts>
               <ts e="T247" id="Seg_4437" n="e" s="T246">на </ts>
               <ts e="T248" id="Seg_4439" n="e" s="T247">пруд. </ts>
            </ts>
            <ts e="T250" id="Seg_4440" n="sc" s="T249">
               <ts e="T250" id="Seg_4442" n="e" s="T249">Тамо-ка. </ts>
            </ts>
            <ts e="T256" id="Seg_4443" n="sc" s="T251">
               <ts e="T252" id="Seg_4445" n="e" s="T251">И </ts>
               <ts e="T253" id="Seg_4447" n="e" s="T252">потом </ts>
               <ts e="T254" id="Seg_4449" n="e" s="T253">зашел </ts>
               <ts e="T255" id="Seg_4451" n="e" s="T254">к </ts>
               <ts e="T256" id="Seg_4453" n="e" s="T255">мельнику. </ts>
            </ts>
            <ts e="T267" id="Seg_4454" n="sc" s="T257">
               <ts e="T258" id="Seg_4456" n="e" s="T257">Пришел, </ts>
               <ts e="T259" id="Seg_4458" n="e" s="T258">говорит: </ts>
               <ts e="T260" id="Seg_4460" n="e" s="T259">"Ой, </ts>
               <ts e="T261" id="Seg_4462" n="e" s="T260">я </ts>
               <ts e="T262" id="Seg_4464" n="e" s="T261">старушку </ts>
               <ts e="T263" id="Seg_4466" n="e" s="T262">нашел, </ts>
               <ts e="T264" id="Seg_4468" n="e" s="T263">ну, </ts>
               <ts e="T265" id="Seg_4470" n="e" s="T264">на </ts>
               <ts e="T266" id="Seg_4472" n="e" s="T265">ваш </ts>
               <ts e="T267" id="Seg_4474" n="e" s="T266">язык". </ts>
            </ts>
            <ts e="T275" id="Seg_4475" n="sc" s="T268">
               <ts e="T269" id="Seg_4477" n="e" s="T268">Я </ts>
               <ts e="T270" id="Seg_4479" n="e" s="T269">говорю: </ts>
               <ts e="T271" id="Seg_4481" n="e" s="T270">"Не </ts>
               <ts e="T272" id="Seg_4483" n="e" s="T271">говори, </ts>
               <ts e="T273" id="Seg_4485" n="e" s="T272">это </ts>
               <ts e="T274" id="Seg_4487" n="e" s="T273">моя </ts>
               <ts e="T275" id="Seg_4489" n="e" s="T274">подружка". </ts>
            </ts>
            <ts e="T300" id="Seg_4490" n="sc" s="T277">
               <ts e="T279" id="Seg_4492" n="e" s="T277">Я </ts>
               <ts e="T280" id="Seg_4494" n="e" s="T279">часто </ts>
               <ts e="T281" id="Seg_4496" n="e" s="T280">к </ts>
               <ts e="T282" id="Seg_4498" n="e" s="T281">ей </ts>
               <ts e="T283" id="Seg_4500" n="e" s="T282">хожу, </ts>
               <ts e="T284" id="Seg_4502" n="e" s="T283">она </ts>
               <ts e="T285" id="Seg_4504" n="e" s="T284">вытащит </ts>
               <ts e="T286" id="Seg_4506" n="e" s="T285">Евангелие, </ts>
               <ts e="T287" id="Seg_4508" n="e" s="T286">она </ts>
               <ts e="T288" id="Seg_4510" n="e" s="T287">не </ts>
               <ts e="T289" id="Seg_4512" n="e" s="T288">умеет </ts>
               <ts e="T290" id="Seg_4514" n="e" s="T289">на </ts>
               <ts e="T291" id="Seg_4516" n="e" s="T290">русский, </ts>
               <ts e="T292" id="Seg_4518" n="e" s="T291">а </ts>
               <ts e="T293" id="Seg_4520" n="e" s="T292">я </ts>
               <ts e="T294" id="Seg_4522" n="e" s="T293">ей </ts>
               <ts e="T295" id="Seg_4524" n="e" s="T294">почитаю, </ts>
               <ts e="T296" id="Seg_4526" n="e" s="T295">с </ts>
               <ts e="T298" id="Seg_4528" n="e" s="T296">ей </ts>
               <ts e="T300" id="Seg_4530" n="e" s="T298">помолимся. </ts>
            </ts>
            <ts e="T319" id="Seg_4531" n="sc" s="T315">
               <ts e="T316" id="Seg_4533" n="e" s="T315">Ну, </ts>
               <ts e="T317" id="Seg_4535" n="e" s="T316">теперь </ts>
               <ts e="T318" id="Seg_4537" n="e" s="T317">говорить </ts>
               <ts e="T319" id="Seg_4539" n="e" s="T318">можно? </ts>
            </ts>
            <ts e="T324" id="Seg_4540" n="sc" s="T320">
               <ts e="T321" id="Seg_4542" n="e" s="T320">Măna </ts>
               <ts e="T322" id="Seg_4544" n="e" s="T321">(pʼaŋdəbi) </ts>
               <ts e="T323" id="Seg_4546" n="e" s="T322">măn </ts>
               <ts e="T324" id="Seg_4548" n="e" s="T323">kagam. </ts>
            </ts>
            <ts e="T327" id="Seg_4549" n="sc" s="T325">
               <ts e="T326" id="Seg_4551" n="e" s="T325">Teʔtə </ts>
               <ts e="T327" id="Seg_4553" n="e" s="T326">kö. </ts>
            </ts>
            <ts e="T335" id="Seg_4554" n="sc" s="T328">
               <ts e="T329" id="Seg_4556" n="e" s="T328">"Šoʔ </ts>
               <ts e="T330" id="Seg_4558" n="e" s="T329">döber, </ts>
               <ts e="T331" id="Seg_4560" n="e" s="T330">šoʔ </ts>
               <ts e="T332" id="Seg_4562" n="e" s="T331">döber". </ts>
               <ts e="T333" id="Seg_4564" n="e" s="T332">Măn </ts>
               <ts e="T334" id="Seg_4566" n="e" s="T333">(šindəzi) </ts>
               <ts e="T335" id="Seg_4568" n="e" s="T334">kalam? </ts>
            </ts>
            <ts e="T339" id="Seg_4569" n="sc" s="T336">
               <ts e="T337" id="Seg_4571" n="e" s="T336">Ĭmbidə </ts>
               <ts e="T338" id="Seg_4573" n="e" s="T337">ej </ts>
               <ts e="T339" id="Seg_4575" n="e" s="T338">tĭmnem. </ts>
            </ts>
            <ts e="T344" id="Seg_4576" n="sc" s="T340">
               <ts e="T341" id="Seg_4578" n="e" s="T340">Dĭgəttə </ts>
               <ts e="T342" id="Seg_4580" n="e" s="T341">pʼaŋdəbiam </ts>
               <ts e="T343" id="Seg_4582" n="e" s="T342">dĭʔnə </ts>
               <ts e="T344" id="Seg_4584" n="e" s="T343">sazən. </ts>
            </ts>
            <ts e="T353" id="Seg_4585" n="sc" s="T345">
               <ts e="T346" id="Seg_4587" n="e" s="T345">Mămbiam: </ts>
               <ts e="T347" id="Seg_4589" n="e" s="T346">"Šoləj </ts>
               <ts e="T348" id="Seg_4591" n="e" s="T347">Агафон </ts>
               <ts e="T349" id="Seg_4593" n="e" s="T348">Иванович </ts>
               <ts e="T350" id="Seg_4595" n="e" s="T349">(măna), </ts>
               <ts e="T351" id="Seg_4597" n="e" s="T350">dĭgəttə </ts>
               <ts e="T352" id="Seg_4599" n="e" s="T351">măn </ts>
               <ts e="T353" id="Seg_4601" n="e" s="T352">šoləm". </ts>
            </ts>
            <ts e="T361" id="Seg_4602" n="sc" s="T354">
               <ts e="T355" id="Seg_4604" n="e" s="T354">Dĭgəttə </ts>
               <ts e="T356" id="Seg_4606" n="e" s="T355">dĭ </ts>
               <ts e="T357" id="Seg_4608" n="e" s="T356">măna </ts>
               <ts e="T358" id="Seg_4610" n="e" s="T357">pʼaŋdlaʔbə: </ts>
               <ts e="T359" id="Seg_4612" n="e" s="T358">"Šoləm </ts>
               <ts e="T360" id="Seg_4614" n="e" s="T359">bostə </ts>
               <ts e="T361" id="Seg_4616" n="e" s="T360">šiʔnʼileʔ". </ts>
            </ts>
            <ts e="T369" id="Seg_4617" n="sc" s="T362">
               <ts e="T363" id="Seg_4619" n="e" s="T362">Dĭgəttə </ts>
               <ts e="T364" id="Seg_4621" n="e" s="T363">šobi </ts>
               <ts e="T365" id="Seg_4623" n="e" s="T364">miʔnʼibeʔ, </ts>
               <ts e="T366" id="Seg_4625" n="e" s="T365">măn </ts>
               <ts e="T367" id="Seg_4627" n="e" s="T366">ej </ts>
               <ts e="T368" id="Seg_4629" n="e" s="T367">edəbiem </ts>
               <ts e="T369" id="Seg_4631" n="e" s="T368">dĭm. </ts>
            </ts>
            <ts e="T375" id="Seg_4632" n="sc" s="T370">
               <ts e="T371" id="Seg_4634" n="e" s="T370">Nu, </ts>
               <ts e="T372" id="Seg_4636" n="e" s="T371">măn </ts>
               <ts e="T373" id="Seg_4638" n="e" s="T372">nʼeveskăm </ts>
               <ts e="T374" id="Seg_4640" n="e" s="T373">dĭm </ts>
               <ts e="T375" id="Seg_4642" n="e" s="T374">detleʔbə. </ts>
            </ts>
            <ts e="T380" id="Seg_4643" n="sc" s="T376">
               <ts e="T377" id="Seg_4645" n="e" s="T376">"Dö, </ts>
               <ts e="T378" id="Seg_4647" n="e" s="T377">tăn </ts>
               <ts e="T379" id="Seg_4649" n="e" s="T378">kuza </ts>
               <ts e="T380" id="Seg_4651" n="e" s="T379">šobi". </ts>
            </ts>
            <ts e="T385" id="Seg_4652" n="sc" s="T381">
               <ts e="T382" id="Seg_4654" n="e" s="T381">(M-) </ts>
               <ts e="T383" id="Seg_4656" n="e" s="T382">Măn </ts>
               <ts e="T384" id="Seg_4658" n="e" s="T383">nuʔməbiem </ts>
               <ts e="T385" id="Seg_4660" n="e" s="T384">nʼiʔnə. </ts>
            </ts>
            <ts e="T395" id="Seg_4661" n="sc" s="T386">
               <ts e="T387" id="Seg_4663" n="e" s="T386">"Oj, </ts>
               <ts e="T388" id="Seg_4665" n="e" s="T387">büžü </ts>
               <ts e="T389" id="Seg_4667" n="e" s="T388">šobial". </ts>
               <ts e="T390" id="Seg_4669" n="e" s="T389">(A </ts>
               <ts e="T391" id="Seg_4671" n="e" s="T390">dĭ=) </ts>
               <ts e="T392" id="Seg_4673" n="e" s="T391">Dĭgəttə </ts>
               <ts e="T393" id="Seg_4675" n="e" s="T392">dĭ </ts>
               <ts e="T394" id="Seg_4677" n="e" s="T393">šobi, </ts>
               <ts e="T395" id="Seg_4679" n="e" s="T394">amorbi. </ts>
            </ts>
            <ts e="T398" id="Seg_4680" n="sc" s="T396">
               <ts e="T397" id="Seg_4682" n="e" s="T396">Iʔbəbi </ts>
               <ts e="T398" id="Seg_4684" n="e" s="T397">kunolzittə. </ts>
            </ts>
            <ts e="T404" id="Seg_4685" n="sc" s="T399">
               <ts e="T400" id="Seg_4687" n="e" s="T399">Dĭgəttə </ts>
               <ts e="T401" id="Seg_4689" n="e" s="T400">uʔbdəbi, </ts>
               <ts e="T402" id="Seg_4691" n="e" s="T401">kambi </ts>
               <ts e="T403" id="Seg_4693" n="e" s="T402">bünə, </ts>
               <ts e="T404" id="Seg_4695" n="e" s="T403">tʼerbəndə. </ts>
            </ts>
            <ts e="T410" id="Seg_4696" n="sc" s="T405">
               <ts e="T406" id="Seg_4698" n="e" s="T405">Dĭn </ts>
               <ts e="T407" id="Seg_4700" n="e" s="T406">tože </ts>
               <ts e="T408" id="Seg_4702" n="e" s="T407">dĭrgit </ts>
               <ts e="T409" id="Seg_4704" n="e" s="T408">(ibi=) </ts>
               <ts e="T410" id="Seg_4706" n="e" s="T409">kubi. </ts>
            </ts>
            <ts e="T413" id="Seg_4707" n="sc" s="T411">
               <ts e="T412" id="Seg_4709" n="e" s="T411">Dʼăbaktərbi </ts>
               <ts e="T413" id="Seg_4711" n="e" s="T412">dĭn. </ts>
            </ts>
            <ts e="T417" id="Seg_4712" n="sc" s="T414">
               <ts e="T415" id="Seg_4714" n="e" s="T414">Dĭgəttə </ts>
               <ts e="T416" id="Seg_4716" n="e" s="T415">šobi </ts>
               <ts e="T417" id="Seg_4718" n="e" s="T416">maːʔndə. </ts>
            </ts>
            <ts e="T421" id="Seg_4719" n="sc" s="T418">
               <ts e="T419" id="Seg_4721" n="e" s="T418">Dĭgəttə </ts>
               <ts e="T420" id="Seg_4723" n="e" s="T419">moltʼanə </ts>
               <ts e="T421" id="Seg_4725" n="e" s="T420">kambi. </ts>
            </ts>
            <ts e="T425" id="Seg_4726" n="sc" s="T422">
               <ts e="T423" id="Seg_4728" n="e" s="T422">Măn </ts>
               <ts e="T424" id="Seg_4730" n="e" s="T423">dĭzi </ts>
               <ts e="T425" id="Seg_4732" n="e" s="T424">mĭmbiem. </ts>
            </ts>
            <ts e="T435" id="Seg_4733" n="sc" s="T426">
               <ts e="T427" id="Seg_4735" n="e" s="T426">Dĭn </ts>
               <ts e="T428" id="Seg_4737" n="e" s="T427">(b-) </ts>
               <ts e="T429" id="Seg_4739" n="e" s="T428">bü </ts>
               <ts e="T430" id="Seg_4741" n="e" s="T429">dĭʔnə, </ts>
               <ts e="T431" id="Seg_4743" n="e" s="T430">dʼibige, </ts>
               <ts e="T432" id="Seg_4745" n="e" s="T431">šišəge </ts>
               <ts e="T433" id="Seg_4747" n="e" s="T432">bü, </ts>
               <ts e="T434" id="Seg_4749" n="e" s="T433">sabən </ts>
               <ts e="T435" id="Seg_4751" n="e" s="T434">mĭbiem. </ts>
            </ts>
            <ts e="T442" id="Seg_4752" n="sc" s="T436">
               <ts e="T437" id="Seg_4754" n="e" s="T436">Dĭ </ts>
               <ts e="T438" id="Seg_4756" n="e" s="T437">băzəjdəbi </ts>
               <ts e="T439" id="Seg_4758" n="e" s="T438">dĭn, </ts>
               <ts e="T440" id="Seg_4760" n="e" s="T439">dĭgəttə </ts>
               <ts e="T441" id="Seg_4762" n="e" s="T440">šobi </ts>
               <ts e="T442" id="Seg_4764" n="e" s="T441">maːʔnʼi. </ts>
            </ts>
            <ts e="T449" id="Seg_4765" n="sc" s="T443">
               <ts e="T444" id="Seg_4767" n="e" s="T443">Mămbiam: </ts>
               <ts e="T445" id="Seg_4769" n="e" s="T444">"Amaʔ". </ts>
               <ts e="T446" id="Seg_4771" n="e" s="T445">"Măn </ts>
               <ts e="T447" id="Seg_4773" n="e" s="T446">dĭn </ts>
               <ts e="T448" id="Seg_4775" n="e" s="T447">ambiam, </ts>
               <ts e="T449" id="Seg_4777" n="e" s="T448">tʼerbəngən". </ts>
            </ts>
            <ts e="T452" id="Seg_4778" n="sc" s="T450">
               <ts e="T451" id="Seg_4780" n="e" s="T450">Ej </ts>
               <ts e="T452" id="Seg_4782" n="e" s="T451">ambi. </ts>
            </ts>
            <ts e="T457" id="Seg_4783" n="sc" s="T453">
               <ts e="T454" id="Seg_4785" n="e" s="T453">Dĭgəttə </ts>
               <ts e="T455" id="Seg_4787" n="e" s="T454">(i-) </ts>
               <ts e="T456" id="Seg_4789" n="e" s="T455">kudajdə </ts>
               <ts e="T457" id="Seg_4791" n="e" s="T456">numan üzəbibeʔ. </ts>
            </ts>
            <ts e="T464" id="Seg_4792" n="sc" s="T458">
               <ts e="T459" id="Seg_4794" n="e" s="T458">Dĭgəttə </ts>
               <ts e="T460" id="Seg_4796" n="e" s="T459">dĭ </ts>
               <ts e="T461" id="Seg_4798" n="e" s="T460">iʔbəbi, </ts>
               <ts e="T462" id="Seg_4800" n="e" s="T461">măn </ts>
               <ts e="T463" id="Seg_4802" n="e" s="T462">tože </ts>
               <ts e="T464" id="Seg_4804" n="e" s="T463">iʔbəbiem. </ts>
            </ts>
            <ts e="T469" id="Seg_4805" n="sc" s="T465">
               <ts e="T466" id="Seg_4807" n="e" s="T465">Ertən </ts>
               <ts e="T467" id="Seg_4809" n="e" s="T466">uʔbdlaʔbəm, </ts>
               <ts e="T468" id="Seg_4811" n="e" s="T467">munujʔ </ts>
               <ts e="T469" id="Seg_4813" n="e" s="T468">mĭndərbiem. </ts>
            </ts>
            <ts e="T474" id="Seg_4814" n="sc" s="T470">
               <ts e="T471" id="Seg_4816" n="e" s="T470">Amorbibaʔ, </ts>
               <ts e="T472" id="Seg_4818" n="e" s="T471">(kam-) </ts>
               <ts e="T473" id="Seg_4820" n="e" s="T472">kambibaʔ </ts>
               <ts e="T474" id="Seg_4822" n="e" s="T473">aftobustə. </ts>
            </ts>
            <ts e="T480" id="Seg_4823" n="sc" s="T475">
               <ts e="T476" id="Seg_4825" n="e" s="T475">Dĭn </ts>
               <ts e="T477" id="Seg_4827" n="e" s="T476">nubibaʔ, </ts>
               <ts e="T478" id="Seg_4829" n="e" s="T477">dĭgəttə </ts>
               <ts e="T479" id="Seg_4831" n="e" s="T478">šobi </ts>
               <ts e="T480" id="Seg_4833" n="e" s="T479">aftobus. </ts>
            </ts>
            <ts e="T487" id="Seg_4834" n="sc" s="T481">
               <ts e="T482" id="Seg_4836" n="e" s="T481">Miʔ </ts>
               <ts e="T483" id="Seg_4838" n="e" s="T482">(amno- </ts>
               <ts e="T484" id="Seg_4840" n="e" s="T483">amn-) </ts>
               <ts e="T485" id="Seg_4842" n="e" s="T484">amnobibaʔ, </ts>
               <ts e="T1918" id="Seg_4844" n="e" s="T485">Kazan </ts>
               <ts e="T486" id="Seg_4846" n="e" s="T1918">turanə </ts>
               <ts e="T487" id="Seg_4848" n="e" s="T486">kambibaʔ. </ts>
            </ts>
            <ts e="T492" id="Seg_4849" n="sc" s="T488">
               <ts e="T489" id="Seg_4851" n="e" s="T488">Dĭn </ts>
               <ts e="T490" id="Seg_4853" n="e" s="T489">tože </ts>
               <ts e="T491" id="Seg_4855" n="e" s="T490">kudajdə </ts>
               <ts e="T492" id="Seg_4857" n="e" s="T491">numan üzəbiʔi. </ts>
            </ts>
            <ts e="T499" id="Seg_4858" n="sc" s="T493">
               <ts e="T494" id="Seg_4860" n="e" s="T493">Kamən </ts>
               <ts e="T495" id="Seg_4862" n="e" s="T494">sumna </ts>
               <ts e="T496" id="Seg_4864" n="e" s="T495">šobi, </ts>
               <ts e="T497" id="Seg_4866" n="e" s="T496">dĭgəttə </ts>
               <ts e="T498" id="Seg_4868" n="e" s="T497">kambibaʔ </ts>
               <ts e="T499" id="Seg_4870" n="e" s="T498">Ujardə. </ts>
            </ts>
            <ts e="T503" id="Seg_4871" n="sc" s="T500">
               <ts e="T501" id="Seg_4873" n="e" s="T500">(Dĭn=) </ts>
               <ts e="T502" id="Seg_4875" n="e" s="T501">Dĭn </ts>
               <ts e="T503" id="Seg_4877" n="e" s="T502">šaːbibaʔ. </ts>
            </ts>
            <ts e="T510" id="Seg_4878" n="sc" s="T504">
               <ts e="T505" id="Seg_4880" n="e" s="T504">Tože </ts>
               <ts e="T506" id="Seg_4882" n="e" s="T505">dĭn </ts>
               <ts e="T507" id="Seg_4884" n="e" s="T506">(măn) </ts>
               <ts e="T508" id="Seg_4886" n="e" s="T507">ige </ts>
               <ts e="T509" id="Seg_4888" n="e" s="T508">il </ts>
               <ts e="T510" id="Seg_4890" n="e" s="T509">tože. </ts>
            </ts>
            <ts e="T515" id="Seg_4891" n="sc" s="T511">
               <ts e="T512" id="Seg_4893" n="e" s="T511">(Dĭgəttə </ts>
               <ts e="T513" id="Seg_4895" n="e" s="T512">dĭg- </ts>
               <ts e="T514" id="Seg_4897" n="e" s="T513">Dĭgəttə </ts>
               <ts e="T515" id="Seg_4899" n="e" s="T514">ert-). </ts>
            </ts>
            <ts e="T522" id="Seg_4900" n="sc" s="T516">
               <ts e="T517" id="Seg_4902" n="e" s="T516">Dĭgəttə </ts>
               <ts e="T518" id="Seg_4904" n="e" s="T517">ertən </ts>
               <ts e="T519" id="Seg_4906" n="e" s="T518">uʔbdəbibaʔ </ts>
               <ts e="T520" id="Seg_4908" n="e" s="T519">i </ts>
               <ts e="T521" id="Seg_4910" n="e" s="T520">kambibaʔ </ts>
               <ts e="T522" id="Seg_4912" n="e" s="T521">Krasnăjarskəgən. </ts>
            </ts>
            <ts e="T531" id="Seg_4913" n="sc" s="T523">
               <ts e="T524" id="Seg_4915" n="e" s="T523">Dĭn </ts>
               <ts e="T525" id="Seg_4917" n="e" s="T524">kambibaʔ </ts>
               <ts e="T526" id="Seg_4919" n="e" s="T525">gijen </ts>
               <ts e="T527" id="Seg_4921" n="e" s="T526">nʼergölaʔbəʔjə, </ts>
               <ts e="T528" id="Seg_4923" n="e" s="T527">dĭn </ts>
               <ts e="T529" id="Seg_4925" n="e" s="T528">amnobibaʔ </ts>
               <ts e="T530" id="Seg_4927" n="e" s="T529">i </ts>
               <ts e="T1917" id="Seg_4929" n="e" s="T530">kalla </ts>
               <ts e="T531" id="Seg_4931" n="e" s="T1917">dʼürbibaʔ. </ts>
            </ts>
            <ts e="T536" id="Seg_4932" n="sc" s="T532">
               <ts e="T533" id="Seg_4934" n="e" s="T532">Năvăsibirʼskanə, </ts>
               <ts e="T534" id="Seg_4936" n="e" s="T533">dĭn </ts>
               <ts e="T535" id="Seg_4938" n="e" s="T534">tože </ts>
               <ts e="T536" id="Seg_4940" n="e" s="T535">šaːbibaʔ. </ts>
            </ts>
            <ts e="T544" id="Seg_4941" n="sc" s="T537">
               <ts e="T538" id="Seg_4943" n="e" s="T537">Dĭgəttə </ts>
               <ts e="T539" id="Seg_4945" n="e" s="T538">dĭn </ts>
               <ts e="T540" id="Seg_4947" n="e" s="T539">bazo </ts>
               <ts e="T541" id="Seg_4949" n="e" s="T540">amnobibaʔ </ts>
               <ts e="T542" id="Seg_4951" n="e" s="T541">i </ts>
               <ts e="T543" id="Seg_4953" n="e" s="T542">šobibaʔ </ts>
               <ts e="T544" id="Seg_4955" n="e" s="T543">Măskvanə. </ts>
            </ts>
            <ts e="T559" id="Seg_4956" n="sc" s="T545">
               <ts e="T546" id="Seg_4958" n="e" s="T545">(D-) </ts>
               <ts e="T547" id="Seg_4960" n="e" s="T546">Dön </ts>
               <ts e="T548" id="Seg_4962" n="e" s="T547">dʼünə </ts>
               <ts e="T549" id="Seg_4964" n="e" s="T548">amnobibaʔ, </ts>
               <ts e="T550" id="Seg_4966" n="e" s="T549">dĭgəttə </ts>
               <ts e="T551" id="Seg_4968" n="e" s="T550">(amnobibaʔ=) </ts>
               <ts e="T552" id="Seg_4970" n="e" s="T551">amnobibaʔ </ts>
               <ts e="T553" id="Seg_4972" n="e" s="T552">mašinanə, </ts>
               <ts e="T554" id="Seg_4974" n="e" s="T553">kambibaʔ </ts>
               <ts e="T555" id="Seg_4976" n="e" s="T554">Măskvanə. </ts>
               <ts e="T556" id="Seg_4978" n="e" s="T555">(Dĭ-) </ts>
               <ts e="T557" id="Seg_4980" n="e" s="T556">Dĭn </ts>
               <ts e="T558" id="Seg_4982" n="e" s="T557">măna </ts>
               <ts e="T559" id="Seg_4984" n="e" s="T558">kürbi. </ts>
            </ts>
            <ts e="T564" id="Seg_4985" n="sc" s="T560">
               <ts e="T561" id="Seg_4987" n="e" s="T560">Tože </ts>
               <ts e="T562" id="Seg_4989" n="e" s="T561">il </ts>
               <ts e="T563" id="Seg_4991" n="e" s="T562">dĭn </ts>
               <ts e="T564" id="Seg_4993" n="e" s="T563">ibiʔi. </ts>
            </ts>
            <ts e="T576" id="Seg_4994" n="sc" s="T565">
               <ts e="T566" id="Seg_4996" n="e" s="T565">Dĭgəttə </ts>
               <ts e="T567" id="Seg_4998" n="e" s="T566">parbibaʔ, </ts>
               <ts e="T568" id="Seg_5000" n="e" s="T567">(m-) </ts>
               <ts e="T569" id="Seg_5002" n="e" s="T568">bazo </ts>
               <ts e="T570" id="Seg_5004" n="e" s="T569">amnolbibaʔ, </ts>
               <ts e="T571" id="Seg_5006" n="e" s="T570">i </ts>
               <ts e="T572" id="Seg_5008" n="e" s="T571">(dö=) </ts>
               <ts e="T573" id="Seg_5010" n="e" s="T572">dö </ts>
               <ts e="T574" id="Seg_5012" n="e" s="T573">turanə </ts>
               <ts e="T575" id="Seg_5014" n="e" s="T574">(so-) </ts>
               <ts e="T576" id="Seg_5016" n="e" s="T575">šobibaʔ. </ts>
            </ts>
            <ts e="T582" id="Seg_5017" n="sc" s="T577">
               <ts e="T578" id="Seg_5019" n="e" s="T577">Dĭgəttə </ts>
               <ts e="T579" id="Seg_5021" n="e" s="T578">döʔə </ts>
               <ts e="T580" id="Seg_5023" n="e" s="T579">(gu) </ts>
               <ts e="T581" id="Seg_5025" n="e" s="T580">kambibaʔ, </ts>
               <ts e="T582" id="Seg_5027" n="e" s="T581">Tartu. </ts>
            </ts>
            <ts e="T587" id="Seg_5028" n="sc" s="T583">
               <ts e="T584" id="Seg_5030" n="e" s="T583">Dĭn </ts>
               <ts e="T585" id="Seg_5032" n="e" s="T584">nagur </ts>
               <ts e="T586" id="Seg_5034" n="e" s="T585">nedʼelʼa </ts>
               <ts e="T587" id="Seg_5036" n="e" s="T586">amnobiam. </ts>
            </ts>
            <ts e="T596" id="Seg_5037" n="sc" s="T588">
               <ts e="T589" id="Seg_5039" n="e" s="T588">Bazo </ts>
               <ts e="T590" id="Seg_5041" n="e" s="T589">(kamb-) </ts>
               <ts e="T591" id="Seg_5043" n="e" s="T590">măna </ts>
               <ts e="T592" id="Seg_5045" n="e" s="T591">ibi, </ts>
               <ts e="T593" id="Seg_5047" n="e" s="T592">kambibaʔ </ts>
               <ts e="T594" id="Seg_5049" n="e" s="T593">gijen </ts>
               <ts e="T595" id="Seg_5051" n="e" s="T594">iʔgö </ts>
               <ts e="T596" id="Seg_5053" n="e" s="T595">sĭrujanə. </ts>
            </ts>
            <ts e="T601" id="Seg_5054" n="sc" s="T597">
               <ts e="T598" id="Seg_5056" n="e" s="T597">Dĭn </ts>
               <ts e="T599" id="Seg_5058" n="e" s="T598">iʔgö </ts>
               <ts e="T600" id="Seg_5060" n="e" s="T599">il </ts>
               <ts e="T601" id="Seg_5062" n="e" s="T600">ibiʔi. </ts>
            </ts>
            <ts e="T607" id="Seg_5063" n="sc" s="T602">
               <ts e="T603" id="Seg_5065" n="e" s="T602">Nʼemsəʔi </ts>
               <ts e="T604" id="Seg_5067" n="e" s="T603">ibiʔi, </ts>
               <ts e="T605" id="Seg_5069" n="e" s="T604">i </ts>
               <ts e="T606" id="Seg_5071" n="e" s="T605">хохлы </ts>
               <ts e="T607" id="Seg_5073" n="e" s="T606">ibiʔi. </ts>
            </ts>
            <ts e="T620" id="Seg_5074" n="sc" s="T608">
               <ts e="T609" id="Seg_5076" n="e" s="T608">Iʔgö </ts>
               <ts e="T610" id="Seg_5078" n="e" s="T609">dĭn, </ts>
               <ts e="T611" id="Seg_5080" n="e" s="T610">kundʼo, </ts>
               <ts e="T612" id="Seg_5082" n="e" s="T611">šide </ts>
               <ts e="T613" id="Seg_5084" n="e" s="T612">nüdʼi </ts>
               <ts e="T614" id="Seg_5086" n="e" s="T613">măn </ts>
               <ts e="T615" id="Seg_5088" n="e" s="T614">šaːbiam </ts>
               <ts e="T616" id="Seg_5090" n="e" s="T615">dĭn. </ts>
               <ts e="T617" id="Seg_5092" n="e" s="T616">Dĭgəttə </ts>
               <ts e="T618" id="Seg_5094" n="e" s="T617">bazo </ts>
               <ts e="T619" id="Seg_5096" n="e" s="T618">döbər </ts>
               <ts e="T620" id="Seg_5098" n="e" s="T619">deʔpi. </ts>
            </ts>
            <ts e="T623" id="Seg_5099" n="sc" s="T621">
               <ts e="T622" id="Seg_5101" n="e" s="T621">Dĭʔnə </ts>
               <ts e="T623" id="Seg_5103" n="e" s="T622">nükenə. </ts>
            </ts>
            <ts e="T627" id="Seg_5104" n="sc" s="T624">
               <ts e="T625" id="Seg_5106" n="e" s="T624">I </ts>
               <ts e="T626" id="Seg_5108" n="e" s="T625">dĭn </ts>
               <ts e="T627" id="Seg_5110" n="e" s="T626">amnolaʔbəm. </ts>
            </ts>
            <ts e="T629" id="Seg_5111" n="sc" s="T628">
               <ts e="T629" id="Seg_5113" n="e" s="T628">Kabarləj. </ts>
            </ts>
            <ts e="T637" id="Seg_5114" n="sc" s="T634">
               <ts e="T635" id="Seg_5116" n="e" s="T634">((BRK)) </ts>
               <ts e="T636" id="Seg_5118" n="e" s="T635">Останови </ts>
               <ts e="T637" id="Seg_5120" n="e" s="T636">маленько. </ts>
            </ts>
            <ts e="T656" id="Seg_5121" n="sc" s="T641">
               <ts e="T642" id="Seg_5123" n="e" s="T641">(Это </ts>
               <ts e="T644" id="Seg_5125" n="e" s="T642">я=) </ts>
               <ts e="T645" id="Seg_5127" n="e" s="T644">Это </ts>
               <ts e="T646" id="Seg_5129" n="e" s="T645">я </ts>
               <ts e="T647" id="Seg_5131" n="e" s="T646">вот, </ts>
               <ts e="T648" id="Seg_5133" n="e" s="T647">всё </ts>
               <ts e="T649" id="Seg_5135" n="e" s="T648">(как=) </ts>
               <ts e="T650" id="Seg_5137" n="e" s="T649">сколь </ts>
               <ts e="T651" id="Seg_5139" n="e" s="T650">он </ts>
               <ts e="T652" id="Seg_5141" n="e" s="T651">мне </ts>
               <ts e="T654" id="Seg_5143" n="e" s="T652">годов </ts>
               <ts e="T656" id="Seg_5145" n="e" s="T654">писал. </ts>
            </ts>
            <ts e="T674" id="Seg_5146" n="sc" s="T670">
               <ts e="T672" id="Seg_5148" n="e" s="T670">Ага, </ts>
               <ts e="T674" id="Seg_5150" n="e" s="T672">ну-ну. </ts>
            </ts>
            <ts e="T677" id="Seg_5151" n="sc" s="T675">
               <ts e="T677" id="Seg_5153" n="e" s="T675">Можно. </ts>
            </ts>
            <ts e="T698" id="Seg_5154" n="sc" s="T680">
               <ts e="T681" id="Seg_5156" n="e" s="T680">Когда </ts>
               <ts e="T682" id="Seg_5158" n="e" s="T681">он </ts>
               <ts e="T683" id="Seg_5160" n="e" s="T682">всё </ts>
               <ts e="T684" id="Seg_5162" n="e" s="T683">писал </ts>
               <ts e="T685" id="Seg_5164" n="e" s="T684">ко </ts>
               <ts e="T686" id="Seg_5166" n="e" s="T685">мне </ts>
               <ts e="T687" id="Seg_5168" n="e" s="T686">четыре </ts>
               <ts e="T688" id="Seg_5170" n="e" s="T687">года, </ts>
               <ts e="T689" id="Seg_5172" n="e" s="T688">а </ts>
               <ts e="T690" id="Seg_5174" n="e" s="T689">я </ts>
               <ts e="T691" id="Seg_5176" n="e" s="T690">отписывалася: </ts>
               <ts e="T692" id="Seg_5178" n="e" s="T691">"Приеду, </ts>
               <ts e="T693" id="Seg_5180" n="e" s="T692">приеду", </ts>
               <ts e="T694" id="Seg_5182" n="e" s="T693">но </ts>
               <ts e="T695" id="Seg_5184" n="e" s="T694">не </ts>
               <ts e="T696" id="Seg_5186" n="e" s="T695">с </ts>
               <ts e="T697" id="Seg_5188" n="e" s="T696">кем </ts>
               <ts e="T698" id="Seg_5190" n="e" s="T697">было. </ts>
            </ts>
            <ts e="T712" id="Seg_5191" n="sc" s="T699">
               <ts e="T700" id="Seg_5193" n="e" s="T699">Хотела </ts>
               <ts e="T701" id="Seg_5195" n="e" s="T700">с </ts>
               <ts e="T702" id="Seg_5197" n="e" s="T701">одним </ts>
               <ts e="T703" id="Seg_5199" n="e" s="T702">человеком </ts>
               <ts e="T704" id="Seg_5201" n="e" s="T703">приехать, </ts>
               <ts e="T705" id="Seg_5203" n="e" s="T704">он </ts>
               <ts e="T706" id="Seg_5205" n="e" s="T705">шибко </ts>
               <ts e="T707" id="Seg_5207" n="e" s="T706">больной </ts>
               <ts e="T708" id="Seg_5209" n="e" s="T707">был, </ts>
               <ts e="T709" id="Seg_5211" n="e" s="T708">хворал, </ts>
               <ts e="T710" id="Seg_5213" n="e" s="T709">а </ts>
               <ts e="T711" id="Seg_5215" n="e" s="T710">потом </ts>
               <ts e="T712" id="Seg_5217" n="e" s="T711">помер. </ts>
            </ts>
            <ts e="T723" id="Seg_5218" n="sc" s="T713">
               <ts e="T714" id="Seg_5220" n="e" s="T713">(Тогда </ts>
               <ts e="T715" id="Seg_5222" n="e" s="T714">я=) </ts>
               <ts e="T716" id="Seg_5224" n="e" s="T715">Тогда </ts>
               <ts e="T717" id="Seg_5226" n="e" s="T716">он </ts>
               <ts e="T718" id="Seg_5228" n="e" s="T717">пишет </ts>
               <ts e="T719" id="Seg_5230" n="e" s="T718">мне: </ts>
               <ts e="T720" id="Seg_5232" n="e" s="T719">"Я </ts>
               <ts e="T721" id="Seg_5234" n="e" s="T720">приеду </ts>
               <ts e="T722" id="Seg_5236" n="e" s="T721">за </ts>
               <ts e="T723" id="Seg_5238" n="e" s="T722">тобой". </ts>
            </ts>
            <ts e="T728" id="Seg_5239" n="sc" s="T724">
               <ts e="T725" id="Seg_5241" n="e" s="T724">(И </ts>
               <ts e="T726" id="Seg_5243" n="e" s="T725">п=) </ts>
               <ts e="T727" id="Seg_5245" n="e" s="T726">И </ts>
               <ts e="T728" id="Seg_5247" n="e" s="T727">приехал. </ts>
            </ts>
            <ts e="T737" id="Seg_5248" n="sc" s="T729">
               <ts e="T730" id="Seg_5250" n="e" s="T729">Четырнадцатого </ts>
               <ts e="T731" id="Seg_5252" n="e" s="T730">числа. </ts>
               <ts e="T732" id="Seg_5254" n="e" s="T731">Я </ts>
               <ts e="T733" id="Seg_5256" n="e" s="T732">ждала, </ts>
               <ts e="T734" id="Seg_5258" n="e" s="T733">а </ts>
               <ts e="T735" id="Seg_5260" n="e" s="T734">он </ts>
               <ts e="T736" id="Seg_5262" n="e" s="T735">вперед </ts>
               <ts e="T737" id="Seg_5264" n="e" s="T736">приехал. </ts>
            </ts>
            <ts e="T750" id="Seg_5265" n="sc" s="T738">
               <ts e="T739" id="Seg_5267" n="e" s="T738">Тагды </ts>
               <ts e="T740" id="Seg_5269" n="e" s="T739">пошел </ts>
               <ts e="T741" id="Seg_5271" n="e" s="T740">на </ts>
               <ts e="T742" id="Seg_5273" n="e" s="T741">мельницу, </ts>
               <ts e="T743" id="Seg_5275" n="e" s="T742">там </ts>
               <ts e="T744" id="Seg_5277" n="e" s="T743">нашел </ts>
               <ts e="T745" id="Seg_5279" n="e" s="T744">людей, </ts>
               <ts e="T746" id="Seg_5281" n="e" s="T745">на </ts>
               <ts e="T747" id="Seg_5283" n="e" s="T746">евонной </ts>
               <ts e="T748" id="Seg_5285" n="e" s="T747">(язы-) </ts>
               <ts e="T749" id="Seg_5287" n="e" s="T748">(наречиях) </ts>
               <ts e="T750" id="Seg_5289" n="e" s="T749">говорят. </ts>
            </ts>
            <ts e="T754" id="Seg_5290" n="sc" s="T751">
               <ts e="T752" id="Seg_5292" n="e" s="T751">И </ts>
               <ts e="T753" id="Seg_5294" n="e" s="T752">пришел </ts>
               <ts e="T754" id="Seg_5296" n="e" s="T753">назад. </ts>
            </ts>
            <ts e="T769" id="Seg_5297" n="sc" s="T755">
               <ts e="T756" id="Seg_5299" n="e" s="T755">Тады </ts>
               <ts e="T757" id="Seg_5301" n="e" s="T756">пошел </ts>
               <ts e="T758" id="Seg_5303" n="e" s="T757">в </ts>
               <ts e="T759" id="Seg_5305" n="e" s="T758">баню, </ts>
               <ts e="T760" id="Seg_5307" n="e" s="T759">я </ts>
               <ts e="T761" id="Seg_5309" n="e" s="T760">пошла </ts>
               <ts e="T762" id="Seg_5311" n="e" s="T761">ему </ts>
               <ts e="T763" id="Seg_5313" n="e" s="T762">показала </ts>
               <ts e="T764" id="Seg_5315" n="e" s="T763">горячу </ts>
               <ts e="T765" id="Seg_5317" n="e" s="T764">воду, </ts>
               <ts e="T766" id="Seg_5319" n="e" s="T765">холодну, </ts>
               <ts e="T767" id="Seg_5321" n="e" s="T766">и </ts>
               <ts e="T768" id="Seg_5323" n="e" s="T767">мыло </ts>
               <ts e="T769" id="Seg_5325" n="e" s="T768">внесла. </ts>
            </ts>
            <ts e="T773" id="Seg_5326" n="sc" s="T770">
               <ts e="T771" id="Seg_5328" n="e" s="T770">Он </ts>
               <ts e="T772" id="Seg_5330" n="e" s="T771">вымылся, </ts>
               <ts e="T773" id="Seg_5332" n="e" s="T772">пришел. </ts>
            </ts>
            <ts e="T785" id="Seg_5333" n="sc" s="T774">
               <ts e="T775" id="Seg_5335" n="e" s="T774">Я </ts>
               <ts e="T776" id="Seg_5337" n="e" s="T775">говорю: </ts>
               <ts e="T777" id="Seg_5339" n="e" s="T776">("Ешь", </ts>
               <ts e="T778" id="Seg_5341" n="e" s="T777">ему </ts>
               <ts e="T779" id="Seg_5343" n="e" s="T778">ужин, </ts>
               <ts e="T780" id="Seg_5345" n="e" s="T779">а) </ts>
               <ts e="T781" id="Seg_5347" n="e" s="T780">он </ts>
               <ts e="T782" id="Seg_5349" n="e" s="T781">говорит: </ts>
               <ts e="T783" id="Seg_5351" n="e" s="T782">"Я </ts>
               <ts e="T784" id="Seg_5353" n="e" s="T783">там </ts>
               <ts e="T785" id="Seg_5355" n="e" s="T784">поужинал". </ts>
            </ts>
            <ts e="T789" id="Seg_5356" n="sc" s="T786">
               <ts e="T787" id="Seg_5358" n="e" s="T786">И </ts>
               <ts e="T788" id="Seg_5360" n="e" s="T787">легли </ts>
               <ts e="T789" id="Seg_5362" n="e" s="T788">спать. </ts>
            </ts>
            <ts e="T801" id="Seg_5363" n="sc" s="T790">
               <ts e="T791" id="Seg_5365" n="e" s="T790">Утром </ts>
               <ts e="T792" id="Seg_5367" n="e" s="T791">встали, </ts>
               <ts e="T793" id="Seg_5369" n="e" s="T792">я </ts>
               <ts e="T794" id="Seg_5371" n="e" s="T793">наварила </ts>
               <ts e="T795" id="Seg_5373" n="e" s="T794">яиц, </ts>
               <ts e="T796" id="Seg_5375" n="e" s="T795">приготовила, </ts>
               <ts e="T797" id="Seg_5377" n="e" s="T796">поели </ts>
               <ts e="T798" id="Seg_5379" n="e" s="T797">и </ts>
               <ts e="T799" id="Seg_5381" n="e" s="T798">пошли, </ts>
               <ts e="T800" id="Seg_5383" n="e" s="T799">на </ts>
               <ts e="T801" id="Seg_5385" n="e" s="T800">автобус. </ts>
            </ts>
            <ts e="T808" id="Seg_5386" n="sc" s="T802">
               <ts e="T803" id="Seg_5388" n="e" s="T802">Тагды </ts>
               <ts e="T804" id="Seg_5390" n="e" s="T803">сяли </ts>
               <ts e="T805" id="Seg_5392" n="e" s="T804">и </ts>
               <ts e="T806" id="Seg_5394" n="e" s="T805">в </ts>
               <ts e="T807" id="Seg_5396" n="e" s="T806">Агинско </ts>
               <ts e="T808" id="Seg_5398" n="e" s="T807">приехали. </ts>
            </ts>
            <ts e="T822" id="Seg_5399" n="sc" s="T809">
               <ts e="T810" id="Seg_5401" n="e" s="T809">Там </ts>
               <ts e="T811" id="Seg_5403" n="e" s="T810">тоже </ts>
               <ts e="T812" id="Seg_5405" n="e" s="T811">(н-) </ts>
               <ts e="T813" id="Seg_5407" n="e" s="T812">пошли </ts>
               <ts e="T814" id="Seg_5409" n="e" s="T813">к </ts>
               <ts e="T815" id="Seg_5411" n="e" s="T814">сестрам, </ts>
               <ts e="T816" id="Seg_5413" n="e" s="T815">тамо-ка </ts>
               <ts e="T817" id="Seg_5415" n="e" s="T816">были </ts>
               <ts e="T818" id="Seg_5417" n="e" s="T817">до </ts>
               <ts e="T819" id="Seg_5419" n="e" s="T818">вечера, </ts>
               <ts e="T820" id="Seg_5421" n="e" s="T819">до </ts>
               <ts e="T821" id="Seg_5423" n="e" s="T820">пяти </ts>
               <ts e="T822" id="Seg_5425" n="e" s="T821">часов. </ts>
            </ts>
            <ts e="T830" id="Seg_5426" n="sc" s="T823">
               <ts e="T824" id="Seg_5428" n="e" s="T823">А </ts>
               <ts e="T825" id="Seg_5430" n="e" s="T824">в </ts>
               <ts e="T826" id="Seg_5432" n="e" s="T825">пять </ts>
               <ts e="T827" id="Seg_5434" n="e" s="T826">часов </ts>
               <ts e="T828" id="Seg_5436" n="e" s="T827">поехали </ts>
               <ts e="T829" id="Seg_5438" n="e" s="T828">в </ts>
               <ts e="T830" id="Seg_5440" n="e" s="T829">Уяр. </ts>
            </ts>
            <ts e="T838" id="Seg_5441" n="sc" s="T831">
               <ts e="T832" id="Seg_5443" n="e" s="T831">Там </ts>
               <ts e="T833" id="Seg_5445" n="e" s="T832">ночевали </ts>
               <ts e="T834" id="Seg_5447" n="e" s="T833">тоже </ts>
               <ts e="T835" id="Seg_5449" n="e" s="T834">у </ts>
               <ts e="T836" id="Seg_5451" n="e" s="T835">верующих </ts>
               <ts e="T837" id="Seg_5453" n="e" s="T836">у </ts>
               <ts e="T838" id="Seg_5455" n="e" s="T837">братьев. </ts>
            </ts>
            <ts e="T848" id="Seg_5456" n="sc" s="T839">
               <ts e="T840" id="Seg_5458" n="e" s="T839">На </ts>
               <ts e="T841" id="Seg_5460" n="e" s="T840">другой </ts>
               <ts e="T842" id="Seg_5462" n="e" s="T841">день </ts>
               <ts e="T843" id="Seg_5464" n="e" s="T842">сели </ts>
               <ts e="T844" id="Seg_5466" n="e" s="T843">в </ts>
               <ts e="T845" id="Seg_5468" n="e" s="T844">Красноярска, </ts>
               <ts e="T846" id="Seg_5470" n="e" s="T845">приехал </ts>
               <ts e="T847" id="Seg_5472" n="e" s="T846">из </ts>
               <ts e="T848" id="Seg_5474" n="e" s="T847">Красноярского. </ts>
            </ts>
            <ts e="T862" id="Seg_5475" n="sc" s="T849">
               <ts e="T850" id="Seg_5477" n="e" s="T849">На </ts>
               <ts e="T851" id="Seg_5479" n="e" s="T850">самолет </ts>
               <ts e="T852" id="Seg_5481" n="e" s="T851">сяли </ts>
               <ts e="T853" id="Seg_5483" n="e" s="T852">и </ts>
               <ts e="T854" id="Seg_5485" n="e" s="T853">приехали </ts>
               <ts e="T855" id="Seg_5487" n="e" s="T854">в </ts>
               <ts e="T856" id="Seg_5489" n="e" s="T855">Новосибирска. </ts>
               <ts e="T857" id="Seg_5491" n="e" s="T856">С </ts>
               <ts e="T858" id="Seg_5493" n="e" s="T857">Новосибирского </ts>
               <ts e="T859" id="Seg_5495" n="e" s="T858">коло </ts>
               <ts e="T860" id="Seg_5497" n="e" s="T859">Москвы </ts>
               <ts e="T861" id="Seg_5499" n="e" s="T860">есть </ts>
               <ts e="T862" id="Seg_5501" n="e" s="T861">посадка. </ts>
            </ts>
            <ts e="T877" id="Seg_5502" n="sc" s="T863">
               <ts e="T864" id="Seg_5504" n="e" s="T863">А </ts>
               <ts e="T865" id="Seg_5506" n="e" s="T864">потом </ts>
               <ts e="T866" id="Seg_5508" n="e" s="T865">с </ts>
               <ts e="T867" id="Seg_5510" n="e" s="T866">этой </ts>
               <ts e="T868" id="Seg_5512" n="e" s="T867">посадки </ts>
               <ts e="T869" id="Seg_5514" n="e" s="T868">мы </ts>
               <ts e="T870" id="Seg_5516" n="e" s="T869">(поехали </ts>
               <ts e="T871" id="Seg_5518" n="e" s="T870">в=) </ts>
               <ts e="T872" id="Seg_5520" n="e" s="T871">съездили </ts>
               <ts e="T873" id="Seg_5522" n="e" s="T872">на </ts>
               <ts e="T874" id="Seg_5524" n="e" s="T873">легковой </ts>
               <ts e="T875" id="Seg_5526" n="e" s="T874">машине </ts>
               <ts e="T876" id="Seg_5528" n="e" s="T875">в </ts>
               <ts e="T877" id="Seg_5530" n="e" s="T876">Москву. </ts>
            </ts>
            <ts e="T886" id="Seg_5531" n="sc" s="T878">
               <ts e="T879" id="Seg_5533" n="e" s="T878">Он </ts>
               <ts e="T880" id="Seg_5535" n="e" s="T879">там </ts>
               <ts e="T881" id="Seg_5537" n="e" s="T880">снял </ts>
               <ts e="T882" id="Seg_5539" n="e" s="T881">меня </ts>
               <ts e="T883" id="Seg_5541" n="e" s="T882">(с </ts>
               <ts e="T884" id="Seg_5543" n="e" s="T883">этим=) </ts>
               <ts e="T885" id="Seg_5545" n="e" s="T884">с </ts>
               <ts e="T886" id="Seg_5547" n="e" s="T885">людям. </ts>
            </ts>
            <ts e="T902" id="Seg_5548" n="sc" s="T887">
               <ts e="T888" id="Seg_5550" n="e" s="T887">А </ts>
               <ts e="T889" id="Seg_5552" n="e" s="T888">потом </ts>
               <ts e="T890" id="Seg_5554" n="e" s="T889">воротились </ts>
               <ts e="T891" id="Seg_5556" n="e" s="T890">назад, </ts>
               <ts e="T892" id="Seg_5558" n="e" s="T891">тады </ts>
               <ts e="T893" id="Seg_5560" n="e" s="T892">сяли </ts>
               <ts e="T894" id="Seg_5562" n="e" s="T893">опять </ts>
               <ts e="T895" id="Seg_5564" n="e" s="T894">на </ts>
               <ts e="T896" id="Seg_5566" n="e" s="T895">самолет </ts>
               <ts e="T897" id="Seg_5568" n="e" s="T896">и </ts>
               <ts e="T898" id="Seg_5570" n="e" s="T897">(при-) </ts>
               <ts e="T899" id="Seg_5572" n="e" s="T898">приехали </ts>
               <ts e="T900" id="Seg_5574" n="e" s="T899">сюда, </ts>
               <ts e="T901" id="Seg_5576" n="e" s="T900">в </ts>
               <ts e="T902" id="Seg_5578" n="e" s="T901">эту… </ts>
            </ts>
            <ts e="T915" id="Seg_5579" n="sc" s="T903">
               <ts e="T904" id="Seg_5581" n="e" s="T903">В </ts>
               <ts e="T905" id="Seg_5583" n="e" s="T904">этот </ts>
               <ts e="T906" id="Seg_5585" n="e" s="T905">город </ts>
               <ts e="T907" id="Seg_5587" n="e" s="T906">или </ts>
               <ts e="T908" id="Seg_5589" n="e" s="T907">в </ts>
               <ts e="T909" id="Seg_5591" n="e" s="T908">деревню </ts>
               <ts e="T910" id="Seg_5593" n="e" s="T909">или </ts>
               <ts e="T911" id="Seg_5595" n="e" s="T910">как </ts>
               <ts e="T912" id="Seg_5597" n="e" s="T911">ли </ts>
               <ts e="T913" id="Seg_5599" n="e" s="T912">я </ts>
               <ts e="T914" id="Seg_5601" n="e" s="T913">сказала </ts>
               <ts e="T915" id="Seg_5603" n="e" s="T914">уж? </ts>
            </ts>
            <ts e="T927" id="Seg_5604" n="sc" s="T916">
               <ts e="T917" id="Seg_5606" n="e" s="T916">Вот </ts>
               <ts e="T920" id="Seg_5608" n="e" s="T917">сюда, </ts>
               <ts e="T922" id="Seg_5610" n="e" s="T920">в </ts>
               <ts e="T925" id="Seg_5612" n="e" s="T922">Таллину, </ts>
               <ts e="T926" id="Seg_5614" n="e" s="T925">в </ts>
               <ts e="T927" id="Seg_5616" n="e" s="T926">Таллину. </ts>
            </ts>
            <ts e="T937" id="Seg_5617" n="sc" s="T933">
               <ts e="T934" id="Seg_5619" n="e" s="T933">А </ts>
               <ts e="T935" id="Seg_5621" n="e" s="T934">потом </ts>
               <ts e="T936" id="Seg_5623" n="e" s="T935">отседа </ts>
               <ts e="T937" id="Seg_5625" n="e" s="T936">(уех-)… </ts>
            </ts>
            <ts e="T963" id="Seg_5626" n="sc" s="T938">
               <ts e="T939" id="Seg_5628" n="e" s="T938">Вот </ts>
               <ts e="T940" id="Seg_5630" n="e" s="T939">только </ts>
               <ts e="T941" id="Seg_5632" n="e" s="T940">не </ts>
               <ts e="T942" id="Seg_5634" n="e" s="T941">сказала, </ts>
               <ts e="T943" id="Seg_5636" n="e" s="T942">мы </ts>
               <ts e="T944" id="Seg_5638" n="e" s="T943">уехали </ts>
               <ts e="T945" id="Seg_5640" n="e" s="T944">на </ts>
               <ts e="T946" id="Seg_5642" n="e" s="T945">машине, </ts>
               <ts e="T947" id="Seg_5644" n="e" s="T946">а </ts>
               <ts e="T948" id="Seg_5646" n="e" s="T947">я </ts>
               <ts e="T949" id="Seg_5648" n="e" s="T948">сказала </ts>
               <ts e="T950" id="Seg_5650" n="e" s="T949">kambibaʔ </ts>
               <ts e="T951" id="Seg_5652" n="e" s="T950">(уе-). </ts>
               <ts e="T952" id="Seg_5654" n="e" s="T951">Ну, </ts>
               <ts e="T954" id="Seg_5656" n="e" s="T952">все </ts>
               <ts e="T956" id="Seg_5658" n="e" s="T954">равно, </ts>
               <ts e="T958" id="Seg_5660" n="e" s="T956">уехали, </ts>
               <ts e="T961" id="Seg_5662" n="e" s="T958">уехали </ts>
               <ts e="T962" id="Seg_5664" n="e" s="T961">в </ts>
               <ts e="T963" id="Seg_5666" n="e" s="T962">Тарту. </ts>
            </ts>
            <ts e="T970" id="Seg_5667" n="sc" s="T964">
               <ts e="T965" id="Seg_5669" n="e" s="T964">И </ts>
               <ts e="T966" id="Seg_5671" n="e" s="T965">там </ts>
               <ts e="T967" id="Seg_5673" n="e" s="T966">я </ts>
               <ts e="T968" id="Seg_5675" n="e" s="T967">три </ts>
               <ts e="T969" id="Seg_5677" n="e" s="T968">недели </ts>
               <ts e="T970" id="Seg_5679" n="e" s="T969">жила. </ts>
            </ts>
            <ts e="T985" id="Seg_5680" n="sc" s="T971">
               <ts e="T972" id="Seg_5682" n="e" s="T971">Тады </ts>
               <ts e="T973" id="Seg_5684" n="e" s="T972">он </ts>
               <ts e="T974" id="Seg_5686" n="e" s="T973">меня </ts>
               <ts e="T975" id="Seg_5688" n="e" s="T974">увез </ts>
               <ts e="T976" id="Seg_5690" n="e" s="T975">туды, </ts>
               <ts e="T977" id="Seg_5692" n="e" s="T976">где </ts>
               <ts e="T978" id="Seg_5694" n="e" s="T977">цурияны, </ts>
               <ts e="T979" id="Seg_5696" n="e" s="T978">там </ts>
               <ts e="T980" id="Seg_5698" n="e" s="T979">две </ts>
               <ts e="T981" id="Seg_5700" n="e" s="T980">ночи </ts>
               <ts e="T982" id="Seg_5702" n="e" s="T981">ночевали, </ts>
               <ts e="T983" id="Seg_5704" n="e" s="T982">это </ts>
               <ts e="T984" id="Seg_5706" n="e" s="T983">я </ts>
               <ts e="T985" id="Seg_5708" n="e" s="T984">говорила. </ts>
            </ts>
            <ts e="T1000" id="Seg_5709" n="sc" s="T986">
               <ts e="T987" id="Seg_5711" n="e" s="T986">А </ts>
               <ts e="T988" id="Seg_5713" n="e" s="T987">потом </ts>
               <ts e="T989" id="Seg_5715" n="e" s="T988">оттель </ts>
               <ts e="T990" id="Seg_5717" n="e" s="T989">обратно </ts>
               <ts e="T991" id="Seg_5719" n="e" s="T990">сюды </ts>
               <ts e="T992" id="Seg_5721" n="e" s="T991">вот </ts>
               <ts e="T993" id="Seg_5723" n="e" s="T992">привез, </ts>
               <ts e="T994" id="Seg_5725" n="e" s="T993">и </ts>
               <ts e="T995" id="Seg_5727" n="e" s="T994">уже </ts>
               <ts e="T996" id="Seg_5729" n="e" s="T995">тут </ts>
               <ts e="T997" id="Seg_5731" n="e" s="T996">вот </ts>
               <ts e="T998" id="Seg_5733" n="e" s="T997">более </ts>
               <ts e="T999" id="Seg_5735" n="e" s="T998">недели </ts>
               <ts e="T1000" id="Seg_5737" n="e" s="T999">живу. </ts>
            </ts>
            <ts e="T1004" id="Seg_5738" n="sc" s="T1001">
               <ts e="T1002" id="Seg_5740" n="e" s="T1001">У </ts>
               <ts e="T1003" id="Seg_5742" n="e" s="T1002">этой </ts>
               <ts e="T1004" id="Seg_5744" n="e" s="T1003">женщины. </ts>
            </ts>
            <ts e="T1009" id="Seg_5745" n="sc" s="T1005">
               <ts e="T1006" id="Seg_5747" n="e" s="T1005">Это </ts>
               <ts e="T1007" id="Seg_5749" n="e" s="T1006">всё </ts>
               <ts e="T1008" id="Seg_5751" n="e" s="T1007">я </ts>
               <ts e="T1009" id="Seg_5753" n="e" s="T1008">рассказала. </ts>
            </ts>
            <ts e="T1033" id="Seg_5754" n="sc" s="T1026">
               <ts e="T1027" id="Seg_5756" n="e" s="T1026">Ну, </ts>
               <ts e="T1028" id="Seg_5758" n="e" s="T1027">не </ts>
               <ts e="T1031" id="Seg_5760" n="e" s="T1028">слыхать </ts>
               <ts e="T1033" id="Seg_5762" n="e" s="T1031">шума. </ts>
            </ts>
            <ts e="T1047" id="Seg_5763" n="sc" s="T1035">
               <ts e="T1038" id="Seg_5765" n="e" s="T1035">Ребятишки </ts>
               <ts e="T1041" id="Seg_5767" n="e" s="T1038">где-то </ts>
               <ts e="T1044" id="Seg_5769" n="e" s="T1041">притихли, </ts>
               <ts e="T1046" id="Seg_5771" n="e" s="T1044">они </ts>
               <ts e="T1047" id="Seg_5773" n="e" s="T1046">вон… </ts>
            </ts>
            <ts e="T1070" id="Seg_5774" n="sc" s="T1050">
               <ts e="T1052" id="Seg_5776" n="e" s="T1050">До </ts>
               <ts e="T1054" id="Seg_5778" n="e" s="T1052">этого </ts>
               <ts e="T1055" id="Seg_5780" n="e" s="T1054">было </ts>
               <ts e="T1056" id="Seg_5782" n="e" s="T1055">здорово </ts>
               <ts e="T1057" id="Seg_5784" n="e" s="T1056">кричали, </ts>
               <ts e="T1058" id="Seg_5786" n="e" s="T1057">может </ts>
               <ts e="T1059" id="Seg_5788" n="e" s="T1058">пообедали </ts>
               <ts e="T1060" id="Seg_5790" n="e" s="T1059">да </ts>
               <ts e="T1061" id="Seg_5792" n="e" s="T1060">спать </ts>
               <ts e="T1062" id="Seg_5794" n="e" s="T1061">легли, </ts>
               <ts e="T1063" id="Seg_5796" n="e" s="T1062">я </ts>
               <ts e="T1064" id="Seg_5798" n="e" s="T1063">и </ts>
               <ts e="T1065" id="Seg_5800" n="e" s="T1064">то </ts>
               <ts e="T1066" id="Seg_5802" n="e" s="T1065">маленько </ts>
               <ts e="T1067" id="Seg_5804" n="e" s="T1066">задремала. </ts>
               <ts e="T1068" id="Seg_5806" n="e" s="T1067">Ждали, </ts>
               <ts e="T1069" id="Seg_5808" n="e" s="T1068">ждали </ts>
               <ts e="T1070" id="Seg_5810" n="e" s="T1069">вас. </ts>
            </ts>
            <ts e="T1136" id="Seg_5811" n="sc" s="T1118">
               <ts e="T1119" id="Seg_5813" n="e" s="T1118">Не </ts>
               <ts e="T1121" id="Seg_5815" n="e" s="T1119">знаю, </ts>
               <ts e="T1124" id="Seg_5817" n="e" s="T1121">что </ts>
               <ts e="T1126" id="Seg_5819" n="e" s="T1124">сказать. </ts>
               <ts e="T1130" id="Seg_5821" n="e" s="T1126">Что </ts>
               <ts e="T1133" id="Seg_5823" n="e" s="T1130">еще </ts>
               <ts e="T1136" id="Seg_5825" n="e" s="T1133">сказать. </ts>
            </ts>
            <ts e="T1191" id="Seg_5826" n="sc" s="T1173">
               <ts e="T1174" id="Seg_5828" n="e" s="T1173">Ну, </ts>
               <ts e="T1175" id="Seg_5830" n="e" s="T1174">и </ts>
               <ts e="T1176" id="Seg_5832" n="e" s="T1175">я </ts>
               <ts e="T1177" id="Seg_5834" n="e" s="T1176">тоже </ts>
               <ts e="T1178" id="Seg_5836" n="e" s="T1177">и </ts>
               <ts e="T1179" id="Seg_5838" n="e" s="T1178">так </ts>
               <ts e="T1180" id="Seg_5840" n="e" s="T1179">как </ts>
               <ts e="T1181" id="Seg_5842" n="e" s="T1180">племянник </ts>
               <ts e="T1182" id="Seg_5844" n="e" s="T1181">меня </ts>
               <ts e="T1183" id="Seg_5846" n="e" s="T1182">не </ts>
               <ts e="T1184" id="Seg_5848" n="e" s="T1183">пускал, </ts>
               <ts e="T1185" id="Seg_5850" n="e" s="T1184">я </ts>
               <ts e="T1186" id="Seg_5852" n="e" s="T1185">говорила </ts>
               <ts e="T1187" id="Seg_5854" n="e" s="T1186">али </ts>
               <ts e="T1188" id="Seg_5856" n="e" s="T1187">нет, </ts>
               <ts e="T1189" id="Seg_5858" n="e" s="T1188">я </ts>
               <ts e="T1191" id="Seg_5860" n="e" s="T1189">забыла. </ts>
            </ts>
            <ts e="T1227" id="Seg_5861" n="sc" s="T1219">
               <ts e="T1220" id="Seg_5863" n="e" s="T1219">Dĭgəttə </ts>
               <ts e="T1221" id="Seg_5865" n="e" s="T1220">Arpit </ts>
               <ts e="T1222" id="Seg_5867" n="e" s="T1221">šobi, </ts>
               <ts e="T1223" id="Seg_5869" n="e" s="T1222">măn </ts>
               <ts e="T1224" id="Seg_5871" n="e" s="T1223">племянник </ts>
               <ts e="T1225" id="Seg_5873" n="e" s="T1224">ej </ts>
               <ts e="T1226" id="Seg_5875" n="e" s="T1225">öʔlubi </ts>
               <ts e="T1227" id="Seg_5877" n="e" s="T1226">меня. </ts>
            </ts>
            <ts e="T1233" id="Seg_5878" n="sc" s="T1228">
               <ts e="T1229" id="Seg_5880" n="e" s="T1228">A </ts>
               <ts e="T1230" id="Seg_5882" n="e" s="T1229">невестка </ts>
               <ts e="T1231" id="Seg_5884" n="e" s="T1230">măndə: </ts>
               <ts e="T1232" id="Seg_5886" n="e" s="T1231">"Kanaʔ, </ts>
               <ts e="T1233" id="Seg_5888" n="e" s="T1232">kanaʔ". </ts>
            </ts>
            <ts e="T1238" id="Seg_5889" n="sc" s="T1234">
               <ts e="T1235" id="Seg_5891" n="e" s="T1234">Dĭgəttə </ts>
               <ts e="T1236" id="Seg_5893" n="e" s="T1235">măn </ts>
               <ts e="T1237" id="Seg_5895" n="e" s="T1236">kambiam </ts>
               <ts e="T1238" id="Seg_5897" n="e" s="T1237">döbər. </ts>
            </ts>
            <ts e="T1249" id="Seg_5898" n="sc" s="T1239">
               <ts e="T1240" id="Seg_5900" n="e" s="T1239">А </ts>
               <ts e="T1241" id="Seg_5902" n="e" s="T1240">потом </ts>
               <ts e="T1242" id="Seg_5904" n="e" s="T1241">невестка </ts>
               <ts e="T1243" id="Seg_5906" n="e" s="T1242">говорит: </ts>
               <ts e="T1244" id="Seg_5908" n="e" s="T1243">поезжай, </ts>
               <ts e="T1245" id="Seg_5910" n="e" s="T1244">а </ts>
               <ts e="T1246" id="Seg_5912" n="e" s="T1245">племянник </ts>
               <ts e="T1247" id="Seg_5914" n="e" s="T1246">говорит: </ts>
               <ts e="T1248" id="Seg_5916" n="e" s="T1247">не </ts>
               <ts e="T1249" id="Seg_5918" n="e" s="T1248">езди. </ts>
            </ts>
            <ts e="T1251" id="Seg_5919" n="sc" s="T1250">
               <ts e="T1251" id="Seg_5921" n="e" s="T1250">Вот. </ts>
            </ts>
            <ts e="T1253" id="Seg_5922" n="sc" s="T1252">
               <ts e="T1253" id="Seg_5924" n="e" s="T1252">Так. </ts>
            </ts>
            <ts e="T1257" id="Seg_5925" n="sc" s="T1254">
               <ts e="T1256" id="Seg_5927" n="e" s="T1254">(Так </ts>
               <ts e="T1257" id="Seg_5929" n="e" s="T1256">вроде). </ts>
            </ts>
            <ts e="T1265" id="Seg_5930" n="sc" s="T1258">
               <ts e="T1259" id="Seg_5932" n="e" s="T1258">Потом </ts>
               <ts e="T1260" id="Seg_5934" n="e" s="T1259">чего </ts>
               <ts e="T1261" id="Seg_5936" n="e" s="T1260">еще. </ts>
               <ts e="T1262" id="Seg_5938" n="e" s="T1261">Потом </ts>
               <ts e="T1263" id="Seg_5940" n="e" s="T1262">чего </ts>
               <ts e="T1264" id="Seg_5942" n="e" s="T1263">еще </ts>
               <ts e="T1265" id="Seg_5944" n="e" s="T1264">рассказать. </ts>
            </ts>
            <ts e="T1274" id="Seg_5945" n="sc" s="T1267">
               <ts e="T1268" id="Seg_5947" n="e" s="T1267">Это </ts>
               <ts e="T1269" id="Seg_5949" n="e" s="T1268">уже </ts>
               <ts e="T1270" id="Seg_5951" n="e" s="T1269">я </ts>
               <ts e="T1271" id="Seg_5953" n="e" s="T1270">говорила, </ts>
               <ts e="T1272" id="Seg_5955" n="e" s="T1271">как </ts>
               <ts e="T1273" id="Seg_5957" n="e" s="T1272">финны </ts>
               <ts e="T1274" id="Seg_5959" n="e" s="T1273">были. </ts>
            </ts>
            <ts e="T1280" id="Seg_5960" n="sc" s="T1275">
               <ts e="T1276" id="Seg_5962" n="e" s="T1275">Там, </ts>
               <ts e="T1277" id="Seg_5964" n="e" s="T1276">у </ts>
               <ts e="T1278" id="Seg_5966" n="e" s="T1277">вас </ts>
               <ts e="T1279" id="Seg_5968" n="e" s="T1278">есть </ts>
               <ts e="T1280" id="Seg_5970" n="e" s="T1279">(наматано). </ts>
            </ts>
            <ts e="T1286" id="Seg_5971" n="sc" s="T1281">
               <ts e="T1282" id="Seg_5973" n="e" s="T1281">Чего </ts>
               <ts e="T1283" id="Seg_5975" n="e" s="T1282">еще </ts>
               <ts e="T1284" id="Seg_5977" n="e" s="T1283">сказать, </ts>
               <ts e="T1285" id="Seg_5979" n="e" s="T1284">не </ts>
               <ts e="T1286" id="Seg_5981" n="e" s="T1285">знаю. </ts>
            </ts>
            <ts e="T1304" id="Seg_5982" n="sc" s="T1295">
               <ts e="T1298" id="Seg_5984" n="e" s="T1295">Ну, </ts>
               <ts e="T1299" id="Seg_5986" n="e" s="T1298">там </ts>
               <ts e="T1302" id="Seg_5988" n="e" s="T1299">я </ts>
               <ts e="T1304" id="Seg_5990" n="e" s="T1302">(расска-). </ts>
            </ts>
            <ts e="T1324" id="Seg_5991" n="sc" s="T1311">
               <ts e="T1313" id="Seg_5993" n="e" s="T1311">Ну, </ts>
               <ts e="T1315" id="Seg_5995" n="e" s="T1313">чего </ts>
               <ts e="T1316" id="Seg_5997" n="e" s="T1315">(одно </ts>
               <ts e="T1318" id="Seg_5999" n="e" s="T1316">слово=) </ts>
               <ts e="T1319" id="Seg_6001" n="e" s="T1318">одно </ts>
               <ts e="T1320" id="Seg_6003" n="e" s="T1319">и </ts>
               <ts e="T1321" id="Seg_6005" n="e" s="T1320">то </ts>
               <ts e="T1322" id="Seg_6007" n="e" s="T1321">же </ts>
               <ts e="T1324" id="Seg_6009" n="e" s="T1322">говорить. </ts>
            </ts>
            <ts e="T1329" id="Seg_6010" n="sc" s="T1325">
               <ts e="T1326" id="Seg_6012" n="e" s="T1325">Погоди, </ts>
               <ts e="T1327" id="Seg_6014" n="e" s="T1326">друго </ts>
               <ts e="T1328" id="Seg_6016" n="e" s="T1327">надо </ts>
               <ts e="T1329" id="Seg_6018" n="e" s="T1328">((…)). </ts>
            </ts>
            <ts e="T1373" id="Seg_6019" n="sc" s="T1368">
               <ts e="T1369" id="Seg_6021" n="e" s="T1368">(Там=) </ts>
               <ts e="T1370" id="Seg_6023" n="e" s="T1369">Там </ts>
               <ts e="T1371" id="Seg_6025" n="e" s="T1370">три </ts>
               <ts e="T1372" id="Seg_6027" n="e" s="T1371">было </ts>
               <ts e="T1373" id="Seg_6029" n="e" s="T1372">их. </ts>
            </ts>
            <ts e="T1389" id="Seg_6030" n="sc" s="T1377">
               <ts e="T1378" id="Seg_6032" n="e" s="T1377">Три, </ts>
               <ts e="T1379" id="Seg_6034" n="e" s="T1378">(и </ts>
               <ts e="T1380" id="Seg_6036" n="e" s="T1379">все=) </ts>
               <ts e="T1381" id="Seg_6038" n="e" s="T1380">на </ts>
               <ts e="T1382" id="Seg_6040" n="e" s="T1381">всех </ts>
               <ts e="T1383" id="Seg_6042" n="e" s="T1382">говорила, </ts>
               <ts e="T1385" id="Seg_6044" n="e" s="T1383">они </ts>
               <ts e="T1386" id="Seg_6046" n="e" s="T1385">знают, </ts>
               <ts e="T1387" id="Seg_6048" n="e" s="T1386">где </ts>
               <ts e="T1388" id="Seg_6050" n="e" s="T1387">вышло </ts>
               <ts e="T1389" id="Seg_6052" n="e" s="T1388">хорошо. </ts>
            </ts>
            <ts e="T1449" id="Seg_6053" n="sc" s="T1444">
               <ts e="T1446" id="Seg_6055" n="e" s="T1444">У </ts>
               <ts e="T1447" id="Seg_6057" n="e" s="T1446">её </ts>
               <ts e="T1448" id="Seg_6059" n="e" s="T1447">нет </ts>
               <ts e="T1449" id="Seg_6061" n="e" s="T1448">радио. </ts>
            </ts>
            <ts e="T1460" id="Seg_6062" n="sc" s="T1453">
               <ts e="T1454" id="Seg_6064" n="e" s="T1453">А </ts>
               <ts e="T1455" id="Seg_6066" n="e" s="T1454">это </ts>
               <ts e="T1457" id="Seg_6068" n="e" s="T1455">не </ts>
               <ts e="T1458" id="Seg_6070" n="e" s="T1457">знаю </ts>
               <ts e="T1460" id="Seg_6072" n="e" s="T1458">чего. </ts>
            </ts>
            <ts e="T1485" id="Seg_6073" n="sc" s="T1463">
               <ts e="T1464" id="Seg_6075" n="e" s="T1463">Она </ts>
               <ts e="T1465" id="Seg_6077" n="e" s="T1464">вчера </ts>
               <ts e="T1467" id="Seg_6079" n="e" s="T1465">говорила: </ts>
               <ts e="T1468" id="Seg_6081" n="e" s="T1467">"Жалко </ts>
               <ts e="T1469" id="Seg_6083" n="e" s="T1468">нету </ts>
               <ts e="T1470" id="Seg_6085" n="e" s="T1469">радио, </ts>
               <ts e="T1471" id="Seg_6087" n="e" s="T1470">может </ts>
               <ts e="T1472" id="Seg_6089" n="e" s="T1471">быть </ts>
               <ts e="T1473" id="Seg_6091" n="e" s="T1472">бы </ts>
               <ts e="T1474" id="Seg_6093" n="e" s="T1473">слухали". </ts>
               <ts e="T1475" id="Seg_6095" n="e" s="T1474">А </ts>
               <ts e="T1477" id="Seg_6097" n="e" s="T1475">я </ts>
               <ts e="T1478" id="Seg_6099" n="e" s="T1477">говорю: </ts>
               <ts e="T1480" id="Seg_6101" n="e" s="T1478">"Может, </ts>
               <ts e="T1481" id="Seg_6103" n="e" s="T1480">еще </ts>
               <ts e="T1483" id="Seg_6105" n="e" s="T1481">не </ts>
               <ts e="T1484" id="Seg_6107" n="e" s="T1483">скоро </ts>
               <ts e="T1485" id="Seg_6109" n="e" s="T1484">справят". </ts>
            </ts>
            <ts e="T1532" id="Seg_6110" n="sc" s="T1527">
               <ts e="T1528" id="Seg_6112" n="e" s="T1527">Они </ts>
               <ts e="T1529" id="Seg_6114" n="e" s="T1528">же </ts>
               <ts e="T1530" id="Seg_6116" n="e" s="T1529">скоро </ts>
               <ts e="T1531" id="Seg_6118" n="e" s="T1530">пошли </ts>
               <ts e="T1532" id="Seg_6120" n="e" s="T1531">так. </ts>
            </ts>
            <ts e="T1543" id="Seg_6121" n="sc" s="T1533">
               <ts e="T1534" id="Seg_6123" n="e" s="T1533">И </ts>
               <ts e="T1535" id="Seg_6125" n="e" s="T1534">мы </ts>
               <ts e="T1536" id="Seg_6127" n="e" s="T1535">после </ts>
               <ts e="T1537" id="Seg_6129" n="e" s="T1536">вас </ts>
               <ts e="T1538" id="Seg_6131" n="e" s="T1537">тоже </ts>
               <ts e="T1539" id="Seg_6133" n="e" s="T1538">скоро </ts>
               <ts e="T1540" id="Seg_6135" n="e" s="T1539">пошли, </ts>
               <ts e="T1541" id="Seg_6137" n="e" s="T1540">немного </ts>
               <ts e="T1542" id="Seg_6139" n="e" s="T1541">были </ts>
               <ts e="T1543" id="Seg_6141" n="e" s="T1542">там. </ts>
            </ts>
            <ts e="T1607" id="Seg_6142" n="sc" s="T1604">
               <ts e="T1605" id="Seg_6144" n="e" s="T1604">(Погодить </ts>
               <ts e="T1606" id="Seg_6146" n="e" s="T1605">надо) </ts>
               <ts e="T1607" id="Seg_6148" n="e" s="T1606">надо. </ts>
            </ts>
            <ts e="T1627" id="Seg_6149" n="sc" s="T1612">
               <ts e="T1613" id="Seg_6151" n="e" s="T1612">Нас, </ts>
               <ts e="T1614" id="Seg_6153" n="e" s="T1613">Агафон </ts>
               <ts e="T1615" id="Seg_6155" n="e" s="T1614">Иванович, </ts>
               <ts e="T1616" id="Seg_6157" n="e" s="T1615">тоже </ts>
               <ts e="T1617" id="Seg_6159" n="e" s="T1616">всё </ts>
               <ts e="T1618" id="Seg_6161" n="e" s="T1617">забывается, </ts>
               <ts e="T1619" id="Seg_6163" n="e" s="T1618">уж </ts>
               <ts e="T1621" id="Seg_6165" n="e" s="T1619">много </ts>
               <ts e="T1623" id="Seg_6167" n="e" s="T1621">лет </ts>
               <ts e="T1625" id="Seg_6169" n="e" s="T1623">не </ts>
               <ts e="T1627" id="Seg_6171" n="e" s="T1625">говорила. </ts>
            </ts>
            <ts e="T1631" id="Seg_6172" n="sc" s="T1628">
               <ts e="T1629" id="Seg_6174" n="e" s="T1628">((…)) </ts>
               <ts e="T1630" id="Seg_6176" n="e" s="T1629">еще </ts>
               <ts e="T1631" id="Seg_6178" n="e" s="T1630">маленько. </ts>
            </ts>
            <ts e="T1641" id="Seg_6179" n="sc" s="T1634">
               <ts e="T1635" id="Seg_6181" n="e" s="T1634">Не </ts>
               <ts e="T1636" id="Seg_6183" n="e" s="T1635">с </ts>
               <ts e="T1638" id="Seg_6185" n="e" s="T1636">кем </ts>
               <ts e="T1641" id="Seg_6187" n="e" s="T1638">разговаривать. </ts>
            </ts>
            <ts e="T1652" id="Seg_6188" n="sc" s="T1643">
               <ts e="T1646" id="Seg_6190" n="e" s="T1643">Там, </ts>
               <ts e="T1647" id="Seg_6192" n="e" s="T1646">пойду </ts>
               <ts e="T1648" id="Seg_6194" n="e" s="T1647">к </ts>
               <ts e="T1649" id="Seg_6196" n="e" s="T1648">своей </ts>
               <ts e="T1650" id="Seg_6198" n="e" s="T1649">племяннице, </ts>
               <ts e="T1651" id="Seg_6200" n="e" s="T1650">толмачу, </ts>
               <ts e="T1652" id="Seg_6202" n="e" s="T1651">толмачу. </ts>
            </ts>
            <ts e="T1660" id="Seg_6203" n="sc" s="T1653">
               <ts e="T1654" id="Seg_6205" n="e" s="T1653">Тут </ts>
               <ts e="T1655" id="Seg_6207" n="e" s="T1654">зять </ts>
               <ts e="T1656" id="Seg_6209" n="e" s="T1655">прибежит: </ts>
               <ts e="T1657" id="Seg_6211" n="e" s="T1656">"Заболтали", — </ts>
               <ts e="T1658" id="Seg_6213" n="e" s="T1657">закричит </ts>
               <ts e="T1659" id="Seg_6215" n="e" s="T1658">на </ts>
               <ts e="T1660" id="Seg_6217" n="e" s="T1659">нас. </ts>
            </ts>
            <ts e="T1664" id="Seg_6218" n="sc" s="T1661">
               <ts e="T1662" id="Seg_6220" n="e" s="T1661">Внука </ts>
               <ts e="T1663" id="Seg_6222" n="e" s="T1662">своего </ts>
               <ts e="T1664" id="Seg_6224" n="e" s="T1663">заставляю. </ts>
            </ts>
            <ts e="T1673" id="Seg_6225" n="sc" s="T1665">
               <ts e="T1666" id="Seg_6227" n="e" s="T1665">Даже </ts>
               <ts e="T1667" id="Seg_6229" n="e" s="T1666">который </ts>
               <ts e="T1668" id="Seg_6231" n="e" s="T1667">настоящий, </ts>
               <ts e="T1669" id="Seg_6233" n="e" s="T1668">и </ts>
               <ts e="T1670" id="Seg_6235" n="e" s="T1669">вот </ts>
               <ts e="T1671" id="Seg_6237" n="e" s="T1670">там </ts>
               <ts e="T1672" id="Seg_6239" n="e" s="T1671">эта, </ts>
               <ts e="T1673" id="Seg_6241" n="e" s="T1672">приходила… </ts>
            </ts>
            <ts e="T1678" id="Seg_6242" n="sc" s="T1674">
               <ts e="T1675" id="Seg_6244" n="e" s="T1674">(Ага- </ts>
               <ts e="T1676" id="Seg_6246" n="e" s="T1675">Аган-) </ts>
               <ts e="T1677" id="Seg_6248" n="e" s="T1676">Агафьей </ts>
               <ts e="T1678" id="Seg_6250" n="e" s="T1677">звать. </ts>
            </ts>
            <ts e="T1684" id="Seg_6251" n="sc" s="T1679">
               <ts e="T1680" id="Seg_6253" n="e" s="T1679">И </ts>
               <ts e="T1681" id="Seg_6255" n="e" s="T1680">то </ts>
               <ts e="T1682" id="Seg_6257" n="e" s="T1681">она </ts>
               <ts e="T1683" id="Seg_6259" n="e" s="T1682">не </ts>
               <ts e="T1684" id="Seg_6261" n="e" s="T1683">знает. </ts>
            </ts>
            <ts e="T1698" id="Seg_6262" n="sc" s="T1685">
               <ts e="T1686" id="Seg_6264" n="e" s="T1685">Сказала </ts>
               <ts e="T1687" id="Seg_6266" n="e" s="T1686">"сахар", </ts>
               <ts e="T1688" id="Seg_6268" n="e" s="T1687">а </ts>
               <ts e="T1689" id="Seg_6270" n="e" s="T1688">она </ts>
               <ts e="T1690" id="Seg_6272" n="e" s="T1689">говорит: </ts>
               <ts e="T1691" id="Seg_6274" n="e" s="T1690">"Врет, </ts>
               <ts e="T1692" id="Seg_6276" n="e" s="T1691">сахар </ts>
               <ts e="T1693" id="Seg_6278" n="e" s="T1692">не </ts>
               <ts e="T1694" id="Seg_6280" n="e" s="T1693">так". </ts>
               <ts e="T1695" id="Seg_6282" n="e" s="T1694">"Ну, — </ts>
               <ts e="T1696" id="Seg_6284" n="e" s="T1695">говорит, — </ts>
               <ts e="T1697" id="Seg_6286" n="e" s="T1696">скажи, </ts>
               <ts e="T1698" id="Seg_6288" n="e" s="T1697">как". </ts>
            </ts>
            <ts e="T1705" id="Seg_6289" n="sc" s="T1699">
               <ts e="T1700" id="Seg_6291" n="e" s="T1699">И </ts>
               <ts e="T1701" id="Seg_6293" n="e" s="T1700">она </ts>
               <ts e="T1702" id="Seg_6295" n="e" s="T1701">сама </ts>
               <ts e="T1703" id="Seg_6297" n="e" s="T1702">не </ts>
               <ts e="T1705" id="Seg_6299" n="e" s="T1703">знает. </ts>
            </ts>
            <ts e="T1710" id="Seg_6300" n="sc" s="T1707">
               <ts e="T1710" id="Seg_6302" n="e" s="T1707">Ну. </ts>
            </ts>
            <ts e="T1715" id="Seg_6303" n="sc" s="T1713">
               <ts e="T1714" id="Seg_6305" n="e" s="T1713">Ну </ts>
               <ts e="T1715" id="Seg_6307" n="e" s="T1714">чего? </ts>
            </ts>
            <ts e="T1722" id="Seg_6308" n="sc" s="T1717">
               <ts e="T1718" id="Seg_6310" n="e" s="T1717">Ну, </ts>
               <ts e="T1719" id="Seg_6312" n="e" s="T1718">давай, </ts>
               <ts e="T1720" id="Seg_6314" n="e" s="T1719">открой, </ts>
               <ts e="T1721" id="Seg_6316" n="e" s="T1720">будем </ts>
               <ts e="T1722" id="Seg_6318" n="e" s="T1721">говорить. </ts>
            </ts>
            <ts e="T1729" id="Seg_6319" n="sc" s="T1723">
               <ts e="T1724" id="Seg_6321" n="e" s="T1723">Taldʼen </ts>
               <ts e="T1725" id="Seg_6323" n="e" s="T1724">miʔ </ts>
               <ts e="T1726" id="Seg_6325" n="e" s="T1725">jaʔtarla </ts>
               <ts e="T1727" id="Seg_6327" n="e" s="T1726">kambibaʔ </ts>
               <ts e="T1728" id="Seg_6329" n="e" s="T1727">onʼiʔ </ts>
               <ts e="T1729" id="Seg_6331" n="e" s="T1728">nükenə. </ts>
            </ts>
            <ts e="T1745" id="Seg_6332" n="sc" s="T1730">
               <ts e="T1731" id="Seg_6334" n="e" s="T1730">Amnobibaʔ </ts>
               <ts e="T1732" id="Seg_6336" n="e" s="T1731">aftobustə, </ts>
               <ts e="T1733" id="Seg_6338" n="e" s="T1732">dĭbər </ts>
               <ts e="T1734" id="Seg_6340" n="e" s="T1733">šobibaʔ. </ts>
               <ts e="T1735" id="Seg_6342" n="e" s="T1734">Dĭn </ts>
               <ts e="T1736" id="Seg_6344" n="e" s="T1735">šobiʔi </ts>
               <ts e="T1737" id="Seg_6346" n="e" s="T1736">il </ts>
               <ts e="T1738" id="Seg_6348" n="e" s="T1737">dʼăbaktərzittə </ts>
               <ts e="T1739" id="Seg_6350" n="e" s="T1738">măn </ts>
               <ts e="T1740" id="Seg_6352" n="e" s="T1739">šĭkenə. </ts>
               <ts e="T1741" id="Seg_6354" n="e" s="T1740">Măn </ts>
               <ts e="T1742" id="Seg_6356" n="e" s="T1741">dĭzeŋdə </ts>
               <ts e="T1743" id="Seg_6358" n="e" s="T1742">bar </ts>
               <ts e="T1744" id="Seg_6360" n="e" s="T1743">mămbiam, </ts>
               <ts e="T1745" id="Seg_6362" n="e" s="T1744">mămbiam. </ts>
            </ts>
            <ts e="T1747" id="Seg_6363" n="sc" s="T1746">
               <ts e="T1747" id="Seg_6365" n="e" s="T1746">Dĭgəttə… </ts>
            </ts>
            <ts e="T1761" id="Seg_6366" n="sc" s="T1748">
               <ts e="T1749" id="Seg_6368" n="e" s="T1748">Надо </ts>
               <ts e="T1750" id="Seg_6370" n="e" s="T1749">еще </ts>
               <ts e="T1751" id="Seg_6372" n="e" s="T1750">надумать </ts>
               <ts e="T1752" id="Seg_6374" n="e" s="T1751">чего. </ts>
               <ts e="T1754" id="Seg_6376" n="e" s="T1752">Это </ts>
               <ts e="T1756" id="Seg_6378" n="e" s="T1754">вчера </ts>
               <ts e="T1757" id="Seg_6380" n="e" s="T1756">что </ts>
               <ts e="T1759" id="Seg_6382" n="e" s="T1757">я </ts>
               <ts e="T1761" id="Seg_6384" n="e" s="T1759">ходила. </ts>
            </ts>
            <ts e="T1784" id="Seg_6385" n="sc" s="T1775">
               <ts e="T1776" id="Seg_6387" n="e" s="T1775">Мы </ts>
               <ts e="T1777" id="Seg_6389" n="e" s="T1776">вчера </ts>
               <ts e="T1778" id="Seg_6391" n="e" s="T1777">ходили </ts>
               <ts e="T1779" id="Seg_6393" n="e" s="T1778">в </ts>
               <ts e="T1780" id="Seg_6395" n="e" s="T1779">гости </ts>
               <ts e="T1781" id="Seg_6397" n="e" s="T1780">там, </ts>
               <ts e="T1782" id="Seg_6399" n="e" s="T1781">к </ts>
               <ts e="T1783" id="Seg_6401" n="e" s="T1782">одной </ts>
               <ts e="T1784" id="Seg_6403" n="e" s="T1783">старушке. </ts>
            </ts>
            <ts e="T1797" id="Seg_6404" n="sc" s="T1785">
               <ts e="T1786" id="Seg_6406" n="e" s="T1785">Там </ts>
               <ts e="T1787" id="Seg_6408" n="e" s="T1786">(приехали=) </ts>
               <ts e="T1788" id="Seg_6410" n="e" s="T1787">пришли </ts>
               <ts e="T1789" id="Seg_6412" n="e" s="T1788">люди, </ts>
               <ts e="T1790" id="Seg_6414" n="e" s="T1789">и </ts>
               <ts e="T1791" id="Seg_6416" n="e" s="T1790">мы </ts>
               <ts e="T1792" id="Seg_6418" n="e" s="T1791">разговаривали </ts>
               <ts e="T1793" id="Seg_6420" n="e" s="T1792">(мо-) </ts>
               <ts e="T1794" id="Seg_6422" n="e" s="T1793">на </ts>
               <ts e="T1795" id="Seg_6424" n="e" s="T1794">мое </ts>
               <ts e="T1796" id="Seg_6426" n="e" s="T1795">(ра-) </ts>
               <ts e="T1797" id="Seg_6428" n="e" s="T1796">наречие. </ts>
            </ts>
            <ts e="T1799" id="Seg_6429" n="sc" s="T1798">
               <ts e="T1799" id="Seg_6431" n="e" s="T1798">((BRK)). </ts>
            </ts>
            <ts e="T1801" id="Seg_6432" n="sc" s="T1800">
               <ts e="T1801" id="Seg_6434" n="e" s="T1800">Ладно. </ts>
            </ts>
            <ts e="T1806" id="Seg_6435" n="sc" s="T1802">
               <ts e="T1803" id="Seg_6437" n="e" s="T1802">Ну, </ts>
               <ts e="T1804" id="Seg_6439" n="e" s="T1803">чего </ts>
               <ts e="T1805" id="Seg_6441" n="e" s="T1804">еще </ts>
               <ts e="T1806" id="Seg_6443" n="e" s="T1805">рассказать. </ts>
            </ts>
            <ts e="T1853" id="Seg_6444" n="sc" s="T1844">
               <ts e="T1845" id="Seg_6446" n="e" s="T1844">Думаешь </ts>
               <ts e="T1847" id="Seg_6448" n="e" s="T1845">много </ts>
               <ts e="T1848" id="Seg_6450" n="e" s="T1847">рассказать, </ts>
               <ts e="T1849" id="Seg_6452" n="e" s="T1848">а </ts>
               <ts e="T1850" id="Seg_6454" n="e" s="T1849">потом </ts>
               <ts e="T1851" id="Seg_6456" n="e" s="T1850">опять </ts>
               <ts e="T1853" id="Seg_6458" n="e" s="T1851">уходит. </ts>
            </ts>
            <ts e="T1864" id="Seg_6459" n="sc" s="T1862">
               <ts e="T1863" id="Seg_6461" n="e" s="T1862">Ну, </ts>
               <ts e="T1864" id="Seg_6463" n="e" s="T1863">открой. </ts>
            </ts>
            <ts e="T1874" id="Seg_6464" n="sc" s="T1865">
               <ts e="T1866" id="Seg_6466" n="e" s="T1865">Măn </ts>
               <ts e="T1867" id="Seg_6468" n="e" s="T1866">oʔbdəbiam </ts>
               <ts e="T1868" id="Seg_6470" n="e" s="T1867">(šo-) </ts>
               <ts e="T1869" id="Seg_6472" n="e" s="T1868">šonəsʼtə </ts>
               <ts e="T1870" id="Seg_6474" n="e" s="T1869">döbər, </ts>
               <ts e="T1871" id="Seg_6476" n="e" s="T1870">il </ts>
               <ts e="T1872" id="Seg_6478" n="e" s="T1871">măndəʔi: </ts>
               <ts e="T1873" id="Seg_6480" n="e" s="T1872">"Iʔ </ts>
               <ts e="T1874" id="Seg_6482" n="e" s="T1873">kanaʔ. </ts>
            </ts>
            <ts e="T1877" id="Seg_6483" n="sc" s="T1875">
               <ts e="T1876" id="Seg_6485" n="e" s="T1875">Külalləl </ts>
               <ts e="T1877" id="Seg_6487" n="e" s="T1876">dĭn. </ts>
            </ts>
            <ts e="T1882" id="Seg_6488" n="sc" s="T1878">
               <ts e="T1879" id="Seg_6490" n="e" s="T1878">Măn </ts>
               <ts e="T1880" id="Seg_6492" n="e" s="T1879">mămbiam:" </ts>
               <ts e="T1881" id="Seg_6494" n="e" s="T1880">"Ej </ts>
               <ts e="T1882" id="Seg_6496" n="e" s="T1881">pĭmniem. </ts>
            </ts>
            <ts e="T1890" id="Seg_6497" n="sc" s="T1883">
               <ts e="T1884" id="Seg_6499" n="e" s="T1883">(Dĭn </ts>
               <ts e="T1885" id="Seg_6501" n="e" s="T1884">m-) </ts>
               <ts e="T1886" id="Seg_6503" n="e" s="T1885">Măna </ts>
               <ts e="T1887" id="Seg_6505" n="e" s="T1886">dĭn </ts>
               <ts e="T1888" id="Seg_6507" n="e" s="T1887">tože </ts>
               <ts e="T1889" id="Seg_6509" n="e" s="T1888">dʼünə </ts>
               <ts e="T1890" id="Seg_6511" n="e" s="T1889">elləʔi. </ts>
            </ts>
            <ts e="T1892" id="Seg_6512" n="sc" s="T1891">
               <ts e="T1892" id="Seg_6514" n="e" s="T1891">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T10" id="Seg_6515" s="T1">PKZ_19700819_09342-2a.PKZ.001 (001)</ta>
            <ta e="T47" id="Seg_6516" s="T43">PKZ_19700819_09342-2a.PKZ.002 (009)</ta>
            <ta e="T97" id="Seg_6517" s="T92">PKZ_19700819_09342-2a.PKZ.003 (017)</ta>
            <ta e="T124" id="Seg_6518" s="T102">PKZ_19700819_09342-2a.PKZ.004 (020)</ta>
            <ta e="T136" id="Seg_6519" s="T130">PKZ_19700819_09342-2a.PKZ.005 (022)</ta>
            <ta e="T146" id="Seg_6520" s="T137">PKZ_19700819_09342-2a.PKZ.006 (023)</ta>
            <ta e="T156" id="Seg_6521" s="T147">PKZ_19700819_09342-2a.PKZ.007 (024)</ta>
            <ta e="T166" id="Seg_6522" s="T157">PKZ_19700819_09342-2a.PKZ.008 (025)</ta>
            <ta e="T177" id="Seg_6523" s="T167">PKZ_19700819_09342-2a.PKZ.009 (027)</ta>
            <ta e="T199" id="Seg_6524" s="T181">PKZ_19700819_09342-2a.PKZ.010 (029)</ta>
            <ta e="T209" id="Seg_6525" s="T200">PKZ_19700819_09342-2a.PKZ.011 (030)</ta>
            <ta e="T225" id="Seg_6526" s="T214">PKZ_19700819_09342-2a.PKZ.012 (032)</ta>
            <ta e="T232" id="Seg_6527" s="T226">PKZ_19700819_09342-2a.PKZ.013 (035)</ta>
            <ta e="T238" id="Seg_6528" s="T234">PKZ_19700819_09342-2a.PKZ.014 (037)</ta>
            <ta e="T244" id="Seg_6529" s="T242">PKZ_19700819_09342-2a.PKZ.015 (038)</ta>
            <ta e="T248" id="Seg_6530" s="T244">PKZ_19700819_09342-2a.PKZ.016 (039)</ta>
            <ta e="T250" id="Seg_6531" s="T249">PKZ_19700819_09342-2a.PKZ.017 (040)</ta>
            <ta e="T256" id="Seg_6532" s="T251">PKZ_19700819_09342-2a.PKZ.018 (041)</ta>
            <ta e="T267" id="Seg_6533" s="T257">PKZ_19700819_09342-2a.PKZ.019 (042)</ta>
            <ta e="T275" id="Seg_6534" s="T268">PKZ_19700819_09342-2a.PKZ.020 (043)</ta>
            <ta e="T300" id="Seg_6535" s="T277">PKZ_19700819_09342-2a.PKZ.021 (045)</ta>
            <ta e="T319" id="Seg_6536" s="T315">PKZ_19700819_09342-2a.PKZ.022 (049)</ta>
            <ta e="T324" id="Seg_6537" s="T320">PKZ_19700819_09342-2a.PKZ.023 (050)</ta>
            <ta e="T327" id="Seg_6538" s="T325">PKZ_19700819_09342-2a.PKZ.024 (051)</ta>
            <ta e="T332" id="Seg_6539" s="T328">PKZ_19700819_09342-2a.PKZ.025 (052)</ta>
            <ta e="T335" id="Seg_6540" s="T332">PKZ_19700819_09342-2a.PKZ.026 (053)</ta>
            <ta e="T339" id="Seg_6541" s="T336">PKZ_19700819_09342-2a.PKZ.027 (054)</ta>
            <ta e="T344" id="Seg_6542" s="T340">PKZ_19700819_09342-2a.PKZ.028 (055)</ta>
            <ta e="T353" id="Seg_6543" s="T345">PKZ_19700819_09342-2a.PKZ.029 (056)</ta>
            <ta e="T361" id="Seg_6544" s="T354">PKZ_19700819_09342-2a.PKZ.030 (057)</ta>
            <ta e="T369" id="Seg_6545" s="T362">PKZ_19700819_09342-2a.PKZ.031 (058)</ta>
            <ta e="T375" id="Seg_6546" s="T370">PKZ_19700819_09342-2a.PKZ.032 (059)</ta>
            <ta e="T380" id="Seg_6547" s="T376">PKZ_19700819_09342-2a.PKZ.033 (060)</ta>
            <ta e="T385" id="Seg_6548" s="T381">PKZ_19700819_09342-2a.PKZ.034 (061)</ta>
            <ta e="T389" id="Seg_6549" s="T386">PKZ_19700819_09342-2a.PKZ.035 (062)</ta>
            <ta e="T395" id="Seg_6550" s="T389">PKZ_19700819_09342-2a.PKZ.036 (063)</ta>
            <ta e="T398" id="Seg_6551" s="T396">PKZ_19700819_09342-2a.PKZ.037 (064)</ta>
            <ta e="T404" id="Seg_6552" s="T399">PKZ_19700819_09342-2a.PKZ.038 (065)</ta>
            <ta e="T410" id="Seg_6553" s="T405">PKZ_19700819_09342-2a.PKZ.039 (066)</ta>
            <ta e="T413" id="Seg_6554" s="T411">PKZ_19700819_09342-2a.PKZ.040 (067)</ta>
            <ta e="T417" id="Seg_6555" s="T414">PKZ_19700819_09342-2a.PKZ.041 (068)</ta>
            <ta e="T421" id="Seg_6556" s="T418">PKZ_19700819_09342-2a.PKZ.042 (069)</ta>
            <ta e="T425" id="Seg_6557" s="T422">PKZ_19700819_09342-2a.PKZ.043 (070)</ta>
            <ta e="T435" id="Seg_6558" s="T426">PKZ_19700819_09342-2a.PKZ.044 (071)</ta>
            <ta e="T442" id="Seg_6559" s="T436">PKZ_19700819_09342-2a.PKZ.045 (072)</ta>
            <ta e="T445" id="Seg_6560" s="T443">PKZ_19700819_09342-2a.PKZ.046 (073)</ta>
            <ta e="T449" id="Seg_6561" s="T445">PKZ_19700819_09342-2a.PKZ.047 (074)</ta>
            <ta e="T452" id="Seg_6562" s="T450">PKZ_19700819_09342-2a.PKZ.048 (075)</ta>
            <ta e="T457" id="Seg_6563" s="T453">PKZ_19700819_09342-2a.PKZ.049 (076)</ta>
            <ta e="T464" id="Seg_6564" s="T458">PKZ_19700819_09342-2a.PKZ.050 (077)</ta>
            <ta e="T469" id="Seg_6565" s="T465">PKZ_19700819_09342-2a.PKZ.051 (078)</ta>
            <ta e="T474" id="Seg_6566" s="T470">PKZ_19700819_09342-2a.PKZ.052 (079)</ta>
            <ta e="T480" id="Seg_6567" s="T475">PKZ_19700819_09342-2a.PKZ.053 (080)</ta>
            <ta e="T487" id="Seg_6568" s="T481">PKZ_19700819_09342-2a.PKZ.054 (081)</ta>
            <ta e="T492" id="Seg_6569" s="T488">PKZ_19700819_09342-2a.PKZ.055 (082)</ta>
            <ta e="T499" id="Seg_6570" s="T493">PKZ_19700819_09342-2a.PKZ.056 (083)</ta>
            <ta e="T503" id="Seg_6571" s="T500">PKZ_19700819_09342-2a.PKZ.057 (084)</ta>
            <ta e="T510" id="Seg_6572" s="T504">PKZ_19700819_09342-2a.PKZ.058 (085)</ta>
            <ta e="T515" id="Seg_6573" s="T511">PKZ_19700819_09342-2a.PKZ.059 (086)</ta>
            <ta e="T522" id="Seg_6574" s="T516">PKZ_19700819_09342-2a.PKZ.060 (087)</ta>
            <ta e="T531" id="Seg_6575" s="T523">PKZ_19700819_09342-2a.PKZ.061 (088)</ta>
            <ta e="T536" id="Seg_6576" s="T532">PKZ_19700819_09342-2a.PKZ.062 (089)</ta>
            <ta e="T544" id="Seg_6577" s="T537">PKZ_19700819_09342-2a.PKZ.063 (090)</ta>
            <ta e="T555" id="Seg_6578" s="T545">PKZ_19700819_09342-2a.PKZ.064 (091)</ta>
            <ta e="T559" id="Seg_6579" s="T555">PKZ_19700819_09342-2a.PKZ.065 (092)</ta>
            <ta e="T564" id="Seg_6580" s="T560">PKZ_19700819_09342-2a.PKZ.066 (093)</ta>
            <ta e="T576" id="Seg_6581" s="T565">PKZ_19700819_09342-2a.PKZ.067 (094)</ta>
            <ta e="T582" id="Seg_6582" s="T577">PKZ_19700819_09342-2a.PKZ.068 (095)</ta>
            <ta e="T587" id="Seg_6583" s="T583">PKZ_19700819_09342-2a.PKZ.069 (096)</ta>
            <ta e="T596" id="Seg_6584" s="T588">PKZ_19700819_09342-2a.PKZ.070 (097)</ta>
            <ta e="T601" id="Seg_6585" s="T597">PKZ_19700819_09342-2a.PKZ.071 (098)</ta>
            <ta e="T607" id="Seg_6586" s="T602">PKZ_19700819_09342-2a.PKZ.072 (099)</ta>
            <ta e="T616" id="Seg_6587" s="T608">PKZ_19700819_09342-2a.PKZ.073 (100)</ta>
            <ta e="T620" id="Seg_6588" s="T616">PKZ_19700819_09342-2a.PKZ.074 (101)</ta>
            <ta e="T623" id="Seg_6589" s="T621">PKZ_19700819_09342-2a.PKZ.075 (102)</ta>
            <ta e="T627" id="Seg_6590" s="T624">PKZ_19700819_09342-2a.PKZ.076 (103)</ta>
            <ta e="T629" id="Seg_6591" s="T628">PKZ_19700819_09342-2a.PKZ.077 (104)</ta>
            <ta e="T637" id="Seg_6592" s="T634">PKZ_19700819_09342-2a.PKZ.078 (106)</ta>
            <ta e="T656" id="Seg_6593" s="T641">PKZ_19700819_09342-2a.PKZ.079 (109)</ta>
            <ta e="T674" id="Seg_6594" s="T670">PKZ_19700819_09342-2a.PKZ.080 (113)</ta>
            <ta e="T677" id="Seg_6595" s="T675">PKZ_19700819_09342-2a.PKZ.081 (115)</ta>
            <ta e="T698" id="Seg_6596" s="T680">PKZ_19700819_09342-2a.PKZ.082 (117)</ta>
            <ta e="T712" id="Seg_6597" s="T699">PKZ_19700819_09342-2a.PKZ.083 (118)</ta>
            <ta e="T723" id="Seg_6598" s="T713">PKZ_19700819_09342-2a.PKZ.084 (119)</ta>
            <ta e="T728" id="Seg_6599" s="T724">PKZ_19700819_09342-2a.PKZ.085 (120)</ta>
            <ta e="T731" id="Seg_6600" s="T729">PKZ_19700819_09342-2a.PKZ.086 (121)</ta>
            <ta e="T737" id="Seg_6601" s="T731">PKZ_19700819_09342-2a.PKZ.087 (122)</ta>
            <ta e="T750" id="Seg_6602" s="T738">PKZ_19700819_09342-2a.PKZ.088 (123)</ta>
            <ta e="T754" id="Seg_6603" s="T751">PKZ_19700819_09342-2a.PKZ.089 (124)</ta>
            <ta e="T769" id="Seg_6604" s="T755">PKZ_19700819_09342-2a.PKZ.090 (125)</ta>
            <ta e="T773" id="Seg_6605" s="T770">PKZ_19700819_09342-2a.PKZ.091 (126)</ta>
            <ta e="T785" id="Seg_6606" s="T774">PKZ_19700819_09342-2a.PKZ.092 (127)</ta>
            <ta e="T789" id="Seg_6607" s="T786">PKZ_19700819_09342-2a.PKZ.093 (128)</ta>
            <ta e="T801" id="Seg_6608" s="T790">PKZ_19700819_09342-2a.PKZ.094 (129)</ta>
            <ta e="T808" id="Seg_6609" s="T802">PKZ_19700819_09342-2a.PKZ.095 (130)</ta>
            <ta e="T822" id="Seg_6610" s="T809">PKZ_19700819_09342-2a.PKZ.096 (131)</ta>
            <ta e="T830" id="Seg_6611" s="T823">PKZ_19700819_09342-2a.PKZ.097 (132)</ta>
            <ta e="T838" id="Seg_6612" s="T831">PKZ_19700819_09342-2a.PKZ.098 (133)</ta>
            <ta e="T848" id="Seg_6613" s="T839">PKZ_19700819_09342-2a.PKZ.099 (134)</ta>
            <ta e="T856" id="Seg_6614" s="T849">PKZ_19700819_09342-2a.PKZ.100 (135)</ta>
            <ta e="T862" id="Seg_6615" s="T856">PKZ_19700819_09342-2a.PKZ.101 (136)</ta>
            <ta e="T877" id="Seg_6616" s="T863">PKZ_19700819_09342-2a.PKZ.102 (137)</ta>
            <ta e="T886" id="Seg_6617" s="T878">PKZ_19700819_09342-2a.PKZ.103 (138)</ta>
            <ta e="T902" id="Seg_6618" s="T887">PKZ_19700819_09342-2a.PKZ.104 (139)</ta>
            <ta e="T915" id="Seg_6619" s="T903">PKZ_19700819_09342-2a.PKZ.105 (140)</ta>
            <ta e="T927" id="Seg_6620" s="T916">PKZ_19700819_09342-2a.PKZ.106 (141)</ta>
            <ta e="T937" id="Seg_6621" s="T933">PKZ_19700819_09342-2a.PKZ.107 (144)</ta>
            <ta e="T951" id="Seg_6622" s="T938">PKZ_19700819_09342-2a.PKZ.108 (145)</ta>
            <ta e="T963" id="Seg_6623" s="T951">PKZ_19700819_09342-2a.PKZ.109 (146)</ta>
            <ta e="T970" id="Seg_6624" s="T964">PKZ_19700819_09342-2a.PKZ.110 (148)</ta>
            <ta e="T985" id="Seg_6625" s="T971">PKZ_19700819_09342-2a.PKZ.111 (149)</ta>
            <ta e="T1000" id="Seg_6626" s="T986">PKZ_19700819_09342-2a.PKZ.112 (150)</ta>
            <ta e="T1004" id="Seg_6627" s="T1001">PKZ_19700819_09342-2a.PKZ.113 (151)</ta>
            <ta e="T1009" id="Seg_6628" s="T1005">PKZ_19700819_09342-2a.PKZ.114 (152)</ta>
            <ta e="T1033" id="Seg_6629" s="T1026">PKZ_19700819_09342-2a.PKZ.115 (157)</ta>
            <ta e="T1047" id="Seg_6630" s="T1035">PKZ_19700819_09342-2a.PKZ.116 (159)</ta>
            <ta e="T1067" id="Seg_6631" s="T1050">PKZ_19700819_09342-2a.PKZ.117 (161)</ta>
            <ta e="T1070" id="Seg_6632" s="T1067">PKZ_19700819_09342-2a.PKZ.118 (162)</ta>
            <ta e="T1126" id="Seg_6633" s="T1118">PKZ_19700819_09342-2a.PKZ.119 (168)</ta>
            <ta e="T1136" id="Seg_6634" s="T1126">PKZ_19700819_09342-2a.PKZ.120 (170)</ta>
            <ta e="T1191" id="Seg_6635" s="T1173">PKZ_19700819_09342-2a.PKZ.121 (175)</ta>
            <ta e="T1227" id="Seg_6636" s="T1219">PKZ_19700819_09342-2a.PKZ.122 (181)</ta>
            <ta e="T1233" id="Seg_6637" s="T1228">PKZ_19700819_09342-2a.PKZ.123 (182)</ta>
            <ta e="T1238" id="Seg_6638" s="T1234">PKZ_19700819_09342-2a.PKZ.124 (183)</ta>
            <ta e="T1249" id="Seg_6639" s="T1239">PKZ_19700819_09342-2a.PKZ.125 (184)</ta>
            <ta e="T1251" id="Seg_6640" s="T1250">PKZ_19700819_09342-2a.PKZ.126 (185)</ta>
            <ta e="T1253" id="Seg_6641" s="T1252">PKZ_19700819_09342-2a.PKZ.127 (186)</ta>
            <ta e="T1257" id="Seg_6642" s="T1254">PKZ_19700819_09342-2a.PKZ.128 (187)</ta>
            <ta e="T1261" id="Seg_6643" s="T1258">PKZ_19700819_09342-2a.PKZ.129 (189)</ta>
            <ta e="T1265" id="Seg_6644" s="T1261">PKZ_19700819_09342-2a.PKZ.130 (190)</ta>
            <ta e="T1274" id="Seg_6645" s="T1267">PKZ_19700819_09342-2a.PKZ.131 (191)</ta>
            <ta e="T1280" id="Seg_6646" s="T1275">PKZ_19700819_09342-2a.PKZ.132 (192)</ta>
            <ta e="T1286" id="Seg_6647" s="T1281">PKZ_19700819_09342-2a.PKZ.133 (193)</ta>
            <ta e="T1304" id="Seg_6648" s="T1295">PKZ_19700819_09342-2a.PKZ.134 (195)</ta>
            <ta e="T1324" id="Seg_6649" s="T1311">PKZ_19700819_09342-2a.PKZ.135 (198)</ta>
            <ta e="T1329" id="Seg_6650" s="T1325">PKZ_19700819_09342-2a.PKZ.136 (200)</ta>
            <ta e="T1373" id="Seg_6651" s="T1368">PKZ_19700819_09342-2a.PKZ.137 (205)</ta>
            <ta e="T1389" id="Seg_6652" s="T1377">PKZ_19700819_09342-2a.PKZ.138 (208)</ta>
            <ta e="T1449" id="Seg_6653" s="T1444">PKZ_19700819_09342-2a.PKZ.139 (214)</ta>
            <ta e="T1460" id="Seg_6654" s="T1453">PKZ_19700819_09342-2a.PKZ.140 (216)</ta>
            <ta e="T1474" id="Seg_6655" s="T1463">PKZ_19700819_09342-2a.PKZ.141 (220)</ta>
            <ta e="T1485" id="Seg_6656" s="T1474">PKZ_19700819_09342-2a.PKZ.142 (221)</ta>
            <ta e="T1532" id="Seg_6657" s="T1527">PKZ_19700819_09342-2a.PKZ.143 (225)</ta>
            <ta e="T1543" id="Seg_6658" s="T1533">PKZ_19700819_09342-2a.PKZ.144 (226)</ta>
            <ta e="T1607" id="Seg_6659" s="T1604">PKZ_19700819_09342-2a.PKZ.145 (232)</ta>
            <ta e="T1627" id="Seg_6660" s="T1612">PKZ_19700819_09342-2a.PKZ.146 (234)</ta>
            <ta e="T1631" id="Seg_6661" s="T1628">PKZ_19700819_09342-2a.PKZ.147 (236)</ta>
            <ta e="T1641" id="Seg_6662" s="T1634">PKZ_19700819_09342-2a.PKZ.148 (238)</ta>
            <ta e="T1652" id="Seg_6663" s="T1643">PKZ_19700819_09342-2a.PKZ.149 (240)</ta>
            <ta e="T1660" id="Seg_6664" s="T1653">PKZ_19700819_09342-2a.PKZ.150 (241)</ta>
            <ta e="T1664" id="Seg_6665" s="T1661">PKZ_19700819_09342-2a.PKZ.151 (242)</ta>
            <ta e="T1673" id="Seg_6666" s="T1665">PKZ_19700819_09342-2a.PKZ.152 (243)</ta>
            <ta e="T1678" id="Seg_6667" s="T1674">PKZ_19700819_09342-2a.PKZ.153 (244)</ta>
            <ta e="T1684" id="Seg_6668" s="T1679">PKZ_19700819_09342-2a.PKZ.154 (245)</ta>
            <ta e="T1694" id="Seg_6669" s="T1685">PKZ_19700819_09342-2a.PKZ.155 (246)</ta>
            <ta e="T1698" id="Seg_6670" s="T1694">PKZ_19700819_09342-2a.PKZ.156 (247)</ta>
            <ta e="T1705" id="Seg_6671" s="T1699">PKZ_19700819_09342-2a.PKZ.157 (248)</ta>
            <ta e="T1710" id="Seg_6672" s="T1707">PKZ_19700819_09342-2a.PKZ.158 (250)</ta>
            <ta e="T1715" id="Seg_6673" s="T1713">PKZ_19700819_09342-2a.PKZ.159 (253)</ta>
            <ta e="T1722" id="Seg_6674" s="T1717">PKZ_19700819_09342-2a.PKZ.160 (254)</ta>
            <ta e="T1729" id="Seg_6675" s="T1723">PKZ_19700819_09342-2a.PKZ.161 (255)</ta>
            <ta e="T1734" id="Seg_6676" s="T1730">PKZ_19700819_09342-2a.PKZ.162 (256)</ta>
            <ta e="T1740" id="Seg_6677" s="T1734">PKZ_19700819_09342-2a.PKZ.163 (257)</ta>
            <ta e="T1745" id="Seg_6678" s="T1740">PKZ_19700819_09342-2a.PKZ.164 (258)</ta>
            <ta e="T1747" id="Seg_6679" s="T1746">PKZ_19700819_09342-2a.PKZ.165 (259)</ta>
            <ta e="T1752" id="Seg_6680" s="T1748">PKZ_19700819_09342-2a.PKZ.166 (260)</ta>
            <ta e="T1761" id="Seg_6681" s="T1752">PKZ_19700819_09342-2a.PKZ.167 (261)</ta>
            <ta e="T1784" id="Seg_6682" s="T1775">PKZ_19700819_09342-2a.PKZ.168 (265)</ta>
            <ta e="T1797" id="Seg_6683" s="T1785">PKZ_19700819_09342-2a.PKZ.169 (266)</ta>
            <ta e="T1799" id="Seg_6684" s="T1798">PKZ_19700819_09342-2a.PKZ.170 (267)</ta>
            <ta e="T1801" id="Seg_6685" s="T1800">PKZ_19700819_09342-2a.PKZ.171 (268)</ta>
            <ta e="T1806" id="Seg_6686" s="T1802">PKZ_19700819_09342-2a.PKZ.172 (269)</ta>
            <ta e="T1853" id="Seg_6687" s="T1844">PKZ_19700819_09342-2a.PKZ.173 (274)</ta>
            <ta e="T1864" id="Seg_6688" s="T1862">PKZ_19700819_09342-2a.PKZ.174 (277)</ta>
            <ta e="T1874" id="Seg_6689" s="T1865">PKZ_19700819_09342-2a.PKZ.175 (278)</ta>
            <ta e="T1877" id="Seg_6690" s="T1875">PKZ_19700819_09342-2a.PKZ.176 (279)</ta>
            <ta e="T1882" id="Seg_6691" s="T1878">PKZ_19700819_09342-2a.PKZ.177 (280)</ta>
            <ta e="T1890" id="Seg_6692" s="T1883">PKZ_19700819_09342-2a.PKZ.178 (281)</ta>
            <ta e="T1892" id="Seg_6693" s="T1891">PKZ_19700819_09342-2a.PKZ.179 (282)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T10" id="Seg_6694" s="T1">Как, не знаю, ((…)) сколько ты работал. </ta>
            <ta e="T47" id="Seg_6695" s="T43">Да, (мужик) такой, наверное. </ta>
            <ta e="T97" id="Seg_6696" s="T92">Ну, ну. </ta>
            <ta e="T124" id="Seg_6697" s="T102">(Он-) Он мне все писал, звал, а я ему написала: Агафон Иванович приедет, тогда… </ta>
            <ta e="T136" id="Seg_6698" s="T130">(Он г-) Он потома-ка… </ta>
            <ta e="T146" id="Seg_6699" s="T137">Он мне пишет: "Вот приеду четырнадцатого числа к тебе". </ta>
            <ta e="T156" id="Seg_6700" s="T147">Ну, я и дожидаю, а он вперед, скорее приехал. </ta>
            <ta e="T166" id="Seg_6701" s="T157">Ну, я уже не отказалась потом. </ta>
            <ta e="T177" id="Seg_6702" s="T167">Племянник не пускал, "зачем ты поедешь" да, то, другое да. </ta>
            <ta e="T199" id="Seg_6703" s="T181">Думаю, ну что, раз я так обещалась, то приеду, а теперь человек приедет, один поедет. </ta>
            <ta e="T209" id="Seg_6704" s="T200">Ну, он у меня только одну ночь и ночевал. </ta>
            <ta e="T225" id="Seg_6705" s="T214">Ночевал, ночевал. </ta>
            <ta e="T232" id="Seg_6706" s="T226">И в баню сходил, помылся. </ta>
            <ta e="T238" id="Seg_6707" s="T234">Ходил… </ta>
            <ta e="T244" id="Seg_6708" s="T242">Ну. </ta>
            <ta e="T248" id="Seg_6709" s="T244">И ходил на пруд. </ta>
            <ta e="T250" id="Seg_6710" s="T249">Тамо-ка. </ta>
            <ta e="T256" id="Seg_6711" s="T251">И потом зашел к мельнику. </ta>
            <ta e="T267" id="Seg_6712" s="T257">Пришел, говорит: "Ой, я старушку нашел, ну, на ваш язык". </ta>
            <ta e="T275" id="Seg_6713" s="T268">Я говорю: "Не говори, это моя подружка". </ta>
            <ta e="T300" id="Seg_6714" s="T277">Я часто к ей хожу, она вытащит Евангелие, она не умеет на русский, а я ей почитаю, с ей помолимся. </ta>
            <ta e="T319" id="Seg_6715" s="T315">Ну, теперь говорить можно? </ta>
            <ta e="T324" id="Seg_6716" s="T320">Măna (pʼaŋdəbi) măn kagam. </ta>
            <ta e="T327" id="Seg_6717" s="T325">Teʔtə kö. </ta>
            <ta e="T332" id="Seg_6718" s="T328">"Šoʔ döber, šoʔ döber". </ta>
            <ta e="T335" id="Seg_6719" s="T332">Măn (šindəzi) kalam? </ta>
            <ta e="T339" id="Seg_6720" s="T336">Ĭmbidə ej tĭmnem. </ta>
            <ta e="T344" id="Seg_6721" s="T340">Dĭgəttə pʼaŋdəbiam dĭʔnə sazən. </ta>
            <ta e="T353" id="Seg_6722" s="T345">Mămbiam: "Šoləj Агафон Иванович (măna), dĭgəttə măn šoləm". </ta>
            <ta e="T361" id="Seg_6723" s="T354">Dĭgəttə dĭ măna pʼaŋdlaʔbə: "Šoləm bostə šiʔnʼileʔ". </ta>
            <ta e="T369" id="Seg_6724" s="T362">Dĭgəttə šobi miʔnʼibeʔ, măn ej edəbiem dĭm. </ta>
            <ta e="T375" id="Seg_6725" s="T370">Nu, măn nʼeveskăm dĭm detleʔbə. </ta>
            <ta e="T380" id="Seg_6726" s="T376">"Dö, tăn kuza šobi". </ta>
            <ta e="T385" id="Seg_6727" s="T381">(M-) Măn nuʔməbiem nʼiʔnə. </ta>
            <ta e="T389" id="Seg_6728" s="T386">"Oj, büžü šobial". </ta>
            <ta e="T395" id="Seg_6729" s="T389">(A dĭ=) Dĭgəttə dĭ šobi, amorbi. </ta>
            <ta e="T398" id="Seg_6730" s="T396">Iʔbəbi kunolzittə. </ta>
            <ta e="T404" id="Seg_6731" s="T399">Dĭgəttə uʔbdəbi, kambi bünə, tʼerbəndə. </ta>
            <ta e="T410" id="Seg_6732" s="T405">Dĭn tože dĭrgit (ibi=) kubi. </ta>
            <ta e="T413" id="Seg_6733" s="T411">Dʼăbaktərbi dĭn. </ta>
            <ta e="T417" id="Seg_6734" s="T414">Dĭgəttə šobi maːʔndə. </ta>
            <ta e="T421" id="Seg_6735" s="T418">Dĭgəttə moltʼanə kambi. </ta>
            <ta e="T425" id="Seg_6736" s="T422">Măn dĭzi mĭmbiem. </ta>
            <ta e="T435" id="Seg_6737" s="T426">Dĭn (b-) bü dĭʔnə, dʼibige, šišəge bü, sabən mĭbiem. </ta>
            <ta e="T442" id="Seg_6738" s="T436">Dĭ băzəjdəbi dĭn, dĭgəttə šobi maːʔnʼi. </ta>
            <ta e="T445" id="Seg_6739" s="T443">Mămbiam: "Amaʔ". </ta>
            <ta e="T449" id="Seg_6740" s="T445">"Măn dĭn ambiam, tʼerbəngən". </ta>
            <ta e="T452" id="Seg_6741" s="T450">Ej ambi. </ta>
            <ta e="T457" id="Seg_6742" s="T453">Dĭgəttə (i-) kudajdə numan üzəbibeʔ. </ta>
            <ta e="T464" id="Seg_6743" s="T458">Dĭgəttə dĭ iʔbəbi, măn tože iʔbəbiem. </ta>
            <ta e="T469" id="Seg_6744" s="T465">Ertən uʔbdlaʔbəm, munujʔ mĭndərbiem. </ta>
            <ta e="T474" id="Seg_6745" s="T470">Amorbibaʔ, (kam-) kambibaʔ aftobustə. </ta>
            <ta e="T480" id="Seg_6746" s="T475">Dĭn nubibaʔ, dĭgəttə šobi aftobus. </ta>
            <ta e="T487" id="Seg_6747" s="T481">Miʔ (amno- amn-) amnobibaʔ, Kazan turanə kambibaʔ. </ta>
            <ta e="T492" id="Seg_6748" s="T488">Dĭn tože kudajdə numan üzəbiʔi. </ta>
            <ta e="T499" id="Seg_6749" s="T493">Kamən sumna šobi, dĭgəttə kambibaʔ Ujardə. </ta>
            <ta e="T503" id="Seg_6750" s="T500">(Dĭn=) Dĭn šaːbibaʔ. </ta>
            <ta e="T510" id="Seg_6751" s="T504">Tože dĭn (măn) ige il tože. </ta>
            <ta e="T515" id="Seg_6752" s="T511">(Dĭgəttə dĭg- Dĭgəttə ert-). </ta>
            <ta e="T522" id="Seg_6753" s="T516">Dĭgəttə ertən uʔbdəbibaʔ i kambibaʔ Krasnăjarskəgən. </ta>
            <ta e="T531" id="Seg_6754" s="T523">Dĭn kambibaʔ gijen nʼergölaʔbəʔjə, dĭn amnobibaʔ i kalla dʼürbibaʔ. </ta>
            <ta e="T536" id="Seg_6755" s="T532">Năvăsibirʼskanə, dĭn tože šaːbibaʔ. </ta>
            <ta e="T544" id="Seg_6756" s="T537">Dĭgəttə dĭn bazo amnobibaʔ i šobibaʔ Măskvanə. </ta>
            <ta e="T555" id="Seg_6757" s="T545">(D-) Dön dʼünə amnobibaʔ, dĭgəttə (amnobibaʔ=) amnobibaʔ mašinanə, kambibaʔ Măskvanə. </ta>
            <ta e="T559" id="Seg_6758" s="T555">(Dĭ-) Dĭn măna kürbi. </ta>
            <ta e="T564" id="Seg_6759" s="T560">Tože il dĭn ibiʔi. </ta>
            <ta e="T576" id="Seg_6760" s="T565">Dĭgəttə parbibaʔ, (m-) bazo amnolbibaʔ, i (dö=) dö turanə (so-) šobibaʔ. </ta>
            <ta e="T582" id="Seg_6761" s="T577">Dĭgəttə döʔə (gu) kambibaʔ, Tartu. </ta>
            <ta e="T587" id="Seg_6762" s="T583">Dĭn nagur nedʼelʼa amnobiam. </ta>
            <ta e="T596" id="Seg_6763" s="T588">Bazo (kamb-) măna ibi, kambibaʔ gijen iʔgö sĭrujanə. </ta>
            <ta e="T601" id="Seg_6764" s="T597">Dĭn iʔgö il ibiʔi. </ta>
            <ta e="T607" id="Seg_6765" s="T602">Nʼemsəʔi ibiʔi, i хохлы ibiʔi. </ta>
            <ta e="T616" id="Seg_6766" s="T608">Iʔgö dĭn, kundʼo, šide nüdʼi măn šaːbiam dĭn. </ta>
            <ta e="T620" id="Seg_6767" s="T616">Dĭgəttə bazo döbər deʔpi. </ta>
            <ta e="T623" id="Seg_6768" s="T621">Dĭʔnə nükenə. </ta>
            <ta e="T627" id="Seg_6769" s="T624">I dĭn amnolaʔbəm. </ta>
            <ta e="T629" id="Seg_6770" s="T628">Kabarləj. </ta>
            <ta e="T637" id="Seg_6771" s="T634">((BRK)) Останови маленько. </ta>
            <ta e="T656" id="Seg_6772" s="T641">(Это я=) Это я вот, всё (как=) сколь он мне годов писал. </ta>
            <ta e="T674" id="Seg_6773" s="T670">Ага, ну-ну. </ta>
            <ta e="T677" id="Seg_6774" s="T675">Можно. </ta>
            <ta e="T698" id="Seg_6775" s="T680">Когда он всё писал ко мне четыре года, а я отписывалася: "Приеду, приеду", но не с кем было. </ta>
            <ta e="T712" id="Seg_6776" s="T699">Хотела с одним человеком приехать, он шибко больной был, хворал, а потом помер. </ta>
            <ta e="T723" id="Seg_6777" s="T713">(Тогда я=) Тогда он пишет мне: "Я приеду за тобой". </ta>
            <ta e="T728" id="Seg_6778" s="T724">(И п=) И приехал. </ta>
            <ta e="T731" id="Seg_6779" s="T729">Четырнадцатого числа. </ta>
            <ta e="T737" id="Seg_6780" s="T731">Я ждала, а он вперед приехал. </ta>
            <ta e="T750" id="Seg_6781" s="T738">Тагды пошел на мельницу, там нашел людей, на евонной (язы-) (наречиях) говорят. </ta>
            <ta e="T754" id="Seg_6782" s="T751">И пришел назад. </ta>
            <ta e="T769" id="Seg_6783" s="T755">Тады пошел в баню, я пошла ему показала горячу воду, холодну, и мыло внесла. </ta>
            <ta e="T773" id="Seg_6784" s="T770">Он вымылся, пришел. </ta>
            <ta e="T785" id="Seg_6785" s="T774">Я говорю: ("Ешь", ему ужин, а) он говорит: "Я там поужинал". </ta>
            <ta e="T789" id="Seg_6786" s="T786">И легли спать. </ta>
            <ta e="T801" id="Seg_6787" s="T790">Утром встали, я наварила яиц, приготовила, поели и пошли, на автобус. </ta>
            <ta e="T808" id="Seg_6788" s="T802">Тагды сяли и в Агинско приехали. </ta>
            <ta e="T822" id="Seg_6789" s="T809">Там тоже (н-) пошли к сестрам, тамо-ка были до вечера, до пяти часов. </ta>
            <ta e="T830" id="Seg_6790" s="T823">А в пять часов поехали в Уяр. </ta>
            <ta e="T838" id="Seg_6791" s="T831">Там ночевали тоже у верующих у братьев. </ta>
            <ta e="T848" id="Seg_6792" s="T839">На другой день сели в Красноярска, приехал из Красноярского. </ta>
            <ta e="T856" id="Seg_6793" s="T849">На самолет сяли и приехали в Новосибирска. </ta>
            <ta e="T862" id="Seg_6794" s="T856">С Новосибирского ((PAUSE)) коло Москвы есть посадка. </ta>
            <ta e="T877" id="Seg_6795" s="T863">А потом с этой посадки мы (поехали в=) съездили на легковой машине в Москву. </ta>
            <ta e="T886" id="Seg_6796" s="T878">Он там снял меня (с этим=) с людям. </ta>
            <ta e="T902" id="Seg_6797" s="T887">А потом воротились назад, тады сяли опять на самолет и (при-) приехали сюда, в эту… </ta>
            <ta e="T915" id="Seg_6798" s="T903">В этот город или в деревню или как ли я сказала уж? </ta>
            <ta e="T927" id="Seg_6799" s="T916">Вот сюда, в Таллину, в Таллину. </ta>
            <ta e="T937" id="Seg_6800" s="T933">А потом отседа (уех-)… </ta>
            <ta e="T951" id="Seg_6801" s="T938">Вот только не сказала, мы уехали на машине, а я сказала kambibaʔ (уе-). </ta>
            <ta e="T963" id="Seg_6802" s="T951">Ну, все равно, уехали, уехали в Тарту. </ta>
            <ta e="T970" id="Seg_6803" s="T964">И там я три недели жила. </ta>
            <ta e="T985" id="Seg_6804" s="T971">Тады он меня увез туды, где цурияны, там две ночи ночевали, это я говорила. </ta>
            <ta e="T1000" id="Seg_6805" s="T986">А потом оттель обратно сюды вот привез, и уже тут вот более недели живу. </ta>
            <ta e="T1004" id="Seg_6806" s="T1001">У этой женщины. </ta>
            <ta e="T1009" id="Seg_6807" s="T1005">Это всё я рассказала. </ta>
            <ta e="T1033" id="Seg_6808" s="T1026">Ну, не слыхать шума. </ta>
            <ta e="T1047" id="Seg_6809" s="T1035">Ребятишки где-то притихли, они вон… </ta>
            <ta e="T1067" id="Seg_6810" s="T1050">До этого было здорово кричали, может пообедали да спать легли, я и то маленько задремала. </ta>
            <ta e="T1070" id="Seg_6811" s="T1067">Ждали, ждали вас. </ta>
            <ta e="T1126" id="Seg_6812" s="T1118">Не знаю, что сказать. </ta>
            <ta e="T1136" id="Seg_6813" s="T1126">Что еще сказать. </ta>
            <ta e="T1191" id="Seg_6814" s="T1173">Ну, и я тоже и так как племянник меня не пускал, я говорила али нет, я забыла. </ta>
            <ta e="T1227" id="Seg_6815" s="T1219">Dĭgəttə Arpit šobi, măn племянник ej öʔlubi меня. </ta>
            <ta e="T1233" id="Seg_6816" s="T1228">A невестка măndə: "Kanaʔ, kanaʔ". </ta>
            <ta e="T1238" id="Seg_6817" s="T1234">Dĭgəttə măn kambiam döbər. </ta>
            <ta e="T1249" id="Seg_6818" s="T1239">А потом невестка говорит: поезжай, а племянник говорит: не езди. </ta>
            <ta e="T1251" id="Seg_6819" s="T1250">Вот. </ta>
            <ta e="T1253" id="Seg_6820" s="T1252">Так. </ta>
            <ta e="T1257" id="Seg_6821" s="T1254">(Так вроде). </ta>
            <ta e="T1261" id="Seg_6822" s="T1258">Потом чего еще. </ta>
            <ta e="T1265" id="Seg_6823" s="T1261">Потом чего еще рассказать. </ta>
            <ta e="T1274" id="Seg_6824" s="T1267">Это уже я говорила, как финны были. </ta>
            <ta e="T1280" id="Seg_6825" s="T1275">Там, у вас есть (наматано). </ta>
            <ta e="T1286" id="Seg_6826" s="T1281">Чего еще сказать, не знаю. </ta>
            <ta e="T1304" id="Seg_6827" s="T1295">Ну, там я (расска-). </ta>
            <ta e="T1324" id="Seg_6828" s="T1311">Ну, чего (одно слово=) одно и то же говорить. </ta>
            <ta e="T1329" id="Seg_6829" s="T1325">Погоди, друго надо ((…)). </ta>
            <ta e="T1373" id="Seg_6830" s="T1368">(Там=) Там три было их. </ta>
            <ta e="T1389" id="Seg_6831" s="T1377">Три, (и все=) на всех говорила, они знают, где вышло хорошо. </ta>
            <ta e="T1449" id="Seg_6832" s="T1444">У её нет радио. </ta>
            <ta e="T1460" id="Seg_6833" s="T1453">А это не знаю чего. </ta>
            <ta e="T1474" id="Seg_6834" s="T1463">Она вчера говорила: "Жалко нету радио, может быть бы слухали". </ta>
            <ta e="T1485" id="Seg_6835" s="T1474">А я говорю: "Может, еще не скоро справят". </ta>
            <ta e="T1532" id="Seg_6836" s="T1527">Они же скоро пошли так. </ta>
            <ta e="T1543" id="Seg_6837" s="T1533">И мы после вас тоже скоро пошли, немного были там. </ta>
            <ta e="T1607" id="Seg_6838" s="T1604">(Погодить надо) надо. </ta>
            <ta e="T1627" id="Seg_6839" s="T1612">Нас, Агафон Иванович, тоже всё забывается, уж много лет не говорила. </ta>
            <ta e="T1631" id="Seg_6840" s="T1628">((…)) еще маленько. </ta>
            <ta e="T1641" id="Seg_6841" s="T1634">Не с кем разговаривать. </ta>
            <ta e="T1652" id="Seg_6842" s="T1643">Там, пойду к своей племяннице, толмачу, толмачу. </ta>
            <ta e="T1660" id="Seg_6843" s="T1653">Тут зять прибежит: "Заболтали", — закричит на нас. </ta>
            <ta e="T1664" id="Seg_6844" s="T1661">Внука своего заставляю. </ta>
            <ta e="T1673" id="Seg_6845" s="T1665">Даже который настоящий, и вот там эта, приходила… </ta>
            <ta e="T1678" id="Seg_6846" s="T1674">(Ага- Аган-) Агафьей звать. </ta>
            <ta e="T1684" id="Seg_6847" s="T1679">И то она не знает. </ta>
            <ta e="T1694" id="Seg_6848" s="T1685">Сказала "сахар", а она говорит: "Врет, сахар не так". </ta>
            <ta e="T1698" id="Seg_6849" s="T1694">"Ну, — говорит, — скажи, как". </ta>
            <ta e="T1705" id="Seg_6850" s="T1699">И она сама не знает. </ta>
            <ta e="T1710" id="Seg_6851" s="T1707">Ну. </ta>
            <ta e="T1715" id="Seg_6852" s="T1713">Ну чего? </ta>
            <ta e="T1722" id="Seg_6853" s="T1717">Ну, давай, открой, будем говорить. </ta>
            <ta e="T1729" id="Seg_6854" s="T1723">Taldʼen miʔ jaʔtarla kambibaʔ onʼiʔ nükenə. </ta>
            <ta e="T1734" id="Seg_6855" s="T1730">Amnobibaʔ aftobustə, dĭbər šobibaʔ. </ta>
            <ta e="T1740" id="Seg_6856" s="T1734">Dĭn šobiʔi il dʼăbaktərzittə măn šĭkenə. </ta>
            <ta e="T1745" id="Seg_6857" s="T1740">Măn dĭzeŋdə bar mămbiam, mămbiam. </ta>
            <ta e="T1747" id="Seg_6858" s="T1746">Dĭgəttə… </ta>
            <ta e="T1752" id="Seg_6859" s="T1748">Надо еще надумать чего. </ta>
            <ta e="T1761" id="Seg_6860" s="T1752">Это вчера что я ходила. </ta>
            <ta e="T1784" id="Seg_6861" s="T1775">Мы вчера ходили в гости там, к одной старушке. </ta>
            <ta e="T1797" id="Seg_6862" s="T1785">Там (приехали=) пришли люди, и мы разговаривали (мо-) на мое (ра-) наречие. </ta>
            <ta e="T1799" id="Seg_6863" s="T1798">((BRK)). </ta>
            <ta e="T1801" id="Seg_6864" s="T1800">((DMG)) Ладно. </ta>
            <ta e="T1806" id="Seg_6865" s="T1802">Ну, чего еще рассказать. </ta>
            <ta e="T1853" id="Seg_6866" s="T1844">Думаешь много рассказать, а потом опять уходит. ((DMG))</ta>
            <ta e="T1864" id="Seg_6867" s="T1862">Ну, открой. </ta>
            <ta e="T1874" id="Seg_6868" s="T1865">((DMG)) Măn oʔbdəbiam (šo-) šonəsʼtə döbər, il măndəʔi: "Iʔ kanaʔ. </ta>
            <ta e="T1877" id="Seg_6869" s="T1875">Külalləl dĭn. </ta>
            <ta e="T1882" id="Seg_6870" s="T1878">Măn mămbiam:" "Ej pĭmniem. </ta>
            <ta e="T1890" id="Seg_6871" s="T1883">(Dĭn m-) Măna dĭn tože dʼünə elləʔi. </ta>
            <ta e="T1892" id="Seg_6872" s="T1891">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T113" id="Seg_6873" s="T112">а</ta>
            <ta e="T152" id="Seg_6874" s="T151">а</ta>
            <ta e="T321" id="Seg_6875" s="T320">măna</ta>
            <ta e="T322" id="Seg_6876" s="T321">pʼaŋdə-bi</ta>
            <ta e="T323" id="Seg_6877" s="T322">măn</ta>
            <ta e="T324" id="Seg_6878" s="T323">kaga-m</ta>
            <ta e="T326" id="Seg_6879" s="T325">teʔtə</ta>
            <ta e="T327" id="Seg_6880" s="T326">kö</ta>
            <ta e="T329" id="Seg_6881" s="T328">šo-ʔ</ta>
            <ta e="T330" id="Seg_6882" s="T329">döber</ta>
            <ta e="T331" id="Seg_6883" s="T330">šo-ʔ</ta>
            <ta e="T332" id="Seg_6884" s="T331">döber</ta>
            <ta e="T333" id="Seg_6885" s="T332">măn</ta>
            <ta e="T334" id="Seg_6886" s="T333">šində-zi</ta>
            <ta e="T335" id="Seg_6887" s="T334">ka-la-m</ta>
            <ta e="T337" id="Seg_6888" s="T336">ĭmbi=də</ta>
            <ta e="T338" id="Seg_6889" s="T337">ej</ta>
            <ta e="T339" id="Seg_6890" s="T338">tĭmne-m</ta>
            <ta e="T341" id="Seg_6891" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_6892" s="T341">pʼaŋdə-bia-m</ta>
            <ta e="T343" id="Seg_6893" s="T342">dĭʔ-nə</ta>
            <ta e="T344" id="Seg_6894" s="T343">sazən</ta>
            <ta e="T346" id="Seg_6895" s="T345">măm-bia-m</ta>
            <ta e="T347" id="Seg_6896" s="T346">šo-lə-j</ta>
            <ta e="T350" id="Seg_6897" s="T349">măna</ta>
            <ta e="T351" id="Seg_6898" s="T350">dĭgəttə</ta>
            <ta e="T352" id="Seg_6899" s="T351">măn</ta>
            <ta e="T353" id="Seg_6900" s="T352">šo-lə-m</ta>
            <ta e="T355" id="Seg_6901" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_6902" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_6903" s="T356">măna</ta>
            <ta e="T358" id="Seg_6904" s="T357">pʼaŋd-laʔbə</ta>
            <ta e="T359" id="Seg_6905" s="T358">šo-lə-m</ta>
            <ta e="T360" id="Seg_6906" s="T359">bos-tə</ta>
            <ta e="T361" id="Seg_6907" s="T360">šiʔnʼileʔ</ta>
            <ta e="T363" id="Seg_6908" s="T362">dĭgəttə</ta>
            <ta e="T364" id="Seg_6909" s="T363">šo-bi</ta>
            <ta e="T365" id="Seg_6910" s="T364">miʔnʼibeʔ</ta>
            <ta e="T366" id="Seg_6911" s="T365">măn</ta>
            <ta e="T367" id="Seg_6912" s="T366">ej</ta>
            <ta e="T368" id="Seg_6913" s="T367">edə-bie-m</ta>
            <ta e="T369" id="Seg_6914" s="T368">dĭ-m</ta>
            <ta e="T371" id="Seg_6915" s="T370">nu</ta>
            <ta e="T372" id="Seg_6916" s="T371">măn</ta>
            <ta e="T373" id="Seg_6917" s="T372">nʼeveskă-m</ta>
            <ta e="T374" id="Seg_6918" s="T373">dĭ-m</ta>
            <ta e="T375" id="Seg_6919" s="T374">det-leʔbə</ta>
            <ta e="T377" id="Seg_6920" s="T376">dö</ta>
            <ta e="T378" id="Seg_6921" s="T377">tăn</ta>
            <ta e="T379" id="Seg_6922" s="T378">kuza</ta>
            <ta e="T380" id="Seg_6923" s="T379">šo-bi</ta>
            <ta e="T383" id="Seg_6924" s="T382">măn</ta>
            <ta e="T384" id="Seg_6925" s="T383">nuʔmə-bie-m</ta>
            <ta e="T385" id="Seg_6926" s="T384">nʼiʔnə</ta>
            <ta e="T387" id="Seg_6927" s="T386">Oj</ta>
            <ta e="T388" id="Seg_6928" s="T387">büžü</ta>
            <ta e="T389" id="Seg_6929" s="T388">šo-bia-l</ta>
            <ta e="T390" id="Seg_6930" s="T389">a</ta>
            <ta e="T391" id="Seg_6931" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_6932" s="T391">dĭgəttə</ta>
            <ta e="T393" id="Seg_6933" s="T392">dĭ</ta>
            <ta e="T394" id="Seg_6934" s="T393">šo-bi</ta>
            <ta e="T395" id="Seg_6935" s="T394">amor-bi</ta>
            <ta e="T397" id="Seg_6936" s="T396">iʔbə-bi</ta>
            <ta e="T398" id="Seg_6937" s="T397">kunol-zittə</ta>
            <ta e="T400" id="Seg_6938" s="T399">dĭgəttə</ta>
            <ta e="T401" id="Seg_6939" s="T400">uʔbdə-bi</ta>
            <ta e="T402" id="Seg_6940" s="T401">kam-bi</ta>
            <ta e="T403" id="Seg_6941" s="T402">bü-nə</ta>
            <ta e="T404" id="Seg_6942" s="T403">tʼerbə-ndə</ta>
            <ta e="T406" id="Seg_6943" s="T405">dĭn</ta>
            <ta e="T407" id="Seg_6944" s="T406">tože</ta>
            <ta e="T408" id="Seg_6945" s="T407">dĭrgit</ta>
            <ta e="T409" id="Seg_6946" s="T408">i-bi</ta>
            <ta e="T410" id="Seg_6947" s="T409">ku-bi</ta>
            <ta e="T412" id="Seg_6948" s="T411">dʼăbaktər-bi</ta>
            <ta e="T413" id="Seg_6949" s="T412">dĭn</ta>
            <ta e="T415" id="Seg_6950" s="T414">dĭgəttə</ta>
            <ta e="T416" id="Seg_6951" s="T415">šo-bi</ta>
            <ta e="T417" id="Seg_6952" s="T416">maːʔ-ndə</ta>
            <ta e="T419" id="Seg_6953" s="T418">dĭgəttə</ta>
            <ta e="T420" id="Seg_6954" s="T419">moltʼa-nə</ta>
            <ta e="T421" id="Seg_6955" s="T420">kam-bi</ta>
            <ta e="T423" id="Seg_6956" s="T422">măn</ta>
            <ta e="T424" id="Seg_6957" s="T423">dĭ-zi</ta>
            <ta e="T425" id="Seg_6958" s="T424">mĭm-bie-m</ta>
            <ta e="T427" id="Seg_6959" s="T426">dĭn</ta>
            <ta e="T429" id="Seg_6960" s="T428">bü</ta>
            <ta e="T430" id="Seg_6961" s="T429">dĭʔ-nə</ta>
            <ta e="T431" id="Seg_6962" s="T430">dʼibige</ta>
            <ta e="T432" id="Seg_6963" s="T431">šišəge</ta>
            <ta e="T433" id="Seg_6964" s="T432">bü</ta>
            <ta e="T434" id="Seg_6965" s="T433">sabən</ta>
            <ta e="T435" id="Seg_6966" s="T434">mĭ-bie-m</ta>
            <ta e="T437" id="Seg_6967" s="T436">dĭ</ta>
            <ta e="T438" id="Seg_6968" s="T437">băzə-jd-ə-bi</ta>
            <ta e="T439" id="Seg_6969" s="T438">dĭn</ta>
            <ta e="T440" id="Seg_6970" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_6971" s="T440">šo-bi</ta>
            <ta e="T442" id="Seg_6972" s="T441">maːʔ-nʼi</ta>
            <ta e="T444" id="Seg_6973" s="T443">măm-bia-m</ta>
            <ta e="T445" id="Seg_6974" s="T444">am-a-ʔ</ta>
            <ta e="T446" id="Seg_6975" s="T445">măn</ta>
            <ta e="T447" id="Seg_6976" s="T446">dĭn</ta>
            <ta e="T448" id="Seg_6977" s="T447">am-bia-m</ta>
            <ta e="T449" id="Seg_6978" s="T448">tʼerbən-gən</ta>
            <ta e="T451" id="Seg_6979" s="T450">ej</ta>
            <ta e="T452" id="Seg_6980" s="T451">am-bi</ta>
            <ta e="T454" id="Seg_6981" s="T453">dĭgəttə</ta>
            <ta e="T456" id="Seg_6982" s="T455">kudaj-də</ta>
            <ta e="T457" id="Seg_6983" s="T456">numan üzə-bi-beʔ</ta>
            <ta e="T459" id="Seg_6984" s="T458">dĭgəttə</ta>
            <ta e="T460" id="Seg_6985" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_6986" s="T460">iʔbə-bi</ta>
            <ta e="T462" id="Seg_6987" s="T461">măn</ta>
            <ta e="T463" id="Seg_6988" s="T462">tože</ta>
            <ta e="T464" id="Seg_6989" s="T463">iʔbə-bie-m</ta>
            <ta e="T466" id="Seg_6990" s="T465">ertə-n</ta>
            <ta e="T467" id="Seg_6991" s="T466">uʔbd-laʔbə-m</ta>
            <ta e="T468" id="Seg_6992" s="T467">munuj-ʔ</ta>
            <ta e="T469" id="Seg_6993" s="T468">mĭndər-bie-m</ta>
            <ta e="T471" id="Seg_6994" s="T470">amor-bi-baʔ</ta>
            <ta e="T472" id="Seg_6995" s="T471">kam</ta>
            <ta e="T473" id="Seg_6996" s="T472">kam-bi-baʔ</ta>
            <ta e="T474" id="Seg_6997" s="T473">aftobus-tə</ta>
            <ta e="T476" id="Seg_6998" s="T475">dĭn</ta>
            <ta e="T477" id="Seg_6999" s="T476">nu-bi-baʔ</ta>
            <ta e="T478" id="Seg_7000" s="T477">dĭgəttə</ta>
            <ta e="T479" id="Seg_7001" s="T478">šo-bi</ta>
            <ta e="T480" id="Seg_7002" s="T479">aftobus</ta>
            <ta e="T482" id="Seg_7003" s="T481">miʔ</ta>
            <ta e="T485" id="Seg_7004" s="T484">amno-bi-baʔ</ta>
            <ta e="T1918" id="Seg_7005" s="T485">Kazan</ta>
            <ta e="T486" id="Seg_7006" s="T1918">tura-nə</ta>
            <ta e="T487" id="Seg_7007" s="T486">kam-bi-baʔ</ta>
            <ta e="T489" id="Seg_7008" s="T488">dĭn</ta>
            <ta e="T490" id="Seg_7009" s="T489">tože</ta>
            <ta e="T491" id="Seg_7010" s="T490">kudaj-də</ta>
            <ta e="T492" id="Seg_7011" s="T491">numan üzə-bi-ʔi</ta>
            <ta e="T494" id="Seg_7012" s="T493">kamən</ta>
            <ta e="T495" id="Seg_7013" s="T494">sumna</ta>
            <ta e="T496" id="Seg_7014" s="T495">šo-bi</ta>
            <ta e="T497" id="Seg_7015" s="T496">dĭgəttə</ta>
            <ta e="T498" id="Seg_7016" s="T497">kam-bi-baʔ</ta>
            <ta e="T499" id="Seg_7017" s="T498">Ujar-də</ta>
            <ta e="T501" id="Seg_7018" s="T500">dĭn</ta>
            <ta e="T502" id="Seg_7019" s="T501">dĭn</ta>
            <ta e="T503" id="Seg_7020" s="T502">šaː-bi-baʔ</ta>
            <ta e="T505" id="Seg_7021" s="T504">tože</ta>
            <ta e="T506" id="Seg_7022" s="T505">dĭn</ta>
            <ta e="T507" id="Seg_7023" s="T506">măn</ta>
            <ta e="T508" id="Seg_7024" s="T507">i-ge</ta>
            <ta e="T509" id="Seg_7025" s="T508">il</ta>
            <ta e="T510" id="Seg_7026" s="T509">tože</ta>
            <ta e="T512" id="Seg_7027" s="T511">dĭgəttə</ta>
            <ta e="T514" id="Seg_7028" s="T513">dĭgəttə</ta>
            <ta e="T517" id="Seg_7029" s="T516">dĭgəttə</ta>
            <ta e="T518" id="Seg_7030" s="T517">ertə-n</ta>
            <ta e="T519" id="Seg_7031" s="T518">uʔbdə-bi-baʔ</ta>
            <ta e="T520" id="Seg_7032" s="T519">i</ta>
            <ta e="T521" id="Seg_7033" s="T520">kam-bi-baʔ</ta>
            <ta e="T522" id="Seg_7034" s="T521">Krasnăjarskə-gən</ta>
            <ta e="T524" id="Seg_7035" s="T523">dĭn</ta>
            <ta e="T525" id="Seg_7036" s="T524">kam-bi-baʔ</ta>
            <ta e="T526" id="Seg_7037" s="T525">gijen</ta>
            <ta e="T527" id="Seg_7038" s="T526">nʼergö-laʔbə-ʔjə</ta>
            <ta e="T528" id="Seg_7039" s="T527">dĭn</ta>
            <ta e="T529" id="Seg_7040" s="T528">amno-bi-baʔ</ta>
            <ta e="T530" id="Seg_7041" s="T529">i</ta>
            <ta e="T1917" id="Seg_7042" s="T530">kal-la</ta>
            <ta e="T531" id="Seg_7043" s="T1917">dʼür-bi-baʔ</ta>
            <ta e="T533" id="Seg_7044" s="T532">Năvăsibirʼska-nə</ta>
            <ta e="T534" id="Seg_7045" s="T533">dĭn</ta>
            <ta e="T535" id="Seg_7046" s="T534">tože</ta>
            <ta e="T536" id="Seg_7047" s="T535">šaː-bi-baʔ</ta>
            <ta e="T538" id="Seg_7048" s="T537">dĭgəttə</ta>
            <ta e="T539" id="Seg_7049" s="T538">dĭn</ta>
            <ta e="T540" id="Seg_7050" s="T539">bazo</ta>
            <ta e="T541" id="Seg_7051" s="T540">amno-bi-baʔ</ta>
            <ta e="T542" id="Seg_7052" s="T541">i</ta>
            <ta e="T543" id="Seg_7053" s="T542">šo-bi-baʔ</ta>
            <ta e="T544" id="Seg_7054" s="T543">Măskva-nə</ta>
            <ta e="T547" id="Seg_7055" s="T546">dön</ta>
            <ta e="T548" id="Seg_7056" s="T547">dʼü-nə</ta>
            <ta e="T549" id="Seg_7057" s="T548">amno-bi-baʔ</ta>
            <ta e="T550" id="Seg_7058" s="T549">dĭgəttə</ta>
            <ta e="T551" id="Seg_7059" s="T550">amno-bi-baʔ</ta>
            <ta e="T552" id="Seg_7060" s="T551">amno-bi-baʔ</ta>
            <ta e="T553" id="Seg_7061" s="T552">mašina-nə</ta>
            <ta e="T554" id="Seg_7062" s="T553">kam-bi-baʔ</ta>
            <ta e="T555" id="Seg_7063" s="T554">Măskva-nə</ta>
            <ta e="T557" id="Seg_7064" s="T556">dĭn</ta>
            <ta e="T558" id="Seg_7065" s="T557">măna</ta>
            <ta e="T559" id="Seg_7066" s="T558">kür-bi</ta>
            <ta e="T561" id="Seg_7067" s="T560">tože</ta>
            <ta e="T562" id="Seg_7068" s="T561">il</ta>
            <ta e="T563" id="Seg_7069" s="T562">dĭn</ta>
            <ta e="T564" id="Seg_7070" s="T563">i-bi-ʔi</ta>
            <ta e="T566" id="Seg_7071" s="T565">dĭgəttə</ta>
            <ta e="T567" id="Seg_7072" s="T566">par-bi-baʔ</ta>
            <ta e="T569" id="Seg_7073" s="T568">bazo</ta>
            <ta e="T570" id="Seg_7074" s="T569">amno-l-bi-baʔ</ta>
            <ta e="T571" id="Seg_7075" s="T570">i</ta>
            <ta e="T572" id="Seg_7076" s="T571">dö</ta>
            <ta e="T573" id="Seg_7077" s="T572">dö</ta>
            <ta e="T574" id="Seg_7078" s="T573">tura-nə</ta>
            <ta e="T576" id="Seg_7079" s="T575">šo-bi-baʔ</ta>
            <ta e="T578" id="Seg_7080" s="T577">dĭgəttə</ta>
            <ta e="T579" id="Seg_7081" s="T578">döʔə</ta>
            <ta e="T581" id="Seg_7082" s="T580">kam-bi-baʔ</ta>
            <ta e="T582" id="Seg_7083" s="T581">Tartu</ta>
            <ta e="T584" id="Seg_7084" s="T583">dĭn</ta>
            <ta e="T585" id="Seg_7085" s="T584">nagur</ta>
            <ta e="T586" id="Seg_7086" s="T585">nedʼelʼa</ta>
            <ta e="T587" id="Seg_7087" s="T586">amno-bia-m</ta>
            <ta e="T589" id="Seg_7088" s="T588">bazo</ta>
            <ta e="T591" id="Seg_7089" s="T590">măna</ta>
            <ta e="T592" id="Seg_7090" s="T591">i-bi</ta>
            <ta e="T593" id="Seg_7091" s="T592">kam-bi-baʔ</ta>
            <ta e="T594" id="Seg_7092" s="T593">gijen</ta>
            <ta e="T595" id="Seg_7093" s="T594">iʔgö</ta>
            <ta e="T596" id="Seg_7094" s="T595">sĭrujanə</ta>
            <ta e="T598" id="Seg_7095" s="T597">dĭn</ta>
            <ta e="T599" id="Seg_7096" s="T598">iʔgö</ta>
            <ta e="T600" id="Seg_7097" s="T599">il</ta>
            <ta e="T601" id="Seg_7098" s="T600">i-bi-ʔi</ta>
            <ta e="T603" id="Seg_7099" s="T602">nʼemsə-ʔi</ta>
            <ta e="T604" id="Seg_7100" s="T603">i-bi-ʔi</ta>
            <ta e="T605" id="Seg_7101" s="T604">i</ta>
            <ta e="T607" id="Seg_7102" s="T606">i-bi-ʔi</ta>
            <ta e="T609" id="Seg_7103" s="T608">iʔgö</ta>
            <ta e="T610" id="Seg_7104" s="T609">dĭn</ta>
            <ta e="T611" id="Seg_7105" s="T610">kundʼo</ta>
            <ta e="T612" id="Seg_7106" s="T611">šide</ta>
            <ta e="T613" id="Seg_7107" s="T612">nüdʼi</ta>
            <ta e="T614" id="Seg_7108" s="T613">măn</ta>
            <ta e="T615" id="Seg_7109" s="T614">šaː-bia-m</ta>
            <ta e="T616" id="Seg_7110" s="T615">dĭn</ta>
            <ta e="T617" id="Seg_7111" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_7112" s="T617">bazo</ta>
            <ta e="T619" id="Seg_7113" s="T618">döbər</ta>
            <ta e="T620" id="Seg_7114" s="T619">deʔ-pi</ta>
            <ta e="T622" id="Seg_7115" s="T621">dĭʔ-nə</ta>
            <ta e="T623" id="Seg_7116" s="T622">nüke-nə</ta>
            <ta e="T625" id="Seg_7117" s="T624">i</ta>
            <ta e="T626" id="Seg_7118" s="T625">dĭn</ta>
            <ta e="T627" id="Seg_7119" s="T626">amno-laʔbə-m</ta>
            <ta e="T629" id="Seg_7120" s="T628">kabarləj</ta>
            <ta e="T950" id="Seg_7121" s="T949">kam-bi-baʔ</ta>
            <ta e="T1220" id="Seg_7122" s="T1219">dĭgəttə</ta>
            <ta e="T1221" id="Seg_7123" s="T1220">Arpit</ta>
            <ta e="T1222" id="Seg_7124" s="T1221">šo-bi</ta>
            <ta e="T1223" id="Seg_7125" s="T1222">măn</ta>
            <ta e="T1225" id="Seg_7126" s="T1224">ej</ta>
            <ta e="T1226" id="Seg_7127" s="T1225">öʔlu-bi</ta>
            <ta e="T1229" id="Seg_7128" s="T1228">a</ta>
            <ta e="T1231" id="Seg_7129" s="T1230">măn-də</ta>
            <ta e="T1232" id="Seg_7130" s="T1231">kan-a-ʔ</ta>
            <ta e="T1233" id="Seg_7131" s="T1232">kan-a-ʔ</ta>
            <ta e="T1235" id="Seg_7132" s="T1234">dĭgəttə</ta>
            <ta e="T1236" id="Seg_7133" s="T1235">măn</ta>
            <ta e="T1237" id="Seg_7134" s="T1236">kam-bia-m</ta>
            <ta e="T1238" id="Seg_7135" s="T1237">döbər</ta>
            <ta e="T1240" id="Seg_7136" s="T1239">а</ta>
            <ta e="T1245" id="Seg_7137" s="T1244">а</ta>
            <ta e="T1454" id="Seg_7138" s="T1453">а</ta>
            <ta e="T1475" id="Seg_7139" s="T1474">а</ta>
            <ta e="T1688" id="Seg_7140" s="T1687">а</ta>
            <ta e="T1724" id="Seg_7141" s="T1723">taldʼen</ta>
            <ta e="T1725" id="Seg_7142" s="T1724">miʔ</ta>
            <ta e="T1726" id="Seg_7143" s="T1725">jaʔtar-la</ta>
            <ta e="T1727" id="Seg_7144" s="T1726">kam-bi-baʔ</ta>
            <ta e="T1728" id="Seg_7145" s="T1727">onʼiʔ</ta>
            <ta e="T1729" id="Seg_7146" s="T1728">nüke-nə</ta>
            <ta e="T1731" id="Seg_7147" s="T1730">amno-bi-baʔ</ta>
            <ta e="T1732" id="Seg_7148" s="T1731">aftobus-tə</ta>
            <ta e="T1733" id="Seg_7149" s="T1732">dĭbər</ta>
            <ta e="T1734" id="Seg_7150" s="T1733">šo-bi-baʔ</ta>
            <ta e="T1735" id="Seg_7151" s="T1734">dĭn</ta>
            <ta e="T1736" id="Seg_7152" s="T1735">šo-bi-ʔi</ta>
            <ta e="T1737" id="Seg_7153" s="T1736">il</ta>
            <ta e="T1738" id="Seg_7154" s="T1737">dʼăbaktər-zittə</ta>
            <ta e="T1739" id="Seg_7155" s="T1738">măn</ta>
            <ta e="T1740" id="Seg_7156" s="T1739">šĭke-nə</ta>
            <ta e="T1741" id="Seg_7157" s="T1740">măn</ta>
            <ta e="T1742" id="Seg_7158" s="T1741">dĭ-zeŋ-də</ta>
            <ta e="T1743" id="Seg_7159" s="T1742">bar</ta>
            <ta e="T1744" id="Seg_7160" s="T1743">măm-bia-m</ta>
            <ta e="T1745" id="Seg_7161" s="T1744">măm-bia-m</ta>
            <ta e="T1747" id="Seg_7162" s="T1746">dĭgəttə</ta>
            <ta e="T1799" id="Seg_7163" s="T1798">BRK</ta>
            <ta e="T1849" id="Seg_7164" s="T1848">а</ta>
            <ta e="T1866" id="Seg_7165" s="T1865">măn</ta>
            <ta e="T1867" id="Seg_7166" s="T1866">oʔbdə-bia-m</ta>
            <ta e="T1868" id="Seg_7167" s="T1867">šo</ta>
            <ta e="T1869" id="Seg_7168" s="T1868">šonə-sʼtə</ta>
            <ta e="T1870" id="Seg_7169" s="T1869">döbər</ta>
            <ta e="T1871" id="Seg_7170" s="T1870">il</ta>
            <ta e="T1872" id="Seg_7171" s="T1871">măn-də-ʔi</ta>
            <ta e="T1873" id="Seg_7172" s="T1872">i-ʔ</ta>
            <ta e="T1874" id="Seg_7173" s="T1873">kan-a-ʔ</ta>
            <ta e="T1876" id="Seg_7174" s="T1875">kü-lal-lə-l</ta>
            <ta e="T1877" id="Seg_7175" s="T1876">dĭn</ta>
            <ta e="T1879" id="Seg_7176" s="T1878">măn</ta>
            <ta e="T1880" id="Seg_7177" s="T1879">măm-bia-m</ta>
            <ta e="T1881" id="Seg_7178" s="T1880">ej</ta>
            <ta e="T1882" id="Seg_7179" s="T1881">pĭm-nie-m</ta>
            <ta e="T1884" id="Seg_7180" s="T1883">dĭn</ta>
            <ta e="T1886" id="Seg_7181" s="T1885">măna</ta>
            <ta e="T1887" id="Seg_7182" s="T1886">dĭn</ta>
            <ta e="T1888" id="Seg_7183" s="T1887">tože</ta>
            <ta e="T1889" id="Seg_7184" s="T1888">dʼü-nə</ta>
            <ta e="T1890" id="Seg_7185" s="T1889">el-lə-ʔi</ta>
            <ta e="T1892" id="Seg_7186" s="T1891">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T113" id="Seg_7187" s="T112">a</ta>
            <ta e="T152" id="Seg_7188" s="T151">a</ta>
            <ta e="T321" id="Seg_7189" s="T320">măna</ta>
            <ta e="T322" id="Seg_7190" s="T321">pʼaŋdə-bi</ta>
            <ta e="T323" id="Seg_7191" s="T322">măn</ta>
            <ta e="T324" id="Seg_7192" s="T323">kaga-m</ta>
            <ta e="T326" id="Seg_7193" s="T325">teʔdə</ta>
            <ta e="T327" id="Seg_7194" s="T326">kö</ta>
            <ta e="T329" id="Seg_7195" s="T328">šo-ʔ</ta>
            <ta e="T330" id="Seg_7196" s="T329">döbər</ta>
            <ta e="T331" id="Seg_7197" s="T330">šo-ʔ</ta>
            <ta e="T332" id="Seg_7198" s="T331">döbər</ta>
            <ta e="T333" id="Seg_7199" s="T332">măn</ta>
            <ta e="T334" id="Seg_7200" s="T333">šində-ziʔ</ta>
            <ta e="T335" id="Seg_7201" s="T334">kan-lV-m</ta>
            <ta e="T337" id="Seg_7202" s="T336">ĭmbi=də</ta>
            <ta e="T338" id="Seg_7203" s="T337">ej</ta>
            <ta e="T339" id="Seg_7204" s="T338">tĭmne-m</ta>
            <ta e="T341" id="Seg_7205" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_7206" s="T341">pʼaŋdə-bi-m</ta>
            <ta e="T343" id="Seg_7207" s="T342">dĭ-Tə</ta>
            <ta e="T344" id="Seg_7208" s="T343">sazən</ta>
            <ta e="T346" id="Seg_7209" s="T345">măn-bi-m</ta>
            <ta e="T347" id="Seg_7210" s="T346">šo-lV-j</ta>
            <ta e="T350" id="Seg_7211" s="T349">măna</ta>
            <ta e="T351" id="Seg_7212" s="T350">dĭgəttə</ta>
            <ta e="T352" id="Seg_7213" s="T351">măn</ta>
            <ta e="T353" id="Seg_7214" s="T352">šo-lV-m</ta>
            <ta e="T355" id="Seg_7215" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_7216" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_7217" s="T356">măna</ta>
            <ta e="T358" id="Seg_7218" s="T357">pʼaŋdə-laʔbə</ta>
            <ta e="T359" id="Seg_7219" s="T358">šo-lV-m</ta>
            <ta e="T360" id="Seg_7220" s="T359">bos-də</ta>
            <ta e="T361" id="Seg_7221" s="T360">šiʔnʼileʔ</ta>
            <ta e="T363" id="Seg_7222" s="T362">dĭgəttə</ta>
            <ta e="T364" id="Seg_7223" s="T363">šo-bi</ta>
            <ta e="T365" id="Seg_7224" s="T364">miʔnʼibeʔ</ta>
            <ta e="T366" id="Seg_7225" s="T365">măn</ta>
            <ta e="T367" id="Seg_7226" s="T366">ej</ta>
            <ta e="T368" id="Seg_7227" s="T367">edəʔ-bi-m</ta>
            <ta e="T369" id="Seg_7228" s="T368">dĭ-m</ta>
            <ta e="T371" id="Seg_7229" s="T370">nu</ta>
            <ta e="T372" id="Seg_7230" s="T371">măn</ta>
            <ta e="T373" id="Seg_7231" s="T372">nʼeveskă-m</ta>
            <ta e="T374" id="Seg_7232" s="T373">dĭ-m</ta>
            <ta e="T375" id="Seg_7233" s="T374">det-laʔbə</ta>
            <ta e="T377" id="Seg_7234" s="T376">dö</ta>
            <ta e="T378" id="Seg_7235" s="T377">tăn</ta>
            <ta e="T379" id="Seg_7236" s="T378">kuza</ta>
            <ta e="T380" id="Seg_7237" s="T379">šo-bi</ta>
            <ta e="T383" id="Seg_7238" s="T382">măn</ta>
            <ta e="T384" id="Seg_7239" s="T383">nuʔmə-bi-m</ta>
            <ta e="T385" id="Seg_7240" s="T384">nʼiʔdə</ta>
            <ta e="T387" id="Seg_7241" s="T386">Oj</ta>
            <ta e="T388" id="Seg_7242" s="T387">büžü</ta>
            <ta e="T389" id="Seg_7243" s="T388">šo-bi-l</ta>
            <ta e="T390" id="Seg_7244" s="T389">a</ta>
            <ta e="T391" id="Seg_7245" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_7246" s="T391">dĭgəttə</ta>
            <ta e="T393" id="Seg_7247" s="T392">dĭ</ta>
            <ta e="T394" id="Seg_7248" s="T393">šo-bi</ta>
            <ta e="T395" id="Seg_7249" s="T394">amor-bi</ta>
            <ta e="T397" id="Seg_7250" s="T396">iʔbə-bi</ta>
            <ta e="T398" id="Seg_7251" s="T397">kunol-zittə</ta>
            <ta e="T400" id="Seg_7252" s="T399">dĭgəttə</ta>
            <ta e="T401" id="Seg_7253" s="T400">uʔbdə-bi</ta>
            <ta e="T402" id="Seg_7254" s="T401">kan-bi</ta>
            <ta e="T403" id="Seg_7255" s="T402">bü-Tə</ta>
            <ta e="T404" id="Seg_7256" s="T403">tʼermən-gəndə</ta>
            <ta e="T406" id="Seg_7257" s="T405">dĭn</ta>
            <ta e="T407" id="Seg_7258" s="T406">tože</ta>
            <ta e="T408" id="Seg_7259" s="T407">dĭrgit</ta>
            <ta e="T409" id="Seg_7260" s="T408">i-bi</ta>
            <ta e="T410" id="Seg_7261" s="T409">ku-bi</ta>
            <ta e="T412" id="Seg_7262" s="T411">tʼăbaktər-bi</ta>
            <ta e="T413" id="Seg_7263" s="T412">dĭn</ta>
            <ta e="T415" id="Seg_7264" s="T414">dĭgəttə</ta>
            <ta e="T416" id="Seg_7265" s="T415">šo-bi</ta>
            <ta e="T417" id="Seg_7266" s="T416">maʔ-gəndə</ta>
            <ta e="T419" id="Seg_7267" s="T418">dĭgəttə</ta>
            <ta e="T420" id="Seg_7268" s="T419">multʼa-Tə</ta>
            <ta e="T421" id="Seg_7269" s="T420">kan-bi</ta>
            <ta e="T423" id="Seg_7270" s="T422">măn</ta>
            <ta e="T424" id="Seg_7271" s="T423">dĭ-ziʔ</ta>
            <ta e="T425" id="Seg_7272" s="T424">mĭn-bi-m</ta>
            <ta e="T427" id="Seg_7273" s="T426">dĭn</ta>
            <ta e="T429" id="Seg_7274" s="T428">bü</ta>
            <ta e="T430" id="Seg_7275" s="T429">dĭ-Tə</ta>
            <ta e="T431" id="Seg_7276" s="T430">dʼibige</ta>
            <ta e="T432" id="Seg_7277" s="T431">šišəge</ta>
            <ta e="T433" id="Seg_7278" s="T432">bü</ta>
            <ta e="T434" id="Seg_7279" s="T433">sabən</ta>
            <ta e="T435" id="Seg_7280" s="T434">mĭ-bi-m</ta>
            <ta e="T437" id="Seg_7281" s="T436">dĭ</ta>
            <ta e="T438" id="Seg_7282" s="T437">bazə-jd-ə-bi</ta>
            <ta e="T439" id="Seg_7283" s="T438">dĭn</ta>
            <ta e="T440" id="Seg_7284" s="T439">dĭgəttə</ta>
            <ta e="T441" id="Seg_7285" s="T440">šo-bi</ta>
            <ta e="T442" id="Seg_7286" s="T441">maʔ-gənʼi</ta>
            <ta e="T444" id="Seg_7287" s="T443">măn-bi-m</ta>
            <ta e="T445" id="Seg_7288" s="T444">am-ə-ʔ</ta>
            <ta e="T446" id="Seg_7289" s="T445">măn</ta>
            <ta e="T447" id="Seg_7290" s="T446">dĭn</ta>
            <ta e="T448" id="Seg_7291" s="T447">am-bi-m</ta>
            <ta e="T449" id="Seg_7292" s="T448">tʼermən-Kən</ta>
            <ta e="T451" id="Seg_7293" s="T450">ej</ta>
            <ta e="T452" id="Seg_7294" s="T451">am-bi</ta>
            <ta e="T454" id="Seg_7295" s="T453">dĭgəttə</ta>
            <ta e="T456" id="Seg_7296" s="T455">kudaj-Tə</ta>
            <ta e="T457" id="Seg_7297" s="T456">numan üzə-bi-bAʔ</ta>
            <ta e="T459" id="Seg_7298" s="T458">dĭgəttə</ta>
            <ta e="T460" id="Seg_7299" s="T459">dĭ</ta>
            <ta e="T461" id="Seg_7300" s="T460">iʔbə-bi</ta>
            <ta e="T462" id="Seg_7301" s="T461">măn</ta>
            <ta e="T463" id="Seg_7302" s="T462">tože</ta>
            <ta e="T464" id="Seg_7303" s="T463">iʔbə-bi-m</ta>
            <ta e="T466" id="Seg_7304" s="T465">ertə-n</ta>
            <ta e="T467" id="Seg_7305" s="T466">uʔbdə-laʔbə-m</ta>
            <ta e="T468" id="Seg_7306" s="T467">munəj-jəʔ</ta>
            <ta e="T469" id="Seg_7307" s="T468">mĭndər-bi-m</ta>
            <ta e="T471" id="Seg_7308" s="T470">amor-bi-bAʔ</ta>
            <ta e="T472" id="Seg_7309" s="T471">kan</ta>
            <ta e="T473" id="Seg_7310" s="T472">kan-bi-bAʔ</ta>
            <ta e="T474" id="Seg_7311" s="T473">aftobus-Tə</ta>
            <ta e="T476" id="Seg_7312" s="T475">dĭn</ta>
            <ta e="T477" id="Seg_7313" s="T476">nu-bi-bAʔ</ta>
            <ta e="T478" id="Seg_7314" s="T477">dĭgəttə</ta>
            <ta e="T479" id="Seg_7315" s="T478">šo-bi</ta>
            <ta e="T480" id="Seg_7316" s="T479">aftobus</ta>
            <ta e="T482" id="Seg_7317" s="T481">miʔ</ta>
            <ta e="T485" id="Seg_7318" s="T484">amnə-bi-bAʔ</ta>
            <ta e="T1918" id="Seg_7319" s="T485">Kazan</ta>
            <ta e="T486" id="Seg_7320" s="T1918">tura-Tə</ta>
            <ta e="T487" id="Seg_7321" s="T486">kan-bi-bAʔ</ta>
            <ta e="T489" id="Seg_7322" s="T488">dĭn</ta>
            <ta e="T490" id="Seg_7323" s="T489">tože</ta>
            <ta e="T491" id="Seg_7324" s="T490">kudaj-Tə</ta>
            <ta e="T492" id="Seg_7325" s="T491">numan üzə-bi-jəʔ</ta>
            <ta e="T494" id="Seg_7326" s="T493">kamən</ta>
            <ta e="T495" id="Seg_7327" s="T494">sumna</ta>
            <ta e="T496" id="Seg_7328" s="T495">šo-bi</ta>
            <ta e="T497" id="Seg_7329" s="T496">dĭgəttə</ta>
            <ta e="T498" id="Seg_7330" s="T497">kan-bi-bAʔ</ta>
            <ta e="T499" id="Seg_7331" s="T498">Ujar-Tə</ta>
            <ta e="T501" id="Seg_7332" s="T500">dĭn</ta>
            <ta e="T502" id="Seg_7333" s="T501">dĭn</ta>
            <ta e="T503" id="Seg_7334" s="T502">šaː-bi-bAʔ</ta>
            <ta e="T505" id="Seg_7335" s="T504">tože</ta>
            <ta e="T506" id="Seg_7336" s="T505">dĭn</ta>
            <ta e="T507" id="Seg_7337" s="T506">măn</ta>
            <ta e="T508" id="Seg_7338" s="T507">i-gA</ta>
            <ta e="T509" id="Seg_7339" s="T508">il</ta>
            <ta e="T510" id="Seg_7340" s="T509">tože</ta>
            <ta e="T512" id="Seg_7341" s="T511">dĭgəttə</ta>
            <ta e="T514" id="Seg_7342" s="T513">dĭgəttə</ta>
            <ta e="T517" id="Seg_7343" s="T516">dĭgəttə</ta>
            <ta e="T518" id="Seg_7344" s="T517">ertə-n</ta>
            <ta e="T519" id="Seg_7345" s="T518">uʔbdə-bi-bAʔ</ta>
            <ta e="T520" id="Seg_7346" s="T519">i</ta>
            <ta e="T521" id="Seg_7347" s="T520">kan-bi-bAʔ</ta>
            <ta e="T522" id="Seg_7348" s="T521">Krasnojarskə-Kən</ta>
            <ta e="T524" id="Seg_7349" s="T523">dĭn</ta>
            <ta e="T525" id="Seg_7350" s="T524">kan-bi-bAʔ</ta>
            <ta e="T526" id="Seg_7351" s="T525">gijen</ta>
            <ta e="T527" id="Seg_7352" s="T526">nʼergö-laʔbə-jəʔ</ta>
            <ta e="T528" id="Seg_7353" s="T527">dĭn</ta>
            <ta e="T529" id="Seg_7354" s="T528">amnə-bi-bAʔ</ta>
            <ta e="T530" id="Seg_7355" s="T529">i</ta>
            <ta e="T1917" id="Seg_7356" s="T530">kan-lAʔ</ta>
            <ta e="T531" id="Seg_7357" s="T1917">tʼür-bi-bAʔ</ta>
            <ta e="T533" id="Seg_7358" s="T532">Năvăsibirʼska-Tə</ta>
            <ta e="T534" id="Seg_7359" s="T533">dĭn</ta>
            <ta e="T535" id="Seg_7360" s="T534">tože</ta>
            <ta e="T536" id="Seg_7361" s="T535">šaː-bi-bAʔ</ta>
            <ta e="T538" id="Seg_7362" s="T537">dĭgəttə</ta>
            <ta e="T539" id="Seg_7363" s="T538">dĭn</ta>
            <ta e="T540" id="Seg_7364" s="T539">bazoʔ</ta>
            <ta e="T541" id="Seg_7365" s="T540">amnə-bi-bAʔ</ta>
            <ta e="T542" id="Seg_7366" s="T541">i</ta>
            <ta e="T543" id="Seg_7367" s="T542">šo-bi-bAʔ</ta>
            <ta e="T544" id="Seg_7368" s="T543">Măskva-Tə</ta>
            <ta e="T547" id="Seg_7369" s="T546">dön</ta>
            <ta e="T548" id="Seg_7370" s="T547">tʼo-Tə</ta>
            <ta e="T549" id="Seg_7371" s="T548">amnə-bi-bAʔ</ta>
            <ta e="T550" id="Seg_7372" s="T549">dĭgəttə</ta>
            <ta e="T551" id="Seg_7373" s="T550">amnə-bi-bAʔ</ta>
            <ta e="T552" id="Seg_7374" s="T551">amnə-bi-bAʔ</ta>
            <ta e="T553" id="Seg_7375" s="T552">mašina-Tə</ta>
            <ta e="T554" id="Seg_7376" s="T553">kan-bi-bAʔ</ta>
            <ta e="T555" id="Seg_7377" s="T554">Măskva-Tə</ta>
            <ta e="T557" id="Seg_7378" s="T556">dĭn</ta>
            <ta e="T558" id="Seg_7379" s="T557">măna</ta>
            <ta e="T559" id="Seg_7380" s="T558">kür-bi</ta>
            <ta e="T561" id="Seg_7381" s="T560">tože</ta>
            <ta e="T562" id="Seg_7382" s="T561">il</ta>
            <ta e="T563" id="Seg_7383" s="T562">dĭn</ta>
            <ta e="T564" id="Seg_7384" s="T563">i-bi-jəʔ</ta>
            <ta e="T566" id="Seg_7385" s="T565">dĭgəttə</ta>
            <ta e="T567" id="Seg_7386" s="T566">par-bi-bAʔ</ta>
            <ta e="T569" id="Seg_7387" s="T568">bazoʔ</ta>
            <ta e="T570" id="Seg_7388" s="T569">amnə-l-bi-bAʔ</ta>
            <ta e="T571" id="Seg_7389" s="T570">i</ta>
            <ta e="T572" id="Seg_7390" s="T571">dö</ta>
            <ta e="T573" id="Seg_7391" s="T572">dö</ta>
            <ta e="T574" id="Seg_7392" s="T573">tura-Tə</ta>
            <ta e="T576" id="Seg_7393" s="T575">šo-bi-bAʔ</ta>
            <ta e="T578" id="Seg_7394" s="T577">dĭgəttə</ta>
            <ta e="T579" id="Seg_7395" s="T578">döʔə</ta>
            <ta e="T581" id="Seg_7396" s="T580">kan-bi-bAʔ</ta>
            <ta e="T582" id="Seg_7397" s="T581">Tartu</ta>
            <ta e="T584" id="Seg_7398" s="T583">dĭn</ta>
            <ta e="T585" id="Seg_7399" s="T584">nagur</ta>
            <ta e="T586" id="Seg_7400" s="T585">nedʼelʼa</ta>
            <ta e="T587" id="Seg_7401" s="T586">amnə-bi-m</ta>
            <ta e="T589" id="Seg_7402" s="T588">bazoʔ</ta>
            <ta e="T591" id="Seg_7403" s="T590">măna</ta>
            <ta e="T592" id="Seg_7404" s="T591">i-bi</ta>
            <ta e="T593" id="Seg_7405" s="T592">kan-bi-bAʔ</ta>
            <ta e="T594" id="Seg_7406" s="T593">gijen</ta>
            <ta e="T595" id="Seg_7407" s="T594">iʔgö</ta>
            <ta e="T596" id="Seg_7408" s="T595">sĭrujanə</ta>
            <ta e="T598" id="Seg_7409" s="T597">dĭn</ta>
            <ta e="T599" id="Seg_7410" s="T598">iʔgö</ta>
            <ta e="T600" id="Seg_7411" s="T599">il</ta>
            <ta e="T601" id="Seg_7412" s="T600">i-bi-jəʔ</ta>
            <ta e="T603" id="Seg_7413" s="T602">nʼemsə-jəʔ</ta>
            <ta e="T604" id="Seg_7414" s="T603">i-bi-jəʔ</ta>
            <ta e="T605" id="Seg_7415" s="T604">i</ta>
            <ta e="T607" id="Seg_7416" s="T606">i-bi-jəʔ</ta>
            <ta e="T609" id="Seg_7417" s="T608">iʔgö</ta>
            <ta e="T610" id="Seg_7418" s="T609">dĭn</ta>
            <ta e="T611" id="Seg_7419" s="T610">kondʼo</ta>
            <ta e="T612" id="Seg_7420" s="T611">šide</ta>
            <ta e="T613" id="Seg_7421" s="T612">nüdʼi</ta>
            <ta e="T614" id="Seg_7422" s="T613">măn</ta>
            <ta e="T615" id="Seg_7423" s="T614">šaː-bi-m</ta>
            <ta e="T616" id="Seg_7424" s="T615">dĭn</ta>
            <ta e="T617" id="Seg_7425" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_7426" s="T617">bazoʔ</ta>
            <ta e="T619" id="Seg_7427" s="T618">döbər</ta>
            <ta e="T620" id="Seg_7428" s="T619">det-bi</ta>
            <ta e="T622" id="Seg_7429" s="T621">dĭ-Tə</ta>
            <ta e="T623" id="Seg_7430" s="T622">nüke-Tə</ta>
            <ta e="T625" id="Seg_7431" s="T624">i</ta>
            <ta e="T626" id="Seg_7432" s="T625">dĭn</ta>
            <ta e="T627" id="Seg_7433" s="T626">amno-laʔbə-m</ta>
            <ta e="T629" id="Seg_7434" s="T628">kabarləj</ta>
            <ta e="T950" id="Seg_7435" s="T949">kan-bi-bAʔ</ta>
            <ta e="T1220" id="Seg_7436" s="T1219">dĭgəttə</ta>
            <ta e="T1221" id="Seg_7437" s="T1220">Arpit</ta>
            <ta e="T1222" id="Seg_7438" s="T1221">šo-bi</ta>
            <ta e="T1223" id="Seg_7439" s="T1222">măn</ta>
            <ta e="T1225" id="Seg_7440" s="T1224">ej</ta>
            <ta e="T1226" id="Seg_7441" s="T1225">öʔlu-bi</ta>
            <ta e="T1229" id="Seg_7442" s="T1228">a</ta>
            <ta e="T1231" id="Seg_7443" s="T1230">măn-ntə</ta>
            <ta e="T1232" id="Seg_7444" s="T1231">kan-ə-ʔ</ta>
            <ta e="T1233" id="Seg_7445" s="T1232">kan-ə-ʔ</ta>
            <ta e="T1235" id="Seg_7446" s="T1234">dĭgəttə</ta>
            <ta e="T1236" id="Seg_7447" s="T1235">măn</ta>
            <ta e="T1237" id="Seg_7448" s="T1236">kan-bi-m</ta>
            <ta e="T1238" id="Seg_7449" s="T1237">döbər</ta>
            <ta e="T1240" id="Seg_7450" s="T1239">a</ta>
            <ta e="T1245" id="Seg_7451" s="T1244">a</ta>
            <ta e="T1454" id="Seg_7452" s="T1453">a</ta>
            <ta e="T1475" id="Seg_7453" s="T1474">a</ta>
            <ta e="T1688" id="Seg_7454" s="T1687">a</ta>
            <ta e="T1724" id="Seg_7455" s="T1723">taldʼen</ta>
            <ta e="T1725" id="Seg_7456" s="T1724">miʔ</ta>
            <ta e="T1726" id="Seg_7457" s="T1725">jaʔtar-lAʔ</ta>
            <ta e="T1727" id="Seg_7458" s="T1726">kan-bi-bAʔ</ta>
            <ta e="T1728" id="Seg_7459" s="T1727">onʼiʔ</ta>
            <ta e="T1729" id="Seg_7460" s="T1728">nüke-Tə</ta>
            <ta e="T1731" id="Seg_7461" s="T1730">amnə-bi-bAʔ</ta>
            <ta e="T1732" id="Seg_7462" s="T1731">aftobus-Tə</ta>
            <ta e="T1733" id="Seg_7463" s="T1732">dĭbər</ta>
            <ta e="T1734" id="Seg_7464" s="T1733">šo-bi-bAʔ</ta>
            <ta e="T1735" id="Seg_7465" s="T1734">dĭn</ta>
            <ta e="T1736" id="Seg_7466" s="T1735">šo-bi-jəʔ</ta>
            <ta e="T1737" id="Seg_7467" s="T1736">il</ta>
            <ta e="T1738" id="Seg_7468" s="T1737">tʼăbaktər-zittə</ta>
            <ta e="T1739" id="Seg_7469" s="T1738">măn</ta>
            <ta e="T1740" id="Seg_7470" s="T1739">šĭkə-Tə</ta>
            <ta e="T1741" id="Seg_7471" s="T1740">măn</ta>
            <ta e="T1742" id="Seg_7472" s="T1741">dĭ-zAŋ-Tə</ta>
            <ta e="T1743" id="Seg_7473" s="T1742">bar</ta>
            <ta e="T1744" id="Seg_7474" s="T1743">măn-bi-m</ta>
            <ta e="T1745" id="Seg_7475" s="T1744">măn-bi-m</ta>
            <ta e="T1747" id="Seg_7476" s="T1746">dĭgəttə</ta>
            <ta e="T1849" id="Seg_7477" s="T1848">a</ta>
            <ta e="T1866" id="Seg_7478" s="T1865">măn</ta>
            <ta e="T1867" id="Seg_7479" s="T1866">oʔbdə-bi-m</ta>
            <ta e="T1868" id="Seg_7480" s="T1867">šo</ta>
            <ta e="T1869" id="Seg_7481" s="T1868">šonə-zittə</ta>
            <ta e="T1870" id="Seg_7482" s="T1869">döbər</ta>
            <ta e="T1871" id="Seg_7483" s="T1870">il</ta>
            <ta e="T1872" id="Seg_7484" s="T1871">măn-ntə-jəʔ</ta>
            <ta e="T1873" id="Seg_7485" s="T1872">e-ʔ</ta>
            <ta e="T1874" id="Seg_7486" s="T1873">kan-ə-ʔ</ta>
            <ta e="T1876" id="Seg_7487" s="T1875">kü-laːm-lV-l</ta>
            <ta e="T1877" id="Seg_7488" s="T1876">dĭn</ta>
            <ta e="T1879" id="Seg_7489" s="T1878">măn</ta>
            <ta e="T1880" id="Seg_7490" s="T1879">măn-bi-m</ta>
            <ta e="T1881" id="Seg_7491" s="T1880">ej</ta>
            <ta e="T1882" id="Seg_7492" s="T1881">pim-liA-m</ta>
            <ta e="T1884" id="Seg_7493" s="T1883">dĭn</ta>
            <ta e="T1886" id="Seg_7494" s="T1885">măna</ta>
            <ta e="T1887" id="Seg_7495" s="T1886">dĭn</ta>
            <ta e="T1888" id="Seg_7496" s="T1887">tože</ta>
            <ta e="T1889" id="Seg_7497" s="T1888">tʼo-Tə</ta>
            <ta e="T1890" id="Seg_7498" s="T1889">hen-lV-jəʔ</ta>
            <ta e="T1892" id="Seg_7499" s="T1891">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T113" id="Seg_7500" s="T112">and</ta>
            <ta e="T152" id="Seg_7501" s="T151">and</ta>
            <ta e="T321" id="Seg_7502" s="T320">I.LAT</ta>
            <ta e="T322" id="Seg_7503" s="T321">write-PST.[3SG]</ta>
            <ta e="T323" id="Seg_7504" s="T322">I.NOM</ta>
            <ta e="T324" id="Seg_7505" s="T323">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T326" id="Seg_7506" s="T325">four.[NOM.SG]</ta>
            <ta e="T327" id="Seg_7507" s="T326">winter.[NOM.SG]</ta>
            <ta e="T329" id="Seg_7508" s="T328">come-IMP.2SG</ta>
            <ta e="T330" id="Seg_7509" s="T329">here</ta>
            <ta e="T331" id="Seg_7510" s="T330">come-IMP.2SG</ta>
            <ta e="T332" id="Seg_7511" s="T331">here</ta>
            <ta e="T333" id="Seg_7512" s="T332">I.NOM</ta>
            <ta e="T334" id="Seg_7513" s="T333">who-INS</ta>
            <ta e="T335" id="Seg_7514" s="T334">go-FUT-1SG</ta>
            <ta e="T337" id="Seg_7515" s="T336">what.[NOM.SG]=INDEF</ta>
            <ta e="T338" id="Seg_7516" s="T337">NEG</ta>
            <ta e="T339" id="Seg_7517" s="T338">know-1SG</ta>
            <ta e="T341" id="Seg_7518" s="T340">then</ta>
            <ta e="T342" id="Seg_7519" s="T341">write-PST-1SG</ta>
            <ta e="T343" id="Seg_7520" s="T342">this-LAT</ta>
            <ta e="T344" id="Seg_7521" s="T343">paper.[NOM.SG]</ta>
            <ta e="T346" id="Seg_7522" s="T345">say-PST-1SG</ta>
            <ta e="T347" id="Seg_7523" s="T346">come-FUT-3SG</ta>
            <ta e="T350" id="Seg_7524" s="T349">I.LAT</ta>
            <ta e="T351" id="Seg_7525" s="T350">then</ta>
            <ta e="T352" id="Seg_7526" s="T351">I.NOM</ta>
            <ta e="T353" id="Seg_7527" s="T352">come-FUT-1SG</ta>
            <ta e="T355" id="Seg_7528" s="T354">then</ta>
            <ta e="T356" id="Seg_7529" s="T355">this.[NOM.SG]</ta>
            <ta e="T357" id="Seg_7530" s="T356">I.LAT</ta>
            <ta e="T358" id="Seg_7531" s="T357">write-DUR.[3SG]</ta>
            <ta e="T359" id="Seg_7532" s="T358">come-FUT-1SG</ta>
            <ta e="T360" id="Seg_7533" s="T359">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T361" id="Seg_7534" s="T360">you.PL.ACC</ta>
            <ta e="T363" id="Seg_7535" s="T362">then</ta>
            <ta e="T364" id="Seg_7536" s="T363">come-PST.[3SG]</ta>
            <ta e="T365" id="Seg_7537" s="T364">we.LAT</ta>
            <ta e="T366" id="Seg_7538" s="T365">I.NOM</ta>
            <ta e="T367" id="Seg_7539" s="T366">NEG</ta>
            <ta e="T368" id="Seg_7540" s="T367">wait-PST-1SG</ta>
            <ta e="T369" id="Seg_7541" s="T368">this-ACC</ta>
            <ta e="T371" id="Seg_7542" s="T370">well</ta>
            <ta e="T372" id="Seg_7543" s="T371">I.GEN</ta>
            <ta e="T373" id="Seg_7544" s="T372">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T374" id="Seg_7545" s="T373">this-ACC</ta>
            <ta e="T375" id="Seg_7546" s="T374">bring-DUR.[3SG]</ta>
            <ta e="T377" id="Seg_7547" s="T376">that.[NOM.SG]</ta>
            <ta e="T378" id="Seg_7548" s="T377">you.GEN</ta>
            <ta e="T379" id="Seg_7549" s="T378">man.[NOM.SG]</ta>
            <ta e="T380" id="Seg_7550" s="T379">come-PST.[3SG]</ta>
            <ta e="T383" id="Seg_7551" s="T382">I.NOM</ta>
            <ta e="T384" id="Seg_7552" s="T383">run-PST-1SG</ta>
            <ta e="T385" id="Seg_7553" s="T384">outwards</ta>
            <ta e="T387" id="Seg_7554" s="T386">oh</ta>
            <ta e="T388" id="Seg_7555" s="T387">soon</ta>
            <ta e="T389" id="Seg_7556" s="T388">come-PST-2SG</ta>
            <ta e="T390" id="Seg_7557" s="T389">and</ta>
            <ta e="T391" id="Seg_7558" s="T390">this.[NOM.SG]</ta>
            <ta e="T392" id="Seg_7559" s="T391">then</ta>
            <ta e="T393" id="Seg_7560" s="T392">this.[NOM.SG]</ta>
            <ta e="T394" id="Seg_7561" s="T393">come-PST.[3SG]</ta>
            <ta e="T395" id="Seg_7562" s="T394">eat-PST.[3SG]</ta>
            <ta e="T397" id="Seg_7563" s="T396">lie.down-PST.[3SG]</ta>
            <ta e="T398" id="Seg_7564" s="T397">sleep-INF.LAT</ta>
            <ta e="T400" id="Seg_7565" s="T399">then</ta>
            <ta e="T401" id="Seg_7566" s="T400">get.up-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7567" s="T401">go-PST.[3SG]</ta>
            <ta e="T403" id="Seg_7568" s="T402">water-LAT</ta>
            <ta e="T404" id="Seg_7569" s="T403">mill-LAT/LOC.3SG</ta>
            <ta e="T406" id="Seg_7570" s="T405">there</ta>
            <ta e="T407" id="Seg_7571" s="T406">also</ta>
            <ta e="T408" id="Seg_7572" s="T407">such.[NOM.SG]</ta>
            <ta e="T409" id="Seg_7573" s="T408">be-PST.[3SG]</ta>
            <ta e="T410" id="Seg_7574" s="T409">find-PST.[3SG]</ta>
            <ta e="T412" id="Seg_7575" s="T411">speak-PST.[3SG]</ta>
            <ta e="T413" id="Seg_7576" s="T412">there</ta>
            <ta e="T415" id="Seg_7577" s="T414">then</ta>
            <ta e="T416" id="Seg_7578" s="T415">come-PST.[3SG]</ta>
            <ta e="T417" id="Seg_7579" s="T416">tent-LAT/LOC.3SG</ta>
            <ta e="T419" id="Seg_7580" s="T418">then</ta>
            <ta e="T420" id="Seg_7581" s="T419">sauna-LAT</ta>
            <ta e="T421" id="Seg_7582" s="T420">go-PST.[3SG]</ta>
            <ta e="T423" id="Seg_7583" s="T422">I.NOM</ta>
            <ta e="T424" id="Seg_7584" s="T423">this-COM</ta>
            <ta e="T425" id="Seg_7585" s="T424">go-PST-1SG</ta>
            <ta e="T427" id="Seg_7586" s="T426">there</ta>
            <ta e="T429" id="Seg_7587" s="T428">water.[NOM.SG]</ta>
            <ta e="T430" id="Seg_7588" s="T429">this-LAT</ta>
            <ta e="T431" id="Seg_7589" s="T430">warm.[NOM.SG]</ta>
            <ta e="T432" id="Seg_7590" s="T431">cold.[NOM.SG]</ta>
            <ta e="T433" id="Seg_7591" s="T432">water.[NOM.SG]</ta>
            <ta e="T434" id="Seg_7592" s="T433">soap.[NOM.SG]</ta>
            <ta e="T435" id="Seg_7593" s="T434">give-PST-1SG</ta>
            <ta e="T437" id="Seg_7594" s="T436">this.[NOM.SG]</ta>
            <ta e="T438" id="Seg_7595" s="T437">wash-DRV-EP-PST.[3SG]</ta>
            <ta e="T439" id="Seg_7596" s="T438">there</ta>
            <ta e="T440" id="Seg_7597" s="T439">then</ta>
            <ta e="T441" id="Seg_7598" s="T440">come-PST.[3SG]</ta>
            <ta e="T442" id="Seg_7599" s="T441">tent-LAT/LOC.1SG</ta>
            <ta e="T444" id="Seg_7600" s="T443">say-PST-1SG</ta>
            <ta e="T445" id="Seg_7601" s="T444">eat-EP-IMP.2SG</ta>
            <ta e="T446" id="Seg_7602" s="T445">I.NOM</ta>
            <ta e="T447" id="Seg_7603" s="T446">there</ta>
            <ta e="T448" id="Seg_7604" s="T447">eat-PST-1SG</ta>
            <ta e="T449" id="Seg_7605" s="T448">mill-LOC</ta>
            <ta e="T451" id="Seg_7606" s="T450">NEG</ta>
            <ta e="T452" id="Seg_7607" s="T451">eat-PST.[3SG]</ta>
            <ta e="T454" id="Seg_7608" s="T453">then</ta>
            <ta e="T456" id="Seg_7609" s="T455">God-LAT</ta>
            <ta e="T457" id="Seg_7610" s="T456">pray-PST-1PL</ta>
            <ta e="T459" id="Seg_7611" s="T458">then</ta>
            <ta e="T460" id="Seg_7612" s="T459">this.[NOM.SG]</ta>
            <ta e="T461" id="Seg_7613" s="T460">lie.down-PST.[3SG]</ta>
            <ta e="T462" id="Seg_7614" s="T461">I.NOM</ta>
            <ta e="T463" id="Seg_7615" s="T462">also</ta>
            <ta e="T464" id="Seg_7616" s="T463">lie.down-PST-1SG</ta>
            <ta e="T466" id="Seg_7617" s="T465">morning-LOC.ADV</ta>
            <ta e="T467" id="Seg_7618" s="T466">get.up-DUR-1SG</ta>
            <ta e="T468" id="Seg_7619" s="T467">egg-PL</ta>
            <ta e="T469" id="Seg_7620" s="T468">boil-PST-1SG</ta>
            <ta e="T471" id="Seg_7621" s="T470">eat-PST-1PL</ta>
            <ta e="T472" id="Seg_7622" s="T471">go</ta>
            <ta e="T473" id="Seg_7623" s="T472">go-PST-1PL</ta>
            <ta e="T474" id="Seg_7624" s="T473">bus-LAT</ta>
            <ta e="T476" id="Seg_7625" s="T475">there</ta>
            <ta e="T477" id="Seg_7626" s="T476">stand-PST-1PL</ta>
            <ta e="T478" id="Seg_7627" s="T477">then</ta>
            <ta e="T479" id="Seg_7628" s="T478">come-PST.[3SG]</ta>
            <ta e="T480" id="Seg_7629" s="T479">bus.[NOM.SG]</ta>
            <ta e="T482" id="Seg_7630" s="T481">we.NOM</ta>
            <ta e="T485" id="Seg_7631" s="T484">sit.down-PST-1PL</ta>
            <ta e="T1918" id="Seg_7632" s="T485">Aginskoe</ta>
            <ta e="T486" id="Seg_7633" s="T1918">settlement-LAT</ta>
            <ta e="T487" id="Seg_7634" s="T486">go-PST-1PL</ta>
            <ta e="T489" id="Seg_7635" s="T488">there</ta>
            <ta e="T490" id="Seg_7636" s="T489">also</ta>
            <ta e="T491" id="Seg_7637" s="T490">God-LAT</ta>
            <ta e="T492" id="Seg_7638" s="T491">pray-PST-3PL</ta>
            <ta e="T494" id="Seg_7639" s="T493">when</ta>
            <ta e="T495" id="Seg_7640" s="T494">five.[NOM.SG]</ta>
            <ta e="T496" id="Seg_7641" s="T495">come-PST.[3SG]</ta>
            <ta e="T497" id="Seg_7642" s="T496">then</ta>
            <ta e="T498" id="Seg_7643" s="T497">go-PST-1PL</ta>
            <ta e="T499" id="Seg_7644" s="T498">Uyar-LAT</ta>
            <ta e="T501" id="Seg_7645" s="T500">there</ta>
            <ta e="T502" id="Seg_7646" s="T501">there</ta>
            <ta e="T503" id="Seg_7647" s="T502">spend.night-PST-1PL</ta>
            <ta e="T505" id="Seg_7648" s="T504">also</ta>
            <ta e="T506" id="Seg_7649" s="T505">there</ta>
            <ta e="T507" id="Seg_7650" s="T506">I.GEN</ta>
            <ta e="T508" id="Seg_7651" s="T507">be-PRS.[3SG]</ta>
            <ta e="T509" id="Seg_7652" s="T508">people.[NOM.SG]</ta>
            <ta e="T510" id="Seg_7653" s="T509">also</ta>
            <ta e="T512" id="Seg_7654" s="T511">then</ta>
            <ta e="T514" id="Seg_7655" s="T513">then</ta>
            <ta e="T517" id="Seg_7656" s="T516">then</ta>
            <ta e="T518" id="Seg_7657" s="T517">morning-LOC.ADV</ta>
            <ta e="T519" id="Seg_7658" s="T518">get.up-PST-1PL</ta>
            <ta e="T520" id="Seg_7659" s="T519">and</ta>
            <ta e="T521" id="Seg_7660" s="T520">go-PST-1PL</ta>
            <ta e="T522" id="Seg_7661" s="T521">Krasnoyarsk-LOC</ta>
            <ta e="T524" id="Seg_7662" s="T523">there</ta>
            <ta e="T525" id="Seg_7663" s="T524">go-PST-1PL</ta>
            <ta e="T526" id="Seg_7664" s="T525">where</ta>
            <ta e="T527" id="Seg_7665" s="T526">fly-DUR-3PL</ta>
            <ta e="T528" id="Seg_7666" s="T527">there</ta>
            <ta e="T529" id="Seg_7667" s="T528">sit.down-PST-1PL</ta>
            <ta e="T530" id="Seg_7668" s="T529">and</ta>
            <ta e="T1917" id="Seg_7669" s="T530">go-CVB</ta>
            <ta e="T531" id="Seg_7670" s="T1917">disappear-PST-1PL</ta>
            <ta e="T533" id="Seg_7671" s="T532">Novosibirsk-LAT</ta>
            <ta e="T534" id="Seg_7672" s="T533">there</ta>
            <ta e="T535" id="Seg_7673" s="T534">also</ta>
            <ta e="T536" id="Seg_7674" s="T535">spend.night-PST-1PL</ta>
            <ta e="T538" id="Seg_7675" s="T537">then</ta>
            <ta e="T539" id="Seg_7676" s="T538">there</ta>
            <ta e="T540" id="Seg_7677" s="T539">again</ta>
            <ta e="T541" id="Seg_7678" s="T540">sit.down-PST-1PL</ta>
            <ta e="T542" id="Seg_7679" s="T541">and</ta>
            <ta e="T543" id="Seg_7680" s="T542">come-PST-1PL</ta>
            <ta e="T544" id="Seg_7681" s="T543">Moscow-LAT</ta>
            <ta e="T547" id="Seg_7682" s="T546">here</ta>
            <ta e="T548" id="Seg_7683" s="T547">place-LAT</ta>
            <ta e="T549" id="Seg_7684" s="T548">sit.down-PST-1PL</ta>
            <ta e="T550" id="Seg_7685" s="T549">then</ta>
            <ta e="T551" id="Seg_7686" s="T550">sit.down-PST-1PL</ta>
            <ta e="T552" id="Seg_7687" s="T551">sit.down-PST-1PL</ta>
            <ta e="T553" id="Seg_7688" s="T552">machine-LAT</ta>
            <ta e="T554" id="Seg_7689" s="T553">go-PST-1PL</ta>
            <ta e="T555" id="Seg_7690" s="T554">Moscow-LAT</ta>
            <ta e="T557" id="Seg_7691" s="T556">there</ta>
            <ta e="T558" id="Seg_7692" s="T557">I.LAT</ta>
            <ta e="T559" id="Seg_7693" s="T558">take.picture-PST.[3SG]</ta>
            <ta e="T561" id="Seg_7694" s="T560">also</ta>
            <ta e="T562" id="Seg_7695" s="T561">people.[NOM.SG]</ta>
            <ta e="T563" id="Seg_7696" s="T562">there</ta>
            <ta e="T564" id="Seg_7697" s="T563">be-PST-3PL</ta>
            <ta e="T566" id="Seg_7698" s="T565">then</ta>
            <ta e="T567" id="Seg_7699" s="T566">return-PST-1PL</ta>
            <ta e="T569" id="Seg_7700" s="T568">again</ta>
            <ta e="T570" id="Seg_7701" s="T569">sit.down-FRQ-PST-1PL</ta>
            <ta e="T571" id="Seg_7702" s="T570">and</ta>
            <ta e="T572" id="Seg_7703" s="T571">that.[NOM.SG]</ta>
            <ta e="T573" id="Seg_7704" s="T572">that.[NOM.SG]</ta>
            <ta e="T574" id="Seg_7705" s="T573">settlement-LAT</ta>
            <ta e="T576" id="Seg_7706" s="T575">come-PST-1PL</ta>
            <ta e="T578" id="Seg_7707" s="T577">then</ta>
            <ta e="T579" id="Seg_7708" s="T578">hence</ta>
            <ta e="T581" id="Seg_7709" s="T580">go-PST-1PL</ta>
            <ta e="T582" id="Seg_7710" s="T581">Tartu.[NOM.SG]</ta>
            <ta e="T584" id="Seg_7711" s="T583">there</ta>
            <ta e="T585" id="Seg_7712" s="T584">three.[NOM.SG]</ta>
            <ta e="T586" id="Seg_7713" s="T585">week.[NOM.SG]</ta>
            <ta e="T587" id="Seg_7714" s="T586">sit-PST-1SG</ta>
            <ta e="T589" id="Seg_7715" s="T588">again</ta>
            <ta e="T591" id="Seg_7716" s="T590">I.LAT</ta>
            <ta e="T592" id="Seg_7717" s="T591">take-PST.[3SG]</ta>
            <ta e="T593" id="Seg_7718" s="T592">go-PST-1PL</ta>
            <ta e="T594" id="Seg_7719" s="T593">where</ta>
            <ta e="T595" id="Seg_7720" s="T594">many</ta>
            <ta e="T596" id="Seg_7721" s="T595">%%</ta>
            <ta e="T598" id="Seg_7722" s="T597">there</ta>
            <ta e="T599" id="Seg_7723" s="T598">many</ta>
            <ta e="T600" id="Seg_7724" s="T599">people.[NOM.SG]</ta>
            <ta e="T601" id="Seg_7725" s="T600">be-PST-3PL</ta>
            <ta e="T603" id="Seg_7726" s="T602">German.PL-PL</ta>
            <ta e="T604" id="Seg_7727" s="T603">be-PST-3PL</ta>
            <ta e="T605" id="Seg_7728" s="T604">and</ta>
            <ta e="T607" id="Seg_7729" s="T606">be-PST-3PL</ta>
            <ta e="T609" id="Seg_7730" s="T608">many</ta>
            <ta e="T610" id="Seg_7731" s="T609">there</ta>
            <ta e="T611" id="Seg_7732" s="T610">long.time</ta>
            <ta e="T612" id="Seg_7733" s="T611">two.[NOM.SG]</ta>
            <ta e="T613" id="Seg_7734" s="T612">evening.[NOM.SG]</ta>
            <ta e="T614" id="Seg_7735" s="T613">I.NOM</ta>
            <ta e="T615" id="Seg_7736" s="T614">spend.night-PST-1SG</ta>
            <ta e="T616" id="Seg_7737" s="T615">there</ta>
            <ta e="T617" id="Seg_7738" s="T616">then</ta>
            <ta e="T618" id="Seg_7739" s="T617">again</ta>
            <ta e="T619" id="Seg_7740" s="T618">here</ta>
            <ta e="T620" id="Seg_7741" s="T619">bring-PST.[3SG]</ta>
            <ta e="T622" id="Seg_7742" s="T621">this-LAT</ta>
            <ta e="T623" id="Seg_7743" s="T622">woman-LAT</ta>
            <ta e="T625" id="Seg_7744" s="T624">and</ta>
            <ta e="T626" id="Seg_7745" s="T625">there</ta>
            <ta e="T627" id="Seg_7746" s="T626">live-DUR-1SG</ta>
            <ta e="T629" id="Seg_7747" s="T628">enough</ta>
            <ta e="T950" id="Seg_7748" s="T949">go-PST-1PL</ta>
            <ta e="T1220" id="Seg_7749" s="T1219">then</ta>
            <ta e="T1221" id="Seg_7750" s="T1220">Arpit.[NOM.SG]</ta>
            <ta e="T1222" id="Seg_7751" s="T1221">come-PST.[3SG]</ta>
            <ta e="T1223" id="Seg_7752" s="T1222">I.GEN</ta>
            <ta e="T1225" id="Seg_7753" s="T1224">NEG</ta>
            <ta e="T1226" id="Seg_7754" s="T1225">send-PST.[3SG]</ta>
            <ta e="T1229" id="Seg_7755" s="T1228">and</ta>
            <ta e="T1231" id="Seg_7756" s="T1230">say-IPFVZ.[3SG]</ta>
            <ta e="T1232" id="Seg_7757" s="T1231">go-EP-IMP.2SG</ta>
            <ta e="T1233" id="Seg_7758" s="T1232">go-EP-IMP.2SG</ta>
            <ta e="T1235" id="Seg_7759" s="T1234">then</ta>
            <ta e="T1236" id="Seg_7760" s="T1235">I.NOM</ta>
            <ta e="T1237" id="Seg_7761" s="T1236">go-PST-1SG</ta>
            <ta e="T1238" id="Seg_7762" s="T1237">here</ta>
            <ta e="T1240" id="Seg_7763" s="T1239">and</ta>
            <ta e="T1245" id="Seg_7764" s="T1244">and</ta>
            <ta e="T1454" id="Seg_7765" s="T1453">and</ta>
            <ta e="T1475" id="Seg_7766" s="T1474">and</ta>
            <ta e="T1688" id="Seg_7767" s="T1687">and</ta>
            <ta e="T1724" id="Seg_7768" s="T1723">yesterday</ta>
            <ta e="T1725" id="Seg_7769" s="T1724">we.NOM</ta>
            <ta e="T1726" id="Seg_7770" s="T1725">visit-CVB</ta>
            <ta e="T1727" id="Seg_7771" s="T1726">go-PST-1PL</ta>
            <ta e="T1728" id="Seg_7772" s="T1727">one.[NOM.SG]</ta>
            <ta e="T1729" id="Seg_7773" s="T1728">woman-LAT</ta>
            <ta e="T1731" id="Seg_7774" s="T1730">sit.down-PST-1PL</ta>
            <ta e="T1732" id="Seg_7775" s="T1731">bus-LAT</ta>
            <ta e="T1733" id="Seg_7776" s="T1732">there</ta>
            <ta e="T1734" id="Seg_7777" s="T1733">come-PST-1PL</ta>
            <ta e="T1735" id="Seg_7778" s="T1734">there</ta>
            <ta e="T1736" id="Seg_7779" s="T1735">come-PST-3PL</ta>
            <ta e="T1737" id="Seg_7780" s="T1736">people.[NOM.SG]</ta>
            <ta e="T1738" id="Seg_7781" s="T1737">speak-INF.LAT</ta>
            <ta e="T1739" id="Seg_7782" s="T1738">I.GEN</ta>
            <ta e="T1740" id="Seg_7783" s="T1739">language-LAT</ta>
            <ta e="T1741" id="Seg_7784" s="T1740">I.NOM</ta>
            <ta e="T1742" id="Seg_7785" s="T1741">this-PL-LAT</ta>
            <ta e="T1743" id="Seg_7786" s="T1742">PTCL</ta>
            <ta e="T1744" id="Seg_7787" s="T1743">say-PST-1SG</ta>
            <ta e="T1745" id="Seg_7788" s="T1744">say-PST-1SG</ta>
            <ta e="T1747" id="Seg_7789" s="T1746">then</ta>
            <ta e="T1849" id="Seg_7790" s="T1848">and</ta>
            <ta e="T1866" id="Seg_7791" s="T1865">I.NOM</ta>
            <ta e="T1867" id="Seg_7792" s="T1866">collect-PST-1SG</ta>
            <ta e="T1868" id="Seg_7793" s="T1867">come</ta>
            <ta e="T1869" id="Seg_7794" s="T1868">come-INF.LAT</ta>
            <ta e="T1870" id="Seg_7795" s="T1869">here</ta>
            <ta e="T1871" id="Seg_7796" s="T1870">people.[NOM.SG]</ta>
            <ta e="T1872" id="Seg_7797" s="T1871">say-IPFVZ-3PL</ta>
            <ta e="T1873" id="Seg_7798" s="T1872">NEG.AUX-IMP.2SG</ta>
            <ta e="T1874" id="Seg_7799" s="T1873">go-EP-CNG</ta>
            <ta e="T1876" id="Seg_7800" s="T1875">die-RES-FUT-2SG</ta>
            <ta e="T1877" id="Seg_7801" s="T1876">there</ta>
            <ta e="T1879" id="Seg_7802" s="T1878">I.NOM</ta>
            <ta e="T1880" id="Seg_7803" s="T1879">say-PST-1SG</ta>
            <ta e="T1881" id="Seg_7804" s="T1880">NEG</ta>
            <ta e="T1882" id="Seg_7805" s="T1881">fear-PRS-1SG</ta>
            <ta e="T1884" id="Seg_7806" s="T1883">there</ta>
            <ta e="T1886" id="Seg_7807" s="T1885">I.ACC</ta>
            <ta e="T1887" id="Seg_7808" s="T1886">there</ta>
            <ta e="T1888" id="Seg_7809" s="T1887">also</ta>
            <ta e="T1889" id="Seg_7810" s="T1888">place-LAT</ta>
            <ta e="T1890" id="Seg_7811" s="T1889">put-FUT-3PL</ta>
            <ta e="T1892" id="Seg_7812" s="T1891">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T113" id="Seg_7813" s="T112">а</ta>
            <ta e="T152" id="Seg_7814" s="T151">а</ta>
            <ta e="T321" id="Seg_7815" s="T320">я.LAT</ta>
            <ta e="T322" id="Seg_7816" s="T321">писать-PST.[3SG]</ta>
            <ta e="T323" id="Seg_7817" s="T322">я.NOM</ta>
            <ta e="T324" id="Seg_7818" s="T323">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T326" id="Seg_7819" s="T325">четыре.[NOM.SG]</ta>
            <ta e="T327" id="Seg_7820" s="T326">зима.[NOM.SG]</ta>
            <ta e="T329" id="Seg_7821" s="T328">прийти-IMP.2SG</ta>
            <ta e="T330" id="Seg_7822" s="T329">здесь</ta>
            <ta e="T331" id="Seg_7823" s="T330">прийти-IMP.2SG</ta>
            <ta e="T332" id="Seg_7824" s="T331">здесь</ta>
            <ta e="T333" id="Seg_7825" s="T332">я.NOM</ta>
            <ta e="T334" id="Seg_7826" s="T333">кто-INS</ta>
            <ta e="T335" id="Seg_7827" s="T334">пойти-FUT-1SG</ta>
            <ta e="T337" id="Seg_7828" s="T336">что.[NOM.SG]=INDEF</ta>
            <ta e="T338" id="Seg_7829" s="T337">NEG</ta>
            <ta e="T339" id="Seg_7830" s="T338">знать-1SG</ta>
            <ta e="T341" id="Seg_7831" s="T340">тогда</ta>
            <ta e="T342" id="Seg_7832" s="T341">писать-PST-1SG</ta>
            <ta e="T343" id="Seg_7833" s="T342">этот-LAT</ta>
            <ta e="T344" id="Seg_7834" s="T343">бумага.[NOM.SG]</ta>
            <ta e="T346" id="Seg_7835" s="T345">сказать-PST-1SG</ta>
            <ta e="T347" id="Seg_7836" s="T346">прийти-FUT-3SG</ta>
            <ta e="T350" id="Seg_7837" s="T349">я.LAT</ta>
            <ta e="T351" id="Seg_7838" s="T350">тогда</ta>
            <ta e="T352" id="Seg_7839" s="T351">я.NOM</ta>
            <ta e="T353" id="Seg_7840" s="T352">прийти-FUT-1SG</ta>
            <ta e="T355" id="Seg_7841" s="T354">тогда</ta>
            <ta e="T356" id="Seg_7842" s="T355">этот.[NOM.SG]</ta>
            <ta e="T357" id="Seg_7843" s="T356">я.LAT</ta>
            <ta e="T358" id="Seg_7844" s="T357">писать-DUR.[3SG]</ta>
            <ta e="T359" id="Seg_7845" s="T358">прийти-FUT-1SG</ta>
            <ta e="T360" id="Seg_7846" s="T359">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T361" id="Seg_7847" s="T360">вы.ACC</ta>
            <ta e="T363" id="Seg_7848" s="T362">тогда</ta>
            <ta e="T364" id="Seg_7849" s="T363">прийти-PST.[3SG]</ta>
            <ta e="T365" id="Seg_7850" s="T364">мы.LAT</ta>
            <ta e="T366" id="Seg_7851" s="T365">я.NOM</ta>
            <ta e="T367" id="Seg_7852" s="T366">NEG</ta>
            <ta e="T368" id="Seg_7853" s="T367">ждать-PST-1SG</ta>
            <ta e="T369" id="Seg_7854" s="T368">этот-ACC</ta>
            <ta e="T371" id="Seg_7855" s="T370">ну</ta>
            <ta e="T372" id="Seg_7856" s="T371">я.GEN</ta>
            <ta e="T373" id="Seg_7857" s="T372">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T374" id="Seg_7858" s="T373">этот-ACC</ta>
            <ta e="T375" id="Seg_7859" s="T374">принести-DUR.[3SG]</ta>
            <ta e="T377" id="Seg_7860" s="T376">тот.[NOM.SG]</ta>
            <ta e="T378" id="Seg_7861" s="T377">ты.GEN</ta>
            <ta e="T379" id="Seg_7862" s="T378">мужчина.[NOM.SG]</ta>
            <ta e="T380" id="Seg_7863" s="T379">прийти-PST.[3SG]</ta>
            <ta e="T383" id="Seg_7864" s="T382">я.NOM</ta>
            <ta e="T384" id="Seg_7865" s="T383">бежать-PST-1SG</ta>
            <ta e="T385" id="Seg_7866" s="T384">наружу</ta>
            <ta e="T387" id="Seg_7867" s="T386">ой</ta>
            <ta e="T388" id="Seg_7868" s="T387">скоро</ta>
            <ta e="T389" id="Seg_7869" s="T388">прийти-PST-2SG</ta>
            <ta e="T390" id="Seg_7870" s="T389">а</ta>
            <ta e="T391" id="Seg_7871" s="T390">этот.[NOM.SG]</ta>
            <ta e="T392" id="Seg_7872" s="T391">тогда</ta>
            <ta e="T393" id="Seg_7873" s="T392">этот.[NOM.SG]</ta>
            <ta e="T394" id="Seg_7874" s="T393">прийти-PST.[3SG]</ta>
            <ta e="T395" id="Seg_7875" s="T394">есть-PST.[3SG]</ta>
            <ta e="T397" id="Seg_7876" s="T396">ложиться-PST.[3SG]</ta>
            <ta e="T398" id="Seg_7877" s="T397">спать-INF.LAT</ta>
            <ta e="T400" id="Seg_7878" s="T399">тогда</ta>
            <ta e="T401" id="Seg_7879" s="T400">встать-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7880" s="T401">пойти-PST.[3SG]</ta>
            <ta e="T403" id="Seg_7881" s="T402">вода-LAT</ta>
            <ta e="T404" id="Seg_7882" s="T403">мельница-LAT/LOC.3SG</ta>
            <ta e="T406" id="Seg_7883" s="T405">там</ta>
            <ta e="T407" id="Seg_7884" s="T406">тоже</ta>
            <ta e="T408" id="Seg_7885" s="T407">такой.[NOM.SG]</ta>
            <ta e="T409" id="Seg_7886" s="T408">быть-PST.[3SG]</ta>
            <ta e="T410" id="Seg_7887" s="T409">видеть-PST.[3SG]</ta>
            <ta e="T412" id="Seg_7888" s="T411">говорить-PST.[3SG]</ta>
            <ta e="T413" id="Seg_7889" s="T412">там</ta>
            <ta e="T415" id="Seg_7890" s="T414">тогда</ta>
            <ta e="T416" id="Seg_7891" s="T415">прийти-PST.[3SG]</ta>
            <ta e="T417" id="Seg_7892" s="T416">чум-LAT/LOC.3SG</ta>
            <ta e="T419" id="Seg_7893" s="T418">тогда</ta>
            <ta e="T420" id="Seg_7894" s="T419">баня-LAT</ta>
            <ta e="T421" id="Seg_7895" s="T420">пойти-PST.[3SG]</ta>
            <ta e="T423" id="Seg_7896" s="T422">я.NOM</ta>
            <ta e="T424" id="Seg_7897" s="T423">этот-COM</ta>
            <ta e="T425" id="Seg_7898" s="T424">идти-PST-1SG</ta>
            <ta e="T427" id="Seg_7899" s="T426">там</ta>
            <ta e="T429" id="Seg_7900" s="T428">вода.[NOM.SG]</ta>
            <ta e="T430" id="Seg_7901" s="T429">этот-LAT</ta>
            <ta e="T431" id="Seg_7902" s="T430">теплый.[NOM.SG]</ta>
            <ta e="T432" id="Seg_7903" s="T431">холодный.[NOM.SG]</ta>
            <ta e="T433" id="Seg_7904" s="T432">вода.[NOM.SG]</ta>
            <ta e="T434" id="Seg_7905" s="T433">мыло.[NOM.SG]</ta>
            <ta e="T435" id="Seg_7906" s="T434">дать-PST-1SG</ta>
            <ta e="T437" id="Seg_7907" s="T436">этот.[NOM.SG]</ta>
            <ta e="T438" id="Seg_7908" s="T437">мыть-DRV-EP-PST.[3SG]</ta>
            <ta e="T439" id="Seg_7909" s="T438">там</ta>
            <ta e="T440" id="Seg_7910" s="T439">тогда</ta>
            <ta e="T441" id="Seg_7911" s="T440">прийти-PST.[3SG]</ta>
            <ta e="T442" id="Seg_7912" s="T441">чум-LAT/LOC.1SG</ta>
            <ta e="T444" id="Seg_7913" s="T443">сказать-PST-1SG</ta>
            <ta e="T445" id="Seg_7914" s="T444">съесть-EP-IMP.2SG</ta>
            <ta e="T446" id="Seg_7915" s="T445">я.NOM</ta>
            <ta e="T447" id="Seg_7916" s="T446">там</ta>
            <ta e="T448" id="Seg_7917" s="T447">съесть-PST-1SG</ta>
            <ta e="T449" id="Seg_7918" s="T448">мельница-LOC</ta>
            <ta e="T451" id="Seg_7919" s="T450">NEG</ta>
            <ta e="T452" id="Seg_7920" s="T451">съесть-PST.[3SG]</ta>
            <ta e="T454" id="Seg_7921" s="T453">тогда</ta>
            <ta e="T456" id="Seg_7922" s="T455">бог-LAT</ta>
            <ta e="T457" id="Seg_7923" s="T456">молиться-PST-1PL</ta>
            <ta e="T459" id="Seg_7924" s="T458">тогда</ta>
            <ta e="T460" id="Seg_7925" s="T459">этот.[NOM.SG]</ta>
            <ta e="T461" id="Seg_7926" s="T460">ложиться-PST.[3SG]</ta>
            <ta e="T462" id="Seg_7927" s="T461">я.NOM</ta>
            <ta e="T463" id="Seg_7928" s="T462">тоже</ta>
            <ta e="T464" id="Seg_7929" s="T463">ложиться-PST-1SG</ta>
            <ta e="T466" id="Seg_7930" s="T465">утро-LOC.ADV</ta>
            <ta e="T467" id="Seg_7931" s="T466">встать-DUR-1SG</ta>
            <ta e="T468" id="Seg_7932" s="T467">яйцо-PL</ta>
            <ta e="T469" id="Seg_7933" s="T468">варить-PST-1SG</ta>
            <ta e="T471" id="Seg_7934" s="T470">есть-PST-1PL</ta>
            <ta e="T472" id="Seg_7935" s="T471">пойти</ta>
            <ta e="T473" id="Seg_7936" s="T472">пойти-PST-1PL</ta>
            <ta e="T474" id="Seg_7937" s="T473">автобус-LAT</ta>
            <ta e="T476" id="Seg_7938" s="T475">там</ta>
            <ta e="T477" id="Seg_7939" s="T476">стоять-PST-1PL</ta>
            <ta e="T478" id="Seg_7940" s="T477">тогда</ta>
            <ta e="T479" id="Seg_7941" s="T478">прийти-PST.[3SG]</ta>
            <ta e="T480" id="Seg_7942" s="T479">автобус.[NOM.SG]</ta>
            <ta e="T482" id="Seg_7943" s="T481">мы.NOM</ta>
            <ta e="T485" id="Seg_7944" s="T484">сесть-PST-1PL</ta>
            <ta e="T1918" id="Seg_7945" s="T485">Агинское</ta>
            <ta e="T486" id="Seg_7946" s="T1918">поселение-LAT</ta>
            <ta e="T487" id="Seg_7947" s="T486">пойти-PST-1PL</ta>
            <ta e="T489" id="Seg_7948" s="T488">там</ta>
            <ta e="T490" id="Seg_7949" s="T489">тоже</ta>
            <ta e="T491" id="Seg_7950" s="T490">бог-LAT</ta>
            <ta e="T492" id="Seg_7951" s="T491">молиться-PST-3PL</ta>
            <ta e="T494" id="Seg_7952" s="T493">когда</ta>
            <ta e="T495" id="Seg_7953" s="T494">пять.[NOM.SG]</ta>
            <ta e="T496" id="Seg_7954" s="T495">прийти-PST.[3SG]</ta>
            <ta e="T497" id="Seg_7955" s="T496">тогда</ta>
            <ta e="T498" id="Seg_7956" s="T497">пойти-PST-1PL</ta>
            <ta e="T499" id="Seg_7957" s="T498">Уяр-LAT</ta>
            <ta e="T501" id="Seg_7958" s="T500">там</ta>
            <ta e="T502" id="Seg_7959" s="T501">там</ta>
            <ta e="T503" id="Seg_7960" s="T502">ночевать-PST-1PL</ta>
            <ta e="T505" id="Seg_7961" s="T504">тоже</ta>
            <ta e="T506" id="Seg_7962" s="T505">там</ta>
            <ta e="T507" id="Seg_7963" s="T506">я.GEN</ta>
            <ta e="T508" id="Seg_7964" s="T507">быть-PRS.[3SG]</ta>
            <ta e="T509" id="Seg_7965" s="T508">люди.[NOM.SG]</ta>
            <ta e="T510" id="Seg_7966" s="T509">тоже</ta>
            <ta e="T512" id="Seg_7967" s="T511">тогда</ta>
            <ta e="T514" id="Seg_7968" s="T513">тогда</ta>
            <ta e="T517" id="Seg_7969" s="T516">тогда</ta>
            <ta e="T518" id="Seg_7970" s="T517">утро-LOC.ADV</ta>
            <ta e="T519" id="Seg_7971" s="T518">встать-PST-1PL</ta>
            <ta e="T520" id="Seg_7972" s="T519">и</ta>
            <ta e="T521" id="Seg_7973" s="T520">пойти-PST-1PL</ta>
            <ta e="T522" id="Seg_7974" s="T521">Красноярск-LOC</ta>
            <ta e="T524" id="Seg_7975" s="T523">там</ta>
            <ta e="T525" id="Seg_7976" s="T524">пойти-PST-1PL</ta>
            <ta e="T526" id="Seg_7977" s="T525">где</ta>
            <ta e="T527" id="Seg_7978" s="T526">лететь-DUR-3PL</ta>
            <ta e="T528" id="Seg_7979" s="T527">там</ta>
            <ta e="T529" id="Seg_7980" s="T528">сесть-PST-1PL</ta>
            <ta e="T530" id="Seg_7981" s="T529">и</ta>
            <ta e="T1917" id="Seg_7982" s="T530">пойти-CVB</ta>
            <ta e="T531" id="Seg_7983" s="T1917">исчезнуть-PST-1PL</ta>
            <ta e="T533" id="Seg_7984" s="T532">Новосибирск-LAT</ta>
            <ta e="T534" id="Seg_7985" s="T533">там</ta>
            <ta e="T535" id="Seg_7986" s="T534">тоже</ta>
            <ta e="T536" id="Seg_7987" s="T535">ночевать-PST-1PL</ta>
            <ta e="T538" id="Seg_7988" s="T537">тогда</ta>
            <ta e="T539" id="Seg_7989" s="T538">там</ta>
            <ta e="T540" id="Seg_7990" s="T539">опять</ta>
            <ta e="T541" id="Seg_7991" s="T540">сесть-PST-1PL</ta>
            <ta e="T542" id="Seg_7992" s="T541">и</ta>
            <ta e="T543" id="Seg_7993" s="T542">прийти-PST-1PL</ta>
            <ta e="T544" id="Seg_7994" s="T543">Москва-LAT</ta>
            <ta e="T547" id="Seg_7995" s="T546">здесь</ta>
            <ta e="T548" id="Seg_7996" s="T547">место-LAT</ta>
            <ta e="T549" id="Seg_7997" s="T548">сесть-PST-1PL</ta>
            <ta e="T550" id="Seg_7998" s="T549">тогда</ta>
            <ta e="T551" id="Seg_7999" s="T550">сесть-PST-1PL</ta>
            <ta e="T552" id="Seg_8000" s="T551">сесть-PST-1PL</ta>
            <ta e="T553" id="Seg_8001" s="T552">машина-LAT</ta>
            <ta e="T554" id="Seg_8002" s="T553">пойти-PST-1PL</ta>
            <ta e="T555" id="Seg_8003" s="T554">Москва-LAT</ta>
            <ta e="T557" id="Seg_8004" s="T556">там</ta>
            <ta e="T558" id="Seg_8005" s="T557">я.LAT</ta>
            <ta e="T559" id="Seg_8006" s="T558">фотографировать-PST.[3SG]</ta>
            <ta e="T561" id="Seg_8007" s="T560">тоже</ta>
            <ta e="T562" id="Seg_8008" s="T561">люди.[NOM.SG]</ta>
            <ta e="T563" id="Seg_8009" s="T562">там</ta>
            <ta e="T564" id="Seg_8010" s="T563">быть-PST-3PL</ta>
            <ta e="T566" id="Seg_8011" s="T565">тогда</ta>
            <ta e="T567" id="Seg_8012" s="T566">вернуться-PST-1PL</ta>
            <ta e="T569" id="Seg_8013" s="T568">опять</ta>
            <ta e="T570" id="Seg_8014" s="T569">сесть-FRQ-PST-1PL</ta>
            <ta e="T571" id="Seg_8015" s="T570">и</ta>
            <ta e="T572" id="Seg_8016" s="T571">тот.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8017" s="T572">тот.[NOM.SG]</ta>
            <ta e="T574" id="Seg_8018" s="T573">поселение-LAT</ta>
            <ta e="T576" id="Seg_8019" s="T575">прийти-PST-1PL</ta>
            <ta e="T578" id="Seg_8020" s="T577">тогда</ta>
            <ta e="T579" id="Seg_8021" s="T578">отсюда</ta>
            <ta e="T581" id="Seg_8022" s="T580">пойти-PST-1PL</ta>
            <ta e="T582" id="Seg_8023" s="T581">Тарту.[NOM.SG]</ta>
            <ta e="T584" id="Seg_8024" s="T583">там</ta>
            <ta e="T585" id="Seg_8025" s="T584">три.[NOM.SG]</ta>
            <ta e="T586" id="Seg_8026" s="T585">неделя.[NOM.SG]</ta>
            <ta e="T587" id="Seg_8027" s="T586">сидеть-PST-1SG</ta>
            <ta e="T589" id="Seg_8028" s="T588">опять</ta>
            <ta e="T591" id="Seg_8029" s="T590">я.LAT</ta>
            <ta e="T592" id="Seg_8030" s="T591">взять-PST.[3SG]</ta>
            <ta e="T593" id="Seg_8031" s="T592">пойти-PST-1PL</ta>
            <ta e="T594" id="Seg_8032" s="T593">где</ta>
            <ta e="T595" id="Seg_8033" s="T594">много</ta>
            <ta e="T596" id="Seg_8034" s="T595">%%</ta>
            <ta e="T598" id="Seg_8035" s="T597">там</ta>
            <ta e="T599" id="Seg_8036" s="T598">много</ta>
            <ta e="T600" id="Seg_8037" s="T599">люди.[NOM.SG]</ta>
            <ta e="T601" id="Seg_8038" s="T600">быть-PST-3PL</ta>
            <ta e="T603" id="Seg_8039" s="T602">немец.PL-PL</ta>
            <ta e="T604" id="Seg_8040" s="T603">быть-PST-3PL</ta>
            <ta e="T605" id="Seg_8041" s="T604">и</ta>
            <ta e="T607" id="Seg_8042" s="T606">быть-PST-3PL</ta>
            <ta e="T609" id="Seg_8043" s="T608">много</ta>
            <ta e="T610" id="Seg_8044" s="T609">там</ta>
            <ta e="T611" id="Seg_8045" s="T610">долго</ta>
            <ta e="T612" id="Seg_8046" s="T611">два.[NOM.SG]</ta>
            <ta e="T613" id="Seg_8047" s="T612">вечер.[NOM.SG]</ta>
            <ta e="T614" id="Seg_8048" s="T613">я.NOM</ta>
            <ta e="T615" id="Seg_8049" s="T614">ночевать-PST-1SG</ta>
            <ta e="T616" id="Seg_8050" s="T615">там</ta>
            <ta e="T617" id="Seg_8051" s="T616">тогда</ta>
            <ta e="T618" id="Seg_8052" s="T617">опять</ta>
            <ta e="T619" id="Seg_8053" s="T618">здесь</ta>
            <ta e="T620" id="Seg_8054" s="T619">принести-PST.[3SG]</ta>
            <ta e="T622" id="Seg_8055" s="T621">этот-LAT</ta>
            <ta e="T623" id="Seg_8056" s="T622">женщина-LAT</ta>
            <ta e="T625" id="Seg_8057" s="T624">и</ta>
            <ta e="T626" id="Seg_8058" s="T625">там</ta>
            <ta e="T627" id="Seg_8059" s="T626">жить-DUR-1SG</ta>
            <ta e="T629" id="Seg_8060" s="T628">хватит</ta>
            <ta e="T950" id="Seg_8061" s="T949">пойти-PST-1PL</ta>
            <ta e="T1220" id="Seg_8062" s="T1219">тогда</ta>
            <ta e="T1221" id="Seg_8063" s="T1220">Арпит.[NOM.SG]</ta>
            <ta e="T1222" id="Seg_8064" s="T1221">прийти-PST.[3SG]</ta>
            <ta e="T1223" id="Seg_8065" s="T1222">я.GEN</ta>
            <ta e="T1225" id="Seg_8066" s="T1224">NEG</ta>
            <ta e="T1226" id="Seg_8067" s="T1225">посылать-PST.[3SG]</ta>
            <ta e="T1229" id="Seg_8068" s="T1228">а</ta>
            <ta e="T1231" id="Seg_8069" s="T1230">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1232" id="Seg_8070" s="T1231">пойти-EP-IMP.2SG</ta>
            <ta e="T1233" id="Seg_8071" s="T1232">пойти-EP-IMP.2SG</ta>
            <ta e="T1235" id="Seg_8072" s="T1234">тогда</ta>
            <ta e="T1236" id="Seg_8073" s="T1235">я.NOM</ta>
            <ta e="T1237" id="Seg_8074" s="T1236">пойти-PST-1SG</ta>
            <ta e="T1238" id="Seg_8075" s="T1237">здесь</ta>
            <ta e="T1240" id="Seg_8076" s="T1239">а</ta>
            <ta e="T1245" id="Seg_8077" s="T1244">а</ta>
            <ta e="T1454" id="Seg_8078" s="T1453">а</ta>
            <ta e="T1475" id="Seg_8079" s="T1474">а</ta>
            <ta e="T1688" id="Seg_8080" s="T1687">а</ta>
            <ta e="T1724" id="Seg_8081" s="T1723">вчера</ta>
            <ta e="T1725" id="Seg_8082" s="T1724">мы.NOM</ta>
            <ta e="T1726" id="Seg_8083" s="T1725">посещать-CVB</ta>
            <ta e="T1727" id="Seg_8084" s="T1726">пойти-PST-1PL</ta>
            <ta e="T1728" id="Seg_8085" s="T1727">один.[NOM.SG]</ta>
            <ta e="T1729" id="Seg_8086" s="T1728">женщина-LAT</ta>
            <ta e="T1731" id="Seg_8087" s="T1730">сесть-PST-1PL</ta>
            <ta e="T1732" id="Seg_8088" s="T1731">автобус-LAT</ta>
            <ta e="T1733" id="Seg_8089" s="T1732">там</ta>
            <ta e="T1734" id="Seg_8090" s="T1733">прийти-PST-1PL</ta>
            <ta e="T1735" id="Seg_8091" s="T1734">там</ta>
            <ta e="T1736" id="Seg_8092" s="T1735">прийти-PST-3PL</ta>
            <ta e="T1737" id="Seg_8093" s="T1736">люди.[NOM.SG]</ta>
            <ta e="T1738" id="Seg_8094" s="T1737">говорить-INF.LAT</ta>
            <ta e="T1739" id="Seg_8095" s="T1738">я.GEN</ta>
            <ta e="T1740" id="Seg_8096" s="T1739">язык-LAT</ta>
            <ta e="T1741" id="Seg_8097" s="T1740">я.NOM</ta>
            <ta e="T1742" id="Seg_8098" s="T1741">этот-PL-LAT</ta>
            <ta e="T1743" id="Seg_8099" s="T1742">PTCL</ta>
            <ta e="T1744" id="Seg_8100" s="T1743">сказать-PST-1SG</ta>
            <ta e="T1745" id="Seg_8101" s="T1744">сказать-PST-1SG</ta>
            <ta e="T1747" id="Seg_8102" s="T1746">тогда</ta>
            <ta e="T1849" id="Seg_8103" s="T1848">а</ta>
            <ta e="T1866" id="Seg_8104" s="T1865">я.NOM</ta>
            <ta e="T1867" id="Seg_8105" s="T1866">собирать-PST-1SG</ta>
            <ta e="T1868" id="Seg_8106" s="T1867">прийти</ta>
            <ta e="T1869" id="Seg_8107" s="T1868">прийти-INF.LAT</ta>
            <ta e="T1870" id="Seg_8108" s="T1869">здесь</ta>
            <ta e="T1871" id="Seg_8109" s="T1870">люди.[NOM.SG]</ta>
            <ta e="T1872" id="Seg_8110" s="T1871">сказать-IPFVZ-3PL</ta>
            <ta e="T1873" id="Seg_8111" s="T1872">NEG.AUX-IMP.2SG</ta>
            <ta e="T1874" id="Seg_8112" s="T1873">пойти-EP-CNG</ta>
            <ta e="T1876" id="Seg_8113" s="T1875">умереть-RES-FUT-2SG</ta>
            <ta e="T1877" id="Seg_8114" s="T1876">там</ta>
            <ta e="T1879" id="Seg_8115" s="T1878">я.NOM</ta>
            <ta e="T1880" id="Seg_8116" s="T1879">сказать-PST-1SG</ta>
            <ta e="T1881" id="Seg_8117" s="T1880">NEG</ta>
            <ta e="T1882" id="Seg_8118" s="T1881">бояться-PRS-1SG</ta>
            <ta e="T1884" id="Seg_8119" s="T1883">там</ta>
            <ta e="T1886" id="Seg_8120" s="T1885">я.ACC</ta>
            <ta e="T1887" id="Seg_8121" s="T1886">там</ta>
            <ta e="T1888" id="Seg_8122" s="T1887">тоже</ta>
            <ta e="T1889" id="Seg_8123" s="T1888">место-LAT</ta>
            <ta e="T1890" id="Seg_8124" s="T1889">класть-FUT-3PL</ta>
            <ta e="T1892" id="Seg_8125" s="T1891">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T113" id="Seg_8126" s="T112">conj</ta>
            <ta e="T152" id="Seg_8127" s="T151">conj</ta>
            <ta e="T321" id="Seg_8128" s="T320">pers</ta>
            <ta e="T322" id="Seg_8129" s="T321">v-v:tense.[v:pn]</ta>
            <ta e="T323" id="Seg_8130" s="T322">pers</ta>
            <ta e="T324" id="Seg_8131" s="T323">n-n:case.poss</ta>
            <ta e="T326" id="Seg_8132" s="T325">num.[n:case]</ta>
            <ta e="T327" id="Seg_8133" s="T326">n.[n:case]</ta>
            <ta e="T329" id="Seg_8134" s="T328">v-v:mood.pn</ta>
            <ta e="T330" id="Seg_8135" s="T329">adv</ta>
            <ta e="T331" id="Seg_8136" s="T330">v-v:mood.pn</ta>
            <ta e="T332" id="Seg_8137" s="T331">adv</ta>
            <ta e="T333" id="Seg_8138" s="T332">pers</ta>
            <ta e="T334" id="Seg_8139" s="T333">que-n:case</ta>
            <ta e="T335" id="Seg_8140" s="T334">v-v:tense-v:pn</ta>
            <ta e="T337" id="Seg_8141" s="T336">que.[n:case]=ptcl</ta>
            <ta e="T338" id="Seg_8142" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_8143" s="T338">v-v:pn</ta>
            <ta e="T341" id="Seg_8144" s="T340">adv</ta>
            <ta e="T342" id="Seg_8145" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_8146" s="T342">dempro-n:case</ta>
            <ta e="T344" id="Seg_8147" s="T343">n.[n:case]</ta>
            <ta e="T346" id="Seg_8148" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_8149" s="T346">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_8150" s="T349">pers</ta>
            <ta e="T351" id="Seg_8151" s="T350">adv</ta>
            <ta e="T352" id="Seg_8152" s="T351">pers</ta>
            <ta e="T353" id="Seg_8153" s="T352">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_8154" s="T354">adv</ta>
            <ta e="T356" id="Seg_8155" s="T355">dempro.[n:case]</ta>
            <ta e="T357" id="Seg_8156" s="T356">pers</ta>
            <ta e="T358" id="Seg_8157" s="T357">v-v&gt;v.[v:pn]</ta>
            <ta e="T359" id="Seg_8158" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_8159" s="T359">refl-n:case.poss</ta>
            <ta e="T361" id="Seg_8160" s="T360">pers</ta>
            <ta e="T363" id="Seg_8161" s="T362">adv</ta>
            <ta e="T364" id="Seg_8162" s="T363">v-v:tense.[v:pn]</ta>
            <ta e="T365" id="Seg_8163" s="T364">pers</ta>
            <ta e="T366" id="Seg_8164" s="T365">pers</ta>
            <ta e="T367" id="Seg_8165" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8166" s="T367">v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_8167" s="T368">dempro-n:case</ta>
            <ta e="T371" id="Seg_8168" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_8169" s="T371">pers</ta>
            <ta e="T373" id="Seg_8170" s="T372">n-n:case.poss</ta>
            <ta e="T374" id="Seg_8171" s="T373">dempro-n:case</ta>
            <ta e="T375" id="Seg_8172" s="T374">v-v&gt;v.[v:pn]</ta>
            <ta e="T377" id="Seg_8173" s="T376">dempro.[n:case]</ta>
            <ta e="T378" id="Seg_8174" s="T377">pers</ta>
            <ta e="T379" id="Seg_8175" s="T378">n.[n:case]</ta>
            <ta e="T380" id="Seg_8176" s="T379">v-v:tense.[v:pn]</ta>
            <ta e="T383" id="Seg_8177" s="T382">pers</ta>
            <ta e="T384" id="Seg_8178" s="T383">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_8179" s="T384">adv</ta>
            <ta e="T387" id="Seg_8180" s="T386">interj</ta>
            <ta e="T388" id="Seg_8181" s="T387">adv</ta>
            <ta e="T389" id="Seg_8182" s="T388">v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_8183" s="T389">conj</ta>
            <ta e="T391" id="Seg_8184" s="T390">dempro.[n:case]</ta>
            <ta e="T392" id="Seg_8185" s="T391">adv</ta>
            <ta e="T393" id="Seg_8186" s="T392">dempro.[n:case]</ta>
            <ta e="T394" id="Seg_8187" s="T393">v-v:tense.[v:pn]</ta>
            <ta e="T395" id="Seg_8188" s="T394">v-v:tense.[v:pn]</ta>
            <ta e="T397" id="Seg_8189" s="T396">v-v:tense.[v:pn]</ta>
            <ta e="T398" id="Seg_8190" s="T397">v-v:n.fin</ta>
            <ta e="T400" id="Seg_8191" s="T399">adv</ta>
            <ta e="T401" id="Seg_8192" s="T400">v-v:tense.[v:pn]</ta>
            <ta e="T402" id="Seg_8193" s="T401">v-v:tense.[v:pn]</ta>
            <ta e="T403" id="Seg_8194" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_8195" s="T403">n-n:case.poss</ta>
            <ta e="T406" id="Seg_8196" s="T405">adv</ta>
            <ta e="T407" id="Seg_8197" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_8198" s="T407">adj.[n:case]</ta>
            <ta e="T409" id="Seg_8199" s="T408">v-v:tense.[v:pn]</ta>
            <ta e="T410" id="Seg_8200" s="T409">v-v:tense.[v:pn]</ta>
            <ta e="T412" id="Seg_8201" s="T411">v-v:tense.[v:pn]</ta>
            <ta e="T413" id="Seg_8202" s="T412">adv</ta>
            <ta e="T415" id="Seg_8203" s="T414">adv</ta>
            <ta e="T416" id="Seg_8204" s="T415">v-v:tense.[v:pn]</ta>
            <ta e="T417" id="Seg_8205" s="T416">n-n:case.poss</ta>
            <ta e="T419" id="Seg_8206" s="T418">adv</ta>
            <ta e="T420" id="Seg_8207" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_8208" s="T420">v-v:tense.[v:pn]</ta>
            <ta e="T423" id="Seg_8209" s="T422">pers</ta>
            <ta e="T424" id="Seg_8210" s="T423">dempro-n:case</ta>
            <ta e="T425" id="Seg_8211" s="T424">v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_8212" s="T426">adv</ta>
            <ta e="T429" id="Seg_8213" s="T428">n.[n:case]</ta>
            <ta e="T430" id="Seg_8214" s="T429">dempro-n:case</ta>
            <ta e="T431" id="Seg_8215" s="T430">adj.[n:case]</ta>
            <ta e="T432" id="Seg_8216" s="T431">adj.[n:case]</ta>
            <ta e="T433" id="Seg_8217" s="T432">n.[n:case]</ta>
            <ta e="T434" id="Seg_8218" s="T433">n.[n:case]</ta>
            <ta e="T435" id="Seg_8219" s="T434">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_8220" s="T436">dempro.[n:case]</ta>
            <ta e="T438" id="Seg_8221" s="T437">v-v&gt;v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T439" id="Seg_8222" s="T438">adv</ta>
            <ta e="T440" id="Seg_8223" s="T439">adv</ta>
            <ta e="T441" id="Seg_8224" s="T440">v-v:tense.[v:pn]</ta>
            <ta e="T442" id="Seg_8225" s="T441">n-n:case.poss</ta>
            <ta e="T444" id="Seg_8226" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_8227" s="T444">v-v:ins-v:mood.pn</ta>
            <ta e="T446" id="Seg_8228" s="T445">pers</ta>
            <ta e="T447" id="Seg_8229" s="T446">adv</ta>
            <ta e="T448" id="Seg_8230" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_8231" s="T448">n-n:case</ta>
            <ta e="T451" id="Seg_8232" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8233" s="T451">v-v:tense.[v:pn]</ta>
            <ta e="T454" id="Seg_8234" s="T453">adv</ta>
            <ta e="T456" id="Seg_8235" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_8236" s="T456">v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_8237" s="T458">adv</ta>
            <ta e="T460" id="Seg_8238" s="T459">dempro.[n:case]</ta>
            <ta e="T461" id="Seg_8239" s="T460">v-v:tense.[v:pn]</ta>
            <ta e="T462" id="Seg_8240" s="T461">pers</ta>
            <ta e="T463" id="Seg_8241" s="T462">ptcl</ta>
            <ta e="T464" id="Seg_8242" s="T463">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_8243" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_8244" s="T466">v-v&gt;v-v:pn</ta>
            <ta e="T468" id="Seg_8245" s="T467">n-n:num</ta>
            <ta e="T469" id="Seg_8246" s="T468">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_8247" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_8248" s="T471">v</ta>
            <ta e="T473" id="Seg_8249" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_8250" s="T473">n-n:case</ta>
            <ta e="T476" id="Seg_8251" s="T475">adv</ta>
            <ta e="T477" id="Seg_8252" s="T476">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_8253" s="T477">adv</ta>
            <ta e="T479" id="Seg_8254" s="T478">v-v:tense.[v:pn]</ta>
            <ta e="T480" id="Seg_8255" s="T479">n.[n:case]</ta>
            <ta e="T482" id="Seg_8256" s="T481">pers</ta>
            <ta e="T485" id="Seg_8257" s="T484">v-v:tense-v:pn</ta>
            <ta e="T1918" id="Seg_8258" s="T485">propr</ta>
            <ta e="T486" id="Seg_8259" s="T1918">n-n:case</ta>
            <ta e="T487" id="Seg_8260" s="T486">v-v:tense-v:pn</ta>
            <ta e="T489" id="Seg_8261" s="T488">adv</ta>
            <ta e="T490" id="Seg_8262" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_8263" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_8264" s="T491">v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_8265" s="T493">que</ta>
            <ta e="T495" id="Seg_8266" s="T494">num.[n:case]</ta>
            <ta e="T496" id="Seg_8267" s="T495">v-v:tense.[v:pn]</ta>
            <ta e="T497" id="Seg_8268" s="T496">adv</ta>
            <ta e="T498" id="Seg_8269" s="T497">v-v:tense-v:pn</ta>
            <ta e="T499" id="Seg_8270" s="T498">propr-n:case</ta>
            <ta e="T501" id="Seg_8271" s="T500">adv</ta>
            <ta e="T502" id="Seg_8272" s="T501">adv</ta>
            <ta e="T503" id="Seg_8273" s="T502">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_8274" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_8275" s="T505">adv</ta>
            <ta e="T507" id="Seg_8276" s="T506">pers</ta>
            <ta e="T508" id="Seg_8277" s="T507">v-v:tense.[v:pn]</ta>
            <ta e="T509" id="Seg_8278" s="T508">n.[n:case]</ta>
            <ta e="T510" id="Seg_8279" s="T509">ptcl</ta>
            <ta e="T512" id="Seg_8280" s="T511">adv</ta>
            <ta e="T514" id="Seg_8281" s="T513">adv</ta>
            <ta e="T517" id="Seg_8282" s="T516">adv</ta>
            <ta e="T518" id="Seg_8283" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_8284" s="T518">v-v:tense-v:pn</ta>
            <ta e="T520" id="Seg_8285" s="T519">conj</ta>
            <ta e="T521" id="Seg_8286" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_8287" s="T521">propr-n:case</ta>
            <ta e="T524" id="Seg_8288" s="T523">adv</ta>
            <ta e="T525" id="Seg_8289" s="T524">v-v:tense-v:pn</ta>
            <ta e="T526" id="Seg_8290" s="T525">que</ta>
            <ta e="T527" id="Seg_8291" s="T526">v-v&gt;v-v:pn</ta>
            <ta e="T528" id="Seg_8292" s="T527">adv</ta>
            <ta e="T529" id="Seg_8293" s="T528">v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_8294" s="T529">conj</ta>
            <ta e="T1917" id="Seg_8295" s="T530">v-v:n-fin</ta>
            <ta e="T531" id="Seg_8296" s="T1917">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_8297" s="T532">propr-n:case</ta>
            <ta e="T534" id="Seg_8298" s="T533">adv</ta>
            <ta e="T535" id="Seg_8299" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_8300" s="T535">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_8301" s="T537">adv</ta>
            <ta e="T539" id="Seg_8302" s="T538">adv</ta>
            <ta e="T540" id="Seg_8303" s="T539">adv</ta>
            <ta e="T541" id="Seg_8304" s="T540">v-v:tense-v:pn</ta>
            <ta e="T542" id="Seg_8305" s="T541">conj</ta>
            <ta e="T543" id="Seg_8306" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_8307" s="T543">propr-n:case</ta>
            <ta e="T547" id="Seg_8308" s="T546">adv</ta>
            <ta e="T548" id="Seg_8309" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_8310" s="T548">v-v:tense-v:pn</ta>
            <ta e="T550" id="Seg_8311" s="T549">adv</ta>
            <ta e="T551" id="Seg_8312" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_8313" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_8314" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_8315" s="T553">v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_8316" s="T554">propr-n:case</ta>
            <ta e="T557" id="Seg_8317" s="T556">adv</ta>
            <ta e="T558" id="Seg_8318" s="T557">pers</ta>
            <ta e="T559" id="Seg_8319" s="T558">v-v:tense.[v:pn]</ta>
            <ta e="T561" id="Seg_8320" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8321" s="T561">n.[n:case]</ta>
            <ta e="T563" id="Seg_8322" s="T562">adv</ta>
            <ta e="T564" id="Seg_8323" s="T563">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_8324" s="T565">adv</ta>
            <ta e="T567" id="Seg_8325" s="T566">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_8326" s="T568">adv</ta>
            <ta e="T570" id="Seg_8327" s="T569">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_8328" s="T570">conj</ta>
            <ta e="T572" id="Seg_8329" s="T571">dempro.[n:case]</ta>
            <ta e="T573" id="Seg_8330" s="T572">dempro.[n:case]</ta>
            <ta e="T574" id="Seg_8331" s="T573">n-n:case</ta>
            <ta e="T576" id="Seg_8332" s="T575">v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_8333" s="T577">adv</ta>
            <ta e="T579" id="Seg_8334" s="T578">adv</ta>
            <ta e="T581" id="Seg_8335" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_8336" s="T581">propr.[n:case]</ta>
            <ta e="T584" id="Seg_8337" s="T583">adv</ta>
            <ta e="T585" id="Seg_8338" s="T584">num.[n:case]</ta>
            <ta e="T586" id="Seg_8339" s="T585">n.[n:case]</ta>
            <ta e="T587" id="Seg_8340" s="T586">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_8341" s="T588">adv</ta>
            <ta e="T591" id="Seg_8342" s="T590">pers</ta>
            <ta e="T592" id="Seg_8343" s="T591">v-v:tense.[v:pn]</ta>
            <ta e="T593" id="Seg_8344" s="T592">v-v:tense-v:pn</ta>
            <ta e="T594" id="Seg_8345" s="T593">que</ta>
            <ta e="T595" id="Seg_8346" s="T594">quant</ta>
            <ta e="T596" id="Seg_8347" s="T595">n</ta>
            <ta e="T598" id="Seg_8348" s="T597">adv</ta>
            <ta e="T599" id="Seg_8349" s="T598">quant</ta>
            <ta e="T600" id="Seg_8350" s="T599">n.[n:case]</ta>
            <ta e="T601" id="Seg_8351" s="T600">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_8352" s="T602">n-n:num</ta>
            <ta e="T604" id="Seg_8353" s="T603">v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_8354" s="T604">conj</ta>
            <ta e="T607" id="Seg_8355" s="T606">v-v:tense-v:pn</ta>
            <ta e="T609" id="Seg_8356" s="T608">quant</ta>
            <ta e="T610" id="Seg_8357" s="T609">adv</ta>
            <ta e="T611" id="Seg_8358" s="T610">adv</ta>
            <ta e="T612" id="Seg_8359" s="T611">num.[n:case]</ta>
            <ta e="T613" id="Seg_8360" s="T612">n.[n:case]</ta>
            <ta e="T614" id="Seg_8361" s="T613">pers</ta>
            <ta e="T615" id="Seg_8362" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_8363" s="T615">adv</ta>
            <ta e="T617" id="Seg_8364" s="T616">adv</ta>
            <ta e="T618" id="Seg_8365" s="T617">adv</ta>
            <ta e="T619" id="Seg_8366" s="T618">adv</ta>
            <ta e="T620" id="Seg_8367" s="T619">v-v:tense.[v:pn]</ta>
            <ta e="T622" id="Seg_8368" s="T621">dempro-n:case</ta>
            <ta e="T623" id="Seg_8369" s="T622">n-n:case</ta>
            <ta e="T625" id="Seg_8370" s="T624">conj</ta>
            <ta e="T626" id="Seg_8371" s="T625">adv</ta>
            <ta e="T627" id="Seg_8372" s="T626">v-v&gt;v-v:pn</ta>
            <ta e="T629" id="Seg_8373" s="T628">ptcl</ta>
            <ta e="T950" id="Seg_8374" s="T949">v-v:tense-v:pn</ta>
            <ta e="T1220" id="Seg_8375" s="T1219">adv</ta>
            <ta e="T1221" id="Seg_8376" s="T1220">propr.[n:case]</ta>
            <ta e="T1222" id="Seg_8377" s="T1221">v-v:tense.[v:pn]</ta>
            <ta e="T1223" id="Seg_8378" s="T1222">pers</ta>
            <ta e="T1225" id="Seg_8379" s="T1224">ptcl</ta>
            <ta e="T1226" id="Seg_8380" s="T1225">v-v:tense.[v:pn]</ta>
            <ta e="T1229" id="Seg_8381" s="T1228">conj</ta>
            <ta e="T1231" id="Seg_8382" s="T1230">v-v&gt;v.[v:pn]</ta>
            <ta e="T1232" id="Seg_8383" s="T1231">v-v:ins-v:mood.pn</ta>
            <ta e="T1233" id="Seg_8384" s="T1232">v-v:ins-v:mood.pn</ta>
            <ta e="T1235" id="Seg_8385" s="T1234">adv</ta>
            <ta e="T1236" id="Seg_8386" s="T1235">pers</ta>
            <ta e="T1237" id="Seg_8387" s="T1236">v-v:tense-v:pn</ta>
            <ta e="T1238" id="Seg_8388" s="T1237">adv</ta>
            <ta e="T1240" id="Seg_8389" s="T1239">conj</ta>
            <ta e="T1245" id="Seg_8390" s="T1244">conj</ta>
            <ta e="T1454" id="Seg_8391" s="T1453">conj</ta>
            <ta e="T1475" id="Seg_8392" s="T1474">conj</ta>
            <ta e="T1688" id="Seg_8393" s="T1687">conj</ta>
            <ta e="T1724" id="Seg_8394" s="T1723">adv</ta>
            <ta e="T1725" id="Seg_8395" s="T1724">pers</ta>
            <ta e="T1726" id="Seg_8396" s="T1725">v-v:n.fin</ta>
            <ta e="T1727" id="Seg_8397" s="T1726">v-v:tense-v:pn</ta>
            <ta e="T1728" id="Seg_8398" s="T1727">num.[n:case]</ta>
            <ta e="T1729" id="Seg_8399" s="T1728">n-n:case</ta>
            <ta e="T1731" id="Seg_8400" s="T1730">v-v:tense-v:pn</ta>
            <ta e="T1732" id="Seg_8401" s="T1731">n-n:case</ta>
            <ta e="T1733" id="Seg_8402" s="T1732">adv</ta>
            <ta e="T1734" id="Seg_8403" s="T1733">v-v:tense-v:pn</ta>
            <ta e="T1735" id="Seg_8404" s="T1734">adv</ta>
            <ta e="T1736" id="Seg_8405" s="T1735">v-v:tense-v:pn</ta>
            <ta e="T1737" id="Seg_8406" s="T1736">n.[n:case]</ta>
            <ta e="T1738" id="Seg_8407" s="T1737">v-v:n.fin</ta>
            <ta e="T1739" id="Seg_8408" s="T1738">pers</ta>
            <ta e="T1740" id="Seg_8409" s="T1739">n-n:case</ta>
            <ta e="T1741" id="Seg_8410" s="T1740">pers</ta>
            <ta e="T1742" id="Seg_8411" s="T1741">dempro-n:num-n:case</ta>
            <ta e="T1743" id="Seg_8412" s="T1742">ptcl</ta>
            <ta e="T1744" id="Seg_8413" s="T1743">v-v:tense-v:pn</ta>
            <ta e="T1745" id="Seg_8414" s="T1744">v-v:tense-v:pn</ta>
            <ta e="T1747" id="Seg_8415" s="T1746">adv</ta>
            <ta e="T1799" id="Seg_8416" s="T1798">%%</ta>
            <ta e="T1849" id="Seg_8417" s="T1848">conj</ta>
            <ta e="T1866" id="Seg_8418" s="T1865">pers</ta>
            <ta e="T1867" id="Seg_8419" s="T1866">v-v:tense-v:pn</ta>
            <ta e="T1868" id="Seg_8420" s="T1867">v</ta>
            <ta e="T1869" id="Seg_8421" s="T1868">v-v:n.fin</ta>
            <ta e="T1870" id="Seg_8422" s="T1869">adv</ta>
            <ta e="T1871" id="Seg_8423" s="T1870">n.[n:case]</ta>
            <ta e="T1872" id="Seg_8424" s="T1871">v-v&gt;v-v:pn</ta>
            <ta e="T1873" id="Seg_8425" s="T1872">aux-v:mood.pn</ta>
            <ta e="T1874" id="Seg_8426" s="T1873">v-v:ins-v:mood.pn</ta>
            <ta e="T1876" id="Seg_8427" s="T1875">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1877" id="Seg_8428" s="T1876">adv</ta>
            <ta e="T1879" id="Seg_8429" s="T1878">pers</ta>
            <ta e="T1880" id="Seg_8430" s="T1879">v-v:tense-v:pn</ta>
            <ta e="T1881" id="Seg_8431" s="T1880">ptcl</ta>
            <ta e="T1882" id="Seg_8432" s="T1881">v-v:tense-v:pn</ta>
            <ta e="T1884" id="Seg_8433" s="T1883">adv</ta>
            <ta e="T1886" id="Seg_8434" s="T1885">pers</ta>
            <ta e="T1887" id="Seg_8435" s="T1886">adv</ta>
            <ta e="T1888" id="Seg_8436" s="T1887">ptcl</ta>
            <ta e="T1889" id="Seg_8437" s="T1888">n-n:case</ta>
            <ta e="T1890" id="Seg_8438" s="T1889">v-v:tense-v:pn</ta>
            <ta e="T1892" id="Seg_8439" s="T1891">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T113" id="Seg_8440" s="T112">conj</ta>
            <ta e="T152" id="Seg_8441" s="T151">conj</ta>
            <ta e="T321" id="Seg_8442" s="T320">pers</ta>
            <ta e="T322" id="Seg_8443" s="T321">v</ta>
            <ta e="T323" id="Seg_8444" s="T322">pers</ta>
            <ta e="T324" id="Seg_8445" s="T323">n</ta>
            <ta e="T326" id="Seg_8446" s="T325">num</ta>
            <ta e="T327" id="Seg_8447" s="T326">n</ta>
            <ta e="T329" id="Seg_8448" s="T328">v</ta>
            <ta e="T330" id="Seg_8449" s="T329">adv</ta>
            <ta e="T331" id="Seg_8450" s="T330">v</ta>
            <ta e="T332" id="Seg_8451" s="T331">adv</ta>
            <ta e="T333" id="Seg_8452" s="T332">pers</ta>
            <ta e="T334" id="Seg_8453" s="T333">que</ta>
            <ta e="T335" id="Seg_8454" s="T334">v</ta>
            <ta e="T337" id="Seg_8455" s="T336">que</ta>
            <ta e="T338" id="Seg_8456" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_8457" s="T338">v</ta>
            <ta e="T341" id="Seg_8458" s="T340">adv</ta>
            <ta e="T342" id="Seg_8459" s="T341">v</ta>
            <ta e="T343" id="Seg_8460" s="T342">dempro</ta>
            <ta e="T344" id="Seg_8461" s="T343">n</ta>
            <ta e="T346" id="Seg_8462" s="T345">v</ta>
            <ta e="T347" id="Seg_8463" s="T346">v</ta>
            <ta e="T350" id="Seg_8464" s="T349">pers</ta>
            <ta e="T351" id="Seg_8465" s="T350">adv</ta>
            <ta e="T352" id="Seg_8466" s="T351">pers</ta>
            <ta e="T353" id="Seg_8467" s="T352">v</ta>
            <ta e="T355" id="Seg_8468" s="T354">adv</ta>
            <ta e="T356" id="Seg_8469" s="T355">dempro</ta>
            <ta e="T357" id="Seg_8470" s="T356">pers</ta>
            <ta e="T358" id="Seg_8471" s="T357">v</ta>
            <ta e="T359" id="Seg_8472" s="T358">v</ta>
            <ta e="T360" id="Seg_8473" s="T359">refl</ta>
            <ta e="T361" id="Seg_8474" s="T360">pers</ta>
            <ta e="T363" id="Seg_8475" s="T362">adv</ta>
            <ta e="T364" id="Seg_8476" s="T363">v</ta>
            <ta e="T365" id="Seg_8477" s="T364">pers</ta>
            <ta e="T366" id="Seg_8478" s="T365">pers</ta>
            <ta e="T367" id="Seg_8479" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8480" s="T367">v</ta>
            <ta e="T369" id="Seg_8481" s="T368">dempro</ta>
            <ta e="T371" id="Seg_8482" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_8483" s="T371">pers</ta>
            <ta e="T373" id="Seg_8484" s="T372">n</ta>
            <ta e="T374" id="Seg_8485" s="T373">dempro</ta>
            <ta e="T375" id="Seg_8486" s="T374">v</ta>
            <ta e="T377" id="Seg_8487" s="T376">dempro</ta>
            <ta e="T378" id="Seg_8488" s="T377">pers</ta>
            <ta e="T379" id="Seg_8489" s="T378">n</ta>
            <ta e="T380" id="Seg_8490" s="T379">v</ta>
            <ta e="T383" id="Seg_8491" s="T382">pers</ta>
            <ta e="T384" id="Seg_8492" s="T383">v</ta>
            <ta e="T385" id="Seg_8493" s="T384">adv</ta>
            <ta e="T387" id="Seg_8494" s="T386">interj</ta>
            <ta e="T388" id="Seg_8495" s="T387">adv</ta>
            <ta e="T389" id="Seg_8496" s="T388">v</ta>
            <ta e="T390" id="Seg_8497" s="T389">conj</ta>
            <ta e="T391" id="Seg_8498" s="T390">dempro</ta>
            <ta e="T392" id="Seg_8499" s="T391">adv</ta>
            <ta e="T393" id="Seg_8500" s="T392">dempro</ta>
            <ta e="T394" id="Seg_8501" s="T393">v</ta>
            <ta e="T395" id="Seg_8502" s="T394">v</ta>
            <ta e="T397" id="Seg_8503" s="T396">v</ta>
            <ta e="T398" id="Seg_8504" s="T397">v</ta>
            <ta e="T400" id="Seg_8505" s="T399">adv</ta>
            <ta e="T401" id="Seg_8506" s="T400">v</ta>
            <ta e="T402" id="Seg_8507" s="T401">v</ta>
            <ta e="T403" id="Seg_8508" s="T402">n</ta>
            <ta e="T404" id="Seg_8509" s="T403">n</ta>
            <ta e="T406" id="Seg_8510" s="T405">adv</ta>
            <ta e="T407" id="Seg_8511" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_8512" s="T407">adj</ta>
            <ta e="T409" id="Seg_8513" s="T408">v</ta>
            <ta e="T410" id="Seg_8514" s="T409">v</ta>
            <ta e="T412" id="Seg_8515" s="T411">v</ta>
            <ta e="T413" id="Seg_8516" s="T412">adv</ta>
            <ta e="T415" id="Seg_8517" s="T414">adv</ta>
            <ta e="T416" id="Seg_8518" s="T415">v</ta>
            <ta e="T417" id="Seg_8519" s="T416">n</ta>
            <ta e="T419" id="Seg_8520" s="T418">adv</ta>
            <ta e="T420" id="Seg_8521" s="T419">n</ta>
            <ta e="T421" id="Seg_8522" s="T420">v</ta>
            <ta e="T423" id="Seg_8523" s="T422">pers</ta>
            <ta e="T424" id="Seg_8524" s="T423">dempro</ta>
            <ta e="T425" id="Seg_8525" s="T424">v</ta>
            <ta e="T427" id="Seg_8526" s="T426">adv</ta>
            <ta e="T429" id="Seg_8527" s="T428">n</ta>
            <ta e="T430" id="Seg_8528" s="T429">dempro</ta>
            <ta e="T431" id="Seg_8529" s="T430">adj</ta>
            <ta e="T432" id="Seg_8530" s="T431">adj</ta>
            <ta e="T433" id="Seg_8531" s="T432">n</ta>
            <ta e="T434" id="Seg_8532" s="T433">n</ta>
            <ta e="T435" id="Seg_8533" s="T434">v</ta>
            <ta e="T437" id="Seg_8534" s="T436">dempro</ta>
            <ta e="T438" id="Seg_8535" s="T437">v</ta>
            <ta e="T439" id="Seg_8536" s="T438">adv</ta>
            <ta e="T440" id="Seg_8537" s="T439">adv</ta>
            <ta e="T441" id="Seg_8538" s="T440">v</ta>
            <ta e="T442" id="Seg_8539" s="T441">n</ta>
            <ta e="T444" id="Seg_8540" s="T443">v</ta>
            <ta e="T445" id="Seg_8541" s="T444">v</ta>
            <ta e="T446" id="Seg_8542" s="T445">pers</ta>
            <ta e="T447" id="Seg_8543" s="T446">adv</ta>
            <ta e="T448" id="Seg_8544" s="T447">v</ta>
            <ta e="T449" id="Seg_8545" s="T448">n</ta>
            <ta e="T451" id="Seg_8546" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8547" s="T451">v</ta>
            <ta e="T454" id="Seg_8548" s="T453">adv</ta>
            <ta e="T456" id="Seg_8549" s="T455">n</ta>
            <ta e="T457" id="Seg_8550" s="T456">v</ta>
            <ta e="T459" id="Seg_8551" s="T458">adv</ta>
            <ta e="T460" id="Seg_8552" s="T459">dempro</ta>
            <ta e="T461" id="Seg_8553" s="T460">v</ta>
            <ta e="T462" id="Seg_8554" s="T461">pers</ta>
            <ta e="T463" id="Seg_8555" s="T462">ptcl</ta>
            <ta e="T464" id="Seg_8556" s="T463">v</ta>
            <ta e="T466" id="Seg_8557" s="T465">adv</ta>
            <ta e="T467" id="Seg_8558" s="T466">v</ta>
            <ta e="T468" id="Seg_8559" s="T467">n</ta>
            <ta e="T469" id="Seg_8560" s="T468">v</ta>
            <ta e="T471" id="Seg_8561" s="T470">v</ta>
            <ta e="T472" id="Seg_8562" s="T471">v</ta>
            <ta e="T473" id="Seg_8563" s="T472">v</ta>
            <ta e="T474" id="Seg_8564" s="T473">n</ta>
            <ta e="T476" id="Seg_8565" s="T475">adv</ta>
            <ta e="T477" id="Seg_8566" s="T476">v</ta>
            <ta e="T478" id="Seg_8567" s="T477">adv</ta>
            <ta e="T479" id="Seg_8568" s="T478">v</ta>
            <ta e="T480" id="Seg_8569" s="T479">n</ta>
            <ta e="T482" id="Seg_8570" s="T481">pers</ta>
            <ta e="T485" id="Seg_8571" s="T484">v</ta>
            <ta e="T1918" id="Seg_8572" s="T485">propr</ta>
            <ta e="T486" id="Seg_8573" s="T1918">n</ta>
            <ta e="T487" id="Seg_8574" s="T486">v</ta>
            <ta e="T489" id="Seg_8575" s="T488">adv</ta>
            <ta e="T490" id="Seg_8576" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_8577" s="T490">n</ta>
            <ta e="T492" id="Seg_8578" s="T491">v</ta>
            <ta e="T494" id="Seg_8579" s="T493">conj</ta>
            <ta e="T495" id="Seg_8580" s="T494">num</ta>
            <ta e="T496" id="Seg_8581" s="T495">v</ta>
            <ta e="T497" id="Seg_8582" s="T496">adv</ta>
            <ta e="T498" id="Seg_8583" s="T497">v</ta>
            <ta e="T499" id="Seg_8584" s="T498">propr</ta>
            <ta e="T501" id="Seg_8585" s="T500">adv</ta>
            <ta e="T502" id="Seg_8586" s="T501">adv</ta>
            <ta e="T503" id="Seg_8587" s="T502">v</ta>
            <ta e="T505" id="Seg_8588" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_8589" s="T505">adv</ta>
            <ta e="T507" id="Seg_8590" s="T506">pers</ta>
            <ta e="T508" id="Seg_8591" s="T507">v</ta>
            <ta e="T509" id="Seg_8592" s="T508">n</ta>
            <ta e="T510" id="Seg_8593" s="T509">ptcl</ta>
            <ta e="T512" id="Seg_8594" s="T511">adv</ta>
            <ta e="T514" id="Seg_8595" s="T513">adv</ta>
            <ta e="T517" id="Seg_8596" s="T516">adv</ta>
            <ta e="T518" id="Seg_8597" s="T517">n</ta>
            <ta e="T519" id="Seg_8598" s="T518">v</ta>
            <ta e="T520" id="Seg_8599" s="T519">conj</ta>
            <ta e="T521" id="Seg_8600" s="T520">v</ta>
            <ta e="T522" id="Seg_8601" s="T521">propr</ta>
            <ta e="T524" id="Seg_8602" s="T523">adv</ta>
            <ta e="T525" id="Seg_8603" s="T524">v</ta>
            <ta e="T526" id="Seg_8604" s="T525">que</ta>
            <ta e="T527" id="Seg_8605" s="T526">v</ta>
            <ta e="T528" id="Seg_8606" s="T527">adv</ta>
            <ta e="T529" id="Seg_8607" s="T528">v</ta>
            <ta e="T530" id="Seg_8608" s="T529">conj</ta>
            <ta e="T1917" id="Seg_8609" s="T530">v</ta>
            <ta e="T531" id="Seg_8610" s="T1917">v</ta>
            <ta e="T533" id="Seg_8611" s="T532">propr</ta>
            <ta e="T534" id="Seg_8612" s="T533">adv</ta>
            <ta e="T535" id="Seg_8613" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_8614" s="T535">v</ta>
            <ta e="T538" id="Seg_8615" s="T537">adv</ta>
            <ta e="T539" id="Seg_8616" s="T538">adv</ta>
            <ta e="T540" id="Seg_8617" s="T539">adv</ta>
            <ta e="T541" id="Seg_8618" s="T540">v</ta>
            <ta e="T542" id="Seg_8619" s="T541">conj</ta>
            <ta e="T543" id="Seg_8620" s="T542">v</ta>
            <ta e="T544" id="Seg_8621" s="T543">propr</ta>
            <ta e="T547" id="Seg_8622" s="T546">adv</ta>
            <ta e="T548" id="Seg_8623" s="T547">n</ta>
            <ta e="T549" id="Seg_8624" s="T548">v</ta>
            <ta e="T550" id="Seg_8625" s="T549">adv</ta>
            <ta e="T551" id="Seg_8626" s="T550">v</ta>
            <ta e="T552" id="Seg_8627" s="T551">v</ta>
            <ta e="T553" id="Seg_8628" s="T552">n</ta>
            <ta e="T554" id="Seg_8629" s="T553">v</ta>
            <ta e="T555" id="Seg_8630" s="T554">propr</ta>
            <ta e="T557" id="Seg_8631" s="T556">adv</ta>
            <ta e="T558" id="Seg_8632" s="T557">pers</ta>
            <ta e="T559" id="Seg_8633" s="T558">v</ta>
            <ta e="T561" id="Seg_8634" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8635" s="T561">n</ta>
            <ta e="T563" id="Seg_8636" s="T562">adv</ta>
            <ta e="T564" id="Seg_8637" s="T563">v</ta>
            <ta e="T566" id="Seg_8638" s="T565">adv</ta>
            <ta e="T567" id="Seg_8639" s="T566">v</ta>
            <ta e="T569" id="Seg_8640" s="T568">adv</ta>
            <ta e="T570" id="Seg_8641" s="T569">v</ta>
            <ta e="T571" id="Seg_8642" s="T570">conj</ta>
            <ta e="T572" id="Seg_8643" s="T571">dempro</ta>
            <ta e="T573" id="Seg_8644" s="T572">dempro</ta>
            <ta e="T574" id="Seg_8645" s="T573">n</ta>
            <ta e="T576" id="Seg_8646" s="T575">v</ta>
            <ta e="T578" id="Seg_8647" s="T577">adv</ta>
            <ta e="T579" id="Seg_8648" s="T578">adv</ta>
            <ta e="T581" id="Seg_8649" s="T580">v</ta>
            <ta e="T582" id="Seg_8650" s="T581">propr</ta>
            <ta e="T584" id="Seg_8651" s="T583">adv</ta>
            <ta e="T585" id="Seg_8652" s="T584">num</ta>
            <ta e="T586" id="Seg_8653" s="T585">n</ta>
            <ta e="T587" id="Seg_8654" s="T586">v</ta>
            <ta e="T589" id="Seg_8655" s="T588">adv</ta>
            <ta e="T591" id="Seg_8656" s="T590">pers</ta>
            <ta e="T592" id="Seg_8657" s="T591">v</ta>
            <ta e="T593" id="Seg_8658" s="T592">v</ta>
            <ta e="T594" id="Seg_8659" s="T593">que</ta>
            <ta e="T595" id="Seg_8660" s="T594">quant</ta>
            <ta e="T596" id="Seg_8661" s="T595">n</ta>
            <ta e="T598" id="Seg_8662" s="T597">adv</ta>
            <ta e="T599" id="Seg_8663" s="T598">quant</ta>
            <ta e="T600" id="Seg_8664" s="T599">n</ta>
            <ta e="T601" id="Seg_8665" s="T600">v</ta>
            <ta e="T603" id="Seg_8666" s="T602">n</ta>
            <ta e="T604" id="Seg_8667" s="T603">v</ta>
            <ta e="T605" id="Seg_8668" s="T604">conj</ta>
            <ta e="T607" id="Seg_8669" s="T606">v</ta>
            <ta e="T609" id="Seg_8670" s="T608">quant</ta>
            <ta e="T610" id="Seg_8671" s="T609">adv</ta>
            <ta e="T611" id="Seg_8672" s="T610">adv</ta>
            <ta e="T612" id="Seg_8673" s="T611">num</ta>
            <ta e="T613" id="Seg_8674" s="T612">n</ta>
            <ta e="T614" id="Seg_8675" s="T613">pers</ta>
            <ta e="T615" id="Seg_8676" s="T614">v</ta>
            <ta e="T616" id="Seg_8677" s="T615">adv</ta>
            <ta e="T617" id="Seg_8678" s="T616">adv</ta>
            <ta e="T618" id="Seg_8679" s="T617">adv</ta>
            <ta e="T619" id="Seg_8680" s="T618">adv</ta>
            <ta e="T620" id="Seg_8681" s="T619">v</ta>
            <ta e="T622" id="Seg_8682" s="T621">dempro</ta>
            <ta e="T623" id="Seg_8683" s="T622">n</ta>
            <ta e="T625" id="Seg_8684" s="T624">conj</ta>
            <ta e="T626" id="Seg_8685" s="T625">adv</ta>
            <ta e="T627" id="Seg_8686" s="T626">v</ta>
            <ta e="T629" id="Seg_8687" s="T628">ptcl</ta>
            <ta e="T950" id="Seg_8688" s="T949">v</ta>
            <ta e="T1220" id="Seg_8689" s="T1219">adv</ta>
            <ta e="T1221" id="Seg_8690" s="T1220">propr</ta>
            <ta e="T1222" id="Seg_8691" s="T1221">v</ta>
            <ta e="T1223" id="Seg_8692" s="T1222">pers</ta>
            <ta e="T1225" id="Seg_8693" s="T1224">ptcl</ta>
            <ta e="T1226" id="Seg_8694" s="T1225">v</ta>
            <ta e="T1229" id="Seg_8695" s="T1228">conj</ta>
            <ta e="T1231" id="Seg_8696" s="T1230">v</ta>
            <ta e="T1232" id="Seg_8697" s="T1231">v</ta>
            <ta e="T1233" id="Seg_8698" s="T1232">v</ta>
            <ta e="T1235" id="Seg_8699" s="T1234">adv</ta>
            <ta e="T1236" id="Seg_8700" s="T1235">pers</ta>
            <ta e="T1237" id="Seg_8701" s="T1236">v</ta>
            <ta e="T1238" id="Seg_8702" s="T1237">adv</ta>
            <ta e="T1240" id="Seg_8703" s="T1239">conj</ta>
            <ta e="T1245" id="Seg_8704" s="T1244">conj</ta>
            <ta e="T1454" id="Seg_8705" s="T1453">conj</ta>
            <ta e="T1475" id="Seg_8706" s="T1474">conj</ta>
            <ta e="T1688" id="Seg_8707" s="T1687">conj</ta>
            <ta e="T1724" id="Seg_8708" s="T1723">adv</ta>
            <ta e="T1725" id="Seg_8709" s="T1724">pers</ta>
            <ta e="T1726" id="Seg_8710" s="T1725">v</ta>
            <ta e="T1727" id="Seg_8711" s="T1726">v</ta>
            <ta e="T1728" id="Seg_8712" s="T1727">num</ta>
            <ta e="T1729" id="Seg_8713" s="T1728">n</ta>
            <ta e="T1731" id="Seg_8714" s="T1730">v</ta>
            <ta e="T1732" id="Seg_8715" s="T1731">n</ta>
            <ta e="T1733" id="Seg_8716" s="T1732">adv</ta>
            <ta e="T1734" id="Seg_8717" s="T1733">v</ta>
            <ta e="T1735" id="Seg_8718" s="T1734">adv</ta>
            <ta e="T1736" id="Seg_8719" s="T1735">v</ta>
            <ta e="T1737" id="Seg_8720" s="T1736">n</ta>
            <ta e="T1738" id="Seg_8721" s="T1737">v</ta>
            <ta e="T1739" id="Seg_8722" s="T1738">pers</ta>
            <ta e="T1740" id="Seg_8723" s="T1739">n</ta>
            <ta e="T1741" id="Seg_8724" s="T1740">pers</ta>
            <ta e="T1742" id="Seg_8725" s="T1741">dempro</ta>
            <ta e="T1743" id="Seg_8726" s="T1742">ptcl</ta>
            <ta e="T1744" id="Seg_8727" s="T1743">v</ta>
            <ta e="T1745" id="Seg_8728" s="T1744">v</ta>
            <ta e="T1747" id="Seg_8729" s="T1746">adv</ta>
            <ta e="T1849" id="Seg_8730" s="T1848">conj</ta>
            <ta e="T1866" id="Seg_8731" s="T1865">pers</ta>
            <ta e="T1867" id="Seg_8732" s="T1866">v</ta>
            <ta e="T1868" id="Seg_8733" s="T1867">v</ta>
            <ta e="T1869" id="Seg_8734" s="T1868">n</ta>
            <ta e="T1870" id="Seg_8735" s="T1869">adv</ta>
            <ta e="T1871" id="Seg_8736" s="T1870">n</ta>
            <ta e="T1872" id="Seg_8737" s="T1871">v</ta>
            <ta e="T1873" id="Seg_8738" s="T1872">aux</ta>
            <ta e="T1874" id="Seg_8739" s="T1873">v</ta>
            <ta e="T1876" id="Seg_8740" s="T1875">v</ta>
            <ta e="T1877" id="Seg_8741" s="T1876">adv</ta>
            <ta e="T1879" id="Seg_8742" s="T1878">pers</ta>
            <ta e="T1880" id="Seg_8743" s="T1879">v</ta>
            <ta e="T1881" id="Seg_8744" s="T1880">ptcl</ta>
            <ta e="T1882" id="Seg_8745" s="T1881">v</ta>
            <ta e="T1884" id="Seg_8746" s="T1883">adv</ta>
            <ta e="T1886" id="Seg_8747" s="T1885">pers</ta>
            <ta e="T1887" id="Seg_8748" s="T1886">adv</ta>
            <ta e="T1888" id="Seg_8749" s="T1887">ptcl</ta>
            <ta e="T1889" id="Seg_8750" s="T1888">n</ta>
            <ta e="T1890" id="Seg_8751" s="T1889">v</ta>
            <ta e="T1892" id="Seg_8752" s="T1891">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T321" id="Seg_8753" s="T320">pro.h:R</ta>
            <ta e="T323" id="Seg_8754" s="T322">pro.h:Poss</ta>
            <ta e="T324" id="Seg_8755" s="T323">np.h:A</ta>
            <ta e="T329" id="Seg_8756" s="T328">0.2.h:A</ta>
            <ta e="T330" id="Seg_8757" s="T329">adv:L</ta>
            <ta e="T331" id="Seg_8758" s="T330">0.2.h:A</ta>
            <ta e="T332" id="Seg_8759" s="T331">adv:L</ta>
            <ta e="T333" id="Seg_8760" s="T332">pro.h:A</ta>
            <ta e="T334" id="Seg_8761" s="T333">pro.h:Com</ta>
            <ta e="T337" id="Seg_8762" s="T336">pro:Th</ta>
            <ta e="T339" id="Seg_8763" s="T338">0.1.h:E</ta>
            <ta e="T341" id="Seg_8764" s="T340">adv:Time</ta>
            <ta e="T342" id="Seg_8765" s="T341">0.1.h:A</ta>
            <ta e="T343" id="Seg_8766" s="T342">pro.h:R</ta>
            <ta e="T344" id="Seg_8767" s="T343">np:P</ta>
            <ta e="T346" id="Seg_8768" s="T345">0.1.h:A</ta>
            <ta e="T352" id="Seg_8769" s="T351">pro.h:A</ta>
            <ta e="T355" id="Seg_8770" s="T354">adv:Time</ta>
            <ta e="T356" id="Seg_8771" s="T355">pro.h:A</ta>
            <ta e="T357" id="Seg_8772" s="T356">pro.h:R</ta>
            <ta e="T359" id="Seg_8773" s="T358">0.1.h:A</ta>
            <ta e="T361" id="Seg_8774" s="T360">pro:G</ta>
            <ta e="T363" id="Seg_8775" s="T362">adv:Time</ta>
            <ta e="T364" id="Seg_8776" s="T363">0.3.h:A</ta>
            <ta e="T365" id="Seg_8777" s="T364">pro:G</ta>
            <ta e="T366" id="Seg_8778" s="T365">pro.h:A</ta>
            <ta e="T369" id="Seg_8779" s="T368">pro.h:Th</ta>
            <ta e="T372" id="Seg_8780" s="T371">pro.h:Poss</ta>
            <ta e="T373" id="Seg_8781" s="T372">np.h:A</ta>
            <ta e="T374" id="Seg_8782" s="T373">pro.h:Th</ta>
            <ta e="T378" id="Seg_8783" s="T377">pro.h:Poss</ta>
            <ta e="T379" id="Seg_8784" s="T378">np.h:A</ta>
            <ta e="T383" id="Seg_8785" s="T382">pro.h:A</ta>
            <ta e="T385" id="Seg_8786" s="T384">adv:G</ta>
            <ta e="T388" id="Seg_8787" s="T387">adv:Time</ta>
            <ta e="T389" id="Seg_8788" s="T388">0.2.h:A</ta>
            <ta e="T392" id="Seg_8789" s="T391">adv:Time</ta>
            <ta e="T393" id="Seg_8790" s="T392">pro.h:A</ta>
            <ta e="T395" id="Seg_8791" s="T394">0.3.h:A</ta>
            <ta e="T397" id="Seg_8792" s="T396">0.3.h:A</ta>
            <ta e="T400" id="Seg_8793" s="T399">adv:Time</ta>
            <ta e="T401" id="Seg_8794" s="T400">0.3.h:A</ta>
            <ta e="T402" id="Seg_8795" s="T401">0.3.h:A</ta>
            <ta e="T403" id="Seg_8796" s="T402">np:G</ta>
            <ta e="T404" id="Seg_8797" s="T403">np:G</ta>
            <ta e="T406" id="Seg_8798" s="T405">adv:L</ta>
            <ta e="T410" id="Seg_8799" s="T409">0.3.h:E</ta>
            <ta e="T412" id="Seg_8800" s="T411">0.3.h:A</ta>
            <ta e="T413" id="Seg_8801" s="T412">adv:L</ta>
            <ta e="T415" id="Seg_8802" s="T414">adv:Time</ta>
            <ta e="T416" id="Seg_8803" s="T415">0.3.h:A</ta>
            <ta e="T417" id="Seg_8804" s="T416">np:G</ta>
            <ta e="T419" id="Seg_8805" s="T418">adv:Time</ta>
            <ta e="T420" id="Seg_8806" s="T419">np:G</ta>
            <ta e="T421" id="Seg_8807" s="T420">0.3.h:A</ta>
            <ta e="T423" id="Seg_8808" s="T422">pro.h:A</ta>
            <ta e="T424" id="Seg_8809" s="T423">pro.h:Com</ta>
            <ta e="T427" id="Seg_8810" s="T426">adv:L</ta>
            <ta e="T429" id="Seg_8811" s="T428">np:Th</ta>
            <ta e="T430" id="Seg_8812" s="T429">pro.h:R</ta>
            <ta e="T433" id="Seg_8813" s="T432">np:Th</ta>
            <ta e="T434" id="Seg_8814" s="T433">np:Th</ta>
            <ta e="T435" id="Seg_8815" s="T434">0.1.h:A</ta>
            <ta e="T437" id="Seg_8816" s="T436">pro.h:A</ta>
            <ta e="T439" id="Seg_8817" s="T438">adv:L</ta>
            <ta e="T440" id="Seg_8818" s="T439">adv:Time</ta>
            <ta e="T441" id="Seg_8819" s="T440">0.3.h:A</ta>
            <ta e="T442" id="Seg_8820" s="T441">np:G</ta>
            <ta e="T444" id="Seg_8821" s="T443">0.1.h:A</ta>
            <ta e="T445" id="Seg_8822" s="T444">0.2.h:A</ta>
            <ta e="T446" id="Seg_8823" s="T445">pro.h:A</ta>
            <ta e="T447" id="Seg_8824" s="T446">adv:L</ta>
            <ta e="T449" id="Seg_8825" s="T448">np:L</ta>
            <ta e="T452" id="Seg_8826" s="T451">0.3.h:A</ta>
            <ta e="T454" id="Seg_8827" s="T453">adv:Time</ta>
            <ta e="T456" id="Seg_8828" s="T455">np:R</ta>
            <ta e="T457" id="Seg_8829" s="T456">0.1.h:A</ta>
            <ta e="T459" id="Seg_8830" s="T458">adv:Time</ta>
            <ta e="T460" id="Seg_8831" s="T459">pro.h:A</ta>
            <ta e="T462" id="Seg_8832" s="T461">pro.h:A</ta>
            <ta e="T466" id="Seg_8833" s="T465">n:Time</ta>
            <ta e="T467" id="Seg_8834" s="T466">0.1.h:A</ta>
            <ta e="T468" id="Seg_8835" s="T467">np:P</ta>
            <ta e="T469" id="Seg_8836" s="T468">0.1.h:A</ta>
            <ta e="T471" id="Seg_8837" s="T470">0.1.h:A</ta>
            <ta e="T473" id="Seg_8838" s="T472">0.1.h:A</ta>
            <ta e="T474" id="Seg_8839" s="T473">np:G</ta>
            <ta e="T476" id="Seg_8840" s="T475">adv:L</ta>
            <ta e="T477" id="Seg_8841" s="T476">0.1.h:A</ta>
            <ta e="T478" id="Seg_8842" s="T477">adv:Time</ta>
            <ta e="T480" id="Seg_8843" s="T479">np:A</ta>
            <ta e="T482" id="Seg_8844" s="T481">pro.h:A</ta>
            <ta e="T486" id="Seg_8845" s="T485">np:G</ta>
            <ta e="T487" id="Seg_8846" s="T486">0.1.h:A</ta>
            <ta e="T489" id="Seg_8847" s="T488">adv:L</ta>
            <ta e="T491" id="Seg_8848" s="T490">np:R</ta>
            <ta e="T492" id="Seg_8849" s="T491">0.3.h:A</ta>
            <ta e="T495" id="Seg_8850" s="T494">np:Th</ta>
            <ta e="T497" id="Seg_8851" s="T496">adv:Time</ta>
            <ta e="T498" id="Seg_8852" s="T497">0.1.h:A</ta>
            <ta e="T499" id="Seg_8853" s="T498">np:G</ta>
            <ta e="T502" id="Seg_8854" s="T501">adv:L</ta>
            <ta e="T503" id="Seg_8855" s="T502">0.1.h:Th</ta>
            <ta e="T506" id="Seg_8856" s="T505">adv:L</ta>
            <ta e="T509" id="Seg_8857" s="T508">np.h:Th</ta>
            <ta e="T512" id="Seg_8858" s="T511">adv:Time</ta>
            <ta e="T514" id="Seg_8859" s="T513">adv:Time</ta>
            <ta e="T517" id="Seg_8860" s="T516">adv:Time</ta>
            <ta e="T518" id="Seg_8861" s="T517">n:Time</ta>
            <ta e="T519" id="Seg_8862" s="T518">0.1.h:A</ta>
            <ta e="T521" id="Seg_8863" s="T520">0.1.h:A</ta>
            <ta e="T522" id="Seg_8864" s="T521">np:G</ta>
            <ta e="T524" id="Seg_8865" s="T523">adv:L</ta>
            <ta e="T525" id="Seg_8866" s="T524">0.1.h:A</ta>
            <ta e="T527" id="Seg_8867" s="T526">0.3.h:Th</ta>
            <ta e="T528" id="Seg_8868" s="T527">adv:L</ta>
            <ta e="T529" id="Seg_8869" s="T528">0.1.h:A</ta>
            <ta e="T531" id="Seg_8870" s="T1917">0.1.h:A</ta>
            <ta e="T533" id="Seg_8871" s="T532">np:G</ta>
            <ta e="T534" id="Seg_8872" s="T533">adv:L</ta>
            <ta e="T536" id="Seg_8873" s="T535">0.1.h:Th</ta>
            <ta e="T538" id="Seg_8874" s="T537">adv:Time</ta>
            <ta e="T539" id="Seg_8875" s="T538">adv:L</ta>
            <ta e="T541" id="Seg_8876" s="T540">0.1.h:A</ta>
            <ta e="T543" id="Seg_8877" s="T542">0.1.h:A</ta>
            <ta e="T544" id="Seg_8878" s="T543">np:G</ta>
            <ta e="T547" id="Seg_8879" s="T546">adv:L</ta>
            <ta e="T548" id="Seg_8880" s="T547">np:G</ta>
            <ta e="T549" id="Seg_8881" s="T548">0.1.h:A</ta>
            <ta e="T550" id="Seg_8882" s="T549">adv:Time</ta>
            <ta e="T552" id="Seg_8883" s="T551">0.1.h:A</ta>
            <ta e="T553" id="Seg_8884" s="T552">np:G</ta>
            <ta e="T554" id="Seg_8885" s="T553">0.1.h:A</ta>
            <ta e="T555" id="Seg_8886" s="T554">np:G</ta>
            <ta e="T557" id="Seg_8887" s="T556">adv:L</ta>
            <ta e="T558" id="Seg_8888" s="T557">pro.h:Th</ta>
            <ta e="T559" id="Seg_8889" s="T558">0.3.h:A</ta>
            <ta e="T562" id="Seg_8890" s="T561">np.h:Th</ta>
            <ta e="T563" id="Seg_8891" s="T562">adv:L</ta>
            <ta e="T566" id="Seg_8892" s="T565">adv:Time</ta>
            <ta e="T567" id="Seg_8893" s="T566">0.1.h:A</ta>
            <ta e="T570" id="Seg_8894" s="T569">0.1.h:A</ta>
            <ta e="T574" id="Seg_8895" s="T573">np:G</ta>
            <ta e="T576" id="Seg_8896" s="T575">0.1.h:A</ta>
            <ta e="T578" id="Seg_8897" s="T577">adv:Time</ta>
            <ta e="T581" id="Seg_8898" s="T580">0.1.h:A</ta>
            <ta e="T582" id="Seg_8899" s="T581">np:G</ta>
            <ta e="T584" id="Seg_8900" s="T583">adv:L</ta>
            <ta e="T587" id="Seg_8901" s="T586">0.1.h:Th</ta>
            <ta e="T593" id="Seg_8902" s="T592">0.1.h:A</ta>
            <ta e="T594" id="Seg_8903" s="T593">pro:L</ta>
            <ta e="T598" id="Seg_8904" s="T597">adv:L</ta>
            <ta e="T600" id="Seg_8905" s="T599">np.h:Th</ta>
            <ta e="T603" id="Seg_8906" s="T602">np.h:Th</ta>
            <ta e="T610" id="Seg_8907" s="T609">adv:L</ta>
            <ta e="T614" id="Seg_8908" s="T613">pro.h:Th</ta>
            <ta e="T616" id="Seg_8909" s="T615">adv:L</ta>
            <ta e="T617" id="Seg_8910" s="T616">adv:Time</ta>
            <ta e="T619" id="Seg_8911" s="T618">adv:L</ta>
            <ta e="T620" id="Seg_8912" s="T619">0.3.h:A</ta>
            <ta e="T622" id="Seg_8913" s="T621">pro:G</ta>
            <ta e="T623" id="Seg_8914" s="T622">np:G</ta>
            <ta e="T626" id="Seg_8915" s="T625">adv:L</ta>
            <ta e="T627" id="Seg_8916" s="T626">0.1.h:E</ta>
            <ta e="T1220" id="Seg_8917" s="T1219">adv:Time</ta>
            <ta e="T1221" id="Seg_8918" s="T1220">np.h:A</ta>
            <ta e="T1223" id="Seg_8919" s="T1222">pro.h:Poss</ta>
            <ta e="T1232" id="Seg_8920" s="T1231">0.2.h:A</ta>
            <ta e="T1233" id="Seg_8921" s="T1232">0.2.h:A</ta>
            <ta e="T1235" id="Seg_8922" s="T1234">adv:Time</ta>
            <ta e="T1236" id="Seg_8923" s="T1235">pro.h:A</ta>
            <ta e="T1238" id="Seg_8924" s="T1237">adv:L</ta>
            <ta e="T1724" id="Seg_8925" s="T1723">adv:Time</ta>
            <ta e="T1725" id="Seg_8926" s="T1724">pro.h:A</ta>
            <ta e="T1729" id="Seg_8927" s="T1728">np:G</ta>
            <ta e="T1731" id="Seg_8928" s="T1730">0.1.h:A</ta>
            <ta e="T1732" id="Seg_8929" s="T1731">np:G</ta>
            <ta e="T1733" id="Seg_8930" s="T1732">adv:L</ta>
            <ta e="T1734" id="Seg_8931" s="T1733">0.1.h:A</ta>
            <ta e="T1735" id="Seg_8932" s="T1734">adv:L</ta>
            <ta e="T1737" id="Seg_8933" s="T1736">np.h:A</ta>
            <ta e="T1739" id="Seg_8934" s="T1738">pro.h:Poss</ta>
            <ta e="T1740" id="Seg_8935" s="T1739">np:Ins</ta>
            <ta e="T1741" id="Seg_8936" s="T1740">pro.h:A</ta>
            <ta e="T1742" id="Seg_8937" s="T1741">pro.h:R</ta>
            <ta e="T1745" id="Seg_8938" s="T1744">0.1.h:A</ta>
            <ta e="T1747" id="Seg_8939" s="T1746">adv:Time</ta>
            <ta e="T1866" id="Seg_8940" s="T1865">pro.h:A</ta>
            <ta e="T1870" id="Seg_8941" s="T1869">adv:Time</ta>
            <ta e="T1871" id="Seg_8942" s="T1870">np.h:A</ta>
            <ta e="T1873" id="Seg_8943" s="T1872">0.2.h:A</ta>
            <ta e="T1876" id="Seg_8944" s="T1875">0.2.h:P</ta>
            <ta e="T1877" id="Seg_8945" s="T1876">adv:L</ta>
            <ta e="T1879" id="Seg_8946" s="T1878">pro.h:A</ta>
            <ta e="T1882" id="Seg_8947" s="T1881">0.1.h:E</ta>
            <ta e="T1884" id="Seg_8948" s="T1883">adv:L</ta>
            <ta e="T1886" id="Seg_8949" s="T1885">pro.h:Th</ta>
            <ta e="T1887" id="Seg_8950" s="T1886">adv:L</ta>
            <ta e="T1889" id="Seg_8951" s="T1888">np:G</ta>
            <ta e="T1890" id="Seg_8952" s="T1889">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T322" id="Seg_8953" s="T321">v:pred</ta>
            <ta e="T324" id="Seg_8954" s="T323">np.h:S</ta>
            <ta e="T329" id="Seg_8955" s="T328">v:pred 0.2.h:S</ta>
            <ta e="T331" id="Seg_8956" s="T330">v:pred 0.2.h:S</ta>
            <ta e="T333" id="Seg_8957" s="T332">pro.h:S</ta>
            <ta e="T335" id="Seg_8958" s="T334">v:pred</ta>
            <ta e="T337" id="Seg_8959" s="T336">pro:O</ta>
            <ta e="T338" id="Seg_8960" s="T337">ptcl.neg</ta>
            <ta e="T339" id="Seg_8961" s="T338">v:pred 0.1.h:S</ta>
            <ta e="T342" id="Seg_8962" s="T341">v:pred 0.1.h:S</ta>
            <ta e="T344" id="Seg_8963" s="T343">np:O</ta>
            <ta e="T346" id="Seg_8964" s="T345">v:pred 0.1.h:S</ta>
            <ta e="T347" id="Seg_8965" s="T346">v:pred</ta>
            <ta e="T351" id="Seg_8966" s="T350">s:cond</ta>
            <ta e="T352" id="Seg_8967" s="T351">pro.h:S</ta>
            <ta e="T353" id="Seg_8968" s="T352">v:pred</ta>
            <ta e="T356" id="Seg_8969" s="T355">pro.h:S</ta>
            <ta e="T358" id="Seg_8970" s="T357">v:pred</ta>
            <ta e="T359" id="Seg_8971" s="T358">v:pred 0.1.h:S</ta>
            <ta e="T364" id="Seg_8972" s="T363">v:pred 0.3.h:S</ta>
            <ta e="T366" id="Seg_8973" s="T365">pro.h:S</ta>
            <ta e="T367" id="Seg_8974" s="T366">ptcl.neg</ta>
            <ta e="T368" id="Seg_8975" s="T367">v:pred</ta>
            <ta e="T369" id="Seg_8976" s="T368">pro.h:O</ta>
            <ta e="T373" id="Seg_8977" s="T372">np.h:S</ta>
            <ta e="T374" id="Seg_8978" s="T373">pro.h:O</ta>
            <ta e="T375" id="Seg_8979" s="T374">v:pred</ta>
            <ta e="T379" id="Seg_8980" s="T378">np.h:S</ta>
            <ta e="T380" id="Seg_8981" s="T379">v:pred</ta>
            <ta e="T383" id="Seg_8982" s="T382">pro.h:S</ta>
            <ta e="T384" id="Seg_8983" s="T383">v:pred</ta>
            <ta e="T389" id="Seg_8984" s="T388">v:pred 0.2.h:S</ta>
            <ta e="T393" id="Seg_8985" s="T392">pro.h:S</ta>
            <ta e="T394" id="Seg_8986" s="T393">v:pred</ta>
            <ta e="T395" id="Seg_8987" s="T394">v:pred 0.3.h:S</ta>
            <ta e="T397" id="Seg_8988" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T398" id="Seg_8989" s="T397">s:purp</ta>
            <ta e="T401" id="Seg_8990" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T402" id="Seg_8991" s="T401">v:pred 0.3.h:S</ta>
            <ta e="T410" id="Seg_8992" s="T409">v:pred 0.3.h:S</ta>
            <ta e="T412" id="Seg_8993" s="T411">v:pred 0.3.h:S</ta>
            <ta e="T416" id="Seg_8994" s="T415">v:pred 0.3.h:S</ta>
            <ta e="T421" id="Seg_8995" s="T420">v:pred 0.3.h:S</ta>
            <ta e="T423" id="Seg_8996" s="T422">pro.h:S</ta>
            <ta e="T425" id="Seg_8997" s="T424">v:pred</ta>
            <ta e="T429" id="Seg_8998" s="T428">np:O</ta>
            <ta e="T433" id="Seg_8999" s="T432">np:O</ta>
            <ta e="T434" id="Seg_9000" s="T433">np:O</ta>
            <ta e="T435" id="Seg_9001" s="T434">v:pred 0.1.h:S</ta>
            <ta e="T437" id="Seg_9002" s="T436">pro.h:S</ta>
            <ta e="T438" id="Seg_9003" s="T437">v:pred</ta>
            <ta e="T441" id="Seg_9004" s="T440">v:pred 0.3.h:S</ta>
            <ta e="T444" id="Seg_9005" s="T443">v:pred 0.1.h:S</ta>
            <ta e="T445" id="Seg_9006" s="T444">v:pred 0.2.h:S</ta>
            <ta e="T446" id="Seg_9007" s="T445">pro.h:S</ta>
            <ta e="T448" id="Seg_9008" s="T447">v:pred</ta>
            <ta e="T451" id="Seg_9009" s="T450">ptcl.neg</ta>
            <ta e="T452" id="Seg_9010" s="T451">v:pred 0.3.h:S</ta>
            <ta e="T457" id="Seg_9011" s="T456">v:pred 0.1.h:S</ta>
            <ta e="T460" id="Seg_9012" s="T459">pro.h:S</ta>
            <ta e="T461" id="Seg_9013" s="T460">v:pred</ta>
            <ta e="T462" id="Seg_9014" s="T461">pro.h:S</ta>
            <ta e="T464" id="Seg_9015" s="T463">v:pred</ta>
            <ta e="T467" id="Seg_9016" s="T466">v:pred 0.1.h:S</ta>
            <ta e="T468" id="Seg_9017" s="T467">np:O</ta>
            <ta e="T469" id="Seg_9018" s="T468">v:pred 0.1.h:S</ta>
            <ta e="T471" id="Seg_9019" s="T470">v:pred 0.1.h:S</ta>
            <ta e="T473" id="Seg_9020" s="T472">v:pred 0.1.h:S</ta>
            <ta e="T477" id="Seg_9021" s="T476">v:pred 0.1.h:S</ta>
            <ta e="T479" id="Seg_9022" s="T478">v:pred</ta>
            <ta e="T480" id="Seg_9023" s="T479">np:S</ta>
            <ta e="T482" id="Seg_9024" s="T481">pro.h:S</ta>
            <ta e="T485" id="Seg_9025" s="T484">v:pred</ta>
            <ta e="T487" id="Seg_9026" s="T486">v:pred 0.1.h:S</ta>
            <ta e="T492" id="Seg_9027" s="T491">v:pred 0.3.h:S</ta>
            <ta e="T495" id="Seg_9028" s="T494">np:S</ta>
            <ta e="T496" id="Seg_9029" s="T495">v:pred</ta>
            <ta e="T498" id="Seg_9030" s="T497">v:pred 0.1.h:S</ta>
            <ta e="T503" id="Seg_9031" s="T502">v:pred 0.1.h:S</ta>
            <ta e="T508" id="Seg_9032" s="T507">v:pred</ta>
            <ta e="T509" id="Seg_9033" s="T508">np.h:S</ta>
            <ta e="T519" id="Seg_9034" s="T518">v:pred 0.1.h:S</ta>
            <ta e="T521" id="Seg_9035" s="T520">v:pred 0.1.h:S</ta>
            <ta e="T525" id="Seg_9036" s="T524">v:pred 0.1.h:S</ta>
            <ta e="T527" id="Seg_9037" s="T526">v:pred 0.3.h:S</ta>
            <ta e="T529" id="Seg_9038" s="T528">v:pred 0.1.h:S</ta>
            <ta e="T1917" id="Seg_9039" s="T530">conv:pred</ta>
            <ta e="T531" id="Seg_9040" s="T1917">v:pred 0.1.h:S</ta>
            <ta e="T536" id="Seg_9041" s="T535">v:pred 0.1.h:S</ta>
            <ta e="T541" id="Seg_9042" s="T540">v:pred 0.1.h:S</ta>
            <ta e="T543" id="Seg_9043" s="T542">v:pred 0.1.h:S</ta>
            <ta e="T549" id="Seg_9044" s="T548">v:pred 0.1.h:S</ta>
            <ta e="T552" id="Seg_9045" s="T551">v:pred 0.1.h:S</ta>
            <ta e="T554" id="Seg_9046" s="T553">v:pred 0.1.h:S</ta>
            <ta e="T559" id="Seg_9047" s="T558">v:pred 0.3.h:S</ta>
            <ta e="T562" id="Seg_9048" s="T561">np.h:S</ta>
            <ta e="T564" id="Seg_9049" s="T563">v:pred</ta>
            <ta e="T567" id="Seg_9050" s="T566">v:pred 0.1.h:S</ta>
            <ta e="T570" id="Seg_9051" s="T569">v:pred 0.1.h:S</ta>
            <ta e="T576" id="Seg_9052" s="T575">v:pred 0.1.h:S</ta>
            <ta e="T581" id="Seg_9053" s="T580">v:pred 0.1.h:S</ta>
            <ta e="T587" id="Seg_9054" s="T586">v:pred 0.1.h:S</ta>
            <ta e="T593" id="Seg_9055" s="T592">v:pred 0.1.h:S</ta>
            <ta e="T600" id="Seg_9056" s="T599">np.h:S</ta>
            <ta e="T601" id="Seg_9057" s="T600">v:pred</ta>
            <ta e="T603" id="Seg_9058" s="T602">np.h:S</ta>
            <ta e="T604" id="Seg_9059" s="T603">v:pred</ta>
            <ta e="T607" id="Seg_9060" s="T606">v:pred</ta>
            <ta e="T614" id="Seg_9061" s="T613">pro.h:S</ta>
            <ta e="T615" id="Seg_9062" s="T614">v:pred</ta>
            <ta e="T620" id="Seg_9063" s="T619">v:pred 0.3.h:S</ta>
            <ta e="T627" id="Seg_9064" s="T626">v:pred 0.1.h:S</ta>
            <ta e="T1221" id="Seg_9065" s="T1220">np.h:S</ta>
            <ta e="T1222" id="Seg_9066" s="T1221">v:pred</ta>
            <ta e="T1225" id="Seg_9067" s="T1224">ptcl.neg</ta>
            <ta e="T1226" id="Seg_9068" s="T1225">v:pred</ta>
            <ta e="T1231" id="Seg_9069" s="T1230">v:pred</ta>
            <ta e="T1232" id="Seg_9070" s="T1231">v:pred 0.2.h:S</ta>
            <ta e="T1233" id="Seg_9071" s="T1232">v:pred 0.2.h:S</ta>
            <ta e="T1236" id="Seg_9072" s="T1235">pro.h:S</ta>
            <ta e="T1237" id="Seg_9073" s="T1236">v:pred</ta>
            <ta e="T1725" id="Seg_9074" s="T1724">pro.h:S</ta>
            <ta e="T1726" id="Seg_9075" s="T1725">conv:pred</ta>
            <ta e="T1727" id="Seg_9076" s="T1726">v:pred</ta>
            <ta e="T1731" id="Seg_9077" s="T1730">v:pred 0.1.h:S</ta>
            <ta e="T1734" id="Seg_9078" s="T1733">v:pred 0.1.h:S</ta>
            <ta e="T1736" id="Seg_9079" s="T1735">v:pred</ta>
            <ta e="T1737" id="Seg_9080" s="T1736">np.h:S</ta>
            <ta e="T1738" id="Seg_9081" s="T1737">s:purp</ta>
            <ta e="T1741" id="Seg_9082" s="T1740">pro.h:S</ta>
            <ta e="T1744" id="Seg_9083" s="T1743">v:pred</ta>
            <ta e="T1745" id="Seg_9084" s="T1744">v:pred 0.1.h:S</ta>
            <ta e="T1866" id="Seg_9085" s="T1865">pro.h:S</ta>
            <ta e="T1867" id="Seg_9086" s="T1866">v:pred</ta>
            <ta e="T1869" id="Seg_9087" s="T1868">s:purp</ta>
            <ta e="T1871" id="Seg_9088" s="T1870">np.h:S</ta>
            <ta e="T1872" id="Seg_9089" s="T1871">v:pred</ta>
            <ta e="T1873" id="Seg_9090" s="T1872">v:pred 0.2.h:S</ta>
            <ta e="T1876" id="Seg_9091" s="T1875">v:pred 0.2.h:S</ta>
            <ta e="T1879" id="Seg_9092" s="T1878">pro.h:S</ta>
            <ta e="T1880" id="Seg_9093" s="T1879">v:pred</ta>
            <ta e="T1881" id="Seg_9094" s="T1880">ptcl.neg</ta>
            <ta e="T1882" id="Seg_9095" s="T1881">v:pred 0.1.h:S</ta>
            <ta e="T1886" id="Seg_9096" s="T1885">pro.h:O</ta>
            <ta e="T1890" id="Seg_9097" s="T1889">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T113" id="Seg_9098" s="T112">RUS:gram</ta>
            <ta e="T152" id="Seg_9099" s="T151">RUS:gram</ta>
            <ta e="T194" id="Seg_9100" s="T193">RUS:gram</ta>
            <ta e="T292" id="Seg_9101" s="T291">RUS:gram</ta>
            <ta e="T337" id="Seg_9102" s="T336">TURK:gram(INDEF)</ta>
            <ta e="T371" id="Seg_9103" s="T370">RUS:disc</ta>
            <ta e="T373" id="Seg_9104" s="T372">RUS:core</ta>
            <ta e="T390" id="Seg_9105" s="T389">RUS:gram</ta>
            <ta e="T404" id="Seg_9106" s="T403">TURK:cult</ta>
            <ta e="T407" id="Seg_9107" s="T406">RUS:mod</ta>
            <ta e="T412" id="Seg_9108" s="T411">%TURK:core</ta>
            <ta e="T449" id="Seg_9109" s="T448">TURK:cult</ta>
            <ta e="T463" id="Seg_9110" s="T462">RUS:mod</ta>
            <ta e="T474" id="Seg_9111" s="T473">RUS:cult</ta>
            <ta e="T480" id="Seg_9112" s="T479">RUS:cult</ta>
            <ta e="T486" id="Seg_9113" s="T1918">TAT:cult</ta>
            <ta e="T490" id="Seg_9114" s="T489">RUS:mod</ta>
            <ta e="T505" id="Seg_9115" s="T504">RUS:mod</ta>
            <ta e="T510" id="Seg_9116" s="T509">RUS:mod</ta>
            <ta e="T520" id="Seg_9117" s="T519">RUS:gram</ta>
            <ta e="T530" id="Seg_9118" s="T529">RUS:gram</ta>
            <ta e="T533" id="Seg_9119" s="T532">RUS:cult</ta>
            <ta e="T535" id="Seg_9120" s="T534">RUS:mod</ta>
            <ta e="T540" id="Seg_9121" s="T539">TURK:core</ta>
            <ta e="T542" id="Seg_9122" s="T541">RUS:gram</ta>
            <ta e="T544" id="Seg_9123" s="T543">RUS:cult</ta>
            <ta e="T553" id="Seg_9124" s="T552">RUS:cult</ta>
            <ta e="T555" id="Seg_9125" s="T554">RUS:cult</ta>
            <ta e="T559" id="Seg_9126" s="T558">RUS:calq</ta>
            <ta e="T561" id="Seg_9127" s="T560">RUS:mod</ta>
            <ta e="T569" id="Seg_9128" s="T568">TURK:core</ta>
            <ta e="T571" id="Seg_9129" s="T570">RUS:gram</ta>
            <ta e="T574" id="Seg_9130" s="T573">TAT:cult</ta>
            <ta e="T582" id="Seg_9131" s="T581">RUS:cult</ta>
            <ta e="T586" id="Seg_9132" s="T585">RUS:cult</ta>
            <ta e="T589" id="Seg_9133" s="T588">TURK:core</ta>
            <ta e="T603" id="Seg_9134" s="T602">RUS:cult</ta>
            <ta e="T605" id="Seg_9135" s="T604">RUS:gram</ta>
            <ta e="T618" id="Seg_9136" s="T617">TURK:core</ta>
            <ta e="T625" id="Seg_9137" s="T624">RUS:gram</ta>
            <ta e="T689" id="Seg_9138" s="T688">RUS:gram</ta>
            <ta e="T710" id="Seg_9139" s="T709">RUS:gram</ta>
            <ta e="T734" id="Seg_9140" s="T733">RUS:gram</ta>
            <ta e="T780" id="Seg_9141" s="T779">RUS:gram</ta>
            <ta e="T824" id="Seg_9142" s="T823">RUS:gram</ta>
            <ta e="T864" id="Seg_9143" s="T863">RUS:gram</ta>
            <ta e="T888" id="Seg_9144" s="T887">RUS:gram</ta>
            <ta e="T934" id="Seg_9145" s="T933">RUS:gram</ta>
            <ta e="T947" id="Seg_9146" s="T946">RUS:gram</ta>
            <ta e="T987" id="Seg_9147" s="T986">RUS:gram</ta>
            <ta e="T1229" id="Seg_9148" s="T1228">RUS:gram</ta>
            <ta e="T1240" id="Seg_9149" s="T1239">RUS:gram</ta>
            <ta e="T1245" id="Seg_9150" s="T1244">RUS:gram</ta>
            <ta e="T1454" id="Seg_9151" s="T1453">RUS:gram</ta>
            <ta e="T1475" id="Seg_9152" s="T1474">RUS:gram</ta>
            <ta e="T1688" id="Seg_9153" s="T1687">RUS:gram</ta>
            <ta e="T1732" id="Seg_9154" s="T1731">RUS:cult</ta>
            <ta e="T1738" id="Seg_9155" s="T1737">%TURK:core</ta>
            <ta e="T1743" id="Seg_9156" s="T1742">TURK:disc</ta>
            <ta e="T1849" id="Seg_9157" s="T1848">RUS:gram</ta>
            <ta e="T1888" id="Seg_9158" s="T1887">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T10" id="Seg_9159" s="T1">RUS:ext</ta>
            <ta e="T47" id="Seg_9160" s="T43">RUS:ext</ta>
            <ta e="T97" id="Seg_9161" s="T92">RUS:ext</ta>
            <ta e="T124" id="Seg_9162" s="T102">RUS:ext</ta>
            <ta e="T136" id="Seg_9163" s="T130">RUS:ext</ta>
            <ta e="T146" id="Seg_9164" s="T137">RUS:ext</ta>
            <ta e="T156" id="Seg_9165" s="T147">RUS:ext</ta>
            <ta e="T166" id="Seg_9166" s="T157">RUS:ext</ta>
            <ta e="T177" id="Seg_9167" s="T167">RUS:ext</ta>
            <ta e="T199" id="Seg_9168" s="T181">RUS:ext</ta>
            <ta e="T209" id="Seg_9169" s="T200">RUS:ext</ta>
            <ta e="T225" id="Seg_9170" s="T214">RUS:ext</ta>
            <ta e="T232" id="Seg_9171" s="T226">RUS:ext</ta>
            <ta e="T238" id="Seg_9172" s="T234">RUS:ext</ta>
            <ta e="T244" id="Seg_9173" s="T242">RUS:ext</ta>
            <ta e="T248" id="Seg_9174" s="T244">RUS:ext</ta>
            <ta e="T250" id="Seg_9175" s="T249">RUS:ext</ta>
            <ta e="T256" id="Seg_9176" s="T251">RUS:ext</ta>
            <ta e="T267" id="Seg_9177" s="T257">RUS:ext</ta>
            <ta e="T275" id="Seg_9178" s="T268">RUS:ext</ta>
            <ta e="T300" id="Seg_9179" s="T277">RUS:ext</ta>
            <ta e="T319" id="Seg_9180" s="T315">RUS:ext</ta>
            <ta e="T349" id="Seg_9181" s="T347">RUS:int</ta>
            <ta e="T606" id="Seg_9182" s="T605">RUS:int</ta>
            <ta e="T637" id="Seg_9183" s="T634">RUS:ext</ta>
            <ta e="T656" id="Seg_9184" s="T641">RUS:ext</ta>
            <ta e="T674" id="Seg_9185" s="T670">RUS:ext</ta>
            <ta e="T677" id="Seg_9186" s="T675">RUS:ext</ta>
            <ta e="T698" id="Seg_9187" s="T680">RUS:ext</ta>
            <ta e="T712" id="Seg_9188" s="T699">RUS:ext</ta>
            <ta e="T723" id="Seg_9189" s="T713">RUS:ext</ta>
            <ta e="T728" id="Seg_9190" s="T724">RUS:ext</ta>
            <ta e="T731" id="Seg_9191" s="T729">RUS:ext</ta>
            <ta e="T737" id="Seg_9192" s="T731">RUS:ext</ta>
            <ta e="T750" id="Seg_9193" s="T738">RUS:ext</ta>
            <ta e="T754" id="Seg_9194" s="T751">RUS:ext</ta>
            <ta e="T769" id="Seg_9195" s="T755">RUS:ext</ta>
            <ta e="T773" id="Seg_9196" s="T770">RUS:ext</ta>
            <ta e="T785" id="Seg_9197" s="T774">RUS:ext</ta>
            <ta e="T789" id="Seg_9198" s="T786">RUS:ext</ta>
            <ta e="T801" id="Seg_9199" s="T790">RUS:ext</ta>
            <ta e="T808" id="Seg_9200" s="T802">RUS:ext</ta>
            <ta e="T822" id="Seg_9201" s="T809">RUS:ext</ta>
            <ta e="T830" id="Seg_9202" s="T823">RUS:ext</ta>
            <ta e="T838" id="Seg_9203" s="T831">RUS:ext</ta>
            <ta e="T848" id="Seg_9204" s="T839">RUS:ext</ta>
            <ta e="T856" id="Seg_9205" s="T849">RUS:ext</ta>
            <ta e="T862" id="Seg_9206" s="T856">RUS:ext</ta>
            <ta e="T877" id="Seg_9207" s="T863">RUS:ext</ta>
            <ta e="T886" id="Seg_9208" s="T878">RUS:ext</ta>
            <ta e="T902" id="Seg_9209" s="T887">RUS:ext</ta>
            <ta e="T915" id="Seg_9210" s="T903">RUS:ext</ta>
            <ta e="T927" id="Seg_9211" s="T916">RUS:ext</ta>
            <ta e="T937" id="Seg_9212" s="T933">RUS:ext</ta>
            <ta e="T951" id="Seg_9213" s="T938">RUS:ext</ta>
            <ta e="T963" id="Seg_9214" s="T951">RUS:ext</ta>
            <ta e="T970" id="Seg_9215" s="T964">RUS:ext</ta>
            <ta e="T985" id="Seg_9216" s="T971">RUS:ext</ta>
            <ta e="T1000" id="Seg_9217" s="T986">RUS:ext</ta>
            <ta e="T1004" id="Seg_9218" s="T1001">RUS:ext</ta>
            <ta e="T1009" id="Seg_9219" s="T1005">RUS:ext</ta>
            <ta e="T1033" id="Seg_9220" s="T1026">RUS:ext</ta>
            <ta e="T1047" id="Seg_9221" s="T1035">RUS:ext</ta>
            <ta e="T1067" id="Seg_9222" s="T1050">RUS:ext</ta>
            <ta e="T1070" id="Seg_9223" s="T1067">RUS:ext</ta>
            <ta e="T1126" id="Seg_9224" s="T1118">RUS:ext</ta>
            <ta e="T1136" id="Seg_9225" s="T1126">RUS:ext</ta>
            <ta e="T1191" id="Seg_9226" s="T1173">RUS:ext</ta>
            <ta e="T1224" id="Seg_9227" s="T1223">RUS:int</ta>
            <ta e="T1227" id="Seg_9228" s="T1226">RUS:int</ta>
            <ta e="T1230" id="Seg_9229" s="T1229">RUS:int</ta>
            <ta e="T1249" id="Seg_9230" s="T1239">RUS:ext</ta>
            <ta e="T1251" id="Seg_9231" s="T1250">RUS:ext</ta>
            <ta e="T1253" id="Seg_9232" s="T1252">RUS:ext</ta>
            <ta e="T1257" id="Seg_9233" s="T1254">RUS:ext</ta>
            <ta e="T1261" id="Seg_9234" s="T1258">RUS:ext</ta>
            <ta e="T1265" id="Seg_9235" s="T1261">RUS:ext</ta>
            <ta e="T1274" id="Seg_9236" s="T1267">RUS:ext</ta>
            <ta e="T1280" id="Seg_9237" s="T1275">RUS:ext</ta>
            <ta e="T1286" id="Seg_9238" s="T1281">RUS:ext</ta>
            <ta e="T1304" id="Seg_9239" s="T1295">RUS:ext</ta>
            <ta e="T1324" id="Seg_9240" s="T1311">RUS:ext</ta>
            <ta e="T1329" id="Seg_9241" s="T1325">RUS:ext</ta>
            <ta e="T1373" id="Seg_9242" s="T1368">RUS:ext</ta>
            <ta e="T1389" id="Seg_9243" s="T1377">RUS:ext</ta>
            <ta e="T1449" id="Seg_9244" s="T1444">RUS:ext</ta>
            <ta e="T1460" id="Seg_9245" s="T1453">RUS:ext</ta>
            <ta e="T1474" id="Seg_9246" s="T1463">RUS:ext</ta>
            <ta e="T1485" id="Seg_9247" s="T1474">RUS:ext</ta>
            <ta e="T1532" id="Seg_9248" s="T1527">RUS:ext</ta>
            <ta e="T1543" id="Seg_9249" s="T1533">RUS:ext</ta>
            <ta e="T1607" id="Seg_9250" s="T1604">RUS:ext</ta>
            <ta e="T1627" id="Seg_9251" s="T1612">RUS:ext</ta>
            <ta e="T1631" id="Seg_9252" s="T1628">RUS:ext</ta>
            <ta e="T1641" id="Seg_9253" s="T1634">RUS:ext</ta>
            <ta e="T1652" id="Seg_9254" s="T1643">RUS:ext</ta>
            <ta e="T1660" id="Seg_9255" s="T1653">RUS:ext</ta>
            <ta e="T1664" id="Seg_9256" s="T1661">RUS:ext</ta>
            <ta e="T1673" id="Seg_9257" s="T1665">RUS:ext</ta>
            <ta e="T1678" id="Seg_9258" s="T1674">RUS:ext</ta>
            <ta e="T1684" id="Seg_9259" s="T1679">RUS:ext</ta>
            <ta e="T1694" id="Seg_9260" s="T1685">RUS:ext</ta>
            <ta e="T1698" id="Seg_9261" s="T1694">RUS:ext</ta>
            <ta e="T1705" id="Seg_9262" s="T1699">RUS:ext</ta>
            <ta e="T1710" id="Seg_9263" s="T1707">RUS:ext</ta>
            <ta e="T1715" id="Seg_9264" s="T1713">RUS:ext</ta>
            <ta e="T1722" id="Seg_9265" s="T1717">RUS:ext</ta>
            <ta e="T1752" id="Seg_9266" s="T1748">RUS:ext</ta>
            <ta e="T1761" id="Seg_9267" s="T1752">RUS:ext</ta>
            <ta e="T1784" id="Seg_9268" s="T1775">RUS:ext</ta>
            <ta e="T1797" id="Seg_9269" s="T1785">RUS:ext</ta>
            <ta e="T1801" id="Seg_9270" s="T1800">RUS:ext</ta>
            <ta e="T1806" id="Seg_9271" s="T1802">RUS:ext</ta>
            <ta e="T1853" id="Seg_9272" s="T1844">RUS:ext</ta>
            <ta e="T1864" id="Seg_9273" s="T1862">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T324" id="Seg_9274" s="T320">Мой друг мне писал.</ta>
            <ta e="T327" id="Seg_9275" s="T325">Четыре месяца.</ta>
            <ta e="T332" id="Seg_9276" s="T328">"Приезжай, приезжай".</ta>
            <ta e="T335" id="Seg_9277" s="T332">(С кем?) я приеду?</ta>
            <ta e="T339" id="Seg_9278" s="T336">Я ничего не знаю.</ta>
            <ta e="T344" id="Seg_9279" s="T340">Тогда я написала ему письмо.</ta>
            <ta e="T353" id="Seg_9280" s="T345">Сказала: "Приедет ко мне Агафон Иванович, тогда я приеду".</ta>
            <ta e="T361" id="Seg_9281" s="T354">Тогда он мне пишет: "Я сам к вам приеду".</ta>
            <ta e="T369" id="Seg_9282" s="T362">Потом он приехал к нам, я его не ждала.</ta>
            <ta e="T375" id="Seg_9283" s="T370">Ну, моя невестка его привела.</ta>
            <ta e="T380" id="Seg_9284" s="T376">"Вот человек твой приехал".</ta>
            <ta e="T385" id="Seg_9285" s="T381">Я выбежала на улицу.</ta>
            <ta e="T389" id="Seg_9286" s="T386">"Ой, ты быстро приехал".</ta>
            <ta e="T395" id="Seg_9287" s="T389">Потом он пришел [=вошел], поел.</ta>
            <ta e="T398" id="Seg_9288" s="T396">Лег спать.</ta>
            <ta e="T404" id="Seg_9289" s="T399">Потом встал, пошел на реку, на мельницу.</ta>
            <ta e="T410" id="Seg_9290" s="T405">Там тоже нашел таких [людей].</ta>
            <ta e="T413" id="Seg_9291" s="T411">Поговорил там.</ta>
            <ta e="T417" id="Seg_9292" s="T414">Потом пришел домой.</ta>
            <ta e="T421" id="Seg_9293" s="T418">Потом пошел в баню.</ta>
            <ta e="T425" id="Seg_9294" s="T422">Я пошла с ним.</ta>
            <ta e="T435" id="Seg_9295" s="T426">Дала ему горячую и холодную воду, мыло.</ta>
            <ta e="T442" id="Seg_9296" s="T436">Он там помылся, потом пришел ко мне домой.</ta>
            <ta e="T445" id="Seg_9297" s="T443">Я сказала: "Ешь".</ta>
            <ta e="T449" id="Seg_9298" s="T445">"Я там поел, на мельнице".</ta>
            <ta e="T452" id="Seg_9299" s="T450">Не поел.</ta>
            <ta e="T457" id="Seg_9300" s="T453">Потом мы помолились.</ta>
            <ta e="T464" id="Seg_9301" s="T458">Потом он лег, я тоже легла.</ta>
            <ta e="T469" id="Seg_9302" s="T465">Утром я встала, сварила яйца.</ta>
            <ta e="T474" id="Seg_9303" s="T470">Мы поели и пошли на автобус.</ta>
            <ta e="T480" id="Seg_9304" s="T475">Мы там стояли, потом пришел автобус.</ta>
            <ta e="T487" id="Seg_9305" s="T481">Мы сели, поехали в Агинское.</ta>
            <ta e="T492" id="Seg_9306" s="T488">Там мы тоже помолились.</ta>
            <ta e="T499" id="Seg_9307" s="T493">В пять часов мы поехали в Уяр.</ta>
            <ta e="T503" id="Seg_9308" s="T500">Там переночевали.</ta>
            <ta e="T510" id="Seg_9309" s="T504">Там тоже есть (мой народ?).</ta>
            <ta e="T515" id="Seg_9310" s="T511">Тогда, тогда.</ta>
            <ta e="T522" id="Seg_9311" s="T516">Потом мы встали рано и поехали в Красноярск.</ta>
            <ta e="T531" id="Seg_9312" s="T523">Там мы поехали туда, где летают, сели и улетели.</ta>
            <ta e="T536" id="Seg_9313" s="T532">В Новосибирск, там мы тоже переночевали.</ta>
            <ta e="T544" id="Seg_9314" s="T537">Потом мы опять сели [в самолет] и прилетели в Москву.</ta>
            <ta e="T555" id="Seg_9315" s="T545">Там мы приземлились, потом сели в машину и поехали в Москву.</ta>
            <ta e="T559" id="Seg_9316" s="T555">Там меня сфотографировали. [?]</ta>
            <ta e="T564" id="Seg_9317" s="T560">Там тоже были люди.</ta>
            <ta e="T576" id="Seg_9318" s="T565">Потом мы вернулись [в аэропорт], сели [в самолет] и прилетели в этот город.</ta>
            <ta e="T582" id="Seg_9319" s="T577">Потом оттуда мы приехали [в] Тарту.</ta>
            <ta e="T587" id="Seg_9320" s="T583">Я там жила три недели.</ta>
            <ta e="T596" id="Seg_9321" s="T588">Взял меня опять, и мы поехали туда, где много (?).</ta>
            <ta e="T601" id="Seg_9322" s="T597">Там было много народа.</ta>
            <ta e="T607" id="Seg_9323" s="T602">Немцы были, и украинцы были.</ta>
            <ta e="T616" id="Seg_9324" s="T608">Там я долго была, две ночи переночевала.</ta>
            <ta e="T620" id="Seg_9325" s="T616">Потом [он] снова меня сюда привез.</ta>
            <ta e="T623" id="Seg_9326" s="T621">К этой женщине.</ta>
            <ta e="T627" id="Seg_9327" s="T624">И я здесь живу.</ta>
            <ta e="T629" id="Seg_9328" s="T628">Хватит.</ta>
            <ta e="T1227" id="Seg_9329" s="T1219">Вот Арпит приехал, а мой племянник не отпускал меня.</ta>
            <ta e="T1233" id="Seg_9330" s="T1228">А невестка говорит: "Поезжай, поезжай".</ta>
            <ta e="T1238" id="Seg_9331" s="T1234">Тогда я поехала сюда.</ta>
            <ta e="T1729" id="Seg_9332" s="T1723">Вчера мы ходили в гости к одной женщине.</ta>
            <ta e="T1734" id="Seg_9333" s="T1730">Мы сели в автобус, приехали туда.</ta>
            <ta e="T1740" id="Seg_9334" s="T1734">Туда пришли люди, чтобы говорить на моем языке.</ta>
            <ta e="T1745" id="Seg_9335" s="T1740">Я им всё говорила, говорила.</ta>
            <ta e="T1747" id="Seg_9336" s="T1746">Потом…</ta>
            <ta e="T1874" id="Seg_9337" s="T1865">Когда я собиралась ехать сюда, люди говорили: "Не езжай.</ta>
            <ta e="T1877" id="Seg_9338" s="T1875">Ты там умрешь.</ta>
            <ta e="T1882" id="Seg_9339" s="T1878">Я сказала: "Я не боюсь.</ta>
            <ta e="T1890" id="Seg_9340" s="T1883">Там меня тоже похоронят.</ta>
            <ta e="T1892" id="Seg_9341" s="T1891">Всё.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T324" id="Seg_9342" s="T320">My friend wrote to me.</ta>
            <ta e="T327" id="Seg_9343" s="T325">Four years.</ta>
            <ta e="T332" id="Seg_9344" s="T328">"Come here, come here."</ta>
            <ta e="T335" id="Seg_9345" s="T332">(With whom?) will I come?</ta>
            <ta e="T339" id="Seg_9346" s="T336">I don't know anything.</ta>
            <ta e="T344" id="Seg_9347" s="T340">Then I wrote him a letter.</ta>
            <ta e="T353" id="Seg_9348" s="T345">I said: "If Agafon Ivanovich comes to me, then I'll come."</ta>
            <ta e="T361" id="Seg_9349" s="T354">Then he writes to me: "I'll come to you myself."</ta>
            <ta e="T369" id="Seg_9350" s="T362">Then he came to us, I wasn't waiting for him.</ta>
            <ta e="T375" id="Seg_9351" s="T370">My daughter-in-law brought him.</ta>
            <ta e="T380" id="Seg_9352" s="T376">"Look, your man's come."</ta>
            <ta e="T385" id="Seg_9353" s="T381">I ran out.</ta>
            <ta e="T389" id="Seg_9354" s="T386">"Oh, you came fast."</ta>
            <ta e="T395" id="Seg_9355" s="T389">Then he came [in], ate.</ta>
            <ta e="T398" id="Seg_9356" s="T396">He went to bed.</ta>
            <ta e="T404" id="Seg_9357" s="T399">Then he got up, went to the river, to the mill.</ta>
            <ta e="T410" id="Seg_9358" s="T405">There he found such [people], too.</ta>
            <ta e="T413" id="Seg_9359" s="T411">There he spoke [a few].</ta>
            <ta e="T417" id="Seg_9360" s="T414">Then he came home.</ta>
            <ta e="T421" id="Seg_9361" s="T418">Then he went to the baths.</ta>
            <ta e="T425" id="Seg_9362" s="T422">I went with him.</ta>
            <ta e="T435" id="Seg_9363" s="T426">I gave him hot and cold water, soap.</ta>
            <ta e="T442" id="Seg_9364" s="T436">He washed there, then came to my place.</ta>
            <ta e="T445" id="Seg_9365" s="T443">I said: "Eat."</ta>
            <ta e="T449" id="Seg_9366" s="T445">"I've eaten there, on the mill."</ta>
            <ta e="T452" id="Seg_9367" s="T450">He didn't eat.</ta>
            <ta e="T457" id="Seg_9368" s="T453">Then we prayed to God.</ta>
            <ta e="T464" id="Seg_9369" s="T458">Then he went to sleep, I went to sleep, too.</ta>
            <ta e="T469" id="Seg_9370" s="T465">In the morning, I got up, I cooked eggs.</ta>
            <ta e="T474" id="Seg_9371" s="T470">We ate, and went to the bus.</ta>
            <ta e="T480" id="Seg_9372" s="T475">We stood there, then a bus came.</ta>
            <ta e="T487" id="Seg_9373" s="T481">We got in and went to Aginskoye.</ta>
            <ta e="T492" id="Seg_9374" s="T488">There [we] prayed to God, too.</ta>
            <ta e="T499" id="Seg_9375" s="T493">At five o'clock, we went off to Uyar.</ta>
            <ta e="T503" id="Seg_9376" s="T500">We passed the night there.</ta>
            <ta e="T510" id="Seg_9377" s="T504">There are (my friends?), too.</ta>
            <ta e="T515" id="Seg_9378" s="T511">Then, then.</ta>
            <ta e="T522" id="Seg_9379" s="T516">Then we got up early and went off to Krasnoyarsk.</ta>
            <ta e="T531" id="Seg_9380" s="T523">There we went where people fly, got in and went off.</ta>
            <ta e="T536" id="Seg_9381" s="T532">To Novosibirsk, there we passed a night, too.</ta>
            <ta e="T544" id="Seg_9382" s="T537">Then we took [a plane] again and came to Moscow.</ta>
            <ta e="T555" id="Seg_9383" s="T545">There we landed, then took a car and went to Moscow.</ta>
            <ta e="T559" id="Seg_9384" s="T555">There I got photographed. [?]</ta>
            <ta e="T564" id="Seg_9385" s="T560">There were [some] people, too.</ta>
            <ta e="T576" id="Seg_9386" s="T565">Then we came back [to the airport], got in [a plane] and came to this town.</ta>
            <ta e="T582" id="Seg_9387" s="T577">Then from there we came [to] Tartu.</ta>
            <ta e="T587" id="Seg_9388" s="T583">I lived there three weeks.</ta>
            <ta e="T596" id="Seg_9389" s="T588">[He] took me again, and we went where there were many (?).</ta>
            <ta e="T601" id="Seg_9390" s="T597">There were many people.</ta>
            <ta e="T607" id="Seg_9391" s="T602">There were many Germans and Ukrainians.</ta>
            <ta e="T616" id="Seg_9392" s="T608">There I was for a long time, I spent two nights there.</ta>
            <ta e="T620" id="Seg_9393" s="T616">Then [he] brought me here again.</ta>
            <ta e="T623" id="Seg_9394" s="T621">To this woman.</ta>
            <ta e="T627" id="Seg_9395" s="T624">And I'm living here.</ta>
            <ta e="T629" id="Seg_9396" s="T628">That's all.</ta>
            <ta e="T1227" id="Seg_9397" s="T1219">So, Arpit came, and my nephew didn't let me go.</ta>
            <ta e="T1233" id="Seg_9398" s="T1228">But my daughter-in-law said: "Go."</ta>
            <ta e="T1238" id="Seg_9399" s="T1234">So I went here.</ta>
            <ta e="T1729" id="Seg_9400" s="T1723">Yesterday we went to visit a woman.</ta>
            <ta e="T1734" id="Seg_9401" s="T1730">We took a bus and came there.</ta>
            <ta e="T1740" id="Seg_9402" s="T1734">People came there to speak my language.</ta>
            <ta e="T1745" id="Seg_9403" s="T1740">I was telling them everything.</ta>
            <ta e="T1747" id="Seg_9404" s="T1746">Then…</ta>
            <ta e="T1874" id="Seg_9405" s="T1865">As I was going to come here, people was saying: "Don't go.</ta>
            <ta e="T1877" id="Seg_9406" s="T1875">You'll die there.</ta>
            <ta e="T1882" id="Seg_9407" s="T1878">I said: "I'm not afraid.</ta>
            <ta e="T1890" id="Seg_9408" s="T1883">There they'll bury me, too.</ta>
            <ta e="T1892" id="Seg_9409" s="T1891">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T324" id="Seg_9410" s="T320">Mein Freund hat mir noch geschrieben.</ta>
            <ta e="T327" id="Seg_9411" s="T325">Vier Jahre.</ta>
            <ta e="T332" id="Seg_9412" s="T328">"Komm her, komm her."</ta>
            <ta e="T335" id="Seg_9413" s="T332">(Mit wem?) werde ich kommen?</ta>
            <ta e="T339" id="Seg_9414" s="T336">Ich weiß nichts.</ta>
            <ta e="T344" id="Seg_9415" s="T340">Dann habe ich ihm einen Brief geschrieben.</ta>
            <ta e="T353" id="Seg_9416" s="T345">Ich sagte: "Wenn Agaon Ivanovich zu mir kommt, dann werde ich kommen."</ta>
            <ta e="T361" id="Seg_9417" s="T354">Dann schreibt er mir: "Ich werde selbst zu dir kommen."</ta>
            <ta e="T369" id="Seg_9418" s="T362">Dann kam er zu uns, ich habe nicht auf ihn gewartet.</ta>
            <ta e="T375" id="Seg_9419" s="T370">Meine Schwiegertochter hat ihn gebracht.</ta>
            <ta e="T380" id="Seg_9420" s="T376">"Sieh, dein Mann ist gekommen."</ta>
            <ta e="T385" id="Seg_9421" s="T381">Ich rannte raus.</ta>
            <ta e="T389" id="Seg_9422" s="T386">"Oh, du bist schnell gekommen."</ta>
            <ta e="T395" id="Seg_9423" s="T389">Dann kam er [rein], aß. </ta>
            <ta e="T398" id="Seg_9424" s="T396">Er ging schlafen.</ta>
            <ta e="T404" id="Seg_9425" s="T399">Dann stand er auf, ging zum Fluss, zur Mühle. </ta>
            <ta e="T410" id="Seg_9426" s="T405">Dort fand er auch solche [Leute].</ta>
            <ta e="T413" id="Seg_9427" s="T411">Dort sprach er. </ta>
            <ta e="T417" id="Seg_9428" s="T414">Dann kam er nach Hause.</ta>
            <ta e="T421" id="Seg_9429" s="T418">Dann ging er zur Sauna. </ta>
            <ta e="T425" id="Seg_9430" s="T422">Ich ging mit ihm.</ta>
            <ta e="T435" id="Seg_9431" s="T426">Ich gab ihm heißes und kaltes Wasser, Seife.</ta>
            <ta e="T442" id="Seg_9432" s="T436">Er wusch sich dort, dann ist er zu mir gekommen.</ta>
            <ta e="T445" id="Seg_9433" s="T443">Ich sagte: "Iss."</ta>
            <ta e="T449" id="Seg_9434" s="T445">"Ich habe dort gegessen, an der Mühle."</ta>
            <ta e="T452" id="Seg_9435" s="T450">Er hat nicht gegessen.</ta>
            <ta e="T457" id="Seg_9436" s="T453">Dann beteten wir. </ta>
            <ta e="T464" id="Seg_9437" s="T458">Dan ging er schlafen, ich ging auch schlafen.</ta>
            <ta e="T469" id="Seg_9438" s="T465">Am morgen stand ich auf, ich kochte Eier.</ta>
            <ta e="T474" id="Seg_9439" s="T470">Wir aßen und gingen dann zum Bus. </ta>
            <ta e="T480" id="Seg_9440" s="T475">Wir standen dort, dann kam der Bus.</ta>
            <ta e="T487" id="Seg_9441" s="T481">We stiegen ein und fuhren nach Aginskoye. </ta>
            <ta e="T492" id="Seg_9442" s="T488">Dort beteten [wir] uns auch. </ta>
            <ta e="T499" id="Seg_9443" s="T493">Um fünf Uhr fuhren wir los nach Uyar. </ta>
            <ta e="T503" id="Seg_9444" s="T500">Wir haben die Nacht dort verbracht. </ta>
            <ta e="T510" id="Seg_9445" s="T504">Dort sind auch (meine Freunde?). </ta>
            <ta e="T515" id="Seg_9446" s="T511">Dann, dann. </ta>
            <ta e="T522" id="Seg_9447" s="T516">Dann standen wir früh auf und fuhren nach Krasnoyarsk.</ta>
            <ta e="T531" id="Seg_9448" s="T523">Dort gingen wir dorthin wie Leute fliegen, stiegen ein und flogen los. </ta>
            <ta e="T536" id="Seg_9449" s="T532">Nach Novosibirsk, dort haben wir auch eine Nacht verbracht. </ta>
            <ta e="T544" id="Seg_9450" s="T537">Dann nahmen wir wieder [ein Flugzeug] und kamen nach Moskau. </ta>
            <ta e="T555" id="Seg_9451" s="T545">Dort sind wir gelandet, dann haben wir ein Auto genommen und sind nach Moskau gefahren. </ta>
            <ta e="T559" id="Seg_9452" s="T555">Dort wurde ich fotografiert. [?]</ta>
            <ta e="T564" id="Seg_9453" s="T560">Dort waren auch [einige] Leute.</ta>
            <ta e="T576" id="Seg_9454" s="T565">Dann kamen wir zurück [zum Flughafen], stiegen in [ein Flugzeug] und kamen in diese Stadt. </ta>
            <ta e="T582" id="Seg_9455" s="T577">Von dort kamen wir [nach] Tartu.</ta>
            <ta e="T587" id="Seg_9456" s="T583">Ich wohnte dort drei Wochen lang.</ta>
            <ta e="T596" id="Seg_9457" s="T588">[Er] nahm mich wieder und wir fuhren dorthin wo viele (?).</ta>
            <ta e="T601" id="Seg_9458" s="T597">Dort waren viele Menschen.</ta>
            <ta e="T607" id="Seg_9459" s="T602">Dort waren viele Deutsche und Ukrainer.</ta>
            <ta e="T616" id="Seg_9460" s="T608">Dort war ich lange, ich habe zwei Nächte dort verbracht.</ta>
            <ta e="T620" id="Seg_9461" s="T616">Dann brachte [er] mich wieder hierher.</ta>
            <ta e="T623" id="Seg_9462" s="T621">Zu dieser Frau.</ta>
            <ta e="T627" id="Seg_9463" s="T624">Und hier lebe ich.</ta>
            <ta e="T629" id="Seg_9464" s="T628">Das ist alles.</ta>
            <ta e="T1227" id="Seg_9465" s="T1219">Also kam Arpit und mein Neffe ließ mich nicht gehen.</ta>
            <ta e="T1233" id="Seg_9466" s="T1228">Aber meine Schwiegertochter sagte: "Geh."</ta>
            <ta e="T1238" id="Seg_9467" s="T1234">Also ging ich dorthin.</ta>
            <ta e="T1729" id="Seg_9468" s="T1723">Gestern haben wir eine Frau besucht.</ta>
            <ta e="T1734" id="Seg_9469" s="T1730">Wir nahmen einen Bus und fuhren hierher. </ta>
            <ta e="T1740" id="Seg_9470" s="T1734">Menschen kamen, um meine Sprache zu sprachen.</ta>
            <ta e="T1745" id="Seg_9471" s="T1740">Ich habe ihnen alles gesagt.</ta>
            <ta e="T1747" id="Seg_9472" s="T1746">Dann…</ta>
            <ta e="T1874" id="Seg_9473" s="T1865">Als ich dabei war hierher zu kommen, sagte die Leute: "Geh nicht. </ta>
            <ta e="T1877" id="Seg_9474" s="T1875">Du wirst dort sterben."</ta>
            <ta e="T1882" id="Seg_9475" s="T1878">Ich sagte: "Ich habe keine Angst.</ta>
            <ta e="T1890" id="Seg_9476" s="T1883">Dort werden sie mich auch beerdigen.</ta>
            <ta e="T1892" id="Seg_9477" s="T1891">Das ist alles.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T457" id="Seg_9478" s="T453">[GVY:] üzəbiʔibeʔ.</ta>
            <ta e="T522" id="Seg_9479" s="T516">[GVY:] The phonologization "Krasnăjarskagən" would be also possible. Cf. the Russian version of this story below.</ta>
            <ta e="T531" id="Seg_9480" s="T523">[GVY:] that is, they went to the airport and took a plane.</ta>
            <ta e="T536" id="Seg_9481" s="T532">[GVY:] The phonologization "Năvăsibirʼskənə" would be impossible, because one would expect the pronounciation [-ke:nə].</ta>
            <ta e="T582" id="Seg_9482" s="T577">[GVY:] dogəʔ as an Ablative form?</ta>
            <ta e="T623" id="Seg_9483" s="T621">[GVY:] A rare instance of the agreement in case?</ta>
            <ta e="T848" id="Seg_9484" s="T839">[GVY:] NOM Красноярское?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T9" id="Seg_9485" n="sc" s="T7">
               <ts e="T9" id="Seg_9487" n="HIAT:u" s="T7">
                  <ts e="T9" id="Seg_9489" n="HIAT:w" s="T7">А</ts>
                  <nts id="Seg_9490" n="HIAT:ip">.</nts>
                  <nts id="Seg_9491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_9492" n="sc" s="T11">
               <ts e="T25" id="Seg_9494" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_9496" n="HIAT:w" s="T11">Вот</ts>
                  <nts id="Seg_9497" n="HIAT:ip">,</nts>
                  <nts id="Seg_9498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_9500" n="HIAT:w" s="T12">и</ts>
                  <nts id="Seg_9501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_9503" n="HIAT:w" s="T13">испортилась</ts>
                  <nts id="Seg_9504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_9506" n="HIAT:w" s="T14">у</ts>
                  <nts id="Seg_9507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_9509" n="HIAT:w" s="T15">него</ts>
                  <nts id="Seg_9510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_9512" n="HIAT:w" s="T16">в</ts>
                  <nts id="Seg_9513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_9515" n="HIAT:w" s="T17">общем</ts>
                  <nts id="Seg_9516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_9518" n="HIAT:w" s="T18">машина</ts>
                  <nts id="Seg_9519" n="HIAT:ip">,</nts>
                  <nts id="Seg_9520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_9522" n="HIAT:w" s="T19">а</ts>
                  <nts id="Seg_9523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_9525" n="HIAT:w" s="T20">у</ts>
                  <nts id="Seg_9526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_9528" n="HIAT:w" s="T21">меня</ts>
                  <nts id="Seg_9529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_9531" n="HIAT:w" s="T22">не</ts>
                  <nts id="Seg_9532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_9534" n="HIAT:w" s="T23">испортилась</ts>
                  <nts id="Seg_9535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_9537" n="HIAT:w" s="T24">никогда</ts>
                  <nts id="Seg_9538" n="HIAT:ip">.</nts>
                  <nts id="Seg_9539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_9540" n="sc" s="T26">
               <ts e="T27" id="Seg_9542" n="HIAT:u" s="T26">
                  <nts id="Seg_9543" n="HIAT:ip">(</nts>
                  <nts id="Seg_9544" n="HIAT:ip">(</nts>
                  <ats e="T27" id="Seg_9545" n="HIAT:non-pho" s="T26">…</ats>
                  <nts id="Seg_9546" n="HIAT:ip">)</nts>
                  <nts id="Seg_9547" n="HIAT:ip">)</nts>
                  <nts id="Seg_9548" n="HIAT:ip">.</nts>
                  <nts id="Seg_9549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_9550" n="sc" s="T28">
               <ts e="T29" id="Seg_9552" n="HIAT:u" s="T28">
                  <nts id="Seg_9553" n="HIAT:ip">(</nts>
                  <nts id="Seg_9554" n="HIAT:ip">(</nts>
                  <ats e="T29" id="Seg_9555" n="HIAT:non-pho" s="T28">…</ats>
                  <nts id="Seg_9556" n="HIAT:ip">)</nts>
                  <nts id="Seg_9557" n="HIAT:ip">)</nts>
                  <nts id="Seg_9558" n="HIAT:ip">.</nts>
                  <nts id="Seg_9559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_9560" n="sc" s="T30">
               <ts e="T32" id="Seg_9562" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_9564" n="HIAT:w" s="T30">Еще</ts>
                  <nts id="Seg_9565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_9567" n="HIAT:w" s="T31">какой</ts>
                  <nts id="Seg_9568" n="HIAT:ip">.</nts>
                  <nts id="Seg_9569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T40" id="Seg_9570" n="sc" s="T33">
               <ts e="T40" id="Seg_9572" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_9574" n="HIAT:w" s="T33">Часы</ts>
                  <nts id="Seg_9575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_9577" n="HIAT:w" s="T34">не</ts>
                  <nts id="Seg_9578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_9580" n="HIAT:w" s="T35">любит</ts>
                  <nts id="Seg_9581" n="HIAT:ip">,</nts>
                  <nts id="Seg_9582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_9584" n="HIAT:w" s="T36">говорит</ts>
                  <nts id="Seg_9585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_9587" n="HIAT:w" s="T37">все</ts>
                  <nts id="Seg_9588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_9590" n="HIAT:w" s="T38">будет</ts>
                  <nts id="Seg_9591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_9593" n="HIAT:w" s="T39">слышно</ts>
                  <nts id="Seg_9594" n="HIAT:ip">.</nts>
                  <nts id="Seg_9595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_9596" n="sc" s="T41">
               <ts e="T42" id="Seg_9598" n="HIAT:u" s="T41">
                  <nts id="Seg_9599" n="HIAT:ip">(</nts>
                  <nts id="Seg_9600" n="HIAT:ip">(</nts>
                  <ats e="T42" id="Seg_9601" n="HIAT:non-pho" s="T41">…</ats>
                  <nts id="Seg_9602" n="HIAT:ip">)</nts>
                  <nts id="Seg_9603" n="HIAT:ip">)</nts>
                  <nts id="Seg_9604" n="HIAT:ip">.</nts>
                  <nts id="Seg_9605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_9606" n="sc" s="T48">
               <ts e="T49" id="Seg_9608" n="HIAT:u" s="T48">
                  <nts id="Seg_9609" n="HIAT:ip">(</nts>
                  <nts id="Seg_9610" n="HIAT:ip">(</nts>
                  <ats e="T49" id="Seg_9611" n="HIAT:non-pho" s="T48">…</ats>
                  <nts id="Seg_9612" n="HIAT:ip">)</nts>
                  <nts id="Seg_9613" n="HIAT:ip">)</nts>
                  <nts id="Seg_9614" n="HIAT:ip">.</nts>
                  <nts id="Seg_9615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_9616" n="sc" s="T50">
               <ts e="T51" id="Seg_9618" n="HIAT:u" s="T50">
                  <nts id="Seg_9619" n="HIAT:ip">(</nts>
                  <nts id="Seg_9620" n="HIAT:ip">(</nts>
                  <ats e="T51" id="Seg_9621" n="HIAT:non-pho" s="T50">…</ats>
                  <nts id="Seg_9622" n="HIAT:ip">)</nts>
                  <nts id="Seg_9623" n="HIAT:ip">)</nts>
                  <nts id="Seg_9624" n="HIAT:ip">.</nts>
                  <nts id="Seg_9625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_9626" n="sc" s="T52">
               <ts e="T53" id="Seg_9628" n="HIAT:u" s="T52">
                  <nts id="Seg_9629" n="HIAT:ip">(</nts>
                  <nts id="Seg_9630" n="HIAT:ip">(</nts>
                  <ats e="T53" id="Seg_9631" n="HIAT:non-pho" s="T52">…</ats>
                  <nts id="Seg_9632" n="HIAT:ip">)</nts>
                  <nts id="Seg_9633" n="HIAT:ip">)</nts>
                  <nts id="Seg_9634" n="HIAT:ip">.</nts>
                  <nts id="Seg_9635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T58" id="Seg_9636" n="sc" s="T54">
               <ts e="T58" id="Seg_9638" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_9640" n="HIAT:w" s="T54">Значит</ts>
                  <nts id="Seg_9641" n="HIAT:ip">,</nts>
                  <nts id="Seg_9642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_9644" n="HIAT:w" s="T55">мы</ts>
                  <nts id="Seg_9645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_9647" n="HIAT:w" s="T56">сделаем</ts>
                  <nts id="Seg_9648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_9650" n="HIAT:w" s="T57">так</ts>
                  <nts id="Seg_9651" n="HIAT:ip">.</nts>
                  <nts id="Seg_9652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T71" id="Seg_9653" n="sc" s="T59">
               <ts e="T71" id="Seg_9655" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_9657" n="HIAT:w" s="T59">Я</ts>
                  <nts id="Seg_9658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_9660" n="HIAT:w" s="T60">сначала</ts>
                  <nts id="Seg_9661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_9663" n="HIAT:w" s="T61">пару</ts>
                  <nts id="Seg_9664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_9666" n="HIAT:w" s="T62">слов</ts>
                  <nts id="Seg_9667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_9669" n="HIAT:w" s="T63">сам</ts>
                  <nts id="Seg_9670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_9672" n="HIAT:w" s="T64">скажу</ts>
                  <nts id="Seg_9673" n="HIAT:ip">,</nts>
                  <nts id="Seg_9674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_9676" n="HIAT:w" s="T65">чтобы</ts>
                  <nts id="Seg_9677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_9679" n="HIAT:w" s="T66">знать</ts>
                  <nts id="Seg_9680" n="HIAT:ip">,</nts>
                  <nts id="Seg_9681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_9683" n="HIAT:w" s="T67">какая</ts>
                  <nts id="Seg_9684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_9686" n="HIAT:w" s="T68">пленка</ts>
                  <nts id="Seg_9687" n="HIAT:ip">,</nts>
                  <nts id="Seg_9688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_9690" n="HIAT:w" s="T69">что</ts>
                  <nts id="Seg_9691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_9693" n="HIAT:w" s="T70">здесь</ts>
                  <nts id="Seg_9694" n="HIAT:ip">.</nts>
                  <nts id="Seg_9695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T80" id="Seg_9696" n="sc" s="T72">
               <ts e="T80" id="Seg_9698" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_9700" n="HIAT:w" s="T72">А</ts>
                  <nts id="Seg_9701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_9703" n="HIAT:w" s="T73">потом</ts>
                  <nts id="Seg_9704" n="HIAT:ip">,</nts>
                  <nts id="Seg_9705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_9707" n="HIAT:w" s="T74">будь</ts>
                  <nts id="Seg_9708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_9710" n="HIAT:w" s="T75">добра</ts>
                  <nts id="Seg_9711" n="HIAT:ip">,</nts>
                  <nts id="Seg_9712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_9714" n="HIAT:w" s="T76">расскажи</ts>
                  <nts id="Seg_9715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_9717" n="HIAT:w" s="T77">о</ts>
                  <nts id="Seg_9718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_9720" n="HIAT:w" s="T78">своей</ts>
                  <nts id="Seg_9721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_9723" n="HIAT:w" s="T79">поездке</ts>
                  <nts id="Seg_9724" n="HIAT:ip">.</nts>
                  <nts id="Seg_9725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T93" id="Seg_9726" n="sc" s="T81">
               <ts e="T93" id="Seg_9728" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_9730" n="HIAT:w" s="T81">Хоть</ts>
                  <nts id="Seg_9731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_9733" n="HIAT:w" s="T82">как</ts>
                  <nts id="Seg_9734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_9736" n="HIAT:w" s="T83">сначала</ts>
                  <nts id="Seg_9737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_9739" n="HIAT:w" s="T84">все</ts>
                  <nts id="Seg_9740" n="HIAT:ip">,</nts>
                  <nts id="Seg_9741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_9743" n="HIAT:w" s="T85">как</ts>
                  <nts id="Seg_9744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_9746" n="HIAT:w" s="T86">он</ts>
                  <nts id="Seg_9747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_9749" n="HIAT:w" s="T87">написал</ts>
                  <nts id="Seg_9750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_9752" n="HIAT:w" s="T88">тебе</ts>
                  <nts id="Seg_9753" n="HIAT:ip">,</nts>
                  <nts id="Seg_9754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_9756" n="HIAT:w" s="T89">как</ts>
                  <nts id="Seg_9757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_9759" n="HIAT:w" s="T90">потом</ts>
                  <nts id="Seg_9760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_9762" n="HIAT:w" s="T91">приехал</ts>
                  <nts id="Seg_9763" n="HIAT:ip">.</nts>
                  <nts id="Seg_9764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T99" id="Seg_9765" n="sc" s="T94">
               <ts e="T99" id="Seg_9767" n="HIAT:u" s="T94">
                  <ts e="T96" id="Seg_9769" n="HIAT:w" s="T94">Как</ts>
                  <nts id="Seg_9770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_9772" n="HIAT:w" s="T96">полетели</ts>
                  <nts id="Seg_9773" n="HIAT:ip">,</nts>
                  <nts id="Seg_9774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_9776" n="HIAT:w" s="T98">всё-всё</ts>
                  <nts id="Seg_9777" n="HIAT:ip">.</nts>
                  <nts id="Seg_9778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T110" id="Seg_9779" n="sc" s="T100">
               <ts e="T110" id="Seg_9781" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_9783" n="HIAT:w" s="T100">А</ts>
                  <nts id="Seg_9784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_9786" n="HIAT:w" s="T101">потом</ts>
                  <nts id="Seg_9787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_9789" n="HIAT:w" s="T104">маленько</ts>
                  <nts id="Seg_9790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_9792" n="HIAT:w" s="T106">по-русски</ts>
                  <nts id="Seg_9793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_9795" n="HIAT:w" s="T108">можно</ts>
                  <nts id="Seg_9796" n="HIAT:ip">.</nts>
                  <nts id="Seg_9797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T133" id="Seg_9798" n="sc" s="T118">
               <ts e="T133" id="Seg_9800" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_9802" n="HIAT:w" s="T118">Ну</ts>
                  <nts id="Seg_9803" n="HIAT:ip">,</nts>
                  <nts id="Seg_9804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_9806" n="HIAT:w" s="T119">я</ts>
                  <nts id="Seg_9807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_9809" n="HIAT:w" s="T121">в</ts>
                  <nts id="Seg_9810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_9812" n="HIAT:w" s="T123">Финляндии</ts>
                  <nts id="Seg_9813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_9815" n="HIAT:w" s="T125">сидел</ts>
                  <nts id="Seg_9816" n="HIAT:ip">,</nts>
                  <nts id="Seg_9817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_9819" n="HIAT:w" s="T126">работал</ts>
                  <nts id="Seg_9820" n="HIAT:ip">,</nts>
                  <nts id="Seg_9821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_9823" n="HIAT:w" s="T127">там</ts>
                  <nts id="Seg_9824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_9826" n="HIAT:w" s="T128">никак</ts>
                  <nts id="Seg_9827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_9829" n="HIAT:w" s="T129">не</ts>
                  <nts id="Seg_9830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_9832" n="HIAT:w" s="T131">приехать</ts>
                  <nts id="Seg_9833" n="HIAT:ip">.</nts>
                  <nts id="Seg_9834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T162" id="Seg_9835" n="sc" s="T158">
               <ts e="T162" id="Seg_9837" n="HIAT:u" s="T158">
                  <ts e="T160" id="Seg_9839" n="HIAT:w" s="T158">Ну</ts>
                  <nts id="Seg_9840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_9842" n="HIAT:w" s="T160">сейчас</ts>
                  <nts id="Seg_9843" n="HIAT:ip">…</nts>
                  <nts id="Seg_9844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T185" id="Seg_9845" n="sc" s="T178">
               <ts e="T185" id="Seg_9847" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_9849" n="HIAT:w" s="T178">Ну</ts>
                  <nts id="Seg_9850" n="HIAT:ip">,</nts>
                  <nts id="Seg_9851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_9853" n="HIAT:w" s="T179">сейчас</ts>
                  <nts id="Seg_9854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_9856" n="HIAT:w" s="T180">это</ts>
                  <nts id="Seg_9857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_9859" n="HIAT:w" s="T182">все</ts>
                  <nts id="Seg_9860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_9862" n="HIAT:w" s="T184">расскажешь</ts>
                  <nts id="Seg_9863" n="HIAT:ip">.</nts>
                  <nts id="Seg_9864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T221" id="Seg_9865" n="sc" s="T210">
               <ts e="T221" id="Seg_9867" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_9869" n="HIAT:w" s="T210">А</ts>
                  <nts id="Seg_9870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_9872" n="HIAT:w" s="T211">он</ts>
                  <nts id="Seg_9873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_9875" n="HIAT:w" s="T212">ночевал</ts>
                  <nts id="Seg_9876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_9878" n="HIAT:w" s="T213">у</ts>
                  <nts id="Seg_9879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_9881" n="HIAT:w" s="T215">тебя</ts>
                  <nts id="Seg_9882" n="HIAT:ip">,</nts>
                  <nts id="Seg_9883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_9885" n="HIAT:w" s="T216">прямо</ts>
                  <nts id="Seg_9886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_9888" n="HIAT:w" s="T217">у</ts>
                  <nts id="Seg_9889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_9891" n="HIAT:w" s="T218">тебя</ts>
                  <nts id="Seg_9892" n="HIAT:ip">,</nts>
                  <nts id="Seg_9893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_9895" n="HIAT:w" s="T219">да</ts>
                  <nts id="Seg_9896" n="HIAT:ip">?</nts>
                  <nts id="Seg_9897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_9898" n="sc" s="T222">
               <ts e="T223" id="Seg_9900" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_9902" n="HIAT:w" s="T222">Ага</ts>
                  <nts id="Seg_9903" n="HIAT:ip">.</nts>
                  <nts id="Seg_9904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_9906" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_9908" n="HIAT:w" s="T223">Ага</ts>
                  <nts id="Seg_9909" n="HIAT:ip">,</nts>
                  <nts id="Seg_9910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_9912" n="HIAT:w" s="T224">ага</ts>
                  <nts id="Seg_9913" n="HIAT:ip">.</nts>
                  <nts id="Seg_9914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_9915" n="sc" s="T233">
               <ts e="T243" id="Seg_9917" n="HIAT:u" s="T233">
                  <ts e="T235" id="Seg_9919" n="HIAT:w" s="T233">В</ts>
                  <nts id="Seg_9920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_9922" n="HIAT:w" s="T235">ту</ts>
                  <nts id="Seg_9923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_9925" n="HIAT:w" s="T236">баню</ts>
                  <nts id="Seg_9926" n="HIAT:ip">,</nts>
                  <nts id="Seg_9927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_9929" n="HIAT:w" s="T237">в</ts>
                  <nts id="Seg_9930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_9932" n="HIAT:w" s="T239">которую</ts>
                  <nts id="Seg_9933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_9935" n="HIAT:w" s="T240">я</ts>
                  <nts id="Seg_9936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_9938" n="HIAT:w" s="T241">ходил</ts>
                  <nts id="Seg_9939" n="HIAT:ip">.</nts>
                  <nts id="Seg_9940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T278" id="Seg_9941" n="sc" s="T276">
               <ts e="T278" id="Seg_9943" n="HIAT:u" s="T276">
                  <ts e="T278" id="Seg_9945" n="HIAT:w" s="T276">Хорошо</ts>
                  <nts id="Seg_9946" n="HIAT:ip">.</nts>
                  <nts id="Seg_9947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T312" id="Seg_9948" n="sc" s="T297">
               <ts e="T311" id="Seg_9950" n="HIAT:u" s="T297">
                  <ts e="T299" id="Seg_9952" n="HIAT:w" s="T297">Сейчас</ts>
                  <nts id="Seg_9953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_9955" n="HIAT:w" s="T299">поговорим</ts>
                  <nts id="Seg_9956" n="HIAT:ip">,</nts>
                  <nts id="Seg_9957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_9959" n="HIAT:w" s="T301">только</ts>
                  <nts id="Seg_9960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_9962" n="HIAT:w" s="T302">мне</ts>
                  <nts id="Seg_9963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_9965" n="HIAT:w" s="T303">придется</ts>
                  <nts id="Seg_9966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_9968" n="HIAT:w" s="T304">сначала</ts>
                  <nts id="Seg_9969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_9971" n="HIAT:w" s="T305">сказать</ts>
                  <nts id="Seg_9972" n="HIAT:ip">,</nts>
                  <nts id="Seg_9973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_9975" n="HIAT:w" s="T306">значит</ts>
                  <nts id="Seg_9976" n="HIAT:ip">,</nts>
                  <nts id="Seg_9977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_9979" n="HIAT:w" s="T307">какая</ts>
                  <nts id="Seg_9980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_9982" n="HIAT:w" s="T308">пленка</ts>
                  <nts id="Seg_9983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_9985" n="HIAT:w" s="T309">и</ts>
                  <nts id="Seg_9986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_9988" n="HIAT:w" s="T310">всё</ts>
                  <nts id="Seg_9989" n="HIAT:ip">.</nts>
                  <nts id="Seg_9990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_9992" n="HIAT:u" s="T311">
                  <nts id="Seg_9993" n="HIAT:ip">(</nts>
                  <nts id="Seg_9994" n="HIAT:ip">(</nts>
                  <ats e="T312" id="Seg_9995" n="HIAT:non-pho" s="T311">…</ats>
                  <nts id="Seg_9996" n="HIAT:ip">)</nts>
                  <nts id="Seg_9997" n="HIAT:ip">)</nts>
                  <nts id="Seg_9998" n="HIAT:ip">.</nts>
                  <nts id="Seg_9999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_10000" n="sc" s="T313">
               <ts e="T314" id="Seg_10002" n="HIAT:u" s="T313">
                  <nts id="Seg_10003" n="HIAT:ip">(</nts>
                  <nts id="Seg_10004" n="HIAT:ip">(</nts>
                  <ats e="T314" id="Seg_10005" n="HIAT:non-pho" s="T313">…</ats>
                  <nts id="Seg_10006" n="HIAT:ip">)</nts>
                  <nts id="Seg_10007" n="HIAT:ip">)</nts>
                  <nts id="Seg_10008" n="HIAT:ip">.</nts>
                  <nts id="Seg_10009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T633" id="Seg_10010" n="sc" s="T630">
               <ts e="T633" id="Seg_10012" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_10014" n="HIAT:w" s="T630">Ugandə</ts>
                  <nts id="Seg_10015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_10017" n="HIAT:w" s="T631">jakše</ts>
                  <nts id="Seg_10018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_10020" n="HIAT:w" s="T632">dʼăbaktərlial</ts>
                  <nts id="Seg_10021" n="HIAT:ip">.</nts>
                  <nts id="Seg_10022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T639" id="Seg_10023" n="sc" s="T638">
               <ts e="T639" id="Seg_10025" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_10027" n="HIAT:w" s="T638">Хорошо</ts>
                  <nts id="Seg_10028" n="HIAT:ip">!</nts>
                  <nts id="Seg_10029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_10030" n="sc" s="T640">
               <ts e="T643" id="Seg_10032" n="HIAT:u" s="T640">
                  <ts e="T643" id="Seg_10034" n="HIAT:w" s="T640">Прекрасно</ts>
                  <nts id="Seg_10035" n="HIAT:ip">.</nts>
                  <nts id="Seg_10036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T655" id="Seg_10037" n="sc" s="T653">
               <ts e="T655" id="Seg_10039" n="HIAT:u" s="T653">
                  <ts e="T655" id="Seg_10041" n="HIAT:w" s="T653">Нет</ts>
                  <nts id="Seg_10042" n="HIAT:ip">.</nts>
                  <nts id="Seg_10043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T664" id="Seg_10044" n="sc" s="T657">
               <ts e="T664" id="Seg_10046" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_10048" n="HIAT:w" s="T657">Это</ts>
                  <nts id="Seg_10049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_10051" n="HIAT:w" s="T658">может</ts>
                  <nts id="Seg_10052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_10054" n="HIAT:w" s="T659">ты</ts>
                  <nts id="Seg_10055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_10057" n="HIAT:w" s="T660">еще</ts>
                  <nts id="Seg_10058" n="HIAT:ip">,</nts>
                  <nts id="Seg_10059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_10061" n="HIAT:w" s="T661">мы</ts>
                  <nts id="Seg_10062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_10064" n="HIAT:w" s="T662">тоже</ts>
                  <nts id="Seg_10065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_10067" n="HIAT:w" s="T663">запишем</ts>
                  <nts id="Seg_10068" n="HIAT:ip">.</nts>
                  <nts id="Seg_10069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T673" id="Seg_10070" n="sc" s="T665">
               <ts e="T673" id="Seg_10072" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_10074" n="HIAT:w" s="T665">Если</ts>
                  <nts id="Seg_10075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_10077" n="HIAT:w" s="T666">это</ts>
                  <nts id="Seg_10078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_10080" n="HIAT:w" s="T667">самое</ts>
                  <nts id="Seg_10081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_10083" n="HIAT:w" s="T668">расскажешь</ts>
                  <nts id="Seg_10084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_10086" n="HIAT:w" s="T669">по-русски</ts>
                  <nts id="Seg_10087" n="HIAT:ip">,</nts>
                  <nts id="Seg_10088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_10090" n="HIAT:w" s="T671">объяснишь</ts>
                  <nts id="Seg_10091" n="HIAT:ip">.</nts>
                  <nts id="Seg_10092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T676" id="Seg_10093" n="sc" s="T674">
               <ts e="T676" id="Seg_10095" n="HIAT:u" s="T674">
                  <ts e="T676" id="Seg_10097" n="HIAT:w" s="T674">Ага</ts>
                  <nts id="Seg_10098" n="HIAT:ip">.</nts>
                  <nts id="Seg_10099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T679" id="Seg_10100" n="sc" s="T678">
               <ts e="T679" id="Seg_10102" n="HIAT:u" s="T678">
                  <nts id="Seg_10103" n="HIAT:ip">(</nts>
                  <nts id="Seg_10104" n="HIAT:ip">(</nts>
                  <ats e="T679" id="Seg_10105" n="HIAT:non-pho" s="T678">…</ats>
                  <nts id="Seg_10106" n="HIAT:ip">)</nts>
                  <nts id="Seg_10107" n="HIAT:ip">)</nts>
                  <nts id="Seg_10108" n="HIAT:ip">.</nts>
                  <nts id="Seg_10109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T924" id="Seg_10110" n="sc" s="T918">
               <ts e="T924" id="Seg_10112" n="HIAT:u" s="T918">
                  <ts e="T919" id="Seg_10114" n="HIAT:w" s="T918">В</ts>
                  <nts id="Seg_10115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_10117" n="HIAT:w" s="T919">Тарту</ts>
                  <nts id="Seg_10118" n="HIAT:ip">,</nts>
                  <nts id="Seg_10119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_10121" n="HIAT:w" s="T921">в</ts>
                  <nts id="Seg_10122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_10124" n="HIAT:w" s="T923">Тарту</ts>
                  <nts id="Seg_10125" n="HIAT:ip">.</nts>
                  <nts id="Seg_10126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T932" id="Seg_10127" n="sc" s="T928">
               <ts e="T932" id="Seg_10129" n="HIAT:u" s="T928">
                  <ts e="T929" id="Seg_10131" n="HIAT:w" s="T928">А</ts>
                  <nts id="Seg_10132" n="HIAT:ip">,</nts>
                  <nts id="Seg_10133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_10135" n="HIAT:w" s="T929">сначала</ts>
                  <nts id="Seg_10136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_10138" n="HIAT:w" s="T930">в</ts>
                  <nts id="Seg_10139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_10141" n="HIAT:w" s="T931">Таллин</ts>
                  <nts id="Seg_10142" n="HIAT:ip">.</nts>
                  <nts id="Seg_10143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T960" id="Seg_10144" n="sc" s="T953">
               <ts e="T960" id="Seg_10146" n="HIAT:u" s="T953">
                  <ts e="T955" id="Seg_10148" n="HIAT:w" s="T953">Все</ts>
                  <nts id="Seg_10149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_10151" n="HIAT:w" s="T955">равно</ts>
                  <nts id="Seg_10152" n="HIAT:ip">,</nts>
                  <nts id="Seg_10153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_10155" n="HIAT:w" s="T957">все</ts>
                  <nts id="Seg_10156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_10158" n="HIAT:w" s="T959">равно</ts>
                  <nts id="Seg_10159" n="HIAT:ip">.</nts>
                  <nts id="Seg_10160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1011" id="Seg_10161" n="sc" s="T1010">
               <ts e="T1011" id="Seg_10163" n="HIAT:u" s="T1010">
                  <nts id="Seg_10164" n="HIAT:ip">(</nts>
                  <nts id="Seg_10165" n="HIAT:ip">(</nts>
                  <ats e="T1011" id="Seg_10166" n="HIAT:non-pho" s="T1010">…</ats>
                  <nts id="Seg_10167" n="HIAT:ip">)</nts>
                  <nts id="Seg_10168" n="HIAT:ip">)</nts>
                  <nts id="Seg_10169" n="HIAT:ip">.</nts>
                  <nts id="Seg_10170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1013" id="Seg_10171" n="sc" s="T1012">
               <ts e="T1013" id="Seg_10173" n="HIAT:u" s="T1012">
                  <ts e="T1013" id="Seg_10175" n="HIAT:w" s="T1012">Хорошо</ts>
                  <nts id="Seg_10176" n="HIAT:ip">.</nts>
                  <nts id="Seg_10177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1025" id="Seg_10178" n="sc" s="T1014">
               <ts e="T1015" id="Seg_10180" n="HIAT:u" s="T1014">
                  <ts e="T1015" id="Seg_10182" n="HIAT:w" s="T1014">Прекрасно</ts>
                  <nts id="Seg_10183" n="HIAT:ip">.</nts>
                  <nts id="Seg_10184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1025" id="Seg_10186" n="HIAT:u" s="T1015">
                  <ts e="T1016" id="Seg_10188" n="HIAT:w" s="T1015">Он</ts>
                  <nts id="Seg_10189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_10191" n="HIAT:w" s="T1016">говорит</ts>
                  <nts id="Seg_10192" n="HIAT:ip">,</nts>
                  <nts id="Seg_10193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_10195" n="HIAT:w" s="T1017">что</ts>
                  <nts id="Seg_10196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_10198" n="HIAT:w" s="T1018">очень</ts>
                  <nts id="Seg_10199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_10201" n="HIAT:w" s="T1019">хорошо</ts>
                  <nts id="Seg_10202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_10204" n="HIAT:w" s="T1020">все</ts>
                  <nts id="Seg_10205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10206" n="HIAT:ip">(</nts>
                  <ts e="T1022" id="Seg_10208" n="HIAT:w" s="T1021">вот</ts>
                  <nts id="Seg_10209" n="HIAT:ip">)</nts>
                  <nts id="Seg_10210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_10212" n="HIAT:w" s="T1022">на</ts>
                  <nts id="Seg_10213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_10215" n="HIAT:w" s="T1023">машинке</ts>
                  <nts id="Seg_10216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_10218" n="HIAT:w" s="T1024">получилось</ts>
                  <nts id="Seg_10219" n="HIAT:ip">.</nts>
                  <nts id="Seg_10220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1045" id="Seg_10221" n="sc" s="T1029">
               <ts e="T1045" id="Seg_10223" n="HIAT:u" s="T1029">
                  <ts e="T1030" id="Seg_10225" n="HIAT:w" s="T1029">Нет</ts>
                  <nts id="Seg_10226" n="HIAT:ip">,</nts>
                  <nts id="Seg_10227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_10229" n="HIAT:w" s="T1030">не</ts>
                  <nts id="Seg_10230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_10232" n="HIAT:w" s="T1032">слыхать</ts>
                  <nts id="Seg_10233" n="HIAT:ip">,</nts>
                  <nts id="Seg_10234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_10236" n="HIAT:w" s="T1034">и</ts>
                  <nts id="Seg_10237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_10239" n="HIAT:w" s="T1036">сейчас</ts>
                  <nts id="Seg_10240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_10242" n="HIAT:w" s="T1037">закрыта</ts>
                  <nts id="Seg_10243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_10245" n="HIAT:w" s="T1039">к</ts>
                  <nts id="Seg_10246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_10248" n="HIAT:w" s="T1040">тому</ts>
                  <nts id="Seg_10249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_10251" n="HIAT:w" s="T1042">же</ts>
                  <nts id="Seg_10252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_10254" n="HIAT:w" s="T1043">дверь</ts>
                  <nts id="Seg_10255" n="HIAT:ip">.</nts>
                  <nts id="Seg_10256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1053" id="Seg_10257" n="sc" s="T1048">
               <ts e="T1053" id="Seg_10259" n="HIAT:u" s="T1048">
                  <ts e="T1049" id="Seg_10261" n="HIAT:w" s="T1048">Убежали</ts>
                  <nts id="Seg_10262" n="HIAT:ip">,</nts>
                  <nts id="Seg_10263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_10265" n="HIAT:w" s="T1049">может</ts>
                  <nts id="Seg_10266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_10268" n="HIAT:w" s="T1051">быть</ts>
                  <nts id="Seg_10269" n="HIAT:ip">.</nts>
                  <nts id="Seg_10270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1092" id="Seg_10271" n="sc" s="T1071">
               <ts e="T1092" id="Seg_10273" n="HIAT:u" s="T1071">
                  <ts e="T1072" id="Seg_10275" n="HIAT:w" s="T1071">Ну</ts>
                  <nts id="Seg_10276" n="HIAT:ip">,</nts>
                  <nts id="Seg_10277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_10279" n="HIAT:w" s="T1072">я</ts>
                  <nts id="Seg_10280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_10282" n="HIAT:w" s="T1073">вот</ts>
                  <nts id="Seg_10283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10284" n="HIAT:ip">(</nts>
                  <ts e="T1075" id="Seg_10286" n="HIAT:w" s="T1074">машиниста</ts>
                  <nts id="Seg_10287" n="HIAT:ip">)</nts>
                  <nts id="Seg_10288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_10290" n="HIAT:w" s="T1075">не</ts>
                  <nts id="Seg_10291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_10293" n="HIAT:w" s="T1076">нашел</ts>
                  <nts id="Seg_10294" n="HIAT:ip">,</nts>
                  <nts id="Seg_10295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_10297" n="HIAT:w" s="T1077">мы</ts>
                  <nts id="Seg_10298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_10300" n="HIAT:w" s="T1078">это</ts>
                  <nts id="Seg_10301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_10303" n="HIAT:w" s="T1079">все</ts>
                  <nts id="Seg_10304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_10306" n="HIAT:w" s="T1080">разошлись</ts>
                  <nts id="Seg_10307" n="HIAT:ip">,</nts>
                  <nts id="Seg_10308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_10310" n="HIAT:w" s="T1081">и</ts>
                  <nts id="Seg_10311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_10313" n="HIAT:w" s="T1082">я</ts>
                  <nts id="Seg_10314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_10316" n="HIAT:w" s="T1083">его</ts>
                  <nts id="Seg_10317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_10319" n="HIAT:w" s="T1084">по</ts>
                  <nts id="Seg_10320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_10322" n="HIAT:w" s="T1085">городу</ts>
                  <nts id="Seg_10323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_10325" n="HIAT:w" s="T1086">искал</ts>
                  <nts id="Seg_10326" n="HIAT:ip">,</nts>
                  <nts id="Seg_10327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_10329" n="HIAT:w" s="T1087">всюду</ts>
                  <nts id="Seg_10330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_10332" n="HIAT:w" s="T1088">ходил</ts>
                  <nts id="Seg_10333" n="HIAT:ip">,</nts>
                  <nts id="Seg_10334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_10336" n="HIAT:w" s="T1089">пока</ts>
                  <nts id="Seg_10337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_10339" n="HIAT:w" s="T1090">нашел</ts>
                  <nts id="Seg_10340" n="HIAT:ip">,</nts>
                  <nts id="Seg_10341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_10343" n="HIAT:w" s="T1091">встретились</ts>
                  <nts id="Seg_10344" n="HIAT:ip">.</nts>
                  <nts id="Seg_10345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1097" id="Seg_10346" n="sc" s="T1093">
               <ts e="T1097" id="Seg_10348" n="HIAT:u" s="T1093">
                  <ts e="T1094" id="Seg_10350" n="HIAT:w" s="T1093">Там</ts>
                  <nts id="Seg_10351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_10353" n="HIAT:w" s="T1094">где-то</ts>
                  <nts id="Seg_10354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_10356" n="HIAT:w" s="T1095">в</ts>
                  <nts id="Seg_10357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_10359" n="HIAT:w" s="T1096">центре</ts>
                  <nts id="Seg_10360" n="HIAT:ip">.</nts>
                  <nts id="Seg_10361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1099" id="Seg_10362" n="sc" s="T1098">
               <ts e="T1099" id="Seg_10364" n="HIAT:u" s="T1098">
                  <ts e="T1099" id="Seg_10366" n="HIAT:w" s="T1098">Так</ts>
                  <nts id="Seg_10367" n="HIAT:ip">.</nts>
                  <nts id="Seg_10368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1106" id="Seg_10369" n="sc" s="T1100">
               <ts e="T1106" id="Seg_10371" n="HIAT:u" s="T1100">
                  <ts e="T1101" id="Seg_10373" n="HIAT:w" s="T1100">Ну</ts>
                  <nts id="Seg_10374" n="HIAT:ip">,</nts>
                  <nts id="Seg_10375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_10377" n="HIAT:w" s="T1101">ты</ts>
                  <nts id="Seg_10378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_10380" n="HIAT:w" s="T1102">что</ts>
                  <nts id="Seg_10381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_10383" n="HIAT:w" s="T1103">еще</ts>
                  <nts id="Seg_10384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_10386" n="HIAT:w" s="T1104">хочешь</ts>
                  <nts id="Seg_10387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_10389" n="HIAT:w" s="T1105">рассказать</ts>
                  <nts id="Seg_10390" n="HIAT:ip">?</nts>
                  <nts id="Seg_10391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1117" id="Seg_10392" n="sc" s="T1107">
               <ts e="T1117" id="Seg_10394" n="HIAT:u" s="T1107">
                  <nts id="Seg_10395" n="HIAT:ip">(</nts>
                  <ts e="T1108" id="Seg_10397" n="HIAT:w" s="T1107">Что</ts>
                  <nts id="Seg_10398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_10400" n="HIAT:w" s="T1108">себе=</ts>
                  <nts id="Seg_10401" n="HIAT:ip">)</nts>
                  <nts id="Seg_10402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1110" id="Seg_10404" n="HIAT:w" s="T1109">Что</ts>
                  <nts id="Seg_10405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_10407" n="HIAT:w" s="T1110">тебе</ts>
                  <nts id="Seg_10408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_10410" n="HIAT:w" s="T1111">нравится</ts>
                  <nts id="Seg_10411" n="HIAT:ip">,</nts>
                  <nts id="Seg_10412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_10414" n="HIAT:w" s="T1112">каждое</ts>
                  <nts id="Seg_10415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_10417" n="HIAT:w" s="T1113">твое</ts>
                  <nts id="Seg_10418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_10420" n="HIAT:w" s="T1114">слово</ts>
                  <nts id="Seg_10421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10422" n="HIAT:ip">—</nts>
                  <nts id="Seg_10423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_10425" n="HIAT:w" s="T1115">это</ts>
                  <nts id="Seg_10426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_10428" n="HIAT:w" s="T1116">золото</ts>
                  <nts id="Seg_10429" n="HIAT:ip">.</nts>
                  <nts id="Seg_10430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1141" id="Seg_10431" n="sc" s="T1120">
               <ts e="T1132" id="Seg_10433" n="HIAT:u" s="T1120">
                  <ts e="T1122" id="Seg_10435" n="HIAT:w" s="T1120">Все</ts>
                  <nts id="Seg_10436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_10438" n="HIAT:w" s="T1122">равно</ts>
                  <nts id="Seg_10439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1125" id="Seg_10441" n="HIAT:w" s="T1123">о</ts>
                  <nts id="Seg_10442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_10444" n="HIAT:w" s="T1125">чем</ts>
                  <nts id="Seg_10445" n="HIAT:ip">,</nts>
                  <nts id="Seg_10446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_10448" n="HIAT:w" s="T1127">все</ts>
                  <nts id="Seg_10449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_10451" n="HIAT:w" s="T1128">равно</ts>
                  <nts id="Seg_10452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1131" id="Seg_10454" n="HIAT:w" s="T1129">о</ts>
                  <nts id="Seg_10455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_10457" n="HIAT:w" s="T1131">чем</ts>
                  <nts id="Seg_10458" n="HIAT:ip">.</nts>
                  <nts id="Seg_10459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1141" id="Seg_10461" n="HIAT:u" s="T1132">
                  <ts e="T1134" id="Seg_10463" n="HIAT:w" s="T1132">Хотя</ts>
                  <nts id="Seg_10464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_10466" n="HIAT:w" s="T1134">у</ts>
                  <nts id="Seg_10467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_10469" n="HIAT:w" s="T1135">тебя</ts>
                  <nts id="Seg_10470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_10472" n="HIAT:w" s="T1137">темы</ts>
                  <nts id="Seg_10473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_10475" n="HIAT:w" s="T1138">новые</ts>
                  <nts id="Seg_10476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_10478" n="HIAT:w" s="T1139">очень</ts>
                  <nts id="Seg_10479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_10481" n="HIAT:w" s="T1140">появились</ts>
                  <nts id="Seg_10482" n="HIAT:ip">.</nts>
                  <nts id="Seg_10483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1152" id="Seg_10484" n="sc" s="T1142">
               <ts e="T1152" id="Seg_10486" n="HIAT:u" s="T1142">
                  <ts e="T1143" id="Seg_10488" n="HIAT:w" s="T1142">А</ts>
                  <nts id="Seg_10489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_10491" n="HIAT:w" s="T1143">можно</ts>
                  <nts id="Seg_10492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_10494" n="HIAT:w" s="T1144">и</ts>
                  <nts id="Seg_10495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_10497" n="HIAT:w" s="T1145">на</ts>
                  <nts id="Seg_10498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1147" id="Seg_10500" n="HIAT:w" s="T1146">старые</ts>
                  <nts id="Seg_10501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_10503" n="HIAT:w" s="T1147">темы</ts>
                  <nts id="Seg_10504" n="HIAT:ip">,</nts>
                  <nts id="Seg_10505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_10507" n="HIAT:w" s="T1148">о</ts>
                  <nts id="Seg_10508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_10510" n="HIAT:w" s="T1149">тех</ts>
                  <nts id="Seg_10511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_10513" n="HIAT:w" s="T1150">же</ts>
                  <nts id="Seg_10514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_10516" n="HIAT:w" s="T1151">вещах</ts>
                  <nts id="Seg_10517" n="HIAT:ip">.</nts>
                  <nts id="Seg_10518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1161" id="Seg_10519" n="sc" s="T1153">
               <ts e="T1161" id="Seg_10521" n="HIAT:u" s="T1153">
                  <ts e="T1154" id="Seg_10523" n="HIAT:w" s="T1153">Потому</ts>
                  <nts id="Seg_10524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_10526" n="HIAT:w" s="T1154">что</ts>
                  <nts id="Seg_10527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_10529" n="HIAT:w" s="T1155">неважно</ts>
                  <nts id="Seg_10530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_10532" n="HIAT:w" s="T1156">то</ts>
                  <nts id="Seg_10533" n="HIAT:ip">,</nts>
                  <nts id="Seg_10534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1158" id="Seg_10536" n="HIAT:w" s="T1157">что</ts>
                  <nts id="Seg_10537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_10539" n="HIAT:w" s="T1158">расскажешь</ts>
                  <nts id="Seg_10540" n="HIAT:ip">,</nts>
                  <nts id="Seg_10541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1160" id="Seg_10543" n="HIAT:w" s="T1159">а</ts>
                  <nts id="Seg_10544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_10546" n="HIAT:w" s="T1160">как</ts>
                  <nts id="Seg_10547" n="HIAT:ip">.</nts>
                  <nts id="Seg_10548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1172" id="Seg_10549" n="sc" s="T1162">
               <ts e="T1172" id="Seg_10551" n="HIAT:u" s="T1162">
                  <ts e="T1163" id="Seg_10553" n="HIAT:w" s="T1162">Потому</ts>
                  <nts id="Seg_10554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_10556" n="HIAT:w" s="T1163">что</ts>
                  <nts id="Seg_10557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1165" id="Seg_10559" n="HIAT:w" s="T1164">именно</ts>
                  <nts id="Seg_10560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1166" id="Seg_10562" n="HIAT:w" s="T1165">камасинском</ts>
                  <nts id="Seg_10563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_10565" n="HIAT:w" s="T1166">языке</ts>
                  <nts id="Seg_10566" n="HIAT:ip">,</nts>
                  <nts id="Seg_10567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_10569" n="HIAT:w" s="T1167">вот</ts>
                  <nts id="Seg_10570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1169" id="Seg_10572" n="HIAT:w" s="T1168">что</ts>
                  <nts id="Seg_10573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_10575" n="HIAT:w" s="T1169">это</ts>
                  <nts id="Seg_10576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171" id="Seg_10578" n="HIAT:w" s="T1170">очень</ts>
                  <nts id="Seg_10579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_10581" n="HIAT:w" s="T1171">важное</ts>
                  <nts id="Seg_10582" n="HIAT:ip">.</nts>
                  <nts id="Seg_10583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1194" id="Seg_10584" n="sc" s="T1190">
               <ts e="T1194" id="Seg_10586" n="HIAT:u" s="T1190">
                  <ts e="T1192" id="Seg_10588" n="HIAT:w" s="T1190">Нет</ts>
                  <nts id="Seg_10589" n="HIAT:ip">,</nts>
                  <nts id="Seg_10590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_10592" n="HIAT:w" s="T1192">не</ts>
                  <nts id="Seg_10593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1194" id="Seg_10595" n="HIAT:w" s="T1193">помню</ts>
                  <nts id="Seg_10596" n="HIAT:ip">.</nts>
                  <nts id="Seg_10597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1197" id="Seg_10598" n="sc" s="T1195">
               <ts e="T1197" id="Seg_10600" n="HIAT:u" s="T1195">
                  <ts e="T1196" id="Seg_10602" n="HIAT:w" s="T1195">Не</ts>
                  <nts id="Seg_10603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1197" id="Seg_10605" n="HIAT:w" s="T1196">помню</ts>
                  <nts id="Seg_10606" n="HIAT:ip">.</nts>
                  <nts id="Seg_10607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1205" id="Seg_10608" n="sc" s="T1198">
               <ts e="T1200" id="Seg_10610" n="HIAT:u" s="T1198">
                  <ts e="T1199" id="Seg_10612" n="HIAT:w" s="T1198">Вот</ts>
                  <nts id="Seg_10613" n="HIAT:ip">,</nts>
                  <nts id="Seg_10614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_10616" n="HIAT:w" s="T1199">давай</ts>
                  <nts id="Seg_10617" n="HIAT:ip">.</nts>
                  <nts id="Seg_10618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1205" id="Seg_10620" n="HIAT:u" s="T1200">
                  <ts e="T1201" id="Seg_10622" n="HIAT:w" s="T1200">Давай</ts>
                  <nts id="Seg_10623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_10625" n="HIAT:w" s="T1201">это</ts>
                  <nts id="Seg_10626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_10628" n="HIAT:w" s="T1202">прямо</ts>
                  <nts id="Seg_10629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_10631" n="HIAT:w" s="T1203">сначала</ts>
                  <nts id="Seg_10632" n="HIAT:ip">,</nts>
                  <nts id="Seg_10633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1205" id="Seg_10635" n="HIAT:w" s="T1204">да</ts>
                  <nts id="Seg_10636" n="HIAT:ip">?</nts>
                  <nts id="Seg_10637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1218" id="Seg_10638" n="sc" s="T1206">
               <ts e="T1218" id="Seg_10640" n="HIAT:u" s="T1206">
                  <ts e="T1207" id="Seg_10642" n="HIAT:w" s="T1206">По-камасински</ts>
                  <nts id="Seg_10643" n="HIAT:ip">,</nts>
                  <nts id="Seg_10644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1208" id="Seg_10646" n="HIAT:w" s="T1207">а</ts>
                  <nts id="Seg_10647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1209" id="Seg_10649" n="HIAT:w" s="T1208">потом</ts>
                  <nts id="Seg_10650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1210" id="Seg_10652" n="HIAT:w" s="T1209">уже</ts>
                  <nts id="Seg_10653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1211" id="Seg_10655" n="HIAT:w" s="T1210">и</ts>
                  <nts id="Seg_10656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1212" id="Seg_10658" n="HIAT:w" s="T1211">расскажешь</ts>
                  <nts id="Seg_10659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_10661" n="HIAT:w" s="T1212">снова</ts>
                  <nts id="Seg_10662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_10664" n="HIAT:w" s="T1213">по-русски</ts>
                  <nts id="Seg_10665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1215" id="Seg_10667" n="HIAT:w" s="T1214">опять</ts>
                  <nts id="Seg_10668" n="HIAT:ip">,</nts>
                  <nts id="Seg_10669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1216" id="Seg_10671" n="HIAT:w" s="T1215">как</ts>
                  <nts id="Seg_10672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1217" id="Seg_10674" n="HIAT:w" s="T1216">сейчас</ts>
                  <nts id="Seg_10675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_10677" n="HIAT:w" s="T1217">делали</ts>
                  <nts id="Seg_10678" n="HIAT:ip">.</nts>
                  <nts id="Seg_10679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1266" id="Seg_10680" n="sc" s="T1255">
               <ts e="T1266" id="Seg_10682" n="HIAT:u" s="T1255">
                  <nts id="Seg_10683" n="HIAT:ip">(</nts>
                  <nts id="Seg_10684" n="HIAT:ip">(</nts>
                  <ats e="T1266" id="Seg_10685" n="HIAT:non-pho" s="T1255">…</ats>
                  <nts id="Seg_10686" n="HIAT:ip">)</nts>
                  <nts id="Seg_10687" n="HIAT:ip">)</nts>
                  <nts id="Seg_10688" n="HIAT:ip">.</nts>
                  <nts id="Seg_10689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1297" id="Seg_10690" n="sc" s="T1287">
               <ts e="T1297" id="Seg_10692" n="HIAT:u" s="T1287">
                  <ts e="T1288" id="Seg_10694" n="HIAT:w" s="T1287">А</ts>
                  <nts id="Seg_10695" n="HIAT:ip">,</nts>
                  <nts id="Seg_10696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1289" id="Seg_10698" n="HIAT:w" s="T1288">как</ts>
                  <nts id="Seg_10699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1290" id="Seg_10701" n="HIAT:w" s="T1289">брат</ts>
                  <nts id="Seg_10702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1291" id="Seg_10704" n="HIAT:w" s="T1290">был</ts>
                  <nts id="Seg_10705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1292" id="Seg_10707" n="HIAT:w" s="T1291">в</ts>
                  <nts id="Seg_10708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_10710" n="HIAT:w" s="T1292">плену</ts>
                  <nts id="Seg_10711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_10713" n="HIAT:w" s="T1293">в</ts>
                  <nts id="Seg_10714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1296" id="Seg_10716" n="HIAT:w" s="T1294">Финляндии</ts>
                  <nts id="Seg_10717" n="HIAT:ip">,</nts>
                  <nts id="Seg_10718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1297" id="Seg_10720" n="HIAT:w" s="T1296">да</ts>
                  <nts id="Seg_10721" n="HIAT:ip">?</nts>
                  <nts id="Seg_10722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1317" id="Seg_10723" n="sc" s="T1300">
               <ts e="T1308" id="Seg_10725" n="HIAT:u" s="T1300">
                  <ts e="T1301" id="Seg_10727" n="HIAT:w" s="T1300">Ага</ts>
                  <nts id="Seg_10728" n="HIAT:ip">,</nts>
                  <nts id="Seg_10729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1303" id="Seg_10731" n="HIAT:w" s="T1301">там</ts>
                  <nts id="Seg_10732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1305" id="Seg_10734" n="HIAT:w" s="T1303">да</ts>
                  <nts id="Seg_10735" n="HIAT:ip">,</nts>
                  <nts id="Seg_10736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1306" id="Seg_10738" n="HIAT:w" s="T1305">это</ts>
                  <nts id="Seg_10739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_10741" n="HIAT:w" s="T1306">мы</ts>
                  <nts id="Seg_10742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1308" id="Seg_10744" n="HIAT:w" s="T1307">записывали</ts>
                  <nts id="Seg_10745" n="HIAT:ip">.</nts>
                  <nts id="Seg_10746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1317" id="Seg_10748" n="HIAT:u" s="T1308">
                  <ts e="T1309" id="Seg_10750" n="HIAT:w" s="T1308">Правильно</ts>
                  <nts id="Seg_10751" n="HIAT:ip">,</nts>
                  <nts id="Seg_10752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1310" id="Seg_10754" n="HIAT:w" s="T1309">про</ts>
                  <nts id="Seg_10755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1312" id="Seg_10757" n="HIAT:w" s="T1310">это</ts>
                  <nts id="Seg_10758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10759" n="HIAT:ip">(</nts>
                  <ts e="T1314" id="Seg_10761" n="HIAT:w" s="T1312">запись</ts>
                  <nts id="Seg_10762" n="HIAT:ip">)</nts>
                  <nts id="Seg_10763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1317" id="Seg_10765" n="HIAT:w" s="T1314">есть</ts>
                  <nts id="Seg_10766" n="HIAT:ip">.</nts>
                  <nts id="Seg_10767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1330" id="Seg_10768" n="sc" s="T1323">
               <ts e="T1330" id="Seg_10770" n="HIAT:u" s="T1323">
                  <nts id="Seg_10771" n="HIAT:ip">(</nts>
                  <nts id="Seg_10772" n="HIAT:ip">(</nts>
                  <ats e="T1330" id="Seg_10773" n="HIAT:non-pho" s="T1323">…</ats>
                  <nts id="Seg_10774" n="HIAT:ip">)</nts>
                  <nts id="Seg_10775" n="HIAT:ip">)</nts>
                  <nts id="Seg_10776" n="HIAT:ip">.</nts>
                  <nts id="Seg_10777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1332" id="Seg_10778" n="sc" s="T1331">
               <ts e="T1332" id="Seg_10780" n="HIAT:u" s="T1331">
                  <nts id="Seg_10781" n="HIAT:ip">(</nts>
                  <nts id="Seg_10782" n="HIAT:ip">(</nts>
                  <ats e="T1332" id="Seg_10783" n="HIAT:non-pho" s="T1331">…</ats>
                  <nts id="Seg_10784" n="HIAT:ip">)</nts>
                  <nts id="Seg_10785" n="HIAT:ip">)</nts>
                  <nts id="Seg_10786" n="HIAT:ip">.</nts>
                  <nts id="Seg_10787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1341" id="Seg_10788" n="sc" s="T1333">
               <ts e="T1341" id="Seg_10790" n="HIAT:u" s="T1333">
                  <ts e="T1334" id="Seg_10792" n="HIAT:w" s="T1333">Да</ts>
                  <nts id="Seg_10793" n="HIAT:ip">,</nts>
                  <nts id="Seg_10794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1335" id="Seg_10796" n="HIAT:w" s="T1334">говорят</ts>
                  <nts id="Seg_10797" n="HIAT:ip">,</nts>
                  <nts id="Seg_10798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1336" id="Seg_10800" n="HIAT:w" s="T1335">что</ts>
                  <nts id="Seg_10801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_10803" n="HIAT:w" s="T1336">это</ts>
                  <nts id="Seg_10804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1338" id="Seg_10806" n="HIAT:w" s="T1337">записано</ts>
                  <nts id="Seg_10807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1339" id="Seg_10809" n="HIAT:w" s="T1338">всё</ts>
                  <nts id="Seg_10810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1340" id="Seg_10812" n="HIAT:w" s="T1339">очень</ts>
                  <nts id="Seg_10813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1341" id="Seg_10815" n="HIAT:w" s="T1340">хорошо</ts>
                  <nts id="Seg_10816" n="HIAT:ip">.</nts>
                  <nts id="Seg_10817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1349" id="Seg_10818" n="sc" s="T1342">
               <ts e="T1349" id="Seg_10820" n="HIAT:u" s="T1342">
                  <ts e="T1343" id="Seg_10822" n="HIAT:w" s="T1342">Вчера</ts>
                  <nts id="Seg_10823" n="HIAT:ip">,</nts>
                  <nts id="Seg_10824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_10826" n="HIAT:w" s="T1343">говорят</ts>
                  <nts id="Seg_10827" n="HIAT:ip">,</nts>
                  <nts id="Seg_10828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1345" id="Seg_10830" n="HIAT:w" s="T1344">очень</ts>
                  <nts id="Seg_10831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1346" id="Seg_10833" n="HIAT:w" s="T1345">хорошо</ts>
                  <nts id="Seg_10834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1347" id="Seg_10836" n="HIAT:w" s="T1346">у</ts>
                  <nts id="Seg_10837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1348" id="Seg_10839" n="HIAT:w" s="T1347">них</ts>
                  <nts id="Seg_10840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_10842" n="HIAT:w" s="T1348">получилось</ts>
                  <nts id="Seg_10843" n="HIAT:ip">.</nts>
                  <nts id="Seg_10844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1367" id="Seg_10845" n="sc" s="T1350">
               <ts e="T1367" id="Seg_10847" n="HIAT:u" s="T1350">
                  <ts e="T1351" id="Seg_10849" n="HIAT:w" s="T1350">Второй</ts>
                  <nts id="Seg_10850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1352" id="Seg_10852" n="HIAT:w" s="T1351">был</ts>
                  <nts id="Seg_10853" n="HIAT:ip">,</nts>
                  <nts id="Seg_10854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1353" id="Seg_10856" n="HIAT:w" s="T1352">который</ts>
                  <nts id="Seg_10857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_10859" n="HIAT:w" s="T1353">сидел</ts>
                  <nts id="Seg_10860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_10862" n="HIAT:w" s="T1354">вот</ts>
                  <nts id="Seg_10863" n="HIAT:ip">,</nts>
                  <nts id="Seg_10864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1356" id="Seg_10866" n="HIAT:w" s="T1355">протягивал</ts>
                  <nts id="Seg_10867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1357" id="Seg_10869" n="HIAT:w" s="T1356">такой</ts>
                  <nts id="Seg_10870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358" id="Seg_10872" n="HIAT:w" s="T1357">длинной</ts>
                  <nts id="Seg_10873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_10875" n="HIAT:w" s="T1358">палкой</ts>
                  <nts id="Seg_10876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_10878" n="HIAT:w" s="T1359">микрофон</ts>
                  <nts id="Seg_10879" n="HIAT:ip">,</nts>
                  <nts id="Seg_10880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_10882" n="HIAT:w" s="T1360">вот</ts>
                  <nts id="Seg_10883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1362" id="Seg_10885" n="HIAT:w" s="T1361">он</ts>
                  <nts id="Seg_10886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1363" id="Seg_10888" n="HIAT:w" s="T1362">как</ts>
                  <nts id="Seg_10889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_10891" n="HIAT:w" s="T1363">раз</ts>
                  <nts id="Seg_10892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_10894" n="HIAT:w" s="T1364">записывал</ts>
                  <nts id="Seg_10895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1366" id="Seg_10897" n="HIAT:w" s="T1365">для</ts>
                  <nts id="Seg_10898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1367" id="Seg_10900" n="HIAT:w" s="T1366">них</ts>
                  <nts id="Seg_10901" n="HIAT:ip">.</nts>
                  <nts id="Seg_10902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1375" id="Seg_10903" n="sc" s="T1374">
               <ts e="T1375" id="Seg_10905" n="HIAT:u" s="T1374">
                  <ts e="T1375" id="Seg_10907" n="HIAT:w" s="T1374">Ага</ts>
                  <nts id="Seg_10908" n="HIAT:ip">.</nts>
                  <nts id="Seg_10909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1384" id="Seg_10910" n="sc" s="T1376">
               <ts e="T1384" id="Seg_10912" n="HIAT:u" s="T1376">
                  <nts id="Seg_10913" n="HIAT:ip">(</nts>
                  <nts id="Seg_10914" n="HIAT:ip">(</nts>
                  <ats e="T1384" id="Seg_10915" n="HIAT:non-pho" s="T1376">…</ats>
                  <nts id="Seg_10916" n="HIAT:ip">)</nts>
                  <nts id="Seg_10917" n="HIAT:ip">)</nts>
                  <nts id="Seg_10918" n="HIAT:ip">.</nts>
                  <nts id="Seg_10919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1406" id="Seg_10920" n="sc" s="T1390">
               <ts e="T1406" id="Seg_10922" n="HIAT:u" s="T1390">
                  <ts e="T1391" id="Seg_10924" n="HIAT:w" s="T1390">Так</ts>
                  <nts id="Seg_10925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_10927" n="HIAT:w" s="T1391">это</ts>
                  <nts id="Seg_10928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1393" id="Seg_10930" n="HIAT:w" s="T1392">в</ts>
                  <nts id="Seg_10931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_10933" n="HIAT:w" s="T1393">радио</ts>
                  <nts id="Seg_10934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_10936" n="HIAT:w" s="T1394">будут</ts>
                  <nts id="Seg_10937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1396" id="Seg_10939" n="HIAT:w" s="T1395">передавать</ts>
                  <nts id="Seg_10940" n="HIAT:ip">,</nts>
                  <nts id="Seg_10941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1397" id="Seg_10943" n="HIAT:w" s="T1396">только</ts>
                  <nts id="Seg_10944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1398" id="Seg_10946" n="HIAT:w" s="T1397">мы</ts>
                  <nts id="Seg_10947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_10949" n="HIAT:w" s="T1398">забыли</ts>
                  <nts id="Seg_10950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_10952" n="HIAT:w" s="T1399">спросить</ts>
                  <nts id="Seg_10953" n="HIAT:ip">,</nts>
                  <nts id="Seg_10954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_10956" n="HIAT:w" s="T1400">когда</ts>
                  <nts id="Seg_10957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_10959" n="HIAT:w" s="T1401">это</ts>
                  <nts id="Seg_10960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1403" id="Seg_10962" n="HIAT:w" s="T1402">будет</ts>
                  <nts id="Seg_10963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1404" id="Seg_10965" n="HIAT:w" s="T1403">передача</ts>
                  <nts id="Seg_10966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1405" id="Seg_10968" n="HIAT:w" s="T1404">в</ts>
                  <nts id="Seg_10969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1406" id="Seg_10971" n="HIAT:w" s="T1405">радио</ts>
                  <nts id="Seg_10972" n="HIAT:ip">.</nts>
                  <nts id="Seg_10973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1417" id="Seg_10974" n="sc" s="T1407">
               <ts e="T1417" id="Seg_10976" n="HIAT:u" s="T1407">
                  <ts e="T1408" id="Seg_10978" n="HIAT:w" s="T1407">Вот</ts>
                  <nts id="Seg_10979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1409" id="Seg_10981" n="HIAT:w" s="T1408">тот</ts>
                  <nts id="Seg_10982" n="HIAT:ip">,</nts>
                  <nts id="Seg_10983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_10985" n="HIAT:w" s="T1409">который</ts>
                  <nts id="Seg_10986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1411" id="Seg_10988" n="HIAT:w" s="T1410">сидел</ts>
                  <nts id="Seg_10989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1412" id="Seg_10991" n="HIAT:w" s="T1411">сзади</ts>
                  <nts id="Seg_10992" n="HIAT:ip">,</nts>
                  <nts id="Seg_10993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1413" id="Seg_10995" n="HIAT:w" s="T1412">самый</ts>
                  <nts id="Seg_10996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1414" id="Seg_10998" n="HIAT:w" s="T1413">сзади</ts>
                  <nts id="Seg_10999" n="HIAT:ip">,</nts>
                  <nts id="Seg_11000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1415" id="Seg_11002" n="HIAT:w" s="T1414">протягивал</ts>
                  <nts id="Seg_11003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1416" id="Seg_11005" n="HIAT:w" s="T1415">микрофон</ts>
                  <nts id="Seg_11006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_11008" n="HIAT:w" s="T1416">подальше</ts>
                  <nts id="Seg_11009" n="HIAT:ip">.</nts>
                  <nts id="Seg_11010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1428" id="Seg_11011" n="sc" s="T1418">
               <ts e="T1428" id="Seg_11013" n="HIAT:u" s="T1418">
                  <ts e="T1419" id="Seg_11015" n="HIAT:w" s="T1418">Такой</ts>
                  <nts id="Seg_11016" n="HIAT:ip">,</nts>
                  <nts id="Seg_11017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1420" id="Seg_11019" n="HIAT:w" s="T1419">эстонец</ts>
                  <nts id="Seg_11020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_11022" n="HIAT:w" s="T1420">он</ts>
                  <nts id="Seg_11023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1422" id="Seg_11025" n="HIAT:w" s="T1421">был</ts>
                  <nts id="Seg_11026" n="HIAT:ip">,</nts>
                  <nts id="Seg_11027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1423" id="Seg_11029" n="HIAT:w" s="T1422">так</ts>
                  <nts id="Seg_11030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_11032" n="HIAT:w" s="T1423">он</ts>
                  <nts id="Seg_11033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_11035" n="HIAT:w" s="T1424">для</ts>
                  <nts id="Seg_11036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1426" id="Seg_11038" n="HIAT:w" s="T1425">эстонского</ts>
                  <nts id="Seg_11039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1427" id="Seg_11041" n="HIAT:w" s="T1426">радио</ts>
                  <nts id="Seg_11042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1428" id="Seg_11044" n="HIAT:w" s="T1427">снимал</ts>
                  <nts id="Seg_11045" n="HIAT:ip">.</nts>
                  <nts id="Seg_11046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1430" id="Seg_11047" n="sc" s="T1429">
               <ts e="T1430" id="Seg_11049" n="HIAT:u" s="T1429">
                  <ts e="T1430" id="Seg_11051" n="HIAT:w" s="T1429">Вот</ts>
                  <nts id="Seg_11052" n="HIAT:ip">.</nts>
                  <nts id="Seg_11053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1445" id="Seg_11054" n="sc" s="T1431">
               <ts e="T1445" id="Seg_11056" n="HIAT:u" s="T1431">
                  <ts e="T1432" id="Seg_11058" n="HIAT:w" s="T1431">Надо</ts>
                  <nts id="Seg_11059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1433" id="Seg_11061" n="HIAT:w" s="T1432">у</ts>
                  <nts id="Seg_11062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1434" id="Seg_11064" n="HIAT:w" s="T1433">него</ts>
                  <nts id="Seg_11065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1435" id="Seg_11067" n="HIAT:w" s="T1434">спросить</ts>
                  <nts id="Seg_11068" n="HIAT:ip">,</nts>
                  <nts id="Seg_11069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_11071" n="HIAT:w" s="T1435">может</ts>
                  <nts id="Seg_11072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11073" n="HIAT:ip">(</nts>
                  <ts e="T1437" id="Seg_11075" n="HIAT:w" s="T1436">быть</ts>
                  <nts id="Seg_11076" n="HIAT:ip">)</nts>
                  <nts id="Seg_11077" n="HIAT:ip">,</nts>
                  <nts id="Seg_11078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_11080" n="HIAT:w" s="T1437">сама</ts>
                  <nts id="Seg_11081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_11083" n="HIAT:w" s="T1438">послушаешь</ts>
                  <nts id="Seg_11084" n="HIAT:ip">,</nts>
                  <nts id="Seg_11085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1440" id="Seg_11087" n="HIAT:w" s="T1439">как</ts>
                  <nts id="Seg_11088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_11090" n="HIAT:w" s="T1440">по</ts>
                  <nts id="Seg_11091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_11093" n="HIAT:w" s="T1441">радио</ts>
                  <nts id="Seg_11094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_11096" n="HIAT:w" s="T1442">будешь</ts>
                  <nts id="Seg_11097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_11099" n="HIAT:w" s="T1443">говорить</ts>
                  <nts id="Seg_11100" n="HIAT:ip">.</nts>
                  <nts id="Seg_11101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1452" id="Seg_11102" n="sc" s="T1450">
               <ts e="T1452" id="Seg_11104" n="HIAT:u" s="T1450">
                  <ts e="T1451" id="Seg_11106" n="HIAT:w" s="T1450">А</ts>
                  <nts id="Seg_11107" n="HIAT:ip">,</nts>
                  <nts id="Seg_11108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11109" n="HIAT:ip">(</nts>
                  <nts id="Seg_11110" n="HIAT:ip">(</nts>
                  <ats e="T1452" id="Seg_11111" n="HIAT:non-pho" s="T1451">…</ats>
                  <nts id="Seg_11112" n="HIAT:ip">)</nts>
                  <nts id="Seg_11113" n="HIAT:ip">)</nts>
                  <nts id="Seg_11114" n="HIAT:ip">.</nts>
                  <nts id="Seg_11115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1459" id="Seg_11116" n="sc" s="T1456">
               <ts e="T1459" id="Seg_11118" n="HIAT:u" s="T1456">
                  <ts e="T1459" id="Seg_11120" n="HIAT:w" s="T1456">Ага</ts>
                  <nts id="Seg_11121" n="HIAT:ip">.</nts>
                  <nts id="Seg_11122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1463" id="Seg_11123" n="sc" s="T1461">
               <ts e="T1462" id="Seg_11125" n="HIAT:u" s="T1461">
                  <ts e="T1462" id="Seg_11127" n="HIAT:w" s="T1461">Так-с</ts>
                  <nts id="Seg_11128" n="HIAT:ip">.</nts>
                  <nts id="Seg_11129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1463" id="Seg_11131" n="HIAT:u" s="T1462">
                  <ts e="T1463" id="Seg_11133" n="HIAT:w" s="T1462">Ну</ts>
                  <nts id="Seg_11134" n="HIAT:ip">…</nts>
                  <nts id="Seg_11135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1482" id="Seg_11136" n="sc" s="T1476">
               <ts e="T1482" id="Seg_11138" n="HIAT:u" s="T1476">
                  <ts e="T1479" id="Seg_11140" n="HIAT:w" s="T1476">Ага</ts>
                  <nts id="Seg_11141" n="HIAT:ip">,</nts>
                  <nts id="Seg_11142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1482" id="Seg_11144" n="HIAT:w" s="T1479">точно-точно</ts>
                  <nts id="Seg_11145" n="HIAT:ip">.</nts>
                  <nts id="Seg_11146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1506" id="Seg_11147" n="sc" s="T1486">
               <ts e="T1506" id="Seg_11149" n="HIAT:u" s="T1486">
                  <ts e="T1487" id="Seg_11151" n="HIAT:w" s="T1486">Да</ts>
                  <nts id="Seg_11152" n="HIAT:ip">,</nts>
                  <nts id="Seg_11153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1488" id="Seg_11155" n="HIAT:w" s="T1487">я</ts>
                  <nts id="Seg_11156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1489" id="Seg_11158" n="HIAT:w" s="T1488">не</ts>
                  <nts id="Seg_11159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1490" id="Seg_11161" n="HIAT:w" s="T1489">знаю</ts>
                  <nts id="Seg_11162" n="HIAT:ip">,</nts>
                  <nts id="Seg_11163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_11165" n="HIAT:w" s="T1490">вот</ts>
                  <nts id="Seg_11166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1492" id="Seg_11168" n="HIAT:w" s="T1491">было</ts>
                  <nts id="Seg_11169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1493" id="Seg_11171" n="HIAT:w" s="T1492">так</ts>
                  <nts id="Seg_11172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1494" id="Seg_11174" n="HIAT:w" s="T1493">это</ts>
                  <nts id="Seg_11175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495" id="Seg_11177" n="HIAT:w" s="T1494">всё</ts>
                  <nts id="Seg_11178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1496" id="Seg_11180" n="HIAT:w" s="T1495">спешно</ts>
                  <nts id="Seg_11181" n="HIAT:ip">,</nts>
                  <nts id="Seg_11182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1497" id="Seg_11184" n="HIAT:w" s="T1496">что</ts>
                  <nts id="Seg_11185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1498" id="Seg_11187" n="HIAT:w" s="T1497">я</ts>
                  <nts id="Seg_11188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1499" id="Seg_11190" n="HIAT:w" s="T1498">не</ts>
                  <nts id="Seg_11191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1500" id="Seg_11193" n="HIAT:w" s="T1499">успел</ts>
                  <nts id="Seg_11194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1501" id="Seg_11196" n="HIAT:w" s="T1500">этого</ts>
                  <nts id="Seg_11197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1502" id="Seg_11199" n="HIAT:w" s="T1501">человека</ts>
                  <nts id="Seg_11200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1503" id="Seg_11202" n="HIAT:w" s="T1502">спросить</ts>
                  <nts id="Seg_11203" n="HIAT:ip">,</nts>
                  <nts id="Seg_11204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1504" id="Seg_11206" n="HIAT:w" s="T1503">когда</ts>
                  <nts id="Seg_11207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_11209" n="HIAT:w" s="T1504">будет</ts>
                  <nts id="Seg_11210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1506" id="Seg_11212" n="HIAT:w" s="T1505">радио</ts>
                  <nts id="Seg_11213" n="HIAT:ip">.</nts>
                  <nts id="Seg_11214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1526" id="Seg_11215" n="sc" s="T1507">
               <ts e="T1526" id="Seg_11217" n="HIAT:u" s="T1507">
                  <ts e="T1508" id="Seg_11219" n="HIAT:w" s="T1507">Я</ts>
                  <nts id="Seg_11220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1509" id="Seg_11222" n="HIAT:w" s="T1508">и</ts>
                  <nts id="Seg_11223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1510" id="Seg_11225" n="HIAT:w" s="T1509">сам</ts>
                  <nts id="Seg_11226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1511" id="Seg_11228" n="HIAT:w" s="T1510">интересно</ts>
                  <nts id="Seg_11229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1512" id="Seg_11231" n="HIAT:w" s="T1511">мне</ts>
                  <nts id="Seg_11232" n="HIAT:ip">,</nts>
                  <nts id="Seg_11233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1513" id="Seg_11235" n="HIAT:w" s="T1512">потому</ts>
                  <nts id="Seg_11236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1514" id="Seg_11238" n="HIAT:w" s="T1513">что</ts>
                  <nts id="Seg_11239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1515" id="Seg_11241" n="HIAT:w" s="T1514">я</ts>
                  <nts id="Seg_11242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1516" id="Seg_11244" n="HIAT:w" s="T1515">тоже</ts>
                  <nts id="Seg_11245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1517" id="Seg_11247" n="HIAT:w" s="T1516">говорил</ts>
                  <nts id="Seg_11248" n="HIAT:ip">,</nts>
                  <nts id="Seg_11249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1518" id="Seg_11251" n="HIAT:w" s="T1517">меня</ts>
                  <nts id="Seg_11252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1519" id="Seg_11254" n="HIAT:w" s="T1518">спрашивал</ts>
                  <nts id="Seg_11255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1520" id="Seg_11257" n="HIAT:w" s="T1519">насчет</ts>
                  <nts id="Seg_11258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1521" id="Seg_11260" n="HIAT:w" s="T1520">тебя</ts>
                  <nts id="Seg_11261" n="HIAT:ip">,</nts>
                  <nts id="Seg_11262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1522" id="Seg_11264" n="HIAT:w" s="T1521">я</ts>
                  <nts id="Seg_11265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1523" id="Seg_11267" n="HIAT:w" s="T1522">рассказал</ts>
                  <nts id="Seg_11268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_11270" n="HIAT:w" s="T1523">про</ts>
                  <nts id="Seg_11271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1525" id="Seg_11273" n="HIAT:w" s="T1524">тебя</ts>
                  <nts id="Seg_11274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1526" id="Seg_11276" n="HIAT:w" s="T1525">там</ts>
                  <nts id="Seg_11277" n="HIAT:ip">.</nts>
                  <nts id="Seg_11278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1559" id="Seg_11279" n="sc" s="T1544">
               <ts e="T1559" id="Seg_11281" n="HIAT:u" s="T1544">
                  <ts e="T1545" id="Seg_11283" n="HIAT:w" s="T1544">Ну</ts>
                  <nts id="Seg_11284" n="HIAT:ip">,</nts>
                  <nts id="Seg_11285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1546" id="Seg_11287" n="HIAT:w" s="T1545">а</ts>
                  <nts id="Seg_11288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1547" id="Seg_11290" n="HIAT:w" s="T1546">может</ts>
                  <nts id="Seg_11291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1548" id="Seg_11293" n="HIAT:w" s="T1547">ты</ts>
                  <nts id="Seg_11294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1549" id="Seg_11296" n="HIAT:w" s="T1548">хочешь</ts>
                  <nts id="Seg_11297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1550" id="Seg_11299" n="HIAT:w" s="T1549">что</ts>
                  <nts id="Seg_11300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1551" id="Seg_11302" n="HIAT:w" s="T1550">сказать</ts>
                  <nts id="Seg_11303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1552" id="Seg_11305" n="HIAT:w" s="T1551">и</ts>
                  <nts id="Seg_11306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1553" id="Seg_11308" n="HIAT:w" s="T1552">о</ts>
                  <nts id="Seg_11309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1554" id="Seg_11311" n="HIAT:w" s="T1553">вчерашнем</ts>
                  <nts id="Seg_11312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1555" id="Seg_11314" n="HIAT:w" s="T1554">дне</ts>
                  <nts id="Seg_11315" n="HIAT:ip">,</nts>
                  <nts id="Seg_11316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1556" id="Seg_11318" n="HIAT:w" s="T1555">например</ts>
                  <nts id="Seg_11319" n="HIAT:ip">,</nts>
                  <nts id="Seg_11320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1557" id="Seg_11322" n="HIAT:w" s="T1556">если</ts>
                  <nts id="Seg_11323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1558" id="Seg_11325" n="HIAT:w" s="T1557">тебе</ts>
                  <nts id="Seg_11326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1559" id="Seg_11328" n="HIAT:w" s="T1558">понравится</ts>
                  <nts id="Seg_11329" n="HIAT:ip">.</nts>
                  <nts id="Seg_11330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1571" id="Seg_11331" n="sc" s="T1560">
               <ts e="T1571" id="Seg_11333" n="HIAT:u" s="T1560">
                  <ts e="T1561" id="Seg_11335" n="HIAT:w" s="T1560">Как</ts>
                  <nts id="Seg_11336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1562" id="Seg_11338" n="HIAT:w" s="T1561">вчера</ts>
                  <nts id="Seg_11339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1563" id="Seg_11341" n="HIAT:w" s="T1562">в</ts>
                  <nts id="Seg_11342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1564" id="Seg_11344" n="HIAT:w" s="T1563">гости</ts>
                  <nts id="Seg_11345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1565" id="Seg_11347" n="HIAT:w" s="T1564">ходили</ts>
                  <nts id="Seg_11348" n="HIAT:ip">,</nts>
                  <nts id="Seg_11349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1566" id="Seg_11351" n="HIAT:w" s="T1565">все</ts>
                  <nts id="Seg_11352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1567" id="Seg_11354" n="HIAT:w" s="T1566">такое</ts>
                  <nts id="Seg_11355" n="HIAT:ip">,</nts>
                  <nts id="Seg_11356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1568" id="Seg_11358" n="HIAT:w" s="T1567">как</ts>
                  <nts id="Seg_11359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1569" id="Seg_11361" n="HIAT:w" s="T1568">спрашивали</ts>
                  <nts id="Seg_11362" n="HIAT:ip">,</nts>
                  <nts id="Seg_11363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1570" id="Seg_11365" n="HIAT:w" s="T1569">как</ts>
                  <nts id="Seg_11366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1571" id="Seg_11368" n="HIAT:w" s="T1570">было</ts>
                  <nts id="Seg_11369" n="HIAT:ip">.</nts>
                  <nts id="Seg_11370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1600" id="Seg_11371" n="sc" s="T1572">
               <ts e="T1578" id="Seg_11373" n="HIAT:u" s="T1572">
                  <ts e="T1573" id="Seg_11375" n="HIAT:w" s="T1572">Что</ts>
                  <nts id="Seg_11376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1574" id="Seg_11378" n="HIAT:w" s="T1573">ты</ts>
                  <nts id="Seg_11379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1575" id="Seg_11381" n="HIAT:w" s="T1574">им</ts>
                  <nts id="Seg_11382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1576" id="Seg_11384" n="HIAT:w" s="T1575">рассказывала</ts>
                  <nts id="Seg_11385" n="HIAT:ip">,</nts>
                  <nts id="Seg_11386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1577" id="Seg_11388" n="HIAT:w" s="T1576">так</ts>
                  <nts id="Seg_11389" n="HIAT:ip">,</nts>
                  <nts id="Seg_11390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1578" id="Seg_11392" n="HIAT:w" s="T1577">коротко</ts>
                  <nts id="Seg_11393" n="HIAT:ip">.</nts>
                  <nts id="Seg_11394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1600" id="Seg_11396" n="HIAT:u" s="T1578">
                  <ts e="T1579" id="Seg_11398" n="HIAT:w" s="T1578">Просто</ts>
                  <nts id="Seg_11399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1580" id="Seg_11401" n="HIAT:w" s="T1579">не</ts>
                  <nts id="Seg_11402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1581" id="Seg_11404" n="HIAT:w" s="T1580">повторять</ts>
                  <nts id="Seg_11405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1582" id="Seg_11407" n="HIAT:w" s="T1581">обязательно</ts>
                  <nts id="Seg_11408" n="HIAT:ip">,</nts>
                  <nts id="Seg_11409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1583" id="Seg_11411" n="HIAT:w" s="T1582">ну</ts>
                  <nts id="Seg_11412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1584" id="Seg_11414" n="HIAT:w" s="T1583">просто</ts>
                  <nts id="Seg_11415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1585" id="Seg_11417" n="HIAT:w" s="T1584">так</ts>
                  <nts id="Seg_11418" n="HIAT:ip">,</nts>
                  <nts id="Seg_11419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1586" id="Seg_11421" n="HIAT:w" s="T1585">о</ts>
                  <nts id="Seg_11422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1587" id="Seg_11424" n="HIAT:w" s="T1586">чем</ts>
                  <nts id="Seg_11425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1588" id="Seg_11427" n="HIAT:w" s="T1587">там</ts>
                  <nts id="Seg_11428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1589" id="Seg_11430" n="HIAT:w" s="T1588">говорили</ts>
                  <nts id="Seg_11431" n="HIAT:ip">,</nts>
                  <nts id="Seg_11432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1590" id="Seg_11434" n="HIAT:w" s="T1589">как</ts>
                  <nts id="Seg_11435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1591" id="Seg_11437" n="HIAT:w" s="T1590">всё</ts>
                  <nts id="Seg_11438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1592" id="Seg_11440" n="HIAT:w" s="T1591">это</ts>
                  <nts id="Seg_11441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1593" id="Seg_11443" n="HIAT:w" s="T1592">было</ts>
                  <nts id="Seg_11444" n="HIAT:ip">,</nts>
                  <nts id="Seg_11445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1594" id="Seg_11447" n="HIAT:w" s="T1593">сколько</ts>
                  <nts id="Seg_11448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1595" id="Seg_11450" n="HIAT:w" s="T1594">там</ts>
                  <nts id="Seg_11451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1596" id="Seg_11453" n="HIAT:w" s="T1595">было</ts>
                  <nts id="Seg_11454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1597" id="Seg_11456" n="HIAT:w" s="T1596">людей</ts>
                  <nts id="Seg_11457" n="HIAT:ip">,</nts>
                  <nts id="Seg_11458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1598" id="Seg_11460" n="HIAT:w" s="T1597">и</ts>
                  <nts id="Seg_11461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1599" id="Seg_11463" n="HIAT:w" s="T1598">всё</ts>
                  <nts id="Seg_11464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1600" id="Seg_11466" n="HIAT:w" s="T1599">такое</ts>
                  <nts id="Seg_11467" n="HIAT:ip">.</nts>
                  <nts id="Seg_11468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1603" id="Seg_11469" n="sc" s="T1601">
               <ts e="T1603" id="Seg_11471" n="HIAT:u" s="T1601">
                  <ts e="T1602" id="Seg_11473" n="HIAT:w" s="T1601">Можно</ts>
                  <nts id="Seg_11474" n="HIAT:ip">,</nts>
                  <nts id="Seg_11475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1603" id="Seg_11477" n="HIAT:w" s="T1602">думаешь</ts>
                  <nts id="Seg_11478" n="HIAT:ip">?</nts>
                  <nts id="Seg_11479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1611" id="Seg_11480" n="sc" s="T1608">
               <ts e="T1611" id="Seg_11482" n="HIAT:u" s="T1608">
                  <ts e="T1609" id="Seg_11484" n="HIAT:w" s="T1608">Ну</ts>
                  <nts id="Seg_11485" n="HIAT:ip">,</nts>
                  <nts id="Seg_11486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1610" id="Seg_11488" n="HIAT:w" s="T1609">давай</ts>
                  <nts id="Seg_11489" n="HIAT:ip">,</nts>
                  <nts id="Seg_11490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1611" id="Seg_11492" n="HIAT:w" s="T1610">давай</ts>
                  <nts id="Seg_11493" n="HIAT:ip">.</nts>
                  <nts id="Seg_11494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1626" id="Seg_11495" n="sc" s="T1620">
               <ts e="T1626" id="Seg_11497" n="HIAT:u" s="T1620">
                  <ts e="T1622" id="Seg_11499" n="HIAT:w" s="T1620">Да</ts>
                  <nts id="Seg_11500" n="HIAT:ip">,</nts>
                  <nts id="Seg_11501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1624" id="Seg_11503" n="HIAT:w" s="T1622">да</ts>
                  <nts id="Seg_11504" n="HIAT:ip">,</nts>
                  <nts id="Seg_11505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1626" id="Seg_11507" n="HIAT:w" s="T1624">действительно</ts>
                  <nts id="Seg_11508" n="HIAT:ip">.</nts>
                  <nts id="Seg_11509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1633" id="Seg_11510" n="sc" s="T1632">
               <ts e="T1633" id="Seg_11512" n="HIAT:u" s="T1632">
                  <ts e="T1633" id="Seg_11514" n="HIAT:w" s="T1632">Да</ts>
                  <nts id="Seg_11515" n="HIAT:ip">.</nts>
                  <nts id="Seg_11516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1645" id="Seg_11517" n="sc" s="T1637">
               <ts e="T1645" id="Seg_11519" n="HIAT:u" s="T1637">
                  <ts e="T1639" id="Seg_11521" n="HIAT:w" s="T1637">Точно</ts>
                  <nts id="Seg_11522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1640" id="Seg_11524" n="HIAT:w" s="T1639">так</ts>
                  <nts id="Seg_11525" n="HIAT:ip">,</nts>
                  <nts id="Seg_11526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1642" id="Seg_11528" n="HIAT:w" s="T1640">не</ts>
                  <nts id="Seg_11529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1644" id="Seg_11531" n="HIAT:w" s="T1642">с</ts>
                  <nts id="Seg_11532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1645" id="Seg_11534" n="HIAT:w" s="T1644">кем</ts>
                  <nts id="Seg_11535" n="HIAT:ip">.</nts>
                  <nts id="Seg_11536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1708" id="Seg_11537" n="sc" s="T1704">
               <ts e="T1708" id="Seg_11539" n="HIAT:u" s="T1704">
                  <ts e="T1706" id="Seg_11541" n="HIAT:w" s="T1704">Sĭreʔpne</ts>
                  <nts id="Seg_11542" n="HIAT:ip">,</nts>
                  <nts id="Seg_11543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1708" id="Seg_11545" n="HIAT:w" s="T1706">да</ts>
                  <nts id="Seg_11546" n="HIAT:ip">?</nts>
                  <nts id="Seg_11547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1711" id="Seg_11548" n="sc" s="T1709">
               <ts e="T1711" id="Seg_11550" n="HIAT:u" s="T1709">
                  <ts e="T1711" id="Seg_11552" n="HIAT:w" s="T1709">Ага</ts>
                  <nts id="Seg_11553" n="HIAT:ip">.</nts>
                  <nts id="Seg_11554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1716" id="Seg_11555" n="sc" s="T1712">
               <ts e="T1716" id="Seg_11557" n="HIAT:u" s="T1712">
                  <nts id="Seg_11558" n="HIAT:ip">(</nts>
                  <nts id="Seg_11559" n="HIAT:ip">(</nts>
                  <ats e="T1716" id="Seg_11560" n="HIAT:non-pho" s="T1712">…</ats>
                  <nts id="Seg_11561" n="HIAT:ip">)</nts>
                  <nts id="Seg_11562" n="HIAT:ip">)</nts>
                  <nts id="Seg_11563" n="HIAT:ip">.</nts>
                  <nts id="Seg_11564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1755" id="Seg_11565" n="sc" s="T1753">
               <ts e="T1755" id="Seg_11567" n="HIAT:u" s="T1753">
                  <ts e="T1755" id="Seg_11569" n="HIAT:w" s="T1753">Ага</ts>
                  <nts id="Seg_11570" n="HIAT:ip">.</nts>
                  <nts id="Seg_11571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1760" id="Seg_11572" n="sc" s="T1758">
               <ts e="T1760" id="Seg_11574" n="HIAT:u" s="T1758">
                  <ts e="T1760" id="Seg_11576" n="HIAT:w" s="T1758">Ага</ts>
                  <nts id="Seg_11577" n="HIAT:ip">.</nts>
                  <nts id="Seg_11578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1774" id="Seg_11579" n="sc" s="T1762">
               <ts e="T1774" id="Seg_11581" n="HIAT:u" s="T1762">
                  <ts e="T1763" id="Seg_11583" n="HIAT:w" s="T1762">Ну</ts>
                  <nts id="Seg_11584" n="HIAT:ip">,</nts>
                  <nts id="Seg_11585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1764" id="Seg_11587" n="HIAT:w" s="T1763">а</ts>
                  <nts id="Seg_11588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1765" id="Seg_11590" n="HIAT:w" s="T1764">ты</ts>
                  <nts id="Seg_11591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1766" id="Seg_11593" n="HIAT:w" s="T1765">повтори</ts>
                  <nts id="Seg_11594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1767" id="Seg_11596" n="HIAT:w" s="T1766">по-русски</ts>
                  <nts id="Seg_11597" n="HIAT:ip">,</nts>
                  <nts id="Seg_11598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1768" id="Seg_11600" n="HIAT:w" s="T1767">просто</ts>
                  <nts id="Seg_11601" n="HIAT:ip">,</nts>
                  <nts id="Seg_11602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1769" id="Seg_11604" n="HIAT:w" s="T1768">так</ts>
                  <nts id="Seg_11605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1770" id="Seg_11607" n="HIAT:w" s="T1769">сказать</ts>
                  <nts id="Seg_11608" n="HIAT:ip">,</nts>
                  <nts id="Seg_11609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1771" id="Seg_11611" n="HIAT:w" s="T1770">переведи</ts>
                  <nts id="Seg_11612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1772" id="Seg_11614" n="HIAT:w" s="T1771">это</ts>
                  <nts id="Seg_11615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1773" id="Seg_11617" n="HIAT:w" s="T1772">еще</ts>
                  <nts id="Seg_11618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1774" id="Seg_11620" n="HIAT:w" s="T1773">раз</ts>
                  <nts id="Seg_11621" n="HIAT:ip">.</nts>
                  <nts id="Seg_11622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1815" id="Seg_11623" n="sc" s="T1807">
               <ts e="T1815" id="Seg_11625" n="HIAT:u" s="T1807">
                  <ts e="T1808" id="Seg_11627" n="HIAT:w" s="T1807">Так</ts>
                  <nts id="Seg_11628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1809" id="Seg_11630" n="HIAT:w" s="T1808">всё</ts>
                  <nts id="Seg_11631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1810" id="Seg_11633" n="HIAT:w" s="T1809">и</ts>
                  <nts id="Seg_11634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1811" id="Seg_11636" n="HIAT:w" s="T1810">былое</ts>
                  <nts id="Seg_11637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1812" id="Seg_11639" n="HIAT:w" s="T1811">и</ts>
                  <nts id="Seg_11640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1813" id="Seg_11642" n="HIAT:w" s="T1812">старое</ts>
                  <nts id="Seg_11643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1814" id="Seg_11645" n="HIAT:w" s="T1813">можно</ts>
                  <nts id="Seg_11646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1815" id="Seg_11648" n="HIAT:w" s="T1814">вспомнить</ts>
                  <nts id="Seg_11649" n="HIAT:ip">.</nts>
                  <nts id="Seg_11650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1819" id="Seg_11651" n="sc" s="T1816">
               <ts e="T1819" id="Seg_11653" n="HIAT:u" s="T1816">
                  <ts e="T1817" id="Seg_11655" n="HIAT:w" s="T1816">Что</ts>
                  <nts id="Seg_11656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1818" id="Seg_11658" n="HIAT:w" s="T1817">только</ts>
                  <nts id="Seg_11659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1819" id="Seg_11661" n="HIAT:w" s="T1818">хочешь</ts>
                  <nts id="Seg_11662" n="HIAT:ip">.</nts>
                  <nts id="Seg_11663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1842" id="Seg_11664" n="sc" s="T1820">
               <ts e="T1842" id="Seg_11666" n="HIAT:u" s="T1820">
                  <ts e="T1821" id="Seg_11668" n="HIAT:w" s="T1820">Тут</ts>
                  <nts id="Seg_11669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1822" id="Seg_11671" n="HIAT:w" s="T1821">он</ts>
                  <nts id="Seg_11672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1823" id="Seg_11674" n="HIAT:w" s="T1822">предлагал</ts>
                  <nts id="Seg_11675" n="HIAT:ip">,</nts>
                  <nts id="Seg_11676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1824" id="Seg_11678" n="HIAT:w" s="T1823">что</ts>
                  <nts id="Seg_11679" n="HIAT:ip">,</nts>
                  <nts id="Seg_11680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1825" id="Seg_11682" n="HIAT:w" s="T1824">но</ts>
                  <nts id="Seg_11683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1826" id="Seg_11685" n="HIAT:w" s="T1825">мы</ts>
                  <nts id="Seg_11686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1827" id="Seg_11688" n="HIAT:w" s="T1826">это</ts>
                  <nts id="Seg_11689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1828" id="Seg_11691" n="HIAT:w" s="T1827">говорили</ts>
                  <nts id="Seg_11692" n="HIAT:ip">,</nts>
                  <nts id="Seg_11693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1829" id="Seg_11695" n="HIAT:w" s="T1828">ну</ts>
                  <nts id="Seg_11696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1830" id="Seg_11698" n="HIAT:w" s="T1829">это</ts>
                  <nts id="Seg_11699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1831" id="Seg_11701" n="HIAT:w" s="T1830">можно</ts>
                  <nts id="Seg_11702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1832" id="Seg_11704" n="HIAT:w" s="T1831">всё</ts>
                  <nts id="Seg_11705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1833" id="Seg_11707" n="HIAT:w" s="T1832">еще</ts>
                  <nts id="Seg_11708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1834" id="Seg_11710" n="HIAT:w" s="T1833">раз</ts>
                  <nts id="Seg_11711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1835" id="Seg_11713" n="HIAT:w" s="T1834">сказать:</ts>
                  <nts id="Seg_11714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1836" id="Seg_11716" n="HIAT:w" s="T1835">о</ts>
                  <nts id="Seg_11717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1837" id="Seg_11719" n="HIAT:w" s="T1836">матери</ts>
                  <nts id="Seg_11720" n="HIAT:ip">,</nts>
                  <nts id="Seg_11721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1838" id="Seg_11723" n="HIAT:w" s="T1837">об</ts>
                  <nts id="Seg_11724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1839" id="Seg_11726" n="HIAT:w" s="T1838">отце</ts>
                  <nts id="Seg_11727" n="HIAT:ip">,</nts>
                  <nts id="Seg_11728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1840" id="Seg_11730" n="HIAT:w" s="T1839">о</ts>
                  <nts id="Seg_11731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1841" id="Seg_11733" n="HIAT:w" s="T1840">чем</ts>
                  <nts id="Seg_11734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1842" id="Seg_11736" n="HIAT:w" s="T1841">угодно</ts>
                  <nts id="Seg_11737" n="HIAT:ip">.</nts>
                  <nts id="Seg_11738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1846" id="Seg_11739" n="sc" s="T1843">
               <ts e="T1846" id="Seg_11741" n="HIAT:u" s="T1843">
                  <nts id="Seg_11742" n="HIAT:ip">(</nts>
                  <nts id="Seg_11743" n="HIAT:ip">(</nts>
                  <ats e="T1846" id="Seg_11744" n="HIAT:non-pho" s="T1843">…</ats>
                  <nts id="Seg_11745" n="HIAT:ip">)</nts>
                  <nts id="Seg_11746" n="HIAT:ip">)</nts>
                  <nts id="Seg_11747" n="HIAT:ip">.</nts>
                  <nts id="Seg_11748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1859" id="Seg_11749" n="sc" s="T1852">
               <ts e="T1859" id="Seg_11751" n="HIAT:u" s="T1852">
                  <ts e="T1854" id="Seg_11753" n="HIAT:w" s="T1852">Ага</ts>
                  <nts id="Seg_11754" n="HIAT:ip">,</nts>
                  <nts id="Seg_11755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1855" id="Seg_11757" n="HIAT:w" s="T1854">ну</ts>
                  <nts id="Seg_11758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1856" id="Seg_11760" n="HIAT:w" s="T1855">ничего</ts>
                  <nts id="Seg_11761" n="HIAT:ip">,</nts>
                  <nts id="Seg_11762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1857" id="Seg_11764" n="HIAT:w" s="T1856">мы</ts>
                  <nts id="Seg_11765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1858" id="Seg_11767" n="HIAT:w" s="T1857">помаленьку</ts>
                  <nts id="Seg_11768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1859" id="Seg_11770" n="HIAT:w" s="T1858">можем</ts>
                  <nts id="Seg_11771" n="HIAT:ip">.</nts>
                  <nts id="Seg_11772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1861" id="Seg_11773" n="sc" s="T1860">
               <ts e="T1861" id="Seg_11775" n="HIAT:u" s="T1860">
                  <nts id="Seg_11776" n="HIAT:ip">(</nts>
                  <nts id="Seg_11777" n="HIAT:ip">(</nts>
                  <ats e="T1861" id="Seg_11778" n="HIAT:non-pho" s="T1860">…</ats>
                  <nts id="Seg_11779" n="HIAT:ip">)</nts>
                  <nts id="Seg_11780" n="HIAT:ip">)</nts>
                  <nts id="Seg_11781" n="HIAT:ip">.</nts>
                  <nts id="Seg_11782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T9" id="Seg_11783" n="sc" s="T7">
               <ts e="T9" id="Seg_11785" n="e" s="T7">А. </ts>
            </ts>
            <ts e="T25" id="Seg_11786" n="sc" s="T11">
               <ts e="T12" id="Seg_11788" n="e" s="T11">Вот, </ts>
               <ts e="T13" id="Seg_11790" n="e" s="T12">и </ts>
               <ts e="T14" id="Seg_11792" n="e" s="T13">испортилась </ts>
               <ts e="T15" id="Seg_11794" n="e" s="T14">у </ts>
               <ts e="T16" id="Seg_11796" n="e" s="T15">него </ts>
               <ts e="T17" id="Seg_11798" n="e" s="T16">в </ts>
               <ts e="T18" id="Seg_11800" n="e" s="T17">общем </ts>
               <ts e="T19" id="Seg_11802" n="e" s="T18">машина, </ts>
               <ts e="T20" id="Seg_11804" n="e" s="T19">а </ts>
               <ts e="T21" id="Seg_11806" n="e" s="T20">у </ts>
               <ts e="T22" id="Seg_11808" n="e" s="T21">меня </ts>
               <ts e="T23" id="Seg_11810" n="e" s="T22">не </ts>
               <ts e="T24" id="Seg_11812" n="e" s="T23">испортилась </ts>
               <ts e="T25" id="Seg_11814" n="e" s="T24">никогда. </ts>
            </ts>
            <ts e="T27" id="Seg_11815" n="sc" s="T26">
               <ts e="T27" id="Seg_11817" n="e" s="T26">((…)). </ts>
            </ts>
            <ts e="T29" id="Seg_11818" n="sc" s="T28">
               <ts e="T29" id="Seg_11820" n="e" s="T28">((…)). </ts>
            </ts>
            <ts e="T32" id="Seg_11821" n="sc" s="T30">
               <ts e="T31" id="Seg_11823" n="e" s="T30">Еще </ts>
               <ts e="T32" id="Seg_11825" n="e" s="T31">какой. </ts>
            </ts>
            <ts e="T40" id="Seg_11826" n="sc" s="T33">
               <ts e="T34" id="Seg_11828" n="e" s="T33">Часы </ts>
               <ts e="T35" id="Seg_11830" n="e" s="T34">не </ts>
               <ts e="T36" id="Seg_11832" n="e" s="T35">любит, </ts>
               <ts e="T37" id="Seg_11834" n="e" s="T36">говорит </ts>
               <ts e="T38" id="Seg_11836" n="e" s="T37">все </ts>
               <ts e="T39" id="Seg_11838" n="e" s="T38">будет </ts>
               <ts e="T40" id="Seg_11840" n="e" s="T39">слышно. </ts>
            </ts>
            <ts e="T42" id="Seg_11841" n="sc" s="T41">
               <ts e="T42" id="Seg_11843" n="e" s="T41">((…)). </ts>
            </ts>
            <ts e="T49" id="Seg_11844" n="sc" s="T48">
               <ts e="T49" id="Seg_11846" n="e" s="T48">((…)). </ts>
            </ts>
            <ts e="T51" id="Seg_11847" n="sc" s="T50">
               <ts e="T51" id="Seg_11849" n="e" s="T50">((…)). </ts>
            </ts>
            <ts e="T53" id="Seg_11850" n="sc" s="T52">
               <ts e="T53" id="Seg_11852" n="e" s="T52">((…)). </ts>
            </ts>
            <ts e="T58" id="Seg_11853" n="sc" s="T54">
               <ts e="T55" id="Seg_11855" n="e" s="T54">Значит, </ts>
               <ts e="T56" id="Seg_11857" n="e" s="T55">мы </ts>
               <ts e="T57" id="Seg_11859" n="e" s="T56">сделаем </ts>
               <ts e="T58" id="Seg_11861" n="e" s="T57">так. </ts>
            </ts>
            <ts e="T71" id="Seg_11862" n="sc" s="T59">
               <ts e="T60" id="Seg_11864" n="e" s="T59">Я </ts>
               <ts e="T61" id="Seg_11866" n="e" s="T60">сначала </ts>
               <ts e="T62" id="Seg_11868" n="e" s="T61">пару </ts>
               <ts e="T63" id="Seg_11870" n="e" s="T62">слов </ts>
               <ts e="T64" id="Seg_11872" n="e" s="T63">сам </ts>
               <ts e="T65" id="Seg_11874" n="e" s="T64">скажу, </ts>
               <ts e="T66" id="Seg_11876" n="e" s="T65">чтобы </ts>
               <ts e="T67" id="Seg_11878" n="e" s="T66">знать, </ts>
               <ts e="T68" id="Seg_11880" n="e" s="T67">какая </ts>
               <ts e="T69" id="Seg_11882" n="e" s="T68">пленка, </ts>
               <ts e="T70" id="Seg_11884" n="e" s="T69">что </ts>
               <ts e="T71" id="Seg_11886" n="e" s="T70">здесь. </ts>
            </ts>
            <ts e="T80" id="Seg_11887" n="sc" s="T72">
               <ts e="T73" id="Seg_11889" n="e" s="T72">А </ts>
               <ts e="T74" id="Seg_11891" n="e" s="T73">потом, </ts>
               <ts e="T75" id="Seg_11893" n="e" s="T74">будь </ts>
               <ts e="T76" id="Seg_11895" n="e" s="T75">добра, </ts>
               <ts e="T77" id="Seg_11897" n="e" s="T76">расскажи </ts>
               <ts e="T78" id="Seg_11899" n="e" s="T77">о </ts>
               <ts e="T79" id="Seg_11901" n="e" s="T78">своей </ts>
               <ts e="T80" id="Seg_11903" n="e" s="T79">поездке. </ts>
            </ts>
            <ts e="T93" id="Seg_11904" n="sc" s="T81">
               <ts e="T82" id="Seg_11906" n="e" s="T81">Хоть </ts>
               <ts e="T83" id="Seg_11908" n="e" s="T82">как </ts>
               <ts e="T84" id="Seg_11910" n="e" s="T83">сначала </ts>
               <ts e="T85" id="Seg_11912" n="e" s="T84">все, </ts>
               <ts e="T86" id="Seg_11914" n="e" s="T85">как </ts>
               <ts e="T87" id="Seg_11916" n="e" s="T86">он </ts>
               <ts e="T88" id="Seg_11918" n="e" s="T87">написал </ts>
               <ts e="T89" id="Seg_11920" n="e" s="T88">тебе, </ts>
               <ts e="T90" id="Seg_11922" n="e" s="T89">как </ts>
               <ts e="T91" id="Seg_11924" n="e" s="T90">потом </ts>
               <ts e="T93" id="Seg_11926" n="e" s="T91">приехал. </ts>
            </ts>
            <ts e="T99" id="Seg_11927" n="sc" s="T94">
               <ts e="T96" id="Seg_11929" n="e" s="T94">Как </ts>
               <ts e="T98" id="Seg_11931" n="e" s="T96">полетели, </ts>
               <ts e="T99" id="Seg_11933" n="e" s="T98">всё-всё. </ts>
            </ts>
            <ts e="T110" id="Seg_11934" n="sc" s="T100">
               <ts e="T101" id="Seg_11936" n="e" s="T100">А </ts>
               <ts e="T104" id="Seg_11938" n="e" s="T101">потом </ts>
               <ts e="T106" id="Seg_11940" n="e" s="T104">маленько </ts>
               <ts e="T108" id="Seg_11942" n="e" s="T106">по-русски </ts>
               <ts e="T110" id="Seg_11944" n="e" s="T108">можно. </ts>
            </ts>
            <ts e="T133" id="Seg_11945" n="sc" s="T118">
               <ts e="T119" id="Seg_11947" n="e" s="T118">Ну, </ts>
               <ts e="T121" id="Seg_11949" n="e" s="T119">я </ts>
               <ts e="T123" id="Seg_11951" n="e" s="T121">в </ts>
               <ts e="T125" id="Seg_11953" n="e" s="T123">Финляндии </ts>
               <ts e="T126" id="Seg_11955" n="e" s="T125">сидел, </ts>
               <ts e="T127" id="Seg_11957" n="e" s="T126">работал, </ts>
               <ts e="T128" id="Seg_11959" n="e" s="T127">там </ts>
               <ts e="T129" id="Seg_11961" n="e" s="T128">никак </ts>
               <ts e="T131" id="Seg_11963" n="e" s="T129">не </ts>
               <ts e="T133" id="Seg_11965" n="e" s="T131">приехать. </ts>
            </ts>
            <ts e="T162" id="Seg_11966" n="sc" s="T158">
               <ts e="T160" id="Seg_11968" n="e" s="T158">Ну </ts>
               <ts e="T162" id="Seg_11970" n="e" s="T160">сейчас… </ts>
            </ts>
            <ts e="T185" id="Seg_11971" n="sc" s="T178">
               <ts e="T179" id="Seg_11973" n="e" s="T178">Ну, </ts>
               <ts e="T180" id="Seg_11975" n="e" s="T179">сейчас </ts>
               <ts e="T182" id="Seg_11977" n="e" s="T180">это </ts>
               <ts e="T184" id="Seg_11979" n="e" s="T182">все </ts>
               <ts e="T185" id="Seg_11981" n="e" s="T184">расскажешь. </ts>
            </ts>
            <ts e="T221" id="Seg_11982" n="sc" s="T210">
               <ts e="T211" id="Seg_11984" n="e" s="T210">А </ts>
               <ts e="T212" id="Seg_11986" n="e" s="T211">он </ts>
               <ts e="T213" id="Seg_11988" n="e" s="T212">ночевал </ts>
               <ts e="T215" id="Seg_11990" n="e" s="T213">у </ts>
               <ts e="T216" id="Seg_11992" n="e" s="T215">тебя, </ts>
               <ts e="T217" id="Seg_11994" n="e" s="T216">прямо </ts>
               <ts e="T218" id="Seg_11996" n="e" s="T217">у </ts>
               <ts e="T219" id="Seg_11998" n="e" s="T218">тебя, </ts>
               <ts e="T221" id="Seg_12000" n="e" s="T219">да? </ts>
            </ts>
            <ts e="T227" id="Seg_12001" n="sc" s="T222">
               <ts e="T223" id="Seg_12003" n="e" s="T222">Ага. </ts>
               <ts e="T224" id="Seg_12005" n="e" s="T223">Ага, </ts>
               <ts e="T227" id="Seg_12007" n="e" s="T224">ага. </ts>
            </ts>
            <ts e="T243" id="Seg_12008" n="sc" s="T233">
               <ts e="T235" id="Seg_12010" n="e" s="T233">В </ts>
               <ts e="T236" id="Seg_12012" n="e" s="T235">ту </ts>
               <ts e="T237" id="Seg_12014" n="e" s="T236">баню, </ts>
               <ts e="T239" id="Seg_12016" n="e" s="T237">в </ts>
               <ts e="T240" id="Seg_12018" n="e" s="T239">которую </ts>
               <ts e="T241" id="Seg_12020" n="e" s="T240">я </ts>
               <ts e="T243" id="Seg_12022" n="e" s="T241">ходил. </ts>
            </ts>
            <ts e="T278" id="Seg_12023" n="sc" s="T276">
               <ts e="T278" id="Seg_12025" n="e" s="T276">Хорошо. </ts>
            </ts>
            <ts e="T312" id="Seg_12026" n="sc" s="T297">
               <ts e="T299" id="Seg_12028" n="e" s="T297">Сейчас </ts>
               <ts e="T301" id="Seg_12030" n="e" s="T299">поговорим, </ts>
               <ts e="T302" id="Seg_12032" n="e" s="T301">только </ts>
               <ts e="T303" id="Seg_12034" n="e" s="T302">мне </ts>
               <ts e="T304" id="Seg_12036" n="e" s="T303">придется </ts>
               <ts e="T305" id="Seg_12038" n="e" s="T304">сначала </ts>
               <ts e="T306" id="Seg_12040" n="e" s="T305">сказать, </ts>
               <ts e="T307" id="Seg_12042" n="e" s="T306">значит, </ts>
               <ts e="T308" id="Seg_12044" n="e" s="T307">какая </ts>
               <ts e="T309" id="Seg_12046" n="e" s="T308">пленка </ts>
               <ts e="T310" id="Seg_12048" n="e" s="T309">и </ts>
               <ts e="T311" id="Seg_12050" n="e" s="T310">всё. </ts>
               <ts e="T312" id="Seg_12052" n="e" s="T311">((…)). </ts>
            </ts>
            <ts e="T314" id="Seg_12053" n="sc" s="T313">
               <ts e="T314" id="Seg_12055" n="e" s="T313">((…)). </ts>
            </ts>
            <ts e="T633" id="Seg_12056" n="sc" s="T630">
               <ts e="T631" id="Seg_12058" n="e" s="T630">Ugandə </ts>
               <ts e="T632" id="Seg_12060" n="e" s="T631">jakše </ts>
               <ts e="T633" id="Seg_12062" n="e" s="T632">dʼăbaktərlial. </ts>
            </ts>
            <ts e="T639" id="Seg_12063" n="sc" s="T638">
               <ts e="T639" id="Seg_12065" n="e" s="T638">Хорошо! </ts>
            </ts>
            <ts e="T643" id="Seg_12066" n="sc" s="T640">
               <ts e="T643" id="Seg_12068" n="e" s="T640">Прекрасно. </ts>
            </ts>
            <ts e="T655" id="Seg_12069" n="sc" s="T653">
               <ts e="T655" id="Seg_12071" n="e" s="T653">Нет. </ts>
            </ts>
            <ts e="T664" id="Seg_12072" n="sc" s="T657">
               <ts e="T658" id="Seg_12074" n="e" s="T657">Это </ts>
               <ts e="T659" id="Seg_12076" n="e" s="T658">может </ts>
               <ts e="T660" id="Seg_12078" n="e" s="T659">ты </ts>
               <ts e="T661" id="Seg_12080" n="e" s="T660">еще, </ts>
               <ts e="T662" id="Seg_12082" n="e" s="T661">мы </ts>
               <ts e="T663" id="Seg_12084" n="e" s="T662">тоже </ts>
               <ts e="T664" id="Seg_12086" n="e" s="T663">запишем. </ts>
            </ts>
            <ts e="T673" id="Seg_12087" n="sc" s="T665">
               <ts e="T666" id="Seg_12089" n="e" s="T665">Если </ts>
               <ts e="T667" id="Seg_12091" n="e" s="T666">это </ts>
               <ts e="T668" id="Seg_12093" n="e" s="T667">самое </ts>
               <ts e="T669" id="Seg_12095" n="e" s="T668">расскажешь </ts>
               <ts e="T671" id="Seg_12097" n="e" s="T669">по-русски, </ts>
               <ts e="T673" id="Seg_12099" n="e" s="T671">объяснишь. </ts>
            </ts>
            <ts e="T676" id="Seg_12100" n="sc" s="T674">
               <ts e="T676" id="Seg_12102" n="e" s="T674">Ага. </ts>
            </ts>
            <ts e="T679" id="Seg_12103" n="sc" s="T678">
               <ts e="T679" id="Seg_12105" n="e" s="T678">((…)). </ts>
            </ts>
            <ts e="T924" id="Seg_12106" n="sc" s="T918">
               <ts e="T919" id="Seg_12108" n="e" s="T918">В </ts>
               <ts e="T921" id="Seg_12110" n="e" s="T919">Тарту, </ts>
               <ts e="T923" id="Seg_12112" n="e" s="T921">в </ts>
               <ts e="T924" id="Seg_12114" n="e" s="T923">Тарту. </ts>
            </ts>
            <ts e="T932" id="Seg_12115" n="sc" s="T928">
               <ts e="T929" id="Seg_12117" n="e" s="T928">А, </ts>
               <ts e="T930" id="Seg_12119" n="e" s="T929">сначала </ts>
               <ts e="T931" id="Seg_12121" n="e" s="T930">в </ts>
               <ts e="T932" id="Seg_12123" n="e" s="T931">Таллин. </ts>
            </ts>
            <ts e="T960" id="Seg_12124" n="sc" s="T953">
               <ts e="T955" id="Seg_12126" n="e" s="T953">Все </ts>
               <ts e="T957" id="Seg_12128" n="e" s="T955">равно, </ts>
               <ts e="T959" id="Seg_12130" n="e" s="T957">все </ts>
               <ts e="T960" id="Seg_12132" n="e" s="T959">равно. </ts>
            </ts>
            <ts e="T1011" id="Seg_12133" n="sc" s="T1010">
               <ts e="T1011" id="Seg_12135" n="e" s="T1010">((…)). </ts>
            </ts>
            <ts e="T1013" id="Seg_12136" n="sc" s="T1012">
               <ts e="T1013" id="Seg_12138" n="e" s="T1012">Хорошо. </ts>
            </ts>
            <ts e="T1025" id="Seg_12139" n="sc" s="T1014">
               <ts e="T1015" id="Seg_12141" n="e" s="T1014">Прекрасно. </ts>
               <ts e="T1016" id="Seg_12143" n="e" s="T1015">Он </ts>
               <ts e="T1017" id="Seg_12145" n="e" s="T1016">говорит, </ts>
               <ts e="T1018" id="Seg_12147" n="e" s="T1017">что </ts>
               <ts e="T1019" id="Seg_12149" n="e" s="T1018">очень </ts>
               <ts e="T1020" id="Seg_12151" n="e" s="T1019">хорошо </ts>
               <ts e="T1021" id="Seg_12153" n="e" s="T1020">все </ts>
               <ts e="T1022" id="Seg_12155" n="e" s="T1021">(вот) </ts>
               <ts e="T1023" id="Seg_12157" n="e" s="T1022">на </ts>
               <ts e="T1024" id="Seg_12159" n="e" s="T1023">машинке </ts>
               <ts e="T1025" id="Seg_12161" n="e" s="T1024">получилось. </ts>
            </ts>
            <ts e="T1045" id="Seg_12162" n="sc" s="T1029">
               <ts e="T1030" id="Seg_12164" n="e" s="T1029">Нет, </ts>
               <ts e="T1032" id="Seg_12166" n="e" s="T1030">не </ts>
               <ts e="T1034" id="Seg_12168" n="e" s="T1032">слыхать, </ts>
               <ts e="T1036" id="Seg_12170" n="e" s="T1034">и </ts>
               <ts e="T1037" id="Seg_12172" n="e" s="T1036">сейчас </ts>
               <ts e="T1039" id="Seg_12174" n="e" s="T1037">закрыта </ts>
               <ts e="T1040" id="Seg_12176" n="e" s="T1039">к </ts>
               <ts e="T1042" id="Seg_12178" n="e" s="T1040">тому </ts>
               <ts e="T1043" id="Seg_12180" n="e" s="T1042">же </ts>
               <ts e="T1045" id="Seg_12182" n="e" s="T1043">дверь. </ts>
            </ts>
            <ts e="T1053" id="Seg_12183" n="sc" s="T1048">
               <ts e="T1049" id="Seg_12185" n="e" s="T1048">Убежали, </ts>
               <ts e="T1051" id="Seg_12187" n="e" s="T1049">может </ts>
               <ts e="T1053" id="Seg_12189" n="e" s="T1051">быть. </ts>
            </ts>
            <ts e="T1092" id="Seg_12190" n="sc" s="T1071">
               <ts e="T1072" id="Seg_12192" n="e" s="T1071">Ну, </ts>
               <ts e="T1073" id="Seg_12194" n="e" s="T1072">я </ts>
               <ts e="T1074" id="Seg_12196" n="e" s="T1073">вот </ts>
               <ts e="T1075" id="Seg_12198" n="e" s="T1074">(машиниста) </ts>
               <ts e="T1076" id="Seg_12200" n="e" s="T1075">не </ts>
               <ts e="T1077" id="Seg_12202" n="e" s="T1076">нашел, </ts>
               <ts e="T1078" id="Seg_12204" n="e" s="T1077">мы </ts>
               <ts e="T1079" id="Seg_12206" n="e" s="T1078">это </ts>
               <ts e="T1080" id="Seg_12208" n="e" s="T1079">все </ts>
               <ts e="T1081" id="Seg_12210" n="e" s="T1080">разошлись, </ts>
               <ts e="T1082" id="Seg_12212" n="e" s="T1081">и </ts>
               <ts e="T1083" id="Seg_12214" n="e" s="T1082">я </ts>
               <ts e="T1084" id="Seg_12216" n="e" s="T1083">его </ts>
               <ts e="T1085" id="Seg_12218" n="e" s="T1084">по </ts>
               <ts e="T1086" id="Seg_12220" n="e" s="T1085">городу </ts>
               <ts e="T1087" id="Seg_12222" n="e" s="T1086">искал, </ts>
               <ts e="T1088" id="Seg_12224" n="e" s="T1087">всюду </ts>
               <ts e="T1089" id="Seg_12226" n="e" s="T1088">ходил, </ts>
               <ts e="T1090" id="Seg_12228" n="e" s="T1089">пока </ts>
               <ts e="T1091" id="Seg_12230" n="e" s="T1090">нашел, </ts>
               <ts e="T1092" id="Seg_12232" n="e" s="T1091">встретились. </ts>
            </ts>
            <ts e="T1097" id="Seg_12233" n="sc" s="T1093">
               <ts e="T1094" id="Seg_12235" n="e" s="T1093">Там </ts>
               <ts e="T1095" id="Seg_12237" n="e" s="T1094">где-то </ts>
               <ts e="T1096" id="Seg_12239" n="e" s="T1095">в </ts>
               <ts e="T1097" id="Seg_12241" n="e" s="T1096">центре. </ts>
            </ts>
            <ts e="T1099" id="Seg_12242" n="sc" s="T1098">
               <ts e="T1099" id="Seg_12244" n="e" s="T1098">Так. </ts>
            </ts>
            <ts e="T1106" id="Seg_12245" n="sc" s="T1100">
               <ts e="T1101" id="Seg_12247" n="e" s="T1100">Ну, </ts>
               <ts e="T1102" id="Seg_12249" n="e" s="T1101">ты </ts>
               <ts e="T1103" id="Seg_12251" n="e" s="T1102">что </ts>
               <ts e="T1104" id="Seg_12253" n="e" s="T1103">еще </ts>
               <ts e="T1105" id="Seg_12255" n="e" s="T1104">хочешь </ts>
               <ts e="T1106" id="Seg_12257" n="e" s="T1105">рассказать? </ts>
            </ts>
            <ts e="T1117" id="Seg_12258" n="sc" s="T1107">
               <ts e="T1108" id="Seg_12260" n="e" s="T1107">(Что </ts>
               <ts e="T1109" id="Seg_12262" n="e" s="T1108">себе=) </ts>
               <ts e="T1110" id="Seg_12264" n="e" s="T1109">Что </ts>
               <ts e="T1111" id="Seg_12266" n="e" s="T1110">тебе </ts>
               <ts e="T1112" id="Seg_12268" n="e" s="T1111">нравится, </ts>
               <ts e="T1113" id="Seg_12270" n="e" s="T1112">каждое </ts>
               <ts e="T1114" id="Seg_12272" n="e" s="T1113">твое </ts>
               <ts e="T1115" id="Seg_12274" n="e" s="T1114">слово — </ts>
               <ts e="T1116" id="Seg_12276" n="e" s="T1115">это </ts>
               <ts e="T1117" id="Seg_12278" n="e" s="T1116">золото. </ts>
            </ts>
            <ts e="T1141" id="Seg_12279" n="sc" s="T1120">
               <ts e="T1122" id="Seg_12281" n="e" s="T1120">Все </ts>
               <ts e="T1123" id="Seg_12283" n="e" s="T1122">равно </ts>
               <ts e="T1125" id="Seg_12285" n="e" s="T1123">о </ts>
               <ts e="T1127" id="Seg_12287" n="e" s="T1125">чем, </ts>
               <ts e="T1128" id="Seg_12289" n="e" s="T1127">все </ts>
               <ts e="T1129" id="Seg_12291" n="e" s="T1128">равно </ts>
               <ts e="T1131" id="Seg_12293" n="e" s="T1129">о </ts>
               <ts e="T1132" id="Seg_12295" n="e" s="T1131">чем. </ts>
               <ts e="T1134" id="Seg_12297" n="e" s="T1132">Хотя </ts>
               <ts e="T1135" id="Seg_12299" n="e" s="T1134">у </ts>
               <ts e="T1137" id="Seg_12301" n="e" s="T1135">тебя </ts>
               <ts e="T1138" id="Seg_12303" n="e" s="T1137">темы </ts>
               <ts e="T1139" id="Seg_12305" n="e" s="T1138">новые </ts>
               <ts e="T1140" id="Seg_12307" n="e" s="T1139">очень </ts>
               <ts e="T1141" id="Seg_12309" n="e" s="T1140">появились. </ts>
            </ts>
            <ts e="T1152" id="Seg_12310" n="sc" s="T1142">
               <ts e="T1143" id="Seg_12312" n="e" s="T1142">А </ts>
               <ts e="T1144" id="Seg_12314" n="e" s="T1143">можно </ts>
               <ts e="T1145" id="Seg_12316" n="e" s="T1144">и </ts>
               <ts e="T1146" id="Seg_12318" n="e" s="T1145">на </ts>
               <ts e="T1147" id="Seg_12320" n="e" s="T1146">старые </ts>
               <ts e="T1148" id="Seg_12322" n="e" s="T1147">темы, </ts>
               <ts e="T1149" id="Seg_12324" n="e" s="T1148">о </ts>
               <ts e="T1150" id="Seg_12326" n="e" s="T1149">тех </ts>
               <ts e="T1151" id="Seg_12328" n="e" s="T1150">же </ts>
               <ts e="T1152" id="Seg_12330" n="e" s="T1151">вещах. </ts>
            </ts>
            <ts e="T1161" id="Seg_12331" n="sc" s="T1153">
               <ts e="T1154" id="Seg_12333" n="e" s="T1153">Потому </ts>
               <ts e="T1155" id="Seg_12335" n="e" s="T1154">что </ts>
               <ts e="T1156" id="Seg_12337" n="e" s="T1155">неважно </ts>
               <ts e="T1157" id="Seg_12339" n="e" s="T1156">то, </ts>
               <ts e="T1158" id="Seg_12341" n="e" s="T1157">что </ts>
               <ts e="T1159" id="Seg_12343" n="e" s="T1158">расскажешь, </ts>
               <ts e="T1160" id="Seg_12345" n="e" s="T1159">а </ts>
               <ts e="T1161" id="Seg_12347" n="e" s="T1160">как. </ts>
            </ts>
            <ts e="T1172" id="Seg_12348" n="sc" s="T1162">
               <ts e="T1163" id="Seg_12350" n="e" s="T1162">Потому </ts>
               <ts e="T1164" id="Seg_12352" n="e" s="T1163">что </ts>
               <ts e="T1165" id="Seg_12354" n="e" s="T1164">именно </ts>
               <ts e="T1166" id="Seg_12356" n="e" s="T1165">камасинском </ts>
               <ts e="T1167" id="Seg_12358" n="e" s="T1166">языке, </ts>
               <ts e="T1168" id="Seg_12360" n="e" s="T1167">вот </ts>
               <ts e="T1169" id="Seg_12362" n="e" s="T1168">что </ts>
               <ts e="T1170" id="Seg_12364" n="e" s="T1169">это </ts>
               <ts e="T1171" id="Seg_12366" n="e" s="T1170">очень </ts>
               <ts e="T1172" id="Seg_12368" n="e" s="T1171">важное. </ts>
            </ts>
            <ts e="T1194" id="Seg_12369" n="sc" s="T1190">
               <ts e="T1192" id="Seg_12371" n="e" s="T1190">Нет, </ts>
               <ts e="T1193" id="Seg_12373" n="e" s="T1192">не </ts>
               <ts e="T1194" id="Seg_12375" n="e" s="T1193">помню. </ts>
            </ts>
            <ts e="T1197" id="Seg_12376" n="sc" s="T1195">
               <ts e="T1196" id="Seg_12378" n="e" s="T1195">Не </ts>
               <ts e="T1197" id="Seg_12380" n="e" s="T1196">помню. </ts>
            </ts>
            <ts e="T1205" id="Seg_12381" n="sc" s="T1198">
               <ts e="T1199" id="Seg_12383" n="e" s="T1198">Вот, </ts>
               <ts e="T1200" id="Seg_12385" n="e" s="T1199">давай. </ts>
               <ts e="T1201" id="Seg_12387" n="e" s="T1200">Давай </ts>
               <ts e="T1202" id="Seg_12389" n="e" s="T1201">это </ts>
               <ts e="T1203" id="Seg_12391" n="e" s="T1202">прямо </ts>
               <ts e="T1204" id="Seg_12393" n="e" s="T1203">сначала, </ts>
               <ts e="T1205" id="Seg_12395" n="e" s="T1204">да? </ts>
            </ts>
            <ts e="T1218" id="Seg_12396" n="sc" s="T1206">
               <ts e="T1207" id="Seg_12398" n="e" s="T1206">По-камасински, </ts>
               <ts e="T1208" id="Seg_12400" n="e" s="T1207">а </ts>
               <ts e="T1209" id="Seg_12402" n="e" s="T1208">потом </ts>
               <ts e="T1210" id="Seg_12404" n="e" s="T1209">уже </ts>
               <ts e="T1211" id="Seg_12406" n="e" s="T1210">и </ts>
               <ts e="T1212" id="Seg_12408" n="e" s="T1211">расскажешь </ts>
               <ts e="T1213" id="Seg_12410" n="e" s="T1212">снова </ts>
               <ts e="T1214" id="Seg_12412" n="e" s="T1213">по-русски </ts>
               <ts e="T1215" id="Seg_12414" n="e" s="T1214">опять, </ts>
               <ts e="T1216" id="Seg_12416" n="e" s="T1215">как </ts>
               <ts e="T1217" id="Seg_12418" n="e" s="T1216">сейчас </ts>
               <ts e="T1218" id="Seg_12420" n="e" s="T1217">делали. </ts>
            </ts>
            <ts e="T1266" id="Seg_12421" n="sc" s="T1255">
               <ts e="T1266" id="Seg_12423" n="e" s="T1255">((…)). </ts>
            </ts>
            <ts e="T1297" id="Seg_12424" n="sc" s="T1287">
               <ts e="T1288" id="Seg_12426" n="e" s="T1287">А, </ts>
               <ts e="T1289" id="Seg_12428" n="e" s="T1288">как </ts>
               <ts e="T1290" id="Seg_12430" n="e" s="T1289">брат </ts>
               <ts e="T1291" id="Seg_12432" n="e" s="T1290">был </ts>
               <ts e="T1292" id="Seg_12434" n="e" s="T1291">в </ts>
               <ts e="T1293" id="Seg_12436" n="e" s="T1292">плену </ts>
               <ts e="T1294" id="Seg_12438" n="e" s="T1293">в </ts>
               <ts e="T1296" id="Seg_12440" n="e" s="T1294">Финляндии, </ts>
               <ts e="T1297" id="Seg_12442" n="e" s="T1296">да? </ts>
            </ts>
            <ts e="T1317" id="Seg_12443" n="sc" s="T1300">
               <ts e="T1301" id="Seg_12445" n="e" s="T1300">Ага, </ts>
               <ts e="T1303" id="Seg_12447" n="e" s="T1301">там </ts>
               <ts e="T1305" id="Seg_12449" n="e" s="T1303">да, </ts>
               <ts e="T1306" id="Seg_12451" n="e" s="T1305">это </ts>
               <ts e="T1307" id="Seg_12453" n="e" s="T1306">мы </ts>
               <ts e="T1308" id="Seg_12455" n="e" s="T1307">записывали. </ts>
               <ts e="T1309" id="Seg_12457" n="e" s="T1308">Правильно, </ts>
               <ts e="T1310" id="Seg_12459" n="e" s="T1309">про </ts>
               <ts e="T1312" id="Seg_12461" n="e" s="T1310">это </ts>
               <ts e="T1314" id="Seg_12463" n="e" s="T1312">(запись) </ts>
               <ts e="T1317" id="Seg_12465" n="e" s="T1314">есть. </ts>
            </ts>
            <ts e="T1330" id="Seg_12466" n="sc" s="T1323">
               <ts e="T1330" id="Seg_12468" n="e" s="T1323">((…)). </ts>
            </ts>
            <ts e="T1332" id="Seg_12469" n="sc" s="T1331">
               <ts e="T1332" id="Seg_12471" n="e" s="T1331">((…)). </ts>
            </ts>
            <ts e="T1341" id="Seg_12472" n="sc" s="T1333">
               <ts e="T1334" id="Seg_12474" n="e" s="T1333">Да, </ts>
               <ts e="T1335" id="Seg_12476" n="e" s="T1334">говорят, </ts>
               <ts e="T1336" id="Seg_12478" n="e" s="T1335">что </ts>
               <ts e="T1337" id="Seg_12480" n="e" s="T1336">это </ts>
               <ts e="T1338" id="Seg_12482" n="e" s="T1337">записано </ts>
               <ts e="T1339" id="Seg_12484" n="e" s="T1338">всё </ts>
               <ts e="T1340" id="Seg_12486" n="e" s="T1339">очень </ts>
               <ts e="T1341" id="Seg_12488" n="e" s="T1340">хорошо. </ts>
            </ts>
            <ts e="T1349" id="Seg_12489" n="sc" s="T1342">
               <ts e="T1343" id="Seg_12491" n="e" s="T1342">Вчера, </ts>
               <ts e="T1344" id="Seg_12493" n="e" s="T1343">говорят, </ts>
               <ts e="T1345" id="Seg_12495" n="e" s="T1344">очень </ts>
               <ts e="T1346" id="Seg_12497" n="e" s="T1345">хорошо </ts>
               <ts e="T1347" id="Seg_12499" n="e" s="T1346">у </ts>
               <ts e="T1348" id="Seg_12501" n="e" s="T1347">них </ts>
               <ts e="T1349" id="Seg_12503" n="e" s="T1348">получилось. </ts>
            </ts>
            <ts e="T1367" id="Seg_12504" n="sc" s="T1350">
               <ts e="T1351" id="Seg_12506" n="e" s="T1350">Второй </ts>
               <ts e="T1352" id="Seg_12508" n="e" s="T1351">был, </ts>
               <ts e="T1353" id="Seg_12510" n="e" s="T1352">который </ts>
               <ts e="T1354" id="Seg_12512" n="e" s="T1353">сидел </ts>
               <ts e="T1355" id="Seg_12514" n="e" s="T1354">вот, </ts>
               <ts e="T1356" id="Seg_12516" n="e" s="T1355">протягивал </ts>
               <ts e="T1357" id="Seg_12518" n="e" s="T1356">такой </ts>
               <ts e="T1358" id="Seg_12520" n="e" s="T1357">длинной </ts>
               <ts e="T1359" id="Seg_12522" n="e" s="T1358">палкой </ts>
               <ts e="T1360" id="Seg_12524" n="e" s="T1359">микрофон, </ts>
               <ts e="T1361" id="Seg_12526" n="e" s="T1360">вот </ts>
               <ts e="T1362" id="Seg_12528" n="e" s="T1361">он </ts>
               <ts e="T1363" id="Seg_12530" n="e" s="T1362">как </ts>
               <ts e="T1364" id="Seg_12532" n="e" s="T1363">раз </ts>
               <ts e="T1365" id="Seg_12534" n="e" s="T1364">записывал </ts>
               <ts e="T1366" id="Seg_12536" n="e" s="T1365">для </ts>
               <ts e="T1367" id="Seg_12538" n="e" s="T1366">них. </ts>
            </ts>
            <ts e="T1375" id="Seg_12539" n="sc" s="T1374">
               <ts e="T1375" id="Seg_12541" n="e" s="T1374">Ага. </ts>
            </ts>
            <ts e="T1384" id="Seg_12542" n="sc" s="T1376">
               <ts e="T1384" id="Seg_12544" n="e" s="T1376">((…)). </ts>
            </ts>
            <ts e="T1406" id="Seg_12545" n="sc" s="T1390">
               <ts e="T1391" id="Seg_12547" n="e" s="T1390">Так </ts>
               <ts e="T1392" id="Seg_12549" n="e" s="T1391">это </ts>
               <ts e="T1393" id="Seg_12551" n="e" s="T1392">в </ts>
               <ts e="T1394" id="Seg_12553" n="e" s="T1393">радио </ts>
               <ts e="T1395" id="Seg_12555" n="e" s="T1394">будут </ts>
               <ts e="T1396" id="Seg_12557" n="e" s="T1395">передавать, </ts>
               <ts e="T1397" id="Seg_12559" n="e" s="T1396">только </ts>
               <ts e="T1398" id="Seg_12561" n="e" s="T1397">мы </ts>
               <ts e="T1399" id="Seg_12563" n="e" s="T1398">забыли </ts>
               <ts e="T1400" id="Seg_12565" n="e" s="T1399">спросить, </ts>
               <ts e="T1401" id="Seg_12567" n="e" s="T1400">когда </ts>
               <ts e="T1402" id="Seg_12569" n="e" s="T1401">это </ts>
               <ts e="T1403" id="Seg_12571" n="e" s="T1402">будет </ts>
               <ts e="T1404" id="Seg_12573" n="e" s="T1403">передача </ts>
               <ts e="T1405" id="Seg_12575" n="e" s="T1404">в </ts>
               <ts e="T1406" id="Seg_12577" n="e" s="T1405">радио. </ts>
            </ts>
            <ts e="T1417" id="Seg_12578" n="sc" s="T1407">
               <ts e="T1408" id="Seg_12580" n="e" s="T1407">Вот </ts>
               <ts e="T1409" id="Seg_12582" n="e" s="T1408">тот, </ts>
               <ts e="T1410" id="Seg_12584" n="e" s="T1409">который </ts>
               <ts e="T1411" id="Seg_12586" n="e" s="T1410">сидел </ts>
               <ts e="T1412" id="Seg_12588" n="e" s="T1411">сзади, </ts>
               <ts e="T1413" id="Seg_12590" n="e" s="T1412">самый </ts>
               <ts e="T1414" id="Seg_12592" n="e" s="T1413">сзади, </ts>
               <ts e="T1415" id="Seg_12594" n="e" s="T1414">протягивал </ts>
               <ts e="T1416" id="Seg_12596" n="e" s="T1415">микрофон </ts>
               <ts e="T1417" id="Seg_12598" n="e" s="T1416">подальше. </ts>
            </ts>
            <ts e="T1428" id="Seg_12599" n="sc" s="T1418">
               <ts e="T1419" id="Seg_12601" n="e" s="T1418">Такой, </ts>
               <ts e="T1420" id="Seg_12603" n="e" s="T1419">эстонец </ts>
               <ts e="T1421" id="Seg_12605" n="e" s="T1420">он </ts>
               <ts e="T1422" id="Seg_12607" n="e" s="T1421">был, </ts>
               <ts e="T1423" id="Seg_12609" n="e" s="T1422">так </ts>
               <ts e="T1424" id="Seg_12611" n="e" s="T1423">он </ts>
               <ts e="T1425" id="Seg_12613" n="e" s="T1424">для </ts>
               <ts e="T1426" id="Seg_12615" n="e" s="T1425">эстонского </ts>
               <ts e="T1427" id="Seg_12617" n="e" s="T1426">радио </ts>
               <ts e="T1428" id="Seg_12619" n="e" s="T1427">снимал. </ts>
            </ts>
            <ts e="T1430" id="Seg_12620" n="sc" s="T1429">
               <ts e="T1430" id="Seg_12622" n="e" s="T1429">Вот. </ts>
            </ts>
            <ts e="T1445" id="Seg_12623" n="sc" s="T1431">
               <ts e="T1432" id="Seg_12625" n="e" s="T1431">Надо </ts>
               <ts e="T1433" id="Seg_12627" n="e" s="T1432">у </ts>
               <ts e="T1434" id="Seg_12629" n="e" s="T1433">него </ts>
               <ts e="T1435" id="Seg_12631" n="e" s="T1434">спросить, </ts>
               <ts e="T1436" id="Seg_12633" n="e" s="T1435">может </ts>
               <ts e="T1437" id="Seg_12635" n="e" s="T1436">(быть), </ts>
               <ts e="T1438" id="Seg_12637" n="e" s="T1437">сама </ts>
               <ts e="T1439" id="Seg_12639" n="e" s="T1438">послушаешь, </ts>
               <ts e="T1440" id="Seg_12641" n="e" s="T1439">как </ts>
               <ts e="T1441" id="Seg_12643" n="e" s="T1440">по </ts>
               <ts e="T1442" id="Seg_12645" n="e" s="T1441">радио </ts>
               <ts e="T1443" id="Seg_12647" n="e" s="T1442">будешь </ts>
               <ts e="T1445" id="Seg_12649" n="e" s="T1443">говорить. </ts>
            </ts>
            <ts e="T1452" id="Seg_12650" n="sc" s="T1450">
               <ts e="T1451" id="Seg_12652" n="e" s="T1450">А, </ts>
               <ts e="T1452" id="Seg_12654" n="e" s="T1451">((…)). </ts>
            </ts>
            <ts e="T1459" id="Seg_12655" n="sc" s="T1456">
               <ts e="T1459" id="Seg_12657" n="e" s="T1456">Ага. </ts>
            </ts>
            <ts e="T1463" id="Seg_12658" n="sc" s="T1461">
               <ts e="T1462" id="Seg_12660" n="e" s="T1461">Так-с. </ts>
               <ts e="T1463" id="Seg_12662" n="e" s="T1462">Ну… </ts>
            </ts>
            <ts e="T1482" id="Seg_12663" n="sc" s="T1476">
               <ts e="T1479" id="Seg_12665" n="e" s="T1476">Ага, </ts>
               <ts e="T1482" id="Seg_12667" n="e" s="T1479">точно-точно. </ts>
            </ts>
            <ts e="T1506" id="Seg_12668" n="sc" s="T1486">
               <ts e="T1487" id="Seg_12670" n="e" s="T1486">Да, </ts>
               <ts e="T1488" id="Seg_12672" n="e" s="T1487">я </ts>
               <ts e="T1489" id="Seg_12674" n="e" s="T1488">не </ts>
               <ts e="T1490" id="Seg_12676" n="e" s="T1489">знаю, </ts>
               <ts e="T1491" id="Seg_12678" n="e" s="T1490">вот </ts>
               <ts e="T1492" id="Seg_12680" n="e" s="T1491">было </ts>
               <ts e="T1493" id="Seg_12682" n="e" s="T1492">так </ts>
               <ts e="T1494" id="Seg_12684" n="e" s="T1493">это </ts>
               <ts e="T1495" id="Seg_12686" n="e" s="T1494">всё </ts>
               <ts e="T1496" id="Seg_12688" n="e" s="T1495">спешно, </ts>
               <ts e="T1497" id="Seg_12690" n="e" s="T1496">что </ts>
               <ts e="T1498" id="Seg_12692" n="e" s="T1497">я </ts>
               <ts e="T1499" id="Seg_12694" n="e" s="T1498">не </ts>
               <ts e="T1500" id="Seg_12696" n="e" s="T1499">успел </ts>
               <ts e="T1501" id="Seg_12698" n="e" s="T1500">этого </ts>
               <ts e="T1502" id="Seg_12700" n="e" s="T1501">человека </ts>
               <ts e="T1503" id="Seg_12702" n="e" s="T1502">спросить, </ts>
               <ts e="T1504" id="Seg_12704" n="e" s="T1503">когда </ts>
               <ts e="T1505" id="Seg_12706" n="e" s="T1504">будет </ts>
               <ts e="T1506" id="Seg_12708" n="e" s="T1505">радио. </ts>
            </ts>
            <ts e="T1526" id="Seg_12709" n="sc" s="T1507">
               <ts e="T1508" id="Seg_12711" n="e" s="T1507">Я </ts>
               <ts e="T1509" id="Seg_12713" n="e" s="T1508">и </ts>
               <ts e="T1510" id="Seg_12715" n="e" s="T1509">сам </ts>
               <ts e="T1511" id="Seg_12717" n="e" s="T1510">интересно </ts>
               <ts e="T1512" id="Seg_12719" n="e" s="T1511">мне, </ts>
               <ts e="T1513" id="Seg_12721" n="e" s="T1512">потому </ts>
               <ts e="T1514" id="Seg_12723" n="e" s="T1513">что </ts>
               <ts e="T1515" id="Seg_12725" n="e" s="T1514">я </ts>
               <ts e="T1516" id="Seg_12727" n="e" s="T1515">тоже </ts>
               <ts e="T1517" id="Seg_12729" n="e" s="T1516">говорил, </ts>
               <ts e="T1518" id="Seg_12731" n="e" s="T1517">меня </ts>
               <ts e="T1519" id="Seg_12733" n="e" s="T1518">спрашивал </ts>
               <ts e="T1520" id="Seg_12735" n="e" s="T1519">насчет </ts>
               <ts e="T1521" id="Seg_12737" n="e" s="T1520">тебя, </ts>
               <ts e="T1522" id="Seg_12739" n="e" s="T1521">я </ts>
               <ts e="T1523" id="Seg_12741" n="e" s="T1522">рассказал </ts>
               <ts e="T1524" id="Seg_12743" n="e" s="T1523">про </ts>
               <ts e="T1525" id="Seg_12745" n="e" s="T1524">тебя </ts>
               <ts e="T1526" id="Seg_12747" n="e" s="T1525">там. </ts>
            </ts>
            <ts e="T1559" id="Seg_12748" n="sc" s="T1544">
               <ts e="T1545" id="Seg_12750" n="e" s="T1544">Ну, </ts>
               <ts e="T1546" id="Seg_12752" n="e" s="T1545">а </ts>
               <ts e="T1547" id="Seg_12754" n="e" s="T1546">может </ts>
               <ts e="T1548" id="Seg_12756" n="e" s="T1547">ты </ts>
               <ts e="T1549" id="Seg_12758" n="e" s="T1548">хочешь </ts>
               <ts e="T1550" id="Seg_12760" n="e" s="T1549">что </ts>
               <ts e="T1551" id="Seg_12762" n="e" s="T1550">сказать </ts>
               <ts e="T1552" id="Seg_12764" n="e" s="T1551">и </ts>
               <ts e="T1553" id="Seg_12766" n="e" s="T1552">о </ts>
               <ts e="T1554" id="Seg_12768" n="e" s="T1553">вчерашнем </ts>
               <ts e="T1555" id="Seg_12770" n="e" s="T1554">дне, </ts>
               <ts e="T1556" id="Seg_12772" n="e" s="T1555">например, </ts>
               <ts e="T1557" id="Seg_12774" n="e" s="T1556">если </ts>
               <ts e="T1558" id="Seg_12776" n="e" s="T1557">тебе </ts>
               <ts e="T1559" id="Seg_12778" n="e" s="T1558">понравится. </ts>
            </ts>
            <ts e="T1571" id="Seg_12779" n="sc" s="T1560">
               <ts e="T1561" id="Seg_12781" n="e" s="T1560">Как </ts>
               <ts e="T1562" id="Seg_12783" n="e" s="T1561">вчера </ts>
               <ts e="T1563" id="Seg_12785" n="e" s="T1562">в </ts>
               <ts e="T1564" id="Seg_12787" n="e" s="T1563">гости </ts>
               <ts e="T1565" id="Seg_12789" n="e" s="T1564">ходили, </ts>
               <ts e="T1566" id="Seg_12791" n="e" s="T1565">все </ts>
               <ts e="T1567" id="Seg_12793" n="e" s="T1566">такое, </ts>
               <ts e="T1568" id="Seg_12795" n="e" s="T1567">как </ts>
               <ts e="T1569" id="Seg_12797" n="e" s="T1568">спрашивали, </ts>
               <ts e="T1570" id="Seg_12799" n="e" s="T1569">как </ts>
               <ts e="T1571" id="Seg_12801" n="e" s="T1570">было. </ts>
            </ts>
            <ts e="T1600" id="Seg_12802" n="sc" s="T1572">
               <ts e="T1573" id="Seg_12804" n="e" s="T1572">Что </ts>
               <ts e="T1574" id="Seg_12806" n="e" s="T1573">ты </ts>
               <ts e="T1575" id="Seg_12808" n="e" s="T1574">им </ts>
               <ts e="T1576" id="Seg_12810" n="e" s="T1575">рассказывала, </ts>
               <ts e="T1577" id="Seg_12812" n="e" s="T1576">так, </ts>
               <ts e="T1578" id="Seg_12814" n="e" s="T1577">коротко. </ts>
               <ts e="T1579" id="Seg_12816" n="e" s="T1578">Просто </ts>
               <ts e="T1580" id="Seg_12818" n="e" s="T1579">не </ts>
               <ts e="T1581" id="Seg_12820" n="e" s="T1580">повторять </ts>
               <ts e="T1582" id="Seg_12822" n="e" s="T1581">обязательно, </ts>
               <ts e="T1583" id="Seg_12824" n="e" s="T1582">ну </ts>
               <ts e="T1584" id="Seg_12826" n="e" s="T1583">просто </ts>
               <ts e="T1585" id="Seg_12828" n="e" s="T1584">так, </ts>
               <ts e="T1586" id="Seg_12830" n="e" s="T1585">о </ts>
               <ts e="T1587" id="Seg_12832" n="e" s="T1586">чем </ts>
               <ts e="T1588" id="Seg_12834" n="e" s="T1587">там </ts>
               <ts e="T1589" id="Seg_12836" n="e" s="T1588">говорили, </ts>
               <ts e="T1590" id="Seg_12838" n="e" s="T1589">как </ts>
               <ts e="T1591" id="Seg_12840" n="e" s="T1590">всё </ts>
               <ts e="T1592" id="Seg_12842" n="e" s="T1591">это </ts>
               <ts e="T1593" id="Seg_12844" n="e" s="T1592">было, </ts>
               <ts e="T1594" id="Seg_12846" n="e" s="T1593">сколько </ts>
               <ts e="T1595" id="Seg_12848" n="e" s="T1594">там </ts>
               <ts e="T1596" id="Seg_12850" n="e" s="T1595">было </ts>
               <ts e="T1597" id="Seg_12852" n="e" s="T1596">людей, </ts>
               <ts e="T1598" id="Seg_12854" n="e" s="T1597">и </ts>
               <ts e="T1599" id="Seg_12856" n="e" s="T1598">всё </ts>
               <ts e="T1600" id="Seg_12858" n="e" s="T1599">такое. </ts>
            </ts>
            <ts e="T1603" id="Seg_12859" n="sc" s="T1601">
               <ts e="T1602" id="Seg_12861" n="e" s="T1601">Можно, </ts>
               <ts e="T1603" id="Seg_12863" n="e" s="T1602">думаешь? </ts>
            </ts>
            <ts e="T1611" id="Seg_12864" n="sc" s="T1608">
               <ts e="T1609" id="Seg_12866" n="e" s="T1608">Ну, </ts>
               <ts e="T1610" id="Seg_12868" n="e" s="T1609">давай, </ts>
               <ts e="T1611" id="Seg_12870" n="e" s="T1610">давай. </ts>
            </ts>
            <ts e="T1626" id="Seg_12871" n="sc" s="T1620">
               <ts e="T1622" id="Seg_12873" n="e" s="T1620">Да, </ts>
               <ts e="T1624" id="Seg_12875" n="e" s="T1622">да, </ts>
               <ts e="T1626" id="Seg_12877" n="e" s="T1624">действительно. </ts>
            </ts>
            <ts e="T1633" id="Seg_12878" n="sc" s="T1632">
               <ts e="T1633" id="Seg_12880" n="e" s="T1632">Да. </ts>
            </ts>
            <ts e="T1645" id="Seg_12881" n="sc" s="T1637">
               <ts e="T1639" id="Seg_12883" n="e" s="T1637">Точно </ts>
               <ts e="T1640" id="Seg_12885" n="e" s="T1639">так, </ts>
               <ts e="T1642" id="Seg_12887" n="e" s="T1640">не </ts>
               <ts e="T1644" id="Seg_12889" n="e" s="T1642">с </ts>
               <ts e="T1645" id="Seg_12891" n="e" s="T1644">кем. </ts>
            </ts>
            <ts e="T1708" id="Seg_12892" n="sc" s="T1704">
               <ts e="T1706" id="Seg_12894" n="e" s="T1704">Sĭreʔpne, </ts>
               <ts e="T1708" id="Seg_12896" n="e" s="T1706">да? </ts>
            </ts>
            <ts e="T1711" id="Seg_12897" n="sc" s="T1709">
               <ts e="T1711" id="Seg_12899" n="e" s="T1709">Ага. </ts>
            </ts>
            <ts e="T1716" id="Seg_12900" n="sc" s="T1712">
               <ts e="T1716" id="Seg_12902" n="e" s="T1712">((…)). </ts>
            </ts>
            <ts e="T1755" id="Seg_12903" n="sc" s="T1753">
               <ts e="T1755" id="Seg_12905" n="e" s="T1753">Ага. </ts>
            </ts>
            <ts e="T1760" id="Seg_12906" n="sc" s="T1758">
               <ts e="T1760" id="Seg_12908" n="e" s="T1758">Ага. </ts>
            </ts>
            <ts e="T1774" id="Seg_12909" n="sc" s="T1762">
               <ts e="T1763" id="Seg_12911" n="e" s="T1762">Ну, </ts>
               <ts e="T1764" id="Seg_12913" n="e" s="T1763">а </ts>
               <ts e="T1765" id="Seg_12915" n="e" s="T1764">ты </ts>
               <ts e="T1766" id="Seg_12917" n="e" s="T1765">повтори </ts>
               <ts e="T1767" id="Seg_12919" n="e" s="T1766">по-русски, </ts>
               <ts e="T1768" id="Seg_12921" n="e" s="T1767">просто, </ts>
               <ts e="T1769" id="Seg_12923" n="e" s="T1768">так </ts>
               <ts e="T1770" id="Seg_12925" n="e" s="T1769">сказать, </ts>
               <ts e="T1771" id="Seg_12927" n="e" s="T1770">переведи </ts>
               <ts e="T1772" id="Seg_12929" n="e" s="T1771">это </ts>
               <ts e="T1773" id="Seg_12931" n="e" s="T1772">еще </ts>
               <ts e="T1774" id="Seg_12933" n="e" s="T1773">раз. </ts>
            </ts>
            <ts e="T1815" id="Seg_12934" n="sc" s="T1807">
               <ts e="T1808" id="Seg_12936" n="e" s="T1807">Так </ts>
               <ts e="T1809" id="Seg_12938" n="e" s="T1808">всё </ts>
               <ts e="T1810" id="Seg_12940" n="e" s="T1809">и </ts>
               <ts e="T1811" id="Seg_12942" n="e" s="T1810">былое </ts>
               <ts e="T1812" id="Seg_12944" n="e" s="T1811">и </ts>
               <ts e="T1813" id="Seg_12946" n="e" s="T1812">старое </ts>
               <ts e="T1814" id="Seg_12948" n="e" s="T1813">можно </ts>
               <ts e="T1815" id="Seg_12950" n="e" s="T1814">вспомнить. </ts>
            </ts>
            <ts e="T1819" id="Seg_12951" n="sc" s="T1816">
               <ts e="T1817" id="Seg_12953" n="e" s="T1816">Что </ts>
               <ts e="T1818" id="Seg_12955" n="e" s="T1817">только </ts>
               <ts e="T1819" id="Seg_12957" n="e" s="T1818">хочешь. </ts>
            </ts>
            <ts e="T1842" id="Seg_12958" n="sc" s="T1820">
               <ts e="T1821" id="Seg_12960" n="e" s="T1820">Тут </ts>
               <ts e="T1822" id="Seg_12962" n="e" s="T1821">он </ts>
               <ts e="T1823" id="Seg_12964" n="e" s="T1822">предлагал, </ts>
               <ts e="T1824" id="Seg_12966" n="e" s="T1823">что, </ts>
               <ts e="T1825" id="Seg_12968" n="e" s="T1824">но </ts>
               <ts e="T1826" id="Seg_12970" n="e" s="T1825">мы </ts>
               <ts e="T1827" id="Seg_12972" n="e" s="T1826">это </ts>
               <ts e="T1828" id="Seg_12974" n="e" s="T1827">говорили, </ts>
               <ts e="T1829" id="Seg_12976" n="e" s="T1828">ну </ts>
               <ts e="T1830" id="Seg_12978" n="e" s="T1829">это </ts>
               <ts e="T1831" id="Seg_12980" n="e" s="T1830">можно </ts>
               <ts e="T1832" id="Seg_12982" n="e" s="T1831">всё </ts>
               <ts e="T1833" id="Seg_12984" n="e" s="T1832">еще </ts>
               <ts e="T1834" id="Seg_12986" n="e" s="T1833">раз </ts>
               <ts e="T1835" id="Seg_12988" n="e" s="T1834">сказать: </ts>
               <ts e="T1836" id="Seg_12990" n="e" s="T1835">о </ts>
               <ts e="T1837" id="Seg_12992" n="e" s="T1836">матери, </ts>
               <ts e="T1838" id="Seg_12994" n="e" s="T1837">об </ts>
               <ts e="T1839" id="Seg_12996" n="e" s="T1838">отце, </ts>
               <ts e="T1840" id="Seg_12998" n="e" s="T1839">о </ts>
               <ts e="T1841" id="Seg_13000" n="e" s="T1840">чем </ts>
               <ts e="T1842" id="Seg_13002" n="e" s="T1841">угодно. </ts>
            </ts>
            <ts e="T1846" id="Seg_13003" n="sc" s="T1843">
               <ts e="T1846" id="Seg_13005" n="e" s="T1843">((…)). </ts>
            </ts>
            <ts e="T1859" id="Seg_13006" n="sc" s="T1852">
               <ts e="T1854" id="Seg_13008" n="e" s="T1852">Ага, </ts>
               <ts e="T1855" id="Seg_13010" n="e" s="T1854">ну </ts>
               <ts e="T1856" id="Seg_13012" n="e" s="T1855">ничего, </ts>
               <ts e="T1857" id="Seg_13014" n="e" s="T1856">мы </ts>
               <ts e="T1858" id="Seg_13016" n="e" s="T1857">помаленьку </ts>
               <ts e="T1859" id="Seg_13018" n="e" s="T1858">можем. </ts>
            </ts>
            <ts e="T1861" id="Seg_13019" n="sc" s="T1860">
               <ts e="T1861" id="Seg_13021" n="e" s="T1860">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T9" id="Seg_13022" s="T7">PKZ_19700819_09342-2a.KA.001 (002)</ta>
            <ta e="T25" id="Seg_13023" s="T11">PKZ_19700819_09342-2a.KA.002 (003)</ta>
            <ta e="T27" id="Seg_13024" s="T26">PKZ_19700819_09342-2a.KA.003 (004)</ta>
            <ta e="T29" id="Seg_13025" s="T28">PKZ_19700819_09342-2a.KA.004 (005)</ta>
            <ta e="T32" id="Seg_13026" s="T30">PKZ_19700819_09342-2a.KA.005 (006)</ta>
            <ta e="T40" id="Seg_13027" s="T33">PKZ_19700819_09342-2a.KA.006 (007)</ta>
            <ta e="T42" id="Seg_13028" s="T41">PKZ_19700819_09342-2a.KA.007 (008)</ta>
            <ta e="T49" id="Seg_13029" s="T48">PKZ_19700819_09342-2a.KA.008 (010)</ta>
            <ta e="T51" id="Seg_13030" s="T50">PKZ_19700819_09342-2a.KA.009 (011)</ta>
            <ta e="T53" id="Seg_13031" s="T52">PKZ_19700819_09342-2a.KA.010 (012)</ta>
            <ta e="T58" id="Seg_13032" s="T54">PKZ_19700819_09342-2a.KA.011 (013)</ta>
            <ta e="T71" id="Seg_13033" s="T59">PKZ_19700819_09342-2a.KA.012 (014)</ta>
            <ta e="T80" id="Seg_13034" s="T72">PKZ_19700819_09342-2a.KA.013 (015)</ta>
            <ta e="T93" id="Seg_13035" s="T81">PKZ_19700819_09342-2a.KA.014 (016)</ta>
            <ta e="T99" id="Seg_13036" s="T94">PKZ_19700819_09342-2a.KA.015 (018)</ta>
            <ta e="T110" id="Seg_13037" s="T100">PKZ_19700819_09342-2a.KA.016 (019)</ta>
            <ta e="T133" id="Seg_13038" s="T118">PKZ_19700819_09342-2a.KA.017 (021)</ta>
            <ta e="T162" id="Seg_13039" s="T158">PKZ_19700819_09342-2a.KA.018 (026)</ta>
            <ta e="T185" id="Seg_13040" s="T178">PKZ_19700819_09342-2a.KA.019 (028)</ta>
            <ta e="T221" id="Seg_13041" s="T210">PKZ_19700819_09342-2a.KA.020 (031)</ta>
            <ta e="T223" id="Seg_13042" s="T222">PKZ_19700819_09342-2a.KA.021 (033)</ta>
            <ta e="T227" id="Seg_13043" s="T223">PKZ_19700819_09342-2a.KA.022 (034)</ta>
            <ta e="T243" id="Seg_13044" s="T233">PKZ_19700819_09342-2a.KA.023 (036)</ta>
            <ta e="T278" id="Seg_13045" s="T276">PKZ_19700819_09342-2a.KA.024 (044)</ta>
            <ta e="T311" id="Seg_13046" s="T297">PKZ_19700819_09342-2a.KA.025 (046)</ta>
            <ta e="T312" id="Seg_13047" s="T311">PKZ_19700819_09342-2a.KA.026 (047)</ta>
            <ta e="T314" id="Seg_13048" s="T313">PKZ_19700819_09342-2a.KA.027 (048)</ta>
            <ta e="T633" id="Seg_13049" s="T630">PKZ_19700819_09342-2a.KA.028 (105)</ta>
            <ta e="T639" id="Seg_13050" s="T638">PKZ_19700819_09342-2a.KA.029 (107)</ta>
            <ta e="T643" id="Seg_13051" s="T640">PKZ_19700819_09342-2a.KA.030 (108)</ta>
            <ta e="T655" id="Seg_13052" s="T653">PKZ_19700819_09342-2a.KA.031 (110)</ta>
            <ta e="T664" id="Seg_13053" s="T657">PKZ_19700819_09342-2a.KA.032 (111)</ta>
            <ta e="T673" id="Seg_13054" s="T665">PKZ_19700819_09342-2a.KA.033 (112)</ta>
            <ta e="T676" id="Seg_13055" s="T674">PKZ_19700819_09342-2a.KA.034 (114)</ta>
            <ta e="T679" id="Seg_13056" s="T678">PKZ_19700819_09342-2a.KA.035 (116)</ta>
            <ta e="T924" id="Seg_13057" s="T918">PKZ_19700819_09342-2a.KA.036 (142)</ta>
            <ta e="T932" id="Seg_13058" s="T928">PKZ_19700819_09342-2a.KA.037 (143)</ta>
            <ta e="T960" id="Seg_13059" s="T953">PKZ_19700819_09342-2a.KA.038 (147)</ta>
            <ta e="T1011" id="Seg_13060" s="T1010">PKZ_19700819_09342-2a.KA.039 (153)</ta>
            <ta e="T1013" id="Seg_13061" s="T1012">PKZ_19700819_09342-2a.KA.040 (154)</ta>
            <ta e="T1015" id="Seg_13062" s="T1014">PKZ_19700819_09342-2a.KA.041 (155)</ta>
            <ta e="T1025" id="Seg_13063" s="T1015">PKZ_19700819_09342-2a.KA.042 (156)</ta>
            <ta e="T1045" id="Seg_13064" s="T1029">PKZ_19700819_09342-2a.KA.043 (158)</ta>
            <ta e="T1053" id="Seg_13065" s="T1048">PKZ_19700819_09342-2a.KA.044 (160)</ta>
            <ta e="T1092" id="Seg_13066" s="T1071">PKZ_19700819_09342-2a.KA.045 (163)</ta>
            <ta e="T1097" id="Seg_13067" s="T1093">PKZ_19700819_09342-2a.KA.046 (164)</ta>
            <ta e="T1099" id="Seg_13068" s="T1098">PKZ_19700819_09342-2a.KA.047 (165)</ta>
            <ta e="T1106" id="Seg_13069" s="T1100">PKZ_19700819_09342-2a.KA.048 (166)</ta>
            <ta e="T1117" id="Seg_13070" s="T1107">PKZ_19700819_09342-2a.KA.049 (167)</ta>
            <ta e="T1132" id="Seg_13071" s="T1120">PKZ_19700819_09342-2a.KA.050 (169)</ta>
            <ta e="T1141" id="Seg_13072" s="T1132">PKZ_19700819_09342-2a.KA.051 (171)</ta>
            <ta e="T1152" id="Seg_13073" s="T1142">PKZ_19700819_09342-2a.KA.052 (172)</ta>
            <ta e="T1161" id="Seg_13074" s="T1153">PKZ_19700819_09342-2a.KA.053 (173)</ta>
            <ta e="T1172" id="Seg_13075" s="T1162">PKZ_19700819_09342-2a.KA.054 (174)</ta>
            <ta e="T1194" id="Seg_13076" s="T1190">PKZ_19700819_09342-2a.KA.055 (176)</ta>
            <ta e="T1197" id="Seg_13077" s="T1195">PKZ_19700819_09342-2a.KA.056 (177)</ta>
            <ta e="T1200" id="Seg_13078" s="T1198">PKZ_19700819_09342-2a.KA.057 (178)</ta>
            <ta e="T1205" id="Seg_13079" s="T1200">PKZ_19700819_09342-2a.KA.058 (179)</ta>
            <ta e="T1218" id="Seg_13080" s="T1206">PKZ_19700819_09342-2a.KA.059 (180)</ta>
            <ta e="T1266" id="Seg_13081" s="T1255">PKZ_19700819_09342-2a.KA.060 (188)</ta>
            <ta e="T1297" id="Seg_13082" s="T1287">PKZ_19700819_09342-2a.KA.061 (194)</ta>
            <ta e="T1308" id="Seg_13083" s="T1300">PKZ_19700819_09342-2a.KA.062 (196)</ta>
            <ta e="T1317" id="Seg_13084" s="T1308">PKZ_19700819_09342-2a.KA.063 (197)</ta>
            <ta e="T1330" id="Seg_13085" s="T1323">PKZ_19700819_09342-2a.KA.064 (199)</ta>
            <ta e="T1332" id="Seg_13086" s="T1331">PKZ_19700819_09342-2a.KA.065 (201)</ta>
            <ta e="T1341" id="Seg_13087" s="T1333">PKZ_19700819_09342-2a.KA.066 (202)</ta>
            <ta e="T1349" id="Seg_13088" s="T1342">PKZ_19700819_09342-2a.KA.067 (203)</ta>
            <ta e="T1367" id="Seg_13089" s="T1350">PKZ_19700819_09342-2a.KA.068 (204)</ta>
            <ta e="T1375" id="Seg_13090" s="T1374">PKZ_19700819_09342-2a.KA.069 (206)</ta>
            <ta e="T1384" id="Seg_13091" s="T1376">PKZ_19700819_09342-2a.KA.070 (207)</ta>
            <ta e="T1406" id="Seg_13092" s="T1390">PKZ_19700819_09342-2a.KA.071 (209)</ta>
            <ta e="T1417" id="Seg_13093" s="T1407">PKZ_19700819_09342-2a.KA.072 (210)</ta>
            <ta e="T1428" id="Seg_13094" s="T1418">PKZ_19700819_09342-2a.KA.073 (211)</ta>
            <ta e="T1430" id="Seg_13095" s="T1429">PKZ_19700819_09342-2a.KA.074 (212)</ta>
            <ta e="T1445" id="Seg_13096" s="T1431">PKZ_19700819_09342-2a.KA.075 (213)</ta>
            <ta e="T1452" id="Seg_13097" s="T1450">PKZ_19700819_09342-2a.KA.076 (215)</ta>
            <ta e="T1459" id="Seg_13098" s="T1456">PKZ_19700819_09342-2a.KA.077 (217)</ta>
            <ta e="T1462" id="Seg_13099" s="T1461">PKZ_19700819_09342-2a.KA.078 (218)</ta>
            <ta e="T1463" id="Seg_13100" s="T1462">PKZ_19700819_09342-2a.KA.079 (219)</ta>
            <ta e="T1482" id="Seg_13101" s="T1476">PKZ_19700819_09342-2a.KA.080 (222)</ta>
            <ta e="T1506" id="Seg_13102" s="T1486">PKZ_19700819_09342-2a.KA.081 (223)</ta>
            <ta e="T1526" id="Seg_13103" s="T1507">PKZ_19700819_09342-2a.KA.082 (224)</ta>
            <ta e="T1559" id="Seg_13104" s="T1544">PKZ_19700819_09342-2a.KA.083 (227)</ta>
            <ta e="T1571" id="Seg_13105" s="T1560">PKZ_19700819_09342-2a.KA.084 (228)</ta>
            <ta e="T1578" id="Seg_13106" s="T1572">PKZ_19700819_09342-2a.KA.085 (229)</ta>
            <ta e="T1600" id="Seg_13107" s="T1578">PKZ_19700819_09342-2a.KA.086 (230)</ta>
            <ta e="T1603" id="Seg_13108" s="T1601">PKZ_19700819_09342-2a.KA.087 (231)</ta>
            <ta e="T1611" id="Seg_13109" s="T1608">PKZ_19700819_09342-2a.KA.088 (233)</ta>
            <ta e="T1626" id="Seg_13110" s="T1620">PKZ_19700819_09342-2a.KA.089 (235)</ta>
            <ta e="T1633" id="Seg_13111" s="T1632">PKZ_19700819_09342-2a.KA.090 (237)</ta>
            <ta e="T1645" id="Seg_13112" s="T1637">PKZ_19700819_09342-2a.KA.091 (239)</ta>
            <ta e="T1708" id="Seg_13113" s="T1704">PKZ_19700819_09342-2a.KA.092 (249)</ta>
            <ta e="T1711" id="Seg_13114" s="T1709">PKZ_19700819_09342-2a.KA.093 (251)</ta>
            <ta e="T1716" id="Seg_13115" s="T1712">PKZ_19700819_09342-2a.KA.094 (252)</ta>
            <ta e="T1755" id="Seg_13116" s="T1753">PKZ_19700819_09342-2a.KA.095 (262)</ta>
            <ta e="T1760" id="Seg_13117" s="T1758">PKZ_19700819_09342-2a.KA.096 (263)</ta>
            <ta e="T1774" id="Seg_13118" s="T1762">PKZ_19700819_09342-2a.KA.097 (264)</ta>
            <ta e="T1815" id="Seg_13119" s="T1807">PKZ_19700819_09342-2a.KA.098 (270)</ta>
            <ta e="T1819" id="Seg_13120" s="T1816">PKZ_19700819_09342-2a.KA.099 (271)</ta>
            <ta e="T1842" id="Seg_13121" s="T1820">PKZ_19700819_09342-2a.KA.100 (272)</ta>
            <ta e="T1846" id="Seg_13122" s="T1843">PKZ_19700819_09342-2a.KA.101 (273)</ta>
            <ta e="T1859" id="Seg_13123" s="T1852">PKZ_19700819_09342-2a.KA.102 (275)</ta>
            <ta e="T1861" id="Seg_13124" s="T1860">PKZ_19700819_09342-2a.KA.103 (276)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T9" id="Seg_13125" s="T7">А. </ta>
            <ta e="T25" id="Seg_13126" s="T11">Вот, и испортилась у него в общем машина, а у меня не испортилась никогда. </ta>
            <ta e="T27" id="Seg_13127" s="T26">((…)) </ta>
            <ta e="T29" id="Seg_13128" s="T28">((…)) </ta>
            <ta e="T32" id="Seg_13129" s="T30">Еще какой. </ta>
            <ta e="T40" id="Seg_13130" s="T33">Часы не любит, говорит все будет слышно. </ta>
            <ta e="T42" id="Seg_13131" s="T41">((…)) </ta>
            <ta e="T49" id="Seg_13132" s="T48">((…)) </ta>
            <ta e="T51" id="Seg_13133" s="T50">((…)) </ta>
            <ta e="T53" id="Seg_13134" s="T52">((…)) </ta>
            <ta e="T58" id="Seg_13135" s="T54">Значит, мы сделаем так. </ta>
            <ta e="T71" id="Seg_13136" s="T59">Я сначала пару слов сам скажу, чтобы знать, какая пленка, что здесь. </ta>
            <ta e="T80" id="Seg_13137" s="T72">А потом, будь добра, расскажи о своей поездке. </ta>
            <ta e="T93" id="Seg_13138" s="T81">Хоть как сначала все, как он написал тебе, как потом приехал. </ta>
            <ta e="T99" id="Seg_13139" s="T94">Как полетели, всё-всё. </ta>
            <ta e="T110" id="Seg_13140" s="T100">А потом маленько по-русски можно. </ta>
            <ta e="T133" id="Seg_13141" s="T118">Ну, я в Финляндии сидел, работал, там никак не приехать. </ta>
            <ta e="T162" id="Seg_13142" s="T158">Ну сейчас… </ta>
            <ta e="T185" id="Seg_13143" s="T178">Ну, сейчас это все расскажешь. </ta>
            <ta e="T221" id="Seg_13144" s="T210">А он ночевал у тебя, прямо у тебя, да? </ta>
            <ta e="T223" id="Seg_13145" s="T222">Ага. </ta>
            <ta e="T227" id="Seg_13146" s="T223">Ага, ага. </ta>
            <ta e="T243" id="Seg_13147" s="T233">В ту баню, в которую я ходил. </ta>
            <ta e="T278" id="Seg_13148" s="T276">Хорошо. </ta>
            <ta e="T311" id="Seg_13149" s="T297">Сейчас поговорим, только мне придется сначала сказать, значит, какая пленка и всё. </ta>
            <ta e="T312" id="Seg_13150" s="T311">((…)) </ta>
            <ta e="T314" id="Seg_13151" s="T313">((…)) </ta>
            <ta e="T633" id="Seg_13152" s="T630">Ugandə jakše dʼăbaktərlial. </ta>
            <ta e="T639" id="Seg_13153" s="T638">Хорошо! </ta>
            <ta e="T643" id="Seg_13154" s="T640">Прекрасно. </ta>
            <ta e="T655" id="Seg_13155" s="T653">Нет. </ta>
            <ta e="T664" id="Seg_13156" s="T657">Это может ты еще, мы тоже запишем. </ta>
            <ta e="T673" id="Seg_13157" s="T665">Если это самое расскажешь по-русски, объяснишь. </ta>
            <ta e="T676" id="Seg_13158" s="T674">Ага. </ta>
            <ta e="T679" id="Seg_13159" s="T678">((…)) </ta>
            <ta e="T924" id="Seg_13160" s="T918">В Тарту, в Тарту. </ta>
            <ta e="T932" id="Seg_13161" s="T928">А, сначала в Таллин. </ta>
            <ta e="T960" id="Seg_13162" s="T953">Все равно, все равно. </ta>
            <ta e="T1011" id="Seg_13163" s="T1010">((…)) </ta>
            <ta e="T1013" id="Seg_13164" s="T1012">Хорошо. </ta>
            <ta e="T1015" id="Seg_13165" s="T1014">Прекрасно. </ta>
            <ta e="T1025" id="Seg_13166" s="T1015">Он говорит, что очень хорошо все (вот) на машинке получилось. </ta>
            <ta e="T1045" id="Seg_13167" s="T1029">Нет, не слыхать, и сейчас закрыта к тому же дверь. </ta>
            <ta e="T1053" id="Seg_13168" s="T1048">Убежали, может быть. </ta>
            <ta e="T1092" id="Seg_13169" s="T1071">Ну, я вот (машиниста) не нашел, мы это все разошлись, и я его по городу искал, всюду ходил, пока нашел, встретились. </ta>
            <ta e="T1097" id="Seg_13170" s="T1093">Там где-то в центре. </ta>
            <ta e="T1099" id="Seg_13171" s="T1098">Так. </ta>
            <ta e="T1106" id="Seg_13172" s="T1100">Ну, ты что еще хочешь рассказать? </ta>
            <ta e="T1117" id="Seg_13173" s="T1107">(Что себе=) Что тебе нравится, каждое твое слово — это золото. </ta>
            <ta e="T1132" id="Seg_13174" s="T1120">Все равно о чем, все равно о чем. </ta>
            <ta e="T1141" id="Seg_13175" s="T1132">Хотя у тебя темы новые очень появились. </ta>
            <ta e="T1152" id="Seg_13176" s="T1142">А можно и на старые темы, о тех же вещах. </ta>
            <ta e="T1161" id="Seg_13177" s="T1153">Потому что неважно то, что расскажешь, а как. </ta>
            <ta e="T1172" id="Seg_13178" s="T1162">Потому что именно камасинском языке, вот что это очень важное. </ta>
            <ta e="T1194" id="Seg_13179" s="T1190">Нет, не помню. </ta>
            <ta e="T1197" id="Seg_13180" s="T1195">Не помню. </ta>
            <ta e="T1200" id="Seg_13181" s="T1198">Вот, давай. </ta>
            <ta e="T1205" id="Seg_13182" s="T1200">Давай это прямо сначала, да? </ta>
            <ta e="T1218" id="Seg_13183" s="T1206">По-камасински, а потом уже и расскажешь снова по-русски опять, как сейчас делали. </ta>
            <ta e="T1266" id="Seg_13184" s="T1255">((…)) </ta>
            <ta e="T1297" id="Seg_13185" s="T1287">А, как брат был в плену в Финляндии, да? </ta>
            <ta e="T1308" id="Seg_13186" s="T1300">Ага, там да, это мы записывали. </ta>
            <ta e="T1317" id="Seg_13187" s="T1308">Правильно, про это (запись) есть. </ta>
            <ta e="T1330" id="Seg_13188" s="T1323">((…)) </ta>
            <ta e="T1332" id="Seg_13189" s="T1331">((…)) </ta>
            <ta e="T1341" id="Seg_13190" s="T1333">Да, говорят, что это записано всё очень хорошо. </ta>
            <ta e="T1349" id="Seg_13191" s="T1342">Вчера, говорят, очень хорошо у них получилось. </ta>
            <ta e="T1367" id="Seg_13192" s="T1350">Второй был, который сидел вот, протягивал такой длинной палкой микрофон, вот он как раз записывал для них. </ta>
            <ta e="T1375" id="Seg_13193" s="T1374">Ага. </ta>
            <ta e="T1384" id="Seg_13194" s="T1376">((…)) </ta>
            <ta e="T1406" id="Seg_13195" s="T1390">Так это в радио будут передавать, только мы забыли спросить, когда это будет передача в радио. </ta>
            <ta e="T1417" id="Seg_13196" s="T1407">Вот тот, который сидел сзади, самый сзади, протягивал микрофон подальше. </ta>
            <ta e="T1428" id="Seg_13197" s="T1418">Такой, эстонец он был, так он для эстонского радио снимал. </ta>
            <ta e="T1430" id="Seg_13198" s="T1429">Вот. </ta>
            <ta e="T1445" id="Seg_13199" s="T1431">Надо у него спросить, может (быть), сама послушаешь, как по радио будешь говорить. </ta>
            <ta e="T1452" id="Seg_13200" s="T1450">А, ((…)). </ta>
            <ta e="T1459" id="Seg_13201" s="T1456">Ага. </ta>
            <ta e="T1462" id="Seg_13202" s="T1461">Так-с. </ta>
            <ta e="T1463" id="Seg_13203" s="T1462">Ну… </ta>
            <ta e="T1482" id="Seg_13204" s="T1476">Ага, точно-точно. </ta>
            <ta e="T1506" id="Seg_13205" s="T1486">Да, я не знаю, вот было так это всё спешно, что я не успел этого человека спросить, когда будет радио. </ta>
            <ta e="T1526" id="Seg_13206" s="T1507">Я и сам интересно мне, потому что я тоже говорил, меня спрашивал насчет тебя, я рассказал про тебя там. </ta>
            <ta e="T1559" id="Seg_13207" s="T1544">Ну, а может ты хочешь что сказать и о вчерашнем дне, например, если тебе понравится. </ta>
            <ta e="T1571" id="Seg_13208" s="T1560">Как вчера в гости ходили, все такое, как спрашивали, как было. </ta>
            <ta e="T1578" id="Seg_13209" s="T1572">Что ты им рассказывала, так, коротко. </ta>
            <ta e="T1600" id="Seg_13210" s="T1578">Просто не повторять обязательно, ну просто так, о чем там говорили, как всё это было, сколько там было людей, и всё такое. </ta>
            <ta e="T1603" id="Seg_13211" s="T1601">Можно, думаешь? </ta>
            <ta e="T1611" id="Seg_13212" s="T1608">Ну, давай, давай. </ta>
            <ta e="T1626" id="Seg_13213" s="T1620">Да, да, действительно. </ta>
            <ta e="T1633" id="Seg_13214" s="T1632">Да. </ta>
            <ta e="T1645" id="Seg_13215" s="T1637">Точно так, не с кем. </ta>
            <ta e="T1708" id="Seg_13216" s="T1704">Sĭreʔpne, да? </ta>
            <ta e="T1711" id="Seg_13217" s="T1709">Ага. </ta>
            <ta e="T1716" id="Seg_13218" s="T1712">((…)) </ta>
            <ta e="T1755" id="Seg_13219" s="T1753">Ага. </ta>
            <ta e="T1760" id="Seg_13220" s="T1758">Ага. </ta>
            <ta e="T1774" id="Seg_13221" s="T1762">Ну, а ты повтори по-русски, просто, так сказать, переведи это еще раз. </ta>
            <ta e="T1815" id="Seg_13222" s="T1807">Так всё и былое и старое можно вспомнить. </ta>
            <ta e="T1819" id="Seg_13223" s="T1816">Что только хочешь. </ta>
            <ta e="T1842" id="Seg_13224" s="T1820">Тут он предлагал, что, но мы это говорили, ну это можно всё еще раз сказать: о матери, об отце, о чем угодно. </ta>
            <ta e="T1846" id="Seg_13225" s="T1843">((…)) </ta>
            <ta e="T1859" id="Seg_13226" s="T1852">((DMG)) Ага, ну ничего, мы помаленьку можем. </ta>
            <ta e="T1861" id="Seg_13227" s="T1860">((…)) </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T9" id="Seg_13228" s="T7">RUS:ext</ta>
            <ta e="T25" id="Seg_13229" s="T11">RUS:ext</ta>
            <ta e="T27" id="Seg_13230" s="T26">FIN:ext</ta>
            <ta e="T29" id="Seg_13231" s="T28">FIN:ext</ta>
            <ta e="T32" id="Seg_13232" s="T30">RUS:ext</ta>
            <ta e="T40" id="Seg_13233" s="T33">RUS:ext</ta>
            <ta e="T42" id="Seg_13234" s="T41">FIN:ext</ta>
            <ta e="T49" id="Seg_13235" s="T48">FIN:ext</ta>
            <ta e="T51" id="Seg_13236" s="T50">FIN:ext</ta>
            <ta e="T53" id="Seg_13237" s="T52">FIN:ext</ta>
            <ta e="T58" id="Seg_13238" s="T54">RUS:ext</ta>
            <ta e="T71" id="Seg_13239" s="T59">RUS:ext</ta>
            <ta e="T80" id="Seg_13240" s="T72">RUS:ext</ta>
            <ta e="T93" id="Seg_13241" s="T81">RUS:ext</ta>
            <ta e="T99" id="Seg_13242" s="T94">RUS:ext</ta>
            <ta e="T110" id="Seg_13243" s="T100">RUS:ext</ta>
            <ta e="T133" id="Seg_13244" s="T118">RUS:ext</ta>
            <ta e="T162" id="Seg_13245" s="T158">RUS:ext</ta>
            <ta e="T185" id="Seg_13246" s="T178">RUS:ext</ta>
            <ta e="T221" id="Seg_13247" s="T210">RUS:ext</ta>
            <ta e="T223" id="Seg_13248" s="T222">RUS:ext</ta>
            <ta e="T227" id="Seg_13249" s="T223">RUS:ext</ta>
            <ta e="T243" id="Seg_13250" s="T233">RUS:ext</ta>
            <ta e="T278" id="Seg_13251" s="T276">RUS:ext</ta>
            <ta e="T311" id="Seg_13252" s="T297">RUS:ext</ta>
            <ta e="T312" id="Seg_13253" s="T311">FIN:ext</ta>
            <ta e="T314" id="Seg_13254" s="T313">FIN:ext</ta>
            <ta e="T639" id="Seg_13255" s="T638">RUS:ext</ta>
            <ta e="T643" id="Seg_13256" s="T640">RUS:ext</ta>
            <ta e="T655" id="Seg_13257" s="T653">RUS:ext</ta>
            <ta e="T664" id="Seg_13258" s="T657">RUS:ext</ta>
            <ta e="T673" id="Seg_13259" s="T665">RUS:ext</ta>
            <ta e="T676" id="Seg_13260" s="T674">RUS:ext</ta>
            <ta e="T679" id="Seg_13261" s="T678">FIN:ext</ta>
            <ta e="T924" id="Seg_13262" s="T918">RUS:ext</ta>
            <ta e="T932" id="Seg_13263" s="T928">RUS:ext</ta>
            <ta e="T960" id="Seg_13264" s="T953">RUS:ext</ta>
            <ta e="T1011" id="Seg_13265" s="T1010">FIN:ext</ta>
            <ta e="T1013" id="Seg_13266" s="T1012">RUS:ext</ta>
            <ta e="T1015" id="Seg_13267" s="T1014">RUS:ext</ta>
            <ta e="T1025" id="Seg_13268" s="T1015">RUS:ext</ta>
            <ta e="T1045" id="Seg_13269" s="T1029">RUS:ext</ta>
            <ta e="T1053" id="Seg_13270" s="T1048">RUS:ext</ta>
            <ta e="T1092" id="Seg_13271" s="T1071">RUS:ext</ta>
            <ta e="T1097" id="Seg_13272" s="T1093">RUS:ext</ta>
            <ta e="T1099" id="Seg_13273" s="T1098">RUS:ext</ta>
            <ta e="T1106" id="Seg_13274" s="T1100">RUS:ext</ta>
            <ta e="T1117" id="Seg_13275" s="T1107">RUS:ext</ta>
            <ta e="T1132" id="Seg_13276" s="T1120">RUS:ext</ta>
            <ta e="T1141" id="Seg_13277" s="T1132">RUS:ext</ta>
            <ta e="T1152" id="Seg_13278" s="T1142">RUS:ext</ta>
            <ta e="T1161" id="Seg_13279" s="T1153">RUS:ext</ta>
            <ta e="T1172" id="Seg_13280" s="T1162">RUS:ext</ta>
            <ta e="T1194" id="Seg_13281" s="T1190">RUS:ext</ta>
            <ta e="T1197" id="Seg_13282" s="T1195">RUS:ext</ta>
            <ta e="T1200" id="Seg_13283" s="T1198">RUS:ext</ta>
            <ta e="T1205" id="Seg_13284" s="T1200">RUS:ext</ta>
            <ta e="T1218" id="Seg_13285" s="T1206">RUS:ext</ta>
            <ta e="T1266" id="Seg_13286" s="T1255">FIN:ext</ta>
            <ta e="T1297" id="Seg_13287" s="T1287">RUS:ext</ta>
            <ta e="T1308" id="Seg_13288" s="T1300">RUS:ext</ta>
            <ta e="T1317" id="Seg_13289" s="T1308">RUS:ext</ta>
            <ta e="T1330" id="Seg_13290" s="T1323">FIN:ext</ta>
            <ta e="T1332" id="Seg_13291" s="T1331">FIN:ext</ta>
            <ta e="T1341" id="Seg_13292" s="T1333">RUS:ext</ta>
            <ta e="T1349" id="Seg_13293" s="T1342">RUS:ext</ta>
            <ta e="T1367" id="Seg_13294" s="T1350">RUS:ext</ta>
            <ta e="T1375" id="Seg_13295" s="T1374">RUS:ext</ta>
            <ta e="T1384" id="Seg_13296" s="T1376">FIN:ext</ta>
            <ta e="T1406" id="Seg_13297" s="T1390">RUS:ext</ta>
            <ta e="T1417" id="Seg_13298" s="T1407">RUS:ext</ta>
            <ta e="T1428" id="Seg_13299" s="T1418">RUS:ext</ta>
            <ta e="T1430" id="Seg_13300" s="T1429">RUS:ext</ta>
            <ta e="T1445" id="Seg_13301" s="T1431">RUS:ext</ta>
            <ta e="T1452" id="Seg_13302" s="T1450">RUS:ext</ta>
            <ta e="T1459" id="Seg_13303" s="T1456">RUS:ext</ta>
            <ta e="T1462" id="Seg_13304" s="T1461">RUS:ext</ta>
            <ta e="T1463" id="Seg_13305" s="T1462">RUS:ext</ta>
            <ta e="T1482" id="Seg_13306" s="T1476">RUS:ext</ta>
            <ta e="T1506" id="Seg_13307" s="T1486">RUS:ext</ta>
            <ta e="T1526" id="Seg_13308" s="T1507">RUS:ext</ta>
            <ta e="T1559" id="Seg_13309" s="T1544">RUS:ext</ta>
            <ta e="T1571" id="Seg_13310" s="T1560">RUS:ext</ta>
            <ta e="T1578" id="Seg_13311" s="T1572">RUS:ext</ta>
            <ta e="T1600" id="Seg_13312" s="T1578">RUS:ext</ta>
            <ta e="T1603" id="Seg_13313" s="T1601">RUS:ext</ta>
            <ta e="T1611" id="Seg_13314" s="T1608">RUS:ext</ta>
            <ta e="T1626" id="Seg_13315" s="T1620">RUS:ext</ta>
            <ta e="T1633" id="Seg_13316" s="T1632">RUS:ext</ta>
            <ta e="T1645" id="Seg_13317" s="T1637">RUS:ext</ta>
            <ta e="T1708" id="Seg_13318" s="T1706">RUS:ext</ta>
            <ta e="T1711" id="Seg_13319" s="T1709">RUS:ext</ta>
            <ta e="T1716" id="Seg_13320" s="T1712">FIN:ext</ta>
            <ta e="T1755" id="Seg_13321" s="T1753">RUS:ext</ta>
            <ta e="T1760" id="Seg_13322" s="T1758">RUS:ext</ta>
            <ta e="T1774" id="Seg_13323" s="T1762">RUS:ext</ta>
            <ta e="T1815" id="Seg_13324" s="T1807">RUS:ext</ta>
            <ta e="T1819" id="Seg_13325" s="T1816">RUS:ext</ta>
            <ta e="T1842" id="Seg_13326" s="T1820">RUS:ext</ta>
            <ta e="T1846" id="Seg_13327" s="T1843">FIN:ext</ta>
            <ta e="T1859" id="Seg_13328" s="T1852">RUS:ext</ta>
            <ta e="T1861" id="Seg_13329" s="T1860">FIN:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T633" id="Seg_13330" s="T630">Ты очень хорошо говоришь.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T633" id="Seg_13331" s="T630">You are speaking very vell.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T633" id="Seg_13332" s="T630">Du sprichst sehr gut.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T1893" />
            <conversion-tli id="T1894" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T1895" />
            <conversion-tli id="T1896" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T1897" />
            <conversion-tli id="T1898" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T1899" />
            <conversion-tli id="T1900" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T1901" />
            <conversion-tli id="T1902" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T1903" />
            <conversion-tli id="T1904" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T1905" />
            <conversion-tli id="T1906" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T1907" />
            <conversion-tli id="T1908" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T1918" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T1917" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T1909" />
            <conversion-tli id="T1910" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1911" />
            <conversion-tli id="T1912" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1913" />
            <conversion-tli id="T1914" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1534" />
            <conversion-tli id="T1535" />
            <conversion-tli id="T1536" />
            <conversion-tli id="T1537" />
            <conversion-tli id="T1538" />
            <conversion-tli id="T1539" />
            <conversion-tli id="T1540" />
            <conversion-tli id="T1541" />
            <conversion-tli id="T1542" />
            <conversion-tli id="T1543" />
            <conversion-tli id="T1544" />
            <conversion-tli id="T1545" />
            <conversion-tli id="T1546" />
            <conversion-tli id="T1547" />
            <conversion-tli id="T1548" />
            <conversion-tli id="T1549" />
            <conversion-tli id="T1550" />
            <conversion-tli id="T1551" />
            <conversion-tli id="T1552" />
            <conversion-tli id="T1553" />
            <conversion-tli id="T1554" />
            <conversion-tli id="T1555" />
            <conversion-tli id="T1556" />
            <conversion-tli id="T1557" />
            <conversion-tli id="T1558" />
            <conversion-tli id="T1559" />
            <conversion-tli id="T1560" />
            <conversion-tli id="T1561" />
            <conversion-tli id="T1562" />
            <conversion-tli id="T1563" />
            <conversion-tli id="T1564" />
            <conversion-tli id="T1565" />
            <conversion-tli id="T1566" />
            <conversion-tli id="T1567" />
            <conversion-tli id="T1568" />
            <conversion-tli id="T1569" />
            <conversion-tli id="T1570" />
            <conversion-tli id="T1571" />
            <conversion-tli id="T1572" />
            <conversion-tli id="T1573" />
            <conversion-tli id="T1574" />
            <conversion-tli id="T1575" />
            <conversion-tli id="T1576" />
            <conversion-tli id="T1577" />
            <conversion-tli id="T1578" />
            <conversion-tli id="T1579" />
            <conversion-tli id="T1580" />
            <conversion-tli id="T1581" />
            <conversion-tli id="T1582" />
            <conversion-tli id="T1583" />
            <conversion-tli id="T1584" />
            <conversion-tli id="T1585" />
            <conversion-tli id="T1586" />
            <conversion-tli id="T1587" />
            <conversion-tli id="T1588" />
            <conversion-tli id="T1589" />
            <conversion-tli id="T1590" />
            <conversion-tli id="T1591" />
            <conversion-tli id="T1592" />
            <conversion-tli id="T1593" />
            <conversion-tli id="T1594" />
            <conversion-tli id="T1595" />
            <conversion-tli id="T1596" />
            <conversion-tli id="T1597" />
            <conversion-tli id="T1598" />
            <conversion-tli id="T1599" />
            <conversion-tli id="T1600" />
            <conversion-tli id="T1601" />
            <conversion-tli id="T1602" />
            <conversion-tli id="T1603" />
            <conversion-tli id="T1604" />
            <conversion-tli id="T1605" />
            <conversion-tli id="T1606" />
            <conversion-tli id="T1607" />
            <conversion-tli id="T1608" />
            <conversion-tli id="T1609" />
            <conversion-tli id="T1610" />
            <conversion-tli id="T1611" />
            <conversion-tli id="T1612" />
            <conversion-tli id="T1613" />
            <conversion-tli id="T1614" />
            <conversion-tli id="T1615" />
            <conversion-tli id="T1616" />
            <conversion-tli id="T1617" />
            <conversion-tli id="T1618" />
            <conversion-tli id="T1619" />
            <conversion-tli id="T1620" />
            <conversion-tli id="T1621" />
            <conversion-tli id="T1622" />
            <conversion-tli id="T1623" />
            <conversion-tli id="T1624" />
            <conversion-tli id="T1625" />
            <conversion-tli id="T1626" />
            <conversion-tli id="T1627" />
            <conversion-tli id="T1628" />
            <conversion-tli id="T1629" />
            <conversion-tli id="T1630" />
            <conversion-tli id="T1631" />
            <conversion-tli id="T1632" />
            <conversion-tli id="T1633" />
            <conversion-tli id="T1634" />
            <conversion-tli id="T1635" />
            <conversion-tli id="T1636" />
            <conversion-tli id="T1637" />
            <conversion-tli id="T1638" />
            <conversion-tli id="T1639" />
            <conversion-tli id="T1640" />
            <conversion-tli id="T1641" />
            <conversion-tli id="T1642" />
            <conversion-tli id="T1643" />
            <conversion-tli id="T1644" />
            <conversion-tli id="T1645" />
            <conversion-tli id="T1646" />
            <conversion-tli id="T1647" />
            <conversion-tli id="T1648" />
            <conversion-tli id="T1649" />
            <conversion-tli id="T1650" />
            <conversion-tli id="T1651" />
            <conversion-tli id="T1652" />
            <conversion-tli id="T1653" />
            <conversion-tli id="T1654" />
            <conversion-tli id="T1655" />
            <conversion-tli id="T1656" />
            <conversion-tli id="T1657" />
            <conversion-tli id="T1658" />
            <conversion-tli id="T1659" />
            <conversion-tli id="T1660" />
            <conversion-tli id="T1661" />
            <conversion-tli id="T1662" />
            <conversion-tli id="T1663" />
            <conversion-tli id="T1664" />
            <conversion-tli id="T1665" />
            <conversion-tli id="T1666" />
            <conversion-tli id="T1667" />
            <conversion-tli id="T1668" />
            <conversion-tli id="T1669" />
            <conversion-tli id="T1670" />
            <conversion-tli id="T1671" />
            <conversion-tli id="T1672" />
            <conversion-tli id="T1673" />
            <conversion-tli id="T1674" />
            <conversion-tli id="T1675" />
            <conversion-tli id="T1676" />
            <conversion-tli id="T1677" />
            <conversion-tli id="T1678" />
            <conversion-tli id="T1679" />
            <conversion-tli id="T1680" />
            <conversion-tli id="T1681" />
            <conversion-tli id="T1682" />
            <conversion-tli id="T1683" />
            <conversion-tli id="T1684" />
            <conversion-tli id="T1685" />
            <conversion-tli id="T1686" />
            <conversion-tli id="T1687" />
            <conversion-tli id="T1688" />
            <conversion-tli id="T1689" />
            <conversion-tli id="T1690" />
            <conversion-tli id="T1691" />
            <conversion-tli id="T1692" />
            <conversion-tli id="T1693" />
            <conversion-tli id="T1694" />
            <conversion-tli id="T1695" />
            <conversion-tli id="T1696" />
            <conversion-tli id="T1697" />
            <conversion-tli id="T1698" />
            <conversion-tli id="T1699" />
            <conversion-tli id="T1700" />
            <conversion-tli id="T1701" />
            <conversion-tli id="T1702" />
            <conversion-tli id="T1703" />
            <conversion-tli id="T1704" />
            <conversion-tli id="T1705" />
            <conversion-tli id="T1706" />
            <conversion-tli id="T1707" />
            <conversion-tli id="T1708" />
            <conversion-tli id="T1709" />
            <conversion-tli id="T1710" />
            <conversion-tli id="T1711" />
            <conversion-tli id="T1712" />
            <conversion-tli id="T1713" />
            <conversion-tli id="T1714" />
            <conversion-tli id="T1715" />
            <conversion-tli id="T1716" />
            <conversion-tli id="T1717" />
            <conversion-tli id="T1718" />
            <conversion-tli id="T1719" />
            <conversion-tli id="T1720" />
            <conversion-tli id="T1721" />
            <conversion-tli id="T1722" />
            <conversion-tli id="T1723" />
            <conversion-tli id="T1724" />
            <conversion-tli id="T1725" />
            <conversion-tli id="T1726" />
            <conversion-tli id="T1727" />
            <conversion-tli id="T1728" />
            <conversion-tli id="T1729" />
            <conversion-tli id="T1730" />
            <conversion-tli id="T1731" />
            <conversion-tli id="T1732" />
            <conversion-tli id="T1733" />
            <conversion-tli id="T1734" />
            <conversion-tli id="T1735" />
            <conversion-tli id="T1736" />
            <conversion-tli id="T1737" />
            <conversion-tli id="T1738" />
            <conversion-tli id="T1739" />
            <conversion-tli id="T1740" />
            <conversion-tli id="T1741" />
            <conversion-tli id="T1742" />
            <conversion-tli id="T1743" />
            <conversion-tli id="T1744" />
            <conversion-tli id="T1745" />
            <conversion-tli id="T1746" />
            <conversion-tli id="T1747" />
            <conversion-tli id="T1748" />
            <conversion-tli id="T1749" />
            <conversion-tli id="T1750" />
            <conversion-tli id="T1751" />
            <conversion-tli id="T1752" />
            <conversion-tli id="T1753" />
            <conversion-tli id="T1754" />
            <conversion-tli id="T1755" />
            <conversion-tli id="T1756" />
            <conversion-tli id="T1757" />
            <conversion-tli id="T1758" />
            <conversion-tli id="T1759" />
            <conversion-tli id="T1760" />
            <conversion-tli id="T1761" />
            <conversion-tli id="T1762" />
            <conversion-tli id="T1763" />
            <conversion-tli id="T1764" />
            <conversion-tli id="T1765" />
            <conversion-tli id="T1766" />
            <conversion-tli id="T1767" />
            <conversion-tli id="T1768" />
            <conversion-tli id="T1769" />
            <conversion-tli id="T1770" />
            <conversion-tli id="T1771" />
            <conversion-tli id="T1772" />
            <conversion-tli id="T1773" />
            <conversion-tli id="T1774" />
            <conversion-tli id="T1775" />
            <conversion-tli id="T1776" />
            <conversion-tli id="T1777" />
            <conversion-tli id="T1778" />
            <conversion-tli id="T1779" />
            <conversion-tli id="T1780" />
            <conversion-tli id="T1781" />
            <conversion-tli id="T1782" />
            <conversion-tli id="T1783" />
            <conversion-tli id="T1784" />
            <conversion-tli id="T1785" />
            <conversion-tli id="T1786" />
            <conversion-tli id="T1787" />
            <conversion-tli id="T1788" />
            <conversion-tli id="T1789" />
            <conversion-tli id="T1790" />
            <conversion-tli id="T1791" />
            <conversion-tli id="T1792" />
            <conversion-tli id="T1793" />
            <conversion-tli id="T1794" />
            <conversion-tli id="T1795" />
            <conversion-tli id="T1796" />
            <conversion-tli id="T1797" />
            <conversion-tli id="T1798" />
            <conversion-tli id="T1799" />
            <conversion-tli id="T1800" />
            <conversion-tli id="T1801" />
            <conversion-tli id="T1802" />
            <conversion-tli id="T1803" />
            <conversion-tli id="T1804" />
            <conversion-tli id="T1805" />
            <conversion-tli id="T1806" />
            <conversion-tli id="T1807" />
            <conversion-tli id="T1808" />
            <conversion-tli id="T1809" />
            <conversion-tli id="T1810" />
            <conversion-tli id="T1811" />
            <conversion-tli id="T1812" />
            <conversion-tli id="T1813" />
            <conversion-tli id="T1814" />
            <conversion-tli id="T1815" />
            <conversion-tli id="T1816" />
            <conversion-tli id="T1817" />
            <conversion-tli id="T1818" />
            <conversion-tli id="T1819" />
            <conversion-tli id="T1820" />
            <conversion-tli id="T1821" />
            <conversion-tli id="T1822" />
            <conversion-tli id="T1823" />
            <conversion-tli id="T1824" />
            <conversion-tli id="T1825" />
            <conversion-tli id="T1826" />
            <conversion-tli id="T1827" />
            <conversion-tli id="T1828" />
            <conversion-tli id="T1829" />
            <conversion-tli id="T1830" />
            <conversion-tli id="T1831" />
            <conversion-tli id="T1832" />
            <conversion-tli id="T1833" />
            <conversion-tli id="T1834" />
            <conversion-tli id="T1835" />
            <conversion-tli id="T1836" />
            <conversion-tli id="T1837" />
            <conversion-tli id="T1838" />
            <conversion-tli id="T1839" />
            <conversion-tli id="T1840" />
            <conversion-tli id="T1841" />
            <conversion-tli id="T1842" />
            <conversion-tli id="T1843" />
            <conversion-tli id="T1844" />
            <conversion-tli id="T1845" />
            <conversion-tli id="T1846" />
            <conversion-tli id="T1847" />
            <conversion-tli id="T1848" />
            <conversion-tli id="T1849" />
            <conversion-tli id="T1850" />
            <conversion-tli id="T1851" />
            <conversion-tli id="T1852" />
            <conversion-tli id="T1853" />
            <conversion-tli id="T1854" />
            <conversion-tli id="T1855" />
            <conversion-tli id="T1856" />
            <conversion-tli id="T1857" />
            <conversion-tli id="T1858" />
            <conversion-tli id="T1859" />
            <conversion-tli id="T1860" />
            <conversion-tli id="T1915" />
            <conversion-tli id="T1916" />
            <conversion-tli id="T1861" />
            <conversion-tli id="T1862" />
            <conversion-tli id="T1863" />
            <conversion-tli id="T1864" />
            <conversion-tli id="T1865" />
            <conversion-tli id="T1866" />
            <conversion-tli id="T1867" />
            <conversion-tli id="T1868" />
            <conversion-tli id="T1869" />
            <conversion-tli id="T1870" />
            <conversion-tli id="T1871" />
            <conversion-tli id="T1872" />
            <conversion-tli id="T1873" />
            <conversion-tli id="T1874" />
            <conversion-tli id="T1875" />
            <conversion-tli id="T1876" />
            <conversion-tli id="T1877" />
            <conversion-tli id="T1878" />
            <conversion-tli id="T1879" />
            <conversion-tli id="T1880" />
            <conversion-tli id="T1881" />
            <conversion-tli id="T1882" />
            <conversion-tli id="T1883" />
            <conversion-tli id="T1884" />
            <conversion-tli id="T1885" />
            <conversion-tli id="T1886" />
            <conversion-tli id="T1887" />
            <conversion-tli id="T1888" />
            <conversion-tli id="T1889" />
            <conversion-tli id="T1890" />
            <conversion-tli id="T1891" />
            <conversion-tli id="T1892" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
