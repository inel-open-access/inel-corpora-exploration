<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID2BEA192E-BB4C-F79B-5935-F940CE1D4D4A">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_SU0229</transcription-name>
         <referenced-file url="PKZ_196X_SU0229.wav" />
         <referenced-file url="PKZ_196X_SU0229.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0229\PKZ_196X_SU0229.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1743</ud-information>
            <ud-information attribute-name="# HIAT:w">924</ud-information>
            <ud-information attribute-name="# e">974</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">56</ud-information>
            <ud-information attribute-name="# HIAT:u">232</ud-information>
            <ud-information attribute-name="# sc">420</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.9075" type="appl" />
         <tli id="T3" time="1.815" type="appl" />
         <tli id="T4" time="2.7225" type="appl" />
         <tli id="T5" time="3.63" type="appl" />
         <tli id="T6" time="4.1" type="appl" />
         <tli id="T7" time="4.755" type="appl" />
         <tli id="T8" time="5.41" type="appl" />
         <tli id="T9" time="6.0649999999999995" type="appl" />
         <tli id="T10" time="6.72" type="appl" />
         <tli id="T11" time="7.375" type="appl" />
         <tli id="T12" time="8.03" type="appl" />
         <tli id="T13" time="8.684999999999999" type="appl" />
         <tli id="T14" time="9.34" type="appl" />
         <tli id="T15" time="9.5" type="appl" />
         <tli id="T16" time="9.937285714285714" type="appl" />
         <tli id="T17" time="10.374571428571429" type="appl" />
         <tli id="T18" time="10.811857142857143" type="appl" />
         <tli id="T19" time="11.249142857142857" type="appl" />
         <tli id="T20" time="11.686428571428571" type="appl" />
         <tli id="T21" time="12.123714285714286" type="appl" />
         <tli id="T22" time="12.561" type="appl" />
         <tli id="T23" time="14.259926854924213" />
         <tli id="T24" time="15.5725" type="appl" />
         <tli id="T25" time="16.725" type="appl" />
         <tli id="T26" time="17.8775" type="appl" />
         <tli id="T27" time="19.03" type="appl" />
         <tli id="T28" time="20.11" type="appl" />
         <tli id="T29" time="21.38" type="appl" />
         <tli id="T30" time="22.65" type="appl" />
         <tli id="T31" time="23.19" type="appl" />
         <tli id="T32" time="25.57" type="appl" />
         <tli id="T33" time="25.85" type="appl" />
         <tli id="T34" time="27.813190667949424" />
         <tli id="T35" time="27.88" type="appl" />
         <tli id="T36" time="28.697499999999998" type="appl" />
         <tli id="T37" time="29.508334967643318" />
         <tli id="T38" time="30.3325" />
         <tli id="T39" time="31.09666666418165" />
         <tli id="T40" time="31.31" />
         <tli id="T41" time="31.82" type="appl" />
         <tli id="T42" time="32.33" type="appl" />
         <tli id="T43" time="32.34" type="appl" />
         <tli id="T44" time="32.86" type="appl" />
         <tli id="T45" time="33.38" type="appl" />
         <tli id="T46" time="33.900000000000006" type="appl" />
         <tli id="T47" time="34.52648956598995" />
         <tli id="T48" time="34.74" type="appl" />
         <tli id="T49" time="35.5" type="appl" />
         <tli id="T50" time="36.260000000000005" type="appl" />
         <tli id="T1178" time="36.56400000000001" type="intp" />
         <tli id="T51" time="37.02" type="appl" />
         <tli id="T52" time="37.78" type="appl" />
         <tli id="T53" time="38.69" type="appl" />
         <tli id="T54" time="39.13" type="appl" />
         <tli id="T55" time="39.42" type="appl" />
         <tli id="T56" time="40.09166666666667" type="appl" />
         <tli id="T57" time="40.763333333333335" type="appl" />
         <tli id="T58" time="41.435" type="appl" />
         <tli id="T59" time="42.10666666666667" type="appl" />
         <tli id="T60" time="42.778333333333336" type="appl" />
         <tli id="T61" time="43.45" type="appl" />
         <tli id="T62" time="43.47" type="appl" />
         <tli id="T63" time="43.93333333333333" type="appl" />
         <tli id="T64" time="44.39666666666667" type="appl" />
         <tli id="T65" time="44.86" type="appl" />
         <tli id="T66" time="45.32333333333333" type="appl" />
         <tli id="T67" time="45.78666666666667" type="appl" />
         <tli id="T68" time="46.25" type="appl" />
         <tli id="T69" time="46.71333333333333" type="appl" />
         <tli id="T70" time="47.17666666666667" type="appl" />
         <tli id="T71" time="47.64" type="appl" />
         <tli id="T72" time="48.081428571428575" type="appl" />
         <tli id="T73" time="48.52285714285714" type="appl" />
         <tli id="T74" time="48.964285714285715" type="appl" />
         <tli id="T75" time="49.40571428571428" type="appl" />
         <tli id="T76" time="49.847142857142856" type="appl" />
         <tli id="T77" time="50.28857142857142" type="appl" />
         <tli id="T78" time="50.83973922190371" />
         <tli id="T79" time="51.22" type="appl" />
         <tli id="T80" time="51.79" type="appl" />
         <tli id="T81" time="52.36" type="appl" />
         <tli id="T82" time="52.67" type="appl" />
         <tli id="T83" time="53.156666666666666" type="appl" />
         <tli id="T84" time="53.64333333333333" type="appl" />
         <tli id="T85" time="54.13" type="appl" />
         <tli id="T86" time="54.61666666666667" type="appl" />
         <tli id="T87" time="55.10333333333333" type="appl" />
         <tli id="T88" time="55.589999999999996" type="appl" />
         <tli id="T89" time="56.07666666666667" type="appl" />
         <tli id="T90" time="56.56333333333333" type="appl" />
         <tli id="T91" time="57.11304037687505" />
         <tli id="T92" time="58.42" type="appl" />
         <tli id="T93" time="59.35333333333333" type="appl" />
         <tli id="T1185" time="59.76814814814815" type="intp" />
         <tli id="T94" time="60.28666666666667" type="appl" />
         <tli id="T95" time="61.41968495297652" />
         <tli id="T96" time="63.2" type="appl" />
         <tli id="T97" time="64.01" type="appl" />
         <tli id="T98" time="64.82" type="appl" />
         <tli id="T99" time="65.63" type="appl" />
         <tli id="T100" time="65.65" type="appl" />
         <tli id="T101" time="66.875" type="appl" />
         <tli id="T102" time="68.1" type="appl" />
         <tli id="T103" time="68.93" type="appl" />
         <tli id="T104" time="69.65475" type="appl" />
         <tli id="T105" time="70.37950000000001" type="appl" />
         <tli id="T106" time="71.10425" type="appl" />
         <tli id="T107" time="71.56629957345088" />
         <tli id="T108" time="74.01" type="appl" />
         <tli id="T109" time="74.46" type="appl" />
         <tli id="T110" time="74.88" type="appl" />
         <tli id="T111" time="75.45" type="appl" />
         <tli id="T112" time="76.02" type="appl" />
         <tli id="T113" time="76.59" type="appl" />
         <tli id="T114" time="77.16" type="appl" />
         <tli id="T115" time="77.73" type="appl" />
         <tli id="T116" time="77.89" type="appl" />
         <tli id="T117" time="78.455" type="appl" />
         <tli id="T118" time="79.02000000000001" type="appl" />
         <tli id="T119" time="79.58500000000001" type="appl" />
         <tli id="T120" time="80.15" type="appl" />
         <tli id="T121" time="80.64" type="appl" />
         <tli id="T122" time="81.37666666666667" type="appl" />
         <tli id="T123" time="82.11333333333333" type="appl" />
         <tli id="T124" time="82.85" type="appl" />
         <tli id="T125" time="82.93" type="appl" />
         <tli id="T126" time="83.32833333333333" type="appl" />
         <tli id="T127" time="83.72666666666667" type="appl" />
         <tli id="T128" time="84.125" type="appl" />
         <tli id="T129" time="84.52333333333333" type="appl" />
         <tli id="T130" time="84.92166666666667" type="appl" />
         <tli id="T131" time="85.52622796719155" />
         <tli id="T132" time="85.55289449707453" />
         <tli id="T133" time="86.0" type="appl" />
         <tli id="T134" time="86.49000000000001" type="appl" />
         <tli id="T135" time="86.98" type="appl" />
         <tli id="T136" time="87.47" type="appl" />
         <tli id="T137" time="87.48" type="appl" />
         <tli id="T138" time="88.105" type="appl" />
         <tli id="T139" time="88.73" type="appl" />
         <tli id="T140" time="90.6" type="appl" />
         <tli id="T141" time="90.99" type="appl" />
         <tli id="T142" time="91.31" type="appl" />
         <tli id="T143" time="91.83142857142857" type="appl" />
         <tli id="T144" time="92.35285714285715" type="appl" />
         <tli id="T145" time="92.87428571428572" type="appl" />
         <tli id="T146" time="93.39571428571428" type="appl" />
         <tli id="T147" time="93.91714285714285" type="appl" />
         <tli id="T148" time="94.43857142857142" type="appl" />
         <tli id="T1179" time="94.68392422887379" type="intp" />
         <tli id="T149" time="95.09284556271106" />
         <tli id="T150" time="95.37" type="appl" />
         <tli id="T151" time="95.71" type="appl" />
         <tli id="T152" time="96.69" type="appl" />
         <tli id="T153" time="97.56" type="appl" />
         <tli id="T1183" time="97.96153846153845" type="intp" />
         <tli id="T154" time="98.42999999999999" type="appl" />
         <tli id="T155" time="99.3" type="appl" />
         <tli id="T156" time="99.34" type="appl" />
         <tli id="T157" time="99.99811111111111" type="appl" />
         <tli id="T158" time="100.65622222222223" type="appl" />
         <tli id="T159" time="101.31433333333334" type="appl" />
         <tli id="T160" time="101.97244444444445" type="appl" />
         <tli id="T161" time="102.63055555555556" type="appl" />
         <tli id="T162" time="103.28866666666667" type="appl" />
         <tli id="T163" time="103.94677777777778" type="appl" />
         <tli id="T164" time="104.6048888888889" type="appl" />
         <tli id="T165" time="105.263" type="appl" />
         <tli id="T166" time="105.64" type="appl" />
         <tli id="T167" time="106.185" type="appl" />
         <tli id="T168" time="106.72999999999999" type="appl" />
         <tli id="T169" time="107.27499999999999" type="appl" />
         <tli id="T1180" time="107.50857142857141" type="intp" />
         <tli id="T170" time="107.82" type="appl" />
         <tli id="T171" time="108.28" type="appl" />
         <tli id="T172" time="108.89714285714285" type="appl" />
         <tli id="T173" time="109.51428571428572" type="appl" />
         <tli id="T174" time="110.13142857142857" type="appl" />
         <tli id="T175" time="110.74857142857142" type="appl" />
         <tli id="T176" time="111.36571428571428" type="appl" />
         <tli id="T177" time="111.98285714285714" type="appl" />
         <tli id="T178" time="112.6" type="appl" />
         <tli id="T179" time="112.62" type="appl" />
         <tli id="T180" time="113.3875" type="appl" />
         <tli id="T181" time="114.155" type="appl" />
         <tli id="T182" time="114.9225" type="appl" />
         <tli id="T183" time="115.69" type="appl" />
         <tli id="T184" time="115.77" type="appl" />
         <tli id="T185" time="116.434" type="appl" />
         <tli id="T186" time="117.098" type="appl" />
         <tli id="T187" time="117.762" type="appl" />
         <tli id="T188" time="118.426" type="appl" />
         <tli id="T189" time="119.09" type="appl" />
         <tli id="T190" time="119.74000000000001" type="appl" />
         <tli id="T1182" time="119.98375" type="intp" />
         <tli id="T191" time="120.39" type="appl" />
         <tli id="T192" time="121.04" type="appl" />
         <tli id="T193" time="121.69" type="appl" />
         <tli id="T194" time="122.34" type="appl" />
         <tli id="T195" time="122.99" type="appl" />
         <tli id="T196" time="123.64" type="appl" />
         <tli id="T197" time="123.73" type="appl" />
         <tli id="T198" time="124.22" type="appl" />
         <tli id="T199" time="124.71000000000001" type="appl" />
         <tli id="T200" time="125.2" type="appl" />
         <tli id="T201" time="125.69000000000001" type="appl" />
         <tli id="T202" time="126.18" type="appl" />
         <tli id="T203" time="126.29" type="appl" />
         <tli id="T204" time="126.73" type="appl" />
         <tli id="T205" time="127.17" type="appl" />
         <tli id="T206" time="127.61000000000001" type="appl" />
         <tli id="T207" time="128.05" type="appl" />
         <tli id="T208" time="128.49" type="appl" />
         <tli id="T209" time="129.102" type="appl" />
         <tli id="T210" time="129.714" type="appl" />
         <tli id="T211" time="130.32600000000002" type="appl" />
         <tli id="T212" time="130.93800000000002" type="appl" />
         <tli id="T213" time="131.55" type="appl" />
         <tli id="T214" time="133.06" type="appl" />
         <tli id="T215" time="133.6625" type="appl" />
         <tli id="T216" time="134.265" type="appl" />
         <tli id="T217" time="134.8675" type="appl" />
         <tli id="T218" time="135.47" type="appl" />
         <tli id="T219" time="135.72" type="appl" />
         <tli id="T220" time="136.73125" type="appl" />
         <tli id="T221" time="137.7425" type="appl" />
         <tli id="T222" time="138.75375" type="appl" />
         <tli id="T223" time="139.765" type="appl" />
         <tli id="T224" time="140.77625" type="appl" />
         <tli id="T225" time="141.7875" type="appl" />
         <tli id="T226" time="142.79875" type="appl" />
         <tli id="T227" time="143.81" type="appl" />
         <tli id="T228" time="145.88" type="appl" />
         <tli id="T229" time="147.16" type="appl" />
         <tli id="T230" time="149.25" type="appl" />
         <tli id="T231" time="149.64" type="appl" />
         <tli id="T232" time="150.03" type="appl" />
         <tli id="T233" time="150.42" type="appl" />
         <tli id="T234" time="150.81" type="appl" />
         <tli id="T235" time="151.2" type="appl" />
         <tli id="T236" time="151.59" type="appl" />
         <tli id="T237" time="152.01333484392714" />
         <tli id="T238" time="152.025886862876" />
         <tli id="T239" time="152.67" type="appl" />
         <tli id="T240" time="153.0" type="appl" />
         <tli id="T241" time="153.66625" type="appl" />
         <tli id="T242" time="154.3325" type="appl" />
         <tli id="T243" time="154.99875" type="appl" />
         <tli id="T244" time="155.66500000000002" type="appl" />
         <tli id="T245" time="156.33125" type="appl" />
         <tli id="T246" time="156.9975" type="appl" />
         <tli id="T247" time="157.66375000000002" type="appl" />
         <tli id="T248" time="158.33" type="appl" />
         <tli id="T249" time="158.75" type="appl" />
         <tli id="T250" time="159.44" type="appl" />
         <tli id="T251" time="160.13" type="appl" />
         <tli id="T252" time="160.82" type="appl" />
         <tli id="T253" time="161.51" type="appl" />
         <tli id="T254" time="162.20000000000002" type="appl" />
         <tli id="T255" time="162.89000000000001" type="appl" />
         <tli id="T256" time="163.58" type="appl" />
         <tli id="T257" time="164.27" type="appl" />
         <tli id="T258" time="164.3" type="appl" />
         <tli id="T259" time="165.154" type="appl" />
         <tli id="T260" time="166.008" type="appl" />
         <tli id="T261" time="166.862" type="appl" />
         <tli id="T262" time="167.716" type="appl" />
         <tli id="T263" time="168.57" type="appl" />
         <tli id="T264" time="169.59" type="appl" />
         <tli id="T265" time="170.46166666666667" type="appl" />
         <tli id="T266" time="171.33333333333334" type="appl" />
         <tli id="T267" time="172.20499999999998" type="appl" />
         <tli id="T268" time="173.07666666666665" type="appl" />
         <tli id="T269" time="173.94833333333332" type="appl" />
         <tli id="T270" time="174.83333497818725" />
         <tli id="T271" time="175.65142857142857" type="appl" />
         <tli id="T272" time="176.48285714285714" type="appl" />
         <tli id="T273" time="177.31428571428572" type="appl" />
         <tli id="T274" time="178.14571428571426" type="appl" />
         <tli id="T275" time="178.97714285714284" type="appl" />
         <tli id="T276" time="179.8085714285714" type="appl" />
         <tli id="T277" time="180.78573934167125" />
         <tli id="T278" time="181.14" type="appl" />
         <tli id="T279" time="181.70166666666665" type="appl" />
         <tli id="T280" time="182.26333333333332" type="appl" />
         <tli id="T281" time="182.825" type="appl" />
         <tli id="T282" time="183.38666666666666" type="appl" />
         <tli id="T283" time="183.94833333333332" type="appl" />
         <tli id="T284" time="184.51" type="appl" />
         <tli id="T285" time="184.55" type="appl" />
         <tli id="T286" time="185.47714285714287" type="appl" />
         <tli id="T287" time="186.40428571428572" type="appl" />
         <tli id="T288" time="187.33142857142857" type="appl" />
         <tli id="T289" time="188.25857142857143" type="appl" />
         <tli id="T290" time="189.18571428571428" type="appl" />
         <tli id="T291" time="190.11285714285714" type="appl" />
         <tli id="T292" time="191.04" type="appl" />
         <tli id="T293" time="192.03" type="appl" />
         <tli id="T294" time="192.762" type="appl" />
         <tli id="T295" time="193.494" type="appl" />
         <tli id="T296" time="194.226" type="appl" />
         <tli id="T297" time="194.958" type="appl" />
         <tli id="T298" time="195.69" type="appl" />
         <tli id="T299" time="196.422" type="appl" />
         <tli id="T300" time="197.154" type="appl" />
         <tli id="T301" time="197.886" type="appl" />
         <tli id="T302" time="198.618" type="appl" />
         <tli id="T303" time="199.35" type="appl" />
         <tli id="T304" time="199.78" type="appl" />
         <tli id="T305" time="200.43" type="appl" />
         <tli id="T306" time="201.08" type="appl" />
         <tli id="T307" time="201.73" type="appl" />
         <tli id="T308" time="202.38" type="appl" />
         <tli id="T309" time="203.03" type="appl" />
         <tli id="T310" time="203.41" type="appl" />
         <tli id="T311" time="204.15333333333334" type="appl" />
         <tli id="T312" time="204.89666666666665" type="appl" />
         <tli id="T313" time="205.64" type="appl" />
         <tli id="T314" time="207.42" type="appl" />
         <tli id="T315" time="207.97" type="appl" />
         <tli id="T316" time="208.65" type="appl" />
         <tli id="T317" time="209.55833333333334" type="appl" />
         <tli id="T318" time="210.46666666666667" type="appl" />
         <tli id="T319" time="211.375" type="appl" />
         <tli id="T320" time="212.28333333333333" type="appl" />
         <tli id="T321" time="213.19166666666666" type="appl" />
         <tli id="T322" time="214.1" type="appl" />
         <tli id="T323" time="215.19" type="appl" />
         <tli id="T324" time="215.91333333333333" type="appl" />
         <tli id="T325" time="216.63666666666668" type="appl" />
         <tli id="T326" time="217.36" type="appl" />
         <tli id="T327" time="217.53" type="appl" />
         <tli id="T328" time="218.525" type="appl" />
         <tli id="T329" time="219.51999999999998" type="appl" />
         <tli id="T330" time="220.515" type="appl" />
         <tli id="T331" time="221.51" type="appl" />
         <tli id="T332" time="222.38" type="appl" />
         <tli id="T333" time="223.135" type="appl" />
         <tli id="T334" time="223.89" type="appl" />
         <tli id="T335" time="223.92" type="appl" />
         <tli id="T336" time="224.62" type="appl" />
         <tli id="T337" time="225.32" type="appl" />
         <tli id="T338" time="225.913" type="appl" />
         <tli id="T339" time="226.506" type="appl" />
         <tli id="T340" time="227.099" type="appl" />
         <tli id="T341" time="227.692" type="appl" />
         <tli id="T342" time="227.83" type="appl" />
         <tli id="T343" time="228.645" type="appl" />
         <tli id="T344" time="229.46" type="appl" />
         <tli id="T345" time="230.552" type="appl" />
         <tli id="T346" time="231.644" type="appl" />
         <tli id="T347" time="232.736" type="appl" />
         <tli id="T348" time="233.828" type="appl" />
         <tli id="T349" time="234.92" type="appl" />
         <tli id="T350" time="235.31" type="appl" />
         <tli id="T351" time="237.25" type="appl" />
         <tli id="T352" time="239.19" type="appl" />
         <tli id="T353" time="239.87" type="appl" />
         <tli id="T354" time="241.3826" type="appl" />
         <tli id="T355" time="242.8952" type="appl" />
         <tli id="T356" time="244.4078" type="appl" />
         <tli id="T357" time="245.9204" type="appl" />
         <tli id="T358" time="247.433" type="appl" />
         <tli id="T359" time="248.55" type="appl" />
         <tli id="T360" time="249.29633333333334" type="appl" />
         <tli id="T361" time="250.04266666666666" type="appl" />
         <tli id="T362" time="250.789" type="appl" />
         <tli id="T363" time="250.92" type="appl" />
         <tli id="T364" time="251.68555555555554" type="appl" />
         <tli id="T365" time="252.4511111111111" type="appl" />
         <tli id="T366" time="253.21666666666667" type="appl" />
         <tli id="T367" time="253.98222222222222" type="appl" />
         <tli id="T368" time="254.74777777777777" type="appl" />
         <tli id="T369" time="255.51333333333332" type="appl" />
         <tli id="T370" time="256.2788888888889" type="appl" />
         <tli id="T371" time="257.0444444444444" type="appl" />
         <tli id="T372" time="258.0053432503141" />
         <tli id="T373" time="259.82" type="appl" />
         <tli id="T374" time="260.16" type="appl" />
         <tli id="T375" time="260.58" type="appl" />
         <tli id="T376" time="261.15142857142854" type="appl" />
         <tli id="T377" time="261.72285714285715" type="appl" />
         <tli id="T378" time="262.2942857142857" type="appl" />
         <tli id="T379" time="262.86571428571426" type="appl" />
         <tli id="T380" time="263.4371428571428" type="appl" />
         <tli id="T381" time="264.00857142857143" type="appl" />
         <tli id="T382" time="264.58" type="appl" />
         <tli id="T383" time="265.2" type="appl" />
         <tli id="T384" time="265.95375" type="appl" />
         <tli id="T385" time="266.7075" type="appl" />
         <tli id="T386" time="267.46125" type="appl" />
         <tli id="T387" time="268.21500000000003" type="appl" />
         <tli id="T388" time="268.96875" type="appl" />
         <tli id="T389" time="269.7225" type="appl" />
         <tli id="T390" time="270.47625" type="appl" />
         <tli id="T391" time="271.23" type="appl" />
         <tli id="T392" time="271.28" type="appl" />
         <tli id="T393" time="271.90139999999997" type="appl" />
         <tli id="T394" time="272.52279999999996" type="appl" />
         <tli id="T395" time="273.1442" type="appl" />
         <tli id="T396" time="273.7656" type="appl" />
         <tli id="T397" time="274.387" type="appl" />
         <tli id="T398" time="275.42525" type="appl" />
         <tli id="T399" time="276.4635" type="appl" />
         <tli id="T400" time="277.50175" type="appl" />
         <tli id="T401" time="278.54" type="appl" />
         <tli id="T402" time="280.15" type="appl" />
         <tli id="T403" time="280.42" type="appl" />
         <tli id="T404" time="282.58" type="appl" />
         <tli id="T405" time="283.01" type="appl" />
         <tli id="T406" time="283.34" type="appl" />
         <tli id="T407" time="283.89454545454544" type="appl" />
         <tli id="T408" time="284.4490909090909" type="appl" />
         <tli id="T409" time="285.00363636363636" type="appl" />
         <tli id="T410" time="285.5581818181818" type="appl" />
         <tli id="T411" time="286.1127272727273" type="appl" />
         <tli id="T412" time="286.6672727272727" type="appl" />
         <tli id="T413" time="287.22181818181815" type="appl" />
         <tli id="T414" time="287.7763636363636" type="appl" />
         <tli id="T415" time="288.3309090909091" type="appl" />
         <tli id="T416" time="288.88545454545454" type="appl" />
         <tli id="T417" time="289.44" type="appl" />
         <tli id="T418" time="290.09" type="appl" />
         <tli id="T419" time="290.4" type="appl" />
         <tli id="T420" time="290.58" type="appl" />
         <tli id="T421" time="291.725" type="appl" />
         <tli id="T422" time="292.87" type="appl" />
         <tli id="T423" time="293.11" type="appl" />
         <tli id="T424" time="293.4" type="appl" />
         <tli id="T425" time="295.38" type="appl" />
         <tli id="T426" time="296.11" type="appl" />
         <tli id="T427" time="297.14" type="appl" />
         <tli id="T428" time="298.43" type="appl" />
         <tli id="T429" time="299.71999999999997" type="appl" />
         <tli id="T430" time="301.01" type="appl" />
         <tli id="T431" time="302.3" type="appl" />
         <tli id="T432" time="303.59000000000003" type="appl" />
         <tli id="T433" time="304.88" type="appl" />
         <tli id="T434" time="306.17" type="appl" />
         <tli id="T435" time="306.28" type="appl" />
         <tli id="T436" time="307.089" type="appl" />
         <tli id="T437" time="307.89799999999997" type="appl" />
         <tli id="T438" time="308.707" type="appl" />
         <tli id="T439" time="309.51599999999996" type="appl" />
         <tli id="T440" time="310.465074162609" />
         <tli id="T441" time="311.32" type="appl" />
         <tli id="T442" time="311.6" type="appl" />
         <tli id="T443" time="312.55" type="appl" />
         <tli id="T444" time="313.395" type="appl" />
         <tli id="T445" time="314.24" type="appl" />
         <tli id="T446" time="315.08500000000004" type="appl" />
         <tli id="T447" time="315.93" type="appl" />
         <tli id="T448" time="316.19" type="appl" />
         <tli id="T449" time="317.015" type="appl" />
         <tli id="T450" time="317.84000000000003" type="appl" />
         <tli id="T451" time="318.665" type="appl" />
         <tli id="T452" time="319.49" type="appl" />
         <tli id="T453" time="321.13" type="appl" />
         <tli id="T454" time="321.5" type="appl" />
         <tli id="T455" time="321.69" type="appl" />
         <tli id="T456" time="322.58666666666664" type="appl" />
         <tli id="T457" time="323.48333333333335" type="appl" />
         <tli id="T458" time="324.38" type="appl" />
         <tli id="T459" time="324.83" type="appl" />
         <tli id="T460" time="325.4848" type="appl" />
         <tli id="T461" time="326.1396" type="appl" />
         <tli id="T462" time="326.7944" type="appl" />
         <tli id="T463" time="327.44919999999996" type="appl" />
         <tli id="T464" time="328.43164867126757" />
         <tli id="T465" time="328.75" type="appl" />
         <tli id="T466" time="329.23" type="appl" />
         <tli id="T467" time="329.43" type="appl" />
         <tli id="T468" time="330.4842857142857" type="appl" />
         <tli id="T469" time="331.53857142857146" type="appl" />
         <tli id="T470" time="332.59285714285716" type="appl" />
         <tli id="T471" time="333.64714285714285" type="appl" />
         <tli id="T472" time="334.70142857142855" type="appl" />
         <tli id="T473" time="335.7557142857143" type="appl" />
         <tli id="T474" time="336.81" type="appl" />
         <tli id="T475" time="337.59" type="appl" />
         <tli id="T476" time="337.85" type="appl" />
         <tli id="T477" time="339.69" type="appl" />
         <tli id="T478" time="340.60333333333335" type="appl" />
         <tli id="T479" time="341.51666666666665" type="appl" />
         <tli id="T480" time="342.43" type="appl" />
         <tli id="T481" time="343.36" type="appl" />
         <tli id="T482" time="343.9316666666667" type="appl" />
         <tli id="T483" time="344.50333333333333" type="appl" />
         <tli id="T484" time="345.07500000000005" type="appl" />
         <tli id="T485" time="345.6466666666667" type="appl" />
         <tli id="T486" time="346.21833333333336" type="appl" />
         <tli id="T487" time="346.79" type="appl" />
         <tli id="T488" time="352.7" type="appl" />
         <tli id="T489" time="353.4" type="appl" />
         <tli id="T490" time="353.83" type="appl" />
         <tli id="T491" time="354.58" type="appl" />
         <tli id="T492" time="355.33" type="appl" />
         <tli id="T493" time="356.08" type="appl" />
         <tli id="T494" time="356.83" type="appl" />
         <tli id="T495" time="357.31" type="appl" />
         <tli id="T496" time="357.78" type="appl" />
         <tli id="T497" time="358.15666666666664" type="appl" />
         <tli id="T498" time="358.53333333333336" type="appl" />
         <tli id="T499" time="358.91" type="appl" />
         <tli id="T500" time="358.99" type="appl" />
         <tli id="T501" time="359.74475" type="appl" />
         <tli id="T502" time="360.4995" type="appl" />
         <tli id="T503" time="361.25425" type="appl" />
         <tli id="T504" time="362.009" type="appl" />
         <tli id="T505" time="362.44" type="appl" />
         <tli id="T506" time="362.93" type="appl" />
         <tli id="T507" time="363.5" type="appl" />
         <tli id="T508" time="364.3485714285714" type="appl" />
         <tli id="T509" time="365.19714285714286" type="appl" />
         <tli id="T510" time="366.04571428571427" type="appl" />
         <tli id="T511" time="366.89428571428573" type="appl" />
         <tli id="T512" time="367.74285714285713" type="appl" />
         <tli id="T513" time="368.5914285714286" type="appl" />
         <tli id="T514" time="369.44" type="appl" />
         <tli id="T515" time="369.86" type="appl" />
         <tli id="T516" time="370.678" type="appl" />
         <tli id="T517" time="371.496" type="appl" />
         <tli id="T518" time="372.314" type="appl" />
         <tli id="T519" time="373.132" type="appl" />
         <tli id="T520" time="373.95" type="appl" />
         <tli id="T521" time="374.97" type="appl" />
         <tli id="T522" time="375.644" type="appl" />
         <tli id="T523" time="376.318" type="appl" />
         <tli id="T524" time="376.992" type="appl" />
         <tli id="T525" time="377.666" type="appl" />
         <tli id="T526" time="378.34" type="appl" />
         <tli id="T527" time="379.17" type="appl" />
         <tli id="T528" time="379.66" type="appl" />
         <tli id="T529" time="379.89" type="appl" />
         <tli id="T530" time="380.86249999999995" type="appl" />
         <tli id="T531" time="381.835" type="appl" />
         <tli id="T532" time="382.8075" type="appl" />
         <tli id="T533" time="383.78" type="appl" />
         <tli id="T534" time="383.97" type="appl" />
         <tli id="T535" time="384.6433333333334" type="appl" />
         <tli id="T536" time="385.31666666666666" type="appl" />
         <tli id="T537" time="385.99" type="appl" />
         <tli id="T538" time="386.54" type="appl" />
         <tli id="T539" time="387.09" type="appl" />
         <tli id="T540" time="387.64" type="appl" />
         <tli id="T541" time="387.76" type="appl" />
         <tli id="T542" time="388.3225" type="appl" />
         <tli id="T543" time="388.885" type="appl" />
         <tli id="T544" time="389.4475" type="appl" />
         <tli id="T545" time="390.01" type="appl" />
         <tli id="T546" time="390.51" type="appl" />
         <tli id="T547" time="391.25" type="appl" />
         <tli id="T548" time="391.98" type="appl" />
         <tli id="T549" time="392.71000000000004" type="appl" />
         <tli id="T550" time="393.44" type="appl" />
         <tli id="T551" time="394.17" type="appl" />
         <tli id="T552" time="394.67" type="appl" />
         <tli id="T553" time="395.64250000000004" type="appl" />
         <tli id="T554" time="396.615" type="appl" />
         <tli id="T555" time="397.5875" type="appl" />
         <tli id="T556" time="398.56" type="appl" />
         <tli id="T557" time="399.34" type="appl" />
         <tli id="T558" time="399.82" type="appl" />
         <tli id="T559" time="400.46" type="appl" />
         <tli id="T560" time="401.6334" type="appl" />
         <tli id="T561" time="402.8068" type="appl" />
         <tli id="T562" time="403.98019999999997" type="appl" />
         <tli id="T1186" time="404.80158" type="intp" />
         <tli id="T563" time="405.1536" type="appl" />
         <tli id="T564" time="406.327" type="appl" />
         <tli id="T565" time="407.15" type="appl" />
         <tli id="T566" time="407.84999999999997" type="appl" />
         <tli id="T567" time="408.55" type="appl" />
         <tli id="T568" time="409.25" type="appl" />
         <tli id="T569" time="409.69" type="appl" />
         <tli id="T570" time="410.49" type="appl" />
         <tli id="T571" time="411.28999999999996" type="appl" />
         <tli id="T572" time="412.09" type="appl" />
         <tli id="T573" time="413.11121431467444" />
         <tli id="T574" time="413.42" type="appl" />
         <tli id="T575" time="414.2916666666667" type="appl" />
         <tli id="T576" time="415.16333333333336" type="appl" />
         <tli id="T577" time="416.03499999999997" type="appl" />
         <tli id="T578" time="416.90666666666664" type="appl" />
         <tli id="T579" time="417.7783333333333" type="appl" />
         <tli id="T580" time="418.65" type="appl" />
         <tli id="T581" time="419.51" type="appl" />
         <tli id="T582" time="420.57" type="appl" />
         <tli id="T583" time="421.63" type="appl" />
         <tli id="T584" time="422.69" type="appl" />
         <tli id="T585" time="423.75" type="appl" />
         <tli id="T586" time="424.81" type="appl" />
         <tli id="T587" time="425.87" type="appl" />
         <tli id="T588" time="426.93" type="appl" />
         <tli id="T589" time="427.99" type="appl" />
         <tli id="T590" time="428.16" type="appl" />
         <tli id="T591" time="429.17333333333335" type="appl" />
         <tli id="T592" time="430.18666666666667" type="appl" />
         <tli id="T593" time="431.20000000000005" type="appl" />
         <tli id="T594" time="432.21333333333337" type="appl" />
         <tli id="T595" time="433.2266666666667" type="appl" />
         <tli id="T596" time="434.24" type="appl" />
         <tli id="T597" time="434.58" type="appl" />
         <tli id="T598" time="435.4733333333333" type="appl" />
         <tli id="T599" time="436.3666666666667" type="appl" />
         <tli id="T600" time="437.26" type="appl" />
         <tli id="T601" time="437.28" type="appl" />
         <tli id="T602" time="438.4762222222222" type="appl" />
         <tli id="T603" time="439.6724444444444" type="appl" />
         <tli id="T604" time="440.8686666666666" type="appl" />
         <tli id="T605" time="442.06488888888885" type="appl" />
         <tli id="T606" time="443.2611111111111" type="appl" />
         <tli id="T607" time="444.45733333333334" type="appl" />
         <tli id="T608" time="445.65355555555556" type="appl" />
         <tli id="T609" time="446.8497777777778" type="appl" />
         <tli id="T610" time="448.2910338627974" />
         <tli id="T611" time="449.12" type="appl" />
         <tli id="T612" time="449.66" type="appl" />
         <tli id="T613" time="450.16" type="appl" />
         <tli id="T614" time="450.80833333333334" type="appl" />
         <tli id="T615" time="451.4566666666667" type="appl" />
         <tli id="T616" time="452.105" type="appl" />
         <tli id="T617" time="452.75333333333333" type="appl" />
         <tli id="T618" time="453.4016666666667" type="appl" />
         <tli id="T619" time="454.05" type="appl" />
         <tli id="T620" time="454.06" type="appl" />
         <tli id="T621" time="455.435" type="appl" />
         <tli id="T622" time="456.81" type="appl" />
         <tli id="T623" time="458.185" type="appl" />
         <tli id="T624" time="459.56" type="appl" />
         <tli id="T625" time="459.57" type="appl" />
         <tli id="T626" time="460.32" type="appl" />
         <tli id="T627" time="461.07" type="appl" />
         <tli id="T628" time="461.82" type="appl" />
         <tli id="T629" time="462.57" type="appl" />
         <tli id="T630" time="463.32" type="appl" />
         <tli id="T631" time="463.51" type="appl" />
         <tli id="T632" time="464.1575" type="appl" />
         <tli id="T633" time="464.805" type="appl" />
         <tli id="T634" time="465.4525" type="appl" />
         <tli id="T635" time="466.1" type="appl" />
         <tli id="T636" time="467.69" type="appl" />
         <tli id="T637" time="468.33" type="appl" />
         <tli id="T638" time="468.65" type="appl" />
         <tli id="T639" time="469.45" type="appl" />
         <tli id="T640" time="470.25" type="appl" />
         <tli id="T641" time="471.04999999999995" type="appl" />
         <tli id="T642" time="471.84999999999997" type="appl" />
         <tli id="T643" time="472.65" type="appl" />
         <tli id="T644" time="473.32666666666665" type="appl" />
         <tli id="T645" time="474.00333333333333" type="appl" />
         <tli id="T1184" time="474.2933333333333" type="intp" />
         <tli id="T646" time="474.67999999999995" type="appl" />
         <tli id="T647" time="475.3566666666666" type="appl" />
         <tli id="T648" time="476.0333333333333" type="appl" />
         <tli id="T649" time="476.71" type="appl" />
         <tli id="T650" time="478.28" type="appl" />
         <tli id="T651" time="478.84" type="appl" />
         <tli id="T652" time="479.19" type="appl" />
         <tli id="T653" time="479.973" type="appl" />
         <tli id="T654" time="480.756" type="appl" />
         <tli id="T655" time="481.539" type="appl" />
         <tli id="T656" time="482.322" type="appl" />
         <tli id="T657" time="483.105" type="appl" />
         <tli id="T658" time="483.888" type="appl" />
         <tli id="T659" time="484.671" type="appl" />
         <tli id="T660" time="485.454" type="appl" />
         <tli id="T661" time="486.23699999999997" type="appl" />
         <tli id="T662" time="487.09750147500586" />
         <tli id="T663" time="488.57" type="appl" />
         <tli id="T664" time="489.04" type="appl" />
         <tli id="T665" time="489.97" type="appl" />
         <tli id="T666" time="490.90625" type="appl" />
         <tli id="T667" time="491.8425" type="appl" />
         <tli id="T668" time="492.77875" type="appl" />
         <tli id="T669" time="493.715" type="appl" />
         <tli id="T670" time="494.09" type="appl" />
         <tli id="T671" time="494.86" type="appl" />
         <tli id="T672" time="495.49" type="appl" />
         <tli id="T673" time="496.12" type="appl" />
         <tli id="T674" time="496.75" type="appl" />
         <tli id="T675" time="497.38" type="appl" />
         <tli id="T676" time="498.01" type="appl" />
         <tli id="T677" time="498.64" type="appl" />
         <tli id="T678" time="499.27" type="appl" />
         <tli id="T679" time="499.3333241290662" />
         <tli id="T680" time="500.01959999999997" type="appl" />
         <tli id="T681" time="500.69919999999996" type="appl" />
         <tli id="T682" time="501.3788" type="appl" />
         <tli id="T683" time="502.0584" type="appl" />
         <tli id="T684" time="502.73132753263155" />
         <tli id="T685" time="504.72" type="appl" />
         <tli id="T686" time="505.66400000000004" type="appl" />
         <tli id="T687" time="506.608" type="appl" />
         <tli id="T688" time="507.552" type="appl" />
         <tli id="T689" time="508.49600000000004" type="appl" />
         <tli id="T690" time="509.44" type="appl" />
         <tli id="T691" time="510.384" type="appl" />
         <tli id="T692" time="511.13" type="appl" />
         <tli id="T693" time="511.7" type="appl" />
         <tli id="T694" time="514.09" type="appl" />
         <tli id="T695" time="514.7" type="appl" />
         <tli id="T696" time="515.213" type="appl" />
         <tli id="T697" time="515.805" type="appl" />
         <tli id="T1187" time="516.23" type="intp" />
         <tli id="T698" time="516.5133333333333" type="appl" />
         <tli id="T699" time="517.2216666666666" type="appl" />
         <tli id="T700" time="517.93" type="appl" />
         <tli id="T701" time="518.69625" type="appl" />
         <tli id="T702" time="519.4625" type="appl" />
         <tli id="T703" time="520.22875" type="appl" />
         <tli id="T704" time="520.9949999999999" type="appl" />
         <tli id="T705" time="521.7612499999999" type="appl" />
         <tli id="T706" time="522.5274999999999" type="appl" />
         <tli id="T707" time="523.2937499999999" type="appl" />
         <tli id="T708" time="524.06" type="appl" />
         <tli id="T709" time="524.74" type="appl" />
         <tli id="T710" time="525.42" type="appl" />
         <tli id="T711" time="526.1" type="appl" />
         <tli id="T712" time="526.106" type="appl" />
         <tli id="T713" time="526.734" type="appl" />
         <tli id="T714" time="527.362" type="appl" />
         <tli id="T715" time="527.5772938373714" />
         <tli id="T716" time="529.208" type="appl" />
         <tli id="T717" time="530.086" type="appl" />
         <tli id="T718" time="530.9639999999999" type="appl" />
         <tli id="T719" time="531.842" type="appl" />
         <tli id="T720" time="533.119" type="appl" />
         <tli id="T721" time="533.459" type="appl" />
         <tli id="T722" time="534.463" type="appl" />
         <tli id="T723" time="535.212" type="appl" />
         <tli id="T724" time="535.961" type="appl" />
         <tli id="T725" time="536.7099999999999" type="appl" />
         <tli id="T726" time="537.459" type="appl" />
         <tli id="T727" time="537.92625" type="appl" />
         <tli id="T728" time="538.3934999999999" type="appl" />
         <tli id="T729" time="538.8607499999999" type="appl" />
         <tli id="T730" time="539.328" type="appl" />
         <tli id="T731" time="539.684" type="appl" />
         <tli id="T732" time="540.5776" type="appl" />
         <tli id="T733" time="541.4712" type="appl" />
         <tli id="T734" time="542.3648000000001" type="appl" />
         <tli id="T735" time="543.2584" type="appl" />
         <tli id="T736" time="544.152" type="appl" />
         <tli id="T737" time="546.456" type="appl" />
         <tli id="T738" time="546.896" type="appl" />
         <tli id="T739" time="547.098" type="appl" />
         <tli id="T740" time="547.7189999999999" type="appl" />
         <tli id="T741" time="548.3399999999999" type="appl" />
         <tli id="T742" time="548.961" type="appl" />
         <tli id="T743" time="549.582" type="appl" />
         <tli id="T744" time="550.203" type="appl" />
         <tli id="T745" time="550.8240000000001" type="appl" />
         <tli id="T746" time="551.445" type="appl" />
         <tli id="T747" time="552.066" type="appl" />
         <tli id="T748" time="552.696" type="appl" />
         <tli id="T749" time="553.016" type="appl" />
         <tli id="T750" time="553.177" type="appl" />
         <tli id="T751" time="553.72" type="appl" />
         <tli id="T752" time="554.263" type="appl" />
         <tli id="T753" time="554.806" type="appl" />
         <tli id="T754" time="555.01" type="appl" />
         <tli id="T755" time="555.6764285714286" type="appl" />
         <tli id="T756" time="556.3428571428572" type="appl" />
         <tli id="T757" time="557.0092857142857" type="appl" />
         <tli id="T758" time="557.6757142857142" type="appl" />
         <tli id="T759" time="558.3421428571428" type="appl" />
         <tli id="T760" time="559.0085714285714" type="appl" />
         <tli id="T761" time="559.675" type="appl" />
         <tli id="T762" time="560.5" type="appl" />
         <tli id="T763" time="561.5542857142857" type="appl" />
         <tli id="T764" time="562.6085714285714" type="appl" />
         <tli id="T765" time="563.6628571428571" type="appl" />
         <tli id="T766" time="564.7171428571429" type="appl" />
         <tli id="T767" time="565.7714285714286" type="appl" />
         <tli id="T768" time="566.8257142857143" type="appl" />
         <tli id="T769" time="567.88" type="appl" />
         <tli id="T770" time="568.035" type="appl" />
         <tli id="T771" time="568.6516" type="appl" />
         <tli id="T772" time="569.2682" type="appl" />
         <tli id="T773" time="569.8848" type="appl" />
         <tli id="T774" time="570.5014" type="appl" />
         <tli id="T775" time="571.118" type="appl" />
         <tli id="T776" time="572.1205" type="appl" />
         <tli id="T777" time="573.123" type="appl" />
         <tli id="T778" time="574.1255000000001" type="appl" />
         <tli id="T779" time="575.128" type="appl" />
         <tli id="T780" time="576.1305" type="appl" />
         <tli id="T781" time="577.133" type="appl" />
         <tli id="T782" time="577.727" type="appl" />
         <tli id="T783" time="578.2716666666666" type="appl" />
         <tli id="T784" time="578.8163333333333" type="appl" />
         <tli id="T785" time="579.361" type="appl" />
         <tli id="T786" time="580.37" type="appl" />
         <tli id="T787" time="581.452" type="appl" />
         <tli id="T788" time="582.534" type="appl" />
         <tli id="T789" time="583.616" type="appl" />
         <tli id="T790" time="584.515" type="appl" />
         <tli id="T791" time="585.095" type="appl" />
         <tli id="T792" time="586.186" type="appl" />
         <tli id="T793" time="586.972125" type="appl" />
         <tli id="T794" time="587.7582500000001" type="appl" />
         <tli id="T795" time="588.5443750000001" type="appl" />
         <tli id="T796" time="589.3305" type="appl" />
         <tli id="T797" time="590.116625" type="appl" />
         <tli id="T798" time="590.90275" type="appl" />
         <tli id="T799" time="591.688875" type="appl" />
         <tli id="T800" time="592.475" type="appl" />
         <tli id="T801" time="593.0814545454546" type="appl" />
         <tli id="T802" time="593.6879090909091" type="appl" />
         <tli id="T803" time="594.2943636363636" type="appl" />
         <tli id="T804" time="594.9008181818182" type="appl" />
         <tli id="T805" time="595.5072727272727" type="appl" />
         <tli id="T806" time="596.1137272727273" type="appl" />
         <tli id="T807" time="596.7201818181818" type="appl" />
         <tli id="T808" time="597.3266363636363" type="appl" />
         <tli id="T809" time="597.9330909090909" type="appl" />
         <tli id="T810" time="598.5395454545454" type="appl" />
         <tli id="T811" time="599.146" type="appl" />
         <tli id="T812" time="599.72" type="appl" />
         <tli id="T813" time="600.11" type="appl" />
         <tli id="T814" time="600.313" type="appl" />
         <tli id="T815" time="600.9486666666667" type="appl" />
         <tli id="T816" time="601.5843333333333" type="appl" />
         <tli id="T817" time="602.22" type="appl" />
         <tli id="T818" time="602.9208333333333" type="appl" />
         <tli id="T819" time="603.6216666666667" type="appl" />
         <tli id="T820" time="604.3225" type="appl" />
         <tli id="T821" time="605.0233333333333" type="appl" />
         <tli id="T822" time="605.7241666666666" type="appl" />
         <tli id="T823" time="606.425" type="appl" />
         <tli id="T824" time="606.769" type="appl" />
         <tli id="T825" time="607.3028571428572" type="appl" />
         <tli id="T826" time="607.8367142857143" type="appl" />
         <tli id="T827" time="608.3705714285715" type="appl" />
         <tli id="T828" time="608.9044285714285" type="appl" />
         <tli id="T829" time="609.4382857142857" type="appl" />
         <tli id="T830" time="609.9721428571428" type="appl" />
         <tli id="T831" time="610.506" type="appl" />
         <tli id="T832" time="611.022" type="appl" />
         <tli id="T833" time="612.3348000000001" type="appl" />
         <tli id="T834" time="613.6476" type="appl" />
         <tli id="T835" time="614.9604" type="appl" />
         <tli id="T836" time="616.2732" type="appl" />
         <tli id="T837" time="617.586" type="appl" />
         <tli id="T838" time="618.103" type="appl" />
         <tli id="T839" time="618.443" type="appl" />
         <tli id="T840" time="618.448" type="appl" />
         <tli id="T841" time="618.94325" type="appl" />
         <tli id="T842" time="619.4385" type="appl" />
         <tli id="T843" time="619.9337499999999" type="appl" />
         <tli id="T844" time="620.429" type="appl" />
         <tli id="T845" time="621.6225" type="appl" />
         <tli id="T846" time="622.816" type="appl" />
         <tli id="T847" time="624.0095" type="appl" />
         <tli id="T848" time="625.036793927197" />
         <tli id="T849" time="626.3515" type="appl" />
         <tli id="T850" time="627.5" type="appl" />
         <tli id="T851" time="628.6485" type="appl" />
         <tli id="T852" time="629.797" type="appl" />
         <tli id="T853" time="630.9455" type="appl" />
         <tli id="T854" time="632.094" type="appl" />
         <tli id="T855" time="632.58" type="appl" />
         <tli id="T856" time="633.02" type="appl" />
         <tli id="T857" time="633.8" type="appl" />
         <tli id="T858" time="634.75" type="appl" />
         <tli id="T859" time="635.7" type="appl" />
         <tli id="T860" time="636.65" type="appl" />
         <tli id="T861" time="637.6" type="appl" />
         <tli id="T862" time="637.71" type="appl" />
         <tli id="T863" time="638.25" type="appl" />
         <tli id="T864" time="638.79" type="appl" />
         <tli id="T865" time="639.33" type="appl" />
         <tli id="T866" time="639.87" type="appl" />
         <tli id="T867" time="640.41" type="appl" />
         <tli id="T868" time="640.53" type="appl" />
         <tli id="T869" time="641.46" type="appl" />
         <tli id="T870" time="642.39" type="appl" />
         <tli id="T871" time="643.32" type="appl" />
         <tli id="T872" time="643.94" type="appl" />
         <tli id="T873" time="644.31" type="appl" />
         <tli id="T874" time="646.01" type="appl" />
         <tli id="T875" time="646.775" type="appl" />
         <tli id="T876" time="647.54" type="appl" />
         <tli id="T877" time="648.3050000000001" type="appl" />
         <tli id="T878" time="649.07" type="appl" />
         <tli id="T879" time="649.835" type="appl" />
         <tli id="T880" time="650.6833290421542" />
         <tli id="T881" time="651.6" type="appl" />
         <tli id="T882" time="652.15" type="appl" />
         <tli id="T883" time="652.7" type="appl" />
         <tli id="T884" time="653.25" type="appl" />
         <tli id="T885" time="653.8" type="appl" />
         <tli id="T886" time="654.35" type="appl" />
         <tli id="T887" time="654.9" type="appl" />
         <tli id="T888" time="655.4499999999999" type="appl" />
         <tli id="T889" time="656.0" type="appl" />
         <tli id="T890" time="656.656631735942" />
         <tli id="T891" time="657.16" type="appl" />
         <tli id="T892" time="657.67" type="appl" />
         <tli id="T893" time="658.589" type="appl" />
         <tli id="T894" time="659.1947142857143" type="appl" />
         <tli id="T895" time="659.8004285714286" type="appl" />
         <tli id="T896" time="660.4061428571429" type="appl" />
         <tli id="T897" time="661.0118571428571" type="appl" />
         <tli id="T898" time="661.6175714285714" type="appl" />
         <tli id="T899" time="662.2232857142857" type="appl" />
         <tli id="T900" time="662.829" type="appl" />
         <tli id="T901" time="662.899" type="appl" />
         <tli id="T902" time="663.934" type="appl" />
         <tli id="T903" time="664.969" type="appl" />
         <tli id="T904" time="665.429" type="appl" />
         <tli id="T905" time="665.769" type="appl" />
         <tli id="T906" time="666.11" type="appl" />
         <tli id="T907" time="666.904" type="appl" />
         <tli id="T908" time="667.698" type="appl" />
         <tli id="T909" time="668.492" type="appl" />
         <tli id="T910" time="669.286" type="appl" />
         <tli id="T911" time="670.0799999999999" type="appl" />
         <tli id="T912" time="670.874" type="appl" />
         <tli id="T913" time="671.668" type="appl" />
         <tli id="T914" time="672.462" type="appl" />
         <tli id="T915" time="673.256" type="appl" />
         <tli id="T916" time="674.05" type="appl" />
         <tli id="T917" time="674.3" type="appl" />
         <tli id="T918" time="674.58" type="appl" />
         <tli id="T919" time="675.5" type="appl" />
         <tli id="T920" time="676.5" type="appl" />
         <tli id="T921" time="677.5" type="appl" />
         <tli id="T922" time="678.5" type="appl" />
         <tli id="T923" time="679.5" type="appl" />
         <tli id="T924" time="679.75" type="appl" />
         <tli id="T925" time="680.16" type="appl" />
         <tli id="T926" time="680.27" type="appl" />
         <tli id="T927" time="681.0044444444444" type="appl" />
         <tli id="T928" time="681.7388888888888" type="appl" />
         <tli id="T929" time="682.4733333333334" type="appl" />
         <tli id="T930" time="683.2077777777778" type="appl" />
         <tli id="T931" time="683.9422222222222" type="appl" />
         <tli id="T932" time="684.6766666666666" type="appl" />
         <tli id="T933" time="685.4111111111112" type="appl" />
         <tli id="T934" time="686.1455555555556" type="appl" />
         <tli id="T935" time="686.88" type="appl" />
         <tli id="T936" time="687.47" type="appl" />
         <tli id="T937" time="688.09" type="appl" />
         <tli id="T938" time="688.58" type="appl" />
         <tli id="T939" time="689.32" type="appl" />
         <tli id="T940" time="690.06" type="appl" />
         <tli id="T941" time="690.8" type="appl" />
         <tli id="T942" time="691.46" type="appl" />
         <tli id="T943" time="692.06" type="appl" />
         <tli id="T944" time="692.54" type="appl" />
         <tli id="T945" time="693.466" type="appl" />
         <tli id="T946" time="694.3919999999999" type="appl" />
         <tli id="T947" time="695.318" type="appl" />
         <tli id="T948" time="696.2439999999999" type="appl" />
         <tli id="T949" time="697.17" type="appl" />
         <tli id="T950" time="697.63" type="appl" />
         <tli id="T951" time="698.13" type="appl" />
         <tli id="T952" time="698.29" type="appl" />
         <tli id="T953" time="698.8533333333334" type="appl" />
         <tli id="T954" time="699.4166666666666" type="appl" />
         <tli id="T955" time="699.98" type="appl" />
         <tli id="T956" time="700.5433333333333" type="appl" />
         <tli id="T957" time="701.1066666666667" type="appl" />
         <tli id="T958" time="701.67" type="appl" />
         <tli id="T959" time="702.2333333333333" type="appl" />
         <tli id="T960" time="702.7966666666666" type="appl" />
         <tli id="T961" time="703.36" type="appl" />
         <tli id="T962" time="703.7" type="appl" />
         <tli id="T963" time="704.3925" type="appl" />
         <tli id="T964" time="705.085" type="appl" />
         <tli id="T965" time="705.7775" type="appl" />
         <tli id="T966" time="706.47" type="appl" />
         <tli id="T967" time="707.1625" type="appl" />
         <tli id="T968" time="707.855" type="appl" />
         <tli id="T969" time="708.5475" type="appl" />
         <tli id="T970" time="709.24" type="appl" />
         <tli id="T971" time="710.41" type="appl" />
         <tli id="T972" time="711.15" type="appl" />
         <tli id="T973" time="711.83" type="appl" />
         <tli id="T974" time="712.84" type="appl" />
         <tli id="T975" time="713.85" type="appl" />
         <tli id="T976" time="714.86" type="appl" />
         <tli id="T977" time="715.87" type="appl" />
         <tli id="T978" time="715.99" type="appl" />
         <tli id="T979" time="716.7950000000001" type="appl" />
         <tli id="T980" time="717.6" type="appl" />
         <tli id="T981" time="718.405" type="appl" />
         <tli id="T982" time="719.21" type="appl" />
         <tli id="T983" time="719.31" type="appl" />
         <tli id="T984" time="720.2099999999999" type="appl" />
         <tli id="T985" time="721.1099999999999" type="appl" />
         <tli id="T986" time="722.01" type="appl" />
         <tli id="T987" time="722.5096272819642" />
         <tli id="T988" time="723.31" type="appl" />
         <tli id="T989" time="724.1225" type="appl" />
         <tli id="T990" time="724.935" type="appl" />
         <tli id="T991" time="725.7475" type="appl" />
         <tli id="T992" time="726.56" type="appl" />
         <tli id="T993" time="726.62" type="appl" />
         <tli id="T994" time="727.386" type="appl" />
         <tli id="T995" time="728.152" type="appl" />
         <tli id="T996" time="728.918" type="appl" />
         <tli id="T997" time="729.6840000000001" type="appl" />
         <tli id="T998" time="730.45" type="appl" />
         <tli id="T999" time="730.61" type="appl" />
         <tli id="T1000" time="731.50225" type="appl" />
         <tli id="T1001" time="732.3945" type="appl" />
         <tli id="T1002" time="733.28675" type="appl" />
         <tli id="T1003" time="734.179" type="appl" />
         <tli id="T1004" time="735.13" type="appl" />
         <tli id="T1005" time="735.49" type="appl" />
         <tli id="T1006" time="736.23" type="appl" />
         <tli id="T1007" time="737.0283333333333" type="appl" />
         <tli id="T1008" time="737.8266666666667" type="appl" />
         <tli id="T1009" time="738.625" type="appl" />
         <tli id="T1010" time="739.4233333333333" type="appl" />
         <tli id="T1011" time="740.2216666666667" type="appl" />
         <tli id="T1012" time="741.02" type="appl" />
         <tli id="T1013" time="741.7674999999999" type="appl" />
         <tli id="T1014" time="742.515" type="appl" />
         <tli id="T1015" time="743.2625" type="appl" />
         <tli id="T1016" time="744.01" type="appl" />
         <tli id="T1017" time="744.94" type="appl" />
         <tli id="T1018" time="745.7366666666667" type="appl" />
         <tli id="T1019" time="746.5333333333334" type="appl" />
         <tli id="T1020" time="747.33" type="appl" />
         <tli id="T1021" time="749.38" type="appl" />
         <tli id="T1022" time="750.0793636363636" type="appl" />
         <tli id="T1023" time="750.7787272727272" type="appl" />
         <tli id="T1024" time="751.478090909091" type="appl" />
         <tli id="T1025" time="752.1774545454546" type="appl" />
         <tli id="T1026" time="752.8768181818182" type="appl" />
         <tli id="T1027" time="753.5761818181818" type="appl" />
         <tli id="T1028" time="754.2755454545454" type="appl" />
         <tli id="T1029" time="754.974909090909" type="appl" />
         <tli id="T1030" time="755.6742727272728" type="appl" />
         <tli id="T1031" time="756.3736363636364" type="appl" />
         <tli id="T1032" time="757.073" type="appl" />
         <tli id="T1033" time="757.95" type="appl" />
         <tli id="T1034" time="758.57" type="appl" />
         <tli id="T1035" time="759.1899999999999" type="appl" />
         <tli id="T1036" time="759.81" type="appl" />
         <tli id="T1037" time="760.01" type="appl" />
         <tli id="T1038" time="760.4218181818181" type="appl" />
         <tli id="T1039" time="760.8336363636364" type="appl" />
         <tli id="T1040" time="761.2454545454545" type="appl" />
         <tli id="T1041" time="761.6572727272727" type="appl" />
         <tli id="T1042" time="762.0690909090908" type="appl" />
         <tli id="T1043" time="762.4809090909091" type="appl" />
         <tli id="T1181" time="762.6574025974026" type="intp" />
         <tli id="T1044" time="762.8927272727273" type="appl" />
         <tli id="T1045" time="763.3045454545454" type="appl" />
         <tli id="T1046" time="763.7163636363636" type="appl" />
         <tli id="T1047" time="764.1281818181818" type="appl" />
         <tli id="T1048" time="764.6399840945671" />
         <tli id="T1049" time="764.6466507270378" />
         <tli id="T1050" time="765.2266666666667" type="appl" />
         <tli id="T1051" time="765.8333333333334" type="appl" />
         <tli id="T1052" time="766.44" type="appl" />
         <tli id="T1053" time="767.29" type="appl" />
         <tli id="T1054" time="767.63" type="appl" />
         <tli id="T1055" time="767.73" type="appl" />
         <tli id="T1056" time="768.4933333333333" type="appl" />
         <tli id="T1057" time="769.2566666666667" type="appl" />
         <tli id="T1058" time="770.02" type="appl" />
         <tli id="T1059" time="770.4" type="appl" />
         <tli id="T1060" time="771.0803333333333" type="appl" />
         <tli id="T1061" time="771.7606666666667" type="appl" />
         <tli id="T1062" time="772.441" type="appl" />
         <tli id="T1063" time="773.1213333333334" type="appl" />
         <tli id="T1064" time="773.8016666666666" type="appl" />
         <tli id="T1065" time="774.482" type="appl" />
         <tli id="T1066" time="775.1623333333333" type="appl" />
         <tli id="T1067" time="775.8426666666667" type="appl" />
         <tli id="T1068" time="776.523" type="appl" />
         <tli id="T1069" time="777.33" type="appl" />
         <tli id="T1070" time="777.63" type="appl" />
         <tli id="T1071" time="778.02" type="appl" />
         <tli id="T1072" time="778.8924999999999" type="appl" />
         <tli id="T1073" time="779.765" type="appl" />
         <tli id="T1074" time="780.6375" type="appl" />
         <tli id="T1075" time="781.7626566819484" />
         <tli id="T1076" time="784.31" type="appl" />
         <tli id="T1077" time="785.1683333333333" type="appl" />
         <tli id="T1078" time="786.0266666666666" type="appl" />
         <tli id="T1079" time="786.885" type="appl" />
         <tli id="T1080" time="787.7433333333333" type="appl" />
         <tli id="T1081" time="788.6016666666667" type="appl" />
         <tli id="T1082" time="789.46" type="appl" />
         <tli id="T1083" time="790.05" type="appl" />
         <tli id="T1084" time="790.38" type="appl" />
         <tli id="T1085" time="790.81" type="appl" />
         <tli id="T1086" time="791.8199999999999" type="appl" />
         <tli id="T1087" time="792.8299999999999" type="appl" />
         <tli id="T1088" time="793.84" type="appl" />
         <tli id="T1089" time="794.85" type="appl" />
         <tli id="T1090" time="795.8959175199285" />
         <tli id="T1091" time="796.5766666666667" type="appl" />
         <tli id="T1092" time="797.2933333333333" type="appl" />
         <tli id="T1093" time="798.01" type="appl" />
         <tli id="T1094" time="798.7266666666667" type="appl" />
         <tli id="T1095" time="799.4433333333333" type="appl" />
         <tli id="T1096" time="800.2358952583837" />
         <tli id="T1097" time="800.36" type="appl" />
         <tli id="T1098" time="801.0996" type="appl" />
         <tli id="T1099" time="801.8392" type="appl" />
         <tli id="T1100" time="802.5788" type="appl" />
         <tli id="T1101" time="803.3184" type="appl" />
         <tli id="T1102" time="804.058" type="appl" />
         <tli id="T1103" time="804.7976" type="appl" />
         <tli id="T1104" time="805.5372" type="appl" />
         <tli id="T1105" time="806.2768" type="appl" />
         <tli id="T1106" time="807.0164" type="appl" />
         <tli id="T1107" time="807.5158579164377" />
         <tli id="T1108" time="808.56" type="appl" />
         <tli id="T1109" time="808.88" type="appl" />
         <tli id="T1110" time="809.3" type="appl" />
         <tli id="T1111" time="809.8855555555555" type="appl" />
         <tli id="T1112" time="810.4711111111111" type="appl" />
         <tli id="T1113" time="811.0566666666666" type="appl" />
         <tli id="T1114" time="811.6422222222222" type="appl" />
         <tli id="T1115" time="812.2277777777778" type="appl" />
         <tli id="T1116" time="812.8133333333334" type="appl" />
         <tli id="T1117" time="813.3988888888889" type="appl" />
         <tli id="T1118" time="813.9844444444445" type="appl" />
         <tli id="T1119" time="814.569988399545" />
         <tli id="T1120" time="814.85" type="appl" />
         <tli id="T1121" time="815.4766666666667" type="appl" />
         <tli id="T1122" time="816.1033333333334" type="appl" />
         <tli id="T1123" time="816.73" type="appl" />
         <tli id="T1124" time="817.3566666666667" type="appl" />
         <tli id="T1125" time="817.9833333333333" type="appl" />
         <tli id="T1126" time="818.61" type="appl" />
         <tli id="T1127" time="818.8" type="appl" />
         <tli id="T1128" time="819.4599999999999" type="appl" />
         <tli id="T1129" time="820.12" type="appl" />
         <tli id="T1130" time="820.78" type="appl" />
         <tli id="T1131" time="821.74" type="appl" />
         <tli id="T1132" time="822.25" type="appl" />
         <tli id="T1133" time="822.67" type="appl" />
         <tli id="T1134" time="823.2766666666666" type="appl" />
         <tli id="T1135" time="823.8833333333333" type="appl" />
         <tli id="T1136" time="824.49" type="appl" />
         <tli id="T1137" time="825.0966666666666" type="appl" />
         <tli id="T1138" time="825.7033333333333" type="appl" />
         <tli id="T1139" time="826.31" type="appl" />
         <tli id="T1140" time="826.34" type="appl" />
         <tli id="T1141" time="827.0" type="appl" />
         <tli id="T1142" time="827.66" type="appl" />
         <tli id="T1143" time="828.3199999999999" type="appl" />
         <tli id="T1144" time="828.98" type="appl" />
         <tli id="T1145" time="829.64" type="appl" />
         <tli id="T1146" time="830.3" type="appl" />
         <tli id="T1147" time="830.46" type="appl" />
         <tli id="T1148" time="831.2833333333333" type="appl" />
         <tli id="T1149" time="832.1066666666667" type="appl" />
         <tli id="T1150" time="832.93" type="appl" />
         <tli id="T1151" time="834.74" type="appl" />
         <tli id="T1152" time="835.27" type="appl" />
         <tli id="T1153" time="835.38" type="appl" />
         <tli id="T1154" time="835.9516666666666" type="appl" />
         <tli id="T1155" time="836.5233333333333" type="appl" />
         <tli id="T1156" time="837.095" type="appl" />
         <tli id="T1157" time="837.6666666666666" type="appl" />
         <tli id="T1158" time="838.2383333333332" type="appl" />
         <tli id="T1159" time="838.81" type="appl" />
         <tli id="T1160" time="839.94" type="appl" />
         <tli id="T1161" time="840.27" type="appl" />
         <tli id="T1162" time="840.44" type="appl" />
         <tli id="T1163" time="840.9183333333334" type="appl" />
         <tli id="T1164" time="841.3966666666666" type="appl" />
         <tli id="T1165" time="841.875" type="appl" />
         <tli id="T1166" time="842.3533333333334" type="appl" />
         <tli id="T1167" time="842.8316666666666" type="appl" />
         <tli id="T1168" time="843.31" type="appl" />
         <tli id="T1169" time="843.34" type="appl" />
         <tli id="T1170" time="844.22" type="appl" />
         <tli id="T1171" time="845.1" type="appl" />
         <tli id="T1172" time="846.033316594426" />
         <tli id="T1173" time="846.0366499106614" />
         <tli id="T1174" time="846.6375" type="appl" />
         <tli id="T1175" time="847.2850000000001" type="appl" />
         <tli id="T1176" time="847.9325" type="appl" />
         <tli id="T1177" time="848.58" type="appl" />
         <tli id="T0" time="848.862" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T91" start="T90">
            <tli id="T90.tx.1" />
         </timeline-fork>
         <timeline-fork end="T413" start="T412">
            <tli id="T412.tx.1" />
         </timeline-fork>
         <timeline-fork end="T522" start="T521">
            <tli id="T521.tx.1" />
         </timeline-fork>
         <timeline-fork end="T696" start="T695">
            <tli id="T695.tx.1" />
         </timeline-fork>
         <timeline-fork end="T698" start="T697">
            <tli id="T697.tx.1" />
         </timeline-fork>
         <timeline-fork end="T1172" start="T1171">
            <tli id="T1171.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">taldʼen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kambiam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tuganbə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_16" n="sc" s="T6">
               <ts e="T14" id="Seg_18" n="HIAT:u" s="T6">
                  <nts id="Seg_19" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">Măn</ts>
                  <nts id="Seg_22" n="HIAT:ip">)</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">mămbiam:</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">Iʔ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">kuroʔ</ts>
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">măna</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">nelʼzʼa</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">bɨlo</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">šoʔsittə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_49" n="sc" s="T15">
               <ts e="T22" id="Seg_51" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">Meim</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">ej</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">kambi</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">i</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">măn</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">ej</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">kambiam</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_75" n="sc" s="T23">
               <ts e="T27" id="Seg_77" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_79" n="HIAT:w" s="T23">Dĭzeŋ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">krăvatʼkən</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_85" n="HIAT:w" s="T25">iʔbolaʔbəʔjə</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">kamrolaʔbəʔjə</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_92" n="sc" s="T28">
               <ts e="T30" id="Seg_94" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T28">Kolʼa</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_99" n="HIAT:w" s="T29">Dunʼatsi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_102" n="sc" s="T31">
               <ts e="T32" id="Seg_104" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_106" n="HIAT:w" s="T31">Pănarlaʔbəʔjə</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T34" id="Seg_109" n="sc" s="T33">
               <ts e="T34" id="Seg_111" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_113" n="HIAT:w" s="T33">Kaknarlaʔbəʔjə</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_116" n="sc" s="T35">
               <ts e="T39" id="Seg_118" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">Măndəʔi:</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <ts e="T37" id="Seg_124" n="HIAT:w" s="T36">Urgaja</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_128" n="HIAT:w" s="T37">amaʔ</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">amnaʔ</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_135" n="sc" s="T40">
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_139" n="HIAT:w" s="T40">Ara</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_142" n="HIAT:w" s="T41">bĭdeʔ</ts>
                  <nts id="Seg_143" n="HIAT:ip">"</nts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_146" n="sc" s="T43">
               <ts e="T47" id="Seg_148" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_150" n="HIAT:w" s="T43">Măn</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_153" n="HIAT:w" s="T44">măndəm:</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_157" n="HIAT:w" s="T45">Ej</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_160" n="HIAT:w" s="T46">bĭtləm</ts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_164" n="sc" s="T48">
               <ts e="T52" id="Seg_166" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">I</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_171" n="HIAT:w" s="T49">dĭgəttə</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_174" n="HIAT:w" s="T50">kalla</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T1178">dʼürbiem</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">maʔnʼi</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_183" n="sc" s="T53">
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T53">
                  <nts id="Seg_186" n="HIAT:ip">(</nts>
                  <nts id="Seg_187" n="HIAT:ip">(</nts>
                  <ats e="T54" id="Seg_188" n="HIAT:non-pho" s="T53">BRK</ats>
                  <nts id="Seg_189" n="HIAT:ip">)</nts>
                  <nts id="Seg_190" n="HIAT:ip">)</nts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_193" n="sc" s="T55">
               <ts e="T61" id="Seg_195" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_197" n="HIAT:w" s="T55">Măn</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_200" n="HIAT:w" s="T56">teinen</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_203" n="HIAT:w" s="T57">nükenə</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">kambiam</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_210" n="HIAT:w" s="T59">dĭ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">ĭzemnie</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_216" n="sc" s="T62">
               <ts e="T71" id="Seg_218" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_220" n="HIAT:w" s="T62">Dĭ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_223" n="HIAT:w" s="T63">măndə:</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_225" n="HIAT:ip">"</nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">Ĭmbi</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">kondʼo</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_233" n="HIAT:w" s="T66">ej</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_236" n="HIAT:w" s="T67">šobiam</ts>
                  <nts id="Seg_237" n="HIAT:ip">,</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_240" n="HIAT:w" s="T68">măn</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_243" n="HIAT:w" s="T69">bar</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_246" n="HIAT:w" s="T70">kulandəgam</ts>
                  <nts id="Seg_247" n="HIAT:ip">"</nts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_251" n="HIAT:u" s="T71">
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <ts e="T72" id="Seg_254" n="HIAT:w" s="T71">Tăn</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_257" n="HIAT:w" s="T72">üge</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_260" n="HIAT:w" s="T73">kulandəgal</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_264" n="HIAT:w" s="T74">a</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_267" n="HIAT:w" s="T75">küsʼtə</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_270" n="HIAT:w" s="T76">ej</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_273" n="HIAT:w" s="T77">molial</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_276" n="sc" s="T79">
               <ts e="T81" id="Seg_278" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">No</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_284" n="HIAT:w" s="T80">küʔ</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_287" n="sc" s="T82">
               <ts e="T91" id="Seg_289" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_291" n="HIAT:w" s="T82">Măna</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_294" n="HIAT:w" s="T83">ĭmbi</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_299" n="HIAT:w" s="T84">m-</ts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_303" n="HIAT:w" s="T85">ĭmbi</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_306" n="HIAT:w" s="T86">măn</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_309" n="HIAT:w" s="T87">tăn</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_312" n="HIAT:w" s="T88">tondə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_315" n="HIAT:w" s="T89">amnolaʔləm</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90.tx.1" id="Seg_318" n="HIAT:w" s="T90">što</ts>
                  <nts id="Seg_319" n="HIAT:ip">_</nts>
                  <ts e="T91" id="Seg_321" n="HIAT:w" s="T90.tx.1">li</ts>
                  <nts id="Seg_322" n="HIAT:ip">?</nts>
                  <nts id="Seg_323" n="HIAT:ip">"</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T95" id="Seg_325" n="sc" s="T92">
               <ts e="T95" id="Seg_327" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_329" n="HIAT:w" s="T92">Măn</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_332" n="HIAT:w" s="T93">vedʼ</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_335" n="HIAT:w" s="T1185">jilə</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_338" n="HIAT:w" s="T94">amnoʔlaʔbəʔjə</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T99" id="Seg_341" n="sc" s="T96">
               <ts e="T99" id="Seg_343" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_345" n="HIAT:w" s="T96">Dĭn</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_348" n="HIAT:w" s="T97">edəʔleʔbəʔjə</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_351" n="HIAT:w" s="T98">măna</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_354" n="sc" s="T100">
               <ts e="T102" id="Seg_356" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_358" n="HIAT:w" s="T100">Dʼăbaktərzittə</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_361" n="HIAT:w" s="T101">nada</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_364" n="sc" s="T103">
               <ts e="T107" id="Seg_366" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_368" n="HIAT:w" s="T103">A</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_371" n="HIAT:w" s="T104">tăn</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_374" n="HIAT:w" s="T105">tondə</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_377" n="HIAT:w" s="T106">amnoʔ</ts>
                  <nts id="Seg_378" n="HIAT:ip">"</nts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T109" id="Seg_381" n="sc" s="T108">
               <ts e="T109" id="Seg_383" n="HIAT:u" s="T108">
                  <nts id="Seg_384" n="HIAT:ip">(</nts>
                  <nts id="Seg_385" n="HIAT:ip">(</nts>
                  <ats e="T109" id="Seg_386" n="HIAT:non-pho" s="T108">BRK</ats>
                  <nts id="Seg_387" n="HIAT:ip">)</nts>
                  <nts id="Seg_388" n="HIAT:ip">)</nts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_391" n="sc" s="T110">
               <ts e="T115" id="Seg_393" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_395" n="HIAT:w" s="T110">Meim</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_397" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_399" n="HIAT:w" s="T111">š-</ts>
                  <nts id="Seg_400" n="HIAT:ip">)</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_403" n="HIAT:w" s="T112">šide</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_406" n="HIAT:w" s="T113">dʼala</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_409" n="HIAT:w" s="T114">ibi</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_412" n="sc" s="T116">
               <ts e="T120" id="Seg_414" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_416" n="HIAT:w" s="T116">A</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T117">tăn</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_422" n="HIAT:w" s="T118">tondə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_425" n="HIAT:w" s="T119">amnoʔ</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_428" n="sc" s="T121">
               <ts e="T124" id="Seg_430" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">Da</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">jil</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_438" n="HIAT:w" s="T123">mĭŋgəʔi</ts>
                  <nts id="Seg_439" n="HIAT:ip">"</nts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_442" n="sc" s="T125">
               <ts e="T131" id="Seg_444" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_446" n="HIAT:w" s="T125">A</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_449" n="HIAT:w" s="T126">dĭ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_452" n="HIAT:w" s="T127">măndə:</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_456" n="HIAT:w" s="T128">A</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_459" n="HIAT:w" s="T129">kudaj</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_462" n="HIAT:w" s="T130">dĭzəŋzi</ts>
                  <nts id="Seg_463" n="HIAT:ip">"</nts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T136" id="Seg_466" n="sc" s="T132">
               <ts e="T136" id="Seg_468" n="HIAT:u" s="T132">
                  <nts id="Seg_469" n="HIAT:ip">"</nts>
                  <ts e="T133" id="Seg_471" n="HIAT:w" s="T132">A</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_474" n="HIAT:w" s="T133">tănzi</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_477" n="HIAT:w" s="T134">pušaj</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_480" n="HIAT:w" s="T135">kudaj</ts>
                  <nts id="Seg_481" n="HIAT:ip">!</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T139" id="Seg_483" n="sc" s="T137">
               <ts e="T139" id="Seg_485" n="HIAT:u" s="T137">
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_488" n="HIAT:w" s="T137">Küʔ=</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_492" n="HIAT:w" s="T138">Küʔ</ts>
                  <nts id="Seg_493" n="HIAT:ip">"</nts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_496" n="sc" s="T140">
               <ts e="T141" id="Seg_498" n="HIAT:u" s="T140">
                  <nts id="Seg_499" n="HIAT:ip">(</nts>
                  <nts id="Seg_500" n="HIAT:ip">(</nts>
                  <ats e="T141" id="Seg_501" n="HIAT:non-pho" s="T140">BRK</ats>
                  <nts id="Seg_502" n="HIAT:ip">)</nts>
                  <nts id="Seg_503" n="HIAT:ip">)</nts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T149" id="Seg_506" n="sc" s="T142">
               <ts e="T149" id="Seg_508" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_510" n="HIAT:w" s="T142">Dĭgəttə</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_513" n="HIAT:w" s="T143">dĭ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_515" n="HIAT:ip">(</nts>
                  <ts e="T145" id="Seg_517" n="HIAT:w" s="T144">malubi</ts>
                  <nts id="Seg_518" n="HIAT:ip">)</nts>
                  <nts id="Seg_519" n="HIAT:ip">,</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_522" n="HIAT:w" s="T145">a</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_525" n="HIAT:w" s="T146">măn</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_528" n="HIAT:w" s="T147">maʔnʼi</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1179" id="Seg_531" n="HIAT:w" s="T148">kalla</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_534" n="HIAT:w" s="T1179">dʼürbiem</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_537" n="sc" s="T150">
               <ts e="T151" id="Seg_539" n="HIAT:u" s="T150">
                  <nts id="Seg_540" n="HIAT:ip">(</nts>
                  <nts id="Seg_541" n="HIAT:ip">(</nts>
                  <ats e="T151" id="Seg_542" n="HIAT:non-pho" s="T150">BRK</ats>
                  <nts id="Seg_543" n="HIAT:ip">)</nts>
                  <nts id="Seg_544" n="HIAT:ip">)</nts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_547" n="sc" s="T152">
               <ts e="T155" id="Seg_549" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_551" n="HIAT:w" s="T152">Măn</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_554" n="HIAT:w" s="T153">Kazan</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_557" n="HIAT:w" s="T1183">turanə</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_560" n="HIAT:w" s="T154">kambiam</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_563" n="sc" s="T156">
               <ts e="T165" id="Seg_565" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_567" n="HIAT:w" s="T156">A</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_570" n="HIAT:w" s="T157">dĭ</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_573" n="HIAT:w" s="T158">nükə</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_576" n="HIAT:w" s="T159">kambi</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_580" n="HIAT:w" s="T160">dĭn</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_583" n="HIAT:w" s="T161">tondə</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_586" n="HIAT:w" s="T162">dĭn</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_589" n="HIAT:w" s="T163">ne</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_592" n="HIAT:w" s="T164">amnolaʔbə</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_595" n="sc" s="T166">
               <ts e="T170" id="Seg_597" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_599" n="HIAT:w" s="T166">Šobi</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_601" n="HIAT:ip">(</nts>
                  <ts e="T168" id="Seg_603" n="HIAT:w" s="T167">dam</ts>
                  <nts id="Seg_604" n="HIAT:ip">)</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_607" n="HIAT:w" s="T168">da</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_610" n="HIAT:w" s="T169">kalla</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_613" n="HIAT:w" s="T1180">dʼürbi</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T178" id="Seg_616" n="sc" s="T171">
               <ts e="T178" id="Seg_618" n="HIAT:u" s="T171">
                  <nts id="Seg_619" n="HIAT:ip">"</nts>
                  <ts e="T172" id="Seg_621" n="HIAT:w" s="T171">Măn</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_624" n="HIAT:w" s="T172">külam</ts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_628" n="HIAT:w" s="T173">a</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_631" n="HIAT:w" s="T174">šində</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_634" n="HIAT:w" s="T175">măna</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_636" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_638" n="HIAT:w" s="T176">bĭzdl-</ts>
                  <nts id="Seg_639" n="HIAT:ip">)</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_642" n="HIAT:w" s="T177">bĭzəjdləj</ts>
                  <nts id="Seg_643" n="HIAT:ip">?</nts>
                  <nts id="Seg_644" n="HIAT:ip">"</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_646" n="sc" s="T179">
               <ts e="T183" id="Seg_648" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_650" n="HIAT:w" s="T179">Dĭzeŋ</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_653" n="HIAT:w" s="T180">bar</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_656" n="HIAT:w" s="T181">kaknarbiʔi</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_659" n="HIAT:w" s="T182">tăŋ</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_662" n="sc" s="T184">
               <ts e="T189" id="Seg_664" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_666" n="HIAT:w" s="T184">Dĭgəttə</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_669" n="HIAT:w" s="T185">măn</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_672" n="HIAT:w" s="T186">sobiam</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_676" n="HIAT:w" s="T187">kambiam</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_679" n="HIAT:w" s="T188">dĭʔnə</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_683" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_685" n="HIAT:w" s="T189">Tăn</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_688" n="HIAT:w" s="T190">kalla</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_691" n="HIAT:w" s="T1182">dʼürbiem</ts>
                  <nts id="Seg_692" n="HIAT:ip">,</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_695" n="HIAT:w" s="T191">a</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_698" n="HIAT:w" s="T192">măn</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_701" n="HIAT:w" s="T193">küllambiam</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_704" n="HIAT:w" s="T194">i</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_707" n="HIAT:w" s="T195">kannambiam</ts>
                  <nts id="Seg_708" n="HIAT:ip">.</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T202" id="Seg_710" n="sc" s="T197">
               <ts e="T202" id="Seg_712" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_714" n="HIAT:w" s="T197">A</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_717" n="HIAT:w" s="T198">măn</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_720" n="HIAT:w" s="T199">mămbiam:</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_722" n="HIAT:ip">"</nts>
                  <ts e="T201" id="Seg_724" n="HIAT:w" s="T200">Nu</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_728" n="HIAT:w" s="T201">kănnaʔ</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_731" n="sc" s="T203">
               <ts e="T208" id="Seg_733" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_735" n="HIAT:w" s="T203">Ĭmbi</ts>
                  <nts id="Seg_736" n="HIAT:ip">,</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_738" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_740" n="HIAT:w" s="T204">i</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_743" n="HIAT:w" s="T205">kl-</ts>
                  <nts id="Seg_744" n="HIAT:ip">)</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_747" n="HIAT:w" s="T206">i</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_750" n="HIAT:w" s="T207">küʔ</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_754" n="HIAT:u" s="T208">
                  <nts id="Seg_755" n="HIAT:ip">(</nts>
                  <ts e="T209" id="Seg_757" n="HIAT:w" s="T208">Mɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_760" n="HIAT:w" s="T209">bɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip">)</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_764" n="HIAT:w" s="T210">Măn</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_767" n="HIAT:w" s="T211">bü</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_770" n="HIAT:w" s="T212">ejümləm</ts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T218" id="Seg_773" n="sc" s="T214">
               <ts e="T218" id="Seg_775" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_777" n="HIAT:w" s="T214">I</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_780" n="HIAT:w" s="T215">tănan</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_782" n="HIAT:ip">(</nts>
                  <ts e="T217" id="Seg_784" n="HIAT:w" s="T216">kămnal-</ts>
                  <nts id="Seg_785" n="HIAT:ip">)</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_788" n="HIAT:w" s="T217">kămlal</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_791" n="sc" s="T219">
               <ts e="T227" id="Seg_793" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_795" n="HIAT:w" s="T219">Dĭgəttə</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_798" n="HIAT:w" s="T220">bĭzəlim</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_802" n="HIAT:w" s="T221">i</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_804" n="HIAT:ip">(</nts>
                  <ts e="T223" id="Seg_806" n="HIAT:w" s="T222">dʼunə</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_809" n="HIAT:w" s="T223">tĭl-</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_812" n="HIAT:w" s="T224">tĭl-</ts>
                  <nts id="Seg_813" n="HIAT:ip">)</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_816" n="HIAT:w" s="T225">dʼünə</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_819" n="HIAT:w" s="T226">enləbim</ts>
                  <nts id="Seg_820" n="HIAT:ip">.</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T229" id="Seg_822" n="sc" s="T228">
               <ts e="T229" id="Seg_824" n="HIAT:u" s="T228">
                  <nts id="Seg_825" n="HIAT:ip">(</nts>
                  <nts id="Seg_826" n="HIAT:ip">(</nts>
                  <ats e="T229" id="Seg_827" n="HIAT:non-pho" s="T228">BRK</ats>
                  <nts id="Seg_828" n="HIAT:ip">)</nts>
                  <nts id="Seg_829" n="HIAT:ip">)</nts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T237" id="Seg_832" n="sc" s="T230">
               <ts e="T237" id="Seg_834" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_836" n="HIAT:w" s="T230">Как</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_839" n="HIAT:w" s="T231">я</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_842" n="HIAT:w" s="T232">говорила</ts>
                  <nts id="Seg_843" n="HIAT:ip">,</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_846" n="HIAT:w" s="T233">погоди</ts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_850" n="HIAT:w" s="T234">как</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_853" n="HIAT:w" s="T235">я</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_856" n="HIAT:w" s="T236">начала</ts>
                  <nts id="Seg_857" n="HIAT:ip">?</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T239" id="Seg_859" n="sc" s="T238">
               <ts e="T239" id="Seg_861" n="HIAT:u" s="T238">
                  <nts id="Seg_862" n="HIAT:ip">(</nts>
                  <nts id="Seg_863" n="HIAT:ip">(</nts>
                  <ats e="T239" id="Seg_864" n="HIAT:non-pho" s="T238">BRK</ats>
                  <nts id="Seg_865" n="HIAT:ip">)</nts>
                  <nts id="Seg_866" n="HIAT:ip">)</nts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_869" n="sc" s="T240">
               <ts e="T248" id="Seg_871" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_873" n="HIAT:w" s="T240">Dĭ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_876" n="HIAT:w" s="T241">nüke</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_879" n="HIAT:w" s="T242">bar</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_882" n="HIAT:w" s="T243">măna:</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_884" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_886" n="HIAT:w" s="T244">Nendeʔ</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_889" n="HIAT:w" s="T245">moltʼa</ts>
                  <nts id="Seg_890" n="HIAT:ip">,</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_893" n="HIAT:w" s="T246">bĭzaʔ</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_896" n="HIAT:w" s="T247">măna</ts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_899" n="sc" s="T249">
               <ts e="T257" id="Seg_901" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_903" n="HIAT:w" s="T249">Külalləm</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_906" n="HIAT:w" s="T250">dăk</ts>
                  <nts id="Seg_907" n="HIAT:ip">,</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_910" n="HIAT:w" s="T251">dĭgəttə</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_913" n="HIAT:w" s="T252">kadəldədə</ts>
                  <nts id="Seg_914" n="HIAT:ip">,</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_917" n="HIAT:w" s="T254">udazaŋdə</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_920" n="HIAT:w" s="T256">bĭzəlil</ts>
                  <nts id="Seg_921" n="HIAT:ip">"</nts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T263" id="Seg_924" n="sc" s="T258">
               <ts e="T263" id="Seg_926" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_928" n="HIAT:w" s="T258">Măn</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_931" n="HIAT:w" s="T259">dĭgəttə</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_934" n="HIAT:w" s="T260">nendəbiem</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_936" n="HIAT:ip">(</nts>
                  <ts e="T262" id="Seg_938" n="HIAT:w" s="T261">voldʼa-</ts>
                  <nts id="Seg_939" n="HIAT:ip">)</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_942" n="HIAT:w" s="T262">moltʼa</ts>
                  <nts id="Seg_943" n="HIAT:ip">.</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T277" id="Seg_945" n="sc" s="T264">
               <ts e="T270" id="Seg_947" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_949" n="HIAT:w" s="T264">Dĭgəttə</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_952" n="HIAT:w" s="T265">sankaʔizi</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_955" n="HIAT:w" s="T266">embiem</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_958" n="HIAT:w" s="T267">i</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_961" n="HIAT:w" s="T268">u</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_964" n="HIAT:w" s="T269">kunnambiam</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_968" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_970" n="HIAT:w" s="T270">Bĭzəbiam</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_973" n="HIAT:w" s="T271">dĭm</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_976" n="HIAT:w" s="T272">da</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_979" n="HIAT:w" s="T273">bazo</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_982" n="HIAT:w" s="T274">maʔnə</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_985" n="HIAT:w" s="T275">deʔpiem</ts>
                  <nts id="Seg_986" n="HIAT:ip">,</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_989" n="HIAT:w" s="T276">sankaʔizi</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T284" id="Seg_992" n="sc" s="T278">
               <ts e="T284" id="Seg_994" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_996" n="HIAT:w" s="T278">Embiem</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_999" n="HIAT:w" s="T279">krăvatʼtə</ts>
                  <nts id="Seg_1000" n="HIAT:ip">,</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1003" n="HIAT:w" s="T280">măndəm:</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1005" n="HIAT:ip">"</nts>
                  <ts e="T282" id="Seg_1007" n="HIAT:w" s="T281">Nu</ts>
                  <nts id="Seg_1008" n="HIAT:ip">,</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1011" n="HIAT:w" s="T282">tüj</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1014" n="HIAT:w" s="T283">küʔ</ts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_1017" n="sc" s="T285">
               <ts e="T292" id="Seg_1019" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1021" n="HIAT:w" s="T285">Kadəldə</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1024" n="HIAT:w" s="T286">da</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1026" n="HIAT:ip">(</nts>
                  <ts e="T288" id="Seg_1028" n="HIAT:w" s="T287">udazaŋzi</ts>
                  <nts id="Seg_1029" n="HIAT:ip">)</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1032" n="HIAT:w" s="T288">băzəlim</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1036" n="HIAT:w" s="T289">i</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1039" n="HIAT:w" s="T290">jakše</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1042" n="HIAT:w" s="T291">moləj</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T303" id="Seg_1045" n="sc" s="T293">
               <ts e="T303" id="Seg_1047" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1049" n="HIAT:w" s="T293">Nu</ts>
                  <nts id="Seg_1050" n="HIAT:ip">,</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1053" n="HIAT:w" s="T294">dĭgəttə</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1056" n="HIAT:w" s="T295">moltʼagən</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1059" n="HIAT:w" s="T296">bĭzəbiam</ts>
                  <nts id="Seg_1060" n="HIAT:ip">,</nts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1063" n="HIAT:w" s="T297">ugandə</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1065" n="HIAT:ip">(</nts>
                  <ts e="T299" id="Seg_1067" n="HIAT:w" s="T298">bal-</ts>
                  <nts id="Seg_1068" n="HIAT:ip">)</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1071" n="HIAT:w" s="T299">balgaš</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1074" n="HIAT:w" s="T300">iʔgö</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1077" n="HIAT:w" s="T301">ibi</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1080" n="HIAT:w" s="T302">ulundə</ts>
                  <nts id="Seg_1081" n="HIAT:ip">.</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_1083" n="sc" s="T304">
               <ts e="T309" id="Seg_1085" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1087" n="HIAT:w" s="T304">Măn</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1090" n="HIAT:w" s="T305">mălliam:</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1092" n="HIAT:ip">"</nts>
                  <ts e="T307" id="Seg_1094" n="HIAT:w" s="T306">Išo</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1097" n="HIAT:w" s="T307">eneʔ</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1100" n="HIAT:w" s="T308">bü</ts>
                  <nts id="Seg_1101" n="HIAT:ip">"</nts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T313" id="Seg_1104" n="sc" s="T310">
               <ts e="T313" id="Seg_1106" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1108" n="HIAT:w" s="T310">Döm</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1110" n="HIAT:ip">(</nts>
                  <ts e="T312" id="Seg_1112" n="HIAT:w" s="T311">kămne-</ts>
                  <nts id="Seg_1113" n="HIAT:ip">)</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1116" n="HIAT:w" s="T312">kămnit</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T315" id="Seg_1119" n="sc" s="T314">
               <ts e="T315" id="Seg_1121" n="HIAT:u" s="T314">
                  <nts id="Seg_1122" n="HIAT:ip">(</nts>
                  <nts id="Seg_1123" n="HIAT:ip">(</nts>
                  <ats e="T315" id="Seg_1124" n="HIAT:non-pho" s="T314">BRK</ats>
                  <nts id="Seg_1125" n="HIAT:ip">)</nts>
                  <nts id="Seg_1126" n="HIAT:ip">)</nts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T322" id="Seg_1129" n="sc" s="T316">
               <ts e="T322" id="Seg_1131" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1133" n="HIAT:w" s="T316">Onʼiʔ</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1135" n="HIAT:ip">(</nts>
                  <ts e="T318" id="Seg_1137" n="HIAT:w" s="T317">ini-</ts>
                  <nts id="Seg_1138" n="HIAT:ip">)</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1141" n="HIAT:w" s="T318">ine</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1144" n="HIAT:w" s="T319">mĭbiem</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1147" n="HIAT:w" s="T320">tĭbin</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1150" n="HIAT:w" s="T321">kagattə</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T326" id="Seg_1153" n="sc" s="T323">
               <ts e="T326" id="Seg_1155" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1157" n="HIAT:w" s="T323">Šide</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1160" n="HIAT:w" s="T324">pʼe</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1163" n="HIAT:w" s="T325">ibi</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T331" id="Seg_1166" n="sc" s="T327">
               <ts e="T331" id="Seg_1168" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1170" n="HIAT:w" s="T327">Bazo</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1173" n="HIAT:w" s="T328">ine</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1176" n="HIAT:w" s="T329">mĭbiem</ts>
                  <nts id="Seg_1177" n="HIAT:ip">,</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1180" n="HIAT:w" s="T330">kuzanə</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_1183" n="sc" s="T332">
               <ts e="T334" id="Seg_1185" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1187" n="HIAT:w" s="T332">Tĭbi</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1190" n="HIAT:w" s="T333">ibi</ts>
                  <nts id="Seg_1191" n="HIAT:ip">.</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T341" id="Seg_1193" n="sc" s="T335">
               <ts e="T337" id="Seg_1195" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1197" n="HIAT:w" s="T335">Naga</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1200" n="HIAT:w" s="T336">ine</ts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1204" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1206" n="HIAT:w" s="T337">Tože</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1209" n="HIAT:w" s="T338">šide</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1212" n="HIAT:w" s="T339">pʼe</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1215" n="HIAT:w" s="T340">ibi</ts>
                  <nts id="Seg_1216" n="HIAT:ip">.</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T349" id="Seg_1218" n="sc" s="T342">
               <ts e="T344" id="Seg_1220" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1222" n="HIAT:w" s="T342">Mĭbiem</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1225" n="HIAT:w" s="T343">ine</ts>
                  <nts id="Seg_1226" n="HIAT:ip">.</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1229" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1231" n="HIAT:w" s="T344">Dĭgəttə</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1234" n="HIAT:w" s="T345">dĭ</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1237" n="HIAT:w" s="T346">sestrandə</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1240" n="HIAT:w" s="T347">ular</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1243" n="HIAT:w" s="T348">mĭbiem</ts>
                  <nts id="Seg_1244" n="HIAT:ip">.</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T352" id="Seg_1246" n="sc" s="T350">
               <ts e="T352" id="Seg_1248" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1250" n="HIAT:w" s="T350">Părga</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1253" n="HIAT:w" s="T351">mĭbiem</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_1256" n="sc" s="T353">
               <ts e="T358" id="Seg_1258" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1260" n="HIAT:w" s="T353">Dĭgəttə</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1263" n="HIAT:w" s="T354">plemʼannican</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1266" n="HIAT:w" s="T355">koʔbdonə</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1269" n="HIAT:w" s="T356">mĭbiem</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1272" n="HIAT:w" s="T357">buga</ts>
                  <nts id="Seg_1273" n="HIAT:ip">.</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T362" id="Seg_1275" n="sc" s="T359">
               <ts e="T362" id="Seg_1277" n="HIAT:u" s="T359">
                  <nts id="Seg_1278" n="HIAT:ip">(</nts>
                  <ts e="T360" id="Seg_1280" n="HIAT:w" s="T359">Pʼel=</ts>
                  <nts id="Seg_1281" n="HIAT:ip">)</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1284" n="HIAT:w" s="T360">Pʼel</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1287" n="HIAT:w" s="T361">kö</ts>
                  <nts id="Seg_1288" n="HIAT:ip">.</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T372" id="Seg_1290" n="sc" s="T363">
               <ts e="T372" id="Seg_1292" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1294" n="HIAT:w" s="T363">Dĭgəttə</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1296" n="HIAT:ip">(</nts>
                  <ts e="T365" id="Seg_1298" n="HIAT:w" s="T364">m-</ts>
                  <nts id="Seg_1299" n="HIAT:ip">)</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1302" n="HIAT:w" s="T365">sestranə</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1305" n="HIAT:w" s="T366">nʼit</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1308" n="HIAT:w" s="T367">mĭbiem</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1311" n="HIAT:w" s="T368">tüžöj</ts>
                  <nts id="Seg_1312" n="HIAT:ip">,</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1315" n="HIAT:w" s="T369">i</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1318" n="HIAT:w" s="T370">ular</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1321" n="HIAT:w" s="T371">mĭbiem</ts>
                  <nts id="Seg_1322" n="HIAT:ip">.</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_1324" n="sc" s="T373">
               <ts e="T374" id="Seg_1326" n="HIAT:u" s="T373">
                  <nts id="Seg_1327" n="HIAT:ip">(</nts>
                  <nts id="Seg_1328" n="HIAT:ip">(</nts>
                  <ats e="T374" id="Seg_1329" n="HIAT:non-pho" s="T373">BRK</ats>
                  <nts id="Seg_1330" n="HIAT:ip">)</nts>
                  <nts id="Seg_1331" n="HIAT:ip">)</nts>
                  <nts id="Seg_1332" n="HIAT:ip">.</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T382" id="Seg_1334" n="sc" s="T375">
               <ts e="T382" id="Seg_1336" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1338" n="HIAT:w" s="T375">Dĭgəttə</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1341" n="HIAT:w" s="T376">onʼiʔ</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1344" n="HIAT:w" s="T377">nenə</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1347" n="HIAT:w" s="T378">bazo</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1349" n="HIAT:ip">(</nts>
                  <ts e="T380" id="Seg_1351" n="HIAT:w" s="T379">bu-</ts>
                  <nts id="Seg_1352" n="HIAT:ip">)</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1355" n="HIAT:w" s="T380">büzo</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1358" n="HIAT:w" s="T381">mĭbiem</ts>
                  <nts id="Seg_1359" n="HIAT:ip">.</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T391" id="Seg_1361" n="sc" s="T383">
               <ts e="T391" id="Seg_1363" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1365" n="HIAT:w" s="T383">Jil</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1368" n="HIAT:w" s="T384">šoləʔi:</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1370" n="HIAT:ip">"</nts>
                  <ts e="T386" id="Seg_1372" n="HIAT:w" s="T385">Deʔ</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1375" n="HIAT:w" s="T386">aktʼa</ts>
                  <nts id="Seg_1376" n="HIAT:ip">,</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1378" n="HIAT:ip">(</nts>
                  <ts e="T388" id="Seg_1380" n="HIAT:w" s="T387">m-</ts>
                  <nts id="Seg_1381" n="HIAT:ip">)</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1384" n="HIAT:w" s="T388">miʔ</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1387" n="HIAT:w" s="T389">tănan</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1390" n="HIAT:w" s="T390">mĭlibeʔ</ts>
                  <nts id="Seg_1391" n="HIAT:ip">"</nts>
                  <nts id="Seg_1392" n="HIAT:ip">.</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T401" id="Seg_1394" n="sc" s="T392">
               <ts e="T397" id="Seg_1396" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1398" n="HIAT:w" s="T392">A</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1400" n="HIAT:ip">(</nts>
                  <ts e="T394" id="Seg_1402" n="HIAT:w" s="T393">boste-</ts>
                  <nts id="Seg_1403" n="HIAT:ip">)</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1406" n="HIAT:w" s="T394">bostəzaŋdə</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1409" n="HIAT:w" s="T395">ej</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1412" n="HIAT:w" s="T396">mĭleʔi</ts>
                  <nts id="Seg_1413" n="HIAT:ip">.</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1416" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1418" n="HIAT:w" s="T397">I</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1421" n="HIAT:w" s="T398">dĭgəttə</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1423" n="HIAT:ip">(</nts>
                  <ts e="T400" id="Seg_1425" n="HIAT:w" s="T399">maluʔle</ts>
                  <nts id="Seg_1426" n="HIAT:ip">)</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1429" n="HIAT:w" s="T400">aktʼa</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T403" id="Seg_1432" n="sc" s="T402">
               <ts e="T403" id="Seg_1434" n="HIAT:u" s="T402">
                  <nts id="Seg_1435" n="HIAT:ip">(</nts>
                  <nts id="Seg_1436" n="HIAT:ip">(</nts>
                  <ats e="T403" id="Seg_1437" n="HIAT:non-pho" s="T402">BRK</ats>
                  <nts id="Seg_1438" n="HIAT:ip">)</nts>
                  <nts id="Seg_1439" n="HIAT:ip">)</nts>
                  <nts id="Seg_1440" n="HIAT:ip">.</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T405" id="Seg_1442" n="sc" s="T404">
               <ts e="T405" id="Seg_1444" n="HIAT:u" s="T404">
                  <nts id="Seg_1445" n="HIAT:ip">(</nts>
                  <nts id="Seg_1446" n="HIAT:ip">(</nts>
                  <ats e="T405" id="Seg_1447" n="HIAT:non-pho" s="T404">BRK</ats>
                  <nts id="Seg_1448" n="HIAT:ip">)</nts>
                  <nts id="Seg_1449" n="HIAT:ip">)</nts>
                  <nts id="Seg_1450" n="HIAT:ip">.</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1452" n="sc" s="T406">
               <ts e="T417" id="Seg_1454" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1456" n="HIAT:w" s="T406">Măn</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1459" n="HIAT:w" s="T407">tüj</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1462" n="HIAT:w" s="T408">en</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1465" n="HIAT:w" s="T409">ej</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1467" n="HIAT:ip">(</nts>
                  <ts e="T411" id="Seg_1469" n="HIAT:w" s="T410">tone-</ts>
                  <nts id="Seg_1470" n="HIAT:ip">)</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1473" n="HIAT:w" s="T411">tenolam</ts>
                  <nts id="Seg_1474" n="HIAT:ip">,</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412.tx.1" id="Seg_1477" n="HIAT:w" s="T412">a</ts>
                  <nts id="Seg_1478" n="HIAT:ip">_</nts>
                  <ts e="T413" id="Seg_1480" n="HIAT:w" s="T412.tx.1">to</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1483" n="HIAT:w" s="T413">inen</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1486" n="HIAT:w" s="T414">ulut</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1489" n="HIAT:w" s="T415">nada</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1492" n="HIAT:w" s="T416">urgo</ts>
                  <nts id="Seg_1493" n="HIAT:ip">.</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T419" id="Seg_1495" n="sc" s="T418">
               <ts e="T419" id="Seg_1497" n="HIAT:u" s="T418">
                  <nts id="Seg_1498" n="HIAT:ip">(</nts>
                  <nts id="Seg_1499" n="HIAT:ip">(</nts>
                  <ats e="T419" id="Seg_1500" n="HIAT:non-pho" s="T418">BRK</ats>
                  <nts id="Seg_1501" n="HIAT:ip">)</nts>
                  <nts id="Seg_1502" n="HIAT:ip">)</nts>
                  <nts id="Seg_1503" n="HIAT:ip">.</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T422" id="Seg_1505" n="sc" s="T420">
               <ts e="T422" id="Seg_1507" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1509" n="HIAT:w" s="T420">Šiʔ</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1512" n="HIAT:w" s="T421">tenolaʔbə</ts>
                  <nts id="Seg_1513" n="HIAT:ip">.</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_1515" n="sc" s="T423">
               <ts e="T424" id="Seg_1517" n="HIAT:u" s="T423">
                  <nts id="Seg_1518" n="HIAT:ip">(</nts>
                  <nts id="Seg_1519" n="HIAT:ip">(</nts>
                  <ats e="T424" id="Seg_1520" n="HIAT:non-pho" s="T423">BRK</ats>
                  <nts id="Seg_1521" n="HIAT:ip">)</nts>
                  <nts id="Seg_1522" n="HIAT:ip">)</nts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T426" id="Seg_1525" n="sc" s="T425">
               <ts e="T426" id="Seg_1527" n="HIAT:u" s="T425">
                  <nts id="Seg_1528" n="HIAT:ip">(</nts>
                  <nts id="Seg_1529" n="HIAT:ip">(</nts>
                  <ats e="T426" id="Seg_1530" n="HIAT:non-pho" s="T425">BRK</ats>
                  <nts id="Seg_1531" n="HIAT:ip">)</nts>
                  <nts id="Seg_1532" n="HIAT:ip">)</nts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T434" id="Seg_1535" n="sc" s="T427">
               <ts e="T434" id="Seg_1537" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1539" n="HIAT:w" s="T427">Tüjö</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1542" n="HIAT:w" s="T428">kalam</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1545" n="HIAT:w" s="T429">tüžöjʔi</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1547" n="HIAT:ip">(</nts>
                  <ts e="T431" id="Seg_1549" n="HIAT:w" s="T430">šendedndə</ts>
                  <nts id="Seg_1550" n="HIAT:ip">)</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1553" n="HIAT:w" s="T431">sürerlam</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1556" n="HIAT:w" s="T432">i</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1559" n="HIAT:w" s="T433">noʔmeləm</ts>
                  <nts id="Seg_1560" n="HIAT:ip">.</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T440" id="Seg_1562" n="sc" s="T435">
               <ts e="T440" id="Seg_1564" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1566" n="HIAT:w" s="T435">Kambiam</ts>
                  <nts id="Seg_1567" n="HIAT:ip">,</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1570" n="HIAT:w" s="T436">a</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1573" n="HIAT:w" s="T437">dĭzeŋ</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1576" n="HIAT:w" s="T438">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1579" n="HIAT:w" s="T439">gĭbərdə</ts>
                  <nts id="Seg_1580" n="HIAT:ip">.</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_1582" n="sc" s="T441">
               <ts e="T442" id="Seg_1584" n="HIAT:u" s="T441">
                  <nts id="Seg_1585" n="HIAT:ip">(</nts>
                  <nts id="Seg_1586" n="HIAT:ip">(</nts>
                  <ats e="T442" id="Seg_1587" n="HIAT:non-pho" s="T441">BRK</ats>
                  <nts id="Seg_1588" n="HIAT:ip">)</nts>
                  <nts id="Seg_1589" n="HIAT:ip">)</nts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T447" id="Seg_1592" n="sc" s="T443">
               <ts e="T447" id="Seg_1594" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1596" n="HIAT:w" s="T443">Traktăr</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1599" n="HIAT:w" s="T444">bar</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1602" n="HIAT:w" s="T445">paʔi</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1605" n="HIAT:w" s="T446">kunnandəga</ts>
                  <nts id="Seg_1606" n="HIAT:ip">.</nts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T452" id="Seg_1608" n="sc" s="T448">
               <ts e="T452" id="Seg_1610" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1612" n="HIAT:w" s="T448">Ej</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1615" n="HIAT:w" s="T449">tĭmnem</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1619" n="HIAT:w" s="T450">kumen</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1621" n="HIAT:ip">(</nts>
                  <ts e="T452" id="Seg_1623" n="HIAT:w" s="T451">kunnandəga</ts>
                  <nts id="Seg_1624" n="HIAT:ip">)</nts>
                  <nts id="Seg_1625" n="HIAT:ip">.</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T454" id="Seg_1627" n="sc" s="T453">
               <ts e="T454" id="Seg_1629" n="HIAT:u" s="T453">
                  <nts id="Seg_1630" n="HIAT:ip">(</nts>
                  <nts id="Seg_1631" n="HIAT:ip">(</nts>
                  <ats e="T454" id="Seg_1632" n="HIAT:non-pho" s="T453">BRK</ats>
                  <nts id="Seg_1633" n="HIAT:ip">)</nts>
                  <nts id="Seg_1634" n="HIAT:ip">)</nts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T458" id="Seg_1637" n="sc" s="T455">
               <ts e="T458" id="Seg_1639" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1641" n="HIAT:w" s="T455">Ugandə</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1644" n="HIAT:w" s="T456">iʔgö</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1647" n="HIAT:w" s="T457">paʔi</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_1650" n="sc" s="T459">
               <ts e="T464" id="Seg_1652" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1654" n="HIAT:w" s="T459">Da</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1657" n="HIAT:w" s="T460">i</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1659" n="HIAT:ip">(</nts>
                  <ts e="T462" id="Seg_1661" n="HIAT:w" s="T461">nʼišpek=</ts>
                  <nts id="Seg_1662" n="HIAT:ip">)</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1665" n="HIAT:w" s="T462">nʼišpek</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1668" n="HIAT:w" s="T463">ugandə</ts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T466" id="Seg_1671" n="sc" s="T465">
               <ts e="T466" id="Seg_1673" n="HIAT:u" s="T465">
                  <nts id="Seg_1674" n="HIAT:ip">(</nts>
                  <nts id="Seg_1675" n="HIAT:ip">(</nts>
                  <ats e="T466" id="Seg_1676" n="HIAT:non-pho" s="T465">BRK</ats>
                  <nts id="Seg_1677" n="HIAT:ip">)</nts>
                  <nts id="Seg_1678" n="HIAT:ip">)</nts>
                  <nts id="Seg_1679" n="HIAT:ip">.</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T474" id="Seg_1681" n="sc" s="T467">
               <ts e="T474" id="Seg_1683" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1685" n="HIAT:w" s="T467">Iʔgö</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1687" n="HIAT:ip">(</nts>
                  <ts e="T469" id="Seg_1689" n="HIAT:w" s="T468">tn-</ts>
                  <nts id="Seg_1690" n="HIAT:ip">)</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1693" n="HIAT:w" s="T469">togonorzittə</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1696" n="HIAT:w" s="T470">kubiam</ts>
                  <nts id="Seg_1697" n="HIAT:ip">,</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1700" n="HIAT:w" s="T471">a</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1703" n="HIAT:w" s="T472">amga</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1706" n="HIAT:w" s="T473">kulaʔpiam</ts>
                  <nts id="Seg_1707" n="HIAT:ip">.</nts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T476" id="Seg_1709" n="sc" s="T475">
               <ts e="T476" id="Seg_1711" n="HIAT:u" s="T475">
                  <nts id="Seg_1712" n="HIAT:ip">(</nts>
                  <nts id="Seg_1713" n="HIAT:ip">(</nts>
                  <ats e="T476" id="Seg_1714" n="HIAT:non-pho" s="T475">BRK</ats>
                  <nts id="Seg_1715" n="HIAT:ip">)</nts>
                  <nts id="Seg_1716" n="HIAT:ip">)</nts>
                  <nts id="Seg_1717" n="HIAT:ip">.</nts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1719" n="sc" s="T477">
               <ts e="T480" id="Seg_1721" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1723" n="HIAT:w" s="T477">Koŋ</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1726" n="HIAT:w" s="T478">ugandə</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1729" n="HIAT:w" s="T479">kuvas</ts>
                  <nts id="Seg_1730" n="HIAT:ip">.</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_1732" n="sc" s="T481">
               <ts e="T487" id="Seg_1734" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1736" n="HIAT:w" s="T481">Bar</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1739" n="HIAT:w" s="T482">ĭmbi</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1742" n="HIAT:w" s="T483">măna</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1745" n="HIAT:w" s="T484">mĭmbi</ts>
                  <nts id="Seg_1746" n="HIAT:ip">,</nts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1749" n="HIAT:w" s="T485">bar</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1752" n="HIAT:w" s="T486">ĭmbi</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T489" id="Seg_1755" n="sc" s="T488">
               <ts e="T489" id="Seg_1757" n="HIAT:u" s="T488">
                  <nts id="Seg_1758" n="HIAT:ip">(</nts>
                  <nts id="Seg_1759" n="HIAT:ip">(</nts>
                  <ats e="T489" id="Seg_1760" n="HIAT:non-pho" s="T488">BRK</ats>
                  <nts id="Seg_1761" n="HIAT:ip">)</nts>
                  <nts id="Seg_1762" n="HIAT:ip">)</nts>
                  <nts id="Seg_1763" n="HIAT:ip">.</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1765" n="sc" s="T490">
               <ts e="T494" id="Seg_1767" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1769" n="HIAT:w" s="T490">Bar</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1772" n="HIAT:w" s="T491">măna</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1774" n="HIAT:ip">(</nts>
                  <ts e="T493" id="Seg_1776" n="HIAT:w" s="T492">pʼem-</ts>
                  <nts id="Seg_1777" n="HIAT:ip">)</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1780" n="HIAT:w" s="T493">pʼerbi</ts>
                  <nts id="Seg_1781" n="HIAT:ip">.</nts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T499" id="Seg_1783" n="sc" s="T495">
               <ts e="T496" id="Seg_1785" n="HIAT:u" s="T495">
                  <nts id="Seg_1786" n="HIAT:ip">(</nts>
                  <nts id="Seg_1787" n="HIAT:ip">(</nts>
                  <ats e="T496" id="Seg_1788" n="HIAT:non-pho" s="T495">BRK</ats>
                  <nts id="Seg_1789" n="HIAT:ip">)</nts>
                  <nts id="Seg_1790" n="HIAT:ip">)</nts>
                  <nts id="Seg_1791" n="HIAT:ip">.</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1794" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1796" n="HIAT:w" s="T496">Это</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1799" n="HIAT:w" s="T497">мало</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1802" n="HIAT:w" s="T498">чего-то</ts>
                  <nts id="Seg_1803" n="HIAT:ip">.</nts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T504" id="Seg_1805" n="sc" s="T500">
               <ts e="T504" id="Seg_1807" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1809" n="HIAT:w" s="T500">Moləj</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1812" n="HIAT:w" s="T501">dʼala</ts>
                  <nts id="Seg_1813" n="HIAT:ip">,</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1816" n="HIAT:w" s="T502">moləj</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1819" n="HIAT:w" s="T503">amorzittə</ts>
                  <nts id="Seg_1820" n="HIAT:ip">.</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T506" id="Seg_1822" n="sc" s="T505">
               <ts e="T506" id="Seg_1824" n="HIAT:u" s="T505">
                  <nts id="Seg_1825" n="HIAT:ip">(</nts>
                  <nts id="Seg_1826" n="HIAT:ip">(</nts>
                  <ats e="T506" id="Seg_1827" n="HIAT:non-pho" s="T505">BRK</ats>
                  <nts id="Seg_1828" n="HIAT:ip">)</nts>
                  <nts id="Seg_1829" n="HIAT:ip">)</nts>
                  <nts id="Seg_1830" n="HIAT:ip">.</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T514" id="Seg_1832" n="sc" s="T507">
               <ts e="T514" id="Seg_1834" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1836" n="HIAT:w" s="T507">Teinen</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1839" n="HIAT:w" s="T508">subbota</ts>
                  <nts id="Seg_1840" n="HIAT:ip">,</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1843" n="HIAT:w" s="T509">năda</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1846" n="HIAT:w" s="T510">moltʼa</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1849" n="HIAT:w" s="T511">nendəsʼtə</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1853" n="HIAT:w" s="T512">bü</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1856" n="HIAT:w" s="T513">tazirzittə</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T520" id="Seg_1859" n="sc" s="T515">
               <ts e="T520" id="Seg_1861" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1863" n="HIAT:w" s="T515">Dĭgəttə</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1866" n="HIAT:w" s="T516">nendəlem</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1868" n="HIAT:ip">(</nts>
                  <ts e="T518" id="Seg_1870" n="HIAT:w" s="T517">mul-</ts>
                  <nts id="Seg_1871" n="HIAT:ip">)</nts>
                  <nts id="Seg_1872" n="HIAT:ip">,</nts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1875" n="HIAT:w" s="T518">băzəjdəsʼtə</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1878" n="HIAT:w" s="T519">nada</ts>
                  <nts id="Seg_1879" n="HIAT:ip">.</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T526" id="Seg_1881" n="sc" s="T521">
               <ts e="T526" id="Seg_1883" n="HIAT:u" s="T521">
                  <ts e="T521.tx.1" id="Seg_1885" n="HIAT:w" s="T521">A</ts>
                  <nts id="Seg_1886" n="HIAT:ip">_</nts>
                  <ts e="T522" id="Seg_1888" n="HIAT:w" s="T521.tx.1">to</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1891" n="HIAT:w" s="T522">selej</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1894" n="HIAT:w" s="T523">nedʼelʼa</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1897" n="HIAT:w" s="T524">ej</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1899" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_1901" n="HIAT:w" s="T525">băzəldəbiam</ts>
                  <nts id="Seg_1902" n="HIAT:ip">)</nts>
                  <nts id="Seg_1903" n="HIAT:ip">.</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T528" id="Seg_1905" n="sc" s="T527">
               <ts e="T528" id="Seg_1907" n="HIAT:u" s="T527">
                  <nts id="Seg_1908" n="HIAT:ip">(</nts>
                  <nts id="Seg_1909" n="HIAT:ip">(</nts>
                  <ats e="T528" id="Seg_1910" n="HIAT:non-pho" s="T527">BRK</ats>
                  <nts id="Seg_1911" n="HIAT:ip">)</nts>
                  <nts id="Seg_1912" n="HIAT:ip">)</nts>
                  <nts id="Seg_1913" n="HIAT:ip">.</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T533" id="Seg_1915" n="sc" s="T529">
               <ts e="T533" id="Seg_1917" n="HIAT:u" s="T529">
                  <nts id="Seg_1918" n="HIAT:ip">(</nts>
                  <ts e="T530" id="Seg_1920" n="HIAT:w" s="T529">Maʔənnə</ts>
                  <nts id="Seg_1921" n="HIAT:ip">)</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1924" n="HIAT:w" s="T530">sazən</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1927" n="HIAT:w" s="T531">edeʔliem</ts>
                  <nts id="Seg_1928" n="HIAT:ip">,</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1931" n="HIAT:w" s="T532">edəʔliem</ts>
                  <nts id="Seg_1932" n="HIAT:ip">.</nts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T540" id="Seg_1934" n="sc" s="T534">
               <ts e="T537" id="Seg_1936" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_1938" n="HIAT:w" s="T534">Üge</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1940" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_1942" n="HIAT:w" s="T535">nada</ts>
                  <nts id="Seg_1943" n="HIAT:ip">)</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1946" n="HIAT:w" s="T536">naga</ts>
                  <nts id="Seg_1947" n="HIAT:ip">.</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_1950" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_1952" n="HIAT:w" s="T537">Nem</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1955" n="HIAT:w" s="T538">ej</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1958" n="HIAT:w" s="T539">pʼaŋdlia</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T545" id="Seg_1961" n="sc" s="T541">
               <ts e="T545" id="Seg_1963" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1965" n="HIAT:w" s="T541">I</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1968" n="HIAT:w" s="T542">sazən</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1971" n="HIAT:w" s="T543">ej</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1974" n="HIAT:w" s="T544">šolia</ts>
                  <nts id="Seg_1975" n="HIAT:ip">.</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T551" id="Seg_1977" n="sc" s="T546">
               <ts e="T547" id="Seg_1979" n="HIAT:u" s="T546">
                  <nts id="Seg_1980" n="HIAT:ip">(</nts>
                  <nts id="Seg_1981" n="HIAT:ip">(</nts>
                  <ats e="T547" id="Seg_1982" n="HIAT:non-pho" s="T546">BRK</ats>
                  <nts id="Seg_1983" n="HIAT:ip">)</nts>
                  <nts id="Seg_1984" n="HIAT:ip">)</nts>
                  <nts id="Seg_1985" n="HIAT:ip">.</nts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_1988" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_1990" n="HIAT:w" s="T547">Teinen</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1993" n="HIAT:w" s="T548">nüdʼin</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1996" n="HIAT:w" s="T549">dʼodunʼi</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1999" n="HIAT:w" s="T550">kubiem</ts>
                  <nts id="Seg_2000" n="HIAT:ip">.</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T556" id="Seg_2002" n="sc" s="T552">
               <ts e="T556" id="Seg_2004" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2006" n="HIAT:w" s="T552">Dʼabrolaʔpiem</ts>
                  <nts id="Seg_2007" n="HIAT:ip">,</nts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2010" n="HIAT:w" s="T553">teinen</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2013" n="HIAT:w" s="T554">sazən</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2016" n="HIAT:w" s="T555">šoləj</ts>
                  <nts id="Seg_2017" n="HIAT:ip">.</nts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T558" id="Seg_2019" n="sc" s="T557">
               <ts e="T558" id="Seg_2021" n="HIAT:u" s="T557">
                  <nts id="Seg_2022" n="HIAT:ip">(</nts>
                  <nts id="Seg_2023" n="HIAT:ip">(</nts>
                  <ats e="T558" id="Seg_2024" n="HIAT:non-pho" s="T557">BRK</ats>
                  <nts id="Seg_2025" n="HIAT:ip">)</nts>
                  <nts id="Seg_2026" n="HIAT:ip">)</nts>
                  <nts id="Seg_2027" n="HIAT:ip">.</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T564" id="Seg_2029" n="sc" s="T559">
               <ts e="T564" id="Seg_2031" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2033" n="HIAT:w" s="T559">Măn</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2036" n="HIAT:w" s="T560">mĭmbiem</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2039" n="HIAT:w" s="T561">Krasnojaskăjdə</ts>
                  <nts id="Seg_2040" n="HIAT:ip">,</nts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_2043" n="HIAT:w" s="T562">munəʔi</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2045" n="HIAT:ip">(</nts>
                  <ts e="T563" id="Seg_2047" n="HIAT:w" s="T1186">-ʔi-</ts>
                  <nts id="Seg_2048" n="HIAT:ip">)</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2051" n="HIAT:w" s="T563">kumbiam</ts>
                  <nts id="Seg_2052" n="HIAT:ip">.</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T568" id="Seg_2054" n="sc" s="T565">
               <ts e="T568" id="Seg_2056" n="HIAT:u" s="T565">
                  <nts id="Seg_2057" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_2059" n="HIAT:w" s="T565">S-</ts>
                  <nts id="Seg_2060" n="HIAT:ip">)</nts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2063" n="HIAT:w" s="T566">Sto</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2066" n="HIAT:w" s="T567">munəj</ts>
                  <nts id="Seg_2067" n="HIAT:ip">.</nts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T573" id="Seg_2069" n="sc" s="T569">
               <ts e="T573" id="Seg_2071" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_2073" n="HIAT:w" s="T569">Dĭgəttə</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2076" n="HIAT:w" s="T570">kagam</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2079" n="HIAT:w" s="T571">mănzi</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2082" n="HIAT:w" s="T572">kambi</ts>
                  <nts id="Seg_2083" n="HIAT:ip">.</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T580" id="Seg_2085" n="sc" s="T574">
               <ts e="T580" id="Seg_2087" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2089" n="HIAT:w" s="T574">Dĭ</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2092" n="HIAT:w" s="T575">măna</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2095" n="HIAT:w" s="T576">kumbi</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2098" n="HIAT:ip">(</nts>
                  <ts e="T578" id="Seg_2100" n="HIAT:w" s="T577">amnobi=</ts>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2104" n="HIAT:w" s="T578">amnobi</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2106" n="HIAT:ip">(</nts>
                  <ts e="T580" id="Seg_2108" n="HIAT:w" s="T579">mašinaʔizi</ts>
                  <nts id="Seg_2109" n="HIAT:ip">)</nts>
                  <nts id="Seg_2110" n="HIAT:ip">.</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T589" id="Seg_2112" n="sc" s="T581">
               <ts e="T589" id="Seg_2114" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2116" n="HIAT:w" s="T581">Măn</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2118" n="HIAT:ip">(</nts>
                  <ts e="T583" id="Seg_2120" n="HIAT:w" s="T582">ej-</ts>
                  <nts id="Seg_2121" n="HIAT:ip">)</nts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2124" n="HIAT:w" s="T583">ej</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2127" n="HIAT:w" s="T584">moliam</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2130" n="HIAT:w" s="T585">kuzittə</ts>
                  <nts id="Seg_2131" n="HIAT:ip">,</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2134" n="HIAT:w" s="T586">dĭgəttə</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2137" n="HIAT:w" s="T587">kubiam</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2140" n="HIAT:w" s="T588">plemʼannicam</ts>
                  <nts id="Seg_2141" n="HIAT:ip">.</nts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T596" id="Seg_2143" n="sc" s="T590">
               <ts e="T596" id="Seg_2145" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2147" n="HIAT:w" s="T590">A</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2150" n="HIAT:w" s="T591">dĭbər</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2153" n="HIAT:w" s="T592">kandəbiam</ts>
                  <nts id="Seg_2154" n="HIAT:ip">,</nts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2156" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_2158" n="HIAT:w" s="T593">dö-</ts>
                  <nts id="Seg_2159" n="HIAT:ip">)</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2162" n="HIAT:w" s="T594">döʔən</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2165" n="HIAT:w" s="T595">mašinazi</ts>
                  <nts id="Seg_2166" n="HIAT:ip">.</nts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T600" id="Seg_2168" n="sc" s="T597">
               <ts e="T600" id="Seg_2170" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2172" n="HIAT:w" s="T597">A</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2175" n="HIAT:w" s="T598">dĭn</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2178" n="HIAT:w" s="T599">avtobussi</ts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T610" id="Seg_2181" n="sc" s="T601">
               <ts e="T610" id="Seg_2183" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2185" n="HIAT:w" s="T601">Dĭgəttə</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2188" n="HIAT:w" s="T602">bazaj</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2190" n="HIAT:ip">(</nts>
                  <ts e="T604" id="Seg_2192" n="HIAT:w" s="T603">aktʼi</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2195" n="HIAT:w" s="T604">=</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2198" n="HIAT:w" s="T605">aktʼa=</ts>
                  <nts id="Seg_2199" n="HIAT:ip">)</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2202" n="HIAT:w" s="T606">aktʼi</ts>
                  <nts id="Seg_2203" n="HIAT:ip">,</nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2206" n="HIAT:w" s="T607">dĭgəttə</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2209" n="HIAT:w" s="T608">dĭbər</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2212" n="HIAT:w" s="T609">šobiam</ts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_2215" n="sc" s="T611">
               <ts e="T612" id="Seg_2217" n="HIAT:u" s="T611">
                  <nts id="Seg_2218" n="HIAT:ip">(</nts>
                  <nts id="Seg_2219" n="HIAT:ip">(</nts>
                  <ats e="T612" id="Seg_2220" n="HIAT:non-pho" s="T611">BRK</ats>
                  <nts id="Seg_2221" n="HIAT:ip">)</nts>
                  <nts id="Seg_2222" n="HIAT:ip">)</nts>
                  <nts id="Seg_2223" n="HIAT:ip">.</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T619" id="Seg_2225" n="sc" s="T613">
               <ts e="T619" id="Seg_2227" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2229" n="HIAT:w" s="T613">Dĭgəttə</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2232" n="HIAT:w" s="T614">mĭmbiem</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2235" n="HIAT:w" s="T615">kudajdə</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2237" n="HIAT:ip">(</nts>
                  <ts e="T617" id="Seg_2239" n="HIAT:w" s="T616">uman-</ts>
                  <nts id="Seg_2240" n="HIAT:ip">)</nts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2243" n="HIAT:w" s="T617">numan</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2246" n="HIAT:w" s="T618">üzittə</ts>
                  <nts id="Seg_2247" n="HIAT:ip">.</nts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T624" id="Seg_2249" n="sc" s="T620">
               <ts e="T624" id="Seg_2251" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_2253" n="HIAT:w" s="T620">Dĭgəttə</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2256" n="HIAT:w" s="T621">mĭmbibeʔ</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2259" n="HIAT:w" s="T622">plemʼannicazi</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2261" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2263" n="HIAT:w" s="T623">žibiaʔinə</ts>
                  <nts id="Seg_2264" n="HIAT:ip">)</nts>
                  <nts id="Seg_2265" n="HIAT:ip">.</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T630" id="Seg_2267" n="sc" s="T625">
               <ts e="T630" id="Seg_2269" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2271" n="HIAT:w" s="T625">Dĭn</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2273" n="HIAT:ip">(</nts>
                  <ts e="T627" id="Seg_2275" n="HIAT:w" s="T626">s-</ts>
                  <nts id="Seg_2276" n="HIAT:ip">)</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2279" n="HIAT:w" s="T627">amnobibaʔ</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2282" n="HIAT:w" s="T628">da</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2285" n="HIAT:w" s="T629">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_2286" n="HIAT:ip">.</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T635" id="Seg_2288" n="sc" s="T631">
               <ts e="T635" id="Seg_2290" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2292" n="HIAT:w" s="T631">A</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2295" n="HIAT:w" s="T632">dĭgəttə</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2298" n="HIAT:w" s="T633">maʔndə</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2301" n="HIAT:w" s="T634">šobiam</ts>
                  <nts id="Seg_2302" n="HIAT:ip">.</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T637" id="Seg_2304" n="sc" s="T636">
               <ts e="T637" id="Seg_2306" n="HIAT:u" s="T636">
                  <nts id="Seg_2307" n="HIAT:ip">(</nts>
                  <nts id="Seg_2308" n="HIAT:ip">(</nts>
                  <ats e="T637" id="Seg_2309" n="HIAT:non-pho" s="T636">BRK</ats>
                  <nts id="Seg_2310" n="HIAT:ip">)</nts>
                  <nts id="Seg_2311" n="HIAT:ip">)</nts>
                  <nts id="Seg_2312" n="HIAT:ip">.</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T649" id="Seg_2314" n="sc" s="T638">
               <ts e="T643" id="Seg_2316" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2318" n="HIAT:w" s="T638">Plemʼannica</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2321" n="HIAT:w" s="T639">măna</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2324" n="HIAT:w" s="T640">pomidor</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2327" n="HIAT:w" s="T641">iʔgö</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2330" n="HIAT:w" s="T642">mĭbi</ts>
                  <nts id="Seg_2331" n="HIAT:ip">.</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2334" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2336" n="HIAT:w" s="T643">Măn</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2338" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_2340" n="HIAT:w" s="T644">Kat-</ts>
                  <nts id="Seg_2341" n="HIAT:ip">)</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_2344" n="HIAT:w" s="T645">Kazan</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2347" n="HIAT:w" s="T1184">turagən</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2350" n="HIAT:w" s="T646">šide</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2353" n="HIAT:w" s="T647">dʼala</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2356" n="HIAT:w" s="T648">amnobiam</ts>
                  <nts id="Seg_2357" n="HIAT:ip">.</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T651" id="Seg_2359" n="sc" s="T650">
               <ts e="T651" id="Seg_2361" n="HIAT:u" s="T650">
                  <nts id="Seg_2362" n="HIAT:ip">(</nts>
                  <nts id="Seg_2363" n="HIAT:ip">(</nts>
                  <ats e="T651" id="Seg_2364" n="HIAT:non-pho" s="T650">BRK</ats>
                  <nts id="Seg_2365" n="HIAT:ip">)</nts>
                  <nts id="Seg_2366" n="HIAT:ip">)</nts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T662" id="Seg_2369" n="sc" s="T652">
               <ts e="T662" id="Seg_2371" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_2373" n="HIAT:w" s="T652">Tüžöjʔi</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2376" n="HIAT:w" s="T653">šobiʔi</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2379" n="HIAT:w" s="T654">maʔndə</ts>
                  <nts id="Seg_2380" n="HIAT:ip">,</nts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2383" n="HIAT:w" s="T655">nada</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2386" n="HIAT:w" s="T656">dĭzem</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2389" n="HIAT:w" s="T657">šedendə</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2392" n="HIAT:w" s="T658">sürerzittə</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2395" n="HIAT:w" s="T659">i</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2397" n="HIAT:ip">(</nts>
                  <ts e="T661" id="Seg_2399" n="HIAT:w" s="T660">nom-</ts>
                  <nts id="Seg_2400" n="HIAT:ip">)</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2403" n="HIAT:w" s="T661">noʔməzittə</ts>
                  <nts id="Seg_2404" n="HIAT:ip">.</nts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T664" id="Seg_2406" n="sc" s="T663">
               <ts e="T664" id="Seg_2408" n="HIAT:u" s="T663">
                  <nts id="Seg_2409" n="HIAT:ip">(</nts>
                  <nts id="Seg_2410" n="HIAT:ip">(</nts>
                  <ats e="T664" id="Seg_2411" n="HIAT:non-pho" s="T663">BRK</ats>
                  <nts id="Seg_2412" n="HIAT:ip">)</nts>
                  <nts id="Seg_2413" n="HIAT:ip">)</nts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_2416" n="sc" s="T665">
               <ts e="T669" id="Seg_2418" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2420" n="HIAT:w" s="T665">Tenöbiam</ts>
                  <nts id="Seg_2421" n="HIAT:ip">,</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2424" n="HIAT:w" s="T666">tenöbiam</ts>
                  <nts id="Seg_2425" n="HIAT:ip">,</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2428" n="HIAT:w" s="T667">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2431" n="HIAT:w" s="T668">tenöluʔpiem</ts>
                  <nts id="Seg_2432" n="HIAT:ip">.</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T671" id="Seg_2434" n="sc" s="T670">
               <ts e="T671" id="Seg_2436" n="HIAT:u" s="T670">
                  <nts id="Seg_2437" n="HIAT:ip">(</nts>
                  <nts id="Seg_2438" n="HIAT:ip">(</nts>
                  <ats e="T671" id="Seg_2439" n="HIAT:non-pho" s="T670">DMG</ats>
                  <nts id="Seg_2440" n="HIAT:ip">)</nts>
                  <nts id="Seg_2441" n="HIAT:ip">)</nts>
                  <nts id="Seg_2442" n="HIAT:ip">.</nts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T678" id="Seg_2444" n="sc" s="T672">
               <ts e="T678" id="Seg_2446" n="HIAT:u" s="T672">
                  <nts id="Seg_2447" n="HIAT:ip">(</nts>
                  <ts e="T673" id="Seg_2449" n="HIAT:w" s="T672">Koʔbdonə</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2452" n="HIAT:w" s="T673">mĭmbiem</ts>
                  <nts id="Seg_2453" n="HIAT:ip">)</nts>
                  <nts id="Seg_2454" n="HIAT:ip">,</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2457" n="HIAT:w" s="T674">sumka</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2460" n="HIAT:w" s="T675">dĭ</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2463" n="HIAT:w" s="T676">bar</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2466" n="HIAT:w" s="T677">sajnʼiluʔpi</ts>
                  <nts id="Seg_2467" n="HIAT:ip">.</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T684" id="Seg_2469" n="sc" s="T679">
               <ts e="T684" id="Seg_2471" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_2473" n="HIAT:w" s="T679">Tüj</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2476" n="HIAT:w" s="T680">amnolaʔbəm</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2479" n="HIAT:w" s="T681">da</ts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2481" n="HIAT:ip">(</nts>
                  <ts e="T683" id="Seg_2483" n="HIAT:w" s="T682">s-</ts>
                  <nts id="Seg_2484" n="HIAT:ip">)</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2487" n="HIAT:w" s="T683">šüdörleʔbəm</ts>
                  <nts id="Seg_2488" n="HIAT:ip">.</nts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T691" id="Seg_2490" n="sc" s="T685">
               <ts e="T691" id="Seg_2492" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2494" n="HIAT:w" s="T685">Tănan</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2497" n="HIAT:w" s="T686">šonəbiam</ts>
                  <nts id="Seg_2498" n="HIAT:ip">,</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2500" n="HIAT:ip">(</nts>
                  <ts e="T688" id="Seg_2502" n="HIAT:w" s="T687">šonəbiam</ts>
                  <nts id="Seg_2503" n="HIAT:ip">)</nts>
                  <nts id="Seg_2504" n="HIAT:ip">,</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2507" n="HIAT:w" s="T688">appi</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2510" n="HIAT:w" s="T689">ej</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2513" n="HIAT:w" s="T690">saʔməluʔpiam</ts>
                  <nts id="Seg_2514" n="HIAT:ip">.</nts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T693" id="Seg_2516" n="sc" s="T692">
               <ts e="T693" id="Seg_2518" n="HIAT:u" s="T692">
                  <nts id="Seg_2519" n="HIAT:ip">(</nts>
                  <nts id="Seg_2520" n="HIAT:ip">(</nts>
                  <ats e="T693" id="Seg_2521" n="HIAT:non-pho" s="T692">BRK</ats>
                  <nts id="Seg_2522" n="HIAT:ip">)</nts>
                  <nts id="Seg_2523" n="HIAT:ip">)</nts>
                  <nts id="Seg_2524" n="HIAT:ip">.</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T696" id="Seg_2526" n="sc" s="T694">
               <ts e="T695" id="Seg_2528" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2530" n="HIAT:w" s="T694">Погоди</ts>
                  <nts id="Seg_2531" n="HIAT:ip">.</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2534" n="HIAT:u" s="T695">
                  <nts id="Seg_2535" n="HIAT:ip">(</nts>
                  <nts id="Seg_2536" n="HIAT:ip">(</nts>
                  <ats e="T695.tx.1" id="Seg_2537" n="HIAT:non-pho" s="T695">KA:</ats>
                  <nts id="Seg_2538" n="HIAT:ip">)</nts>
                  <nts id="Seg_2539" n="HIAT:ip">)</nts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2542" n="HIAT:w" s="T695.tx.1">Kozaŋ</ts>
                  <nts id="Seg_2543" n="HIAT:ip">.</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T711" id="Seg_2545" n="sc" s="T697">
               <ts e="T700" id="Seg_2547" n="HIAT:u" s="T697">
                  <nts id="Seg_2548" n="HIAT:ip">(</nts>
                  <nts id="Seg_2549" n="HIAT:ip">(</nts>
                  <ats e="T697.tx.1" id="Seg_2550" n="HIAT:non-pho" s="T697">PKZ:</ats>
                  <nts id="Seg_2551" n="HIAT:ip">)</nts>
                  <nts id="Seg_2552" n="HIAT:ip">)</nts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2555" n="HIAT:w" s="T697.tx.1">Kazaŋ</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2558" n="HIAT:w" s="T698">šonəga</ts>
                  <nts id="Seg_2559" n="HIAT:ip">,</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2562" n="HIAT:w" s="T699">šonəga</ts>
                  <nts id="Seg_2563" n="HIAT:ip">.</nts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2566" n="HIAT:u" s="T700">
                  <ts e="T701" id="Seg_2568" n="HIAT:w" s="T700">Dĭgəttə</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2571" n="HIAT:w" s="T701">noʔ</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2574" n="HIAT:w" s="T702">dĭʔnə</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2576" n="HIAT:ip">(</nts>
                  <ts e="T704" id="Seg_2578" n="HIAT:w" s="T703">püj-</ts>
                  <nts id="Seg_2579" n="HIAT:ip">)</nts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2582" n="HIAT:w" s="T704">püje</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2584" n="HIAT:ip">(</nts>
                  <ts e="T706" id="Seg_2586" n="HIAT:w" s="T705">bĭʔpi-</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2589" n="HIAT:w" s="T706">bə-</ts>
                  <nts id="Seg_2590" n="HIAT:ip">)</nts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2593" n="HIAT:w" s="T707">bătluʔbi</ts>
                  <nts id="Seg_2594" n="HIAT:ip">.</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_2597" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2599" n="HIAT:w" s="T708">Kemdə</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2602" n="HIAT:w" s="T709">bar</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2605" n="HIAT:w" s="T710">mʼaŋnaʔbə</ts>
                  <nts id="Seg_2606" n="HIAT:ip">.</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T715" id="Seg_2608" n="sc" s="T712">
               <ts e="T715" id="Seg_2610" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2612" n="HIAT:w" s="T712">Dĭ</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2615" n="HIAT:w" s="T713">măndə:</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2617" n="HIAT:ip">"</nts>
                  <ts e="T715" id="Seg_2619" n="HIAT:w" s="T714">Šü</ts>
                  <nts id="Seg_2620" n="HIAT:ip">!</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T719" id="Seg_2622" n="sc" s="T716">
               <ts e="T719" id="Seg_2624" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2626" n="HIAT:w" s="T716">Nendədə</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2629" n="HIAT:w" s="T717">dĭ</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2632" n="HIAT:w" s="T718">noʔ</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T721" id="Seg_2635" n="sc" s="T720">
               <ts e="T721" id="Seg_2637" n="HIAT:u" s="T720">
                  <nts id="Seg_2638" n="HIAT:ip">(</nts>
                  <nts id="Seg_2639" n="HIAT:ip">(</nts>
                  <ats e="T721" id="Seg_2640" n="HIAT:non-pho" s="T720">BRK</ats>
                  <nts id="Seg_2641" n="HIAT:ip">)</nts>
                  <nts id="Seg_2642" n="HIAT:ip">)</nts>
                  <nts id="Seg_2643" n="HIAT:ip">.</nts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T730" id="Seg_2645" n="sc" s="T722">
               <ts e="T726" id="Seg_2647" n="HIAT:u" s="T722">
                  <nts id="Seg_2648" n="HIAT:ip">"</nts>
                  <ts e="T723" id="Seg_2650" n="HIAT:w" s="T722">Măn</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2653" n="HIAT:w" s="T723">iʔgö</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2656" n="HIAT:w" s="T724">dʼüm</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2659" n="HIAT:w" s="T725">nendəsʼtə</ts>
                  <nts id="Seg_2660" n="HIAT:ip">"</nts>
                  <nts id="Seg_2661" n="HIAT:ip">.</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2664" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2666" n="HIAT:w" s="T726">Dĭgəttə</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2669" n="HIAT:w" s="T727">dĭ</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2672" n="HIAT:w" s="T728">bünə</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2675" n="HIAT:w" s="T729">kambi</ts>
                  <nts id="Seg_2676" n="HIAT:ip">.</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T736" id="Seg_2678" n="sc" s="T731">
               <ts e="T736" id="Seg_2680" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2682" n="HIAT:w" s="T731">Bü</ts>
                  <nts id="Seg_2683" n="HIAT:ip">,</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2686" n="HIAT:w" s="T732">kanaʔ</ts>
                  <nts id="Seg_2687" n="HIAT:ip">,</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2690" n="HIAT:w" s="T733">šüm</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2692" n="HIAT:ip">(</nts>
                  <ts e="T735" id="Seg_2694" n="HIAT:w" s="T734">kăm-</ts>
                  <nts id="Seg_2695" n="HIAT:ip">)</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2698" n="HIAT:w" s="T735">kămnaʔ</ts>
                  <nts id="Seg_2699" n="HIAT:ip">.</nts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T738" id="Seg_2701" n="sc" s="T737">
               <ts e="T738" id="Seg_2703" n="HIAT:u" s="T737">
                  <nts id="Seg_2704" n="HIAT:ip">(</nts>
                  <nts id="Seg_2705" n="HIAT:ip">(</nts>
                  <ats e="T738" id="Seg_2706" n="HIAT:non-pho" s="T737">BRK</ats>
                  <nts id="Seg_2707" n="HIAT:ip">)</nts>
                  <nts id="Seg_2708" n="HIAT:ip">)</nts>
                  <nts id="Seg_2709" n="HIAT:ip">.</nts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T747" id="Seg_2711" n="sc" s="T739">
               <ts e="T747" id="Seg_2713" n="HIAT:u" s="T739">
                  <nts id="Seg_2714" n="HIAT:ip">"</nts>
                  <ts e="T740" id="Seg_2716" n="HIAT:w" s="T739">Măn</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2719" n="HIAT:w" s="T740">iʔgö</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2721" n="HIAT:ip">(</nts>
                  <ts e="T742" id="Seg_2723" n="HIAT:w" s="T741">dʼ-</ts>
                  <nts id="Seg_2724" n="HIAT:ip">)</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2727" n="HIAT:w" s="T742">dʼü</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2730" n="HIAT:w" s="T743">kămnastə</ts>
                  <nts id="Seg_2731" n="HIAT:ip">,</nts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2734" n="HIAT:w" s="T744">štobɨ</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2737" n="HIAT:w" s="T745">noʔ</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2740" n="HIAT:w" s="T746">özerbi</ts>
                  <nts id="Seg_2741" n="HIAT:ip">.</nts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T749" id="Seg_2743" n="sc" s="T748">
               <ts e="T749" id="Seg_2745" n="HIAT:u" s="T748">
                  <nts id="Seg_2746" n="HIAT:ip">(</nts>
                  <nts id="Seg_2747" n="HIAT:ip">(</nts>
                  <ats e="T749" id="Seg_2748" n="HIAT:non-pho" s="T748">BRK</ats>
                  <nts id="Seg_2749" n="HIAT:ip">)</nts>
                  <nts id="Seg_2750" n="HIAT:ip">)</nts>
                  <nts id="Seg_2751" n="HIAT:ip">.</nts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T753" id="Seg_2753" n="sc" s="T750">
               <ts e="T753" id="Seg_2755" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_2757" n="HIAT:w" s="T750">Dĭgəttə</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2760" n="HIAT:w" s="T751">dĭ</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2763" n="HIAT:w" s="T752">kambi</ts>
                  <nts id="Seg_2764" n="HIAT:ip">.</nts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T761" id="Seg_2766" n="sc" s="T754">
               <ts e="T761" id="Seg_2768" n="HIAT:u" s="T754">
                  <nts id="Seg_2769" n="HIAT:ip">"</nts>
                  <ts e="T755" id="Seg_2771" n="HIAT:w" s="T754">Bulan</ts>
                  <nts id="Seg_2772" n="HIAT:ip">,</nts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2775" n="HIAT:w" s="T755">bulan</ts>
                  <nts id="Seg_2776" n="HIAT:ip">,</nts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2779" n="HIAT:w" s="T756">kanaʔ</ts>
                  <nts id="Seg_2780" n="HIAT:ip">,</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2783" n="HIAT:w" s="T757">bar</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2786" n="HIAT:w" s="T758">bü</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2788" n="HIAT:ip">(</nts>
                  <ts e="T760" id="Seg_2790" n="HIAT:w" s="T759">bĭ-</ts>
                  <nts id="Seg_2791" n="HIAT:ip">)</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2794" n="HIAT:w" s="T760">bĭdeʔ</ts>
                  <nts id="Seg_2795" n="HIAT:ip">!</nts>
                  <nts id="Seg_2796" n="HIAT:ip">"</nts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T769" id="Seg_2798" n="sc" s="T762">
               <ts e="T769" id="Seg_2800" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_2802" n="HIAT:w" s="T762">Dĭgəttə</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2805" n="HIAT:w" s="T763">bulan</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2808" n="HIAT:w" s="T764">mămbi:</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2810" n="HIAT:ip">"</nts>
                  <ts e="T766" id="Seg_2812" n="HIAT:w" s="T765">Măn</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2815" n="HIAT:w" s="T766">üjündə</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2818" n="HIAT:w" s="T767">iʔgö</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2821" n="HIAT:w" s="T768">bü</ts>
                  <nts id="Seg_2822" n="HIAT:ip">!</nts>
                  <nts id="Seg_2823" n="HIAT:ip">"</nts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T781" id="Seg_2825" n="sc" s="T770">
               <ts e="T775" id="Seg_2827" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2829" n="HIAT:w" s="T770">Dĭgəttə</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2832" n="HIAT:w" s="T771">dĭ</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2834" n="HIAT:ip">(</nts>
                  <ts e="T773" id="Seg_2836" n="HIAT:w" s="T772">m-</ts>
                  <nts id="Seg_2837" n="HIAT:ip">)</nts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2840" n="HIAT:w" s="T773">kambi</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2843" n="HIAT:w" s="T774">nükenə</ts>
                  <nts id="Seg_2844" n="HIAT:ip">.</nts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_2847" n="HIAT:u" s="T775">
                  <nts id="Seg_2848" n="HIAT:ip">"</nts>
                  <ts e="T776" id="Seg_2850" n="HIAT:w" s="T775">Nüke</ts>
                  <nts id="Seg_2851" n="HIAT:ip">,</nts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2854" n="HIAT:w" s="T776">nüke</ts>
                  <nts id="Seg_2855" n="HIAT:ip">,</nts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2858" n="HIAT:w" s="T777">kanaʔ</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2861" n="HIAT:w" s="T778">žilaʔi</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2864" n="HIAT:w" s="T779">üjüttə</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2867" n="HIAT:w" s="T780">it</ts>
                  <nts id="Seg_2868" n="HIAT:ip">!</nts>
                  <nts id="Seg_2869" n="HIAT:ip">"</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T785" id="Seg_2871" n="sc" s="T782">
               <ts e="T785" id="Seg_2873" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_2875" n="HIAT:w" s="T782">Nüke</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2878" n="HIAT:w" s="T783">ej</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2881" n="HIAT:w" s="T784">kambi</ts>
                  <nts id="Seg_2882" n="HIAT:ip">.</nts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T789" id="Seg_2884" n="sc" s="T786">
               <ts e="T789" id="Seg_2886" n="HIAT:u" s="T786">
                  <nts id="Seg_2887" n="HIAT:ip">"</nts>
                  <ts e="T787" id="Seg_2889" n="HIAT:w" s="T786">Sagərʔi</ts>
                  <nts id="Seg_2890" n="HIAT:ip">,</nts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2893" n="HIAT:w" s="T787">kabarləj</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2896" n="HIAT:w" s="T788">măna</ts>
                  <nts id="Seg_2897" n="HIAT:ip">"</nts>
                  <nts id="Seg_2898" n="HIAT:ip">.</nts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T791" id="Seg_2900" n="sc" s="T790">
               <ts e="T791" id="Seg_2902" n="HIAT:u" s="T790">
                  <nts id="Seg_2903" n="HIAT:ip">(</nts>
                  <nts id="Seg_2904" n="HIAT:ip">(</nts>
                  <ats e="T791" id="Seg_2905" n="HIAT:non-pho" s="T790">BRK</ats>
                  <nts id="Seg_2906" n="HIAT:ip">)</nts>
                  <nts id="Seg_2907" n="HIAT:ip">)</nts>
                  <nts id="Seg_2908" n="HIAT:ip">.</nts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T811" id="Seg_2910" n="sc" s="T792">
               <ts e="T800" id="Seg_2912" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_2914" n="HIAT:w" s="T792">Dĭ</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2917" n="HIAT:w" s="T793">kambi</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2920" n="HIAT:w" s="T794">nükenə:</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2922" n="HIAT:ip">"</nts>
                  <ts e="T796" id="Seg_2924" n="HIAT:w" s="T795">Nüke</ts>
                  <nts id="Seg_2925" n="HIAT:ip">,</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2928" n="HIAT:w" s="T796">nüke</ts>
                  <nts id="Seg_2929" n="HIAT:ip">,</nts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2932" n="HIAT:w" s="T797">it</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2934" n="HIAT:ip">(</nts>
                  <ts e="T799" id="Seg_2936" n="HIAT:w" s="T798">žil-</ts>
                  <nts id="Seg_2937" n="HIAT:ip">)</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2940" n="HIAT:w" s="T799">žilaʔi</ts>
                  <nts id="Seg_2941" n="HIAT:ip">"</nts>
                  <nts id="Seg_2942" n="HIAT:ip">.</nts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_2945" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_2947" n="HIAT:w" s="T800">A</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2950" n="HIAT:w" s="T801">dĭ</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2953" n="HIAT:w" s="T802">măndə:</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2955" n="HIAT:ip">"</nts>
                  <ts e="T804" id="Seg_2957" n="HIAT:w" s="T803">Măn</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2960" n="HIAT:w" s="T804">iʔgö</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2963" n="HIAT:w" s="T805">ige</ts>
                  <nts id="Seg_2964" n="HIAT:ip">,</nts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2966" n="HIAT:ip">(</nts>
                  <ts e="T807" id="Seg_2968" n="HIAT:w" s="T806">da</ts>
                  <nts id="Seg_2969" n="HIAT:ip">)</nts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2972" n="HIAT:w" s="T807">sagər</ts>
                  <nts id="Seg_2973" n="HIAT:ip">,</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2976" n="HIAT:w" s="T808">măna</ts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2979" n="HIAT:w" s="T809">ej</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2982" n="HIAT:w" s="T810">kereʔ</ts>
                  <nts id="Seg_2983" n="HIAT:ip">"</nts>
                  <nts id="Seg_2984" n="HIAT:ip">.</nts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T813" id="Seg_2986" n="sc" s="T812">
               <ts e="T813" id="Seg_2988" n="HIAT:u" s="T812">
                  <nts id="Seg_2989" n="HIAT:ip">(</nts>
                  <nts id="Seg_2990" n="HIAT:ip">(</nts>
                  <ats e="T813" id="Seg_2991" n="HIAT:non-pho" s="T812">BRK</ats>
                  <nts id="Seg_2992" n="HIAT:ip">)</nts>
                  <nts id="Seg_2993" n="HIAT:ip">)</nts>
                  <nts id="Seg_2994" n="HIAT:ip">.</nts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T823" id="Seg_2996" n="sc" s="T814">
               <ts e="T817" id="Seg_2998" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_3000" n="HIAT:w" s="T814">Dĭgəttə</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3003" n="HIAT:w" s="T815">kambi</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3006" n="HIAT:w" s="T816">tumoʔinə</ts>
                  <nts id="Seg_3007" n="HIAT:ip">.</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3010" n="HIAT:u" s="T817">
                  <nts id="Seg_3011" n="HIAT:ip">"</nts>
                  <ts e="T818" id="Seg_3013" n="HIAT:w" s="T817">Tumo</ts>
                  <nts id="Seg_3014" n="HIAT:ip">,</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3017" n="HIAT:w" s="T818">tumo</ts>
                  <nts id="Seg_3018" n="HIAT:ip">,</nts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3021" n="HIAT:w" s="T819">kanaʔ</ts>
                  <nts id="Seg_3022" n="HIAT:ip">,</nts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3025" n="HIAT:w" s="T820">nüken</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3028" n="HIAT:w" s="T821">žilaʔi</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3031" n="HIAT:w" s="T822">amnaʔ</ts>
                  <nts id="Seg_3032" n="HIAT:ip">!</nts>
                  <nts id="Seg_3033" n="HIAT:ip">"</nts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T831" id="Seg_3035" n="sc" s="T824">
               <ts e="T831" id="Seg_3037" n="HIAT:u" s="T824">
                  <nts id="Seg_3038" n="HIAT:ip">"</nts>
                  <ts e="T825" id="Seg_3040" n="HIAT:w" s="T824">dʼok</ts>
                  <nts id="Seg_3041" n="HIAT:ip">,</nts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3043" n="HIAT:ip">(</nts>
                  <ts e="T826" id="Seg_3045" n="HIAT:w" s="T825">mi-</ts>
                  <nts id="Seg_3046" n="HIAT:ip">)</nts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3049" n="HIAT:w" s="T826">miʔ</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3051" n="HIAT:ip">(</nts>
                  <ts e="T828" id="Seg_3053" n="HIAT:w" s="T827">em</ts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3056" n="HIAT:w" s="T828">kallaʔ=</ts>
                  <nts id="Seg_3057" n="HIAT:ip">)</nts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3060" n="HIAT:w" s="T829">ej</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3063" n="HIAT:w" s="T830">kallam</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T837" id="Seg_3066" n="sc" s="T832">
               <ts e="T837" id="Seg_3068" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_3070" n="HIAT:w" s="T832">Miʔnʼibeʔ</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3073" n="HIAT:w" s="T833">kabarləj</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3076" n="HIAT:w" s="T834">dʼügən</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3079" n="HIAT:w" s="T835">kornʼiʔi</ts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3082" n="HIAT:w" s="T836">amzittə</ts>
                  <nts id="Seg_3083" n="HIAT:ip">"</nts>
                  <nts id="Seg_3084" n="HIAT:ip">.</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T839" id="Seg_3086" n="sc" s="T838">
               <ts e="T839" id="Seg_3088" n="HIAT:u" s="T838">
                  <nts id="Seg_3089" n="HIAT:ip">(</nts>
                  <nts id="Seg_3090" n="HIAT:ip">(</nts>
                  <ats e="T839" id="Seg_3091" n="HIAT:non-pho" s="T838">BRK</ats>
                  <nts id="Seg_3092" n="HIAT:ip">)</nts>
                  <nts id="Seg_3093" n="HIAT:ip">)</nts>
                  <nts id="Seg_3094" n="HIAT:ip">.</nts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T854" id="Seg_3096" n="sc" s="T840">
               <ts e="T844" id="Seg_3098" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3100" n="HIAT:w" s="T840">Dĭgəttə</ts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3103" n="HIAT:w" s="T841">kambi</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3105" n="HIAT:ip">(</nts>
                  <nts id="Seg_3106" n="HIAT:ip">(</nts>
                  <ats e="T843" id="Seg_3107" n="HIAT:non-pho" s="T842">DMG</ats>
                  <nts id="Seg_3108" n="HIAT:ip">)</nts>
                  <nts id="Seg_3109" n="HIAT:ip">)</nts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3112" n="HIAT:w" s="T843">esseŋdə</ts>
                  <nts id="Seg_3113" n="HIAT:ip">.</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3116" n="HIAT:u" s="T844">
                  <nts id="Seg_3117" n="HIAT:ip">"</nts>
                  <ts e="T845" id="Seg_3119" n="HIAT:w" s="T844">Esseŋ</ts>
                  <nts id="Seg_3120" n="HIAT:ip">,</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3123" n="HIAT:w" s="T845">esseŋ</ts>
                  <nts id="Seg_3124" n="HIAT:ip">,</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3127" n="HIAT:w" s="T846">kutlaʔləj</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3130" n="HIAT:w" s="T847">tumoʔi</ts>
                  <nts id="Seg_3131" n="HIAT:ip">!</nts>
                  <nts id="Seg_3132" n="HIAT:ip">"</nts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3135" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3137" n="HIAT:w" s="T848">Dĭzeŋ:</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3139" n="HIAT:ip">"</nts>
                  <ts e="T850" id="Seg_3141" n="HIAT:w" s="T849">Miʔ</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3143" n="HIAT:ip">(</nts>
                  <ts e="T851" id="Seg_3145" n="HIAT:w" s="T850">bo-</ts>
                  <nts id="Seg_3146" n="HIAT:ip">)</nts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3149" n="HIAT:w" s="T851">bospə</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3152" n="HIAT:w" s="T852">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3155" n="HIAT:w" s="T853">baːbkaʔiziʔ</ts>
                  <nts id="Seg_3156" n="HIAT:ip">"</nts>
                  <nts id="Seg_3157" n="HIAT:ip">.</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T856" id="Seg_3159" n="sc" s="T855">
               <ts e="T856" id="Seg_3161" n="HIAT:u" s="T855">
                  <nts id="Seg_3162" n="HIAT:ip">(</nts>
                  <nts id="Seg_3163" n="HIAT:ip">(</nts>
                  <ats e="T856" id="Seg_3164" n="HIAT:non-pho" s="T855">BRK</ats>
                  <nts id="Seg_3165" n="HIAT:ip">)</nts>
                  <nts id="Seg_3166" n="HIAT:ip">)</nts>
                  <nts id="Seg_3167" n="HIAT:ip">.</nts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T861" id="Seg_3169" n="sc" s="T857">
               <ts e="T861" id="Seg_3171" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_3173" n="HIAT:w" s="T857">Šiʔ</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3176" n="HIAT:w" s="T858">turagən</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3179" n="HIAT:w" s="T859">jakše</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3182" n="HIAT:w" s="T860">amnozittə</ts>
                  <nts id="Seg_3183" n="HIAT:ip">.</nts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T867" id="Seg_3185" n="sc" s="T862">
               <ts e="T867" id="Seg_3187" n="HIAT:u" s="T862">
                  <ts e="T863" id="Seg_3189" n="HIAT:w" s="T862">Tolʼko</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3192" n="HIAT:w" s="T863">ipek</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3195" n="HIAT:w" s="T864">gijendə</ts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3198" n="HIAT:w" s="T865">ej</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3201" n="HIAT:w" s="T866">ilil</ts>
                  <nts id="Seg_3202" n="HIAT:ip">.</nts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T871" id="Seg_3204" n="sc" s="T868">
               <ts e="T871" id="Seg_3206" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3208" n="HIAT:w" s="T868">Amzittə</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3211" n="HIAT:w" s="T869">naga</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3214" n="HIAT:w" s="T870">ipek</ts>
                  <nts id="Seg_3215" n="HIAT:ip">.</nts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T873" id="Seg_3217" n="sc" s="T872">
               <ts e="T873" id="Seg_3219" n="HIAT:u" s="T872">
                  <nts id="Seg_3220" n="HIAT:ip">(</nts>
                  <nts id="Seg_3221" n="HIAT:ip">(</nts>
                  <ats e="T873" id="Seg_3222" n="HIAT:non-pho" s="T872">BRK</ats>
                  <nts id="Seg_3223" n="HIAT:ip">)</nts>
                  <nts id="Seg_3224" n="HIAT:ip">)</nts>
                  <nts id="Seg_3225" n="HIAT:ip">.</nts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T880" id="Seg_3227" n="sc" s="T874">
               <ts e="T880" id="Seg_3229" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3231" n="HIAT:w" s="T874">Magazingəndə</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3234" n="HIAT:w" s="T875">ĭmbi</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3237" n="HIAT:w" s="T876">naga</ts>
                  <nts id="Seg_3238" n="HIAT:ip">,</nts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3241" n="HIAT:w" s="T877">ĭmbidə</ts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3244" n="HIAT:w" s="T878">ej</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3247" n="HIAT:w" s="T879">iləl</ts>
                  <nts id="Seg_3248" n="HIAT:ip">.</nts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T890" id="Seg_3250" n="sc" s="T881">
               <ts e="T890" id="Seg_3252" n="HIAT:u" s="T881">
                  <nts id="Seg_3253" n="HIAT:ip">"</nts>
                  <ts e="T882" id="Seg_3255" n="HIAT:w" s="T881">Dʼok</ts>
                  <nts id="Seg_3256" n="HIAT:ip">,</nts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3259" n="HIAT:w" s="T882">kanaʔ</ts>
                  <nts id="Seg_3260" n="HIAT:ip">,</nts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3263" n="HIAT:w" s="T883">dĭn</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3266" n="HIAT:w" s="T884">ige</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3269" n="HIAT:w" s="T885">kălbasa</ts>
                  <nts id="Seg_3270" n="HIAT:ip">,</nts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3273" n="HIAT:w" s="T886">iʔ</ts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3276" n="HIAT:w" s="T887">da</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3279" n="HIAT:w" s="T888">dĭgəttə</ts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3282" n="HIAT:w" s="T889">amorlal</ts>
                  <nts id="Seg_3283" n="HIAT:ip">"</nts>
                  <nts id="Seg_3284" n="HIAT:ip">.</nts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T892" id="Seg_3286" n="sc" s="T891">
               <ts e="T892" id="Seg_3288" n="HIAT:u" s="T891">
                  <nts id="Seg_3289" n="HIAT:ip">(</nts>
                  <nts id="Seg_3290" n="HIAT:ip">(</nts>
                  <ats e="T892" id="Seg_3291" n="HIAT:non-pho" s="T891">BRK</ats>
                  <nts id="Seg_3292" n="HIAT:ip">)</nts>
                  <nts id="Seg_3293" n="HIAT:ip">)</nts>
                  <nts id="Seg_3294" n="HIAT:ip">.</nts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T900" id="Seg_3296" n="sc" s="T893">
               <ts e="T900" id="Seg_3298" n="HIAT:u" s="T893">
                  <ts e="T894" id="Seg_3300" n="HIAT:w" s="T893">Dĭ</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3303" n="HIAT:w" s="T894">nüke</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3306" n="HIAT:w" s="T895">ugandə</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3309" n="HIAT:w" s="T896">jakše</ts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3312" n="HIAT:w" s="T897">šödörlaʔbə</ts>
                  <nts id="Seg_3313" n="HIAT:ip">,</nts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3316" n="HIAT:w" s="T898">bar</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3319" n="HIAT:w" s="T899">kuvas</ts>
                  <nts id="Seg_3320" n="HIAT:ip">.</nts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T903" id="Seg_3322" n="sc" s="T901">
               <ts e="T903" id="Seg_3324" n="HIAT:u" s="T901">
                  <ts e="T902" id="Seg_3326" n="HIAT:w" s="T901">Jakše</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3329" n="HIAT:w" s="T902">măndosʼtə</ts>
                  <nts id="Seg_3330" n="HIAT:ip">.</nts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T905" id="Seg_3332" n="sc" s="T904">
               <ts e="T905" id="Seg_3334" n="HIAT:u" s="T904">
                  <nts id="Seg_3335" n="HIAT:ip">(</nts>
                  <nts id="Seg_3336" n="HIAT:ip">(</nts>
                  <ats e="T905" id="Seg_3337" n="HIAT:non-pho" s="T904">BRK</ats>
                  <nts id="Seg_3338" n="HIAT:ip">)</nts>
                  <nts id="Seg_3339" n="HIAT:ip">)</nts>
                  <nts id="Seg_3340" n="HIAT:ip">.</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T916" id="Seg_3342" n="sc" s="T906">
               <ts e="T916" id="Seg_3344" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3346" n="HIAT:w" s="T906">Dĭ</ts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3349" n="HIAT:w" s="T907">nüke</ts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3352" n="HIAT:w" s="T908">ugandə</ts>
                  <nts id="Seg_3353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3355" n="HIAT:w" s="T909">iʔgö</ts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3357" n="HIAT:ip">(</nts>
                  <ts e="T911" id="Seg_3359" n="HIAT:w" s="T910">tono-</ts>
                  <nts id="Seg_3360" n="HIAT:ip">)</nts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3363" n="HIAT:w" s="T911">togonoria</ts>
                  <nts id="Seg_3364" n="HIAT:ip">,</nts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3367" n="HIAT:w" s="T912">bar</ts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3370" n="HIAT:w" s="T913">ĭmbi</ts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3373" n="HIAT:w" s="T914">azittə</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3376" n="HIAT:w" s="T915">умеет</ts>
                  <nts id="Seg_3377" n="HIAT:ip">.</nts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T918" id="Seg_3379" n="sc" s="T917">
               <ts e="T918" id="Seg_3381" n="HIAT:u" s="T917">
                  <nts id="Seg_3382" n="HIAT:ip">(</nts>
                  <nts id="Seg_3383" n="HIAT:ip">(</nts>
                  <ats e="T918" id="Seg_3384" n="HIAT:non-pho" s="T917">BRK</ats>
                  <nts id="Seg_3385" n="HIAT:ip">)</nts>
                  <nts id="Seg_3386" n="HIAT:ip">)</nts>
                  <nts id="Seg_3387" n="HIAT:ip">.</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T923" id="Seg_3389" n="sc" s="T919">
               <ts e="T923" id="Seg_3391" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_3393" n="HIAT:w" s="T919">Bar</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3396" n="HIAT:w" s="T920">ĭmbi</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3399" n="HIAT:w" s="T921">togonorzittə</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3402" n="HIAT:w" s="T922">tĭmnet</ts>
                  <nts id="Seg_3403" n="HIAT:ip">.</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T925" id="Seg_3405" n="sc" s="T924">
               <ts e="T925" id="Seg_3407" n="HIAT:u" s="T924">
                  <nts id="Seg_3408" n="HIAT:ip">(</nts>
                  <nts id="Seg_3409" n="HIAT:ip">(</nts>
                  <ats e="T925" id="Seg_3410" n="HIAT:non-pho" s="T924">BRK</ats>
                  <nts id="Seg_3411" n="HIAT:ip">)</nts>
                  <nts id="Seg_3412" n="HIAT:ip">)</nts>
                  <nts id="Seg_3413" n="HIAT:ip">.</nts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T935" id="Seg_3415" n="sc" s="T926">
               <ts e="T935" id="Seg_3417" n="HIAT:u" s="T926">
                  <ts e="T927" id="Seg_3419" n="HIAT:w" s="T926">Tĭn</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3422" n="HIAT:w" s="T927">nüken</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3424" n="HIAT:ip">(</nts>
                  <ts e="T929" id="Seg_3426" n="HIAT:w" s="T928">kuz-</ts>
                  <nts id="Seg_3427" n="HIAT:ip">)</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3430" n="HIAT:w" s="T929">tukazaŋdə</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3433" n="HIAT:w" s="T930">amga</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3436" n="HIAT:w" s="T931">amnambi</ts>
                  <nts id="Seg_3437" n="HIAT:ip">,</nts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3440" n="HIAT:w" s="T932">tolʼko</ts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3443" n="HIAT:w" s="T933">onʼiʔ</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3446" n="HIAT:w" s="T934">kagat</ts>
                  <nts id="Seg_3447" n="HIAT:ip">.</nts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T937" id="Seg_3449" n="sc" s="T936">
               <ts e="T937" id="Seg_3451" n="HIAT:u" s="T936">
                  <nts id="Seg_3452" n="HIAT:ip">(</nts>
                  <nts id="Seg_3453" n="HIAT:ip">(</nts>
                  <ats e="T937" id="Seg_3454" n="HIAT:non-pho" s="T936">BRK</ats>
                  <nts id="Seg_3455" n="HIAT:ip">)</nts>
                  <nts id="Seg_3456" n="HIAT:ip">)</nts>
                  <nts id="Seg_3457" n="HIAT:ip">.</nts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T941" id="Seg_3459" n="sc" s="T938">
               <ts e="T941" id="Seg_3461" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3463" n="HIAT:w" s="T938">A</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3466" n="HIAT:w" s="T939">sestrat</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3469" n="HIAT:w" s="T940">külambi</ts>
                  <nts id="Seg_3470" n="HIAT:ip">.</nts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T943" id="Seg_3472" n="sc" s="T942">
               <ts e="T943" id="Seg_3474" n="HIAT:u" s="T942">
                  <nts id="Seg_3475" n="HIAT:ip">(</nts>
                  <nts id="Seg_3476" n="HIAT:ip">(</nts>
                  <ats e="T943" id="Seg_3477" n="HIAT:non-pho" s="T942">BRK</ats>
                  <nts id="Seg_3478" n="HIAT:ip">)</nts>
                  <nts id="Seg_3479" n="HIAT:ip">)</nts>
                  <nts id="Seg_3480" n="HIAT:ip">.</nts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T949" id="Seg_3482" n="sc" s="T944">
               <ts e="T949" id="Seg_3484" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3486" n="HIAT:w" s="T944">Šödörluʔpiem</ts>
                  <nts id="Seg_3487" n="HIAT:ip">,</nts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3490" n="HIAT:w" s="T945">bar</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3493" n="HIAT:w" s="T946">tüj</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3496" n="HIAT:w" s="T947">naga</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3499" n="HIAT:w" s="T948">šöʔsittə</ts>
                  <nts id="Seg_3500" n="HIAT:ip">.</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T951" id="Seg_3502" n="sc" s="T950">
               <ts e="T951" id="Seg_3504" n="HIAT:u" s="T950">
                  <nts id="Seg_3505" n="HIAT:ip">(</nts>
                  <nts id="Seg_3506" n="HIAT:ip">(</nts>
                  <ats e="T951" id="Seg_3507" n="HIAT:non-pho" s="T950">BRK</ats>
                  <nts id="Seg_3508" n="HIAT:ip">)</nts>
                  <nts id="Seg_3509" n="HIAT:ip">)</nts>
                  <nts id="Seg_3510" n="HIAT:ip">.</nts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T961" id="Seg_3512" n="sc" s="T952">
               <ts e="T961" id="Seg_3514" n="HIAT:u" s="T952">
                  <ts e="T953" id="Seg_3516" n="HIAT:w" s="T952">Miʔ</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3519" n="HIAT:w" s="T953">jelezʼe</ts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3522" n="HIAT:w" s="T954">kambibaʔ</ts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3525" n="HIAT:w" s="T955">bü</ts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3528" n="HIAT:w" s="T956">tažerzittə</ts>
                  <nts id="Seg_3529" n="HIAT:ip">,</nts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3532" n="HIAT:w" s="T957">a</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3535" n="HIAT:w" s="T958">dĭn</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3538" n="HIAT:w" s="T959">bü</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3541" n="HIAT:w" s="T960">ige</ts>
                  <nts id="Seg_3542" n="HIAT:ip">.</nts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T970" id="Seg_3544" n="sc" s="T962">
               <ts e="T970" id="Seg_3546" n="HIAT:u" s="T962">
                  <nts id="Seg_3547" n="HIAT:ip">(</nts>
                  <ts e="T963" id="Seg_3549" n="HIAT:w" s="T962">Nagur</ts>
                  <nts id="Seg_3550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3552" n="HIAT:w" s="T963">vin-</ts>
                  <nts id="Seg_3553" n="HIAT:ip">)</nts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3556" n="HIAT:w" s="T964">Nagur</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3559" n="HIAT:w" s="T965">vedro</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3562" n="HIAT:w" s="T966">deʔpibeʔ</ts>
                  <nts id="Seg_3563" n="HIAT:ip">,</nts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3566" n="HIAT:w" s="T967">i</ts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3569" n="HIAT:w" s="T968">kabarləj</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3572" n="HIAT:w" s="T969">tüj</ts>
                  <nts id="Seg_3573" n="HIAT:ip">.</nts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T972" id="Seg_3575" n="sc" s="T971">
               <ts e="T972" id="Seg_3577" n="HIAT:u" s="T971">
                  <nts id="Seg_3578" n="HIAT:ip">(</nts>
                  <nts id="Seg_3579" n="HIAT:ip">(</nts>
                  <ats e="T972" id="Seg_3580" n="HIAT:non-pho" s="T971">BRK</ats>
                  <nts id="Seg_3581" n="HIAT:ip">)</nts>
                  <nts id="Seg_3582" n="HIAT:ip">)</nts>
                  <nts id="Seg_3583" n="HIAT:ip">.</nts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T977" id="Seg_3585" n="sc" s="T973">
               <ts e="T977" id="Seg_3587" n="HIAT:u" s="T973">
                  <ts e="T974" id="Seg_3589" n="HIAT:w" s="T973">Dʼijegən</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3592" n="HIAT:w" s="T974">šonəgam</ts>
                  <nts id="Seg_3593" n="HIAT:ip">,</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3596" n="HIAT:w" s="T975">dʼije</ts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3599" n="HIAT:w" s="T976">nüjleʔbəm</ts>
                  <nts id="Seg_3600" n="HIAT:ip">.</nts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T982" id="Seg_3602" n="sc" s="T978">
               <ts e="T982" id="Seg_3604" n="HIAT:u" s="T978">
                  <ts e="T979" id="Seg_3606" n="HIAT:w" s="T978">Pagən</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3609" n="HIAT:w" s="T979">šonəgam</ts>
                  <nts id="Seg_3610" n="HIAT:ip">,</nts>
                  <nts id="Seg_3611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3613" n="HIAT:w" s="T980">pa</ts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3616" n="HIAT:w" s="T981">nüjleʔbəm</ts>
                  <nts id="Seg_3617" n="HIAT:ip">.</nts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T987" id="Seg_3619" n="sc" s="T983">
               <ts e="T987" id="Seg_3621" n="HIAT:u" s="T983">
                  <ts e="T984" id="Seg_3623" n="HIAT:w" s="T983">Sʼtʼep</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3626" n="HIAT:w" s="T984">šonəgam</ts>
                  <nts id="Seg_3627" n="HIAT:ip">,</nts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3630" n="HIAT:w" s="T985">sʼtʼep</ts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3633" n="HIAT:w" s="T986">nüjleʔbəm</ts>
                  <nts id="Seg_3634" n="HIAT:ip">.</nts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T992" id="Seg_3636" n="sc" s="T988">
               <ts e="T992" id="Seg_3638" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_3640" n="HIAT:w" s="T988">Măja</ts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3643" n="HIAT:w" s="T989">šonəgam</ts>
                  <nts id="Seg_3644" n="HIAT:ip">,</nts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3647" n="HIAT:w" s="T990">măja</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3650" n="HIAT:w" s="T991">nüjleʔbəm</ts>
                  <nts id="Seg_3651" n="HIAT:ip">.</nts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T998" id="Seg_3653" n="sc" s="T993">
               <ts e="T998" id="Seg_3655" n="HIAT:u" s="T993">
                  <ts e="T994" id="Seg_3657" n="HIAT:w" s="T993">Dʼălam</ts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3660" n="HIAT:w" s="T994">šonəga</ts>
                  <nts id="Seg_3661" n="HIAT:ip">,</nts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3664" n="HIAT:w" s="T995">Dʼălam</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3666" n="HIAT:ip">(</nts>
                  <ts e="T997" id="Seg_3668" n="HIAT:w" s="T996">nujle-</ts>
                  <nts id="Seg_3669" n="HIAT:ip">)</nts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3672" n="HIAT:w" s="T997">nüjleʔbəm</ts>
                  <nts id="Seg_3673" n="HIAT:ip">.</nts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1003" id="Seg_3675" n="sc" s="T999">
               <ts e="T1003" id="Seg_3677" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_3679" n="HIAT:w" s="T999">Turagən</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3682" n="HIAT:w" s="T1000">amnolaʔbə</ts>
                  <nts id="Seg_3683" n="HIAT:ip">,</nts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3686" n="HIAT:w" s="T1001">tura</ts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3689" n="HIAT:w" s="T1002">nüjleʔbəm</ts>
                  <nts id="Seg_3690" n="HIAT:ip">.</nts>
                  <nts id="Seg_3691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1005" id="Seg_3692" n="sc" s="T1004">
               <ts e="T1005" id="Seg_3694" n="HIAT:u" s="T1004">
                  <nts id="Seg_3695" n="HIAT:ip">(</nts>
                  <nts id="Seg_3696" n="HIAT:ip">(</nts>
                  <ats e="T1005" id="Seg_3697" n="HIAT:non-pho" s="T1004">BRK</ats>
                  <nts id="Seg_3698" n="HIAT:ip">)</nts>
                  <nts id="Seg_3699" n="HIAT:ip">)</nts>
                  <nts id="Seg_3700" n="HIAT:ip">.</nts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1016" id="Seg_3702" n="sc" s="T1006">
               <ts e="T1012" id="Seg_3704" n="HIAT:u" s="T1006">
                  <nts id="Seg_3705" n="HIAT:ip">(</nts>
                  <ts e="T1007" id="Seg_3707" n="HIAT:w" s="T1006">Miʔ=</ts>
                  <nts id="Seg_3708" n="HIAT:ip">)</nts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3711" n="HIAT:w" s="T1007">Măn</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3714" n="HIAT:w" s="T1008">turanʼi</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3717" n="HIAT:w" s="T1009">ugandə</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_3720" n="HIAT:w" s="T1010">tumo</ts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_3723" n="HIAT:w" s="T1011">iʔgö</ts>
                  <nts id="Seg_3724" n="HIAT:ip">.</nts>
                  <nts id="Seg_3725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1016" id="Seg_3727" n="HIAT:u" s="T1012">
                  <ts e="T1013" id="Seg_3729" n="HIAT:w" s="T1012">Ĭmbi</ts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3732" n="HIAT:w" s="T1013">păpală</ts>
                  <nts id="Seg_3733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_3735" n="HIAT:w" s="T1014">bar</ts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3738" n="HIAT:w" s="T1015">amniaʔi</ts>
                  <nts id="Seg_3739" n="HIAT:ip">.</nts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1020" id="Seg_3741" n="sc" s="T1017">
               <ts e="T1020" id="Seg_3743" n="HIAT:u" s="T1017">
                  <ts e="T1018" id="Seg_3745" n="HIAT:w" s="T1017">A</ts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_3748" n="HIAT:w" s="T1018">tospak</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_3751" n="HIAT:w" s="T1019">naga</ts>
                  <nts id="Seg_3752" n="HIAT:ip">.</nts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1032" id="Seg_3754" n="sc" s="T1021">
               <ts e="T1032" id="Seg_3756" n="HIAT:u" s="T1021">
                  <ts e="T1022" id="Seg_3758" n="HIAT:w" s="T1021">Kanaʔ</ts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_3761" n="HIAT:w" s="T1022">miʔnenə</ts>
                  <nts id="Seg_3762" n="HIAT:ip">,</nts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3764" n="HIAT:ip">(</nts>
                  <ts e="T1024" id="Seg_3766" n="HIAT:w" s="T1023">dĭ=</ts>
                  <nts id="Seg_3767" n="HIAT:ip">)</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_3770" n="HIAT:w" s="T1024">dĭn</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_3773" n="HIAT:w" s="T1025">šide</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3775" n="HIAT:ip">(</nts>
                  <ts e="T1027" id="Seg_3777" n="HIAT:w" s="T1026">do-</ts>
                  <nts id="Seg_3778" n="HIAT:ip">)</nts>
                  <nts id="Seg_3779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_3781" n="HIAT:w" s="T1027">tospak</ts>
                  <nts id="Seg_3782" n="HIAT:ip">,</nts>
                  <nts id="Seg_3783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_3785" n="HIAT:w" s="T1028">onʼiʔ</ts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_3788" n="HIAT:w" s="T1029">sĭre</ts>
                  <nts id="Seg_3789" n="HIAT:ip">,</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_3792" n="HIAT:w" s="T1030">onʼiʔ</ts>
                  <nts id="Seg_3793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_3795" n="HIAT:w" s="T1031">komə</ts>
                  <nts id="Seg_3796" n="HIAT:ip">.</nts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1036" id="Seg_3798" n="sc" s="T1033">
               <ts e="T1036" id="Seg_3800" n="HIAT:u" s="T1033">
                  <ts e="T1034" id="Seg_3802" n="HIAT:w" s="T1033">Dĭ</ts>
                  <nts id="Seg_3803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_3805" n="HIAT:w" s="T1034">mĭləj</ts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_3808" n="HIAT:w" s="T1035">tănan</ts>
                  <nts id="Seg_3809" n="HIAT:ip">.</nts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1048" id="Seg_3811" n="sc" s="T1037">
               <ts e="T1048" id="Seg_3813" n="HIAT:u" s="T1037">
                  <ts e="T1038" id="Seg_3815" n="HIAT:w" s="T1037">Da</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_3818" n="HIAT:w" s="T1038">dĭ</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_3821" n="HIAT:w" s="T1039">sĭre</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_3824" n="HIAT:w" s="T1040">măna</ts>
                  <nts id="Seg_3825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_3827" n="HIAT:w" s="T1041">ibi</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_3830" n="HIAT:w" s="T1042">da</ts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_3833" n="HIAT:w" s="T1043">kalla</ts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_3836" n="HIAT:w" s="T1181">dʼürbi</ts>
                  <nts id="Seg_3837" n="HIAT:ip">,</nts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_3840" n="HIAT:w" s="T1044">dĭ</ts>
                  <nts id="Seg_3841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_3843" n="HIAT:w" s="T1045">dĭm</ts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_3846" n="HIAT:w" s="T1046">ej</ts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_3849" n="HIAT:w" s="T1047">öʔlübi</ts>
                  <nts id="Seg_3850" n="HIAT:ip">.</nts>
                  <nts id="Seg_3851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1052" id="Seg_3852" n="sc" s="T1049">
               <ts e="T1052" id="Seg_3854" n="HIAT:u" s="T1049">
                  <nts id="Seg_3855" n="HIAT:ip">(</nts>
                  <ts e="T1050" id="Seg_3857" n="HIAT:w" s="T1049">Daška</ts>
                  <nts id="Seg_3858" n="HIAT:ip">)</nts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_3861" n="HIAT:w" s="T1050">ej</ts>
                  <nts id="Seg_3862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_3864" n="HIAT:w" s="T1051">šolia</ts>
                  <nts id="Seg_3865" n="HIAT:ip">.</nts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1054" id="Seg_3867" n="sc" s="T1053">
               <ts e="T1054" id="Seg_3869" n="HIAT:u" s="T1053">
                  <nts id="Seg_3870" n="HIAT:ip">(</nts>
                  <nts id="Seg_3871" n="HIAT:ip">(</nts>
                  <ats e="T1054" id="Seg_3872" n="HIAT:non-pho" s="T1053">BRK</ats>
                  <nts id="Seg_3873" n="HIAT:ip">)</nts>
                  <nts id="Seg_3874" n="HIAT:ip">)</nts>
                  <nts id="Seg_3875" n="HIAT:ip">.</nts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1058" id="Seg_3877" n="sc" s="T1055">
               <ts e="T1058" id="Seg_3879" n="HIAT:u" s="T1055">
                  <ts e="T1056" id="Seg_3881" n="HIAT:w" s="T1055">Măn</ts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_3884" n="HIAT:w" s="T1056">kunolzittə</ts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_3887" n="HIAT:w" s="T1057">iʔpiem</ts>
                  <nts id="Seg_3888" n="HIAT:ip">.</nts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1068" id="Seg_3890" n="sc" s="T1059">
               <ts e="T1068" id="Seg_3892" n="HIAT:u" s="T1059">
                  <ts e="T1060" id="Seg_3894" n="HIAT:w" s="T1059">A</ts>
                  <nts id="Seg_3895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_3897" n="HIAT:w" s="T1060">dĭn</ts>
                  <nts id="Seg_3898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_3900" n="HIAT:w" s="T1061">ugandə</ts>
                  <nts id="Seg_3901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_3903" n="HIAT:w" s="T1062">putʼuga</ts>
                  <nts id="Seg_3904" n="HIAT:ip">,</nts>
                  <nts id="Seg_3905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_3907" n="HIAT:w" s="T1063">kuliam</ts>
                  <nts id="Seg_3908" n="HIAT:ip">,</nts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_3911" n="HIAT:w" s="T1064">tüʔ</ts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3913" n="HIAT:ip">(</nts>
                  <ts e="T1066" id="Seg_3915" n="HIAT:w" s="T1065">ip-</ts>
                  <nts id="Seg_3916" n="HIAT:ip">)</nts>
                  <nts id="Seg_3917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_3919" n="HIAT:w" s="T1066">iʔgö</ts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_3922" n="HIAT:w" s="T1067">iʔbolaʔbə</ts>
                  <nts id="Seg_3923" n="HIAT:ip">.</nts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1070" id="Seg_3925" n="sc" s="T1069">
               <ts e="T1070" id="Seg_3927" n="HIAT:u" s="T1069">
                  <nts id="Seg_3928" n="HIAT:ip">(</nts>
                  <nts id="Seg_3929" n="HIAT:ip">(</nts>
                  <ats e="T1070" id="Seg_3930" n="HIAT:non-pho" s="T1069">BRK</ats>
                  <nts id="Seg_3931" n="HIAT:ip">)</nts>
                  <nts id="Seg_3932" n="HIAT:ip">)</nts>
                  <nts id="Seg_3933" n="HIAT:ip">.</nts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1075" id="Seg_3935" n="sc" s="T1071">
               <ts e="T1075" id="Seg_3937" n="HIAT:u" s="T1071">
                  <ts e="T1072" id="Seg_3939" n="HIAT:w" s="T1071">Ertən</ts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_3942" n="HIAT:w" s="T1072">uʔbdəbiam</ts>
                  <nts id="Seg_3943" n="HIAT:ip">,</nts>
                  <nts id="Seg_3944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_3946" n="HIAT:w" s="T1073">kălenkanə</ts>
                  <nts id="Seg_3947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_3949" n="HIAT:w" s="T1074">nubiam</ts>
                  <nts id="Seg_3950" n="HIAT:ip">.</nts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1082" id="Seg_3952" n="sc" s="T1076">
               <ts e="T1082" id="Seg_3954" n="HIAT:u" s="T1076">
                  <ts e="T1077" id="Seg_3956" n="HIAT:w" s="T1076">Kuliom</ts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_3959" n="HIAT:w" s="T1077">bar:</ts>
                  <nts id="Seg_3960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_3962" n="HIAT:w" s="T1078">dĭn</ts>
                  <nts id="Seg_3963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_3965" n="HIAT:w" s="T1079">bʼeʔ</ts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_3968" n="HIAT:w" s="T1080">kuči</ts>
                  <nts id="Seg_3969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_3971" n="HIAT:w" s="T1081">iʔbolaʔbə</ts>
                  <nts id="Seg_3972" n="HIAT:ip">.</nts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1084" id="Seg_3974" n="sc" s="T1083">
               <ts e="T1084" id="Seg_3976" n="HIAT:u" s="T1083">
                  <nts id="Seg_3977" n="HIAT:ip">(</nts>
                  <nts id="Seg_3978" n="HIAT:ip">(</nts>
                  <ats e="T1084" id="Seg_3979" n="HIAT:non-pho" s="T1083">BRK</ats>
                  <nts id="Seg_3980" n="HIAT:ip">)</nts>
                  <nts id="Seg_3981" n="HIAT:ip">)</nts>
                  <nts id="Seg_3982" n="HIAT:ip">.</nts>
                  <nts id="Seg_3983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1096" id="Seg_3984" n="sc" s="T1085">
               <ts e="T1090" id="Seg_3986" n="HIAT:u" s="T1085">
                  <ts e="T1086" id="Seg_3988" n="HIAT:w" s="T1085">Dĭgəttə</ts>
                  <nts id="Seg_3989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_3991" n="HIAT:w" s="T1086">ujam</ts>
                  <nts id="Seg_3992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_3994" n="HIAT:w" s="T1087">kunəlambam</ts>
                  <nts id="Seg_3995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_3997" n="HIAT:w" s="T1088">niʔtə</ts>
                  <nts id="Seg_3998" n="HIAT:ip">,</nts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_4001" n="HIAT:w" s="T1089">takšegən</ts>
                  <nts id="Seg_4002" n="HIAT:ip">.</nts>
                  <nts id="Seg_4003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1096" id="Seg_4005" n="HIAT:u" s="T1090">
                  <nts id="Seg_4006" n="HIAT:ip">(</nts>
                  <ts e="T1091" id="Seg_4008" n="HIAT:w" s="T1090">Takšezi</ts>
                  <nts id="Seg_4009" n="HIAT:ip">)</nts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_4012" n="HIAT:w" s="T1091">kajliam</ts>
                  <nts id="Seg_4013" n="HIAT:ip">,</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_4016" n="HIAT:w" s="T1092">štobɨ</ts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_4019" n="HIAT:w" s="T1093">tumoʔi</ts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_4022" n="HIAT:w" s="T1094">ej</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_4025" n="HIAT:w" s="T1095">ambiʔi</ts>
                  <nts id="Seg_4026" n="HIAT:ip">.</nts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1107" id="Seg_4028" n="sc" s="T1097">
               <ts e="T1107" id="Seg_4030" n="HIAT:u" s="T1097">
                  <ts e="T1098" id="Seg_4032" n="HIAT:w" s="T1097">A</ts>
                  <nts id="Seg_4033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_4035" n="HIAT:w" s="T1098">kajaʔ</ts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_4038" n="HIAT:w" s="T1099">măslăbojkănə</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_4041" n="HIAT:w" s="T1100">tože</ts>
                  <nts id="Seg_4042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4043" n="HIAT:ip">(</nts>
                  <ts e="T1102" id="Seg_4045" n="HIAT:w" s="T1101">ka-</ts>
                  <nts id="Seg_4046" n="HIAT:ip">)</nts>
                  <nts id="Seg_4047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_4049" n="HIAT:w" s="T1102">kajlam</ts>
                  <nts id="Seg_4050" n="HIAT:ip">,</nts>
                  <nts id="Seg_4051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_4053" n="HIAT:w" s="T1103">štobɨ</ts>
                  <nts id="Seg_4054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_4056" n="HIAT:w" s="T1104">tumoʔi</ts>
                  <nts id="Seg_4057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_4059" n="HIAT:w" s="T1105">ej</ts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_4062" n="HIAT:w" s="T1106">ambiʔi</ts>
                  <nts id="Seg_4063" n="HIAT:ip">.</nts>
                  <nts id="Seg_4064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1109" id="Seg_4065" n="sc" s="T1108">
               <ts e="T1109" id="Seg_4067" n="HIAT:u" s="T1108">
                  <nts id="Seg_4068" n="HIAT:ip">(</nts>
                  <nts id="Seg_4069" n="HIAT:ip">(</nts>
                  <ats e="T1109" id="Seg_4070" n="HIAT:non-pho" s="T1108">BRK</ats>
                  <nts id="Seg_4071" n="HIAT:ip">)</nts>
                  <nts id="Seg_4072" n="HIAT:ip">)</nts>
                  <nts id="Seg_4073" n="HIAT:ip">.</nts>
                  <nts id="Seg_4074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1119" id="Seg_4075" n="sc" s="T1110">
               <ts e="T1119" id="Seg_4077" n="HIAT:u" s="T1110">
                  <ts e="T1111" id="Seg_4079" n="HIAT:w" s="T1110">A</ts>
                  <nts id="Seg_4080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_4082" n="HIAT:w" s="T1111">xoš</ts>
                  <nts id="Seg_4083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_4085" n="HIAT:w" s="T1112">dăk</ts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_4088" n="HIAT:w" s="T1113">miʔnʼibeʔ</ts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_4091" n="HIAT:w" s="T1114">šide</ts>
                  <nts id="Seg_4092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_4094" n="HIAT:w" s="T1115">ige</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4096" n="HIAT:ip">(</nts>
                  <ts e="T1117" id="Seg_4098" n="HIAT:w" s="T1116">tospak</ts>
                  <nts id="Seg_4099" n="HIAT:ip">)</nts>
                  <nts id="Seg_4100" n="HIAT:ip">,</nts>
                  <nts id="Seg_4101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1118" id="Seg_4103" n="HIAT:w" s="T1117">detlem</ts>
                  <nts id="Seg_4104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_4106" n="HIAT:w" s="T1118">onʼiʔ</ts>
                  <nts id="Seg_4107" n="HIAT:ip">.</nts>
                  <nts id="Seg_4108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1126" id="Seg_4109" n="sc" s="T1120">
               <ts e="T1126" id="Seg_4111" n="HIAT:u" s="T1120">
                  <ts e="T1121" id="Seg_4113" n="HIAT:w" s="T1120">Girgit</ts>
                  <nts id="Seg_4114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4115" n="HIAT:ip">(</nts>
                  <ts e="T1122" id="Seg_4117" n="HIAT:w" s="T1121">tănan</ts>
                  <nts id="Seg_4118" n="HIAT:ip">)</nts>
                  <nts id="Seg_4119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_4121" n="HIAT:w" s="T1122">deʔsittə</ts>
                  <nts id="Seg_4122" n="HIAT:ip">,</nts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_4125" n="HIAT:w" s="T1123">sĭre</ts>
                  <nts id="Seg_4126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1125" id="Seg_4128" n="HIAT:w" s="T1124">ali</ts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_4131" n="HIAT:w" s="T1125">komə</ts>
                  <nts id="Seg_4132" n="HIAT:ip">?</nts>
                  <nts id="Seg_4133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1130" id="Seg_4134" n="sc" s="T1127">
               <ts e="T1130" id="Seg_4136" n="HIAT:u" s="T1127">
                  <nts id="Seg_4137" n="HIAT:ip">"</nts>
                  <ts e="T1128" id="Seg_4139" n="HIAT:w" s="T1127">Nu</ts>
                  <nts id="Seg_4140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_4142" n="HIAT:w" s="T1128">deʔ</ts>
                  <nts id="Seg_4143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_4145" n="HIAT:w" s="T1129">komə</ts>
                  <nts id="Seg_4146" n="HIAT:ip">"</nts>
                  <nts id="Seg_4147" n="HIAT:ip">.</nts>
                  <nts id="Seg_4148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1132" id="Seg_4149" n="sc" s="T1131">
               <ts e="T1132" id="Seg_4151" n="HIAT:u" s="T1131">
                  <nts id="Seg_4152" n="HIAT:ip">(</nts>
                  <nts id="Seg_4153" n="HIAT:ip">(</nts>
                  <ats e="T1132" id="Seg_4154" n="HIAT:non-pho" s="T1131">BRK</ats>
                  <nts id="Seg_4155" n="HIAT:ip">)</nts>
                  <nts id="Seg_4156" n="HIAT:ip">)</nts>
                  <nts id="Seg_4157" n="HIAT:ip">.</nts>
                  <nts id="Seg_4158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1139" id="Seg_4159" n="sc" s="T1133">
               <ts e="T1139" id="Seg_4161" n="HIAT:u" s="T1133">
                  <ts e="T1134" id="Seg_4163" n="HIAT:w" s="T1133">Moltʼanə</ts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_4166" n="HIAT:w" s="T1134">šoga</ts>
                  <nts id="Seg_4167" n="HIAT:ip">,</nts>
                  <nts id="Seg_4168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1136" id="Seg_4170" n="HIAT:w" s="T1135">dĭn</ts>
                  <nts id="Seg_4171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_4173" n="HIAT:w" s="T1136">ugandə</ts>
                  <nts id="Seg_4174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_4176" n="HIAT:w" s="T1137">bü</ts>
                  <nts id="Seg_4177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_4179" n="HIAT:w" s="T1138">iʔgö</ts>
                  <nts id="Seg_4180" n="HIAT:ip">.</nts>
                  <nts id="Seg_4181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1146" id="Seg_4182" n="sc" s="T1140">
               <ts e="T1146" id="Seg_4184" n="HIAT:u" s="T1140">
                  <ts e="T1141" id="Seg_4186" n="HIAT:w" s="T1140">Dʼibige</ts>
                  <nts id="Seg_4187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1142" id="Seg_4189" n="HIAT:w" s="T1141">bü</ts>
                  <nts id="Seg_4190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1143" id="Seg_4192" n="HIAT:w" s="T1142">ige</ts>
                  <nts id="Seg_4193" n="HIAT:ip">,</nts>
                  <nts id="Seg_4194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_4196" n="HIAT:w" s="T1143">šišege</ts>
                  <nts id="Seg_4197" n="HIAT:ip">,</nts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_4200" n="HIAT:w" s="T1144">sabən</ts>
                  <nts id="Seg_4201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_4203" n="HIAT:w" s="T1145">ige</ts>
                  <nts id="Seg_4204" n="HIAT:ip">.</nts>
                  <nts id="Seg_4205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1150" id="Seg_4206" n="sc" s="T1147">
               <ts e="T1150" id="Seg_4208" n="HIAT:u" s="T1147">
                  <ts e="T1148" id="Seg_4210" n="HIAT:w" s="T1147">Ejü</ts>
                  <nts id="Seg_4211" n="HIAT:ip">,</nts>
                  <nts id="Seg_4212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4213" n="HIAT:ip">(</nts>
                  <ts e="T1149" id="Seg_4215" n="HIAT:w" s="T1148">ejümnel</ts>
                  <nts id="Seg_4216" n="HIAT:ip">)</nts>
                  <nts id="Seg_4217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_4219" n="HIAT:w" s="T1149">bar</ts>
                  <nts id="Seg_4220" n="HIAT:ip">.</nts>
                  <nts id="Seg_4221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1152" id="Seg_4222" n="sc" s="T1151">
               <ts e="T1152" id="Seg_4224" n="HIAT:u" s="T1151">
                  <nts id="Seg_4225" n="HIAT:ip">(</nts>
                  <nts id="Seg_4226" n="HIAT:ip">(</nts>
                  <ats e="T1152" id="Seg_4227" n="HIAT:non-pho" s="T1151">BRK</ats>
                  <nts id="Seg_4228" n="HIAT:ip">)</nts>
                  <nts id="Seg_4229" n="HIAT:ip">)</nts>
                  <nts id="Seg_4230" n="HIAT:ip">.</nts>
                  <nts id="Seg_4231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1159" id="Seg_4232" n="sc" s="T1153">
               <ts e="T1159" id="Seg_4234" n="HIAT:u" s="T1153">
                  <ts e="T1154" id="Seg_4236" n="HIAT:w" s="T1153">Püjel</ts>
                  <nts id="Seg_4237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_4239" n="HIAT:w" s="T1154">ej</ts>
                  <nts id="Seg_4240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_4242" n="HIAT:w" s="T1155">ĭzemne</ts>
                  <nts id="Seg_4243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_4245" n="HIAT:w" s="T1156">i</ts>
                  <nts id="Seg_4246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1158" id="Seg_4248" n="HIAT:w" s="T1157">kokluʔi</ts>
                  <nts id="Seg_4249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_4251" n="HIAT:w" s="T1158">mʼaŋnaʔi</ts>
                  <nts id="Seg_4252" n="HIAT:ip">.</nts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1161" id="Seg_4254" n="sc" s="T1160">
               <ts e="T1161" id="Seg_4256" n="HIAT:u" s="T1160">
                  <nts id="Seg_4257" n="HIAT:ip">(</nts>
                  <nts id="Seg_4258" n="HIAT:ip">(</nts>
                  <ats e="T1161" id="Seg_4259" n="HIAT:non-pho" s="T1160">BRK</ats>
                  <nts id="Seg_4260" n="HIAT:ip">)</nts>
                  <nts id="Seg_4261" n="HIAT:ip">)</nts>
                  <nts id="Seg_4262" n="HIAT:ip">.</nts>
                  <nts id="Seg_4263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1168" id="Seg_4264" n="sc" s="T1162">
               <ts e="T1168" id="Seg_4266" n="HIAT:u" s="T1162">
                  <ts e="T1163" id="Seg_4268" n="HIAT:w" s="T1162">Dö</ts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_4271" n="HIAT:w" s="T1163">zaplatka</ts>
                  <nts id="Seg_4272" n="HIAT:ip">,</nts>
                  <nts id="Seg_4273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1165" id="Seg_4275" n="HIAT:w" s="T1164">a</ts>
                  <nts id="Seg_4276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1166" id="Seg_4278" n="HIAT:w" s="T1165">gibər</ts>
                  <nts id="Seg_4279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_4281" n="HIAT:w" s="T1166">dĭm</ts>
                  <nts id="Seg_4282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_4284" n="HIAT:w" s="T1167">zaplatka</ts>
                  <nts id="Seg_4285" n="HIAT:ip">.</nts>
                  <nts id="Seg_4286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1172" id="Seg_4287" n="sc" s="T1169">
               <ts e="T1172" id="Seg_4289" n="HIAT:u" s="T1169">
                  <ts e="T1170" id="Seg_4291" n="HIAT:w" s="T1169">Kötengəndə</ts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171" id="Seg_4294" n="HIAT:w" s="T1170">amnolzittə</ts>
                  <nts id="Seg_4295" n="HIAT:ip">,</nts>
                  <nts id="Seg_4296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171.tx.1" id="Seg_4298" n="HIAT:w" s="T1171">što</ts>
                  <nts id="Seg_4299" n="HIAT:ip">_</nts>
                  <ts e="T1172" id="Seg_4301" n="HIAT:w" s="T1171.tx.1">li</ts>
                  <nts id="Seg_4302" n="HIAT:ip">.</nts>
                  <nts id="Seg_4303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1177" id="Seg_4304" n="sc" s="T1173">
               <ts e="T1177" id="Seg_4306" n="HIAT:u" s="T1173">
                  <ts e="T1174" id="Seg_4308" n="HIAT:w" s="T1173">Barəbtɨʔ</ts>
                  <nts id="Seg_4309" n="HIAT:ip">,</nts>
                  <nts id="Seg_4310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_4312" n="HIAT:w" s="T1174">ej</ts>
                  <nts id="Seg_4313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_4315" n="HIAT:w" s="T1175">kereʔ</ts>
                  <nts id="Seg_4316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1177" id="Seg_4318" n="HIAT:w" s="T1176">măna</ts>
                  <nts id="Seg_4319" n="HIAT:ip">.</nts>
                  <nts id="Seg_4320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T5" id="Seg_4321" n="sc" s="T1">
               <ts e="T2" id="Seg_4323" n="e" s="T1">Măn </ts>
               <ts e="T3" id="Seg_4325" n="e" s="T2">taldʼen </ts>
               <ts e="T4" id="Seg_4327" n="e" s="T3">kambiam </ts>
               <ts e="T5" id="Seg_4329" n="e" s="T4">tuganbə. </ts>
            </ts>
            <ts e="T14" id="Seg_4330" n="sc" s="T6">
               <ts e="T7" id="Seg_4332" n="e" s="T6">(Măn) </ts>
               <ts e="T8" id="Seg_4334" n="e" s="T7">mămbiam: </ts>
               <ts e="T9" id="Seg_4336" n="e" s="T8">"Iʔ </ts>
               <ts e="T10" id="Seg_4338" n="e" s="T9">kuroʔ", </ts>
               <ts e="T11" id="Seg_4340" n="e" s="T10">măna </ts>
               <ts e="T12" id="Seg_4342" n="e" s="T11">nelʼzʼa </ts>
               <ts e="T13" id="Seg_4344" n="e" s="T12">bɨlo </ts>
               <ts e="T14" id="Seg_4346" n="e" s="T13">šoʔsittə. </ts>
            </ts>
            <ts e="T22" id="Seg_4347" n="sc" s="T15">
               <ts e="T16" id="Seg_4349" n="e" s="T15">Meim </ts>
               <ts e="T17" id="Seg_4351" n="e" s="T16">ej </ts>
               <ts e="T18" id="Seg_4353" n="e" s="T17">kambi, </ts>
               <ts e="T19" id="Seg_4355" n="e" s="T18">i </ts>
               <ts e="T20" id="Seg_4357" n="e" s="T19">măn </ts>
               <ts e="T21" id="Seg_4359" n="e" s="T20">ej </ts>
               <ts e="T22" id="Seg_4361" n="e" s="T21">kambiam. </ts>
            </ts>
            <ts e="T27" id="Seg_4362" n="sc" s="T23">
               <ts e="T24" id="Seg_4364" n="e" s="T23">Dĭzeŋ </ts>
               <ts e="T25" id="Seg_4366" n="e" s="T24">krăvatʼkən </ts>
               <ts e="T26" id="Seg_4368" n="e" s="T25">iʔbolaʔbəʔjə, </ts>
               <ts e="T27" id="Seg_4370" n="e" s="T26">kamrolaʔbəʔjə. </ts>
            </ts>
            <ts e="T30" id="Seg_4371" n="sc" s="T28">
               <ts e="T29" id="Seg_4373" n="e" s="T28">Kolʼa </ts>
               <ts e="T30" id="Seg_4375" n="e" s="T29">Dunʼatsi. </ts>
            </ts>
            <ts e="T32" id="Seg_4376" n="sc" s="T31">
               <ts e="T32" id="Seg_4378" n="e" s="T31">Pănarlaʔbəʔjə. </ts>
            </ts>
            <ts e="T34" id="Seg_4379" n="sc" s="T33">
               <ts e="T34" id="Seg_4381" n="e" s="T33">Kaknarlaʔbəʔjə. </ts>
            </ts>
            <ts e="T39" id="Seg_4382" n="sc" s="T35">
               <ts e="T36" id="Seg_4384" n="e" s="T35">Măndəʔi: </ts>
               <ts e="T37" id="Seg_4386" n="e" s="T36">"Urgaja, </ts>
               <ts e="T38" id="Seg_4388" n="e" s="T37">amaʔ, </ts>
               <ts e="T39" id="Seg_4390" n="e" s="T38">amnaʔ. </ts>
            </ts>
            <ts e="T42" id="Seg_4391" n="sc" s="T40">
               <ts e="T41" id="Seg_4393" n="e" s="T40">Ara </ts>
               <ts e="T42" id="Seg_4395" n="e" s="T41">bĭdeʔ". </ts>
            </ts>
            <ts e="T47" id="Seg_4396" n="sc" s="T43">
               <ts e="T44" id="Seg_4398" n="e" s="T43">Măn </ts>
               <ts e="T45" id="Seg_4400" n="e" s="T44">măndəm: </ts>
               <ts e="T46" id="Seg_4402" n="e" s="T45">"Ej </ts>
               <ts e="T47" id="Seg_4404" n="e" s="T46">bĭtləm". </ts>
            </ts>
            <ts e="T52" id="Seg_4405" n="sc" s="T48">
               <ts e="T49" id="Seg_4407" n="e" s="T48">I </ts>
               <ts e="T50" id="Seg_4409" n="e" s="T49">dĭgəttə </ts>
               <ts e="T1178" id="Seg_4411" n="e" s="T50">kalla </ts>
               <ts e="T51" id="Seg_4413" n="e" s="T1178">dʼürbiem </ts>
               <ts e="T52" id="Seg_4415" n="e" s="T51">maʔnʼi. </ts>
            </ts>
            <ts e="T54" id="Seg_4416" n="sc" s="T53">
               <ts e="T54" id="Seg_4418" n="e" s="T53">((BRK)). </ts>
            </ts>
            <ts e="T61" id="Seg_4419" n="sc" s="T55">
               <ts e="T56" id="Seg_4421" n="e" s="T55">Măn </ts>
               <ts e="T57" id="Seg_4423" n="e" s="T56">teinen </ts>
               <ts e="T58" id="Seg_4425" n="e" s="T57">nükenə </ts>
               <ts e="T59" id="Seg_4427" n="e" s="T58">kambiam, </ts>
               <ts e="T60" id="Seg_4429" n="e" s="T59">dĭ </ts>
               <ts e="T61" id="Seg_4431" n="e" s="T60">ĭzemnie. </ts>
            </ts>
            <ts e="T78" id="Seg_4432" n="sc" s="T62">
               <ts e="T63" id="Seg_4434" n="e" s="T62">Dĭ </ts>
               <ts e="T64" id="Seg_4436" n="e" s="T63">măndə: </ts>
               <ts e="T65" id="Seg_4438" n="e" s="T64">"Ĭmbi </ts>
               <ts e="T66" id="Seg_4440" n="e" s="T65">kondʼo </ts>
               <ts e="T67" id="Seg_4442" n="e" s="T66">ej </ts>
               <ts e="T68" id="Seg_4444" n="e" s="T67">šobiam, </ts>
               <ts e="T69" id="Seg_4446" n="e" s="T68">măn </ts>
               <ts e="T70" id="Seg_4448" n="e" s="T69">bar </ts>
               <ts e="T71" id="Seg_4450" n="e" s="T70">kulandəgam". </ts>
               <ts e="T72" id="Seg_4452" n="e" s="T71">"Tăn </ts>
               <ts e="T73" id="Seg_4454" n="e" s="T72">üge </ts>
               <ts e="T74" id="Seg_4456" n="e" s="T73">kulandəgal, </ts>
               <ts e="T75" id="Seg_4458" n="e" s="T74">a </ts>
               <ts e="T76" id="Seg_4460" n="e" s="T75">küsʼtə </ts>
               <ts e="T77" id="Seg_4462" n="e" s="T76">ej </ts>
               <ts e="T78" id="Seg_4464" n="e" s="T77">molial. </ts>
            </ts>
            <ts e="T81" id="Seg_4465" n="sc" s="T79">
               <ts e="T80" id="Seg_4467" n="e" s="T79">No, </ts>
               <ts e="T81" id="Seg_4469" n="e" s="T80">küʔ. </ts>
            </ts>
            <ts e="T91" id="Seg_4470" n="sc" s="T82">
               <ts e="T83" id="Seg_4472" n="e" s="T82">Măna </ts>
               <ts e="T84" id="Seg_4474" n="e" s="T83">ĭmbi, </ts>
               <ts e="T85" id="Seg_4476" n="e" s="T84">(m-) </ts>
               <ts e="T86" id="Seg_4478" n="e" s="T85">ĭmbi </ts>
               <ts e="T87" id="Seg_4480" n="e" s="T86">măn </ts>
               <ts e="T88" id="Seg_4482" n="e" s="T87">tăn </ts>
               <ts e="T89" id="Seg_4484" n="e" s="T88">tondə </ts>
               <ts e="T90" id="Seg_4486" n="e" s="T89">amnolaʔləm </ts>
               <ts e="T91" id="Seg_4488" n="e" s="T90">što_li?" </ts>
            </ts>
            <ts e="T95" id="Seg_4489" n="sc" s="T92">
               <ts e="T93" id="Seg_4491" n="e" s="T92">Măn </ts>
               <ts e="T1185" id="Seg_4493" n="e" s="T93">vedʼ </ts>
               <ts e="T94" id="Seg_4495" n="e" s="T1185">jilə </ts>
               <ts e="T95" id="Seg_4497" n="e" s="T94">amnoʔlaʔbəʔjə. </ts>
            </ts>
            <ts e="T99" id="Seg_4498" n="sc" s="T96">
               <ts e="T97" id="Seg_4500" n="e" s="T96">Dĭn </ts>
               <ts e="T98" id="Seg_4502" n="e" s="T97">edəʔleʔbəʔjə </ts>
               <ts e="T99" id="Seg_4504" n="e" s="T98">măna. </ts>
            </ts>
            <ts e="T102" id="Seg_4505" n="sc" s="T100">
               <ts e="T101" id="Seg_4507" n="e" s="T100">Dʼăbaktərzittə </ts>
               <ts e="T102" id="Seg_4509" n="e" s="T101">nada. </ts>
            </ts>
            <ts e="T107" id="Seg_4510" n="sc" s="T103">
               <ts e="T104" id="Seg_4512" n="e" s="T103">A </ts>
               <ts e="T105" id="Seg_4514" n="e" s="T104">tăn </ts>
               <ts e="T106" id="Seg_4516" n="e" s="T105">tondə </ts>
               <ts e="T107" id="Seg_4518" n="e" s="T106">amnoʔ". </ts>
            </ts>
            <ts e="T109" id="Seg_4519" n="sc" s="T108">
               <ts e="T109" id="Seg_4521" n="e" s="T108">((BRK)). </ts>
            </ts>
            <ts e="T115" id="Seg_4522" n="sc" s="T110">
               <ts e="T111" id="Seg_4524" n="e" s="T110">Meim </ts>
               <ts e="T112" id="Seg_4526" n="e" s="T111">(š-) </ts>
               <ts e="T113" id="Seg_4528" n="e" s="T112">šide </ts>
               <ts e="T114" id="Seg_4530" n="e" s="T113">dʼala </ts>
               <ts e="T115" id="Seg_4532" n="e" s="T114">ibi. </ts>
            </ts>
            <ts e="T120" id="Seg_4533" n="sc" s="T116">
               <ts e="T117" id="Seg_4535" n="e" s="T116">A </ts>
               <ts e="T118" id="Seg_4537" n="e" s="T117">tăn </ts>
               <ts e="T119" id="Seg_4539" n="e" s="T118">tondə </ts>
               <ts e="T120" id="Seg_4541" n="e" s="T119">amnoʔ. </ts>
            </ts>
            <ts e="T124" id="Seg_4542" n="sc" s="T121">
               <ts e="T122" id="Seg_4544" n="e" s="T121">Da </ts>
               <ts e="T123" id="Seg_4546" n="e" s="T122">jil </ts>
               <ts e="T124" id="Seg_4548" n="e" s="T123">mĭŋgəʔi". </ts>
            </ts>
            <ts e="T131" id="Seg_4549" n="sc" s="T125">
               <ts e="T126" id="Seg_4551" n="e" s="T125">A </ts>
               <ts e="T127" id="Seg_4553" n="e" s="T126">dĭ </ts>
               <ts e="T128" id="Seg_4555" n="e" s="T127">măndə: </ts>
               <ts e="T129" id="Seg_4557" n="e" s="T128">"A </ts>
               <ts e="T130" id="Seg_4559" n="e" s="T129">kudaj </ts>
               <ts e="T131" id="Seg_4561" n="e" s="T130">dĭzəŋzi". </ts>
            </ts>
            <ts e="T136" id="Seg_4562" n="sc" s="T132">
               <ts e="T133" id="Seg_4564" n="e" s="T132">"A </ts>
               <ts e="T134" id="Seg_4566" n="e" s="T133">tănzi </ts>
               <ts e="T135" id="Seg_4568" n="e" s="T134">pušaj </ts>
               <ts e="T136" id="Seg_4570" n="e" s="T135">kudaj! </ts>
            </ts>
            <ts e="T139" id="Seg_4571" n="sc" s="T137">
               <ts e="T138" id="Seg_4573" n="e" s="T137">(Küʔ=) </ts>
               <ts e="T139" id="Seg_4575" n="e" s="T138">Küʔ". </ts>
            </ts>
            <ts e="T141" id="Seg_4576" n="sc" s="T140">
               <ts e="T141" id="Seg_4578" n="e" s="T140">((BRK)). </ts>
            </ts>
            <ts e="T149" id="Seg_4579" n="sc" s="T142">
               <ts e="T143" id="Seg_4581" n="e" s="T142">Dĭgəttə </ts>
               <ts e="T144" id="Seg_4583" n="e" s="T143">dĭ </ts>
               <ts e="T145" id="Seg_4585" n="e" s="T144">(malubi), </ts>
               <ts e="T146" id="Seg_4587" n="e" s="T145">a </ts>
               <ts e="T147" id="Seg_4589" n="e" s="T146">măn </ts>
               <ts e="T148" id="Seg_4591" n="e" s="T147">maʔnʼi </ts>
               <ts e="T1179" id="Seg_4593" n="e" s="T148">kalla </ts>
               <ts e="T149" id="Seg_4595" n="e" s="T1179">dʼürbiem. </ts>
            </ts>
            <ts e="T151" id="Seg_4596" n="sc" s="T150">
               <ts e="T151" id="Seg_4598" n="e" s="T150">((BRK)). </ts>
            </ts>
            <ts e="T155" id="Seg_4599" n="sc" s="T152">
               <ts e="T153" id="Seg_4601" n="e" s="T152">Măn </ts>
               <ts e="T1183" id="Seg_4603" n="e" s="T153">Kazan </ts>
               <ts e="T154" id="Seg_4605" n="e" s="T1183">turanə </ts>
               <ts e="T155" id="Seg_4607" n="e" s="T154">kambiam. </ts>
            </ts>
            <ts e="T165" id="Seg_4608" n="sc" s="T156">
               <ts e="T157" id="Seg_4610" n="e" s="T156">A </ts>
               <ts e="T158" id="Seg_4612" n="e" s="T157">dĭ </ts>
               <ts e="T159" id="Seg_4614" n="e" s="T158">nükə </ts>
               <ts e="T160" id="Seg_4616" n="e" s="T159">kambi, </ts>
               <ts e="T161" id="Seg_4618" n="e" s="T160">dĭn </ts>
               <ts e="T162" id="Seg_4620" n="e" s="T161">tondə </ts>
               <ts e="T163" id="Seg_4622" n="e" s="T162">dĭn </ts>
               <ts e="T164" id="Seg_4624" n="e" s="T163">ne </ts>
               <ts e="T165" id="Seg_4626" n="e" s="T164">amnolaʔbə. </ts>
            </ts>
            <ts e="T170" id="Seg_4627" n="sc" s="T166">
               <ts e="T167" id="Seg_4629" n="e" s="T166">Šobi </ts>
               <ts e="T168" id="Seg_4631" n="e" s="T167">(dam) </ts>
               <ts e="T169" id="Seg_4633" n="e" s="T168">da </ts>
               <ts e="T1180" id="Seg_4635" n="e" s="T169">kalla </ts>
               <ts e="T170" id="Seg_4637" n="e" s="T1180">dʼürbi. </ts>
            </ts>
            <ts e="T178" id="Seg_4638" n="sc" s="T171">
               <ts e="T172" id="Seg_4640" n="e" s="T171">"Măn </ts>
               <ts e="T173" id="Seg_4642" n="e" s="T172">külam, </ts>
               <ts e="T174" id="Seg_4644" n="e" s="T173">a </ts>
               <ts e="T175" id="Seg_4646" n="e" s="T174">šində </ts>
               <ts e="T176" id="Seg_4648" n="e" s="T175">măna </ts>
               <ts e="T177" id="Seg_4650" n="e" s="T176">(bĭzdl-) </ts>
               <ts e="T178" id="Seg_4652" n="e" s="T177">bĭzəjdləj?" </ts>
            </ts>
            <ts e="T183" id="Seg_4653" n="sc" s="T179">
               <ts e="T180" id="Seg_4655" n="e" s="T179">Dĭzeŋ </ts>
               <ts e="T181" id="Seg_4657" n="e" s="T180">bar </ts>
               <ts e="T182" id="Seg_4659" n="e" s="T181">kaknarbiʔi </ts>
               <ts e="T183" id="Seg_4661" n="e" s="T182">tăŋ. </ts>
            </ts>
            <ts e="T196" id="Seg_4662" n="sc" s="T184">
               <ts e="T185" id="Seg_4664" n="e" s="T184">Dĭgəttə </ts>
               <ts e="T186" id="Seg_4666" n="e" s="T185">măn </ts>
               <ts e="T187" id="Seg_4668" n="e" s="T186">sobiam, </ts>
               <ts e="T188" id="Seg_4670" n="e" s="T187">kambiam </ts>
               <ts e="T189" id="Seg_4672" n="e" s="T188">dĭʔnə. </ts>
               <ts e="T190" id="Seg_4674" n="e" s="T189">Tăn </ts>
               <ts e="T1182" id="Seg_4676" n="e" s="T190">kalla </ts>
               <ts e="T191" id="Seg_4678" n="e" s="T1182">dʼürbiem, </ts>
               <ts e="T192" id="Seg_4680" n="e" s="T191">a </ts>
               <ts e="T193" id="Seg_4682" n="e" s="T192">măn </ts>
               <ts e="T194" id="Seg_4684" n="e" s="T193">küllambiam </ts>
               <ts e="T195" id="Seg_4686" n="e" s="T194">i </ts>
               <ts e="T196" id="Seg_4688" n="e" s="T195">kannambiam. </ts>
            </ts>
            <ts e="T202" id="Seg_4689" n="sc" s="T197">
               <ts e="T198" id="Seg_4691" n="e" s="T197">A </ts>
               <ts e="T199" id="Seg_4693" n="e" s="T198">măn </ts>
               <ts e="T200" id="Seg_4695" n="e" s="T199">mămbiam: </ts>
               <ts e="T201" id="Seg_4697" n="e" s="T200">"Nu, </ts>
               <ts e="T202" id="Seg_4699" n="e" s="T201">kănnaʔ. </ts>
            </ts>
            <ts e="T213" id="Seg_4700" n="sc" s="T203">
               <ts e="T204" id="Seg_4702" n="e" s="T203">Ĭmbi, </ts>
               <ts e="T205" id="Seg_4704" n="e" s="T204">(i </ts>
               <ts e="T206" id="Seg_4706" n="e" s="T205">kl-) </ts>
               <ts e="T207" id="Seg_4708" n="e" s="T206">i </ts>
               <ts e="T208" id="Seg_4710" n="e" s="T207">küʔ. </ts>
               <ts e="T209" id="Seg_4712" n="e" s="T208">(Mɨ </ts>
               <ts e="T210" id="Seg_4714" n="e" s="T209">bɨ) </ts>
               <ts e="T211" id="Seg_4716" n="e" s="T210">Măn </ts>
               <ts e="T212" id="Seg_4718" n="e" s="T211">bü </ts>
               <ts e="T213" id="Seg_4720" n="e" s="T212">ejümləm. </ts>
            </ts>
            <ts e="T218" id="Seg_4721" n="sc" s="T214">
               <ts e="T215" id="Seg_4723" n="e" s="T214">I </ts>
               <ts e="T216" id="Seg_4725" n="e" s="T215">tănan </ts>
               <ts e="T217" id="Seg_4727" n="e" s="T216">(kămnal-) </ts>
               <ts e="T218" id="Seg_4729" n="e" s="T217">kămlal. </ts>
            </ts>
            <ts e="T227" id="Seg_4730" n="sc" s="T219">
               <ts e="T220" id="Seg_4732" n="e" s="T219">Dĭgəttə </ts>
               <ts e="T221" id="Seg_4734" n="e" s="T220">bĭzəlim, </ts>
               <ts e="T222" id="Seg_4736" n="e" s="T221">i </ts>
               <ts e="T223" id="Seg_4738" n="e" s="T222">(dʼunə </ts>
               <ts e="T224" id="Seg_4740" n="e" s="T223">tĭl- </ts>
               <ts e="T225" id="Seg_4742" n="e" s="T224">tĭl-) </ts>
               <ts e="T226" id="Seg_4744" n="e" s="T225">dʼünə </ts>
               <ts e="T227" id="Seg_4746" n="e" s="T226">enləbim. </ts>
            </ts>
            <ts e="T229" id="Seg_4747" n="sc" s="T228">
               <ts e="T229" id="Seg_4749" n="e" s="T228">((BRK)). </ts>
            </ts>
            <ts e="T237" id="Seg_4750" n="sc" s="T230">
               <ts e="T231" id="Seg_4752" n="e" s="T230">Как </ts>
               <ts e="T232" id="Seg_4754" n="e" s="T231">я </ts>
               <ts e="T233" id="Seg_4756" n="e" s="T232">говорила, </ts>
               <ts e="T234" id="Seg_4758" n="e" s="T233">погоди, </ts>
               <ts e="T235" id="Seg_4760" n="e" s="T234">как </ts>
               <ts e="T236" id="Seg_4762" n="e" s="T235">я </ts>
               <ts e="T237" id="Seg_4764" n="e" s="T236">начала? </ts>
            </ts>
            <ts e="T239" id="Seg_4765" n="sc" s="T238">
               <ts e="T239" id="Seg_4767" n="e" s="T238">((BRK)). </ts>
            </ts>
            <ts e="T248" id="Seg_4768" n="sc" s="T240">
               <ts e="T241" id="Seg_4770" n="e" s="T240">Dĭ </ts>
               <ts e="T242" id="Seg_4772" n="e" s="T241">nüke </ts>
               <ts e="T243" id="Seg_4774" n="e" s="T242">bar </ts>
               <ts e="T244" id="Seg_4776" n="e" s="T243">măna: </ts>
               <ts e="T245" id="Seg_4778" n="e" s="T244">"Nendeʔ </ts>
               <ts e="T246" id="Seg_4780" n="e" s="T245">moltʼa, </ts>
               <ts e="T247" id="Seg_4782" n="e" s="T246">bĭzaʔ </ts>
               <ts e="T248" id="Seg_4784" n="e" s="T247">măna. </ts>
            </ts>
            <ts e="T257" id="Seg_4785" n="sc" s="T249">
               <ts e="T250" id="Seg_4787" n="e" s="T249">Külalləm </ts>
               <ts e="T251" id="Seg_4789" n="e" s="T250">dăk, </ts>
               <ts e="T252" id="Seg_4791" n="e" s="T251">dĭgəttə </ts>
               <ts e="T254" id="Seg_4793" n="e" s="T252">kadəldədə, </ts>
               <ts e="T256" id="Seg_4795" n="e" s="T254">udazaŋdə </ts>
               <ts e="T257" id="Seg_4797" n="e" s="T256">bĭzəlil". </ts>
            </ts>
            <ts e="T263" id="Seg_4798" n="sc" s="T258">
               <ts e="T259" id="Seg_4800" n="e" s="T258">Măn </ts>
               <ts e="T260" id="Seg_4802" n="e" s="T259">dĭgəttə </ts>
               <ts e="T261" id="Seg_4804" n="e" s="T260">nendəbiem </ts>
               <ts e="T262" id="Seg_4806" n="e" s="T261">(voldʼa-) </ts>
               <ts e="T263" id="Seg_4808" n="e" s="T262">moltʼa. </ts>
            </ts>
            <ts e="T277" id="Seg_4809" n="sc" s="T264">
               <ts e="T265" id="Seg_4811" n="e" s="T264">Dĭgəttə </ts>
               <ts e="T266" id="Seg_4813" n="e" s="T265">sankaʔizi </ts>
               <ts e="T267" id="Seg_4815" n="e" s="T266">embiem </ts>
               <ts e="T268" id="Seg_4817" n="e" s="T267">i </ts>
               <ts e="T269" id="Seg_4819" n="e" s="T268">u </ts>
               <ts e="T270" id="Seg_4821" n="e" s="T269">kunnambiam. </ts>
               <ts e="T271" id="Seg_4823" n="e" s="T270">Bĭzəbiam </ts>
               <ts e="T272" id="Seg_4825" n="e" s="T271">dĭm </ts>
               <ts e="T273" id="Seg_4827" n="e" s="T272">da </ts>
               <ts e="T274" id="Seg_4829" n="e" s="T273">bazo </ts>
               <ts e="T275" id="Seg_4831" n="e" s="T274">maʔnə </ts>
               <ts e="T276" id="Seg_4833" n="e" s="T275">deʔpiem, </ts>
               <ts e="T277" id="Seg_4835" n="e" s="T276">sankaʔizi. </ts>
            </ts>
            <ts e="T284" id="Seg_4836" n="sc" s="T278">
               <ts e="T279" id="Seg_4838" n="e" s="T278">Embiem </ts>
               <ts e="T280" id="Seg_4840" n="e" s="T279">krăvatʼtə, </ts>
               <ts e="T281" id="Seg_4842" n="e" s="T280">măndəm: </ts>
               <ts e="T282" id="Seg_4844" n="e" s="T281">"Nu, </ts>
               <ts e="T283" id="Seg_4846" n="e" s="T282">tüj </ts>
               <ts e="T284" id="Seg_4848" n="e" s="T283">küʔ. </ts>
            </ts>
            <ts e="T292" id="Seg_4849" n="sc" s="T285">
               <ts e="T286" id="Seg_4851" n="e" s="T285">Kadəldə </ts>
               <ts e="T287" id="Seg_4853" n="e" s="T286">da </ts>
               <ts e="T288" id="Seg_4855" n="e" s="T287">(udazaŋzi) </ts>
               <ts e="T289" id="Seg_4857" n="e" s="T288">băzəlim, </ts>
               <ts e="T290" id="Seg_4859" n="e" s="T289">i </ts>
               <ts e="T291" id="Seg_4861" n="e" s="T290">jakše </ts>
               <ts e="T292" id="Seg_4863" n="e" s="T291">moləj. </ts>
            </ts>
            <ts e="T303" id="Seg_4864" n="sc" s="T293">
               <ts e="T294" id="Seg_4866" n="e" s="T293">Nu, </ts>
               <ts e="T295" id="Seg_4868" n="e" s="T294">dĭgəttə </ts>
               <ts e="T296" id="Seg_4870" n="e" s="T295">moltʼagən </ts>
               <ts e="T297" id="Seg_4872" n="e" s="T296">bĭzəbiam, </ts>
               <ts e="T298" id="Seg_4874" n="e" s="T297">ugandə </ts>
               <ts e="T299" id="Seg_4876" n="e" s="T298">(bal-) </ts>
               <ts e="T300" id="Seg_4878" n="e" s="T299">balgaš </ts>
               <ts e="T301" id="Seg_4880" n="e" s="T300">iʔgö </ts>
               <ts e="T302" id="Seg_4882" n="e" s="T301">ibi </ts>
               <ts e="T303" id="Seg_4884" n="e" s="T302">ulundə. </ts>
            </ts>
            <ts e="T309" id="Seg_4885" n="sc" s="T304">
               <ts e="T305" id="Seg_4887" n="e" s="T304">Măn </ts>
               <ts e="T306" id="Seg_4889" n="e" s="T305">mălliam: </ts>
               <ts e="T307" id="Seg_4891" n="e" s="T306">"Išo </ts>
               <ts e="T308" id="Seg_4893" n="e" s="T307">eneʔ </ts>
               <ts e="T309" id="Seg_4895" n="e" s="T308">bü". </ts>
            </ts>
            <ts e="T313" id="Seg_4896" n="sc" s="T310">
               <ts e="T311" id="Seg_4898" n="e" s="T310">Döm </ts>
               <ts e="T312" id="Seg_4900" n="e" s="T311">(kămne-) </ts>
               <ts e="T313" id="Seg_4902" n="e" s="T312">kămnit. </ts>
            </ts>
            <ts e="T315" id="Seg_4903" n="sc" s="T314">
               <ts e="T315" id="Seg_4905" n="e" s="T314">((BRK)). </ts>
            </ts>
            <ts e="T322" id="Seg_4906" n="sc" s="T316">
               <ts e="T317" id="Seg_4908" n="e" s="T316">Onʼiʔ </ts>
               <ts e="T318" id="Seg_4910" n="e" s="T317">(ini-) </ts>
               <ts e="T319" id="Seg_4912" n="e" s="T318">ine </ts>
               <ts e="T320" id="Seg_4914" n="e" s="T319">mĭbiem </ts>
               <ts e="T321" id="Seg_4916" n="e" s="T320">tĭbin </ts>
               <ts e="T322" id="Seg_4918" n="e" s="T321">kagattə. </ts>
            </ts>
            <ts e="T326" id="Seg_4919" n="sc" s="T323">
               <ts e="T324" id="Seg_4921" n="e" s="T323">Šide </ts>
               <ts e="T325" id="Seg_4923" n="e" s="T324">pʼe </ts>
               <ts e="T326" id="Seg_4925" n="e" s="T325">ibi. </ts>
            </ts>
            <ts e="T331" id="Seg_4926" n="sc" s="T327">
               <ts e="T328" id="Seg_4928" n="e" s="T327">Bazo </ts>
               <ts e="T329" id="Seg_4930" n="e" s="T328">ine </ts>
               <ts e="T330" id="Seg_4932" n="e" s="T329">mĭbiem, </ts>
               <ts e="T331" id="Seg_4934" n="e" s="T330">kuzanə. </ts>
            </ts>
            <ts e="T334" id="Seg_4935" n="sc" s="T332">
               <ts e="T333" id="Seg_4937" n="e" s="T332">Tĭbi </ts>
               <ts e="T334" id="Seg_4939" n="e" s="T333">ibi. </ts>
            </ts>
            <ts e="T341" id="Seg_4940" n="sc" s="T335">
               <ts e="T336" id="Seg_4942" n="e" s="T335">Naga </ts>
               <ts e="T337" id="Seg_4944" n="e" s="T336">ine. </ts>
               <ts e="T338" id="Seg_4946" n="e" s="T337">Tože </ts>
               <ts e="T339" id="Seg_4948" n="e" s="T338">šide </ts>
               <ts e="T340" id="Seg_4950" n="e" s="T339">pʼe </ts>
               <ts e="T341" id="Seg_4952" n="e" s="T340">ibi. </ts>
            </ts>
            <ts e="T349" id="Seg_4953" n="sc" s="T342">
               <ts e="T343" id="Seg_4955" n="e" s="T342">Mĭbiem </ts>
               <ts e="T344" id="Seg_4957" n="e" s="T343">ine. </ts>
               <ts e="T345" id="Seg_4959" n="e" s="T344">Dĭgəttə </ts>
               <ts e="T346" id="Seg_4961" n="e" s="T345">dĭ </ts>
               <ts e="T347" id="Seg_4963" n="e" s="T346">sestrandə </ts>
               <ts e="T348" id="Seg_4965" n="e" s="T347">ular </ts>
               <ts e="T349" id="Seg_4967" n="e" s="T348">mĭbiem. </ts>
            </ts>
            <ts e="T352" id="Seg_4968" n="sc" s="T350">
               <ts e="T351" id="Seg_4970" n="e" s="T350">Părga </ts>
               <ts e="T352" id="Seg_4972" n="e" s="T351">mĭbiem. </ts>
            </ts>
            <ts e="T358" id="Seg_4973" n="sc" s="T353">
               <ts e="T354" id="Seg_4975" n="e" s="T353">Dĭgəttə </ts>
               <ts e="T355" id="Seg_4977" n="e" s="T354">plemʼannican </ts>
               <ts e="T356" id="Seg_4979" n="e" s="T355">koʔbdonə </ts>
               <ts e="T357" id="Seg_4981" n="e" s="T356">mĭbiem </ts>
               <ts e="T358" id="Seg_4983" n="e" s="T357">buga. </ts>
            </ts>
            <ts e="T362" id="Seg_4984" n="sc" s="T359">
               <ts e="T360" id="Seg_4986" n="e" s="T359">(Pʼel=) </ts>
               <ts e="T361" id="Seg_4988" n="e" s="T360">Pʼel </ts>
               <ts e="T362" id="Seg_4990" n="e" s="T361">kö. </ts>
            </ts>
            <ts e="T372" id="Seg_4991" n="sc" s="T363">
               <ts e="T364" id="Seg_4993" n="e" s="T363">Dĭgəttə </ts>
               <ts e="T365" id="Seg_4995" n="e" s="T364">(m-) </ts>
               <ts e="T366" id="Seg_4997" n="e" s="T365">sestranə </ts>
               <ts e="T367" id="Seg_4999" n="e" s="T366">nʼit </ts>
               <ts e="T368" id="Seg_5001" n="e" s="T367">mĭbiem </ts>
               <ts e="T369" id="Seg_5003" n="e" s="T368">tüžöj, </ts>
               <ts e="T370" id="Seg_5005" n="e" s="T369">i </ts>
               <ts e="T371" id="Seg_5007" n="e" s="T370">ular </ts>
               <ts e="T372" id="Seg_5009" n="e" s="T371">mĭbiem. </ts>
            </ts>
            <ts e="T374" id="Seg_5010" n="sc" s="T373">
               <ts e="T374" id="Seg_5012" n="e" s="T373">((BRK)). </ts>
            </ts>
            <ts e="T382" id="Seg_5013" n="sc" s="T375">
               <ts e="T376" id="Seg_5015" n="e" s="T375">Dĭgəttə </ts>
               <ts e="T377" id="Seg_5017" n="e" s="T376">onʼiʔ </ts>
               <ts e="T378" id="Seg_5019" n="e" s="T377">nenə </ts>
               <ts e="T379" id="Seg_5021" n="e" s="T378">bazo </ts>
               <ts e="T380" id="Seg_5023" n="e" s="T379">(bu-) </ts>
               <ts e="T381" id="Seg_5025" n="e" s="T380">büzo </ts>
               <ts e="T382" id="Seg_5027" n="e" s="T381">mĭbiem. </ts>
            </ts>
            <ts e="T391" id="Seg_5028" n="sc" s="T383">
               <ts e="T384" id="Seg_5030" n="e" s="T383">Jil </ts>
               <ts e="T385" id="Seg_5032" n="e" s="T384">šoləʔi: </ts>
               <ts e="T386" id="Seg_5034" n="e" s="T385">"Deʔ </ts>
               <ts e="T387" id="Seg_5036" n="e" s="T386">aktʼa, </ts>
               <ts e="T388" id="Seg_5038" n="e" s="T387">(m-) </ts>
               <ts e="T389" id="Seg_5040" n="e" s="T388">miʔ </ts>
               <ts e="T390" id="Seg_5042" n="e" s="T389">tănan </ts>
               <ts e="T391" id="Seg_5044" n="e" s="T390">mĭlibeʔ". </ts>
            </ts>
            <ts e="T401" id="Seg_5045" n="sc" s="T392">
               <ts e="T393" id="Seg_5047" n="e" s="T392">A </ts>
               <ts e="T394" id="Seg_5049" n="e" s="T393">(boste-) </ts>
               <ts e="T395" id="Seg_5051" n="e" s="T394">bostəzaŋdə </ts>
               <ts e="T396" id="Seg_5053" n="e" s="T395">ej </ts>
               <ts e="T397" id="Seg_5055" n="e" s="T396">mĭleʔi. </ts>
               <ts e="T398" id="Seg_5057" n="e" s="T397">I </ts>
               <ts e="T399" id="Seg_5059" n="e" s="T398">dĭgəttə </ts>
               <ts e="T400" id="Seg_5061" n="e" s="T399">(maluʔle) </ts>
               <ts e="T401" id="Seg_5063" n="e" s="T400">aktʼa. </ts>
            </ts>
            <ts e="T403" id="Seg_5064" n="sc" s="T402">
               <ts e="T403" id="Seg_5066" n="e" s="T402">((BRK)). </ts>
            </ts>
            <ts e="T405" id="Seg_5067" n="sc" s="T404">
               <ts e="T405" id="Seg_5069" n="e" s="T404">((BRK)). </ts>
            </ts>
            <ts e="T417" id="Seg_5070" n="sc" s="T406">
               <ts e="T407" id="Seg_5072" n="e" s="T406">Măn </ts>
               <ts e="T408" id="Seg_5074" n="e" s="T407">tüj </ts>
               <ts e="T409" id="Seg_5076" n="e" s="T408">en </ts>
               <ts e="T410" id="Seg_5078" n="e" s="T409">ej </ts>
               <ts e="T411" id="Seg_5080" n="e" s="T410">(tone-) </ts>
               <ts e="T412" id="Seg_5082" n="e" s="T411">tenolam, </ts>
               <ts e="T413" id="Seg_5084" n="e" s="T412">a_to </ts>
               <ts e="T414" id="Seg_5086" n="e" s="T413">inen </ts>
               <ts e="T415" id="Seg_5088" n="e" s="T414">ulut </ts>
               <ts e="T416" id="Seg_5090" n="e" s="T415">nada </ts>
               <ts e="T417" id="Seg_5092" n="e" s="T416">urgo. </ts>
            </ts>
            <ts e="T419" id="Seg_5093" n="sc" s="T418">
               <ts e="T419" id="Seg_5095" n="e" s="T418">((BRK)). </ts>
            </ts>
            <ts e="T422" id="Seg_5096" n="sc" s="T420">
               <ts e="T421" id="Seg_5098" n="e" s="T420">Šiʔ </ts>
               <ts e="T422" id="Seg_5100" n="e" s="T421">tenolaʔbə. </ts>
            </ts>
            <ts e="T424" id="Seg_5101" n="sc" s="T423">
               <ts e="T424" id="Seg_5103" n="e" s="T423">((BRK)). </ts>
            </ts>
            <ts e="T426" id="Seg_5104" n="sc" s="T425">
               <ts e="T426" id="Seg_5106" n="e" s="T425">((BRK)). </ts>
            </ts>
            <ts e="T434" id="Seg_5107" n="sc" s="T427">
               <ts e="T428" id="Seg_5109" n="e" s="T427">Tüjö </ts>
               <ts e="T429" id="Seg_5111" n="e" s="T428">kalam </ts>
               <ts e="T430" id="Seg_5113" n="e" s="T429">tüžöjʔi </ts>
               <ts e="T431" id="Seg_5115" n="e" s="T430">(šendedndə) </ts>
               <ts e="T432" id="Seg_5117" n="e" s="T431">sürerlam </ts>
               <ts e="T433" id="Seg_5119" n="e" s="T432">i </ts>
               <ts e="T434" id="Seg_5121" n="e" s="T433">noʔmeləm. </ts>
            </ts>
            <ts e="T440" id="Seg_5122" n="sc" s="T435">
               <ts e="T436" id="Seg_5124" n="e" s="T435">Kambiam, </ts>
               <ts e="T437" id="Seg_5126" n="e" s="T436">a </ts>
               <ts e="T438" id="Seg_5128" n="e" s="T437">dĭzeŋ </ts>
               <ts e="T439" id="Seg_5130" n="e" s="T438">nuʔməluʔpiʔi </ts>
               <ts e="T440" id="Seg_5132" n="e" s="T439">gĭbərdə. </ts>
            </ts>
            <ts e="T442" id="Seg_5133" n="sc" s="T441">
               <ts e="T442" id="Seg_5135" n="e" s="T441">((BRK)). </ts>
            </ts>
            <ts e="T447" id="Seg_5136" n="sc" s="T443">
               <ts e="T444" id="Seg_5138" n="e" s="T443">Traktăr </ts>
               <ts e="T445" id="Seg_5140" n="e" s="T444">bar </ts>
               <ts e="T446" id="Seg_5142" n="e" s="T445">paʔi </ts>
               <ts e="T447" id="Seg_5144" n="e" s="T446">kunnandəga. </ts>
            </ts>
            <ts e="T452" id="Seg_5145" n="sc" s="T448">
               <ts e="T449" id="Seg_5147" n="e" s="T448">Ej </ts>
               <ts e="T450" id="Seg_5149" n="e" s="T449">tĭmnem, </ts>
               <ts e="T451" id="Seg_5151" n="e" s="T450">kumen </ts>
               <ts e="T452" id="Seg_5153" n="e" s="T451">(kunnandəga). </ts>
            </ts>
            <ts e="T454" id="Seg_5154" n="sc" s="T453">
               <ts e="T454" id="Seg_5156" n="e" s="T453">((BRK)). </ts>
            </ts>
            <ts e="T458" id="Seg_5157" n="sc" s="T455">
               <ts e="T456" id="Seg_5159" n="e" s="T455">Ugandə </ts>
               <ts e="T457" id="Seg_5161" n="e" s="T456">iʔgö </ts>
               <ts e="T458" id="Seg_5163" n="e" s="T457">paʔi. </ts>
            </ts>
            <ts e="T464" id="Seg_5164" n="sc" s="T459">
               <ts e="T460" id="Seg_5166" n="e" s="T459">Da </ts>
               <ts e="T461" id="Seg_5168" n="e" s="T460">i </ts>
               <ts e="T462" id="Seg_5170" n="e" s="T461">(nʼišpek=) </ts>
               <ts e="T463" id="Seg_5172" n="e" s="T462">nʼišpek </ts>
               <ts e="T464" id="Seg_5174" n="e" s="T463">ugandə. </ts>
            </ts>
            <ts e="T466" id="Seg_5175" n="sc" s="T465">
               <ts e="T466" id="Seg_5177" n="e" s="T465">((BRK)). </ts>
            </ts>
            <ts e="T474" id="Seg_5178" n="sc" s="T467">
               <ts e="T468" id="Seg_5180" n="e" s="T467">Iʔgö </ts>
               <ts e="T469" id="Seg_5182" n="e" s="T468">(tn-) </ts>
               <ts e="T470" id="Seg_5184" n="e" s="T469">togonorzittə </ts>
               <ts e="T471" id="Seg_5186" n="e" s="T470">kubiam, </ts>
               <ts e="T472" id="Seg_5188" n="e" s="T471">a </ts>
               <ts e="T473" id="Seg_5190" n="e" s="T472">amga </ts>
               <ts e="T474" id="Seg_5192" n="e" s="T473">kulaʔpiam. </ts>
            </ts>
            <ts e="T476" id="Seg_5193" n="sc" s="T475">
               <ts e="T476" id="Seg_5195" n="e" s="T475">((BRK)). </ts>
            </ts>
            <ts e="T480" id="Seg_5196" n="sc" s="T477">
               <ts e="T478" id="Seg_5198" n="e" s="T477">Koŋ </ts>
               <ts e="T479" id="Seg_5200" n="e" s="T478">ugandə </ts>
               <ts e="T480" id="Seg_5202" n="e" s="T479">kuvas. </ts>
            </ts>
            <ts e="T487" id="Seg_5203" n="sc" s="T481">
               <ts e="T482" id="Seg_5205" n="e" s="T481">Bar </ts>
               <ts e="T483" id="Seg_5207" n="e" s="T482">ĭmbi </ts>
               <ts e="T484" id="Seg_5209" n="e" s="T483">măna </ts>
               <ts e="T485" id="Seg_5211" n="e" s="T484">mĭmbi, </ts>
               <ts e="T486" id="Seg_5213" n="e" s="T485">bar </ts>
               <ts e="T487" id="Seg_5215" n="e" s="T486">ĭmbi. </ts>
            </ts>
            <ts e="T489" id="Seg_5216" n="sc" s="T488">
               <ts e="T489" id="Seg_5218" n="e" s="T488">((BRK)). </ts>
            </ts>
            <ts e="T494" id="Seg_5219" n="sc" s="T490">
               <ts e="T491" id="Seg_5221" n="e" s="T490">Bar </ts>
               <ts e="T492" id="Seg_5223" n="e" s="T491">măna </ts>
               <ts e="T493" id="Seg_5225" n="e" s="T492">(pʼem-) </ts>
               <ts e="T494" id="Seg_5227" n="e" s="T493">pʼerbi. </ts>
            </ts>
            <ts e="T499" id="Seg_5228" n="sc" s="T495">
               <ts e="T496" id="Seg_5230" n="e" s="T495">((BRK)). </ts>
               <ts e="T497" id="Seg_5232" n="e" s="T496">Это </ts>
               <ts e="T498" id="Seg_5234" n="e" s="T497">мало </ts>
               <ts e="T499" id="Seg_5236" n="e" s="T498">чего-то. </ts>
            </ts>
            <ts e="T504" id="Seg_5237" n="sc" s="T500">
               <ts e="T501" id="Seg_5239" n="e" s="T500">Moləj </ts>
               <ts e="T502" id="Seg_5241" n="e" s="T501">dʼala, </ts>
               <ts e="T503" id="Seg_5243" n="e" s="T502">moləj </ts>
               <ts e="T504" id="Seg_5245" n="e" s="T503">amorzittə. </ts>
            </ts>
            <ts e="T506" id="Seg_5246" n="sc" s="T505">
               <ts e="T506" id="Seg_5248" n="e" s="T505">((BRK)). </ts>
            </ts>
            <ts e="T514" id="Seg_5249" n="sc" s="T507">
               <ts e="T508" id="Seg_5251" n="e" s="T507">Teinen </ts>
               <ts e="T509" id="Seg_5253" n="e" s="T508">subbota, </ts>
               <ts e="T510" id="Seg_5255" n="e" s="T509">năda </ts>
               <ts e="T511" id="Seg_5257" n="e" s="T510">moltʼa </ts>
               <ts e="T512" id="Seg_5259" n="e" s="T511">nendəsʼtə, </ts>
               <ts e="T513" id="Seg_5261" n="e" s="T512">bü </ts>
               <ts e="T514" id="Seg_5263" n="e" s="T513">tazirzittə. </ts>
            </ts>
            <ts e="T520" id="Seg_5264" n="sc" s="T515">
               <ts e="T516" id="Seg_5266" n="e" s="T515">Dĭgəttə </ts>
               <ts e="T517" id="Seg_5268" n="e" s="T516">nendəlem </ts>
               <ts e="T518" id="Seg_5270" n="e" s="T517">(mul-), </ts>
               <ts e="T519" id="Seg_5272" n="e" s="T518">băzəjdəsʼtə </ts>
               <ts e="T520" id="Seg_5274" n="e" s="T519">nada. </ts>
            </ts>
            <ts e="T526" id="Seg_5275" n="sc" s="T521">
               <ts e="T522" id="Seg_5277" n="e" s="T521">A_to </ts>
               <ts e="T523" id="Seg_5279" n="e" s="T522">selej </ts>
               <ts e="T524" id="Seg_5281" n="e" s="T523">nedʼelʼa </ts>
               <ts e="T525" id="Seg_5283" n="e" s="T524">ej </ts>
               <ts e="T526" id="Seg_5285" n="e" s="T525">(băzəldəbiam). </ts>
            </ts>
            <ts e="T528" id="Seg_5286" n="sc" s="T527">
               <ts e="T528" id="Seg_5288" n="e" s="T527">((BRK)). </ts>
            </ts>
            <ts e="T533" id="Seg_5289" n="sc" s="T529">
               <ts e="T530" id="Seg_5291" n="e" s="T529">(Maʔənnə) </ts>
               <ts e="T531" id="Seg_5293" n="e" s="T530">sazən </ts>
               <ts e="T532" id="Seg_5295" n="e" s="T531">edeʔliem, </ts>
               <ts e="T533" id="Seg_5297" n="e" s="T532">edəʔliem. </ts>
            </ts>
            <ts e="T540" id="Seg_5298" n="sc" s="T534">
               <ts e="T535" id="Seg_5300" n="e" s="T534">Üge </ts>
               <ts e="T536" id="Seg_5302" n="e" s="T535">(nada) </ts>
               <ts e="T537" id="Seg_5304" n="e" s="T536">naga. </ts>
               <ts e="T538" id="Seg_5306" n="e" s="T537">Nem </ts>
               <ts e="T539" id="Seg_5308" n="e" s="T538">ej </ts>
               <ts e="T540" id="Seg_5310" n="e" s="T539">pʼaŋdlia. </ts>
            </ts>
            <ts e="T545" id="Seg_5311" n="sc" s="T541">
               <ts e="T542" id="Seg_5313" n="e" s="T541">I </ts>
               <ts e="T543" id="Seg_5315" n="e" s="T542">sazən </ts>
               <ts e="T544" id="Seg_5317" n="e" s="T543">ej </ts>
               <ts e="T545" id="Seg_5319" n="e" s="T544">šolia. </ts>
            </ts>
            <ts e="T551" id="Seg_5320" n="sc" s="T546">
               <ts e="T547" id="Seg_5322" n="e" s="T546">((BRK)). </ts>
               <ts e="T548" id="Seg_5324" n="e" s="T547">Teinen </ts>
               <ts e="T549" id="Seg_5326" n="e" s="T548">nüdʼin </ts>
               <ts e="T550" id="Seg_5328" n="e" s="T549">dʼodunʼi </ts>
               <ts e="T551" id="Seg_5330" n="e" s="T550">kubiem. </ts>
            </ts>
            <ts e="T556" id="Seg_5331" n="sc" s="T552">
               <ts e="T553" id="Seg_5333" n="e" s="T552">Dʼabrolaʔpiem, </ts>
               <ts e="T554" id="Seg_5335" n="e" s="T553">teinen </ts>
               <ts e="T555" id="Seg_5337" n="e" s="T554">sazən </ts>
               <ts e="T556" id="Seg_5339" n="e" s="T555">šoləj. </ts>
            </ts>
            <ts e="T558" id="Seg_5340" n="sc" s="T557">
               <ts e="T558" id="Seg_5342" n="e" s="T557">((BRK)). </ts>
            </ts>
            <ts e="T564" id="Seg_5343" n="sc" s="T559">
               <ts e="T560" id="Seg_5345" n="e" s="T559">Măn </ts>
               <ts e="T561" id="Seg_5347" n="e" s="T560">mĭmbiem </ts>
               <ts e="T562" id="Seg_5349" n="e" s="T561">Krasnojaskăjdə, </ts>
               <ts e="T1186" id="Seg_5351" n="e" s="T562">munəʔi </ts>
               <ts e="T563" id="Seg_5353" n="e" s="T1186">(-ʔi-) </ts>
               <ts e="T564" id="Seg_5355" n="e" s="T563">kumbiam. </ts>
            </ts>
            <ts e="T568" id="Seg_5356" n="sc" s="T565">
               <ts e="T566" id="Seg_5358" n="e" s="T565">(S-) </ts>
               <ts e="T567" id="Seg_5360" n="e" s="T566">Sto </ts>
               <ts e="T568" id="Seg_5362" n="e" s="T567">munəj. </ts>
            </ts>
            <ts e="T573" id="Seg_5363" n="sc" s="T569">
               <ts e="T570" id="Seg_5365" n="e" s="T569">Dĭgəttə </ts>
               <ts e="T571" id="Seg_5367" n="e" s="T570">kagam </ts>
               <ts e="T572" id="Seg_5369" n="e" s="T571">mănzi </ts>
               <ts e="T573" id="Seg_5371" n="e" s="T572">kambi. </ts>
            </ts>
            <ts e="T580" id="Seg_5372" n="sc" s="T574">
               <ts e="T575" id="Seg_5374" n="e" s="T574">Dĭ </ts>
               <ts e="T576" id="Seg_5376" n="e" s="T575">măna </ts>
               <ts e="T577" id="Seg_5378" n="e" s="T576">kumbi, </ts>
               <ts e="T578" id="Seg_5380" n="e" s="T577">(amnobi=) </ts>
               <ts e="T579" id="Seg_5382" n="e" s="T578">amnobi </ts>
               <ts e="T580" id="Seg_5384" n="e" s="T579">(mašinaʔizi). </ts>
            </ts>
            <ts e="T589" id="Seg_5385" n="sc" s="T581">
               <ts e="T582" id="Seg_5387" n="e" s="T581">Măn </ts>
               <ts e="T583" id="Seg_5389" n="e" s="T582">(ej-) </ts>
               <ts e="T584" id="Seg_5391" n="e" s="T583">ej </ts>
               <ts e="T585" id="Seg_5393" n="e" s="T584">moliam </ts>
               <ts e="T586" id="Seg_5395" n="e" s="T585">kuzittə, </ts>
               <ts e="T587" id="Seg_5397" n="e" s="T586">dĭgəttə </ts>
               <ts e="T588" id="Seg_5399" n="e" s="T587">kubiam </ts>
               <ts e="T589" id="Seg_5401" n="e" s="T588">plemʼannicam. </ts>
            </ts>
            <ts e="T596" id="Seg_5402" n="sc" s="T590">
               <ts e="T591" id="Seg_5404" n="e" s="T590">A </ts>
               <ts e="T592" id="Seg_5406" n="e" s="T591">dĭbər </ts>
               <ts e="T593" id="Seg_5408" n="e" s="T592">kandəbiam, </ts>
               <ts e="T594" id="Seg_5410" n="e" s="T593">(dö-) </ts>
               <ts e="T595" id="Seg_5412" n="e" s="T594">döʔən </ts>
               <ts e="T596" id="Seg_5414" n="e" s="T595">mašinazi. </ts>
            </ts>
            <ts e="T600" id="Seg_5415" n="sc" s="T597">
               <ts e="T598" id="Seg_5417" n="e" s="T597">A </ts>
               <ts e="T599" id="Seg_5419" n="e" s="T598">dĭn </ts>
               <ts e="T600" id="Seg_5421" n="e" s="T599">avtobussi. </ts>
            </ts>
            <ts e="T610" id="Seg_5422" n="sc" s="T601">
               <ts e="T602" id="Seg_5424" n="e" s="T601">Dĭgəttə </ts>
               <ts e="T603" id="Seg_5426" n="e" s="T602">bazaj </ts>
               <ts e="T604" id="Seg_5428" n="e" s="T603">(aktʼi </ts>
               <ts e="T605" id="Seg_5430" n="e" s="T604">= </ts>
               <ts e="T606" id="Seg_5432" n="e" s="T605">aktʼa=) </ts>
               <ts e="T607" id="Seg_5434" n="e" s="T606">aktʼi, </ts>
               <ts e="T608" id="Seg_5436" n="e" s="T607">dĭgəttə </ts>
               <ts e="T609" id="Seg_5438" n="e" s="T608">dĭbər </ts>
               <ts e="T610" id="Seg_5440" n="e" s="T609">šobiam. </ts>
            </ts>
            <ts e="T612" id="Seg_5441" n="sc" s="T611">
               <ts e="T612" id="Seg_5443" n="e" s="T611">((BRK)). </ts>
            </ts>
            <ts e="T619" id="Seg_5444" n="sc" s="T613">
               <ts e="T614" id="Seg_5446" n="e" s="T613">Dĭgəttə </ts>
               <ts e="T615" id="Seg_5448" n="e" s="T614">mĭmbiem </ts>
               <ts e="T616" id="Seg_5450" n="e" s="T615">kudajdə </ts>
               <ts e="T617" id="Seg_5452" n="e" s="T616">(uman-) </ts>
               <ts e="T618" id="Seg_5454" n="e" s="T617">numan </ts>
               <ts e="T619" id="Seg_5456" n="e" s="T618">üzittə. </ts>
            </ts>
            <ts e="T624" id="Seg_5457" n="sc" s="T620">
               <ts e="T621" id="Seg_5459" n="e" s="T620">Dĭgəttə </ts>
               <ts e="T622" id="Seg_5461" n="e" s="T621">mĭmbibeʔ </ts>
               <ts e="T623" id="Seg_5463" n="e" s="T622">plemʼannicazi </ts>
               <ts e="T624" id="Seg_5465" n="e" s="T623">(žibiaʔinə). </ts>
            </ts>
            <ts e="T630" id="Seg_5466" n="sc" s="T625">
               <ts e="T626" id="Seg_5468" n="e" s="T625">Dĭn </ts>
               <ts e="T627" id="Seg_5470" n="e" s="T626">(s-) </ts>
               <ts e="T628" id="Seg_5472" n="e" s="T627">amnobibaʔ </ts>
               <ts e="T629" id="Seg_5474" n="e" s="T628">da </ts>
               <ts e="T630" id="Seg_5476" n="e" s="T629">dʼăbaktərbibaʔ. </ts>
            </ts>
            <ts e="T635" id="Seg_5477" n="sc" s="T631">
               <ts e="T632" id="Seg_5479" n="e" s="T631">A </ts>
               <ts e="T633" id="Seg_5481" n="e" s="T632">dĭgəttə </ts>
               <ts e="T634" id="Seg_5483" n="e" s="T633">maʔndə </ts>
               <ts e="T635" id="Seg_5485" n="e" s="T634">šobiam. </ts>
            </ts>
            <ts e="T637" id="Seg_5486" n="sc" s="T636">
               <ts e="T637" id="Seg_5488" n="e" s="T636">((BRK)). </ts>
            </ts>
            <ts e="T649" id="Seg_5489" n="sc" s="T638">
               <ts e="T639" id="Seg_5491" n="e" s="T638">Plemʼannica </ts>
               <ts e="T640" id="Seg_5493" n="e" s="T639">măna </ts>
               <ts e="T641" id="Seg_5495" n="e" s="T640">pomidor </ts>
               <ts e="T642" id="Seg_5497" n="e" s="T641">iʔgö </ts>
               <ts e="T643" id="Seg_5499" n="e" s="T642">mĭbi. </ts>
               <ts e="T644" id="Seg_5501" n="e" s="T643">Măn </ts>
               <ts e="T645" id="Seg_5503" n="e" s="T644">(Kat-) </ts>
               <ts e="T1184" id="Seg_5505" n="e" s="T645">Kazan </ts>
               <ts e="T646" id="Seg_5507" n="e" s="T1184">turagən </ts>
               <ts e="T647" id="Seg_5509" n="e" s="T646">šide </ts>
               <ts e="T648" id="Seg_5511" n="e" s="T647">dʼala </ts>
               <ts e="T649" id="Seg_5513" n="e" s="T648">amnobiam. </ts>
            </ts>
            <ts e="T651" id="Seg_5514" n="sc" s="T650">
               <ts e="T651" id="Seg_5516" n="e" s="T650">((BRK)). </ts>
            </ts>
            <ts e="T662" id="Seg_5517" n="sc" s="T652">
               <ts e="T653" id="Seg_5519" n="e" s="T652">Tüžöjʔi </ts>
               <ts e="T654" id="Seg_5521" n="e" s="T653">šobiʔi </ts>
               <ts e="T655" id="Seg_5523" n="e" s="T654">maʔndə, </ts>
               <ts e="T656" id="Seg_5525" n="e" s="T655">nada </ts>
               <ts e="T657" id="Seg_5527" n="e" s="T656">dĭzem </ts>
               <ts e="T658" id="Seg_5529" n="e" s="T657">šedendə </ts>
               <ts e="T659" id="Seg_5531" n="e" s="T658">sürerzittə </ts>
               <ts e="T660" id="Seg_5533" n="e" s="T659">i </ts>
               <ts e="T661" id="Seg_5535" n="e" s="T660">(nom-) </ts>
               <ts e="T662" id="Seg_5537" n="e" s="T661">noʔməzittə. </ts>
            </ts>
            <ts e="T664" id="Seg_5538" n="sc" s="T663">
               <ts e="T664" id="Seg_5540" n="e" s="T663">((BRK)). </ts>
            </ts>
            <ts e="T669" id="Seg_5541" n="sc" s="T665">
               <ts e="T666" id="Seg_5543" n="e" s="T665">Tenöbiam, </ts>
               <ts e="T667" id="Seg_5545" n="e" s="T666">tenöbiam, </ts>
               <ts e="T668" id="Seg_5547" n="e" s="T667">ĭmbi-nʼibudʼ </ts>
               <ts e="T669" id="Seg_5549" n="e" s="T668">tenöluʔpiem. </ts>
            </ts>
            <ts e="T671" id="Seg_5550" n="sc" s="T670">
               <ts e="T671" id="Seg_5552" n="e" s="T670">((DMG)). </ts>
            </ts>
            <ts e="T678" id="Seg_5553" n="sc" s="T672">
               <ts e="T673" id="Seg_5555" n="e" s="T672">(Koʔbdonə </ts>
               <ts e="T674" id="Seg_5557" n="e" s="T673">mĭmbiem), </ts>
               <ts e="T675" id="Seg_5559" n="e" s="T674">sumka </ts>
               <ts e="T676" id="Seg_5561" n="e" s="T675">dĭ </ts>
               <ts e="T677" id="Seg_5563" n="e" s="T676">bar </ts>
               <ts e="T678" id="Seg_5565" n="e" s="T677">sajnʼiluʔpi. </ts>
            </ts>
            <ts e="T684" id="Seg_5566" n="sc" s="T679">
               <ts e="T680" id="Seg_5568" n="e" s="T679">Tüj </ts>
               <ts e="T681" id="Seg_5570" n="e" s="T680">amnolaʔbəm </ts>
               <ts e="T682" id="Seg_5572" n="e" s="T681">da </ts>
               <ts e="T683" id="Seg_5574" n="e" s="T682">(s-) </ts>
               <ts e="T684" id="Seg_5576" n="e" s="T683">šüdörleʔbəm. </ts>
            </ts>
            <ts e="T691" id="Seg_5577" n="sc" s="T685">
               <ts e="T686" id="Seg_5579" n="e" s="T685">Tănan </ts>
               <ts e="T687" id="Seg_5581" n="e" s="T686">šonəbiam, </ts>
               <ts e="T688" id="Seg_5583" n="e" s="T687">(šonəbiam), </ts>
               <ts e="T689" id="Seg_5585" n="e" s="T688">appi </ts>
               <ts e="T690" id="Seg_5587" n="e" s="T689">ej </ts>
               <ts e="T691" id="Seg_5589" n="e" s="T690">saʔməluʔpiam. </ts>
            </ts>
            <ts e="T693" id="Seg_5590" n="sc" s="T692">
               <ts e="T693" id="Seg_5592" n="e" s="T692">((BRK)). </ts>
            </ts>
            <ts e="T696" id="Seg_5593" n="sc" s="T694">
               <ts e="T695" id="Seg_5595" n="e" s="T694">Погоди. </ts>
               <ts e="T696" id="Seg_5597" n="e" s="T695">((KA:)) Kozaŋ. </ts>
            </ts>
            <ts e="T711" id="Seg_5598" n="sc" s="T697">
               <ts e="T698" id="Seg_5600" n="e" s="T697">((PKZ:)) Kazaŋ </ts>
               <ts e="T699" id="Seg_5602" n="e" s="T698">šonəga, </ts>
               <ts e="T700" id="Seg_5604" n="e" s="T699">šonəga. </ts>
               <ts e="T701" id="Seg_5606" n="e" s="T700">Dĭgəttə </ts>
               <ts e="T702" id="Seg_5608" n="e" s="T701">noʔ </ts>
               <ts e="T703" id="Seg_5610" n="e" s="T702">dĭʔnə </ts>
               <ts e="T704" id="Seg_5612" n="e" s="T703">(püj-) </ts>
               <ts e="T705" id="Seg_5614" n="e" s="T704">püje </ts>
               <ts e="T706" id="Seg_5616" n="e" s="T705">(bĭʔpi- </ts>
               <ts e="T707" id="Seg_5618" n="e" s="T706">bə-) </ts>
               <ts e="T708" id="Seg_5620" n="e" s="T707">bătluʔbi. </ts>
               <ts e="T709" id="Seg_5622" n="e" s="T708">Kemdə </ts>
               <ts e="T710" id="Seg_5624" n="e" s="T709">bar </ts>
               <ts e="T711" id="Seg_5626" n="e" s="T710">mʼaŋnaʔbə. </ts>
            </ts>
            <ts e="T715" id="Seg_5627" n="sc" s="T712">
               <ts e="T713" id="Seg_5629" n="e" s="T712">Dĭ </ts>
               <ts e="T714" id="Seg_5631" n="e" s="T713">măndə: </ts>
               <ts e="T715" id="Seg_5633" n="e" s="T714">"Šü! </ts>
            </ts>
            <ts e="T719" id="Seg_5634" n="sc" s="T716">
               <ts e="T717" id="Seg_5636" n="e" s="T716">Nendədə </ts>
               <ts e="T718" id="Seg_5638" n="e" s="T717">dĭ </ts>
               <ts e="T719" id="Seg_5640" n="e" s="T718">noʔ. </ts>
            </ts>
            <ts e="T721" id="Seg_5641" n="sc" s="T720">
               <ts e="T721" id="Seg_5643" n="e" s="T720">((BRK)). </ts>
            </ts>
            <ts e="T730" id="Seg_5644" n="sc" s="T722">
               <ts e="T723" id="Seg_5646" n="e" s="T722">"Măn </ts>
               <ts e="T724" id="Seg_5648" n="e" s="T723">iʔgö </ts>
               <ts e="T725" id="Seg_5650" n="e" s="T724">dʼüm </ts>
               <ts e="T726" id="Seg_5652" n="e" s="T725">nendəsʼtə". </ts>
               <ts e="T727" id="Seg_5654" n="e" s="T726">Dĭgəttə </ts>
               <ts e="T728" id="Seg_5656" n="e" s="T727">dĭ </ts>
               <ts e="T729" id="Seg_5658" n="e" s="T728">bünə </ts>
               <ts e="T730" id="Seg_5660" n="e" s="T729">kambi. </ts>
            </ts>
            <ts e="T736" id="Seg_5661" n="sc" s="T731">
               <ts e="T732" id="Seg_5663" n="e" s="T731">Bü, </ts>
               <ts e="T733" id="Seg_5665" n="e" s="T732">kanaʔ, </ts>
               <ts e="T734" id="Seg_5667" n="e" s="T733">šüm </ts>
               <ts e="T735" id="Seg_5669" n="e" s="T734">(kăm-) </ts>
               <ts e="T736" id="Seg_5671" n="e" s="T735">kămnaʔ. </ts>
            </ts>
            <ts e="T738" id="Seg_5672" n="sc" s="T737">
               <ts e="T738" id="Seg_5674" n="e" s="T737">((BRK)). </ts>
            </ts>
            <ts e="T747" id="Seg_5675" n="sc" s="T739">
               <ts e="T740" id="Seg_5677" n="e" s="T739">"Măn </ts>
               <ts e="T741" id="Seg_5679" n="e" s="T740">iʔgö </ts>
               <ts e="T742" id="Seg_5681" n="e" s="T741">(dʼ-) </ts>
               <ts e="T743" id="Seg_5683" n="e" s="T742">dʼü </ts>
               <ts e="T744" id="Seg_5685" n="e" s="T743">kămnastə, </ts>
               <ts e="T745" id="Seg_5687" n="e" s="T744">štobɨ </ts>
               <ts e="T746" id="Seg_5689" n="e" s="T745">noʔ </ts>
               <ts e="T747" id="Seg_5691" n="e" s="T746">özerbi. </ts>
            </ts>
            <ts e="T749" id="Seg_5692" n="sc" s="T748">
               <ts e="T749" id="Seg_5694" n="e" s="T748">((BRK)). </ts>
            </ts>
            <ts e="T753" id="Seg_5695" n="sc" s="T750">
               <ts e="T751" id="Seg_5697" n="e" s="T750">Dĭgəttə </ts>
               <ts e="T752" id="Seg_5699" n="e" s="T751">dĭ </ts>
               <ts e="T753" id="Seg_5701" n="e" s="T752">kambi. </ts>
            </ts>
            <ts e="T761" id="Seg_5702" n="sc" s="T754">
               <ts e="T755" id="Seg_5704" n="e" s="T754">"Bulan, </ts>
               <ts e="T756" id="Seg_5706" n="e" s="T755">bulan, </ts>
               <ts e="T757" id="Seg_5708" n="e" s="T756">kanaʔ, </ts>
               <ts e="T758" id="Seg_5710" n="e" s="T757">bar </ts>
               <ts e="T759" id="Seg_5712" n="e" s="T758">bü </ts>
               <ts e="T760" id="Seg_5714" n="e" s="T759">(bĭ-) </ts>
               <ts e="T761" id="Seg_5716" n="e" s="T760">bĭdeʔ!" </ts>
            </ts>
            <ts e="T769" id="Seg_5717" n="sc" s="T762">
               <ts e="T763" id="Seg_5719" n="e" s="T762">Dĭgəttə </ts>
               <ts e="T764" id="Seg_5721" n="e" s="T763">bulan </ts>
               <ts e="T765" id="Seg_5723" n="e" s="T764">mămbi: </ts>
               <ts e="T766" id="Seg_5725" n="e" s="T765">"Măn </ts>
               <ts e="T767" id="Seg_5727" n="e" s="T766">üjündə </ts>
               <ts e="T768" id="Seg_5729" n="e" s="T767">iʔgö </ts>
               <ts e="T769" id="Seg_5731" n="e" s="T768">bü!" </ts>
            </ts>
            <ts e="T781" id="Seg_5732" n="sc" s="T770">
               <ts e="T771" id="Seg_5734" n="e" s="T770">Dĭgəttə </ts>
               <ts e="T772" id="Seg_5736" n="e" s="T771">dĭ </ts>
               <ts e="T773" id="Seg_5738" n="e" s="T772">(m-) </ts>
               <ts e="T774" id="Seg_5740" n="e" s="T773">kambi </ts>
               <ts e="T775" id="Seg_5742" n="e" s="T774">nükenə. </ts>
               <ts e="T776" id="Seg_5744" n="e" s="T775">"Nüke, </ts>
               <ts e="T777" id="Seg_5746" n="e" s="T776">nüke, </ts>
               <ts e="T778" id="Seg_5748" n="e" s="T777">kanaʔ </ts>
               <ts e="T779" id="Seg_5750" n="e" s="T778">žilaʔi </ts>
               <ts e="T780" id="Seg_5752" n="e" s="T779">üjüttə </ts>
               <ts e="T781" id="Seg_5754" n="e" s="T780">it!" </ts>
            </ts>
            <ts e="T785" id="Seg_5755" n="sc" s="T782">
               <ts e="T783" id="Seg_5757" n="e" s="T782">Nüke </ts>
               <ts e="T784" id="Seg_5759" n="e" s="T783">ej </ts>
               <ts e="T785" id="Seg_5761" n="e" s="T784">kambi. </ts>
            </ts>
            <ts e="T789" id="Seg_5762" n="sc" s="T786">
               <ts e="T787" id="Seg_5764" n="e" s="T786">"Sagərʔi, </ts>
               <ts e="T788" id="Seg_5766" n="e" s="T787">kabarləj </ts>
               <ts e="T789" id="Seg_5768" n="e" s="T788">măna". </ts>
            </ts>
            <ts e="T791" id="Seg_5769" n="sc" s="T790">
               <ts e="T791" id="Seg_5771" n="e" s="T790">((BRK)). </ts>
            </ts>
            <ts e="T811" id="Seg_5772" n="sc" s="T792">
               <ts e="T793" id="Seg_5774" n="e" s="T792">Dĭ </ts>
               <ts e="T794" id="Seg_5776" n="e" s="T793">kambi </ts>
               <ts e="T795" id="Seg_5778" n="e" s="T794">nükenə: </ts>
               <ts e="T796" id="Seg_5780" n="e" s="T795">"Nüke, </ts>
               <ts e="T797" id="Seg_5782" n="e" s="T796">nüke, </ts>
               <ts e="T798" id="Seg_5784" n="e" s="T797">it </ts>
               <ts e="T799" id="Seg_5786" n="e" s="T798">(žil-) </ts>
               <ts e="T800" id="Seg_5788" n="e" s="T799">žilaʔi". </ts>
               <ts e="T801" id="Seg_5790" n="e" s="T800">A </ts>
               <ts e="T802" id="Seg_5792" n="e" s="T801">dĭ </ts>
               <ts e="T803" id="Seg_5794" n="e" s="T802">măndə: </ts>
               <ts e="T804" id="Seg_5796" n="e" s="T803">"Măn </ts>
               <ts e="T805" id="Seg_5798" n="e" s="T804">iʔgö </ts>
               <ts e="T806" id="Seg_5800" n="e" s="T805">ige, </ts>
               <ts e="T807" id="Seg_5802" n="e" s="T806">(da) </ts>
               <ts e="T808" id="Seg_5804" n="e" s="T807">sagər, </ts>
               <ts e="T809" id="Seg_5806" n="e" s="T808">măna </ts>
               <ts e="T810" id="Seg_5808" n="e" s="T809">ej </ts>
               <ts e="T811" id="Seg_5810" n="e" s="T810">kereʔ". </ts>
            </ts>
            <ts e="T813" id="Seg_5811" n="sc" s="T812">
               <ts e="T813" id="Seg_5813" n="e" s="T812">((BRK)). </ts>
            </ts>
            <ts e="T823" id="Seg_5814" n="sc" s="T814">
               <ts e="T815" id="Seg_5816" n="e" s="T814">Dĭgəttə </ts>
               <ts e="T816" id="Seg_5818" n="e" s="T815">kambi </ts>
               <ts e="T817" id="Seg_5820" n="e" s="T816">tumoʔinə. </ts>
               <ts e="T818" id="Seg_5822" n="e" s="T817">"Tumo, </ts>
               <ts e="T819" id="Seg_5824" n="e" s="T818">tumo, </ts>
               <ts e="T820" id="Seg_5826" n="e" s="T819">kanaʔ, </ts>
               <ts e="T821" id="Seg_5828" n="e" s="T820">nüken </ts>
               <ts e="T822" id="Seg_5830" n="e" s="T821">žilaʔi </ts>
               <ts e="T823" id="Seg_5832" n="e" s="T822">amnaʔ!" </ts>
            </ts>
            <ts e="T831" id="Seg_5833" n="sc" s="T824">
               <ts e="T825" id="Seg_5835" n="e" s="T824">"dʼok, </ts>
               <ts e="T826" id="Seg_5837" n="e" s="T825">(mi-) </ts>
               <ts e="T827" id="Seg_5839" n="e" s="T826">miʔ </ts>
               <ts e="T828" id="Seg_5841" n="e" s="T827">(em </ts>
               <ts e="T829" id="Seg_5843" n="e" s="T828">kallaʔ=) </ts>
               <ts e="T830" id="Seg_5845" n="e" s="T829">ej </ts>
               <ts e="T831" id="Seg_5847" n="e" s="T830">kallam. </ts>
            </ts>
            <ts e="T837" id="Seg_5848" n="sc" s="T832">
               <ts e="T833" id="Seg_5850" n="e" s="T832">Miʔnʼibeʔ </ts>
               <ts e="T834" id="Seg_5852" n="e" s="T833">kabarləj </ts>
               <ts e="T835" id="Seg_5854" n="e" s="T834">dʼügən </ts>
               <ts e="T836" id="Seg_5856" n="e" s="T835">kornʼiʔi </ts>
               <ts e="T837" id="Seg_5858" n="e" s="T836">amzittə". </ts>
            </ts>
            <ts e="T839" id="Seg_5859" n="sc" s="T838">
               <ts e="T839" id="Seg_5861" n="e" s="T838">((BRK)). </ts>
            </ts>
            <ts e="T854" id="Seg_5862" n="sc" s="T840">
               <ts e="T841" id="Seg_5864" n="e" s="T840">Dĭgəttə </ts>
               <ts e="T842" id="Seg_5866" n="e" s="T841">kambi </ts>
               <ts e="T843" id="Seg_5868" n="e" s="T842">((DMG)) </ts>
               <ts e="T844" id="Seg_5870" n="e" s="T843">esseŋdə. </ts>
               <ts e="T845" id="Seg_5872" n="e" s="T844">"Esseŋ, </ts>
               <ts e="T846" id="Seg_5874" n="e" s="T845">esseŋ, </ts>
               <ts e="T847" id="Seg_5876" n="e" s="T846">kutlaʔləj </ts>
               <ts e="T848" id="Seg_5878" n="e" s="T847">tumoʔi!" </ts>
               <ts e="T849" id="Seg_5880" n="e" s="T848">Dĭzeŋ: </ts>
               <ts e="T850" id="Seg_5882" n="e" s="T849">"Miʔ </ts>
               <ts e="T851" id="Seg_5884" n="e" s="T850">(bo-) </ts>
               <ts e="T852" id="Seg_5886" n="e" s="T851">bospə </ts>
               <ts e="T853" id="Seg_5888" n="e" s="T852">sʼarlaʔbəʔjə </ts>
               <ts e="T854" id="Seg_5890" n="e" s="T853">baːbkaʔiziʔ". </ts>
            </ts>
            <ts e="T856" id="Seg_5891" n="sc" s="T855">
               <ts e="T856" id="Seg_5893" n="e" s="T855">((BRK)). </ts>
            </ts>
            <ts e="T861" id="Seg_5894" n="sc" s="T857">
               <ts e="T858" id="Seg_5896" n="e" s="T857">Šiʔ </ts>
               <ts e="T859" id="Seg_5898" n="e" s="T858">turagən </ts>
               <ts e="T860" id="Seg_5900" n="e" s="T859">jakše </ts>
               <ts e="T861" id="Seg_5902" n="e" s="T860">amnozittə. </ts>
            </ts>
            <ts e="T867" id="Seg_5903" n="sc" s="T862">
               <ts e="T863" id="Seg_5905" n="e" s="T862">Tolʼko </ts>
               <ts e="T864" id="Seg_5907" n="e" s="T863">ipek </ts>
               <ts e="T865" id="Seg_5909" n="e" s="T864">gijendə </ts>
               <ts e="T866" id="Seg_5911" n="e" s="T865">ej </ts>
               <ts e="T867" id="Seg_5913" n="e" s="T866">ilil. </ts>
            </ts>
            <ts e="T871" id="Seg_5914" n="sc" s="T868">
               <ts e="T869" id="Seg_5916" n="e" s="T868">Amzittə </ts>
               <ts e="T870" id="Seg_5918" n="e" s="T869">naga </ts>
               <ts e="T871" id="Seg_5920" n="e" s="T870">ipek. </ts>
            </ts>
            <ts e="T873" id="Seg_5921" n="sc" s="T872">
               <ts e="T873" id="Seg_5923" n="e" s="T872">((BRK)). </ts>
            </ts>
            <ts e="T880" id="Seg_5924" n="sc" s="T874">
               <ts e="T875" id="Seg_5926" n="e" s="T874">Magazingəndə </ts>
               <ts e="T876" id="Seg_5928" n="e" s="T875">ĭmbi </ts>
               <ts e="T877" id="Seg_5930" n="e" s="T876">naga, </ts>
               <ts e="T878" id="Seg_5932" n="e" s="T877">ĭmbidə </ts>
               <ts e="T879" id="Seg_5934" n="e" s="T878">ej </ts>
               <ts e="T880" id="Seg_5936" n="e" s="T879">iləl. </ts>
            </ts>
            <ts e="T890" id="Seg_5937" n="sc" s="T881">
               <ts e="T882" id="Seg_5939" n="e" s="T881">"Dʼok, </ts>
               <ts e="T883" id="Seg_5941" n="e" s="T882">kanaʔ, </ts>
               <ts e="T884" id="Seg_5943" n="e" s="T883">dĭn </ts>
               <ts e="T885" id="Seg_5945" n="e" s="T884">ige </ts>
               <ts e="T886" id="Seg_5947" n="e" s="T885">kălbasa, </ts>
               <ts e="T887" id="Seg_5949" n="e" s="T886">iʔ </ts>
               <ts e="T888" id="Seg_5951" n="e" s="T887">da </ts>
               <ts e="T889" id="Seg_5953" n="e" s="T888">dĭgəttə </ts>
               <ts e="T890" id="Seg_5955" n="e" s="T889">amorlal". </ts>
            </ts>
            <ts e="T892" id="Seg_5956" n="sc" s="T891">
               <ts e="T892" id="Seg_5958" n="e" s="T891">((BRK)). </ts>
            </ts>
            <ts e="T900" id="Seg_5959" n="sc" s="T893">
               <ts e="T894" id="Seg_5961" n="e" s="T893">Dĭ </ts>
               <ts e="T895" id="Seg_5963" n="e" s="T894">nüke </ts>
               <ts e="T896" id="Seg_5965" n="e" s="T895">ugandə </ts>
               <ts e="T897" id="Seg_5967" n="e" s="T896">jakše </ts>
               <ts e="T898" id="Seg_5969" n="e" s="T897">šödörlaʔbə, </ts>
               <ts e="T899" id="Seg_5971" n="e" s="T898">bar </ts>
               <ts e="T900" id="Seg_5973" n="e" s="T899">kuvas. </ts>
            </ts>
            <ts e="T903" id="Seg_5974" n="sc" s="T901">
               <ts e="T902" id="Seg_5976" n="e" s="T901">Jakše </ts>
               <ts e="T903" id="Seg_5978" n="e" s="T902">măndosʼtə. </ts>
            </ts>
            <ts e="T905" id="Seg_5979" n="sc" s="T904">
               <ts e="T905" id="Seg_5981" n="e" s="T904">((BRK)). </ts>
            </ts>
            <ts e="T916" id="Seg_5982" n="sc" s="T906">
               <ts e="T907" id="Seg_5984" n="e" s="T906">Dĭ </ts>
               <ts e="T908" id="Seg_5986" n="e" s="T907">nüke </ts>
               <ts e="T909" id="Seg_5988" n="e" s="T908">ugandə </ts>
               <ts e="T910" id="Seg_5990" n="e" s="T909">iʔgö </ts>
               <ts e="T911" id="Seg_5992" n="e" s="T910">(tono-) </ts>
               <ts e="T912" id="Seg_5994" n="e" s="T911">togonoria, </ts>
               <ts e="T913" id="Seg_5996" n="e" s="T912">bar </ts>
               <ts e="T914" id="Seg_5998" n="e" s="T913">ĭmbi </ts>
               <ts e="T915" id="Seg_6000" n="e" s="T914">azittə </ts>
               <ts e="T916" id="Seg_6002" n="e" s="T915">умеет. </ts>
            </ts>
            <ts e="T918" id="Seg_6003" n="sc" s="T917">
               <ts e="T918" id="Seg_6005" n="e" s="T917">((BRK)). </ts>
            </ts>
            <ts e="T923" id="Seg_6006" n="sc" s="T919">
               <ts e="T920" id="Seg_6008" n="e" s="T919">Bar </ts>
               <ts e="T921" id="Seg_6010" n="e" s="T920">ĭmbi </ts>
               <ts e="T922" id="Seg_6012" n="e" s="T921">togonorzittə </ts>
               <ts e="T923" id="Seg_6014" n="e" s="T922">tĭmnet. </ts>
            </ts>
            <ts e="T925" id="Seg_6015" n="sc" s="T924">
               <ts e="T925" id="Seg_6017" n="e" s="T924">((BRK)). </ts>
            </ts>
            <ts e="T935" id="Seg_6018" n="sc" s="T926">
               <ts e="T927" id="Seg_6020" n="e" s="T926">Tĭn </ts>
               <ts e="T928" id="Seg_6022" n="e" s="T927">nüken </ts>
               <ts e="T929" id="Seg_6024" n="e" s="T928">(kuz-) </ts>
               <ts e="T930" id="Seg_6026" n="e" s="T929">tukazaŋdə </ts>
               <ts e="T931" id="Seg_6028" n="e" s="T930">amga </ts>
               <ts e="T932" id="Seg_6030" n="e" s="T931">amnambi, </ts>
               <ts e="T933" id="Seg_6032" n="e" s="T932">tolʼko </ts>
               <ts e="T934" id="Seg_6034" n="e" s="T933">onʼiʔ </ts>
               <ts e="T935" id="Seg_6036" n="e" s="T934">kagat. </ts>
            </ts>
            <ts e="T937" id="Seg_6037" n="sc" s="T936">
               <ts e="T937" id="Seg_6039" n="e" s="T936">((BRK)). </ts>
            </ts>
            <ts e="T941" id="Seg_6040" n="sc" s="T938">
               <ts e="T939" id="Seg_6042" n="e" s="T938">A </ts>
               <ts e="T940" id="Seg_6044" n="e" s="T939">sestrat </ts>
               <ts e="T941" id="Seg_6046" n="e" s="T940">külambi. </ts>
            </ts>
            <ts e="T943" id="Seg_6047" n="sc" s="T942">
               <ts e="T943" id="Seg_6049" n="e" s="T942">((BRK)). </ts>
            </ts>
            <ts e="T949" id="Seg_6050" n="sc" s="T944">
               <ts e="T945" id="Seg_6052" n="e" s="T944">Šödörluʔpiem, </ts>
               <ts e="T946" id="Seg_6054" n="e" s="T945">bar </ts>
               <ts e="T947" id="Seg_6056" n="e" s="T946">tüj </ts>
               <ts e="T948" id="Seg_6058" n="e" s="T947">naga </ts>
               <ts e="T949" id="Seg_6060" n="e" s="T948">šöʔsittə. </ts>
            </ts>
            <ts e="T951" id="Seg_6061" n="sc" s="T950">
               <ts e="T951" id="Seg_6063" n="e" s="T950">((BRK)). </ts>
            </ts>
            <ts e="T961" id="Seg_6064" n="sc" s="T952">
               <ts e="T953" id="Seg_6066" n="e" s="T952">Miʔ </ts>
               <ts e="T954" id="Seg_6068" n="e" s="T953">jelezʼe </ts>
               <ts e="T955" id="Seg_6070" n="e" s="T954">kambibaʔ </ts>
               <ts e="T956" id="Seg_6072" n="e" s="T955">bü </ts>
               <ts e="T957" id="Seg_6074" n="e" s="T956">tažerzittə, </ts>
               <ts e="T958" id="Seg_6076" n="e" s="T957">a </ts>
               <ts e="T959" id="Seg_6078" n="e" s="T958">dĭn </ts>
               <ts e="T960" id="Seg_6080" n="e" s="T959">bü </ts>
               <ts e="T961" id="Seg_6082" n="e" s="T960">ige. </ts>
            </ts>
            <ts e="T970" id="Seg_6083" n="sc" s="T962">
               <ts e="T963" id="Seg_6085" n="e" s="T962">(Nagur </ts>
               <ts e="T964" id="Seg_6087" n="e" s="T963">vin-) </ts>
               <ts e="T965" id="Seg_6089" n="e" s="T964">Nagur </ts>
               <ts e="T966" id="Seg_6091" n="e" s="T965">vedro </ts>
               <ts e="T967" id="Seg_6093" n="e" s="T966">deʔpibeʔ, </ts>
               <ts e="T968" id="Seg_6095" n="e" s="T967">i </ts>
               <ts e="T969" id="Seg_6097" n="e" s="T968">kabarləj </ts>
               <ts e="T970" id="Seg_6099" n="e" s="T969">tüj. </ts>
            </ts>
            <ts e="T972" id="Seg_6100" n="sc" s="T971">
               <ts e="T972" id="Seg_6102" n="e" s="T971">((BRK)). </ts>
            </ts>
            <ts e="T977" id="Seg_6103" n="sc" s="T973">
               <ts e="T974" id="Seg_6105" n="e" s="T973">Dʼijegən </ts>
               <ts e="T975" id="Seg_6107" n="e" s="T974">šonəgam, </ts>
               <ts e="T976" id="Seg_6109" n="e" s="T975">dʼije </ts>
               <ts e="T977" id="Seg_6111" n="e" s="T976">nüjleʔbəm. </ts>
            </ts>
            <ts e="T982" id="Seg_6112" n="sc" s="T978">
               <ts e="T979" id="Seg_6114" n="e" s="T978">Pagən </ts>
               <ts e="T980" id="Seg_6116" n="e" s="T979">šonəgam, </ts>
               <ts e="T981" id="Seg_6118" n="e" s="T980">pa </ts>
               <ts e="T982" id="Seg_6120" n="e" s="T981">nüjleʔbəm. </ts>
            </ts>
            <ts e="T987" id="Seg_6121" n="sc" s="T983">
               <ts e="T984" id="Seg_6123" n="e" s="T983">Sʼtʼep </ts>
               <ts e="T985" id="Seg_6125" n="e" s="T984">šonəgam, </ts>
               <ts e="T986" id="Seg_6127" n="e" s="T985">sʼtʼep </ts>
               <ts e="T987" id="Seg_6129" n="e" s="T986">nüjleʔbəm. </ts>
            </ts>
            <ts e="T992" id="Seg_6130" n="sc" s="T988">
               <ts e="T989" id="Seg_6132" n="e" s="T988">Măja </ts>
               <ts e="T990" id="Seg_6134" n="e" s="T989">šonəgam, </ts>
               <ts e="T991" id="Seg_6136" n="e" s="T990">măja </ts>
               <ts e="T992" id="Seg_6138" n="e" s="T991">nüjleʔbəm. </ts>
            </ts>
            <ts e="T998" id="Seg_6139" n="sc" s="T993">
               <ts e="T994" id="Seg_6141" n="e" s="T993">Dʼălam </ts>
               <ts e="T995" id="Seg_6143" n="e" s="T994">šonəga, </ts>
               <ts e="T996" id="Seg_6145" n="e" s="T995">Dʼălam </ts>
               <ts e="T997" id="Seg_6147" n="e" s="T996">(nujle-) </ts>
               <ts e="T998" id="Seg_6149" n="e" s="T997">nüjleʔbəm. </ts>
            </ts>
            <ts e="T1003" id="Seg_6150" n="sc" s="T999">
               <ts e="T1000" id="Seg_6152" n="e" s="T999">Turagən </ts>
               <ts e="T1001" id="Seg_6154" n="e" s="T1000">amnolaʔbə, </ts>
               <ts e="T1002" id="Seg_6156" n="e" s="T1001">tura </ts>
               <ts e="T1003" id="Seg_6158" n="e" s="T1002">nüjleʔbəm. </ts>
            </ts>
            <ts e="T1005" id="Seg_6159" n="sc" s="T1004">
               <ts e="T1005" id="Seg_6161" n="e" s="T1004">((BRK)). </ts>
            </ts>
            <ts e="T1016" id="Seg_6162" n="sc" s="T1006">
               <ts e="T1007" id="Seg_6164" n="e" s="T1006">(Miʔ=) </ts>
               <ts e="T1008" id="Seg_6166" n="e" s="T1007">Măn </ts>
               <ts e="T1009" id="Seg_6168" n="e" s="T1008">turanʼi </ts>
               <ts e="T1010" id="Seg_6170" n="e" s="T1009">ugandə </ts>
               <ts e="T1011" id="Seg_6172" n="e" s="T1010">tumo </ts>
               <ts e="T1012" id="Seg_6174" n="e" s="T1011">iʔgö. </ts>
               <ts e="T1013" id="Seg_6176" n="e" s="T1012">Ĭmbi </ts>
               <ts e="T1014" id="Seg_6178" n="e" s="T1013">păpală </ts>
               <ts e="T1015" id="Seg_6180" n="e" s="T1014">bar </ts>
               <ts e="T1016" id="Seg_6182" n="e" s="T1015">amniaʔi. </ts>
            </ts>
            <ts e="T1020" id="Seg_6183" n="sc" s="T1017">
               <ts e="T1018" id="Seg_6185" n="e" s="T1017">A </ts>
               <ts e="T1019" id="Seg_6187" n="e" s="T1018">tospak </ts>
               <ts e="T1020" id="Seg_6189" n="e" s="T1019">naga. </ts>
            </ts>
            <ts e="T1032" id="Seg_6190" n="sc" s="T1021">
               <ts e="T1022" id="Seg_6192" n="e" s="T1021">Kanaʔ </ts>
               <ts e="T1023" id="Seg_6194" n="e" s="T1022">miʔnenə, </ts>
               <ts e="T1024" id="Seg_6196" n="e" s="T1023">(dĭ=) </ts>
               <ts e="T1025" id="Seg_6198" n="e" s="T1024">dĭn </ts>
               <ts e="T1026" id="Seg_6200" n="e" s="T1025">šide </ts>
               <ts e="T1027" id="Seg_6202" n="e" s="T1026">(do-) </ts>
               <ts e="T1028" id="Seg_6204" n="e" s="T1027">tospak, </ts>
               <ts e="T1029" id="Seg_6206" n="e" s="T1028">onʼiʔ </ts>
               <ts e="T1030" id="Seg_6208" n="e" s="T1029">sĭre, </ts>
               <ts e="T1031" id="Seg_6210" n="e" s="T1030">onʼiʔ </ts>
               <ts e="T1032" id="Seg_6212" n="e" s="T1031">komə. </ts>
            </ts>
            <ts e="T1036" id="Seg_6213" n="sc" s="T1033">
               <ts e="T1034" id="Seg_6215" n="e" s="T1033">Dĭ </ts>
               <ts e="T1035" id="Seg_6217" n="e" s="T1034">mĭləj </ts>
               <ts e="T1036" id="Seg_6219" n="e" s="T1035">tănan. </ts>
            </ts>
            <ts e="T1048" id="Seg_6220" n="sc" s="T1037">
               <ts e="T1038" id="Seg_6222" n="e" s="T1037">Da </ts>
               <ts e="T1039" id="Seg_6224" n="e" s="T1038">dĭ </ts>
               <ts e="T1040" id="Seg_6226" n="e" s="T1039">sĭre </ts>
               <ts e="T1041" id="Seg_6228" n="e" s="T1040">măna </ts>
               <ts e="T1042" id="Seg_6230" n="e" s="T1041">ibi </ts>
               <ts e="T1043" id="Seg_6232" n="e" s="T1042">da </ts>
               <ts e="T1181" id="Seg_6234" n="e" s="T1043">kalla </ts>
               <ts e="T1044" id="Seg_6236" n="e" s="T1181">dʼürbi, </ts>
               <ts e="T1045" id="Seg_6238" n="e" s="T1044">dĭ </ts>
               <ts e="T1046" id="Seg_6240" n="e" s="T1045">dĭm </ts>
               <ts e="T1047" id="Seg_6242" n="e" s="T1046">ej </ts>
               <ts e="T1048" id="Seg_6244" n="e" s="T1047">öʔlübi. </ts>
            </ts>
            <ts e="T1052" id="Seg_6245" n="sc" s="T1049">
               <ts e="T1050" id="Seg_6247" n="e" s="T1049">(Daška) </ts>
               <ts e="T1051" id="Seg_6249" n="e" s="T1050">ej </ts>
               <ts e="T1052" id="Seg_6251" n="e" s="T1051">šolia. </ts>
            </ts>
            <ts e="T1054" id="Seg_6252" n="sc" s="T1053">
               <ts e="T1054" id="Seg_6254" n="e" s="T1053">((BRK)). </ts>
            </ts>
            <ts e="T1058" id="Seg_6255" n="sc" s="T1055">
               <ts e="T1056" id="Seg_6257" n="e" s="T1055">Măn </ts>
               <ts e="T1057" id="Seg_6259" n="e" s="T1056">kunolzittə </ts>
               <ts e="T1058" id="Seg_6261" n="e" s="T1057">iʔpiem. </ts>
            </ts>
            <ts e="T1068" id="Seg_6262" n="sc" s="T1059">
               <ts e="T1060" id="Seg_6264" n="e" s="T1059">A </ts>
               <ts e="T1061" id="Seg_6266" n="e" s="T1060">dĭn </ts>
               <ts e="T1062" id="Seg_6268" n="e" s="T1061">ugandə </ts>
               <ts e="T1063" id="Seg_6270" n="e" s="T1062">putʼuga, </ts>
               <ts e="T1064" id="Seg_6272" n="e" s="T1063">kuliam, </ts>
               <ts e="T1065" id="Seg_6274" n="e" s="T1064">tüʔ </ts>
               <ts e="T1066" id="Seg_6276" n="e" s="T1065">(ip-) </ts>
               <ts e="T1067" id="Seg_6278" n="e" s="T1066">iʔgö </ts>
               <ts e="T1068" id="Seg_6280" n="e" s="T1067">iʔbolaʔbə. </ts>
            </ts>
            <ts e="T1070" id="Seg_6281" n="sc" s="T1069">
               <ts e="T1070" id="Seg_6283" n="e" s="T1069">((BRK)). </ts>
            </ts>
            <ts e="T1075" id="Seg_6284" n="sc" s="T1071">
               <ts e="T1072" id="Seg_6286" n="e" s="T1071">Ertən </ts>
               <ts e="T1073" id="Seg_6288" n="e" s="T1072">uʔbdəbiam, </ts>
               <ts e="T1074" id="Seg_6290" n="e" s="T1073">kălenkanə </ts>
               <ts e="T1075" id="Seg_6292" n="e" s="T1074">nubiam. </ts>
            </ts>
            <ts e="T1082" id="Seg_6293" n="sc" s="T1076">
               <ts e="T1077" id="Seg_6295" n="e" s="T1076">Kuliom </ts>
               <ts e="T1078" id="Seg_6297" n="e" s="T1077">bar: </ts>
               <ts e="T1079" id="Seg_6299" n="e" s="T1078">dĭn </ts>
               <ts e="T1080" id="Seg_6301" n="e" s="T1079">bʼeʔ </ts>
               <ts e="T1081" id="Seg_6303" n="e" s="T1080">kuči </ts>
               <ts e="T1082" id="Seg_6305" n="e" s="T1081">iʔbolaʔbə. </ts>
            </ts>
            <ts e="T1084" id="Seg_6306" n="sc" s="T1083">
               <ts e="T1084" id="Seg_6308" n="e" s="T1083">((BRK)). </ts>
            </ts>
            <ts e="T1096" id="Seg_6309" n="sc" s="T1085">
               <ts e="T1086" id="Seg_6311" n="e" s="T1085">Dĭgəttə </ts>
               <ts e="T1087" id="Seg_6313" n="e" s="T1086">ujam </ts>
               <ts e="T1088" id="Seg_6315" n="e" s="T1087">kunəlambam </ts>
               <ts e="T1089" id="Seg_6317" n="e" s="T1088">niʔtə, </ts>
               <ts e="T1090" id="Seg_6319" n="e" s="T1089">takšegən. </ts>
               <ts e="T1091" id="Seg_6321" n="e" s="T1090">(Takšezi) </ts>
               <ts e="T1092" id="Seg_6323" n="e" s="T1091">kajliam, </ts>
               <ts e="T1093" id="Seg_6325" n="e" s="T1092">štobɨ </ts>
               <ts e="T1094" id="Seg_6327" n="e" s="T1093">tumoʔi </ts>
               <ts e="T1095" id="Seg_6329" n="e" s="T1094">ej </ts>
               <ts e="T1096" id="Seg_6331" n="e" s="T1095">ambiʔi. </ts>
            </ts>
            <ts e="T1107" id="Seg_6332" n="sc" s="T1097">
               <ts e="T1098" id="Seg_6334" n="e" s="T1097">A </ts>
               <ts e="T1099" id="Seg_6336" n="e" s="T1098">kajaʔ </ts>
               <ts e="T1100" id="Seg_6338" n="e" s="T1099">măslăbojkănə </ts>
               <ts e="T1101" id="Seg_6340" n="e" s="T1100">tože </ts>
               <ts e="T1102" id="Seg_6342" n="e" s="T1101">(ka-) </ts>
               <ts e="T1103" id="Seg_6344" n="e" s="T1102">kajlam, </ts>
               <ts e="T1104" id="Seg_6346" n="e" s="T1103">štobɨ </ts>
               <ts e="T1105" id="Seg_6348" n="e" s="T1104">tumoʔi </ts>
               <ts e="T1106" id="Seg_6350" n="e" s="T1105">ej </ts>
               <ts e="T1107" id="Seg_6352" n="e" s="T1106">ambiʔi. </ts>
            </ts>
            <ts e="T1109" id="Seg_6353" n="sc" s="T1108">
               <ts e="T1109" id="Seg_6355" n="e" s="T1108">((BRK)). </ts>
            </ts>
            <ts e="T1119" id="Seg_6356" n="sc" s="T1110">
               <ts e="T1111" id="Seg_6358" n="e" s="T1110">A </ts>
               <ts e="T1112" id="Seg_6360" n="e" s="T1111">xoš </ts>
               <ts e="T1113" id="Seg_6362" n="e" s="T1112">dăk </ts>
               <ts e="T1114" id="Seg_6364" n="e" s="T1113">miʔnʼibeʔ </ts>
               <ts e="T1115" id="Seg_6366" n="e" s="T1114">šide </ts>
               <ts e="T1116" id="Seg_6368" n="e" s="T1115">ige </ts>
               <ts e="T1117" id="Seg_6370" n="e" s="T1116">(tospak), </ts>
               <ts e="T1118" id="Seg_6372" n="e" s="T1117">detlem </ts>
               <ts e="T1119" id="Seg_6374" n="e" s="T1118">onʼiʔ. </ts>
            </ts>
            <ts e="T1126" id="Seg_6375" n="sc" s="T1120">
               <ts e="T1121" id="Seg_6377" n="e" s="T1120">Girgit </ts>
               <ts e="T1122" id="Seg_6379" n="e" s="T1121">(tănan) </ts>
               <ts e="T1123" id="Seg_6381" n="e" s="T1122">deʔsittə, </ts>
               <ts e="T1124" id="Seg_6383" n="e" s="T1123">sĭre </ts>
               <ts e="T1125" id="Seg_6385" n="e" s="T1124">ali </ts>
               <ts e="T1126" id="Seg_6387" n="e" s="T1125">komə? </ts>
            </ts>
            <ts e="T1130" id="Seg_6388" n="sc" s="T1127">
               <ts e="T1128" id="Seg_6390" n="e" s="T1127">"Nu </ts>
               <ts e="T1129" id="Seg_6392" n="e" s="T1128">deʔ </ts>
               <ts e="T1130" id="Seg_6394" n="e" s="T1129">komə". </ts>
            </ts>
            <ts e="T1132" id="Seg_6395" n="sc" s="T1131">
               <ts e="T1132" id="Seg_6397" n="e" s="T1131">((BRK)). </ts>
            </ts>
            <ts e="T1139" id="Seg_6398" n="sc" s="T1133">
               <ts e="T1134" id="Seg_6400" n="e" s="T1133">Moltʼanə </ts>
               <ts e="T1135" id="Seg_6402" n="e" s="T1134">šoga, </ts>
               <ts e="T1136" id="Seg_6404" n="e" s="T1135">dĭn </ts>
               <ts e="T1137" id="Seg_6406" n="e" s="T1136">ugandə </ts>
               <ts e="T1138" id="Seg_6408" n="e" s="T1137">bü </ts>
               <ts e="T1139" id="Seg_6410" n="e" s="T1138">iʔgö. </ts>
            </ts>
            <ts e="T1146" id="Seg_6411" n="sc" s="T1140">
               <ts e="T1141" id="Seg_6413" n="e" s="T1140">Dʼibige </ts>
               <ts e="T1142" id="Seg_6415" n="e" s="T1141">bü </ts>
               <ts e="T1143" id="Seg_6417" n="e" s="T1142">ige, </ts>
               <ts e="T1144" id="Seg_6419" n="e" s="T1143">šišege, </ts>
               <ts e="T1145" id="Seg_6421" n="e" s="T1144">sabən </ts>
               <ts e="T1146" id="Seg_6423" n="e" s="T1145">ige. </ts>
            </ts>
            <ts e="T1150" id="Seg_6424" n="sc" s="T1147">
               <ts e="T1148" id="Seg_6426" n="e" s="T1147">Ejü, </ts>
               <ts e="T1149" id="Seg_6428" n="e" s="T1148">(ejümnel) </ts>
               <ts e="T1150" id="Seg_6430" n="e" s="T1149">bar. </ts>
            </ts>
            <ts e="T1152" id="Seg_6431" n="sc" s="T1151">
               <ts e="T1152" id="Seg_6433" n="e" s="T1151">((BRK)). </ts>
            </ts>
            <ts e="T1159" id="Seg_6434" n="sc" s="T1153">
               <ts e="T1154" id="Seg_6436" n="e" s="T1153">Püjel </ts>
               <ts e="T1155" id="Seg_6438" n="e" s="T1154">ej </ts>
               <ts e="T1156" id="Seg_6440" n="e" s="T1155">ĭzemne </ts>
               <ts e="T1157" id="Seg_6442" n="e" s="T1156">i </ts>
               <ts e="T1158" id="Seg_6444" n="e" s="T1157">kokluʔi </ts>
               <ts e="T1159" id="Seg_6446" n="e" s="T1158">mʼaŋnaʔi. </ts>
            </ts>
            <ts e="T1161" id="Seg_6447" n="sc" s="T1160">
               <ts e="T1161" id="Seg_6449" n="e" s="T1160">((BRK)). </ts>
            </ts>
            <ts e="T1168" id="Seg_6450" n="sc" s="T1162">
               <ts e="T1163" id="Seg_6452" n="e" s="T1162">Dö </ts>
               <ts e="T1164" id="Seg_6454" n="e" s="T1163">zaplatka, </ts>
               <ts e="T1165" id="Seg_6456" n="e" s="T1164">a </ts>
               <ts e="T1166" id="Seg_6458" n="e" s="T1165">gibər </ts>
               <ts e="T1167" id="Seg_6460" n="e" s="T1166">dĭm </ts>
               <ts e="T1168" id="Seg_6462" n="e" s="T1167">zaplatka. </ts>
            </ts>
            <ts e="T1172" id="Seg_6463" n="sc" s="T1169">
               <ts e="T1170" id="Seg_6465" n="e" s="T1169">Kötengəndə </ts>
               <ts e="T1171" id="Seg_6467" n="e" s="T1170">amnolzittə, </ts>
               <ts e="T1172" id="Seg_6469" n="e" s="T1171">što_li. </ts>
            </ts>
            <ts e="T1177" id="Seg_6470" n="sc" s="T1173">
               <ts e="T1174" id="Seg_6472" n="e" s="T1173">Barəbtɨʔ, </ts>
               <ts e="T1175" id="Seg_6474" n="e" s="T1174">ej </ts>
               <ts e="T1176" id="Seg_6476" n="e" s="T1175">kereʔ </ts>
               <ts e="T1177" id="Seg_6478" n="e" s="T1176">măna. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_6479" s="T1">PKZ_196X_SU0229.001 (001)</ta>
            <ta e="T14" id="Seg_6480" s="T6">PKZ_196X_SU0229.002 (002)</ta>
            <ta e="T22" id="Seg_6481" s="T15">PKZ_196X_SU0229.003 (003)</ta>
            <ta e="T27" id="Seg_6482" s="T23">PKZ_196X_SU0229.004 (004)</ta>
            <ta e="T30" id="Seg_6483" s="T28">PKZ_196X_SU0229.005 (005)</ta>
            <ta e="T32" id="Seg_6484" s="T31">PKZ_196X_SU0229.006 (006)</ta>
            <ta e="T34" id="Seg_6485" s="T33">PKZ_196X_SU0229.007 (007)</ta>
            <ta e="T39" id="Seg_6486" s="T35">PKZ_196X_SU0229.008 (008)</ta>
            <ta e="T42" id="Seg_6487" s="T40">PKZ_196X_SU0229.009 (009)</ta>
            <ta e="T47" id="Seg_6488" s="T43">PKZ_196X_SU0229.010 (010)</ta>
            <ta e="T52" id="Seg_6489" s="T48">PKZ_196X_SU0229.011 (011)</ta>
            <ta e="T54" id="Seg_6490" s="T53">PKZ_196X_SU0229.012 (012)</ta>
            <ta e="T61" id="Seg_6491" s="T55">PKZ_196X_SU0229.013 (013)</ta>
            <ta e="T71" id="Seg_6492" s="T62">PKZ_196X_SU0229.014 (014)</ta>
            <ta e="T78" id="Seg_6493" s="T71">PKZ_196X_SU0229.015 (015)</ta>
            <ta e="T81" id="Seg_6494" s="T79">PKZ_196X_SU0229.016 (016)</ta>
            <ta e="T91" id="Seg_6495" s="T82">PKZ_196X_SU0229.017 (017)</ta>
            <ta e="T95" id="Seg_6496" s="T92">PKZ_196X_SU0229.018 (018)</ta>
            <ta e="T99" id="Seg_6497" s="T96">PKZ_196X_SU0229.019 (019)</ta>
            <ta e="T102" id="Seg_6498" s="T100">PKZ_196X_SU0229.020 (020)</ta>
            <ta e="T107" id="Seg_6499" s="T103">PKZ_196X_SU0229.021 (021)</ta>
            <ta e="T109" id="Seg_6500" s="T108">PKZ_196X_SU0229.022 (022)</ta>
            <ta e="T115" id="Seg_6501" s="T110">PKZ_196X_SU0229.023 (023)</ta>
            <ta e="T120" id="Seg_6502" s="T116">PKZ_196X_SU0229.024 (024)</ta>
            <ta e="T124" id="Seg_6503" s="T121">PKZ_196X_SU0229.025 (025)</ta>
            <ta e="T131" id="Seg_6504" s="T125">PKZ_196X_SU0229.026 (026)</ta>
            <ta e="T136" id="Seg_6505" s="T132">PKZ_196X_SU0229.027 (027)</ta>
            <ta e="T139" id="Seg_6506" s="T137">PKZ_196X_SU0229.028 (028)</ta>
            <ta e="T141" id="Seg_6507" s="T140">PKZ_196X_SU0229.029 (029)</ta>
            <ta e="T149" id="Seg_6508" s="T142">PKZ_196X_SU0229.030 (030)</ta>
            <ta e="T151" id="Seg_6509" s="T150">PKZ_196X_SU0229.031 (031)</ta>
            <ta e="T155" id="Seg_6510" s="T152">PKZ_196X_SU0229.032 (032)</ta>
            <ta e="T165" id="Seg_6511" s="T156">PKZ_196X_SU0229.033 (033)</ta>
            <ta e="T170" id="Seg_6512" s="T166">PKZ_196X_SU0229.034 (034)</ta>
            <ta e="T178" id="Seg_6513" s="T171">PKZ_196X_SU0229.035 (035)</ta>
            <ta e="T183" id="Seg_6514" s="T179">PKZ_196X_SU0229.036 (036)</ta>
            <ta e="T189" id="Seg_6515" s="T184">PKZ_196X_SU0229.037 (037)</ta>
            <ta e="T196" id="Seg_6516" s="T189">PKZ_196X_SU0229.038 (038)</ta>
            <ta e="T202" id="Seg_6517" s="T197">PKZ_196X_SU0229.039 (039)</ta>
            <ta e="T208" id="Seg_6518" s="T203">PKZ_196X_SU0229.040 (040)</ta>
            <ta e="T213" id="Seg_6519" s="T208">PKZ_196X_SU0229.041 (041)</ta>
            <ta e="T218" id="Seg_6520" s="T214">PKZ_196X_SU0229.042 (042)</ta>
            <ta e="T227" id="Seg_6521" s="T219">PKZ_196X_SU0229.043 (043)</ta>
            <ta e="T229" id="Seg_6522" s="T228">PKZ_196X_SU0229.044 (044)</ta>
            <ta e="T237" id="Seg_6523" s="T230">PKZ_196X_SU0229.045 (045)</ta>
            <ta e="T239" id="Seg_6524" s="T238">PKZ_196X_SU0229.046 (046)</ta>
            <ta e="T248" id="Seg_6525" s="T240">PKZ_196X_SU0229.047 (047)</ta>
            <ta e="T257" id="Seg_6526" s="T249">PKZ_196X_SU0229.048 (048)</ta>
            <ta e="T263" id="Seg_6527" s="T258">PKZ_196X_SU0229.049 (049)</ta>
            <ta e="T270" id="Seg_6528" s="T264">PKZ_196X_SU0229.050 (050)</ta>
            <ta e="T277" id="Seg_6529" s="T270">PKZ_196X_SU0229.051 (051)</ta>
            <ta e="T284" id="Seg_6530" s="T278">PKZ_196X_SU0229.052 (052)</ta>
            <ta e="T292" id="Seg_6531" s="T285">PKZ_196X_SU0229.053 (053)</ta>
            <ta e="T303" id="Seg_6532" s="T293">PKZ_196X_SU0229.054 (054)</ta>
            <ta e="T309" id="Seg_6533" s="T304">PKZ_196X_SU0229.055 (055)</ta>
            <ta e="T313" id="Seg_6534" s="T310">PKZ_196X_SU0229.056 (056)</ta>
            <ta e="T315" id="Seg_6535" s="T314">PKZ_196X_SU0229.057 (057)</ta>
            <ta e="T322" id="Seg_6536" s="T316">PKZ_196X_SU0229.058 (058)</ta>
            <ta e="T326" id="Seg_6537" s="T323">PKZ_196X_SU0229.059 (059)</ta>
            <ta e="T331" id="Seg_6538" s="T327">PKZ_196X_SU0229.060 (060)</ta>
            <ta e="T334" id="Seg_6539" s="T332">PKZ_196X_SU0229.061 (061)</ta>
            <ta e="T337" id="Seg_6540" s="T335">PKZ_196X_SU0229.062 (062)</ta>
            <ta e="T341" id="Seg_6541" s="T337">PKZ_196X_SU0229.063 (063)</ta>
            <ta e="T344" id="Seg_6542" s="T342">PKZ_196X_SU0229.064 (064)</ta>
            <ta e="T349" id="Seg_6543" s="T344">PKZ_196X_SU0229.065 (065)</ta>
            <ta e="T352" id="Seg_6544" s="T350">PKZ_196X_SU0229.066 (066)</ta>
            <ta e="T358" id="Seg_6545" s="T353">PKZ_196X_SU0229.067 (067)</ta>
            <ta e="T362" id="Seg_6546" s="T359">PKZ_196X_SU0229.068 (068)</ta>
            <ta e="T372" id="Seg_6547" s="T363">PKZ_196X_SU0229.069 (069)</ta>
            <ta e="T374" id="Seg_6548" s="T373">PKZ_196X_SU0229.070 (070)</ta>
            <ta e="T382" id="Seg_6549" s="T375">PKZ_196X_SU0229.071 (071)</ta>
            <ta e="T391" id="Seg_6550" s="T383">PKZ_196X_SU0229.072 (072)</ta>
            <ta e="T397" id="Seg_6551" s="T392">PKZ_196X_SU0229.073 (073)</ta>
            <ta e="T401" id="Seg_6552" s="T397">PKZ_196X_SU0229.074 (074)</ta>
            <ta e="T403" id="Seg_6553" s="T402">PKZ_196X_SU0229.075 (075)</ta>
            <ta e="T405" id="Seg_6554" s="T404">PKZ_196X_SU0229.076 (076)</ta>
            <ta e="T417" id="Seg_6555" s="T406">PKZ_196X_SU0229.077 (077)</ta>
            <ta e="T419" id="Seg_6556" s="T418">PKZ_196X_SU0229.078 (078)</ta>
            <ta e="T422" id="Seg_6557" s="T420">PKZ_196X_SU0229.079 (079)</ta>
            <ta e="T424" id="Seg_6558" s="T423">PKZ_196X_SU0229.080 (080)</ta>
            <ta e="T426" id="Seg_6559" s="T425">PKZ_196X_SU0229.081 (081)</ta>
            <ta e="T434" id="Seg_6560" s="T427">PKZ_196X_SU0229.082 (082)</ta>
            <ta e="T440" id="Seg_6561" s="T435">PKZ_196X_SU0229.083 (083)</ta>
            <ta e="T442" id="Seg_6562" s="T441">PKZ_196X_SU0229.084 (084)</ta>
            <ta e="T447" id="Seg_6563" s="T443">PKZ_196X_SU0229.085 (085)</ta>
            <ta e="T452" id="Seg_6564" s="T448">PKZ_196X_SU0229.086 (086)</ta>
            <ta e="T454" id="Seg_6565" s="T453">PKZ_196X_SU0229.087 (087)</ta>
            <ta e="T458" id="Seg_6566" s="T455">PKZ_196X_SU0229.088 (088)</ta>
            <ta e="T464" id="Seg_6567" s="T459">PKZ_196X_SU0229.089 (089)</ta>
            <ta e="T466" id="Seg_6568" s="T465">PKZ_196X_SU0229.090 (090)</ta>
            <ta e="T474" id="Seg_6569" s="T467">PKZ_196X_SU0229.091 (091)</ta>
            <ta e="T476" id="Seg_6570" s="T475">PKZ_196X_SU0229.092 (092)</ta>
            <ta e="T480" id="Seg_6571" s="T477">PKZ_196X_SU0229.093 (093)</ta>
            <ta e="T487" id="Seg_6572" s="T481">PKZ_196X_SU0229.094 (094)</ta>
            <ta e="T489" id="Seg_6573" s="T488">PKZ_196X_SU0229.095 (095)</ta>
            <ta e="T494" id="Seg_6574" s="T490">PKZ_196X_SU0229.096 (096)</ta>
            <ta e="T496" id="Seg_6575" s="T495">PKZ_196X_SU0229.097 (097)</ta>
            <ta e="T499" id="Seg_6576" s="T496">PKZ_196X_SU0229.098 (098)</ta>
            <ta e="T504" id="Seg_6577" s="T500">PKZ_196X_SU0229.099 (099)</ta>
            <ta e="T506" id="Seg_6578" s="T505">PKZ_196X_SU0229.100 (100)</ta>
            <ta e="T514" id="Seg_6579" s="T507">PKZ_196X_SU0229.101 (101)</ta>
            <ta e="T520" id="Seg_6580" s="T515">PKZ_196X_SU0229.102 (102)</ta>
            <ta e="T526" id="Seg_6581" s="T521">PKZ_196X_SU0229.103 (103)</ta>
            <ta e="T528" id="Seg_6582" s="T527">PKZ_196X_SU0229.104 (104)</ta>
            <ta e="T533" id="Seg_6583" s="T529">PKZ_196X_SU0229.105 (105)</ta>
            <ta e="T537" id="Seg_6584" s="T534">PKZ_196X_SU0229.106 (106)</ta>
            <ta e="T540" id="Seg_6585" s="T537">PKZ_196X_SU0229.107 (107)</ta>
            <ta e="T545" id="Seg_6586" s="T541">PKZ_196X_SU0229.108 (108)</ta>
            <ta e="T547" id="Seg_6587" s="T546">PKZ_196X_SU0229.109 (109)</ta>
            <ta e="T551" id="Seg_6588" s="T547">PKZ_196X_SU0229.110 (110)</ta>
            <ta e="T556" id="Seg_6589" s="T552">PKZ_196X_SU0229.111 (111)</ta>
            <ta e="T558" id="Seg_6590" s="T557">PKZ_196X_SU0229.112 (112)</ta>
            <ta e="T564" id="Seg_6591" s="T559">PKZ_196X_SU0229.113 (113)</ta>
            <ta e="T568" id="Seg_6592" s="T565">PKZ_196X_SU0229.114 (114)</ta>
            <ta e="T573" id="Seg_6593" s="T569">PKZ_196X_SU0229.115 (115)</ta>
            <ta e="T580" id="Seg_6594" s="T574">PKZ_196X_SU0229.116 (116)</ta>
            <ta e="T589" id="Seg_6595" s="T581">PKZ_196X_SU0229.117 (117)</ta>
            <ta e="T596" id="Seg_6596" s="T590">PKZ_196X_SU0229.118 (118)</ta>
            <ta e="T600" id="Seg_6597" s="T597">PKZ_196X_SU0229.119 (119)</ta>
            <ta e="T610" id="Seg_6598" s="T601">PKZ_196X_SU0229.120 (120)</ta>
            <ta e="T612" id="Seg_6599" s="T611">PKZ_196X_SU0229.121 (121)</ta>
            <ta e="T619" id="Seg_6600" s="T613">PKZ_196X_SU0229.122 (122)</ta>
            <ta e="T624" id="Seg_6601" s="T620">PKZ_196X_SU0229.123 (123)</ta>
            <ta e="T630" id="Seg_6602" s="T625">PKZ_196X_SU0229.124 (124)</ta>
            <ta e="T635" id="Seg_6603" s="T631">PKZ_196X_SU0229.125 (125)</ta>
            <ta e="T637" id="Seg_6604" s="T636">PKZ_196X_SU0229.126 (126)</ta>
            <ta e="T643" id="Seg_6605" s="T638">PKZ_196X_SU0229.127 (127)</ta>
            <ta e="T649" id="Seg_6606" s="T643">PKZ_196X_SU0229.128 (128)</ta>
            <ta e="T651" id="Seg_6607" s="T650">PKZ_196X_SU0229.129 (129)</ta>
            <ta e="T662" id="Seg_6608" s="T652">PKZ_196X_SU0229.130 (130)</ta>
            <ta e="T664" id="Seg_6609" s="T663">PKZ_196X_SU0229.131 (131)</ta>
            <ta e="T669" id="Seg_6610" s="T665">PKZ_196X_SU0229.132 (132)</ta>
            <ta e="T671" id="Seg_6611" s="T670">PKZ_196X_SU0229.133 (133)</ta>
            <ta e="T678" id="Seg_6612" s="T672">PKZ_196X_SU0229.134 (134)</ta>
            <ta e="T684" id="Seg_6613" s="T679">PKZ_196X_SU0229.135 (135)</ta>
            <ta e="T691" id="Seg_6614" s="T685">PKZ_196X_SU0229.136 (136)</ta>
            <ta e="T693" id="Seg_6615" s="T692">PKZ_196X_SU0229.137 (137)</ta>
            <ta e="T695" id="Seg_6616" s="T694">PKZ_196X_SU0229.138 (138)</ta>
            <ta e="T696" id="Seg_6617" s="T695">PKZ_196X_SU0229.139 (139)</ta>
            <ta e="T700" id="Seg_6618" s="T697">PKZ_196X_SU0229.140 (140)</ta>
            <ta e="T708" id="Seg_6619" s="T700">PKZ_196X_SU0229.141 (141)</ta>
            <ta e="T711" id="Seg_6620" s="T708">PKZ_196X_SU0229.142 (142)</ta>
            <ta e="T715" id="Seg_6621" s="T712">PKZ_196X_SU0229.143 (143)</ta>
            <ta e="T719" id="Seg_6622" s="T716">PKZ_196X_SU0229.144 (144)</ta>
            <ta e="T721" id="Seg_6623" s="T720">PKZ_196X_SU0229.145 (145)</ta>
            <ta e="T726" id="Seg_6624" s="T722">PKZ_196X_SU0229.146 (146)</ta>
            <ta e="T730" id="Seg_6625" s="T726">PKZ_196X_SU0229.147 (147)</ta>
            <ta e="T736" id="Seg_6626" s="T731">PKZ_196X_SU0229.148 (148)</ta>
            <ta e="T738" id="Seg_6627" s="T737">PKZ_196X_SU0229.149 (149)</ta>
            <ta e="T747" id="Seg_6628" s="T739">PKZ_196X_SU0229.150 (150)</ta>
            <ta e="T749" id="Seg_6629" s="T748">PKZ_196X_SU0229.151 (151)</ta>
            <ta e="T753" id="Seg_6630" s="T750">PKZ_196X_SU0229.152 (152)</ta>
            <ta e="T761" id="Seg_6631" s="T754">PKZ_196X_SU0229.153 (153)</ta>
            <ta e="T769" id="Seg_6632" s="T762">PKZ_196X_SU0229.154 (154)</ta>
            <ta e="T775" id="Seg_6633" s="T770">PKZ_196X_SU0229.155 (155)</ta>
            <ta e="T781" id="Seg_6634" s="T775">PKZ_196X_SU0229.156 (156)</ta>
            <ta e="T785" id="Seg_6635" s="T782">PKZ_196X_SU0229.157 (157)</ta>
            <ta e="T789" id="Seg_6636" s="T786">PKZ_196X_SU0229.158 (158)</ta>
            <ta e="T791" id="Seg_6637" s="T790">PKZ_196X_SU0229.159 (159)</ta>
            <ta e="T800" id="Seg_6638" s="T792">PKZ_196X_SU0229.160 (160)</ta>
            <ta e="T811" id="Seg_6639" s="T800">PKZ_196X_SU0229.161 (161)</ta>
            <ta e="T813" id="Seg_6640" s="T812">PKZ_196X_SU0229.162 (162)</ta>
            <ta e="T817" id="Seg_6641" s="T814">PKZ_196X_SU0229.163 (163)</ta>
            <ta e="T823" id="Seg_6642" s="T817">PKZ_196X_SU0229.164 (164)</ta>
            <ta e="T831" id="Seg_6643" s="T824">PKZ_196X_SU0229.165 (165)</ta>
            <ta e="T837" id="Seg_6644" s="T832">PKZ_196X_SU0229.166 (166)</ta>
            <ta e="T839" id="Seg_6645" s="T838">PKZ_196X_SU0229.167 (167)</ta>
            <ta e="T844" id="Seg_6646" s="T840">PKZ_196X_SU0229.168 (168)</ta>
            <ta e="T848" id="Seg_6647" s="T844">PKZ_196X_SU0229.169 (169)</ta>
            <ta e="T854" id="Seg_6648" s="T848">PKZ_196X_SU0229.170 (170)</ta>
            <ta e="T856" id="Seg_6649" s="T855">PKZ_196X_SU0229.171 (171)</ta>
            <ta e="T861" id="Seg_6650" s="T857">PKZ_196X_SU0229.172 (172)</ta>
            <ta e="T867" id="Seg_6651" s="T862">PKZ_196X_SU0229.173 (173)</ta>
            <ta e="T871" id="Seg_6652" s="T868">PKZ_196X_SU0229.174 (174)</ta>
            <ta e="T873" id="Seg_6653" s="T872">PKZ_196X_SU0229.175 (175)</ta>
            <ta e="T880" id="Seg_6654" s="T874">PKZ_196X_SU0229.176 (176)</ta>
            <ta e="T890" id="Seg_6655" s="T881">PKZ_196X_SU0229.177 (177)</ta>
            <ta e="T892" id="Seg_6656" s="T891">PKZ_196X_SU0229.178 (178)</ta>
            <ta e="T900" id="Seg_6657" s="T893">PKZ_196X_SU0229.179 (179)</ta>
            <ta e="T903" id="Seg_6658" s="T901">PKZ_196X_SU0229.180 (180)</ta>
            <ta e="T905" id="Seg_6659" s="T904">PKZ_196X_SU0229.181 (181)</ta>
            <ta e="T916" id="Seg_6660" s="T906">PKZ_196X_SU0229.182 (182)</ta>
            <ta e="T918" id="Seg_6661" s="T917">PKZ_196X_SU0229.183 (183)</ta>
            <ta e="T923" id="Seg_6662" s="T919">PKZ_196X_SU0229.184 (184)</ta>
            <ta e="T925" id="Seg_6663" s="T924">PKZ_196X_SU0229.185 (185)</ta>
            <ta e="T935" id="Seg_6664" s="T926">PKZ_196X_SU0229.186 (186)</ta>
            <ta e="T937" id="Seg_6665" s="T936">PKZ_196X_SU0229.187 (187)</ta>
            <ta e="T941" id="Seg_6666" s="T938">PKZ_196X_SU0229.188 (188)</ta>
            <ta e="T943" id="Seg_6667" s="T942">PKZ_196X_SU0229.189 (189)</ta>
            <ta e="T949" id="Seg_6668" s="T944">PKZ_196X_SU0229.190 (190)</ta>
            <ta e="T951" id="Seg_6669" s="T950">PKZ_196X_SU0229.191 (191)</ta>
            <ta e="T961" id="Seg_6670" s="T952">PKZ_196X_SU0229.192 (192)</ta>
            <ta e="T970" id="Seg_6671" s="T962">PKZ_196X_SU0229.193 (193)</ta>
            <ta e="T972" id="Seg_6672" s="T971">PKZ_196X_SU0229.194 (194)</ta>
            <ta e="T977" id="Seg_6673" s="T973">PKZ_196X_SU0229.195 (195)</ta>
            <ta e="T982" id="Seg_6674" s="T978">PKZ_196X_SU0229.196 (196)</ta>
            <ta e="T987" id="Seg_6675" s="T983">PKZ_196X_SU0229.197 (197)</ta>
            <ta e="T992" id="Seg_6676" s="T988">PKZ_196X_SU0229.198 (198)</ta>
            <ta e="T998" id="Seg_6677" s="T993">PKZ_196X_SU0229.199 (199)</ta>
            <ta e="T1003" id="Seg_6678" s="T999">PKZ_196X_SU0229.200 (200)</ta>
            <ta e="T1005" id="Seg_6679" s="T1004">PKZ_196X_SU0229.201 (201)</ta>
            <ta e="T1012" id="Seg_6680" s="T1006">PKZ_196X_SU0229.202 (202)</ta>
            <ta e="T1016" id="Seg_6681" s="T1012">PKZ_196X_SU0229.203 (203)</ta>
            <ta e="T1020" id="Seg_6682" s="T1017">PKZ_196X_SU0229.204 (204)</ta>
            <ta e="T1032" id="Seg_6683" s="T1021">PKZ_196X_SU0229.205 (205)</ta>
            <ta e="T1036" id="Seg_6684" s="T1033">PKZ_196X_SU0229.206 (206)</ta>
            <ta e="T1048" id="Seg_6685" s="T1037">PKZ_196X_SU0229.207 (207)</ta>
            <ta e="T1052" id="Seg_6686" s="T1049">PKZ_196X_SU0229.208 (208)</ta>
            <ta e="T1054" id="Seg_6687" s="T1053">PKZ_196X_SU0229.209 (209)</ta>
            <ta e="T1058" id="Seg_6688" s="T1055">PKZ_196X_SU0229.210 (210)</ta>
            <ta e="T1068" id="Seg_6689" s="T1059">PKZ_196X_SU0229.211 (211)</ta>
            <ta e="T1070" id="Seg_6690" s="T1069">PKZ_196X_SU0229.212 (212)</ta>
            <ta e="T1075" id="Seg_6691" s="T1071">PKZ_196X_SU0229.213 (213)</ta>
            <ta e="T1082" id="Seg_6692" s="T1076">PKZ_196X_SU0229.214 (214)</ta>
            <ta e="T1084" id="Seg_6693" s="T1083">PKZ_196X_SU0229.215 (215)</ta>
            <ta e="T1090" id="Seg_6694" s="T1085">PKZ_196X_SU0229.216 (216)</ta>
            <ta e="T1096" id="Seg_6695" s="T1090">PKZ_196X_SU0229.217 (217)</ta>
            <ta e="T1107" id="Seg_6696" s="T1097">PKZ_196X_SU0229.218 (218)</ta>
            <ta e="T1109" id="Seg_6697" s="T1108">PKZ_196X_SU0229.219 (219)</ta>
            <ta e="T1119" id="Seg_6698" s="T1110">PKZ_196X_SU0229.220 (220)</ta>
            <ta e="T1126" id="Seg_6699" s="T1120">PKZ_196X_SU0229.221 (221)</ta>
            <ta e="T1130" id="Seg_6700" s="T1127">PKZ_196X_SU0229.222 (222)</ta>
            <ta e="T1132" id="Seg_6701" s="T1131">PKZ_196X_SU0229.223 (223)</ta>
            <ta e="T1139" id="Seg_6702" s="T1133">PKZ_196X_SU0229.224 (224)</ta>
            <ta e="T1146" id="Seg_6703" s="T1140">PKZ_196X_SU0229.225 (225)</ta>
            <ta e="T1150" id="Seg_6704" s="T1147">PKZ_196X_SU0229.226 (226)</ta>
            <ta e="T1152" id="Seg_6705" s="T1151">PKZ_196X_SU0229.227 (227)</ta>
            <ta e="T1159" id="Seg_6706" s="T1153">PKZ_196X_SU0229.228 (228)</ta>
            <ta e="T1161" id="Seg_6707" s="T1160">PKZ_196X_SU0229.229 (229)</ta>
            <ta e="T1168" id="Seg_6708" s="T1162">PKZ_196X_SU0229.230 (230)</ta>
            <ta e="T1172" id="Seg_6709" s="T1169">PKZ_196X_SU0229.231 (231)</ta>
            <ta e="T1177" id="Seg_6710" s="T1173">PKZ_196X_SU0229.232 (232)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_6711" s="T1">Măn taldʼen kambiam tuganbə. </ta>
            <ta e="T14" id="Seg_6712" s="T6">(Măn) mămbiam: "Iʔ kuroʔ", măna nelʼzʼa bɨlo šoʔsittə. </ta>
            <ta e="T22" id="Seg_6713" s="T15">Meim ej kambi, i măn ej kambiam. </ta>
            <ta e="T27" id="Seg_6714" s="T23">Dĭzeŋ krăvatʼkən iʔbolaʔbəʔjə, kamrolaʔbəʔjə. </ta>
            <ta e="T30" id="Seg_6715" s="T28">Kolʼa Dunʼatsi. </ta>
            <ta e="T32" id="Seg_6716" s="T31">Pănarlaʔbəʔjə. </ta>
            <ta e="T34" id="Seg_6717" s="T33">Kaknarlaʔbəʔjə. </ta>
            <ta e="T39" id="Seg_6718" s="T35">Măndəʔi: "Urgaja, amaʔ, amnaʔ. </ta>
            <ta e="T42" id="Seg_6719" s="T40">Ara bĭdeʔ". </ta>
            <ta e="T47" id="Seg_6720" s="T43">Măn măndəm: "Ej bĭtləm". </ta>
            <ta e="T52" id="Seg_6721" s="T48">I dĭgəttə kalla dʼürbiem maʔnʼi. </ta>
            <ta e="T54" id="Seg_6722" s="T53">((BRK)). </ta>
            <ta e="T61" id="Seg_6723" s="T55">Măn teinen nükenə kambiam, dĭ ĭzemnie. </ta>
            <ta e="T71" id="Seg_6724" s="T62">Dĭ măndə: "Ĭmbi kondʼo ej šobiam, măn bar kulandəgam". </ta>
            <ta e="T78" id="Seg_6725" s="T71">"Tăn üge kulandəgal, a küsʼtə ej molial. </ta>
            <ta e="T81" id="Seg_6726" s="T79">No, küʔ. </ta>
            <ta e="T91" id="Seg_6727" s="T82">Măna ĭmbi, (m-) ĭmbi măn tăn tondə amnolaʔləm što li?" </ta>
            <ta e="T95" id="Seg_6728" s="T92">Măn vedʼ jilə amnoʔlaʔbəʔjə. </ta>
            <ta e="T99" id="Seg_6729" s="T96">Dĭn edəʔleʔbəʔjə măna. </ta>
            <ta e="T102" id="Seg_6730" s="T100">Dʼăbaktərzittə nada. </ta>
            <ta e="T107" id="Seg_6731" s="T103">A tăn tondə amnoʔ". </ta>
            <ta e="T109" id="Seg_6732" s="T108">((BRK)). </ta>
            <ta e="T115" id="Seg_6733" s="T110">Meim (š-) šide dʼala ibi. </ta>
            <ta e="T120" id="Seg_6734" s="T116">A tăn tondə amnoʔ. </ta>
            <ta e="T124" id="Seg_6735" s="T121">Da jil mĭŋgəʔi". </ta>
            <ta e="T131" id="Seg_6736" s="T125">A dĭ măndə: "A kudaj dĭzəŋzi". </ta>
            <ta e="T136" id="Seg_6737" s="T132">"A tănzi pušaj kudaj! </ta>
            <ta e="T139" id="Seg_6738" s="T137">(Küʔ=) Küʔ". </ta>
            <ta e="T141" id="Seg_6739" s="T140">((BRK)). </ta>
            <ta e="T149" id="Seg_6740" s="T142">Dĭgəttə dĭ (malubi), a măn maʔnʼi kalla dʼürbiem. </ta>
            <ta e="T151" id="Seg_6741" s="T150">((BRK)). </ta>
            <ta e="T155" id="Seg_6742" s="T152">Măn Kazan turanə kambiam. </ta>
            <ta e="T165" id="Seg_6743" s="T156">A dĭ nükə kambi, dĭn tondə dĭn ne amnolaʔbə. </ta>
            <ta e="T170" id="Seg_6744" s="T166">Šobi (dam) da kalla dʼürbi. </ta>
            <ta e="T178" id="Seg_6745" s="T171">"Măn külam, a šində măna (bĭzdl-) bĭzəjdləj?" </ta>
            <ta e="T183" id="Seg_6746" s="T179">Dĭzeŋ bar kaknarbiʔi tăŋ. </ta>
            <ta e="T189" id="Seg_6747" s="T184">Dĭgəttə măn sobiam, kambiam dĭʔnə. </ta>
            <ta e="T196" id="Seg_6748" s="T189">Tăn kalla dʼürbiem, a măn küllambiam i kannambiam. </ta>
            <ta e="T202" id="Seg_6749" s="T197">A măn mămbiam: "Nu, kănnaʔ. </ta>
            <ta e="T208" id="Seg_6750" s="T203">Ĭmbi, (i kl-) i küʔ. </ta>
            <ta e="T213" id="Seg_6751" s="T208">(Mɨ bɨ) Măn bü ejümləm. </ta>
            <ta e="T218" id="Seg_6752" s="T214">I tănan (kămnal-) kămlal. </ta>
            <ta e="T227" id="Seg_6753" s="T219">Dĭgəttə bĭzəlim, i (dʼunə tĭl- tĭl-) dʼünə enləbim. </ta>
            <ta e="T229" id="Seg_6754" s="T228">((BRK)). </ta>
            <ta e="T237" id="Seg_6755" s="T230">Как я говорила, погоди, как я начала? </ta>
            <ta e="T239" id="Seg_6756" s="T238">((BRK)). </ta>
            <ta e="T248" id="Seg_6757" s="T240">Dĭ nüke bar măna: "Nendeʔ moltʼa, bĭzaʔ măna. </ta>
            <ta e="T257" id="Seg_6758" s="T249">Külalləm dăk, dĭgəttə kadəldə də, udazaŋ də bĭzəlil". </ta>
            <ta e="T263" id="Seg_6759" s="T258">Măn dĭgəttə nendəbiem (voldʼa-) moltʼa. </ta>
            <ta e="T270" id="Seg_6760" s="T264">Dĭgəttə sankaʔizi embiem i u kunnambiam. </ta>
            <ta e="T277" id="Seg_6761" s="T270">Bĭzəbiam dĭm da bazo maʔnə deʔpiem, sankaʔizi. </ta>
            <ta e="T284" id="Seg_6762" s="T278">Embiem krăvatʼtə, măndəm: "Nu, tüj küʔ. </ta>
            <ta e="T292" id="Seg_6763" s="T285">Kadəldə da (udazaŋzi) băzəlim, i jakše moləj. </ta>
            <ta e="T303" id="Seg_6764" s="T293">Nu, dĭgəttə moltʼagən bĭzəbiam, ugandə (bal-) balgaš iʔgö ibi ulundə. </ta>
            <ta e="T309" id="Seg_6765" s="T304">Măn mălliam: "Išo eneʔ bü". </ta>
            <ta e="T313" id="Seg_6766" s="T310">Döm (kămne-) kămnit. </ta>
            <ta e="T315" id="Seg_6767" s="T314">((BRK)). </ta>
            <ta e="T322" id="Seg_6768" s="T316">Onʼiʔ (ini-) ine mĭbiem tĭbin kagattə. </ta>
            <ta e="T326" id="Seg_6769" s="T323">Šide pʼe ibi. </ta>
            <ta e="T331" id="Seg_6770" s="T327">Bazo ine mĭbiem, kuzanə. </ta>
            <ta e="T334" id="Seg_6771" s="T332">Tĭbi ibi. </ta>
            <ta e="T337" id="Seg_6772" s="T335">Naga ine. </ta>
            <ta e="T341" id="Seg_6773" s="T337">Tože šide pʼe ibi. </ta>
            <ta e="T344" id="Seg_6774" s="T342">Mĭbiem ine. </ta>
            <ta e="T349" id="Seg_6775" s="T344">Dĭgəttə dĭ sestrandə ular mĭbiem. </ta>
            <ta e="T352" id="Seg_6776" s="T350">Părga mĭbiem. </ta>
            <ta e="T358" id="Seg_6777" s="T353">Dĭgəttə plemʼannican koʔbdonə mĭbiem buga. </ta>
            <ta e="T362" id="Seg_6778" s="T359">(Pʼel=) Pʼel kö. </ta>
            <ta e="T372" id="Seg_6779" s="T363">Dĭgəttə (m-) sestranə nʼit mĭbiem tüžöj, i ular mĭbiem. </ta>
            <ta e="T374" id="Seg_6780" s="T373">((BRK)). </ta>
            <ta e="T382" id="Seg_6781" s="T375">Dĭgəttə onʼiʔ nenə bazo (bu-) büzo mĭbiem. </ta>
            <ta e="T391" id="Seg_6782" s="T383">Jil šoləʔi: "Deʔ aktʼa, (m-) miʔ tănan mĭlibeʔ". </ta>
            <ta e="T397" id="Seg_6783" s="T392">A (boste-) bostəzaŋdə ej mĭleʔi. </ta>
            <ta e="T401" id="Seg_6784" s="T397">I dĭgəttə (maluʔle) aktʼa. </ta>
            <ta e="T403" id="Seg_6785" s="T402">((BRK)). </ta>
            <ta e="T405" id="Seg_6786" s="T404">((BRK)). </ta>
            <ta e="T417" id="Seg_6787" s="T406">Măn tüj en ej (tone-) tenolam, a to inen ulut nada urgo. </ta>
            <ta e="T419" id="Seg_6788" s="T418">((BRK)). </ta>
            <ta e="T422" id="Seg_6789" s="T420">Šiʔ tenolaʔbə. </ta>
            <ta e="T424" id="Seg_6790" s="T423">((BRK)). </ta>
            <ta e="T426" id="Seg_6791" s="T425">((BRK)). </ta>
            <ta e="T434" id="Seg_6792" s="T427">Tüjö kalam tüžöjʔi (šendedndə) sürerlam i noʔmeləm. </ta>
            <ta e="T440" id="Seg_6793" s="T435">Kambiam, a dĭzeŋ nuʔməluʔpiʔi gĭbərdə. </ta>
            <ta e="T442" id="Seg_6794" s="T441">((BRK)). </ta>
            <ta e="T447" id="Seg_6795" s="T443">Traktăr bar paʔi kunnandəga. </ta>
            <ta e="T452" id="Seg_6796" s="T448">Ej tĭmnem, kumen (kunnandəga). </ta>
            <ta e="T454" id="Seg_6797" s="T453">((BRK)). </ta>
            <ta e="T458" id="Seg_6798" s="T455">Ugandə iʔgö paʔi. </ta>
            <ta e="T464" id="Seg_6799" s="T459">Da i (nʼišpek=) nʼišpek ugandə. </ta>
            <ta e="T466" id="Seg_6800" s="T465">((BRK)). </ta>
            <ta e="T474" id="Seg_6801" s="T467">Iʔgö (tn-) togonorzittə kubiam, a amga kulaʔpiam. </ta>
            <ta e="T476" id="Seg_6802" s="T475">((BRK)). </ta>
            <ta e="T480" id="Seg_6803" s="T477">Koŋ ugandə kuvas. </ta>
            <ta e="T487" id="Seg_6804" s="T481">Bar ĭmbi măna mĭmbi, bar ĭmbi. </ta>
            <ta e="T489" id="Seg_6805" s="T488">((BRK)). </ta>
            <ta e="T494" id="Seg_6806" s="T490">Bar măna (pʼem-) pʼerbi. </ta>
            <ta e="T496" id="Seg_6807" s="T495">((BRK)). </ta>
            <ta e="T499" id="Seg_6808" s="T496">Это мало чего-то. </ta>
            <ta e="T504" id="Seg_6809" s="T500">Moləj dʼala, moləj amorzittə. </ta>
            <ta e="T506" id="Seg_6810" s="T505">((BRK)). </ta>
            <ta e="T514" id="Seg_6811" s="T507">Teinen subbota, năda moltʼa nendəsʼtə, bü tazirzittə. </ta>
            <ta e="T520" id="Seg_6812" s="T515">Dĭgəttə nendəlem (mul-), băzəjdəsʼtə nada. </ta>
            <ta e="T526" id="Seg_6813" s="T521">A to selej nedʼelʼa ej (băzəldəbiam). </ta>
            <ta e="T528" id="Seg_6814" s="T527">((BRK)). </ta>
            <ta e="T533" id="Seg_6815" s="T529">(Maʔənnə) sazən edeʔliem, edəʔliem. </ta>
            <ta e="T537" id="Seg_6816" s="T534">Üge (nada) naga. </ta>
            <ta e="T540" id="Seg_6817" s="T537">Nem ej pʼaŋdlia. </ta>
            <ta e="T545" id="Seg_6818" s="T541">I sazən ej šolia. </ta>
            <ta e="T547" id="Seg_6819" s="T546">((BRK)). </ta>
            <ta e="T551" id="Seg_6820" s="T547">Teinen nüdʼin dʼodunʼi kubiem. </ta>
            <ta e="T556" id="Seg_6821" s="T552">Dʼabrolaʔpiem, teinen sazən šoləj. </ta>
            <ta e="T558" id="Seg_6822" s="T557">((BRK)). </ta>
            <ta e="T564" id="Seg_6823" s="T559">Măn mĭmbiem Krasnojaskăjdə, munəʔi (-ʔi-) kumbiam. </ta>
            <ta e="T568" id="Seg_6824" s="T565">(S-) Sto munəj. </ta>
            <ta e="T573" id="Seg_6825" s="T569">Dĭgəttə kagam mănzi kambi. </ta>
            <ta e="T580" id="Seg_6826" s="T574">Dĭ măna kumbi, (amnobi=) amnobi (mašinaʔizi). </ta>
            <ta e="T589" id="Seg_6827" s="T581">Măn (ej-) ej moliam kuzittə, dĭgəttə kubiam plemʼannicam. </ta>
            <ta e="T596" id="Seg_6828" s="T590">A dĭbər kandəbiam, (dö-) döʔən mašinazi. </ta>
            <ta e="T600" id="Seg_6829" s="T597">A dĭn avtobussi. </ta>
            <ta e="T610" id="Seg_6830" s="T601">Dĭgəttə bazaj (aktʼi = aktʼa=) aktʼi, dĭgəttə dĭbər šobiam. </ta>
            <ta e="T612" id="Seg_6831" s="T611">((BRK)). </ta>
            <ta e="T619" id="Seg_6832" s="T613">Dĭgəttə mĭmbiem kudajdə (uman-) numan üzittə. </ta>
            <ta e="T624" id="Seg_6833" s="T620">Dĭgəttə mĭmbibeʔ plemʼannicazi (žibiaʔinə). </ta>
            <ta e="T630" id="Seg_6834" s="T625">Dĭn (s-) amnobibaʔ da dʼăbaktərbibaʔ. </ta>
            <ta e="T635" id="Seg_6835" s="T631">A dĭgəttə maʔndə šobiam. </ta>
            <ta e="T637" id="Seg_6836" s="T636">((BRK)). </ta>
            <ta e="T643" id="Seg_6837" s="T638">Plemʼannica măna pomidor iʔgö mĭbi. </ta>
            <ta e="T649" id="Seg_6838" s="T643">Măn (Kat-) Kazan turagən šide dʼala amnobiam. </ta>
            <ta e="T651" id="Seg_6839" s="T650">((BRK)). </ta>
            <ta e="T662" id="Seg_6840" s="T652">Tüžöjʔi šobiʔi maʔndə, nada dĭzem šedendə sürerzittə i (nom-) noʔməzittə. </ta>
            <ta e="T664" id="Seg_6841" s="T663">((BRK)). </ta>
            <ta e="T669" id="Seg_6842" s="T665">Tenöbiam, tenöbiam, ĭmbi-nʼibudʼ tenöluʔpiem. </ta>
            <ta e="T671" id="Seg_6843" s="T670">((DMG)). </ta>
            <ta e="T678" id="Seg_6844" s="T672">(Koʔbdonə mĭmbiem), sumka dĭ bar sajnʼiluʔpi. </ta>
            <ta e="T684" id="Seg_6845" s="T679">Tüj amnolaʔbəm da (s-) šüdörleʔbəm. </ta>
            <ta e="T691" id="Seg_6846" s="T685">Tănan šonəbiam, (šonəbiam), appi ej saʔməluʔpiam. </ta>
            <ta e="T693" id="Seg_6847" s="T692">((BRK)). </ta>
            <ta e="T695" id="Seg_6848" s="T694">Погоди. </ta>
            <ta e="T696" id="Seg_6849" s="T695">[KA:] Kozaŋ. </ta>
            <ta e="T700" id="Seg_6850" s="T697">[PKZ:] Kazaŋ šonəga, šonəga. </ta>
            <ta e="T708" id="Seg_6851" s="T700">Dĭgəttə noʔ dĭʔnə (püj-) püje (bĭʔpi- bə-) bătluʔbi. </ta>
            <ta e="T711" id="Seg_6852" s="T708">Kemdə bar mʼaŋnaʔbə. </ta>
            <ta e="T715" id="Seg_6853" s="T712">Dĭ măndə: "Šü! </ta>
            <ta e="T719" id="Seg_6854" s="T716">Nendədə dĭ noʔ. </ta>
            <ta e="T721" id="Seg_6855" s="T720">((BRK)). </ta>
            <ta e="T726" id="Seg_6856" s="T722">"Măn iʔgö dʼüm nendəsʼtə". </ta>
            <ta e="T730" id="Seg_6857" s="T726">Dĭgəttə dĭ bünə kambi. </ta>
            <ta e="T736" id="Seg_6858" s="T731">Bü, kanaʔ, šüm (kăm-) kămnaʔ. </ta>
            <ta e="T738" id="Seg_6859" s="T737">((BRK)). </ta>
            <ta e="T747" id="Seg_6860" s="T739">"Măn iʔgö (dʼ-) dʼü kămnastə, štobɨ noʔ özerbi. </ta>
            <ta e="T749" id="Seg_6861" s="T748">((BRK)). </ta>
            <ta e="T753" id="Seg_6862" s="T750">Dĭgəttə dĭ kambi. </ta>
            <ta e="T761" id="Seg_6863" s="T754">"Bulan, bulan, kanaʔ, bar bü (bĭ-) bĭdeʔ!" </ta>
            <ta e="T769" id="Seg_6864" s="T762">Dĭgəttə bulan mămbi: "Măn üjündə iʔgö bü!" </ta>
            <ta e="T775" id="Seg_6865" s="T770">Dĭgəttə dĭ (m-) kambi nükenə. </ta>
            <ta e="T781" id="Seg_6866" s="T775">"Nüke, nüke, kanaʔ žilaʔi üjüttə it!" </ta>
            <ta e="T785" id="Seg_6867" s="T782">Nüke ej kambi. </ta>
            <ta e="T789" id="Seg_6868" s="T786">"Sagərʔi, kabarləj măna". </ta>
            <ta e="T791" id="Seg_6869" s="T790">((BRK)). </ta>
            <ta e="T800" id="Seg_6870" s="T792">Dĭ kambi nükenə: "Nüke, nüke, it (žil-) žilaʔi". </ta>
            <ta e="T811" id="Seg_6871" s="T800">A dĭ măndə: "Măn iʔgö ige, (da) sagər, măna ej kereʔ". </ta>
            <ta e="T813" id="Seg_6872" s="T812">((BRK)). </ta>
            <ta e="T817" id="Seg_6873" s="T814">Dĭgəttə kambi tumoʔinə. </ta>
            <ta e="T823" id="Seg_6874" s="T817">"Tumo, tumo, kanaʔ, nüken žilaʔi amnaʔ!" </ta>
            <ta e="T831" id="Seg_6875" s="T824">"dʼok, (mi-) miʔ (em kallaʔ=) ej kallam. </ta>
            <ta e="T837" id="Seg_6876" s="T832">Miʔnʼibeʔ kabarləj dʼügən kornʼiʔi amzittə". </ta>
            <ta e="T839" id="Seg_6877" s="T838">((BRK)). </ta>
            <ta e="T844" id="Seg_6878" s="T840">Dĭgəttə kambi ((DMG)) esseŋdə. </ta>
            <ta e="T848" id="Seg_6879" s="T844">"Esseŋ, esseŋ, kutlaʔləj tumoʔi!" </ta>
            <ta e="T854" id="Seg_6880" s="T848">Dĭzeŋ: "Miʔ (bo-) bospə sʼarlaʔbəʔjə baːbkaʔiziʔ". </ta>
            <ta e="T856" id="Seg_6881" s="T855">((BRK)). </ta>
            <ta e="T861" id="Seg_6882" s="T857">Šiʔ turagən jakše amnozittə. </ta>
            <ta e="T867" id="Seg_6883" s="T862">Tolʼko ipek gijendə ej ilil. </ta>
            <ta e="T871" id="Seg_6884" s="T868">Amzittə naga ipek. </ta>
            <ta e="T873" id="Seg_6885" s="T872">((BRK)). </ta>
            <ta e="T880" id="Seg_6886" s="T874">Magazingəndə ĭmbi naga, ĭmbidə ej iləl. </ta>
            <ta e="T890" id="Seg_6887" s="T881">"Dʼok, kanaʔ, dĭn ige kălbasa, iʔ da dĭgəttə amorlal". </ta>
            <ta e="T892" id="Seg_6888" s="T891">((BRK)). </ta>
            <ta e="T900" id="Seg_6889" s="T893">Dĭ nüke ugandə jakše šödörlaʔbə, bar kuvas. </ta>
            <ta e="T903" id="Seg_6890" s="T901">Jakše măndosʼtə. </ta>
            <ta e="T905" id="Seg_6891" s="T904">((BRK)). </ta>
            <ta e="T916" id="Seg_6892" s="T906">Dĭ nüke ugandə iʔgö (tono-) togonoria, bar ĭmbi azittə умеет. </ta>
            <ta e="T918" id="Seg_6893" s="T917">((BRK)). </ta>
            <ta e="T923" id="Seg_6894" s="T919">Bar ĭmbi togonorzittə tĭmnet. </ta>
            <ta e="T925" id="Seg_6895" s="T924">((BRK)). </ta>
            <ta e="T935" id="Seg_6896" s="T926">Tĭn nüken (kuz-) tukazaŋdə amga amnambi, tolʼko onʼiʔ kagat. </ta>
            <ta e="T937" id="Seg_6897" s="T936">((BRK)). </ta>
            <ta e="T941" id="Seg_6898" s="T938">A sestrat külambi. </ta>
            <ta e="T943" id="Seg_6899" s="T942">((BRK)). </ta>
            <ta e="T949" id="Seg_6900" s="T944">Šödörluʔpiem, bar tüj naga šöʔsittə. </ta>
            <ta e="T951" id="Seg_6901" s="T950">((BRK)). </ta>
            <ta e="T961" id="Seg_6902" s="T952">Miʔ jelezʼe kambibaʔ bü tažerzittə, a dĭn bü ige. </ta>
            <ta e="T970" id="Seg_6903" s="T962">(Nagur vin-) Nagur vedro deʔpibeʔ, i kabarləj tüj. </ta>
            <ta e="T972" id="Seg_6904" s="T971">((BRK)). </ta>
            <ta e="T977" id="Seg_6905" s="T973">Dʼijegən šonəgam, dʼije nüjleʔbəm. </ta>
            <ta e="T982" id="Seg_6906" s="T978">Pagən šonəgam, pa nüjleʔbəm. </ta>
            <ta e="T987" id="Seg_6907" s="T983">Sʼtʼep šonəgam, sʼtʼep nüjleʔbəm. </ta>
            <ta e="T992" id="Seg_6908" s="T988">Măja šonəgam, măja nüjleʔbəm. </ta>
            <ta e="T998" id="Seg_6909" s="T993">Dʼălam šonəga, Dʼălam (nujle-) nüjleʔbəm. </ta>
            <ta e="T1003" id="Seg_6910" s="T999">Turagən amnolaʔbə, tura nüjleʔbəm. </ta>
            <ta e="T1005" id="Seg_6911" s="T1004">((BRK)). </ta>
            <ta e="T1012" id="Seg_6912" s="T1006">(Miʔ=) Măn turanʼi ugandə tumo iʔgö. </ta>
            <ta e="T1016" id="Seg_6913" s="T1012">Ĭmbi păpală bar amniaʔi. </ta>
            <ta e="T1020" id="Seg_6914" s="T1017">A tospak naga. </ta>
            <ta e="T1032" id="Seg_6915" s="T1021">Kanaʔ miʔnenə, (dĭ=) dĭn šide (do-) tospak, onʼiʔ sĭre, onʼiʔ komə. </ta>
            <ta e="T1036" id="Seg_6916" s="T1033">Dĭ mĭləj tănan. </ta>
            <ta e="T1048" id="Seg_6917" s="T1037">Da dĭ sĭre măna ibi da kalla dʼürbi, dĭ dĭm ej öʔlübi. </ta>
            <ta e="T1052" id="Seg_6918" s="T1049">(Daška) ej šolia. </ta>
            <ta e="T1054" id="Seg_6919" s="T1053">((BRK)). </ta>
            <ta e="T1058" id="Seg_6920" s="T1055">((DMG)) Măn kunolzittə iʔpiem. </ta>
            <ta e="T1068" id="Seg_6921" s="T1059">A dĭn ugandə putʼuga, kuliam, tüʔ (ip-) iʔgö iʔbolaʔbə. </ta>
            <ta e="T1070" id="Seg_6922" s="T1069">((BRK)). </ta>
            <ta e="T1075" id="Seg_6923" s="T1071">Ertən uʔbdəbiam, kălenkanə nubiam. </ta>
            <ta e="T1082" id="Seg_6924" s="T1076">Kuliom bar: dĭn bʼeʔ kuči iʔbolaʔbə. </ta>
            <ta e="T1084" id="Seg_6925" s="T1083">((BRK)). </ta>
            <ta e="T1090" id="Seg_6926" s="T1085">Dĭgəttə ujam kunəlambam niʔtə, takšegən. </ta>
            <ta e="T1096" id="Seg_6927" s="T1090">(Takšezi) kajliam, štobɨ tumoʔi ej ambiʔi. </ta>
            <ta e="T1107" id="Seg_6928" s="T1097">A kajaʔ măslăbojkănə tože (ka-) kajlam, štobɨ tumoʔi ej ambiʔi. </ta>
            <ta e="T1109" id="Seg_6929" s="T1108">((BRK)). </ta>
            <ta e="T1119" id="Seg_6930" s="T1110">A xoš dăk miʔnʼibeʔ šide ige (tospak), detlem onʼiʔ. </ta>
            <ta e="T1126" id="Seg_6931" s="T1120">Girgit (tănan) deʔsittə, sĭre ali komə? </ta>
            <ta e="T1130" id="Seg_6932" s="T1127">"Nu deʔ komə". </ta>
            <ta e="T1132" id="Seg_6933" s="T1131">((BRK)). </ta>
            <ta e="T1139" id="Seg_6934" s="T1133">Moltʼanə šoga, dĭn ugandə bü iʔgö. </ta>
            <ta e="T1146" id="Seg_6935" s="T1140">Dʼibige bü ige, šišege, sabən ige. </ta>
            <ta e="T1150" id="Seg_6936" s="T1147">Ejü, (ejümnel) bar. </ta>
            <ta e="T1152" id="Seg_6937" s="T1151">((BRK)). </ta>
            <ta e="T1159" id="Seg_6938" s="T1153">Püjel ej ĭzemne i kokluʔi mʼaŋnaʔi. </ta>
            <ta e="T1161" id="Seg_6939" s="T1160">((BRK)). </ta>
            <ta e="T1168" id="Seg_6940" s="T1162">Dö zaplatka, a gibər dĭm zaplatka. </ta>
            <ta e="T1172" id="Seg_6941" s="T1169">Kötengəndə amnolzittə, što li. </ta>
            <ta e="T1177" id="Seg_6942" s="T1173">Barəbtɨʔ, ej kereʔ măna. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_6943" s="T1">măn</ta>
            <ta e="T3" id="Seg_6944" s="T2">taldʼen</ta>
            <ta e="T4" id="Seg_6945" s="T3">kam-bia-m</ta>
            <ta e="T5" id="Seg_6946" s="T4">tugan-bə</ta>
            <ta e="T7" id="Seg_6947" s="T6">măn</ta>
            <ta e="T8" id="Seg_6948" s="T7">măm-bia-m</ta>
            <ta e="T9" id="Seg_6949" s="T8">i-ʔ</ta>
            <ta e="T10" id="Seg_6950" s="T9">kuro-ʔ</ta>
            <ta e="T11" id="Seg_6951" s="T10">măna</ta>
            <ta e="T14" id="Seg_6952" s="T13">šo-ʔsittə</ta>
            <ta e="T16" id="Seg_6953" s="T15">mei-m</ta>
            <ta e="T17" id="Seg_6954" s="T16">ej</ta>
            <ta e="T18" id="Seg_6955" s="T17">kam-bi</ta>
            <ta e="T19" id="Seg_6956" s="T18">i</ta>
            <ta e="T20" id="Seg_6957" s="T19">măn</ta>
            <ta e="T21" id="Seg_6958" s="T20">ej</ta>
            <ta e="T22" id="Seg_6959" s="T21">kam-bia-m</ta>
            <ta e="T24" id="Seg_6960" s="T23">dĭ-zeŋ</ta>
            <ta e="T25" id="Seg_6961" s="T24">krăvatʼ-kən</ta>
            <ta e="T26" id="Seg_6962" s="T25">iʔbo-laʔbə-ʔjə</ta>
            <ta e="T27" id="Seg_6963" s="T26">kamro-laʔbə-ʔjə</ta>
            <ta e="T29" id="Seg_6964" s="T28">Kolʼa</ta>
            <ta e="T30" id="Seg_6965" s="T29">Dunʼa-t-si</ta>
            <ta e="T32" id="Seg_6966" s="T31">pănar-laʔbə-ʔjə</ta>
            <ta e="T34" id="Seg_6967" s="T33">kaknar-laʔbə-ʔjə</ta>
            <ta e="T36" id="Seg_6968" s="T35">măn-də-ʔi</ta>
            <ta e="T37" id="Seg_6969" s="T36">urgaja</ta>
            <ta e="T38" id="Seg_6970" s="T37">am-a-ʔ</ta>
            <ta e="T39" id="Seg_6971" s="T38">amna-ʔ</ta>
            <ta e="T41" id="Seg_6972" s="T40">ara</ta>
            <ta e="T42" id="Seg_6973" s="T41">bĭd-e-ʔ</ta>
            <ta e="T44" id="Seg_6974" s="T43">măn</ta>
            <ta e="T45" id="Seg_6975" s="T44">măn-də-m</ta>
            <ta e="T46" id="Seg_6976" s="T45">ej</ta>
            <ta e="T47" id="Seg_6977" s="T46">bĭt-lə-m</ta>
            <ta e="T49" id="Seg_6978" s="T48">i</ta>
            <ta e="T50" id="Seg_6979" s="T49">dĭgəttə</ta>
            <ta e="T1178" id="Seg_6980" s="T50">kal-la</ta>
            <ta e="T51" id="Seg_6981" s="T1178">dʼür-bie-m</ta>
            <ta e="T52" id="Seg_6982" s="T51">maʔ-nʼi</ta>
            <ta e="T56" id="Seg_6983" s="T55">măn</ta>
            <ta e="T57" id="Seg_6984" s="T56">teinen</ta>
            <ta e="T58" id="Seg_6985" s="T57">nüke-nə</ta>
            <ta e="T59" id="Seg_6986" s="T58">kam-bia-m</ta>
            <ta e="T60" id="Seg_6987" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_6988" s="T60">ĭzem-nie</ta>
            <ta e="T63" id="Seg_6989" s="T62">dĭ</ta>
            <ta e="T64" id="Seg_6990" s="T63">măn-də</ta>
            <ta e="T65" id="Seg_6991" s="T64">ĭmbi</ta>
            <ta e="T66" id="Seg_6992" s="T65">kondʼo</ta>
            <ta e="T67" id="Seg_6993" s="T66">ej</ta>
            <ta e="T68" id="Seg_6994" s="T67">šo-bia-m</ta>
            <ta e="T69" id="Seg_6995" s="T68">măn</ta>
            <ta e="T70" id="Seg_6996" s="T69">bar</ta>
            <ta e="T71" id="Seg_6997" s="T70">ku-landə-ga-m</ta>
            <ta e="T72" id="Seg_6998" s="T71">tăn</ta>
            <ta e="T73" id="Seg_6999" s="T72">üge</ta>
            <ta e="T74" id="Seg_7000" s="T73">ku-landə-ga-l</ta>
            <ta e="T75" id="Seg_7001" s="T74">a</ta>
            <ta e="T76" id="Seg_7002" s="T75">kü-sʼtə</ta>
            <ta e="T77" id="Seg_7003" s="T76">ej</ta>
            <ta e="T78" id="Seg_7004" s="T77">mo-lia-l</ta>
            <ta e="T80" id="Seg_7005" s="T79">no</ta>
            <ta e="T81" id="Seg_7006" s="T80">kü-ʔ</ta>
            <ta e="T83" id="Seg_7007" s="T82">măna</ta>
            <ta e="T84" id="Seg_7008" s="T83">ĭmbi</ta>
            <ta e="T86" id="Seg_7009" s="T85">ĭmbi</ta>
            <ta e="T87" id="Seg_7010" s="T86">măn</ta>
            <ta e="T88" id="Seg_7011" s="T87">tăn</ta>
            <ta e="T89" id="Seg_7012" s="T88">to-ndə</ta>
            <ta e="T90" id="Seg_7013" s="T89">amno-laʔ-lə-m</ta>
            <ta e="T91" id="Seg_7014" s="T90">štoli</ta>
            <ta e="T93" id="Seg_7015" s="T92">măn</ta>
            <ta e="T1185" id="Seg_7016" s="T93">vedʼ</ta>
            <ta e="T94" id="Seg_7017" s="T1185">jilə</ta>
            <ta e="T95" id="Seg_7018" s="T94">amno-ʔlaʔbə-ʔjə</ta>
            <ta e="T97" id="Seg_7019" s="T96">dĭn</ta>
            <ta e="T98" id="Seg_7020" s="T97">edəʔ-leʔbə-ʔjə</ta>
            <ta e="T99" id="Seg_7021" s="T98">măna</ta>
            <ta e="T101" id="Seg_7022" s="T100">dʼăbaktər-zittə</ta>
            <ta e="T102" id="Seg_7023" s="T101">nada</ta>
            <ta e="T104" id="Seg_7024" s="T103">a</ta>
            <ta e="T105" id="Seg_7025" s="T104">tăn</ta>
            <ta e="T106" id="Seg_7026" s="T105">to-ndə</ta>
            <ta e="T107" id="Seg_7027" s="T106">amno-ʔ</ta>
            <ta e="T111" id="Seg_7028" s="T110">mei-m</ta>
            <ta e="T113" id="Seg_7029" s="T112">šide</ta>
            <ta e="T114" id="Seg_7030" s="T113">dʼala</ta>
            <ta e="T115" id="Seg_7031" s="T114">i-bi</ta>
            <ta e="T117" id="Seg_7032" s="T116">a</ta>
            <ta e="T118" id="Seg_7033" s="T117">tăn</ta>
            <ta e="T119" id="Seg_7034" s="T118">to-ndə</ta>
            <ta e="T120" id="Seg_7035" s="T119">amno-ʔ</ta>
            <ta e="T122" id="Seg_7036" s="T121">da</ta>
            <ta e="T123" id="Seg_7037" s="T122">jil</ta>
            <ta e="T124" id="Seg_7038" s="T123">mĭŋ-gə-ʔi</ta>
            <ta e="T126" id="Seg_7039" s="T125">a</ta>
            <ta e="T127" id="Seg_7040" s="T126">dĭ</ta>
            <ta e="T128" id="Seg_7041" s="T127">măn-də</ta>
            <ta e="T129" id="Seg_7042" s="T128">a</ta>
            <ta e="T130" id="Seg_7043" s="T129">kudaj</ta>
            <ta e="T131" id="Seg_7044" s="T130">dĭ-zəŋ-zi</ta>
            <ta e="T133" id="Seg_7045" s="T132">a</ta>
            <ta e="T134" id="Seg_7046" s="T133">tăn-zi</ta>
            <ta e="T135" id="Seg_7047" s="T134">pušaj</ta>
            <ta e="T136" id="Seg_7048" s="T135">kudaj</ta>
            <ta e="T138" id="Seg_7049" s="T137">kü-ʔ</ta>
            <ta e="T139" id="Seg_7050" s="T138">kü-ʔ</ta>
            <ta e="T143" id="Seg_7051" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_7052" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_7053" s="T144">ma-lubi</ta>
            <ta e="T146" id="Seg_7054" s="T145">a</ta>
            <ta e="T147" id="Seg_7055" s="T146">măn</ta>
            <ta e="T148" id="Seg_7056" s="T147">maʔ-nʼi</ta>
            <ta e="T1179" id="Seg_7057" s="T148">kal-la</ta>
            <ta e="T149" id="Seg_7058" s="T1179">dʼür-bie-m</ta>
            <ta e="T153" id="Seg_7059" s="T152">măn</ta>
            <ta e="T1183" id="Seg_7060" s="T153">Kazan</ta>
            <ta e="T154" id="Seg_7061" s="T1183">tura-nə</ta>
            <ta e="T155" id="Seg_7062" s="T154">kam-bia-m</ta>
            <ta e="T157" id="Seg_7063" s="T156">a</ta>
            <ta e="T158" id="Seg_7064" s="T157">dĭ</ta>
            <ta e="T159" id="Seg_7065" s="T158">nükə</ta>
            <ta e="T160" id="Seg_7066" s="T159">kam-bi</ta>
            <ta e="T161" id="Seg_7067" s="T160">dĭ-n</ta>
            <ta e="T162" id="Seg_7068" s="T161">to-ndə</ta>
            <ta e="T163" id="Seg_7069" s="T162">dĭn</ta>
            <ta e="T164" id="Seg_7070" s="T163">ne</ta>
            <ta e="T165" id="Seg_7071" s="T164">amno-laʔbə</ta>
            <ta e="T167" id="Seg_7072" s="T166">šo-bi</ta>
            <ta e="T169" id="Seg_7073" s="T168">da</ta>
            <ta e="T1180" id="Seg_7074" s="T169">kal-la</ta>
            <ta e="T170" id="Seg_7075" s="T1180">dʼür-bi</ta>
            <ta e="T172" id="Seg_7076" s="T171">măn</ta>
            <ta e="T173" id="Seg_7077" s="T172">kü-la-m</ta>
            <ta e="T174" id="Seg_7078" s="T173">a</ta>
            <ta e="T175" id="Seg_7079" s="T174">šində</ta>
            <ta e="T176" id="Seg_7080" s="T175">măna</ta>
            <ta e="T178" id="Seg_7081" s="T177">bĭzəj-d-lə-j</ta>
            <ta e="T180" id="Seg_7082" s="T179">dĭ-zeŋ</ta>
            <ta e="T181" id="Seg_7083" s="T180">bar</ta>
            <ta e="T182" id="Seg_7084" s="T181">kaknar-bi-ʔi</ta>
            <ta e="T183" id="Seg_7085" s="T182">tăŋ</ta>
            <ta e="T185" id="Seg_7086" s="T184">dĭgəttə</ta>
            <ta e="T186" id="Seg_7087" s="T185">măn</ta>
            <ta e="T187" id="Seg_7088" s="T186">so-bia-m</ta>
            <ta e="T188" id="Seg_7089" s="T187">kam-bia-m</ta>
            <ta e="T189" id="Seg_7090" s="T188">dĭʔ-nə</ta>
            <ta e="T190" id="Seg_7091" s="T189">tăn</ta>
            <ta e="T1182" id="Seg_7092" s="T190">kal-la</ta>
            <ta e="T191" id="Seg_7093" s="T1182">dʼür-bie-m</ta>
            <ta e="T192" id="Seg_7094" s="T191">a</ta>
            <ta e="T193" id="Seg_7095" s="T192">măn</ta>
            <ta e="T194" id="Seg_7096" s="T193">kü-l-lam-bia-m</ta>
            <ta e="T195" id="Seg_7097" s="T194">i</ta>
            <ta e="T196" id="Seg_7098" s="T195">kanna-m-bia-m</ta>
            <ta e="T198" id="Seg_7099" s="T197">a</ta>
            <ta e="T199" id="Seg_7100" s="T198">măn</ta>
            <ta e="T200" id="Seg_7101" s="T199">măm-bia-m</ta>
            <ta e="T201" id="Seg_7102" s="T200">nu</ta>
            <ta e="T202" id="Seg_7103" s="T201">kănna-ʔ</ta>
            <ta e="T204" id="Seg_7104" s="T203">ĭmbi</ta>
            <ta e="T205" id="Seg_7105" s="T204">i</ta>
            <ta e="T207" id="Seg_7106" s="T206">i</ta>
            <ta e="T208" id="Seg_7107" s="T207">kü-ʔ</ta>
            <ta e="T209" id="Seg_7108" s="T208">mɨ</ta>
            <ta e="T210" id="Seg_7109" s="T209">bɨ</ta>
            <ta e="T211" id="Seg_7110" s="T210">măn</ta>
            <ta e="T212" id="Seg_7111" s="T211">bü</ta>
            <ta e="T213" id="Seg_7112" s="T212">ejüm-lə-m</ta>
            <ta e="T215" id="Seg_7113" s="T214">i</ta>
            <ta e="T216" id="Seg_7114" s="T215">tănan</ta>
            <ta e="T218" id="Seg_7115" s="T217">kăm-la-l</ta>
            <ta e="T220" id="Seg_7116" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_7117" s="T220">bĭzə-li-m</ta>
            <ta e="T222" id="Seg_7118" s="T221">i</ta>
            <ta e="T223" id="Seg_7119" s="T222">dʼu-nə</ta>
            <ta e="T226" id="Seg_7120" s="T225">dʼü-nə</ta>
            <ta e="T227" id="Seg_7121" s="T226">en-lə-bi-m</ta>
            <ta e="T241" id="Seg_7122" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_7123" s="T241">nüke</ta>
            <ta e="T243" id="Seg_7124" s="T242">bar</ta>
            <ta e="T244" id="Seg_7125" s="T243">măna</ta>
            <ta e="T245" id="Seg_7126" s="T244">nende-ʔ</ta>
            <ta e="T246" id="Seg_7127" s="T245">moltʼa</ta>
            <ta e="T247" id="Seg_7128" s="T246">bĭza-ʔ</ta>
            <ta e="T248" id="Seg_7129" s="T247">măna</ta>
            <ta e="T250" id="Seg_7130" s="T249">kü-lal-lə-m</ta>
            <ta e="T251" id="Seg_7131" s="T250">dăk</ta>
            <ta e="T252" id="Seg_7132" s="T251">dĭgəttə</ta>
            <ta e="T254" id="Seg_7133" s="T252">kadəl-də=də</ta>
            <ta e="T256" id="Seg_7134" s="T254">uda-zaŋ=də</ta>
            <ta e="T257" id="Seg_7135" s="T256">bĭzə-li-l</ta>
            <ta e="T259" id="Seg_7136" s="T258">măn</ta>
            <ta e="T260" id="Seg_7137" s="T259">dĭgəttə</ta>
            <ta e="T261" id="Seg_7138" s="T260">nendə-bie-m</ta>
            <ta e="T263" id="Seg_7139" s="T262">moltʼa</ta>
            <ta e="T265" id="Seg_7140" s="T264">dĭgəttə</ta>
            <ta e="T266" id="Seg_7141" s="T265">sanka-ʔi-zi</ta>
            <ta e="T267" id="Seg_7142" s="T266">em-bie-m</ta>
            <ta e="T268" id="Seg_7143" s="T267">i</ta>
            <ta e="T269" id="Seg_7144" s="T268">u</ta>
            <ta e="T270" id="Seg_7145" s="T269">kun-nam-bia-m</ta>
            <ta e="T271" id="Seg_7146" s="T270">bĭzə-bia-m</ta>
            <ta e="T272" id="Seg_7147" s="T271">dĭ-m</ta>
            <ta e="T273" id="Seg_7148" s="T272">da</ta>
            <ta e="T274" id="Seg_7149" s="T273">bazo</ta>
            <ta e="T275" id="Seg_7150" s="T274">maʔ-nə</ta>
            <ta e="T276" id="Seg_7151" s="T275">deʔ-pie-m</ta>
            <ta e="T277" id="Seg_7152" s="T276">sanka-ʔi-zi</ta>
            <ta e="T279" id="Seg_7153" s="T278">em-bie-m</ta>
            <ta e="T280" id="Seg_7154" s="T279">krăvatʼ-tə</ta>
            <ta e="T281" id="Seg_7155" s="T280">măn-də-m</ta>
            <ta e="T282" id="Seg_7156" s="T281">nu</ta>
            <ta e="T283" id="Seg_7157" s="T282">tüj</ta>
            <ta e="T284" id="Seg_7158" s="T283">kü-ʔ</ta>
            <ta e="T286" id="Seg_7159" s="T285">kadəl-də</ta>
            <ta e="T287" id="Seg_7160" s="T286">da</ta>
            <ta e="T288" id="Seg_7161" s="T287">uda-zaŋ-zi</ta>
            <ta e="T289" id="Seg_7162" s="T288">băzə-li-m</ta>
            <ta e="T290" id="Seg_7163" s="T289">i</ta>
            <ta e="T291" id="Seg_7164" s="T290">jakše</ta>
            <ta e="T292" id="Seg_7165" s="T291">mo-lə-j</ta>
            <ta e="T294" id="Seg_7166" s="T293">nu</ta>
            <ta e="T295" id="Seg_7167" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_7168" s="T295">moltʼa-gən</ta>
            <ta e="T297" id="Seg_7169" s="T296">bĭzə-bia-m</ta>
            <ta e="T298" id="Seg_7170" s="T297">ugandə</ta>
            <ta e="T300" id="Seg_7171" s="T299">balgaš</ta>
            <ta e="T301" id="Seg_7172" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_7173" s="T301">i-bi</ta>
            <ta e="T303" id="Seg_7174" s="T302">ulu-ndə</ta>
            <ta e="T305" id="Seg_7175" s="T304">măn</ta>
            <ta e="T306" id="Seg_7176" s="T305">măl-lia-m</ta>
            <ta e="T307" id="Seg_7177" s="T306">išo</ta>
            <ta e="T308" id="Seg_7178" s="T307">en-e-ʔ</ta>
            <ta e="T309" id="Seg_7179" s="T308">bü</ta>
            <ta e="T311" id="Seg_7180" s="T310">dö-m</ta>
            <ta e="T313" id="Seg_7181" s="T312">kămni-t</ta>
            <ta e="T317" id="Seg_7182" s="T316">onʼiʔ</ta>
            <ta e="T319" id="Seg_7183" s="T318">ine</ta>
            <ta e="T320" id="Seg_7184" s="T319">mĭ-bie-m</ta>
            <ta e="T321" id="Seg_7185" s="T320">tĭbi-n</ta>
            <ta e="T322" id="Seg_7186" s="T321">kaga-ttə</ta>
            <ta e="T324" id="Seg_7187" s="T323">šide</ta>
            <ta e="T325" id="Seg_7188" s="T324">pʼe</ta>
            <ta e="T326" id="Seg_7189" s="T325">i-bi</ta>
            <ta e="T328" id="Seg_7190" s="T327">bazo</ta>
            <ta e="T329" id="Seg_7191" s="T328">ine</ta>
            <ta e="T330" id="Seg_7192" s="T329">mĭ-bie-m</ta>
            <ta e="T331" id="Seg_7193" s="T330">kuza-nə</ta>
            <ta e="T333" id="Seg_7194" s="T332">tĭbi</ta>
            <ta e="T334" id="Seg_7195" s="T333">i-bi</ta>
            <ta e="T336" id="Seg_7196" s="T335">naga</ta>
            <ta e="T337" id="Seg_7197" s="T336">ine</ta>
            <ta e="T338" id="Seg_7198" s="T337">tože</ta>
            <ta e="T339" id="Seg_7199" s="T338">šide</ta>
            <ta e="T340" id="Seg_7200" s="T339">pʼe</ta>
            <ta e="T341" id="Seg_7201" s="T340">i-bi</ta>
            <ta e="T343" id="Seg_7202" s="T342">mĭ-bie-m</ta>
            <ta e="T344" id="Seg_7203" s="T343">ine</ta>
            <ta e="T345" id="Seg_7204" s="T344">dĭgəttə</ta>
            <ta e="T346" id="Seg_7205" s="T345">dĭ</ta>
            <ta e="T347" id="Seg_7206" s="T346">sestra-ndə</ta>
            <ta e="T348" id="Seg_7207" s="T347">ular</ta>
            <ta e="T349" id="Seg_7208" s="T348">mĭ-bie-m</ta>
            <ta e="T351" id="Seg_7209" s="T350">părga</ta>
            <ta e="T352" id="Seg_7210" s="T351">mĭ-bie-m</ta>
            <ta e="T354" id="Seg_7211" s="T353">dĭgəttə</ta>
            <ta e="T355" id="Seg_7212" s="T354">plemʼannica-n</ta>
            <ta e="T356" id="Seg_7213" s="T355">koʔbdo-nə</ta>
            <ta e="T357" id="Seg_7214" s="T356">mĭ-bie-m</ta>
            <ta e="T358" id="Seg_7215" s="T357">buga</ta>
            <ta e="T360" id="Seg_7216" s="T359">pʼel</ta>
            <ta e="T361" id="Seg_7217" s="T360">pʼel</ta>
            <ta e="T362" id="Seg_7218" s="T361">kö</ta>
            <ta e="T364" id="Seg_7219" s="T363">dĭgəttə</ta>
            <ta e="T366" id="Seg_7220" s="T365">sestra-nə</ta>
            <ta e="T367" id="Seg_7221" s="T366">nʼi-t</ta>
            <ta e="T368" id="Seg_7222" s="T367">mĭ-bie-m</ta>
            <ta e="T369" id="Seg_7223" s="T368">tüžöj</ta>
            <ta e="T370" id="Seg_7224" s="T369">i</ta>
            <ta e="T371" id="Seg_7225" s="T370">ular</ta>
            <ta e="T372" id="Seg_7226" s="T371">mĭ-bie-m</ta>
            <ta e="T376" id="Seg_7227" s="T375">dĭgəttə</ta>
            <ta e="T377" id="Seg_7228" s="T376">onʼiʔ</ta>
            <ta e="T378" id="Seg_7229" s="T377">ne-nə</ta>
            <ta e="T379" id="Seg_7230" s="T378">bazo</ta>
            <ta e="T381" id="Seg_7231" s="T380">büzo</ta>
            <ta e="T382" id="Seg_7232" s="T381">mĭ-bie-m</ta>
            <ta e="T384" id="Seg_7233" s="T383">jil</ta>
            <ta e="T385" id="Seg_7234" s="T384">šo-lə-ʔi</ta>
            <ta e="T386" id="Seg_7235" s="T385">de-ʔ</ta>
            <ta e="T387" id="Seg_7236" s="T386">aktʼa</ta>
            <ta e="T389" id="Seg_7237" s="T388">miʔ</ta>
            <ta e="T390" id="Seg_7238" s="T389">tănan</ta>
            <ta e="T391" id="Seg_7239" s="T390">mĭ-li-beʔ</ta>
            <ta e="T393" id="Seg_7240" s="T392">a</ta>
            <ta e="T394" id="Seg_7241" s="T393">boste</ta>
            <ta e="T395" id="Seg_7242" s="T394">bostə-zaŋ-də</ta>
            <ta e="T396" id="Seg_7243" s="T395">ej</ta>
            <ta e="T397" id="Seg_7244" s="T396">mĭ-le-ʔi</ta>
            <ta e="T398" id="Seg_7245" s="T397">i</ta>
            <ta e="T399" id="Seg_7246" s="T398">dĭgəttə</ta>
            <ta e="T401" id="Seg_7247" s="T400">aktʼa</ta>
            <ta e="T407" id="Seg_7248" s="T406">măn</ta>
            <ta e="T408" id="Seg_7249" s="T407">tüj</ta>
            <ta e="T409" id="Seg_7250" s="T408">e-n</ta>
            <ta e="T410" id="Seg_7251" s="T409">ej</ta>
            <ta e="T412" id="Seg_7252" s="T411">teno-la-m</ta>
            <ta e="T413" id="Seg_7253" s="T412">ato</ta>
            <ta e="T414" id="Seg_7254" s="T413">ine-n</ta>
            <ta e="T415" id="Seg_7255" s="T414">ulu-t</ta>
            <ta e="T416" id="Seg_7256" s="T415">nada</ta>
            <ta e="T417" id="Seg_7257" s="T416">urgo</ta>
            <ta e="T421" id="Seg_7258" s="T420">šiʔ</ta>
            <ta e="T422" id="Seg_7259" s="T421">teno-laʔbə</ta>
            <ta e="T428" id="Seg_7260" s="T427">tüjö</ta>
            <ta e="T429" id="Seg_7261" s="T428">ka-la-m</ta>
            <ta e="T430" id="Seg_7262" s="T429">tüžöj-ʔi</ta>
            <ta e="T432" id="Seg_7263" s="T431">sürer-la-m</ta>
            <ta e="T433" id="Seg_7264" s="T432">i</ta>
            <ta e="T434" id="Seg_7265" s="T433">noʔme-lə-m</ta>
            <ta e="T436" id="Seg_7266" s="T435">kam-bia-m</ta>
            <ta e="T437" id="Seg_7267" s="T436">a</ta>
            <ta e="T438" id="Seg_7268" s="T437">dĭ-zeŋ</ta>
            <ta e="T439" id="Seg_7269" s="T438">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T440" id="Seg_7270" s="T439">gĭbər=də</ta>
            <ta e="T444" id="Seg_7271" s="T443">traktăr</ta>
            <ta e="T445" id="Seg_7272" s="T444">bar</ta>
            <ta e="T446" id="Seg_7273" s="T445">pa-ʔi</ta>
            <ta e="T447" id="Seg_7274" s="T446">kun-nandə-ga</ta>
            <ta e="T449" id="Seg_7275" s="T448">ej</ta>
            <ta e="T450" id="Seg_7276" s="T449">tĭmne-m</ta>
            <ta e="T451" id="Seg_7277" s="T450">kumen</ta>
            <ta e="T452" id="Seg_7278" s="T451">kun-nandə-ga</ta>
            <ta e="T456" id="Seg_7279" s="T455">ugandə</ta>
            <ta e="T457" id="Seg_7280" s="T456">iʔgö</ta>
            <ta e="T458" id="Seg_7281" s="T457">pa-ʔi</ta>
            <ta e="T460" id="Seg_7282" s="T459">da</ta>
            <ta e="T461" id="Seg_7283" s="T460">i</ta>
            <ta e="T462" id="Seg_7284" s="T461">nʼišpek</ta>
            <ta e="T463" id="Seg_7285" s="T462">nʼišpek</ta>
            <ta e="T464" id="Seg_7286" s="T463">ugandə</ta>
            <ta e="T468" id="Seg_7287" s="T467">iʔgö</ta>
            <ta e="T470" id="Seg_7288" s="T469">togonor-zittə</ta>
            <ta e="T471" id="Seg_7289" s="T470">ku-bia-m</ta>
            <ta e="T472" id="Seg_7290" s="T471">a</ta>
            <ta e="T473" id="Seg_7291" s="T472">amga</ta>
            <ta e="T474" id="Seg_7292" s="T473">ku-laʔpia-m</ta>
            <ta e="T478" id="Seg_7293" s="T477">koŋ</ta>
            <ta e="T479" id="Seg_7294" s="T478">ugandə</ta>
            <ta e="T480" id="Seg_7295" s="T479">kuvas</ta>
            <ta e="T482" id="Seg_7296" s="T481">bar</ta>
            <ta e="T483" id="Seg_7297" s="T482">ĭmbi</ta>
            <ta e="T484" id="Seg_7298" s="T483">măna</ta>
            <ta e="T485" id="Seg_7299" s="T484">mĭ-m-bi</ta>
            <ta e="T486" id="Seg_7300" s="T485">bar</ta>
            <ta e="T487" id="Seg_7301" s="T486">ĭmbi</ta>
            <ta e="T491" id="Seg_7302" s="T490">bar</ta>
            <ta e="T492" id="Seg_7303" s="T491">măna</ta>
            <ta e="T494" id="Seg_7304" s="T493">pʼer-bi</ta>
            <ta e="T501" id="Seg_7305" s="T500">mo-lə-j</ta>
            <ta e="T502" id="Seg_7306" s="T501">dʼala</ta>
            <ta e="T503" id="Seg_7307" s="T502">mo-lə-j</ta>
            <ta e="T504" id="Seg_7308" s="T503">amor-zittə</ta>
            <ta e="T508" id="Seg_7309" s="T507">teinen</ta>
            <ta e="T509" id="Seg_7310" s="T508">subbota</ta>
            <ta e="T510" id="Seg_7311" s="T509">năda</ta>
            <ta e="T511" id="Seg_7312" s="T510">moltʼa</ta>
            <ta e="T512" id="Seg_7313" s="T511">nendə-sʼtə</ta>
            <ta e="T513" id="Seg_7314" s="T512">bü</ta>
            <ta e="T514" id="Seg_7315" s="T513">tazir-zittə</ta>
            <ta e="T516" id="Seg_7316" s="T515">dĭgəttə</ta>
            <ta e="T517" id="Seg_7317" s="T516">nendə-le-m</ta>
            <ta e="T519" id="Seg_7318" s="T518">băzəj-də-sʼtə</ta>
            <ta e="T520" id="Seg_7319" s="T519">nada</ta>
            <ta e="T522" id="Seg_7320" s="T521">ato</ta>
            <ta e="T523" id="Seg_7321" s="T522">selej</ta>
            <ta e="T524" id="Seg_7322" s="T523">nedʼelʼa</ta>
            <ta e="T525" id="Seg_7323" s="T524">ej</ta>
            <ta e="T526" id="Seg_7324" s="T525">băzəl-də-bia-m</ta>
            <ta e="T530" id="Seg_7325" s="T529">maʔ-ə-n-nə</ta>
            <ta e="T531" id="Seg_7326" s="T530">sazən</ta>
            <ta e="T532" id="Seg_7327" s="T531">edeʔ-lie-m</ta>
            <ta e="T533" id="Seg_7328" s="T532">edəʔ-lie-m</ta>
            <ta e="T535" id="Seg_7329" s="T534">üge</ta>
            <ta e="T536" id="Seg_7330" s="T535">nada</ta>
            <ta e="T537" id="Seg_7331" s="T536">naga</ta>
            <ta e="T538" id="Seg_7332" s="T537">ne-m</ta>
            <ta e="T539" id="Seg_7333" s="T538">ej</ta>
            <ta e="T540" id="Seg_7334" s="T539">pʼaŋd-lia</ta>
            <ta e="T542" id="Seg_7335" s="T541">i</ta>
            <ta e="T543" id="Seg_7336" s="T542">sazən</ta>
            <ta e="T544" id="Seg_7337" s="T543">ej</ta>
            <ta e="T545" id="Seg_7338" s="T544">šo-lia</ta>
            <ta e="T548" id="Seg_7339" s="T547">teinen</ta>
            <ta e="T549" id="Seg_7340" s="T548">nüdʼi-n</ta>
            <ta e="T550" id="Seg_7341" s="T549">dʼodu-nʼi</ta>
            <ta e="T551" id="Seg_7342" s="T550">ku-bie-m</ta>
            <ta e="T553" id="Seg_7343" s="T552">dʼabro-laʔpie-m</ta>
            <ta e="T554" id="Seg_7344" s="T553">teinen</ta>
            <ta e="T555" id="Seg_7345" s="T554">sazən</ta>
            <ta e="T556" id="Seg_7346" s="T555">šo-lə-j</ta>
            <ta e="T560" id="Seg_7347" s="T559">măn</ta>
            <ta e="T561" id="Seg_7348" s="T560">mĭm-bie-m</ta>
            <ta e="T562" id="Seg_7349" s="T561">Krasnojaskăj-də</ta>
            <ta e="T1186" id="Seg_7350" s="T562">munə-ʔi</ta>
            <ta e="T564" id="Seg_7351" s="T563">kum-bia-m</ta>
            <ta e="T567" id="Seg_7352" s="T566">sto</ta>
            <ta e="T568" id="Seg_7353" s="T567">munəj</ta>
            <ta e="T570" id="Seg_7354" s="T569">dĭgəttə</ta>
            <ta e="T571" id="Seg_7355" s="T570">kaga-m</ta>
            <ta e="T572" id="Seg_7356" s="T571">măn-zi</ta>
            <ta e="T573" id="Seg_7357" s="T572">kam-bi</ta>
            <ta e="T575" id="Seg_7358" s="T574">dĭ</ta>
            <ta e="T576" id="Seg_7359" s="T575">măna</ta>
            <ta e="T577" id="Seg_7360" s="T576">kum-bi</ta>
            <ta e="T578" id="Seg_7361" s="T577">amno-bi</ta>
            <ta e="T579" id="Seg_7362" s="T578">amno-bi</ta>
            <ta e="T580" id="Seg_7363" s="T579">mašina-ʔi-zi</ta>
            <ta e="T582" id="Seg_7364" s="T581">măn</ta>
            <ta e="T584" id="Seg_7365" s="T583">ej</ta>
            <ta e="T585" id="Seg_7366" s="T584">mo-lia-m</ta>
            <ta e="T586" id="Seg_7367" s="T585">ku-zittə</ta>
            <ta e="T587" id="Seg_7368" s="T586">dĭgəttə</ta>
            <ta e="T588" id="Seg_7369" s="T587">ku-bia-m</ta>
            <ta e="T589" id="Seg_7370" s="T588">plemʼannica-m</ta>
            <ta e="T591" id="Seg_7371" s="T590">a</ta>
            <ta e="T592" id="Seg_7372" s="T591">dĭbər</ta>
            <ta e="T593" id="Seg_7373" s="T592">kan-də-bia-m</ta>
            <ta e="T595" id="Seg_7374" s="T594">döʔən</ta>
            <ta e="T596" id="Seg_7375" s="T595">mašina-zi</ta>
            <ta e="T598" id="Seg_7376" s="T597">a</ta>
            <ta e="T599" id="Seg_7377" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_7378" s="T599">aftobus-si</ta>
            <ta e="T602" id="Seg_7379" s="T601">dĭgəttə</ta>
            <ta e="T603" id="Seg_7380" s="T602">baza-j</ta>
            <ta e="T604" id="Seg_7381" s="T603">aktʼi</ta>
            <ta e="T606" id="Seg_7382" s="T605">aktʼa</ta>
            <ta e="T607" id="Seg_7383" s="T606">aktʼi</ta>
            <ta e="T608" id="Seg_7384" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_7385" s="T608">dĭbər</ta>
            <ta e="T610" id="Seg_7386" s="T609">šo-bia-m</ta>
            <ta e="T614" id="Seg_7387" s="T613">dĭgəttə</ta>
            <ta e="T615" id="Seg_7388" s="T614">mĭm-bie-m</ta>
            <ta e="T616" id="Seg_7389" s="T615">kudaj-də</ta>
            <ta e="T618" id="Seg_7390" s="T617">num-a-n</ta>
            <ta e="T619" id="Seg_7391" s="T618">üzit-tə</ta>
            <ta e="T621" id="Seg_7392" s="T620">dĭgəttə</ta>
            <ta e="T622" id="Seg_7393" s="T621">mĭm-bi-beʔ</ta>
            <ta e="T623" id="Seg_7394" s="T622">plemʼannica-zi</ta>
            <ta e="T624" id="Seg_7395" s="T623">žibiaʔi-nə</ta>
            <ta e="T626" id="Seg_7396" s="T625">dĭn</ta>
            <ta e="T628" id="Seg_7397" s="T627">amno-bi-baʔ</ta>
            <ta e="T629" id="Seg_7398" s="T628">da</ta>
            <ta e="T630" id="Seg_7399" s="T629">dʼăbaktər-bi-baʔ</ta>
            <ta e="T632" id="Seg_7400" s="T631">a</ta>
            <ta e="T633" id="Seg_7401" s="T632">dĭgəttə</ta>
            <ta e="T634" id="Seg_7402" s="T633">maʔ-ndə</ta>
            <ta e="T635" id="Seg_7403" s="T634">šo-bia-m</ta>
            <ta e="T639" id="Seg_7404" s="T638">plemʼannica</ta>
            <ta e="T640" id="Seg_7405" s="T639">măna</ta>
            <ta e="T641" id="Seg_7406" s="T640">pomidor</ta>
            <ta e="T642" id="Seg_7407" s="T641">iʔgö</ta>
            <ta e="T643" id="Seg_7408" s="T642">mĭ-bi</ta>
            <ta e="T644" id="Seg_7409" s="T643">măn</ta>
            <ta e="T1184" id="Seg_7410" s="T645">Kazan</ta>
            <ta e="T646" id="Seg_7411" s="T1184">tura-gən</ta>
            <ta e="T647" id="Seg_7412" s="T646">šide</ta>
            <ta e="T648" id="Seg_7413" s="T647">dʼala</ta>
            <ta e="T649" id="Seg_7414" s="T648">amno-bia-m</ta>
            <ta e="T653" id="Seg_7415" s="T652">tüžöj-ʔi</ta>
            <ta e="T654" id="Seg_7416" s="T653">šo-bi-ʔi</ta>
            <ta e="T655" id="Seg_7417" s="T654">maʔ-ndə</ta>
            <ta e="T656" id="Seg_7418" s="T655">nada</ta>
            <ta e="T657" id="Seg_7419" s="T656">dĭ-zem</ta>
            <ta e="T658" id="Seg_7420" s="T657">šeden-də</ta>
            <ta e="T659" id="Seg_7421" s="T658">sürer-zittə</ta>
            <ta e="T660" id="Seg_7422" s="T659">i</ta>
            <ta e="T662" id="Seg_7423" s="T661">noʔmə-zittə</ta>
            <ta e="T666" id="Seg_7424" s="T665">tenö-bia-m</ta>
            <ta e="T667" id="Seg_7425" s="T666">tenö-bia-m</ta>
            <ta e="T668" id="Seg_7426" s="T667">ĭmbi=nʼibudʼ</ta>
            <ta e="T669" id="Seg_7427" s="T668">tenö-luʔ-pie-m</ta>
            <ta e="T673" id="Seg_7428" s="T672">koʔbdo-nə</ta>
            <ta e="T674" id="Seg_7429" s="T673">mĭm-bie-m</ta>
            <ta e="T675" id="Seg_7430" s="T674">sumka</ta>
            <ta e="T676" id="Seg_7431" s="T675">dĭ</ta>
            <ta e="T677" id="Seg_7432" s="T676">bar</ta>
            <ta e="T678" id="Seg_7433" s="T677">sajnʼi-luʔpi</ta>
            <ta e="T680" id="Seg_7434" s="T679">tüj</ta>
            <ta e="T681" id="Seg_7435" s="T680">amno-laʔbə-m</ta>
            <ta e="T682" id="Seg_7436" s="T681">da</ta>
            <ta e="T684" id="Seg_7437" s="T683">šüdör-leʔbə-m</ta>
            <ta e="T686" id="Seg_7438" s="T685">tănan</ta>
            <ta e="T687" id="Seg_7439" s="T686">šonə-bia-m</ta>
            <ta e="T688" id="Seg_7440" s="T687">šonə-bia-m</ta>
            <ta e="T689" id="Seg_7441" s="T688">appi</ta>
            <ta e="T690" id="Seg_7442" s="T689">ej</ta>
            <ta e="T691" id="Seg_7443" s="T690">saʔmə-luʔ-pia-m</ta>
            <ta e="T696" id="Seg_7444" s="T695">kozaŋ</ta>
            <ta e="T698" id="Seg_7445" s="T697">kazaŋ</ta>
            <ta e="T699" id="Seg_7446" s="T698">šonə-ga</ta>
            <ta e="T700" id="Seg_7447" s="T699">šonə-ga</ta>
            <ta e="T701" id="Seg_7448" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_7449" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_7450" s="T702">dĭʔ-nə</ta>
            <ta e="T705" id="Seg_7451" s="T704">püje</ta>
            <ta e="T708" id="Seg_7452" s="T707">băt-luʔbi</ta>
            <ta e="T709" id="Seg_7453" s="T708">kem-də</ta>
            <ta e="T710" id="Seg_7454" s="T709">bar</ta>
            <ta e="T711" id="Seg_7455" s="T710">mʼaŋ-naʔbə</ta>
            <ta e="T713" id="Seg_7456" s="T712">dĭ</ta>
            <ta e="T714" id="Seg_7457" s="T713">măn-də</ta>
            <ta e="T715" id="Seg_7458" s="T714">šü</ta>
            <ta e="T717" id="Seg_7459" s="T716">nendə-də</ta>
            <ta e="T718" id="Seg_7460" s="T717">dĭ</ta>
            <ta e="T719" id="Seg_7461" s="T718">noʔ</ta>
            <ta e="T723" id="Seg_7462" s="T722">măn</ta>
            <ta e="T724" id="Seg_7463" s="T723">iʔgö</ta>
            <ta e="T725" id="Seg_7464" s="T724">dʼü-m</ta>
            <ta e="T726" id="Seg_7465" s="T725">nendə-sʼtə</ta>
            <ta e="T727" id="Seg_7466" s="T726">dĭgəttə</ta>
            <ta e="T728" id="Seg_7467" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_7468" s="T728">bü-nə</ta>
            <ta e="T730" id="Seg_7469" s="T729">kam-bi</ta>
            <ta e="T732" id="Seg_7470" s="T731">bü</ta>
            <ta e="T733" id="Seg_7471" s="T732">kan-a-ʔ</ta>
            <ta e="T734" id="Seg_7472" s="T733">šü-m</ta>
            <ta e="T736" id="Seg_7473" s="T735">kămna-ʔ</ta>
            <ta e="T740" id="Seg_7474" s="T739">măn</ta>
            <ta e="T741" id="Seg_7475" s="T740">iʔgö</ta>
            <ta e="T743" id="Seg_7476" s="T742">dʼü</ta>
            <ta e="T744" id="Seg_7477" s="T743">kăm-na-stə</ta>
            <ta e="T745" id="Seg_7478" s="T744">štobɨ</ta>
            <ta e="T746" id="Seg_7479" s="T745">noʔ</ta>
            <ta e="T747" id="Seg_7480" s="T746">özer-bi</ta>
            <ta e="T751" id="Seg_7481" s="T750">dĭgəttə</ta>
            <ta e="T752" id="Seg_7482" s="T751">dĭ</ta>
            <ta e="T753" id="Seg_7483" s="T752">kam-bi</ta>
            <ta e="T755" id="Seg_7484" s="T754">bulan</ta>
            <ta e="T756" id="Seg_7485" s="T755">bulan</ta>
            <ta e="T757" id="Seg_7486" s="T756">kan-a-ʔ</ta>
            <ta e="T758" id="Seg_7487" s="T757">bar</ta>
            <ta e="T759" id="Seg_7488" s="T758">bü</ta>
            <ta e="T761" id="Seg_7489" s="T760">bĭd-e-ʔ</ta>
            <ta e="T763" id="Seg_7490" s="T762">dĭgəttə</ta>
            <ta e="T764" id="Seg_7491" s="T763">bulan</ta>
            <ta e="T765" id="Seg_7492" s="T764">măm-bi</ta>
            <ta e="T766" id="Seg_7493" s="T765">măn</ta>
            <ta e="T767" id="Seg_7494" s="T766">üjü-ndə</ta>
            <ta e="T768" id="Seg_7495" s="T767">iʔgö</ta>
            <ta e="T769" id="Seg_7496" s="T768">bü</ta>
            <ta e="T771" id="Seg_7497" s="T770">dĭgəttə</ta>
            <ta e="T772" id="Seg_7498" s="T771">dĭ</ta>
            <ta e="T774" id="Seg_7499" s="T773">kam-bi</ta>
            <ta e="T775" id="Seg_7500" s="T774">nüke-nə</ta>
            <ta e="T776" id="Seg_7501" s="T775">nüke</ta>
            <ta e="T777" id="Seg_7502" s="T776">nüke</ta>
            <ta e="T778" id="Seg_7503" s="T777">kan-a-ʔ</ta>
            <ta e="T779" id="Seg_7504" s="T778">žila-ʔi</ta>
            <ta e="T780" id="Seg_7505" s="T779">üjü-ttə</ta>
            <ta e="T781" id="Seg_7506" s="T780">i-t</ta>
            <ta e="T783" id="Seg_7507" s="T782">nüke</ta>
            <ta e="T784" id="Seg_7508" s="T783">ej</ta>
            <ta e="T785" id="Seg_7509" s="T784">kam-bi</ta>
            <ta e="T787" id="Seg_7510" s="T786">sagər-ʔi</ta>
            <ta e="T788" id="Seg_7511" s="T787">kabarləj</ta>
            <ta e="T789" id="Seg_7512" s="T788">măna</ta>
            <ta e="T793" id="Seg_7513" s="T792">dĭ</ta>
            <ta e="T794" id="Seg_7514" s="T793">kam-bi</ta>
            <ta e="T795" id="Seg_7515" s="T794">nüke-nə</ta>
            <ta e="T796" id="Seg_7516" s="T795">nüke</ta>
            <ta e="T797" id="Seg_7517" s="T796">nüke</ta>
            <ta e="T798" id="Seg_7518" s="T797">i-t</ta>
            <ta e="T800" id="Seg_7519" s="T799">žila-ʔi</ta>
            <ta e="T801" id="Seg_7520" s="T800">a</ta>
            <ta e="T802" id="Seg_7521" s="T801">dĭ</ta>
            <ta e="T803" id="Seg_7522" s="T802">măn-də</ta>
            <ta e="T804" id="Seg_7523" s="T803">măn</ta>
            <ta e="T805" id="Seg_7524" s="T804">iʔgö</ta>
            <ta e="T806" id="Seg_7525" s="T805">i-ge</ta>
            <ta e="T807" id="Seg_7526" s="T806">da</ta>
            <ta e="T808" id="Seg_7527" s="T807">sagər</ta>
            <ta e="T809" id="Seg_7528" s="T808">măna</ta>
            <ta e="T810" id="Seg_7529" s="T809">ej</ta>
            <ta e="T811" id="Seg_7530" s="T810">kereʔ</ta>
            <ta e="T815" id="Seg_7531" s="T814">dĭgəttə</ta>
            <ta e="T816" id="Seg_7532" s="T815">kam-bi</ta>
            <ta e="T817" id="Seg_7533" s="T816">tumo-ʔi-nə</ta>
            <ta e="T818" id="Seg_7534" s="T817">tumo</ta>
            <ta e="T819" id="Seg_7535" s="T818">tumo</ta>
            <ta e="T820" id="Seg_7536" s="T819">kan-a-ʔ</ta>
            <ta e="T821" id="Seg_7537" s="T820">nüke-n</ta>
            <ta e="T822" id="Seg_7538" s="T821">žila-ʔi</ta>
            <ta e="T823" id="Seg_7539" s="T822">amna-ʔ</ta>
            <ta e="T825" id="Seg_7540" s="T824">dʼok</ta>
            <ta e="T827" id="Seg_7541" s="T826">miʔ</ta>
            <ta e="T828" id="Seg_7542" s="T827">e-m</ta>
            <ta e="T829" id="Seg_7543" s="T828">kal-laʔ</ta>
            <ta e="T830" id="Seg_7544" s="T829">ej</ta>
            <ta e="T831" id="Seg_7545" s="T830">kal-la-m</ta>
            <ta e="T833" id="Seg_7546" s="T832">miʔnʼibeʔ</ta>
            <ta e="T834" id="Seg_7547" s="T833">kabarləj</ta>
            <ta e="T835" id="Seg_7548" s="T834">dʼü-gən</ta>
            <ta e="T836" id="Seg_7549" s="T835">kornʼi-ʔi</ta>
            <ta e="T837" id="Seg_7550" s="T836">am-zittə</ta>
            <ta e="T841" id="Seg_7551" s="T840">dĭgəttə</ta>
            <ta e="T842" id="Seg_7552" s="T841">kam-bi</ta>
            <ta e="T844" id="Seg_7553" s="T843">es-seŋ-də</ta>
            <ta e="T845" id="Seg_7554" s="T844">es-seŋ</ta>
            <ta e="T846" id="Seg_7555" s="T845">es-seŋ</ta>
            <ta e="T847" id="Seg_7556" s="T846">kut-laʔ-ləj</ta>
            <ta e="T848" id="Seg_7557" s="T847">tumo-ʔi</ta>
            <ta e="T849" id="Seg_7558" s="T848">dĭ-zeŋ</ta>
            <ta e="T850" id="Seg_7559" s="T849">miʔ</ta>
            <ta e="T852" id="Seg_7560" s="T851">bos-pə</ta>
            <ta e="T853" id="Seg_7561" s="T852">sʼar-laʔbə-ʔjə</ta>
            <ta e="T854" id="Seg_7562" s="T853">baːbka-ʔi-ziʔ</ta>
            <ta e="T858" id="Seg_7563" s="T857">šiʔ</ta>
            <ta e="T859" id="Seg_7564" s="T858">tura-gən</ta>
            <ta e="T860" id="Seg_7565" s="T859">jakše</ta>
            <ta e="T861" id="Seg_7566" s="T860">amno-zittə</ta>
            <ta e="T863" id="Seg_7567" s="T862">tolʼko</ta>
            <ta e="T864" id="Seg_7568" s="T863">ipek</ta>
            <ta e="T865" id="Seg_7569" s="T864">gijen=də</ta>
            <ta e="T866" id="Seg_7570" s="T865">ej</ta>
            <ta e="T867" id="Seg_7571" s="T866">i-li-l</ta>
            <ta e="T869" id="Seg_7572" s="T868">am-zittə</ta>
            <ta e="T870" id="Seg_7573" s="T869">naga</ta>
            <ta e="T871" id="Seg_7574" s="T870">ipek</ta>
            <ta e="T875" id="Seg_7575" s="T874">magazin-gəndə</ta>
            <ta e="T876" id="Seg_7576" s="T875">ĭmbi</ta>
            <ta e="T877" id="Seg_7577" s="T876">naga</ta>
            <ta e="T878" id="Seg_7578" s="T877">ĭmbi=də</ta>
            <ta e="T879" id="Seg_7579" s="T878">ej</ta>
            <ta e="T880" id="Seg_7580" s="T879">i-lə-l</ta>
            <ta e="T882" id="Seg_7581" s="T881">dʼok</ta>
            <ta e="T883" id="Seg_7582" s="T882">kan-a-ʔ</ta>
            <ta e="T884" id="Seg_7583" s="T883">dĭn</ta>
            <ta e="T885" id="Seg_7584" s="T884">i-ge</ta>
            <ta e="T886" id="Seg_7585" s="T885">kălbasa</ta>
            <ta e="T887" id="Seg_7586" s="T886">i-ʔ</ta>
            <ta e="T888" id="Seg_7587" s="T887">da</ta>
            <ta e="T889" id="Seg_7588" s="T888">dĭgəttə</ta>
            <ta e="T890" id="Seg_7589" s="T889">amor-la-l</ta>
            <ta e="T894" id="Seg_7590" s="T893">dĭ</ta>
            <ta e="T895" id="Seg_7591" s="T894">nüke</ta>
            <ta e="T896" id="Seg_7592" s="T895">ugandə</ta>
            <ta e="T897" id="Seg_7593" s="T896">jakše</ta>
            <ta e="T898" id="Seg_7594" s="T897">šödör-laʔbə</ta>
            <ta e="T899" id="Seg_7595" s="T898">bar</ta>
            <ta e="T900" id="Seg_7596" s="T899">kuvas</ta>
            <ta e="T902" id="Seg_7597" s="T901">jakše</ta>
            <ta e="T903" id="Seg_7598" s="T902">măndo-sʼtə</ta>
            <ta e="T907" id="Seg_7599" s="T906">dĭ</ta>
            <ta e="T908" id="Seg_7600" s="T907">nüke</ta>
            <ta e="T909" id="Seg_7601" s="T908">ugandə</ta>
            <ta e="T910" id="Seg_7602" s="T909">iʔgö</ta>
            <ta e="T912" id="Seg_7603" s="T911">togonor-ia</ta>
            <ta e="T913" id="Seg_7604" s="T912">bar</ta>
            <ta e="T914" id="Seg_7605" s="T913">ĭmbi</ta>
            <ta e="T915" id="Seg_7606" s="T914">a-zittə</ta>
            <ta e="T920" id="Seg_7607" s="T919">bar</ta>
            <ta e="T921" id="Seg_7608" s="T920">ĭmbi</ta>
            <ta e="T922" id="Seg_7609" s="T921">togonor-zittə</ta>
            <ta e="T923" id="Seg_7610" s="T922">tĭmne-t</ta>
            <ta e="T927" id="Seg_7611" s="T926">tĭn</ta>
            <ta e="T928" id="Seg_7612" s="T927">nüke-n</ta>
            <ta e="T930" id="Seg_7613" s="T929">tuka-zaŋ-də</ta>
            <ta e="T931" id="Seg_7614" s="T930">amga</ta>
            <ta e="T932" id="Seg_7615" s="T931">amna-m-bi</ta>
            <ta e="T933" id="Seg_7616" s="T932">tolʼko</ta>
            <ta e="T934" id="Seg_7617" s="T933">onʼiʔ</ta>
            <ta e="T935" id="Seg_7618" s="T934">kaga-t</ta>
            <ta e="T939" id="Seg_7619" s="T938">a</ta>
            <ta e="T940" id="Seg_7620" s="T939">sestra-t</ta>
            <ta e="T941" id="Seg_7621" s="T940">kü-lam-bi</ta>
            <ta e="T945" id="Seg_7622" s="T944">šödör-luʔ-pie-m</ta>
            <ta e="T946" id="Seg_7623" s="T945">bar</ta>
            <ta e="T947" id="Seg_7624" s="T946">tüj</ta>
            <ta e="T948" id="Seg_7625" s="T947">naga</ta>
            <ta e="T949" id="Seg_7626" s="T948">šöʔ-sittə</ta>
            <ta e="T953" id="Seg_7627" s="T952">miʔ</ta>
            <ta e="T954" id="Seg_7628" s="T953">jelezʼe</ta>
            <ta e="T955" id="Seg_7629" s="T954">kam-bi-baʔ</ta>
            <ta e="T956" id="Seg_7630" s="T955">bü</ta>
            <ta e="T957" id="Seg_7631" s="T956">tažer-zittə</ta>
            <ta e="T958" id="Seg_7632" s="T957">a</ta>
            <ta e="T959" id="Seg_7633" s="T958">dĭn</ta>
            <ta e="T960" id="Seg_7634" s="T959">bü</ta>
            <ta e="T961" id="Seg_7635" s="T960">i-ge</ta>
            <ta e="T963" id="Seg_7636" s="T962">nagur</ta>
            <ta e="T965" id="Seg_7637" s="T964">nagur</ta>
            <ta e="T966" id="Seg_7638" s="T965">vedro</ta>
            <ta e="T967" id="Seg_7639" s="T966">deʔ-pi-beʔ</ta>
            <ta e="T968" id="Seg_7640" s="T967">i</ta>
            <ta e="T969" id="Seg_7641" s="T968">kabarləj</ta>
            <ta e="T970" id="Seg_7642" s="T969">tüj</ta>
            <ta e="T974" id="Seg_7643" s="T973">dʼije-gən</ta>
            <ta e="T975" id="Seg_7644" s="T974">šonə-ga-m</ta>
            <ta e="T976" id="Seg_7645" s="T975">dʼije</ta>
            <ta e="T977" id="Seg_7646" s="T976">nüj-leʔbə-m</ta>
            <ta e="T979" id="Seg_7647" s="T978">pa-gən</ta>
            <ta e="T980" id="Seg_7648" s="T979">šonə-ga-m</ta>
            <ta e="T981" id="Seg_7649" s="T980">pa</ta>
            <ta e="T982" id="Seg_7650" s="T981">nüj-leʔbə-m</ta>
            <ta e="T984" id="Seg_7651" s="T983">sʼtʼep</ta>
            <ta e="T985" id="Seg_7652" s="T984">šonə-ga-m</ta>
            <ta e="T986" id="Seg_7653" s="T985">sʼtʼep</ta>
            <ta e="T987" id="Seg_7654" s="T986">nüj-leʔbə-m</ta>
            <ta e="T989" id="Seg_7655" s="T988">măja</ta>
            <ta e="T990" id="Seg_7656" s="T989">šonə-ga-m</ta>
            <ta e="T991" id="Seg_7657" s="T990">măja</ta>
            <ta e="T992" id="Seg_7658" s="T991">nüj-leʔbə-m</ta>
            <ta e="T994" id="Seg_7659" s="T993">Dʼăla-m</ta>
            <ta e="T995" id="Seg_7660" s="T994">šonə-ga</ta>
            <ta e="T996" id="Seg_7661" s="T995">Dʼăla-m</ta>
            <ta e="T998" id="Seg_7662" s="T997">nüj-leʔbə-m</ta>
            <ta e="T1000" id="Seg_7663" s="T999">tura-gən</ta>
            <ta e="T1001" id="Seg_7664" s="T1000">amno-laʔbə</ta>
            <ta e="T1002" id="Seg_7665" s="T1001">tura</ta>
            <ta e="T1003" id="Seg_7666" s="T1002">nüj-leʔbə-m</ta>
            <ta e="T1007" id="Seg_7667" s="T1006">miʔ</ta>
            <ta e="T1008" id="Seg_7668" s="T1007">măn</ta>
            <ta e="T1009" id="Seg_7669" s="T1008">tura-nʼi</ta>
            <ta e="T1010" id="Seg_7670" s="T1009">ugandə</ta>
            <ta e="T1011" id="Seg_7671" s="T1010">tumo</ta>
            <ta e="T1012" id="Seg_7672" s="T1011">iʔgö</ta>
            <ta e="T1013" id="Seg_7673" s="T1012">ĭmbi</ta>
            <ta e="T1014" id="Seg_7674" s="T1013">păpală</ta>
            <ta e="T1015" id="Seg_7675" s="T1014">bar</ta>
            <ta e="T1016" id="Seg_7676" s="T1015">am-nia-ʔi</ta>
            <ta e="T1018" id="Seg_7677" s="T1017">a</ta>
            <ta e="T1019" id="Seg_7678" s="T1018">tospak</ta>
            <ta e="T1020" id="Seg_7679" s="T1019">naga</ta>
            <ta e="T1022" id="Seg_7680" s="T1021">kan-a-ʔ</ta>
            <ta e="T1023" id="Seg_7681" s="T1022">miʔne-nə</ta>
            <ta e="T1024" id="Seg_7682" s="T1023">dĭ</ta>
            <ta e="T1025" id="Seg_7683" s="T1024">dĭn</ta>
            <ta e="T1026" id="Seg_7684" s="T1025">šide</ta>
            <ta e="T1028" id="Seg_7685" s="T1027">tospak</ta>
            <ta e="T1029" id="Seg_7686" s="T1028">onʼiʔ</ta>
            <ta e="T1030" id="Seg_7687" s="T1029">sĭre</ta>
            <ta e="T1031" id="Seg_7688" s="T1030">onʼiʔ</ta>
            <ta e="T1032" id="Seg_7689" s="T1031">komə</ta>
            <ta e="T1034" id="Seg_7690" s="T1033">dĭ</ta>
            <ta e="T1035" id="Seg_7691" s="T1034">mĭ-lə-j</ta>
            <ta e="T1036" id="Seg_7692" s="T1035">tănan</ta>
            <ta e="T1038" id="Seg_7693" s="T1037">da</ta>
            <ta e="T1039" id="Seg_7694" s="T1038">dĭ</ta>
            <ta e="T1040" id="Seg_7695" s="T1039">sĭre</ta>
            <ta e="T1041" id="Seg_7696" s="T1040">măna</ta>
            <ta e="T1042" id="Seg_7697" s="T1041">i-bi</ta>
            <ta e="T1043" id="Seg_7698" s="T1042">da</ta>
            <ta e="T1181" id="Seg_7699" s="T1043">kal-la</ta>
            <ta e="T1044" id="Seg_7700" s="T1181">dʼür-bi</ta>
            <ta e="T1045" id="Seg_7701" s="T1044">dĭ</ta>
            <ta e="T1046" id="Seg_7702" s="T1045">dĭ-m</ta>
            <ta e="T1047" id="Seg_7703" s="T1046">ej</ta>
            <ta e="T1048" id="Seg_7704" s="T1047">öʔ-lü-bi</ta>
            <ta e="T1050" id="Seg_7705" s="T1049">daška</ta>
            <ta e="T1051" id="Seg_7706" s="T1050">ej</ta>
            <ta e="T1052" id="Seg_7707" s="T1051">šo-lia</ta>
            <ta e="T1056" id="Seg_7708" s="T1055">măn</ta>
            <ta e="T1057" id="Seg_7709" s="T1056">kunol-zittə</ta>
            <ta e="T1058" id="Seg_7710" s="T1057">iʔ-pie-m</ta>
            <ta e="T1060" id="Seg_7711" s="T1059">a</ta>
            <ta e="T1061" id="Seg_7712" s="T1060">dĭn</ta>
            <ta e="T1062" id="Seg_7713" s="T1061">ugandə</ta>
            <ta e="T1063" id="Seg_7714" s="T1062">putʼuga</ta>
            <ta e="T1064" id="Seg_7715" s="T1063">ku-lia-m</ta>
            <ta e="T1065" id="Seg_7716" s="T1064">tüʔ</ta>
            <ta e="T1067" id="Seg_7717" s="T1066">iʔgö</ta>
            <ta e="T1068" id="Seg_7718" s="T1067">iʔbo-laʔbə</ta>
            <ta e="T1072" id="Seg_7719" s="T1071">ertə-n</ta>
            <ta e="T1073" id="Seg_7720" s="T1072">uʔbdə-bia-m</ta>
            <ta e="T1074" id="Seg_7721" s="T1073">kălenka-nə</ta>
            <ta e="T1075" id="Seg_7722" s="T1074">nu-bia-m</ta>
            <ta e="T1077" id="Seg_7723" s="T1076">ku-lio-m</ta>
            <ta e="T1078" id="Seg_7724" s="T1077">bar</ta>
            <ta e="T1079" id="Seg_7725" s="T1078">dĭn</ta>
            <ta e="T1080" id="Seg_7726" s="T1079">bʼeʔ</ta>
            <ta e="T1081" id="Seg_7727" s="T1080">kuči</ta>
            <ta e="T1082" id="Seg_7728" s="T1081">iʔbo-laʔbə</ta>
            <ta e="T1086" id="Seg_7729" s="T1085">dĭgəttə</ta>
            <ta e="T1087" id="Seg_7730" s="T1086">uja-m</ta>
            <ta e="T1088" id="Seg_7731" s="T1087">kun-ə-lamba-m</ta>
            <ta e="T1089" id="Seg_7732" s="T1088">niʔtə</ta>
            <ta e="T1090" id="Seg_7733" s="T1089">takše-gən</ta>
            <ta e="T1091" id="Seg_7734" s="T1090">takše-zi</ta>
            <ta e="T1092" id="Seg_7735" s="T1091">kaj-lia-m</ta>
            <ta e="T1093" id="Seg_7736" s="T1092">štobɨ</ta>
            <ta e="T1094" id="Seg_7737" s="T1093">tumo-ʔi</ta>
            <ta e="T1095" id="Seg_7738" s="T1094">ej</ta>
            <ta e="T1096" id="Seg_7739" s="T1095">am-bi-ʔi</ta>
            <ta e="T1098" id="Seg_7740" s="T1097">a</ta>
            <ta e="T1099" id="Seg_7741" s="T1098">kajaʔ</ta>
            <ta e="T1100" id="Seg_7742" s="T1099">măslăbojkă-nə</ta>
            <ta e="T1101" id="Seg_7743" s="T1100">tože</ta>
            <ta e="T1103" id="Seg_7744" s="T1102">kaj-la-m</ta>
            <ta e="T1104" id="Seg_7745" s="T1103">štobɨ</ta>
            <ta e="T1105" id="Seg_7746" s="T1104">tumo-ʔi</ta>
            <ta e="T1106" id="Seg_7747" s="T1105">ej</ta>
            <ta e="T1107" id="Seg_7748" s="T1106">am-bi-ʔi</ta>
            <ta e="T1111" id="Seg_7749" s="T1110">a</ta>
            <ta e="T1113" id="Seg_7750" s="T1112">dăk</ta>
            <ta e="T1114" id="Seg_7751" s="T1113">miʔnʼibeʔ</ta>
            <ta e="T1115" id="Seg_7752" s="T1114">šide</ta>
            <ta e="T1116" id="Seg_7753" s="T1115">i-ge</ta>
            <ta e="T1117" id="Seg_7754" s="T1116">tospak</ta>
            <ta e="T1118" id="Seg_7755" s="T1117">det-le-m</ta>
            <ta e="T1119" id="Seg_7756" s="T1118">onʼiʔ</ta>
            <ta e="T1121" id="Seg_7757" s="T1120">girgit</ta>
            <ta e="T1122" id="Seg_7758" s="T1121">tănan</ta>
            <ta e="T1123" id="Seg_7759" s="T1122">deʔ-sittə</ta>
            <ta e="T1124" id="Seg_7760" s="T1123">sĭre</ta>
            <ta e="T1125" id="Seg_7761" s="T1124">ali</ta>
            <ta e="T1126" id="Seg_7762" s="T1125">komə</ta>
            <ta e="T1128" id="Seg_7763" s="T1127">nu</ta>
            <ta e="T1129" id="Seg_7764" s="T1128">de-ʔ</ta>
            <ta e="T1130" id="Seg_7765" s="T1129">komə</ta>
            <ta e="T1134" id="Seg_7766" s="T1133">moltʼa-nə</ta>
            <ta e="T1135" id="Seg_7767" s="T1134">šo-ga</ta>
            <ta e="T1136" id="Seg_7768" s="T1135">dĭn</ta>
            <ta e="T1137" id="Seg_7769" s="T1136">ugandə</ta>
            <ta e="T1138" id="Seg_7770" s="T1137">bü</ta>
            <ta e="T1139" id="Seg_7771" s="T1138">iʔgö</ta>
            <ta e="T1141" id="Seg_7772" s="T1140">dʼibige</ta>
            <ta e="T1142" id="Seg_7773" s="T1141">bü</ta>
            <ta e="T1143" id="Seg_7774" s="T1142">i-ge</ta>
            <ta e="T1144" id="Seg_7775" s="T1143">šišege</ta>
            <ta e="T1145" id="Seg_7776" s="T1144">sabən</ta>
            <ta e="T1146" id="Seg_7777" s="T1145">i-ge</ta>
            <ta e="T1148" id="Seg_7778" s="T1147">ejü</ta>
            <ta e="T1149" id="Seg_7779" s="T1148">ejü-m-ne-l</ta>
            <ta e="T1150" id="Seg_7780" s="T1149">bar</ta>
            <ta e="T1154" id="Seg_7781" s="T1153">püje-l</ta>
            <ta e="T1155" id="Seg_7782" s="T1154">ej</ta>
            <ta e="T1156" id="Seg_7783" s="T1155">ĭzem-ne</ta>
            <ta e="T1157" id="Seg_7784" s="T1156">i</ta>
            <ta e="T1158" id="Seg_7785" s="T1157">koklu-ʔi</ta>
            <ta e="T1159" id="Seg_7786" s="T1158">mʼaŋ-na-ʔi</ta>
            <ta e="T1163" id="Seg_7787" s="T1162">dö</ta>
            <ta e="T1164" id="Seg_7788" s="T1163">zaplatka</ta>
            <ta e="T1165" id="Seg_7789" s="T1164">a</ta>
            <ta e="T1166" id="Seg_7790" s="T1165">gibər</ta>
            <ta e="T1167" id="Seg_7791" s="T1166">dĭ-m</ta>
            <ta e="T1168" id="Seg_7792" s="T1167">zaplatka</ta>
            <ta e="T1170" id="Seg_7793" s="T1169">köten-gəndə</ta>
            <ta e="T1171" id="Seg_7794" s="T1170">amnol-zittə</ta>
            <ta e="T1172" id="Seg_7795" s="T1171">štoli</ta>
            <ta e="T1174" id="Seg_7796" s="T1173">barə-btɨ-ʔ</ta>
            <ta e="T1175" id="Seg_7797" s="T1174">ej</ta>
            <ta e="T1176" id="Seg_7798" s="T1175">kereʔ</ta>
            <ta e="T1177" id="Seg_7799" s="T1176">măna</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_7800" s="T1">măn</ta>
            <ta e="T3" id="Seg_7801" s="T2">taldʼen</ta>
            <ta e="T4" id="Seg_7802" s="T3">kan-bi-m</ta>
            <ta e="T5" id="Seg_7803" s="T4">tugan-m</ta>
            <ta e="T7" id="Seg_7804" s="T6">măn</ta>
            <ta e="T8" id="Seg_7805" s="T7">măn-bi-m</ta>
            <ta e="T9" id="Seg_7806" s="T8">e-ʔ</ta>
            <ta e="T10" id="Seg_7807" s="T9">kuroː-ʔ</ta>
            <ta e="T11" id="Seg_7808" s="T10">măna</ta>
            <ta e="T14" id="Seg_7809" s="T13">šöʔ-zittə</ta>
            <ta e="T16" id="Seg_7810" s="T15">mej-m</ta>
            <ta e="T17" id="Seg_7811" s="T16">ej</ta>
            <ta e="T18" id="Seg_7812" s="T17">kan-bi</ta>
            <ta e="T19" id="Seg_7813" s="T18">i</ta>
            <ta e="T20" id="Seg_7814" s="T19">măn</ta>
            <ta e="T21" id="Seg_7815" s="T20">ej</ta>
            <ta e="T22" id="Seg_7816" s="T21">kan-bi-m</ta>
            <ta e="T24" id="Seg_7817" s="T23">dĭ-zAŋ</ta>
            <ta e="T25" id="Seg_7818" s="T24">krăvatʼ-Kən</ta>
            <ta e="T26" id="Seg_7819" s="T25">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T27" id="Seg_7820" s="T26">kamro-laʔbə-jəʔ</ta>
            <ta e="T29" id="Seg_7821" s="T28">Kolʼa</ta>
            <ta e="T30" id="Seg_7822" s="T29">Dunʼa-t-ziʔ</ta>
            <ta e="T32" id="Seg_7823" s="T31">panar-laʔbə-jəʔ</ta>
            <ta e="T34" id="Seg_7824" s="T33">kakənar-laʔbə-jəʔ</ta>
            <ta e="T36" id="Seg_7825" s="T35">măn-ntə-jəʔ</ta>
            <ta e="T37" id="Seg_7826" s="T36">urgaja</ta>
            <ta e="T38" id="Seg_7827" s="T37">am-ə-ʔ</ta>
            <ta e="T39" id="Seg_7828" s="T38">amnə-ʔ</ta>
            <ta e="T41" id="Seg_7829" s="T40">ara</ta>
            <ta e="T42" id="Seg_7830" s="T41">bĭs-ə-ʔ</ta>
            <ta e="T44" id="Seg_7831" s="T43">măn</ta>
            <ta e="T45" id="Seg_7832" s="T44">măn-ntə-m</ta>
            <ta e="T46" id="Seg_7833" s="T45">ej</ta>
            <ta e="T47" id="Seg_7834" s="T46">bĭs-lV-m</ta>
            <ta e="T49" id="Seg_7835" s="T48">i</ta>
            <ta e="T50" id="Seg_7836" s="T49">dĭgəttə</ta>
            <ta e="T1178" id="Seg_7837" s="T50">kan-lAʔ</ta>
            <ta e="T51" id="Seg_7838" s="T1178">tʼür-bi-m</ta>
            <ta e="T52" id="Seg_7839" s="T51">maʔ-gənʼi</ta>
            <ta e="T56" id="Seg_7840" s="T55">măn</ta>
            <ta e="T57" id="Seg_7841" s="T56">teinen</ta>
            <ta e="T58" id="Seg_7842" s="T57">nüke-Tə</ta>
            <ta e="T59" id="Seg_7843" s="T58">kan-bi-m</ta>
            <ta e="T60" id="Seg_7844" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_7845" s="T60">ĭzem-liA</ta>
            <ta e="T63" id="Seg_7846" s="T62">dĭ</ta>
            <ta e="T64" id="Seg_7847" s="T63">măn-ntə</ta>
            <ta e="T65" id="Seg_7848" s="T64">ĭmbi</ta>
            <ta e="T66" id="Seg_7849" s="T65">kondʼo</ta>
            <ta e="T67" id="Seg_7850" s="T66">ej</ta>
            <ta e="T68" id="Seg_7851" s="T67">šo-bi-m</ta>
            <ta e="T69" id="Seg_7852" s="T68">măn</ta>
            <ta e="T70" id="Seg_7853" s="T69">bar</ta>
            <ta e="T71" id="Seg_7854" s="T70">koʔ-landə-gA-m</ta>
            <ta e="T72" id="Seg_7855" s="T71">tăn</ta>
            <ta e="T73" id="Seg_7856" s="T72">üge</ta>
            <ta e="T74" id="Seg_7857" s="T73">koʔ-landə-gA-l</ta>
            <ta e="T75" id="Seg_7858" s="T74">a</ta>
            <ta e="T76" id="Seg_7859" s="T75">kü-zittə</ta>
            <ta e="T77" id="Seg_7860" s="T76">ej</ta>
            <ta e="T78" id="Seg_7861" s="T77">mo-liA-l</ta>
            <ta e="T80" id="Seg_7862" s="T79">no</ta>
            <ta e="T81" id="Seg_7863" s="T80">kü-ʔ</ta>
            <ta e="T83" id="Seg_7864" s="T82">măna</ta>
            <ta e="T84" id="Seg_7865" s="T83">ĭmbi</ta>
            <ta e="T86" id="Seg_7866" s="T85">ĭmbi</ta>
            <ta e="T87" id="Seg_7867" s="T86">măn</ta>
            <ta e="T88" id="Seg_7868" s="T87">tăn</ta>
            <ta e="T89" id="Seg_7869" s="T88">toʔ-gəndə</ta>
            <ta e="T90" id="Seg_7870" s="T89">amno-laʔbə-lV-m</ta>
            <ta e="T91" id="Seg_7871" s="T90">štoli</ta>
            <ta e="T93" id="Seg_7872" s="T92">măn</ta>
            <ta e="T1185" id="Seg_7873" s="T93">vedʼ</ta>
            <ta e="T94" id="Seg_7874" s="T1185">il</ta>
            <ta e="T95" id="Seg_7875" s="T94">amno-laʔbə-jəʔ</ta>
            <ta e="T97" id="Seg_7876" s="T96">dĭn</ta>
            <ta e="T98" id="Seg_7877" s="T97">edəʔ-laʔbə-jəʔ</ta>
            <ta e="T99" id="Seg_7878" s="T98">măna</ta>
            <ta e="T101" id="Seg_7879" s="T100">tʼăbaktər-zittə</ta>
            <ta e="T102" id="Seg_7880" s="T101">nadə</ta>
            <ta e="T104" id="Seg_7881" s="T103">a</ta>
            <ta e="T105" id="Seg_7882" s="T104">tăn</ta>
            <ta e="T106" id="Seg_7883" s="T105">toʔ-gəndə</ta>
            <ta e="T107" id="Seg_7884" s="T106">amno-ʔ</ta>
            <ta e="T111" id="Seg_7885" s="T110">mej-m</ta>
            <ta e="T113" id="Seg_7886" s="T112">šide</ta>
            <ta e="T114" id="Seg_7887" s="T113">tʼala</ta>
            <ta e="T115" id="Seg_7888" s="T114">i-bi</ta>
            <ta e="T117" id="Seg_7889" s="T116">a</ta>
            <ta e="T118" id="Seg_7890" s="T117">tăn</ta>
            <ta e="T119" id="Seg_7891" s="T118">toʔ-gəndə</ta>
            <ta e="T120" id="Seg_7892" s="T119">amno-ʔ</ta>
            <ta e="T122" id="Seg_7893" s="T121">da</ta>
            <ta e="T123" id="Seg_7894" s="T122">il</ta>
            <ta e="T124" id="Seg_7895" s="T123">mĭn-gA-jəʔ</ta>
            <ta e="T126" id="Seg_7896" s="T125">a</ta>
            <ta e="T127" id="Seg_7897" s="T126">dĭ</ta>
            <ta e="T128" id="Seg_7898" s="T127">măn-ntə</ta>
            <ta e="T129" id="Seg_7899" s="T128">a</ta>
            <ta e="T130" id="Seg_7900" s="T129">kudaj</ta>
            <ta e="T131" id="Seg_7901" s="T130">dĭ-zAŋ-ziʔ</ta>
            <ta e="T133" id="Seg_7902" s="T132">a</ta>
            <ta e="T134" id="Seg_7903" s="T133">tăn-ziʔ</ta>
            <ta e="T135" id="Seg_7904" s="T134">pušaj</ta>
            <ta e="T136" id="Seg_7905" s="T135">kudaj</ta>
            <ta e="T138" id="Seg_7906" s="T137">kü-ʔ</ta>
            <ta e="T139" id="Seg_7907" s="T138">kü-ʔ</ta>
            <ta e="T143" id="Seg_7908" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_7909" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_7910" s="T144">ma-laʔbə</ta>
            <ta e="T146" id="Seg_7911" s="T145">a</ta>
            <ta e="T147" id="Seg_7912" s="T146">măn</ta>
            <ta e="T148" id="Seg_7913" s="T147">maʔ-gənʼi</ta>
            <ta e="T1179" id="Seg_7914" s="T148">kan-lAʔ</ta>
            <ta e="T149" id="Seg_7915" s="T1179">tʼür-bi-m</ta>
            <ta e="T153" id="Seg_7916" s="T152">măn</ta>
            <ta e="T1183" id="Seg_7917" s="T153">Kazan</ta>
            <ta e="T154" id="Seg_7918" s="T1183">tura-Tə</ta>
            <ta e="T155" id="Seg_7919" s="T154">kan-bi-m</ta>
            <ta e="T157" id="Seg_7920" s="T156">a</ta>
            <ta e="T158" id="Seg_7921" s="T157">dĭ</ta>
            <ta e="T159" id="Seg_7922" s="T158">nüke</ta>
            <ta e="T160" id="Seg_7923" s="T159">kan-bi</ta>
            <ta e="T161" id="Seg_7924" s="T160">dĭ-n</ta>
            <ta e="T162" id="Seg_7925" s="T161">toʔ-gəndə</ta>
            <ta e="T163" id="Seg_7926" s="T162">dĭn</ta>
            <ta e="T164" id="Seg_7927" s="T163">ne</ta>
            <ta e="T165" id="Seg_7928" s="T164">amno-laʔbə</ta>
            <ta e="T167" id="Seg_7929" s="T166">šo-bi</ta>
            <ta e="T169" id="Seg_7930" s="T168">da</ta>
            <ta e="T1180" id="Seg_7931" s="T169">kan-lAʔ</ta>
            <ta e="T170" id="Seg_7932" s="T1180">tʼür-bi</ta>
            <ta e="T172" id="Seg_7933" s="T171">măn</ta>
            <ta e="T173" id="Seg_7934" s="T172">kü-lV-m</ta>
            <ta e="T174" id="Seg_7935" s="T173">a</ta>
            <ta e="T175" id="Seg_7936" s="T174">šində</ta>
            <ta e="T176" id="Seg_7937" s="T175">măna</ta>
            <ta e="T178" id="Seg_7938" s="T177">băzəj-də-lV-j</ta>
            <ta e="T180" id="Seg_7939" s="T179">dĭ-zAŋ</ta>
            <ta e="T181" id="Seg_7940" s="T180">bar</ta>
            <ta e="T182" id="Seg_7941" s="T181">kakənar-bi-jəʔ</ta>
            <ta e="T183" id="Seg_7942" s="T182">tăŋ</ta>
            <ta e="T185" id="Seg_7943" s="T184">dĭgəttə</ta>
            <ta e="T186" id="Seg_7944" s="T185">măn</ta>
            <ta e="T187" id="Seg_7945" s="T186">šo-bi-m</ta>
            <ta e="T188" id="Seg_7946" s="T187">kan-bi-m</ta>
            <ta e="T189" id="Seg_7947" s="T188">dĭ-Tə</ta>
            <ta e="T190" id="Seg_7948" s="T189">tăn</ta>
            <ta e="T1182" id="Seg_7949" s="T190">kan-lAʔ</ta>
            <ta e="T191" id="Seg_7950" s="T1182">tʼür-bi-m</ta>
            <ta e="T192" id="Seg_7951" s="T191">a</ta>
            <ta e="T193" id="Seg_7952" s="T192">măn</ta>
            <ta e="T194" id="Seg_7953" s="T193">kü-l-laːm-bi-m</ta>
            <ta e="T195" id="Seg_7954" s="T194">i</ta>
            <ta e="T196" id="Seg_7955" s="T195">kănna-m-bi-m</ta>
            <ta e="T198" id="Seg_7956" s="T197">a</ta>
            <ta e="T199" id="Seg_7957" s="T198">măn</ta>
            <ta e="T200" id="Seg_7958" s="T199">măn-bi-m</ta>
            <ta e="T201" id="Seg_7959" s="T200">nu</ta>
            <ta e="T202" id="Seg_7960" s="T201">kănna-ʔ</ta>
            <ta e="T204" id="Seg_7961" s="T203">ĭmbi</ta>
            <ta e="T205" id="Seg_7962" s="T204">i</ta>
            <ta e="T207" id="Seg_7963" s="T206">i</ta>
            <ta e="T208" id="Seg_7964" s="T207">kü-ʔ</ta>
            <ta e="T210" id="Seg_7965" s="T209">bɨ</ta>
            <ta e="T211" id="Seg_7966" s="T210">măn</ta>
            <ta e="T212" id="Seg_7967" s="T211">bü</ta>
            <ta e="T213" id="Seg_7968" s="T212">ejüm-lə-m</ta>
            <ta e="T215" id="Seg_7969" s="T214">i</ta>
            <ta e="T216" id="Seg_7970" s="T215">tănan</ta>
            <ta e="T218" id="Seg_7971" s="T217">kămnə-lV-l</ta>
            <ta e="T220" id="Seg_7972" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_7973" s="T220">bazə-lV-m</ta>
            <ta e="T222" id="Seg_7974" s="T221">i</ta>
            <ta e="T223" id="Seg_7975" s="T222">tʼo-Tə</ta>
            <ta e="T226" id="Seg_7976" s="T225">tʼo-Tə</ta>
            <ta e="T227" id="Seg_7977" s="T226">hen-lV-bi-m</ta>
            <ta e="T241" id="Seg_7978" s="T240">dĭ</ta>
            <ta e="T242" id="Seg_7979" s="T241">nüke</ta>
            <ta e="T243" id="Seg_7980" s="T242">bar</ta>
            <ta e="T244" id="Seg_7981" s="T243">măna</ta>
            <ta e="T245" id="Seg_7982" s="T244">nendə-ʔ</ta>
            <ta e="T246" id="Seg_7983" s="T245">multʼa</ta>
            <ta e="T247" id="Seg_7984" s="T246">bazə-ʔ</ta>
            <ta e="T248" id="Seg_7985" s="T247">măna</ta>
            <ta e="T250" id="Seg_7986" s="T249">kü-laːm-lV-m</ta>
            <ta e="T251" id="Seg_7987" s="T250">tak</ta>
            <ta e="T252" id="Seg_7988" s="T251">dĭgəttə</ta>
            <ta e="T254" id="Seg_7989" s="T252">kadul-də=da</ta>
            <ta e="T256" id="Seg_7990" s="T254">uda-zAŋ=da</ta>
            <ta e="T257" id="Seg_7991" s="T256">bazə-lV-l</ta>
            <ta e="T259" id="Seg_7992" s="T258">măn</ta>
            <ta e="T260" id="Seg_7993" s="T259">dĭgəttə</ta>
            <ta e="T261" id="Seg_7994" s="T260">nendə-bi-m</ta>
            <ta e="T263" id="Seg_7995" s="T262">multʼa</ta>
            <ta e="T265" id="Seg_7996" s="T264">dĭgəttə</ta>
            <ta e="T266" id="Seg_7997" s="T265">sanka-jəʔ-ziʔ</ta>
            <ta e="T267" id="Seg_7998" s="T266">hen-bi-m</ta>
            <ta e="T268" id="Seg_7999" s="T267">i</ta>
            <ta e="T269" id="Seg_8000" s="T268">u</ta>
            <ta e="T270" id="Seg_8001" s="T269">kun-laːm-bi-m</ta>
            <ta e="T271" id="Seg_8002" s="T270">bazə-bi-m</ta>
            <ta e="T272" id="Seg_8003" s="T271">dĭ-m</ta>
            <ta e="T273" id="Seg_8004" s="T272">da</ta>
            <ta e="T274" id="Seg_8005" s="T273">bazoʔ</ta>
            <ta e="T275" id="Seg_8006" s="T274">maʔ-Tə</ta>
            <ta e="T276" id="Seg_8007" s="T275">det-bi-m</ta>
            <ta e="T277" id="Seg_8008" s="T276">sanka-jəʔ-ziʔ</ta>
            <ta e="T279" id="Seg_8009" s="T278">hen-bi-m</ta>
            <ta e="T280" id="Seg_8010" s="T279">krăvatʼ-Tə</ta>
            <ta e="T281" id="Seg_8011" s="T280">măn-ntə-m</ta>
            <ta e="T282" id="Seg_8012" s="T281">nu</ta>
            <ta e="T283" id="Seg_8013" s="T282">tüj</ta>
            <ta e="T284" id="Seg_8014" s="T283">kü-ʔ</ta>
            <ta e="T286" id="Seg_8015" s="T285">kadul-də</ta>
            <ta e="T287" id="Seg_8016" s="T286">da</ta>
            <ta e="T288" id="Seg_8017" s="T287">uda-zAŋ-ziʔ</ta>
            <ta e="T289" id="Seg_8018" s="T288">bazə-lV-m</ta>
            <ta e="T290" id="Seg_8019" s="T289">i</ta>
            <ta e="T291" id="Seg_8020" s="T290">jakšə</ta>
            <ta e="T292" id="Seg_8021" s="T291">mo-lV-j</ta>
            <ta e="T294" id="Seg_8022" s="T293">nu</ta>
            <ta e="T295" id="Seg_8023" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_8024" s="T295">multʼa-Kən</ta>
            <ta e="T297" id="Seg_8025" s="T296">bazə-bi-m</ta>
            <ta e="T298" id="Seg_8026" s="T297">ugaːndə</ta>
            <ta e="T300" id="Seg_8027" s="T299">balgaš</ta>
            <ta e="T301" id="Seg_8028" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_8029" s="T301">i-bi</ta>
            <ta e="T303" id="Seg_8030" s="T302">ulu-gəndə</ta>
            <ta e="T305" id="Seg_8031" s="T304">măn</ta>
            <ta e="T306" id="Seg_8032" s="T305">măn-liA-m</ta>
            <ta e="T307" id="Seg_8033" s="T306">ĭššo</ta>
            <ta e="T308" id="Seg_8034" s="T307">hen-ə-ʔ</ta>
            <ta e="T309" id="Seg_8035" s="T308">bü</ta>
            <ta e="T311" id="Seg_8036" s="T310">dö-m</ta>
            <ta e="T313" id="Seg_8037" s="T312">kămnə-t</ta>
            <ta e="T317" id="Seg_8038" s="T316">onʼiʔ</ta>
            <ta e="T319" id="Seg_8039" s="T318">ine</ta>
            <ta e="T320" id="Seg_8040" s="T319">mĭ-bi-m</ta>
            <ta e="T321" id="Seg_8041" s="T320">tibi-n</ta>
            <ta e="T322" id="Seg_8042" s="T321">kaga-Tə</ta>
            <ta e="T324" id="Seg_8043" s="T323">šide</ta>
            <ta e="T325" id="Seg_8044" s="T324">pʼe</ta>
            <ta e="T326" id="Seg_8045" s="T325">i-bi</ta>
            <ta e="T328" id="Seg_8046" s="T327">bazoʔ</ta>
            <ta e="T329" id="Seg_8047" s="T328">ine</ta>
            <ta e="T330" id="Seg_8048" s="T329">mĭ-bi-m</ta>
            <ta e="T331" id="Seg_8049" s="T330">kuza-Tə</ta>
            <ta e="T333" id="Seg_8050" s="T332">tibi</ta>
            <ta e="T334" id="Seg_8051" s="T333">i-bi</ta>
            <ta e="T336" id="Seg_8052" s="T335">naga</ta>
            <ta e="T337" id="Seg_8053" s="T336">ine</ta>
            <ta e="T338" id="Seg_8054" s="T337">tože</ta>
            <ta e="T339" id="Seg_8055" s="T338">šide</ta>
            <ta e="T340" id="Seg_8056" s="T339">pʼe</ta>
            <ta e="T341" id="Seg_8057" s="T340">i-bi</ta>
            <ta e="T343" id="Seg_8058" s="T342">mĭ-bi-m</ta>
            <ta e="T344" id="Seg_8059" s="T343">ine</ta>
            <ta e="T345" id="Seg_8060" s="T344">dĭgəttə</ta>
            <ta e="T346" id="Seg_8061" s="T345">dĭ</ta>
            <ta e="T347" id="Seg_8062" s="T346">sʼestra-gəndə</ta>
            <ta e="T348" id="Seg_8063" s="T347">ular</ta>
            <ta e="T349" id="Seg_8064" s="T348">mĭ-bi-m</ta>
            <ta e="T351" id="Seg_8065" s="T350">parga</ta>
            <ta e="T352" id="Seg_8066" s="T351">mĭ-bi-m</ta>
            <ta e="T354" id="Seg_8067" s="T353">dĭgəttə</ta>
            <ta e="T355" id="Seg_8068" s="T354">plemʼannica-n</ta>
            <ta e="T356" id="Seg_8069" s="T355">koʔbdo-nə</ta>
            <ta e="T357" id="Seg_8070" s="T356">mĭ-bi-m</ta>
            <ta e="T358" id="Seg_8071" s="T357">buga</ta>
            <ta e="T360" id="Seg_8072" s="T359">pʼel</ta>
            <ta e="T361" id="Seg_8073" s="T360">pʼel</ta>
            <ta e="T362" id="Seg_8074" s="T361">kö</ta>
            <ta e="T364" id="Seg_8075" s="T363">dĭgəttə</ta>
            <ta e="T366" id="Seg_8076" s="T365">sʼestra-Tə</ta>
            <ta e="T367" id="Seg_8077" s="T366">nʼi-t</ta>
            <ta e="T368" id="Seg_8078" s="T367">mĭ-bi-m</ta>
            <ta e="T369" id="Seg_8079" s="T368">tüžöj</ta>
            <ta e="T370" id="Seg_8080" s="T369">i</ta>
            <ta e="T371" id="Seg_8081" s="T370">ular</ta>
            <ta e="T372" id="Seg_8082" s="T371">mĭ-bi-m</ta>
            <ta e="T376" id="Seg_8083" s="T375">dĭgəttə</ta>
            <ta e="T377" id="Seg_8084" s="T376">onʼiʔ</ta>
            <ta e="T378" id="Seg_8085" s="T377">ne-Tə</ta>
            <ta e="T379" id="Seg_8086" s="T378">bazoʔ</ta>
            <ta e="T381" id="Seg_8087" s="T380">büzəj</ta>
            <ta e="T382" id="Seg_8088" s="T381">mĭ-bi-m</ta>
            <ta e="T384" id="Seg_8089" s="T383">il</ta>
            <ta e="T385" id="Seg_8090" s="T384">šo-lV-jəʔ</ta>
            <ta e="T386" id="Seg_8091" s="T385">det-ʔ</ta>
            <ta e="T387" id="Seg_8092" s="T386">aktʼa</ta>
            <ta e="T389" id="Seg_8093" s="T388">miʔ</ta>
            <ta e="T390" id="Seg_8094" s="T389">tănan</ta>
            <ta e="T391" id="Seg_8095" s="T390">mĭ-lV-bAʔ</ta>
            <ta e="T393" id="Seg_8096" s="T392">a</ta>
            <ta e="T394" id="Seg_8097" s="T393">bos</ta>
            <ta e="T395" id="Seg_8098" s="T394">bostə-zAŋ-də</ta>
            <ta e="T396" id="Seg_8099" s="T395">ej</ta>
            <ta e="T397" id="Seg_8100" s="T396">mĭ-lV-jəʔ</ta>
            <ta e="T398" id="Seg_8101" s="T397">i</ta>
            <ta e="T399" id="Seg_8102" s="T398">dĭgəttə</ta>
            <ta e="T401" id="Seg_8103" s="T400">aktʼa</ta>
            <ta e="T407" id="Seg_8104" s="T406">măn</ta>
            <ta e="T408" id="Seg_8105" s="T407">tüj</ta>
            <ta e="T409" id="Seg_8106" s="T408">e-m</ta>
            <ta e="T410" id="Seg_8107" s="T409">ej</ta>
            <ta e="T412" id="Seg_8108" s="T411">tenö-lV-m</ta>
            <ta e="T413" id="Seg_8109" s="T412">ato</ta>
            <ta e="T414" id="Seg_8110" s="T413">ine-n</ta>
            <ta e="T415" id="Seg_8111" s="T414">ulu-t</ta>
            <ta e="T416" id="Seg_8112" s="T415">nadə</ta>
            <ta e="T417" id="Seg_8113" s="T416">urgo</ta>
            <ta e="T421" id="Seg_8114" s="T420">šiʔ</ta>
            <ta e="T422" id="Seg_8115" s="T421">tenö-laʔbə</ta>
            <ta e="T428" id="Seg_8116" s="T427">tüjö</ta>
            <ta e="T429" id="Seg_8117" s="T428">kan-lV-m</ta>
            <ta e="T430" id="Seg_8118" s="T429">tüžöj-jəʔ</ta>
            <ta e="T432" id="Seg_8119" s="T431">sürer-lV-m</ta>
            <ta e="T433" id="Seg_8120" s="T432">i</ta>
            <ta e="T434" id="Seg_8121" s="T433">nuʔmə-lV-m</ta>
            <ta e="T436" id="Seg_8122" s="T435">kan-bi-m</ta>
            <ta e="T437" id="Seg_8123" s="T436">a</ta>
            <ta e="T438" id="Seg_8124" s="T437">dĭ-zAŋ</ta>
            <ta e="T439" id="Seg_8125" s="T438">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T440" id="Seg_8126" s="T439">gibər=də</ta>
            <ta e="T444" id="Seg_8127" s="T443">traktăr</ta>
            <ta e="T445" id="Seg_8128" s="T444">bar</ta>
            <ta e="T446" id="Seg_8129" s="T445">pa-jəʔ</ta>
            <ta e="T447" id="Seg_8130" s="T446">kun-nandə-gA</ta>
            <ta e="T449" id="Seg_8131" s="T448">ej</ta>
            <ta e="T450" id="Seg_8132" s="T449">tĭmne-m</ta>
            <ta e="T451" id="Seg_8133" s="T450">kumən</ta>
            <ta e="T452" id="Seg_8134" s="T451">kun-nandə-gA</ta>
            <ta e="T456" id="Seg_8135" s="T455">ugaːndə</ta>
            <ta e="T457" id="Seg_8136" s="T456">iʔgö</ta>
            <ta e="T458" id="Seg_8137" s="T457">pa-jəʔ</ta>
            <ta e="T460" id="Seg_8138" s="T459">da</ta>
            <ta e="T461" id="Seg_8139" s="T460">i</ta>
            <ta e="T462" id="Seg_8140" s="T461">nʼešpək</ta>
            <ta e="T463" id="Seg_8141" s="T462">nʼešpək</ta>
            <ta e="T464" id="Seg_8142" s="T463">ugaːndə</ta>
            <ta e="T468" id="Seg_8143" s="T467">iʔgö</ta>
            <ta e="T470" id="Seg_8144" s="T469">togonər-zittə</ta>
            <ta e="T471" id="Seg_8145" s="T470">ku-bi-m</ta>
            <ta e="T472" id="Seg_8146" s="T471">a</ta>
            <ta e="T473" id="Seg_8147" s="T472">amka</ta>
            <ta e="T474" id="Seg_8148" s="T473">ku-laʔpi-m</ta>
            <ta e="T478" id="Seg_8149" s="T477">koŋ</ta>
            <ta e="T479" id="Seg_8150" s="T478">ugaːndə</ta>
            <ta e="T480" id="Seg_8151" s="T479">kuvas</ta>
            <ta e="T482" id="Seg_8152" s="T481">bar</ta>
            <ta e="T483" id="Seg_8153" s="T482">ĭmbi</ta>
            <ta e="T484" id="Seg_8154" s="T483">măna</ta>
            <ta e="T485" id="Seg_8155" s="T484">mĭ-m-bi</ta>
            <ta e="T486" id="Seg_8156" s="T485">bar</ta>
            <ta e="T487" id="Seg_8157" s="T486">ĭmbi</ta>
            <ta e="T491" id="Seg_8158" s="T490">bar</ta>
            <ta e="T492" id="Seg_8159" s="T491">măna</ta>
            <ta e="T494" id="Seg_8160" s="T493">pʼer-bi</ta>
            <ta e="T501" id="Seg_8161" s="T500">mo-lV-j</ta>
            <ta e="T502" id="Seg_8162" s="T501">tʼala</ta>
            <ta e="T503" id="Seg_8163" s="T502">mo-lV-j</ta>
            <ta e="T504" id="Seg_8164" s="T503">amor-zittə</ta>
            <ta e="T508" id="Seg_8165" s="T507">teinen</ta>
            <ta e="T509" id="Seg_8166" s="T508">subbota</ta>
            <ta e="T510" id="Seg_8167" s="T509">nadə</ta>
            <ta e="T511" id="Seg_8168" s="T510">multʼa</ta>
            <ta e="T512" id="Seg_8169" s="T511">nendə-zittə</ta>
            <ta e="T513" id="Seg_8170" s="T512">bü</ta>
            <ta e="T514" id="Seg_8171" s="T513">tažor-zittə</ta>
            <ta e="T516" id="Seg_8172" s="T515">dĭgəttə</ta>
            <ta e="T517" id="Seg_8173" s="T516">nendə-lV-m</ta>
            <ta e="T519" id="Seg_8174" s="T518">bazəj-ntə-zittə</ta>
            <ta e="T520" id="Seg_8175" s="T519">nadə</ta>
            <ta e="T522" id="Seg_8176" s="T521">ato</ta>
            <ta e="T523" id="Seg_8177" s="T522">celɨj</ta>
            <ta e="T524" id="Seg_8178" s="T523">nedʼelʼa</ta>
            <ta e="T525" id="Seg_8179" s="T524">ej</ta>
            <ta e="T526" id="Seg_8180" s="T525">băzəj-ntə-bi-m</ta>
            <ta e="T530" id="Seg_8181" s="T529">maʔ-ə-ŋ-Tə</ta>
            <ta e="T531" id="Seg_8182" s="T530">sazən</ta>
            <ta e="T532" id="Seg_8183" s="T531">edəʔ-liA-m</ta>
            <ta e="T533" id="Seg_8184" s="T532">edəʔ-liA-m</ta>
            <ta e="T535" id="Seg_8185" s="T534">üge</ta>
            <ta e="T536" id="Seg_8186" s="T535">nadə</ta>
            <ta e="T537" id="Seg_8187" s="T536">naga</ta>
            <ta e="T538" id="Seg_8188" s="T537">ne-m</ta>
            <ta e="T539" id="Seg_8189" s="T538">ej</ta>
            <ta e="T540" id="Seg_8190" s="T539">pʼaŋdə-liA</ta>
            <ta e="T542" id="Seg_8191" s="T541">i</ta>
            <ta e="T543" id="Seg_8192" s="T542">sazən</ta>
            <ta e="T544" id="Seg_8193" s="T543">ej</ta>
            <ta e="T545" id="Seg_8194" s="T544">šo-liA</ta>
            <ta e="T548" id="Seg_8195" s="T547">teinen</ta>
            <ta e="T549" id="Seg_8196" s="T548">nüdʼi-n</ta>
            <ta e="T550" id="Seg_8197" s="T549">dʼodə-nʼi</ta>
            <ta e="T551" id="Seg_8198" s="T550">ku-bi-m</ta>
            <ta e="T553" id="Seg_8199" s="T552">tʼabəro-laʔpi-m</ta>
            <ta e="T554" id="Seg_8200" s="T553">teinen</ta>
            <ta e="T555" id="Seg_8201" s="T554">sazən</ta>
            <ta e="T556" id="Seg_8202" s="T555">šo-lV-j</ta>
            <ta e="T560" id="Seg_8203" s="T559">măn</ta>
            <ta e="T561" id="Seg_8204" s="T560">mĭn-bi-m</ta>
            <ta e="T562" id="Seg_8205" s="T561">Krasnojarskə-Tə</ta>
            <ta e="T1186" id="Seg_8206" s="T562">munəj-jəʔ</ta>
            <ta e="T564" id="Seg_8207" s="T563">kun-bi-m</ta>
            <ta e="T567" id="Seg_8208" s="T566">sto</ta>
            <ta e="T568" id="Seg_8209" s="T567">munəj</ta>
            <ta e="T570" id="Seg_8210" s="T569">dĭgəttə</ta>
            <ta e="T571" id="Seg_8211" s="T570">kaga-m</ta>
            <ta e="T572" id="Seg_8212" s="T571">măn-ziʔ</ta>
            <ta e="T573" id="Seg_8213" s="T572">kan-bi</ta>
            <ta e="T575" id="Seg_8214" s="T574">dĭ</ta>
            <ta e="T576" id="Seg_8215" s="T575">măna</ta>
            <ta e="T577" id="Seg_8216" s="T576">kun-bi</ta>
            <ta e="T578" id="Seg_8217" s="T577">amno-bi</ta>
            <ta e="T579" id="Seg_8218" s="T578">amno-bi</ta>
            <ta e="T580" id="Seg_8219" s="T579">mašina-jəʔ-ziʔ</ta>
            <ta e="T582" id="Seg_8220" s="T581">măn</ta>
            <ta e="T584" id="Seg_8221" s="T583">ej</ta>
            <ta e="T585" id="Seg_8222" s="T584">mo-liA-m</ta>
            <ta e="T586" id="Seg_8223" s="T585">ku-zittə</ta>
            <ta e="T587" id="Seg_8224" s="T586">dĭgəttə</ta>
            <ta e="T588" id="Seg_8225" s="T587">ku-bi-m</ta>
            <ta e="T589" id="Seg_8226" s="T588">plemʼannica-m</ta>
            <ta e="T591" id="Seg_8227" s="T590">a</ta>
            <ta e="T592" id="Seg_8228" s="T591">dĭbər</ta>
            <ta e="T593" id="Seg_8229" s="T592">kan-ntə-bi-m</ta>
            <ta e="T595" id="Seg_8230" s="T594">döʔə</ta>
            <ta e="T596" id="Seg_8231" s="T595">mašina-ziʔ</ta>
            <ta e="T598" id="Seg_8232" s="T597">a</ta>
            <ta e="T599" id="Seg_8233" s="T598">dĭn</ta>
            <ta e="T600" id="Seg_8234" s="T599">aftobus-ziʔ</ta>
            <ta e="T602" id="Seg_8235" s="T601">dĭgəttə</ta>
            <ta e="T603" id="Seg_8236" s="T602">baza-j</ta>
            <ta e="T604" id="Seg_8237" s="T603">aʔtʼi</ta>
            <ta e="T606" id="Seg_8238" s="T605">aktʼa</ta>
            <ta e="T607" id="Seg_8239" s="T606">aʔtʼi</ta>
            <ta e="T608" id="Seg_8240" s="T607">dĭgəttə</ta>
            <ta e="T609" id="Seg_8241" s="T608">dĭbər</ta>
            <ta e="T610" id="Seg_8242" s="T609">šo-bi-m</ta>
            <ta e="T614" id="Seg_8243" s="T613">dĭgəttə</ta>
            <ta e="T615" id="Seg_8244" s="T614">mĭn-bi-m</ta>
            <ta e="T616" id="Seg_8245" s="T615">kudaj-Tə</ta>
            <ta e="T618" id="Seg_8246" s="T617">num-ə-n</ta>
            <ta e="T619" id="Seg_8247" s="T618">üzə-zittə</ta>
            <ta e="T621" id="Seg_8248" s="T620">dĭgəttə</ta>
            <ta e="T622" id="Seg_8249" s="T621">mĭn-bi-bAʔ</ta>
            <ta e="T623" id="Seg_8250" s="T622">plemʼannica-ziʔ</ta>
            <ta e="T624" id="Seg_8251" s="T623">žibiaʔi-Tə</ta>
            <ta e="T626" id="Seg_8252" s="T625">dĭn</ta>
            <ta e="T628" id="Seg_8253" s="T627">amnə-bi-bAʔ</ta>
            <ta e="T629" id="Seg_8254" s="T628">da</ta>
            <ta e="T630" id="Seg_8255" s="T629">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T632" id="Seg_8256" s="T631">a</ta>
            <ta e="T633" id="Seg_8257" s="T632">dĭgəttə</ta>
            <ta e="T634" id="Seg_8258" s="T633">maʔ-gəndə</ta>
            <ta e="T635" id="Seg_8259" s="T634">šo-bi-m</ta>
            <ta e="T639" id="Seg_8260" s="T638">plemʼannica</ta>
            <ta e="T640" id="Seg_8261" s="T639">măna</ta>
            <ta e="T641" id="Seg_8262" s="T640">pomidor</ta>
            <ta e="T642" id="Seg_8263" s="T641">iʔgö</ta>
            <ta e="T643" id="Seg_8264" s="T642">mĭ-bi</ta>
            <ta e="T644" id="Seg_8265" s="T643">măn</ta>
            <ta e="T1184" id="Seg_8266" s="T645">Kazan</ta>
            <ta e="T646" id="Seg_8267" s="T1184">tura-Kən</ta>
            <ta e="T647" id="Seg_8268" s="T646">šide</ta>
            <ta e="T648" id="Seg_8269" s="T647">tʼala</ta>
            <ta e="T649" id="Seg_8270" s="T648">amnə-bi-m</ta>
            <ta e="T653" id="Seg_8271" s="T652">tüžöj-jəʔ</ta>
            <ta e="T654" id="Seg_8272" s="T653">šo-bi-jəʔ</ta>
            <ta e="T655" id="Seg_8273" s="T654">maʔ-gəndə</ta>
            <ta e="T656" id="Seg_8274" s="T655">nadə</ta>
            <ta e="T657" id="Seg_8275" s="T656">dĭ-zem</ta>
            <ta e="T658" id="Seg_8276" s="T657">šeden-də</ta>
            <ta e="T659" id="Seg_8277" s="T658">sürer-zittə</ta>
            <ta e="T660" id="Seg_8278" s="T659">i</ta>
            <ta e="T662" id="Seg_8279" s="T661">%%-zittə</ta>
            <ta e="T666" id="Seg_8280" s="T665">tenö-bi-m</ta>
            <ta e="T667" id="Seg_8281" s="T666">tenö-bi-m</ta>
            <ta e="T668" id="Seg_8282" s="T667">ĭmbi=nʼibudʼ</ta>
            <ta e="T669" id="Seg_8283" s="T668">tenö-luʔbdə-bi-m</ta>
            <ta e="T673" id="Seg_8284" s="T672">koʔbdo-Tə</ta>
            <ta e="T674" id="Seg_8285" s="T673">mĭn-bi-m</ta>
            <ta e="T675" id="Seg_8286" s="T674">sumka</ta>
            <ta e="T676" id="Seg_8287" s="T675">dĭ</ta>
            <ta e="T677" id="Seg_8288" s="T676">bar</ta>
            <ta e="T678" id="Seg_8289" s="T677">sajnʼi-luʔbdə</ta>
            <ta e="T680" id="Seg_8290" s="T679">tüj</ta>
            <ta e="T681" id="Seg_8291" s="T680">amno-laʔbə-m</ta>
            <ta e="T682" id="Seg_8292" s="T681">da</ta>
            <ta e="T684" id="Seg_8293" s="T683">šödör-laʔbə-m</ta>
            <ta e="T686" id="Seg_8294" s="T685">tănan</ta>
            <ta e="T687" id="Seg_8295" s="T686">šonə-bi-m</ta>
            <ta e="T688" id="Seg_8296" s="T687">šonə-bi-m</ta>
            <ta e="T689" id="Seg_8297" s="T688">ap</ta>
            <ta e="T690" id="Seg_8298" s="T689">ej</ta>
            <ta e="T691" id="Seg_8299" s="T690">saʔmə-luʔbdə-bi-m</ta>
            <ta e="T696" id="Seg_8300" s="T695">kozan</ta>
            <ta e="T698" id="Seg_8301" s="T697">kozan</ta>
            <ta e="T699" id="Seg_8302" s="T698">šonə-gA</ta>
            <ta e="T700" id="Seg_8303" s="T699">šonə-gA</ta>
            <ta e="T701" id="Seg_8304" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_8305" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_8306" s="T702">dĭ-Tə</ta>
            <ta e="T705" id="Seg_8307" s="T704">püje</ta>
            <ta e="T708" id="Seg_8308" s="T707">băt-luʔbə</ta>
            <ta e="T709" id="Seg_8309" s="T708">kem-də</ta>
            <ta e="T710" id="Seg_8310" s="T709">bar</ta>
            <ta e="T711" id="Seg_8311" s="T710">mʼaŋ-laʔbə</ta>
            <ta e="T713" id="Seg_8312" s="T712">dĭ</ta>
            <ta e="T714" id="Seg_8313" s="T713">măn-ntə</ta>
            <ta e="T715" id="Seg_8314" s="T714">šü</ta>
            <ta e="T717" id="Seg_8315" s="T716">nendə-t</ta>
            <ta e="T718" id="Seg_8316" s="T717">dĭ</ta>
            <ta e="T719" id="Seg_8317" s="T718">noʔ</ta>
            <ta e="T723" id="Seg_8318" s="T722">măn</ta>
            <ta e="T724" id="Seg_8319" s="T723">iʔgö</ta>
            <ta e="T725" id="Seg_8320" s="T724">tʼo-m</ta>
            <ta e="T726" id="Seg_8321" s="T725">nendə-zittə</ta>
            <ta e="T727" id="Seg_8322" s="T726">dĭgəttə</ta>
            <ta e="T728" id="Seg_8323" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_8324" s="T728">bü-Tə</ta>
            <ta e="T730" id="Seg_8325" s="T729">kan-bi</ta>
            <ta e="T732" id="Seg_8326" s="T731">bü</ta>
            <ta e="T733" id="Seg_8327" s="T732">kan-ə-ʔ</ta>
            <ta e="T734" id="Seg_8328" s="T733">šü-m</ta>
            <ta e="T736" id="Seg_8329" s="T735">kămnə-ʔ</ta>
            <ta e="T740" id="Seg_8330" s="T739">măn</ta>
            <ta e="T741" id="Seg_8331" s="T740">iʔgö</ta>
            <ta e="T743" id="Seg_8332" s="T742">tʼo</ta>
            <ta e="T744" id="Seg_8333" s="T743">kămnə-na-zittə</ta>
            <ta e="T745" id="Seg_8334" s="T744">štobɨ</ta>
            <ta e="T746" id="Seg_8335" s="T745">noʔ</ta>
            <ta e="T747" id="Seg_8336" s="T746">özer-bi</ta>
            <ta e="T751" id="Seg_8337" s="T750">dĭgəttə</ta>
            <ta e="T752" id="Seg_8338" s="T751">dĭ</ta>
            <ta e="T753" id="Seg_8339" s="T752">kan-bi</ta>
            <ta e="T755" id="Seg_8340" s="T754">bulan</ta>
            <ta e="T756" id="Seg_8341" s="T755">bulan</ta>
            <ta e="T757" id="Seg_8342" s="T756">kan-ə-ʔ</ta>
            <ta e="T758" id="Seg_8343" s="T757">bar</ta>
            <ta e="T759" id="Seg_8344" s="T758">bü</ta>
            <ta e="T761" id="Seg_8345" s="T760">bĭs-ə-ʔ</ta>
            <ta e="T763" id="Seg_8346" s="T762">dĭgəttə</ta>
            <ta e="T764" id="Seg_8347" s="T763">bulan</ta>
            <ta e="T765" id="Seg_8348" s="T764">măn-bi</ta>
            <ta e="T766" id="Seg_8349" s="T765">măn</ta>
            <ta e="T767" id="Seg_8350" s="T766">üjü-gəndə</ta>
            <ta e="T768" id="Seg_8351" s="T767">iʔgö</ta>
            <ta e="T769" id="Seg_8352" s="T768">bü</ta>
            <ta e="T771" id="Seg_8353" s="T770">dĭgəttə</ta>
            <ta e="T772" id="Seg_8354" s="T771">dĭ</ta>
            <ta e="T774" id="Seg_8355" s="T773">kan-bi</ta>
            <ta e="T775" id="Seg_8356" s="T774">nüke-Tə</ta>
            <ta e="T776" id="Seg_8357" s="T775">nüke</ta>
            <ta e="T777" id="Seg_8358" s="T776">nüke</ta>
            <ta e="T778" id="Seg_8359" s="T777">kan-ə-ʔ</ta>
            <ta e="T779" id="Seg_8360" s="T778">žila-jəʔ</ta>
            <ta e="T780" id="Seg_8361" s="T779">üjü-ttə</ta>
            <ta e="T781" id="Seg_8362" s="T780">i-t</ta>
            <ta e="T783" id="Seg_8363" s="T782">nüke</ta>
            <ta e="T784" id="Seg_8364" s="T783">ej</ta>
            <ta e="T785" id="Seg_8365" s="T784">kan-bi</ta>
            <ta e="T787" id="Seg_8366" s="T786">sagər-jəʔ</ta>
            <ta e="T788" id="Seg_8367" s="T787">kabarləj</ta>
            <ta e="T789" id="Seg_8368" s="T788">măna</ta>
            <ta e="T793" id="Seg_8369" s="T792">dĭ</ta>
            <ta e="T794" id="Seg_8370" s="T793">kan-bi</ta>
            <ta e="T795" id="Seg_8371" s="T794">nüke-Tə</ta>
            <ta e="T796" id="Seg_8372" s="T795">nüke</ta>
            <ta e="T797" id="Seg_8373" s="T796">nüke</ta>
            <ta e="T798" id="Seg_8374" s="T797">i-t</ta>
            <ta e="T800" id="Seg_8375" s="T799">žila-jəʔ</ta>
            <ta e="T801" id="Seg_8376" s="T800">a</ta>
            <ta e="T802" id="Seg_8377" s="T801">dĭ</ta>
            <ta e="T803" id="Seg_8378" s="T802">măn-ntə</ta>
            <ta e="T804" id="Seg_8379" s="T803">măn</ta>
            <ta e="T805" id="Seg_8380" s="T804">iʔgö</ta>
            <ta e="T806" id="Seg_8381" s="T805">i-gA</ta>
            <ta e="T807" id="Seg_8382" s="T806">da</ta>
            <ta e="T808" id="Seg_8383" s="T807">sagər</ta>
            <ta e="T809" id="Seg_8384" s="T808">măna</ta>
            <ta e="T810" id="Seg_8385" s="T809">ej</ta>
            <ta e="T811" id="Seg_8386" s="T810">kereʔ</ta>
            <ta e="T815" id="Seg_8387" s="T814">dĭgəttə</ta>
            <ta e="T816" id="Seg_8388" s="T815">kan-bi</ta>
            <ta e="T817" id="Seg_8389" s="T816">tumo-jəʔ-Tə</ta>
            <ta e="T818" id="Seg_8390" s="T817">tumo</ta>
            <ta e="T819" id="Seg_8391" s="T818">tumo</ta>
            <ta e="T820" id="Seg_8392" s="T819">kan-ə-ʔ</ta>
            <ta e="T821" id="Seg_8393" s="T820">nüke-n</ta>
            <ta e="T822" id="Seg_8394" s="T821">žila-jəʔ</ta>
            <ta e="T823" id="Seg_8395" s="T822">amna-ʔ</ta>
            <ta e="T825" id="Seg_8396" s="T824">dʼok</ta>
            <ta e="T827" id="Seg_8397" s="T826">miʔ</ta>
            <ta e="T828" id="Seg_8398" s="T827">e-m</ta>
            <ta e="T829" id="Seg_8399" s="T828">kan-lAʔ</ta>
            <ta e="T830" id="Seg_8400" s="T829">ej</ta>
            <ta e="T831" id="Seg_8401" s="T830">kan-lV-m</ta>
            <ta e="T833" id="Seg_8402" s="T832">miʔnʼibeʔ</ta>
            <ta e="T834" id="Seg_8403" s="T833">kabarləj</ta>
            <ta e="T835" id="Seg_8404" s="T834">tʼo-Kən</ta>
            <ta e="T836" id="Seg_8405" s="T835">kornʼi-jəʔ</ta>
            <ta e="T837" id="Seg_8406" s="T836">am-zittə</ta>
            <ta e="T841" id="Seg_8407" s="T840">dĭgəttə</ta>
            <ta e="T842" id="Seg_8408" s="T841">kan-bi</ta>
            <ta e="T844" id="Seg_8409" s="T843">ešši-zAŋ-Tə</ta>
            <ta e="T845" id="Seg_8410" s="T844">ešši-zAŋ</ta>
            <ta e="T846" id="Seg_8411" s="T845">ešši-zAŋ</ta>
            <ta e="T847" id="Seg_8412" s="T846">kut-laʔbə-ləj</ta>
            <ta e="T848" id="Seg_8413" s="T847">tumo-jəʔ</ta>
            <ta e="T849" id="Seg_8414" s="T848">dĭ-zAŋ</ta>
            <ta e="T850" id="Seg_8415" s="T849">miʔ</ta>
            <ta e="T852" id="Seg_8416" s="T851">bos-m</ta>
            <ta e="T853" id="Seg_8417" s="T852">sʼar-laʔbə-jəʔ</ta>
            <ta e="T854" id="Seg_8418" s="T853">baːbka-jəʔ-ziʔ</ta>
            <ta e="T858" id="Seg_8419" s="T857">šiʔ</ta>
            <ta e="T859" id="Seg_8420" s="T858">tura-Kən</ta>
            <ta e="T860" id="Seg_8421" s="T859">jakšə</ta>
            <ta e="T861" id="Seg_8422" s="T860">amno-zittə</ta>
            <ta e="T863" id="Seg_8423" s="T862">tolʼko</ta>
            <ta e="T864" id="Seg_8424" s="T863">ipek</ta>
            <ta e="T865" id="Seg_8425" s="T864">gijen=də</ta>
            <ta e="T866" id="Seg_8426" s="T865">ej</ta>
            <ta e="T867" id="Seg_8427" s="T866">i-lV-l</ta>
            <ta e="T869" id="Seg_8428" s="T868">am-zittə</ta>
            <ta e="T870" id="Seg_8429" s="T869">naga</ta>
            <ta e="T871" id="Seg_8430" s="T870">ipek</ta>
            <ta e="T875" id="Seg_8431" s="T874">măgăzin-gəndə</ta>
            <ta e="T876" id="Seg_8432" s="T875">ĭmbi</ta>
            <ta e="T877" id="Seg_8433" s="T876">naga</ta>
            <ta e="T878" id="Seg_8434" s="T877">ĭmbi=də</ta>
            <ta e="T879" id="Seg_8435" s="T878">ej</ta>
            <ta e="T880" id="Seg_8436" s="T879">i-lV-l</ta>
            <ta e="T882" id="Seg_8437" s="T881">dʼok</ta>
            <ta e="T883" id="Seg_8438" s="T882">kan-ə-ʔ</ta>
            <ta e="T884" id="Seg_8439" s="T883">dĭn</ta>
            <ta e="T885" id="Seg_8440" s="T884">i-gA</ta>
            <ta e="T886" id="Seg_8441" s="T885">kălbasa</ta>
            <ta e="T887" id="Seg_8442" s="T886">e-ʔ</ta>
            <ta e="T888" id="Seg_8443" s="T887">da</ta>
            <ta e="T889" id="Seg_8444" s="T888">dĭgəttə</ta>
            <ta e="T890" id="Seg_8445" s="T889">amor-lV-l</ta>
            <ta e="T894" id="Seg_8446" s="T893">dĭ</ta>
            <ta e="T895" id="Seg_8447" s="T894">nüke</ta>
            <ta e="T896" id="Seg_8448" s="T895">ugaːndə</ta>
            <ta e="T897" id="Seg_8449" s="T896">jakšə</ta>
            <ta e="T898" id="Seg_8450" s="T897">šödör-laʔbə</ta>
            <ta e="T899" id="Seg_8451" s="T898">bar</ta>
            <ta e="T900" id="Seg_8452" s="T899">kuvas</ta>
            <ta e="T902" id="Seg_8453" s="T901">jakšə</ta>
            <ta e="T903" id="Seg_8454" s="T902">măndo-zittə</ta>
            <ta e="T907" id="Seg_8455" s="T906">dĭ</ta>
            <ta e="T908" id="Seg_8456" s="T907">nüke</ta>
            <ta e="T909" id="Seg_8457" s="T908">ugaːndə</ta>
            <ta e="T910" id="Seg_8458" s="T909">iʔgö</ta>
            <ta e="T912" id="Seg_8459" s="T911">togonər-liA</ta>
            <ta e="T913" id="Seg_8460" s="T912">bar</ta>
            <ta e="T914" id="Seg_8461" s="T913">ĭmbi</ta>
            <ta e="T915" id="Seg_8462" s="T914">a-zittə</ta>
            <ta e="T920" id="Seg_8463" s="T919">bar</ta>
            <ta e="T921" id="Seg_8464" s="T920">ĭmbi</ta>
            <ta e="T922" id="Seg_8465" s="T921">togonər-zittə</ta>
            <ta e="T923" id="Seg_8466" s="T922">tĭmne-t</ta>
            <ta e="T927" id="Seg_8467" s="T926">dĭn</ta>
            <ta e="T928" id="Seg_8468" s="T927">nüke-n</ta>
            <ta e="T930" id="Seg_8469" s="T929">tugan-zAŋ-də</ta>
            <ta e="T931" id="Seg_8470" s="T930">amka</ta>
            <ta e="T932" id="Seg_8471" s="T931">amnə-m-bi</ta>
            <ta e="T933" id="Seg_8472" s="T932">tolʼko</ta>
            <ta e="T934" id="Seg_8473" s="T933">onʼiʔ</ta>
            <ta e="T935" id="Seg_8474" s="T934">kaga-t</ta>
            <ta e="T939" id="Seg_8475" s="T938">a</ta>
            <ta e="T940" id="Seg_8476" s="T939">sʼestra-t</ta>
            <ta e="T941" id="Seg_8477" s="T940">kü-laːm-bi</ta>
            <ta e="T945" id="Seg_8478" s="T944">šödör-luʔbdə-bi-m</ta>
            <ta e="T946" id="Seg_8479" s="T945">bar</ta>
            <ta e="T947" id="Seg_8480" s="T946">tüj</ta>
            <ta e="T948" id="Seg_8481" s="T947">naga</ta>
            <ta e="T949" id="Seg_8482" s="T948">šöʔ-zittə</ta>
            <ta e="T953" id="Seg_8483" s="T952">miʔ</ta>
            <ta e="T954" id="Seg_8484" s="T953">jelezʼe</ta>
            <ta e="T955" id="Seg_8485" s="T954">kan-bi-bAʔ</ta>
            <ta e="T956" id="Seg_8486" s="T955">bü</ta>
            <ta e="T957" id="Seg_8487" s="T956">tažor-zittə</ta>
            <ta e="T958" id="Seg_8488" s="T957">a</ta>
            <ta e="T959" id="Seg_8489" s="T958">dĭn</ta>
            <ta e="T960" id="Seg_8490" s="T959">bü</ta>
            <ta e="T961" id="Seg_8491" s="T960">i-gA</ta>
            <ta e="T963" id="Seg_8492" s="T962">nagur</ta>
            <ta e="T965" id="Seg_8493" s="T964">nagur</ta>
            <ta e="T966" id="Seg_8494" s="T965">vedro</ta>
            <ta e="T967" id="Seg_8495" s="T966">det-bi-bAʔ</ta>
            <ta e="T968" id="Seg_8496" s="T967">i</ta>
            <ta e="T969" id="Seg_8497" s="T968">kabarləj</ta>
            <ta e="T970" id="Seg_8498" s="T969">tüj</ta>
            <ta e="T974" id="Seg_8499" s="T973">dʼije-Kən</ta>
            <ta e="T975" id="Seg_8500" s="T974">šonə-gA-m</ta>
            <ta e="T976" id="Seg_8501" s="T975">dʼije</ta>
            <ta e="T977" id="Seg_8502" s="T976">nüj-laʔbə-m</ta>
            <ta e="T979" id="Seg_8503" s="T978">pa-Kən</ta>
            <ta e="T980" id="Seg_8504" s="T979">šonə-gA-m</ta>
            <ta e="T981" id="Seg_8505" s="T980">pa</ta>
            <ta e="T982" id="Seg_8506" s="T981">nüj-laʔbə-m</ta>
            <ta e="T984" id="Seg_8507" s="T983">sʼtʼep</ta>
            <ta e="T985" id="Seg_8508" s="T984">šonə-gA-m</ta>
            <ta e="T986" id="Seg_8509" s="T985">sʼtʼep</ta>
            <ta e="T987" id="Seg_8510" s="T986">nüj-laʔbə-m</ta>
            <ta e="T989" id="Seg_8511" s="T988">măja</ta>
            <ta e="T990" id="Seg_8512" s="T989">šonə-gA-m</ta>
            <ta e="T991" id="Seg_8513" s="T990">măja</ta>
            <ta e="T992" id="Seg_8514" s="T991">nüj-laʔbə-m</ta>
            <ta e="T994" id="Seg_8515" s="T993">Dʼăla-m</ta>
            <ta e="T995" id="Seg_8516" s="T994">šonə-gA</ta>
            <ta e="T996" id="Seg_8517" s="T995">Dʼăla-m</ta>
            <ta e="T998" id="Seg_8518" s="T997">nüj-laʔbə-m</ta>
            <ta e="T1000" id="Seg_8519" s="T999">tura-Kən</ta>
            <ta e="T1001" id="Seg_8520" s="T1000">amnə-laʔbə</ta>
            <ta e="T1002" id="Seg_8521" s="T1001">tura</ta>
            <ta e="T1003" id="Seg_8522" s="T1002">nüj-laʔbə-m</ta>
            <ta e="T1007" id="Seg_8523" s="T1006">miʔ</ta>
            <ta e="T1008" id="Seg_8524" s="T1007">măn</ta>
            <ta e="T1009" id="Seg_8525" s="T1008">tura-gənʼi</ta>
            <ta e="T1010" id="Seg_8526" s="T1009">ugaːndə</ta>
            <ta e="T1011" id="Seg_8527" s="T1010">tumo</ta>
            <ta e="T1012" id="Seg_8528" s="T1011">iʔgö</ta>
            <ta e="T1013" id="Seg_8529" s="T1012">ĭmbi</ta>
            <ta e="T1014" id="Seg_8530" s="T1013">păpală</ta>
            <ta e="T1015" id="Seg_8531" s="T1014">bar</ta>
            <ta e="T1016" id="Seg_8532" s="T1015">am-liA-jəʔ</ta>
            <ta e="T1018" id="Seg_8533" s="T1017">a</ta>
            <ta e="T1019" id="Seg_8534" s="T1018">tospak</ta>
            <ta e="T1020" id="Seg_8535" s="T1019">naga</ta>
            <ta e="T1022" id="Seg_8536" s="T1021">kan-ə-ʔ</ta>
            <ta e="T1023" id="Seg_8537" s="T1022">miʔne-Tə</ta>
            <ta e="T1024" id="Seg_8538" s="T1023">dĭ</ta>
            <ta e="T1025" id="Seg_8539" s="T1024">dĭn</ta>
            <ta e="T1026" id="Seg_8540" s="T1025">šide</ta>
            <ta e="T1028" id="Seg_8541" s="T1027">tospak</ta>
            <ta e="T1029" id="Seg_8542" s="T1028">onʼiʔ</ta>
            <ta e="T1030" id="Seg_8543" s="T1029">sĭri</ta>
            <ta e="T1031" id="Seg_8544" s="T1030">onʼiʔ</ta>
            <ta e="T1032" id="Seg_8545" s="T1031">kömə</ta>
            <ta e="T1034" id="Seg_8546" s="T1033">dĭ</ta>
            <ta e="T1035" id="Seg_8547" s="T1034">mĭ-lV-j</ta>
            <ta e="T1036" id="Seg_8548" s="T1035">tănan</ta>
            <ta e="T1038" id="Seg_8549" s="T1037">da</ta>
            <ta e="T1039" id="Seg_8550" s="T1038">dĭ</ta>
            <ta e="T1040" id="Seg_8551" s="T1039">sĭri</ta>
            <ta e="T1041" id="Seg_8552" s="T1040">măna</ta>
            <ta e="T1042" id="Seg_8553" s="T1041">i-bi</ta>
            <ta e="T1043" id="Seg_8554" s="T1042">da</ta>
            <ta e="T1181" id="Seg_8555" s="T1043">kan-lAʔ</ta>
            <ta e="T1044" id="Seg_8556" s="T1181">tʼür-bi</ta>
            <ta e="T1045" id="Seg_8557" s="T1044">dĭ</ta>
            <ta e="T1046" id="Seg_8558" s="T1045">dĭ-m</ta>
            <ta e="T1047" id="Seg_8559" s="T1046">ej</ta>
            <ta e="T1048" id="Seg_8560" s="T1047">öʔ-lei-bi</ta>
            <ta e="T1050" id="Seg_8561" s="T1049">daška</ta>
            <ta e="T1051" id="Seg_8562" s="T1050">ej</ta>
            <ta e="T1052" id="Seg_8563" s="T1051">šo-liA</ta>
            <ta e="T1056" id="Seg_8564" s="T1055">măn</ta>
            <ta e="T1057" id="Seg_8565" s="T1056">kunol-zittə</ta>
            <ta e="T1058" id="Seg_8566" s="T1057">iʔbə-bi-m</ta>
            <ta e="T1060" id="Seg_8567" s="T1059">a</ta>
            <ta e="T1061" id="Seg_8568" s="T1060">dĭn</ta>
            <ta e="T1062" id="Seg_8569" s="T1061">ugaːndə</ta>
            <ta e="T1063" id="Seg_8570" s="T1062">putʼəga</ta>
            <ta e="T1064" id="Seg_8571" s="T1063">ku-liA-m</ta>
            <ta e="T1065" id="Seg_8572" s="T1064">tüʔ</ta>
            <ta e="T1067" id="Seg_8573" s="T1066">iʔgö</ta>
            <ta e="T1068" id="Seg_8574" s="T1067">iʔbö-laʔbə</ta>
            <ta e="T1072" id="Seg_8575" s="T1071">ertə-n</ta>
            <ta e="T1073" id="Seg_8576" s="T1072">uʔbdə-bi-m</ta>
            <ta e="T1074" id="Seg_8577" s="T1073">kălʼenka-Tə</ta>
            <ta e="T1075" id="Seg_8578" s="T1074">nu-bi-m</ta>
            <ta e="T1077" id="Seg_8579" s="T1076">ku-liA-m</ta>
            <ta e="T1078" id="Seg_8580" s="T1077">bar</ta>
            <ta e="T1079" id="Seg_8581" s="T1078">dĭn</ta>
            <ta e="T1080" id="Seg_8582" s="T1079">biəʔ</ta>
            <ta e="T1081" id="Seg_8583" s="T1080">kuči</ta>
            <ta e="T1082" id="Seg_8584" s="T1081">iʔbö-laʔbə</ta>
            <ta e="T1086" id="Seg_8585" s="T1085">dĭgəttə</ta>
            <ta e="T1087" id="Seg_8586" s="T1086">uja-m</ta>
            <ta e="T1088" id="Seg_8587" s="T1087">kun-ə-labaʔ-m</ta>
            <ta e="T1089" id="Seg_8588" s="T1088">nʼiʔdə</ta>
            <ta e="T1090" id="Seg_8589" s="T1089">takše-Kən</ta>
            <ta e="T1091" id="Seg_8590" s="T1090">takše-ziʔ</ta>
            <ta e="T1092" id="Seg_8591" s="T1091">kaj-liA-m</ta>
            <ta e="T1093" id="Seg_8592" s="T1092">štobɨ</ta>
            <ta e="T1094" id="Seg_8593" s="T1093">tumo-jəʔ</ta>
            <ta e="T1095" id="Seg_8594" s="T1094">ej</ta>
            <ta e="T1096" id="Seg_8595" s="T1095">am-bi-jəʔ</ta>
            <ta e="T1098" id="Seg_8596" s="T1097">a</ta>
            <ta e="T1099" id="Seg_8597" s="T1098">kajaʔ</ta>
            <ta e="T1100" id="Seg_8598" s="T1099">măslăbojkă-Tə</ta>
            <ta e="T1101" id="Seg_8599" s="T1100">tože</ta>
            <ta e="T1103" id="Seg_8600" s="T1102">kaj-labaʔ-m</ta>
            <ta e="T1104" id="Seg_8601" s="T1103">štobɨ</ta>
            <ta e="T1105" id="Seg_8602" s="T1104">tumo-jəʔ</ta>
            <ta e="T1106" id="Seg_8603" s="T1105">ej</ta>
            <ta e="T1107" id="Seg_8604" s="T1106">am-bi-jəʔ</ta>
            <ta e="T1111" id="Seg_8605" s="T1110">a</ta>
            <ta e="T1113" id="Seg_8606" s="T1112">tak</ta>
            <ta e="T1114" id="Seg_8607" s="T1113">miʔnʼibeʔ</ta>
            <ta e="T1115" id="Seg_8608" s="T1114">šide</ta>
            <ta e="T1116" id="Seg_8609" s="T1115">i-gA</ta>
            <ta e="T1117" id="Seg_8610" s="T1116">tospak</ta>
            <ta e="T1118" id="Seg_8611" s="T1117">det-lV-m</ta>
            <ta e="T1119" id="Seg_8612" s="T1118">onʼiʔ</ta>
            <ta e="T1121" id="Seg_8613" s="T1120">girgit</ta>
            <ta e="T1122" id="Seg_8614" s="T1121">tănan</ta>
            <ta e="T1123" id="Seg_8615" s="T1122">det-zittə</ta>
            <ta e="T1124" id="Seg_8616" s="T1123">sĭri</ta>
            <ta e="T1125" id="Seg_8617" s="T1124">aľi</ta>
            <ta e="T1126" id="Seg_8618" s="T1125">kömə</ta>
            <ta e="T1128" id="Seg_8619" s="T1127">nu</ta>
            <ta e="T1129" id="Seg_8620" s="T1128">det-ʔ</ta>
            <ta e="T1130" id="Seg_8621" s="T1129">kömə</ta>
            <ta e="T1134" id="Seg_8622" s="T1133">multʼa-Tə</ta>
            <ta e="T1135" id="Seg_8623" s="T1134">šo-gA</ta>
            <ta e="T1136" id="Seg_8624" s="T1135">dĭn</ta>
            <ta e="T1137" id="Seg_8625" s="T1136">ugaːndə</ta>
            <ta e="T1138" id="Seg_8626" s="T1137">bü</ta>
            <ta e="T1139" id="Seg_8627" s="T1138">iʔgö</ta>
            <ta e="T1141" id="Seg_8628" s="T1140">dʼibige</ta>
            <ta e="T1142" id="Seg_8629" s="T1141">bü</ta>
            <ta e="T1143" id="Seg_8630" s="T1142">i-gA</ta>
            <ta e="T1144" id="Seg_8631" s="T1143">šišəge</ta>
            <ta e="T1145" id="Seg_8632" s="T1144">sabən</ta>
            <ta e="T1146" id="Seg_8633" s="T1145">i-gA</ta>
            <ta e="T1148" id="Seg_8634" s="T1147">ejü</ta>
            <ta e="T1149" id="Seg_8635" s="T1148">ejü-m-lV-l</ta>
            <ta e="T1150" id="Seg_8636" s="T1149">bar</ta>
            <ta e="T1154" id="Seg_8637" s="T1153">püje-l</ta>
            <ta e="T1155" id="Seg_8638" s="T1154">ej</ta>
            <ta e="T1156" id="Seg_8639" s="T1155">ĭzem-NTA</ta>
            <ta e="T1157" id="Seg_8640" s="T1156">i</ta>
            <ta e="T1158" id="Seg_8641" s="T1157">koklu-jəʔ</ta>
            <ta e="T1159" id="Seg_8642" s="T1158">mʼaŋ-NTA-jəʔ</ta>
            <ta e="T1163" id="Seg_8643" s="T1162">dö</ta>
            <ta e="T1164" id="Seg_8644" s="T1163">zaplatka</ta>
            <ta e="T1165" id="Seg_8645" s="T1164">a</ta>
            <ta e="T1166" id="Seg_8646" s="T1165">gibər</ta>
            <ta e="T1167" id="Seg_8647" s="T1166">dĭ-m</ta>
            <ta e="T1168" id="Seg_8648" s="T1167">zaplatka</ta>
            <ta e="T1170" id="Seg_8649" s="T1169">köten-gəndə</ta>
            <ta e="T1171" id="Seg_8650" s="T1170">amnol-zittə</ta>
            <ta e="T1172" id="Seg_8651" s="T1171">štoli</ta>
            <ta e="T1174" id="Seg_8652" s="T1173">barəʔ-btɨ-ʔ</ta>
            <ta e="T1175" id="Seg_8653" s="T1174">ej</ta>
            <ta e="T1176" id="Seg_8654" s="T1175">kereʔ</ta>
            <ta e="T1177" id="Seg_8655" s="T1176">măna</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_8656" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_8657" s="T2">yesterday</ta>
            <ta e="T4" id="Seg_8658" s="T3">go-PST-1SG</ta>
            <ta e="T5" id="Seg_8659" s="T4">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T7" id="Seg_8660" s="T6">I.NOM</ta>
            <ta e="T8" id="Seg_8661" s="T7">say-PST-1SG</ta>
            <ta e="T9" id="Seg_8662" s="T8">NEG.AUX-IMP.2SG</ta>
            <ta e="T10" id="Seg_8663" s="T9">be.angry-CNG</ta>
            <ta e="T11" id="Seg_8664" s="T10">I.LAT</ta>
            <ta e="T14" id="Seg_8665" s="T13">sew-INF.LAT</ta>
            <ta e="T16" id="Seg_8666" s="T15">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T17" id="Seg_8667" s="T16">NEG</ta>
            <ta e="T18" id="Seg_8668" s="T17">go-PST.[3SG]</ta>
            <ta e="T19" id="Seg_8669" s="T18">and</ta>
            <ta e="T20" id="Seg_8670" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_8671" s="T20">NEG</ta>
            <ta e="T22" id="Seg_8672" s="T21">go-PST-1SG</ta>
            <ta e="T24" id="Seg_8673" s="T23">this-PL</ta>
            <ta e="T25" id="Seg_8674" s="T24">bed-LOC</ta>
            <ta e="T26" id="Seg_8675" s="T25">lie-DUR-3PL</ta>
            <ta e="T27" id="Seg_8676" s="T26">hug-DUR-3PL</ta>
            <ta e="T29" id="Seg_8677" s="T28">Kolya.[NOM.SG]</ta>
            <ta e="T30" id="Seg_8678" s="T29">Dunya-3SG-COM</ta>
            <ta e="T32" id="Seg_8679" s="T31">kiss-DUR-3PL</ta>
            <ta e="T34" id="Seg_8680" s="T33">laugh-DUR-3PL</ta>
            <ta e="T36" id="Seg_8681" s="T35">say-IPFVZ-3PL</ta>
            <ta e="T37" id="Seg_8682" s="T36">grandmother.[NOM.SG]</ta>
            <ta e="T38" id="Seg_8683" s="T37">eat-EP-IMP.2SG</ta>
            <ta e="T39" id="Seg_8684" s="T38">sit-IMP.2SG</ta>
            <ta e="T41" id="Seg_8685" s="T40">vodka.[NOM.SG]</ta>
            <ta e="T42" id="Seg_8686" s="T41">drink-EP-IMP.2SG</ta>
            <ta e="T44" id="Seg_8687" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_8688" s="T44">say-IPFVZ-1SG</ta>
            <ta e="T46" id="Seg_8689" s="T45">NEG</ta>
            <ta e="T47" id="Seg_8690" s="T46">drink-FUT-1SG</ta>
            <ta e="T49" id="Seg_8691" s="T48">and</ta>
            <ta e="T50" id="Seg_8692" s="T49">then</ta>
            <ta e="T1178" id="Seg_8693" s="T50">go-CVB</ta>
            <ta e="T51" id="Seg_8694" s="T1178">disappear-PST-1SG</ta>
            <ta e="T52" id="Seg_8695" s="T51">house-LAT/LOC.1SG</ta>
            <ta e="T56" id="Seg_8696" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_8697" s="T56">today</ta>
            <ta e="T58" id="Seg_8698" s="T57">woman-LAT</ta>
            <ta e="T59" id="Seg_8699" s="T58">go-PST-1SG</ta>
            <ta e="T60" id="Seg_8700" s="T59">this.[NOM.SG]</ta>
            <ta e="T61" id="Seg_8701" s="T60">hurt-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_8702" s="T62">this.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8703" s="T63">say-IPFVZ.[3SG]</ta>
            <ta e="T65" id="Seg_8704" s="T64">what.[NOM.SG]</ta>
            <ta e="T66" id="Seg_8705" s="T65">long.time</ta>
            <ta e="T67" id="Seg_8706" s="T66">NEG</ta>
            <ta e="T68" id="Seg_8707" s="T67">come-PST-1SG</ta>
            <ta e="T69" id="Seg_8708" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_8709" s="T69">PTCL</ta>
            <ta e="T71" id="Seg_8710" s="T70">dry-%%-PRS-1SG</ta>
            <ta e="T72" id="Seg_8711" s="T71">you.NOM</ta>
            <ta e="T73" id="Seg_8712" s="T72">always</ta>
            <ta e="T74" id="Seg_8713" s="T73">dry-%%-PRS-2SG</ta>
            <ta e="T75" id="Seg_8714" s="T74">and</ta>
            <ta e="T76" id="Seg_8715" s="T75">die-INF.LAT</ta>
            <ta e="T77" id="Seg_8716" s="T76">NEG</ta>
            <ta e="T78" id="Seg_8717" s="T77">can-PRS-2SG</ta>
            <ta e="T80" id="Seg_8718" s="T79">well</ta>
            <ta e="T81" id="Seg_8719" s="T80">die-IMP.2SG</ta>
            <ta e="T83" id="Seg_8720" s="T82">I.LAT</ta>
            <ta e="T84" id="Seg_8721" s="T83">what.[NOM.SG]</ta>
            <ta e="T86" id="Seg_8722" s="T85">what.[NOM.SG]</ta>
            <ta e="T87" id="Seg_8723" s="T86">I.NOM</ta>
            <ta e="T88" id="Seg_8724" s="T87">you.NOM</ta>
            <ta e="T89" id="Seg_8725" s="T88">edge-LAT/LOC.3SG</ta>
            <ta e="T90" id="Seg_8726" s="T89">sit-DUR-FUT-1SG</ta>
            <ta e="T91" id="Seg_8727" s="T90">isn_t.it</ta>
            <ta e="T93" id="Seg_8728" s="T92">I.NOM</ta>
            <ta e="T1185" id="Seg_8729" s="T93">you.know</ta>
            <ta e="T94" id="Seg_8730" s="T1185">people.[NOM.SG]</ta>
            <ta e="T95" id="Seg_8731" s="T94">live-DUR-3PL</ta>
            <ta e="T97" id="Seg_8732" s="T96">there</ta>
            <ta e="T98" id="Seg_8733" s="T97">wait-DUR-3PL</ta>
            <ta e="T99" id="Seg_8734" s="T98">I.LAT</ta>
            <ta e="T101" id="Seg_8735" s="T100">speak-INF.LAT</ta>
            <ta e="T102" id="Seg_8736" s="T101">one.should</ta>
            <ta e="T104" id="Seg_8737" s="T103">and</ta>
            <ta e="T105" id="Seg_8738" s="T104">you.NOM</ta>
            <ta e="T106" id="Seg_8739" s="T105">edge-LAT/LOC.3SG</ta>
            <ta e="T107" id="Seg_8740" s="T106">sit-IMP.2SG</ta>
            <ta e="T111" id="Seg_8741" s="T110">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T113" id="Seg_8742" s="T112">two.[NOM.SG]</ta>
            <ta e="T114" id="Seg_8743" s="T113">day.[NOM.SG]</ta>
            <ta e="T115" id="Seg_8744" s="T114">be-PST.[3SG]</ta>
            <ta e="T117" id="Seg_8745" s="T116">and</ta>
            <ta e="T118" id="Seg_8746" s="T117">you.NOM</ta>
            <ta e="T119" id="Seg_8747" s="T118">edge-LAT/LOC.3SG</ta>
            <ta e="T120" id="Seg_8748" s="T119">sit-IMP.2SG</ta>
            <ta e="T122" id="Seg_8749" s="T121">and</ta>
            <ta e="T123" id="Seg_8750" s="T122">people.[NOM.SG]</ta>
            <ta e="T124" id="Seg_8751" s="T123">go-PRS-3PL</ta>
            <ta e="T126" id="Seg_8752" s="T125">and</ta>
            <ta e="T127" id="Seg_8753" s="T126">this.[NOM.SG]</ta>
            <ta e="T128" id="Seg_8754" s="T127">say-IPFVZ.[3SG]</ta>
            <ta e="T129" id="Seg_8755" s="T128">and</ta>
            <ta e="T130" id="Seg_8756" s="T129">God.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8757" s="T130">this-PL-COM</ta>
            <ta e="T133" id="Seg_8758" s="T132">and</ta>
            <ta e="T134" id="Seg_8759" s="T133">you.NOM-COM</ta>
            <ta e="T135" id="Seg_8760" s="T134">JUSS</ta>
            <ta e="T136" id="Seg_8761" s="T135">God.[NOM.SG]</ta>
            <ta e="T138" id="Seg_8762" s="T137">die-IMP.2SG</ta>
            <ta e="T139" id="Seg_8763" s="T138">die-IMP.2SG</ta>
            <ta e="T143" id="Seg_8764" s="T142">then</ta>
            <ta e="T144" id="Seg_8765" s="T143">this.[NOM.SG]</ta>
            <ta e="T145" id="Seg_8766" s="T144">remain-DUR.[3SG]</ta>
            <ta e="T146" id="Seg_8767" s="T145">and</ta>
            <ta e="T147" id="Seg_8768" s="T146">I.NOM</ta>
            <ta e="T148" id="Seg_8769" s="T147">tent-LAT/LOC.1SG</ta>
            <ta e="T1179" id="Seg_8770" s="T148">go-CVB</ta>
            <ta e="T149" id="Seg_8771" s="T1179">disappear-PST-1SG</ta>
            <ta e="T153" id="Seg_8772" s="T152">I.NOM</ta>
            <ta e="T1183" id="Seg_8773" s="T153">Aginskoe</ta>
            <ta e="T154" id="Seg_8774" s="T1183">settlement-LAT</ta>
            <ta e="T155" id="Seg_8775" s="T154">go-PST-1SG</ta>
            <ta e="T157" id="Seg_8776" s="T156">and</ta>
            <ta e="T158" id="Seg_8777" s="T157">this.[NOM.SG]</ta>
            <ta e="T159" id="Seg_8778" s="T158">woman.[NOM.SG]</ta>
            <ta e="T160" id="Seg_8779" s="T159">go-PST.[3SG]</ta>
            <ta e="T161" id="Seg_8780" s="T160">this-GEN</ta>
            <ta e="T162" id="Seg_8781" s="T161">edge-LAT/LOC.3SG</ta>
            <ta e="T163" id="Seg_8782" s="T162">there</ta>
            <ta e="T164" id="Seg_8783" s="T163">woman.[NOM.SG]</ta>
            <ta e="T165" id="Seg_8784" s="T164">live-DUR.[3SG]</ta>
            <ta e="T167" id="Seg_8785" s="T166">come-PST.[3SG]</ta>
            <ta e="T169" id="Seg_8786" s="T168">and</ta>
            <ta e="T1180" id="Seg_8787" s="T169">go-CVB</ta>
            <ta e="T170" id="Seg_8788" s="T1180">disappear-PST.[3SG]</ta>
            <ta e="T172" id="Seg_8789" s="T171">I.NOM</ta>
            <ta e="T173" id="Seg_8790" s="T172">die-FUT-1SG</ta>
            <ta e="T174" id="Seg_8791" s="T173">and</ta>
            <ta e="T175" id="Seg_8792" s="T174">who.[NOM.SG]</ta>
            <ta e="T176" id="Seg_8793" s="T175">I.LAT</ta>
            <ta e="T178" id="Seg_8794" s="T177">wash.oneself-TR-FUT-3SG</ta>
            <ta e="T180" id="Seg_8795" s="T179">this-PL</ta>
            <ta e="T181" id="Seg_8796" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_8797" s="T181">laugh-PST-3PL</ta>
            <ta e="T183" id="Seg_8798" s="T182">strongly</ta>
            <ta e="T185" id="Seg_8799" s="T184">then</ta>
            <ta e="T186" id="Seg_8800" s="T185">I.NOM</ta>
            <ta e="T187" id="Seg_8801" s="T186">come-PST-1SG</ta>
            <ta e="T188" id="Seg_8802" s="T187">go-PST-1SG</ta>
            <ta e="T189" id="Seg_8803" s="T188">this-LAT</ta>
            <ta e="T190" id="Seg_8804" s="T189">you.NOM</ta>
            <ta e="T1182" id="Seg_8805" s="T190">go-CVB</ta>
            <ta e="T191" id="Seg_8806" s="T1182">disappear-PST-1SG</ta>
            <ta e="T192" id="Seg_8807" s="T191">and</ta>
            <ta e="T193" id="Seg_8808" s="T192">I.NOM</ta>
            <ta e="T194" id="Seg_8809" s="T193">die-FRQ-RES-PST-1SG</ta>
            <ta e="T195" id="Seg_8810" s="T194">and</ta>
            <ta e="T196" id="Seg_8811" s="T195">freeze-FACT-PST-1SG</ta>
            <ta e="T198" id="Seg_8812" s="T197">and</ta>
            <ta e="T199" id="Seg_8813" s="T198">I.NOM</ta>
            <ta e="T200" id="Seg_8814" s="T199">say-PST-1SG</ta>
            <ta e="T201" id="Seg_8815" s="T200">well</ta>
            <ta e="T202" id="Seg_8816" s="T201">freeze-IMP.2SG</ta>
            <ta e="T204" id="Seg_8817" s="T203">what.[NOM.SG]</ta>
            <ta e="T205" id="Seg_8818" s="T204">and</ta>
            <ta e="T207" id="Seg_8819" s="T206">and</ta>
            <ta e="T208" id="Seg_8820" s="T207">die-IMP.2SG</ta>
            <ta e="T210" id="Seg_8821" s="T209">IRREAL</ta>
            <ta e="T211" id="Seg_8822" s="T210">I.NOM</ta>
            <ta e="T212" id="Seg_8823" s="T211">water.[NOM.SG]</ta>
            <ta e="T213" id="Seg_8824" s="T212">become.warm-TR-1SG</ta>
            <ta e="T215" id="Seg_8825" s="T214">and</ta>
            <ta e="T216" id="Seg_8826" s="T215">you.DAT</ta>
            <ta e="T218" id="Seg_8827" s="T217">pour-FUT-2SG</ta>
            <ta e="T220" id="Seg_8828" s="T219">then</ta>
            <ta e="T221" id="Seg_8829" s="T220">wash-FUT-1SG</ta>
            <ta e="T222" id="Seg_8830" s="T221">and</ta>
            <ta e="T223" id="Seg_8831" s="T222">place-LAT</ta>
            <ta e="T226" id="Seg_8832" s="T225">place-LAT</ta>
            <ta e="T227" id="Seg_8833" s="T226">put-FUT-PST-1SG</ta>
            <ta e="T241" id="Seg_8834" s="T240">this.[NOM.SG]</ta>
            <ta e="T242" id="Seg_8835" s="T241">woman.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8836" s="T242">PTCL</ta>
            <ta e="T244" id="Seg_8837" s="T243">I.LAT</ta>
            <ta e="T245" id="Seg_8838" s="T244">burn-IMP.2SG</ta>
            <ta e="T246" id="Seg_8839" s="T245">sauna.[NOM.SG]</ta>
            <ta e="T247" id="Seg_8840" s="T246">wash-IMP.2SG</ta>
            <ta e="T248" id="Seg_8841" s="T247">I.LAT</ta>
            <ta e="T250" id="Seg_8842" s="T249">die-RES-FUT-1SG</ta>
            <ta e="T251" id="Seg_8843" s="T250">so</ta>
            <ta e="T252" id="Seg_8844" s="T251">then</ta>
            <ta e="T254" id="Seg_8845" s="T252">face-NOM/GEN/ACC.3SG=PTCL</ta>
            <ta e="T256" id="Seg_8846" s="T254">hand-PL=PTCL</ta>
            <ta e="T257" id="Seg_8847" s="T256">wash-FUT-2SG</ta>
            <ta e="T259" id="Seg_8848" s="T258">I.NOM</ta>
            <ta e="T260" id="Seg_8849" s="T259">then</ta>
            <ta e="T261" id="Seg_8850" s="T260">burn-PST-1SG</ta>
            <ta e="T263" id="Seg_8851" s="T262">sauna.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8852" s="T264">then</ta>
            <ta e="T266" id="Seg_8853" s="T265">sledge-PL-INS</ta>
            <ta e="T267" id="Seg_8854" s="T266">put-PST-1SG</ta>
            <ta e="T268" id="Seg_8855" s="T267">and</ta>
            <ta e="T269" id="Seg_8856" s="T268">%%</ta>
            <ta e="T270" id="Seg_8857" s="T269">bring-RES-PST-1SG</ta>
            <ta e="T271" id="Seg_8858" s="T270">wash-PST-1SG</ta>
            <ta e="T272" id="Seg_8859" s="T271">this-ACC</ta>
            <ta e="T273" id="Seg_8860" s="T272">and</ta>
            <ta e="T274" id="Seg_8861" s="T273">again</ta>
            <ta e="T275" id="Seg_8862" s="T274">tent-LAT</ta>
            <ta e="T276" id="Seg_8863" s="T275">bring-PST-1SG</ta>
            <ta e="T277" id="Seg_8864" s="T276">sledge-PL-INS</ta>
            <ta e="T279" id="Seg_8865" s="T278">put-PST-1SG</ta>
            <ta e="T280" id="Seg_8866" s="T279">bed-LAT</ta>
            <ta e="T281" id="Seg_8867" s="T280">say-IPFVZ-1SG</ta>
            <ta e="T282" id="Seg_8868" s="T281">well</ta>
            <ta e="T283" id="Seg_8869" s="T282">now</ta>
            <ta e="T284" id="Seg_8870" s="T283">die-IMP.2SG</ta>
            <ta e="T286" id="Seg_8871" s="T285">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T287" id="Seg_8872" s="T286">and</ta>
            <ta e="T288" id="Seg_8873" s="T287">hand-PL-COM</ta>
            <ta e="T289" id="Seg_8874" s="T288">wash-FUT-1SG</ta>
            <ta e="T290" id="Seg_8875" s="T289">and</ta>
            <ta e="T291" id="Seg_8876" s="T290">good</ta>
            <ta e="T292" id="Seg_8877" s="T291">become-FUT-3SG</ta>
            <ta e="T294" id="Seg_8878" s="T293">well</ta>
            <ta e="T295" id="Seg_8879" s="T294">then</ta>
            <ta e="T296" id="Seg_8880" s="T295">sauna-LOC</ta>
            <ta e="T297" id="Seg_8881" s="T296">wash-PST-1SG</ta>
            <ta e="T298" id="Seg_8882" s="T297">very</ta>
            <ta e="T300" id="Seg_8883" s="T299">dirty.[NOM.SG]</ta>
            <ta e="T301" id="Seg_8884" s="T300">many</ta>
            <ta e="T302" id="Seg_8885" s="T301">be-PST.[3SG]</ta>
            <ta e="T303" id="Seg_8886" s="T302">head-LAT/LOC.3SG</ta>
            <ta e="T305" id="Seg_8887" s="T304">I.NOM</ta>
            <ta e="T306" id="Seg_8888" s="T305">say-PRS-1SG</ta>
            <ta e="T307" id="Seg_8889" s="T306">more</ta>
            <ta e="T308" id="Seg_8890" s="T307">put-EP-IMP.2SG</ta>
            <ta e="T309" id="Seg_8891" s="T308">water.[NOM.SG]</ta>
            <ta e="T311" id="Seg_8892" s="T310">that-ACC</ta>
            <ta e="T313" id="Seg_8893" s="T312">pour-IMP.2SG.O</ta>
            <ta e="T317" id="Seg_8894" s="T316">one.[NOM.SG]</ta>
            <ta e="T319" id="Seg_8895" s="T318">horse.[NOM.SG]</ta>
            <ta e="T320" id="Seg_8896" s="T319">give-PST-1SG</ta>
            <ta e="T321" id="Seg_8897" s="T320">man-GEN</ta>
            <ta e="T322" id="Seg_8898" s="T321">brother-LAT</ta>
            <ta e="T324" id="Seg_8899" s="T323">two.[NOM.SG]</ta>
            <ta e="T325" id="Seg_8900" s="T324">year.[NOM.SG]</ta>
            <ta e="T326" id="Seg_8901" s="T325">be-PST.[3SG]</ta>
            <ta e="T328" id="Seg_8902" s="T327">again</ta>
            <ta e="T329" id="Seg_8903" s="T328">horse.[NOM.SG]</ta>
            <ta e="T330" id="Seg_8904" s="T329">give-PST-1SG</ta>
            <ta e="T331" id="Seg_8905" s="T330">man-LAT</ta>
            <ta e="T333" id="Seg_8906" s="T332">man.[NOM.SG]</ta>
            <ta e="T334" id="Seg_8907" s="T333">be-PST.[3SG]</ta>
            <ta e="T336" id="Seg_8908" s="T335">NEG.EX.[3SG]</ta>
            <ta e="T337" id="Seg_8909" s="T336">horse.[NOM.SG]</ta>
            <ta e="T338" id="Seg_8910" s="T337">also</ta>
            <ta e="T339" id="Seg_8911" s="T338">two.[NOM.SG]</ta>
            <ta e="T340" id="Seg_8912" s="T339">year.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8913" s="T340">be-PST.[3SG]</ta>
            <ta e="T343" id="Seg_8914" s="T342">give-PST-1SG</ta>
            <ta e="T344" id="Seg_8915" s="T343">horse.[NOM.SG]</ta>
            <ta e="T345" id="Seg_8916" s="T344">then</ta>
            <ta e="T346" id="Seg_8917" s="T345">this.[NOM.SG]</ta>
            <ta e="T347" id="Seg_8918" s="T346">sister-LAT/LOC.3SG</ta>
            <ta e="T348" id="Seg_8919" s="T347">sheep.[NOM.SG]</ta>
            <ta e="T349" id="Seg_8920" s="T348">give-PST-1SG</ta>
            <ta e="T351" id="Seg_8921" s="T350">fur.coat.[NOM.SG]</ta>
            <ta e="T352" id="Seg_8922" s="T351">give-PST-1SG</ta>
            <ta e="T354" id="Seg_8923" s="T353">then</ta>
            <ta e="T355" id="Seg_8924" s="T354">niece-GEN</ta>
            <ta e="T356" id="Seg_8925" s="T355">daughter-GEN.1SG</ta>
            <ta e="T357" id="Seg_8926" s="T356">give-PST-1SG</ta>
            <ta e="T358" id="Seg_8927" s="T357">bull.[NOM.SG]</ta>
            <ta e="T360" id="Seg_8928" s="T359">half.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8929" s="T360">half.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8930" s="T361">winter.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8931" s="T363">then</ta>
            <ta e="T366" id="Seg_8932" s="T365">sister-LAT</ta>
            <ta e="T367" id="Seg_8933" s="T366">son-NOM/GEN.3SG</ta>
            <ta e="T368" id="Seg_8934" s="T367">give-PST-1SG</ta>
            <ta e="T369" id="Seg_8935" s="T368">cow.[NOM.SG]</ta>
            <ta e="T370" id="Seg_8936" s="T369">and</ta>
            <ta e="T371" id="Seg_8937" s="T370">sheep.[NOM.SG]</ta>
            <ta e="T372" id="Seg_8938" s="T371">give-PST-1SG</ta>
            <ta e="T376" id="Seg_8939" s="T375">then</ta>
            <ta e="T377" id="Seg_8940" s="T376">one.[NOM.SG]</ta>
            <ta e="T378" id="Seg_8941" s="T377">woman-LAT</ta>
            <ta e="T379" id="Seg_8942" s="T378">again</ta>
            <ta e="T381" id="Seg_8943" s="T380">calf.[NOM.SG]</ta>
            <ta e="T382" id="Seg_8944" s="T381">give-PST-1SG</ta>
            <ta e="T384" id="Seg_8945" s="T383">people.[NOM.SG]</ta>
            <ta e="T385" id="Seg_8946" s="T384">come-FUT-3PL</ta>
            <ta e="T386" id="Seg_8947" s="T385">bring-IMP.2SG</ta>
            <ta e="T387" id="Seg_8948" s="T386">money.[NOM.SG]</ta>
            <ta e="T389" id="Seg_8949" s="T388">we.NOM</ta>
            <ta e="T390" id="Seg_8950" s="T389">you.DAT</ta>
            <ta e="T391" id="Seg_8951" s="T390">give-FUT-1PL</ta>
            <ta e="T393" id="Seg_8952" s="T392">and</ta>
            <ta e="T394" id="Seg_8953" s="T393">self</ta>
            <ta e="T395" id="Seg_8954" s="T394">self-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T396" id="Seg_8955" s="T395">NEG</ta>
            <ta e="T397" id="Seg_8956" s="T396">give-FUT-3PL</ta>
            <ta e="T398" id="Seg_8957" s="T397">and</ta>
            <ta e="T399" id="Seg_8958" s="T398">then</ta>
            <ta e="T401" id="Seg_8959" s="T400">money.[NOM.SG]</ta>
            <ta e="T407" id="Seg_8960" s="T406">I.NOM</ta>
            <ta e="T408" id="Seg_8961" s="T407">now</ta>
            <ta e="T409" id="Seg_8962" s="T408">NEG.AUX-1SG</ta>
            <ta e="T410" id="Seg_8963" s="T409">NEG</ta>
            <ta e="T412" id="Seg_8964" s="T411">think-FUT-1SG</ta>
            <ta e="T413" id="Seg_8965" s="T412">otherwise</ta>
            <ta e="T414" id="Seg_8966" s="T413">horse-GEN</ta>
            <ta e="T415" id="Seg_8967" s="T414">head-NOM/GEN.3SG</ta>
            <ta e="T416" id="Seg_8968" s="T415">one.should</ta>
            <ta e="T417" id="Seg_8969" s="T416">big.[NOM.SG]</ta>
            <ta e="T421" id="Seg_8970" s="T420">you.PL.NOM</ta>
            <ta e="T422" id="Seg_8971" s="T421">think-DUR.[3SG]</ta>
            <ta e="T428" id="Seg_8972" s="T427">soon</ta>
            <ta e="T429" id="Seg_8973" s="T428">go-FUT-1SG</ta>
            <ta e="T430" id="Seg_8974" s="T429">cow-PL</ta>
            <ta e="T432" id="Seg_8975" s="T431">drive-FUT-1SG</ta>
            <ta e="T433" id="Seg_8976" s="T432">and</ta>
            <ta e="T434" id="Seg_8977" s="T433">run-FUT-1SG</ta>
            <ta e="T436" id="Seg_8978" s="T435">go-PST-1SG</ta>
            <ta e="T437" id="Seg_8979" s="T436">and</ta>
            <ta e="T438" id="Seg_8980" s="T437">this-PL</ta>
            <ta e="T439" id="Seg_8981" s="T438">run-MOM-PST-3PL</ta>
            <ta e="T440" id="Seg_8982" s="T439">where.to=INDEF</ta>
            <ta e="T444" id="Seg_8983" s="T443">tractor.[NOM.SG]</ta>
            <ta e="T445" id="Seg_8984" s="T444">PTCL</ta>
            <ta e="T446" id="Seg_8985" s="T445">tree-PL</ta>
            <ta e="T447" id="Seg_8986" s="T446">bring-%%-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_8987" s="T448">NEG</ta>
            <ta e="T450" id="Seg_8988" s="T449">know-1SG</ta>
            <ta e="T451" id="Seg_8989" s="T450">how.much</ta>
            <ta e="T452" id="Seg_8990" s="T451">bring-%%-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_8991" s="T455">very</ta>
            <ta e="T457" id="Seg_8992" s="T456">many</ta>
            <ta e="T458" id="Seg_8993" s="T457">tree-PL</ta>
            <ta e="T460" id="Seg_8994" s="T459">and</ta>
            <ta e="T461" id="Seg_8995" s="T460">and</ta>
            <ta e="T462" id="Seg_8996" s="T461">thick</ta>
            <ta e="T463" id="Seg_8997" s="T462">thick</ta>
            <ta e="T464" id="Seg_8998" s="T463">very</ta>
            <ta e="T468" id="Seg_8999" s="T467">many</ta>
            <ta e="T470" id="Seg_9000" s="T469">work-INF.LAT</ta>
            <ta e="T471" id="Seg_9001" s="T470">see-PST-1SG</ta>
            <ta e="T472" id="Seg_9002" s="T471">and</ta>
            <ta e="T473" id="Seg_9003" s="T472">few</ta>
            <ta e="T474" id="Seg_9004" s="T473">see-DUR.PST-1SG</ta>
            <ta e="T478" id="Seg_9005" s="T477">chief.[NOM.SG]</ta>
            <ta e="T479" id="Seg_9006" s="T478">very</ta>
            <ta e="T480" id="Seg_9007" s="T479">beautiful.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9008" s="T481">PTCL</ta>
            <ta e="T483" id="Seg_9009" s="T482">what.[NOM.SG]</ta>
            <ta e="T484" id="Seg_9010" s="T483">I.LAT</ta>
            <ta e="T485" id="Seg_9011" s="T484">give-FACT-PST.[3SG]</ta>
            <ta e="T486" id="Seg_9012" s="T485">PTCL</ta>
            <ta e="T487" id="Seg_9013" s="T486">what.[NOM.SG]</ta>
            <ta e="T491" id="Seg_9014" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_9015" s="T491">I.LAT</ta>
            <ta e="T494" id="Seg_9016" s="T493">show-PST.[3SG]</ta>
            <ta e="T501" id="Seg_9017" s="T500">become-FUT-3SG</ta>
            <ta e="T502" id="Seg_9018" s="T501">day.[NOM.SG]</ta>
            <ta e="T503" id="Seg_9019" s="T502">become-FUT-3SG</ta>
            <ta e="T504" id="Seg_9020" s="T503">eat-INF.LAT</ta>
            <ta e="T508" id="Seg_9021" s="T507">today</ta>
            <ta e="T509" id="Seg_9022" s="T508">Saturday</ta>
            <ta e="T510" id="Seg_9023" s="T509">one.should</ta>
            <ta e="T511" id="Seg_9024" s="T510">sauna.[NOM.SG]</ta>
            <ta e="T512" id="Seg_9025" s="T511">burn-INF.LAT</ta>
            <ta e="T513" id="Seg_9026" s="T512">water.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9027" s="T513">carry-INF.LAT</ta>
            <ta e="T516" id="Seg_9028" s="T515">then</ta>
            <ta e="T517" id="Seg_9029" s="T516">burn-FUT-1SG</ta>
            <ta e="T519" id="Seg_9030" s="T518">wash.oneself-IPFVZ-INF.LAT</ta>
            <ta e="T520" id="Seg_9031" s="T519">one.should</ta>
            <ta e="T522" id="Seg_9032" s="T521">otherwise</ta>
            <ta e="T523" id="Seg_9033" s="T522">whole</ta>
            <ta e="T524" id="Seg_9034" s="T523">week.[NOM.SG]</ta>
            <ta e="T525" id="Seg_9035" s="T524">NEG</ta>
            <ta e="T526" id="Seg_9036" s="T525">wash.oneself-IPFVZ-PST-1SG</ta>
            <ta e="T530" id="Seg_9037" s="T529">house-EP-NOM/GEN/ACC.1SG-LAT</ta>
            <ta e="T531" id="Seg_9038" s="T530">paper.[NOM.SG]</ta>
            <ta e="T532" id="Seg_9039" s="T531">wait-PRS-1SG</ta>
            <ta e="T533" id="Seg_9040" s="T532">wait-PRS-1SG</ta>
            <ta e="T535" id="Seg_9041" s="T534">always</ta>
            <ta e="T536" id="Seg_9042" s="T535">one.should</ta>
            <ta e="T537" id="Seg_9043" s="T536">NEG.EX.[3SG]</ta>
            <ta e="T538" id="Seg_9044" s="T537">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T539" id="Seg_9045" s="T538">NEG</ta>
            <ta e="T540" id="Seg_9046" s="T539">write-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_9047" s="T541">and</ta>
            <ta e="T543" id="Seg_9048" s="T542">paper.[NOM.SG]</ta>
            <ta e="T544" id="Seg_9049" s="T543">NEG</ta>
            <ta e="T545" id="Seg_9050" s="T544">come-PRS.[3SG]</ta>
            <ta e="T548" id="Seg_9051" s="T547">today</ta>
            <ta e="T549" id="Seg_9052" s="T548">evening-LOC.ADV</ta>
            <ta e="T550" id="Seg_9053" s="T549">dream-LAT/LOC.1SG</ta>
            <ta e="T551" id="Seg_9054" s="T550">see-PST-1SG</ta>
            <ta e="T553" id="Seg_9055" s="T552">fight-DUR.PST-1SG</ta>
            <ta e="T554" id="Seg_9056" s="T553">today</ta>
            <ta e="T555" id="Seg_9057" s="T554">paper.[NOM.SG]</ta>
            <ta e="T556" id="Seg_9058" s="T555">come-FUT-3SG</ta>
            <ta e="T560" id="Seg_9059" s="T559">I.NOM</ta>
            <ta e="T561" id="Seg_9060" s="T560">go-PST-1SG</ta>
            <ta e="T562" id="Seg_9061" s="T561">Krasnoyarsk-LAT</ta>
            <ta e="T1186" id="Seg_9062" s="T562">egg-PL</ta>
            <ta e="T564" id="Seg_9063" s="T563">bring-PST-1SG</ta>
            <ta e="T567" id="Seg_9064" s="T566">hundred</ta>
            <ta e="T568" id="Seg_9065" s="T567">egg.[NOM.SG]</ta>
            <ta e="T570" id="Seg_9066" s="T569">then</ta>
            <ta e="T571" id="Seg_9067" s="T570">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T572" id="Seg_9068" s="T571">I.NOM-INS</ta>
            <ta e="T573" id="Seg_9069" s="T572">go-PST.[3SG]</ta>
            <ta e="T575" id="Seg_9070" s="T574">this.[NOM.SG]</ta>
            <ta e="T576" id="Seg_9071" s="T575">I.LAT</ta>
            <ta e="T577" id="Seg_9072" s="T576">bring-PST.[3SG]</ta>
            <ta e="T578" id="Seg_9073" s="T577">sit-PST.[3SG]</ta>
            <ta e="T579" id="Seg_9074" s="T578">sit-PST.[3SG]</ta>
            <ta e="T580" id="Seg_9075" s="T579">machine-3PL-INS</ta>
            <ta e="T582" id="Seg_9076" s="T581">I.NOM</ta>
            <ta e="T584" id="Seg_9077" s="T583">NEG</ta>
            <ta e="T585" id="Seg_9078" s="T584">can-PRS-1SG</ta>
            <ta e="T586" id="Seg_9079" s="T585">find-INF.LAT</ta>
            <ta e="T587" id="Seg_9080" s="T586">then</ta>
            <ta e="T588" id="Seg_9081" s="T587">find-PST-1SG</ta>
            <ta e="T589" id="Seg_9082" s="T588">niece-ACC</ta>
            <ta e="T591" id="Seg_9083" s="T590">and</ta>
            <ta e="T592" id="Seg_9084" s="T591">there</ta>
            <ta e="T593" id="Seg_9085" s="T592">go-IPFVZ-PST-1SG</ta>
            <ta e="T595" id="Seg_9086" s="T594">hence</ta>
            <ta e="T596" id="Seg_9087" s="T595">machine-INS</ta>
            <ta e="T598" id="Seg_9088" s="T597">and</ta>
            <ta e="T599" id="Seg_9089" s="T598">there</ta>
            <ta e="T600" id="Seg_9090" s="T599">bus-INS</ta>
            <ta e="T602" id="Seg_9091" s="T601">then</ta>
            <ta e="T603" id="Seg_9092" s="T602">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T604" id="Seg_9093" s="T603">road.[NOM.SG]</ta>
            <ta e="T606" id="Seg_9094" s="T605">money.[NOM.SG]</ta>
            <ta e="T607" id="Seg_9095" s="T606">road.[NOM.SG]</ta>
            <ta e="T608" id="Seg_9096" s="T607">then</ta>
            <ta e="T609" id="Seg_9097" s="T608">there</ta>
            <ta e="T610" id="Seg_9098" s="T609">come-PST-1SG</ta>
            <ta e="T614" id="Seg_9099" s="T613">then</ta>
            <ta e="T615" id="Seg_9100" s="T614">go-PST-1SG</ta>
            <ta e="T616" id="Seg_9101" s="T615">God-LAT</ta>
            <ta e="T618" id="Seg_9102" s="T617">God-EP-GEN</ta>
            <ta e="T619" id="Seg_9103" s="T618">pray-INF.LAT</ta>
            <ta e="T621" id="Seg_9104" s="T620">then</ta>
            <ta e="T622" id="Seg_9105" s="T621">go-PST-1PL</ta>
            <ta e="T623" id="Seg_9106" s="T622">niece-INS</ta>
            <ta e="T624" id="Seg_9107" s="T623">%%-LAT</ta>
            <ta e="T626" id="Seg_9108" s="T625">there</ta>
            <ta e="T628" id="Seg_9109" s="T627">sit.down-PST-1PL</ta>
            <ta e="T629" id="Seg_9110" s="T628">and</ta>
            <ta e="T630" id="Seg_9111" s="T629">speak-PST-1PL</ta>
            <ta e="T632" id="Seg_9112" s="T631">and</ta>
            <ta e="T633" id="Seg_9113" s="T632">then</ta>
            <ta e="T634" id="Seg_9114" s="T633">house-LAT/LOC.3SG</ta>
            <ta e="T635" id="Seg_9115" s="T634">come-PST-1SG</ta>
            <ta e="T639" id="Seg_9116" s="T638">niece.[NOM.SG]</ta>
            <ta e="T640" id="Seg_9117" s="T639">I.LAT</ta>
            <ta e="T641" id="Seg_9118" s="T640">tomato.[NOM.SG]</ta>
            <ta e="T642" id="Seg_9119" s="T641">many</ta>
            <ta e="T643" id="Seg_9120" s="T642">give-PST.[3SG]</ta>
            <ta e="T644" id="Seg_9121" s="T643">I.NOM</ta>
            <ta e="T1184" id="Seg_9122" s="T645">Aginskoe</ta>
            <ta e="T646" id="Seg_9123" s="T1184">settlement-LOC</ta>
            <ta e="T647" id="Seg_9124" s="T646">two.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9125" s="T647">day.[NOM.SG]</ta>
            <ta e="T649" id="Seg_9126" s="T648">sit-PST-1SG</ta>
            <ta e="T653" id="Seg_9127" s="T652">cow-PL</ta>
            <ta e="T654" id="Seg_9128" s="T653">come-PST-3PL</ta>
            <ta e="T655" id="Seg_9129" s="T654">house-LAT/LOC.3SG</ta>
            <ta e="T656" id="Seg_9130" s="T655">one.should</ta>
            <ta e="T657" id="Seg_9131" s="T656">this-ACC.PL</ta>
            <ta e="T658" id="Seg_9132" s="T657">corral-NOM/GEN/ACC.3SG</ta>
            <ta e="T659" id="Seg_9133" s="T658">drive-INF.LAT</ta>
            <ta e="T660" id="Seg_9134" s="T659">and</ta>
            <ta e="T662" id="Seg_9135" s="T661">%%-INF.LAT</ta>
            <ta e="T666" id="Seg_9136" s="T665">think-PST-1SG</ta>
            <ta e="T667" id="Seg_9137" s="T666">think-PST-1SG</ta>
            <ta e="T668" id="Seg_9138" s="T667">what=INDEF</ta>
            <ta e="T669" id="Seg_9139" s="T668">think-MOM-PST-1SG</ta>
            <ta e="T673" id="Seg_9140" s="T672">daughter-LAT</ta>
            <ta e="T674" id="Seg_9141" s="T673">go-PST-1SG</ta>
            <ta e="T675" id="Seg_9142" s="T674">bag.[NOM.SG]</ta>
            <ta e="T676" id="Seg_9143" s="T675">this.[NOM.SG]</ta>
            <ta e="T677" id="Seg_9144" s="T676">PTCL</ta>
            <ta e="T678" id="Seg_9145" s="T677">tear.off-MOM.[3SG]</ta>
            <ta e="T680" id="Seg_9146" s="T679">now</ta>
            <ta e="T681" id="Seg_9147" s="T680">sit-DUR-1SG</ta>
            <ta e="T682" id="Seg_9148" s="T681">and</ta>
            <ta e="T684" id="Seg_9149" s="T683">sew-DUR-1SG</ta>
            <ta e="T686" id="Seg_9150" s="T685">you.DAT</ta>
            <ta e="T687" id="Seg_9151" s="T686">come-PST-1SG</ta>
            <ta e="T688" id="Seg_9152" s="T687">come-PST-1SG</ta>
            <ta e="T689" id="Seg_9153" s="T688">%%</ta>
            <ta e="T690" id="Seg_9154" s="T689">NEG</ta>
            <ta e="T691" id="Seg_9155" s="T690">fall-MOM-PST-1SG</ta>
            <ta e="T696" id="Seg_9156" s="T695">hare</ta>
            <ta e="T698" id="Seg_9157" s="T697">hare.[NOM.SG]</ta>
            <ta e="T699" id="Seg_9158" s="T698">come-PRS.[3SG]</ta>
            <ta e="T700" id="Seg_9159" s="T699">come-PRS.[3SG]</ta>
            <ta e="T701" id="Seg_9160" s="T700">then</ta>
            <ta e="T702" id="Seg_9161" s="T701">grass.[NOM.SG]</ta>
            <ta e="T703" id="Seg_9162" s="T702">this-LAT</ta>
            <ta e="T705" id="Seg_9163" s="T704">nose.[NOM.SG]</ta>
            <ta e="T708" id="Seg_9164" s="T707">cut-MOM.PRS.[3SG]</ta>
            <ta e="T709" id="Seg_9165" s="T708">blood-NOM/GEN/ACC.3SG</ta>
            <ta e="T710" id="Seg_9166" s="T709">PTCL</ta>
            <ta e="T711" id="Seg_9167" s="T710">flow-DUR.[3SG]</ta>
            <ta e="T713" id="Seg_9168" s="T712">this.[NOM.SG]</ta>
            <ta e="T714" id="Seg_9169" s="T713">say-IPFVZ.[3SG]</ta>
            <ta e="T715" id="Seg_9170" s="T714">fire.[NOM.SG]</ta>
            <ta e="T717" id="Seg_9171" s="T716">burn-IMP.2SG.O</ta>
            <ta e="T718" id="Seg_9172" s="T717">this.[NOM.SG]</ta>
            <ta e="T719" id="Seg_9173" s="T718">grass.[NOM.SG]</ta>
            <ta e="T723" id="Seg_9174" s="T722">I.NOM</ta>
            <ta e="T724" id="Seg_9175" s="T723">many</ta>
            <ta e="T725" id="Seg_9176" s="T724">earth-ACC</ta>
            <ta e="T726" id="Seg_9177" s="T725">burn-INF.LAT</ta>
            <ta e="T727" id="Seg_9178" s="T726">then</ta>
            <ta e="T728" id="Seg_9179" s="T727">this.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9180" s="T728">water-LAT</ta>
            <ta e="T730" id="Seg_9181" s="T729">go-PST.[3SG]</ta>
            <ta e="T732" id="Seg_9182" s="T731">water.[NOM.SG]</ta>
            <ta e="T733" id="Seg_9183" s="T732">go-EP-IMP.2SG</ta>
            <ta e="T734" id="Seg_9184" s="T733">fire-ACC</ta>
            <ta e="T736" id="Seg_9185" s="T735">pour-IMP.2SG</ta>
            <ta e="T740" id="Seg_9186" s="T739">I.NOM</ta>
            <ta e="T741" id="Seg_9187" s="T740">many</ta>
            <ta e="T743" id="Seg_9188" s="T742">earth.[NOM.SG]</ta>
            <ta e="T744" id="Seg_9189" s="T743">pour-%%-INF.LAT</ta>
            <ta e="T745" id="Seg_9190" s="T744">so.that</ta>
            <ta e="T746" id="Seg_9191" s="T745">grass.[NOM.SG]</ta>
            <ta e="T747" id="Seg_9192" s="T746">grow-PST.[3SG]</ta>
            <ta e="T751" id="Seg_9193" s="T750">then</ta>
            <ta e="T752" id="Seg_9194" s="T751">this.[NOM.SG]</ta>
            <ta e="T753" id="Seg_9195" s="T752">go-PST.[3SG]</ta>
            <ta e="T755" id="Seg_9196" s="T754">moose.[NOM.SG]</ta>
            <ta e="T756" id="Seg_9197" s="T755">moose.[NOM.SG]</ta>
            <ta e="T757" id="Seg_9198" s="T756">go-EP-IMP.2SG</ta>
            <ta e="T758" id="Seg_9199" s="T757">PTCL</ta>
            <ta e="T759" id="Seg_9200" s="T758">water.[NOM.SG]</ta>
            <ta e="T761" id="Seg_9201" s="T760">drink-EP-IMP.2SG</ta>
            <ta e="T763" id="Seg_9202" s="T762">then</ta>
            <ta e="T764" id="Seg_9203" s="T763">moose.[NOM.SG]</ta>
            <ta e="T765" id="Seg_9204" s="T764">say-PST.[3SG]</ta>
            <ta e="T766" id="Seg_9205" s="T765">I.NOM</ta>
            <ta e="T767" id="Seg_9206" s="T766">foot-LAT/LOC.3SG</ta>
            <ta e="T768" id="Seg_9207" s="T767">many</ta>
            <ta e="T769" id="Seg_9208" s="T768">water.[NOM.SG]</ta>
            <ta e="T771" id="Seg_9209" s="T770">then</ta>
            <ta e="T772" id="Seg_9210" s="T771">this.[NOM.SG]</ta>
            <ta e="T774" id="Seg_9211" s="T773">go-PST.[3SG]</ta>
            <ta e="T775" id="Seg_9212" s="T774">woman-LAT</ta>
            <ta e="T776" id="Seg_9213" s="T775">woman.[NOM.SG]</ta>
            <ta e="T777" id="Seg_9214" s="T776">woman.[NOM.SG]</ta>
            <ta e="T778" id="Seg_9215" s="T777">go-EP-IMP.2SG</ta>
            <ta e="T779" id="Seg_9216" s="T778">tendon-PL</ta>
            <ta e="T780" id="Seg_9217" s="T779">foot-ABL.3SG</ta>
            <ta e="T781" id="Seg_9218" s="T780">take-IMP.2SG.O</ta>
            <ta e="T783" id="Seg_9219" s="T782">woman.[NOM.SG]</ta>
            <ta e="T784" id="Seg_9220" s="T783">NEG</ta>
            <ta e="T785" id="Seg_9221" s="T784">go-PST.[3SG]</ta>
            <ta e="T787" id="Seg_9222" s="T786">black-PL</ta>
            <ta e="T788" id="Seg_9223" s="T787">enough</ta>
            <ta e="T789" id="Seg_9224" s="T788">I.LAT</ta>
            <ta e="T793" id="Seg_9225" s="T792">this.[NOM.SG]</ta>
            <ta e="T794" id="Seg_9226" s="T793">go-PST.[3SG]</ta>
            <ta e="T795" id="Seg_9227" s="T794">woman-LAT</ta>
            <ta e="T796" id="Seg_9228" s="T795">woman.[NOM.SG]</ta>
            <ta e="T797" id="Seg_9229" s="T796">woman.[NOM.SG]</ta>
            <ta e="T798" id="Seg_9230" s="T797">take-IMP.2SG.O</ta>
            <ta e="T800" id="Seg_9231" s="T799">tendon-PL</ta>
            <ta e="T801" id="Seg_9232" s="T800">and</ta>
            <ta e="T802" id="Seg_9233" s="T801">this.[NOM.SG]</ta>
            <ta e="T803" id="Seg_9234" s="T802">say-IPFVZ.[3SG]</ta>
            <ta e="T804" id="Seg_9235" s="T803">I.NOM</ta>
            <ta e="T805" id="Seg_9236" s="T804">many</ta>
            <ta e="T806" id="Seg_9237" s="T805">be-PRS.[3SG]</ta>
            <ta e="T807" id="Seg_9238" s="T806">and</ta>
            <ta e="T808" id="Seg_9239" s="T807">black.[NOM.SG]</ta>
            <ta e="T809" id="Seg_9240" s="T808">I.LAT</ta>
            <ta e="T810" id="Seg_9241" s="T809">NEG</ta>
            <ta e="T811" id="Seg_9242" s="T810">one.needs</ta>
            <ta e="T815" id="Seg_9243" s="T814">then</ta>
            <ta e="T816" id="Seg_9244" s="T815">go-PST.[3SG]</ta>
            <ta e="T817" id="Seg_9245" s="T816">mouse-PL-LAT</ta>
            <ta e="T818" id="Seg_9246" s="T817">mouse.[NOM.SG]</ta>
            <ta e="T819" id="Seg_9247" s="T818">mouse.[NOM.SG]</ta>
            <ta e="T820" id="Seg_9248" s="T819">go-EP-IMP.2SG</ta>
            <ta e="T821" id="Seg_9249" s="T820">woman-GEN</ta>
            <ta e="T822" id="Seg_9250" s="T821">tendon-PL</ta>
            <ta e="T823" id="Seg_9251" s="T822">eat-IMP.2SG</ta>
            <ta e="T825" id="Seg_9252" s="T824">no</ta>
            <ta e="T827" id="Seg_9253" s="T826">we.NOM</ta>
            <ta e="T828" id="Seg_9254" s="T827">NEG.AUX-1SG</ta>
            <ta e="T829" id="Seg_9255" s="T828">go-CVB</ta>
            <ta e="T830" id="Seg_9256" s="T829">NEG</ta>
            <ta e="T831" id="Seg_9257" s="T830">go-FUT-1SG</ta>
            <ta e="T833" id="Seg_9258" s="T832">we.LAT</ta>
            <ta e="T834" id="Seg_9259" s="T833">enough</ta>
            <ta e="T835" id="Seg_9260" s="T834">earth-LOC</ta>
            <ta e="T836" id="Seg_9261" s="T835">root.PL-PL</ta>
            <ta e="T837" id="Seg_9262" s="T836">eat-INF.LAT</ta>
            <ta e="T841" id="Seg_9263" s="T840">then</ta>
            <ta e="T842" id="Seg_9264" s="T841">go-PST.[3SG]</ta>
            <ta e="T844" id="Seg_9265" s="T843">child-PL-LAT</ta>
            <ta e="T845" id="Seg_9266" s="T844">child-PL</ta>
            <ta e="T846" id="Seg_9267" s="T845">child-PL</ta>
            <ta e="T847" id="Seg_9268" s="T846">kill-DUR-2DU</ta>
            <ta e="T848" id="Seg_9269" s="T847">mouse-PL</ta>
            <ta e="T849" id="Seg_9270" s="T848">this-PL</ta>
            <ta e="T850" id="Seg_9271" s="T849">we.NOM</ta>
            <ta e="T852" id="Seg_9272" s="T851">self-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_9273" s="T852">play-DUR-3PL</ta>
            <ta e="T854" id="Seg_9274" s="T853">headstock-PL-INS</ta>
            <ta e="T858" id="Seg_9275" s="T857">you.PL.NOM</ta>
            <ta e="T859" id="Seg_9276" s="T858">house-LOC</ta>
            <ta e="T860" id="Seg_9277" s="T859">good</ta>
            <ta e="T861" id="Seg_9278" s="T860">live-INF.LAT</ta>
            <ta e="T863" id="Seg_9279" s="T862">only</ta>
            <ta e="T864" id="Seg_9280" s="T863">bread.[NOM.SG]</ta>
            <ta e="T865" id="Seg_9281" s="T864">where=INDEF</ta>
            <ta e="T866" id="Seg_9282" s="T865">NEG</ta>
            <ta e="T867" id="Seg_9283" s="T866">take-FUT-2SG</ta>
            <ta e="T869" id="Seg_9284" s="T868">eat-INF.LAT</ta>
            <ta e="T870" id="Seg_9285" s="T869">NEG.EX.[3SG]</ta>
            <ta e="T871" id="Seg_9286" s="T870">bread.[NOM.SG]</ta>
            <ta e="T875" id="Seg_9287" s="T874">shop-LAT/LOC.3SG</ta>
            <ta e="T876" id="Seg_9288" s="T875">what.[NOM.SG]</ta>
            <ta e="T877" id="Seg_9289" s="T876">NEG.EX.[3SG]</ta>
            <ta e="T878" id="Seg_9290" s="T877">what.[NOM.SG]=INDEF</ta>
            <ta e="T879" id="Seg_9291" s="T878">NEG</ta>
            <ta e="T880" id="Seg_9292" s="T879">take-FUT-2SG</ta>
            <ta e="T882" id="Seg_9293" s="T881">no</ta>
            <ta e="T883" id="Seg_9294" s="T882">go-EP-IMP.2SG</ta>
            <ta e="T884" id="Seg_9295" s="T883">there</ta>
            <ta e="T885" id="Seg_9296" s="T884">be-PRS.[3SG]</ta>
            <ta e="T886" id="Seg_9297" s="T885">sausage.[NOM.SG]</ta>
            <ta e="T887" id="Seg_9298" s="T886">take-IMP.2SG</ta>
            <ta e="T888" id="Seg_9299" s="T887">and</ta>
            <ta e="T889" id="Seg_9300" s="T888">then</ta>
            <ta e="T890" id="Seg_9301" s="T889">eat-FUT-2SG</ta>
            <ta e="T894" id="Seg_9302" s="T893">this.[NOM.SG]</ta>
            <ta e="T895" id="Seg_9303" s="T894">woman.[NOM.SG]</ta>
            <ta e="T896" id="Seg_9304" s="T895">very</ta>
            <ta e="T897" id="Seg_9305" s="T896">good</ta>
            <ta e="T898" id="Seg_9306" s="T897">sew-DUR.[3SG]</ta>
            <ta e="T899" id="Seg_9307" s="T898">PTCL</ta>
            <ta e="T900" id="Seg_9308" s="T899">beautiful.[NOM.SG]</ta>
            <ta e="T902" id="Seg_9309" s="T901">good</ta>
            <ta e="T903" id="Seg_9310" s="T902">look-INF.LAT</ta>
            <ta e="T907" id="Seg_9311" s="T906">this.[NOM.SG]</ta>
            <ta e="T908" id="Seg_9312" s="T907">woman.[NOM.SG]</ta>
            <ta e="T909" id="Seg_9313" s="T908">very</ta>
            <ta e="T910" id="Seg_9314" s="T909">many</ta>
            <ta e="T912" id="Seg_9315" s="T911">work-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_9316" s="T912">PTCL</ta>
            <ta e="T914" id="Seg_9317" s="T913">what.[NOM.SG]</ta>
            <ta e="T915" id="Seg_9318" s="T914">make-INF.LAT</ta>
            <ta e="T920" id="Seg_9319" s="T919">PTCL</ta>
            <ta e="T921" id="Seg_9320" s="T920">what.[NOM.SG]</ta>
            <ta e="T922" id="Seg_9321" s="T921">work-INF.LAT</ta>
            <ta e="T923" id="Seg_9322" s="T922">know-3SG.O</ta>
            <ta e="T927" id="Seg_9323" s="T926">there</ta>
            <ta e="T928" id="Seg_9324" s="T927">woman-GEN</ta>
            <ta e="T930" id="Seg_9325" s="T929">relative-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T931" id="Seg_9326" s="T930">few</ta>
            <ta e="T932" id="Seg_9327" s="T931">live-%%-PST.[3SG]</ta>
            <ta e="T933" id="Seg_9328" s="T932">only</ta>
            <ta e="T934" id="Seg_9329" s="T933">one.[NOM.SG]</ta>
            <ta e="T935" id="Seg_9330" s="T934">brother-NOM/GEN.3SG</ta>
            <ta e="T939" id="Seg_9331" s="T938">and</ta>
            <ta e="T940" id="Seg_9332" s="T939">sister-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_9333" s="T940">die-RES-PST.[3SG]</ta>
            <ta e="T945" id="Seg_9334" s="T944">sew-MOM-PST-1SG</ta>
            <ta e="T946" id="Seg_9335" s="T945">PTCL</ta>
            <ta e="T947" id="Seg_9336" s="T946">now</ta>
            <ta e="T948" id="Seg_9337" s="T947">NEG.EX.[3SG]</ta>
            <ta e="T949" id="Seg_9338" s="T948">sew-INF.LAT</ta>
            <ta e="T953" id="Seg_9339" s="T952">we.NOM</ta>
            <ta e="T954" id="Seg_9340" s="T953">%%</ta>
            <ta e="T955" id="Seg_9341" s="T954">go-PST-1PL</ta>
            <ta e="T956" id="Seg_9342" s="T955">water.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9343" s="T956">carry-INF.LAT</ta>
            <ta e="T958" id="Seg_9344" s="T957">and</ta>
            <ta e="T959" id="Seg_9345" s="T958">there</ta>
            <ta e="T960" id="Seg_9346" s="T959">water.[NOM.SG]</ta>
            <ta e="T961" id="Seg_9347" s="T960">be-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_9348" s="T962">three.[NOM.SG]</ta>
            <ta e="T965" id="Seg_9349" s="T964">three.[NOM.SG]</ta>
            <ta e="T966" id="Seg_9350" s="T965">bucket.[NOM.SG]</ta>
            <ta e="T967" id="Seg_9351" s="T966">bring-PST-1PL</ta>
            <ta e="T968" id="Seg_9352" s="T967">and</ta>
            <ta e="T969" id="Seg_9353" s="T968">enough</ta>
            <ta e="T970" id="Seg_9354" s="T969">now</ta>
            <ta e="T974" id="Seg_9355" s="T973">forest-LOC</ta>
            <ta e="T975" id="Seg_9356" s="T974">come-PRS-1SG</ta>
            <ta e="T976" id="Seg_9357" s="T975">forest.[NOM.SG]</ta>
            <ta e="T977" id="Seg_9358" s="T976">sing-DUR-1SG</ta>
            <ta e="T979" id="Seg_9359" s="T978">tree-LOC</ta>
            <ta e="T980" id="Seg_9360" s="T979">come-PRS-1SG</ta>
            <ta e="T981" id="Seg_9361" s="T980">tree.[NOM.SG]</ta>
            <ta e="T982" id="Seg_9362" s="T981">sing-DUR-1SG</ta>
            <ta e="T984" id="Seg_9363" s="T983">steppe.[NOM.SG]</ta>
            <ta e="T985" id="Seg_9364" s="T984">come-PRS-1SG</ta>
            <ta e="T986" id="Seg_9365" s="T985">steppe.[NOM.SG]</ta>
            <ta e="T987" id="Seg_9366" s="T986">sing-DUR-1SG</ta>
            <ta e="T989" id="Seg_9367" s="T988">mountain.[NOM.SG]</ta>
            <ta e="T990" id="Seg_9368" s="T989">come-PRS-1SG</ta>
            <ta e="T991" id="Seg_9369" s="T990">mountain.[NOM.SG]</ta>
            <ta e="T992" id="Seg_9370" s="T991">sing-DUR-1SG</ta>
            <ta e="T994" id="Seg_9371" s="T993">Dyala-ACC</ta>
            <ta e="T995" id="Seg_9372" s="T994">come-PRS.[3SG]</ta>
            <ta e="T996" id="Seg_9373" s="T995">Dyala-ACC</ta>
            <ta e="T998" id="Seg_9374" s="T997">sing-DUR-1SG</ta>
            <ta e="T1000" id="Seg_9375" s="T999">house-LOC</ta>
            <ta e="T1001" id="Seg_9376" s="T1000">sit-DUR.[3SG]</ta>
            <ta e="T1002" id="Seg_9377" s="T1001">house.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_9378" s="T1002">sing-DUR-1SG</ta>
            <ta e="T1007" id="Seg_9379" s="T1006">we.NOM</ta>
            <ta e="T1008" id="Seg_9380" s="T1007">I.GEN</ta>
            <ta e="T1009" id="Seg_9381" s="T1008">house-LAT/LOC.1SG</ta>
            <ta e="T1010" id="Seg_9382" s="T1009">very</ta>
            <ta e="T1011" id="Seg_9383" s="T1010">mouse.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_9384" s="T1011">many</ta>
            <ta e="T1013" id="Seg_9385" s="T1012">what.[NOM.SG]</ta>
            <ta e="T1014" id="Seg_9386" s="T1013">INDEF</ta>
            <ta e="T1015" id="Seg_9387" s="T1014">PTCL</ta>
            <ta e="T1016" id="Seg_9388" s="T1015">eat-PRS-3PL</ta>
            <ta e="T1018" id="Seg_9389" s="T1017">and</ta>
            <ta e="T1019" id="Seg_9390" s="T1018">cat.[NOM.SG]</ta>
            <ta e="T1020" id="Seg_9391" s="T1019">NEG.EX.[3SG]</ta>
            <ta e="T1022" id="Seg_9392" s="T1021">go-EP-IMP.2SG</ta>
            <ta e="T1023" id="Seg_9393" s="T1022">%%-LAT</ta>
            <ta e="T1024" id="Seg_9394" s="T1023">this.[NOM.SG]</ta>
            <ta e="T1025" id="Seg_9395" s="T1024">there</ta>
            <ta e="T1026" id="Seg_9396" s="T1025">two.[NOM.SG]</ta>
            <ta e="T1028" id="Seg_9397" s="T1027">cat.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_9398" s="T1028">one.[NOM.SG]</ta>
            <ta e="T1030" id="Seg_9399" s="T1029">white.[NOM.SG]</ta>
            <ta e="T1031" id="Seg_9400" s="T1030">one.[NOM.SG]</ta>
            <ta e="T1032" id="Seg_9401" s="T1031">red.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_9402" s="T1033">this.[NOM.SG]</ta>
            <ta e="T1035" id="Seg_9403" s="T1034">give-FUT-3SG</ta>
            <ta e="T1036" id="Seg_9404" s="T1035">you.DAT</ta>
            <ta e="T1038" id="Seg_9405" s="T1037">and</ta>
            <ta e="T1039" id="Seg_9406" s="T1038">this.[NOM.SG]</ta>
            <ta e="T1040" id="Seg_9407" s="T1039">snow.[NOM.SG]</ta>
            <ta e="T1041" id="Seg_9408" s="T1040">I.LAT</ta>
            <ta e="T1042" id="Seg_9409" s="T1041">take-PST.[3SG]</ta>
            <ta e="T1043" id="Seg_9410" s="T1042">and</ta>
            <ta e="T1181" id="Seg_9411" s="T1043">go-CVB</ta>
            <ta e="T1044" id="Seg_9412" s="T1181">disappear-PST.[3SG]</ta>
            <ta e="T1045" id="Seg_9413" s="T1044">this.[NOM.SG]</ta>
            <ta e="T1046" id="Seg_9414" s="T1045">this-ACC</ta>
            <ta e="T1047" id="Seg_9415" s="T1046">NEG</ta>
            <ta e="T1048" id="Seg_9416" s="T1047">let-SEM-PST.[3SG]</ta>
            <ta e="T1050" id="Seg_9417" s="T1049">else</ta>
            <ta e="T1051" id="Seg_9418" s="T1050">NEG</ta>
            <ta e="T1052" id="Seg_9419" s="T1051">come-PRS.[3SG]</ta>
            <ta e="T1056" id="Seg_9420" s="T1055">I.NOM</ta>
            <ta e="T1057" id="Seg_9421" s="T1056">sleep-INF.LAT</ta>
            <ta e="T1058" id="Seg_9422" s="T1057">lie.down-PST-1SG</ta>
            <ta e="T1060" id="Seg_9423" s="T1059">and</ta>
            <ta e="T1061" id="Seg_9424" s="T1060">there</ta>
            <ta e="T1062" id="Seg_9425" s="T1061">very</ta>
            <ta e="T1063" id="Seg_9426" s="T1062">stinking.[NOM.SG]</ta>
            <ta e="T1064" id="Seg_9427" s="T1063">see-PRS-1SG</ta>
            <ta e="T1065" id="Seg_9428" s="T1064">shit.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_9429" s="T1066">many</ta>
            <ta e="T1068" id="Seg_9430" s="T1067">lie-DUR.[3SG]</ta>
            <ta e="T1072" id="Seg_9431" s="T1071">morning-LOC.ADV</ta>
            <ta e="T1073" id="Seg_9432" s="T1072">get.up-PST-1SG</ta>
            <ta e="T1074" id="Seg_9433" s="T1073">knee-LAT</ta>
            <ta e="T1075" id="Seg_9434" s="T1074">stand-PST-1SG</ta>
            <ta e="T1077" id="Seg_9435" s="T1076">see-PRS-1SG</ta>
            <ta e="T1078" id="Seg_9436" s="T1077">PTCL</ta>
            <ta e="T1079" id="Seg_9437" s="T1078">there</ta>
            <ta e="T1080" id="Seg_9438" s="T1079">ten.[NOM.SG]</ta>
            <ta e="T1081" id="Seg_9439" s="T1080">heap.PL</ta>
            <ta e="T1082" id="Seg_9440" s="T1081">lie-DUR.[3SG]</ta>
            <ta e="T1086" id="Seg_9441" s="T1085">then</ta>
            <ta e="T1087" id="Seg_9442" s="T1086">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T1088" id="Seg_9443" s="T1087">bring-EP-RES-1SG</ta>
            <ta e="T1089" id="Seg_9444" s="T1088">outwards</ta>
            <ta e="T1090" id="Seg_9445" s="T1089">cup-LOC</ta>
            <ta e="T1091" id="Seg_9446" s="T1090">cup-INS</ta>
            <ta e="T1092" id="Seg_9447" s="T1091">close-PRS-1SG</ta>
            <ta e="T1093" id="Seg_9448" s="T1092">so.that</ta>
            <ta e="T1094" id="Seg_9449" s="T1093">mouse-PL</ta>
            <ta e="T1095" id="Seg_9450" s="T1094">NEG</ta>
            <ta e="T1096" id="Seg_9451" s="T1095">eat-PST-3PL</ta>
            <ta e="T1098" id="Seg_9452" s="T1097">and</ta>
            <ta e="T1099" id="Seg_9453" s="T1098">butter.[NOM.SG]</ta>
            <ta e="T1100" id="Seg_9454" s="T1099">churn-LAT</ta>
            <ta e="T1101" id="Seg_9455" s="T1100">also</ta>
            <ta e="T1103" id="Seg_9456" s="T1102">close-RES-1SG</ta>
            <ta e="T1104" id="Seg_9457" s="T1103">so.that</ta>
            <ta e="T1105" id="Seg_9458" s="T1104">mouse-PL</ta>
            <ta e="T1106" id="Seg_9459" s="T1105">NEG</ta>
            <ta e="T1107" id="Seg_9460" s="T1106">eat-PST-3PL</ta>
            <ta e="T1111" id="Seg_9461" s="T1110">and</ta>
            <ta e="T1113" id="Seg_9462" s="T1112">so</ta>
            <ta e="T1114" id="Seg_9463" s="T1113">we.LAT</ta>
            <ta e="T1115" id="Seg_9464" s="T1114">two.[NOM.SG]</ta>
            <ta e="T1116" id="Seg_9465" s="T1115">be-PRS.[3SG]</ta>
            <ta e="T1117" id="Seg_9466" s="T1116">cat.[NOM.SG]</ta>
            <ta e="T1118" id="Seg_9467" s="T1117">bring-FUT-1SG</ta>
            <ta e="T1119" id="Seg_9468" s="T1118">one.[NOM.SG]</ta>
            <ta e="T1121" id="Seg_9469" s="T1120">what.kind</ta>
            <ta e="T1122" id="Seg_9470" s="T1121">you.DAT</ta>
            <ta e="T1123" id="Seg_9471" s="T1122">bring-INF.LAT</ta>
            <ta e="T1124" id="Seg_9472" s="T1123">white</ta>
            <ta e="T1125" id="Seg_9473" s="T1124">or</ta>
            <ta e="T1126" id="Seg_9474" s="T1125">red.[NOM.SG]</ta>
            <ta e="T1128" id="Seg_9475" s="T1127">well</ta>
            <ta e="T1129" id="Seg_9476" s="T1128">bring-IMP.2SG</ta>
            <ta e="T1130" id="Seg_9477" s="T1129">red.[NOM.SG]</ta>
            <ta e="T1134" id="Seg_9478" s="T1133">sauna-LAT</ta>
            <ta e="T1135" id="Seg_9479" s="T1134">come-PRS.[3SG]</ta>
            <ta e="T1136" id="Seg_9480" s="T1135">there</ta>
            <ta e="T1137" id="Seg_9481" s="T1136">very</ta>
            <ta e="T1138" id="Seg_9482" s="T1137">water.[NOM.SG]</ta>
            <ta e="T1139" id="Seg_9483" s="T1138">many</ta>
            <ta e="T1141" id="Seg_9484" s="T1140">warm.[NOM.SG]</ta>
            <ta e="T1142" id="Seg_9485" s="T1141">water.[NOM.SG]</ta>
            <ta e="T1143" id="Seg_9486" s="T1142">be-PRS.[3SG]</ta>
            <ta e="T1144" id="Seg_9487" s="T1143">cold.[NOM.SG]</ta>
            <ta e="T1145" id="Seg_9488" s="T1144">soap.[NOM.SG]</ta>
            <ta e="T1146" id="Seg_9489" s="T1145">be-PRS.[3SG]</ta>
            <ta e="T1148" id="Seg_9490" s="T1147">warm.[NOM.SG]</ta>
            <ta e="T1149" id="Seg_9491" s="T1148">warm-FACT-FUT-2SG</ta>
            <ta e="T1150" id="Seg_9492" s="T1149">PTCL</ta>
            <ta e="T1154" id="Seg_9493" s="T1153">nose-NOM/GEN/ACC.2SG</ta>
            <ta e="T1155" id="Seg_9494" s="T1154">NEG</ta>
            <ta e="T1156" id="Seg_9495" s="T1155">hurt-PTCP.[NOM.SG]</ta>
            <ta e="T1157" id="Seg_9496" s="T1156">and</ta>
            <ta e="T1158" id="Seg_9497" s="T1157">%%-NOM/GEN/ACC.3PL</ta>
            <ta e="T1159" id="Seg_9498" s="T1158">flow-PTCP-PL</ta>
            <ta e="T1163" id="Seg_9499" s="T1162">that.[NOM.SG]</ta>
            <ta e="T1164" id="Seg_9500" s="T1163">patch.[NOM.SG]</ta>
            <ta e="T1165" id="Seg_9501" s="T1164">and</ta>
            <ta e="T1166" id="Seg_9502" s="T1165">where.to</ta>
            <ta e="T1167" id="Seg_9503" s="T1166">this-ACC</ta>
            <ta e="T1168" id="Seg_9504" s="T1167">patch.[NOM.SG]</ta>
            <ta e="T1170" id="Seg_9505" s="T1169">ass-LAT/LOC.3SG</ta>
            <ta e="T1171" id="Seg_9506" s="T1170">seat-INF.LAT</ta>
            <ta e="T1172" id="Seg_9507" s="T1171">isn_t.it</ta>
            <ta e="T1174" id="Seg_9508" s="T1173">throw.away-%%-IMP.2SG</ta>
            <ta e="T1175" id="Seg_9509" s="T1174">NEG</ta>
            <ta e="T1176" id="Seg_9510" s="T1175">one.needs</ta>
            <ta e="T1177" id="Seg_9511" s="T1176">I.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_9512" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_9513" s="T2">вчера</ta>
            <ta e="T4" id="Seg_9514" s="T3">пойти-PST-1SG</ta>
            <ta e="T5" id="Seg_9515" s="T4">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T7" id="Seg_9516" s="T6">я.NOM</ta>
            <ta e="T8" id="Seg_9517" s="T7">сказать-PST-1SG</ta>
            <ta e="T9" id="Seg_9518" s="T8">NEG.AUX-IMP.2SG</ta>
            <ta e="T10" id="Seg_9519" s="T9">сердиться-CNG</ta>
            <ta e="T11" id="Seg_9520" s="T10">я.LAT</ta>
            <ta e="T14" id="Seg_9521" s="T13">шить-INF.LAT</ta>
            <ta e="T16" id="Seg_9522" s="T15">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T17" id="Seg_9523" s="T16">NEG</ta>
            <ta e="T18" id="Seg_9524" s="T17">пойти-PST.[3SG]</ta>
            <ta e="T19" id="Seg_9525" s="T18">и</ta>
            <ta e="T20" id="Seg_9526" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_9527" s="T20">NEG</ta>
            <ta e="T22" id="Seg_9528" s="T21">пойти-PST-1SG</ta>
            <ta e="T24" id="Seg_9529" s="T23">этот-PL</ta>
            <ta e="T25" id="Seg_9530" s="T24">кровать-LOC</ta>
            <ta e="T26" id="Seg_9531" s="T25">лежать-DUR-3PL</ta>
            <ta e="T27" id="Seg_9532" s="T26">обнимать-DUR-3PL</ta>
            <ta e="T29" id="Seg_9533" s="T28">Коля.[NOM.SG]</ta>
            <ta e="T30" id="Seg_9534" s="T29">Дуня-3SG-COM</ta>
            <ta e="T32" id="Seg_9535" s="T31">целовать-DUR-3PL</ta>
            <ta e="T34" id="Seg_9536" s="T33">смеяться-DUR-3PL</ta>
            <ta e="T36" id="Seg_9537" s="T35">сказать-IPFVZ-3PL</ta>
            <ta e="T37" id="Seg_9538" s="T36">бабушка.[NOM.SG]</ta>
            <ta e="T38" id="Seg_9539" s="T37">съесть-EP-IMP.2SG</ta>
            <ta e="T39" id="Seg_9540" s="T38">сидеть-IMP.2SG</ta>
            <ta e="T41" id="Seg_9541" s="T40">водка.[NOM.SG]</ta>
            <ta e="T42" id="Seg_9542" s="T41">пить-EP-IMP.2SG</ta>
            <ta e="T44" id="Seg_9543" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_9544" s="T44">сказать-IPFVZ-1SG</ta>
            <ta e="T46" id="Seg_9545" s="T45">NEG</ta>
            <ta e="T47" id="Seg_9546" s="T46">пить-FUT-1SG</ta>
            <ta e="T49" id="Seg_9547" s="T48">и</ta>
            <ta e="T50" id="Seg_9548" s="T49">тогда</ta>
            <ta e="T1178" id="Seg_9549" s="T50">пойти-CVB</ta>
            <ta e="T51" id="Seg_9550" s="T1178">исчезнуть-PST-1SG</ta>
            <ta e="T52" id="Seg_9551" s="T51">дом-LAT/LOC.1SG</ta>
            <ta e="T56" id="Seg_9552" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_9553" s="T56">сегодня</ta>
            <ta e="T58" id="Seg_9554" s="T57">женщина-LAT</ta>
            <ta e="T59" id="Seg_9555" s="T58">пойти-PST-1SG</ta>
            <ta e="T60" id="Seg_9556" s="T59">этот.[NOM.SG]</ta>
            <ta e="T61" id="Seg_9557" s="T60">болеть-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_9558" s="T62">этот.[NOM.SG]</ta>
            <ta e="T64" id="Seg_9559" s="T63">сказать-IPFVZ.[3SG]</ta>
            <ta e="T65" id="Seg_9560" s="T64">что.[NOM.SG]</ta>
            <ta e="T66" id="Seg_9561" s="T65">долго</ta>
            <ta e="T67" id="Seg_9562" s="T66">NEG</ta>
            <ta e="T68" id="Seg_9563" s="T67">прийти-PST-1SG</ta>
            <ta e="T69" id="Seg_9564" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_9565" s="T69">PTCL</ta>
            <ta e="T71" id="Seg_9566" s="T70">сушить-%%-PRS-1SG</ta>
            <ta e="T72" id="Seg_9567" s="T71">ты.NOM</ta>
            <ta e="T73" id="Seg_9568" s="T72">всегда</ta>
            <ta e="T74" id="Seg_9569" s="T73">сушить-%%-PRS-2SG</ta>
            <ta e="T75" id="Seg_9570" s="T74">а</ta>
            <ta e="T76" id="Seg_9571" s="T75">умереть-INF.LAT</ta>
            <ta e="T77" id="Seg_9572" s="T76">NEG</ta>
            <ta e="T78" id="Seg_9573" s="T77">мочь-PRS-2SG</ta>
            <ta e="T80" id="Seg_9574" s="T79">ну</ta>
            <ta e="T81" id="Seg_9575" s="T80">умереть-IMP.2SG</ta>
            <ta e="T83" id="Seg_9576" s="T82">я.LAT</ta>
            <ta e="T84" id="Seg_9577" s="T83">что.[NOM.SG]</ta>
            <ta e="T86" id="Seg_9578" s="T85">что.[NOM.SG]</ta>
            <ta e="T87" id="Seg_9579" s="T86">я.NOM</ta>
            <ta e="T88" id="Seg_9580" s="T87">ты.NOM</ta>
            <ta e="T89" id="Seg_9581" s="T88">край-LAT/LOC.3SG</ta>
            <ta e="T90" id="Seg_9582" s="T89">сидеть-DUR-FUT-1SG</ta>
            <ta e="T91" id="Seg_9583" s="T90">что.ли</ta>
            <ta e="T93" id="Seg_9584" s="T92">я.NOM</ta>
            <ta e="T1185" id="Seg_9585" s="T93">ведь</ta>
            <ta e="T94" id="Seg_9586" s="T1185">люди.[NOM.SG]</ta>
            <ta e="T95" id="Seg_9587" s="T94">жить-DUR-3PL</ta>
            <ta e="T97" id="Seg_9588" s="T96">там</ta>
            <ta e="T98" id="Seg_9589" s="T97">ждать-DUR-3PL</ta>
            <ta e="T99" id="Seg_9590" s="T98">я.LAT</ta>
            <ta e="T101" id="Seg_9591" s="T100">говорить-INF.LAT</ta>
            <ta e="T102" id="Seg_9592" s="T101">надо</ta>
            <ta e="T104" id="Seg_9593" s="T103">а</ta>
            <ta e="T105" id="Seg_9594" s="T104">ты.NOM</ta>
            <ta e="T106" id="Seg_9595" s="T105">край-LAT/LOC.3SG</ta>
            <ta e="T107" id="Seg_9596" s="T106">сидеть-IMP.2SG</ta>
            <ta e="T111" id="Seg_9597" s="T110">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T113" id="Seg_9598" s="T112">два.[NOM.SG]</ta>
            <ta e="T114" id="Seg_9599" s="T113">день.[NOM.SG]</ta>
            <ta e="T115" id="Seg_9600" s="T114">быть-PST.[3SG]</ta>
            <ta e="T117" id="Seg_9601" s="T116">а</ta>
            <ta e="T118" id="Seg_9602" s="T117">ты.NOM</ta>
            <ta e="T119" id="Seg_9603" s="T118">край-LAT/LOC.3SG</ta>
            <ta e="T120" id="Seg_9604" s="T119">сидеть-IMP.2SG</ta>
            <ta e="T122" id="Seg_9605" s="T121">и</ta>
            <ta e="T123" id="Seg_9606" s="T122">люди.[NOM.SG]</ta>
            <ta e="T124" id="Seg_9607" s="T123">идти-PRS-3PL</ta>
            <ta e="T126" id="Seg_9608" s="T125">а</ta>
            <ta e="T127" id="Seg_9609" s="T126">этот.[NOM.SG]</ta>
            <ta e="T128" id="Seg_9610" s="T127">сказать-IPFVZ.[3SG]</ta>
            <ta e="T129" id="Seg_9611" s="T128">а</ta>
            <ta e="T130" id="Seg_9612" s="T129">бог.[NOM.SG]</ta>
            <ta e="T131" id="Seg_9613" s="T130">этот-PL-COM</ta>
            <ta e="T133" id="Seg_9614" s="T132">а</ta>
            <ta e="T134" id="Seg_9615" s="T133">ты.NOM-COM</ta>
            <ta e="T135" id="Seg_9616" s="T134">JUSS</ta>
            <ta e="T136" id="Seg_9617" s="T135">бог.[NOM.SG]</ta>
            <ta e="T138" id="Seg_9618" s="T137">умереть-IMP.2SG</ta>
            <ta e="T139" id="Seg_9619" s="T138">умереть-IMP.2SG</ta>
            <ta e="T143" id="Seg_9620" s="T142">тогда</ta>
            <ta e="T144" id="Seg_9621" s="T143">этот.[NOM.SG]</ta>
            <ta e="T145" id="Seg_9622" s="T144">остаться-DUR.[3SG]</ta>
            <ta e="T146" id="Seg_9623" s="T145">а</ta>
            <ta e="T147" id="Seg_9624" s="T146">я.NOM</ta>
            <ta e="T148" id="Seg_9625" s="T147">чум-LAT/LOC.1SG</ta>
            <ta e="T1179" id="Seg_9626" s="T148">пойти-CVB</ta>
            <ta e="T149" id="Seg_9627" s="T1179">исчезнуть-PST-1SG</ta>
            <ta e="T153" id="Seg_9628" s="T152">я.NOM</ta>
            <ta e="T1183" id="Seg_9629" s="T153">Агинское</ta>
            <ta e="T154" id="Seg_9630" s="T1183">поселение-LAT</ta>
            <ta e="T155" id="Seg_9631" s="T154">пойти-PST-1SG</ta>
            <ta e="T157" id="Seg_9632" s="T156">а</ta>
            <ta e="T158" id="Seg_9633" s="T157">этот.[NOM.SG]</ta>
            <ta e="T159" id="Seg_9634" s="T158">женщина.[NOM.SG]</ta>
            <ta e="T160" id="Seg_9635" s="T159">пойти-PST.[3SG]</ta>
            <ta e="T161" id="Seg_9636" s="T160">этот-GEN</ta>
            <ta e="T162" id="Seg_9637" s="T161">край-LAT/LOC.3SG</ta>
            <ta e="T163" id="Seg_9638" s="T162">там</ta>
            <ta e="T164" id="Seg_9639" s="T163">женщина.[NOM.SG]</ta>
            <ta e="T165" id="Seg_9640" s="T164">жить-DUR.[3SG]</ta>
            <ta e="T167" id="Seg_9641" s="T166">прийти-PST.[3SG]</ta>
            <ta e="T169" id="Seg_9642" s="T168">и</ta>
            <ta e="T1180" id="Seg_9643" s="T169">пойти-CVB</ta>
            <ta e="T170" id="Seg_9644" s="T1180">исчезнуть-PST.[3SG]</ta>
            <ta e="T172" id="Seg_9645" s="T171">я.NOM</ta>
            <ta e="T173" id="Seg_9646" s="T172">умереть-FUT-1SG</ta>
            <ta e="T174" id="Seg_9647" s="T173">а</ta>
            <ta e="T175" id="Seg_9648" s="T174">кто.[NOM.SG]</ta>
            <ta e="T176" id="Seg_9649" s="T175">я.LAT</ta>
            <ta e="T178" id="Seg_9650" s="T177">мыться-TR-FUT-3SG</ta>
            <ta e="T180" id="Seg_9651" s="T179">этот-PL</ta>
            <ta e="T181" id="Seg_9652" s="T180">PTCL</ta>
            <ta e="T182" id="Seg_9653" s="T181">смеяться-PST-3PL</ta>
            <ta e="T183" id="Seg_9654" s="T182">сильно</ta>
            <ta e="T185" id="Seg_9655" s="T184">тогда</ta>
            <ta e="T186" id="Seg_9656" s="T185">я.NOM</ta>
            <ta e="T187" id="Seg_9657" s="T186">прийти-PST-1SG</ta>
            <ta e="T188" id="Seg_9658" s="T187">пойти-PST-1SG</ta>
            <ta e="T189" id="Seg_9659" s="T188">этот-LAT</ta>
            <ta e="T190" id="Seg_9660" s="T189">ты.NOM</ta>
            <ta e="T1182" id="Seg_9661" s="T190">пойти-CVB</ta>
            <ta e="T191" id="Seg_9662" s="T1182">исчезнуть-PST-1SG</ta>
            <ta e="T192" id="Seg_9663" s="T191">а</ta>
            <ta e="T193" id="Seg_9664" s="T192">я.NOM</ta>
            <ta e="T194" id="Seg_9665" s="T193">умереть-FRQ-RES-PST-1SG</ta>
            <ta e="T195" id="Seg_9666" s="T194">и</ta>
            <ta e="T196" id="Seg_9667" s="T195">мерзнуть-FACT-PST-1SG</ta>
            <ta e="T198" id="Seg_9668" s="T197">а</ta>
            <ta e="T199" id="Seg_9669" s="T198">я.NOM</ta>
            <ta e="T200" id="Seg_9670" s="T199">сказать-PST-1SG</ta>
            <ta e="T201" id="Seg_9671" s="T200">ну</ta>
            <ta e="T202" id="Seg_9672" s="T201">мерзнуть-IMP.2SG</ta>
            <ta e="T204" id="Seg_9673" s="T203">что.[NOM.SG]</ta>
            <ta e="T205" id="Seg_9674" s="T204">и</ta>
            <ta e="T207" id="Seg_9675" s="T206">и</ta>
            <ta e="T208" id="Seg_9676" s="T207">умереть-IMP.2SG</ta>
            <ta e="T210" id="Seg_9677" s="T209">IRREAL</ta>
            <ta e="T211" id="Seg_9678" s="T210">я.NOM</ta>
            <ta e="T212" id="Seg_9679" s="T211">вода.[NOM.SG]</ta>
            <ta e="T213" id="Seg_9680" s="T212">нагреться-TR-1SG</ta>
            <ta e="T215" id="Seg_9681" s="T214">и</ta>
            <ta e="T216" id="Seg_9682" s="T215">ты.DAT</ta>
            <ta e="T218" id="Seg_9683" s="T217">лить-FUT-2SG</ta>
            <ta e="T220" id="Seg_9684" s="T219">тогда</ta>
            <ta e="T221" id="Seg_9685" s="T220">мыть-FUT-1SG</ta>
            <ta e="T222" id="Seg_9686" s="T221">и</ta>
            <ta e="T223" id="Seg_9687" s="T222">место-LAT</ta>
            <ta e="T226" id="Seg_9688" s="T225">место-LAT</ta>
            <ta e="T227" id="Seg_9689" s="T226">класть-FUT-PST-1SG</ta>
            <ta e="T241" id="Seg_9690" s="T240">этот.[NOM.SG]</ta>
            <ta e="T242" id="Seg_9691" s="T241">женщина.[NOM.SG]</ta>
            <ta e="T243" id="Seg_9692" s="T242">PTCL</ta>
            <ta e="T244" id="Seg_9693" s="T243">я.LAT</ta>
            <ta e="T245" id="Seg_9694" s="T244">жечь-IMP.2SG</ta>
            <ta e="T246" id="Seg_9695" s="T245">баня.[NOM.SG]</ta>
            <ta e="T247" id="Seg_9696" s="T246">мыть-IMP.2SG</ta>
            <ta e="T248" id="Seg_9697" s="T247">я.LAT</ta>
            <ta e="T250" id="Seg_9698" s="T249">умереть-RES-FUT-1SG</ta>
            <ta e="T251" id="Seg_9699" s="T250">так</ta>
            <ta e="T252" id="Seg_9700" s="T251">тогда</ta>
            <ta e="T254" id="Seg_9701" s="T252">лицо-NOM/GEN/ACC.3SG=PTCL</ta>
            <ta e="T256" id="Seg_9702" s="T254">рука-PL=PTCL</ta>
            <ta e="T257" id="Seg_9703" s="T256">мыть-FUT-2SG</ta>
            <ta e="T259" id="Seg_9704" s="T258">я.NOM</ta>
            <ta e="T260" id="Seg_9705" s="T259">тогда</ta>
            <ta e="T261" id="Seg_9706" s="T260">жечь-PST-1SG</ta>
            <ta e="T263" id="Seg_9707" s="T262">баня.[NOM.SG]</ta>
            <ta e="T265" id="Seg_9708" s="T264">тогда</ta>
            <ta e="T266" id="Seg_9709" s="T265">санка-PL-INS</ta>
            <ta e="T267" id="Seg_9710" s="T266">класть-PST-1SG</ta>
            <ta e="T268" id="Seg_9711" s="T267">и</ta>
            <ta e="T269" id="Seg_9712" s="T268">%%</ta>
            <ta e="T270" id="Seg_9713" s="T269">нести-RES-PST-1SG</ta>
            <ta e="T271" id="Seg_9714" s="T270">мыть-PST-1SG</ta>
            <ta e="T272" id="Seg_9715" s="T271">этот-ACC</ta>
            <ta e="T273" id="Seg_9716" s="T272">и</ta>
            <ta e="T274" id="Seg_9717" s="T273">опять</ta>
            <ta e="T275" id="Seg_9718" s="T274">чум-LAT</ta>
            <ta e="T276" id="Seg_9719" s="T275">принести-PST-1SG</ta>
            <ta e="T277" id="Seg_9720" s="T276">санка-PL-INS</ta>
            <ta e="T279" id="Seg_9721" s="T278">класть-PST-1SG</ta>
            <ta e="T280" id="Seg_9722" s="T279">кровать-LAT</ta>
            <ta e="T281" id="Seg_9723" s="T280">сказать-IPFVZ-1SG</ta>
            <ta e="T282" id="Seg_9724" s="T281">ну</ta>
            <ta e="T283" id="Seg_9725" s="T282">сейчас</ta>
            <ta e="T284" id="Seg_9726" s="T283">умереть-IMP.2SG</ta>
            <ta e="T286" id="Seg_9727" s="T285">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T287" id="Seg_9728" s="T286">и</ta>
            <ta e="T288" id="Seg_9729" s="T287">рука-PL-COM</ta>
            <ta e="T289" id="Seg_9730" s="T288">мыть-FUT-1SG</ta>
            <ta e="T290" id="Seg_9731" s="T289">и</ta>
            <ta e="T291" id="Seg_9732" s="T290">хороший</ta>
            <ta e="T292" id="Seg_9733" s="T291">стать-FUT-3SG</ta>
            <ta e="T294" id="Seg_9734" s="T293">ну</ta>
            <ta e="T295" id="Seg_9735" s="T294">тогда</ta>
            <ta e="T296" id="Seg_9736" s="T295">баня-LOC</ta>
            <ta e="T297" id="Seg_9737" s="T296">мыть-PST-1SG</ta>
            <ta e="T298" id="Seg_9738" s="T297">очень</ta>
            <ta e="T300" id="Seg_9739" s="T299">грязный.[NOM.SG]</ta>
            <ta e="T301" id="Seg_9740" s="T300">много</ta>
            <ta e="T302" id="Seg_9741" s="T301">быть-PST.[3SG]</ta>
            <ta e="T303" id="Seg_9742" s="T302">голова-LAT/LOC.3SG</ta>
            <ta e="T305" id="Seg_9743" s="T304">я.NOM</ta>
            <ta e="T306" id="Seg_9744" s="T305">сказать-PRS-1SG</ta>
            <ta e="T307" id="Seg_9745" s="T306">еще</ta>
            <ta e="T308" id="Seg_9746" s="T307">класть-EP-IMP.2SG</ta>
            <ta e="T309" id="Seg_9747" s="T308">вода.[NOM.SG]</ta>
            <ta e="T311" id="Seg_9748" s="T310">тот-ACC</ta>
            <ta e="T313" id="Seg_9749" s="T312">лить-IMP.2SG.O</ta>
            <ta e="T317" id="Seg_9750" s="T316">один.[NOM.SG]</ta>
            <ta e="T319" id="Seg_9751" s="T318">лошадь.[NOM.SG]</ta>
            <ta e="T320" id="Seg_9752" s="T319">дать-PST-1SG</ta>
            <ta e="T321" id="Seg_9753" s="T320">мужчина-GEN</ta>
            <ta e="T322" id="Seg_9754" s="T321">брат-LAT</ta>
            <ta e="T324" id="Seg_9755" s="T323">два.[NOM.SG]</ta>
            <ta e="T325" id="Seg_9756" s="T324">год.[NOM.SG]</ta>
            <ta e="T326" id="Seg_9757" s="T325">быть-PST.[3SG]</ta>
            <ta e="T328" id="Seg_9758" s="T327">опять</ta>
            <ta e="T329" id="Seg_9759" s="T328">лошадь.[NOM.SG]</ta>
            <ta e="T330" id="Seg_9760" s="T329">дать-PST-1SG</ta>
            <ta e="T331" id="Seg_9761" s="T330">мужчина-LAT</ta>
            <ta e="T333" id="Seg_9762" s="T332">мужчина.[NOM.SG]</ta>
            <ta e="T334" id="Seg_9763" s="T333">быть-PST.[3SG]</ta>
            <ta e="T336" id="Seg_9764" s="T335">NEG.EX.[3SG]</ta>
            <ta e="T337" id="Seg_9765" s="T336">лошадь.[NOM.SG]</ta>
            <ta e="T338" id="Seg_9766" s="T337">тоже</ta>
            <ta e="T339" id="Seg_9767" s="T338">два.[NOM.SG]</ta>
            <ta e="T340" id="Seg_9768" s="T339">год.[NOM.SG]</ta>
            <ta e="T341" id="Seg_9769" s="T340">быть-PST.[3SG]</ta>
            <ta e="T343" id="Seg_9770" s="T342">дать-PST-1SG</ta>
            <ta e="T344" id="Seg_9771" s="T343">лошадь.[NOM.SG]</ta>
            <ta e="T345" id="Seg_9772" s="T344">тогда</ta>
            <ta e="T346" id="Seg_9773" s="T345">этот.[NOM.SG]</ta>
            <ta e="T347" id="Seg_9774" s="T346">сестра-LAT/LOC.3SG</ta>
            <ta e="T348" id="Seg_9775" s="T347">овца.[NOM.SG]</ta>
            <ta e="T349" id="Seg_9776" s="T348">дать-PST-1SG</ta>
            <ta e="T351" id="Seg_9777" s="T350">парка.[NOM.SG]</ta>
            <ta e="T352" id="Seg_9778" s="T351">дать-PST-1SG</ta>
            <ta e="T354" id="Seg_9779" s="T353">тогда</ta>
            <ta e="T355" id="Seg_9780" s="T354">племянница-GEN</ta>
            <ta e="T356" id="Seg_9781" s="T355">дочь-GEN.1SG</ta>
            <ta e="T357" id="Seg_9782" s="T356">дать-PST-1SG</ta>
            <ta e="T358" id="Seg_9783" s="T357">бык.[NOM.SG]</ta>
            <ta e="T360" id="Seg_9784" s="T359">половина.[NOM.SG]</ta>
            <ta e="T361" id="Seg_9785" s="T360">половина.[NOM.SG]</ta>
            <ta e="T362" id="Seg_9786" s="T361">зима.[NOM.SG]</ta>
            <ta e="T364" id="Seg_9787" s="T363">тогда</ta>
            <ta e="T366" id="Seg_9788" s="T365">сестра-LAT</ta>
            <ta e="T367" id="Seg_9789" s="T366">сын-NOM/GEN.3SG</ta>
            <ta e="T368" id="Seg_9790" s="T367">дать-PST-1SG</ta>
            <ta e="T369" id="Seg_9791" s="T368">корова.[NOM.SG]</ta>
            <ta e="T370" id="Seg_9792" s="T369">и</ta>
            <ta e="T371" id="Seg_9793" s="T370">овца.[NOM.SG]</ta>
            <ta e="T372" id="Seg_9794" s="T371">дать-PST-1SG</ta>
            <ta e="T376" id="Seg_9795" s="T375">тогда</ta>
            <ta e="T377" id="Seg_9796" s="T376">один.[NOM.SG]</ta>
            <ta e="T378" id="Seg_9797" s="T377">женщина-LAT</ta>
            <ta e="T379" id="Seg_9798" s="T378">опять</ta>
            <ta e="T381" id="Seg_9799" s="T380">теленок.[NOM.SG]</ta>
            <ta e="T382" id="Seg_9800" s="T381">дать-PST-1SG</ta>
            <ta e="T384" id="Seg_9801" s="T383">люди.[NOM.SG]</ta>
            <ta e="T385" id="Seg_9802" s="T384">прийти-FUT-3PL</ta>
            <ta e="T386" id="Seg_9803" s="T385">принести-IMP.2SG</ta>
            <ta e="T387" id="Seg_9804" s="T386">деньги.[NOM.SG]</ta>
            <ta e="T389" id="Seg_9805" s="T388">мы.NOM</ta>
            <ta e="T390" id="Seg_9806" s="T389">ты.DAT</ta>
            <ta e="T391" id="Seg_9807" s="T390">дать-FUT-1PL</ta>
            <ta e="T393" id="Seg_9808" s="T392">а</ta>
            <ta e="T394" id="Seg_9809" s="T393">сам</ta>
            <ta e="T395" id="Seg_9810" s="T394">сам-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T396" id="Seg_9811" s="T395">NEG</ta>
            <ta e="T397" id="Seg_9812" s="T396">дать-FUT-3PL</ta>
            <ta e="T398" id="Seg_9813" s="T397">и</ta>
            <ta e="T399" id="Seg_9814" s="T398">тогда</ta>
            <ta e="T401" id="Seg_9815" s="T400">деньги.[NOM.SG]</ta>
            <ta e="T407" id="Seg_9816" s="T406">я.NOM</ta>
            <ta e="T408" id="Seg_9817" s="T407">сейчас</ta>
            <ta e="T409" id="Seg_9818" s="T408">NEG.AUX-1SG</ta>
            <ta e="T410" id="Seg_9819" s="T409">NEG</ta>
            <ta e="T412" id="Seg_9820" s="T411">думать-FUT-1SG</ta>
            <ta e="T413" id="Seg_9821" s="T412">а.то</ta>
            <ta e="T414" id="Seg_9822" s="T413">лошадь-GEN</ta>
            <ta e="T415" id="Seg_9823" s="T414">голова-NOM/GEN.3SG</ta>
            <ta e="T416" id="Seg_9824" s="T415">надо</ta>
            <ta e="T417" id="Seg_9825" s="T416">большой.[NOM.SG]</ta>
            <ta e="T421" id="Seg_9826" s="T420">вы.NOM</ta>
            <ta e="T422" id="Seg_9827" s="T421">думать-DUR.[3SG]</ta>
            <ta e="T428" id="Seg_9828" s="T427">скоро</ta>
            <ta e="T429" id="Seg_9829" s="T428">пойти-FUT-1SG</ta>
            <ta e="T430" id="Seg_9830" s="T429">корова-PL</ta>
            <ta e="T432" id="Seg_9831" s="T431">гнать-FUT-1SG</ta>
            <ta e="T433" id="Seg_9832" s="T432">и</ta>
            <ta e="T434" id="Seg_9833" s="T433">бежать-FUT-1SG</ta>
            <ta e="T436" id="Seg_9834" s="T435">пойти-PST-1SG</ta>
            <ta e="T437" id="Seg_9835" s="T436">а</ta>
            <ta e="T438" id="Seg_9836" s="T437">этот-PL</ta>
            <ta e="T439" id="Seg_9837" s="T438">бежать-MOM-PST-3PL</ta>
            <ta e="T440" id="Seg_9838" s="T439">куда=INDEF</ta>
            <ta e="T444" id="Seg_9839" s="T443">трактор.[NOM.SG]</ta>
            <ta e="T445" id="Seg_9840" s="T444">PTCL</ta>
            <ta e="T446" id="Seg_9841" s="T445">дерево-PL</ta>
            <ta e="T447" id="Seg_9842" s="T446">нести-%%-PRS.[3SG]</ta>
            <ta e="T449" id="Seg_9843" s="T448">NEG</ta>
            <ta e="T450" id="Seg_9844" s="T449">знать-1SG</ta>
            <ta e="T451" id="Seg_9845" s="T450">сколько</ta>
            <ta e="T452" id="Seg_9846" s="T451">нести-%%-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_9847" s="T455">очень</ta>
            <ta e="T457" id="Seg_9848" s="T456">много</ta>
            <ta e="T458" id="Seg_9849" s="T457">дерево-PL</ta>
            <ta e="T460" id="Seg_9850" s="T459">и</ta>
            <ta e="T461" id="Seg_9851" s="T460">и</ta>
            <ta e="T462" id="Seg_9852" s="T461">толстый</ta>
            <ta e="T463" id="Seg_9853" s="T462">толстый</ta>
            <ta e="T464" id="Seg_9854" s="T463">очень</ta>
            <ta e="T468" id="Seg_9855" s="T467">много</ta>
            <ta e="T470" id="Seg_9856" s="T469">работать-INF.LAT</ta>
            <ta e="T471" id="Seg_9857" s="T470">видеть-PST-1SG</ta>
            <ta e="T472" id="Seg_9858" s="T471">а</ta>
            <ta e="T473" id="Seg_9859" s="T472">мало</ta>
            <ta e="T474" id="Seg_9860" s="T473">видеть-DUR.PST-1SG</ta>
            <ta e="T478" id="Seg_9861" s="T477">вождь.[NOM.SG]</ta>
            <ta e="T479" id="Seg_9862" s="T478">очень</ta>
            <ta e="T480" id="Seg_9863" s="T479">красивый.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9864" s="T481">PTCL</ta>
            <ta e="T483" id="Seg_9865" s="T482">что.[NOM.SG]</ta>
            <ta e="T484" id="Seg_9866" s="T483">я.LAT</ta>
            <ta e="T485" id="Seg_9867" s="T484">дать-FACT-PST.[3SG]</ta>
            <ta e="T486" id="Seg_9868" s="T485">PTCL</ta>
            <ta e="T487" id="Seg_9869" s="T486">что.[NOM.SG]</ta>
            <ta e="T491" id="Seg_9870" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_9871" s="T491">я.LAT</ta>
            <ta e="T494" id="Seg_9872" s="T493">показать-PST.[3SG]</ta>
            <ta e="T501" id="Seg_9873" s="T500">стать-FUT-3SG</ta>
            <ta e="T502" id="Seg_9874" s="T501">день.[NOM.SG]</ta>
            <ta e="T503" id="Seg_9875" s="T502">стать-FUT-3SG</ta>
            <ta e="T504" id="Seg_9876" s="T503">есть-INF.LAT</ta>
            <ta e="T508" id="Seg_9877" s="T507">сегодня</ta>
            <ta e="T509" id="Seg_9878" s="T508">суббота</ta>
            <ta e="T510" id="Seg_9879" s="T509">надо</ta>
            <ta e="T511" id="Seg_9880" s="T510">баня.[NOM.SG]</ta>
            <ta e="T512" id="Seg_9881" s="T511">жечь-INF.LAT</ta>
            <ta e="T513" id="Seg_9882" s="T512">вода.[NOM.SG]</ta>
            <ta e="T514" id="Seg_9883" s="T513">носить-INF.LAT</ta>
            <ta e="T516" id="Seg_9884" s="T515">тогда</ta>
            <ta e="T517" id="Seg_9885" s="T516">жечь-FUT-1SG</ta>
            <ta e="T519" id="Seg_9886" s="T518">мыться-IPFVZ-INF.LAT</ta>
            <ta e="T520" id="Seg_9887" s="T519">надо</ta>
            <ta e="T522" id="Seg_9888" s="T521">а.то</ta>
            <ta e="T523" id="Seg_9889" s="T522">целый</ta>
            <ta e="T524" id="Seg_9890" s="T523">неделя.[NOM.SG]</ta>
            <ta e="T525" id="Seg_9891" s="T524">NEG</ta>
            <ta e="T526" id="Seg_9892" s="T525">мыться-IPFVZ-PST-1SG</ta>
            <ta e="T530" id="Seg_9893" s="T529">дом-EP-NOM/GEN/ACC.1SG-LAT</ta>
            <ta e="T531" id="Seg_9894" s="T530">бумага.[NOM.SG]</ta>
            <ta e="T532" id="Seg_9895" s="T531">ждать-PRS-1SG</ta>
            <ta e="T533" id="Seg_9896" s="T532">ждать-PRS-1SG</ta>
            <ta e="T535" id="Seg_9897" s="T534">всегда</ta>
            <ta e="T536" id="Seg_9898" s="T535">надо</ta>
            <ta e="T537" id="Seg_9899" s="T536">NEG.EX.[3SG]</ta>
            <ta e="T538" id="Seg_9900" s="T537">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T539" id="Seg_9901" s="T538">NEG</ta>
            <ta e="T540" id="Seg_9902" s="T539">писать-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_9903" s="T541">и</ta>
            <ta e="T543" id="Seg_9904" s="T542">бумага.[NOM.SG]</ta>
            <ta e="T544" id="Seg_9905" s="T543">NEG</ta>
            <ta e="T545" id="Seg_9906" s="T544">прийти-PRS.[3SG]</ta>
            <ta e="T548" id="Seg_9907" s="T547">сегодня</ta>
            <ta e="T549" id="Seg_9908" s="T548">вечер-LOC.ADV</ta>
            <ta e="T550" id="Seg_9909" s="T549">сон-LAT/LOC.1SG</ta>
            <ta e="T551" id="Seg_9910" s="T550">видеть-PST-1SG</ta>
            <ta e="T553" id="Seg_9911" s="T552">бороться-DUR.PST-1SG</ta>
            <ta e="T554" id="Seg_9912" s="T553">сегодня</ta>
            <ta e="T555" id="Seg_9913" s="T554">бумага.[NOM.SG]</ta>
            <ta e="T556" id="Seg_9914" s="T555">прийти-FUT-3SG</ta>
            <ta e="T560" id="Seg_9915" s="T559">я.NOM</ta>
            <ta e="T561" id="Seg_9916" s="T560">идти-PST-1SG</ta>
            <ta e="T562" id="Seg_9917" s="T561">Красноярск-LAT</ta>
            <ta e="T1186" id="Seg_9918" s="T562">яйцо-PL</ta>
            <ta e="T564" id="Seg_9919" s="T563">нести-PST-1SG</ta>
            <ta e="T567" id="Seg_9920" s="T566">сто</ta>
            <ta e="T568" id="Seg_9921" s="T567">яйцо.[NOM.SG]</ta>
            <ta e="T570" id="Seg_9922" s="T569">тогда</ta>
            <ta e="T571" id="Seg_9923" s="T570">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T572" id="Seg_9924" s="T571">я.NOM-INS</ta>
            <ta e="T573" id="Seg_9925" s="T572">пойти-PST.[3SG]</ta>
            <ta e="T575" id="Seg_9926" s="T574">этот.[NOM.SG]</ta>
            <ta e="T576" id="Seg_9927" s="T575">я.LAT</ta>
            <ta e="T577" id="Seg_9928" s="T576">нести-PST.[3SG]</ta>
            <ta e="T578" id="Seg_9929" s="T577">сидеть-PST.[3SG]</ta>
            <ta e="T579" id="Seg_9930" s="T578">сидеть-PST.[3SG]</ta>
            <ta e="T580" id="Seg_9931" s="T579">машина-3PL-INS</ta>
            <ta e="T582" id="Seg_9932" s="T581">я.NOM</ta>
            <ta e="T584" id="Seg_9933" s="T583">NEG</ta>
            <ta e="T585" id="Seg_9934" s="T584">мочь-PRS-1SG</ta>
            <ta e="T586" id="Seg_9935" s="T585">видеть-INF.LAT</ta>
            <ta e="T587" id="Seg_9936" s="T586">тогда</ta>
            <ta e="T588" id="Seg_9937" s="T587">видеть-PST-1SG</ta>
            <ta e="T589" id="Seg_9938" s="T588">племянница-ACC</ta>
            <ta e="T591" id="Seg_9939" s="T590">а</ta>
            <ta e="T592" id="Seg_9940" s="T591">там</ta>
            <ta e="T593" id="Seg_9941" s="T592">пойти-IPFVZ-PST-1SG</ta>
            <ta e="T595" id="Seg_9942" s="T594">отсюда</ta>
            <ta e="T596" id="Seg_9943" s="T595">машина-INS</ta>
            <ta e="T598" id="Seg_9944" s="T597">а</ta>
            <ta e="T599" id="Seg_9945" s="T598">там</ta>
            <ta e="T600" id="Seg_9946" s="T599">автобус-INS</ta>
            <ta e="T602" id="Seg_9947" s="T601">тогда</ta>
            <ta e="T603" id="Seg_9948" s="T602">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T604" id="Seg_9949" s="T603">дорога.[NOM.SG]</ta>
            <ta e="T606" id="Seg_9950" s="T605">деньги.[NOM.SG]</ta>
            <ta e="T607" id="Seg_9951" s="T606">дорога.[NOM.SG]</ta>
            <ta e="T608" id="Seg_9952" s="T607">тогда</ta>
            <ta e="T609" id="Seg_9953" s="T608">там</ta>
            <ta e="T610" id="Seg_9954" s="T609">прийти-PST-1SG</ta>
            <ta e="T614" id="Seg_9955" s="T613">тогда</ta>
            <ta e="T615" id="Seg_9956" s="T614">идти-PST-1SG</ta>
            <ta e="T616" id="Seg_9957" s="T615">Бог-LAT</ta>
            <ta e="T618" id="Seg_9958" s="T617">бог-EP-GEN</ta>
            <ta e="T619" id="Seg_9959" s="T618">молиться-INF.LAT</ta>
            <ta e="T621" id="Seg_9960" s="T620">тогда</ta>
            <ta e="T622" id="Seg_9961" s="T621">идти-PST-1PL</ta>
            <ta e="T623" id="Seg_9962" s="T622">племянница-INS</ta>
            <ta e="T624" id="Seg_9963" s="T623">%%-LAT</ta>
            <ta e="T626" id="Seg_9964" s="T625">там</ta>
            <ta e="T628" id="Seg_9965" s="T627">сесть-PST-1PL</ta>
            <ta e="T629" id="Seg_9966" s="T628">и</ta>
            <ta e="T630" id="Seg_9967" s="T629">говорить-PST-1PL</ta>
            <ta e="T632" id="Seg_9968" s="T631">а</ta>
            <ta e="T633" id="Seg_9969" s="T632">тогда</ta>
            <ta e="T634" id="Seg_9970" s="T633">дом-LAT/LOC.3SG</ta>
            <ta e="T635" id="Seg_9971" s="T634">прийти-PST-1SG</ta>
            <ta e="T639" id="Seg_9972" s="T638">племянница.[NOM.SG]</ta>
            <ta e="T640" id="Seg_9973" s="T639">я.LAT</ta>
            <ta e="T641" id="Seg_9974" s="T640">помидор.[NOM.SG]</ta>
            <ta e="T642" id="Seg_9975" s="T641">много</ta>
            <ta e="T643" id="Seg_9976" s="T642">дать-PST.[3SG]</ta>
            <ta e="T644" id="Seg_9977" s="T643">я.NOM</ta>
            <ta e="T1184" id="Seg_9978" s="T645">Агинское</ta>
            <ta e="T646" id="Seg_9979" s="T1184">поселение-LOC</ta>
            <ta e="T647" id="Seg_9980" s="T646">два.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9981" s="T647">день.[NOM.SG]</ta>
            <ta e="T649" id="Seg_9982" s="T648">сидеть-PST-1SG</ta>
            <ta e="T653" id="Seg_9983" s="T652">корова-PL</ta>
            <ta e="T654" id="Seg_9984" s="T653">прийти-PST-3PL</ta>
            <ta e="T655" id="Seg_9985" s="T654">дом-LAT/LOC.3SG</ta>
            <ta e="T656" id="Seg_9986" s="T655">надо</ta>
            <ta e="T657" id="Seg_9987" s="T656">этот-ACC.PL</ta>
            <ta e="T658" id="Seg_9988" s="T657">кораль-NOM/GEN/ACC.3SG</ta>
            <ta e="T659" id="Seg_9989" s="T658">гнать-INF.LAT</ta>
            <ta e="T660" id="Seg_9990" s="T659">и</ta>
            <ta e="T662" id="Seg_9991" s="T661">%%-INF.LAT</ta>
            <ta e="T666" id="Seg_9992" s="T665">думать-PST-1SG</ta>
            <ta e="T667" id="Seg_9993" s="T666">думать-PST-1SG</ta>
            <ta e="T668" id="Seg_9994" s="T667">что=INDEF</ta>
            <ta e="T669" id="Seg_9995" s="T668">думать-MOM-PST-1SG</ta>
            <ta e="T673" id="Seg_9996" s="T672">дочь-LAT</ta>
            <ta e="T674" id="Seg_9997" s="T673">идти-PST-1SG</ta>
            <ta e="T675" id="Seg_9998" s="T674">сумка.[NOM.SG]</ta>
            <ta e="T676" id="Seg_9999" s="T675">этот.[NOM.SG]</ta>
            <ta e="T677" id="Seg_10000" s="T676">PTCL</ta>
            <ta e="T678" id="Seg_10001" s="T677">оторваться-MOM.[3SG]</ta>
            <ta e="T680" id="Seg_10002" s="T679">сейчас</ta>
            <ta e="T681" id="Seg_10003" s="T680">сидеть-DUR-1SG</ta>
            <ta e="T682" id="Seg_10004" s="T681">и</ta>
            <ta e="T684" id="Seg_10005" s="T683">шить-DUR-1SG</ta>
            <ta e="T686" id="Seg_10006" s="T685">ты.DAT</ta>
            <ta e="T687" id="Seg_10007" s="T686">прийти-PST-1SG</ta>
            <ta e="T688" id="Seg_10008" s="T687">прийти-PST-1SG</ta>
            <ta e="T689" id="Seg_10009" s="T688">%%</ta>
            <ta e="T690" id="Seg_10010" s="T689">NEG</ta>
            <ta e="T691" id="Seg_10011" s="T690">упасть-MOM-PST-1SG</ta>
            <ta e="T696" id="Seg_10012" s="T695">заяц</ta>
            <ta e="T698" id="Seg_10013" s="T697">заяц.[NOM.SG]</ta>
            <ta e="T699" id="Seg_10014" s="T698">прийти-PRS.[3SG]</ta>
            <ta e="T700" id="Seg_10015" s="T699">прийти-PRS.[3SG]</ta>
            <ta e="T701" id="Seg_10016" s="T700">тогда</ta>
            <ta e="T702" id="Seg_10017" s="T701">трава.[NOM.SG]</ta>
            <ta e="T703" id="Seg_10018" s="T702">этот-LAT</ta>
            <ta e="T705" id="Seg_10019" s="T704">нос.[NOM.SG]</ta>
            <ta e="T708" id="Seg_10020" s="T707">резать-MOM.PRS.[3SG]</ta>
            <ta e="T709" id="Seg_10021" s="T708">кровь-NOM/GEN/ACC.3SG</ta>
            <ta e="T710" id="Seg_10022" s="T709">PTCL</ta>
            <ta e="T711" id="Seg_10023" s="T710">течь-DUR.[3SG]</ta>
            <ta e="T713" id="Seg_10024" s="T712">этот.[NOM.SG]</ta>
            <ta e="T714" id="Seg_10025" s="T713">сказать-IPFVZ.[3SG]</ta>
            <ta e="T715" id="Seg_10026" s="T714">огонь.[NOM.SG]</ta>
            <ta e="T717" id="Seg_10027" s="T716">жечь-IMP.2SG.O</ta>
            <ta e="T718" id="Seg_10028" s="T717">этот.[NOM.SG]</ta>
            <ta e="T719" id="Seg_10029" s="T718">трава.[NOM.SG]</ta>
            <ta e="T723" id="Seg_10030" s="T722">я.NOM</ta>
            <ta e="T724" id="Seg_10031" s="T723">много</ta>
            <ta e="T725" id="Seg_10032" s="T724">земля-ACC</ta>
            <ta e="T726" id="Seg_10033" s="T725">жечь-INF.LAT</ta>
            <ta e="T727" id="Seg_10034" s="T726">тогда</ta>
            <ta e="T728" id="Seg_10035" s="T727">этот.[NOM.SG]</ta>
            <ta e="T729" id="Seg_10036" s="T728">вода-LAT</ta>
            <ta e="T730" id="Seg_10037" s="T729">пойти-PST.[3SG]</ta>
            <ta e="T732" id="Seg_10038" s="T731">вода.[NOM.SG]</ta>
            <ta e="T733" id="Seg_10039" s="T732">пойти-EP-IMP.2SG</ta>
            <ta e="T734" id="Seg_10040" s="T733">огонь-ACC</ta>
            <ta e="T736" id="Seg_10041" s="T735">лить-IMP.2SG</ta>
            <ta e="T740" id="Seg_10042" s="T739">я.NOM</ta>
            <ta e="T741" id="Seg_10043" s="T740">много</ta>
            <ta e="T743" id="Seg_10044" s="T742">земля.[NOM.SG]</ta>
            <ta e="T744" id="Seg_10045" s="T743">лить-%%-INF.LAT</ta>
            <ta e="T745" id="Seg_10046" s="T744">чтобы</ta>
            <ta e="T746" id="Seg_10047" s="T745">трава.[NOM.SG]</ta>
            <ta e="T747" id="Seg_10048" s="T746">расти-PST.[3SG]</ta>
            <ta e="T751" id="Seg_10049" s="T750">тогда</ta>
            <ta e="T752" id="Seg_10050" s="T751">этот.[NOM.SG]</ta>
            <ta e="T753" id="Seg_10051" s="T752">пойти-PST.[3SG]</ta>
            <ta e="T755" id="Seg_10052" s="T754">лось.[NOM.SG]</ta>
            <ta e="T756" id="Seg_10053" s="T755">лось.[NOM.SG]</ta>
            <ta e="T757" id="Seg_10054" s="T756">пойти-EP-IMP.2SG</ta>
            <ta e="T758" id="Seg_10055" s="T757">PTCL</ta>
            <ta e="T759" id="Seg_10056" s="T758">вода.[NOM.SG]</ta>
            <ta e="T761" id="Seg_10057" s="T760">пить-EP-IMP.2SG</ta>
            <ta e="T763" id="Seg_10058" s="T762">тогда</ta>
            <ta e="T764" id="Seg_10059" s="T763">лось.[NOM.SG]</ta>
            <ta e="T765" id="Seg_10060" s="T764">сказать-PST.[3SG]</ta>
            <ta e="T766" id="Seg_10061" s="T765">я.NOM</ta>
            <ta e="T767" id="Seg_10062" s="T766">нога-LAT/LOC.3SG</ta>
            <ta e="T768" id="Seg_10063" s="T767">много</ta>
            <ta e="T769" id="Seg_10064" s="T768">вода.[NOM.SG]</ta>
            <ta e="T771" id="Seg_10065" s="T770">тогда</ta>
            <ta e="T772" id="Seg_10066" s="T771">этот.[NOM.SG]</ta>
            <ta e="T774" id="Seg_10067" s="T773">пойти-PST.[3SG]</ta>
            <ta e="T775" id="Seg_10068" s="T774">женщина-LAT</ta>
            <ta e="T776" id="Seg_10069" s="T775">женщина.[NOM.SG]</ta>
            <ta e="T777" id="Seg_10070" s="T776">женщина.[NOM.SG]</ta>
            <ta e="T778" id="Seg_10071" s="T777">пойти-EP-IMP.2SG</ta>
            <ta e="T779" id="Seg_10072" s="T778">жила-PL</ta>
            <ta e="T780" id="Seg_10073" s="T779">нога-ABL.3SG</ta>
            <ta e="T781" id="Seg_10074" s="T780">взять-IMP.2SG.O</ta>
            <ta e="T783" id="Seg_10075" s="T782">женщина.[NOM.SG]</ta>
            <ta e="T784" id="Seg_10076" s="T783">NEG</ta>
            <ta e="T785" id="Seg_10077" s="T784">пойти-PST.[3SG]</ta>
            <ta e="T787" id="Seg_10078" s="T786">черный-PL</ta>
            <ta e="T788" id="Seg_10079" s="T787">хватит</ta>
            <ta e="T789" id="Seg_10080" s="T788">я.LAT</ta>
            <ta e="T793" id="Seg_10081" s="T792">этот.[NOM.SG]</ta>
            <ta e="T794" id="Seg_10082" s="T793">пойти-PST.[3SG]</ta>
            <ta e="T795" id="Seg_10083" s="T794">женщина-LAT</ta>
            <ta e="T796" id="Seg_10084" s="T795">женщина.[NOM.SG]</ta>
            <ta e="T797" id="Seg_10085" s="T796">женщина.[NOM.SG]</ta>
            <ta e="T798" id="Seg_10086" s="T797">взять-IMP.2SG.O</ta>
            <ta e="T800" id="Seg_10087" s="T799">жила-PL</ta>
            <ta e="T801" id="Seg_10088" s="T800">а</ta>
            <ta e="T802" id="Seg_10089" s="T801">этот.[NOM.SG]</ta>
            <ta e="T803" id="Seg_10090" s="T802">сказать-IPFVZ.[3SG]</ta>
            <ta e="T804" id="Seg_10091" s="T803">я.NOM</ta>
            <ta e="T805" id="Seg_10092" s="T804">много</ta>
            <ta e="T806" id="Seg_10093" s="T805">быть-PRS.[3SG]</ta>
            <ta e="T807" id="Seg_10094" s="T806">и</ta>
            <ta e="T808" id="Seg_10095" s="T807">черный.[NOM.SG]</ta>
            <ta e="T809" id="Seg_10096" s="T808">я.LAT</ta>
            <ta e="T810" id="Seg_10097" s="T809">NEG</ta>
            <ta e="T811" id="Seg_10098" s="T810">нужно</ta>
            <ta e="T815" id="Seg_10099" s="T814">тогда</ta>
            <ta e="T816" id="Seg_10100" s="T815">пойти-PST.[3SG]</ta>
            <ta e="T817" id="Seg_10101" s="T816">мышь-PL-LAT</ta>
            <ta e="T818" id="Seg_10102" s="T817">мышь.[NOM.SG]</ta>
            <ta e="T819" id="Seg_10103" s="T818">мышь.[NOM.SG]</ta>
            <ta e="T820" id="Seg_10104" s="T819">пойти-EP-IMP.2SG</ta>
            <ta e="T821" id="Seg_10105" s="T820">женщина-GEN</ta>
            <ta e="T822" id="Seg_10106" s="T821">жила-PL</ta>
            <ta e="T823" id="Seg_10107" s="T822">есть-IMP.2SG</ta>
            <ta e="T825" id="Seg_10108" s="T824">нет</ta>
            <ta e="T827" id="Seg_10109" s="T826">мы.NOM</ta>
            <ta e="T828" id="Seg_10110" s="T827">NEG.AUX-1SG</ta>
            <ta e="T829" id="Seg_10111" s="T828">пойти-CVB</ta>
            <ta e="T830" id="Seg_10112" s="T829">NEG</ta>
            <ta e="T831" id="Seg_10113" s="T830">пойти-FUT-1SG</ta>
            <ta e="T833" id="Seg_10114" s="T832">мы.LAT</ta>
            <ta e="T834" id="Seg_10115" s="T833">хватит</ta>
            <ta e="T835" id="Seg_10116" s="T834">земля-LOC</ta>
            <ta e="T836" id="Seg_10117" s="T835">корень.PL-PL</ta>
            <ta e="T837" id="Seg_10118" s="T836">съесть-INF.LAT</ta>
            <ta e="T841" id="Seg_10119" s="T840">тогда</ta>
            <ta e="T842" id="Seg_10120" s="T841">пойти-PST.[3SG]</ta>
            <ta e="T844" id="Seg_10121" s="T843">ребенок-PL-LAT</ta>
            <ta e="T845" id="Seg_10122" s="T844">ребенок-PL</ta>
            <ta e="T846" id="Seg_10123" s="T845">ребенок-PL</ta>
            <ta e="T847" id="Seg_10124" s="T846">убить-DUR-2DU</ta>
            <ta e="T848" id="Seg_10125" s="T847">мышь-PL</ta>
            <ta e="T849" id="Seg_10126" s="T848">этот-PL</ta>
            <ta e="T850" id="Seg_10127" s="T849">мы.NOM</ta>
            <ta e="T852" id="Seg_10128" s="T851">сам-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_10129" s="T852">играть-DUR-3PL</ta>
            <ta e="T854" id="Seg_10130" s="T853">бабка-PL-INS</ta>
            <ta e="T858" id="Seg_10131" s="T857">вы.NOM</ta>
            <ta e="T859" id="Seg_10132" s="T858">дом-LOC</ta>
            <ta e="T860" id="Seg_10133" s="T859">хороший</ta>
            <ta e="T861" id="Seg_10134" s="T860">жить-INF.LAT</ta>
            <ta e="T863" id="Seg_10135" s="T862">только</ta>
            <ta e="T864" id="Seg_10136" s="T863">хлеб.[NOM.SG]</ta>
            <ta e="T865" id="Seg_10137" s="T864">где=INDEF</ta>
            <ta e="T866" id="Seg_10138" s="T865">NEG</ta>
            <ta e="T867" id="Seg_10139" s="T866">взять-FUT-2SG</ta>
            <ta e="T869" id="Seg_10140" s="T868">съесть-INF.LAT</ta>
            <ta e="T870" id="Seg_10141" s="T869">NEG.EX.[3SG]</ta>
            <ta e="T871" id="Seg_10142" s="T870">хлеб.[NOM.SG]</ta>
            <ta e="T875" id="Seg_10143" s="T874">магазин-LAT/LOC.3SG</ta>
            <ta e="T876" id="Seg_10144" s="T875">что.[NOM.SG]</ta>
            <ta e="T877" id="Seg_10145" s="T876">NEG.EX.[3SG]</ta>
            <ta e="T878" id="Seg_10146" s="T877">что.[NOM.SG]=INDEF</ta>
            <ta e="T879" id="Seg_10147" s="T878">NEG</ta>
            <ta e="T880" id="Seg_10148" s="T879">взять-FUT-2SG</ta>
            <ta e="T882" id="Seg_10149" s="T881">нет</ta>
            <ta e="T883" id="Seg_10150" s="T882">пойти-EP-IMP.2SG</ta>
            <ta e="T884" id="Seg_10151" s="T883">там</ta>
            <ta e="T885" id="Seg_10152" s="T884">быть-PRS.[3SG]</ta>
            <ta e="T886" id="Seg_10153" s="T885">колбаса.[NOM.SG]</ta>
            <ta e="T887" id="Seg_10154" s="T886">взять-IMP.2SG</ta>
            <ta e="T888" id="Seg_10155" s="T887">и</ta>
            <ta e="T889" id="Seg_10156" s="T888">тогда</ta>
            <ta e="T890" id="Seg_10157" s="T889">есть-FUT-2SG</ta>
            <ta e="T894" id="Seg_10158" s="T893">этот.[NOM.SG]</ta>
            <ta e="T895" id="Seg_10159" s="T894">женщина.[NOM.SG]</ta>
            <ta e="T896" id="Seg_10160" s="T895">очень</ta>
            <ta e="T897" id="Seg_10161" s="T896">хороший</ta>
            <ta e="T898" id="Seg_10162" s="T897">шить-DUR.[3SG]</ta>
            <ta e="T899" id="Seg_10163" s="T898">PTCL</ta>
            <ta e="T900" id="Seg_10164" s="T899">красивый.[NOM.SG]</ta>
            <ta e="T902" id="Seg_10165" s="T901">хороший</ta>
            <ta e="T903" id="Seg_10166" s="T902">смотреть-INF.LAT</ta>
            <ta e="T907" id="Seg_10167" s="T906">этот.[NOM.SG]</ta>
            <ta e="T908" id="Seg_10168" s="T907">женщина.[NOM.SG]</ta>
            <ta e="T909" id="Seg_10169" s="T908">очень</ta>
            <ta e="T910" id="Seg_10170" s="T909">много</ta>
            <ta e="T912" id="Seg_10171" s="T911">работать-PRS.[3SG]</ta>
            <ta e="T913" id="Seg_10172" s="T912">PTCL</ta>
            <ta e="T914" id="Seg_10173" s="T913">что.[NOM.SG]</ta>
            <ta e="T915" id="Seg_10174" s="T914">делать-INF.LAT</ta>
            <ta e="T920" id="Seg_10175" s="T919">PTCL</ta>
            <ta e="T921" id="Seg_10176" s="T920">что.[NOM.SG]</ta>
            <ta e="T922" id="Seg_10177" s="T921">работать-INF.LAT</ta>
            <ta e="T923" id="Seg_10178" s="T922">знать-3SG.O</ta>
            <ta e="T927" id="Seg_10179" s="T926">там</ta>
            <ta e="T928" id="Seg_10180" s="T927">женщина-GEN</ta>
            <ta e="T930" id="Seg_10181" s="T929">родственник-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T931" id="Seg_10182" s="T930">мало</ta>
            <ta e="T932" id="Seg_10183" s="T931">жить-%%-PST.[3SG]</ta>
            <ta e="T933" id="Seg_10184" s="T932">только</ta>
            <ta e="T934" id="Seg_10185" s="T933">один.[NOM.SG]</ta>
            <ta e="T935" id="Seg_10186" s="T934">брат-NOM/GEN.3SG</ta>
            <ta e="T939" id="Seg_10187" s="T938">а</ta>
            <ta e="T940" id="Seg_10188" s="T939">сестра-NOM/GEN.3SG</ta>
            <ta e="T941" id="Seg_10189" s="T940">умереть-RES-PST.[3SG]</ta>
            <ta e="T945" id="Seg_10190" s="T944">шить-MOM-PST-1SG</ta>
            <ta e="T946" id="Seg_10191" s="T945">PTCL</ta>
            <ta e="T947" id="Seg_10192" s="T946">сейчас</ta>
            <ta e="T948" id="Seg_10193" s="T947">NEG.EX.[3SG]</ta>
            <ta e="T949" id="Seg_10194" s="T948">шить-INF.LAT</ta>
            <ta e="T953" id="Seg_10195" s="T952">мы.NOM</ta>
            <ta e="T954" id="Seg_10196" s="T953">%%</ta>
            <ta e="T955" id="Seg_10197" s="T954">пойти-PST-1PL</ta>
            <ta e="T956" id="Seg_10198" s="T955">вода.[NOM.SG]</ta>
            <ta e="T957" id="Seg_10199" s="T956">носить-INF.LAT</ta>
            <ta e="T958" id="Seg_10200" s="T957">а</ta>
            <ta e="T959" id="Seg_10201" s="T958">там</ta>
            <ta e="T960" id="Seg_10202" s="T959">вода.[NOM.SG]</ta>
            <ta e="T961" id="Seg_10203" s="T960">быть-PRS.[3SG]</ta>
            <ta e="T963" id="Seg_10204" s="T962">три.[NOM.SG]</ta>
            <ta e="T965" id="Seg_10205" s="T964">три.[NOM.SG]</ta>
            <ta e="T966" id="Seg_10206" s="T965">ведро.[NOM.SG]</ta>
            <ta e="T967" id="Seg_10207" s="T966">принести-PST-1PL</ta>
            <ta e="T968" id="Seg_10208" s="T967">и</ta>
            <ta e="T969" id="Seg_10209" s="T968">хватит</ta>
            <ta e="T970" id="Seg_10210" s="T969">сейчас</ta>
            <ta e="T974" id="Seg_10211" s="T973">лес-LOC</ta>
            <ta e="T975" id="Seg_10212" s="T974">прийти-PRS-1SG</ta>
            <ta e="T976" id="Seg_10213" s="T975">лес.[NOM.SG]</ta>
            <ta e="T977" id="Seg_10214" s="T976">петь-DUR-1SG</ta>
            <ta e="T979" id="Seg_10215" s="T978">дерево-LOC</ta>
            <ta e="T980" id="Seg_10216" s="T979">прийти-PRS-1SG</ta>
            <ta e="T981" id="Seg_10217" s="T980">дерево.[NOM.SG]</ta>
            <ta e="T982" id="Seg_10218" s="T981">петь-DUR-1SG</ta>
            <ta e="T984" id="Seg_10219" s="T983">степь.[NOM.SG]</ta>
            <ta e="T985" id="Seg_10220" s="T984">прийти-PRS-1SG</ta>
            <ta e="T986" id="Seg_10221" s="T985">степь.[NOM.SG]</ta>
            <ta e="T987" id="Seg_10222" s="T986">петь-DUR-1SG</ta>
            <ta e="T989" id="Seg_10223" s="T988">гора.[NOM.SG]</ta>
            <ta e="T990" id="Seg_10224" s="T989">прийти-PRS-1SG</ta>
            <ta e="T991" id="Seg_10225" s="T990">гора.[NOM.SG]</ta>
            <ta e="T992" id="Seg_10226" s="T991">петь-DUR-1SG</ta>
            <ta e="T994" id="Seg_10227" s="T993">Дяла-ACC</ta>
            <ta e="T995" id="Seg_10228" s="T994">прийти-PRS.[3SG]</ta>
            <ta e="T996" id="Seg_10229" s="T995">Дяла-ACC</ta>
            <ta e="T998" id="Seg_10230" s="T997">петь-DUR-1SG</ta>
            <ta e="T1000" id="Seg_10231" s="T999">дом-LOC</ta>
            <ta e="T1001" id="Seg_10232" s="T1000">сидеть-DUR.[3SG]</ta>
            <ta e="T1002" id="Seg_10233" s="T1001">дом.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_10234" s="T1002">петь-DUR-1SG</ta>
            <ta e="T1007" id="Seg_10235" s="T1006">мы.NOM</ta>
            <ta e="T1008" id="Seg_10236" s="T1007">я.GEN</ta>
            <ta e="T1009" id="Seg_10237" s="T1008">дом-LAT/LOC.1SG</ta>
            <ta e="T1010" id="Seg_10238" s="T1009">очень</ta>
            <ta e="T1011" id="Seg_10239" s="T1010">мышь.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_10240" s="T1011">много</ta>
            <ta e="T1013" id="Seg_10241" s="T1012">что.[NOM.SG]</ta>
            <ta e="T1014" id="Seg_10242" s="T1013">INDEF</ta>
            <ta e="T1015" id="Seg_10243" s="T1014">PTCL</ta>
            <ta e="T1016" id="Seg_10244" s="T1015">съесть-PRS-3PL</ta>
            <ta e="T1018" id="Seg_10245" s="T1017">а</ta>
            <ta e="T1019" id="Seg_10246" s="T1018">кот.[NOM.SG]</ta>
            <ta e="T1020" id="Seg_10247" s="T1019">NEG.EX.[3SG]</ta>
            <ta e="T1022" id="Seg_10248" s="T1021">пойти-EP-IMP.2SG</ta>
            <ta e="T1023" id="Seg_10249" s="T1022">%%-LAT</ta>
            <ta e="T1024" id="Seg_10250" s="T1023">этот.[NOM.SG]</ta>
            <ta e="T1025" id="Seg_10251" s="T1024">там</ta>
            <ta e="T1026" id="Seg_10252" s="T1025">два.[NOM.SG]</ta>
            <ta e="T1028" id="Seg_10253" s="T1027">кот.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_10254" s="T1028">один.[NOM.SG]</ta>
            <ta e="T1030" id="Seg_10255" s="T1029">белый.[NOM.SG]</ta>
            <ta e="T1031" id="Seg_10256" s="T1030">один.[NOM.SG]</ta>
            <ta e="T1032" id="Seg_10257" s="T1031">красный.[NOM.SG]</ta>
            <ta e="T1034" id="Seg_10258" s="T1033">этот.[NOM.SG]</ta>
            <ta e="T1035" id="Seg_10259" s="T1034">дать-FUT-3SG</ta>
            <ta e="T1036" id="Seg_10260" s="T1035">ты.DAT</ta>
            <ta e="T1038" id="Seg_10261" s="T1037">и</ta>
            <ta e="T1039" id="Seg_10262" s="T1038">этот.[NOM.SG]</ta>
            <ta e="T1040" id="Seg_10263" s="T1039">снег.[NOM.SG]</ta>
            <ta e="T1041" id="Seg_10264" s="T1040">я.LAT</ta>
            <ta e="T1042" id="Seg_10265" s="T1041">взять-PST.[3SG]</ta>
            <ta e="T1043" id="Seg_10266" s="T1042">и</ta>
            <ta e="T1181" id="Seg_10267" s="T1043">пойти-CVB</ta>
            <ta e="T1044" id="Seg_10268" s="T1181">исчезнуть-PST.[3SG]</ta>
            <ta e="T1045" id="Seg_10269" s="T1044">этот.[NOM.SG]</ta>
            <ta e="T1046" id="Seg_10270" s="T1045">этот-ACC</ta>
            <ta e="T1047" id="Seg_10271" s="T1046">NEG</ta>
            <ta e="T1048" id="Seg_10272" s="T1047">пускать-SEM-PST.[3SG]</ta>
            <ta e="T1050" id="Seg_10273" s="T1049">еще</ta>
            <ta e="T1051" id="Seg_10274" s="T1050">NEG</ta>
            <ta e="T1052" id="Seg_10275" s="T1051">прийти-PRS.[3SG]</ta>
            <ta e="T1056" id="Seg_10276" s="T1055">я.NOM</ta>
            <ta e="T1057" id="Seg_10277" s="T1056">спать-INF.LAT</ta>
            <ta e="T1058" id="Seg_10278" s="T1057">ложиться-PST-1SG</ta>
            <ta e="T1060" id="Seg_10279" s="T1059">а</ta>
            <ta e="T1061" id="Seg_10280" s="T1060">там</ta>
            <ta e="T1062" id="Seg_10281" s="T1061">очень</ta>
            <ta e="T1063" id="Seg_10282" s="T1062">вонючий.[NOM.SG]</ta>
            <ta e="T1064" id="Seg_10283" s="T1063">видеть-PRS-1SG</ta>
            <ta e="T1065" id="Seg_10284" s="T1064">дерьмо.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_10285" s="T1066">много</ta>
            <ta e="T1068" id="Seg_10286" s="T1067">лежать-DUR.[3SG]</ta>
            <ta e="T1072" id="Seg_10287" s="T1071">утро-LOC.ADV</ta>
            <ta e="T1073" id="Seg_10288" s="T1072">встать-PST-1SG</ta>
            <ta e="T1074" id="Seg_10289" s="T1073">коленка-LAT</ta>
            <ta e="T1075" id="Seg_10290" s="T1074">стоять-PST-1SG</ta>
            <ta e="T1077" id="Seg_10291" s="T1076">видеть-PRS-1SG</ta>
            <ta e="T1078" id="Seg_10292" s="T1077">PTCL</ta>
            <ta e="T1079" id="Seg_10293" s="T1078">там</ta>
            <ta e="T1080" id="Seg_10294" s="T1079">десять.[NOM.SG]</ta>
            <ta e="T1081" id="Seg_10295" s="T1080">куча.PL</ta>
            <ta e="T1082" id="Seg_10296" s="T1081">лежать-DUR.[3SG]</ta>
            <ta e="T1086" id="Seg_10297" s="T1085">тогда</ta>
            <ta e="T1087" id="Seg_10298" s="T1086">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T1088" id="Seg_10299" s="T1087">нести-EP-RES-1SG</ta>
            <ta e="T1089" id="Seg_10300" s="T1088">наружу</ta>
            <ta e="T1090" id="Seg_10301" s="T1089">чашка-LOC</ta>
            <ta e="T1091" id="Seg_10302" s="T1090">чашка-INS</ta>
            <ta e="T1092" id="Seg_10303" s="T1091">закрыть-PRS-1SG</ta>
            <ta e="T1093" id="Seg_10304" s="T1092">чтобы</ta>
            <ta e="T1094" id="Seg_10305" s="T1093">мышь-PL</ta>
            <ta e="T1095" id="Seg_10306" s="T1094">NEG</ta>
            <ta e="T1096" id="Seg_10307" s="T1095">съесть-PST-3PL</ta>
            <ta e="T1098" id="Seg_10308" s="T1097">а</ta>
            <ta e="T1099" id="Seg_10309" s="T1098">масло.[NOM.SG]</ta>
            <ta e="T1100" id="Seg_10310" s="T1099">маслобойка-LAT</ta>
            <ta e="T1101" id="Seg_10311" s="T1100">тоже</ta>
            <ta e="T1103" id="Seg_10312" s="T1102">закрыть-RES-1SG</ta>
            <ta e="T1104" id="Seg_10313" s="T1103">чтобы</ta>
            <ta e="T1105" id="Seg_10314" s="T1104">мышь-PL</ta>
            <ta e="T1106" id="Seg_10315" s="T1105">NEG</ta>
            <ta e="T1107" id="Seg_10316" s="T1106">съесть-PST-3PL</ta>
            <ta e="T1111" id="Seg_10317" s="T1110">а</ta>
            <ta e="T1113" id="Seg_10318" s="T1112">так</ta>
            <ta e="T1114" id="Seg_10319" s="T1113">мы.LAT</ta>
            <ta e="T1115" id="Seg_10320" s="T1114">два.[NOM.SG]</ta>
            <ta e="T1116" id="Seg_10321" s="T1115">быть-PRS.[3SG]</ta>
            <ta e="T1117" id="Seg_10322" s="T1116">кот.[NOM.SG]</ta>
            <ta e="T1118" id="Seg_10323" s="T1117">принести-FUT-1SG</ta>
            <ta e="T1119" id="Seg_10324" s="T1118">один.[NOM.SG]</ta>
            <ta e="T1121" id="Seg_10325" s="T1120">какой</ta>
            <ta e="T1122" id="Seg_10326" s="T1121">ты.DAT</ta>
            <ta e="T1123" id="Seg_10327" s="T1122">принести-INF.LAT</ta>
            <ta e="T1124" id="Seg_10328" s="T1123">белый</ta>
            <ta e="T1125" id="Seg_10329" s="T1124">или</ta>
            <ta e="T1126" id="Seg_10330" s="T1125">красный.[NOM.SG]</ta>
            <ta e="T1128" id="Seg_10331" s="T1127">ну</ta>
            <ta e="T1129" id="Seg_10332" s="T1128">принести-IMP.2SG</ta>
            <ta e="T1130" id="Seg_10333" s="T1129">красный.[NOM.SG]</ta>
            <ta e="T1134" id="Seg_10334" s="T1133">баня-LAT</ta>
            <ta e="T1135" id="Seg_10335" s="T1134">прийти-PRS.[3SG]</ta>
            <ta e="T1136" id="Seg_10336" s="T1135">там</ta>
            <ta e="T1137" id="Seg_10337" s="T1136">очень</ta>
            <ta e="T1138" id="Seg_10338" s="T1137">вода.[NOM.SG]</ta>
            <ta e="T1139" id="Seg_10339" s="T1138">много</ta>
            <ta e="T1141" id="Seg_10340" s="T1140">теплый.[NOM.SG]</ta>
            <ta e="T1142" id="Seg_10341" s="T1141">вода.[NOM.SG]</ta>
            <ta e="T1143" id="Seg_10342" s="T1142">быть-PRS.[3SG]</ta>
            <ta e="T1144" id="Seg_10343" s="T1143">холодный.[NOM.SG]</ta>
            <ta e="T1145" id="Seg_10344" s="T1144">мыло.[NOM.SG]</ta>
            <ta e="T1146" id="Seg_10345" s="T1145">быть-PRS.[3SG]</ta>
            <ta e="T1148" id="Seg_10346" s="T1147">теплый.[NOM.SG]</ta>
            <ta e="T1149" id="Seg_10347" s="T1148">теплый-FACT-FUT-2SG</ta>
            <ta e="T1150" id="Seg_10348" s="T1149">PTCL</ta>
            <ta e="T1154" id="Seg_10349" s="T1153">нос-NOM/GEN/ACC.2SG</ta>
            <ta e="T1155" id="Seg_10350" s="T1154">NEG</ta>
            <ta e="T1156" id="Seg_10351" s="T1155">болеть-PTCP.[NOM.SG]</ta>
            <ta e="T1157" id="Seg_10352" s="T1156">и</ta>
            <ta e="T1158" id="Seg_10353" s="T1157">%%-NOM/GEN/ACC.3PL</ta>
            <ta e="T1159" id="Seg_10354" s="T1158">течь-PTCP-PL</ta>
            <ta e="T1163" id="Seg_10355" s="T1162">тот.[NOM.SG]</ta>
            <ta e="T1164" id="Seg_10356" s="T1163">заплатка.[NOM.SG]</ta>
            <ta e="T1165" id="Seg_10357" s="T1164">а</ta>
            <ta e="T1166" id="Seg_10358" s="T1165">куда</ta>
            <ta e="T1167" id="Seg_10359" s="T1166">этот-ACC</ta>
            <ta e="T1168" id="Seg_10360" s="T1167">заплатка.[NOM.SG]</ta>
            <ta e="T1170" id="Seg_10361" s="T1169">зад-LAT/LOC.3SG</ta>
            <ta e="T1171" id="Seg_10362" s="T1170">сажать-INF.LAT</ta>
            <ta e="T1172" id="Seg_10363" s="T1171">что.ли</ta>
            <ta e="T1174" id="Seg_10364" s="T1173">выбросить-%%-IMP.2SG</ta>
            <ta e="T1175" id="Seg_10365" s="T1174">NEG</ta>
            <ta e="T1176" id="Seg_10366" s="T1175">нужно</ta>
            <ta e="T1177" id="Seg_10367" s="T1176">я.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_10368" s="T1">pers</ta>
            <ta e="T3" id="Seg_10369" s="T2">adv</ta>
            <ta e="T4" id="Seg_10370" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_10371" s="T4">n-n:case.poss</ta>
            <ta e="T7" id="Seg_10372" s="T6">pers</ta>
            <ta e="T8" id="Seg_10373" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_10374" s="T8">aux-v:mood.pn</ta>
            <ta e="T10" id="Seg_10375" s="T9">v-v:n.fin</ta>
            <ta e="T11" id="Seg_10376" s="T10">pers</ta>
            <ta e="T14" id="Seg_10377" s="T13">v-v:n.fin</ta>
            <ta e="T16" id="Seg_10378" s="T15">n-n:case.poss</ta>
            <ta e="T17" id="Seg_10379" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_10380" s="T17">v-v:tense.[v:pn]</ta>
            <ta e="T19" id="Seg_10381" s="T18">conj</ta>
            <ta e="T20" id="Seg_10382" s="T19">pers</ta>
            <ta e="T21" id="Seg_10383" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_10384" s="T21">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_10385" s="T23">dempro-n:num</ta>
            <ta e="T25" id="Seg_10386" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_10387" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_10388" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T29" id="Seg_10389" s="T28">propr.[n:case]</ta>
            <ta e="T30" id="Seg_10390" s="T29">propr-n:case.poss-n:case</ta>
            <ta e="T32" id="Seg_10391" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_10392" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_10393" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_10394" s="T36">n.[n:case]</ta>
            <ta e="T38" id="Seg_10395" s="T37">v-v:ins-v:mood.pn</ta>
            <ta e="T39" id="Seg_10396" s="T38">v-v:mood.pn</ta>
            <ta e="T41" id="Seg_10397" s="T40">n.[n:case]</ta>
            <ta e="T42" id="Seg_10398" s="T41">v-v:ins-v:mood.pn</ta>
            <ta e="T44" id="Seg_10399" s="T43">pers</ta>
            <ta e="T45" id="Seg_10400" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_10401" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_10402" s="T46">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_10403" s="T48">conj</ta>
            <ta e="T50" id="Seg_10404" s="T49">adv</ta>
            <ta e="T1178" id="Seg_10405" s="T50">v-v:n-fin</ta>
            <ta e="T51" id="Seg_10406" s="T1178">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_10407" s="T51">n-n:case.poss</ta>
            <ta e="T56" id="Seg_10408" s="T55">pers</ta>
            <ta e="T57" id="Seg_10409" s="T56">adv</ta>
            <ta e="T58" id="Seg_10410" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_10411" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_10412" s="T59">dempro.[n:case]</ta>
            <ta e="T61" id="Seg_10413" s="T60">v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_10414" s="T62">dempro.[n:case]</ta>
            <ta e="T64" id="Seg_10415" s="T63">v-v&gt;v.[v:pn]</ta>
            <ta e="T65" id="Seg_10416" s="T64">que.[n:case]</ta>
            <ta e="T66" id="Seg_10417" s="T65">adv</ta>
            <ta e="T67" id="Seg_10418" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_10419" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_10420" s="T68">pers</ta>
            <ta e="T70" id="Seg_10421" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_10422" s="T70">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_10423" s="T71">pers</ta>
            <ta e="T73" id="Seg_10424" s="T72">adv</ta>
            <ta e="T74" id="Seg_10425" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_10426" s="T74">conj</ta>
            <ta e="T76" id="Seg_10427" s="T75">v-v:n.fin</ta>
            <ta e="T77" id="Seg_10428" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_10429" s="T77">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_10430" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_10431" s="T80">v-v:mood.pn</ta>
            <ta e="T83" id="Seg_10432" s="T82">pers</ta>
            <ta e="T84" id="Seg_10433" s="T83">que.[n:case]</ta>
            <ta e="T86" id="Seg_10434" s="T85">que.[n:case]</ta>
            <ta e="T87" id="Seg_10435" s="T86">pers</ta>
            <ta e="T88" id="Seg_10436" s="T87">pers</ta>
            <ta e="T89" id="Seg_10437" s="T88">n-n:case.poss</ta>
            <ta e="T90" id="Seg_10438" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_10439" s="T90">ptcl</ta>
            <ta e="T93" id="Seg_10440" s="T92">pers</ta>
            <ta e="T1185" id="Seg_10441" s="T93">ptcl</ta>
            <ta e="T94" id="Seg_10442" s="T1185">n.[n:case]</ta>
            <ta e="T95" id="Seg_10443" s="T94">v-v&gt;v-v:pn</ta>
            <ta e="T97" id="Seg_10444" s="T96">adv</ta>
            <ta e="T98" id="Seg_10445" s="T97">v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_10446" s="T98">pers</ta>
            <ta e="T101" id="Seg_10447" s="T100">v-v:n.fin</ta>
            <ta e="T102" id="Seg_10448" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_10449" s="T103">conj</ta>
            <ta e="T105" id="Seg_10450" s="T104">pers</ta>
            <ta e="T106" id="Seg_10451" s="T105">n-n:case.poss</ta>
            <ta e="T107" id="Seg_10452" s="T106">v-v:mood.pn</ta>
            <ta e="T111" id="Seg_10453" s="T110">n-n:case.poss</ta>
            <ta e="T113" id="Seg_10454" s="T112">num.[n:case]</ta>
            <ta e="T114" id="Seg_10455" s="T113">n.[n:case]</ta>
            <ta e="T115" id="Seg_10456" s="T114">v-v:tense.[v:pn]</ta>
            <ta e="T117" id="Seg_10457" s="T116">conj</ta>
            <ta e="T118" id="Seg_10458" s="T117">pers</ta>
            <ta e="T119" id="Seg_10459" s="T118">n-n:case.poss</ta>
            <ta e="T120" id="Seg_10460" s="T119">v-v:mood.pn</ta>
            <ta e="T122" id="Seg_10461" s="T121">conj</ta>
            <ta e="T123" id="Seg_10462" s="T122">n.[n:case]</ta>
            <ta e="T124" id="Seg_10463" s="T123">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_10464" s="T125">conj</ta>
            <ta e="T127" id="Seg_10465" s="T126">dempro.[n:case]</ta>
            <ta e="T128" id="Seg_10466" s="T127">v-v&gt;v.[v:pn]</ta>
            <ta e="T129" id="Seg_10467" s="T128">conj</ta>
            <ta e="T130" id="Seg_10468" s="T129">n.[n:case]</ta>
            <ta e="T131" id="Seg_10469" s="T130">dempro-n:num-n:case</ta>
            <ta e="T133" id="Seg_10470" s="T132">conj</ta>
            <ta e="T134" id="Seg_10471" s="T133">pers-n:case</ta>
            <ta e="T135" id="Seg_10472" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_10473" s="T135">n.[n:case]</ta>
            <ta e="T138" id="Seg_10474" s="T137">v-v:mood.pn</ta>
            <ta e="T139" id="Seg_10475" s="T138">v-v:mood.pn</ta>
            <ta e="T143" id="Seg_10476" s="T142">adv</ta>
            <ta e="T144" id="Seg_10477" s="T143">dempro.[n:case]</ta>
            <ta e="T145" id="Seg_10478" s="T144">v-v&gt;v.[v:pn]</ta>
            <ta e="T146" id="Seg_10479" s="T145">conj</ta>
            <ta e="T147" id="Seg_10480" s="T146">pers</ta>
            <ta e="T148" id="Seg_10481" s="T147">n-n:case.poss</ta>
            <ta e="T1179" id="Seg_10482" s="T148">v-v:n-fin</ta>
            <ta e="T149" id="Seg_10483" s="T1179">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_10484" s="T152">pers</ta>
            <ta e="T1183" id="Seg_10485" s="T153">propr</ta>
            <ta e="T154" id="Seg_10486" s="T1183">n-n:case</ta>
            <ta e="T155" id="Seg_10487" s="T154">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_10488" s="T156">conj</ta>
            <ta e="T158" id="Seg_10489" s="T157">dempro.[n:case]</ta>
            <ta e="T159" id="Seg_10490" s="T158">n.[n:case]</ta>
            <ta e="T160" id="Seg_10491" s="T159">v-v:tense.[v:pn]</ta>
            <ta e="T161" id="Seg_10492" s="T160">dempro-n:case</ta>
            <ta e="T162" id="Seg_10493" s="T161">n-n:case.poss</ta>
            <ta e="T163" id="Seg_10494" s="T162">adv</ta>
            <ta e="T164" id="Seg_10495" s="T163">n.[n:case]</ta>
            <ta e="T165" id="Seg_10496" s="T164">v-v&gt;v.[v:pn]</ta>
            <ta e="T167" id="Seg_10497" s="T166">v-v:tense.[v:pn]</ta>
            <ta e="T169" id="Seg_10498" s="T168">conj</ta>
            <ta e="T1180" id="Seg_10499" s="T169">v-v:n-fin</ta>
            <ta e="T170" id="Seg_10500" s="T1180">v-v:tense.[v:pn]</ta>
            <ta e="T172" id="Seg_10501" s="T171">pers</ta>
            <ta e="T173" id="Seg_10502" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_10503" s="T173">conj</ta>
            <ta e="T175" id="Seg_10504" s="T174">que</ta>
            <ta e="T176" id="Seg_10505" s="T175">pers</ta>
            <ta e="T178" id="Seg_10506" s="T177">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_10507" s="T179">dempro-n:num</ta>
            <ta e="T181" id="Seg_10508" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_10509" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_10510" s="T182">adv</ta>
            <ta e="T185" id="Seg_10511" s="T184">adv</ta>
            <ta e="T186" id="Seg_10512" s="T185">pers</ta>
            <ta e="T187" id="Seg_10513" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_10514" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_10515" s="T188">dempro-n:case</ta>
            <ta e="T190" id="Seg_10516" s="T189">pers</ta>
            <ta e="T1182" id="Seg_10517" s="T190">v-v:n.fin</ta>
            <ta e="T191" id="Seg_10518" s="T1182">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_10519" s="T191">conj</ta>
            <ta e="T193" id="Seg_10520" s="T192">pers</ta>
            <ta e="T194" id="Seg_10521" s="T193">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_10522" s="T194">conj</ta>
            <ta e="T196" id="Seg_10523" s="T195">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_10524" s="T197">conj</ta>
            <ta e="T199" id="Seg_10525" s="T198">pers</ta>
            <ta e="T200" id="Seg_10526" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_10527" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_10528" s="T201">v-v:mood.pn</ta>
            <ta e="T204" id="Seg_10529" s="T203">que.[n:case]</ta>
            <ta e="T205" id="Seg_10530" s="T204">conj</ta>
            <ta e="T207" id="Seg_10531" s="T206">conj</ta>
            <ta e="T208" id="Seg_10532" s="T207">v-v:mood.pn</ta>
            <ta e="T210" id="Seg_10533" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_10534" s="T210">pers</ta>
            <ta e="T212" id="Seg_10535" s="T211">n.[n:case]</ta>
            <ta e="T213" id="Seg_10536" s="T212">v-v&gt;v-v:pn</ta>
            <ta e="T215" id="Seg_10537" s="T214">conj</ta>
            <ta e="T216" id="Seg_10538" s="T215">pers</ta>
            <ta e="T218" id="Seg_10539" s="T217">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_10540" s="T219">adv</ta>
            <ta e="T221" id="Seg_10541" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_10542" s="T221">conj</ta>
            <ta e="T223" id="Seg_10543" s="T222">n-n:case</ta>
            <ta e="T226" id="Seg_10544" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_10545" s="T226">v-v:tense-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_10546" s="T240">dempro.[n:case]</ta>
            <ta e="T242" id="Seg_10547" s="T241">n.[n:case]</ta>
            <ta e="T243" id="Seg_10548" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_10549" s="T243">pers</ta>
            <ta e="T245" id="Seg_10550" s="T244">v-v:mood.pn</ta>
            <ta e="T246" id="Seg_10551" s="T245">n.[n:case]</ta>
            <ta e="T247" id="Seg_10552" s="T246">v-v:mood.pn</ta>
            <ta e="T248" id="Seg_10553" s="T247">pers</ta>
            <ta e="T250" id="Seg_10554" s="T249">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_10555" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_10556" s="T251">adv</ta>
            <ta e="T254" id="Seg_10557" s="T252">n-n:case.poss=ptcl</ta>
            <ta e="T256" id="Seg_10558" s="T254">n-n:num=ptcl</ta>
            <ta e="T257" id="Seg_10559" s="T256">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_10560" s="T258">pers</ta>
            <ta e="T260" id="Seg_10561" s="T259">adv</ta>
            <ta e="T261" id="Seg_10562" s="T260">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_10563" s="T262">n.[n:case]</ta>
            <ta e="T265" id="Seg_10564" s="T264">adv</ta>
            <ta e="T266" id="Seg_10565" s="T265">n-n:num-n:case</ta>
            <ta e="T267" id="Seg_10566" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_10567" s="T267">conj</ta>
            <ta e="T269" id="Seg_10568" s="T268">%%</ta>
            <ta e="T270" id="Seg_10569" s="T269">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_10570" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_10571" s="T271">dempro-n:case</ta>
            <ta e="T273" id="Seg_10572" s="T272">conj</ta>
            <ta e="T274" id="Seg_10573" s="T273">adv</ta>
            <ta e="T275" id="Seg_10574" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_10575" s="T275">v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_10576" s="T276">n-n:num-n:case</ta>
            <ta e="T279" id="Seg_10577" s="T278">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_10578" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_10579" s="T280">v-v&gt;v-v:pn</ta>
            <ta e="T282" id="Seg_10580" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_10581" s="T282">adv</ta>
            <ta e="T284" id="Seg_10582" s="T283">v-v:mood.pn</ta>
            <ta e="T286" id="Seg_10583" s="T285">n-n:case.poss</ta>
            <ta e="T287" id="Seg_10584" s="T286">conj</ta>
            <ta e="T288" id="Seg_10585" s="T287">n-n:num-n:case</ta>
            <ta e="T289" id="Seg_10586" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_10587" s="T289">conj</ta>
            <ta e="T291" id="Seg_10588" s="T290">adj</ta>
            <ta e="T292" id="Seg_10589" s="T291">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_10590" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_10591" s="T294">adv</ta>
            <ta e="T296" id="Seg_10592" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_10593" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_10594" s="T297">adv</ta>
            <ta e="T300" id="Seg_10595" s="T299">adj.[n:case]</ta>
            <ta e="T301" id="Seg_10596" s="T300">quant</ta>
            <ta e="T302" id="Seg_10597" s="T301">v-v:tense.[v:pn]</ta>
            <ta e="T303" id="Seg_10598" s="T302">n-n:case.poss</ta>
            <ta e="T305" id="Seg_10599" s="T304">pers</ta>
            <ta e="T306" id="Seg_10600" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_10601" s="T306">adv</ta>
            <ta e="T308" id="Seg_10602" s="T307">v-v:ins-v:mood.pn</ta>
            <ta e="T309" id="Seg_10603" s="T308">n.[n:case]</ta>
            <ta e="T311" id="Seg_10604" s="T310">dempro-n:case</ta>
            <ta e="T313" id="Seg_10605" s="T312">v-v:mood.pn</ta>
            <ta e="T317" id="Seg_10606" s="T316">num.[n:case]</ta>
            <ta e="T319" id="Seg_10607" s="T318">n.[n:case]</ta>
            <ta e="T320" id="Seg_10608" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_10609" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_10610" s="T321">n-n:case</ta>
            <ta e="T324" id="Seg_10611" s="T323">num.[n:case]</ta>
            <ta e="T325" id="Seg_10612" s="T324">n.[n:case]</ta>
            <ta e="T326" id="Seg_10613" s="T325">v-v:tense.[v:pn]</ta>
            <ta e="T328" id="Seg_10614" s="T327">adv</ta>
            <ta e="T329" id="Seg_10615" s="T328">n.[n:case]</ta>
            <ta e="T330" id="Seg_10616" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_10617" s="T330">n-n:case</ta>
            <ta e="T333" id="Seg_10618" s="T332">n.[n:case]</ta>
            <ta e="T334" id="Seg_10619" s="T333">v-v:tense.[v:pn]</ta>
            <ta e="T336" id="Seg_10620" s="T335">v.[v:pn]</ta>
            <ta e="T337" id="Seg_10621" s="T336">n.[n:case]</ta>
            <ta e="T338" id="Seg_10622" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_10623" s="T338">num.[n:case]</ta>
            <ta e="T340" id="Seg_10624" s="T339">n.[n:case]</ta>
            <ta e="T341" id="Seg_10625" s="T340">v-v:tense.[v:pn]</ta>
            <ta e="T343" id="Seg_10626" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_10627" s="T343">n.[n:case]</ta>
            <ta e="T345" id="Seg_10628" s="T344">adv</ta>
            <ta e="T346" id="Seg_10629" s="T345">dempro.[n:case]</ta>
            <ta e="T347" id="Seg_10630" s="T346">n-n:case.poss</ta>
            <ta e="T348" id="Seg_10631" s="T347">n.[n:case]</ta>
            <ta e="T349" id="Seg_10632" s="T348">v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_10633" s="T350">n.[n:case]</ta>
            <ta e="T352" id="Seg_10634" s="T351">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_10635" s="T353">adv</ta>
            <ta e="T355" id="Seg_10636" s="T354">n-n:case</ta>
            <ta e="T356" id="Seg_10637" s="T355">n-n:case.poss</ta>
            <ta e="T357" id="Seg_10638" s="T356">v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_10639" s="T357">n.[n:case]</ta>
            <ta e="T360" id="Seg_10640" s="T359">quant.[n:case]</ta>
            <ta e="T361" id="Seg_10641" s="T360">quant.[n:case]</ta>
            <ta e="T362" id="Seg_10642" s="T361">n.[n:case]</ta>
            <ta e="T364" id="Seg_10643" s="T363">adv</ta>
            <ta e="T366" id="Seg_10644" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_10645" s="T366">n-n:case.poss</ta>
            <ta e="T368" id="Seg_10646" s="T367">v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_10647" s="T368">n.[n:case]</ta>
            <ta e="T370" id="Seg_10648" s="T369">conj</ta>
            <ta e="T371" id="Seg_10649" s="T370">n.[n:case]</ta>
            <ta e="T372" id="Seg_10650" s="T371">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_10651" s="T375">adv</ta>
            <ta e="T377" id="Seg_10652" s="T376">num.[n:case]</ta>
            <ta e="T378" id="Seg_10653" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_10654" s="T378">adv</ta>
            <ta e="T381" id="Seg_10655" s="T380">n.[n:case]</ta>
            <ta e="T382" id="Seg_10656" s="T381">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_10657" s="T383">n.[n:case]</ta>
            <ta e="T385" id="Seg_10658" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_10659" s="T385">v-v:mood.pn</ta>
            <ta e="T387" id="Seg_10660" s="T386">n.[n:case]</ta>
            <ta e="T389" id="Seg_10661" s="T388">pers</ta>
            <ta e="T390" id="Seg_10662" s="T389">pers</ta>
            <ta e="T391" id="Seg_10663" s="T390">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_10664" s="T392">conj</ta>
            <ta e="T394" id="Seg_10665" s="T393">refl</ta>
            <ta e="T395" id="Seg_10666" s="T394">refl-n:num-n:case.poss</ta>
            <ta e="T396" id="Seg_10667" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_10668" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_10669" s="T397">conj</ta>
            <ta e="T399" id="Seg_10670" s="T398">adv</ta>
            <ta e="T401" id="Seg_10671" s="T400">n.[n:case]</ta>
            <ta e="T407" id="Seg_10672" s="T406">pers</ta>
            <ta e="T408" id="Seg_10673" s="T407">adv</ta>
            <ta e="T409" id="Seg_10674" s="T408">aux-v:pn</ta>
            <ta e="T410" id="Seg_10675" s="T409">ptcl</ta>
            <ta e="T412" id="Seg_10676" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_10677" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_10678" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_10679" s="T414">n-n:case.poss</ta>
            <ta e="T416" id="Seg_10680" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_10681" s="T416">adj.[n:case]</ta>
            <ta e="T421" id="Seg_10682" s="T420">pers</ta>
            <ta e="T422" id="Seg_10683" s="T421">v-v&gt;v.[v:pn]</ta>
            <ta e="T428" id="Seg_10684" s="T427">adv</ta>
            <ta e="T429" id="Seg_10685" s="T428">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_10686" s="T429">n-n:num</ta>
            <ta e="T432" id="Seg_10687" s="T431">v-v:tense-v:pn</ta>
            <ta e="T433" id="Seg_10688" s="T432">conj</ta>
            <ta e="T434" id="Seg_10689" s="T433">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_10690" s="T435">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_10691" s="T436">conj</ta>
            <ta e="T438" id="Seg_10692" s="T437">dempro-n:num</ta>
            <ta e="T439" id="Seg_10693" s="T438">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_10694" s="T439">que=ptcl</ta>
            <ta e="T444" id="Seg_10695" s="T443">n.[n:case]</ta>
            <ta e="T445" id="Seg_10696" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_10697" s="T445">n-n:num</ta>
            <ta e="T447" id="Seg_10698" s="T446">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T449" id="Seg_10699" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_10700" s="T449">v-v:pn</ta>
            <ta e="T451" id="Seg_10701" s="T450">adv</ta>
            <ta e="T452" id="Seg_10702" s="T451">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T456" id="Seg_10703" s="T455">adv</ta>
            <ta e="T457" id="Seg_10704" s="T456">quant</ta>
            <ta e="T458" id="Seg_10705" s="T457">n-n:num</ta>
            <ta e="T460" id="Seg_10706" s="T459">conj</ta>
            <ta e="T461" id="Seg_10707" s="T460">conj</ta>
            <ta e="T462" id="Seg_10708" s="T461">adj</ta>
            <ta e="T463" id="Seg_10709" s="T462">adj</ta>
            <ta e="T464" id="Seg_10710" s="T463">adv</ta>
            <ta e="T468" id="Seg_10711" s="T467">quant</ta>
            <ta e="T470" id="Seg_10712" s="T469">v-v:n.fin</ta>
            <ta e="T471" id="Seg_10713" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_10714" s="T471">conj</ta>
            <ta e="T473" id="Seg_10715" s="T472">adv</ta>
            <ta e="T474" id="Seg_10716" s="T473">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_10717" s="T477">n.[n:case]</ta>
            <ta e="T479" id="Seg_10718" s="T478">adv</ta>
            <ta e="T480" id="Seg_10719" s="T479">adj.[n:case]</ta>
            <ta e="T482" id="Seg_10720" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_10721" s="T482">que.[n:case]</ta>
            <ta e="T484" id="Seg_10722" s="T483">pers</ta>
            <ta e="T485" id="Seg_10723" s="T484">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T486" id="Seg_10724" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_10725" s="T486">que.[n:case]</ta>
            <ta e="T491" id="Seg_10726" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_10727" s="T491">pers</ta>
            <ta e="T494" id="Seg_10728" s="T493">v-v:tense.[v:pn]</ta>
            <ta e="T501" id="Seg_10729" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_10730" s="T501">n.[n:case]</ta>
            <ta e="T503" id="Seg_10731" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_10732" s="T503">v-v:n.fin</ta>
            <ta e="T508" id="Seg_10733" s="T507">adv</ta>
            <ta e="T509" id="Seg_10734" s="T508">n</ta>
            <ta e="T510" id="Seg_10735" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_10736" s="T510">n.[n:case]</ta>
            <ta e="T512" id="Seg_10737" s="T511">v-v:n.fin</ta>
            <ta e="T513" id="Seg_10738" s="T512">n.[n:case]</ta>
            <ta e="T514" id="Seg_10739" s="T513">v-v:n.fin</ta>
            <ta e="T516" id="Seg_10740" s="T515">adv</ta>
            <ta e="T517" id="Seg_10741" s="T516">v-v:tense-v:pn</ta>
            <ta e="T519" id="Seg_10742" s="T518">v-v&gt;v-v:n.fin</ta>
            <ta e="T520" id="Seg_10743" s="T519">ptcl</ta>
            <ta e="T522" id="Seg_10744" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_10745" s="T522">adj</ta>
            <ta e="T524" id="Seg_10746" s="T523">n.[n:case]</ta>
            <ta e="T525" id="Seg_10747" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_10748" s="T525">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_10749" s="T529">n-n:ins-n:case.poss-n:case</ta>
            <ta e="T531" id="Seg_10750" s="T530">n.[n:case]</ta>
            <ta e="T532" id="Seg_10751" s="T531">v-v:tense-v:pn</ta>
            <ta e="T533" id="Seg_10752" s="T532">v-v:tense-v:pn</ta>
            <ta e="T535" id="Seg_10753" s="T534">adv</ta>
            <ta e="T536" id="Seg_10754" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_10755" s="T536">v.[v:pn]</ta>
            <ta e="T538" id="Seg_10756" s="T537">n-n:case.poss</ta>
            <ta e="T539" id="Seg_10757" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_10758" s="T539">v-v:tense.[v:pn]</ta>
            <ta e="T542" id="Seg_10759" s="T541">conj</ta>
            <ta e="T543" id="Seg_10760" s="T542">n.[n:case]</ta>
            <ta e="T544" id="Seg_10761" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_10762" s="T544">v-v:tense.[v:pn]</ta>
            <ta e="T548" id="Seg_10763" s="T547">adv</ta>
            <ta e="T549" id="Seg_10764" s="T548">n-n:case</ta>
            <ta e="T550" id="Seg_10765" s="T549">n-n:case.poss</ta>
            <ta e="T551" id="Seg_10766" s="T550">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_10767" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_10768" s="T553">adv</ta>
            <ta e="T555" id="Seg_10769" s="T554">n.[n:case]</ta>
            <ta e="T556" id="Seg_10770" s="T555">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_10771" s="T559">pers</ta>
            <ta e="T561" id="Seg_10772" s="T560">v-v:tense-v:pn</ta>
            <ta e="T562" id="Seg_10773" s="T561">propr-n:case</ta>
            <ta e="T1186" id="Seg_10774" s="T562">n-n:num</ta>
            <ta e="T564" id="Seg_10775" s="T563">v-v:tense-v:pn</ta>
            <ta e="T567" id="Seg_10776" s="T566">num</ta>
            <ta e="T568" id="Seg_10777" s="T567">n.[n:case]</ta>
            <ta e="T570" id="Seg_10778" s="T569">adv</ta>
            <ta e="T571" id="Seg_10779" s="T570">n-n:case.poss</ta>
            <ta e="T572" id="Seg_10780" s="T571">pers-n:case</ta>
            <ta e="T573" id="Seg_10781" s="T572">v-v:tense.[v:pn]</ta>
            <ta e="T575" id="Seg_10782" s="T574">dempro.[n:case]</ta>
            <ta e="T576" id="Seg_10783" s="T575">pers</ta>
            <ta e="T577" id="Seg_10784" s="T576">v-v:tense.[v:pn]</ta>
            <ta e="T578" id="Seg_10785" s="T577">v-v:tense.[v:pn]</ta>
            <ta e="T579" id="Seg_10786" s="T578">v-v:tense.[v:pn]</ta>
            <ta e="T580" id="Seg_10787" s="T579">n-n:case.poss-n:case</ta>
            <ta e="T582" id="Seg_10788" s="T581">pers</ta>
            <ta e="T584" id="Seg_10789" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_10790" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_10791" s="T585">v-v:n.fin</ta>
            <ta e="T587" id="Seg_10792" s="T586">adv</ta>
            <ta e="T588" id="Seg_10793" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_10794" s="T588">n-n:case</ta>
            <ta e="T591" id="Seg_10795" s="T590">conj</ta>
            <ta e="T592" id="Seg_10796" s="T591">adv</ta>
            <ta e="T593" id="Seg_10797" s="T592">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_10798" s="T594">adv</ta>
            <ta e="T596" id="Seg_10799" s="T595">n-n:case</ta>
            <ta e="T598" id="Seg_10800" s="T597">conj</ta>
            <ta e="T599" id="Seg_10801" s="T598">adv</ta>
            <ta e="T600" id="Seg_10802" s="T599">n-n:case</ta>
            <ta e="T602" id="Seg_10803" s="T601">adv</ta>
            <ta e="T603" id="Seg_10804" s="T602">n-n&gt;adj.[n:case]</ta>
            <ta e="T604" id="Seg_10805" s="T603">n.[n:case]</ta>
            <ta e="T606" id="Seg_10806" s="T605">n.[n:case]</ta>
            <ta e="T607" id="Seg_10807" s="T606">n.[n:case]</ta>
            <ta e="T608" id="Seg_10808" s="T607">adv</ta>
            <ta e="T609" id="Seg_10809" s="T608">adv</ta>
            <ta e="T610" id="Seg_10810" s="T609">v-v:tense-v:pn</ta>
            <ta e="T614" id="Seg_10811" s="T613">adv</ta>
            <ta e="T615" id="Seg_10812" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_10813" s="T615">n-n:case</ta>
            <ta e="T618" id="Seg_10814" s="T617">n-n:ins-n:case</ta>
            <ta e="T619" id="Seg_10815" s="T618">v-v:n.fin</ta>
            <ta e="T621" id="Seg_10816" s="T620">adv</ta>
            <ta e="T622" id="Seg_10817" s="T621">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_10818" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_10819" s="T623">n-n:case</ta>
            <ta e="T626" id="Seg_10820" s="T625">adv</ta>
            <ta e="T628" id="Seg_10821" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_10822" s="T628">conj</ta>
            <ta e="T630" id="Seg_10823" s="T629">v-v:tense-v:pn</ta>
            <ta e="T632" id="Seg_10824" s="T631">conj</ta>
            <ta e="T633" id="Seg_10825" s="T632">adv</ta>
            <ta e="T634" id="Seg_10826" s="T633">n-n:case.poss</ta>
            <ta e="T635" id="Seg_10827" s="T634">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_10828" s="T638">n.[n:case]</ta>
            <ta e="T640" id="Seg_10829" s="T639">pers</ta>
            <ta e="T641" id="Seg_10830" s="T640">n.[n:case]</ta>
            <ta e="T642" id="Seg_10831" s="T641">quant</ta>
            <ta e="T643" id="Seg_10832" s="T642">v-v:tense.[v:pn]</ta>
            <ta e="T644" id="Seg_10833" s="T643">pers</ta>
            <ta e="T1184" id="Seg_10834" s="T645">propr</ta>
            <ta e="T646" id="Seg_10835" s="T1184">n-n:case</ta>
            <ta e="T647" id="Seg_10836" s="T646">num.[n:case]</ta>
            <ta e="T648" id="Seg_10837" s="T647">n.[n:case]</ta>
            <ta e="T649" id="Seg_10838" s="T648">v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_10839" s="T652">n-n:num</ta>
            <ta e="T654" id="Seg_10840" s="T653">v-v:tense-v:pn</ta>
            <ta e="T655" id="Seg_10841" s="T654">n-n:case.poss</ta>
            <ta e="T656" id="Seg_10842" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_10843" s="T656">dempro-n:case</ta>
            <ta e="T658" id="Seg_10844" s="T657">n-n:case.poss</ta>
            <ta e="T659" id="Seg_10845" s="T658">v-v:n.fin</ta>
            <ta e="T660" id="Seg_10846" s="T659">conj</ta>
            <ta e="T662" id="Seg_10847" s="T661">%%-v:n.fin</ta>
            <ta e="T666" id="Seg_10848" s="T665">v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_10849" s="T666">v-v:tense-v:pn</ta>
            <ta e="T668" id="Seg_10850" s="T667">que=ptcl</ta>
            <ta e="T669" id="Seg_10851" s="T668">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10852" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_10853" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_10854" s="T674">n.[n:case]</ta>
            <ta e="T676" id="Seg_10855" s="T675">dempro.[n:case]</ta>
            <ta e="T677" id="Seg_10856" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_10857" s="T677">v-v&gt;v.[v:pn]</ta>
            <ta e="T680" id="Seg_10858" s="T679">adv</ta>
            <ta e="T681" id="Seg_10859" s="T680">v-v&gt;v-v:pn</ta>
            <ta e="T682" id="Seg_10860" s="T681">conj</ta>
            <ta e="T684" id="Seg_10861" s="T683">v-v&gt;v-v:pn</ta>
            <ta e="T686" id="Seg_10862" s="T685">pers</ta>
            <ta e="T687" id="Seg_10863" s="T686">v-v:tense-v:pn</ta>
            <ta e="T688" id="Seg_10864" s="T687">v-v:tense-v:pn</ta>
            <ta e="T689" id="Seg_10865" s="T688">%%</ta>
            <ta e="T690" id="Seg_10866" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_10867" s="T690">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_10868" s="T695">n</ta>
            <ta e="T698" id="Seg_10869" s="T697">n.[n:case]</ta>
            <ta e="T699" id="Seg_10870" s="T698">v-v:tense.[v:pn]</ta>
            <ta e="T700" id="Seg_10871" s="T699">v-v:tense.[v:pn]</ta>
            <ta e="T701" id="Seg_10872" s="T700">adv</ta>
            <ta e="T702" id="Seg_10873" s="T701">n.[n:case]</ta>
            <ta e="T703" id="Seg_10874" s="T702">dempro-n:case</ta>
            <ta e="T705" id="Seg_10875" s="T704">n.[n:case]</ta>
            <ta e="T708" id="Seg_10876" s="T707">v-v:tense.[v:pn]</ta>
            <ta e="T709" id="Seg_10877" s="T708">n-n:case.poss</ta>
            <ta e="T710" id="Seg_10878" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_10879" s="T710">v-v&gt;v.[v:pn]</ta>
            <ta e="T713" id="Seg_10880" s="T712">dempro.[n:case]</ta>
            <ta e="T714" id="Seg_10881" s="T713">v-v&gt;v.[v:pn]</ta>
            <ta e="T715" id="Seg_10882" s="T714">n.[n:case]</ta>
            <ta e="T717" id="Seg_10883" s="T716">v-v:mood.pn</ta>
            <ta e="T718" id="Seg_10884" s="T717">dempro.[n:case]</ta>
            <ta e="T719" id="Seg_10885" s="T718">n.[n:case]</ta>
            <ta e="T723" id="Seg_10886" s="T722">pers</ta>
            <ta e="T724" id="Seg_10887" s="T723">quant</ta>
            <ta e="T725" id="Seg_10888" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_10889" s="T725">v-v:n.fin</ta>
            <ta e="T727" id="Seg_10890" s="T726">adv</ta>
            <ta e="T728" id="Seg_10891" s="T727">dempro.[n:case]</ta>
            <ta e="T729" id="Seg_10892" s="T728">n-n:case</ta>
            <ta e="T730" id="Seg_10893" s="T729">v-v:tense.[v:pn]</ta>
            <ta e="T732" id="Seg_10894" s="T731">n.[n:case]</ta>
            <ta e="T733" id="Seg_10895" s="T732">v-v:ins-v:mood.pn</ta>
            <ta e="T734" id="Seg_10896" s="T733">n-n:case</ta>
            <ta e="T736" id="Seg_10897" s="T735">v-v:mood.pn</ta>
            <ta e="T740" id="Seg_10898" s="T739">pers</ta>
            <ta e="T741" id="Seg_10899" s="T740">quant</ta>
            <ta e="T743" id="Seg_10900" s="T742">n.[n:case]</ta>
            <ta e="T744" id="Seg_10901" s="T743">v-v&gt;v-v:n.fin</ta>
            <ta e="T745" id="Seg_10902" s="T744">conj</ta>
            <ta e="T746" id="Seg_10903" s="T745">n.[n:case]</ta>
            <ta e="T747" id="Seg_10904" s="T746">v-v:tense.[v:pn]</ta>
            <ta e="T751" id="Seg_10905" s="T750">adv</ta>
            <ta e="T752" id="Seg_10906" s="T751">dempro.[n:case]</ta>
            <ta e="T753" id="Seg_10907" s="T752">v-v:tense.[v:pn]</ta>
            <ta e="T755" id="Seg_10908" s="T754">n.[n:case]</ta>
            <ta e="T756" id="Seg_10909" s="T755">n.[n:case]</ta>
            <ta e="T757" id="Seg_10910" s="T756">v-v:ins-v:mood.pn</ta>
            <ta e="T758" id="Seg_10911" s="T757">ptcl</ta>
            <ta e="T759" id="Seg_10912" s="T758">n.[n:case]</ta>
            <ta e="T761" id="Seg_10913" s="T760">v-v:ins-v:mood.pn</ta>
            <ta e="T763" id="Seg_10914" s="T762">adv</ta>
            <ta e="T764" id="Seg_10915" s="T763">n.[n:case]</ta>
            <ta e="T765" id="Seg_10916" s="T764">v-v:tense.[v:pn]</ta>
            <ta e="T766" id="Seg_10917" s="T765">pers</ta>
            <ta e="T767" id="Seg_10918" s="T766">n-n:case.poss</ta>
            <ta e="T768" id="Seg_10919" s="T767">quant</ta>
            <ta e="T769" id="Seg_10920" s="T768">n.[n:case]</ta>
            <ta e="T771" id="Seg_10921" s="T770">adv</ta>
            <ta e="T772" id="Seg_10922" s="T771">dempro.[n:case]</ta>
            <ta e="T774" id="Seg_10923" s="T773">v-v:tense.[v:pn]</ta>
            <ta e="T775" id="Seg_10924" s="T774">n-n:case</ta>
            <ta e="T776" id="Seg_10925" s="T775">n.[n:case]</ta>
            <ta e="T777" id="Seg_10926" s="T776">n.[n:case]</ta>
            <ta e="T778" id="Seg_10927" s="T777">v-v:ins-v:mood.pn</ta>
            <ta e="T779" id="Seg_10928" s="T778">n-n:num</ta>
            <ta e="T780" id="Seg_10929" s="T779">n-n:case.poss</ta>
            <ta e="T781" id="Seg_10930" s="T780">v-v:mood.pn</ta>
            <ta e="T783" id="Seg_10931" s="T782">n.[n:case]</ta>
            <ta e="T784" id="Seg_10932" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_10933" s="T784">v-v:tense.[v:pn]</ta>
            <ta e="T787" id="Seg_10934" s="T786">adj-n:num</ta>
            <ta e="T788" id="Seg_10935" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_10936" s="T788">pers</ta>
            <ta e="T793" id="Seg_10937" s="T792">dempro.[n:case]</ta>
            <ta e="T794" id="Seg_10938" s="T793">v-v:tense.[v:pn]</ta>
            <ta e="T795" id="Seg_10939" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_10940" s="T795">n.[n:case]</ta>
            <ta e="T797" id="Seg_10941" s="T796">n.[n:case]</ta>
            <ta e="T798" id="Seg_10942" s="T797">v-v:mood.pn</ta>
            <ta e="T800" id="Seg_10943" s="T799">n-n:num</ta>
            <ta e="T801" id="Seg_10944" s="T800">conj</ta>
            <ta e="T802" id="Seg_10945" s="T801">dempro.[n:case]</ta>
            <ta e="T803" id="Seg_10946" s="T802">v-v&gt;v.[v:pn]</ta>
            <ta e="T804" id="Seg_10947" s="T803">pers</ta>
            <ta e="T805" id="Seg_10948" s="T804">quant</ta>
            <ta e="T806" id="Seg_10949" s="T805">v-v:tense.[v:pn]</ta>
            <ta e="T807" id="Seg_10950" s="T806">conj</ta>
            <ta e="T808" id="Seg_10951" s="T807">adj.[n:case]</ta>
            <ta e="T809" id="Seg_10952" s="T808">pers</ta>
            <ta e="T810" id="Seg_10953" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_10954" s="T810">adv</ta>
            <ta e="T815" id="Seg_10955" s="T814">adv</ta>
            <ta e="T816" id="Seg_10956" s="T815">v-v:tense.[v:pn]</ta>
            <ta e="T817" id="Seg_10957" s="T816">n-n:num-n:case</ta>
            <ta e="T818" id="Seg_10958" s="T817">n.[n:case]</ta>
            <ta e="T819" id="Seg_10959" s="T818">n.[n:case]</ta>
            <ta e="T820" id="Seg_10960" s="T819">v-v:ins-v:mood.pn</ta>
            <ta e="T821" id="Seg_10961" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_10962" s="T821">n-n:num</ta>
            <ta e="T823" id="Seg_10963" s="T822">v-v:mood.pn</ta>
            <ta e="T825" id="Seg_10964" s="T824">ptcl</ta>
            <ta e="T827" id="Seg_10965" s="T826">pers</ta>
            <ta e="T828" id="Seg_10966" s="T827">aux-v:pn</ta>
            <ta e="T829" id="Seg_10967" s="T828">v-v:n.fin</ta>
            <ta e="T830" id="Seg_10968" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_10969" s="T830">v-v:tense-v:pn</ta>
            <ta e="T833" id="Seg_10970" s="T832">pers</ta>
            <ta e="T834" id="Seg_10971" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_10972" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_10973" s="T835">n-n:num</ta>
            <ta e="T837" id="Seg_10974" s="T836">v-v:n.fin</ta>
            <ta e="T841" id="Seg_10975" s="T840">adv</ta>
            <ta e="T842" id="Seg_10976" s="T841">v-v:tense.[v:pn]</ta>
            <ta e="T844" id="Seg_10977" s="T843">n-n:num-n:case</ta>
            <ta e="T845" id="Seg_10978" s="T844">n-n:num</ta>
            <ta e="T846" id="Seg_10979" s="T845">n-n:num</ta>
            <ta e="T847" id="Seg_10980" s="T846">v-v&gt;v-v:pn</ta>
            <ta e="T848" id="Seg_10981" s="T847">n-n:num</ta>
            <ta e="T849" id="Seg_10982" s="T848">dempro-n:num</ta>
            <ta e="T850" id="Seg_10983" s="T849">pers</ta>
            <ta e="T852" id="Seg_10984" s="T851">refl-n:case.poss</ta>
            <ta e="T853" id="Seg_10985" s="T852">v-v&gt;v-v:pn</ta>
            <ta e="T854" id="Seg_10986" s="T853">n-n:num-n:case</ta>
            <ta e="T858" id="Seg_10987" s="T857">pers</ta>
            <ta e="T859" id="Seg_10988" s="T858">n-n:case</ta>
            <ta e="T860" id="Seg_10989" s="T859">adj</ta>
            <ta e="T861" id="Seg_10990" s="T860">v-v:n.fin</ta>
            <ta e="T863" id="Seg_10991" s="T862">adv</ta>
            <ta e="T864" id="Seg_10992" s="T863">n.[n:case]</ta>
            <ta e="T865" id="Seg_10993" s="T864">que=ptcl</ta>
            <ta e="T866" id="Seg_10994" s="T865">ptcl</ta>
            <ta e="T867" id="Seg_10995" s="T866">v-v:tense-v:pn</ta>
            <ta e="T869" id="Seg_10996" s="T868">v-v:n.fin</ta>
            <ta e="T870" id="Seg_10997" s="T869">v.[v:pn]</ta>
            <ta e="T871" id="Seg_10998" s="T870">n.[n:case]</ta>
            <ta e="T875" id="Seg_10999" s="T874">n-n:case.poss</ta>
            <ta e="T876" id="Seg_11000" s="T875">que.[n:case]</ta>
            <ta e="T877" id="Seg_11001" s="T876">v.[v:pn]</ta>
            <ta e="T878" id="Seg_11002" s="T877">que.[n:case]=ptcl</ta>
            <ta e="T879" id="Seg_11003" s="T878">ptcl</ta>
            <ta e="T880" id="Seg_11004" s="T879">v-v:tense-v:pn</ta>
            <ta e="T882" id="Seg_11005" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_11006" s="T882">v-v:ins-v:mood.pn</ta>
            <ta e="T884" id="Seg_11007" s="T883">adv</ta>
            <ta e="T885" id="Seg_11008" s="T884">v-v:tense.[v:pn]</ta>
            <ta e="T886" id="Seg_11009" s="T885">n.[n:case]</ta>
            <ta e="T887" id="Seg_11010" s="T886">v-v:mood.pn</ta>
            <ta e="T888" id="Seg_11011" s="T887">conj</ta>
            <ta e="T889" id="Seg_11012" s="T888">adv</ta>
            <ta e="T890" id="Seg_11013" s="T889">v-v:tense-v:pn</ta>
            <ta e="T894" id="Seg_11014" s="T893">dempro.[n:case]</ta>
            <ta e="T895" id="Seg_11015" s="T894">n.[n:case]</ta>
            <ta e="T896" id="Seg_11016" s="T895">adv</ta>
            <ta e="T897" id="Seg_11017" s="T896">adj</ta>
            <ta e="T898" id="Seg_11018" s="T897">v-v&gt;v.[v:pn]</ta>
            <ta e="T899" id="Seg_11019" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_11020" s="T899">adj.[n:case]</ta>
            <ta e="T902" id="Seg_11021" s="T901">adj</ta>
            <ta e="T903" id="Seg_11022" s="T902">v-v:n.fin</ta>
            <ta e="T907" id="Seg_11023" s="T906">dempro.[n:case]</ta>
            <ta e="T908" id="Seg_11024" s="T907">n.[n:case]</ta>
            <ta e="T909" id="Seg_11025" s="T908">adv</ta>
            <ta e="T910" id="Seg_11026" s="T909">quant</ta>
            <ta e="T912" id="Seg_11027" s="T911">v-v:tense.[v:pn]</ta>
            <ta e="T913" id="Seg_11028" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_11029" s="T913">que.[n:case]</ta>
            <ta e="T915" id="Seg_11030" s="T914">v-v:n.fin</ta>
            <ta e="T920" id="Seg_11031" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_11032" s="T920">que.[n:case]</ta>
            <ta e="T922" id="Seg_11033" s="T921">v-v:n.fin</ta>
            <ta e="T923" id="Seg_11034" s="T922">v-v:pn</ta>
            <ta e="T927" id="Seg_11035" s="T926">adv</ta>
            <ta e="T928" id="Seg_11036" s="T927">n-n:case</ta>
            <ta e="T930" id="Seg_11037" s="T929">n-n:num-n:case.poss</ta>
            <ta e="T931" id="Seg_11038" s="T930">adv</ta>
            <ta e="T932" id="Seg_11039" s="T931">v-%%-v:tense.[v:pn]</ta>
            <ta e="T933" id="Seg_11040" s="T932">adv</ta>
            <ta e="T934" id="Seg_11041" s="T933">num.[n:case]</ta>
            <ta e="T935" id="Seg_11042" s="T934">n-n:case.poss</ta>
            <ta e="T939" id="Seg_11043" s="T938">conj</ta>
            <ta e="T940" id="Seg_11044" s="T939">n-n:case.poss</ta>
            <ta e="T941" id="Seg_11045" s="T940">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T945" id="Seg_11046" s="T944">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T946" id="Seg_11047" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_11048" s="T946">adv</ta>
            <ta e="T948" id="Seg_11049" s="T947">v.[v:pn]</ta>
            <ta e="T949" id="Seg_11050" s="T948">v-v:n.fin</ta>
            <ta e="T953" id="Seg_11051" s="T952">pers</ta>
            <ta e="T954" id="Seg_11052" s="T953">%%</ta>
            <ta e="T955" id="Seg_11053" s="T954">v-v:tense-v:pn</ta>
            <ta e="T956" id="Seg_11054" s="T955">n.[n:case]</ta>
            <ta e="T957" id="Seg_11055" s="T956">v-v:n.fin</ta>
            <ta e="T958" id="Seg_11056" s="T957">conj</ta>
            <ta e="T959" id="Seg_11057" s="T958">adv</ta>
            <ta e="T960" id="Seg_11058" s="T959">n.[n:case]</ta>
            <ta e="T961" id="Seg_11059" s="T960">v-v:tense.[v:pn]</ta>
            <ta e="T963" id="Seg_11060" s="T962">num.[n:case]</ta>
            <ta e="T965" id="Seg_11061" s="T964">num.[n:case]</ta>
            <ta e="T966" id="Seg_11062" s="T965">n.[n:case]</ta>
            <ta e="T967" id="Seg_11063" s="T966">v-v:tense-v:pn</ta>
            <ta e="T968" id="Seg_11064" s="T967">conj</ta>
            <ta e="T969" id="Seg_11065" s="T968">ptcl</ta>
            <ta e="T970" id="Seg_11066" s="T969">adv</ta>
            <ta e="T974" id="Seg_11067" s="T973">n-n:case</ta>
            <ta e="T975" id="Seg_11068" s="T974">v-v:tense-v:pn</ta>
            <ta e="T976" id="Seg_11069" s="T975">n.[n:case]</ta>
            <ta e="T977" id="Seg_11070" s="T976">v-v&gt;v-v:pn</ta>
            <ta e="T979" id="Seg_11071" s="T978">n-n:case</ta>
            <ta e="T980" id="Seg_11072" s="T979">v-v:tense-v:pn</ta>
            <ta e="T981" id="Seg_11073" s="T980">n.[n:case]</ta>
            <ta e="T982" id="Seg_11074" s="T981">v-v&gt;v-v:pn</ta>
            <ta e="T984" id="Seg_11075" s="T983">n.[n:case]</ta>
            <ta e="T985" id="Seg_11076" s="T984">v-v:tense-v:pn</ta>
            <ta e="T986" id="Seg_11077" s="T985">n.[n:case]</ta>
            <ta e="T987" id="Seg_11078" s="T986">v-v&gt;v-v:pn</ta>
            <ta e="T989" id="Seg_11079" s="T988">n.[n:case]</ta>
            <ta e="T990" id="Seg_11080" s="T989">v-v:tense-v:pn</ta>
            <ta e="T991" id="Seg_11081" s="T990">n.[n:case]</ta>
            <ta e="T992" id="Seg_11082" s="T991">v-v&gt;v-v:pn</ta>
            <ta e="T994" id="Seg_11083" s="T993">propr-n:case</ta>
            <ta e="T995" id="Seg_11084" s="T994">v-v:tense.[v:pn]</ta>
            <ta e="T996" id="Seg_11085" s="T995">propr-n:case</ta>
            <ta e="T998" id="Seg_11086" s="T997">v-v&gt;v-v:pn</ta>
            <ta e="T1000" id="Seg_11087" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_11088" s="T1000">v-v&gt;v.[v:pn]</ta>
            <ta e="T1002" id="Seg_11089" s="T1001">n.[n:case]</ta>
            <ta e="T1003" id="Seg_11090" s="T1002">v-v&gt;v-v:pn</ta>
            <ta e="T1007" id="Seg_11091" s="T1006">pers</ta>
            <ta e="T1008" id="Seg_11092" s="T1007">pers</ta>
            <ta e="T1009" id="Seg_11093" s="T1008">n-n:case.poss</ta>
            <ta e="T1010" id="Seg_11094" s="T1009">adv</ta>
            <ta e="T1011" id="Seg_11095" s="T1010">n.[n:case]</ta>
            <ta e="T1012" id="Seg_11096" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_11097" s="T1012">que.[n:case]</ta>
            <ta e="T1014" id="Seg_11098" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_11099" s="T1014">ptcl</ta>
            <ta e="T1016" id="Seg_11100" s="T1015">v-v:tense-v:pn</ta>
            <ta e="T1018" id="Seg_11101" s="T1017">conj</ta>
            <ta e="T1019" id="Seg_11102" s="T1018">n.[n:case]</ta>
            <ta e="T1020" id="Seg_11103" s="T1019">v.[v:pn]</ta>
            <ta e="T1022" id="Seg_11104" s="T1021">v-v:ins-v:mood.pn</ta>
            <ta e="T1023" id="Seg_11105" s="T1022">n-n:case</ta>
            <ta e="T1024" id="Seg_11106" s="T1023">dempro.[n:case]</ta>
            <ta e="T1025" id="Seg_11107" s="T1024">adv</ta>
            <ta e="T1026" id="Seg_11108" s="T1025">num.[n:case]</ta>
            <ta e="T1028" id="Seg_11109" s="T1027">n.[n:case]</ta>
            <ta e="T1029" id="Seg_11110" s="T1028">num.[n:case]</ta>
            <ta e="T1030" id="Seg_11111" s="T1029">adj.[n:case]</ta>
            <ta e="T1031" id="Seg_11112" s="T1030">num.[n:case]</ta>
            <ta e="T1032" id="Seg_11113" s="T1031">adj.[n:case]</ta>
            <ta e="T1034" id="Seg_11114" s="T1033">dempro.[n:case]</ta>
            <ta e="T1035" id="Seg_11115" s="T1034">v-v:tense-v:pn</ta>
            <ta e="T1036" id="Seg_11116" s="T1035">pers</ta>
            <ta e="T1038" id="Seg_11117" s="T1037">conj</ta>
            <ta e="T1039" id="Seg_11118" s="T1038">dempro.[n:case]</ta>
            <ta e="T1040" id="Seg_11119" s="T1039">n.[n:case]</ta>
            <ta e="T1041" id="Seg_11120" s="T1040">pers</ta>
            <ta e="T1042" id="Seg_11121" s="T1041">v-v:tense.[v:pn]</ta>
            <ta e="T1043" id="Seg_11122" s="T1042">conj</ta>
            <ta e="T1181" id="Seg_11123" s="T1043">v-v:n-fin</ta>
            <ta e="T1044" id="Seg_11124" s="T1181">v-v:tense.[v:pn]</ta>
            <ta e="T1045" id="Seg_11125" s="T1044">dempro.[n:case]</ta>
            <ta e="T1046" id="Seg_11126" s="T1045">dempro-n:case</ta>
            <ta e="T1047" id="Seg_11127" s="T1046">ptcl</ta>
            <ta e="T1048" id="Seg_11128" s="T1047">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1050" id="Seg_11129" s="T1049">adv</ta>
            <ta e="T1051" id="Seg_11130" s="T1050">ptcl</ta>
            <ta e="T1052" id="Seg_11131" s="T1051">v-v:tense.[v:pn]</ta>
            <ta e="T1056" id="Seg_11132" s="T1055">pers</ta>
            <ta e="T1057" id="Seg_11133" s="T1056">v-v:n.fin</ta>
            <ta e="T1058" id="Seg_11134" s="T1057">v-v:tense-v:pn</ta>
            <ta e="T1060" id="Seg_11135" s="T1059">conj</ta>
            <ta e="T1061" id="Seg_11136" s="T1060">adv</ta>
            <ta e="T1062" id="Seg_11137" s="T1061">adv</ta>
            <ta e="T1063" id="Seg_11138" s="T1062">adj.[n:case]</ta>
            <ta e="T1064" id="Seg_11139" s="T1063">v-v:tense-v:pn</ta>
            <ta e="T1065" id="Seg_11140" s="T1064">n.[n:case]</ta>
            <ta e="T1067" id="Seg_11141" s="T1066">quant</ta>
            <ta e="T1068" id="Seg_11142" s="T1067">v-v&gt;v.[v:pn]</ta>
            <ta e="T1072" id="Seg_11143" s="T1071">n-n:case</ta>
            <ta e="T1073" id="Seg_11144" s="T1072">v-v:tense-v:pn</ta>
            <ta e="T1074" id="Seg_11145" s="T1073">n-n:case</ta>
            <ta e="T1075" id="Seg_11146" s="T1074">v-v:tense-v:pn</ta>
            <ta e="T1077" id="Seg_11147" s="T1076">v-v:tense-v:pn</ta>
            <ta e="T1078" id="Seg_11148" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_11149" s="T1078">adv</ta>
            <ta e="T1080" id="Seg_11150" s="T1079">num.[n:case]</ta>
            <ta e="T1081" id="Seg_11151" s="T1080">n</ta>
            <ta e="T1082" id="Seg_11152" s="T1081">v-v&gt;v.[v:pn]</ta>
            <ta e="T1086" id="Seg_11153" s="T1085">adv</ta>
            <ta e="T1087" id="Seg_11154" s="T1086">n-n:case.poss</ta>
            <ta e="T1088" id="Seg_11155" s="T1087">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T1089" id="Seg_11156" s="T1088">adv</ta>
            <ta e="T1090" id="Seg_11157" s="T1089">n-n:case</ta>
            <ta e="T1091" id="Seg_11158" s="T1090">n-n:case</ta>
            <ta e="T1092" id="Seg_11159" s="T1091">v-v:tense-v:pn</ta>
            <ta e="T1093" id="Seg_11160" s="T1092">conj</ta>
            <ta e="T1094" id="Seg_11161" s="T1093">n-n:num</ta>
            <ta e="T1095" id="Seg_11162" s="T1094">ptcl</ta>
            <ta e="T1096" id="Seg_11163" s="T1095">v-v:tense-v:pn</ta>
            <ta e="T1098" id="Seg_11164" s="T1097">conj</ta>
            <ta e="T1099" id="Seg_11165" s="T1098">n.[n:case]</ta>
            <ta e="T1100" id="Seg_11166" s="T1099">n-n:case</ta>
            <ta e="T1101" id="Seg_11167" s="T1100">ptcl</ta>
            <ta e="T1103" id="Seg_11168" s="T1102">v-v&gt;v-v:pn</ta>
            <ta e="T1104" id="Seg_11169" s="T1103">conj</ta>
            <ta e="T1105" id="Seg_11170" s="T1104">n-n:num</ta>
            <ta e="T1106" id="Seg_11171" s="T1105">ptcl</ta>
            <ta e="T1107" id="Seg_11172" s="T1106">v-v:tense-v:pn</ta>
            <ta e="T1111" id="Seg_11173" s="T1110">conj</ta>
            <ta e="T1113" id="Seg_11174" s="T1112">ptcl</ta>
            <ta e="T1114" id="Seg_11175" s="T1113">pers</ta>
            <ta e="T1115" id="Seg_11176" s="T1114">num.[n:case]</ta>
            <ta e="T1116" id="Seg_11177" s="T1115">v-v:tense.[v:pn]</ta>
            <ta e="T1117" id="Seg_11178" s="T1116">n.[n:case]</ta>
            <ta e="T1118" id="Seg_11179" s="T1117">v-v:tense-v:pn</ta>
            <ta e="T1119" id="Seg_11180" s="T1118">num.[n:case]</ta>
            <ta e="T1121" id="Seg_11181" s="T1120">que</ta>
            <ta e="T1122" id="Seg_11182" s="T1121">pers</ta>
            <ta e="T1123" id="Seg_11183" s="T1122">v-v:n.fin</ta>
            <ta e="T1124" id="Seg_11184" s="T1123">adj</ta>
            <ta e="T1125" id="Seg_11185" s="T1124">conj</ta>
            <ta e="T1126" id="Seg_11186" s="T1125">adj.[n:case]</ta>
            <ta e="T1128" id="Seg_11187" s="T1127">ptcl</ta>
            <ta e="T1129" id="Seg_11188" s="T1128">v-v:mood.pn</ta>
            <ta e="T1130" id="Seg_11189" s="T1129">adj.[n:case]</ta>
            <ta e="T1134" id="Seg_11190" s="T1133">n-n:case</ta>
            <ta e="T1135" id="Seg_11191" s="T1134">v-v:tense.[v:pn]</ta>
            <ta e="T1136" id="Seg_11192" s="T1135">adv</ta>
            <ta e="T1137" id="Seg_11193" s="T1136">adv</ta>
            <ta e="T1138" id="Seg_11194" s="T1137">n.[n:case]</ta>
            <ta e="T1139" id="Seg_11195" s="T1138">quant</ta>
            <ta e="T1141" id="Seg_11196" s="T1140">adj.[n:case]</ta>
            <ta e="T1142" id="Seg_11197" s="T1141">n.[n:case]</ta>
            <ta e="T1143" id="Seg_11198" s="T1142">v-v:tense.[v:pn]</ta>
            <ta e="T1144" id="Seg_11199" s="T1143">adj.[n:case]</ta>
            <ta e="T1145" id="Seg_11200" s="T1144">n.[n:case]</ta>
            <ta e="T1146" id="Seg_11201" s="T1145">v-v:tense.[v:pn]</ta>
            <ta e="T1148" id="Seg_11202" s="T1147">adj.[n:case]</ta>
            <ta e="T1149" id="Seg_11203" s="T1148">adj-adj&gt;v-v:tense-v:pn</ta>
            <ta e="T1150" id="Seg_11204" s="T1149">ptcl</ta>
            <ta e="T1154" id="Seg_11205" s="T1153">n-n:case.poss</ta>
            <ta e="T1155" id="Seg_11206" s="T1154">ptcl</ta>
            <ta e="T1156" id="Seg_11207" s="T1155">v-v:n.fin.[n:case]</ta>
            <ta e="T1157" id="Seg_11208" s="T1156">conj</ta>
            <ta e="T1158" id="Seg_11209" s="T1157">n-n:case.poss</ta>
            <ta e="T1159" id="Seg_11210" s="T1158">v-v:n.fin-n:num</ta>
            <ta e="T1163" id="Seg_11211" s="T1162">dempro.[n:case]</ta>
            <ta e="T1164" id="Seg_11212" s="T1163">n.[n:case]</ta>
            <ta e="T1165" id="Seg_11213" s="T1164">conj</ta>
            <ta e="T1166" id="Seg_11214" s="T1165">que</ta>
            <ta e="T1167" id="Seg_11215" s="T1166">dempro-n:case</ta>
            <ta e="T1168" id="Seg_11216" s="T1167">n.[n:case]</ta>
            <ta e="T1170" id="Seg_11217" s="T1169">n-n:case.poss</ta>
            <ta e="T1171" id="Seg_11218" s="T1170">v-v:n.fin</ta>
            <ta e="T1172" id="Seg_11219" s="T1171">ptcl</ta>
            <ta e="T1174" id="Seg_11220" s="T1173">v-v&gt;v-v:mood.pn</ta>
            <ta e="T1175" id="Seg_11221" s="T1174">ptcl</ta>
            <ta e="T1176" id="Seg_11222" s="T1175">adv</ta>
            <ta e="T1177" id="Seg_11223" s="T1176">pers</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_11224" s="T1">pers</ta>
            <ta e="T3" id="Seg_11225" s="T2">adv</ta>
            <ta e="T4" id="Seg_11226" s="T3">v</ta>
            <ta e="T5" id="Seg_11227" s="T4">n</ta>
            <ta e="T7" id="Seg_11228" s="T6">pers</ta>
            <ta e="T8" id="Seg_11229" s="T7">v</ta>
            <ta e="T9" id="Seg_11230" s="T8">aux</ta>
            <ta e="T10" id="Seg_11231" s="T9">v</ta>
            <ta e="T11" id="Seg_11232" s="T10">pers</ta>
            <ta e="T14" id="Seg_11233" s="T13">v</ta>
            <ta e="T16" id="Seg_11234" s="T15">n</ta>
            <ta e="T17" id="Seg_11235" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_11236" s="T17">v</ta>
            <ta e="T19" id="Seg_11237" s="T18">conj</ta>
            <ta e="T20" id="Seg_11238" s="T19">pers</ta>
            <ta e="T21" id="Seg_11239" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_11240" s="T21">v</ta>
            <ta e="T24" id="Seg_11241" s="T23">dempro</ta>
            <ta e="T25" id="Seg_11242" s="T24">n</ta>
            <ta e="T26" id="Seg_11243" s="T25">v</ta>
            <ta e="T27" id="Seg_11244" s="T26">v</ta>
            <ta e="T29" id="Seg_11245" s="T28">propr</ta>
            <ta e="T30" id="Seg_11246" s="T29">propr</ta>
            <ta e="T32" id="Seg_11247" s="T31">v</ta>
            <ta e="T34" id="Seg_11248" s="T33">v</ta>
            <ta e="T36" id="Seg_11249" s="T35">v</ta>
            <ta e="T37" id="Seg_11250" s="T36">n</ta>
            <ta e="T38" id="Seg_11251" s="T37">v</ta>
            <ta e="T39" id="Seg_11252" s="T38">v</ta>
            <ta e="T41" id="Seg_11253" s="T40">n</ta>
            <ta e="T42" id="Seg_11254" s="T41">v</ta>
            <ta e="T44" id="Seg_11255" s="T43">pers</ta>
            <ta e="T45" id="Seg_11256" s="T44">v</ta>
            <ta e="T46" id="Seg_11257" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_11258" s="T46">v</ta>
            <ta e="T49" id="Seg_11259" s="T48">conj</ta>
            <ta e="T50" id="Seg_11260" s="T49">adv</ta>
            <ta e="T1178" id="Seg_11261" s="T50">v</ta>
            <ta e="T51" id="Seg_11262" s="T1178">v</ta>
            <ta e="T52" id="Seg_11263" s="T51">n</ta>
            <ta e="T56" id="Seg_11264" s="T55">pers</ta>
            <ta e="T57" id="Seg_11265" s="T56">adv</ta>
            <ta e="T58" id="Seg_11266" s="T57">n</ta>
            <ta e="T59" id="Seg_11267" s="T58">v</ta>
            <ta e="T60" id="Seg_11268" s="T59">dempro</ta>
            <ta e="T61" id="Seg_11269" s="T60">v</ta>
            <ta e="T63" id="Seg_11270" s="T62">dempro</ta>
            <ta e="T64" id="Seg_11271" s="T63">v</ta>
            <ta e="T65" id="Seg_11272" s="T64">que</ta>
            <ta e="T66" id="Seg_11273" s="T65">adv</ta>
            <ta e="T67" id="Seg_11274" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_11275" s="T67">v</ta>
            <ta e="T69" id="Seg_11276" s="T68">pers</ta>
            <ta e="T70" id="Seg_11277" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_11278" s="T70">v</ta>
            <ta e="T72" id="Seg_11279" s="T71">pers</ta>
            <ta e="T73" id="Seg_11280" s="T72">adv</ta>
            <ta e="T74" id="Seg_11281" s="T73">v</ta>
            <ta e="T75" id="Seg_11282" s="T74">conj</ta>
            <ta e="T76" id="Seg_11283" s="T75">v</ta>
            <ta e="T77" id="Seg_11284" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_11285" s="T77">v</ta>
            <ta e="T80" id="Seg_11286" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_11287" s="T80">v</ta>
            <ta e="T83" id="Seg_11288" s="T82">pers</ta>
            <ta e="T84" id="Seg_11289" s="T83">que</ta>
            <ta e="T86" id="Seg_11290" s="T85">que</ta>
            <ta e="T87" id="Seg_11291" s="T86">pers</ta>
            <ta e="T88" id="Seg_11292" s="T87">pers</ta>
            <ta e="T89" id="Seg_11293" s="T88">n</ta>
            <ta e="T90" id="Seg_11294" s="T89">v</ta>
            <ta e="T91" id="Seg_11295" s="T90">ptcl</ta>
            <ta e="T93" id="Seg_11296" s="T92">pers</ta>
            <ta e="T1185" id="Seg_11297" s="T93">ptcl</ta>
            <ta e="T94" id="Seg_11298" s="T1185">n</ta>
            <ta e="T95" id="Seg_11299" s="T94">v</ta>
            <ta e="T97" id="Seg_11300" s="T96">adv</ta>
            <ta e="T98" id="Seg_11301" s="T97">v</ta>
            <ta e="T99" id="Seg_11302" s="T98">pers</ta>
            <ta e="T101" id="Seg_11303" s="T100">v</ta>
            <ta e="T102" id="Seg_11304" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_11305" s="T103">conj</ta>
            <ta e="T105" id="Seg_11306" s="T104">pers</ta>
            <ta e="T106" id="Seg_11307" s="T105">n</ta>
            <ta e="T107" id="Seg_11308" s="T106">v</ta>
            <ta e="T111" id="Seg_11309" s="T110">n</ta>
            <ta e="T113" id="Seg_11310" s="T112">num</ta>
            <ta e="T114" id="Seg_11311" s="T113">n</ta>
            <ta e="T115" id="Seg_11312" s="T114">v</ta>
            <ta e="T117" id="Seg_11313" s="T116">conj</ta>
            <ta e="T118" id="Seg_11314" s="T117">pers</ta>
            <ta e="T119" id="Seg_11315" s="T118">n</ta>
            <ta e="T120" id="Seg_11316" s="T119">v</ta>
            <ta e="T122" id="Seg_11317" s="T121">conj</ta>
            <ta e="T123" id="Seg_11318" s="T122">n</ta>
            <ta e="T124" id="Seg_11319" s="T123">v</ta>
            <ta e="T126" id="Seg_11320" s="T125">conj</ta>
            <ta e="T127" id="Seg_11321" s="T126">dempro</ta>
            <ta e="T128" id="Seg_11322" s="T127">v</ta>
            <ta e="T129" id="Seg_11323" s="T128">conj</ta>
            <ta e="T130" id="Seg_11324" s="T129">n</ta>
            <ta e="T131" id="Seg_11325" s="T130">dempro</ta>
            <ta e="T133" id="Seg_11326" s="T132">conj</ta>
            <ta e="T134" id="Seg_11327" s="T133">pers</ta>
            <ta e="T135" id="Seg_11328" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_11329" s="T135">n</ta>
            <ta e="T138" id="Seg_11330" s="T137">v</ta>
            <ta e="T139" id="Seg_11331" s="T138">v</ta>
            <ta e="T143" id="Seg_11332" s="T142">adv</ta>
            <ta e="T144" id="Seg_11333" s="T143">dempro</ta>
            <ta e="T145" id="Seg_11334" s="T144">v</ta>
            <ta e="T146" id="Seg_11335" s="T145">conj</ta>
            <ta e="T147" id="Seg_11336" s="T146">pers</ta>
            <ta e="T148" id="Seg_11337" s="T147">n</ta>
            <ta e="T1179" id="Seg_11338" s="T148">v</ta>
            <ta e="T149" id="Seg_11339" s="T1179">v</ta>
            <ta e="T153" id="Seg_11340" s="T152">pers</ta>
            <ta e="T1183" id="Seg_11341" s="T153">propr</ta>
            <ta e="T154" id="Seg_11342" s="T1183">n</ta>
            <ta e="T155" id="Seg_11343" s="T154">v</ta>
            <ta e="T157" id="Seg_11344" s="T156">conj</ta>
            <ta e="T158" id="Seg_11345" s="T157">dempro</ta>
            <ta e="T159" id="Seg_11346" s="T158">n</ta>
            <ta e="T160" id="Seg_11347" s="T159">v</ta>
            <ta e="T161" id="Seg_11348" s="T160">dempro</ta>
            <ta e="T162" id="Seg_11349" s="T161">n</ta>
            <ta e="T163" id="Seg_11350" s="T162">adv</ta>
            <ta e="T164" id="Seg_11351" s="T163">n</ta>
            <ta e="T165" id="Seg_11352" s="T164">v</ta>
            <ta e="T167" id="Seg_11353" s="T166">v</ta>
            <ta e="T169" id="Seg_11354" s="T168">conj</ta>
            <ta e="T1180" id="Seg_11355" s="T169">v</ta>
            <ta e="T170" id="Seg_11356" s="T1180">v</ta>
            <ta e="T172" id="Seg_11357" s="T171">pers</ta>
            <ta e="T173" id="Seg_11358" s="T172">v</ta>
            <ta e="T174" id="Seg_11359" s="T173">conj</ta>
            <ta e="T175" id="Seg_11360" s="T174">que</ta>
            <ta e="T176" id="Seg_11361" s="T175">pers</ta>
            <ta e="T178" id="Seg_11362" s="T177">v</ta>
            <ta e="T180" id="Seg_11363" s="T179">dempro</ta>
            <ta e="T181" id="Seg_11364" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_11365" s="T181">v</ta>
            <ta e="T183" id="Seg_11366" s="T182">adv</ta>
            <ta e="T185" id="Seg_11367" s="T184">adv</ta>
            <ta e="T186" id="Seg_11368" s="T185">pers</ta>
            <ta e="T187" id="Seg_11369" s="T186">v</ta>
            <ta e="T188" id="Seg_11370" s="T187">v</ta>
            <ta e="T189" id="Seg_11371" s="T188">dempro</ta>
            <ta e="T190" id="Seg_11372" s="T189">pers</ta>
            <ta e="T1182" id="Seg_11373" s="T190">v</ta>
            <ta e="T191" id="Seg_11374" s="T1182">v</ta>
            <ta e="T192" id="Seg_11375" s="T191">conj</ta>
            <ta e="T193" id="Seg_11376" s="T192">pers</ta>
            <ta e="T194" id="Seg_11377" s="T193">v</ta>
            <ta e="T195" id="Seg_11378" s="T194">conj</ta>
            <ta e="T196" id="Seg_11379" s="T195">v</ta>
            <ta e="T198" id="Seg_11380" s="T197">conj</ta>
            <ta e="T199" id="Seg_11381" s="T198">pers</ta>
            <ta e="T200" id="Seg_11382" s="T199">v</ta>
            <ta e="T201" id="Seg_11383" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_11384" s="T201">v</ta>
            <ta e="T204" id="Seg_11385" s="T203">que</ta>
            <ta e="T205" id="Seg_11386" s="T204">conj</ta>
            <ta e="T207" id="Seg_11387" s="T206">conj</ta>
            <ta e="T208" id="Seg_11388" s="T207">v</ta>
            <ta e="T210" id="Seg_11389" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_11390" s="T210">pers</ta>
            <ta e="T212" id="Seg_11391" s="T211">n</ta>
            <ta e="T213" id="Seg_11392" s="T212">v</ta>
            <ta e="T215" id="Seg_11393" s="T214">conj</ta>
            <ta e="T216" id="Seg_11394" s="T215">pers</ta>
            <ta e="T218" id="Seg_11395" s="T217">v</ta>
            <ta e="T220" id="Seg_11396" s="T219">adv</ta>
            <ta e="T221" id="Seg_11397" s="T220">v</ta>
            <ta e="T222" id="Seg_11398" s="T221">conj</ta>
            <ta e="T223" id="Seg_11399" s="T222">n</ta>
            <ta e="T226" id="Seg_11400" s="T225">n</ta>
            <ta e="T227" id="Seg_11401" s="T226">v</ta>
            <ta e="T241" id="Seg_11402" s="T240">dempro</ta>
            <ta e="T242" id="Seg_11403" s="T241">n</ta>
            <ta e="T243" id="Seg_11404" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_11405" s="T243">pers</ta>
            <ta e="T245" id="Seg_11406" s="T244">v</ta>
            <ta e="T246" id="Seg_11407" s="T245">n</ta>
            <ta e="T247" id="Seg_11408" s="T246">v</ta>
            <ta e="T248" id="Seg_11409" s="T247">pers</ta>
            <ta e="T250" id="Seg_11410" s="T249">v</ta>
            <ta e="T251" id="Seg_11411" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_11412" s="T251">adv</ta>
            <ta e="T254" id="Seg_11413" s="T252">n=ptcl</ta>
            <ta e="T256" id="Seg_11414" s="T254">n=ptcl</ta>
            <ta e="T257" id="Seg_11415" s="T256">v</ta>
            <ta e="T259" id="Seg_11416" s="T258">pers</ta>
            <ta e="T260" id="Seg_11417" s="T259">adv</ta>
            <ta e="T261" id="Seg_11418" s="T260">v</ta>
            <ta e="T263" id="Seg_11419" s="T262">n</ta>
            <ta e="T265" id="Seg_11420" s="T264">adv</ta>
            <ta e="T266" id="Seg_11421" s="T265">n</ta>
            <ta e="T267" id="Seg_11422" s="T266">v</ta>
            <ta e="T268" id="Seg_11423" s="T267">conj</ta>
            <ta e="T270" id="Seg_11424" s="T269">v</ta>
            <ta e="T271" id="Seg_11425" s="T270">v</ta>
            <ta e="T272" id="Seg_11426" s="T271">dempro</ta>
            <ta e="T273" id="Seg_11427" s="T272">conj</ta>
            <ta e="T274" id="Seg_11428" s="T273">adv</ta>
            <ta e="T275" id="Seg_11429" s="T274">n</ta>
            <ta e="T276" id="Seg_11430" s="T275">v</ta>
            <ta e="T277" id="Seg_11431" s="T276">n</ta>
            <ta e="T279" id="Seg_11432" s="T278">v</ta>
            <ta e="T280" id="Seg_11433" s="T279">n</ta>
            <ta e="T281" id="Seg_11434" s="T280">v</ta>
            <ta e="T282" id="Seg_11435" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_11436" s="T282">adv</ta>
            <ta e="T284" id="Seg_11437" s="T283">v</ta>
            <ta e="T286" id="Seg_11438" s="T285">n</ta>
            <ta e="T287" id="Seg_11439" s="T286">conj</ta>
            <ta e="T288" id="Seg_11440" s="T287">n</ta>
            <ta e="T289" id="Seg_11441" s="T288">v</ta>
            <ta e="T290" id="Seg_11442" s="T289">conj</ta>
            <ta e="T291" id="Seg_11443" s="T290">adj</ta>
            <ta e="T292" id="Seg_11444" s="T291">v</ta>
            <ta e="T294" id="Seg_11445" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_11446" s="T294">adv</ta>
            <ta e="T296" id="Seg_11447" s="T295">n</ta>
            <ta e="T297" id="Seg_11448" s="T296">v</ta>
            <ta e="T298" id="Seg_11449" s="T297">adv</ta>
            <ta e="T300" id="Seg_11450" s="T299">adj</ta>
            <ta e="T301" id="Seg_11451" s="T300">quant</ta>
            <ta e="T302" id="Seg_11452" s="T301">v</ta>
            <ta e="T303" id="Seg_11453" s="T302">n</ta>
            <ta e="T305" id="Seg_11454" s="T304">pers</ta>
            <ta e="T306" id="Seg_11455" s="T305">v</ta>
            <ta e="T307" id="Seg_11456" s="T306">adv</ta>
            <ta e="T308" id="Seg_11457" s="T307">v</ta>
            <ta e="T309" id="Seg_11458" s="T308">n</ta>
            <ta e="T311" id="Seg_11459" s="T310">dempro</ta>
            <ta e="T313" id="Seg_11460" s="T312">v</ta>
            <ta e="T317" id="Seg_11461" s="T316">num</ta>
            <ta e="T319" id="Seg_11462" s="T318">n</ta>
            <ta e="T320" id="Seg_11463" s="T319">v</ta>
            <ta e="T321" id="Seg_11464" s="T320">n</ta>
            <ta e="T322" id="Seg_11465" s="T321">n</ta>
            <ta e="T324" id="Seg_11466" s="T323">num</ta>
            <ta e="T325" id="Seg_11467" s="T324">n</ta>
            <ta e="T326" id="Seg_11468" s="T325">v</ta>
            <ta e="T328" id="Seg_11469" s="T327">adv</ta>
            <ta e="T329" id="Seg_11470" s="T328">n</ta>
            <ta e="T330" id="Seg_11471" s="T329">v</ta>
            <ta e="T331" id="Seg_11472" s="T330">n</ta>
            <ta e="T333" id="Seg_11473" s="T332">n</ta>
            <ta e="T334" id="Seg_11474" s="T333">v</ta>
            <ta e="T336" id="Seg_11475" s="T335">v</ta>
            <ta e="T337" id="Seg_11476" s="T336">n</ta>
            <ta e="T338" id="Seg_11477" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_11478" s="T338">num</ta>
            <ta e="T340" id="Seg_11479" s="T339">n</ta>
            <ta e="T341" id="Seg_11480" s="T340">v</ta>
            <ta e="T343" id="Seg_11481" s="T342">v</ta>
            <ta e="T344" id="Seg_11482" s="T343">n</ta>
            <ta e="T345" id="Seg_11483" s="T344">adv</ta>
            <ta e="T346" id="Seg_11484" s="T345">dempro</ta>
            <ta e="T347" id="Seg_11485" s="T346">n</ta>
            <ta e="T348" id="Seg_11486" s="T347">n</ta>
            <ta e="T349" id="Seg_11487" s="T348">v</ta>
            <ta e="T351" id="Seg_11488" s="T350">n</ta>
            <ta e="T352" id="Seg_11489" s="T351">v</ta>
            <ta e="T354" id="Seg_11490" s="T353">adv</ta>
            <ta e="T355" id="Seg_11491" s="T354">n</ta>
            <ta e="T356" id="Seg_11492" s="T355">n</ta>
            <ta e="T357" id="Seg_11493" s="T356">v</ta>
            <ta e="T358" id="Seg_11494" s="T357">n</ta>
            <ta e="T360" id="Seg_11495" s="T359">quant</ta>
            <ta e="T361" id="Seg_11496" s="T360">quant</ta>
            <ta e="T362" id="Seg_11497" s="T361">n</ta>
            <ta e="T364" id="Seg_11498" s="T363">adv</ta>
            <ta e="T366" id="Seg_11499" s="T365">n</ta>
            <ta e="T367" id="Seg_11500" s="T366">n</ta>
            <ta e="T368" id="Seg_11501" s="T367">v</ta>
            <ta e="T369" id="Seg_11502" s="T368">n</ta>
            <ta e="T370" id="Seg_11503" s="T369">conj</ta>
            <ta e="T371" id="Seg_11504" s="T370">n</ta>
            <ta e="T372" id="Seg_11505" s="T371">v</ta>
            <ta e="T376" id="Seg_11506" s="T375">adv</ta>
            <ta e="T377" id="Seg_11507" s="T376">num</ta>
            <ta e="T378" id="Seg_11508" s="T377">n</ta>
            <ta e="T379" id="Seg_11509" s="T378">adv</ta>
            <ta e="T381" id="Seg_11510" s="T380">n</ta>
            <ta e="T382" id="Seg_11511" s="T381">v</ta>
            <ta e="T384" id="Seg_11512" s="T383">n</ta>
            <ta e="T385" id="Seg_11513" s="T384">v</ta>
            <ta e="T386" id="Seg_11514" s="T385">v</ta>
            <ta e="T387" id="Seg_11515" s="T386">n</ta>
            <ta e="T389" id="Seg_11516" s="T388">pers</ta>
            <ta e="T390" id="Seg_11517" s="T389">pers</ta>
            <ta e="T391" id="Seg_11518" s="T390">v</ta>
            <ta e="T393" id="Seg_11519" s="T392">conj</ta>
            <ta e="T394" id="Seg_11520" s="T393">refl</ta>
            <ta e="T395" id="Seg_11521" s="T394">refl</ta>
            <ta e="T396" id="Seg_11522" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_11523" s="T396">v</ta>
            <ta e="T398" id="Seg_11524" s="T397">conj</ta>
            <ta e="T399" id="Seg_11525" s="T398">adv</ta>
            <ta e="T401" id="Seg_11526" s="T400">n</ta>
            <ta e="T407" id="Seg_11527" s="T406">pers</ta>
            <ta e="T408" id="Seg_11528" s="T407">adv</ta>
            <ta e="T409" id="Seg_11529" s="T408">v</ta>
            <ta e="T410" id="Seg_11530" s="T409">ptcl</ta>
            <ta e="T412" id="Seg_11531" s="T411">v</ta>
            <ta e="T413" id="Seg_11532" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_11533" s="T413">n</ta>
            <ta e="T415" id="Seg_11534" s="T414">n</ta>
            <ta e="T416" id="Seg_11535" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_11536" s="T416">adj</ta>
            <ta e="T421" id="Seg_11537" s="T420">pers</ta>
            <ta e="T422" id="Seg_11538" s="T421">v</ta>
            <ta e="T428" id="Seg_11539" s="T427">adv</ta>
            <ta e="T429" id="Seg_11540" s="T428">v</ta>
            <ta e="T430" id="Seg_11541" s="T429">n</ta>
            <ta e="T432" id="Seg_11542" s="T431">v</ta>
            <ta e="T433" id="Seg_11543" s="T432">conj</ta>
            <ta e="T434" id="Seg_11544" s="T433">v</ta>
            <ta e="T436" id="Seg_11545" s="T435">v</ta>
            <ta e="T437" id="Seg_11546" s="T436">conj</ta>
            <ta e="T438" id="Seg_11547" s="T437">dempro</ta>
            <ta e="T439" id="Seg_11548" s="T438">v</ta>
            <ta e="T440" id="Seg_11549" s="T439">que</ta>
            <ta e="T444" id="Seg_11550" s="T443">n</ta>
            <ta e="T445" id="Seg_11551" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_11552" s="T445">n</ta>
            <ta e="T447" id="Seg_11553" s="T446">v</ta>
            <ta e="T449" id="Seg_11554" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_11555" s="T449">v</ta>
            <ta e="T451" id="Seg_11556" s="T450">adv</ta>
            <ta e="T452" id="Seg_11557" s="T451">v</ta>
            <ta e="T456" id="Seg_11558" s="T455">adv</ta>
            <ta e="T457" id="Seg_11559" s="T456">quant</ta>
            <ta e="T458" id="Seg_11560" s="T457">n</ta>
            <ta e="T460" id="Seg_11561" s="T459">conj</ta>
            <ta e="T461" id="Seg_11562" s="T460">conj</ta>
            <ta e="T462" id="Seg_11563" s="T461">adj</ta>
            <ta e="T463" id="Seg_11564" s="T462">adj</ta>
            <ta e="T464" id="Seg_11565" s="T463">adv</ta>
            <ta e="T468" id="Seg_11566" s="T467">quant</ta>
            <ta e="T470" id="Seg_11567" s="T469">v</ta>
            <ta e="T471" id="Seg_11568" s="T470">v</ta>
            <ta e="T472" id="Seg_11569" s="T471">conj</ta>
            <ta e="T473" id="Seg_11570" s="T472">adv</ta>
            <ta e="T474" id="Seg_11571" s="T473">v</ta>
            <ta e="T478" id="Seg_11572" s="T477">n</ta>
            <ta e="T479" id="Seg_11573" s="T478">adv</ta>
            <ta e="T480" id="Seg_11574" s="T479">adj</ta>
            <ta e="T482" id="Seg_11575" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_11576" s="T482">que</ta>
            <ta e="T484" id="Seg_11577" s="T483">pers</ta>
            <ta e="T485" id="Seg_11578" s="T484">v</ta>
            <ta e="T486" id="Seg_11579" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_11580" s="T486">que</ta>
            <ta e="T491" id="Seg_11581" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_11582" s="T491">pers</ta>
            <ta e="T494" id="Seg_11583" s="T493">v</ta>
            <ta e="T501" id="Seg_11584" s="T500">v</ta>
            <ta e="T502" id="Seg_11585" s="T501">n</ta>
            <ta e="T503" id="Seg_11586" s="T502">v</ta>
            <ta e="T504" id="Seg_11587" s="T503">v</ta>
            <ta e="T508" id="Seg_11588" s="T507">adv</ta>
            <ta e="T509" id="Seg_11589" s="T508">n</ta>
            <ta e="T510" id="Seg_11590" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_11591" s="T510">n</ta>
            <ta e="T512" id="Seg_11592" s="T511">v</ta>
            <ta e="T513" id="Seg_11593" s="T512">n</ta>
            <ta e="T514" id="Seg_11594" s="T513">v</ta>
            <ta e="T516" id="Seg_11595" s="T515">adv</ta>
            <ta e="T517" id="Seg_11596" s="T516">v</ta>
            <ta e="T519" id="Seg_11597" s="T518">v</ta>
            <ta e="T520" id="Seg_11598" s="T519">ptcl</ta>
            <ta e="T522" id="Seg_11599" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_11600" s="T522">adj</ta>
            <ta e="T524" id="Seg_11601" s="T523">n</ta>
            <ta e="T525" id="Seg_11602" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_11603" s="T525">v</ta>
            <ta e="T530" id="Seg_11604" s="T529">n</ta>
            <ta e="T531" id="Seg_11605" s="T530">n</ta>
            <ta e="T532" id="Seg_11606" s="T531">v</ta>
            <ta e="T533" id="Seg_11607" s="T532">v</ta>
            <ta e="T535" id="Seg_11608" s="T534">adv</ta>
            <ta e="T536" id="Seg_11609" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_11610" s="T536">v</ta>
            <ta e="T538" id="Seg_11611" s="T537">n</ta>
            <ta e="T539" id="Seg_11612" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_11613" s="T539">v</ta>
            <ta e="T542" id="Seg_11614" s="T541">conj</ta>
            <ta e="T543" id="Seg_11615" s="T542">n</ta>
            <ta e="T544" id="Seg_11616" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_11617" s="T544">v</ta>
            <ta e="T548" id="Seg_11618" s="T547">adv</ta>
            <ta e="T549" id="Seg_11619" s="T548">n</ta>
            <ta e="T550" id="Seg_11620" s="T549">n</ta>
            <ta e="T551" id="Seg_11621" s="T550">v</ta>
            <ta e="T553" id="Seg_11622" s="T552">v</ta>
            <ta e="T554" id="Seg_11623" s="T553">adv</ta>
            <ta e="T555" id="Seg_11624" s="T554">n</ta>
            <ta e="T556" id="Seg_11625" s="T555">v</ta>
            <ta e="T560" id="Seg_11626" s="T559">pers</ta>
            <ta e="T561" id="Seg_11627" s="T560">v</ta>
            <ta e="T562" id="Seg_11628" s="T561">propr</ta>
            <ta e="T1186" id="Seg_11629" s="T562">n</ta>
            <ta e="T564" id="Seg_11630" s="T563">v</ta>
            <ta e="T567" id="Seg_11631" s="T566">num</ta>
            <ta e="T568" id="Seg_11632" s="T567">n</ta>
            <ta e="T570" id="Seg_11633" s="T569">adv</ta>
            <ta e="T571" id="Seg_11634" s="T570">n</ta>
            <ta e="T572" id="Seg_11635" s="T571">pers</ta>
            <ta e="T573" id="Seg_11636" s="T572">v</ta>
            <ta e="T575" id="Seg_11637" s="T574">dempro</ta>
            <ta e="T576" id="Seg_11638" s="T575">pers</ta>
            <ta e="T577" id="Seg_11639" s="T576">v</ta>
            <ta e="T578" id="Seg_11640" s="T577">v</ta>
            <ta e="T579" id="Seg_11641" s="T578">v</ta>
            <ta e="T580" id="Seg_11642" s="T579">n</ta>
            <ta e="T582" id="Seg_11643" s="T581">pers</ta>
            <ta e="T584" id="Seg_11644" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_11645" s="T584">v</ta>
            <ta e="T586" id="Seg_11646" s="T585">v</ta>
            <ta e="T587" id="Seg_11647" s="T586">adv</ta>
            <ta e="T588" id="Seg_11648" s="T587">v</ta>
            <ta e="T589" id="Seg_11649" s="T588">n</ta>
            <ta e="T591" id="Seg_11650" s="T590">conj</ta>
            <ta e="T592" id="Seg_11651" s="T591">adv</ta>
            <ta e="T593" id="Seg_11652" s="T592">v</ta>
            <ta e="T595" id="Seg_11653" s="T594">adv</ta>
            <ta e="T596" id="Seg_11654" s="T595">n</ta>
            <ta e="T598" id="Seg_11655" s="T597">conj</ta>
            <ta e="T599" id="Seg_11656" s="T598">adv</ta>
            <ta e="T600" id="Seg_11657" s="T599">n</ta>
            <ta e="T602" id="Seg_11658" s="T601">adv</ta>
            <ta e="T603" id="Seg_11659" s="T602">n</ta>
            <ta e="T604" id="Seg_11660" s="T603">n</ta>
            <ta e="T606" id="Seg_11661" s="T605">n</ta>
            <ta e="T607" id="Seg_11662" s="T606">n</ta>
            <ta e="T608" id="Seg_11663" s="T607">adv</ta>
            <ta e="T609" id="Seg_11664" s="T608">adv</ta>
            <ta e="T610" id="Seg_11665" s="T609">v</ta>
            <ta e="T614" id="Seg_11666" s="T613">adv</ta>
            <ta e="T615" id="Seg_11667" s="T614">v</ta>
            <ta e="T616" id="Seg_11668" s="T615">n</ta>
            <ta e="T618" id="Seg_11669" s="T617">n</ta>
            <ta e="T619" id="Seg_11670" s="T618">v</ta>
            <ta e="T621" id="Seg_11671" s="T620">adv</ta>
            <ta e="T622" id="Seg_11672" s="T621">v</ta>
            <ta e="T623" id="Seg_11673" s="T622">n</ta>
            <ta e="T624" id="Seg_11674" s="T623">n</ta>
            <ta e="T626" id="Seg_11675" s="T625">adv</ta>
            <ta e="T628" id="Seg_11676" s="T627">v</ta>
            <ta e="T629" id="Seg_11677" s="T628">conj</ta>
            <ta e="T630" id="Seg_11678" s="T629">v</ta>
            <ta e="T632" id="Seg_11679" s="T631">conj</ta>
            <ta e="T633" id="Seg_11680" s="T632">adv</ta>
            <ta e="T634" id="Seg_11681" s="T633">n</ta>
            <ta e="T635" id="Seg_11682" s="T634">v</ta>
            <ta e="T639" id="Seg_11683" s="T638">n</ta>
            <ta e="T640" id="Seg_11684" s="T639">pers</ta>
            <ta e="T641" id="Seg_11685" s="T640">n</ta>
            <ta e="T642" id="Seg_11686" s="T641">quant</ta>
            <ta e="T643" id="Seg_11687" s="T642">v</ta>
            <ta e="T644" id="Seg_11688" s="T643">pers</ta>
            <ta e="T1184" id="Seg_11689" s="T645">propr</ta>
            <ta e="T646" id="Seg_11690" s="T1184">n</ta>
            <ta e="T647" id="Seg_11691" s="T646">num</ta>
            <ta e="T648" id="Seg_11692" s="T647">n</ta>
            <ta e="T649" id="Seg_11693" s="T648">v</ta>
            <ta e="T653" id="Seg_11694" s="T652">n</ta>
            <ta e="T654" id="Seg_11695" s="T653">v</ta>
            <ta e="T655" id="Seg_11696" s="T654">n</ta>
            <ta e="T656" id="Seg_11697" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_11698" s="T656">dempro</ta>
            <ta e="T658" id="Seg_11699" s="T657">n</ta>
            <ta e="T659" id="Seg_11700" s="T658">v</ta>
            <ta e="T660" id="Seg_11701" s="T659">conj</ta>
            <ta e="T666" id="Seg_11702" s="T665">v</ta>
            <ta e="T667" id="Seg_11703" s="T666">v</ta>
            <ta e="T668" id="Seg_11704" s="T667">que</ta>
            <ta e="T669" id="Seg_11705" s="T668">v</ta>
            <ta e="T673" id="Seg_11706" s="T672">n</ta>
            <ta e="T674" id="Seg_11707" s="T673">v</ta>
            <ta e="T675" id="Seg_11708" s="T674">n</ta>
            <ta e="T676" id="Seg_11709" s="T675">dempro</ta>
            <ta e="T677" id="Seg_11710" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_11711" s="T677">v</ta>
            <ta e="T680" id="Seg_11712" s="T679">adv</ta>
            <ta e="T681" id="Seg_11713" s="T680">v</ta>
            <ta e="T682" id="Seg_11714" s="T681">conj</ta>
            <ta e="T684" id="Seg_11715" s="T683">v</ta>
            <ta e="T686" id="Seg_11716" s="T685">pers</ta>
            <ta e="T687" id="Seg_11717" s="T686">v</ta>
            <ta e="T688" id="Seg_11718" s="T687">v</ta>
            <ta e="T690" id="Seg_11719" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_11720" s="T690">v</ta>
            <ta e="T696" id="Seg_11721" s="T695">n</ta>
            <ta e="T698" id="Seg_11722" s="T697">n</ta>
            <ta e="T699" id="Seg_11723" s="T698">v</ta>
            <ta e="T700" id="Seg_11724" s="T699">v</ta>
            <ta e="T701" id="Seg_11725" s="T700">adv</ta>
            <ta e="T702" id="Seg_11726" s="T701">n</ta>
            <ta e="T703" id="Seg_11727" s="T702">dempro</ta>
            <ta e="T705" id="Seg_11728" s="T704">n</ta>
            <ta e="T708" id="Seg_11729" s="T707">v</ta>
            <ta e="T709" id="Seg_11730" s="T708">n</ta>
            <ta e="T710" id="Seg_11731" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_11732" s="T710">v</ta>
            <ta e="T713" id="Seg_11733" s="T712">dempro</ta>
            <ta e="T714" id="Seg_11734" s="T713">v</ta>
            <ta e="T715" id="Seg_11735" s="T714">n</ta>
            <ta e="T717" id="Seg_11736" s="T716">v</ta>
            <ta e="T718" id="Seg_11737" s="T717">dempro</ta>
            <ta e="T719" id="Seg_11738" s="T718">n</ta>
            <ta e="T723" id="Seg_11739" s="T722">pers</ta>
            <ta e="T724" id="Seg_11740" s="T723">quant</ta>
            <ta e="T725" id="Seg_11741" s="T724">n</ta>
            <ta e="T726" id="Seg_11742" s="T725">v</ta>
            <ta e="T727" id="Seg_11743" s="T726">adv</ta>
            <ta e="T728" id="Seg_11744" s="T727">dempro</ta>
            <ta e="T729" id="Seg_11745" s="T728">n</ta>
            <ta e="T730" id="Seg_11746" s="T729">v</ta>
            <ta e="T732" id="Seg_11747" s="T731">n</ta>
            <ta e="T733" id="Seg_11748" s="T732">v</ta>
            <ta e="T734" id="Seg_11749" s="T733">n</ta>
            <ta e="T736" id="Seg_11750" s="T735">v</ta>
            <ta e="T740" id="Seg_11751" s="T739">pers</ta>
            <ta e="T741" id="Seg_11752" s="T740">quant</ta>
            <ta e="T743" id="Seg_11753" s="T742">n</ta>
            <ta e="T744" id="Seg_11754" s="T743">v</ta>
            <ta e="T745" id="Seg_11755" s="T744">conj</ta>
            <ta e="T746" id="Seg_11756" s="T745">n</ta>
            <ta e="T747" id="Seg_11757" s="T746">v</ta>
            <ta e="T751" id="Seg_11758" s="T750">adv</ta>
            <ta e="T752" id="Seg_11759" s="T751">dempro</ta>
            <ta e="T753" id="Seg_11760" s="T752">v</ta>
            <ta e="T755" id="Seg_11761" s="T754">n</ta>
            <ta e="T756" id="Seg_11762" s="T755">n</ta>
            <ta e="T757" id="Seg_11763" s="T756">v</ta>
            <ta e="T758" id="Seg_11764" s="T757">ptcl</ta>
            <ta e="T759" id="Seg_11765" s="T758">n</ta>
            <ta e="T761" id="Seg_11766" s="T760">v</ta>
            <ta e="T763" id="Seg_11767" s="T762">adv</ta>
            <ta e="T764" id="Seg_11768" s="T763">n</ta>
            <ta e="T765" id="Seg_11769" s="T764">v</ta>
            <ta e="T766" id="Seg_11770" s="T765">pers</ta>
            <ta e="T767" id="Seg_11771" s="T766">n</ta>
            <ta e="T768" id="Seg_11772" s="T767">quant</ta>
            <ta e="T769" id="Seg_11773" s="T768">n</ta>
            <ta e="T771" id="Seg_11774" s="T770">adv</ta>
            <ta e="T772" id="Seg_11775" s="T771">dempro</ta>
            <ta e="T774" id="Seg_11776" s="T773">v</ta>
            <ta e="T775" id="Seg_11777" s="T774">n</ta>
            <ta e="T776" id="Seg_11778" s="T775">n</ta>
            <ta e="T777" id="Seg_11779" s="T776">n</ta>
            <ta e="T778" id="Seg_11780" s="T777">v</ta>
            <ta e="T779" id="Seg_11781" s="T778">n</ta>
            <ta e="T780" id="Seg_11782" s="T779">n</ta>
            <ta e="T781" id="Seg_11783" s="T780">v</ta>
            <ta e="T783" id="Seg_11784" s="T782">n</ta>
            <ta e="T784" id="Seg_11785" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_11786" s="T784">v</ta>
            <ta e="T787" id="Seg_11787" s="T786">adj</ta>
            <ta e="T788" id="Seg_11788" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_11789" s="T788">pers</ta>
            <ta e="T793" id="Seg_11790" s="T792">dempro</ta>
            <ta e="T794" id="Seg_11791" s="T793">v</ta>
            <ta e="T795" id="Seg_11792" s="T794">n</ta>
            <ta e="T796" id="Seg_11793" s="T795">n</ta>
            <ta e="T797" id="Seg_11794" s="T796">n</ta>
            <ta e="T798" id="Seg_11795" s="T797">v</ta>
            <ta e="T800" id="Seg_11796" s="T799">n</ta>
            <ta e="T801" id="Seg_11797" s="T800">conj</ta>
            <ta e="T802" id="Seg_11798" s="T801">dempro</ta>
            <ta e="T803" id="Seg_11799" s="T802">v</ta>
            <ta e="T804" id="Seg_11800" s="T803">pers</ta>
            <ta e="T805" id="Seg_11801" s="T804">quant</ta>
            <ta e="T806" id="Seg_11802" s="T805">v</ta>
            <ta e="T807" id="Seg_11803" s="T806">conj</ta>
            <ta e="T808" id="Seg_11804" s="T807">adj</ta>
            <ta e="T809" id="Seg_11805" s="T808">pers</ta>
            <ta e="T810" id="Seg_11806" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_11807" s="T810">adv</ta>
            <ta e="T815" id="Seg_11808" s="T814">adv</ta>
            <ta e="T816" id="Seg_11809" s="T815">v</ta>
            <ta e="T817" id="Seg_11810" s="T816">n</ta>
            <ta e="T818" id="Seg_11811" s="T817">n</ta>
            <ta e="T819" id="Seg_11812" s="T818">n</ta>
            <ta e="T820" id="Seg_11813" s="T819">v</ta>
            <ta e="T821" id="Seg_11814" s="T820">n</ta>
            <ta e="T822" id="Seg_11815" s="T821">n</ta>
            <ta e="T823" id="Seg_11816" s="T822">v</ta>
            <ta e="T825" id="Seg_11817" s="T824">ptcl</ta>
            <ta e="T827" id="Seg_11818" s="T826">pers</ta>
            <ta e="T828" id="Seg_11819" s="T827">aux</ta>
            <ta e="T829" id="Seg_11820" s="T828">v</ta>
            <ta e="T830" id="Seg_11821" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_11822" s="T830">v</ta>
            <ta e="T833" id="Seg_11823" s="T832">pers</ta>
            <ta e="T834" id="Seg_11824" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_11825" s="T834">n</ta>
            <ta e="T836" id="Seg_11826" s="T835">n</ta>
            <ta e="T837" id="Seg_11827" s="T836">v</ta>
            <ta e="T841" id="Seg_11828" s="T840">adv</ta>
            <ta e="T842" id="Seg_11829" s="T841">v</ta>
            <ta e="T844" id="Seg_11830" s="T843">n</ta>
            <ta e="T845" id="Seg_11831" s="T844">n</ta>
            <ta e="T846" id="Seg_11832" s="T845">n</ta>
            <ta e="T847" id="Seg_11833" s="T846">v</ta>
            <ta e="T848" id="Seg_11834" s="T847">n</ta>
            <ta e="T849" id="Seg_11835" s="T848">dempro</ta>
            <ta e="T850" id="Seg_11836" s="T849">pers</ta>
            <ta e="T852" id="Seg_11837" s="T851">refl</ta>
            <ta e="T853" id="Seg_11838" s="T852">v</ta>
            <ta e="T854" id="Seg_11839" s="T853">n</ta>
            <ta e="T858" id="Seg_11840" s="T857">pers</ta>
            <ta e="T859" id="Seg_11841" s="T858">n</ta>
            <ta e="T860" id="Seg_11842" s="T859">adj</ta>
            <ta e="T861" id="Seg_11843" s="T860">v</ta>
            <ta e="T863" id="Seg_11844" s="T862">adv</ta>
            <ta e="T864" id="Seg_11845" s="T863">n</ta>
            <ta e="T865" id="Seg_11846" s="T864">que</ta>
            <ta e="T866" id="Seg_11847" s="T865">ptcl</ta>
            <ta e="T867" id="Seg_11848" s="T866">v</ta>
            <ta e="T869" id="Seg_11849" s="T868">v</ta>
            <ta e="T870" id="Seg_11850" s="T869">v</ta>
            <ta e="T871" id="Seg_11851" s="T870">n</ta>
            <ta e="T875" id="Seg_11852" s="T874">n</ta>
            <ta e="T876" id="Seg_11853" s="T875">que</ta>
            <ta e="T877" id="Seg_11854" s="T876">v</ta>
            <ta e="T878" id="Seg_11855" s="T877">que</ta>
            <ta e="T879" id="Seg_11856" s="T878">ptcl</ta>
            <ta e="T880" id="Seg_11857" s="T879">v</ta>
            <ta e="T882" id="Seg_11858" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_11859" s="T882">v</ta>
            <ta e="T884" id="Seg_11860" s="T883">adv</ta>
            <ta e="T885" id="Seg_11861" s="T884">v</ta>
            <ta e="T886" id="Seg_11862" s="T885">n</ta>
            <ta e="T887" id="Seg_11863" s="T886">v</ta>
            <ta e="T888" id="Seg_11864" s="T887">conj</ta>
            <ta e="T889" id="Seg_11865" s="T888">adv</ta>
            <ta e="T890" id="Seg_11866" s="T889">v</ta>
            <ta e="T894" id="Seg_11867" s="T893">dempro</ta>
            <ta e="T895" id="Seg_11868" s="T894">n</ta>
            <ta e="T896" id="Seg_11869" s="T895">adv</ta>
            <ta e="T897" id="Seg_11870" s="T896">adj</ta>
            <ta e="T898" id="Seg_11871" s="T897">v</ta>
            <ta e="T899" id="Seg_11872" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_11873" s="T899">adj</ta>
            <ta e="T902" id="Seg_11874" s="T901">adj</ta>
            <ta e="T903" id="Seg_11875" s="T902">v</ta>
            <ta e="T907" id="Seg_11876" s="T906">dempro</ta>
            <ta e="T908" id="Seg_11877" s="T907">n</ta>
            <ta e="T909" id="Seg_11878" s="T908">adv</ta>
            <ta e="T910" id="Seg_11879" s="T909">quant</ta>
            <ta e="T912" id="Seg_11880" s="T911">v</ta>
            <ta e="T913" id="Seg_11881" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_11882" s="T913">que</ta>
            <ta e="T915" id="Seg_11883" s="T914">v</ta>
            <ta e="T920" id="Seg_11884" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_11885" s="T920">que</ta>
            <ta e="T922" id="Seg_11886" s="T921">v</ta>
            <ta e="T923" id="Seg_11887" s="T922">v</ta>
            <ta e="T927" id="Seg_11888" s="T926">adv</ta>
            <ta e="T928" id="Seg_11889" s="T927">n</ta>
            <ta e="T930" id="Seg_11890" s="T929">n</ta>
            <ta e="T931" id="Seg_11891" s="T930">adv</ta>
            <ta e="T932" id="Seg_11892" s="T931">v</ta>
            <ta e="T933" id="Seg_11893" s="T932">adv</ta>
            <ta e="T934" id="Seg_11894" s="T933">num</ta>
            <ta e="T935" id="Seg_11895" s="T934">n</ta>
            <ta e="T939" id="Seg_11896" s="T938">conj</ta>
            <ta e="T940" id="Seg_11897" s="T939">n</ta>
            <ta e="T941" id="Seg_11898" s="T940">v</ta>
            <ta e="T945" id="Seg_11899" s="T944">v</ta>
            <ta e="T946" id="Seg_11900" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_11901" s="T946">adv</ta>
            <ta e="T948" id="Seg_11902" s="T947">v</ta>
            <ta e="T949" id="Seg_11903" s="T948">v</ta>
            <ta e="T953" id="Seg_11904" s="T952">pers</ta>
            <ta e="T955" id="Seg_11905" s="T954">v</ta>
            <ta e="T956" id="Seg_11906" s="T955">n</ta>
            <ta e="T957" id="Seg_11907" s="T956">v</ta>
            <ta e="T958" id="Seg_11908" s="T957">conj</ta>
            <ta e="T959" id="Seg_11909" s="T958">adv</ta>
            <ta e="T960" id="Seg_11910" s="T959">n</ta>
            <ta e="T961" id="Seg_11911" s="T960">v</ta>
            <ta e="T963" id="Seg_11912" s="T962">num</ta>
            <ta e="T965" id="Seg_11913" s="T964">num</ta>
            <ta e="T966" id="Seg_11914" s="T965">n</ta>
            <ta e="T967" id="Seg_11915" s="T966">v</ta>
            <ta e="T968" id="Seg_11916" s="T967">conj</ta>
            <ta e="T969" id="Seg_11917" s="T968">ptcl</ta>
            <ta e="T970" id="Seg_11918" s="T969">adv</ta>
            <ta e="T974" id="Seg_11919" s="T973">n</ta>
            <ta e="T975" id="Seg_11920" s="T974">v</ta>
            <ta e="T976" id="Seg_11921" s="T975">n</ta>
            <ta e="T977" id="Seg_11922" s="T976">v</ta>
            <ta e="T979" id="Seg_11923" s="T978">n</ta>
            <ta e="T980" id="Seg_11924" s="T979">v</ta>
            <ta e="T981" id="Seg_11925" s="T980">n</ta>
            <ta e="T982" id="Seg_11926" s="T981">v</ta>
            <ta e="T984" id="Seg_11927" s="T983">n</ta>
            <ta e="T985" id="Seg_11928" s="T984">v</ta>
            <ta e="T986" id="Seg_11929" s="T985">n</ta>
            <ta e="T987" id="Seg_11930" s="T986">v</ta>
            <ta e="T989" id="Seg_11931" s="T988">n</ta>
            <ta e="T990" id="Seg_11932" s="T989">v</ta>
            <ta e="T991" id="Seg_11933" s="T990">n</ta>
            <ta e="T992" id="Seg_11934" s="T991">v</ta>
            <ta e="T994" id="Seg_11935" s="T993">propr</ta>
            <ta e="T995" id="Seg_11936" s="T994">v</ta>
            <ta e="T996" id="Seg_11937" s="T995">propr</ta>
            <ta e="T998" id="Seg_11938" s="T997">v</ta>
            <ta e="T1000" id="Seg_11939" s="T999">n</ta>
            <ta e="T1001" id="Seg_11940" s="T1000">v</ta>
            <ta e="T1002" id="Seg_11941" s="T1001">n</ta>
            <ta e="T1003" id="Seg_11942" s="T1002">v</ta>
            <ta e="T1007" id="Seg_11943" s="T1006">pers</ta>
            <ta e="T1008" id="Seg_11944" s="T1007">pers</ta>
            <ta e="T1009" id="Seg_11945" s="T1008">n</ta>
            <ta e="T1010" id="Seg_11946" s="T1009">adv</ta>
            <ta e="T1011" id="Seg_11947" s="T1010">n</ta>
            <ta e="T1012" id="Seg_11948" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_11949" s="T1012">que</ta>
            <ta e="T1014" id="Seg_11950" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_11951" s="T1014">ptcl</ta>
            <ta e="T1016" id="Seg_11952" s="T1015">v</ta>
            <ta e="T1018" id="Seg_11953" s="T1017">conj</ta>
            <ta e="T1019" id="Seg_11954" s="T1018">n</ta>
            <ta e="T1020" id="Seg_11955" s="T1019">v</ta>
            <ta e="T1022" id="Seg_11956" s="T1021">v</ta>
            <ta e="T1023" id="Seg_11957" s="T1022">n</ta>
            <ta e="T1024" id="Seg_11958" s="T1023">dempro</ta>
            <ta e="T1025" id="Seg_11959" s="T1024">adv</ta>
            <ta e="T1026" id="Seg_11960" s="T1025">num</ta>
            <ta e="T1028" id="Seg_11961" s="T1027">n</ta>
            <ta e="T1029" id="Seg_11962" s="T1028">num</ta>
            <ta e="T1030" id="Seg_11963" s="T1029">adj</ta>
            <ta e="T1031" id="Seg_11964" s="T1030">num</ta>
            <ta e="T1032" id="Seg_11965" s="T1031">adj</ta>
            <ta e="T1034" id="Seg_11966" s="T1033">dempro</ta>
            <ta e="T1035" id="Seg_11967" s="T1034">v</ta>
            <ta e="T1036" id="Seg_11968" s="T1035">pers</ta>
            <ta e="T1038" id="Seg_11969" s="T1037">conj</ta>
            <ta e="T1039" id="Seg_11970" s="T1038">dempro</ta>
            <ta e="T1040" id="Seg_11971" s="T1039">n</ta>
            <ta e="T1041" id="Seg_11972" s="T1040">pers</ta>
            <ta e="T1042" id="Seg_11973" s="T1041">v</ta>
            <ta e="T1043" id="Seg_11974" s="T1042">conj</ta>
            <ta e="T1181" id="Seg_11975" s="T1043">v</ta>
            <ta e="T1044" id="Seg_11976" s="T1181">v</ta>
            <ta e="T1045" id="Seg_11977" s="T1044">dempro</ta>
            <ta e="T1046" id="Seg_11978" s="T1045">dempro</ta>
            <ta e="T1047" id="Seg_11979" s="T1046">ptcl</ta>
            <ta e="T1048" id="Seg_11980" s="T1047">v</ta>
            <ta e="T1050" id="Seg_11981" s="T1049">adv</ta>
            <ta e="T1051" id="Seg_11982" s="T1050">ptcl</ta>
            <ta e="T1052" id="Seg_11983" s="T1051">v</ta>
            <ta e="T1056" id="Seg_11984" s="T1055">pers</ta>
            <ta e="T1057" id="Seg_11985" s="T1056">v</ta>
            <ta e="T1058" id="Seg_11986" s="T1057">v</ta>
            <ta e="T1060" id="Seg_11987" s="T1059">conj</ta>
            <ta e="T1061" id="Seg_11988" s="T1060">adv</ta>
            <ta e="T1062" id="Seg_11989" s="T1061">adv</ta>
            <ta e="T1063" id="Seg_11990" s="T1062">adj</ta>
            <ta e="T1064" id="Seg_11991" s="T1063">v</ta>
            <ta e="T1065" id="Seg_11992" s="T1064">n</ta>
            <ta e="T1067" id="Seg_11993" s="T1066">quant</ta>
            <ta e="T1068" id="Seg_11994" s="T1067">v</ta>
            <ta e="T1072" id="Seg_11995" s="T1071">n</ta>
            <ta e="T1073" id="Seg_11996" s="T1072">v</ta>
            <ta e="T1074" id="Seg_11997" s="T1073">n</ta>
            <ta e="T1075" id="Seg_11998" s="T1074">v</ta>
            <ta e="T1077" id="Seg_11999" s="T1076">v</ta>
            <ta e="T1078" id="Seg_12000" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_12001" s="T1078">adv</ta>
            <ta e="T1080" id="Seg_12002" s="T1079">num</ta>
            <ta e="T1081" id="Seg_12003" s="T1080">n</ta>
            <ta e="T1082" id="Seg_12004" s="T1081">v</ta>
            <ta e="T1086" id="Seg_12005" s="T1085">adv</ta>
            <ta e="T1087" id="Seg_12006" s="T1086">n</ta>
            <ta e="T1088" id="Seg_12007" s="T1087">v</ta>
            <ta e="T1089" id="Seg_12008" s="T1088">adv</ta>
            <ta e="T1090" id="Seg_12009" s="T1089">n</ta>
            <ta e="T1091" id="Seg_12010" s="T1090">n</ta>
            <ta e="T1092" id="Seg_12011" s="T1091">v</ta>
            <ta e="T1093" id="Seg_12012" s="T1092">conj</ta>
            <ta e="T1094" id="Seg_12013" s="T1093">n</ta>
            <ta e="T1095" id="Seg_12014" s="T1094">ptcl</ta>
            <ta e="T1096" id="Seg_12015" s="T1095">v</ta>
            <ta e="T1098" id="Seg_12016" s="T1097">conj</ta>
            <ta e="T1099" id="Seg_12017" s="T1098">n</ta>
            <ta e="T1100" id="Seg_12018" s="T1099">n</ta>
            <ta e="T1101" id="Seg_12019" s="T1100">ptcl</ta>
            <ta e="T1103" id="Seg_12020" s="T1102">v</ta>
            <ta e="T1104" id="Seg_12021" s="T1103">conj</ta>
            <ta e="T1105" id="Seg_12022" s="T1104">n</ta>
            <ta e="T1106" id="Seg_12023" s="T1105">ptcl</ta>
            <ta e="T1107" id="Seg_12024" s="T1106">v</ta>
            <ta e="T1111" id="Seg_12025" s="T1110">conj</ta>
            <ta e="T1113" id="Seg_12026" s="T1112">ptcl</ta>
            <ta e="T1114" id="Seg_12027" s="T1113">pers</ta>
            <ta e="T1115" id="Seg_12028" s="T1114">num</ta>
            <ta e="T1116" id="Seg_12029" s="T1115">v</ta>
            <ta e="T1117" id="Seg_12030" s="T1116">n</ta>
            <ta e="T1118" id="Seg_12031" s="T1117">v</ta>
            <ta e="T1119" id="Seg_12032" s="T1118">num</ta>
            <ta e="T1121" id="Seg_12033" s="T1120">que</ta>
            <ta e="T1122" id="Seg_12034" s="T1121">pers</ta>
            <ta e="T1123" id="Seg_12035" s="T1122">v</ta>
            <ta e="T1124" id="Seg_12036" s="T1123">adj</ta>
            <ta e="T1125" id="Seg_12037" s="T1124">conj</ta>
            <ta e="T1126" id="Seg_12038" s="T1125">adj</ta>
            <ta e="T1128" id="Seg_12039" s="T1127">ptcl</ta>
            <ta e="T1129" id="Seg_12040" s="T1128">v</ta>
            <ta e="T1130" id="Seg_12041" s="T1129">adj</ta>
            <ta e="T1134" id="Seg_12042" s="T1133">n</ta>
            <ta e="T1135" id="Seg_12043" s="T1134">v</ta>
            <ta e="T1136" id="Seg_12044" s="T1135">adv</ta>
            <ta e="T1137" id="Seg_12045" s="T1136">adv</ta>
            <ta e="T1138" id="Seg_12046" s="T1137">n</ta>
            <ta e="T1139" id="Seg_12047" s="T1138">quant</ta>
            <ta e="T1141" id="Seg_12048" s="T1140">adj</ta>
            <ta e="T1142" id="Seg_12049" s="T1141">n</ta>
            <ta e="T1143" id="Seg_12050" s="T1142">v</ta>
            <ta e="T1144" id="Seg_12051" s="T1143">adj</ta>
            <ta e="T1145" id="Seg_12052" s="T1144">n</ta>
            <ta e="T1146" id="Seg_12053" s="T1145">v</ta>
            <ta e="T1148" id="Seg_12054" s="T1147">adj</ta>
            <ta e="T1149" id="Seg_12055" s="T1148">v</ta>
            <ta e="T1150" id="Seg_12056" s="T1149">ptcl</ta>
            <ta e="T1154" id="Seg_12057" s="T1153">n</ta>
            <ta e="T1155" id="Seg_12058" s="T1154">ptcl</ta>
            <ta e="T1156" id="Seg_12059" s="T1155">adj</ta>
            <ta e="T1157" id="Seg_12060" s="T1156">conj</ta>
            <ta e="T1158" id="Seg_12061" s="T1157">n</ta>
            <ta e="T1159" id="Seg_12062" s="T1158">adj</ta>
            <ta e="T1163" id="Seg_12063" s="T1162">dempro</ta>
            <ta e="T1164" id="Seg_12064" s="T1163">n</ta>
            <ta e="T1165" id="Seg_12065" s="T1164">conj</ta>
            <ta e="T1166" id="Seg_12066" s="T1165">que</ta>
            <ta e="T1167" id="Seg_12067" s="T1166">dempro</ta>
            <ta e="T1168" id="Seg_12068" s="T1167">n</ta>
            <ta e="T1170" id="Seg_12069" s="T1169">n</ta>
            <ta e="T1171" id="Seg_12070" s="T1170">v</ta>
            <ta e="T1172" id="Seg_12071" s="T1171">ptcl</ta>
            <ta e="T1174" id="Seg_12072" s="T1173">v</ta>
            <ta e="T1175" id="Seg_12073" s="T1174">ptcl</ta>
            <ta e="T1176" id="Seg_12074" s="T1175">adv</ta>
            <ta e="T1177" id="Seg_12075" s="T1176">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_12076" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_12077" s="T2">adv:Time</ta>
            <ta e="T5" id="Seg_12078" s="T4">np:L</ta>
            <ta e="T7" id="Seg_12079" s="T6">pro.h:A</ta>
            <ta e="T9" id="Seg_12080" s="T8">0.2.h:A</ta>
            <ta e="T11" id="Seg_12081" s="T10">pro.h:A</ta>
            <ta e="T16" id="Seg_12082" s="T15">np.h:A</ta>
            <ta e="T20" id="Seg_12083" s="T19">pro.h:A</ta>
            <ta e="T24" id="Seg_12084" s="T23">pro.h:A</ta>
            <ta e="T25" id="Seg_12085" s="T24">np:L</ta>
            <ta e="T27" id="Seg_12086" s="T26">0.3.h:A</ta>
            <ta e="T30" id="Seg_12087" s="T29">np.h:Com</ta>
            <ta e="T32" id="Seg_12088" s="T31">0.3.h:A</ta>
            <ta e="T34" id="Seg_12089" s="T33">0.3.h:A</ta>
            <ta e="T36" id="Seg_12090" s="T35">0.3.h:A</ta>
            <ta e="T38" id="Seg_12091" s="T37">0.2.h:A</ta>
            <ta e="T39" id="Seg_12092" s="T38">0.2.h:A</ta>
            <ta e="T41" id="Seg_12093" s="T40">np:P</ta>
            <ta e="T42" id="Seg_12094" s="T41">0.2.h:A</ta>
            <ta e="T44" id="Seg_12095" s="T43">pro.h:A</ta>
            <ta e="T47" id="Seg_12096" s="T46">0.1.h:A</ta>
            <ta e="T50" id="Seg_12097" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_12098" s="T1178">0.1.h:A</ta>
            <ta e="T52" id="Seg_12099" s="T51">np:L</ta>
            <ta e="T56" id="Seg_12100" s="T55">pro.h:A</ta>
            <ta e="T57" id="Seg_12101" s="T56">adv:Time</ta>
            <ta e="T58" id="Seg_12102" s="T57">np:G</ta>
            <ta e="T60" id="Seg_12103" s="T59">pro.h:E</ta>
            <ta e="T63" id="Seg_12104" s="T62">pro.h:A</ta>
            <ta e="T68" id="Seg_12105" s="T67">0.2.h:A</ta>
            <ta e="T69" id="Seg_12106" s="T68">pro.h:P</ta>
            <ta e="T72" id="Seg_12107" s="T71">pro.h:P</ta>
            <ta e="T78" id="Seg_12108" s="T77">0.2.h:P</ta>
            <ta e="T81" id="Seg_12109" s="T80">0.2.h:P</ta>
            <ta e="T87" id="Seg_12110" s="T86">pro.h:Th</ta>
            <ta e="T88" id="Seg_12111" s="T87">pro.h:Poss</ta>
            <ta e="T89" id="Seg_12112" s="T88">np:L</ta>
            <ta e="T94" id="Seg_12113" s="T1185">pro.h:Th</ta>
            <ta e="T97" id="Seg_12114" s="T96">adv:L</ta>
            <ta e="T98" id="Seg_12115" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_12116" s="T98">pro.h:B</ta>
            <ta e="T105" id="Seg_12117" s="T104">pro.h:Poss</ta>
            <ta e="T106" id="Seg_12118" s="T105">np:L</ta>
            <ta e="T107" id="Seg_12119" s="T106">0.2.h:Th</ta>
            <ta e="T111" id="Seg_12120" s="T110">np.h:Th 0.1.h:Poss</ta>
            <ta e="T118" id="Seg_12121" s="T117">pro.h:Poss</ta>
            <ta e="T119" id="Seg_12122" s="T118">np:L</ta>
            <ta e="T120" id="Seg_12123" s="T119">0.2.h:Th</ta>
            <ta e="T123" id="Seg_12124" s="T122">np.h:A</ta>
            <ta e="T127" id="Seg_12125" s="T126">pro.h:A</ta>
            <ta e="T130" id="Seg_12126" s="T129">np:Th</ta>
            <ta e="T131" id="Seg_12127" s="T130">pro.h:Com</ta>
            <ta e="T134" id="Seg_12128" s="T133">pro.h:Com</ta>
            <ta e="T136" id="Seg_12129" s="T135">np:Th</ta>
            <ta e="T139" id="Seg_12130" s="T138">0.2.h:P</ta>
            <ta e="T143" id="Seg_12131" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_12132" s="T143">pro.h:Th</ta>
            <ta e="T147" id="Seg_12133" s="T146">pro.h:A</ta>
            <ta e="T148" id="Seg_12134" s="T147">np:G</ta>
            <ta e="T153" id="Seg_12135" s="T152">pro.h:A</ta>
            <ta e="T154" id="Seg_12136" s="T153">np:G</ta>
            <ta e="T159" id="Seg_12137" s="T158">np.h:A</ta>
            <ta e="T161" id="Seg_12138" s="T160">pro.h:Poss</ta>
            <ta e="T162" id="Seg_12139" s="T161">np:L</ta>
            <ta e="T163" id="Seg_12140" s="T162">adv:L</ta>
            <ta e="T164" id="Seg_12141" s="T163">np.h:Th</ta>
            <ta e="T167" id="Seg_12142" s="T166">0.3.h:A</ta>
            <ta e="T170" id="Seg_12143" s="T1180">0.3.h:A</ta>
            <ta e="T172" id="Seg_12144" s="T171">pro.h:P</ta>
            <ta e="T175" id="Seg_12145" s="T174">pro.h:A</ta>
            <ta e="T176" id="Seg_12146" s="T175">pro.h:P</ta>
            <ta e="T180" id="Seg_12147" s="T179">pro.h:A</ta>
            <ta e="T185" id="Seg_12148" s="T184">adv:Time</ta>
            <ta e="T186" id="Seg_12149" s="T185">pro.h:A</ta>
            <ta e="T188" id="Seg_12150" s="T187">0.1.h:A</ta>
            <ta e="T189" id="Seg_12151" s="T188">pro:G</ta>
            <ta e="T190" id="Seg_12152" s="T189">pro.h:A</ta>
            <ta e="T193" id="Seg_12153" s="T192">pro.h:P</ta>
            <ta e="T196" id="Seg_12154" s="T195">0.1.h:P</ta>
            <ta e="T199" id="Seg_12155" s="T198">pro.h:A</ta>
            <ta e="T202" id="Seg_12156" s="T201">0.2.h:P</ta>
            <ta e="T208" id="Seg_12157" s="T207">0.2.h:P</ta>
            <ta e="T211" id="Seg_12158" s="T210">pro.h:A</ta>
            <ta e="T212" id="Seg_12159" s="T211">np:P</ta>
            <ta e="T216" id="Seg_12160" s="T215">pro:G</ta>
            <ta e="T218" id="Seg_12161" s="T217">0.2.h:A</ta>
            <ta e="T220" id="Seg_12162" s="T219">adv:Time</ta>
            <ta e="T221" id="Seg_12163" s="T220">0.1.h:A</ta>
            <ta e="T226" id="Seg_12164" s="T225">np:G</ta>
            <ta e="T227" id="Seg_12165" s="T226">0.1.h:A</ta>
            <ta e="T242" id="Seg_12166" s="T241">np.h:A</ta>
            <ta e="T244" id="Seg_12167" s="T243">pro.h:R</ta>
            <ta e="T245" id="Seg_12168" s="T244">0.2.h:A</ta>
            <ta e="T246" id="Seg_12169" s="T245">np:P</ta>
            <ta e="T247" id="Seg_12170" s="T246">0.2.h:A</ta>
            <ta e="T248" id="Seg_12171" s="T247">pro.h:P</ta>
            <ta e="T250" id="Seg_12172" s="T249">0.1.h:P</ta>
            <ta e="T252" id="Seg_12173" s="T251">adv:Time</ta>
            <ta e="T254" id="Seg_12174" s="T252">np:P</ta>
            <ta e="T256" id="Seg_12175" s="T254">np:P</ta>
            <ta e="T257" id="Seg_12176" s="T256">0.2.h:A</ta>
            <ta e="T259" id="Seg_12177" s="T258">pro.h:A</ta>
            <ta e="T260" id="Seg_12178" s="T259">adv:Time</ta>
            <ta e="T263" id="Seg_12179" s="T262">np:P</ta>
            <ta e="T265" id="Seg_12180" s="T264">adv:Time</ta>
            <ta e="T266" id="Seg_12181" s="T265">np:Ins</ta>
            <ta e="T267" id="Seg_12182" s="T266">0.1.h:A</ta>
            <ta e="T270" id="Seg_12183" s="T269">0.1.h:A</ta>
            <ta e="T271" id="Seg_12184" s="T270">0.1.h:A</ta>
            <ta e="T272" id="Seg_12185" s="T271">pro.h:P</ta>
            <ta e="T275" id="Seg_12186" s="T274">np:G</ta>
            <ta e="T276" id="Seg_12187" s="T275">0.1.h:A</ta>
            <ta e="T277" id="Seg_12188" s="T276">np:Ins</ta>
            <ta e="T279" id="Seg_12189" s="T278">0.1.h:A</ta>
            <ta e="T280" id="Seg_12190" s="T279">np:G</ta>
            <ta e="T281" id="Seg_12191" s="T280">0.1.h:A</ta>
            <ta e="T283" id="Seg_12192" s="T282">adv:Time</ta>
            <ta e="T284" id="Seg_12193" s="T283">0.2.h:P</ta>
            <ta e="T286" id="Seg_12194" s="T285">np:P 0.3.h:Poss</ta>
            <ta e="T289" id="Seg_12195" s="T288">0.1.h:A</ta>
            <ta e="T292" id="Seg_12196" s="T291">0.3:P</ta>
            <ta e="T295" id="Seg_12197" s="T294">adv:Time</ta>
            <ta e="T296" id="Seg_12198" s="T295">np:L</ta>
            <ta e="T297" id="Seg_12199" s="T296">0.1.h:A</ta>
            <ta e="T300" id="Seg_12200" s="T299">np:Th</ta>
            <ta e="T303" id="Seg_12201" s="T302">np:L</ta>
            <ta e="T305" id="Seg_12202" s="T304">pro.h:A</ta>
            <ta e="T308" id="Seg_12203" s="T307">0.2.h:A</ta>
            <ta e="T309" id="Seg_12204" s="T308">np:Th</ta>
            <ta e="T311" id="Seg_12205" s="T310">pro:Th</ta>
            <ta e="T313" id="Seg_12206" s="T312">0.2.h:A</ta>
            <ta e="T319" id="Seg_12207" s="T318">np:Th</ta>
            <ta e="T320" id="Seg_12208" s="T319">0.1.h:A</ta>
            <ta e="T321" id="Seg_12209" s="T320">np.h:Poss</ta>
            <ta e="T322" id="Seg_12210" s="T321">np.h:R</ta>
            <ta e="T325" id="Seg_12211" s="T324">np:Th</ta>
            <ta e="T329" id="Seg_12212" s="T328">np:Th</ta>
            <ta e="T330" id="Seg_12213" s="T329">0.1.h:A</ta>
            <ta e="T331" id="Seg_12214" s="T330">np.h:R</ta>
            <ta e="T333" id="Seg_12215" s="T332">np.h:Th</ta>
            <ta e="T337" id="Seg_12216" s="T336">np:Th</ta>
            <ta e="T340" id="Seg_12217" s="T339">np:Th</ta>
            <ta e="T343" id="Seg_12218" s="T342">0.1.h:A</ta>
            <ta e="T344" id="Seg_12219" s="T343">np:Th</ta>
            <ta e="T345" id="Seg_12220" s="T344">adv:Time</ta>
            <ta e="T347" id="Seg_12221" s="T346">np.h:R</ta>
            <ta e="T348" id="Seg_12222" s="T347">np:Th</ta>
            <ta e="T349" id="Seg_12223" s="T348">0.1.h:A</ta>
            <ta e="T351" id="Seg_12224" s="T350">np:Th</ta>
            <ta e="T352" id="Seg_12225" s="T351">0.1.h:A</ta>
            <ta e="T354" id="Seg_12226" s="T353">adv:Time</ta>
            <ta e="T355" id="Seg_12227" s="T354">np.h:Poss</ta>
            <ta e="T356" id="Seg_12228" s="T355">np.h:R</ta>
            <ta e="T357" id="Seg_12229" s="T356">0.1.h:A</ta>
            <ta e="T358" id="Seg_12230" s="T357">np:Th</ta>
            <ta e="T364" id="Seg_12231" s="T363">adv:Time</ta>
            <ta e="T366" id="Seg_12232" s="T365">np.h:R</ta>
            <ta e="T367" id="Seg_12233" s="T366">np.h:R 0.3.h:Poss</ta>
            <ta e="T368" id="Seg_12234" s="T367">0.1.h:A</ta>
            <ta e="T369" id="Seg_12235" s="T368">np:Th</ta>
            <ta e="T371" id="Seg_12236" s="T370">np:Th</ta>
            <ta e="T372" id="Seg_12237" s="T371">0.1.h:A</ta>
            <ta e="T376" id="Seg_12238" s="T375">adv:Time</ta>
            <ta e="T378" id="Seg_12239" s="T377">np.h:R</ta>
            <ta e="T381" id="Seg_12240" s="T380">np:Th</ta>
            <ta e="T382" id="Seg_12241" s="T381">0.1.h:A</ta>
            <ta e="T384" id="Seg_12242" s="T383">np.h:A</ta>
            <ta e="T386" id="Seg_12243" s="T385">0.2.h:A</ta>
            <ta e="T387" id="Seg_12244" s="T386">np:Th</ta>
            <ta e="T389" id="Seg_12245" s="T388">pro.h:A</ta>
            <ta e="T390" id="Seg_12246" s="T389">pro.h:R</ta>
            <ta e="T397" id="Seg_12247" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_12248" s="T398">adv:Time</ta>
            <ta e="T407" id="Seg_12249" s="T406">pro.h:E</ta>
            <ta e="T408" id="Seg_12250" s="T407">adv:Time</ta>
            <ta e="T414" id="Seg_12251" s="T413">np:Poss</ta>
            <ta e="T421" id="Seg_12252" s="T420">pro.h:E</ta>
            <ta e="T428" id="Seg_12253" s="T427">adv:Time</ta>
            <ta e="T429" id="Seg_12254" s="T428">0.1.h:A</ta>
            <ta e="T430" id="Seg_12255" s="T429">np:Th</ta>
            <ta e="T432" id="Seg_12256" s="T431">0.1.h:A</ta>
            <ta e="T434" id="Seg_12257" s="T433">0.1.h:A</ta>
            <ta e="T436" id="Seg_12258" s="T435">0.1.h:A</ta>
            <ta e="T438" id="Seg_12259" s="T437">pro.h:A</ta>
            <ta e="T440" id="Seg_12260" s="T439">pro:G</ta>
            <ta e="T444" id="Seg_12261" s="T443">np:A</ta>
            <ta e="T446" id="Seg_12262" s="T445">np:Th</ta>
            <ta e="T450" id="Seg_12263" s="T449">0.1.h:E</ta>
            <ta e="T458" id="Seg_12264" s="T457">np:Th</ta>
            <ta e="T471" id="Seg_12265" s="T470">0.1.h:E</ta>
            <ta e="T474" id="Seg_12266" s="T473">0.1.h:E</ta>
            <ta e="T478" id="Seg_12267" s="T477">np.h:Th</ta>
            <ta e="T483" id="Seg_12268" s="T482">pro:Th</ta>
            <ta e="T484" id="Seg_12269" s="T483">pro.h:R</ta>
            <ta e="T485" id="Seg_12270" s="T484">0.3.h:A</ta>
            <ta e="T492" id="Seg_12271" s="T491">pro.h:R</ta>
            <ta e="T494" id="Seg_12272" s="T493">0.3.h:A</ta>
            <ta e="T502" id="Seg_12273" s="T501">np:Th</ta>
            <ta e="T508" id="Seg_12274" s="T507">adv:Time</ta>
            <ta e="T509" id="Seg_12275" s="T508">n:Time</ta>
            <ta e="T511" id="Seg_12276" s="T510">np:P</ta>
            <ta e="T513" id="Seg_12277" s="T512">np:Th</ta>
            <ta e="T516" id="Seg_12278" s="T515">adv:Time</ta>
            <ta e="T517" id="Seg_12279" s="T516">0.1.h:A</ta>
            <ta e="T526" id="Seg_12280" s="T525">0.1.h:A</ta>
            <ta e="T530" id="Seg_12281" s="T529">np:L</ta>
            <ta e="T531" id="Seg_12282" s="T530">np:Th</ta>
            <ta e="T532" id="Seg_12283" s="T531">0.1.h:A</ta>
            <ta e="T533" id="Seg_12284" s="T532">0.1.h:A</ta>
            <ta e="T537" id="Seg_12285" s="T536">0.3.h:Th</ta>
            <ta e="T538" id="Seg_12286" s="T537">np.h:A</ta>
            <ta e="T543" id="Seg_12287" s="T542">np:Th</ta>
            <ta e="T548" id="Seg_12288" s="T547">adv:Time</ta>
            <ta e="T549" id="Seg_12289" s="T548">n:Time</ta>
            <ta e="T550" id="Seg_12290" s="T549">np:L</ta>
            <ta e="T551" id="Seg_12291" s="T550">0.1.h:E</ta>
            <ta e="T553" id="Seg_12292" s="T552">0.1.h:A</ta>
            <ta e="T554" id="Seg_12293" s="T553">adv:Time</ta>
            <ta e="T560" id="Seg_12294" s="T559">pro.h:A</ta>
            <ta e="T562" id="Seg_12295" s="T561">np:G</ta>
            <ta e="T1186" id="Seg_12296" s="T562">np:Th</ta>
            <ta e="T564" id="Seg_12297" s="T563">0.1.h:A</ta>
            <ta e="T570" id="Seg_12298" s="T569">adv:Time</ta>
            <ta e="T571" id="Seg_12299" s="T570">np.h:A 0.1.h:Poss</ta>
            <ta e="T572" id="Seg_12300" s="T571">pro.h:Com</ta>
            <ta e="T575" id="Seg_12301" s="T574">pro.h:A</ta>
            <ta e="T576" id="Seg_12302" s="T575">pro.h:Th</ta>
            <ta e="T579" id="Seg_12303" s="T578">0.3.h:Th</ta>
            <ta e="T582" id="Seg_12304" s="T581">pro.h:E</ta>
            <ta e="T587" id="Seg_12305" s="T586">adv:Time</ta>
            <ta e="T588" id="Seg_12306" s="T587">0.1.h:E</ta>
            <ta e="T589" id="Seg_12307" s="T588">np.h:Th</ta>
            <ta e="T592" id="Seg_12308" s="T591">adv:L</ta>
            <ta e="T593" id="Seg_12309" s="T592">0.1.h:A</ta>
            <ta e="T596" id="Seg_12310" s="T595">np:Ins</ta>
            <ta e="T599" id="Seg_12311" s="T598">adv:L</ta>
            <ta e="T600" id="Seg_12312" s="T599">np:Ins</ta>
            <ta e="T602" id="Seg_12313" s="T601">adv:Time</ta>
            <ta e="T604" id="Seg_12314" s="T603">np:Ins</ta>
            <ta e="T608" id="Seg_12315" s="T607">adv:Time</ta>
            <ta e="T609" id="Seg_12316" s="T608">adv:L</ta>
            <ta e="T610" id="Seg_12317" s="T609">0.1.h:A</ta>
            <ta e="T614" id="Seg_12318" s="T613">adv:Time</ta>
            <ta e="T615" id="Seg_12319" s="T614">0.1.h:A</ta>
            <ta e="T616" id="Seg_12320" s="T615">np:R</ta>
            <ta e="T618" id="Seg_12321" s="T617">np:R</ta>
            <ta e="T621" id="Seg_12322" s="T620">adv:Time</ta>
            <ta e="T622" id="Seg_12323" s="T621">0.1.h:A</ta>
            <ta e="T623" id="Seg_12324" s="T622">np.h:Com</ta>
            <ta e="T626" id="Seg_12325" s="T625">adv:L</ta>
            <ta e="T628" id="Seg_12326" s="T627">0.1.h:Th</ta>
            <ta e="T630" id="Seg_12327" s="T629">0.1.h:A</ta>
            <ta e="T633" id="Seg_12328" s="T632">adv:Time</ta>
            <ta e="T634" id="Seg_12329" s="T633">np:G</ta>
            <ta e="T635" id="Seg_12330" s="T634">0.1.h:A</ta>
            <ta e="T639" id="Seg_12331" s="T638">np.h:A</ta>
            <ta e="T640" id="Seg_12332" s="T639">pro.h:R</ta>
            <ta e="T641" id="Seg_12333" s="T640">np:Th</ta>
            <ta e="T644" id="Seg_12334" s="T643">pro.h:Th</ta>
            <ta e="T646" id="Seg_12335" s="T645">np:L</ta>
            <ta e="T648" id="Seg_12336" s="T647">n:Time</ta>
            <ta e="T653" id="Seg_12337" s="T652">np:A</ta>
            <ta e="T655" id="Seg_12338" s="T654">np:G</ta>
            <ta e="T657" id="Seg_12339" s="T656">pro:Th</ta>
            <ta e="T658" id="Seg_12340" s="T657">np:L</ta>
            <ta e="T666" id="Seg_12341" s="T665">0.1.h:E</ta>
            <ta e="T667" id="Seg_12342" s="T666">0.1.h:E</ta>
            <ta e="T668" id="Seg_12343" s="T667">pro:Th</ta>
            <ta e="T669" id="Seg_12344" s="T668">0.1.h:E</ta>
            <ta e="T673" id="Seg_12345" s="T672">np:G</ta>
            <ta e="T674" id="Seg_12346" s="T673">0.1.h:A</ta>
            <ta e="T675" id="Seg_12347" s="T674">np:P</ta>
            <ta e="T680" id="Seg_12348" s="T679">adv:Time</ta>
            <ta e="T681" id="Seg_12349" s="T680">0.1.h:Th</ta>
            <ta e="T684" id="Seg_12350" s="T683">0.1.h:A</ta>
            <ta e="T686" id="Seg_12351" s="T685">pro:G</ta>
            <ta e="T687" id="Seg_12352" s="T686">0.1.h:A</ta>
            <ta e="T691" id="Seg_12353" s="T690">0.1.h:E</ta>
            <ta e="T698" id="Seg_12354" s="T697">np.h:A</ta>
            <ta e="T700" id="Seg_12355" s="T699">0.3.h:A</ta>
            <ta e="T701" id="Seg_12356" s="T700">adv:Time</ta>
            <ta e="T702" id="Seg_12357" s="T701">np:Th</ta>
            <ta e="T703" id="Seg_12358" s="T702">pro.h:Poss</ta>
            <ta e="T705" id="Seg_12359" s="T704">np:E</ta>
            <ta e="T709" id="Seg_12360" s="T708">np:Th</ta>
            <ta e="T713" id="Seg_12361" s="T712">pro.h:A</ta>
            <ta e="T717" id="Seg_12362" s="T716">0.2.h:A</ta>
            <ta e="T719" id="Seg_12363" s="T718">np:P</ta>
            <ta e="T723" id="Seg_12364" s="T722">pro.h:A</ta>
            <ta e="T725" id="Seg_12365" s="T724">np:P</ta>
            <ta e="T727" id="Seg_12366" s="T726">adv:Time</ta>
            <ta e="T728" id="Seg_12367" s="T727">pro.h:A</ta>
            <ta e="T729" id="Seg_12368" s="T728">np:G</ta>
            <ta e="T733" id="Seg_12369" s="T732">0.2.h:A</ta>
            <ta e="T734" id="Seg_12370" s="T733">np:P</ta>
            <ta e="T736" id="Seg_12371" s="T735">0.2.h:A</ta>
            <ta e="T740" id="Seg_12372" s="T739">pro.h:A</ta>
            <ta e="T743" id="Seg_12373" s="T742">np:P</ta>
            <ta e="T746" id="Seg_12374" s="T745">np:P</ta>
            <ta e="T751" id="Seg_12375" s="T750">adv:Time</ta>
            <ta e="T752" id="Seg_12376" s="T751">pro.h:A</ta>
            <ta e="T757" id="Seg_12377" s="T756">0.2.h:A</ta>
            <ta e="T759" id="Seg_12378" s="T758">np:P</ta>
            <ta e="T761" id="Seg_12379" s="T760">0.2.h:A</ta>
            <ta e="T763" id="Seg_12380" s="T762">adv:Time</ta>
            <ta e="T764" id="Seg_12381" s="T763">np.h:A</ta>
            <ta e="T766" id="Seg_12382" s="T765">pro.h:Poss</ta>
            <ta e="T767" id="Seg_12383" s="T766">np:L</ta>
            <ta e="T769" id="Seg_12384" s="T768">np:Th</ta>
            <ta e="T771" id="Seg_12385" s="T770">adv:Time</ta>
            <ta e="T772" id="Seg_12386" s="T771">pro.h:A</ta>
            <ta e="T775" id="Seg_12387" s="T774">np:G</ta>
            <ta e="T778" id="Seg_12388" s="T777">0.2.h:A</ta>
            <ta e="T779" id="Seg_12389" s="T778">np:Th</ta>
            <ta e="T780" id="Seg_12390" s="T779">np:So</ta>
            <ta e="T781" id="Seg_12391" s="T780">0.2.h:A</ta>
            <ta e="T783" id="Seg_12392" s="T782">np.h:A</ta>
            <ta e="T793" id="Seg_12393" s="T792">pro.h:A</ta>
            <ta e="T795" id="Seg_12394" s="T794">np:G</ta>
            <ta e="T798" id="Seg_12395" s="T797">0.2.h:A</ta>
            <ta e="T800" id="Seg_12396" s="T799">np:Th</ta>
            <ta e="T802" id="Seg_12397" s="T801">pro.h:A</ta>
            <ta e="T804" id="Seg_12398" s="T803">pro.h:Poss</ta>
            <ta e="T805" id="Seg_12399" s="T804">np:Th</ta>
            <ta e="T809" id="Seg_12400" s="T808">pro.h:B</ta>
            <ta e="T815" id="Seg_12401" s="T814">adv:Time</ta>
            <ta e="T816" id="Seg_12402" s="T815">0.3.h:A</ta>
            <ta e="T817" id="Seg_12403" s="T816">np:G</ta>
            <ta e="T820" id="Seg_12404" s="T819">0.2.h:A</ta>
            <ta e="T821" id="Seg_12405" s="T820">np.h:Poss</ta>
            <ta e="T822" id="Seg_12406" s="T821">np:P</ta>
            <ta e="T823" id="Seg_12407" s="T822">0.2.h:A</ta>
            <ta e="T827" id="Seg_12408" s="T826">pro.h:A</ta>
            <ta e="T833" id="Seg_12409" s="T832">pro.h:B</ta>
            <ta e="T835" id="Seg_12410" s="T834">np:L</ta>
            <ta e="T836" id="Seg_12411" s="T835">np:P</ta>
            <ta e="T841" id="Seg_12412" s="T840">adv:Time</ta>
            <ta e="T842" id="Seg_12413" s="T841">0.3.h:A</ta>
            <ta e="T844" id="Seg_12414" s="T843">np:G</ta>
            <ta e="T847" id="Seg_12415" s="T846">0.2.h:A</ta>
            <ta e="T848" id="Seg_12416" s="T847">np:P</ta>
            <ta e="T849" id="Seg_12417" s="T848">pro.h:A</ta>
            <ta e="T850" id="Seg_12418" s="T849">pro.h:A</ta>
            <ta e="T854" id="Seg_12419" s="T853">np:Ins</ta>
            <ta e="T858" id="Seg_12420" s="T857">pro.h:Poss</ta>
            <ta e="T859" id="Seg_12421" s="T858">np:L</ta>
            <ta e="T863" id="Seg_12422" s="T862">pro.h:E</ta>
            <ta e="T864" id="Seg_12423" s="T863">np:Th</ta>
            <ta e="T871" id="Seg_12424" s="T870">np:Th</ta>
            <ta e="T875" id="Seg_12425" s="T874">np:L</ta>
            <ta e="T876" id="Seg_12426" s="T875">pro:Th</ta>
            <ta e="T878" id="Seg_12427" s="T877">pro:Th</ta>
            <ta e="T880" id="Seg_12428" s="T879">0.2.h:A</ta>
            <ta e="T883" id="Seg_12429" s="T882">0.2.h:A</ta>
            <ta e="T884" id="Seg_12430" s="T883">adv:L</ta>
            <ta e="T886" id="Seg_12431" s="T885">np:Th</ta>
            <ta e="T887" id="Seg_12432" s="T886">0.2.h:A</ta>
            <ta e="T889" id="Seg_12433" s="T888">adv:Time</ta>
            <ta e="T890" id="Seg_12434" s="T889">0.2.h:A</ta>
            <ta e="T895" id="Seg_12435" s="T894">np.h:A</ta>
            <ta e="T908" id="Seg_12436" s="T907">np.h:A</ta>
            <ta e="T914" id="Seg_12437" s="T913">pro:Th</ta>
            <ta e="T921" id="Seg_12438" s="T920">pro:Th</ta>
            <ta e="T923" id="Seg_12439" s="T922">0.3.h:E</ta>
            <ta e="T927" id="Seg_12440" s="T926">adv:L</ta>
            <ta e="T928" id="Seg_12441" s="T927">np.h:Poss</ta>
            <ta e="T931" id="Seg_12442" s="T930">np:Th</ta>
            <ta e="T940" id="Seg_12443" s="T939">np.h:P 0.3.h:Poss</ta>
            <ta e="T945" id="Seg_12444" s="T944">0.1.h:A</ta>
            <ta e="T947" id="Seg_12445" s="T946">adv:Time</ta>
            <ta e="T948" id="Seg_12446" s="T947">0.3:Th</ta>
            <ta e="T953" id="Seg_12447" s="T952">pro.h:A</ta>
            <ta e="T956" id="Seg_12448" s="T955">np:Th</ta>
            <ta e="T959" id="Seg_12449" s="T958">adv:L</ta>
            <ta e="T960" id="Seg_12450" s="T959">np:Th</ta>
            <ta e="T966" id="Seg_12451" s="T965">np:Th</ta>
            <ta e="T967" id="Seg_12452" s="T966">0.1.h:A</ta>
            <ta e="T970" id="Seg_12453" s="T969">adv:Time</ta>
            <ta e="T974" id="Seg_12454" s="T973">np:L</ta>
            <ta e="T975" id="Seg_12455" s="T974">0.1.h:A</ta>
            <ta e="T976" id="Seg_12456" s="T975">np:Th</ta>
            <ta e="T977" id="Seg_12457" s="T976">0.1.h:A</ta>
            <ta e="T979" id="Seg_12458" s="T978">np:L</ta>
            <ta e="T980" id="Seg_12459" s="T979">0.1.h:A</ta>
            <ta e="T981" id="Seg_12460" s="T980">np:Th</ta>
            <ta e="T982" id="Seg_12461" s="T981">0.1.h:A</ta>
            <ta e="T984" id="Seg_12462" s="T983">np:Pth</ta>
            <ta e="T985" id="Seg_12463" s="T984">0.1.h:A</ta>
            <ta e="T986" id="Seg_12464" s="T985">np:Th</ta>
            <ta e="T987" id="Seg_12465" s="T986">0.1.h:A</ta>
            <ta e="T989" id="Seg_12466" s="T988">np:L</ta>
            <ta e="T990" id="Seg_12467" s="T989">0.1.h:A</ta>
            <ta e="T991" id="Seg_12468" s="T990">np:Th</ta>
            <ta e="T992" id="Seg_12469" s="T991">0.1.h:A</ta>
            <ta e="T994" id="Seg_12470" s="T993">np:Pth</ta>
            <ta e="T995" id="Seg_12471" s="T994">0.3.h:A</ta>
            <ta e="T996" id="Seg_12472" s="T995">np:Pth</ta>
            <ta e="T998" id="Seg_12473" s="T997">0.1.h:A</ta>
            <ta e="T1000" id="Seg_12474" s="T999">np:L</ta>
            <ta e="T1001" id="Seg_12475" s="T1000">0.3.h:Th</ta>
            <ta e="T1002" id="Seg_12476" s="T1001">np:Th</ta>
            <ta e="T1003" id="Seg_12477" s="T1002">0.1.h:A</ta>
            <ta e="T1008" id="Seg_12478" s="T1007">pro.h:Poss</ta>
            <ta e="T1009" id="Seg_12479" s="T1008">np:L</ta>
            <ta e="T1011" id="Seg_12480" s="T1010">np:Th</ta>
            <ta e="T1013" id="Seg_12481" s="T1012">pro:P</ta>
            <ta e="T1016" id="Seg_12482" s="T1015">0.3.h:A</ta>
            <ta e="T1019" id="Seg_12483" s="T1018">np:Th</ta>
            <ta e="T1022" id="Seg_12484" s="T1021">0.2.h:A</ta>
            <ta e="T1025" id="Seg_12485" s="T1024">adv:L</ta>
            <ta e="T1028" id="Seg_12486" s="T1027">np:Th</ta>
            <ta e="T1029" id="Seg_12487" s="T1028">np:Th</ta>
            <ta e="T1031" id="Seg_12488" s="T1030">np:Th</ta>
            <ta e="T1034" id="Seg_12489" s="T1033">pro.h:A</ta>
            <ta e="T1036" id="Seg_12490" s="T1035">pro.h:R</ta>
            <ta e="T1039" id="Seg_12491" s="T1038">pro.h:A</ta>
            <ta e="T1040" id="Seg_12492" s="T1039">np:Th</ta>
            <ta e="T1041" id="Seg_12493" s="T1040">pro:So</ta>
            <ta e="T1044" id="Seg_12494" s="T1181">0.3.h:A</ta>
            <ta e="T1045" id="Seg_12495" s="T1044">pro.h:A</ta>
            <ta e="T1046" id="Seg_12496" s="T1045">pro:Th</ta>
            <ta e="T1052" id="Seg_12497" s="T1051">0.3.h:A</ta>
            <ta e="T1056" id="Seg_12498" s="T1055">pro.h:A</ta>
            <ta e="T1061" id="Seg_12499" s="T1060">adv:L</ta>
            <ta e="T1064" id="Seg_12500" s="T1063">0.1.h:E</ta>
            <ta e="T1065" id="Seg_12501" s="T1064">np:Th</ta>
            <ta e="T1072" id="Seg_12502" s="T1071">n:Time</ta>
            <ta e="T1073" id="Seg_12503" s="T1072">0.1.h:A</ta>
            <ta e="T1074" id="Seg_12504" s="T1073">np:L</ta>
            <ta e="T1075" id="Seg_12505" s="T1074">0.1.h:A</ta>
            <ta e="T1077" id="Seg_12506" s="T1076">0.1.h:E</ta>
            <ta e="T1079" id="Seg_12507" s="T1078">adv:L</ta>
            <ta e="T1081" id="Seg_12508" s="T1080">np:Th</ta>
            <ta e="T1086" id="Seg_12509" s="T1085">adv:Time</ta>
            <ta e="T1087" id="Seg_12510" s="T1086">np:Th</ta>
            <ta e="T1088" id="Seg_12511" s="T1087">0.1.h:A</ta>
            <ta e="T1089" id="Seg_12512" s="T1088">adv:G</ta>
            <ta e="T1090" id="Seg_12513" s="T1089">np:L</ta>
            <ta e="T1092" id="Seg_12514" s="T1091">0.1.h:A</ta>
            <ta e="T1094" id="Seg_12515" s="T1093">np:A</ta>
            <ta e="T1099" id="Seg_12516" s="T1098">np:Th</ta>
            <ta e="T1100" id="Seg_12517" s="T1099">np:G</ta>
            <ta e="T1103" id="Seg_12518" s="T1102">0.1.h:A</ta>
            <ta e="T1105" id="Seg_12519" s="T1104">np:A</ta>
            <ta e="T1118" id="Seg_12520" s="T1117">0.1.h:A</ta>
            <ta e="T1119" id="Seg_12521" s="T1118">np:Th</ta>
            <ta e="T1129" id="Seg_12522" s="T1128">0.2.h:A</ta>
            <ta e="T1130" id="Seg_12523" s="T1129">np:Th</ta>
            <ta e="T1134" id="Seg_12524" s="T1133">np:G</ta>
            <ta e="T1135" id="Seg_12525" s="T1134">0.3.h:A</ta>
            <ta e="T1136" id="Seg_12526" s="T1135">adv:L</ta>
            <ta e="T1138" id="Seg_12527" s="T1137">np:Th</ta>
            <ta e="T1142" id="Seg_12528" s="T1141">np:Th</ta>
            <ta e="T1145" id="Seg_12529" s="T1144">np:Th</ta>
            <ta e="T1154" id="Seg_12530" s="T1153">np:E</ta>
            <ta e="T1159" id="Seg_12531" s="T1158">0.3:Th</ta>
            <ta e="T1163" id="Seg_12532" s="T1162">pro:Th</ta>
            <ta e="T1166" id="Seg_12533" s="T1165">adv:L</ta>
            <ta e="T1168" id="Seg_12534" s="T1167">np:Th</ta>
            <ta e="T1170" id="Seg_12535" s="T1169">np:L</ta>
            <ta e="T1174" id="Seg_12536" s="T1173">0.2.h:A</ta>
            <ta e="T1177" id="Seg_12537" s="T1176">pro.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_12538" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_12539" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_12540" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_12541" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_12542" s="T8">v:pred 0.2.h:S</ta>
            <ta e="T11" id="Seg_12543" s="T10">pro.h:S</ta>
            <ta e="T14" id="Seg_12544" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_12545" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_12546" s="T16">ptcl.neg</ta>
            <ta e="T18" id="Seg_12547" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_12548" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_12549" s="T20">ptcl.neg</ta>
            <ta e="T22" id="Seg_12550" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_12551" s="T23">pro.h:S</ta>
            <ta e="T26" id="Seg_12552" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_12553" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_12554" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_12555" s="T33">v:pred 0.3.h:S</ta>
            <ta e="T36" id="Seg_12556" s="T35">v:pred 0.3.h:S</ta>
            <ta e="T38" id="Seg_12557" s="T37">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_12558" s="T38">v:pred 0.2.h:S</ta>
            <ta e="T41" id="Seg_12559" s="T40">np:O</ta>
            <ta e="T42" id="Seg_12560" s="T41">v:pred 0.2.h:S</ta>
            <ta e="T44" id="Seg_12561" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_12562" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_12563" s="T45">ptcl.neg</ta>
            <ta e="T47" id="Seg_12564" s="T46">v:pred 0.1.h:S</ta>
            <ta e="T1178" id="Seg_12565" s="T50">conv:pred</ta>
            <ta e="T51" id="Seg_12566" s="T1178">v:pred 0.1.h:S</ta>
            <ta e="T56" id="Seg_12567" s="T55">pro.h:S</ta>
            <ta e="T59" id="Seg_12568" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_12569" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_12570" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_12571" s="T62">pro.h:S</ta>
            <ta e="T64" id="Seg_12572" s="T63">v:pred</ta>
            <ta e="T67" id="Seg_12573" s="T66">ptcl.neg</ta>
            <ta e="T68" id="Seg_12574" s="T67">v:pred 0.2.h:S</ta>
            <ta e="T69" id="Seg_12575" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_12576" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_12577" s="T71">pro.h:S</ta>
            <ta e="T74" id="Seg_12578" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_12579" s="T76">ptcl.neg</ta>
            <ta e="T78" id="Seg_12580" s="T77">v:pred 0.2.h:S</ta>
            <ta e="T81" id="Seg_12581" s="T80">v:pred 0.2.h:S</ta>
            <ta e="T87" id="Seg_12582" s="T86">pro.h:S</ta>
            <ta e="T90" id="Seg_12583" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_12584" s="T90">ptcl:pred</ta>
            <ta e="T94" id="Seg_12585" s="T1185">pro.h:S</ta>
            <ta e="T95" id="Seg_12586" s="T94">v:pred</ta>
            <ta e="T98" id="Seg_12587" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_12588" s="T101">ptcl:pred</ta>
            <ta e="T107" id="Seg_12589" s="T106">v:pred 0.2.h:S</ta>
            <ta e="T111" id="Seg_12590" s="T110">np.h:S</ta>
            <ta e="T115" id="Seg_12591" s="T114">v:pred</ta>
            <ta e="T120" id="Seg_12592" s="T119">v:pred 0.2.h:S</ta>
            <ta e="T123" id="Seg_12593" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_12594" s="T123">v:pred</ta>
            <ta e="T127" id="Seg_12595" s="T126">pro.h:S</ta>
            <ta e="T128" id="Seg_12596" s="T127">v:pred</ta>
            <ta e="T130" id="Seg_12597" s="T129">np:S</ta>
            <ta e="T135" id="Seg_12598" s="T134">ptcl:pred</ta>
            <ta e="T139" id="Seg_12599" s="T138">v:pred 0.2.h:S</ta>
            <ta e="T144" id="Seg_12600" s="T143">pro.h:S</ta>
            <ta e="T145" id="Seg_12601" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_12602" s="T146">pro.h:S</ta>
            <ta e="T1179" id="Seg_12603" s="T148">conv:pred</ta>
            <ta e="T149" id="Seg_12604" s="T1179">v:pred</ta>
            <ta e="T153" id="Seg_12605" s="T152">pro.h:S</ta>
            <ta e="T155" id="Seg_12606" s="T154">v:pred</ta>
            <ta e="T159" id="Seg_12607" s="T158">np.h:S</ta>
            <ta e="T160" id="Seg_12608" s="T159">v:pred</ta>
            <ta e="T164" id="Seg_12609" s="T163">np.h:S</ta>
            <ta e="T165" id="Seg_12610" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_12611" s="T166">v:pred 0.3.h:S</ta>
            <ta e="T1180" id="Seg_12612" s="T169">conv:pred</ta>
            <ta e="T170" id="Seg_12613" s="T1180">v:pred 0.3.h:S</ta>
            <ta e="T172" id="Seg_12614" s="T171">pro.h:S</ta>
            <ta e="T173" id="Seg_12615" s="T172">v:pred</ta>
            <ta e="T175" id="Seg_12616" s="T174">pro.h:S</ta>
            <ta e="T178" id="Seg_12617" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_12618" s="T179">pro.h:S</ta>
            <ta e="T182" id="Seg_12619" s="T181">v:pred</ta>
            <ta e="T186" id="Seg_12620" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_12621" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_12622" s="T187">v:pred 0.1.h:S</ta>
            <ta e="T190" id="Seg_12623" s="T189">pro.h:S</ta>
            <ta e="T1182" id="Seg_12624" s="T190">conv:pred</ta>
            <ta e="T191" id="Seg_12625" s="T1182">v:pred</ta>
            <ta e="T193" id="Seg_12626" s="T192">pro.h:S</ta>
            <ta e="T194" id="Seg_12627" s="T193">v:pred</ta>
            <ta e="T196" id="Seg_12628" s="T195">v:pred 0.1.h:S</ta>
            <ta e="T199" id="Seg_12629" s="T198">pro.h:S</ta>
            <ta e="T200" id="Seg_12630" s="T199">v:pred</ta>
            <ta e="T202" id="Seg_12631" s="T201">v:pred 0.2.h:S</ta>
            <ta e="T208" id="Seg_12632" s="T207">v:pred 0.2.h:S</ta>
            <ta e="T211" id="Seg_12633" s="T210">pro.h:S</ta>
            <ta e="T212" id="Seg_12634" s="T211">np:O</ta>
            <ta e="T213" id="Seg_12635" s="T212">v:pred</ta>
            <ta e="T218" id="Seg_12636" s="T217">v:pred 0.2.h:S</ta>
            <ta e="T221" id="Seg_12637" s="T220">v:pred 0.1.h:S</ta>
            <ta e="T227" id="Seg_12638" s="T226">v:pred 0.1.h:S</ta>
            <ta e="T242" id="Seg_12639" s="T241">np.h:S</ta>
            <ta e="T245" id="Seg_12640" s="T244">v:pred 0.2.h:S</ta>
            <ta e="T246" id="Seg_12641" s="T245">np:O</ta>
            <ta e="T247" id="Seg_12642" s="T246">v:pred 0.2.h:S</ta>
            <ta e="T250" id="Seg_12643" s="T249">v:pred 0.1.h:S</ta>
            <ta e="T254" id="Seg_12644" s="T252">np:O</ta>
            <ta e="T256" id="Seg_12645" s="T254">np:O</ta>
            <ta e="T257" id="Seg_12646" s="T256">v:pred 0.2.h:S</ta>
            <ta e="T259" id="Seg_12647" s="T258">pro.h:S</ta>
            <ta e="T261" id="Seg_12648" s="T260">v:pred</ta>
            <ta e="T263" id="Seg_12649" s="T262">np:O</ta>
            <ta e="T267" id="Seg_12650" s="T266">v:pred 0.1.h:S</ta>
            <ta e="T270" id="Seg_12651" s="T269">v:pred 0.1.h:S</ta>
            <ta e="T271" id="Seg_12652" s="T270">v:pred 0.1.h:S</ta>
            <ta e="T272" id="Seg_12653" s="T271">pro.h:O</ta>
            <ta e="T276" id="Seg_12654" s="T275">v:pred 0.1.h:S</ta>
            <ta e="T279" id="Seg_12655" s="T278">v:pred 0.1.h:S</ta>
            <ta e="T281" id="Seg_12656" s="T280">v:pred 0.1.h:S</ta>
            <ta e="T284" id="Seg_12657" s="T283">v:pred 0.2.h:S</ta>
            <ta e="T286" id="Seg_12658" s="T285">np:O</ta>
            <ta e="T289" id="Seg_12659" s="T288">v:pred 0.1.h:S</ta>
            <ta e="T291" id="Seg_12660" s="T290">adj:pred</ta>
            <ta e="T292" id="Seg_12661" s="T291">cop 0.3:S</ta>
            <ta e="T297" id="Seg_12662" s="T296">v:pred 0.1.h:S</ta>
            <ta e="T300" id="Seg_12663" s="T299">np:S</ta>
            <ta e="T301" id="Seg_12664" s="T300">adj:pred</ta>
            <ta e="T302" id="Seg_12665" s="T301">cop</ta>
            <ta e="T305" id="Seg_12666" s="T304">pro.h:S</ta>
            <ta e="T306" id="Seg_12667" s="T305">v:pred</ta>
            <ta e="T308" id="Seg_12668" s="T307">v:pred 0.2.h:S</ta>
            <ta e="T309" id="Seg_12669" s="T308">np:O</ta>
            <ta e="T311" id="Seg_12670" s="T310">pro:O</ta>
            <ta e="T313" id="Seg_12671" s="T312">v:pred 0.2.h:S</ta>
            <ta e="T319" id="Seg_12672" s="T318">np:O</ta>
            <ta e="T320" id="Seg_12673" s="T319">v:pred 0.1.h:S</ta>
            <ta e="T325" id="Seg_12674" s="T324">np:S</ta>
            <ta e="T326" id="Seg_12675" s="T325">v:pred</ta>
            <ta e="T329" id="Seg_12676" s="T328">np:O</ta>
            <ta e="T330" id="Seg_12677" s="T329">v:pred 0.1.h:S</ta>
            <ta e="T333" id="Seg_12678" s="T332">np.h:S</ta>
            <ta e="T334" id="Seg_12679" s="T333">v:pred</ta>
            <ta e="T336" id="Seg_12680" s="T335">v:pred</ta>
            <ta e="T337" id="Seg_12681" s="T336">np:S</ta>
            <ta e="T340" id="Seg_12682" s="T339">np:S</ta>
            <ta e="T341" id="Seg_12683" s="T340">v:pred</ta>
            <ta e="T343" id="Seg_12684" s="T342">v:pred 0.1.h:S</ta>
            <ta e="T344" id="Seg_12685" s="T343">np:O</ta>
            <ta e="T348" id="Seg_12686" s="T347">np:O</ta>
            <ta e="T349" id="Seg_12687" s="T348">v:pred 0.1.h:S</ta>
            <ta e="T351" id="Seg_12688" s="T350">np:O</ta>
            <ta e="T352" id="Seg_12689" s="T351">v:pred 0.1.h:S</ta>
            <ta e="T357" id="Seg_12690" s="T356">v:pred 0.1.h:S</ta>
            <ta e="T358" id="Seg_12691" s="T357">np:O</ta>
            <ta e="T368" id="Seg_12692" s="T367">v:pred 0.1.h:S</ta>
            <ta e="T369" id="Seg_12693" s="T368">np:O</ta>
            <ta e="T371" id="Seg_12694" s="T370">np:O</ta>
            <ta e="T372" id="Seg_12695" s="T371">v:pred 0.1.h:S</ta>
            <ta e="T381" id="Seg_12696" s="T380">np:O</ta>
            <ta e="T382" id="Seg_12697" s="T381">v:pred 0.1.h:S</ta>
            <ta e="T384" id="Seg_12698" s="T383">np.h:S</ta>
            <ta e="T385" id="Seg_12699" s="T384">v:pred</ta>
            <ta e="T386" id="Seg_12700" s="T385">v:pred 0.2.h:S</ta>
            <ta e="T387" id="Seg_12701" s="T386">np:O</ta>
            <ta e="T389" id="Seg_12702" s="T388">pro.h:S</ta>
            <ta e="T391" id="Seg_12703" s="T390">v:pred</ta>
            <ta e="T396" id="Seg_12704" s="T395">ptcl.neg</ta>
            <ta e="T397" id="Seg_12705" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T407" id="Seg_12706" s="T406">pro.h:S</ta>
            <ta e="T409" id="Seg_12707" s="T408">v:pred</ta>
            <ta e="T410" id="Seg_12708" s="T409">ptcl.neg</ta>
            <ta e="T412" id="Seg_12709" s="T411">v:pred</ta>
            <ta e="T416" id="Seg_12710" s="T415">ptcl:pred</ta>
            <ta e="T421" id="Seg_12711" s="T420">pro.h:S</ta>
            <ta e="T422" id="Seg_12712" s="T421">v:pred</ta>
            <ta e="T429" id="Seg_12713" s="T428">v:pred 0.1.h:S</ta>
            <ta e="T430" id="Seg_12714" s="T429">np:O</ta>
            <ta e="T432" id="Seg_12715" s="T431">v:pred 0.1.h:S</ta>
            <ta e="T434" id="Seg_12716" s="T433">v:pred 0.1.h:S</ta>
            <ta e="T436" id="Seg_12717" s="T435">v:pred 0.1.h:S</ta>
            <ta e="T438" id="Seg_12718" s="T437">pro.h:S</ta>
            <ta e="T439" id="Seg_12719" s="T438">v:pred</ta>
            <ta e="T444" id="Seg_12720" s="T443">np:S</ta>
            <ta e="T446" id="Seg_12721" s="T445">np:O</ta>
            <ta e="T447" id="Seg_12722" s="T446">v:pred</ta>
            <ta e="T449" id="Seg_12723" s="T448">ptcl.neg</ta>
            <ta e="T450" id="Seg_12724" s="T449">v:pred 0.1.h:S</ta>
            <ta e="T457" id="Seg_12725" s="T456">adj:pred</ta>
            <ta e="T458" id="Seg_12726" s="T457">np:S</ta>
            <ta e="T463" id="Seg_12727" s="T462">adj:pred</ta>
            <ta e="T471" id="Seg_12728" s="T470">v:pred 0.1.h:S</ta>
            <ta e="T474" id="Seg_12729" s="T473">v:pred 0.1.h:S</ta>
            <ta e="T478" id="Seg_12730" s="T477">np.h:S</ta>
            <ta e="T480" id="Seg_12731" s="T479">adj:pred</ta>
            <ta e="T483" id="Seg_12732" s="T482">pro:O</ta>
            <ta e="T485" id="Seg_12733" s="T484">v:pred 0.3.h:S</ta>
            <ta e="T494" id="Seg_12734" s="T493">v:pred 0.3.h:S</ta>
            <ta e="T501" id="Seg_12735" s="T500">v:pred</ta>
            <ta e="T502" id="Seg_12736" s="T501">np:S</ta>
            <ta e="T503" id="Seg_12737" s="T502">v:pred</ta>
            <ta e="T504" id="Seg_12738" s="T503">s:purp</ta>
            <ta e="T508" id="Seg_12739" s="T507">adj:pred</ta>
            <ta e="T509" id="Seg_12740" s="T508">np:S</ta>
            <ta e="T510" id="Seg_12741" s="T509">ptcl:pred</ta>
            <ta e="T511" id="Seg_12742" s="T510">np:O</ta>
            <ta e="T513" id="Seg_12743" s="T512">np:O</ta>
            <ta e="T514" id="Seg_12744" s="T513">v:pred</ta>
            <ta e="T517" id="Seg_12745" s="T516">v:pred 0.1.h:S</ta>
            <ta e="T520" id="Seg_12746" s="T519">ptcl:pred</ta>
            <ta e="T525" id="Seg_12747" s="T524">ptcl.neg</ta>
            <ta e="T526" id="Seg_12748" s="T525">v:pred 0.1.h:S</ta>
            <ta e="T531" id="Seg_12749" s="T530">np:O</ta>
            <ta e="T532" id="Seg_12750" s="T531">v:pred 0.1.h:S</ta>
            <ta e="T533" id="Seg_12751" s="T532">v:pred 0.1.h:S</ta>
            <ta e="T537" id="Seg_12752" s="T536">v:pred 0.3.h:S</ta>
            <ta e="T538" id="Seg_12753" s="T537">np.h:S</ta>
            <ta e="T539" id="Seg_12754" s="T538">ptcl.neg</ta>
            <ta e="T540" id="Seg_12755" s="T539">v:pred</ta>
            <ta e="T543" id="Seg_12756" s="T542">np:S</ta>
            <ta e="T544" id="Seg_12757" s="T543">ptcl.neg</ta>
            <ta e="T545" id="Seg_12758" s="T544">v:pred</ta>
            <ta e="T551" id="Seg_12759" s="T550">v:pred 0.1.h:S</ta>
            <ta e="T553" id="Seg_12760" s="T552">v:pred 0.1.h:S</ta>
            <ta e="T555" id="Seg_12761" s="T554">np:S</ta>
            <ta e="T556" id="Seg_12762" s="T555">v:pred</ta>
            <ta e="T560" id="Seg_12763" s="T559">pro.h:S</ta>
            <ta e="T561" id="Seg_12764" s="T560">v:pred</ta>
            <ta e="T1186" id="Seg_12765" s="T562">np:O</ta>
            <ta e="T564" id="Seg_12766" s="T563">v:pred 0.1.h:S</ta>
            <ta e="T571" id="Seg_12767" s="T570">np.h:S</ta>
            <ta e="T573" id="Seg_12768" s="T572">v:pred</ta>
            <ta e="T575" id="Seg_12769" s="T574">pro.h:S</ta>
            <ta e="T577" id="Seg_12770" s="T576">v:pred</ta>
            <ta e="T579" id="Seg_12771" s="T578">v:pred 0.3.h:S</ta>
            <ta e="T582" id="Seg_12772" s="T581">pro.h:S</ta>
            <ta e="T584" id="Seg_12773" s="T583">ptcl.neg</ta>
            <ta e="T585" id="Seg_12774" s="T584">v:pred</ta>
            <ta e="T588" id="Seg_12775" s="T587">v:pred 0.1.h:S</ta>
            <ta e="T589" id="Seg_12776" s="T588">np.h:O</ta>
            <ta e="T593" id="Seg_12777" s="T592">v:pred 0.1.h:S</ta>
            <ta e="T610" id="Seg_12778" s="T609">v:pred 0.1.h:S</ta>
            <ta e="T615" id="Seg_12779" s="T614">v:pred 0.1.h:S</ta>
            <ta e="T622" id="Seg_12780" s="T621">v:pred 0.1.h:S</ta>
            <ta e="T628" id="Seg_12781" s="T627">v:pred 0.1.h:S</ta>
            <ta e="T630" id="Seg_12782" s="T629">v:pred 0.1.h:S</ta>
            <ta e="T635" id="Seg_12783" s="T634">v:pred 0.1.h:S</ta>
            <ta e="T639" id="Seg_12784" s="T638">np.h:S</ta>
            <ta e="T641" id="Seg_12785" s="T640">np:O</ta>
            <ta e="T643" id="Seg_12786" s="T642">v:pred</ta>
            <ta e="T644" id="Seg_12787" s="T643">pro.h:S</ta>
            <ta e="T649" id="Seg_12788" s="T648">v:pred</ta>
            <ta e="T653" id="Seg_12789" s="T652">np:S</ta>
            <ta e="T654" id="Seg_12790" s="T653">v:pred</ta>
            <ta e="T656" id="Seg_12791" s="T655">ptcl:pred</ta>
            <ta e="T657" id="Seg_12792" s="T656">pro:O</ta>
            <ta e="T666" id="Seg_12793" s="T665">v:pred 0.1.h:S</ta>
            <ta e="T667" id="Seg_12794" s="T666">v:pred 0.1.h:S</ta>
            <ta e="T668" id="Seg_12795" s="T667">pro:O</ta>
            <ta e="T669" id="Seg_12796" s="T668">v:pred 0.1.h:S</ta>
            <ta e="T674" id="Seg_12797" s="T673">v:pred 0.1.h:S</ta>
            <ta e="T675" id="Seg_12798" s="T674">np:S</ta>
            <ta e="T678" id="Seg_12799" s="T677">v:pred</ta>
            <ta e="T681" id="Seg_12800" s="T680">v:pred 0.1.h:S</ta>
            <ta e="T684" id="Seg_12801" s="T683">v:pred 0.1.h:S</ta>
            <ta e="T687" id="Seg_12802" s="T686">v:pred 0.1.h:S</ta>
            <ta e="T690" id="Seg_12803" s="T689">ptcl.neg</ta>
            <ta e="T691" id="Seg_12804" s="T690">v:pred 0.1.h:S</ta>
            <ta e="T698" id="Seg_12805" s="T697">np.h:S</ta>
            <ta e="T699" id="Seg_12806" s="T698">v:pred</ta>
            <ta e="T700" id="Seg_12807" s="T699">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_12808" s="T701">np:S</ta>
            <ta e="T705" id="Seg_12809" s="T704">np:O</ta>
            <ta e="T708" id="Seg_12810" s="T707">v:pred</ta>
            <ta e="T709" id="Seg_12811" s="T708">np:S</ta>
            <ta e="T711" id="Seg_12812" s="T710">v:pred</ta>
            <ta e="T713" id="Seg_12813" s="T712">pro.h:S</ta>
            <ta e="T714" id="Seg_12814" s="T713">v:pred</ta>
            <ta e="T717" id="Seg_12815" s="T716">v:pred 0.2.h:S</ta>
            <ta e="T719" id="Seg_12816" s="T718">np:O</ta>
            <ta e="T723" id="Seg_12817" s="T722">pro.h:S</ta>
            <ta e="T725" id="Seg_12818" s="T724">np:O</ta>
            <ta e="T726" id="Seg_12819" s="T725">v:pred</ta>
            <ta e="T728" id="Seg_12820" s="T727">pro.h:S</ta>
            <ta e="T730" id="Seg_12821" s="T729">v:pred</ta>
            <ta e="T733" id="Seg_12822" s="T732">v:pred 0.2.h:S</ta>
            <ta e="T734" id="Seg_12823" s="T733">np:O</ta>
            <ta e="T736" id="Seg_12824" s="T735">v:pred 0.2.h:S</ta>
            <ta e="T740" id="Seg_12825" s="T739">pro.h:S</ta>
            <ta e="T743" id="Seg_12826" s="T742">np:O</ta>
            <ta e="T744" id="Seg_12827" s="T743">v:pred</ta>
            <ta e="T746" id="Seg_12828" s="T745">np:S</ta>
            <ta e="T747" id="Seg_12829" s="T746">v:pred</ta>
            <ta e="T752" id="Seg_12830" s="T751">pro.h:S</ta>
            <ta e="T753" id="Seg_12831" s="T752">v:pred</ta>
            <ta e="T757" id="Seg_12832" s="T756">v:pred 0.2.h:S</ta>
            <ta e="T759" id="Seg_12833" s="T758">np:O</ta>
            <ta e="T761" id="Seg_12834" s="T760">v:pred 0.2.h:S</ta>
            <ta e="T764" id="Seg_12835" s="T763">np.h:S</ta>
            <ta e="T765" id="Seg_12836" s="T764">v:pred</ta>
            <ta e="T769" id="Seg_12837" s="T768">np:S</ta>
            <ta e="T772" id="Seg_12838" s="T771">pro.h:S</ta>
            <ta e="T774" id="Seg_12839" s="T773">v:pred</ta>
            <ta e="T778" id="Seg_12840" s="T777">v:pred 0.2.h:S</ta>
            <ta e="T779" id="Seg_12841" s="T778">np:O</ta>
            <ta e="T781" id="Seg_12842" s="T780">v:pred 0.2.h:S</ta>
            <ta e="T783" id="Seg_12843" s="T782">np.h:S</ta>
            <ta e="T784" id="Seg_12844" s="T783">ptcl.neg</ta>
            <ta e="T785" id="Seg_12845" s="T784">v:pred</ta>
            <ta e="T787" id="Seg_12846" s="T786">adj:pred</ta>
            <ta e="T788" id="Seg_12847" s="T787">adj:pred</ta>
            <ta e="T793" id="Seg_12848" s="T792">pro.h:S</ta>
            <ta e="T794" id="Seg_12849" s="T793">v:pred</ta>
            <ta e="T798" id="Seg_12850" s="T797">v:pred 0.2.h:S</ta>
            <ta e="T800" id="Seg_12851" s="T799">np:O</ta>
            <ta e="T802" id="Seg_12852" s="T801">pro.h:S</ta>
            <ta e="T803" id="Seg_12853" s="T802">v:pred</ta>
            <ta e="T805" id="Seg_12854" s="T804">np:S</ta>
            <ta e="T806" id="Seg_12855" s="T805">v:pred</ta>
            <ta e="T808" id="Seg_12856" s="T807">adj:pred</ta>
            <ta e="T810" id="Seg_12857" s="T809">ptcl.neg</ta>
            <ta e="T811" id="Seg_12858" s="T810">adj:pred</ta>
            <ta e="T816" id="Seg_12859" s="T815">v:pred 0.3.h:S</ta>
            <ta e="T820" id="Seg_12860" s="T819">v:pred 0.2.h:S</ta>
            <ta e="T822" id="Seg_12861" s="T821">np:O</ta>
            <ta e="T823" id="Seg_12862" s="T822">v:pred 0.2.h:S</ta>
            <ta e="T827" id="Seg_12863" s="T826">pro.h:S</ta>
            <ta e="T830" id="Seg_12864" s="T829">ptcl.neg</ta>
            <ta e="T831" id="Seg_12865" s="T830">v:pred</ta>
            <ta e="T834" id="Seg_12866" s="T833">adj:pred</ta>
            <ta e="T836" id="Seg_12867" s="T835">np:O</ta>
            <ta e="T842" id="Seg_12868" s="T841">v:pred 0.3.h:S</ta>
            <ta e="T847" id="Seg_12869" s="T846">v:pred 0.2.h:S</ta>
            <ta e="T848" id="Seg_12870" s="T847">np:O</ta>
            <ta e="T849" id="Seg_12871" s="T848">pro.h:S</ta>
            <ta e="T850" id="Seg_12872" s="T849">pro.h:S</ta>
            <ta e="T853" id="Seg_12873" s="T852">v:pred</ta>
            <ta e="T860" id="Seg_12874" s="T859">adj:pred</ta>
            <ta e="T863" id="Seg_12875" s="T862">pro.h:S</ta>
            <ta e="T864" id="Seg_12876" s="T863">np:O</ta>
            <ta e="T866" id="Seg_12877" s="T865">ptcl.neg</ta>
            <ta e="T867" id="Seg_12878" s="T866">v:pred</ta>
            <ta e="T870" id="Seg_12879" s="T869">v:pred</ta>
            <ta e="T871" id="Seg_12880" s="T870">np:S</ta>
            <ta e="T876" id="Seg_12881" s="T875">pro:S</ta>
            <ta e="T877" id="Seg_12882" s="T876">v:pred</ta>
            <ta e="T878" id="Seg_12883" s="T877">pro:O</ta>
            <ta e="T879" id="Seg_12884" s="T878">ptcl.neg</ta>
            <ta e="T880" id="Seg_12885" s="T879">v:pred 0.2.h:S</ta>
            <ta e="T883" id="Seg_12886" s="T882">v:pred 0.2.h:S</ta>
            <ta e="T885" id="Seg_12887" s="T884">v:pred</ta>
            <ta e="T886" id="Seg_12888" s="T885">np:S</ta>
            <ta e="T887" id="Seg_12889" s="T886">v:pred 0.2.h:S</ta>
            <ta e="T890" id="Seg_12890" s="T889">v:pred 0.2.h:S</ta>
            <ta e="T895" id="Seg_12891" s="T894">np.h:S</ta>
            <ta e="T898" id="Seg_12892" s="T897">v:pred</ta>
            <ta e="T908" id="Seg_12893" s="T907">np.h:S</ta>
            <ta e="T912" id="Seg_12894" s="T911">v:pred</ta>
            <ta e="T914" id="Seg_12895" s="T913">pro:O</ta>
            <ta e="T915" id="Seg_12896" s="T914">v:pred</ta>
            <ta e="T921" id="Seg_12897" s="T920">pro:O</ta>
            <ta e="T922" id="Seg_12898" s="T921">v:pred</ta>
            <ta e="T923" id="Seg_12899" s="T922">v:pred 0.3.h:S</ta>
            <ta e="T931" id="Seg_12900" s="T930">np:S</ta>
            <ta e="T932" id="Seg_12901" s="T931">v:pred</ta>
            <ta e="T940" id="Seg_12902" s="T939">np.h:S</ta>
            <ta e="T941" id="Seg_12903" s="T940">v:pred</ta>
            <ta e="T945" id="Seg_12904" s="T944">v:pred 0.1.h:S</ta>
            <ta e="T948" id="Seg_12905" s="T947">v:pred 0.3:S</ta>
            <ta e="T953" id="Seg_12906" s="T952">pro.h:S</ta>
            <ta e="T955" id="Seg_12907" s="T954">v:pred</ta>
            <ta e="T956" id="Seg_12908" s="T955">np:O</ta>
            <ta e="T957" id="Seg_12909" s="T956">v:pred</ta>
            <ta e="T960" id="Seg_12910" s="T959">np:S</ta>
            <ta e="T961" id="Seg_12911" s="T960">v:pred</ta>
            <ta e="T966" id="Seg_12912" s="T965">np:O</ta>
            <ta e="T967" id="Seg_12913" s="T966">v:pred 0.1.h:S</ta>
            <ta e="T975" id="Seg_12914" s="T974">v:pred 0.1.h:S</ta>
            <ta e="T977" id="Seg_12915" s="T976">v:pred 0.1.h:S</ta>
            <ta e="T980" id="Seg_12916" s="T979">v:pred 0.1.h:S</ta>
            <ta e="T981" id="Seg_12917" s="T980">np:O</ta>
            <ta e="T982" id="Seg_12918" s="T981">v:pred 0.1.h:S</ta>
            <ta e="T985" id="Seg_12919" s="T984">v:pred 0.1.h:S</ta>
            <ta e="T986" id="Seg_12920" s="T985">np:O</ta>
            <ta e="T987" id="Seg_12921" s="T986">v:pred 0.1.h:S</ta>
            <ta e="T990" id="Seg_12922" s="T989">v:pred 0.1.h:S</ta>
            <ta e="T991" id="Seg_12923" s="T990">np:O</ta>
            <ta e="T992" id="Seg_12924" s="T991">v:pred 0.1.h:S</ta>
            <ta e="T995" id="Seg_12925" s="T994">v:pred 0.3.h:S</ta>
            <ta e="T996" id="Seg_12926" s="T995">np:O</ta>
            <ta e="T998" id="Seg_12927" s="T997">v:pred 0.1.h:S</ta>
            <ta e="T1001" id="Seg_12928" s="T1000">v:pred 0.3.h:S</ta>
            <ta e="T1002" id="Seg_12929" s="T1001">np:O</ta>
            <ta e="T1003" id="Seg_12930" s="T1002">v:pred 0.1.h:S</ta>
            <ta e="T1011" id="Seg_12931" s="T1010">np:S</ta>
            <ta e="T1012" id="Seg_12932" s="T1011">adj:pred</ta>
            <ta e="T1013" id="Seg_12933" s="T1012">pro:O</ta>
            <ta e="T1016" id="Seg_12934" s="T1015">v:pred 0.3.h:S</ta>
            <ta e="T1019" id="Seg_12935" s="T1018">np:S</ta>
            <ta e="T1020" id="Seg_12936" s="T1019">v:pred</ta>
            <ta e="T1022" id="Seg_12937" s="T1021">v:pred 0.2.h:S</ta>
            <ta e="T1028" id="Seg_12938" s="T1027">np:S</ta>
            <ta e="T1029" id="Seg_12939" s="T1028">np:S</ta>
            <ta e="T1030" id="Seg_12940" s="T1029">adj:pred</ta>
            <ta e="T1031" id="Seg_12941" s="T1030">np:S</ta>
            <ta e="T1032" id="Seg_12942" s="T1031">adj:pred</ta>
            <ta e="T1034" id="Seg_12943" s="T1033">pro.h:S</ta>
            <ta e="T1035" id="Seg_12944" s="T1034">v:pred</ta>
            <ta e="T1039" id="Seg_12945" s="T1038">pro.h:S</ta>
            <ta e="T1040" id="Seg_12946" s="T1039">np:O</ta>
            <ta e="T1042" id="Seg_12947" s="T1041">v:pred</ta>
            <ta e="T1181" id="Seg_12948" s="T1043">conv:pred</ta>
            <ta e="T1044" id="Seg_12949" s="T1181">v:pred 0.3.h:S</ta>
            <ta e="T1045" id="Seg_12950" s="T1044">pro.h:S</ta>
            <ta e="T1046" id="Seg_12951" s="T1045">pro:O</ta>
            <ta e="T1047" id="Seg_12952" s="T1046">ptcl.neg</ta>
            <ta e="T1048" id="Seg_12953" s="T1047">v:pred</ta>
            <ta e="T1051" id="Seg_12954" s="T1050">ptcl.neg</ta>
            <ta e="T1052" id="Seg_12955" s="T1051">v:pred 0.3.h:S</ta>
            <ta e="T1056" id="Seg_12956" s="T1055">pro.h:S</ta>
            <ta e="T1058" id="Seg_12957" s="T1057">v:pred</ta>
            <ta e="T1063" id="Seg_12958" s="T1062">adj:pred</ta>
            <ta e="T1064" id="Seg_12959" s="T1063">v:pred 0.1.h:S</ta>
            <ta e="T1065" id="Seg_12960" s="T1064">np:S</ta>
            <ta e="T1068" id="Seg_12961" s="T1067">v:pred</ta>
            <ta e="T1073" id="Seg_12962" s="T1072">v:pred 0.1.h:S</ta>
            <ta e="T1075" id="Seg_12963" s="T1074">v:pred 0.1.h:S</ta>
            <ta e="T1077" id="Seg_12964" s="T1076">v:pred 0.1.h:S</ta>
            <ta e="T1081" id="Seg_12965" s="T1080">np:S</ta>
            <ta e="T1082" id="Seg_12966" s="T1081">v:pred</ta>
            <ta e="T1087" id="Seg_12967" s="T1086">np:O</ta>
            <ta e="T1088" id="Seg_12968" s="T1087">v:pred 0.1.h:S</ta>
            <ta e="T1092" id="Seg_12969" s="T1091">v:pred 0.1.h:S</ta>
            <ta e="T1094" id="Seg_12970" s="T1093">np:S</ta>
            <ta e="T1095" id="Seg_12971" s="T1094">ptcl.neg</ta>
            <ta e="T1096" id="Seg_12972" s="T1095">v:pred</ta>
            <ta e="T1099" id="Seg_12973" s="T1098">np:O</ta>
            <ta e="T1103" id="Seg_12974" s="T1102">v:pred 0.1.h:S</ta>
            <ta e="T1105" id="Seg_12975" s="T1104">np:S</ta>
            <ta e="T1106" id="Seg_12976" s="T1105">ptcl.neg</ta>
            <ta e="T1107" id="Seg_12977" s="T1106">v:pred</ta>
            <ta e="T1116" id="Seg_12978" s="T1115">v:pred</ta>
            <ta e="T1117" id="Seg_12979" s="T1116">np:S</ta>
            <ta e="T1118" id="Seg_12980" s="T1117">v:pred 0.1.h:S</ta>
            <ta e="T1119" id="Seg_12981" s="T1118">np:O</ta>
            <ta e="T1123" id="Seg_12982" s="T1122">v:pred</ta>
            <ta e="T1129" id="Seg_12983" s="T1128">v:pred 0.2.h:S</ta>
            <ta e="T1130" id="Seg_12984" s="T1129">np:O</ta>
            <ta e="T1135" id="Seg_12985" s="T1134">v:pred 0.3.h:S</ta>
            <ta e="T1138" id="Seg_12986" s="T1137">np:S</ta>
            <ta e="T1139" id="Seg_12987" s="T1138">adj:pred</ta>
            <ta e="T1142" id="Seg_12988" s="T1141">np:S</ta>
            <ta e="T1143" id="Seg_12989" s="T1142">v:pred</ta>
            <ta e="T1145" id="Seg_12990" s="T1144">np:S</ta>
            <ta e="T1146" id="Seg_12991" s="T1145">v:pred</ta>
            <ta e="T1154" id="Seg_12992" s="T1153">np:S</ta>
            <ta e="T1155" id="Seg_12993" s="T1154">ptcl.neg</ta>
            <ta e="T1156" id="Seg_12994" s="T1155">adj:pred</ta>
            <ta e="T1159" id="Seg_12995" s="T1158">adj:pred 0.3:S</ta>
            <ta e="T1163" id="Seg_12996" s="T1162">pro:S</ta>
            <ta e="T1164" id="Seg_12997" s="T1163">n:pred</ta>
            <ta e="T1168" id="Seg_12998" s="T1167">np:O</ta>
            <ta e="T1172" id="Seg_12999" s="T1171">ptcl:pred</ta>
            <ta e="T1174" id="Seg_13000" s="T1173">v:pred 0.2.h:S</ta>
            <ta e="T1175" id="Seg_13001" s="T1174">ptcl.neg</ta>
            <ta e="T1176" id="Seg_13002" s="T1175">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_13003" s="T4">TURK:core</ta>
            <ta e="T19" id="Seg_13004" s="T18">RUS:gram</ta>
            <ta e="T25" id="Seg_13005" s="T24">RUS:cult</ta>
            <ta e="T29" id="Seg_13006" s="T28">RUS:cult</ta>
            <ta e="T30" id="Seg_13007" s="T29">RUS:cult</ta>
            <ta e="T41" id="Seg_13008" s="T40">TURK:cult</ta>
            <ta e="T49" id="Seg_13009" s="T48">RUS:gram</ta>
            <ta e="T70" id="Seg_13010" s="T69">TURK:disc</ta>
            <ta e="T73" id="Seg_13011" s="T72">TURK:core</ta>
            <ta e="T75" id="Seg_13012" s="T74">RUS:gram</ta>
            <ta e="T80" id="Seg_13013" s="T79">RUS:disc</ta>
            <ta e="T91" id="Seg_13014" s="T90">RUS:mod</ta>
            <ta e="T101" id="Seg_13015" s="T100">%TURK:core</ta>
            <ta e="T102" id="Seg_13016" s="T101">RUS:mod</ta>
            <ta e="T104" id="Seg_13017" s="T103">RUS:gram</ta>
            <ta e="T117" id="Seg_13018" s="T116">RUS:gram</ta>
            <ta e="T122" id="Seg_13019" s="T121">RUS:gram</ta>
            <ta e="T126" id="Seg_13020" s="T125">RUS:gram</ta>
            <ta e="T129" id="Seg_13021" s="T128">RUS:gram</ta>
            <ta e="T133" id="Seg_13022" s="T132">RUS:gram</ta>
            <ta e="T135" id="Seg_13023" s="T134">RUS:gram</ta>
            <ta e="T146" id="Seg_13024" s="T145">RUS:gram</ta>
            <ta e="T154" id="Seg_13025" s="T1183">TAT:cult</ta>
            <ta e="T157" id="Seg_13026" s="T156">RUS:gram</ta>
            <ta e="T169" id="Seg_13027" s="T168">RUS:gram</ta>
            <ta e="T174" id="Seg_13028" s="T173">RUS:gram</ta>
            <ta e="T181" id="Seg_13029" s="T180">TURK:disc</ta>
            <ta e="T192" id="Seg_13030" s="T191">RUS:gram</ta>
            <ta e="T195" id="Seg_13031" s="T194">RUS:gram</ta>
            <ta e="T198" id="Seg_13032" s="T197">RUS:gram</ta>
            <ta e="T201" id="Seg_13033" s="T200">RUS:disc</ta>
            <ta e="T205" id="Seg_13034" s="T204">RUS:gram</ta>
            <ta e="T207" id="Seg_13035" s="T206">RUS:gram</ta>
            <ta e="T210" id="Seg_13036" s="T209">RUS:gram</ta>
            <ta e="T215" id="Seg_13037" s="T214">RUS:gram</ta>
            <ta e="T222" id="Seg_13038" s="T221">RUS:gram</ta>
            <ta e="T243" id="Seg_13039" s="T242">TURK:disc</ta>
            <ta e="T251" id="Seg_13040" s="T250">RUS:gram</ta>
            <ta e="T254" id="Seg_13041" s="T252">TURK:mod(PTCL)</ta>
            <ta e="T256" id="Seg_13042" s="T254">TURK:mod(PTCL)</ta>
            <ta e="T266" id="Seg_13043" s="T265">RUS:cult</ta>
            <ta e="T268" id="Seg_13044" s="T267">RUS:gram</ta>
            <ta e="T273" id="Seg_13045" s="T272">RUS:gram</ta>
            <ta e="T274" id="Seg_13046" s="T273">TURK:core</ta>
            <ta e="T277" id="Seg_13047" s="T276">RUS:cult</ta>
            <ta e="T280" id="Seg_13048" s="T279">RUS:cult</ta>
            <ta e="T282" id="Seg_13049" s="T281">RUS:disc</ta>
            <ta e="T287" id="Seg_13050" s="T286">RUS:gram</ta>
            <ta e="T290" id="Seg_13051" s="T289">RUS:gram</ta>
            <ta e="T291" id="Seg_13052" s="T290">TURK:core</ta>
            <ta e="T294" id="Seg_13053" s="T293">RUS:disc</ta>
            <ta e="T307" id="Seg_13054" s="T306">RUS:mod</ta>
            <ta e="T328" id="Seg_13055" s="T327">TURK:core</ta>
            <ta e="T338" id="Seg_13056" s="T337">RUS:mod</ta>
            <ta e="T347" id="Seg_13057" s="T346">RUS:core</ta>
            <ta e="T355" id="Seg_13058" s="T354">RUS:core</ta>
            <ta e="T366" id="Seg_13059" s="T365">RUS:core</ta>
            <ta e="T369" id="Seg_13060" s="T368">TURK:cult</ta>
            <ta e="T370" id="Seg_13061" s="T369">RUS:gram</ta>
            <ta e="T379" id="Seg_13062" s="T378">TURK:core</ta>
            <ta e="T387" id="Seg_13063" s="T386">TAT:cult</ta>
            <ta e="T393" id="Seg_13064" s="T392">RUS:gram</ta>
            <ta e="T398" id="Seg_13065" s="T397">RUS:gram</ta>
            <ta e="T401" id="Seg_13066" s="T400">TAT:cult</ta>
            <ta e="T413" id="Seg_13067" s="T412">RUS:gram</ta>
            <ta e="T416" id="Seg_13068" s="T415">RUS:mod</ta>
            <ta e="T430" id="Seg_13069" s="T429">TURK:cult</ta>
            <ta e="T431" id="Seg_13070" s="T430">TURK:gram(INDEF)</ta>
            <ta e="T433" id="Seg_13071" s="T432">RUS:gram</ta>
            <ta e="T437" id="Seg_13072" s="T436">RUS:gram</ta>
            <ta e="T440" id="Seg_13073" s="T439">TURK:gram(INDEF)</ta>
            <ta e="T444" id="Seg_13074" s="T443">RUS:cult</ta>
            <ta e="T445" id="Seg_13075" s="T444">TURK:disc</ta>
            <ta e="T460" id="Seg_13076" s="T459">RUS:gram</ta>
            <ta e="T461" id="Seg_13077" s="T460">RUS:gram</ta>
            <ta e="T472" id="Seg_13078" s="T471">RUS:gram</ta>
            <ta e="T478" id="Seg_13079" s="T477">TURK:cult</ta>
            <ta e="T482" id="Seg_13080" s="T481">TURK:disc</ta>
            <ta e="T486" id="Seg_13081" s="T485">TURK:disc</ta>
            <ta e="T491" id="Seg_13082" s="T490">TURK:disc</ta>
            <ta e="T509" id="Seg_13083" s="T508">RUS:cult</ta>
            <ta e="T510" id="Seg_13084" s="T509">RUS:mod</ta>
            <ta e="T520" id="Seg_13085" s="T519">RUS:mod</ta>
            <ta e="T522" id="Seg_13086" s="T521">RUS:gram</ta>
            <ta e="T523" id="Seg_13087" s="T522">RUS:core</ta>
            <ta e="T524" id="Seg_13088" s="T523">RUS:cult</ta>
            <ta e="T535" id="Seg_13089" s="T534">TURK:core</ta>
            <ta e="T536" id="Seg_13090" s="T535">RUS:mod</ta>
            <ta e="T542" id="Seg_13091" s="T541">RUS:gram</ta>
            <ta e="T567" id="Seg_13092" s="T566">RUS:cult</ta>
            <ta e="T580" id="Seg_13093" s="T579">RUS:cult</ta>
            <ta e="T589" id="Seg_13094" s="T588">RUS:core</ta>
            <ta e="T591" id="Seg_13095" s="T590">RUS:gram</ta>
            <ta e="T596" id="Seg_13096" s="T595">RUS:cult</ta>
            <ta e="T598" id="Seg_13097" s="T597">RUS:gram</ta>
            <ta e="T600" id="Seg_13098" s="T599">RUS:cult</ta>
            <ta e="T606" id="Seg_13099" s="T605">TAT:cult</ta>
            <ta e="T616" id="Seg_13100" s="T615">TURK:cult</ta>
            <ta e="T623" id="Seg_13101" s="T622">RUS:core</ta>
            <ta e="T629" id="Seg_13102" s="T628">RUS:gram</ta>
            <ta e="T630" id="Seg_13103" s="T629">%TURK:core</ta>
            <ta e="T632" id="Seg_13104" s="T631">RUS:gram</ta>
            <ta e="T639" id="Seg_13105" s="T638">RUS:core</ta>
            <ta e="T641" id="Seg_13106" s="T640">RUS:cult</ta>
            <ta e="T646" id="Seg_13107" s="T1184">TAT:cult</ta>
            <ta e="T653" id="Seg_13108" s="T652">TURK:cult</ta>
            <ta e="T656" id="Seg_13109" s="T655">RUS:mod</ta>
            <ta e="T660" id="Seg_13110" s="T659">RUS:gram</ta>
            <ta e="T668" id="Seg_13111" s="T667">RUS:gram(INDEF)</ta>
            <ta e="T675" id="Seg_13112" s="T674">RUS:cult</ta>
            <ta e="T677" id="Seg_13113" s="T676">TURK:disc</ta>
            <ta e="T682" id="Seg_13114" s="T681">RUS:gram</ta>
            <ta e="T710" id="Seg_13115" s="T709">TURK:disc</ta>
            <ta e="T745" id="Seg_13116" s="T744">RUS:gram</ta>
            <ta e="T758" id="Seg_13117" s="T757">TURK:disc</ta>
            <ta e="T779" id="Seg_13118" s="T778">RUS:core</ta>
            <ta e="T800" id="Seg_13119" s="T799">RUS:core</ta>
            <ta e="T801" id="Seg_13120" s="T800">RUS:gram</ta>
            <ta e="T807" id="Seg_13121" s="T806">RUS:gram</ta>
            <ta e="T811" id="Seg_13122" s="T810">TURK:core</ta>
            <ta e="T822" id="Seg_13123" s="T821">RUS:core</ta>
            <ta e="T825" id="Seg_13124" s="T824">TURK:disc</ta>
            <ta e="T836" id="Seg_13125" s="T835">RUS:core</ta>
            <ta e="T854" id="Seg_13126" s="T853">RUS:cult</ta>
            <ta e="T859" id="Seg_13127" s="T858">TAT:cult</ta>
            <ta e="T860" id="Seg_13128" s="T859">TURK:core</ta>
            <ta e="T863" id="Seg_13129" s="T862">RUS:mod</ta>
            <ta e="T864" id="Seg_13130" s="T863">TURK:cult</ta>
            <ta e="T865" id="Seg_13131" s="T864">TURK:gram(INDEF)</ta>
            <ta e="T871" id="Seg_13132" s="T870">TURK:cult</ta>
            <ta e="T875" id="Seg_13133" s="T874">RUS:cult</ta>
            <ta e="T878" id="Seg_13134" s="T877">TURK:gram(INDEF)</ta>
            <ta e="T882" id="Seg_13135" s="T881">TURK:disc</ta>
            <ta e="T886" id="Seg_13136" s="T885">RUS:cult</ta>
            <ta e="T888" id="Seg_13137" s="T887">RUS:gram</ta>
            <ta e="T897" id="Seg_13138" s="T896">TURK:core</ta>
            <ta e="T899" id="Seg_13139" s="T898">TURK:disc</ta>
            <ta e="T902" id="Seg_13140" s="T901">TURK:core</ta>
            <ta e="T913" id="Seg_13141" s="T912">TURK:disc</ta>
            <ta e="T920" id="Seg_13142" s="T919">TURK:disc</ta>
            <ta e="T930" id="Seg_13143" s="T929">TURK:core</ta>
            <ta e="T933" id="Seg_13144" s="T932">RUS:mod</ta>
            <ta e="T939" id="Seg_13145" s="T938">RUS:gram</ta>
            <ta e="T940" id="Seg_13146" s="T939">RUS:core</ta>
            <ta e="T946" id="Seg_13147" s="T945">TURK:disc</ta>
            <ta e="T958" id="Seg_13148" s="T957">RUS:gram</ta>
            <ta e="T966" id="Seg_13149" s="T965">RUS:cult</ta>
            <ta e="T968" id="Seg_13150" s="T967">RUS:gram</ta>
            <ta e="T984" id="Seg_13151" s="T983">RUS:core</ta>
            <ta e="T986" id="Seg_13152" s="T985">RUS:core</ta>
            <ta e="T1000" id="Seg_13153" s="T999">TAT:cult</ta>
            <ta e="T1002" id="Seg_13154" s="T1001">TAT:cult</ta>
            <ta e="T1009" id="Seg_13155" s="T1008">TAT:cult</ta>
            <ta e="T1014" id="Seg_13156" s="T1013">RUS:gram(INDEF)</ta>
            <ta e="T1015" id="Seg_13157" s="T1014">TURK:disc</ta>
            <ta e="T1018" id="Seg_13158" s="T1017">RUS:gram</ta>
            <ta e="T1038" id="Seg_13159" s="T1037">RUS:gram</ta>
            <ta e="T1043" id="Seg_13160" s="T1042">RUS:gram</ta>
            <ta e="T1060" id="Seg_13161" s="T1059">RUS:gram</ta>
            <ta e="T1074" id="Seg_13162" s="T1073">RUS:core</ta>
            <ta e="T1078" id="Seg_13163" s="T1077">TURK:disc</ta>
            <ta e="T1081" id="Seg_13164" s="T1080">RUS:core</ta>
            <ta e="T1093" id="Seg_13165" s="T1092">RUS:gram</ta>
            <ta e="T1098" id="Seg_13166" s="T1097">RUS:gram</ta>
            <ta e="T1100" id="Seg_13167" s="T1099">RUS:cult</ta>
            <ta e="T1101" id="Seg_13168" s="T1100">RUS:mod</ta>
            <ta e="T1104" id="Seg_13169" s="T1103">RUS:gram</ta>
            <ta e="T1111" id="Seg_13170" s="T1110">RUS:gram</ta>
            <ta e="T1113" id="Seg_13171" s="T1112">RUS:gram</ta>
            <ta e="T1125" id="Seg_13172" s="T1124">RUS:gram</ta>
            <ta e="T1128" id="Seg_13173" s="T1127">RUS:disc</ta>
            <ta e="T1150" id="Seg_13174" s="T1149">TURK:disc</ta>
            <ta e="T1157" id="Seg_13175" s="T1156">RUS:gram</ta>
            <ta e="T1164" id="Seg_13176" s="T1163">RUS:core</ta>
            <ta e="T1165" id="Seg_13177" s="T1164">RUS:gram</ta>
            <ta e="T1168" id="Seg_13178" s="T1167">RUS:core</ta>
            <ta e="T1172" id="Seg_13179" s="T1171">RUS:mod</ta>
            <ta e="T1176" id="Seg_13180" s="T1175">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T237" id="Seg_13181" s="T230">RUS:ext</ta>
            <ta e="T499" id="Seg_13182" s="T496">RUS:ext</ta>
            <ta e="T695" id="Seg_13183" s="T694">RUS:ext</ta>
            <ta e="T747" id="Seg_13184" s="T744">RUS:calq</ta>
            <ta e="T916" id="Seg_13185" s="T915">RUS:int</ta>
            <ta e="T1096" id="Seg_13186" s="T1092">RUS:calq</ta>
            <ta e="T1107" id="Seg_13187" s="T1103">RUS:calq</ta>
            <ta e="T1112" id="Seg_13188" s="T1111">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_13189" s="T1">Вчера я ходила [к] родственникам.</ta>
            <ta e="T14" id="Seg_13190" s="T6">Я сказала: "Не сердитесь, мне нельзя было (шить?).</ta>
            <ta e="T22" id="Seg_13191" s="T15">Моя невестка не пошла, и я не пошла".</ta>
            <ta e="T27" id="Seg_13192" s="T23">Они на кровати лежат, обнимаются.</ta>
            <ta e="T30" id="Seg_13193" s="T28">Коля с Дуней.</ta>
            <ta e="T32" id="Seg_13194" s="T31">Целуются.</ta>
            <ta e="T34" id="Seg_13195" s="T33">Смеются.</ta>
            <ta e="T39" id="Seg_13196" s="T35">Говорят: "Бабушка, садись, поешь.</ta>
            <ta e="T42" id="Seg_13197" s="T40">Выпей водки".</ta>
            <ta e="T47" id="Seg_13198" s="T43">Я говорю: "Я не буду пить".</ta>
            <ta e="T52" id="Seg_13199" s="T48">И потом пошла домой.</ta>
            <ta e="T61" id="Seg_13200" s="T55">Я сегодня ходила к женщине, она болеет.</ta>
            <ta e="T71" id="Seg_13201" s="T62">Она говорит: "Почему [ты] долго не приходила, я умираю".</ta>
            <ta e="T78" id="Seg_13202" s="T71">"Ты всё время умираешь, а умереть не можешь.</ta>
            <ta e="T81" id="Seg_13203" s="T79">Ну, умирай.</ta>
            <ta e="T91" id="Seg_13204" s="T82">Мне что, разве я буду около тебя сидеть, что ли?</ta>
            <ta e="T95" id="Seg_13205" s="T92">(У меня?) ведь люди живут (/сидят).</ta>
            <ta e="T99" id="Seg_13206" s="T96">Они меня там ждут.</ta>
            <ta e="T102" id="Seg_13207" s="T100">[Мне] надо говорить.</ta>
            <ta e="T107" id="Seg_13208" s="T103">Я не буду рядом с тобой сидеть!" [?]</ta>
            <ta e="T115" id="Seg_13209" s="T110">"Моя невестка была [здесь?] два дня.</ta>
            <ta e="T120" id="Seg_13210" s="T116">Я не буду рядом с тобой сидеть!" [?]</ta>
            <ta e="T124" id="Seg_13211" s="T121">Да и люди ходят [ко мне]."</ta>
            <ta e="T131" id="Seg_13212" s="T125">А она говорит: "И Бог с ними".</ta>
            <ta e="T136" id="Seg_13213" s="T132">"Да пускай [лучше] Бог с тобой!</ta>
            <ta e="T139" id="Seg_13214" s="T137">Помирай!"</ta>
            <ta e="T149" id="Seg_13215" s="T142">Потом она осталась, а я пошла домой.</ta>
            <ta e="T155" id="Seg_13216" s="T152">Я пошла в Агинское.</ta>
            <ta e="T165" id="Seg_13217" s="T156">А [пока меня не было?] та женщина пришла, которая рядом с ней живёт. [?]</ta>
            <ta e="T170" id="Seg_13218" s="T166">Пришла и ушла.</ta>
            <ta e="T178" id="Seg_13219" s="T171">"Я умру, а кто меня обмоет?"</ta>
            <ta e="T183" id="Seg_13220" s="T179">Они смеялись сильно.</ta>
            <ta e="T189" id="Seg_13221" s="T184">Потом я вернулась и к ней пошла.</ta>
            <ta e="T196" id="Seg_13222" s="T189">"Ты ушла, а я умирала и замерзала".</ta>
            <ta e="T202" id="Seg_13223" s="T197">Я говорю: "Ну, замерзай.</ta>
            <ta e="T208" id="Seg_13224" s="T203">И умирай.</ta>
            <ta e="T213" id="Seg_13225" s="T208">Я согрею воды.</ta>
            <ta e="T218" id="Seg_13226" s="T214">И ты на себя польёшь. [?]</ta>
            <ta e="T227" id="Seg_13227" s="T219">Потом я [тебя] обмою и положу в землю.</ta>
            <ta e="T237" id="Seg_13228" s="T230">Как я говорила, погоди, как я начала? </ta>
            <ta e="T248" id="Seg_13229" s="T240">Эта женщина [говорит] мне: «Затопи баню, помой меня.</ta>
            <ta e="T257" id="Seg_13230" s="T249">Когда умру, помой мне лицо и руки".</ta>
            <ta e="T263" id="Seg_13231" s="T258">Тогда я затопила баню.</ta>
            <ta e="T270" id="Seg_13232" s="T264">Потом положила [её] на санки и отвезла.</ta>
            <ta e="T277" id="Seg_13233" s="T270">Помыла её и снова домой привезла на санках.</ta>
            <ta e="T284" id="Seg_13234" s="T278">Положила её на кровать и говорю: "Ну, теперь умирай.</ta>
            <ta e="T292" id="Seg_13235" s="T285">Лицо и руки я [тебе] помою, и всё будет хорошо".</ta>
            <ta e="T303" id="Seg_13236" s="T293">Ну, потом сама в бане помылась, очень много грязи было на голове.</ta>
            <ta e="T309" id="Seg_13237" s="T304">Я говорю: "Принеси ещё воды.</ta>
            <ta e="T313" id="Seg_13238" s="T310">Налей сюда".</ta>
            <ta e="T322" id="Seg_13239" s="T316">Я отдала одну лошадь брату мужа.</ta>
            <ta e="T326" id="Seg_13240" s="T323">Ей было два года.</ta>
            <ta e="T331" id="Seg_13241" s="T327">Ещё одну лошадь отдала кому-то. [?]</ta>
            <ta e="T334" id="Seg_13242" s="T332">Это был мужчина.</ta>
            <ta e="T337" id="Seg_13243" s="T335">Лошади [у него] не было.</ta>
            <ta e="T341" id="Seg_13244" s="T337">[Этой] тоже два года было.</ta>
            <ta e="T344" id="Seg_13245" s="T342">Отдала лошадь.</ta>
            <ta e="T349" id="Seg_13246" s="T344">Потом я отдала [её?] сестре овцу.</ta>
            <ta e="T352" id="Seg_13247" s="T350">Пальто отдала.</ta>
            <ta e="T358" id="Seg_13248" s="T353">Потом племяннице бычка отдал.</ta>
            <ta e="T362" id="Seg_13249" s="T359">Полгода [ему было].</ta>
            <ta e="T372" id="Seg_13250" s="T363">Потом (сыну сестры?) отдала корову и овцу.</ta>
            <ta e="T382" id="Seg_13251" s="T375">Потом одной женщине ещё телёнка отдала.</ta>
            <ta e="T391" id="Seg_13252" s="T383">Люди придут: "Дай денег, мы тебе отдадим".</ta>
            <ta e="T397" id="Seg_13253" s="T392">А сами не отдадут.</ta>
            <ta e="T401" id="Seg_13254" s="T397">И тогда (?) деньги.</ta>
            <ta e="T417" id="Seg_13255" s="T406">Я теперь не думаю, иначе мне голову надо было бы большую, как у лошади. [?]</ta>
            <ta e="T422" id="Seg_13256" s="T420">Ты думаешь!</ta>
            <ta e="T434" id="Seg_13257" s="T427">Скоро я пойду, коров загоню, подою и покормлю. [?]</ta>
            <ta e="T440" id="Seg_13258" s="T435">Я пришла, а они убежали куда-то.</ta>
            <ta e="T447" id="Seg_13259" s="T443">Трактор брёвна везёт.</ta>
            <ta e="T452" id="Seg_13260" s="T448">Я не знаю, сколько он везёт.</ta>
            <ta e="T458" id="Seg_13261" s="T455">Очень много брёвен.</ta>
            <ta e="T464" id="Seg_13262" s="T459">И очень толстых.</ta>
            <ta e="T474" id="Seg_13263" s="T467">[?]</ta>
            <ta e="T480" id="Seg_13264" s="T477">Начальник очень красивый.</ta>
            <ta e="T487" id="Seg_13265" s="T481">Всего мне (дал?), всего.</ta>
            <ta e="T494" id="Seg_13266" s="T490">Всё мне показал.</ta>
            <ta e="T499" id="Seg_13267" s="T496">Это мало чего-то. </ta>
            <ta e="T504" id="Seg_13268" s="T500">Будет день, будет пища.</ta>
            <ta e="T514" id="Seg_13269" s="T507">Сегодня суббота, надо баню топить, воду носить.</ta>
            <ta e="T520" id="Seg_13270" s="T515">Потом натоплю, надо помыться.</ta>
            <ta e="T526" id="Seg_13271" s="T521">А то я целую неделю не мылась.</ta>
            <ta e="T533" id="Seg_13272" s="T529">Я дома письма ждала, ждала.</ta>
            <ta e="T537" id="Seg_13273" s="T534">Всё ещё нет.</ta>
            <ta e="T540" id="Seg_13274" s="T537">Моя (женщина?) не пишет.</ta>
            <ta e="T545" id="Seg_13275" s="T541">И письмо не приходит.</ta>
            <ta e="T551" id="Seg_13276" s="T547">Сегодня ночью я во сне видела.</ta>
            <ta e="T556" id="Seg_13277" s="T552">Я (ругалась?), сегодня письмо придёт.</ta>
            <ta e="T564" id="Seg_13278" s="T559">Я поехала в Красноярск, яйца повезла.</ta>
            <ta e="T568" id="Seg_13279" s="T565">Сто яиц.</ta>
            <ta e="T573" id="Seg_13280" s="T569">Потом брат мой со мной пошёл.</ta>
            <ta e="T580" id="Seg_13281" s="T574">Он меня привёл, (посадил?) в машину.</ta>
            <ta e="T589" id="Seg_13282" s="T581">Я не могла найти, потом увидела племянницу.</ta>
            <ta e="T596" id="Seg_13283" s="T590">А туда пришла, отсюда на машине.</ta>
            <ta e="T600" id="Seg_13284" s="T597">А там на автобусе.</ta>
            <ta e="T610" id="Seg_13285" s="T601">Потом по железной дороге, потом туда приехала.</ta>
            <ta e="T619" id="Seg_13286" s="T613">Потом пошла Богу помолиться.</ta>
            <ta e="T624" id="Seg_13287" s="T620">Потом мы с племянницей пошли (к Джибьевым?).</ta>
            <ta e="T630" id="Seg_13288" s="T625">Мы там сидели, разговаривали.</ta>
            <ta e="T635" id="Seg_13289" s="T631">А потом я домой пришла.</ta>
            <ta e="T643" id="Seg_13290" s="T638">Племянница дала мне много помидоров.</ta>
            <ta e="T649" id="Seg_13291" s="T643">Я в Агинском два дня провела.</ta>
            <ta e="T662" id="Seg_13292" s="T652">Коровы пришли домой, надо их загнать, подоить и накормить.</ta>
            <ta e="T669" id="Seg_13293" s="T665">Я думала, думала и кое-что придумала.</ta>
            <ta e="T678" id="Seg_13294" s="T672">Я пошла к дочке, сумка порвалась.</ta>
            <ta e="T684" id="Seg_13295" s="T679">Теперь сижу и зашиваю.</ta>
            <ta e="T691" id="Seg_13296" s="T685">Собиралась к тебе пойти, (чуть?) не упала.</ta>
            <ta e="T696" id="Seg_13297" s="T695">[KA:] Заяц.</ta>
            <ta e="T700" id="Seg_13298" s="T697">[PKZ:] Заяц идёт, идёт.</ta>
            <ta e="T708" id="Seg_13299" s="T700">Потом травинка ему нос порезала.</ta>
            <ta e="T711" id="Seg_13300" s="T708">Кровь течёт.</ta>
            <ta e="T715" id="Seg_13301" s="T712">Он говорит: "Огонь!</ta>
            <ta e="T719" id="Seg_13302" s="T716">Сожги эту траву.</ta>
            <ta e="T726" id="Seg_13303" s="T722">"У меня много земли, чтобы жечь".</ta>
            <ta e="T730" id="Seg_13304" s="T726">Тогда он пошёл к воде.</ta>
            <ta e="T736" id="Seg_13305" s="T731">"Вода, иди, залей огонь!"</ta>
            <ta e="T747" id="Seg_13306" s="T739">"У меня много земли, чтобы заливать, чтобы трава росла".</ta>
            <ta e="T753" id="Seg_13307" s="T750">Потом он пошёл.</ta>
            <ta e="T761" id="Seg_13308" s="T754">"Лось, лось, пойди, выпей всю воду!"</ta>
            <ta e="T769" id="Seg_13309" s="T762">Лось сказал: "У меня под ногами много воды".</ta>
            <ta e="T775" id="Seg_13310" s="T770">Тогда он пошёл к женщине.</ta>
            <ta e="T781" id="Seg_13311" s="T775">"Женщина, пойди, вынь у него жилы из ног!"</ta>
            <ta e="T785" id="Seg_13312" s="T782">Женщина не пошла.</ta>
            <ta e="T789" id="Seg_13313" s="T786">"Они чёрные, у меня таких хватает".</ta>
            <ta e="T800" id="Seg_13314" s="T792">Он пошёл к женщине: "Женщина, женщина, возьми жилы".</ta>
            <ta e="T811" id="Seg_13315" s="T800">А она говорит: "У меня много, они чёрные, мне не нужно".</ta>
            <ta e="T817" id="Seg_13316" s="T814">Тогда он пошёл к мышам.</ta>
            <ta e="T823" id="Seg_13317" s="T817">"Мышь, мышь, пойди, съешь у женщины жилы!"</ta>
            <ta e="T831" id="Seg_13318" s="T824">"Нет, мы не пойдём.</ta>
            <ta e="T837" id="Seg_13319" s="T832">Нам хватит корешков в земле, чтобы есть".</ta>
            <ta e="T844" id="Seg_13320" s="T840">Потом он пошёл к детям.</ta>
            <ta e="T848" id="Seg_13321" s="T844">"Дети, дети, убейте мышей!"</ta>
            <ta e="T854" id="Seg_13322" s="T848">Они [говорят:] "Мы со своими бабками играем".</ta>
            <ta e="T861" id="Seg_13323" s="T857">У вас дома хорошо.</ta>
            <ta e="T867" id="Seg_13324" s="T862">Только хлеба нигде не найдёшь.</ta>
            <ta e="T871" id="Seg_13325" s="T868">Нет хлеба, чтобы поесть.</ta>
            <ta e="T880" id="Seg_13326" s="T874">В магазине тоже ничего нет, купить нечего.</ta>
            <ta e="T890" id="Seg_13327" s="T881">"Неправда, иди, там есть колбаса, купи и тогда поешь".</ta>
            <ta e="T900" id="Seg_13328" s="T893">Эта женщина шьёт очень хорошо, красиво.</ta>
            <ta e="T903" id="Seg_13329" s="T901">Приятно посмотреть.</ta>
            <ta e="T916" id="Seg_13330" s="T906">Эта женщина очень много работает, она всё умеет делать.</ta>
            <ta e="T923" id="Seg_13331" s="T919">Всё умеет делать.</ta>
            <ta e="T935" id="Seg_13332" s="T926">Из родственников этой женщины мало кто живёт, только один брат.</ta>
            <ta e="T941" id="Seg_13333" s="T938">Её сестра умерла.</ta>
            <ta e="T949" id="Seg_13334" s="T944">Я зашила, теперь шить нечего.</ta>
            <ta e="T961" id="Seg_13335" s="T952">Мы (?) пошли за водой, а там есть вода.</ta>
            <ta e="T970" id="Seg_13336" s="T962">Мы принесли три ведра, и сейчас хватит.</ta>
            <ta e="T977" id="Seg_13337" s="T973">Я иду по лесу, пою про лес.</ta>
            <ta e="T982" id="Seg_13338" s="T978">Подхожу к дереву, пою про дерево.</ta>
            <ta e="T987" id="Seg_13339" s="T983">Выхожу в степь, пою про степь.</ta>
            <ta e="T992" id="Seg_13340" s="T988">Прихожу к горе, пою про гору.</ta>
            <ta e="T998" id="Seg_13341" s="T993">Иду через (Дяла?), пою про (Дяла?).</ta>
            <ta e="T1003" id="Seg_13342" s="T999">Сижу дома, пою про дом.</ta>
            <ta e="T1012" id="Seg_13343" s="T1006">У меня в доме очень много мышей.</ta>
            <ta e="T1016" id="Seg_13344" s="T1012">Они что попало едят.</ta>
            <ta e="T1020" id="Seg_13345" s="T1017">А кошки нет.</ta>
            <ta e="T1032" id="Seg_13346" s="T1021">Иди к (?), там две кошки, одна белая, одна красная.</ta>
            <ta e="T1036" id="Seg_13347" s="T1033">Он(а) тебе даст.</ta>
            <ta e="T1048" id="Seg_13348" s="T1037">Да он(а) у меня белую [кошку] взяла и ушёл (/ушла), не отпускает её.</ta>
            <ta e="T1052" id="Seg_13349" s="T1049">Больше не придёт.</ta>
            <ta e="T1058" id="Seg_13350" s="T1055">Я легла поспать.</ta>
            <ta e="T1068" id="Seg_13351" s="T1059">А там очень воняет, видишь, дерьма много лежит.</ta>
            <ta e="T1075" id="Seg_13352" s="T1071">Утром я поднялась, на коленки встала.</ta>
            <ta e="T1082" id="Seg_13353" s="T1076">Вижу: там десять куч лежит.</ta>
            <ta e="T1090" id="Seg_13354" s="T1085">Тогда я вынесла мясо наружу, в миску.</ta>
            <ta e="T1096" id="Seg_13355" s="T1090">[Другой] миской накрыла, чтобы мыши не съели.</ta>
            <ta e="T1107" id="Seg_13356" s="T1097">А масло в маслобойку тоже закрыла, чтобы мыши не съели.</ta>
            <ta e="T1119" id="Seg_13357" s="T1110">А если хочешь, у нас две кошки, одну отдам.</ta>
            <ta e="T1126" id="Seg_13358" s="T1120">Какую тебе дать, белую или красную?</ta>
            <ta e="T1130" id="Seg_13359" s="T1127">"Ну, дай красную".</ta>
            <ta e="T1139" id="Seg_13360" s="T1133">Он(а) идёт в баню, там очень много воды.</ta>
            <ta e="T1146" id="Seg_13361" s="T1140">Есть горячая вода, холодная, мыло есть.</ta>
            <ta e="T1150" id="Seg_13362" s="T1147">Тепло, (будет?) тепло.</ta>
            <ta e="T1159" id="Seg_13363" s="T1153">Твой нос не болит, и (?) течёт.</ta>
            <ta e="T1168" id="Seg_13364" s="T1162">Вон заплатка, а куда эта заплатка?</ta>
            <ta e="T1172" id="Seg_13365" s="T1169">На зад себе, что ли, поставить?</ta>
            <ta e="T1177" id="Seg_13366" s="T1173">Выбрось, [она] мне не нужна.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_13367" s="T1">Yesterday I went [to] my relatives.</ta>
            <ta e="T14" id="Seg_13368" s="T6">I said: "Don't be angry, I couldn't (sew?).</ta>
            <ta e="T22" id="Seg_13369" s="T15">My daughter-in-law didn't go, and I didn't go."</ta>
            <ta e="T27" id="Seg_13370" s="T23">They are lying on the bed, hugging each other.</ta>
            <ta e="T30" id="Seg_13371" s="T28">Kolya with Dunya.</ta>
            <ta e="T32" id="Seg_13372" s="T31">They are kissing each other.</ta>
            <ta e="T34" id="Seg_13373" s="T33">They are laughing.</ta>
            <ta e="T39" id="Seg_13374" s="T35">They say: "Grandmother, sit down, eat.</ta>
            <ta e="T42" id="Seg_13375" s="T40">Drink some vodka."</ta>
            <ta e="T47" id="Seg_13376" s="T43">I say: "I won't drink."</ta>
            <ta e="T52" id="Seg_13377" s="T48">Then I went home.</ta>
            <ta e="T61" id="Seg_13378" s="T55">Today I went to a woman, she's ill.</ta>
            <ta e="T71" id="Seg_13379" s="T62">She says: "Why didn't [you] come for so long, I'm dying."</ta>
            <ta e="T78" id="Seg_13380" s="T71">"You're always dying, but cannot die.</ta>
            <ta e="T81" id="Seg_13381" s="T79">Well, die.</ta>
            <ta e="T91" id="Seg_13382" s="T82">What do I care, I won't be sitting near you.</ta>
            <ta e="T95" id="Seg_13383" s="T92">After all, people are sitting (/living) (at my place?).</ta>
            <ta e="T99" id="Seg_13384" s="T96">They are waiting for me there. </ta>
            <ta e="T102" id="Seg_13385" s="T100">[I] have to talk.</ta>
            <ta e="T107" id="Seg_13386" s="T103">I won't sit near you!" [?]</ta>
            <ta e="T115" id="Seg_13387" s="T110">"My daughter-in-law was [here?] for two days.</ta>
            <ta e="T120" id="Seg_13388" s="T116">I won't sit near you!" [?]</ta>
            <ta e="T124" id="Seg_13389" s="T121">And people are coming [to me]."</ta>
            <ta e="T131" id="Seg_13390" s="T125">She says: "To hell with them."</ta>
            <ta e="T136" id="Seg_13391" s="T132">"[Rather] to hell with you!</ta>
            <ta e="T139" id="Seg_13392" s="T137">Die!"</ta>
            <ta e="T149" id="Seg_13393" s="T142">Then she remained [there], and I went home.</ta>
            <ta e="T155" id="Seg_13394" s="T152">I went to Aginskoye.</ta>
            <ta e="T165" id="Seg_13395" s="T156">And [while I was gone?] that woman came, that woman is living close to her. [?]</ta>
            <ta e="T170" id="Seg_13396" s="T166">She came and went away.</ta>
            <ta e="T178" id="Seg_13397" s="T171">"I'll die, who will wash me?"</ta>
            <ta e="T183" id="Seg_13398" s="T179">They laughed.</ta>
            <ta e="T189" id="Seg_13399" s="T184">Then I returned and went to her.</ta>
            <ta e="T196" id="Seg_13400" s="T189">"You had left, and I (died?) and froze."</ta>
            <ta e="T202" id="Seg_13401" s="T197">I said: "Well, freeze.</ta>
            <ta e="T208" id="Seg_13402" s="T203">And die.</ta>
            <ta e="T213" id="Seg_13403" s="T208">I'll warm up the water.</ta>
            <ta e="T218" id="Seg_13404" s="T214">And you'll pour it on yourself. [?]</ta>
            <ta e="T227" id="Seg_13405" s="T219">Then I'll wash [you] and put into the ground.</ta>
            <ta e="T237" id="Seg_13406" s="T230">How did I say, wait, how did I begin?</ta>
            <ta e="T248" id="Seg_13407" s="T240">This woman [says] to me: "Heat the sauna, wash me.</ta>
            <ta e="T257" id="Seg_13408" s="T249">If I die, you'll wash [my] face and hands."</ta>
            <ta e="T263" id="Seg_13409" s="T258">Then I heated the sauna.</ta>
            <ta e="T270" id="Seg_13410" s="T264">Then I put [her] on the sledge and carried [her].</ta>
            <ta e="T277" id="Seg_13411" s="T270">I washed her and carried back home, with the sledge.</ta>
            <ta e="T284" id="Seg_13412" s="T278">I put her on the bed and said: "Well, die now.</ta>
            <ta e="T292" id="Seg_13413" s="T285">I'll wash [your] face and hands, and it'll be OK."</ta>
            <ta e="T303" id="Seg_13414" s="T293">Then I washed [myself] in the bath, there was a lot of dirt on [my] head.</ta>
            <ta e="T309" id="Seg_13415" s="T304">I say: "Bring more water.</ta>
            <ta e="T313" id="Seg_13416" s="T310">Pour it here."</ta>
            <ta e="T322" id="Seg_13417" s="T316">I gave one horse to [my] husband's brother.</ta>
            <ta e="T326" id="Seg_13418" s="T323">It was two years old.</ta>
            <ta e="T331" id="Seg_13419" s="T327">Another horse I gave to someone. [?]</ta>
            <ta e="T334" id="Seg_13420" s="T332">It was a man.</ta>
            <ta e="T337" id="Seg_13421" s="T335">[He had] no horse.</ta>
            <ta e="T341" id="Seg_13422" s="T337">It was two years old, too.</ta>
            <ta e="T344" id="Seg_13423" s="T342">I gave the horse.</ta>
            <ta e="T349" id="Seg_13424" s="T344">Then I gave [a] sheep to [her?] sister.</ta>
            <ta e="T352" id="Seg_13425" s="T350">I gave [away] a coat.</ta>
            <ta e="T358" id="Seg_13426" s="T353">Then I gave the bull to [my] niece's daughter.</ta>
            <ta e="T362" id="Seg_13427" s="T359">Half-year-old.</ta>
            <ta e="T372" id="Seg_13428" s="T363">Then I gave a cow and a sheep [to my] (sister's son?).</ta>
            <ta e="T382" id="Seg_13429" s="T375">Then I gave (another?) calf to a woman.</ta>
            <ta e="T391" id="Seg_13430" s="T383">People will come: "Give [us] money, we'll give you back."</ta>
            <ta e="T397" id="Seg_13431" s="T392">But they don't [=won't?] give [it back].</ta>
            <ta e="T401" id="Seg_13432" s="T397">And then (?) money.</ta>
            <ta e="T417" id="Seg_13433" s="T406">Now I'm not thinking, otherwise I'd need a head big [like] that of the horse. [?] </ta>
            <ta e="T422" id="Seg_13434" s="T420">You think!</ta>
            <ta e="T434" id="Seg_13435" s="T427">Now I'm going to bring the cows to the corral, milk and feed them. [?]</ta>
            <ta e="T440" id="Seg_13436" s="T435">I came, but they were gone somewhere.</ta>
            <ta e="T447" id="Seg_13437" s="T443">A tractor is carrying logs.</ta>
            <ta e="T452" id="Seg_13438" s="T448">I don't know how much it carries.</ta>
            <ta e="T458" id="Seg_13439" s="T455">Very many logs.</ta>
            <ta e="T464" id="Seg_13440" s="T459">And very thick.</ta>
            <ta e="T474" id="Seg_13441" s="T467">[?]</ta>
            <ta e="T480" id="Seg_13442" s="T477">The chief is very beautiful.</ta>
            <ta e="T487" id="Seg_13443" s="T481">He (gave?) me everything.</ta>
            <ta e="T494" id="Seg_13444" s="T490">He showed me everything.</ta>
            <ta e="T499" id="Seg_13445" s="T496">This is somehow too little.</ta>
            <ta e="T504" id="Seg_13446" s="T500">A day will be and food will be.</ta>
            <ta e="T514" id="Seg_13447" s="T507">Today is Saturday, [I] have to heat the bath, bring water.</ta>
            <ta e="T520" id="Seg_13448" s="T515">Then I'll heat [the bath], I have to wash myself.</ta>
            <ta e="T526" id="Seg_13449" s="T521">Because I haven't washed myself since a week.</ta>
            <ta e="T533" id="Seg_13450" s="T529">I was waiting for a letter at home for a long time.</ta>
            <ta e="T537" id="Seg_13451" s="T534">It's still not here.</ta>
            <ta e="T540" id="Seg_13452" s="T537">My (woman?) doesn't write.</ta>
            <ta e="T545" id="Seg_13453" s="T541">And the letter isn't coming.</ta>
            <ta e="T551" id="Seg_13454" s="T547">I saw it in my dream tonight.</ta>
            <ta e="T556" id="Seg_13455" s="T552">I (quarreled?), today the letter will come.</ta>
            <ta e="T564" id="Seg_13456" s="T559">I went to Krasnoyarsk, I carried eggs.</ta>
            <ta e="T568" id="Seg_13457" s="T565">A hundred eggs.</ta>
            <ta e="T573" id="Seg_13458" s="T569">Then my brother came with me.</ta>
            <ta e="T580" id="Seg_13459" s="T574">He brought me, (seated?) [me] in a car.</ta>
            <ta e="T589" id="Seg_13460" s="T581">I couldn't find [her], then I found my niece.</ta>
            <ta e="T596" id="Seg_13461" s="T590">I came there, from here [I went] with a car.</ta>
            <ta e="T600" id="Seg_13462" s="T597">And then by bus.</ta>
            <ta e="T610" id="Seg_13463" s="T601">Then [by] railroad, then I came there.</ta>
            <ta e="T619" id="Seg_13464" s="T613">Then I went to pray to God.</ta>
            <ta e="T624" id="Seg_13465" s="T620">Then we went with my niece to (the Dzhibyeva's?).</ta>
            <ta e="T630" id="Seg_13466" s="T625">We were sitting and chattering there.</ta>
            <ta e="T635" id="Seg_13467" s="T631">Then I came home.</ta>
            <ta e="T643" id="Seg_13468" s="T638">My niece gave me a lot of tomatoes.</ta>
            <ta e="T649" id="Seg_13469" s="T643">I passed two days in Aginskoye.</ta>
            <ta e="T662" id="Seg_13470" s="T652">The cows came home, [I] have to [bring them] in the corral, milk and feed them.</ta>
            <ta e="T669" id="Seg_13471" s="T665">I thought and thought and found something out.</ta>
            <ta e="T678" id="Seg_13472" s="T672">I went to [my] daughter, my bag ripped.</ta>
            <ta e="T684" id="Seg_13473" s="T679">Now I'm sitting and sewing [it] up.</ta>
            <ta e="T691" id="Seg_13474" s="T685">I was going to you and (nearly?) fell.</ta>
            <ta e="T695" id="Seg_13475" s="T694">Wait a bit.</ta>
            <ta e="T696" id="Seg_13476" s="T695">[KA:] Hare.</ta>
            <ta e="T700" id="Seg_13477" s="T697">[PKZ:] A [hare] is going.</ta>
            <ta e="T708" id="Seg_13478" s="T700">A blade of grass scratched its nose.</ta>
            <ta e="T711" id="Seg_13479" s="T708">Blood is flowing.</ta>
            <ta e="T715" id="Seg_13480" s="T712">He says: "Fire!</ta>
            <ta e="T719" id="Seg_13481" s="T716">Burn this blade.</ta>
            <ta e="T726" id="Seg_13482" s="T722">"I have a lot of earth to burn."</ta>
            <ta e="T730" id="Seg_13483" s="T726">Then it went to the water.</ta>
            <ta e="T736" id="Seg_13484" s="T731">"Water, go and flood the fire!"</ta>
            <ta e="T747" id="Seg_13485" s="T739">"I have a lot of earth to flood, to make grass grow."</ta>
            <ta e="T753" id="Seg_13486" s="T750">Then it went.</ta>
            <ta e="T761" id="Seg_13487" s="T754">"Moose, go and drink all the water!"</ta>
            <ta e="T769" id="Seg_13488" s="T762">The moose said: "There is a lot of water at my feet."</ta>
            <ta e="T775" id="Seg_13489" s="T770">Then it went to a woman.</ta>
            <ta e="T781" id="Seg_13490" s="T775">"Woman, go and take sinews from its feet!"</ta>
            <ta e="T785" id="Seg_13491" s="T782">The woman didn't go.</ta>
            <ta e="T789" id="Seg_13492" s="T786">"They are black, I have enough of them."</ta>
            <ta e="T800" id="Seg_13493" s="T792">It went to a woman: "Woman, take [its] sinews."</ta>
            <ta e="T811" id="Seg_13494" s="T800">She says: "I have enough of them, they are black, I don't need [them]."</ta>
            <ta e="T817" id="Seg_13495" s="T814">Then it went to the mice.</ta>
            <ta e="T823" id="Seg_13496" s="T817">"Mouse, go and eat the woman's sinews!"</ta>
            <ta e="T831" id="Seg_13497" s="T824">"No, we won't go.</ta>
            <ta e="T837" id="Seg_13498" s="T832">It's enough for us to eat roots in the ground."</ta>
            <ta e="T844" id="Seg_13499" s="T840">Then it came to the children.</ta>
            <ta e="T848" id="Seg_13500" s="T844">"Children, kill the mice!"</ta>
            <ta e="T854" id="Seg_13501" s="T848">They [say:] "We're playing knucklebones ourselves."</ta>
            <ta e="T861" id="Seg_13502" s="T857">It's good to be in your house.</ta>
            <ta e="T867" id="Seg_13503" s="T862">Only one can't find bread.</ta>
            <ta e="T871" id="Seg_13504" s="T868">There is no bread to eat.</ta>
            <ta e="T880" id="Seg_13505" s="T874">In the shop, too, there is nothing, one can't buy anything.</ta>
            <ta e="T890" id="Seg_13506" s="T881">"It's not true, go, there is sausage, buy [it] and then you'll eat."</ta>
            <ta e="T900" id="Seg_13507" s="T893">This woman sews very well, beautifully.</ta>
            <ta e="T903" id="Seg_13508" s="T901">It's good to look at.</ta>
            <ta e="T916" id="Seg_13509" s="T906">This woman works very much, she knows how to do everything.</ta>
            <ta e="T923" id="Seg_13510" s="T919">She knows how to do anything.</ta>
            <ta e="T935" id="Seg_13511" s="T926">Of this woman's relatives, few are living, only one [of] her brothers.</ta>
            <ta e="T941" id="Seg_13512" s="T938">Her sister has died.</ta>
            <ta e="T949" id="Seg_13513" s="T944">I have done all the sewing, now there is nothing to sew.</ta>
            <ta e="T961" id="Seg_13514" s="T952">We (?) went to bring water, but there was water.</ta>
            <ta e="T970" id="Seg_13515" s="T962">We brought three buckets, and that's enough for now.</ta>
            <ta e="T977" id="Seg_13516" s="T973">I'm walking through the forest, I'm singing about the forest. </ta>
            <ta e="T982" id="Seg_13517" s="T978">I'm walking by a tree, I'm singing about the tree.</ta>
            <ta e="T987" id="Seg_13518" s="T983">I'm walking through the steppe, I'm singing about the steppe.</ta>
            <ta e="T992" id="Seg_13519" s="T988">I'm walking on a mountain, I'm singing about the mountain.</ta>
            <ta e="T998" id="Seg_13520" s="T993">[I]'m walking through (Dyala?), I'm singing about (Dyala?).</ta>
            <ta e="T1003" id="Seg_13521" s="T999">I'm sitting at home, I'm singing about the home.</ta>
            <ta e="T1012" id="Seg_13522" s="T1006">In my house, there is a lot of mice.</ta>
            <ta e="T1016" id="Seg_13523" s="T1012">They eat all sorts of things.</ta>
            <ta e="T1020" id="Seg_13524" s="T1017">And there is no cat.</ta>
            <ta e="T1032" id="Seg_13525" s="T1021">Go to (?), there are two cats, one is white, another is red.</ta>
            <ta e="T1036" id="Seg_13526" s="T1033">S/he'll give you.</ta>
            <ta e="T1048" id="Seg_13527" s="T1037">S/he took the white [cat] from me and went away, s/he didn't let him go.</ta>
            <ta e="T1052" id="Seg_13528" s="T1049">It won't come again.</ta>
            <ta e="T1058" id="Seg_13529" s="T1055">I laid down to sleep.</ta>
            <ta e="T1068" id="Seg_13530" s="T1059">It stinks very much there, I see, there is a lot of shit.</ta>
            <ta e="T1075" id="Seg_13531" s="T1071">In the morning i stood up, got down to my knees.</ta>
            <ta e="T1082" id="Seg_13532" s="T1076">I see: there are ten heaps.</ta>
            <ta e="T1090" id="Seg_13533" s="T1085">I took the meat outside, in a bowl.</ta>
            <ta e="T1096" id="Seg_13534" s="T1090">I covered [it] with [another] bowl, so that the mice wouldn't eat it.</ta>
            <ta e="T1107" id="Seg_13535" s="T1097">The butter I closed in the churn, so that the mice wouldn't eat it.</ta>
            <ta e="T1119" id="Seg_13536" s="T1110">If you want, we have two (cats?), I'll give [you] one.</ta>
            <ta e="T1126" id="Seg_13537" s="T1120">Which one do you want, the white one or the red one?</ta>
            <ta e="T1130" id="Seg_13538" s="T1127">"Well, give me the red one."</ta>
            <ta e="T1139" id="Seg_13539" s="T1133">S/he goes to the bath, there is very much water.</ta>
            <ta e="T1146" id="Seg_13540" s="T1140">There is hot water, cold [water], there is soap.</ta>
            <ta e="T1150" id="Seg_13541" s="T1147">[It will be?] warm.</ta>
            <ta e="T1159" id="Seg_13542" s="T1153">Your nose doesn't hurt, and (?) flow.</ta>
            <ta e="T1168" id="Seg_13543" s="T1162">This [is] a patch, where [do I put] this patch?</ta>
            <ta e="T1172" id="Seg_13544" s="T1169">To put it on [my] back, or what?</ta>
            <ta e="T1177" id="Seg_13545" s="T1173">Throw(?) [it], I don't need [it].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_13546" s="T1">Gestern ging ich [zu] meinen Verwandten.</ta>
            <ta e="T14" id="Seg_13547" s="T6">Ich sagte: „Seid nicht böse, ich konnte nicht (nähen?).</ta>
            <ta e="T22" id="Seg_13548" s="T15">Meine Schwiegertochter ging nicht, und ich ging nicht.“</ta>
            <ta e="T27" id="Seg_13549" s="T23">Sie liegen auf dem Bett und umarmen sich.</ta>
            <ta e="T30" id="Seg_13550" s="T28">Kolya mit Dunya.</ta>
            <ta e="T32" id="Seg_13551" s="T31">Sie küssen einander.</ta>
            <ta e="T34" id="Seg_13552" s="T33">Sie lachen.</ta>
            <ta e="T39" id="Seg_13553" s="T35">Sie sagen: „Großmutter, setzt dich, iss.</ta>
            <ta e="T42" id="Seg_13554" s="T40">Trink etwas Vodka.“</ta>
            <ta e="T47" id="Seg_13555" s="T43">Ich sage: „Ich werde nicht trinken.“</ta>
            <ta e="T52" id="Seg_13556" s="T48">Dann ging ich nach Hause.</ta>
            <ta e="T61" id="Seg_13557" s="T55">Heute ging ich zu einer Frau, sie ist krank.</ta>
            <ta e="T71" id="Seg_13558" s="T62">Sie sagt: „Warum bist [du] so lange nicht gekommen, ich sterbe.“</ta>
            <ta e="T78" id="Seg_13559" s="T71">„Du stirbst stänidg, aber kannst nicht sterben.</ta>
            <ta e="T81" id="Seg_13560" s="T79">Also, stirb.</ta>
            <ta e="T91" id="Seg_13561" s="T82">Was kümmert es mich, dann ich sitze nicht mehr bei dir.</ta>
            <ta e="T95" id="Seg_13562" s="T92">Immerhin sitzen (/leben) die Leute (bei mir?).</ta>
            <ta e="T99" id="Seg_13563" s="T96">Sie warten dort auf mich.</ta>
            <ta e="T102" id="Seg_13564" s="T100">[Ich] muss sprechen.</ta>
            <ta e="T107" id="Seg_13565" s="T103">Ich werde nicht bei dir sitzen!“ [?]</ta>
            <ta e="T115" id="Seg_13566" s="T110">„Meine Schwiegertochter war für zwei Tage [hier?].</ta>
            <ta e="T120" id="Seg_13567" s="T116">Ich werde nicht bei dir sitzen!“ [?]</ta>
            <ta e="T124" id="Seg_13568" s="T121">Und die Leute kommen [zu mir].“</ta>
            <ta e="T131" id="Seg_13569" s="T125">Sie sagt: „Zur Hölle mit ihnen.“</ta>
            <ta e="T136" id="Seg_13570" s="T132">„[Eher] zur Hölle mit dir!</ta>
            <ta e="T139" id="Seg_13571" s="T137">Stirb!“</ta>
            <ta e="T149" id="Seg_13572" s="T142">Dann blieb sie [da], und ich ging nach Hause.</ta>
            <ta e="T155" id="Seg_13573" s="T152">Ich ging nach Aginskoye.</ta>
            <ta e="T165" id="Seg_13574" s="T156">Und [während ich weg war,?] kam diese Frau, diese Frau lebt nahe bei ihr. [?]</ta>
            <ta e="T170" id="Seg_13575" s="T166">Sie kam und ging weg.</ta>
            <ta e="T178" id="Seg_13576" s="T171">„Ich werde sterben, wer wird mich waschen?“</ta>
            <ta e="T183" id="Seg_13577" s="T179">Sie lachten.</ta>
            <ta e="T189" id="Seg_13578" s="T184">Dann kamm ich wieder und ging ich zu ihr.</ta>
            <ta e="T196" id="Seg_13579" s="T189">„Du warst gegangen, und ich (starb?) und fror.“</ta>
            <ta e="T202" id="Seg_13580" s="T197">Ich sagte: „Also, frier.</ta>
            <ta e="T208" id="Seg_13581" s="T203">Und sterbe.</ta>
            <ta e="T213" id="Seg_13582" s="T208">Ich mache das Wasser warm.</ta>
            <ta e="T218" id="Seg_13583" s="T214">Und du gießt es auf dich. [?]</ta>
            <ta e="T227" id="Seg_13584" s="T219">Dann werde ich [dich] waschen, und dich in die Erde legen.</ta>
            <ta e="T237" id="Seg_13585" s="T230">Wie ich schon sagte, warte, wie habe ich angefangen?</ta>
            <ta e="T248" id="Seg_13586" s="T240">Diese Frau [sagt] zu mir: "Erhitze die Sauna, wasche mich.</ta>
            <ta e="T257" id="Seg_13587" s="T249">Wenn ich sterbe, wäschst du [mein] Gesicht und Hände.“</ta>
            <ta e="T263" id="Seg_13588" s="T258">Dann erhitzte ich die Sauna.</ta>
            <ta e="T270" id="Seg_13589" s="T264">Dann legte ic [sie] auf den Schlitten und trug [sie]. </ta>
            <ta e="T277" id="Seg_13590" s="T270">Ich wusch ihr und trug sie wieder nach Hause, auf einem Schlitten.</ta>
            <ta e="T284" id="Seg_13591" s="T278">Ich legte sie aufs Bsett und sagte: „Also, stirb jetzt.</ta>
            <ta e="T292" id="Seg_13592" s="T285">Ich werde [dein] Gesicht und Hände waschen, und es wird gut sein.“</ta>
            <ta e="T303" id="Seg_13593" s="T293">Dan wusch ich [mich selbst] im Bad, es war viel Schmutz in [meinem] Haar.</ta>
            <ta e="T309" id="Seg_13594" s="T304">Ich sage: „Hol mehr Wasser.</ta>
            <ta e="T313" id="Seg_13595" s="T310">Schütte es hierher.“</ta>
            <ta e="T322" id="Seg_13596" s="T316">Ich schenkte ein Pferd an [meinem] Mann sein Bruder. </ta>
            <ta e="T326" id="Seg_13597" s="T323">Es war zwei Jahre alt.</ta>
            <ta e="T331" id="Seg_13598" s="T327">Ein anderes Pferd schenkte ich jemanden. [?]</ta>
            <ta e="T334" id="Seg_13599" s="T332">Es war ein Mann.</ta>
            <ta e="T337" id="Seg_13600" s="T335">[Er hatte] kein Pferd.</ta>
            <ta e="T341" id="Seg_13601" s="T337">Es war auch zwei Jahre alt.</ta>
            <ta e="T344" id="Seg_13602" s="T342">Das Pferd schenkte ich.</ta>
            <ta e="T349" id="Seg_13603" s="T344">Dann schenkte ich [ein Schaf] an [ihre?] Schwester.</ta>
            <ta e="T352" id="Seg_13604" s="T350">Ich [ver]schenkte einen Mantel.</ta>
            <ta e="T358" id="Seg_13605" s="T353">Dann schenkte ich den Bullen an [meiner] Nichte die Tochter.</ta>
            <ta e="T362" id="Seg_13606" s="T359">Halbes Jahr alt.</ta>
            <ta e="T372" id="Seg_13607" s="T363">Dann schenkte ich eine Kuh und ein Schaf (dem Sohn [meiner] Schwester?).</ta>
            <ta e="T382" id="Seg_13608" s="T375">Dann schenkte ich ein (anderes?) Kalb an eine Frau.</ta>
            <ta e="T391" id="Seg_13609" s="T383">Leute warden kommen: „Gib [uns] Geld, wir geben es zurück.“</ta>
            <ta e="T397" id="Seg_13610" s="T392">Aber sie tun [es] nicht [wollen nicht?] [zurück] geben.</ta>
            <ta e="T401" id="Seg_13611" s="T397">Und dann (?) Geld.</ta>
            <ta e="T417" id="Seg_13612" s="T406">Jetzt denke ich nicht nach, sonst bräuchte ich einen Kopf groß [wie] den eines Pferds. [?]</ta>
            <ta e="T422" id="Seg_13613" s="T420">Meinst du!</ta>
            <ta e="T434" id="Seg_13614" s="T427">Jetzt gehe ich die Kühe zum Stall bringen, melken und füttern. [?]</ta>
            <ta e="T440" id="Seg_13615" s="T435">Ich kam, aber sie waren weggewesen.</ta>
            <ta e="T447" id="Seg_13616" s="T443">Ein Traktor trägt Baumstämme.</ta>
            <ta e="T452" id="Seg_13617" s="T448">Ich weiß nicht wieviele er trägt.</ta>
            <ta e="T458" id="Seg_13618" s="T455">Sehr viele Baumstämme.</ta>
            <ta e="T464" id="Seg_13619" s="T459">Und sehr dick.</ta>
            <ta e="T474" id="Seg_13620" s="T467">[?]</ta>
            <ta e="T480" id="Seg_13621" s="T477">Der Anführer ist wunderschön.</ta>
            <ta e="T487" id="Seg_13622" s="T481">Er (gab?) mir alles.</ta>
            <ta e="T494" id="Seg_13623" s="T490">Er zeigte mir alles.</ta>
            <ta e="T499" id="Seg_13624" s="T496">Das reicht nicht aus.</ta>
            <ta e="T504" id="Seg_13625" s="T500">Ein Tag wird es geben und Essen wird es geben.</ta>
            <ta e="T514" id="Seg_13626" s="T507">Heute ist Samstag, [ich] muss das Bad erhitzen, hole Wasser.</ta>
            <ta e="T520" id="Seg_13627" s="T515">Dann werde ich [das Bad] erhitzen, ich muss mich waschen.</ta>
            <ta e="T526" id="Seg_13628" s="T521">Weil ich mich seit einer Woche nicht gewaschen habe.</ta>
            <ta e="T533" id="Seg_13629" s="T529">Ich wartete langer Zeit zu Hause auf einen Brief.</ta>
            <ta e="T537" id="Seg_13630" s="T534">Er ist immer noch nicht da.</ta>
            <ta e="T540" id="Seg_13631" s="T537">Meine (Frau?) schreibt nicht.</ta>
            <ta e="T545" id="Seg_13632" s="T541">Und der Brief kommt nicht.</ta>
            <ta e="T551" id="Seg_13633" s="T547">Heute Nacht habe ich es in meinem Traum gesehen.</ta>
            <ta e="T556" id="Seg_13634" s="T552">Ich (zerstritt mich?), heute wird der Brief kommen.</ta>
            <ta e="T564" id="Seg_13635" s="T559">Ich ging nach Krasnoyarsk, ich trug Eier.</ta>
            <ta e="T568" id="Seg_13636" s="T565">Einhundert Eier.</ta>
            <ta e="T573" id="Seg_13637" s="T569">Dann kam mein Bruder mit mir.</ta>
            <ta e="T580" id="Seg_13638" s="T574">Er brachte mich, (setzte?) [mich] in ein Auto.</ta>
            <ta e="T589" id="Seg_13639" s="T581">Ich konnte [sie] nicht finden, dann fand ich meine Nichte.</ta>
            <ta e="T596" id="Seg_13640" s="T590">Ich kam dorthin, von hier [fuhr ich] mit einem Auto.</ta>
            <ta e="T600" id="Seg_13641" s="T597">Und dann mit dem Bus</ta>
            <ta e="T610" id="Seg_13642" s="T601">Dann [mit der] Eisenbahn, dann kam ich dort an.</ta>
            <ta e="T619" id="Seg_13643" s="T613">Dann ging ich zu Gott zu beten.</ta>
            <ta e="T624" id="Seg_13644" s="T620">Dann gingen wir mit meiner Nichte zu (Dzhibyeva?).</ta>
            <ta e="T630" id="Seg_13645" s="T625">Wir saßen und plauderten da.</ta>
            <ta e="T635" id="Seg_13646" s="T631">Dann kam ich nach Hause.</ta>
            <ta e="T643" id="Seg_13647" s="T638">Meine Nichte gab mir viele Tomaten.</ta>
            <ta e="T649" id="Seg_13648" s="T643">Ich verbrachte zwei Tage in Aginskoye.</ta>
            <ta e="T662" id="Seg_13649" s="T652">Die Kühe kamen nach Hause, [Ich] mußte [sie]in den Korral [bringen], melken und füttern.</ta>
            <ta e="T669" id="Seg_13650" s="T665">Ich dachte und dachte und fand etwas heraus.</ta>
            <ta e="T678" id="Seg_13651" s="T672">Ich ging zu meiner Tochter, meine Tasche riss.</ta>
            <ta e="T684" id="Seg_13652" s="T679">Nun sitze ich und nähe [sie] zurecht.</ta>
            <ta e="T691" id="Seg_13653" s="T685">Ich wollte zu dir gehen und fiel (fast?).</ta>
            <ta e="T695" id="Seg_13654" s="T694">Warte ein bißchen.</ta>
            <ta e="T696" id="Seg_13655" s="T695">[KA:] Hase.</ta>
            <ta e="T700" id="Seg_13656" s="T697">[PKZ:] Ein [Hase] geht.</ta>
            <ta e="T708" id="Seg_13657" s="T700">Ein Grasblatt scharrte seine Nase. </ta>
            <ta e="T711" id="Seg_13658" s="T708">Blut fließt.</ta>
            <ta e="T715" id="Seg_13659" s="T712">Er sagt: „Feuer!</ta>
            <ta e="T719" id="Seg_13660" s="T716">Verbrenne dieses Blatt.</ta>
            <ta e="T726" id="Seg_13661" s="T722">„Ich habe viel Erde zu brennen.“</ta>
            <ta e="T730" id="Seg_13662" s="T726">Dann ging er zum Wasser.</ta>
            <ta e="T736" id="Seg_13663" s="T731">„Wasser, gehe das Feuer überschwemmen!“</ta>
            <ta e="T747" id="Seg_13664" s="T739">„Ich habe viel Erder zu überschwemmen, damit das Gras wächst.“</ta>
            <ta e="T753" id="Seg_13665" s="T750">Dann ging er.</ta>
            <ta e="T761" id="Seg_13666" s="T754">„Elch, geh das ganze Wasser trinken!“</ta>
            <ta e="T769" id="Seg_13667" s="T762">Der Elch sagte: „Es ist schon viel Wasser zu meinen Füßen.“</ta>
            <ta e="T775" id="Seg_13668" s="T770">Dann ging er zu einer Frau.</ta>
            <ta e="T781" id="Seg_13669" s="T775">„Frau, go ihm die Sehnen von den Füßen holen!“</ta>
            <ta e="T785" id="Seg_13670" s="T782">Die Frau ging nicht.</ta>
            <ta e="T789" id="Seg_13671" s="T786">„Sie sind schwarz, ich habe genug davon.“</ta>
            <ta e="T800" id="Seg_13672" s="T792">Er ging zu einer Frau: „Frau, nimm [seine] Sehnen.“</ta>
            <ta e="T811" id="Seg_13673" s="T800">Sie sagt: „Ich habe genug davon, sie sind schwarz, ich brauche [sie] nicht.“</ta>
            <ta e="T817" id="Seg_13674" s="T814">Dann ging er zu den Mäusen.</ta>
            <ta e="T823" id="Seg_13675" s="T817">„Maus, geh die Sehnen der Frau fressen!“</ta>
            <ta e="T831" id="Seg_13676" s="T824">„Nein, wir gehen nicht.“</ta>
            <ta e="T837" id="Seg_13677" s="T832">Es reicht uns, Wurzeln in der Erde zu fressen.“</ta>
            <ta e="T844" id="Seg_13678" s="T840">Dann kam er zu den Kindern.</ta>
            <ta e="T848" id="Seg_13679" s="T844">„Kinder, tötet die Mäuse!“</ta>
            <ta e="T854" id="Seg_13680" s="T848">Sie [sagen]: „Wir spielen selber Knöcheln.“</ta>
            <ta e="T861" id="Seg_13681" s="T857">Es ist gut in deinem Haus zu sein.</ta>
            <ta e="T867" id="Seg_13682" s="T862">Nur findet man kein Brot.</ta>
            <ta e="T871" id="Seg_13683" s="T868">Es ist kein Brot zu essen.</ta>
            <ta e="T880" id="Seg_13684" s="T874">Auch im Laden, gibt es nichts, man kann nichts kaufen.</ta>
            <ta e="T890" id="Seg_13685" s="T881">„Das stimmt nicht, geh, es gibt Wurst, kauf [sie] und dann isst du.“</ta>
            <ta e="T900" id="Seg_13686" s="T893">Diese Frau näht sehr gut, wunderschön.</ta>
            <ta e="T903" id="Seg_13687" s="T901">Es ist gut anzuschauen.</ta>
            <ta e="T916" id="Seg_13688" s="T906">Diese Frau arbeitet sehr viel, sie weiß, wie man alles macht.</ta>
            <ta e="T923" id="Seg_13689" s="T919">Sie weiß, wie man alles macht.</ta>
            <ta e="T935" id="Seg_13690" s="T926">Von den Verwandten dieser Frau, leben nur wenige, nur einer [von] ihren Brüdern.</ta>
            <ta e="T941" id="Seg_13691" s="T938">Ihre Schwester ist gestorben.</ta>
            <ta e="T949" id="Seg_13692" s="T944">Ich habe alles genäht, jetzt gibt es nichts mehr zu nähen.</ta>
            <ta e="T961" id="Seg_13693" s="T952">Wir (?) gingen Wasser holen, aber es war Wasser.</ta>
            <ta e="T970" id="Seg_13694" s="T962">Wir brachten drei Eimer, und das reicht erstmal.</ta>
            <ta e="T977" id="Seg_13695" s="T973">Ich laufe durch den Wald, ich sing von dem Wald.</ta>
            <ta e="T982" id="Seg_13696" s="T978">Ich laufe an einem Baum vorbei, ich singe von dem Baum</ta>
            <ta e="T987" id="Seg_13697" s="T983">Ich laufe durch die Steppe, ich singe von der Steppe.</ta>
            <ta e="T992" id="Seg_13698" s="T988">Ich laufe auf einem Berg, ich singe von dem Berg.</ta>
            <ta e="T998" id="Seg_13699" s="T993">[Ich] laufe durch (Dyala?), ich singe von (Dyala?).</ta>
            <ta e="T1003" id="Seg_13700" s="T999">Ich sitze zu Hause, ich singe vom Zuhause.</ta>
            <ta e="T1012" id="Seg_13701" s="T1006">In meinem Haus sind viele Mäuse.</ta>
            <ta e="T1016" id="Seg_13702" s="T1012">Sie essen aller art von Dingen.</ta>
            <ta e="T1020" id="Seg_13703" s="T1017">Und es ist keine Katze.</ta>
            <ta e="T1032" id="Seg_13704" s="T1021">Gehe zu (?), es sind zwei Katzen, eine ist weiß, eine andere ist rot.</ta>
            <ta e="T1036" id="Seg_13705" s="T1033">Sie/Er wird dir geben.</ta>
            <ta e="T1048" id="Seg_13706" s="T1037">Sie/Er nahm mir die weiße [Katze] und ging weg, sie/er ließ sie nicht laufen.</ta>
            <ta e="T1052" id="Seg_13707" s="T1049">Sie wird nicht zurückkommen.</ta>
            <ta e="T1058" id="Seg_13708" s="T1055">Ich legte mich schlafen.</ta>
            <ta e="T1068" id="Seg_13709" s="T1059">Es stinkt sehr dort, ich sehe, es ist eine Menge Kot.</ta>
            <ta e="T1075" id="Seg_13710" s="T1071">Am Morgen, stand ich auf, kniete mich hin.</ta>
            <ta e="T1082" id="Seg_13711" s="T1076">Ich sehe: es sind zehn Häufchen.</ta>
            <ta e="T1090" id="Seg_13712" s="T1085">Ich nahm das Fleisch hinaus, in einer Schüssel.</ta>
            <ta e="T1096" id="Seg_13713" s="T1090">Ich bedeckte [es] mit [eine weitere] Schüssel, damit die Mäuse es nicht fressen.</ta>
            <ta e="T1107" id="Seg_13714" s="T1097">Die Butter tat ich in das Butterfaß, damit die Mäuse sie nicht fressen.</ta>
            <ta e="T1119" id="Seg_13715" s="T1110">Wenn du willst, wir haben zwei (Katzen?), ich schenke [dir] eine.</ta>
            <ta e="T1126" id="Seg_13716" s="T1120">Welche willst du, die Weiße oder die Rote?</ta>
            <ta e="T1130" id="Seg_13717" s="T1127">„Na ja, gib mir die Rote.“</ta>
            <ta e="T1139" id="Seg_13718" s="T1133">Sie/Er geht ins Bad, es ist sehr viel Wasser</ta>
            <ta e="T1146" id="Seg_13719" s="T1140">Es ist heißes Wasser, kaltes [Wasser], es ist Seife.</ta>
            <ta e="T1150" id="Seg_13720" s="T1147">[Es wird?] warm.</ta>
            <ta e="T1159" id="Seg_13721" s="T1153">Deine Nase tut nicht mehr weh, und (?) fließen.</ta>
            <ta e="T1168" id="Seg_13722" s="T1162">Es [ist] ein Flicken, wo [tue ich] diesen Flicken hin?</ta>
            <ta e="T1172" id="Seg_13723" s="T1169">Um auf [meinen] Rücken zu setzen, oder was?</ta>
            <ta e="T1177" id="Seg_13724" s="T1173">Werf(?) [ihn], ich brauch [ihn] nicht.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_13725" s="T6">[GVY:] šozittə 'come'?</ta>
            <ta e="T71" id="Seg_13726" s="T62">[GVY:] šobiam = šobial.</ta>
            <ta e="T95" id="Seg_13727" s="T92">[AAV] probably referring to the researchers who are waiting for her to make recordings </ta>
            <ta e="T149" id="Seg_13728" s="T142">[GVY:] malubi = maluʔpi.</ta>
            <ta e="T165" id="Seg_13729" s="T156">[GVY:] Unclear.</ta>
            <ta e="T183" id="Seg_13730" s="T179">[GVY:] This whole fragment is unclear.</ta>
            <ta e="T196" id="Seg_13731" s="T189">[AAV] Sound becomes very quiet until the next break.</ta>
            <ta e="T313" id="Seg_13732" s="T310">[GVY:] döm = dön?</ta>
            <ta e="T401" id="Seg_13733" s="T397">[GVY:] dĭgəttəʔ</ta>
            <ta e="T422" id="Seg_13734" s="T420">[GVY:] tenolaʔbə = tenoʔlagaʔ?</ta>
            <ta e="T434" id="Seg_13735" s="T427">[AAV] šendedndə -- prob. intended šeden-də "corral-LAT"; cf. sent. (130)</ta>
            <ta e="T474" id="Seg_13736" s="T467">[GVY:] Unclear</ta>
            <ta e="T480" id="Seg_13737" s="T477">[GVY:] = very kind?</ta>
            <ta e="T487" id="Seg_13738" s="T481">[GVY:] mĭmbi = mĭbi?</ta>
            <ta e="T580" id="Seg_13739" s="T574">[GVY:] Apparently, her brother accompanied her until she sat in a car.</ta>
            <ta e="T624" id="Seg_13740" s="T620">[AAV] Probably they went to visit Aleksandra Dzhibyeva, the other Kamas speaker (SAE).</ta>
            <ta e="T700" id="Seg_13741" s="T697">[KlT:] Text 5 from Donner's collection. [GVY:] Kazaŋ = kozan.</ta>
            <ta e="T747" id="Seg_13742" s="T739">[AAV] чтобы + PST</ta>
            <ta e="T781" id="Seg_13743" s="T775">[GVY:] jit?</ta>
            <ta e="T848" id="Seg_13744" s="T844">[GVY:] kuʔtlaləj? = kuʔkaʔ?</ta>
            <ta e="T854" id="Seg_13745" s="T848">[GVY:] s’arlaʔbəʔjə = s’arlaʔbəʔbaʔ</ta>
            <ta e="T861" id="Seg_13746" s="T857">[GVY:] at your place?</ta>
            <ta e="T900" id="Seg_13747" s="T893">[GVY:] šoderlaʔbə</ta>
            <ta e="T949" id="Seg_13748" s="T944">[GVY:] Södörluʔpiem</ta>
            <ta e="T1096" id="Seg_13749" s="T1090">[GVY:] takšeži.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
