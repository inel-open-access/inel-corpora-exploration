<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDAFAA79CF-0057-234C-9FA5-6F869F1741E7">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_SU0220_rest2_misc</transcription-name>
         <referenced-file url="PKZ_196X_SU0220_rest2_misc.wav" />
         <referenced-file url="PKZ_196X_SU0220_rest2_misc.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0220_rest2_misc\PKZ_196X_SU0220_rest2_misc.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">69</ud-information>
            <ud-information attribute-name="# HIAT:w">43</ud-information>
            <ud-information attribute-name="# e">44</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">16</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.06" type="appl" />
         <tli id="T2" time="1.1042857142857143" type="appl" />
         <tli id="T3" time="2.1485714285714286" type="appl" />
         <tli id="T4" time="3.1928571428571426" type="appl" />
         <tli id="T5" time="4.237142857142857" type="appl" />
         <tli id="T6" time="5.281428571428571" type="appl" />
         <tli id="T7" time="6.325714285714285" type="appl" />
         <tli id="T9" time="7.37" type="appl" />
         <tli id="T10" time="8.0125" type="appl" />
         <tli id="T11" time="8.655" type="appl" />
         <tli id="T12" time="9.2975" type="appl" />
         <tli id="T13" time="9.94" type="appl" />
         <tli id="T14" time="10.83" type="appl" />
         <tli id="T15" time="11.757142857142858" type="appl" />
         <tli id="T16" time="12.684285714285714" type="appl" />
         <tli id="T17" time="13.611428571428572" type="appl" />
         <tli id="T18" time="14.538571428571428" type="appl" />
         <tli id="T19" time="15.465714285714286" type="appl" />
         <tli id="T20" time="16.392857142857142" type="appl" />
         <tli id="T21" time="17.32" type="appl" />
         <tli id="T22" time="18.34" type="appl" />
         <tli id="T23" time="19.2" type="appl" />
         <tli id="T24" time="20.06" type="appl" />
         <tli id="T25" time="20.92" type="appl" />
         <tli id="T26" time="21.78" type="appl" />
         <tli id="T27" time="22.64" type="appl" />
         <tli id="T28" time="23.5" type="appl" />
         <tli id="T29" time="25.32" type="appl" />
         <tli id="T30" time="25.72" type="appl" />
         <tli id="T31" time="26.365" type="appl" />
         <tli id="T32" time="27.293" type="appl" />
         <tli id="T33" time="28.221" type="appl" />
         <tli id="T34" time="29.149" type="appl" />
         <tli id="T35" time="30.076999999999998" type="appl" />
         <tli id="T36" time="31.005" type="appl" />
         <tli id="T37" time="31.4" type="appl" />
         <tli id="T38" time="32.302499999999995" type="appl" />
         <tli id="T39" time="33.205" type="appl" />
         <tli id="T40" time="34.1075" type="appl" />
         <tli id="T41" time="35.01" type="appl" />
         <tli id="T42" time="35.14" type="appl" />
         <tli id="T43" time="36.2025" type="appl" />
         <tli id="T44" time="37.265" type="appl" />
         <tli id="T45" time="38.3275" type="appl" />
         <tli id="T46" time="39.39" type="appl" />
         <tli id="T47" time="40.22" type="appl" />
         <tli id="T48" time="41.43666666666667" type="appl" />
         <tli id="T49" time="42.653333333333336" type="appl" />
         <tli id="T50" time="43.870000000000005" type="appl" />
         <tli id="T51" time="45.086666666666666" type="appl" />
         <tli id="T52" time="46.303333333333335" type="appl" />
         <tli id="T53" time="47.52" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T13" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Kros</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">n-</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">mămbi:</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_14" n="HIAT:ip">"</nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Šindinədə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">iʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">măŋgaʔ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T7">učitelʼ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">Onʼiʔ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">učitelʼ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">šiʔ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">Kros</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T21" id="Seg_43" n="sc" s="T14">
               <ts e="T21" id="Seg_45" n="HIAT:u" s="T14">
                  <nts id="Seg_46" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">Šindi-</ts>
                  <nts id="Seg_49" n="HIAT:ip">)</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">šindinə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">dʼügən</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">iʔ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">numəjleʔ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_64" n="HIAT:w" s="T19">šiʔ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">abam</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T28" id="Seg_70" n="sc" s="T22">
               <ts e="T28" id="Seg_72" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_74" n="HIAT:w" s="T22">Onʼiʔ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_76" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">aba=</ts>
                  <nts id="Seg_79" n="HIAT:ip">)</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">aba</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_85" n="HIAT:w" s="T25">šiʔ</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">nʼuʔtən</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_92" n="HIAT:w" s="T27">amnolaʔbə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_95" n="sc" s="T29">
               <ts e="T30" id="Seg_97" n="HIAT:u" s="T29">
                  <nts id="Seg_98" n="HIAT:ip">(</nts>
                  <nts id="Seg_99" n="HIAT:ip">(</nts>
                  <ats e="T30" id="Seg_100" n="HIAT:non-pho" s="T29">BRK</ats>
                  <nts id="Seg_101" n="HIAT:ip">)</nts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_105" n="sc" s="T31">
               <ts e="T36" id="Seg_107" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">Măn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">ugandə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_115" n="HIAT:w" s="T33">iʔgö</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_118" n="HIAT:w" s="T34">šiʔnʼileʔ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_121" n="HIAT:w" s="T35">dʼăbaktərbiam</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_124" n="sc" s="T37">
               <ts e="T41" id="Seg_126" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_128" n="HIAT:w" s="T37">Jakše</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_131" n="HIAT:w" s="T38">dʼăbaktərbiam</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_135" n="HIAT:w" s="T39">ej</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">jakše</ts>
                  <nts id="Seg_139" n="HIAT:ip">?</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_141" n="sc" s="T42">
               <ts e="T46" id="Seg_143" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">Iʔgö</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T43">ugandə</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">kudaj</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">dʼăbaktərbiam</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_158" n="sc" s="T47">
               <ts e="T53" id="Seg_160" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_162" n="HIAT:w" s="T47">Skazkaʔi</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_165" n="HIAT:w" s="T48">dʼăbaktərbiam</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_169" n="HIAT:w" s="T49">zagadkaʔi</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_172" n="HIAT:w" s="T50">dʼăbaktərbiam</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_176" n="HIAT:w" s="T51">bar</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_179" n="HIAT:w" s="T52">dʼăbaktərbiam</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T13" id="Seg_182" n="sc" s="T1">
               <ts e="T2" id="Seg_184" n="e" s="T1">Kros </ts>
               <ts e="T3" id="Seg_186" n="e" s="T2">(n-) </ts>
               <ts e="T4" id="Seg_188" n="e" s="T3">mămbi: </ts>
               <ts e="T5" id="Seg_190" n="e" s="T4">"Šindinədə </ts>
               <ts e="T6" id="Seg_192" n="e" s="T5">iʔ </ts>
               <ts e="T7" id="Seg_194" n="e" s="T6">măŋgaʔ </ts>
               <ts e="T9" id="Seg_196" n="e" s="T7">učitelʼ. </ts>
               <ts e="T10" id="Seg_198" n="e" s="T9">Onʼiʔ </ts>
               <ts e="T11" id="Seg_200" n="e" s="T10">učitelʼ </ts>
               <ts e="T12" id="Seg_202" n="e" s="T11">šiʔ </ts>
               <ts e="T13" id="Seg_204" n="e" s="T12">Kros. </ts>
            </ts>
            <ts e="T21" id="Seg_205" n="sc" s="T14">
               <ts e="T15" id="Seg_207" n="e" s="T14">(Šindi-) </ts>
               <ts e="T16" id="Seg_209" n="e" s="T15">šindinə </ts>
               <ts e="T17" id="Seg_211" n="e" s="T16">dʼügən </ts>
               <ts e="T18" id="Seg_213" n="e" s="T17">iʔ </ts>
               <ts e="T19" id="Seg_215" n="e" s="T18">numəjleʔ </ts>
               <ts e="T20" id="Seg_217" n="e" s="T19">šiʔ </ts>
               <ts e="T21" id="Seg_219" n="e" s="T20">abam. </ts>
            </ts>
            <ts e="T28" id="Seg_220" n="sc" s="T22">
               <ts e="T23" id="Seg_222" n="e" s="T22">Onʼiʔ </ts>
               <ts e="T24" id="Seg_224" n="e" s="T23">(aba=) </ts>
               <ts e="T25" id="Seg_226" n="e" s="T24">aba </ts>
               <ts e="T26" id="Seg_228" n="e" s="T25">šiʔ, </ts>
               <ts e="T27" id="Seg_230" n="e" s="T26">nʼuʔtən </ts>
               <ts e="T28" id="Seg_232" n="e" s="T27">amnolaʔbə. </ts>
            </ts>
            <ts e="T30" id="Seg_233" n="sc" s="T29">
               <ts e="T30" id="Seg_235" n="e" s="T29">((BRK)). </ts>
            </ts>
            <ts e="T36" id="Seg_236" n="sc" s="T31">
               <ts e="T32" id="Seg_238" n="e" s="T31">Măn </ts>
               <ts e="T33" id="Seg_240" n="e" s="T32">ugandə </ts>
               <ts e="T34" id="Seg_242" n="e" s="T33">iʔgö </ts>
               <ts e="T35" id="Seg_244" n="e" s="T34">šiʔnʼileʔ </ts>
               <ts e="T36" id="Seg_246" n="e" s="T35">dʼăbaktərbiam. </ts>
            </ts>
            <ts e="T41" id="Seg_247" n="sc" s="T37">
               <ts e="T38" id="Seg_249" n="e" s="T37">Jakše </ts>
               <ts e="T39" id="Seg_251" n="e" s="T38">dʼăbaktərbiam, </ts>
               <ts e="T40" id="Seg_253" n="e" s="T39">ej </ts>
               <ts e="T41" id="Seg_255" n="e" s="T40">jakše? </ts>
            </ts>
            <ts e="T46" id="Seg_256" n="sc" s="T42">
               <ts e="T43" id="Seg_258" n="e" s="T42">Iʔgö </ts>
               <ts e="T44" id="Seg_260" n="e" s="T43">ugandə, </ts>
               <ts e="T45" id="Seg_262" n="e" s="T44">kudaj </ts>
               <ts e="T46" id="Seg_264" n="e" s="T45">dʼăbaktərbiam. </ts>
            </ts>
            <ts e="T53" id="Seg_265" n="sc" s="T47">
               <ts e="T48" id="Seg_267" n="e" s="T47">Skazkaʔi </ts>
               <ts e="T49" id="Seg_269" n="e" s="T48">dʼăbaktərbiam, </ts>
               <ts e="T50" id="Seg_271" n="e" s="T49">zagadkaʔi </ts>
               <ts e="T51" id="Seg_273" n="e" s="T50">dʼăbaktərbiam, </ts>
               <ts e="T52" id="Seg_275" n="e" s="T51">bar </ts>
               <ts e="T53" id="Seg_277" n="e" s="T52">dʼăbaktərbiam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_278" s="T1">PKZ_196X_SU0220_rest2_misc.001 (001)</ta>
            <ta e="T13" id="Seg_279" s="T9">PKZ_196X_SU0220_rest2_misc.002 (002)</ta>
            <ta e="T21" id="Seg_280" s="T14">PKZ_196X_SU0220_rest2_misc.003 (003)</ta>
            <ta e="T28" id="Seg_281" s="T22">PKZ_196X_SU0220_rest2_misc.004 (004)</ta>
            <ta e="T30" id="Seg_282" s="T29">PKZ_196X_SU0220_rest2_misc.005 (005)</ta>
            <ta e="T36" id="Seg_283" s="T31">PKZ_196X_SU0220_rest2_misc.006 (006)</ta>
            <ta e="T41" id="Seg_284" s="T37">PKZ_196X_SU0220_rest2_misc.007 (007)</ta>
            <ta e="T46" id="Seg_285" s="T42">PKZ_196X_SU0220_rest2_misc.008 (008)</ta>
            <ta e="T53" id="Seg_286" s="T47">PKZ_196X_SU0220_rest2_misc.009 (009)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_287" s="T1">Kros (n-) mămbi: "Šindinədə iʔ măŋgaʔ učitelʼ. </ta>
            <ta e="T13" id="Seg_288" s="T9">Onʼiʔ učitelʼ šiʔ Kros. </ta>
            <ta e="T21" id="Seg_289" s="T14">(Šindi-) šindinə dʼügən iʔ numəjleʔ šiʔ abam. </ta>
            <ta e="T28" id="Seg_290" s="T22">Onʼiʔ (aba=) aba šiʔ, nʼuʔtən amnolaʔbə. </ta>
            <ta e="T30" id="Seg_291" s="T29">((BRK)). </ta>
            <ta e="T36" id="Seg_292" s="T31">Măn ugandə iʔgö šiʔnʼileʔ dʼăbaktərbiam. </ta>
            <ta e="T41" id="Seg_293" s="T37">Jakše dʼăbaktərbiam, ej jakše? </ta>
            <ta e="T46" id="Seg_294" s="T42">Iʔgö ugandə, kudaj dʼăbaktərbiam. </ta>
            <ta e="T53" id="Seg_295" s="T47">Skazkaʔi dʼăbaktərbiam, zagadkaʔi dʼăbaktərbiam, bar dʼăbaktərbiam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_296" s="T1">kros</ta>
            <ta e="T4" id="Seg_297" s="T3">măm-bi</ta>
            <ta e="T5" id="Seg_298" s="T4">šindi-nə=də</ta>
            <ta e="T6" id="Seg_299" s="T5">i-ʔ</ta>
            <ta e="T7" id="Seg_300" s="T6">măŋ-gaʔ</ta>
            <ta e="T9" id="Seg_301" s="T7">učitelʼ</ta>
            <ta e="T10" id="Seg_302" s="T9">onʼiʔ</ta>
            <ta e="T11" id="Seg_303" s="T10">učitelʼ</ta>
            <ta e="T12" id="Seg_304" s="T11">šiʔ</ta>
            <ta e="T13" id="Seg_305" s="T12">kros</ta>
            <ta e="T15" id="Seg_306" s="T14">šindi</ta>
            <ta e="T16" id="Seg_307" s="T15">šindi-nə</ta>
            <ta e="T17" id="Seg_308" s="T16">dʼü-gən</ta>
            <ta e="T18" id="Seg_309" s="T17">i-ʔ</ta>
            <ta e="T19" id="Seg_310" s="T18">numəj-leʔ</ta>
            <ta e="T20" id="Seg_311" s="T19">šiʔ</ta>
            <ta e="T21" id="Seg_312" s="T20">aba-m</ta>
            <ta e="T23" id="Seg_313" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_314" s="T23">aba</ta>
            <ta e="T25" id="Seg_315" s="T24">aba</ta>
            <ta e="T26" id="Seg_316" s="T25">šiʔ</ta>
            <ta e="T27" id="Seg_317" s="T26">nʼuʔtə-n</ta>
            <ta e="T28" id="Seg_318" s="T27">amno-laʔbə</ta>
            <ta e="T32" id="Seg_319" s="T31">măn</ta>
            <ta e="T33" id="Seg_320" s="T32">ugandə</ta>
            <ta e="T34" id="Seg_321" s="T33">iʔgö</ta>
            <ta e="T35" id="Seg_322" s="T34">šiʔnʼileʔ</ta>
            <ta e="T36" id="Seg_323" s="T35">dʼăbaktər-bia-m</ta>
            <ta e="T38" id="Seg_324" s="T37">jakše</ta>
            <ta e="T39" id="Seg_325" s="T38">dʼăbaktər-bia-m</ta>
            <ta e="T40" id="Seg_326" s="T39">ej</ta>
            <ta e="T41" id="Seg_327" s="T40">jakše</ta>
            <ta e="T43" id="Seg_328" s="T42">iʔgö</ta>
            <ta e="T44" id="Seg_329" s="T43">ugandə</ta>
            <ta e="T45" id="Seg_330" s="T44">kudaj</ta>
            <ta e="T46" id="Seg_331" s="T45">dʼăbaktər-bia-m</ta>
            <ta e="T48" id="Seg_332" s="T47">skazka-ʔi</ta>
            <ta e="T49" id="Seg_333" s="T48">dʼăbaktər-bia-m</ta>
            <ta e="T50" id="Seg_334" s="T49">zagadka-ʔi</ta>
            <ta e="T51" id="Seg_335" s="T50">dʼăbaktər-bia-m</ta>
            <ta e="T52" id="Seg_336" s="T51">bar</ta>
            <ta e="T53" id="Seg_337" s="T52">dʼăbaktər-bia-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_338" s="T1">kroːs</ta>
            <ta e="T4" id="Seg_339" s="T3">măn-bi</ta>
            <ta e="T5" id="Seg_340" s="T4">šində-Tə=də</ta>
            <ta e="T6" id="Seg_341" s="T5">e-ʔ</ta>
            <ta e="T7" id="Seg_342" s="T6">măn-KAʔ</ta>
            <ta e="T9" id="Seg_343" s="T7">učitelʼ</ta>
            <ta e="T10" id="Seg_344" s="T9">onʼiʔ</ta>
            <ta e="T11" id="Seg_345" s="T10">učitel'</ta>
            <ta e="T12" id="Seg_346" s="T11">šiʔ</ta>
            <ta e="T13" id="Seg_347" s="T12">kroːs</ta>
            <ta e="T15" id="Seg_348" s="T14">šində</ta>
            <ta e="T16" id="Seg_349" s="T15">šində-Tə</ta>
            <ta e="T17" id="Seg_350" s="T16">tʼo-Kən</ta>
            <ta e="T18" id="Seg_351" s="T17">e-ʔ</ta>
            <ta e="T19" id="Seg_352" s="T18">numəj-lAʔ</ta>
            <ta e="T20" id="Seg_353" s="T19">šiʔ</ta>
            <ta e="T21" id="Seg_354" s="T20">aba-m</ta>
            <ta e="T23" id="Seg_355" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_356" s="T23">aba</ta>
            <ta e="T25" id="Seg_357" s="T24">aba</ta>
            <ta e="T26" id="Seg_358" s="T25">šiʔ</ta>
            <ta e="T27" id="Seg_359" s="T26">nʼuʔdə-n</ta>
            <ta e="T28" id="Seg_360" s="T27">amno-laʔbə</ta>
            <ta e="T32" id="Seg_361" s="T31">măn</ta>
            <ta e="T33" id="Seg_362" s="T32">ugaːndə</ta>
            <ta e="T34" id="Seg_363" s="T33">iʔgö</ta>
            <ta e="T35" id="Seg_364" s="T34">šiʔnʼileʔ</ta>
            <ta e="T36" id="Seg_365" s="T35">tʼăbaktər-bi-m</ta>
            <ta e="T38" id="Seg_366" s="T37">jakšə</ta>
            <ta e="T39" id="Seg_367" s="T38">tʼăbaktər-bi-m</ta>
            <ta e="T40" id="Seg_368" s="T39">ej</ta>
            <ta e="T41" id="Seg_369" s="T40">jakšə</ta>
            <ta e="T43" id="Seg_370" s="T42">iʔgö</ta>
            <ta e="T44" id="Seg_371" s="T43">ugaːndə</ta>
            <ta e="T45" id="Seg_372" s="T44">kudaj</ta>
            <ta e="T46" id="Seg_373" s="T45">tʼăbaktər-bi-m</ta>
            <ta e="T48" id="Seg_374" s="T47">skazka-jəʔ</ta>
            <ta e="T49" id="Seg_375" s="T48">tʼăbaktər-bi-m</ta>
            <ta e="T50" id="Seg_376" s="T49">zagadka-jəʔ</ta>
            <ta e="T51" id="Seg_377" s="T50">tʼăbaktər-bi-m</ta>
            <ta e="T52" id="Seg_378" s="T51">bar</ta>
            <ta e="T53" id="Seg_379" s="T52">tʼăbaktər-bi-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_380" s="T1">Christ.[NOM.SG]</ta>
            <ta e="T4" id="Seg_381" s="T3">say-PST.[3SG]</ta>
            <ta e="T5" id="Seg_382" s="T4">who-LAT=INDEF</ta>
            <ta e="T6" id="Seg_383" s="T5">NEG.AUX-IMP.2SG</ta>
            <ta e="T7" id="Seg_384" s="T6">say-CNG</ta>
            <ta e="T9" id="Seg_385" s="T7">teacher.[NOM.SG]</ta>
            <ta e="T10" id="Seg_386" s="T9">one.[NOM.SG]</ta>
            <ta e="T11" id="Seg_387" s="T10">teacher.[NOM.SG]</ta>
            <ta e="T12" id="Seg_388" s="T11">you.PL.NOM</ta>
            <ta e="T13" id="Seg_389" s="T12">Christ.[NOM.SG]</ta>
            <ta e="T15" id="Seg_390" s="T14">who.[NOM.SG]</ta>
            <ta e="T16" id="Seg_391" s="T15">who-LAT</ta>
            <ta e="T17" id="Seg_392" s="T16">earth-LOC</ta>
            <ta e="T18" id="Seg_393" s="T17">NEG.AUX-IMP.2SG</ta>
            <ta e="T19" id="Seg_394" s="T18">name-CNG</ta>
            <ta e="T20" id="Seg_395" s="T19">you.PL.NOM</ta>
            <ta e="T21" id="Seg_396" s="T20">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T23" id="Seg_397" s="T22">one.[NOM.SG]</ta>
            <ta e="T24" id="Seg_398" s="T23">father</ta>
            <ta e="T25" id="Seg_399" s="T24">father</ta>
            <ta e="T26" id="Seg_400" s="T25">you.PL.NOM</ta>
            <ta e="T27" id="Seg_401" s="T26">up-LOC.ADV</ta>
            <ta e="T28" id="Seg_402" s="T27">live-DUR.[3SG]</ta>
            <ta e="T32" id="Seg_403" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_404" s="T32">very</ta>
            <ta e="T34" id="Seg_405" s="T33">many</ta>
            <ta e="T35" id="Seg_406" s="T34">you.PL.ACC</ta>
            <ta e="T36" id="Seg_407" s="T35">speak-PST-1SG</ta>
            <ta e="T38" id="Seg_408" s="T37">good</ta>
            <ta e="T39" id="Seg_409" s="T38">speak-PST-1SG</ta>
            <ta e="T40" id="Seg_410" s="T39">NEG</ta>
            <ta e="T41" id="Seg_411" s="T40">good</ta>
            <ta e="T43" id="Seg_412" s="T42">many</ta>
            <ta e="T44" id="Seg_413" s="T43">very</ta>
            <ta e="T45" id="Seg_414" s="T44">God.[NOM.SG]</ta>
            <ta e="T46" id="Seg_415" s="T45">speak-PST-1SG</ta>
            <ta e="T48" id="Seg_416" s="T47">tale-PL</ta>
            <ta e="T49" id="Seg_417" s="T48">speak-PST-1SG</ta>
            <ta e="T50" id="Seg_418" s="T49">riddle-PL</ta>
            <ta e="T51" id="Seg_419" s="T50">speak-PST-1SG</ta>
            <ta e="T52" id="Seg_420" s="T51">all</ta>
            <ta e="T53" id="Seg_421" s="T52">speak-PST-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_422" s="T1">Христос.[NOM.SG]</ta>
            <ta e="T4" id="Seg_423" s="T3">сказать-PST.[3SG]</ta>
            <ta e="T5" id="Seg_424" s="T4">кто-LAT=INDEF</ta>
            <ta e="T6" id="Seg_425" s="T5">NEG.AUX-IMP.2SG</ta>
            <ta e="T7" id="Seg_426" s="T6">сказать-CNG</ta>
            <ta e="T9" id="Seg_427" s="T7">учитель.[NOM.SG]</ta>
            <ta e="T10" id="Seg_428" s="T9">один.[NOM.SG]</ta>
            <ta e="T11" id="Seg_429" s="T10">учитель.[NOM.SG]</ta>
            <ta e="T12" id="Seg_430" s="T11">вы.NOM</ta>
            <ta e="T13" id="Seg_431" s="T12">Христос.[NOM.SG]</ta>
            <ta e="T15" id="Seg_432" s="T14">кто.[NOM.SG]</ta>
            <ta e="T16" id="Seg_433" s="T15">кто-LAT</ta>
            <ta e="T17" id="Seg_434" s="T16">земля-LOC</ta>
            <ta e="T18" id="Seg_435" s="T17">NEG.AUX-IMP.2SG</ta>
            <ta e="T19" id="Seg_436" s="T18">называть-CNG</ta>
            <ta e="T20" id="Seg_437" s="T19">вы.NOM</ta>
            <ta e="T21" id="Seg_438" s="T20">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T23" id="Seg_439" s="T22">один.[NOM.SG]</ta>
            <ta e="T24" id="Seg_440" s="T23">отец</ta>
            <ta e="T25" id="Seg_441" s="T24">отец</ta>
            <ta e="T26" id="Seg_442" s="T25">вы.NOM</ta>
            <ta e="T27" id="Seg_443" s="T26">вверх-LOC.ADV</ta>
            <ta e="T28" id="Seg_444" s="T27">жить-DUR.[3SG]</ta>
            <ta e="T32" id="Seg_445" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_446" s="T32">очень</ta>
            <ta e="T34" id="Seg_447" s="T33">много</ta>
            <ta e="T35" id="Seg_448" s="T34">вы.ACC</ta>
            <ta e="T36" id="Seg_449" s="T35">говорить-PST-1SG</ta>
            <ta e="T38" id="Seg_450" s="T37">хороший</ta>
            <ta e="T39" id="Seg_451" s="T38">говорить-PST-1SG</ta>
            <ta e="T40" id="Seg_452" s="T39">NEG</ta>
            <ta e="T41" id="Seg_453" s="T40">хороший</ta>
            <ta e="T43" id="Seg_454" s="T42">много</ta>
            <ta e="T44" id="Seg_455" s="T43">очень</ta>
            <ta e="T45" id="Seg_456" s="T44">бог.[NOM.SG]</ta>
            <ta e="T46" id="Seg_457" s="T45">говорить-PST-1SG</ta>
            <ta e="T48" id="Seg_458" s="T47">сказка-PL</ta>
            <ta e="T49" id="Seg_459" s="T48">говорить-PST-1SG</ta>
            <ta e="T50" id="Seg_460" s="T49">загадка-PL</ta>
            <ta e="T51" id="Seg_461" s="T50">говорить-PST-1SG</ta>
            <ta e="T52" id="Seg_462" s="T51">весь</ta>
            <ta e="T53" id="Seg_463" s="T52">говорить-PST-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_464" s="T1">propr.[n:case]</ta>
            <ta e="T4" id="Seg_465" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_466" s="T4">que-n:case=ptcl</ta>
            <ta e="T6" id="Seg_467" s="T5">aux-v:mood.pn</ta>
            <ta e="T7" id="Seg_468" s="T6">v-v:mood.pn</ta>
            <ta e="T9" id="Seg_469" s="T7">n.[n:case]</ta>
            <ta e="T10" id="Seg_470" s="T9">num.[n:case]</ta>
            <ta e="T11" id="Seg_471" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_472" s="T11">pers</ta>
            <ta e="T13" id="Seg_473" s="T12">propr.[n:case]</ta>
            <ta e="T15" id="Seg_474" s="T14">que.[n:case]</ta>
            <ta e="T16" id="Seg_475" s="T15">que-n:case</ta>
            <ta e="T17" id="Seg_476" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_477" s="T17">aux-v:mood.pn</ta>
            <ta e="T19" id="Seg_478" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_479" s="T19">pers</ta>
            <ta e="T21" id="Seg_480" s="T20">n-n:case.poss</ta>
            <ta e="T23" id="Seg_481" s="T22">num.[n:case]</ta>
            <ta e="T24" id="Seg_482" s="T23">n</ta>
            <ta e="T25" id="Seg_483" s="T24">n</ta>
            <ta e="T26" id="Seg_484" s="T25">pers</ta>
            <ta e="T27" id="Seg_485" s="T26">adv-n:case</ta>
            <ta e="T28" id="Seg_486" s="T27">v-v&gt;v.[v:pn]</ta>
            <ta e="T32" id="Seg_487" s="T31">pers</ta>
            <ta e="T33" id="Seg_488" s="T32">adv</ta>
            <ta e="T34" id="Seg_489" s="T33">quant</ta>
            <ta e="T35" id="Seg_490" s="T34">pers</ta>
            <ta e="T36" id="Seg_491" s="T35">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_492" s="T37">adj</ta>
            <ta e="T39" id="Seg_493" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_494" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_495" s="T40">adj</ta>
            <ta e="T43" id="Seg_496" s="T42">quant</ta>
            <ta e="T44" id="Seg_497" s="T43">adv</ta>
            <ta e="T45" id="Seg_498" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_499" s="T45">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_500" s="T47">n-n:num</ta>
            <ta e="T49" id="Seg_501" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_502" s="T49">n-n:num</ta>
            <ta e="T51" id="Seg_503" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_504" s="T51">quant</ta>
            <ta e="T53" id="Seg_505" s="T52">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_506" s="T1">propr</ta>
            <ta e="T4" id="Seg_507" s="T3">v</ta>
            <ta e="T5" id="Seg_508" s="T4">que</ta>
            <ta e="T6" id="Seg_509" s="T5">aux</ta>
            <ta e="T7" id="Seg_510" s="T6">v</ta>
            <ta e="T9" id="Seg_511" s="T7">n</ta>
            <ta e="T10" id="Seg_512" s="T9">num</ta>
            <ta e="T11" id="Seg_513" s="T10">n</ta>
            <ta e="T12" id="Seg_514" s="T11">pers</ta>
            <ta e="T13" id="Seg_515" s="T12">propr</ta>
            <ta e="T15" id="Seg_516" s="T14">que</ta>
            <ta e="T16" id="Seg_517" s="T15">que</ta>
            <ta e="T17" id="Seg_518" s="T16">n</ta>
            <ta e="T18" id="Seg_519" s="T17">aux</ta>
            <ta e="T19" id="Seg_520" s="T18">v</ta>
            <ta e="T20" id="Seg_521" s="T19">pers</ta>
            <ta e="T21" id="Seg_522" s="T20">n</ta>
            <ta e="T23" id="Seg_523" s="T22">num</ta>
            <ta e="T24" id="Seg_524" s="T23">n</ta>
            <ta e="T25" id="Seg_525" s="T24">n</ta>
            <ta e="T26" id="Seg_526" s="T25">pers</ta>
            <ta e="T27" id="Seg_527" s="T26">adv</ta>
            <ta e="T28" id="Seg_528" s="T27">v</ta>
            <ta e="T32" id="Seg_529" s="T31">pers</ta>
            <ta e="T33" id="Seg_530" s="T32">adv</ta>
            <ta e="T34" id="Seg_531" s="T33">quant</ta>
            <ta e="T35" id="Seg_532" s="T34">pers</ta>
            <ta e="T36" id="Seg_533" s="T35">v</ta>
            <ta e="T38" id="Seg_534" s="T37">adj</ta>
            <ta e="T39" id="Seg_535" s="T38">v</ta>
            <ta e="T40" id="Seg_536" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_537" s="T40">adj</ta>
            <ta e="T43" id="Seg_538" s="T42">quant</ta>
            <ta e="T44" id="Seg_539" s="T43">adv</ta>
            <ta e="T45" id="Seg_540" s="T44">n</ta>
            <ta e="T46" id="Seg_541" s="T45">v</ta>
            <ta e="T48" id="Seg_542" s="T47">n</ta>
            <ta e="T49" id="Seg_543" s="T48">v</ta>
            <ta e="T50" id="Seg_544" s="T49">n</ta>
            <ta e="T51" id="Seg_545" s="T50">v</ta>
            <ta e="T52" id="Seg_546" s="T51">quant</ta>
            <ta e="T53" id="Seg_547" s="T52">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_548" s="T1">np.h:A</ta>
            <ta e="T5" id="Seg_549" s="T4">pro.h:R</ta>
            <ta e="T6" id="Seg_550" s="T5">0.2.h:A</ta>
            <ta e="T9" id="Seg_551" s="T7">np.h:Th</ta>
            <ta e="T16" id="Seg_552" s="T15">pro.h:R</ta>
            <ta e="T17" id="Seg_553" s="T16">np:L</ta>
            <ta e="T18" id="Seg_554" s="T17">0.2.h:A</ta>
            <ta e="T20" id="Seg_555" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_556" s="T20">np.h:Th</ta>
            <ta e="T26" id="Seg_557" s="T25">pro.h:Poss</ta>
            <ta e="T27" id="Seg_558" s="T26">adv:L</ta>
            <ta e="T28" id="Seg_559" s="T27">0.3:E</ta>
            <ta e="T32" id="Seg_560" s="T31">pro.h:A</ta>
            <ta e="T35" id="Seg_561" s="T34">pro.h:R</ta>
            <ta e="T39" id="Seg_562" s="T38">0.1.h:A</ta>
            <ta e="T45" id="Seg_563" s="T44">np:R</ta>
            <ta e="T46" id="Seg_564" s="T45">0.1.h:A</ta>
            <ta e="T48" id="Seg_565" s="T47">np:Th</ta>
            <ta e="T49" id="Seg_566" s="T48">0.1.h:A</ta>
            <ta e="T50" id="Seg_567" s="T49">np:Th</ta>
            <ta e="T51" id="Seg_568" s="T50">0.1.h:A</ta>
            <ta e="T52" id="Seg_569" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_570" s="T52">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_571" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_572" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_573" s="T5">v:pred 0.2.h:S</ta>
            <ta e="T9" id="Seg_574" s="T7">np.h:O</ta>
            <ta e="T11" id="Seg_575" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_576" s="T12">n:pred</ta>
            <ta e="T18" id="Seg_577" s="T17">v:pred 0.2.h:S</ta>
            <ta e="T21" id="Seg_578" s="T20">np.h:O</ta>
            <ta e="T25" id="Seg_579" s="T24">n:pred</ta>
            <ta e="T26" id="Seg_580" s="T25">pro.h:S</ta>
            <ta e="T28" id="Seg_581" s="T27">v:pred 0.3:S</ta>
            <ta e="T32" id="Seg_582" s="T31">pro.h:S</ta>
            <ta e="T35" id="Seg_583" s="T34">pro.h:O</ta>
            <ta e="T36" id="Seg_584" s="T35">v:pred</ta>
            <ta e="T39" id="Seg_585" s="T38">v:pred 0.1.h:S</ta>
            <ta e="T40" id="Seg_586" s="T39">ptcl.neg</ta>
            <ta e="T46" id="Seg_587" s="T45">v:pred 0.1.h:S</ta>
            <ta e="T48" id="Seg_588" s="T47">np:O</ta>
            <ta e="T49" id="Seg_589" s="T48">v:pred 0.1.h:S</ta>
            <ta e="T50" id="Seg_590" s="T49">np:O</ta>
            <ta e="T51" id="Seg_591" s="T50">v:pred 0.1.h:S</ta>
            <ta e="T52" id="Seg_592" s="T51">np:O</ta>
            <ta e="T53" id="Seg_593" s="T52">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_594" s="T1">TURK:cult</ta>
            <ta e="T5" id="Seg_595" s="T4">TURK:gram(INDEF)</ta>
            <ta e="T9" id="Seg_596" s="T7">RUS:cult</ta>
            <ta e="T11" id="Seg_597" s="T10">RUS:cult</ta>
            <ta e="T13" id="Seg_598" s="T12">TURK:cult</ta>
            <ta e="T21" id="Seg_599" s="T20">TURK:core</ta>
            <ta e="T24" id="Seg_600" s="T23">TURK:core</ta>
            <ta e="T25" id="Seg_601" s="T24">TURK:core</ta>
            <ta e="T36" id="Seg_602" s="T35">%TURK:core</ta>
            <ta e="T38" id="Seg_603" s="T37">TURK:core</ta>
            <ta e="T39" id="Seg_604" s="T38">%TURK:core</ta>
            <ta e="T41" id="Seg_605" s="T40">TURK:core</ta>
            <ta e="T46" id="Seg_606" s="T45">%TURK:core</ta>
            <ta e="T48" id="Seg_607" s="T47">RUS:core</ta>
            <ta e="T49" id="Seg_608" s="T48">%TURK:core</ta>
            <ta e="T50" id="Seg_609" s="T49">RUS:core</ta>
            <ta e="T51" id="Seg_610" s="T50">%TURK:core</ta>
            <ta e="T52" id="Seg_611" s="T51">TURK:disc</ta>
            <ta e="T53" id="Seg_612" s="T52">%TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_613" s="T1">Христос сказал: «Не называйте никого учителем.</ta>
            <ta e="T13" id="Seg_614" s="T9">Ваш единственный учитель Христос.</ta>
            <ta e="T21" id="Seg_615" s="T14">Не называйте никого на земле "мой отец".</ta>
            <ta e="T28" id="Seg_616" s="T22">Один отец у вас, [который] живёт на небесах».</ta>
            <ta e="T36" id="Seg_617" s="T31">Я с вами очень много говорила.</ta>
            <ta e="T41" id="Seg_618" s="T37">Хорошо я говорила, плохо?</ta>
            <ta e="T46" id="Seg_619" s="T42">Очень много, я говорила с Богом.</ta>
            <ta e="T53" id="Seg_620" s="T47">Сказки рассказывала, загадки рассказывала, обо всём рассказывала.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_621" s="T1">Christ said: “Don't call anyone teacher. </ta>
            <ta e="T13" id="Seg_622" s="T9">Your only teacher ist Christ.</ta>
            <ta e="T21" id="Seg_623" s="T14">Don't call anyone on earth "my father".</ta>
            <ta e="T28" id="Seg_624" s="T22">You have one father, [he] lives in heaven.”</ta>
            <ta e="T36" id="Seg_625" s="T31">I spoke with you very much.</ta>
            <ta e="T41" id="Seg_626" s="T37">Was I speaking good or not?</ta>
            <ta e="T46" id="Seg_627" s="T42">Very much; I spoke to God.</ta>
            <ta e="T53" id="Seg_628" s="T47">I told tales, I told riddles, I told everything.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_629" s="T1">Christus sagte: „Nenne niemanden Lehrer.</ta>
            <ta e="T13" id="Seg_630" s="T9">Du hast einen Lehrer, Christus.</ta>
            <ta e="T21" id="Seg_631" s="T14">Nenne niemanden auf der Erde "meinen Vater".</ta>
            <ta e="T28" id="Seg_632" s="T22">Du hast einen Vater, [er] lebt im Himmel.“</ta>
            <ta e="T36" id="Seg_633" s="T31">Ich habe sehr viel mit dir gesprochen.</ta>
            <ta e="T41" id="Seg_634" s="T37">Habe ich gut gesprochen oder nicht?</ta>
            <ta e="T46" id="Seg_635" s="T42">Sehr viel, ich habe zu Gott gesprochen.</ta>
            <ta e="T53" id="Seg_636" s="T47">Ich habe Märchen erzählt, ich habe Rätsel erzählt, ich habe alles erzählt.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
