<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID81BE15F3-6C19-4B02-C85F-F9BD58C0D265">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0221.wav" />
         <referenced-file url="PKZ_196X_SU0221.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0221\PKZ_196X_SU0221.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1503</ud-information>
            <ud-information attribute-name="# HIAT:w">788</ud-information>
            <ud-information attribute-name="# e">839</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">58</ud-information>
            <ud-information attribute-name="# HIAT:u">194</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.2" type="appl" />
         <tli id="T1" time="1.083" type="appl" />
         <tli id="T2" time="1.967" type="appl" />
         <tli id="T3" time="4.966628233839904" />
         <tli id="T4" time="6.165" type="appl" />
         <tli id="T5" time="7.17" type="appl" />
         <tli id="T6" time="8.402" type="appl" />
         <tli id="T7" time="9.245" type="appl" />
         <tli id="T850" time="9.605857142857142" type="intp" />
         <tli id="T8" time="10.087" type="appl" />
         <tli id="T9" time="10.93" type="appl" />
         <tli id="T10" time="11.795" type="appl" />
         <tli id="T11" time="12.45" type="appl" />
         <tli id="T12" time="13.105" type="appl" />
         <tli id="T13" time="13.76" type="appl" />
         <tli id="T14" time="14.552" type="appl" />
         <tli id="T15" time="15.308" type="appl" />
         <tli id="T16" time="16.065" type="appl" />
         <tli id="T17" time="18.265" type="appl" />
         <tli id="T18" time="19.144" type="appl" />
         <tli id="T19" time="19.838" type="appl" />
         <tli id="T20" time="20.532" type="appl" />
         <tli id="T21" time="21.226" type="appl" />
         <tli id="T22" time="21.919" type="appl" />
         <tli id="T23" time="22.613" type="appl" />
         <tli id="T24" time="23.307" type="appl" />
         <tli id="T25" time="24.001" type="appl" />
         <tli id="T26" time="24.993139931095037" />
         <tli id="T27" time="25.784" type="appl" />
         <tli id="T28" time="26.548" type="appl" />
         <tli id="T29" time="27.312" type="appl" />
         <tli id="T30" time="28.076" type="appl" />
         <tli id="T31" time="29.133107895141453" />
         <tli id="T32" time="31.12642580375639" />
         <tli id="T33" time="32.248" type="appl" />
         <tli id="T34" time="33.096" type="appl" />
         <tli id="T35" time="33.944" type="appl" />
         <tli id="T36" time="34.792" type="appl" />
         <tli id="T37" time="35.64" type="appl" />
         <tli id="T38" time="36.592" type="appl" />
         <tli id="T39" time="37.355" type="appl" />
         <tli id="T40" time="38.118" type="appl" />
         <tli id="T41" time="38.88" type="appl" />
         <tli id="T42" time="39.642" type="appl" />
         <tli id="T43" time="40.23302200164271" />
         <tli id="T44" time="41.83300962059785" />
         <tli id="T45" time="42.616" type="appl" />
         <tli id="T46" time="43.36" type="appl" />
         <tli id="T47" time="44.104" type="appl" />
         <tli id="T48" time="44.848" type="appl" />
         <tli id="T49" time="45.592" type="appl" />
         <tli id="T50" time="46.582" type="appl" />
         <tli id="T51" time="47.334" type="appl" />
         <tli id="T52" time="48.086" type="appl" />
         <tli id="T53" time="48.838" type="appl" />
         <tli id="T54" time="49.591" type="appl" />
         <tli id="T55" time="50.343" type="appl" />
         <tli id="T56" time="51.095" type="appl" />
         <tli id="T57" time="51.847" type="appl" />
         <tli id="T58" time="52.599" type="appl" />
         <tli id="T59" time="53.759" type="appl" />
         <tli id="T60" time="54.732" type="appl" />
         <tli id="T61" time="55.561" type="appl" />
         <tli id="T62" time="56.389" type="appl" />
         <tli id="T63" time="57.217" type="appl" />
         <tli id="T64" time="58.046" type="appl" />
         <tli id="T65" time="58.874" type="appl" />
         <tli id="T66" time="60.2728669290558" />
         <tli id="T67" time="61.15" type="appl" />
         <tli id="T68" time="61.9" type="appl" />
         <tli id="T69" time="62.649" type="appl" />
         <tli id="T70" time="63.399" type="appl" />
         <tli id="T71" time="64.102" type="appl" />
         <tli id="T72" time="64.749" type="appl" />
         <tli id="T73" time="65.396" type="appl" />
         <tli id="T74" time="66.044" type="appl" />
         <tli id="T75" time="68.18613902780476" />
         <tli id="T76" time="69.174" type="appl" />
         <tli id="T77" time="69.924" type="appl" />
         <tli id="T78" time="70.674" type="appl" />
         <tli id="T79" time="71.424" type="appl" />
         <tli id="T80" time="72.174" type="appl" />
         <tli id="T81" time="72.924" type="appl" />
         <tli id="T82" time="74.85942072186347" />
         <tli id="T83" time="75.861" type="appl" />
         <tli id="T84" time="76.447" type="appl" />
         <tli id="T85" time="77.034" type="appl" />
         <tli id="T86" time="77.621" type="appl" />
         <tli id="T87" time="78.207" type="appl" />
         <tli id="T88" time="78.95272238035702" />
         <tli id="T89" time="80.05938048346765" />
         <tli id="T90" time="80.792" type="appl" />
         <tli id="T91" time="81.446" type="appl" />
         <tli id="T92" time="82.099" type="appl" />
         <tli id="T93" time="82.752" type="appl" />
         <tli id="T94" time="83.406" type="appl" />
         <tli id="T95" time="84.059" type="appl" />
         <tli id="T96" time="85.062" type="appl" />
         <tli id="T97" time="85.864" type="appl" />
         <tli id="T98" time="86.666" type="appl" />
         <tli id="T99" time="87.469" type="appl" />
         <tli id="T100" time="88.049" type="appl" />
         <tli id="T101" time="91.56595810978669" />
         <tli id="T102" time="92.604" type="appl" />
         <tli id="T103" time="94.2259375262996" />
         <tli id="T104" time="94.947" type="appl" />
         <tli id="T105" time="95.671" type="appl" />
         <tli id="T106" time="96.394" type="appl" />
         <tli id="T107" time="97.117" type="appl" />
         <tli id="T108" time="97.841" type="appl" />
         <tli id="T109" time="98.67923639905807" />
         <tli id="T110" time="99.609" type="appl" />
         <tli id="T111" time="100.409" type="appl" />
         <tli id="T112" time="101.209" type="appl" />
         <tli id="T113" time="102.194" type="appl" />
         <tli id="T114" time="102.844" type="appl" />
         <tli id="T115" time="103.494" type="appl" />
         <tli id="T116" time="104.67252335506086" />
         <tli id="T117" time="105.116" type="appl" />
         <tli id="T118" time="105.682" type="appl" />
         <tli id="T119" time="106.249" type="appl" />
         <tli id="T120" time="106.816" type="appl" />
         <tli id="T121" time="107.382" type="appl" />
         <tli id="T122" time="108.17249627152522" />
         <tli id="T123" time="109.034" type="appl" />
         <tli id="T124" time="109.658" type="appl" />
         <tli id="T125" time="110.282" type="appl" />
         <tli id="T126" time="110.906" type="appl" />
         <tli id="T127" time="111.53" type="appl" />
         <tli id="T128" time="112.154" type="appl" />
         <tli id="T129" time="112.54579576333592" />
         <tli id="T130" time="115.26577471555964" />
         <tli id="T131" time="116.459" type="appl" />
         <tli id="T132" time="117.344" type="appl" />
         <tli id="T133" time="118.229" type="appl" />
         <tli id="T134" time="119.44574237007996" />
         <tli id="T135" time="122.53905176672654" />
         <tli id="T136" time="123.549" type="appl" />
         <tli id="T137" time="124.437" type="appl" />
         <tli id="T138" time="125.326" type="appl" />
         <tli id="T139" time="126.214" type="appl" />
         <tli id="T140" time="127.103" type="appl" />
         <tli id="T141" time="127.991" type="appl" />
         <tli id="T142" time="129.1323340798375" />
         <tli id="T143" time="130.215" type="appl" />
         <tli id="T144" time="131.015" type="appl" />
         <tli id="T145" time="131.72" type="appl" />
         <tli id="T146" time="132.425" type="appl" />
         <tli id="T147" time="133.13" type="appl" />
         <tli id="T148" time="134.83228997236517" />
         <tli id="T149" time="137.865599833301" />
         <tli id="T150" time="139.392" type="appl" />
         <tli id="T151" time="140.544" type="appl" />
         <tli id="T152" time="141.696" type="appl" />
         <tli id="T153" time="142.848" type="appl" />
         <tli id="T154" time="144.0" type="appl" />
         <tli id="T155" time="145.375" type="appl" />
         <tli id="T156" time="146.24" type="appl" />
         <tli id="T157" time="146.89" type="appl" />
         <tli id="T158" time="147.54" type="appl" />
         <tli id="T159" time="148.255" type="appl" />
         <tli id="T160" time="148.97" type="appl" />
         <tli id="T161" time="152.92548329671618" />
         <tli id="T162" time="154.033" type="appl" />
         <tli id="T163" time="154.927" type="appl" />
         <tli id="T164" time="155.82" type="appl" />
         <tli id="T165" time="156.713" type="appl" />
         <tli id="T166" time="157.607" type="appl" />
         <tli id="T167" time="158.66543887971775" />
         <tli id="T168" time="159.78543021298634" />
         <tli id="T169" time="161.077" type="appl" />
         <tli id="T170" time="161.889" type="appl" />
         <tli id="T171" time="162.701" type="appl" />
         <tli id="T172" time="163.513" type="appl" />
         <tli id="T173" time="164.38539461748235" />
         <tli id="T174" time="165.56" type="appl" />
         <tli id="T175" time="166.27" type="appl" />
         <tli id="T176" time="167.05870726415324" />
         <tli id="T177" time="167.741" type="appl" />
         <tli id="T178" time="168.538" type="appl" />
         <tli id="T179" time="169.334" type="appl" />
         <tli id="T180" time="170.13" type="appl" />
         <tli id="T181" time="171.34" type="appl" />
         <tli id="T182" time="172.55" type="appl" />
         <tli id="T183" time="173.76" type="appl" />
         <tli id="T184" time="175.26531042637728" />
         <tli id="T185" time="176.225" type="appl" />
         <tli id="T186" time="177.045" type="appl" />
         <tli id="T187" time="177.865" type="appl" />
         <tli id="T188" time="178.685" type="appl" />
         <tli id="T189" time="179.505" type="appl" />
         <tli id="T190" time="181.1785980014323" />
         <tli id="T191" time="182.754" type="appl" />
         <tli id="T192" time="184.028" type="appl" />
         <tli id="T193" time="185.302" type="appl" />
         <tli id="T194" time="186.576" type="appl" />
         <tli id="T195" time="187.85" type="appl" />
         <tli id="T196" time="189.124" type="appl" />
         <tli id="T197" time="190.398" type="appl" />
         <tli id="T198" time="191.672" type="appl" />
         <tli id="T199" time="192.946" type="appl" />
         <tli id="T200" time="194.63182723081343" />
         <tli id="T201" time="195.84515117518774" />
         <tli id="T202" time="196.842" type="appl" />
         <tli id="T203" time="197.644" type="appl" />
         <tli id="T204" time="198.446" type="appl" />
         <tli id="T205" time="199.248" type="appl" />
         <tli id="T206" time="200.1984508217615" />
         <tli id="T207" time="201.155" type="appl" />
         <tli id="T208" time="202.805" type="appl" />
         <tli id="T209" time="204.455" type="appl" />
         <tli id="T210" time="206.0517388611057" />
         <tli id="T211" time="207.01" type="appl" />
         <tli id="T212" time="208.3517210633537" />
         <tli id="T213" time="209.068" type="appl" />
         <tli id="T214" time="209.952" type="appl" />
         <tli id="T215" time="210.835" type="appl" />
         <tli id="T216" time="211.718" type="appl" />
         <tli id="T217" time="212.602" type="appl" />
         <tli id="T218" time="214.3516746344355" />
         <tli id="T219" time="215.095" type="appl" />
         <tli id="T220" time="215.99" type="appl" />
         <tli id="T221" time="216.485" type="appl" />
         <tli id="T222" time="217.563" type="appl" />
         <tli id="T223" time="218.521" type="appl" />
         <tli id="T224" time="219.479" type="appl" />
         <tli id="T225" time="220.437" type="appl" />
         <tli id="T226" time="221.395" type="appl" />
         <tli id="T227" time="222.69827671331814" />
         <tli id="T851" time="223.36137976870975" type="intp" />
         <tli id="T228" time="224.135" type="appl" />
         <tli id="T229" time="225.46" type="appl" />
         <tli id="T230" time="226.784" type="appl" />
         <tli id="T231" time="228.109" type="appl" />
         <tli id="T232" time="229.434" type="appl" />
         <tli id="T233" time="230.758" type="appl" />
         <tli id="T234" time="232.083" type="appl" />
         <tli id="T235" time="233.408" type="appl" />
         <tli id="T236" time="234.7848498515084" />
         <tli id="T237" time="235.831" type="appl" />
         <tli id="T238" time="236.531" type="appl" />
         <tli id="T239" time="237.232" type="appl" />
         <tli id="T240" time="237.932" type="appl" />
         <tli id="T241" time="238.633" type="appl" />
         <tli id="T242" time="239.333" type="appl" />
         <tli id="T243" time="240.034" type="appl" />
         <tli id="T244" time="240.734" type="appl" />
         <tli id="T245" time="241.435" type="appl" />
         <tli id="T246" time="243.01811947404838" />
         <tli id="T247" time="244.002" type="appl" />
         <tli id="T248" time="244.704" type="appl" />
         <tli id="T249" time="245.406" type="appl" />
         <tli id="T250" time="246.108" type="appl" />
         <tli id="T251" time="246.81" type="appl" />
         <tli id="T252" time="247.512" type="appl" />
         <tli id="T253" time="248.214" type="appl" />
         <tli id="T254" time="248.916" type="appl" />
         <tli id="T255" time="249.618" type="appl" />
         <tli id="T256" time="250.83805896169162" />
         <tli id="T257" time="251.859" type="appl" />
         <tli id="T258" time="252.778" type="appl" />
         <tli id="T259" time="253.696" type="appl" />
         <tli id="T260" time="254.615" type="appl" />
         <tli id="T261" time="255.534" type="appl" />
         <tli id="T262" time="256.452" type="appl" />
         <tli id="T263" time="257.371" type="appl" />
         <tli id="T264" time="258.29" type="appl" />
         <tli id="T265" time="258.953" type="appl" />
         <tli id="T266" time="259.606" type="appl" />
         <tli id="T267" time="260.26" type="appl" />
         <tli id="T268" time="260.913" type="appl" />
         <tli id="T269" time="261.566" type="appl" />
         <tli id="T270" time="262.32463674277363" />
         <tli id="T271" time="263.487" type="appl" />
         <tli id="T272" time="264.543" type="appl" />
         <tli id="T273" time="265.17128138149803" />
         <tli id="T274" time="266.36" type="appl" />
         <tli id="T275" time="267.12" type="appl" />
         <tli id="T276" time="267.88" type="appl" />
         <tli id="T277" time="268.75125367891013" />
         <tli id="T278" time="270.5712395954716" />
         <tli id="T279" time="271.798" type="appl" />
         <tli id="T280" time="272.726" type="appl" />
         <tli id="T281" time="273.654" type="appl" />
         <tli id="T282" time="274.582" type="appl" />
         <tli id="T283" time="275.51" type="appl" />
         <tli id="T284" time="276.87119084510744" />
         <tli id="T285" time="278.535" type="appl" />
         <tli id="T286" time="279.96" type="appl" />
         <tli id="T287" time="282.5644801225562" />
         <tli id="T288" time="283.882" type="appl" />
         <tli id="T289" time="284.715" type="appl" />
         <tli id="T290" time="285.548" type="appl" />
         <tli id="T291" time="286.38" type="appl" />
         <tli id="T292" time="287.04" type="appl" />
         <tli id="T293" time="287.58" type="appl" />
         <tli id="T294" time="288.1733429699812" />
         <tli id="T295" time="289.129" type="appl" />
         <tli id="T296" time="290.123" type="appl" />
         <tli id="T297" time="291.117" type="appl" />
         <tli id="T298" time="292.111" type="appl" />
         <tli id="T299" time="293.105" type="appl" />
         <tli id="T300" time="293.778" type="appl" />
         <tli id="T301" time="294.395" type="appl" />
         <tli id="T302" time="295.013" type="appl" />
         <tli id="T303" time="295.63" type="appl" />
         <tli id="T852" time="295.9152307692308" type="intp" />
         <tli id="T304" time="296.248" type="appl" />
         <tli id="T305" time="296.865" type="appl" />
         <tli id="T306" time="297.482" type="appl" />
         <tli id="T307" time="298.15769279228977" />
         <tli id="T308" time="299.343" type="appl" />
         <tli id="T309" time="300.327" type="appl" />
         <tli id="T310" time="301.31" type="appl" />
         <tli id="T311" time="302.293" type="appl" />
         <tli id="T312" time="303.277" type="appl" />
         <tli id="T313" time="304.650975879216" />
         <tli id="T314" time="305.675" type="appl" />
         <tli id="T315" time="306.565" type="appl" />
         <tli id="T316" time="307.67095250999387" />
         <tli id="T317" time="308.527" type="appl" />
         <tli id="T318" time="309.133" type="appl" />
         <tli id="T319" time="309.74" type="appl" />
         <tli id="T320" time="310.347" type="appl" />
         <tli id="T321" time="310.953" type="appl" />
         <tli id="T322" time="311.56" type="appl" />
         <tli id="T323" time="312.705" type="appl" />
         <tli id="T324" time="313.82" type="appl" />
         <tli id="T325" time="314.935" type="appl" />
         <tli id="T326" time="316.05" type="appl" />
         <tli id="T327" time="316.554" type="appl" />
         <tli id="T328" time="316.972" type="appl" />
         <tli id="T329" time="317.391" type="appl" />
         <tli id="T330" time="317.81" type="appl" />
         <tli id="T331" time="318.229" type="appl" />
         <tli id="T332" time="318.648" type="appl" />
         <tli id="T333" time="319.066" type="appl" />
         <tli id="T334" time="319.485" type="appl" />
         <tli id="T335" time="319.953" type="appl" />
         <tli id="T336" time="320.291" type="appl" />
         <tli id="T337" time="320.629" type="appl" />
         <tli id="T338" time="320.967" type="appl" />
         <tli id="T339" time="321.305" type="appl" />
         <tli id="T340" time="322.056" type="appl" />
         <tli id="T341" time="322.806" type="appl" />
         <tli id="T342" time="323.557" type="appl" />
         <tli id="T343" time="324.308" type="appl" />
         <tli id="T344" time="325.058" type="appl" />
         <tli id="T345" time="325.809" type="appl" />
         <tli id="T346" time="326.559" type="appl" />
         <tli id="T347" time="327.6574645167751" />
         <tli id="T348" time="329.65078242539005" />
         <tli id="T349" time="330.574" type="appl" />
         <tli id="T350" time="331.32" type="appl" />
         <tli id="T351" time="332.067" type="appl" />
         <tli id="T352" time="332.813" type="appl" />
         <tli id="T353" time="333.6066632722751" />
         <tli id="T354" time="334.308" type="appl" />
         <tli id="T355" time="335.056" type="appl" />
         <tli id="T356" time="335.803" type="appl" />
         <tli id="T357" time="336.551" type="appl" />
         <tli id="T358" time="337.299" type="appl" />
         <tli id="T359" time="338.047" type="appl" />
         <tli id="T360" time="338.794" type="appl" />
         <tli id="T361" time="339.542" type="appl" />
         <tli id="T362" time="340.4840319287321" />
         <tli id="T363" time="341.207" type="appl" />
         <tli id="T364" time="341.767" type="appl" />
         <tli id="T365" time="342.327" type="appl" />
         <tli id="T366" time="343.0906784246132" />
         <tli id="T367" time="344.009" type="appl" />
         <tli id="T368" time="344.885" type="appl" />
         <tli id="T369" time="345.762" type="appl" />
         <tli id="T370" time="346.282" type="appl" />
         <tli id="T371" time="346.691" type="appl" />
         <tli id="T372" time="347.101" type="appl" />
         <tli id="T373" time="347.511" type="appl" />
         <tli id="T374" time="347.921" type="appl" />
         <tli id="T375" time="348.33" type="appl" />
         <tli id="T376" time="348.700010018411" />
         <tli id="T377" time="349.504" type="appl" />
         <tli id="T378" time="350.266" type="appl" />
         <tli id="T379" time="351.028" type="appl" />
         <tli id="T380" time="352.05060909076195" />
         <tli id="T381" time="354.5105900549055" />
         <tli id="T382" time="355.426" type="appl" />
         <tli id="T383" time="356.126" type="appl" />
         <tli id="T384" time="356.826" type="appl" />
         <tli id="T385" time="357.526" type="appl" />
         <tli id="T386" time="358.128" type="appl" />
         <tli id="T387" time="358.725" type="appl" />
         <tli id="T388" time="359.322" type="appl" />
         <tli id="T389" time="359.92" type="appl" />
         <tli id="T390" time="360.518" type="appl" />
         <tli id="T391" time="361.115" type="appl" />
         <tli id="T392" time="361.712" type="appl" />
         <tli id="T393" time="362.31" type="appl" />
         <tli id="T394" time="363.4" type="appl" />
         <tli id="T395" time="365.1171746455623" />
         <tli id="T396" time="366.035" type="appl" />
         <tli id="T397" time="366.655" type="appl" />
         <tli id="T398" time="367.275" type="appl" />
         <tli id="T399" time="367.895" type="appl" />
         <tli id="T400" time="368.515" type="appl" />
         <tli id="T401" time="373.18377889112776" />
         <tli id="T402" time="373.675" type="appl" />
         <tli id="T403" time="374.11043838710594" />
         <tli id="T404" time="374.782" type="appl" />
         <tli id="T405" time="375.475" type="appl" />
         <tli id="T406" time="376.168" type="appl" />
         <tli id="T407" time="376.96666628500634" />
         <tli id="T408" time="378.099" type="appl" />
         <tli id="T409" time="379.047" type="appl" />
         <tli id="T410" time="379.996" type="appl" />
         <tli id="T411" time="380.945" type="appl" />
         <tli id="T412" time="381.893" type="appl" />
         <tli id="T413" time="382.842" type="appl" />
         <tli id="T414" time="383.791" type="appl" />
         <tli id="T415" time="384.739" type="appl" />
         <tli id="T416" time="385.688" type="appl" />
         <tli id="T417" time="386.637" type="appl" />
         <tli id="T418" time="387.585" type="appl" />
         <tli id="T419" time="388.534" type="appl" />
         <tli id="T420" time="389.483" type="appl" />
         <tli id="T421" time="390.431" type="appl" />
         <tli id="T422" time="392.49696277493206" />
         <tli id="T423" time="393.805" type="appl" />
         <tli id="T424" time="394.8569445128909" />
         <tli id="T425" time="397.8369214531948" />
         <tli id="T426" time="398.573" type="appl" />
         <tli id="T427" time="399.127" type="appl" />
         <tli id="T428" time="399.68" type="appl" />
         <tli id="T429" time="400.233" type="appl" />
         <tli id="T430" time="400.787" type="appl" />
         <tli id="T431" time="401.34" type="appl" />
         <tli id="T432" time="401.894" type="appl" />
         <tli id="T433" time="402.447" type="appl" />
         <tli id="T434" time="403.0" type="appl" />
         <tli id="T435" time="403.554" type="appl" />
         <tli id="T436" time="404.107" type="appl" />
         <tli id="T437" time="404.66" type="appl" />
         <tli id="T438" time="405.214" type="appl" />
         <tli id="T439" time="405.94352538923414" />
         <tli id="T440" time="406.743" type="appl" />
         <tli id="T441" time="407.537" type="appl" />
         <tli id="T442" time="408.33" type="appl" />
         <tli id="T443" time="409.11" type="appl" />
         <tli id="T444" time="409.72" type="appl" />
         <tli id="T445" time="410.33" type="appl" />
         <tli id="T446" time="410.94" type="appl" />
         <tli id="T447" time="411.55" type="appl" />
         <tli id="T448" time="412.16" type="appl" />
         <tli id="T449" time="412.77" type="appl" />
         <tli id="T450" time="413.38" type="appl" />
         <tli id="T451" time="413.753" type="appl" />
         <tli id="T452" time="414.071" type="appl" />
         <tli id="T453" time="414.389" type="appl" />
         <tli id="T454" time="414.707" type="appl" />
         <tli id="T455" time="415.2634532696478" />
         <tli id="T456" time="416.2234458410209" />
         <tli id="T457" time="417.077" type="appl" />
         <tli id="T458" time="417.924" type="appl" />
         <tli id="T459" time="418.772" type="appl" />
         <tli id="T460" time="419.619" type="appl" />
         <tli id="T461" time="420.466" type="appl" />
         <tli id="T462" time="421.313" type="appl" />
         <tli id="T463" time="422.161" type="appl" />
         <tli id="T464" time="423.008" type="appl" />
         <tli id="T465" time="423.9967190231113" />
         <tli id="T466" time="424.765" type="appl" />
         <tli id="T467" time="425.429" type="appl" />
         <tli id="T468" time="426.094" type="appl" />
         <tli id="T469" time="426.759" type="appl" />
         <tli id="T470" time="427.423" type="appl" />
         <tli id="T471" time="428.088" type="appl" />
         <tli id="T472" time="428.752" type="appl" />
         <tli id="T473" time="429.417" type="appl" />
         <tli id="T474" time="430.082" type="appl" />
         <tli id="T475" time="430.746" type="appl" />
         <tli id="T476" time="431.411" type="appl" />
         <tli id="T477" time="433.5499784312893" />
         <tli id="T478" time="434.754" type="appl" />
         <tli id="T479" time="435.519" type="appl" />
         <tli id="T480" time="436.283" type="appl" />
         <tli id="T481" time="437.047" type="appl" />
         <tli id="T482" time="437.811" type="appl" />
         <tli id="T483" time="438.576" type="appl" />
         <tli id="T484" time="439.34" type="appl" />
         <tli id="T485" time="439.974" type="appl" />
         <tli id="T486" time="440.607" type="appl" />
         <tli id="T487" time="441.241" type="appl" />
         <tli id="T488" time="441.875" type="appl" />
         <tli id="T489" time="442.508" type="appl" />
         <tli id="T490" time="443.142" type="appl" />
         <tli id="T491" time="443.775" type="appl" />
         <tli id="T492" time="444.409" type="appl" />
         <tli id="T493" time="445.043" type="appl" />
         <tli id="T494" time="445.676" type="appl" />
         <tli id="T495" time="449.2098572518126" />
         <tli id="T496" time="450.7" type="appl" />
         <tli id="T497" time="451.885" type="appl" />
         <tli id="T498" time="452.754" type="appl" />
         <tli id="T499" time="453.553" type="appl" />
         <tli id="T500" time="454.352" type="appl" />
         <tli id="T501" time="455.152" type="appl" />
         <tli id="T502" time="455.951" type="appl" />
         <tli id="T503" time="456.75" type="appl" />
         <tli id="T504" time="457.76" type="appl" />
         <tli id="T505" time="458.75" type="appl" />
         <tli id="T506" time="459.74" type="appl" />
         <tli id="T507" time="460.73" type="appl" />
         <tli id="T508" time="461.498" type="appl" />
         <tli id="T509" time="462.22" type="appl" />
         <tli id="T510" time="463.2964149136969" />
         <tli id="T511" time="464.26" type="appl" />
         <tli id="T512" time="465.08" type="appl" />
         <tli id="T513" time="465.9" type="appl" />
         <tli id="T514" time="466.72" type="appl" />
         <tli id="T515" time="467.54" type="appl" />
         <tli id="T516" time="468.36" type="appl" />
         <tli id="T517" time="469.18" type="appl" />
         <tli id="T518" time="470.0" type="appl" />
         <tli id="T519" time="470.82" type="appl" />
         <tli id="T520" time="473.103005694876" />
         <tli id="T521" time="473.998" type="appl" />
         <tli id="T522" time="474.596" type="appl" />
         <tli id="T523" time="475.194" type="appl" />
         <tli id="T524" time="475.792" type="appl" />
         <tli id="T525" time="476.2696478573913" />
         <tli id="T526" time="477.32" type="appl" />
         <tli id="T527" time="478.23" type="appl" />
         <tli id="T528" time="479.14" type="appl" />
         <tli id="T529" time="480.05" type="appl" />
         <tli id="T530" time="480.96" type="appl" />
         <tli id="T531" time="481.9900202585985" />
         <tli id="T532" time="482.919" type="appl" />
         <tli id="T533" time="483.938" type="appl" />
         <tli id="T534" time="484.957" type="appl" />
         <tli id="T535" time="485.976" type="appl" />
         <tli id="T536" time="486.995" type="appl" />
         <tli id="T537" time="488.014" type="appl" />
         <tli id="T538" time="489.033" type="appl" />
         <tli id="T539" time="490.052" type="appl" />
         <tli id="T540" time="491.071" type="appl" />
         <tli id="T541" time="492.09" type="appl" />
         <tli id="T542" time="494.74950485632326" />
         <tli id="T543" time="495.622" type="appl" />
         <tli id="T544" time="496.473" type="appl" />
         <tli id="T545" time="497.325" type="appl" />
         <tli id="T546" time="498.177" type="appl" />
         <tli id="T547" time="499.028" type="appl" />
         <tli id="T548" time="499.88" type="appl" />
         <tli id="T549" time="501.8894496059105" />
         <tli id="T550" time="504.51609594702853" />
         <tli id="T551" time="505.753" type="appl" />
         <tli id="T552" time="506.657" type="appl" />
         <tli id="T848" time="506.995625" type="intp" />
         <tli id="T553" time="507.56" type="appl" />
         <tli id="T554" time="508.463" type="appl" />
         <tli id="T555" time="509.367" type="appl" />
         <tli id="T556" time="512.4760343513304" />
         <tli id="T557" time="513.317" type="appl" />
         <tli id="T558" time="514.293" type="appl" />
         <tli id="T559" time="515.27" type="appl" />
         <tli id="T560" time="516.247" type="appl" />
         <tli id="T561" time="517.223" type="appl" />
         <tli id="T562" time="518.2" type="appl" />
         <tli id="T563" time="521.9759608388765" />
         <tli id="T564" time="523.258" type="appl" />
         <tli id="T565" time="524.396" type="appl" />
         <tli id="T566" time="525.534" type="appl" />
         <tli id="T567" time="526.672" type="appl" />
         <tli id="T568" time="528.0692470210639" />
         <tli id="T569" time="529.372" type="appl" />
         <tli id="T570" time="530.563" type="appl" />
         <tli id="T571" time="531.755" type="appl" />
         <tli id="T572" time="532.947" type="appl" />
         <tli id="T573" time="534.138" type="appl" />
         <tli id="T574" time="536.9158452308701" />
         <tli id="T575" time="537.96" type="appl" />
         <tli id="T576" time="538.68" type="appl" />
         <tli id="T577" time="539.4" type="appl" />
         <tli id="T578" time="540.12" type="appl" />
         <tli id="T579" time="541.059" type="appl" />
         <tli id="T580" time="541.883" type="appl" />
         <tli id="T581" time="542.707" type="appl" />
         <tli id="T582" time="543.531" type="appl" />
         <tli id="T583" time="544.355" type="appl" />
         <tli id="T584" time="545.332" type="appl" />
         <tli id="T585" time="546.225" type="appl" />
         <tli id="T586" time="547.118" type="appl" />
         <tli id="T587" time="548.01" type="appl" />
         <tli id="T588" time="549.775745718222" />
         <tli id="T589" time="550.912" type="appl" />
         <tli id="T590" time="551.743" type="appl" />
         <tli id="T591" time="552.575" type="appl" />
         <tli id="T592" time="553.407" type="appl" />
         <tli id="T593" time="554.238" type="appl" />
         <tli id="T594" time="555.7623660591457" />
         <tli id="T595" time="556.733" type="appl" />
         <tli id="T596" time="557.497" type="appl" />
         <tli id="T597" time="558.26" type="appl" />
         <tli id="T598" time="559.393" type="appl" />
         <tli id="T599" time="560.257" type="appl" />
         <tli id="T600" time="561.7156533246747" />
         <tli id="T601" time="562.45" type="appl" />
         <tli id="T602" time="563.36" type="appl" />
         <tli id="T603" time="565.0356276340067" />
         <tli id="T604" time="566.102" type="appl" />
         <tli id="T605" time="567.055" type="appl" />
         <tli id="T606" time="568.008" type="appl" />
         <tli id="T607" time="569.0289300663154" />
         <tli id="T608" time="570.3889195424274" />
         <tli id="T609" time="571.738" type="appl" />
         <tli id="T610" time="572.946" type="appl" />
         <tli id="T611" time="574.155" type="appl" />
         <tli id="T612" time="575.363" type="appl" />
         <tli id="T613" time="576.571" type="appl" />
         <tli id="T614" time="577.779" type="appl" />
         <tli id="T615" time="578.987" type="appl" />
         <tli id="T616" time="580.195" type="appl" />
         <tli id="T617" time="581.404" type="appl" />
         <tli id="T618" time="582.612" type="appl" />
         <tli id="T619" time="584.1154799900465" />
         <tli id="T620" time="585.364" type="appl" />
         <tli id="T621" time="586.498" type="appl" />
         <tli id="T622" time="587.632" type="appl" />
         <tli id="T623" time="588.766" type="appl" />
         <tli id="T624" time="589.9" type="appl" />
         <tli id="T625" time="591.034" type="appl" />
         <tli id="T626" time="592.168" type="appl" />
         <tli id="T627" time="593.302" type="appl" />
         <tli id="T628" time="594.436" type="appl" />
         <tli id="T629" time="595.57" type="appl" />
         <tli id="T630" time="596.704" type="appl" />
         <tli id="T631" time="597.838" type="appl" />
         <tli id="T632" time="598.972" type="appl" />
         <tli id="T633" time="600.106" type="appl" />
         <tli id="T634" time="600.579" type="appl" />
         <tli id="T635" time="604.2219910682496" />
         <tli id="T636" time="605.086" type="appl" />
         <tli id="T637" time="605.626" type="appl" />
         <tli id="T638" time="606.404" type="appl" />
         <tli id="T639" time="607.181" type="appl" />
         <tli id="T640" time="607.959" type="appl" />
         <tli id="T641" time="608.81" type="appl" />
         <tli id="T642" time="609.502" type="appl" />
         <tli id="T643" time="610.193" type="appl" />
         <tli id="T644" time="610.885" type="appl" />
         <tli id="T645" time="611.576" type="appl" />
         <tli id="T646" time="612.268" type="appl" />
         <tli id="T647" time="613.1285888137663" />
         <tli id="T648" time="613.977" type="appl" />
         <tli id="T649" time="614.662" type="appl" />
         <tli id="T650" time="615.346" type="appl" />
         <tli id="T651" time="616.03" type="appl" />
         <tli id="T652" time="616.715" type="appl" />
         <tli id="T653" time="617.399" type="appl" />
         <tli id="T654" time="618.083" type="appl" />
         <tli id="T655" time="618.768" type="appl" />
         <tli id="T656" time="619.6685382062457" />
         <tli id="T657" time="623.8751723210818" />
         <tli id="T658" time="624.91" type="appl" />
         <tli id="T659" time="625.98" type="appl" />
         <tli id="T660" time="627.05" type="appl" />
         <tli id="T661" time="628.121" type="appl" />
         <tli id="T662" time="629.191" type="appl" />
         <tli id="T663" time="630.261" type="appl" />
         <tli id="T664" time="631.8351107253835" />
         <tli id="T665" time="633.352" type="appl" />
         <tli id="T666" time="634.625" type="appl" />
         <tli id="T667" time="635.897" type="appl" />
         <tli id="T668" time="637.169" type="appl" />
         <tli id="T669" time="638.441" type="appl" />
         <tli id="T670" time="639.714" type="appl" />
         <tli id="T671" time="640.986" type="appl" />
         <tli id="T672" time="642.7816926850684" />
         <tli id="T673" time="643.979" type="appl" />
         <tli id="T674" time="645.032" type="appl" />
         <tli id="T675" time="646.086" type="appl" />
         <tli id="T676" time="647.139" type="appl" />
         <tli id="T677" time="649.0549774743882" />
         <tli id="T678" time="650.45" type="appl" />
         <tli id="T679" time="651.728" type="appl" />
         <tli id="T680" time="653.005" type="appl" />
         <tli id="T681" time="654.9349319740484" />
         <tli id="T682" time="655.834" type="appl" />
         <tli id="T683" time="656.574" type="appl" />
         <tli id="T684" time="657.314" type="appl" />
         <tli id="T685" time="658.054" type="appl" />
         <tli id="T686" time="658.794" type="appl" />
         <tli id="T687" time="659.534" type="appl" />
         <tli id="T688" time="660.601" type="appl" />
         <tli id="T689" time="661.615" type="appl" />
         <tli id="T690" time="662.63" type="appl" />
         <tli id="T849" time="663.0649999999999" type="intp" />
         <tli id="T691" time="663.645" type="appl" />
         <tli id="T692" time="664.66" type="appl" />
         <tli id="T693" time="665.674" type="appl" />
         <tli id="T694" time="666.689" type="appl" />
         <tli id="T695" time="667.704" type="appl" />
         <tli id="T696" time="668.718" type="appl" />
         <tli id="T697" time="669.733" type="appl" />
         <tli id="T698" time="672.3414639452644" />
         <tli id="T699" time="673.479" type="appl" />
         <tli id="T700" time="674.492" type="appl" />
         <tli id="T701" time="675.505" type="appl" />
         <tli id="T702" time="676.518" type="appl" />
         <tli id="T703" time="677.53" type="appl" />
         <tli id="T704" time="678.543" type="appl" />
         <tli id="T705" time="679.556" type="appl" />
         <tli id="T706" time="680.569" type="appl" />
         <tli id="T707" time="681.582" type="appl" />
         <tli id="T708" time="682.791" type="appl" />
         <tli id="T709" time="684.002" type="appl" />
         <tli id="T710" time="684.934" type="appl" />
         <tli id="T711" time="685.865" type="appl" />
         <tli id="T712" time="686.796" type="appl" />
         <tli id="T713" time="687.728" type="appl" />
         <tli id="T714" time="688.659" type="appl" />
         <tli id="T715" time="689.59" type="appl" />
         <tli id="T716" time="690.522" type="appl" />
         <tli id="T717" time="691.5546486028841" />
         <tli id="T718" time="692.7613059321793" />
         <tli id="T719" time="693.921" type="appl" />
         <tli id="T720" time="695.041" type="appl" />
         <tli id="T721" time="695.8612819439049" />
         <tli id="T722" time="696.823" type="appl" />
         <tli id="T723" time="697.732" type="appl" />
         <tli id="T724" time="698.64" type="appl" />
         <tli id="T725" time="699.549" type="appl" />
         <tli id="T726" time="700.458" type="appl" />
         <tli id="T727" time="702.1279001181458" />
         <tli id="T728" time="703.465" type="appl" />
         <tli id="T729" time="704.682" type="appl" />
         <tli id="T730" time="705.9" type="appl" />
         <tli id="T731" time="707.2011941932495" />
         <tli id="T732" time="708.656" type="appl" />
         <tli id="T733" time="710.047" type="appl" />
         <tli id="T734" time="711.437" type="appl" />
         <tli id="T735" time="712.828" type="appl" />
         <tli id="T736" time="714.218" type="appl" />
         <tli id="T737" time="715.609" type="appl" />
         <tli id="T738" time="716.999" type="appl" />
         <tli id="T739" time="718.0744433870655" />
         <tli id="T740" time="719.217" type="appl" />
         <tli id="T741" time="720.128" type="appl" />
         <tli id="T742" time="721.039" type="appl" />
         <tli id="T743" time="721.95" type="appl" />
         <tli id="T744" time="722.862" type="appl" />
         <tli id="T745" time="723.773" type="appl" />
         <tli id="T746" time="724.684" type="appl" />
         <tli id="T747" time="725.595" type="appl" />
         <tli id="T748" time="726.506" type="appl" />
         <tli id="T749" time="727.526" type="appl" />
         <tli id="T750" time="728.547" type="appl" />
         <tli id="T751" time="729.567" type="appl" />
         <tli id="T752" time="730.588" type="appl" />
         <tli id="T753" time="731.608" type="appl" />
         <tli id="T754" time="732.629" type="appl" />
         <tli id="T755" time="733.649" type="appl" />
         <tli id="T756" time="734.67" type="appl" />
         <tli id="T757" time="735.69" type="appl" />
         <tli id="T758" time="738.1476213898734" />
         <tli id="T759" time="739.431" type="appl" />
         <tli id="T760" time="740.329" type="appl" />
         <tli id="T761" time="741.226" type="appl" />
         <tli id="T762" time="742.124" type="appl" />
         <tli id="T763" time="743.022" type="appl" />
         <tli id="T764" time="743.92" type="appl" />
         <tli id="T765" time="744.817" type="appl" />
         <tli id="T766" time="745.715" type="appl" />
         <tli id="T767" time="746.613" type="appl" />
         <tli id="T768" time="747.511" type="appl" />
         <tli id="T769" time="748.408" type="appl" />
         <tli id="T770" time="749.3942010281123" />
         <tli id="T771" time="750.5141923613809" />
         <tli id="T772" time="751.203" type="appl" />
         <tli id="T773" time="752.021" type="appl" />
         <tli id="T774" time="752.838" type="appl" />
         <tli id="T775" time="753.655" type="appl" />
         <tli id="T776" time="754.472" type="appl" />
         <tli id="T777" time="755.29" type="appl" />
         <tli id="T778" time="756.107" type="appl" />
         <tli id="T779" time="756.924" type="appl" />
         <tli id="T780" time="757.742" type="appl" />
         <tli id="T781" time="759.1407922735807" />
         <tli id="T782" time="760.305" type="appl" />
         <tli id="T783" time="761.479" type="appl" />
         <tli id="T784" time="762.652" type="appl" />
         <tli id="T785" time="763.826" type="appl" />
         <tli id="T786" time="764.997" type="appl" />
         <tli id="T787" time="765.982" type="appl" />
         <tli id="T788" time="766.966" type="appl" />
         <tli id="T789" time="767.95" type="appl" />
         <tli id="T790" time="768.935" type="appl" />
         <tli id="T791" time="769.919" type="appl" />
         <tli id="T792" time="771.4673635546142" />
         <tli id="T793" time="772.632" type="appl" />
         <tli id="T794" time="773.706" type="appl" />
         <tli id="T795" time="774.779" type="appl" />
         <tli id="T796" time="775.852" type="appl" />
         <tli id="T797" time="776.926" type="appl" />
         <tli id="T798" time="778.2006447843837" />
         <tli id="T799" time="779.154" type="appl" />
         <tli id="T800" time="779.949" type="appl" />
         <tli id="T801" time="780.745" type="appl" />
         <tli id="T802" time="781.54" type="appl" />
         <tli id="T803" time="782.335" type="appl" />
         <tli id="T804" time="783.13" type="appl" />
         <tli id="T805" time="783.925" type="appl" />
         <tli id="T806" time="784.72" type="appl" />
         <tli id="T807" time="785.516" type="appl" />
         <tli id="T808" time="786.311" type="appl" />
         <tli id="T809" time="787.106" type="appl" />
         <tli id="T810" time="788.268" type="appl" />
         <tli id="T811" time="789.325" type="appl" />
         <tli id="T812" time="790.382" type="appl" />
         <tli id="T813" time="791.438" type="appl" />
         <tli id="T814" time="792.309" type="appl" />
         <tli id="T815" time="793.154" type="appl" />
         <tli id="T816" time="793.998" type="appl" />
         <tli id="T817" time="795.1605135453082" />
         <tli id="T818" time="795.826" type="appl" />
         <tli id="T819" time="796.489" type="appl" />
         <tli id="T820" time="797.151" type="appl" />
         <tli id="T821" time="798.4138217038503" />
         <tli id="T822" time="799.332" type="appl" />
         <tli id="T823" time="800.157" type="appl" />
         <tli id="T824" time="800.983" type="appl" />
         <tli id="T825" time="801.809" type="appl" />
         <tli id="T826" time="802.635" type="appl" />
         <tli id="T827" time="803.46" type="appl" />
         <tli id="T828" time="804.286" type="appl" />
         <tli id="T829" time="805.112" type="appl" />
         <tli id="T830" time="805.937" type="appl" />
         <tli id="T831" time="806.763" type="appl" />
         <tli id="T832" time="807.589" type="appl" />
         <tli id="T833" time="808.415" type="appl" />
         <tli id="T834" time="809.24" type="appl" />
         <tli id="T835" time="810.066" type="appl" />
         <tli id="T836" time="812.7137110482619" />
         <tli id="T837" time="814.094" type="appl" />
         <tli id="T838" time="815.516" type="appl" />
         <tli id="T839" time="816.938" type="appl" />
         <tli id="T840" time="818.36" type="appl" />
         <tli id="T841" time="819.782" type="appl" />
         <tli id="T842" time="821.204" type="appl" />
         <tli id="T843" time="822.626" type="appl" />
         <tli id="T844" time="823.075" type="appl" />
         <tli id="T845" time="823.418" type="appl" />
         <tli id="T846" time="823.76" type="appl" />
         <tli id="T847" time="823.833" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T220" start="T218">
            <tli id="T218.tx.1" />
         </timeline-fork>
         <timeline-fork end="T226" start="T225">
            <tli id="T225.tx.1" />
         </timeline-fork>
         <timeline-fork end="T235" start="T234">
            <tli id="T234.tx.1" />
         </timeline-fork>
         <timeline-fork end="T455" start="T450">
            <tli id="T450.tx.1" />
         </timeline-fork>
         <timeline-fork end="T509" start="T507">
            <tli id="T507.tx.1" />
         </timeline-fork>
         <timeline-fork end="T846" start="T843">
            <tli id="T843.tx.1" />
            <tli id="T843.tx.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T846" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kamen</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">miʔnʼibeʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šobial</ts>
                  <nts id="Seg_11" n="HIAT:ip">?</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Urgo</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">dʼalagən</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Šide</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">dʼala</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_31" n="HIAT:w" s="T7">Kazan</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T850">turagən</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">amnobiam</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T9">
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_44" n="HIAT:w" s="T9">Ĭmbi=</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">ĭmbi=</ts>
                  <nts id="Seg_48" n="HIAT:ip">)</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_51" n="HIAT:w" s="T11">Ĭmbi</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_54" n="HIAT:w" s="T12">kubial</ts>
                  <nts id="Seg_55" n="HIAT:ip">?</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_58" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_60" n="HIAT:w" s="T13">Dĭgəttə</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_63" n="HIAT:w" s="T14">döber</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">šobiam</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_70" n="HIAT:u" s="T16">
                  <nts id="Seg_71" n="HIAT:ip">(</nts>
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <ats e="T17" id="Seg_73" n="HIAT:non-pho" s="T16">BRK</ats>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_79" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_81" n="HIAT:w" s="T17">Măndərbiam</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_85" n="HIAT:w" s="T18">măndərbiam</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_89" n="HIAT:w" s="T19">kădedə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_93" n="HIAT:w" s="T20">ej</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_96" n="HIAT:w" s="T21">moliam</ts>
                  <nts id="Seg_97" n="HIAT:ip">)</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_100" n="HIAT:w" s="T22">ej</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_103" n="HIAT:w" s="T23">moliam</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_106" n="HIAT:w" s="T24">šozittə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_109" n="HIAT:w" s="T25">döber</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_113" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_115" n="HIAT:w" s="T26">Nagur</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_118" n="HIAT:w" s="T27">dʼalagən</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_121" n="HIAT:w" s="T28">dĭgəttə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_124" n="HIAT:w" s="T29">šobiam</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">döber</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_131" n="HIAT:u" s="T31">
                  <nts id="Seg_132" n="HIAT:ip">(</nts>
                  <nts id="Seg_133" n="HIAT:ip">(</nts>
                  <ats e="T32" id="Seg_134" n="HIAT:non-pho" s="T31">BRK</ats>
                  <nts id="Seg_135" n="HIAT:ip">)</nts>
                  <nts id="Seg_136" n="HIAT:ip">)</nts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_140" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_142" n="HIAT:w" s="T32">Dĭgəttə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_145" n="HIAT:w" s="T33">döber</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_148" n="HIAT:w" s="T34">šobiam</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_152" n="HIAT:w" s="T35">urgajanə</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_155" n="HIAT:w" s="T36">šobiam</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_159" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_161" n="HIAT:w" s="T37">Dĭ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_164" n="HIAT:w" s="T38">măna</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_167" n="HIAT:w" s="T39">bar</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_170" n="HIAT:w" s="T40">bădəbi</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_174" n="HIAT:w" s="T41">mĭj</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_177" n="HIAT:w" s="T42">mĭbi</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_181" n="HIAT:u" s="T43">
                  <nts id="Seg_182" n="HIAT:ip">(</nts>
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <ats e="T44" id="Seg_184" n="HIAT:non-pho" s="T43">BRK</ats>
                  <nts id="Seg_185" n="HIAT:ip">)</nts>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_190" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_192" n="HIAT:w" s="T44">Dĭgəttə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_195" n="HIAT:w" s="T45">döber</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_198" n="HIAT:w" s="T46">šobiam</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_202" n="HIAT:w" s="T47">buzo</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_205" n="HIAT:w" s="T48">kirgarlaʔbə</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_209" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_211" n="HIAT:w" s="T49">Nüke</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_214" n="HIAT:w" s="T50">bar</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_217" n="HIAT:w" s="T51">mĭbi</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_220" n="HIAT:w" s="T52">dĭʔnə</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_223" n="HIAT:w" s="T53">toltanoʔ</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_227" n="HIAT:w" s="T54">dĭ</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">amorlaʔbə</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_234" n="HIAT:w" s="T56">ej</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">kirgarlia</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_241" n="HIAT:u" s="T58">
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <nts id="Seg_243" n="HIAT:ip">(</nts>
                  <ats e="T59" id="Seg_244" n="HIAT:non-pho" s="T58">BRK</ats>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip">)</nts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_250" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_252" n="HIAT:w" s="T59">Dĭ</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_255" n="HIAT:w" s="T60">nükegən</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_258" n="HIAT:w" s="T61">turagən</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_261" n="HIAT:w" s="T62">šišəge</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_265" n="HIAT:w" s="T63">pʼeš</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_268" n="HIAT:w" s="T64">nendəbi</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_272" n="HIAT:u" s="T65">
                  <nts id="Seg_273" n="HIAT:ip">(</nts>
                  <nts id="Seg_274" n="HIAT:ip">(</nts>
                  <ats e="T66" id="Seg_275" n="HIAT:non-pho" s="T65">BRK</ats>
                  <nts id="Seg_276" n="HIAT:ip">)</nts>
                  <nts id="Seg_277" n="HIAT:ip">)</nts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_281" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_283" n="HIAT:w" s="T66">Nʼiʔnen</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_286" n="HIAT:w" s="T67">bar</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_289" n="HIAT:w" s="T68">šišəge</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_293" n="HIAT:w" s="T69">ugandə</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_297" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_299" n="HIAT:w" s="T70">Büzo</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_302" n="HIAT:w" s="T71">turagən</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_305" n="HIAT:w" s="T72">nuga</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_308" n="HIAT:w" s="T73">bar</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_312" n="HIAT:u" s="T74">
                  <nts id="Seg_313" n="HIAT:ip">(</nts>
                  <nts id="Seg_314" n="HIAT:ip">(</nts>
                  <ats e="T75" id="Seg_315" n="HIAT:non-pho" s="T74">BRK</ats>
                  <nts id="Seg_316" n="HIAT:ip">)</nts>
                  <nts id="Seg_317" n="HIAT:ip">)</nts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_321" n="HIAT:u" s="T75">
                  <nts id="Seg_322" n="HIAT:ip">(</nts>
                  <ts e="T76" id="Seg_324" n="HIAT:w" s="T75">Tüžöj</ts>
                  <nts id="Seg_325" n="HIAT:ip">)</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_328" n="HIAT:w" s="T76">Tüžöj</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_331" n="HIAT:w" s="T77">nʼeluʔpi</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_334" n="HIAT:w" s="T78">bar</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_338" n="HIAT:w" s="T79">üdʼüge</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_341" n="HIAT:w" s="T80">büzo</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_345" n="HIAT:u" s="T81">
                  <nts id="Seg_346" n="HIAT:ip">(</nts>
                  <nts id="Seg_347" n="HIAT:ip">(</nts>
                  <ats e="T82" id="Seg_348" n="HIAT:non-pho" s="T81">BRK</ats>
                  <nts id="Seg_349" n="HIAT:ip">)</nts>
                  <nts id="Seg_350" n="HIAT:ip">)</nts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_354" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_356" n="HIAT:w" s="T82">Miʔ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_359" n="HIAT:w" s="T83">turagən</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_362" n="HIAT:w" s="T84">ugandə</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_365" n="HIAT:w" s="T85">iʔgö</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_368" n="HIAT:w" s="T86">il</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_371" n="HIAT:w" s="T87">kübiʔi</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_375" n="HIAT:u" s="T88">
                  <nts id="Seg_376" n="HIAT:ip">(</nts>
                  <nts id="Seg_377" n="HIAT:ip">(</nts>
                  <ats e="T89" id="Seg_378" n="HIAT:non-pho" s="T88">BRK</ats>
                  <nts id="Seg_379" n="HIAT:ip">)</nts>
                  <nts id="Seg_380" n="HIAT:ip">)</nts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_384" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_386" n="HIAT:w" s="T89">Mĭmbiem</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_389" n="HIAT:w" s="T90">nükenə</ts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_393" n="HIAT:w" s="T91">dĭ</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_395" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_397" n="HIAT:w" s="T92">uga-</ts>
                  <nts id="Seg_398" n="HIAT:ip">)</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_401" n="HIAT:w" s="T93">ugandə</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_404" n="HIAT:w" s="T94">ĭzemnie</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_408" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_410" n="HIAT:w" s="T95">Măn</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_413" n="HIAT:w" s="T96">teinen</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_415" n="HIAT:ip">(</nts>
                  <ts e="T98" id="Seg_417" n="HIAT:w" s="T97">kulambi-</ts>
                  <nts id="Seg_418" n="HIAT:ip">)</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_421" n="HIAT:w" s="T98">külalləm</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_425" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_427" n="HIAT:w" s="T99">No</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_431" n="HIAT:w" s="T100">küʔ</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_435" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_437" n="HIAT:w" s="T101">No</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_441" n="HIAT:w" s="T102">küʔ</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_445" n="HIAT:u" s="T103">
                  <nts id="Seg_446" n="HIAT:ip">(</nts>
                  <ts e="T104" id="Seg_448" n="HIAT:w" s="T103">Taʔ-</ts>
                  <nts id="Seg_449" n="HIAT:ip">)</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_452" n="HIAT:w" s="T104">Karəldʼan</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_454" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_456" n="HIAT:w" s="T105">šo-</ts>
                  <nts id="Seg_457" n="HIAT:ip">)</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_460" n="HIAT:w" s="T106">šolam</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_462" n="HIAT:ip">(</nts>
                  <ts e="T108" id="Seg_464" n="HIAT:w" s="T107">băzəsʼtə</ts>
                  <nts id="Seg_465" n="HIAT:ip">)</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_468" n="HIAT:w" s="T108">tănan</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_472" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_474" n="HIAT:w" s="T109">Dĭgəttə</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_477" n="HIAT:w" s="T110">ej</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_480" n="HIAT:w" s="T111">kambiam</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_484" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_486" n="HIAT:w" s="T112">Noʔ</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_489" n="HIAT:w" s="T113">deʔpiʔi</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_492" n="HIAT:w" s="T114">šide</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_495" n="HIAT:w" s="T115">tibi</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_499" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_501" n="HIAT:w" s="T116">Dĭgəttə</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_504" n="HIAT:w" s="T117">šobiam:</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_507" n="HIAT:w" s="T118">Ĭmbi</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_510" n="HIAT:w" s="T119">konʼdʼo</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_513" n="HIAT:w" s="T120">ej</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_516" n="HIAT:w" s="T121">šobial</ts>
                  <nts id="Seg_517" n="HIAT:ip">?</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_520" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_522" n="HIAT:w" s="T122">A</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_525" n="HIAT:w" s="T123">măn</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_528" n="HIAT:w" s="T124">tenəbiem</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_531" n="HIAT:w" s="T125">tăn</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_533" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_535" n="HIAT:w" s="T126">külambiam</ts>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip">,</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_540" n="HIAT:w" s="T127">băzəsʼtə</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_543" n="HIAT:w" s="T128">šobiam</ts>
                  <nts id="Seg_544" n="HIAT:ip">"</nts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_548" n="HIAT:u" s="T129">
                  <nts id="Seg_549" n="HIAT:ip">(</nts>
                  <nts id="Seg_550" n="HIAT:ip">(</nts>
                  <ats e="T130" id="Seg_551" n="HIAT:non-pho" s="T129">BRK</ats>
                  <nts id="Seg_552" n="HIAT:ip">)</nts>
                  <nts id="Seg_553" n="HIAT:ip">)</nts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_557" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_559" n="HIAT:w" s="T130">Büzo</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_562" n="HIAT:w" s="T131">nomlaʔbə</ts>
                  <nts id="Seg_563" n="HIAT:ip">,</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_566" n="HIAT:w" s="T132">ej</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_569" n="HIAT:w" s="T133">kirgarlaʔbə</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_573" n="HIAT:u" s="T134">
                  <nts id="Seg_574" n="HIAT:ip">(</nts>
                  <nts id="Seg_575" n="HIAT:ip">(</nts>
                  <ats e="T135" id="Seg_576" n="HIAT:non-pho" s="T134">BRK</ats>
                  <nts id="Seg_577" n="HIAT:ip">)</nts>
                  <nts id="Seg_578" n="HIAT:ip">)</nts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_582" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_584" n="HIAT:w" s="T135">Esseŋ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_587" n="HIAT:w" s="T136">bar</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_590" n="HIAT:w" s="T137">nuʔməleʔbəʔjə</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_592" n="HIAT:ip">(</nts>
                  <ts e="T139" id="Seg_594" n="HIAT:w" s="T138">turan</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_597" n="HIAT:w" s="T139">to-</ts>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_601" n="HIAT:w" s="T140">turan</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_604" n="HIAT:w" s="T141">tondə</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_608" n="HIAT:u" s="T142">
                  <nts id="Seg_609" n="HIAT:ip">(</nts>
                  <nts id="Seg_610" n="HIAT:ip">(</nts>
                  <ats e="T143" id="Seg_611" n="HIAT:non-pho" s="T142">BRK</ats>
                  <nts id="Seg_612" n="HIAT:ip">)</nts>
                  <nts id="Seg_613" n="HIAT:ip">)</nts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_617" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_619" n="HIAT:w" s="T143">Šide</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_622" n="HIAT:w" s="T144">nʼi</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_625" n="HIAT:w" s="T145">šobiʔi</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_628" n="HIAT:w" s="T146">nʼilgösʼtə</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_632" n="HIAT:u" s="T147">
                  <nts id="Seg_633" n="HIAT:ip">(</nts>
                  <nts id="Seg_634" n="HIAT:ip">(</nts>
                  <ats e="T148" id="Seg_635" n="HIAT:non-pho" s="T147">BRK</ats>
                  <nts id="Seg_636" n="HIAT:ip">)</nts>
                  <nts id="Seg_637" n="HIAT:ip">)</nts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_641" n="HIAT:u" s="T148">
                  <nts id="Seg_642" n="HIAT:ip">(</nts>
                  <nts id="Seg_643" n="HIAT:ip">(</nts>
                  <ats e="T149" id="Seg_644" n="HIAT:non-pho" s="T148">BRK</ats>
                  <nts id="Seg_645" n="HIAT:ip">)</nts>
                  <nts id="Seg_646" n="HIAT:ip">)</nts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_650" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_652" n="HIAT:w" s="T149">Iʔ</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_654" n="HIAT:ip">(</nts>
                  <ts e="T151" id="Seg_656" n="HIAT:w" s="T150">kirgargaʔ</ts>
                  <nts id="Seg_657" n="HIAT:ip">)</nts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_661" n="HIAT:w" s="T151">iʔ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_664" n="HIAT:w" s="T152">alomgaʔ</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_668" n="HIAT:w" s="T153">nʼilgöleʔ</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_672" n="HIAT:u" s="T154">
                  <nts id="Seg_673" n="HIAT:ip">(</nts>
                  <nts id="Seg_674" n="HIAT:ip">(</nts>
                  <ats e="T155" id="Seg_675" n="HIAT:non-pho" s="T154">BRK</ats>
                  <nts id="Seg_676" n="HIAT:ip">)</nts>
                  <nts id="Seg_677" n="HIAT:ip">)</nts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_681" n="HIAT:u" s="T155">
                  <nts id="Seg_682" n="HIAT:ip">"</nts>
                  <ts e="T156" id="Seg_684" n="HIAT:w" s="T155">Ial</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_687" n="HIAT:w" s="T156">kamen</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_690" n="HIAT:w" s="T157">šoləj</ts>
                  <nts id="Seg_691" n="HIAT:ip">?</nts>
                  <nts id="Seg_692" n="HIAT:ip">"</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_695" n="HIAT:u" s="T158">
                  <nts id="Seg_696" n="HIAT:ip">"</nts>
                  <ts e="T159" id="Seg_698" n="HIAT:w" s="T158">Büžü</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_701" n="HIAT:w" s="T159">šoləj</ts>
                  <nts id="Seg_702" n="HIAT:ip">!</nts>
                  <nts id="Seg_703" n="HIAT:ip">"</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_706" n="HIAT:u" s="T160">
                  <nts id="Seg_707" n="HIAT:ip">(</nts>
                  <nts id="Seg_708" n="HIAT:ip">(</nts>
                  <ats e="T161" id="Seg_709" n="HIAT:non-pho" s="T160">BRK</ats>
                  <nts id="Seg_710" n="HIAT:ip">)</nts>
                  <nts id="Seg_711" n="HIAT:ip">)</nts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_715" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_717" n="HIAT:w" s="T161">Nada</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_720" n="HIAT:w" s="T162">bü</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_723" n="HIAT:w" s="T163">ejümzittə</ts>
                  <nts id="Seg_724" n="HIAT:ip">,</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_727" n="HIAT:w" s="T164">dĭgəttə</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_730" n="HIAT:w" s="T165">tüžöj</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_733" n="HIAT:w" s="T166">bĭdəlzittə</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_737" n="HIAT:u" s="T167">
                  <nts id="Seg_738" n="HIAT:ip">(</nts>
                  <nts id="Seg_739" n="HIAT:ip">(</nts>
                  <ats e="T168" id="Seg_740" n="HIAT:non-pho" s="T167">BRK</ats>
                  <nts id="Seg_741" n="HIAT:ip">)</nts>
                  <nts id="Seg_742" n="HIAT:ip">)</nts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_746" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_748" n="HIAT:w" s="T168">Ĭmbi</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_751" n="HIAT:w" s="T169">nʼilgösʼtə</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_754" n="HIAT:w" s="T170">šobilaʔ</ts>
                  <nts id="Seg_755" n="HIAT:ip">,</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_758" n="HIAT:w" s="T171">măndərzittə</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_761" n="HIAT:w" s="T172">šobilaʔ</ts>
                  <nts id="Seg_762" n="HIAT:ip">?</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_765" n="HIAT:u" s="T173">
                  <nts id="Seg_766" n="HIAT:ip">(</nts>
                  <nts id="Seg_767" n="HIAT:ip">(</nts>
                  <ats e="T174" id="Seg_768" n="HIAT:non-pho" s="T173">BRK</ats>
                  <nts id="Seg_769" n="HIAT:ip">)</nts>
                  <nts id="Seg_770" n="HIAT:ip">)</nts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_774" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_776" n="HIAT:w" s="T174">Ej</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_779" n="HIAT:w" s="T175">kürümnieʔi</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_783" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_785" n="HIAT:w" s="T176">Kuvas</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_788" n="HIAT:w" s="T177">nʼizeŋ</ts>
                  <nts id="Seg_789" n="HIAT:ip">,</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_792" n="HIAT:w" s="T178">ej</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_795" n="HIAT:w" s="T179">kürümnieʔi</ts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_799" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_801" n="HIAT:w" s="T180">Nulaʔbəʔjə</ts>
                  <nts id="Seg_802" n="HIAT:ip">,</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_805" n="HIAT:w" s="T181">jakšeʔi</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_808" n="HIAT:w" s="T182">ugandə</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_812" n="HIAT:u" s="T183">
                  <nts id="Seg_813" n="HIAT:ip">(</nts>
                  <nts id="Seg_814" n="HIAT:ip">(</nts>
                  <ats e="T184" id="Seg_815" n="HIAT:non-pho" s="T183">BRK</ats>
                  <nts id="Seg_816" n="HIAT:ip">)</nts>
                  <nts id="Seg_817" n="HIAT:ip">)</nts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_821" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_823" n="HIAT:w" s="T184">Sanə</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_826" n="HIAT:w" s="T185">teinen</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_829" n="HIAT:w" s="T186">mĭbiem</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_832" n="HIAT:w" s="T187">tănan</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_835" n="HIAT:w" s="T188">lejzittə</ts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_839" n="HIAT:u" s="T189">
                  <nts id="Seg_840" n="HIAT:ip">(</nts>
                  <nts id="Seg_841" n="HIAT:ip">(</nts>
                  <ats e="T190" id="Seg_842" n="HIAT:non-pho" s="T189">BRK</ats>
                  <nts id="Seg_843" n="HIAT:ip">)</nts>
                  <nts id="Seg_844" n="HIAT:ip">)</nts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_848" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_850" n="HIAT:w" s="T190">Sʼimat</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_853" n="HIAT:w" s="T191">măndərzittə</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_856" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_858" n="HIAT:w" s="T192">kuʔi-</ts>
                  <nts id="Seg_859" n="HIAT:ip">)</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_862" n="HIAT:w" s="T193">kuzaŋdə</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_865" n="HIAT:w" s="T194">nʼilgösʼtə</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_869" n="HIAT:w" s="T195">aŋdə</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_872" n="HIAT:w" s="T196">amzittə</ts>
                  <nts id="Seg_873" n="HIAT:ip">,</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_876" n="HIAT:w" s="T197">tĭmeʔi</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_879" n="HIAT:w" s="T198">talbəzittə</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_882" n="HIAT:w" s="T199">ipek</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_886" n="HIAT:u" s="T200">
                  <nts id="Seg_887" n="HIAT:ip">(</nts>
                  <nts id="Seg_888" n="HIAT:ip">(</nts>
                  <ats e="T201" id="Seg_889" n="HIAT:non-pho" s="T200">BRK</ats>
                  <nts id="Seg_890" n="HIAT:ip">)</nts>
                  <nts id="Seg_891" n="HIAT:ip">)</nts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_895" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_897" n="HIAT:w" s="T201">Tenöbiam</ts>
                  <nts id="Seg_898" n="HIAT:ip">,</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_901" n="HIAT:w" s="T202">tenöbiam</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_905" n="HIAT:w" s="T203">kăde</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_908" n="HIAT:w" s="T204">döber</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_911" n="HIAT:w" s="T205">šozittə</ts>
                  <nts id="Seg_912" n="HIAT:ip">.</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_915" n="HIAT:u" s="T206">
                  <nts id="Seg_916" n="HIAT:ip">(</nts>
                  <nts id="Seg_917" n="HIAT:ip">(</nts>
                  <ats e="T207" id="Seg_918" n="HIAT:non-pho" s="T206">BRK</ats>
                  <nts id="Seg_919" n="HIAT:ip">)</nts>
                  <nts id="Seg_920" n="HIAT:ip">)</nts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_924" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_926" n="HIAT:w" s="T207">Kuba</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_929" n="HIAT:w" s="T208">dʼügən</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_932" n="HIAT:w" s="T209">iʔbolaʔpi</ts>
                  <nts id="Seg_933" n="HIAT:ip">.</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_936" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_938" n="HIAT:w" s="T210">Ipek</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_941" n="HIAT:w" s="T211">embiʔi</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_945" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_947" n="HIAT:w" s="T212">Dĭgəttə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_950" n="HIAT:w" s="T213">amorbiʔi</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_953" n="HIAT:w" s="T214">dĭ</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_956" n="HIAT:w" s="T215">ipek</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_960" n="HIAT:w" s="T216">i</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_963" n="HIAT:w" s="T217">uja</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_967" n="HIAT:u" s="T218">
                  <ts e="T218.tx.1" id="Seg_969" n="HIAT:w" s="T218">И</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_972" n="HIAT:w" s="T218.tx.1">мясо</ts>
                  <nts id="Seg_973" n="HIAT:ip">.</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_976" n="HIAT:u" s="T220">
                  <nts id="Seg_977" n="HIAT:ip">(</nts>
                  <nts id="Seg_978" n="HIAT:ip">(</nts>
                  <ats e="T221" id="Seg_979" n="HIAT:non-pho" s="T220">BRK</ats>
                  <nts id="Seg_980" n="HIAT:ip">)</nts>
                  <nts id="Seg_981" n="HIAT:ip">)</nts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_985" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_987" n="HIAT:w" s="T221">Dĭgəttə</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_990" n="HIAT:w" s="T222">ambiʔi</ts>
                  <nts id="Seg_991" n="HIAT:ip">,</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_994" n="HIAT:w" s="T223">dĭgəttə</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_997" n="HIAT:w" s="T224">kudajdə</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225.tx.1" id="Seg_1000" n="HIAT:w" s="T225">numan</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_1003" n="HIAT:w" s="T225.tx.1">üzəbiʔi</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_1007" n="HIAT:u" s="T226">
                  <nts id="Seg_1008" n="HIAT:ip">(</nts>
                  <nts id="Seg_1009" n="HIAT:ip">(</nts>
                  <ats e="T227" id="Seg_1010" n="HIAT:non-pho" s="T226">BRK</ats>
                  <nts id="Seg_1011" n="HIAT:ip">)</nts>
                  <nts id="Seg_1012" n="HIAT:ip">)</nts>
                  <nts id="Seg_1013" n="HIAT:ip">.</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_1016" n="HIAT:u" s="T227">
                  <ts e="T851" id="Seg_1018" n="HIAT:w" s="T227">Kazan</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_1021" n="HIAT:w" s="T851">turanə</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_1024" n="HIAT:w" s="T228">mĭmbiem</ts>
                  <nts id="Seg_1025" n="HIAT:ip">,</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1027" n="HIAT:ip">(</nts>
                  <ts e="T230" id="Seg_1029" n="HIAT:w" s="T229">tʼegergən=</ts>
                  <nts id="Seg_1030" n="HIAT:ip">)</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_1033" n="HIAT:w" s="T230">tʼegergən</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1035" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_1037" n="HIAT:w" s="T231">ku-</ts>
                  <nts id="Seg_1038" n="HIAT:ip">)</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1041" n="HIAT:w" s="T232">kudaj</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1043" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_1045" n="HIAT:w" s="T233">numan=</ts>
                  <nts id="Seg_1046" n="HIAT:ip">)</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234.tx.1" id="Seg_1049" n="HIAT:w" s="T234">numan</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1052" n="HIAT:w" s="T234.tx.1">üzəbiem</ts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_1056" n="HIAT:u" s="T235">
                  <nts id="Seg_1057" n="HIAT:ip">(</nts>
                  <nts id="Seg_1058" n="HIAT:ip">(</nts>
                  <ats e="T236" id="Seg_1059" n="HIAT:non-pho" s="T235">BRK</ats>
                  <nts id="Seg_1060" n="HIAT:ip">)</nts>
                  <nts id="Seg_1061" n="HIAT:ip">)</nts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_1065" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_1067" n="HIAT:w" s="T236">Iʔgö</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_1070" n="HIAT:w" s="T237">il</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1073" n="HIAT:w" s="T238">külambiʔi</ts>
                  <nts id="Seg_1074" n="HIAT:ip">,</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1076" n="HIAT:ip">(</nts>
                  <ts e="T240" id="Seg_1078" n="HIAT:w" s="T239">kros-</ts>
                  <nts id="Seg_1079" n="HIAT:ip">)</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1082" n="HIAT:w" s="T240">krospaʔi</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1085" n="HIAT:w" s="T241">ugandə</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_1088" n="HIAT:w" s="T242">iʔgö</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1091" n="HIAT:w" s="T243">nugaʔi</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1094" n="HIAT:w" s="T244">bar</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_1098" n="HIAT:u" s="T245">
                  <nts id="Seg_1099" n="HIAT:ip">(</nts>
                  <nts id="Seg_1100" n="HIAT:ip">(</nts>
                  <ats e="T246" id="Seg_1101" n="HIAT:non-pho" s="T245">BRK</ats>
                  <nts id="Seg_1102" n="HIAT:ip">)</nts>
                  <nts id="Seg_1103" n="HIAT:ip">)</nts>
                  <nts id="Seg_1104" n="HIAT:ip">.</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_1107" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_1109" n="HIAT:w" s="T246">Miʔ</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1112" n="HIAT:w" s="T247">koʔbdo</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1115" n="HIAT:w" s="T248">ĭzembi</ts>
                  <nts id="Seg_1116" n="HIAT:ip">,</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1119" n="HIAT:w" s="T249">a</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1122" n="HIAT:w" s="T250">abat</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1124" n="HIAT:ip">(</nts>
                  <ts e="T252" id="Seg_1126" n="HIAT:w" s="T251">k-</ts>
                  <nts id="Seg_1127" n="HIAT:ip">)</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1130" n="HIAT:w" s="T252">kumbi</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1132" n="HIAT:ip">(</nts>
                  <ts e="T254" id="Seg_1134" n="HIAT:w" s="T253">dʼizi-</ts>
                  <nts id="Seg_1135" n="HIAT:ip">)</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1138" n="HIAT:w" s="T254">dʼazirzittə</ts>
                  <nts id="Seg_1139" n="HIAT:ip">.</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1142" n="HIAT:u" s="T255">
                  <nts id="Seg_1143" n="HIAT:ip">(</nts>
                  <nts id="Seg_1144" n="HIAT:ip">(</nts>
                  <ats e="T256" id="Seg_1145" n="HIAT:non-pho" s="T255">BRK</ats>
                  <nts id="Seg_1146" n="HIAT:ip">)</nts>
                  <nts id="Seg_1147" n="HIAT:ip">)</nts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1151" n="HIAT:u" s="T256">
                  <nts id="Seg_1152" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_1154" n="HIAT:w" s="T256">Miʔ-</ts>
                  <nts id="Seg_1155" n="HIAT:ip">)</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1158" n="HIAT:w" s="T257">Miʔnʼibeʔ</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1161" n="HIAT:w" s="T258">teinen</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1164" n="HIAT:w" s="T259">nüdʼin</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1167" n="HIAT:w" s="T260">tibi</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1170" n="HIAT:w" s="T261">nezi</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1173" n="HIAT:w" s="T262">bar</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1176" n="HIAT:w" s="T263">dʼabrobiʔi</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1180" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1182" n="HIAT:w" s="T264">Udagən</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1185" n="HIAT:w" s="T265">bar</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1187" n="HIAT:ip">(</nts>
                  <ts e="T267" id="Seg_1189" n="HIAT:w" s="T266">krom-</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1192" n="HIAT:w" s="T267">mʼa-</ts>
                  <nts id="Seg_1193" n="HIAT:ip">)</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1196" n="HIAT:w" s="T268">kem</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1199" n="HIAT:w" s="T269">mʼaŋnaʔbə</ts>
                  <nts id="Seg_1200" n="HIAT:ip">.</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1203" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1205" n="HIAT:w" s="T270">Esseŋdə</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1208" n="HIAT:w" s="T271">bar</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1211" n="HIAT:w" s="T272">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1215" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1217" n="HIAT:w" s="T273">Măn</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1220" n="HIAT:w" s="T274">šobiam</ts>
                  <nts id="Seg_1221" n="HIAT:ip">,</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1224" n="HIAT:w" s="T275">bar</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1227" n="HIAT:w" s="T276">kudonzəbiam</ts>
                  <nts id="Seg_1228" n="HIAT:ip">.</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1231" n="HIAT:u" s="T277">
                  <nts id="Seg_1232" n="HIAT:ip">(</nts>
                  <nts id="Seg_1233" n="HIAT:ip">(</nts>
                  <ats e="T278" id="Seg_1234" n="HIAT:non-pho" s="T277">BRK</ats>
                  <nts id="Seg_1235" n="HIAT:ip">)</nts>
                  <nts id="Seg_1236" n="HIAT:ip">)</nts>
                  <nts id="Seg_1237" n="HIAT:ip">.</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1240" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1242" n="HIAT:w" s="T278">Büzo</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1245" n="HIAT:w" s="T279">iʔbəbi</ts>
                  <nts id="Seg_1246" n="HIAT:ip">,</nts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1249" n="HIAT:w" s="T280">ej</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1252" n="HIAT:w" s="T281">kirgaria</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1255" n="HIAT:w" s="T282">tüj</ts>
                  <nts id="Seg_1256" n="HIAT:ip">.</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1259" n="HIAT:u" s="T283">
                  <nts id="Seg_1260" n="HIAT:ip">(</nts>
                  <nts id="Seg_1261" n="HIAT:ip">(</nts>
                  <ats e="T284" id="Seg_1262" n="HIAT:non-pho" s="T283">BRK</ats>
                  <nts id="Seg_1263" n="HIAT:ip">)</nts>
                  <nts id="Seg_1264" n="HIAT:ip">)</nts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1268" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1270" n="HIAT:w" s="T284">Možna</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1273" n="HIAT:w" s="T285">dʼăbaktərzittə</ts>
                  <nts id="Seg_1274" n="HIAT:ip">.</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1277" n="HIAT:u" s="T286">
                  <nts id="Seg_1278" n="HIAT:ip">(</nts>
                  <nts id="Seg_1279" n="HIAT:ip">(</nts>
                  <ats e="T287" id="Seg_1280" n="HIAT:non-pho" s="T286">BRK</ats>
                  <nts id="Seg_1281" n="HIAT:ip">)</nts>
                  <nts id="Seg_1282" n="HIAT:ip">)</nts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1286" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1288" n="HIAT:w" s="T287">Büzʼen</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1291" n="HIAT:w" s="T288">bar</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1294" n="HIAT:w" s="T289">püjet</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1297" n="HIAT:w" s="T290">tʼukumbi</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1301" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1303" n="HIAT:w" s="T291">Gijendə</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1306" n="HIAT:w" s="T292">ara</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1309" n="HIAT:w" s="T293">bĭtlem</ts>
                  <nts id="Seg_1310" n="HIAT:ip">.</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1313" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1315" n="HIAT:w" s="T294">Dĭgəttə</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1318" n="HIAT:w" s="T295">pograšsʼe</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1321" n="HIAT:w" s="T296">abi</ts>
                  <nts id="Seg_1322" n="HIAT:ip">,</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1325" n="HIAT:w" s="T297">amnobi</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1328" n="HIAT:w" s="T298">pagən</ts>
                  <nts id="Seg_1329" n="HIAT:ip">.</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1332" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1334" n="HIAT:w" s="T299">A</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1337" n="HIAT:w" s="T300">măn</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1340" n="HIAT:w" s="T301">urgajam</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1343" n="HIAT:w" s="T302">mĭmbi</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_1346" n="HIAT:w" s="T303">Kazan</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1349" n="HIAT:w" s="T852">turanə</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1352" n="HIAT:w" s="T304">i</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1355" n="HIAT:w" s="T305">ara</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1358" n="HIAT:w" s="T306">deʔpi</ts>
                  <nts id="Seg_1359" n="HIAT:ip">.</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1362" n="HIAT:u" s="T307">
                  <nts id="Seg_1363" n="HIAT:ip">(</nts>
                  <ts e="T308" id="Seg_1365" n="HIAT:w" s="T307">Dĭgəttə</ts>
                  <nts id="Seg_1366" n="HIAT:ip">)</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1369" n="HIAT:w" s="T308">amnəbi</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1372" n="HIAT:w" s="T309">tüʔsʼittə</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1374" n="HIAT:ip">(</nts>
                  <ts e="T311" id="Seg_1376" n="HIAT:w" s="T310">dĭʔ-</ts>
                  <nts id="Seg_1377" n="HIAT:ip">)</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1380" n="HIAT:w" s="T311">diʔnə</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1383" n="HIAT:w" s="T312">kötenzi</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1387" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1389" n="HIAT:w" s="T313">Dĭgəttə</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1392" n="HIAT:w" s="T314">dĭ</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1395" n="HIAT:w" s="T315">šobi</ts>
                  <nts id="Seg_1396" n="HIAT:ip">.</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1399" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1401" n="HIAT:w" s="T316">Dĭ</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1404" n="HIAT:w" s="T317">dĭʔnə</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1407" n="HIAT:w" s="T318">ara</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1410" n="HIAT:w" s="T319">mĭbi</ts>
                  <nts id="Seg_1411" n="HIAT:ip">,</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1414" n="HIAT:w" s="T320">dĭ</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1417" n="HIAT:w" s="T321">bĭʔpi</ts>
                  <nts id="Seg_1418" n="HIAT:ip">.</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1421" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1423" n="HIAT:w" s="T322">I</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1425" n="HIAT:ip">(</nts>
                  <ts e="T324" id="Seg_1427" n="HIAT:w" s="T323">măndə=</ts>
                  <nts id="Seg_1428" n="HIAT:ip">)</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1431" n="HIAT:w" s="T324">măndə</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1434" n="HIAT:w" s="T325">dĭʔnə:</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1437" n="HIAT:w" s="T326">Ne</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1440" n="HIAT:w" s="T327">dak</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1443" n="HIAT:w" s="T328">ne</ts>
                  <nts id="Seg_1444" n="HIAT:ip">,</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1447" n="HIAT:w" s="T329">ot</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1450" n="HIAT:w" s="T330">dak</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1453" n="HIAT:w" s="T331">ne</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1456" n="HIAT:w" s="T332">dak</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1459" n="HIAT:w" s="T333">ne</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1463" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1465" n="HIAT:w" s="T334">Ĭmbi</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1468" n="HIAT:w" s="T335">tăn</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1471" n="HIAT:w" s="T336">dăre</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1473" n="HIAT:ip">(</nts>
                  <ts e="T338" id="Seg_1475" n="HIAT:w" s="T337">m-</ts>
                  <nts id="Seg_1476" n="HIAT:ip">)</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1478" n="HIAT:ip">(</nts>
                  <nts id="Seg_1479" n="HIAT:ip">(</nts>
                  <ats e="T339" id="Seg_1480" n="HIAT:non-pho" s="T338">BRK</ats>
                  <nts id="Seg_1481" n="HIAT:ip">)</nts>
                  <nts id="Seg_1482" n="HIAT:ip">)</nts>
                  <nts id="Seg_1483" n="HIAT:ip">?</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1486" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1488" n="HIAT:w" s="T339">A</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1491" n="HIAT:w" s="T340">tăn</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1494" n="HIAT:w" s="T341">măna</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1496" n="HIAT:ip">(</nts>
                  <ts e="T343" id="Seg_1498" n="HIAT:w" s="T342">am-</ts>
                  <nts id="Seg_1499" n="HIAT:ip">)</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1502" n="HIAT:w" s="T343">kötenzi</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1505" n="HIAT:w" s="T344">amnobial</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1508" n="HIAT:w" s="T345">i</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1511" n="HIAT:w" s="T346">tüʔpiel</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1515" n="HIAT:u" s="T347">
                  <nts id="Seg_1516" n="HIAT:ip">(</nts>
                  <nts id="Seg_1517" n="HIAT:ip">(</nts>
                  <ats e="T348" id="Seg_1518" n="HIAT:non-pho" s="T347">BRK</ats>
                  <nts id="Seg_1519" n="HIAT:ip">)</nts>
                  <nts id="Seg_1520" n="HIAT:ip">)</nts>
                  <nts id="Seg_1521" n="HIAT:ip">.</nts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1524" n="HIAT:u" s="T348">
                  <nts id="Seg_1525" n="HIAT:ip">(</nts>
                  <ts e="T349" id="Seg_1527" n="HIAT:w" s="T348">Măr-</ts>
                  <nts id="Seg_1528" n="HIAT:ip">)</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1531" n="HIAT:w" s="T349">Măn</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1534" n="HIAT:w" s="T350">urgajam</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1537" n="HIAT:w" s="T351">šamandə</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1540" n="HIAT:w" s="T352">mĭmbiem</ts>
                  <nts id="Seg_1541" n="HIAT:ip">.</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1544" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1546" n="HIAT:w" s="T353">Dĭn</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1549" n="HIAT:w" s="T354">bar</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1551" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_1553" n="HIAT:w" s="T355">moi-</ts>
                  <nts id="Seg_1554" n="HIAT:ip">)</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1557" n="HIAT:w" s="T356">uja</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1560" n="HIAT:w" s="T357">mĭnzerbiʔi</ts>
                  <nts id="Seg_1561" n="HIAT:ip">,</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1564" n="HIAT:w" s="T358">takšenə</ts>
                  <nts id="Seg_1565" n="HIAT:ip">,</nts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1568" n="HIAT:w" s="T359">urgo</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1571" n="HIAT:w" s="T360">takše</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1574" n="HIAT:w" s="T361">embiʔi</ts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1578" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1580" n="HIAT:w" s="T362">Dĭ</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1583" n="HIAT:w" s="T363">bar</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1586" n="HIAT:w" s="T364">šĭšəge</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1589" n="HIAT:w" s="T365">molambi</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1593" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1595" n="HIAT:w" s="T366">Amnaʔ</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1597" n="HIAT:ip">(</nts>
                  <ts e="T368" id="Seg_1599" n="HIAT:w" s="T367">a-</ts>
                  <nts id="Seg_1600" n="HIAT:ip">)</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1603" n="HIAT:w" s="T368">amorzittə</ts>
                  <nts id="Seg_1604" n="HIAT:ip">!</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1607" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1609" n="HIAT:w" s="T369">Dĭ</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1612" n="HIAT:w" s="T370">bar</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1615" n="HIAT:w" s="T371">uja</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1618" n="HIAT:w" s="T372">ibi</ts>
                  <nts id="Seg_1619" n="HIAT:ip">,</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1622" n="HIAT:w" s="T373">dĭ</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1625" n="HIAT:w" s="T374">ugandə</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1628" n="HIAT:w" s="T375">šišəge</ts>
                  <nts id="Seg_1629" n="HIAT:ip">.</nts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1632" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1634" n="HIAT:w" s="T376">Dĭgəttə</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1637" n="HIAT:w" s="T377">mendə</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1639" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_1641" n="HIAT:w" s="T378">baʔ-</ts>
                  <nts id="Seg_1642" n="HIAT:ip">)</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1645" n="HIAT:w" s="T379">baʔluʔpi</ts>
                  <nts id="Seg_1646" n="HIAT:ip">.</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1649" n="HIAT:u" s="T380">
                  <nts id="Seg_1650" n="HIAT:ip">(</nts>
                  <nts id="Seg_1651" n="HIAT:ip">(</nts>
                  <ats e="T381" id="Seg_1652" n="HIAT:non-pho" s="T380">BRK</ats>
                  <nts id="Seg_1653" n="HIAT:ip">)</nts>
                  <nts id="Seg_1654" n="HIAT:ip">)</nts>
                  <nts id="Seg_1655" n="HIAT:ip">.</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1658" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1660" n="HIAT:w" s="T381">Bubenzi</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1663" n="HIAT:w" s="T382">bar</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1665" n="HIAT:ip">(</nts>
                  <ts e="T384" id="Seg_1667" n="HIAT:w" s="T383">š-</ts>
                  <nts id="Seg_1668" n="HIAT:ip">)</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1671" n="HIAT:w" s="T384">sʼarbi</ts>
                  <nts id="Seg_1672" n="HIAT:ip">.</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1675" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1677" n="HIAT:w" s="T385">Bar</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1680" n="HIAT:w" s="T386">ildə</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1682" n="HIAT:ip">(</nts>
                  <ts e="T388" id="Seg_1684" n="HIAT:w" s="T387">mĭmbi</ts>
                  <nts id="Seg_1685" n="HIAT:ip">)</nts>
                  <nts id="Seg_1686" n="HIAT:ip">,</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1689" n="HIAT:w" s="T388">a</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1692" n="HIAT:w" s="T389">măn</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1695" n="HIAT:w" s="T390">urgajanə</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1698" n="HIAT:w" s="T391">ej</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1701" n="HIAT:w" s="T392">mĭbiem</ts>
                  <nts id="Seg_1702" n="HIAT:ip">.</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1705" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1707" n="HIAT:w" s="T393">Dĭgəttə</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1710" n="HIAT:w" s="T394">mĭmbi</ts>
                  <nts id="Seg_1711" n="HIAT:ip">.</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1714" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1716" n="HIAT:w" s="T395">Iʔ</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1719" n="HIAT:w" s="T396">deʔkeʔ</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1722" n="HIAT:w" s="T397">dĭ</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1725" n="HIAT:w" s="T398">ne</ts>
                  <nts id="Seg_1726" n="HIAT:ip">,</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1729" n="HIAT:w" s="T399">kazak</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1732" n="HIAT:w" s="T400">ne</ts>
                  <nts id="Seg_1733" n="HIAT:ip">.</nts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1736" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1738" n="HIAT:w" s="T401">Забыла</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1740" n="HIAT:ip">(</nts>
                  <nts id="Seg_1741" n="HIAT:ip">(</nts>
                  <ats e="T403" id="Seg_1742" n="HIAT:non-pho" s="T402">BRK</ats>
                  <nts id="Seg_1743" n="HIAT:ip">)</nts>
                  <nts id="Seg_1744" n="HIAT:ip">)</nts>
                  <nts id="Seg_1745" n="HIAT:ip">.</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1748" n="HIAT:u" s="T403">
                  <nts id="Seg_1749" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1751" n="HIAT:w" s="T403">Šamangən</ts>
                  <nts id="Seg_1752" n="HIAT:ip">)</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1755" n="HIAT:w" s="T404">bar</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1758" n="HIAT:w" s="T405">părga</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1761" n="HIAT:w" s="T406">ibi</ts>
                  <nts id="Seg_1762" n="HIAT:ip">.</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_1765" n="HIAT:u" s="T407">
                  <ts e="T409" id="Seg_1767" n="HIAT:w" s="T407">Всякий-всякий</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1770" n="HIAT:w" s="T409">был</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1773" n="HIAT:w" s="T410">bar</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1775" n="HIAT:ip">(</nts>
                  <ts e="T412" id="Seg_1777" n="HIAT:w" s="T411">kuriz-</ts>
                  <nts id="Seg_1778" n="HIAT:ip">)</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1781" n="HIAT:w" s="T412">kurizən</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1784" n="HIAT:w" s="T413">kuba</ts>
                  <nts id="Seg_1785" n="HIAT:ip">,</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1788" n="HIAT:w" s="T414">albugan</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1791" n="HIAT:w" s="T415">kuba</ts>
                  <nts id="Seg_1792" n="HIAT:ip">,</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1795" n="HIAT:w" s="T416">părgagən</ts>
                  <nts id="Seg_1796" n="HIAT:ip">,</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1799" n="HIAT:w" s="T417">tažəbən</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1801" n="HIAT:ip">(</nts>
                  <ts e="T419" id="Seg_1803" n="HIAT:w" s="T418">p-</ts>
                  <nts id="Seg_1804" n="HIAT:ip">)</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1807" n="HIAT:w" s="T419">kuba</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1810" n="HIAT:w" s="T420">părgagən</ts>
                  <nts id="Seg_1811" n="HIAT:ip">.</nts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1814" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1816" n="HIAT:w" s="T422">Poʔton</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1819" n="HIAT:w" s="T423">kuba</ts>
                  <nts id="Seg_1820" n="HIAT:ip">.</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1823" n="HIAT:u" s="T424">
                  <nts id="Seg_1824" n="HIAT:ip">(</nts>
                  <nts id="Seg_1825" n="HIAT:ip">(</nts>
                  <ats e="T425" id="Seg_1826" n="HIAT:non-pho" s="T424">BRK</ats>
                  <nts id="Seg_1827" n="HIAT:ip">)</nts>
                  <nts id="Seg_1828" n="HIAT:ip">)</nts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1832" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1834" n="HIAT:w" s="T425">Dĭ</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1837" n="HIAT:w" s="T426">šaman</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1840" n="HIAT:w" s="T427">bar</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1843" n="HIAT:w" s="T428">шаманил</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1846" n="HIAT:w" s="T429">-</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1849" n="HIAT:w" s="T430">шаманил</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1853" n="HIAT:w" s="T431">dĭgəttə</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1856" n="HIAT:w" s="T432">dagaj</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1859" n="HIAT:w" s="T433">ibi</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1862" n="HIAT:w" s="T434">da</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1865" n="HIAT:w" s="T435">bar</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1868" n="HIAT:w" s="T436">müʔbdəbi</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1870" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_1872" n="HIAT:w" s="T437">siʔ-</ts>
                  <nts id="Seg_1873" n="HIAT:ip">)</nts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1876" n="HIAT:w" s="T438">sĭjdə</ts>
                  <nts id="Seg_1877" n="HIAT:ip">.</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1880" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1882" n="HIAT:w" s="T439">I</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1885" n="HIAT:w" s="T440">dĭgəttə</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1888" n="HIAT:w" s="T441">saʔməluʔpi</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1892" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1894" n="HIAT:w" s="T442">A</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1897" n="HIAT:w" s="T443">il</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1900" n="HIAT:w" s="T444">iʔgö</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1903" n="HIAT:w" s="T445">šonəgaʔi</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1906" n="HIAT:w" s="T446">быдто</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1908" n="HIAT:ip">(</nts>
                  <ts e="T448" id="Seg_1910" n="HIAT:w" s="T447">s</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1913" n="HIAT:w" s="T448">-</ts>
                  <nts id="Seg_1914" n="HIAT:ip">)</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1917" n="HIAT:w" s="T449">Минусинские</ts>
                  <nts id="Seg_1918" n="HIAT:ip">.</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1921" n="HIAT:u" s="T450">
                  <ts e="T450.tx.1" id="Seg_1923" n="HIAT:w" s="T450">По-русски</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1926" n="HIAT:w" s="T450.tx.1">сказала</ts>
                  <nts id="Seg_1927" n="HIAT:ip">…</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1930" n="HIAT:u" s="T455">
                  <nts id="Seg_1931" n="HIAT:ip">(</nts>
                  <nts id="Seg_1932" n="HIAT:ip">(</nts>
                  <ats e="T456" id="Seg_1933" n="HIAT:non-pho" s="T455">BRK</ats>
                  <nts id="Seg_1934" n="HIAT:ip">)</nts>
                  <nts id="Seg_1935" n="HIAT:ip">)</nts>
                  <nts id="Seg_1936" n="HIAT:ip">.</nts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1939" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1941" n="HIAT:w" s="T456">Tibiʔi</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1943" n="HIAT:ip">(</nts>
                  <ts e="T458" id="Seg_1945" n="HIAT:w" s="T457">ka-</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1948" n="HIAT:w" s="T458">ka-</ts>
                  <nts id="Seg_1949" n="HIAT:ip">)</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1952" n="HIAT:w" s="T459">kamen</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1955" n="HIAT:w" s="T460">kallaʔi</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1958" n="HIAT:w" s="T461">dʼijenə</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1961" n="HIAT:w" s="T462">dĭgəttə</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1964" n="HIAT:w" s="T463">šamandə</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1967" n="HIAT:w" s="T464">kallaʔi</ts>
                  <nts id="Seg_1968" n="HIAT:ip">.</nts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1971" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1973" n="HIAT:w" s="T465">Dĭ</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1976" n="HIAT:w" s="T466">mălləj:</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1979" n="HIAT:w" s="T467">Tăn</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1981" n="HIAT:ip">(</nts>
                  <ts e="T469" id="Seg_1983" n="HIAT:w" s="T468">u-</ts>
                  <nts id="Seg_1984" n="HIAT:ip">)</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1987" n="HIAT:w" s="T469">albuga</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1990" n="HIAT:w" s="T470">kutlal</ts>
                  <nts id="Seg_1991" n="HIAT:ip">,</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1994" n="HIAT:w" s="T471">a</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1997" n="HIAT:w" s="T472">šindinə</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_2000" n="HIAT:w" s="T473">mălləj:</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2003" n="HIAT:w" s="T474">Ej</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_2006" n="HIAT:w" s="T475">kutlal</ts>
                  <nts id="Seg_2007" n="HIAT:ip">"</nts>
                  <nts id="Seg_2008" n="HIAT:ip">.</nts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_2011" n="HIAT:u" s="T476">
                  <nts id="Seg_2012" n="HIAT:ip">(</nts>
                  <nts id="Seg_2013" n="HIAT:ip">(</nts>
                  <ats e="T477" id="Seg_2014" n="HIAT:non-pho" s="T476">BRK</ats>
                  <nts id="Seg_2015" n="HIAT:ip">)</nts>
                  <nts id="Seg_2016" n="HIAT:ip">)</nts>
                  <nts id="Seg_2017" n="HIAT:ip">.</nts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_2020" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_2022" n="HIAT:w" s="T477">Минусинский</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_2025" n="HIAT:w" s="T478">šaman</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_2028" n="HIAT:w" s="T479">i</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_2031" n="HIAT:w" s="T480">miʔ</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_2034" n="HIAT:w" s="T481">šaman</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_2037" n="HIAT:w" s="T482">bar</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_2040" n="HIAT:w" s="T483">dʼabrobiʔi</ts>
                  <nts id="Seg_2041" n="HIAT:ip">.</nts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_2044" n="HIAT:u" s="T484">
                  <nts id="Seg_2045" n="HIAT:ip">(</nts>
                  <ts e="T485" id="Seg_2047" n="HIAT:w" s="T484">Minu-</ts>
                  <nts id="Seg_2048" n="HIAT:ip">)</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_2051" n="HIAT:w" s="T485">Минусинский</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_2054" n="HIAT:w" s="T486">kambi</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_2057" n="HIAT:w" s="T487">maʔndə</ts>
                  <nts id="Seg_2058" n="HIAT:ip">,</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_2061" n="HIAT:w" s="T488">a</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2064" n="HIAT:w" s="T489">miʔ</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2067" n="HIAT:w" s="T490">šaman</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2069" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_2071" n="HIAT:w" s="T491">dĭ-</ts>
                  <nts id="Seg_2072" n="HIAT:ip">)</nts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2075" n="HIAT:w" s="T492">dĭm</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2078" n="HIAT:w" s="T493">bar</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2081" n="HIAT:w" s="T494">bĭdleʔpi</ts>
                  <nts id="Seg_2082" n="HIAT:ip">.</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_2085" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_2087" n="HIAT:w" s="T495">Pograš</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2090" n="HIAT:w" s="T496">molambi</ts>
                  <nts id="Seg_2091" n="HIAT:ip">.</nts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_2094" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_2096" n="HIAT:w" s="T497">A</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2099" n="HIAT:w" s="T498">dĭ</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2102" n="HIAT:w" s="T499">bar</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2105" n="HIAT:w" s="T500">multuksi</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2108" n="HIAT:w" s="T501">dĭm</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2111" n="HIAT:w" s="T502">kutlambi</ts>
                  <nts id="Seg_2112" n="HIAT:ip">.</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2115" n="HIAT:u" s="T503">
                  <nts id="Seg_2116" n="HIAT:ip">(</nts>
                  <ts e="T504" id="Seg_2118" n="HIAT:w" s="T503">Udanə</ts>
                  <nts id="Seg_2119" n="HIAT:ip">)</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2122" n="HIAT:w" s="T504">Udabə</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2125" n="HIAT:w" s="T505">bar</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2128" n="HIAT:w" s="T506">băldəbi</ts>
                  <nts id="Seg_2129" n="HIAT:ip">.</nts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2132" n="HIAT:u" s="T507">
                  <ts e="T507.tx.1" id="Seg_2134" n="HIAT:w" s="T507">Руку</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2137" n="HIAT:w" s="T507.tx.1">сломал</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_2141" n="HIAT:u" s="T509">
                  <nts id="Seg_2142" n="HIAT:ip">(</nts>
                  <nts id="Seg_2143" n="HIAT:ip">(</nts>
                  <ats e="T510" id="Seg_2144" n="HIAT:non-pho" s="T509">BRK</ats>
                  <nts id="Seg_2145" n="HIAT:ip">)</nts>
                  <nts id="Seg_2146" n="HIAT:ip">)</nts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_2150" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_2152" n="HIAT:w" s="T510">Girgit</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2155" n="HIAT:w" s="T511">kuza</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2158" n="HIAT:w" s="T512">ĭzemnuʔpi</ts>
                  <nts id="Seg_2159" n="HIAT:ip">,</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2162" n="HIAT:w" s="T513">dĭgəttə</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2165" n="HIAT:w" s="T514">kalaʔi</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2168" n="HIAT:w" s="T515">šamandə</ts>
                  <nts id="Seg_2169" n="HIAT:ip">,</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2172" n="HIAT:w" s="T516">dĭ</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2175" n="HIAT:w" s="T517">bar</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2178" n="HIAT:w" s="T518">dĭm</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2181" n="HIAT:w" s="T519">dʼaziria</ts>
                  <nts id="Seg_2182" n="HIAT:ip">.</nts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2185" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_2187" n="HIAT:w" s="T520">Lʼevăj</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2190" n="HIAT:w" s="T521">udandə</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2192" n="HIAT:ip">(</nts>
                  <ts e="T523" id="Seg_2194" n="HIAT:w" s="T522">nuldləj</ts>
                  <nts id="Seg_2195" n="HIAT:ip">)</nts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2197" n="HIAT:ip">(</nts>
                  <ts e="T524" id="Seg_2199" n="HIAT:w" s="T523">dĭ</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2202" n="HIAT:w" s="T524">üge</ts>
                  <nts id="Seg_2203" n="HIAT:ip">)</nts>
                  <nts id="Seg_2204" n="HIAT:ip">.</nts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2207" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2209" n="HIAT:w" s="T525">Dăre</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2212" n="HIAT:w" s="T526">bar</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2215" n="HIAT:w" s="T527">ulutsi</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2217" n="HIAT:ip">(</nts>
                  <ts e="T529" id="Seg_2219" n="HIAT:w" s="T528">numa-</ts>
                  <nts id="Seg_2220" n="HIAT:ip">)</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2223" n="HIAT:w" s="T529">кланяется</ts>
                  <nts id="Seg_2224" n="HIAT:ip">,</nts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2227" n="HIAT:w" s="T530">кланяется</ts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2231" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2233" n="HIAT:w" s="T531">Dĭgəttə</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2236" n="HIAT:w" s="T532">dĭ</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2239" n="HIAT:w" s="T533">dĭʔnə</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2242" n="HIAT:w" s="T534">tʼažerluʔpi</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2245" n="HIAT:w" s="T535">i</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2248" n="HIAT:w" s="T536">bostə</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2251" n="HIAT:w" s="T537">bar</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2254" n="HIAT:w" s="T538">suʔmileʔbə</ts>
                  <nts id="Seg_2255" n="HIAT:ip">,</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2258" n="HIAT:w" s="T539">bubendə</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2261" n="HIAT:w" s="T540">sʼarlaʔbə</ts>
                  <nts id="Seg_2262" n="HIAT:ip">.</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2265" n="HIAT:u" s="T541">
                  <nts id="Seg_2266" n="HIAT:ip">(</nts>
                  <nts id="Seg_2267" n="HIAT:ip">(</nts>
                  <ats e="T542" id="Seg_2268" n="HIAT:non-pho" s="T541">BRK</ats>
                  <nts id="Seg_2269" n="HIAT:ip">)</nts>
                  <nts id="Seg_2270" n="HIAT:ip">)</nts>
                  <nts id="Seg_2271" n="HIAT:ip">.</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2274" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2276" n="HIAT:w" s="T542">Dĭgəttə</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2279" n="HIAT:w" s="T543">dĭ</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2282" n="HIAT:w" s="T544">ej</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2285" n="HIAT:w" s="T545">ĭzemnie</ts>
                  <nts id="Seg_2286" n="HIAT:ip">,</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2289" n="HIAT:w" s="T546">maʔndə</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2292" n="HIAT:w" s="T547">šoləj</ts>
                  <nts id="Seg_2293" n="HIAT:ip">.</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2296" n="HIAT:u" s="T548">
                  <nts id="Seg_2297" n="HIAT:ip">(</nts>
                  <nts id="Seg_2298" n="HIAT:ip">(</nts>
                  <ats e="T549" id="Seg_2299" n="HIAT:non-pho" s="T548">BRK</ats>
                  <nts id="Seg_2300" n="HIAT:ip">)</nts>
                  <nts id="Seg_2301" n="HIAT:ip">)</nts>
                  <nts id="Seg_2302" n="HIAT:ip">.</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2305" n="HIAT:u" s="T549">
                  <nts id="Seg_2306" n="HIAT:ip">(</nts>
                  <nts id="Seg_2307" n="HIAT:ip">(</nts>
                  <ats e="T550" id="Seg_2308" n="HIAT:non-pho" s="T549">BRK</ats>
                  <nts id="Seg_2309" n="HIAT:ip">)</nts>
                  <nts id="Seg_2310" n="HIAT:ip">)</nts>
                  <nts id="Seg_2311" n="HIAT:ip">.</nts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2314" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2316" n="HIAT:w" s="T550">Tibizeŋ</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2319" n="HIAT:w" s="T551">dʼijenə</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2322" n="HIAT:w" s="T552">kalla</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2325" n="HIAT:w" s="T848">dʼürbiʔi</ts>
                  <nts id="Seg_2326" n="HIAT:ip">,</nts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2329" n="HIAT:w" s="T553">nezeŋ</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2331" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2333" n="HIAT:w" s="T554">maʔən</ts>
                  <nts id="Seg_2334" n="HIAT:ip">)</nts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2337" n="HIAT:w" s="T555">amnobiʔi</ts>
                  <nts id="Seg_2338" n="HIAT:ip">.</nts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2341" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2343" n="HIAT:w" s="T556">Sarankaʔi</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2346" n="HIAT:w" s="T557">tĭlbiʔi</ts>
                  <nts id="Seg_2347" n="HIAT:ip">,</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2350" n="HIAT:w" s="T558">ujam</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2352" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_2354" n="HIAT:w" s="T559">embiʔi</ts>
                  <nts id="Seg_2355" n="HIAT:ip">)</nts>
                  <nts id="Seg_2356" n="HIAT:ip">,</nts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2359" n="HIAT:w" s="T560">ipek</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2362" n="HIAT:w" s="T561">naga</ts>
                  <nts id="Seg_2363" n="HIAT:ip">.</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2366" n="HIAT:u" s="T562">
                  <nts id="Seg_2367" n="HIAT:ip">(</nts>
                  <nts id="Seg_2368" n="HIAT:ip">(</nts>
                  <ats e="T563" id="Seg_2369" n="HIAT:non-pho" s="T562">BRK</ats>
                  <nts id="Seg_2370" n="HIAT:ip">)</nts>
                  <nts id="Seg_2371" n="HIAT:ip">)</nts>
                  <nts id="Seg_2372" n="HIAT:ip">.</nts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2375" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2377" n="HIAT:w" s="T563">Nezeŋ</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2380" n="HIAT:w" s="T564">bar</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2382" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_2384" n="HIAT:w" s="T565">n-</ts>
                  <nts id="Seg_2385" n="HIAT:ip">)</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2388" n="HIAT:w" s="T566">noʔ</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2391" n="HIAT:w" s="T567">nĭŋgəbiʔi</ts>
                  <nts id="Seg_2392" n="HIAT:ip">.</nts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2395" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2397" n="HIAT:w" s="T568">Travʼanʼigaʔi</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2400" n="HIAT:w" s="T569">kürbiʔi</ts>
                  <nts id="Seg_2401" n="HIAT:ip">,</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2404" n="HIAT:w" s="T570">jamaʔi</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2407" n="HIAT:w" s="T571">šöʔpiʔi</ts>
                  <nts id="Seg_2408" n="HIAT:ip">,</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2411" n="HIAT:w" s="T572">žilaʔi</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2414" n="HIAT:w" s="T573">kürbiʔi</ts>
                  <nts id="Seg_2415" n="HIAT:ip">.</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2418" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2420" n="HIAT:w" s="T574">Dʼijegən</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2423" n="HIAT:w" s="T575">maʔ</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2425" n="HIAT:ip">(</nts>
                  <ts e="T577" id="Seg_2427" n="HIAT:w" s="T576">ab-</ts>
                  <nts id="Seg_2428" n="HIAT:ip">)</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2431" n="HIAT:w" s="T577">abiʔi</ts>
                  <nts id="Seg_2432" n="HIAT:ip">.</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2435" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2437" n="HIAT:w" s="T578">Dĭgəttə</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2440" n="HIAT:w" s="T579">dĭ</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2443" n="HIAT:w" s="T580">maʔkən</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2446" n="HIAT:w" s="T581">uja</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2449" n="HIAT:w" s="T582">mĭnzerbiʔi</ts>
                  <nts id="Seg_2450" n="HIAT:ip">.</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2453" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2455" n="HIAT:w" s="T583">Segi</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2458" n="HIAT:w" s="T584">bü</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2461" n="HIAT:w" s="T585">mĭnzerbiʔi</ts>
                  <nts id="Seg_2462" n="HIAT:ip">,</nts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2465" n="HIAT:w" s="T586">amorbiʔi</ts>
                  <nts id="Seg_2466" n="HIAT:ip">.</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2469" n="HIAT:u" s="T587">
                  <nts id="Seg_2470" n="HIAT:ip">(</nts>
                  <nts id="Seg_2471" n="HIAT:ip">(</nts>
                  <ats e="T588" id="Seg_2472" n="HIAT:non-pho" s="T587">BRK</ats>
                  <nts id="Seg_2473" n="HIAT:ip">)</nts>
                  <nts id="Seg_2474" n="HIAT:ip">)</nts>
                  <nts id="Seg_2475" n="HIAT:ip">.</nts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2478" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2480" n="HIAT:w" s="T588">Turaʔi</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2483" n="HIAT:w" s="T589">ibiʔi</ts>
                  <nts id="Seg_2484" n="HIAT:ip">,</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2487" n="HIAT:w" s="T590">dĭ</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2490" n="HIAT:w" s="T591">turagən</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2493" n="HIAT:w" s="T592">ej</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2495" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_2497" n="HIAT:w" s="T593">amnobiʔi</ts>
                  <nts id="Seg_2498" n="HIAT:ip">)</nts>
                  <nts id="Seg_2499" n="HIAT:ip">.</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2502" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2504" n="HIAT:w" s="T594">Nʼiʔnen</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2507" n="HIAT:w" s="T595">maʔ</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2510" n="HIAT:w" s="T596">ibi</ts>
                  <nts id="Seg_2511" n="HIAT:ip">.</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2514" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2516" n="HIAT:w" s="T597">Dĭn</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2519" n="HIAT:w" s="T598">mĭnzerbiʔi</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2522" n="HIAT:w" s="T599">mĭj</ts>
                  <nts id="Seg_2523" n="HIAT:ip">.</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2526" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2528" n="HIAT:w" s="T600">Segi</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2531" n="HIAT:w" s="T601">bü</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2534" n="HIAT:w" s="T602">mĭnzerbiʔi</ts>
                  <nts id="Seg_2535" n="HIAT:ip">.</nts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2538" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2540" n="HIAT:w" s="T603">Amorbiʔi</ts>
                  <nts id="Seg_2541" n="HIAT:ip">,</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2544" n="HIAT:w" s="T604">a</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2547" n="HIAT:w" s="T605">turagən</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2550" n="HIAT:w" s="T606">kunolbiʔi</ts>
                  <nts id="Seg_2551" n="HIAT:ip">.</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2554" n="HIAT:u" s="T607">
                  <nts id="Seg_2555" n="HIAT:ip">(</nts>
                  <nts id="Seg_2556" n="HIAT:ip">(</nts>
                  <ats e="T608" id="Seg_2557" n="HIAT:non-pho" s="T607">BRK</ats>
                  <nts id="Seg_2558" n="HIAT:ip">)</nts>
                  <nts id="Seg_2559" n="HIAT:ip">)</nts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2563" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2565" n="HIAT:w" s="T608">Maʔi</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2568" n="HIAT:w" s="T609">abiʔi</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2570" n="HIAT:ip">(</nts>
                  <ts e="T611" id="Seg_2572" n="HIAT:w" s="T610">pats-</ts>
                  <nts id="Seg_2573" n="HIAT:ip">)</nts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2576" n="HIAT:w" s="T611">paʔi</ts>
                  <nts id="Seg_2577" n="HIAT:ip">,</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2580" n="HIAT:w" s="T612">dĭgəttə</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2583" n="HIAT:w" s="T613">kuba</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2586" n="HIAT:w" s="T614">kürbiʔi</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2589" n="HIAT:w" s="T615">patsi</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2592" n="HIAT:w" s="T616">da</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2595" n="HIAT:w" s="T617">enbiʔi</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2598" n="HIAT:w" s="T618">diʔnə</ts>
                  <nts id="Seg_2599" n="HIAT:ip">.</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_2602" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2604" n="HIAT:w" s="T619">Dĭgəttə</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2607" n="HIAT:w" s="T620">taganəʔi</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2610" n="HIAT:w" s="T621">abiʔi</ts>
                  <nts id="Seg_2611" n="HIAT:ip">,</nts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2614" n="HIAT:w" s="T622">dĭgəttə</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2617" n="HIAT:w" s="T623">aspaʔi</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2620" n="HIAT:w" s="T624">edəbiʔi</ts>
                  <nts id="Seg_2621" n="HIAT:ip">,</nts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2623" n="HIAT:ip">(</nts>
                  <ts e="T626" id="Seg_2625" n="HIAT:w" s="T625">mĭnzer-</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2628" n="HIAT:w" s="T626">mĭnzeri-</ts>
                  <nts id="Seg_2629" n="HIAT:ip">)</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2632" n="HIAT:w" s="T627">mĭnzerzittə</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2635" n="HIAT:w" s="T628">uja</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2638" n="HIAT:w" s="T629">da</ts>
                  <nts id="Seg_2639" n="HIAT:ip">,</nts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2642" n="HIAT:w" s="T630">segi</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2645" n="HIAT:w" s="T631">bü</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2648" n="HIAT:w" s="T632">mĭnzerzittə</ts>
                  <nts id="Seg_2649" n="HIAT:ip">.</nts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2652" n="HIAT:u" s="T633">
                  <ts e="T635" id="Seg_2654" n="HIAT:w" s="T633">Dĭgəttə</ts>
                  <nts id="Seg_2655" n="HIAT:ip">…</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2658" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2660" n="HIAT:w" s="T635">Стой</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2662" n="HIAT:ip">(</nts>
                  <nts id="Seg_2663" n="HIAT:ip">(</nts>
                  <ats e="T637" id="Seg_2664" n="HIAT:non-pho" s="T636">BRK</ats>
                  <nts id="Seg_2665" n="HIAT:ip">)</nts>
                  <nts id="Seg_2666" n="HIAT:ip">)</nts>
                  <nts id="Seg_2667" n="HIAT:ip">.</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2670" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2672" n="HIAT:w" s="T637">Dʼügən</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2675" n="HIAT:w" s="T638">šü</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2678" n="HIAT:w" s="T639">ambiʔi</ts>
                  <nts id="Seg_2679" n="HIAT:ip">.</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2682" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2684" n="HIAT:w" s="T640">Nʼüʔnən</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2687" n="HIAT:w" s="T641">ši</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2690" n="HIAT:w" s="T642">ibi</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2693" n="HIAT:w" s="T643">dĭbər</ts>
                  <nts id="Seg_2694" n="HIAT:ip">,</nts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2696" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_2698" n="HIAT:w" s="T644">be-</ts>
                  <nts id="Seg_2699" n="HIAT:ip">)</nts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2702" n="HIAT:w" s="T645">ber</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2705" n="HIAT:w" s="T646">kandlaʔbə</ts>
                  <nts id="Seg_2706" n="HIAT:ip">.</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2709" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2711" n="HIAT:w" s="T647">Dĭgəttə</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2714" n="HIAT:w" s="T648">paʔi</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2717" n="HIAT:w" s="T649">ambiʔi</ts>
                  <nts id="Seg_2718" n="HIAT:ip">,</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2721" n="HIAT:w" s="T650">štobɨ</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2724" n="HIAT:w" s="T651">šünə</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2727" n="HIAT:w" s="T652">šindidə</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2730" n="HIAT:w" s="T653">ej</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2732" n="HIAT:ip">(</nts>
                  <ts e="T655" id="Seg_2734" n="HIAT:w" s="T654">š-</ts>
                  <nts id="Seg_2735" n="HIAT:ip">)</nts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2738" n="HIAT:w" s="T655">saʔməbi</ts>
                  <nts id="Seg_2739" n="HIAT:ip">.</nts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2742" n="HIAT:u" s="T656">
                  <nts id="Seg_2743" n="HIAT:ip">(</nts>
                  <nts id="Seg_2744" n="HIAT:ip">(</nts>
                  <ats e="T657" id="Seg_2745" n="HIAT:non-pho" s="T656">BRK</ats>
                  <nts id="Seg_2746" n="HIAT:ip">)</nts>
                  <nts id="Seg_2747" n="HIAT:ip">)</nts>
                  <nts id="Seg_2748" n="HIAT:ip">.</nts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2751" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2753" n="HIAT:w" s="T657">Šišəge</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2756" n="HIAT:w" s="T658">mobi</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2759" n="HIAT:w" s="T659">dak</ts>
                  <nts id="Seg_2760" n="HIAT:ip">,</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2762" n="HIAT:ip">(</nts>
                  <ts e="T661" id="Seg_2764" n="HIAT:w" s="T660">kožaʔi=</ts>
                  <nts id="Seg_2765" n="HIAT:ip">)</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2768" n="HIAT:w" s="T661">kožazi</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2771" n="HIAT:w" s="T662">kajbiʔi</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2773" n="HIAT:ip">(</nts>
                  <ts e="T664" id="Seg_2775" n="HIAT:w" s="T663">a=</ts>
                  <nts id="Seg_2776" n="HIAT:ip">)</nts>
                  <nts id="Seg_2777" n="HIAT:ip">.</nts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2780" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2782" n="HIAT:w" s="T664">Ejü</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2785" n="HIAT:w" s="T665">mobi</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2788" n="HIAT:w" s="T666">dak</ts>
                  <nts id="Seg_2789" n="HIAT:ip">,</nts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2791" n="HIAT:ip">(</nts>
                  <ts e="T668" id="Seg_2793" n="HIAT:w" s="T667">ku-</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2796" n="HIAT:w" s="T668">kürbiʔi=</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2799" n="HIAT:w" s="T669">kambiʔ-</ts>
                  <nts id="Seg_2800" n="HIAT:ip">)</nts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2803" n="HIAT:w" s="T670">kajbiʔi</ts>
                  <nts id="Seg_2804" n="HIAT:ip">.</nts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2807" n="HIAT:u" s="T671">
                  <nts id="Seg_2808" n="HIAT:ip">(</nts>
                  <nts id="Seg_2809" n="HIAT:ip">(</nts>
                  <ats e="T672" id="Seg_2810" n="HIAT:non-pho" s="T671">BRK</ats>
                  <nts id="Seg_2811" n="HIAT:ip">)</nts>
                  <nts id="Seg_2812" n="HIAT:ip">)</nts>
                  <nts id="Seg_2813" n="HIAT:ip">.</nts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2816" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2818" n="HIAT:w" s="T672">Urgo</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2821" n="HIAT:w" s="T673">ibi</ts>
                  <nts id="Seg_2822" n="HIAT:ip">,</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2824" n="HIAT:ip">(</nts>
                  <ts e="T675" id="Seg_2826" n="HIAT:w" s="T674">me-</ts>
                  <nts id="Seg_2827" n="HIAT:ip">)</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2830" n="HIAT:w" s="T675">метра</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2833" n="HIAT:w" s="T676">teʔtə</ts>
                  <nts id="Seg_2834" n="HIAT:ip">.</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2837" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2839" n="HIAT:w" s="T677">Il</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2842" n="HIAT:w" s="T678">iʔbobiʔi</ts>
                  <nts id="Seg_2843" n="HIAT:ip">,</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2846" n="HIAT:w" s="T679">bʼeʔ</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2849" n="HIAT:w" s="T680">il</ts>
                  <nts id="Seg_2850" n="HIAT:ip">.</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2853" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2855" n="HIAT:w" s="T681">Kamen</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2858" n="HIAT:w" s="T682">sĭre</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2861" n="HIAT:w" s="T683">ibi</ts>
                  <nts id="Seg_2862" n="HIAT:ip">,</nts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2865" n="HIAT:w" s="T684">dĭzeŋ</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2868" n="HIAT:w" s="T685">dön</ts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2871" n="HIAT:w" s="T686">amnobiʔi</ts>
                  <nts id="Seg_2872" n="HIAT:ip">.</nts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2875" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2877" n="HIAT:w" s="T687">A</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2880" n="HIAT:w" s="T688">kamen</ts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2883" n="HIAT:w" s="T689">sĭre</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2886" n="HIAT:w" s="T690">kalla</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2889" n="HIAT:w" s="T849">dʼürbi</ts>
                  <nts id="Seg_2890" n="HIAT:ip">,</nts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2893" n="HIAT:w" s="T691">dĭgəttə</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2896" n="HIAT:w" s="T692">dĭzeŋ</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2899" n="HIAT:w" s="T693">kandəgaʔi</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2902" n="HIAT:w" s="T694">dʼijenə</ts>
                  <nts id="Seg_2903" n="HIAT:ip">,</nts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2906" n="HIAT:w" s="T695">urgo</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2909" n="HIAT:w" s="T696">măjanə</ts>
                  <nts id="Seg_2910" n="HIAT:ip">.</nts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2913" n="HIAT:u" s="T697">
                  <nts id="Seg_2914" n="HIAT:ip">(</nts>
                  <nts id="Seg_2915" n="HIAT:ip">(</nts>
                  <ats e="T698" id="Seg_2916" n="HIAT:non-pho" s="T697">BRK</ats>
                  <nts id="Seg_2917" n="HIAT:ip">)</nts>
                  <nts id="Seg_2918" n="HIAT:ip">)</nts>
                  <nts id="Seg_2919" n="HIAT:ip">.</nts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2922" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_2924" n="HIAT:w" s="T698">Ălenʼəʔizi</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2927" n="HIAT:w" s="T699">mĭmbiʔi</ts>
                  <nts id="Seg_2928" n="HIAT:ip">,</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2931" n="HIAT:w" s="T700">Tʼelamdə</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2934" n="HIAT:w" s="T701">kanbiʔi</ts>
                  <nts id="Seg_2935" n="HIAT:ip">,</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2938" n="HIAT:w" s="T702">dĭn</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2941" n="HIAT:w" s="T703">urgo</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2944" n="HIAT:w" s="T704">bü</ts>
                  <nts id="Seg_2945" n="HIAT:ip">,</nts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2948" n="HIAT:w" s="T705">kola</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2951" n="HIAT:w" s="T706">dʼaʔpiʔi</ts>
                  <nts id="Seg_2952" n="HIAT:ip">.</nts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2955" n="HIAT:u" s="T707">
                  <nts id="Seg_2956" n="HIAT:ip">(</nts>
                  <nts id="Seg_2957" n="HIAT:ip">(</nts>
                  <ats e="T708" id="Seg_2958" n="HIAT:non-pho" s="T707">BRK</ats>
                  <nts id="Seg_2959" n="HIAT:ip">)</nts>
                  <nts id="Seg_2960" n="HIAT:ip">)</nts>
                  <nts id="Seg_2961" n="HIAT:ip">.</nts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2964" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2966" n="HIAT:w" s="T708">Dĭgəttə</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2969" n="HIAT:w" s="T709">dĭzeŋ</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2972" n="HIAT:w" s="T710">kambiʔi</ts>
                  <nts id="Seg_2973" n="HIAT:ip">,</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2976" n="HIAT:w" s="T711">esseŋdə</ts>
                  <nts id="Seg_2977" n="HIAT:ip">,</nts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2980" n="HIAT:w" s="T712">i</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2983" n="HIAT:w" s="T713">nezeŋdə</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2986" n="HIAT:w" s="T714">ibiʔi</ts>
                  <nts id="Seg_2987" n="HIAT:ip">,</nts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2990" n="HIAT:w" s="T715">ălenʼəʔizi</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2993" n="HIAT:w" s="T716">kambiʔi</ts>
                  <nts id="Seg_2994" n="HIAT:ip">.</nts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2997" n="HIAT:u" s="T717">
                  <nts id="Seg_2998" n="HIAT:ip">(</nts>
                  <nts id="Seg_2999" n="HIAT:ip">(</nts>
                  <ats e="T718" id="Seg_3000" n="HIAT:non-pho" s="T717">BRK</ats>
                  <nts id="Seg_3001" n="HIAT:ip">)</nts>
                  <nts id="Seg_3002" n="HIAT:ip">)</nts>
                  <nts id="Seg_3003" n="HIAT:ip">.</nts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_3006" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_3008" n="HIAT:w" s="T718">I</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_3011" n="HIAT:w" s="T719">menzeŋzi</ts>
                  <nts id="Seg_3012" n="HIAT:ip">.</nts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_3015" n="HIAT:u" s="T720">
                  <nts id="Seg_3016" n="HIAT:ip">(</nts>
                  <nts id="Seg_3017" n="HIAT:ip">(</nts>
                  <ats e="T721" id="Seg_3018" n="HIAT:non-pho" s="T720">BRK</ats>
                  <nts id="Seg_3019" n="HIAT:ip">)</nts>
                  <nts id="Seg_3020" n="HIAT:ip">)</nts>
                  <nts id="Seg_3021" n="HIAT:ip">.</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_3024" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_3026" n="HIAT:w" s="T721">Dĭgəttə</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_3029" n="HIAT:w" s="T722">dĭzeŋ</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_3032" n="HIAT:w" s="T723">dʼijegən</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3035" n="HIAT:w" s="T724">keʔbde</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_3038" n="HIAT:w" s="T725">oʔbdəbiʔi</ts>
                  <nts id="Seg_3039" n="HIAT:ip">,</nts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_3042" n="HIAT:w" s="T726">ambiʔi</ts>
                  <nts id="Seg_3043" n="HIAT:ip">.</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_3046" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_3048" n="HIAT:w" s="T727">San</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_3051" n="HIAT:w" s="T728">oʔbdəbiʔi</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_3054" n="HIAT:w" s="T729">lejbiʔi</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_3058" n="HIAT:u" s="T730">
                  <nts id="Seg_3059" n="HIAT:ip">(</nts>
                  <nts id="Seg_3060" n="HIAT:ip">(</nts>
                  <ats e="T731" id="Seg_3061" n="HIAT:non-pho" s="T730">BRK</ats>
                  <nts id="Seg_3062" n="HIAT:ip">)</nts>
                  <nts id="Seg_3063" n="HIAT:ip">)</nts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_3067" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_3069" n="HIAT:w" s="T731">Dĭgəttə</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3072" n="HIAT:w" s="T732">dĭzeŋ</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_3075" n="HIAT:w" s="T733">beškeʔi</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3078" n="HIAT:w" s="T734">oʔbdəbiʔi</ts>
                  <nts id="Seg_3079" n="HIAT:ip">,</nts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3082" n="HIAT:w" s="T735">mĭnzerbiʔi</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_3085" n="HIAT:w" s="T736">i</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3088" n="HIAT:w" s="T737">tustʼarbiʔi</ts>
                  <nts id="Seg_3089" n="HIAT:ip">.</nts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T739" id="Seg_3092" n="HIAT:u" s="T738">
                  <nts id="Seg_3093" n="HIAT:ip">(</nts>
                  <nts id="Seg_3094" n="HIAT:ip">(</nts>
                  <ats e="T739" id="Seg_3095" n="HIAT:non-pho" s="T738">BRK</ats>
                  <nts id="Seg_3096" n="HIAT:ip">)</nts>
                  <nts id="Seg_3097" n="HIAT:ip">)</nts>
                  <nts id="Seg_3098" n="HIAT:ip">.</nts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_3101" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_3103" n="HIAT:w" s="T739">Dĭgəttə</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3105" n="HIAT:ip">(</nts>
                  <ts e="T741" id="Seg_3107" n="HIAT:w" s="T740">dĭn=</ts>
                  <nts id="Seg_3108" n="HIAT:ip">)</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3111" n="HIAT:w" s="T741">dĭzen</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3114" n="HIAT:w" s="T742">dĭn</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3117" n="HIAT:w" s="T743">kuʔpiʔi</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3120" n="HIAT:w" s="T744">sɨneʔi</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3123" n="HIAT:w" s="T745">i</ts>
                  <nts id="Seg_3124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3126" n="HIAT:w" s="T746">uja</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3129" n="HIAT:w" s="T747">tustʼarbiʔi</ts>
                  <nts id="Seg_3130" n="HIAT:ip">.</nts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_3133" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_3135" n="HIAT:w" s="T748">Dĭgəttə</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3138" n="HIAT:w" s="T749">edəbiʔi</ts>
                  <nts id="Seg_3139" n="HIAT:ip">,</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3142" n="HIAT:w" s="T750">dĭ</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3144" n="HIAT:ip">(</nts>
                  <ts e="T752" id="Seg_3146" n="HIAT:w" s="T751">ko-</ts>
                  <nts id="Seg_3147" n="HIAT:ip">)</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3150" n="HIAT:w" s="T752">kolambi</ts>
                  <nts id="Seg_3151" n="HIAT:ip">,</nts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3154" n="HIAT:w" s="T753">baltuzi</ts>
                  <nts id="Seg_3155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3157" n="HIAT:w" s="T754">jaʔpiʔi</ts>
                  <nts id="Seg_3158" n="HIAT:ip">,</nts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3161" n="HIAT:w" s="T755">buranə</ts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3164" n="HIAT:w" s="T756">kămnəbiʔi</ts>
                  <nts id="Seg_3165" n="HIAT:ip">.</nts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_3168" n="HIAT:u" s="T757">
                  <nts id="Seg_3169" n="HIAT:ip">(</nts>
                  <nts id="Seg_3170" n="HIAT:ip">(</nts>
                  <ats e="T758" id="Seg_3171" n="HIAT:non-pho" s="T757">BRK</ats>
                  <nts id="Seg_3172" n="HIAT:ip">)</nts>
                  <nts id="Seg_3173" n="HIAT:ip">)</nts>
                  <nts id="Seg_3174" n="HIAT:ip">.</nts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_3177" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3179" n="HIAT:w" s="T758">Dĭzeŋ</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3182" n="HIAT:w" s="T759">dĭgəttə</ts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3185" n="HIAT:w" s="T760">kola</ts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3188" n="HIAT:w" s="T761">dʼaʔpiʔi</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3191" n="HIAT:w" s="T762">i</ts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3194" n="HIAT:w" s="T763">tustʼarbiʔi</ts>
                  <nts id="Seg_3195" n="HIAT:ip">,</nts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3198" n="HIAT:w" s="T764">i</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3201" n="HIAT:w" s="T765">tože</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3204" n="HIAT:w" s="T766">kola</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3206" n="HIAT:ip">(</nts>
                  <ts e="T769" id="Seg_3208" n="HIAT:w" s="T767">koʔpiʔi</ts>
                  <nts id="Seg_3209" n="HIAT:ip">)</nts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3212" n="HIAT:w" s="T769">сушили</ts>
                  <nts id="Seg_3213" n="HIAT:ip">.</nts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3216" n="HIAT:u" s="T770">
                  <nts id="Seg_3217" n="HIAT:ip">(</nts>
                  <nts id="Seg_3218" n="HIAT:ip">(</nts>
                  <ats e="T771" id="Seg_3219" n="HIAT:non-pho" s="T770">BRK</ats>
                  <nts id="Seg_3220" n="HIAT:ip">)</nts>
                  <nts id="Seg_3221" n="HIAT:ip">)</nts>
                  <nts id="Seg_3222" n="HIAT:ip">.</nts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3225" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3227" n="HIAT:w" s="T771">Dĭgəttə</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3230" n="HIAT:w" s="T772">kubatsi</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3233" n="HIAT:w" s="T773">abiʔi</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3236" n="HIAT:w" s="T774">buraʔi</ts>
                  <nts id="Seg_3237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3239" n="HIAT:w" s="T775">i</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3242" n="HIAT:w" s="T776">dĭbər</ts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3245" n="HIAT:w" s="T777">embiʔi</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3248" n="HIAT:w" s="T778">uja</ts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3251" n="HIAT:w" s="T779">i</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3254" n="HIAT:w" s="T780">kola</ts>
                  <nts id="Seg_3255" n="HIAT:ip">.</nts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3258" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3260" n="HIAT:w" s="T781">I</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3263" n="HIAT:w" s="T782">ălenʼəʔizi</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3266" n="HIAT:w" s="T783">deʔpiʔi</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3269" n="HIAT:w" s="T784">maʔnə</ts>
                  <nts id="Seg_3270" n="HIAT:ip">.</nts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_3273" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3275" n="HIAT:w" s="T785">Döber</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3278" n="HIAT:w" s="T786">gijen</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3281" n="HIAT:w" s="T787">amnobiʔi</ts>
                  <nts id="Seg_3282" n="HIAT:ip">,</nts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3285" n="HIAT:w" s="T788">kamen</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3288" n="HIAT:w" s="T789">šišəge</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3291" n="HIAT:w" s="T790">ibi</ts>
                  <nts id="Seg_3292" n="HIAT:ip">.</nts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3295" n="HIAT:u" s="T791">
                  <nts id="Seg_3296" n="HIAT:ip">(</nts>
                  <nts id="Seg_3297" n="HIAT:ip">(</nts>
                  <ats e="T792" id="Seg_3298" n="HIAT:non-pho" s="T791">BRK</ats>
                  <nts id="Seg_3299" n="HIAT:ip">)</nts>
                  <nts id="Seg_3300" n="HIAT:ip">)</nts>
                  <nts id="Seg_3301" n="HIAT:ip">.</nts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T798" id="Seg_3304" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3306" n="HIAT:w" s="T792">Dĭzeŋ</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3308" n="HIAT:ip">(</nts>
                  <ts e="T794" id="Seg_3310" n="HIAT:w" s="T793">dĭgət-</ts>
                  <nts id="Seg_3311" n="HIAT:ip">)</nts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3314" n="HIAT:w" s="T794">dĭrgidəʔi</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3317" n="HIAT:w" s="T795">menzeŋdə</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3319" n="HIAT:ip">(</nts>
                  <ts e="T797" id="Seg_3321" n="HIAT:w" s="T796">b-</ts>
                  <nts id="Seg_3322" n="HIAT:ip">)</nts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3325" n="HIAT:w" s="T797">ibiʔi</ts>
                  <nts id="Seg_3326" n="HIAT:ip">.</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_3329" n="HIAT:u" s="T798">
                  <nts id="Seg_3330" n="HIAT:ip">(</nts>
                  <ts e="T799" id="Seg_3332" n="HIAT:w" s="T798">Ka-</ts>
                  <nts id="Seg_3333" n="HIAT:ip">)</nts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3336" n="HIAT:w" s="T799">Kalaʔi</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3339" n="HIAT:w" s="T800">poʔto</ts>
                  <nts id="Seg_3340" n="HIAT:ip">,</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3343" n="HIAT:w" s="T801">iʔgö</ts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3346" n="HIAT:w" s="T802">dʼabəluʔi</ts>
                  <nts id="Seg_3347" n="HIAT:ip">,</nts>
                  <nts id="Seg_3348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3350" n="HIAT:w" s="T803">dĭgəttə</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3353" n="HIAT:w" s="T804">maʔndə</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3356" n="HIAT:w" s="T805">šoləj</ts>
                  <nts id="Seg_3357" n="HIAT:ip">,</nts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3360" n="HIAT:w" s="T806">dĭ</ts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3363" n="HIAT:w" s="T807">ine</ts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3366" n="HIAT:w" s="T808">kolerləj</ts>
                  <nts id="Seg_3367" n="HIAT:ip">.</nts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_3370" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_3372" n="HIAT:w" s="T809">Dĭgəttə</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3375" n="HIAT:w" s="T810">kandəga</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3378" n="HIAT:w" s="T811">poʔto</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3381" n="HIAT:w" s="T812">oʔbdəsʼtə</ts>
                  <nts id="Seg_3382" n="HIAT:ip">.</nts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3385" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_3387" n="HIAT:w" s="T813">Dĭgəttə</ts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3390" n="HIAT:w" s="T814">šoləj</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3393" n="HIAT:w" s="T815">maʔndə</ts>
                  <nts id="Seg_3394" n="HIAT:ip">.</nts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T817" id="Seg_3397" n="HIAT:u" s="T816">
                  <nts id="Seg_3398" n="HIAT:ip">(</nts>
                  <nts id="Seg_3399" n="HIAT:ip">(</nts>
                  <ats e="T817" id="Seg_3400" n="HIAT:non-pho" s="T816">BRK</ats>
                  <nts id="Seg_3401" n="HIAT:ip">)</nts>
                  <nts id="Seg_3402" n="HIAT:ip">)</nts>
                  <nts id="Seg_3403" n="HIAT:ip">.</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_3406" n="HIAT:u" s="T817">
                  <ts e="T818" id="Seg_3408" n="HIAT:w" s="T817">I</ts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3411" n="HIAT:w" s="T818">poʔto</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3414" n="HIAT:w" s="T819">detləj</ts>
                  <nts id="Seg_3415" n="HIAT:ip">.</nts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3418" n="HIAT:u" s="T820">
                  <nts id="Seg_3419" n="HIAT:ip">(</nts>
                  <nts id="Seg_3420" n="HIAT:ip">(</nts>
                  <ats e="T821" id="Seg_3421" n="HIAT:non-pho" s="T820">BRK</ats>
                  <nts id="Seg_3422" n="HIAT:ip">)</nts>
                  <nts id="Seg_3423" n="HIAT:ip">)</nts>
                  <nts id="Seg_3424" n="HIAT:ip">.</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3427" n="HIAT:u" s="T821">
                  <nts id="Seg_3428" n="HIAT:ip">(</nts>
                  <ts e="T822" id="Seg_3430" n="HIAT:w" s="T821">Dĭz-</ts>
                  <nts id="Seg_3431" n="HIAT:ip">)</nts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3434" n="HIAT:w" s="T822">Dĭzem</ts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3436" n="HIAT:ip">(</nts>
                  <ts e="T824" id="Seg_3438" n="HIAT:w" s="T823">a-</ts>
                  <nts id="Seg_3439" n="HIAT:ip">)</nts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3442" n="HIAT:w" s="T824">armijanə</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3445" n="HIAT:w" s="T825">ej</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3448" n="HIAT:w" s="T826">ibiʔi</ts>
                  <nts id="Seg_3449" n="HIAT:ip">,</nts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3452" n="HIAT:w" s="T827">dĭzeŋ</ts>
                  <nts id="Seg_3453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3454" n="HIAT:ip">(</nts>
                  <ts e="T829" id="Seg_3456" n="HIAT:w" s="T828">ĭsak</ts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3459" n="HIAT:w" s="T829">aktʼa=</ts>
                  <nts id="Seg_3460" n="HIAT:ip">)</nts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3463" n="HIAT:w" s="T830">aktʼa</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3466" n="HIAT:w" s="T831">mĭbiʔi</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3469" n="HIAT:w" s="T832">i</ts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3472" n="HIAT:w" s="T833">albuga</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3475" n="HIAT:w" s="T834">mĭbiʔi</ts>
                  <nts id="Seg_3476" n="HIAT:ip">.</nts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3479" n="HIAT:u" s="T835">
                  <nts id="Seg_3480" n="HIAT:ip">(</nts>
                  <nts id="Seg_3481" n="HIAT:ip">(</nts>
                  <ats e="T836" id="Seg_3482" n="HIAT:non-pho" s="T835">BRK</ats>
                  <nts id="Seg_3483" n="HIAT:ip">)</nts>
                  <nts id="Seg_3484" n="HIAT:ip">)</nts>
                  <nts id="Seg_3485" n="HIAT:ip">.</nts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3488" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3490" n="HIAT:w" s="T836">Dĭzeŋ</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3493" n="HIAT:w" s="T837">ĭsaktə</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3496" n="HIAT:w" s="T838">mĭmbiʔi</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3499" n="HIAT:w" s="T839">aktʼa</ts>
                  <nts id="Seg_3500" n="HIAT:ip">,</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3502" n="HIAT:ip">(</nts>
                  <ts e="T841" id="Seg_3504" n="HIAT:w" s="T840">bʼeʔ=</ts>
                  <nts id="Seg_3505" n="HIAT:ip">)</nts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3508" n="HIAT:w" s="T841">bʼeʔ</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3511" n="HIAT:w" s="T842">sumna</ts>
                  <nts id="Seg_3512" n="HIAT:ip">.</nts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T846" id="Seg_3515" n="HIAT:u" s="T843">
                  <ts e="T843.tx.1" id="Seg_3517" n="HIAT:w" s="T843">Ну</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843.tx.2" id="Seg_3520" n="HIAT:w" s="T843.tx.1">вот</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3523" n="HIAT:w" s="T843.tx.2">так</ts>
                  <nts id="Seg_3524" n="HIAT:ip">.</nts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T846" id="Seg_3526" n="sc" s="T0">
               <ts e="T1" id="Seg_3528" n="e" s="T0">Kamen </ts>
               <ts e="T2" id="Seg_3530" n="e" s="T1">miʔnʼibeʔ </ts>
               <ts e="T3" id="Seg_3532" n="e" s="T2">šobial? </ts>
               <ts e="T4" id="Seg_3534" n="e" s="T3">Urgo </ts>
               <ts e="T5" id="Seg_3536" n="e" s="T4">dʼalagən. </ts>
               <ts e="T6" id="Seg_3538" n="e" s="T5">Šide </ts>
               <ts e="T7" id="Seg_3540" n="e" s="T6">dʼala </ts>
               <ts e="T850" id="Seg_3542" n="e" s="T7">Kazan </ts>
               <ts e="T8" id="Seg_3544" n="e" s="T850">turagən </ts>
               <ts e="T9" id="Seg_3546" n="e" s="T8">amnobiam. </ts>
               <ts e="T10" id="Seg_3548" n="e" s="T9">(Ĭmbi= </ts>
               <ts e="T11" id="Seg_3550" n="e" s="T10">ĭmbi=) </ts>
               <ts e="T12" id="Seg_3552" n="e" s="T11">Ĭmbi </ts>
               <ts e="T13" id="Seg_3554" n="e" s="T12">kubial? </ts>
               <ts e="T14" id="Seg_3556" n="e" s="T13">Dĭgəttə </ts>
               <ts e="T15" id="Seg_3558" n="e" s="T14">döber </ts>
               <ts e="T16" id="Seg_3560" n="e" s="T15">šobiam. </ts>
               <ts e="T17" id="Seg_3562" n="e" s="T16">((BRK)). </ts>
               <ts e="T18" id="Seg_3564" n="e" s="T17">Măndərbiam, </ts>
               <ts e="T19" id="Seg_3566" n="e" s="T18">măndərbiam, </ts>
               <ts e="T20" id="Seg_3568" n="e" s="T19">kădedə </ts>
               <ts e="T21" id="Seg_3570" n="e" s="T20">(ej </ts>
               <ts e="T22" id="Seg_3572" n="e" s="T21">moliam) </ts>
               <ts e="T23" id="Seg_3574" n="e" s="T22">ej </ts>
               <ts e="T24" id="Seg_3576" n="e" s="T23">moliam </ts>
               <ts e="T25" id="Seg_3578" n="e" s="T24">šozittə </ts>
               <ts e="T26" id="Seg_3580" n="e" s="T25">döber. </ts>
               <ts e="T27" id="Seg_3582" n="e" s="T26">Nagur </ts>
               <ts e="T28" id="Seg_3584" n="e" s="T27">dʼalagən </ts>
               <ts e="T29" id="Seg_3586" n="e" s="T28">dĭgəttə </ts>
               <ts e="T30" id="Seg_3588" n="e" s="T29">šobiam </ts>
               <ts e="T31" id="Seg_3590" n="e" s="T30">döber. </ts>
               <ts e="T32" id="Seg_3592" n="e" s="T31">((BRK)). </ts>
               <ts e="T33" id="Seg_3594" n="e" s="T32">Dĭgəttə </ts>
               <ts e="T34" id="Seg_3596" n="e" s="T33">döber </ts>
               <ts e="T35" id="Seg_3598" n="e" s="T34">šobiam, </ts>
               <ts e="T36" id="Seg_3600" n="e" s="T35">urgajanə </ts>
               <ts e="T37" id="Seg_3602" n="e" s="T36">šobiam. </ts>
               <ts e="T38" id="Seg_3604" n="e" s="T37">Dĭ </ts>
               <ts e="T39" id="Seg_3606" n="e" s="T38">măna </ts>
               <ts e="T40" id="Seg_3608" n="e" s="T39">bar </ts>
               <ts e="T41" id="Seg_3610" n="e" s="T40">bădəbi, </ts>
               <ts e="T42" id="Seg_3612" n="e" s="T41">mĭj </ts>
               <ts e="T43" id="Seg_3614" n="e" s="T42">mĭbi. </ts>
               <ts e="T44" id="Seg_3616" n="e" s="T43">((BRK)). </ts>
               <ts e="T45" id="Seg_3618" n="e" s="T44">Dĭgəttə </ts>
               <ts e="T46" id="Seg_3620" n="e" s="T45">döber </ts>
               <ts e="T47" id="Seg_3622" n="e" s="T46">šobiam, </ts>
               <ts e="T48" id="Seg_3624" n="e" s="T47">buzo </ts>
               <ts e="T49" id="Seg_3626" n="e" s="T48">kirgarlaʔbə. </ts>
               <ts e="T50" id="Seg_3628" n="e" s="T49">Nüke </ts>
               <ts e="T51" id="Seg_3630" n="e" s="T50">bar </ts>
               <ts e="T52" id="Seg_3632" n="e" s="T51">mĭbi </ts>
               <ts e="T53" id="Seg_3634" n="e" s="T52">dĭʔnə </ts>
               <ts e="T54" id="Seg_3636" n="e" s="T53">toltanoʔ, </ts>
               <ts e="T55" id="Seg_3638" n="e" s="T54">dĭ </ts>
               <ts e="T56" id="Seg_3640" n="e" s="T55">amorlaʔbə, </ts>
               <ts e="T57" id="Seg_3642" n="e" s="T56">ej </ts>
               <ts e="T58" id="Seg_3644" n="e" s="T57">kirgarlia. </ts>
               <ts e="T59" id="Seg_3646" n="e" s="T58">((BRK)). </ts>
               <ts e="T60" id="Seg_3648" n="e" s="T59">Dĭ </ts>
               <ts e="T61" id="Seg_3650" n="e" s="T60">nükegən </ts>
               <ts e="T62" id="Seg_3652" n="e" s="T61">turagən </ts>
               <ts e="T63" id="Seg_3654" n="e" s="T62">šišəge, </ts>
               <ts e="T64" id="Seg_3656" n="e" s="T63">pʼeš </ts>
               <ts e="T65" id="Seg_3658" n="e" s="T64">nendəbi. </ts>
               <ts e="T66" id="Seg_3660" n="e" s="T65">((BRK)). </ts>
               <ts e="T67" id="Seg_3662" n="e" s="T66">Nʼiʔnen </ts>
               <ts e="T68" id="Seg_3664" n="e" s="T67">bar </ts>
               <ts e="T69" id="Seg_3666" n="e" s="T68">šišəge, </ts>
               <ts e="T70" id="Seg_3668" n="e" s="T69">ugandə. </ts>
               <ts e="T71" id="Seg_3670" n="e" s="T70">Büzo </ts>
               <ts e="T72" id="Seg_3672" n="e" s="T71">turagən </ts>
               <ts e="T73" id="Seg_3674" n="e" s="T72">nuga </ts>
               <ts e="T74" id="Seg_3676" n="e" s="T73">bar. </ts>
               <ts e="T75" id="Seg_3678" n="e" s="T74">((BRK)). </ts>
               <ts e="T76" id="Seg_3680" n="e" s="T75">(Tüžöj) </ts>
               <ts e="T77" id="Seg_3682" n="e" s="T76">Tüžöj </ts>
               <ts e="T78" id="Seg_3684" n="e" s="T77">nʼeluʔpi </ts>
               <ts e="T79" id="Seg_3686" n="e" s="T78">bar, </ts>
               <ts e="T80" id="Seg_3688" n="e" s="T79">üdʼüge </ts>
               <ts e="T81" id="Seg_3690" n="e" s="T80">büzo. </ts>
               <ts e="T82" id="Seg_3692" n="e" s="T81">((BRK)). </ts>
               <ts e="T83" id="Seg_3694" n="e" s="T82">Miʔ </ts>
               <ts e="T84" id="Seg_3696" n="e" s="T83">turagən </ts>
               <ts e="T85" id="Seg_3698" n="e" s="T84">ugandə </ts>
               <ts e="T86" id="Seg_3700" n="e" s="T85">iʔgö </ts>
               <ts e="T87" id="Seg_3702" n="e" s="T86">il </ts>
               <ts e="T88" id="Seg_3704" n="e" s="T87">kübiʔi. </ts>
               <ts e="T89" id="Seg_3706" n="e" s="T88">((BRK)). </ts>
               <ts e="T90" id="Seg_3708" n="e" s="T89">Mĭmbiem </ts>
               <ts e="T91" id="Seg_3710" n="e" s="T90">nükenə, </ts>
               <ts e="T92" id="Seg_3712" n="e" s="T91">dĭ </ts>
               <ts e="T93" id="Seg_3714" n="e" s="T92">(uga-) </ts>
               <ts e="T94" id="Seg_3716" n="e" s="T93">ugandə </ts>
               <ts e="T95" id="Seg_3718" n="e" s="T94">ĭzemnie. </ts>
               <ts e="T96" id="Seg_3720" n="e" s="T95">Măn </ts>
               <ts e="T97" id="Seg_3722" n="e" s="T96">teinen </ts>
               <ts e="T98" id="Seg_3724" n="e" s="T97">(kulambi-) </ts>
               <ts e="T99" id="Seg_3726" n="e" s="T98">külalləm. </ts>
               <ts e="T100" id="Seg_3728" n="e" s="T99">No, </ts>
               <ts e="T101" id="Seg_3730" n="e" s="T100">küʔ. </ts>
               <ts e="T102" id="Seg_3732" n="e" s="T101">No, </ts>
               <ts e="T103" id="Seg_3734" n="e" s="T102">küʔ. </ts>
               <ts e="T104" id="Seg_3736" n="e" s="T103">(Taʔ-) </ts>
               <ts e="T105" id="Seg_3738" n="e" s="T104">Karəldʼan </ts>
               <ts e="T106" id="Seg_3740" n="e" s="T105">(šo-) </ts>
               <ts e="T107" id="Seg_3742" n="e" s="T106">šolam </ts>
               <ts e="T108" id="Seg_3744" n="e" s="T107">(băzəsʼtə) </ts>
               <ts e="T109" id="Seg_3746" n="e" s="T108">tănan. </ts>
               <ts e="T110" id="Seg_3748" n="e" s="T109">Dĭgəttə </ts>
               <ts e="T111" id="Seg_3750" n="e" s="T110">ej </ts>
               <ts e="T112" id="Seg_3752" n="e" s="T111">kambiam. </ts>
               <ts e="T113" id="Seg_3754" n="e" s="T112">Noʔ </ts>
               <ts e="T114" id="Seg_3756" n="e" s="T113">deʔpiʔi </ts>
               <ts e="T115" id="Seg_3758" n="e" s="T114">šide </ts>
               <ts e="T116" id="Seg_3760" n="e" s="T115">tibi. </ts>
               <ts e="T117" id="Seg_3762" n="e" s="T116">Dĭgəttə </ts>
               <ts e="T118" id="Seg_3764" n="e" s="T117">šobiam: </ts>
               <ts e="T119" id="Seg_3766" n="e" s="T118">Ĭmbi </ts>
               <ts e="T120" id="Seg_3768" n="e" s="T119">konʼdʼo </ts>
               <ts e="T121" id="Seg_3770" n="e" s="T120">ej </ts>
               <ts e="T122" id="Seg_3772" n="e" s="T121">šobial? </ts>
               <ts e="T123" id="Seg_3774" n="e" s="T122">A </ts>
               <ts e="T124" id="Seg_3776" n="e" s="T123">măn </ts>
               <ts e="T125" id="Seg_3778" n="e" s="T124">tenəbiem </ts>
               <ts e="T126" id="Seg_3780" n="e" s="T125">tăn </ts>
               <ts e="T127" id="Seg_3782" n="e" s="T126">(külambiam), </ts>
               <ts e="T128" id="Seg_3784" n="e" s="T127">băzəsʼtə </ts>
               <ts e="T129" id="Seg_3786" n="e" s="T128">šobiam". </ts>
               <ts e="T130" id="Seg_3788" n="e" s="T129">((BRK)). </ts>
               <ts e="T131" id="Seg_3790" n="e" s="T130">Büzo </ts>
               <ts e="T132" id="Seg_3792" n="e" s="T131">nomlaʔbə, </ts>
               <ts e="T133" id="Seg_3794" n="e" s="T132">ej </ts>
               <ts e="T134" id="Seg_3796" n="e" s="T133">kirgarlaʔbə. </ts>
               <ts e="T135" id="Seg_3798" n="e" s="T134">((BRK)). </ts>
               <ts e="T136" id="Seg_3800" n="e" s="T135">Esseŋ </ts>
               <ts e="T137" id="Seg_3802" n="e" s="T136">bar </ts>
               <ts e="T138" id="Seg_3804" n="e" s="T137">nuʔməleʔbəʔjə </ts>
               <ts e="T139" id="Seg_3806" n="e" s="T138">(turan </ts>
               <ts e="T140" id="Seg_3808" n="e" s="T139">to-) </ts>
               <ts e="T141" id="Seg_3810" n="e" s="T140">turan </ts>
               <ts e="T142" id="Seg_3812" n="e" s="T141">tondə. </ts>
               <ts e="T143" id="Seg_3814" n="e" s="T142">((BRK)). </ts>
               <ts e="T144" id="Seg_3816" n="e" s="T143">Šide </ts>
               <ts e="T145" id="Seg_3818" n="e" s="T144">nʼi </ts>
               <ts e="T146" id="Seg_3820" n="e" s="T145">šobiʔi </ts>
               <ts e="T147" id="Seg_3822" n="e" s="T146">nʼilgösʼtə. </ts>
               <ts e="T148" id="Seg_3824" n="e" s="T147">((BRK)). </ts>
               <ts e="T149" id="Seg_3826" n="e" s="T148">((BRK)). </ts>
               <ts e="T150" id="Seg_3828" n="e" s="T149">Iʔ </ts>
               <ts e="T151" id="Seg_3830" n="e" s="T150">(kirgargaʔ), </ts>
               <ts e="T152" id="Seg_3832" n="e" s="T151">iʔ </ts>
               <ts e="T153" id="Seg_3834" n="e" s="T152">alomgaʔ, </ts>
               <ts e="T154" id="Seg_3836" n="e" s="T153">nʼilgöleʔ. </ts>
               <ts e="T155" id="Seg_3838" n="e" s="T154">((BRK)). </ts>
               <ts e="T156" id="Seg_3840" n="e" s="T155">"Ial </ts>
               <ts e="T157" id="Seg_3842" n="e" s="T156">kamen </ts>
               <ts e="T158" id="Seg_3844" n="e" s="T157">šoləj?" </ts>
               <ts e="T159" id="Seg_3846" n="e" s="T158">"Büžü </ts>
               <ts e="T160" id="Seg_3848" n="e" s="T159">šoləj!" </ts>
               <ts e="T161" id="Seg_3850" n="e" s="T160">((BRK)). </ts>
               <ts e="T162" id="Seg_3852" n="e" s="T161">Nada </ts>
               <ts e="T163" id="Seg_3854" n="e" s="T162">bü </ts>
               <ts e="T164" id="Seg_3856" n="e" s="T163">ejümzittə, </ts>
               <ts e="T165" id="Seg_3858" n="e" s="T164">dĭgəttə </ts>
               <ts e="T166" id="Seg_3860" n="e" s="T165">tüžöj </ts>
               <ts e="T167" id="Seg_3862" n="e" s="T166">bĭdəlzittə. </ts>
               <ts e="T168" id="Seg_3864" n="e" s="T167">((BRK)). </ts>
               <ts e="T169" id="Seg_3866" n="e" s="T168">Ĭmbi </ts>
               <ts e="T170" id="Seg_3868" n="e" s="T169">nʼilgösʼtə </ts>
               <ts e="T171" id="Seg_3870" n="e" s="T170">šobilaʔ, </ts>
               <ts e="T172" id="Seg_3872" n="e" s="T171">măndərzittə </ts>
               <ts e="T173" id="Seg_3874" n="e" s="T172">šobilaʔ? </ts>
               <ts e="T174" id="Seg_3876" n="e" s="T173">((BRK)). </ts>
               <ts e="T175" id="Seg_3878" n="e" s="T174">Ej </ts>
               <ts e="T176" id="Seg_3880" n="e" s="T175">kürümnieʔi. </ts>
               <ts e="T177" id="Seg_3882" n="e" s="T176">Kuvas </ts>
               <ts e="T178" id="Seg_3884" n="e" s="T177">nʼizeŋ, </ts>
               <ts e="T179" id="Seg_3886" n="e" s="T178">ej </ts>
               <ts e="T180" id="Seg_3888" n="e" s="T179">kürümnieʔi. </ts>
               <ts e="T181" id="Seg_3890" n="e" s="T180">Nulaʔbəʔjə, </ts>
               <ts e="T182" id="Seg_3892" n="e" s="T181">jakšeʔi </ts>
               <ts e="T183" id="Seg_3894" n="e" s="T182">ugandə. </ts>
               <ts e="T184" id="Seg_3896" n="e" s="T183">((BRK)). </ts>
               <ts e="T185" id="Seg_3898" n="e" s="T184">Sanə </ts>
               <ts e="T186" id="Seg_3900" n="e" s="T185">teinen </ts>
               <ts e="T187" id="Seg_3902" n="e" s="T186">mĭbiem </ts>
               <ts e="T188" id="Seg_3904" n="e" s="T187">tănan </ts>
               <ts e="T189" id="Seg_3906" n="e" s="T188">lejzittə. </ts>
               <ts e="T190" id="Seg_3908" n="e" s="T189">((BRK)). </ts>
               <ts e="T191" id="Seg_3910" n="e" s="T190">Sʼimat </ts>
               <ts e="T192" id="Seg_3912" n="e" s="T191">măndərzittə, </ts>
               <ts e="T193" id="Seg_3914" n="e" s="T192">(kuʔi-) </ts>
               <ts e="T194" id="Seg_3916" n="e" s="T193">kuzaŋdə </ts>
               <ts e="T195" id="Seg_3918" n="e" s="T194">nʼilgösʼtə, </ts>
               <ts e="T196" id="Seg_3920" n="e" s="T195">aŋdə </ts>
               <ts e="T197" id="Seg_3922" n="e" s="T196">amzittə, </ts>
               <ts e="T198" id="Seg_3924" n="e" s="T197">tĭmeʔi </ts>
               <ts e="T199" id="Seg_3926" n="e" s="T198">talbəzittə </ts>
               <ts e="T200" id="Seg_3928" n="e" s="T199">ipek. </ts>
               <ts e="T201" id="Seg_3930" n="e" s="T200">((BRK)). </ts>
               <ts e="T202" id="Seg_3932" n="e" s="T201">Tenöbiam, </ts>
               <ts e="T203" id="Seg_3934" n="e" s="T202">tenöbiam, </ts>
               <ts e="T204" id="Seg_3936" n="e" s="T203">kăde </ts>
               <ts e="T205" id="Seg_3938" n="e" s="T204">döber </ts>
               <ts e="T206" id="Seg_3940" n="e" s="T205">šozittə. </ts>
               <ts e="T207" id="Seg_3942" n="e" s="T206">((BRK)). </ts>
               <ts e="T208" id="Seg_3944" n="e" s="T207">Kuba </ts>
               <ts e="T209" id="Seg_3946" n="e" s="T208">dʼügən </ts>
               <ts e="T210" id="Seg_3948" n="e" s="T209">iʔbolaʔpi. </ts>
               <ts e="T211" id="Seg_3950" n="e" s="T210">Ipek </ts>
               <ts e="T212" id="Seg_3952" n="e" s="T211">embiʔi. </ts>
               <ts e="T213" id="Seg_3954" n="e" s="T212">Dĭgəttə </ts>
               <ts e="T214" id="Seg_3956" n="e" s="T213">amorbiʔi </ts>
               <ts e="T215" id="Seg_3958" n="e" s="T214">dĭ </ts>
               <ts e="T216" id="Seg_3960" n="e" s="T215">ipek, </ts>
               <ts e="T217" id="Seg_3962" n="e" s="T216">i </ts>
               <ts e="T218" id="Seg_3964" n="e" s="T217">uja. </ts>
               <ts e="T220" id="Seg_3966" n="e" s="T218">И мясо. </ts>
               <ts e="T221" id="Seg_3968" n="e" s="T220">((BRK)). </ts>
               <ts e="T222" id="Seg_3970" n="e" s="T221">Dĭgəttə </ts>
               <ts e="T223" id="Seg_3972" n="e" s="T222">ambiʔi, </ts>
               <ts e="T224" id="Seg_3974" n="e" s="T223">dĭgəttə </ts>
               <ts e="T225" id="Seg_3976" n="e" s="T224">kudajdə </ts>
               <ts e="T226" id="Seg_3978" n="e" s="T225">numan üzəbiʔi. </ts>
               <ts e="T227" id="Seg_3980" n="e" s="T226">((BRK)). </ts>
               <ts e="T851" id="Seg_3982" n="e" s="T227">Kazan </ts>
               <ts e="T228" id="Seg_3984" n="e" s="T851">turanə </ts>
               <ts e="T229" id="Seg_3986" n="e" s="T228">mĭmbiem, </ts>
               <ts e="T230" id="Seg_3988" n="e" s="T229">(tʼegergən=) </ts>
               <ts e="T231" id="Seg_3990" n="e" s="T230">tʼegergən </ts>
               <ts e="T232" id="Seg_3992" n="e" s="T231">(ku-) </ts>
               <ts e="T233" id="Seg_3994" n="e" s="T232">kudaj </ts>
               <ts e="T234" id="Seg_3996" n="e" s="T233">(numan=) </ts>
               <ts e="T235" id="Seg_3998" n="e" s="T234">numan üzəbiem. </ts>
               <ts e="T236" id="Seg_4000" n="e" s="T235">((BRK)). </ts>
               <ts e="T237" id="Seg_4002" n="e" s="T236">Iʔgö </ts>
               <ts e="T238" id="Seg_4004" n="e" s="T237">il </ts>
               <ts e="T239" id="Seg_4006" n="e" s="T238">külambiʔi, </ts>
               <ts e="T240" id="Seg_4008" n="e" s="T239">(kros-) </ts>
               <ts e="T241" id="Seg_4010" n="e" s="T240">krospaʔi </ts>
               <ts e="T242" id="Seg_4012" n="e" s="T241">ugandə </ts>
               <ts e="T243" id="Seg_4014" n="e" s="T242">iʔgö </ts>
               <ts e="T244" id="Seg_4016" n="e" s="T243">nugaʔi </ts>
               <ts e="T245" id="Seg_4018" n="e" s="T244">bar. </ts>
               <ts e="T246" id="Seg_4020" n="e" s="T245">((BRK)). </ts>
               <ts e="T247" id="Seg_4022" n="e" s="T246">Miʔ </ts>
               <ts e="T248" id="Seg_4024" n="e" s="T247">koʔbdo </ts>
               <ts e="T249" id="Seg_4026" n="e" s="T248">ĭzembi, </ts>
               <ts e="T250" id="Seg_4028" n="e" s="T249">a </ts>
               <ts e="T251" id="Seg_4030" n="e" s="T250">abat </ts>
               <ts e="T252" id="Seg_4032" n="e" s="T251">(k-) </ts>
               <ts e="T253" id="Seg_4034" n="e" s="T252">kumbi </ts>
               <ts e="T254" id="Seg_4036" n="e" s="T253">(dʼizi-) </ts>
               <ts e="T255" id="Seg_4038" n="e" s="T254">dʼazirzittə. </ts>
               <ts e="T256" id="Seg_4040" n="e" s="T255">((BRK)). </ts>
               <ts e="T257" id="Seg_4042" n="e" s="T256">(Miʔ-) </ts>
               <ts e="T258" id="Seg_4044" n="e" s="T257">Miʔnʼibeʔ </ts>
               <ts e="T259" id="Seg_4046" n="e" s="T258">teinen </ts>
               <ts e="T260" id="Seg_4048" n="e" s="T259">nüdʼin </ts>
               <ts e="T261" id="Seg_4050" n="e" s="T260">tibi </ts>
               <ts e="T262" id="Seg_4052" n="e" s="T261">nezi </ts>
               <ts e="T263" id="Seg_4054" n="e" s="T262">bar </ts>
               <ts e="T264" id="Seg_4056" n="e" s="T263">dʼabrobiʔi. </ts>
               <ts e="T265" id="Seg_4058" n="e" s="T264">Udagən </ts>
               <ts e="T266" id="Seg_4060" n="e" s="T265">bar </ts>
               <ts e="T267" id="Seg_4062" n="e" s="T266">(krom- </ts>
               <ts e="T268" id="Seg_4064" n="e" s="T267">mʼa-) </ts>
               <ts e="T269" id="Seg_4066" n="e" s="T268">kem </ts>
               <ts e="T270" id="Seg_4068" n="e" s="T269">mʼaŋnaʔbə. </ts>
               <ts e="T271" id="Seg_4070" n="e" s="T270">Esseŋdə </ts>
               <ts e="T272" id="Seg_4072" n="e" s="T271">bar </ts>
               <ts e="T273" id="Seg_4074" n="e" s="T272">kirgarlaʔbəʔjə. </ts>
               <ts e="T274" id="Seg_4076" n="e" s="T273">Măn </ts>
               <ts e="T275" id="Seg_4078" n="e" s="T274">šobiam, </ts>
               <ts e="T276" id="Seg_4080" n="e" s="T275">bar </ts>
               <ts e="T277" id="Seg_4082" n="e" s="T276">kudonzəbiam. </ts>
               <ts e="T278" id="Seg_4084" n="e" s="T277">((BRK)). </ts>
               <ts e="T279" id="Seg_4086" n="e" s="T278">Büzo </ts>
               <ts e="T280" id="Seg_4088" n="e" s="T279">iʔbəbi, </ts>
               <ts e="T281" id="Seg_4090" n="e" s="T280">ej </ts>
               <ts e="T282" id="Seg_4092" n="e" s="T281">kirgaria </ts>
               <ts e="T283" id="Seg_4094" n="e" s="T282">tüj. </ts>
               <ts e="T284" id="Seg_4096" n="e" s="T283">((BRK)). </ts>
               <ts e="T285" id="Seg_4098" n="e" s="T284">Možna </ts>
               <ts e="T286" id="Seg_4100" n="e" s="T285">dʼăbaktərzittə. </ts>
               <ts e="T287" id="Seg_4102" n="e" s="T286">((BRK)). </ts>
               <ts e="T288" id="Seg_4104" n="e" s="T287">Büzʼen </ts>
               <ts e="T289" id="Seg_4106" n="e" s="T288">bar </ts>
               <ts e="T290" id="Seg_4108" n="e" s="T289">püjet </ts>
               <ts e="T291" id="Seg_4110" n="e" s="T290">tʼukumbi. </ts>
               <ts e="T292" id="Seg_4112" n="e" s="T291">Gijendə </ts>
               <ts e="T293" id="Seg_4114" n="e" s="T292">ara </ts>
               <ts e="T294" id="Seg_4116" n="e" s="T293">bĭtlem. </ts>
               <ts e="T295" id="Seg_4118" n="e" s="T294">Dĭgəttə </ts>
               <ts e="T296" id="Seg_4120" n="e" s="T295">pograšsʼe </ts>
               <ts e="T297" id="Seg_4122" n="e" s="T296">abi, </ts>
               <ts e="T298" id="Seg_4124" n="e" s="T297">amnobi </ts>
               <ts e="T299" id="Seg_4126" n="e" s="T298">pagən. </ts>
               <ts e="T300" id="Seg_4128" n="e" s="T299">A </ts>
               <ts e="T301" id="Seg_4130" n="e" s="T300">măn </ts>
               <ts e="T302" id="Seg_4132" n="e" s="T301">urgajam </ts>
               <ts e="T303" id="Seg_4134" n="e" s="T302">mĭmbi </ts>
               <ts e="T852" id="Seg_4136" n="e" s="T303">Kazan </ts>
               <ts e="T304" id="Seg_4138" n="e" s="T852">turanə </ts>
               <ts e="T305" id="Seg_4140" n="e" s="T304">i </ts>
               <ts e="T306" id="Seg_4142" n="e" s="T305">ara </ts>
               <ts e="T307" id="Seg_4144" n="e" s="T306">deʔpi. </ts>
               <ts e="T308" id="Seg_4146" n="e" s="T307">(Dĭgəttə) </ts>
               <ts e="T309" id="Seg_4148" n="e" s="T308">amnəbi </ts>
               <ts e="T310" id="Seg_4150" n="e" s="T309">tüʔsʼittə </ts>
               <ts e="T311" id="Seg_4152" n="e" s="T310">(dĭʔ-) </ts>
               <ts e="T312" id="Seg_4154" n="e" s="T311">diʔnə </ts>
               <ts e="T313" id="Seg_4156" n="e" s="T312">kötenzi. </ts>
               <ts e="T314" id="Seg_4158" n="e" s="T313">Dĭgəttə </ts>
               <ts e="T315" id="Seg_4160" n="e" s="T314">dĭ </ts>
               <ts e="T316" id="Seg_4162" n="e" s="T315">šobi. </ts>
               <ts e="T317" id="Seg_4164" n="e" s="T316">Dĭ </ts>
               <ts e="T318" id="Seg_4166" n="e" s="T317">dĭʔnə </ts>
               <ts e="T319" id="Seg_4168" n="e" s="T318">ara </ts>
               <ts e="T320" id="Seg_4170" n="e" s="T319">mĭbi, </ts>
               <ts e="T321" id="Seg_4172" n="e" s="T320">dĭ </ts>
               <ts e="T322" id="Seg_4174" n="e" s="T321">bĭʔpi. </ts>
               <ts e="T323" id="Seg_4176" n="e" s="T322">I </ts>
               <ts e="T324" id="Seg_4178" n="e" s="T323">(măndə=) </ts>
               <ts e="T325" id="Seg_4180" n="e" s="T324">măndə </ts>
               <ts e="T326" id="Seg_4182" n="e" s="T325">dĭʔnə: </ts>
               <ts e="T327" id="Seg_4184" n="e" s="T326">Ne </ts>
               <ts e="T328" id="Seg_4186" n="e" s="T327">dak </ts>
               <ts e="T329" id="Seg_4188" n="e" s="T328">ne, </ts>
               <ts e="T330" id="Seg_4190" n="e" s="T329">ot </ts>
               <ts e="T331" id="Seg_4192" n="e" s="T330">dak </ts>
               <ts e="T332" id="Seg_4194" n="e" s="T331">ne </ts>
               <ts e="T333" id="Seg_4196" n="e" s="T332">dak </ts>
               <ts e="T334" id="Seg_4198" n="e" s="T333">ne. </ts>
               <ts e="T335" id="Seg_4200" n="e" s="T334">Ĭmbi </ts>
               <ts e="T336" id="Seg_4202" n="e" s="T335">tăn </ts>
               <ts e="T337" id="Seg_4204" n="e" s="T336">dăre </ts>
               <ts e="T338" id="Seg_4206" n="e" s="T337">(m-) </ts>
               <ts e="T339" id="Seg_4208" n="e" s="T338">((BRK))? </ts>
               <ts e="T340" id="Seg_4210" n="e" s="T339">A </ts>
               <ts e="T341" id="Seg_4212" n="e" s="T340">tăn </ts>
               <ts e="T342" id="Seg_4214" n="e" s="T341">măna </ts>
               <ts e="T343" id="Seg_4216" n="e" s="T342">(am-) </ts>
               <ts e="T344" id="Seg_4218" n="e" s="T343">kötenzi </ts>
               <ts e="T345" id="Seg_4220" n="e" s="T344">amnobial </ts>
               <ts e="T346" id="Seg_4222" n="e" s="T345">i </ts>
               <ts e="T347" id="Seg_4224" n="e" s="T346">tüʔpiel. </ts>
               <ts e="T348" id="Seg_4226" n="e" s="T347">((BRK)). </ts>
               <ts e="T349" id="Seg_4228" n="e" s="T348">(Măr-) </ts>
               <ts e="T350" id="Seg_4230" n="e" s="T349">Măn </ts>
               <ts e="T351" id="Seg_4232" n="e" s="T350">urgajam </ts>
               <ts e="T352" id="Seg_4234" n="e" s="T351">šamandə </ts>
               <ts e="T353" id="Seg_4236" n="e" s="T352">mĭmbiem. </ts>
               <ts e="T354" id="Seg_4238" n="e" s="T353">Dĭn </ts>
               <ts e="T355" id="Seg_4240" n="e" s="T354">bar </ts>
               <ts e="T356" id="Seg_4242" n="e" s="T355">(moi-) </ts>
               <ts e="T357" id="Seg_4244" n="e" s="T356">uja </ts>
               <ts e="T358" id="Seg_4246" n="e" s="T357">mĭnzerbiʔi, </ts>
               <ts e="T359" id="Seg_4248" n="e" s="T358">takšenə, </ts>
               <ts e="T360" id="Seg_4250" n="e" s="T359">urgo </ts>
               <ts e="T361" id="Seg_4252" n="e" s="T360">takše </ts>
               <ts e="T362" id="Seg_4254" n="e" s="T361">embiʔi. </ts>
               <ts e="T363" id="Seg_4256" n="e" s="T362">Dĭ </ts>
               <ts e="T364" id="Seg_4258" n="e" s="T363">bar </ts>
               <ts e="T365" id="Seg_4260" n="e" s="T364">šĭšəge </ts>
               <ts e="T366" id="Seg_4262" n="e" s="T365">molambi. </ts>
               <ts e="T367" id="Seg_4264" n="e" s="T366">Amnaʔ </ts>
               <ts e="T368" id="Seg_4266" n="e" s="T367">(a-) </ts>
               <ts e="T369" id="Seg_4268" n="e" s="T368">amorzittə! </ts>
               <ts e="T370" id="Seg_4270" n="e" s="T369">Dĭ </ts>
               <ts e="T371" id="Seg_4272" n="e" s="T370">bar </ts>
               <ts e="T372" id="Seg_4274" n="e" s="T371">uja </ts>
               <ts e="T373" id="Seg_4276" n="e" s="T372">ibi, </ts>
               <ts e="T374" id="Seg_4278" n="e" s="T373">dĭ </ts>
               <ts e="T375" id="Seg_4280" n="e" s="T374">ugandə </ts>
               <ts e="T376" id="Seg_4282" n="e" s="T375">šišəge. </ts>
               <ts e="T377" id="Seg_4284" n="e" s="T376">Dĭgəttə </ts>
               <ts e="T378" id="Seg_4286" n="e" s="T377">mendə </ts>
               <ts e="T379" id="Seg_4288" n="e" s="T378">(baʔ-) </ts>
               <ts e="T380" id="Seg_4290" n="e" s="T379">baʔluʔpi. </ts>
               <ts e="T381" id="Seg_4292" n="e" s="T380">((BRK)). </ts>
               <ts e="T382" id="Seg_4294" n="e" s="T381">Bubenzi </ts>
               <ts e="T383" id="Seg_4296" n="e" s="T382">bar </ts>
               <ts e="T384" id="Seg_4298" n="e" s="T383">(š-) </ts>
               <ts e="T385" id="Seg_4300" n="e" s="T384">sʼarbi. </ts>
               <ts e="T386" id="Seg_4302" n="e" s="T385">Bar </ts>
               <ts e="T387" id="Seg_4304" n="e" s="T386">ildə </ts>
               <ts e="T388" id="Seg_4306" n="e" s="T387">(mĭmbi), </ts>
               <ts e="T389" id="Seg_4308" n="e" s="T388">a </ts>
               <ts e="T390" id="Seg_4310" n="e" s="T389">măn </ts>
               <ts e="T391" id="Seg_4312" n="e" s="T390">urgajanə </ts>
               <ts e="T392" id="Seg_4314" n="e" s="T391">ej </ts>
               <ts e="T393" id="Seg_4316" n="e" s="T392">mĭbiem. </ts>
               <ts e="T394" id="Seg_4318" n="e" s="T393">Dĭgəttə </ts>
               <ts e="T395" id="Seg_4320" n="e" s="T394">mĭmbi. </ts>
               <ts e="T396" id="Seg_4322" n="e" s="T395">Iʔ </ts>
               <ts e="T397" id="Seg_4324" n="e" s="T396">deʔkeʔ </ts>
               <ts e="T398" id="Seg_4326" n="e" s="T397">dĭ </ts>
               <ts e="T399" id="Seg_4328" n="e" s="T398">ne, </ts>
               <ts e="T400" id="Seg_4330" n="e" s="T399">kazak </ts>
               <ts e="T401" id="Seg_4332" n="e" s="T400">ne. </ts>
               <ts e="T402" id="Seg_4334" n="e" s="T401">Забыла </ts>
               <ts e="T403" id="Seg_4336" n="e" s="T402">((BRK)). </ts>
               <ts e="T404" id="Seg_4338" n="e" s="T403">(Šamangən) </ts>
               <ts e="T405" id="Seg_4340" n="e" s="T404">bar </ts>
               <ts e="T406" id="Seg_4342" n="e" s="T405">părga </ts>
               <ts e="T407" id="Seg_4344" n="e" s="T406">ibi. </ts>
               <ts e="T409" id="Seg_4346" n="e" s="T407">Всякий-всякий </ts>
               <ts e="T410" id="Seg_4348" n="e" s="T409">был </ts>
               <ts e="T411" id="Seg_4350" n="e" s="T410">bar </ts>
               <ts e="T412" id="Seg_4352" n="e" s="T411">(kuriz-) </ts>
               <ts e="T413" id="Seg_4354" n="e" s="T412">kurizən </ts>
               <ts e="T414" id="Seg_4356" n="e" s="T413">kuba, </ts>
               <ts e="T415" id="Seg_4358" n="e" s="T414">albugan </ts>
               <ts e="T416" id="Seg_4360" n="e" s="T415">kuba, </ts>
               <ts e="T417" id="Seg_4362" n="e" s="T416">părgagən, </ts>
               <ts e="T418" id="Seg_4364" n="e" s="T417">tažəbən </ts>
               <ts e="T419" id="Seg_4366" n="e" s="T418">(p-) </ts>
               <ts e="T420" id="Seg_4368" n="e" s="T419">kuba </ts>
               <ts e="T422" id="Seg_4370" n="e" s="T420">părgagən. </ts>
               <ts e="T423" id="Seg_4372" n="e" s="T422">Poʔton </ts>
               <ts e="T424" id="Seg_4374" n="e" s="T423">kuba. </ts>
               <ts e="T425" id="Seg_4376" n="e" s="T424">((BRK)). </ts>
               <ts e="T426" id="Seg_4378" n="e" s="T425">Dĭ </ts>
               <ts e="T427" id="Seg_4380" n="e" s="T426">šaman </ts>
               <ts e="T428" id="Seg_4382" n="e" s="T427">bar </ts>
               <ts e="T429" id="Seg_4384" n="e" s="T428">шаманил </ts>
               <ts e="T430" id="Seg_4386" n="e" s="T429">- </ts>
               <ts e="T431" id="Seg_4388" n="e" s="T430">шаманил, </ts>
               <ts e="T432" id="Seg_4390" n="e" s="T431">dĭgəttə </ts>
               <ts e="T433" id="Seg_4392" n="e" s="T432">dagaj </ts>
               <ts e="T434" id="Seg_4394" n="e" s="T433">ibi </ts>
               <ts e="T435" id="Seg_4396" n="e" s="T434">da </ts>
               <ts e="T436" id="Seg_4398" n="e" s="T435">bar </ts>
               <ts e="T437" id="Seg_4400" n="e" s="T436">müʔbdəbi </ts>
               <ts e="T438" id="Seg_4402" n="e" s="T437">(siʔ-) </ts>
               <ts e="T439" id="Seg_4404" n="e" s="T438">sĭjdə. </ts>
               <ts e="T440" id="Seg_4406" n="e" s="T439">I </ts>
               <ts e="T441" id="Seg_4408" n="e" s="T440">dĭgəttə </ts>
               <ts e="T442" id="Seg_4410" n="e" s="T441">saʔməluʔpi. </ts>
               <ts e="T443" id="Seg_4412" n="e" s="T442">A </ts>
               <ts e="T444" id="Seg_4414" n="e" s="T443">il </ts>
               <ts e="T445" id="Seg_4416" n="e" s="T444">iʔgö </ts>
               <ts e="T446" id="Seg_4418" n="e" s="T445">šonəgaʔi </ts>
               <ts e="T447" id="Seg_4420" n="e" s="T446">быдто </ts>
               <ts e="T448" id="Seg_4422" n="e" s="T447">(s </ts>
               <ts e="T449" id="Seg_4424" n="e" s="T448">-) </ts>
               <ts e="T450" id="Seg_4426" n="e" s="T449">Минусинские. </ts>
               <ts e="T455" id="Seg_4428" n="e" s="T450">По-русски сказала… </ts>
               <ts e="T456" id="Seg_4430" n="e" s="T455">((BRK)). </ts>
               <ts e="T457" id="Seg_4432" n="e" s="T456">Tibiʔi </ts>
               <ts e="T458" id="Seg_4434" n="e" s="T457">(ka- </ts>
               <ts e="T459" id="Seg_4436" n="e" s="T458">ka-) </ts>
               <ts e="T460" id="Seg_4438" n="e" s="T459">kamen </ts>
               <ts e="T461" id="Seg_4440" n="e" s="T460">kallaʔi </ts>
               <ts e="T462" id="Seg_4442" n="e" s="T461">dʼijenə </ts>
               <ts e="T463" id="Seg_4444" n="e" s="T462">dĭgəttə </ts>
               <ts e="T464" id="Seg_4446" n="e" s="T463">šamandə </ts>
               <ts e="T465" id="Seg_4448" n="e" s="T464">kallaʔi. </ts>
               <ts e="T466" id="Seg_4450" n="e" s="T465">Dĭ </ts>
               <ts e="T467" id="Seg_4452" n="e" s="T466">mălləj: </ts>
               <ts e="T468" id="Seg_4454" n="e" s="T467">Tăn </ts>
               <ts e="T469" id="Seg_4456" n="e" s="T468">(u-) </ts>
               <ts e="T470" id="Seg_4458" n="e" s="T469">albuga </ts>
               <ts e="T471" id="Seg_4460" n="e" s="T470">kutlal, </ts>
               <ts e="T472" id="Seg_4462" n="e" s="T471">a </ts>
               <ts e="T473" id="Seg_4464" n="e" s="T472">šindinə </ts>
               <ts e="T474" id="Seg_4466" n="e" s="T473">mălləj: </ts>
               <ts e="T475" id="Seg_4468" n="e" s="T474">Ej </ts>
               <ts e="T476" id="Seg_4470" n="e" s="T475">kutlal". </ts>
               <ts e="T477" id="Seg_4472" n="e" s="T476">((BRK)). </ts>
               <ts e="T478" id="Seg_4474" n="e" s="T477">Минусинский </ts>
               <ts e="T479" id="Seg_4476" n="e" s="T478">šaman </ts>
               <ts e="T480" id="Seg_4478" n="e" s="T479">i </ts>
               <ts e="T481" id="Seg_4480" n="e" s="T480">miʔ </ts>
               <ts e="T482" id="Seg_4482" n="e" s="T481">šaman </ts>
               <ts e="T483" id="Seg_4484" n="e" s="T482">bar </ts>
               <ts e="T484" id="Seg_4486" n="e" s="T483">dʼabrobiʔi. </ts>
               <ts e="T485" id="Seg_4488" n="e" s="T484">(Minu-) </ts>
               <ts e="T486" id="Seg_4490" n="e" s="T485">Минусинский </ts>
               <ts e="T487" id="Seg_4492" n="e" s="T486">kambi </ts>
               <ts e="T488" id="Seg_4494" n="e" s="T487">maʔndə, </ts>
               <ts e="T489" id="Seg_4496" n="e" s="T488">a </ts>
               <ts e="T490" id="Seg_4498" n="e" s="T489">miʔ </ts>
               <ts e="T491" id="Seg_4500" n="e" s="T490">šaman </ts>
               <ts e="T492" id="Seg_4502" n="e" s="T491">(dĭ-) </ts>
               <ts e="T493" id="Seg_4504" n="e" s="T492">dĭm </ts>
               <ts e="T494" id="Seg_4506" n="e" s="T493">bar </ts>
               <ts e="T495" id="Seg_4508" n="e" s="T494">bĭdleʔpi. </ts>
               <ts e="T496" id="Seg_4510" n="e" s="T495">Pograš </ts>
               <ts e="T497" id="Seg_4512" n="e" s="T496">molambi. </ts>
               <ts e="T498" id="Seg_4514" n="e" s="T497">A </ts>
               <ts e="T499" id="Seg_4516" n="e" s="T498">dĭ </ts>
               <ts e="T500" id="Seg_4518" n="e" s="T499">bar </ts>
               <ts e="T501" id="Seg_4520" n="e" s="T500">multuksi </ts>
               <ts e="T502" id="Seg_4522" n="e" s="T501">dĭm </ts>
               <ts e="T503" id="Seg_4524" n="e" s="T502">kutlambi. </ts>
               <ts e="T504" id="Seg_4526" n="e" s="T503">(Udanə) </ts>
               <ts e="T505" id="Seg_4528" n="e" s="T504">Udabə </ts>
               <ts e="T506" id="Seg_4530" n="e" s="T505">bar </ts>
               <ts e="T507" id="Seg_4532" n="e" s="T506">băldəbi. </ts>
               <ts e="T509" id="Seg_4534" n="e" s="T507">Руку сломал. </ts>
               <ts e="T510" id="Seg_4536" n="e" s="T509">((BRK)). </ts>
               <ts e="T511" id="Seg_4538" n="e" s="T510">Girgit </ts>
               <ts e="T512" id="Seg_4540" n="e" s="T511">kuza </ts>
               <ts e="T513" id="Seg_4542" n="e" s="T512">ĭzemnuʔpi, </ts>
               <ts e="T514" id="Seg_4544" n="e" s="T513">dĭgəttə </ts>
               <ts e="T515" id="Seg_4546" n="e" s="T514">kalaʔi </ts>
               <ts e="T516" id="Seg_4548" n="e" s="T515">šamandə, </ts>
               <ts e="T517" id="Seg_4550" n="e" s="T516">dĭ </ts>
               <ts e="T518" id="Seg_4552" n="e" s="T517">bar </ts>
               <ts e="T519" id="Seg_4554" n="e" s="T518">dĭm </ts>
               <ts e="T520" id="Seg_4556" n="e" s="T519">dʼaziria. </ts>
               <ts e="T521" id="Seg_4558" n="e" s="T520">Lʼevăj </ts>
               <ts e="T522" id="Seg_4560" n="e" s="T521">udandə </ts>
               <ts e="T523" id="Seg_4562" n="e" s="T522">(nuldləj) </ts>
               <ts e="T524" id="Seg_4564" n="e" s="T523">(dĭ </ts>
               <ts e="T525" id="Seg_4566" n="e" s="T524">üge). </ts>
               <ts e="T526" id="Seg_4568" n="e" s="T525">Dăre </ts>
               <ts e="T527" id="Seg_4570" n="e" s="T526">bar </ts>
               <ts e="T528" id="Seg_4572" n="e" s="T527">ulutsi </ts>
               <ts e="T529" id="Seg_4574" n="e" s="T528">(numa-) </ts>
               <ts e="T530" id="Seg_4576" n="e" s="T529">кланяется, </ts>
               <ts e="T531" id="Seg_4578" n="e" s="T530">кланяется. </ts>
               <ts e="T532" id="Seg_4580" n="e" s="T531">Dĭgəttə </ts>
               <ts e="T533" id="Seg_4582" n="e" s="T532">dĭ </ts>
               <ts e="T534" id="Seg_4584" n="e" s="T533">dĭʔnə </ts>
               <ts e="T535" id="Seg_4586" n="e" s="T534">tʼažerluʔpi </ts>
               <ts e="T536" id="Seg_4588" n="e" s="T535">i </ts>
               <ts e="T537" id="Seg_4590" n="e" s="T536">bostə </ts>
               <ts e="T538" id="Seg_4592" n="e" s="T537">bar </ts>
               <ts e="T539" id="Seg_4594" n="e" s="T538">suʔmileʔbə, </ts>
               <ts e="T540" id="Seg_4596" n="e" s="T539">bubendə </ts>
               <ts e="T541" id="Seg_4598" n="e" s="T540">sʼarlaʔbə. </ts>
               <ts e="T542" id="Seg_4600" n="e" s="T541">((BRK)). </ts>
               <ts e="T543" id="Seg_4602" n="e" s="T542">Dĭgəttə </ts>
               <ts e="T544" id="Seg_4604" n="e" s="T543">dĭ </ts>
               <ts e="T545" id="Seg_4606" n="e" s="T544">ej </ts>
               <ts e="T546" id="Seg_4608" n="e" s="T545">ĭzemnie, </ts>
               <ts e="T547" id="Seg_4610" n="e" s="T546">maʔndə </ts>
               <ts e="T548" id="Seg_4612" n="e" s="T547">šoləj. </ts>
               <ts e="T549" id="Seg_4614" n="e" s="T548">((BRK)). </ts>
               <ts e="T550" id="Seg_4616" n="e" s="T549">((BRK)). </ts>
               <ts e="T551" id="Seg_4618" n="e" s="T550">Tibizeŋ </ts>
               <ts e="T552" id="Seg_4620" n="e" s="T551">dʼijenə </ts>
               <ts e="T848" id="Seg_4622" n="e" s="T552">kalla </ts>
               <ts e="T553" id="Seg_4624" n="e" s="T848">dʼürbiʔi, </ts>
               <ts e="T554" id="Seg_4626" n="e" s="T553">nezeŋ </ts>
               <ts e="T555" id="Seg_4628" n="e" s="T554">(maʔən) </ts>
               <ts e="T556" id="Seg_4630" n="e" s="T555">amnobiʔi. </ts>
               <ts e="T557" id="Seg_4632" n="e" s="T556">Sarankaʔi </ts>
               <ts e="T558" id="Seg_4634" n="e" s="T557">tĭlbiʔi, </ts>
               <ts e="T559" id="Seg_4636" n="e" s="T558">ujam </ts>
               <ts e="T560" id="Seg_4638" n="e" s="T559">(embiʔi), </ts>
               <ts e="T561" id="Seg_4640" n="e" s="T560">ipek </ts>
               <ts e="T562" id="Seg_4642" n="e" s="T561">naga. </ts>
               <ts e="T563" id="Seg_4644" n="e" s="T562">((BRK)). </ts>
               <ts e="T564" id="Seg_4646" n="e" s="T563">Nezeŋ </ts>
               <ts e="T565" id="Seg_4648" n="e" s="T564">bar </ts>
               <ts e="T566" id="Seg_4650" n="e" s="T565">(n-) </ts>
               <ts e="T567" id="Seg_4652" n="e" s="T566">noʔ </ts>
               <ts e="T568" id="Seg_4654" n="e" s="T567">nĭŋgəbiʔi. </ts>
               <ts e="T569" id="Seg_4656" n="e" s="T568">Travʼanʼigaʔi </ts>
               <ts e="T570" id="Seg_4658" n="e" s="T569">kürbiʔi, </ts>
               <ts e="T571" id="Seg_4660" n="e" s="T570">jamaʔi </ts>
               <ts e="T572" id="Seg_4662" n="e" s="T571">šöʔpiʔi, </ts>
               <ts e="T573" id="Seg_4664" n="e" s="T572">žilaʔi </ts>
               <ts e="T574" id="Seg_4666" n="e" s="T573">kürbiʔi. </ts>
               <ts e="T575" id="Seg_4668" n="e" s="T574">Dʼijegən </ts>
               <ts e="T576" id="Seg_4670" n="e" s="T575">maʔ </ts>
               <ts e="T577" id="Seg_4672" n="e" s="T576">(ab-) </ts>
               <ts e="T578" id="Seg_4674" n="e" s="T577">abiʔi. </ts>
               <ts e="T579" id="Seg_4676" n="e" s="T578">Dĭgəttə </ts>
               <ts e="T580" id="Seg_4678" n="e" s="T579">dĭ </ts>
               <ts e="T581" id="Seg_4680" n="e" s="T580">maʔkən </ts>
               <ts e="T582" id="Seg_4682" n="e" s="T581">uja </ts>
               <ts e="T583" id="Seg_4684" n="e" s="T582">mĭnzerbiʔi. </ts>
               <ts e="T584" id="Seg_4686" n="e" s="T583">Segi </ts>
               <ts e="T585" id="Seg_4688" n="e" s="T584">bü </ts>
               <ts e="T586" id="Seg_4690" n="e" s="T585">mĭnzerbiʔi, </ts>
               <ts e="T587" id="Seg_4692" n="e" s="T586">amorbiʔi. </ts>
               <ts e="T588" id="Seg_4694" n="e" s="T587">((BRK)). </ts>
               <ts e="T589" id="Seg_4696" n="e" s="T588">Turaʔi </ts>
               <ts e="T590" id="Seg_4698" n="e" s="T589">ibiʔi, </ts>
               <ts e="T591" id="Seg_4700" n="e" s="T590">dĭ </ts>
               <ts e="T592" id="Seg_4702" n="e" s="T591">turagən </ts>
               <ts e="T593" id="Seg_4704" n="e" s="T592">ej </ts>
               <ts e="T594" id="Seg_4706" n="e" s="T593">(amnobiʔi). </ts>
               <ts e="T595" id="Seg_4708" n="e" s="T594">Nʼiʔnen </ts>
               <ts e="T596" id="Seg_4710" n="e" s="T595">maʔ </ts>
               <ts e="T597" id="Seg_4712" n="e" s="T596">ibi. </ts>
               <ts e="T598" id="Seg_4714" n="e" s="T597">Dĭn </ts>
               <ts e="T599" id="Seg_4716" n="e" s="T598">mĭnzerbiʔi </ts>
               <ts e="T600" id="Seg_4718" n="e" s="T599">mĭj. </ts>
               <ts e="T601" id="Seg_4720" n="e" s="T600">Segi </ts>
               <ts e="T602" id="Seg_4722" n="e" s="T601">bü </ts>
               <ts e="T603" id="Seg_4724" n="e" s="T602">mĭnzerbiʔi. </ts>
               <ts e="T604" id="Seg_4726" n="e" s="T603">Amorbiʔi, </ts>
               <ts e="T605" id="Seg_4728" n="e" s="T604">a </ts>
               <ts e="T606" id="Seg_4730" n="e" s="T605">turagən </ts>
               <ts e="T607" id="Seg_4732" n="e" s="T606">kunolbiʔi. </ts>
               <ts e="T608" id="Seg_4734" n="e" s="T607">((BRK)). </ts>
               <ts e="T609" id="Seg_4736" n="e" s="T608">Maʔi </ts>
               <ts e="T610" id="Seg_4738" n="e" s="T609">abiʔi </ts>
               <ts e="T611" id="Seg_4740" n="e" s="T610">(pats-) </ts>
               <ts e="T612" id="Seg_4742" n="e" s="T611">paʔi, </ts>
               <ts e="T613" id="Seg_4744" n="e" s="T612">dĭgəttə </ts>
               <ts e="T614" id="Seg_4746" n="e" s="T613">kuba </ts>
               <ts e="T615" id="Seg_4748" n="e" s="T614">kürbiʔi </ts>
               <ts e="T616" id="Seg_4750" n="e" s="T615">patsi </ts>
               <ts e="T617" id="Seg_4752" n="e" s="T616">da </ts>
               <ts e="T618" id="Seg_4754" n="e" s="T617">enbiʔi </ts>
               <ts e="T619" id="Seg_4756" n="e" s="T618">diʔnə. </ts>
               <ts e="T620" id="Seg_4758" n="e" s="T619">Dĭgəttə </ts>
               <ts e="T621" id="Seg_4760" n="e" s="T620">taganəʔi </ts>
               <ts e="T622" id="Seg_4762" n="e" s="T621">abiʔi, </ts>
               <ts e="T623" id="Seg_4764" n="e" s="T622">dĭgəttə </ts>
               <ts e="T624" id="Seg_4766" n="e" s="T623">aspaʔi </ts>
               <ts e="T625" id="Seg_4768" n="e" s="T624">edəbiʔi, </ts>
               <ts e="T626" id="Seg_4770" n="e" s="T625">(mĭnzer- </ts>
               <ts e="T627" id="Seg_4772" n="e" s="T626">mĭnzeri-) </ts>
               <ts e="T628" id="Seg_4774" n="e" s="T627">mĭnzerzittə </ts>
               <ts e="T629" id="Seg_4776" n="e" s="T628">uja </ts>
               <ts e="T630" id="Seg_4778" n="e" s="T629">da, </ts>
               <ts e="T631" id="Seg_4780" n="e" s="T630">segi </ts>
               <ts e="T632" id="Seg_4782" n="e" s="T631">bü </ts>
               <ts e="T633" id="Seg_4784" n="e" s="T632">mĭnzerzittə. </ts>
               <ts e="T635" id="Seg_4786" n="e" s="T633">Dĭgəttə… </ts>
               <ts e="T636" id="Seg_4788" n="e" s="T635">Стой </ts>
               <ts e="T637" id="Seg_4790" n="e" s="T636">((BRK)). </ts>
               <ts e="T638" id="Seg_4792" n="e" s="T637">Dʼügən </ts>
               <ts e="T639" id="Seg_4794" n="e" s="T638">šü </ts>
               <ts e="T640" id="Seg_4796" n="e" s="T639">ambiʔi. </ts>
               <ts e="T641" id="Seg_4798" n="e" s="T640">Nʼüʔnən </ts>
               <ts e="T642" id="Seg_4800" n="e" s="T641">ši </ts>
               <ts e="T643" id="Seg_4802" n="e" s="T642">ibi </ts>
               <ts e="T644" id="Seg_4804" n="e" s="T643">dĭbər, </ts>
               <ts e="T645" id="Seg_4806" n="e" s="T644">(be-) </ts>
               <ts e="T646" id="Seg_4808" n="e" s="T645">ber </ts>
               <ts e="T647" id="Seg_4810" n="e" s="T646">kandlaʔbə. </ts>
               <ts e="T648" id="Seg_4812" n="e" s="T647">Dĭgəttə </ts>
               <ts e="T649" id="Seg_4814" n="e" s="T648">paʔi </ts>
               <ts e="T650" id="Seg_4816" n="e" s="T649">ambiʔi, </ts>
               <ts e="T651" id="Seg_4818" n="e" s="T650">štobɨ </ts>
               <ts e="T652" id="Seg_4820" n="e" s="T651">šünə </ts>
               <ts e="T653" id="Seg_4822" n="e" s="T652">šindidə </ts>
               <ts e="T654" id="Seg_4824" n="e" s="T653">ej </ts>
               <ts e="T655" id="Seg_4826" n="e" s="T654">(š-) </ts>
               <ts e="T656" id="Seg_4828" n="e" s="T655">saʔməbi. </ts>
               <ts e="T657" id="Seg_4830" n="e" s="T656">((BRK)). </ts>
               <ts e="T658" id="Seg_4832" n="e" s="T657">Šišəge </ts>
               <ts e="T659" id="Seg_4834" n="e" s="T658">mobi </ts>
               <ts e="T660" id="Seg_4836" n="e" s="T659">dak, </ts>
               <ts e="T661" id="Seg_4838" n="e" s="T660">(kožaʔi=) </ts>
               <ts e="T662" id="Seg_4840" n="e" s="T661">kožazi </ts>
               <ts e="T663" id="Seg_4842" n="e" s="T662">kajbiʔi </ts>
               <ts e="T664" id="Seg_4844" n="e" s="T663">(a=). </ts>
               <ts e="T665" id="Seg_4846" n="e" s="T664">Ejü </ts>
               <ts e="T666" id="Seg_4848" n="e" s="T665">mobi </ts>
               <ts e="T667" id="Seg_4850" n="e" s="T666">dak, </ts>
               <ts e="T668" id="Seg_4852" n="e" s="T667">(ku- </ts>
               <ts e="T669" id="Seg_4854" n="e" s="T668">kürbiʔi= </ts>
               <ts e="T670" id="Seg_4856" n="e" s="T669">kambiʔ-) </ts>
               <ts e="T671" id="Seg_4858" n="e" s="T670">kajbiʔi. </ts>
               <ts e="T672" id="Seg_4860" n="e" s="T671">((BRK)). </ts>
               <ts e="T673" id="Seg_4862" n="e" s="T672">Urgo </ts>
               <ts e="T674" id="Seg_4864" n="e" s="T673">ibi, </ts>
               <ts e="T675" id="Seg_4866" n="e" s="T674">(me-) </ts>
               <ts e="T676" id="Seg_4868" n="e" s="T675">метра </ts>
               <ts e="T677" id="Seg_4870" n="e" s="T676">teʔtə. </ts>
               <ts e="T678" id="Seg_4872" n="e" s="T677">Il </ts>
               <ts e="T679" id="Seg_4874" n="e" s="T678">iʔbobiʔi, </ts>
               <ts e="T680" id="Seg_4876" n="e" s="T679">bʼeʔ </ts>
               <ts e="T681" id="Seg_4878" n="e" s="T680">il. </ts>
               <ts e="T682" id="Seg_4880" n="e" s="T681">Kamen </ts>
               <ts e="T683" id="Seg_4882" n="e" s="T682">sĭre </ts>
               <ts e="T684" id="Seg_4884" n="e" s="T683">ibi, </ts>
               <ts e="T685" id="Seg_4886" n="e" s="T684">dĭzeŋ </ts>
               <ts e="T686" id="Seg_4888" n="e" s="T685">dön </ts>
               <ts e="T687" id="Seg_4890" n="e" s="T686">amnobiʔi. </ts>
               <ts e="T688" id="Seg_4892" n="e" s="T687">A </ts>
               <ts e="T689" id="Seg_4894" n="e" s="T688">kamen </ts>
               <ts e="T690" id="Seg_4896" n="e" s="T689">sĭre </ts>
               <ts e="T849" id="Seg_4898" n="e" s="T690">kalla </ts>
               <ts e="T691" id="Seg_4900" n="e" s="T849">dʼürbi, </ts>
               <ts e="T692" id="Seg_4902" n="e" s="T691">dĭgəttə </ts>
               <ts e="T693" id="Seg_4904" n="e" s="T692">dĭzeŋ </ts>
               <ts e="T694" id="Seg_4906" n="e" s="T693">kandəgaʔi </ts>
               <ts e="T695" id="Seg_4908" n="e" s="T694">dʼijenə, </ts>
               <ts e="T696" id="Seg_4910" n="e" s="T695">urgo </ts>
               <ts e="T697" id="Seg_4912" n="e" s="T696">măjanə. </ts>
               <ts e="T698" id="Seg_4914" n="e" s="T697">((BRK)). </ts>
               <ts e="T699" id="Seg_4916" n="e" s="T698">Ălenʼəʔizi </ts>
               <ts e="T700" id="Seg_4918" n="e" s="T699">mĭmbiʔi, </ts>
               <ts e="T701" id="Seg_4920" n="e" s="T700">Tʼelamdə </ts>
               <ts e="T702" id="Seg_4922" n="e" s="T701">kanbiʔi, </ts>
               <ts e="T703" id="Seg_4924" n="e" s="T702">dĭn </ts>
               <ts e="T704" id="Seg_4926" n="e" s="T703">urgo </ts>
               <ts e="T705" id="Seg_4928" n="e" s="T704">bü, </ts>
               <ts e="T706" id="Seg_4930" n="e" s="T705">kola </ts>
               <ts e="T707" id="Seg_4932" n="e" s="T706">dʼaʔpiʔi. </ts>
               <ts e="T708" id="Seg_4934" n="e" s="T707">((BRK)). </ts>
               <ts e="T709" id="Seg_4936" n="e" s="T708">Dĭgəttə </ts>
               <ts e="T710" id="Seg_4938" n="e" s="T709">dĭzeŋ </ts>
               <ts e="T711" id="Seg_4940" n="e" s="T710">kambiʔi, </ts>
               <ts e="T712" id="Seg_4942" n="e" s="T711">esseŋdə, </ts>
               <ts e="T713" id="Seg_4944" n="e" s="T712">i </ts>
               <ts e="T714" id="Seg_4946" n="e" s="T713">nezeŋdə </ts>
               <ts e="T715" id="Seg_4948" n="e" s="T714">ibiʔi, </ts>
               <ts e="T716" id="Seg_4950" n="e" s="T715">ălenʼəʔizi </ts>
               <ts e="T717" id="Seg_4952" n="e" s="T716">kambiʔi. </ts>
               <ts e="T718" id="Seg_4954" n="e" s="T717">((BRK)). </ts>
               <ts e="T719" id="Seg_4956" n="e" s="T718">I </ts>
               <ts e="T720" id="Seg_4958" n="e" s="T719">menzeŋzi. </ts>
               <ts e="T721" id="Seg_4960" n="e" s="T720">((BRK)). </ts>
               <ts e="T722" id="Seg_4962" n="e" s="T721">Dĭgəttə </ts>
               <ts e="T723" id="Seg_4964" n="e" s="T722">dĭzeŋ </ts>
               <ts e="T724" id="Seg_4966" n="e" s="T723">dʼijegən </ts>
               <ts e="T725" id="Seg_4968" n="e" s="T724">keʔbde </ts>
               <ts e="T726" id="Seg_4970" n="e" s="T725">oʔbdəbiʔi, </ts>
               <ts e="T727" id="Seg_4972" n="e" s="T726">ambiʔi. </ts>
               <ts e="T728" id="Seg_4974" n="e" s="T727">San </ts>
               <ts e="T729" id="Seg_4976" n="e" s="T728">oʔbdəbiʔi </ts>
               <ts e="T730" id="Seg_4978" n="e" s="T729">lejbiʔi. </ts>
               <ts e="T731" id="Seg_4980" n="e" s="T730">((BRK)). </ts>
               <ts e="T732" id="Seg_4982" n="e" s="T731">Dĭgəttə </ts>
               <ts e="T733" id="Seg_4984" n="e" s="T732">dĭzeŋ </ts>
               <ts e="T734" id="Seg_4986" n="e" s="T733">beškeʔi </ts>
               <ts e="T735" id="Seg_4988" n="e" s="T734">oʔbdəbiʔi, </ts>
               <ts e="T736" id="Seg_4990" n="e" s="T735">mĭnzerbiʔi </ts>
               <ts e="T737" id="Seg_4992" n="e" s="T736">i </ts>
               <ts e="T738" id="Seg_4994" n="e" s="T737">tustʼarbiʔi. </ts>
               <ts e="T739" id="Seg_4996" n="e" s="T738">((BRK)). </ts>
               <ts e="T740" id="Seg_4998" n="e" s="T739">Dĭgəttə </ts>
               <ts e="T741" id="Seg_5000" n="e" s="T740">(dĭn=) </ts>
               <ts e="T742" id="Seg_5002" n="e" s="T741">dĭzen </ts>
               <ts e="T743" id="Seg_5004" n="e" s="T742">dĭn </ts>
               <ts e="T744" id="Seg_5006" n="e" s="T743">kuʔpiʔi </ts>
               <ts e="T745" id="Seg_5008" n="e" s="T744">sɨneʔi </ts>
               <ts e="T746" id="Seg_5010" n="e" s="T745">i </ts>
               <ts e="T747" id="Seg_5012" n="e" s="T746">uja </ts>
               <ts e="T748" id="Seg_5014" n="e" s="T747">tustʼarbiʔi. </ts>
               <ts e="T749" id="Seg_5016" n="e" s="T748">Dĭgəttə </ts>
               <ts e="T750" id="Seg_5018" n="e" s="T749">edəbiʔi, </ts>
               <ts e="T751" id="Seg_5020" n="e" s="T750">dĭ </ts>
               <ts e="T752" id="Seg_5022" n="e" s="T751">(ko-) </ts>
               <ts e="T753" id="Seg_5024" n="e" s="T752">kolambi, </ts>
               <ts e="T754" id="Seg_5026" n="e" s="T753">baltuzi </ts>
               <ts e="T755" id="Seg_5028" n="e" s="T754">jaʔpiʔi, </ts>
               <ts e="T756" id="Seg_5030" n="e" s="T755">buranə </ts>
               <ts e="T757" id="Seg_5032" n="e" s="T756">kămnəbiʔi. </ts>
               <ts e="T758" id="Seg_5034" n="e" s="T757">((BRK)). </ts>
               <ts e="T759" id="Seg_5036" n="e" s="T758">Dĭzeŋ </ts>
               <ts e="T760" id="Seg_5038" n="e" s="T759">dĭgəttə </ts>
               <ts e="T761" id="Seg_5040" n="e" s="T760">kola </ts>
               <ts e="T762" id="Seg_5042" n="e" s="T761">dʼaʔpiʔi </ts>
               <ts e="T763" id="Seg_5044" n="e" s="T762">i </ts>
               <ts e="T764" id="Seg_5046" n="e" s="T763">tustʼarbiʔi, </ts>
               <ts e="T765" id="Seg_5048" n="e" s="T764">i </ts>
               <ts e="T766" id="Seg_5050" n="e" s="T765">tože </ts>
               <ts e="T767" id="Seg_5052" n="e" s="T766">kola </ts>
               <ts e="T769" id="Seg_5054" n="e" s="T767">(koʔpiʔi) </ts>
               <ts e="T770" id="Seg_5056" n="e" s="T769">сушили. </ts>
               <ts e="T771" id="Seg_5058" n="e" s="T770">((BRK)). </ts>
               <ts e="T772" id="Seg_5060" n="e" s="T771">Dĭgəttə </ts>
               <ts e="T773" id="Seg_5062" n="e" s="T772">kubatsi </ts>
               <ts e="T774" id="Seg_5064" n="e" s="T773">abiʔi </ts>
               <ts e="T775" id="Seg_5066" n="e" s="T774">buraʔi </ts>
               <ts e="T776" id="Seg_5068" n="e" s="T775">i </ts>
               <ts e="T777" id="Seg_5070" n="e" s="T776">dĭbər </ts>
               <ts e="T778" id="Seg_5072" n="e" s="T777">embiʔi </ts>
               <ts e="T779" id="Seg_5074" n="e" s="T778">uja </ts>
               <ts e="T780" id="Seg_5076" n="e" s="T779">i </ts>
               <ts e="T781" id="Seg_5078" n="e" s="T780">kola. </ts>
               <ts e="T782" id="Seg_5080" n="e" s="T781">I </ts>
               <ts e="T783" id="Seg_5082" n="e" s="T782">ălenʼəʔizi </ts>
               <ts e="T784" id="Seg_5084" n="e" s="T783">deʔpiʔi </ts>
               <ts e="T785" id="Seg_5086" n="e" s="T784">maʔnə. </ts>
               <ts e="T786" id="Seg_5088" n="e" s="T785">Döber </ts>
               <ts e="T787" id="Seg_5090" n="e" s="T786">gijen </ts>
               <ts e="T788" id="Seg_5092" n="e" s="T787">amnobiʔi, </ts>
               <ts e="T789" id="Seg_5094" n="e" s="T788">kamen </ts>
               <ts e="T790" id="Seg_5096" n="e" s="T789">šišəge </ts>
               <ts e="T791" id="Seg_5098" n="e" s="T790">ibi. </ts>
               <ts e="T792" id="Seg_5100" n="e" s="T791">((BRK)). </ts>
               <ts e="T793" id="Seg_5102" n="e" s="T792">Dĭzeŋ </ts>
               <ts e="T794" id="Seg_5104" n="e" s="T793">(dĭgət-) </ts>
               <ts e="T795" id="Seg_5106" n="e" s="T794">dĭrgidəʔi </ts>
               <ts e="T796" id="Seg_5108" n="e" s="T795">menzeŋdə </ts>
               <ts e="T797" id="Seg_5110" n="e" s="T796">(b-) </ts>
               <ts e="T798" id="Seg_5112" n="e" s="T797">ibiʔi. </ts>
               <ts e="T799" id="Seg_5114" n="e" s="T798">(Ka-) </ts>
               <ts e="T800" id="Seg_5116" n="e" s="T799">Kalaʔi </ts>
               <ts e="T801" id="Seg_5118" n="e" s="T800">poʔto, </ts>
               <ts e="T802" id="Seg_5120" n="e" s="T801">iʔgö </ts>
               <ts e="T803" id="Seg_5122" n="e" s="T802">dʼabəluʔi, </ts>
               <ts e="T804" id="Seg_5124" n="e" s="T803">dĭgəttə </ts>
               <ts e="T805" id="Seg_5126" n="e" s="T804">maʔndə </ts>
               <ts e="T806" id="Seg_5128" n="e" s="T805">šoləj, </ts>
               <ts e="T807" id="Seg_5130" n="e" s="T806">dĭ </ts>
               <ts e="T808" id="Seg_5132" n="e" s="T807">ine </ts>
               <ts e="T809" id="Seg_5134" n="e" s="T808">kolerləj. </ts>
               <ts e="T810" id="Seg_5136" n="e" s="T809">Dĭgəttə </ts>
               <ts e="T811" id="Seg_5138" n="e" s="T810">kandəga </ts>
               <ts e="T812" id="Seg_5140" n="e" s="T811">poʔto </ts>
               <ts e="T813" id="Seg_5142" n="e" s="T812">oʔbdəsʼtə. </ts>
               <ts e="T814" id="Seg_5144" n="e" s="T813">Dĭgəttə </ts>
               <ts e="T815" id="Seg_5146" n="e" s="T814">šoləj </ts>
               <ts e="T816" id="Seg_5148" n="e" s="T815">maʔndə. </ts>
               <ts e="T817" id="Seg_5150" n="e" s="T816">((BRK)). </ts>
               <ts e="T818" id="Seg_5152" n="e" s="T817">I </ts>
               <ts e="T819" id="Seg_5154" n="e" s="T818">poʔto </ts>
               <ts e="T820" id="Seg_5156" n="e" s="T819">detləj. </ts>
               <ts e="T821" id="Seg_5158" n="e" s="T820">((BRK)). </ts>
               <ts e="T822" id="Seg_5160" n="e" s="T821">(Dĭz-) </ts>
               <ts e="T823" id="Seg_5162" n="e" s="T822">Dĭzem </ts>
               <ts e="T824" id="Seg_5164" n="e" s="T823">(a-) </ts>
               <ts e="T825" id="Seg_5166" n="e" s="T824">armijanə </ts>
               <ts e="T826" id="Seg_5168" n="e" s="T825">ej </ts>
               <ts e="T827" id="Seg_5170" n="e" s="T826">ibiʔi, </ts>
               <ts e="T828" id="Seg_5172" n="e" s="T827">dĭzeŋ </ts>
               <ts e="T829" id="Seg_5174" n="e" s="T828">(ĭsak </ts>
               <ts e="T830" id="Seg_5176" n="e" s="T829">aktʼa=) </ts>
               <ts e="T831" id="Seg_5178" n="e" s="T830">aktʼa </ts>
               <ts e="T832" id="Seg_5180" n="e" s="T831">mĭbiʔi </ts>
               <ts e="T833" id="Seg_5182" n="e" s="T832">i </ts>
               <ts e="T834" id="Seg_5184" n="e" s="T833">albuga </ts>
               <ts e="T835" id="Seg_5186" n="e" s="T834">mĭbiʔi. </ts>
               <ts e="T836" id="Seg_5188" n="e" s="T835">((BRK)). </ts>
               <ts e="T837" id="Seg_5190" n="e" s="T836">Dĭzeŋ </ts>
               <ts e="T838" id="Seg_5192" n="e" s="T837">ĭsaktə </ts>
               <ts e="T839" id="Seg_5194" n="e" s="T838">mĭmbiʔi </ts>
               <ts e="T840" id="Seg_5196" n="e" s="T839">aktʼa, </ts>
               <ts e="T841" id="Seg_5198" n="e" s="T840">(bʼeʔ=) </ts>
               <ts e="T842" id="Seg_5200" n="e" s="T841">bʼeʔ </ts>
               <ts e="T843" id="Seg_5202" n="e" s="T842">sumna. </ts>
               <ts e="T846" id="Seg_5204" n="e" s="T843">Ну вот так. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_5205" s="T0">PKZ_196X_SU0221.001 (001)</ta>
            <ta e="T5" id="Seg_5206" s="T3">PKZ_196X_SU0221.002 (002)</ta>
            <ta e="T9" id="Seg_5207" s="T5">PKZ_196X_SU0221.003 (003)</ta>
            <ta e="T13" id="Seg_5208" s="T9">PKZ_196X_SU0221.004 (004)</ta>
            <ta e="T16" id="Seg_5209" s="T13">PKZ_196X_SU0221.005 (005)</ta>
            <ta e="T17" id="Seg_5210" s="T16">PKZ_196X_SU0221.006 (006)</ta>
            <ta e="T26" id="Seg_5211" s="T17">PKZ_196X_SU0221.007 (007)</ta>
            <ta e="T31" id="Seg_5212" s="T26">PKZ_196X_SU0221.008 (008)</ta>
            <ta e="T32" id="Seg_5213" s="T31">PKZ_196X_SU0221.009 (009)</ta>
            <ta e="T37" id="Seg_5214" s="T32">PKZ_196X_SU0221.010 (010)</ta>
            <ta e="T43" id="Seg_5215" s="T37">PKZ_196X_SU0221.011 (011)</ta>
            <ta e="T44" id="Seg_5216" s="T43">PKZ_196X_SU0221.012 (012)</ta>
            <ta e="T49" id="Seg_5217" s="T44">PKZ_196X_SU0221.013 (013)</ta>
            <ta e="T58" id="Seg_5218" s="T49">PKZ_196X_SU0221.014 (014)</ta>
            <ta e="T59" id="Seg_5219" s="T58">PKZ_196X_SU0221.015 (015)</ta>
            <ta e="T65" id="Seg_5220" s="T59">PKZ_196X_SU0221.016 (016)</ta>
            <ta e="T66" id="Seg_5221" s="T65">PKZ_196X_SU0221.017 (017)</ta>
            <ta e="T70" id="Seg_5222" s="T66">PKZ_196X_SU0221.018 (018)</ta>
            <ta e="T74" id="Seg_5223" s="T70">PKZ_196X_SU0221.019 (019)</ta>
            <ta e="T75" id="Seg_5224" s="T74">PKZ_196X_SU0221.020 (020)</ta>
            <ta e="T81" id="Seg_5225" s="T75">PKZ_196X_SU0221.021 (021)</ta>
            <ta e="T82" id="Seg_5226" s="T81">PKZ_196X_SU0221.022 (022)</ta>
            <ta e="T88" id="Seg_5227" s="T82">PKZ_196X_SU0221.023 (023)</ta>
            <ta e="T89" id="Seg_5228" s="T88">PKZ_196X_SU0221.024 (024)</ta>
            <ta e="T95" id="Seg_5229" s="T89">PKZ_196X_SU0221.025 (025)</ta>
            <ta e="T99" id="Seg_5230" s="T95">PKZ_196X_SU0221.026 (026)</ta>
            <ta e="T101" id="Seg_5231" s="T99">PKZ_196X_SU0221.027 (027)</ta>
            <ta e="T103" id="Seg_5232" s="T101">PKZ_196X_SU0221.028 (028)</ta>
            <ta e="T109" id="Seg_5233" s="T103">PKZ_196X_SU0221.029 (029)</ta>
            <ta e="T112" id="Seg_5234" s="T109">PKZ_196X_SU0221.030 (030)</ta>
            <ta e="T116" id="Seg_5235" s="T112">PKZ_196X_SU0221.031 (031)</ta>
            <ta e="T122" id="Seg_5236" s="T116">PKZ_196X_SU0221.032 (032)</ta>
            <ta e="T129" id="Seg_5237" s="T122">PKZ_196X_SU0221.033 (033)</ta>
            <ta e="T130" id="Seg_5238" s="T129">PKZ_196X_SU0221.034 (034)</ta>
            <ta e="T134" id="Seg_5239" s="T130">PKZ_196X_SU0221.035 (035)</ta>
            <ta e="T135" id="Seg_5240" s="T134">PKZ_196X_SU0221.036 (036)</ta>
            <ta e="T142" id="Seg_5241" s="T135">PKZ_196X_SU0221.037 (037)</ta>
            <ta e="T143" id="Seg_5242" s="T142">PKZ_196X_SU0221.038 (038)</ta>
            <ta e="T147" id="Seg_5243" s="T143">PKZ_196X_SU0221.039 (039)</ta>
            <ta e="T148" id="Seg_5244" s="T147">PKZ_196X_SU0221.040 (040)</ta>
            <ta e="T149" id="Seg_5245" s="T148">PKZ_196X_SU0221.041 (041)</ta>
            <ta e="T154" id="Seg_5246" s="T149">PKZ_196X_SU0221.042 (042)</ta>
            <ta e="T155" id="Seg_5247" s="T154">PKZ_196X_SU0221.043 (043)</ta>
            <ta e="T158" id="Seg_5248" s="T155">PKZ_196X_SU0221.044 (044)</ta>
            <ta e="T160" id="Seg_5249" s="T158">PKZ_196X_SU0221.045 (045)</ta>
            <ta e="T161" id="Seg_5250" s="T160">PKZ_196X_SU0221.046 (046)</ta>
            <ta e="T167" id="Seg_5251" s="T161">PKZ_196X_SU0221.047 (047)</ta>
            <ta e="T168" id="Seg_5252" s="T167">PKZ_196X_SU0221.048 (048)</ta>
            <ta e="T173" id="Seg_5253" s="T168">PKZ_196X_SU0221.049 (049)</ta>
            <ta e="T174" id="Seg_5254" s="T173">PKZ_196X_SU0221.050 (050)</ta>
            <ta e="T176" id="Seg_5255" s="T174">PKZ_196X_SU0221.051 (051)</ta>
            <ta e="T180" id="Seg_5256" s="T176">PKZ_196X_SU0221.052 (052)</ta>
            <ta e="T183" id="Seg_5257" s="T180">PKZ_196X_SU0221.053 (053)</ta>
            <ta e="T184" id="Seg_5258" s="T183">PKZ_196X_SU0221.054 (054)</ta>
            <ta e="T189" id="Seg_5259" s="T184">PKZ_196X_SU0221.055 (055)</ta>
            <ta e="T190" id="Seg_5260" s="T189">PKZ_196X_SU0221.056 (056)</ta>
            <ta e="T200" id="Seg_5261" s="T190">PKZ_196X_SU0221.057 (057)</ta>
            <ta e="T201" id="Seg_5262" s="T200">PKZ_196X_SU0221.058 (058)</ta>
            <ta e="T206" id="Seg_5263" s="T201">PKZ_196X_SU0221.059 (059)</ta>
            <ta e="T207" id="Seg_5264" s="T206">PKZ_196X_SU0221.060 (060)</ta>
            <ta e="T210" id="Seg_5265" s="T207">PKZ_196X_SU0221.061 (061)</ta>
            <ta e="T212" id="Seg_5266" s="T210">PKZ_196X_SU0221.062 (062)</ta>
            <ta e="T218" id="Seg_5267" s="T212">PKZ_196X_SU0221.063 (063)</ta>
            <ta e="T220" id="Seg_5268" s="T218">PKZ_196X_SU0221.064 (064)</ta>
            <ta e="T221" id="Seg_5269" s="T220">PKZ_196X_SU0221.065 (065)</ta>
            <ta e="T226" id="Seg_5270" s="T221">PKZ_196X_SU0221.066 (066)</ta>
            <ta e="T227" id="Seg_5271" s="T226">PKZ_196X_SU0221.067 (067)</ta>
            <ta e="T235" id="Seg_5272" s="T227">PKZ_196X_SU0221.068 (068)</ta>
            <ta e="T236" id="Seg_5273" s="T235">PKZ_196X_SU0221.069 (069)</ta>
            <ta e="T245" id="Seg_5274" s="T236">PKZ_196X_SU0221.070 (070)</ta>
            <ta e="T246" id="Seg_5275" s="T245">PKZ_196X_SU0221.071 (071)</ta>
            <ta e="T255" id="Seg_5276" s="T246">PKZ_196X_SU0221.072 (072)</ta>
            <ta e="T256" id="Seg_5277" s="T255">PKZ_196X_SU0221.073 (073)</ta>
            <ta e="T264" id="Seg_5278" s="T256">PKZ_196X_SU0221.074 (074)</ta>
            <ta e="T270" id="Seg_5279" s="T264">PKZ_196X_SU0221.075 (075)</ta>
            <ta e="T273" id="Seg_5280" s="T270">PKZ_196X_SU0221.076 (076)</ta>
            <ta e="T277" id="Seg_5281" s="T273">PKZ_196X_SU0221.077 (077)</ta>
            <ta e="T278" id="Seg_5282" s="T277">PKZ_196X_SU0221.078 (078)</ta>
            <ta e="T283" id="Seg_5283" s="T278">PKZ_196X_SU0221.079 (079)</ta>
            <ta e="T284" id="Seg_5284" s="T283">PKZ_196X_SU0221.080 (080)</ta>
            <ta e="T286" id="Seg_5285" s="T284">PKZ_196X_SU0221.081 (081)</ta>
            <ta e="T287" id="Seg_5286" s="T286">PKZ_196X_SU0221.082 (082)</ta>
            <ta e="T291" id="Seg_5287" s="T287">PKZ_196X_SU0221.083 (083)</ta>
            <ta e="T294" id="Seg_5288" s="T291">PKZ_196X_SU0221.084 (084)</ta>
            <ta e="T299" id="Seg_5289" s="T294">PKZ_196X_SU0221.085 (085)</ta>
            <ta e="T307" id="Seg_5290" s="T299">PKZ_196X_SU0221.086 (086)</ta>
            <ta e="T313" id="Seg_5291" s="T307">PKZ_196X_SU0221.087 (087)</ta>
            <ta e="T316" id="Seg_5292" s="T313">PKZ_196X_SU0221.088 (088)</ta>
            <ta e="T322" id="Seg_5293" s="T316">PKZ_196X_SU0221.089 (089)</ta>
            <ta e="T334" id="Seg_5294" s="T322">PKZ_196X_SU0221.090 (090) </ta>
            <ta e="T339" id="Seg_5295" s="T334">PKZ_196X_SU0221.091 (092)</ta>
            <ta e="T347" id="Seg_5296" s="T339">PKZ_196X_SU0221.092 (093)</ta>
            <ta e="T348" id="Seg_5297" s="T347">PKZ_196X_SU0221.093 (094)</ta>
            <ta e="T353" id="Seg_5298" s="T348">PKZ_196X_SU0221.094 (095)</ta>
            <ta e="T362" id="Seg_5299" s="T353">PKZ_196X_SU0221.095 (096)</ta>
            <ta e="T366" id="Seg_5300" s="T362">PKZ_196X_SU0221.096 (097)</ta>
            <ta e="T369" id="Seg_5301" s="T366">PKZ_196X_SU0221.097 (098)</ta>
            <ta e="T376" id="Seg_5302" s="T369">PKZ_196X_SU0221.098 (099)</ta>
            <ta e="T380" id="Seg_5303" s="T376">PKZ_196X_SU0221.099 (100)</ta>
            <ta e="T381" id="Seg_5304" s="T380">PKZ_196X_SU0221.100 (101)</ta>
            <ta e="T385" id="Seg_5305" s="T381">PKZ_196X_SU0221.101 (102)</ta>
            <ta e="T393" id="Seg_5306" s="T385">PKZ_196X_SU0221.102 (103)</ta>
            <ta e="T395" id="Seg_5307" s="T393">PKZ_196X_SU0221.103 (104)</ta>
            <ta e="T401" id="Seg_5308" s="T395">PKZ_196X_SU0221.104 (105)</ta>
            <ta e="T403" id="Seg_5309" s="T401">PKZ_196X_SU0221.105 (106)</ta>
            <ta e="T407" id="Seg_5310" s="T403">PKZ_196X_SU0221.106 (107)</ta>
            <ta e="T422" id="Seg_5311" s="T407">PKZ_196X_SU0221.107 (108)</ta>
            <ta e="T424" id="Seg_5312" s="T422">PKZ_196X_SU0221.108 (109)</ta>
            <ta e="T425" id="Seg_5313" s="T424">PKZ_196X_SU0221.109 (110)</ta>
            <ta e="T439" id="Seg_5314" s="T425">PKZ_196X_SU0221.110 (111)</ta>
            <ta e="T442" id="Seg_5315" s="T439">PKZ_196X_SU0221.111 (112)</ta>
            <ta e="T450" id="Seg_5316" s="T442">PKZ_196X_SU0221.112 (113)</ta>
            <ta e="T455" id="Seg_5317" s="T450">PKZ_196X_SU0221.113 (114)</ta>
            <ta e="T456" id="Seg_5318" s="T455">PKZ_196X_SU0221.114 (115)</ta>
            <ta e="T465" id="Seg_5319" s="T456">PKZ_196X_SU0221.115 (116)</ta>
            <ta e="T476" id="Seg_5320" s="T465">PKZ_196X_SU0221.116 (117)</ta>
            <ta e="T477" id="Seg_5321" s="T476">PKZ_196X_SU0221.117 (118)</ta>
            <ta e="T484" id="Seg_5322" s="T477">PKZ_196X_SU0221.118 (119)</ta>
            <ta e="T495" id="Seg_5323" s="T484">PKZ_196X_SU0221.119 (120)</ta>
            <ta e="T497" id="Seg_5324" s="T495">PKZ_196X_SU0221.120 (121)</ta>
            <ta e="T503" id="Seg_5325" s="T497">PKZ_196X_SU0221.121 (122)</ta>
            <ta e="T507" id="Seg_5326" s="T503">PKZ_196X_SU0221.122 (123)</ta>
            <ta e="T509" id="Seg_5327" s="T507">PKZ_196X_SU0221.123 (124)</ta>
            <ta e="T510" id="Seg_5328" s="T509">PKZ_196X_SU0221.124 (125)</ta>
            <ta e="T520" id="Seg_5329" s="T510">PKZ_196X_SU0221.125 (126)</ta>
            <ta e="T525" id="Seg_5330" s="T520">PKZ_196X_SU0221.126 (127)</ta>
            <ta e="T531" id="Seg_5331" s="T525">PKZ_196X_SU0221.127 (128)</ta>
            <ta e="T541" id="Seg_5332" s="T531">PKZ_196X_SU0221.128 (129)</ta>
            <ta e="T542" id="Seg_5333" s="T541">PKZ_196X_SU0221.129 (130)</ta>
            <ta e="T548" id="Seg_5334" s="T542">PKZ_196X_SU0221.130 (131)</ta>
            <ta e="T549" id="Seg_5335" s="T548">PKZ_196X_SU0221.131 (132)</ta>
            <ta e="T550" id="Seg_5336" s="T549">PKZ_196X_SU0221.132 (133)</ta>
            <ta e="T556" id="Seg_5337" s="T550">PKZ_196X_SU0221.133 (134)</ta>
            <ta e="T562" id="Seg_5338" s="T556">PKZ_196X_SU0221.134 (135)</ta>
            <ta e="T563" id="Seg_5339" s="T562">PKZ_196X_SU0221.135 (136)</ta>
            <ta e="T568" id="Seg_5340" s="T563">PKZ_196X_SU0221.136 (137)</ta>
            <ta e="T574" id="Seg_5341" s="T568">PKZ_196X_SU0221.137 (138)</ta>
            <ta e="T578" id="Seg_5342" s="T574">PKZ_196X_SU0221.138 (139)</ta>
            <ta e="T583" id="Seg_5343" s="T578">PKZ_196X_SU0221.139 (140)</ta>
            <ta e="T587" id="Seg_5344" s="T583">PKZ_196X_SU0221.140 (141)</ta>
            <ta e="T588" id="Seg_5345" s="T587">PKZ_196X_SU0221.141 (142)</ta>
            <ta e="T594" id="Seg_5346" s="T588">PKZ_196X_SU0221.142 (143)</ta>
            <ta e="T597" id="Seg_5347" s="T594">PKZ_196X_SU0221.143 (144)</ta>
            <ta e="T600" id="Seg_5348" s="T597">PKZ_196X_SU0221.144 (145)</ta>
            <ta e="T603" id="Seg_5349" s="T600">PKZ_196X_SU0221.145 (146)</ta>
            <ta e="T607" id="Seg_5350" s="T603">PKZ_196X_SU0221.146 (147)</ta>
            <ta e="T608" id="Seg_5351" s="T607">PKZ_196X_SU0221.147 (148)</ta>
            <ta e="T619" id="Seg_5352" s="T608">PKZ_196X_SU0221.148 (149)</ta>
            <ta e="T633" id="Seg_5353" s="T619">PKZ_196X_SU0221.149 (150)</ta>
            <ta e="T635" id="Seg_5354" s="T633">PKZ_196X_SU0221.150 (151)</ta>
            <ta e="T637" id="Seg_5355" s="T635">PKZ_196X_SU0221.151 (152)</ta>
            <ta e="T640" id="Seg_5356" s="T637">PKZ_196X_SU0221.152 (153)</ta>
            <ta e="T647" id="Seg_5357" s="T640">PKZ_196X_SU0221.153 (154)</ta>
            <ta e="T656" id="Seg_5358" s="T647">PKZ_196X_SU0221.154 (155)</ta>
            <ta e="T657" id="Seg_5359" s="T656">PKZ_196X_SU0221.155 (156)</ta>
            <ta e="T664" id="Seg_5360" s="T657">PKZ_196X_SU0221.156 (157)</ta>
            <ta e="T671" id="Seg_5361" s="T664">PKZ_196X_SU0221.157 (158)</ta>
            <ta e="T672" id="Seg_5362" s="T671">PKZ_196X_SU0221.158 (159)</ta>
            <ta e="T677" id="Seg_5363" s="T672">PKZ_196X_SU0221.159 (160)</ta>
            <ta e="T681" id="Seg_5364" s="T677">PKZ_196X_SU0221.160 (161)</ta>
            <ta e="T687" id="Seg_5365" s="T681">PKZ_196X_SU0221.161 (162)</ta>
            <ta e="T697" id="Seg_5366" s="T687">PKZ_196X_SU0221.162 (163)</ta>
            <ta e="T698" id="Seg_5367" s="T697">PKZ_196X_SU0221.163 (164)</ta>
            <ta e="T707" id="Seg_5368" s="T698">PKZ_196X_SU0221.164 (165)</ta>
            <ta e="T708" id="Seg_5369" s="T707">PKZ_196X_SU0221.165 (166)</ta>
            <ta e="T717" id="Seg_5370" s="T708">PKZ_196X_SU0221.166 (167)</ta>
            <ta e="T718" id="Seg_5371" s="T717">PKZ_196X_SU0221.167 (168)</ta>
            <ta e="T720" id="Seg_5372" s="T718">PKZ_196X_SU0221.168 (169)</ta>
            <ta e="T721" id="Seg_5373" s="T720">PKZ_196X_SU0221.169 (170)</ta>
            <ta e="T727" id="Seg_5374" s="T721">PKZ_196X_SU0221.170 (171)</ta>
            <ta e="T730" id="Seg_5375" s="T727">PKZ_196X_SU0221.171 (172)</ta>
            <ta e="T731" id="Seg_5376" s="T730">PKZ_196X_SU0221.172 (173)</ta>
            <ta e="T738" id="Seg_5377" s="T731">PKZ_196X_SU0221.173 (174)</ta>
            <ta e="T739" id="Seg_5378" s="T738">PKZ_196X_SU0221.174 (175)</ta>
            <ta e="T748" id="Seg_5379" s="T739">PKZ_196X_SU0221.175 (176)</ta>
            <ta e="T757" id="Seg_5380" s="T748">PKZ_196X_SU0221.176 (177)</ta>
            <ta e="T758" id="Seg_5381" s="T757">PKZ_196X_SU0221.177 (178)</ta>
            <ta e="T770" id="Seg_5382" s="T758">PKZ_196X_SU0221.178 (179)</ta>
            <ta e="T771" id="Seg_5383" s="T770">PKZ_196X_SU0221.179 (180)</ta>
            <ta e="T781" id="Seg_5384" s="T771">PKZ_196X_SU0221.180 (181)</ta>
            <ta e="T785" id="Seg_5385" s="T781">PKZ_196X_SU0221.181 (182)</ta>
            <ta e="T791" id="Seg_5386" s="T785">PKZ_196X_SU0221.182 (183)</ta>
            <ta e="T792" id="Seg_5387" s="T791">PKZ_196X_SU0221.183 (184)</ta>
            <ta e="T798" id="Seg_5388" s="T792">PKZ_196X_SU0221.184 (185)</ta>
            <ta e="T809" id="Seg_5389" s="T798">PKZ_196X_SU0221.185 (186)</ta>
            <ta e="T813" id="Seg_5390" s="T809">PKZ_196X_SU0221.186 (187)</ta>
            <ta e="T816" id="Seg_5391" s="T813">PKZ_196X_SU0221.187 (188)</ta>
            <ta e="T817" id="Seg_5392" s="T816">PKZ_196X_SU0221.188 (189)</ta>
            <ta e="T820" id="Seg_5393" s="T817">PKZ_196X_SU0221.189 (190)</ta>
            <ta e="T821" id="Seg_5394" s="T820">PKZ_196X_SU0221.190 (191)</ta>
            <ta e="T835" id="Seg_5395" s="T821">PKZ_196X_SU0221.191 (192)</ta>
            <ta e="T836" id="Seg_5396" s="T835">PKZ_196X_SU0221.192 (193)</ta>
            <ta e="T843" id="Seg_5397" s="T836">PKZ_196X_SU0221.193 (194)</ta>
            <ta e="T846" id="Seg_5398" s="T843">PKZ_196X_SU0221.194 (195)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_5399" s="T0">Kamen miʔnʼibeʔ šobial? </ta>
            <ta e="T5" id="Seg_5400" s="T3">Urgo dʼalagən. </ta>
            <ta e="T9" id="Seg_5401" s="T5">Šide dʼala Kazan turagən amnobiam. </ta>
            <ta e="T13" id="Seg_5402" s="T9">(Ĭmbi= ĭmbi=) Ĭmbi kubial? </ta>
            <ta e="T16" id="Seg_5403" s="T13">Dĭgəttə döber šobiam. </ta>
            <ta e="T17" id="Seg_5404" s="T16">((BRK)). </ta>
            <ta e="T26" id="Seg_5405" s="T17">Măndərbiam, măndərbiam, kădedə (ej moliam) ej moliam šozittə döber. </ta>
            <ta e="T31" id="Seg_5406" s="T26">Nagur dʼalagən dĭgəttə šobiam döber. </ta>
            <ta e="T32" id="Seg_5407" s="T31">((BRK)). </ta>
            <ta e="T37" id="Seg_5408" s="T32">Dĭgəttə döber šobiam, urgajanə šobiam. </ta>
            <ta e="T43" id="Seg_5409" s="T37">Dĭ măna bar bădəbi, mĭj mĭbi. </ta>
            <ta e="T44" id="Seg_5410" s="T43">((BRK)). </ta>
            <ta e="T49" id="Seg_5411" s="T44">Dĭgəttə döber šobiam, buzo kirgarlaʔbə. </ta>
            <ta e="T58" id="Seg_5412" s="T49">Nüke bar mĭbi dĭʔnə toltanoʔ, dĭ amorlaʔbə, ej kirgarlia. </ta>
            <ta e="T59" id="Seg_5413" s="T58">((BRK)). </ta>
            <ta e="T65" id="Seg_5414" s="T59">Dĭ nükegən turagən šišəge, pʼeš nendəbi. </ta>
            <ta e="T66" id="Seg_5415" s="T65">((BRK)). </ta>
            <ta e="T70" id="Seg_5416" s="T66">Nʼiʔnen bar šišəge, ugandə. </ta>
            <ta e="T74" id="Seg_5417" s="T70">Büzo turagən nuga bar. </ta>
            <ta e="T75" id="Seg_5418" s="T74">((BRK)). </ta>
            <ta e="T81" id="Seg_5419" s="T75">(Tüžöj) Tüžöj nʼeluʔpi bar, üdʼüge büzo. </ta>
            <ta e="T82" id="Seg_5420" s="T81">((BRK)). </ta>
            <ta e="T88" id="Seg_5421" s="T82">Miʔ turagən ugandə iʔgö il kübiʔi. </ta>
            <ta e="T89" id="Seg_5422" s="T88">((BRK)). </ta>
            <ta e="T95" id="Seg_5423" s="T89">Mĭmbiem nükenə, dĭ (uga-) ugandə ĭzemnie. </ta>
            <ta e="T99" id="Seg_5424" s="T95">"Măn teinen (kulambi-) külalləm". </ta>
            <ta e="T101" id="Seg_5425" s="T99">"No, küʔ" ((NOISE)). </ta>
            <ta e="T103" id="Seg_5426" s="T101">"No, küʔ. </ta>
            <ta e="T109" id="Seg_5427" s="T103">(Taʔ-) Karəldʼan (šo-) šolam (băzəsʼtə) tănan. </ta>
            <ta e="T112" id="Seg_5428" s="T109">Dĭgəttə ej kambiam. </ta>
            <ta e="T116" id="Seg_5429" s="T112">Noʔ deʔpiʔi šide tibi. </ta>
            <ta e="T122" id="Seg_5430" s="T116">Dĭgəttə šobiam:" Ĭmbi konʼdʼo ej šobial?" </ta>
            <ta e="T129" id="Seg_5431" s="T122">"A măn tenəbiem tăn (külambiam), băzəsʼtə šobiam". </ta>
            <ta e="T130" id="Seg_5432" s="T129">((BRK)). </ta>
            <ta e="T134" id="Seg_5433" s="T130">Büzo nomlaʔbə, ej kirgarlaʔbə. </ta>
            <ta e="T135" id="Seg_5434" s="T134">((BRK)). </ta>
            <ta e="T142" id="Seg_5435" s="T135">Esseŋ bar nuʔməleʔbəʔjə (turan to-) turan tondə. </ta>
            <ta e="T143" id="Seg_5436" s="T142">((BRK)). </ta>
            <ta e="T147" id="Seg_5437" s="T143">Šide nʼi šobiʔi nʼilgösʼtə. </ta>
            <ta e="T148" id="Seg_5438" s="T147">((BRK)). </ta>
            <ta e="T149" id="Seg_5439" s="T148">((BRK)). </ta>
            <ta e="T154" id="Seg_5440" s="T149">Iʔ (kirgargaʔ), iʔ alomgaʔ, nʼilgöleʔ. </ta>
            <ta e="T155" id="Seg_5441" s="T154">((BRK)). </ta>
            <ta e="T158" id="Seg_5442" s="T155">"Ial kamen šoləj?" </ta>
            <ta e="T160" id="Seg_5443" s="T158">"Büžü šoləj!" </ta>
            <ta e="T161" id="Seg_5444" s="T160">((BRK)). </ta>
            <ta e="T167" id="Seg_5445" s="T161">Nada bü ejümzittə, dĭgəttə tüžöj bĭdəlzittə. </ta>
            <ta e="T168" id="Seg_5446" s="T167">((BRK)). </ta>
            <ta e="T173" id="Seg_5447" s="T168">Ĭmbi nʼilgösʼtə šobilaʔ, măndərzittə šobilaʔ? </ta>
            <ta e="T174" id="Seg_5448" s="T173">((BRK)). </ta>
            <ta e="T176" id="Seg_5449" s="T174">Ej kürümnieʔi. </ta>
            <ta e="T180" id="Seg_5450" s="T176">Kuvas nʼizeŋ, ej kürümnieʔi. </ta>
            <ta e="T183" id="Seg_5451" s="T180">Nulaʔbəʔjə, jakšeʔi ugandə. </ta>
            <ta e="T184" id="Seg_5452" s="T183">((BRK)). </ta>
            <ta e="T189" id="Seg_5453" s="T184">Sanə teinen mĭbiem tănan lejzittə. </ta>
            <ta e="T190" id="Seg_5454" s="T189">((BRK)). </ta>
            <ta e="T200" id="Seg_5455" s="T190">Sʼimat măndərzittə, (kuʔi-) kuzaŋdə nʼilgösʼtə, aŋdə amzittə, tĭmeʔi talbəzittə ipek. </ta>
            <ta e="T201" id="Seg_5456" s="T200">((BRK)). </ta>
            <ta e="T206" id="Seg_5457" s="T201">Tenöbiam, tenöbiam, kăde döber šozittə. </ta>
            <ta e="T207" id="Seg_5458" s="T206">((BRK)). </ta>
            <ta e="T210" id="Seg_5459" s="T207">Kuba dʼügən iʔbolaʔpi. </ta>
            <ta e="T212" id="Seg_5460" s="T210">Ipek embiʔi. </ta>
            <ta e="T218" id="Seg_5461" s="T212">Dĭgəttə amorbiʔi dĭ ipek, i uja. </ta>
            <ta e="T220" id="Seg_5462" s="T218">И мясо. </ta>
            <ta e="T221" id="Seg_5463" s="T220">((BRK)). </ta>
            <ta e="T226" id="Seg_5464" s="T221">Dĭgəttə ambiʔi, dĭgəttə kudajdə numan üzəbiʔi. </ta>
            <ta e="T227" id="Seg_5465" s="T226">((BRK)). </ta>
            <ta e="T235" id="Seg_5466" s="T227">Kazan turanə mĭmbiem, (tʼegergən=) tʼegergən (ku-) kudaj (numan=) numan üzəbiem. </ta>
            <ta e="T236" id="Seg_5467" s="T235">((BRK)). </ta>
            <ta e="T245" id="Seg_5468" s="T236">Iʔgö il külambiʔi, (kros-) krospaʔi ugandə iʔgö nugaʔi bar. </ta>
            <ta e="T246" id="Seg_5469" s="T245">((BRK)). </ta>
            <ta e="T255" id="Seg_5470" s="T246">Miʔ koʔbdo ĭzembi, a abat (k-) kumbi (dʼizi-) dʼazirzittə. </ta>
            <ta e="T256" id="Seg_5471" s="T255">((BRK)). </ta>
            <ta e="T264" id="Seg_5472" s="T256">(Miʔ-) Miʔnʼibeʔ teinen nüdʼin tibi nezi bar dʼabrobiʔi. </ta>
            <ta e="T270" id="Seg_5473" s="T264">Udagən bar (krom- mʼa-) kem mʼaŋnaʔbə. </ta>
            <ta e="T273" id="Seg_5474" s="T270">Esseŋdə bar kirgarlaʔbəʔjə. </ta>
            <ta e="T277" id="Seg_5475" s="T273">Măn šobiam, bar kudonzəbiam. </ta>
            <ta e="T278" id="Seg_5476" s="T277">((BRK)). </ta>
            <ta e="T283" id="Seg_5477" s="T278">Büzo iʔbəbi, ej kirgaria tüj. </ta>
            <ta e="T284" id="Seg_5478" s="T283">((BRK)). </ta>
            <ta e="T286" id="Seg_5479" s="T284">Možna dʼăbaktərzittə. </ta>
            <ta e="T287" id="Seg_5480" s="T286">((BRK)). </ta>
            <ta e="T291" id="Seg_5481" s="T287">Büzʼen bar püjet tʼukumbi. </ta>
            <ta e="T294" id="Seg_5482" s="T291">Gijendə ara bĭtlem. </ta>
            <ta e="T299" id="Seg_5483" s="T294">Dĭgəttə pograšsʼe abi, amnobi pagən. </ta>
            <ta e="T307" id="Seg_5484" s="T299">A măn urgajam mĭmbi Kazan turanə i ara deʔpi. </ta>
            <ta e="T313" id="Seg_5485" s="T307">(Dĭgəttə) amnəbi tüʔsʼittə (dĭʔ-) diʔnə kötenzi. </ta>
            <ta e="T316" id="Seg_5486" s="T313">Dĭgəttə dĭ šobi. </ta>
            <ta e="T322" id="Seg_5487" s="T316">Dĭ dĭʔnə ara mĭbi, dĭ bĭʔpi. </ta>
            <ta e="T334" id="Seg_5488" s="T322">I (măndə=) măndə dĭʔnə: "Ne dak ne, ot dak ne dak ne". </ta>
            <ta e="T339" id="Seg_5489" s="T334">"Ĭmbi tăn dăre (m-) ((BRK))?" </ta>
            <ta e="T347" id="Seg_5490" s="T339">"A tăn măna (am-) kötenzi amnobial i tüʔpiel. </ta>
            <ta e="T348" id="Seg_5491" s="T347">((BRK)). </ta>
            <ta e="T353" id="Seg_5492" s="T348">(Măr-) Măn urgajam šamandə mĭmbiem. </ta>
            <ta e="T362" id="Seg_5493" s="T353">Dĭn bar (moi-) uja mĭnzerbiʔi, takšenə, urgo takše embiʔi. </ta>
            <ta e="T366" id="Seg_5494" s="T362">Dĭ bar šĭšəge molambi. </ta>
            <ta e="T369" id="Seg_5495" s="T366">"Amnaʔ (a-) amorzittə!" </ta>
            <ta e="T376" id="Seg_5496" s="T369">Dĭ bar uja ibi, dĭ ugandə šišəge. </ta>
            <ta e="T380" id="Seg_5497" s="T376">Dĭgəttə mendə (baʔ-) baʔluʔpi. </ta>
            <ta e="T381" id="Seg_5498" s="T380">((BRK)). </ta>
            <ta e="T385" id="Seg_5499" s="T381">Bubenzi bar (š-) sʼarbi. </ta>
            <ta e="T393" id="Seg_5500" s="T385">Bar ildə (mĭmbi), a măn urgajanə ej mĭbiem. </ta>
            <ta e="T395" id="Seg_5501" s="T393">Dĭgəttə mĭmbi. </ta>
            <ta e="T401" id="Seg_5502" s="T395">Iʔ deʔkeʔ dĭ ne, kazak ne. </ta>
            <ta e="T403" id="Seg_5503" s="T401">Забыла ((BRK)). </ta>
            <ta e="T407" id="Seg_5504" s="T403">(Šamangən) bar părga ibi. </ta>
            <ta e="T422" id="Seg_5505" s="T407">Всякий-всякий был bar (kuriz-) kurizən kuba, albugan kuba, părgagən, tažəbən (p-) kuba părgagən. </ta>
            <ta e="T424" id="Seg_5506" s="T422">Poʔton kuba. </ta>
            <ta e="T425" id="Seg_5507" s="T424">((BRK)). </ta>
            <ta e="T439" id="Seg_5508" s="T425">Dĭ šaman bar шаманил - шаманил, dĭgəttə dagaj ibi da bar müʔbdəbi (siʔ-) sĭjdə. </ta>
            <ta e="T442" id="Seg_5509" s="T439">I dĭgəttə saʔməluʔpi. </ta>
            <ta e="T450" id="Seg_5510" s="T442">A il iʔgö šonəgaʔi быдто (s -) Минусинские. </ta>
            <ta e="T455" id="Seg_5511" s="T450">По-русски сказала … </ta>
            <ta e="T456" id="Seg_5512" s="T455">((BRK)). </ta>
            <ta e="T465" id="Seg_5513" s="T456">Tibiʔi (ka- ka-) kamen kallaʔi dʼijenə dĭgəttə šamandə kallaʔi. </ta>
            <ta e="T476" id="Seg_5514" s="T465">Dĭ mălləj:" Tăn (u-) albuga kutlal", a šindinə mălləj:" Ej kutlal". </ta>
            <ta e="T477" id="Seg_5515" s="T476">((BRK)). </ta>
            <ta e="T484" id="Seg_5516" s="T477">Минусинский šaman i miʔ šaman bar dʼabrobiʔi. </ta>
            <ta e="T495" id="Seg_5517" s="T484">(Minu-) Минусинский kambi maʔndə, a miʔ šaman (dĭ-) dĭm bar bĭdleʔpi. </ta>
            <ta e="T497" id="Seg_5518" s="T495">Pograš molambi. </ta>
            <ta e="T503" id="Seg_5519" s="T497">A dĭ bar multuksi dĭm kutlambi. </ta>
            <ta e="T507" id="Seg_5520" s="T503">(Udanə) Udabə bar băldəbi. </ta>
            <ta e="T509" id="Seg_5521" s="T507">Руку сломал. </ta>
            <ta e="T510" id="Seg_5522" s="T509">((BRK)). </ta>
            <ta e="T520" id="Seg_5523" s="T510">Girgit kuza ĭzemnuʔpi, dĭgəttə kalaʔi šamandə, dĭ bar dĭm dʼaziria. </ta>
            <ta e="T525" id="Seg_5524" s="T520">Lʼevăj udandə (nuldləj) (dĭ üge). </ta>
            <ta e="T531" id="Seg_5525" s="T525">Dăre bar ulutsi (numa-) кланяется, кланяется. </ta>
            <ta e="T541" id="Seg_5526" s="T531">Dĭgəttə dĭ dĭʔnə tʼažerluʔpi i bostə bar suʔmileʔbə, bubendə sʼarlaʔbə. </ta>
            <ta e="T542" id="Seg_5527" s="T541">((BRK)). </ta>
            <ta e="T548" id="Seg_5528" s="T542">Dĭgəttə dĭ ej ĭzemnie, maʔndə šoləj. </ta>
            <ta e="T549" id="Seg_5529" s="T548">((BRK)). </ta>
            <ta e="T550" id="Seg_5530" s="T549">((BRK)). </ta>
            <ta e="T556" id="Seg_5531" s="T550">Tibizeŋ dʼijenə kalla dʼürbiʔi, nezeŋ (maʔən) amnobiʔi. </ta>
            <ta e="T562" id="Seg_5532" s="T556">Sarankaʔi tĭlbiʔi, ujam (embiʔi), ipek naga. </ta>
            <ta e="T563" id="Seg_5533" s="T562">((BRK)). </ta>
            <ta e="T568" id="Seg_5534" s="T563">Nezeŋ bar (n-) noʔ nĭŋgəbiʔi. </ta>
            <ta e="T574" id="Seg_5535" s="T568">Travʼanʼigaʔi kürbiʔi, jamaʔi šöʔpiʔi, žilaʔi kürbiʔi. </ta>
            <ta e="T578" id="Seg_5536" s="T574">Dʼijegən maʔ (ab-) abiʔi. </ta>
            <ta e="T583" id="Seg_5537" s="T578">Dĭgəttə dĭ maʔkən uja mĭnzerbiʔi. </ta>
            <ta e="T587" id="Seg_5538" s="T583">Segi bü mĭnzerbiʔi, amorbiʔi. </ta>
            <ta e="T588" id="Seg_5539" s="T587">((BRK)). </ta>
            <ta e="T594" id="Seg_5540" s="T588">Turaʔi ibiʔi, dĭ turagən ej (amnobiʔi). </ta>
            <ta e="T597" id="Seg_5541" s="T594">Nʼiʔnen maʔ ibi. </ta>
            <ta e="T600" id="Seg_5542" s="T597">Dĭn mĭnzerbiʔi mĭj. </ta>
            <ta e="T603" id="Seg_5543" s="T600">Segi bü mĭnzerbiʔi. </ta>
            <ta e="T607" id="Seg_5544" s="T603">Amorbiʔi, a turagən kunolbiʔi. </ta>
            <ta e="T608" id="Seg_5545" s="T607">((BRK)). </ta>
            <ta e="T619" id="Seg_5546" s="T608">Maʔi abiʔi (pats-) paʔi, dĭgəttə kuba kürbiʔi patsi da enbiʔi diʔnə. </ta>
            <ta e="T633" id="Seg_5547" s="T619">Dĭgəttə taganəʔi abiʔi, dĭgəttə aspaʔi edəbiʔi, (mĭnzer- mĭnzeri-) mĭnzerzittə uja da, segi bü mĭnzerzittə. </ta>
            <ta e="T635" id="Seg_5548" s="T633">Dĭgəttə … </ta>
            <ta e="T637" id="Seg_5549" s="T635">Стой ((BRK)). </ta>
            <ta e="T640" id="Seg_5550" s="T637">Dʼügən šü ambiʔi. </ta>
            <ta e="T647" id="Seg_5551" s="T640">Nʼüʔnən ši ibi dĭbər, (be-) ber kandlaʔbə. </ta>
            <ta e="T656" id="Seg_5552" s="T647">Dĭgəttə paʔi ambiʔi, štobɨ šünə šindidə ej (š-) saʔməbi. </ta>
            <ta e="T657" id="Seg_5553" s="T656">((BRK)). </ta>
            <ta e="T664" id="Seg_5554" s="T657">Šišəge mobi dak, (kožaʔi=) kožazi kajbiʔi (a=). </ta>
            <ta e="T671" id="Seg_5555" s="T664">Ejü mobi dak, (ku- kürbiʔi= kambiʔ-) kajbiʔi. </ta>
            <ta e="T672" id="Seg_5556" s="T671">((BRK)). </ta>
            <ta e="T677" id="Seg_5557" s="T672">Urgo ibi, (me-) метра teʔtə. </ta>
            <ta e="T681" id="Seg_5558" s="T677">Il iʔbobiʔi, bʼeʔ il. </ta>
            <ta e="T687" id="Seg_5559" s="T681">Kamen sĭre ibi, dĭzeŋ dön amnobiʔi. </ta>
            <ta e="T697" id="Seg_5560" s="T687">A kamen sĭre kalla dʼürbi, dĭgəttə dĭzeŋ kandəgaʔi dʼijenə, urgo măjanə. </ta>
            <ta e="T698" id="Seg_5561" s="T697">((BRK)). </ta>
            <ta e="T707" id="Seg_5562" s="T698">Ălenʼəʔizi mĭmbiʔi, Tʼelamdə kanbiʔi, dĭn urgo bü, kola dʼaʔpiʔi. </ta>
            <ta e="T708" id="Seg_5563" s="T707">((BRK)). </ta>
            <ta e="T717" id="Seg_5564" s="T708">Dĭgəttə dĭzeŋ kambiʔi, esseŋdə, i nezeŋdə ibiʔi, ălenʼəʔizi kambiʔi. </ta>
            <ta e="T718" id="Seg_5565" s="T717">((BRK)). </ta>
            <ta e="T720" id="Seg_5566" s="T718">I menzeŋzi. </ta>
            <ta e="T721" id="Seg_5567" s="T720">((BRK)). </ta>
            <ta e="T727" id="Seg_5568" s="T721">Dĭgəttə dĭzeŋ dʼijegən keʔbde oʔbdəbiʔi, ambiʔi. </ta>
            <ta e="T730" id="Seg_5569" s="T727">San oʔbdəbiʔi lejbiʔi. </ta>
            <ta e="T731" id="Seg_5570" s="T730">((BRK)). </ta>
            <ta e="T738" id="Seg_5571" s="T731">Dĭgəttə dĭzeŋ beškeʔi oʔbdəbiʔi, mĭnzerbiʔi i tustʼarbiʔi. </ta>
            <ta e="T739" id="Seg_5572" s="T738">((BRK)). </ta>
            <ta e="T748" id="Seg_5573" s="T739">Dĭgəttə (dĭn=) dĭzen dĭn kuʔpiʔi sɨneʔi i uja tustʼarbiʔi. </ta>
            <ta e="T757" id="Seg_5574" s="T748">Dĭgəttə edəbiʔi, dĭ (ko-) kolambi, baltuzi jaʔpiʔi, buranə kămnəbiʔi. </ta>
            <ta e="T758" id="Seg_5575" s="T757">((BRK)). </ta>
            <ta e="T770" id="Seg_5576" s="T758">Dĭzeŋ dĭgəttə kola dʼaʔpiʔi i tustʼarbiʔi, i tože kola (koʔpiʔi ) ((PAUSE)) сушили. </ta>
            <ta e="T771" id="Seg_5577" s="T770">((BRK)). </ta>
            <ta e="T781" id="Seg_5578" s="T771">Dĭgəttə kubatsi abiʔi buraʔi i dĭbər embiʔi uja i kola. </ta>
            <ta e="T785" id="Seg_5579" s="T781">I ălenʼəʔizi deʔpiʔi maʔnə. </ta>
            <ta e="T791" id="Seg_5580" s="T785">Döber gijen amnobiʔi, kamen šišəge ibi. </ta>
            <ta e="T792" id="Seg_5581" s="T791">((BRK)). </ta>
            <ta e="T798" id="Seg_5582" s="T792">Dĭzeŋ (dĭgət-) dĭrgidəʔi menzeŋdə (b-) ibiʔi. </ta>
            <ta e="T809" id="Seg_5583" s="T798">(Ka-) Kalaʔi poʔto, iʔgö dʼabəluʔi, dĭgəttə maʔndə šoləj, dĭ ine kolerləj. </ta>
            <ta e="T813" id="Seg_5584" s="T809">Dĭgəttə kandəga poʔto oʔbdəsʼtə. </ta>
            <ta e="T816" id="Seg_5585" s="T813">Dĭgəttə šoləj maʔndə. </ta>
            <ta e="T817" id="Seg_5586" s="T816">((BRK)). </ta>
            <ta e="T820" id="Seg_5587" s="T817">I poʔto detləj. </ta>
            <ta e="T821" id="Seg_5588" s="T820">((BRK)). </ta>
            <ta e="T835" id="Seg_5589" s="T821">(Dĭz-) Dĭzem (a-) armijanə ej ibiʔi, dĭzeŋ (ĭsak aktʼa=) aktʼa mĭbiʔi i albuga mĭbiʔi. </ta>
            <ta e="T836" id="Seg_5590" s="T835">((BRK)). </ta>
            <ta e="T843" id="Seg_5591" s="T836">Dĭzeŋ ĭsaktə mĭmbiʔi aktʼa, (bʼeʔ=) bʼeʔ sumna. </ta>
            <ta e="T846" id="Seg_5592" s="T843">Ну вот так. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5593" s="T0">kamen</ta>
            <ta e="T2" id="Seg_5594" s="T1">miʔnʼibeʔ</ta>
            <ta e="T3" id="Seg_5595" s="T2">šo-bia-l</ta>
            <ta e="T4" id="Seg_5596" s="T3">urgo</ta>
            <ta e="T5" id="Seg_5597" s="T4">dʼala-gən</ta>
            <ta e="T6" id="Seg_5598" s="T5">šide</ta>
            <ta e="T7" id="Seg_5599" s="T6">dʼala</ta>
            <ta e="T850" id="Seg_5600" s="T7">Kazan</ta>
            <ta e="T8" id="Seg_5601" s="T850">tura-gən</ta>
            <ta e="T9" id="Seg_5602" s="T8">amno-bia-m</ta>
            <ta e="T10" id="Seg_5603" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_5604" s="T10">ĭmbi</ta>
            <ta e="T12" id="Seg_5605" s="T11">ĭmbi</ta>
            <ta e="T13" id="Seg_5606" s="T12">ku-bia-l</ta>
            <ta e="T14" id="Seg_5607" s="T13">dĭgəttə</ta>
            <ta e="T15" id="Seg_5608" s="T14">döber</ta>
            <ta e="T16" id="Seg_5609" s="T15">šo-bia-m</ta>
            <ta e="T18" id="Seg_5610" s="T17">măndə-r-bia-m</ta>
            <ta e="T19" id="Seg_5611" s="T18">măndə-r-bia-m</ta>
            <ta e="T20" id="Seg_5612" s="T19">kăde=də</ta>
            <ta e="T21" id="Seg_5613" s="T20">ej</ta>
            <ta e="T22" id="Seg_5614" s="T21">mo-lia-m</ta>
            <ta e="T23" id="Seg_5615" s="T22">ej</ta>
            <ta e="T24" id="Seg_5616" s="T23">mo-lia-m</ta>
            <ta e="T25" id="Seg_5617" s="T24">šo-zittə</ta>
            <ta e="T26" id="Seg_5618" s="T25">döber</ta>
            <ta e="T27" id="Seg_5619" s="T26">nagur</ta>
            <ta e="T28" id="Seg_5620" s="T27">dʼala-gən</ta>
            <ta e="T29" id="Seg_5621" s="T28">dĭgəttə</ta>
            <ta e="T30" id="Seg_5622" s="T29">šo-bia-m</ta>
            <ta e="T31" id="Seg_5623" s="T30">döber</ta>
            <ta e="T33" id="Seg_5624" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_5625" s="T33">döber</ta>
            <ta e="T35" id="Seg_5626" s="T34">šo-bia-m</ta>
            <ta e="T36" id="Seg_5627" s="T35">urgaja-nə</ta>
            <ta e="T37" id="Seg_5628" s="T36">šo-bia-m</ta>
            <ta e="T38" id="Seg_5629" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_5630" s="T38">măna</ta>
            <ta e="T40" id="Seg_5631" s="T39">bar</ta>
            <ta e="T41" id="Seg_5632" s="T40">bădə-bi</ta>
            <ta e="T42" id="Seg_5633" s="T41">mĭj</ta>
            <ta e="T43" id="Seg_5634" s="T42">mĭ-bi</ta>
            <ta e="T45" id="Seg_5635" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_5636" s="T45">döber</ta>
            <ta e="T47" id="Seg_5637" s="T46">šo-bia-m</ta>
            <ta e="T48" id="Seg_5638" s="T47">buzo</ta>
            <ta e="T49" id="Seg_5639" s="T48">kirgar-laʔbə</ta>
            <ta e="T50" id="Seg_5640" s="T49">nüke</ta>
            <ta e="T51" id="Seg_5641" s="T50">bar</ta>
            <ta e="T52" id="Seg_5642" s="T51">mĭ-bi</ta>
            <ta e="T53" id="Seg_5643" s="T52">dĭʔ-nə</ta>
            <ta e="T54" id="Seg_5644" s="T53">toltanoʔ</ta>
            <ta e="T55" id="Seg_5645" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_5646" s="T55">amor-laʔbə</ta>
            <ta e="T57" id="Seg_5647" s="T56">ej</ta>
            <ta e="T58" id="Seg_5648" s="T57">kirgar-lia</ta>
            <ta e="T60" id="Seg_5649" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_5650" s="T60">nüke-gən</ta>
            <ta e="T62" id="Seg_5651" s="T61">tura-gən</ta>
            <ta e="T63" id="Seg_5652" s="T62">šišəge</ta>
            <ta e="T64" id="Seg_5653" s="T63">pʼeš</ta>
            <ta e="T65" id="Seg_5654" s="T64">nendə-bi</ta>
            <ta e="T67" id="Seg_5655" s="T66">nʼiʔnen</ta>
            <ta e="T68" id="Seg_5656" s="T67">bar</ta>
            <ta e="T69" id="Seg_5657" s="T68">šišəge</ta>
            <ta e="T70" id="Seg_5658" s="T69">ugandə</ta>
            <ta e="T71" id="Seg_5659" s="T70">büzo</ta>
            <ta e="T72" id="Seg_5660" s="T71">tura-gən</ta>
            <ta e="T73" id="Seg_5661" s="T72">nu-ga</ta>
            <ta e="T74" id="Seg_5662" s="T73">bar</ta>
            <ta e="T76" id="Seg_5663" s="T75">tüžöj</ta>
            <ta e="T77" id="Seg_5664" s="T76">tüžöj</ta>
            <ta e="T78" id="Seg_5665" s="T77">nʼe-luʔ-pi</ta>
            <ta e="T79" id="Seg_5666" s="T78">bar</ta>
            <ta e="T80" id="Seg_5667" s="T79">üdʼüge</ta>
            <ta e="T81" id="Seg_5668" s="T80">büzo</ta>
            <ta e="T83" id="Seg_5669" s="T82">miʔ</ta>
            <ta e="T84" id="Seg_5670" s="T83">tura-gən</ta>
            <ta e="T85" id="Seg_5671" s="T84">ugandə</ta>
            <ta e="T86" id="Seg_5672" s="T85">iʔgö</ta>
            <ta e="T87" id="Seg_5673" s="T86">il</ta>
            <ta e="T88" id="Seg_5674" s="T87">kü-bi-ʔi</ta>
            <ta e="T90" id="Seg_5675" s="T89">mĭm-bie-m</ta>
            <ta e="T91" id="Seg_5676" s="T90">nüke-nə</ta>
            <ta e="T92" id="Seg_5677" s="T91">dĭ</ta>
            <ta e="T94" id="Seg_5678" s="T93">ugandə</ta>
            <ta e="T95" id="Seg_5679" s="T94">ĭzem-nie</ta>
            <ta e="T96" id="Seg_5680" s="T95">măn</ta>
            <ta e="T97" id="Seg_5681" s="T96">teinen</ta>
            <ta e="T99" id="Seg_5682" s="T98">kü-lal-lə-m</ta>
            <ta e="T100" id="Seg_5683" s="T99">no</ta>
            <ta e="T101" id="Seg_5684" s="T100">kü-ʔ</ta>
            <ta e="T102" id="Seg_5685" s="T101">no</ta>
            <ta e="T103" id="Seg_5686" s="T102">kü-ʔ</ta>
            <ta e="T105" id="Seg_5687" s="T104">karəldʼan</ta>
            <ta e="T107" id="Seg_5688" s="T106">šo-la-m</ta>
            <ta e="T108" id="Seg_5689" s="T107">băzə-sʼtə</ta>
            <ta e="T109" id="Seg_5690" s="T108">tănan</ta>
            <ta e="T110" id="Seg_5691" s="T109">dĭgəttə</ta>
            <ta e="T111" id="Seg_5692" s="T110">ej</ta>
            <ta e="T112" id="Seg_5693" s="T111">kam-bia-m</ta>
            <ta e="T113" id="Seg_5694" s="T112">noʔ</ta>
            <ta e="T114" id="Seg_5695" s="T113">deʔ-pi-ʔi</ta>
            <ta e="T115" id="Seg_5696" s="T114">šide</ta>
            <ta e="T116" id="Seg_5697" s="T115">tibi</ta>
            <ta e="T117" id="Seg_5698" s="T116">dĭgəttə</ta>
            <ta e="T118" id="Seg_5699" s="T117">šo-bia-m</ta>
            <ta e="T119" id="Seg_5700" s="T118">ĭmbi</ta>
            <ta e="T120" id="Seg_5701" s="T119">kondʼo</ta>
            <ta e="T121" id="Seg_5702" s="T120">ej</ta>
            <ta e="T122" id="Seg_5703" s="T121">šo-bia-l</ta>
            <ta e="T123" id="Seg_5704" s="T122">a</ta>
            <ta e="T124" id="Seg_5705" s="T123">măn</ta>
            <ta e="T125" id="Seg_5706" s="T124">tenə-bie-m</ta>
            <ta e="T126" id="Seg_5707" s="T125">tăn</ta>
            <ta e="T127" id="Seg_5708" s="T126">kü-lam-bia-m</ta>
            <ta e="T128" id="Seg_5709" s="T127">băzə-sʼtə</ta>
            <ta e="T129" id="Seg_5710" s="T128">šo-bia-m</ta>
            <ta e="T131" id="Seg_5711" s="T130">büzo</ta>
            <ta e="T132" id="Seg_5712" s="T131">nom-laʔbə</ta>
            <ta e="T133" id="Seg_5713" s="T132">ej</ta>
            <ta e="T134" id="Seg_5714" s="T133">kirgar-laʔbə</ta>
            <ta e="T136" id="Seg_5715" s="T135">es-seŋ</ta>
            <ta e="T137" id="Seg_5716" s="T136">bar</ta>
            <ta e="T138" id="Seg_5717" s="T137">nuʔmə-leʔbə-ʔjə</ta>
            <ta e="T139" id="Seg_5718" s="T138">tura-n</ta>
            <ta e="T141" id="Seg_5719" s="T140">tura-n</ta>
            <ta e="T142" id="Seg_5720" s="T141">to-ndə</ta>
            <ta e="T144" id="Seg_5721" s="T143">šide</ta>
            <ta e="T145" id="Seg_5722" s="T144">nʼi</ta>
            <ta e="T146" id="Seg_5723" s="T145">šo-bi-ʔi</ta>
            <ta e="T147" id="Seg_5724" s="T146">nʼilgö-sʼtə</ta>
            <ta e="T150" id="Seg_5725" s="T149">i-ʔ</ta>
            <ta e="T151" id="Seg_5726" s="T150">kirgar-gaʔ</ta>
            <ta e="T152" id="Seg_5727" s="T151">i-ʔ</ta>
            <ta e="T153" id="Seg_5728" s="T152">alom-gaʔ</ta>
            <ta e="T154" id="Seg_5729" s="T153">nʼilgö-leʔ</ta>
            <ta e="T156" id="Seg_5730" s="T155">ia-l</ta>
            <ta e="T157" id="Seg_5731" s="T156">kamen</ta>
            <ta e="T158" id="Seg_5732" s="T157">šo-lə-j</ta>
            <ta e="T159" id="Seg_5733" s="T158">büžü</ta>
            <ta e="T160" id="Seg_5734" s="T159">šo-lə-j</ta>
            <ta e="T162" id="Seg_5735" s="T161">nada</ta>
            <ta e="T163" id="Seg_5736" s="T162">bü</ta>
            <ta e="T164" id="Seg_5737" s="T163">ejü-m-zittə</ta>
            <ta e="T165" id="Seg_5738" s="T164">dĭgəttə</ta>
            <ta e="T166" id="Seg_5739" s="T165">tüžöj</ta>
            <ta e="T167" id="Seg_5740" s="T166">bĭd-əl-zittə</ta>
            <ta e="T169" id="Seg_5741" s="T168">ĭmbi</ta>
            <ta e="T170" id="Seg_5742" s="T169">nʼilgö-sʼtə</ta>
            <ta e="T171" id="Seg_5743" s="T170">šo-bi-laʔ</ta>
            <ta e="T172" id="Seg_5744" s="T171">măndə-r-zittə</ta>
            <ta e="T173" id="Seg_5745" s="T172">šo-bi-laʔ</ta>
            <ta e="T175" id="Seg_5746" s="T174">ej</ta>
            <ta e="T176" id="Seg_5747" s="T175">kürüm-nie-ʔi</ta>
            <ta e="T177" id="Seg_5748" s="T176">kuvas</ta>
            <ta e="T178" id="Seg_5749" s="T177">nʼi-zeŋ</ta>
            <ta e="T179" id="Seg_5750" s="T178">ej</ta>
            <ta e="T180" id="Seg_5751" s="T179">kürüm-nie-ʔi</ta>
            <ta e="T181" id="Seg_5752" s="T180">nu-laʔbə-ʔjə</ta>
            <ta e="T182" id="Seg_5753" s="T181">jakše-ʔi</ta>
            <ta e="T183" id="Seg_5754" s="T182">ugandə</ta>
            <ta e="T185" id="Seg_5755" s="T184">sanə</ta>
            <ta e="T186" id="Seg_5756" s="T185">teinen</ta>
            <ta e="T187" id="Seg_5757" s="T186">mĭ-bie-m</ta>
            <ta e="T188" id="Seg_5758" s="T187">tănan</ta>
            <ta e="T189" id="Seg_5759" s="T188">lej-zittə</ta>
            <ta e="T191" id="Seg_5760" s="T190">sʼima-t</ta>
            <ta e="T192" id="Seg_5761" s="T191">măndə-r-zittə</ta>
            <ta e="T194" id="Seg_5762" s="T193">ku-zaŋ-də</ta>
            <ta e="T195" id="Seg_5763" s="T194">nʼilgö-sʼtə</ta>
            <ta e="T196" id="Seg_5764" s="T195">aŋ-də</ta>
            <ta e="T197" id="Seg_5765" s="T196">am-zittə</ta>
            <ta e="T198" id="Seg_5766" s="T197">tĭme-ʔi</ta>
            <ta e="T199" id="Seg_5767" s="T198">talbə-zittə</ta>
            <ta e="T200" id="Seg_5768" s="T199">ipek</ta>
            <ta e="T202" id="Seg_5769" s="T201">tenö-bia-m</ta>
            <ta e="T203" id="Seg_5770" s="T202">tenö-bia-m</ta>
            <ta e="T204" id="Seg_5771" s="T203">kăde</ta>
            <ta e="T205" id="Seg_5772" s="T204">döber</ta>
            <ta e="T206" id="Seg_5773" s="T205">šo-zittə</ta>
            <ta e="T208" id="Seg_5774" s="T207">kuba</ta>
            <ta e="T209" id="Seg_5775" s="T208">dʼü-gən</ta>
            <ta e="T210" id="Seg_5776" s="T209">iʔbo-laʔ-pi</ta>
            <ta e="T211" id="Seg_5777" s="T210">ipek</ta>
            <ta e="T212" id="Seg_5778" s="T211">em-bi-ʔi</ta>
            <ta e="T213" id="Seg_5779" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_5780" s="T213">amor-bi-ʔi</ta>
            <ta e="T215" id="Seg_5781" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_5782" s="T215">ipek</ta>
            <ta e="T217" id="Seg_5783" s="T216">i</ta>
            <ta e="T218" id="Seg_5784" s="T217">uja</ta>
            <ta e="T222" id="Seg_5785" s="T221">dĭgəttə</ta>
            <ta e="T223" id="Seg_5786" s="T222">am-bi-ʔi</ta>
            <ta e="T224" id="Seg_5787" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_5788" s="T224">kudaj-də</ta>
            <ta e="T226" id="Seg_5789" s="T225">numan üzə-bi-ʔi</ta>
            <ta e="T851" id="Seg_5790" s="T227">Kazan</ta>
            <ta e="T228" id="Seg_5791" s="T851">tura-nə</ta>
            <ta e="T229" id="Seg_5792" s="T228">mĭm-bie-m</ta>
            <ta e="T230" id="Seg_5793" s="T229">tʼeger-gən</ta>
            <ta e="T231" id="Seg_5794" s="T230">tʼeger-gən</ta>
            <ta e="T233" id="Seg_5795" s="T232">kudaj</ta>
            <ta e="T235" id="Seg_5796" s="T234">numan üzə-bie-m</ta>
            <ta e="T237" id="Seg_5797" s="T236">iʔgö</ta>
            <ta e="T238" id="Seg_5798" s="T237">il</ta>
            <ta e="T239" id="Seg_5799" s="T238">kü-lam-bi-ʔi</ta>
            <ta e="T241" id="Seg_5800" s="T240">krospa-ʔi</ta>
            <ta e="T242" id="Seg_5801" s="T241">ugandə</ta>
            <ta e="T243" id="Seg_5802" s="T242">iʔgö</ta>
            <ta e="T244" id="Seg_5803" s="T243">nu-ga-ʔi</ta>
            <ta e="T245" id="Seg_5804" s="T244">bar</ta>
            <ta e="T247" id="Seg_5805" s="T246">miʔ</ta>
            <ta e="T248" id="Seg_5806" s="T247">koʔbdo</ta>
            <ta e="T249" id="Seg_5807" s="T248">ĭzem-bi</ta>
            <ta e="T250" id="Seg_5808" s="T249">a</ta>
            <ta e="T251" id="Seg_5809" s="T250">aba-t</ta>
            <ta e="T253" id="Seg_5810" s="T252">kum-bi</ta>
            <ta e="T255" id="Seg_5811" s="T254">dʼazir-zittə</ta>
            <ta e="T258" id="Seg_5812" s="T257">miʔnʼibeʔ</ta>
            <ta e="T259" id="Seg_5813" s="T258">teinen</ta>
            <ta e="T260" id="Seg_5814" s="T259">nüdʼi-n</ta>
            <ta e="T261" id="Seg_5815" s="T260">tibi</ta>
            <ta e="T262" id="Seg_5816" s="T261">ne-zi</ta>
            <ta e="T263" id="Seg_5817" s="T262">bar</ta>
            <ta e="T264" id="Seg_5818" s="T263">dʼabro-bi-ʔi</ta>
            <ta e="T265" id="Seg_5819" s="T264">uda-gən</ta>
            <ta e="T266" id="Seg_5820" s="T265">bar</ta>
            <ta e="T269" id="Seg_5821" s="T268">kem</ta>
            <ta e="T270" id="Seg_5822" s="T269">mʼaŋ-naʔbə</ta>
            <ta e="T271" id="Seg_5823" s="T270">es-seŋ-də</ta>
            <ta e="T272" id="Seg_5824" s="T271">bar</ta>
            <ta e="T273" id="Seg_5825" s="T272">kirgar-laʔbə-ʔjə</ta>
            <ta e="T274" id="Seg_5826" s="T273">măn</ta>
            <ta e="T275" id="Seg_5827" s="T274">šo-bia-m</ta>
            <ta e="T276" id="Seg_5828" s="T275">bar</ta>
            <ta e="T277" id="Seg_5829" s="T276">kudo-nzə-bia-m</ta>
            <ta e="T279" id="Seg_5830" s="T278">büzo</ta>
            <ta e="T280" id="Seg_5831" s="T279">iʔbə-bi</ta>
            <ta e="T281" id="Seg_5832" s="T280">ej</ta>
            <ta e="T282" id="Seg_5833" s="T281">kirgar-ia</ta>
            <ta e="T283" id="Seg_5834" s="T282">tüj</ta>
            <ta e="T285" id="Seg_5835" s="T284">možna</ta>
            <ta e="T286" id="Seg_5836" s="T285">dʼăbaktər-zittə</ta>
            <ta e="T288" id="Seg_5837" s="T287">büzʼe-n</ta>
            <ta e="T289" id="Seg_5838" s="T288">bar</ta>
            <ta e="T290" id="Seg_5839" s="T289">püje-t</ta>
            <ta e="T291" id="Seg_5840" s="T290">tʼukum-bi</ta>
            <ta e="T292" id="Seg_5841" s="T291">gijen=də</ta>
            <ta e="T293" id="Seg_5842" s="T292">ara</ta>
            <ta e="T294" id="Seg_5843" s="T293">bĭt-le-m</ta>
            <ta e="T295" id="Seg_5844" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_5845" s="T295">pograšsʼe</ta>
            <ta e="T297" id="Seg_5846" s="T296">a-bi</ta>
            <ta e="T298" id="Seg_5847" s="T297">amno-bi</ta>
            <ta e="T299" id="Seg_5848" s="T298">pa-gən</ta>
            <ta e="T300" id="Seg_5849" s="T299">a</ta>
            <ta e="T301" id="Seg_5850" s="T300">măn</ta>
            <ta e="T302" id="Seg_5851" s="T301">urgaja-m</ta>
            <ta e="T303" id="Seg_5852" s="T302">mĭm-bi</ta>
            <ta e="T852" id="Seg_5853" s="T303">Kazan</ta>
            <ta e="T304" id="Seg_5854" s="T852">tura-nə</ta>
            <ta e="T305" id="Seg_5855" s="T304">i</ta>
            <ta e="T306" id="Seg_5856" s="T305">ara</ta>
            <ta e="T307" id="Seg_5857" s="T306">deʔ-pi</ta>
            <ta e="T308" id="Seg_5858" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_5859" s="T308">amnə-bi</ta>
            <ta e="T310" id="Seg_5860" s="T309">tüʔ-sʼittə</ta>
            <ta e="T312" id="Seg_5861" s="T311">diʔ-nə</ta>
            <ta e="T313" id="Seg_5862" s="T312">köten-zi</ta>
            <ta e="T314" id="Seg_5863" s="T313">dĭgəttə</ta>
            <ta e="T315" id="Seg_5864" s="T314">dĭ</ta>
            <ta e="T316" id="Seg_5865" s="T315">šo-bi</ta>
            <ta e="T317" id="Seg_5866" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_5867" s="T317">dĭʔ-nə</ta>
            <ta e="T319" id="Seg_5868" s="T318">ara</ta>
            <ta e="T320" id="Seg_5869" s="T319">mĭ-bi</ta>
            <ta e="T321" id="Seg_5870" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_5871" s="T321">bĭʔ-pi</ta>
            <ta e="T323" id="Seg_5872" s="T322">i</ta>
            <ta e="T324" id="Seg_5873" s="T323">măn-də</ta>
            <ta e="T325" id="Seg_5874" s="T324">măn-də</ta>
            <ta e="T326" id="Seg_5875" s="T325">dĭʔ-nə</ta>
            <ta e="T327" id="Seg_5876" s="T326">ne</ta>
            <ta e="T328" id="Seg_5877" s="T327">dak</ta>
            <ta e="T329" id="Seg_5878" s="T328">ne</ta>
            <ta e="T330" id="Seg_5879" s="T329">ot</ta>
            <ta e="T331" id="Seg_5880" s="T330">dak</ta>
            <ta e="T332" id="Seg_5881" s="T331">ne</ta>
            <ta e="T333" id="Seg_5882" s="T332">dak</ta>
            <ta e="T334" id="Seg_5883" s="T333">ne</ta>
            <ta e="T335" id="Seg_5884" s="T334">ĭmbi</ta>
            <ta e="T336" id="Seg_5885" s="T335">tăn</ta>
            <ta e="T337" id="Seg_5886" s="T336">dăre</ta>
            <ta e="T340" id="Seg_5887" s="T339">a</ta>
            <ta e="T341" id="Seg_5888" s="T340">tăn</ta>
            <ta e="T342" id="Seg_5889" s="T341">măna</ta>
            <ta e="T344" id="Seg_5890" s="T343">köten-zi</ta>
            <ta e="T345" id="Seg_5891" s="T344">amno-bia-l</ta>
            <ta e="T346" id="Seg_5892" s="T345">i</ta>
            <ta e="T347" id="Seg_5893" s="T346">tüʔ-pie-l</ta>
            <ta e="T350" id="Seg_5894" s="T349">măn</ta>
            <ta e="T351" id="Seg_5895" s="T350">urgaja-m</ta>
            <ta e="T352" id="Seg_5896" s="T351">šaman-də</ta>
            <ta e="T353" id="Seg_5897" s="T352">mĭm-bie-m</ta>
            <ta e="T354" id="Seg_5898" s="T353">dĭn</ta>
            <ta e="T355" id="Seg_5899" s="T354">bar</ta>
            <ta e="T357" id="Seg_5900" s="T356">uja</ta>
            <ta e="T358" id="Seg_5901" s="T357">mĭnzer-bi-ʔi</ta>
            <ta e="T359" id="Seg_5902" s="T358">takše-nə</ta>
            <ta e="T360" id="Seg_5903" s="T359">urgo</ta>
            <ta e="T361" id="Seg_5904" s="T360">takše</ta>
            <ta e="T362" id="Seg_5905" s="T361">em-bi-ʔi</ta>
            <ta e="T363" id="Seg_5906" s="T362">dĭ</ta>
            <ta e="T364" id="Seg_5907" s="T363">bar</ta>
            <ta e="T365" id="Seg_5908" s="T364">šĭšəge</ta>
            <ta e="T366" id="Seg_5909" s="T365">mo-lam-bi</ta>
            <ta e="T367" id="Seg_5910" s="T366">amna-ʔ</ta>
            <ta e="T369" id="Seg_5911" s="T368">amor-zittə</ta>
            <ta e="T370" id="Seg_5912" s="T369">dĭ</ta>
            <ta e="T371" id="Seg_5913" s="T370">bar</ta>
            <ta e="T372" id="Seg_5914" s="T371">uja</ta>
            <ta e="T373" id="Seg_5915" s="T372">i-bi</ta>
            <ta e="T374" id="Seg_5916" s="T373">dĭ</ta>
            <ta e="T375" id="Seg_5917" s="T374">ugandə</ta>
            <ta e="T376" id="Seg_5918" s="T375">šišəge</ta>
            <ta e="T377" id="Seg_5919" s="T376">dĭgəttə</ta>
            <ta e="T378" id="Seg_5920" s="T377">men-də</ta>
            <ta e="T380" id="Seg_5921" s="T379">baʔ-luʔ-pi</ta>
            <ta e="T382" id="Seg_5922" s="T381">buben-zi</ta>
            <ta e="T383" id="Seg_5923" s="T382">bar</ta>
            <ta e="T385" id="Seg_5924" s="T384">sʼar-bi</ta>
            <ta e="T386" id="Seg_5925" s="T385">bar</ta>
            <ta e="T387" id="Seg_5926" s="T386">il-də</ta>
            <ta e="T388" id="Seg_5927" s="T387">mĭm-bi</ta>
            <ta e="T389" id="Seg_5928" s="T388">a</ta>
            <ta e="T390" id="Seg_5929" s="T389">măn</ta>
            <ta e="T391" id="Seg_5930" s="T390">urgaja-nə</ta>
            <ta e="T392" id="Seg_5931" s="T391">ej</ta>
            <ta e="T393" id="Seg_5932" s="T392">mĭ-bie-m</ta>
            <ta e="T394" id="Seg_5933" s="T393">dĭgəttə</ta>
            <ta e="T395" id="Seg_5934" s="T394">mĭm-bi</ta>
            <ta e="T396" id="Seg_5935" s="T395">i-ʔ</ta>
            <ta e="T397" id="Seg_5936" s="T396">deʔ-keʔ</ta>
            <ta e="T398" id="Seg_5937" s="T397">dĭ</ta>
            <ta e="T399" id="Seg_5938" s="T398">ne</ta>
            <ta e="T400" id="Seg_5939" s="T399">kazak</ta>
            <ta e="T401" id="Seg_5940" s="T400">ne</ta>
            <ta e="T404" id="Seg_5941" s="T403">šaman-gən</ta>
            <ta e="T405" id="Seg_5942" s="T404">bar</ta>
            <ta e="T406" id="Seg_5943" s="T405">părga</ta>
            <ta e="T407" id="Seg_5944" s="T406">i-bi</ta>
            <ta e="T411" id="Seg_5945" s="T410">bar</ta>
            <ta e="T413" id="Seg_5946" s="T412">kurizə-n</ta>
            <ta e="T414" id="Seg_5947" s="T413">kuba</ta>
            <ta e="T415" id="Seg_5948" s="T414">albuga-n</ta>
            <ta e="T416" id="Seg_5949" s="T415">kuba</ta>
            <ta e="T417" id="Seg_5950" s="T416">părga-gən</ta>
            <ta e="T418" id="Seg_5951" s="T417">tažəb-ə-n</ta>
            <ta e="T420" id="Seg_5952" s="T419">kuba</ta>
            <ta e="T422" id="Seg_5953" s="T420">părga-gən</ta>
            <ta e="T423" id="Seg_5954" s="T422">poʔto-n</ta>
            <ta e="T424" id="Seg_5955" s="T423">kuba</ta>
            <ta e="T426" id="Seg_5956" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_5957" s="T426">šaman</ta>
            <ta e="T428" id="Seg_5958" s="T427">bar</ta>
            <ta e="T432" id="Seg_5959" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_5960" s="T432">dagaj</ta>
            <ta e="T434" id="Seg_5961" s="T433">i-bi</ta>
            <ta e="T435" id="Seg_5962" s="T434">da</ta>
            <ta e="T436" id="Seg_5963" s="T435">bar</ta>
            <ta e="T437" id="Seg_5964" s="T436">müʔbdə-bi</ta>
            <ta e="T439" id="Seg_5965" s="T438">sĭj-də</ta>
            <ta e="T440" id="Seg_5966" s="T439">i</ta>
            <ta e="T441" id="Seg_5967" s="T440">dĭgəttə</ta>
            <ta e="T442" id="Seg_5968" s="T441">saʔmə-luʔ-pi</ta>
            <ta e="T443" id="Seg_5969" s="T442">a</ta>
            <ta e="T444" id="Seg_5970" s="T443">il</ta>
            <ta e="T445" id="Seg_5971" s="T444">iʔgö</ta>
            <ta e="T446" id="Seg_5972" s="T445">šonə-ga-ʔi</ta>
            <ta e="T457" id="Seg_5973" s="T456">tibi-ʔi</ta>
            <ta e="T460" id="Seg_5974" s="T459">kamen</ta>
            <ta e="T461" id="Seg_5975" s="T460">kal-la-ʔi</ta>
            <ta e="T462" id="Seg_5976" s="T461">dʼije-nə</ta>
            <ta e="T463" id="Seg_5977" s="T462">dĭgəttə</ta>
            <ta e="T464" id="Seg_5978" s="T463">šaman-də</ta>
            <ta e="T465" id="Seg_5979" s="T464">kal-la-ʔi</ta>
            <ta e="T466" id="Seg_5980" s="T465">dĭ</ta>
            <ta e="T467" id="Seg_5981" s="T466">măl-lə-j</ta>
            <ta e="T468" id="Seg_5982" s="T467">tăn</ta>
            <ta e="T470" id="Seg_5983" s="T469">albuga</ta>
            <ta e="T471" id="Seg_5984" s="T470">kut-la-l</ta>
            <ta e="T472" id="Seg_5985" s="T471">a</ta>
            <ta e="T473" id="Seg_5986" s="T472">šindi-nə</ta>
            <ta e="T474" id="Seg_5987" s="T473">măl-lə-j</ta>
            <ta e="T475" id="Seg_5988" s="T474">ej</ta>
            <ta e="T476" id="Seg_5989" s="T475">kut-la-l</ta>
            <ta e="T479" id="Seg_5990" s="T478">šaman</ta>
            <ta e="T480" id="Seg_5991" s="T479">i</ta>
            <ta e="T481" id="Seg_5992" s="T480">miʔ</ta>
            <ta e="T482" id="Seg_5993" s="T481">šaman</ta>
            <ta e="T483" id="Seg_5994" s="T482">bar</ta>
            <ta e="T484" id="Seg_5995" s="T483">dʼabro-bi-ʔi</ta>
            <ta e="T487" id="Seg_5996" s="T486">kam-bi</ta>
            <ta e="T488" id="Seg_5997" s="T487">maʔ-ndə</ta>
            <ta e="T489" id="Seg_5998" s="T488">a</ta>
            <ta e="T490" id="Seg_5999" s="T489">miʔ</ta>
            <ta e="T491" id="Seg_6000" s="T490">šaman</ta>
            <ta e="T493" id="Seg_6001" s="T492">dĭ-m</ta>
            <ta e="T494" id="Seg_6002" s="T493">bar</ta>
            <ta e="T495" id="Seg_6003" s="T494">bĭd-leʔ-pi</ta>
            <ta e="T496" id="Seg_6004" s="T495">pograš</ta>
            <ta e="T497" id="Seg_6005" s="T496">mo-lam-bi</ta>
            <ta e="T498" id="Seg_6006" s="T497">a</ta>
            <ta e="T499" id="Seg_6007" s="T498">dĭ</ta>
            <ta e="T500" id="Seg_6008" s="T499">bar</ta>
            <ta e="T501" id="Seg_6009" s="T500">multuk-si</ta>
            <ta e="T502" id="Seg_6010" s="T501">dĭ-m</ta>
            <ta e="T503" id="Seg_6011" s="T502">kut-lam-bi</ta>
            <ta e="T504" id="Seg_6012" s="T503">uda-nə</ta>
            <ta e="T505" id="Seg_6013" s="T504">uda-bə</ta>
            <ta e="T506" id="Seg_6014" s="T505">bar</ta>
            <ta e="T507" id="Seg_6015" s="T506">băldə-bi</ta>
            <ta e="T511" id="Seg_6016" s="T510">girgit</ta>
            <ta e="T512" id="Seg_6017" s="T511">kuza</ta>
            <ta e="T513" id="Seg_6018" s="T512">ĭzem-nuʔ-pi</ta>
            <ta e="T514" id="Seg_6019" s="T513">dĭgəttə</ta>
            <ta e="T515" id="Seg_6020" s="T514">ka-la-ʔi</ta>
            <ta e="T516" id="Seg_6021" s="T515">šaman-də</ta>
            <ta e="T517" id="Seg_6022" s="T516">dĭ</ta>
            <ta e="T518" id="Seg_6023" s="T517">bar</ta>
            <ta e="T519" id="Seg_6024" s="T518">dĭ-m</ta>
            <ta e="T520" id="Seg_6025" s="T519">dʼazir-ia</ta>
            <ta e="T521" id="Seg_6026" s="T520">lʼevăj</ta>
            <ta e="T522" id="Seg_6027" s="T521">uda-ndə</ta>
            <ta e="T523" id="Seg_6028" s="T522">nuld-lə-j</ta>
            <ta e="T524" id="Seg_6029" s="T523">dĭ</ta>
            <ta e="T525" id="Seg_6030" s="T524">üge</ta>
            <ta e="T526" id="Seg_6031" s="T525">dăre</ta>
            <ta e="T527" id="Seg_6032" s="T526">bar</ta>
            <ta e="T528" id="Seg_6033" s="T527">ulu-t-si</ta>
            <ta e="T532" id="Seg_6034" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_6035" s="T532">dĭ</ta>
            <ta e="T534" id="Seg_6036" s="T533">dĭʔ-nə</ta>
            <ta e="T535" id="Seg_6037" s="T534">tʼažer-luʔ-pi</ta>
            <ta e="T536" id="Seg_6038" s="T535">i</ta>
            <ta e="T537" id="Seg_6039" s="T536">bos-tə</ta>
            <ta e="T538" id="Seg_6040" s="T537">bar</ta>
            <ta e="T539" id="Seg_6041" s="T538">suʔmi-leʔbə</ta>
            <ta e="T540" id="Seg_6042" s="T539">buben-də</ta>
            <ta e="T541" id="Seg_6043" s="T540">sʼar-laʔbə</ta>
            <ta e="T543" id="Seg_6044" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_6045" s="T543">dĭ</ta>
            <ta e="T545" id="Seg_6046" s="T544">ej</ta>
            <ta e="T546" id="Seg_6047" s="T545">ĭzem-nie</ta>
            <ta e="T547" id="Seg_6048" s="T546">maʔ-ndə</ta>
            <ta e="T548" id="Seg_6049" s="T547">šo-lə-j</ta>
            <ta e="T551" id="Seg_6050" s="T550">tibi-zeŋ</ta>
            <ta e="T552" id="Seg_6051" s="T551">dʼije-nə</ta>
            <ta e="T848" id="Seg_6052" s="T552">kal-la</ta>
            <ta e="T553" id="Seg_6053" s="T848">dʼür-bi-ʔi</ta>
            <ta e="T554" id="Seg_6054" s="T553">ne-zeŋ</ta>
            <ta e="T555" id="Seg_6055" s="T554">maʔ-ən</ta>
            <ta e="T556" id="Seg_6056" s="T555">amno-bi-ʔi</ta>
            <ta e="T557" id="Seg_6057" s="T556">saranka-ʔi</ta>
            <ta e="T558" id="Seg_6058" s="T557">tĭl-bi-ʔi</ta>
            <ta e="T559" id="Seg_6059" s="T558">uja-m</ta>
            <ta e="T560" id="Seg_6060" s="T559">em-bi-ʔi</ta>
            <ta e="T561" id="Seg_6061" s="T560">ipek</ta>
            <ta e="T562" id="Seg_6062" s="T561">naga</ta>
            <ta e="T564" id="Seg_6063" s="T563">ne-zeŋ</ta>
            <ta e="T565" id="Seg_6064" s="T564">bar</ta>
            <ta e="T567" id="Seg_6065" s="T566">noʔ</ta>
            <ta e="T568" id="Seg_6066" s="T567">nĭŋgə-bi-ʔi</ta>
            <ta e="T569" id="Seg_6067" s="T568">travʼanʼiga-ʔi</ta>
            <ta e="T570" id="Seg_6068" s="T569">kür-bi-ʔi</ta>
            <ta e="T571" id="Seg_6069" s="T570">jama-ʔi</ta>
            <ta e="T572" id="Seg_6070" s="T571">šöʔ-pi-ʔi</ta>
            <ta e="T573" id="Seg_6071" s="T572">žila-ʔi</ta>
            <ta e="T574" id="Seg_6072" s="T573">kür-bi-ʔi</ta>
            <ta e="T575" id="Seg_6073" s="T574">dʼije-gən</ta>
            <ta e="T576" id="Seg_6074" s="T575">maʔ</ta>
            <ta e="T578" id="Seg_6075" s="T577">a-bi-ʔi</ta>
            <ta e="T579" id="Seg_6076" s="T578">dĭgəttə</ta>
            <ta e="T580" id="Seg_6077" s="T579">dĭ</ta>
            <ta e="T581" id="Seg_6078" s="T580">maʔ-kən</ta>
            <ta e="T582" id="Seg_6079" s="T581">uja</ta>
            <ta e="T583" id="Seg_6080" s="T582">mĭnzer-bi-ʔi</ta>
            <ta e="T584" id="Seg_6081" s="T583">segi</ta>
            <ta e="T585" id="Seg_6082" s="T584">bü</ta>
            <ta e="T586" id="Seg_6083" s="T585">mĭnzer-bi-ʔi</ta>
            <ta e="T587" id="Seg_6084" s="T586">amor-bi-ʔi</ta>
            <ta e="T589" id="Seg_6085" s="T588">tura-ʔi</ta>
            <ta e="T590" id="Seg_6086" s="T589">i-bi-ʔi</ta>
            <ta e="T591" id="Seg_6087" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_6088" s="T591">tura-gən</ta>
            <ta e="T593" id="Seg_6089" s="T592">ej</ta>
            <ta e="T594" id="Seg_6090" s="T593">amno-bi-ʔi</ta>
            <ta e="T595" id="Seg_6091" s="T594">nʼiʔnen</ta>
            <ta e="T596" id="Seg_6092" s="T595">maʔ</ta>
            <ta e="T597" id="Seg_6093" s="T596">i-bi</ta>
            <ta e="T598" id="Seg_6094" s="T597">dĭn</ta>
            <ta e="T599" id="Seg_6095" s="T598">mĭnzer-bi-ʔi</ta>
            <ta e="T600" id="Seg_6096" s="T599">mĭj</ta>
            <ta e="T601" id="Seg_6097" s="T600">segi</ta>
            <ta e="T602" id="Seg_6098" s="T601">bü</ta>
            <ta e="T603" id="Seg_6099" s="T602">mĭnzer-bi-ʔi</ta>
            <ta e="T604" id="Seg_6100" s="T603">amor-bi-ʔi</ta>
            <ta e="T605" id="Seg_6101" s="T604">a</ta>
            <ta e="T606" id="Seg_6102" s="T605">tura-gən</ta>
            <ta e="T607" id="Seg_6103" s="T606">kunol-bi-ʔi</ta>
            <ta e="T609" id="Seg_6104" s="T608">ma-ʔi</ta>
            <ta e="T610" id="Seg_6105" s="T609">a-bi-ʔi</ta>
            <ta e="T612" id="Seg_6106" s="T611">pa-ʔi</ta>
            <ta e="T613" id="Seg_6107" s="T612">dĭgəttə</ta>
            <ta e="T614" id="Seg_6108" s="T613">kuba</ta>
            <ta e="T615" id="Seg_6109" s="T614">kür-bi-ʔi</ta>
            <ta e="T616" id="Seg_6110" s="T615">pa-t-si</ta>
            <ta e="T617" id="Seg_6111" s="T616">da</ta>
            <ta e="T618" id="Seg_6112" s="T617">en-bi-ʔi</ta>
            <ta e="T619" id="Seg_6113" s="T618">diʔ-nə</ta>
            <ta e="T620" id="Seg_6114" s="T619">dĭgəttə</ta>
            <ta e="T621" id="Seg_6115" s="T620">tagan-əʔi</ta>
            <ta e="T622" id="Seg_6116" s="T621">a-bi-ʔi</ta>
            <ta e="T623" id="Seg_6117" s="T622">dĭgəttə</ta>
            <ta e="T624" id="Seg_6118" s="T623">aspa-ʔi</ta>
            <ta e="T625" id="Seg_6119" s="T624">edə-bi-ʔi</ta>
            <ta e="T628" id="Seg_6120" s="T627">mĭnzer-zittə</ta>
            <ta e="T629" id="Seg_6121" s="T628">uja</ta>
            <ta e="T630" id="Seg_6122" s="T629">da</ta>
            <ta e="T631" id="Seg_6123" s="T630">segi</ta>
            <ta e="T632" id="Seg_6124" s="T631">bü</ta>
            <ta e="T633" id="Seg_6125" s="T632">mĭnzer-zittə</ta>
            <ta e="T635" id="Seg_6126" s="T633">dĭgəttə</ta>
            <ta e="T638" id="Seg_6127" s="T637">dʼü-gən</ta>
            <ta e="T639" id="Seg_6128" s="T638">šü</ta>
            <ta e="T640" id="Seg_6129" s="T639">am-bi-ʔi</ta>
            <ta e="T641" id="Seg_6130" s="T640">nʼüʔnən</ta>
            <ta e="T642" id="Seg_6131" s="T641">ši</ta>
            <ta e="T643" id="Seg_6132" s="T642">i-bi</ta>
            <ta e="T644" id="Seg_6133" s="T643">dĭbər</ta>
            <ta e="T646" id="Seg_6134" s="T645">ber</ta>
            <ta e="T647" id="Seg_6135" s="T646">kand-laʔbə</ta>
            <ta e="T648" id="Seg_6136" s="T647">dĭgəttə</ta>
            <ta e="T649" id="Seg_6137" s="T648">pa-ʔi</ta>
            <ta e="T650" id="Seg_6138" s="T649">am-bi-ʔi</ta>
            <ta e="T651" id="Seg_6139" s="T650">štobɨ</ta>
            <ta e="T652" id="Seg_6140" s="T651">šü-nə</ta>
            <ta e="T653" id="Seg_6141" s="T652">šindi=də</ta>
            <ta e="T654" id="Seg_6142" s="T653">ej</ta>
            <ta e="T656" id="Seg_6143" s="T655">saʔmə-bi</ta>
            <ta e="T658" id="Seg_6144" s="T657">šišəge</ta>
            <ta e="T659" id="Seg_6145" s="T658">mo-bi</ta>
            <ta e="T660" id="Seg_6146" s="T659">dak</ta>
            <ta e="T662" id="Seg_6147" s="T661">koža-zi</ta>
            <ta e="T663" id="Seg_6148" s="T662">kaj-bi-ʔi</ta>
            <ta e="T664" id="Seg_6149" s="T663">a</ta>
            <ta e="T665" id="Seg_6150" s="T664">ejü</ta>
            <ta e="T666" id="Seg_6151" s="T665">mo-bi</ta>
            <ta e="T667" id="Seg_6152" s="T666">dak</ta>
            <ta e="T669" id="Seg_6153" s="T668">kür-bi-ʔi</ta>
            <ta e="T671" id="Seg_6154" s="T670">kaj-bi-ʔi</ta>
            <ta e="T673" id="Seg_6155" s="T672">urgo</ta>
            <ta e="T674" id="Seg_6156" s="T673">i-bi</ta>
            <ta e="T677" id="Seg_6157" s="T676">teʔtə</ta>
            <ta e="T678" id="Seg_6158" s="T677">il</ta>
            <ta e="T679" id="Seg_6159" s="T678">iʔbo-bi-ʔi</ta>
            <ta e="T680" id="Seg_6160" s="T679">bʼeʔ</ta>
            <ta e="T681" id="Seg_6161" s="T680">il</ta>
            <ta e="T682" id="Seg_6162" s="T681">kamen</ta>
            <ta e="T683" id="Seg_6163" s="T682">sĭre</ta>
            <ta e="T684" id="Seg_6164" s="T683">i-bi</ta>
            <ta e="T685" id="Seg_6165" s="T684">dĭ-zeŋ</ta>
            <ta e="T686" id="Seg_6166" s="T685">dön</ta>
            <ta e="T687" id="Seg_6167" s="T686">amno-bi-ʔi</ta>
            <ta e="T688" id="Seg_6168" s="T687">a</ta>
            <ta e="T689" id="Seg_6169" s="T688">kamen</ta>
            <ta e="T690" id="Seg_6170" s="T689">sĭre</ta>
            <ta e="T849" id="Seg_6171" s="T690">kal-la</ta>
            <ta e="T691" id="Seg_6172" s="T849">dʼür-bi</ta>
            <ta e="T692" id="Seg_6173" s="T691">dĭgəttə</ta>
            <ta e="T693" id="Seg_6174" s="T692">dĭ-zeŋ</ta>
            <ta e="T694" id="Seg_6175" s="T693">kan-də-ga-ʔi</ta>
            <ta e="T695" id="Seg_6176" s="T694">dʼije-nə</ta>
            <ta e="T696" id="Seg_6177" s="T695">urgo</ta>
            <ta e="T697" id="Seg_6178" s="T696">măja-nə</ta>
            <ta e="T699" id="Seg_6179" s="T698">ălenʼə-ʔi-zi</ta>
            <ta e="T700" id="Seg_6180" s="T699">mĭm-bi-ʔi</ta>
            <ta e="T701" id="Seg_6181" s="T700">Tʼelam-də</ta>
            <ta e="T702" id="Seg_6182" s="T701">kan-bi-ʔi</ta>
            <ta e="T703" id="Seg_6183" s="T702">dĭn</ta>
            <ta e="T704" id="Seg_6184" s="T703">urgo</ta>
            <ta e="T705" id="Seg_6185" s="T704">bü</ta>
            <ta e="T706" id="Seg_6186" s="T705">kola</ta>
            <ta e="T707" id="Seg_6187" s="T706">dʼaʔ-pi-ʔi</ta>
            <ta e="T709" id="Seg_6188" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_6189" s="T709">dĭ-zeŋ</ta>
            <ta e="T711" id="Seg_6190" s="T710">kam-bi-ʔi</ta>
            <ta e="T712" id="Seg_6191" s="T711">es-seŋ-də</ta>
            <ta e="T713" id="Seg_6192" s="T712">i</ta>
            <ta e="T714" id="Seg_6193" s="T713">ne-zeŋ-də</ta>
            <ta e="T715" id="Seg_6194" s="T714">i-bi-ʔi</ta>
            <ta e="T716" id="Seg_6195" s="T715">ălenʼə-ʔi-zi</ta>
            <ta e="T717" id="Seg_6196" s="T716">kam-bi-ʔi</ta>
            <ta e="T719" id="Seg_6197" s="T718">i</ta>
            <ta e="T720" id="Seg_6198" s="T719">men-zeŋ-zi</ta>
            <ta e="T722" id="Seg_6199" s="T721">dĭgəttə</ta>
            <ta e="T723" id="Seg_6200" s="T722">dĭ-zeŋ</ta>
            <ta e="T724" id="Seg_6201" s="T723">dʼije-gən</ta>
            <ta e="T725" id="Seg_6202" s="T724">keʔbde</ta>
            <ta e="T726" id="Seg_6203" s="T725">oʔbdə-bi-ʔi</ta>
            <ta e="T727" id="Seg_6204" s="T726">am-bi-ʔi</ta>
            <ta e="T728" id="Seg_6205" s="T727">san</ta>
            <ta e="T729" id="Seg_6206" s="T728">oʔbdə-bi-ʔi</ta>
            <ta e="T730" id="Seg_6207" s="T729">lej-bi-ʔi</ta>
            <ta e="T732" id="Seg_6208" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_6209" s="T732">dĭ-zeŋ</ta>
            <ta e="T734" id="Seg_6210" s="T733">beške-ʔi</ta>
            <ta e="T735" id="Seg_6211" s="T734">oʔbdə-bi-ʔi</ta>
            <ta e="T736" id="Seg_6212" s="T735">mĭnzer-bi-ʔi</ta>
            <ta e="T737" id="Seg_6213" s="T736">i</ta>
            <ta e="T738" id="Seg_6214" s="T737">tustʼar-bi-ʔi</ta>
            <ta e="T740" id="Seg_6215" s="T739">dĭgəttə</ta>
            <ta e="T741" id="Seg_6216" s="T740">dĭn</ta>
            <ta e="T742" id="Seg_6217" s="T741">dĭ-zen</ta>
            <ta e="T743" id="Seg_6218" s="T742">dĭn</ta>
            <ta e="T744" id="Seg_6219" s="T743">kuʔ-pi-ʔi</ta>
            <ta e="T745" id="Seg_6220" s="T744">sɨne-ʔi</ta>
            <ta e="T746" id="Seg_6221" s="T745">i</ta>
            <ta e="T747" id="Seg_6222" s="T746">uja</ta>
            <ta e="T748" id="Seg_6223" s="T747">tustʼar-bi-ʔi</ta>
            <ta e="T749" id="Seg_6224" s="T748">dĭgəttə</ta>
            <ta e="T750" id="Seg_6225" s="T749">edə-bi-ʔi</ta>
            <ta e="T751" id="Seg_6226" s="T750">dĭ</ta>
            <ta e="T753" id="Seg_6227" s="T752">ko-lam-bi</ta>
            <ta e="T754" id="Seg_6228" s="T753">baltu-zi</ta>
            <ta e="T755" id="Seg_6229" s="T754">jaʔ-pi-ʔi</ta>
            <ta e="T756" id="Seg_6230" s="T755">bura-nə</ta>
            <ta e="T757" id="Seg_6231" s="T756">kămnə-bi-ʔi</ta>
            <ta e="T759" id="Seg_6232" s="T758">dĭ-zeŋ</ta>
            <ta e="T760" id="Seg_6233" s="T759">dĭgəttə</ta>
            <ta e="T761" id="Seg_6234" s="T760">kola</ta>
            <ta e="T762" id="Seg_6235" s="T761">dʼaʔ-pi-ʔi</ta>
            <ta e="T763" id="Seg_6236" s="T762">i</ta>
            <ta e="T764" id="Seg_6237" s="T763">tustʼar-bi-ʔi</ta>
            <ta e="T765" id="Seg_6238" s="T764">i</ta>
            <ta e="T766" id="Seg_6239" s="T765">tože</ta>
            <ta e="T767" id="Seg_6240" s="T766">kola</ta>
            <ta e="T769" id="Seg_6241" s="T767">koʔ-pi-ʔi</ta>
            <ta e="T772" id="Seg_6242" s="T771">dĭgəttə</ta>
            <ta e="T773" id="Seg_6243" s="T772">kuba-t-si</ta>
            <ta e="T774" id="Seg_6244" s="T773">a-bi-ʔi</ta>
            <ta e="T775" id="Seg_6245" s="T774">bura-ʔi</ta>
            <ta e="T776" id="Seg_6246" s="T775">i</ta>
            <ta e="T777" id="Seg_6247" s="T776">dĭbər</ta>
            <ta e="T778" id="Seg_6248" s="T777">em-bi-ʔi</ta>
            <ta e="T779" id="Seg_6249" s="T778">uja</ta>
            <ta e="T780" id="Seg_6250" s="T779">i</ta>
            <ta e="T781" id="Seg_6251" s="T780">kola</ta>
            <ta e="T782" id="Seg_6252" s="T781">i</ta>
            <ta e="T783" id="Seg_6253" s="T782">ălenʼə-ʔi-zi</ta>
            <ta e="T784" id="Seg_6254" s="T783">deʔ-pi-ʔi</ta>
            <ta e="T785" id="Seg_6255" s="T784">maʔ-nə</ta>
            <ta e="T786" id="Seg_6256" s="T785">döber</ta>
            <ta e="T787" id="Seg_6257" s="T786">gijen</ta>
            <ta e="T788" id="Seg_6258" s="T787">amno-bi-ʔi</ta>
            <ta e="T789" id="Seg_6259" s="T788">kamen</ta>
            <ta e="T790" id="Seg_6260" s="T789">šišəge</ta>
            <ta e="T791" id="Seg_6261" s="T790">i-bi</ta>
            <ta e="T793" id="Seg_6262" s="T792">dĭ-zeŋ</ta>
            <ta e="T795" id="Seg_6263" s="T794">dĭrgid-əʔi</ta>
            <ta e="T796" id="Seg_6264" s="T795">men-zeŋ-də</ta>
            <ta e="T798" id="Seg_6265" s="T797">i-bi-ʔi</ta>
            <ta e="T800" id="Seg_6266" s="T799">ka-la-ʔi</ta>
            <ta e="T801" id="Seg_6267" s="T800">poʔto</ta>
            <ta e="T802" id="Seg_6268" s="T801">iʔgö</ta>
            <ta e="T803" id="Seg_6269" s="T802">dʼabə-lu-ʔi</ta>
            <ta e="T804" id="Seg_6270" s="T803">dĭgəttə</ta>
            <ta e="T805" id="Seg_6271" s="T804">maʔ-ndə</ta>
            <ta e="T806" id="Seg_6272" s="T805">šo-lə-j</ta>
            <ta e="T807" id="Seg_6273" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_6274" s="T807">ine</ta>
            <ta e="T809" id="Seg_6275" s="T808">koler-lə-j</ta>
            <ta e="T810" id="Seg_6276" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_6277" s="T810">kandə-ga</ta>
            <ta e="T812" id="Seg_6278" s="T811">poʔto</ta>
            <ta e="T813" id="Seg_6279" s="T812">oʔbdə-sʼtə</ta>
            <ta e="T814" id="Seg_6280" s="T813">dĭgəttə</ta>
            <ta e="T815" id="Seg_6281" s="T814">šo-lə-j</ta>
            <ta e="T816" id="Seg_6282" s="T815">maʔ-ndə</ta>
            <ta e="T818" id="Seg_6283" s="T817">i</ta>
            <ta e="T819" id="Seg_6284" s="T818">poʔto</ta>
            <ta e="T820" id="Seg_6285" s="T819">det-lə-j</ta>
            <ta e="T823" id="Seg_6286" s="T822">dĭ-zem</ta>
            <ta e="T825" id="Seg_6287" s="T824">armija-nə</ta>
            <ta e="T826" id="Seg_6288" s="T825">ej</ta>
            <ta e="T827" id="Seg_6289" s="T826">i-bi-ʔi</ta>
            <ta e="T828" id="Seg_6290" s="T827">dĭ-zeŋ</ta>
            <ta e="T829" id="Seg_6291" s="T828">ĭsak</ta>
            <ta e="T830" id="Seg_6292" s="T829">aktʼa</ta>
            <ta e="T831" id="Seg_6293" s="T830">aktʼa</ta>
            <ta e="T832" id="Seg_6294" s="T831">mĭ-bi-ʔi</ta>
            <ta e="T833" id="Seg_6295" s="T832">i</ta>
            <ta e="T834" id="Seg_6296" s="T833">albuga</ta>
            <ta e="T835" id="Seg_6297" s="T834">mĭ-bi-ʔi</ta>
            <ta e="T837" id="Seg_6298" s="T836">dĭ-zeŋ</ta>
            <ta e="T838" id="Seg_6299" s="T837">ĭsak-tə</ta>
            <ta e="T839" id="Seg_6300" s="T838">mĭm-bi-ʔi</ta>
            <ta e="T840" id="Seg_6301" s="T839">aktʼa</ta>
            <ta e="T841" id="Seg_6302" s="T840">bʼeʔ</ta>
            <ta e="T842" id="Seg_6303" s="T841">bʼeʔ</ta>
            <ta e="T843" id="Seg_6304" s="T842">sumna</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6305" s="T0">kamən</ta>
            <ta e="T2" id="Seg_6306" s="T1">miʔnʼibeʔ</ta>
            <ta e="T3" id="Seg_6307" s="T2">šo-bi-l</ta>
            <ta e="T4" id="Seg_6308" s="T3">urgo</ta>
            <ta e="T5" id="Seg_6309" s="T4">tʼala-Kən</ta>
            <ta e="T6" id="Seg_6310" s="T5">šide</ta>
            <ta e="T7" id="Seg_6311" s="T6">tʼala</ta>
            <ta e="T850" id="Seg_6312" s="T7">Kazan</ta>
            <ta e="T8" id="Seg_6313" s="T850">tura-Kən</ta>
            <ta e="T9" id="Seg_6314" s="T8">amnə-bi-m</ta>
            <ta e="T10" id="Seg_6315" s="T9">ĭmbi</ta>
            <ta e="T11" id="Seg_6316" s="T10">ĭmbi</ta>
            <ta e="T12" id="Seg_6317" s="T11">ĭmbi</ta>
            <ta e="T13" id="Seg_6318" s="T12">ku-bi-l</ta>
            <ta e="T14" id="Seg_6319" s="T13">dĭgəttə</ta>
            <ta e="T15" id="Seg_6320" s="T14">döbər</ta>
            <ta e="T16" id="Seg_6321" s="T15">šo-bi-m</ta>
            <ta e="T18" id="Seg_6322" s="T17">măndo-r-bi-m</ta>
            <ta e="T19" id="Seg_6323" s="T18">măndo-r-bi-m</ta>
            <ta e="T20" id="Seg_6324" s="T19">kădaʔ=də</ta>
            <ta e="T21" id="Seg_6325" s="T20">ej</ta>
            <ta e="T22" id="Seg_6326" s="T21">mo-liA-m</ta>
            <ta e="T23" id="Seg_6327" s="T22">ej</ta>
            <ta e="T24" id="Seg_6328" s="T23">mo-liA-m</ta>
            <ta e="T25" id="Seg_6329" s="T24">šo-zittə</ta>
            <ta e="T26" id="Seg_6330" s="T25">döbər</ta>
            <ta e="T27" id="Seg_6331" s="T26">nagur</ta>
            <ta e="T28" id="Seg_6332" s="T27">tʼala-Kən</ta>
            <ta e="T29" id="Seg_6333" s="T28">dĭgəttə</ta>
            <ta e="T30" id="Seg_6334" s="T29">šo-bi-m</ta>
            <ta e="T31" id="Seg_6335" s="T30">döbər</ta>
            <ta e="T33" id="Seg_6336" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_6337" s="T33">döbər</ta>
            <ta e="T35" id="Seg_6338" s="T34">šo-bi-m</ta>
            <ta e="T36" id="Seg_6339" s="T35">urgaja-Tə</ta>
            <ta e="T37" id="Seg_6340" s="T36">šo-bi-m</ta>
            <ta e="T38" id="Seg_6341" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_6342" s="T38">măna</ta>
            <ta e="T40" id="Seg_6343" s="T39">bar</ta>
            <ta e="T41" id="Seg_6344" s="T40">bădə-bi</ta>
            <ta e="T42" id="Seg_6345" s="T41">mĭje</ta>
            <ta e="T43" id="Seg_6346" s="T42">mĭ-bi</ta>
            <ta e="T45" id="Seg_6347" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_6348" s="T45">döbər</ta>
            <ta e="T47" id="Seg_6349" s="T46">šo-bi-m</ta>
            <ta e="T48" id="Seg_6350" s="T47">büzəj</ta>
            <ta e="T49" id="Seg_6351" s="T48">kirgaːr-laʔbə</ta>
            <ta e="T50" id="Seg_6352" s="T49">nüke</ta>
            <ta e="T51" id="Seg_6353" s="T50">bar</ta>
            <ta e="T52" id="Seg_6354" s="T51">mĭ-bi</ta>
            <ta e="T53" id="Seg_6355" s="T52">dĭ-Tə</ta>
            <ta e="T54" id="Seg_6356" s="T53">toltano</ta>
            <ta e="T55" id="Seg_6357" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_6358" s="T55">amor-laʔbə</ta>
            <ta e="T57" id="Seg_6359" s="T56">ej</ta>
            <ta e="T58" id="Seg_6360" s="T57">kirgaːr-liA</ta>
            <ta e="T60" id="Seg_6361" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_6362" s="T60">nüke-Kən</ta>
            <ta e="T62" id="Seg_6363" s="T61">tura-Kən</ta>
            <ta e="T63" id="Seg_6364" s="T62">šišəge</ta>
            <ta e="T64" id="Seg_6365" s="T63">pʼeːš</ta>
            <ta e="T65" id="Seg_6366" s="T64">nendə-bi</ta>
            <ta e="T67" id="Seg_6367" s="T66">nʼiʔnen</ta>
            <ta e="T68" id="Seg_6368" s="T67">bar</ta>
            <ta e="T69" id="Seg_6369" s="T68">šišəge</ta>
            <ta e="T70" id="Seg_6370" s="T69">ugaːndə</ta>
            <ta e="T71" id="Seg_6371" s="T70">büzəj</ta>
            <ta e="T72" id="Seg_6372" s="T71">tura-Kən</ta>
            <ta e="T73" id="Seg_6373" s="T72">nu-gA</ta>
            <ta e="T74" id="Seg_6374" s="T73">bar</ta>
            <ta e="T76" id="Seg_6375" s="T75">tüžöj</ta>
            <ta e="T77" id="Seg_6376" s="T76">tüžöj</ta>
            <ta e="T78" id="Seg_6377" s="T77">nʼe-luʔbdə-bi</ta>
            <ta e="T79" id="Seg_6378" s="T78">bar</ta>
            <ta e="T80" id="Seg_6379" s="T79">üdʼüge</ta>
            <ta e="T81" id="Seg_6380" s="T80">büzəj</ta>
            <ta e="T83" id="Seg_6381" s="T82">miʔ</ta>
            <ta e="T84" id="Seg_6382" s="T83">tura-Kən</ta>
            <ta e="T85" id="Seg_6383" s="T84">ugaːndə</ta>
            <ta e="T86" id="Seg_6384" s="T85">iʔgö</ta>
            <ta e="T87" id="Seg_6385" s="T86">il</ta>
            <ta e="T88" id="Seg_6386" s="T87">kü-bi-jəʔ</ta>
            <ta e="T90" id="Seg_6387" s="T89">mĭn-bi-m</ta>
            <ta e="T91" id="Seg_6388" s="T90">nüke-Tə</ta>
            <ta e="T92" id="Seg_6389" s="T91">dĭ</ta>
            <ta e="T94" id="Seg_6390" s="T93">ugaːndə</ta>
            <ta e="T95" id="Seg_6391" s="T94">ĭzem-liA</ta>
            <ta e="T96" id="Seg_6392" s="T95">măn</ta>
            <ta e="T97" id="Seg_6393" s="T96">teinen</ta>
            <ta e="T99" id="Seg_6394" s="T98">kü-laːm-lV-m</ta>
            <ta e="T100" id="Seg_6395" s="T99">no</ta>
            <ta e="T101" id="Seg_6396" s="T100">kü-ʔ</ta>
            <ta e="T102" id="Seg_6397" s="T101">no</ta>
            <ta e="T103" id="Seg_6398" s="T102">kü-ʔ</ta>
            <ta e="T105" id="Seg_6399" s="T104">karəldʼaːn</ta>
            <ta e="T107" id="Seg_6400" s="T106">šo-lV-m</ta>
            <ta e="T108" id="Seg_6401" s="T107">bazə-zittə</ta>
            <ta e="T109" id="Seg_6402" s="T108">tănan</ta>
            <ta e="T110" id="Seg_6403" s="T109">dĭgəttə</ta>
            <ta e="T111" id="Seg_6404" s="T110">ej</ta>
            <ta e="T112" id="Seg_6405" s="T111">kan-bi-m</ta>
            <ta e="T113" id="Seg_6406" s="T112">noʔ</ta>
            <ta e="T114" id="Seg_6407" s="T113">det-bi-jəʔ</ta>
            <ta e="T115" id="Seg_6408" s="T114">šide</ta>
            <ta e="T116" id="Seg_6409" s="T115">tibi</ta>
            <ta e="T117" id="Seg_6410" s="T116">dĭgəttə</ta>
            <ta e="T118" id="Seg_6411" s="T117">šo-bi-m</ta>
            <ta e="T119" id="Seg_6412" s="T118">ĭmbi</ta>
            <ta e="T120" id="Seg_6413" s="T119">kondʼo</ta>
            <ta e="T121" id="Seg_6414" s="T120">ej</ta>
            <ta e="T122" id="Seg_6415" s="T121">šo-bi-l</ta>
            <ta e="T123" id="Seg_6416" s="T122">a</ta>
            <ta e="T124" id="Seg_6417" s="T123">măn</ta>
            <ta e="T125" id="Seg_6418" s="T124">tene-bi-m</ta>
            <ta e="T126" id="Seg_6419" s="T125">tăn</ta>
            <ta e="T127" id="Seg_6420" s="T126">kü-laːm-bi-m</ta>
            <ta e="T128" id="Seg_6421" s="T127">bazə-zittə</ta>
            <ta e="T129" id="Seg_6422" s="T128">šo-bi-m</ta>
            <ta e="T131" id="Seg_6423" s="T130">büzəj</ta>
            <ta e="T132" id="Seg_6424" s="T131">nom-laʔbə</ta>
            <ta e="T133" id="Seg_6425" s="T132">ej</ta>
            <ta e="T134" id="Seg_6426" s="T133">kirgaːr-laʔbə</ta>
            <ta e="T136" id="Seg_6427" s="T135">ešši-zAŋ</ta>
            <ta e="T137" id="Seg_6428" s="T136">bar</ta>
            <ta e="T138" id="Seg_6429" s="T137">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T139" id="Seg_6430" s="T138">tura-n</ta>
            <ta e="T141" id="Seg_6431" s="T140">tura-n</ta>
            <ta e="T142" id="Seg_6432" s="T141">toʔ-gəndə</ta>
            <ta e="T144" id="Seg_6433" s="T143">šide</ta>
            <ta e="T145" id="Seg_6434" s="T144">nʼi</ta>
            <ta e="T146" id="Seg_6435" s="T145">šo-bi-jəʔ</ta>
            <ta e="T147" id="Seg_6436" s="T146">nʼilgö-zittə</ta>
            <ta e="T150" id="Seg_6437" s="T149">e-ʔ</ta>
            <ta e="T151" id="Seg_6438" s="T150">kirgaːr-KAʔ</ta>
            <ta e="T152" id="Seg_6439" s="T151">e-ʔ</ta>
            <ta e="T153" id="Seg_6440" s="T152">alom-KAʔ</ta>
            <ta e="T154" id="Seg_6441" s="T153">nʼilgö-lAʔ</ta>
            <ta e="T156" id="Seg_6442" s="T155">ija-l</ta>
            <ta e="T157" id="Seg_6443" s="T156">kamən</ta>
            <ta e="T158" id="Seg_6444" s="T157">šo-lV-j</ta>
            <ta e="T159" id="Seg_6445" s="T158">büžü</ta>
            <ta e="T160" id="Seg_6446" s="T159">šo-lV-j</ta>
            <ta e="T162" id="Seg_6447" s="T161">nadə</ta>
            <ta e="T163" id="Seg_6448" s="T162">bü</ta>
            <ta e="T164" id="Seg_6449" s="T163">ejü-m-zittə</ta>
            <ta e="T165" id="Seg_6450" s="T164">dĭgəttə</ta>
            <ta e="T166" id="Seg_6451" s="T165">tüžöj</ta>
            <ta e="T167" id="Seg_6452" s="T166">bĭs-lə-zittə</ta>
            <ta e="T169" id="Seg_6453" s="T168">ĭmbi</ta>
            <ta e="T170" id="Seg_6454" s="T169">nʼilgö-zittə</ta>
            <ta e="T171" id="Seg_6455" s="T170">šo-bi-lAʔ</ta>
            <ta e="T172" id="Seg_6456" s="T171">măndo-r-zittə</ta>
            <ta e="T173" id="Seg_6457" s="T172">šo-bi-lAʔ</ta>
            <ta e="T175" id="Seg_6458" s="T174">ej</ta>
            <ta e="T176" id="Seg_6459" s="T175">kürüm-liA-jəʔ</ta>
            <ta e="T177" id="Seg_6460" s="T176">kuvas</ta>
            <ta e="T178" id="Seg_6461" s="T177">nʼi-zAŋ</ta>
            <ta e="T179" id="Seg_6462" s="T178">ej</ta>
            <ta e="T180" id="Seg_6463" s="T179">kürüm-liA-jəʔ</ta>
            <ta e="T181" id="Seg_6464" s="T180">nu-laʔbə-jəʔ</ta>
            <ta e="T182" id="Seg_6465" s="T181">jakšə-jəʔ</ta>
            <ta e="T183" id="Seg_6466" s="T182">ugaːndə</ta>
            <ta e="T185" id="Seg_6467" s="T184">sanə</ta>
            <ta e="T186" id="Seg_6468" s="T185">teinen</ta>
            <ta e="T187" id="Seg_6469" s="T186">mĭ-bi-m</ta>
            <ta e="T188" id="Seg_6470" s="T187">tănan</ta>
            <ta e="T189" id="Seg_6471" s="T188">lej-zittə</ta>
            <ta e="T191" id="Seg_6472" s="T190">sima-t</ta>
            <ta e="T192" id="Seg_6473" s="T191">măndo-r-zittə</ta>
            <ta e="T194" id="Seg_6474" s="T193">ku-zAŋ-də</ta>
            <ta e="T195" id="Seg_6475" s="T194">nʼilgö-zittə</ta>
            <ta e="T196" id="Seg_6476" s="T195">aŋ-də</ta>
            <ta e="T197" id="Seg_6477" s="T196">am-zittə</ta>
            <ta e="T198" id="Seg_6478" s="T197">tĭme-jəʔ</ta>
            <ta e="T199" id="Seg_6479" s="T198">talbə-zittə</ta>
            <ta e="T200" id="Seg_6480" s="T199">ipek</ta>
            <ta e="T202" id="Seg_6481" s="T201">tenö-bi-m</ta>
            <ta e="T203" id="Seg_6482" s="T202">tenö-bi-m</ta>
            <ta e="T204" id="Seg_6483" s="T203">kădaʔ</ta>
            <ta e="T205" id="Seg_6484" s="T204">döbər</ta>
            <ta e="T206" id="Seg_6485" s="T205">šo-zittə</ta>
            <ta e="T208" id="Seg_6486" s="T207">kuba</ta>
            <ta e="T209" id="Seg_6487" s="T208">tʼo-Kən</ta>
            <ta e="T210" id="Seg_6488" s="T209">iʔbö-laʔbə-bi</ta>
            <ta e="T211" id="Seg_6489" s="T210">ipek</ta>
            <ta e="T212" id="Seg_6490" s="T211">hen-bi-jəʔ</ta>
            <ta e="T213" id="Seg_6491" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_6492" s="T213">amor-bi-jəʔ</ta>
            <ta e="T215" id="Seg_6493" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_6494" s="T215">ipek</ta>
            <ta e="T217" id="Seg_6495" s="T216">i</ta>
            <ta e="T218" id="Seg_6496" s="T217">uja</ta>
            <ta e="T222" id="Seg_6497" s="T221">dĭgəttə</ta>
            <ta e="T223" id="Seg_6498" s="T222">am-bi-jəʔ</ta>
            <ta e="T224" id="Seg_6499" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_6500" s="T224">kudaj-Tə</ta>
            <ta e="T226" id="Seg_6501" s="T225">numan üzə-bi-jəʔ</ta>
            <ta e="T851" id="Seg_6502" s="T227">Kazan</ta>
            <ta e="T228" id="Seg_6503" s="T851">tura-Tə</ta>
            <ta e="T229" id="Seg_6504" s="T228">mĭn-bi-m</ta>
            <ta e="T230" id="Seg_6505" s="T229">tʼeger-Kən</ta>
            <ta e="T231" id="Seg_6506" s="T230">tʼeger-Kən</ta>
            <ta e="T233" id="Seg_6507" s="T232">kudaj</ta>
            <ta e="T235" id="Seg_6508" s="T234">numan üzə-bi-m</ta>
            <ta e="T237" id="Seg_6509" s="T236">iʔgö</ta>
            <ta e="T238" id="Seg_6510" s="T237">il</ta>
            <ta e="T239" id="Seg_6511" s="T238">kü-laːm-bi-jəʔ</ta>
            <ta e="T241" id="Seg_6512" s="T240">krospa-jəʔ</ta>
            <ta e="T242" id="Seg_6513" s="T241">ugaːndə</ta>
            <ta e="T243" id="Seg_6514" s="T242">iʔgö</ta>
            <ta e="T244" id="Seg_6515" s="T243">nu-gA-jəʔ</ta>
            <ta e="T245" id="Seg_6516" s="T244">bar</ta>
            <ta e="T247" id="Seg_6517" s="T246">miʔ</ta>
            <ta e="T248" id="Seg_6518" s="T247">koʔbdo</ta>
            <ta e="T249" id="Seg_6519" s="T248">ĭzem-bi</ta>
            <ta e="T250" id="Seg_6520" s="T249">a</ta>
            <ta e="T251" id="Seg_6521" s="T250">aba-t</ta>
            <ta e="T253" id="Seg_6522" s="T252">kun-bi</ta>
            <ta e="T255" id="Seg_6523" s="T254">tʼezer-zittə</ta>
            <ta e="T258" id="Seg_6524" s="T257">miʔnʼibeʔ</ta>
            <ta e="T259" id="Seg_6525" s="T258">teinen</ta>
            <ta e="T260" id="Seg_6526" s="T259">nüdʼi-n</ta>
            <ta e="T261" id="Seg_6527" s="T260">tibi</ta>
            <ta e="T262" id="Seg_6528" s="T261">ne-ziʔ</ta>
            <ta e="T263" id="Seg_6529" s="T262">bar</ta>
            <ta e="T264" id="Seg_6530" s="T263">tʼabəro-bi-jəʔ</ta>
            <ta e="T265" id="Seg_6531" s="T264">uda-Kən</ta>
            <ta e="T266" id="Seg_6532" s="T265">bar</ta>
            <ta e="T269" id="Seg_6533" s="T268">kem</ta>
            <ta e="T270" id="Seg_6534" s="T269">mʼaŋ-laʔbə</ta>
            <ta e="T271" id="Seg_6535" s="T270">ešši-zAŋ-Tə</ta>
            <ta e="T272" id="Seg_6536" s="T271">bar</ta>
            <ta e="T273" id="Seg_6537" s="T272">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T274" id="Seg_6538" s="T273">măn</ta>
            <ta e="T275" id="Seg_6539" s="T274">šo-bi-m</ta>
            <ta e="T276" id="Seg_6540" s="T275">bar</ta>
            <ta e="T277" id="Seg_6541" s="T276">kudo-nzə-bi-m</ta>
            <ta e="T279" id="Seg_6542" s="T278">büzəj</ta>
            <ta e="T280" id="Seg_6543" s="T279">iʔbö-bi</ta>
            <ta e="T281" id="Seg_6544" s="T280">ej</ta>
            <ta e="T282" id="Seg_6545" s="T281">kirgaːr-liA</ta>
            <ta e="T283" id="Seg_6546" s="T282">tüj</ta>
            <ta e="T285" id="Seg_6547" s="T284">možna</ta>
            <ta e="T286" id="Seg_6548" s="T285">tʼăbaktər-zittə</ta>
            <ta e="T288" id="Seg_6549" s="T287">büzʼe-n</ta>
            <ta e="T289" id="Seg_6550" s="T288">bar</ta>
            <ta e="T290" id="Seg_6551" s="T289">püje-t</ta>
            <ta e="T291" id="Seg_6552" s="T290">dʼükəm-bi</ta>
            <ta e="T292" id="Seg_6553" s="T291">gijen=də</ta>
            <ta e="T293" id="Seg_6554" s="T292">ara</ta>
            <ta e="T294" id="Seg_6555" s="T293">bĭs-lV-m</ta>
            <ta e="T295" id="Seg_6556" s="T294">dĭgəttə</ta>
            <ta e="T296" id="Seg_6557" s="T295">pograšsʼe</ta>
            <ta e="T297" id="Seg_6558" s="T296">a-bi</ta>
            <ta e="T298" id="Seg_6559" s="T297">amnə-bi</ta>
            <ta e="T299" id="Seg_6560" s="T298">pa-Kən</ta>
            <ta e="T300" id="Seg_6561" s="T299">a</ta>
            <ta e="T301" id="Seg_6562" s="T300">măn</ta>
            <ta e="T302" id="Seg_6563" s="T301">urgaja-m</ta>
            <ta e="T303" id="Seg_6564" s="T302">mĭn-bi</ta>
            <ta e="T852" id="Seg_6565" s="T303">Kazan</ta>
            <ta e="T304" id="Seg_6566" s="T852">tura-Tə</ta>
            <ta e="T305" id="Seg_6567" s="T304">i</ta>
            <ta e="T306" id="Seg_6568" s="T305">ara</ta>
            <ta e="T307" id="Seg_6569" s="T306">det-bi</ta>
            <ta e="T308" id="Seg_6570" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_6571" s="T308">amnə-bi</ta>
            <ta e="T310" id="Seg_6572" s="T309">tüʔ-zittə</ta>
            <ta e="T312" id="Seg_6573" s="T311">dĭ-Tə</ta>
            <ta e="T313" id="Seg_6574" s="T312">köten-ziʔ</ta>
            <ta e="T314" id="Seg_6575" s="T313">dĭgəttə</ta>
            <ta e="T315" id="Seg_6576" s="T314">dĭ</ta>
            <ta e="T316" id="Seg_6577" s="T315">šo-bi</ta>
            <ta e="T317" id="Seg_6578" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_6579" s="T317">dĭ-Tə</ta>
            <ta e="T319" id="Seg_6580" s="T318">ara</ta>
            <ta e="T320" id="Seg_6581" s="T319">mĭ-bi</ta>
            <ta e="T321" id="Seg_6582" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_6583" s="T321">bĭs-bi</ta>
            <ta e="T323" id="Seg_6584" s="T322">i</ta>
            <ta e="T324" id="Seg_6585" s="T323">măn-ntə</ta>
            <ta e="T325" id="Seg_6586" s="T324">măn-ntə</ta>
            <ta e="T326" id="Seg_6587" s="T325">dĭ-Tə</ta>
            <ta e="T327" id="Seg_6588" s="T326">ne</ta>
            <ta e="T328" id="Seg_6589" s="T327">tak</ta>
            <ta e="T329" id="Seg_6590" s="T328">ne</ta>
            <ta e="T330" id="Seg_6591" s="T329">vot</ta>
            <ta e="T331" id="Seg_6592" s="T330">tak</ta>
            <ta e="T332" id="Seg_6593" s="T331">ne</ta>
            <ta e="T333" id="Seg_6594" s="T332">tak</ta>
            <ta e="T334" id="Seg_6595" s="T333">ne</ta>
            <ta e="T335" id="Seg_6596" s="T334">ĭmbi</ta>
            <ta e="T336" id="Seg_6597" s="T335">tăn</ta>
            <ta e="T337" id="Seg_6598" s="T336">dărəʔ</ta>
            <ta e="T340" id="Seg_6599" s="T339">a</ta>
            <ta e="T341" id="Seg_6600" s="T340">tăn</ta>
            <ta e="T342" id="Seg_6601" s="T341">măna</ta>
            <ta e="T344" id="Seg_6602" s="T343">köten-ziʔ</ta>
            <ta e="T345" id="Seg_6603" s="T344">amno-bi-l</ta>
            <ta e="T346" id="Seg_6604" s="T345">i</ta>
            <ta e="T347" id="Seg_6605" s="T346">tüʔ-bi-l</ta>
            <ta e="T350" id="Seg_6606" s="T349">măn</ta>
            <ta e="T351" id="Seg_6607" s="T350">urgaja-m</ta>
            <ta e="T352" id="Seg_6608" s="T351">šaman-də</ta>
            <ta e="T353" id="Seg_6609" s="T352">mĭn-bi-m</ta>
            <ta e="T354" id="Seg_6610" s="T353">dĭn</ta>
            <ta e="T355" id="Seg_6611" s="T354">bar</ta>
            <ta e="T357" id="Seg_6612" s="T356">uja</ta>
            <ta e="T358" id="Seg_6613" s="T357">mĭnzər-bi-jəʔ</ta>
            <ta e="T359" id="Seg_6614" s="T358">takše-Tə</ta>
            <ta e="T360" id="Seg_6615" s="T359">urgo</ta>
            <ta e="T361" id="Seg_6616" s="T360">takše</ta>
            <ta e="T362" id="Seg_6617" s="T361">hen-bi-jəʔ</ta>
            <ta e="T363" id="Seg_6618" s="T362">dĭ</ta>
            <ta e="T364" id="Seg_6619" s="T363">bar</ta>
            <ta e="T365" id="Seg_6620" s="T364">šišəge</ta>
            <ta e="T366" id="Seg_6621" s="T365">mo-laːm-bi</ta>
            <ta e="T367" id="Seg_6622" s="T366">amnə-ʔ</ta>
            <ta e="T369" id="Seg_6623" s="T368">amor-zittə</ta>
            <ta e="T370" id="Seg_6624" s="T369">dĭ</ta>
            <ta e="T371" id="Seg_6625" s="T370">bar</ta>
            <ta e="T372" id="Seg_6626" s="T371">uja</ta>
            <ta e="T373" id="Seg_6627" s="T372">i-bi</ta>
            <ta e="T374" id="Seg_6628" s="T373">dĭ</ta>
            <ta e="T375" id="Seg_6629" s="T374">ugaːndə</ta>
            <ta e="T376" id="Seg_6630" s="T375">šišəge</ta>
            <ta e="T377" id="Seg_6631" s="T376">dĭgəttə</ta>
            <ta e="T378" id="Seg_6632" s="T377">men-də</ta>
            <ta e="T380" id="Seg_6633" s="T379">baʔbdə-luʔbdə-bi</ta>
            <ta e="T382" id="Seg_6634" s="T381">buben-ziʔ</ta>
            <ta e="T383" id="Seg_6635" s="T382">bar</ta>
            <ta e="T385" id="Seg_6636" s="T384">sʼar-bi</ta>
            <ta e="T386" id="Seg_6637" s="T385">bar</ta>
            <ta e="T387" id="Seg_6638" s="T386">il-də</ta>
            <ta e="T388" id="Seg_6639" s="T387">mĭn-bi</ta>
            <ta e="T389" id="Seg_6640" s="T388">a</ta>
            <ta e="T390" id="Seg_6641" s="T389">măn</ta>
            <ta e="T391" id="Seg_6642" s="T390">urgaja-Tə</ta>
            <ta e="T392" id="Seg_6643" s="T391">ej</ta>
            <ta e="T393" id="Seg_6644" s="T392">mĭ-bi-m</ta>
            <ta e="T394" id="Seg_6645" s="T393">dĭgəttə</ta>
            <ta e="T395" id="Seg_6646" s="T394">mĭn-bi</ta>
            <ta e="T396" id="Seg_6647" s="T395">e-ʔ</ta>
            <ta e="T397" id="Seg_6648" s="T396">det-KAʔ</ta>
            <ta e="T398" id="Seg_6649" s="T397">dĭ</ta>
            <ta e="T399" id="Seg_6650" s="T398">ne</ta>
            <ta e="T400" id="Seg_6651" s="T399">kazak</ta>
            <ta e="T401" id="Seg_6652" s="T400">ne</ta>
            <ta e="T404" id="Seg_6653" s="T403">šaman-Kən</ta>
            <ta e="T405" id="Seg_6654" s="T404">bar</ta>
            <ta e="T406" id="Seg_6655" s="T405">parga</ta>
            <ta e="T407" id="Seg_6656" s="T406">i-bi</ta>
            <ta e="T411" id="Seg_6657" s="T410">bar</ta>
            <ta e="T413" id="Seg_6658" s="T412">kuriza-n</ta>
            <ta e="T414" id="Seg_6659" s="T413">kuba</ta>
            <ta e="T415" id="Seg_6660" s="T414">albuga-n</ta>
            <ta e="T416" id="Seg_6661" s="T415">kuba</ta>
            <ta e="T417" id="Seg_6662" s="T416">parga-Kən</ta>
            <ta e="T418" id="Seg_6663" s="T417">tažəp-ə-n</ta>
            <ta e="T420" id="Seg_6664" s="T419">kuba</ta>
            <ta e="T422" id="Seg_6665" s="T420">parga-Kən</ta>
            <ta e="T423" id="Seg_6666" s="T422">poʔto-n</ta>
            <ta e="T424" id="Seg_6667" s="T423">kuba</ta>
            <ta e="T426" id="Seg_6668" s="T425">dĭ</ta>
            <ta e="T427" id="Seg_6669" s="T426">šaman</ta>
            <ta e="T428" id="Seg_6670" s="T427">bar</ta>
            <ta e="T432" id="Seg_6671" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_6672" s="T432">tagaj</ta>
            <ta e="T434" id="Seg_6673" s="T433">i-bi</ta>
            <ta e="T435" id="Seg_6674" s="T434">da</ta>
            <ta e="T436" id="Seg_6675" s="T435">bar</ta>
            <ta e="T437" id="Seg_6676" s="T436">müʔbdə-bi</ta>
            <ta e="T439" id="Seg_6677" s="T438">sĭj-də</ta>
            <ta e="T440" id="Seg_6678" s="T439">i</ta>
            <ta e="T441" id="Seg_6679" s="T440">dĭgəttə</ta>
            <ta e="T442" id="Seg_6680" s="T441">saʔmə-luʔbdə-bi</ta>
            <ta e="T443" id="Seg_6681" s="T442">a</ta>
            <ta e="T444" id="Seg_6682" s="T443">il</ta>
            <ta e="T445" id="Seg_6683" s="T444">iʔgö</ta>
            <ta e="T446" id="Seg_6684" s="T445">šonə-gA-jəʔ</ta>
            <ta e="T457" id="Seg_6685" s="T456">tibi-jəʔ</ta>
            <ta e="T460" id="Seg_6686" s="T459">kamən</ta>
            <ta e="T461" id="Seg_6687" s="T460">kan-lV-jəʔ</ta>
            <ta e="T462" id="Seg_6688" s="T461">dʼije-Tə</ta>
            <ta e="T463" id="Seg_6689" s="T462">dĭgəttə</ta>
            <ta e="T464" id="Seg_6690" s="T463">šaman-Tə</ta>
            <ta e="T465" id="Seg_6691" s="T464">kan-lV-jəʔ</ta>
            <ta e="T466" id="Seg_6692" s="T465">dĭ</ta>
            <ta e="T467" id="Seg_6693" s="T466">măn-lV-j</ta>
            <ta e="T468" id="Seg_6694" s="T467">tăn</ta>
            <ta e="T470" id="Seg_6695" s="T469">albuga</ta>
            <ta e="T471" id="Seg_6696" s="T470">kut-lV-l</ta>
            <ta e="T472" id="Seg_6697" s="T471">a</ta>
            <ta e="T473" id="Seg_6698" s="T472">šində-Tə</ta>
            <ta e="T474" id="Seg_6699" s="T473">măn-lV-j</ta>
            <ta e="T475" id="Seg_6700" s="T474">ej</ta>
            <ta e="T476" id="Seg_6701" s="T475">kut-lV-l</ta>
            <ta e="T479" id="Seg_6702" s="T478">šaman</ta>
            <ta e="T480" id="Seg_6703" s="T479">i</ta>
            <ta e="T481" id="Seg_6704" s="T480">miʔ</ta>
            <ta e="T482" id="Seg_6705" s="T481">šaman</ta>
            <ta e="T483" id="Seg_6706" s="T482">bar</ta>
            <ta e="T484" id="Seg_6707" s="T483">tʼabəro-bi-jəʔ</ta>
            <ta e="T487" id="Seg_6708" s="T486">kan-bi</ta>
            <ta e="T488" id="Seg_6709" s="T487">maʔ-gəndə</ta>
            <ta e="T489" id="Seg_6710" s="T488">a</ta>
            <ta e="T490" id="Seg_6711" s="T489">miʔ</ta>
            <ta e="T491" id="Seg_6712" s="T490">šaman</ta>
            <ta e="T493" id="Seg_6713" s="T492">dĭ-m</ta>
            <ta e="T494" id="Seg_6714" s="T493">bar</ta>
            <ta e="T495" id="Seg_6715" s="T494">bĭdə-laʔbə-bi</ta>
            <ta e="T496" id="Seg_6716" s="T495">pograšsʼe</ta>
            <ta e="T497" id="Seg_6717" s="T496">mo-laːm-bi</ta>
            <ta e="T498" id="Seg_6718" s="T497">a</ta>
            <ta e="T499" id="Seg_6719" s="T498">dĭ</ta>
            <ta e="T500" id="Seg_6720" s="T499">bar</ta>
            <ta e="T501" id="Seg_6721" s="T500">multuk-ziʔ</ta>
            <ta e="T502" id="Seg_6722" s="T501">dĭ-m</ta>
            <ta e="T503" id="Seg_6723" s="T502">kut-laːm-bi</ta>
            <ta e="T504" id="Seg_6724" s="T503">uda-Tə</ta>
            <ta e="T505" id="Seg_6725" s="T504">uda-bə</ta>
            <ta e="T506" id="Seg_6726" s="T505">bar</ta>
            <ta e="T507" id="Seg_6727" s="T506">băldə-bi</ta>
            <ta e="T511" id="Seg_6728" s="T510">girgit</ta>
            <ta e="T512" id="Seg_6729" s="T511">kuza</ta>
            <ta e="T513" id="Seg_6730" s="T512">ĭzem-luʔbdə-bi</ta>
            <ta e="T514" id="Seg_6731" s="T513">dĭgəttə</ta>
            <ta e="T515" id="Seg_6732" s="T514">kan-lV-jəʔ</ta>
            <ta e="T516" id="Seg_6733" s="T515">šaman-Tə</ta>
            <ta e="T517" id="Seg_6734" s="T516">dĭ</ta>
            <ta e="T518" id="Seg_6735" s="T517">bar</ta>
            <ta e="T519" id="Seg_6736" s="T518">dĭ-m</ta>
            <ta e="T520" id="Seg_6737" s="T519">tʼezer-liA</ta>
            <ta e="T521" id="Seg_6738" s="T520">lʼevăj</ta>
            <ta e="T522" id="Seg_6739" s="T521">uda-gəndə</ta>
            <ta e="T523" id="Seg_6740" s="T522">nuldə-lV-j</ta>
            <ta e="T524" id="Seg_6741" s="T523">dĭ</ta>
            <ta e="T525" id="Seg_6742" s="T524">üge</ta>
            <ta e="T526" id="Seg_6743" s="T525">dărəʔ</ta>
            <ta e="T527" id="Seg_6744" s="T526">bar</ta>
            <ta e="T528" id="Seg_6745" s="T527">ulu-t-ziʔ</ta>
            <ta e="T532" id="Seg_6746" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_6747" s="T532">dĭ</ta>
            <ta e="T534" id="Seg_6748" s="T533">dĭ-Tə</ta>
            <ta e="T535" id="Seg_6749" s="T534">tʼažer-luʔbdə-bi</ta>
            <ta e="T536" id="Seg_6750" s="T535">i</ta>
            <ta e="T537" id="Seg_6751" s="T536">bos-də</ta>
            <ta e="T538" id="Seg_6752" s="T537">bar</ta>
            <ta e="T539" id="Seg_6753" s="T538">süʔmə-laʔbə</ta>
            <ta e="T540" id="Seg_6754" s="T539">buben-Tə</ta>
            <ta e="T541" id="Seg_6755" s="T540">sʼar-laʔbə</ta>
            <ta e="T543" id="Seg_6756" s="T542">dĭgəttə</ta>
            <ta e="T544" id="Seg_6757" s="T543">dĭ</ta>
            <ta e="T545" id="Seg_6758" s="T544">ej</ta>
            <ta e="T546" id="Seg_6759" s="T545">ĭzem-liA</ta>
            <ta e="T547" id="Seg_6760" s="T546">maʔ-gəndə</ta>
            <ta e="T548" id="Seg_6761" s="T547">šo-lV-j</ta>
            <ta e="T551" id="Seg_6762" s="T550">tibi-zAŋ</ta>
            <ta e="T552" id="Seg_6763" s="T551">dʼije-Tə</ta>
            <ta e="T848" id="Seg_6764" s="T552">kan-lAʔ</ta>
            <ta e="T553" id="Seg_6765" s="T848">tʼür-bi-jəʔ</ta>
            <ta e="T554" id="Seg_6766" s="T553">ne-zAŋ</ta>
            <ta e="T555" id="Seg_6767" s="T554">maʔ-Kən</ta>
            <ta e="T556" id="Seg_6768" s="T555">amno-bi-jəʔ</ta>
            <ta e="T557" id="Seg_6769" s="T556">saranka-jəʔ</ta>
            <ta e="T558" id="Seg_6770" s="T557">tĭl-bi-jəʔ</ta>
            <ta e="T559" id="Seg_6771" s="T558">uja-m</ta>
            <ta e="T560" id="Seg_6772" s="T559">hen-bi-jəʔ</ta>
            <ta e="T561" id="Seg_6773" s="T560">ipek</ta>
            <ta e="T562" id="Seg_6774" s="T561">naga</ta>
            <ta e="T564" id="Seg_6775" s="T563">ne-zAŋ</ta>
            <ta e="T565" id="Seg_6776" s="T564">bar</ta>
            <ta e="T567" id="Seg_6777" s="T566">noʔ</ta>
            <ta e="T568" id="Seg_6778" s="T567">nĭŋgə-bi-jəʔ</ta>
            <ta e="T569" id="Seg_6779" s="T568">travʼanʼiga-jəʔ</ta>
            <ta e="T570" id="Seg_6780" s="T569">kür-bi-jəʔ</ta>
            <ta e="T571" id="Seg_6781" s="T570">jama-jəʔ</ta>
            <ta e="T572" id="Seg_6782" s="T571">šöʔ-bi-jəʔ</ta>
            <ta e="T573" id="Seg_6783" s="T572">žila-jəʔ</ta>
            <ta e="T574" id="Seg_6784" s="T573">kür-bi-jəʔ</ta>
            <ta e="T575" id="Seg_6785" s="T574">dʼije-Kən</ta>
            <ta e="T576" id="Seg_6786" s="T575">maʔ</ta>
            <ta e="T578" id="Seg_6787" s="T577">a-bi-jəʔ</ta>
            <ta e="T579" id="Seg_6788" s="T578">dĭgəttə</ta>
            <ta e="T580" id="Seg_6789" s="T579">dĭ</ta>
            <ta e="T581" id="Seg_6790" s="T580">maʔ-Kən</ta>
            <ta e="T582" id="Seg_6791" s="T581">uja</ta>
            <ta e="T583" id="Seg_6792" s="T582">mĭnzər-bi-jəʔ</ta>
            <ta e="T584" id="Seg_6793" s="T583">segi</ta>
            <ta e="T585" id="Seg_6794" s="T584">bü</ta>
            <ta e="T586" id="Seg_6795" s="T585">mĭnzər-bi-jəʔ</ta>
            <ta e="T587" id="Seg_6796" s="T586">amor-bi-jəʔ</ta>
            <ta e="T589" id="Seg_6797" s="T588">tura-jəʔ</ta>
            <ta e="T590" id="Seg_6798" s="T589">i-bi-jəʔ</ta>
            <ta e="T591" id="Seg_6799" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_6800" s="T591">tura-Kən</ta>
            <ta e="T593" id="Seg_6801" s="T592">ej</ta>
            <ta e="T594" id="Seg_6802" s="T593">amno-bi-jəʔ</ta>
            <ta e="T595" id="Seg_6803" s="T594">nʼiʔnen</ta>
            <ta e="T596" id="Seg_6804" s="T595">maʔ</ta>
            <ta e="T597" id="Seg_6805" s="T596">i-bi</ta>
            <ta e="T598" id="Seg_6806" s="T597">dĭn</ta>
            <ta e="T599" id="Seg_6807" s="T598">mĭnzər-bi-jəʔ</ta>
            <ta e="T600" id="Seg_6808" s="T599">mĭje</ta>
            <ta e="T601" id="Seg_6809" s="T600">segi</ta>
            <ta e="T602" id="Seg_6810" s="T601">bü</ta>
            <ta e="T603" id="Seg_6811" s="T602">mĭnzər-bi-jəʔ</ta>
            <ta e="T604" id="Seg_6812" s="T603">amor-bi-jəʔ</ta>
            <ta e="T605" id="Seg_6813" s="T604">a</ta>
            <ta e="T606" id="Seg_6814" s="T605">tura-Kən</ta>
            <ta e="T607" id="Seg_6815" s="T606">kunol-bi-jəʔ</ta>
            <ta e="T609" id="Seg_6816" s="T608">maʔ-jəʔ</ta>
            <ta e="T610" id="Seg_6817" s="T609">a-bi-jəʔ</ta>
            <ta e="T612" id="Seg_6818" s="T611">pa-jəʔ</ta>
            <ta e="T613" id="Seg_6819" s="T612">dĭgəttə</ta>
            <ta e="T614" id="Seg_6820" s="T613">kuba</ta>
            <ta e="T615" id="Seg_6821" s="T614">kür-bi-jəʔ</ta>
            <ta e="T616" id="Seg_6822" s="T615">pa-t-ziʔ</ta>
            <ta e="T617" id="Seg_6823" s="T616">da</ta>
            <ta e="T618" id="Seg_6824" s="T617">hen-bi-jəʔ</ta>
            <ta e="T619" id="Seg_6825" s="T618">dĭ-Tə</ta>
            <ta e="T620" id="Seg_6826" s="T619">dĭgəttə</ta>
            <ta e="T621" id="Seg_6827" s="T620">tagan-jəʔ</ta>
            <ta e="T622" id="Seg_6828" s="T621">a-bi-jəʔ</ta>
            <ta e="T623" id="Seg_6829" s="T622">dĭgəttə</ta>
            <ta e="T624" id="Seg_6830" s="T623">aspaʔ-jəʔ</ta>
            <ta e="T625" id="Seg_6831" s="T624">edə-bi-jəʔ</ta>
            <ta e="T628" id="Seg_6832" s="T627">mĭnzər-zittə</ta>
            <ta e="T629" id="Seg_6833" s="T628">uja</ta>
            <ta e="T630" id="Seg_6834" s="T629">da</ta>
            <ta e="T631" id="Seg_6835" s="T630">segi</ta>
            <ta e="T632" id="Seg_6836" s="T631">bü</ta>
            <ta e="T633" id="Seg_6837" s="T632">mĭnzər-zittə</ta>
            <ta e="T635" id="Seg_6838" s="T633">dĭgəttə</ta>
            <ta e="T638" id="Seg_6839" s="T637">tʼo-Kən</ta>
            <ta e="T639" id="Seg_6840" s="T638">šü</ta>
            <ta e="T640" id="Seg_6841" s="T639">am-bi-jəʔ</ta>
            <ta e="T641" id="Seg_6842" s="T640">nʼuʔnun</ta>
            <ta e="T642" id="Seg_6843" s="T641">ši</ta>
            <ta e="T643" id="Seg_6844" s="T642">i-bi</ta>
            <ta e="T644" id="Seg_6845" s="T643">dĭbər</ta>
            <ta e="T646" id="Seg_6846" s="T645">bor</ta>
            <ta e="T647" id="Seg_6847" s="T646">kandə-laʔbə</ta>
            <ta e="T648" id="Seg_6848" s="T647">dĭgəttə</ta>
            <ta e="T649" id="Seg_6849" s="T648">pa-jəʔ</ta>
            <ta e="T650" id="Seg_6850" s="T649">am-bi-jəʔ</ta>
            <ta e="T651" id="Seg_6851" s="T650">štobɨ</ta>
            <ta e="T652" id="Seg_6852" s="T651">šü-Tə</ta>
            <ta e="T653" id="Seg_6853" s="T652">šində=də</ta>
            <ta e="T654" id="Seg_6854" s="T653">ej</ta>
            <ta e="T656" id="Seg_6855" s="T655">saʔmə-bi</ta>
            <ta e="T658" id="Seg_6856" s="T657">šišəge</ta>
            <ta e="T659" id="Seg_6857" s="T658">mo-bi</ta>
            <ta e="T660" id="Seg_6858" s="T659">tak</ta>
            <ta e="T662" id="Seg_6859" s="T661">koža-ziʔ</ta>
            <ta e="T663" id="Seg_6860" s="T662">kaj-bi-jəʔ</ta>
            <ta e="T664" id="Seg_6861" s="T663">a</ta>
            <ta e="T665" id="Seg_6862" s="T664">ejü</ta>
            <ta e="T666" id="Seg_6863" s="T665">mo-bi</ta>
            <ta e="T667" id="Seg_6864" s="T666">tak</ta>
            <ta e="T669" id="Seg_6865" s="T668">kür-bi-jəʔ</ta>
            <ta e="T671" id="Seg_6866" s="T670">kaj-bi-jəʔ</ta>
            <ta e="T673" id="Seg_6867" s="T672">urgo</ta>
            <ta e="T674" id="Seg_6868" s="T673">i-bi</ta>
            <ta e="T677" id="Seg_6869" s="T676">teʔdə</ta>
            <ta e="T678" id="Seg_6870" s="T677">il</ta>
            <ta e="T679" id="Seg_6871" s="T678">iʔbö-bi-jəʔ</ta>
            <ta e="T680" id="Seg_6872" s="T679">biəʔ</ta>
            <ta e="T681" id="Seg_6873" s="T680">il</ta>
            <ta e="T682" id="Seg_6874" s="T681">kamən</ta>
            <ta e="T683" id="Seg_6875" s="T682">sĭri</ta>
            <ta e="T684" id="Seg_6876" s="T683">i-bi</ta>
            <ta e="T685" id="Seg_6877" s="T684">dĭ-zAŋ</ta>
            <ta e="T686" id="Seg_6878" s="T685">dön</ta>
            <ta e="T687" id="Seg_6879" s="T686">amno-bi-jəʔ</ta>
            <ta e="T688" id="Seg_6880" s="T687">a</ta>
            <ta e="T689" id="Seg_6881" s="T688">kamən</ta>
            <ta e="T690" id="Seg_6882" s="T689">sĭri</ta>
            <ta e="T849" id="Seg_6883" s="T690">kan-lAʔ</ta>
            <ta e="T691" id="Seg_6884" s="T849">tʼür-bi</ta>
            <ta e="T692" id="Seg_6885" s="T691">dĭgəttə</ta>
            <ta e="T693" id="Seg_6886" s="T692">dĭ-zAŋ</ta>
            <ta e="T694" id="Seg_6887" s="T693">kan-ntə-gA-jəʔ</ta>
            <ta e="T695" id="Seg_6888" s="T694">dʼije-Tə</ta>
            <ta e="T696" id="Seg_6889" s="T695">urgo</ta>
            <ta e="T697" id="Seg_6890" s="T696">măja-Tə</ta>
            <ta e="T699" id="Seg_6891" s="T698">olenʼə-jəʔ-ziʔ</ta>
            <ta e="T700" id="Seg_6892" s="T699">mĭn-bi-jəʔ</ta>
            <ta e="T701" id="Seg_6893" s="T700">Dʼelam-Tə</ta>
            <ta e="T702" id="Seg_6894" s="T701">kan-bi-jəʔ</ta>
            <ta e="T703" id="Seg_6895" s="T702">dĭn</ta>
            <ta e="T704" id="Seg_6896" s="T703">urgo</ta>
            <ta e="T705" id="Seg_6897" s="T704">bü</ta>
            <ta e="T706" id="Seg_6898" s="T705">kola</ta>
            <ta e="T707" id="Seg_6899" s="T706">dʼabə-bi-jəʔ</ta>
            <ta e="T709" id="Seg_6900" s="T708">dĭgəttə</ta>
            <ta e="T710" id="Seg_6901" s="T709">dĭ-zAŋ</ta>
            <ta e="T711" id="Seg_6902" s="T710">kan-bi-jəʔ</ta>
            <ta e="T712" id="Seg_6903" s="T711">ešši-zAŋ-Tə</ta>
            <ta e="T713" id="Seg_6904" s="T712">i</ta>
            <ta e="T714" id="Seg_6905" s="T713">ne-zAŋ-də</ta>
            <ta e="T715" id="Seg_6906" s="T714">i-bi-jəʔ</ta>
            <ta e="T716" id="Seg_6907" s="T715">olenʼə-jəʔ-ziʔ</ta>
            <ta e="T717" id="Seg_6908" s="T716">kan-bi-jəʔ</ta>
            <ta e="T719" id="Seg_6909" s="T718">i</ta>
            <ta e="T720" id="Seg_6910" s="T719">men-zAŋ-ziʔ</ta>
            <ta e="T722" id="Seg_6911" s="T721">dĭgəttə</ta>
            <ta e="T723" id="Seg_6912" s="T722">dĭ-zAŋ</ta>
            <ta e="T724" id="Seg_6913" s="T723">dʼije-Kən</ta>
            <ta e="T725" id="Seg_6914" s="T724">keʔbde</ta>
            <ta e="T726" id="Seg_6915" s="T725">oʔbdə-bi-jəʔ</ta>
            <ta e="T727" id="Seg_6916" s="T726">am-bi-jəʔ</ta>
            <ta e="T728" id="Seg_6917" s="T727">sanə</ta>
            <ta e="T729" id="Seg_6918" s="T728">oʔbdə-bi-jəʔ</ta>
            <ta e="T730" id="Seg_6919" s="T729">lej-bi-jəʔ</ta>
            <ta e="T732" id="Seg_6920" s="T731">dĭgəttə</ta>
            <ta e="T733" id="Seg_6921" s="T732">dĭ-zAŋ</ta>
            <ta e="T734" id="Seg_6922" s="T733">beške-jəʔ</ta>
            <ta e="T735" id="Seg_6923" s="T734">oʔbdə-bi-jəʔ</ta>
            <ta e="T736" id="Seg_6924" s="T735">mĭnzər-bi-jəʔ</ta>
            <ta e="T737" id="Seg_6925" s="T736">i</ta>
            <ta e="T738" id="Seg_6926" s="T737">tustʼar-bi-jəʔ</ta>
            <ta e="T740" id="Seg_6927" s="T739">dĭgəttə</ta>
            <ta e="T741" id="Seg_6928" s="T740">dĭn</ta>
            <ta e="T742" id="Seg_6929" s="T741">dĭ-zen</ta>
            <ta e="T743" id="Seg_6930" s="T742">dĭn</ta>
            <ta e="T744" id="Seg_6931" s="T743">kut-bi-jəʔ</ta>
            <ta e="T745" id="Seg_6932" s="T744">sɨne-jəʔ</ta>
            <ta e="T746" id="Seg_6933" s="T745">i</ta>
            <ta e="T747" id="Seg_6934" s="T746">uja</ta>
            <ta e="T748" id="Seg_6935" s="T747">tustʼar-bi-jəʔ</ta>
            <ta e="T749" id="Seg_6936" s="T748">dĭgəttə</ta>
            <ta e="T750" id="Seg_6937" s="T749">edə-bi-jəʔ</ta>
            <ta e="T751" id="Seg_6938" s="T750">dĭ</ta>
            <ta e="T753" id="Seg_6939" s="T752">koː-laːm-bi</ta>
            <ta e="T754" id="Seg_6940" s="T753">baltu-ziʔ</ta>
            <ta e="T755" id="Seg_6941" s="T754">hʼaʔ-bi-jəʔ</ta>
            <ta e="T756" id="Seg_6942" s="T755">băra-Tə</ta>
            <ta e="T757" id="Seg_6943" s="T756">kămnə-bi-jəʔ</ta>
            <ta e="T759" id="Seg_6944" s="T758">dĭ-zAŋ</ta>
            <ta e="T760" id="Seg_6945" s="T759">dĭgəttə</ta>
            <ta e="T761" id="Seg_6946" s="T760">kola</ta>
            <ta e="T762" id="Seg_6947" s="T761">dʼabə-bi-jəʔ</ta>
            <ta e="T763" id="Seg_6948" s="T762">i</ta>
            <ta e="T764" id="Seg_6949" s="T763">tustʼar-bi-jəʔ</ta>
            <ta e="T765" id="Seg_6950" s="T764">i</ta>
            <ta e="T766" id="Seg_6951" s="T765">tože</ta>
            <ta e="T767" id="Seg_6952" s="T766">kola</ta>
            <ta e="T769" id="Seg_6953" s="T767">koʔ-bi-jəʔ</ta>
            <ta e="T772" id="Seg_6954" s="T771">dĭgəttə</ta>
            <ta e="T773" id="Seg_6955" s="T772">kuba-t-ziʔ</ta>
            <ta e="T774" id="Seg_6956" s="T773">a-bi-jəʔ</ta>
            <ta e="T775" id="Seg_6957" s="T774">băra-jəʔ</ta>
            <ta e="T776" id="Seg_6958" s="T775">i</ta>
            <ta e="T777" id="Seg_6959" s="T776">dĭbər</ta>
            <ta e="T778" id="Seg_6960" s="T777">hen-bi-jəʔ</ta>
            <ta e="T779" id="Seg_6961" s="T778">uja</ta>
            <ta e="T780" id="Seg_6962" s="T779">i</ta>
            <ta e="T781" id="Seg_6963" s="T780">kola</ta>
            <ta e="T782" id="Seg_6964" s="T781">i</ta>
            <ta e="T783" id="Seg_6965" s="T782">olenʼə-jəʔ-ziʔ</ta>
            <ta e="T784" id="Seg_6966" s="T783">det-bi-jəʔ</ta>
            <ta e="T785" id="Seg_6967" s="T784">maʔ-Tə</ta>
            <ta e="T786" id="Seg_6968" s="T785">döbər</ta>
            <ta e="T787" id="Seg_6969" s="T786">gijen</ta>
            <ta e="T788" id="Seg_6970" s="T787">amno-bi-jəʔ</ta>
            <ta e="T789" id="Seg_6971" s="T788">kamən</ta>
            <ta e="T790" id="Seg_6972" s="T789">šišəge</ta>
            <ta e="T791" id="Seg_6973" s="T790">i-bi</ta>
            <ta e="T793" id="Seg_6974" s="T792">dĭ-zAŋ</ta>
            <ta e="T795" id="Seg_6975" s="T794">dĭrgit-jəʔ</ta>
            <ta e="T796" id="Seg_6976" s="T795">men-zAŋ-də</ta>
            <ta e="T798" id="Seg_6977" s="T797">i-bi-jəʔ</ta>
            <ta e="T800" id="Seg_6978" s="T799">kan-lV-jəʔ</ta>
            <ta e="T801" id="Seg_6979" s="T800">poʔto</ta>
            <ta e="T802" id="Seg_6980" s="T801">iʔgö</ta>
            <ta e="T803" id="Seg_6981" s="T802">dʼabə-lV-jəʔ</ta>
            <ta e="T804" id="Seg_6982" s="T803">dĭgəttə</ta>
            <ta e="T805" id="Seg_6983" s="T804">maʔ-gəndə</ta>
            <ta e="T806" id="Seg_6984" s="T805">šo-lV-j</ta>
            <ta e="T807" id="Seg_6985" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_6986" s="T807">ine</ta>
            <ta e="T809" id="Seg_6987" s="T808">koler-lV-j</ta>
            <ta e="T810" id="Seg_6988" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_6989" s="T810">kandə-gA</ta>
            <ta e="T812" id="Seg_6990" s="T811">poʔto</ta>
            <ta e="T813" id="Seg_6991" s="T812">oʔbdə-zittə</ta>
            <ta e="T814" id="Seg_6992" s="T813">dĭgəttə</ta>
            <ta e="T815" id="Seg_6993" s="T814">šo-lV-j</ta>
            <ta e="T816" id="Seg_6994" s="T815">maʔ-gəndə</ta>
            <ta e="T818" id="Seg_6995" s="T817">i</ta>
            <ta e="T819" id="Seg_6996" s="T818">poʔto</ta>
            <ta e="T820" id="Seg_6997" s="T819">det-lV-j</ta>
            <ta e="T823" id="Seg_6998" s="T822">dĭ-zem</ta>
            <ta e="T825" id="Seg_6999" s="T824">armija-Tə</ta>
            <ta e="T826" id="Seg_7000" s="T825">ej</ta>
            <ta e="T827" id="Seg_7001" s="T826">i-bi-jəʔ</ta>
            <ta e="T828" id="Seg_7002" s="T827">dĭ-zAŋ</ta>
            <ta e="T829" id="Seg_7003" s="T828">ĭsak</ta>
            <ta e="T830" id="Seg_7004" s="T829">aktʼa</ta>
            <ta e="T831" id="Seg_7005" s="T830">aktʼa</ta>
            <ta e="T832" id="Seg_7006" s="T831">mĭ-bi-jəʔ</ta>
            <ta e="T833" id="Seg_7007" s="T832">i</ta>
            <ta e="T834" id="Seg_7008" s="T833">albuga</ta>
            <ta e="T835" id="Seg_7009" s="T834">mĭ-bi-jəʔ</ta>
            <ta e="T837" id="Seg_7010" s="T836">dĭ-zAŋ</ta>
            <ta e="T838" id="Seg_7011" s="T837">ĭsak-Tə</ta>
            <ta e="T839" id="Seg_7012" s="T838">mĭn-bi-jəʔ</ta>
            <ta e="T840" id="Seg_7013" s="T839">aktʼa</ta>
            <ta e="T841" id="Seg_7014" s="T840">biəʔ</ta>
            <ta e="T842" id="Seg_7015" s="T841">biəʔ</ta>
            <ta e="T843" id="Seg_7016" s="T842">sumna</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7017" s="T0">when</ta>
            <ta e="T2" id="Seg_7018" s="T1">we.LAT</ta>
            <ta e="T3" id="Seg_7019" s="T2">come-PST-2SG</ta>
            <ta e="T4" id="Seg_7020" s="T3">big.[NOM.SG]</ta>
            <ta e="T5" id="Seg_7021" s="T4">day-LOC</ta>
            <ta e="T6" id="Seg_7022" s="T5">two.[NOM.SG]</ta>
            <ta e="T7" id="Seg_7023" s="T6">day.[NOM.SG]</ta>
            <ta e="T850" id="Seg_7024" s="T7">Aginskoe</ta>
            <ta e="T8" id="Seg_7025" s="T850">settlement-LOC</ta>
            <ta e="T9" id="Seg_7026" s="T8">sit-PST-1SG</ta>
            <ta e="T10" id="Seg_7027" s="T9">what.[NOM.SG]</ta>
            <ta e="T11" id="Seg_7028" s="T10">what.[NOM.SG]</ta>
            <ta e="T12" id="Seg_7029" s="T11">what.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7030" s="T12">see-PST-2SG</ta>
            <ta e="T14" id="Seg_7031" s="T13">then</ta>
            <ta e="T15" id="Seg_7032" s="T14">here</ta>
            <ta e="T16" id="Seg_7033" s="T15">come-PST-1SG</ta>
            <ta e="T18" id="Seg_7034" s="T17">look-FRQ-PST-1SG</ta>
            <ta e="T19" id="Seg_7035" s="T18">look-FRQ-PST-1SG</ta>
            <ta e="T20" id="Seg_7036" s="T19">how=INDEF</ta>
            <ta e="T21" id="Seg_7037" s="T20">NEG</ta>
            <ta e="T22" id="Seg_7038" s="T21">can-PRS-1SG</ta>
            <ta e="T23" id="Seg_7039" s="T22">NEG</ta>
            <ta e="T24" id="Seg_7040" s="T23">can-PRS-1SG</ta>
            <ta e="T25" id="Seg_7041" s="T24">come-INF.LAT</ta>
            <ta e="T26" id="Seg_7042" s="T25">here</ta>
            <ta e="T27" id="Seg_7043" s="T26">three.[NOM.SG]</ta>
            <ta e="T28" id="Seg_7044" s="T27">day-LOC</ta>
            <ta e="T29" id="Seg_7045" s="T28">then</ta>
            <ta e="T30" id="Seg_7046" s="T29">come-PST-1SG</ta>
            <ta e="T31" id="Seg_7047" s="T30">here</ta>
            <ta e="T33" id="Seg_7048" s="T32">then</ta>
            <ta e="T34" id="Seg_7049" s="T33">here</ta>
            <ta e="T35" id="Seg_7050" s="T34">come-PST-1SG</ta>
            <ta e="T36" id="Seg_7051" s="T35">grandmother-LAT</ta>
            <ta e="T37" id="Seg_7052" s="T36">come-PST-1SG</ta>
            <ta e="T38" id="Seg_7053" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_7054" s="T38">I.ACC</ta>
            <ta e="T40" id="Seg_7055" s="T39">PTCL</ta>
            <ta e="T41" id="Seg_7056" s="T40">feed-PST.[3SG]</ta>
            <ta e="T42" id="Seg_7057" s="T41">soup.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7058" s="T42">give-PST.[3SG]</ta>
            <ta e="T45" id="Seg_7059" s="T44">then</ta>
            <ta e="T46" id="Seg_7060" s="T45">here</ta>
            <ta e="T47" id="Seg_7061" s="T46">come-PST-1SG</ta>
            <ta e="T48" id="Seg_7062" s="T47">calf.[NOM.SG]</ta>
            <ta e="T49" id="Seg_7063" s="T48">shout-DUR.[3SG]</ta>
            <ta e="T50" id="Seg_7064" s="T49">woman.[NOM.SG]</ta>
            <ta e="T51" id="Seg_7065" s="T50">PTCL</ta>
            <ta e="T52" id="Seg_7066" s="T51">give-PST.[3SG]</ta>
            <ta e="T53" id="Seg_7067" s="T52">this-LAT</ta>
            <ta e="T54" id="Seg_7068" s="T53">potato.[NOM.SG]</ta>
            <ta e="T55" id="Seg_7069" s="T54">this.[NOM.SG]</ta>
            <ta e="T56" id="Seg_7070" s="T55">eat-DUR.[3SG]</ta>
            <ta e="T57" id="Seg_7071" s="T56">NEG</ta>
            <ta e="T58" id="Seg_7072" s="T57">shout-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_7073" s="T59">this.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7074" s="T60">woman-LOC</ta>
            <ta e="T62" id="Seg_7075" s="T61">house-LOC</ta>
            <ta e="T63" id="Seg_7076" s="T62">cold.[NOM.SG]</ta>
            <ta e="T64" id="Seg_7077" s="T63">stove.[NOM.SG]</ta>
            <ta e="T65" id="Seg_7078" s="T64">light-PST.[3SG]</ta>
            <ta e="T67" id="Seg_7079" s="T66">outside</ta>
            <ta e="T68" id="Seg_7080" s="T67">PTCL</ta>
            <ta e="T69" id="Seg_7081" s="T68">cold.[NOM.SG]</ta>
            <ta e="T70" id="Seg_7082" s="T69">very</ta>
            <ta e="T71" id="Seg_7083" s="T70">calf</ta>
            <ta e="T72" id="Seg_7084" s="T71">house-LOC</ta>
            <ta e="T73" id="Seg_7085" s="T72">stand-PRS.[3SG]</ta>
            <ta e="T74" id="Seg_7086" s="T73">PTCL</ta>
            <ta e="T76" id="Seg_7087" s="T75">cow.[NOM.SG]</ta>
            <ta e="T77" id="Seg_7088" s="T76">cow.[NOM.SG]</ta>
            <ta e="T78" id="Seg_7089" s="T77">give.birth-MOM-PST.[3SG]</ta>
            <ta e="T79" id="Seg_7090" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_7091" s="T79">small.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7092" s="T80">calf.[NOM.SG]</ta>
            <ta e="T83" id="Seg_7093" s="T82">we.NOM</ta>
            <ta e="T84" id="Seg_7094" s="T83">settlement-LOC</ta>
            <ta e="T85" id="Seg_7095" s="T84">very</ta>
            <ta e="T86" id="Seg_7096" s="T85">many</ta>
            <ta e="T87" id="Seg_7097" s="T86">people.[NOM.SG]</ta>
            <ta e="T88" id="Seg_7098" s="T87">die-PST-3PL</ta>
            <ta e="T90" id="Seg_7099" s="T89">go-PST-1SG</ta>
            <ta e="T91" id="Seg_7100" s="T90">woman-LAT</ta>
            <ta e="T92" id="Seg_7101" s="T91">this.[NOM.SG]</ta>
            <ta e="T94" id="Seg_7102" s="T93">very</ta>
            <ta e="T95" id="Seg_7103" s="T94">hurt-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_7104" s="T95">I.NOM</ta>
            <ta e="T97" id="Seg_7105" s="T96">today</ta>
            <ta e="T99" id="Seg_7106" s="T98">die-RES-FUT-1SG</ta>
            <ta e="T100" id="Seg_7107" s="T99">well</ta>
            <ta e="T101" id="Seg_7108" s="T100">die-IMP.2SG</ta>
            <ta e="T102" id="Seg_7109" s="T101">well</ta>
            <ta e="T103" id="Seg_7110" s="T102">die-IMP.2SG</ta>
            <ta e="T105" id="Seg_7111" s="T104">tomorrow</ta>
            <ta e="T107" id="Seg_7112" s="T106">come-FUT-1SG</ta>
            <ta e="T108" id="Seg_7113" s="T107">wash-INF.LAT</ta>
            <ta e="T109" id="Seg_7114" s="T108">you.ACC</ta>
            <ta e="T110" id="Seg_7115" s="T109">then</ta>
            <ta e="T111" id="Seg_7116" s="T110">NEG</ta>
            <ta e="T112" id="Seg_7117" s="T111">go-PST-1SG</ta>
            <ta e="T113" id="Seg_7118" s="T112">grass.[NOM.SG]</ta>
            <ta e="T114" id="Seg_7119" s="T113">bring-PST-3PL</ta>
            <ta e="T115" id="Seg_7120" s="T114">two.[NOM.SG]</ta>
            <ta e="T116" id="Seg_7121" s="T115">man.[NOM.SG]</ta>
            <ta e="T117" id="Seg_7122" s="T116">then</ta>
            <ta e="T118" id="Seg_7123" s="T117">come-PST-1SG</ta>
            <ta e="T119" id="Seg_7124" s="T118">what.[NOM.SG]</ta>
            <ta e="T120" id="Seg_7125" s="T119">long.time</ta>
            <ta e="T121" id="Seg_7126" s="T120">NEG</ta>
            <ta e="T122" id="Seg_7127" s="T121">come-PST-2SG</ta>
            <ta e="T123" id="Seg_7128" s="T122">and</ta>
            <ta e="T124" id="Seg_7129" s="T123">I.NOM</ta>
            <ta e="T125" id="Seg_7130" s="T124">think-PST-1SG</ta>
            <ta e="T126" id="Seg_7131" s="T125">you.NOM</ta>
            <ta e="T127" id="Seg_7132" s="T126">die-RES-PST-1SG</ta>
            <ta e="T128" id="Seg_7133" s="T127">wash-INF.LAT</ta>
            <ta e="T129" id="Seg_7134" s="T128">come-PST-1SG</ta>
            <ta e="T131" id="Seg_7135" s="T130">calf.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7136" s="T131">%sleep-DUR.[3SG]</ta>
            <ta e="T133" id="Seg_7137" s="T132">NEG</ta>
            <ta e="T134" id="Seg_7138" s="T133">shout-DUR.[3SG]</ta>
            <ta e="T136" id="Seg_7139" s="T135">child-PL</ta>
            <ta e="T137" id="Seg_7140" s="T136">PTCL</ta>
            <ta e="T138" id="Seg_7141" s="T137">run-DUR-3PL</ta>
            <ta e="T139" id="Seg_7142" s="T138">house-GEN</ta>
            <ta e="T141" id="Seg_7143" s="T140">house-GEN</ta>
            <ta e="T142" id="Seg_7144" s="T141">edge-LAT/LOC.3SG</ta>
            <ta e="T144" id="Seg_7145" s="T143">two.[NOM.SG]</ta>
            <ta e="T145" id="Seg_7146" s="T144">boy.[NOM.SG]</ta>
            <ta e="T146" id="Seg_7147" s="T145">come-PST-3PL</ta>
            <ta e="T147" id="Seg_7148" s="T146">listen-INF.LAT</ta>
            <ta e="T150" id="Seg_7149" s="T149">NEG.AUX-IMP.2SG</ta>
            <ta e="T151" id="Seg_7150" s="T150">shout-CNG</ta>
            <ta e="T152" id="Seg_7151" s="T151">NEG.AUX-IMP.2SG</ta>
            <ta e="T153" id="Seg_7152" s="T152">make.noise-CNG</ta>
            <ta e="T154" id="Seg_7153" s="T153">listen-2PL</ta>
            <ta e="T156" id="Seg_7154" s="T155">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T157" id="Seg_7155" s="T156">when</ta>
            <ta e="T158" id="Seg_7156" s="T157">come-FUT-3SG</ta>
            <ta e="T159" id="Seg_7157" s="T158">soon</ta>
            <ta e="T160" id="Seg_7158" s="T159">come-FUT-3SG</ta>
            <ta e="T162" id="Seg_7159" s="T161">one.should</ta>
            <ta e="T163" id="Seg_7160" s="T162">water.[NOM.SG]</ta>
            <ta e="T164" id="Seg_7161" s="T163">warm-FACT-INF.LAT</ta>
            <ta e="T165" id="Seg_7162" s="T164">then</ta>
            <ta e="T166" id="Seg_7163" s="T165">cow.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7164" s="T166">drink-TR-INF.LAT</ta>
            <ta e="T169" id="Seg_7165" s="T168">what.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7166" s="T169">listen-INF.LAT</ta>
            <ta e="T171" id="Seg_7167" s="T170">come-PST-2PL</ta>
            <ta e="T172" id="Seg_7168" s="T171">look-FRQ-INF.LAT</ta>
            <ta e="T173" id="Seg_7169" s="T172">come-PST-2PL</ta>
            <ta e="T175" id="Seg_7170" s="T174">NEG</ta>
            <ta e="T176" id="Seg_7171" s="T175">shout-PRS-3PL</ta>
            <ta e="T177" id="Seg_7172" s="T176">beautiful.[NOM.SG]</ta>
            <ta e="T178" id="Seg_7173" s="T177">boy-PL</ta>
            <ta e="T179" id="Seg_7174" s="T178">NEG</ta>
            <ta e="T180" id="Seg_7175" s="T179">shout-PRS-3PL</ta>
            <ta e="T181" id="Seg_7176" s="T180">stand-DUR-3PL</ta>
            <ta e="T182" id="Seg_7177" s="T181">good-PL</ta>
            <ta e="T183" id="Seg_7178" s="T182">very</ta>
            <ta e="T185" id="Seg_7179" s="T184">pine.nut.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7180" s="T185">today</ta>
            <ta e="T187" id="Seg_7181" s="T186">give-PST-1SG</ta>
            <ta e="T188" id="Seg_7182" s="T187">you.DAT</ta>
            <ta e="T189" id="Seg_7183" s="T188">crack-INF.LAT</ta>
            <ta e="T191" id="Seg_7184" s="T190">eye-NOM/GEN.3SG</ta>
            <ta e="T192" id="Seg_7185" s="T191">look-FRQ-INF.LAT</ta>
            <ta e="T194" id="Seg_7186" s="T193">ear-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T195" id="Seg_7187" s="T194">listen-INF.LAT</ta>
            <ta e="T196" id="Seg_7188" s="T195">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T197" id="Seg_7189" s="T196">eat-INF.LAT</ta>
            <ta e="T198" id="Seg_7190" s="T197">tooth-PL</ta>
            <ta e="T199" id="Seg_7191" s="T198">bite-INF.LAT</ta>
            <ta e="T200" id="Seg_7192" s="T199">bread.[NOM.SG]</ta>
            <ta e="T202" id="Seg_7193" s="T201">think-PST-1SG</ta>
            <ta e="T203" id="Seg_7194" s="T202">think-PST-1SG</ta>
            <ta e="T204" id="Seg_7195" s="T203">how</ta>
            <ta e="T205" id="Seg_7196" s="T204">here</ta>
            <ta e="T206" id="Seg_7197" s="T205">come-INF.LAT</ta>
            <ta e="T208" id="Seg_7198" s="T207">mitten.[NOM.SG]</ta>
            <ta e="T209" id="Seg_7199" s="T208">earth-LOC</ta>
            <ta e="T210" id="Seg_7200" s="T209">lie-DUR-PST.[3SG]</ta>
            <ta e="T211" id="Seg_7201" s="T210">bread.[NOM.SG]</ta>
            <ta e="T212" id="Seg_7202" s="T211">put-PST-3PL</ta>
            <ta e="T213" id="Seg_7203" s="T212">then</ta>
            <ta e="T214" id="Seg_7204" s="T213">eat-PST-3PL</ta>
            <ta e="T215" id="Seg_7205" s="T214">this</ta>
            <ta e="T216" id="Seg_7206" s="T215">bread.[NOM.SG]</ta>
            <ta e="T217" id="Seg_7207" s="T216">and</ta>
            <ta e="T218" id="Seg_7208" s="T217">meat.[NOM.SG]</ta>
            <ta e="T222" id="Seg_7209" s="T221">then</ta>
            <ta e="T223" id="Seg_7210" s="T222">eat-PST-3PL</ta>
            <ta e="T224" id="Seg_7211" s="T223">then</ta>
            <ta e="T225" id="Seg_7212" s="T224">God-LAT</ta>
            <ta e="T226" id="Seg_7213" s="T225">pray-PST-3PL</ta>
            <ta e="T851" id="Seg_7214" s="T227">Aginskoe</ta>
            <ta e="T228" id="Seg_7215" s="T851">settlement-LAT</ta>
            <ta e="T229" id="Seg_7216" s="T228">go-PST-1SG</ta>
            <ta e="T230" id="Seg_7217" s="T229">church-LOC</ta>
            <ta e="T231" id="Seg_7218" s="T230">church-LOC</ta>
            <ta e="T233" id="Seg_7219" s="T232">God.[NOM.SG]</ta>
            <ta e="T235" id="Seg_7220" s="T234">pray-PST-1SG</ta>
            <ta e="T237" id="Seg_7221" s="T236">many</ta>
            <ta e="T238" id="Seg_7222" s="T237">people.[NOM.SG]</ta>
            <ta e="T239" id="Seg_7223" s="T238">die-RES-PST-3PL</ta>
            <ta e="T241" id="Seg_7224" s="T240">cross-PL</ta>
            <ta e="T242" id="Seg_7225" s="T241">very</ta>
            <ta e="T243" id="Seg_7226" s="T242">many</ta>
            <ta e="T244" id="Seg_7227" s="T243">stand-PRS-3PL</ta>
            <ta e="T245" id="Seg_7228" s="T244">PTCL</ta>
            <ta e="T247" id="Seg_7229" s="T246">we.NOM</ta>
            <ta e="T248" id="Seg_7230" s="T247">girl.[NOM.SG]</ta>
            <ta e="T249" id="Seg_7231" s="T248">hurt-PST.[3SG]</ta>
            <ta e="T250" id="Seg_7232" s="T249">and</ta>
            <ta e="T251" id="Seg_7233" s="T250">father-NOM/GEN.3SG</ta>
            <ta e="T253" id="Seg_7234" s="T252">bring-PST.[3SG]</ta>
            <ta e="T255" id="Seg_7235" s="T254">heal-INF.LAT</ta>
            <ta e="T258" id="Seg_7236" s="T257">we.LAT</ta>
            <ta e="T259" id="Seg_7237" s="T258">today</ta>
            <ta e="T260" id="Seg_7238" s="T259">evening-LOC.ADV</ta>
            <ta e="T261" id="Seg_7239" s="T260">man.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7240" s="T261">woman-COM</ta>
            <ta e="T263" id="Seg_7241" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_7242" s="T263">fight-PST-3PL</ta>
            <ta e="T265" id="Seg_7243" s="T264">hand-LOC</ta>
            <ta e="T266" id="Seg_7244" s="T265">PTCL</ta>
            <ta e="T269" id="Seg_7245" s="T268">blood.[NOM.SG]</ta>
            <ta e="T270" id="Seg_7246" s="T269">flow-DUR.[3SG]</ta>
            <ta e="T271" id="Seg_7247" s="T270">child-PL-LAT</ta>
            <ta e="T272" id="Seg_7248" s="T271">PTCL</ta>
            <ta e="T273" id="Seg_7249" s="T272">shout-DUR-3PL</ta>
            <ta e="T274" id="Seg_7250" s="T273">I.NOM</ta>
            <ta e="T275" id="Seg_7251" s="T274">come-PST-1SG</ta>
            <ta e="T276" id="Seg_7252" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_7253" s="T276">scold-DES-PST-1SG</ta>
            <ta e="T279" id="Seg_7254" s="T278">calf.[NOM.SG]</ta>
            <ta e="T280" id="Seg_7255" s="T279">lie-PST.[3SG]</ta>
            <ta e="T281" id="Seg_7256" s="T280">NEG</ta>
            <ta e="T282" id="Seg_7257" s="T281">shout-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_7258" s="T282">now</ta>
            <ta e="T285" id="Seg_7259" s="T284">one.can</ta>
            <ta e="T286" id="Seg_7260" s="T285">speak-INF.LAT</ta>
            <ta e="T288" id="Seg_7261" s="T287">man-GEN</ta>
            <ta e="T289" id="Seg_7262" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_7263" s="T289">nose-NOM/GEN.3SG</ta>
            <ta e="T291" id="Seg_7264" s="T290">itch-PST.[3SG]</ta>
            <ta e="T292" id="Seg_7265" s="T291">where=INDEF</ta>
            <ta e="T293" id="Seg_7266" s="T292">vodka.[NOM.SG]</ta>
            <ta e="T294" id="Seg_7267" s="T293">drink-FUT-1SG</ta>
            <ta e="T295" id="Seg_7268" s="T294">then</ta>
            <ta e="T296" id="Seg_7269" s="T295">%%</ta>
            <ta e="T297" id="Seg_7270" s="T296">make-PST.[3SG]</ta>
            <ta e="T298" id="Seg_7271" s="T297">sit-PST.[3SG]</ta>
            <ta e="T299" id="Seg_7272" s="T298">tree-LOC</ta>
            <ta e="T300" id="Seg_7273" s="T299">and</ta>
            <ta e="T301" id="Seg_7274" s="T300">I.NOM</ta>
            <ta e="T302" id="Seg_7275" s="T301">grandmother-NOM/GEN/ACC.1SG</ta>
            <ta e="T303" id="Seg_7276" s="T302">go-PST.[3SG]</ta>
            <ta e="T852" id="Seg_7277" s="T303">Aginskoe</ta>
            <ta e="T304" id="Seg_7278" s="T852">settlement-LAT</ta>
            <ta e="T305" id="Seg_7279" s="T304">and</ta>
            <ta e="T306" id="Seg_7280" s="T305">vodka.[NOM.SG]</ta>
            <ta e="T307" id="Seg_7281" s="T306">bring-PST.[3SG]</ta>
            <ta e="T308" id="Seg_7282" s="T307">then</ta>
            <ta e="T309" id="Seg_7283" s="T308">sit.down-PST.[3SG]</ta>
            <ta e="T310" id="Seg_7284" s="T309">shit-INF.LAT</ta>
            <ta e="T312" id="Seg_7285" s="T311">this-LAT</ta>
            <ta e="T313" id="Seg_7286" s="T312">ass-INS</ta>
            <ta e="T314" id="Seg_7287" s="T313">then</ta>
            <ta e="T315" id="Seg_7288" s="T314">this.[NOM.SG]</ta>
            <ta e="T316" id="Seg_7289" s="T315">come-PST.[3SG]</ta>
            <ta e="T317" id="Seg_7290" s="T316">this.[NOM.SG]</ta>
            <ta e="T318" id="Seg_7291" s="T317">this-LAT</ta>
            <ta e="T319" id="Seg_7292" s="T318">vodka.[NOM.SG]</ta>
            <ta e="T320" id="Seg_7293" s="T319">give-PST.[3SG]</ta>
            <ta e="T321" id="Seg_7294" s="T320">this.[NOM.SG]</ta>
            <ta e="T322" id="Seg_7295" s="T321">drink-PST.[3SG]</ta>
            <ta e="T323" id="Seg_7296" s="T322">and</ta>
            <ta e="T324" id="Seg_7297" s="T323">say-IPFVZ.[3SG]</ta>
            <ta e="T325" id="Seg_7298" s="T324">say-IPFVZ.[3SG]</ta>
            <ta e="T326" id="Seg_7299" s="T325">this-LAT</ta>
            <ta e="T327" id="Seg_7300" s="T326">woman.[NOM.SG]</ta>
            <ta e="T328" id="Seg_7301" s="T327">so</ta>
            <ta e="T329" id="Seg_7302" s="T328">woman.[NOM.SG]</ta>
            <ta e="T330" id="Seg_7303" s="T329">look</ta>
            <ta e="T331" id="Seg_7304" s="T330">so</ta>
            <ta e="T332" id="Seg_7305" s="T331">woman.[NOM.SG]</ta>
            <ta e="T333" id="Seg_7306" s="T332">so</ta>
            <ta e="T334" id="Seg_7307" s="T333">woman.[NOM.SG]</ta>
            <ta e="T335" id="Seg_7308" s="T334">what.[NOM.SG]</ta>
            <ta e="T336" id="Seg_7309" s="T335">you.NOM</ta>
            <ta e="T337" id="Seg_7310" s="T336">so</ta>
            <ta e="T340" id="Seg_7311" s="T339">and</ta>
            <ta e="T341" id="Seg_7312" s="T340">you.NOM</ta>
            <ta e="T342" id="Seg_7313" s="T341">I.LAT</ta>
            <ta e="T344" id="Seg_7314" s="T343">ass-INS</ta>
            <ta e="T345" id="Seg_7315" s="T344">sit-PST-2SG</ta>
            <ta e="T346" id="Seg_7316" s="T345">and</ta>
            <ta e="T347" id="Seg_7317" s="T346">shit-PST-2SG</ta>
            <ta e="T350" id="Seg_7318" s="T349">I.NOM</ta>
            <ta e="T351" id="Seg_7319" s="T350">grandmother-NOM/GEN/ACC.1SG</ta>
            <ta e="T352" id="Seg_7320" s="T351">shaman-NOM/GEN/ACC.3SG</ta>
            <ta e="T353" id="Seg_7321" s="T352">go-PST-1SG</ta>
            <ta e="T354" id="Seg_7322" s="T353">there</ta>
            <ta e="T355" id="Seg_7323" s="T354">PTCL</ta>
            <ta e="T357" id="Seg_7324" s="T356">meat.[NOM.SG]</ta>
            <ta e="T358" id="Seg_7325" s="T357">boil-PST-3PL</ta>
            <ta e="T359" id="Seg_7326" s="T358">cup-LAT</ta>
            <ta e="T360" id="Seg_7327" s="T359">big.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7328" s="T360">cup.[NOM.SG]</ta>
            <ta e="T362" id="Seg_7329" s="T361">put-PST-3PL</ta>
            <ta e="T363" id="Seg_7330" s="T362">this.[NOM.SG]</ta>
            <ta e="T364" id="Seg_7331" s="T363">PTCL</ta>
            <ta e="T365" id="Seg_7332" s="T364">cold.[NOM.SG]</ta>
            <ta e="T366" id="Seg_7333" s="T365">become-RES-PST.[3SG]</ta>
            <ta e="T367" id="Seg_7334" s="T366">sit-IMP.2SG</ta>
            <ta e="T369" id="Seg_7335" s="T368">eat-INF.LAT</ta>
            <ta e="T370" id="Seg_7336" s="T369">this.[NOM.SG]</ta>
            <ta e="T371" id="Seg_7337" s="T370">PTCL</ta>
            <ta e="T372" id="Seg_7338" s="T371">meat.[NOM.SG]</ta>
            <ta e="T373" id="Seg_7339" s="T372">take-PST.[3SG]</ta>
            <ta e="T374" id="Seg_7340" s="T373">this.[NOM.SG]</ta>
            <ta e="T375" id="Seg_7341" s="T374">very</ta>
            <ta e="T376" id="Seg_7342" s="T375">cold.[NOM.SG]</ta>
            <ta e="T377" id="Seg_7343" s="T376">then</ta>
            <ta e="T378" id="Seg_7344" s="T377">dog-NOM/GEN/ACC.3SG</ta>
            <ta e="T380" id="Seg_7345" s="T379">throw-MOM-PST.[3SG]</ta>
            <ta e="T382" id="Seg_7346" s="T381">drum-INS</ta>
            <ta e="T383" id="Seg_7347" s="T382">PTCL</ta>
            <ta e="T385" id="Seg_7348" s="T384">play-PST.[3SG]</ta>
            <ta e="T386" id="Seg_7349" s="T385">all</ta>
            <ta e="T387" id="Seg_7350" s="T386">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T388" id="Seg_7351" s="T387">go-PST.[3SG]</ta>
            <ta e="T389" id="Seg_7352" s="T388">and</ta>
            <ta e="T390" id="Seg_7353" s="T389">I.NOM</ta>
            <ta e="T391" id="Seg_7354" s="T390">grandmother-LAT</ta>
            <ta e="T392" id="Seg_7355" s="T391">NEG</ta>
            <ta e="T393" id="Seg_7356" s="T392">give-PST-1SG</ta>
            <ta e="T394" id="Seg_7357" s="T393">then</ta>
            <ta e="T395" id="Seg_7358" s="T394">go-PST.[3SG]</ta>
            <ta e="T396" id="Seg_7359" s="T395">NEG.AUX-IMP.2SG</ta>
            <ta e="T397" id="Seg_7360" s="T396">bring-CNG</ta>
            <ta e="T398" id="Seg_7361" s="T397">this.[NOM.SG]</ta>
            <ta e="T399" id="Seg_7362" s="T398">woman.[NOM.SG]</ta>
            <ta e="T400" id="Seg_7363" s="T399">Russian</ta>
            <ta e="T401" id="Seg_7364" s="T400">woman.[NOM.SG]</ta>
            <ta e="T404" id="Seg_7365" s="T403">shaman-LOC</ta>
            <ta e="T405" id="Seg_7366" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_7367" s="T405">fur.coat.[NOM.SG]</ta>
            <ta e="T407" id="Seg_7368" s="T406">be-PST.[3SG]</ta>
            <ta e="T411" id="Seg_7369" s="T410">PTCL</ta>
            <ta e="T413" id="Seg_7370" s="T412">hen-GEN</ta>
            <ta e="T414" id="Seg_7371" s="T413">skin.[NOM.SG]</ta>
            <ta e="T415" id="Seg_7372" s="T414">sable-GEN</ta>
            <ta e="T416" id="Seg_7373" s="T415">skin.[NOM.SG]</ta>
            <ta e="T417" id="Seg_7374" s="T416">fur.coat-LOC</ta>
            <ta e="T418" id="Seg_7375" s="T417">squirrel-EP-GEN</ta>
            <ta e="T420" id="Seg_7376" s="T419">skin.[NOM.SG]</ta>
            <ta e="T422" id="Seg_7377" s="T420">fur.coat-LOC</ta>
            <ta e="T423" id="Seg_7378" s="T422">goat-GEN</ta>
            <ta e="T424" id="Seg_7379" s="T423">skin.[NOM.SG]</ta>
            <ta e="T426" id="Seg_7380" s="T425">this.[NOM.SG]</ta>
            <ta e="T427" id="Seg_7381" s="T426">shaman.[NOM.SG]</ta>
            <ta e="T428" id="Seg_7382" s="T427">PTCL</ta>
            <ta e="T432" id="Seg_7383" s="T431">then</ta>
            <ta e="T433" id="Seg_7384" s="T432">knife.[NOM.SG]</ta>
            <ta e="T434" id="Seg_7385" s="T433">take-PST.[3SG]</ta>
            <ta e="T435" id="Seg_7386" s="T434">and</ta>
            <ta e="T436" id="Seg_7387" s="T435">PTCL</ta>
            <ta e="T437" id="Seg_7388" s="T436">punch-PST.[3SG]</ta>
            <ta e="T439" id="Seg_7389" s="T438">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T440" id="Seg_7390" s="T439">and</ta>
            <ta e="T441" id="Seg_7391" s="T440">then</ta>
            <ta e="T442" id="Seg_7392" s="T441">fall-MOM-PST.[3SG]</ta>
            <ta e="T443" id="Seg_7393" s="T442">and</ta>
            <ta e="T444" id="Seg_7394" s="T443">people.[NOM.SG]</ta>
            <ta e="T445" id="Seg_7395" s="T444">many</ta>
            <ta e="T446" id="Seg_7396" s="T445">come-PRS-3PL</ta>
            <ta e="T457" id="Seg_7397" s="T456">man-PL</ta>
            <ta e="T460" id="Seg_7398" s="T459">when</ta>
            <ta e="T461" id="Seg_7399" s="T460">go-FUT-3PL</ta>
            <ta e="T462" id="Seg_7400" s="T461">forest-LAT</ta>
            <ta e="T463" id="Seg_7401" s="T462">then</ta>
            <ta e="T464" id="Seg_7402" s="T463">shaman-LAT</ta>
            <ta e="T465" id="Seg_7403" s="T464">go-FUT-3PL</ta>
            <ta e="T466" id="Seg_7404" s="T465">this.[NOM.SG]</ta>
            <ta e="T467" id="Seg_7405" s="T466">say-FUT-3SG</ta>
            <ta e="T468" id="Seg_7406" s="T467">you.NOM</ta>
            <ta e="T470" id="Seg_7407" s="T469">sable.[NOM.SG]</ta>
            <ta e="T471" id="Seg_7408" s="T470">kill-FUT-2SG</ta>
            <ta e="T472" id="Seg_7409" s="T471">and</ta>
            <ta e="T473" id="Seg_7410" s="T472">who-LAT</ta>
            <ta e="T474" id="Seg_7411" s="T473">say-FUT-3SG</ta>
            <ta e="T475" id="Seg_7412" s="T474">NEG</ta>
            <ta e="T476" id="Seg_7413" s="T475">kill-FUT-2SG</ta>
            <ta e="T479" id="Seg_7414" s="T478">shaman.[NOM.SG]</ta>
            <ta e="T480" id="Seg_7415" s="T479">and</ta>
            <ta e="T481" id="Seg_7416" s="T480">we.NOM</ta>
            <ta e="T482" id="Seg_7417" s="T481">shaman.[NOM.SG]</ta>
            <ta e="T483" id="Seg_7418" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_7419" s="T483">fight-PST-3PL</ta>
            <ta e="T487" id="Seg_7420" s="T486">go-PST.[3SG]</ta>
            <ta e="T488" id="Seg_7421" s="T487">house-LAT/LOC.3SG</ta>
            <ta e="T489" id="Seg_7422" s="T488">and</ta>
            <ta e="T490" id="Seg_7423" s="T489">we.NOM</ta>
            <ta e="T491" id="Seg_7424" s="T490">shaman.[NOM.SG]</ta>
            <ta e="T493" id="Seg_7425" s="T492">this-ACC</ta>
            <ta e="T494" id="Seg_7426" s="T493">PTCL</ta>
            <ta e="T495" id="Seg_7427" s="T494">catch.up-DUR-PST.[3SG]</ta>
            <ta e="T496" id="Seg_7428" s="T495">%%</ta>
            <ta e="T497" id="Seg_7429" s="T496">become-RES-PST.[3SG]</ta>
            <ta e="T498" id="Seg_7430" s="T497">and</ta>
            <ta e="T499" id="Seg_7431" s="T498">this.[NOM.SG]</ta>
            <ta e="T500" id="Seg_7432" s="T499">PTCL</ta>
            <ta e="T501" id="Seg_7433" s="T500">gun-INS</ta>
            <ta e="T502" id="Seg_7434" s="T501">this-ACC</ta>
            <ta e="T503" id="Seg_7435" s="T502">kill-RES-PST.[3SG]</ta>
            <ta e="T504" id="Seg_7436" s="T503">hand-LAT</ta>
            <ta e="T505" id="Seg_7437" s="T504">hand-ACC.3SG</ta>
            <ta e="T506" id="Seg_7438" s="T505">PTCL</ta>
            <ta e="T507" id="Seg_7439" s="T506">break-PST.[3SG]</ta>
            <ta e="T511" id="Seg_7440" s="T510">what.kind</ta>
            <ta e="T512" id="Seg_7441" s="T511">human.[NOM.SG]</ta>
            <ta e="T513" id="Seg_7442" s="T512">hurt-MOM-PST.[3SG]</ta>
            <ta e="T514" id="Seg_7443" s="T513">then</ta>
            <ta e="T515" id="Seg_7444" s="T514">go-FUT-3PL</ta>
            <ta e="T516" id="Seg_7445" s="T515">shaman-LAT</ta>
            <ta e="T517" id="Seg_7446" s="T516">this.[NOM.SG]</ta>
            <ta e="T518" id="Seg_7447" s="T517">PTCL</ta>
            <ta e="T519" id="Seg_7448" s="T518">this-ACC</ta>
            <ta e="T520" id="Seg_7449" s="T519">heal-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_7450" s="T520">left</ta>
            <ta e="T522" id="Seg_7451" s="T521">hand-LAT/LOC.3SG</ta>
            <ta e="T523" id="Seg_7452" s="T522">place-FUT-3SG</ta>
            <ta e="T524" id="Seg_7453" s="T523">this.[NOM.SG]</ta>
            <ta e="T525" id="Seg_7454" s="T524">always</ta>
            <ta e="T526" id="Seg_7455" s="T525">so</ta>
            <ta e="T527" id="Seg_7456" s="T526">PTCL</ta>
            <ta e="T528" id="Seg_7457" s="T527">head-3SG-INS</ta>
            <ta e="T532" id="Seg_7458" s="T531">then</ta>
            <ta e="T533" id="Seg_7459" s="T532">this.[NOM.SG]</ta>
            <ta e="T534" id="Seg_7460" s="T533">this-LAT</ta>
            <ta e="T535" id="Seg_7461" s="T534">%%-MOM-PST.[3SG]</ta>
            <ta e="T536" id="Seg_7462" s="T535">and</ta>
            <ta e="T537" id="Seg_7463" s="T536">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T538" id="Seg_7464" s="T537">PTCL</ta>
            <ta e="T539" id="Seg_7465" s="T538">jump-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_7466" s="T539">drum-LAT</ta>
            <ta e="T541" id="Seg_7467" s="T540">play-DUR.[3SG]</ta>
            <ta e="T543" id="Seg_7468" s="T542">then</ta>
            <ta e="T544" id="Seg_7469" s="T543">this.[NOM.SG]</ta>
            <ta e="T545" id="Seg_7470" s="T544">NEG</ta>
            <ta e="T546" id="Seg_7471" s="T545">hurt-PRS.[3SG]</ta>
            <ta e="T547" id="Seg_7472" s="T546">house-LAT/LOC.3SG</ta>
            <ta e="T548" id="Seg_7473" s="T547">come-FUT-3SG</ta>
            <ta e="T551" id="Seg_7474" s="T550">man-PL</ta>
            <ta e="T552" id="Seg_7475" s="T551">forest-LAT</ta>
            <ta e="T848" id="Seg_7476" s="T552">go-CVB</ta>
            <ta e="T553" id="Seg_7477" s="T848">disappear-PST-3PL</ta>
            <ta e="T554" id="Seg_7478" s="T553">woman-PL</ta>
            <ta e="T555" id="Seg_7479" s="T554">house-LOC</ta>
            <ta e="T556" id="Seg_7480" s="T555">live-PST-3PL</ta>
            <ta e="T557" id="Seg_7481" s="T556">Lilium.martagon-PL</ta>
            <ta e="T558" id="Seg_7482" s="T557">dig-PST-3PL</ta>
            <ta e="T559" id="Seg_7483" s="T558">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T560" id="Seg_7484" s="T559">put-PST-3PL</ta>
            <ta e="T561" id="Seg_7485" s="T560">bread.[NOM.SG]</ta>
            <ta e="T562" id="Seg_7486" s="T561">NEG.EX.[3SG]</ta>
            <ta e="T564" id="Seg_7487" s="T563">woman-PL</ta>
            <ta e="T565" id="Seg_7488" s="T564">PTCL</ta>
            <ta e="T567" id="Seg_7489" s="T566">grass.[NOM.SG]</ta>
            <ta e="T568" id="Seg_7490" s="T567">tear-PST-3PL</ta>
            <ta e="T569" id="Seg_7491" s="T568">%%-PL</ta>
            <ta e="T570" id="Seg_7492" s="T569">plait-PST-3PL</ta>
            <ta e="T571" id="Seg_7493" s="T570">boot-PL</ta>
            <ta e="T572" id="Seg_7494" s="T571">sew-PST-3PL</ta>
            <ta e="T573" id="Seg_7495" s="T572">tendon-PL</ta>
            <ta e="T574" id="Seg_7496" s="T573">plait-PST-3PL</ta>
            <ta e="T575" id="Seg_7497" s="T574">forest-LOC</ta>
            <ta e="T576" id="Seg_7498" s="T575">tent.[NOM.SG]</ta>
            <ta e="T578" id="Seg_7499" s="T577">make-PST-3PL</ta>
            <ta e="T579" id="Seg_7500" s="T578">then</ta>
            <ta e="T580" id="Seg_7501" s="T579">this.[NOM.SG]</ta>
            <ta e="T581" id="Seg_7502" s="T580">tent-LOC</ta>
            <ta e="T582" id="Seg_7503" s="T581">meat.[NOM.SG]</ta>
            <ta e="T583" id="Seg_7504" s="T582">boil-PST-3PL</ta>
            <ta e="T584" id="Seg_7505" s="T583">yellow.[NOM.SG]</ta>
            <ta e="T585" id="Seg_7506" s="T584">water.[NOM.SG]</ta>
            <ta e="T586" id="Seg_7507" s="T585">boil-PST-3PL</ta>
            <ta e="T587" id="Seg_7508" s="T586">eat-PST-3PL</ta>
            <ta e="T589" id="Seg_7509" s="T588">house-PL</ta>
            <ta e="T590" id="Seg_7510" s="T589">be-PST-3PL</ta>
            <ta e="T591" id="Seg_7511" s="T590">this.[NOM.SG]</ta>
            <ta e="T592" id="Seg_7512" s="T591">house-LOC</ta>
            <ta e="T593" id="Seg_7513" s="T592">NEG</ta>
            <ta e="T594" id="Seg_7514" s="T593">live-PST-3PL</ta>
            <ta e="T595" id="Seg_7515" s="T594">outside</ta>
            <ta e="T596" id="Seg_7516" s="T595">tent.[NOM.SG]</ta>
            <ta e="T597" id="Seg_7517" s="T596">be-PST.[3SG]</ta>
            <ta e="T598" id="Seg_7518" s="T597">there</ta>
            <ta e="T599" id="Seg_7519" s="T598">boil-PST-3PL</ta>
            <ta e="T600" id="Seg_7520" s="T599">soup.[NOM.SG]</ta>
            <ta e="T601" id="Seg_7521" s="T600">yellow.[NOM.SG]</ta>
            <ta e="T602" id="Seg_7522" s="T601">water.[NOM.SG]</ta>
            <ta e="T603" id="Seg_7523" s="T602">boil-PST-3PL</ta>
            <ta e="T604" id="Seg_7524" s="T603">eat-PST-3PL</ta>
            <ta e="T605" id="Seg_7525" s="T604">and</ta>
            <ta e="T606" id="Seg_7526" s="T605">house-LOC</ta>
            <ta e="T607" id="Seg_7527" s="T606">sleep-PST-3PL</ta>
            <ta e="T609" id="Seg_7528" s="T608">tent-PL</ta>
            <ta e="T610" id="Seg_7529" s="T609">make-PST-3PL</ta>
            <ta e="T612" id="Seg_7530" s="T611">tree-PL</ta>
            <ta e="T613" id="Seg_7531" s="T612">then</ta>
            <ta e="T614" id="Seg_7532" s="T613">skin.[NOM.SG]</ta>
            <ta e="T615" id="Seg_7533" s="T614">plait-PST-3PL</ta>
            <ta e="T616" id="Seg_7534" s="T615">tree-3SG-INS</ta>
            <ta e="T617" id="Seg_7535" s="T616">and</ta>
            <ta e="T618" id="Seg_7536" s="T617">put-PST-3PL</ta>
            <ta e="T619" id="Seg_7537" s="T618">this-LAT</ta>
            <ta e="T620" id="Seg_7538" s="T619">then</ta>
            <ta e="T621" id="Seg_7539" s="T620">rope-PL</ta>
            <ta e="T622" id="Seg_7540" s="T621">make-PST-3PL</ta>
            <ta e="T623" id="Seg_7541" s="T622">then</ta>
            <ta e="T624" id="Seg_7542" s="T623">cauldron-PL</ta>
            <ta e="T625" id="Seg_7543" s="T624">hang.up-PST-3PL</ta>
            <ta e="T628" id="Seg_7544" s="T627">boil-INF.LAT</ta>
            <ta e="T629" id="Seg_7545" s="T628">meat.[NOM.SG]</ta>
            <ta e="T630" id="Seg_7546" s="T629">and</ta>
            <ta e="T631" id="Seg_7547" s="T630">yellow.[NOM.SG]</ta>
            <ta e="T632" id="Seg_7548" s="T631">water.[NOM.SG]</ta>
            <ta e="T633" id="Seg_7549" s="T632">boil-INF.LAT</ta>
            <ta e="T635" id="Seg_7550" s="T633">then</ta>
            <ta e="T638" id="Seg_7551" s="T637">earth-LOC</ta>
            <ta e="T639" id="Seg_7552" s="T638">fire.[NOM.SG]</ta>
            <ta e="T640" id="Seg_7553" s="T639">eat-PST-3PL</ta>
            <ta e="T641" id="Seg_7554" s="T640">%above</ta>
            <ta e="T642" id="Seg_7555" s="T641">hole.[NOM.SG]</ta>
            <ta e="T643" id="Seg_7556" s="T642">be-PST.[3SG]</ta>
            <ta e="T644" id="Seg_7557" s="T643">there</ta>
            <ta e="T646" id="Seg_7558" s="T645">smoke.[NOM.SG]</ta>
            <ta e="T647" id="Seg_7559" s="T646">walk-DUR.[3SG]</ta>
            <ta e="T648" id="Seg_7560" s="T647">then</ta>
            <ta e="T649" id="Seg_7561" s="T648">tree-PL</ta>
            <ta e="T650" id="Seg_7562" s="T649">eat-PST-3PL</ta>
            <ta e="T651" id="Seg_7563" s="T650">so.that</ta>
            <ta e="T652" id="Seg_7564" s="T651">fire-LAT</ta>
            <ta e="T653" id="Seg_7565" s="T652">who.[NOM.SG]=INDEF</ta>
            <ta e="T654" id="Seg_7566" s="T653">NEG</ta>
            <ta e="T656" id="Seg_7567" s="T655">fall-PST.[3SG]</ta>
            <ta e="T658" id="Seg_7568" s="T657">cold.[NOM.SG]</ta>
            <ta e="T659" id="Seg_7569" s="T658">become-PST.[3SG]</ta>
            <ta e="T660" id="Seg_7570" s="T659">so</ta>
            <ta e="T662" id="Seg_7571" s="T661">skin-INS</ta>
            <ta e="T663" id="Seg_7572" s="T662">close-PST-3PL</ta>
            <ta e="T664" id="Seg_7573" s="T663">and</ta>
            <ta e="T665" id="Seg_7574" s="T664">warm.[NOM.SG]</ta>
            <ta e="T666" id="Seg_7575" s="T665">become-PST.[3SG]</ta>
            <ta e="T667" id="Seg_7576" s="T666">so</ta>
            <ta e="T669" id="Seg_7577" s="T668">plait-PST-3PL</ta>
            <ta e="T671" id="Seg_7578" s="T670">close-PST-3PL</ta>
            <ta e="T673" id="Seg_7579" s="T672">big.[NOM.SG]</ta>
            <ta e="T674" id="Seg_7580" s="T673">be-PST.[3SG]</ta>
            <ta e="T677" id="Seg_7581" s="T676">four.[NOM.SG]</ta>
            <ta e="T678" id="Seg_7582" s="T677">people.[NOM.SG]</ta>
            <ta e="T679" id="Seg_7583" s="T678">lie-PST-3PL</ta>
            <ta e="T680" id="Seg_7584" s="T679">ten.[NOM.SG]</ta>
            <ta e="T681" id="Seg_7585" s="T680">people.[NOM.SG]</ta>
            <ta e="T682" id="Seg_7586" s="T681">when</ta>
            <ta e="T683" id="Seg_7587" s="T682">snow.[NOM.SG]</ta>
            <ta e="T684" id="Seg_7588" s="T683">be-PST.[3SG]</ta>
            <ta e="T685" id="Seg_7589" s="T684">this-PL</ta>
            <ta e="T686" id="Seg_7590" s="T685">here</ta>
            <ta e="T687" id="Seg_7591" s="T686">live-PST-3PL</ta>
            <ta e="T688" id="Seg_7592" s="T687">and</ta>
            <ta e="T689" id="Seg_7593" s="T688">when</ta>
            <ta e="T690" id="Seg_7594" s="T689">snow.[NOM.SG]</ta>
            <ta e="T849" id="Seg_7595" s="T690">go-CVB</ta>
            <ta e="T691" id="Seg_7596" s="T849">disappear-PST.[3SG]</ta>
            <ta e="T692" id="Seg_7597" s="T691">then</ta>
            <ta e="T693" id="Seg_7598" s="T692">this-PL</ta>
            <ta e="T694" id="Seg_7599" s="T693">go-IPFVZ-PRS-3PL</ta>
            <ta e="T695" id="Seg_7600" s="T694">forest-LAT</ta>
            <ta e="T696" id="Seg_7601" s="T695">big.[NOM.SG]</ta>
            <ta e="T697" id="Seg_7602" s="T696">mountain-LAT</ta>
            <ta e="T699" id="Seg_7603" s="T698">reindeer-PL-INS</ta>
            <ta e="T700" id="Seg_7604" s="T699">go-PST-3PL</ta>
            <ta e="T701" id="Seg_7605" s="T700">Sayan.mountains-LAT</ta>
            <ta e="T702" id="Seg_7606" s="T701">go-PST-3PL</ta>
            <ta e="T703" id="Seg_7607" s="T702">there</ta>
            <ta e="T704" id="Seg_7608" s="T703">big.[NOM.SG]</ta>
            <ta e="T705" id="Seg_7609" s="T704">river.[NOM.SG]</ta>
            <ta e="T706" id="Seg_7610" s="T705">fish.[NOM.SG]</ta>
            <ta e="T707" id="Seg_7611" s="T706">capture-PST-3PL</ta>
            <ta e="T709" id="Seg_7612" s="T708">then</ta>
            <ta e="T710" id="Seg_7613" s="T709">this-PL</ta>
            <ta e="T711" id="Seg_7614" s="T710">go-PST-3PL</ta>
            <ta e="T712" id="Seg_7615" s="T711">child-PL-LAT</ta>
            <ta e="T713" id="Seg_7616" s="T712">and</ta>
            <ta e="T714" id="Seg_7617" s="T713">woman-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T715" id="Seg_7618" s="T714">take-PST-3PL</ta>
            <ta e="T716" id="Seg_7619" s="T715">reindeer-PL-INS</ta>
            <ta e="T717" id="Seg_7620" s="T716">go-PST-3PL</ta>
            <ta e="T719" id="Seg_7621" s="T718">and</ta>
            <ta e="T720" id="Seg_7622" s="T719">dog-PL-COM</ta>
            <ta e="T722" id="Seg_7623" s="T721">then</ta>
            <ta e="T723" id="Seg_7624" s="T722">this-PL</ta>
            <ta e="T724" id="Seg_7625" s="T723">forest-LOC</ta>
            <ta e="T725" id="Seg_7626" s="T724">berry.[NOM.SG]</ta>
            <ta e="T726" id="Seg_7627" s="T725">collect-PST-3PL</ta>
            <ta e="T727" id="Seg_7628" s="T726">eat-PST-3PL</ta>
            <ta e="T728" id="Seg_7629" s="T727">pine.nut.[NOM.SG]</ta>
            <ta e="T729" id="Seg_7630" s="T728">collect-PST-3PL</ta>
            <ta e="T730" id="Seg_7631" s="T729">crack-PST-3PL</ta>
            <ta e="T732" id="Seg_7632" s="T731">then</ta>
            <ta e="T733" id="Seg_7633" s="T732">this-PL</ta>
            <ta e="T734" id="Seg_7634" s="T733">mushroom-NOM/GEN/ACC.3PL</ta>
            <ta e="T735" id="Seg_7635" s="T734">collect-PST-3PL</ta>
            <ta e="T736" id="Seg_7636" s="T735">boil-PST-3PL</ta>
            <ta e="T737" id="Seg_7637" s="T736">and</ta>
            <ta e="T738" id="Seg_7638" s="T737">salt-PST-3PL</ta>
            <ta e="T740" id="Seg_7639" s="T739">then</ta>
            <ta e="T741" id="Seg_7640" s="T740">there</ta>
            <ta e="T742" id="Seg_7641" s="T741">this-GEN.PL</ta>
            <ta e="T743" id="Seg_7642" s="T742">there</ta>
            <ta e="T744" id="Seg_7643" s="T743">kill-PST-3PL</ta>
            <ta e="T745" id="Seg_7644" s="T744">%%-NOM/GEN/ACC.3PL</ta>
            <ta e="T746" id="Seg_7645" s="T745">and</ta>
            <ta e="T747" id="Seg_7646" s="T746">meat.[NOM.SG]</ta>
            <ta e="T748" id="Seg_7647" s="T747">salt-PST-3PL</ta>
            <ta e="T749" id="Seg_7648" s="T748">then</ta>
            <ta e="T750" id="Seg_7649" s="T749">hang.up-PST-3PL</ta>
            <ta e="T751" id="Seg_7650" s="T750">this.[NOM.SG]</ta>
            <ta e="T753" id="Seg_7651" s="T752">become.dry-RES-PST.[3SG]</ta>
            <ta e="T754" id="Seg_7652" s="T753">axe-INS</ta>
            <ta e="T755" id="Seg_7653" s="T754">cut-PST-3PL</ta>
            <ta e="T756" id="Seg_7654" s="T755">sack-LAT</ta>
            <ta e="T757" id="Seg_7655" s="T756">pour-PST-3PL</ta>
            <ta e="T759" id="Seg_7656" s="T758">this-PL</ta>
            <ta e="T760" id="Seg_7657" s="T759">then</ta>
            <ta e="T761" id="Seg_7658" s="T760">fish.[NOM.SG]</ta>
            <ta e="T762" id="Seg_7659" s="T761">capture-PST-3PL</ta>
            <ta e="T763" id="Seg_7660" s="T762">and</ta>
            <ta e="T764" id="Seg_7661" s="T763">salt-PST-3PL</ta>
            <ta e="T765" id="Seg_7662" s="T764">and</ta>
            <ta e="T766" id="Seg_7663" s="T765">also</ta>
            <ta e="T767" id="Seg_7664" s="T766">fish.[NOM.SG]</ta>
            <ta e="T769" id="Seg_7665" s="T767">dry-PST-3PL</ta>
            <ta e="T772" id="Seg_7666" s="T771">then</ta>
            <ta e="T773" id="Seg_7667" s="T772">skin-3SG-INS</ta>
            <ta e="T774" id="Seg_7668" s="T773">make-PST-3PL</ta>
            <ta e="T775" id="Seg_7669" s="T774">sack-PL</ta>
            <ta e="T776" id="Seg_7670" s="T775">and</ta>
            <ta e="T777" id="Seg_7671" s="T776">there</ta>
            <ta e="T778" id="Seg_7672" s="T777">put-PST-3PL</ta>
            <ta e="T779" id="Seg_7673" s="T778">meat.[NOM.SG]</ta>
            <ta e="T780" id="Seg_7674" s="T779">and</ta>
            <ta e="T781" id="Seg_7675" s="T780">fish.[NOM.SG]</ta>
            <ta e="T782" id="Seg_7676" s="T781">and</ta>
            <ta e="T783" id="Seg_7677" s="T782">reindeer-PL-INS</ta>
            <ta e="T784" id="Seg_7678" s="T783">bring-PST-3PL</ta>
            <ta e="T785" id="Seg_7679" s="T784">house-LAT</ta>
            <ta e="T786" id="Seg_7680" s="T785">here</ta>
            <ta e="T787" id="Seg_7681" s="T786">where</ta>
            <ta e="T788" id="Seg_7682" s="T787">live-PST-3PL</ta>
            <ta e="T789" id="Seg_7683" s="T788">when</ta>
            <ta e="T790" id="Seg_7684" s="T789">cold.[NOM.SG]</ta>
            <ta e="T791" id="Seg_7685" s="T790">be-PST.[3SG]</ta>
            <ta e="T793" id="Seg_7686" s="T792">this-PL</ta>
            <ta e="T795" id="Seg_7687" s="T794">such-PL</ta>
            <ta e="T796" id="Seg_7688" s="T795">dog-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T798" id="Seg_7689" s="T797">be-PST-3PL</ta>
            <ta e="T800" id="Seg_7690" s="T799">go-FUT-3PL</ta>
            <ta e="T801" id="Seg_7691" s="T800">goat.[NOM.SG]</ta>
            <ta e="T802" id="Seg_7692" s="T801">many</ta>
            <ta e="T803" id="Seg_7693" s="T802">capture-FUT-3PL</ta>
            <ta e="T804" id="Seg_7694" s="T803">then</ta>
            <ta e="T805" id="Seg_7695" s="T804">house-LAT/LOC.3SG</ta>
            <ta e="T806" id="Seg_7696" s="T805">come-FUT-3SG</ta>
            <ta e="T807" id="Seg_7697" s="T806">this.[NOM.SG]</ta>
            <ta e="T808" id="Seg_7698" s="T807">horse.[NOM.SG]</ta>
            <ta e="T809" id="Seg_7699" s="T808">%%-FUT-3SG</ta>
            <ta e="T810" id="Seg_7700" s="T809">then</ta>
            <ta e="T811" id="Seg_7701" s="T810">walk-PRS.[3SG]</ta>
            <ta e="T812" id="Seg_7702" s="T811">goat.[NOM.SG]</ta>
            <ta e="T813" id="Seg_7703" s="T812">collect-INF.LAT</ta>
            <ta e="T814" id="Seg_7704" s="T813">then</ta>
            <ta e="T815" id="Seg_7705" s="T814">come-FUT-3SG</ta>
            <ta e="T816" id="Seg_7706" s="T815">house-LAT/LOC.3SG</ta>
            <ta e="T818" id="Seg_7707" s="T817">and</ta>
            <ta e="T819" id="Seg_7708" s="T818">goat.[NOM.SG]</ta>
            <ta e="T820" id="Seg_7709" s="T819">bring-FUT-3SG</ta>
            <ta e="T823" id="Seg_7710" s="T822">this-ACC.PL</ta>
            <ta e="T825" id="Seg_7711" s="T824">army-LAT</ta>
            <ta e="T826" id="Seg_7712" s="T825">NEG</ta>
            <ta e="T827" id="Seg_7713" s="T826">take-PST-3PL</ta>
            <ta e="T828" id="Seg_7714" s="T827">this-PL</ta>
            <ta e="T829" id="Seg_7715" s="T828">yasak.[NOM.SG]</ta>
            <ta e="T830" id="Seg_7716" s="T829">money.[NOM.SG]</ta>
            <ta e="T831" id="Seg_7717" s="T830">money.[NOM.SG]</ta>
            <ta e="T832" id="Seg_7718" s="T831">give-PST-3PL</ta>
            <ta e="T833" id="Seg_7719" s="T832">and</ta>
            <ta e="T834" id="Seg_7720" s="T833">sable.[NOM.SG]</ta>
            <ta e="T835" id="Seg_7721" s="T834">give-PST-3PL</ta>
            <ta e="T837" id="Seg_7722" s="T836">this-PL</ta>
            <ta e="T838" id="Seg_7723" s="T837">yasak-LAT</ta>
            <ta e="T839" id="Seg_7724" s="T838">go-PST-3PL</ta>
            <ta e="T840" id="Seg_7725" s="T839">money.[NOM.SG]</ta>
            <ta e="T841" id="Seg_7726" s="T840">ten.[NOM.SG]</ta>
            <ta e="T842" id="Seg_7727" s="T841">ten.[NOM.SG]</ta>
            <ta e="T843" id="Seg_7728" s="T842">five.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7729" s="T0">когда</ta>
            <ta e="T2" id="Seg_7730" s="T1">мы.LAT</ta>
            <ta e="T3" id="Seg_7731" s="T2">прийти-PST-2SG</ta>
            <ta e="T4" id="Seg_7732" s="T3">большой.[NOM.SG]</ta>
            <ta e="T5" id="Seg_7733" s="T4">день-LOC</ta>
            <ta e="T6" id="Seg_7734" s="T5">два.[NOM.SG]</ta>
            <ta e="T7" id="Seg_7735" s="T6">день.[NOM.SG]</ta>
            <ta e="T850" id="Seg_7736" s="T7">Агинское</ta>
            <ta e="T8" id="Seg_7737" s="T850">поселение-LOC</ta>
            <ta e="T9" id="Seg_7738" s="T8">сидеть-PST-1SG</ta>
            <ta e="T10" id="Seg_7739" s="T9">что.[NOM.SG]</ta>
            <ta e="T11" id="Seg_7740" s="T10">что.[NOM.SG]</ta>
            <ta e="T12" id="Seg_7741" s="T11">что.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7742" s="T12">видеть-PST-2SG</ta>
            <ta e="T14" id="Seg_7743" s="T13">тогда</ta>
            <ta e="T15" id="Seg_7744" s="T14">здесь</ta>
            <ta e="T16" id="Seg_7745" s="T15">прийти-PST-1SG</ta>
            <ta e="T18" id="Seg_7746" s="T17">смотреть-FRQ-PST-1SG</ta>
            <ta e="T19" id="Seg_7747" s="T18">смотреть-FRQ-PST-1SG</ta>
            <ta e="T20" id="Seg_7748" s="T19">как=INDEF</ta>
            <ta e="T21" id="Seg_7749" s="T20">NEG</ta>
            <ta e="T22" id="Seg_7750" s="T21">мочь-PRS-1SG</ta>
            <ta e="T23" id="Seg_7751" s="T22">NEG</ta>
            <ta e="T24" id="Seg_7752" s="T23">мочь-PRS-1SG</ta>
            <ta e="T25" id="Seg_7753" s="T24">прийти-INF.LAT</ta>
            <ta e="T26" id="Seg_7754" s="T25">здесь</ta>
            <ta e="T27" id="Seg_7755" s="T26">три.[NOM.SG]</ta>
            <ta e="T28" id="Seg_7756" s="T27">день-LOC</ta>
            <ta e="T29" id="Seg_7757" s="T28">тогда</ta>
            <ta e="T30" id="Seg_7758" s="T29">прийти-PST-1SG</ta>
            <ta e="T31" id="Seg_7759" s="T30">здесь</ta>
            <ta e="T33" id="Seg_7760" s="T32">тогда</ta>
            <ta e="T34" id="Seg_7761" s="T33">здесь</ta>
            <ta e="T35" id="Seg_7762" s="T34">прийти-PST-1SG</ta>
            <ta e="T36" id="Seg_7763" s="T35">бабушка-LAT</ta>
            <ta e="T37" id="Seg_7764" s="T36">прийти-PST-1SG</ta>
            <ta e="T38" id="Seg_7765" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_7766" s="T38">я.ACC</ta>
            <ta e="T40" id="Seg_7767" s="T39">PTCL</ta>
            <ta e="T41" id="Seg_7768" s="T40">кормить-PST.[3SG]</ta>
            <ta e="T42" id="Seg_7769" s="T41">суп.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7770" s="T42">дать-PST.[3SG]</ta>
            <ta e="T45" id="Seg_7771" s="T44">тогда</ta>
            <ta e="T46" id="Seg_7772" s="T45">здесь</ta>
            <ta e="T47" id="Seg_7773" s="T46">прийти-PST-1SG</ta>
            <ta e="T48" id="Seg_7774" s="T47">теленок.[NOM.SG]</ta>
            <ta e="T49" id="Seg_7775" s="T48">кричать-DUR.[3SG]</ta>
            <ta e="T50" id="Seg_7776" s="T49">женщина.[NOM.SG]</ta>
            <ta e="T51" id="Seg_7777" s="T50">PTCL</ta>
            <ta e="T52" id="Seg_7778" s="T51">дать-PST.[3SG]</ta>
            <ta e="T53" id="Seg_7779" s="T52">этот-LAT</ta>
            <ta e="T54" id="Seg_7780" s="T53">картофель.[NOM.SG]</ta>
            <ta e="T55" id="Seg_7781" s="T54">этот.[NOM.SG]</ta>
            <ta e="T56" id="Seg_7782" s="T55">есть-DUR.[3SG]</ta>
            <ta e="T57" id="Seg_7783" s="T56">NEG</ta>
            <ta e="T58" id="Seg_7784" s="T57">кричать-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_7785" s="T59">этот.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7786" s="T60">женщина-LOC</ta>
            <ta e="T62" id="Seg_7787" s="T61">дом-LOC</ta>
            <ta e="T63" id="Seg_7788" s="T62">холодный.[NOM.SG]</ta>
            <ta e="T64" id="Seg_7789" s="T63">печь.[NOM.SG]</ta>
            <ta e="T65" id="Seg_7790" s="T64">светить-PST.[3SG]</ta>
            <ta e="T67" id="Seg_7791" s="T66">снаружи</ta>
            <ta e="T68" id="Seg_7792" s="T67">PTCL</ta>
            <ta e="T69" id="Seg_7793" s="T68">холодный.[NOM.SG]</ta>
            <ta e="T70" id="Seg_7794" s="T69">очень</ta>
            <ta e="T71" id="Seg_7795" s="T70">теленок</ta>
            <ta e="T72" id="Seg_7796" s="T71">дом-LOC</ta>
            <ta e="T73" id="Seg_7797" s="T72">стоять-PRS.[3SG]</ta>
            <ta e="T74" id="Seg_7798" s="T73">PTCL</ta>
            <ta e="T76" id="Seg_7799" s="T75">корова.[NOM.SG]</ta>
            <ta e="T77" id="Seg_7800" s="T76">корова.[NOM.SG]</ta>
            <ta e="T78" id="Seg_7801" s="T77">рожать-MOM-PST.[3SG]</ta>
            <ta e="T79" id="Seg_7802" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_7803" s="T79">маленький.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7804" s="T80">теленок.[NOM.SG]</ta>
            <ta e="T83" id="Seg_7805" s="T82">мы.NOM</ta>
            <ta e="T84" id="Seg_7806" s="T83">поселение-LOC</ta>
            <ta e="T85" id="Seg_7807" s="T84">очень</ta>
            <ta e="T86" id="Seg_7808" s="T85">много</ta>
            <ta e="T87" id="Seg_7809" s="T86">люди.[NOM.SG]</ta>
            <ta e="T88" id="Seg_7810" s="T87">умереть-PST-3PL</ta>
            <ta e="T90" id="Seg_7811" s="T89">идти-PST-1SG</ta>
            <ta e="T91" id="Seg_7812" s="T90">женщина-LAT</ta>
            <ta e="T92" id="Seg_7813" s="T91">этот.[NOM.SG]</ta>
            <ta e="T94" id="Seg_7814" s="T93">очень</ta>
            <ta e="T95" id="Seg_7815" s="T94">болеть-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_7816" s="T95">я.NOM</ta>
            <ta e="T97" id="Seg_7817" s="T96">сегодня</ta>
            <ta e="T99" id="Seg_7818" s="T98">умереть-RES-FUT-1SG</ta>
            <ta e="T100" id="Seg_7819" s="T99">ну</ta>
            <ta e="T101" id="Seg_7820" s="T100">умереть-IMP.2SG</ta>
            <ta e="T102" id="Seg_7821" s="T101">ну</ta>
            <ta e="T103" id="Seg_7822" s="T102">умереть-IMP.2SG</ta>
            <ta e="T105" id="Seg_7823" s="T104">завтра</ta>
            <ta e="T107" id="Seg_7824" s="T106">прийти-FUT-1SG</ta>
            <ta e="T108" id="Seg_7825" s="T107">мыть-INF.LAT</ta>
            <ta e="T109" id="Seg_7826" s="T108">ты.ACC</ta>
            <ta e="T110" id="Seg_7827" s="T109">тогда</ta>
            <ta e="T111" id="Seg_7828" s="T110">NEG</ta>
            <ta e="T112" id="Seg_7829" s="T111">пойти-PST-1SG</ta>
            <ta e="T113" id="Seg_7830" s="T112">трава.[NOM.SG]</ta>
            <ta e="T114" id="Seg_7831" s="T113">принести-PST-3PL</ta>
            <ta e="T115" id="Seg_7832" s="T114">два.[NOM.SG]</ta>
            <ta e="T116" id="Seg_7833" s="T115">мужчина.[NOM.SG]</ta>
            <ta e="T117" id="Seg_7834" s="T116">тогда</ta>
            <ta e="T118" id="Seg_7835" s="T117">прийти-PST-1SG</ta>
            <ta e="T119" id="Seg_7836" s="T118">что.[NOM.SG]</ta>
            <ta e="T120" id="Seg_7837" s="T119">долго</ta>
            <ta e="T121" id="Seg_7838" s="T120">NEG</ta>
            <ta e="T122" id="Seg_7839" s="T121">прийти-PST-2SG</ta>
            <ta e="T123" id="Seg_7840" s="T122">а</ta>
            <ta e="T124" id="Seg_7841" s="T123">я.NOM</ta>
            <ta e="T125" id="Seg_7842" s="T124">думать-PST-1SG</ta>
            <ta e="T126" id="Seg_7843" s="T125">ты.NOM</ta>
            <ta e="T127" id="Seg_7844" s="T126">умереть-RES-PST-1SG</ta>
            <ta e="T128" id="Seg_7845" s="T127">мыть-INF.LAT</ta>
            <ta e="T129" id="Seg_7846" s="T128">прийти-PST-1SG</ta>
            <ta e="T131" id="Seg_7847" s="T130">теленок.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7848" s="T131">%спать-DUR.[3SG]</ta>
            <ta e="T133" id="Seg_7849" s="T132">NEG</ta>
            <ta e="T134" id="Seg_7850" s="T133">кричать-DUR.[3SG]</ta>
            <ta e="T136" id="Seg_7851" s="T135">ребенок-PL</ta>
            <ta e="T137" id="Seg_7852" s="T136">PTCL</ta>
            <ta e="T138" id="Seg_7853" s="T137">бежать-DUR-3PL</ta>
            <ta e="T139" id="Seg_7854" s="T138">дом-GEN</ta>
            <ta e="T141" id="Seg_7855" s="T140">дом-GEN</ta>
            <ta e="T142" id="Seg_7856" s="T141">край-LAT/LOC.3SG</ta>
            <ta e="T144" id="Seg_7857" s="T143">два.[NOM.SG]</ta>
            <ta e="T145" id="Seg_7858" s="T144">мальчик.[NOM.SG]</ta>
            <ta e="T146" id="Seg_7859" s="T145">прийти-PST-3PL</ta>
            <ta e="T147" id="Seg_7860" s="T146">слушать-INF.LAT</ta>
            <ta e="T150" id="Seg_7861" s="T149">NEG.AUX-IMP.2SG</ta>
            <ta e="T151" id="Seg_7862" s="T150">кричать-CNG</ta>
            <ta e="T152" id="Seg_7863" s="T151">NEG.AUX-IMP.2SG</ta>
            <ta e="T153" id="Seg_7864" s="T152">шуметь-CNG</ta>
            <ta e="T154" id="Seg_7865" s="T153">слушать-2PL</ta>
            <ta e="T156" id="Seg_7866" s="T155">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T157" id="Seg_7867" s="T156">когда</ta>
            <ta e="T158" id="Seg_7868" s="T157">прийти-FUT-3SG</ta>
            <ta e="T159" id="Seg_7869" s="T158">скоро</ta>
            <ta e="T160" id="Seg_7870" s="T159">прийти-FUT-3SG</ta>
            <ta e="T162" id="Seg_7871" s="T161">надо</ta>
            <ta e="T163" id="Seg_7872" s="T162">вода.[NOM.SG]</ta>
            <ta e="T164" id="Seg_7873" s="T163">теплый-FACT-INF.LAT</ta>
            <ta e="T165" id="Seg_7874" s="T164">тогда</ta>
            <ta e="T166" id="Seg_7875" s="T165">корова.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7876" s="T166">пить-TR-INF.LAT</ta>
            <ta e="T169" id="Seg_7877" s="T168">что.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7878" s="T169">слушать-INF.LAT</ta>
            <ta e="T171" id="Seg_7879" s="T170">прийти-PST-2PL</ta>
            <ta e="T172" id="Seg_7880" s="T171">смотреть-FRQ-INF.LAT</ta>
            <ta e="T173" id="Seg_7881" s="T172">прийти-PST-2PL</ta>
            <ta e="T175" id="Seg_7882" s="T174">NEG</ta>
            <ta e="T176" id="Seg_7883" s="T175">кричать-PRS-3PL</ta>
            <ta e="T177" id="Seg_7884" s="T176">красивый.[NOM.SG]</ta>
            <ta e="T178" id="Seg_7885" s="T177">мальчик-PL</ta>
            <ta e="T179" id="Seg_7886" s="T178">NEG</ta>
            <ta e="T180" id="Seg_7887" s="T179">кричать-PRS-3PL</ta>
            <ta e="T181" id="Seg_7888" s="T180">стоять-DUR-3PL</ta>
            <ta e="T182" id="Seg_7889" s="T181">хороший-PL</ta>
            <ta e="T183" id="Seg_7890" s="T182">очень</ta>
            <ta e="T185" id="Seg_7891" s="T184">кедровый.орех.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7892" s="T185">сегодня</ta>
            <ta e="T187" id="Seg_7893" s="T186">дать-PST-1SG</ta>
            <ta e="T188" id="Seg_7894" s="T187">ты.DAT</ta>
            <ta e="T189" id="Seg_7895" s="T188">лущить-INF.LAT</ta>
            <ta e="T191" id="Seg_7896" s="T190">глаз-NOM/GEN.3SG</ta>
            <ta e="T192" id="Seg_7897" s="T191">смотреть-FRQ-INF.LAT</ta>
            <ta e="T194" id="Seg_7898" s="T193">ухо-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T195" id="Seg_7899" s="T194">слушать-INF.LAT</ta>
            <ta e="T196" id="Seg_7900" s="T195">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T197" id="Seg_7901" s="T196">съесть-INF.LAT</ta>
            <ta e="T198" id="Seg_7902" s="T197">зуб-PL</ta>
            <ta e="T199" id="Seg_7903" s="T198">укусить-INF.LAT</ta>
            <ta e="T200" id="Seg_7904" s="T199">хлеб.[NOM.SG]</ta>
            <ta e="T202" id="Seg_7905" s="T201">думать-PST-1SG</ta>
            <ta e="T203" id="Seg_7906" s="T202">думать-PST-1SG</ta>
            <ta e="T204" id="Seg_7907" s="T203">как</ta>
            <ta e="T205" id="Seg_7908" s="T204">здесь</ta>
            <ta e="T206" id="Seg_7909" s="T205">прийти-INF.LAT</ta>
            <ta e="T208" id="Seg_7910" s="T207">рукавица.[NOM.SG]</ta>
            <ta e="T209" id="Seg_7911" s="T208">земля-LOC</ta>
            <ta e="T210" id="Seg_7912" s="T209">лежать-DUR-PST.[3SG]</ta>
            <ta e="T211" id="Seg_7913" s="T210">хлеб.[NOM.SG]</ta>
            <ta e="T212" id="Seg_7914" s="T211">класть-PST-3PL</ta>
            <ta e="T213" id="Seg_7915" s="T212">тогда</ta>
            <ta e="T214" id="Seg_7916" s="T213">есть-PST-3PL</ta>
            <ta e="T215" id="Seg_7917" s="T214">этот</ta>
            <ta e="T216" id="Seg_7918" s="T215">хлеб.[NOM.SG]</ta>
            <ta e="T217" id="Seg_7919" s="T216">и</ta>
            <ta e="T218" id="Seg_7920" s="T217">мясо.[NOM.SG]</ta>
            <ta e="T222" id="Seg_7921" s="T221">тогда</ta>
            <ta e="T223" id="Seg_7922" s="T222">съесть-PST-3PL</ta>
            <ta e="T224" id="Seg_7923" s="T223">тогда</ta>
            <ta e="T225" id="Seg_7924" s="T224">бог-LAT</ta>
            <ta e="T226" id="Seg_7925" s="T225">молиться-PST-3PL</ta>
            <ta e="T851" id="Seg_7926" s="T227">Агинское</ta>
            <ta e="T228" id="Seg_7927" s="T851">поселение-LAT</ta>
            <ta e="T229" id="Seg_7928" s="T228">идти-PST-1SG</ta>
            <ta e="T230" id="Seg_7929" s="T229">церковь-LOC</ta>
            <ta e="T231" id="Seg_7930" s="T230">церковь-LOC</ta>
            <ta e="T233" id="Seg_7931" s="T232">бог.[NOM.SG]</ta>
            <ta e="T235" id="Seg_7932" s="T234">молиться-PST-1SG</ta>
            <ta e="T237" id="Seg_7933" s="T236">много</ta>
            <ta e="T238" id="Seg_7934" s="T237">люди.[NOM.SG]</ta>
            <ta e="T239" id="Seg_7935" s="T238">умереть-RES-PST-3PL</ta>
            <ta e="T241" id="Seg_7936" s="T240">крест-PL</ta>
            <ta e="T242" id="Seg_7937" s="T241">очень</ta>
            <ta e="T243" id="Seg_7938" s="T242">много</ta>
            <ta e="T244" id="Seg_7939" s="T243">стоять-PRS-3PL</ta>
            <ta e="T245" id="Seg_7940" s="T244">PTCL</ta>
            <ta e="T247" id="Seg_7941" s="T246">мы.NOM</ta>
            <ta e="T248" id="Seg_7942" s="T247">девушка.[NOM.SG]</ta>
            <ta e="T249" id="Seg_7943" s="T248">болеть-PST.[3SG]</ta>
            <ta e="T250" id="Seg_7944" s="T249">а</ta>
            <ta e="T251" id="Seg_7945" s="T250">отец-NOM/GEN.3SG</ta>
            <ta e="T253" id="Seg_7946" s="T252">нести-PST.[3SG]</ta>
            <ta e="T255" id="Seg_7947" s="T254">вылечить-INF.LAT</ta>
            <ta e="T258" id="Seg_7948" s="T257">мы.LAT</ta>
            <ta e="T259" id="Seg_7949" s="T258">сегодня</ta>
            <ta e="T260" id="Seg_7950" s="T259">вечер-LOC.ADV</ta>
            <ta e="T261" id="Seg_7951" s="T260">мужчина.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7952" s="T261">женщина-COM</ta>
            <ta e="T263" id="Seg_7953" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_7954" s="T263">бороться-PST-3PL</ta>
            <ta e="T265" id="Seg_7955" s="T264">рука-LOC</ta>
            <ta e="T266" id="Seg_7956" s="T265">PTCL</ta>
            <ta e="T269" id="Seg_7957" s="T268">кровь.[NOM.SG]</ta>
            <ta e="T270" id="Seg_7958" s="T269">течь-DUR.[3SG]</ta>
            <ta e="T271" id="Seg_7959" s="T270">ребенок-PL-LAT</ta>
            <ta e="T272" id="Seg_7960" s="T271">PTCL</ta>
            <ta e="T273" id="Seg_7961" s="T272">кричать-DUR-3PL</ta>
            <ta e="T274" id="Seg_7962" s="T273">я.NOM</ta>
            <ta e="T275" id="Seg_7963" s="T274">прийти-PST-1SG</ta>
            <ta e="T276" id="Seg_7964" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_7965" s="T276">ругать-DES-PST-1SG</ta>
            <ta e="T279" id="Seg_7966" s="T278">теленок.[NOM.SG]</ta>
            <ta e="T280" id="Seg_7967" s="T279">лежать-PST.[3SG]</ta>
            <ta e="T281" id="Seg_7968" s="T280">NEG</ta>
            <ta e="T282" id="Seg_7969" s="T281">кричать-PRS.[3SG]</ta>
            <ta e="T283" id="Seg_7970" s="T282">сейчас</ta>
            <ta e="T285" id="Seg_7971" s="T284">можно</ta>
            <ta e="T286" id="Seg_7972" s="T285">говорить-INF.LAT</ta>
            <ta e="T288" id="Seg_7973" s="T287">мужчина-GEN</ta>
            <ta e="T289" id="Seg_7974" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_7975" s="T289">нос-NOM/GEN.3SG</ta>
            <ta e="T291" id="Seg_7976" s="T290">чесаться-PST.[3SG]</ta>
            <ta e="T292" id="Seg_7977" s="T291">где=INDEF</ta>
            <ta e="T293" id="Seg_7978" s="T292">водка.[NOM.SG]</ta>
            <ta e="T294" id="Seg_7979" s="T293">пить-FUT-1SG</ta>
            <ta e="T295" id="Seg_7980" s="T294">тогда</ta>
            <ta e="T296" id="Seg_7981" s="T295">%%</ta>
            <ta e="T297" id="Seg_7982" s="T296">делать-PST.[3SG]</ta>
            <ta e="T298" id="Seg_7983" s="T297">сидеть-PST.[3SG]</ta>
            <ta e="T299" id="Seg_7984" s="T298">дерево-LOC</ta>
            <ta e="T300" id="Seg_7985" s="T299">а</ta>
            <ta e="T301" id="Seg_7986" s="T300">я.NOM</ta>
            <ta e="T302" id="Seg_7987" s="T301">бабушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T303" id="Seg_7988" s="T302">идти-PST.[3SG]</ta>
            <ta e="T852" id="Seg_7989" s="T303">Агинское</ta>
            <ta e="T304" id="Seg_7990" s="T852">поселение-LAT</ta>
            <ta e="T305" id="Seg_7991" s="T304">и</ta>
            <ta e="T306" id="Seg_7992" s="T305">водка.[NOM.SG]</ta>
            <ta e="T307" id="Seg_7993" s="T306">принести-PST.[3SG]</ta>
            <ta e="T308" id="Seg_7994" s="T307">тогда</ta>
            <ta e="T309" id="Seg_7995" s="T308">сесть-PST.[3SG]</ta>
            <ta e="T310" id="Seg_7996" s="T309">испражняться-INF.LAT</ta>
            <ta e="T312" id="Seg_7997" s="T311">этот-LAT</ta>
            <ta e="T313" id="Seg_7998" s="T312">зад-INS</ta>
            <ta e="T314" id="Seg_7999" s="T313">тогда</ta>
            <ta e="T315" id="Seg_8000" s="T314">этот.[NOM.SG]</ta>
            <ta e="T316" id="Seg_8001" s="T315">прийти-PST.[3SG]</ta>
            <ta e="T317" id="Seg_8002" s="T316">этот.[NOM.SG]</ta>
            <ta e="T318" id="Seg_8003" s="T317">этот-LAT</ta>
            <ta e="T319" id="Seg_8004" s="T318">водка.[NOM.SG]</ta>
            <ta e="T320" id="Seg_8005" s="T319">дать-PST.[3SG]</ta>
            <ta e="T321" id="Seg_8006" s="T320">этот.[NOM.SG]</ta>
            <ta e="T322" id="Seg_8007" s="T321">пить-PST.[3SG]</ta>
            <ta e="T323" id="Seg_8008" s="T322">и</ta>
            <ta e="T324" id="Seg_8009" s="T323">сказать-IPFVZ.[3SG]</ta>
            <ta e="T325" id="Seg_8010" s="T324">сказать-IPFVZ.[3SG]</ta>
            <ta e="T326" id="Seg_8011" s="T325">этот-LAT</ta>
            <ta e="T327" id="Seg_8012" s="T326">женщина.[NOM.SG]</ta>
            <ta e="T328" id="Seg_8013" s="T327">так</ta>
            <ta e="T329" id="Seg_8014" s="T328">женщина.[NOM.SG]</ta>
            <ta e="T330" id="Seg_8015" s="T329">вот</ta>
            <ta e="T331" id="Seg_8016" s="T330">так</ta>
            <ta e="T332" id="Seg_8017" s="T331">женщина.[NOM.SG]</ta>
            <ta e="T333" id="Seg_8018" s="T332">так</ta>
            <ta e="T334" id="Seg_8019" s="T333">женщина.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8020" s="T334">что.[NOM.SG]</ta>
            <ta e="T336" id="Seg_8021" s="T335">ты.NOM</ta>
            <ta e="T337" id="Seg_8022" s="T336">так</ta>
            <ta e="T340" id="Seg_8023" s="T339">а</ta>
            <ta e="T341" id="Seg_8024" s="T340">ты.NOM</ta>
            <ta e="T342" id="Seg_8025" s="T341">я.LAT</ta>
            <ta e="T344" id="Seg_8026" s="T343">зад-INS</ta>
            <ta e="T345" id="Seg_8027" s="T344">сидеть-PST-2SG</ta>
            <ta e="T346" id="Seg_8028" s="T345">и</ta>
            <ta e="T347" id="Seg_8029" s="T346">испражняться-PST-2SG</ta>
            <ta e="T350" id="Seg_8030" s="T349">я.NOM</ta>
            <ta e="T351" id="Seg_8031" s="T350">бабушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T352" id="Seg_8032" s="T351">шаман-NOM/GEN/ACC.3SG</ta>
            <ta e="T353" id="Seg_8033" s="T352">идти-PST-1SG</ta>
            <ta e="T354" id="Seg_8034" s="T353">там</ta>
            <ta e="T355" id="Seg_8035" s="T354">PTCL</ta>
            <ta e="T357" id="Seg_8036" s="T356">мясо.[NOM.SG]</ta>
            <ta e="T358" id="Seg_8037" s="T357">кипятить-PST-3PL</ta>
            <ta e="T359" id="Seg_8038" s="T358">чашка-LAT</ta>
            <ta e="T360" id="Seg_8039" s="T359">большой.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8040" s="T360">чашка.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8041" s="T361">класть-PST-3PL</ta>
            <ta e="T363" id="Seg_8042" s="T362">этот.[NOM.SG]</ta>
            <ta e="T364" id="Seg_8043" s="T363">PTCL</ta>
            <ta e="T365" id="Seg_8044" s="T364">холодный.[NOM.SG]</ta>
            <ta e="T366" id="Seg_8045" s="T365">стать-RES-PST.[3SG]</ta>
            <ta e="T367" id="Seg_8046" s="T366">сидеть-IMP.2SG</ta>
            <ta e="T369" id="Seg_8047" s="T368">есть-INF.LAT</ta>
            <ta e="T370" id="Seg_8048" s="T369">этот.[NOM.SG]</ta>
            <ta e="T371" id="Seg_8049" s="T370">PTCL</ta>
            <ta e="T372" id="Seg_8050" s="T371">мясо.[NOM.SG]</ta>
            <ta e="T373" id="Seg_8051" s="T372">взять-PST.[3SG]</ta>
            <ta e="T374" id="Seg_8052" s="T373">этот.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8053" s="T374">очень</ta>
            <ta e="T376" id="Seg_8054" s="T375">холодный.[NOM.SG]</ta>
            <ta e="T377" id="Seg_8055" s="T376">тогда</ta>
            <ta e="T378" id="Seg_8056" s="T377">собака-NOM/GEN/ACC.3SG</ta>
            <ta e="T380" id="Seg_8057" s="T379">бросить-MOM-PST.[3SG]</ta>
            <ta e="T382" id="Seg_8058" s="T381">бубен-INS</ta>
            <ta e="T383" id="Seg_8059" s="T382">PTCL</ta>
            <ta e="T385" id="Seg_8060" s="T384">играть-PST.[3SG]</ta>
            <ta e="T386" id="Seg_8061" s="T385">весь</ta>
            <ta e="T387" id="Seg_8062" s="T386">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T388" id="Seg_8063" s="T387">идти-PST.[3SG]</ta>
            <ta e="T389" id="Seg_8064" s="T388">а</ta>
            <ta e="T390" id="Seg_8065" s="T389">я.NOM</ta>
            <ta e="T391" id="Seg_8066" s="T390">бабушка-LAT</ta>
            <ta e="T392" id="Seg_8067" s="T391">NEG</ta>
            <ta e="T393" id="Seg_8068" s="T392">дать-PST-1SG</ta>
            <ta e="T394" id="Seg_8069" s="T393">тогда</ta>
            <ta e="T395" id="Seg_8070" s="T394">идти-PST.[3SG]</ta>
            <ta e="T396" id="Seg_8071" s="T395">NEG.AUX-IMP.2SG</ta>
            <ta e="T397" id="Seg_8072" s="T396">принести-CNG</ta>
            <ta e="T398" id="Seg_8073" s="T397">этот.[NOM.SG]</ta>
            <ta e="T399" id="Seg_8074" s="T398">женщина.[NOM.SG]</ta>
            <ta e="T400" id="Seg_8075" s="T399">русский</ta>
            <ta e="T401" id="Seg_8076" s="T400">женщина.[NOM.SG]</ta>
            <ta e="T404" id="Seg_8077" s="T403">шаман-LOC</ta>
            <ta e="T405" id="Seg_8078" s="T404">PTCL</ta>
            <ta e="T406" id="Seg_8079" s="T405">парка.[NOM.SG]</ta>
            <ta e="T407" id="Seg_8080" s="T406">быть-PST.[3SG]</ta>
            <ta e="T411" id="Seg_8081" s="T410">PTCL</ta>
            <ta e="T413" id="Seg_8082" s="T412">курица-GEN</ta>
            <ta e="T414" id="Seg_8083" s="T413">кожа.[NOM.SG]</ta>
            <ta e="T415" id="Seg_8084" s="T414">соболь-GEN</ta>
            <ta e="T416" id="Seg_8085" s="T415">кожа.[NOM.SG]</ta>
            <ta e="T417" id="Seg_8086" s="T416">парка-LOC</ta>
            <ta e="T418" id="Seg_8087" s="T417">белка-EP-GEN</ta>
            <ta e="T420" id="Seg_8088" s="T419">кожа.[NOM.SG]</ta>
            <ta e="T422" id="Seg_8089" s="T420">парка-LOC</ta>
            <ta e="T423" id="Seg_8090" s="T422">коза-GEN</ta>
            <ta e="T424" id="Seg_8091" s="T423">кожа.[NOM.SG]</ta>
            <ta e="T426" id="Seg_8092" s="T425">этот.[NOM.SG]</ta>
            <ta e="T427" id="Seg_8093" s="T426">шаман.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8094" s="T427">PTCL</ta>
            <ta e="T432" id="Seg_8095" s="T431">тогда</ta>
            <ta e="T433" id="Seg_8096" s="T432">нож.[NOM.SG]</ta>
            <ta e="T434" id="Seg_8097" s="T433">взять-PST.[3SG]</ta>
            <ta e="T435" id="Seg_8098" s="T434">и</ta>
            <ta e="T436" id="Seg_8099" s="T435">PTCL</ta>
            <ta e="T437" id="Seg_8100" s="T436">уколоть-PST.[3SG]</ta>
            <ta e="T439" id="Seg_8101" s="T438">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T440" id="Seg_8102" s="T439">и</ta>
            <ta e="T441" id="Seg_8103" s="T440">тогда</ta>
            <ta e="T442" id="Seg_8104" s="T441">упасть-MOM-PST.[3SG]</ta>
            <ta e="T443" id="Seg_8105" s="T442">а</ta>
            <ta e="T444" id="Seg_8106" s="T443">люди.[NOM.SG]</ta>
            <ta e="T445" id="Seg_8107" s="T444">много</ta>
            <ta e="T446" id="Seg_8108" s="T445">прийти-PRS-3PL</ta>
            <ta e="T457" id="Seg_8109" s="T456">мужчина-PL</ta>
            <ta e="T460" id="Seg_8110" s="T459">когда</ta>
            <ta e="T461" id="Seg_8111" s="T460">пойти-FUT-3PL</ta>
            <ta e="T462" id="Seg_8112" s="T461">лес-LAT</ta>
            <ta e="T463" id="Seg_8113" s="T462">тогда</ta>
            <ta e="T464" id="Seg_8114" s="T463">шаман-LAT</ta>
            <ta e="T465" id="Seg_8115" s="T464">пойти-FUT-3PL</ta>
            <ta e="T466" id="Seg_8116" s="T465">этот.[NOM.SG]</ta>
            <ta e="T467" id="Seg_8117" s="T466">сказать-FUT-3SG</ta>
            <ta e="T468" id="Seg_8118" s="T467">ты.NOM</ta>
            <ta e="T470" id="Seg_8119" s="T469">соболь.[NOM.SG]</ta>
            <ta e="T471" id="Seg_8120" s="T470">убить-FUT-2SG</ta>
            <ta e="T472" id="Seg_8121" s="T471">а</ta>
            <ta e="T473" id="Seg_8122" s="T472">кто-LAT</ta>
            <ta e="T474" id="Seg_8123" s="T473">сказать-FUT-3SG</ta>
            <ta e="T475" id="Seg_8124" s="T474">NEG</ta>
            <ta e="T476" id="Seg_8125" s="T475">убить-FUT-2SG</ta>
            <ta e="T479" id="Seg_8126" s="T478">шаман.[NOM.SG]</ta>
            <ta e="T480" id="Seg_8127" s="T479">и</ta>
            <ta e="T481" id="Seg_8128" s="T480">мы.NOM</ta>
            <ta e="T482" id="Seg_8129" s="T481">шаман.[NOM.SG]</ta>
            <ta e="T483" id="Seg_8130" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_8131" s="T483">бороться-PST-3PL</ta>
            <ta e="T487" id="Seg_8132" s="T486">пойти-PST.[3SG]</ta>
            <ta e="T488" id="Seg_8133" s="T487">дом-LAT/LOC.3SG</ta>
            <ta e="T489" id="Seg_8134" s="T488">а</ta>
            <ta e="T490" id="Seg_8135" s="T489">мы.NOM</ta>
            <ta e="T491" id="Seg_8136" s="T490">шаман.[NOM.SG]</ta>
            <ta e="T493" id="Seg_8137" s="T492">этот-ACC</ta>
            <ta e="T494" id="Seg_8138" s="T493">PTCL</ta>
            <ta e="T495" id="Seg_8139" s="T494">догонять-DUR-PST.[3SG]</ta>
            <ta e="T496" id="Seg_8140" s="T495">%%</ta>
            <ta e="T497" id="Seg_8141" s="T496">стать-RES-PST.[3SG]</ta>
            <ta e="T498" id="Seg_8142" s="T497">а</ta>
            <ta e="T499" id="Seg_8143" s="T498">этот.[NOM.SG]</ta>
            <ta e="T500" id="Seg_8144" s="T499">PTCL</ta>
            <ta e="T501" id="Seg_8145" s="T500">ружье-INS</ta>
            <ta e="T502" id="Seg_8146" s="T501">этот-ACC</ta>
            <ta e="T503" id="Seg_8147" s="T502">убить-RES-PST.[3SG]</ta>
            <ta e="T504" id="Seg_8148" s="T503">рука-LAT</ta>
            <ta e="T505" id="Seg_8149" s="T504">рука-ACC.3SG</ta>
            <ta e="T506" id="Seg_8150" s="T505">PTCL</ta>
            <ta e="T507" id="Seg_8151" s="T506">сломать-PST.[3SG]</ta>
            <ta e="T511" id="Seg_8152" s="T510">какой</ta>
            <ta e="T512" id="Seg_8153" s="T511">человек.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8154" s="T512">болеть-MOM-PST.[3SG]</ta>
            <ta e="T514" id="Seg_8155" s="T513">тогда</ta>
            <ta e="T515" id="Seg_8156" s="T514">пойти-FUT-3PL</ta>
            <ta e="T516" id="Seg_8157" s="T515">шаман-LAT</ta>
            <ta e="T517" id="Seg_8158" s="T516">этот.[NOM.SG]</ta>
            <ta e="T518" id="Seg_8159" s="T517">PTCL</ta>
            <ta e="T519" id="Seg_8160" s="T518">этот-ACC</ta>
            <ta e="T520" id="Seg_8161" s="T519">вылечить-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_8162" s="T520">левый</ta>
            <ta e="T522" id="Seg_8163" s="T521">рука-LAT/LOC.3SG</ta>
            <ta e="T523" id="Seg_8164" s="T522">поставить-FUT-3SG</ta>
            <ta e="T524" id="Seg_8165" s="T523">этот.[NOM.SG]</ta>
            <ta e="T525" id="Seg_8166" s="T524">всегда</ta>
            <ta e="T526" id="Seg_8167" s="T525">так</ta>
            <ta e="T527" id="Seg_8168" s="T526">PTCL</ta>
            <ta e="T528" id="Seg_8169" s="T527">голова-3SG-INS</ta>
            <ta e="T532" id="Seg_8170" s="T531">тогда</ta>
            <ta e="T533" id="Seg_8171" s="T532">этот.[NOM.SG]</ta>
            <ta e="T534" id="Seg_8172" s="T533">этот-LAT</ta>
            <ta e="T535" id="Seg_8173" s="T534">%%-MOM-PST.[3SG]</ta>
            <ta e="T536" id="Seg_8174" s="T535">и</ta>
            <ta e="T537" id="Seg_8175" s="T536">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T538" id="Seg_8176" s="T537">PTCL</ta>
            <ta e="T539" id="Seg_8177" s="T538">прыгнуть-DUR.[3SG]</ta>
            <ta e="T540" id="Seg_8178" s="T539">бубен-LAT</ta>
            <ta e="T541" id="Seg_8179" s="T540">играть-DUR.[3SG]</ta>
            <ta e="T543" id="Seg_8180" s="T542">тогда</ta>
            <ta e="T544" id="Seg_8181" s="T543">этот.[NOM.SG]</ta>
            <ta e="T545" id="Seg_8182" s="T544">NEG</ta>
            <ta e="T546" id="Seg_8183" s="T545">болеть-PRS.[3SG]</ta>
            <ta e="T547" id="Seg_8184" s="T546">дом-LAT/LOC.3SG</ta>
            <ta e="T548" id="Seg_8185" s="T547">прийти-FUT-3SG</ta>
            <ta e="T551" id="Seg_8186" s="T550">мужчина-PL</ta>
            <ta e="T552" id="Seg_8187" s="T551">лес-LAT</ta>
            <ta e="T848" id="Seg_8188" s="T552">пойти-CVB</ta>
            <ta e="T553" id="Seg_8189" s="T848">исчезнуть-PST-3PL</ta>
            <ta e="T554" id="Seg_8190" s="T553">женщина-PL</ta>
            <ta e="T555" id="Seg_8191" s="T554">дома-LOC</ta>
            <ta e="T556" id="Seg_8192" s="T555">жить-PST-3PL</ta>
            <ta e="T557" id="Seg_8193" s="T556">саранка-PL</ta>
            <ta e="T558" id="Seg_8194" s="T557">копать-PST-3PL</ta>
            <ta e="T559" id="Seg_8195" s="T558">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T560" id="Seg_8196" s="T559">класть-PST-3PL</ta>
            <ta e="T561" id="Seg_8197" s="T560">хлеб.[NOM.SG]</ta>
            <ta e="T562" id="Seg_8198" s="T561">NEG.EX.[3SG]</ta>
            <ta e="T564" id="Seg_8199" s="T563">женщина-PL</ta>
            <ta e="T565" id="Seg_8200" s="T564">PTCL</ta>
            <ta e="T567" id="Seg_8201" s="T566">трава.[NOM.SG]</ta>
            <ta e="T568" id="Seg_8202" s="T567">рвать-PST-3PL</ta>
            <ta e="T569" id="Seg_8203" s="T568">%%-PL</ta>
            <ta e="T570" id="Seg_8204" s="T569">плести-PST-3PL</ta>
            <ta e="T571" id="Seg_8205" s="T570">сапог-PL</ta>
            <ta e="T572" id="Seg_8206" s="T571">шить-PST-3PL</ta>
            <ta e="T573" id="Seg_8207" s="T572">жила-PL</ta>
            <ta e="T574" id="Seg_8208" s="T573">плести-PST-3PL</ta>
            <ta e="T575" id="Seg_8209" s="T574">лес-LOC</ta>
            <ta e="T576" id="Seg_8210" s="T575">чум.[NOM.SG]</ta>
            <ta e="T578" id="Seg_8211" s="T577">делать-PST-3PL</ta>
            <ta e="T579" id="Seg_8212" s="T578">тогда</ta>
            <ta e="T580" id="Seg_8213" s="T579">этот.[NOM.SG]</ta>
            <ta e="T581" id="Seg_8214" s="T580">чум-LOC</ta>
            <ta e="T582" id="Seg_8215" s="T581">мясо.[NOM.SG]</ta>
            <ta e="T583" id="Seg_8216" s="T582">кипятить-PST-3PL</ta>
            <ta e="T584" id="Seg_8217" s="T583">желтый.[NOM.SG]</ta>
            <ta e="T585" id="Seg_8218" s="T584">вода.[NOM.SG]</ta>
            <ta e="T586" id="Seg_8219" s="T585">кипятить-PST-3PL</ta>
            <ta e="T587" id="Seg_8220" s="T586">есть-PST-3PL</ta>
            <ta e="T589" id="Seg_8221" s="T588">дом-PL</ta>
            <ta e="T590" id="Seg_8222" s="T589">быть-PST-3PL</ta>
            <ta e="T591" id="Seg_8223" s="T590">этот.[NOM.SG]</ta>
            <ta e="T592" id="Seg_8224" s="T591">дом-LOC</ta>
            <ta e="T593" id="Seg_8225" s="T592">NEG</ta>
            <ta e="T594" id="Seg_8226" s="T593">жить-PST-3PL</ta>
            <ta e="T595" id="Seg_8227" s="T594">снаружи</ta>
            <ta e="T596" id="Seg_8228" s="T595">чум.[NOM.SG]</ta>
            <ta e="T597" id="Seg_8229" s="T596">быть-PST.[3SG]</ta>
            <ta e="T598" id="Seg_8230" s="T597">там</ta>
            <ta e="T599" id="Seg_8231" s="T598">кипятить-PST-3PL</ta>
            <ta e="T600" id="Seg_8232" s="T599">суп.[NOM.SG]</ta>
            <ta e="T601" id="Seg_8233" s="T600">желтый.[NOM.SG]</ta>
            <ta e="T602" id="Seg_8234" s="T601">вода.[NOM.SG]</ta>
            <ta e="T603" id="Seg_8235" s="T602">кипятить-PST-3PL</ta>
            <ta e="T604" id="Seg_8236" s="T603">есть-PST-3PL</ta>
            <ta e="T605" id="Seg_8237" s="T604">а</ta>
            <ta e="T606" id="Seg_8238" s="T605">дом-LOC</ta>
            <ta e="T607" id="Seg_8239" s="T606">спать-PST-3PL</ta>
            <ta e="T609" id="Seg_8240" s="T608">чум-PL</ta>
            <ta e="T610" id="Seg_8241" s="T609">делать-PST-3PL</ta>
            <ta e="T612" id="Seg_8242" s="T611">дерево-PL</ta>
            <ta e="T613" id="Seg_8243" s="T612">тогда</ta>
            <ta e="T614" id="Seg_8244" s="T613">кожа.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8245" s="T614">плести-PST-3PL</ta>
            <ta e="T616" id="Seg_8246" s="T615">дерево-3SG-INS</ta>
            <ta e="T617" id="Seg_8247" s="T616">и</ta>
            <ta e="T618" id="Seg_8248" s="T617">класть-PST-3PL</ta>
            <ta e="T619" id="Seg_8249" s="T618">этот-LAT</ta>
            <ta e="T620" id="Seg_8250" s="T619">тогда</ta>
            <ta e="T621" id="Seg_8251" s="T620">канат-PL</ta>
            <ta e="T622" id="Seg_8252" s="T621">делать-PST-3PL</ta>
            <ta e="T623" id="Seg_8253" s="T622">тогда</ta>
            <ta e="T624" id="Seg_8254" s="T623">котел-PL</ta>
            <ta e="T625" id="Seg_8255" s="T624">вешать-PST-3PL</ta>
            <ta e="T628" id="Seg_8256" s="T627">кипятить-INF.LAT</ta>
            <ta e="T629" id="Seg_8257" s="T628">мясо.[NOM.SG]</ta>
            <ta e="T630" id="Seg_8258" s="T629">и</ta>
            <ta e="T631" id="Seg_8259" s="T630">желтый.[NOM.SG]</ta>
            <ta e="T632" id="Seg_8260" s="T631">вода.[NOM.SG]</ta>
            <ta e="T633" id="Seg_8261" s="T632">кипятить-INF.LAT</ta>
            <ta e="T635" id="Seg_8262" s="T633">тогда</ta>
            <ta e="T638" id="Seg_8263" s="T637">земля-LOC</ta>
            <ta e="T639" id="Seg_8264" s="T638">огонь.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8265" s="T639">съесть-PST-3PL</ta>
            <ta e="T641" id="Seg_8266" s="T640">наверху</ta>
            <ta e="T642" id="Seg_8267" s="T641">отверстие.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8268" s="T642">быть-PST.[3SG]</ta>
            <ta e="T644" id="Seg_8269" s="T643">там</ta>
            <ta e="T646" id="Seg_8270" s="T645">дым.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8271" s="T646">идти-DUR.[3SG]</ta>
            <ta e="T648" id="Seg_8272" s="T647">тогда</ta>
            <ta e="T649" id="Seg_8273" s="T648">дерево-PL</ta>
            <ta e="T650" id="Seg_8274" s="T649">съесть-PST-3PL</ta>
            <ta e="T651" id="Seg_8275" s="T650">чтобы</ta>
            <ta e="T652" id="Seg_8276" s="T651">огонь-LAT</ta>
            <ta e="T653" id="Seg_8277" s="T652">кто.[NOM.SG]=INDEF</ta>
            <ta e="T654" id="Seg_8278" s="T653">NEG</ta>
            <ta e="T656" id="Seg_8279" s="T655">упасть-PST.[3SG]</ta>
            <ta e="T658" id="Seg_8280" s="T657">холодный.[NOM.SG]</ta>
            <ta e="T659" id="Seg_8281" s="T658">стать-PST.[3SG]</ta>
            <ta e="T660" id="Seg_8282" s="T659">так</ta>
            <ta e="T662" id="Seg_8283" s="T661">шкура-INS</ta>
            <ta e="T663" id="Seg_8284" s="T662">закрыть-PST-3PL</ta>
            <ta e="T664" id="Seg_8285" s="T663">а</ta>
            <ta e="T665" id="Seg_8286" s="T664">теплый.[NOM.SG]</ta>
            <ta e="T666" id="Seg_8287" s="T665">стать-PST.[3SG]</ta>
            <ta e="T667" id="Seg_8288" s="T666">так</ta>
            <ta e="T669" id="Seg_8289" s="T668">плести-PST-3PL</ta>
            <ta e="T671" id="Seg_8290" s="T670">закрыть-PST-3PL</ta>
            <ta e="T673" id="Seg_8291" s="T672">большой.[NOM.SG]</ta>
            <ta e="T674" id="Seg_8292" s="T673">быть-PST.[3SG]</ta>
            <ta e="T677" id="Seg_8293" s="T676">четыре.[NOM.SG]</ta>
            <ta e="T678" id="Seg_8294" s="T677">люди.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8295" s="T678">лежать-PST-3PL</ta>
            <ta e="T680" id="Seg_8296" s="T679">десять.[NOM.SG]</ta>
            <ta e="T681" id="Seg_8297" s="T680">люди.[NOM.SG]</ta>
            <ta e="T682" id="Seg_8298" s="T681">когда</ta>
            <ta e="T683" id="Seg_8299" s="T682">снег.[NOM.SG]</ta>
            <ta e="T684" id="Seg_8300" s="T683">быть-PST.[3SG]</ta>
            <ta e="T685" id="Seg_8301" s="T684">этот-PL</ta>
            <ta e="T686" id="Seg_8302" s="T685">здесь</ta>
            <ta e="T687" id="Seg_8303" s="T686">жить-PST-3PL</ta>
            <ta e="T688" id="Seg_8304" s="T687">а</ta>
            <ta e="T689" id="Seg_8305" s="T688">когда</ta>
            <ta e="T690" id="Seg_8306" s="T689">снег.[NOM.SG]</ta>
            <ta e="T849" id="Seg_8307" s="T690">пойти-CVB</ta>
            <ta e="T691" id="Seg_8308" s="T849">исчезнуть-PST.[3SG]</ta>
            <ta e="T692" id="Seg_8309" s="T691">тогда</ta>
            <ta e="T693" id="Seg_8310" s="T692">этот-PL</ta>
            <ta e="T694" id="Seg_8311" s="T693">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T695" id="Seg_8312" s="T694">лес-LAT</ta>
            <ta e="T696" id="Seg_8313" s="T695">большой.[NOM.SG]</ta>
            <ta e="T697" id="Seg_8314" s="T696">гора-LAT</ta>
            <ta e="T699" id="Seg_8315" s="T698">олень-PL-INS</ta>
            <ta e="T700" id="Seg_8316" s="T699">идти-PST-3PL</ta>
            <ta e="T701" id="Seg_8317" s="T700">Саяны-LAT</ta>
            <ta e="T702" id="Seg_8318" s="T701">пойти-PST-3PL</ta>
            <ta e="T703" id="Seg_8319" s="T702">там</ta>
            <ta e="T704" id="Seg_8320" s="T703">большой.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8321" s="T704">река.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8322" s="T705">рыба.[NOM.SG]</ta>
            <ta e="T707" id="Seg_8323" s="T706">ловить-PST-3PL</ta>
            <ta e="T709" id="Seg_8324" s="T708">тогда</ta>
            <ta e="T710" id="Seg_8325" s="T709">этот-PL</ta>
            <ta e="T711" id="Seg_8326" s="T710">пойти-PST-3PL</ta>
            <ta e="T712" id="Seg_8327" s="T711">ребенок-PL-LAT</ta>
            <ta e="T713" id="Seg_8328" s="T712">и</ta>
            <ta e="T714" id="Seg_8329" s="T713">женщина-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T715" id="Seg_8330" s="T714">взять-PST-3PL</ta>
            <ta e="T716" id="Seg_8331" s="T715">олень-PL-INS</ta>
            <ta e="T717" id="Seg_8332" s="T716">пойти-PST-3PL</ta>
            <ta e="T719" id="Seg_8333" s="T718">и</ta>
            <ta e="T720" id="Seg_8334" s="T719">собака-PL-COM</ta>
            <ta e="T722" id="Seg_8335" s="T721">тогда</ta>
            <ta e="T723" id="Seg_8336" s="T722">этот-PL</ta>
            <ta e="T724" id="Seg_8337" s="T723">лес-LOC</ta>
            <ta e="T725" id="Seg_8338" s="T724">ягода.[NOM.SG]</ta>
            <ta e="T726" id="Seg_8339" s="T725">собирать-PST-3PL</ta>
            <ta e="T727" id="Seg_8340" s="T726">съесть-PST-3PL</ta>
            <ta e="T728" id="Seg_8341" s="T727">кедровый.орех.[NOM.SG]</ta>
            <ta e="T729" id="Seg_8342" s="T728">собирать-PST-3PL</ta>
            <ta e="T730" id="Seg_8343" s="T729">лущить-PST-3PL</ta>
            <ta e="T732" id="Seg_8344" s="T731">тогда</ta>
            <ta e="T733" id="Seg_8345" s="T732">этот-PL</ta>
            <ta e="T734" id="Seg_8346" s="T733">гриб-NOM/GEN/ACC.3PL</ta>
            <ta e="T735" id="Seg_8347" s="T734">собирать-PST-3PL</ta>
            <ta e="T736" id="Seg_8348" s="T735">кипятить-PST-3PL</ta>
            <ta e="T737" id="Seg_8349" s="T736">и</ta>
            <ta e="T738" id="Seg_8350" s="T737">солить-PST-3PL</ta>
            <ta e="T740" id="Seg_8351" s="T739">тогда</ta>
            <ta e="T741" id="Seg_8352" s="T740">там</ta>
            <ta e="T742" id="Seg_8353" s="T741">этот-GEN.PL</ta>
            <ta e="T743" id="Seg_8354" s="T742">там</ta>
            <ta e="T744" id="Seg_8355" s="T743">убить-PST-3PL</ta>
            <ta e="T745" id="Seg_8356" s="T744">%%-NOM/GEN/ACC.3PL</ta>
            <ta e="T746" id="Seg_8357" s="T745">и</ta>
            <ta e="T747" id="Seg_8358" s="T746">мясо.[NOM.SG]</ta>
            <ta e="T748" id="Seg_8359" s="T747">солить-PST-3PL</ta>
            <ta e="T749" id="Seg_8360" s="T748">тогда</ta>
            <ta e="T750" id="Seg_8361" s="T749">вешать-PST-3PL</ta>
            <ta e="T751" id="Seg_8362" s="T750">этот.[NOM.SG]</ta>
            <ta e="T753" id="Seg_8363" s="T752">высохнуть-RES-PST.[3SG]</ta>
            <ta e="T754" id="Seg_8364" s="T753">топор-INS</ta>
            <ta e="T755" id="Seg_8365" s="T754">резать-PST-3PL</ta>
            <ta e="T756" id="Seg_8366" s="T755">мешок-LAT</ta>
            <ta e="T757" id="Seg_8367" s="T756">лить-PST-3PL</ta>
            <ta e="T759" id="Seg_8368" s="T758">этот-PL</ta>
            <ta e="T760" id="Seg_8369" s="T759">тогда</ta>
            <ta e="T761" id="Seg_8370" s="T760">рыба.[NOM.SG]</ta>
            <ta e="T762" id="Seg_8371" s="T761">ловить-PST-3PL</ta>
            <ta e="T763" id="Seg_8372" s="T762">и</ta>
            <ta e="T764" id="Seg_8373" s="T763">солить-PST-3PL</ta>
            <ta e="T765" id="Seg_8374" s="T764">и</ta>
            <ta e="T766" id="Seg_8375" s="T765">тоже</ta>
            <ta e="T767" id="Seg_8376" s="T766">рыба.[NOM.SG]</ta>
            <ta e="T769" id="Seg_8377" s="T767">сушить-PST-3PL</ta>
            <ta e="T772" id="Seg_8378" s="T771">тогда</ta>
            <ta e="T773" id="Seg_8379" s="T772">кожа-3SG-INS</ta>
            <ta e="T774" id="Seg_8380" s="T773">делать-PST-3PL</ta>
            <ta e="T775" id="Seg_8381" s="T774">мешок-PL</ta>
            <ta e="T776" id="Seg_8382" s="T775">и</ta>
            <ta e="T777" id="Seg_8383" s="T776">там</ta>
            <ta e="T778" id="Seg_8384" s="T777">класть-PST-3PL</ta>
            <ta e="T779" id="Seg_8385" s="T778">мясо.[NOM.SG]</ta>
            <ta e="T780" id="Seg_8386" s="T779">и</ta>
            <ta e="T781" id="Seg_8387" s="T780">рыба.[NOM.SG]</ta>
            <ta e="T782" id="Seg_8388" s="T781">и</ta>
            <ta e="T783" id="Seg_8389" s="T782">олень-PL-INS</ta>
            <ta e="T784" id="Seg_8390" s="T783">принести-PST-3PL</ta>
            <ta e="T785" id="Seg_8391" s="T784">дома-LAT</ta>
            <ta e="T786" id="Seg_8392" s="T785">здесь</ta>
            <ta e="T787" id="Seg_8393" s="T786">где</ta>
            <ta e="T788" id="Seg_8394" s="T787">жить-PST-3PL</ta>
            <ta e="T789" id="Seg_8395" s="T788">когда</ta>
            <ta e="T790" id="Seg_8396" s="T789">холодный.[NOM.SG]</ta>
            <ta e="T791" id="Seg_8397" s="T790">быть-PST.[3SG]</ta>
            <ta e="T793" id="Seg_8398" s="T792">этот-PL</ta>
            <ta e="T795" id="Seg_8399" s="T794">такой-PL</ta>
            <ta e="T796" id="Seg_8400" s="T795">собака-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T798" id="Seg_8401" s="T797">быть-PST-3PL</ta>
            <ta e="T800" id="Seg_8402" s="T799">пойти-FUT-3PL</ta>
            <ta e="T801" id="Seg_8403" s="T800">коза.[NOM.SG]</ta>
            <ta e="T802" id="Seg_8404" s="T801">много</ta>
            <ta e="T803" id="Seg_8405" s="T802">ловить-FUT-3PL</ta>
            <ta e="T804" id="Seg_8406" s="T803">тогда</ta>
            <ta e="T805" id="Seg_8407" s="T804">дома-LAT/LOC.3SG</ta>
            <ta e="T806" id="Seg_8408" s="T805">прийти-FUT-3SG</ta>
            <ta e="T807" id="Seg_8409" s="T806">этот.[NOM.SG]</ta>
            <ta e="T808" id="Seg_8410" s="T807">лошадь.[NOM.SG]</ta>
            <ta e="T809" id="Seg_8411" s="T808">%%-FUT-3SG</ta>
            <ta e="T810" id="Seg_8412" s="T809">тогда</ta>
            <ta e="T811" id="Seg_8413" s="T810">идти-PRS.[3SG]</ta>
            <ta e="T812" id="Seg_8414" s="T811">коза.[NOM.SG]</ta>
            <ta e="T813" id="Seg_8415" s="T812">собирать-INF.LAT</ta>
            <ta e="T814" id="Seg_8416" s="T813">тогда</ta>
            <ta e="T815" id="Seg_8417" s="T814">прийти-FUT-3SG</ta>
            <ta e="T816" id="Seg_8418" s="T815">дома-LAT/LOC.3SG</ta>
            <ta e="T818" id="Seg_8419" s="T817">и</ta>
            <ta e="T819" id="Seg_8420" s="T818">коза.[NOM.SG]</ta>
            <ta e="T820" id="Seg_8421" s="T819">принести-FUT-3SG</ta>
            <ta e="T823" id="Seg_8422" s="T822">этот-ACC.PL</ta>
            <ta e="T825" id="Seg_8423" s="T824">армия-LAT</ta>
            <ta e="T826" id="Seg_8424" s="T825">NEG</ta>
            <ta e="T827" id="Seg_8425" s="T826">взять-PST-3PL</ta>
            <ta e="T828" id="Seg_8426" s="T827">этот-PL</ta>
            <ta e="T829" id="Seg_8427" s="T828">ясак.[NOM.SG]</ta>
            <ta e="T830" id="Seg_8428" s="T829">деньги.[NOM.SG]</ta>
            <ta e="T831" id="Seg_8429" s="T830">деньги.[NOM.SG]</ta>
            <ta e="T832" id="Seg_8430" s="T831">дать-PST-3PL</ta>
            <ta e="T833" id="Seg_8431" s="T832">и</ta>
            <ta e="T834" id="Seg_8432" s="T833">соболь.[NOM.SG]</ta>
            <ta e="T835" id="Seg_8433" s="T834">дать-PST-3PL</ta>
            <ta e="T837" id="Seg_8434" s="T836">этот-PL</ta>
            <ta e="T838" id="Seg_8435" s="T837">ясак-LAT</ta>
            <ta e="T839" id="Seg_8436" s="T838">идти-PST-3PL</ta>
            <ta e="T840" id="Seg_8437" s="T839">деньги.[NOM.SG]</ta>
            <ta e="T841" id="Seg_8438" s="T840">десять.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8439" s="T841">десять.[NOM.SG]</ta>
            <ta e="T843" id="Seg_8440" s="T842">пять.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8441" s="T0">que</ta>
            <ta e="T2" id="Seg_8442" s="T1">pers</ta>
            <ta e="T3" id="Seg_8443" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_8444" s="T3">adj-n:case</ta>
            <ta e="T5" id="Seg_8445" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_8446" s="T5">num-n:case</ta>
            <ta e="T7" id="Seg_8447" s="T6">n-n:case</ta>
            <ta e="T850" id="Seg_8448" s="T7">propr</ta>
            <ta e="T8" id="Seg_8449" s="T850">n-n:case</ta>
            <ta e="T9" id="Seg_8450" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_8451" s="T9">que-n:case</ta>
            <ta e="T11" id="Seg_8452" s="T10">que-n:case</ta>
            <ta e="T12" id="Seg_8453" s="T11">que-n:case</ta>
            <ta e="T13" id="Seg_8454" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_8455" s="T13">adv</ta>
            <ta e="T15" id="Seg_8456" s="T14">adv</ta>
            <ta e="T16" id="Seg_8457" s="T15">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_8458" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_8459" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_8460" s="T19">que=ptcl</ta>
            <ta e="T21" id="Seg_8461" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_8462" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_8463" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_8464" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_8465" s="T24">v-v:n.fin</ta>
            <ta e="T26" id="Seg_8466" s="T25">adv</ta>
            <ta e="T27" id="Seg_8467" s="T26">num-n:case</ta>
            <ta e="T28" id="Seg_8468" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_8469" s="T28">adv</ta>
            <ta e="T30" id="Seg_8470" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_8471" s="T30">adv</ta>
            <ta e="T33" id="Seg_8472" s="T32">adv</ta>
            <ta e="T34" id="Seg_8473" s="T33">adv</ta>
            <ta e="T35" id="Seg_8474" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_8475" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_8476" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_8477" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_8478" s="T38">pers</ta>
            <ta e="T40" id="Seg_8479" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_8480" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_8481" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_8482" s="T42">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_8483" s="T44">adv</ta>
            <ta e="T46" id="Seg_8484" s="T45">adv</ta>
            <ta e="T47" id="Seg_8485" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_8486" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_8487" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_8488" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_8489" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_8490" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_8491" s="T52">dempro-n:case</ta>
            <ta e="T54" id="Seg_8492" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_8493" s="T54">dempro-n:case</ta>
            <ta e="T56" id="Seg_8494" s="T55">v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_8495" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8496" s="T57">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_8497" s="T59">dempro-n:case</ta>
            <ta e="T61" id="Seg_8498" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_8499" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_8500" s="T62">adj-n:case</ta>
            <ta e="T64" id="Seg_8501" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_8502" s="T64">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_8503" s="T66">adv</ta>
            <ta e="T68" id="Seg_8504" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_8505" s="T68">adj-n:case</ta>
            <ta e="T70" id="Seg_8506" s="T69">adv</ta>
            <ta e="T71" id="Seg_8507" s="T70">n</ta>
            <ta e="T72" id="Seg_8508" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_8509" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_8510" s="T73">ptcl</ta>
            <ta e="T76" id="Seg_8511" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_8512" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_8513" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_8514" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_8515" s="T79">adj-n:case</ta>
            <ta e="T81" id="Seg_8516" s="T80">n-n:case</ta>
            <ta e="T83" id="Seg_8517" s="T82">pers</ta>
            <ta e="T84" id="Seg_8518" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_8519" s="T84">adv</ta>
            <ta e="T86" id="Seg_8520" s="T85">quant</ta>
            <ta e="T87" id="Seg_8521" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_8522" s="T87">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_8523" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_8524" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_8525" s="T91">dempro-n:case</ta>
            <ta e="T94" id="Seg_8526" s="T93">adv</ta>
            <ta e="T95" id="Seg_8527" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_8528" s="T95">pers</ta>
            <ta e="T97" id="Seg_8529" s="T96">adv</ta>
            <ta e="T99" id="Seg_8530" s="T98">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_8531" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_8532" s="T100">v-v:mood.pn</ta>
            <ta e="T102" id="Seg_8533" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_8534" s="T102">v-v:mood.pn</ta>
            <ta e="T105" id="Seg_8535" s="T104">adv</ta>
            <ta e="T107" id="Seg_8536" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_8537" s="T107">v-v:n.fin</ta>
            <ta e="T109" id="Seg_8538" s="T108">pers</ta>
            <ta e="T110" id="Seg_8539" s="T109">adv</ta>
            <ta e="T111" id="Seg_8540" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_8541" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_8542" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_8543" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_8544" s="T114">num-n:case</ta>
            <ta e="T116" id="Seg_8545" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_8546" s="T116">adv</ta>
            <ta e="T118" id="Seg_8547" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_8548" s="T118">que-n:case</ta>
            <ta e="T120" id="Seg_8549" s="T119">adv</ta>
            <ta e="T121" id="Seg_8550" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_8551" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_8552" s="T122">conj</ta>
            <ta e="T124" id="Seg_8553" s="T123">pers</ta>
            <ta e="T125" id="Seg_8554" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_8555" s="T125">pers</ta>
            <ta e="T127" id="Seg_8556" s="T126">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_8557" s="T127">v-v:n.fin</ta>
            <ta e="T129" id="Seg_8558" s="T128">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_8559" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_8560" s="T131">v-v&gt;v-v:pn</ta>
            <ta e="T133" id="Seg_8561" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_8562" s="T133">v-v&gt;v-v:pn</ta>
            <ta e="T136" id="Seg_8563" s="T135">n-n:num</ta>
            <ta e="T137" id="Seg_8564" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_8565" s="T137">v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_8566" s="T138">n-n:case</ta>
            <ta e="T141" id="Seg_8567" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_8568" s="T141">n-n:case.poss</ta>
            <ta e="T144" id="Seg_8569" s="T143">num-n:case</ta>
            <ta e="T145" id="Seg_8570" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_8571" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_8572" s="T146">v-v:n.fin</ta>
            <ta e="T150" id="Seg_8573" s="T149">aux-v:mood.pn</ta>
            <ta e="T151" id="Seg_8574" s="T150">v-v:mood.pn</ta>
            <ta e="T152" id="Seg_8575" s="T151">aux-v:mood.pn</ta>
            <ta e="T153" id="Seg_8576" s="T152">v-v:mood.pn</ta>
            <ta e="T154" id="Seg_8577" s="T153">v-v:pn</ta>
            <ta e="T156" id="Seg_8578" s="T155">n-n:case.poss</ta>
            <ta e="T157" id="Seg_8579" s="T156">que</ta>
            <ta e="T158" id="Seg_8580" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_8581" s="T158">adv</ta>
            <ta e="T160" id="Seg_8582" s="T159">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_8583" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_8584" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_8585" s="T163">adj-adj&gt;v-v:n.fin</ta>
            <ta e="T165" id="Seg_8586" s="T164">adv</ta>
            <ta e="T166" id="Seg_8587" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_8588" s="T166">v-v&gt;v-v:n.fin</ta>
            <ta e="T169" id="Seg_8589" s="T168">que-n:case</ta>
            <ta e="T170" id="Seg_8590" s="T169">v-v:n.fin</ta>
            <ta e="T171" id="Seg_8591" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_8592" s="T171">v-v&gt;v-v:n.fin</ta>
            <ta e="T173" id="Seg_8593" s="T172">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_8594" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_8595" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_8596" s="T176">adj-n:case</ta>
            <ta e="T178" id="Seg_8597" s="T177">n-n:num</ta>
            <ta e="T179" id="Seg_8598" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8599" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_8600" s="T180">v-v&gt;v-v:pn</ta>
            <ta e="T182" id="Seg_8601" s="T181">adj-n:num</ta>
            <ta e="T183" id="Seg_8602" s="T182">adv</ta>
            <ta e="T185" id="Seg_8603" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_8604" s="T185">adv</ta>
            <ta e="T187" id="Seg_8605" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_8606" s="T187">pers</ta>
            <ta e="T189" id="Seg_8607" s="T188">v-v:n.fin</ta>
            <ta e="T191" id="Seg_8608" s="T190">n-n:case.poss</ta>
            <ta e="T192" id="Seg_8609" s="T191">v-v&gt;v-v:n.fin</ta>
            <ta e="T194" id="Seg_8610" s="T193">n-n:num-n:case.poss</ta>
            <ta e="T195" id="Seg_8611" s="T194">v-v:n.fin</ta>
            <ta e="T196" id="Seg_8612" s="T195">n-n:case.poss</ta>
            <ta e="T197" id="Seg_8613" s="T196">v-v:n.fin</ta>
            <ta e="T198" id="Seg_8614" s="T197">n-n:num</ta>
            <ta e="T199" id="Seg_8615" s="T198">v-v:n.fin</ta>
            <ta e="T200" id="Seg_8616" s="T199">n-n:case</ta>
            <ta e="T202" id="Seg_8617" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_8618" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_8619" s="T203">que</ta>
            <ta e="T205" id="Seg_8620" s="T204">adv</ta>
            <ta e="T206" id="Seg_8621" s="T205">v-v:n.fin</ta>
            <ta e="T208" id="Seg_8622" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_8623" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_8624" s="T209">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_8625" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_8626" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_8627" s="T212">adv</ta>
            <ta e="T214" id="Seg_8628" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_8629" s="T214">dempro</ta>
            <ta e="T216" id="Seg_8630" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_8631" s="T216">conj</ta>
            <ta e="T218" id="Seg_8632" s="T217">n-n:case</ta>
            <ta e="T222" id="Seg_8633" s="T221">adv</ta>
            <ta e="T223" id="Seg_8634" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_8635" s="T223">adv</ta>
            <ta e="T225" id="Seg_8636" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_8637" s="T225">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_8638" s="T227">propr</ta>
            <ta e="T228" id="Seg_8639" s="T851">n-n:case</ta>
            <ta e="T229" id="Seg_8640" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_8641" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_8642" s="T230">n-n:case</ta>
            <ta e="T233" id="Seg_8643" s="T232">n-n:case</ta>
            <ta e="T235" id="Seg_8644" s="T234">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_8645" s="T236">quant</ta>
            <ta e="T238" id="Seg_8646" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_8647" s="T238">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_8648" s="T240">n-n:num</ta>
            <ta e="T242" id="Seg_8649" s="T241">adv</ta>
            <ta e="T243" id="Seg_8650" s="T242">quant</ta>
            <ta e="T244" id="Seg_8651" s="T243">v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_8652" s="T244">ptcl</ta>
            <ta e="T247" id="Seg_8653" s="T246">pers</ta>
            <ta e="T248" id="Seg_8654" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_8655" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_8656" s="T249">conj</ta>
            <ta e="T251" id="Seg_8657" s="T250">n-n:case.poss</ta>
            <ta e="T253" id="Seg_8658" s="T252">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_8659" s="T254">v-v:n.fin</ta>
            <ta e="T258" id="Seg_8660" s="T257">pers</ta>
            <ta e="T259" id="Seg_8661" s="T258">adv</ta>
            <ta e="T260" id="Seg_8662" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_8663" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_8664" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_8665" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_8666" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_8667" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_8668" s="T265">ptcl</ta>
            <ta e="T269" id="Seg_8669" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_8670" s="T269">v-v&gt;v-v:pn</ta>
            <ta e="T271" id="Seg_8671" s="T270">n-n:num-n:case</ta>
            <ta e="T272" id="Seg_8672" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_8673" s="T272">v-v&gt;v-v:pn</ta>
            <ta e="T274" id="Seg_8674" s="T273">pers</ta>
            <ta e="T275" id="Seg_8675" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_8676" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_8677" s="T276">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_8678" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_8679" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_8680" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_8681" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_8682" s="T282">adv</ta>
            <ta e="T285" id="Seg_8683" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_8684" s="T285">v-v:n.fin</ta>
            <ta e="T288" id="Seg_8685" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_8686" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_8687" s="T289">n-n:case.poss</ta>
            <ta e="T291" id="Seg_8688" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_8689" s="T291">que=ptcl</ta>
            <ta e="T293" id="Seg_8690" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_8691" s="T293">v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_8692" s="T294">adv</ta>
            <ta e="T296" id="Seg_8693" s="T295">%%</ta>
            <ta e="T297" id="Seg_8694" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_8695" s="T297">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_8696" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_8697" s="T299">conj</ta>
            <ta e="T301" id="Seg_8698" s="T300">pers</ta>
            <ta e="T302" id="Seg_8699" s="T301">n-n:case.poss</ta>
            <ta e="T303" id="Seg_8700" s="T302">v-v:tense-v:pn</ta>
            <ta e="T852" id="Seg_8701" s="T303">propr</ta>
            <ta e="T304" id="Seg_8702" s="T852">n-n:case</ta>
            <ta e="T305" id="Seg_8703" s="T304">conj</ta>
            <ta e="T306" id="Seg_8704" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_8705" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_8706" s="T307">adv</ta>
            <ta e="T309" id="Seg_8707" s="T308">v-v:tense-v:pn</ta>
            <ta e="T310" id="Seg_8708" s="T309">v-v:n.fin</ta>
            <ta e="T312" id="Seg_8709" s="T311">dempro-n:case</ta>
            <ta e="T313" id="Seg_8710" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_8711" s="T313">adv</ta>
            <ta e="T315" id="Seg_8712" s="T314">dempro-n:case</ta>
            <ta e="T316" id="Seg_8713" s="T315">v-v:tense-v:pn</ta>
            <ta e="T317" id="Seg_8714" s="T316">dempro-n:case</ta>
            <ta e="T318" id="Seg_8715" s="T317">dempro-n:case</ta>
            <ta e="T319" id="Seg_8716" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_8717" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_8718" s="T320">dempro-n:case</ta>
            <ta e="T322" id="Seg_8719" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_8720" s="T322">conj</ta>
            <ta e="T324" id="Seg_8721" s="T323">v-v&gt;v-v:pn</ta>
            <ta e="T325" id="Seg_8722" s="T324">v-v&gt;v-v:pn</ta>
            <ta e="T326" id="Seg_8723" s="T325">dempro-n:case</ta>
            <ta e="T327" id="Seg_8724" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_8725" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_8726" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_8727" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_8728" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_8729" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_8730" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_8731" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_8732" s="T334">que-n:case</ta>
            <ta e="T336" id="Seg_8733" s="T335">pers</ta>
            <ta e="T337" id="Seg_8734" s="T336">ptcl</ta>
            <ta e="T340" id="Seg_8735" s="T339">conj</ta>
            <ta e="T341" id="Seg_8736" s="T340">pers</ta>
            <ta e="T342" id="Seg_8737" s="T341">pers</ta>
            <ta e="T344" id="Seg_8738" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_8739" s="T344">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_8740" s="T345">conj</ta>
            <ta e="T347" id="Seg_8741" s="T346">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_8742" s="T349">pers</ta>
            <ta e="T351" id="Seg_8743" s="T350">n-n:case.poss</ta>
            <ta e="T352" id="Seg_8744" s="T351">n-n:case.poss</ta>
            <ta e="T353" id="Seg_8745" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_8746" s="T353">adv</ta>
            <ta e="T355" id="Seg_8747" s="T354">ptcl</ta>
            <ta e="T357" id="Seg_8748" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_8749" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_8750" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_8751" s="T359">adj-n:case</ta>
            <ta e="T361" id="Seg_8752" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_8753" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_8754" s="T362">dempro-n:case</ta>
            <ta e="T364" id="Seg_8755" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_8756" s="T364">adj-n:case</ta>
            <ta e="T366" id="Seg_8757" s="T365">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T367" id="Seg_8758" s="T366">v-v:mood.pn</ta>
            <ta e="T369" id="Seg_8759" s="T368">v-v:n.fin</ta>
            <ta e="T370" id="Seg_8760" s="T369">dempro-n:case</ta>
            <ta e="T371" id="Seg_8761" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_8762" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_8763" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_8764" s="T373">dempro-n:case</ta>
            <ta e="T375" id="Seg_8765" s="T374">adv</ta>
            <ta e="T376" id="Seg_8766" s="T375">adj-n:case</ta>
            <ta e="T377" id="Seg_8767" s="T376">adv</ta>
            <ta e="T378" id="Seg_8768" s="T377">n-n:case.poss</ta>
            <ta e="T380" id="Seg_8769" s="T379">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_8770" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_8771" s="T382">ptcl</ta>
            <ta e="T385" id="Seg_8772" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_8773" s="T385">quant</ta>
            <ta e="T387" id="Seg_8774" s="T386">n-n:case.poss</ta>
            <ta e="T388" id="Seg_8775" s="T387">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_8776" s="T388">conj</ta>
            <ta e="T390" id="Seg_8777" s="T389">pers</ta>
            <ta e="T391" id="Seg_8778" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_8779" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_8780" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_8781" s="T393">adv</ta>
            <ta e="T395" id="Seg_8782" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_8783" s="T395">aux-v:mood.pn</ta>
            <ta e="T397" id="Seg_8784" s="T396">v-v:mood.pn</ta>
            <ta e="T398" id="Seg_8785" s="T397">dempro-n:case</ta>
            <ta e="T399" id="Seg_8786" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_8787" s="T399">n</ta>
            <ta e="T401" id="Seg_8788" s="T400">n-n:case</ta>
            <ta e="T404" id="Seg_8789" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_8790" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_8791" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_8792" s="T406">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_8793" s="T410">ptcl</ta>
            <ta e="T413" id="Seg_8794" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_8795" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_8796" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_8797" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_8798" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_8799" s="T417">n-n:ins-n:case</ta>
            <ta e="T420" id="Seg_8800" s="T419">n-n:case</ta>
            <ta e="T422" id="Seg_8801" s="T420">n-n:case</ta>
            <ta e="T423" id="Seg_8802" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_8803" s="T423">n-n:case</ta>
            <ta e="T426" id="Seg_8804" s="T425">dempro-n:case</ta>
            <ta e="T427" id="Seg_8805" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_8806" s="T427">ptcl</ta>
            <ta e="T432" id="Seg_8807" s="T431">adv</ta>
            <ta e="T433" id="Seg_8808" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_8809" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_8810" s="T434">conj</ta>
            <ta e="T436" id="Seg_8811" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_8812" s="T436">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_8813" s="T438">n-n:case.poss</ta>
            <ta e="T440" id="Seg_8814" s="T439">conj</ta>
            <ta e="T441" id="Seg_8815" s="T440">adv</ta>
            <ta e="T442" id="Seg_8816" s="T441">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_8817" s="T442">conj</ta>
            <ta e="T444" id="Seg_8818" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_8819" s="T444">quant</ta>
            <ta e="T446" id="Seg_8820" s="T445">v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_8821" s="T456">n-n:num</ta>
            <ta e="T460" id="Seg_8822" s="T459">que</ta>
            <ta e="T461" id="Seg_8823" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_8824" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_8825" s="T462">adv</ta>
            <ta e="T464" id="Seg_8826" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_8827" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_8828" s="T465">dempro-n:case</ta>
            <ta e="T467" id="Seg_8829" s="T466">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_8830" s="T467">pers</ta>
            <ta e="T470" id="Seg_8831" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_8832" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_8833" s="T471">conj</ta>
            <ta e="T473" id="Seg_8834" s="T472">que-n:case</ta>
            <ta e="T474" id="Seg_8835" s="T473">v-v:tense-v:pn</ta>
            <ta e="T475" id="Seg_8836" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_8837" s="T475">v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_8838" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_8839" s="T479">conj</ta>
            <ta e="T481" id="Seg_8840" s="T480">pers</ta>
            <ta e="T482" id="Seg_8841" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_8842" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_8843" s="T483">v-v:tense-v:pn</ta>
            <ta e="T487" id="Seg_8844" s="T486">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_8845" s="T487">n-n:case.poss</ta>
            <ta e="T489" id="Seg_8846" s="T488">conj</ta>
            <ta e="T490" id="Seg_8847" s="T489">pers</ta>
            <ta e="T491" id="Seg_8848" s="T490">n-n:case</ta>
            <ta e="T493" id="Seg_8849" s="T492">dempro-n:case</ta>
            <ta e="T494" id="Seg_8850" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8851" s="T494">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_8852" s="T495">%%</ta>
            <ta e="T497" id="Seg_8853" s="T496">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_8854" s="T497">conj</ta>
            <ta e="T499" id="Seg_8855" s="T498">dempro-n:case</ta>
            <ta e="T500" id="Seg_8856" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_8857" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_8858" s="T501">dempro-n:case</ta>
            <ta e="T503" id="Seg_8859" s="T502">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_8860" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_8861" s="T504">n-n:case.poss</ta>
            <ta e="T506" id="Seg_8862" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_8863" s="T506">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_8864" s="T510">que</ta>
            <ta e="T512" id="Seg_8865" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_8866" s="T512">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_8867" s="T513">adv</ta>
            <ta e="T515" id="Seg_8868" s="T514">v-v:tense-v:pn</ta>
            <ta e="T516" id="Seg_8869" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_8870" s="T516">dempro-n:case</ta>
            <ta e="T518" id="Seg_8871" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_8872" s="T518">dempro-n:case</ta>
            <ta e="T520" id="Seg_8873" s="T519">v-v:tense-v:pn</ta>
            <ta e="T521" id="Seg_8874" s="T520">adj</ta>
            <ta e="T522" id="Seg_8875" s="T521">n-n:case.poss</ta>
            <ta e="T523" id="Seg_8876" s="T522">v-v:tense-v:pn</ta>
            <ta e="T524" id="Seg_8877" s="T523">dempro-n:case</ta>
            <ta e="T525" id="Seg_8878" s="T524">adv</ta>
            <ta e="T526" id="Seg_8879" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_8880" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_8881" s="T527">n-n:case.poss-n:case</ta>
            <ta e="T532" id="Seg_8882" s="T531">adv</ta>
            <ta e="T533" id="Seg_8883" s="T532">dempro-n:case</ta>
            <ta e="T534" id="Seg_8884" s="T533">dempro-n:case</ta>
            <ta e="T535" id="Seg_8885" s="T534">%%-v&gt;v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_8886" s="T535">conj</ta>
            <ta e="T537" id="Seg_8887" s="T536">refl-n:case.poss</ta>
            <ta e="T538" id="Seg_8888" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_8889" s="T538">v-v&gt;v-v:pn</ta>
            <ta e="T540" id="Seg_8890" s="T539">n-n:case</ta>
            <ta e="T541" id="Seg_8891" s="T540">v-v&gt;v-v:pn</ta>
            <ta e="T543" id="Seg_8892" s="T542">adv</ta>
            <ta e="T544" id="Seg_8893" s="T543">dempro-n:case</ta>
            <ta e="T545" id="Seg_8894" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_8895" s="T545">v-v:tense-v:pn</ta>
            <ta e="T547" id="Seg_8896" s="T546">n-n:case.poss</ta>
            <ta e="T548" id="Seg_8897" s="T547">v-v:tense-v:pn</ta>
            <ta e="T551" id="Seg_8898" s="T550">n-n:num</ta>
            <ta e="T552" id="Seg_8899" s="T551">n-n:case</ta>
            <ta e="T848" id="Seg_8900" s="T552">v-v:n-fin</ta>
            <ta e="T553" id="Seg_8901" s="T848">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_8902" s="T553">n-n:num</ta>
            <ta e="T555" id="Seg_8903" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_8904" s="T555">v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_8905" s="T556">n-n:num</ta>
            <ta e="T558" id="Seg_8906" s="T557">v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_8907" s="T558">n-n:case.poss</ta>
            <ta e="T560" id="Seg_8908" s="T559">v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_8909" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_8910" s="T561">v-v:pn</ta>
            <ta e="T564" id="Seg_8911" s="T563">n-n:num</ta>
            <ta e="T565" id="Seg_8912" s="T564">ptcl</ta>
            <ta e="T567" id="Seg_8913" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_8914" s="T567">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_8915" s="T568">n-n:num</ta>
            <ta e="T570" id="Seg_8916" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_8917" s="T570">n-n:num</ta>
            <ta e="T572" id="Seg_8918" s="T571">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_8919" s="T572">n-n:num</ta>
            <ta e="T574" id="Seg_8920" s="T573">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_8921" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_8922" s="T575">n-n:case</ta>
            <ta e="T578" id="Seg_8923" s="T577">v-v:tense-v:pn</ta>
            <ta e="T579" id="Seg_8924" s="T578">adv</ta>
            <ta e="T580" id="Seg_8925" s="T579">dempro-n:case</ta>
            <ta e="T581" id="Seg_8926" s="T580">n-n:case</ta>
            <ta e="T582" id="Seg_8927" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_8928" s="T582">v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_8929" s="T583">adj-n:case</ta>
            <ta e="T585" id="Seg_8930" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_8931" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_8932" s="T586">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_8933" s="T588">n-n:num</ta>
            <ta e="T590" id="Seg_8934" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_8935" s="T590">dempro-n:case</ta>
            <ta e="T592" id="Seg_8936" s="T591">n-n:case</ta>
            <ta e="T593" id="Seg_8937" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_8938" s="T593">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_8939" s="T594">adv</ta>
            <ta e="T596" id="Seg_8940" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_8941" s="T596">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_8942" s="T597">adv</ta>
            <ta e="T599" id="Seg_8943" s="T598">v-v:tense-v:pn</ta>
            <ta e="T600" id="Seg_8944" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_8945" s="T600">adj-n:case</ta>
            <ta e="T602" id="Seg_8946" s="T601">n-n:case</ta>
            <ta e="T603" id="Seg_8947" s="T602">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_8948" s="T603">v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_8949" s="T604">conj</ta>
            <ta e="T606" id="Seg_8950" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_8951" s="T606">v-v:tense-v:pn</ta>
            <ta e="T609" id="Seg_8952" s="T608">n-n:num</ta>
            <ta e="T610" id="Seg_8953" s="T609">v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_8954" s="T611">n-n:num</ta>
            <ta e="T613" id="Seg_8955" s="T612">adv</ta>
            <ta e="T614" id="Seg_8956" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_8957" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_8958" s="T615">n-n:case.poss-n:case</ta>
            <ta e="T617" id="Seg_8959" s="T616">conj</ta>
            <ta e="T618" id="Seg_8960" s="T617">v-v:tense-v:pn</ta>
            <ta e="T619" id="Seg_8961" s="T618">dempro-n:case</ta>
            <ta e="T620" id="Seg_8962" s="T619">adv</ta>
            <ta e="T621" id="Seg_8963" s="T620">n-n:num</ta>
            <ta e="T622" id="Seg_8964" s="T621">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_8965" s="T622">adv</ta>
            <ta e="T624" id="Seg_8966" s="T623">n-n:num</ta>
            <ta e="T625" id="Seg_8967" s="T624">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_8968" s="T627">v-v:n.fin</ta>
            <ta e="T629" id="Seg_8969" s="T628">n-n:case</ta>
            <ta e="T630" id="Seg_8970" s="T629">conj</ta>
            <ta e="T631" id="Seg_8971" s="T630">adj-n:case</ta>
            <ta e="T632" id="Seg_8972" s="T631">n-n:case</ta>
            <ta e="T633" id="Seg_8973" s="T632">v-v:n.fin</ta>
            <ta e="T635" id="Seg_8974" s="T633">adv</ta>
            <ta e="T638" id="Seg_8975" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_8976" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_8977" s="T639">v-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_8978" s="T640">adv</ta>
            <ta e="T642" id="Seg_8979" s="T641">n-n:case</ta>
            <ta e="T643" id="Seg_8980" s="T642">v-v:tense-v:pn</ta>
            <ta e="T644" id="Seg_8981" s="T643">adv</ta>
            <ta e="T646" id="Seg_8982" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_8983" s="T646">v-v&gt;v-v:pn</ta>
            <ta e="T648" id="Seg_8984" s="T647">adv</ta>
            <ta e="T649" id="Seg_8985" s="T648">n-n:num</ta>
            <ta e="T650" id="Seg_8986" s="T649">v-v:tense-v:pn</ta>
            <ta e="T651" id="Seg_8987" s="T650">conj</ta>
            <ta e="T652" id="Seg_8988" s="T651">n-n:case</ta>
            <ta e="T653" id="Seg_8989" s="T652">que-n:case=ptcl</ta>
            <ta e="T654" id="Seg_8990" s="T653">ptcl</ta>
            <ta e="T656" id="Seg_8991" s="T655">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_8992" s="T657">adj-n:case</ta>
            <ta e="T659" id="Seg_8993" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_8994" s="T659">ptcl</ta>
            <ta e="T662" id="Seg_8995" s="T661">n-n:case</ta>
            <ta e="T663" id="Seg_8996" s="T662">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_8997" s="T663">conj</ta>
            <ta e="T665" id="Seg_8998" s="T664">adj-n:case</ta>
            <ta e="T666" id="Seg_8999" s="T665">v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_9000" s="T666">ptcl</ta>
            <ta e="T669" id="Seg_9001" s="T668">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_9002" s="T670">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_9003" s="T672">adj-n:case</ta>
            <ta e="T674" id="Seg_9004" s="T673">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_9005" s="T676">num-n:case</ta>
            <ta e="T678" id="Seg_9006" s="T677">n-n:case</ta>
            <ta e="T679" id="Seg_9007" s="T678">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_9008" s="T679">num-n:case</ta>
            <ta e="T681" id="Seg_9009" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_9010" s="T681">que</ta>
            <ta e="T683" id="Seg_9011" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_9012" s="T683">v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_9013" s="T684">dempro-n:num</ta>
            <ta e="T686" id="Seg_9014" s="T685">adv</ta>
            <ta e="T687" id="Seg_9015" s="T686">v-v:tense-v:pn</ta>
            <ta e="T688" id="Seg_9016" s="T687">conj</ta>
            <ta e="T689" id="Seg_9017" s="T688">que</ta>
            <ta e="T690" id="Seg_9018" s="T689">n-n:case</ta>
            <ta e="T849" id="Seg_9019" s="T690">v-v:n-fin</ta>
            <ta e="T691" id="Seg_9020" s="T849">v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_9021" s="T691">adv</ta>
            <ta e="T693" id="Seg_9022" s="T692">dempro-n:num</ta>
            <ta e="T694" id="Seg_9023" s="T693">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T695" id="Seg_9024" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_9025" s="T695">adj-n:case</ta>
            <ta e="T697" id="Seg_9026" s="T696">n-n:case</ta>
            <ta e="T699" id="Seg_9027" s="T698">n-n:case-n:case</ta>
            <ta e="T700" id="Seg_9028" s="T699">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_9029" s="T700">propr-n:case</ta>
            <ta e="T702" id="Seg_9030" s="T701">v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_9031" s="T702">adv</ta>
            <ta e="T704" id="Seg_9032" s="T703">adj-n:case</ta>
            <ta e="T705" id="Seg_9033" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_9034" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_9035" s="T706">v-v:tense-v:pn</ta>
            <ta e="T709" id="Seg_9036" s="T708">adv</ta>
            <ta e="T710" id="Seg_9037" s="T709">dempro-n:num</ta>
            <ta e="T711" id="Seg_9038" s="T710">v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_9039" s="T711">n-n:num-n:case</ta>
            <ta e="T713" id="Seg_9040" s="T712">conj</ta>
            <ta e="T714" id="Seg_9041" s="T713">n-n:num-n:case.poss</ta>
            <ta e="T715" id="Seg_9042" s="T714">v-v:tense-v:pn</ta>
            <ta e="T716" id="Seg_9043" s="T715">n-n:case-n:case</ta>
            <ta e="T717" id="Seg_9044" s="T716">v-v:tense-v:pn</ta>
            <ta e="T719" id="Seg_9045" s="T718">conj</ta>
            <ta e="T720" id="Seg_9046" s="T719">n-n:num-n:case</ta>
            <ta e="T722" id="Seg_9047" s="T721">adv</ta>
            <ta e="T723" id="Seg_9048" s="T722">dempro-n:num</ta>
            <ta e="T724" id="Seg_9049" s="T723">n-n:case</ta>
            <ta e="T725" id="Seg_9050" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_9051" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_9052" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_9053" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_9054" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_9055" s="T729">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_9056" s="T731">adv</ta>
            <ta e="T733" id="Seg_9057" s="T732">dempro-n:num</ta>
            <ta e="T734" id="Seg_9058" s="T733">n-n:case.poss</ta>
            <ta e="T735" id="Seg_9059" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_9060" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_9061" s="T736">conj</ta>
            <ta e="T738" id="Seg_9062" s="T737">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_9063" s="T739">adv</ta>
            <ta e="T741" id="Seg_9064" s="T740">adv</ta>
            <ta e="T742" id="Seg_9065" s="T741">dempro-n:case</ta>
            <ta e="T743" id="Seg_9066" s="T742">adv</ta>
            <ta e="T744" id="Seg_9067" s="T743">v-v:tense-v:pn</ta>
            <ta e="T745" id="Seg_9068" s="T744">n-n:case.poss</ta>
            <ta e="T746" id="Seg_9069" s="T745">conj</ta>
            <ta e="T747" id="Seg_9070" s="T746">n-n:case</ta>
            <ta e="T748" id="Seg_9071" s="T747">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_9072" s="T748">adv</ta>
            <ta e="T750" id="Seg_9073" s="T749">v-v:tense-v:pn</ta>
            <ta e="T751" id="Seg_9074" s="T750">dempro-n:case</ta>
            <ta e="T753" id="Seg_9075" s="T752">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T754" id="Seg_9076" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_9077" s="T754">v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_9078" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_9079" s="T756">v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_9080" s="T758">dempro-n:num</ta>
            <ta e="T760" id="Seg_9081" s="T759">adv</ta>
            <ta e="T761" id="Seg_9082" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_9083" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_9084" s="T762">conj</ta>
            <ta e="T764" id="Seg_9085" s="T763">v-v:tense-v:pn</ta>
            <ta e="T765" id="Seg_9086" s="T764">conj</ta>
            <ta e="T766" id="Seg_9087" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_9088" s="T766">n-n:case</ta>
            <ta e="T769" id="Seg_9089" s="T767">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_9090" s="T771">adv</ta>
            <ta e="T773" id="Seg_9091" s="T772">n-n:case.poss-n:case</ta>
            <ta e="T774" id="Seg_9092" s="T773">v-v:tense-v:pn</ta>
            <ta e="T775" id="Seg_9093" s="T774">n-n:num</ta>
            <ta e="T776" id="Seg_9094" s="T775">conj</ta>
            <ta e="T777" id="Seg_9095" s="T776">adv</ta>
            <ta e="T778" id="Seg_9096" s="T777">v-v:tense-v:pn</ta>
            <ta e="T779" id="Seg_9097" s="T778">n-n:case</ta>
            <ta e="T780" id="Seg_9098" s="T779">conj</ta>
            <ta e="T781" id="Seg_9099" s="T780">n-n:case</ta>
            <ta e="T782" id="Seg_9100" s="T781">conj</ta>
            <ta e="T783" id="Seg_9101" s="T782">n-n:case-n:case</ta>
            <ta e="T784" id="Seg_9102" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_9103" s="T784">n-n:case</ta>
            <ta e="T786" id="Seg_9104" s="T785">adv</ta>
            <ta e="T787" id="Seg_9105" s="T786">que</ta>
            <ta e="T788" id="Seg_9106" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_9107" s="T788">que</ta>
            <ta e="T790" id="Seg_9108" s="T789">adj-n:case</ta>
            <ta e="T791" id="Seg_9109" s="T790">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_9110" s="T792">dempro-n:num</ta>
            <ta e="T795" id="Seg_9111" s="T794">adj-n:num</ta>
            <ta e="T796" id="Seg_9112" s="T795">n-n:num-n:case.poss</ta>
            <ta e="T798" id="Seg_9113" s="T797">v-v:tense-v:pn</ta>
            <ta e="T800" id="Seg_9114" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_9115" s="T800">n-n:case</ta>
            <ta e="T802" id="Seg_9116" s="T801">quant</ta>
            <ta e="T803" id="Seg_9117" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_9118" s="T803">adv</ta>
            <ta e="T805" id="Seg_9119" s="T804">n-n:case.poss</ta>
            <ta e="T806" id="Seg_9120" s="T805">v-v:tense-v:pn</ta>
            <ta e="T807" id="Seg_9121" s="T806">dempro-n:case</ta>
            <ta e="T808" id="Seg_9122" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_9123" s="T808">v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_9124" s="T809">adv</ta>
            <ta e="T811" id="Seg_9125" s="T810">v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_9126" s="T811">n-n:case</ta>
            <ta e="T813" id="Seg_9127" s="T812">v-v:n.fin</ta>
            <ta e="T814" id="Seg_9128" s="T813">adv</ta>
            <ta e="T815" id="Seg_9129" s="T814">v-v:tense-v:pn</ta>
            <ta e="T816" id="Seg_9130" s="T815">n-n:case.poss</ta>
            <ta e="T818" id="Seg_9131" s="T817">conj</ta>
            <ta e="T819" id="Seg_9132" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_9133" s="T819">v-v:tense-v:pn</ta>
            <ta e="T823" id="Seg_9134" s="T822">dempro-n:case</ta>
            <ta e="T825" id="Seg_9135" s="T824">n-n:case</ta>
            <ta e="T826" id="Seg_9136" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_9137" s="T826">v-v:tense-v:pn</ta>
            <ta e="T828" id="Seg_9138" s="T827">dempro-n:num</ta>
            <ta e="T829" id="Seg_9139" s="T828">n-n:case</ta>
            <ta e="T830" id="Seg_9140" s="T829">n-n:case</ta>
            <ta e="T831" id="Seg_9141" s="T830">n-n:case</ta>
            <ta e="T832" id="Seg_9142" s="T831">v-v:tense-v:pn</ta>
            <ta e="T833" id="Seg_9143" s="T832">conj</ta>
            <ta e="T834" id="Seg_9144" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_9145" s="T834">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_9146" s="T836">dempro-n:num</ta>
            <ta e="T838" id="Seg_9147" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_9148" s="T838">v-v:tense-v:pn</ta>
            <ta e="T840" id="Seg_9149" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_9150" s="T840">num-n:case</ta>
            <ta e="T842" id="Seg_9151" s="T841">num-n:case</ta>
            <ta e="T843" id="Seg_9152" s="T842">num-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9153" s="T0">que</ta>
            <ta e="T2" id="Seg_9154" s="T1">pers</ta>
            <ta e="T3" id="Seg_9155" s="T2">v</ta>
            <ta e="T4" id="Seg_9156" s="T3">adj</ta>
            <ta e="T5" id="Seg_9157" s="T4">n</ta>
            <ta e="T6" id="Seg_9158" s="T5">num</ta>
            <ta e="T7" id="Seg_9159" s="T6">n</ta>
            <ta e="T850" id="Seg_9160" s="T7">propr</ta>
            <ta e="T8" id="Seg_9161" s="T850">n</ta>
            <ta e="T9" id="Seg_9162" s="T8">v</ta>
            <ta e="T10" id="Seg_9163" s="T9">que</ta>
            <ta e="T11" id="Seg_9164" s="T10">que</ta>
            <ta e="T12" id="Seg_9165" s="T11">que</ta>
            <ta e="T13" id="Seg_9166" s="T12">v</ta>
            <ta e="T14" id="Seg_9167" s="T13">adv</ta>
            <ta e="T15" id="Seg_9168" s="T14">adv</ta>
            <ta e="T16" id="Seg_9169" s="T15">v</ta>
            <ta e="T18" id="Seg_9170" s="T17">v</ta>
            <ta e="T19" id="Seg_9171" s="T18">v</ta>
            <ta e="T20" id="Seg_9172" s="T19">que</ta>
            <ta e="T21" id="Seg_9173" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_9174" s="T21">v</ta>
            <ta e="T23" id="Seg_9175" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_9176" s="T23">v</ta>
            <ta e="T25" id="Seg_9177" s="T24">v</ta>
            <ta e="T26" id="Seg_9178" s="T25">adv</ta>
            <ta e="T27" id="Seg_9179" s="T26">num</ta>
            <ta e="T28" id="Seg_9180" s="T27">n</ta>
            <ta e="T29" id="Seg_9181" s="T28">adv</ta>
            <ta e="T30" id="Seg_9182" s="T29">v</ta>
            <ta e="T31" id="Seg_9183" s="T30">adv</ta>
            <ta e="T33" id="Seg_9184" s="T32">adv</ta>
            <ta e="T34" id="Seg_9185" s="T33">adv</ta>
            <ta e="T35" id="Seg_9186" s="T34">v</ta>
            <ta e="T36" id="Seg_9187" s="T35">n</ta>
            <ta e="T37" id="Seg_9188" s="T36">v</ta>
            <ta e="T38" id="Seg_9189" s="T37">dempro</ta>
            <ta e="T39" id="Seg_9190" s="T38">pers</ta>
            <ta e="T40" id="Seg_9191" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_9192" s="T40">v</ta>
            <ta e="T42" id="Seg_9193" s="T41">n</ta>
            <ta e="T43" id="Seg_9194" s="T42">v</ta>
            <ta e="T45" id="Seg_9195" s="T44">adv</ta>
            <ta e="T46" id="Seg_9196" s="T45">adv</ta>
            <ta e="T47" id="Seg_9197" s="T46">v</ta>
            <ta e="T48" id="Seg_9198" s="T47">n</ta>
            <ta e="T49" id="Seg_9199" s="T48">v</ta>
            <ta e="T50" id="Seg_9200" s="T49">n</ta>
            <ta e="T51" id="Seg_9201" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_9202" s="T51">v</ta>
            <ta e="T53" id="Seg_9203" s="T52">dempro</ta>
            <ta e="T54" id="Seg_9204" s="T53">n</ta>
            <ta e="T55" id="Seg_9205" s="T54">dempro</ta>
            <ta e="T56" id="Seg_9206" s="T55">v</ta>
            <ta e="T57" id="Seg_9207" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9208" s="T57">v</ta>
            <ta e="T60" id="Seg_9209" s="T59">dempro</ta>
            <ta e="T61" id="Seg_9210" s="T60">n</ta>
            <ta e="T62" id="Seg_9211" s="T61">n</ta>
            <ta e="T63" id="Seg_9212" s="T62">adj</ta>
            <ta e="T64" id="Seg_9213" s="T63">n</ta>
            <ta e="T65" id="Seg_9214" s="T64">v</ta>
            <ta e="T67" id="Seg_9215" s="T66">adv</ta>
            <ta e="T68" id="Seg_9216" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9217" s="T68">adj</ta>
            <ta e="T70" id="Seg_9218" s="T69">adv</ta>
            <ta e="T71" id="Seg_9219" s="T70">n</ta>
            <ta e="T72" id="Seg_9220" s="T71">n</ta>
            <ta e="T73" id="Seg_9221" s="T72">v</ta>
            <ta e="T74" id="Seg_9222" s="T73">ptcl</ta>
            <ta e="T76" id="Seg_9223" s="T75">n</ta>
            <ta e="T77" id="Seg_9224" s="T76">n</ta>
            <ta e="T78" id="Seg_9225" s="T77">v</ta>
            <ta e="T79" id="Seg_9226" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_9227" s="T79">adj</ta>
            <ta e="T81" id="Seg_9228" s="T80">n</ta>
            <ta e="T83" id="Seg_9229" s="T82">pers</ta>
            <ta e="T84" id="Seg_9230" s="T83">n</ta>
            <ta e="T85" id="Seg_9231" s="T84">adv</ta>
            <ta e="T86" id="Seg_9232" s="T85">quant</ta>
            <ta e="T87" id="Seg_9233" s="T86">n</ta>
            <ta e="T88" id="Seg_9234" s="T87">v</ta>
            <ta e="T90" id="Seg_9235" s="T89">v</ta>
            <ta e="T91" id="Seg_9236" s="T90">n</ta>
            <ta e="T92" id="Seg_9237" s="T91">dempro</ta>
            <ta e="T94" id="Seg_9238" s="T93">adv</ta>
            <ta e="T95" id="Seg_9239" s="T94">v</ta>
            <ta e="T96" id="Seg_9240" s="T95">pers</ta>
            <ta e="T97" id="Seg_9241" s="T96">adv</ta>
            <ta e="T99" id="Seg_9242" s="T98">v</ta>
            <ta e="T100" id="Seg_9243" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_9244" s="T100">v</ta>
            <ta e="T102" id="Seg_9245" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_9246" s="T102">v</ta>
            <ta e="T105" id="Seg_9247" s="T104">adv</ta>
            <ta e="T107" id="Seg_9248" s="T106">v</ta>
            <ta e="T108" id="Seg_9249" s="T107">v</ta>
            <ta e="T109" id="Seg_9250" s="T108">pers</ta>
            <ta e="T110" id="Seg_9251" s="T109">adv</ta>
            <ta e="T111" id="Seg_9252" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_9253" s="T111">v</ta>
            <ta e="T113" id="Seg_9254" s="T112">n</ta>
            <ta e="T114" id="Seg_9255" s="T113">v</ta>
            <ta e="T115" id="Seg_9256" s="T114">num</ta>
            <ta e="T116" id="Seg_9257" s="T115">n</ta>
            <ta e="T117" id="Seg_9258" s="T116">adv</ta>
            <ta e="T118" id="Seg_9259" s="T117">v</ta>
            <ta e="T119" id="Seg_9260" s="T118">que</ta>
            <ta e="T120" id="Seg_9261" s="T119">adv</ta>
            <ta e="T121" id="Seg_9262" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_9263" s="T121">v</ta>
            <ta e="T123" id="Seg_9264" s="T122">conj</ta>
            <ta e="T124" id="Seg_9265" s="T123">pers</ta>
            <ta e="T125" id="Seg_9266" s="T124">v</ta>
            <ta e="T126" id="Seg_9267" s="T125">pers</ta>
            <ta e="T127" id="Seg_9268" s="T126">v</ta>
            <ta e="T128" id="Seg_9269" s="T127">v</ta>
            <ta e="T129" id="Seg_9270" s="T128">v</ta>
            <ta e="T131" id="Seg_9271" s="T130">n</ta>
            <ta e="T132" id="Seg_9272" s="T131">v</ta>
            <ta e="T133" id="Seg_9273" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_9274" s="T133">v</ta>
            <ta e="T136" id="Seg_9275" s="T135">n</ta>
            <ta e="T137" id="Seg_9276" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_9277" s="T137">v</ta>
            <ta e="T139" id="Seg_9278" s="T138">n</ta>
            <ta e="T141" id="Seg_9279" s="T140">n</ta>
            <ta e="T142" id="Seg_9280" s="T141">n</ta>
            <ta e="T144" id="Seg_9281" s="T143">num</ta>
            <ta e="T145" id="Seg_9282" s="T144">n</ta>
            <ta e="T146" id="Seg_9283" s="T145">v</ta>
            <ta e="T147" id="Seg_9284" s="T146">v</ta>
            <ta e="T150" id="Seg_9285" s="T149">aux</ta>
            <ta e="T151" id="Seg_9286" s="T150">v</ta>
            <ta e="T152" id="Seg_9287" s="T151">aux</ta>
            <ta e="T153" id="Seg_9288" s="T152">v</ta>
            <ta e="T154" id="Seg_9289" s="T153">v</ta>
            <ta e="T156" id="Seg_9290" s="T155">n</ta>
            <ta e="T157" id="Seg_9291" s="T156">que</ta>
            <ta e="T158" id="Seg_9292" s="T157">v</ta>
            <ta e="T159" id="Seg_9293" s="T158">adv</ta>
            <ta e="T160" id="Seg_9294" s="T159">v</ta>
            <ta e="T162" id="Seg_9295" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_9296" s="T162">n</ta>
            <ta e="T164" id="Seg_9297" s="T163">v</ta>
            <ta e="T165" id="Seg_9298" s="T164">adv</ta>
            <ta e="T166" id="Seg_9299" s="T165">n</ta>
            <ta e="T167" id="Seg_9300" s="T166">v</ta>
            <ta e="T169" id="Seg_9301" s="T168">que</ta>
            <ta e="T170" id="Seg_9302" s="T169">v</ta>
            <ta e="T171" id="Seg_9303" s="T170">v</ta>
            <ta e="T172" id="Seg_9304" s="T171">v</ta>
            <ta e="T173" id="Seg_9305" s="T172">v</ta>
            <ta e="T175" id="Seg_9306" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_9307" s="T175">v</ta>
            <ta e="T177" id="Seg_9308" s="T176">adj</ta>
            <ta e="T178" id="Seg_9309" s="T177">n</ta>
            <ta e="T179" id="Seg_9310" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9311" s="T179">v</ta>
            <ta e="T181" id="Seg_9312" s="T180">v</ta>
            <ta e="T182" id="Seg_9313" s="T181">adj</ta>
            <ta e="T183" id="Seg_9314" s="T182">adv</ta>
            <ta e="T185" id="Seg_9315" s="T184">n</ta>
            <ta e="T186" id="Seg_9316" s="T185">adv</ta>
            <ta e="T187" id="Seg_9317" s="T186">v</ta>
            <ta e="T188" id="Seg_9318" s="T187">pers</ta>
            <ta e="T189" id="Seg_9319" s="T188">v</ta>
            <ta e="T191" id="Seg_9320" s="T190">n</ta>
            <ta e="T192" id="Seg_9321" s="T191">v</ta>
            <ta e="T194" id="Seg_9322" s="T193">n</ta>
            <ta e="T195" id="Seg_9323" s="T194">v</ta>
            <ta e="T196" id="Seg_9324" s="T195">n</ta>
            <ta e="T197" id="Seg_9325" s="T196">v</ta>
            <ta e="T198" id="Seg_9326" s="T197">n</ta>
            <ta e="T199" id="Seg_9327" s="T198">v</ta>
            <ta e="T200" id="Seg_9328" s="T199">n</ta>
            <ta e="T202" id="Seg_9329" s="T201">v</ta>
            <ta e="T203" id="Seg_9330" s="T202">v</ta>
            <ta e="T204" id="Seg_9331" s="T203">que</ta>
            <ta e="T205" id="Seg_9332" s="T204">adv</ta>
            <ta e="T206" id="Seg_9333" s="T205">v</ta>
            <ta e="T208" id="Seg_9334" s="T207">n</ta>
            <ta e="T209" id="Seg_9335" s="T208">n</ta>
            <ta e="T210" id="Seg_9336" s="T209">v</ta>
            <ta e="T211" id="Seg_9337" s="T210">n</ta>
            <ta e="T212" id="Seg_9338" s="T211">v</ta>
            <ta e="T213" id="Seg_9339" s="T212">adv</ta>
            <ta e="T214" id="Seg_9340" s="T213">v</ta>
            <ta e="T215" id="Seg_9341" s="T214">dempro</ta>
            <ta e="T216" id="Seg_9342" s="T215">n</ta>
            <ta e="T217" id="Seg_9343" s="T216">conj</ta>
            <ta e="T218" id="Seg_9344" s="T217">n</ta>
            <ta e="T222" id="Seg_9345" s="T221">adv</ta>
            <ta e="T223" id="Seg_9346" s="T222">v</ta>
            <ta e="T224" id="Seg_9347" s="T223">adv</ta>
            <ta e="T225" id="Seg_9348" s="T224">n</ta>
            <ta e="T226" id="Seg_9349" s="T225">v</ta>
            <ta e="T851" id="Seg_9350" s="T227">propr</ta>
            <ta e="T228" id="Seg_9351" s="T851">n</ta>
            <ta e="T229" id="Seg_9352" s="T228">v</ta>
            <ta e="T230" id="Seg_9353" s="T229">n</ta>
            <ta e="T231" id="Seg_9354" s="T230">n</ta>
            <ta e="T233" id="Seg_9355" s="T232">n</ta>
            <ta e="T235" id="Seg_9356" s="T234">v</ta>
            <ta e="T237" id="Seg_9357" s="T236">quant</ta>
            <ta e="T238" id="Seg_9358" s="T237">n</ta>
            <ta e="T239" id="Seg_9359" s="T238">v</ta>
            <ta e="T241" id="Seg_9360" s="T240">n</ta>
            <ta e="T242" id="Seg_9361" s="T241">adv</ta>
            <ta e="T243" id="Seg_9362" s="T242">quant</ta>
            <ta e="T244" id="Seg_9363" s="T243">v</ta>
            <ta e="T245" id="Seg_9364" s="T244">ptcl</ta>
            <ta e="T247" id="Seg_9365" s="T246">pers</ta>
            <ta e="T248" id="Seg_9366" s="T247">n</ta>
            <ta e="T249" id="Seg_9367" s="T248">v</ta>
            <ta e="T250" id="Seg_9368" s="T249">conj</ta>
            <ta e="T251" id="Seg_9369" s="T250">n</ta>
            <ta e="T253" id="Seg_9370" s="T252">v</ta>
            <ta e="T255" id="Seg_9371" s="T254">v</ta>
            <ta e="T258" id="Seg_9372" s="T257">pers</ta>
            <ta e="T259" id="Seg_9373" s="T258">adv</ta>
            <ta e="T260" id="Seg_9374" s="T259">n</ta>
            <ta e="T261" id="Seg_9375" s="T260">n</ta>
            <ta e="T262" id="Seg_9376" s="T261">n</ta>
            <ta e="T263" id="Seg_9377" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_9378" s="T263">v</ta>
            <ta e="T265" id="Seg_9379" s="T264">n</ta>
            <ta e="T266" id="Seg_9380" s="T265">ptcl</ta>
            <ta e="T269" id="Seg_9381" s="T268">n</ta>
            <ta e="T270" id="Seg_9382" s="T269">v</ta>
            <ta e="T271" id="Seg_9383" s="T270">n</ta>
            <ta e="T272" id="Seg_9384" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_9385" s="T272">v</ta>
            <ta e="T274" id="Seg_9386" s="T273">pers</ta>
            <ta e="T275" id="Seg_9387" s="T274">v</ta>
            <ta e="T276" id="Seg_9388" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_9389" s="T276">v</ta>
            <ta e="T279" id="Seg_9390" s="T278">n</ta>
            <ta e="T280" id="Seg_9391" s="T279">v</ta>
            <ta e="T281" id="Seg_9392" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_9393" s="T281">v</ta>
            <ta e="T283" id="Seg_9394" s="T282">adv</ta>
            <ta e="T285" id="Seg_9395" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_9396" s="T285">v</ta>
            <ta e="T288" id="Seg_9397" s="T287">n</ta>
            <ta e="T289" id="Seg_9398" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_9399" s="T289">n</ta>
            <ta e="T291" id="Seg_9400" s="T290">v</ta>
            <ta e="T292" id="Seg_9401" s="T291">que</ta>
            <ta e="T293" id="Seg_9402" s="T292">n</ta>
            <ta e="T294" id="Seg_9403" s="T293">v</ta>
            <ta e="T295" id="Seg_9404" s="T294">adv</ta>
            <ta e="T296" id="Seg_9405" s="T295">%%</ta>
            <ta e="T297" id="Seg_9406" s="T296">v</ta>
            <ta e="T298" id="Seg_9407" s="T297">v</ta>
            <ta e="T299" id="Seg_9408" s="T298">n</ta>
            <ta e="T300" id="Seg_9409" s="T299">conj</ta>
            <ta e="T301" id="Seg_9410" s="T300">pers</ta>
            <ta e="T302" id="Seg_9411" s="T301">n</ta>
            <ta e="T303" id="Seg_9412" s="T302">v</ta>
            <ta e="T852" id="Seg_9413" s="T303">propr</ta>
            <ta e="T304" id="Seg_9414" s="T852">n</ta>
            <ta e="T305" id="Seg_9415" s="T304">conj</ta>
            <ta e="T306" id="Seg_9416" s="T305">n</ta>
            <ta e="T307" id="Seg_9417" s="T306">v</ta>
            <ta e="T308" id="Seg_9418" s="T307">adv</ta>
            <ta e="T309" id="Seg_9419" s="T308">v</ta>
            <ta e="T310" id="Seg_9420" s="T309">v</ta>
            <ta e="T312" id="Seg_9421" s="T311">dempro</ta>
            <ta e="T313" id="Seg_9422" s="T312">n</ta>
            <ta e="T314" id="Seg_9423" s="T313">adv</ta>
            <ta e="T315" id="Seg_9424" s="T314">dempro</ta>
            <ta e="T316" id="Seg_9425" s="T315">v</ta>
            <ta e="T317" id="Seg_9426" s="T316">dempro</ta>
            <ta e="T318" id="Seg_9427" s="T317">dempro</ta>
            <ta e="T319" id="Seg_9428" s="T318">n</ta>
            <ta e="T320" id="Seg_9429" s="T319">v</ta>
            <ta e="T321" id="Seg_9430" s="T320">dempro</ta>
            <ta e="T322" id="Seg_9431" s="T321">v</ta>
            <ta e="T323" id="Seg_9432" s="T322">conj</ta>
            <ta e="T324" id="Seg_9433" s="T323">v</ta>
            <ta e="T325" id="Seg_9434" s="T324">v</ta>
            <ta e="T326" id="Seg_9435" s="T325">dempro</ta>
            <ta e="T327" id="Seg_9436" s="T326">n</ta>
            <ta e="T328" id="Seg_9437" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_9438" s="T328">n</ta>
            <ta e="T330" id="Seg_9439" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_9440" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9441" s="T331">n</ta>
            <ta e="T333" id="Seg_9442" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_9443" s="T333">n</ta>
            <ta e="T335" id="Seg_9444" s="T334">que</ta>
            <ta e="T336" id="Seg_9445" s="T335">pers</ta>
            <ta e="T337" id="Seg_9446" s="T336">ptcl</ta>
            <ta e="T340" id="Seg_9447" s="T339">conj</ta>
            <ta e="T341" id="Seg_9448" s="T340">pers</ta>
            <ta e="T342" id="Seg_9449" s="T341">pers</ta>
            <ta e="T344" id="Seg_9450" s="T343">n</ta>
            <ta e="T345" id="Seg_9451" s="T344">v</ta>
            <ta e="T346" id="Seg_9452" s="T345">conj</ta>
            <ta e="T347" id="Seg_9453" s="T346">v</ta>
            <ta e="T350" id="Seg_9454" s="T349">pers</ta>
            <ta e="T351" id="Seg_9455" s="T350">n</ta>
            <ta e="T352" id="Seg_9456" s="T351">n</ta>
            <ta e="T353" id="Seg_9457" s="T352">v</ta>
            <ta e="T354" id="Seg_9458" s="T353">adv</ta>
            <ta e="T355" id="Seg_9459" s="T354">ptcl</ta>
            <ta e="T357" id="Seg_9460" s="T356">n</ta>
            <ta e="T358" id="Seg_9461" s="T357">v</ta>
            <ta e="T359" id="Seg_9462" s="T358">n</ta>
            <ta e="T360" id="Seg_9463" s="T359">adj</ta>
            <ta e="T361" id="Seg_9464" s="T360">n</ta>
            <ta e="T362" id="Seg_9465" s="T361">v</ta>
            <ta e="T363" id="Seg_9466" s="T362">dempro</ta>
            <ta e="T364" id="Seg_9467" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_9468" s="T364">adj</ta>
            <ta e="T366" id="Seg_9469" s="T365">v</ta>
            <ta e="T367" id="Seg_9470" s="T366">v</ta>
            <ta e="T369" id="Seg_9471" s="T368">v</ta>
            <ta e="T370" id="Seg_9472" s="T369">dempro</ta>
            <ta e="T371" id="Seg_9473" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_9474" s="T371">n</ta>
            <ta e="T373" id="Seg_9475" s="T372">v</ta>
            <ta e="T374" id="Seg_9476" s="T373">dempro</ta>
            <ta e="T375" id="Seg_9477" s="T374">adv</ta>
            <ta e="T376" id="Seg_9478" s="T375">adj</ta>
            <ta e="T377" id="Seg_9479" s="T376">adv</ta>
            <ta e="T378" id="Seg_9480" s="T377">n</ta>
            <ta e="T380" id="Seg_9481" s="T379">v</ta>
            <ta e="T382" id="Seg_9482" s="T381">n</ta>
            <ta e="T383" id="Seg_9483" s="T382">ptcl</ta>
            <ta e="T385" id="Seg_9484" s="T384">v</ta>
            <ta e="T386" id="Seg_9485" s="T385">quant</ta>
            <ta e="T387" id="Seg_9486" s="T386">n</ta>
            <ta e="T388" id="Seg_9487" s="T387">v</ta>
            <ta e="T389" id="Seg_9488" s="T388">conj</ta>
            <ta e="T390" id="Seg_9489" s="T389">pers</ta>
            <ta e="T391" id="Seg_9490" s="T390">n</ta>
            <ta e="T392" id="Seg_9491" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_9492" s="T392">v</ta>
            <ta e="T394" id="Seg_9493" s="T393">adv</ta>
            <ta e="T395" id="Seg_9494" s="T394">v</ta>
            <ta e="T396" id="Seg_9495" s="T395">aux</ta>
            <ta e="T397" id="Seg_9496" s="T396">v</ta>
            <ta e="T398" id="Seg_9497" s="T397">dempro</ta>
            <ta e="T399" id="Seg_9498" s="T398">n</ta>
            <ta e="T400" id="Seg_9499" s="T399">n</ta>
            <ta e="T401" id="Seg_9500" s="T400">n</ta>
            <ta e="T404" id="Seg_9501" s="T403">n</ta>
            <ta e="T405" id="Seg_9502" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_9503" s="T405">n</ta>
            <ta e="T407" id="Seg_9504" s="T406">v</ta>
            <ta e="T411" id="Seg_9505" s="T410">ptcl</ta>
            <ta e="T413" id="Seg_9506" s="T412">n</ta>
            <ta e="T414" id="Seg_9507" s="T413">n</ta>
            <ta e="T415" id="Seg_9508" s="T414">n</ta>
            <ta e="T416" id="Seg_9509" s="T415">n</ta>
            <ta e="T417" id="Seg_9510" s="T416">n</ta>
            <ta e="T418" id="Seg_9511" s="T417">n</ta>
            <ta e="T420" id="Seg_9512" s="T419">n</ta>
            <ta e="T422" id="Seg_9513" s="T420">n</ta>
            <ta e="T423" id="Seg_9514" s="T422">n</ta>
            <ta e="T424" id="Seg_9515" s="T423">n</ta>
            <ta e="T426" id="Seg_9516" s="T425">dempro</ta>
            <ta e="T427" id="Seg_9517" s="T426">n</ta>
            <ta e="T428" id="Seg_9518" s="T427">ptcl</ta>
            <ta e="T432" id="Seg_9519" s="T431">adv</ta>
            <ta e="T433" id="Seg_9520" s="T432">n</ta>
            <ta e="T434" id="Seg_9521" s="T433">v</ta>
            <ta e="T435" id="Seg_9522" s="T434">conj</ta>
            <ta e="T436" id="Seg_9523" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_9524" s="T436">v</ta>
            <ta e="T439" id="Seg_9525" s="T438">n</ta>
            <ta e="T440" id="Seg_9526" s="T439">conj</ta>
            <ta e="T441" id="Seg_9527" s="T440">adv</ta>
            <ta e="T442" id="Seg_9528" s="T441">v</ta>
            <ta e="T443" id="Seg_9529" s="T442">conj</ta>
            <ta e="T444" id="Seg_9530" s="T443">n</ta>
            <ta e="T445" id="Seg_9531" s="T444">quant</ta>
            <ta e="T446" id="Seg_9532" s="T445">v</ta>
            <ta e="T457" id="Seg_9533" s="T456">n</ta>
            <ta e="T460" id="Seg_9534" s="T459">que</ta>
            <ta e="T461" id="Seg_9535" s="T460">v</ta>
            <ta e="T462" id="Seg_9536" s="T461">n</ta>
            <ta e="T463" id="Seg_9537" s="T462">adv</ta>
            <ta e="T464" id="Seg_9538" s="T463">n</ta>
            <ta e="T465" id="Seg_9539" s="T464">v</ta>
            <ta e="T466" id="Seg_9540" s="T465">dempro</ta>
            <ta e="T467" id="Seg_9541" s="T466">v</ta>
            <ta e="T468" id="Seg_9542" s="T467">pers</ta>
            <ta e="T470" id="Seg_9543" s="T469">n</ta>
            <ta e="T471" id="Seg_9544" s="T470">v</ta>
            <ta e="T472" id="Seg_9545" s="T471">conj</ta>
            <ta e="T473" id="Seg_9546" s="T472">que</ta>
            <ta e="T474" id="Seg_9547" s="T473">v</ta>
            <ta e="T475" id="Seg_9548" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_9549" s="T475">v</ta>
            <ta e="T479" id="Seg_9550" s="T478">n</ta>
            <ta e="T480" id="Seg_9551" s="T479">conj</ta>
            <ta e="T481" id="Seg_9552" s="T480">pers</ta>
            <ta e="T482" id="Seg_9553" s="T481">n</ta>
            <ta e="T483" id="Seg_9554" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_9555" s="T483">v</ta>
            <ta e="T487" id="Seg_9556" s="T486">v</ta>
            <ta e="T488" id="Seg_9557" s="T487">n</ta>
            <ta e="T489" id="Seg_9558" s="T488">conj</ta>
            <ta e="T490" id="Seg_9559" s="T489">pers</ta>
            <ta e="T491" id="Seg_9560" s="T490">n</ta>
            <ta e="T493" id="Seg_9561" s="T492">dempro</ta>
            <ta e="T494" id="Seg_9562" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_9563" s="T494">v</ta>
            <ta e="T496" id="Seg_9564" s="T495">%%</ta>
            <ta e="T497" id="Seg_9565" s="T496">v</ta>
            <ta e="T498" id="Seg_9566" s="T497">conj</ta>
            <ta e="T499" id="Seg_9567" s="T498">dempro</ta>
            <ta e="T500" id="Seg_9568" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_9569" s="T500">n</ta>
            <ta e="T502" id="Seg_9570" s="T501">dempro</ta>
            <ta e="T503" id="Seg_9571" s="T502">v</ta>
            <ta e="T504" id="Seg_9572" s="T503">n</ta>
            <ta e="T505" id="Seg_9573" s="T504">n</ta>
            <ta e="T506" id="Seg_9574" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_9575" s="T506">v</ta>
            <ta e="T511" id="Seg_9576" s="T510">que</ta>
            <ta e="T512" id="Seg_9577" s="T511">n</ta>
            <ta e="T513" id="Seg_9578" s="T512">v</ta>
            <ta e="T514" id="Seg_9579" s="T513">adv</ta>
            <ta e="T515" id="Seg_9580" s="T514">v</ta>
            <ta e="T516" id="Seg_9581" s="T515">n</ta>
            <ta e="T517" id="Seg_9582" s="T516">dempro</ta>
            <ta e="T518" id="Seg_9583" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_9584" s="T518">dempro</ta>
            <ta e="T520" id="Seg_9585" s="T519">v</ta>
            <ta e="T521" id="Seg_9586" s="T520">adj</ta>
            <ta e="T522" id="Seg_9587" s="T521">n</ta>
            <ta e="T523" id="Seg_9588" s="T522">v</ta>
            <ta e="T524" id="Seg_9589" s="T523">dempro</ta>
            <ta e="T525" id="Seg_9590" s="T524">adv</ta>
            <ta e="T526" id="Seg_9591" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_9592" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_9593" s="T527">n</ta>
            <ta e="T532" id="Seg_9594" s="T531">adv</ta>
            <ta e="T533" id="Seg_9595" s="T532">dempro</ta>
            <ta e="T534" id="Seg_9596" s="T533">dempro</ta>
            <ta e="T535" id="Seg_9597" s="T534">v</ta>
            <ta e="T536" id="Seg_9598" s="T535">conj</ta>
            <ta e="T537" id="Seg_9599" s="T536">refl</ta>
            <ta e="T538" id="Seg_9600" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_9601" s="T538">v</ta>
            <ta e="T540" id="Seg_9602" s="T539">n</ta>
            <ta e="T541" id="Seg_9603" s="T540">v</ta>
            <ta e="T543" id="Seg_9604" s="T542">adv</ta>
            <ta e="T544" id="Seg_9605" s="T543">dempro</ta>
            <ta e="T545" id="Seg_9606" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_9607" s="T545">v</ta>
            <ta e="T547" id="Seg_9608" s="T546">n</ta>
            <ta e="T548" id="Seg_9609" s="T547">v</ta>
            <ta e="T551" id="Seg_9610" s="T550">n</ta>
            <ta e="T552" id="Seg_9611" s="T551">n</ta>
            <ta e="T848" id="Seg_9612" s="T552">v</ta>
            <ta e="T553" id="Seg_9613" s="T848">v</ta>
            <ta e="T554" id="Seg_9614" s="T553">n</ta>
            <ta e="T555" id="Seg_9615" s="T554">n</ta>
            <ta e="T556" id="Seg_9616" s="T555">v</ta>
            <ta e="T557" id="Seg_9617" s="T556">n</ta>
            <ta e="T558" id="Seg_9618" s="T557">v</ta>
            <ta e="T559" id="Seg_9619" s="T558">n</ta>
            <ta e="T560" id="Seg_9620" s="T559">v</ta>
            <ta e="T561" id="Seg_9621" s="T560">n</ta>
            <ta e="T562" id="Seg_9622" s="T561">v</ta>
            <ta e="T564" id="Seg_9623" s="T563">n</ta>
            <ta e="T565" id="Seg_9624" s="T564">ptcl</ta>
            <ta e="T567" id="Seg_9625" s="T566">n</ta>
            <ta e="T568" id="Seg_9626" s="T567">v</ta>
            <ta e="T569" id="Seg_9627" s="T568">n</ta>
            <ta e="T570" id="Seg_9628" s="T569">v</ta>
            <ta e="T571" id="Seg_9629" s="T570">n</ta>
            <ta e="T572" id="Seg_9630" s="T571">v</ta>
            <ta e="T573" id="Seg_9631" s="T572">n</ta>
            <ta e="T574" id="Seg_9632" s="T573">v</ta>
            <ta e="T575" id="Seg_9633" s="T574">n</ta>
            <ta e="T576" id="Seg_9634" s="T575">n</ta>
            <ta e="T578" id="Seg_9635" s="T577">v</ta>
            <ta e="T579" id="Seg_9636" s="T578">adv</ta>
            <ta e="T580" id="Seg_9637" s="T579">dempro</ta>
            <ta e="T581" id="Seg_9638" s="T580">n</ta>
            <ta e="T582" id="Seg_9639" s="T581">n</ta>
            <ta e="T583" id="Seg_9640" s="T582">v</ta>
            <ta e="T584" id="Seg_9641" s="T583">adj</ta>
            <ta e="T585" id="Seg_9642" s="T584">n</ta>
            <ta e="T586" id="Seg_9643" s="T585">v</ta>
            <ta e="T587" id="Seg_9644" s="T586">v</ta>
            <ta e="T589" id="Seg_9645" s="T588">n</ta>
            <ta e="T590" id="Seg_9646" s="T589">v</ta>
            <ta e="T591" id="Seg_9647" s="T590">dempro</ta>
            <ta e="T592" id="Seg_9648" s="T591">n</ta>
            <ta e="T593" id="Seg_9649" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_9650" s="T593">v</ta>
            <ta e="T595" id="Seg_9651" s="T594">adv</ta>
            <ta e="T596" id="Seg_9652" s="T595">n</ta>
            <ta e="T597" id="Seg_9653" s="T596">v</ta>
            <ta e="T598" id="Seg_9654" s="T597">adv</ta>
            <ta e="T599" id="Seg_9655" s="T598">v</ta>
            <ta e="T600" id="Seg_9656" s="T599">n</ta>
            <ta e="T601" id="Seg_9657" s="T600">adj</ta>
            <ta e="T602" id="Seg_9658" s="T601">n</ta>
            <ta e="T603" id="Seg_9659" s="T602">v</ta>
            <ta e="T604" id="Seg_9660" s="T603">v</ta>
            <ta e="T605" id="Seg_9661" s="T604">conj</ta>
            <ta e="T606" id="Seg_9662" s="T605">n</ta>
            <ta e="T607" id="Seg_9663" s="T606">v</ta>
            <ta e="T609" id="Seg_9664" s="T608">n</ta>
            <ta e="T610" id="Seg_9665" s="T609">v</ta>
            <ta e="T612" id="Seg_9666" s="T611">n</ta>
            <ta e="T613" id="Seg_9667" s="T612">adv</ta>
            <ta e="T614" id="Seg_9668" s="T613">n</ta>
            <ta e="T615" id="Seg_9669" s="T614">v</ta>
            <ta e="T616" id="Seg_9670" s="T615">n</ta>
            <ta e="T617" id="Seg_9671" s="T616">conj</ta>
            <ta e="T618" id="Seg_9672" s="T617">v</ta>
            <ta e="T619" id="Seg_9673" s="T618">dempro</ta>
            <ta e="T620" id="Seg_9674" s="T619">adv</ta>
            <ta e="T621" id="Seg_9675" s="T620">n</ta>
            <ta e="T622" id="Seg_9676" s="T621">v</ta>
            <ta e="T623" id="Seg_9677" s="T622">adv</ta>
            <ta e="T624" id="Seg_9678" s="T623">n</ta>
            <ta e="T625" id="Seg_9679" s="T624">v</ta>
            <ta e="T628" id="Seg_9680" s="T627">v</ta>
            <ta e="T629" id="Seg_9681" s="T628">n</ta>
            <ta e="T630" id="Seg_9682" s="T629">conj</ta>
            <ta e="T631" id="Seg_9683" s="T630">adj</ta>
            <ta e="T632" id="Seg_9684" s="T631">n</ta>
            <ta e="T633" id="Seg_9685" s="T632">v</ta>
            <ta e="T635" id="Seg_9686" s="T633">adv</ta>
            <ta e="T638" id="Seg_9687" s="T637">n</ta>
            <ta e="T639" id="Seg_9688" s="T638">n</ta>
            <ta e="T640" id="Seg_9689" s="T639">v</ta>
            <ta e="T641" id="Seg_9690" s="T640">adv</ta>
            <ta e="T642" id="Seg_9691" s="T641">n</ta>
            <ta e="T643" id="Seg_9692" s="T642">v</ta>
            <ta e="T644" id="Seg_9693" s="T643">adv</ta>
            <ta e="T646" id="Seg_9694" s="T645">n</ta>
            <ta e="T647" id="Seg_9695" s="T646">v</ta>
            <ta e="T648" id="Seg_9696" s="T647">adv</ta>
            <ta e="T649" id="Seg_9697" s="T648">n</ta>
            <ta e="T650" id="Seg_9698" s="T649">v</ta>
            <ta e="T651" id="Seg_9699" s="T650">conj</ta>
            <ta e="T652" id="Seg_9700" s="T651">n</ta>
            <ta e="T653" id="Seg_9701" s="T652">que</ta>
            <ta e="T654" id="Seg_9702" s="T653">ptcl</ta>
            <ta e="T656" id="Seg_9703" s="T655">v</ta>
            <ta e="T658" id="Seg_9704" s="T657">adj</ta>
            <ta e="T659" id="Seg_9705" s="T658">v</ta>
            <ta e="T660" id="Seg_9706" s="T659">ptcl</ta>
            <ta e="T662" id="Seg_9707" s="T661">n</ta>
            <ta e="T663" id="Seg_9708" s="T662">v</ta>
            <ta e="T664" id="Seg_9709" s="T663">conj</ta>
            <ta e="T665" id="Seg_9710" s="T664">adj</ta>
            <ta e="T666" id="Seg_9711" s="T665">v</ta>
            <ta e="T667" id="Seg_9712" s="T666">ptcl</ta>
            <ta e="T669" id="Seg_9713" s="T668">v</ta>
            <ta e="T671" id="Seg_9714" s="T670">v</ta>
            <ta e="T673" id="Seg_9715" s="T672">adj</ta>
            <ta e="T674" id="Seg_9716" s="T673">v</ta>
            <ta e="T677" id="Seg_9717" s="T676">num</ta>
            <ta e="T678" id="Seg_9718" s="T677">n</ta>
            <ta e="T679" id="Seg_9719" s="T678">v</ta>
            <ta e="T680" id="Seg_9720" s="T679">num</ta>
            <ta e="T681" id="Seg_9721" s="T680">n</ta>
            <ta e="T682" id="Seg_9722" s="T681">que</ta>
            <ta e="T683" id="Seg_9723" s="T682">adj</ta>
            <ta e="T684" id="Seg_9724" s="T683">v</ta>
            <ta e="T685" id="Seg_9725" s="T684">dempro</ta>
            <ta e="T686" id="Seg_9726" s="T685">adv</ta>
            <ta e="T687" id="Seg_9727" s="T686">v</ta>
            <ta e="T688" id="Seg_9728" s="T687">conj</ta>
            <ta e="T689" id="Seg_9729" s="T688">que</ta>
            <ta e="T690" id="Seg_9730" s="T689">n</ta>
            <ta e="T849" id="Seg_9731" s="T690">v</ta>
            <ta e="T691" id="Seg_9732" s="T849">v</ta>
            <ta e="T692" id="Seg_9733" s="T691">adv</ta>
            <ta e="T693" id="Seg_9734" s="T692">dempro</ta>
            <ta e="T694" id="Seg_9735" s="T693">v</ta>
            <ta e="T695" id="Seg_9736" s="T694">n</ta>
            <ta e="T696" id="Seg_9737" s="T695">adj</ta>
            <ta e="T697" id="Seg_9738" s="T696">n</ta>
            <ta e="T699" id="Seg_9739" s="T698">n</ta>
            <ta e="T700" id="Seg_9740" s="T699">v</ta>
            <ta e="T701" id="Seg_9741" s="T700">propr</ta>
            <ta e="T702" id="Seg_9742" s="T701">v</ta>
            <ta e="T703" id="Seg_9743" s="T702">adv</ta>
            <ta e="T704" id="Seg_9744" s="T703">adj</ta>
            <ta e="T705" id="Seg_9745" s="T704">n</ta>
            <ta e="T706" id="Seg_9746" s="T705">n</ta>
            <ta e="T707" id="Seg_9747" s="T706">v</ta>
            <ta e="T709" id="Seg_9748" s="T708">adv</ta>
            <ta e="T710" id="Seg_9749" s="T709">dempro</ta>
            <ta e="T711" id="Seg_9750" s="T710">v</ta>
            <ta e="T712" id="Seg_9751" s="T711">n</ta>
            <ta e="T713" id="Seg_9752" s="T712">conj</ta>
            <ta e="T714" id="Seg_9753" s="T713">n</ta>
            <ta e="T715" id="Seg_9754" s="T714">v</ta>
            <ta e="T716" id="Seg_9755" s="T715">n</ta>
            <ta e="T717" id="Seg_9756" s="T716">v</ta>
            <ta e="T719" id="Seg_9757" s="T718">conj</ta>
            <ta e="T720" id="Seg_9758" s="T719">n</ta>
            <ta e="T722" id="Seg_9759" s="T721">adv</ta>
            <ta e="T723" id="Seg_9760" s="T722">dempro</ta>
            <ta e="T724" id="Seg_9761" s="T723">n</ta>
            <ta e="T725" id="Seg_9762" s="T724">n</ta>
            <ta e="T726" id="Seg_9763" s="T725">v</ta>
            <ta e="T727" id="Seg_9764" s="T726">v</ta>
            <ta e="T728" id="Seg_9765" s="T727">n</ta>
            <ta e="T729" id="Seg_9766" s="T728">v</ta>
            <ta e="T730" id="Seg_9767" s="T729">v</ta>
            <ta e="T732" id="Seg_9768" s="T731">adv</ta>
            <ta e="T733" id="Seg_9769" s="T732">dempro</ta>
            <ta e="T734" id="Seg_9770" s="T733">n</ta>
            <ta e="T735" id="Seg_9771" s="T734">v</ta>
            <ta e="T736" id="Seg_9772" s="T735">v</ta>
            <ta e="T737" id="Seg_9773" s="T736">conj</ta>
            <ta e="T738" id="Seg_9774" s="T737">v</ta>
            <ta e="T740" id="Seg_9775" s="T739">adv</ta>
            <ta e="T741" id="Seg_9776" s="T740">adv</ta>
            <ta e="T742" id="Seg_9777" s="T741">dempro</ta>
            <ta e="T743" id="Seg_9778" s="T742">adv</ta>
            <ta e="T744" id="Seg_9779" s="T743">v</ta>
            <ta e="T745" id="Seg_9780" s="T744">n</ta>
            <ta e="T746" id="Seg_9781" s="T745">conj</ta>
            <ta e="T747" id="Seg_9782" s="T746">n</ta>
            <ta e="T748" id="Seg_9783" s="T747">v</ta>
            <ta e="T749" id="Seg_9784" s="T748">adv</ta>
            <ta e="T750" id="Seg_9785" s="T749">v</ta>
            <ta e="T751" id="Seg_9786" s="T750">dempro</ta>
            <ta e="T753" id="Seg_9787" s="T752">v</ta>
            <ta e="T754" id="Seg_9788" s="T753">n</ta>
            <ta e="T755" id="Seg_9789" s="T754">v</ta>
            <ta e="T756" id="Seg_9790" s="T755">n</ta>
            <ta e="T757" id="Seg_9791" s="T756">v</ta>
            <ta e="T759" id="Seg_9792" s="T758">dempro</ta>
            <ta e="T760" id="Seg_9793" s="T759">adv</ta>
            <ta e="T761" id="Seg_9794" s="T760">n</ta>
            <ta e="T762" id="Seg_9795" s="T761">v</ta>
            <ta e="T763" id="Seg_9796" s="T762">conj</ta>
            <ta e="T764" id="Seg_9797" s="T763">v</ta>
            <ta e="T765" id="Seg_9798" s="T764">conj</ta>
            <ta e="T766" id="Seg_9799" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_9800" s="T766">n</ta>
            <ta e="T769" id="Seg_9801" s="T767">v</ta>
            <ta e="T772" id="Seg_9802" s="T771">adv</ta>
            <ta e="T773" id="Seg_9803" s="T772">n</ta>
            <ta e="T774" id="Seg_9804" s="T773">v</ta>
            <ta e="T775" id="Seg_9805" s="T774">n</ta>
            <ta e="T776" id="Seg_9806" s="T775">conj</ta>
            <ta e="T777" id="Seg_9807" s="T776">adv</ta>
            <ta e="T778" id="Seg_9808" s="T777">v</ta>
            <ta e="T779" id="Seg_9809" s="T778">n</ta>
            <ta e="T780" id="Seg_9810" s="T779">conj</ta>
            <ta e="T781" id="Seg_9811" s="T780">n</ta>
            <ta e="T782" id="Seg_9812" s="T781">conj</ta>
            <ta e="T783" id="Seg_9813" s="T782">n</ta>
            <ta e="T784" id="Seg_9814" s="T783">v</ta>
            <ta e="T785" id="Seg_9815" s="T784">n</ta>
            <ta e="T786" id="Seg_9816" s="T785">adv</ta>
            <ta e="T787" id="Seg_9817" s="T786">que</ta>
            <ta e="T788" id="Seg_9818" s="T787">v</ta>
            <ta e="T789" id="Seg_9819" s="T788">que</ta>
            <ta e="T790" id="Seg_9820" s="T789">adj</ta>
            <ta e="T791" id="Seg_9821" s="T790">v</ta>
            <ta e="T793" id="Seg_9822" s="T792">dempro</ta>
            <ta e="T795" id="Seg_9823" s="T794">adj</ta>
            <ta e="T796" id="Seg_9824" s="T795">n</ta>
            <ta e="T798" id="Seg_9825" s="T797">v</ta>
            <ta e="T800" id="Seg_9826" s="T799">v</ta>
            <ta e="T801" id="Seg_9827" s="T800">n</ta>
            <ta e="T802" id="Seg_9828" s="T801">quant</ta>
            <ta e="T803" id="Seg_9829" s="T802">v</ta>
            <ta e="T804" id="Seg_9830" s="T803">adv</ta>
            <ta e="T805" id="Seg_9831" s="T804">n</ta>
            <ta e="T806" id="Seg_9832" s="T805">v</ta>
            <ta e="T807" id="Seg_9833" s="T806">dempro</ta>
            <ta e="T808" id="Seg_9834" s="T807">n</ta>
            <ta e="T809" id="Seg_9835" s="T808">v</ta>
            <ta e="T810" id="Seg_9836" s="T809">adv</ta>
            <ta e="T811" id="Seg_9837" s="T810">v</ta>
            <ta e="T812" id="Seg_9838" s="T811">n</ta>
            <ta e="T813" id="Seg_9839" s="T812">v</ta>
            <ta e="T814" id="Seg_9840" s="T813">adv</ta>
            <ta e="T815" id="Seg_9841" s="T814">v</ta>
            <ta e="T816" id="Seg_9842" s="T815">n</ta>
            <ta e="T818" id="Seg_9843" s="T817">conj</ta>
            <ta e="T819" id="Seg_9844" s="T818">n</ta>
            <ta e="T820" id="Seg_9845" s="T819">v</ta>
            <ta e="T823" id="Seg_9846" s="T822">dempro</ta>
            <ta e="T825" id="Seg_9847" s="T824">n</ta>
            <ta e="T826" id="Seg_9848" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_9849" s="T826">v</ta>
            <ta e="T828" id="Seg_9850" s="T827">dempro</ta>
            <ta e="T829" id="Seg_9851" s="T828">n</ta>
            <ta e="T830" id="Seg_9852" s="T829">n</ta>
            <ta e="T831" id="Seg_9853" s="T830">n</ta>
            <ta e="T832" id="Seg_9854" s="T831">v</ta>
            <ta e="T833" id="Seg_9855" s="T832">conj</ta>
            <ta e="T834" id="Seg_9856" s="T833">n</ta>
            <ta e="T835" id="Seg_9857" s="T834">v</ta>
            <ta e="T837" id="Seg_9858" s="T836">dempro</ta>
            <ta e="T838" id="Seg_9859" s="T837">n</ta>
            <ta e="T839" id="Seg_9860" s="T838">v</ta>
            <ta e="T840" id="Seg_9861" s="T839">n</ta>
            <ta e="T841" id="Seg_9862" s="T840">num</ta>
            <ta e="T842" id="Seg_9863" s="T841">num</ta>
            <ta e="T843" id="Seg_9864" s="T842">num</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_9865" s="T0">pro:L</ta>
            <ta e="T2" id="Seg_9866" s="T1">pro:G</ta>
            <ta e="T3" id="Seg_9867" s="T2">0.2.h:A</ta>
            <ta e="T5" id="Seg_9868" s="T4">np:L</ta>
            <ta e="T8" id="Seg_9869" s="T7">np:L</ta>
            <ta e="T9" id="Seg_9870" s="T8">0.1.h:E</ta>
            <ta e="T12" id="Seg_9871" s="T11">pro:Th</ta>
            <ta e="T13" id="Seg_9872" s="T12">0.2.h:E</ta>
            <ta e="T14" id="Seg_9873" s="T13">adv:Time</ta>
            <ta e="T15" id="Seg_9874" s="T14">adv:L</ta>
            <ta e="T16" id="Seg_9875" s="T15">0.1.h:A</ta>
            <ta e="T18" id="Seg_9876" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_9877" s="T18">0.1.h:A</ta>
            <ta e="T24" id="Seg_9878" s="T23">0.1.h:A</ta>
            <ta e="T26" id="Seg_9879" s="T25">adv:L</ta>
            <ta e="T29" id="Seg_9880" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_9881" s="T29">0.1.h:A</ta>
            <ta e="T31" id="Seg_9882" s="T30">adv:L</ta>
            <ta e="T33" id="Seg_9883" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_9884" s="T33">adv:L</ta>
            <ta e="T35" id="Seg_9885" s="T34">0.1.h:A</ta>
            <ta e="T36" id="Seg_9886" s="T35">np:G</ta>
            <ta e="T37" id="Seg_9887" s="T36">0.1.h:A</ta>
            <ta e="T38" id="Seg_9888" s="T37">pro.h:A</ta>
            <ta e="T39" id="Seg_9889" s="T38">pro.h:R</ta>
            <ta e="T42" id="Seg_9890" s="T41">np:Th</ta>
            <ta e="T43" id="Seg_9891" s="T42">0.3.h:A</ta>
            <ta e="T45" id="Seg_9892" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_9893" s="T45">adv:L</ta>
            <ta e="T47" id="Seg_9894" s="T46">0.1.h:A</ta>
            <ta e="T48" id="Seg_9895" s="T47">np:A</ta>
            <ta e="T50" id="Seg_9896" s="T49">np.h:A</ta>
            <ta e="T53" id="Seg_9897" s="T52">pro.h:R</ta>
            <ta e="T54" id="Seg_9898" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_9899" s="T54">pro:A</ta>
            <ta e="T58" id="Seg_9900" s="T57">0.3:A</ta>
            <ta e="T61" id="Seg_9901" s="T60">np:Poss</ta>
            <ta e="T62" id="Seg_9902" s="T61">np:L</ta>
            <ta e="T64" id="Seg_9903" s="T63">np:P</ta>
            <ta e="T67" id="Seg_9904" s="T66">adv:L</ta>
            <ta e="T71" id="Seg_9905" s="T70">np:Th</ta>
            <ta e="T72" id="Seg_9906" s="T71">np:L</ta>
            <ta e="T77" id="Seg_9907" s="T76">np:A</ta>
            <ta e="T83" id="Seg_9908" s="T82">pro.h:Poss</ta>
            <ta e="T84" id="Seg_9909" s="T83">np:L</ta>
            <ta e="T87" id="Seg_9910" s="T86">np.h:P</ta>
            <ta e="T90" id="Seg_9911" s="T89">0.1.h:A</ta>
            <ta e="T91" id="Seg_9912" s="T90">np:G</ta>
            <ta e="T92" id="Seg_9913" s="T91">pro.h:E</ta>
            <ta e="T96" id="Seg_9914" s="T95">pro.h:P</ta>
            <ta e="T97" id="Seg_9915" s="T96">adv:L</ta>
            <ta e="T101" id="Seg_9916" s="T100">0.2.h:P</ta>
            <ta e="T103" id="Seg_9917" s="T102">0.2.h:P</ta>
            <ta e="T105" id="Seg_9918" s="T104">adv:L</ta>
            <ta e="T107" id="Seg_9919" s="T106">0.1.h:A</ta>
            <ta e="T109" id="Seg_9920" s="T108">pro.h:P</ta>
            <ta e="T110" id="Seg_9921" s="T109">adv:Time</ta>
            <ta e="T112" id="Seg_9922" s="T111">0.1.h:A</ta>
            <ta e="T113" id="Seg_9923" s="T112">np:Th</ta>
            <ta e="T116" id="Seg_9924" s="T115">np.h:A</ta>
            <ta e="T117" id="Seg_9925" s="T116">adv:Time</ta>
            <ta e="T118" id="Seg_9926" s="T117">0.1.h:A</ta>
            <ta e="T122" id="Seg_9927" s="T121">0.2.h:A</ta>
            <ta e="T124" id="Seg_9928" s="T123">pro.h:E</ta>
            <ta e="T126" id="Seg_9929" s="T125">pro.h:P</ta>
            <ta e="T129" id="Seg_9930" s="T128">0.1.h:A</ta>
            <ta e="T131" id="Seg_9931" s="T130">np:E</ta>
            <ta e="T134" id="Seg_9932" s="T133">0.3:A</ta>
            <ta e="T136" id="Seg_9933" s="T135">np.h:A</ta>
            <ta e="T141" id="Seg_9934" s="T140">np:Poss</ta>
            <ta e="T142" id="Seg_9935" s="T141">np:L</ta>
            <ta e="T145" id="Seg_9936" s="T144">np.h:A</ta>
            <ta e="T150" id="Seg_9937" s="T149">0.2.h:A</ta>
            <ta e="T152" id="Seg_9938" s="T151">0.2.h:A</ta>
            <ta e="T154" id="Seg_9939" s="T153">0.2.h:E</ta>
            <ta e="T156" id="Seg_9940" s="T155">np.h:A</ta>
            <ta e="T159" id="Seg_9941" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_9942" s="T159">0.3.h:A</ta>
            <ta e="T163" id="Seg_9943" s="T162">np:P</ta>
            <ta e="T165" id="Seg_9944" s="T164">adv:Time</ta>
            <ta e="T166" id="Seg_9945" s="T165">np:R</ta>
            <ta e="T169" id="Seg_9946" s="T168">pro:Th</ta>
            <ta e="T171" id="Seg_9947" s="T170">0.2.h:A</ta>
            <ta e="T173" id="Seg_9948" s="T172">0.2.h:A</ta>
            <ta e="T176" id="Seg_9949" s="T175">0.3.h:A</ta>
            <ta e="T180" id="Seg_9950" s="T179">0.3.h:A</ta>
            <ta e="T181" id="Seg_9951" s="T180">0.3.h:Th</ta>
            <ta e="T185" id="Seg_9952" s="T184">np:Th</ta>
            <ta e="T186" id="Seg_9953" s="T185">adv:Time</ta>
            <ta e="T187" id="Seg_9954" s="T186">0.1.h:A</ta>
            <ta e="T188" id="Seg_9955" s="T187">pro.h:R</ta>
            <ta e="T191" id="Seg_9956" s="T190">np:Th</ta>
            <ta e="T194" id="Seg_9957" s="T193">np:Th</ta>
            <ta e="T196" id="Seg_9958" s="T195">np:Th</ta>
            <ta e="T198" id="Seg_9959" s="T197">np:Th</ta>
            <ta e="T200" id="Seg_9960" s="T199">np:P</ta>
            <ta e="T202" id="Seg_9961" s="T201">0.1.h:E</ta>
            <ta e="T203" id="Seg_9962" s="T202">0.1.h:E</ta>
            <ta e="T205" id="Seg_9963" s="T204">adv:L</ta>
            <ta e="T208" id="Seg_9964" s="T207">np:Th</ta>
            <ta e="T209" id="Seg_9965" s="T208">np:L</ta>
            <ta e="T211" id="Seg_9966" s="T210">np:Th</ta>
            <ta e="T212" id="Seg_9967" s="T211">0.3.h:A</ta>
            <ta e="T213" id="Seg_9968" s="T212">adv:Time</ta>
            <ta e="T214" id="Seg_9969" s="T213">0.3.h:A</ta>
            <ta e="T216" id="Seg_9970" s="T215">np:P</ta>
            <ta e="T218" id="Seg_9971" s="T217">np:P</ta>
            <ta e="T222" id="Seg_9972" s="T221">adv:Time</ta>
            <ta e="T223" id="Seg_9973" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_9974" s="T223">adv:Time</ta>
            <ta e="T225" id="Seg_9975" s="T224">np:R</ta>
            <ta e="T226" id="Seg_9976" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_9977" s="T227">np:G</ta>
            <ta e="T229" id="Seg_9978" s="T228">0.1.h:A</ta>
            <ta e="T231" id="Seg_9979" s="T230">np:L</ta>
            <ta e="T233" id="Seg_9980" s="T232">np:R</ta>
            <ta e="T235" id="Seg_9981" s="T234">0.1.h:A</ta>
            <ta e="T238" id="Seg_9982" s="T237">np.h:P</ta>
            <ta e="T241" id="Seg_9983" s="T240">np:Th</ta>
            <ta e="T247" id="Seg_9984" s="T246">pro.h:Poss</ta>
            <ta e="T248" id="Seg_9985" s="T247">np.h:E</ta>
            <ta e="T251" id="Seg_9986" s="T250">np.h:A 0.3.h:Poss</ta>
            <ta e="T259" id="Seg_9987" s="T258">adv:Time</ta>
            <ta e="T260" id="Seg_9988" s="T259">n:Time</ta>
            <ta e="T261" id="Seg_9989" s="T260">np.h:A</ta>
            <ta e="T262" id="Seg_9990" s="T261">np.h:Com</ta>
            <ta e="T265" id="Seg_9991" s="T264">np:L</ta>
            <ta e="T269" id="Seg_9992" s="T268">np:Th</ta>
            <ta e="T273" id="Seg_9993" s="T272">0.3.h:A</ta>
            <ta e="T274" id="Seg_9994" s="T273">pro.h:A</ta>
            <ta e="T277" id="Seg_9995" s="T276">0.1.h:A</ta>
            <ta e="T279" id="Seg_9996" s="T278">np:Th</ta>
            <ta e="T282" id="Seg_9997" s="T281">0.3:A</ta>
            <ta e="T283" id="Seg_9998" s="T282">adv:Time</ta>
            <ta e="T288" id="Seg_9999" s="T287">np:Poss</ta>
            <ta e="T290" id="Seg_10000" s="T289">np:E</ta>
            <ta e="T292" id="Seg_10001" s="T291">pro:L</ta>
            <ta e="T293" id="Seg_10002" s="T292">np:P</ta>
            <ta e="T294" id="Seg_10003" s="T293">0.1.h:A</ta>
            <ta e="T295" id="Seg_10004" s="T294">adv:Time</ta>
            <ta e="T297" id="Seg_10005" s="T296">0.3.h:A</ta>
            <ta e="T298" id="Seg_10006" s="T297">0.3.h:E</ta>
            <ta e="T299" id="Seg_10007" s="T298">np:L</ta>
            <ta e="T301" id="Seg_10008" s="T300">pro.h:Poss</ta>
            <ta e="T302" id="Seg_10009" s="T301">np.h:A</ta>
            <ta e="T304" id="Seg_10010" s="T303">np:G</ta>
            <ta e="T306" id="Seg_10011" s="T305">np:P</ta>
            <ta e="T307" id="Seg_10012" s="T306">0.3.h:A</ta>
            <ta e="T308" id="Seg_10013" s="T307">adv:Time</ta>
            <ta e="T309" id="Seg_10014" s="T308">0.3.h:A</ta>
            <ta e="T312" id="Seg_10015" s="T311">pro:G</ta>
            <ta e="T313" id="Seg_10016" s="T312">np:Ins</ta>
            <ta e="T314" id="Seg_10017" s="T313">adv:Time</ta>
            <ta e="T315" id="Seg_10018" s="T314">pro.h:A</ta>
            <ta e="T317" id="Seg_10019" s="T316">pro.h:A</ta>
            <ta e="T318" id="Seg_10020" s="T317">pro.h:R</ta>
            <ta e="T319" id="Seg_10021" s="T318">np:Th</ta>
            <ta e="T321" id="Seg_10022" s="T320">pro.h:A</ta>
            <ta e="T325" id="Seg_10023" s="T324">0.3.h:A</ta>
            <ta e="T326" id="Seg_10024" s="T325">pro.h:R</ta>
            <ta e="T341" id="Seg_10025" s="T340">pro.h:Th</ta>
            <ta e="T342" id="Seg_10026" s="T341">pro:G</ta>
            <ta e="T344" id="Seg_10027" s="T343">np:Ins</ta>
            <ta e="T347" id="Seg_10028" s="T346">0.2.h:A</ta>
            <ta e="T350" id="Seg_10029" s="T349">pro.h:Poss</ta>
            <ta e="T351" id="Seg_10030" s="T350">np.h:A</ta>
            <ta e="T352" id="Seg_10031" s="T351">np:G</ta>
            <ta e="T354" id="Seg_10032" s="T353">adv:L</ta>
            <ta e="T357" id="Seg_10033" s="T356">np:P</ta>
            <ta e="T358" id="Seg_10034" s="T357">0.3.h:A</ta>
            <ta e="T359" id="Seg_10035" s="T358">np:G</ta>
            <ta e="T362" id="Seg_10036" s="T361">0.3.h:A</ta>
            <ta e="T363" id="Seg_10037" s="T362">0.3:P</ta>
            <ta e="T367" id="Seg_10038" s="T366">0.2.h:A</ta>
            <ta e="T370" id="Seg_10039" s="T369">pro.h:A</ta>
            <ta e="T372" id="Seg_10040" s="T371">np:Th</ta>
            <ta e="T374" id="Seg_10041" s="T373">pro:Th</ta>
            <ta e="T377" id="Seg_10042" s="T376">adv:Time</ta>
            <ta e="T378" id="Seg_10043" s="T377">np:G</ta>
            <ta e="T380" id="Seg_10044" s="T379">0.3.h:A</ta>
            <ta e="T382" id="Seg_10045" s="T381">np:Ins</ta>
            <ta e="T385" id="Seg_10046" s="T384">0.3.h:A</ta>
            <ta e="T387" id="Seg_10047" s="T386">np.h:A</ta>
            <ta e="T390" id="Seg_10048" s="T389">pro.h:Poss</ta>
            <ta e="T394" id="Seg_10049" s="T393">adv:Time</ta>
            <ta e="T395" id="Seg_10050" s="T394">0.3.h:A</ta>
            <ta e="T396" id="Seg_10051" s="T395">0.2.h:A</ta>
            <ta e="T399" id="Seg_10052" s="T398">np.h:Th</ta>
            <ta e="T406" id="Seg_10053" s="T405">np:Th</ta>
            <ta e="T413" id="Seg_10054" s="T412">np:Poss</ta>
            <ta e="T415" id="Seg_10055" s="T414">np:Poss</ta>
            <ta e="T417" id="Seg_10056" s="T416">np:L</ta>
            <ta e="T418" id="Seg_10057" s="T417">np:Poss</ta>
            <ta e="T422" id="Seg_10058" s="T420">np:L</ta>
            <ta e="T423" id="Seg_10059" s="T422">np:Poss</ta>
            <ta e="T432" id="Seg_10060" s="T431">adv:Time</ta>
            <ta e="T433" id="Seg_10061" s="T432">np:Th</ta>
            <ta e="T434" id="Seg_10062" s="T433">0.3.h:A</ta>
            <ta e="T437" id="Seg_10063" s="T436">0.3.h:A</ta>
            <ta e="T439" id="Seg_10064" s="T438">np:P</ta>
            <ta e="T441" id="Seg_10065" s="T440">adv:Time</ta>
            <ta e="T442" id="Seg_10066" s="T441">0.3.h:E</ta>
            <ta e="T444" id="Seg_10067" s="T443">np.h:A</ta>
            <ta e="T457" id="Seg_10068" s="T456">np.h:A</ta>
            <ta e="T462" id="Seg_10069" s="T461">np:G</ta>
            <ta e="T463" id="Seg_10070" s="T462">adv:Time</ta>
            <ta e="T464" id="Seg_10071" s="T463">np:G</ta>
            <ta e="T465" id="Seg_10072" s="T464">0.3.h:A</ta>
            <ta e="T466" id="Seg_10073" s="T465">pro.h:A</ta>
            <ta e="T468" id="Seg_10074" s="T467">pro.h:A</ta>
            <ta e="T470" id="Seg_10075" s="T469">np:P</ta>
            <ta e="T473" id="Seg_10076" s="T472">pro.h:R</ta>
            <ta e="T474" id="Seg_10077" s="T473">0.3.h:A</ta>
            <ta e="T476" id="Seg_10078" s="T475">0.2.h:A</ta>
            <ta e="T479" id="Seg_10079" s="T478">np.h:A</ta>
            <ta e="T481" id="Seg_10080" s="T480">pro.h:Poss</ta>
            <ta e="T482" id="Seg_10081" s="T481">np.h:A</ta>
            <ta e="T487" id="Seg_10082" s="T486">0.3.h:A</ta>
            <ta e="T488" id="Seg_10083" s="T487">np:G</ta>
            <ta e="T490" id="Seg_10084" s="T489">pro.h:Poss</ta>
            <ta e="T491" id="Seg_10085" s="T490">np.h:A</ta>
            <ta e="T493" id="Seg_10086" s="T492">pro.h:Th</ta>
            <ta e="T499" id="Seg_10087" s="T498">pro.h:A</ta>
            <ta e="T501" id="Seg_10088" s="T500">np:Ins</ta>
            <ta e="T502" id="Seg_10089" s="T501">pro.h:E</ta>
            <ta e="T505" id="Seg_10090" s="T504">np:P</ta>
            <ta e="T507" id="Seg_10091" s="T506">0.3.h:A</ta>
            <ta e="T512" id="Seg_10092" s="T511">np.h:E</ta>
            <ta e="T514" id="Seg_10093" s="T513">adv:Time</ta>
            <ta e="T515" id="Seg_10094" s="T514">0.3.h:A</ta>
            <ta e="T516" id="Seg_10095" s="T515">np:G</ta>
            <ta e="T517" id="Seg_10096" s="T516">pro.h:A</ta>
            <ta e="T519" id="Seg_10097" s="T518">pro.h:P</ta>
            <ta e="T522" id="Seg_10098" s="T521">np:G</ta>
            <ta e="T524" id="Seg_10099" s="T523">pro.h:A</ta>
            <ta e="T528" id="Seg_10100" s="T527">np:Ins</ta>
            <ta e="T532" id="Seg_10101" s="T531">adv:Time</ta>
            <ta e="T537" id="Seg_10102" s="T536">pro.h:A</ta>
            <ta e="T540" id="Seg_10103" s="T539">np:G</ta>
            <ta e="T541" id="Seg_10104" s="T540">0.3.h:A</ta>
            <ta e="T543" id="Seg_10105" s="T542">adv:Time</ta>
            <ta e="T544" id="Seg_10106" s="T543">pro.h:E</ta>
            <ta e="T547" id="Seg_10107" s="T546">np:G</ta>
            <ta e="T548" id="Seg_10108" s="T547">0.3.h:A</ta>
            <ta e="T551" id="Seg_10109" s="T550">np.h:A</ta>
            <ta e="T552" id="Seg_10110" s="T551">np:G</ta>
            <ta e="T554" id="Seg_10111" s="T553">np.h:A</ta>
            <ta e="T557" id="Seg_10112" s="T556">np:Th</ta>
            <ta e="T558" id="Seg_10113" s="T557">0.3.h:A</ta>
            <ta e="T559" id="Seg_10114" s="T558">np:Th</ta>
            <ta e="T560" id="Seg_10115" s="T559">0.3.h:A</ta>
            <ta e="T561" id="Seg_10116" s="T560">np:Th</ta>
            <ta e="T564" id="Seg_10117" s="T563">np.h:A</ta>
            <ta e="T567" id="Seg_10118" s="T566">np:P</ta>
            <ta e="T570" id="Seg_10119" s="T569">0.3.h:A</ta>
            <ta e="T571" id="Seg_10120" s="T570">np:P</ta>
            <ta e="T572" id="Seg_10121" s="T571">0.3.h:A</ta>
            <ta e="T573" id="Seg_10122" s="T572">np:P</ta>
            <ta e="T574" id="Seg_10123" s="T573">0.3.h:A</ta>
            <ta e="T575" id="Seg_10124" s="T574">np:L</ta>
            <ta e="T576" id="Seg_10125" s="T575">np:P</ta>
            <ta e="T578" id="Seg_10126" s="T577">0.3.h:A</ta>
            <ta e="T579" id="Seg_10127" s="T578">adv:Time</ta>
            <ta e="T581" id="Seg_10128" s="T580">np:L</ta>
            <ta e="T582" id="Seg_10129" s="T581">np:P</ta>
            <ta e="T583" id="Seg_10130" s="T582">0.3.h:A</ta>
            <ta e="T585" id="Seg_10131" s="T584">np:P</ta>
            <ta e="T586" id="Seg_10132" s="T585">0.3.h:A</ta>
            <ta e="T587" id="Seg_10133" s="T586">0.3.h:A</ta>
            <ta e="T589" id="Seg_10134" s="T588">np:Th</ta>
            <ta e="T591" id="Seg_10135" s="T590">pro.h:E</ta>
            <ta e="T592" id="Seg_10136" s="T591">np:L</ta>
            <ta e="T595" id="Seg_10137" s="T594">adv:L</ta>
            <ta e="T596" id="Seg_10138" s="T595">np:Th</ta>
            <ta e="T598" id="Seg_10139" s="T597">adv:L</ta>
            <ta e="T599" id="Seg_10140" s="T598">0.3.h:A</ta>
            <ta e="T600" id="Seg_10141" s="T599">np:P</ta>
            <ta e="T602" id="Seg_10142" s="T601">np:P</ta>
            <ta e="T603" id="Seg_10143" s="T602">0.3.h:A</ta>
            <ta e="T604" id="Seg_10144" s="T603">0.3.h:A</ta>
            <ta e="T606" id="Seg_10145" s="T605">np:L</ta>
            <ta e="T607" id="Seg_10146" s="T606">0.3.h:E</ta>
            <ta e="T609" id="Seg_10147" s="T608">np:P</ta>
            <ta e="T610" id="Seg_10148" s="T609">0.3.h:A</ta>
            <ta e="T613" id="Seg_10149" s="T612">adv:Time</ta>
            <ta e="T614" id="Seg_10150" s="T613">np:P</ta>
            <ta e="T615" id="Seg_10151" s="T614">0.3.h:A</ta>
            <ta e="T616" id="Seg_10152" s="T615">np:Ins</ta>
            <ta e="T618" id="Seg_10153" s="T617">0.3.h:A</ta>
            <ta e="T619" id="Seg_10154" s="T618">pro:G</ta>
            <ta e="T620" id="Seg_10155" s="T619">adv:Time</ta>
            <ta e="T621" id="Seg_10156" s="T620">np:Th</ta>
            <ta e="T622" id="Seg_10157" s="T621">0.3.h:A</ta>
            <ta e="T623" id="Seg_10158" s="T622">adv:Time</ta>
            <ta e="T624" id="Seg_10159" s="T623">np:Th</ta>
            <ta e="T625" id="Seg_10160" s="T624">0.3.h:A</ta>
            <ta e="T629" id="Seg_10161" s="T628">np:P</ta>
            <ta e="T632" id="Seg_10162" s="T631">np:P</ta>
            <ta e="T635" id="Seg_10163" s="T633">adv:Time</ta>
            <ta e="T638" id="Seg_10164" s="T637">np:L</ta>
            <ta e="T639" id="Seg_10165" s="T638">np:Th</ta>
            <ta e="T640" id="Seg_10166" s="T639">0.3.h:A</ta>
            <ta e="T642" id="Seg_10167" s="T641">np:Th</ta>
            <ta e="T644" id="Seg_10168" s="T643">adv:L</ta>
            <ta e="T646" id="Seg_10169" s="T645">np:Th</ta>
            <ta e="T648" id="Seg_10170" s="T647">adv:Time</ta>
            <ta e="T649" id="Seg_10171" s="T648">np:Th</ta>
            <ta e="T650" id="Seg_10172" s="T649">0.3.h:A</ta>
            <ta e="T652" id="Seg_10173" s="T651">np:G</ta>
            <ta e="T653" id="Seg_10174" s="T652">pro.h:Th</ta>
            <ta e="T659" id="Seg_10175" s="T658">0.3:P</ta>
            <ta e="T662" id="Seg_10176" s="T661">np:Ins</ta>
            <ta e="T663" id="Seg_10177" s="T662">0.3.h:A</ta>
            <ta e="T666" id="Seg_10178" s="T665">0.3:P</ta>
            <ta e="T669" id="Seg_10179" s="T668">0.3.h:A</ta>
            <ta e="T671" id="Seg_10180" s="T670">0.3.h:A</ta>
            <ta e="T674" id="Seg_10181" s="T673">0.3:Th</ta>
            <ta e="T678" id="Seg_10182" s="T677">np.h:Th</ta>
            <ta e="T683" id="Seg_10183" s="T682">np:Th</ta>
            <ta e="T685" id="Seg_10184" s="T684">pro.h:E</ta>
            <ta e="T686" id="Seg_10185" s="T685">adv:L</ta>
            <ta e="T690" id="Seg_10186" s="T689">np:Th</ta>
            <ta e="T692" id="Seg_10187" s="T691">adv:Time</ta>
            <ta e="T693" id="Seg_10188" s="T692">pro.h:A</ta>
            <ta e="T695" id="Seg_10189" s="T694">np:G</ta>
            <ta e="T697" id="Seg_10190" s="T696">np:G</ta>
            <ta e="T699" id="Seg_10191" s="T698">np:Ins</ta>
            <ta e="T700" id="Seg_10192" s="T699">0.3.h:A</ta>
            <ta e="T701" id="Seg_10193" s="T700">np:G</ta>
            <ta e="T702" id="Seg_10194" s="T701">0.3.h:A</ta>
            <ta e="T703" id="Seg_10195" s="T702">adv:L</ta>
            <ta e="T705" id="Seg_10196" s="T704">np:Th</ta>
            <ta e="T706" id="Seg_10197" s="T705">np:P</ta>
            <ta e="T707" id="Seg_10198" s="T706">0.3.h:A</ta>
            <ta e="T709" id="Seg_10199" s="T708">adv:Time</ta>
            <ta e="T710" id="Seg_10200" s="T709">pro.h:A</ta>
            <ta e="T714" id="Seg_10201" s="T713">np.h:Th</ta>
            <ta e="T715" id="Seg_10202" s="T714">0.3.h:A</ta>
            <ta e="T716" id="Seg_10203" s="T715">np:Ins</ta>
            <ta e="T717" id="Seg_10204" s="T716">0.3.h:A</ta>
            <ta e="T720" id="Seg_10205" s="T719">np:Com</ta>
            <ta e="T722" id="Seg_10206" s="T721">adv:Time</ta>
            <ta e="T723" id="Seg_10207" s="T722">pro.h:A</ta>
            <ta e="T724" id="Seg_10208" s="T723">np:L</ta>
            <ta e="T725" id="Seg_10209" s="T724">np:Th</ta>
            <ta e="T727" id="Seg_10210" s="T726">0.3.h:A</ta>
            <ta e="T728" id="Seg_10211" s="T727">np:Th</ta>
            <ta e="T729" id="Seg_10212" s="T728">0.3.h:A</ta>
            <ta e="T730" id="Seg_10213" s="T729">0.3.h:A</ta>
            <ta e="T732" id="Seg_10214" s="T731">adv:Time</ta>
            <ta e="T733" id="Seg_10215" s="T732">pro.h:A</ta>
            <ta e="T734" id="Seg_10216" s="T733">np:Th</ta>
            <ta e="T736" id="Seg_10217" s="T735">0.3.h:A</ta>
            <ta e="T738" id="Seg_10218" s="T737">0.3.h:A</ta>
            <ta e="T740" id="Seg_10219" s="T739">adv:Time</ta>
            <ta e="T741" id="Seg_10220" s="T740">adv:L</ta>
            <ta e="T743" id="Seg_10221" s="T742">adv:L</ta>
            <ta e="T744" id="Seg_10222" s="T743">0.3.h:A</ta>
            <ta e="T747" id="Seg_10223" s="T746">np:Th</ta>
            <ta e="T748" id="Seg_10224" s="T747">0.3.h:A</ta>
            <ta e="T749" id="Seg_10225" s="T748">adv:Time</ta>
            <ta e="T750" id="Seg_10226" s="T749">0.3.h:A</ta>
            <ta e="T751" id="Seg_10227" s="T750">pro:P</ta>
            <ta e="T754" id="Seg_10228" s="T753">np:Ins</ta>
            <ta e="T755" id="Seg_10229" s="T754">0.3.h:A</ta>
            <ta e="T756" id="Seg_10230" s="T755">np:G</ta>
            <ta e="T757" id="Seg_10231" s="T756">0.3.h:A</ta>
            <ta e="T759" id="Seg_10232" s="T758">pro.h:A</ta>
            <ta e="T760" id="Seg_10233" s="T759">adv:Time</ta>
            <ta e="T761" id="Seg_10234" s="T760">np:P</ta>
            <ta e="T764" id="Seg_10235" s="T763">0.3.h:A</ta>
            <ta e="T767" id="Seg_10236" s="T766">np:P</ta>
            <ta e="T769" id="Seg_10237" s="T767">0.3.h:A</ta>
            <ta e="T772" id="Seg_10238" s="T771">adv:Time</ta>
            <ta e="T773" id="Seg_10239" s="T772">np:Ins</ta>
            <ta e="T774" id="Seg_10240" s="T773">0.3.h:A</ta>
            <ta e="T775" id="Seg_10241" s="T774">np:P</ta>
            <ta e="T777" id="Seg_10242" s="T776">adv:L</ta>
            <ta e="T778" id="Seg_10243" s="T777">0.3.h:A</ta>
            <ta e="T779" id="Seg_10244" s="T778">np:Th</ta>
            <ta e="T781" id="Seg_10245" s="T780">np:Th</ta>
            <ta e="T783" id="Seg_10246" s="T782">np:Ins</ta>
            <ta e="T784" id="Seg_10247" s="T783">0.3.h:A</ta>
            <ta e="T785" id="Seg_10248" s="T784">np:G</ta>
            <ta e="T786" id="Seg_10249" s="T785">adv:L</ta>
            <ta e="T787" id="Seg_10250" s="T786">pro:L</ta>
            <ta e="T788" id="Seg_10251" s="T787">0.3.h:E</ta>
            <ta e="T793" id="Seg_10252" s="T792">pro.h:Poss</ta>
            <ta e="T796" id="Seg_10253" s="T795">np:Th</ta>
            <ta e="T801" id="Seg_10254" s="T800">np:A</ta>
            <ta e="T802" id="Seg_10255" s="T801">np:Th</ta>
            <ta e="T803" id="Seg_10256" s="T802">0.3.h:A</ta>
            <ta e="T804" id="Seg_10257" s="T803">adv:Time</ta>
            <ta e="T805" id="Seg_10258" s="T804">np:G</ta>
            <ta e="T806" id="Seg_10259" s="T805">0.3.h:A</ta>
            <ta e="T810" id="Seg_10260" s="T809">adv:Time</ta>
            <ta e="T811" id="Seg_10261" s="T810">0.3.h:A</ta>
            <ta e="T812" id="Seg_10262" s="T811">np:Th</ta>
            <ta e="T814" id="Seg_10263" s="T813">adv:Time</ta>
            <ta e="T815" id="Seg_10264" s="T814">0.3.h:A</ta>
            <ta e="T816" id="Seg_10265" s="T815">np:G</ta>
            <ta e="T819" id="Seg_10266" s="T818">np:Th</ta>
            <ta e="T820" id="Seg_10267" s="T819">0.3.h:A</ta>
            <ta e="T823" id="Seg_10268" s="T822">pro.h:Th</ta>
            <ta e="T825" id="Seg_10269" s="T824">np:G</ta>
            <ta e="T827" id="Seg_10270" s="T826">0.3.h:A</ta>
            <ta e="T828" id="Seg_10271" s="T827">pro.h:A</ta>
            <ta e="T831" id="Seg_10272" s="T830">np:Th</ta>
            <ta e="T834" id="Seg_10273" s="T833">np:Th</ta>
            <ta e="T835" id="Seg_10274" s="T834">0.3.h:A</ta>
            <ta e="T837" id="Seg_10275" s="T836">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_10276" s="T2">v:pred 0.2.h:S</ta>
            <ta e="T9" id="Seg_10277" s="T8">v:pred 0.1.h:S</ta>
            <ta e="T12" id="Seg_10278" s="T11">pro:O</ta>
            <ta e="T13" id="Seg_10279" s="T12">v:pred 0.2.h:S</ta>
            <ta e="T16" id="Seg_10280" s="T15">v:pred 0.1.h:S</ta>
            <ta e="T18" id="Seg_10281" s="T17">v:pred 0.1.h:S</ta>
            <ta e="T19" id="Seg_10282" s="T18">v:pred 0.1.h:S</ta>
            <ta e="T23" id="Seg_10283" s="T22">ptcl.neg</ta>
            <ta e="T24" id="Seg_10284" s="T23">v:pred 0.1.h:S</ta>
            <ta e="T30" id="Seg_10285" s="T29">v:pred 0.1.h:S</ta>
            <ta e="T35" id="Seg_10286" s="T34">v:pred 0.1.h:S</ta>
            <ta e="T37" id="Seg_10287" s="T36">v:pred 0.1.h:S</ta>
            <ta e="T38" id="Seg_10288" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_10289" s="T38">pro.h:O</ta>
            <ta e="T41" id="Seg_10290" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_10291" s="T41">np:O</ta>
            <ta e="T43" id="Seg_10292" s="T42">v:pred 0.3.h:S</ta>
            <ta e="T47" id="Seg_10293" s="T46">v:pred 0.1.h:S</ta>
            <ta e="T48" id="Seg_10294" s="T47">np:S</ta>
            <ta e="T49" id="Seg_10295" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_10296" s="T49">np.h:S</ta>
            <ta e="T52" id="Seg_10297" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_10298" s="T53">np:O</ta>
            <ta e="T55" id="Seg_10299" s="T54">pro:S</ta>
            <ta e="T56" id="Seg_10300" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_10301" s="T56">ptcl.neg</ta>
            <ta e="T58" id="Seg_10302" s="T57">v:pred 0.3:S</ta>
            <ta e="T63" id="Seg_10303" s="T62">adj:pred</ta>
            <ta e="T64" id="Seg_10304" s="T63">np:O</ta>
            <ta e="T65" id="Seg_10305" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_10306" s="T68">adj:pred</ta>
            <ta e="T71" id="Seg_10307" s="T70">np:S</ta>
            <ta e="T73" id="Seg_10308" s="T72">v:pred</ta>
            <ta e="T77" id="Seg_10309" s="T76">np:S</ta>
            <ta e="T78" id="Seg_10310" s="T77">v:pred</ta>
            <ta e="T81" id="Seg_10311" s="T80">n:pred</ta>
            <ta e="T87" id="Seg_10312" s="T86">np.h:S</ta>
            <ta e="T88" id="Seg_10313" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_10314" s="T89">v:pred 0.1.h:S</ta>
            <ta e="T92" id="Seg_10315" s="T91">pro.h:S</ta>
            <ta e="T95" id="Seg_10316" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_10317" s="T95">pro.h:S</ta>
            <ta e="T99" id="Seg_10318" s="T98">v:pred</ta>
            <ta e="T101" id="Seg_10319" s="T100">v:pred 0.2.h:S</ta>
            <ta e="T103" id="Seg_10320" s="T102">v:pred 0.2.h:S</ta>
            <ta e="T107" id="Seg_10321" s="T106">v:pred 0.1.h:S</ta>
            <ta e="T108" id="Seg_10322" s="T107">s:purp</ta>
            <ta e="T109" id="Seg_10323" s="T108">pro.h:O</ta>
            <ta e="T111" id="Seg_10324" s="T110">ptcl.neg</ta>
            <ta e="T112" id="Seg_10325" s="T111">v:pred 0.1.h:S</ta>
            <ta e="T113" id="Seg_10326" s="T112">np:O</ta>
            <ta e="T114" id="Seg_10327" s="T113">v:pred</ta>
            <ta e="T116" id="Seg_10328" s="T115">np.h:S</ta>
            <ta e="T118" id="Seg_10329" s="T117">v:pred 0.1.h:S</ta>
            <ta e="T121" id="Seg_10330" s="T120">ptcl:pred</ta>
            <ta e="T122" id="Seg_10331" s="T121">v:pred 0.2.h:S</ta>
            <ta e="T124" id="Seg_10332" s="T123">pro.h:S</ta>
            <ta e="T125" id="Seg_10333" s="T124">v:pred</ta>
            <ta e="T126" id="Seg_10334" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_10335" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_10336" s="T127">s:purp</ta>
            <ta e="T129" id="Seg_10337" s="T128">v:pred 0.1.h:S</ta>
            <ta e="T131" id="Seg_10338" s="T130">np:S</ta>
            <ta e="T132" id="Seg_10339" s="T131">v:pred</ta>
            <ta e="T133" id="Seg_10340" s="T132">ptcl.neg</ta>
            <ta e="T134" id="Seg_10341" s="T133">v:pred 0.3:S</ta>
            <ta e="T136" id="Seg_10342" s="T135">np.h:S</ta>
            <ta e="T138" id="Seg_10343" s="T137">v:pred</ta>
            <ta e="T145" id="Seg_10344" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_10345" s="T145">v:pred</ta>
            <ta e="T147" id="Seg_10346" s="T146">s:purp</ta>
            <ta e="T150" id="Seg_10347" s="T149">v:pred 0.2.h:S</ta>
            <ta e="T152" id="Seg_10348" s="T151">v:pred 0.2.h:S</ta>
            <ta e="T154" id="Seg_10349" s="T153">v:pred 0.2.h:S</ta>
            <ta e="T156" id="Seg_10350" s="T155">np.h:S</ta>
            <ta e="T158" id="Seg_10351" s="T157">v:pred</ta>
            <ta e="T160" id="Seg_10352" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T162" id="Seg_10353" s="T161">ptcl:pred</ta>
            <ta e="T163" id="Seg_10354" s="T162">np:O</ta>
            <ta e="T166" id="Seg_10355" s="T165">np:O</ta>
            <ta e="T167" id="Seg_10356" s="T166">v:pred</ta>
            <ta e="T169" id="Seg_10357" s="T168">pro:O</ta>
            <ta e="T170" id="Seg_10358" s="T169">s:purp</ta>
            <ta e="T171" id="Seg_10359" s="T170">v:pred 0.2.h:S</ta>
            <ta e="T172" id="Seg_10360" s="T171">s:purp</ta>
            <ta e="T173" id="Seg_10361" s="T172">v:pred 0.2.h:S</ta>
            <ta e="T175" id="Seg_10362" s="T174">ptcl.neg</ta>
            <ta e="T176" id="Seg_10363" s="T175">v:pred 0.3.h:S</ta>
            <ta e="T179" id="Seg_10364" s="T178">ptcl.neg</ta>
            <ta e="T180" id="Seg_10365" s="T179">v:pred 0.3.h:S</ta>
            <ta e="T181" id="Seg_10366" s="T180">v:pred 0.3.h:S</ta>
            <ta e="T182" id="Seg_10367" s="T181">adj:pred</ta>
            <ta e="T185" id="Seg_10368" s="T184">np:O</ta>
            <ta e="T187" id="Seg_10369" s="T186">v:pred 0.1.h:S</ta>
            <ta e="T189" id="Seg_10370" s="T188">s:purp</ta>
            <ta e="T191" id="Seg_10371" s="T190">np:S</ta>
            <ta e="T192" id="Seg_10372" s="T191">v:pred</ta>
            <ta e="T194" id="Seg_10373" s="T193">np:S</ta>
            <ta e="T195" id="Seg_10374" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_10375" s="T195">np:S</ta>
            <ta e="T197" id="Seg_10376" s="T196">v:pred</ta>
            <ta e="T198" id="Seg_10377" s="T197">np:S</ta>
            <ta e="T199" id="Seg_10378" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_10379" s="T199">np:O</ta>
            <ta e="T202" id="Seg_10380" s="T201">v:pred 0.1.h:S</ta>
            <ta e="T203" id="Seg_10381" s="T202">v:pred 0.1.h:S</ta>
            <ta e="T208" id="Seg_10382" s="T207">np:S</ta>
            <ta e="T210" id="Seg_10383" s="T209">v:pred</ta>
            <ta e="T211" id="Seg_10384" s="T210">np:O</ta>
            <ta e="T212" id="Seg_10385" s="T211">v:pred 0.3.h:S</ta>
            <ta e="T214" id="Seg_10386" s="T213">v:pred 0.3.h:S</ta>
            <ta e="T216" id="Seg_10387" s="T215">np:O</ta>
            <ta e="T218" id="Seg_10388" s="T217">np:O</ta>
            <ta e="T223" id="Seg_10389" s="T222">v:pred 0.3.h:S</ta>
            <ta e="T226" id="Seg_10390" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T229" id="Seg_10391" s="T228">v:pred 0.1.h:S</ta>
            <ta e="T235" id="Seg_10392" s="T234">v:pred 0.1.h:S</ta>
            <ta e="T238" id="Seg_10393" s="T237">np.h:S</ta>
            <ta e="T239" id="Seg_10394" s="T238">v:pred</ta>
            <ta e="T241" id="Seg_10395" s="T240">np:S</ta>
            <ta e="T244" id="Seg_10396" s="T243">v:pred</ta>
            <ta e="T248" id="Seg_10397" s="T247">np.h:S</ta>
            <ta e="T249" id="Seg_10398" s="T248">v:pred</ta>
            <ta e="T251" id="Seg_10399" s="T250">np.h:S</ta>
            <ta e="T253" id="Seg_10400" s="T252">v:pred</ta>
            <ta e="T255" id="Seg_10401" s="T254">s:purp</ta>
            <ta e="T261" id="Seg_10402" s="T260">np.h:S</ta>
            <ta e="T264" id="Seg_10403" s="T263">v:pred</ta>
            <ta e="T269" id="Seg_10404" s="T268">np:S</ta>
            <ta e="T270" id="Seg_10405" s="T269">v:pred</ta>
            <ta e="T273" id="Seg_10406" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_10407" s="T273">pro.h:S</ta>
            <ta e="T275" id="Seg_10408" s="T274">v:pred</ta>
            <ta e="T277" id="Seg_10409" s="T276">v:pred 0.1.h:S</ta>
            <ta e="T279" id="Seg_10410" s="T278">np:S</ta>
            <ta e="T280" id="Seg_10411" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_10412" s="T280">ptcl.neg</ta>
            <ta e="T282" id="Seg_10413" s="T281">v:pred 0.3:S</ta>
            <ta e="T285" id="Seg_10414" s="T284">ptcl:pred</ta>
            <ta e="T290" id="Seg_10415" s="T289">np:S</ta>
            <ta e="T291" id="Seg_10416" s="T290">v:pred</ta>
            <ta e="T293" id="Seg_10417" s="T292">np:O</ta>
            <ta e="T294" id="Seg_10418" s="T293">v:pred 0.1.h:S</ta>
            <ta e="T297" id="Seg_10419" s="T296">v:pred 0.3.h:S</ta>
            <ta e="T298" id="Seg_10420" s="T297">v:pred 0.3.h:S</ta>
            <ta e="T302" id="Seg_10421" s="T301">np.h:S</ta>
            <ta e="T303" id="Seg_10422" s="T302">v:pred</ta>
            <ta e="T306" id="Seg_10423" s="T305">np:O</ta>
            <ta e="T307" id="Seg_10424" s="T306">v:pred 0.3.h:S</ta>
            <ta e="T309" id="Seg_10425" s="T308">v:pred 0.3.h:S</ta>
            <ta e="T310" id="Seg_10426" s="T309">s:purp</ta>
            <ta e="T315" id="Seg_10427" s="T314">pro.h:S</ta>
            <ta e="T316" id="Seg_10428" s="T315">v:pred</ta>
            <ta e="T317" id="Seg_10429" s="T316">pro.h:S</ta>
            <ta e="T319" id="Seg_10430" s="T318">np:O</ta>
            <ta e="T320" id="Seg_10431" s="T319">v:pred</ta>
            <ta e="T321" id="Seg_10432" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_10433" s="T321">v:pred</ta>
            <ta e="T325" id="Seg_10434" s="T324">v:pred 0.3.h:S</ta>
            <ta e="T336" id="Seg_10435" s="T335">pro.h:S</ta>
            <ta e="T341" id="Seg_10436" s="T340">pro.h:S</ta>
            <ta e="T345" id="Seg_10437" s="T344">v:pred</ta>
            <ta e="T347" id="Seg_10438" s="T346">v:pred 0.2.h:S</ta>
            <ta e="T351" id="Seg_10439" s="T350">np.h:S</ta>
            <ta e="T353" id="Seg_10440" s="T352">v:pred</ta>
            <ta e="T357" id="Seg_10441" s="T356">np:O</ta>
            <ta e="T358" id="Seg_10442" s="T357">v:pred 0.3.h:S</ta>
            <ta e="T362" id="Seg_10443" s="T361">v:pred 0.3.h:S</ta>
            <ta e="T363" id="Seg_10444" s="T362">0.3:S</ta>
            <ta e="T365" id="Seg_10445" s="T364">adj:pred</ta>
            <ta e="T366" id="Seg_10446" s="T365">cop</ta>
            <ta e="T367" id="Seg_10447" s="T366">v:pred 0.2.h:S</ta>
            <ta e="T369" id="Seg_10448" s="T368">s:purp</ta>
            <ta e="T370" id="Seg_10449" s="T369">pro.h:S</ta>
            <ta e="T372" id="Seg_10450" s="T371">np:O</ta>
            <ta e="T373" id="Seg_10451" s="T372">v:pred</ta>
            <ta e="T374" id="Seg_10452" s="T373">pro:S</ta>
            <ta e="T376" id="Seg_10453" s="T375">adj:pred</ta>
            <ta e="T380" id="Seg_10454" s="T379">v:pred 0.3.h:S</ta>
            <ta e="T385" id="Seg_10455" s="T384">v:pred 0.3.h:S</ta>
            <ta e="T387" id="Seg_10456" s="T386">np.h:S</ta>
            <ta e="T388" id="Seg_10457" s="T387">v:pred</ta>
            <ta e="T392" id="Seg_10458" s="T391">ptcl.neg</ta>
            <ta e="T393" id="Seg_10459" s="T392">v:pred 0.1.h:S</ta>
            <ta e="T395" id="Seg_10460" s="T394">v:pred 0.3.h:S</ta>
            <ta e="T396" id="Seg_10461" s="T395">v:pred 0.2.h:S</ta>
            <ta e="T399" id="Seg_10462" s="T398">np.h:O</ta>
            <ta e="T406" id="Seg_10463" s="T405">np:S</ta>
            <ta e="T407" id="Seg_10464" s="T406">v:pred</ta>
            <ta e="T427" id="Seg_10465" s="T426">np.h:S</ta>
            <ta e="T433" id="Seg_10466" s="T432">np:O</ta>
            <ta e="T434" id="Seg_10467" s="T433">v:pred 0.3.h:S</ta>
            <ta e="T437" id="Seg_10468" s="T436">v:pred 0.3.h:S</ta>
            <ta e="T439" id="Seg_10469" s="T438">np:O</ta>
            <ta e="T442" id="Seg_10470" s="T441">v:pred 0.3.h:S</ta>
            <ta e="T444" id="Seg_10471" s="T443">np.h:S</ta>
            <ta e="T446" id="Seg_10472" s="T445">v:pred</ta>
            <ta e="T457" id="Seg_10473" s="T456">np.h:S</ta>
            <ta e="T461" id="Seg_10474" s="T460">v:pred</ta>
            <ta e="T465" id="Seg_10475" s="T464">v:pred 0.3.h:S</ta>
            <ta e="T466" id="Seg_10476" s="T465">pro.h:S</ta>
            <ta e="T467" id="Seg_10477" s="T466">v:pred</ta>
            <ta e="T468" id="Seg_10478" s="T467">pro.h:S</ta>
            <ta e="T470" id="Seg_10479" s="T469">np:O</ta>
            <ta e="T471" id="Seg_10480" s="T470">v:pred</ta>
            <ta e="T474" id="Seg_10481" s="T473">v:pred 0.3.h:S</ta>
            <ta e="T475" id="Seg_10482" s="T474">ptcl.neg</ta>
            <ta e="T476" id="Seg_10483" s="T475">v:pred 0.2.h:S</ta>
            <ta e="T479" id="Seg_10484" s="T478">np.h:S</ta>
            <ta e="T482" id="Seg_10485" s="T481">np.h:S</ta>
            <ta e="T484" id="Seg_10486" s="T483">v:pred</ta>
            <ta e="T487" id="Seg_10487" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T491" id="Seg_10488" s="T490">np.h:S</ta>
            <ta e="T493" id="Seg_10489" s="T492">pro.h:O</ta>
            <ta e="T495" id="Seg_10490" s="T494">v:pred</ta>
            <ta e="T499" id="Seg_10491" s="T498">pro.h:S</ta>
            <ta e="T502" id="Seg_10492" s="T501">pro.h:O</ta>
            <ta e="T503" id="Seg_10493" s="T502">v:pred</ta>
            <ta e="T505" id="Seg_10494" s="T504">np:O</ta>
            <ta e="T507" id="Seg_10495" s="T506">v:pred 0.3.h:S</ta>
            <ta e="T512" id="Seg_10496" s="T511">np.h:S</ta>
            <ta e="T513" id="Seg_10497" s="T512">v:pred</ta>
            <ta e="T515" id="Seg_10498" s="T514">v:pred 0.3.h:S</ta>
            <ta e="T517" id="Seg_10499" s="T516">pro.h:S</ta>
            <ta e="T519" id="Seg_10500" s="T518">pro.h:O</ta>
            <ta e="T520" id="Seg_10501" s="T519">v:pred</ta>
            <ta e="T523" id="Seg_10502" s="T522">v:pred</ta>
            <ta e="T524" id="Seg_10503" s="T523">pro.h:S</ta>
            <ta e="T533" id="Seg_10504" s="T532">pro.h:S</ta>
            <ta e="T535" id="Seg_10505" s="T534">v:pred</ta>
            <ta e="T537" id="Seg_10506" s="T536">pro.h:S</ta>
            <ta e="T539" id="Seg_10507" s="T538">v:pred</ta>
            <ta e="T541" id="Seg_10508" s="T540">v:pred 0.3.h:S</ta>
            <ta e="T544" id="Seg_10509" s="T543">pro.h:S</ta>
            <ta e="T545" id="Seg_10510" s="T544">ptcl.neg</ta>
            <ta e="T546" id="Seg_10511" s="T545">v:pred</ta>
            <ta e="T548" id="Seg_10512" s="T547">v:pred 0.3.h:S</ta>
            <ta e="T551" id="Seg_10513" s="T550">np.h:S</ta>
            <ta e="T848" id="Seg_10514" s="T552">conv:pred</ta>
            <ta e="T553" id="Seg_10515" s="T848">v:pred</ta>
            <ta e="T554" id="Seg_10516" s="T553">np.h:S</ta>
            <ta e="T556" id="Seg_10517" s="T555">v:pred</ta>
            <ta e="T557" id="Seg_10518" s="T556">np:O</ta>
            <ta e="T558" id="Seg_10519" s="T557">v:pred 0.3.h:S</ta>
            <ta e="T559" id="Seg_10520" s="T558">np:O</ta>
            <ta e="T560" id="Seg_10521" s="T559">v:pred 0.3.h:S</ta>
            <ta e="T561" id="Seg_10522" s="T560">np:S</ta>
            <ta e="T562" id="Seg_10523" s="T561">v:pred</ta>
            <ta e="T564" id="Seg_10524" s="T563">np.h:S</ta>
            <ta e="T567" id="Seg_10525" s="T566">np:O</ta>
            <ta e="T568" id="Seg_10526" s="T567">v:pred</ta>
            <ta e="T570" id="Seg_10527" s="T569">v:pred 0.3.h:S</ta>
            <ta e="T571" id="Seg_10528" s="T570">np:O</ta>
            <ta e="T572" id="Seg_10529" s="T571">v:pred 0.3.h:S</ta>
            <ta e="T573" id="Seg_10530" s="T572">np:O</ta>
            <ta e="T574" id="Seg_10531" s="T573">v:pred 0.3.h:S</ta>
            <ta e="T576" id="Seg_10532" s="T575">np:O</ta>
            <ta e="T578" id="Seg_10533" s="T577">v:pred 0.3.h:S</ta>
            <ta e="T582" id="Seg_10534" s="T581">np:O</ta>
            <ta e="T583" id="Seg_10535" s="T582">v:pred 0.3.h:S</ta>
            <ta e="T585" id="Seg_10536" s="T584">np:O</ta>
            <ta e="T586" id="Seg_10537" s="T585">v:pred 0.3.h:S</ta>
            <ta e="T587" id="Seg_10538" s="T586">v:pred 0.3.h:S</ta>
            <ta e="T589" id="Seg_10539" s="T588">np:S</ta>
            <ta e="T590" id="Seg_10540" s="T589">v:pred</ta>
            <ta e="T591" id="Seg_10541" s="T590">pro.h:S</ta>
            <ta e="T593" id="Seg_10542" s="T592">ptcl.neg</ta>
            <ta e="T594" id="Seg_10543" s="T593">v:pred</ta>
            <ta e="T596" id="Seg_10544" s="T595">np:S</ta>
            <ta e="T597" id="Seg_10545" s="T596">v:pred</ta>
            <ta e="T599" id="Seg_10546" s="T598">v:pred 0.3.h:S</ta>
            <ta e="T600" id="Seg_10547" s="T599">np:O</ta>
            <ta e="T602" id="Seg_10548" s="T601">np:O</ta>
            <ta e="T603" id="Seg_10549" s="T602">v:pred 0.3.h:S</ta>
            <ta e="T604" id="Seg_10550" s="T603">v:pred 0.3.h:S</ta>
            <ta e="T607" id="Seg_10551" s="T606">v:pred 0.3.h:S</ta>
            <ta e="T609" id="Seg_10552" s="T608">np:O</ta>
            <ta e="T610" id="Seg_10553" s="T609">v:pred 0.3.h:S</ta>
            <ta e="T614" id="Seg_10554" s="T613">np:O</ta>
            <ta e="T615" id="Seg_10555" s="T614">v:pred 0.3.h:S</ta>
            <ta e="T618" id="Seg_10556" s="T617">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_10557" s="T620">np:O</ta>
            <ta e="T622" id="Seg_10558" s="T621">v:pred 0.3.h:S</ta>
            <ta e="T624" id="Seg_10559" s="T623">np:O</ta>
            <ta e="T625" id="Seg_10560" s="T624">v:pred 0.3.h:S</ta>
            <ta e="T628" id="Seg_10561" s="T627">v:pred</ta>
            <ta e="T629" id="Seg_10562" s="T628">np:O</ta>
            <ta e="T632" id="Seg_10563" s="T631">np:O</ta>
            <ta e="T633" id="Seg_10564" s="T632">v:pred</ta>
            <ta e="T640" id="Seg_10565" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T642" id="Seg_10566" s="T641">np:S</ta>
            <ta e="T643" id="Seg_10567" s="T642">v:pred</ta>
            <ta e="T646" id="Seg_10568" s="T645">np:S</ta>
            <ta e="T647" id="Seg_10569" s="T646">v:pred</ta>
            <ta e="T649" id="Seg_10570" s="T648">np:O</ta>
            <ta e="T650" id="Seg_10571" s="T649">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_10572" s="T652">pro.h:S</ta>
            <ta e="T654" id="Seg_10573" s="T653">ptcl.neg</ta>
            <ta e="T656" id="Seg_10574" s="T655">v:pred</ta>
            <ta e="T658" id="Seg_10575" s="T657">adj:pred</ta>
            <ta e="T659" id="Seg_10576" s="T658">cop 0.3:S</ta>
            <ta e="T663" id="Seg_10577" s="T662">v:pred 0.3.h:S</ta>
            <ta e="T665" id="Seg_10578" s="T664">adj:pred</ta>
            <ta e="T666" id="Seg_10579" s="T665">cop 0.3:S</ta>
            <ta e="T669" id="Seg_10580" s="T668">v:pred 0.3.h:S</ta>
            <ta e="T671" id="Seg_10581" s="T670">v:pred 0.3.h:S</ta>
            <ta e="T673" id="Seg_10582" s="T672">adj:pred</ta>
            <ta e="T674" id="Seg_10583" s="T673">cop 0.3:S</ta>
            <ta e="T678" id="Seg_10584" s="T677">np.h:S</ta>
            <ta e="T679" id="Seg_10585" s="T678">v:pred</ta>
            <ta e="T683" id="Seg_10586" s="T682">np:S</ta>
            <ta e="T684" id="Seg_10587" s="T683">v:pred</ta>
            <ta e="T685" id="Seg_10588" s="T684">pro.h:S</ta>
            <ta e="T687" id="Seg_10589" s="T686">v:pred</ta>
            <ta e="T690" id="Seg_10590" s="T689">np:S</ta>
            <ta e="T849" id="Seg_10591" s="T690">conv:pred</ta>
            <ta e="T691" id="Seg_10592" s="T849">v:pred</ta>
            <ta e="T693" id="Seg_10593" s="T692">pro.h:S</ta>
            <ta e="T694" id="Seg_10594" s="T693">v:pred</ta>
            <ta e="T700" id="Seg_10595" s="T699">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_10596" s="T701">v:pred 0.3.h:S</ta>
            <ta e="T705" id="Seg_10597" s="T704">np:S</ta>
            <ta e="T706" id="Seg_10598" s="T705">np:O</ta>
            <ta e="T707" id="Seg_10599" s="T706">v:pred 0.3.h:S</ta>
            <ta e="T710" id="Seg_10600" s="T709">pro.h:S</ta>
            <ta e="T711" id="Seg_10601" s="T710">v:pred</ta>
            <ta e="T714" id="Seg_10602" s="T713">np.h:O</ta>
            <ta e="T715" id="Seg_10603" s="T714">v:pred 0.3.h:S</ta>
            <ta e="T717" id="Seg_10604" s="T716">v:pred 0.3.h:S</ta>
            <ta e="T723" id="Seg_10605" s="T722">pro.h:S</ta>
            <ta e="T725" id="Seg_10606" s="T724">np:O</ta>
            <ta e="T726" id="Seg_10607" s="T725">v:pred</ta>
            <ta e="T727" id="Seg_10608" s="T726">v:pred 0.3.h:S</ta>
            <ta e="T728" id="Seg_10609" s="T727">np:O</ta>
            <ta e="T729" id="Seg_10610" s="T728">v:pred 0.3.h:S</ta>
            <ta e="T730" id="Seg_10611" s="T729">v:pred 0.3.h:S</ta>
            <ta e="T733" id="Seg_10612" s="T732">pro.h:S</ta>
            <ta e="T734" id="Seg_10613" s="T733">np:O</ta>
            <ta e="T735" id="Seg_10614" s="T734">v:pred</ta>
            <ta e="T736" id="Seg_10615" s="T735">v:pred 0.3.h:S</ta>
            <ta e="T738" id="Seg_10616" s="T737">v:pred 0.3.h:S</ta>
            <ta e="T744" id="Seg_10617" s="T743">v:pred 0.3.h:S</ta>
            <ta e="T747" id="Seg_10618" s="T746">np:O</ta>
            <ta e="T748" id="Seg_10619" s="T747">v:pred 0.3.h:S</ta>
            <ta e="T750" id="Seg_10620" s="T749">v:pred 0.3.h:S</ta>
            <ta e="T751" id="Seg_10621" s="T750">pro:S</ta>
            <ta e="T753" id="Seg_10622" s="T752">v:pred</ta>
            <ta e="T755" id="Seg_10623" s="T754">v:pred 0.3.h:S</ta>
            <ta e="T757" id="Seg_10624" s="T756">v:pred 0.3.h:S</ta>
            <ta e="T759" id="Seg_10625" s="T758">pro.h:S</ta>
            <ta e="T761" id="Seg_10626" s="T760">np:O</ta>
            <ta e="T762" id="Seg_10627" s="T761">v:pred</ta>
            <ta e="T764" id="Seg_10628" s="T763">v:pred 0.3.h:S</ta>
            <ta e="T767" id="Seg_10629" s="T766">np:O</ta>
            <ta e="T769" id="Seg_10630" s="T767">v:pred 0.3.h:S</ta>
            <ta e="T774" id="Seg_10631" s="T773">v:pred 0.3.h:S</ta>
            <ta e="T775" id="Seg_10632" s="T774">np:O</ta>
            <ta e="T778" id="Seg_10633" s="T777">v:pred 0.3.h:S</ta>
            <ta e="T779" id="Seg_10634" s="T778">np:O</ta>
            <ta e="T781" id="Seg_10635" s="T780">np:O</ta>
            <ta e="T784" id="Seg_10636" s="T783">v:pred 0.3.h:S</ta>
            <ta e="T788" id="Seg_10637" s="T787">v:pred 0.3.h:S</ta>
            <ta e="T790" id="Seg_10638" s="T789">adj:pred</ta>
            <ta e="T791" id="Seg_10639" s="T790">cop 0.3:S</ta>
            <ta e="T796" id="Seg_10640" s="T795">np:S</ta>
            <ta e="T798" id="Seg_10641" s="T797">v:pred</ta>
            <ta e="T800" id="Seg_10642" s="T799">v:pred</ta>
            <ta e="T801" id="Seg_10643" s="T800">np:S</ta>
            <ta e="T802" id="Seg_10644" s="T801">np:O</ta>
            <ta e="T803" id="Seg_10645" s="T802">v:pred 0.3.h:S</ta>
            <ta e="T806" id="Seg_10646" s="T805">v:pred 0.3.h:S</ta>
            <ta e="T808" id="Seg_10647" s="T807">np:S</ta>
            <ta e="T811" id="Seg_10648" s="T810">v:pred 0.3.h:S</ta>
            <ta e="T812" id="Seg_10649" s="T811">np:O</ta>
            <ta e="T813" id="Seg_10650" s="T812">s:purp</ta>
            <ta e="T815" id="Seg_10651" s="T814">v:pred 0.3.h:S</ta>
            <ta e="T819" id="Seg_10652" s="T818">np:O</ta>
            <ta e="T820" id="Seg_10653" s="T819">v:pred 0.3.h:S</ta>
            <ta e="T823" id="Seg_10654" s="T822">pro.h:O</ta>
            <ta e="T826" id="Seg_10655" s="T825">ptcl.neg</ta>
            <ta e="T827" id="Seg_10656" s="T826">v:pred 0.3.h:S</ta>
            <ta e="T828" id="Seg_10657" s="T827">pro.h:S</ta>
            <ta e="T831" id="Seg_10658" s="T830">np:O</ta>
            <ta e="T832" id="Seg_10659" s="T831">v:pred</ta>
            <ta e="T834" id="Seg_10660" s="T833">np:O</ta>
            <ta e="T835" id="Seg_10661" s="T834">v:pred 0.3.h:S</ta>
            <ta e="T837" id="Seg_10662" s="T836">pro.h:S</ta>
            <ta e="T839" id="Seg_10663" s="T838">v:pred</ta>
            <ta e="T840" id="Seg_10664" s="T839">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_10665" s="T850">TAT:cult</ta>
            <ta e="T20" id="Seg_10666" s="T19">TURK:gram(INDEF)</ta>
            <ta e="T40" id="Seg_10667" s="T39">TURK:disc</ta>
            <ta e="T51" id="Seg_10668" s="T50">TURK:disc</ta>
            <ta e="T62" id="Seg_10669" s="T61">TAT:cult</ta>
            <ta e="T64" id="Seg_10670" s="T63">RUS:cult</ta>
            <ta e="T68" id="Seg_10671" s="T67">TURK:disc</ta>
            <ta e="T72" id="Seg_10672" s="T71">TAT:cult</ta>
            <ta e="T74" id="Seg_10673" s="T73">TURK:disc</ta>
            <ta e="T76" id="Seg_10674" s="T75">TURK:cult</ta>
            <ta e="T77" id="Seg_10675" s="T76">TURK:cult</ta>
            <ta e="T79" id="Seg_10676" s="T78">TURK:disc</ta>
            <ta e="T84" id="Seg_10677" s="T83">TAT:cult</ta>
            <ta e="T100" id="Seg_10678" s="T99">RUS:disc</ta>
            <ta e="T102" id="Seg_10679" s="T101">RUS:disc</ta>
            <ta e="T123" id="Seg_10680" s="T122">RUS:gram</ta>
            <ta e="T137" id="Seg_10681" s="T136">TURK:disc</ta>
            <ta e="T139" id="Seg_10682" s="T138">TAT:cult</ta>
            <ta e="T141" id="Seg_10683" s="T140">TAT:cult</ta>
            <ta e="T162" id="Seg_10684" s="T161">RUS:mod</ta>
            <ta e="T166" id="Seg_10685" s="T165">TURK:cult</ta>
            <ta e="T182" id="Seg_10686" s="T181">TURK:core</ta>
            <ta e="T217" id="Seg_10687" s="T216">RUS:gram</ta>
            <ta e="T228" id="Seg_10688" s="T851">TAT:cult</ta>
            <ta e="T245" id="Seg_10689" s="T244">TURK:disc</ta>
            <ta e="T250" id="Seg_10690" s="T249">RUS:gram</ta>
            <ta e="T263" id="Seg_10691" s="T262">TURK:disc</ta>
            <ta e="T266" id="Seg_10692" s="T265">TURK:disc</ta>
            <ta e="T272" id="Seg_10693" s="T271">TURK:disc</ta>
            <ta e="T276" id="Seg_10694" s="T275">TURK:disc</ta>
            <ta e="T285" id="Seg_10695" s="T284">RUS:mod</ta>
            <ta e="T286" id="Seg_10696" s="T285">%TURK:core</ta>
            <ta e="T289" id="Seg_10697" s="T288">TURK:disc</ta>
            <ta e="T292" id="Seg_10698" s="T291">TURK:gram(INDEF)</ta>
            <ta e="T293" id="Seg_10699" s="T292">TURK:cult</ta>
            <ta e="T300" id="Seg_10700" s="T299">RUS:gram</ta>
            <ta e="T304" id="Seg_10701" s="T852">TAT:cult</ta>
            <ta e="T305" id="Seg_10702" s="T304">RUS:gram</ta>
            <ta e="T306" id="Seg_10703" s="T305">TURK:cult</ta>
            <ta e="T319" id="Seg_10704" s="T318">TURK:cult</ta>
            <ta e="T323" id="Seg_10705" s="T322">RUS:gram</ta>
            <ta e="T328" id="Seg_10706" s="T327">RUS:gram</ta>
            <ta e="T330" id="Seg_10707" s="T329">RUS:mod</ta>
            <ta e="T331" id="Seg_10708" s="T330">RUS:gram</ta>
            <ta e="T333" id="Seg_10709" s="T332">RUS:gram</ta>
            <ta e="T340" id="Seg_10710" s="T339">RUS:gram</ta>
            <ta e="T346" id="Seg_10711" s="T345">RUS:gram</ta>
            <ta e="T352" id="Seg_10712" s="T351">RUS:cult</ta>
            <ta e="T355" id="Seg_10713" s="T354">TURK:disc</ta>
            <ta e="T364" id="Seg_10714" s="T363">TURK:disc</ta>
            <ta e="T371" id="Seg_10715" s="T370">TURK:disc</ta>
            <ta e="T382" id="Seg_10716" s="T381">RUS:cult</ta>
            <ta e="T383" id="Seg_10717" s="T382">TURK:disc</ta>
            <ta e="T386" id="Seg_10718" s="T385">TURK:disc</ta>
            <ta e="T389" id="Seg_10719" s="T388">RUS:gram</ta>
            <ta e="T400" id="Seg_10720" s="T399">RUS:cult</ta>
            <ta e="T404" id="Seg_10721" s="T403">RUS:cult</ta>
            <ta e="T405" id="Seg_10722" s="T404">TURK:disc</ta>
            <ta e="T411" id="Seg_10723" s="T410">TURK:disc</ta>
            <ta e="T413" id="Seg_10724" s="T412">RUS:cult</ta>
            <ta e="T427" id="Seg_10725" s="T426">RUS:cult</ta>
            <ta e="T428" id="Seg_10726" s="T427">TURK:disc</ta>
            <ta e="T435" id="Seg_10727" s="T434">RUS:gram</ta>
            <ta e="T436" id="Seg_10728" s="T435">TURK:disc</ta>
            <ta e="T440" id="Seg_10729" s="T439">RUS:gram</ta>
            <ta e="T443" id="Seg_10730" s="T442">RUS:gram</ta>
            <ta e="T464" id="Seg_10731" s="T463">RUS:cult</ta>
            <ta e="T472" id="Seg_10732" s="T471">RUS:gram</ta>
            <ta e="T479" id="Seg_10733" s="T478">RUS:cult</ta>
            <ta e="T480" id="Seg_10734" s="T479">RUS:gram</ta>
            <ta e="T482" id="Seg_10735" s="T481">RUS:cult</ta>
            <ta e="T483" id="Seg_10736" s="T482">TURK:disc</ta>
            <ta e="T489" id="Seg_10737" s="T488">RUS:gram</ta>
            <ta e="T491" id="Seg_10738" s="T490">RUS:cult</ta>
            <ta e="T494" id="Seg_10739" s="T493">TURK:disc</ta>
            <ta e="T498" id="Seg_10740" s="T497">RUS:gram</ta>
            <ta e="T500" id="Seg_10741" s="T499">TURK:disc</ta>
            <ta e="T501" id="Seg_10742" s="T500">TURK:cult</ta>
            <ta e="T506" id="Seg_10743" s="T505">TURK:disc</ta>
            <ta e="T516" id="Seg_10744" s="T515">RUS:cult</ta>
            <ta e="T518" id="Seg_10745" s="T517">TURK:disc</ta>
            <ta e="T521" id="Seg_10746" s="T520">RUS:core</ta>
            <ta e="T527" id="Seg_10747" s="T526">TURK:disc</ta>
            <ta e="T536" id="Seg_10748" s="T535">RUS:gram</ta>
            <ta e="T538" id="Seg_10749" s="T537">TURK:disc</ta>
            <ta e="T540" id="Seg_10750" s="T539">RUS:cult</ta>
            <ta e="T557" id="Seg_10751" s="T556">RUS:core</ta>
            <ta e="T565" id="Seg_10752" s="T564">TURK:disc</ta>
            <ta e="T569" id="Seg_10753" s="T568">RUS:cult</ta>
            <ta e="T573" id="Seg_10754" s="T572">RUS:core</ta>
            <ta e="T589" id="Seg_10755" s="T588">TAT:cult</ta>
            <ta e="T592" id="Seg_10756" s="T591">TAT:cult</ta>
            <ta e="T605" id="Seg_10757" s="T604">RUS:gram</ta>
            <ta e="T606" id="Seg_10758" s="T605">TAT:cult</ta>
            <ta e="T617" id="Seg_10759" s="T616">RUS:gram</ta>
            <ta e="T621" id="Seg_10760" s="T620">TURK:cult</ta>
            <ta e="T630" id="Seg_10761" s="T629">RUS:gram</ta>
            <ta e="T651" id="Seg_10762" s="T650">RUS:gram</ta>
            <ta e="T653" id="Seg_10763" s="T652">TURK:gram(INDEF)</ta>
            <ta e="T660" id="Seg_10764" s="T659">RUS:gram</ta>
            <ta e="T662" id="Seg_10765" s="T661">RUS:core</ta>
            <ta e="T664" id="Seg_10766" s="T663">RUS:gram</ta>
            <ta e="T667" id="Seg_10767" s="T666">RUS:gram</ta>
            <ta e="T688" id="Seg_10768" s="T687">RUS:gram</ta>
            <ta e="T699" id="Seg_10769" s="T698">RUS:core</ta>
            <ta e="T713" id="Seg_10770" s="T712">RUS:gram</ta>
            <ta e="T716" id="Seg_10771" s="T715">RUS:core</ta>
            <ta e="T719" id="Seg_10772" s="T718">RUS:gram</ta>
            <ta e="T737" id="Seg_10773" s="T736">RUS:gram</ta>
            <ta e="T746" id="Seg_10774" s="T745">RUS:gram</ta>
            <ta e="T763" id="Seg_10775" s="T762">RUS:gram</ta>
            <ta e="T765" id="Seg_10776" s="T764">RUS:gram</ta>
            <ta e="T766" id="Seg_10777" s="T765">RUS:mod</ta>
            <ta e="T776" id="Seg_10778" s="T775">RUS:gram</ta>
            <ta e="T780" id="Seg_10779" s="T779">RUS:gram</ta>
            <ta e="T782" id="Seg_10780" s="T781">RUS:gram</ta>
            <ta e="T783" id="Seg_10781" s="T782">RUS:core</ta>
            <ta e="T818" id="Seg_10782" s="T817">RUS:gram</ta>
            <ta e="T825" id="Seg_10783" s="T824">RUS:cult</ta>
            <ta e="T829" id="Seg_10784" s="T828">RUS:cult</ta>
            <ta e="T833" id="Seg_10785" s="T832">RUS:gram</ta>
            <ta e="T838" id="Seg_10786" s="T837">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T699" id="Seg_10787" s="T698">parad:infl</ta>
            <ta e="T713" id="Seg_10788" s="T712">parad:infl</ta>
            <ta e="T783" id="Seg_10789" s="T782">parad:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T220" id="Seg_10790" s="T218">RUS:ext</ta>
            <ta e="T402" id="Seg_10791" s="T401">RUS:ext</ta>
            <ta e="T410" id="Seg_10792" s="T407">RUS:int</ta>
            <ta e="T431" id="Seg_10793" s="T428">RUS:int</ta>
            <ta e="T450" id="Seg_10794" s="T446">RUS:int</ta>
            <ta e="T455" id="Seg_10795" s="T450">RUS:ext</ta>
            <ta e="T478" id="Seg_10796" s="T477">RUS:int</ta>
            <ta e="T486" id="Seg_10797" s="T485">RUS:int</ta>
            <ta e="T509" id="Seg_10798" s="T507">RUS:ext</ta>
            <ta e="T531" id="Seg_10799" s="T529">RUS:int</ta>
            <ta e="T636" id="Seg_10800" s="T635">RUS:ext</ta>
            <ta e="T676" id="Seg_10801" s="T675">RUS:int</ta>
            <ta e="T770" id="Seg_10802" s="T769">RUS:int</ta>
            <ta e="T846" id="Seg_10803" s="T843">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_10804" s="T0">Когда ты к нам приехал(а)/пришел(-ла)?</ta>
            <ta e="T5" id="Seg_10805" s="T3">В праздник.</ta>
            <ta e="T9" id="Seg_10806" s="T5">Я два дня жил(а) в Агинском.</ta>
            <ta e="T13" id="Seg_10807" s="T9">Что ты видел(а)?</ta>
            <ta e="T16" id="Seg_10808" s="T13">Потом я приехал(а) сюда.</ta>
            <ta e="T26" id="Seg_10809" s="T17">Я искала-искала [возможность?], никак не могла приехать сюда.</ta>
            <ta e="T31" id="Seg_10810" s="T26">Потом я приехала сюда (на?) три дня.</ta>
            <ta e="T37" id="Seg_10811" s="T32">Я приехала, пришла к бабушке.</ta>
            <ta e="T43" id="Seg_10812" s="T37">Она меня покормила, дала суп.</ta>
            <ta e="T49" id="Seg_10813" s="T44">Потом я сюда пришла, теленок мычит.</ta>
            <ta e="T58" id="Seg_10814" s="T49">Женщина дала ему картошки, он ест, не мычит.</ta>
            <ta e="T65" id="Seg_10815" s="T59">У этой женщины в доме холодно, она печь затопила.</ta>
            <ta e="T70" id="Seg_10816" s="T66">На улице холодно очень.</ta>
            <ta e="T74" id="Seg_10817" s="T70">Теленок находится в доме.</ta>
            <ta e="T81" id="Seg_10818" s="T75">Корова родила, теленок маленький.</ta>
            <ta e="T88" id="Seg_10819" s="T82">У нас в деревне очень много народу умерло.</ta>
            <ta e="T95" id="Seg_10820" s="T89">Я пошла к женщине, она очень больна.</ta>
            <ta e="T99" id="Seg_10821" s="T95">"Я сегодня умру".</ta>
            <ta e="T101" id="Seg_10822" s="T99">"Ну, умирай".</ta>
            <ta e="T103" id="Seg_10823" s="T101">"Ну, умирай.</ta>
            <ta e="T109" id="Seg_10824" s="T103">Я завтра приду помыть тебя.</ta>
            <ta e="T112" id="Seg_10825" s="T109">Но я не пришла.</ta>
            <ta e="T116" id="Seg_10826" s="T112">Двое мужчин принесли траву.</ta>
            <ta e="T122" id="Seg_10827" s="T116">Потом я пришла: "Почему ты так долго не приходила?"</ta>
            <ta e="T129" id="Seg_10828" s="T122">"А я думала, ты умерла, я пришла, чтобы тебя помыть".</ta>
            <ta e="T134" id="Seg_10829" s="T130">Теленок (спит?), не мычит.</ta>
            <ta e="T142" id="Seg_10830" s="T135">Дети бегают около дома.</ta>
            <ta e="T147" id="Seg_10831" s="T143">Два мальчика пришли послушать.</ta>
            <ta e="T154" id="Seg_10832" s="T149">Не кричите, не шумите, слушайте!</ta>
            <ta e="T158" id="Seg_10833" s="T155">"Когда твоя мать придет?"</ta>
            <ta e="T160" id="Seg_10834" s="T158">"Она скоро придет!"</ta>
            <ta e="T167" id="Seg_10835" s="T161">Надо воду согреть, потом корову напоить.</ta>
            <ta e="T173" id="Seg_10836" s="T168">Что слушать, смотреть пришли?</ta>
            <ta e="T176" id="Seg_10837" s="T174">Они не кричат.</ta>
            <ta e="T180" id="Seg_10838" s="T176">Хорошие мальчики, не кричат.</ta>
            <ta e="T183" id="Seg_10839" s="T180">Стоят, [они] очень хорошие.</ta>
            <ta e="T189" id="Seg_10840" s="T184">Я сегодня дала тебе орехи лущить.</ta>
            <ta e="T200" id="Seg_10841" s="T190">Глаза чтобы смотреть, уши чтобы слушать, рот чтобы есть, зубы чтобы кусать хлеб.</ta>
            <ta e="T206" id="Seg_10842" s="T201">Я думала-думала, как сюда прийти.</ta>
            <ta e="T210" id="Seg_10843" s="T207">Шкура лежала на земле.</ta>
            <ta e="T212" id="Seg_10844" s="T210">Хлеб поставили [печься].</ta>
            <ta e="T218" id="Seg_10845" s="T212">Потом съели этот хлеб, и мясо.</ta>
            <ta e="T220" id="Seg_10846" s="T218">И мясо.</ta>
            <ta e="T226" id="Seg_10847" s="T221">Потом поели, потом Богу помолились.</ta>
            <ta e="T235" id="Seg_10848" s="T227">Я ездила в Агинское, в церкви Богу молилась.</ta>
            <ta e="T245" id="Seg_10849" s="T236">Много народу умерло, крестов много стоит.</ta>
            <ta e="T255" id="Seg_10850" s="T246">У нас девушка заболела, отец повез ее лечиться.</ta>
            <ta e="T264" id="Seg_10851" s="T256">У нас сегодня ночью мужчина с женщиной подрались.</ta>
            <ta e="T270" id="Seg_10852" s="T264">По рукам кровь течет.</ta>
            <ta e="T273" id="Seg_10853" s="T270">Дети кричат.</ta>
            <ta e="T277" id="Seg_10854" s="T273">Я пришла, отругала [их].</ta>
            <ta e="T283" id="Seg_10855" s="T278">Теленок лег, теперь не мычит.</ta>
            <ta e="T286" id="Seg_10856" s="T284">Можно разговаривать.</ta>
            <ta e="T291" id="Seg_10857" s="T287">У мужчины нос зачесался.</ta>
            <ta e="T294" id="Seg_10858" s="T291">"Я где-нибудь водки выпью".</ta>
            <ta e="T299" id="Seg_10859" s="T294">Потом он сделал (?), он сидел на дереве.</ta>
            <ta e="T307" id="Seg_10860" s="T299">А моя бабушка пошла в Агинское и принесла водки.</ta>
            <ta e="T313" id="Seg_10861" s="T307">Она села испражняться к нему задом.</ta>
            <ta e="T316" id="Seg_10862" s="T313">Потом он пришел.</ta>
            <ta e="T322" id="Seg_10863" s="T316">Она ему водки дала, он выпил.</ta>
            <ta e="T326" id="Seg_10864" s="T322">И говорит ей:</ta>
            <ta e="T334" id="Seg_10865" s="T326">"Ну и баба, вот так баба так баба!"</ta>
            <ta e="T339" id="Seg_10866" s="T334">"Почему ты так [говоришь]?"</ta>
            <ta e="T347" id="Seg_10867" s="T339">"А ты ко мне задом сидела и срала".</ta>
            <ta e="T353" id="Seg_10868" s="T348">Моя бабушка пошла к шаману.</ta>
            <ta e="T362" id="Seg_10869" s="T353">Она мясо сварили, положили в большую миску.</ta>
            <ta e="T366" id="Seg_10870" s="T362">Оно остыло.</ta>
            <ta e="T369" id="Seg_10871" s="T366">"Сядь поешь!"</ta>
            <ta e="T376" id="Seg_10872" s="T369">Он взял мясо, оно совсем холодное.</ta>
            <ta e="T380" id="Seg_10873" s="T376">Тогда он его собаке бросил.</ta>
            <ta e="T385" id="Seg_10874" s="T381">[Шаман] бил в бубен.</ta>
            <ta e="T393" id="Seg_10875" s="T385">Все пошли туда, а я бабушке не дала.</ta>
            <ta e="T395" id="Seg_10876" s="T393">Потом она пошла.</ta>
            <ta e="T401" id="Seg_10877" s="T395">"Не приводите эту женщину, русскую женщину".</ta>
            <ta e="T403" id="Seg_10878" s="T401">Забыла (?).</ta>
            <ta e="T407" id="Seg_10879" s="T403">У шамана была парка.</ta>
            <ta e="T422" id="Seg_10880" s="T407">Всякая разная была: куриная кожа, соболья шкурка, беличья шкурка на парке.</ta>
            <ta e="T424" id="Seg_10881" s="T422">Козья шкура.</ta>
            <ta e="T439" id="Seg_10882" s="T425">Этот шаман шаманил-шаманил, потом взял нож и ткнул [себя?] в сердце.</ta>
            <ta e="T442" id="Seg_10883" s="T439">И потом упал.</ta>
            <ta e="T450" id="Seg_10884" s="T442">А людей много пришло, вроде минусинские.</ta>
            <ta e="T455" id="Seg_10885" s="T450">По-русски сказала…</ta>
            <ta e="T465" id="Seg_10886" s="T456">Мужчины когда идут в лес, [сначала] идут к шаману.</ta>
            <ta e="T476" id="Seg_10887" s="T465">Он скажет: "Ты соболя убьешь", а кому-то скажет: "Ты не убьешь".</ta>
            <ta e="T484" id="Seg_10888" s="T477">Минусинский шаман с нашим шаманом подрались.</ta>
            <ta e="T495" id="Seg_10889" s="T484">Минусинский шаман пошел в дом, а наш шаман его догнал.</ta>
            <ta e="T497" id="Seg_10890" s="T495">Была (драка?).</ta>
            <ta e="T503" id="Seg_10891" s="T497">А он ружьем его ударил.</ta>
            <ta e="T507" id="Seg_10892" s="T503">Сломал ему руку.</ta>
            <ta e="T509" id="Seg_10893" s="T507">Руку сломал.</ta>
            <ta e="T520" id="Seg_10894" s="T510">Если какой человек заболеет, пойдут к шаману, и он его лечит.</ta>
            <ta e="T525" id="Seg_10895" s="T520">Всегда кладет его [=бубен] на левую руку. [?]</ta>
            <ta e="T531" id="Seg_10896" s="T525">И так головой кланяется, кланяется.</ta>
            <ta e="T541" id="Seg_10897" s="T531">Потом он его (лечит?) и сам прыгает, в бубен бьет.</ta>
            <ta e="T548" id="Seg_10898" s="T542">Потом тот [больше] не болеет, идет домой.</ta>
            <ta e="T556" id="Seg_10899" s="T550">Мужчины уходили в лес, женщины сидели дома.</ta>
            <ta e="T562" id="Seg_10900" s="T556">Саранку копали, мясо (ставили?), хлеба не было.</ta>
            <ta e="T568" id="Seg_10901" s="T563">Женщины траву собирали.</ta>
            <ta e="T574" id="Seg_10902" s="T568">(?) вязали, сапоги шили, жилы плели.</ta>
            <ta e="T578" id="Seg_10903" s="T574">В лесу юрту ставили.</ta>
            <ta e="T583" id="Seg_10904" s="T578">В этой юрте мясо варили.</ta>
            <ta e="T587" id="Seg_10905" s="T583">Чай кипятили, ели.</ta>
            <ta e="T594" id="Seg_10906" s="T588">Дома были, они не жили в этих домах.</ta>
            <ta e="T597" id="Seg_10907" s="T594">На улице стояла юрта.</ta>
            <ta e="T600" id="Seg_10908" s="T597">Они там суп варили.</ta>
            <ta e="T603" id="Seg_10909" s="T600">Чай кипятили.</ta>
            <ta e="T607" id="Seg_10910" s="T603">Ели, а в доме они спали.</ta>
            <ta e="T619" id="Seg_10911" s="T608">Юрты делали [из] шестов, потом связывали шкуры с ветками и клали их туда. [?]</ta>
            <ta e="T633" id="Seg_10912" s="T619">Потом канаты [для котлов] протягивали, потом вешали котлы, чтобы варить мясо, кипятить чай.</ta>
            <ta e="T635" id="Seg_10913" s="T633">Потом…</ta>
            <ta e="T637" id="Seg_10914" s="T635">Стой (?).</ta>
            <ta e="T640" id="Seg_10915" s="T637">Потом огонь раскладывали.</ta>
            <ta e="T647" id="Seg_10916" s="T640">Наверху отверстие было, туда дым выходил.</ta>
            <ta e="T656" id="Seg_10917" s="T647">Потом они (клали?) дрова, чтобы в огонь никто не упал.</ta>
            <ta e="T664" id="Seg_10918" s="T657">Как станет холодно, они закрывали [юрту] шкурами.</ta>
            <ta e="T671" id="Seg_10919" s="T664">А как станет тепло, они снимали шкуры. [?]</ta>
            <ta e="T677" id="Seg_10920" s="T672">Она большая была, метра четыре.</ta>
            <ta e="T681" id="Seg_10921" s="T677">Десять человек могли лечь.</ta>
            <ta e="T687" id="Seg_10922" s="T681">Когда зима была, они там жили.</ta>
            <ta e="T697" id="Seg_10923" s="T687">А когда зима кончалась, они уходили в лес, в горы.</ta>
            <ta e="T707" id="Seg_10924" s="T698">С оленями шли, уходили на Белогорье, там большая река, они рыбу ловили.</ta>
            <ta e="T717" id="Seg_10925" s="T708">Они уходили, брали детей и женщин, уходили с оленями.</ta>
            <ta e="T720" id="Seg_10926" s="T718">И с собаками.</ta>
            <ta e="T727" id="Seg_10927" s="T721">Потом они в лесу собирали ягоды и ели.</ta>
            <ta e="T730" id="Seg_10928" s="T727">Собирали и лущили орехи.</ta>
            <ta e="T738" id="Seg_10929" s="T731">Потом они собирали грибы, варили их и солили.</ta>
            <ta e="T748" id="Seg_10930" s="T739">Потом они убивали там (?) и солили мясо.</ta>
            <ta e="T757" id="Seg_10931" s="T748">Они вешали [его], оно высыхало, они рубили его топором и клали в мешки.</ta>
            <ta e="T770" id="Seg_10932" s="T758">Потом они рыбу ловили и солили, и сушили рыбу тоже.</ta>
            <ta e="T781" id="Seg_10933" s="T771">Потом они из шкур делали мешки и складывали туда мясо и рыбу.</ta>
            <ta e="T785" id="Seg_10934" s="T781">И на оленях привозили домой.</ta>
            <ta e="T791" id="Seg_10935" s="T785">Туда, где они жили, когда было холодно.</ta>
            <ta e="T798" id="Seg_10936" s="T792">У них были такие собаки.</ta>
            <ta e="T809" id="Seg_10937" s="T798">Уйдут козы, они многих поймают, потом домой придет, эта лошадь (?).</ta>
            <ta e="T813" id="Seg_10938" s="T809">Потом пойдет коз собирать.</ta>
            <ta e="T816" id="Seg_10939" s="T813">Потом придет домой.</ta>
            <ta e="T820" id="Seg_10940" s="T817">И коз приведет.</ta>
            <ta e="T835" id="Seg_10941" s="T821">Их в армию не брали, они платили ясак деньгами и соболями.</ta>
            <ta e="T843" id="Seg_10942" s="T836">Они давали 15 [рублей] ясака.</ta>
            <ta e="T846" id="Seg_10943" s="T843">Ну вот так.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_10944" s="T0">When did you came to us?</ta>
            <ta e="T5" id="Seg_10945" s="T3">In a holiday.</ta>
            <ta e="T9" id="Seg_10946" s="T5">I lived two days in Aginskoye.</ta>
            <ta e="T13" id="Seg_10947" s="T9">What did you see?</ta>
            <ta e="T16" id="Seg_10948" s="T13">Then I came here.</ta>
            <ta e="T26" id="Seg_10949" s="T17">I looked [for an occasion?], but could by no means come here.</ta>
            <ta e="T31" id="Seg_10950" s="T26">Then I came here (for?) three days.</ta>
            <ta e="T37" id="Seg_10951" s="T32">Then I came here, I came to my grandmother.</ta>
            <ta e="T43" id="Seg_10952" s="T37">She gave me to eat, gave me soup.</ta>
            <ta e="T49" id="Seg_10953" s="T44">Then I came here, the calf is mooing.</ta>
            <ta e="T58" id="Seg_10954" s="T49">The woman gave him potatoes, it's eating [them], doesn't moo.</ta>
            <ta e="T65" id="Seg_10955" s="T59">It is cold in this woman's house, she heated the stove.</ta>
            <ta e="T70" id="Seg_10956" s="T66">It is very cold outside.</ta>
            <ta e="T74" id="Seg_10957" s="T70">The calf is staying in the house.</ta>
            <ta e="T81" id="Seg_10958" s="T75">The cow gave birth [to a calf], [this is] a little calf.</ta>
            <ta e="T88" id="Seg_10959" s="T82">In our village many people have died.</ta>
            <ta e="T95" id="Seg_10960" s="T89">I went to a woman, she is very ill.</ta>
            <ta e="T99" id="Seg_10961" s="T95">"I'll die today."</ta>
            <ta e="T101" id="Seg_10962" s="T99">"Well, die."</ta>
            <ta e="T103" id="Seg_10963" s="T101">"Well. die.</ta>
            <ta e="T109" id="Seg_10964" s="T103">Tomorrow I'll come to you to wash you.</ta>
            <ta e="T112" id="Seg_10965" s="T109">But I didn't come.</ta>
            <ta e="T116" id="Seg_10966" s="T112">Two men brought grass.</ta>
            <ta e="T122" id="Seg_10967" s="T116">Then I came: "Why didn't you come so long?"</ta>
            <ta e="T129" id="Seg_10968" s="T122">"I thought you've died, I came to wash you." [?]</ta>
            <ta e="T134" id="Seg_10969" s="T130">The calf (sleeps?), doesn't moo.</ta>
            <ta e="T142" id="Seg_10970" s="T135">Children are running near the house.</ta>
            <ta e="T147" id="Seg_10971" s="T143">Two boys came to listen.</ta>
            <ta e="T154" id="Seg_10972" s="T149">Don't shout, be quiet, listen!</ta>
            <ta e="T158" id="Seg_10973" s="T155">"When does your mother come?"</ta>
            <ta e="T160" id="Seg_10974" s="T158">"She'll come soon!"</ta>
            <ta e="T167" id="Seg_10975" s="T161">[I] should warm up water, then give the cow to drink.</ta>
            <ta e="T173" id="Seg_10976" s="T168">What did you come to hear, to look at?</ta>
            <ta e="T176" id="Seg_10977" s="T174">They don't shout.</ta>
            <ta e="T180" id="Seg_10978" s="T176">Good boys, they don't shout.</ta>
            <ta e="T183" id="Seg_10979" s="T180">They are staying, [they are] very good.</ta>
            <ta e="T189" id="Seg_10980" s="T184">I gave you today nuts to crack.</ta>
            <ta e="T200" id="Seg_10981" s="T190">Eyes to look, ears to listen, mouth to eat, teeth to bite bread.</ta>
            <ta e="T206" id="Seg_10982" s="T201">I was thinking how to come here.</ta>
            <ta e="T210" id="Seg_10983" s="T207">A skin lay on the ground.</ta>
            <ta e="T212" id="Seg_10984" s="T210">They put bread [to bake].</ta>
            <ta e="T218" id="Seg_10985" s="T212">Then they ate this bread, and meat.</ta>
            <ta e="T220" id="Seg_10986" s="T218">And meat.</ta>
            <ta e="T226" id="Seg_10987" s="T221">Then they ate, then they prayed to God.</ta>
            <ta e="T235" id="Seg_10988" s="T227">I went to Aginskoye, I prayed to God in the church.</ta>
            <ta e="T245" id="Seg_10989" s="T236">Many people died, there stand very many crosses.</ta>
            <ta e="T255" id="Seg_10990" s="T246">A girl fell ill and her father brought her to be treated. [?]</ta>
            <ta e="T264" id="Seg_10991" s="T256">Last night a man and a woman fought.</ta>
            <ta e="T270" id="Seg_10992" s="T264">There was blood on [their] hands.</ta>
            <ta e="T273" id="Seg_10993" s="T270">The children are screaming.</ta>
            <ta e="T277" id="Seg_10994" s="T273">I came, sweared [at them].</ta>
            <ta e="T283" id="Seg_10995" s="T278">The calf lay down, it doesn't moo now.</ta>
            <ta e="T286" id="Seg_10996" s="T284">One can speak.</ta>
            <ta e="T291" id="Seg_10997" s="T287">The man's nose itches.</ta>
            <ta e="T294" id="Seg_10998" s="T291">"I'll drink vodka somewhere."</ta>
            <ta e="T299" id="Seg_10999" s="T294">Then he made (?), he was sitting on a tree.</ta>
            <ta e="T307" id="Seg_11000" s="T299">My grandmother went to Aginskoye and brought vodka.</ta>
            <ta e="T313" id="Seg_11001" s="T307">Then she sat down to defecate with her back to him.</ta>
            <ta e="T316" id="Seg_11002" s="T313">Then he came.</ta>
            <ta e="T322" id="Seg_11003" s="T316">She gave him vodka, he drank [it].</ta>
            <ta e="T326" id="Seg_11004" s="T322">And says her:</ta>
            <ta e="T334" id="Seg_11005" s="T326">"What a woman!"</ta>
            <ta e="T339" id="Seg_11006" s="T334">"Why do you [say] so?"</ta>
            <ta e="T347" id="Seg_11007" s="T339">"You sat down with you back to me and defecated."</ta>
            <ta e="T353" id="Seg_11008" s="T348">My grandmother went to a shaman.</ta>
            <ta e="T362" id="Seg_11009" s="T353">There they boiled meat, put it in a big bowl.</ta>
            <ta e="T366" id="Seg_11010" s="T362">It became cold.</ta>
            <ta e="T369" id="Seg_11011" s="T366">"Sit down to eat!"</ta>
            <ta e="T376" id="Seg_11012" s="T369">He took the meat, it is very cold.</ta>
            <ta e="T380" id="Seg_11013" s="T376">Then he threw it to the dog.</ta>
            <ta e="T385" id="Seg_11014" s="T381">[A shaman] played [= beated] the drum.</ta>
            <ta e="T393" id="Seg_11015" s="T385">Everybody went there, and I don't let my grandmother [go there].</ta>
            <ta e="T395" id="Seg_11016" s="T393">Then she went.</ta>
            <ta e="T401" id="Seg_11017" s="T395">"Don't bring this woman, Russian woman."</ta>
            <ta e="T403" id="Seg_11018" s="T401">I forgot (?).</ta>
            <ta e="T407" id="Seg_11019" s="T403">The shaman had a coat.</ta>
            <ta e="T422" id="Seg_11020" s="T407">There were all kinds [of skin]: hen's skin, ermine's skin [was] on the coat, squirrel's skin [was] on the coat.</ta>
            <ta e="T424" id="Seg_11021" s="T422">Goat's skin.</ta>
            <ta e="T439" id="Seg_11022" s="T425">This shaman shamanised for some time, then he took a knife and stabbed [it] in [his?] heart.</ta>
            <ta e="T442" id="Seg_11023" s="T439">And then he fell down.</ta>
            <ta e="T450" id="Seg_11024" s="T442">Many people had come, from Minusinsk.</ta>
            <ta e="T455" id="Seg_11025" s="T450">I said it in Russian…</ta>
            <ta e="T465" id="Seg_11026" s="T456">When the men go to the forest, first they go to the shaman.</ta>
            <ta e="T476" id="Seg_11027" s="T465">He says [to one of them]: "You'll kill a sable", and to another he'll say: "You won't kill."</ta>
            <ta e="T484" id="Seg_11028" s="T477">The shaman from Minusinsk and our shaman fought.</ta>
            <ta e="T495" id="Seg_11029" s="T484">The shaman from Minusinsk went into a house, and our shaman ran after him. </ta>
            <ta e="T497" id="Seg_11030" s="T495">There was a (fight?).</ta>
            <ta e="T503" id="Seg_11031" s="T497">And he (hit?) him with a gun.</ta>
            <ta e="T507" id="Seg_11032" s="T503">He broke his arm.</ta>
            <ta e="T509" id="Seg_11033" s="T507">Broke [his] arm.</ta>
            <ta e="T520" id="Seg_11034" s="T510">If someone falls ill, they go to a shaman, and he treats him.</ta>
            <ta e="T525" id="Seg_11035" s="T520">He always puts on [= takes the drum in] his left hand. [?]</ta>
            <ta e="T531" id="Seg_11036" s="T525">He bows so with his head.</ta>
            <ta e="T541" id="Seg_11037" s="T531">Then he (treats?) him and himself jumps and beats his drum.</ta>
            <ta e="T548" id="Seg_11038" s="T542">Then that [man] isn't ill [anymore], he comes home.</ta>
            <ta e="T556" id="Seg_11039" s="T550">The men went to the forest, the women lived home.</ta>
            <ta e="T562" id="Seg_11040" s="T556">They dug out Lilium [roots], (put?) meat, there is no bread.</ta>
            <ta e="T568" id="Seg_11041" s="T563">The women picked grass.</ta>
            <ta e="T574" id="Seg_11042" s="T568">They plaited/skinned (?), sew boots, plaited tendons.</ta>
            <ta e="T578" id="Seg_11043" s="T574">They made a tent in the forest.</ta>
            <ta e="T583" id="Seg_11044" s="T578">Then in this tent they boiled meat.</ta>
            <ta e="T587" id="Seg_11045" s="T583">They boiled [= made] tea, they ate.</ta>
            <ta e="T594" id="Seg_11046" s="T588">There were houses, they didn't live in these houses.</ta>
            <ta e="T597" id="Seg_11047" s="T594">There was a tent outside.</ta>
            <ta e="T600" id="Seg_11048" s="T597">They boiled soup there.</ta>
            <ta e="T603" id="Seg_11049" s="T600">They boiled tea.</ta>
            <ta e="T607" id="Seg_11050" s="T603">They ate, and in the houses they slept.</ta>
            <ta e="T619" id="Seg_11051" s="T608">They made tents [with] poles, then interlaced the skins with twigs and put [them] there. [?]</ta>
            <ta e="T633" id="Seg_11052" s="T619">Then they strung ropes, then hung the kettles, to boil meat, to boil tea.</ta>
            <ta e="T635" id="Seg_11053" s="T633">Then…</ta>
            <ta e="T637" id="Seg_11054" s="T635">Stop (?).</ta>
            <ta e="T640" id="Seg_11055" s="T637">On the ground they put [the place for] fire.</ta>
            <ta e="T647" id="Seg_11056" s="T640">On the top there is a hole, the smoke goes [through it]</ta>
            <ta e="T656" id="Seg_11057" s="T647">Then they (put?) sticks to prevent anybody from falling into the fire.</ta>
            <ta e="T664" id="Seg_11058" s="T657">When it became cold, they covered [the tent] with skins.</ta>
            <ta e="T671" id="Seg_11059" s="T664">As it became warm, they took the skins off. [?]</ta>
            <ta e="T677" id="Seg_11060" s="T672">It was big, about four meters.</ta>
            <ta e="T681" id="Seg_11061" s="T677">Ten persons [could] lie [inside].</ta>
            <ta e="T687" id="Seg_11062" s="T681">In winter, they lived here.</ta>
            <ta e="T697" id="Seg_11063" s="T687">When the winter went by, they went to the forest, on a high mountain.</ta>
            <ta e="T707" id="Seg_11064" s="T698">They went with reindeer, they went to Delam, there is a big river, [there] they caught fish.</ta>
            <ta e="T717" id="Seg_11065" s="T708">Then they went, they took children and women, they went with the reindeer.</ta>
            <ta e="T720" id="Seg_11066" s="T718">And with dogs.</ta>
            <ta e="T727" id="Seg_11067" s="T721">Then they gathered and ate berries in the forest.</ta>
            <ta e="T730" id="Seg_11068" s="T727">They gathered and cracked nuts.</ta>
            <ta e="T738" id="Seg_11069" s="T731">Then they gathered mushrooms, boiled and salted them.</ta>
            <ta e="T748" id="Seg_11070" s="T739">Then they killed (?) there and salted the meat.</ta>
            <ta e="T757" id="Seg_11071" s="T748">Then they hung [it], it dried out, they chopped it with axes and put in bags.</ta>
            <ta e="T770" id="Seg_11072" s="T758">Then they caught fish, salted and dried the fish, too.</ta>
            <ta e="T781" id="Seg_11073" s="T771">Then they made bags with skins and put meat and fish there.</ta>
            <ta e="T785" id="Seg_11074" s="T781">And on the reindeer they brought it home.</ta>
            <ta e="T791" id="Seg_11075" s="T785">Where they lived when it was cold.</ta>
            <ta e="T798" id="Seg_11076" s="T792">They had such dogs.</ta>
            <ta e="T809" id="Seg_11077" s="T798">[If] the goats go, they will catch many, they will come home, this horse will (?).</ta>
            <ta e="T813" id="Seg_11078" s="T809">Then they go to gather goats.</ta>
            <ta e="T816" id="Seg_11079" s="T813">Then he'll come home.</ta>
            <ta e="T820" id="Seg_11080" s="T817">And he'll bring goats.</ta>
            <ta e="T835" id="Seg_11081" s="T821">They were not recruited into the army, they paid yasak with money and with sables.</ta>
            <ta e="T843" id="Seg_11082" s="T836">They gave 15 [roubles] for the yasak.</ta>
            <ta e="T846" id="Seg_11083" s="T843">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_11084" s="T0">Wann bist du zu uns gekommen?</ta>
            <ta e="T5" id="Seg_11085" s="T3">In einem Feiertag.</ta>
            <ta e="T9" id="Seg_11086" s="T5">I habe zwei Tage in Aginskoje gewohnt.</ta>
            <ta e="T13" id="Seg_11087" s="T9">Was hast du gesehen?</ta>
            <ta e="T16" id="Seg_11088" s="T13">Dann bin ich hierher gekommen?</ta>
            <ta e="T26" id="Seg_11089" s="T17">Ich suchte [nach einer Möglichkeit?], aber ich konnte auf keinem Weg herkommen.</ta>
            <ta e="T31" id="Seg_11090" s="T26">Dann kam ich (für?) drei Tage hierher.</ta>
            <ta e="T37" id="Seg_11091" s="T32">Dann kam ich hierher, ich kam zu meiner Großmutter.</ta>
            <ta e="T43" id="Seg_11092" s="T37">Sie gab mir zu essen, gab mir Suppe.</ta>
            <ta e="T49" id="Seg_11093" s="T44">Dann kam ich hierher, das Kalb muht.</ta>
            <ta e="T58" id="Seg_11094" s="T49">Die Frau gab ihm Kartoffeln, es isst [sie], es muht nicht.</ta>
            <ta e="T65" id="Seg_11095" s="T59">Es ist kalt im Haus der Frau, sie heizte den Ofen.</ta>
            <ta e="T70" id="Seg_11096" s="T66">Es ist sehr kalt draußen.</ta>
            <ta e="T74" id="Seg_11097" s="T70">Das Kalb bleibt im Haus.</ta>
            <ta e="T81" id="Seg_11098" s="T75">Die Kuh gebar [ein Kalb], [das ist] ein kleines Kalb.</ta>
            <ta e="T88" id="Seg_11099" s="T82">In unserem Dorf sind viele Leute gestorben.</ta>
            <ta e="T95" id="Seg_11100" s="T89">Ich bin zu einer Frau gegangen, sie ist sehr krank.</ta>
            <ta e="T99" id="Seg_11101" s="T95">"Ich sterbe heute."</ta>
            <ta e="T101" id="Seg_11102" s="T99">"Nun, stirb."</ta>
            <ta e="T103" id="Seg_11103" s="T101">"Nun, stirb.</ta>
            <ta e="T109" id="Seg_11104" s="T103">Morgen komme ich, um dich zu waschen.</ta>
            <ta e="T112" id="Seg_11105" s="T109">Aber ich bin nicht gegangen.</ta>
            <ta e="T116" id="Seg_11106" s="T112">Zwei Männer brachten Gras.</ta>
            <ta e="T122" id="Seg_11107" s="T116">Dann bin ich gekommen: "Warum bist du so lange nicht gekommen?"</ta>
            <ta e="T129" id="Seg_11108" s="T122">"Ich dachte, du seist gestorben, ich bin gekommen, um dich zu waschen." [?]</ta>
            <ta e="T134" id="Seg_11109" s="T130">Das Kalb (schläft?), es muht nicht.</ta>
            <ta e="T142" id="Seg_11110" s="T135">Kinder rennen in der Nähe des Hauses herum.</ta>
            <ta e="T147" id="Seg_11111" s="T143">Zwei Jungen sind gekommen um zuzuhören.</ta>
            <ta e="T154" id="Seg_11112" s="T149">Schreit nicht, seid leise, hört zu!</ta>
            <ta e="T158" id="Seg_11113" s="T155">"Wann kommt deine Mutter?"</ta>
            <ta e="T160" id="Seg_11114" s="T158">"Er wird bald kommen!"</ta>
            <ta e="T167" id="Seg_11115" s="T161">[Ich] muss Wasser erwärmen und dann der Kuh zu trinken geben.</ta>
            <ta e="T173" id="Seg_11116" s="T168">Um was zu hören, bist du gekommen, um was anzusehen?</ta>
            <ta e="T176" id="Seg_11117" s="T174">Sie schreien nicht.</ta>
            <ta e="T180" id="Seg_11118" s="T176">Gute Jungs, sie schreien nicht.</ta>
            <ta e="T183" id="Seg_11119" s="T180">Sie bleiben, [sie sind] sehr good.</ta>
            <ta e="T189" id="Seg_11120" s="T184">Ich habe dir heute Nüsse zum knacken gegeben.</ta>
            <ta e="T200" id="Seg_11121" s="T190">Augen um zu gucken, Ohren um zuzuhören, der Mund um zu essen, die Zähne um Brot zu beißen.</ta>
            <ta e="T206" id="Seg_11122" s="T201">Ich überlegte, wie ich hierher komme.</ta>
            <ta e="T210" id="Seg_11123" s="T207">Ein Fell lag auf dem Boden.</ta>
            <ta e="T212" id="Seg_11124" s="T210">Sie legten Brot [zum Backen] hin.</ta>
            <ta e="T218" id="Seg_11125" s="T212">Dann aßen sie dieses Brot und Fleisch.</ta>
            <ta e="T220" id="Seg_11126" s="T218">Und Fleisch.</ta>
            <ta e="T226" id="Seg_11127" s="T221">Dann aßen sie, dann beteten sie zu Gott.</ta>
            <ta e="T235" id="Seg_11128" s="T227">Ich bin nach Aginskoje gegangen, ich habe in der Kirche zu Gott gebetet.</ta>
            <ta e="T245" id="Seg_11129" s="T236">Viele Leute sind gestorben, dort stehen sehr viele Kreuze.</ta>
            <ta e="T255" id="Seg_11130" s="T246">Eine unsere Mädchen ist krank geworden und ihr Vater brachte sie, damit sie behandelt wird. [?]</ta>
            <ta e="T264" id="Seg_11131" s="T256">Letzte Nacht haben sich ein Mann und eine Frau geschlagen.</ta>
            <ta e="T270" id="Seg_11132" s="T264">Sie hatten Blut an den Händen.</ta>
            <ta e="T273" id="Seg_11133" s="T270">Die Kinder schreien.</ta>
            <ta e="T277" id="Seg_11134" s="T273">Ich bin gekommen, ich habe sie beschimpft.</ta>
            <ta e="T283" id="Seg_11135" s="T278">Das Kalb legte sich, es muht jetzt nicht.</ta>
            <ta e="T286" id="Seg_11136" s="T284">Man kann sprechen.</ta>
            <ta e="T291" id="Seg_11137" s="T287">Die Nase Dem Mann juckt die Nase.</ta>
            <ta e="T294" id="Seg_11138" s="T291">"Ich werde irgendwo Wodka trinken."</ta>
            <ta e="T299" id="Seg_11139" s="T294">Dann machte er (?), er saß auf einem Baum.</ta>
            <ta e="T307" id="Seg_11140" s="T299">Meine Großmutter ging nach Aginskoje und brachte Wodka.</ta>
            <ta e="T313" id="Seg_11141" s="T307">Dann setzte sie sich mit dem Rücken zu ihm, um zu koten. </ta>
            <ta e="T316" id="Seg_11142" s="T313">Dann kam er.</ta>
            <ta e="T322" id="Seg_11143" s="T316">Sie gab ihm Wodka, er trank [ihn].</ta>
            <ta e="T326" id="Seg_11144" s="T322">Und sagt ihr:</ta>
            <ta e="T334" id="Seg_11145" s="T326">"Was für eine Frau!"</ta>
            <ta e="T339" id="Seg_11146" s="T334">"Warum [sagst] du das so?"</ta>
            <ta e="T347" id="Seg_11147" s="T339">"Du hast dich mit dem Rücken zu mir gesetzt und gekotet."</ta>
            <ta e="T353" id="Seg_11148" s="T348">Meine Großmutter ging zu einem Schamanen.</ta>
            <ta e="T362" id="Seg_11149" s="T353">Dort kochten sie Fleisch und legten es in eine große Schüssel.</ta>
            <ta e="T366" id="Seg_11150" s="T362">Es wurde kalt.</ta>
            <ta e="T369" id="Seg_11151" s="T366">"Setz dich zum Essen!"</ta>
            <ta e="T376" id="Seg_11152" s="T369">Er nahm das Fleisch, es ist sehr kalt.</ta>
            <ta e="T380" id="Seg_11153" s="T376">Dann warf er es dem Hund hin.</ta>
            <ta e="T385" id="Seg_11154" s="T381">[Ein Schamane] spielte [= schlug] die Trommel.</ta>
            <ta e="T393" id="Seg_11155" s="T385">Jeder ging dorthin und ich lasse meine Großmutter nicht [dorthin].</ta>
            <ta e="T395" id="Seg_11156" s="T393">Dann ging sie.</ta>
            <ta e="T401" id="Seg_11157" s="T395">"Bring diese Frau nicht, russische Frau."</ta>
            <ta e="T403" id="Seg_11158" s="T401">Ich habe vergessen (?).</ta>
            <ta e="T407" id="Seg_11159" s="T403">Der Schamane hatte einen Mantel.</ta>
            <ta e="T422" id="Seg_11160" s="T407">Dort waren alle möglichen Arten [von Fell]: Hühnerfell, Hermelinfell [war] am Mantel, Eichhörnchenfell [war] am Mantel.</ta>
            <ta e="T424" id="Seg_11161" s="T422">Ziegenfell.</ta>
            <ta e="T439" id="Seg_11162" s="T425">Dieser Schamane schamanisierte einige Zeit, dann nahm er ein Messer und steckte [es] in [sein?] Herz.</ta>
            <ta e="T442" id="Seg_11163" s="T439">Und dann fiel er um.</ta>
            <ta e="T450" id="Seg_11164" s="T442">Viele Leute waren gekommen, aus Minusinsk.</ta>
            <ta e="T455" id="Seg_11165" s="T450">Ich habe es auf Russisch gesagt…</ta>
            <ta e="T465" id="Seg_11166" s="T456">Wenn die Männer in den Wald gehen, erst gehen sie zum Schamanen.</ta>
            <ta e="T476" id="Seg_11167" s="T465">Er sagt [zu einem von ihnen]: "Zu tötest einen Zobel", und zu einem anderen sagt er: "Du tötest nicht."</ta>
            <ta e="T484" id="Seg_11168" s="T477">Der Schamane aus Minusinsk und unser Schamane kämpften.</ta>
            <ta e="T495" id="Seg_11169" s="T484">Der Schamane aus Minusinsk ging in ein Haus und unser Schamane rannte ihm hinterher.</ta>
            <ta e="T497" id="Seg_11170" s="T495">Dort war ein (Kampf?).</ta>
            <ta e="T503" id="Seg_11171" s="T497">Und er (schlug?) ihn mit einer Pistole.</ta>
            <ta e="T507" id="Seg_11172" s="T503">Er brach seinen Arm.</ta>
            <ta e="T509" id="Seg_11173" s="T507">Brach [seinen] Arm.</ta>
            <ta e="T520" id="Seg_11174" s="T510">Wenn jemand krank wird, dann geht man zum Schamanen, und er behandelt ihn.</ta>
            <ta e="T525" id="Seg_11175" s="T520">Er legt [nimmet den Trommel] immer in seiner linken Hand. [?]</ta>
            <ta e="T531" id="Seg_11176" s="T525">Er beugt sich so mit seinem Kopf.</ta>
            <ta e="T541" id="Seg_11177" s="T531">Dann (behandelt?) er ihn und er selbst springt und schlägt seine Trommel.</ta>
            <ta e="T548" id="Seg_11178" s="T542">Dann ist dieser [Mann] nicht [mehr] krank, er kommt nach Hause.</ta>
            <ta e="T556" id="Seg_11179" s="T550">Die Männer gingen in den Wald, die Frauen lebten zuhause.</ta>
            <ta e="T562" id="Seg_11180" s="T556">Sie gruben Türkenbund[wurzeln] aus, (legten?) Fleisch, es gibt kein Brot.</ta>
            <ta e="T568" id="Seg_11181" s="T563">Die Frauen pflückten Gras.</ta>
            <ta e="T574" id="Seg_11182" s="T568">Sie flochten/zogen (?) ab, nähten Stiefel, flochten Sehnen.</ta>
            <ta e="T578" id="Seg_11183" s="T574">Sie machten ein Zelt im Wald.</ta>
            <ta e="T583" id="Seg_11184" s="T578">Dann kochten sie Fleisch in diesem Zelt.</ta>
            <ta e="T587" id="Seg_11185" s="T583">Sie kochten [= machten] Tee, sie aßen.</ta>
            <ta e="T594" id="Seg_11186" s="T588">Dort gab es Häuser, sie lebten nicht in diesen Häusern.</ta>
            <ta e="T597" id="Seg_11187" s="T594">Dort draußen war ein Zelt.</ta>
            <ta e="T600" id="Seg_11188" s="T597">Dort kochten sie Suppe.</ta>
            <ta e="T603" id="Seg_11189" s="T600">Sie kochten Tee.</ta>
            <ta e="T607" id="Seg_11190" s="T603">Sie aßen, und in den Häusern schliefen sie.</ta>
            <ta e="T619" id="Seg_11191" s="T608">Sie machten Zelte [aus] Stangen, dann verflochten sie die Häute mit Zweigen und legten [sie] dorthin. [?]</ta>
            <ta e="T633" id="Seg_11192" s="T619">Dann spannten sie Seile, sie hängten die Kessel auf, um Fleisch zu kochen, um Tee zu kochen.</ta>
            <ta e="T635" id="Seg_11193" s="T633">Dann…</ta>
            <ta e="T637" id="Seg_11194" s="T635">Stop (?).</ta>
            <ta e="T640" id="Seg_11195" s="T637">Auf den Boden legten sie [den Platz für] das Feuer.</ta>
            <ta e="T647" id="Seg_11196" s="T640">An der Spitze ist ein Loch, Rauch geht [dadurch].</ta>
            <ta e="T656" id="Seg_11197" s="T647">Dann (stellten?) sie Stöcker auf, um zu verhindern, dass jemand ins Feuer fällt.</ta>
            <ta e="T664" id="Seg_11198" s="T657">Wenn es kalt wurde, bedeckten sie [das Zelt] mit Häuten.</ta>
            <ta e="T671" id="Seg_11199" s="T664">Wenn es warm wurde, nahmen sie sie herunter. [?] </ta>
            <ta e="T677" id="Seg_11200" s="T672">Es war groß, ungefähr vier Meter.</ta>
            <ta e="T681" id="Seg_11201" s="T677">Zehn Leute [konnten darin] liegen.</ta>
            <ta e="T687" id="Seg_11202" s="T681">Im Winter lebten sie dort.</ta>
            <ta e="T697" id="Seg_11203" s="T687">Wenn der Winter vorüber war, gingen sie in den Wald, auf einen hohen Berg.</ta>
            <ta e="T707" id="Seg_11204" s="T698">Sie gingen mit Rentieren, sie gingen nach Delam, dort ist ein großer Fluss, [dort] fingen sie Fisch.</ta>
            <ta e="T717" id="Seg_11205" s="T708">Dann gingen sie, sie nahmen Kinder und Frauen, sie gingen mit den Rentieren.</ta>
            <ta e="T720" id="Seg_11206" s="T718">Und mit Hunden.</ta>
            <ta e="T727" id="Seg_11207" s="T721">Dann sammelten und aßen sie Beeren im Wald.</ta>
            <ta e="T730" id="Seg_11208" s="T727">Sie sammelten und knackten Nüsse.</ta>
            <ta e="T738" id="Seg_11209" s="T731">Dann sammelten sie Pilze, kochten und salzten sie.</ta>
            <ta e="T748" id="Seg_11210" s="T739">Dann töteten sie (?) dort und salzten das Fleisch.</ta>
            <ta e="T757" id="Seg_11211" s="T748">Dann hängten sie [es] auf, es trocknete, sie hackten es mit Äxten und legten in Säcke.</ta>
            <ta e="T770" id="Seg_11212" s="T758">Dann fingen sie Fisch, salzten und trockneten auch den Fisch.</ta>
            <ta e="T781" id="Seg_11213" s="T771">Dann machten sie Säcke mit Häuten und taten Fleisch und Fisch dorthin.</ta>
            <ta e="T785" id="Seg_11214" s="T781">Und auf den Rentieren brachten sie es nach Hause.</ta>
            <ta e="T791" id="Seg_11215" s="T785">Wo sie lebten, wenn es kalt war.</ta>
            <ta e="T798" id="Seg_11216" s="T792">Sie hatten solche Hunde.</ta>
            <ta e="T809" id="Seg_11217" s="T798">[Wenn] die Ziegen gehen, fangen sie viele, sie kommen nach Hause, dieses Pferd wird (?).</ta>
            <ta e="T813" id="Seg_11218" s="T809">Dann gehen sie, um die Ziegen zu sammeln.</ta>
            <ta e="T816" id="Seg_11219" s="T813">Dann kommt er nach Hause.</ta>
            <ta e="T820" id="Seg_11220" s="T817">Und er bringt Ziegen mit.</ta>
            <ta e="T835" id="Seg_11221" s="T821">Sie wurden nicht in die Armee eingezogen, sie bezahlten den Tribut mit Geld und mit Zobeln.</ta>
            <ta e="T843" id="Seg_11222" s="T836">Sie gaben 15 [Rubel] für den Tribut.</ta>
            <ta e="T846" id="Seg_11223" s="T843">Nun gut.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_11224" s="T9">[GVY:] Unclear</ta>
            <ta e="T88" id="Seg_11225" s="T82">[GVY:] tura here means 'village' rather than 'house'?</ta>
            <ta e="T235" id="Seg_11226" s="T227">[GVY:] tʼegermaʔ 'church'</ta>
            <ta e="T255" id="Seg_11227" s="T246">[GVY:] very tentatively</ta>
            <ta e="T264" id="Seg_11228" s="T256">!!! fought?</ta>
            <ta e="T294" id="Seg_11229" s="T291">[GVY:] an omen</ta>
            <ta e="T353" id="Seg_11230" s="T348">[GVY:] mĭmbiem = mĭmbi?</ta>
            <ta e="T401" id="Seg_11231" s="T395">[GVY:] Unclear</ta>
            <ta e="T407" id="Seg_11232" s="T403">[GVY:] šamargən</ta>
            <ta e="T450" id="Seg_11233" s="T442">[GVY:] быдто (Standard Russian будто) showing the evidentiality here?</ta>
            <ta e="T503" id="Seg_11234" s="T497">[GVY:] Evidently not killed or shot, but hit him with the rifle.</ta>
            <ta e="T562" id="Seg_11235" s="T556">[GVY:] it is not clear what they did with the meat.</ta>
            <ta e="T574" id="Seg_11236" s="T568">[GVY:] травянига or травяник: a word apparently of Russian origin, but its meaning is not clear. </ta>
            <ta e="T583" id="Seg_11237" s="T578">[GVY:] Note the pronounciation of -ʔg- in maʔgən.</ta>
            <ta e="T594" id="Seg_11238" s="T588">[GVY:] SG = PL</ta>
            <ta e="T619" id="Seg_11239" s="T608">[GVY:] "da" is cliticised to the preceding word and has a big pause after it.</ta>
            <ta e="T640" id="Seg_11240" s="T637">[GVY:] ambiʔi = embiʔi (cf. below).</ta>
            <ta e="T656" id="Seg_11241" s="T647">[GVY:] ambiʔi = embiʔi (cf. above)?</ta>
            <ta e="T671" id="Seg_11242" s="T664">[GVY:] maybe she meant karbiʔi 'opened'</ta>
            <ta e="T781" id="Seg_11243" s="T771">[GVY:] gubadzi</ta>
            <ta e="T809" id="Seg_11244" s="T798">[GVY:] Unclear</ta>
            <ta e="T843" id="Seg_11245" s="T836">[GVY:] mĭmbiʔi = mĭbiʔi.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T850" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T851" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T852" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T848" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T849" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
