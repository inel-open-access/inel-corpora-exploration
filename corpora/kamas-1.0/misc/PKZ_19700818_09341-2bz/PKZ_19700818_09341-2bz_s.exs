<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3F9A55B8-CD56-2063-8A5E-662F18F9063D">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_19700818_09341_2bz</transcription-name>
         <referenced-file url="PKZ_19700818_09341-2bz.wav" />
         <referenced-file url="PKZ_19700818_09341-2bz.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700818_09341-2bz\PKZ_19700818_09341-2bz.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">2086</ud-information>
            <ud-information attribute-name="# HIAT:w">1176</ud-information>
            <ud-information attribute-name="# e">1236</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">67</ud-information>
            <ud-information attribute-name="# HIAT:u">244</ud-information>
            <ud-information attribute-name="# sc">448</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.26" type="appl" />
         <tli id="T2" time="0.799" type="appl" />
         <tli id="T3" time="1.338" type="appl" />
         <tli id="T4" time="1.877" type="appl" />
         <tli id="T5" time="2.4160000000000004" type="appl" />
         <tli id="T6" time="2.955" type="appl" />
         <tli id="T7" time="3.4939999999999998" type="appl" />
         <tli id="T8" time="4.033" type="appl" />
         <tli id="T9" time="4.572" type="appl" />
         <tli id="T10" time="5.111" type="appl" />
         <tli id="T11" time="5.62" type="appl" />
         <tli id="T12" time="5.65" type="appl" />
         <tli id="T13" time="14.86" type="appl" />
         <tli id="T14" time="15.343785714285714" type="appl" />
         <tli id="T15" time="15.47" type="appl" />
         <tli id="T16" time="15.470000000000002" type="appl" />
         <tli id="T17" time="15.827571428571428" type="appl" />
         <tli id="T18" time="16.31135714285714" type="appl" />
         <tli id="T19" time="16.795142857142856" type="appl" />
         <tli id="T20" time="17.278928571428573" type="appl" />
         <tli id="T21" time="17.762714285714285" type="appl" />
         <tli id="T22" time="18.246499999999997" type="appl" />
         <tli id="T23" time="18.730285714285714" type="appl" />
         <tli id="T24" time="19.21407142857143" type="appl" />
         <tli id="T25" time="19.697857142857142" type="appl" />
         <tli id="T26" time="20.181642857142855" type="appl" />
         <tli id="T27" time="20.66542857142857" type="appl" />
         <tli id="T28" time="21.149214285714287" type="appl" />
         <tli id="T29" time="21.633" type="appl" />
         <tli id="T30" time="22.54" type="appl" />
         <tli id="T1461" />
         <tli id="T1462" />
         <tli id="T31" time="40.555" type="appl" />
         <tli id="T32" time="42.07" type="appl" />
         <tli id="T1463" />
         <tli id="T1464" />
         <tli id="T33" time="48.389" type="appl" />
         <tli id="T34" time="48.74" type="appl" />
         <tli id="T35" time="49.27" type="appl" />
         <tli id="T36" time="49.800000000000004" type="appl" />
         <tli id="T37" time="50.33" type="appl" />
         <tli id="T38" time="50.86" type="appl" />
         <tli id="T39" time="51.39" type="appl" />
         <tli id="T40" time="51.92" type="appl" />
         <tli id="T41" time="52.45" type="appl" />
         <tli id="T42" time="52.98" type="appl" />
         <tli id="T43" time="53.51" type="appl" />
         <tli id="T44" time="54.04" type="appl" />
         <tli id="T45" time="54.57" type="appl" />
         <tli id="T46" time="55.099999999999994" type="appl" />
         <tli id="T47" time="55.629999999999995" type="appl" />
         <tli id="T48" time="56.16" type="appl" />
         <tli id="T49" time="56.58" type="appl" />
         <tli id="T50" time="56.69" type="appl" />
         <tli id="T51" time="57.485" type="appl" />
         <tli id="T52" time="58.253105869595295" />
         <tli id="T53" time="60.006432356629354" />
         <tli id="T54" time="61.28666666666667" type="appl" />
         <tli id="T55" time="62.473333333333336" type="appl" />
         <tli id="T56" time="63.660000000000004" type="appl" />
         <tli id="T57" time="64.84666666666666" type="appl" />
         <tli id="T58" time="66.03333333333333" type="appl" />
         <tli id="T59" time="67.22" type="appl" />
         <tli id="T60" time="68.40666666666667" type="appl" />
         <tli id="T61" time="69.59333333333333" type="appl" />
         <tli id="T62" time="70.78" type="appl" />
         <tli id="T63" time="70.8" type="appl" />
         <tli id="T64" time="71.28" type="appl" />
         <tli id="T65" time="71.75999999999999" type="appl" />
         <tli id="T66" time="72.24" type="appl" />
         <tli id="T67" time="72.72" type="appl" />
         <tli id="T68" time="73.36128571428571" type="appl" />
         <tli id="T69" time="74.00257142857143" type="appl" />
         <tli id="T70" time="74.64385714285714" type="appl" />
         <tli id="T71" time="75.28514285714286" type="appl" />
         <tli id="T72" time="75.92642857142857" type="appl" />
         <tli id="T73" time="76.56771428571429" type="appl" />
         <tli id="T74" time="77.209" type="appl" />
         <tli id="T75" time="78.2" type="appl" />
         <tli id="T76" time="78.87642857142858" type="appl" />
         <tli id="T77" time="79.55285714285715" type="appl" />
         <tli id="T78" time="80.22928571428572" type="appl" />
         <tli id="T79" time="80.90571428571428" type="appl" />
         <tli id="T80" time="81.58214285714286" type="appl" />
         <tli id="T81" time="82.25857142857143" type="appl" />
         <tli id="T82" time="82.935" type="appl" />
         <tli id="T83" time="83.61142857142858" type="appl" />
         <tli id="T84" time="84.28785714285715" type="appl" />
         <tli id="T85" time="84.96428571428572" type="appl" />
         <tli id="T86" time="85.64071428571428" type="appl" />
         <tli id="T87" time="86.31714285714285" type="appl" />
         <tli id="T88" time="86.99357142857143" type="appl" />
         <tli id="T89" time="87.67" type="appl" />
         <tli id="T90" time="88.85" type="appl" />
         <tli id="T91" time="91.4396429511752" />
         <tli id="T92" time="91.958" type="appl" />
         <tli id="T93" time="92.876" type="appl" />
         <tli id="T94" time="93.794" type="appl" />
         <tli id="T95" time="94.712" type="appl" />
         <tli id="T96" time="95.63" type="appl" />
         <tli id="T97" time="122.238" type="appl" />
         <tli id="T98" time="122.92" type="appl" />
         <tli id="T1465" />
         <tli id="T1466" />
         <tli id="T99" time="125.365" type="appl" />
         <tli id="T100" time="125.4" type="appl" />
         <tli id="T101" time="126.0975" type="appl" />
         <tli id="T102" time="126.795" type="appl" />
         <tli id="T103" time="127.4925" type="appl" />
         <tli id="T104" time="128.19" type="appl" />
         <tli id="T105" time="128.78" type="appl" />
         <tli id="T106" time="129.4432857142857" type="appl" />
         <tli id="T107" time="130.10657142857144" type="appl" />
         <tli id="T108" time="130.76985714285715" type="appl" />
         <tli id="T109" time="131.43314285714285" type="appl" />
         <tli id="T110" time="132.09642857142856" type="appl" />
         <tli id="T111" time="132.7597142857143" type="appl" />
         <tli id="T112" time="133.423" type="appl" />
         <tli id="T113" time="134.0862857142857" type="appl" />
         <tli id="T114" time="134.74957142857144" type="appl" />
         <tli id="T115" time="135.41285714285715" type="appl" />
         <tli id="T116" time="136.07614285714286" type="appl" />
         <tli id="T117" time="136.73942857142856" type="appl" />
         <tli id="T118" time="137.4027142857143" type="appl" />
         <tli id="T119" time="138.066" type="appl" />
         <tli id="T120" time="138.885" type="appl" />
         <tli id="T121" time="139.83733333333333" type="appl" />
         <tli id="T122" time="140.78966666666665" type="appl" />
         <tli id="T123" time="141.742" type="appl" />
         <tli id="T124" time="142.3635" type="appl" />
         <tli id="T125" time="142.98499999999999" type="appl" />
         <tli id="T126" time="143.60649999999998" type="appl" />
         <tli id="T127" time="144.228" type="appl" />
         <tli id="T128" time="144.8495" type="appl" />
         <tli id="T129" time="145.4376612691513" />
         <tli id="T130" time="145.971" type="appl" />
         <tli id="T131" time="146.45100366647063" />
         <tli id="T132" time="146.4516677263776" />
         <tli id="T133" time="146.985" type="appl" />
         <tli id="T134" time="147.485" type="appl" />
         <tli id="T135" time="147.985" type="appl" />
         <tli id="T136" time="148.171" type="appl" />
         <tli id="T137" time="148.9796" type="appl" />
         <tli id="T138" time="149.7882" type="appl" />
         <tli id="T139" time="150.5968" type="appl" />
         <tli id="T140" time="151.4054" type="appl" />
         <tli id="T141" time="152.214" type="appl" />
         <tli id="T142" time="152.499" type="appl" />
         <tli id="T143" time="153.56333333333333" type="appl" />
         <tli id="T144" time="154.62766666666667" type="appl" />
         <tli id="T145" time="155.692" type="appl" />
         <tli id="T146" time="156.75633333333332" type="appl" />
         <tli id="T147" time="157.82066666666665" type="appl" />
         <tli id="T148" time="158.885" type="appl" />
         <tli id="T149" time="159.099" type="appl" />
         <tli id="T150" time="159.76566666666665" type="appl" />
         <tli id="T151" time="160.43233333333333" type="appl" />
         <tli id="T152" time="161.099" type="appl" />
         <tli id="T153" time="161.76566666666665" type="appl" />
         <tli id="T154" time="162.43233333333333" type="appl" />
         <tli id="T155" time="163.099" type="appl" />
         <tli id="T156" time="164.142" type="appl" />
         <tli id="T1467" />
         <tli id="T1468" />
         <tli id="T157" time="209.199" type="appl" />
         <tli id="T158" time="209.242" type="appl" />
         <tli id="T1469" />
         <tli id="T1470" />
         <tli id="T159" time="212.857" type="appl" />
         <tli id="T160" time="213.386" type="appl" />
         <tli id="T161" time="213.80663636363636" type="appl" />
         <tli id="T162" time="214.22727272727272" type="appl" />
         <tli id="T163" time="214.64790909090908" type="appl" />
         <tli id="T164" time="215.06854545454544" type="appl" />
         <tli id="T165" time="215.4891818181818" type="appl" />
         <tli id="T166" time="215.9098181818182" type="appl" />
         <tli id="T167" time="216.33045454545456" type="appl" />
         <tli id="T168" time="216.75109090909092" type="appl" />
         <tli id="T169" time="217.17172727272728" type="appl" />
         <tli id="T170" time="217.59236363636364" type="appl" />
         <tli id="T171" time="218.013" type="appl" />
         <tli id="T172" time="219.329" type="appl" />
         <tli id="T173" time="219.8432" type="appl" />
         <tli id="T174" time="220.3574" type="appl" />
         <tli id="T175" time="220.8716" type="appl" />
         <tli id="T176" time="221.38580000000002" type="appl" />
         <tli id="T177" time="221.9" type="appl" />
         <tli id="T178" time="221.915" type="appl" />
         <tli id="T179" time="222.912" type="appl" />
         <tli id="T180" time="223.909" type="appl" />
         <tli id="T181" time="224.906" type="appl" />
         <tli id="T182" time="225.903" type="appl" />
         <tli id="T183" time="226.9" type="appl" />
         <tli id="T184" time="227.897" type="appl" />
         <tli id="T185" time="228.894" type="appl" />
         <tli id="T186" time="229.891" type="appl" />
         <tli id="T187" time="230.888" type="appl" />
         <tli id="T188" time="231.086" type="appl" />
         <tli id="T189" time="231.961" type="appl" />
         <tli id="T190" time="232.836" type="appl" />
         <tli id="T191" time="233.711" type="appl" />
         <tli id="T192" time="234.586" type="appl" />
         <tli id="T193" time="234.828" type="appl" />
         <tli id="T194" time="235.671" type="appl" />
         <tli id="T195" time="236.514" type="appl" />
         <tli id="T196" time="237.357" type="appl" />
         <tli id="T197" time="238.2" type="appl" />
         <tli id="T198" time="239.043" type="appl" />
         <tli id="T199" time="239.886" type="appl" />
         <tli id="T200" time="240.729" type="appl" />
         <tli id="T1531" time="241.08027113353296" type="intp" />
         <tli id="T201" time="241.66572302275455" />
         <tli id="T202" time="241.728" type="appl" />
         <tli id="T203" time="242.61985714285714" type="appl" />
         <tli id="T204" time="243.5117142857143" type="appl" />
         <tli id="T205" time="244.40357142857144" type="appl" />
         <tli id="T206" time="245.29542857142857" type="appl" />
         <tli id="T207" time="246.1872857142857" type="appl" />
         <tli id="T208" time="247.07914285714287" type="appl" />
         <tli id="T209" time="247.971" type="appl" />
         <tli id="T210" time="248.299" type="appl" />
         <tli id="T211" time="250.127" type="appl" />
         <tli id="T212" time="250.826" type="appl" />
         <tli id="T213" time="252.17833333333334" type="appl" />
         <tli id="T214" time="253.53066666666666" type="appl" />
         <tli id="T1532" time="254.03779166666666" type="intp" />
         <tli id="T215" time="254.883" type="appl" />
         <tli id="T216" time="255.398" type="appl" />
         <tli id="T217" time="257.655" type="appl" />
         <tli id="T218" time="258.0693333333333" type="appl" />
         <tli id="T219" time="258.48366666666664" type="appl" />
         <tli id="T220" time="258.898" type="appl" />
         <tli id="T221" time="259.31233333333336" type="appl" />
         <tli id="T222" time="259.7266666666667" type="appl" />
         <tli id="T223" time="260.141" type="appl" />
         <tli id="T224" time="260.155" type="appl" />
         <tli id="T225" time="260.369" type="appl" />
         <tli id="T226" time="260.8405" type="appl" />
         <tli id="T227" time="261.312" type="appl" />
         <tli id="T228" time="261.455" type="appl" />
         <tli id="T229" time="300.914" type="appl" />
         <tli id="T230" time="301.44599999999997" type="appl" />
         <tli id="T231" time="301.97799999999995" type="appl" />
         <tli id="T232" time="302.51" type="appl" />
         <tli id="T233" time="303.042" type="appl" />
         <tli id="T234" time="313.54" type="appl" />
         <tli id="T235" time="313.657" type="appl" />
         <tli id="T1471" />
         <tli id="T1472" />
         <tli id="T236" time="316.69" type="appl" />
         <tli id="T237" time="316.943" type="appl" />
         <tli id="T238" time="317.3461111111111" type="appl" />
         <tli id="T239" time="317.7492222222222" type="appl" />
         <tli id="T240" time="318.15233333333333" type="appl" />
         <tli id="T241" time="318.55544444444445" type="appl" />
         <tli id="T242" time="318.95855555555556" type="appl" />
         <tli id="T243" time="319.3616666666667" type="appl" />
         <tli id="T244" time="319.7647777777778" type="appl" />
         <tli id="T245" time="320.1678888888889" type="appl" />
         <tli id="T246" time="320.571" type="appl" />
         <tli id="T247" time="321.457" type="appl" />
         <tli id="T248" time="322.451" type="appl" />
         <tli id="T249" time="323.445" type="appl" />
         <tli id="T250" time="324.43899999999996" type="appl" />
         <tli id="T251" time="325.433" type="appl" />
         <tli id="T252" time="325.557" type="appl" />
         <tli id="T253" time="326.44275000000005" type="appl" />
         <tli id="T254" time="327.3285" type="appl" />
         <tli id="T255" time="328.21425" type="appl" />
         <tli id="T256" time="329.1" type="appl" />
         <tli id="T257" time="329.9775714285714" type="appl" />
         <tli id="T258" time="330.8551428571429" type="appl" />
         <tli id="T259" time="331.7327142857143" type="appl" />
         <tli id="T260" time="332.61028571428574" type="appl" />
         <tli id="T261" time="333.48785714285714" type="appl" />
         <tli id="T262" time="334.3654285714286" type="appl" />
         <tli id="T263" time="335.243" type="appl" />
         <tli id="T264" time="335.371" type="appl" />
         <tli id="T265" time="359.87" type="appl" />
         <tli id="T266" time="360.3073846153846" type="appl" />
         <tli id="T267" time="360.74476923076924" type="appl" />
         <tli id="T268" time="360.8" type="appl" />
         <tli id="T269" time="361.1821538461538" type="appl" />
         <tli id="T270" time="361.61953846153847" type="appl" />
         <tli id="T271" time="362.05692307692306" type="appl" />
         <tli id="T272" time="362.4943076923077" type="appl" />
         <tli id="T273" time="362.9316923076923" type="appl" />
         <tli id="T274" time="363.36907692307693" type="appl" />
         <tli id="T275" time="363.8064615384615" type="appl" />
         <tli id="T276" time="364.24384615384616" type="appl" />
         <tli id="T277" time="364.68123076923075" type="appl" />
         <tli id="T278" time="365.1186153846154" type="appl" />
         <tli id="T279" time="365.556" type="appl" />
         <tli id="T280" time="365.685" type="appl" />
         <tli id="T281" time="366.27671428571426" type="appl" />
         <tli id="T282" time="366.8684285714286" type="appl" />
         <tli id="T283" time="367.46014285714284" type="appl" />
         <tli id="T284" time="368.05185714285716" type="appl" />
         <tli id="T285" time="368.6435714285714" type="appl" />
         <tli id="T286" time="369.23528571428574" type="appl" />
         <tli id="T287" time="369.827" type="appl" />
         <tli id="T288" time="369.842" type="appl" />
         <tli id="T289" time="370.2740833333333" type="appl" />
         <tli id="T290" time="370.70616666666666" type="appl" />
         <tli id="T291" time="371.13824999999997" type="appl" />
         <tli id="T292" time="371.57033333333334" type="appl" />
         <tli id="T293" time="372.00241666666665" type="appl" />
         <tli id="T294" time="372.43449999999996" type="appl" />
         <tli id="T295" time="372.8665833333333" type="appl" />
         <tli id="T296" time="373.29866666666663" type="appl" />
         <tli id="T297" time="373.73075" type="appl" />
         <tli id="T298" time="374.1628333333333" type="appl" />
         <tli id="T299" time="374.5949166666667" type="appl" />
         <tli id="T300" time="375.027" type="appl" />
         <tli id="T301" time="377.3718597910633" />
         <tli id="T302" time="378.1251901828308" />
         <tli id="T303" time="378.678521355545" />
         <tli id="T304" time="378.8518540120578" />
         <tli id="T305" time="379.01852002793555" />
         <tli id="T306" time="379.07692857142854" type="appl" />
         <tli id="T307" time="379.46671428571426" type="appl" />
         <tli id="T308" time="379.8565" type="appl" />
         <tli id="T309" time="380.2462857142857" type="appl" />
         <tli id="T310" time="380.6360714285714" type="appl" />
         <tli id="T311" time="381.02585714285715" type="appl" />
         <tli id="T312" time="381.4156428571428" type="appl" />
         <tli id="T313" time="381.80542857142854" type="appl" />
         <tli id="T314" time="382.19521428571426" type="appl" />
         <tli id="T315" time="382.585" type="appl" />
         <tli id="T316" time="383.0468333333333" type="appl" />
         <tli id="T317" time="383.50866666666667" type="appl" />
         <tli id="T318" time="383.9705" type="appl" />
         <tli id="T319" time="384.4323333333333" type="appl" />
         <tli id="T320" time="384.89416666666665" type="appl" />
         <tli id="T321" time="385.356" type="appl" />
         <tli id="T322" time="385.81783333333334" type="appl" />
         <tli id="T323" time="386.2796666666667" type="appl" />
         <tli id="T324" time="386.7415" type="appl" />
         <tli id="T325" time="387.2033333333333" type="appl" />
         <tli id="T326" time="387.66516666666666" type="appl" />
         <tli id="T327" time="388.2251507450231" />
         <tli id="T328" time="388.599" type="appl" />
         <tli id="T329" time="389.0038" type="appl" />
         <tli id="T330" time="389.4086" type="appl" />
         <tli id="T331" time="389.8134" type="appl" />
         <tli id="T332" time="390.21819999999997" type="appl" />
         <tli id="T333" time="390.623" type="appl" />
         <tli id="T334" time="391.0278" type="appl" />
         <tli id="T335" time="391.4326" type="appl" />
         <tli id="T336" time="391.8374" type="appl" />
         <tli id="T337" time="392.24219999999997" type="appl" />
         <tli id="T338" time="392.647" type="appl" />
         <tli id="T339" time="393.0518" type="appl" />
         <tli id="T340" time="393.4566" type="appl" />
         <tli id="T341" time="393.8614" type="appl" />
         <tli id="T342" time="394.26619999999997" type="appl" />
         <tli id="T343" time="394.671" type="appl" />
         <tli id="T344" time="394.685" type="appl" />
         <tli id="T345" time="395.24199999999996" type="appl" />
         <tli id="T346" time="395.799" type="appl" />
         <tli id="T347" time="395.942" type="appl" />
         <tli id="T348" time="396.5323333333333" type="appl" />
         <tli id="T349" time="396.8517837268561" />
         <tli id="T350" time="397.3251152119489" />
         <tli id="T351" time="397.61688888888887" type="appl" />
         <tli id="T352" time="397.713" type="appl" />
         <tli id="T353" time="398.10577777777775" type="appl" />
         <tli id="T354" time="398.5946666666667" type="appl" />
         <tli id="T355" time="399.08355555555556" type="appl" />
         <tli id="T356" time="399.57244444444444" type="appl" />
         <tli id="T357" time="400.0613333333333" type="appl" />
         <tli id="T358" time="400.55022222222226" type="appl" />
         <tli id="T359" time="401.03911111111114" type="appl" />
         <tli id="T360" time="401.528" type="appl" />
         <tli id="T361" time="402.013" type="appl" />
         <tli id="T1473" />
         <tli id="T1474" />
         <tli id="T362" time="439.128" type="appl" />
         <tli id="T363" time="439.899" type="appl" />
         <tli id="T1475" />
         <tli id="T1476" />
         <tli id="T364" time="443.27" type="appl" />
         <tli id="T365" time="443.528" type="appl" />
         <tli id="T366" time="443.94800000000004" type="appl" />
         <tli id="T367" time="444.368" type="appl" />
         <tli id="T368" time="444.788" type="appl" />
         <tli id="T369" time="445.20799999999997" type="appl" />
         <tli id="T370" time="445.628" type="appl" />
         <tli id="T371" time="446.628" type="appl" />
         <tli id="T372" time="447.44014285714286" type="appl" />
         <tli id="T373" time="448.2522857142857" type="appl" />
         <tli id="T374" time="449.06442857142855" type="appl" />
         <tli id="T375" time="449.8765714285714" type="appl" />
         <tli id="T376" time="450.6887142857143" type="appl" />
         <tli id="T377" time="451.5008571428571" type="appl" />
         <tli id="T378" time="452.10490131065166" />
         <tli id="T379" time="453.67" type="appl" />
         <tli id="T380" time="453.671" type="appl" />
         <tli id="T381" time="454.892" type="appl" />
         <tli id="T382" time="456.1" type="appl" />
         <tli id="T383" time="456.114" type="appl" />
         <tli id="T384" time="456.91749999999996" type="appl" />
         <tli id="T385" time="457.721" type="appl" />
         <tli id="T386" time="458.5245" type="appl" />
         <tli id="T387" time="459.328" type="appl" />
         <tli id="T388" time="459.37" type="appl" />
         <tli id="T389" time="460.49525" type="appl" />
         <tli id="T390" time="461.6205" type="appl" />
         <tli id="T391" time="462.74575" type="appl" />
         <tli id="T392" time="464.0048548443238" />
         <tli id="T393" time="464.660125" type="appl" />
         <tli id="T394" time="465.44925" type="appl" />
         <tli id="T395" time="466.238375" type="appl" />
         <tli id="T396" time="467.02750000000003" type="appl" />
         <tli id="T397" time="467.816625" type="appl" />
         <tli id="T398" time="468.60575" type="appl" />
         <tli id="T399" time="469.394875" type="appl" />
         <tli id="T400" time="470.184" type="appl" />
         <tli id="T401" time="470.971" type="appl" />
         <tli id="T402" time="486.485" type="appl" />
         <tli id="T403" time="486.79809917576654" />
         <tli id="T404" time="487.7645714285714" type="appl" />
         <tli id="T405" time="488.64414285714287" type="appl" />
         <tli id="T406" time="489.5237142857143" type="appl" />
         <tli id="T407" time="490.4032857142857" type="appl" />
         <tli id="T408" time="491.2828571428571" type="appl" />
         <tli id="T409" time="492.16242857142856" type="appl" />
         <tli id="T410" time="493.042" type="appl" />
         <tli id="T411" time="494.628" type="appl" />
         <tli id="T1477" />
         <tli id="T1478" />
         <tli id="T412" time="501.881" type="appl" />
         <tli id="T413" time="503.999" type="appl" />
         <tli id="T1479" />
         <tli id="T1480" />
         <tli id="T414" time="511.014" type="appl" />
         <tli id="T415" time="511.385" type="appl" />
         <tli id="T416" time="511.968875" type="appl" />
         <tli id="T417" time="512.5527500000001" type="appl" />
         <tli id="T418" time="513.136625" type="appl" />
         <tli id="T419" time="513.7205" type="appl" />
         <tli id="T420" time="514.304375" type="appl" />
         <tli id="T421" time="514.88825" type="appl" />
         <tli id="T422" time="515.472125" type="appl" />
         <tli id="T423" time="516.056" type="appl" />
         <tli id="T424" time="516.071" type="appl" />
         <tli id="T425" time="516.6151818181819" type="appl" />
         <tli id="T426" time="517.1593636363637" type="appl" />
         <tli id="T427" time="517.7035454545455" type="appl" />
         <tli id="T428" time="518.2477272727273" type="appl" />
         <tli id="T429" time="518.7919090909091" type="appl" />
         <tli id="T430" time="519.3360909090909" type="appl" />
         <tli id="T431" time="519.8802727272728" type="appl" />
         <tli id="T432" time="520.4244545454545" type="appl" />
         <tli id="T433" time="520.956" type="appl" />
         <tli id="T434" time="520.9686363636364" type="appl" />
         <tli id="T435" time="521.5128181818181" type="appl" />
         <tli id="T436" time="522.057" type="appl" />
         <tli id="T437" time="522.835" type="appl" />
         <tli id="T438" time="524.714" type="appl" />
         <tli id="T439" time="524.827" type="appl" />
         <tli id="T440" time="525.156" type="appl" />
         <tli id="T441" time="525.9606666666666" type="appl" />
         <tli id="T442" time="526.57" type="appl" />
         <tli id="T443" time="526.7653333333334" type="appl" />
         <tli id="T444" time="527.57" type="appl" />
         <tli id="T445" time="528.827" type="appl" />
         <tli id="T1481" />
         <tli id="T1482" />
         <tli id="T446" time="530.399" type="appl" />
         <tli id="T447" time="531.527" type="appl" />
         <tli id="T1483" />
         <tli id="T1484" />
         <tli id="T448" time="536.37" type="appl" />
         <tli id="T449" time="536.4912384698799" />
         <tli id="T450" time="536.979375" type="appl" />
         <tli id="T451" time="537.3597500000001" type="appl" />
         <tli id="T452" time="537.740125" type="appl" />
         <tli id="T453" time="538.1205" type="appl" />
         <tli id="T454" time="538.5008750000001" type="appl" />
         <tli id="T455" time="538.88125" type="appl" />
         <tli id="T456" time="539.2616250000001" type="appl" />
         <tli id="T457" time="539.642" type="appl" />
         <tli id="T458" time="540.37" type="appl" />
         <tli id="T459" time="541.288" type="appl" />
         <tli id="T460" time="542.206" type="appl" />
         <tli id="T461" time="543.124" type="appl" />
         <tli id="T462" time="544.042" type="appl" />
         <tli id="T463" time="545.128" type="appl" />
         <tli id="T464" time="546.1452" type="appl" />
         <tli id="T465" time="547.1624" type="appl" />
         <tli id="T466" time="548.1796" type="appl" />
         <tli id="T467" time="549.1968" type="appl" />
         <tli id="T468" time="550.214" type="appl" />
         <tli id="T469" time="552.799" type="appl" />
         <tli id="T470" time="554.742" type="appl" />
         <tli id="T471" time="556.6850000000001" type="appl" />
         <tli id="T472" time="558.628" type="appl" />
         <tli id="T473" time="558.642" type="appl" />
         <tli id="T474" time="559.492" type="appl" />
         <tli id="T475" time="560.342" type="appl" />
         <tli id="T476" time="560.371" type="appl" />
         <tli id="T477" time="561.171" type="appl" />
         <tli id="T478" time="561.971" type="appl" />
         <tli id="T479" time="562.771" type="appl" />
         <tli id="T480" time="563.571" type="appl" />
         <tli id="T481" time="563.814" type="appl" />
         <tli id="T1485" />
         <tli id="T1486" />
         <tli id="T482" time="574.914" type="appl" />
         <tli id="T483" time="575.028" type="appl" />
         <tli id="T1487" />
         <tli id="T1488" />
         <tli id="T484" time="580.756" type="appl" />
         <tli id="T485" time="580.971" type="appl" />
         <tli id="T486" time="581.3485714285714" type="appl" />
         <tli id="T487" time="581.7261428571429" type="appl" />
         <tli id="T488" time="582.1037142857143" type="appl" />
         <tli id="T489" time="582.4812857142857" type="appl" />
         <tli id="T490" time="582.8588571428571" type="appl" />
         <tli id="T491" time="583.2364285714286" type="appl" />
         <tli id="T492" time="583.614" type="appl" />
         <tli id="T493" time="583.686" type="appl" />
         <tli id="T494" time="584.2537500000001" type="appl" />
         <tli id="T495" time="584.8215" type="appl" />
         <tli id="T496" time="585.38925" type="appl" />
         <tli id="T497" time="585.957" type="appl" />
         <tli id="T498" time="586.52475" type="appl" />
         <tli id="T499" time="587.0925" type="appl" />
         <tli id="T500" time="587.6602499999999" type="appl" />
         <tli id="T501" time="588.228" type="appl" />
         <tli id="T502" time="588.585" type="appl" />
         <tli id="T503" time="589.3826666666666" type="appl" />
         <tli id="T504" time="590.1803333333334" type="appl" />
         <tli id="T505" time="590.9780000000001" type="appl" />
         <tli id="T506" time="591.7756666666667" type="appl" />
         <tli id="T507" time="592.5733333333333" type="appl" />
         <tli id="T508" time="593.371" type="appl" />
         <tli id="T509" time="594.057" type="appl" />
         <tli id="T510" time="594.6855" type="appl" />
         <tli id="T511" time="595.314" type="appl" />
         <tli id="T512" time="595.9425" type="appl" />
         <tli id="T513" time="596.571" type="appl" />
         <tli id="T514" time="597.1995" type="appl" />
         <tli id="T515" time="597.828" type="appl" />
         <tli id="T516" time="598.285" type="appl" />
         <tli id="T517" time="599.144" type="appl" />
         <tli id="T518" time="600.003" type="appl" />
         <tli id="T519" time="600.0086467003644" />
         <tli id="T520" time="600.8106" type="appl" />
         <tli id="T521" time="601.5792" type="appl" />
         <tli id="T522" time="602.3478" type="appl" />
         <tli id="T523" time="603.1164" type="appl" />
         <tli id="T524" time="603.885" type="appl" />
         <tli id="T525" time="604.0" type="appl" />
         <tli id="T526" time="604.6828" type="appl" />
         <tli id="T527" time="605.3656" type="appl" />
         <tli id="T528" time="606.0484" type="appl" />
         <tli id="T529" time="606.7312" type="appl" />
         <tli id="T530" time="607.414" type="appl" />
         <tli id="T531" time="607.427" type="appl" />
         <tli id="T532" time="608.1676666666667" type="appl" />
         <tli id="T533" time="608.9083333333333" type="appl" />
         <tli id="T534" time="609.649" type="appl" />
         <tli id="T535" time="610.3896666666667" type="appl" />
         <tli id="T536" time="611.1303333333333" type="appl" />
         <tli id="T537" time="611.871" type="appl" />
         <tli id="T538" time="611.928" type="appl" />
         <tli id="T539" time="612.4923333333334" type="appl" />
         <tli id="T540" time="613.0566666666666" type="appl" />
         <tli id="T541" time="613.621" type="appl" />
         <tli id="T542" time="614.1853333333333" type="appl" />
         <tli id="T543" time="614.7496666666666" type="appl" />
         <tli id="T544" time="615.4909299959392" />
         <tli id="T545" time="615.971" type="appl" />
         <tli id="T546" time="632.385" type="appl" />
         <tli id="T547" time="633.025" type="appl" />
         <tli id="T548" time="633.665" type="appl" />
         <tli id="T549" time="634.3050000000001" type="appl" />
         <tli id="T550" time="634.945" type="appl" />
         <tli id="T551" time="635.485" type="appl" />
         <tli id="T552" time="635.585" type="appl" />
         <tli id="T553" time="637.728" type="appl" />
         <tli id="T1489" />
         <tli id="T1490" />
         <tli id="T554" time="640.071" type="appl" />
         <tli id="T555" time="640.185" type="appl" />
         <tli id="T556" time="640.8421999999999" type="appl" />
         <tli id="T557" time="641.4993999999999" type="appl" />
         <tli id="T558" time="642.1566" type="appl" />
         <tli id="T559" time="642.8138" type="appl" />
         <tli id="T560" time="643.471" type="appl" />
         <tli id="T561" time="644.013" type="appl" />
         <tli id="T562" time="645.884" type="appl" />
         <tli id="T563" time="647.67" type="appl" />
         <tli id="T564" time="648.9345" type="appl" />
         <tli id="T565" time="650.199" type="appl" />
         <tli id="T566" time="651.899" type="appl" />
         <tli id="T567" time="652.559" type="appl" />
         <tli id="T568" time="653.2189999999999" type="appl" />
         <tli id="T569" time="653.879" type="appl" />
         <tli id="T570" time="654.539" type="appl" />
         <tli id="T571" time="655.5041070878721" />
         <tli id="T572" time="655.77" type="appl" />
         <tli id="T573" time="655.798" type="appl" />
         <tli id="T1491" />
         <tli id="T1492" />
         <tli id="T574" time="656.984" type="appl" />
         <tli id="T575" time="656.9843333333333" type="appl" />
         <tli id="T576" time="658.1986666666667" type="appl" />
         <tli id="T577" time="659.413" type="appl" />
         <tli id="T578" time="659.613" type="appl" />
         <tli id="T1493" />
         <tli id="T1494" />
         <tli id="T579" time="664.199" type="appl" />
         <tli id="T580" time="664.4011428571429" type="appl" />
         <tli id="T581" time="664.6032857142857" type="appl" />
         <tli id="T582" time="664.8054285714286" type="appl" />
         <tli id="T583" time="665.0075714285714" type="appl" />
         <tli id="T584" time="665.2097142857143" type="appl" />
         <tli id="T585" time="665.4118571428571" type="appl" />
         <tli id="T586" time="665.614" type="appl" />
         <tli id="T587" time="665.67" type="appl" />
         <tli id="T1495" />
         <tli id="T1496" />
         <tli id="T588" time="666.156" type="appl" />
         <tli id="T589" time="666.414" type="appl" />
         <tli id="T590" time="667.0707285897888" />
         <tli id="T591" time="667.099" type="appl" />
         <tli id="T592" time="667.514" type="appl" />
         <tli id="T593" time="668.714" type="appl" />
         <tli id="T594" time="670.185" type="appl" />
         <tli id="T595" time="670.442" type="appl" />
         <tli id="T1497" />
         <tli id="T1498" />
         <tli id="T596" time="672.542" type="appl" />
         <tli id="T597" time="672.685" type="appl" />
         <tli id="T598" time="673.2027499999999" type="appl" />
         <tli id="T599" time="673.7204999999999" type="appl" />
         <tli id="T600" time="674.23825" type="appl" />
         <tli id="T601" time="674.756" type="appl" />
         <tli id="T602" time="675.527" type="appl" />
         <tli id="T603" time="676.1556666666667" type="appl" />
         <tli id="T604" time="676.7843333333334" type="appl" />
         <tli id="T605" time="677.413" type="appl" />
         <tli id="T606" time="677.527" type="appl" />
         <tli id="T1499" />
         <tli id="T1500" />
         <tli id="T607" time="679.227" type="appl" />
         <tli id="T608" time="680.228" type="appl" />
         <tli id="T609" time="680.5822499999999" type="appl" />
         <tli id="T610" time="680.9365" type="appl" />
         <tli id="T611" time="681.29075" type="appl" />
         <tli id="T612" time="681.645" type="appl" />
         <tli id="T613" time="681.99925" type="appl" />
         <tli id="T614" time="682.3534999999999" type="appl" />
         <tli id="T615" time="682.70775" type="appl" />
         <tli id="T616" time="683.4506646302552" />
         <tli id="T617" time="683.928" type="appl" />
         <tli id="T618" time="684.7" type="appl" />
         <tli id="T619" time="685.1101428571429" type="appl" />
         <tli id="T620" time="685.37" type="appl" />
         <tli id="T621" time="685.5202857142857" type="appl" />
         <tli id="T622" time="685.9304285714286" type="appl" />
         <tli id="T623" time="686.3405714285715" type="appl" />
         <tli id="T624" time="686.7507142857144" type="appl" />
         <tli id="T625" time="687.1608571428571" type="appl" />
         <tli id="T626" time="687.571" type="appl" />
         <tli id="T627" time="687.9811428571429" type="appl" />
         <tli id="T628" time="688.3912857142857" type="appl" />
         <tli id="T629" time="688.8014285714286" type="appl" />
         <tli id="T630" time="689.2115714285715" type="appl" />
         <tli id="T631" time="689.6217142857143" type="appl" />
         <tli id="T632" time="690.0318571428571" type="appl" />
         <tli id="T633" time="690.442" type="appl" />
         <tli id="T634" time="690.828" type="appl" />
         <tli id="T635" time="691.4683333333332" type="appl" />
         <tli id="T636" time="692.1086666666666" type="appl" />
         <tli id="T637" time="692.749" type="appl" />
         <tli id="T638" time="693.3893333333333" type="appl" />
         <tli id="T639" time="694.0296666666666" type="appl" />
         <tli id="T640" time="694.67" type="appl" />
         <tli id="T641" time="695.2372852731305" />
         <tli id="T642" time="696.042" type="appl" />
         <tli id="T643" time="696.556" type="appl" />
         <tli id="T644" time="696.785" type="appl" />
         <tli id="T645" time="697.4233333333333" type="appl" />
         <tli id="T646" time="698.0616666666667" type="appl" />
         <tli id="T647" time="698.628" type="appl" />
         <tli id="T648" time="699.0328333333334" type="intp" />
         <tli id="T649" time="699.4376666666667" type="appl" />
         <tli id="T650" time="700.1905992650176" />
         <tli id="T651" time="700.277265593274" />
         <tli id="T652" time="700.4639315310571" />
         <tli id="T653" time="700.5512" type="appl" />
         <tli id="T654" time="700.9482999999999" type="appl" />
         <tli id="T655" time="701.0372626256767" />
         <tli id="T656" time="701.3453999999999" type="appl" />
         <tli id="T657" time="701.7425" type="appl" />
         <tli id="T658" time="702.1396" type="appl" />
         <tli id="T659" time="702.5367" type="appl" />
         <tli id="T660" time="702.9337999999999" type="appl" />
         <tli id="T661" time="703.171" type="appl" />
         <tli id="T662" time="703.3308999999999" type="appl" />
         <tli id="T663" time="703.728" type="appl" />
         <tli id="T664" time="703.7511111111112" type="appl" />
         <tli id="T665" time="704.3312222222223" type="appl" />
         <tli id="T666" time="704.9113333333333" type="appl" />
         <tli id="T667" time="705.4914444444445" type="appl" />
         <tli id="T668" time="706.0715555555556" type="appl" />
         <tli id="T669" time="706.6516666666668" type="appl" />
         <tli id="T670" time="707.0772390410867" />
         <tli id="T671" time="707.2317777777778" type="appl" />
         <tli id="T672" time="707.8118888888889" type="appl" />
         <tli id="T673" time="707.8425" type="appl" />
         <tli id="T674" time="708.392" type="appl" />
         <tli id="T675" time="708.657" type="appl" />
         <tli id="T676" time="708.999" type="appl" />
         <tli id="T677" time="709.7358571428572" type="appl" />
         <tli id="T678" time="710.4727142857143" type="appl" />
         <tli id="T679" time="711.2095714285715" type="appl" />
         <tli id="T680" time="711.9464285714286" type="appl" />
         <tli id="T681" time="712.6832857142857" type="appl" />
         <tli id="T682" time="713.4201428571429" type="appl" />
         <tli id="T683" time="714.157" type="appl" />
         <tli id="T684" time="714.899" type="appl" />
         <tli id="T685" time="715.5955" type="appl" />
         <tli id="T686" time="716.2919999999999" type="appl" />
         <tli id="T687" time="716.9884999999999" type="appl" />
         <tli id="T688" time="717.685" type="appl" />
         <tli id="T689" time="718.185" type="appl" />
         <tli id="T690" time="718.8154166666666" type="appl" />
         <tli id="T691" time="719.4458333333333" type="appl" />
         <tli id="T692" time="720.07625" type="appl" />
         <tli id="T693" time="720.7066666666666" type="appl" />
         <tli id="T694" time="721.3370833333333" type="appl" />
         <tli id="T695" time="721.9675" type="appl" />
         <tli id="T696" time="722.5979166666666" type="appl" />
         <tli id="T697" time="723.2283333333334" type="appl" />
         <tli id="T698" time="723.85875" type="appl" />
         <tli id="T699" time="724.4891666666666" type="appl" />
         <tli id="T700" time="725.1195833333334" type="appl" />
         <tli id="T701" time="725.75" type="appl" />
         <tli id="T702" time="725.813" type="appl" />
         <tli id="T703" time="726.325" type="appl" />
         <tli id="T704" time="726.856" type="appl" />
         <tli id="T1501" />
         <tli id="T1502" />
         <tli id="T705" time="744.928" type="appl" />
         <tli id="T706" time="745.356" type="appl" />
         <tli id="T1503" />
         <tli id="T1504" />
         <tli id="T707" time="748.363" type="appl" />
         <tli id="T708" time="749.299" type="appl" />
         <tli id="T709" time="749.8255714285714" type="appl" />
         <tli id="T710" time="750.3521428571429" type="appl" />
         <tli id="T711" time="750.8787142857143" type="appl" />
         <tli id="T712" time="751.4052857142857" type="appl" />
         <tli id="T713" time="751.9318571428571" type="appl" />
         <tli id="T714" time="752.4584285714286" type="appl" />
         <tli id="T715" time="752.985" type="appl" />
         <tli id="T716" time="753.356" type="appl" />
         <tli id="T717" time="753.942" type="appl" />
         <tli id="T718" time="754.528" type="appl" />
         <tli id="T719" time="756.37" type="appl" />
         <tli id="T1505" />
         <tli id="T1506" />
         <tli id="T720" time="762.557" type="appl" />
         <tli id="T721" time="765.013" type="appl" />
         <tli id="T722" time="768.042" type="appl" />
         <tli id="T723" time="768.051" type="appl" />
         <tli id="T724" time="768.6065" type="appl" />
         <tli id="T725" time="769.171" type="appl" />
         <tli id="T726" time="769.7355" type="appl" />
         <tli id="T727" time="770.3" type="appl" />
         <tli id="T728" time="770.957" type="appl" />
         <tli id="T729" time="772.365875" type="appl" />
         <tli id="T730" time="773.77475" type="appl" />
         <tli id="T731" time="775.183625" type="appl" />
         <tli id="T732" time="776.5925" type="appl" />
         <tli id="T733" time="778.0013749999999" type="appl" />
         <tli id="T734" time="779.4102499999999" type="appl" />
         <tli id="T735" time="780.819125" type="appl" />
         <tli id="T736" time="782.228" type="appl" />
         <tli id="T737" time="783.014" type="appl" />
         <tli id="T738" time="783.789" type="appl" />
         <tli id="T739" time="784.5640000000001" type="appl" />
         <tli id="T740" time="785.339" type="appl" />
         <tli id="T741" time="786.114" type="appl" />
         <tli id="T742" time="787.057" type="appl" />
         <tli id="T1507" />
         <tli id="T1508" />
         <tli id="T743" time="794.055" type="appl" />
         <tli id="T744" time="794.914" type="appl" />
         <tli id="T1509" />
         <tli id="T1510" />
         <tli id="T745" time="797.8" type="appl" />
         <tli id="T746" time="798.454" type="appl" />
         <tli id="T747" time="799.108" type="appl" />
         <tli id="T748" time="799.762" type="appl" />
         <tli id="T749" time="800.4159999999999" type="appl" />
         <tli id="T750" time="801.07" type="appl" />
         <tli id="T751" time="801.724" type="appl" />
         <tli id="T752" time="802.378" type="appl" />
         <tli id="T753" time="803.032" type="appl" />
         <tli id="T754" time="803.686" type="appl" />
         <tli id="T755" time="804.3430000000001" type="appl" />
         <tli id="T756" time="805.0" type="appl" />
         <tli id="T757" time="805.657" type="appl" />
         <tli id="T758" time="812.405" type="appl" />
         <tli id="T759" time="812.954" type="appl" />
         <tli id="T760" time="813.5029999999999" type="appl" />
         <tli id="T761" time="814.052" type="appl" />
         <tli id="T762" time="814.601" type="appl" />
         <tli id="T763" time="814.648" type="appl" />
         <tli id="T764" time="815.224" type="appl" />
         <tli id="T765" time="815.8" type="appl" />
         <tli id="T766" time="816.376" type="appl" />
         <tli id="T767" time="817.133" type="appl" />
         <tli id="T768" time="817.933" type="appl" />
         <tli id="T769" time="818.733" type="appl" />
         <tli id="T770" time="819.033" type="appl" />
         <tli id="T771" time="819.893" type="appl" />
         <tli id="T772" time="820.753" type="appl" />
         <tli id="T773" time="821.6129999999999" type="appl" />
         <tli id="T774" time="822.473" type="appl" />
         <tli id="T775" time="823.333" type="appl" />
         <tli id="T776" time="823.519" type="appl" />
         <tli id="T777" time="824.5725" type="appl" />
         <tli id="T778" time="825.626" type="appl" />
         <tli id="T779" time="826.6795" type="appl" />
         <tli id="T780" time="827.733" type="appl" />
         <tli id="T781" time="828.9259999999999" type="appl" />
         <tli id="T782" time="830.119" type="appl" />
         <tli id="T783" time="831.176" type="appl" />
         <tli id="T784" time="832.848" type="appl" />
         <tli id="T785" time="833.071" type="appl" />
         <tli id="T786" time="833.9452" type="appl" />
         <tli id="T787" time="834.8194" type="appl" />
         <tli id="T788" time="835.6936000000001" type="appl" />
         <tli id="T789" time="836.5678" type="appl" />
         <tli id="T790" time="837.442" type="appl" />
         <tli id="T791" time="838.828" type="appl" />
         <tli id="T792" time="839.9495" type="appl" />
         <tli id="T793" time="841.071" type="appl" />
         <tli id="T794" time="841.571" type="appl" />
         <tli id="T795" time="842.633" type="appl" />
         <tli id="T796" time="843.6949999999999" type="appl" />
         <tli id="T797" time="844.757" type="appl" />
         <tli id="T798" time="846.028" type="appl" />
         <tli id="T1511" />
         <tli id="T1512" />
         <tli id="T799" time="860.091" type="appl" />
         <tli id="T800" time="860.485" type="appl" />
         <tli id="T801" time="861.278" type="appl" />
         <tli id="T802" time="862.071" type="appl" />
         <tli id="T803" time="862.864" type="appl" />
         <tli id="T804" time="863.657" type="appl" />
         <tli id="T805" time="863.7166274036423" />
         <tli id="T806" time="865.32075" type="appl" />
         <tli id="T807" time="866.8135" type="appl" />
         <tli id="T808" time="868.30625" type="appl" />
         <tli id="T809" time="869.799" type="appl" />
         <tli id="T810" time="869.814" type="appl" />
         <tli id="T811" time="870.4756666666666" type="appl" />
         <tli id="T812" time="871.1373333333333" type="appl" />
         <tli id="T813" time="871.799" type="appl" />
         <tli id="T814" time="871.971" type="appl" />
         <tli id="T815" time="872.7115" type="appl" />
         <tli id="T816" time="873.452" type="appl" />
         <tli id="T817" time="874.1925" type="appl" />
         <tli id="T818" time="874.933" type="appl" />
         <tli id="T819" time="875.6735" type="appl" />
         <tli id="T820" time="876.414" type="appl" />
         <tli id="T821" time="877.1743333333333" type="appl" />
         <tli id="T822" time="877.9346666666667" type="appl" />
         <tli id="T823" time="878.6949999999999" type="appl" />
         <tli id="T824" time="879.4553333333333" type="appl" />
         <tli id="T825" time="880.2156666666666" type="appl" />
         <tli id="T826" time="880.976" type="appl" />
         <tli id="T827" time="881.7363333333333" type="appl" />
         <tli id="T828" time="882.4966666666667" type="appl" />
         <tli id="T829" time="883.257" type="appl" />
         <tli id="T830" time="884.943211185834" />
         <tli id="T831" time="886.034" type="appl" />
         <tli id="T832" time="886.9540000000001" type="appl" />
         <tli id="T833" time="887.874" type="appl" />
         <tli id="T834" time="888.7940000000001" type="appl" />
         <tli id="T835" time="889.714" type="appl" />
         <tli id="T836" time="890.614" type="appl" />
         <tli id="T837" time="891.2283333333334" type="appl" />
         <tli id="T838" time="891.8426666666667" type="appl" />
         <tli id="T839" time="892.457" type="appl" />
         <tli id="T840" time="894.028" type="appl" />
         <tli id="T841" time="894.5261875" type="appl" />
         <tli id="T842" time="895.024375" type="appl" />
         <tli id="T843" time="895.5225625" type="appl" />
         <tli id="T844" time="896.02075" type="appl" />
         <tli id="T845" time="896.5189375" type="appl" />
         <tli id="T846" time="897.0171250000001" type="appl" />
         <tli id="T847" time="897.5153125" type="appl" />
         <tli id="T848" time="898.0135" type="appl" />
         <tli id="T849" time="898.5116875" type="appl" />
         <tli id="T850" time="899.009875" type="appl" />
         <tli id="T851" time="899.5080625" type="appl" />
         <tli id="T852" time="900.00625" type="appl" />
         <tli id="T853" time="900.5044375" type="appl" />
         <tli id="T854" time="901.0026250000001" type="appl" />
         <tli id="T855" time="901.5008125" type="appl" />
         <tli id="T856" time="902.1898105088649" />
         <tli id="T857" time="902.471" type="appl" />
         <tli id="T858" time="903.1086363636364" type="appl" />
         <tli id="T859" time="903.7462727272728" type="appl" />
         <tli id="T860" time="904.3839090909091" type="appl" />
         <tli id="T861" time="905.0215454545455" type="appl" />
         <tli id="T862" time="905.6591818181819" type="appl" />
         <tli id="T863" time="906.2968181818181" type="appl" />
         <tli id="T864" time="906.9344545454545" type="appl" />
         <tli id="T865" time="907.5720909090909" type="appl" />
         <tli id="T866" time="908.2097272727273" type="appl" />
         <tli id="T867" time="908.8473636363636" type="appl" />
         <tli id="T868" time="909.7764475516207" />
         <tli id="T869" time="909.841" type="appl" />
         <tli id="T870" time="910.35" type="appl" />
         <tli id="T871" time="910.963" type="appl" />
         <tli id="T872" time="911.576" type="appl" />
         <tli id="T873" time="912.189" type="appl" />
         <tli id="T874" time="912.802" type="appl" />
         <tli id="T875" time="913.415" type="appl" />
         <tli id="T876" time="914.028" type="appl" />
         <tli id="T877" time="914.641" type="appl" />
         <tli id="T878" time="923.355" type="appl" />
         <tli id="T879" time="924.0146666666667" type="appl" />
         <tli id="T880" time="924.6743333333334" type="appl" />
         <tli id="T881" time="925.3340000000001" type="appl" />
         <tli id="T882" time="925.9936666666666" type="appl" />
         <tli id="T883" time="926.6533333333333" type="appl" />
         <tli id="T884" time="927.313" type="appl" />
         <tli id="T885" time="972.314" type="appl" />
         <tli id="T886" time="972.4" type="appl" />
         <tli id="T1513" />
         <tli id="T1514" />
         <tli id="T887" time="975.673" type="appl" />
         <tli id="T888" time="976.51225" type="appl" />
         <tli id="T889" time="977.3515" type="appl" />
         <tli id="T890" time="978.19075" type="appl" />
         <tli id="T891" time="979.03" type="appl" />
         <tli id="T892" time="980.744" type="appl" />
         <tli id="T893" time="981.7327777777778" type="appl" />
         <tli id="T894" time="982.7215555555556" type="appl" />
         <tli id="T895" time="983.7103333333333" type="appl" />
         <tli id="T896" time="984.6991111111112" type="appl" />
         <tli id="T897" time="985.6878888888889" type="appl" />
         <tli id="T898" time="986.6766666666667" type="appl" />
         <tli id="T899" time="987.6654444444445" type="appl" />
         <tli id="T900" time="988.6542222222223" type="appl" />
         <tli id="T901" time="989.643" type="appl" />
         <tli id="T902" time="989.985" type="appl" />
         <tli id="T903" time="991.0771111111111" type="appl" />
         <tli id="T904" time="992.1692222222222" type="appl" />
         <tli id="T905" time="993.2613333333334" type="appl" />
         <tli id="T906" time="994.3534444444444" type="appl" />
         <tli id="T907" time="995.4455555555555" type="appl" />
         <tli id="T908" time="996.5376666666666" type="appl" />
         <tli id="T909" time="997.6297777777778" type="appl" />
         <tli id="T910" time="998.7218888888889" type="appl" />
         <tli id="T911" time="999.814" type="appl" />
         <tli id="T912" time="999.9" type="appl" />
         <tli id="T913" time="1000.66" type="appl" />
         <tli id="T914" time="1001.42" type="appl" />
         <tli id="T915" time="1002.1800000000001" type="appl" />
         <tli id="T916" time="1002.94" type="appl" />
         <tli id="T917" time="1003.7" type="appl" />
         <tli id="T918" time="1005.071" type="appl" />
         <tli id="T919" time="1005.9855" type="appl" />
         <tli id="T920" time="1006.9" type="appl" />
         <tli id="T921" time="1007.071" type="appl" />
         <tli id="T922" time="1008.1613333333333" type="appl" />
         <tli id="T923" time="1009.2516666666667" type="appl" />
         <tli id="T924" time="1010.342" type="appl" />
         <tli id="T925" time="1010.8" type="appl" />
         <tli id="T926" time="1011.53" type="appl" />
         <tli id="T927" time="1012.26" type="appl" />
         <tli id="T928" time="1012.99" type="appl" />
         <tli id="T929" time="1013.72" type="appl" />
         <tli id="T930" time="1014.45" type="appl" />
         <tli id="T931" time="1015.18" type="appl" />
         <tli id="T932" time="1015.91" type="appl" />
         <tli id="T933" time="1016.64" type="appl" />
         <tli id="T934" time="1017.37" type="appl" />
         <tli id="T935" time="1018.1" type="appl" />
         <tli id="T936" time="1018.857" type="appl" />
         <tli id="T937" time="1020.1713333333333" type="appl" />
         <tli id="T938" time="1021.4856666666666" type="appl" />
         <tli id="T939" time="1023.3693373332678" />
         <tli id="T940" time="1023.942" type="appl" />
         <tli id="T1515" />
         <tli id="T1516" />
         <tli id="T941" time="1049.399" type="appl" />
         <tli id="T942" time="1049.455" type="appl" />
         <tli id="T943" time="1050.099" type="appl" />
         <tli id="T944" time="1050.398" type="appl" />
         <tli id="T945" time="1050.4025" type="appl" />
         <tli id="T946" time="1050.9158964375445" />
         <tli id="T947" time="1050.9825628438955" />
         <tli id="T948" time="1051.255895109935" />
         <tli id="T949" time="1051.313" type="appl" />
         <tli id="T950" time="1051.70225" type="appl" />
         <tli id="T951" time="1052.0915" type="appl" />
         <tli id="T952" time="1052.48075" type="appl" />
         <tli id="T953" time="1052.499" type="appl" />
         <tli id="T954" time="1052.8492222217262" />
         <tli id="T955" time="1052.87" type="appl" />
         <tli id="T956" time="1053.2275" type="appl" />
         <tli id="T957" time="1053.699" type="appl" />
         <tli id="T958" time="1053.799" type="appl" />
         <tli id="T959" time="1053.842" type="appl" />
         <tli id="T960" time="1054.28" type="appl" />
         <tli id="T961" time="1054.5052857142857" type="appl" />
         <tli id="T962" time="1054.761" type="appl" />
         <tli id="T963" time="1055.1685714285716" type="appl" />
         <tli id="T964" time="1055.242" type="appl" />
         <tli id="T965" time="1055.8318571428572" type="appl" />
         <tli id="T966" time="1056.4951428571428" type="appl" />
         <tli id="T967" time="1057.1584285714287" type="appl" />
         <tli id="T968" time="1057.8217142857143" type="appl" />
         <tli id="T969" time="1058.4850000000001" type="appl" />
         <tli id="T970" time="1059.1482857142857" type="appl" />
         <tli id="T971" time="1059.8115714285714" type="appl" />
         <tli id="T972" time="1060.4748571428572" type="appl" />
         <tli id="T973" time="1061.1381428571428" type="appl" />
         <tli id="T974" time="1061.8014285714285" type="appl" />
         <tli id="T975" time="1062.4647142857143" type="appl" />
         <tli id="T976" time="1063.128" type="appl" />
         <tli id="T977" time="1063.5245" type="appl" />
         <tli id="T978" time="1063.9209999999998" type="appl" />
         <tli id="T979" time="1064.3174999999999" type="appl" />
         <tli id="T980" time="1064.714" type="appl" />
         <tli id="T981" time="1064.743" type="appl" />
         <tli id="T982" time="1069.5091571688674" />
         <tli id="T983" time="1070.421" type="appl" />
         <tli id="T984" time="1070.9024850616056" />
         <tli id="T985" time="1078.771" type="appl" />
         <tli id="T986" time="1078.957" type="appl" />
         <tli id="T1517" />
         <tli id="T1518" />
         <tli id="T987" time="1082.243" type="appl" />
         <tli id="T988" time="1082.928" type="appl" />
         <tli id="T989" time="1083.5766" type="appl" />
         <tli id="T990" time="1084.2252" type="appl" />
         <tli id="T991" time="1084.8738" type="appl" />
         <tli id="T992" time="1085.5224" type="appl" />
         <tli id="T993" time="1086.171" type="appl" />
         <tli id="T994" time="1086.8196" type="appl" />
         <tli id="T995" time="1087.4682" type="appl" />
         <tli id="T996" time="1088.1168" type="appl" />
         <tli id="T997" time="1088.7654" type="appl" />
         <tli id="T998" time="1089.414" type="appl" />
         <tli id="T999" time="1091.543" type="appl" />
         <tli id="T1000" time="1092.55" type="appl" />
         <tli id="T1001" time="1093.557" type="appl" />
         <tli id="T1002" time="1094.257" type="appl" />
         <tli id="T1003" time="1094.6315555555557" type="appl" />
         <tli id="T1004" time="1095.006111111111" type="appl" />
         <tli id="T1005" time="1095.3806666666667" type="appl" />
         <tli id="T1006" time="1095.7552222222223" type="appl" />
         <tli id="T1007" time="1096.1297777777777" type="appl" />
         <tli id="T1008" time="1096.5043333333333" type="appl" />
         <tli id="T1009" time="1096.878888888889" type="appl" />
         <tli id="T1010" time="1097.2534444444443" type="appl" />
         <tli id="T1011" time="1097.628" type="appl" />
         <tli id="T1012" time="1097.643" type="appl" />
         <tli id="T1013" time="1098.685" type="appl" />
         <tli id="T1014" time="1099.0541666666666" type="appl" />
         <tli id="T1015" time="1099.4233333333334" type="appl" />
         <tli id="T1016" time="1099.7925" type="appl" />
         <tli id="T1017" time="1100.1616666666666" type="appl" />
         <tli id="T1018" time="1100.5308333333335" type="appl" />
         <tli id="T1019" time="1101.0623672948452" />
         <tli id="T1020" time="1101.414" type="appl" />
         <tli id="T1021" time="1101.971" type="appl" />
         <tli id="T1022" time="1102.0978695652175" type="appl" />
         <tli id="T1023" time="1102.7817391304347" type="appl" />
         <tli id="T1024" time="1103.4656086956522" type="appl" />
         <tli id="T1025" time="1104.1494782608695" type="appl" />
         <tli id="T1026" time="1104.833347826087" type="appl" />
         <tli id="T1027" time="1105.5172173913043" type="appl" />
         <tli id="T1028" time="1106.2010869565217" type="appl" />
         <tli id="T1029" time="1106.8849565217392" type="appl" />
         <tli id="T1030" time="1107.5688260869565" type="appl" />
         <tli id="T1031" time="1108.252695652174" type="appl" />
         <tli id="T1032" time="1108.9365652173913" type="appl" />
         <tli id="T1033" time="1109.6204347826088" type="appl" />
         <tli id="T1034" time="1110.304304347826" type="appl" />
         <tli id="T1035" time="1110.9881739130435" type="appl" />
         <tli id="T1036" time="1111.6720434782608" type="appl" />
         <tli id="T1037" time="1112.3559130434783" type="appl" />
         <tli id="T1038" time="1113.0397826086958" type="appl" />
         <tli id="T1039" time="1113.723652173913" type="appl" />
         <tli id="T1040" time="1114.4075217391305" type="appl" />
         <tli id="T1041" time="1114.428" type="appl" />
         <tli id="T1042" time="1114.962" type="appl" />
         <tli id="T1043" time="1115.0913913043478" type="appl" />
         <tli id="T1044" time="1115.496" type="appl" />
         <tli id="T1045" time="1115.7752608695653" type="appl" />
         <tli id="T1046" time="1115.9336666666668" type="appl" />
         <tli id="T1047" time="1116.3713333333333" type="appl" />
         <tli id="T1048" time="1116.4591304347825" type="appl" />
         <tli id="T1049" time="1116.809" type="appl" />
         <tli id="T1050" time="1117.143" type="appl" />
         <tli id="T1051" time="1117.928" type="appl" />
         <tli id="T1052" time="1118.369181818182" type="appl" />
         <tli id="T1053" time="1118.8103636363637" type="appl" />
         <tli id="T1054" time="1119.2515454545455" type="appl" />
         <tli id="T1055" time="1119.6927272727273" type="appl" />
         <tli id="T1056" time="1120.1339090909091" type="appl" />
         <tli id="T1057" time="1120.575090909091" type="appl" />
         <tli id="T1058" time="1121.0162727272727" type="appl" />
         <tli id="T1059" time="1121.4574545454545" type="appl" />
         <tli id="T1060" time="1121.8986363636363" type="appl" />
         <tli id="T1061" time="1122.3398181818181" type="appl" />
         <tli id="T1062" time="1122.781" type="appl" />
         <tli id="T1063" time="1123.222181818182" type="appl" />
         <tli id="T1064" time="1123.6633636363638" type="appl" />
         <tli id="T1065" time="1124.1045454545456" type="appl" />
         <tli id="T1066" time="1124.5457272727274" type="appl" />
         <tli id="T1067" time="1124.9869090909092" type="appl" />
         <tli id="T1068" time="1125.428090909091" type="appl" />
         <tli id="T1069" time="1125.8692727272728" type="appl" />
         <tli id="T1070" time="1126.3104545454546" type="appl" />
         <tli id="T1071" time="1126.7516363636364" type="appl" />
         <tli id="T1072" time="1127.1928181818182" type="appl" />
         <tli id="T1073" time="1127.2089318657486" />
         <tli id="T1074" time="1127.8289294448139" />
         <tli id="T1075" time="1128.082261788948" />
         <tli id="T1076" time="1128.244625" type="appl" />
         <tli id="T1077" time="1128.3035" type="appl" />
         <tli id="T1078" time="1128.7069999999999" type="appl" />
         <tli id="T1079" time="1128.78925" type="appl" />
         <tli id="T1080" time="1129.1105" type="appl" />
         <tli id="T1081" time="1129.333875" type="appl" />
         <tli id="T1082" time="1129.41558991597" />
         <tli id="T1083" time="1129.8785" type="appl" />
         <tli id="T1084" time="1130.423125" type="appl" />
         <tli id="T1085" time="1130.96775" type="appl" />
         <tli id="T1086" time="1131.512375" type="appl" />
         <tli id="T1087" time="1132.057" type="appl" />
         <tli id="T1088" time="1132.185" type="appl" />
         <tli id="T1089" time="1132.68145" type="appl" />
         <tli id="T1090" time="1133.1779" type="appl" />
         <tli id="T1091" time="1133.67435" type="appl" />
         <tli id="T1092" time="1134.1707999999999" type="appl" />
         <tli id="T1093" time="1134.66725" type="appl" />
         <tli id="T1094" time="1135.1637" type="appl" />
         <tli id="T1095" time="1135.66015" type="appl" />
         <tli id="T1096" time="1136.1566" type="appl" />
         <tli id="T1097" time="1136.65305" type="appl" />
         <tli id="T1098" time="1137.1495" type="appl" />
         <tli id="T1099" time="1137.64595" type="appl" />
         <tli id="T1100" time="1138.1424" type="appl" />
         <tli id="T1101" time="1138.63885" type="appl" />
         <tli id="T1102" time="1139.1353" type="appl" />
         <tli id="T1103" time="1139.63175" type="appl" />
         <tli id="T1104" time="1140.1282" type="appl" />
         <tli id="T1105" time="1140.62465" type="appl" />
         <tli id="T1106" time="1141.1211" type="appl" />
         <tli id="T1107" time="1141.61755" type="appl" />
         <tli id="T1108" time="1142.114" type="appl" />
         <tli id="T1109" time="1142.2" type="appl" />
         <tli id="T1110" time="1142.757142857143" type="appl" />
         <tli id="T1111" time="1143.3142857142857" type="appl" />
         <tli id="T1112" time="1143.8714285714286" type="appl" />
         <tli id="T1113" time="1144.4285714285713" type="appl" />
         <tli id="T1114" time="1144.9857142857143" type="appl" />
         <tli id="T1115" time="1145.542857142857" type="appl" />
         <tli id="T1116" time="1146.1" type="appl" />
         <tli id="T1117" time="1146.842" type="appl" />
         <tli id="T1118" time="1147.4106000000002" type="appl" />
         <tli id="T1119" time="1147.9792" type="appl" />
         <tli id="T1120" time="1148.5478" type="appl" />
         <tli id="T1121" time="1149.1163999999999" type="appl" />
         <tli id="T1122" time="1149.685" type="appl" />
         <tli id="T1123" time="1150.228" type="appl" />
         <tli id="T1124" time="1150.771" type="appl" />
         <tli id="T1125" time="1151.3139999999999" type="appl" />
         <tli id="T1126" time="1151.857" type="appl" />
         <tli id="T1127" time="1152.457" type="appl" />
         <tli id="T1128" time="1154.337" type="appl" />
         <tli id="T1129" time="1154.7646666666667" type="appl" />
         <tli id="T1130" time="1155.1923333333334" type="appl" />
         <tli id="T1131" time="1155.62" type="appl" />
         <tli id="T1132" time="1156.0476666666666" type="appl" />
         <tli id="T1133" time="1156.4753333333333" type="appl" />
         <tli id="T1134" time="1156.903" type="appl" />
         <tli id="T1135" time="1157.428" type="appl" />
         <tli id="T1136" time="1157.471" type="appl" />
         <tli id="T1137" time="1158.4495000000002" type="appl" />
         <tli id="T1138" time="1159.471" type="appl" />
         <tli id="T1139" time="1160.471" type="appl" />
         <tli id="T1140" time="1161.2097857142858" type="appl" />
         <tli id="T1141" time="1161.9485714285715" type="appl" />
         <tli id="T1142" time="1162.687357142857" type="appl" />
         <tli id="T1143" time="1163.4261428571428" type="appl" />
         <tli id="T1144" time="1164.1649285714286" type="appl" />
         <tli id="T1145" time="1164.9037142857144" type="appl" />
         <tli id="T1146" time="1165.6425" type="appl" />
         <tli id="T1147" time="1166.3812857142857" type="appl" />
         <tli id="T1148" time="1167.1200714285715" type="appl" />
         <tli id="T1149" time="1167.8588571428572" type="appl" />
         <tli id="T1150" time="1168.597642857143" type="appl" />
         <tli id="T1151" time="1169.3364285714285" type="appl" />
         <tli id="T1152" time="1170.0752142857143" type="appl" />
         <tli id="T1153" time="1170.814" type="appl" />
         <tli id="T1154" time="1171.885" type="appl" />
         <tli id="T1155" time="1174.785" type="appl" />
         <tli id="T1156" time="1175.528" type="appl" />
         <tli id="T1157" time="1176.271" type="appl" />
         <tli id="T1158" time="1177.014" type="appl" />
         <tli id="T1159" time="1224.671" type="appl" />
         <tli id="T1160" time="1224.885" type="appl" />
         <tli id="T1161" time="1225.4931428571429" type="appl" />
         <tli id="T1162" time="1226.1012857142857" type="appl" />
         <tli id="T1163" time="1226.7094285714286" type="appl" />
         <tli id="T1164" time="1227.3175714285715" type="appl" />
         <tli id="T1165" time="1227.9257142857143" type="appl" />
         <tli id="T1166" time="1228.057" type="appl" />
         <tli id="T1167" time="1228.5338571428572" type="appl" />
         <tli id="T1168" time="1229.142" type="appl" />
         <tli id="T1169" time="1237.228" type="appl" />
         <tli id="T1170" time="1237.4" type="appl" />
         <tli id="T1519" />
         <tli id="T1520" />
         <tli id="T1171" time="1242.6" type="appl" />
         <tli id="T1172" time="1243.842" type="appl" />
         <tli id="T1173" time="1244.3449" type="appl" />
         <tli id="T1174" time="1244.8478" type="appl" />
         <tli id="T1175" time="1245.3507000000002" type="appl" />
         <tli id="T1176" time="1245.8536000000001" type="appl" />
         <tli id="T1177" time="1246.3565" type="appl" />
         <tli id="T1178" time="1246.8594" type="appl" />
         <tli id="T1179" time="1247.3623" type="appl" />
         <tli id="T1180" time="1247.8652000000002" type="appl" />
         <tli id="T1181" time="1248.3681000000001" type="appl" />
         <tli id="T1182" time="1248.871" type="appl" />
         <tli id="T1183" time="1250.0284522863888" />
         <tli id="T1184" time="1250.6086666666667" type="appl" />
         <tli id="T1185" time="1251.0753333333332" type="appl" />
         <tli id="T1186" time="1251.542" type="appl" />
         <tli id="T1187" time="1252.285" type="appl" />
         <tli id="T1188" time="1252.9850000000001" type="appl" />
         <tli id="T1189" time="1253.685" type="appl" />
         <tli id="T1190" time="1254.385" type="appl" />
         <tli id="T1191" time="1255.085" type="appl" />
         <tli id="T1192" time="1255.7849999999999" type="appl" />
         <tli id="T1193" time="1256.485" type="appl" />
         <tli id="T1194" time="1256.785" type="appl" />
         <tli id="T1195" time="1257.5421250000002" type="appl" />
         <tli id="T1196" time="1258.29925" type="appl" />
         <tli id="T1197" time="1259.0563750000001" type="appl" />
         <tli id="T1198" time="1259.8135000000002" type="appl" />
         <tli id="T1199" time="1260.570625" type="appl" />
         <tli id="T1200" time="1261.3277500000002" type="appl" />
         <tli id="T1201" time="1262.084875" type="appl" />
         <tli id="T1202" time="1262.7617358994498" />
         <tli id="T1203" time="1263.368625" type="appl" />
         <tli id="T1204" time="1263.89525" type="appl" />
         <tli id="T1205" time="1264.421875" type="appl" />
         <tli id="T1206" time="1264.9485" type="appl" />
         <tli id="T1207" time="1265.4751250000002" type="appl" />
         <tli id="T1208" time="1266.0017500000001" type="appl" />
         <tli id="T1209" time="1266.528375" type="appl" />
         <tli id="T1210" time="1267.055" type="appl" />
         <tli id="T1211" time="1267.528" type="appl" />
         <tli id="T1212" time="1268.2136666666668" type="appl" />
         <tli id="T1213" time="1268.8993333333333" type="appl" />
         <tli id="T1214" time="1269.585" type="appl" />
         <tli id="T1215" time="1269.699" type="appl" />
         <tli id="T1216" time="1270.3055454545456" type="appl" />
         <tli id="T1217" time="1270.912090909091" type="appl" />
         <tli id="T1218" time="1271.5186363636365" type="appl" />
         <tli id="T1219" time="1272.125181818182" type="appl" />
         <tli id="T1220" time="1272.7317272727273" type="appl" />
         <tli id="T1221" time="1273.3382727272729" type="appl" />
         <tli id="T1222" time="1273.9448181818182" type="appl" />
         <tli id="T1223" time="1274.5513636363637" type="appl" />
         <tli id="T1224" time="1275.1579090909092" type="appl" />
         <tli id="T1225" time="1275.7644545454546" type="appl" />
         <tli id="T1226" time="1276.371" type="appl" />
         <tli id="T1227" time="1277.356" type="appl" />
         <tli id="T1228" time="1278.0513333333333" type="appl" />
         <tli id="T1229" time="1278.7466666666667" type="appl" />
         <tli id="T1230" time="1279.442" type="appl" />
         <tli id="T1231" time="1286.228" type="appl" />
         <tli id="T1521" />
         <tli id="T1522" />
         <tli id="T1232" time="1298.071" type="appl" />
         <tli id="T1233" time="1298.085" type="appl" />
         <tli id="T1234" time="1298.5959047619049" type="appl" />
         <tli id="T1235" time="1299.1068095238095" type="appl" />
         <tli id="T1236" time="1299.6177142857143" type="appl" />
         <tli id="T1237" time="1300.1286190476192" type="appl" />
         <tli id="T1238" time="1300.6395238095238" type="appl" />
         <tli id="T1239" time="1301.1504285714286" type="appl" />
         <tli id="T1240" time="1301.6613333333335" type="appl" />
         <tli id="T1241" time="1302.172238095238" type="appl" />
         <tli id="T1242" time="1302.683142857143" type="appl" />
         <tli id="T1243" time="1303.1940476190478" type="appl" />
         <tli id="T1244" time="1303.7049523809524" type="appl" />
         <tli id="T1245" time="1304.2158571428572" type="appl" />
         <tli id="T1246" time="1304.726761904762" type="appl" />
         <tli id="T1247" time="1305.2376666666667" type="appl" />
         <tli id="T1248" time="1305.7485714285715" type="appl" />
         <tli id="T1249" time="1306.2594761904763" type="appl" />
         <tli id="T1250" time="1306.770380952381" type="appl" />
         <tli id="T1251" time="1307.2812857142858" type="appl" />
         <tli id="T1252" time="1307.7921904761906" type="appl" />
         <tli id="T1253" time="1308.3030952380952" type="appl" />
         <tli id="T1254" time="1308.814" type="appl" />
         <tli id="T1255" time="1311.3" type="appl" />
         <tli id="T1256" time="1311.8060714285714" type="appl" />
         <tli id="T1257" time="1312.3121428571428" type="appl" />
         <tli id="T1258" time="1312.8182142857142" type="appl" />
         <tli id="T1259" time="1313.3242857142857" type="appl" />
         <tli id="T1260" time="1313.830357142857" type="appl" />
         <tli id="T1261" time="1314.3364285714285" type="appl" />
         <tli id="T1262" time="1314.8425" type="appl" />
         <tli id="T1263" time="1315.3485714285714" type="appl" />
         <tli id="T1264" time="1315.8546428571428" type="appl" />
         <tli id="T1265" time="1316.3607142857143" type="appl" />
         <tli id="T1266" time="1316.8667857142857" type="appl" />
         <tli id="T1267" time="1317.3728571428571" type="appl" />
         <tli id="T1268" time="1317.8789285714286" type="appl" />
         <tli id="T1269" time="1317.941520436259" />
         <tli id="T1270" time="1320.014" type="appl" />
         <tli id="T1271" time="1320.9591666666665" type="appl" />
         <tli id="T1272" time="1321.9043333333332" type="appl" />
         <tli id="T1273" time="1322.8494999999998" type="appl" />
         <tli id="T1274" time="1323.7946666666667" type="appl" />
         <tli id="T1275" time="1324.7398333333333" type="appl" />
         <tli id="T1276" time="1325.685" type="appl" />
         <tli id="T1277" time="1326.328" type="appl" />
         <tli id="T1278" time="1327.624" type="appl" />
         <tli id="T1279" time="1328.92" type="appl" />
         <tli id="T1280" time="1330.2160000000001" type="appl" />
         <tli id="T1281" time="1331.512" type="appl" />
         <tli id="T1282" time="1332.808" type="appl" />
         <tli id="T1533" time="1333.3634285714286" type="intp" />
         <tli id="T1283" time="1334.104" type="appl" />
         <tli id="T1284" time="1335.4" type="appl" />
         <tli id="T1285" time="1335.971" type="appl" />
         <tli id="T1286" time="1336.6186666666667" type="appl" />
         <tli id="T1287" time="1337.2663333333333" type="appl" />
         <tli id="T1288" time="1337.914" type="appl" />
         <tli id="T1289" time="1338.342" type="appl" />
         <tli id="T1290" time="1338.8867500000001" type="appl" />
         <tli id="T1291" time="1339.4315000000001" type="appl" />
         <tli id="T1292" time="1339.9762500000002" type="appl" />
         <tli id="T1293" time="1340.5210000000002" type="appl" />
         <tli id="T1294" time="1341.06575" type="appl" />
         <tli id="T1295" time="1341.6105" type="appl" />
         <tli id="T1296" time="1342.15525" type="appl" />
         <tli id="T1297" time="1342.7" type="appl" />
         <tli id="T1298" time="1343.042" type="appl" />
         <tli id="T1299" time="1343.6991666666665" type="appl" />
         <tli id="T1300" time="1344.3563333333332" type="appl" />
         <tli id="T1301" time="1345.0135" type="appl" />
         <tli id="T1302" time="1345.6706666666666" type="appl" />
         <tli id="T1303" time="1346.3278333333333" type="appl" />
         <tli id="T1304" time="1346.985" type="appl" />
         <tli id="T1305" time="1348.514" type="appl" />
         <tli id="T1306" time="1349.35325" type="appl" />
         <tli id="T1307" time="1350.1925" type="appl" />
         <tli id="T1308" time="1351.03175" type="appl" />
         <tli id="T1309" time="1351.871" type="appl" />
         <tli id="T1310" time="1352.6" type="appl" />
         <tli id="T1523" />
         <tli id="T1524" />
         <tli id="T1311" time="1366.585" type="appl" />
         <tli id="T1312" time="1369.171" type="appl" />
         <tli id="T1313" time="1370.1682" type="appl" />
         <tli id="T1314" time="1371.1654" type="appl" />
         <tli id="T1315" time="1372.1625999999999" type="appl" />
         <tli id="T1316" time="1373.1598" type="appl" />
         <tli id="T1317" time="1374.157" type="appl" />
         <tli id="T1318" time="1374.9636923076923" type="appl" />
         <tli id="T1319" time="1375.7703846153845" type="appl" />
         <tli id="T1320" time="1376.577076923077" type="appl" />
         <tli id="T1321" time="1377.383769230769" type="appl" />
         <tli id="T1322" time="1378.1904615384615" type="appl" />
         <tli id="T1323" time="1378.9971538461539" type="appl" />
         <tli id="T1324" time="1379.803846153846" type="appl" />
         <tli id="T1325" time="1380.6105384615385" type="appl" />
         <tli id="T1326" time="1381.4172307692309" type="appl" />
         <tli id="T1327" time="1382.223923076923" type="appl" />
         <tli id="T1328" time="1383.0306153846154" type="appl" />
         <tli id="T1329" time="1383.8373076923076" type="appl" />
         <tli id="T1330" time="1384.644" type="appl" />
         <tli id="T1331" time="1385.829" type="appl" />
         <tli id="T1332" time="1386.3065714285715" type="appl" />
         <tli id="T1333" time="1386.7841428571428" type="appl" />
         <tli id="T1334" time="1387.2617142857143" type="appl" />
         <tli id="T1335" time="1387.7392857142856" type="appl" />
         <tli id="T1336" time="1388.2168571428572" type="appl" />
         <tli id="T1337" time="1388.6944285714285" type="appl" />
         <tli id="T1338" time="1389.129" type="appl" />
         <tli id="T1339" time="1389.172" type="appl" />
         <tli id="T1340" time="1389.5121666666666" type="appl" />
         <tli id="T1341" time="1389.8953333333334" type="appl" />
         <tli id="T1342" time="1390.2785" type="appl" />
         <tli id="T1343" time="1390.6616666666666" type="appl" />
         <tli id="T1344" time="1391.0448333333334" type="appl" />
         <tli id="T1345" time="1391.428" type="appl" />
         <tli id="T1346" time="1391.429" type="appl" />
         <tli id="T1347" time="1391.8575" type="appl" />
         <tli id="T1348" time="1392.2" type="appl" />
         <tli id="T1349" time="1392.286" type="appl" />
         <tli id="T1350" time="1392.5714166666667" type="appl" />
         <tli id="T1351" time="1392.7145" type="appl" />
         <tli id="T1352" time="1392.9428333333333" type="appl" />
         <tli id="T1353" time="1393.143" type="appl" />
         <tli id="T1354" time="1393.31425" type="appl" />
         <tli id="T1355" time="1393.5715" type="appl" />
         <tli id="T1356" time="1393.6856666666667" type="appl" />
         <tli id="T1357" time="1394.0" type="appl" />
         <tli id="T1358" time="1394.0570833333334" type="appl" />
         <tli id="T1359" time="1394.4285" type="appl" />
         <tli id="T1360" time="1394.7999166666666" type="appl" />
         <tli id="T1361" time="1394.857" type="appl" />
         <tli id="T1362" time="1395.1713333333332" type="appl" />
         <tli id="T1363" time="1395.54275" type="appl" />
         <tli id="T1364" time="1395.6999999999998" type="appl" />
         <tli id="T1365" time="1395.9141666666667" type="appl" />
         <tli id="T1366" time="1396.2855833333333" type="appl" />
         <tli id="T1367" time="1396.543" type="appl" />
         <tli id="T1368" time="1396.657" type="appl" />
         <tli id="T1369" time="1396.843" type="appl" />
         <tli id="T1370" time="1397.1714" type="appl" />
         <tli id="T1371" time="1397.4998" type="appl" />
         <tli id="T1372" time="1397.8282" type="appl" />
         <tli id="T1373" time="1398.1566" type="appl" />
         <tli id="T1374" time="1398.329" type="appl" />
         <tli id="T1375" time="1398.485" type="appl" />
         <tli id="T1376" time="1399.257" type="appl" />
         <tli id="T1377" time="1399.585" type="appl" />
         <tli id="T1378" time="1400.1014444444445" type="appl" />
         <tli id="T1379" time="1400.617888888889" type="appl" />
         <tli id="T1380" time="1401.1343333333334" type="appl" />
         <tli id="T1381" time="1401.6507777777779" type="appl" />
         <tli id="T1382" time="1402.1672222222223" type="appl" />
         <tli id="T1383" time="1402.6836666666668" type="appl" />
         <tli id="T1384" time="1403.200111111111" type="appl" />
         <tli id="T1385" time="1403.7165555555555" type="appl" />
         <tli id="T1386" time="1404.233" type="appl" />
         <tli id="T1387" time="1404.7494444444444" type="appl" />
         <tli id="T1388" time="1405.2658888888889" type="appl" />
         <tli id="T1389" time="1405.7823333333333" type="appl" />
         <tli id="T1390" time="1406.2987777777778" type="appl" />
         <tli id="T1391" time="1406.8152222222222" type="appl" />
         <tli id="T1392" time="1407.3316666666667" type="appl" />
         <tli id="T1393" time="1407.8481111111112" type="appl" />
         <tli id="T1394" time="1408.3645555555556" type="appl" />
         <tli id="T1395" time="1408.881" type="appl" />
         <tli id="T1396" time="1409.3974444444445" type="appl" />
         <tli id="T1397" time="1409.913888888889" type="appl" />
         <tli id="T1398" time="1410.4303333333332" type="appl" />
         <tli id="T1399" time="1410.9467777777777" type="appl" />
         <tli id="T1400" time="1411.4632222222222" type="appl" />
         <tli id="T1401" time="1411.9796666666666" type="appl" />
         <tli id="T1402" time="1412.496111111111" type="appl" />
         <tli id="T1403" time="1413.0125555555555" type="appl" />
         <tli id="T1404" time="1413.529" type="appl" />
         <tli id="T1405" time="1414.475" type="appl" />
         <tli id="T1406" time="1414.614" type="appl" />
         <tli id="T1407" time="1415.0901666666666" type="appl" />
         <tli id="T1408" time="1415.5663333333334" type="appl" />
         <tli id="T1409" time="1416.0425" type="appl" />
         <tli id="T1410" time="1416.5186666666666" type="appl" />
         <tli id="T1411" time="1416.9948333333334" type="appl" />
         <tli id="T1412" time="1417.471" type="appl" />
         <tli id="T1413" time="1429.341" type="appl" />
         <tli id="T1414" time="1431.789" type="appl" />
         <tli id="T1525" />
         <tli id="T1526" />
         <tli id="T1415" time="1442.732" type="appl" />
         <tli id="T1416" time="1443.411" type="appl" />
         <tli id="T1417" time="1443.858888888889" type="appl" />
         <tli id="T1418" time="1444.3067777777778" type="appl" />
         <tli id="T1419" time="1444.7546666666667" type="appl" />
         <tli id="T1420" time="1445.2025555555556" type="appl" />
         <tli id="T1421" time="1445.6504444444445" type="appl" />
         <tli id="T1422" time="1446.0983333333334" type="appl" />
         <tli id="T1423" time="1446.5462222222222" type="appl" />
         <tli id="T1424" time="1446.9941111111111" type="appl" />
         <tli id="T1425" time="1447.442" type="appl" />
         <tli id="T1426" time="1447.585" type="appl" />
         <tli id="T1427" time="1448.1566666666668" type="appl" />
         <tli id="T1428" time="1448.7283333333332" type="appl" />
         <tli id="T1429" time="1449.5799647545514" />
         <tli id="T1430" time="1449.5837147399084" />
         <tli id="T1431" time="1449.9046111111113" type="appl" />
         <tli id="T1432" time="1450.3522222222223" type="appl" />
         <tli id="T1433" time="1450.7998333333335" type="appl" />
         <tli id="T1434" time="1451.2474444444445" type="appl" />
         <tli id="T1435" time="1451.6950555555557" type="appl" />
         <tli id="T1436" time="1452.1426666666666" type="appl" />
         <tli id="T1437" time="1452.5902777777778" type="appl" />
         <tli id="T1438" time="1453.0378888888888" type="appl" />
         <tli id="T1439" time="1453.4855" type="appl" />
         <tli id="T1440" time="1453.9331111111112" type="appl" />
         <tli id="T1441" time="1454.3807222222222" type="appl" />
         <tli id="T1442" time="1454.8283333333334" type="appl" />
         <tli id="T1443" time="1455.2759444444443" type="appl" />
         <tli id="T1444" time="1455.7235555555555" type="appl" />
         <tli id="T1445" time="1456.1711666666665" type="appl" />
         <tli id="T1446" time="1456.6187777777777" type="appl" />
         <tli id="T1447" time="1457.0663888888887" type="appl" />
         <tli id="T1448" time="1457.514" type="appl" />
         <tli id="T1449" time="1457.785" type="appl" />
         <tli id="T1527" />
         <tli id="T1528" />
         <tli id="T1450" time="1460.2" type="appl" />
         <tli id="T1451" time="1460.228" type="appl" />
         <tli id="T1452" time="1460.835" type="appl" />
         <tli id="T1453" time="1461.442" type="appl" />
         <tli id="T1454" time="1463.87" type="appl" />
         <tli id="T1455" time="1464.9485" type="appl" />
         <tli id="T1456" time="1465.055" type="appl" />
         <tli id="T1457" time="1466.027" type="appl" />
         <tli id="T1458" time="1469.927" type="appl" />
         <tli id="T1459" time="1472.35" type="appl" />
         <tli id="T1529" />
         <tli id="T1530" />
         <tli id="T1460" time="1487.411" type="appl" />
         <tli id="T0" time="1493.907" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T12" id="Seg_0" n="sc" s="T1">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T2" id="Seg_5" n="HIAT:non-pho" s="T1">…</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">не</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">могут</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">найти</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">такой</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">лес</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">густой</ts>
                  <nts id="Seg_27" n="HIAT:ip">,</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">пока</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">сами</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_36" n="HIAT:w" s="T10">придут</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_39" n="sc" s="T13">
               <ts e="T29" id="Seg_41" n="HIAT:u" s="T13">
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">Та-</ts>
                  <nts id="Seg_45" n="HIAT:ip">)</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_48" n="HIAT:w" s="T14">Таки</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_51" n="HIAT:w" s="T17">горы</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_55" n="HIAT:w" s="T18">что</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_58" n="HIAT:w" s="T19">нельзя</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_61" n="HIAT:w" s="T20">взглянуть</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_64" n="HIAT:w" s="T21">выше</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_67" n="HIAT:w" s="T22">их</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_71" n="HIAT:w" s="T23">сколь</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_74" n="HIAT:w" s="T24">раз</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_77" n="HIAT:w" s="T25">это</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ats e="T27" id="Seg_81" n="HIAT:non-pho" s="T26">…</ats>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_86" n="HIAT:w" s="T27">горы</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_89" n="HIAT:w" s="T28">там</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_92" n="sc" s="T49">
               <ts e="T52" id="Seg_94" n="HIAT:u" s="T49">
                  <ts e="T51" id="Seg_96" n="HIAT:w" s="T49">Ну</ts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_100" n="HIAT:w" s="T51">как</ts>
                  <nts id="Seg_101" n="HIAT:ip">…</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_103" n="sc" s="T53">
               <ts e="T62" id="Seg_105" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_107" n="HIAT:w" s="T53">Sʼarbibaʔ</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_111" n="HIAT:w" s="T54">i</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_114" n="HIAT:w" s="T55">dĭgəttə</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_117" n="HIAT:w" s="T56">girgit</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_120" n="HIAT:w" s="T57">nʼi</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_123" n="HIAT:w" s="T58">măna</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_126" n="HIAT:w" s="T59">šoləj</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_130" n="HIAT:w" s="T60">măn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_133" n="HIAT:w" s="T61">dĭʔnə</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T74" id="Seg_136" n="sc" s="T63">
               <ts e="T67" id="Seg_138" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_140" n="HIAT:w" s="T63">I</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_143" n="HIAT:w" s="T64">koʔbsaŋ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_146" n="HIAT:w" s="T65">iʔgö</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_149" n="HIAT:w" s="T66">ibiʔi</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_153" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_155" n="HIAT:w" s="T67">Kamen</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_157" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_159" n="HIAT:w" s="T68">m-</ts>
                  <nts id="Seg_160" n="HIAT:ip">)</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_163" n="HIAT:w" s="T69">măn</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_166" n="HIAT:w" s="T70">ibiem</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_169" n="HIAT:w" s="T71">dak</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_173" n="HIAT:w" s="T72">nagobiʔi</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_176" n="HIAT:w" s="T73">nʼiʔi</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_179" n="sc" s="T75">
               <ts e="T89" id="Seg_181" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_183" n="HIAT:w" s="T75">Măn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_186" n="HIAT:w" s="T76">mĭmbiem</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_189" n="HIAT:w" s="T77">dĭbər</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_193" n="HIAT:w" s="T78">Pjankăftə</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_196" n="HIAT:w" s="T79">tibinə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_199" n="HIAT:w" s="T80">mĭmbiem</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_203" n="HIAT:w" s="T81">dĭn</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T83" id="Seg_207" n="HIAT:w" s="T82">nʼibə</ts>
                  <nts id="Seg_208" n="HIAT:ip">)</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_211" n="HIAT:w" s="T83">ibi</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_215" n="HIAT:w" s="T84">i</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_217" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_219" n="HIAT:w" s="T85">măna=</ts>
                  <nts id="Seg_220" n="HIAT:ip">)</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_223" n="HIAT:w" s="T86">măn</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_226" n="HIAT:w" s="T87">dĭbər</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_229" n="HIAT:w" s="T88">maluʔpiam</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T96" id="Seg_232" n="sc" s="T91">
               <ts e="T96" id="Seg_234" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_236" n="HIAT:w" s="T91">Вот</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_239" n="HIAT:w" s="T92">как</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_242" n="HIAT:w" s="T93">ты</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_245" n="HIAT:w" s="T94">говоришь</ts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_248" n="HIAT:ip">(</nts>
                  <nts id="Seg_249" n="HIAT:ip">(</nts>
                  <ats e="T96" id="Seg_250" n="HIAT:non-pho" s="T95">…</ats>
                  <nts id="Seg_251" n="HIAT:ip">)</nts>
                  <nts id="Seg_252" n="HIAT:ip">)</nts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T119" id="Seg_255" n="sc" s="T105">
               <ts e="T119" id="Seg_257" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_259" n="HIAT:w" s="T105">Nu</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_263" n="HIAT:w" s="T106">iat</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_266" n="HIAT:w" s="T107">abat</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_269" n="HIAT:w" s="T108">šoləj</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_272" n="HIAT:w" s="T109">nʼin</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_275" n="HIAT:ip">(</nts>
                  <ts e="T111" id="Seg_277" n="HIAT:w" s="T110">koʔbdonə</ts>
                  <nts id="Seg_278" n="HIAT:ip">)</nts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_282" n="HIAT:w" s="T111">dĭgəttə</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_284" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_286" n="HIAT:w" s="T112">dĭ=</ts>
                  <nts id="Seg_287" n="HIAT:ip">)</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_290" n="HIAT:w" s="T113">dĭʔnə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_293" n="HIAT:w" s="T114">mănliaʔi:</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_295" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_297" n="HIAT:w" s="T115">Kanaʔ</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_300" n="HIAT:w" s="T116">miʔ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_303" n="HIAT:w" s="T117">nʼinə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_306" n="HIAT:w" s="T118">tibinə</ts>
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_310" n="sc" s="T120">
               <ts e="T123" id="Seg_312" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_314" n="HIAT:w" s="T120">Abandə</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_317" n="HIAT:w" s="T121">iandə</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_320" n="HIAT:w" s="T122">surarləʔjə</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_324" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_326" n="HIAT:w" s="T123">Nada</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_329" n="HIAT:w" s="T124">koʔbdom</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_332" n="HIAT:w" s="T125">surarzittə</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_336" n="HIAT:w" s="T126">dĭgəttə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_339" n="HIAT:w" s="T127">šoləj</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_342" n="HIAT:w" s="T128">koʔbdo</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_346" n="HIAT:u" s="T129">
                  <nts id="Seg_347" n="HIAT:ip">"</nts>
                  <ts e="T130" id="Seg_349" n="HIAT:w" s="T129">Kalal</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_352" n="HIAT:w" s="T130">tibinə</ts>
                  <nts id="Seg_353" n="HIAT:ip">?</nts>
                  <nts id="Seg_354" n="HIAT:ip">"</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_356" n="sc" s="T132">
               <ts e="T135" id="Seg_358" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_360" n="HIAT:w" s="T132">Dĭ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_363" n="HIAT:w" s="T133">măndə:</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_365" n="HIAT:ip">"</nts>
                  <ts e="T135" id="Seg_367" n="HIAT:w" s="T134">Kalam</ts>
                  <nts id="Seg_368" n="HIAT:ip">"</nts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_371" n="sc" s="T136">
               <ts e="T141" id="Seg_373" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_375" n="HIAT:w" s="T136">Nu</ts>
                  <nts id="Seg_376" n="HIAT:ip">,</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_379" n="HIAT:w" s="T137">dĭgəttə</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_382" n="HIAT:w" s="T138">ara</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_385" n="HIAT:w" s="T139">detləʔi</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_389" n="HIAT:w" s="T140">ipek</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_392" n="sc" s="T142">
               <ts e="T148" id="Seg_394" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_396" n="HIAT:w" s="T142">Amnolaʔbəʔjə</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_400" n="HIAT:w" s="T143">bĭtleʔbəʔjə</ts>
                  <nts id="Seg_401" n="HIAT:ip">,</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_404" n="HIAT:w" s="T144">dĭgəttə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_406" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_408" n="HIAT:w" s="T145">šobi=</ts>
                  <nts id="Seg_409" n="HIAT:ip">)</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_412" n="HIAT:w" s="T146">šolaʔbəʔjə</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_415" n="HIAT:w" s="T147">inezi</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_418" n="sc" s="T149">
               <ts e="T155" id="Seg_420" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_422" n="HIAT:w" s="T149">Dĭm</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_424" n="HIAT:ip">(</nts>
                  <ts e="T151" id="Seg_426" n="HIAT:w" s="T150">il-</ts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_430" n="HIAT:w" s="T151">iluʔpiʔi</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_433" n="HIAT:w" s="T152">i</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_436" n="HIAT:w" s="T153">kumbiʔi</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_439" n="HIAT:w" s="T154">maːʔndə</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_442" n="sc" s="T172">
               <ts e="T177" id="Seg_444" n="HIAT:u" s="T172">
                  <nts id="Seg_445" n="HIAT:ip">(</nts>
                  <ts e="T173" id="Seg_447" n="HIAT:w" s="T172">I-</ts>
                  <nts id="Seg_448" n="HIAT:ip">)</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_451" n="HIAT:w" s="T173">Iat</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_454" n="HIAT:w" s="T174">i</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_457" n="HIAT:w" s="T175">abat</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_460" n="HIAT:w" s="T176">mămbiʔi</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_463" n="sc" s="T178">
               <ts e="T187" id="Seg_465" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_467" n="HIAT:w" s="T178">Dĭgəttə</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_470" n="HIAT:w" s="T179">abiʔi</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_473" n="HIAT:w" s="T180">пиво</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_477" n="HIAT:w" s="T181">ara</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_480" n="HIAT:w" s="T182">iʔgö</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_483" n="HIAT:w" s="T183">ibi</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_487" n="HIAT:w" s="T184">bar</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_490" n="HIAT:w" s="T185">ĭmbi</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_493" n="HIAT:w" s="T186">abiʔi</ts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T192" id="Seg_496" n="sc" s="T188">
               <ts e="T192" id="Seg_498" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_500" n="HIAT:w" s="T188">Nabəʔi</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_503" n="HIAT:w" s="T189">pürbiʔi</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_507" n="HIAT:w" s="T190">uja</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_510" n="HIAT:w" s="T191">iʔgö</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_513" n="sc" s="T193">
               <ts e="T201" id="Seg_515" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_517" n="HIAT:w" s="T193">Dĭgəttə</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_521" n="HIAT:w" s="T194">nulbi-</ts>
                  <nts id="Seg_522" n="HIAT:ip">)</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_525" n="HIAT:w" s="T195">nuldəbiʔi</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_528" n="HIAT:w" s="T196">stolbə</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_532" n="HIAT:w" s="T197">bar</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_535" n="HIAT:w" s="T198">ambiʔi</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_538" n="HIAT:w" s="T199">i</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1531" id="Seg_541" n="HIAT:w" s="T200">kalla</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_544" n="HIAT:w" s="T1531">dʼürbiʔi</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_547" n="sc" s="T202">
               <ts e="T209" id="Seg_549" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_551" n="HIAT:w" s="T202">Koʔbsaŋ</ts>
                  <nts id="Seg_552" n="HIAT:ip">,</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_554" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_556" n="HIAT:w" s="T203">il</ts>
                  <nts id="Seg_557" n="HIAT:ip">)</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_560" n="HIAT:w" s="T204">bar</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_563" n="HIAT:w" s="T205">ara</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_566" n="HIAT:w" s="T206">biʔpiʔi</ts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_570" n="HIAT:w" s="T207">sʼarbiʔi</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_573" n="HIAT:w" s="T208">garmonʼnʼazi</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T211" id="Seg_576" n="sc" s="T210">
               <ts e="T211" id="Seg_578" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_580" n="HIAT:w" s="T210">Suʔmibiʔi</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T215" id="Seg_583" n="sc" s="T212">
               <ts e="T215" id="Seg_585" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_587" n="HIAT:w" s="T212">Dĭgəttə</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_590" n="HIAT:w" s="T213">bar</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1532" id="Seg_593" n="HIAT:w" s="T214">kalla</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_596" n="HIAT:w" s="T1532">dʼürbiʔi</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_599" n="sc" s="T217">
               <ts e="T223" id="Seg_601" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_603" n="HIAT:w" s="T217">Ну</ts>
                  <nts id="Seg_604" n="HIAT:ip">,</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_607" n="HIAT:w" s="T218">это</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_610" n="HIAT:w" s="T219">я</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_613" n="HIAT:w" s="T220">про</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_616" n="HIAT:w" s="T221">свадьбу</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_619" n="HIAT:w" s="T222">говорила</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T233" id="Seg_622" n="sc" s="T229">
               <ts e="T233" id="Seg_624" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_626" n="HIAT:w" s="T229">Пиво</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_629" n="HIAT:w" s="T230">делали</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_632" n="HIAT:w" s="T231">сами</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_635" n="HIAT:w" s="T232">они</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T251" id="Seg_638" n="sc" s="T247">
               <ts e="T251" id="Seg_640" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_642" n="HIAT:w" s="T247">Amnobiʔi</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_645" n="HIAT:w" s="T248">jakše</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_649" n="HIAT:w" s="T249">i</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_652" n="HIAT:w" s="T250">dʼabrolaʔpiʔi</ts>
                  <nts id="Seg_653" n="HIAT:ip">.</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T263" id="Seg_655" n="sc" s="T252">
               <ts e="T256" id="Seg_657" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_659" n="HIAT:w" s="T252">Ara</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_662" n="HIAT:w" s="T253">bĭtləʔi</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_665" n="HIAT:w" s="T254">dak</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_669" n="HIAT:w" s="T255">dʼabrolaʔbəʔjə</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_673" n="HIAT:u" s="T256">
                  <nts id="Seg_674" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_676" n="HIAT:w" s="T256">Eʔbdən=</ts>
                  <nts id="Seg_677" n="HIAT:ip">)</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_680" n="HIAT:w" s="T257">Eʔbdəzaŋdə</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_683" n="HIAT:w" s="T258">onʼiʔ</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_685" n="HIAT:ip">(</nts>
                  <ts e="T260" id="Seg_687" n="HIAT:w" s="T259">onʼiʔtə</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_690" n="HIAT:w" s="T260">ibiʔi</ts>
                  <nts id="Seg_691" n="HIAT:ip">)</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_694" n="HIAT:w" s="T261">i</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_697" n="HIAT:w" s="T262">dʼabrolaʔpiʔi</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_700" n="sc" s="T265">
               <ts e="T279" id="Seg_702" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_704" n="HIAT:w" s="T265">Вот</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_707" n="HIAT:w" s="T266">я</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_710" n="HIAT:w" s="T267">помню</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_712" n="HIAT:ip">(</nts>
                  <ts e="T270" id="Seg_714" n="HIAT:w" s="T269">дву-</ts>
                  <nts id="Seg_715" n="HIAT:ip">)</nts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_719" n="HIAT:w" s="T270">я</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_722" n="HIAT:w" s="T271">тебе</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_725" n="HIAT:w" s="T272">может</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_728" n="HIAT:w" s="T273">рассказывала</ts>
                  <nts id="Seg_729" n="HIAT:ip">,</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_732" n="HIAT:w" s="T274">двух</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_734" n="HIAT:ip">(</nts>
                  <ts e="T276" id="Seg_736" n="HIAT:w" s="T275">ста-</ts>
                  <nts id="Seg_737" n="HIAT:ip">)</nts>
                  <nts id="Seg_738" n="HIAT:ip">,</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_741" n="HIAT:w" s="T276">старик</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_744" n="HIAT:w" s="T277">со</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_747" n="HIAT:w" s="T278">старухой</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T287" id="Seg_750" n="sc" s="T280">
               <ts e="T287" id="Seg_752" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_754" n="HIAT:w" s="T280">Напились</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_757" n="HIAT:w" s="T281">пьяны</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_761" n="HIAT:w" s="T282">так</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_764" n="HIAT:w" s="T283">через</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_767" n="HIAT:w" s="T284">пол</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_770" n="HIAT:w" s="T285">был</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_773" n="HIAT:w" s="T286">порог</ts>
                  <nts id="Seg_774" n="HIAT:ip">.</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_776" n="sc" s="T288">
               <ts e="T300" id="Seg_778" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_780" n="HIAT:w" s="T288">Они</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_783" n="HIAT:w" s="T289">друг</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_786" n="HIAT:w" s="T290">дружку</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_789" n="HIAT:w" s="T291">взяли</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_792" n="HIAT:w" s="T292">за</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_795" n="HIAT:w" s="T293">волосы</ts>
                  <nts id="Seg_796" n="HIAT:ip">,</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_799" n="HIAT:w" s="T294">тянулись</ts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_803" n="HIAT:w" s="T295">тянулись</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_806" n="HIAT:w" s="T296">и</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_809" n="HIAT:w" s="T297">так</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_812" n="HIAT:w" s="T298">и</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_815" n="HIAT:w" s="T299">уснули</ts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T327" id="Seg_818" n="sc" s="T301">
               <ts e="T315" id="Seg_820" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_822" n="HIAT:w" s="T301">Утром</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_825" n="HIAT:w" s="T302">встали</ts>
                  <nts id="Seg_826" n="HIAT:ip">,</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_828" n="HIAT:ip">"</nts>
                  <ts e="T304" id="Seg_830" n="HIAT:w" s="T303">Чего-то</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_834" n="HIAT:w" s="T304">говорит</ts>
                  <nts id="Seg_835" n="HIAT:ip">,</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_838" n="HIAT:w" s="T305">у</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_841" n="HIAT:w" s="T306">меня</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_844" n="HIAT:w" s="T307">тут</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_847" n="HIAT:w" s="T308">больно</ts>
                  <nts id="Seg_848" n="HIAT:ip">"</nts>
                  <nts id="Seg_849" n="HIAT:ip">,</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_852" n="HIAT:w" s="T309">и</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_855" n="HIAT:w" s="T310">он</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_858" n="HIAT:w" s="T311">говорит:</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_860" n="HIAT:ip">"</nts>
                  <ts e="T313" id="Seg_862" n="HIAT:w" s="T312">У</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_865" n="HIAT:w" s="T313">меня</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_868" n="HIAT:w" s="T314">больно</ts>
                  <nts id="Seg_869" n="HIAT:ip">"</nts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_873" n="HIAT:u" s="T315">
                  <nts id="Seg_874" n="HIAT:ip">"</nts>
                  <ts e="T316" id="Seg_876" n="HIAT:w" s="T315">Может</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_880" n="HIAT:w" s="T316">мы</ts>
                  <nts id="Seg_881" n="HIAT:ip">,</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_884" n="HIAT:w" s="T317">говорит</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_888" n="HIAT:w" s="T318">дрались</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_891" n="HIAT:w" s="T319">с</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_894" n="HIAT:w" s="T320">тобой</ts>
                  <nts id="Seg_895" n="HIAT:ip">,</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_898" n="HIAT:w" s="T321">не</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_901" n="HIAT:w" s="T322">знаем</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_905" n="HIAT:w" s="T323">так</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_908" n="HIAT:w" s="T324">напились</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_911" n="HIAT:w" s="T325">пьяные</ts>
                  <nts id="Seg_912" n="HIAT:ip">"</nts>
                  <nts id="Seg_913" n="HIAT:ip">,</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_916" n="HIAT:w" s="T326">вот</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T343" id="Seg_919" n="sc" s="T328">
               <ts e="T343" id="Seg_921" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_923" n="HIAT:w" s="T328">Я</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_926" n="HIAT:w" s="T329">их</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_929" n="HIAT:w" s="T330">помню</ts>
                  <nts id="Seg_930" n="HIAT:ip">,</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_933" n="HIAT:w" s="T331">фамилие</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_936" n="HIAT:w" s="T332">у</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_939" n="HIAT:w" s="T333">его</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_942" n="HIAT:w" s="T334">было</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_945" n="HIAT:w" s="T335">Тугуин</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_949" n="HIAT:w" s="T336">а</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_952" n="HIAT:w" s="T337">ее</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_955" n="HIAT:w" s="T338">звали</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_958" n="HIAT:w" s="T339">Вера</ts>
                  <nts id="Seg_959" n="HIAT:ip">,</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_962" n="HIAT:w" s="T340">и</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_965" n="HIAT:w" s="T341">он</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_968" n="HIAT:w" s="T342">Василий</ts>
                  <nts id="Seg_969" n="HIAT:ip">.</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_971" n="sc" s="T344">
               <ts e="T346" id="Seg_973" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_975" n="HIAT:w" s="T344">Звали</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_978" n="HIAT:w" s="T345">его</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T360" id="Seg_981" n="sc" s="T350">
               <ts e="T360" id="Seg_983" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_985" n="HIAT:w" s="T350">Камасинцы</ts>
                  <nts id="Seg_986" n="HIAT:ip">,</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_989" n="HIAT:w" s="T351">я</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_992" n="HIAT:w" s="T353">их</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_995" n="HIAT:w" s="T354">хорошо</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_998" n="HIAT:w" s="T355">помню</ts>
                  <nts id="Seg_999" n="HIAT:ip">,</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1002" n="HIAT:w" s="T356">недалёко</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1005" n="HIAT:w" s="T357">от</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1008" n="HIAT:w" s="T358">нас</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1011" n="HIAT:w" s="T359">жили</ts>
                  <nts id="Seg_1012" n="HIAT:ip">.</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T378" id="Seg_1014" n="sc" s="T371">
               <ts e="T378" id="Seg_1016" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1018" n="HIAT:w" s="T371">Šində</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1021" n="HIAT:w" s="T372">sumna</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1024" n="HIAT:w" s="T373">ibi</ts>
                  <nts id="Seg_1025" n="HIAT:ip">,</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1028" n="HIAT:w" s="T374">šində</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1031" n="HIAT:w" s="T375">sejʔpü</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1034" n="HIAT:w" s="T376">ibi</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1037" n="HIAT:w" s="T377">esseŋ</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T387" id="Seg_1040" n="sc" s="T379">
               <ts e="T383" id="Seg_1042" n="HIAT:u" s="T379">
                  <ts e="T381" id="Seg_1044" n="HIAT:w" s="T379">Iʔgö</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1047" n="HIAT:w" s="T381">deʔpiʔi</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1051" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1053" n="HIAT:w" s="T383">Dĭzeŋ</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1056" n="HIAT:w" s="T384">ĭmbidə</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1059" n="HIAT:w" s="T385">ej</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1062" n="HIAT:w" s="T386">abiʔi</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T400" id="Seg_1065" n="sc" s="T388">
               <ts e="T392" id="Seg_1067" n="HIAT:u" s="T388">
                  <ts e="T389" id="Seg_1069" n="HIAT:w" s="T388">Tüj</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1072" n="HIAT:w" s="T389">il</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1075" n="HIAT:w" s="T390">esseŋ</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1077" n="HIAT:ip">(</nts>
                  <ts e="T392" id="Seg_1079" n="HIAT:w" s="T391">barəʔbəʔjə</ts>
                  <nts id="Seg_1080" n="HIAT:ip">)</nts>
                  <nts id="Seg_1081" n="HIAT:ip">.</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1084" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1086" n="HIAT:w" s="T392">Bălʼnitsanə</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1089" n="HIAT:w" s="T393">mĭmbiʔi</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1092" n="HIAT:w" s="T394">i</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1095" n="HIAT:w" s="T395">ĭmbidə</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1098" n="HIAT:w" s="T396">abiʔi</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1102" n="HIAT:w" s="T397">ej</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1105" n="HIAT:w" s="T398">deʔlieʔi</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1108" n="HIAT:w" s="T399">esseŋ</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_1111" n="sc" s="T403">
               <ts e="T410" id="Seg_1113" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1115" n="HIAT:w" s="T403">Nu</ts>
                  <nts id="Seg_1116" n="HIAT:ip">,</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1119" n="HIAT:w" s="T404">esseŋdə</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1122" n="HIAT:w" s="T405">dĭzeŋ</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1125" n="HIAT:w" s="T406">ĭzembiʔi</ts>
                  <nts id="Seg_1126" n="HIAT:ip">,</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1129" n="HIAT:w" s="T407">külambiʔi</ts>
                  <nts id="Seg_1130" n="HIAT:ip">,</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1133" n="HIAT:w" s="T408">amga</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1136" n="HIAT:w" s="T409">maːluʔpiʔi</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T438" id="Seg_1139" n="sc" s="T433">
               <ts e="T438" id="Seg_1141" n="HIAT:u" s="T433">
                  <ts e="T437" id="Seg_1143" n="HIAT:w" s="T433">Dʼorbiʔi</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1146" n="HIAT:w" s="T437">esseŋ</ts>
                  <nts id="Seg_1147" n="HIAT:ip">.</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T444" id="Seg_1149" n="sc" s="T440">
               <ts e="T444" id="Seg_1151" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1153" n="HIAT:w" s="T440">I</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1155" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1157" n="HIAT:w" s="T441">izeb-</ts>
                  <nts id="Seg_1158" n="HIAT:ip">)</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1161" n="HIAT:w" s="T443">ĭzembiʔi</ts>
                  <nts id="Seg_1162" n="HIAT:ip">.</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T462" id="Seg_1164" n="sc" s="T458">
               <ts e="T462" id="Seg_1166" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1168" n="HIAT:w" s="T458">Iat</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1171" n="HIAT:w" s="T459">uʔlia</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1174" n="HIAT:w" s="T460">da</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1177" n="HIAT:w" s="T461">iləj</ts>
                  <nts id="Seg_1178" n="HIAT:ip">.</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T468" id="Seg_1180" n="sc" s="T463">
               <ts e="T468" id="Seg_1182" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1184" n="HIAT:w" s="T463">Dĭgəttə</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1187" n="HIAT:w" s="T464">mĭlie</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1190" n="HIAT:w" s="T465">nüjö</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1192" n="HIAT:ip">(</nts>
                  <ts e="T467" id="Seg_1194" n="HIAT:w" s="T466">imiʔ</ts>
                  <nts id="Seg_1195" n="HIAT:ip">)</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1198" n="HIAT:w" s="T467">dĭzeŋdə</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T472" id="Seg_1201" n="sc" s="T469">
               <ts e="T472" id="Seg_1203" n="HIAT:u" s="T469">
                  <nts id="Seg_1204" n="HIAT:ip">(</nts>
                  <ts e="T470" id="Seg_1206" n="HIAT:w" s="T469">Amnəj</ts>
                  <nts id="Seg_1207" n="HIAT:ip">)</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1210" n="HIAT:w" s="T470">da</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1213" n="HIAT:w" s="T471">embi</ts>
                  <nts id="Seg_1214" n="HIAT:ip">.</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T475" id="Seg_1216" n="sc" s="T473">
               <ts e="T475" id="Seg_1218" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1220" n="HIAT:w" s="T473">Dĭ</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1223" n="HIAT:w" s="T474">kunolluʔpi</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1226" n="sc" s="T476">
               <ts e="T480" id="Seg_1228" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1230" n="HIAT:w" s="T476">I</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1233" n="HIAT:w" s="T477">bostə</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1236" n="HIAT:w" s="T478">iʔbəbi</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1239" n="HIAT:w" s="T479">kunolluʔpi</ts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T508" id="Seg_1242" n="sc" s="T502">
               <ts e="T508" id="Seg_1244" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_1246" n="HIAT:w" s="T502">Nʼilgölaʔpiʔi</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1248" n="HIAT:ip">(</nts>
                  <ts e="T504" id="Seg_1250" n="HIAT:w" s="T503">abam</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1253" n="HIAT:w" s="T504">=</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1256" n="HIAT:w" s="T505">abat=</ts>
                  <nts id="Seg_1257" n="HIAT:ip">)</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1260" n="HIAT:w" s="T506">abat</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1263" n="HIAT:w" s="T507">iat</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T515" id="Seg_1266" n="sc" s="T509">
               <ts e="T515" id="Seg_1268" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_1270" n="HIAT:w" s="T509">Măn</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1273" n="HIAT:w" s="T510">üdʼüge</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1276" n="HIAT:w" s="T511">ibiem</ts>
                  <nts id="Seg_1277" n="HIAT:ip">,</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1280" n="HIAT:w" s="T512">iam</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1283" n="HIAT:w" s="T513">măndə:</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1285" n="HIAT:ip">"</nts>
                  <ts e="T515" id="Seg_1287" n="HIAT:w" s="T514">Kanaʔ</ts>
                  <nts id="Seg_1288" n="HIAT:ip">"</nts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T518" id="Seg_1291" n="sc" s="T516">
               <ts e="T518" id="Seg_1293" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1295" n="HIAT:w" s="T516">Öʔləj</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1298" n="HIAT:w" s="T517">gibər-nʼibudʼ</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T524" id="Seg_1301" n="sc" s="T519">
               <ts e="T524" id="Seg_1303" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1305" n="HIAT:w" s="T519">Măn</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1308" n="HIAT:w" s="T520">nuliam</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1311" n="HIAT:w" s="T521">da</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1314" n="HIAT:w" s="T522">mʼegəlaʔbəm</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1317" n="HIAT:w" s="T523">dĭn</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T530" id="Seg_1320" n="sc" s="T525">
               <ts e="T530" id="Seg_1322" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_1324" n="HIAT:w" s="T525">A</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1327" n="HIAT:w" s="T526">dĭ:</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1329" n="HIAT:ip">"</nts>
                  <ts e="T528" id="Seg_1331" n="HIAT:w" s="T527">Măn</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1334" n="HIAT:w" s="T528">tănan</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1337" n="HIAT:w" s="T529">münörləm</ts>
                  <nts id="Seg_1338" n="HIAT:ip">"</nts>
                  <nts id="Seg_1339" n="HIAT:ip">.</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T537" id="Seg_1341" n="sc" s="T531">
               <ts e="T537" id="Seg_1343" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1345" n="HIAT:w" s="T531">Măn</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1348" n="HIAT:w" s="T532">dĭgəttə</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1351" n="HIAT:w" s="T533">nuʔmiluʔpiam</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1354" n="HIAT:w" s="T534">i</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1357" n="HIAT:w" s="T535">büžü</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1360" n="HIAT:w" s="T536">šobiam</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T544" id="Seg_1363" n="sc" s="T538">
               <ts e="T544" id="Seg_1365" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1367" n="HIAT:w" s="T538">Dĭgəttə</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1370" n="HIAT:w" s="T539">dĭ</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1373" n="HIAT:w" s="T540">ĭmbidə</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1376" n="HIAT:w" s="T541">măna</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1379" n="HIAT:w" s="T542">ej</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1382" n="HIAT:w" s="T543">münörleʔpi</ts>
                  <nts id="Seg_1383" n="HIAT:ip">.</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T552" id="Seg_1385" n="sc" s="T546">
               <ts e="T552" id="Seg_1387" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_1389" n="HIAT:w" s="T546">Любила</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1391" n="HIAT:ip">(</nts>
                  <nts id="Seg_1392" n="HIAT:ip">(</nts>
                  <ats e="T548" id="Seg_1393" n="HIAT:non-pho" s="T547">…</ats>
                  <nts id="Seg_1394" n="HIAT:ip">)</nts>
                  <nts id="Seg_1395" n="HIAT:ip">)</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1398" n="HIAT:w" s="T548">тоже</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1401" n="HIAT:w" s="T549">над</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1404" n="HIAT:w" s="T550">матерью</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T562" id="Seg_1407" n="sc" s="T561">
               <ts e="T562" id="Seg_1409" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1411" n="HIAT:w" s="T561">Münörbiʔi</ts>
                  <nts id="Seg_1412" n="HIAT:ip">.</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T565" id="Seg_1414" n="sc" s="T563">
               <ts e="T565" id="Seg_1416" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_1418" n="HIAT:w" s="T563">Tăŋ</ts>
                  <nts id="Seg_1419" n="HIAT:ip">,</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1422" n="HIAT:w" s="T564">šerəpsi</ts>
                  <nts id="Seg_1423" n="HIAT:ip">.</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_1425" n="sc" s="T566">
               <ts e="T571" id="Seg_1427" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_1429" n="HIAT:w" s="T566">Măna</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1432" n="HIAT:w" s="T567">iam</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1435" n="HIAT:w" s="T568">ugandə</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1438" n="HIAT:w" s="T569">tăŋ</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1441" n="HIAT:w" s="T570">münörbiʔi</ts>
                  <nts id="Seg_1442" n="HIAT:ip">.</nts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_1444" n="sc" s="T572">
               <ts e="T577" id="Seg_1446" n="HIAT:u" s="T572">
                  <ts e="T575" id="Seg_1448" n="HIAT:w" s="T572">Măn</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1451" n="HIAT:w" s="T575">tăŋ</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1454" n="HIAT:w" s="T576">alombiam</ts>
                  <nts id="Seg_1455" n="HIAT:ip">.</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T591" id="Seg_1457" n="sc" s="T589">
               <ts e="T591" id="Seg_1459" n="HIAT:u" s="T589">
                  <ts e="T591" id="Seg_1461" n="HIAT:w" s="T589">Прутом</ts>
                  <nts id="Seg_1462" n="HIAT:ip">.</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T593" id="Seg_1464" n="sc" s="T592">
               <ts e="T593" id="Seg_1466" n="HIAT:u" s="T592">
                  <nts id="Seg_1467" n="HIAT:ip">(</nts>
                  <nts id="Seg_1468" n="HIAT:ip">(</nts>
                  <ats e="T593" id="Seg_1469" n="HIAT:non-pho" s="T592">…</ats>
                  <nts id="Seg_1470" n="HIAT:ip">)</nts>
                  <nts id="Seg_1471" n="HIAT:ip">)</nts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T605" id="Seg_1474" n="sc" s="T602">
               <ts e="T605" id="Seg_1476" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_1478" n="HIAT:w" s="T602">Нет</ts>
                  <nts id="Seg_1479" n="HIAT:ip">,</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1482" n="HIAT:w" s="T603">не</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1485" n="HIAT:w" s="T604">умели</ts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T616" id="Seg_1488" n="sc" s="T608">
               <ts e="T616" id="Seg_1490" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_1492" n="HIAT:w" s="T608">Я</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1495" n="HIAT:w" s="T609">не</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1498" n="HIAT:w" s="T610">знаю</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1502" n="HIAT:w" s="T611">почему</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1505" n="HIAT:w" s="T612">у</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1508" n="HIAT:w" s="T613">их</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1511" n="HIAT:w" s="T614">не</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1514" n="HIAT:w" s="T615">было</ts>
                  <nts id="Seg_1515" n="HIAT:ip">.</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T633" id="Seg_1517" n="sc" s="T618">
               <ts e="T633" id="Seg_1519" n="HIAT:u" s="T618">
                  <ts e="T619" id="Seg_1521" n="HIAT:w" s="T618">Вот</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_1524" n="HIAT:w" s="T619">это</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_1527" n="HIAT:w" s="T621">у</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1530" n="HIAT:w" s="T622">одной</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_1533" n="HIAT:w" s="T623">сестре</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_1536" n="HIAT:w" s="T624">стояла</ts>
                  <nts id="Seg_1537" n="HIAT:ip">,</nts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1540" n="HIAT:w" s="T625">так</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_1543" n="HIAT:w" s="T626">она</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_1546" n="HIAT:w" s="T627">говорит:</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_1549" n="HIAT:w" s="T628">это</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_1552" n="HIAT:w" s="T629">они</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_1555" n="HIAT:w" s="T630">выдумали</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_1558" n="HIAT:w" s="T631">свой</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_1561" n="HIAT:w" s="T632">язык</ts>
                  <nts id="Seg_1562" n="HIAT:ip">.</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T640" id="Seg_1564" n="sc" s="T634">
               <ts e="T640" id="Seg_1566" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_1568" n="HIAT:w" s="T634">Нету</ts>
                  <nts id="Seg_1569" n="HIAT:ip">,</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1572" n="HIAT:w" s="T635">говорит</ts>
                  <nts id="Seg_1573" n="HIAT:ip">,</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_1576" n="HIAT:w" s="T636">у</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_1579" n="HIAT:w" s="T637">их</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_1582" n="HIAT:w" s="T638">никаких</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_1585" n="HIAT:w" s="T639">книг</ts>
                  <nts id="Seg_1586" n="HIAT:ip">.</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_1588" n="sc" s="T641">
               <ts e="T643" id="Seg_1590" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_1592" n="HIAT:w" s="T641">Не</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_1595" n="HIAT:w" s="T642">знаю</ts>
                  <nts id="Seg_1596" n="HIAT:ip">.</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T655" id="Seg_1598" n="sc" s="T647">
               <ts e="T655" id="Seg_1600" n="HIAT:u" s="T647">
                  <ts e="T649" id="Seg_1602" n="HIAT:w" s="T647">Ну</ts>
                  <nts id="Seg_1603" n="HIAT:ip">,</nts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_1606" n="HIAT:w" s="T649">я</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_1609" n="HIAT:w" s="T652">говорила</ts>
                  <nts id="Seg_1610" n="HIAT:ip">…</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T674" id="Seg_1612" n="sc" s="T661">
               <ts e="T674" id="Seg_1614" n="HIAT:u" s="T661">
                  <ts e="T664" id="Seg_1616" n="HIAT:w" s="T661">Ну</ts>
                  <nts id="Seg_1617" n="HIAT:ip">,</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_1620" n="HIAT:w" s="T664">что</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1622" n="HIAT:ip">(</nts>
                  <ts e="T666" id="Seg_1624" n="HIAT:w" s="T665">нету=</ts>
                  <nts id="Seg_1625" n="HIAT:ip">)</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_1628" n="HIAT:w" s="T666">не</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_1631" n="HIAT:w" s="T667">умеют</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_1634" n="HIAT:w" s="T668">ни</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_1637" n="HIAT:w" s="T669">писать</ts>
                  <nts id="Seg_1638" n="HIAT:ip">,</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_1641" n="HIAT:w" s="T671">ни</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_1644" n="HIAT:w" s="T672">читать</ts>
                  <nts id="Seg_1645" n="HIAT:ip">.</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T683" id="Seg_1647" n="sc" s="T676">
               <ts e="T683" id="Seg_1649" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_1651" n="HIAT:w" s="T676">Только</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_1654" n="HIAT:w" s="T677">один</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_1657" n="HIAT:w" s="T678">у</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_1660" n="HIAT:w" s="T679">нас</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_1663" n="HIAT:w" s="T680">там</ts>
                  <nts id="Seg_1664" n="HIAT:ip">,</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_1667" n="HIAT:w" s="T681">Александр</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_1670" n="HIAT:w" s="T682">Шайбин</ts>
                  <nts id="Seg_1671" n="HIAT:ip">.</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T688" id="Seg_1673" n="sc" s="T684">
               <ts e="T688" id="Seg_1675" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_1677" n="HIAT:w" s="T684">Вот</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_1680" n="HIAT:w" s="T685">он</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_1683" n="HIAT:w" s="T686">там</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_1686" n="HIAT:w" s="T687">ходил</ts>
                  <nts id="Seg_1687" n="HIAT:ip">.</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T701" id="Seg_1689" n="sc" s="T689">
               <ts e="T701" id="Seg_1691" n="HIAT:u" s="T689">
                  <nts id="Seg_1692" n="HIAT:ip">(</nts>
                  <ts e="T690" id="Seg_1694" n="HIAT:w" s="T689">Там=</ts>
                  <nts id="Seg_1695" n="HIAT:ip">)</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_1698" n="HIAT:w" s="T690">Там</ts>
                  <nts id="Seg_1699" n="HIAT:ip">,</nts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_1702" n="HIAT:w" s="T691">еслиф</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_1705" n="HIAT:w" s="T692">кому</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_1708" n="HIAT:w" s="T693">надо</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_1711" n="HIAT:w" s="T694">учить</ts>
                  <nts id="Seg_1712" n="HIAT:ip">,</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_1715" n="HIAT:w" s="T695">пускай</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_1718" n="HIAT:w" s="T696">наймет</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_1721" n="HIAT:w" s="T697">себе</ts>
                  <nts id="Seg_1722" n="HIAT:ip">,</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_1725" n="HIAT:w" s="T698">по-русски</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_1728" n="HIAT:w" s="T699">учил</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_1731" n="HIAT:w" s="T700">учитель</ts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T703" id="Seg_1734" n="sc" s="T702">
               <ts e="T703" id="Seg_1736" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_1738" n="HIAT:w" s="T702">Так</ts>
                  <nts id="Seg_1739" n="HIAT:ip">.</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T727" id="Seg_1741" n="sc" s="T722">
               <ts e="T727" id="Seg_1743" n="HIAT:u" s="T722">
                  <ts e="T724" id="Seg_1745" n="HIAT:w" s="T722">Pʼaŋdəsʼtə</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_1748" n="HIAT:w" s="T724">ej</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_1751" n="HIAT:w" s="T725">moliam</ts>
                  <nts id="Seg_1752" n="HIAT:ip">,</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_1755" n="HIAT:w" s="T726">a</ts>
                  <nts id="Seg_1756" n="HIAT:ip">…</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T736" id="Seg_1758" n="sc" s="T728">
               <ts e="T736" id="Seg_1760" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_1762" n="HIAT:w" s="T728">A</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_1765" n="HIAT:w" s="T729">moliam</ts>
                  <nts id="Seg_1766" n="HIAT:ip">,</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_1769" n="HIAT:w" s="T730">knižkam</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_1772" n="HIAT:w" s="T731">măndliam</ts>
                  <nts id="Seg_1773" n="HIAT:ip">,</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_1776" n="HIAT:w" s="T732">ĭmbi</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_1779" n="HIAT:w" s="T733">dĭn</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_1782" n="HIAT:w" s="T734">ige</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_1785" n="HIAT:w" s="T735">pʼaŋdona</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T741" id="Seg_1788" n="sc" s="T737">
               <ts e="T741" id="Seg_1790" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_1792" n="HIAT:w" s="T737">A</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_1795" n="HIAT:w" s="T738">pʼaŋdəsʼtə</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_1798" n="HIAT:w" s="T739">ej</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_1801" n="HIAT:w" s="T740">moliam</ts>
                  <nts id="Seg_1802" n="HIAT:ip">.</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T762" id="Seg_1804" n="sc" s="T758">
               <ts e="T762" id="Seg_1806" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_1808" n="HIAT:w" s="T758">Они</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_1811" n="HIAT:w" s="T759">много</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_1814" n="HIAT:w" s="T760">не</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_1817" n="HIAT:w" s="T761">жили</ts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T766" id="Seg_1820" n="sc" s="T763">
               <ts e="T766" id="Seg_1822" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_1824" n="HIAT:w" s="T763">Надо</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_1827" n="HIAT:w" s="T764">говорить</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_1830" n="HIAT:w" s="T765">по-своему</ts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T775" id="Seg_1833" n="sc" s="T770">
               <ts e="T775" id="Seg_1835" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_1837" n="HIAT:w" s="T770">Dĭzeŋ</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_1840" n="HIAT:w" s="T771">iʔgö</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_1843" n="HIAT:w" s="T772">ej</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1845" n="HIAT:ip">(</nts>
                  <ts e="T774" id="Seg_1847" n="HIAT:w" s="T773">amnobi=</ts>
                  <nts id="Seg_1848" n="HIAT:ip">)</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_1851" n="HIAT:w" s="T774">amnolaʔpiʔi</ts>
                  <nts id="Seg_1852" n="HIAT:ip">.</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T782" id="Seg_1854" n="sc" s="T776">
               <ts e="T780" id="Seg_1856" n="HIAT:u" s="T776">
                  <ts e="T777" id="Seg_1858" n="HIAT:w" s="T776">Dĭzeŋ</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_1861" n="HIAT:w" s="T777">üge</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_1864" n="HIAT:w" s="T778">nʼiʔneŋ</ts>
                  <nts id="Seg_1865" n="HIAT:ip">,</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_1868" n="HIAT:w" s="T779">nʼineŋ</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_1872" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_1874" n="HIAT:w" s="T780">Turazaŋdə</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_1877" n="HIAT:w" s="T781">nagobiʔi</ts>
                  <nts id="Seg_1878" n="HIAT:ip">.</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T784" id="Seg_1880" n="sc" s="T783">
               <ts e="T784" id="Seg_1882" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_1884" n="HIAT:w" s="T783">Kănnambiʔi</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T790" id="Seg_1887" n="sc" s="T785">
               <ts e="T790" id="Seg_1889" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_1891" n="HIAT:w" s="T785">I</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_1894" n="HIAT:w" s="T786">dĭgəttə</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_1897" n="HIAT:w" s="T787">dĭzeŋdə</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1899" n="HIAT:ip">(</nts>
                  <ts e="T789" id="Seg_1901" n="HIAT:w" s="T788">dʼazirzittə</ts>
                  <nts id="Seg_1902" n="HIAT:ip">)</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_1905" n="HIAT:w" s="T789">nagobi</ts>
                  <nts id="Seg_1906" n="HIAT:ip">.</nts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T793" id="Seg_1908" n="sc" s="T791">
               <ts e="T793" id="Seg_1910" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_1912" n="HIAT:w" s="T791">Dĭzeŋ</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_1915" n="HIAT:w" s="T792">külambiʔi</ts>
                  <nts id="Seg_1916" n="HIAT:ip">.</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T797" id="Seg_1918" n="sc" s="T794">
               <ts e="T797" id="Seg_1920" n="HIAT:u" s="T794">
                  <nts id="Seg_1921" n="HIAT:ip">(</nts>
                  <ts e="T795" id="Seg_1923" n="HIAT:w" s="T794">A-</ts>
                  <nts id="Seg_1924" n="HIAT:ip">)</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_1927" n="HIAT:w" s="T795">Amga</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_1930" n="HIAT:w" s="T796">amnolaʔbəʔi</ts>
                  <nts id="Seg_1931" n="HIAT:ip">.</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T804" id="Seg_1933" n="sc" s="T800">
               <ts e="T804" id="Seg_1935" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_1937" n="HIAT:w" s="T800">Miʔnʼibeʔ</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_1940" n="HIAT:w" s="T801">sĭre</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_1943" n="HIAT:w" s="T802">pe</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_1946" n="HIAT:w" s="T803">nagobi</ts>
                  <nts id="Seg_1947" n="HIAT:ip">.</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T809" id="Seg_1949" n="sc" s="T805">
               <ts e="T809" id="Seg_1951" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_1953" n="HIAT:w" s="T805">Dĭzeŋ</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_1956" n="HIAT:w" s="T806">nuldəbiʔi</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_1959" n="HIAT:w" s="T807">teʔtə</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_1962" n="HIAT:w" s="T808">pa</ts>
                  <nts id="Seg_1963" n="HIAT:ip">.</nts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T813" id="Seg_1965" n="sc" s="T810">
               <ts e="T813" id="Seg_1967" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_1969" n="HIAT:w" s="T810">I</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_1972" n="HIAT:w" s="T811">dĭn</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_1975" n="HIAT:w" s="T812">embiʔi</ts>
                  <nts id="Seg_1976" n="HIAT:ip">.</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T829" id="Seg_1978" n="sc" s="T814">
               <ts e="T820" id="Seg_1980" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_1982" n="HIAT:w" s="T814">Dĭgəttə</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1984" n="HIAT:ip">(</nts>
                  <ts e="T816" id="Seg_1986" n="HIAT:w" s="T815">dĭ=</ts>
                  <nts id="Seg_1987" n="HIAT:ip">)</nts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_1990" n="HIAT:w" s="T816">dĭbər</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_1993" n="HIAT:w" s="T817">nʼuʔtə</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_1996" n="HIAT:w" s="T818">piʔi</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_1999" n="HIAT:w" s="T819">embiʔi</ts>
                  <nts id="Seg_2000" n="HIAT:ip">.</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T829" id="Seg_2003" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2005" n="HIAT:w" s="T820">Dĭgəttə</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2008" n="HIAT:w" s="T821">dĭ</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2011" n="HIAT:w" s="T822">paʔi</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2013" n="HIAT:ip">(</nts>
                  <ts e="T824" id="Seg_2015" n="HIAT:w" s="T823">jaʔ-</ts>
                  <nts id="Seg_2016" n="HIAT:ip">)</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2019" n="HIAT:w" s="T824">jaʔpiʔi</ts>
                  <nts id="Seg_2020" n="HIAT:ip">,</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2023" n="HIAT:w" s="T825">dĭ</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2026" n="HIAT:w" s="T826">saməʔluʔpi</ts>
                  <nts id="Seg_2027" n="HIAT:ip">,</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2030" n="HIAT:w" s="T827">dĭzem</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2033" n="HIAT:w" s="T828">pʼaŋdluʔpi</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T835" id="Seg_2036" n="sc" s="T830">
               <ts e="T835" id="Seg_2038" n="HIAT:u" s="T830">
                  <nts id="Seg_2039" n="HIAT:ip">(</nts>
                  <ts e="T831" id="Seg_2041" n="HIAT:w" s="T830">Bostə</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2044" n="HIAT:w" s="T831">n-</ts>
                  <nts id="Seg_2045" n="HIAT:ip">)</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2048" n="HIAT:w" s="T832">Bostə</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2050" n="HIAT:ip">(</nts>
                  <ts e="T834" id="Seg_2052" n="HIAT:w" s="T833">nuldəzi</ts>
                  <nts id="Seg_2053" n="HIAT:ip">)</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2056" n="HIAT:w" s="T834">kutlaʔpiʔi</ts>
                  <nts id="Seg_2057" n="HIAT:ip">.</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T839" id="Seg_2059" n="sc" s="T836">
               <ts e="T839" id="Seg_2061" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_2063" n="HIAT:w" s="T836">Самих</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2066" n="HIAT:w" s="T837">себя</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2069" n="HIAT:w" s="T838">убивали</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T856" id="Seg_2072" n="sc" s="T840">
               <ts e="T856" id="Seg_2074" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_2076" n="HIAT:w" s="T840">Когда</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2079" n="HIAT:w" s="T841">березы</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2082" n="HIAT:w" s="T842">там</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2085" n="HIAT:w" s="T843">не</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2088" n="HIAT:w" s="T844">было</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2090" n="HIAT:ip">(</nts>
                  <ts e="T846" id="Seg_2092" n="HIAT:w" s="T845">березы=</ts>
                  <nts id="Seg_2093" n="HIAT:ip">)</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2096" n="HIAT:w" s="T846">береза</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2099" n="HIAT:w" s="T847">выросла</ts>
                  <nts id="Seg_2100" n="HIAT:ip">,</nts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2103" n="HIAT:w" s="T848">они</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2106" n="HIAT:w" s="T849">сказали:</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2108" n="HIAT:ip">"</nts>
                  <ts e="T851" id="Seg_2110" n="HIAT:w" s="T850">Русские</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2113" n="HIAT:w" s="T851">люди</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2116" n="HIAT:w" s="T852">придут</ts>
                  <nts id="Seg_2117" n="HIAT:ip">,</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2120" n="HIAT:w" s="T853">нам</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2123" n="HIAT:w" s="T854">плохо</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2126" n="HIAT:w" s="T855">будет</ts>
                  <nts id="Seg_2127" n="HIAT:ip">"</nts>
                  <nts id="Seg_2128" n="HIAT:ip">.</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T868" id="Seg_2130" n="sc" s="T857">
               <ts e="T868" id="Seg_2132" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_2134" n="HIAT:w" s="T857">Делали</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2136" n="HIAT:ip">(</nts>
                  <ts e="T859" id="Seg_2138" n="HIAT:w" s="T858">алачин</ts>
                  <nts id="Seg_2139" n="HIAT:ip">)</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2142" n="HIAT:w" s="T859">такие</ts>
                  <nts id="Seg_2143" n="HIAT:ip">,</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2146" n="HIAT:w" s="T860">туда</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2149" n="HIAT:w" s="T861">камни</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2152" n="HIAT:w" s="T862">накладали</ts>
                  <nts id="Seg_2153" n="HIAT:ip">,</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2156" n="HIAT:w" s="T863">и</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2159" n="HIAT:w" s="T864">подрубали</ts>
                  <nts id="Seg_2160" n="HIAT:ip">,</nts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2163" n="HIAT:w" s="T865">и</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2166" n="HIAT:w" s="T866">их</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2169" n="HIAT:w" s="T867">задавляло</ts>
                  <nts id="Seg_2170" n="HIAT:ip">.</nts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T877" id="Seg_2172" n="sc" s="T870">
               <ts e="T877" id="Seg_2174" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_2176" n="HIAT:w" s="T870">Вот</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2179" n="HIAT:w" s="T871">они</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2182" n="HIAT:w" s="T872">погребли</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2185" n="HIAT:w" s="T873">сами</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2188" n="HIAT:w" s="T874">себя</ts>
                  <nts id="Seg_2189" n="HIAT:ip">,</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2192" n="HIAT:w" s="T875">людей</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2195" n="HIAT:w" s="T876">боялись</ts>
                  <nts id="Seg_2196" n="HIAT:ip">.</nts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T884" id="Seg_2198" n="sc" s="T878">
               <ts e="T884" id="Seg_2200" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_2202" n="HIAT:w" s="T878">Это</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2204" n="HIAT:ip">(</nts>
                  <ts e="T880" id="Seg_2206" n="HIAT:w" s="T879">рассказывали=</ts>
                  <nts id="Seg_2207" n="HIAT:ip">)</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2210" n="HIAT:w" s="T880">рассказывали</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2213" n="HIAT:w" s="T881">тоже</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2216" n="HIAT:w" s="T882">старые</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2219" n="HIAT:w" s="T883">люди</ts>
                  <nts id="Seg_2220" n="HIAT:ip">.</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T901" id="Seg_2222" n="sc" s="T892">
               <ts e="T901" id="Seg_2224" n="HIAT:u" s="T892">
                  <ts e="T893" id="Seg_2226" n="HIAT:w" s="T892">Nu</ts>
                  <nts id="Seg_2227" n="HIAT:ip">,</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2230" n="HIAT:w" s="T893">dʼü</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2232" n="HIAT:ip">(</nts>
                  <ts e="T895" id="Seg_2234" n="HIAT:w" s="T894">dĭl-</ts>
                  <nts id="Seg_2235" n="HIAT:ip">)</nts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_2238" n="HIAT:w" s="T895">tĭlbiʔi</ts>
                  <nts id="Seg_2239" n="HIAT:ip">,</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_2242" n="HIAT:w" s="T896">dĭgəttə</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_2245" n="HIAT:w" s="T897">pa</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2247" n="HIAT:ip">(</nts>
                  <ts e="T899" id="Seg_2249" n="HIAT:w" s="T898">ambiʔi</ts>
                  <nts id="Seg_2250" n="HIAT:ip">)</nts>
                  <nts id="Seg_2251" n="HIAT:ip">,</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_2254" n="HIAT:w" s="T899">pa</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2257" n="HIAT:w" s="T900">jaʔpiʔi</ts>
                  <nts id="Seg_2258" n="HIAT:ip">.</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T911" id="Seg_2260" n="sc" s="T902">
               <ts e="T911" id="Seg_2262" n="HIAT:u" s="T902">
                  <ts e="T903" id="Seg_2264" n="HIAT:w" s="T902">Dĭgəttə</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_2267" n="HIAT:w" s="T903">šide</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_2270" n="HIAT:w" s="T904">abiʔi</ts>
                  <nts id="Seg_2271" n="HIAT:ip">,</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2274" n="HIAT:w" s="T905">i</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_2277" n="HIAT:w" s="T906">dĭbər</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_2280" n="HIAT:w" s="T907">dĭn</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_2283" n="HIAT:w" s="T908">tože</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2286" n="HIAT:w" s="T909">baltuzi</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2289" n="HIAT:w" s="T910">jaʔpiʔi</ts>
                  <nts id="Seg_2290" n="HIAT:ip">.</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T917" id="Seg_2292" n="sc" s="T912">
               <ts e="T917" id="Seg_2294" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_2296" n="HIAT:w" s="T912">Dĭgəttə</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_2299" n="HIAT:w" s="T913">dĭm</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_2302" n="HIAT:w" s="T914">dĭbər</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_2305" n="HIAT:w" s="T915">embiʔi</ts>
                  <nts id="Seg_2306" n="HIAT:ip">,</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_2309" n="HIAT:w" s="T916">kumbiʔi</ts>
                  <nts id="Seg_2310" n="HIAT:ip">.</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T920" id="Seg_2312" n="sc" s="T918">
               <ts e="T920" id="Seg_2314" n="HIAT:u" s="T918">
                  <ts e="T919" id="Seg_2316" n="HIAT:w" s="T918">Embiʔi</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_2319" n="HIAT:w" s="T919">dʼünə</ts>
                  <nts id="Seg_2320" n="HIAT:ip">.</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T924" id="Seg_2322" n="sc" s="T921">
               <ts e="T924" id="Seg_2324" n="HIAT:u" s="T921">
                  <ts e="T922" id="Seg_2326" n="HIAT:w" s="T921">I</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_2329" n="HIAT:w" s="T922">krospa</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_2332" n="HIAT:w" s="T923">abiʔi</ts>
                  <nts id="Seg_2333" n="HIAT:ip">.</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T935" id="Seg_2335" n="sc" s="T925">
               <ts e="T935" id="Seg_2337" n="HIAT:u" s="T925">
                  <ts e="T926" id="Seg_2339" n="HIAT:w" s="T925">I</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_2342" n="HIAT:w" s="T926">dĭgəttə</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_2345" n="HIAT:w" s="T927">maːʔndə</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2347" n="HIAT:ip">(</nts>
                  <ts e="T929" id="Seg_2349" n="HIAT:w" s="T928">so-</ts>
                  <nts id="Seg_2350" n="HIAT:ip">)</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_2353" n="HIAT:w" s="T929">šobiʔi</ts>
                  <nts id="Seg_2354" n="HIAT:ip">,</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2356" n="HIAT:ip">(</nts>
                  <ts e="T931" id="Seg_2358" n="HIAT:w" s="T930">mĭ</ts>
                  <nts id="Seg_2359" n="HIAT:ip">)</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_2362" n="HIAT:w" s="T931">mĭnzerleʔbəʔjə</ts>
                  <nts id="Seg_2363" n="HIAT:ip">,</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_2366" n="HIAT:w" s="T932">i</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_2369" n="HIAT:w" s="T933">dĭgəttə</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_2372" n="HIAT:w" s="T934">amnaʔbəʔjə</ts>
                  <nts id="Seg_2373" n="HIAT:ip">.</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T939" id="Seg_2375" n="sc" s="T936">
               <ts e="T939" id="Seg_2377" n="HIAT:u" s="T936">
                  <ts e="T937" id="Seg_2379" n="HIAT:w" s="T936">I</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_2382" n="HIAT:w" s="T937">bar</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2384" n="HIAT:ip">(</nts>
                  <ts e="T939" id="Seg_2386" n="HIAT:w" s="T938">kulandə</ts>
                  <nts id="Seg_2387" n="HIAT:ip">)</nts>
                  <nts id="Seg_2388" n="HIAT:ip">.</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T944" id="Seg_2390" n="sc" s="T942">
               <ts e="T944" id="Seg_2392" n="HIAT:u" s="T942">
                  <nts id="Seg_2393" n="HIAT:ip">(</nts>
                  <nts id="Seg_2394" n="HIAT:ip">(</nts>
                  <ats e="T944" id="Seg_2395" n="HIAT:non-pho" s="T942">…</ats>
                  <nts id="Seg_2396" n="HIAT:ip">)</nts>
                  <nts id="Seg_2397" n="HIAT:ip">)</nts>
                  <nts id="Seg_2398" n="HIAT:ip">.</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T953" id="Seg_2400" n="sc" s="T946">
               <ts e="T953" id="Seg_2402" n="HIAT:u" s="T946">
                  <ts e="T953" id="Seg_2404" n="HIAT:w" s="T946">По-русски</ts>
                  <nts id="Seg_2405" n="HIAT:ip">.</nts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T957" id="Seg_2407" n="sc" s="T954">
               <ts e="T957" id="Seg_2409" n="HIAT:u" s="T954">
                  <ts e="T956" id="Seg_2411" n="HIAT:w" s="T954">Пили</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_2414" n="HIAT:w" s="T956">же</ts>
                  <nts id="Seg_2415" n="HIAT:ip">.</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T980" id="Seg_2417" n="sc" s="T959">
               <ts e="T976" id="Seg_2419" n="HIAT:u" s="T959">
                  <nts id="Seg_2420" n="HIAT:ip">(</nts>
                  <ts e="T961" id="Seg_2422" n="HIAT:w" s="T959">По-ру-</ts>
                  <nts id="Seg_2423" n="HIAT:ip">)</nts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_2426" n="HIAT:w" s="T961">По-русски</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_2429" n="HIAT:w" s="T963">сказать</ts>
                  <nts id="Seg_2430" n="HIAT:ip">,</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_2433" n="HIAT:w" s="T965">они</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_2436" n="HIAT:w" s="T966">отрежут</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_2439" n="HIAT:w" s="T967">от</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_2442" n="HIAT:w" s="T968">лесины</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_2445" n="HIAT:w" s="T969">такую</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_2448" n="HIAT:w" s="T970">чурку</ts>
                  <nts id="Seg_2449" n="HIAT:ip">,</nts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_2452" n="HIAT:w" s="T971">расколят</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_2455" n="HIAT:w" s="T972">пополам</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_2458" n="HIAT:w" s="T973">и</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_2461" n="HIAT:w" s="T974">выдолбят</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_2464" n="HIAT:w" s="T975">туды</ts>
                  <nts id="Seg_2465" n="HIAT:ip">.</nts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_2468" n="HIAT:u" s="T976">
                  <ts e="T977" id="Seg_2470" n="HIAT:w" s="T976">Не</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_2473" n="HIAT:w" s="T977">делали</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_2476" n="HIAT:w" s="T978">с</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_2479" n="HIAT:w" s="T979">досок</ts>
                  <nts id="Seg_2480" n="HIAT:ip">.</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T984" id="Seg_2482" n="sc" s="T982">
               <ts e="T984" id="Seg_2484" n="HIAT:u" s="T982">
                  <ts e="T983" id="Seg_2486" n="HIAT:w" s="T982">Клали</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_2489" n="HIAT:w" s="T983">туда</ts>
                  <nts id="Seg_2490" n="HIAT:ip">.</nts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1001" id="Seg_2492" n="sc" s="T999">
               <ts e="T1001" id="Seg_2494" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_2496" n="HIAT:w" s="T999">Ставили</ts>
                  <nts id="Seg_2497" n="HIAT:ip">,</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_2500" n="HIAT:w" s="T1000">наверное</ts>
                  <nts id="Seg_2501" n="HIAT:ip">.</nts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1011" id="Seg_2503" n="sc" s="T1002">
               <ts e="T1011" id="Seg_2505" n="HIAT:u" s="T1002">
                  <ts e="T1003" id="Seg_2507" n="HIAT:w" s="T1002">Ну</ts>
                  <nts id="Seg_2508" n="HIAT:ip">,</nts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_2511" n="HIAT:w" s="T1003">я</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_2514" n="HIAT:w" s="T1004">не</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_2517" n="HIAT:w" s="T1005">помню</ts>
                  <nts id="Seg_2518" n="HIAT:ip">,</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_2521" n="HIAT:w" s="T1006">это</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_2524" n="HIAT:w" s="T1007">при</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_2527" n="HIAT:w" s="T1008">мне</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_2530" n="HIAT:w" s="T1009">ставили</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_2533" n="HIAT:w" s="T1010">кресты</ts>
                  <nts id="Seg_2534" n="HIAT:ip">.</nts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1019" id="Seg_2536" n="sc" s="T1013">
               <ts e="T1019" id="Seg_2538" n="HIAT:u" s="T1013">
                  <ts e="T1014" id="Seg_2540" n="HIAT:w" s="T1013">А</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_2543" n="HIAT:w" s="T1014">их</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_2546" n="HIAT:w" s="T1015">не</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_2549" n="HIAT:w" s="T1016">помню</ts>
                  <nts id="Seg_2550" n="HIAT:ip">,</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_2553" n="HIAT:w" s="T1017">не</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_2556" n="HIAT:w" s="T1018">знаю</ts>
                  <nts id="Seg_2557" n="HIAT:ip">.</nts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1050" id="Seg_2559" n="sc" s="T1020">
               <ts e="T1050" id="Seg_2561" n="HIAT:u" s="T1020">
                  <nts id="Seg_2562" n="HIAT:ip">(</nts>
                  <ts e="T1022" id="Seg_2564" n="HIAT:w" s="T1020">Вот=</ts>
                  <nts id="Seg_2565" n="HIAT:ip">)</nts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_2568" n="HIAT:w" s="T1022">Вот</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_2571" n="HIAT:w" s="T1023">ты</ts>
                  <nts id="Seg_2572" n="HIAT:ip">,</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_2575" n="HIAT:w" s="T1024">вот</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_2578" n="HIAT:w" s="T1025">где</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_2581" n="HIAT:w" s="T1026">туто-ка</ts>
                  <nts id="Seg_2582" n="HIAT:ip">,</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_2585" n="HIAT:w" s="T1027">на</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_2588" n="HIAT:w" s="T1028">бугре</ts>
                  <nts id="Seg_2589" n="HIAT:ip">,</nts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_2592" n="HIAT:w" s="T1029">как</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_2595" n="HIAT:w" s="T1030">тут</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_2598" n="HIAT:w" s="T1031">ямы</ts>
                  <nts id="Seg_2599" n="HIAT:ip">,</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_2602" n="HIAT:w" s="T1032">сюды</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_2605" n="HIAT:w" s="T1033">переходишь</ts>
                  <nts id="Seg_2606" n="HIAT:ip">,</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_2609" n="HIAT:w" s="T1034">тут</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_2612" n="HIAT:w" s="T1035">на</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_2615" n="HIAT:w" s="T1036">углу</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_2618" n="HIAT:w" s="T1037">было</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_2621" n="HIAT:w" s="T1038">кладбище</ts>
                  <nts id="Seg_2622" n="HIAT:ip">,</nts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_2625" n="HIAT:w" s="T1039">и</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_2628" n="HIAT:w" s="T1040">туда</ts>
                  <nts id="Seg_2629" n="HIAT:ip">,</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_2632" n="HIAT:w" s="T1043">там</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_2635" n="HIAT:w" s="T1045">за</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2637" n="HIAT:ip">(</nts>
                  <ts e="T1050" id="Seg_2639" n="HIAT:w" s="T1048">бугром</ts>
                  <nts id="Seg_2640" n="HIAT:ip">)</nts>
                  <nts id="Seg_2641" n="HIAT:ip">.</nts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1073" id="Seg_2643" n="sc" s="T1051">
               <ts e="T1073" id="Seg_2645" n="HIAT:u" s="T1051">
                  <ts e="T1052" id="Seg_2647" n="HIAT:w" s="T1051">Давнишнее</ts>
                  <nts id="Seg_2648" n="HIAT:ip">,</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_2651" n="HIAT:w" s="T1052">а</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_2654" n="HIAT:w" s="T1053">потом</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_2657" n="HIAT:w" s="T1054">там</ts>
                  <nts id="Seg_2658" n="HIAT:ip">,</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_2661" n="HIAT:w" s="T1055">где</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_2664" n="HIAT:w" s="T1056">ты</ts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_2667" n="HIAT:w" s="T1057">на</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_2670" n="HIAT:w" s="T1058">краю</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_2673" n="HIAT:w" s="T1059">снимал</ts>
                  <nts id="Seg_2674" n="HIAT:ip">,</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_2677" n="HIAT:w" s="T1060">там</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_2680" n="HIAT:w" s="T1061">еще</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_2683" n="HIAT:w" s="T1062">стара</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_2686" n="HIAT:w" s="T1063">стойба</ts>
                  <nts id="Seg_2687" n="HIAT:ip">,</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_2690" n="HIAT:w" s="T1064">там</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_2693" n="HIAT:w" s="T1065">тоже</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_2696" n="HIAT:w" s="T1066">у</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_2699" n="HIAT:w" s="T1067">их</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_2702" n="HIAT:w" s="T1068">кладбище</ts>
                  <nts id="Seg_2703" n="HIAT:ip">,</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_2706" n="HIAT:w" s="T1069">это</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_2709" n="HIAT:w" s="T1070">я</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_2712" n="HIAT:w" s="T1071">видала</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_2715" n="HIAT:w" s="T1072">там</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1087" id="Seg_2718" n="sc" s="T1074">
               <ts e="T1087" id="Seg_2720" n="HIAT:u" s="T1074">
                  <nts id="Seg_2721" n="HIAT:ip">(</nts>
                  <ts e="T1076" id="Seg_2723" n="HIAT:w" s="T1074">Кресто-</ts>
                  <nts id="Seg_2724" n="HIAT:ip">)</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_2727" n="HIAT:w" s="T1076">Там</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_2730" n="HIAT:w" s="T1079">крестов</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_2733" n="HIAT:w" s="T1081">не</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_2736" n="HIAT:w" s="T1083">было</ts>
                  <nts id="Seg_2737" n="HIAT:ip">,</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_2740" n="HIAT:w" s="T1084">уже</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_2743" n="HIAT:w" s="T1085">только</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_2746" n="HIAT:w" s="T1086">ямки</ts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1108" id="Seg_2749" n="sc" s="T1088">
               <ts e="T1108" id="Seg_2751" n="HIAT:u" s="T1088">
                  <ts e="T1089" id="Seg_2753" n="HIAT:w" s="T1088">Называли</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_2756" n="HIAT:w" s="T1089">стара</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_2759" n="HIAT:w" s="T1090">стойба</ts>
                  <nts id="Seg_2760" n="HIAT:ip">,</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_2763" n="HIAT:w" s="T1091">как</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_2766" n="HIAT:w" s="T1092">они</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_2769" n="HIAT:w" s="T1093">с</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_2772" n="HIAT:w" s="T1094">тайги</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_2775" n="HIAT:w" s="T1095">выезжали</ts>
                  <nts id="Seg_2776" n="HIAT:ip">,</nts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_2779" n="HIAT:w" s="T1096">и</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_2782" n="HIAT:w" s="T1097">там</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_2785" n="HIAT:w" s="T1098">они</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_2788" n="HIAT:w" s="T1099">стояли</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_2791" n="HIAT:w" s="T1100">зимовали</ts>
                  <nts id="Seg_2792" n="HIAT:ip">,</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_2795" n="HIAT:w" s="T1101">а</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_2798" n="HIAT:w" s="T1102">потом</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_2801" n="HIAT:w" s="T1103">сюды</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_2804" n="HIAT:w" s="T1104">вот</ts>
                  <nts id="Seg_2805" n="HIAT:ip">,</nts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_2808" n="HIAT:w" s="T1105">где</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_2811" n="HIAT:w" s="T1106">наша</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_2814" n="HIAT:w" s="T1107">деревня</ts>
                  <nts id="Seg_2815" n="HIAT:ip">.</nts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1116" id="Seg_2817" n="sc" s="T1109">
               <ts e="T1116" id="Seg_2819" n="HIAT:u" s="T1109">
                  <ts e="T1110" id="Seg_2821" n="HIAT:w" s="T1109">Этот</ts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_2824" n="HIAT:w" s="T1110">ключ</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_2827" n="HIAT:w" s="T1111">нашли</ts>
                  <nts id="Seg_2828" n="HIAT:ip">,</nts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_2831" n="HIAT:w" s="T1112">он</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_2834" n="HIAT:w" s="T1113">никогда</ts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_2837" n="HIAT:w" s="T1114">не</ts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_2840" n="HIAT:w" s="T1115">мерзнет</ts>
                  <nts id="Seg_2841" n="HIAT:ip">.</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1126" id="Seg_2843" n="sc" s="T1117">
               <ts e="T1122" id="Seg_2845" n="HIAT:u" s="T1117">
                  <nts id="Seg_2846" n="HIAT:ip">(</nts>
                  <ts e="T1118" id="Seg_2848" n="HIAT:w" s="T1117">Ле-</ts>
                  <nts id="Seg_2849" n="HIAT:ip">)</nts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_2852" n="HIAT:w" s="T1118">Зимой</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_2855" n="HIAT:w" s="T1119">только</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_2858" n="HIAT:w" s="T1120">пар</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_2861" n="HIAT:w" s="T1121">идет</ts>
                  <nts id="Seg_2862" n="HIAT:ip">.</nts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1126" id="Seg_2865" n="HIAT:u" s="T1122">
                  <ts e="T1123" id="Seg_2867" n="HIAT:w" s="T1122">А</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2869" n="HIAT:ip">(</nts>
                  <ts e="T1124" id="Seg_2871" n="HIAT:w" s="T1123">ни-</ts>
                  <nts id="Seg_2872" n="HIAT:ip">)</nts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1125" id="Seg_2875" n="HIAT:w" s="T1124">лёду</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_2878" n="HIAT:w" s="T1125">нету-ка</ts>
                  <nts id="Seg_2879" n="HIAT:ip">.</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1134" id="Seg_2881" n="sc" s="T1128">
               <ts e="T1134" id="Seg_2883" n="HIAT:u" s="T1128">
                  <ts e="T1129" id="Seg_2885" n="HIAT:w" s="T1128">Сейчас</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_2888" n="HIAT:w" s="T1129">она</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1131" id="Seg_2891" n="HIAT:w" s="T1130">холоднее</ts>
                  <nts id="Seg_2892" n="HIAT:ip">,</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_2895" n="HIAT:w" s="T1131">как</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_2898" n="HIAT:w" s="T1132">и</ts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_2901" n="HIAT:w" s="T1133">зимой</ts>
                  <nts id="Seg_2902" n="HIAT:ip">.</nts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1138" id="Seg_2904" n="sc" s="T1135">
               <ts e="T1138" id="Seg_2906" n="HIAT:u" s="T1135">
                  <ts e="T1137" id="Seg_2908" n="HIAT:w" s="T1135">Зимой</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_2911" n="HIAT:w" s="T1137">теплая</ts>
                  <nts id="Seg_2912" n="HIAT:ip">.</nts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1153" id="Seg_2914" n="sc" s="T1139">
               <ts e="T1153" id="Seg_2916" n="HIAT:u" s="T1139">
                  <ts e="T1140" id="Seg_2918" n="HIAT:w" s="T1139">Вот</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_2921" n="HIAT:w" s="T1140">мерзла</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1142" id="Seg_2924" n="HIAT:w" s="T1141">тряпка</ts>
                  <nts id="Seg_2925" n="HIAT:ip">,</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1143" id="Seg_2928" n="HIAT:w" s="T1142">туды</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2930" n="HIAT:ip">(</nts>
                  <ts e="T1144" id="Seg_2932" n="HIAT:w" s="T1143">пос-</ts>
                  <nts id="Seg_2933" n="HIAT:ip">)</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_2936" n="HIAT:w" s="T1144">спустишь</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_2939" n="HIAT:w" s="T1145">в</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1147" id="Seg_2942" n="HIAT:w" s="T1146">речку</ts>
                  <nts id="Seg_2943" n="HIAT:ip">,</nts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_2946" n="HIAT:w" s="T1147">она</ts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_2949" n="HIAT:w" s="T1148">в</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_2952" n="HIAT:w" s="T1149">речке</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_2955" n="HIAT:w" s="T1150">растает</ts>
                  <nts id="Seg_2956" n="HIAT:ip">,</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_2959" n="HIAT:w" s="T1151">тряпка</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1153" id="Seg_2962" n="HIAT:w" s="T1152">морожена</ts>
                  <nts id="Seg_2963" n="HIAT:ip">.</nts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1158" id="Seg_2965" n="sc" s="T1155">
               <ts e="T1158" id="Seg_2967" n="HIAT:u" s="T1155">
                  <ts e="T1156" id="Seg_2969" n="HIAT:w" s="T1155">Так</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2971" n="HIAT:ip">(</nts>
                  <nts id="Seg_2972" n="HIAT:ip">(</nts>
                  <ats e="T1157" id="Seg_2973" n="HIAT:non-pho" s="T1156">…</ats>
                  <nts id="Seg_2974" n="HIAT:ip">)</nts>
                  <nts id="Seg_2975" n="HIAT:ip">)</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1158" id="Seg_2978" n="HIAT:w" s="T1157">мороз</ts>
                  <nts id="Seg_2979" n="HIAT:ip">.</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1168" id="Seg_2981" n="sc" s="T1160">
               <ts e="T1168" id="Seg_2983" n="HIAT:u" s="T1160">
                  <ts e="T1161" id="Seg_2985" n="HIAT:w" s="T1160">И</ts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1162" id="Seg_2988" n="HIAT:w" s="T1161">она</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1163" id="Seg_2991" n="HIAT:w" s="T1162">была</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2993" n="HIAT:ip">(</nts>
                  <nts id="Seg_2994" n="HIAT:ip">(</nts>
                  <ats e="T1164" id="Seg_2995" n="HIAT:non-pho" s="T1163">…</ats>
                  <nts id="Seg_2996" n="HIAT:ip">)</nts>
                  <nts id="Seg_2997" n="HIAT:ip">)</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2999" n="HIAT:ip">(</nts>
                  <ts e="T1165" id="Seg_3001" n="HIAT:w" s="T1164">талисы</ts>
                  <nts id="Seg_3002" n="HIAT:ip">)</nts>
                  <nts id="Seg_3003" n="HIAT:ip">,</nts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_3006" n="HIAT:w" s="T1165">талая</ts>
                  <nts id="Seg_3007" n="HIAT:ip">,</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3009" n="HIAT:ip">(</nts>
                  <ts e="T1168" id="Seg_3011" n="HIAT:w" s="T1167">талицы</ts>
                  <nts id="Seg_3012" n="HIAT:ip">)</nts>
                  <nts id="Seg_3013" n="HIAT:ip">.</nts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1193" id="Seg_3015" n="sc" s="T1187">
               <ts e="T1193" id="Seg_3017" n="HIAT:u" s="T1187">
                  <ts e="T1188" id="Seg_3019" n="HIAT:w" s="T1187">Ну</ts>
                  <nts id="Seg_3020" n="HIAT:ip">,</nts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1189" id="Seg_3023" n="HIAT:w" s="T1188">приходили</ts>
                  <nts id="Seg_3024" n="HIAT:ip">,</nts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1190" id="Seg_3027" n="HIAT:w" s="T1189">поминали</ts>
                  <nts id="Seg_3028" n="HIAT:ip">,</nts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_3031" n="HIAT:w" s="T1190">на</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_3034" n="HIAT:w" s="T1191">стол</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_3037" n="HIAT:w" s="T1192">собирали</ts>
                  <nts id="Seg_3038" n="HIAT:ip">.</nts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1210" id="Seg_3040" n="sc" s="T1194">
               <ts e="T1202" id="Seg_3042" n="HIAT:u" s="T1194">
                  <nts id="Seg_3043" n="HIAT:ip">(</nts>
                  <ts e="T1195" id="Seg_3045" n="HIAT:w" s="T1194">Нав-</ts>
                  <nts id="Seg_3046" n="HIAT:ip">)</nts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1196" id="Seg_3049" n="HIAT:w" s="T1195">Наварют</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1197" id="Seg_3052" n="HIAT:w" s="T1196">и</ts>
                  <nts id="Seg_3053" n="HIAT:ip">,</nts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_3056" n="HIAT:w" s="T1197">этого</ts>
                  <nts id="Seg_3057" n="HIAT:ip">,</nts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_3060" n="HIAT:w" s="T1198">и</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_3063" n="HIAT:w" s="T1199">киселю</ts>
                  <nts id="Seg_3064" n="HIAT:ip">,</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_3067" n="HIAT:w" s="T1200">и</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_3070" n="HIAT:w" s="T1201">хлебного</ts>
                  <nts id="Seg_3071" n="HIAT:ip">.</nts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1210" id="Seg_3074" n="HIAT:u" s="T1202">
                  <ts e="T1203" id="Seg_3076" n="HIAT:w" s="T1202">Ну</ts>
                  <nts id="Seg_3077" n="HIAT:ip">,</nts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_3080" n="HIAT:w" s="T1203">они</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1205" id="Seg_3083" n="HIAT:w" s="T1204">картофельного</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1206" id="Seg_3086" n="HIAT:w" s="T1205">тады</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1207" id="Seg_3089" n="HIAT:w" s="T1206">не</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1208" id="Seg_3092" n="HIAT:w" s="T1207">знали</ts>
                  <nts id="Seg_3093" n="HIAT:ip">,</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1209" id="Seg_3096" n="HIAT:w" s="T1208">хлебный</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1210" id="Seg_3099" n="HIAT:w" s="T1209">делали</ts>
                  <nts id="Seg_3100" n="HIAT:ip">.</nts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1214" id="Seg_3102" n="sc" s="T1211">
               <ts e="T1214" id="Seg_3104" n="HIAT:u" s="T1211">
                  <ts e="T1212" id="Seg_3106" n="HIAT:w" s="T1211">Лапшу</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_3109" n="HIAT:w" s="T1212">сами</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_3112" n="HIAT:w" s="T1213">делали</ts>
                  <nts id="Seg_3113" n="HIAT:ip">.</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1226" id="Seg_3115" n="sc" s="T1215">
               <ts e="T1226" id="Seg_3117" n="HIAT:u" s="T1215">
                  <ts e="T1216" id="Seg_3119" n="HIAT:w" s="T1215">Как</ts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1217" id="Seg_3122" n="HIAT:w" s="T1216">теперича</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_3125" n="HIAT:w" s="T1217">вот</ts>
                  <nts id="Seg_3126" n="HIAT:ip">,</nts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1219" id="Seg_3129" n="HIAT:w" s="T1218">лапша</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1220" id="Seg_3132" n="HIAT:w" s="T1219">есть</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_3135" n="HIAT:w" s="T1220">в</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_3138" n="HIAT:w" s="T1221">магазинах</ts>
                  <nts id="Seg_3139" n="HIAT:ip">,</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_3142" n="HIAT:w" s="T1222">а</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_3145" n="HIAT:w" s="T1223">они</ts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_3148" n="HIAT:w" s="T1224">сами</ts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_3151" n="HIAT:w" s="T1225">делали</ts>
                  <nts id="Seg_3152" n="HIAT:ip">.</nts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1230" id="Seg_3154" n="sc" s="T1227">
               <ts e="T1230" id="Seg_3156" n="HIAT:u" s="T1227">
                  <ts e="T1228" id="Seg_3158" n="HIAT:w" s="T1227">Не</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_3161" n="HIAT:w" s="T1228">знали</ts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1230" id="Seg_3164" n="HIAT:w" s="T1229">этого</ts>
                  <nts id="Seg_3165" n="HIAT:ip">.</nts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1276" id="Seg_3167" n="sc" s="T1270">
               <ts e="T1276" id="Seg_3169" n="HIAT:u" s="T1270">
                  <ts e="T1271" id="Seg_3171" n="HIAT:w" s="T1270">Nu</ts>
                  <nts id="Seg_3172" n="HIAT:ip">,</nts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1272" id="Seg_3175" n="HIAT:w" s="T1271">kamen</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_3178" n="HIAT:w" s="T1272">külalləj</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_3181" n="HIAT:w" s="T1273">kuza</ts>
                  <nts id="Seg_3182" n="HIAT:ip">,</nts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1275" id="Seg_3185" n="HIAT:w" s="T1274">dʼünə</ts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3187" n="HIAT:ip">(</nts>
                  <ts e="T1276" id="Seg_3189" n="HIAT:w" s="T1275">tĭlleʔbəʔə</ts>
                  <nts id="Seg_3190" n="HIAT:ip">)</nts>
                  <nts id="Seg_3191" n="HIAT:ip">.</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1284" id="Seg_3193" n="sc" s="T1277">
               <ts e="T1284" id="Seg_3195" n="HIAT:u" s="T1277">
                  <ts e="T1278" id="Seg_3197" n="HIAT:w" s="T1277">A</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3199" n="HIAT:ip">(</nts>
                  <ts e="T1279" id="Seg_3201" n="HIAT:w" s="T1278">dĭn</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1280" id="Seg_3204" n="HIAT:w" s="T1279">nʼuʔtə=</ts>
                  <nts id="Seg_3205" n="HIAT:ip">)</nts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_3208" n="HIAT:w" s="T1280">dux</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1282" id="Seg_3211" n="HIAT:w" s="T1281">dĭn</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1533" id="Seg_3214" n="HIAT:w" s="T1282">kalla</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_3217" n="HIAT:w" s="T1533">dʼürləj</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_3220" n="HIAT:w" s="T1283">nʼuʔtə</ts>
                  <nts id="Seg_3221" n="HIAT:ip">.</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1288" id="Seg_3223" n="sc" s="T1285">
               <ts e="T1288" id="Seg_3225" n="HIAT:u" s="T1285">
                  <ts e="T1286" id="Seg_3227" n="HIAT:w" s="T1285">Măn</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1287" id="Seg_3230" n="HIAT:w" s="T1286">dăre</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1288" id="Seg_3233" n="HIAT:w" s="T1287">tĭmnem</ts>
                  <nts id="Seg_3234" n="HIAT:ip">.</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1297" id="Seg_3236" n="sc" s="T1289">
               <ts e="T1297" id="Seg_3238" n="HIAT:u" s="T1289">
                  <ts e="T1290" id="Seg_3240" n="HIAT:w" s="T1289">Не</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1291" id="Seg_3243" n="HIAT:w" s="T1290">знаю</ts>
                  <nts id="Seg_3244" n="HIAT:ip">,</nts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1292" id="Seg_3247" n="HIAT:w" s="T1291">šində</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_3250" n="HIAT:w" s="T1292">kăde</ts>
                  <nts id="Seg_3251" n="HIAT:ip">,</nts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_3254" n="HIAT:w" s="T1293">a</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1295" id="Seg_3257" n="HIAT:w" s="T1294">măn</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1296" id="Seg_3260" n="HIAT:w" s="T1295">dăre</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1297" id="Seg_3263" n="HIAT:w" s="T1296">tĭmnem</ts>
                  <nts id="Seg_3264" n="HIAT:ip">.</nts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1304" id="Seg_3266" n="sc" s="T1298">
               <ts e="T1304" id="Seg_3268" n="HIAT:u" s="T1298">
                  <ts e="T1299" id="Seg_3270" n="HIAT:w" s="T1298">I</ts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_3273" n="HIAT:w" s="T1299">iam</ts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3275" n="HIAT:ip">(</nts>
                  <ts e="T1301" id="Seg_3277" n="HIAT:w" s="T1300">măndə=</ts>
                  <nts id="Seg_3278" n="HIAT:ip">)</nts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1302" id="Seg_3281" n="HIAT:w" s="T1301">dăre</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1303" id="Seg_3284" n="HIAT:w" s="T1302">măndə</ts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1304" id="Seg_3287" n="HIAT:w" s="T1303">măna</ts>
                  <nts id="Seg_3288" n="HIAT:ip">.</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1309" id="Seg_3290" n="sc" s="T1305">
               <ts e="T1309" id="Seg_3292" n="HIAT:u" s="T1305">
                  <ts e="T1306" id="Seg_3294" n="HIAT:w" s="T1305">Душа</ts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_3297" n="HIAT:w" s="T1306">dĭn</ts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1308" id="Seg_3300" n="HIAT:w" s="T1307">kalaj</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1309" id="Seg_3303" n="HIAT:w" s="T1308">kudajdə</ts>
                  <nts id="Seg_3304" n="HIAT:ip">.</nts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1330" id="Seg_3306" n="sc" s="T1312">
               <ts e="T1317" id="Seg_3308" n="HIAT:u" s="T1312">
                  <ts e="T1313" id="Seg_3310" n="HIAT:w" s="T1312">Kuzam</ts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1314" id="Seg_3313" n="HIAT:w" s="T1313">kudaj</ts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1315" id="Seg_3316" n="HIAT:w" s="T1314">abi</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1316" id="Seg_3319" n="HIAT:w" s="T1315">bostə</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1317" id="Seg_3322" n="HIAT:w" s="T1316">dʼügən</ts>
                  <nts id="Seg_3323" n="HIAT:ip">.</nts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1330" id="Seg_3326" n="HIAT:u" s="T1317">
                  <ts e="T1318" id="Seg_3328" n="HIAT:w" s="T1317">I</ts>
                  <nts id="Seg_3329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_3331" n="HIAT:w" s="T1318">dĭʔnə</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1320" id="Seg_3334" n="HIAT:w" s="T1319">bar</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3336" n="HIAT:ip">(</nts>
                  <nts id="Seg_3337" n="HIAT:ip">(</nts>
                  <ats e="T1321" id="Seg_3338" n="HIAT:non-pho" s="T1320">…</ats>
                  <nts id="Seg_3339" n="HIAT:ip">)</nts>
                  <nts id="Seg_3340" n="HIAT:ip">)</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_3343" n="HIAT:w" s="T1321">bostə</ts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_3346" n="HIAT:w" s="T1322">duxtə</ts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3348" n="HIAT:ip">(</nts>
                  <ts e="T1324" id="Seg_3350" n="HIAT:w" s="T1323">da-</ts>
                  <nts id="Seg_3351" n="HIAT:ip">)</nts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_3354" n="HIAT:w" s="T1324">mĭbi</ts>
                  <nts id="Seg_3355" n="HIAT:ip">,</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1326" id="Seg_3358" n="HIAT:w" s="T1325">i</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1327" id="Seg_3361" n="HIAT:w" s="T1326">dĭ</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1328" id="Seg_3364" n="HIAT:w" s="T1327">uʔbdəbi</ts>
                  <nts id="Seg_3365" n="HIAT:ip">,</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_3368" n="HIAT:w" s="T1328">kuza</ts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1330" id="Seg_3371" n="HIAT:w" s="T1329">molambi</ts>
                  <nts id="Seg_3372" n="HIAT:ip">.</nts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1339" id="Seg_3374" n="sc" s="T1331">
               <ts e="T1339" id="Seg_3376" n="HIAT:u" s="T1331">
                  <nts id="Seg_3377" n="HIAT:ip">(</nts>
                  <ts e="T1332" id="Seg_3379" n="HIAT:w" s="T1331">Понят-</ts>
                  <nts id="Seg_3380" n="HIAT:ip">)</nts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333" id="Seg_3383" n="HIAT:w" s="T1332">Понимаешь</ts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1334" id="Seg_3386" n="HIAT:w" s="T1333">это</ts>
                  <nts id="Seg_3387" n="HIAT:ip">,</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1335" id="Seg_3390" n="HIAT:w" s="T1334">чего</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1336" id="Seg_3393" n="HIAT:w" s="T1335">я</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_3396" n="HIAT:w" s="T1336">тебе</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1339" id="Seg_3399" n="HIAT:w" s="T1337">рассказала</ts>
                  <nts id="Seg_3400" n="HIAT:ip">?</nts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1368" id="Seg_3402" n="sc" s="T1348">
               <ts e="T1368" id="Seg_3404" n="HIAT:u" s="T1348">
                  <nts id="Seg_3405" n="HIAT:ip">(</nts>
                  <ts e="T1350" id="Seg_3407" n="HIAT:w" s="T1348">Это=</ts>
                  <nts id="Seg_3408" n="HIAT:ip">)</nts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1352" id="Seg_3411" n="HIAT:w" s="T1350">это</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_3414" n="HIAT:w" s="T1352">я</ts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1356" id="Seg_3417" n="HIAT:w" s="T1354">тебе</ts>
                  <nts id="Seg_3418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358" id="Seg_3420" n="HIAT:w" s="T1356">не</ts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_3423" n="HIAT:w" s="T1358">говорила</ts>
                  <nts id="Seg_3424" n="HIAT:ip">,</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_3427" n="HIAT:w" s="T1359">вот</ts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1362" id="Seg_3430" n="HIAT:w" s="T1360">это</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1363" id="Seg_3433" n="HIAT:w" s="T1362">вот</ts>
                  <nts id="Seg_3434" n="HIAT:ip">,</nts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_3437" n="HIAT:w" s="T1363">чего</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1366" id="Seg_3440" n="HIAT:w" s="T1365">сейчас</ts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_3443" n="HIAT:w" s="T1366">гоою</ts>
                  <nts id="Seg_3444" n="HIAT:ip">.</nts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1375" id="Seg_3446" n="sc" s="T1369">
               <ts e="T1375" id="Seg_3448" n="HIAT:u" s="T1369">
                  <ts e="T1370" id="Seg_3450" n="HIAT:w" s="T1369">Это</ts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1371" id="Seg_3453" n="HIAT:w" s="T1370">я</ts>
                  <nts id="Seg_3454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_3456" n="HIAT:w" s="T1371">с</ts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1373" id="Seg_3459" n="HIAT:w" s="T1372">Писания</ts>
                  <nts id="Seg_3460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1375" id="Seg_3462" n="HIAT:w" s="T1373">говорю</ts>
                  <nts id="Seg_3463" n="HIAT:ip">.</nts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1404" id="Seg_3465" n="sc" s="T1377">
               <ts e="T1404" id="Seg_3467" n="HIAT:u" s="T1377">
                  <nts id="Seg_3468" n="HIAT:ip">(</nts>
                  <ts e="T1378" id="Seg_3470" n="HIAT:w" s="T1377">Когда=</ts>
                  <nts id="Seg_3471" n="HIAT:ip">)</nts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1379" id="Seg_3474" n="HIAT:w" s="T1378">Когда</ts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1380" id="Seg_3477" n="HIAT:w" s="T1379">Господь</ts>
                  <nts id="Seg_3478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1381" id="Seg_3480" n="HIAT:w" s="T1380">очертил</ts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3482" n="HIAT:ip">(</nts>
                  <ts e="T1382" id="Seg_3484" n="HIAT:w" s="T1381">свою=</ts>
                  <nts id="Seg_3485" n="HIAT:ip">)</nts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_3488" n="HIAT:w" s="T1382">свою</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1384" id="Seg_3491" n="HIAT:w" s="T1383">и</ts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_3494" n="HIAT:w" s="T1384">вдунул</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_3497" n="HIAT:w" s="T1385">в</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1387" id="Seg_3500" n="HIAT:w" s="T1386">человека</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_3503" n="HIAT:w" s="T1387">на</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1389" id="Seg_3506" n="HIAT:w" s="T1388">земле</ts>
                  <nts id="Seg_3507" n="HIAT:ip">,</nts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1390" id="Seg_3510" n="HIAT:w" s="T1389">по</ts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1391" id="Seg_3513" n="HIAT:w" s="T1390">образу</ts>
                  <nts id="Seg_3514" n="HIAT:ip">,</nts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_3517" n="HIAT:w" s="T1391">по</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1393" id="Seg_3520" n="HIAT:w" s="T1392">подобию</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_3523" n="HIAT:w" s="T1393">своему</ts>
                  <nts id="Seg_3524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_3526" n="HIAT:w" s="T1394">очертил</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1396" id="Seg_3529" n="HIAT:w" s="T1395">и</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1397" id="Seg_3532" n="HIAT:w" s="T1396">вдунул</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1398" id="Seg_3535" n="HIAT:w" s="T1397">в</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_3538" n="HIAT:w" s="T1398">его</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_3541" n="HIAT:w" s="T1399">духа</ts>
                  <nts id="Seg_3542" n="HIAT:ip">,</nts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_3545" n="HIAT:w" s="T1400">и</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_3548" n="HIAT:w" s="T1401">он</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1403" id="Seg_3551" n="HIAT:w" s="T1402">стал</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1404" id="Seg_3554" n="HIAT:w" s="T1403">живой</ts>
                  <nts id="Seg_3555" n="HIAT:ip">.</nts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1412" id="Seg_3557" n="sc" s="T1406">
               <ts e="T1412" id="Seg_3559" n="HIAT:u" s="T1406">
                  <ts e="T1407" id="Seg_3561" n="HIAT:w" s="T1406">Так</ts>
                  <nts id="Seg_3562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3563" n="HIAT:ip">(</nts>
                  <ts e="T1408" id="Seg_3565" n="HIAT:w" s="T1407">я</ts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1409" id="Seg_3568" n="HIAT:w" s="T1408">это=</ts>
                  <nts id="Seg_3569" n="HIAT:ip">)</nts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_3572" n="HIAT:w" s="T1409">я</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1411" id="Seg_3575" n="HIAT:w" s="T1410">это</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3577" n="HIAT:ip">(</nts>
                  <ts e="T1412" id="Seg_3579" n="HIAT:w" s="T1411">передаю</ts>
                  <nts id="Seg_3580" n="HIAT:ip">)</nts>
                  <nts id="Seg_3581" n="HIAT:ip">.</nts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1453" id="Seg_3583" n="sc" s="T1451">
               <ts e="T1453" id="Seg_3585" n="HIAT:u" s="T1451">
                  <ts e="T1452" id="Seg_3587" n="HIAT:w" s="T1451">No</ts>
                  <nts id="Seg_3588" n="HIAT:ip">,</nts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1453" id="Seg_3591" n="HIAT:w" s="T1452">kabarləj</ts>
                  <nts id="Seg_3592" n="HIAT:ip">.</nts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1457" id="Seg_3594" n="sc" s="T1454">
               <ts e="T1457" id="Seg_3596" n="HIAT:u" s="T1454">
                  <ts e="T1455" id="Seg_3598" n="HIAT:w" s="T1454">Хватит</ts>
                  <nts id="Seg_3599" n="HIAT:ip">,</nts>
                  <nts id="Seg_3600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_3602" n="HIAT:w" s="T1455">говорю</ts>
                  <nts id="Seg_3603" n="HIAT:ip">.</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T12" id="Seg_3605" n="sc" s="T1">
               <ts e="T2" id="Seg_3607" n="e" s="T1">((…)) </ts>
               <ts e="T3" id="Seg_3609" n="e" s="T2">не </ts>
               <ts e="T4" id="Seg_3611" n="e" s="T3">могут </ts>
               <ts e="T5" id="Seg_3613" n="e" s="T4">найти, </ts>
               <ts e="T6" id="Seg_3615" n="e" s="T5">такой </ts>
               <ts e="T7" id="Seg_3617" n="e" s="T6">лес </ts>
               <ts e="T8" id="Seg_3619" n="e" s="T7">густой, </ts>
               <ts e="T9" id="Seg_3621" n="e" s="T8">пока </ts>
               <ts e="T10" id="Seg_3623" n="e" s="T9">сами </ts>
               <ts e="T12" id="Seg_3625" n="e" s="T10">придут. </ts>
            </ts>
            <ts e="T29" id="Seg_3626" n="sc" s="T13">
               <ts e="T14" id="Seg_3628" n="e" s="T13">(Та-) </ts>
               <ts e="T17" id="Seg_3630" n="e" s="T14">Таки </ts>
               <ts e="T18" id="Seg_3632" n="e" s="T17">горы, </ts>
               <ts e="T19" id="Seg_3634" n="e" s="T18">что </ts>
               <ts e="T20" id="Seg_3636" n="e" s="T19">нельзя </ts>
               <ts e="T21" id="Seg_3638" n="e" s="T20">взглянуть </ts>
               <ts e="T22" id="Seg_3640" n="e" s="T21">выше </ts>
               <ts e="T23" id="Seg_3642" n="e" s="T22">их, </ts>
               <ts e="T24" id="Seg_3644" n="e" s="T23">сколь </ts>
               <ts e="T25" id="Seg_3646" n="e" s="T24">раз </ts>
               <ts e="T26" id="Seg_3648" n="e" s="T25">это </ts>
               <ts e="T27" id="Seg_3650" n="e" s="T26">((…)) </ts>
               <ts e="T28" id="Seg_3652" n="e" s="T27">горы </ts>
               <ts e="T29" id="Seg_3654" n="e" s="T28">там. </ts>
            </ts>
            <ts e="T52" id="Seg_3655" n="sc" s="T49">
               <ts e="T51" id="Seg_3657" n="e" s="T49">Ну, </ts>
               <ts e="T52" id="Seg_3659" n="e" s="T51">как… </ts>
            </ts>
            <ts e="T62" id="Seg_3660" n="sc" s="T53">
               <ts e="T54" id="Seg_3662" n="e" s="T53">Sʼarbibaʔ, </ts>
               <ts e="T55" id="Seg_3664" n="e" s="T54">i </ts>
               <ts e="T56" id="Seg_3666" n="e" s="T55">dĭgəttə </ts>
               <ts e="T57" id="Seg_3668" n="e" s="T56">girgit </ts>
               <ts e="T58" id="Seg_3670" n="e" s="T57">nʼi </ts>
               <ts e="T59" id="Seg_3672" n="e" s="T58">măna </ts>
               <ts e="T60" id="Seg_3674" n="e" s="T59">šoləj, </ts>
               <ts e="T61" id="Seg_3676" n="e" s="T60">măn </ts>
               <ts e="T62" id="Seg_3678" n="e" s="T61">dĭʔnə. </ts>
            </ts>
            <ts e="T74" id="Seg_3679" n="sc" s="T63">
               <ts e="T64" id="Seg_3681" n="e" s="T63">I </ts>
               <ts e="T65" id="Seg_3683" n="e" s="T64">koʔbsaŋ </ts>
               <ts e="T66" id="Seg_3685" n="e" s="T65">iʔgö </ts>
               <ts e="T67" id="Seg_3687" n="e" s="T66">ibiʔi. </ts>
               <ts e="T68" id="Seg_3689" n="e" s="T67">Kamen </ts>
               <ts e="T69" id="Seg_3691" n="e" s="T68">(m-) </ts>
               <ts e="T70" id="Seg_3693" n="e" s="T69">măn </ts>
               <ts e="T71" id="Seg_3695" n="e" s="T70">ibiem </ts>
               <ts e="T72" id="Seg_3697" n="e" s="T71">dak, </ts>
               <ts e="T73" id="Seg_3699" n="e" s="T72">nagobiʔi </ts>
               <ts e="T74" id="Seg_3701" n="e" s="T73">nʼiʔi. </ts>
            </ts>
            <ts e="T89" id="Seg_3702" n="sc" s="T75">
               <ts e="T76" id="Seg_3704" n="e" s="T75">Măn </ts>
               <ts e="T77" id="Seg_3706" n="e" s="T76">mĭmbiem </ts>
               <ts e="T78" id="Seg_3708" n="e" s="T77">dĭbər, </ts>
               <ts e="T79" id="Seg_3710" n="e" s="T78">Pjankăftə </ts>
               <ts e="T80" id="Seg_3712" n="e" s="T79">tibinə </ts>
               <ts e="T81" id="Seg_3714" n="e" s="T80">mĭmbiem, </ts>
               <ts e="T82" id="Seg_3716" n="e" s="T81">dĭn </ts>
               <ts e="T83" id="Seg_3718" n="e" s="T82">(nʼibə) </ts>
               <ts e="T84" id="Seg_3720" n="e" s="T83">ibi, </ts>
               <ts e="T85" id="Seg_3722" n="e" s="T84">i </ts>
               <ts e="T86" id="Seg_3724" n="e" s="T85">(măna=) </ts>
               <ts e="T87" id="Seg_3726" n="e" s="T86">măn </ts>
               <ts e="T88" id="Seg_3728" n="e" s="T87">dĭbər </ts>
               <ts e="T89" id="Seg_3730" n="e" s="T88">maluʔpiam. </ts>
            </ts>
            <ts e="T96" id="Seg_3731" n="sc" s="T91">
               <ts e="T92" id="Seg_3733" n="e" s="T91">Вот </ts>
               <ts e="T93" id="Seg_3735" n="e" s="T92">как </ts>
               <ts e="T94" id="Seg_3737" n="e" s="T93">ты </ts>
               <ts e="T95" id="Seg_3739" n="e" s="T94">говоришь, </ts>
               <ts e="T96" id="Seg_3741" n="e" s="T95">((…)). </ts>
            </ts>
            <ts e="T119" id="Seg_3742" n="sc" s="T105">
               <ts e="T106" id="Seg_3744" n="e" s="T105">Nu, </ts>
               <ts e="T107" id="Seg_3746" n="e" s="T106">iat </ts>
               <ts e="T108" id="Seg_3748" n="e" s="T107">abat </ts>
               <ts e="T109" id="Seg_3750" n="e" s="T108">šoləj </ts>
               <ts e="T110" id="Seg_3752" n="e" s="T109">nʼin, </ts>
               <ts e="T111" id="Seg_3754" n="e" s="T110">(koʔbdonə), </ts>
               <ts e="T112" id="Seg_3756" n="e" s="T111">dĭgəttə </ts>
               <ts e="T113" id="Seg_3758" n="e" s="T112">(dĭ=) </ts>
               <ts e="T114" id="Seg_3760" n="e" s="T113">dĭʔnə </ts>
               <ts e="T115" id="Seg_3762" n="e" s="T114">mănliaʔi: </ts>
               <ts e="T116" id="Seg_3764" n="e" s="T115">"Kanaʔ </ts>
               <ts e="T117" id="Seg_3766" n="e" s="T116">miʔ </ts>
               <ts e="T118" id="Seg_3768" n="e" s="T117">nʼinə </ts>
               <ts e="T119" id="Seg_3770" n="e" s="T118">tibinə". </ts>
            </ts>
            <ts e="T131" id="Seg_3771" n="sc" s="T120">
               <ts e="T121" id="Seg_3773" n="e" s="T120">Abandə </ts>
               <ts e="T122" id="Seg_3775" n="e" s="T121">iandə </ts>
               <ts e="T123" id="Seg_3777" n="e" s="T122">surarləʔjə. </ts>
               <ts e="T124" id="Seg_3779" n="e" s="T123">Nada </ts>
               <ts e="T125" id="Seg_3781" n="e" s="T124">koʔbdom </ts>
               <ts e="T126" id="Seg_3783" n="e" s="T125">surarzittə, </ts>
               <ts e="T127" id="Seg_3785" n="e" s="T126">dĭgəttə </ts>
               <ts e="T128" id="Seg_3787" n="e" s="T127">šoləj </ts>
               <ts e="T129" id="Seg_3789" n="e" s="T128">koʔbdo. </ts>
               <ts e="T130" id="Seg_3791" n="e" s="T129">"Kalal </ts>
               <ts e="T131" id="Seg_3793" n="e" s="T130">tibinə?" </ts>
            </ts>
            <ts e="T135" id="Seg_3794" n="sc" s="T132">
               <ts e="T133" id="Seg_3796" n="e" s="T132">Dĭ </ts>
               <ts e="T134" id="Seg_3798" n="e" s="T133">măndə: </ts>
               <ts e="T135" id="Seg_3800" n="e" s="T134">"Kalam". </ts>
            </ts>
            <ts e="T141" id="Seg_3801" n="sc" s="T136">
               <ts e="T137" id="Seg_3803" n="e" s="T136">Nu, </ts>
               <ts e="T138" id="Seg_3805" n="e" s="T137">dĭgəttə </ts>
               <ts e="T139" id="Seg_3807" n="e" s="T138">ara </ts>
               <ts e="T140" id="Seg_3809" n="e" s="T139">detləʔi, </ts>
               <ts e="T141" id="Seg_3811" n="e" s="T140">ipek. </ts>
            </ts>
            <ts e="T148" id="Seg_3812" n="sc" s="T142">
               <ts e="T143" id="Seg_3814" n="e" s="T142">Amnolaʔbəʔjə, </ts>
               <ts e="T144" id="Seg_3816" n="e" s="T143">bĭtleʔbəʔjə, </ts>
               <ts e="T145" id="Seg_3818" n="e" s="T144">dĭgəttə </ts>
               <ts e="T146" id="Seg_3820" n="e" s="T145">(šobi=) </ts>
               <ts e="T147" id="Seg_3822" n="e" s="T146">šolaʔbəʔjə </ts>
               <ts e="T148" id="Seg_3824" n="e" s="T147">inezi. </ts>
            </ts>
            <ts e="T155" id="Seg_3825" n="sc" s="T149">
               <ts e="T150" id="Seg_3827" n="e" s="T149">Dĭm </ts>
               <ts e="T151" id="Seg_3829" n="e" s="T150">(il-) </ts>
               <ts e="T152" id="Seg_3831" n="e" s="T151">iluʔpiʔi </ts>
               <ts e="T153" id="Seg_3833" n="e" s="T152">i </ts>
               <ts e="T154" id="Seg_3835" n="e" s="T153">kumbiʔi </ts>
               <ts e="T155" id="Seg_3837" n="e" s="T154">maːʔndə. </ts>
            </ts>
            <ts e="T177" id="Seg_3838" n="sc" s="T172">
               <ts e="T173" id="Seg_3840" n="e" s="T172">(I-) </ts>
               <ts e="T174" id="Seg_3842" n="e" s="T173">Iat </ts>
               <ts e="T175" id="Seg_3844" n="e" s="T174">i </ts>
               <ts e="T176" id="Seg_3846" n="e" s="T175">abat </ts>
               <ts e="T177" id="Seg_3848" n="e" s="T176">mămbiʔi. </ts>
            </ts>
            <ts e="T187" id="Seg_3849" n="sc" s="T178">
               <ts e="T179" id="Seg_3851" n="e" s="T178">Dĭgəttə </ts>
               <ts e="T180" id="Seg_3853" n="e" s="T179">abiʔi </ts>
               <ts e="T181" id="Seg_3855" n="e" s="T180">пиво, </ts>
               <ts e="T182" id="Seg_3857" n="e" s="T181">ara </ts>
               <ts e="T183" id="Seg_3859" n="e" s="T182">iʔgö </ts>
               <ts e="T184" id="Seg_3861" n="e" s="T183">ibi, </ts>
               <ts e="T185" id="Seg_3863" n="e" s="T184">bar </ts>
               <ts e="T186" id="Seg_3865" n="e" s="T185">ĭmbi </ts>
               <ts e="T187" id="Seg_3867" n="e" s="T186">abiʔi. </ts>
            </ts>
            <ts e="T192" id="Seg_3868" n="sc" s="T188">
               <ts e="T189" id="Seg_3870" n="e" s="T188">Nabəʔi </ts>
               <ts e="T190" id="Seg_3872" n="e" s="T189">pürbiʔi, </ts>
               <ts e="T191" id="Seg_3874" n="e" s="T190">uja </ts>
               <ts e="T192" id="Seg_3876" n="e" s="T191">iʔgö. </ts>
            </ts>
            <ts e="T201" id="Seg_3877" n="sc" s="T193">
               <ts e="T194" id="Seg_3879" n="e" s="T193">Dĭgəttə </ts>
               <ts e="T195" id="Seg_3881" n="e" s="T194">(nulbi-) </ts>
               <ts e="T196" id="Seg_3883" n="e" s="T195">nuldəbiʔi </ts>
               <ts e="T197" id="Seg_3885" n="e" s="T196">stolbə, </ts>
               <ts e="T198" id="Seg_3887" n="e" s="T197">bar </ts>
               <ts e="T199" id="Seg_3889" n="e" s="T198">ambiʔi </ts>
               <ts e="T200" id="Seg_3891" n="e" s="T199">i </ts>
               <ts e="T1531" id="Seg_3893" n="e" s="T200">kalla </ts>
               <ts e="T201" id="Seg_3895" n="e" s="T1531">dʼürbiʔi. </ts>
            </ts>
            <ts e="T209" id="Seg_3896" n="sc" s="T202">
               <ts e="T203" id="Seg_3898" n="e" s="T202">Koʔbsaŋ, </ts>
               <ts e="T204" id="Seg_3900" n="e" s="T203">(il) </ts>
               <ts e="T205" id="Seg_3902" n="e" s="T204">bar </ts>
               <ts e="T206" id="Seg_3904" n="e" s="T205">ara </ts>
               <ts e="T207" id="Seg_3906" n="e" s="T206">biʔpiʔi, </ts>
               <ts e="T208" id="Seg_3908" n="e" s="T207">sʼarbiʔi </ts>
               <ts e="T209" id="Seg_3910" n="e" s="T208">garmonʼnʼazi. </ts>
            </ts>
            <ts e="T211" id="Seg_3911" n="sc" s="T210">
               <ts e="T211" id="Seg_3913" n="e" s="T210">Suʔmibiʔi. </ts>
            </ts>
            <ts e="T215" id="Seg_3914" n="sc" s="T212">
               <ts e="T213" id="Seg_3916" n="e" s="T212">Dĭgəttə </ts>
               <ts e="T214" id="Seg_3918" n="e" s="T213">bar </ts>
               <ts e="T1532" id="Seg_3920" n="e" s="T214">kalla </ts>
               <ts e="T215" id="Seg_3922" n="e" s="T1532">dʼürbiʔi. </ts>
            </ts>
            <ts e="T223" id="Seg_3923" n="sc" s="T217">
               <ts e="T218" id="Seg_3925" n="e" s="T217">Ну, </ts>
               <ts e="T219" id="Seg_3927" n="e" s="T218">это </ts>
               <ts e="T220" id="Seg_3929" n="e" s="T219">я </ts>
               <ts e="T221" id="Seg_3931" n="e" s="T220">про </ts>
               <ts e="T222" id="Seg_3933" n="e" s="T221">свадьбу </ts>
               <ts e="T223" id="Seg_3935" n="e" s="T222">говорила. </ts>
            </ts>
            <ts e="T233" id="Seg_3936" n="sc" s="T229">
               <ts e="T230" id="Seg_3938" n="e" s="T229">Пиво </ts>
               <ts e="T231" id="Seg_3940" n="e" s="T230">делали </ts>
               <ts e="T232" id="Seg_3942" n="e" s="T231">сами </ts>
               <ts e="T233" id="Seg_3944" n="e" s="T232">они. </ts>
            </ts>
            <ts e="T251" id="Seg_3945" n="sc" s="T247">
               <ts e="T248" id="Seg_3947" n="e" s="T247">Amnobiʔi </ts>
               <ts e="T249" id="Seg_3949" n="e" s="T248">jakše, </ts>
               <ts e="T250" id="Seg_3951" n="e" s="T249">i </ts>
               <ts e="T251" id="Seg_3953" n="e" s="T250">dʼabrolaʔpiʔi. </ts>
            </ts>
            <ts e="T263" id="Seg_3954" n="sc" s="T252">
               <ts e="T253" id="Seg_3956" n="e" s="T252">Ara </ts>
               <ts e="T254" id="Seg_3958" n="e" s="T253">bĭtləʔi </ts>
               <ts e="T255" id="Seg_3960" n="e" s="T254">dak, </ts>
               <ts e="T256" id="Seg_3962" n="e" s="T255">dʼabrolaʔbəʔjə. </ts>
               <ts e="T257" id="Seg_3964" n="e" s="T256">(Eʔbdən=) </ts>
               <ts e="T258" id="Seg_3966" n="e" s="T257">Eʔbdəzaŋdə </ts>
               <ts e="T259" id="Seg_3968" n="e" s="T258">onʼiʔ </ts>
               <ts e="T260" id="Seg_3970" n="e" s="T259">(onʼiʔtə </ts>
               <ts e="T261" id="Seg_3972" n="e" s="T260">ibiʔi) </ts>
               <ts e="T262" id="Seg_3974" n="e" s="T261">i </ts>
               <ts e="T263" id="Seg_3976" n="e" s="T262">dʼabrolaʔpiʔi. </ts>
            </ts>
            <ts e="T279" id="Seg_3977" n="sc" s="T265">
               <ts e="T266" id="Seg_3979" n="e" s="T265">Вот </ts>
               <ts e="T267" id="Seg_3981" n="e" s="T266">я </ts>
               <ts e="T269" id="Seg_3983" n="e" s="T267">помню </ts>
               <ts e="T270" id="Seg_3985" n="e" s="T269">(дву-), </ts>
               <ts e="T271" id="Seg_3987" n="e" s="T270">я </ts>
               <ts e="T272" id="Seg_3989" n="e" s="T271">тебе </ts>
               <ts e="T273" id="Seg_3991" n="e" s="T272">может </ts>
               <ts e="T274" id="Seg_3993" n="e" s="T273">рассказывала, </ts>
               <ts e="T275" id="Seg_3995" n="e" s="T274">двух </ts>
               <ts e="T276" id="Seg_3997" n="e" s="T275">(ста-), </ts>
               <ts e="T277" id="Seg_3999" n="e" s="T276">старик </ts>
               <ts e="T278" id="Seg_4001" n="e" s="T277">со </ts>
               <ts e="T279" id="Seg_4003" n="e" s="T278">старухой. </ts>
            </ts>
            <ts e="T287" id="Seg_4004" n="sc" s="T280">
               <ts e="T281" id="Seg_4006" n="e" s="T280">Напились </ts>
               <ts e="T282" id="Seg_4008" n="e" s="T281">пьяны, </ts>
               <ts e="T283" id="Seg_4010" n="e" s="T282">так </ts>
               <ts e="T284" id="Seg_4012" n="e" s="T283">через </ts>
               <ts e="T285" id="Seg_4014" n="e" s="T284">пол </ts>
               <ts e="T286" id="Seg_4016" n="e" s="T285">был </ts>
               <ts e="T287" id="Seg_4018" n="e" s="T286">порог. </ts>
            </ts>
            <ts e="T300" id="Seg_4019" n="sc" s="T288">
               <ts e="T289" id="Seg_4021" n="e" s="T288">Они </ts>
               <ts e="T290" id="Seg_4023" n="e" s="T289">друг </ts>
               <ts e="T291" id="Seg_4025" n="e" s="T290">дружку </ts>
               <ts e="T292" id="Seg_4027" n="e" s="T291">взяли </ts>
               <ts e="T293" id="Seg_4029" n="e" s="T292">за </ts>
               <ts e="T294" id="Seg_4031" n="e" s="T293">волосы, </ts>
               <ts e="T295" id="Seg_4033" n="e" s="T294">тянулись, </ts>
               <ts e="T296" id="Seg_4035" n="e" s="T295">тянулись </ts>
               <ts e="T297" id="Seg_4037" n="e" s="T296">и </ts>
               <ts e="T298" id="Seg_4039" n="e" s="T297">так </ts>
               <ts e="T299" id="Seg_4041" n="e" s="T298">и </ts>
               <ts e="T300" id="Seg_4043" n="e" s="T299">уснули. </ts>
            </ts>
            <ts e="T327" id="Seg_4044" n="sc" s="T301">
               <ts e="T302" id="Seg_4046" n="e" s="T301">Утром </ts>
               <ts e="T303" id="Seg_4048" n="e" s="T302">встали, </ts>
               <ts e="T304" id="Seg_4050" n="e" s="T303">"Чего-то, </ts>
               <ts e="T305" id="Seg_4052" n="e" s="T304">говорит, </ts>
               <ts e="T306" id="Seg_4054" n="e" s="T305">у </ts>
               <ts e="T307" id="Seg_4056" n="e" s="T306">меня </ts>
               <ts e="T308" id="Seg_4058" n="e" s="T307">тут </ts>
               <ts e="T309" id="Seg_4060" n="e" s="T308">больно", </ts>
               <ts e="T310" id="Seg_4062" n="e" s="T309">и </ts>
               <ts e="T311" id="Seg_4064" n="e" s="T310">он </ts>
               <ts e="T312" id="Seg_4066" n="e" s="T311">говорит: </ts>
               <ts e="T313" id="Seg_4068" n="e" s="T312">"У </ts>
               <ts e="T314" id="Seg_4070" n="e" s="T313">меня </ts>
               <ts e="T315" id="Seg_4072" n="e" s="T314">больно". </ts>
               <ts e="T316" id="Seg_4074" n="e" s="T315">"Может, </ts>
               <ts e="T317" id="Seg_4076" n="e" s="T316">мы, </ts>
               <ts e="T318" id="Seg_4078" n="e" s="T317">говорит, </ts>
               <ts e="T319" id="Seg_4080" n="e" s="T318">дрались </ts>
               <ts e="T320" id="Seg_4082" n="e" s="T319">с </ts>
               <ts e="T321" id="Seg_4084" n="e" s="T320">тобой, </ts>
               <ts e="T322" id="Seg_4086" n="e" s="T321">не </ts>
               <ts e="T323" id="Seg_4088" n="e" s="T322">знаем, </ts>
               <ts e="T324" id="Seg_4090" n="e" s="T323">так </ts>
               <ts e="T325" id="Seg_4092" n="e" s="T324">напились </ts>
               <ts e="T326" id="Seg_4094" n="e" s="T325">пьяные", </ts>
               <ts e="T327" id="Seg_4096" n="e" s="T326">вот. </ts>
            </ts>
            <ts e="T343" id="Seg_4097" n="sc" s="T328">
               <ts e="T329" id="Seg_4099" n="e" s="T328">Я </ts>
               <ts e="T330" id="Seg_4101" n="e" s="T329">их </ts>
               <ts e="T331" id="Seg_4103" n="e" s="T330">помню, </ts>
               <ts e="T332" id="Seg_4105" n="e" s="T331">фамилие </ts>
               <ts e="T333" id="Seg_4107" n="e" s="T332">у </ts>
               <ts e="T334" id="Seg_4109" n="e" s="T333">его </ts>
               <ts e="T335" id="Seg_4111" n="e" s="T334">было </ts>
               <ts e="T336" id="Seg_4113" n="e" s="T335">Тугуин, </ts>
               <ts e="T337" id="Seg_4115" n="e" s="T336">а </ts>
               <ts e="T338" id="Seg_4117" n="e" s="T337">ее </ts>
               <ts e="T339" id="Seg_4119" n="e" s="T338">звали </ts>
               <ts e="T340" id="Seg_4121" n="e" s="T339">Вера, </ts>
               <ts e="T341" id="Seg_4123" n="e" s="T340">и </ts>
               <ts e="T342" id="Seg_4125" n="e" s="T341">он </ts>
               <ts e="T343" id="Seg_4127" n="e" s="T342">Василий. </ts>
            </ts>
            <ts e="T346" id="Seg_4128" n="sc" s="T344">
               <ts e="T345" id="Seg_4130" n="e" s="T344">Звали </ts>
               <ts e="T346" id="Seg_4132" n="e" s="T345">его. </ts>
            </ts>
            <ts e="T360" id="Seg_4133" n="sc" s="T350">
               <ts e="T351" id="Seg_4135" n="e" s="T350">Камасинцы, </ts>
               <ts e="T353" id="Seg_4137" n="e" s="T351">я </ts>
               <ts e="T354" id="Seg_4139" n="e" s="T353">их </ts>
               <ts e="T355" id="Seg_4141" n="e" s="T354">хорошо </ts>
               <ts e="T356" id="Seg_4143" n="e" s="T355">помню, </ts>
               <ts e="T357" id="Seg_4145" n="e" s="T356">недалёко </ts>
               <ts e="T358" id="Seg_4147" n="e" s="T357">от </ts>
               <ts e="T359" id="Seg_4149" n="e" s="T358">нас </ts>
               <ts e="T360" id="Seg_4151" n="e" s="T359">жили. </ts>
            </ts>
            <ts e="T378" id="Seg_4152" n="sc" s="T371">
               <ts e="T372" id="Seg_4154" n="e" s="T371">Šində </ts>
               <ts e="T373" id="Seg_4156" n="e" s="T372">sumna </ts>
               <ts e="T374" id="Seg_4158" n="e" s="T373">ibi, </ts>
               <ts e="T375" id="Seg_4160" n="e" s="T374">šində </ts>
               <ts e="T376" id="Seg_4162" n="e" s="T375">sejʔpü </ts>
               <ts e="T377" id="Seg_4164" n="e" s="T376">ibi </ts>
               <ts e="T378" id="Seg_4166" n="e" s="T377">esseŋ. </ts>
            </ts>
            <ts e="T387" id="Seg_4167" n="sc" s="T379">
               <ts e="T381" id="Seg_4169" n="e" s="T379">Iʔgö </ts>
               <ts e="T383" id="Seg_4171" n="e" s="T381">deʔpiʔi. </ts>
               <ts e="T384" id="Seg_4173" n="e" s="T383">Dĭzeŋ </ts>
               <ts e="T385" id="Seg_4175" n="e" s="T384">ĭmbidə </ts>
               <ts e="T386" id="Seg_4177" n="e" s="T385">ej </ts>
               <ts e="T387" id="Seg_4179" n="e" s="T386">abiʔi. </ts>
            </ts>
            <ts e="T400" id="Seg_4180" n="sc" s="T388">
               <ts e="T389" id="Seg_4182" n="e" s="T388">Tüj </ts>
               <ts e="T390" id="Seg_4184" n="e" s="T389">il </ts>
               <ts e="T391" id="Seg_4186" n="e" s="T390">esseŋ </ts>
               <ts e="T392" id="Seg_4188" n="e" s="T391">(barəʔbəʔjə). </ts>
               <ts e="T393" id="Seg_4190" n="e" s="T392">Bălʼnitsanə </ts>
               <ts e="T394" id="Seg_4192" n="e" s="T393">mĭmbiʔi </ts>
               <ts e="T395" id="Seg_4194" n="e" s="T394">i </ts>
               <ts e="T396" id="Seg_4196" n="e" s="T395">ĭmbidə </ts>
               <ts e="T397" id="Seg_4198" n="e" s="T396">abiʔi, </ts>
               <ts e="T398" id="Seg_4200" n="e" s="T397">ej </ts>
               <ts e="T399" id="Seg_4202" n="e" s="T398">deʔlieʔi </ts>
               <ts e="T400" id="Seg_4204" n="e" s="T399">esseŋ. </ts>
            </ts>
            <ts e="T410" id="Seg_4205" n="sc" s="T403">
               <ts e="T404" id="Seg_4207" n="e" s="T403">Nu, </ts>
               <ts e="T405" id="Seg_4209" n="e" s="T404">esseŋdə </ts>
               <ts e="T406" id="Seg_4211" n="e" s="T405">dĭzeŋ </ts>
               <ts e="T407" id="Seg_4213" n="e" s="T406">ĭzembiʔi, </ts>
               <ts e="T408" id="Seg_4215" n="e" s="T407">külambiʔi, </ts>
               <ts e="T409" id="Seg_4217" n="e" s="T408">amga </ts>
               <ts e="T410" id="Seg_4219" n="e" s="T409">maːluʔpiʔi. </ts>
            </ts>
            <ts e="T438" id="Seg_4220" n="sc" s="T433">
               <ts e="T437" id="Seg_4222" n="e" s="T433">Dʼorbiʔi </ts>
               <ts e="T438" id="Seg_4224" n="e" s="T437">esseŋ. </ts>
            </ts>
            <ts e="T444" id="Seg_4225" n="sc" s="T440">
               <ts e="T441" id="Seg_4227" n="e" s="T440">I </ts>
               <ts e="T443" id="Seg_4229" n="e" s="T441">(izeb-) </ts>
               <ts e="T444" id="Seg_4231" n="e" s="T443">ĭzembiʔi. </ts>
            </ts>
            <ts e="T462" id="Seg_4232" n="sc" s="T458">
               <ts e="T459" id="Seg_4234" n="e" s="T458">Iat </ts>
               <ts e="T460" id="Seg_4236" n="e" s="T459">uʔlia </ts>
               <ts e="T461" id="Seg_4238" n="e" s="T460">da </ts>
               <ts e="T462" id="Seg_4240" n="e" s="T461">iləj. </ts>
            </ts>
            <ts e="T468" id="Seg_4241" n="sc" s="T463">
               <ts e="T464" id="Seg_4243" n="e" s="T463">Dĭgəttə </ts>
               <ts e="T465" id="Seg_4245" n="e" s="T464">mĭlie </ts>
               <ts e="T466" id="Seg_4247" n="e" s="T465">nüjö </ts>
               <ts e="T467" id="Seg_4249" n="e" s="T466">(imiʔ) </ts>
               <ts e="T468" id="Seg_4251" n="e" s="T467">dĭzeŋdə. </ts>
            </ts>
            <ts e="T472" id="Seg_4252" n="sc" s="T469">
               <ts e="T470" id="Seg_4254" n="e" s="T469">(Amnəj) </ts>
               <ts e="T471" id="Seg_4256" n="e" s="T470">da </ts>
               <ts e="T472" id="Seg_4258" n="e" s="T471">embi. </ts>
            </ts>
            <ts e="T475" id="Seg_4259" n="sc" s="T473">
               <ts e="T474" id="Seg_4261" n="e" s="T473">Dĭ </ts>
               <ts e="T475" id="Seg_4263" n="e" s="T474">kunolluʔpi. </ts>
            </ts>
            <ts e="T480" id="Seg_4264" n="sc" s="T476">
               <ts e="T477" id="Seg_4266" n="e" s="T476">I </ts>
               <ts e="T478" id="Seg_4268" n="e" s="T477">bostə </ts>
               <ts e="T479" id="Seg_4270" n="e" s="T478">iʔbəbi </ts>
               <ts e="T480" id="Seg_4272" n="e" s="T479">kunolluʔpi. </ts>
            </ts>
            <ts e="T508" id="Seg_4273" n="sc" s="T502">
               <ts e="T503" id="Seg_4275" n="e" s="T502">Nʼilgölaʔpiʔi </ts>
               <ts e="T504" id="Seg_4277" n="e" s="T503">(abam </ts>
               <ts e="T505" id="Seg_4279" n="e" s="T504">= </ts>
               <ts e="T506" id="Seg_4281" n="e" s="T505">abat=) </ts>
               <ts e="T507" id="Seg_4283" n="e" s="T506">abat </ts>
               <ts e="T508" id="Seg_4285" n="e" s="T507">iat. </ts>
            </ts>
            <ts e="T515" id="Seg_4286" n="sc" s="T509">
               <ts e="T510" id="Seg_4288" n="e" s="T509">Măn </ts>
               <ts e="T511" id="Seg_4290" n="e" s="T510">üdʼüge </ts>
               <ts e="T512" id="Seg_4292" n="e" s="T511">ibiem, </ts>
               <ts e="T513" id="Seg_4294" n="e" s="T512">iam </ts>
               <ts e="T514" id="Seg_4296" n="e" s="T513">măndə: </ts>
               <ts e="T515" id="Seg_4298" n="e" s="T514">"Kanaʔ". </ts>
            </ts>
            <ts e="T518" id="Seg_4299" n="sc" s="T516">
               <ts e="T517" id="Seg_4301" n="e" s="T516">Öʔləj </ts>
               <ts e="T518" id="Seg_4303" n="e" s="T517">gibər-nʼibudʼ. </ts>
            </ts>
            <ts e="T524" id="Seg_4304" n="sc" s="T519">
               <ts e="T520" id="Seg_4306" n="e" s="T519">Măn </ts>
               <ts e="T521" id="Seg_4308" n="e" s="T520">nuliam </ts>
               <ts e="T522" id="Seg_4310" n="e" s="T521">da </ts>
               <ts e="T523" id="Seg_4312" n="e" s="T522">mʼegəlaʔbəm </ts>
               <ts e="T524" id="Seg_4314" n="e" s="T523">dĭn. </ts>
            </ts>
            <ts e="T530" id="Seg_4315" n="sc" s="T525">
               <ts e="T526" id="Seg_4317" n="e" s="T525">A </ts>
               <ts e="T527" id="Seg_4319" n="e" s="T526">dĭ: </ts>
               <ts e="T528" id="Seg_4321" n="e" s="T527">"Măn </ts>
               <ts e="T529" id="Seg_4323" n="e" s="T528">tănan </ts>
               <ts e="T530" id="Seg_4325" n="e" s="T529">münörləm". </ts>
            </ts>
            <ts e="T537" id="Seg_4326" n="sc" s="T531">
               <ts e="T532" id="Seg_4328" n="e" s="T531">Măn </ts>
               <ts e="T533" id="Seg_4330" n="e" s="T532">dĭgəttə </ts>
               <ts e="T534" id="Seg_4332" n="e" s="T533">nuʔmiluʔpiam </ts>
               <ts e="T535" id="Seg_4334" n="e" s="T534">i </ts>
               <ts e="T536" id="Seg_4336" n="e" s="T535">büžü </ts>
               <ts e="T537" id="Seg_4338" n="e" s="T536">šobiam. </ts>
            </ts>
            <ts e="T544" id="Seg_4339" n="sc" s="T538">
               <ts e="T539" id="Seg_4341" n="e" s="T538">Dĭgəttə </ts>
               <ts e="T540" id="Seg_4343" n="e" s="T539">dĭ </ts>
               <ts e="T541" id="Seg_4345" n="e" s="T540">ĭmbidə </ts>
               <ts e="T542" id="Seg_4347" n="e" s="T541">măna </ts>
               <ts e="T543" id="Seg_4349" n="e" s="T542">ej </ts>
               <ts e="T544" id="Seg_4351" n="e" s="T543">münörleʔpi. </ts>
            </ts>
            <ts e="T552" id="Seg_4352" n="sc" s="T546">
               <ts e="T547" id="Seg_4354" n="e" s="T546">Любила </ts>
               <ts e="T548" id="Seg_4356" n="e" s="T547">((…)) </ts>
               <ts e="T549" id="Seg_4358" n="e" s="T548">тоже </ts>
               <ts e="T550" id="Seg_4360" n="e" s="T549">над </ts>
               <ts e="T552" id="Seg_4362" n="e" s="T550">матерью. </ts>
            </ts>
            <ts e="T562" id="Seg_4363" n="sc" s="T561">
               <ts e="T562" id="Seg_4365" n="e" s="T561">Münörbiʔi. </ts>
            </ts>
            <ts e="T565" id="Seg_4366" n="sc" s="T563">
               <ts e="T564" id="Seg_4368" n="e" s="T563">Tăŋ, </ts>
               <ts e="T565" id="Seg_4370" n="e" s="T564">šerəpsi. </ts>
            </ts>
            <ts e="T571" id="Seg_4371" n="sc" s="T566">
               <ts e="T567" id="Seg_4373" n="e" s="T566">Măna </ts>
               <ts e="T568" id="Seg_4375" n="e" s="T567">iam </ts>
               <ts e="T569" id="Seg_4377" n="e" s="T568">ugandə </ts>
               <ts e="T570" id="Seg_4379" n="e" s="T569">tăŋ </ts>
               <ts e="T571" id="Seg_4381" n="e" s="T570">münörbiʔi. </ts>
            </ts>
            <ts e="T577" id="Seg_4382" n="sc" s="T572">
               <ts e="T575" id="Seg_4384" n="e" s="T572">Măn </ts>
               <ts e="T576" id="Seg_4386" n="e" s="T575">tăŋ </ts>
               <ts e="T577" id="Seg_4388" n="e" s="T576">alombiam. </ts>
            </ts>
            <ts e="T591" id="Seg_4389" n="sc" s="T589">
               <ts e="T591" id="Seg_4391" n="e" s="T589">Прутом. </ts>
            </ts>
            <ts e="T593" id="Seg_4392" n="sc" s="T592">
               <ts e="T593" id="Seg_4394" n="e" s="T592">((…)). </ts>
            </ts>
            <ts e="T605" id="Seg_4395" n="sc" s="T602">
               <ts e="T603" id="Seg_4397" n="e" s="T602">Нет, </ts>
               <ts e="T604" id="Seg_4399" n="e" s="T603">не </ts>
               <ts e="T605" id="Seg_4401" n="e" s="T604">умели. </ts>
            </ts>
            <ts e="T616" id="Seg_4402" n="sc" s="T608">
               <ts e="T609" id="Seg_4404" n="e" s="T608">Я </ts>
               <ts e="T610" id="Seg_4406" n="e" s="T609">не </ts>
               <ts e="T611" id="Seg_4408" n="e" s="T610">знаю, </ts>
               <ts e="T612" id="Seg_4410" n="e" s="T611">почему </ts>
               <ts e="T613" id="Seg_4412" n="e" s="T612">у </ts>
               <ts e="T614" id="Seg_4414" n="e" s="T613">их </ts>
               <ts e="T615" id="Seg_4416" n="e" s="T614">не </ts>
               <ts e="T616" id="Seg_4418" n="e" s="T615">было. </ts>
            </ts>
            <ts e="T633" id="Seg_4419" n="sc" s="T618">
               <ts e="T619" id="Seg_4421" n="e" s="T618">Вот </ts>
               <ts e="T621" id="Seg_4423" n="e" s="T619">это </ts>
               <ts e="T622" id="Seg_4425" n="e" s="T621">у </ts>
               <ts e="T623" id="Seg_4427" n="e" s="T622">одной </ts>
               <ts e="T624" id="Seg_4429" n="e" s="T623">сестре </ts>
               <ts e="T625" id="Seg_4431" n="e" s="T624">стояла, </ts>
               <ts e="T626" id="Seg_4433" n="e" s="T625">так </ts>
               <ts e="T627" id="Seg_4435" n="e" s="T626">она </ts>
               <ts e="T628" id="Seg_4437" n="e" s="T627">говорит: </ts>
               <ts e="T629" id="Seg_4439" n="e" s="T628">это </ts>
               <ts e="T630" id="Seg_4441" n="e" s="T629">они </ts>
               <ts e="T631" id="Seg_4443" n="e" s="T630">выдумали </ts>
               <ts e="T632" id="Seg_4445" n="e" s="T631">свой </ts>
               <ts e="T633" id="Seg_4447" n="e" s="T632">язык. </ts>
            </ts>
            <ts e="T640" id="Seg_4448" n="sc" s="T634">
               <ts e="T635" id="Seg_4450" n="e" s="T634">Нету, </ts>
               <ts e="T636" id="Seg_4452" n="e" s="T635">говорит, </ts>
               <ts e="T637" id="Seg_4454" n="e" s="T636">у </ts>
               <ts e="T638" id="Seg_4456" n="e" s="T637">их </ts>
               <ts e="T639" id="Seg_4458" n="e" s="T638">никаких </ts>
               <ts e="T640" id="Seg_4460" n="e" s="T639">книг. </ts>
            </ts>
            <ts e="T643" id="Seg_4461" n="sc" s="T641">
               <ts e="T642" id="Seg_4463" n="e" s="T641">Не </ts>
               <ts e="T643" id="Seg_4465" n="e" s="T642">знаю. </ts>
            </ts>
            <ts e="T655" id="Seg_4466" n="sc" s="T647">
               <ts e="T649" id="Seg_4468" n="e" s="T647">Ну, </ts>
               <ts e="T652" id="Seg_4470" n="e" s="T649">я </ts>
               <ts e="T655" id="Seg_4472" n="e" s="T652">говорила… </ts>
            </ts>
            <ts e="T674" id="Seg_4473" n="sc" s="T661">
               <ts e="T664" id="Seg_4475" n="e" s="T661">Ну, </ts>
               <ts e="T665" id="Seg_4477" n="e" s="T664">что </ts>
               <ts e="T666" id="Seg_4479" n="e" s="T665">(нету=) </ts>
               <ts e="T667" id="Seg_4481" n="e" s="T666">не </ts>
               <ts e="T668" id="Seg_4483" n="e" s="T667">умеют </ts>
               <ts e="T669" id="Seg_4485" n="e" s="T668">ни </ts>
               <ts e="T671" id="Seg_4487" n="e" s="T669">писать, </ts>
               <ts e="T672" id="Seg_4489" n="e" s="T671">ни </ts>
               <ts e="T674" id="Seg_4491" n="e" s="T672">читать. </ts>
            </ts>
            <ts e="T683" id="Seg_4492" n="sc" s="T676">
               <ts e="T677" id="Seg_4494" n="e" s="T676">Только </ts>
               <ts e="T678" id="Seg_4496" n="e" s="T677">один </ts>
               <ts e="T679" id="Seg_4498" n="e" s="T678">у </ts>
               <ts e="T680" id="Seg_4500" n="e" s="T679">нас </ts>
               <ts e="T681" id="Seg_4502" n="e" s="T680">там, </ts>
               <ts e="T682" id="Seg_4504" n="e" s="T681">Александр </ts>
               <ts e="T683" id="Seg_4506" n="e" s="T682">Шайбин. </ts>
            </ts>
            <ts e="T688" id="Seg_4507" n="sc" s="T684">
               <ts e="T685" id="Seg_4509" n="e" s="T684">Вот </ts>
               <ts e="T686" id="Seg_4511" n="e" s="T685">он </ts>
               <ts e="T687" id="Seg_4513" n="e" s="T686">там </ts>
               <ts e="T688" id="Seg_4515" n="e" s="T687">ходил. </ts>
            </ts>
            <ts e="T701" id="Seg_4516" n="sc" s="T689">
               <ts e="T690" id="Seg_4518" n="e" s="T689">(Там=) </ts>
               <ts e="T691" id="Seg_4520" n="e" s="T690">Там, </ts>
               <ts e="T692" id="Seg_4522" n="e" s="T691">еслиф </ts>
               <ts e="T693" id="Seg_4524" n="e" s="T692">кому </ts>
               <ts e="T694" id="Seg_4526" n="e" s="T693">надо </ts>
               <ts e="T695" id="Seg_4528" n="e" s="T694">учить, </ts>
               <ts e="T696" id="Seg_4530" n="e" s="T695">пускай </ts>
               <ts e="T697" id="Seg_4532" n="e" s="T696">наймет </ts>
               <ts e="T698" id="Seg_4534" n="e" s="T697">себе, </ts>
               <ts e="T699" id="Seg_4536" n="e" s="T698">по-русски </ts>
               <ts e="T700" id="Seg_4538" n="e" s="T699">учил </ts>
               <ts e="T701" id="Seg_4540" n="e" s="T700">учитель. </ts>
            </ts>
            <ts e="T703" id="Seg_4541" n="sc" s="T702">
               <ts e="T703" id="Seg_4543" n="e" s="T702">Так. </ts>
            </ts>
            <ts e="T727" id="Seg_4544" n="sc" s="T722">
               <ts e="T724" id="Seg_4546" n="e" s="T722">Pʼaŋdəsʼtə </ts>
               <ts e="T725" id="Seg_4548" n="e" s="T724">ej </ts>
               <ts e="T726" id="Seg_4550" n="e" s="T725">moliam, </ts>
               <ts e="T727" id="Seg_4552" n="e" s="T726">a… </ts>
            </ts>
            <ts e="T736" id="Seg_4553" n="sc" s="T728">
               <ts e="T729" id="Seg_4555" n="e" s="T728">A </ts>
               <ts e="T730" id="Seg_4557" n="e" s="T729">moliam, </ts>
               <ts e="T731" id="Seg_4559" n="e" s="T730">knižkam </ts>
               <ts e="T732" id="Seg_4561" n="e" s="T731">măndliam, </ts>
               <ts e="T733" id="Seg_4563" n="e" s="T732">ĭmbi </ts>
               <ts e="T734" id="Seg_4565" n="e" s="T733">dĭn </ts>
               <ts e="T735" id="Seg_4567" n="e" s="T734">ige </ts>
               <ts e="T736" id="Seg_4569" n="e" s="T735">pʼaŋdona. </ts>
            </ts>
            <ts e="T741" id="Seg_4570" n="sc" s="T737">
               <ts e="T738" id="Seg_4572" n="e" s="T737">A </ts>
               <ts e="T739" id="Seg_4574" n="e" s="T738">pʼaŋdəsʼtə </ts>
               <ts e="T740" id="Seg_4576" n="e" s="T739">ej </ts>
               <ts e="T741" id="Seg_4578" n="e" s="T740">moliam. </ts>
            </ts>
            <ts e="T762" id="Seg_4579" n="sc" s="T758">
               <ts e="T759" id="Seg_4581" n="e" s="T758">Они </ts>
               <ts e="T760" id="Seg_4583" n="e" s="T759">много </ts>
               <ts e="T761" id="Seg_4585" n="e" s="T760">не </ts>
               <ts e="T762" id="Seg_4587" n="e" s="T761">жили. </ts>
            </ts>
            <ts e="T766" id="Seg_4588" n="sc" s="T763">
               <ts e="T764" id="Seg_4590" n="e" s="T763">Надо </ts>
               <ts e="T765" id="Seg_4592" n="e" s="T764">говорить </ts>
               <ts e="T766" id="Seg_4594" n="e" s="T765">по-своему. </ts>
            </ts>
            <ts e="T775" id="Seg_4595" n="sc" s="T770">
               <ts e="T771" id="Seg_4597" n="e" s="T770">Dĭzeŋ </ts>
               <ts e="T772" id="Seg_4599" n="e" s="T771">iʔgö </ts>
               <ts e="T773" id="Seg_4601" n="e" s="T772">ej </ts>
               <ts e="T774" id="Seg_4603" n="e" s="T773">(amnobi=) </ts>
               <ts e="T775" id="Seg_4605" n="e" s="T774">amnolaʔpiʔi. </ts>
            </ts>
            <ts e="T782" id="Seg_4606" n="sc" s="T776">
               <ts e="T777" id="Seg_4608" n="e" s="T776">Dĭzeŋ </ts>
               <ts e="T778" id="Seg_4610" n="e" s="T777">üge </ts>
               <ts e="T779" id="Seg_4612" n="e" s="T778">nʼiʔneŋ, </ts>
               <ts e="T780" id="Seg_4614" n="e" s="T779">nʼineŋ. </ts>
               <ts e="T781" id="Seg_4616" n="e" s="T780">Turazaŋdə </ts>
               <ts e="T782" id="Seg_4618" n="e" s="T781">nagobiʔi. </ts>
            </ts>
            <ts e="T784" id="Seg_4619" n="sc" s="T783">
               <ts e="T784" id="Seg_4621" n="e" s="T783">Kănnambiʔi. </ts>
            </ts>
            <ts e="T790" id="Seg_4622" n="sc" s="T785">
               <ts e="T786" id="Seg_4624" n="e" s="T785">I </ts>
               <ts e="T787" id="Seg_4626" n="e" s="T786">dĭgəttə </ts>
               <ts e="T788" id="Seg_4628" n="e" s="T787">dĭzeŋdə </ts>
               <ts e="T789" id="Seg_4630" n="e" s="T788">(dʼazirzittə) </ts>
               <ts e="T790" id="Seg_4632" n="e" s="T789">nagobi. </ts>
            </ts>
            <ts e="T793" id="Seg_4633" n="sc" s="T791">
               <ts e="T792" id="Seg_4635" n="e" s="T791">Dĭzeŋ </ts>
               <ts e="T793" id="Seg_4637" n="e" s="T792">külambiʔi. </ts>
            </ts>
            <ts e="T797" id="Seg_4638" n="sc" s="T794">
               <ts e="T795" id="Seg_4640" n="e" s="T794">(A-) </ts>
               <ts e="T796" id="Seg_4642" n="e" s="T795">Amga </ts>
               <ts e="T797" id="Seg_4644" n="e" s="T796">amnolaʔbəʔi. </ts>
            </ts>
            <ts e="T804" id="Seg_4645" n="sc" s="T800">
               <ts e="T801" id="Seg_4647" n="e" s="T800">Miʔnʼibeʔ </ts>
               <ts e="T802" id="Seg_4649" n="e" s="T801">sĭre </ts>
               <ts e="T803" id="Seg_4651" n="e" s="T802">pe </ts>
               <ts e="T804" id="Seg_4653" n="e" s="T803">nagobi. </ts>
            </ts>
            <ts e="T809" id="Seg_4654" n="sc" s="T805">
               <ts e="T806" id="Seg_4656" n="e" s="T805">Dĭzeŋ </ts>
               <ts e="T807" id="Seg_4658" n="e" s="T806">nuldəbiʔi </ts>
               <ts e="T808" id="Seg_4660" n="e" s="T807">teʔtə </ts>
               <ts e="T809" id="Seg_4662" n="e" s="T808">pa. </ts>
            </ts>
            <ts e="T813" id="Seg_4663" n="sc" s="T810">
               <ts e="T811" id="Seg_4665" n="e" s="T810">I </ts>
               <ts e="T812" id="Seg_4667" n="e" s="T811">dĭn </ts>
               <ts e="T813" id="Seg_4669" n="e" s="T812">embiʔi. </ts>
            </ts>
            <ts e="T829" id="Seg_4670" n="sc" s="T814">
               <ts e="T815" id="Seg_4672" n="e" s="T814">Dĭgəttə </ts>
               <ts e="T816" id="Seg_4674" n="e" s="T815">(dĭ=) </ts>
               <ts e="T817" id="Seg_4676" n="e" s="T816">dĭbər </ts>
               <ts e="T818" id="Seg_4678" n="e" s="T817">nʼuʔtə </ts>
               <ts e="T819" id="Seg_4680" n="e" s="T818">piʔi </ts>
               <ts e="T820" id="Seg_4682" n="e" s="T819">embiʔi. </ts>
               <ts e="T821" id="Seg_4684" n="e" s="T820">Dĭgəttə </ts>
               <ts e="T822" id="Seg_4686" n="e" s="T821">dĭ </ts>
               <ts e="T823" id="Seg_4688" n="e" s="T822">paʔi </ts>
               <ts e="T824" id="Seg_4690" n="e" s="T823">(jaʔ-) </ts>
               <ts e="T825" id="Seg_4692" n="e" s="T824">jaʔpiʔi, </ts>
               <ts e="T826" id="Seg_4694" n="e" s="T825">dĭ </ts>
               <ts e="T827" id="Seg_4696" n="e" s="T826">saməʔluʔpi, </ts>
               <ts e="T828" id="Seg_4698" n="e" s="T827">dĭzem </ts>
               <ts e="T829" id="Seg_4700" n="e" s="T828">pʼaŋdluʔpi. </ts>
            </ts>
            <ts e="T835" id="Seg_4701" n="sc" s="T830">
               <ts e="T831" id="Seg_4703" n="e" s="T830">(Bostə </ts>
               <ts e="T832" id="Seg_4705" n="e" s="T831">n-) </ts>
               <ts e="T833" id="Seg_4707" n="e" s="T832">Bostə </ts>
               <ts e="T834" id="Seg_4709" n="e" s="T833">(nuldəzi) </ts>
               <ts e="T835" id="Seg_4711" n="e" s="T834">kutlaʔpiʔi. </ts>
            </ts>
            <ts e="T839" id="Seg_4712" n="sc" s="T836">
               <ts e="T837" id="Seg_4714" n="e" s="T836">Самих </ts>
               <ts e="T838" id="Seg_4716" n="e" s="T837">себя </ts>
               <ts e="T839" id="Seg_4718" n="e" s="T838">убивали. </ts>
            </ts>
            <ts e="T856" id="Seg_4719" n="sc" s="T840">
               <ts e="T841" id="Seg_4721" n="e" s="T840">Когда </ts>
               <ts e="T842" id="Seg_4723" n="e" s="T841">березы </ts>
               <ts e="T843" id="Seg_4725" n="e" s="T842">там </ts>
               <ts e="T844" id="Seg_4727" n="e" s="T843">не </ts>
               <ts e="T845" id="Seg_4729" n="e" s="T844">было </ts>
               <ts e="T846" id="Seg_4731" n="e" s="T845">(березы=) </ts>
               <ts e="T847" id="Seg_4733" n="e" s="T846">береза </ts>
               <ts e="T848" id="Seg_4735" n="e" s="T847">выросла, </ts>
               <ts e="T849" id="Seg_4737" n="e" s="T848">они </ts>
               <ts e="T850" id="Seg_4739" n="e" s="T849">сказали: </ts>
               <ts e="T851" id="Seg_4741" n="e" s="T850">"Русские </ts>
               <ts e="T852" id="Seg_4743" n="e" s="T851">люди </ts>
               <ts e="T853" id="Seg_4745" n="e" s="T852">придут, </ts>
               <ts e="T854" id="Seg_4747" n="e" s="T853">нам </ts>
               <ts e="T855" id="Seg_4749" n="e" s="T854">плохо </ts>
               <ts e="T856" id="Seg_4751" n="e" s="T855">будет". </ts>
            </ts>
            <ts e="T868" id="Seg_4752" n="sc" s="T857">
               <ts e="T858" id="Seg_4754" n="e" s="T857">Делали </ts>
               <ts e="T859" id="Seg_4756" n="e" s="T858">(алачин) </ts>
               <ts e="T860" id="Seg_4758" n="e" s="T859">такие, </ts>
               <ts e="T861" id="Seg_4760" n="e" s="T860">туда </ts>
               <ts e="T862" id="Seg_4762" n="e" s="T861">камни </ts>
               <ts e="T863" id="Seg_4764" n="e" s="T862">накладали, </ts>
               <ts e="T864" id="Seg_4766" n="e" s="T863">и </ts>
               <ts e="T865" id="Seg_4768" n="e" s="T864">подрубали, </ts>
               <ts e="T866" id="Seg_4770" n="e" s="T865">и </ts>
               <ts e="T867" id="Seg_4772" n="e" s="T866">их </ts>
               <ts e="T868" id="Seg_4774" n="e" s="T867">задавляло. </ts>
            </ts>
            <ts e="T877" id="Seg_4775" n="sc" s="T870">
               <ts e="T871" id="Seg_4777" n="e" s="T870">Вот </ts>
               <ts e="T872" id="Seg_4779" n="e" s="T871">они </ts>
               <ts e="T873" id="Seg_4781" n="e" s="T872">погребли </ts>
               <ts e="T874" id="Seg_4783" n="e" s="T873">сами </ts>
               <ts e="T875" id="Seg_4785" n="e" s="T874">себя, </ts>
               <ts e="T876" id="Seg_4787" n="e" s="T875">людей </ts>
               <ts e="T877" id="Seg_4789" n="e" s="T876">боялись. </ts>
            </ts>
            <ts e="T884" id="Seg_4790" n="sc" s="T878">
               <ts e="T879" id="Seg_4792" n="e" s="T878">Это </ts>
               <ts e="T880" id="Seg_4794" n="e" s="T879">(рассказывали=) </ts>
               <ts e="T881" id="Seg_4796" n="e" s="T880">рассказывали </ts>
               <ts e="T882" id="Seg_4798" n="e" s="T881">тоже </ts>
               <ts e="T883" id="Seg_4800" n="e" s="T882">старые </ts>
               <ts e="T884" id="Seg_4802" n="e" s="T883">люди. </ts>
            </ts>
            <ts e="T901" id="Seg_4803" n="sc" s="T892">
               <ts e="T893" id="Seg_4805" n="e" s="T892">Nu, </ts>
               <ts e="T894" id="Seg_4807" n="e" s="T893">dʼü </ts>
               <ts e="T895" id="Seg_4809" n="e" s="T894">(dĭl-) </ts>
               <ts e="T896" id="Seg_4811" n="e" s="T895">tĭlbiʔi, </ts>
               <ts e="T897" id="Seg_4813" n="e" s="T896">dĭgəttə </ts>
               <ts e="T898" id="Seg_4815" n="e" s="T897">pa </ts>
               <ts e="T899" id="Seg_4817" n="e" s="T898">(ambiʔi), </ts>
               <ts e="T900" id="Seg_4819" n="e" s="T899">pa </ts>
               <ts e="T901" id="Seg_4821" n="e" s="T900">jaʔpiʔi. </ts>
            </ts>
            <ts e="T911" id="Seg_4822" n="sc" s="T902">
               <ts e="T903" id="Seg_4824" n="e" s="T902">Dĭgəttə </ts>
               <ts e="T904" id="Seg_4826" n="e" s="T903">šide </ts>
               <ts e="T905" id="Seg_4828" n="e" s="T904">abiʔi, </ts>
               <ts e="T906" id="Seg_4830" n="e" s="T905">i </ts>
               <ts e="T907" id="Seg_4832" n="e" s="T906">dĭbər </ts>
               <ts e="T908" id="Seg_4834" n="e" s="T907">dĭn </ts>
               <ts e="T909" id="Seg_4836" n="e" s="T908">tože </ts>
               <ts e="T910" id="Seg_4838" n="e" s="T909">baltuzi </ts>
               <ts e="T911" id="Seg_4840" n="e" s="T910">jaʔpiʔi. </ts>
            </ts>
            <ts e="T917" id="Seg_4841" n="sc" s="T912">
               <ts e="T913" id="Seg_4843" n="e" s="T912">Dĭgəttə </ts>
               <ts e="T914" id="Seg_4845" n="e" s="T913">dĭm </ts>
               <ts e="T915" id="Seg_4847" n="e" s="T914">dĭbər </ts>
               <ts e="T916" id="Seg_4849" n="e" s="T915">embiʔi, </ts>
               <ts e="T917" id="Seg_4851" n="e" s="T916">kumbiʔi. </ts>
            </ts>
            <ts e="T920" id="Seg_4852" n="sc" s="T918">
               <ts e="T919" id="Seg_4854" n="e" s="T918">Embiʔi </ts>
               <ts e="T920" id="Seg_4856" n="e" s="T919">dʼünə. </ts>
            </ts>
            <ts e="T924" id="Seg_4857" n="sc" s="T921">
               <ts e="T922" id="Seg_4859" n="e" s="T921">I </ts>
               <ts e="T923" id="Seg_4861" n="e" s="T922">krospa </ts>
               <ts e="T924" id="Seg_4863" n="e" s="T923">abiʔi. </ts>
            </ts>
            <ts e="T935" id="Seg_4864" n="sc" s="T925">
               <ts e="T926" id="Seg_4866" n="e" s="T925">I </ts>
               <ts e="T927" id="Seg_4868" n="e" s="T926">dĭgəttə </ts>
               <ts e="T928" id="Seg_4870" n="e" s="T927">maːʔndə </ts>
               <ts e="T929" id="Seg_4872" n="e" s="T928">(so-) </ts>
               <ts e="T930" id="Seg_4874" n="e" s="T929">šobiʔi, </ts>
               <ts e="T931" id="Seg_4876" n="e" s="T930">(mĭ) </ts>
               <ts e="T932" id="Seg_4878" n="e" s="T931">mĭnzerleʔbəʔjə, </ts>
               <ts e="T933" id="Seg_4880" n="e" s="T932">i </ts>
               <ts e="T934" id="Seg_4882" n="e" s="T933">dĭgəttə </ts>
               <ts e="T935" id="Seg_4884" n="e" s="T934">amnaʔbəʔjə. </ts>
            </ts>
            <ts e="T939" id="Seg_4885" n="sc" s="T936">
               <ts e="T937" id="Seg_4887" n="e" s="T936">I </ts>
               <ts e="T938" id="Seg_4889" n="e" s="T937">bar </ts>
               <ts e="T939" id="Seg_4891" n="e" s="T938">(kulandə). </ts>
            </ts>
            <ts e="T944" id="Seg_4892" n="sc" s="T942">
               <ts e="T944" id="Seg_4894" n="e" s="T942">((…)). </ts>
            </ts>
            <ts e="T953" id="Seg_4895" n="sc" s="T946">
               <ts e="T953" id="Seg_4897" n="e" s="T946">По-русски. </ts>
            </ts>
            <ts e="T957" id="Seg_4898" n="sc" s="T954">
               <ts e="T956" id="Seg_4900" n="e" s="T954">Пили </ts>
               <ts e="T957" id="Seg_4902" n="e" s="T956">же. </ts>
            </ts>
            <ts e="T980" id="Seg_4903" n="sc" s="T959">
               <ts e="T961" id="Seg_4905" n="e" s="T959">(По-ру-) </ts>
               <ts e="T963" id="Seg_4907" n="e" s="T961">По-русски </ts>
               <ts e="T965" id="Seg_4909" n="e" s="T963">сказать, </ts>
               <ts e="T966" id="Seg_4911" n="e" s="T965">они </ts>
               <ts e="T967" id="Seg_4913" n="e" s="T966">отрежут </ts>
               <ts e="T968" id="Seg_4915" n="e" s="T967">от </ts>
               <ts e="T969" id="Seg_4917" n="e" s="T968">лесины </ts>
               <ts e="T970" id="Seg_4919" n="e" s="T969">такую </ts>
               <ts e="T971" id="Seg_4921" n="e" s="T970">чурку, </ts>
               <ts e="T972" id="Seg_4923" n="e" s="T971">расколят </ts>
               <ts e="T973" id="Seg_4925" n="e" s="T972">пополам </ts>
               <ts e="T974" id="Seg_4927" n="e" s="T973">и </ts>
               <ts e="T975" id="Seg_4929" n="e" s="T974">выдолбят </ts>
               <ts e="T976" id="Seg_4931" n="e" s="T975">туды. </ts>
               <ts e="T977" id="Seg_4933" n="e" s="T976">Не </ts>
               <ts e="T978" id="Seg_4935" n="e" s="T977">делали </ts>
               <ts e="T979" id="Seg_4937" n="e" s="T978">с </ts>
               <ts e="T980" id="Seg_4939" n="e" s="T979">досок. </ts>
            </ts>
            <ts e="T984" id="Seg_4940" n="sc" s="T982">
               <ts e="T983" id="Seg_4942" n="e" s="T982">Клали </ts>
               <ts e="T984" id="Seg_4944" n="e" s="T983">туда. </ts>
            </ts>
            <ts e="T1001" id="Seg_4945" n="sc" s="T999">
               <ts e="T1000" id="Seg_4947" n="e" s="T999">Ставили, </ts>
               <ts e="T1001" id="Seg_4949" n="e" s="T1000">наверное. </ts>
            </ts>
            <ts e="T1011" id="Seg_4950" n="sc" s="T1002">
               <ts e="T1003" id="Seg_4952" n="e" s="T1002">Ну, </ts>
               <ts e="T1004" id="Seg_4954" n="e" s="T1003">я </ts>
               <ts e="T1005" id="Seg_4956" n="e" s="T1004">не </ts>
               <ts e="T1006" id="Seg_4958" n="e" s="T1005">помню, </ts>
               <ts e="T1007" id="Seg_4960" n="e" s="T1006">это </ts>
               <ts e="T1008" id="Seg_4962" n="e" s="T1007">при </ts>
               <ts e="T1009" id="Seg_4964" n="e" s="T1008">мне </ts>
               <ts e="T1010" id="Seg_4966" n="e" s="T1009">ставили </ts>
               <ts e="T1011" id="Seg_4968" n="e" s="T1010">кресты. </ts>
            </ts>
            <ts e="T1019" id="Seg_4969" n="sc" s="T1013">
               <ts e="T1014" id="Seg_4971" n="e" s="T1013">А </ts>
               <ts e="T1015" id="Seg_4973" n="e" s="T1014">их </ts>
               <ts e="T1016" id="Seg_4975" n="e" s="T1015">не </ts>
               <ts e="T1017" id="Seg_4977" n="e" s="T1016">помню, </ts>
               <ts e="T1018" id="Seg_4979" n="e" s="T1017">не </ts>
               <ts e="T1019" id="Seg_4981" n="e" s="T1018">знаю. </ts>
            </ts>
            <ts e="T1050" id="Seg_4982" n="sc" s="T1020">
               <ts e="T1022" id="Seg_4984" n="e" s="T1020">(Вот=) </ts>
               <ts e="T1023" id="Seg_4986" n="e" s="T1022">Вот </ts>
               <ts e="T1024" id="Seg_4988" n="e" s="T1023">ты, </ts>
               <ts e="T1025" id="Seg_4990" n="e" s="T1024">вот </ts>
               <ts e="T1026" id="Seg_4992" n="e" s="T1025">где </ts>
               <ts e="T1027" id="Seg_4994" n="e" s="T1026">туто-ка, </ts>
               <ts e="T1028" id="Seg_4996" n="e" s="T1027">на </ts>
               <ts e="T1029" id="Seg_4998" n="e" s="T1028">бугре, </ts>
               <ts e="T1030" id="Seg_5000" n="e" s="T1029">как </ts>
               <ts e="T1031" id="Seg_5002" n="e" s="T1030">тут </ts>
               <ts e="T1032" id="Seg_5004" n="e" s="T1031">ямы, </ts>
               <ts e="T1033" id="Seg_5006" n="e" s="T1032">сюды </ts>
               <ts e="T1034" id="Seg_5008" n="e" s="T1033">переходишь, </ts>
               <ts e="T1035" id="Seg_5010" n="e" s="T1034">тут </ts>
               <ts e="T1036" id="Seg_5012" n="e" s="T1035">на </ts>
               <ts e="T1037" id="Seg_5014" n="e" s="T1036">углу </ts>
               <ts e="T1038" id="Seg_5016" n="e" s="T1037">было </ts>
               <ts e="T1039" id="Seg_5018" n="e" s="T1038">кладбище, </ts>
               <ts e="T1040" id="Seg_5020" n="e" s="T1039">и </ts>
               <ts e="T1043" id="Seg_5022" n="e" s="T1040">туда, </ts>
               <ts e="T1045" id="Seg_5024" n="e" s="T1043">там </ts>
               <ts e="T1048" id="Seg_5026" n="e" s="T1045">за </ts>
               <ts e="T1050" id="Seg_5028" n="e" s="T1048">(бугром). </ts>
            </ts>
            <ts e="T1073" id="Seg_5029" n="sc" s="T1051">
               <ts e="T1052" id="Seg_5031" n="e" s="T1051">Давнишнее, </ts>
               <ts e="T1053" id="Seg_5033" n="e" s="T1052">а </ts>
               <ts e="T1054" id="Seg_5035" n="e" s="T1053">потом </ts>
               <ts e="T1055" id="Seg_5037" n="e" s="T1054">там, </ts>
               <ts e="T1056" id="Seg_5039" n="e" s="T1055">где </ts>
               <ts e="T1057" id="Seg_5041" n="e" s="T1056">ты </ts>
               <ts e="T1058" id="Seg_5043" n="e" s="T1057">на </ts>
               <ts e="T1059" id="Seg_5045" n="e" s="T1058">краю </ts>
               <ts e="T1060" id="Seg_5047" n="e" s="T1059">снимал, </ts>
               <ts e="T1061" id="Seg_5049" n="e" s="T1060">там </ts>
               <ts e="T1062" id="Seg_5051" n="e" s="T1061">еще </ts>
               <ts e="T1063" id="Seg_5053" n="e" s="T1062">стара </ts>
               <ts e="T1064" id="Seg_5055" n="e" s="T1063">стойба, </ts>
               <ts e="T1065" id="Seg_5057" n="e" s="T1064">там </ts>
               <ts e="T1066" id="Seg_5059" n="e" s="T1065">тоже </ts>
               <ts e="T1067" id="Seg_5061" n="e" s="T1066">у </ts>
               <ts e="T1068" id="Seg_5063" n="e" s="T1067">их </ts>
               <ts e="T1069" id="Seg_5065" n="e" s="T1068">кладбище, </ts>
               <ts e="T1070" id="Seg_5067" n="e" s="T1069">это </ts>
               <ts e="T1071" id="Seg_5069" n="e" s="T1070">я </ts>
               <ts e="T1072" id="Seg_5071" n="e" s="T1071">видала </ts>
               <ts e="T1073" id="Seg_5073" n="e" s="T1072">там. </ts>
            </ts>
            <ts e="T1087" id="Seg_5074" n="sc" s="T1074">
               <ts e="T1076" id="Seg_5076" n="e" s="T1074">(Кресто-) </ts>
               <ts e="T1079" id="Seg_5078" n="e" s="T1076">Там </ts>
               <ts e="T1081" id="Seg_5080" n="e" s="T1079">крестов </ts>
               <ts e="T1083" id="Seg_5082" n="e" s="T1081">не </ts>
               <ts e="T1084" id="Seg_5084" n="e" s="T1083">было, </ts>
               <ts e="T1085" id="Seg_5086" n="e" s="T1084">уже </ts>
               <ts e="T1086" id="Seg_5088" n="e" s="T1085">только </ts>
               <ts e="T1087" id="Seg_5090" n="e" s="T1086">ямки. </ts>
            </ts>
            <ts e="T1108" id="Seg_5091" n="sc" s="T1088">
               <ts e="T1089" id="Seg_5093" n="e" s="T1088">Называли </ts>
               <ts e="T1090" id="Seg_5095" n="e" s="T1089">стара </ts>
               <ts e="T1091" id="Seg_5097" n="e" s="T1090">стойба, </ts>
               <ts e="T1092" id="Seg_5099" n="e" s="T1091">как </ts>
               <ts e="T1093" id="Seg_5101" n="e" s="T1092">они </ts>
               <ts e="T1094" id="Seg_5103" n="e" s="T1093">с </ts>
               <ts e="T1095" id="Seg_5105" n="e" s="T1094">тайги </ts>
               <ts e="T1096" id="Seg_5107" n="e" s="T1095">выезжали, </ts>
               <ts e="T1097" id="Seg_5109" n="e" s="T1096">и </ts>
               <ts e="T1098" id="Seg_5111" n="e" s="T1097">там </ts>
               <ts e="T1099" id="Seg_5113" n="e" s="T1098">они </ts>
               <ts e="T1100" id="Seg_5115" n="e" s="T1099">стояли </ts>
               <ts e="T1101" id="Seg_5117" n="e" s="T1100">зимовали, </ts>
               <ts e="T1102" id="Seg_5119" n="e" s="T1101">а </ts>
               <ts e="T1103" id="Seg_5121" n="e" s="T1102">потом </ts>
               <ts e="T1104" id="Seg_5123" n="e" s="T1103">сюды </ts>
               <ts e="T1105" id="Seg_5125" n="e" s="T1104">вот, </ts>
               <ts e="T1106" id="Seg_5127" n="e" s="T1105">где </ts>
               <ts e="T1107" id="Seg_5129" n="e" s="T1106">наша </ts>
               <ts e="T1108" id="Seg_5131" n="e" s="T1107">деревня. </ts>
            </ts>
            <ts e="T1116" id="Seg_5132" n="sc" s="T1109">
               <ts e="T1110" id="Seg_5134" n="e" s="T1109">Этот </ts>
               <ts e="T1111" id="Seg_5136" n="e" s="T1110">ключ </ts>
               <ts e="T1112" id="Seg_5138" n="e" s="T1111">нашли, </ts>
               <ts e="T1113" id="Seg_5140" n="e" s="T1112">он </ts>
               <ts e="T1114" id="Seg_5142" n="e" s="T1113">никогда </ts>
               <ts e="T1115" id="Seg_5144" n="e" s="T1114">не </ts>
               <ts e="T1116" id="Seg_5146" n="e" s="T1115">мерзнет. </ts>
            </ts>
            <ts e="T1126" id="Seg_5147" n="sc" s="T1117">
               <ts e="T1118" id="Seg_5149" n="e" s="T1117">(Ле-) </ts>
               <ts e="T1119" id="Seg_5151" n="e" s="T1118">Зимой </ts>
               <ts e="T1120" id="Seg_5153" n="e" s="T1119">только </ts>
               <ts e="T1121" id="Seg_5155" n="e" s="T1120">пар </ts>
               <ts e="T1122" id="Seg_5157" n="e" s="T1121">идет. </ts>
               <ts e="T1123" id="Seg_5159" n="e" s="T1122">А </ts>
               <ts e="T1124" id="Seg_5161" n="e" s="T1123">(ни-) </ts>
               <ts e="T1125" id="Seg_5163" n="e" s="T1124">лёду </ts>
               <ts e="T1126" id="Seg_5165" n="e" s="T1125">нету-ка. </ts>
            </ts>
            <ts e="T1134" id="Seg_5166" n="sc" s="T1128">
               <ts e="T1129" id="Seg_5168" n="e" s="T1128">Сейчас </ts>
               <ts e="T1130" id="Seg_5170" n="e" s="T1129">она </ts>
               <ts e="T1131" id="Seg_5172" n="e" s="T1130">холоднее, </ts>
               <ts e="T1132" id="Seg_5174" n="e" s="T1131">как </ts>
               <ts e="T1133" id="Seg_5176" n="e" s="T1132">и </ts>
               <ts e="T1134" id="Seg_5178" n="e" s="T1133">зимой. </ts>
            </ts>
            <ts e="T1138" id="Seg_5179" n="sc" s="T1135">
               <ts e="T1137" id="Seg_5181" n="e" s="T1135">Зимой </ts>
               <ts e="T1138" id="Seg_5183" n="e" s="T1137">теплая. </ts>
            </ts>
            <ts e="T1153" id="Seg_5184" n="sc" s="T1139">
               <ts e="T1140" id="Seg_5186" n="e" s="T1139">Вот </ts>
               <ts e="T1141" id="Seg_5188" n="e" s="T1140">мерзла </ts>
               <ts e="T1142" id="Seg_5190" n="e" s="T1141">тряпка, </ts>
               <ts e="T1143" id="Seg_5192" n="e" s="T1142">туды </ts>
               <ts e="T1144" id="Seg_5194" n="e" s="T1143">(пос-) </ts>
               <ts e="T1145" id="Seg_5196" n="e" s="T1144">спустишь </ts>
               <ts e="T1146" id="Seg_5198" n="e" s="T1145">в </ts>
               <ts e="T1147" id="Seg_5200" n="e" s="T1146">речку, </ts>
               <ts e="T1148" id="Seg_5202" n="e" s="T1147">она </ts>
               <ts e="T1149" id="Seg_5204" n="e" s="T1148">в </ts>
               <ts e="T1150" id="Seg_5206" n="e" s="T1149">речке </ts>
               <ts e="T1151" id="Seg_5208" n="e" s="T1150">растает, </ts>
               <ts e="T1152" id="Seg_5210" n="e" s="T1151">тряпка </ts>
               <ts e="T1153" id="Seg_5212" n="e" s="T1152">морожена. </ts>
            </ts>
            <ts e="T1158" id="Seg_5213" n="sc" s="T1155">
               <ts e="T1156" id="Seg_5215" n="e" s="T1155">Так </ts>
               <ts e="T1157" id="Seg_5217" n="e" s="T1156">((…)) </ts>
               <ts e="T1158" id="Seg_5219" n="e" s="T1157">мороз. </ts>
            </ts>
            <ts e="T1168" id="Seg_5220" n="sc" s="T1160">
               <ts e="T1161" id="Seg_5222" n="e" s="T1160">И </ts>
               <ts e="T1162" id="Seg_5224" n="e" s="T1161">она </ts>
               <ts e="T1163" id="Seg_5226" n="e" s="T1162">была </ts>
               <ts e="T1164" id="Seg_5228" n="e" s="T1163">((…)) </ts>
               <ts e="T1165" id="Seg_5230" n="e" s="T1164">(талисы), </ts>
               <ts e="T1167" id="Seg_5232" n="e" s="T1165">талая, </ts>
               <ts e="T1168" id="Seg_5234" n="e" s="T1167">(талицы). </ts>
            </ts>
            <ts e="T1193" id="Seg_5235" n="sc" s="T1187">
               <ts e="T1188" id="Seg_5237" n="e" s="T1187">Ну, </ts>
               <ts e="T1189" id="Seg_5239" n="e" s="T1188">приходили, </ts>
               <ts e="T1190" id="Seg_5241" n="e" s="T1189">поминали, </ts>
               <ts e="T1191" id="Seg_5243" n="e" s="T1190">на </ts>
               <ts e="T1192" id="Seg_5245" n="e" s="T1191">стол </ts>
               <ts e="T1193" id="Seg_5247" n="e" s="T1192">собирали. </ts>
            </ts>
            <ts e="T1210" id="Seg_5248" n="sc" s="T1194">
               <ts e="T1195" id="Seg_5250" n="e" s="T1194">(Нав-) </ts>
               <ts e="T1196" id="Seg_5252" n="e" s="T1195">Наварют </ts>
               <ts e="T1197" id="Seg_5254" n="e" s="T1196">и, </ts>
               <ts e="T1198" id="Seg_5256" n="e" s="T1197">этого, </ts>
               <ts e="T1199" id="Seg_5258" n="e" s="T1198">и </ts>
               <ts e="T1200" id="Seg_5260" n="e" s="T1199">киселю, </ts>
               <ts e="T1201" id="Seg_5262" n="e" s="T1200">и </ts>
               <ts e="T1202" id="Seg_5264" n="e" s="T1201">хлебного. </ts>
               <ts e="T1203" id="Seg_5266" n="e" s="T1202">Ну, </ts>
               <ts e="T1204" id="Seg_5268" n="e" s="T1203">они </ts>
               <ts e="T1205" id="Seg_5270" n="e" s="T1204">картофельного </ts>
               <ts e="T1206" id="Seg_5272" n="e" s="T1205">тады </ts>
               <ts e="T1207" id="Seg_5274" n="e" s="T1206">не </ts>
               <ts e="T1208" id="Seg_5276" n="e" s="T1207">знали, </ts>
               <ts e="T1209" id="Seg_5278" n="e" s="T1208">хлебный </ts>
               <ts e="T1210" id="Seg_5280" n="e" s="T1209">делали. </ts>
            </ts>
            <ts e="T1214" id="Seg_5281" n="sc" s="T1211">
               <ts e="T1212" id="Seg_5283" n="e" s="T1211">Лапшу </ts>
               <ts e="T1213" id="Seg_5285" n="e" s="T1212">сами </ts>
               <ts e="T1214" id="Seg_5287" n="e" s="T1213">делали. </ts>
            </ts>
            <ts e="T1226" id="Seg_5288" n="sc" s="T1215">
               <ts e="T1216" id="Seg_5290" n="e" s="T1215">Как </ts>
               <ts e="T1217" id="Seg_5292" n="e" s="T1216">теперича </ts>
               <ts e="T1218" id="Seg_5294" n="e" s="T1217">вот, </ts>
               <ts e="T1219" id="Seg_5296" n="e" s="T1218">лапша </ts>
               <ts e="T1220" id="Seg_5298" n="e" s="T1219">есть </ts>
               <ts e="T1221" id="Seg_5300" n="e" s="T1220">в </ts>
               <ts e="T1222" id="Seg_5302" n="e" s="T1221">магазинах, </ts>
               <ts e="T1223" id="Seg_5304" n="e" s="T1222">а </ts>
               <ts e="T1224" id="Seg_5306" n="e" s="T1223">они </ts>
               <ts e="T1225" id="Seg_5308" n="e" s="T1224">сами </ts>
               <ts e="T1226" id="Seg_5310" n="e" s="T1225">делали. </ts>
            </ts>
            <ts e="T1230" id="Seg_5311" n="sc" s="T1227">
               <ts e="T1228" id="Seg_5313" n="e" s="T1227">Не </ts>
               <ts e="T1229" id="Seg_5315" n="e" s="T1228">знали </ts>
               <ts e="T1230" id="Seg_5317" n="e" s="T1229">этого. </ts>
            </ts>
            <ts e="T1276" id="Seg_5318" n="sc" s="T1270">
               <ts e="T1271" id="Seg_5320" n="e" s="T1270">Nu, </ts>
               <ts e="T1272" id="Seg_5322" n="e" s="T1271">kamen </ts>
               <ts e="T1273" id="Seg_5324" n="e" s="T1272">külalləj </ts>
               <ts e="T1274" id="Seg_5326" n="e" s="T1273">kuza, </ts>
               <ts e="T1275" id="Seg_5328" n="e" s="T1274">dʼünə </ts>
               <ts e="T1276" id="Seg_5330" n="e" s="T1275">(tĭlleʔbəʔə). </ts>
            </ts>
            <ts e="T1284" id="Seg_5331" n="sc" s="T1277">
               <ts e="T1278" id="Seg_5333" n="e" s="T1277">A </ts>
               <ts e="T1279" id="Seg_5335" n="e" s="T1278">(dĭn </ts>
               <ts e="T1280" id="Seg_5337" n="e" s="T1279">nʼuʔtə=) </ts>
               <ts e="T1281" id="Seg_5339" n="e" s="T1280">dux </ts>
               <ts e="T1282" id="Seg_5341" n="e" s="T1281">dĭn </ts>
               <ts e="T1533" id="Seg_5343" n="e" s="T1282">kalla </ts>
               <ts e="T1283" id="Seg_5345" n="e" s="T1533">dʼürləj </ts>
               <ts e="T1284" id="Seg_5347" n="e" s="T1283">nʼuʔtə. </ts>
            </ts>
            <ts e="T1288" id="Seg_5348" n="sc" s="T1285">
               <ts e="T1286" id="Seg_5350" n="e" s="T1285">Măn </ts>
               <ts e="T1287" id="Seg_5352" n="e" s="T1286">dăre </ts>
               <ts e="T1288" id="Seg_5354" n="e" s="T1287">tĭmnem. </ts>
            </ts>
            <ts e="T1297" id="Seg_5355" n="sc" s="T1289">
               <ts e="T1290" id="Seg_5357" n="e" s="T1289">Не </ts>
               <ts e="T1291" id="Seg_5359" n="e" s="T1290">знаю, </ts>
               <ts e="T1292" id="Seg_5361" n="e" s="T1291">šində </ts>
               <ts e="T1293" id="Seg_5363" n="e" s="T1292">kăde, </ts>
               <ts e="T1294" id="Seg_5365" n="e" s="T1293">a </ts>
               <ts e="T1295" id="Seg_5367" n="e" s="T1294">măn </ts>
               <ts e="T1296" id="Seg_5369" n="e" s="T1295">dăre </ts>
               <ts e="T1297" id="Seg_5371" n="e" s="T1296">tĭmnem. </ts>
            </ts>
            <ts e="T1304" id="Seg_5372" n="sc" s="T1298">
               <ts e="T1299" id="Seg_5374" n="e" s="T1298">I </ts>
               <ts e="T1300" id="Seg_5376" n="e" s="T1299">iam </ts>
               <ts e="T1301" id="Seg_5378" n="e" s="T1300">(măndə=) </ts>
               <ts e="T1302" id="Seg_5380" n="e" s="T1301">dăre </ts>
               <ts e="T1303" id="Seg_5382" n="e" s="T1302">măndə </ts>
               <ts e="T1304" id="Seg_5384" n="e" s="T1303">măna. </ts>
            </ts>
            <ts e="T1309" id="Seg_5385" n="sc" s="T1305">
               <ts e="T1306" id="Seg_5387" n="e" s="T1305">Душа </ts>
               <ts e="T1307" id="Seg_5389" n="e" s="T1306">dĭn </ts>
               <ts e="T1308" id="Seg_5391" n="e" s="T1307">kalaj </ts>
               <ts e="T1309" id="Seg_5393" n="e" s="T1308">kudajdə. </ts>
            </ts>
            <ts e="T1330" id="Seg_5394" n="sc" s="T1312">
               <ts e="T1313" id="Seg_5396" n="e" s="T1312">Kuzam </ts>
               <ts e="T1314" id="Seg_5398" n="e" s="T1313">kudaj </ts>
               <ts e="T1315" id="Seg_5400" n="e" s="T1314">abi </ts>
               <ts e="T1316" id="Seg_5402" n="e" s="T1315">bostə </ts>
               <ts e="T1317" id="Seg_5404" n="e" s="T1316">dʼügən. </ts>
               <ts e="T1318" id="Seg_5406" n="e" s="T1317">I </ts>
               <ts e="T1319" id="Seg_5408" n="e" s="T1318">dĭʔnə </ts>
               <ts e="T1320" id="Seg_5410" n="e" s="T1319">bar </ts>
               <ts e="T1321" id="Seg_5412" n="e" s="T1320">((…)) </ts>
               <ts e="T1322" id="Seg_5414" n="e" s="T1321">bostə </ts>
               <ts e="T1323" id="Seg_5416" n="e" s="T1322">duxtə </ts>
               <ts e="T1324" id="Seg_5418" n="e" s="T1323">(da-) </ts>
               <ts e="T1325" id="Seg_5420" n="e" s="T1324">mĭbi, </ts>
               <ts e="T1326" id="Seg_5422" n="e" s="T1325">i </ts>
               <ts e="T1327" id="Seg_5424" n="e" s="T1326">dĭ </ts>
               <ts e="T1328" id="Seg_5426" n="e" s="T1327">uʔbdəbi, </ts>
               <ts e="T1329" id="Seg_5428" n="e" s="T1328">kuza </ts>
               <ts e="T1330" id="Seg_5430" n="e" s="T1329">molambi. </ts>
            </ts>
            <ts e="T1339" id="Seg_5431" n="sc" s="T1331">
               <ts e="T1332" id="Seg_5433" n="e" s="T1331">(Понят-) </ts>
               <ts e="T1333" id="Seg_5435" n="e" s="T1332">Понимаешь </ts>
               <ts e="T1334" id="Seg_5437" n="e" s="T1333">это, </ts>
               <ts e="T1335" id="Seg_5439" n="e" s="T1334">чего </ts>
               <ts e="T1336" id="Seg_5441" n="e" s="T1335">я </ts>
               <ts e="T1337" id="Seg_5443" n="e" s="T1336">тебе </ts>
               <ts e="T1339" id="Seg_5445" n="e" s="T1337">рассказала? </ts>
            </ts>
            <ts e="T1368" id="Seg_5446" n="sc" s="T1348">
               <ts e="T1350" id="Seg_5448" n="e" s="T1348">(Это=) </ts>
               <ts e="T1352" id="Seg_5450" n="e" s="T1350">это </ts>
               <ts e="T1354" id="Seg_5452" n="e" s="T1352">я </ts>
               <ts e="T1356" id="Seg_5454" n="e" s="T1354">тебе </ts>
               <ts e="T1358" id="Seg_5456" n="e" s="T1356">не </ts>
               <ts e="T1359" id="Seg_5458" n="e" s="T1358">говорила, </ts>
               <ts e="T1360" id="Seg_5460" n="e" s="T1359">вот </ts>
               <ts e="T1362" id="Seg_5462" n="e" s="T1360">это </ts>
               <ts e="T1363" id="Seg_5464" n="e" s="T1362">вот, </ts>
               <ts e="T1365" id="Seg_5466" n="e" s="T1363">чего </ts>
               <ts e="T1366" id="Seg_5468" n="e" s="T1365">сейчас </ts>
               <ts e="T1368" id="Seg_5470" n="e" s="T1366">гоою. </ts>
            </ts>
            <ts e="T1375" id="Seg_5471" n="sc" s="T1369">
               <ts e="T1370" id="Seg_5473" n="e" s="T1369">Это </ts>
               <ts e="T1371" id="Seg_5475" n="e" s="T1370">я </ts>
               <ts e="T1372" id="Seg_5477" n="e" s="T1371">с </ts>
               <ts e="T1373" id="Seg_5479" n="e" s="T1372">Писания </ts>
               <ts e="T1375" id="Seg_5481" n="e" s="T1373">говорю. </ts>
            </ts>
            <ts e="T1404" id="Seg_5482" n="sc" s="T1377">
               <ts e="T1378" id="Seg_5484" n="e" s="T1377">(Когда=) </ts>
               <ts e="T1379" id="Seg_5486" n="e" s="T1378">Когда </ts>
               <ts e="T1380" id="Seg_5488" n="e" s="T1379">Господь </ts>
               <ts e="T1381" id="Seg_5490" n="e" s="T1380">очертил </ts>
               <ts e="T1382" id="Seg_5492" n="e" s="T1381">(свою=) </ts>
               <ts e="T1383" id="Seg_5494" n="e" s="T1382">свою </ts>
               <ts e="T1384" id="Seg_5496" n="e" s="T1383">и </ts>
               <ts e="T1385" id="Seg_5498" n="e" s="T1384">вдунул </ts>
               <ts e="T1386" id="Seg_5500" n="e" s="T1385">в </ts>
               <ts e="T1387" id="Seg_5502" n="e" s="T1386">человека </ts>
               <ts e="T1388" id="Seg_5504" n="e" s="T1387">на </ts>
               <ts e="T1389" id="Seg_5506" n="e" s="T1388">земле, </ts>
               <ts e="T1390" id="Seg_5508" n="e" s="T1389">по </ts>
               <ts e="T1391" id="Seg_5510" n="e" s="T1390">образу, </ts>
               <ts e="T1392" id="Seg_5512" n="e" s="T1391">по </ts>
               <ts e="T1393" id="Seg_5514" n="e" s="T1392">подобию </ts>
               <ts e="T1394" id="Seg_5516" n="e" s="T1393">своему </ts>
               <ts e="T1395" id="Seg_5518" n="e" s="T1394">очертил </ts>
               <ts e="T1396" id="Seg_5520" n="e" s="T1395">и </ts>
               <ts e="T1397" id="Seg_5522" n="e" s="T1396">вдунул </ts>
               <ts e="T1398" id="Seg_5524" n="e" s="T1397">в </ts>
               <ts e="T1399" id="Seg_5526" n="e" s="T1398">его </ts>
               <ts e="T1400" id="Seg_5528" n="e" s="T1399">духа, </ts>
               <ts e="T1401" id="Seg_5530" n="e" s="T1400">и </ts>
               <ts e="T1402" id="Seg_5532" n="e" s="T1401">он </ts>
               <ts e="T1403" id="Seg_5534" n="e" s="T1402">стал </ts>
               <ts e="T1404" id="Seg_5536" n="e" s="T1403">живой. </ts>
            </ts>
            <ts e="T1412" id="Seg_5537" n="sc" s="T1406">
               <ts e="T1407" id="Seg_5539" n="e" s="T1406">Так </ts>
               <ts e="T1408" id="Seg_5541" n="e" s="T1407">(я </ts>
               <ts e="T1409" id="Seg_5543" n="e" s="T1408">это=) </ts>
               <ts e="T1410" id="Seg_5545" n="e" s="T1409">я </ts>
               <ts e="T1411" id="Seg_5547" n="e" s="T1410">это </ts>
               <ts e="T1412" id="Seg_5549" n="e" s="T1411">(передаю). </ts>
            </ts>
            <ts e="T1453" id="Seg_5550" n="sc" s="T1451">
               <ts e="T1452" id="Seg_5552" n="e" s="T1451">No, </ts>
               <ts e="T1453" id="Seg_5554" n="e" s="T1452">kabarləj. </ts>
            </ts>
            <ts e="T1457" id="Seg_5555" n="sc" s="T1454">
               <ts e="T1455" id="Seg_5557" n="e" s="T1454">Хватит, </ts>
               <ts e="T1457" id="Seg_5559" n="e" s="T1455">говорю. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T12" id="Seg_5560" s="T1">PKZ_19700818_09341-2bz.PKZ.001 (001)</ta>
            <ta e="T29" id="Seg_5561" s="T13">PKZ_19700818_09341-2bz.PKZ.002 (003)</ta>
            <ta e="T52" id="Seg_5562" s="T49">PKZ_19700818_09341-2bz.PKZ.003 (007)</ta>
            <ta e="T62" id="Seg_5563" s="T53">PKZ_19700818_09341-2bz.PKZ.004 (008)</ta>
            <ta e="T67" id="Seg_5564" s="T63">PKZ_19700818_09341-2bz.PKZ.005 (009)</ta>
            <ta e="T74" id="Seg_5565" s="T67">PKZ_19700818_09341-2bz.PKZ.006 (010)</ta>
            <ta e="T89" id="Seg_5566" s="T75">PKZ_19700818_09341-2bz.PKZ.007 (011)</ta>
            <ta e="T96" id="Seg_5567" s="T91">PKZ_19700818_09341-2bz.PKZ.008 (013)</ta>
            <ta e="T119" id="Seg_5568" s="T105">PKZ_19700818_09341-2bz.PKZ.009 (016)</ta>
            <ta e="T123" id="Seg_5569" s="T120">PKZ_19700818_09341-2bz.PKZ.010 (017)</ta>
            <ta e="T129" id="Seg_5570" s="T123">PKZ_19700818_09341-2bz.PKZ.011 (018)</ta>
            <ta e="T131" id="Seg_5571" s="T129">PKZ_19700818_09341-2bz.PKZ.012 (019)</ta>
            <ta e="T135" id="Seg_5572" s="T132">PKZ_19700818_09341-2bz.PKZ.013 (020)</ta>
            <ta e="T141" id="Seg_5573" s="T136">PKZ_19700818_09341-2bz.PKZ.014 (021)</ta>
            <ta e="T148" id="Seg_5574" s="T142">PKZ_19700818_09341-2bz.PKZ.015 (022)</ta>
            <ta e="T155" id="Seg_5575" s="T149">PKZ_19700818_09341-2bz.PKZ.016 (023)</ta>
            <ta e="T177" id="Seg_5576" s="T172">PKZ_19700818_09341-2bz.PKZ.017 (027)</ta>
            <ta e="T187" id="Seg_5577" s="T178">PKZ_19700818_09341-2bz.PKZ.018 (028)</ta>
            <ta e="T192" id="Seg_5578" s="T188">PKZ_19700818_09341-2bz.PKZ.019 (029)</ta>
            <ta e="T201" id="Seg_5579" s="T193">PKZ_19700818_09341-2bz.PKZ.020 (030)</ta>
            <ta e="T209" id="Seg_5580" s="T202">PKZ_19700818_09341-2bz.PKZ.021 (031)</ta>
            <ta e="T211" id="Seg_5581" s="T210">PKZ_19700818_09341-2bz.PKZ.022 (032)</ta>
            <ta e="T215" id="Seg_5582" s="T212">PKZ_19700818_09341-2bz.PKZ.023 (033)</ta>
            <ta e="T223" id="Seg_5583" s="T217">PKZ_19700818_09341-2bz.PKZ.024 (035)</ta>
            <ta e="T233" id="Seg_5584" s="T229">PKZ_19700818_09341-2bz.PKZ.025 (038)</ta>
            <ta e="T251" id="Seg_5585" s="T247">PKZ_19700818_09341-2bz.PKZ.026 (041)</ta>
            <ta e="T256" id="Seg_5586" s="T252">PKZ_19700818_09341-2bz.PKZ.027 (042)</ta>
            <ta e="T263" id="Seg_5587" s="T256">PKZ_19700818_09341-2bz.PKZ.028 (043)</ta>
            <ta e="T279" id="Seg_5588" s="T265">PKZ_19700818_09341-2bz.PKZ.029 (045)</ta>
            <ta e="T287" id="Seg_5589" s="T280">PKZ_19700818_09341-2bz.PKZ.030 (046)</ta>
            <ta e="T300" id="Seg_5590" s="T288">PKZ_19700818_09341-2bz.PKZ.031 (047)</ta>
            <ta e="T315" id="Seg_5591" s="T301">PKZ_19700818_09341-2bz.PKZ.032 (048)</ta>
            <ta e="T327" id="Seg_5592" s="T315">PKZ_19700818_09341-2bz.PKZ.033 (049)</ta>
            <ta e="T343" id="Seg_5593" s="T328">PKZ_19700818_09341-2bz.PKZ.034 (050)</ta>
            <ta e="T346" id="Seg_5594" s="T344">PKZ_19700818_09341-2bz.PKZ.035 (051)</ta>
            <ta e="T360" id="Seg_5595" s="T350">PKZ_19700818_09341-2bz.PKZ.036 (053)</ta>
            <ta e="T378" id="Seg_5596" s="T371">PKZ_19700818_09341-2bz.PKZ.037 (057)</ta>
            <ta e="T383" id="Seg_5597" s="T379">PKZ_19700818_09341-2bz.PKZ.038 (058)</ta>
            <ta e="T387" id="Seg_5598" s="T383">PKZ_19700818_09341-2bz.PKZ.039 (060)</ta>
            <ta e="T392" id="Seg_5599" s="T388">PKZ_19700818_09341-2bz.PKZ.040 (061)</ta>
            <ta e="T400" id="Seg_5600" s="T392">PKZ_19700818_09341-2bz.PKZ.041 (062)</ta>
            <ta e="T410" id="Seg_5601" s="T403">PKZ_19700818_09341-2bz.PKZ.042 (064)</ta>
            <ta e="T438" id="Seg_5602" s="T433">PKZ_19700818_09341-2bz.PKZ.043 (069)</ta>
            <ta e="T444" id="Seg_5603" s="T440">PKZ_19700818_09341-2bz.PKZ.044 (071)</ta>
            <ta e="T462" id="Seg_5604" s="T458">PKZ_19700818_09341-2bz.PKZ.045 (075)</ta>
            <ta e="T468" id="Seg_5605" s="T463">PKZ_19700818_09341-2bz.PKZ.046 (076)</ta>
            <ta e="T472" id="Seg_5606" s="T469">PKZ_19700818_09341-2bz.PKZ.047 (077)</ta>
            <ta e="T475" id="Seg_5607" s="T473">PKZ_19700818_09341-2bz.PKZ.048 (078)</ta>
            <ta e="T480" id="Seg_5608" s="T476">PKZ_19700818_09341-2bz.PKZ.049 (079)</ta>
            <ta e="T508" id="Seg_5609" s="T502">PKZ_19700818_09341-2bz.PKZ.050 (084)</ta>
            <ta e="T515" id="Seg_5610" s="T509">PKZ_19700818_09341-2bz.PKZ.051 (085)</ta>
            <ta e="T518" id="Seg_5611" s="T516">PKZ_19700818_09341-2bz.PKZ.052 (086)</ta>
            <ta e="T524" id="Seg_5612" s="T519">PKZ_19700818_09341-2bz.PKZ.053 (087)</ta>
            <ta e="T530" id="Seg_5613" s="T525">PKZ_19700818_09341-2bz.PKZ.054 (088)</ta>
            <ta e="T537" id="Seg_5614" s="T531">PKZ_19700818_09341-2bz.PKZ.055 (089)</ta>
            <ta e="T544" id="Seg_5615" s="T538">PKZ_19700818_09341-2bz.PKZ.056 (090)</ta>
            <ta e="T552" id="Seg_5616" s="T546">PKZ_19700818_09341-2bz.PKZ.057 (092)</ta>
            <ta e="T562" id="Seg_5617" s="T561">PKZ_19700818_09341-2bz.PKZ.058 (095)</ta>
            <ta e="T565" id="Seg_5618" s="T563">PKZ_19700818_09341-2bz.PKZ.059 (096)</ta>
            <ta e="T571" id="Seg_5619" s="T566">PKZ_19700818_09341-2bz.PKZ.060 (097)</ta>
            <ta e="T577" id="Seg_5620" s="T572">PKZ_19700818_09341-2bz.PKZ.061 (098)</ta>
            <ta e="T591" id="Seg_5621" s="T589">PKZ_19700818_09341-2bz.PKZ.062 (103)</ta>
            <ta e="T593" id="Seg_5622" s="T592">PKZ_19700818_09341-2bz.PKZ.063 (105)</ta>
            <ta e="T605" id="Seg_5623" s="T602">PKZ_19700818_09341-2bz.PKZ.064 (108)</ta>
            <ta e="T616" id="Seg_5624" s="T608">PKZ_19700818_09341-2bz.PKZ.065 (110)</ta>
            <ta e="T633" id="Seg_5625" s="T618">PKZ_19700818_09341-2bz.PKZ.066 (112)</ta>
            <ta e="T640" id="Seg_5626" s="T634">PKZ_19700818_09341-2bz.PKZ.067 (113)</ta>
            <ta e="T643" id="Seg_5627" s="T641">PKZ_19700818_09341-2bz.PKZ.068 (114)</ta>
            <ta e="T655" id="Seg_5628" s="T647">PKZ_19700818_09341-2bz.PKZ.069 (116)</ta>
            <ta e="T674" id="Seg_5629" s="T661">PKZ_19700818_09341-2bz.PKZ.070 (118)</ta>
            <ta e="T683" id="Seg_5630" s="T676">PKZ_19700818_09341-2bz.PKZ.071 (120)</ta>
            <ta e="T688" id="Seg_5631" s="T684">PKZ_19700818_09341-2bz.PKZ.072 (121)</ta>
            <ta e="T701" id="Seg_5632" s="T689">PKZ_19700818_09341-2bz.PKZ.073 (122)</ta>
            <ta e="T703" id="Seg_5633" s="T702">PKZ_19700818_09341-2bz.PKZ.074 (123)</ta>
            <ta e="T727" id="Seg_5634" s="T722">PKZ_19700818_09341-2bz.PKZ.075 (130)</ta>
            <ta e="T736" id="Seg_5635" s="T728">PKZ_19700818_09341-2bz.PKZ.076 (131)</ta>
            <ta e="T741" id="Seg_5636" s="T737">PKZ_19700818_09341-2bz.PKZ.077 (132)</ta>
            <ta e="T762" id="Seg_5637" s="T758">PKZ_19700818_09341-2bz.PKZ.078 (137)</ta>
            <ta e="T766" id="Seg_5638" s="T763">PKZ_19700818_09341-2bz.PKZ.079 (138)</ta>
            <ta e="T775" id="Seg_5639" s="T770">PKZ_19700818_09341-2bz.PKZ.080 (140)</ta>
            <ta e="T780" id="Seg_5640" s="T776">PKZ_19700818_09341-2bz.PKZ.081 (141)</ta>
            <ta e="T782" id="Seg_5641" s="T780">PKZ_19700818_09341-2bz.PKZ.082 (142)</ta>
            <ta e="T784" id="Seg_5642" s="T783">PKZ_19700818_09341-2bz.PKZ.083 (143)</ta>
            <ta e="T790" id="Seg_5643" s="T785">PKZ_19700818_09341-2bz.PKZ.084 (144)</ta>
            <ta e="T793" id="Seg_5644" s="T791">PKZ_19700818_09341-2bz.PKZ.085 (145)</ta>
            <ta e="T797" id="Seg_5645" s="T794">PKZ_19700818_09341-2bz.PKZ.086 (146)</ta>
            <ta e="T804" id="Seg_5646" s="T800">PKZ_19700818_09341-2bz.PKZ.087 (148)</ta>
            <ta e="T809" id="Seg_5647" s="T805">PKZ_19700818_09341-2bz.PKZ.088 (149)</ta>
            <ta e="T813" id="Seg_5648" s="T810">PKZ_19700818_09341-2bz.PKZ.089 (150)</ta>
            <ta e="T820" id="Seg_5649" s="T814">PKZ_19700818_09341-2bz.PKZ.090 (151)</ta>
            <ta e="T829" id="Seg_5650" s="T820">PKZ_19700818_09341-2bz.PKZ.091 (152)</ta>
            <ta e="T835" id="Seg_5651" s="T830">PKZ_19700818_09341-2bz.PKZ.092 (153)</ta>
            <ta e="T839" id="Seg_5652" s="T836">PKZ_19700818_09341-2bz.PKZ.093 (154)</ta>
            <ta e="T856" id="Seg_5653" s="T840">PKZ_19700818_09341-2bz.PKZ.094 (155)</ta>
            <ta e="T868" id="Seg_5654" s="T857">PKZ_19700818_09341-2bz.PKZ.095 (156)</ta>
            <ta e="T877" id="Seg_5655" s="T870">PKZ_19700818_09341-2bz.PKZ.096 (158)</ta>
            <ta e="T884" id="Seg_5656" s="T878">PKZ_19700818_09341-2bz.PKZ.097 (159)</ta>
            <ta e="T901" id="Seg_5657" s="T892">PKZ_19700818_09341-2bz.PKZ.098 (162)</ta>
            <ta e="T911" id="Seg_5658" s="T902">PKZ_19700818_09341-2bz.PKZ.099 (163)</ta>
            <ta e="T917" id="Seg_5659" s="T912">PKZ_19700818_09341-2bz.PKZ.100 (164)</ta>
            <ta e="T920" id="Seg_5660" s="T918">PKZ_19700818_09341-2bz.PKZ.101 (165)</ta>
            <ta e="T924" id="Seg_5661" s="T921">PKZ_19700818_09341-2bz.PKZ.102 (166)</ta>
            <ta e="T935" id="Seg_5662" s="T925">PKZ_19700818_09341-2bz.PKZ.103 (167)</ta>
            <ta e="T939" id="Seg_5663" s="T936">PKZ_19700818_09341-2bz.PKZ.104 (168)</ta>
            <ta e="T944" id="Seg_5664" s="T942">PKZ_19700818_09341-2bz.PKZ.105 (170)</ta>
            <ta e="T953" id="Seg_5665" s="T946">PKZ_19700818_09341-2bz.PKZ.106 (172)</ta>
            <ta e="T957" id="Seg_5666" s="T954">PKZ_19700818_09341-2bz.PKZ.107 (174)</ta>
            <ta e="T976" id="Seg_5667" s="T959">PKZ_19700818_09341-2bz.PKZ.108 (176)</ta>
            <ta e="T980" id="Seg_5668" s="T976">PKZ_19700818_09341-2bz.PKZ.109 (177)</ta>
            <ta e="T984" id="Seg_5669" s="T982">PKZ_19700818_09341-2bz.PKZ.110 (179)</ta>
            <ta e="T1001" id="Seg_5670" s="T999">PKZ_19700818_09341-2bz.PKZ.111 (182)</ta>
            <ta e="T1011" id="Seg_5671" s="T1002">PKZ_19700818_09341-2bz.PKZ.112 (183)</ta>
            <ta e="T1019" id="Seg_5672" s="T1013">PKZ_19700818_09341-2bz.PKZ.113 (185)</ta>
            <ta e="T1050" id="Seg_5673" s="T1020">PKZ_19700818_09341-2bz.PKZ.114 (186)</ta>
            <ta e="T1073" id="Seg_5674" s="T1051">PKZ_19700818_09341-2bz.PKZ.115 (189)</ta>
            <ta e="T1087" id="Seg_5675" s="T1074">PKZ_19700818_09341-2bz.PKZ.116 (190)</ta>
            <ta e="T1108" id="Seg_5676" s="T1088">PKZ_19700818_09341-2bz.PKZ.117 (192)</ta>
            <ta e="T1116" id="Seg_5677" s="T1109">PKZ_19700818_09341-2bz.PKZ.118 (193)</ta>
            <ta e="T1122" id="Seg_5678" s="T1117">PKZ_19700818_09341-2bz.PKZ.119 (194)</ta>
            <ta e="T1126" id="Seg_5679" s="T1122">PKZ_19700818_09341-2bz.PKZ.120 (195)</ta>
            <ta e="T1134" id="Seg_5680" s="T1128">PKZ_19700818_09341-2bz.PKZ.121 (197)</ta>
            <ta e="T1138" id="Seg_5681" s="T1135">PKZ_19700818_09341-2bz.PKZ.122 (198)</ta>
            <ta e="T1153" id="Seg_5682" s="T1139">PKZ_19700818_09341-2bz.PKZ.123 (199)</ta>
            <ta e="T1158" id="Seg_5683" s="T1155">PKZ_19700818_09341-2bz.PKZ.124 (201)</ta>
            <ta e="T1168" id="Seg_5684" s="T1160">PKZ_19700818_09341-2bz.PKZ.125 (202)</ta>
            <ta e="T1193" id="Seg_5685" s="T1187">PKZ_19700818_09341-2bz.PKZ.126 (207)</ta>
            <ta e="T1202" id="Seg_5686" s="T1194">PKZ_19700818_09341-2bz.PKZ.127 (208)</ta>
            <ta e="T1210" id="Seg_5687" s="T1202">PKZ_19700818_09341-2bz.PKZ.128 (209)</ta>
            <ta e="T1214" id="Seg_5688" s="T1211">PKZ_19700818_09341-2bz.PKZ.129 (210)</ta>
            <ta e="T1226" id="Seg_5689" s="T1215">PKZ_19700818_09341-2bz.PKZ.130 (211)</ta>
            <ta e="T1230" id="Seg_5690" s="T1227">PKZ_19700818_09341-2bz.PKZ.131 (212)</ta>
            <ta e="T1276" id="Seg_5691" s="T1270">PKZ_19700818_09341-2bz.PKZ.132 (216)</ta>
            <ta e="T1284" id="Seg_5692" s="T1277">PKZ_19700818_09341-2bz.PKZ.133 (217)</ta>
            <ta e="T1288" id="Seg_5693" s="T1285">PKZ_19700818_09341-2bz.PKZ.134 (218)</ta>
            <ta e="T1297" id="Seg_5694" s="T1289">PKZ_19700818_09341-2bz.PKZ.135 (219)</ta>
            <ta e="T1304" id="Seg_5695" s="T1298">PKZ_19700818_09341-2bz.PKZ.136 (220)</ta>
            <ta e="T1309" id="Seg_5696" s="T1305">PKZ_19700818_09341-2bz.PKZ.137 (221)</ta>
            <ta e="T1317" id="Seg_5697" s="T1312">PKZ_19700818_09341-2bz.PKZ.138 (223)</ta>
            <ta e="T1330" id="Seg_5698" s="T1317">PKZ_19700818_09341-2bz.PKZ.139 (224)</ta>
            <ta e="T1339" id="Seg_5699" s="T1331">PKZ_19700818_09341-2bz.PKZ.140 (225)</ta>
            <ta e="T1368" id="Seg_5700" s="T1348">PKZ_19700818_09341-2bz.PKZ.141 (228)</ta>
            <ta e="T1375" id="Seg_5701" s="T1369">PKZ_19700818_09341-2bz.PKZ.142 (230)</ta>
            <ta e="T1404" id="Seg_5702" s="T1377">PKZ_19700818_09341-2bz.PKZ.143 (232)</ta>
            <ta e="T1412" id="Seg_5703" s="T1406">PKZ_19700818_09341-2bz.PKZ.144 (234)</ta>
            <ta e="T1453" id="Seg_5704" s="T1451">PKZ_19700818_09341-2bz.PKZ.145 (240)</ta>
            <ta e="T1457" id="Seg_5705" s="T1454">PKZ_19700818_09341-2bz.PKZ.146 (241)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T12" id="Seg_5706" s="T1">((…)) не могут найти, такой лес густой, пока сами придут. </ta>
            <ta e="T29" id="Seg_5707" s="T13">(Та-) Таки горы, что нельзя взглянуть выше их, сколь раз это ((…)) горы там. </ta>
            <ta e="T52" id="Seg_5708" s="T49">Ну, как… </ta>
            <ta e="T62" id="Seg_5709" s="T53">Sʼarbibaʔ, i dĭgəttə girgit nʼi măna šoləj, măn dĭʔnə. </ta>
            <ta e="T67" id="Seg_5710" s="T63">I koʔbsaŋ iʔgö ibiʔi. </ta>
            <ta e="T74" id="Seg_5711" s="T67">Kamen (m-) măn ibiem dak, nagobiʔi nʼiʔi. </ta>
            <ta e="T89" id="Seg_5712" s="T75">Măn mĭmbiem dĭbər, Pjankăftə tibinə mĭmbiem, dĭn (nʼibə) ibi, i (măna=) măn dĭbər maluʔpiam. </ta>
            <ta e="T96" id="Seg_5713" s="T91">Вот как ты говоришь, ((…)). </ta>
            <ta e="T119" id="Seg_5714" s="T105">Nu, iat abat šoləj nʼin, (koʔbdonə), dĭgəttə (dĭ=) dĭʔnə mănliaʔi: "Kanaʔ miʔ nʼinə tibinə". </ta>
            <ta e="T123" id="Seg_5715" s="T120">Abandə iandə surarləʔjə. </ta>
            <ta e="T129" id="Seg_5716" s="T123">Nada koʔbdom surarzittə, dĭgəttə šoləj koʔbdo. </ta>
            <ta e="T131" id="Seg_5717" s="T129">"Kalal tibinə?" </ta>
            <ta e="T135" id="Seg_5718" s="T132">Dĭ măndə: "Kalam". </ta>
            <ta e="T141" id="Seg_5719" s="T136">Nu, dĭgəttə ara detləʔi, ipek. </ta>
            <ta e="T148" id="Seg_5720" s="T142">Amnolaʔbəʔjə, bĭtleʔbəʔjə, dĭgəttə (šobi=) šolaʔbəʔjə inezi. </ta>
            <ta e="T155" id="Seg_5721" s="T149">Dĭm (il-) iluʔpiʔi i kumbiʔi maːʔndə. </ta>
            <ta e="T177" id="Seg_5722" s="T172">(I-) Iat i abat mămbiʔi. </ta>
            <ta e="T187" id="Seg_5723" s="T178">Dĭgəttə abiʔi пиво, ara iʔgö ibi, bar ĭmbi abiʔi. </ta>
            <ta e="T192" id="Seg_5724" s="T188">Nabəʔi pürbiʔi, uja iʔgö. </ta>
            <ta e="T201" id="Seg_5725" s="T193">Dĭgəttə (nulbi-) nuldəbiʔi stolbə, bar ambiʔi i kalla dʼürbiʔi. </ta>
            <ta e="T209" id="Seg_5726" s="T202">Koʔbsaŋ, (il) bar ara biʔpiʔi, sʼarbiʔi garmonʼnʼazi. </ta>
            <ta e="T211" id="Seg_5727" s="T210">Suʔmibiʔi. </ta>
            <ta e="T215" id="Seg_5728" s="T212">Dĭgəttə bar kalla dʼürbiʔi. </ta>
            <ta e="T223" id="Seg_5729" s="T217">Ну, это я про свадьбу говорила. </ta>
            <ta e="T233" id="Seg_5730" s="T229">Пиво делали сами они. </ta>
            <ta e="T251" id="Seg_5731" s="T247">Amnobiʔi jakše, i dʼabrolaʔpiʔi. </ta>
            <ta e="T256" id="Seg_5732" s="T252">Ara bĭtləʔi dak, dʼabrolaʔbəʔjə. </ta>
            <ta e="T263" id="Seg_5733" s="T256">(Eʔbdən=) Eʔbdəzaŋdə onʼiʔ (onʼiʔtə ibiʔi) i dʼabrolaʔpiʔi. </ta>
            <ta e="T279" id="Seg_5734" s="T265">Вот я помню (дву-), я тебе может рассказывала, двух (ста-), старик со старухой. </ta>
            <ta e="T287" id="Seg_5735" s="T280">Напились пьяны, так через пол был порог. </ta>
            <ta e="T300" id="Seg_5736" s="T288">Они друг дружку взяли за волосы, тянулись, тянулись и так и уснули. </ta>
            <ta e="T315" id="Seg_5737" s="T301">Утром встали, "Чего-то, говорит, у меня тут больно", и он говорит: "У меня больно". </ta>
            <ta e="T327" id="Seg_5738" s="T315">"Может, мы, говорит, дрались с тобой, не знаем, так напились пьяные", вот. </ta>
            <ta e="T343" id="Seg_5739" s="T328">Я их помню, фамилие у его было Тугуин, а ее звали Вера, и он Василий. </ta>
            <ta e="T346" id="Seg_5740" s="T344">Звали его. </ta>
            <ta e="T360" id="Seg_5741" s="T350">Камасинцы, я их хорошо помню, недалёко от нас жили. </ta>
            <ta e="T378" id="Seg_5742" s="T371">Šində sumna ibi, šində sejʔpü ibi esseŋ. </ta>
            <ta e="T383" id="Seg_5743" s="T379">Iʔgö deʔpiʔi. </ta>
            <ta e="T387" id="Seg_5744" s="T383">Dĭzeŋ ĭmbidə ej abiʔi. </ta>
            <ta e="T392" id="Seg_5745" s="T388">Tüj il esseŋ (barəʔbəʔjə). </ta>
            <ta e="T400" id="Seg_5746" s="T392">Bălʼnitsanə mĭmbiʔi i ĭmbidə abiʔi, ej deʔlieʔi esseŋ. </ta>
            <ta e="T410" id="Seg_5747" s="T403">Nu, esseŋdə dĭzeŋ ĭzembiʔi, külambiʔi, amga maːluʔpiʔi. </ta>
            <ta e="T438" id="Seg_5748" s="T433">Dʼorbiʔi esseŋ. </ta>
            <ta e="T444" id="Seg_5749" s="T440">I (izeb-) ĭzembiʔi. </ta>
            <ta e="T462" id="Seg_5750" s="T458">Iat uʔlia da iləj. </ta>
            <ta e="T468" id="Seg_5751" s="T463">Dĭgəttə mĭlie nüjö (imiʔ) dĭzeŋdə. </ta>
            <ta e="T472" id="Seg_5752" s="T469">(Amnəj) da embi. </ta>
            <ta e="T475" id="Seg_5753" s="T473">Dĭ kunolluʔpi. </ta>
            <ta e="T480" id="Seg_5754" s="T476">I bostə iʔbəbi kunolluʔpi. </ta>
            <ta e="T508" id="Seg_5755" s="T502">Nʼilgölaʔpiʔi (abam = abat=) abat iat. </ta>
            <ta e="T515" id="Seg_5756" s="T509">Măn üdʼüge ibiem, iam măndə: "Kanaʔ". </ta>
            <ta e="T518" id="Seg_5757" s="T516">Öʔləj gibər-nʼibudʼ. </ta>
            <ta e="T524" id="Seg_5758" s="T519">Măn nuliam da mʼegəlaʔbəm dĭn. </ta>
            <ta e="T530" id="Seg_5759" s="T525">A dĭ: "Măn tănan münörləm". </ta>
            <ta e="T537" id="Seg_5760" s="T531">Măn dĭgəttə nuʔmiluʔpiam i büžü šobiam. </ta>
            <ta e="T544" id="Seg_5761" s="T538">Dĭgəttə dĭ ĭmbidə măna ej münörleʔpi. </ta>
            <ta e="T552" id="Seg_5762" s="T546">Любила ((…)) тоже над матерью. </ta>
            <ta e="T562" id="Seg_5763" s="T561">Münörbiʔi. </ta>
            <ta e="T565" id="Seg_5764" s="T563">Tăŋ, šerəpsi. </ta>
            <ta e="T571" id="Seg_5765" s="T566">Măna iam ugandə tăŋ münörbiʔi. </ta>
            <ta e="T577" id="Seg_5766" s="T572">Măn tăŋ alombiam. </ta>
            <ta e="T591" id="Seg_5767" s="T589">Прутом. </ta>
            <ta e="T593" id="Seg_5768" s="T592">((…)). </ta>
            <ta e="T605" id="Seg_5769" s="T602">Нет, не умели. </ta>
            <ta e="T616" id="Seg_5770" s="T608">Я не знаю, почему у их не было. </ta>
            <ta e="T633" id="Seg_5771" s="T618">Вот это у одной сестре стояла, так она говорит: это они выдумали свой язык. </ta>
            <ta e="T640" id="Seg_5772" s="T634">Нету, говорит, у их никаких книг. </ta>
            <ta e="T643" id="Seg_5773" s="T641">Не знаю. </ta>
            <ta e="T655" id="Seg_5774" s="T647">Ну, я говорила… </ta>
            <ta e="T674" id="Seg_5775" s="T661">Ну, что (нету=) не умеют ни писать, ни читать. </ta>
            <ta e="T683" id="Seg_5776" s="T676">Только один у нас там, Александр Шайбин. </ta>
            <ta e="T688" id="Seg_5777" s="T684">Вот он там ходил. </ta>
            <ta e="T701" id="Seg_5778" s="T689">(Там=) Там, еслиф кому надо учить, пускай наймет себе, по-русски учил учитель. </ta>
            <ta e="T703" id="Seg_5779" s="T702">Так. </ta>
            <ta e="T727" id="Seg_5780" s="T722">Pʼaŋdəsʼtə ej moliam, a… </ta>
            <ta e="T736" id="Seg_5781" s="T728">A moliam, knižkam măndliam, ĭmbi dĭn ige pʼaŋdona. </ta>
            <ta e="T741" id="Seg_5782" s="T737">A pʼaŋdəsʼtə ej moliam. </ta>
            <ta e="T762" id="Seg_5783" s="T758">Они много не жили. </ta>
            <ta e="T766" id="Seg_5784" s="T763">Надо говорить по-своему. </ta>
            <ta e="T775" id="Seg_5785" s="T770">Dĭzeŋ iʔgö ej (amnobi=) amnolaʔpiʔi. </ta>
            <ta e="T780" id="Seg_5786" s="T776">Dĭzeŋ üge nʼiʔneŋ, nʼineŋ. </ta>
            <ta e="T782" id="Seg_5787" s="T780">Turazaŋdə nagobiʔi. </ta>
            <ta e="T784" id="Seg_5788" s="T783">Kănnambiʔi. </ta>
            <ta e="T790" id="Seg_5789" s="T785">I dĭgəttə dĭzeŋdə (dʼazirzittə) nagobi. </ta>
            <ta e="T793" id="Seg_5790" s="T791">Dĭzeŋ külambiʔi. </ta>
            <ta e="T797" id="Seg_5791" s="T794">(A-) Amga amnolaʔbəʔi. </ta>
            <ta e="T804" id="Seg_5792" s="T800">Miʔnʼibeʔ sĭre pe nagobi. </ta>
            <ta e="T809" id="Seg_5793" s="T805">Dĭzeŋ nuldəbiʔi teʔtə pa. </ta>
            <ta e="T813" id="Seg_5794" s="T810">I dĭn embiʔi. </ta>
            <ta e="T820" id="Seg_5795" s="T814">Dĭgəttə (dĭ=) dĭbər nʼuʔtə piʔi embiʔi. </ta>
            <ta e="T829" id="Seg_5796" s="T820">Dĭgəttə dĭ paʔi (jaʔ-) jaʔpiʔi, dĭ saməʔluʔpi, dĭzem pʼaŋdluʔpi. </ta>
            <ta e="T835" id="Seg_5797" s="T830">(Bostə n-) Bostə (nuldəzi) kutlaʔpiʔi. </ta>
            <ta e="T839" id="Seg_5798" s="T836">Самих себя убивали. </ta>
            <ta e="T856" id="Seg_5799" s="T840">Когда березы там не было (березы=) береза выросла, они сказали: "Русские люди придут, нам плохо будет". </ta>
            <ta e="T868" id="Seg_5800" s="T857">Делали (алачин) такие, туда камни накладали, и подрубали, и их задавляло. </ta>
            <ta e="T877" id="Seg_5801" s="T870">Вот они погребли сами себя, людей боялись. </ta>
            <ta e="T884" id="Seg_5802" s="T878">Это (рассказывали=) рассказывали тоже старые люди. </ta>
            <ta e="T901" id="Seg_5803" s="T892">Nu, dʼü (dĭl-) tĭlbiʔi, dĭgəttə pa (ambiʔi), pa jaʔpiʔi. </ta>
            <ta e="T911" id="Seg_5804" s="T902">Dĭgəttə šide abiʔi, i dĭbər dĭn tože baltuzi jaʔpiʔi. </ta>
            <ta e="T917" id="Seg_5805" s="T912">Dĭgəttə dĭm dĭbər embiʔi, kumbiʔi. </ta>
            <ta e="T920" id="Seg_5806" s="T918">Embiʔi dʼünə. </ta>
            <ta e="T924" id="Seg_5807" s="T921">I krospa abiʔi. </ta>
            <ta e="T935" id="Seg_5808" s="T925">I dĭgəttə maːʔndə (so-) šobiʔi, (mĭ) mĭnzerleʔbəʔjə, i dĭgəttə amnaʔbəʔjə. </ta>
            <ta e="T939" id="Seg_5809" s="T936">I bar (kulandə). </ta>
            <ta e="T944" id="Seg_5810" s="T942">((…)). </ta>
            <ta e="T953" id="Seg_5811" s="T946">По-русски. </ta>
            <ta e="T957" id="Seg_5812" s="T954">Пили же. </ta>
            <ta e="T976" id="Seg_5813" s="T959">(По-ру-) По-русски сказать, они отрежут от лесины такую чурку, расколят пополам и выдолбят туды. </ta>
            <ta e="T980" id="Seg_5814" s="T976">Не делали с досок. </ta>
            <ta e="T984" id="Seg_5815" s="T982">Клали туда. </ta>
            <ta e="T1001" id="Seg_5816" s="T999">Ставили, наверное. </ta>
            <ta e="T1011" id="Seg_5817" s="T1002">Ну, я не помню, это при мне ставили кресты. </ta>
            <ta e="T1019" id="Seg_5818" s="T1013">А их не помню, не знаю. </ta>
            <ta e="T1050" id="Seg_5819" s="T1020">(Вот=) Вот ты, вот где туто-ка, на бугре, как тут ямы, сюды переходишь, тут на углу было кладбище, и туда, там за (бугром). </ta>
            <ta e="T1073" id="Seg_5820" s="T1051">Давнишнее, а потом там, где ты на краю снимал, там еще стара стойба, там тоже у их кладбище, это я видала там. </ta>
            <ta e="T1087" id="Seg_5821" s="T1074">(Кресто-) Там крестов не было, уже только ямки. </ta>
            <ta e="T1108" id="Seg_5822" s="T1088">Называли стара стойба, как они с тайги выезжали, и там они стояли зимовали, а потом сюды вот, где наша деревня. </ta>
            <ta e="T1116" id="Seg_5823" s="T1109">Этот ключ нашли, он никогда не мерзнет. </ta>
            <ta e="T1122" id="Seg_5824" s="T1117">(Ле-) Зимой только пар идет. </ta>
            <ta e="T1126" id="Seg_5825" s="T1122">А (ни-) лёду нету-ка. </ta>
            <ta e="T1134" id="Seg_5826" s="T1128">Сейчас она холоднее, как и зимой. </ta>
            <ta e="T1138" id="Seg_5827" s="T1135">Зимой теплая. </ta>
            <ta e="T1153" id="Seg_5828" s="T1139">Вот мерзла тряпка, туды (пос-) спустишь в речку, она в речке растает, тряпка морожена. </ta>
            <ta e="T1158" id="Seg_5829" s="T1155">Так ((…)) мороз. </ta>
            <ta e="T1168" id="Seg_5830" s="T1160">И она была ((…)) (талисы), талая, (талицы). </ta>
            <ta e="T1193" id="Seg_5831" s="T1187">Ну, приходили, поминали, на стол собирали. </ta>
            <ta e="T1202" id="Seg_5832" s="T1194">(Нав-) Наварют и, этого, и киселю, и хлебного. </ta>
            <ta e="T1210" id="Seg_5833" s="T1202">Ну, они картофельного тады не знали, хлебный делали. </ta>
            <ta e="T1214" id="Seg_5834" s="T1211">Лапшу сами делали. </ta>
            <ta e="T1226" id="Seg_5835" s="T1215">Как теперича вот, лапша есть в магазинах, а они сами делали. </ta>
            <ta e="T1230" id="Seg_5836" s="T1227">Не знали этого. </ta>
            <ta e="T1276" id="Seg_5837" s="T1270">Nu, kamen külalləj kuza, dʼünə (tĭlleʔbəʔə). </ta>
            <ta e="T1284" id="Seg_5838" s="T1277">A (dĭn nʼuʔtə=) dux dĭn kalla dʼürləj nʼuʔtə. </ta>
            <ta e="T1288" id="Seg_5839" s="T1285">Măn dăre tĭmnem. </ta>
            <ta e="T1297" id="Seg_5840" s="T1289">Не знаю, šində kăde, a măn dăre tĭmnem. </ta>
            <ta e="T1304" id="Seg_5841" s="T1298">I iam (măndə=) dăre măndə măna. </ta>
            <ta e="T1309" id="Seg_5842" s="T1305">Душа dĭn kalaj kudajdə. </ta>
            <ta e="T1317" id="Seg_5843" s="T1312">Kuzam kudaj abi bostə dʼügən. </ta>
            <ta e="T1330" id="Seg_5844" s="T1317">I dĭʔnə bar ((…)) bostə duxtə (da-) mĭbi, i dĭ uʔbdəbi, kuza molambi. </ta>
            <ta e="T1339" id="Seg_5845" s="T1331">(Понят-) Понимаешь это, чего я тебе рассказала? </ta>
            <ta e="T1368" id="Seg_5846" s="T1348">(Это=) это я тебе не говорила, вот это вот, чего сейчас гоою. </ta>
            <ta e="T1375" id="Seg_5847" s="T1369">Это я с Писания говорю. </ta>
            <ta e="T1404" id="Seg_5848" s="T1377">(Когда=) Когда Господь очертил (свою=) свою и вдунул в человека на земле, по образу, по подобию своему очертил и вдунул в его духа, и он стал живой. </ta>
            <ta e="T1412" id="Seg_5849" s="T1406">Так (я это=) я это (передаю). </ta>
            <ta e="T1453" id="Seg_5850" s="T1451">No, kabarləj. </ta>
            <ta e="T1457" id="Seg_5851" s="T1454">Хватит, говорю. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T54" id="Seg_5852" s="T53">sʼar-bi-baʔ</ta>
            <ta e="T55" id="Seg_5853" s="T54">i</ta>
            <ta e="T56" id="Seg_5854" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_5855" s="T56">girgit</ta>
            <ta e="T58" id="Seg_5856" s="T57">nʼi</ta>
            <ta e="T59" id="Seg_5857" s="T58">măna</ta>
            <ta e="T60" id="Seg_5858" s="T59">šo-lə-j</ta>
            <ta e="T61" id="Seg_5859" s="T60">măn</ta>
            <ta e="T62" id="Seg_5860" s="T61">dĭʔ-nə</ta>
            <ta e="T64" id="Seg_5861" s="T63">i</ta>
            <ta e="T65" id="Seg_5862" s="T64">koʔb-saŋ</ta>
            <ta e="T66" id="Seg_5863" s="T65">iʔgö</ta>
            <ta e="T67" id="Seg_5864" s="T66">i-bi-ʔi</ta>
            <ta e="T68" id="Seg_5865" s="T67">kamen</ta>
            <ta e="T70" id="Seg_5866" s="T69">măn</ta>
            <ta e="T71" id="Seg_5867" s="T70">i-bie-m</ta>
            <ta e="T72" id="Seg_5868" s="T71">dak</ta>
            <ta e="T73" id="Seg_5869" s="T72">nago-bi-ʔi</ta>
            <ta e="T74" id="Seg_5870" s="T73">nʼi-ʔi</ta>
            <ta e="T76" id="Seg_5871" s="T75">măn</ta>
            <ta e="T77" id="Seg_5872" s="T76">mĭm-bie-m</ta>
            <ta e="T78" id="Seg_5873" s="T77">dĭbər</ta>
            <ta e="T79" id="Seg_5874" s="T78">Pjankăf-tə</ta>
            <ta e="T80" id="Seg_5875" s="T79">tibi-nə</ta>
            <ta e="T81" id="Seg_5876" s="T80">mĭm-bie-m</ta>
            <ta e="T82" id="Seg_5877" s="T81">dĭn</ta>
            <ta e="T83" id="Seg_5878" s="T82">nʼi-bə</ta>
            <ta e="T84" id="Seg_5879" s="T83">i-bi</ta>
            <ta e="T85" id="Seg_5880" s="T84">i</ta>
            <ta e="T86" id="Seg_5881" s="T85">măna</ta>
            <ta e="T87" id="Seg_5882" s="T86">măn</ta>
            <ta e="T88" id="Seg_5883" s="T87">dĭbər</ta>
            <ta e="T89" id="Seg_5884" s="T88">ma-luʔ-pia-m</ta>
            <ta e="T106" id="Seg_5885" s="T105">nu</ta>
            <ta e="T107" id="Seg_5886" s="T106">ia-t</ta>
            <ta e="T108" id="Seg_5887" s="T107">aba-t</ta>
            <ta e="T109" id="Seg_5888" s="T108">šo-lə-j</ta>
            <ta e="T110" id="Seg_5889" s="T109">nʼi-n</ta>
            <ta e="T111" id="Seg_5890" s="T110">koʔbdo-nə</ta>
            <ta e="T112" id="Seg_5891" s="T111">dĭgəttə</ta>
            <ta e="T113" id="Seg_5892" s="T112">dĭ</ta>
            <ta e="T114" id="Seg_5893" s="T113">dĭʔ-nə</ta>
            <ta e="T115" id="Seg_5894" s="T114">măn-lia-ʔi</ta>
            <ta e="T116" id="Seg_5895" s="T115">kan-a-ʔ</ta>
            <ta e="T117" id="Seg_5896" s="T116">miʔ</ta>
            <ta e="T118" id="Seg_5897" s="T117">nʼi-nə</ta>
            <ta e="T119" id="Seg_5898" s="T118">tibi-nə</ta>
            <ta e="T121" id="Seg_5899" s="T120">aba-ndə</ta>
            <ta e="T122" id="Seg_5900" s="T121">ia-ndə</ta>
            <ta e="T123" id="Seg_5901" s="T122">surar-lə-ʔjə</ta>
            <ta e="T124" id="Seg_5902" s="T123">nada</ta>
            <ta e="T125" id="Seg_5903" s="T124">koʔbdo-m</ta>
            <ta e="T126" id="Seg_5904" s="T125">surar-zittə</ta>
            <ta e="T127" id="Seg_5905" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_5906" s="T127">šo-lə-j</ta>
            <ta e="T129" id="Seg_5907" s="T128">koʔbdo</ta>
            <ta e="T130" id="Seg_5908" s="T129">ka-la-l</ta>
            <ta e="T131" id="Seg_5909" s="T130">tibi-nə</ta>
            <ta e="T133" id="Seg_5910" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_5911" s="T133">măn-də</ta>
            <ta e="T135" id="Seg_5912" s="T134">ka-la-m</ta>
            <ta e="T137" id="Seg_5913" s="T136">nu</ta>
            <ta e="T138" id="Seg_5914" s="T137">dĭgəttə</ta>
            <ta e="T139" id="Seg_5915" s="T138">ara</ta>
            <ta e="T140" id="Seg_5916" s="T139">det-lə-ʔi</ta>
            <ta e="T141" id="Seg_5917" s="T140">ipek</ta>
            <ta e="T143" id="Seg_5918" s="T142">amno-laʔbə-ʔjə</ta>
            <ta e="T144" id="Seg_5919" s="T143">bĭt-leʔbə-ʔjə</ta>
            <ta e="T145" id="Seg_5920" s="T144">dĭgəttə</ta>
            <ta e="T146" id="Seg_5921" s="T145">šo-bi</ta>
            <ta e="T147" id="Seg_5922" s="T146">šo-laʔbə-ʔjə</ta>
            <ta e="T148" id="Seg_5923" s="T147">ine-zi</ta>
            <ta e="T150" id="Seg_5924" s="T149">dĭ-m</ta>
            <ta e="T152" id="Seg_5925" s="T151">i-luʔ-pi-ʔi</ta>
            <ta e="T153" id="Seg_5926" s="T152">i</ta>
            <ta e="T154" id="Seg_5927" s="T153">kum-bi-ʔi</ta>
            <ta e="T155" id="Seg_5928" s="T154">maːʔ-ndə</ta>
            <ta e="T174" id="Seg_5929" s="T173">ia-t</ta>
            <ta e="T175" id="Seg_5930" s="T174">i</ta>
            <ta e="T176" id="Seg_5931" s="T175">aba-t</ta>
            <ta e="T177" id="Seg_5932" s="T176">măm-bi-ʔi</ta>
            <ta e="T179" id="Seg_5933" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_5934" s="T179">a-bi-ʔi</ta>
            <ta e="T182" id="Seg_5935" s="T181">ara</ta>
            <ta e="T183" id="Seg_5936" s="T182">iʔgö</ta>
            <ta e="T184" id="Seg_5937" s="T183">i-bi</ta>
            <ta e="T185" id="Seg_5938" s="T184">bar</ta>
            <ta e="T186" id="Seg_5939" s="T185">ĭmbi</ta>
            <ta e="T187" id="Seg_5940" s="T186">a-bi-ʔi</ta>
            <ta e="T189" id="Seg_5941" s="T188">nabə-ʔi</ta>
            <ta e="T190" id="Seg_5942" s="T189">pür-bi-ʔi</ta>
            <ta e="T191" id="Seg_5943" s="T190">uja</ta>
            <ta e="T192" id="Seg_5944" s="T191">iʔgö</ta>
            <ta e="T194" id="Seg_5945" s="T193">dĭgəttə</ta>
            <ta e="T195" id="Seg_5946" s="T194">nulbi</ta>
            <ta e="T196" id="Seg_5947" s="T195">nuldə-bi-ʔi</ta>
            <ta e="T197" id="Seg_5948" s="T196">stol-bə</ta>
            <ta e="T198" id="Seg_5949" s="T197">bar</ta>
            <ta e="T199" id="Seg_5950" s="T198">am-bi-ʔi</ta>
            <ta e="T200" id="Seg_5951" s="T199">i</ta>
            <ta e="T1531" id="Seg_5952" s="T200">kal-la</ta>
            <ta e="T201" id="Seg_5953" s="T1531">dʼür-bi-ʔi</ta>
            <ta e="T203" id="Seg_5954" s="T202">koʔb-saŋ</ta>
            <ta e="T204" id="Seg_5955" s="T203">il</ta>
            <ta e="T205" id="Seg_5956" s="T204">bar</ta>
            <ta e="T206" id="Seg_5957" s="T205">ara</ta>
            <ta e="T207" id="Seg_5958" s="T206">biʔ-pi-ʔi</ta>
            <ta e="T208" id="Seg_5959" s="T207">sʼar-bi-ʔi</ta>
            <ta e="T209" id="Seg_5960" s="T208">garmonʼnʼa-zi</ta>
            <ta e="T211" id="Seg_5961" s="T210">suʔmi-bi-ʔi</ta>
            <ta e="T213" id="Seg_5962" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_5963" s="T213">bar</ta>
            <ta e="T1532" id="Seg_5964" s="T214">kal-la</ta>
            <ta e="T215" id="Seg_5965" s="T1532">dʼür-bi-ʔi</ta>
            <ta e="T248" id="Seg_5966" s="T247">amno-bi-ʔi</ta>
            <ta e="T249" id="Seg_5967" s="T248">jakše</ta>
            <ta e="T250" id="Seg_5968" s="T249">i</ta>
            <ta e="T251" id="Seg_5969" s="T250">dʼabro-laʔpi-ʔi</ta>
            <ta e="T253" id="Seg_5970" s="T252">ara</ta>
            <ta e="T254" id="Seg_5971" s="T253">bĭt-lə-ʔi</ta>
            <ta e="T255" id="Seg_5972" s="T254">dak</ta>
            <ta e="T256" id="Seg_5973" s="T255">dʼabro-laʔbə-ʔjə</ta>
            <ta e="T257" id="Seg_5974" s="T256">eʔbdə-n</ta>
            <ta e="T258" id="Seg_5975" s="T257">eʔbdə-zaŋ-də</ta>
            <ta e="T259" id="Seg_5976" s="T258">onʼiʔ</ta>
            <ta e="T260" id="Seg_5977" s="T259">onʼiʔ-tə</ta>
            <ta e="T261" id="Seg_5978" s="T260">i-bi-ʔi</ta>
            <ta e="T262" id="Seg_5979" s="T261">i</ta>
            <ta e="T263" id="Seg_5980" s="T262">dʼabro-laʔpi-ʔi</ta>
            <ta e="T372" id="Seg_5981" s="T371">šində</ta>
            <ta e="T373" id="Seg_5982" s="T372">sumna</ta>
            <ta e="T374" id="Seg_5983" s="T373">i-bi</ta>
            <ta e="T375" id="Seg_5984" s="T374">šində</ta>
            <ta e="T376" id="Seg_5985" s="T375">sejʔpü</ta>
            <ta e="T377" id="Seg_5986" s="T376">i-bi</ta>
            <ta e="T378" id="Seg_5987" s="T377">es-seŋ</ta>
            <ta e="T381" id="Seg_5988" s="T379">iʔgö</ta>
            <ta e="T383" id="Seg_5989" s="T381">deʔ-pi-ʔi</ta>
            <ta e="T384" id="Seg_5990" s="T383">dĭ-zeŋ</ta>
            <ta e="T385" id="Seg_5991" s="T384">ĭmbi=də</ta>
            <ta e="T386" id="Seg_5992" s="T385">ej</ta>
            <ta e="T387" id="Seg_5993" s="T386">a-bi-ʔi</ta>
            <ta e="T389" id="Seg_5994" s="T388">tüj</ta>
            <ta e="T390" id="Seg_5995" s="T389">il</ta>
            <ta e="T391" id="Seg_5996" s="T390">es-seŋ</ta>
            <ta e="T392" id="Seg_5997" s="T391">barəʔ-bə-ʔjə</ta>
            <ta e="T393" id="Seg_5998" s="T392">bălʼnitsa-nə</ta>
            <ta e="T394" id="Seg_5999" s="T393">mĭm-bi-ʔi</ta>
            <ta e="T395" id="Seg_6000" s="T394">i</ta>
            <ta e="T396" id="Seg_6001" s="T395">ĭmbi=də</ta>
            <ta e="T397" id="Seg_6002" s="T396">a-bi-ʔi</ta>
            <ta e="T398" id="Seg_6003" s="T397">ej</ta>
            <ta e="T399" id="Seg_6004" s="T398">deʔ-lie-ʔi</ta>
            <ta e="T400" id="Seg_6005" s="T399">es-seŋ</ta>
            <ta e="T404" id="Seg_6006" s="T403">nu</ta>
            <ta e="T405" id="Seg_6007" s="T404">es-seŋ-də</ta>
            <ta e="T406" id="Seg_6008" s="T405">dĭ-zeŋ</ta>
            <ta e="T407" id="Seg_6009" s="T406">ĭzem-bi-ʔi</ta>
            <ta e="T408" id="Seg_6010" s="T407">kü-lam-bi-ʔi</ta>
            <ta e="T409" id="Seg_6011" s="T408">amga</ta>
            <ta e="T410" id="Seg_6012" s="T409">maː-luʔ-pi-ʔi</ta>
            <ta e="T437" id="Seg_6013" s="T433">dʼor-bi-ʔi</ta>
            <ta e="T438" id="Seg_6014" s="T437">es-seŋ</ta>
            <ta e="T441" id="Seg_6015" s="T440">i</ta>
            <ta e="T444" id="Seg_6016" s="T443">ĭzem-bi-ʔi</ta>
            <ta e="T459" id="Seg_6017" s="T458">ia-t</ta>
            <ta e="T460" id="Seg_6018" s="T459">uʔ-lia</ta>
            <ta e="T461" id="Seg_6019" s="T460">da</ta>
            <ta e="T462" id="Seg_6020" s="T461">i-lə-j</ta>
            <ta e="T464" id="Seg_6021" s="T463">dĭgəttə</ta>
            <ta e="T465" id="Seg_6022" s="T464">mĭ-lie</ta>
            <ta e="T466" id="Seg_6023" s="T465">nüjö</ta>
            <ta e="T468" id="Seg_6024" s="T467">dĭ-zeŋ-də</ta>
            <ta e="T470" id="Seg_6025" s="T469">am-nə-j</ta>
            <ta e="T471" id="Seg_6026" s="T470">da</ta>
            <ta e="T472" id="Seg_6027" s="T471">em-bi</ta>
            <ta e="T474" id="Seg_6028" s="T473">dĭ</ta>
            <ta e="T475" id="Seg_6029" s="T474">kunol-luʔ-pi</ta>
            <ta e="T477" id="Seg_6030" s="T476">i</ta>
            <ta e="T478" id="Seg_6031" s="T477">bos-tə</ta>
            <ta e="T479" id="Seg_6032" s="T478">iʔbə-bi</ta>
            <ta e="T480" id="Seg_6033" s="T479">kunol-luʔ-pi</ta>
            <ta e="T503" id="Seg_6034" s="T502">nʼilgö-laʔpi-ʔi</ta>
            <ta e="T504" id="Seg_6035" s="T503">aba-m</ta>
            <ta e="T506" id="Seg_6036" s="T505">aba-t</ta>
            <ta e="T507" id="Seg_6037" s="T506">aba-t</ta>
            <ta e="T508" id="Seg_6038" s="T507">ia-t</ta>
            <ta e="T510" id="Seg_6039" s="T509">măn</ta>
            <ta e="T511" id="Seg_6040" s="T510">üdʼüge</ta>
            <ta e="T512" id="Seg_6041" s="T511">i-bie-m</ta>
            <ta e="T513" id="Seg_6042" s="T512">ia-m</ta>
            <ta e="T514" id="Seg_6043" s="T513">măn-də</ta>
            <ta e="T515" id="Seg_6044" s="T514">kan-a-ʔ</ta>
            <ta e="T517" id="Seg_6045" s="T516">öʔ-lə-j</ta>
            <ta e="T518" id="Seg_6046" s="T517">gibər=nʼibudʼ</ta>
            <ta e="T520" id="Seg_6047" s="T519">măn</ta>
            <ta e="T521" id="Seg_6048" s="T520">nu-lia-m</ta>
            <ta e="T522" id="Seg_6049" s="T521">da</ta>
            <ta e="T523" id="Seg_6050" s="T522">mʼegə-laʔbə-m</ta>
            <ta e="T524" id="Seg_6051" s="T523">dĭn</ta>
            <ta e="T526" id="Seg_6052" s="T525">a</ta>
            <ta e="T527" id="Seg_6053" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_6054" s="T527">măn</ta>
            <ta e="T529" id="Seg_6055" s="T528">tănan</ta>
            <ta e="T530" id="Seg_6056" s="T529">münör-lə-m</ta>
            <ta e="T532" id="Seg_6057" s="T531">măn</ta>
            <ta e="T533" id="Seg_6058" s="T532">dĭgəttə</ta>
            <ta e="T534" id="Seg_6059" s="T533">nuʔmi-luʔ-pia-m</ta>
            <ta e="T535" id="Seg_6060" s="T534">i</ta>
            <ta e="T536" id="Seg_6061" s="T535">büžü</ta>
            <ta e="T537" id="Seg_6062" s="T536">šo-bia-m</ta>
            <ta e="T539" id="Seg_6063" s="T538">dĭgəttə</ta>
            <ta e="T540" id="Seg_6064" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_6065" s="T540">ĭmbi=də</ta>
            <ta e="T542" id="Seg_6066" s="T541">măna</ta>
            <ta e="T543" id="Seg_6067" s="T542">ej</ta>
            <ta e="T544" id="Seg_6068" s="T543">münör-leʔpi</ta>
            <ta e="T562" id="Seg_6069" s="T561">münör-bi-ʔi</ta>
            <ta e="T564" id="Seg_6070" s="T563">tăŋ</ta>
            <ta e="T565" id="Seg_6071" s="T564">šerəp-si</ta>
            <ta e="T567" id="Seg_6072" s="T566">măna</ta>
            <ta e="T568" id="Seg_6073" s="T567">ia-m</ta>
            <ta e="T569" id="Seg_6074" s="T568">ugandə</ta>
            <ta e="T570" id="Seg_6075" s="T569">tăŋ</ta>
            <ta e="T571" id="Seg_6076" s="T570">münör-bi-ʔi</ta>
            <ta e="T575" id="Seg_6077" s="T572">măn</ta>
            <ta e="T576" id="Seg_6078" s="T575">tăŋ</ta>
            <ta e="T577" id="Seg_6079" s="T576">alom-bia-m</ta>
            <ta e="T724" id="Seg_6080" s="T722">pʼaŋdə-sʼtə</ta>
            <ta e="T725" id="Seg_6081" s="T724">ej</ta>
            <ta e="T726" id="Seg_6082" s="T725">mo-lia-m</ta>
            <ta e="T727" id="Seg_6083" s="T726">a</ta>
            <ta e="T729" id="Seg_6084" s="T728">a</ta>
            <ta e="T730" id="Seg_6085" s="T729">mo-lia-m</ta>
            <ta e="T731" id="Seg_6086" s="T730">knižka-m</ta>
            <ta e="T732" id="Seg_6087" s="T731">mănd-lia-m</ta>
            <ta e="T733" id="Seg_6088" s="T732">ĭmbi</ta>
            <ta e="T734" id="Seg_6089" s="T733">dĭn</ta>
            <ta e="T735" id="Seg_6090" s="T734">i-ge</ta>
            <ta e="T736" id="Seg_6091" s="T735">pʼaŋd-o-na</ta>
            <ta e="T738" id="Seg_6092" s="T737">a</ta>
            <ta e="T739" id="Seg_6093" s="T738">pʼaŋdə-sʼtə</ta>
            <ta e="T740" id="Seg_6094" s="T739">ej</ta>
            <ta e="T741" id="Seg_6095" s="T740">mo-lia-m</ta>
            <ta e="T771" id="Seg_6096" s="T770">dĭ-zeŋ</ta>
            <ta e="T772" id="Seg_6097" s="T771">iʔgö</ta>
            <ta e="T773" id="Seg_6098" s="T772">ej</ta>
            <ta e="T774" id="Seg_6099" s="T773">amno-bi</ta>
            <ta e="T775" id="Seg_6100" s="T774">amno-laʔpi-ʔi</ta>
            <ta e="T777" id="Seg_6101" s="T776">dĭ-zeŋ</ta>
            <ta e="T778" id="Seg_6102" s="T777">üge</ta>
            <ta e="T779" id="Seg_6103" s="T778">nʼiʔneŋ</ta>
            <ta e="T780" id="Seg_6104" s="T779">nʼineŋ</ta>
            <ta e="T781" id="Seg_6105" s="T780">tura-zaŋ-də</ta>
            <ta e="T782" id="Seg_6106" s="T781">nago-bi-ʔi</ta>
            <ta e="T784" id="Seg_6107" s="T783">kănna-m-bi-ʔi</ta>
            <ta e="T786" id="Seg_6108" s="T785">i</ta>
            <ta e="T787" id="Seg_6109" s="T786">dĭgəttə</ta>
            <ta e="T788" id="Seg_6110" s="T787">dĭ-zeŋ-də</ta>
            <ta e="T789" id="Seg_6111" s="T788">dʼazir-zittə</ta>
            <ta e="T790" id="Seg_6112" s="T789">nago-bi</ta>
            <ta e="T792" id="Seg_6113" s="T791">dĭ-zeŋ</ta>
            <ta e="T793" id="Seg_6114" s="T792">kü-lam-bi-ʔi</ta>
            <ta e="T795" id="Seg_6115" s="T794">a</ta>
            <ta e="T796" id="Seg_6116" s="T795">amga</ta>
            <ta e="T797" id="Seg_6117" s="T796">amno-laʔbə-ʔi</ta>
            <ta e="T801" id="Seg_6118" s="T800">miʔnʼibeʔ</ta>
            <ta e="T802" id="Seg_6119" s="T801">sĭre</ta>
            <ta e="T803" id="Seg_6120" s="T802">pe</ta>
            <ta e="T804" id="Seg_6121" s="T803">nago-bi</ta>
            <ta e="T806" id="Seg_6122" s="T805">dĭ-zeŋ</ta>
            <ta e="T807" id="Seg_6123" s="T806">nuldə-bi-ʔi</ta>
            <ta e="T808" id="Seg_6124" s="T807">teʔtə</ta>
            <ta e="T809" id="Seg_6125" s="T808">pa</ta>
            <ta e="T811" id="Seg_6126" s="T810">i</ta>
            <ta e="T812" id="Seg_6127" s="T811">dĭn</ta>
            <ta e="T813" id="Seg_6128" s="T812">em-bi-ʔi</ta>
            <ta e="T815" id="Seg_6129" s="T814">dĭgəttə</ta>
            <ta e="T816" id="Seg_6130" s="T815">dĭ</ta>
            <ta e="T817" id="Seg_6131" s="T816">dĭbər</ta>
            <ta e="T818" id="Seg_6132" s="T817">nʼuʔtə</ta>
            <ta e="T819" id="Seg_6133" s="T818">pi-ʔi</ta>
            <ta e="T820" id="Seg_6134" s="T819">em-bi-ʔi</ta>
            <ta e="T821" id="Seg_6135" s="T820">dĭgəttə</ta>
            <ta e="T822" id="Seg_6136" s="T821">dĭ</ta>
            <ta e="T823" id="Seg_6137" s="T822">pa-ʔi</ta>
            <ta e="T825" id="Seg_6138" s="T824">jaʔ-pi-ʔi</ta>
            <ta e="T826" id="Seg_6139" s="T825">dĭ</ta>
            <ta e="T827" id="Seg_6140" s="T826">saməʔ-luʔpi</ta>
            <ta e="T828" id="Seg_6141" s="T827">dĭ-zem</ta>
            <ta e="T829" id="Seg_6142" s="T828">pʼaŋd-luʔpi</ta>
            <ta e="T831" id="Seg_6143" s="T830">bos-tə</ta>
            <ta e="T833" id="Seg_6144" s="T832">bos-tə</ta>
            <ta e="T834" id="Seg_6145" s="T833">nuldəzi</ta>
            <ta e="T835" id="Seg_6146" s="T834">kut-laʔpi-ʔi</ta>
            <ta e="T893" id="Seg_6147" s="T892">nu</ta>
            <ta e="T894" id="Seg_6148" s="T893">dʼü</ta>
            <ta e="T896" id="Seg_6149" s="T895">tĭl-bi-ʔi</ta>
            <ta e="T897" id="Seg_6150" s="T896">dĭgəttə</ta>
            <ta e="T898" id="Seg_6151" s="T897">pa</ta>
            <ta e="T899" id="Seg_6152" s="T898">am-bi-ʔi</ta>
            <ta e="T900" id="Seg_6153" s="T899">pa</ta>
            <ta e="T901" id="Seg_6154" s="T900">jaʔ-pi-ʔi</ta>
            <ta e="T903" id="Seg_6155" s="T902">dĭgəttə</ta>
            <ta e="T904" id="Seg_6156" s="T903">šide</ta>
            <ta e="T905" id="Seg_6157" s="T904">a-bi-ʔi</ta>
            <ta e="T906" id="Seg_6158" s="T905">i</ta>
            <ta e="T907" id="Seg_6159" s="T906">dĭbər</ta>
            <ta e="T908" id="Seg_6160" s="T907">dĭn</ta>
            <ta e="T909" id="Seg_6161" s="T908">tože</ta>
            <ta e="T910" id="Seg_6162" s="T909">baltu-zi</ta>
            <ta e="T911" id="Seg_6163" s="T910">jaʔ-pi-ʔi</ta>
            <ta e="T913" id="Seg_6164" s="T912">dĭgəttə</ta>
            <ta e="T914" id="Seg_6165" s="T913">dĭ-m</ta>
            <ta e="T915" id="Seg_6166" s="T914">dĭbər</ta>
            <ta e="T916" id="Seg_6167" s="T915">em-bi-ʔi</ta>
            <ta e="T917" id="Seg_6168" s="T916">kum-bi-ʔi</ta>
            <ta e="T919" id="Seg_6169" s="T918">em-bi-ʔi</ta>
            <ta e="T920" id="Seg_6170" s="T919">dʼü-nə</ta>
            <ta e="T922" id="Seg_6171" s="T921">i</ta>
            <ta e="T923" id="Seg_6172" s="T922">krospa</ta>
            <ta e="T924" id="Seg_6173" s="T923">a-bi-ʔi</ta>
            <ta e="T926" id="Seg_6174" s="T925">i</ta>
            <ta e="T927" id="Seg_6175" s="T926">dĭgəttə</ta>
            <ta e="T928" id="Seg_6176" s="T927">maːʔ-ndə</ta>
            <ta e="T930" id="Seg_6177" s="T929">šo-bi-ʔi</ta>
            <ta e="T931" id="Seg_6178" s="T930">mĭ</ta>
            <ta e="T932" id="Seg_6179" s="T931">mĭnzer-leʔbə-ʔjə</ta>
            <ta e="T933" id="Seg_6180" s="T932">i</ta>
            <ta e="T934" id="Seg_6181" s="T933">dĭgəttə</ta>
            <ta e="T935" id="Seg_6182" s="T934">am-naʔbə-ʔjə</ta>
            <ta e="T937" id="Seg_6183" s="T936">i</ta>
            <ta e="T938" id="Seg_6184" s="T937">bar</ta>
            <ta e="T939" id="Seg_6185" s="T938">kulandə</ta>
            <ta e="T1271" id="Seg_6186" s="T1270">nu</ta>
            <ta e="T1272" id="Seg_6187" s="T1271">kamen</ta>
            <ta e="T1273" id="Seg_6188" s="T1272">kü-lal-lə-j</ta>
            <ta e="T1274" id="Seg_6189" s="T1273">kuza</ta>
            <ta e="T1275" id="Seg_6190" s="T1274">dʼü-nə</ta>
            <ta e="T1276" id="Seg_6191" s="T1275">tĭl-leʔbə-ʔə</ta>
            <ta e="T1278" id="Seg_6192" s="T1277">a</ta>
            <ta e="T1279" id="Seg_6193" s="T1278">dĭn</ta>
            <ta e="T1280" id="Seg_6194" s="T1279">nʼuʔtə</ta>
            <ta e="T1281" id="Seg_6195" s="T1280">dux</ta>
            <ta e="T1282" id="Seg_6196" s="T1281">dĭn</ta>
            <ta e="T1533" id="Seg_6197" s="T1282">kal-la</ta>
            <ta e="T1283" id="Seg_6198" s="T1533">dʼür-lə-j</ta>
            <ta e="T1284" id="Seg_6199" s="T1283">nʼuʔtə</ta>
            <ta e="T1286" id="Seg_6200" s="T1285">măn</ta>
            <ta e="T1287" id="Seg_6201" s="T1286">dăre</ta>
            <ta e="T1288" id="Seg_6202" s="T1287">tĭmne-m</ta>
            <ta e="T1292" id="Seg_6203" s="T1291">šində</ta>
            <ta e="T1293" id="Seg_6204" s="T1292">kăde</ta>
            <ta e="T1294" id="Seg_6205" s="T1293">a</ta>
            <ta e="T1295" id="Seg_6206" s="T1294">măn</ta>
            <ta e="T1296" id="Seg_6207" s="T1295">dăre</ta>
            <ta e="T1297" id="Seg_6208" s="T1296">tĭmne-m</ta>
            <ta e="T1299" id="Seg_6209" s="T1298">i</ta>
            <ta e="T1300" id="Seg_6210" s="T1299">ia-m</ta>
            <ta e="T1301" id="Seg_6211" s="T1300">măn-də</ta>
            <ta e="T1302" id="Seg_6212" s="T1301">dăre</ta>
            <ta e="T1303" id="Seg_6213" s="T1302">măn-də</ta>
            <ta e="T1304" id="Seg_6214" s="T1303">măna</ta>
            <ta e="T1307" id="Seg_6215" s="T1306">dĭ-n</ta>
            <ta e="T1308" id="Seg_6216" s="T1307">ka-la-j</ta>
            <ta e="T1309" id="Seg_6217" s="T1308">kudaj-də</ta>
            <ta e="T1313" id="Seg_6218" s="T1312">kuza-m</ta>
            <ta e="T1314" id="Seg_6219" s="T1313">kudaj</ta>
            <ta e="T1315" id="Seg_6220" s="T1314">a-bi</ta>
            <ta e="T1316" id="Seg_6221" s="T1315">bos-tə</ta>
            <ta e="T1317" id="Seg_6222" s="T1316">dʼü-gən</ta>
            <ta e="T1318" id="Seg_6223" s="T1317">i</ta>
            <ta e="T1319" id="Seg_6224" s="T1318">dĭʔ-nə</ta>
            <ta e="T1320" id="Seg_6225" s="T1319">bar</ta>
            <ta e="T1322" id="Seg_6226" s="T1321">bos-tə</ta>
            <ta e="T1323" id="Seg_6227" s="T1322">dux-tə</ta>
            <ta e="T1325" id="Seg_6228" s="T1324">mĭ-bi</ta>
            <ta e="T1326" id="Seg_6229" s="T1325">i</ta>
            <ta e="T1327" id="Seg_6230" s="T1326">dĭ</ta>
            <ta e="T1328" id="Seg_6231" s="T1327">uʔbdə-bi</ta>
            <ta e="T1329" id="Seg_6232" s="T1328">kuza</ta>
            <ta e="T1330" id="Seg_6233" s="T1329">mo-lam-bi</ta>
            <ta e="T1452" id="Seg_6234" s="T1451">no</ta>
            <ta e="T1453" id="Seg_6235" s="T1452">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T54" id="Seg_6236" s="T53">sʼar-bi-bAʔ</ta>
            <ta e="T55" id="Seg_6237" s="T54">i</ta>
            <ta e="T56" id="Seg_6238" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_6239" s="T56">girgit</ta>
            <ta e="T58" id="Seg_6240" s="T57">nʼi</ta>
            <ta e="T59" id="Seg_6241" s="T58">măna</ta>
            <ta e="T60" id="Seg_6242" s="T59">šo-lV-j</ta>
            <ta e="T61" id="Seg_6243" s="T60">măn</ta>
            <ta e="T62" id="Seg_6244" s="T61">dĭ-Tə</ta>
            <ta e="T64" id="Seg_6245" s="T63">i</ta>
            <ta e="T65" id="Seg_6246" s="T64">koʔbdo-zAŋ</ta>
            <ta e="T66" id="Seg_6247" s="T65">iʔgö</ta>
            <ta e="T67" id="Seg_6248" s="T66">i-bi-jəʔ</ta>
            <ta e="T68" id="Seg_6249" s="T67">kamən</ta>
            <ta e="T70" id="Seg_6250" s="T69">măn</ta>
            <ta e="T71" id="Seg_6251" s="T70">i-bi-m</ta>
            <ta e="T72" id="Seg_6252" s="T71">tak</ta>
            <ta e="T73" id="Seg_6253" s="T72">naga-bi-jəʔ</ta>
            <ta e="T74" id="Seg_6254" s="T73">nʼi-jəʔ</ta>
            <ta e="T76" id="Seg_6255" s="T75">măn</ta>
            <ta e="T77" id="Seg_6256" s="T76">mĭn-bi-m</ta>
            <ta e="T78" id="Seg_6257" s="T77">dĭbər</ta>
            <ta e="T79" id="Seg_6258" s="T78">Pjankăf-Tə</ta>
            <ta e="T80" id="Seg_6259" s="T79">tibi-Tə</ta>
            <ta e="T81" id="Seg_6260" s="T80">mĭn-bi-m</ta>
            <ta e="T82" id="Seg_6261" s="T81">dĭn</ta>
            <ta e="T83" id="Seg_6262" s="T82">nʼi-m</ta>
            <ta e="T84" id="Seg_6263" s="T83">i-bi</ta>
            <ta e="T85" id="Seg_6264" s="T84">i</ta>
            <ta e="T86" id="Seg_6265" s="T85">măna</ta>
            <ta e="T87" id="Seg_6266" s="T86">măn</ta>
            <ta e="T88" id="Seg_6267" s="T87">dĭbər</ta>
            <ta e="T89" id="Seg_6268" s="T88">ma-luʔbdə-bi-m</ta>
            <ta e="T106" id="Seg_6269" s="T105">nu</ta>
            <ta e="T107" id="Seg_6270" s="T106">ija-t</ta>
            <ta e="T108" id="Seg_6271" s="T107">aba-t</ta>
            <ta e="T109" id="Seg_6272" s="T108">šo-lV-j</ta>
            <ta e="T110" id="Seg_6273" s="T109">nʼi-n</ta>
            <ta e="T111" id="Seg_6274" s="T110">koʔbdo-nə</ta>
            <ta e="T112" id="Seg_6275" s="T111">dĭgəttə</ta>
            <ta e="T113" id="Seg_6276" s="T112">dĭ</ta>
            <ta e="T114" id="Seg_6277" s="T113">dĭ-Tə</ta>
            <ta e="T115" id="Seg_6278" s="T114">măn-liA-jəʔ</ta>
            <ta e="T116" id="Seg_6279" s="T115">kan-ə-ʔ</ta>
            <ta e="T117" id="Seg_6280" s="T116">miʔ</ta>
            <ta e="T118" id="Seg_6281" s="T117">nʼi-Tə</ta>
            <ta e="T119" id="Seg_6282" s="T118">tibi-Tə</ta>
            <ta e="T121" id="Seg_6283" s="T120">aba-gəndə</ta>
            <ta e="T122" id="Seg_6284" s="T121">ija-gəndə</ta>
            <ta e="T123" id="Seg_6285" s="T122">surar-lV-jəʔ</ta>
            <ta e="T124" id="Seg_6286" s="T123">nadə</ta>
            <ta e="T125" id="Seg_6287" s="T124">koʔbdo-m</ta>
            <ta e="T126" id="Seg_6288" s="T125">surar-zittə</ta>
            <ta e="T127" id="Seg_6289" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_6290" s="T127">šo-lV-j</ta>
            <ta e="T129" id="Seg_6291" s="T128">koʔbdo</ta>
            <ta e="T130" id="Seg_6292" s="T129">kan-lV-l</ta>
            <ta e="T131" id="Seg_6293" s="T130">tibi-Tə</ta>
            <ta e="T133" id="Seg_6294" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_6295" s="T133">măn-ntə</ta>
            <ta e="T135" id="Seg_6296" s="T134">kan-lV-m</ta>
            <ta e="T137" id="Seg_6297" s="T136">nu</ta>
            <ta e="T138" id="Seg_6298" s="T137">dĭgəttə</ta>
            <ta e="T139" id="Seg_6299" s="T138">ara</ta>
            <ta e="T140" id="Seg_6300" s="T139">det-lV-jəʔ</ta>
            <ta e="T141" id="Seg_6301" s="T140">ipek</ta>
            <ta e="T143" id="Seg_6302" s="T142">amno-laʔbə-jəʔ</ta>
            <ta e="T144" id="Seg_6303" s="T143">bĭs-laʔbə-jəʔ</ta>
            <ta e="T145" id="Seg_6304" s="T144">dĭgəttə</ta>
            <ta e="T146" id="Seg_6305" s="T145">šo-bi</ta>
            <ta e="T147" id="Seg_6306" s="T146">šo-laʔbə-jəʔ</ta>
            <ta e="T148" id="Seg_6307" s="T147">ine-ziʔ</ta>
            <ta e="T150" id="Seg_6308" s="T149">dĭ-m</ta>
            <ta e="T152" id="Seg_6309" s="T151">i-luʔbdə-bi-jəʔ</ta>
            <ta e="T153" id="Seg_6310" s="T152">i</ta>
            <ta e="T154" id="Seg_6311" s="T153">kun-bi-jəʔ</ta>
            <ta e="T155" id="Seg_6312" s="T154">maʔ-gəndə</ta>
            <ta e="T174" id="Seg_6313" s="T173">ija-t</ta>
            <ta e="T175" id="Seg_6314" s="T174">i</ta>
            <ta e="T176" id="Seg_6315" s="T175">aba-t</ta>
            <ta e="T177" id="Seg_6316" s="T176">măn-bi-jəʔ</ta>
            <ta e="T179" id="Seg_6317" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_6318" s="T179">a-bi-jəʔ</ta>
            <ta e="T182" id="Seg_6319" s="T181">ara</ta>
            <ta e="T183" id="Seg_6320" s="T182">iʔgö</ta>
            <ta e="T184" id="Seg_6321" s="T183">i-bi</ta>
            <ta e="T185" id="Seg_6322" s="T184">bar</ta>
            <ta e="T186" id="Seg_6323" s="T185">ĭmbi</ta>
            <ta e="T187" id="Seg_6324" s="T186">a-bi-jəʔ</ta>
            <ta e="T189" id="Seg_6325" s="T188">nabə-jəʔ</ta>
            <ta e="T190" id="Seg_6326" s="T189">pür-bi-jəʔ</ta>
            <ta e="T191" id="Seg_6327" s="T190">uja</ta>
            <ta e="T192" id="Seg_6328" s="T191">iʔgö</ta>
            <ta e="T194" id="Seg_6329" s="T193">dĭgəttə</ta>
            <ta e="T196" id="Seg_6330" s="T195">nuldə-bi-jəʔ</ta>
            <ta e="T197" id="Seg_6331" s="T196">stol-bə</ta>
            <ta e="T198" id="Seg_6332" s="T197">bar</ta>
            <ta e="T199" id="Seg_6333" s="T198">am-bi-jəʔ</ta>
            <ta e="T200" id="Seg_6334" s="T199">i</ta>
            <ta e="T1531" id="Seg_6335" s="T200">kan-lAʔ</ta>
            <ta e="T201" id="Seg_6336" s="T1531">tʼür-bi-jəʔ</ta>
            <ta e="T203" id="Seg_6337" s="T202">koʔbdo-zAŋ</ta>
            <ta e="T204" id="Seg_6338" s="T203">il</ta>
            <ta e="T205" id="Seg_6339" s="T204">bar</ta>
            <ta e="T206" id="Seg_6340" s="T205">ara</ta>
            <ta e="T207" id="Seg_6341" s="T206">bĭs-bi-jəʔ</ta>
            <ta e="T208" id="Seg_6342" s="T207">sʼar-bi-jəʔ</ta>
            <ta e="T209" id="Seg_6343" s="T208">garmonʼ-ziʔ</ta>
            <ta e="T211" id="Seg_6344" s="T210">süʔmə-bi-jəʔ</ta>
            <ta e="T213" id="Seg_6345" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_6346" s="T213">bar</ta>
            <ta e="T1532" id="Seg_6347" s="T214">kan-lAʔ</ta>
            <ta e="T215" id="Seg_6348" s="T1532">tʼür-bi-jəʔ</ta>
            <ta e="T248" id="Seg_6349" s="T247">amno-bi-jəʔ</ta>
            <ta e="T249" id="Seg_6350" s="T248">jakšə</ta>
            <ta e="T250" id="Seg_6351" s="T249">i</ta>
            <ta e="T251" id="Seg_6352" s="T250">tʼabəro-laʔpi-jəʔ</ta>
            <ta e="T253" id="Seg_6353" s="T252">ara</ta>
            <ta e="T254" id="Seg_6354" s="T253">bĭs-lV-jəʔ</ta>
            <ta e="T255" id="Seg_6355" s="T254">tak</ta>
            <ta e="T256" id="Seg_6356" s="T255">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T257" id="Seg_6357" s="T256">eʔbdə-n</ta>
            <ta e="T258" id="Seg_6358" s="T257">eʔbdə-zAŋ-də</ta>
            <ta e="T259" id="Seg_6359" s="T258">onʼiʔ</ta>
            <ta e="T260" id="Seg_6360" s="T259">onʼiʔ-Tə</ta>
            <ta e="T261" id="Seg_6361" s="T260">i-bi-jəʔ</ta>
            <ta e="T262" id="Seg_6362" s="T261">i</ta>
            <ta e="T263" id="Seg_6363" s="T262">tʼabəro-laʔpi-jəʔ</ta>
            <ta e="T372" id="Seg_6364" s="T371">šində</ta>
            <ta e="T373" id="Seg_6365" s="T372">sumna</ta>
            <ta e="T374" id="Seg_6366" s="T373">i-bi</ta>
            <ta e="T375" id="Seg_6367" s="T374">šində</ta>
            <ta e="T376" id="Seg_6368" s="T375">sejʔpü</ta>
            <ta e="T377" id="Seg_6369" s="T376">i-bi</ta>
            <ta e="T378" id="Seg_6370" s="T377">ešši-zAŋ</ta>
            <ta e="T381" id="Seg_6371" s="T379">iʔgö</ta>
            <ta e="T383" id="Seg_6372" s="T381">det-bi-jəʔ</ta>
            <ta e="T384" id="Seg_6373" s="T383">dĭ-zAŋ</ta>
            <ta e="T385" id="Seg_6374" s="T384">ĭmbi=də</ta>
            <ta e="T386" id="Seg_6375" s="T385">ej</ta>
            <ta e="T387" id="Seg_6376" s="T386">a-bi-jəʔ</ta>
            <ta e="T389" id="Seg_6377" s="T388">tüj</ta>
            <ta e="T390" id="Seg_6378" s="T389">il</ta>
            <ta e="T391" id="Seg_6379" s="T390">ešši-zAŋ</ta>
            <ta e="T392" id="Seg_6380" s="T391">barəʔ-bə-jəʔ</ta>
            <ta e="T393" id="Seg_6381" s="T392">bălʼnʼitsa-Tə</ta>
            <ta e="T394" id="Seg_6382" s="T393">mĭn-bi-jəʔ</ta>
            <ta e="T395" id="Seg_6383" s="T394">i</ta>
            <ta e="T396" id="Seg_6384" s="T395">ĭmbi=də</ta>
            <ta e="T397" id="Seg_6385" s="T396">a-bi-jəʔ</ta>
            <ta e="T398" id="Seg_6386" s="T397">ej</ta>
            <ta e="T399" id="Seg_6387" s="T398">det-liA-jəʔ</ta>
            <ta e="T400" id="Seg_6388" s="T399">ešši-zAŋ</ta>
            <ta e="T404" id="Seg_6389" s="T403">nu</ta>
            <ta e="T405" id="Seg_6390" s="T404">ešši-zAŋ-Tə</ta>
            <ta e="T406" id="Seg_6391" s="T405">dĭ-zAŋ</ta>
            <ta e="T407" id="Seg_6392" s="T406">ĭzem-bi-jəʔ</ta>
            <ta e="T408" id="Seg_6393" s="T407">kü-laːm-bi-jəʔ</ta>
            <ta e="T409" id="Seg_6394" s="T408">amka</ta>
            <ta e="T410" id="Seg_6395" s="T409">ma-luʔbdə-bi-jəʔ</ta>
            <ta e="T437" id="Seg_6396" s="T433">tʼor-bi-jəʔ</ta>
            <ta e="T438" id="Seg_6397" s="T437">ešši-zAŋ</ta>
            <ta e="T441" id="Seg_6398" s="T440">i</ta>
            <ta e="T444" id="Seg_6399" s="T443">ĭzem-bi-jəʔ</ta>
            <ta e="T459" id="Seg_6400" s="T458">ija-t</ta>
            <ta e="T460" id="Seg_6401" s="T459">uʔbdə-liA</ta>
            <ta e="T461" id="Seg_6402" s="T460">da</ta>
            <ta e="T462" id="Seg_6403" s="T461">i-lV-j</ta>
            <ta e="T464" id="Seg_6404" s="T463">dĭgəttə</ta>
            <ta e="T465" id="Seg_6405" s="T464">mĭ-liA</ta>
            <ta e="T466" id="Seg_6406" s="T465">nüjü</ta>
            <ta e="T468" id="Seg_6407" s="T467">dĭ-zAŋ-Tə</ta>
            <ta e="T470" id="Seg_6408" s="T469">am-lV-j</ta>
            <ta e="T471" id="Seg_6409" s="T470">da</ta>
            <ta e="T472" id="Seg_6410" s="T471">hen-bi</ta>
            <ta e="T474" id="Seg_6411" s="T473">dĭ</ta>
            <ta e="T475" id="Seg_6412" s="T474">kunol-luʔbdə-bi</ta>
            <ta e="T477" id="Seg_6413" s="T476">i</ta>
            <ta e="T478" id="Seg_6414" s="T477">bos-də</ta>
            <ta e="T479" id="Seg_6415" s="T478">iʔbə-bi</ta>
            <ta e="T480" id="Seg_6416" s="T479">kunol-luʔbdə-bi</ta>
            <ta e="T503" id="Seg_6417" s="T502">nʼilgö-laʔpi-jəʔ</ta>
            <ta e="T504" id="Seg_6418" s="T503">aba-m</ta>
            <ta e="T506" id="Seg_6419" s="T505">aba-t</ta>
            <ta e="T507" id="Seg_6420" s="T506">aba-t</ta>
            <ta e="T508" id="Seg_6421" s="T507">ija-t</ta>
            <ta e="T510" id="Seg_6422" s="T509">măn</ta>
            <ta e="T511" id="Seg_6423" s="T510">üdʼüge</ta>
            <ta e="T512" id="Seg_6424" s="T511">i-bi-m</ta>
            <ta e="T513" id="Seg_6425" s="T512">ija-m</ta>
            <ta e="T514" id="Seg_6426" s="T513">măn-ntə</ta>
            <ta e="T515" id="Seg_6427" s="T514">kan-ə-ʔ</ta>
            <ta e="T517" id="Seg_6428" s="T516">öʔ-lV-j</ta>
            <ta e="T518" id="Seg_6429" s="T517">gibər=nʼibudʼ</ta>
            <ta e="T520" id="Seg_6430" s="T519">măn</ta>
            <ta e="T521" id="Seg_6431" s="T520">nu-liA-m</ta>
            <ta e="T522" id="Seg_6432" s="T521">da</ta>
            <ta e="T523" id="Seg_6433" s="T522">mʼegə-laʔbə-m</ta>
            <ta e="T524" id="Seg_6434" s="T523">dĭn</ta>
            <ta e="T526" id="Seg_6435" s="T525">a</ta>
            <ta e="T527" id="Seg_6436" s="T526">dĭ</ta>
            <ta e="T528" id="Seg_6437" s="T527">măn</ta>
            <ta e="T529" id="Seg_6438" s="T528">tănan</ta>
            <ta e="T530" id="Seg_6439" s="T529">münör-lV-m</ta>
            <ta e="T532" id="Seg_6440" s="T531">măn</ta>
            <ta e="T533" id="Seg_6441" s="T532">dĭgəttə</ta>
            <ta e="T534" id="Seg_6442" s="T533">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T535" id="Seg_6443" s="T534">i</ta>
            <ta e="T536" id="Seg_6444" s="T535">büžü</ta>
            <ta e="T537" id="Seg_6445" s="T536">šo-bi-m</ta>
            <ta e="T539" id="Seg_6446" s="T538">dĭgəttə</ta>
            <ta e="T540" id="Seg_6447" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_6448" s="T540">ĭmbi=də</ta>
            <ta e="T542" id="Seg_6449" s="T541">măna</ta>
            <ta e="T543" id="Seg_6450" s="T542">ej</ta>
            <ta e="T544" id="Seg_6451" s="T543">münör-laʔpi</ta>
            <ta e="T562" id="Seg_6452" s="T561">münör-bi-jəʔ</ta>
            <ta e="T564" id="Seg_6453" s="T563">tăŋ</ta>
            <ta e="T565" id="Seg_6454" s="T564">šerəp-ziʔ</ta>
            <ta e="T567" id="Seg_6455" s="T566">măna</ta>
            <ta e="T568" id="Seg_6456" s="T567">ija-m</ta>
            <ta e="T569" id="Seg_6457" s="T568">ugaːndə</ta>
            <ta e="T570" id="Seg_6458" s="T569">tăŋ</ta>
            <ta e="T571" id="Seg_6459" s="T570">münör-bi-jəʔ</ta>
            <ta e="T575" id="Seg_6460" s="T572">măn</ta>
            <ta e="T576" id="Seg_6461" s="T575">tăŋ</ta>
            <ta e="T577" id="Seg_6462" s="T576">alom-bi-m</ta>
            <ta e="T724" id="Seg_6463" s="T722">pʼaŋdə-zittə</ta>
            <ta e="T725" id="Seg_6464" s="T724">ej</ta>
            <ta e="T726" id="Seg_6465" s="T725">mo-liA-m</ta>
            <ta e="T727" id="Seg_6466" s="T726">a</ta>
            <ta e="T729" id="Seg_6467" s="T728">a</ta>
            <ta e="T730" id="Seg_6468" s="T729">mo-liA-m</ta>
            <ta e="T731" id="Seg_6469" s="T730">knižka-m</ta>
            <ta e="T732" id="Seg_6470" s="T731">măndo-liA-m</ta>
            <ta e="T733" id="Seg_6471" s="T732">ĭmbi</ta>
            <ta e="T734" id="Seg_6472" s="T733">dĭn</ta>
            <ta e="T735" id="Seg_6473" s="T734">i-gA</ta>
            <ta e="T736" id="Seg_6474" s="T735">pʼaŋdə-o-NTA</ta>
            <ta e="T738" id="Seg_6475" s="T737">a</ta>
            <ta e="T739" id="Seg_6476" s="T738">pʼaŋdə-zittə</ta>
            <ta e="T740" id="Seg_6477" s="T739">ej</ta>
            <ta e="T741" id="Seg_6478" s="T740">mo-liA-m</ta>
            <ta e="T771" id="Seg_6479" s="T770">dĭ-zAŋ</ta>
            <ta e="T772" id="Seg_6480" s="T771">iʔgö</ta>
            <ta e="T773" id="Seg_6481" s="T772">ej</ta>
            <ta e="T774" id="Seg_6482" s="T773">amno-bi</ta>
            <ta e="T775" id="Seg_6483" s="T774">amno-laʔpi-jəʔ</ta>
            <ta e="T777" id="Seg_6484" s="T776">dĭ-zAŋ</ta>
            <ta e="T778" id="Seg_6485" s="T777">üge</ta>
            <ta e="T779" id="Seg_6486" s="T778">nʼiʔnen</ta>
            <ta e="T780" id="Seg_6487" s="T779">nʼiʔnen</ta>
            <ta e="T781" id="Seg_6488" s="T780">tura-zAŋ-Tə</ta>
            <ta e="T782" id="Seg_6489" s="T781">naga-bi-jəʔ</ta>
            <ta e="T784" id="Seg_6490" s="T783">kănna-m-bi-jəʔ</ta>
            <ta e="T786" id="Seg_6491" s="T785">i</ta>
            <ta e="T787" id="Seg_6492" s="T786">dĭgəttə</ta>
            <ta e="T788" id="Seg_6493" s="T787">dĭ-zAŋ-Tə</ta>
            <ta e="T789" id="Seg_6494" s="T788">tʼezer-zittə</ta>
            <ta e="T790" id="Seg_6495" s="T789">naga-bi</ta>
            <ta e="T792" id="Seg_6496" s="T791">dĭ-zAŋ</ta>
            <ta e="T793" id="Seg_6497" s="T792">kü-laːm-bi-jəʔ</ta>
            <ta e="T796" id="Seg_6498" s="T795">amka</ta>
            <ta e="T797" id="Seg_6499" s="T796">amno-laʔbə-jəʔ</ta>
            <ta e="T801" id="Seg_6500" s="T800">miʔnʼibeʔ</ta>
            <ta e="T802" id="Seg_6501" s="T801">sĭri</ta>
            <ta e="T803" id="Seg_6502" s="T802">pa</ta>
            <ta e="T804" id="Seg_6503" s="T803">naga-bi</ta>
            <ta e="T806" id="Seg_6504" s="T805">dĭ-zAŋ</ta>
            <ta e="T807" id="Seg_6505" s="T806">nuldə-bi-jəʔ</ta>
            <ta e="T808" id="Seg_6506" s="T807">teʔdə</ta>
            <ta e="T809" id="Seg_6507" s="T808">pa</ta>
            <ta e="T811" id="Seg_6508" s="T810">i</ta>
            <ta e="T812" id="Seg_6509" s="T811">dĭn</ta>
            <ta e="T813" id="Seg_6510" s="T812">hen-bi-jəʔ</ta>
            <ta e="T815" id="Seg_6511" s="T814">dĭgəttə</ta>
            <ta e="T816" id="Seg_6512" s="T815">dĭ</ta>
            <ta e="T817" id="Seg_6513" s="T816">dĭbər</ta>
            <ta e="T818" id="Seg_6514" s="T817">nʼuʔdə</ta>
            <ta e="T819" id="Seg_6515" s="T818">pi-jəʔ</ta>
            <ta e="T820" id="Seg_6516" s="T819">hen-bi-jəʔ</ta>
            <ta e="T821" id="Seg_6517" s="T820">dĭgəttə</ta>
            <ta e="T822" id="Seg_6518" s="T821">dĭ</ta>
            <ta e="T823" id="Seg_6519" s="T822">pa-jəʔ</ta>
            <ta e="T825" id="Seg_6520" s="T824">hʼaʔ-bi-jəʔ</ta>
            <ta e="T826" id="Seg_6521" s="T825">dĭ</ta>
            <ta e="T827" id="Seg_6522" s="T826">saʔmə-luʔbdə</ta>
            <ta e="T828" id="Seg_6523" s="T827">dĭ-zem</ta>
            <ta e="T829" id="Seg_6524" s="T828">pʼaŋdə-luʔbdə</ta>
            <ta e="T831" id="Seg_6525" s="T830">bos-də</ta>
            <ta e="T833" id="Seg_6526" s="T832">bos-də</ta>
            <ta e="T835" id="Seg_6527" s="T834">kut-laʔpi-jəʔ</ta>
            <ta e="T893" id="Seg_6528" s="T892">nu</ta>
            <ta e="T894" id="Seg_6529" s="T893">tʼo</ta>
            <ta e="T896" id="Seg_6530" s="T895">tĭl-bi-jəʔ</ta>
            <ta e="T897" id="Seg_6531" s="T896">dĭgəttə</ta>
            <ta e="T898" id="Seg_6532" s="T897">pa</ta>
            <ta e="T899" id="Seg_6533" s="T898">am-bi-jəʔ</ta>
            <ta e="T900" id="Seg_6534" s="T899">pa</ta>
            <ta e="T901" id="Seg_6535" s="T900">hʼaʔ-bi-jəʔ</ta>
            <ta e="T903" id="Seg_6536" s="T902">dĭgəttə</ta>
            <ta e="T904" id="Seg_6537" s="T903">šide</ta>
            <ta e="T905" id="Seg_6538" s="T904">a-bi-jəʔ</ta>
            <ta e="T906" id="Seg_6539" s="T905">i</ta>
            <ta e="T907" id="Seg_6540" s="T906">dĭbər</ta>
            <ta e="T908" id="Seg_6541" s="T907">dĭn</ta>
            <ta e="T909" id="Seg_6542" s="T908">tože</ta>
            <ta e="T910" id="Seg_6543" s="T909">baltu-ziʔ</ta>
            <ta e="T911" id="Seg_6544" s="T910">hʼaʔ-bi-jəʔ</ta>
            <ta e="T913" id="Seg_6545" s="T912">dĭgəttə</ta>
            <ta e="T914" id="Seg_6546" s="T913">dĭ-m</ta>
            <ta e="T915" id="Seg_6547" s="T914">dĭbər</ta>
            <ta e="T916" id="Seg_6548" s="T915">hen-bi-jəʔ</ta>
            <ta e="T917" id="Seg_6549" s="T916">kun-bi-jəʔ</ta>
            <ta e="T919" id="Seg_6550" s="T918">hen-bi-jəʔ</ta>
            <ta e="T920" id="Seg_6551" s="T919">tʼo-Tə</ta>
            <ta e="T922" id="Seg_6552" s="T921">i</ta>
            <ta e="T923" id="Seg_6553" s="T922">krospa</ta>
            <ta e="T924" id="Seg_6554" s="T923">a-bi-jəʔ</ta>
            <ta e="T926" id="Seg_6555" s="T925">i</ta>
            <ta e="T927" id="Seg_6556" s="T926">dĭgəttə</ta>
            <ta e="T928" id="Seg_6557" s="T927">maʔ-gəndə</ta>
            <ta e="T930" id="Seg_6558" s="T929">šo-bi-jəʔ</ta>
            <ta e="T932" id="Seg_6559" s="T931">mĭnzər-laʔbə-jəʔ</ta>
            <ta e="T933" id="Seg_6560" s="T932">i</ta>
            <ta e="T934" id="Seg_6561" s="T933">dĭgəttə</ta>
            <ta e="T935" id="Seg_6562" s="T934">am-laʔbə-jəʔ</ta>
            <ta e="T937" id="Seg_6563" s="T936">i</ta>
            <ta e="T938" id="Seg_6564" s="T937">bar</ta>
            <ta e="T1271" id="Seg_6565" s="T1270">nu</ta>
            <ta e="T1272" id="Seg_6566" s="T1271">kamən</ta>
            <ta e="T1273" id="Seg_6567" s="T1272">kü-laːm-lV-j</ta>
            <ta e="T1274" id="Seg_6568" s="T1273">kuza</ta>
            <ta e="T1275" id="Seg_6569" s="T1274">tʼo-Tə</ta>
            <ta e="T1276" id="Seg_6570" s="T1275">tĭl-laʔbə-jəʔ</ta>
            <ta e="T1278" id="Seg_6571" s="T1277">a</ta>
            <ta e="T1279" id="Seg_6572" s="T1278">dĭn</ta>
            <ta e="T1280" id="Seg_6573" s="T1279">nʼuʔdə</ta>
            <ta e="T1281" id="Seg_6574" s="T1280">dux</ta>
            <ta e="T1282" id="Seg_6575" s="T1281">dĭn</ta>
            <ta e="T1533" id="Seg_6576" s="T1282">kan-lAʔ</ta>
            <ta e="T1283" id="Seg_6577" s="T1533">tʼür-lV-j</ta>
            <ta e="T1284" id="Seg_6578" s="T1283">nʼuʔdə</ta>
            <ta e="T1286" id="Seg_6579" s="T1285">măn</ta>
            <ta e="T1287" id="Seg_6580" s="T1286">dărəʔ</ta>
            <ta e="T1288" id="Seg_6581" s="T1287">tĭmne-m</ta>
            <ta e="T1292" id="Seg_6582" s="T1291">šində</ta>
            <ta e="T1293" id="Seg_6583" s="T1292">kădaʔ</ta>
            <ta e="T1294" id="Seg_6584" s="T1293">a</ta>
            <ta e="T1295" id="Seg_6585" s="T1294">măn</ta>
            <ta e="T1296" id="Seg_6586" s="T1295">dărəʔ</ta>
            <ta e="T1297" id="Seg_6587" s="T1296">tĭmne-m</ta>
            <ta e="T1299" id="Seg_6588" s="T1298">i</ta>
            <ta e="T1300" id="Seg_6589" s="T1299">ija-m</ta>
            <ta e="T1301" id="Seg_6590" s="T1300">măn-ntə</ta>
            <ta e="T1302" id="Seg_6591" s="T1301">dărəʔ</ta>
            <ta e="T1303" id="Seg_6592" s="T1302">măn-ntə</ta>
            <ta e="T1304" id="Seg_6593" s="T1303">măna</ta>
            <ta e="T1307" id="Seg_6594" s="T1306">dĭ-n</ta>
            <ta e="T1308" id="Seg_6595" s="T1307">kan-lV-j</ta>
            <ta e="T1309" id="Seg_6596" s="T1308">kudaj-Tə</ta>
            <ta e="T1313" id="Seg_6597" s="T1312">kuza-m</ta>
            <ta e="T1314" id="Seg_6598" s="T1313">kudaj</ta>
            <ta e="T1315" id="Seg_6599" s="T1314">a-bi</ta>
            <ta e="T1316" id="Seg_6600" s="T1315">bos-də</ta>
            <ta e="T1317" id="Seg_6601" s="T1316">tʼo-Kən</ta>
            <ta e="T1318" id="Seg_6602" s="T1317">i</ta>
            <ta e="T1319" id="Seg_6603" s="T1318">dĭ-Tə</ta>
            <ta e="T1320" id="Seg_6604" s="T1319">bar</ta>
            <ta e="T1322" id="Seg_6605" s="T1321">bos-də</ta>
            <ta e="T1323" id="Seg_6606" s="T1322">dux-Tə</ta>
            <ta e="T1325" id="Seg_6607" s="T1324">mĭ-bi</ta>
            <ta e="T1326" id="Seg_6608" s="T1325">i</ta>
            <ta e="T1327" id="Seg_6609" s="T1326">dĭ</ta>
            <ta e="T1328" id="Seg_6610" s="T1327">uʔbdə-bi</ta>
            <ta e="T1329" id="Seg_6611" s="T1328">kuza</ta>
            <ta e="T1330" id="Seg_6612" s="T1329">mo-laːm-bi</ta>
            <ta e="T1452" id="Seg_6613" s="T1451">no</ta>
            <ta e="T1453" id="Seg_6614" s="T1452">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T54" id="Seg_6615" s="T53">play-PST-1PL</ta>
            <ta e="T55" id="Seg_6616" s="T54">and</ta>
            <ta e="T56" id="Seg_6617" s="T55">then</ta>
            <ta e="T57" id="Seg_6618" s="T56">what.kind</ta>
            <ta e="T58" id="Seg_6619" s="T57">boy.[NOM.SG]</ta>
            <ta e="T59" id="Seg_6620" s="T58">I.LAT</ta>
            <ta e="T60" id="Seg_6621" s="T59">come-FUT-3SG</ta>
            <ta e="T61" id="Seg_6622" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_6623" s="T61">this-LAT</ta>
            <ta e="T64" id="Seg_6624" s="T63">and</ta>
            <ta e="T65" id="Seg_6625" s="T64">daughter-PL</ta>
            <ta e="T66" id="Seg_6626" s="T65">many</ta>
            <ta e="T67" id="Seg_6627" s="T66">be-PST-3PL</ta>
            <ta e="T68" id="Seg_6628" s="T67">when</ta>
            <ta e="T70" id="Seg_6629" s="T69">I.NOM</ta>
            <ta e="T71" id="Seg_6630" s="T70">be-PST-1SG</ta>
            <ta e="T72" id="Seg_6631" s="T71">so</ta>
            <ta e="T73" id="Seg_6632" s="T72">NEG.EX-PST-3PL</ta>
            <ta e="T74" id="Seg_6633" s="T73">son-PL</ta>
            <ta e="T76" id="Seg_6634" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_6635" s="T76">go-PST-1SG</ta>
            <ta e="T78" id="Seg_6636" s="T77">there</ta>
            <ta e="T79" id="Seg_6637" s="T78">Pyankovo-LAT</ta>
            <ta e="T80" id="Seg_6638" s="T79">man-LAT</ta>
            <ta e="T81" id="Seg_6639" s="T80">go-PST-1SG</ta>
            <ta e="T82" id="Seg_6640" s="T81">there</ta>
            <ta e="T83" id="Seg_6641" s="T82">son-NOM/GEN/ACC.1SG</ta>
            <ta e="T84" id="Seg_6642" s="T83">be-PST.[3SG]</ta>
            <ta e="T85" id="Seg_6643" s="T84">and</ta>
            <ta e="T86" id="Seg_6644" s="T85">I.LAT</ta>
            <ta e="T87" id="Seg_6645" s="T86">I.NOM</ta>
            <ta e="T88" id="Seg_6646" s="T87">there</ta>
            <ta e="T89" id="Seg_6647" s="T88">remain-MOM-PST-1SG</ta>
            <ta e="T106" id="Seg_6648" s="T105">well</ta>
            <ta e="T107" id="Seg_6649" s="T106">mother-NOM/GEN.3SG</ta>
            <ta e="T108" id="Seg_6650" s="T107">father-NOM/GEN.3SG</ta>
            <ta e="T109" id="Seg_6651" s="T108">come-FUT-3SG</ta>
            <ta e="T110" id="Seg_6652" s="T109">boy-GEN</ta>
            <ta e="T111" id="Seg_6653" s="T110">daughter-GEN.1SG</ta>
            <ta e="T112" id="Seg_6654" s="T111">then</ta>
            <ta e="T113" id="Seg_6655" s="T112">this.[NOM.SG]</ta>
            <ta e="T114" id="Seg_6656" s="T113">this-LAT</ta>
            <ta e="T115" id="Seg_6657" s="T114">say-PRS-3PL</ta>
            <ta e="T116" id="Seg_6658" s="T115">go-EP-IMP.2SG</ta>
            <ta e="T117" id="Seg_6659" s="T116">we.GEN</ta>
            <ta e="T118" id="Seg_6660" s="T117">boy-LAT</ta>
            <ta e="T119" id="Seg_6661" s="T118">man-LAT</ta>
            <ta e="T121" id="Seg_6662" s="T120">father-LAT/LOC.3SG</ta>
            <ta e="T122" id="Seg_6663" s="T121">mother-LAT/LOC.3SG</ta>
            <ta e="T123" id="Seg_6664" s="T122">ask-FUT-3PL</ta>
            <ta e="T124" id="Seg_6665" s="T123">one.should</ta>
            <ta e="T125" id="Seg_6666" s="T124">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T126" id="Seg_6667" s="T125">ask-INF.LAT</ta>
            <ta e="T127" id="Seg_6668" s="T126">then</ta>
            <ta e="T128" id="Seg_6669" s="T127">come-FUT-3SG</ta>
            <ta e="T129" id="Seg_6670" s="T128">girl.[NOM.SG]</ta>
            <ta e="T130" id="Seg_6671" s="T129">go-FUT-2SG</ta>
            <ta e="T131" id="Seg_6672" s="T130">man-LAT</ta>
            <ta e="T133" id="Seg_6673" s="T132">this.[NOM.SG]</ta>
            <ta e="T134" id="Seg_6674" s="T133">say-IPFVZ.[3SG]</ta>
            <ta e="T135" id="Seg_6675" s="T134">go-FUT-1SG</ta>
            <ta e="T137" id="Seg_6676" s="T136">well</ta>
            <ta e="T138" id="Seg_6677" s="T137">then</ta>
            <ta e="T139" id="Seg_6678" s="T138">vodka.[NOM.SG]</ta>
            <ta e="T140" id="Seg_6679" s="T139">bring-FUT-3PL</ta>
            <ta e="T141" id="Seg_6680" s="T140">bread.[NOM.SG]</ta>
            <ta e="T143" id="Seg_6681" s="T142">sit-DUR-3PL</ta>
            <ta e="T144" id="Seg_6682" s="T143">drink-DUR-3PL</ta>
            <ta e="T145" id="Seg_6683" s="T144">then</ta>
            <ta e="T146" id="Seg_6684" s="T145">come-PST.[3SG]</ta>
            <ta e="T147" id="Seg_6685" s="T146">come-DUR-3PL</ta>
            <ta e="T148" id="Seg_6686" s="T147">horse-COM</ta>
            <ta e="T150" id="Seg_6687" s="T149">this-ACC</ta>
            <ta e="T152" id="Seg_6688" s="T151">take-MOM-PST-3PL</ta>
            <ta e="T153" id="Seg_6689" s="T152">and</ta>
            <ta e="T154" id="Seg_6690" s="T153">bring-PST-3PL</ta>
            <ta e="T155" id="Seg_6691" s="T154">tent-LAT/LOC.3SG</ta>
            <ta e="T174" id="Seg_6692" s="T173">mother-NOM/GEN.3SG</ta>
            <ta e="T175" id="Seg_6693" s="T174">and</ta>
            <ta e="T176" id="Seg_6694" s="T175">father-NOM/GEN.3SG</ta>
            <ta e="T177" id="Seg_6695" s="T176">say-PST-3PL</ta>
            <ta e="T179" id="Seg_6696" s="T178">then</ta>
            <ta e="T180" id="Seg_6697" s="T179">make-PST-3PL</ta>
            <ta e="T182" id="Seg_6698" s="T181">vodka.[NOM.SG]</ta>
            <ta e="T183" id="Seg_6699" s="T182">many</ta>
            <ta e="T184" id="Seg_6700" s="T183">be-PST.[3SG]</ta>
            <ta e="T185" id="Seg_6701" s="T184">PTCL</ta>
            <ta e="T186" id="Seg_6702" s="T185">what.[NOM.SG]</ta>
            <ta e="T187" id="Seg_6703" s="T186">make-PST-3PL</ta>
            <ta e="T189" id="Seg_6704" s="T188">duck-PL</ta>
            <ta e="T190" id="Seg_6705" s="T189">bake-PST-3PL</ta>
            <ta e="T191" id="Seg_6706" s="T190">meat.[NOM.SG]</ta>
            <ta e="T192" id="Seg_6707" s="T191">many</ta>
            <ta e="T194" id="Seg_6708" s="T193">then</ta>
            <ta e="T196" id="Seg_6709" s="T195">set.up-PST-3PL</ta>
            <ta e="T197" id="Seg_6710" s="T196">table-ACC.3SG</ta>
            <ta e="T198" id="Seg_6711" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_6712" s="T198">eat-PST-3PL</ta>
            <ta e="T200" id="Seg_6713" s="T199">and</ta>
            <ta e="T1531" id="Seg_6714" s="T200">go-CVB</ta>
            <ta e="T201" id="Seg_6715" s="T1531">disappear-PST-3PL</ta>
            <ta e="T203" id="Seg_6716" s="T202">daughter-PL</ta>
            <ta e="T204" id="Seg_6717" s="T203">people.[NOM.SG]</ta>
            <ta e="T205" id="Seg_6718" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_6719" s="T205">vodka.[NOM.SG]</ta>
            <ta e="T207" id="Seg_6720" s="T206">drink-PST-3PL</ta>
            <ta e="T208" id="Seg_6721" s="T207">play-PST-3PL</ta>
            <ta e="T209" id="Seg_6722" s="T208">accordion-INS</ta>
            <ta e="T211" id="Seg_6723" s="T210">jump-PST-3PL</ta>
            <ta e="T213" id="Seg_6724" s="T212">then</ta>
            <ta e="T214" id="Seg_6725" s="T213">PTCL</ta>
            <ta e="T1532" id="Seg_6726" s="T214">go-CVB</ta>
            <ta e="T215" id="Seg_6727" s="T1532">disappear-PST-3PL</ta>
            <ta e="T248" id="Seg_6728" s="T247">live-PST-3PL</ta>
            <ta e="T249" id="Seg_6729" s="T248">good</ta>
            <ta e="T250" id="Seg_6730" s="T249">and</ta>
            <ta e="T251" id="Seg_6731" s="T250">fight-DUR.PST-3PL</ta>
            <ta e="T253" id="Seg_6732" s="T252">vodka.[NOM.SG]</ta>
            <ta e="T254" id="Seg_6733" s="T253">drink-FUT-3PL</ta>
            <ta e="T255" id="Seg_6734" s="T254">so</ta>
            <ta e="T256" id="Seg_6735" s="T255">fight-DUR-3PL</ta>
            <ta e="T257" id="Seg_6736" s="T256">hair-GEN</ta>
            <ta e="T258" id="Seg_6737" s="T257">hair-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T259" id="Seg_6738" s="T258">one.[NOM.SG]</ta>
            <ta e="T260" id="Seg_6739" s="T259">one-LAT</ta>
            <ta e="T261" id="Seg_6740" s="T260">take-PST-3PL</ta>
            <ta e="T262" id="Seg_6741" s="T261">and</ta>
            <ta e="T263" id="Seg_6742" s="T262">fight-DUR.PST-3PL</ta>
            <ta e="T372" id="Seg_6743" s="T371">who.[NOM.SG]</ta>
            <ta e="T373" id="Seg_6744" s="T372">five.[NOM.SG]</ta>
            <ta e="T374" id="Seg_6745" s="T373">be-PST.[3SG]</ta>
            <ta e="T375" id="Seg_6746" s="T374">who.[NOM.SG]</ta>
            <ta e="T376" id="Seg_6747" s="T375">seven.[NOM.SG]</ta>
            <ta e="T377" id="Seg_6748" s="T376">be-PST.[3SG]</ta>
            <ta e="T378" id="Seg_6749" s="T377">child-PL</ta>
            <ta e="T381" id="Seg_6750" s="T379">many</ta>
            <ta e="T383" id="Seg_6751" s="T381">bring-PST-3PL</ta>
            <ta e="T384" id="Seg_6752" s="T383">this-PL</ta>
            <ta e="T385" id="Seg_6753" s="T384">what.[NOM.SG]=INDEF</ta>
            <ta e="T386" id="Seg_6754" s="T385">NEG</ta>
            <ta e="T387" id="Seg_6755" s="T386">make-PST-3PL</ta>
            <ta e="T389" id="Seg_6756" s="T388">now</ta>
            <ta e="T390" id="Seg_6757" s="T389">people.[NOM.SG]</ta>
            <ta e="T391" id="Seg_6758" s="T390">child-PL</ta>
            <ta e="T392" id="Seg_6759" s="T391">leave-%%-3PL</ta>
            <ta e="T393" id="Seg_6760" s="T392">hospital-LAT</ta>
            <ta e="T394" id="Seg_6761" s="T393">go-PST-3PL</ta>
            <ta e="T395" id="Seg_6762" s="T394">and</ta>
            <ta e="T396" id="Seg_6763" s="T395">what.[NOM.SG]=INDEF</ta>
            <ta e="T397" id="Seg_6764" s="T396">make-PST-3PL</ta>
            <ta e="T398" id="Seg_6765" s="T397">NEG</ta>
            <ta e="T399" id="Seg_6766" s="T398">bring-PRS-3PL</ta>
            <ta e="T400" id="Seg_6767" s="T399">child-PL</ta>
            <ta e="T404" id="Seg_6768" s="T403">well</ta>
            <ta e="T405" id="Seg_6769" s="T404">child-PL-LAT</ta>
            <ta e="T406" id="Seg_6770" s="T405">this-PL</ta>
            <ta e="T407" id="Seg_6771" s="T406">hurt-PST-3PL</ta>
            <ta e="T408" id="Seg_6772" s="T407">die-RES-PST-3PL</ta>
            <ta e="T409" id="Seg_6773" s="T408">few</ta>
            <ta e="T410" id="Seg_6774" s="T409">remain-MOM-PST-3PL</ta>
            <ta e="T437" id="Seg_6775" s="T433">cry-PST-3PL</ta>
            <ta e="T438" id="Seg_6776" s="T437">child-PL</ta>
            <ta e="T441" id="Seg_6777" s="T440">and</ta>
            <ta e="T444" id="Seg_6778" s="T443">hurt-PST-3PL</ta>
            <ta e="T459" id="Seg_6779" s="T458">mother-NOM/GEN.3SG</ta>
            <ta e="T460" id="Seg_6780" s="T459">get.up-PRS.[3SG]</ta>
            <ta e="T461" id="Seg_6781" s="T460">and</ta>
            <ta e="T462" id="Seg_6782" s="T461">take-FUT-3SG</ta>
            <ta e="T464" id="Seg_6783" s="T463">then</ta>
            <ta e="T465" id="Seg_6784" s="T464">give-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_6785" s="T465">breast.[NOM.SG]</ta>
            <ta e="T468" id="Seg_6786" s="T467">this-PL-LAT</ta>
            <ta e="T470" id="Seg_6787" s="T469">eat-FUT-3SG</ta>
            <ta e="T471" id="Seg_6788" s="T470">and</ta>
            <ta e="T472" id="Seg_6789" s="T471">put-PST.[3SG]</ta>
            <ta e="T474" id="Seg_6790" s="T473">this.[NOM.SG]</ta>
            <ta e="T475" id="Seg_6791" s="T474">sleep-MOM-PST.[3SG]</ta>
            <ta e="T477" id="Seg_6792" s="T476">and</ta>
            <ta e="T478" id="Seg_6793" s="T477">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T479" id="Seg_6794" s="T478">lie.down-PST.[3SG]</ta>
            <ta e="T480" id="Seg_6795" s="T479">sleep-MOM-PST.[3SG]</ta>
            <ta e="T503" id="Seg_6796" s="T502">listen-DUR.PST-3PL</ta>
            <ta e="T504" id="Seg_6797" s="T503">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T506" id="Seg_6798" s="T505">father-NOM/GEN.3SG</ta>
            <ta e="T507" id="Seg_6799" s="T506">father-NOM/GEN.3SG</ta>
            <ta e="T508" id="Seg_6800" s="T507">mother-NOM/GEN.3SG</ta>
            <ta e="T510" id="Seg_6801" s="T509">I.NOM</ta>
            <ta e="T511" id="Seg_6802" s="T510">small.[NOM.SG]</ta>
            <ta e="T512" id="Seg_6803" s="T511">be-PST-1SG</ta>
            <ta e="T513" id="Seg_6804" s="T512">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T514" id="Seg_6805" s="T513">say-IPFVZ.[3SG]</ta>
            <ta e="T515" id="Seg_6806" s="T514">go-EP-IMP.2SG</ta>
            <ta e="T517" id="Seg_6807" s="T516">let-FUT-3SG</ta>
            <ta e="T518" id="Seg_6808" s="T517">where.to=INDEF</ta>
            <ta e="T520" id="Seg_6809" s="T519">I.NOM</ta>
            <ta e="T521" id="Seg_6810" s="T520">stand-PRS-1SG</ta>
            <ta e="T522" id="Seg_6811" s="T521">and</ta>
            <ta e="T523" id="Seg_6812" s="T522">try-DUR-1SG</ta>
            <ta e="T524" id="Seg_6813" s="T523">there</ta>
            <ta e="T526" id="Seg_6814" s="T525">and</ta>
            <ta e="T527" id="Seg_6815" s="T526">this.[NOM.SG]</ta>
            <ta e="T528" id="Seg_6816" s="T527">I.NOM</ta>
            <ta e="T529" id="Seg_6817" s="T528">you.ACC</ta>
            <ta e="T530" id="Seg_6818" s="T529">beat-FUT-1SG</ta>
            <ta e="T532" id="Seg_6819" s="T531">I.NOM</ta>
            <ta e="T533" id="Seg_6820" s="T532">then</ta>
            <ta e="T534" id="Seg_6821" s="T533">run-MOM-PST-1SG</ta>
            <ta e="T535" id="Seg_6822" s="T534">and</ta>
            <ta e="T536" id="Seg_6823" s="T535">soon</ta>
            <ta e="T537" id="Seg_6824" s="T536">come-PST-1SG</ta>
            <ta e="T539" id="Seg_6825" s="T538">then</ta>
            <ta e="T540" id="Seg_6826" s="T539">this.[NOM.SG]</ta>
            <ta e="T541" id="Seg_6827" s="T540">what.[NOM.SG]=INDEF</ta>
            <ta e="T542" id="Seg_6828" s="T541">I.LAT</ta>
            <ta e="T543" id="Seg_6829" s="T542">NEG</ta>
            <ta e="T544" id="Seg_6830" s="T543">beat-DUR.PST.[3SG]</ta>
            <ta e="T562" id="Seg_6831" s="T561">beat-PST-3PL</ta>
            <ta e="T564" id="Seg_6832" s="T563">strongly</ta>
            <ta e="T565" id="Seg_6833" s="T564">%%-INS</ta>
            <ta e="T567" id="Seg_6834" s="T566">I.LAT</ta>
            <ta e="T568" id="Seg_6835" s="T567">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T569" id="Seg_6836" s="T568">very</ta>
            <ta e="T570" id="Seg_6837" s="T569">strongly</ta>
            <ta e="T571" id="Seg_6838" s="T570">beat-PST-3PL</ta>
            <ta e="T575" id="Seg_6839" s="T572">I.NOM</ta>
            <ta e="T576" id="Seg_6840" s="T575">strongly</ta>
            <ta e="T577" id="Seg_6841" s="T576">make.noise-PST-1SG</ta>
            <ta e="T724" id="Seg_6842" s="T722">press-INF.LAT</ta>
            <ta e="T725" id="Seg_6843" s="T724">NEG</ta>
            <ta e="T726" id="Seg_6844" s="T725">can-PRS-1SG</ta>
            <ta e="T727" id="Seg_6845" s="T726">and</ta>
            <ta e="T729" id="Seg_6846" s="T728">and</ta>
            <ta e="T730" id="Seg_6847" s="T729">can-PRS-1SG</ta>
            <ta e="T731" id="Seg_6848" s="T730">book-NOM/GEN/ACC.1SG</ta>
            <ta e="T732" id="Seg_6849" s="T731">look-PRS-1SG</ta>
            <ta e="T733" id="Seg_6850" s="T732">what.[NOM.SG]</ta>
            <ta e="T734" id="Seg_6851" s="T733">there</ta>
            <ta e="T735" id="Seg_6852" s="T734">be-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_6853" s="T735">write-DETR-PTCP</ta>
            <ta e="T738" id="Seg_6854" s="T737">and</ta>
            <ta e="T739" id="Seg_6855" s="T738">write-INF.LAT</ta>
            <ta e="T740" id="Seg_6856" s="T739">NEG</ta>
            <ta e="T741" id="Seg_6857" s="T740">can-PRS-1SG</ta>
            <ta e="T771" id="Seg_6858" s="T770">this-PL</ta>
            <ta e="T772" id="Seg_6859" s="T771">many</ta>
            <ta e="T773" id="Seg_6860" s="T772">NEG</ta>
            <ta e="T774" id="Seg_6861" s="T773">live-PST.[3SG]</ta>
            <ta e="T775" id="Seg_6862" s="T774">live-DUR.PST-3PL</ta>
            <ta e="T777" id="Seg_6863" s="T776">this-PL</ta>
            <ta e="T778" id="Seg_6864" s="T777">always</ta>
            <ta e="T779" id="Seg_6865" s="T778">outside</ta>
            <ta e="T780" id="Seg_6866" s="T779">outside</ta>
            <ta e="T781" id="Seg_6867" s="T780">house-PL-LAT</ta>
            <ta e="T782" id="Seg_6868" s="T781">NEG.EX-PST-3PL</ta>
            <ta e="T784" id="Seg_6869" s="T783">freeze-FACT-PST-3PL</ta>
            <ta e="T786" id="Seg_6870" s="T785">and</ta>
            <ta e="T787" id="Seg_6871" s="T786">then</ta>
            <ta e="T788" id="Seg_6872" s="T787">this-PL-LAT</ta>
            <ta e="T789" id="Seg_6873" s="T788">heal-INF.LAT</ta>
            <ta e="T790" id="Seg_6874" s="T789">NEG.EX-PST.[3SG]</ta>
            <ta e="T792" id="Seg_6875" s="T791">this-PL</ta>
            <ta e="T793" id="Seg_6876" s="T792">die-RES-PST-3PL</ta>
            <ta e="T796" id="Seg_6877" s="T795">few</ta>
            <ta e="T797" id="Seg_6878" s="T796">live-DUR-3PL</ta>
            <ta e="T801" id="Seg_6879" s="T800">we.LAT</ta>
            <ta e="T802" id="Seg_6880" s="T801">white.[NOM.SG]</ta>
            <ta e="T803" id="Seg_6881" s="T802">tree.[NOM.SG]</ta>
            <ta e="T804" id="Seg_6882" s="T803">NEG.EX-PST.[3SG]</ta>
            <ta e="T806" id="Seg_6883" s="T805">this-PL</ta>
            <ta e="T807" id="Seg_6884" s="T806">set.up-PST-3PL</ta>
            <ta e="T808" id="Seg_6885" s="T807">four.[NOM.SG]</ta>
            <ta e="T809" id="Seg_6886" s="T808">tree.[NOM.SG]</ta>
            <ta e="T811" id="Seg_6887" s="T810">and</ta>
            <ta e="T812" id="Seg_6888" s="T811">there</ta>
            <ta e="T813" id="Seg_6889" s="T812">put-PST-3PL</ta>
            <ta e="T815" id="Seg_6890" s="T814">then</ta>
            <ta e="T816" id="Seg_6891" s="T815">this.[NOM.SG]</ta>
            <ta e="T817" id="Seg_6892" s="T816">there</ta>
            <ta e="T818" id="Seg_6893" s="T817">up</ta>
            <ta e="T819" id="Seg_6894" s="T818">stone-PL</ta>
            <ta e="T820" id="Seg_6895" s="T819">put-PST-3PL</ta>
            <ta e="T821" id="Seg_6896" s="T820">then</ta>
            <ta e="T822" id="Seg_6897" s="T821">this.[NOM.SG]</ta>
            <ta e="T823" id="Seg_6898" s="T822">tree-PL</ta>
            <ta e="T825" id="Seg_6899" s="T824">cut-PST-3PL</ta>
            <ta e="T826" id="Seg_6900" s="T825">this.[NOM.SG]</ta>
            <ta e="T827" id="Seg_6901" s="T826">fall-MOM.[3SG]</ta>
            <ta e="T828" id="Seg_6902" s="T827">this-ACC.PL</ta>
            <ta e="T829" id="Seg_6903" s="T828">press-MOM.[3SG]</ta>
            <ta e="T831" id="Seg_6904" s="T830">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T833" id="Seg_6905" s="T832">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T835" id="Seg_6906" s="T834">kill-DUR.PST-3PL</ta>
            <ta e="T893" id="Seg_6907" s="T892">well</ta>
            <ta e="T894" id="Seg_6908" s="T893">earth.[NOM.SG]</ta>
            <ta e="T896" id="Seg_6909" s="T895">dig-PST-3PL</ta>
            <ta e="T897" id="Seg_6910" s="T896">then</ta>
            <ta e="T898" id="Seg_6911" s="T897">tree.[NOM.SG]</ta>
            <ta e="T899" id="Seg_6912" s="T898">eat-PST-3PL</ta>
            <ta e="T900" id="Seg_6913" s="T899">tree.[NOM.SG]</ta>
            <ta e="T901" id="Seg_6914" s="T900">cut-PST-3PL</ta>
            <ta e="T903" id="Seg_6915" s="T902">then</ta>
            <ta e="T904" id="Seg_6916" s="T903">two.[NOM.SG]</ta>
            <ta e="T905" id="Seg_6917" s="T904">make-PST-3PL</ta>
            <ta e="T906" id="Seg_6918" s="T905">and</ta>
            <ta e="T907" id="Seg_6919" s="T906">there</ta>
            <ta e="T908" id="Seg_6920" s="T907">there</ta>
            <ta e="T909" id="Seg_6921" s="T908">also</ta>
            <ta e="T910" id="Seg_6922" s="T909">axe-INS</ta>
            <ta e="T911" id="Seg_6923" s="T910">cut-PST-3PL</ta>
            <ta e="T913" id="Seg_6924" s="T912">then</ta>
            <ta e="T914" id="Seg_6925" s="T913">this-ACC</ta>
            <ta e="T915" id="Seg_6926" s="T914">there</ta>
            <ta e="T916" id="Seg_6927" s="T915">put-PST-3PL</ta>
            <ta e="T917" id="Seg_6928" s="T916">bring-PST-3PL</ta>
            <ta e="T919" id="Seg_6929" s="T918">put-PST-3PL</ta>
            <ta e="T920" id="Seg_6930" s="T919">place-LAT</ta>
            <ta e="T922" id="Seg_6931" s="T921">and</ta>
            <ta e="T923" id="Seg_6932" s="T922">cross.[NOM.SG]</ta>
            <ta e="T924" id="Seg_6933" s="T923">make-PST-3PL</ta>
            <ta e="T926" id="Seg_6934" s="T925">and</ta>
            <ta e="T927" id="Seg_6935" s="T926">then</ta>
            <ta e="T928" id="Seg_6936" s="T927">tent-LAT/LOC.3SG</ta>
            <ta e="T930" id="Seg_6937" s="T929">come-PST-3PL</ta>
            <ta e="T932" id="Seg_6938" s="T931">boil-DUR-3PL</ta>
            <ta e="T933" id="Seg_6939" s="T932">and</ta>
            <ta e="T934" id="Seg_6940" s="T933">then</ta>
            <ta e="T935" id="Seg_6941" s="T934">eat-DUR-3PL</ta>
            <ta e="T937" id="Seg_6942" s="T936">and</ta>
            <ta e="T938" id="Seg_6943" s="T937">PTCL</ta>
            <ta e="T1271" id="Seg_6944" s="T1270">well</ta>
            <ta e="T1272" id="Seg_6945" s="T1271">when</ta>
            <ta e="T1273" id="Seg_6946" s="T1272">die-RES-FUT-3SG</ta>
            <ta e="T1274" id="Seg_6947" s="T1273">man.[NOM.SG]</ta>
            <ta e="T1275" id="Seg_6948" s="T1274">place-LAT</ta>
            <ta e="T1276" id="Seg_6949" s="T1275">dig-DUR-3PL</ta>
            <ta e="T1278" id="Seg_6950" s="T1277">and</ta>
            <ta e="T1279" id="Seg_6951" s="T1278">there</ta>
            <ta e="T1280" id="Seg_6952" s="T1279">up</ta>
            <ta e="T1281" id="Seg_6953" s="T1280">spirit.[NOM.SG]</ta>
            <ta e="T1282" id="Seg_6954" s="T1281">there</ta>
            <ta e="T1533" id="Seg_6955" s="T1282">go-CVB</ta>
            <ta e="T1283" id="Seg_6956" s="T1533">disappear-FUT-3SG</ta>
            <ta e="T1284" id="Seg_6957" s="T1283">up</ta>
            <ta e="T1286" id="Seg_6958" s="T1285">I.NOM</ta>
            <ta e="T1287" id="Seg_6959" s="T1286">so</ta>
            <ta e="T1288" id="Seg_6960" s="T1287">know-1SG</ta>
            <ta e="T1292" id="Seg_6961" s="T1291">who.[NOM.SG]</ta>
            <ta e="T1293" id="Seg_6962" s="T1292">how</ta>
            <ta e="T1294" id="Seg_6963" s="T1293">and</ta>
            <ta e="T1295" id="Seg_6964" s="T1294">I.NOM</ta>
            <ta e="T1296" id="Seg_6965" s="T1295">so</ta>
            <ta e="T1297" id="Seg_6966" s="T1296">know-1SG</ta>
            <ta e="T1299" id="Seg_6967" s="T1298">and</ta>
            <ta e="T1300" id="Seg_6968" s="T1299">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T1301" id="Seg_6969" s="T1300">say-IPFVZ.[3SG]</ta>
            <ta e="T1302" id="Seg_6970" s="T1301">so</ta>
            <ta e="T1303" id="Seg_6971" s="T1302">say-IPFVZ.[3SG]</ta>
            <ta e="T1304" id="Seg_6972" s="T1303">I.LAT</ta>
            <ta e="T1307" id="Seg_6973" s="T1306">this-GEN</ta>
            <ta e="T1308" id="Seg_6974" s="T1307">go-FUT-3SG</ta>
            <ta e="T1309" id="Seg_6975" s="T1308">God-LAT</ta>
            <ta e="T1313" id="Seg_6976" s="T1312">man-ACC</ta>
            <ta e="T1314" id="Seg_6977" s="T1313">God.[NOM.SG]</ta>
            <ta e="T1315" id="Seg_6978" s="T1314">make-PST.[3SG]</ta>
            <ta e="T1316" id="Seg_6979" s="T1315">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1317" id="Seg_6980" s="T1316">earth-LOC</ta>
            <ta e="T1318" id="Seg_6981" s="T1317">and</ta>
            <ta e="T1319" id="Seg_6982" s="T1318">this-LAT</ta>
            <ta e="T1320" id="Seg_6983" s="T1319">PTCL</ta>
            <ta e="T1322" id="Seg_6984" s="T1321">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1323" id="Seg_6985" s="T1322">spirit-LAT</ta>
            <ta e="T1325" id="Seg_6986" s="T1324">give-PST.[3SG]</ta>
            <ta e="T1326" id="Seg_6987" s="T1325">and</ta>
            <ta e="T1327" id="Seg_6988" s="T1326">this.[NOM.SG]</ta>
            <ta e="T1328" id="Seg_6989" s="T1327">get.up-PST.[3SG]</ta>
            <ta e="T1329" id="Seg_6990" s="T1328">man.[NOM.SG]</ta>
            <ta e="T1330" id="Seg_6991" s="T1329">become-RES-PST.[3SG]</ta>
            <ta e="T1452" id="Seg_6992" s="T1451">well</ta>
            <ta e="T1453" id="Seg_6993" s="T1452">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T54" id="Seg_6994" s="T53">играть-PST-1PL</ta>
            <ta e="T55" id="Seg_6995" s="T54">и</ta>
            <ta e="T56" id="Seg_6996" s="T55">тогда</ta>
            <ta e="T57" id="Seg_6997" s="T56">какой</ta>
            <ta e="T58" id="Seg_6998" s="T57">мальчик.[NOM.SG]</ta>
            <ta e="T59" id="Seg_6999" s="T58">я.LAT</ta>
            <ta e="T60" id="Seg_7000" s="T59">прийти-FUT-3SG</ta>
            <ta e="T61" id="Seg_7001" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_7002" s="T61">этот-LAT</ta>
            <ta e="T64" id="Seg_7003" s="T63">и</ta>
            <ta e="T65" id="Seg_7004" s="T64">дочь-PL</ta>
            <ta e="T66" id="Seg_7005" s="T65">много</ta>
            <ta e="T67" id="Seg_7006" s="T66">быть-PST-3PL</ta>
            <ta e="T68" id="Seg_7007" s="T67">когда</ta>
            <ta e="T70" id="Seg_7008" s="T69">я.NOM</ta>
            <ta e="T71" id="Seg_7009" s="T70">быть-PST-1SG</ta>
            <ta e="T72" id="Seg_7010" s="T71">так</ta>
            <ta e="T73" id="Seg_7011" s="T72">NEG.EX-PST-3PL</ta>
            <ta e="T74" id="Seg_7012" s="T73">сын-PL</ta>
            <ta e="T76" id="Seg_7013" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_7014" s="T76">идти-PST-1SG</ta>
            <ta e="T78" id="Seg_7015" s="T77">там</ta>
            <ta e="T79" id="Seg_7016" s="T78">Пьянково-LAT</ta>
            <ta e="T80" id="Seg_7017" s="T79">мужчина-LAT</ta>
            <ta e="T81" id="Seg_7018" s="T80">идти-PST-1SG</ta>
            <ta e="T82" id="Seg_7019" s="T81">там</ta>
            <ta e="T83" id="Seg_7020" s="T82">сын-NOM/GEN/ACC.1SG</ta>
            <ta e="T84" id="Seg_7021" s="T83">быть-PST.[3SG]</ta>
            <ta e="T85" id="Seg_7022" s="T84">и</ta>
            <ta e="T86" id="Seg_7023" s="T85">я.LAT</ta>
            <ta e="T87" id="Seg_7024" s="T86">я.NOM</ta>
            <ta e="T88" id="Seg_7025" s="T87">там</ta>
            <ta e="T89" id="Seg_7026" s="T88">остаться-MOM-PST-1SG</ta>
            <ta e="T106" id="Seg_7027" s="T105">ну</ta>
            <ta e="T107" id="Seg_7028" s="T106">мать-NOM/GEN.3SG</ta>
            <ta e="T108" id="Seg_7029" s="T107">отец-NOM/GEN.3SG</ta>
            <ta e="T109" id="Seg_7030" s="T108">прийти-FUT-3SG</ta>
            <ta e="T110" id="Seg_7031" s="T109">мальчик-GEN</ta>
            <ta e="T111" id="Seg_7032" s="T110">дочь-GEN.1SG</ta>
            <ta e="T112" id="Seg_7033" s="T111">тогда</ta>
            <ta e="T113" id="Seg_7034" s="T112">этот.[NOM.SG]</ta>
            <ta e="T114" id="Seg_7035" s="T113">этот-LAT</ta>
            <ta e="T115" id="Seg_7036" s="T114">сказать-PRS-3PL</ta>
            <ta e="T116" id="Seg_7037" s="T115">пойти-EP-IMP.2SG</ta>
            <ta e="T117" id="Seg_7038" s="T116">мы.GEN</ta>
            <ta e="T118" id="Seg_7039" s="T117">мальчик-LAT</ta>
            <ta e="T119" id="Seg_7040" s="T118">мужчина-LAT</ta>
            <ta e="T121" id="Seg_7041" s="T120">отец-LAT/LOC.3SG</ta>
            <ta e="T122" id="Seg_7042" s="T121">мать-LAT/LOC.3SG</ta>
            <ta e="T123" id="Seg_7043" s="T122">спросить-FUT-3PL</ta>
            <ta e="T124" id="Seg_7044" s="T123">надо</ta>
            <ta e="T125" id="Seg_7045" s="T124">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T126" id="Seg_7046" s="T125">спросить-INF.LAT</ta>
            <ta e="T127" id="Seg_7047" s="T126">тогда</ta>
            <ta e="T128" id="Seg_7048" s="T127">прийти-FUT-3SG</ta>
            <ta e="T129" id="Seg_7049" s="T128">девушка.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7050" s="T129">пойти-FUT-2SG</ta>
            <ta e="T131" id="Seg_7051" s="T130">мужчина-LAT</ta>
            <ta e="T133" id="Seg_7052" s="T132">этот.[NOM.SG]</ta>
            <ta e="T134" id="Seg_7053" s="T133">сказать-IPFVZ.[3SG]</ta>
            <ta e="T135" id="Seg_7054" s="T134">пойти-FUT-1SG</ta>
            <ta e="T137" id="Seg_7055" s="T136">ну</ta>
            <ta e="T138" id="Seg_7056" s="T137">тогда</ta>
            <ta e="T139" id="Seg_7057" s="T138">водка.[NOM.SG]</ta>
            <ta e="T140" id="Seg_7058" s="T139">принести-FUT-3PL</ta>
            <ta e="T141" id="Seg_7059" s="T140">хлеб.[NOM.SG]</ta>
            <ta e="T143" id="Seg_7060" s="T142">сидеть-DUR-3PL</ta>
            <ta e="T144" id="Seg_7061" s="T143">пить-DUR-3PL</ta>
            <ta e="T145" id="Seg_7062" s="T144">тогда</ta>
            <ta e="T146" id="Seg_7063" s="T145">прийти-PST.[3SG]</ta>
            <ta e="T147" id="Seg_7064" s="T146">прийти-DUR-3PL</ta>
            <ta e="T148" id="Seg_7065" s="T147">лошадь-COM</ta>
            <ta e="T150" id="Seg_7066" s="T149">этот-ACC</ta>
            <ta e="T152" id="Seg_7067" s="T151">взять-MOM-PST-3PL</ta>
            <ta e="T153" id="Seg_7068" s="T152">и</ta>
            <ta e="T154" id="Seg_7069" s="T153">нести-PST-3PL</ta>
            <ta e="T155" id="Seg_7070" s="T154">чум-LAT/LOC.3SG</ta>
            <ta e="T174" id="Seg_7071" s="T173">мать-NOM/GEN.3SG</ta>
            <ta e="T175" id="Seg_7072" s="T174">и</ta>
            <ta e="T176" id="Seg_7073" s="T175">отец-NOM/GEN.3SG</ta>
            <ta e="T177" id="Seg_7074" s="T176">сказать-PST-3PL</ta>
            <ta e="T179" id="Seg_7075" s="T178">тогда</ta>
            <ta e="T180" id="Seg_7076" s="T179">делать-PST-3PL</ta>
            <ta e="T182" id="Seg_7077" s="T181">водка.[NOM.SG]</ta>
            <ta e="T183" id="Seg_7078" s="T182">много</ta>
            <ta e="T184" id="Seg_7079" s="T183">быть-PST.[3SG]</ta>
            <ta e="T185" id="Seg_7080" s="T184">PTCL</ta>
            <ta e="T186" id="Seg_7081" s="T185">что.[NOM.SG]</ta>
            <ta e="T187" id="Seg_7082" s="T186">делать-PST-3PL</ta>
            <ta e="T189" id="Seg_7083" s="T188">утка-PL</ta>
            <ta e="T190" id="Seg_7084" s="T189">печь-PST-3PL</ta>
            <ta e="T191" id="Seg_7085" s="T190">мясо.[NOM.SG]</ta>
            <ta e="T192" id="Seg_7086" s="T191">много</ta>
            <ta e="T194" id="Seg_7087" s="T193">тогда</ta>
            <ta e="T196" id="Seg_7088" s="T195">установить-PST-3PL</ta>
            <ta e="T197" id="Seg_7089" s="T196">стол-ACC.3SG</ta>
            <ta e="T198" id="Seg_7090" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_7091" s="T198">съесть-PST-3PL</ta>
            <ta e="T200" id="Seg_7092" s="T199">и</ta>
            <ta e="T1531" id="Seg_7093" s="T200">пойти-CVB</ta>
            <ta e="T201" id="Seg_7094" s="T1531">исчезнуть-PST-3PL</ta>
            <ta e="T203" id="Seg_7095" s="T202">дочь-PL</ta>
            <ta e="T204" id="Seg_7096" s="T203">люди.[NOM.SG]</ta>
            <ta e="T205" id="Seg_7097" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_7098" s="T205">водка.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7099" s="T206">пить-PST-3PL</ta>
            <ta e="T208" id="Seg_7100" s="T207">играть-PST-3PL</ta>
            <ta e="T209" id="Seg_7101" s="T208">гармонь-INS</ta>
            <ta e="T211" id="Seg_7102" s="T210">прыгнуть-PST-3PL</ta>
            <ta e="T213" id="Seg_7103" s="T212">тогда</ta>
            <ta e="T214" id="Seg_7104" s="T213">PTCL</ta>
            <ta e="T1532" id="Seg_7105" s="T214">пойти-CVB</ta>
            <ta e="T215" id="Seg_7106" s="T1532">исчезнуть-PST-3PL</ta>
            <ta e="T248" id="Seg_7107" s="T247">жить-PST-3PL</ta>
            <ta e="T249" id="Seg_7108" s="T248">хороший</ta>
            <ta e="T250" id="Seg_7109" s="T249">и</ta>
            <ta e="T251" id="Seg_7110" s="T250">бороться-DUR.PST-3PL</ta>
            <ta e="T253" id="Seg_7111" s="T252">водка.[NOM.SG]</ta>
            <ta e="T254" id="Seg_7112" s="T253">пить-FUT-3PL</ta>
            <ta e="T255" id="Seg_7113" s="T254">так</ta>
            <ta e="T256" id="Seg_7114" s="T255">бороться-DUR-3PL</ta>
            <ta e="T257" id="Seg_7115" s="T256">волосы-GEN</ta>
            <ta e="T258" id="Seg_7116" s="T257">волосы-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T259" id="Seg_7117" s="T258">один.[NOM.SG]</ta>
            <ta e="T260" id="Seg_7118" s="T259">один-LAT</ta>
            <ta e="T261" id="Seg_7119" s="T260">взять-PST-3PL</ta>
            <ta e="T262" id="Seg_7120" s="T261">и</ta>
            <ta e="T263" id="Seg_7121" s="T262">бороться-DUR.PST-3PL</ta>
            <ta e="T372" id="Seg_7122" s="T371">кто.[NOM.SG]</ta>
            <ta e="T373" id="Seg_7123" s="T372">пять.[NOM.SG]</ta>
            <ta e="T374" id="Seg_7124" s="T373">быть-PST.[3SG]</ta>
            <ta e="T375" id="Seg_7125" s="T374">кто.[NOM.SG]</ta>
            <ta e="T376" id="Seg_7126" s="T375">семь.[NOM.SG]</ta>
            <ta e="T377" id="Seg_7127" s="T376">быть-PST.[3SG]</ta>
            <ta e="T378" id="Seg_7128" s="T377">ребенок-PL</ta>
            <ta e="T381" id="Seg_7129" s="T379">много</ta>
            <ta e="T383" id="Seg_7130" s="T381">принести-PST-3PL</ta>
            <ta e="T384" id="Seg_7131" s="T383">этот-PL</ta>
            <ta e="T385" id="Seg_7132" s="T384">что.[NOM.SG]=INDEF</ta>
            <ta e="T386" id="Seg_7133" s="T385">NEG</ta>
            <ta e="T387" id="Seg_7134" s="T386">делать-PST-3PL</ta>
            <ta e="T389" id="Seg_7135" s="T388">сейчас</ta>
            <ta e="T390" id="Seg_7136" s="T389">люди.[NOM.SG]</ta>
            <ta e="T391" id="Seg_7137" s="T390">ребенок-PL</ta>
            <ta e="T392" id="Seg_7138" s="T391">оставить-%%-3PL</ta>
            <ta e="T393" id="Seg_7139" s="T392">больница-LAT</ta>
            <ta e="T394" id="Seg_7140" s="T393">идти-PST-3PL</ta>
            <ta e="T395" id="Seg_7141" s="T394">и</ta>
            <ta e="T396" id="Seg_7142" s="T395">что.[NOM.SG]=INDEF</ta>
            <ta e="T397" id="Seg_7143" s="T396">делать-PST-3PL</ta>
            <ta e="T398" id="Seg_7144" s="T397">NEG</ta>
            <ta e="T399" id="Seg_7145" s="T398">принести-PRS-3PL</ta>
            <ta e="T400" id="Seg_7146" s="T399">ребенок-PL</ta>
            <ta e="T404" id="Seg_7147" s="T403">ну</ta>
            <ta e="T405" id="Seg_7148" s="T404">ребенок-PL-LAT</ta>
            <ta e="T406" id="Seg_7149" s="T405">этот-PL</ta>
            <ta e="T407" id="Seg_7150" s="T406">болеть-PST-3PL</ta>
            <ta e="T408" id="Seg_7151" s="T407">умереть-RES-PST-3PL</ta>
            <ta e="T409" id="Seg_7152" s="T408">мало</ta>
            <ta e="T410" id="Seg_7153" s="T409">остаться-MOM-PST-3PL</ta>
            <ta e="T437" id="Seg_7154" s="T433">плакать-PST-3PL</ta>
            <ta e="T438" id="Seg_7155" s="T437">ребенок-PL</ta>
            <ta e="T441" id="Seg_7156" s="T440">и</ta>
            <ta e="T444" id="Seg_7157" s="T443">болеть-PST-3PL</ta>
            <ta e="T459" id="Seg_7158" s="T458">мать-NOM/GEN.3SG</ta>
            <ta e="T460" id="Seg_7159" s="T459">встать-PRS.[3SG]</ta>
            <ta e="T461" id="Seg_7160" s="T460">и</ta>
            <ta e="T462" id="Seg_7161" s="T461">взять-FUT-3SG</ta>
            <ta e="T464" id="Seg_7162" s="T463">тогда</ta>
            <ta e="T465" id="Seg_7163" s="T464">дать-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_7164" s="T465">грудь.[NOM.SG]</ta>
            <ta e="T468" id="Seg_7165" s="T467">этот-PL-LAT</ta>
            <ta e="T470" id="Seg_7166" s="T469">съесть-FUT-3SG</ta>
            <ta e="T471" id="Seg_7167" s="T470">и</ta>
            <ta e="T472" id="Seg_7168" s="T471">класть-PST.[3SG]</ta>
            <ta e="T474" id="Seg_7169" s="T473">этот.[NOM.SG]</ta>
            <ta e="T475" id="Seg_7170" s="T474">спать-MOM-PST.[3SG]</ta>
            <ta e="T477" id="Seg_7171" s="T476">и</ta>
            <ta e="T478" id="Seg_7172" s="T477">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T479" id="Seg_7173" s="T478">ложиться-PST.[3SG]</ta>
            <ta e="T480" id="Seg_7174" s="T479">спать-MOM-PST.[3SG]</ta>
            <ta e="T503" id="Seg_7175" s="T502">слушать-DUR.PST-3PL</ta>
            <ta e="T504" id="Seg_7176" s="T503">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T506" id="Seg_7177" s="T505">отец-NOM/GEN.3SG</ta>
            <ta e="T507" id="Seg_7178" s="T506">отец-NOM/GEN.3SG</ta>
            <ta e="T508" id="Seg_7179" s="T507">мать-NOM/GEN.3SG</ta>
            <ta e="T510" id="Seg_7180" s="T509">я.NOM</ta>
            <ta e="T511" id="Seg_7181" s="T510">маленький.[NOM.SG]</ta>
            <ta e="T512" id="Seg_7182" s="T511">быть-PST-1SG</ta>
            <ta e="T513" id="Seg_7183" s="T512">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T514" id="Seg_7184" s="T513">сказать-IPFVZ.[3SG]</ta>
            <ta e="T515" id="Seg_7185" s="T514">пойти-EP-IMP.2SG</ta>
            <ta e="T517" id="Seg_7186" s="T516">пускать-FUT-3SG</ta>
            <ta e="T518" id="Seg_7187" s="T517">куда=INDEF</ta>
            <ta e="T520" id="Seg_7188" s="T519">я.NOM</ta>
            <ta e="T521" id="Seg_7189" s="T520">стоять-PRS-1SG</ta>
            <ta e="T522" id="Seg_7190" s="T521">и</ta>
            <ta e="T523" id="Seg_7191" s="T522">пробовать-DUR-1SG</ta>
            <ta e="T524" id="Seg_7192" s="T523">там</ta>
            <ta e="T526" id="Seg_7193" s="T525">а</ta>
            <ta e="T527" id="Seg_7194" s="T526">этот.[NOM.SG]</ta>
            <ta e="T528" id="Seg_7195" s="T527">я.NOM</ta>
            <ta e="T529" id="Seg_7196" s="T528">ты.ACC</ta>
            <ta e="T530" id="Seg_7197" s="T529">бить-FUT-1SG</ta>
            <ta e="T532" id="Seg_7198" s="T531">я.NOM</ta>
            <ta e="T533" id="Seg_7199" s="T532">тогда</ta>
            <ta e="T534" id="Seg_7200" s="T533">бежать-MOM-PST-1SG</ta>
            <ta e="T535" id="Seg_7201" s="T534">и</ta>
            <ta e="T536" id="Seg_7202" s="T535">скоро</ta>
            <ta e="T537" id="Seg_7203" s="T536">прийти-PST-1SG</ta>
            <ta e="T539" id="Seg_7204" s="T538">тогда</ta>
            <ta e="T540" id="Seg_7205" s="T539">этот.[NOM.SG]</ta>
            <ta e="T541" id="Seg_7206" s="T540">что.[NOM.SG]=INDEF</ta>
            <ta e="T542" id="Seg_7207" s="T541">я.LAT</ta>
            <ta e="T543" id="Seg_7208" s="T542">NEG</ta>
            <ta e="T544" id="Seg_7209" s="T543">бить-DUR.PST.[3SG]</ta>
            <ta e="T562" id="Seg_7210" s="T561">бить-PST-3PL</ta>
            <ta e="T564" id="Seg_7211" s="T563">сильно</ta>
            <ta e="T565" id="Seg_7212" s="T564">%%-INS</ta>
            <ta e="T567" id="Seg_7213" s="T566">я.LAT</ta>
            <ta e="T568" id="Seg_7214" s="T567">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T569" id="Seg_7215" s="T568">очень</ta>
            <ta e="T570" id="Seg_7216" s="T569">сильно</ta>
            <ta e="T571" id="Seg_7217" s="T570">бить-PST-3PL</ta>
            <ta e="T575" id="Seg_7218" s="T572">я.NOM</ta>
            <ta e="T576" id="Seg_7219" s="T575">сильно</ta>
            <ta e="T577" id="Seg_7220" s="T576">шуметь-PST-1SG</ta>
            <ta e="T724" id="Seg_7221" s="T722">нажимать-INF.LAT</ta>
            <ta e="T725" id="Seg_7222" s="T724">NEG</ta>
            <ta e="T726" id="Seg_7223" s="T725">мочь-PRS-1SG</ta>
            <ta e="T727" id="Seg_7224" s="T726">а</ta>
            <ta e="T729" id="Seg_7225" s="T728">а</ta>
            <ta e="T730" id="Seg_7226" s="T729">мочь-PRS-1SG</ta>
            <ta e="T731" id="Seg_7227" s="T730">книжка-NOM/GEN/ACC.1SG</ta>
            <ta e="T732" id="Seg_7228" s="T731">смотреть-PRS-1SG</ta>
            <ta e="T733" id="Seg_7229" s="T732">что.[NOM.SG]</ta>
            <ta e="T734" id="Seg_7230" s="T733">там</ta>
            <ta e="T735" id="Seg_7231" s="T734">быть-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_7232" s="T735">писать-DETR-PTCP</ta>
            <ta e="T738" id="Seg_7233" s="T737">а</ta>
            <ta e="T739" id="Seg_7234" s="T738">писать-INF.LAT</ta>
            <ta e="T740" id="Seg_7235" s="T739">NEG</ta>
            <ta e="T741" id="Seg_7236" s="T740">мочь-PRS-1SG</ta>
            <ta e="T771" id="Seg_7237" s="T770">этот-PL</ta>
            <ta e="T772" id="Seg_7238" s="T771">много</ta>
            <ta e="T773" id="Seg_7239" s="T772">NEG</ta>
            <ta e="T774" id="Seg_7240" s="T773">жить-PST.[3SG]</ta>
            <ta e="T775" id="Seg_7241" s="T774">жить-DUR.PST-3PL</ta>
            <ta e="T777" id="Seg_7242" s="T776">этот-PL</ta>
            <ta e="T778" id="Seg_7243" s="T777">всегда</ta>
            <ta e="T779" id="Seg_7244" s="T778">снаружи</ta>
            <ta e="T780" id="Seg_7245" s="T779">снаружи</ta>
            <ta e="T781" id="Seg_7246" s="T780">дом-PL-LAT</ta>
            <ta e="T782" id="Seg_7247" s="T781">NEG.EX-PST-3PL</ta>
            <ta e="T784" id="Seg_7248" s="T783">мерзнуть-FACT-PST-3PL</ta>
            <ta e="T786" id="Seg_7249" s="T785">и</ta>
            <ta e="T787" id="Seg_7250" s="T786">тогда</ta>
            <ta e="T788" id="Seg_7251" s="T787">этот-PL-LAT</ta>
            <ta e="T789" id="Seg_7252" s="T788">вылечить-INF.LAT</ta>
            <ta e="T790" id="Seg_7253" s="T789">NEG.EX-PST.[3SG]</ta>
            <ta e="T792" id="Seg_7254" s="T791">этот-PL</ta>
            <ta e="T793" id="Seg_7255" s="T792">умереть-RES-PST-3PL</ta>
            <ta e="T796" id="Seg_7256" s="T795">мало</ta>
            <ta e="T797" id="Seg_7257" s="T796">жить-DUR-3PL</ta>
            <ta e="T801" id="Seg_7258" s="T800">мы.LAT</ta>
            <ta e="T802" id="Seg_7259" s="T801">белый.[NOM.SG]</ta>
            <ta e="T803" id="Seg_7260" s="T802">дерево.[NOM.SG]</ta>
            <ta e="T804" id="Seg_7261" s="T803">NEG.EX-PST.[3SG]</ta>
            <ta e="T806" id="Seg_7262" s="T805">этот-PL</ta>
            <ta e="T807" id="Seg_7263" s="T806">установить-PST-3PL</ta>
            <ta e="T808" id="Seg_7264" s="T807">четыре.[NOM.SG]</ta>
            <ta e="T809" id="Seg_7265" s="T808">дерево.[NOM.SG]</ta>
            <ta e="T811" id="Seg_7266" s="T810">и</ta>
            <ta e="T812" id="Seg_7267" s="T811">там</ta>
            <ta e="T813" id="Seg_7268" s="T812">класть-PST-3PL</ta>
            <ta e="T815" id="Seg_7269" s="T814">тогда</ta>
            <ta e="T816" id="Seg_7270" s="T815">этот.[NOM.SG]</ta>
            <ta e="T817" id="Seg_7271" s="T816">там</ta>
            <ta e="T818" id="Seg_7272" s="T817">вверх</ta>
            <ta e="T819" id="Seg_7273" s="T818">камень-PL</ta>
            <ta e="T820" id="Seg_7274" s="T819">класть-PST-3PL</ta>
            <ta e="T821" id="Seg_7275" s="T820">тогда</ta>
            <ta e="T822" id="Seg_7276" s="T821">этот.[NOM.SG]</ta>
            <ta e="T823" id="Seg_7277" s="T822">дерево-PL</ta>
            <ta e="T825" id="Seg_7278" s="T824">резать-PST-3PL</ta>
            <ta e="T826" id="Seg_7279" s="T825">этот.[NOM.SG]</ta>
            <ta e="T827" id="Seg_7280" s="T826">упасть-MOM.[3SG]</ta>
            <ta e="T828" id="Seg_7281" s="T827">этот-ACC.PL</ta>
            <ta e="T829" id="Seg_7282" s="T828">нажимать-MOM.[3SG]</ta>
            <ta e="T831" id="Seg_7283" s="T830">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T833" id="Seg_7284" s="T832">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T835" id="Seg_7285" s="T834">убить-DUR.PST-3PL</ta>
            <ta e="T893" id="Seg_7286" s="T892">ну</ta>
            <ta e="T894" id="Seg_7287" s="T893">земля.[NOM.SG]</ta>
            <ta e="T896" id="Seg_7288" s="T895">копать-PST-3PL</ta>
            <ta e="T897" id="Seg_7289" s="T896">тогда</ta>
            <ta e="T898" id="Seg_7290" s="T897">дерево.[NOM.SG]</ta>
            <ta e="T899" id="Seg_7291" s="T898">съесть-PST-3PL</ta>
            <ta e="T900" id="Seg_7292" s="T899">дерево.[NOM.SG]</ta>
            <ta e="T901" id="Seg_7293" s="T900">резать-PST-3PL</ta>
            <ta e="T903" id="Seg_7294" s="T902">тогда</ta>
            <ta e="T904" id="Seg_7295" s="T903">два.[NOM.SG]</ta>
            <ta e="T905" id="Seg_7296" s="T904">делать-PST-3PL</ta>
            <ta e="T906" id="Seg_7297" s="T905">и</ta>
            <ta e="T907" id="Seg_7298" s="T906">там</ta>
            <ta e="T908" id="Seg_7299" s="T907">там</ta>
            <ta e="T909" id="Seg_7300" s="T908">тоже</ta>
            <ta e="T910" id="Seg_7301" s="T909">топор-INS</ta>
            <ta e="T911" id="Seg_7302" s="T910">резать-PST-3PL</ta>
            <ta e="T913" id="Seg_7303" s="T912">тогда</ta>
            <ta e="T914" id="Seg_7304" s="T913">этот-ACC</ta>
            <ta e="T915" id="Seg_7305" s="T914">там</ta>
            <ta e="T916" id="Seg_7306" s="T915">класть-PST-3PL</ta>
            <ta e="T917" id="Seg_7307" s="T916">нести-PST-3PL</ta>
            <ta e="T919" id="Seg_7308" s="T918">класть-PST-3PL</ta>
            <ta e="T920" id="Seg_7309" s="T919">место-LAT</ta>
            <ta e="T922" id="Seg_7310" s="T921">и</ta>
            <ta e="T923" id="Seg_7311" s="T922">крест.[NOM.SG]</ta>
            <ta e="T924" id="Seg_7312" s="T923">делать-PST-3PL</ta>
            <ta e="T926" id="Seg_7313" s="T925">и</ta>
            <ta e="T927" id="Seg_7314" s="T926">тогда</ta>
            <ta e="T928" id="Seg_7315" s="T927">чум-LAT/LOC.3SG</ta>
            <ta e="T930" id="Seg_7316" s="T929">прийти-PST-3PL</ta>
            <ta e="T932" id="Seg_7317" s="T931">кипятить-DUR-3PL</ta>
            <ta e="T933" id="Seg_7318" s="T932">и</ta>
            <ta e="T934" id="Seg_7319" s="T933">тогда</ta>
            <ta e="T935" id="Seg_7320" s="T934">съесть-DUR-3PL</ta>
            <ta e="T937" id="Seg_7321" s="T936">и</ta>
            <ta e="T938" id="Seg_7322" s="T937">PTCL</ta>
            <ta e="T1271" id="Seg_7323" s="T1270">ну</ta>
            <ta e="T1272" id="Seg_7324" s="T1271">когда</ta>
            <ta e="T1273" id="Seg_7325" s="T1272">умереть-RES-FUT-3SG</ta>
            <ta e="T1274" id="Seg_7326" s="T1273">мужчина.[NOM.SG]</ta>
            <ta e="T1275" id="Seg_7327" s="T1274">место-LAT</ta>
            <ta e="T1276" id="Seg_7328" s="T1275">копать-DUR-3PL</ta>
            <ta e="T1278" id="Seg_7329" s="T1277">а</ta>
            <ta e="T1279" id="Seg_7330" s="T1278">там</ta>
            <ta e="T1280" id="Seg_7331" s="T1279">вверх</ta>
            <ta e="T1281" id="Seg_7332" s="T1280">дух.[NOM.SG]</ta>
            <ta e="T1282" id="Seg_7333" s="T1281">там</ta>
            <ta e="T1533" id="Seg_7334" s="T1282">пойти-CVB</ta>
            <ta e="T1283" id="Seg_7335" s="T1533">исчезнуть-FUT-3SG</ta>
            <ta e="T1284" id="Seg_7336" s="T1283">вверх</ta>
            <ta e="T1286" id="Seg_7337" s="T1285">я.NOM</ta>
            <ta e="T1287" id="Seg_7338" s="T1286">так</ta>
            <ta e="T1288" id="Seg_7339" s="T1287">знать-1SG</ta>
            <ta e="T1292" id="Seg_7340" s="T1291">кто.[NOM.SG]</ta>
            <ta e="T1293" id="Seg_7341" s="T1292">как</ta>
            <ta e="T1294" id="Seg_7342" s="T1293">а</ta>
            <ta e="T1295" id="Seg_7343" s="T1294">я.NOM</ta>
            <ta e="T1296" id="Seg_7344" s="T1295">так</ta>
            <ta e="T1297" id="Seg_7345" s="T1296">знать-1SG</ta>
            <ta e="T1299" id="Seg_7346" s="T1298">и</ta>
            <ta e="T1300" id="Seg_7347" s="T1299">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T1301" id="Seg_7348" s="T1300">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1302" id="Seg_7349" s="T1301">так</ta>
            <ta e="T1303" id="Seg_7350" s="T1302">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1304" id="Seg_7351" s="T1303">я.LAT</ta>
            <ta e="T1307" id="Seg_7352" s="T1306">этот-GEN</ta>
            <ta e="T1308" id="Seg_7353" s="T1307">пойти-FUT-3SG</ta>
            <ta e="T1309" id="Seg_7354" s="T1308">бог-LAT</ta>
            <ta e="T1313" id="Seg_7355" s="T1312">мужчина-ACC</ta>
            <ta e="T1314" id="Seg_7356" s="T1313">бог.[NOM.SG]</ta>
            <ta e="T1315" id="Seg_7357" s="T1314">делать-PST.[3SG]</ta>
            <ta e="T1316" id="Seg_7358" s="T1315">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1317" id="Seg_7359" s="T1316">земля-LOC</ta>
            <ta e="T1318" id="Seg_7360" s="T1317">и</ta>
            <ta e="T1319" id="Seg_7361" s="T1318">этот-LAT</ta>
            <ta e="T1320" id="Seg_7362" s="T1319">PTCL</ta>
            <ta e="T1322" id="Seg_7363" s="T1321">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1323" id="Seg_7364" s="T1322">дух-LAT</ta>
            <ta e="T1325" id="Seg_7365" s="T1324">дать-PST.[3SG]</ta>
            <ta e="T1326" id="Seg_7366" s="T1325">и</ta>
            <ta e="T1327" id="Seg_7367" s="T1326">этот.[NOM.SG]</ta>
            <ta e="T1328" id="Seg_7368" s="T1327">встать-PST.[3SG]</ta>
            <ta e="T1329" id="Seg_7369" s="T1328">мужчина.[NOM.SG]</ta>
            <ta e="T1330" id="Seg_7370" s="T1329">стать-RES-PST.[3SG]</ta>
            <ta e="T1452" id="Seg_7371" s="T1451">ну</ta>
            <ta e="T1453" id="Seg_7372" s="T1452">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T54" id="Seg_7373" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_7374" s="T54">conj</ta>
            <ta e="T56" id="Seg_7375" s="T55">adv</ta>
            <ta e="T57" id="Seg_7376" s="T56">que</ta>
            <ta e="T58" id="Seg_7377" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_7378" s="T58">pers</ta>
            <ta e="T60" id="Seg_7379" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_7380" s="T60">pers</ta>
            <ta e="T62" id="Seg_7381" s="T61">dempro-n:case</ta>
            <ta e="T64" id="Seg_7382" s="T63">conj</ta>
            <ta e="T65" id="Seg_7383" s="T64">n-n:num</ta>
            <ta e="T66" id="Seg_7384" s="T65">quant</ta>
            <ta e="T67" id="Seg_7385" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_7386" s="T67">que</ta>
            <ta e="T70" id="Seg_7387" s="T69">pers</ta>
            <ta e="T71" id="Seg_7388" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_7389" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_7390" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_7391" s="T73">n-n:num</ta>
            <ta e="T76" id="Seg_7392" s="T75">pers</ta>
            <ta e="T77" id="Seg_7393" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_7394" s="T77">adv</ta>
            <ta e="T79" id="Seg_7395" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_7396" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_7397" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_7398" s="T81">adv</ta>
            <ta e="T83" id="Seg_7399" s="T82">n-n:case.poss</ta>
            <ta e="T84" id="Seg_7400" s="T83">v-v:tense.[v:pn]</ta>
            <ta e="T85" id="Seg_7401" s="T84">conj</ta>
            <ta e="T86" id="Seg_7402" s="T85">pers</ta>
            <ta e="T87" id="Seg_7403" s="T86">pers</ta>
            <ta e="T88" id="Seg_7404" s="T87">adv</ta>
            <ta e="T89" id="Seg_7405" s="T88">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_7406" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_7407" s="T106">n-n:case.poss</ta>
            <ta e="T108" id="Seg_7408" s="T107">n-n:case.poss</ta>
            <ta e="T109" id="Seg_7409" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_7410" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_7411" s="T110">n-n:case.poss</ta>
            <ta e="T112" id="Seg_7412" s="T111">adv</ta>
            <ta e="T113" id="Seg_7413" s="T112">dempro.[n:case]</ta>
            <ta e="T114" id="Seg_7414" s="T113">dempro-n:case</ta>
            <ta e="T115" id="Seg_7415" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_7416" s="T115">v-v:ins-v:mood.pn</ta>
            <ta e="T117" id="Seg_7417" s="T116">pers</ta>
            <ta e="T118" id="Seg_7418" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_7419" s="T118">n-n:case</ta>
            <ta e="T121" id="Seg_7420" s="T120">n-n:case.poss</ta>
            <ta e="T122" id="Seg_7421" s="T121">n-n:case.poss</ta>
            <ta e="T123" id="Seg_7422" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_7423" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_7424" s="T124">n-n:case.poss</ta>
            <ta e="T126" id="Seg_7425" s="T125">v-v:n.fin</ta>
            <ta e="T127" id="Seg_7426" s="T126">adv</ta>
            <ta e="T128" id="Seg_7427" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_7428" s="T128">n.[n:case]</ta>
            <ta e="T130" id="Seg_7429" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_7430" s="T130">n-n:case</ta>
            <ta e="T133" id="Seg_7431" s="T132">dempro.[n:case]</ta>
            <ta e="T134" id="Seg_7432" s="T133">v-v&gt;v.[v:pn]</ta>
            <ta e="T135" id="Seg_7433" s="T134">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_7434" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_7435" s="T137">adv</ta>
            <ta e="T139" id="Seg_7436" s="T138">n.[n:case]</ta>
            <ta e="T140" id="Seg_7437" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_7438" s="T140">n.[n:case]</ta>
            <ta e="T143" id="Seg_7439" s="T142">v-v&gt;v-v:pn</ta>
            <ta e="T144" id="Seg_7440" s="T143">v-v&gt;v-v:pn</ta>
            <ta e="T145" id="Seg_7441" s="T144">adv</ta>
            <ta e="T146" id="Seg_7442" s="T145">v-v:tense.[v:pn]</ta>
            <ta e="T147" id="Seg_7443" s="T146">v-v&gt;v-v:pn</ta>
            <ta e="T148" id="Seg_7444" s="T147">n-n:case</ta>
            <ta e="T150" id="Seg_7445" s="T149">dempro-n:case</ta>
            <ta e="T152" id="Seg_7446" s="T151">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_7447" s="T152">conj</ta>
            <ta e="T154" id="Seg_7448" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_7449" s="T154">n-n:case.poss</ta>
            <ta e="T174" id="Seg_7450" s="T173">n-n:case.poss</ta>
            <ta e="T175" id="Seg_7451" s="T174">conj</ta>
            <ta e="T176" id="Seg_7452" s="T175">n-n:case.poss</ta>
            <ta e="T177" id="Seg_7453" s="T176">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_7454" s="T178">adv</ta>
            <ta e="T180" id="Seg_7455" s="T179">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_7456" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_7457" s="T182">quant</ta>
            <ta e="T184" id="Seg_7458" s="T183">v-v:tense.[v:pn]</ta>
            <ta e="T185" id="Seg_7459" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_7460" s="T185">que.[n:case]</ta>
            <ta e="T187" id="Seg_7461" s="T186">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_7462" s="T188">n-n:num</ta>
            <ta e="T190" id="Seg_7463" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_7464" s="T190">n.[n:case]</ta>
            <ta e="T192" id="Seg_7465" s="T191">quant</ta>
            <ta e="T194" id="Seg_7466" s="T193">adv</ta>
            <ta e="T196" id="Seg_7467" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_7468" s="T196">n-n:case.poss</ta>
            <ta e="T198" id="Seg_7469" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_7470" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_7471" s="T199">conj</ta>
            <ta e="T1531" id="Seg_7472" s="T200">v-v:n-fin</ta>
            <ta e="T201" id="Seg_7473" s="T1531">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_7474" s="T202">n-n:num</ta>
            <ta e="T204" id="Seg_7475" s="T203">n.[n:case]</ta>
            <ta e="T205" id="Seg_7476" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_7477" s="T205">n.[n:case]</ta>
            <ta e="T207" id="Seg_7478" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_7479" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_7480" s="T208">n-n:case</ta>
            <ta e="T211" id="Seg_7481" s="T210">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_7482" s="T212">adv</ta>
            <ta e="T214" id="Seg_7483" s="T213">ptcl</ta>
            <ta e="T1532" id="Seg_7484" s="T214">v-v:n-fin</ta>
            <ta e="T215" id="Seg_7485" s="T1532">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_7486" s="T247">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_7487" s="T248">adj</ta>
            <ta e="T250" id="Seg_7488" s="T249">conj</ta>
            <ta e="T251" id="Seg_7489" s="T250">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_7490" s="T252">n.[n:case]</ta>
            <ta e="T254" id="Seg_7491" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_7492" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_7493" s="T255">v-v&gt;v-v:pn</ta>
            <ta e="T257" id="Seg_7494" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_7495" s="T257">n-n:num-n:case.poss</ta>
            <ta e="T259" id="Seg_7496" s="T258">num.[n:case]</ta>
            <ta e="T260" id="Seg_7497" s="T259">num-n:case</ta>
            <ta e="T261" id="Seg_7498" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_7499" s="T261">conj</ta>
            <ta e="T263" id="Seg_7500" s="T262">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_7501" s="T371">que</ta>
            <ta e="T373" id="Seg_7502" s="T372">num.[n:case]</ta>
            <ta e="T374" id="Seg_7503" s="T373">v-v:tense.[v:pn]</ta>
            <ta e="T375" id="Seg_7504" s="T374">que</ta>
            <ta e="T376" id="Seg_7505" s="T375">num.[n:case]</ta>
            <ta e="T377" id="Seg_7506" s="T376">v-v:tense.[v:pn]</ta>
            <ta e="T378" id="Seg_7507" s="T377">n-n:num</ta>
            <ta e="T381" id="Seg_7508" s="T379">quant</ta>
            <ta e="T383" id="Seg_7509" s="T381">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_7510" s="T383">dempro-n:num</ta>
            <ta e="T385" id="Seg_7511" s="T384">que.[n:case]=ptcl</ta>
            <ta e="T386" id="Seg_7512" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7513" s="T386">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_7514" s="T388">adv</ta>
            <ta e="T390" id="Seg_7515" s="T389">n.[n:case]</ta>
            <ta e="T391" id="Seg_7516" s="T390">n-n:num</ta>
            <ta e="T392" id="Seg_7517" s="T391">v-v&gt;v-v:pn</ta>
            <ta e="T393" id="Seg_7518" s="T392">n-n:case</ta>
            <ta e="T394" id="Seg_7519" s="T393">v-v:tense-v:pn</ta>
            <ta e="T395" id="Seg_7520" s="T394">conj</ta>
            <ta e="T396" id="Seg_7521" s="T395">que.[n:case]=ptcl</ta>
            <ta e="T397" id="Seg_7522" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_7523" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_7524" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_7525" s="T399">n-n:num</ta>
            <ta e="T404" id="Seg_7526" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_7527" s="T404">n-n:num-n:case</ta>
            <ta e="T406" id="Seg_7528" s="T405">dempro-n:num</ta>
            <ta e="T407" id="Seg_7529" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_7530" s="T407">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_7531" s="T408">adv</ta>
            <ta e="T410" id="Seg_7532" s="T409">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_7533" s="T433">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_7534" s="T437">n-n:num</ta>
            <ta e="T441" id="Seg_7535" s="T440">conj</ta>
            <ta e="T444" id="Seg_7536" s="T443">v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_7537" s="T458">n-n:case.poss</ta>
            <ta e="T460" id="Seg_7538" s="T459">v-v:tense.[v:pn]</ta>
            <ta e="T461" id="Seg_7539" s="T460">conj</ta>
            <ta e="T462" id="Seg_7540" s="T461">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_7541" s="T463">adv</ta>
            <ta e="T465" id="Seg_7542" s="T464">v-v:tense.[v:pn]</ta>
            <ta e="T466" id="Seg_7543" s="T465">n.[n:case]</ta>
            <ta e="T468" id="Seg_7544" s="T467">dempro-n:num-n:case</ta>
            <ta e="T470" id="Seg_7545" s="T469">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_7546" s="T470">conj</ta>
            <ta e="T472" id="Seg_7547" s="T471">v-v:tense.[v:pn]</ta>
            <ta e="T474" id="Seg_7548" s="T473">dempro.[n:case]</ta>
            <ta e="T475" id="Seg_7549" s="T474">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T477" id="Seg_7550" s="T476">conj</ta>
            <ta e="T478" id="Seg_7551" s="T477">refl-n:case.poss</ta>
            <ta e="T479" id="Seg_7552" s="T478">v-v:tense.[v:pn]</ta>
            <ta e="T480" id="Seg_7553" s="T479">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T503" id="Seg_7554" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_7555" s="T503">n-n:case.poss</ta>
            <ta e="T506" id="Seg_7556" s="T505">n-n:case.poss</ta>
            <ta e="T507" id="Seg_7557" s="T506">n-n:case.poss</ta>
            <ta e="T508" id="Seg_7558" s="T507">n-n:case.poss</ta>
            <ta e="T510" id="Seg_7559" s="T509">pers</ta>
            <ta e="T511" id="Seg_7560" s="T510">adj.[n:case]</ta>
            <ta e="T512" id="Seg_7561" s="T511">v-v:tense-v:pn</ta>
            <ta e="T513" id="Seg_7562" s="T512">n-n:case.poss</ta>
            <ta e="T514" id="Seg_7563" s="T513">v-v&gt;v.[v:pn]</ta>
            <ta e="T515" id="Seg_7564" s="T514">v-v:ins-v:mood.pn</ta>
            <ta e="T517" id="Seg_7565" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_7566" s="T517">que=ptcl</ta>
            <ta e="T520" id="Seg_7567" s="T519">pers</ta>
            <ta e="T521" id="Seg_7568" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_7569" s="T521">conj</ta>
            <ta e="T523" id="Seg_7570" s="T522">v-v&gt;v-v:pn</ta>
            <ta e="T524" id="Seg_7571" s="T523">adv</ta>
            <ta e="T526" id="Seg_7572" s="T525">conj</ta>
            <ta e="T527" id="Seg_7573" s="T526">dempro.[n:case]</ta>
            <ta e="T528" id="Seg_7574" s="T527">pers</ta>
            <ta e="T529" id="Seg_7575" s="T528">pers</ta>
            <ta e="T530" id="Seg_7576" s="T529">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_7577" s="T531">pers</ta>
            <ta e="T533" id="Seg_7578" s="T532">adv</ta>
            <ta e="T534" id="Seg_7579" s="T533">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T535" id="Seg_7580" s="T534">conj</ta>
            <ta e="T536" id="Seg_7581" s="T535">adv</ta>
            <ta e="T537" id="Seg_7582" s="T536">v-v:tense-v:pn</ta>
            <ta e="T539" id="Seg_7583" s="T538">adv</ta>
            <ta e="T540" id="Seg_7584" s="T539">dempro.[n:case]</ta>
            <ta e="T541" id="Seg_7585" s="T540">que.[n:case]=ptcl</ta>
            <ta e="T542" id="Seg_7586" s="T541">pers</ta>
            <ta e="T543" id="Seg_7587" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_7588" s="T543">v-v:tense.[v:pn]</ta>
            <ta e="T562" id="Seg_7589" s="T561">v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_7590" s="T563">adv</ta>
            <ta e="T565" id="Seg_7591" s="T564">n-n:case</ta>
            <ta e="T567" id="Seg_7592" s="T566">pers</ta>
            <ta e="T568" id="Seg_7593" s="T567">n-n:case.poss</ta>
            <ta e="T569" id="Seg_7594" s="T568">adv</ta>
            <ta e="T570" id="Seg_7595" s="T569">adv</ta>
            <ta e="T571" id="Seg_7596" s="T570">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_7597" s="T572">pers</ta>
            <ta e="T576" id="Seg_7598" s="T575">adv</ta>
            <ta e="T577" id="Seg_7599" s="T576">v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_7600" s="T722">v-v:n.fin</ta>
            <ta e="T725" id="Seg_7601" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_7602" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_7603" s="T726">conj</ta>
            <ta e="T729" id="Seg_7604" s="T728">conj</ta>
            <ta e="T730" id="Seg_7605" s="T729">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_7606" s="T730">n-n:case.poss</ta>
            <ta e="T732" id="Seg_7607" s="T731">v-v:tense-v:pn</ta>
            <ta e="T733" id="Seg_7608" s="T732">que.[n:case]</ta>
            <ta e="T734" id="Seg_7609" s="T733">adv</ta>
            <ta e="T735" id="Seg_7610" s="T734">v-v:tense.[v:pn]</ta>
            <ta e="T736" id="Seg_7611" s="T735">v-v&gt;v-v:n.fin</ta>
            <ta e="T738" id="Seg_7612" s="T737">conj</ta>
            <ta e="T739" id="Seg_7613" s="T738">v-v:n.fin</ta>
            <ta e="T740" id="Seg_7614" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_7615" s="T740">v-v:tense-v:pn</ta>
            <ta e="T771" id="Seg_7616" s="T770">dempro-n:num</ta>
            <ta e="T772" id="Seg_7617" s="T771">quant</ta>
            <ta e="T773" id="Seg_7618" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_7619" s="T773">v-v:tense.[v:pn]</ta>
            <ta e="T775" id="Seg_7620" s="T774">v-v:tense-v:pn</ta>
            <ta e="T777" id="Seg_7621" s="T776">dempro-n:num</ta>
            <ta e="T778" id="Seg_7622" s="T777">adv</ta>
            <ta e="T779" id="Seg_7623" s="T778">adv</ta>
            <ta e="T780" id="Seg_7624" s="T779">adv</ta>
            <ta e="T781" id="Seg_7625" s="T780">n-n:num-n:case</ta>
            <ta e="T782" id="Seg_7626" s="T781">v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_7627" s="T783">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T786" id="Seg_7628" s="T785">conj</ta>
            <ta e="T787" id="Seg_7629" s="T786">adv</ta>
            <ta e="T788" id="Seg_7630" s="T787">dempro-n:num-n:case</ta>
            <ta e="T789" id="Seg_7631" s="T788">v-v:n.fin</ta>
            <ta e="T790" id="Seg_7632" s="T789">v-v:tense.[v:pn]</ta>
            <ta e="T792" id="Seg_7633" s="T791">dempro-n:num</ta>
            <ta e="T793" id="Seg_7634" s="T792">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T796" id="Seg_7635" s="T795">adv</ta>
            <ta e="T797" id="Seg_7636" s="T796">v-v&gt;v-v:pn</ta>
            <ta e="T801" id="Seg_7637" s="T800">pers</ta>
            <ta e="T802" id="Seg_7638" s="T801">adj.[n:case]</ta>
            <ta e="T803" id="Seg_7639" s="T802">n.[n:case]</ta>
            <ta e="T804" id="Seg_7640" s="T803">v-v:tense.[v:pn]</ta>
            <ta e="T806" id="Seg_7641" s="T805">dempro-n:num</ta>
            <ta e="T807" id="Seg_7642" s="T806">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_7643" s="T807">num.[n:case]</ta>
            <ta e="T809" id="Seg_7644" s="T808">n.[n:case]</ta>
            <ta e="T811" id="Seg_7645" s="T810">conj</ta>
            <ta e="T812" id="Seg_7646" s="T811">adv</ta>
            <ta e="T813" id="Seg_7647" s="T812">v-v:tense-v:pn</ta>
            <ta e="T815" id="Seg_7648" s="T814">adv</ta>
            <ta e="T816" id="Seg_7649" s="T815">dempro.[n:case]</ta>
            <ta e="T817" id="Seg_7650" s="T816">adv</ta>
            <ta e="T818" id="Seg_7651" s="T817">adv</ta>
            <ta e="T819" id="Seg_7652" s="T818">n-n:num</ta>
            <ta e="T820" id="Seg_7653" s="T819">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_7654" s="T820">adv</ta>
            <ta e="T822" id="Seg_7655" s="T821">dempro.[n:case]</ta>
            <ta e="T823" id="Seg_7656" s="T822">n-n:num</ta>
            <ta e="T825" id="Seg_7657" s="T824">v-v:tense-v:pn</ta>
            <ta e="T826" id="Seg_7658" s="T825">dempro.[n:case]</ta>
            <ta e="T827" id="Seg_7659" s="T826">v-v&gt;v.[v:pn]</ta>
            <ta e="T828" id="Seg_7660" s="T827">dempro-n:case</ta>
            <ta e="T829" id="Seg_7661" s="T828">v-v&gt;v.[v:pn]</ta>
            <ta e="T831" id="Seg_7662" s="T830">refl-n:case.poss</ta>
            <ta e="T833" id="Seg_7663" s="T832">refl-n:case.poss</ta>
            <ta e="T835" id="Seg_7664" s="T834">v-v:tense-v:pn</ta>
            <ta e="T893" id="Seg_7665" s="T892">ptcl</ta>
            <ta e="T894" id="Seg_7666" s="T893">n.[n:case]</ta>
            <ta e="T896" id="Seg_7667" s="T895">v-v:tense-v:pn</ta>
            <ta e="T897" id="Seg_7668" s="T896">adv</ta>
            <ta e="T898" id="Seg_7669" s="T897">n.[n:case]</ta>
            <ta e="T899" id="Seg_7670" s="T898">v-v:tense-v:pn</ta>
            <ta e="T900" id="Seg_7671" s="T899">n.[n:case]</ta>
            <ta e="T901" id="Seg_7672" s="T900">v-v:tense-v:pn</ta>
            <ta e="T903" id="Seg_7673" s="T902">adv</ta>
            <ta e="T904" id="Seg_7674" s="T903">num.[n:case]</ta>
            <ta e="T905" id="Seg_7675" s="T904">v-v:tense-v:pn</ta>
            <ta e="T906" id="Seg_7676" s="T905">conj</ta>
            <ta e="T907" id="Seg_7677" s="T906">adv</ta>
            <ta e="T908" id="Seg_7678" s="T907">adv</ta>
            <ta e="T909" id="Seg_7679" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_7680" s="T909">n-n:case</ta>
            <ta e="T911" id="Seg_7681" s="T910">v-v:tense-v:pn</ta>
            <ta e="T913" id="Seg_7682" s="T912">adv</ta>
            <ta e="T914" id="Seg_7683" s="T913">dempro-n:case</ta>
            <ta e="T915" id="Seg_7684" s="T914">adv</ta>
            <ta e="T916" id="Seg_7685" s="T915">v-v:tense-v:pn</ta>
            <ta e="T917" id="Seg_7686" s="T916">v-v:tense-v:pn</ta>
            <ta e="T919" id="Seg_7687" s="T918">v-v:tense-v:pn</ta>
            <ta e="T920" id="Seg_7688" s="T919">n-n:case</ta>
            <ta e="T922" id="Seg_7689" s="T921">conj</ta>
            <ta e="T923" id="Seg_7690" s="T922">n.[n:case]</ta>
            <ta e="T924" id="Seg_7691" s="T923">v-v:tense-v:pn</ta>
            <ta e="T926" id="Seg_7692" s="T925">conj</ta>
            <ta e="T927" id="Seg_7693" s="T926">adv</ta>
            <ta e="T928" id="Seg_7694" s="T927">n-n:case.poss</ta>
            <ta e="T930" id="Seg_7695" s="T929">v-v:tense-v:pn</ta>
            <ta e="T932" id="Seg_7696" s="T931">v-v&gt;v-v:pn</ta>
            <ta e="T933" id="Seg_7697" s="T932">conj</ta>
            <ta e="T934" id="Seg_7698" s="T933">adv</ta>
            <ta e="T935" id="Seg_7699" s="T934">v-v&gt;v-v:pn</ta>
            <ta e="T937" id="Seg_7700" s="T936">conj</ta>
            <ta e="T938" id="Seg_7701" s="T937">ptcl</ta>
            <ta e="T1271" id="Seg_7702" s="T1270">ptcl</ta>
            <ta e="T1272" id="Seg_7703" s="T1271">que</ta>
            <ta e="T1273" id="Seg_7704" s="T1272">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1274" id="Seg_7705" s="T1273">n.[n:case]</ta>
            <ta e="T1275" id="Seg_7706" s="T1274">n-n:case</ta>
            <ta e="T1276" id="Seg_7707" s="T1275">v-v&gt;v-v:pn</ta>
            <ta e="T1278" id="Seg_7708" s="T1277">conj</ta>
            <ta e="T1279" id="Seg_7709" s="T1278">adv</ta>
            <ta e="T1280" id="Seg_7710" s="T1279">adv</ta>
            <ta e="T1281" id="Seg_7711" s="T1280">n.[n:case]</ta>
            <ta e="T1282" id="Seg_7712" s="T1281">adv</ta>
            <ta e="T1533" id="Seg_7713" s="T1282">v-v:n-fin</ta>
            <ta e="T1283" id="Seg_7714" s="T1533">v-v:tense-v:pn</ta>
            <ta e="T1284" id="Seg_7715" s="T1283">adv</ta>
            <ta e="T1286" id="Seg_7716" s="T1285">pers</ta>
            <ta e="T1287" id="Seg_7717" s="T1286">ptcl</ta>
            <ta e="T1288" id="Seg_7718" s="T1287">v-v:pn</ta>
            <ta e="T1292" id="Seg_7719" s="T1291">que</ta>
            <ta e="T1293" id="Seg_7720" s="T1292">que</ta>
            <ta e="T1294" id="Seg_7721" s="T1293">conj</ta>
            <ta e="T1295" id="Seg_7722" s="T1294">pers</ta>
            <ta e="T1296" id="Seg_7723" s="T1295">ptcl</ta>
            <ta e="T1297" id="Seg_7724" s="T1296">v-v:pn</ta>
            <ta e="T1299" id="Seg_7725" s="T1298">conj</ta>
            <ta e="T1300" id="Seg_7726" s="T1299">n-n:case.poss</ta>
            <ta e="T1301" id="Seg_7727" s="T1300">v-v&gt;v.[v:pn]</ta>
            <ta e="T1302" id="Seg_7728" s="T1301">ptcl</ta>
            <ta e="T1303" id="Seg_7729" s="T1302">v-v&gt;v.[v:pn]</ta>
            <ta e="T1304" id="Seg_7730" s="T1303">pers</ta>
            <ta e="T1307" id="Seg_7731" s="T1306">dempro-n:case</ta>
            <ta e="T1308" id="Seg_7732" s="T1307">v-v:tense-v:pn</ta>
            <ta e="T1309" id="Seg_7733" s="T1308">n-n:case</ta>
            <ta e="T1313" id="Seg_7734" s="T1312">n-n:case</ta>
            <ta e="T1314" id="Seg_7735" s="T1313">n.[n:case]</ta>
            <ta e="T1315" id="Seg_7736" s="T1314">v-v:tense.[v:pn]</ta>
            <ta e="T1316" id="Seg_7737" s="T1315">refl-n:case.poss</ta>
            <ta e="T1317" id="Seg_7738" s="T1316">n-n:case</ta>
            <ta e="T1318" id="Seg_7739" s="T1317">conj</ta>
            <ta e="T1319" id="Seg_7740" s="T1318">dempro-n:case</ta>
            <ta e="T1320" id="Seg_7741" s="T1319">ptcl</ta>
            <ta e="T1322" id="Seg_7742" s="T1321">refl-n:case.poss</ta>
            <ta e="T1323" id="Seg_7743" s="T1322">n-n:case</ta>
            <ta e="T1325" id="Seg_7744" s="T1324">v-v:tense.[v:pn]</ta>
            <ta e="T1326" id="Seg_7745" s="T1325">conj</ta>
            <ta e="T1327" id="Seg_7746" s="T1326">dempro.[n:case]</ta>
            <ta e="T1328" id="Seg_7747" s="T1327">v-v:tense.[v:pn]</ta>
            <ta e="T1329" id="Seg_7748" s="T1328">n.[n:case]</ta>
            <ta e="T1330" id="Seg_7749" s="T1329">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1452" id="Seg_7750" s="T1451">ptcl</ta>
            <ta e="T1453" id="Seg_7751" s="T1452">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T54" id="Seg_7752" s="T53">v</ta>
            <ta e="T55" id="Seg_7753" s="T54">conj</ta>
            <ta e="T56" id="Seg_7754" s="T55">adv</ta>
            <ta e="T57" id="Seg_7755" s="T56">que</ta>
            <ta e="T58" id="Seg_7756" s="T57">n</ta>
            <ta e="T59" id="Seg_7757" s="T58">pers</ta>
            <ta e="T60" id="Seg_7758" s="T59">v</ta>
            <ta e="T61" id="Seg_7759" s="T60">pers</ta>
            <ta e="T62" id="Seg_7760" s="T61">dempro</ta>
            <ta e="T64" id="Seg_7761" s="T63">conj</ta>
            <ta e="T65" id="Seg_7762" s="T64">n</ta>
            <ta e="T66" id="Seg_7763" s="T65">quant</ta>
            <ta e="T67" id="Seg_7764" s="T66">v</ta>
            <ta e="T68" id="Seg_7765" s="T67">que</ta>
            <ta e="T70" id="Seg_7766" s="T69">pers</ta>
            <ta e="T71" id="Seg_7767" s="T70">v</ta>
            <ta e="T72" id="Seg_7768" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_7769" s="T72">v</ta>
            <ta e="T74" id="Seg_7770" s="T73">n</ta>
            <ta e="T76" id="Seg_7771" s="T75">pers</ta>
            <ta e="T77" id="Seg_7772" s="T76">v</ta>
            <ta e="T78" id="Seg_7773" s="T77">adv</ta>
            <ta e="T79" id="Seg_7774" s="T78">propr</ta>
            <ta e="T80" id="Seg_7775" s="T79">n</ta>
            <ta e="T81" id="Seg_7776" s="T80">v</ta>
            <ta e="T82" id="Seg_7777" s="T81">adv</ta>
            <ta e="T83" id="Seg_7778" s="T82">n</ta>
            <ta e="T84" id="Seg_7779" s="T83">v</ta>
            <ta e="T85" id="Seg_7780" s="T84">conj</ta>
            <ta e="T86" id="Seg_7781" s="T85">pers</ta>
            <ta e="T87" id="Seg_7782" s="T86">pers</ta>
            <ta e="T88" id="Seg_7783" s="T87">adv</ta>
            <ta e="T89" id="Seg_7784" s="T88">v</ta>
            <ta e="T106" id="Seg_7785" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_7786" s="T106">n</ta>
            <ta e="T108" id="Seg_7787" s="T107">n</ta>
            <ta e="T109" id="Seg_7788" s="T108">v</ta>
            <ta e="T110" id="Seg_7789" s="T109">n</ta>
            <ta e="T111" id="Seg_7790" s="T110">n</ta>
            <ta e="T112" id="Seg_7791" s="T111">adv</ta>
            <ta e="T113" id="Seg_7792" s="T112">dempro</ta>
            <ta e="T114" id="Seg_7793" s="T113">dempro</ta>
            <ta e="T115" id="Seg_7794" s="T114">v</ta>
            <ta e="T116" id="Seg_7795" s="T115">v</ta>
            <ta e="T117" id="Seg_7796" s="T116">pers</ta>
            <ta e="T118" id="Seg_7797" s="T117">n</ta>
            <ta e="T119" id="Seg_7798" s="T118">n</ta>
            <ta e="T121" id="Seg_7799" s="T120">n</ta>
            <ta e="T122" id="Seg_7800" s="T121">n</ta>
            <ta e="T123" id="Seg_7801" s="T122">v</ta>
            <ta e="T124" id="Seg_7802" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_7803" s="T124">n</ta>
            <ta e="T126" id="Seg_7804" s="T125">v</ta>
            <ta e="T127" id="Seg_7805" s="T126">adv</ta>
            <ta e="T128" id="Seg_7806" s="T127">v</ta>
            <ta e="T129" id="Seg_7807" s="T128">n</ta>
            <ta e="T130" id="Seg_7808" s="T129">v</ta>
            <ta e="T131" id="Seg_7809" s="T130">n</ta>
            <ta e="T133" id="Seg_7810" s="T132">dempro</ta>
            <ta e="T134" id="Seg_7811" s="T133">v</ta>
            <ta e="T135" id="Seg_7812" s="T134">v</ta>
            <ta e="T137" id="Seg_7813" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_7814" s="T137">adv</ta>
            <ta e="T139" id="Seg_7815" s="T138">n</ta>
            <ta e="T140" id="Seg_7816" s="T139">v</ta>
            <ta e="T141" id="Seg_7817" s="T140">n</ta>
            <ta e="T143" id="Seg_7818" s="T142">v</ta>
            <ta e="T144" id="Seg_7819" s="T143">v</ta>
            <ta e="T145" id="Seg_7820" s="T144">adv</ta>
            <ta e="T146" id="Seg_7821" s="T145">v</ta>
            <ta e="T147" id="Seg_7822" s="T146">v</ta>
            <ta e="T148" id="Seg_7823" s="T147">n</ta>
            <ta e="T150" id="Seg_7824" s="T149">dempro</ta>
            <ta e="T152" id="Seg_7825" s="T151">v</ta>
            <ta e="T153" id="Seg_7826" s="T152">conj</ta>
            <ta e="T154" id="Seg_7827" s="T153">v</ta>
            <ta e="T155" id="Seg_7828" s="T154">n</ta>
            <ta e="T174" id="Seg_7829" s="T173">n</ta>
            <ta e="T175" id="Seg_7830" s="T174">conj</ta>
            <ta e="T176" id="Seg_7831" s="T175">n</ta>
            <ta e="T177" id="Seg_7832" s="T176">v</ta>
            <ta e="T179" id="Seg_7833" s="T178">adv</ta>
            <ta e="T180" id="Seg_7834" s="T179">v</ta>
            <ta e="T182" id="Seg_7835" s="T181">n</ta>
            <ta e="T183" id="Seg_7836" s="T182">quant</ta>
            <ta e="T184" id="Seg_7837" s="T183">v</ta>
            <ta e="T185" id="Seg_7838" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_7839" s="T185">que</ta>
            <ta e="T187" id="Seg_7840" s="T186">v</ta>
            <ta e="T189" id="Seg_7841" s="T188">n</ta>
            <ta e="T190" id="Seg_7842" s="T189">v</ta>
            <ta e="T191" id="Seg_7843" s="T190">n</ta>
            <ta e="T192" id="Seg_7844" s="T191">quant</ta>
            <ta e="T194" id="Seg_7845" s="T193">adv</ta>
            <ta e="T196" id="Seg_7846" s="T195">v</ta>
            <ta e="T197" id="Seg_7847" s="T196">n</ta>
            <ta e="T198" id="Seg_7848" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_7849" s="T198">v</ta>
            <ta e="T200" id="Seg_7850" s="T199">conj</ta>
            <ta e="T1531" id="Seg_7851" s="T200">v</ta>
            <ta e="T201" id="Seg_7852" s="T1531">v</ta>
            <ta e="T203" id="Seg_7853" s="T202">n</ta>
            <ta e="T204" id="Seg_7854" s="T203">n</ta>
            <ta e="T205" id="Seg_7855" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_7856" s="T205">n</ta>
            <ta e="T207" id="Seg_7857" s="T206">v</ta>
            <ta e="T208" id="Seg_7858" s="T207">v</ta>
            <ta e="T209" id="Seg_7859" s="T208">n</ta>
            <ta e="T211" id="Seg_7860" s="T210">v</ta>
            <ta e="T213" id="Seg_7861" s="T212">adv</ta>
            <ta e="T214" id="Seg_7862" s="T213">ptcl</ta>
            <ta e="T1532" id="Seg_7863" s="T214">v</ta>
            <ta e="T215" id="Seg_7864" s="T1532">v</ta>
            <ta e="T248" id="Seg_7865" s="T247">v</ta>
            <ta e="T249" id="Seg_7866" s="T248">adj</ta>
            <ta e="T250" id="Seg_7867" s="T249">conj</ta>
            <ta e="T251" id="Seg_7868" s="T250">v</ta>
            <ta e="T253" id="Seg_7869" s="T252">n</ta>
            <ta e="T254" id="Seg_7870" s="T253">v</ta>
            <ta e="T255" id="Seg_7871" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_7872" s="T255">v</ta>
            <ta e="T257" id="Seg_7873" s="T256">n</ta>
            <ta e="T258" id="Seg_7874" s="T257">n</ta>
            <ta e="T259" id="Seg_7875" s="T258">num</ta>
            <ta e="T260" id="Seg_7876" s="T259">num</ta>
            <ta e="T261" id="Seg_7877" s="T260">v</ta>
            <ta e="T262" id="Seg_7878" s="T261">conj</ta>
            <ta e="T263" id="Seg_7879" s="T262">v</ta>
            <ta e="T372" id="Seg_7880" s="T371">que</ta>
            <ta e="T373" id="Seg_7881" s="T372">num</ta>
            <ta e="T374" id="Seg_7882" s="T373">v</ta>
            <ta e="T375" id="Seg_7883" s="T374">que</ta>
            <ta e="T376" id="Seg_7884" s="T375">num</ta>
            <ta e="T377" id="Seg_7885" s="T376">v</ta>
            <ta e="T378" id="Seg_7886" s="T377">n</ta>
            <ta e="T381" id="Seg_7887" s="T379">quant</ta>
            <ta e="T383" id="Seg_7888" s="T381">v</ta>
            <ta e="T384" id="Seg_7889" s="T383">dempro</ta>
            <ta e="T385" id="Seg_7890" s="T384">que</ta>
            <ta e="T386" id="Seg_7891" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7892" s="T386">v</ta>
            <ta e="T389" id="Seg_7893" s="T388">adv</ta>
            <ta e="T390" id="Seg_7894" s="T389">n</ta>
            <ta e="T391" id="Seg_7895" s="T390">n</ta>
            <ta e="T392" id="Seg_7896" s="T391">v</ta>
            <ta e="T393" id="Seg_7897" s="T392">n</ta>
            <ta e="T394" id="Seg_7898" s="T393">v</ta>
            <ta e="T395" id="Seg_7899" s="T394">conj</ta>
            <ta e="T396" id="Seg_7900" s="T395">que</ta>
            <ta e="T397" id="Seg_7901" s="T396">v</ta>
            <ta e="T398" id="Seg_7902" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_7903" s="T398">v</ta>
            <ta e="T400" id="Seg_7904" s="T399">n</ta>
            <ta e="T404" id="Seg_7905" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_7906" s="T404">n</ta>
            <ta e="T406" id="Seg_7907" s="T405">dempro</ta>
            <ta e="T407" id="Seg_7908" s="T406">v</ta>
            <ta e="T408" id="Seg_7909" s="T407">v</ta>
            <ta e="T409" id="Seg_7910" s="T408">adv</ta>
            <ta e="T410" id="Seg_7911" s="T409">v</ta>
            <ta e="T437" id="Seg_7912" s="T433">v</ta>
            <ta e="T438" id="Seg_7913" s="T437">n</ta>
            <ta e="T441" id="Seg_7914" s="T440">conj</ta>
            <ta e="T444" id="Seg_7915" s="T443">v</ta>
            <ta e="T459" id="Seg_7916" s="T458">n</ta>
            <ta e="T460" id="Seg_7917" s="T459">v</ta>
            <ta e="T461" id="Seg_7918" s="T460">conj</ta>
            <ta e="T462" id="Seg_7919" s="T461">v</ta>
            <ta e="T464" id="Seg_7920" s="T463">adv</ta>
            <ta e="T465" id="Seg_7921" s="T464">v</ta>
            <ta e="T466" id="Seg_7922" s="T465">n</ta>
            <ta e="T468" id="Seg_7923" s="T467">dempro</ta>
            <ta e="T470" id="Seg_7924" s="T469">v</ta>
            <ta e="T471" id="Seg_7925" s="T470">conj</ta>
            <ta e="T472" id="Seg_7926" s="T471">v</ta>
            <ta e="T474" id="Seg_7927" s="T473">dempro</ta>
            <ta e="T475" id="Seg_7928" s="T474">v</ta>
            <ta e="T477" id="Seg_7929" s="T476">conj</ta>
            <ta e="T478" id="Seg_7930" s="T477">refl</ta>
            <ta e="T479" id="Seg_7931" s="T478">v</ta>
            <ta e="T480" id="Seg_7932" s="T479">v</ta>
            <ta e="T503" id="Seg_7933" s="T502">v</ta>
            <ta e="T504" id="Seg_7934" s="T503">n</ta>
            <ta e="T506" id="Seg_7935" s="T505">n</ta>
            <ta e="T507" id="Seg_7936" s="T506">n</ta>
            <ta e="T508" id="Seg_7937" s="T507">n</ta>
            <ta e="T510" id="Seg_7938" s="T509">pers</ta>
            <ta e="T511" id="Seg_7939" s="T510">adj</ta>
            <ta e="T512" id="Seg_7940" s="T511">v</ta>
            <ta e="T513" id="Seg_7941" s="T512">n</ta>
            <ta e="T514" id="Seg_7942" s="T513">v</ta>
            <ta e="T515" id="Seg_7943" s="T514">v</ta>
            <ta e="T517" id="Seg_7944" s="T516">v</ta>
            <ta e="T518" id="Seg_7945" s="T517">que</ta>
            <ta e="T520" id="Seg_7946" s="T519">pers</ta>
            <ta e="T521" id="Seg_7947" s="T520">v</ta>
            <ta e="T522" id="Seg_7948" s="T521">conj</ta>
            <ta e="T523" id="Seg_7949" s="T522">v</ta>
            <ta e="T524" id="Seg_7950" s="T523">adv</ta>
            <ta e="T526" id="Seg_7951" s="T525">conj</ta>
            <ta e="T527" id="Seg_7952" s="T526">dempro</ta>
            <ta e="T528" id="Seg_7953" s="T527">pers</ta>
            <ta e="T529" id="Seg_7954" s="T528">pers</ta>
            <ta e="T530" id="Seg_7955" s="T529">v</ta>
            <ta e="T532" id="Seg_7956" s="T531">pers</ta>
            <ta e="T533" id="Seg_7957" s="T532">adv</ta>
            <ta e="T534" id="Seg_7958" s="T533">v</ta>
            <ta e="T535" id="Seg_7959" s="T534">conj</ta>
            <ta e="T536" id="Seg_7960" s="T535">adv</ta>
            <ta e="T537" id="Seg_7961" s="T536">v</ta>
            <ta e="T539" id="Seg_7962" s="T538">adv</ta>
            <ta e="T540" id="Seg_7963" s="T539">dempro</ta>
            <ta e="T541" id="Seg_7964" s="T540">que</ta>
            <ta e="T542" id="Seg_7965" s="T541">pers</ta>
            <ta e="T543" id="Seg_7966" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_7967" s="T543">v</ta>
            <ta e="T562" id="Seg_7968" s="T561">v</ta>
            <ta e="T564" id="Seg_7969" s="T563">adv</ta>
            <ta e="T565" id="Seg_7970" s="T564">n</ta>
            <ta e="T567" id="Seg_7971" s="T566">pers</ta>
            <ta e="T568" id="Seg_7972" s="T567">n</ta>
            <ta e="T569" id="Seg_7973" s="T568">adv</ta>
            <ta e="T570" id="Seg_7974" s="T569">adv</ta>
            <ta e="T571" id="Seg_7975" s="T570">v</ta>
            <ta e="T575" id="Seg_7976" s="T572">pers</ta>
            <ta e="T576" id="Seg_7977" s="T575">adv</ta>
            <ta e="T577" id="Seg_7978" s="T576">v</ta>
            <ta e="T724" id="Seg_7979" s="T722">v</ta>
            <ta e="T725" id="Seg_7980" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_7981" s="T725">v</ta>
            <ta e="T727" id="Seg_7982" s="T726">conj</ta>
            <ta e="T729" id="Seg_7983" s="T728">conj</ta>
            <ta e="T730" id="Seg_7984" s="T729">v</ta>
            <ta e="T731" id="Seg_7985" s="T730">n</ta>
            <ta e="T732" id="Seg_7986" s="T731">v</ta>
            <ta e="T733" id="Seg_7987" s="T732">que</ta>
            <ta e="T734" id="Seg_7988" s="T733">adv</ta>
            <ta e="T735" id="Seg_7989" s="T734">v</ta>
            <ta e="T736" id="Seg_7990" s="T735">adj</ta>
            <ta e="T738" id="Seg_7991" s="T737">conj</ta>
            <ta e="T739" id="Seg_7992" s="T738">v</ta>
            <ta e="T740" id="Seg_7993" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_7994" s="T740">v</ta>
            <ta e="T771" id="Seg_7995" s="T770">dempro</ta>
            <ta e="T772" id="Seg_7996" s="T771">quant</ta>
            <ta e="T773" id="Seg_7997" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_7998" s="T773">v</ta>
            <ta e="T775" id="Seg_7999" s="T774">v</ta>
            <ta e="T777" id="Seg_8000" s="T776">dempro</ta>
            <ta e="T778" id="Seg_8001" s="T777">adv</ta>
            <ta e="T779" id="Seg_8002" s="T778">adv</ta>
            <ta e="T780" id="Seg_8003" s="T779">adv</ta>
            <ta e="T781" id="Seg_8004" s="T780">n</ta>
            <ta e="T782" id="Seg_8005" s="T781">v</ta>
            <ta e="T784" id="Seg_8006" s="T783">v</ta>
            <ta e="T786" id="Seg_8007" s="T785">conj</ta>
            <ta e="T787" id="Seg_8008" s="T786">adv</ta>
            <ta e="T788" id="Seg_8009" s="T787">dempro</ta>
            <ta e="T789" id="Seg_8010" s="T788">v</ta>
            <ta e="T790" id="Seg_8011" s="T789">v</ta>
            <ta e="T792" id="Seg_8012" s="T791">dempro</ta>
            <ta e="T793" id="Seg_8013" s="T792">v</ta>
            <ta e="T796" id="Seg_8014" s="T795">adv</ta>
            <ta e="T797" id="Seg_8015" s="T796">v</ta>
            <ta e="T801" id="Seg_8016" s="T800">pers</ta>
            <ta e="T802" id="Seg_8017" s="T801">adj</ta>
            <ta e="T803" id="Seg_8018" s="T802">n</ta>
            <ta e="T804" id="Seg_8019" s="T803">v</ta>
            <ta e="T806" id="Seg_8020" s="T805">dempro</ta>
            <ta e="T807" id="Seg_8021" s="T806">v</ta>
            <ta e="T808" id="Seg_8022" s="T807">num</ta>
            <ta e="T809" id="Seg_8023" s="T808">n</ta>
            <ta e="T811" id="Seg_8024" s="T810">conj</ta>
            <ta e="T812" id="Seg_8025" s="T811">adv</ta>
            <ta e="T813" id="Seg_8026" s="T812">v</ta>
            <ta e="T815" id="Seg_8027" s="T814">adv</ta>
            <ta e="T816" id="Seg_8028" s="T815">dempro</ta>
            <ta e="T817" id="Seg_8029" s="T816">adv</ta>
            <ta e="T818" id="Seg_8030" s="T817">adv</ta>
            <ta e="T819" id="Seg_8031" s="T818">n</ta>
            <ta e="T820" id="Seg_8032" s="T819">v</ta>
            <ta e="T821" id="Seg_8033" s="T820">adv</ta>
            <ta e="T822" id="Seg_8034" s="T821">dempro</ta>
            <ta e="T823" id="Seg_8035" s="T822">n</ta>
            <ta e="T825" id="Seg_8036" s="T824">v</ta>
            <ta e="T826" id="Seg_8037" s="T825">dempro</ta>
            <ta e="T827" id="Seg_8038" s="T826">v</ta>
            <ta e="T828" id="Seg_8039" s="T827">dempro</ta>
            <ta e="T829" id="Seg_8040" s="T828">v</ta>
            <ta e="T831" id="Seg_8041" s="T830">refl</ta>
            <ta e="T833" id="Seg_8042" s="T832">refl</ta>
            <ta e="T835" id="Seg_8043" s="T834">v</ta>
            <ta e="T893" id="Seg_8044" s="T892">ptcl</ta>
            <ta e="T894" id="Seg_8045" s="T893">n</ta>
            <ta e="T896" id="Seg_8046" s="T895">v</ta>
            <ta e="T897" id="Seg_8047" s="T896">adv</ta>
            <ta e="T898" id="Seg_8048" s="T897">n</ta>
            <ta e="T899" id="Seg_8049" s="T898">v</ta>
            <ta e="T900" id="Seg_8050" s="T899">n</ta>
            <ta e="T901" id="Seg_8051" s="T900">v</ta>
            <ta e="T903" id="Seg_8052" s="T902">adv</ta>
            <ta e="T904" id="Seg_8053" s="T903">num</ta>
            <ta e="T905" id="Seg_8054" s="T904">v</ta>
            <ta e="T906" id="Seg_8055" s="T905">conj</ta>
            <ta e="T907" id="Seg_8056" s="T906">adv</ta>
            <ta e="T908" id="Seg_8057" s="T907">adv</ta>
            <ta e="T909" id="Seg_8058" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_8059" s="T909">n</ta>
            <ta e="T911" id="Seg_8060" s="T910">v</ta>
            <ta e="T913" id="Seg_8061" s="T912">adv</ta>
            <ta e="T914" id="Seg_8062" s="T913">dempro</ta>
            <ta e="T915" id="Seg_8063" s="T914">adv</ta>
            <ta e="T916" id="Seg_8064" s="T915">v</ta>
            <ta e="T917" id="Seg_8065" s="T916">v</ta>
            <ta e="T919" id="Seg_8066" s="T918">v</ta>
            <ta e="T920" id="Seg_8067" s="T919">n</ta>
            <ta e="T922" id="Seg_8068" s="T921">conj</ta>
            <ta e="T923" id="Seg_8069" s="T922">n</ta>
            <ta e="T924" id="Seg_8070" s="T923">v</ta>
            <ta e="T926" id="Seg_8071" s="T925">conj</ta>
            <ta e="T927" id="Seg_8072" s="T926">adv</ta>
            <ta e="T928" id="Seg_8073" s="T927">n</ta>
            <ta e="T930" id="Seg_8074" s="T929">v</ta>
            <ta e="T932" id="Seg_8075" s="T931">v</ta>
            <ta e="T933" id="Seg_8076" s="T932">conj</ta>
            <ta e="T934" id="Seg_8077" s="T933">adv</ta>
            <ta e="T935" id="Seg_8078" s="T934">v</ta>
            <ta e="T937" id="Seg_8079" s="T936">conj</ta>
            <ta e="T938" id="Seg_8080" s="T937">ptcl</ta>
            <ta e="T1271" id="Seg_8081" s="T1270">ptcl</ta>
            <ta e="T1272" id="Seg_8082" s="T1271">que</ta>
            <ta e="T1273" id="Seg_8083" s="T1272">v</ta>
            <ta e="T1274" id="Seg_8084" s="T1273">n</ta>
            <ta e="T1275" id="Seg_8085" s="T1274">n</ta>
            <ta e="T1276" id="Seg_8086" s="T1275">v</ta>
            <ta e="T1278" id="Seg_8087" s="T1277">conj</ta>
            <ta e="T1279" id="Seg_8088" s="T1278">adv</ta>
            <ta e="T1280" id="Seg_8089" s="T1279">adv</ta>
            <ta e="T1281" id="Seg_8090" s="T1280">n</ta>
            <ta e="T1282" id="Seg_8091" s="T1281">adv</ta>
            <ta e="T1533" id="Seg_8092" s="T1282">v</ta>
            <ta e="T1283" id="Seg_8093" s="T1533">v</ta>
            <ta e="T1284" id="Seg_8094" s="T1283">adv</ta>
            <ta e="T1286" id="Seg_8095" s="T1285">pers</ta>
            <ta e="T1287" id="Seg_8096" s="T1286">ptcl</ta>
            <ta e="T1288" id="Seg_8097" s="T1287">v</ta>
            <ta e="T1292" id="Seg_8098" s="T1291">que</ta>
            <ta e="T1293" id="Seg_8099" s="T1292">que</ta>
            <ta e="T1294" id="Seg_8100" s="T1293">conj</ta>
            <ta e="T1295" id="Seg_8101" s="T1294">pers</ta>
            <ta e="T1296" id="Seg_8102" s="T1295">ptcl</ta>
            <ta e="T1297" id="Seg_8103" s="T1296">v</ta>
            <ta e="T1299" id="Seg_8104" s="T1298">conj</ta>
            <ta e="T1300" id="Seg_8105" s="T1299">n</ta>
            <ta e="T1301" id="Seg_8106" s="T1300">v</ta>
            <ta e="T1302" id="Seg_8107" s="T1301">ptcl</ta>
            <ta e="T1303" id="Seg_8108" s="T1302">v</ta>
            <ta e="T1304" id="Seg_8109" s="T1303">pers</ta>
            <ta e="T1307" id="Seg_8110" s="T1306">dempro</ta>
            <ta e="T1308" id="Seg_8111" s="T1307">v</ta>
            <ta e="T1309" id="Seg_8112" s="T1308">n</ta>
            <ta e="T1313" id="Seg_8113" s="T1312">n</ta>
            <ta e="T1314" id="Seg_8114" s="T1313">n</ta>
            <ta e="T1315" id="Seg_8115" s="T1314">v</ta>
            <ta e="T1316" id="Seg_8116" s="T1315">refl</ta>
            <ta e="T1317" id="Seg_8117" s="T1316">n</ta>
            <ta e="T1318" id="Seg_8118" s="T1317">conj</ta>
            <ta e="T1319" id="Seg_8119" s="T1318">dempro</ta>
            <ta e="T1320" id="Seg_8120" s="T1319">ptcl</ta>
            <ta e="T1322" id="Seg_8121" s="T1321">refl</ta>
            <ta e="T1323" id="Seg_8122" s="T1322">n</ta>
            <ta e="T1325" id="Seg_8123" s="T1324">v</ta>
            <ta e="T1326" id="Seg_8124" s="T1325">conj</ta>
            <ta e="T1327" id="Seg_8125" s="T1326">dempro</ta>
            <ta e="T1328" id="Seg_8126" s="T1327">v</ta>
            <ta e="T1329" id="Seg_8127" s="T1328">n</ta>
            <ta e="T1330" id="Seg_8128" s="T1329">v</ta>
            <ta e="T1452" id="Seg_8129" s="T1451">ptcl</ta>
            <ta e="T1453" id="Seg_8130" s="T1452">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T54" id="Seg_8131" s="T53">0.1.h:A</ta>
            <ta e="T56" id="Seg_8132" s="T55">adv:Time</ta>
            <ta e="T58" id="Seg_8133" s="T57">np.h:A</ta>
            <ta e="T59" id="Seg_8134" s="T58">pro:G</ta>
            <ta e="T61" id="Seg_8135" s="T60">pro.h:A</ta>
            <ta e="T62" id="Seg_8136" s="T61">pro:G</ta>
            <ta e="T65" id="Seg_8137" s="T64">np.h:Th</ta>
            <ta e="T70" id="Seg_8138" s="T69">pro.h:Th</ta>
            <ta e="T74" id="Seg_8139" s="T73">np.h:Th</ta>
            <ta e="T76" id="Seg_8140" s="T75">pro.h:A</ta>
            <ta e="T78" id="Seg_8141" s="T77">adv:L</ta>
            <ta e="T79" id="Seg_8142" s="T78">np:L</ta>
            <ta e="T80" id="Seg_8143" s="T79">np.h:R</ta>
            <ta e="T81" id="Seg_8144" s="T80">0.1.h:A</ta>
            <ta e="T82" id="Seg_8145" s="T81">adv:L</ta>
            <ta e="T83" id="Seg_8146" s="T82">np.h:Th</ta>
            <ta e="T87" id="Seg_8147" s="T86">pro.h:Th</ta>
            <ta e="T88" id="Seg_8148" s="T87">adv:L</ta>
            <ta e="T107" id="Seg_8149" s="T106">np.h:A 0.3.h:Poss</ta>
            <ta e="T108" id="Seg_8150" s="T107">np.h:A 0.3.h:Poss</ta>
            <ta e="T110" id="Seg_8151" s="T109">np.h:Poss</ta>
            <ta e="T114" id="Seg_8152" s="T113">pro.h:R</ta>
            <ta e="T116" id="Seg_8153" s="T115">0.2.h:A</ta>
            <ta e="T117" id="Seg_8154" s="T116">pro.h:Poss</ta>
            <ta e="T118" id="Seg_8155" s="T117">pro.h:R</ta>
            <ta e="T121" id="Seg_8156" s="T120">np.h:R</ta>
            <ta e="T122" id="Seg_8157" s="T121">np.h:R</ta>
            <ta e="T123" id="Seg_8158" s="T122">0.3.h:A</ta>
            <ta e="T125" id="Seg_8159" s="T124">np.h:R</ta>
            <ta e="T127" id="Seg_8160" s="T126">adv:Time</ta>
            <ta e="T129" id="Seg_8161" s="T128">np.h:A</ta>
            <ta e="T130" id="Seg_8162" s="T129">0.2.h:A</ta>
            <ta e="T131" id="Seg_8163" s="T130">np:G</ta>
            <ta e="T133" id="Seg_8164" s="T132">pro.h:A</ta>
            <ta e="T135" id="Seg_8165" s="T134">0.1.h:A</ta>
            <ta e="T138" id="Seg_8166" s="T137">adv:Time</ta>
            <ta e="T139" id="Seg_8167" s="T138">np:Th</ta>
            <ta e="T140" id="Seg_8168" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_8169" s="T140">np:Th</ta>
            <ta e="T143" id="Seg_8170" s="T142">0.3.h:Th</ta>
            <ta e="T144" id="Seg_8171" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_8172" s="T144">adv:Time</ta>
            <ta e="T147" id="Seg_8173" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_8174" s="T147">np:Com</ta>
            <ta e="T150" id="Seg_8175" s="T149">pro.h:Th</ta>
            <ta e="T152" id="Seg_8176" s="T151">0.3.h:A</ta>
            <ta e="T154" id="Seg_8177" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_8178" s="T154">np:G</ta>
            <ta e="T174" id="Seg_8179" s="T173">np.h:A</ta>
            <ta e="T176" id="Seg_8180" s="T175">np.h:A</ta>
            <ta e="T179" id="Seg_8181" s="T178">adv:Time</ta>
            <ta e="T180" id="Seg_8182" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_8183" s="T181">np:Th</ta>
            <ta e="T186" id="Seg_8184" s="T185">pro:P</ta>
            <ta e="T187" id="Seg_8185" s="T186">0.3.h:A</ta>
            <ta e="T189" id="Seg_8186" s="T188">np:P</ta>
            <ta e="T190" id="Seg_8187" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_8188" s="T190">np:Th</ta>
            <ta e="T194" id="Seg_8189" s="T193">adv:Time</ta>
            <ta e="T196" id="Seg_8190" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_8191" s="T196">np:Th</ta>
            <ta e="T199" id="Seg_8192" s="T198">0.3.h:A</ta>
            <ta e="T201" id="Seg_8193" s="T1531">0.3.h:A</ta>
            <ta e="T203" id="Seg_8194" s="T202">np.h:A</ta>
            <ta e="T206" id="Seg_8195" s="T205">np:P</ta>
            <ta e="T208" id="Seg_8196" s="T207">0.3.h:A</ta>
            <ta e="T209" id="Seg_8197" s="T208">np:Ins</ta>
            <ta e="T211" id="Seg_8198" s="T210">0.3.h:A</ta>
            <ta e="T213" id="Seg_8199" s="T212">adv:Time</ta>
            <ta e="T215" id="Seg_8200" s="T1532">0.3.h:A</ta>
            <ta e="T248" id="Seg_8201" s="T247">0.3.h:E</ta>
            <ta e="T251" id="Seg_8202" s="T250">0.3.h:A</ta>
            <ta e="T253" id="Seg_8203" s="T252">np:P</ta>
            <ta e="T254" id="Seg_8204" s="T253">0.3.h:A</ta>
            <ta e="T256" id="Seg_8205" s="T255">0.3.h:A</ta>
            <ta e="T263" id="Seg_8206" s="T262">0.3.h:A</ta>
            <ta e="T372" id="Seg_8207" s="T371">pro.h:Poss</ta>
            <ta e="T373" id="Seg_8208" s="T372">np.h:Th</ta>
            <ta e="T375" id="Seg_8209" s="T374">pro.h:Poss</ta>
            <ta e="T378" id="Seg_8210" s="T377">np.h:Th</ta>
            <ta e="T381" id="Seg_8211" s="T379">np.h:Th</ta>
            <ta e="T383" id="Seg_8212" s="T381">0.3.h:A</ta>
            <ta e="T384" id="Seg_8213" s="T383">pro.h:A</ta>
            <ta e="T385" id="Seg_8214" s="T384">pro:P</ta>
            <ta e="T389" id="Seg_8215" s="T388">adv:Time</ta>
            <ta e="T390" id="Seg_8216" s="T389">np.h:A</ta>
            <ta e="T391" id="Seg_8217" s="T390">np.h:Th</ta>
            <ta e="T393" id="Seg_8218" s="T392">np:G</ta>
            <ta e="T394" id="Seg_8219" s="T393">0.3.h:A</ta>
            <ta e="T396" id="Seg_8220" s="T395">pro:P</ta>
            <ta e="T397" id="Seg_8221" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_8222" s="T398">0.3.h:A</ta>
            <ta e="T400" id="Seg_8223" s="T399">np.h:Th</ta>
            <ta e="T406" id="Seg_8224" s="T405">pro.h:E</ta>
            <ta e="T408" id="Seg_8225" s="T407">0.3.h:P</ta>
            <ta e="T409" id="Seg_8226" s="T408">np.h:Th</ta>
            <ta e="T438" id="Seg_8227" s="T437">np.h:E</ta>
            <ta e="T444" id="Seg_8228" s="T443">0.3.h:E</ta>
            <ta e="T459" id="Seg_8229" s="T458">np.h:A</ta>
            <ta e="T462" id="Seg_8230" s="T461">0.3.h:A</ta>
            <ta e="T464" id="Seg_8231" s="T463">adv:Time</ta>
            <ta e="T465" id="Seg_8232" s="T464">0.3.h:A</ta>
            <ta e="T466" id="Seg_8233" s="T465">np:Th</ta>
            <ta e="T468" id="Seg_8234" s="T467">pro.h:R</ta>
            <ta e="T470" id="Seg_8235" s="T469">0.3.h:A</ta>
            <ta e="T472" id="Seg_8236" s="T471">0.3.h:A</ta>
            <ta e="T474" id="Seg_8237" s="T473">pro.h:E</ta>
            <ta e="T478" id="Seg_8238" s="T477">pro.h:A</ta>
            <ta e="T480" id="Seg_8239" s="T479">0.3.h:E</ta>
            <ta e="T503" id="Seg_8240" s="T502">0.3.h:E</ta>
            <ta e="T507" id="Seg_8241" s="T506">np.h:Th</ta>
            <ta e="T508" id="Seg_8242" s="T507">np.h:Th</ta>
            <ta e="T510" id="Seg_8243" s="T509">pro.h:Th</ta>
            <ta e="T513" id="Seg_8244" s="T512">np.h:A 0.1.h:Poss</ta>
            <ta e="T515" id="Seg_8245" s="T514">0.2.h:A</ta>
            <ta e="T517" id="Seg_8246" s="T516">0.3.h:A</ta>
            <ta e="T518" id="Seg_8247" s="T517">pro:G</ta>
            <ta e="T520" id="Seg_8248" s="T519">pro.h:Th</ta>
            <ta e="T523" id="Seg_8249" s="T522">0.1.h:A</ta>
            <ta e="T524" id="Seg_8250" s="T523">adv:L</ta>
            <ta e="T527" id="Seg_8251" s="T526">pro.h:A</ta>
            <ta e="T528" id="Seg_8252" s="T527">pro.h:A</ta>
            <ta e="T529" id="Seg_8253" s="T528">pro.h:E</ta>
            <ta e="T532" id="Seg_8254" s="T531">pro.h:A</ta>
            <ta e="T533" id="Seg_8255" s="T532">adv:Time</ta>
            <ta e="T536" id="Seg_8256" s="T535">adv:Time</ta>
            <ta e="T537" id="Seg_8257" s="T536">0.1.h:A</ta>
            <ta e="T539" id="Seg_8258" s="T538">adv:Time</ta>
            <ta e="T540" id="Seg_8259" s="T539">pro.h:A</ta>
            <ta e="T542" id="Seg_8260" s="T541">pro.h:E</ta>
            <ta e="T562" id="Seg_8261" s="T561">0.3.h:A</ta>
            <ta e="T567" id="Seg_8262" s="T566">pro.h:Poss</ta>
            <ta e="T568" id="Seg_8263" s="T567">np.h:A</ta>
            <ta e="T575" id="Seg_8264" s="T572">pro.h:A</ta>
            <ta e="T726" id="Seg_8265" s="T725">0.1.h:A</ta>
            <ta e="T730" id="Seg_8266" s="T729">0.1.h:A</ta>
            <ta e="T731" id="Seg_8267" s="T730">np:Th</ta>
            <ta e="T733" id="Seg_8268" s="T732">pro:Th</ta>
            <ta e="T734" id="Seg_8269" s="T733">adv:L</ta>
            <ta e="T741" id="Seg_8270" s="T740">0.1.h:A</ta>
            <ta e="T771" id="Seg_8271" s="T770">pro.h:E</ta>
            <ta e="T777" id="Seg_8272" s="T776">pro.h:Th</ta>
            <ta e="T781" id="Seg_8273" s="T780">np:Th</ta>
            <ta e="T784" id="Seg_8274" s="T783">0.3.h:P</ta>
            <ta e="T787" id="Seg_8275" s="T786">adv:Time</ta>
            <ta e="T788" id="Seg_8276" s="T787">pro.h:P</ta>
            <ta e="T790" id="Seg_8277" s="T789">0.3.h:Th</ta>
            <ta e="T792" id="Seg_8278" s="T791">pro.h:P</ta>
            <ta e="T797" id="Seg_8279" s="T796">0.3.h:E</ta>
            <ta e="T801" id="Seg_8280" s="T800">pro.h:Poss</ta>
            <ta e="T803" id="Seg_8281" s="T802">np:Th</ta>
            <ta e="T806" id="Seg_8282" s="T805">pro.h:A</ta>
            <ta e="T809" id="Seg_8283" s="T808">np:Th</ta>
            <ta e="T812" id="Seg_8284" s="T811">adv:L</ta>
            <ta e="T813" id="Seg_8285" s="T812">0.3.h:A</ta>
            <ta e="T815" id="Seg_8286" s="T814">adv:Time</ta>
            <ta e="T817" id="Seg_8287" s="T816">adv:L</ta>
            <ta e="T819" id="Seg_8288" s="T818">np:Th</ta>
            <ta e="T820" id="Seg_8289" s="T819">0.3.h:A</ta>
            <ta e="T821" id="Seg_8290" s="T820">adv:Time</ta>
            <ta e="T822" id="Seg_8291" s="T821">pro.h:A</ta>
            <ta e="T823" id="Seg_8292" s="T822">np:P</ta>
            <ta e="T826" id="Seg_8293" s="T825">pro.h:E</ta>
            <ta e="T828" id="Seg_8294" s="T827">pro:Th</ta>
            <ta e="T829" id="Seg_8295" s="T828">0.3.h:A</ta>
            <ta e="T833" id="Seg_8296" s="T832">pro.h:A</ta>
            <ta e="T894" id="Seg_8297" s="T893">np:P</ta>
            <ta e="T896" id="Seg_8298" s="T895">0.3.h:A</ta>
            <ta e="T897" id="Seg_8299" s="T896">adv:Time</ta>
            <ta e="T900" id="Seg_8300" s="T899">np:P</ta>
            <ta e="T901" id="Seg_8301" s="T900">0.3.h:A</ta>
            <ta e="T903" id="Seg_8302" s="T902">adv:Time</ta>
            <ta e="T904" id="Seg_8303" s="T903">np:P</ta>
            <ta e="T905" id="Seg_8304" s="T904">0.3.h:A</ta>
            <ta e="T907" id="Seg_8305" s="T906">adv:L</ta>
            <ta e="T908" id="Seg_8306" s="T907">adv:L</ta>
            <ta e="T910" id="Seg_8307" s="T909">np:Ins</ta>
            <ta e="T911" id="Seg_8308" s="T910">0.3.h:A</ta>
            <ta e="T913" id="Seg_8309" s="T912">adv:Time</ta>
            <ta e="T914" id="Seg_8310" s="T913">pro.h:Th</ta>
            <ta e="T915" id="Seg_8311" s="T914">adv:L</ta>
            <ta e="T916" id="Seg_8312" s="T915">0.3.h:A</ta>
            <ta e="T917" id="Seg_8313" s="T916">0.3.h:A</ta>
            <ta e="T919" id="Seg_8314" s="T918">0.3.h:A</ta>
            <ta e="T920" id="Seg_8315" s="T919">np:G</ta>
            <ta e="T923" id="Seg_8316" s="T922">np:P</ta>
            <ta e="T924" id="Seg_8317" s="T923">0.3.h:A</ta>
            <ta e="T927" id="Seg_8318" s="T926">adv:Time</ta>
            <ta e="T928" id="Seg_8319" s="T927">np:G</ta>
            <ta e="T930" id="Seg_8320" s="T929">0.3.h:A</ta>
            <ta e="T932" id="Seg_8321" s="T931">0.3.h:A</ta>
            <ta e="T934" id="Seg_8322" s="T933">adv:Time</ta>
            <ta e="T935" id="Seg_8323" s="T934">0.3.h:A</ta>
            <ta e="T1273" id="Seg_8324" s="T1272">0.3.h:P</ta>
            <ta e="T1274" id="Seg_8325" s="T1273">np.h:Th</ta>
            <ta e="T1275" id="Seg_8326" s="T1274">np:G</ta>
            <ta e="T1276" id="Seg_8327" s="T1275">0.3.h:A</ta>
            <ta e="T1279" id="Seg_8328" s="T1278">adv:L</ta>
            <ta e="T1281" id="Seg_8329" s="T1280">np:Th</ta>
            <ta e="T1282" id="Seg_8330" s="T1281">adv:L</ta>
            <ta e="T1286" id="Seg_8331" s="T1285">pro.h:E</ta>
            <ta e="T1295" id="Seg_8332" s="T1294">pro.h:E</ta>
            <ta e="T1300" id="Seg_8333" s="T1299">np.h:A</ta>
            <ta e="T1304" id="Seg_8334" s="T1303">pro.h:R</ta>
            <ta e="T1307" id="Seg_8335" s="T1306">pro.h:Poss np:Th</ta>
            <ta e="T1309" id="Seg_8336" s="T1308">np:G</ta>
            <ta e="T1313" id="Seg_8337" s="T1312">np.h:P</ta>
            <ta e="T1314" id="Seg_8338" s="T1313">np:A</ta>
            <ta e="T1316" id="Seg_8339" s="T1315">pro.h:Poss</ta>
            <ta e="T1317" id="Seg_8340" s="T1316">np:L</ta>
            <ta e="T1319" id="Seg_8341" s="T1318">pro.h:R</ta>
            <ta e="T1323" id="Seg_8342" s="T1322">np:Th</ta>
            <ta e="T1325" id="Seg_8343" s="T1324">0.3.h:A</ta>
            <ta e="T1327" id="Seg_8344" s="T1326">pro.h:A</ta>
            <ta e="T1329" id="Seg_8345" s="T1328">np.h:Th</ta>
            <ta e="T1330" id="Seg_8346" s="T1329">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T54" id="Seg_8347" s="T53">v:pred 0.1.h:S</ta>
            <ta e="T58" id="Seg_8348" s="T57">np.h:S</ta>
            <ta e="T60" id="Seg_8349" s="T59">v:pred</ta>
            <ta e="T65" id="Seg_8350" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_8351" s="T65">adj:pred</ta>
            <ta e="T67" id="Seg_8352" s="T66">cop</ta>
            <ta e="T70" id="Seg_8353" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_8354" s="T70">cop</ta>
            <ta e="T73" id="Seg_8355" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_8356" s="T73">np.h:S</ta>
            <ta e="T76" id="Seg_8357" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_8358" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_8359" s="T80">v:pred 0.1.h:S</ta>
            <ta e="T83" id="Seg_8360" s="T82">np.h:S</ta>
            <ta e="T84" id="Seg_8361" s="T83">v:pred</ta>
            <ta e="T87" id="Seg_8362" s="T86">pro.h:S</ta>
            <ta e="T89" id="Seg_8363" s="T88">v:pred</ta>
            <ta e="T107" id="Seg_8364" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_8365" s="T107">np.h:S</ta>
            <ta e="T109" id="Seg_8366" s="T108">v:pred</ta>
            <ta e="T113" id="Seg_8367" s="T112">pro.h:S</ta>
            <ta e="T115" id="Seg_8368" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_8369" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T123" id="Seg_8370" s="T122">v:pred 0.3.h:S</ta>
            <ta e="T124" id="Seg_8371" s="T123">ptcl:pred</ta>
            <ta e="T125" id="Seg_8372" s="T124">np.h:O</ta>
            <ta e="T126" id="Seg_8373" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_8374" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_8375" s="T128">np.h:S</ta>
            <ta e="T130" id="Seg_8376" s="T129">v:pred 0.2.h:S</ta>
            <ta e="T133" id="Seg_8377" s="T132">pro.h:S</ta>
            <ta e="T134" id="Seg_8378" s="T133">v:pred</ta>
            <ta e="T135" id="Seg_8379" s="T134">v:pred 0.1.h:S</ta>
            <ta e="T139" id="Seg_8380" s="T138">np:O</ta>
            <ta e="T140" id="Seg_8381" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_8382" s="T140">np:O</ta>
            <ta e="T143" id="Seg_8383" s="T142">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_8384" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_8385" s="T146">v:pred 0.3.h:S</ta>
            <ta e="T150" id="Seg_8386" s="T149">pro.h:O</ta>
            <ta e="T152" id="Seg_8387" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T154" id="Seg_8388" s="T153">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_8389" s="T173">np.h:S</ta>
            <ta e="T176" id="Seg_8390" s="T175">np.h:S</ta>
            <ta e="T177" id="Seg_8391" s="T176">v:pred</ta>
            <ta e="T180" id="Seg_8392" s="T179">v:pred 0.3.h:S</ta>
            <ta e="T182" id="Seg_8393" s="T181">np:S</ta>
            <ta e="T183" id="Seg_8394" s="T182">adj:pred</ta>
            <ta e="T184" id="Seg_8395" s="T183">cop</ta>
            <ta e="T186" id="Seg_8396" s="T185">pro:O</ta>
            <ta e="T187" id="Seg_8397" s="T186">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_8398" s="T188">np:O</ta>
            <ta e="T190" id="Seg_8399" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_8400" s="T190">np:S</ta>
            <ta e="T192" id="Seg_8401" s="T191">adj:pred</ta>
            <ta e="T196" id="Seg_8402" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_8403" s="T196">np:O</ta>
            <ta e="T199" id="Seg_8404" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T1531" id="Seg_8405" s="T200">conv:pred</ta>
            <ta e="T201" id="Seg_8406" s="T1531">v:pred 0.3.h:S</ta>
            <ta e="T203" id="Seg_8407" s="T202">np.h:S</ta>
            <ta e="T206" id="Seg_8408" s="T205">np:O</ta>
            <ta e="T207" id="Seg_8409" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_8410" s="T207">v:pred 0.3.h:S</ta>
            <ta e="T211" id="Seg_8411" s="T210">v:pred 0.3.h:S</ta>
            <ta e="T1532" id="Seg_8412" s="T214">conv:pred</ta>
            <ta e="T215" id="Seg_8413" s="T1532">v:pred 0.3.h:S</ta>
            <ta e="T248" id="Seg_8414" s="T247">v:pred 0.3.h:S</ta>
            <ta e="T251" id="Seg_8415" s="T250">v:pred 0.3.h:S</ta>
            <ta e="T253" id="Seg_8416" s="T252">np:O</ta>
            <ta e="T254" id="Seg_8417" s="T253">v:pred 0.3.h:S</ta>
            <ta e="T256" id="Seg_8418" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T263" id="Seg_8419" s="T262">v:pred 0.3.h:S</ta>
            <ta e="T373" id="Seg_8420" s="T372">np.h:S</ta>
            <ta e="T374" id="Seg_8421" s="T373">v:pred</ta>
            <ta e="T377" id="Seg_8422" s="T376">v:pred</ta>
            <ta e="T378" id="Seg_8423" s="T377">np.h:S</ta>
            <ta e="T381" id="Seg_8424" s="T379">np.h:O</ta>
            <ta e="T383" id="Seg_8425" s="T381">v:pred 0.3.h:S</ta>
            <ta e="T384" id="Seg_8426" s="T383">pro.h:S</ta>
            <ta e="T385" id="Seg_8427" s="T384">pro:O</ta>
            <ta e="T386" id="Seg_8428" s="T385">ptcl.neg</ta>
            <ta e="T387" id="Seg_8429" s="T386">v:pred</ta>
            <ta e="T390" id="Seg_8430" s="T389">np.h:S</ta>
            <ta e="T391" id="Seg_8431" s="T390">np.h:O</ta>
            <ta e="T392" id="Seg_8432" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_8433" s="T393">v:pred 0.3.h:S</ta>
            <ta e="T396" id="Seg_8434" s="T395">pro:O</ta>
            <ta e="T397" id="Seg_8435" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T398" id="Seg_8436" s="T397">ptcl.neg</ta>
            <ta e="T399" id="Seg_8437" s="T398">v:pred 0.3.h:S</ta>
            <ta e="T400" id="Seg_8438" s="T399">np.h:O</ta>
            <ta e="T406" id="Seg_8439" s="T405">pro.h:S</ta>
            <ta e="T407" id="Seg_8440" s="T406">v:pred</ta>
            <ta e="T408" id="Seg_8441" s="T407">v:pred 0.3.h:S</ta>
            <ta e="T409" id="Seg_8442" s="T408">np.h:S</ta>
            <ta e="T410" id="Seg_8443" s="T409">v:pred </ta>
            <ta e="T437" id="Seg_8444" s="T433">v:pred</ta>
            <ta e="T438" id="Seg_8445" s="T437">np.h:S</ta>
            <ta e="T444" id="Seg_8446" s="T443">v:pred 0.3.h:S</ta>
            <ta e="T459" id="Seg_8447" s="T458">np.h:S</ta>
            <ta e="T460" id="Seg_8448" s="T459">v:pred</ta>
            <ta e="T462" id="Seg_8449" s="T461">v:pred 0.3.h:S</ta>
            <ta e="T465" id="Seg_8450" s="T464">v:pred 0.3.h:S</ta>
            <ta e="T466" id="Seg_8451" s="T465">np:O</ta>
            <ta e="T470" id="Seg_8452" s="T469">v:pred 0.3.h:S</ta>
            <ta e="T472" id="Seg_8453" s="T471">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_8454" s="T473">pro.h:S</ta>
            <ta e="T475" id="Seg_8455" s="T474">v:pred</ta>
            <ta e="T478" id="Seg_8456" s="T477">pro.h:S</ta>
            <ta e="T479" id="Seg_8457" s="T478">v:pred</ta>
            <ta e="T480" id="Seg_8458" s="T479">v:pred 0.3.h:S</ta>
            <ta e="T503" id="Seg_8459" s="T502">v:pred 0.3.h:S</ta>
            <ta e="T507" id="Seg_8460" s="T506">np.h:O</ta>
            <ta e="T508" id="Seg_8461" s="T507">np.h:O</ta>
            <ta e="T510" id="Seg_8462" s="T509">pro.h:S</ta>
            <ta e="T511" id="Seg_8463" s="T510">adj:pred</ta>
            <ta e="T512" id="Seg_8464" s="T511">cop</ta>
            <ta e="T513" id="Seg_8465" s="T512">np.h:S</ta>
            <ta e="T514" id="Seg_8466" s="T513">v:pred</ta>
            <ta e="T515" id="Seg_8467" s="T514">v:pred 0.2.h:S</ta>
            <ta e="T517" id="Seg_8468" s="T516">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_8469" s="T519">pro.h:S</ta>
            <ta e="T521" id="Seg_8470" s="T520">v:pred</ta>
            <ta e="T523" id="Seg_8471" s="T522">v:pred 0.1.h:S</ta>
            <ta e="T527" id="Seg_8472" s="T526">pro.h:S</ta>
            <ta e="T528" id="Seg_8473" s="T527">pro.h:S</ta>
            <ta e="T529" id="Seg_8474" s="T528">pro.h:O</ta>
            <ta e="T530" id="Seg_8475" s="T529">v:pred</ta>
            <ta e="T532" id="Seg_8476" s="T531">pro.h:S</ta>
            <ta e="T534" id="Seg_8477" s="T533">v:pred</ta>
            <ta e="T537" id="Seg_8478" s="T536">v:pred 0.1.h:S</ta>
            <ta e="T540" id="Seg_8479" s="T539">pro.h:S</ta>
            <ta e="T542" id="Seg_8480" s="T541">pro.h:O</ta>
            <ta e="T543" id="Seg_8481" s="T542">ptcl.neg</ta>
            <ta e="T544" id="Seg_8482" s="T543">v:pred</ta>
            <ta e="T562" id="Seg_8483" s="T561">v:pred 0.3.h:S</ta>
            <ta e="T568" id="Seg_8484" s="T567">np.h:S</ta>
            <ta e="T571" id="Seg_8485" s="T570">v:pred</ta>
            <ta e="T575" id="Seg_8486" s="T572">pro.h:S</ta>
            <ta e="T577" id="Seg_8487" s="T576">v:pred</ta>
            <ta e="T725" id="Seg_8488" s="T724">ptcl.neg</ta>
            <ta e="T726" id="Seg_8489" s="T725">v:pred 0.1.h:S</ta>
            <ta e="T730" id="Seg_8490" s="T729">v:pred 0.1.h:S</ta>
            <ta e="T732" id="Seg_8491" s="T731">v:pred</ta>
            <ta e="T733" id="Seg_8492" s="T732">pro:S</ta>
            <ta e="T735" id="Seg_8493" s="T734">cop</ta>
            <ta e="T736" id="Seg_8494" s="T735">adj:pred</ta>
            <ta e="T740" id="Seg_8495" s="T739">ptcl.neg</ta>
            <ta e="T741" id="Seg_8496" s="T740">v:pred 0.1.h:S</ta>
            <ta e="T771" id="Seg_8497" s="T770">pro.h:S</ta>
            <ta e="T773" id="Seg_8498" s="T772">ptcl.neg</ta>
            <ta e="T775" id="Seg_8499" s="T774">v:pred</ta>
            <ta e="T777" id="Seg_8500" s="T776">pro.h:S</ta>
            <ta e="T779" id="Seg_8501" s="T778">adj:pred</ta>
            <ta e="T781" id="Seg_8502" s="T780">np:S</ta>
            <ta e="T782" id="Seg_8503" s="T781">v:pred</ta>
            <ta e="T784" id="Seg_8504" s="T783">v:pred 0.3.h:S</ta>
            <ta e="T790" id="Seg_8505" s="T789">v:pred 0.3.h:S</ta>
            <ta e="T792" id="Seg_8506" s="T791">pro.h:S</ta>
            <ta e="T793" id="Seg_8507" s="T792">v:pred</ta>
            <ta e="T797" id="Seg_8508" s="T796">v:pred 0.3.h:S</ta>
            <ta e="T803" id="Seg_8509" s="T802">np:S</ta>
            <ta e="T804" id="Seg_8510" s="T803">v:pred</ta>
            <ta e="T806" id="Seg_8511" s="T805">pro.h:S</ta>
            <ta e="T807" id="Seg_8512" s="T806">v:pred</ta>
            <ta e="T809" id="Seg_8513" s="T808">np:O</ta>
            <ta e="T813" id="Seg_8514" s="T812">v:pred 0.3.h:S</ta>
            <ta e="T819" id="Seg_8515" s="T818">np:O</ta>
            <ta e="T820" id="Seg_8516" s="T819">v:pred 0.3.h:S</ta>
            <ta e="T822" id="Seg_8517" s="T821">pro.h:S</ta>
            <ta e="T823" id="Seg_8518" s="T822">np:O</ta>
            <ta e="T825" id="Seg_8519" s="T824">v:pred</ta>
            <ta e="T826" id="Seg_8520" s="T825">pro.h:S</ta>
            <ta e="T827" id="Seg_8521" s="T826">v:pred</ta>
            <ta e="T828" id="Seg_8522" s="T827">pro:O</ta>
            <ta e="T829" id="Seg_8523" s="T828">v:pred 0.3.h:S</ta>
            <ta e="T833" id="Seg_8524" s="T832">pro.h:S</ta>
            <ta e="T835" id="Seg_8525" s="T834">v:pred</ta>
            <ta e="T894" id="Seg_8526" s="T893">np:O</ta>
            <ta e="T896" id="Seg_8527" s="T895">v:pred 0.3.h:S</ta>
            <ta e="T900" id="Seg_8528" s="T899">np:O</ta>
            <ta e="T901" id="Seg_8529" s="T900">v:pred 0.3.h:S</ta>
            <ta e="T904" id="Seg_8530" s="T903">np:O</ta>
            <ta e="T905" id="Seg_8531" s="T904">v:pred 0.3.h:S</ta>
            <ta e="T911" id="Seg_8532" s="T910">v:pred 0.3.h:S</ta>
            <ta e="T914" id="Seg_8533" s="T913">pro.h:O</ta>
            <ta e="T916" id="Seg_8534" s="T915">v:pred 0.3.h:S</ta>
            <ta e="T917" id="Seg_8535" s="T916">v:pred 0.3.h:S</ta>
            <ta e="T919" id="Seg_8536" s="T918">v:pred 0.3.h:S</ta>
            <ta e="T923" id="Seg_8537" s="T922">np:O</ta>
            <ta e="T924" id="Seg_8538" s="T923">v:pred 0.3.h:S</ta>
            <ta e="T930" id="Seg_8539" s="T929">v:pred 0.3.h:S</ta>
            <ta e="T932" id="Seg_8540" s="T931">v:pred 0.3.h:S</ta>
            <ta e="T935" id="Seg_8541" s="T934">v:pred 0.3.h:S</ta>
            <ta e="T1273" id="Seg_8542" s="T1272">v:pred 0.3.h:S</ta>
            <ta e="T1274" id="Seg_8543" s="T1273">np.h:O</ta>
            <ta e="T1276" id="Seg_8544" s="T1275">v:pred 0.3.h:S</ta>
            <ta e="T1281" id="Seg_8545" s="T1280">np:S</ta>
            <ta e="T1533" id="Seg_8546" s="T1282">conv:pred</ta>
            <ta e="T1283" id="Seg_8547" s="T1533">v:pred</ta>
            <ta e="T1286" id="Seg_8548" s="T1285">pro.h:S</ta>
            <ta e="T1288" id="Seg_8549" s="T1287">v:pred</ta>
            <ta e="T1295" id="Seg_8550" s="T1294">pro.h:S</ta>
            <ta e="T1297" id="Seg_8551" s="T1296">v:pred</ta>
            <ta e="T1300" id="Seg_8552" s="T1299">np.h:S</ta>
            <ta e="T1303" id="Seg_8553" s="T1302">v:pred</ta>
            <ta e="T1307" id="Seg_8554" s="T1306">np:S</ta>
            <ta e="T1308" id="Seg_8555" s="T1307">v:pred</ta>
            <ta e="T1313" id="Seg_8556" s="T1312">np.h:O</ta>
            <ta e="T1314" id="Seg_8557" s="T1313">np:S</ta>
            <ta e="T1315" id="Seg_8558" s="T1314">v:pred</ta>
            <ta e="T1323" id="Seg_8559" s="T1322">np:O</ta>
            <ta e="T1325" id="Seg_8560" s="T1324">v:pred 0.3.h:S</ta>
            <ta e="T1327" id="Seg_8561" s="T1326">pro.h:S</ta>
            <ta e="T1328" id="Seg_8562" s="T1327">v:pred</ta>
            <ta e="T1329" id="Seg_8563" s="T1328">np.h:O</ta>
            <ta e="T1330" id="Seg_8564" s="T1329">cop 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T55" id="Seg_8565" s="T54">RUS:gram</ta>
            <ta e="T64" id="Seg_8566" s="T63">RUS:gram</ta>
            <ta e="T72" id="Seg_8567" s="T71">RUS:gram</ta>
            <ta e="T79" id="Seg_8568" s="T78">RUS:cult</ta>
            <ta e="T85" id="Seg_8569" s="T84">RUS:gram</ta>
            <ta e="T106" id="Seg_8570" s="T105">RUS:disc</ta>
            <ta e="T108" id="Seg_8571" s="T107">TURK:core</ta>
            <ta e="T121" id="Seg_8572" s="T120">TURK:core</ta>
            <ta e="T124" id="Seg_8573" s="T123">RUS:mod</ta>
            <ta e="T137" id="Seg_8574" s="T136">RUS:disc</ta>
            <ta e="T139" id="Seg_8575" s="T138">TURK:cult</ta>
            <ta e="T141" id="Seg_8576" s="T140">TURK:cult</ta>
            <ta e="T153" id="Seg_8577" s="T152">RUS:gram</ta>
            <ta e="T175" id="Seg_8578" s="T174">RUS:gram</ta>
            <ta e="T176" id="Seg_8579" s="T175">TURK:core</ta>
            <ta e="T182" id="Seg_8580" s="T181">TURK:cult</ta>
            <ta e="T185" id="Seg_8581" s="T184">TURK:disc</ta>
            <ta e="T197" id="Seg_8582" s="T196">RUS:cult</ta>
            <ta e="T198" id="Seg_8583" s="T197">TURK:disc</ta>
            <ta e="T200" id="Seg_8584" s="T199">RUS:gram</ta>
            <ta e="T205" id="Seg_8585" s="T204">TURK:disc</ta>
            <ta e="T206" id="Seg_8586" s="T205">TURK:cult</ta>
            <ta e="T209" id="Seg_8587" s="T208">RUS:cult</ta>
            <ta e="T214" id="Seg_8588" s="T213">TURK:disc</ta>
            <ta e="T249" id="Seg_8589" s="T248">TURK:core</ta>
            <ta e="T250" id="Seg_8590" s="T249">RUS:gram</ta>
            <ta e="T253" id="Seg_8591" s="T252">TURK:cult</ta>
            <ta e="T255" id="Seg_8592" s="T254">RUS:gram</ta>
            <ta e="T262" id="Seg_8593" s="T261">RUS:gram</ta>
            <ta e="T385" id="Seg_8594" s="T384">TURK:gram(INDEF)</ta>
            <ta e="T393" id="Seg_8595" s="T392">RUS:cult</ta>
            <ta e="T395" id="Seg_8596" s="T394">RUS:gram</ta>
            <ta e="T396" id="Seg_8597" s="T395">TURK:gram(INDEF)</ta>
            <ta e="T404" id="Seg_8598" s="T403">RUS:disc</ta>
            <ta e="T441" id="Seg_8599" s="T440">RUS:gram</ta>
            <ta e="T461" id="Seg_8600" s="T460">RUS:gram</ta>
            <ta e="T471" id="Seg_8601" s="T470">RUS:gram</ta>
            <ta e="T477" id="Seg_8602" s="T476">RUS:gram</ta>
            <ta e="T504" id="Seg_8603" s="T503">TURK:core</ta>
            <ta e="T506" id="Seg_8604" s="T505">TURK:core</ta>
            <ta e="T507" id="Seg_8605" s="T506">TURK:core</ta>
            <ta e="T518" id="Seg_8606" s="T517">RUS:gram(INDEF)</ta>
            <ta e="T522" id="Seg_8607" s="T521">RUS:gram</ta>
            <ta e="T526" id="Seg_8608" s="T525">RUS:gram</ta>
            <ta e="T535" id="Seg_8609" s="T534">RUS:gram</ta>
            <ta e="T541" id="Seg_8610" s="T540">TURK:gram(INDEF)</ta>
            <ta e="T727" id="Seg_8611" s="T726">RUS:gram</ta>
            <ta e="T729" id="Seg_8612" s="T728">RUS:gram</ta>
            <ta e="T731" id="Seg_8613" s="T730">RUS:cult</ta>
            <ta e="T738" id="Seg_8614" s="T737">RUS:gram</ta>
            <ta e="T778" id="Seg_8615" s="T777">TURK:core</ta>
            <ta e="T781" id="Seg_8616" s="T780">TAT:cult</ta>
            <ta e="T786" id="Seg_8617" s="T785">RUS:gram</ta>
            <ta e="T811" id="Seg_8618" s="T810">RUS:gram</ta>
            <ta e="T893" id="Seg_8619" s="T892">RUS:disc</ta>
            <ta e="T906" id="Seg_8620" s="T905">RUS:gram</ta>
            <ta e="T909" id="Seg_8621" s="T908">RUS:mod</ta>
            <ta e="T910" id="Seg_8622" s="T909">TURK:cult</ta>
            <ta e="T922" id="Seg_8623" s="T921">RUS:gram</ta>
            <ta e="T926" id="Seg_8624" s="T925">RUS:gram</ta>
            <ta e="T933" id="Seg_8625" s="T932">RUS:gram</ta>
            <ta e="T937" id="Seg_8626" s="T936">RUS:gram</ta>
            <ta e="T938" id="Seg_8627" s="T937">TURK:disc</ta>
            <ta e="T1271" id="Seg_8628" s="T1270">RUS:disc</ta>
            <ta e="T1278" id="Seg_8629" s="T1277">RUS:gram</ta>
            <ta e="T1281" id="Seg_8630" s="T1280">RUS:cult</ta>
            <ta e="T1294" id="Seg_8631" s="T1293">RUS:gram</ta>
            <ta e="T1299" id="Seg_8632" s="T1298">RUS:gram</ta>
            <ta e="T1318" id="Seg_8633" s="T1317">RUS:gram</ta>
            <ta e="T1320" id="Seg_8634" s="T1319">TURK:disc</ta>
            <ta e="T1323" id="Seg_8635" s="T1322">RUS:cult</ta>
            <ta e="T1326" id="Seg_8636" s="T1325">RUS:gram</ta>
            <ta e="T1452" id="Seg_8637" s="T1451">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T12" id="Seg_8638" s="T1">RUS:ext </ta>
            <ta e="T29" id="Seg_8639" s="T13">RUS:ext </ta>
            <ta e="T52" id="Seg_8640" s="T49">RUS:ext </ta>
            <ta e="T96" id="Seg_8641" s="T91">RUS:ext </ta>
            <ta e="T181" id="Seg_8642" s="T180">RUS:int</ta>
            <ta e="T223" id="Seg_8643" s="T217">RUS:ext </ta>
            <ta e="T233" id="Seg_8644" s="T229">RUS:ext </ta>
            <ta e="T279" id="Seg_8645" s="T265">RUS:ext </ta>
            <ta e="T287" id="Seg_8646" s="T280">RUS:ext </ta>
            <ta e="T300" id="Seg_8647" s="T288">RUS:ext </ta>
            <ta e="T315" id="Seg_8648" s="T301">RUS:ext </ta>
            <ta e="T327" id="Seg_8649" s="T315">RUS:ext </ta>
            <ta e="T343" id="Seg_8650" s="T328">RUS:ext </ta>
            <ta e="T346" id="Seg_8651" s="T344">RUS:ext </ta>
            <ta e="T360" id="Seg_8652" s="T350">RUS:ext </ta>
            <ta e="T552" id="Seg_8653" s="T546">RUS:ext </ta>
            <ta e="T591" id="Seg_8654" s="T589">RUS:ext </ta>
            <ta e="T605" id="Seg_8655" s="T602">RUS:ext </ta>
            <ta e="T616" id="Seg_8656" s="T608">RUS:ext </ta>
            <ta e="T633" id="Seg_8657" s="T618">RUS:ext </ta>
            <ta e="T640" id="Seg_8658" s="T634">RUS:ext </ta>
            <ta e="T643" id="Seg_8659" s="T641">RUS:ext </ta>
            <ta e="T655" id="Seg_8660" s="T647">RUS:ext </ta>
            <ta e="T674" id="Seg_8661" s="T661">RUS:ext </ta>
            <ta e="T683" id="Seg_8662" s="T676">RUS:ext </ta>
            <ta e="T688" id="Seg_8663" s="T684">RUS:ext </ta>
            <ta e="T701" id="Seg_8664" s="T689">RUS:ext </ta>
            <ta e="T703" id="Seg_8665" s="T702">RUS:ext </ta>
            <ta e="T762" id="Seg_8666" s="T758">RUS:ext </ta>
            <ta e="T766" id="Seg_8667" s="T763">RUS:ext </ta>
            <ta e="T839" id="Seg_8668" s="T836">RUS:ext </ta>
            <ta e="T856" id="Seg_8669" s="T840">RUS:ext </ta>
            <ta e="T868" id="Seg_8670" s="T857">RUS:ext </ta>
            <ta e="T877" id="Seg_8671" s="T870">RUS:ext </ta>
            <ta e="T884" id="Seg_8672" s="T878">RUS:ext </ta>
            <ta e="T953" id="Seg_8673" s="T946">RUS:ext </ta>
            <ta e="T957" id="Seg_8674" s="T954">RUS:ext </ta>
            <ta e="T976" id="Seg_8675" s="T959">RUS:ext </ta>
            <ta e="T980" id="Seg_8676" s="T976">RUS:ext </ta>
            <ta e="T984" id="Seg_8677" s="T982">RUS:ext </ta>
            <ta e="T1001" id="Seg_8678" s="T999">RUS:ext </ta>
            <ta e="T1011" id="Seg_8679" s="T1002">RUS:ext </ta>
            <ta e="T1019" id="Seg_8680" s="T1013">RUS:ext </ta>
            <ta e="T1050" id="Seg_8681" s="T1020">RUS:ext </ta>
            <ta e="T1073" id="Seg_8682" s="T1051">RUS:ext </ta>
            <ta e="T1087" id="Seg_8683" s="T1074">RUS:ext </ta>
            <ta e="T1108" id="Seg_8684" s="T1088">RUS:ext </ta>
            <ta e="T1116" id="Seg_8685" s="T1109">RUS:ext </ta>
            <ta e="T1122" id="Seg_8686" s="T1117">RUS:ext </ta>
            <ta e="T1126" id="Seg_8687" s="T1122">RUS:ext </ta>
            <ta e="T1134" id="Seg_8688" s="T1128">RUS:ext </ta>
            <ta e="T1138" id="Seg_8689" s="T1135">RUS:ext </ta>
            <ta e="T1153" id="Seg_8690" s="T1139">RUS:ext </ta>
            <ta e="T1158" id="Seg_8691" s="T1155">RUS:ext </ta>
            <ta e="T1168" id="Seg_8692" s="T1160">RUS:ext </ta>
            <ta e="T1193" id="Seg_8693" s="T1187">RUS:ext </ta>
            <ta e="T1202" id="Seg_8694" s="T1194">RUS:ext </ta>
            <ta e="T1210" id="Seg_8695" s="T1202">RUS:ext </ta>
            <ta e="T1214" id="Seg_8696" s="T1211">RUS:ext </ta>
            <ta e="T1226" id="Seg_8697" s="T1215">RUS:ext </ta>
            <ta e="T1230" id="Seg_8698" s="T1227">RUS:ext </ta>
            <ta e="T1291" id="Seg_8699" s="T1289">RUS:int</ta>
            <ta e="T1306" id="Seg_8700" s="T1305">RUS:int</ta>
            <ta e="T1339" id="Seg_8701" s="T1331">RUS:ext </ta>
            <ta e="T1368" id="Seg_8702" s="T1348">RUS:ext </ta>
            <ta e="T1375" id="Seg_8703" s="T1369">RUS:ext </ta>
            <ta e="T1404" id="Seg_8704" s="T1377">RUS:ext </ta>
            <ta e="T1412" id="Seg_8705" s="T1406">RUS:ext </ta>
            <ta e="T1457" id="Seg_8706" s="T1454">RUS:ext </ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T62" id="Seg_8707" s="T53">Играли, какой-нибудь парень подойдет ко мне, я к нему.</ta>
            <ta e="T67" id="Seg_8708" s="T63">И девушек много было.</ta>
            <ta e="T74" id="Seg_8709" s="T67">Когда я была [молодая], не было парней.</ta>
            <ta e="T89" id="Seg_8710" s="T75">Я пошла туда, в Пьянково вышла замуж, там был мой парень, и я там осталась.</ta>
            <ta e="T119" id="Seg_8711" s="T105">Ну, родители парня приходят к девушке, говорят: "Выходи замуж за нашего сына".</ta>
            <ta e="T123" id="Seg_8712" s="T120">Ее родителей спросят.</ta>
            <ta e="T129" id="Seg_8713" s="T123">Надо девушку спросить, тогда приходит девушка.</ta>
            <ta e="T131" id="Seg_8714" s="T129">"Пойдешь замуж?"</ta>
            <ta e="T135" id="Seg_8715" s="T132">Она говорит: "Пойду".</ta>
            <ta e="T141" id="Seg_8716" s="T136">Ну, потом они принесут водку, хлеб.</ta>
            <ta e="T148" id="Seg_8717" s="T142">Сидят, пьют, потом приезжают на лошади.</ta>
            <ta e="T155" id="Seg_8718" s="T149">Берут ее и везут домой.</ta>
            <ta e="T177" id="Seg_8719" s="T172">Родители говорили.</ta>
            <ta e="T187" id="Seg_8720" s="T178">Потом делали пиво, водки много было, все делали.</ta>
            <ta e="T192" id="Seg_8721" s="T188">Уток жарили, мяса много.</ta>
            <ta e="T201" id="Seg_8722" s="T193">Потом ставили столы, ели все и уходили.</ta>
            <ta e="T209" id="Seg_8723" s="T202">Девушки (и все люди?) пили водку, играли на гармошке.</ta>
            <ta e="T211" id="Seg_8724" s="T210">Плясали.</ta>
            <ta e="T215" id="Seg_8725" s="T212">Потом все уходили.</ta>
            <ta e="T251" id="Seg_8726" s="T247">[И] хорошо жили, и ссорились.</ta>
            <ta e="T256" id="Seg_8727" s="T252">Водки выпьют, так дерутся.</ta>
            <ta e="T263" id="Seg_8728" s="T256">Вцепятся друг другу в волосы и дерутся.</ta>
            <ta e="T378" id="Seg_8729" s="T371">У кого пять было, у кого семь детей.</ta>
            <ta e="T383" id="Seg_8730" s="T379">Много рожали.</ta>
            <ta e="T387" id="Seg_8731" s="T383">Они ничего не делали.</ta>
            <ta e="T392" id="Seg_8732" s="T388">Сейчас люди детей (?).</ta>
            <ta e="T400" id="Seg_8733" s="T392">Идут в больницу и что-то делают, и не рожают детей.</ta>
            <ta e="T410" id="Seg_8734" s="T403">Ну, дети у них болели, умирали, мало оставалось.</ta>
            <ta e="T438" id="Seg_8735" s="T433">Плакали дети.</ta>
            <ta e="T444" id="Seg_8736" s="T440">И болели.</ta>
            <ta e="T462" id="Seg_8737" s="T458">Мать встанет да возьмет.</ta>
            <ta e="T468" id="Seg_8738" s="T463">Даст ему грудь.</ta>
            <ta e="T472" id="Seg_8739" s="T469">Поест, и она его кладет.</ta>
            <ta e="T475" id="Seg_8740" s="T473">Он спит.</ta>
            <ta e="T480" id="Seg_8741" s="T476">И сама ложится и спит.</ta>
            <ta e="T508" id="Seg_8742" s="T502">Слушались родителей.</ta>
            <ta e="T515" id="Seg_8743" s="T509">Я маленькая была, мать скажет: "Пойди".</ta>
            <ta e="T518" id="Seg_8744" s="T516">Пошлет куда-нибудь.</ta>
            <ta e="T524" id="Seg_8745" s="T519">Я стою и (?) там.</ta>
            <ta e="T530" id="Seg_8746" s="T525">А она: "Я тебя побью".</ta>
            <ta e="T537" id="Seg_8747" s="T531">Я тогда побегу и скоро вернусь.</ta>
            <ta e="T544" id="Seg_8748" s="T538">Поэтому она (никогда?) меня не била.</ta>
            <ta e="T562" id="Seg_8749" s="T561">Били.</ta>
            <ta e="T565" id="Seg_8750" s="T563">Сильно, палками.</ta>
            <ta e="T571" id="Seg_8751" s="T566">Меня мать сильно била.</ta>
            <ta e="T577" id="Seg_8752" s="T572">Я очень кричала.</ta>
            <ta e="T727" id="Seg_8753" s="T722">Писать не умею, а…</ta>
            <ta e="T736" id="Seg_8754" s="T728">Но могу… в книжку смотрю, что там написано.</ta>
            <ta e="T741" id="Seg_8755" s="T737">А писать не умею.</ta>
            <ta e="T775" id="Seg_8756" s="T770">Они долго не жили.</ta>
            <ta e="T780" id="Seg_8757" s="T776">Они все время были на улице.</ta>
            <ta e="T782" id="Seg_8758" s="T780">Домов у них не было.</ta>
            <ta e="T784" id="Seg_8759" s="T783">Они мерзли.</ta>
            <ta e="T790" id="Seg_8760" s="T785">И потом лечить их было некому. [?]</ta>
            <ta e="T793" id="Seg_8761" s="T791">Они умирали.</ta>
            <ta e="T797" id="Seg_8762" s="T794">Они мало жили.</ta>
            <ta e="T804" id="Seg_8763" s="T800">У нас не было берез.</ta>
            <ta e="T809" id="Seg_8764" s="T805">Они ставили четыре дерева.</ta>
            <ta e="T813" id="Seg_8765" s="T810">И туда клали.</ta>
            <ta e="T820" id="Seg_8766" s="T814">Потом туда наверх клали камни.</ta>
            <ta e="T829" id="Seg_8767" s="T820">Потом эти деревья подрубали, те падали и придавливали их.</ta>
            <ta e="T835" id="Seg_8768" s="T830">Убивали собственным (?).</ta>
            <ta e="T901" id="Seg_8769" s="T892">Ну, землю копали, клали дерево, дерево выдабливали.</ta>
            <ta e="T911" id="Seg_8770" s="T902">Потом они делали два, и там тоже топором рубили.</ta>
            <ta e="T917" id="Seg_8771" s="T912">Потом они клали его туда и несли.</ta>
            <ta e="T920" id="Seg_8772" s="T918">Клали в землю.</ta>
            <ta e="T924" id="Seg_8773" s="T921">И делали крест.</ta>
            <ta e="T935" id="Seg_8774" s="T925">Потом приходили домой, варили (суп?), и потом ели.</ta>
            <ta e="T939" id="Seg_8775" s="T936">(…).</ta>
            <ta e="T1276" id="Seg_8776" s="T1270">Ну, когда умирает человек, его хоронят.</ta>
            <ta e="T1284" id="Seg_8777" s="T1277">А дух его уходит наверх.</ta>
            <ta e="T1288" id="Seg_8778" s="T1285">Я так знаю.</ta>
            <ta e="T1297" id="Seg_8779" s="T1289">Не знаю, кто как, а я так знаю.</ta>
            <ta e="T1304" id="Seg_8780" s="T1298">И мать мне так говорила.</ta>
            <ta e="T1309" id="Seg_8781" s="T1305">Его душа идет к богу.</ta>
            <ta e="T1317" id="Seg_8782" s="T1312">Бог создал человека на (своей?) земле.</ta>
            <ta e="T1330" id="Seg_8783" s="T1317">И дал ему свой дух, и тот встал и стал человеком.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T12" id="Seg_8784" s="T1">They cannot find (…), for the forest is so thick, until they arrive.</ta>
            <ta e="T29" id="Seg_8785" s="T13">Such mountains, that you cannot glance over, how often are (…) mountains there.</ta>
            <ta e="T52" id="Seg_8786" s="T49">Well, then…</ta>
            <ta e="T62" id="Seg_8787" s="T53">We played, some boy comes to me, I [come] to him.</ta>
            <ta e="T67" id="Seg_8788" s="T63">And there were many girls, too.</ta>
            <ta e="T74" id="Seg_8789" s="T67">When I was [young], there were no boys.</ta>
            <ta e="T89" id="Seg_8790" s="T75">I went there, I married in Pyankovo, there was my boy, and I remained there.</ta>
            <ta e="T96" id="Seg_8791" s="T91">That's what they say (…).</ta>
            <ta e="T119" id="Seg_8792" s="T105">Well, the boy's parents come to the girl, say her: "Marry our son."</ta>
            <ta e="T123" id="Seg_8793" s="T120">They ask [her] parents.</ta>
            <ta e="T129" id="Seg_8794" s="T123">They have to ask the girl, so comes the girl.</ta>
            <ta e="T131" id="Seg_8795" s="T129">"Will you marry?"</ta>
            <ta e="T135" id="Seg_8796" s="T132">She says: "I will."</ta>
            <ta e="T141" id="Seg_8797" s="T136">Then they bring vodka and bread.</ta>
            <ta e="T148" id="Seg_8798" s="T142">They sit and drink, then they come with a horse.</ta>
            <ta e="T155" id="Seg_8799" s="T149">They take her and bring home.</ta>
            <ta e="T177" id="Seg_8800" s="T172">The parents told.</ta>
            <ta e="T187" id="Seg_8801" s="T178">Then they made beer, there was a lot of vodka, they made all sorts of things.</ta>
            <ta e="T192" id="Seg_8802" s="T188">They baked ducks, [there was] a lot of meat.</ta>
            <ta e="T201" id="Seg_8803" s="T193">Then they set tables, ate everything and went away.</ta>
            <ta e="T209" id="Seg_8804" s="T202">Girls (and everyone?) drank vodka, played accordeon.</ta>
            <ta e="T211" id="Seg_8805" s="T210">They danced.</ta>
            <ta e="T215" id="Seg_8806" s="T212">Then everyone left.</ta>
            <ta e="T223" id="Seg_8807" s="T217">Well, I have talked about the wedding.</ta>
            <ta e="T233" id="Seg_8808" s="T229">They made their own beer.</ta>
            <ta e="T251" id="Seg_8809" s="T247">[Sometimes] they lived well, [sometimes] they quarelled.</ta>
            <ta e="T256" id="Seg_8810" s="T252">When they have drunk vodka, they fight.</ta>
            <ta e="T263" id="Seg_8811" s="T256">They grip at one another's hair and fight.</ta>
            <ta e="T279" id="Seg_8812" s="T265">Well, I remember, I can tell you, an old man with an old wife.</ta>
            <ta e="T287" id="Seg_8813" s="T280">Drunk.</ta>
            <ta e="T300" id="Seg_8814" s="T288">They pulled each others hair, pulled, pulled, fell asleep.</ta>
            <ta e="T315" id="Seg_8815" s="T301">In the morning they got up: "Something", he/she said, "is hurting." and he said: "It hurts."</ta>
            <ta e="T327" id="Seg_8816" s="T315">"Maybe we fought, we do not remember, we were that drunk", well.</ta>
            <ta e="T343" id="Seg_8817" s="T328">I remember her, their last name was Tuguin and her name was Vera and he was Vasily.</ta>
            <ta e="T346" id="Seg_8818" s="T344">She called for him.</ta>
            <ta e="T360" id="Seg_8819" s="T350">They were Kamas, I remember them well, they did not live far from us.</ta>
            <ta e="T378" id="Seg_8820" s="T371">Some had five children, some had seven.</ta>
            <ta e="T383" id="Seg_8821" s="T379">That had many children.</ta>
            <ta e="T387" id="Seg_8822" s="T383">They didn't do anything.</ta>
            <ta e="T392" id="Seg_8823" s="T388">Now people (?) children.</ta>
            <ta e="T400" id="Seg_8824" s="T392">They go to the hospital and make something and have no [more] children.</ta>
            <ta e="T410" id="Seg_8825" s="T403">Their children used to fall ill, to die, there survived few of them.</ta>
            <ta e="T438" id="Seg_8826" s="T433">The children cried.</ta>
            <ta e="T444" id="Seg_8827" s="T440">And they fell ill.</ta>
            <ta e="T462" id="Seg_8828" s="T458">The mother gets up and takes it.</ta>
            <ta e="T468" id="Seg_8829" s="T463">She gives it breast.</ta>
            <ta e="T472" id="Seg_8830" s="T469">It eats and she puts him back.</ta>
            <ta e="T475" id="Seg_8831" s="T473">It sleeps.</ta>
            <ta e="T480" id="Seg_8832" s="T476">And she goes to sleep herself.</ta>
            <ta e="T508" id="Seg_8833" s="T502">They listened to their parents.</ta>
            <ta e="T515" id="Seg_8834" s="T509">When I was small, my mother says: "Go."</ta>
            <ta e="T518" id="Seg_8835" s="T516">She sends me somewhere.</ta>
            <ta e="T524" id="Seg_8836" s="T519">I'm staying and (?) there.</ta>
            <ta e="T530" id="Seg_8837" s="T525">She [says]:" I'll beat you."</ta>
            <ta e="T537" id="Seg_8838" s="T531">Then I run and soon come back.</ta>
            <ta e="T544" id="Seg_8839" s="T538">So she (never?) beat me.</ta>
            <ta e="T552" id="Seg_8840" s="T546">Loved (…) Mom as well.</ta>
            <ta e="T562" id="Seg_8841" s="T561">They beat [them].</ta>
            <ta e="T565" id="Seg_8842" s="T563">Strongly, with sticks.</ta>
            <ta e="T571" id="Seg_8843" s="T566">My mother used to beat me strongly.</ta>
            <ta e="T577" id="Seg_8844" s="T572">I shouted very much.</ta>
            <ta e="T591" id="Seg_8845" s="T589">With a stick.</ta>
            <ta e="T605" id="Seg_8846" s="T602">No, they could not.</ta>
            <ta e="T616" id="Seg_8847" s="T608">I do not know, why they could not.</ta>
            <ta e="T633" id="Seg_8848" s="T618">There was a sister who said: They invented their own language.</ta>
            <ta e="T640" id="Seg_8849" s="T634">No, she said, they do not have books.</ta>
            <ta e="T643" id="Seg_8850" s="T641">I do not know.</ta>
            <ta e="T655" id="Seg_8851" s="T647">Well, like I said…</ta>
            <ta e="T674" id="Seg_8852" s="T661">Well, that I cannot write or read.</ta>
            <ta e="T683" id="Seg_8853" s="T676">We only have on there, Alexander Shaybin.</ta>
            <ta e="T688" id="Seg_8854" s="T684">He went there.</ta>
            <ta e="T701" id="Seg_8855" s="T689">When someone was supposed to be taught, they hired a teacher who taught in Russian.</ta>
            <ta e="T703" id="Seg_8856" s="T702">Well.</ta>
            <ta e="T727" id="Seg_8857" s="T722">I cannot write, but…</ta>
            <ta e="T736" id="Seg_8858" s="T728">But I can look in a book, what is written there.</ta>
            <ta e="T741" id="Seg_8859" s="T737">As to write, I cannot.</ta>
            <ta e="T762" id="Seg_8860" s="T758">They did not get old.</ta>
            <ta e="T766" id="Seg_8861" s="T763">I can say it my way,</ta>
            <ta e="T775" id="Seg_8862" s="T770">They didn't live long.</ta>
            <ta e="T780" id="Seg_8863" s="T776">They were always outside.</ta>
            <ta e="T782" id="Seg_8864" s="T780">They had no houses.</ta>
            <ta e="T784" id="Seg_8865" s="T783">They froze.</ta>
            <ta e="T790" id="Seg_8866" s="T785">And then there was nobody to treat them. [?]</ta>
            <ta e="T793" id="Seg_8867" s="T791">They died.</ta>
            <ta e="T797" id="Seg_8868" s="T794">They live[d] little.</ta>
            <ta e="T804" id="Seg_8869" s="T800">We had no birches.</ta>
            <ta e="T809" id="Seg_8870" s="T805">They used to set four trees.</ta>
            <ta e="T813" id="Seg_8871" s="T810">And put [the dead] there.</ta>
            <ta e="T820" id="Seg_8872" s="T814">They they put there stones on the top.</ta>
            <ta e="T829" id="Seg_8873" s="T820">They they cut the trees, they fell and pressed them down.</ta>
            <ta e="T835" id="Seg_8874" s="T830">They killed them with their own (?).</ta>
            <ta e="T839" id="Seg_8875" s="T836">They killed themselves.</ta>
            <ta e="T856" id="Seg_8876" s="T840">When there was no birch tree, a birch grew, they said: "The Russians will come, things will be bad for us."</ta>
            <ta e="T868" id="Seg_8877" s="T857">They make some, put stones there and cut and crushed them.</ta>
            <ta e="T877" id="Seg_8878" s="T870">They buried themselves, they were scared.</ta>
            <ta e="T884" id="Seg_8879" s="T878">That is how the old people told it.</ta>
            <ta e="T901" id="Seg_8880" s="T892">Well, they used to dig the ground, put a tree, they chopped a tree.</ta>
            <ta e="T911" id="Seg_8881" s="T902">Then they made [=cut it in] two, and chopped there with an axe.</ta>
            <ta e="T917" id="Seg_8882" s="T912">Then they put [him] in and carried [him].</ta>
            <ta e="T920" id="Seg_8883" s="T918">They put [it] into the ground.</ta>
            <ta e="T924" id="Seg_8884" s="T921">And made the cross.</ta>
            <ta e="T935" id="Seg_8885" s="T925">Then they came home, boiled (soup?), and then they ate.</ta>
            <ta e="T939" id="Seg_8886" s="T936">(…).</ta>
            <ta e="T953" id="Seg_8887" s="T946">In Russian.</ta>
            <ta e="T957" id="Seg_8888" s="T954">They drank.</ta>
            <ta e="T976" id="Seg_8889" s="T959">To say it in Russian, they will cut a wedge from wood, made it in two halves and stanced chunks.</ta>
            <ta e="T980" id="Seg_8890" s="T976">She does not make planks.</ta>
            <ta e="T984" id="Seg_8891" s="T982">She put it there.</ta>
            <ta e="T1001" id="Seg_8892" s="T999">They probably did.</ta>
            <ta e="T1011" id="Seg_8893" s="T1002">Well, I do not remember, they put crosses with me.</ta>
            <ta e="T1019" id="Seg_8894" s="T1013">But I cannot remember it, I do not know.</ta>
            <ta e="T1050" id="Seg_8895" s="T1020">Here you are, here is hell, on the hill, like the pits here, you move here, there was a graveyard on the corner and back there (the hill).</ta>
            <ta e="T1073" id="Seg_8896" s="T1051">Old, and there, where you put the boundary, there is another old village, there was also a graveyard there, I saw it.</ta>
            <ta e="T1087" id="Seg_8897" s="T1074">There were no crosses, only pits.</ta>
            <ta e="T1108" id="Seg_8898" s="T1088">They called it the old village when they left the Taiga, and they stayed there for the winter and then here, where our village is.</ta>
            <ta e="T1116" id="Seg_8899" s="T1109">This spring was found, that never freezed.</ta>
            <ta e="T1122" id="Seg_8900" s="T1117">In the winter, there is only steam.</ta>
            <ta e="T1126" id="Seg_8901" s="T1122">And no ice.</ta>
            <ta e="T1134" id="Seg_8902" s="T1128">Now it is colder than in the winter.</ta>
            <ta e="T1138" id="Seg_8903" s="T1135">In the winter, it is warm.</ta>
            <ta e="T1153" id="Seg_8904" s="T1139">Here, if a cloth freezes, put it in the river, it will melt, the cloth is frozen.</ta>
            <ta e="T1158" id="Seg_8905" s="T1155">So, (…) frost.</ta>
            <ta e="T1168" id="Seg_8906" s="T1160">And it (…) melted.</ta>
            <ta e="T1193" id="Seg_8907" s="T1187">Well, they came, they remembered, gathered around the table.</ta>
            <ta e="T1202" id="Seg_8908" s="T1194">They made kissel and bread.</ta>
            <ta e="T1210" id="Seg_8909" s="T1202">Well, they did not know potatoes, they made bread.</ta>
            <ta e="T1214" id="Seg_8910" s="T1211">They made their own pasta.</ta>
            <ta e="T1226" id="Seg_8911" s="T1215">Pasta like you can buy in the stores now, they made it themselves.</ta>
            <ta e="T1230" id="Seg_8912" s="T1227">They did not know that.</ta>
            <ta e="T1276" id="Seg_8913" s="T1270">When someone dies, he is buried.</ta>
            <ta e="T1284" id="Seg_8914" s="T1277">And his soul goes up.</ta>
            <ta e="T1288" id="Seg_8915" s="T1285">I know so.</ta>
            <ta e="T1297" id="Seg_8916" s="T1289">I don't know who [knows] what, I know so.</ta>
            <ta e="T1304" id="Seg_8917" s="T1298">My mother told me this, too.</ta>
            <ta e="T1309" id="Seg_8918" s="T1305">His soul goes to God.</ta>
            <ta e="T1317" id="Seg_8919" s="T1312">God created man on (his own?) land.</ta>
            <ta e="T1330" id="Seg_8920" s="T1317">And gave him his own spirit, and that stood up and became a man.</ta>
            <ta e="T1339" id="Seg_8921" s="T1331">Did you understand what I said?</ta>
            <ta e="T1368" id="Seg_8922" s="T1348">I did not tell you before, what I am saying now.</ta>
            <ta e="T1375" id="Seg_8923" s="T1369">This I say with the bible.</ta>
            <ta e="T1404" id="Seg_8924" s="T1377">When God created his own and put them on earth, he created them in his image and gave his spirit to them and that is how they came alive.</ta>
            <ta e="T1412" id="Seg_8925" s="T1406">That is how I describe it.</ta>
            <ta e="T1457" id="Seg_8926" s="T1454">Enough, I say.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T12" id="Seg_8927" s="T1">(…) können sie nicht finden, ein solcher Wald ist dicht, bis sie selbst ankommen.</ta>
            <ta e="T29" id="Seg_8928" s="T13">Solche Berge, die man nicht überblicken kann, wie oft sind (…) Berge dort.</ta>
            <ta e="T52" id="Seg_8929" s="T49">Nun, denn…</ta>
            <ta e="T62" id="Seg_8930" s="T53">Wir haben gespielt, ein Junge kommt zu mir, ich [komme] zu ihm.</ta>
            <ta e="T67" id="Seg_8931" s="T63">Und dort waren auch viele Mädchen.</ta>
            <ta e="T74" id="Seg_8932" s="T67">Als ich [jung] war, gab es keine Jungen.</ta>
            <ta e="T89" id="Seg_8933" s="T75">Ich bin dorthin gegangen, ich habe in Pjankowo geheiratet, dort war mein Junge und ich bin dort geblieben.</ta>
            <ta e="T96" id="Seg_8934" s="T91">So sagt man (…).</ta>
            <ta e="T119" id="Seg_8935" s="T105">Nun, die Eltern des Jungen kamen zum Mädchen, sagten zu ihr: "Heirate unseren Sohn."</ta>
            <ta e="T123" id="Seg_8936" s="T120">Sie fragen [ihre] Eltern.</ta>
            <ta e="T129" id="Seg_8937" s="T123">Sie müssen das Mädchen fragen, so kommt das Mädchen.</ta>
            <ta e="T131" id="Seg_8938" s="T129">"Wirst du heiraten?"</ta>
            <ta e="T135" id="Seg_8939" s="T132">Sie sagt: "Ich werde."</ta>
            <ta e="T141" id="Seg_8940" s="T136">Dann bringen sie Wodka und Brot.</ta>
            <ta e="T148" id="Seg_8941" s="T142">Sie sitzen und trinken, dann kommen sie mit einem Pferd. </ta>
            <ta e="T155" id="Seg_8942" s="T149">Sie nehmen sie und bringen sie nach Hause.</ta>
            <ta e="T177" id="Seg_8943" s="T172">Die Eltern erzählten.</ta>
            <ta e="T187" id="Seg_8944" s="T178">Dann machten sie Bier, es gab viel Wodka, sie machten alle möglichen Dinge.</ta>
            <ta e="T192" id="Seg_8945" s="T188">Sie bucken Enten, es gab viel Fleisch.</ta>
            <ta e="T201" id="Seg_8946" s="T193">Dann deckten sie den Tisch, aßen alles und gingen fort.</ta>
            <ta e="T209" id="Seg_8947" s="T202">Mädchen (und alle?) tranken Wodka, spielen Akkordeon.</ta>
            <ta e="T211" id="Seg_8948" s="T210">Sie tanzten.</ta>
            <ta e="T215" id="Seg_8949" s="T212">Dann ging jeder.</ta>
            <ta e="T223" id="Seg_8950" s="T217">Nun, ich habe über die Hochzeit gesprochen.</ta>
            <ta e="T233" id="Seg_8951" s="T229">Sie haben selbst Bier gemacht.</ta>
            <ta e="T251" id="Seg_8952" s="T247">[Manchmal] lebten sie gut, [manchmal] stritten sie.</ta>
            <ta e="T256" id="Seg_8953" s="T252">Wenn sie Wodka getrunken hatten, stritten sie.</ta>
            <ta e="T263" id="Seg_8954" s="T256">Sie zogen sich gegenseitig an den Haaren und stritten.</ta>
            <ta e="T279" id="Seg_8955" s="T265">Also ich erinnere mich, ich kann es dir sagen, ein alter Mann mit einer alten Frau.</ta>
            <ta e="T287" id="Seg_8956" s="T280">Betrunken, </ta>
            <ta e="T300" id="Seg_8957" s="T288">Sie zogen sich an den Haaren, zogen, zogen, schliefen ein. </ta>
            <ta e="T315" id="Seg_8958" s="T301">Am Morgen standen sie auf, "Etwas", sagt er/sie?, "tut hier weh", und er sagt: "Es tut weh."</ta>
            <ta e="T327" id="Seg_8959" s="T315">"Vielleicht haben wir uns gestritten, wir wissen es nicht, so betrunken waren wir", nun.</ta>
            <ta e="T343" id="Seg_8960" s="T328">Ich erinnere mich an sie, ihr Nachname war Tuguin und ihr Name war Vera und er war Vasily.</ta>
            <ta e="T346" id="Seg_8961" s="T344">Sie rief ihn.</ta>
            <ta e="T360" id="Seg_8962" s="T350">Sie waren Kamassen, ich erinnere mich noch gut, lebten nicht weit von uns.</ta>
            <ta e="T378" id="Seg_8963" s="T371">Einige hatten fünf Kinder, einige hatten sieben.</ta>
            <ta e="T383" id="Seg_8964" s="T379">Sie hatten viele Kinder.</ta>
            <ta e="T387" id="Seg_8965" s="T383">Sie machten nichts.</ta>
            <ta e="T392" id="Seg_8966" s="T388">Jetzt Leute (?) Kinder.</ta>
            <ta e="T400" id="Seg_8967" s="T392">Sie gehen ins Krankenhaus und machen etwas und haben keine Kinder [mehr].</ta>
            <ta e="T410" id="Seg_8968" s="T403">Ihre Kinder wurden früher krank, starben, nur ein paar wenige überlebten.</ta>
            <ta e="T438" id="Seg_8969" s="T433">Die Kinder weinten.</ta>
            <ta e="T444" id="Seg_8970" s="T440">Und sie wurden krank.</ta>
            <ta e="T462" id="Seg_8971" s="T458">Die Mutter steht auf und nimmt es hoch.</ta>
            <ta e="T468" id="Seg_8972" s="T463">Sie stillt es.</ta>
            <ta e="T472" id="Seg_8973" s="T469">Es isst und sie legt es zurück.</ta>
            <ta e="T475" id="Seg_8974" s="T473">Es schläft.</ta>
            <ta e="T480" id="Seg_8975" s="T476">Und sie geht selbst schlafen.</ta>
            <ta e="T508" id="Seg_8976" s="T502">Sie hören auf ihre Eltern.</ta>
            <ta e="T515" id="Seg_8977" s="T509">Als ich klein war, sagte meine Mutter: "Geh."</ta>
            <ta e="T518" id="Seg_8978" s="T516">Sie schickte mich irgendwohin.</ta>
            <ta e="T524" id="Seg_8979" s="T519">Ich blieb und (?) dort.</ta>
            <ta e="T530" id="Seg_8980" s="T525">Sie [sagt]: "Ich werde dich schlagen."</ta>
            <ta e="T537" id="Seg_8981" s="T531">Dann bin ich gerannt und bald zurückgekommen.</ta>
            <ta e="T544" id="Seg_8982" s="T538">So hat sie mich (niemals?) geschlagen.</ta>
            <ta e="T552" id="Seg_8983" s="T546">Liebte (…) auch Mama.</ta>
            <ta e="T562" id="Seg_8984" s="T561">Sie schlugen [sie].</ta>
            <ta e="T565" id="Seg_8985" s="T563">Stark, mit Stöcken.</ta>
            <ta e="T571" id="Seg_8986" s="T566">Meine Mutter schlug mich doll.</ta>
            <ta e="T577" id="Seg_8987" s="T572">Ich schrie viel.</ta>
            <ta e="T591" id="Seg_8988" s="T589">Mit einem Stock.</ta>
            <ta e="T605" id="Seg_8989" s="T602">Nein, konnten sie nicht. </ta>
            <ta e="T616" id="Seg_8990" s="T608">Ich weiß nicht, warum sie es nicht getan haben.</ta>
            <ta e="T633" id="Seg_8991" s="T618">Da stand eine Schwester, die sagte: Sie haben ihre eigene Sprache erfunden. </ta>
            <ta e="T640" id="Seg_8992" s="T634">Nein, sagte sie, sie haben keine Bücher. </ta>
            <ta e="T643" id="Seg_8993" s="T641">Ich weiß nicht. </ta>
            <ta e="T655" id="Seg_8994" s="T647">Nun, ich habe gesagt…</ta>
            <ta e="T674" id="Seg_8995" s="T661">Nun, dass ich weder schreiben noch lesen kann. </ta>
            <ta e="T683" id="Seg_8996" s="T676">Nur einen haben wir dort, Alexander Shaybin.</ta>
            <ta e="T688" id="Seg_8997" s="T684">Also ging er dorthin.</ta>
            <ta e="T701" id="Seg_8998" s="T689">Wenn jemand unterrichtet werden muss, stellen sie einen Lehrer ein, der auf Russisch unterrichtet. </ta>
            <ta e="T703" id="Seg_8999" s="T702">Nun.</ta>
            <ta e="T727" id="Seg_9000" s="T722">Ich kann nicht schreiben, aber…</ta>
            <ta e="T736" id="Seg_9001" s="T728">Aber ich kann in einem Buch sehen, was dort geschrieben steht.</ta>
            <ta e="T741" id="Seg_9002" s="T737">Aber schreiben kann ich nicht.</ta>
            <ta e="T762" id="Seg_9003" s="T758">Sie wurden nicht alt.</ta>
            <ta e="T766" id="Seg_9004" s="T763">Ich kann es auf meine Weise sagen. </ta>
            <ta e="T775" id="Seg_9005" s="T770">Sie lebten nicht lange.</ta>
            <ta e="T780" id="Seg_9006" s="T776">Sie waren immer draußen.</ta>
            <ta e="T782" id="Seg_9007" s="T780">Sie hatten keine Häuser.</ta>
            <ta e="T784" id="Seg_9008" s="T783">Sie erfroren.</ta>
            <ta e="T790" id="Seg_9009" s="T785">Und dann gab es niemanden um sie zu behandeln. [?]</ta>
            <ta e="T793" id="Seg_9010" s="T791">Sie starben.</ta>
            <ta e="T797" id="Seg_9011" s="T794">Sie lebten kurz.</ta>
            <ta e="T804" id="Seg_9012" s="T800">Wir hatten keine Birken.</ta>
            <ta e="T809" id="Seg_9013" s="T805">Sie haben früher vier Bäume gepflanzt.</ta>
            <ta e="T813" id="Seg_9014" s="T810">Und haben [die Toten] dorthin gebracht.</ta>
            <ta e="T820" id="Seg_9015" s="T814">Dann setzten sie Steine oben drauf.</ta>
            <ta e="T829" id="Seg_9016" s="T820">Dann fällten sie die Bäume, sie fielen und pressten sie runter.</ta>
            <ta e="T835" id="Seg_9017" s="T830">Sie brachten sie mit ihrem eigenen (?) um.</ta>
            <ta e="T839" id="Seg_9018" s="T836">Sie haben sich umgebracht.</ta>
            <ta e="T856" id="Seg_9019" s="T840">Als es dort keine Birke gab, wuchs die Birke, sie sagten: "Die Russen werden kommen, es wird schlecht für uns."</ta>
            <ta e="T868" id="Seg_9020" s="T857">Sie machten solche, legten Steine dorthin und zerschnitten und zerdrückten sie.</ta>
            <ta e="T877" id="Seg_9021" s="T870">Also haben sie sich selbt begraben, sie hatten Angst. </ta>
            <ta e="T884" id="Seg_9022" s="T878">Dies wurde auch von alten Leuten erzählt.</ta>
            <ta e="T901" id="Seg_9023" s="T892">Früher haben sie den Boden gegraben, einen Baum gepflanzt und einen Baum gefällt.</ta>
            <ta e="T911" id="Seg_9024" s="T902">Dann machten sie es in zwei Teile und hackten es mit einer Axt.</ta>
            <ta e="T917" id="Seg_9025" s="T912">Dann legten sie ihn hinein und trugen ihn.</ta>
            <ta e="T920" id="Seg_9026" s="T918">Sie legten es in den Boden.</ta>
            <ta e="T924" id="Seg_9027" s="T921">Und machten das Kreuz.</ta>
            <ta e="T935" id="Seg_9028" s="T925">Dann kamen sie nach Hause, kochten (Suppe?) und aßen dann.</ta>
            <ta e="T939" id="Seg_9029" s="T936">(…)</ta>
            <ta e="T953" id="Seg_9030" s="T946">Auf Russisch. </ta>
            <ta e="T957" id="Seg_9031" s="T954">Sie haben getrunken. </ta>
            <ta e="T976" id="Seg_9032" s="T959">Um auf russisch zu sagen, sie werden sie einen Keil aus Holz schneiden, ihn in zwei Hälften hacken und die Klötze ausstechen.</ta>
            <ta e="T980" id="Seg_9033" s="T976">Sie machten keine Bretter. </ta>
            <ta e="T984" id="Seg_9034" s="T982">Sie haben es dort hingelegt.</ta>
            <ta e="T1001" id="Seg_9035" s="T999">Sie haben es wahrscheinlich getan.</ta>
            <ta e="T1011" id="Seg_9036" s="T1002">Nun, ich erinnere mich nicht, sie haben mich bekreuzigt.</ta>
            <ta e="T1019" id="Seg_9037" s="T1013">Aber ich kann mich nicht an sie erinnern, ich weiß es nicht.</ta>
            <ta e="T1050" id="Seg_9038" s="T1020">Hier bist du, hier ist die Hölle, auf dem Hügel, wie die Gruben hier, du ziehst hierher, da war ein Friedhof an der Ecke und da hinten (der Hügel).</ta>
            <ta e="T1073" id="Seg_9039" s="T1051">Alt, und dann dort, wo du die Grenze gezogen hast, dort ist noch eine alte Siedlung, dort gibt es auch einen Friedhof, ich habe es dort gesehen.</ta>
            <ta e="T1087" id="Seg_9040" s="T1074">Es gab keine Kreuze, nur Gruben.</ta>
            <ta e="T1108" id="Seg_9041" s="T1088">Sie nannten die alte Siedlung wie sie die Taiga verließen, und standen dort für den Winter und dann hier, wo unser Dorf ist.</ta>
            <ta e="T1116" id="Seg_9042" s="T1109">Diese Quelle wurde gefunden, sie friert nie ein. </ta>
            <ta e="T1122" id="Seg_9043" s="T1117">Im Winter kommt nur Dampf.</ta>
            <ta e="T1126" id="Seg_9044" s="T1122">Und kein Eis. </ta>
            <ta e="T1134" id="Seg_9045" s="T1128">Jetzt ist sie kälter als im Winter. </ta>
            <ta e="T1138" id="Seg_9046" s="T1135">Im Winter ist sie warm. </ta>
            <ta e="T1153" id="Seg_9047" s="T1139">Hier friert der Lappen, leg ihn in den Fluss, er wird im Fluss auftauen, der Lappen ist gefroren.</ta>
            <ta e="T1158" id="Seg_9048" s="T1155">Also (…) Frost.</ta>
            <ta e="T1168" id="Seg_9049" s="T1160">Und es war (…) geschmolzen.</ta>
            <ta e="T1193" id="Seg_9050" s="T1187">Nun, sie kamen, erinnerten sich, versammelten sich am Tisch. </ta>
            <ta e="T1202" id="Seg_9051" s="T1194">Sie machten Kissel und Brot. </ta>
            <ta e="T1210" id="Seg_9052" s="T1202">Nun, sie kannten keine Kartoffeln, sie machten Brot.</ta>
            <ta e="T1214" id="Seg_9053" s="T1211">Sie haben selbst Nudeln gemacht.</ta>
            <ta e="T1226" id="Seg_9054" s="T1215">Wie jetzt gab es Nudeln in den Läden, sie machten sie selbst. </ta>
            <ta e="T1230" id="Seg_9055" s="T1227">Sie wussten das nicht. </ta>
            <ta e="T1276" id="Seg_9056" s="T1270">Wenn jemand stirbt, wird er begraben.</ta>
            <ta e="T1284" id="Seg_9057" s="T1277">Und seine Seele steigt auf.</ta>
            <ta e="T1288" id="Seg_9058" s="T1285">Ich weiß das.</ta>
            <ta e="T1297" id="Seg_9059" s="T1289">Ich weiß nicht wer was [weiß], ich weiß das.</ta>
            <ta e="T1304" id="Seg_9060" s="T1298">Meine Mutter hat es mir auch erzählt.</ta>
            <ta e="T1309" id="Seg_9061" s="T1305">Seine Seele geht zu Gott.</ta>
            <ta e="T1317" id="Seg_9062" s="T1312">Gott schuf den Menschen auf (seinem eigenen?) Land.</ta>
            <ta e="T1330" id="Seg_9063" s="T1317">Und gab ihm seinen eigenen Geist, und dieser stand auf und wurde ein Mann.</ta>
            <ta e="T1339" id="Seg_9064" s="T1331">Verstehst du, was ich dir gesagt habe?</ta>
            <ta e="T1368" id="Seg_9065" s="T1348">Das habe ich dir nicht erzählt, das, worüber ich jetzt spreche. </ta>
            <ta e="T1375" id="Seg_9066" s="T1369">Das sage ich mit der Bibel.</ta>
            <ta e="T1404" id="Seg_9067" s="T1377">Als Gott die Seinen schuf und auf die Erde bließ, schuhf er sie nach seinem Ebenbild und hauchte ihnen seinen Geist ein und so wurden sie lebendig. </ta>
            <ta e="T1412" id="Seg_9068" s="T1406">So übermittel ich das. </ta>
            <ta e="T1457" id="Seg_9069" s="T1454">Genug, sage ich.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T74" id="Seg_9070" s="T67">[GVY:] "When I was about to marry, there were no unmarried boys in our village"?</ta>
            <ta e="T468" id="Seg_9071" s="T463">[GVY:] Cf. emer- 'saugen', emetər- 'stillen (D 17b)</ta>
            <ta e="T472" id="Seg_9072" s="T469">[GVY:] Amnoj?</ta>
            <ta e="T565" id="Seg_9073" s="T563">[GVY:] probably, with rods.</ta>
            <ta e="T571" id="Seg_9074" s="T566">[GVY:] the plural form is unclear</ta>
            <ta e="T736" id="Seg_9075" s="T728">[GVY:] That is, "I can read."</ta>
            <ta e="T790" id="Seg_9076" s="T785">[GVY:] dʼazirzittə-to?</ta>
            <ta e="T797" id="Seg_9077" s="T794">[GVY:] amnolaʔpiʔi?</ta>
            <ta e="T804" id="Seg_9078" s="T800">[GVY:] pe = pa?</ta>
            <ta e="T935" id="Seg_9079" s="T925">[GVY:] mĭ = mĭje?</ta>
            <ta e="T1284" id="Seg_9080" s="T1277">[GVY:] nʼuktə.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T404" start="T401">
            <tli id="T401.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T1345" start="T1333">
            <tli id="T1333.tx-KA.1" />
            <tli id="T1333.tx-KA.2" />
            <tli id="T1333.tx-KA.3" />
            <tli id="T1333.tx-KA.4" />
            <tli id="T1333.tx-KA.5" />
            <tli id="T1333.tx-KA.6" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T15" id="Seg_9081" n="sc" s="T11">
               <ts e="T15" id="Seg_9083" n="HIAT:u" s="T11">
                  <nts id="Seg_9084" n="HIAT:ip">(</nts>
                  <nts id="Seg_9085" n="HIAT:ip">(</nts>
                  <ats e="T15" id="Seg_9086" n="HIAT:non-pho" s="T11">…</ats>
                  <nts id="Seg_9087" n="HIAT:ip">)</nts>
                  <nts id="Seg_9088" n="HIAT:ip">)</nts>
                  <nts id="Seg_9089" n="HIAT:ip">.</nts>
                  <nts id="Seg_9090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_9091" n="sc" s="T30">
               <ts e="T31" id="Seg_9093" n="HIAT:u" s="T30">
                  <nts id="Seg_9094" n="HIAT:ip">(</nts>
                  <nts id="Seg_9095" n="HIAT:ip">(</nts>
                  <ats e="T31" id="Seg_9096" n="HIAT:non-pho" s="T30">…</ats>
                  <nts id="Seg_9097" n="HIAT:ip">)</nts>
                  <nts id="Seg_9098" n="HIAT:ip">)</nts>
                  <nts id="Seg_9099" n="HIAT:ip">.</nts>
                  <nts id="Seg_9100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_9101" n="sc" s="T32">
               <ts e="T33" id="Seg_9103" n="HIAT:u" s="T32">
                  <nts id="Seg_9104" n="HIAT:ip">(</nts>
                  <nts id="Seg_9105" n="HIAT:ip">(</nts>
                  <ats e="T33" id="Seg_9106" n="HIAT:non-pho" s="T32">…</ats>
                  <nts id="Seg_9107" n="HIAT:ip">)</nts>
                  <nts id="Seg_9108" n="HIAT:ip">)</nts>
                  <nts id="Seg_9109" n="HIAT:ip">.</nts>
                  <nts id="Seg_9110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_9111" n="sc" s="T34">
               <ts e="T50" id="Seg_9113" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_9115" n="HIAT:w" s="T34">Как</ts>
                  <nts id="Seg_9116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_9118" n="HIAT:w" s="T35">молодые</ts>
                  <nts id="Seg_9119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_9121" n="HIAT:w" s="T36">между</ts>
                  <nts id="Seg_9122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9123" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_9125" n="HIAT:w" s="T37">со-</ts>
                  <nts id="Seg_9126" n="HIAT:ip">)</nts>
                  <nts id="Seg_9127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_9129" n="HIAT:w" s="T38">собой</ts>
                  <nts id="Seg_9130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_9132" n="HIAT:w" s="T39">сообщались</ts>
                  <nts id="Seg_9133" n="HIAT:ip">,</nts>
                  <nts id="Seg_9134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_9136" n="HIAT:w" s="T40">когда</ts>
                  <nts id="Seg_9137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9138" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_9140" n="HIAT:w" s="T41">вы</ts>
                  <nts id="Seg_9141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_9143" n="HIAT:w" s="T42">были</ts>
                  <nts id="Seg_9144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_9146" n="HIAT:w" s="T43">еще</ts>
                  <nts id="Seg_9147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_9149" n="HIAT:w" s="T44">молоды=</ts>
                  <nts id="Seg_9150" n="HIAT:ip">)</nts>
                  <nts id="Seg_9151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_9153" n="HIAT:w" s="T45">ты</ts>
                  <nts id="Seg_9154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_9156" n="HIAT:w" s="T46">была</ts>
                  <nts id="Seg_9157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_9159" n="HIAT:w" s="T47">еще</ts>
                  <nts id="Seg_9160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_9162" n="HIAT:w" s="T48">молода</ts>
                  <nts id="Seg_9163" n="HIAT:ip">.</nts>
                  <nts id="Seg_9164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_9165" n="sc" s="T90">
               <ts e="T97" id="Seg_9167" n="HIAT:u" s="T90">
                  <nts id="Seg_9168" n="HIAT:ip">(</nts>
                  <nts id="Seg_9169" n="HIAT:ip">(</nts>
                  <ats e="T97" id="Seg_9170" n="HIAT:non-pho" s="T90">…</ats>
                  <nts id="Seg_9171" n="HIAT:ip">)</nts>
                  <nts id="Seg_9172" n="HIAT:ip">)</nts>
                  <nts id="Seg_9173" n="HIAT:ip">.</nts>
                  <nts id="Seg_9174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T99" id="Seg_9175" n="sc" s="T98">
               <ts e="T99" id="Seg_9177" n="HIAT:u" s="T98">
                  <nts id="Seg_9178" n="HIAT:ip">(</nts>
                  <nts id="Seg_9179" n="HIAT:ip">(</nts>
                  <ats e="T99" id="Seg_9180" n="HIAT:non-pho" s="T98">…</ats>
                  <nts id="Seg_9181" n="HIAT:ip">)</nts>
                  <nts id="Seg_9182" n="HIAT:ip">)</nts>
                  <nts id="Seg_9183" n="HIAT:ip">.</nts>
                  <nts id="Seg_9184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_9185" n="sc" s="T100">
               <ts e="T104" id="Seg_9187" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_9189" n="HIAT:w" s="T100">Ну</ts>
                  <nts id="Seg_9190" n="HIAT:ip">,</nts>
                  <nts id="Seg_9191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_9193" n="HIAT:w" s="T101">насчет</ts>
                  <nts id="Seg_9194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_9196" n="HIAT:w" s="T102">свадьб</ts>
                  <nts id="Seg_9197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_9199" n="HIAT:w" s="T103">спрашивают</ts>
                  <nts id="Seg_9200" n="HIAT:ip">.</nts>
                  <nts id="Seg_9201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T157" id="Seg_9202" n="sc" s="T156">
               <ts e="T157" id="Seg_9204" n="HIAT:u" s="T156">
                  <nts id="Seg_9205" n="HIAT:ip">(</nts>
                  <nts id="Seg_9206" n="HIAT:ip">(</nts>
                  <ats e="T157" id="Seg_9207" n="HIAT:non-pho" s="T156">…</ats>
                  <nts id="Seg_9208" n="HIAT:ip">)</nts>
                  <nts id="Seg_9209" n="HIAT:ip">)</nts>
                  <nts id="Seg_9210" n="HIAT:ip">.</nts>
                  <nts id="Seg_9211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T159" id="Seg_9212" n="sc" s="T158">
               <ts e="T159" id="Seg_9214" n="HIAT:u" s="T158">
                  <nts id="Seg_9215" n="HIAT:ip">(</nts>
                  <nts id="Seg_9216" n="HIAT:ip">(</nts>
                  <ats e="T159" id="Seg_9217" n="HIAT:non-pho" s="T158">…</ats>
                  <nts id="Seg_9218" n="HIAT:ip">)</nts>
                  <nts id="Seg_9219" n="HIAT:ip">)</nts>
                  <nts id="Seg_9220" n="HIAT:ip">.</nts>
                  <nts id="Seg_9221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T171" id="Seg_9222" n="sc" s="T160">
               <ts e="T171" id="Seg_9224" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_9226" n="HIAT:w" s="T160">Как</ts>
                  <nts id="Seg_9227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_9229" n="HIAT:w" s="T161">они</ts>
                  <nts id="Seg_9230" n="HIAT:ip">,</nts>
                  <nts id="Seg_9231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_9233" n="HIAT:w" s="T162">сами</ts>
                  <nts id="Seg_9234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_9236" n="HIAT:w" s="T163">решали</ts>
                  <nts id="Seg_9237" n="HIAT:ip">,</nts>
                  <nts id="Seg_9238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_9240" n="HIAT:w" s="T164">с</ts>
                  <nts id="Seg_9241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_9243" n="HIAT:w" s="T165">кем</ts>
                  <nts id="Seg_9244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_9246" n="HIAT:w" s="T166">жениться</ts>
                  <nts id="Seg_9247" n="HIAT:ip">,</nts>
                  <nts id="Seg_9248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_9250" n="HIAT:w" s="T167">или</ts>
                  <nts id="Seg_9251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_9253" n="HIAT:w" s="T168">это</ts>
                  <nts id="Seg_9254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_9256" n="HIAT:w" s="T169">родители</ts>
                  <nts id="Seg_9257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_9259" n="HIAT:w" s="T170">решали</ts>
                  <nts id="Seg_9260" n="HIAT:ip">?</nts>
                  <nts id="Seg_9261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_9262" n="sc" s="T216">
               <ts e="T224" id="Seg_9264" n="HIAT:u" s="T216">
                  <nts id="Seg_9265" n="HIAT:ip">(</nts>
                  <nts id="Seg_9266" n="HIAT:ip">(</nts>
                  <ats e="T224" id="Seg_9267" n="HIAT:non-pho" s="T216">…</ats>
                  <nts id="Seg_9268" n="HIAT:ip">)</nts>
                  <nts id="Seg_9269" n="HIAT:ip">)</nts>
                  <nts id="Seg_9270" n="HIAT:ip">.</nts>
                  <nts id="Seg_9271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_9272" n="sc" s="T225">
               <ts e="T227" id="Seg_9274" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_9276" n="HIAT:w" s="T225">Нет</ts>
                  <nts id="Seg_9277" n="HIAT:ip">,</nts>
                  <nts id="Seg_9278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_9280" n="HIAT:w" s="T226">ничего</ts>
                  <nts id="Seg_9281" n="HIAT:ip">.</nts>
                  <nts id="Seg_9282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T234" id="Seg_9283" n="sc" s="T228">
               <ts e="T234" id="Seg_9285" n="HIAT:u" s="T228">
                  <nts id="Seg_9286" n="HIAT:ip">(</nts>
                  <nts id="Seg_9287" n="HIAT:ip">(</nts>
                  <ats e="T234" id="Seg_9288" n="HIAT:non-pho" s="T228">…</ats>
                  <nts id="Seg_9289" n="HIAT:ip">)</nts>
                  <nts id="Seg_9290" n="HIAT:ip">)</nts>
                  <nts id="Seg_9291" n="HIAT:ip">.</nts>
                  <nts id="Seg_9292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T236" id="Seg_9293" n="sc" s="T235">
               <ts e="T236" id="Seg_9295" n="HIAT:u" s="T235">
                  <nts id="Seg_9296" n="HIAT:ip">(</nts>
                  <nts id="Seg_9297" n="HIAT:ip">(</nts>
                  <ats e="T236" id="Seg_9298" n="HIAT:non-pho" s="T235">…</ats>
                  <nts id="Seg_9299" n="HIAT:ip">)</nts>
                  <nts id="Seg_9300" n="HIAT:ip">)</nts>
                  <nts id="Seg_9301" n="HIAT:ip">.</nts>
                  <nts id="Seg_9302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T246" id="Seg_9303" n="sc" s="T237">
               <ts e="T246" id="Seg_9305" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_9307" n="HIAT:w" s="T237">Как</ts>
                  <nts id="Seg_9308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_9310" n="HIAT:w" s="T238">муж</ts>
                  <nts id="Seg_9311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_9313" n="HIAT:w" s="T239">и</ts>
                  <nts id="Seg_9314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_9316" n="HIAT:w" s="T240">жена</ts>
                  <nts id="Seg_9317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_9319" n="HIAT:w" s="T241">жили</ts>
                  <nts id="Seg_9320" n="HIAT:ip">,</nts>
                  <nts id="Seg_9321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_9323" n="HIAT:w" s="T242">хорошо</ts>
                  <nts id="Seg_9324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_9326" n="HIAT:w" s="T243">обычно</ts>
                  <nts id="Seg_9327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_9329" n="HIAT:w" s="T244">или</ts>
                  <nts id="Seg_9330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_9332" n="HIAT:w" s="T245">как</ts>
                  <nts id="Seg_9333" n="HIAT:ip">?</nts>
                  <nts id="Seg_9334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T268" id="Seg_9335" n="sc" s="T264">
               <ts e="T268" id="Seg_9337" n="HIAT:u" s="T264">
                  <nts id="Seg_9338" n="HIAT:ip">(</nts>
                  <nts id="Seg_9339" n="HIAT:ip">(</nts>
                  <ats e="T268" id="Seg_9340" n="HIAT:non-pho" s="T264">…</ats>
                  <nts id="Seg_9341" n="HIAT:ip">)</nts>
                  <nts id="Seg_9342" n="HIAT:ip">)</nts>
                  <nts id="Seg_9343" n="HIAT:ip">.</nts>
                  <nts id="Seg_9344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T302" id="Seg_9345" n="sc" s="T300">
               <ts e="T302" id="Seg_9347" n="HIAT:u" s="T300">
                  <nts id="Seg_9348" n="HIAT:ip">(</nts>
                  <nts id="Seg_9349" n="HIAT:ip">(</nts>
                  <ats e="T302" id="Seg_9350" n="HIAT:non-pho" s="T300">LAUGH</ats>
                  <nts id="Seg_9351" n="HIAT:ip">)</nts>
                  <nts id="Seg_9352" n="HIAT:ip">)</nts>
                  <nts id="Seg_9353" n="HIAT:ip">.</nts>
                  <nts id="Seg_9354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_9355" n="sc" s="T347">
               <ts e="T350" id="Seg_9357" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_9359" n="HIAT:w" s="T347">Они</ts>
                  <nts id="Seg_9360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_9362" n="HIAT:w" s="T348">были</ts>
                  <nts id="Seg_9363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_9365" n="HIAT:w" s="T349">камасинцы</ts>
                  <nts id="Seg_9366" n="HIAT:ip">?</nts>
                  <nts id="Seg_9367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T362" id="Seg_9368" n="sc" s="T361">
               <ts e="T362" id="Seg_9370" n="HIAT:u" s="T361">
                  <nts id="Seg_9371" n="HIAT:ip">(</nts>
                  <nts id="Seg_9372" n="HIAT:ip">(</nts>
                  <ats e="T362" id="Seg_9373" n="HIAT:non-pho" s="T361">…</ats>
                  <nts id="Seg_9374" n="HIAT:ip">)</nts>
                  <nts id="Seg_9375" n="HIAT:ip">)</nts>
                  <nts id="Seg_9376" n="HIAT:ip">.</nts>
                  <nts id="Seg_9377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T364" id="Seg_9378" n="sc" s="T363">
               <ts e="T364" id="Seg_9380" n="HIAT:u" s="T363">
                  <nts id="Seg_9381" n="HIAT:ip">(</nts>
                  <nts id="Seg_9382" n="HIAT:ip">(</nts>
                  <ats e="T364" id="Seg_9383" n="HIAT:non-pho" s="T363">…</ats>
                  <nts id="Seg_9384" n="HIAT:ip">)</nts>
                  <nts id="Seg_9385" n="HIAT:ip">)</nts>
                  <nts id="Seg_9386" n="HIAT:ip">.</nts>
                  <nts id="Seg_9387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T370" id="Seg_9388" n="sc" s="T365">
               <ts e="T370" id="Seg_9390" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_9392" n="HIAT:w" s="T365">Было</ts>
                  <nts id="Seg_9393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_9395" n="HIAT:w" s="T366">ли</ts>
                  <nts id="Seg_9396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_9398" n="HIAT:w" s="T367">вообще</ts>
                  <nts id="Seg_9399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_9401" n="HIAT:w" s="T368">много</ts>
                  <nts id="Seg_9402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_9404" n="HIAT:w" s="T369">детей</ts>
                  <nts id="Seg_9405" n="HIAT:ip">?</nts>
                  <nts id="Seg_9406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T382" id="Seg_9407" n="sc" s="T380">
               <ts e="T382" id="Seg_9409" n="HIAT:u" s="T380">
                  <nts id="Seg_9410" n="HIAT:ip">(</nts>
                  <nts id="Seg_9411" n="HIAT:ip">(</nts>
                  <ats e="T382" id="Seg_9412" n="HIAT:non-pho" s="T380">…</ats>
                  <nts id="Seg_9413" n="HIAT:ip">)</nts>
                  <nts id="Seg_9414" n="HIAT:ip">)</nts>
                  <nts id="Seg_9415" n="HIAT:ip">.</nts>
                  <nts id="Seg_9416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T404" id="Seg_9417" n="sc" s="T401">
               <ts e="T404" id="Seg_9419" n="HIAT:u" s="T401">
                  <nts id="Seg_9420" n="HIAT:ip">(</nts>
                  <nts id="Seg_9421" n="HIAT:ip">(</nts>
                  <ats e="T401.tx-KA.1" id="Seg_9422" n="HIAT:non-pho" s="T401">…</ats>
                  <nts id="Seg_9423" n="HIAT:ip">)</nts>
                  <nts id="Seg_9424" n="HIAT:ip">)</nts>
                  <nts id="Seg_9425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9426" n="HIAT:ip">(</nts>
                  <nts id="Seg_9427" n="HIAT:ip">(</nts>
                  <ats e="T404" id="Seg_9428" n="HIAT:non-pho" s="T401.tx-KA.1">COUGH</ats>
                  <nts id="Seg_9429" n="HIAT:ip">)</nts>
                  <nts id="Seg_9430" n="HIAT:ip">)</nts>
                  <nts id="Seg_9431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T412" id="Seg_9432" n="sc" s="T411">
               <ts e="T412" id="Seg_9434" n="HIAT:u" s="T411">
                  <nts id="Seg_9435" n="HIAT:ip">(</nts>
                  <nts id="Seg_9436" n="HIAT:ip">(</nts>
                  <ats e="T412" id="Seg_9437" n="HIAT:non-pho" s="T411">…</ats>
                  <nts id="Seg_9438" n="HIAT:ip">)</nts>
                  <nts id="Seg_9439" n="HIAT:ip">)</nts>
                  <nts id="Seg_9440" n="HIAT:ip">.</nts>
                  <nts id="Seg_9441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T414" id="Seg_9442" n="sc" s="T413">
               <ts e="T414" id="Seg_9444" n="HIAT:u" s="T413">
                  <nts id="Seg_9445" n="HIAT:ip">(</nts>
                  <nts id="Seg_9446" n="HIAT:ip">(</nts>
                  <ats e="T414" id="Seg_9447" n="HIAT:non-pho" s="T413">…</ats>
                  <nts id="Seg_9448" n="HIAT:ip">)</nts>
                  <nts id="Seg_9449" n="HIAT:ip">)</nts>
                  <nts id="Seg_9450" n="HIAT:ip">.</nts>
                  <nts id="Seg_9451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T423" id="Seg_9452" n="sc" s="T415">
               <ts e="T423" id="Seg_9454" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_9456" n="HIAT:w" s="T415">Как</ts>
                  <nts id="Seg_9457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_9459" n="HIAT:w" s="T416">дети</ts>
                  <nts id="Seg_9460" n="HIAT:ip">,</nts>
                  <nts id="Seg_9461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_9463" n="HIAT:w" s="T417">были</ts>
                  <nts id="Seg_9464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_9466" n="HIAT:w" s="T418">они</ts>
                  <nts id="Seg_9467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_9469" n="HIAT:w" s="T419">вообще</ts>
                  <nts id="Seg_9470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_9472" n="HIAT:w" s="T420">здоровы</ts>
                  <nts id="Seg_9473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_9475" n="HIAT:w" s="T421">или</ts>
                  <nts id="Seg_9476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_9478" n="HIAT:w" s="T422">болели</ts>
                  <nts id="Seg_9479" n="HIAT:ip">?</nts>
                  <nts id="Seg_9480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T436" id="Seg_9481" n="sc" s="T424">
               <ts e="T436" id="Seg_9483" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_9485" n="HIAT:w" s="T424">Как</ts>
                  <nts id="Seg_9486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_9488" n="HIAT:w" s="T425">они</ts>
                  <nts id="Seg_9489" n="HIAT:ip">,</nts>
                  <nts id="Seg_9490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_9492" n="HIAT:w" s="T426">давали</ts>
                  <nts id="Seg_9493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_9495" n="HIAT:w" s="T427">матери</ts>
                  <nts id="Seg_9496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_9498" n="HIAT:w" s="T428">и</ts>
                  <nts id="Seg_9499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_9501" n="HIAT:w" s="T429">отцу</ts>
                  <nts id="Seg_9502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_9504" n="HIAT:w" s="T430">спать</ts>
                  <nts id="Seg_9505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_9507" n="HIAT:w" s="T431">ночью</ts>
                  <nts id="Seg_9508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_9510" n="HIAT:w" s="T432">или</ts>
                  <nts id="Seg_9511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_9513" n="HIAT:w" s="T434">кричали</ts>
                  <nts id="Seg_9514" n="HIAT:ip">,</nts>
                  <nts id="Seg_9515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_9517" n="HIAT:w" s="T435">плакали</ts>
                  <nts id="Seg_9518" n="HIAT:ip">?</nts>
                  <nts id="Seg_9519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_9520" n="sc" s="T439">
               <ts e="T442" id="Seg_9522" n="HIAT:u" s="T439">
                  <nts id="Seg_9523" n="HIAT:ip">(</nts>
                  <nts id="Seg_9524" n="HIAT:ip">(</nts>
                  <ats e="T442" id="Seg_9525" n="HIAT:non-pho" s="T439">…</ats>
                  <nts id="Seg_9526" n="HIAT:ip">)</nts>
                  <nts id="Seg_9527" n="HIAT:ip">)</nts>
                  <nts id="Seg_9528" n="HIAT:ip">.</nts>
                  <nts id="Seg_9529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_9530" n="sc" s="T445">
               <ts e="T446" id="Seg_9532" n="HIAT:u" s="T445">
                  <nts id="Seg_9533" n="HIAT:ip">(</nts>
                  <nts id="Seg_9534" n="HIAT:ip">(</nts>
                  <ats e="T446" id="Seg_9535" n="HIAT:non-pho" s="T445">…</ats>
                  <nts id="Seg_9536" n="HIAT:ip">)</nts>
                  <nts id="Seg_9537" n="HIAT:ip">)</nts>
                  <nts id="Seg_9538" n="HIAT:ip">.</nts>
                  <nts id="Seg_9539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T448" id="Seg_9540" n="sc" s="T447">
               <ts e="T448" id="Seg_9542" n="HIAT:u" s="T447">
                  <nts id="Seg_9543" n="HIAT:ip">(</nts>
                  <nts id="Seg_9544" n="HIAT:ip">(</nts>
                  <ats e="T448" id="Seg_9545" n="HIAT:non-pho" s="T447">…</ats>
                  <nts id="Seg_9546" n="HIAT:ip">)</nts>
                  <nts id="Seg_9547" n="HIAT:ip">)</nts>
                  <nts id="Seg_9548" n="HIAT:ip">.</nts>
                  <nts id="Seg_9549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T457" id="Seg_9550" n="sc" s="T449">
               <ts e="T457" id="Seg_9552" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_9554" n="HIAT:w" s="T449">Что</ts>
                  <nts id="Seg_9555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_9557" n="HIAT:w" s="T450">делали</ts>
                  <nts id="Seg_9558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_9560" n="HIAT:w" s="T451">ребенку</ts>
                  <nts id="Seg_9561" n="HIAT:ip">,</nts>
                  <nts id="Seg_9562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_9564" n="HIAT:w" s="T452">когда</ts>
                  <nts id="Seg_9565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_9567" n="HIAT:w" s="T453">он</ts>
                  <nts id="Seg_9568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_9570" n="HIAT:w" s="T454">много</ts>
                  <nts id="Seg_9571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_9573" n="HIAT:w" s="T455">плакал</ts>
                  <nts id="Seg_9574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_9576" n="HIAT:w" s="T456">ночью</ts>
                  <nts id="Seg_9577" n="HIAT:ip">?</nts>
                  <nts id="Seg_9578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T482" id="Seg_9579" n="sc" s="T481">
               <ts e="T482" id="Seg_9581" n="HIAT:u" s="T481">
                  <nts id="Seg_9582" n="HIAT:ip">(</nts>
                  <nts id="Seg_9583" n="HIAT:ip">(</nts>
                  <ats e="T482" id="Seg_9584" n="HIAT:non-pho" s="T481">…</ats>
                  <nts id="Seg_9585" n="HIAT:ip">)</nts>
                  <nts id="Seg_9586" n="HIAT:ip">)</nts>
                  <nts id="Seg_9587" n="HIAT:ip">.</nts>
                  <nts id="Seg_9588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T484" id="Seg_9589" n="sc" s="T483">
               <ts e="T484" id="Seg_9591" n="HIAT:u" s="T483">
                  <nts id="Seg_9592" n="HIAT:ip">(</nts>
                  <nts id="Seg_9593" n="HIAT:ip">(</nts>
                  <ats e="T484" id="Seg_9594" n="HIAT:non-pho" s="T483">…</ats>
                  <nts id="Seg_9595" n="HIAT:ip">)</nts>
                  <nts id="Seg_9596" n="HIAT:ip">)</nts>
                  <nts id="Seg_9597" n="HIAT:ip">.</nts>
                  <nts id="Seg_9598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_9599" n="sc" s="T485">
               <ts e="T492" id="Seg_9601" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_9603" n="HIAT:w" s="T485">Как</ts>
                  <nts id="Seg_9604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_9606" n="HIAT:w" s="T486">вообще</ts>
                  <nts id="Seg_9607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_9609" n="HIAT:w" s="T487">дети</ts>
                  <nts id="Seg_9610" n="HIAT:ip">,</nts>
                  <nts id="Seg_9611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_9613" n="HIAT:w" s="T488">были</ts>
                  <nts id="Seg_9614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_9616" n="HIAT:w" s="T489">ли</ts>
                  <nts id="Seg_9617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_9619" n="HIAT:w" s="T490">они</ts>
                  <nts id="Seg_9620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_9622" n="HIAT:w" s="T491">послушны</ts>
                  <nts id="Seg_9623" n="HIAT:ip">?</nts>
                  <nts id="Seg_9624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T501" id="Seg_9625" n="sc" s="T493">
               <ts e="T501" id="Seg_9627" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_9629" n="HIAT:w" s="T493">И</ts>
                  <nts id="Seg_9630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_9632" n="HIAT:w" s="T494">как</ts>
                  <nts id="Seg_9633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_9635" n="HIAT:w" s="T495">вообще</ts>
                  <nts id="Seg_9636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_9638" n="HIAT:w" s="T496">это</ts>
                  <nts id="Seg_9639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_9641" n="HIAT:w" s="T497">семьи</ts>
                  <nts id="Seg_9642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_9644" n="HIAT:w" s="T498">с</ts>
                  <nts id="Seg_9645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_9647" n="HIAT:w" s="T499">ними</ts>
                  <nts id="Seg_9648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_9650" n="HIAT:w" s="T500">справлялись</ts>
                  <nts id="Seg_9651" n="HIAT:ip">?</nts>
                  <nts id="Seg_9652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T551" id="Seg_9653" n="sc" s="T545">
               <ts e="T551" id="Seg_9655" n="HIAT:u" s="T545">
                  <nts id="Seg_9656" n="HIAT:ip">(</nts>
                  <nts id="Seg_9657" n="HIAT:ip">(</nts>
                  <ats e="T551" id="Seg_9658" n="HIAT:non-pho" s="T545">…</ats>
                  <nts id="Seg_9659" n="HIAT:ip">)</nts>
                  <nts id="Seg_9660" n="HIAT:ip">)</nts>
                  <nts id="Seg_9661" n="HIAT:ip">.</nts>
                  <nts id="Seg_9662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T554" id="Seg_9663" n="sc" s="T553">
               <ts e="T554" id="Seg_9665" n="HIAT:u" s="T553">
                  <nts id="Seg_9666" n="HIAT:ip">(</nts>
                  <nts id="Seg_9667" n="HIAT:ip">(</nts>
                  <ats e="T554" id="Seg_9668" n="HIAT:non-pho" s="T553">…</ats>
                  <nts id="Seg_9669" n="HIAT:ip">)</nts>
                  <nts id="Seg_9670" n="HIAT:ip">)</nts>
                  <nts id="Seg_9671" n="HIAT:ip">.</nts>
                  <nts id="Seg_9672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T560" id="Seg_9673" n="sc" s="T555">
               <ts e="T560" id="Seg_9675" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_9677" n="HIAT:w" s="T555">Били</ts>
                  <nts id="Seg_9678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_9680" n="HIAT:w" s="T556">ли</ts>
                  <nts id="Seg_9681" n="HIAT:ip">,</nts>
                  <nts id="Seg_9682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_9684" n="HIAT:w" s="T557">значит</ts>
                  <nts id="Seg_9685" n="HIAT:ip">,</nts>
                  <nts id="Seg_9686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_9688" n="HIAT:w" s="T558">родители</ts>
                  <nts id="Seg_9689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_9691" n="HIAT:w" s="T559">детей</ts>
                  <nts id="Seg_9692" n="HIAT:ip">?</nts>
                  <nts id="Seg_9693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T574" id="Seg_9694" n="sc" s="T573">
               <ts e="T574" id="Seg_9696" n="HIAT:u" s="T573">
                  <nts id="Seg_9697" n="HIAT:ip">(</nts>
                  <nts id="Seg_9698" n="HIAT:ip">(</nts>
                  <ats e="T574" id="Seg_9699" n="HIAT:non-pho" s="T573">…</ats>
                  <nts id="Seg_9700" n="HIAT:ip">)</nts>
                  <nts id="Seg_9701" n="HIAT:ip">)</nts>
                  <nts id="Seg_9702" n="HIAT:ip">.</nts>
                  <nts id="Seg_9703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_9704" n="sc" s="T578">
               <ts e="T579" id="Seg_9706" n="HIAT:u" s="T578">
                  <nts id="Seg_9707" n="HIAT:ip">(</nts>
                  <nts id="Seg_9708" n="HIAT:ip">(</nts>
                  <ats e="T579" id="Seg_9709" n="HIAT:non-pho" s="T578">…</ats>
                  <nts id="Seg_9710" n="HIAT:ip">)</nts>
                  <nts id="Seg_9711" n="HIAT:ip">)</nts>
                  <nts id="Seg_9712" n="HIAT:ip">.</nts>
                  <nts id="Seg_9713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_9715" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_9717" n="HIAT:w" s="T579">Чем</ts>
                  <nts id="Seg_9718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_9720" n="HIAT:w" s="T580">это</ts>
                  <nts id="Seg_9721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_9723" n="HIAT:w" s="T581">били</ts>
                  <nts id="Seg_9724" n="HIAT:ip">,</nts>
                  <nts id="Seg_9725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_9727" n="HIAT:w" s="T582">это</ts>
                  <nts id="Seg_9728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_9730" n="HIAT:w" s="T583">я</ts>
                  <nts id="Seg_9731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_9733" n="HIAT:w" s="T584">не</ts>
                  <nts id="Seg_9734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_9736" n="HIAT:w" s="T585">понял</ts>
                  <nts id="Seg_9737" n="HIAT:ip">.</nts>
                  <nts id="Seg_9738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T588" id="Seg_9739" n="sc" s="T587">
               <ts e="T588" id="Seg_9741" n="HIAT:u" s="T587">
                  <nts id="Seg_9742" n="HIAT:ip">(</nts>
                  <nts id="Seg_9743" n="HIAT:ip">(</nts>
                  <ats e="T588" id="Seg_9744" n="HIAT:non-pho" s="T587">…</ats>
                  <nts id="Seg_9745" n="HIAT:ip">)</nts>
                  <nts id="Seg_9746" n="HIAT:ip">)</nts>
                  <nts id="Seg_9747" n="HIAT:ip">.</nts>
                  <nts id="Seg_9748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T594" id="Seg_9749" n="sc" s="T590">
               <ts e="T594" id="Seg_9751" n="HIAT:u" s="T590">
                  <nts id="Seg_9752" n="HIAT:ip">(</nts>
                  <nts id="Seg_9753" n="HIAT:ip">(</nts>
                  <ats e="T594" id="Seg_9754" n="HIAT:non-pho" s="T590">…</ats>
                  <nts id="Seg_9755" n="HIAT:ip">)</nts>
                  <nts id="Seg_9756" n="HIAT:ip">)</nts>
                  <nts id="Seg_9757" n="HIAT:ip">.</nts>
                  <nts id="Seg_9758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T596" id="Seg_9759" n="sc" s="T595">
               <ts e="T596" id="Seg_9761" n="HIAT:u" s="T595">
                  <nts id="Seg_9762" n="HIAT:ip">(</nts>
                  <nts id="Seg_9763" n="HIAT:ip">(</nts>
                  <ats e="T596" id="Seg_9764" n="HIAT:non-pho" s="T595">…</ats>
                  <nts id="Seg_9765" n="HIAT:ip">)</nts>
                  <nts id="Seg_9766" n="HIAT:ip">)</nts>
                  <nts id="Seg_9767" n="HIAT:ip">.</nts>
                  <nts id="Seg_9768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T601" id="Seg_9769" n="sc" s="T597">
               <ts e="T601" id="Seg_9771" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_9773" n="HIAT:w" s="T597">Умели</ts>
                  <nts id="Seg_9774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_9776" n="HIAT:w" s="T598">ли</ts>
                  <nts id="Seg_9777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_9779" n="HIAT:w" s="T599">камасинцы</ts>
                  <nts id="Seg_9780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_9782" n="HIAT:w" s="T600">читать</ts>
                  <nts id="Seg_9783" n="HIAT:ip">?</nts>
                  <nts id="Seg_9784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T607" id="Seg_9785" n="sc" s="T606">
               <ts e="T607" id="Seg_9787" n="HIAT:u" s="T606">
                  <nts id="Seg_9788" n="HIAT:ip">(</nts>
                  <nts id="Seg_9789" n="HIAT:ip">(</nts>
                  <ats e="T607" id="Seg_9790" n="HIAT:non-pho" s="T606">…</ats>
                  <nts id="Seg_9791" n="HIAT:ip">)</nts>
                  <nts id="Seg_9792" n="HIAT:ip">)</nts>
                  <nts id="Seg_9793" n="HIAT:ip">.</nts>
                  <nts id="Seg_9794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T620" id="Seg_9795" n="sc" s="T617">
               <ts e="T620" id="Seg_9797" n="HIAT:u" s="T617">
                  <nts id="Seg_9798" n="HIAT:ip">(</nts>
                  <nts id="Seg_9799" n="HIAT:ip">(</nts>
                  <ats e="T620" id="Seg_9800" n="HIAT:non-pho" s="T617">…</ats>
                  <nts id="Seg_9801" n="HIAT:ip">)</nts>
                  <nts id="Seg_9802" n="HIAT:ip">)</nts>
                  <nts id="Seg_9803" n="HIAT:ip">.</nts>
                  <nts id="Seg_9804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T648" id="Seg_9805" n="sc" s="T644">
               <ts e="T648" id="Seg_9807" n="HIAT:u" s="T644">
                  <nts id="Seg_9808" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_9810" n="HIAT:w" s="T644">Дальше</ts>
                  <nts id="Seg_9811" n="HIAT:ip">)</nts>
                  <nts id="Seg_9812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_9814" n="HIAT:w" s="T645">здесь</ts>
                  <nts id="Seg_9815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_9817" n="HIAT:w" s="T646">говорили</ts>
                  <nts id="Seg_9818" n="HIAT:ip">?</nts>
                  <nts id="Seg_9819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T663" id="Seg_9820" n="sc" s="T650">
               <ts e="T663" id="Seg_9822" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_9824" n="HIAT:w" s="T650">Ага</ts>
                  <nts id="Seg_9825" n="HIAT:ip">,</nts>
                  <nts id="Seg_9826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_9828" n="HIAT:w" s="T651">что</ts>
                  <nts id="Seg_9829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_9831" n="HIAT:w" s="T653">такого</ts>
                  <nts id="Seg_9832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_9834" n="HIAT:w" s="T654">языка</ts>
                  <nts id="Seg_9835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_9837" n="HIAT:w" s="T656">нет</ts>
                  <nts id="Seg_9838" n="HIAT:ip">,</nts>
                  <nts id="Seg_9839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_9841" n="HIAT:w" s="T657">поскольку</ts>
                  <nts id="Seg_9842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_9844" n="HIAT:w" s="T658">нет</ts>
                  <nts id="Seg_9845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_9847" n="HIAT:w" s="T659">книг</ts>
                  <nts id="Seg_9848" n="HIAT:ip">,</nts>
                  <nts id="Seg_9849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_9851" n="HIAT:w" s="T660">значит</ts>
                  <nts id="Seg_9852" n="HIAT:ip">,</nts>
                  <nts id="Seg_9853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_9855" n="HIAT:w" s="T662">да</ts>
                  <nts id="Seg_9856" n="HIAT:ip">?</nts>
                  <nts id="Seg_9857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_9858" n="sc" s="T670">
               <ts e="T675" id="Seg_9860" n="HIAT:u" s="T670">
                  <ts e="T673" id="Seg_9862" n="HIAT:w" s="T670">Ага</ts>
                  <nts id="Seg_9863" n="HIAT:ip">,</nts>
                  <nts id="Seg_9864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_9866" n="HIAT:w" s="T673">ага</ts>
                  <nts id="Seg_9867" n="HIAT:ip">.</nts>
                  <nts id="Seg_9868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T705" id="Seg_9869" n="sc" s="T704">
               <ts e="T705" id="Seg_9871" n="HIAT:u" s="T704">
                  <nts id="Seg_9872" n="HIAT:ip">(</nts>
                  <nts id="Seg_9873" n="HIAT:ip">(</nts>
                  <ats e="T705" id="Seg_9874" n="HIAT:non-pho" s="T704">…</ats>
                  <nts id="Seg_9875" n="HIAT:ip">)</nts>
                  <nts id="Seg_9876" n="HIAT:ip">)</nts>
                  <nts id="Seg_9877" n="HIAT:ip">.</nts>
                  <nts id="Seg_9878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T707" id="Seg_9879" n="sc" s="T706">
               <ts e="T707" id="Seg_9881" n="HIAT:u" s="T706">
                  <nts id="Seg_9882" n="HIAT:ip">(</nts>
                  <nts id="Seg_9883" n="HIAT:ip">(</nts>
                  <ats e="T707" id="Seg_9884" n="HIAT:non-pho" s="T706">…</ats>
                  <nts id="Seg_9885" n="HIAT:ip">)</nts>
                  <nts id="Seg_9886" n="HIAT:ip">)</nts>
                  <nts id="Seg_9887" n="HIAT:ip">.</nts>
                  <nts id="Seg_9888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T715" id="Seg_9889" n="sc" s="T708">
               <ts e="T715" id="Seg_9891" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_9893" n="HIAT:w" s="T708">Ты</ts>
                  <nts id="Seg_9894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_9896" n="HIAT:w" s="T709">сама</ts>
                  <nts id="Seg_9897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_9899" n="HIAT:w" s="T710">умеешь</ts>
                  <nts id="Seg_9900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_9902" n="HIAT:w" s="T711">читать</ts>
                  <nts id="Seg_9903" n="HIAT:ip">,</nts>
                  <nts id="Seg_9904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_9906" n="HIAT:w" s="T712">умеешь</ts>
                  <nts id="Seg_9907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_9909" n="HIAT:w" s="T713">писать</ts>
                  <nts id="Seg_9910" n="HIAT:ip">,</nts>
                  <nts id="Seg_9911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_9913" n="HIAT:w" s="T714">наверное</ts>
                  <nts id="Seg_9914" n="HIAT:ip">?</nts>
                  <nts id="Seg_9915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T718" id="Seg_9916" n="sc" s="T716">
               <ts e="T718" id="Seg_9918" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_9920" n="HIAT:w" s="T716">Нет</ts>
                  <nts id="Seg_9921" n="HIAT:ip">,</nts>
                  <nts id="Seg_9922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_9924" n="HIAT:w" s="T717">да</ts>
                  <nts id="Seg_9925" n="HIAT:ip">?</nts>
                  <nts id="Seg_9926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T720" id="Seg_9927" n="sc" s="T719">
               <ts e="T720" id="Seg_9929" n="HIAT:u" s="T719">
                  <nts id="Seg_9930" n="HIAT:ip">(</nts>
                  <nts id="Seg_9931" n="HIAT:ip">(</nts>
                  <ats e="T720" id="Seg_9932" n="HIAT:non-pho" s="T719">…</ats>
                  <nts id="Seg_9933" n="HIAT:ip">)</nts>
                  <nts id="Seg_9934" n="HIAT:ip">)</nts>
                  <nts id="Seg_9935" n="HIAT:ip">.</nts>
                  <nts id="Seg_9936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_9937" n="sc" s="T721">
               <ts e="T723" id="Seg_9939" n="HIAT:u" s="T721">
                  <nts id="Seg_9940" n="HIAT:ip">(</nts>
                  <nts id="Seg_9941" n="HIAT:ip">(</nts>
                  <ats e="T723" id="Seg_9942" n="HIAT:non-pho" s="T721">…</ats>
                  <nts id="Seg_9943" n="HIAT:ip">)</nts>
                  <nts id="Seg_9944" n="HIAT:ip">)</nts>
                  <nts id="Seg_9945" n="HIAT:ip">.</nts>
                  <nts id="Seg_9946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T743" id="Seg_9947" n="sc" s="T742">
               <ts e="T743" id="Seg_9949" n="HIAT:u" s="T742">
                  <nts id="Seg_9950" n="HIAT:ip">(</nts>
                  <nts id="Seg_9951" n="HIAT:ip">(</nts>
                  <ats e="T743" id="Seg_9952" n="HIAT:non-pho" s="T742">…</ats>
                  <nts id="Seg_9953" n="HIAT:ip">)</nts>
                  <nts id="Seg_9954" n="HIAT:ip">)</nts>
                  <nts id="Seg_9955" n="HIAT:ip">.</nts>
                  <nts id="Seg_9956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T757" id="Seg_9957" n="sc" s="T744">
               <ts e="T745" id="Seg_9959" n="HIAT:u" s="T744">
                  <nts id="Seg_9960" n="HIAT:ip">(</nts>
                  <nts id="Seg_9961" n="HIAT:ip">(</nts>
                  <ats e="T745" id="Seg_9962" n="HIAT:non-pho" s="T744">…</ats>
                  <nts id="Seg_9963" n="HIAT:ip">)</nts>
                  <nts id="Seg_9964" n="HIAT:ip">)</nts>
                  <nts id="Seg_9965" n="HIAT:ip">.</nts>
                  <nts id="Seg_9966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_9968" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_9970" n="HIAT:w" s="T745">Жили</ts>
                  <nts id="Seg_9971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_9973" n="HIAT:w" s="T746">ли</ts>
                  <nts id="Seg_9974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_9976" n="HIAT:w" s="T747">люди</ts>
                  <nts id="Seg_9977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_9979" n="HIAT:w" s="T748">много</ts>
                  <nts id="Seg_9980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_9982" n="HIAT:w" s="T749">лет</ts>
                  <nts id="Seg_9983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_9985" n="HIAT:w" s="T750">в</ts>
                  <nts id="Seg_9986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_9988" n="HIAT:w" s="T751">былые</ts>
                  <nts id="Seg_9989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_9991" n="HIAT:w" s="T752">времена</ts>
                  <nts id="Seg_9992" n="HIAT:ip">,</nts>
                  <nts id="Seg_9993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_9995" n="HIAT:w" s="T753">камасинцы</ts>
                  <nts id="Seg_9996" n="HIAT:ip">?</nts>
                  <nts id="Seg_9997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_9999" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_10001" n="HIAT:w" s="T754">Стали</ts>
                  <nts id="Seg_10002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_10004" n="HIAT:w" s="T755">ли</ts>
                  <nts id="Seg_10005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_10007" n="HIAT:w" s="T756">старым</ts>
                  <nts id="Seg_10008" n="HIAT:ip">?</nts>
                  <nts id="Seg_10009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T769" id="Seg_10010" n="sc" s="T767">
               <ts e="T769" id="Seg_10012" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_10014" n="HIAT:w" s="T767">По-своему</ts>
                  <nts id="Seg_10015" n="HIAT:ip">,</nts>
                  <nts id="Seg_10016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_10018" n="HIAT:w" s="T768">да</ts>
                  <nts id="Seg_10019" n="HIAT:ip">.</nts>
                  <nts id="Seg_10020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T799" id="Seg_10021" n="sc" s="T798">
               <ts e="T799" id="Seg_10023" n="HIAT:u" s="T798">
                  <nts id="Seg_10024" n="HIAT:ip">(</nts>
                  <nts id="Seg_10025" n="HIAT:ip">(</nts>
                  <ats e="T799" id="Seg_10026" n="HIAT:non-pho" s="T798">…</ats>
                  <nts id="Seg_10027" n="HIAT:ip">)</nts>
                  <nts id="Seg_10028" n="HIAT:ip">)</nts>
                  <nts id="Seg_10029" n="HIAT:ip">.</nts>
                  <nts id="Seg_10030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T885" id="Seg_10031" n="sc" s="T869">
               <ts e="T885" id="Seg_10033" n="HIAT:u" s="T869">
                  <nts id="Seg_10034" n="HIAT:ip">(</nts>
                  <nts id="Seg_10035" n="HIAT:ip">(</nts>
                  <ats e="T885" id="Seg_10036" n="HIAT:non-pho" s="T869">…</ats>
                  <nts id="Seg_10037" n="HIAT:ip">)</nts>
                  <nts id="Seg_10038" n="HIAT:ip">)</nts>
                  <nts id="Seg_10039" n="HIAT:ip">.</nts>
                  <nts id="Seg_10040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T891" id="Seg_10041" n="sc" s="T886">
               <ts e="T887" id="Seg_10043" n="HIAT:u" s="T886">
                  <nts id="Seg_10044" n="HIAT:ip">(</nts>
                  <nts id="Seg_10045" n="HIAT:ip">(</nts>
                  <ats e="T887" id="Seg_10046" n="HIAT:non-pho" s="T886">…</ats>
                  <nts id="Seg_10047" n="HIAT:ip">)</nts>
                  <nts id="Seg_10048" n="HIAT:ip">)</nts>
                  <nts id="Seg_10049" n="HIAT:ip">.</nts>
                  <nts id="Seg_10050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_10052" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_10054" n="HIAT:w" s="T887">Как</ts>
                  <nts id="Seg_10055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10056" n="HIAT:ip">(</nts>
                  <ts e="T889" id="Seg_10058" n="HIAT:w" s="T888">это=</ts>
                  <nts id="Seg_10059" n="HIAT:ip">)</nts>
                  <nts id="Seg_10060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_10062" n="HIAT:w" s="T889">похороны</ts>
                  <nts id="Seg_10063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_10065" n="HIAT:w" s="T890">были</ts>
                  <nts id="Seg_10066" n="HIAT:ip">?</nts>
                  <nts id="Seg_10067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T941" id="Seg_10068" n="sc" s="T940">
               <ts e="T941" id="Seg_10070" n="HIAT:u" s="T940">
                  <nts id="Seg_10071" n="HIAT:ip">(</nts>
                  <nts id="Seg_10072" n="HIAT:ip">(</nts>
                  <ats e="T941" id="Seg_10073" n="HIAT:non-pho" s="T940">…</ats>
                  <nts id="Seg_10074" n="HIAT:ip">)</nts>
                  <nts id="Seg_10075" n="HIAT:ip">)</nts>
                  <nts id="Seg_10076" n="HIAT:ip">.</nts>
                  <nts id="Seg_10077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T955" id="Seg_10078" n="sc" s="T943">
               <ts e="T949" id="Seg_10080" n="HIAT:u" s="T943">
                  <ts e="T945" id="Seg_10082" n="HIAT:w" s="T943">Наверное</ts>
                  <nts id="Seg_10083" n="HIAT:ip">,</nts>
                  <nts id="Seg_10084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_10086" n="HIAT:w" s="T945">и</ts>
                  <nts id="Seg_10087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_10089" n="HIAT:w" s="T947">пили</ts>
                  <nts id="Seg_10090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_10092" n="HIAT:w" s="T948">еще</ts>
                  <nts id="Seg_10093" n="HIAT:ip">?</nts>
                  <nts id="Seg_10094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_10096" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_10098" n="HIAT:w" s="T949">Не</ts>
                  <nts id="Seg_10099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_10101" n="HIAT:w" s="T950">только</ts>
                  <nts id="Seg_10102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_10104" n="HIAT:w" s="T951">ели</ts>
                  <nts id="Seg_10105" n="HIAT:ip">,</nts>
                  <nts id="Seg_10106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_10108" n="HIAT:w" s="T952">да</ts>
                  <nts id="Seg_10109" n="HIAT:ip">?</nts>
                  <nts id="Seg_10110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T964" id="Seg_10111" n="sc" s="T958">
               <ts e="T964" id="Seg_10113" n="HIAT:u" s="T958">
                  <ts e="T960" id="Seg_10115" n="HIAT:w" s="T958">И</ts>
                  <nts id="Seg_10116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_10118" n="HIAT:w" s="T960">делали</ts>
                  <nts id="Seg_10119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_10121" n="HIAT:w" s="T962">крест</ts>
                  <nts id="Seg_10122" n="HIAT:ip">.</nts>
                  <nts id="Seg_10123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T985" id="Seg_10124" n="sc" s="T981">
               <ts e="T985" id="Seg_10126" n="HIAT:u" s="T981">
                  <nts id="Seg_10127" n="HIAT:ip">(</nts>
                  <nts id="Seg_10128" n="HIAT:ip">(</nts>
                  <ats e="T985" id="Seg_10129" n="HIAT:non-pho" s="T981">…</ats>
                  <nts id="Seg_10130" n="HIAT:ip">)</nts>
                  <nts id="Seg_10131" n="HIAT:ip">)</nts>
                  <nts id="Seg_10132" n="HIAT:ip">.</nts>
                  <nts id="Seg_10133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T987" id="Seg_10134" n="sc" s="T986">
               <ts e="T987" id="Seg_10136" n="HIAT:u" s="T986">
                  <nts id="Seg_10137" n="HIAT:ip">(</nts>
                  <nts id="Seg_10138" n="HIAT:ip">(</nts>
                  <ats e="T987" id="Seg_10139" n="HIAT:non-pho" s="T986">…</ats>
                  <nts id="Seg_10140" n="HIAT:ip">)</nts>
                  <nts id="Seg_10141" n="HIAT:ip">)</nts>
                  <nts id="Seg_10142" n="HIAT:ip">.</nts>
                  <nts id="Seg_10143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T998" id="Seg_10144" n="sc" s="T988">
               <ts e="T998" id="Seg_10146" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_10148" n="HIAT:w" s="T988">Во</ts>
                  <nts id="Seg_10149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_10151" n="HIAT:w" s="T989">времена</ts>
                  <nts id="Seg_10152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_10154" n="HIAT:w" s="T990">шаманов</ts>
                  <nts id="Seg_10155" n="HIAT:ip">,</nts>
                  <nts id="Seg_10156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_10158" n="HIAT:w" s="T991">наверное</ts>
                  <nts id="Seg_10159" n="HIAT:ip">,</nts>
                  <nts id="Seg_10160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10161" n="HIAT:ip">(</nts>
                  <ts e="T993" id="Seg_10163" n="HIAT:w" s="T992">это=</ts>
                  <nts id="Seg_10164" n="HIAT:ip">)</nts>
                  <nts id="Seg_10165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_10167" n="HIAT:w" s="T993">крестов</ts>
                  <nts id="Seg_10168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_10170" n="HIAT:w" s="T994">на</ts>
                  <nts id="Seg_10171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_10173" n="HIAT:w" s="T995">могилах</ts>
                  <nts id="Seg_10174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_10176" n="HIAT:w" s="T996">не</ts>
                  <nts id="Seg_10177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_10179" n="HIAT:w" s="T997">ставили</ts>
                  <nts id="Seg_10180" n="HIAT:ip">?</nts>
                  <nts id="Seg_10181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1021" id="Seg_10182" n="sc" s="T1012">
               <ts e="T1021" id="Seg_10184" n="HIAT:u" s="T1012">
                  <nts id="Seg_10185" n="HIAT:ip">(</nts>
                  <nts id="Seg_10186" n="HIAT:ip">(</nts>
                  <ats e="T1021" id="Seg_10187" n="HIAT:non-pho" s="T1012">…</ats>
                  <nts id="Seg_10188" n="HIAT:ip">)</nts>
                  <nts id="Seg_10189" n="HIAT:ip">)</nts>
                  <nts id="Seg_10190" n="HIAT:ip">.</nts>
                  <nts id="Seg_10191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1049" id="Seg_10192" n="sc" s="T1041">
               <ts e="T1044" id="Seg_10194" n="HIAT:u" s="T1041">
                  <ts e="T1042" id="Seg_10196" n="HIAT:w" s="T1041">Ага</ts>
                  <nts id="Seg_10197" n="HIAT:ip">,</nts>
                  <nts id="Seg_10198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_10200" n="HIAT:w" s="T1042">ага</ts>
                  <nts id="Seg_10201" n="HIAT:ip">.</nts>
                  <nts id="Seg_10202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1049" id="Seg_10204" n="HIAT:u" s="T1044">
                  <ts e="T1046" id="Seg_10206" n="HIAT:w" s="T1044">Это</ts>
                  <nts id="Seg_10207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_10209" n="HIAT:w" s="T1046">старое</ts>
                  <nts id="Seg_10210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_10212" n="HIAT:w" s="T1047">кладбище</ts>
                  <nts id="Seg_10213" n="HIAT:ip">.</nts>
                  <nts id="Seg_10214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1082" id="Seg_10215" n="sc" s="T1075">
               <ts e="T1082" id="Seg_10217" n="HIAT:u" s="T1075">
                  <ts e="T1077" id="Seg_10219" n="HIAT:w" s="T1075">Там</ts>
                  <nts id="Seg_10220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_10222" n="HIAT:w" s="T1077">всё</ts>
                  <nts id="Seg_10223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_10225" n="HIAT:w" s="T1078">были</ts>
                  <nts id="Seg_10226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_10228" n="HIAT:w" s="T1080">кресты</ts>
                  <nts id="Seg_10229" n="HIAT:ip">.</nts>
                  <nts id="Seg_10230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1136" id="Seg_10231" n="sc" s="T1127">
               <ts e="T1136" id="Seg_10233" n="HIAT:u" s="T1127">
                  <nts id="Seg_10234" n="HIAT:ip">(</nts>
                  <nts id="Seg_10235" n="HIAT:ip">(</nts>
                  <ats e="T1136" id="Seg_10236" n="HIAT:non-pho" s="T1127">…</ats>
                  <nts id="Seg_10237" n="HIAT:ip">)</nts>
                  <nts id="Seg_10238" n="HIAT:ip">)</nts>
                  <nts id="Seg_10239" n="HIAT:ip">.</nts>
                  <nts id="Seg_10240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1159" id="Seg_10241" n="sc" s="T1154">
               <ts e="T1159" id="Seg_10243" n="HIAT:u" s="T1154">
                  <nts id="Seg_10244" n="HIAT:ip">(</nts>
                  <nts id="Seg_10245" n="HIAT:ip">(</nts>
                  <ats e="T1159" id="Seg_10246" n="HIAT:non-pho" s="T1154">…</ats>
                  <nts id="Seg_10247" n="HIAT:ip">)</nts>
                  <nts id="Seg_10248" n="HIAT:ip">)</nts>
                  <nts id="Seg_10249" n="HIAT:ip">.</nts>
                  <nts id="Seg_10250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1169" id="Seg_10251" n="sc" s="T1166">
               <ts e="T1169" id="Seg_10253" n="HIAT:u" s="T1166">
                  <nts id="Seg_10254" n="HIAT:ip">(</nts>
                  <nts id="Seg_10255" n="HIAT:ip">(</nts>
                  <ats e="T1169" id="Seg_10256" n="HIAT:non-pho" s="T1166">…</ats>
                  <nts id="Seg_10257" n="HIAT:ip">)</nts>
                  <nts id="Seg_10258" n="HIAT:ip">)</nts>
                  <nts id="Seg_10259" n="HIAT:ip">.</nts>
                  <nts id="Seg_10260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1171" id="Seg_10261" n="sc" s="T1170">
               <ts e="T1171" id="Seg_10263" n="HIAT:u" s="T1170">
                  <nts id="Seg_10264" n="HIAT:ip">(</nts>
                  <nts id="Seg_10265" n="HIAT:ip">(</nts>
                  <ats e="T1171" id="Seg_10266" n="HIAT:non-pho" s="T1170">…</ats>
                  <nts id="Seg_10267" n="HIAT:ip">)</nts>
                  <nts id="Seg_10268" n="HIAT:ip">)</nts>
                  <nts id="Seg_10269" n="HIAT:ip">.</nts>
                  <nts id="Seg_10270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1182" id="Seg_10271" n="sc" s="T1172">
               <ts e="T1182" id="Seg_10273" n="HIAT:u" s="T1172">
                  <ts e="T1173" id="Seg_10275" n="HIAT:w" s="T1172">Так</ts>
                  <nts id="Seg_10276" n="HIAT:ip">,</nts>
                  <nts id="Seg_10277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1174" id="Seg_10279" n="HIAT:w" s="T1173">когда</ts>
                  <nts id="Seg_10280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_10282" n="HIAT:w" s="T1174">человека</ts>
                  <nts id="Seg_10283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_10285" n="HIAT:w" s="T1175">похоронили</ts>
                  <nts id="Seg_10286" n="HIAT:ip">,</nts>
                  <nts id="Seg_10287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1177" id="Seg_10289" n="HIAT:w" s="T1176">что</ts>
                  <nts id="Seg_10290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_10292" n="HIAT:w" s="T1177">стало</ts>
                  <nts id="Seg_10293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1179" id="Seg_10295" n="HIAT:w" s="T1178">затем</ts>
                  <nts id="Seg_10296" n="HIAT:ip">,</nts>
                  <nts id="Seg_10297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_10299" n="HIAT:w" s="T1179">или</ts>
                  <nts id="Seg_10300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_10302" n="HIAT:w" s="T1180">было</ts>
                  <nts id="Seg_10303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_10305" n="HIAT:w" s="T1181">всё</ts>
                  <nts id="Seg_10306" n="HIAT:ip">?</nts>
                  <nts id="Seg_10307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1186" id="Seg_10308" n="sc" s="T1183">
               <ts e="T1186" id="Seg_10310" n="HIAT:u" s="T1183">
                  <ts e="T1184" id="Seg_10312" n="HIAT:w" s="T1183">Что</ts>
                  <nts id="Seg_10313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_10315" n="HIAT:w" s="T1184">еще</ts>
                  <nts id="Seg_10316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_10318" n="HIAT:w" s="T1185">делали</ts>
                  <nts id="Seg_10319" n="HIAT:ip">?</nts>
                  <nts id="Seg_10320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1232" id="Seg_10321" n="sc" s="T1231">
               <ts e="T1232" id="Seg_10323" n="HIAT:u" s="T1231">
                  <nts id="Seg_10324" n="HIAT:ip">(</nts>
                  <nts id="Seg_10325" n="HIAT:ip">(</nts>
                  <ats e="T1232" id="Seg_10326" n="HIAT:non-pho" s="T1231">…</ats>
                  <nts id="Seg_10327" n="HIAT:ip">)</nts>
                  <nts id="Seg_10328" n="HIAT:ip">)</nts>
                  <nts id="Seg_10329" n="HIAT:ip">.</nts>
                  <nts id="Seg_10330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1254" id="Seg_10331" n="sc" s="T1233">
               <ts e="T1254" id="Seg_10333" n="HIAT:u" s="T1233">
                  <ts e="T1234" id="Seg_10335" n="HIAT:w" s="T1233">Как</ts>
                  <nts id="Seg_10336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1235" id="Seg_10338" n="HIAT:w" s="T1234">это</ts>
                  <nts id="Seg_10339" n="HIAT:ip">,</nts>
                  <nts id="Seg_10340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1236" id="Seg_10342" n="HIAT:w" s="T1235">насчет</ts>
                  <nts id="Seg_10343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1237" id="Seg_10345" n="HIAT:w" s="T1236">вот</ts>
                  <nts id="Seg_10346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1238" id="Seg_10348" n="HIAT:w" s="T1237">такого</ts>
                  <nts id="Seg_10349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1239" id="Seg_10351" n="HIAT:w" s="T1238">вопроса:</ts>
                  <nts id="Seg_10352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_10354" n="HIAT:w" s="T1239">был</ts>
                  <nts id="Seg_10355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_10357" n="HIAT:w" s="T1240">иногда</ts>
                  <nts id="Seg_10358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_10360" n="HIAT:w" s="T1241">разговор</ts>
                  <nts id="Seg_10361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1243" id="Seg_10363" n="HIAT:w" s="T1242">с</ts>
                  <nts id="Seg_10364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_10366" n="HIAT:w" s="T1243">матерью</ts>
                  <nts id="Seg_10367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_10369" n="HIAT:w" s="T1244">у</ts>
                  <nts id="Seg_10370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_10372" n="HIAT:w" s="T1245">тебя</ts>
                  <nts id="Seg_10373" n="HIAT:ip">,</nts>
                  <nts id="Seg_10374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_10376" n="HIAT:w" s="T1246">что</ts>
                  <nts id="Seg_10377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1248" id="Seg_10379" n="HIAT:w" s="T1247">получится</ts>
                  <nts id="Seg_10380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1249" id="Seg_10382" n="HIAT:w" s="T1248">после</ts>
                  <nts id="Seg_10383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1250" id="Seg_10385" n="HIAT:w" s="T1249">смерти</ts>
                  <nts id="Seg_10386" n="HIAT:ip">,</nts>
                  <nts id="Seg_10387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1251" id="Seg_10389" n="HIAT:w" s="T1250">что</ts>
                  <nts id="Seg_10390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1252" id="Seg_10392" n="HIAT:w" s="T1251">станет</ts>
                  <nts id="Seg_10393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1253" id="Seg_10395" n="HIAT:w" s="T1252">с</ts>
                  <nts id="Seg_10396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1254" id="Seg_10398" n="HIAT:w" s="T1253">человеком</ts>
                  <nts id="Seg_10399" n="HIAT:ip">?</nts>
                  <nts id="Seg_10400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1269" id="Seg_10401" n="sc" s="T1255">
               <ts e="T1269" id="Seg_10403" n="HIAT:u" s="T1255">
                  <ts e="T1256" id="Seg_10405" n="HIAT:w" s="T1255">Будет</ts>
                  <nts id="Seg_10406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1257" id="Seg_10408" n="HIAT:w" s="T1256">ли</ts>
                  <nts id="Seg_10409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1258" id="Seg_10411" n="HIAT:w" s="T1257">он</ts>
                  <nts id="Seg_10412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_10414" n="HIAT:w" s="T1258">жить</ts>
                  <nts id="Seg_10415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_10417" n="HIAT:w" s="T1259">еще</ts>
                  <nts id="Seg_10418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_10420" n="HIAT:w" s="T1260">где-нибудь</ts>
                  <nts id="Seg_10421" n="HIAT:ip">,</nts>
                  <nts id="Seg_10422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1262" id="Seg_10424" n="HIAT:w" s="T1261">продолжать</ts>
                  <nts id="Seg_10425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1263" id="Seg_10427" n="HIAT:w" s="T1262">жить</ts>
                  <nts id="Seg_10428" n="HIAT:ip">,</nts>
                  <nts id="Seg_10429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_10431" n="HIAT:w" s="T1263">или</ts>
                  <nts id="Seg_10432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_10434" n="HIAT:w" s="T1264">все</ts>
                  <nts id="Seg_10435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1266" id="Seg_10437" n="HIAT:w" s="T1265">будет</ts>
                  <nts id="Seg_10438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1267" id="Seg_10440" n="HIAT:w" s="T1266">кончено</ts>
                  <nts id="Seg_10441" n="HIAT:ip">,</nts>
                  <nts id="Seg_10442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1268" id="Seg_10444" n="HIAT:w" s="T1267">если</ts>
                  <nts id="Seg_10445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_10447" n="HIAT:w" s="T1268">похоронили</ts>
                  <nts id="Seg_10448" n="HIAT:ip">?</nts>
                  <nts id="Seg_10449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1311" id="Seg_10450" n="sc" s="T1310">
               <ts e="T1311" id="Seg_10452" n="HIAT:u" s="T1310">
                  <nts id="Seg_10453" n="HIAT:ip">(</nts>
                  <nts id="Seg_10454" n="HIAT:ip">(</nts>
                  <ats e="T1311" id="Seg_10455" n="HIAT:non-pho" s="T1310">…</ats>
                  <nts id="Seg_10456" n="HIAT:ip">)</nts>
                  <nts id="Seg_10457" n="HIAT:ip">)</nts>
                  <nts id="Seg_10458" n="HIAT:ip">.</nts>
                  <nts id="Seg_10459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1345" id="Seg_10460" n="sc" s="T1333">
               <ts e="T1345" id="Seg_10462" n="HIAT:u" s="T1333">
                  <nts id="Seg_10463" n="HIAT:ip">(</nts>
                  <nts id="Seg_10464" n="HIAT:ip">(</nts>
                  <ats e="T1333.tx-KA.1" id="Seg_10465" n="HIAT:non-pho" s="T1333">LAUGH</ats>
                  <nts id="Seg_10466" n="HIAT:ip">)</nts>
                  <nts id="Seg_10467" n="HIAT:ip">)</nts>
                  <nts id="Seg_10468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10469" n="HIAT:ip">(</nts>
                  <ts e="T1333.tx-KA.2" id="Seg_10471" n="HIAT:w" s="T1333.tx-KA.1">Уже=</ts>
                  <nts id="Seg_10472" n="HIAT:ip">)</nts>
                  <nts id="Seg_10473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333.tx-KA.3" id="Seg_10475" n="HIAT:w" s="T1333.tx-KA.2">Уже</ts>
                  <nts id="Seg_10476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333.tx-KA.4" id="Seg_10478" n="HIAT:w" s="T1333.tx-KA.3">начинаю</ts>
                  <nts id="Seg_10479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333.tx-KA.5" id="Seg_10481" n="HIAT:w" s="T1333.tx-KA.4">не</ts>
                  <nts id="Seg_10482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333.tx-KA.6" id="Seg_10484" n="HIAT:w" s="T1333.tx-KA.5">понимать</ts>
                  <nts id="Seg_10485" n="HIAT:ip">,</nts>
                  <nts id="Seg_10486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1345" id="Seg_10488" n="HIAT:w" s="T1333.tx-KA.6">устал</ts>
                  <nts id="Seg_10489" n="HIAT:ip">.</nts>
                  <nts id="Seg_10490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1367" id="Seg_10491" n="sc" s="T1346">
               <ts e="T1361" id="Seg_10493" n="HIAT:u" s="T1346">
                  <ts e="T1347" id="Seg_10495" n="HIAT:w" s="T1346">Это</ts>
                  <nts id="Seg_10496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_10498" n="HIAT:w" s="T1347">все</ts>
                  <nts id="Seg_10499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_10501" n="HIAT:w" s="T1349">же</ts>
                  <nts id="Seg_10502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1353" id="Seg_10504" n="HIAT:w" s="T1351">я</ts>
                  <nts id="Seg_10505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_10507" n="HIAT:w" s="T1353">не</ts>
                  <nts id="Seg_10508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1357" id="Seg_10510" n="HIAT:w" s="T1355">привык</ts>
                  <nts id="Seg_10511" n="HIAT:ip">,</nts>
                  <nts id="Seg_10512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_10514" n="HIAT:w" s="T1357">уже</ts>
                  <nts id="Seg_10515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_10517" n="HIAT:w" s="T1359">давно</ts>
                  <nts id="Seg_10518" n="HIAT:ip">…</nts>
                  <nts id="Seg_10519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1367" id="Seg_10521" n="HIAT:u" s="T1361">
                  <ts e="T1364" id="Seg_10523" n="HIAT:w" s="T1361">Ага</ts>
                  <nts id="Seg_10524" n="HIAT:ip">,</nts>
                  <nts id="Seg_10525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1367" id="Seg_10527" n="HIAT:w" s="T1364">ага</ts>
                  <nts id="Seg_10528" n="HIAT:ip">.</nts>
                  <nts id="Seg_10529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1376" id="Seg_10530" n="sc" s="T1374">
               <ts e="T1376" id="Seg_10532" n="HIAT:u" s="T1374">
                  <ts e="T1376" id="Seg_10534" n="HIAT:w" s="T1374">Ага</ts>
                  <nts id="Seg_10535" n="HIAT:ip">.</nts>
                  <nts id="Seg_10536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1413" id="Seg_10537" n="sc" s="T1405">
               <ts e="T1413" id="Seg_10539" n="HIAT:u" s="T1405">
                  <nts id="Seg_10540" n="HIAT:ip">(</nts>
                  <nts id="Seg_10541" n="HIAT:ip">(</nts>
                  <ats e="T1413" id="Seg_10542" n="HIAT:non-pho" s="T1405">…</ats>
                  <nts id="Seg_10543" n="HIAT:ip">)</nts>
                  <nts id="Seg_10544" n="HIAT:ip">)</nts>
                  <nts id="Seg_10545" n="HIAT:ip">.</nts>
                  <nts id="Seg_10546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1415" id="Seg_10547" n="sc" s="T1414">
               <ts e="T1415" id="Seg_10549" n="HIAT:u" s="T1414">
                  <nts id="Seg_10550" n="HIAT:ip">(</nts>
                  <nts id="Seg_10551" n="HIAT:ip">(</nts>
                  <ats e="T1415" id="Seg_10552" n="HIAT:non-pho" s="T1414">…</ats>
                  <nts id="Seg_10553" n="HIAT:ip">)</nts>
                  <nts id="Seg_10554" n="HIAT:ip">)</nts>
                  <nts id="Seg_10555" n="HIAT:ip">.</nts>
                  <nts id="Seg_10556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1425" id="Seg_10557" n="sc" s="T1416">
               <ts e="T1425" id="Seg_10559" n="HIAT:u" s="T1416">
                  <ts e="T1417" id="Seg_10561" n="HIAT:w" s="T1416">Так</ts>
                  <nts id="Seg_10562" n="HIAT:ip">,</nts>
                  <nts id="Seg_10563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_10565" n="HIAT:w" s="T1417">он</ts>
                  <nts id="Seg_10566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1419" id="Seg_10568" n="HIAT:w" s="T1418">считает</ts>
                  <nts id="Seg_10569" n="HIAT:ip">,</nts>
                  <nts id="Seg_10570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1420" id="Seg_10572" n="HIAT:w" s="T1419">что</ts>
                  <nts id="Seg_10573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_10575" n="HIAT:w" s="T1420">наверное</ts>
                  <nts id="Seg_10576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1422" id="Seg_10578" n="HIAT:w" s="T1421">ты</ts>
                  <nts id="Seg_10579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1423" id="Seg_10581" n="HIAT:w" s="T1422">уже</ts>
                  <nts id="Seg_10582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_10584" n="HIAT:w" s="T1423">наверное</ts>
                  <nts id="Seg_10585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_10587" n="HIAT:w" s="T1424">устала</ts>
                  <nts id="Seg_10588" n="HIAT:ip">.</nts>
                  <nts id="Seg_10589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1429" id="Seg_10590" n="sc" s="T1426">
               <ts e="T1429" id="Seg_10592" n="HIAT:u" s="T1426">
                  <ts e="T1427" id="Seg_10594" n="HIAT:w" s="T1426">Но</ts>
                  <nts id="Seg_10595" n="HIAT:ip">,</nts>
                  <nts id="Seg_10596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1428" id="Seg_10598" n="HIAT:w" s="T1427">надо</ts>
                  <nts id="Seg_10599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_10601" n="HIAT:w" s="T1428">кончать</ts>
                  <nts id="Seg_10602" n="HIAT:ip">.</nts>
                  <nts id="Seg_10603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1448" id="Seg_10604" n="sc" s="T1430">
               <ts e="T1448" id="Seg_10606" n="HIAT:u" s="T1430">
                  <ts e="T1431" id="Seg_10608" n="HIAT:w" s="T1430">И</ts>
                  <nts id="Seg_10609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1432" id="Seg_10611" n="HIAT:w" s="T1431">очень</ts>
                  <nts id="Seg_10612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1433" id="Seg_10614" n="HIAT:w" s="T1432">большое</ts>
                  <nts id="Seg_10615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1434" id="Seg_10617" n="HIAT:w" s="T1433">тебе</ts>
                  <nts id="Seg_10618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1435" id="Seg_10620" n="HIAT:w" s="T1434">спасибо</ts>
                  <nts id="Seg_10621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_10623" n="HIAT:w" s="T1435">за</ts>
                  <nts id="Seg_10624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1437" id="Seg_10626" n="HIAT:w" s="T1436">эти</ts>
                  <nts id="Seg_10627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_10629" n="HIAT:w" s="T1437">очень</ts>
                  <nts id="Seg_10630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_10632" n="HIAT:w" s="T1438">редкие</ts>
                  <nts id="Seg_10633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1440" id="Seg_10635" n="HIAT:w" s="T1439">и</ts>
                  <nts id="Seg_10636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_10638" n="HIAT:w" s="T1440">очень</ts>
                  <nts id="Seg_10639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_10641" n="HIAT:w" s="T1441">ценные</ts>
                  <nts id="Seg_10642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_10644" n="HIAT:w" s="T1442">данные</ts>
                  <nts id="Seg_10645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1444" id="Seg_10647" n="HIAT:w" s="T1443">записи</ts>
                  <nts id="Seg_10648" n="HIAT:ip">,</nts>
                  <nts id="Seg_10649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_10651" n="HIAT:w" s="T1444">которые</ts>
                  <nts id="Seg_10652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1446" id="Seg_10654" n="HIAT:w" s="T1445">они</ts>
                  <nts id="Seg_10655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_10657" n="HIAT:w" s="T1446">сейчас</ts>
                  <nts id="Seg_10658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1448" id="Seg_10660" n="HIAT:w" s="T1447">получили</ts>
                  <nts id="Seg_10661" n="HIAT:ip">.</nts>
                  <nts id="Seg_10662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1450" id="Seg_10663" n="sc" s="T1449">
               <ts e="T1450" id="Seg_10665" n="HIAT:u" s="T1449">
                  <nts id="Seg_10666" n="HIAT:ip">(</nts>
                  <nts id="Seg_10667" n="HIAT:ip">(</nts>
                  <ats e="T1450" id="Seg_10668" n="HIAT:non-pho" s="T1449">…</ats>
                  <nts id="Seg_10669" n="HIAT:ip">)</nts>
                  <nts id="Seg_10670" n="HIAT:ip">)</nts>
                  <nts id="Seg_10671" n="HIAT:ip">.</nts>
                  <nts id="Seg_10672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1458" id="Seg_10673" n="sc" s="T1456">
               <ts e="T1458" id="Seg_10675" n="HIAT:u" s="T1456">
                  <nts id="Seg_10676" n="HIAT:ip">(</nts>
                  <nts id="Seg_10677" n="HIAT:ip">(</nts>
                  <ats e="T1458" id="Seg_10678" n="HIAT:non-pho" s="T1456">…</ats>
                  <nts id="Seg_10679" n="HIAT:ip">)</nts>
                  <nts id="Seg_10680" n="HIAT:ip">)</nts>
                  <nts id="Seg_10681" n="HIAT:ip">.</nts>
                  <nts id="Seg_10682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1460" id="Seg_10683" n="sc" s="T1459">
               <ts e="T1460" id="Seg_10685" n="HIAT:u" s="T1459">
                  <nts id="Seg_10686" n="HIAT:ip">(</nts>
                  <nts id="Seg_10687" n="HIAT:ip">(</nts>
                  <ats e="T1460" id="Seg_10688" n="HIAT:non-pho" s="T1459">…</ats>
                  <nts id="Seg_10689" n="HIAT:ip">)</nts>
                  <nts id="Seg_10690" n="HIAT:ip">)</nts>
                  <nts id="Seg_10691" n="HIAT:ip">.</nts>
                  <nts id="Seg_10692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T15" id="Seg_10693" n="sc" s="T11">
               <ts e="T15" id="Seg_10695" n="e" s="T11">((…)). </ts>
            </ts>
            <ts e="T31" id="Seg_10696" n="sc" s="T30">
               <ts e="T31" id="Seg_10698" n="e" s="T30">((…)). </ts>
            </ts>
            <ts e="T33" id="Seg_10699" n="sc" s="T32">
               <ts e="T33" id="Seg_10701" n="e" s="T32">((…)). </ts>
            </ts>
            <ts e="T50" id="Seg_10702" n="sc" s="T34">
               <ts e="T35" id="Seg_10704" n="e" s="T34">Как </ts>
               <ts e="T36" id="Seg_10706" n="e" s="T35">молодые </ts>
               <ts e="T37" id="Seg_10708" n="e" s="T36">между </ts>
               <ts e="T38" id="Seg_10710" n="e" s="T37">(со-) </ts>
               <ts e="T39" id="Seg_10712" n="e" s="T38">собой </ts>
               <ts e="T40" id="Seg_10714" n="e" s="T39">сообщались, </ts>
               <ts e="T41" id="Seg_10716" n="e" s="T40">когда </ts>
               <ts e="T42" id="Seg_10718" n="e" s="T41">(вы </ts>
               <ts e="T43" id="Seg_10720" n="e" s="T42">были </ts>
               <ts e="T44" id="Seg_10722" n="e" s="T43">еще </ts>
               <ts e="T45" id="Seg_10724" n="e" s="T44">молоды=) </ts>
               <ts e="T46" id="Seg_10726" n="e" s="T45">ты </ts>
               <ts e="T47" id="Seg_10728" n="e" s="T46">была </ts>
               <ts e="T48" id="Seg_10730" n="e" s="T47">еще </ts>
               <ts e="T50" id="Seg_10732" n="e" s="T48">молода. </ts>
            </ts>
            <ts e="T97" id="Seg_10733" n="sc" s="T90">
               <ts e="T97" id="Seg_10735" n="e" s="T90">((…)). </ts>
            </ts>
            <ts e="T99" id="Seg_10736" n="sc" s="T98">
               <ts e="T99" id="Seg_10738" n="e" s="T98">((…)). </ts>
            </ts>
            <ts e="T104" id="Seg_10739" n="sc" s="T100">
               <ts e="T101" id="Seg_10741" n="e" s="T100">Ну, </ts>
               <ts e="T102" id="Seg_10743" n="e" s="T101">насчет </ts>
               <ts e="T103" id="Seg_10745" n="e" s="T102">свадьб </ts>
               <ts e="T104" id="Seg_10747" n="e" s="T103">спрашивают. </ts>
            </ts>
            <ts e="T157" id="Seg_10748" n="sc" s="T156">
               <ts e="T157" id="Seg_10750" n="e" s="T156">((…)). </ts>
            </ts>
            <ts e="T159" id="Seg_10751" n="sc" s="T158">
               <ts e="T159" id="Seg_10753" n="e" s="T158">((…)). </ts>
            </ts>
            <ts e="T171" id="Seg_10754" n="sc" s="T160">
               <ts e="T161" id="Seg_10756" n="e" s="T160">Как </ts>
               <ts e="T162" id="Seg_10758" n="e" s="T161">они, </ts>
               <ts e="T163" id="Seg_10760" n="e" s="T162">сами </ts>
               <ts e="T164" id="Seg_10762" n="e" s="T163">решали, </ts>
               <ts e="T165" id="Seg_10764" n="e" s="T164">с </ts>
               <ts e="T166" id="Seg_10766" n="e" s="T165">кем </ts>
               <ts e="T167" id="Seg_10768" n="e" s="T166">жениться, </ts>
               <ts e="T168" id="Seg_10770" n="e" s="T167">или </ts>
               <ts e="T169" id="Seg_10772" n="e" s="T168">это </ts>
               <ts e="T170" id="Seg_10774" n="e" s="T169">родители </ts>
               <ts e="T171" id="Seg_10776" n="e" s="T170">решали? </ts>
            </ts>
            <ts e="T224" id="Seg_10777" n="sc" s="T216">
               <ts e="T224" id="Seg_10779" n="e" s="T216">((…)). </ts>
            </ts>
            <ts e="T227" id="Seg_10780" n="sc" s="T225">
               <ts e="T226" id="Seg_10782" n="e" s="T225">Нет, </ts>
               <ts e="T227" id="Seg_10784" n="e" s="T226">ничего. </ts>
            </ts>
            <ts e="T234" id="Seg_10785" n="sc" s="T228">
               <ts e="T234" id="Seg_10787" n="e" s="T228">((…)). </ts>
            </ts>
            <ts e="T236" id="Seg_10788" n="sc" s="T235">
               <ts e="T236" id="Seg_10790" n="e" s="T235">((…)). </ts>
            </ts>
            <ts e="T246" id="Seg_10791" n="sc" s="T237">
               <ts e="T238" id="Seg_10793" n="e" s="T237">Как </ts>
               <ts e="T239" id="Seg_10795" n="e" s="T238">муж </ts>
               <ts e="T240" id="Seg_10797" n="e" s="T239">и </ts>
               <ts e="T241" id="Seg_10799" n="e" s="T240">жена </ts>
               <ts e="T242" id="Seg_10801" n="e" s="T241">жили, </ts>
               <ts e="T243" id="Seg_10803" n="e" s="T242">хорошо </ts>
               <ts e="T244" id="Seg_10805" n="e" s="T243">обычно </ts>
               <ts e="T245" id="Seg_10807" n="e" s="T244">или </ts>
               <ts e="T246" id="Seg_10809" n="e" s="T245">как? </ts>
            </ts>
            <ts e="T268" id="Seg_10810" n="sc" s="T264">
               <ts e="T268" id="Seg_10812" n="e" s="T264">((…)). </ts>
            </ts>
            <ts e="T302" id="Seg_10813" n="sc" s="T300">
               <ts e="T302" id="Seg_10815" n="e" s="T300">((LAUGH)). </ts>
            </ts>
            <ts e="T350" id="Seg_10816" n="sc" s="T347">
               <ts e="T348" id="Seg_10818" n="e" s="T347">Они </ts>
               <ts e="T349" id="Seg_10820" n="e" s="T348">были </ts>
               <ts e="T350" id="Seg_10822" n="e" s="T349">камасинцы? </ts>
            </ts>
            <ts e="T362" id="Seg_10823" n="sc" s="T361">
               <ts e="T362" id="Seg_10825" n="e" s="T361">((…)). </ts>
            </ts>
            <ts e="T364" id="Seg_10826" n="sc" s="T363">
               <ts e="T364" id="Seg_10828" n="e" s="T363">((…)). </ts>
            </ts>
            <ts e="T370" id="Seg_10829" n="sc" s="T365">
               <ts e="T366" id="Seg_10831" n="e" s="T365">Было </ts>
               <ts e="T367" id="Seg_10833" n="e" s="T366">ли </ts>
               <ts e="T368" id="Seg_10835" n="e" s="T367">вообще </ts>
               <ts e="T369" id="Seg_10837" n="e" s="T368">много </ts>
               <ts e="T370" id="Seg_10839" n="e" s="T369">детей? </ts>
            </ts>
            <ts e="T382" id="Seg_10840" n="sc" s="T380">
               <ts e="T382" id="Seg_10842" n="e" s="T380">((…)). </ts>
            </ts>
            <ts e="T404" id="Seg_10843" n="sc" s="T401">
               <ts e="T404" id="Seg_10845" n="e" s="T401">((…)) ((COUGH)) </ts>
            </ts>
            <ts e="T412" id="Seg_10846" n="sc" s="T411">
               <ts e="T412" id="Seg_10848" n="e" s="T411">((…)). </ts>
            </ts>
            <ts e="T414" id="Seg_10849" n="sc" s="T413">
               <ts e="T414" id="Seg_10851" n="e" s="T413">((…)). </ts>
            </ts>
            <ts e="T423" id="Seg_10852" n="sc" s="T415">
               <ts e="T416" id="Seg_10854" n="e" s="T415">Как </ts>
               <ts e="T417" id="Seg_10856" n="e" s="T416">дети, </ts>
               <ts e="T418" id="Seg_10858" n="e" s="T417">были </ts>
               <ts e="T419" id="Seg_10860" n="e" s="T418">они </ts>
               <ts e="T420" id="Seg_10862" n="e" s="T419">вообще </ts>
               <ts e="T421" id="Seg_10864" n="e" s="T420">здоровы </ts>
               <ts e="T422" id="Seg_10866" n="e" s="T421">или </ts>
               <ts e="T423" id="Seg_10868" n="e" s="T422">болели? </ts>
            </ts>
            <ts e="T436" id="Seg_10869" n="sc" s="T424">
               <ts e="T425" id="Seg_10871" n="e" s="T424">Как </ts>
               <ts e="T426" id="Seg_10873" n="e" s="T425">они, </ts>
               <ts e="T427" id="Seg_10875" n="e" s="T426">давали </ts>
               <ts e="T428" id="Seg_10877" n="e" s="T427">матери </ts>
               <ts e="T429" id="Seg_10879" n="e" s="T428">и </ts>
               <ts e="T430" id="Seg_10881" n="e" s="T429">отцу </ts>
               <ts e="T431" id="Seg_10883" n="e" s="T430">спать </ts>
               <ts e="T432" id="Seg_10885" n="e" s="T431">ночью </ts>
               <ts e="T434" id="Seg_10887" n="e" s="T432">или </ts>
               <ts e="T435" id="Seg_10889" n="e" s="T434">кричали, </ts>
               <ts e="T436" id="Seg_10891" n="e" s="T435">плакали? </ts>
            </ts>
            <ts e="T442" id="Seg_10892" n="sc" s="T439">
               <ts e="T442" id="Seg_10894" n="e" s="T439">((…)). </ts>
            </ts>
            <ts e="T446" id="Seg_10895" n="sc" s="T445">
               <ts e="T446" id="Seg_10897" n="e" s="T445">((…)). </ts>
            </ts>
            <ts e="T448" id="Seg_10898" n="sc" s="T447">
               <ts e="T448" id="Seg_10900" n="e" s="T447">((…)). </ts>
            </ts>
            <ts e="T457" id="Seg_10901" n="sc" s="T449">
               <ts e="T450" id="Seg_10903" n="e" s="T449">Что </ts>
               <ts e="T451" id="Seg_10905" n="e" s="T450">делали </ts>
               <ts e="T452" id="Seg_10907" n="e" s="T451">ребенку, </ts>
               <ts e="T453" id="Seg_10909" n="e" s="T452">когда </ts>
               <ts e="T454" id="Seg_10911" n="e" s="T453">он </ts>
               <ts e="T455" id="Seg_10913" n="e" s="T454">много </ts>
               <ts e="T456" id="Seg_10915" n="e" s="T455">плакал </ts>
               <ts e="T457" id="Seg_10917" n="e" s="T456">ночью? </ts>
            </ts>
            <ts e="T482" id="Seg_10918" n="sc" s="T481">
               <ts e="T482" id="Seg_10920" n="e" s="T481">((…)). </ts>
            </ts>
            <ts e="T484" id="Seg_10921" n="sc" s="T483">
               <ts e="T484" id="Seg_10923" n="e" s="T483">((…)). </ts>
            </ts>
            <ts e="T492" id="Seg_10924" n="sc" s="T485">
               <ts e="T486" id="Seg_10926" n="e" s="T485">Как </ts>
               <ts e="T487" id="Seg_10928" n="e" s="T486">вообще </ts>
               <ts e="T488" id="Seg_10930" n="e" s="T487">дети, </ts>
               <ts e="T489" id="Seg_10932" n="e" s="T488">были </ts>
               <ts e="T490" id="Seg_10934" n="e" s="T489">ли </ts>
               <ts e="T491" id="Seg_10936" n="e" s="T490">они </ts>
               <ts e="T492" id="Seg_10938" n="e" s="T491">послушны? </ts>
            </ts>
            <ts e="T501" id="Seg_10939" n="sc" s="T493">
               <ts e="T494" id="Seg_10941" n="e" s="T493">И </ts>
               <ts e="T495" id="Seg_10943" n="e" s="T494">как </ts>
               <ts e="T496" id="Seg_10945" n="e" s="T495">вообще </ts>
               <ts e="T497" id="Seg_10947" n="e" s="T496">это </ts>
               <ts e="T498" id="Seg_10949" n="e" s="T497">семьи </ts>
               <ts e="T499" id="Seg_10951" n="e" s="T498">с </ts>
               <ts e="T500" id="Seg_10953" n="e" s="T499">ними </ts>
               <ts e="T501" id="Seg_10955" n="e" s="T500">справлялись? </ts>
            </ts>
            <ts e="T551" id="Seg_10956" n="sc" s="T545">
               <ts e="T551" id="Seg_10958" n="e" s="T545">((…)). </ts>
            </ts>
            <ts e="T554" id="Seg_10959" n="sc" s="T553">
               <ts e="T554" id="Seg_10961" n="e" s="T553">((…)). </ts>
            </ts>
            <ts e="T560" id="Seg_10962" n="sc" s="T555">
               <ts e="T556" id="Seg_10964" n="e" s="T555">Били </ts>
               <ts e="T557" id="Seg_10966" n="e" s="T556">ли, </ts>
               <ts e="T558" id="Seg_10968" n="e" s="T557">значит, </ts>
               <ts e="T559" id="Seg_10970" n="e" s="T558">родители </ts>
               <ts e="T560" id="Seg_10972" n="e" s="T559">детей? </ts>
            </ts>
            <ts e="T574" id="Seg_10973" n="sc" s="T573">
               <ts e="T574" id="Seg_10975" n="e" s="T573">((…)). </ts>
            </ts>
            <ts e="T586" id="Seg_10976" n="sc" s="T578">
               <ts e="T579" id="Seg_10978" n="e" s="T578">((…)). </ts>
               <ts e="T580" id="Seg_10980" n="e" s="T579">Чем </ts>
               <ts e="T581" id="Seg_10982" n="e" s="T580">это </ts>
               <ts e="T582" id="Seg_10984" n="e" s="T581">били, </ts>
               <ts e="T583" id="Seg_10986" n="e" s="T582">это </ts>
               <ts e="T584" id="Seg_10988" n="e" s="T583">я </ts>
               <ts e="T585" id="Seg_10990" n="e" s="T584">не </ts>
               <ts e="T586" id="Seg_10992" n="e" s="T585">понял. </ts>
            </ts>
            <ts e="T588" id="Seg_10993" n="sc" s="T587">
               <ts e="T588" id="Seg_10995" n="e" s="T587">((…)). </ts>
            </ts>
            <ts e="T594" id="Seg_10996" n="sc" s="T590">
               <ts e="T594" id="Seg_10998" n="e" s="T590">((…)). </ts>
            </ts>
            <ts e="T596" id="Seg_10999" n="sc" s="T595">
               <ts e="T596" id="Seg_11001" n="e" s="T595">((…)). </ts>
            </ts>
            <ts e="T601" id="Seg_11002" n="sc" s="T597">
               <ts e="T598" id="Seg_11004" n="e" s="T597">Умели </ts>
               <ts e="T599" id="Seg_11006" n="e" s="T598">ли </ts>
               <ts e="T600" id="Seg_11008" n="e" s="T599">камасинцы </ts>
               <ts e="T601" id="Seg_11010" n="e" s="T600">читать? </ts>
            </ts>
            <ts e="T607" id="Seg_11011" n="sc" s="T606">
               <ts e="T607" id="Seg_11013" n="e" s="T606">((…)). </ts>
            </ts>
            <ts e="T620" id="Seg_11014" n="sc" s="T617">
               <ts e="T620" id="Seg_11016" n="e" s="T617">((…)). </ts>
            </ts>
            <ts e="T648" id="Seg_11017" n="sc" s="T644">
               <ts e="T645" id="Seg_11019" n="e" s="T644">(Дальше) </ts>
               <ts e="T646" id="Seg_11021" n="e" s="T645">здесь </ts>
               <ts e="T648" id="Seg_11023" n="e" s="T646">говорили? </ts>
            </ts>
            <ts e="T663" id="Seg_11024" n="sc" s="T650">
               <ts e="T651" id="Seg_11026" n="e" s="T650">Ага, </ts>
               <ts e="T653" id="Seg_11028" n="e" s="T651">что </ts>
               <ts e="T654" id="Seg_11030" n="e" s="T653">такого </ts>
               <ts e="T656" id="Seg_11032" n="e" s="T654">языка </ts>
               <ts e="T657" id="Seg_11034" n="e" s="T656">нет, </ts>
               <ts e="T658" id="Seg_11036" n="e" s="T657">поскольку </ts>
               <ts e="T659" id="Seg_11038" n="e" s="T658">нет </ts>
               <ts e="T660" id="Seg_11040" n="e" s="T659">книг, </ts>
               <ts e="T662" id="Seg_11042" n="e" s="T660">значит, </ts>
               <ts e="T663" id="Seg_11044" n="e" s="T662">да? </ts>
            </ts>
            <ts e="T675" id="Seg_11045" n="sc" s="T670">
               <ts e="T673" id="Seg_11047" n="e" s="T670">Ага, </ts>
               <ts e="T675" id="Seg_11049" n="e" s="T673">ага. </ts>
            </ts>
            <ts e="T705" id="Seg_11050" n="sc" s="T704">
               <ts e="T705" id="Seg_11052" n="e" s="T704">((…)). </ts>
            </ts>
            <ts e="T707" id="Seg_11053" n="sc" s="T706">
               <ts e="T707" id="Seg_11055" n="e" s="T706">((…)). </ts>
            </ts>
            <ts e="T715" id="Seg_11056" n="sc" s="T708">
               <ts e="T709" id="Seg_11058" n="e" s="T708">Ты </ts>
               <ts e="T710" id="Seg_11060" n="e" s="T709">сама </ts>
               <ts e="T711" id="Seg_11062" n="e" s="T710">умеешь </ts>
               <ts e="T712" id="Seg_11064" n="e" s="T711">читать, </ts>
               <ts e="T713" id="Seg_11066" n="e" s="T712">умеешь </ts>
               <ts e="T714" id="Seg_11068" n="e" s="T713">писать, </ts>
               <ts e="T715" id="Seg_11070" n="e" s="T714">наверное? </ts>
            </ts>
            <ts e="T718" id="Seg_11071" n="sc" s="T716">
               <ts e="T717" id="Seg_11073" n="e" s="T716">Нет, </ts>
               <ts e="T718" id="Seg_11075" n="e" s="T717">да? </ts>
            </ts>
            <ts e="T720" id="Seg_11076" n="sc" s="T719">
               <ts e="T720" id="Seg_11078" n="e" s="T719">((…)). </ts>
            </ts>
            <ts e="T723" id="Seg_11079" n="sc" s="T721">
               <ts e="T723" id="Seg_11081" n="e" s="T721">((…)). </ts>
            </ts>
            <ts e="T743" id="Seg_11082" n="sc" s="T742">
               <ts e="T743" id="Seg_11084" n="e" s="T742">((…)). </ts>
            </ts>
            <ts e="T757" id="Seg_11085" n="sc" s="T744">
               <ts e="T745" id="Seg_11087" n="e" s="T744">((…)). </ts>
               <ts e="T746" id="Seg_11089" n="e" s="T745">Жили </ts>
               <ts e="T747" id="Seg_11091" n="e" s="T746">ли </ts>
               <ts e="T748" id="Seg_11093" n="e" s="T747">люди </ts>
               <ts e="T749" id="Seg_11095" n="e" s="T748">много </ts>
               <ts e="T750" id="Seg_11097" n="e" s="T749">лет </ts>
               <ts e="T751" id="Seg_11099" n="e" s="T750">в </ts>
               <ts e="T752" id="Seg_11101" n="e" s="T751">былые </ts>
               <ts e="T753" id="Seg_11103" n="e" s="T752">времена, </ts>
               <ts e="T754" id="Seg_11105" n="e" s="T753">камасинцы? </ts>
               <ts e="T755" id="Seg_11107" n="e" s="T754">Стали </ts>
               <ts e="T756" id="Seg_11109" n="e" s="T755">ли </ts>
               <ts e="T757" id="Seg_11111" n="e" s="T756">старым? </ts>
            </ts>
            <ts e="T769" id="Seg_11112" n="sc" s="T767">
               <ts e="T768" id="Seg_11114" n="e" s="T767">По-своему, </ts>
               <ts e="T769" id="Seg_11116" n="e" s="T768">да. </ts>
            </ts>
            <ts e="T799" id="Seg_11117" n="sc" s="T798">
               <ts e="T799" id="Seg_11119" n="e" s="T798">((…)). </ts>
            </ts>
            <ts e="T885" id="Seg_11120" n="sc" s="T869">
               <ts e="T885" id="Seg_11122" n="e" s="T869">((…)). </ts>
            </ts>
            <ts e="T891" id="Seg_11123" n="sc" s="T886">
               <ts e="T887" id="Seg_11125" n="e" s="T886">((…)). </ts>
               <ts e="T888" id="Seg_11127" n="e" s="T887">Как </ts>
               <ts e="T889" id="Seg_11129" n="e" s="T888">(это=) </ts>
               <ts e="T890" id="Seg_11131" n="e" s="T889">похороны </ts>
               <ts e="T891" id="Seg_11133" n="e" s="T890">были? </ts>
            </ts>
            <ts e="T941" id="Seg_11134" n="sc" s="T940">
               <ts e="T941" id="Seg_11136" n="e" s="T940">((…)). </ts>
            </ts>
            <ts e="T955" id="Seg_11137" n="sc" s="T943">
               <ts e="T945" id="Seg_11139" n="e" s="T943">Наверное, </ts>
               <ts e="T947" id="Seg_11141" n="e" s="T945">и </ts>
               <ts e="T948" id="Seg_11143" n="e" s="T947">пили </ts>
               <ts e="T949" id="Seg_11145" n="e" s="T948">еще? </ts>
               <ts e="T950" id="Seg_11147" n="e" s="T949">Не </ts>
               <ts e="T951" id="Seg_11149" n="e" s="T950">только </ts>
               <ts e="T952" id="Seg_11151" n="e" s="T951">ели, </ts>
               <ts e="T955" id="Seg_11153" n="e" s="T952">да? </ts>
            </ts>
            <ts e="T964" id="Seg_11154" n="sc" s="T958">
               <ts e="T960" id="Seg_11156" n="e" s="T958">И </ts>
               <ts e="T962" id="Seg_11158" n="e" s="T960">делали </ts>
               <ts e="T964" id="Seg_11160" n="e" s="T962">крест. </ts>
            </ts>
            <ts e="T985" id="Seg_11161" n="sc" s="T981">
               <ts e="T985" id="Seg_11163" n="e" s="T981">((…)). </ts>
            </ts>
            <ts e="T987" id="Seg_11164" n="sc" s="T986">
               <ts e="T987" id="Seg_11166" n="e" s="T986">((…)). </ts>
            </ts>
            <ts e="T998" id="Seg_11167" n="sc" s="T988">
               <ts e="T989" id="Seg_11169" n="e" s="T988">Во </ts>
               <ts e="T990" id="Seg_11171" n="e" s="T989">времена </ts>
               <ts e="T991" id="Seg_11173" n="e" s="T990">шаманов, </ts>
               <ts e="T992" id="Seg_11175" n="e" s="T991">наверное, </ts>
               <ts e="T993" id="Seg_11177" n="e" s="T992">(это=) </ts>
               <ts e="T994" id="Seg_11179" n="e" s="T993">крестов </ts>
               <ts e="T995" id="Seg_11181" n="e" s="T994">на </ts>
               <ts e="T996" id="Seg_11183" n="e" s="T995">могилах </ts>
               <ts e="T997" id="Seg_11185" n="e" s="T996">не </ts>
               <ts e="T998" id="Seg_11187" n="e" s="T997">ставили? </ts>
            </ts>
            <ts e="T1021" id="Seg_11188" n="sc" s="T1012">
               <ts e="T1021" id="Seg_11190" n="e" s="T1012">((…)). </ts>
            </ts>
            <ts e="T1049" id="Seg_11191" n="sc" s="T1041">
               <ts e="T1042" id="Seg_11193" n="e" s="T1041">Ага, </ts>
               <ts e="T1044" id="Seg_11195" n="e" s="T1042">ага. </ts>
               <ts e="T1046" id="Seg_11197" n="e" s="T1044">Это </ts>
               <ts e="T1047" id="Seg_11199" n="e" s="T1046">старое </ts>
               <ts e="T1049" id="Seg_11201" n="e" s="T1047">кладбище. </ts>
            </ts>
            <ts e="T1082" id="Seg_11202" n="sc" s="T1075">
               <ts e="T1077" id="Seg_11204" n="e" s="T1075">Там </ts>
               <ts e="T1078" id="Seg_11206" n="e" s="T1077">всё </ts>
               <ts e="T1080" id="Seg_11208" n="e" s="T1078">были </ts>
               <ts e="T1082" id="Seg_11210" n="e" s="T1080">кресты. </ts>
            </ts>
            <ts e="T1136" id="Seg_11211" n="sc" s="T1127">
               <ts e="T1136" id="Seg_11213" n="e" s="T1127">((…)). </ts>
            </ts>
            <ts e="T1159" id="Seg_11214" n="sc" s="T1154">
               <ts e="T1159" id="Seg_11216" n="e" s="T1154">((…)). </ts>
            </ts>
            <ts e="T1169" id="Seg_11217" n="sc" s="T1166">
               <ts e="T1169" id="Seg_11219" n="e" s="T1166">((…)). </ts>
            </ts>
            <ts e="T1171" id="Seg_11220" n="sc" s="T1170">
               <ts e="T1171" id="Seg_11222" n="e" s="T1170">((…)). </ts>
            </ts>
            <ts e="T1182" id="Seg_11223" n="sc" s="T1172">
               <ts e="T1173" id="Seg_11225" n="e" s="T1172">Так, </ts>
               <ts e="T1174" id="Seg_11227" n="e" s="T1173">когда </ts>
               <ts e="T1175" id="Seg_11229" n="e" s="T1174">человека </ts>
               <ts e="T1176" id="Seg_11231" n="e" s="T1175">похоронили, </ts>
               <ts e="T1177" id="Seg_11233" n="e" s="T1176">что </ts>
               <ts e="T1178" id="Seg_11235" n="e" s="T1177">стало </ts>
               <ts e="T1179" id="Seg_11237" n="e" s="T1178">затем, </ts>
               <ts e="T1180" id="Seg_11239" n="e" s="T1179">или </ts>
               <ts e="T1181" id="Seg_11241" n="e" s="T1180">было </ts>
               <ts e="T1182" id="Seg_11243" n="e" s="T1181">всё? </ts>
            </ts>
            <ts e="T1186" id="Seg_11244" n="sc" s="T1183">
               <ts e="T1184" id="Seg_11246" n="e" s="T1183">Что </ts>
               <ts e="T1185" id="Seg_11248" n="e" s="T1184">еще </ts>
               <ts e="T1186" id="Seg_11250" n="e" s="T1185">делали? </ts>
            </ts>
            <ts e="T1232" id="Seg_11251" n="sc" s="T1231">
               <ts e="T1232" id="Seg_11253" n="e" s="T1231">((…)). </ts>
            </ts>
            <ts e="T1254" id="Seg_11254" n="sc" s="T1233">
               <ts e="T1234" id="Seg_11256" n="e" s="T1233">Как </ts>
               <ts e="T1235" id="Seg_11258" n="e" s="T1234">это, </ts>
               <ts e="T1236" id="Seg_11260" n="e" s="T1235">насчет </ts>
               <ts e="T1237" id="Seg_11262" n="e" s="T1236">вот </ts>
               <ts e="T1238" id="Seg_11264" n="e" s="T1237">такого </ts>
               <ts e="T1239" id="Seg_11266" n="e" s="T1238">вопроса: </ts>
               <ts e="T1240" id="Seg_11268" n="e" s="T1239">был </ts>
               <ts e="T1241" id="Seg_11270" n="e" s="T1240">иногда </ts>
               <ts e="T1242" id="Seg_11272" n="e" s="T1241">разговор </ts>
               <ts e="T1243" id="Seg_11274" n="e" s="T1242">с </ts>
               <ts e="T1244" id="Seg_11276" n="e" s="T1243">матерью </ts>
               <ts e="T1245" id="Seg_11278" n="e" s="T1244">у </ts>
               <ts e="T1246" id="Seg_11280" n="e" s="T1245">тебя, </ts>
               <ts e="T1247" id="Seg_11282" n="e" s="T1246">что </ts>
               <ts e="T1248" id="Seg_11284" n="e" s="T1247">получится </ts>
               <ts e="T1249" id="Seg_11286" n="e" s="T1248">после </ts>
               <ts e="T1250" id="Seg_11288" n="e" s="T1249">смерти, </ts>
               <ts e="T1251" id="Seg_11290" n="e" s="T1250">что </ts>
               <ts e="T1252" id="Seg_11292" n="e" s="T1251">станет </ts>
               <ts e="T1253" id="Seg_11294" n="e" s="T1252">с </ts>
               <ts e="T1254" id="Seg_11296" n="e" s="T1253">человеком? </ts>
            </ts>
            <ts e="T1269" id="Seg_11297" n="sc" s="T1255">
               <ts e="T1256" id="Seg_11299" n="e" s="T1255">Будет </ts>
               <ts e="T1257" id="Seg_11301" n="e" s="T1256">ли </ts>
               <ts e="T1258" id="Seg_11303" n="e" s="T1257">он </ts>
               <ts e="T1259" id="Seg_11305" n="e" s="T1258">жить </ts>
               <ts e="T1260" id="Seg_11307" n="e" s="T1259">еще </ts>
               <ts e="T1261" id="Seg_11309" n="e" s="T1260">где-нибудь, </ts>
               <ts e="T1262" id="Seg_11311" n="e" s="T1261">продолжать </ts>
               <ts e="T1263" id="Seg_11313" n="e" s="T1262">жить, </ts>
               <ts e="T1264" id="Seg_11315" n="e" s="T1263">или </ts>
               <ts e="T1265" id="Seg_11317" n="e" s="T1264">все </ts>
               <ts e="T1266" id="Seg_11319" n="e" s="T1265">будет </ts>
               <ts e="T1267" id="Seg_11321" n="e" s="T1266">кончено, </ts>
               <ts e="T1268" id="Seg_11323" n="e" s="T1267">если </ts>
               <ts e="T1269" id="Seg_11325" n="e" s="T1268">похоронили? </ts>
            </ts>
            <ts e="T1311" id="Seg_11326" n="sc" s="T1310">
               <ts e="T1311" id="Seg_11328" n="e" s="T1310">((…)). </ts>
            </ts>
            <ts e="T1345" id="Seg_11329" n="sc" s="T1333">
               <ts e="T1345" id="Seg_11331" n="e" s="T1333">((LAUGH)) (Уже=) Уже начинаю не понимать, устал. </ts>
            </ts>
            <ts e="T1367" id="Seg_11332" n="sc" s="T1346">
               <ts e="T1347" id="Seg_11334" n="e" s="T1346">Это </ts>
               <ts e="T1349" id="Seg_11336" n="e" s="T1347">все </ts>
               <ts e="T1351" id="Seg_11338" n="e" s="T1349">же </ts>
               <ts e="T1353" id="Seg_11340" n="e" s="T1351">я </ts>
               <ts e="T1355" id="Seg_11342" n="e" s="T1353">не </ts>
               <ts e="T1357" id="Seg_11344" n="e" s="T1355">привык, </ts>
               <ts e="T1359" id="Seg_11346" n="e" s="T1357">уже </ts>
               <ts e="T1361" id="Seg_11348" n="e" s="T1359">давно… </ts>
               <ts e="T1364" id="Seg_11350" n="e" s="T1361">Ага, </ts>
               <ts e="T1367" id="Seg_11352" n="e" s="T1364">ага. </ts>
            </ts>
            <ts e="T1376" id="Seg_11353" n="sc" s="T1374">
               <ts e="T1376" id="Seg_11355" n="e" s="T1374">Ага. </ts>
            </ts>
            <ts e="T1413" id="Seg_11356" n="sc" s="T1405">
               <ts e="T1413" id="Seg_11358" n="e" s="T1405">((…)). </ts>
            </ts>
            <ts e="T1415" id="Seg_11359" n="sc" s="T1414">
               <ts e="T1415" id="Seg_11361" n="e" s="T1414">((…)). </ts>
            </ts>
            <ts e="T1425" id="Seg_11362" n="sc" s="T1416">
               <ts e="T1417" id="Seg_11364" n="e" s="T1416">Так, </ts>
               <ts e="T1418" id="Seg_11366" n="e" s="T1417">он </ts>
               <ts e="T1419" id="Seg_11368" n="e" s="T1418">считает, </ts>
               <ts e="T1420" id="Seg_11370" n="e" s="T1419">что </ts>
               <ts e="T1421" id="Seg_11372" n="e" s="T1420">наверное </ts>
               <ts e="T1422" id="Seg_11374" n="e" s="T1421">ты </ts>
               <ts e="T1423" id="Seg_11376" n="e" s="T1422">уже </ts>
               <ts e="T1424" id="Seg_11378" n="e" s="T1423">наверное </ts>
               <ts e="T1425" id="Seg_11380" n="e" s="T1424">устала. </ts>
            </ts>
            <ts e="T1429" id="Seg_11381" n="sc" s="T1426">
               <ts e="T1427" id="Seg_11383" n="e" s="T1426">Но, </ts>
               <ts e="T1428" id="Seg_11385" n="e" s="T1427">надо </ts>
               <ts e="T1429" id="Seg_11387" n="e" s="T1428">кончать. </ts>
            </ts>
            <ts e="T1448" id="Seg_11388" n="sc" s="T1430">
               <ts e="T1431" id="Seg_11390" n="e" s="T1430">И </ts>
               <ts e="T1432" id="Seg_11392" n="e" s="T1431">очень </ts>
               <ts e="T1433" id="Seg_11394" n="e" s="T1432">большое </ts>
               <ts e="T1434" id="Seg_11396" n="e" s="T1433">тебе </ts>
               <ts e="T1435" id="Seg_11398" n="e" s="T1434">спасибо </ts>
               <ts e="T1436" id="Seg_11400" n="e" s="T1435">за </ts>
               <ts e="T1437" id="Seg_11402" n="e" s="T1436">эти </ts>
               <ts e="T1438" id="Seg_11404" n="e" s="T1437">очень </ts>
               <ts e="T1439" id="Seg_11406" n="e" s="T1438">редкие </ts>
               <ts e="T1440" id="Seg_11408" n="e" s="T1439">и </ts>
               <ts e="T1441" id="Seg_11410" n="e" s="T1440">очень </ts>
               <ts e="T1442" id="Seg_11412" n="e" s="T1441">ценные </ts>
               <ts e="T1443" id="Seg_11414" n="e" s="T1442">данные </ts>
               <ts e="T1444" id="Seg_11416" n="e" s="T1443">записи, </ts>
               <ts e="T1445" id="Seg_11418" n="e" s="T1444">которые </ts>
               <ts e="T1446" id="Seg_11420" n="e" s="T1445">они </ts>
               <ts e="T1447" id="Seg_11422" n="e" s="T1446">сейчас </ts>
               <ts e="T1448" id="Seg_11424" n="e" s="T1447">получили. </ts>
            </ts>
            <ts e="T1450" id="Seg_11425" n="sc" s="T1449">
               <ts e="T1450" id="Seg_11427" n="e" s="T1449">((…)). </ts>
            </ts>
            <ts e="T1458" id="Seg_11428" n="sc" s="T1456">
               <ts e="T1458" id="Seg_11430" n="e" s="T1456">((…)). </ts>
            </ts>
            <ts e="T1460" id="Seg_11431" n="sc" s="T1459">
               <ts e="T1460" id="Seg_11433" n="e" s="T1459">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T15" id="Seg_11434" s="T11">PKZ_19700818_09341-2bz.KA.001 (002)</ta>
            <ta e="T31" id="Seg_11435" s="T30">PKZ_19700818_09341-2bz.KA.002 (004)</ta>
            <ta e="T33" id="Seg_11436" s="T32">PKZ_19700818_09341-2bz.KA.003 (005)</ta>
            <ta e="T50" id="Seg_11437" s="T34">PKZ_19700818_09341-2bz.KA.004 (006)</ta>
            <ta e="T97" id="Seg_11438" s="T90">PKZ_19700818_09341-2bz.KA.005 (012)</ta>
            <ta e="T99" id="Seg_11439" s="T98">PKZ_19700818_09341-2bz.KA.006 (014)</ta>
            <ta e="T104" id="Seg_11440" s="T100">PKZ_19700818_09341-2bz.KA.007 (015)</ta>
            <ta e="T157" id="Seg_11441" s="T156">PKZ_19700818_09341-2bz.KA.008 (024)</ta>
            <ta e="T159" id="Seg_11442" s="T158">PKZ_19700818_09341-2bz.KA.009 (025)</ta>
            <ta e="T171" id="Seg_11443" s="T160">PKZ_19700818_09341-2bz.KA.010 (026)</ta>
            <ta e="T224" id="Seg_11444" s="T216">PKZ_19700818_09341-2bz.KA.011 (034)</ta>
            <ta e="T227" id="Seg_11445" s="T225">PKZ_19700818_09341-2bz.KA.012 (036)</ta>
            <ta e="T234" id="Seg_11446" s="T228">PKZ_19700818_09341-2bz.KA.013 (037)</ta>
            <ta e="T236" id="Seg_11447" s="T235">PKZ_19700818_09341-2bz.KA.014 (039)</ta>
            <ta e="T246" id="Seg_11448" s="T237">PKZ_19700818_09341-2bz.KA.015 (040)</ta>
            <ta e="T268" id="Seg_11449" s="T264">PKZ_19700818_09341-2bz.KA.016 (044)</ta>
            <ta e="T302" id="Seg_11450" s="T300">PKZ_19700818_09341-2bz.KA.017 (044)</ta>
            <ta e="T350" id="Seg_11451" s="T347">PKZ_19700818_09341-2bz.KA.018 (052)</ta>
            <ta e="T362" id="Seg_11452" s="T361">PKZ_19700818_09341-2bz.KA.019 (054)</ta>
            <ta e="T364" id="Seg_11453" s="T363">PKZ_19700818_09341-2bz.KA.020 (055)</ta>
            <ta e="T370" id="Seg_11454" s="T365">PKZ_19700818_09341-2bz.KA.021 (056)</ta>
            <ta e="T382" id="Seg_11455" s="T380">PKZ_19700818_09341-2bz.KA.022 (059)</ta>
            <ta e="T404" id="Seg_11456" s="T401">PKZ_19700818_09341-2bz.KA.023 (063)</ta>
            <ta e="T412" id="Seg_11457" s="T411">PKZ_19700818_09341-2bz.KA.024 (065)</ta>
            <ta e="T414" id="Seg_11458" s="T413">PKZ_19700818_09341-2bz.KA.025 (066)</ta>
            <ta e="T423" id="Seg_11459" s="T415">PKZ_19700818_09341-2bz.KA.026 (067)</ta>
            <ta e="T436" id="Seg_11460" s="T424">PKZ_19700818_09341-2bz.KA.027 (068)</ta>
            <ta e="T442" id="Seg_11461" s="T439">PKZ_19700818_09341-2bz.KA.028 (070)</ta>
            <ta e="T446" id="Seg_11462" s="T445">PKZ_19700818_09341-2bz.KA.029 (072)</ta>
            <ta e="T448" id="Seg_11463" s="T447">PKZ_19700818_09341-2bz.KA.030 (073)</ta>
            <ta e="T457" id="Seg_11464" s="T449">PKZ_19700818_09341-2bz.KA.031 (074)</ta>
            <ta e="T482" id="Seg_11465" s="T481">PKZ_19700818_09341-2bz.KA.032 (080)</ta>
            <ta e="T484" id="Seg_11466" s="T483">PKZ_19700818_09341-2bz.KA.033 (081)</ta>
            <ta e="T492" id="Seg_11467" s="T485">PKZ_19700818_09341-2bz.KA.034 (082)</ta>
            <ta e="T501" id="Seg_11468" s="T493">PKZ_19700818_09341-2bz.KA.035 (083)</ta>
            <ta e="T551" id="Seg_11469" s="T545">PKZ_19700818_09341-2bz.KA.036 (091)</ta>
            <ta e="T554" id="Seg_11470" s="T553">PKZ_19700818_09341-2bz.KA.037 (093)</ta>
            <ta e="T560" id="Seg_11471" s="T555">PKZ_19700818_09341-2bz.KA.038 (094)</ta>
            <ta e="T574" id="Seg_11472" s="T573">PKZ_19700818_09341-2bz.KA.039 (099)</ta>
            <ta e="T579" id="Seg_11473" s="T578">PKZ_19700818_09341-2bz.KA.040 (100)</ta>
            <ta e="T586" id="Seg_11474" s="T579">PKZ_19700818_09341-2bz.KA.041 (101)</ta>
            <ta e="T588" id="Seg_11475" s="T587">PKZ_19700818_09341-2bz.KA.042 (102)</ta>
            <ta e="T594" id="Seg_11476" s="T590">PKZ_19700818_09341-2bz.KA.043 (104)</ta>
            <ta e="T596" id="Seg_11477" s="T595">PKZ_19700818_09341-2bz.KA.044 (106)</ta>
            <ta e="T601" id="Seg_11478" s="T597">PKZ_19700818_09341-2bz.KA.045 (107)</ta>
            <ta e="T607" id="Seg_11479" s="T606">PKZ_19700818_09341-2bz.KA.046 (109)</ta>
            <ta e="T620" id="Seg_11480" s="T617">PKZ_19700818_09341-2bz.KA.047 (111)</ta>
            <ta e="T648" id="Seg_11481" s="T644">PKZ_19700818_09341-2bz.KA.048 (115)</ta>
            <ta e="T663" id="Seg_11482" s="T650">PKZ_19700818_09341-2bz.KA.049 (117)</ta>
            <ta e="T675" id="Seg_11483" s="T670">PKZ_19700818_09341-2bz.KA.050 (119)</ta>
            <ta e="T705" id="Seg_11484" s="T704">PKZ_19700818_09341-2bz.KA.051 (124)</ta>
            <ta e="T707" id="Seg_11485" s="T706">PKZ_19700818_09341-2bz.KA.052 (125)</ta>
            <ta e="T715" id="Seg_11486" s="T708">PKZ_19700818_09341-2bz.KA.053 (126)</ta>
            <ta e="T718" id="Seg_11487" s="T716">PKZ_19700818_09341-2bz.KA.054 (127)</ta>
            <ta e="T720" id="Seg_11488" s="T719">PKZ_19700818_09341-2bz.KA.055 (128)</ta>
            <ta e="T723" id="Seg_11489" s="T721">PKZ_19700818_09341-2bz.KA.056 (129)</ta>
            <ta e="T743" id="Seg_11490" s="T742">PKZ_19700818_09341-2bz.KA.057 (133)</ta>
            <ta e="T745" id="Seg_11491" s="T744">PKZ_19700818_09341-2bz.KA.058 (134)</ta>
            <ta e="T754" id="Seg_11492" s="T745">PKZ_19700818_09341-2bz.KA.059 (135)</ta>
            <ta e="T757" id="Seg_11493" s="T754">PKZ_19700818_09341-2bz.KA.060 (136)</ta>
            <ta e="T769" id="Seg_11494" s="T767">PKZ_19700818_09341-2bz.KA.061 (139)</ta>
            <ta e="T799" id="Seg_11495" s="T798">PKZ_19700818_09341-2bz.KA.062 (147)</ta>
            <ta e="T885" id="Seg_11496" s="T869">PKZ_19700818_09341-2bz.KA.063 (157)</ta>
            <ta e="T887" id="Seg_11497" s="T886">PKZ_19700818_09341-2bz.KA.064 (160)</ta>
            <ta e="T891" id="Seg_11498" s="T887">PKZ_19700818_09341-2bz.KA.065 (161)</ta>
            <ta e="T941" id="Seg_11499" s="T940">PKZ_19700818_09341-2bz.KA.066 (169)</ta>
            <ta e="T949" id="Seg_11500" s="T943">PKZ_19700818_09341-2bz.KA.067 (171)</ta>
            <ta e="T955" id="Seg_11501" s="T949">PKZ_19700818_09341-2bz.KA.068 (173)</ta>
            <ta e="T964" id="Seg_11502" s="T958">PKZ_19700818_09341-2bz.KA.069 (175)</ta>
            <ta e="T985" id="Seg_11503" s="T981">PKZ_19700818_09341-2bz.KA.070 (178)</ta>
            <ta e="T987" id="Seg_11504" s="T986">PKZ_19700818_09341-2bz.KA.071 (180)</ta>
            <ta e="T998" id="Seg_11505" s="T988">PKZ_19700818_09341-2bz.KA.072 (181)</ta>
            <ta e="T1021" id="Seg_11506" s="T1012">PKZ_19700818_09341-2bz.KA.073 (184)</ta>
            <ta e="T1044" id="Seg_11507" s="T1041">PKZ_19700818_09341-2bz.KA.074 (187)</ta>
            <ta e="T1049" id="Seg_11508" s="T1044">PKZ_19700818_09341-2bz.KA.075 (188)</ta>
            <ta e="T1082" id="Seg_11509" s="T1075">PKZ_19700818_09341-2bz.KA.076 (191)</ta>
            <ta e="T1136" id="Seg_11510" s="T1127">PKZ_19700818_09341-2bz.KA.077 (196)</ta>
            <ta e="T1159" id="Seg_11511" s="T1154">PKZ_19700818_09341-2bz.KA.078 (200)</ta>
            <ta e="T1169" id="Seg_11512" s="T1166">PKZ_19700818_09341-2bz.KA.079 (203)</ta>
            <ta e="T1171" id="Seg_11513" s="T1170">PKZ_19700818_09341-2bz.KA.080 (204)</ta>
            <ta e="T1182" id="Seg_11514" s="T1172">PKZ_19700818_09341-2bz.KA.081 (205)</ta>
            <ta e="T1186" id="Seg_11515" s="T1183">PKZ_19700818_09341-2bz.KA.082 (206)</ta>
            <ta e="T1232" id="Seg_11516" s="T1231">PKZ_19700818_09341-2bz.KA.083 (213)</ta>
            <ta e="T1254" id="Seg_11517" s="T1233">PKZ_19700818_09341-2bz.KA.084 (214)</ta>
            <ta e="T1269" id="Seg_11518" s="T1255">PKZ_19700818_09341-2bz.KA.085 (215)</ta>
            <ta e="T1311" id="Seg_11519" s="T1310">PKZ_19700818_09341-2bz.KA.086 (222)</ta>
            <ta e="T1345" id="Seg_11520" s="T1333">PKZ_19700818_09341-2bz.KA.087 (226)</ta>
            <ta e="T1361" id="Seg_11521" s="T1346">PKZ_19700818_09341-2bz.KA.088 (227)</ta>
            <ta e="T1367" id="Seg_11522" s="T1361">PKZ_19700818_09341-2bz.KA.089 (229)</ta>
            <ta e="T1376" id="Seg_11523" s="T1374">PKZ_19700818_09341-2bz.KA.090 (231)</ta>
            <ta e="T1413" id="Seg_11524" s="T1405">PKZ_19700818_09341-2bz.KA.091 (233)</ta>
            <ta e="T1415" id="Seg_11525" s="T1414">PKZ_19700818_09341-2bz.KA.092 (235)</ta>
            <ta e="T1425" id="Seg_11526" s="T1416">PKZ_19700818_09341-2bz.KA.093 (236)</ta>
            <ta e="T1429" id="Seg_11527" s="T1426">PKZ_19700818_09341-2bz.KA.094 (237)</ta>
            <ta e="T1448" id="Seg_11528" s="T1430">PKZ_19700818_09341-2bz.KA.095 (238)</ta>
            <ta e="T1450" id="Seg_11529" s="T1449">PKZ_19700818_09341-2bz.KA.096 (239)</ta>
            <ta e="T1458" id="Seg_11530" s="T1456">PKZ_19700818_09341-2bz.KA.097 (242)</ta>
            <ta e="T1460" id="Seg_11531" s="T1459">PKZ_19700818_09341-2bz.KA.098 (243)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T15" id="Seg_11532" s="T11">((…)) </ta>
            <ta e="T31" id="Seg_11533" s="T30">((…)) </ta>
            <ta e="T33" id="Seg_11534" s="T32">((…)) </ta>
            <ta e="T50" id="Seg_11535" s="T34">Как молодые между (со-) собой сообщались, когда (вы были еще молоды=) ты была еще молода. </ta>
            <ta e="T97" id="Seg_11536" s="T90">((…)) </ta>
            <ta e="T99" id="Seg_11537" s="T98">((…)) </ta>
            <ta e="T104" id="Seg_11538" s="T100">Ну, насчет свадьб спрашивают. </ta>
            <ta e="T157" id="Seg_11539" s="T156">((…)) </ta>
            <ta e="T159" id="Seg_11540" s="T158">((…)) </ta>
            <ta e="T171" id="Seg_11541" s="T160">Как они, сами решали, с кем жениться, или это родители решали? </ta>
            <ta e="T224" id="Seg_11542" s="T216">((…)) </ta>
            <ta e="T227" id="Seg_11543" s="T225">Нет, ничего. </ta>
            <ta e="T234" id="Seg_11544" s="T228">((…)) </ta>
            <ta e="T236" id="Seg_11545" s="T235">((…)) </ta>
            <ta e="T246" id="Seg_11546" s="T237">Как муж и жена жили, хорошо обычно или как? </ta>
            <ta e="T268" id="Seg_11547" s="T264">((…)) </ta>
            <ta e="T302" id="Seg_11548" s="T300">((LAUGH)). </ta>
            <ta e="T350" id="Seg_11549" s="T347">Они были камасинцы? </ta>
            <ta e="T362" id="Seg_11550" s="T361">((…)) </ta>
            <ta e="T364" id="Seg_11551" s="T363">((…)) </ta>
            <ta e="T370" id="Seg_11552" s="T365">Было ли вообще много детей? </ta>
            <ta e="T382" id="Seg_11553" s="T380">((…)) </ta>
            <ta e="T404" id="Seg_11554" s="T401">((…)) ((COUGH)) </ta>
            <ta e="T412" id="Seg_11555" s="T411">((…)) </ta>
            <ta e="T414" id="Seg_11556" s="T413">((…)) </ta>
            <ta e="T423" id="Seg_11557" s="T415">Как дети, были они вообще здоровы или болели? </ta>
            <ta e="T436" id="Seg_11558" s="T424">Как они, давали матери и отцу спать ночью или кричали, плакали? </ta>
            <ta e="T442" id="Seg_11559" s="T439">((…)) </ta>
            <ta e="T446" id="Seg_11560" s="T445">((…)) </ta>
            <ta e="T448" id="Seg_11561" s="T447">((…)) </ta>
            <ta e="T457" id="Seg_11562" s="T449">Что делали ребенку, когда он много плакал ночью? </ta>
            <ta e="T482" id="Seg_11563" s="T481">((…)) </ta>
            <ta e="T484" id="Seg_11564" s="T483">((…)) </ta>
            <ta e="T492" id="Seg_11565" s="T485">Как вообще дети, были ли они послушны? </ta>
            <ta e="T501" id="Seg_11566" s="T493">И как вообще это семьи с ними справлялись? </ta>
            <ta e="T551" id="Seg_11567" s="T545">((…)) </ta>
            <ta e="T554" id="Seg_11568" s="T553">((…)) </ta>
            <ta e="T560" id="Seg_11569" s="T555">Били ли, значит, родители детей? </ta>
            <ta e="T574" id="Seg_11570" s="T573">((…)) </ta>
            <ta e="T579" id="Seg_11571" s="T578">((…)). </ta>
            <ta e="T586" id="Seg_11572" s="T579">Чем это били, это я не понял. </ta>
            <ta e="T588" id="Seg_11573" s="T587">((…)) </ta>
            <ta e="T594" id="Seg_11574" s="T590">((…)) </ta>
            <ta e="T596" id="Seg_11575" s="T595">((…)) </ta>
            <ta e="T601" id="Seg_11576" s="T597">Умели ли камасинцы читать? </ta>
            <ta e="T607" id="Seg_11577" s="T606">((…)) </ta>
            <ta e="T620" id="Seg_11578" s="T617">((…)) </ta>
            <ta e="T648" id="Seg_11579" s="T644">(Дальше) здесь говорили? </ta>
            <ta e="T663" id="Seg_11580" s="T650">Ага, что такого языка нет, поскольку нет книг, значит, да? </ta>
            <ta e="T675" id="Seg_11581" s="T670">Ага, ага. </ta>
            <ta e="T705" id="Seg_11582" s="T704">((…)) </ta>
            <ta e="T707" id="Seg_11583" s="T706">((…)) </ta>
            <ta e="T715" id="Seg_11584" s="T708">Ты сама умеешь читать, умеешь писать, наверное? </ta>
            <ta e="T718" id="Seg_11585" s="T716">Нет, да? </ta>
            <ta e="T720" id="Seg_11586" s="T719">((…)) </ta>
            <ta e="T723" id="Seg_11587" s="T721">((…)) </ta>
            <ta e="T743" id="Seg_11588" s="T742">((…)) </ta>
            <ta e="T745" id="Seg_11589" s="T744">((…)) </ta>
            <ta e="T754" id="Seg_11590" s="T745">Жили ли люди много лет в былые времена, камасинцы? </ta>
            <ta e="T757" id="Seg_11591" s="T754">Стали ли старым? </ta>
            <ta e="T769" id="Seg_11592" s="T767">По-своему, да. </ta>
            <ta e="T799" id="Seg_11593" s="T798">((…)) </ta>
            <ta e="T885" id="Seg_11594" s="T869">((…)) </ta>
            <ta e="T887" id="Seg_11595" s="T886">((…)) </ta>
            <ta e="T891" id="Seg_11596" s="T887">Как (это=) похороны были? </ta>
            <ta e="T941" id="Seg_11597" s="T940">((…)) </ta>
            <ta e="T949" id="Seg_11598" s="T943">Наверное, и пили еще? </ta>
            <ta e="T955" id="Seg_11599" s="T949">Не только ели, да? </ta>
            <ta e="T964" id="Seg_11600" s="T958">И делали крест. </ta>
            <ta e="T985" id="Seg_11601" s="T981">((…)) </ta>
            <ta e="T987" id="Seg_11602" s="T986">((…)) </ta>
            <ta e="T998" id="Seg_11603" s="T988">Во времена шаманов, наверное, (это=) крестов на могилах не ставили? </ta>
            <ta e="T1021" id="Seg_11604" s="T1012">((…)) </ta>
            <ta e="T1044" id="Seg_11605" s="T1041">Ага, ага. </ta>
            <ta e="T1049" id="Seg_11606" s="T1044">Это старое кладбище. </ta>
            <ta e="T1082" id="Seg_11607" s="T1075">Там всё были кресты. </ta>
            <ta e="T1136" id="Seg_11608" s="T1127">((…)) </ta>
            <ta e="T1159" id="Seg_11609" s="T1154">((…)) </ta>
            <ta e="T1169" id="Seg_11610" s="T1166">((…)) </ta>
            <ta e="T1171" id="Seg_11611" s="T1170">((…)) </ta>
            <ta e="T1182" id="Seg_11612" s="T1172">Так, когда человека похоронили, что стало затем, или было всё? </ta>
            <ta e="T1186" id="Seg_11613" s="T1183">Что еще делали? </ta>
            <ta e="T1232" id="Seg_11614" s="T1231">((…)) </ta>
            <ta e="T1254" id="Seg_11615" s="T1233">Как это, насчет вот такого вопроса: был иногда разговор с матерью у тебя, что получится после смерти, что станет с человеком? </ta>
            <ta e="T1269" id="Seg_11616" s="T1255">Будет ли он жить еще где-нибудь, продолжать жить, или все будет кончено, если похоронили? </ta>
            <ta e="T1311" id="Seg_11617" s="T1310">((…)) </ta>
            <ta e="T1345" id="Seg_11618" s="T1333">((LAUGH)) (Уже=) Уже начинаю не понимать, устал. </ta>
            <ta e="T1361" id="Seg_11619" s="T1346">Это все же я не привык, уже давно… </ta>
            <ta e="T1367" id="Seg_11620" s="T1361">Ага, ага. </ta>
            <ta e="T1376" id="Seg_11621" s="T1374">Ага. </ta>
            <ta e="T1413" id="Seg_11622" s="T1405">((…)) </ta>
            <ta e="T1415" id="Seg_11623" s="T1414">((…)) </ta>
            <ta e="T1425" id="Seg_11624" s="T1416">Так, он считает, что наверное ты уже наверное устала. </ta>
            <ta e="T1429" id="Seg_11625" s="T1426">Но, надо кончать. </ta>
            <ta e="T1448" id="Seg_11626" s="T1430">И очень большое тебе спасибо за эти очень редкие и очень ценные данные записи, которые они сейчас получили. </ta>
            <ta e="T1450" id="Seg_11627" s="T1449">((…)) </ta>
            <ta e="T1458" id="Seg_11628" s="T1456">((…)) </ta>
            <ta e="T1460" id="Seg_11629" s="T1459">((…)) </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T15" id="Seg_11630" s="T11">FIN:ext</ta>
            <ta e="T31" id="Seg_11631" s="T30">FIN:ext</ta>
            <ta e="T33" id="Seg_11632" s="T32">FIN:ext</ta>
            <ta e="T50" id="Seg_11633" s="T34">RUS:ext</ta>
            <ta e="T97" id="Seg_11634" s="T90">FIN:ext</ta>
            <ta e="T99" id="Seg_11635" s="T98">FIN:ext</ta>
            <ta e="T104" id="Seg_11636" s="T100">RUS:ext</ta>
            <ta e="T157" id="Seg_11637" s="T156">FIN:ext</ta>
            <ta e="T159" id="Seg_11638" s="T158">FIN:ext</ta>
            <ta e="T171" id="Seg_11639" s="T160">RUS:ext</ta>
            <ta e="T224" id="Seg_11640" s="T216">FIN:ext</ta>
            <ta e="T227" id="Seg_11641" s="T225">RUS:ext</ta>
            <ta e="T234" id="Seg_11642" s="T228">FIN:ext</ta>
            <ta e="T236" id="Seg_11643" s="T235">FIN:ext</ta>
            <ta e="T246" id="Seg_11644" s="T237">RUS:ext</ta>
            <ta e="T268" id="Seg_11645" s="T264">FIN:ext</ta>
            <ta e="T350" id="Seg_11646" s="T347">RUS:ext</ta>
            <ta e="T362" id="Seg_11647" s="T361">FIN:ext</ta>
            <ta e="T364" id="Seg_11648" s="T363">FIN:ext</ta>
            <ta e="T370" id="Seg_11649" s="T365">RUS:ext</ta>
            <ta e="T382" id="Seg_11650" s="T380">FIN:ext</ta>
            <ta e="T404" id="Seg_11651" s="T401">FIN:ext</ta>
            <ta e="T412" id="Seg_11652" s="T411">FIN:ext</ta>
            <ta e="T414" id="Seg_11653" s="T413">FIN:ext</ta>
            <ta e="T423" id="Seg_11654" s="T415">RUS:ext</ta>
            <ta e="T436" id="Seg_11655" s="T424">RUS:ext</ta>
            <ta e="T442" id="Seg_11656" s="T439">FIN:ext</ta>
            <ta e="T446" id="Seg_11657" s="T445">FIN:ext</ta>
            <ta e="T448" id="Seg_11658" s="T447">FIN:ext</ta>
            <ta e="T457" id="Seg_11659" s="T449">RUS:ext</ta>
            <ta e="T482" id="Seg_11660" s="T481">FIN:ext</ta>
            <ta e="T484" id="Seg_11661" s="T483">FIN:ext</ta>
            <ta e="T492" id="Seg_11662" s="T485">RUS:ext</ta>
            <ta e="T501" id="Seg_11663" s="T493">RUS:ext </ta>
            <ta e="T551" id="Seg_11664" s="T545">FIN:ext</ta>
            <ta e="T554" id="Seg_11665" s="T553">FIN:ext</ta>
            <ta e="T560" id="Seg_11666" s="T555">RUS:ext</ta>
            <ta e="T574" id="Seg_11667" s="T573">FIN:ext</ta>
            <ta e="T579" id="Seg_11668" s="T578">FIN:ext</ta>
            <ta e="T586" id="Seg_11669" s="T579">RUS:ext</ta>
            <ta e="T588" id="Seg_11670" s="T587">FIN:ext</ta>
            <ta e="T594" id="Seg_11671" s="T590">FIN:ext</ta>
            <ta e="T596" id="Seg_11672" s="T595">FIN:ext</ta>
            <ta e="T601" id="Seg_11673" s="T597">RUS:ext</ta>
            <ta e="T607" id="Seg_11674" s="T606">FIN:ext</ta>
            <ta e="T620" id="Seg_11675" s="T617">FIN:ext</ta>
            <ta e="T648" id="Seg_11676" s="T644">RUS:ext</ta>
            <ta e="T663" id="Seg_11677" s="T650">RUS:ext</ta>
            <ta e="T675" id="Seg_11678" s="T670">RUS:ext</ta>
            <ta e="T705" id="Seg_11679" s="T704">FIN:ext</ta>
            <ta e="T707" id="Seg_11680" s="T706">FIN:ext</ta>
            <ta e="T715" id="Seg_11681" s="T708">RUS:ext</ta>
            <ta e="T718" id="Seg_11682" s="T716">RUS:ext</ta>
            <ta e="T720" id="Seg_11683" s="T719">FIN:ext</ta>
            <ta e="T723" id="Seg_11684" s="T721">FIN:ext</ta>
            <ta e="T743" id="Seg_11685" s="T742">FIN:ext</ta>
            <ta e="T745" id="Seg_11686" s="T744">FIN:ext</ta>
            <ta e="T757" id="Seg_11687" s="T745">RUS:ext</ta>
            <ta e="T769" id="Seg_11688" s="T767">RUS:ext</ta>
            <ta e="T799" id="Seg_11689" s="T798">FIN:ext</ta>
            <ta e="T885" id="Seg_11690" s="T869">FIN:ext</ta>
            <ta e="T887" id="Seg_11691" s="T886">FIN:ext</ta>
            <ta e="T891" id="Seg_11692" s="T887">RUS:ext</ta>
            <ta e="T941" id="Seg_11693" s="T940">FIN:ext</ta>
            <ta e="T955" id="Seg_11694" s="T943">RUS:ext</ta>
            <ta e="T964" id="Seg_11695" s="T958">RUS:ext</ta>
            <ta e="T985" id="Seg_11696" s="T981">FIN:ext</ta>
            <ta e="T987" id="Seg_11697" s="T986">FIN:ext</ta>
            <ta e="T998" id="Seg_11698" s="T988">RUS:ext</ta>
            <ta e="T1021" id="Seg_11699" s="T1012">FIN:ext</ta>
            <ta e="T1049" id="Seg_11700" s="T1041">RUS:ext</ta>
            <ta e="T1082" id="Seg_11701" s="T1075">RUS:ext</ta>
            <ta e="T1136" id="Seg_11702" s="T1127">FIN:ext</ta>
            <ta e="T1159" id="Seg_11703" s="T1154">FIN:ext</ta>
            <ta e="T1169" id="Seg_11704" s="T1166">FIN:ext</ta>
            <ta e="T1171" id="Seg_11705" s="T1170">FIN:ext</ta>
            <ta e="T1182" id="Seg_11706" s="T1172">RUS:ext</ta>
            <ta e="T1186" id="Seg_11707" s="T1183">RUS:ext</ta>
            <ta e="T1232" id="Seg_11708" s="T1231">FIN:ext</ta>
            <ta e="T1254" id="Seg_11709" s="T1233">RUS:ext</ta>
            <ta e="T1269" id="Seg_11710" s="T1255">RUS:ext</ta>
            <ta e="T1311" id="Seg_11711" s="T1310">FIN:ext</ta>
            <ta e="T1345" id="Seg_11712" s="T1333">RUS:ext</ta>
            <ta e="T1361" id="Seg_11713" s="T1346">RUS:ext</ta>
            <ta e="T1367" id="Seg_11714" s="T1361">RUS:ext</ta>
            <ta e="T1376" id="Seg_11715" s="T1374">RUS:ext</ta>
            <ta e="T1413" id="Seg_11716" s="T1405">FIN:ext</ta>
            <ta e="T1415" id="Seg_11717" s="T1414">FIN:ext</ta>
            <ta e="T1425" id="Seg_11718" s="T1416">RUS:ext</ta>
            <ta e="T1429" id="Seg_11719" s="T1426">RUS:ext</ta>
            <ta e="T1448" id="Seg_11720" s="T1430">RUS:ext</ta>
            <ta e="T1450" id="Seg_11721" s="T1449">FIN:ext</ta>
            <ta e="T1458" id="Seg_11722" s="T1456">FIN:ext</ta>
            <ta e="T1460" id="Seg_11723" s="T1459">FIN:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA">
            <ta e="T50" id="Seg_11724" s="T34">How youthfully you communicated when you were still young.</ta>
            <ta e="T104" id="Seg_11725" s="T100">Well, they asked for marriage.</ta>
            <ta e="T171" id="Seg_11726" s="T160">Did they decide for themselves who they want to marry, or did their parents decide?</ta>
            <ta e="T227" id="Seg_11727" s="T225">No, nothing.</ta>
            <ta e="T246" id="Seg_11728" s="T237">How do husband and wife live together, usually well or not?</ta>
            <ta e="T350" id="Seg_11729" s="T347">Were they Kamas?</ta>
            <ta e="T370" id="Seg_11730" s="T365">Were there many children?</ta>
            <ta e="T423" id="Seg_11731" s="T415">Were the children more healthy of ill overall?</ta>
            <ta e="T436" id="Seg_11732" s="T424">Did they let their mothers and fathers sleep at night or did they cry, scream?</ta>
            <ta e="T457" id="Seg_11733" s="T449">What did you do with the child if it cried a lot at night?</ta>
            <ta e="T492" id="Seg_11734" s="T485">How were the children and did they behave?</ta>
            <ta e="T501" id="Seg_11735" s="T493">And how did the families deal with them?</ta>
            <ta e="T560" id="Seg_11736" s="T555">Did the parents hit their children?</ta>
            <ta e="T586" id="Seg_11737" s="T579">I did not understand what you were beaten with.</ta>
            <ta e="T601" id="Seg_11738" s="T597">Could they read Kamas?</ta>
            <ta e="T648" id="Seg_11739" s="T644">Did they continue to speak?</ta>
            <ta e="T663" id="Seg_11740" s="T650">Yes, that there wasn't a language like that, because there were no books, right?</ta>
            <ta e="T675" id="Seg_11741" s="T670">Yes, yes.</ta>
            <ta e="T715" id="Seg_11742" s="T708">You can read yourself, you can probably write?</ta>
            <ta e="T718" id="Seg_11743" s="T716">No, yes?</ta>
            <ta e="T754" id="Seg_11744" s="T745">Did people live for many years back then, the Kamas people?</ta>
            <ta e="T757" id="Seg_11745" s="T754">Did they get old?</ta>
            <ta e="T769" id="Seg_11746" s="T767">Your way, yes.</ta>
            <ta e="T891" id="Seg_11747" s="T887">How was the burial?</ta>
            <ta e="T949" id="Seg_11748" s="T943">They probably drank more?</ta>
            <ta e="T955" id="Seg_11749" s="T949">They did not just eat, did they?</ta>
            <ta e="T998" id="Seg_11750" s="T988">At the time of the shaman, they probably did not put crosses on the graves?</ta>
            <ta e="T1044" id="Seg_11751" s="T1041">Yes, yes.</ta>
            <ta e="T1049" id="Seg_11752" s="T1044">This is an old graveyard.</ta>
            <ta e="T1082" id="Seg_11753" s="T1075">There were crosses everywhere.</ta>
            <ta e="T1182" id="Seg_11754" s="T1172">So, when a person was buried, what happened next, or was that all?</ta>
            <ta e="T1186" id="Seg_11755" s="T1183">What else did you do?</ta>
            <ta e="T1254" id="Seg_11756" s="T1233">How about this question: Did you ever have a conversation with your mother about what happens after death, what happens to the person?</ta>
            <ta e="T1269" id="Seg_11757" s="T1255">Does the person live on somewhere else or is it over, when they are buried?</ta>
            <ta e="T1345" id="Seg_11758" s="T1333">I am starting to not understand now, I am tired.</ta>
            <ta e="T1361" id="Seg_11759" s="T1346">I have not become used to this…</ta>
            <ta e="T1367" id="Seg_11760" s="T1361">Yes, yes.</ta>
            <ta e="T1376" id="Seg_11761" s="T1374">Yes.</ta>
            <ta e="T1425" id="Seg_11762" s="T1416">He thinks, that you are probably tired.</ta>
            <ta e="T1429" id="Seg_11763" s="T1426">So, we need to finish up.</ta>
            <ta e="T1448" id="Seg_11764" s="T1430">Thank you very much for this very rare and wonderful recording, that you just gave us.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T50" id="Seg_11765" s="T34">Wie jung ihr miteinander kommuniziert habt, als (ihr noch jung wart =) du noch jung warst.</ta>
            <ta e="T104" id="Seg_11766" s="T100">Nun, nach Hochzeiten fragen sie.</ta>
            <ta e="T171" id="Seg_11767" s="T160">Haben sie für sich selbst entschieden, wen sie heiraten wollen, oder haben ihre Eltern entschieden?</ta>
            <ta e="T227" id="Seg_11768" s="T225">Nein, nichts.</ta>
            <ta e="T246" id="Seg_11769" s="T237">Wie lebten Ehemann und Ehefrau, normalerweise gut oder wie?</ta>
            <ta e="T350" id="Seg_11770" s="T347">Waren sie Kamassen?</ta>
            <ta e="T370" id="Seg_11771" s="T365">Gab es überhaupt viele Kinder?</ta>
            <ta e="T423" id="Seg_11772" s="T415">Waren die Kinder im Allgemeinen gesund oder krank?</ta>
            <ta e="T436" id="Seg_11773" s="T424">Haben sie ihre Mütter und Väter nachts schlafen lassen oder haben sie geweint, geschrien?</ta>
            <ta e="T457" id="Seg_11774" s="T449">Was hat man mit dem Kind gemacht, wenn es nachts viel geweint hat?</ta>
            <ta e="T492" id="Seg_11775" s="T485">Wie geht es den Kindern überhaupt und waren sie gehorsam?</ta>
            <ta e="T501" id="Seg_11776" s="T493">Und wie sind die Familien damit umgegangen?</ta>
            <ta e="T560" id="Seg_11777" s="T555">Haben die Eltern die Kinder geschlagen?</ta>
            <ta e="T586" id="Seg_11778" s="T579">Womit du geschlagen wurdest, habe ich nicht verstanden.</ta>
            <ta e="T601" id="Seg_11779" s="T597">Konnten sie Kamassisch lesen?</ta>
            <ta e="T648" id="Seg_11780" s="T644">Haben sie hier (weiter) gesprochen?</ta>
            <ta e="T663" id="Seg_11781" s="T650">Ja, dass es keine solche Sprache gibt, da es keine Bücher gibt, ja?</ta>
            <ta e="T675" id="Seg_11782" s="T670">Ja, ja. </ta>
            <ta e="T715" id="Seg_11783" s="T708">Du kannst selbst lesen, du kannst wahrscheinlich schreiben?</ta>
            <ta e="T718" id="Seg_11784" s="T716">Nein, ja?</ta>
            <ta e="T754" id="Seg_11785" s="T745">Haben die Leute früher viele Jahre gelebt, die Kamassen?</ta>
            <ta e="T757" id="Seg_11786" s="T754">Sind sie alt geworden?</ta>
            <ta e="T769" id="Seg_11787" s="T767">Auf deine Weise, ja. </ta>
            <ta e="T891" id="Seg_11788" s="T887">Wie war (dieses =) Begräbnis?</ta>
            <ta e="T949" id="Seg_11789" s="T943">Wahrscheinlich haben sie mehr getrunken?</ta>
            <ta e="T955" id="Seg_11790" s="T949">Nicht nur gegessen, oder?</ta>
            <ta e="T998" id="Seg_11791" s="T988">Zur Zeit der Schamanen wurden wahrscheinlich keine Kreuze auf die Gräber gelegt?</ta>
            <ta e="T1044" id="Seg_11792" s="T1041">Ja, ja. </ta>
            <ta e="T1049" id="Seg_11793" s="T1044">Dies ist ein alter Friedhof.</ta>
            <ta e="T1082" id="Seg_11794" s="T1075">Da waren alle Kreuze.</ta>
            <ta e="T1182" id="Seg_11795" s="T1172">Also, wenn eine Person begraben wurde, was geschah als nächstes, oder war das alles?</ta>
            <ta e="T1186" id="Seg_11796" s="T1183">Was hat man noch gemacht?</ta>
            <ta e="T1254" id="Seg_11797" s="T1233">Wie ist es mit dieser Frage: Hattest du manchmal ein Gespräch mit deiner Mutter, was passiert nach dem Tod, was passiert mit der Person?</ta>
            <ta e="T1269" id="Seg_11798" s="T1255">Wird diese Person woanders weiterleben oder ist es vorbei, wenn man begraben ist?</ta>
            <ta e="T1345" id="Seg_11799" s="T1333">Ich fange schon an es nicht mehr zu verstehen, ich bin müde. </ta>
            <ta e="T1361" id="Seg_11800" s="T1346">Ich bin noch lange nicht daran gewöhnt …</ta>
            <ta e="T1367" id="Seg_11801" s="T1361">Ja, ja. </ta>
            <ta e="T1376" id="Seg_11802" s="T1374">Ja. </ta>
            <ta e="T1425" id="Seg_11803" s="T1416">So, er denkt, dass du wahrscheinlich schon müde bist. </ta>
            <ta e="T1429" id="Seg_11804" s="T1426">So, wir müssen fertig werden. </ta>
            <ta e="T1448" id="Seg_11805" s="T1430">Und vielen Dank für diese sehr seltenen und sehr wertvollen Aufnahmedaten, die wir gerade erhalten haben. </ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
